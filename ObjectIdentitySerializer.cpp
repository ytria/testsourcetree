#include "ObjectIdentitySerializer.h"

ObjectIdentitySerializer::ObjectIdentitySerializer(const ObjectIdentity& p_ObjectIdentity)
	: m_ObjectIdentity(p_ObjectIdentity)
{
}

void ObjectIdentitySerializer::Serialize()
{
	m_Json = web::json::value::object();
	auto& obj = m_Json.as_object();

	JsonSerializeUtil::SerializeString(_YUID("signInType"), m_ObjectIdentity.m_SignInType, obj);
	JsonSerializeUtil::SerializeString(_YUID("issuer"), m_ObjectIdentity.m_Issuer, obj);
	JsonSerializeUtil::SerializeString(_YUID("issuerAssignedId"), m_ObjectIdentity.m_IssuerAssignedId, obj);
}
