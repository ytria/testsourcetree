#include "AppRoleDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"

void AppRoleDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		ListStringDeserializer allowedMemberTypesDeserializer;
		if (JsonSerializeUtil::DeserializeAny(allowedMemberTypesDeserializer, _YTEXT("allowedMemberTypes"), p_Object))
			m_Data.m_AllowedMemberTypes = allowedMemberTypesDeserializer.GetData();
	}

	JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.m_Description, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isEnabled"), m_Data.m_IsEnabled, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("origin"), m_Data.m_Origin, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("value"), m_Data.m_Value, p_Object);

}
