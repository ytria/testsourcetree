#include "CosmosDbAccountDeserializer.h"
#include "CosmosLocationDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"

void Azure::CosmosDbAccountDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);
	auto itt = p_Object.find(_YTEXT("properties"));
	ASSERT(itt != p_Object.end());
	if (itt != p_Object.end())
	{
		ASSERT(itt->second.is_object());
		if (itt->second.is_object())
		{
			const auto& propsObj = itt->second.as_object();
			ListDeserializer<Azure::CosmosLocation, Azure::CosmosLocationDeserializer> deserializer;
			JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("writeLocations"), propsObj);
			m_Data.m_WriteLocations = deserializer.GetData();

			JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, propsObj, true);
		}
	}
}
