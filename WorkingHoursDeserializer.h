#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class WorkingHoursDeserializer : public JsonObjectDeserializer, public Encapsulate<WorkingHours>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

