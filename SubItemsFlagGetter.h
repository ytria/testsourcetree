#pragma once

#include "GridModifications.h"
#include "IRowFlagsGetter.h"
#include "SapioError.h"

class SubItemsFlagGetter : public IRowFlagsGetter
{
public:
	SubItemsFlagGetter(GridModificationsO365& p_Modifications, GridBackendColumn* p_MultiValueElderColumn);

    virtual bool    IsFlaggedAsDeletion(GridBackendRow* p_Row, GridBackendColumn* p_ExplosionColumn) override;
    virtual bool    IsFlaggedAsUpdated(GridBackendRow* p_Row, GridBackendColumn* p_ExplosionColumn) override;
    virtual wstring IsFlaggedAsError(GridBackendRow* p_Row, GridBackendColumn* p_ExplosionColumn) override;
	virtual bool	AppliesOn(GridBackendColumn* p_Column) override;

	GridBackendColumn* GetMultiValueElderColumn();

private:
    wstring GetErrorMessage(const SapioError& p_Error) const;

    GridModificationsO365& m_Modifications;
	GridBackendColumn* m_MultiValueElderColumn;
};

