#include "DlgOnPremGroupEditHTML.h"

#include "OnPremiseGroupsColumns.h"

DlgOnPremGroupEditHTML::DlgOnPremGroupEditHTML(vector<OnPremiseGroup>& p_Groups, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
	: DlgFormsHTML(p_Action, p_Parent, _YTEXT(""))
	, m_Groups(p_Groups)
{
}

bool DlgOnPremGroupEditHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	std::map<wstring, wstring> alreadyProcessedProperties;

	for (const auto& item : data)
	{
		const auto& propName = item.first;
		ASSERT(!propName.empty());
		const auto& propValue = item.second;

		{
			auto itt = m_StringSetters.find(propName);
			if (m_StringSetters.end() != itt)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties[propName] = propValue;

				for (auto& group : m_Groups)
					itt->second(group, boost::YOpt<PooledString>(propValue));

				continue;
			}
		}

		{
			auto it = m_BoolSetters.find(propName);
			if (m_BoolSetters.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties[propName] = propValue;

				for (auto& businessUser : m_Groups)
					it->second(businessUser, boost::YOpt<bool>(Str::getBoolFromString(propValue)));

				continue;
			}
		}
	}

	return true;
}

void DlgOnPremGroupEditHTML::addEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, const vector<wstring>& p_ApplicableUserTypes)
{
	addObjectStringEditor<OnPremiseGroup>(m_Groups
		, p_Getter
		, p_PropName
		, OnPremiseGroupsColumns::GetTitle(p_PropName)
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, [p_Flags, &p_ApplicableUserTypes](const OnPremiseGroup&)
		{
			return 0;
		}
	);
	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

void DlgOnPremGroupEditHTML::addEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides /*= BoolEditor::CheckUncheckedLabelOverrides()*/)
{
	addObjectBoolToggleEditor<OnPremiseGroup>(m_Groups
		, p_Getter
		, p_PropName
		, OnPremiseGroupsColumns::GetTitle(p_PropName)
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, [p_Flags, &p_ApplicableUserTypes](const OnPremiseGroup&)
		{
			return 0;
		}
		, p_CheckUncheckedLabelOverrides
	);
	ASSERT(!hasProperty(p_PropName));
	m_BoolSetters[p_PropName] = p_Setter;
}

void DlgOnPremGroupEditHTML::addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues, const vector<wstring>& p_ApplicableUserTypes)
{
	addObjectListEditor<OnPremiseGroup>(m_Groups
		, p_Getter
		, p_PropName
		, OnPremiseGroupsColumns::GetTitle(p_PropName)
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, p_LabelsAndValues
		, [p_Flags, &p_ApplicableUserTypes](const OnPremiseGroup&)
		{
			return 0;
		}
	);
	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

void DlgOnPremGroupEditHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgOnPremGroupEditHTML"), false, true, true);

	getGenerator().addCategory(_T("Info"), CategoryFlags::EXPAND_BY_DEFAULT);
	{
		addEditor		(&OnPremiseGroup::GetDescription	, &OnPremiseGroup::SetDescription	, _YUID("onPremDescription")	, 0, {});
		addEditor		(&OnPremiseGroup::GetDisplayName	, &OnPremiseGroup::SetDisplayName	, _YUID("onPremDisplayName")	, 0, {});
		addComboEditor	(&OnPremiseGroup::GetGroupCategory	, &OnPremiseGroup::SetGroupCategory	, _YUID("onPremGroupCategory")	, 0, { { _T("Distribution"), _YTEXT("Distribution") }, { _T("Security"), _YTEXT("Security") } }, {});
		addComboEditor	(&OnPremiseGroup::GetGroupScope		, &OnPremiseGroup::SetGroupScope	, _YUID("onPremGroupScope")		, 0, { { _T("DomainLocal"), _YTEXT("DomainLocal") }, { _T("Global"), _YTEXT("Global") }, { _YTEXT("Universal"), _YTEXT("Universal") } }, {});
		addEditor		(&OnPremiseGroup::GetHomePage		, &OnPremiseGroup::SetHomePage		, _YUID("onPremHomePage")		, 0, {});
		addEditor		(&OnPremiseGroup::GetManagedBy		, &OnPremiseGroup::SetManagedBy		, _YUID("onPremManagedBy")		, 0, {});
	}

	getGenerator().addApplyButton(isEditDialog() ? LocalizedStrings::g_ApplyButtonText : LocalizedStrings::g_CreateButtonText);
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
	getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);
}

bool DlgOnPremGroupEditHTML::hasProperty(const wstring& p_PropertyName) const
{
	return m_StringSetters.end() != m_StringSetters.find(p_PropertyName);
}
