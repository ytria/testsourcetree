#include "MessageItemAttachmentRequester.h"

#include "BusinessItemAttachment.h"
#include "ItemAttachmentDeserializer.h"
#include "MSGraphSession.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

MessageItemAttachmentRequester::MessageItemAttachmentRequester(PooledString p_UserId, PooledString p_MessageId, PooledString p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger)
    : m_UserId(p_UserId), m_MessageId(p_MessageId), m_AttachmentId(p_AttachmentId), m_Logger(p_Logger)
{
}

TaskWrapper<void> MessageItemAttachmentRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ItemAttachmentDeserializer>();

    web::uri_builder uri(_YTEXT("users"));
    uri.append_path(m_UserId);
    uri.append_path(_YTEXT("messages"));
    uri.append_path(m_MessageId);
    uri.append_path(_YTEXT("attachments"));
    uri.append_path(m_AttachmentId);
    uri.append_query(_YTEXT("$expand"), _YTEXT("microsoft.graph.ItemAttachment/Item"));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
    return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, uri.to_uri(), false, m_Logger, httpLogger, p_TaskData);
}

const BusinessItemAttachment& MessageItemAttachmentRequester::GetData() const
{
    return m_Deserializer->GetData();
}
