#include "SignInStatusDeserializer.h"

#include "JsonSerializeUtil.h"

void SignInStatusDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeInt32(_YTEXT("errorCode"), m_Data.m_ErrorCode, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("failureReason"), m_Data.m_FailureReason, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("additionalDetails"), m_Data.m_AdditionalDetails, p_Object);
}
