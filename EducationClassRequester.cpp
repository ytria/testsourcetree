#include "EducationClassRequester.h"

#include "EducationClass.h"
#include "EducationClassDeserializer.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"

pplx::task<void> EducationClassRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<EducationClassDeserializer>();

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("classes"));
	uri.append_path(m_ClassId);

	return p_Session->GetMSGraphSession()->getObject(m_Deserializer, uri.to_uri(), false, true, p_TaskData);
}

const EducationClass& EducationClassRequester::GetData() const
{
	return m_Deserializer->GetData();
}
