#pragma once

#include "IRequester.h"
#include "IPageRequestLogger.h"
#include "Sapio365Session.h"

template <class T, class U>
class ValueListDeserializer;

class BusinessChatMessage;
class ChatMessageDeserializer;

class ChannelMessagesRequester : public IRequester
{
public:
    ChannelMessagesRequester(PooledString p_TeamId, PooledString p_ChannelId, const std::shared_ptr<IPageRequestLogger>& p_Logger);

    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
    const vector<BusinessChatMessage>& GetData() const;

    void SetSessionMode(Sapio365Session::SessionType p_SessionMode);

private:
    std::shared_ptr<ValueListDeserializer<BusinessChatMessage, ChatMessageDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;

    PooledString m_TeamId;
    PooledString m_ChannelId;

    Sapio365Session::SessionType m_SessionMode;
};

