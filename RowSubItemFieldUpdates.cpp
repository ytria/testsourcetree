#include "RowSubItemFieldUpdates.h"

#include "BaseO365Grid.h"

RowSubItemFieldUpdates::RowSubItemFieldUpdates(	O365Grid& p_Grid,
												const SubItemKey& p_Key,
												GridBackendColumn* p_ColSubItemElder,
												GridBackendColumn* p_ColSubItemPrimaryKey,
												GridBackendRow* p_Row)
    : ISubItemModification(p_Grid, p_Key, p_ColSubItemElder, p_ColSubItemPrimaryKey, p_Grid.GetName(p_Row))
{
    p_Row->SetEditModified(true);
    GetGrid().OnRowModified(p_Row);
}

RowSubItemFieldUpdates::~RowSubItemFieldUpdates()
{
    // TODO: Revert 
}

bool RowSubItemFieldUpdates::IsCorrespondingRow(GridBackendRow* p_Row) const
{
    bool corresponding = false;
    for (const auto& fieldUpdate : m_FieldUpdates)
    {
        if (fieldUpdate->IsCorrespondingRow(p_Row))
        {
            corresponding = true;
            break;
        }
    }

    return corresponding;
}

void RowSubItemFieldUpdates::AddFieldUpdate(std::unique_ptr<SubItemFieldUpdate> p_FieldUpdate)
{
    m_State = Modification::State::AppliedLocally;

    // Merge with existing field modification if it already exists. Else, overwrite
    bool fieldExisted = false;
    for (auto& fieldMod : m_FieldUpdates)
    {
        if (fieldMod->GetColumnKey() == p_FieldUpdate->GetColumnKey())
        {
            fieldMod = SubItemFieldUpdate::CreateReplacement(*fieldMod, std::move(p_FieldUpdate));
            fieldExisted = true;
            break;
        }
    }

    if (!fieldExisted)
    {
        m_FieldUpdates.push_back(std::move(p_FieldUpdate));
    }
}

void RowSubItemFieldUpdates::ShowAppliedLocally(GridBackendRow* p_Row) const
{
	// Nothing to do here.
	ASSERT(false);
}

void RowSubItemFieldUpdates::RevertSpecific(const SubItemInfo& p_Info)
{
    for (auto& fieldUpdate : m_FieldUpdates)
    {
        fieldUpdate->RevertSpecific(p_Info);
    }
}

const std::vector<std::unique_ptr<SubItemFieldUpdate>>& RowSubItemFieldUpdates::GetFieldUpdates() const
{
    return m_FieldUpdates;
}

bool RowSubItemFieldUpdates::Refresh(GridBackendRow* p_Row, const std::map<SubItemKey, HttpResultWithError>& p_Errors)
{
    if (m_FieldUpdates.empty())
        return false;

    const auto oldState = m_State;

    m_State = GetHighestPriorityState(p_Row, p_Errors);

    switch (m_State)
    {
    case Modification::State::RemoteError:
    {
        auto ittErr = p_Errors.find(GetKey());
        ASSERT(ittErr != p_Errors.end());
        if (ittErr != p_Errors.end())
        {
            auto httpResultError = ittErr->second;
            ASSERT(boost::none != httpResultError.m_Error);
            SetError(httpResultError.m_Error);
            if (!IsCollapsedRow(p_Row))
            {
                if (httpResultError.m_Error)
                    GetGrid().OnRowError(p_Row, false, httpResultError.m_Error->GetFullErrorMessage());
            }
        }
        break;
    }
    case Modification::State::AppliedLocally:
        if (!p_Row->IsModified())
            GetGrid().OnRowModified(p_Row);
        break;
    case Modification::State::RemoteHasOldValue:
        if (!p_Row->IsChangesSaved())
            GetGrid().OnRowSavedNotReceived(p_Row, false, YtriaTranslate::Do(RowSubItemFieldUpdates_Refresh_1, _YLOC("One or more modified value(s) have not been received.")).c_str());
        break;
    case Modification::State::RemoteHasNewValue:
        if (!p_Row->IsChangesSaved())
            GetGrid().OnRowSaved(p_Row, false, YtriaTranslate::Do(RowSubItemFieldUpdates_Refresh_2, _YLOC("All values have been modified successfully.")).c_str());
        break;
    case Modification::State::RemoteHasOtherValue:
        if (!p_Row->IsChangesSaved())
            GetGrid().OnRowSavedOverwritten(p_Row, false, YtriaTranslate::Do(RowSubItemFieldUpdates_Refresh_3, _YLOC("One or more value(s) sent have been overwritten.")).c_str());
        break;
    default:
        break;
    }

    return oldState != m_State;
}

Modification::State RowSubItemFieldUpdates::GetHighestPriorityState(GridBackendRow* p_Row, const std::map<SubItemKey, HttpResultWithError>& p_Errors) const
{
    int highestPriorityState = 0;
    for (const auto& modification : m_FieldUpdates)
    {
        modification->Refresh(p_Row, p_Errors);
        if (static_cast<int>(modification->GetState()) > highestPriorityState)
            highestPriorityState = static_cast<int>(modification->GetState());
    }

    return static_cast<State>(highestPriorityState);
}

vector<Modification::ModificationLog> RowSubItemFieldUpdates::GetModificationLogs() const
{
	vector<Modification::ModificationLog> mlogs;

	for (const auto& fu : GetFieldUpdates())
	{
		const auto fumLogs = fu->GetModificationLogs();
		mlogs.insert(mlogs.end(), fumLogs.begin(), fumLogs.end());
	}

	return mlogs;
}