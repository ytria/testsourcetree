#include "CreateMessageRequester.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

CreateMessageRequester::CreateMessageRequester(const wstring& p_Subject, const wstring& p_ContentType, const wstring& p_Content)
	:m_Subject(p_Subject),
	m_ContentType(p_ContentType),
	m_Content(p_Content),
	m_UseMainGraphSession(false)
{
}

void CreateMessageRequester::AddRecipient(const wstring& p_Recipient)
{
	m_Recipients.push_back(p_Recipient);
}

TaskWrapper<void> CreateMessageRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	web::uri_builder uri;
	uri.append_path(_YTEXT("me"));
	uri.append_path(_YTEXT("messages"));

	vector<web::json::value> recipients;
	for (const auto& recipient : m_Recipients)
	{
		web::json::value element = web::json::value::object({
			std::make_pair(_YTEXT("emailAddress"), web::json::value::object({
				std::make_pair(_YTEXT("address"), web::json::value(recipient))
			}))
		});
		recipients.push_back(element);
	}

	web::json::value message = web::json::value::object({
		std::make_pair(_YTEXT("subject"), web::json::value(m_Subject)),
		std::make_pair(_YTEXT("body"), web::json::value::object({
			std::make_pair(_YTEXT("contentType"), web::json::value(m_ContentType)),
			std::make_pair(_YTEXT("content"), web::json::value(m_Content))
		})),
		std::make_pair(_YTEXT("toRecipients"), web::json::value::array(recipients))
	});

	WebPayloadJSON payload(message);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	m_Result = std::make_shared<HttpResultWithError>();
	m_MessageId = std::make_shared<wstring>();

	auto graphSession = p_Session->GetMSGraphSession(httpLogger->GetSessionType());
	if (m_UseMainGraphSession)
		graphSession = p_Session->GetMainMSGraphSession();

	return graphSession->Post(uri.to_uri(), httpLogger, p_TaskData, payload).Then([httpLogger, result = m_Result, msgId = m_MessageId] (const RestResultInfo& p_Result) {
		*result = HttpResultWithError(p_Result);

		int status = result->m_ResultInfo->Response.status_code();
		bool success = status >= 200 && status < 300;
		if (success && result->m_ResultInfo->GetBodyAsString())
		{
			web::json::value response = json::value::parse(*result->m_ResultInfo->GetBodyAsString());
			*msgId = response[_YTEXT("id")].as_string();
		}
	});
}

void CreateMessageRequester::UseMainGraphSession()
{
	m_UseMainGraphSession = true;
}

const HttpResultWithError& CreateMessageRequester::GetResult() const
{
	return *m_Result;
}

wstring CreateMessageRequester::GetMessageId() const
{
	return *m_MessageId;
}
