#include "ModuleOnPremiseGroups.h"

#include "FrameMailboxPermissions.h"
#include "RefreshSpecificData.h"
#include "YCallbackMessage.h"
#include "MultiObjectsRequestLogger.h"
#include "InvokeResultWrapper.h"
#include "FrameOnPremiseGroups.h"
#include "BasicPageRequestLogger.h"
#include "OnPremiseGroupsRequester.h"
#include "safeTaskCall.h"
#include "OnPremiseGroupsCollectionDeserializer.h"
#include "ModuleUtil.h"

void ModuleOnPremiseGroups::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
	case Command::ModuleTask::List:
		showOnPremiseGroups(p_Command);
		break;
	case Command::ModuleTask::UpdateRefresh:
		showOnPremiseGroups(p_Command);
		break;
	default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
		break;
	}
}

void ModuleOnPremiseGroups::showOnPremiseGroups(const Command& p_Command)
{
	auto		p_SourceWindow = p_Command.GetSourceWindow();
	auto		p_Action = p_Command.GetAutomationAction();

	bool isUpdate = false;

	auto& info = p_Command.GetCommandInfo();
	
	FrameOnPremiseGroups* frame = dynamic_cast<FrameOnPremiseGroups*>(info.GetFrame());

	const bool isRefresh = nullptr != frame;
	if (nullptr == frame)
	{
		if (!FrameOnPremiseGroups::CanCreateNewFrame(true, p_SourceWindow.GetCWnd(), p_Action))
			return;

		if (!ModuleUtil::WarnIfPowerShellHostIncompatible(p_SourceWindow.GetCWnd()))
			return;

		ModuleCriteria moduleCriteria;
		moduleCriteria.m_Origin = Origin::Tenant;
		moduleCriteria.m_UsedContainer = ModuleCriteria::UsedContainer::IDS;
		moduleCriteria.m_Privilege = info.GetRBACPrivilege();

		ASSERT(moduleCriteria.m_IDs.end() == std::find_if(moduleCriteria.m_IDs.begin(), moduleCriteria.m_IDs.end(), [](const PooledString& id) {return O365Grid::IsTemporaryCreatedObjectID(id); }));

		if (ShouldCreateFrame<FrameOnPremiseGroups>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
		{
			CWaitCursor _;

			frame = new FrameOnPremiseGroups(false, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), _T("On-Premises Groups"), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
			frame->InitModuleCriteria(moduleCriteria);
			AddGridFrame(frame);
			frame->Erect();
		}
	}
	else
	{
		isUpdate = true;
	}

	// When the Group wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
	if (nullptr != frame)
	{
		GridOnPremiseGroups* grid = dynamic_cast<GridOnPremiseGroups*>(&frame->GetGrid());

		auto taskData = Util::AddFrameTask(_T("Show On-Premises Groups"), frame, p_Command.GetAutomationSetup(), p_Command.GetLicenseContext(), !isUpdate);

		YSafeCreateTask([isUpdate, taskData, hwnd = frame->GetSafeHwnd(), moduleCriteria = frame->GetModuleCriteria(), autoAction = p_Command.GetAutomationAction(), grid, bom = frame->GetBusinessObjectManager()]()
		{
			auto deserializer = std::make_shared<OnPremiseGroupsCollectionDeserializer>();

			auto logger = std::make_shared<BasicRequestLogger>(_T("Groups"));
			auto requester = std::make_shared<OnPremiseGroupsRequester>(deserializer, logger);
			requester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();

			wstring error = GetPSErrorString(requester->GetResult());
			if (!error.empty())
			{
				OnPremiseGroup errorGroup;
				errorGroup.SetError(SapioError(error, 0));

				deserializer->GetData() = { errorGroup };
			}

			YCallbackMessage::DoPost([=]() {
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameOnPremiseGroups*>(CWnd::FromHandle(hwnd)) : nullptr;

				if (ModuleBase::ShouldBuildView(hwnd, taskData.IsCanceled(), taskData, false))
				{
					ASSERT(nullptr != frame);
					if (nullptr != frame)
					{
						frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s Groups...", Str::getStringFromNumber(deserializer->GetData().size()).c_str()));
						frame->GetGrid().ClearLog(taskData.GetId());

						{
							CWaitCursor _;
							frame->ShowOnPremiseGroups(deserializer->GetData(), true);
						}

						if (!isUpdate)
						{
							frame->UpdateContext(taskData, hwnd);
							shouldFinishTask = false;
						}
					}
				}

				if (shouldFinishTask)
				{
					frame->ProcessLicenseContext(); // Let's consume license tokens if needed
					frame->TaskFinished(taskData, nullptr, hwnd);
				}
			});
		});
	}
	else
	{
		SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
}
