#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "TextColumn.h"

class TextColumnDeserializer : public JsonObjectDeserializer, public Encapsulate<TextColumn>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

