#include "QueryRoleExists.h"
#include "SessionsSqlEngine.h"
#include "SqlQuerySelect.h"

QueryRoleExists::QueryRoleExists(int64_t p_RoleId, SessionsSqlEngine& p_Engine)
	:m_RoleId(p_RoleId),
	m_Engine(p_Engine)
{
}

void QueryRoleExists::Run()
{
	SqlQuerySelect query(m_Engine, m_Handler, _YTEXT(R"(SELECT 1 FROM Roles WHERE Id=?)"));
	query.BindInt64(1, m_RoleId);
	query.Run();
}

bool QueryRoleExists::Exists() const
{
	return m_Handler.Exists();
}
