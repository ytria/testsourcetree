#pragma once

#include "BusinessUser.h"
#include "DlgFormsHTML.h"
#include "Organization.h"

class BaseDlgBusinessUserEditHTML : public DlgFormsHTML
{
public:
	BaseDlgBusinessUserEditHTML(vector<BusinessUser>& p_BusinessUsers, DlgFormsHTML::Action p_Action, CWnd* p_Parent, const wstring& p_AutomationActionName);
	BaseDlgBusinessUserEditHTML(vector<BusinessUser>& p_BusinessUsers, DlgFormsHTML::Action p_Action, CWnd* p_Parent);
	virtual ~BaseDlgBusinessUserEditHTML();

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override final;

	//Returns true if processed
	virtual bool processPostedDataSpecific(const IPostedDataTarget::PostedData::value_type& p_Value) = 0;

	virtual void postProcessPostedData() = 0;

protected:
	using StringGetter = std::function<const boost::YOpt<PooledString>&(const BusinessUser&)>;
	using StringSetter = std::function<void(BusinessUser&, const boost::YOpt<PooledString>&)>;
	void addStringEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes);
	void addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues, vector<wstring> p_ApplicableUserTypes);
	void addUserNameEditor(StringGetter p_Getter, StringSetter p_SetterLeft, StringSetter p_SetterRight, const wstring& p_PropNameLeft, const wstring& p_PropNameRight, const wstring& p_PropLabel, uint32_t p_Flags, const vector<std::tuple<wstring, wstring>>& p_LabelsAndValues, vector<wstring> p_ApplicableUserTypes);
	void addMultiCommaSeparatedStringsCheckListEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const map<wstring, wstring>& p_LabelsAndValues, vector<wstring> p_ApplicableUserTypes);

	using BoolGetter = std::function<const boost::YOpt<bool>&(const BusinessUser&)>;
	using BoolSetter = std::function<void(BusinessUser&, const boost::YOpt<bool>&)>;
	void addBoolEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides = BoolEditor::CheckUncheckedLabelOverrides());
	void addBoolToggleEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides = BoolEditor::CheckUncheckedLabelOverrides());

	using DateGetter = std::function<const boost::YOpt<YTimeDate>&(const BusinessUser&)>;
	using DateSetter = std::function<void(BusinessUser&, const boost::YOpt<YTimeDate>&)>;
	// If p_MaxDate contains an empty string, today's date is used
	void addDateEditor(DateGetter p_Getter, DateSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, const boost::YOpt<wstring>& p_MaxDate);

	const ComboEditor::LabelsAndValues& getLocationList();
	const ComboEditor::LabelsAndValues& getLanguageList();

	static uint32_t getRestrictionsImpl(const BusinessUser& bu, uint32_t p_Flags, const vector<wstring>& p_ApplicableUserTypes);

	bool hasProperty(const wstring& p_PropertyName) const;

protected:
	vector<BusinessUser>& m_BusinessUsers;

	std::map<wstring, StringSetter> m_StringSetters;
	std::map<wstring, StringSetter> m_MultiCommaSeparatedStringSetters;
	std::map<wstring, BoolSetter>	m_BoolSetters;
	std::map<wstring, DateSetter>	m_DateSetters;
};

class DlgBusinessUserEditHTML : public BaseDlgBusinessUserEditHTML
{
public:
	DlgBusinessUserEditHTML(vector<BusinessUser>& p_BusinessUsers, boost::YOpt<Organization> p_Org, DlgFormsHTML::Action p_Action, CWnd* p_Parent);
	virtual ~DlgBusinessUserEditHTML();

	virtual void generateJSONScriptData() override;

	//Returns true if processed
	virtual bool processPostedDataSpecific(const IPostedDataTarget::PostedData::value_type& p_Value);
	virtual void postProcessPostedData();

	// Only in case of creation dialog
	bool GetAutoGeneratePassword() const;

protected:
	virtual wstring getDialogTitle() const override;

private:
	bool m_AutoGeneratePassword;

	//wstring m_DefaultDomain;
	//wstring m_PostedPrincipalName;
	//wstring m_PostedDomain;

	boost::YOpt<Organization> m_Org;
};
