#pragma once

class MSGraphSession;
class GraphCache;

class RoleDelegationSessions
{
public:
    RoleDelegationSessions();

    std::shared_ptr<MSGraphSession> GetGraphSessionUser() const;
    std::shared_ptr<MSGraphSession> GetGraphSessionUltraAdmin() const;

    void SetTenant(const wstring& p_Tenant);
    void SetUserSessionCredentials(const wstring& p_Username, const wstring& p_Password);
    void SetUltraAdminSessionCredentials(const wstring& p_ClientId, const wstring& p_ClientSecret);

    void CreateSessions();
    void ClearSessions();

    bool AreSessionsValid() const;
	const wstring& GetSessionError() const;

private:
    std::pair<bool, wstring> CreateUserSession();
    std::pair<bool, wstring> CreateUltraAdminSession();

    std::shared_ptr<MSGraphSession> m_MsGraphSessionUser;
    std::shared_ptr<MSGraphSession> m_MsGraphSessionUltraAdmin;

    wstring m_UserSessionUsername;
    wstring m_UserSessionPassword;

    wstring m_Tenant;
    wstring m_UltraAdminSessionClientId;
    wstring m_UltraAdminSessionClientSecret;

	wstring m_SessionError;
    bool m_AreSessionsValid;
};

