#pragma once

#include "DlgFormsHTML.h"
#include "OnPremiseUser.h"

class DlgOnPremUserEditHTML : public DlgFormsHTML
{
public:
	DlgOnPremUserEditHTML(vector<OnPremiseUser>& p_Users, DlgFormsHTML::Action p_Action, CWnd* p_Parent);

	bool processPostedData(const IPostedDataTarget::PostedData& data) override;

protected:
	using StringGetter = std::function <const boost::YOpt<PooledString>&(const OnPremiseUser&)>;
	using StringSetter = std::function<void(OnPremiseUser&, const boost::YOpt<PooledString>&)>;
	using BoolGetter = std::function<const boost::YOpt<bool>& (const OnPremiseUser&)>;
	using BoolSetter = std::function<void(OnPremiseUser&, const boost::YOpt<bool>&)>;
	using DateGetter = std::function<const boost::YOpt<YTimeDate>& (const OnPremiseUser&)>;
	using DateSetter = std::function<void(OnPremiseUser&, const boost::YOpt<YTimeDate>&)>;
	using VectorStringGetter = std::function<const boost::YOpt<vector<PooledString>>& (const OnPremiseUser&)>;
	using VectorStringSetter = std::function<void(OnPremiseUser&, const boost::YOpt<vector<PooledString>>&)>;

	void addEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, const vector<wstring>& p_ApplicableUserTypes);
	void addEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides = BoolEditor::CheckUncheckedLabelOverrides());
	void addEditor(DateGetter p_Getter, DateSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, bool p_WithTime, const boost::YOpt<wstring>& p_MaxDate = boost::none, const boost::YOpt<std::pair<wstring, wstring>>& p_MinMaxDate = boost::none);
	void addEditor(VectorStringGetter p_Getter, VectorStringSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, const vector<wstring>& p_ApplicableUserTypes);


	void generateJSONScriptData() override;

	bool hasProperty(const wstring& p_PropertyName) const;

protected:
	vector<OnPremiseUser>& m_Users;

	std::map<wstring, StringSetter> m_StringSetters;
	std::map<wstring, BoolSetter>	m_BoolSetters;
	std::map<wstring, DateSetter>	m_DateSetters;
	std::map<wstring, VectorStringSetter>	m_VectorStringsSetters;
};	

