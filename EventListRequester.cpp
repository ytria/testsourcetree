#include "EventListRequester.h"

#include "EventStartCutOffCondition.h"
#include "IPropertySetBuilder.h"
#include "MsGraphHttpRequestLogger.h"
#include "MSGraphUtil.h"
#include "MultiObjectsPageRequestLogger.h"
#include "PaginatorUtil.h"
#include "RESTUtils.h"

EventListRequester::EventListRequester(IPropertySetBuilder& p_Properties, const wstring& p_Id, Context p_Ctx, const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_Id(p_Id)
	, m_Ctx(p_Ctx)
	, m_Properties(p_Properties)
	, m_Logger(p_Logger)
{
}

void EventListRequester::SetCutOffDateTime(const boost::YOpt<wstring>& p_CutOff)
{
	m_CutOffDateTime = p_CutOff;
}

void EventListRequester::SetCustomFilter(const ODataFilter& p_CustomFilter)
{
	m_CustomFilter = p_CustomFilter;
}

TaskWrapper<void> EventListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<DeserializerType>();

	web::uri_builder uri;
	std::shared_ptr<MsGraphHttpRequestLogger> httpLogger;
	if (m_Ctx == Context::Users)
	{
		uri.append_path(_YTEXT("users"));
		httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	}
	else if (m_Ctx == Context::Groups)
	{
		uri.append_path(_YTEXT("groups"));
		httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	}
	uri.append_path(m_Id);
	uri.append_path(_YTEXT("events"));
	uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(m_Properties.GetStringPropertySet()));

	Rest::OrderBy(uri, _YTEXT("start/dateTime"));

	if (m_CutOffDateTime || !m_CustomFilter.IsEmpty())
	{
		ODataFilter oDataFilter;
		if (m_CutOffDateTime)
		{
			oDataFilter.GreaterThan(_YTEXT("start/dateTime"), *m_CutOffDateTime, true);
			if (!m_CustomFilter.IsEmpty())
				oDataFilter.And(m_CustomFilter);
		}
		else if (!m_CustomFilter.IsEmpty())
			oDataFilter = m_CustomFilter;
		oDataFilter.ApplyTo(uri);
	}
	
	m_Paginator.emplace(uri.to_uri(), Util::CreateDefaultGraphPageRequester(m_Deserializer, httpLogger, p_TaskData.GetOriginator()), m_Logger, 100);

	return m_Paginator->Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

vector<BusinessEvent>& EventListRequester::GetData()
{
	return m_Deserializer->GetData();
}
