#pragma once

#include "IRequester.h"

#include "SingleRequestResult.h"
#include "PasswordCredentialDeserializer.h"

class AddApplicationPasswordRequester : public IRequester
{
public:
	AddApplicationPasswordRequester(const wstring& p_PwdDisplayName, const wstring& p_Id);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const SingleRequestResult& GetResult() const;
	const PasswordCredential& GetData() const;

private:
	wstring m_Id;
	wstring m_PwdDisplayName;

	std::shared_ptr<SingleRequestResult> m_Result;
	std::shared_ptr<PasswordCredentialDeserializer> m_Deserializer;
};

