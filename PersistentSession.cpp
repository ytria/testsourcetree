#include "PersistentSession.h"

#include "FileUtil.h"
#include "MainFrameSapio365Session.h"
#include "MFCUtil.h"
#include "MSGraphSession.h"
#include "Sapio365Session.h"
#include "Office365Admin.h"
#include "Resource.h"
#include "SessionTypes.h"
#include "TimeUtil.h"

#include "LoggerService.h"
#include "YtriaTranslate.h"
#include "RoleDelegationManager.h"
#include "Sapio365SessionSavedInfo_DEPRECATED.h"

std::set<wstring> PersistentSession::g_OpenedSessions;

wstring PersistentSession::g_ActiveStatus;
wstring PersistentSession::g_LockedStatus;
wstring PersistentSession::g_StandbyStatus;
wstring PersistentSession::g_CachedStatus;
wstring PersistentSession::g_InactiveStatus;
wstring PersistentSession::g_InvalidStatus;

const wstring PersistentSession::g_Roles = _YTEXT("roles");
const wstring PersistentSession::g_RoleID = _YTEXT("id");
const wstring PersistentSession::g_RoleName = _YTEXT("name");
const wstring PersistentSession::g_RoleLastUsedOn = _YTEXT("lastUsedOn");
const wstring PersistentSession::g_RoleCreatedOn = _YTEXT("createdOn");
const wstring PersistentSession::g_RoleIsFavorite = _YTEXT("isFavorite");
const wstring PersistentSession::g_RoleAccessCount = _YTEXT("accessCount");
const wstring PersistentSession::g_RoleShowOwnScopeOnly = _YTEXT("showOwnScopeOnly");

const PersistentSession PersistentSession::g_EmptySession = PersistentSession::initEmptySession();

PersistentSession::PersistentSession()
	: PersistentSession(g_EmptySession)
{
}

PersistentSession::PersistentSession(const wstring& p_EmailAddress, const wstring& p_FullName, const vector<uint8_t>& p_ProfilePhoto)
	: m_DisplayName(p_EmailAddress)
	, m_EmailOrAppId(p_EmailAddress)
	, m_Fullname(p_FullName)
	, m_AccessCount(1)
	, m_LastUsedOn(YTimeDate::GetCurrentTimeDate())
	, m_CreatedOn(YTimeDate::GetCurrentTimeDate())
	, m_ProfilePhoto(p_ProfilePhoto)
	, m_IsFavorite(false)
	, m_RoleId(0)
	, m_CredentialsId(0)
	, m_UseDeltaUsers(true)
	, m_UseDeltaGroups(true)
	, m_AutoLoadOnPrem(false)
	, m_AskedAutoLoadOnPrem(false)
	, m_UseOnPrem(false)
	, m_DontAskAgainUseOnPrem(false)
{
}

bool PersistentSession::operator==(const PersistentSession& p_Other) const
{
	return GetEmailOrAppId() == p_Other.GetEmailOrAppId() &&
		GetFullname() == p_Other.GetFullname() &&
		GetSessionName() == GetSessionName() &&
		GetTenantDisplayName() == p_Other.GetTenantDisplayName() &&
		GetSessionType() == p_Other.GetSessionType() &&
		GetRoleId() == p_Other.GetRoleId();
}

SessionStatus PersistentSession::GetStatus(int64_t p_RoleId) const
{
	SessionStatus status = SessionStatus::INVALID;

	if (GetEmailOrAppId().empty())
		return status;

	auto sapioSession = Sapio365Session::Find(GetIdentifier());
	if (sapioSession && sapioSession->IsConnected())
	{
		status = SessionStatus::ACTIVE;
		sapioSession.reset();
	}
	else
	{
		if ((IsUltraAdmin() && !m_AppClientSecret.empty() && !m_AccessToken.empty()) ||
			(!m_AccessToken.empty() && !m_RefreshToken.empty()))
		{
			if (IsInactive())
				status = SessionStatus::INACTIVE;
			else
				status = SessionStatus::CACHED;
		}
		else
		{
			status = SessionStatus::LOCKED;
		}
	}

	return status;
}

SessionStatus PersistentSession::GetStatus(const wstring& p_StatusString)
{
	initStatusStrings();

	if (p_StatusString == g_ActiveStatus)
		return SessionStatus::ACTIVE;

	if (p_StatusString == g_CachedStatus)
		return SessionStatus::CACHED;

	if (p_StatusString == g_InactiveStatus)
		return SessionStatus::INACTIVE;

	if (p_StatusString == g_LockedStatus)
		return SessionStatus::LOCKED;

	ASSERT(false);
	return SessionStatus::INVALID;
}

const wstring& PersistentSession::GetStatusString(SessionStatus p_Status)
{
	initStatusStrings();

	switch (p_Status)
	{
	case SessionStatus::INVALID:
		// ASSERT(false); // if this status deserves an ASSERT, please check if PersistentSession::GetStatus may return it - else a status or session history cleanup is missing (DB?)
		return g_InvalidStatus;
	case SessionStatus::ACTIVE:
		return g_ActiveStatus;
	case SessionStatus::CACHED:
		return g_CachedStatus;
	case SessionStatus::INACTIVE:
		return g_InactiveStatus;
	case SessionStatus::LOCKED:
		return g_LockedStatus;
	default:
		break;
	}

	ASSERT(false);
	return g_InvalidStatus;
}

bool PersistentSession::GetRoleShowOwnScopeOnly() const
{
	return m_RoleShowOwnScopeOnly;
}

void PersistentSession::SetRoleShowOwnScopeOnly(bool p_Val)
{
	m_RoleShowOwnScopeOnly = p_Val;
}

bool PersistentSession::IsInactive() const
{
	bool isInactive = false;

	const int64_t TWO_WEEKS = 60 * 60 * 24 * 14;

	const YTimeDate now = YTimeDate::GetCurrentTimeDate();
	if (now.DifferenceInSeconds(GetLastUsedOn()) > TWO_WEEKS)
		isInactive = true;

	return isInactive;
}

bool PersistentSession::IsValid() const
{
	// return !(*this == g_EmptySession);
	return m_Id != g_EmptySession.m_Id
		|| m_DisplayName != g_EmptySession.m_DisplayName
		|| m_EmailOrAppId != g_EmptySession.m_EmailOrAppId
		|| m_Fullname != g_EmptySession.m_Fullname
		|| m_TenantName != g_EmptySession.m_TenantName
		|| m_TenantDomain != g_EmptySession.m_TenantDomain
		|| m_AccessCount != g_EmptySession.m_AccessCount
		|| m_LastUsedOn != g_EmptySession.m_LastUsedOn
		|| m_CreatedOn != g_EmptySession.m_CreatedOn
		|| m_SessionType != g_EmptySession.m_SessionType;
}

wstring PersistentSession::GetConnectedUserId() const
{
	return m_Id;
}

void PersistentSession::SetConnectedUserId(const wstring& p_Id)
{
	m_Id = p_Id;
}

const wstring& PersistentSession::GetEmailOrAppId() const
{
	return m_EmailOrAppId;
}

const wstring& PersistentSession::GetSessionName() const
{
	return m_DisplayName;
}

const wstring& PersistentSession::GetTenantDisplayName() const
{
	return m_TenantName;
}

const wstring& PersistentSession::GetTenantName() const
{
	return m_TenantDomain;
}

const wstring& PersistentSession::GetFullname() const
{
	return m_Fullname;
}

int32_t PersistentSession::GetAccessCount() const
{
	return m_AccessCount;
}

const YTimeDate& PersistentSession::GetLastUsedOn() const
{
	return m_LastUsedOn;
}

const YTimeDate& PersistentSession::GetCreatedOn() const
{
	return m_CreatedOn;
}

const vector<uint8_t>& PersistentSession::GetProfilePhoto() const
{
	return m_ProfilePhoto;
}

const wstring& PersistentSession::GetSessionType() const
{
	return m_SessionType;
}

bool PersistentSession::IsUltraAdmin() const
{
	return m_SessionType == SessionTypes::g_UltraAdmin;
}

bool PersistentSession::IsAdvanced() const
{
	return m_SessionType == SessionTypes::g_AdvancedSession;
}

bool PersistentSession::IsElevated() const
{
	return m_SessionType == SessionTypes::g_ElevatedSession;
}

bool PersistentSession::IsStandard() const
{
	return m_SessionType == SessionTypes::g_StandardSession;
}

bool PersistentSession::IsRole() const
{
	return m_SessionType == SessionTypes::g_Role;
}

bool PersistentSession::IsPartnerAdvanced() const
{
	return m_SessionType == SessionTypes::g_PartnerAdvancedSession;
}

bool PersistentSession::IsPartnerElevated() const
{
	return m_SessionType == SessionTypes::g_PartnerElevatedSession;
}

bool PersistentSession::IsFavorite() const
{
	return m_IsFavorite;
}

int PersistentSession::GetSessionTypeResourcePng() const
{
	int image = 0;
	if (IsStandard())
		image = IDB_ICON_STANDARD_SESSION;
	else if (IsAdvanced())
		image = IDB_ICON_ADVANCED_SESSION;
	else if (IsUltraAdmin())
		image = IDB_ICON_ULTRAADMIN_SESSION;
	else if (IsRole())
		image = IDB_ICON_ROLE_SESSION;
	else if (IsElevated())
		image = IDB_ICON_ELEVATED_SESSION;
	else if (IsPartnerAdvanced())
		image = IDB_ICON_PARTNER_SESSION;
	else if (IsPartnerElevated())
		image = IDB_ICON_PARTNER_ELEVATED_SESSION;

	ASSERT(image != 0);
	return image;
}

SessionIdentifier PersistentSession::GetIdentifier() const
{
	return SessionIdentifier(m_EmailOrAppId, m_SessionType, m_RoleId);
}

bool PersistentSession::UseDeltaUsers() const
{
	return m_UseDeltaUsers;
}

bool PersistentSession::UseDeltaGroups() const
{
	return m_UseDeltaGroups;
}

bool PersistentSession::GetAutoLoadOnPrem() const
{
	return m_AutoLoadOnPrem;
}

bool PersistentSession::GetAskedAutoLoadOnPrem() const
{
	return m_AskedAutoLoadOnPrem;
}

bool PersistentSession::GetUseOnPrem() const
{
	return m_UseOnPrem;
}

bool PersistentSession::GetDontAskAgainUseOnPrem() const
{
	return m_DontAskAgainUseOnPrem;
}

const wstring& PersistentSession::GetRefreshToken() const
{
	return m_RefreshToken;
}

void PersistentSession::SetRefreshToken(const wstring& p_Val)
{
	m_RefreshToken = p_Val;
}

void PersistentSession::SetTechnicalName(const wstring& p_TechnicalSessionName)
{
	m_EmailOrAppId = p_TechnicalSessionName;
}

void PersistentSession::SetSessionName(const wstring& val)
{
	m_DisplayName = val;
}

void PersistentSession::SetFullname(const wstring& val)
{
	m_Fullname = val;
}

void PersistentSession::SetAccessCount(int32_t val)
{
	m_AccessCount = val;
}

void PersistentSession::SetLastUsedOn(const YTimeDate& val)
{
	m_LastUsedOn = val;
}

void PersistentSession::SetCreatedOn(const YTimeDate& val)
{
	m_CreatedOn = val;
}

void PersistentSession::SetProfilePhoto(const vector<uint8_t>& val)
{
	m_ProfilePhoto = val;
}

void PersistentSession::CheckUpdateTo1dot4()
{
	Sapio365SessionSavedInfo_DEPRECATED::CheckUpdateTo1dot4();
}

wstring PersistentSession::GetAADComputerName() const
{
	return m_AADComputerName;
}

void PersistentSession::SetAADComputerName(const wstring& val)
{
	m_AADComputerName = val;
}

bool PersistentSession::GetUseRemoteRSAT() const
{
	return m_UseRemoteRSAT;
}

void PersistentSession::SetUseRemoteRSAT(bool val)
{
	m_UseRemoteRSAT = val;
}

bool PersistentSession::GetUseMsDsConsistencyGuid() const
{
	return m_UseMsDsConsistencyGuid;
}

void PersistentSession::SetUseMsDsConsistencyGuid(bool val)
{
	m_UseMsDsConsistencyGuid = val;
}

wstring PersistentSession::GetADDSServer() const
{
	return m_ADDSServer;
}

void PersistentSession::SetADDSServer(const wstring& val)
{
	m_ADDSServer = val;
}

wstring PersistentSession::GetADDSUsername() const
{
	return m_ADDSUsername;
}

void PersistentSession::SetADDSUsername(const wstring& val)
{
	m_ADDSUsername = val;
}

wstring PersistentSession::GetADDSPassword() const
{
	return m_ADDSPassword;
}

void PersistentSession::SetADDSPassword(const wstring& val)
{
	m_ADDSPassword = val;
}

void PersistentSession::SetRoleNameInDb(const wstring& p_Val)
{
	m_RoleNameInDb = p_Val;
}

const wstring& PersistentSession::GetAppClientSecret() const
{
	return m_AppClientSecret;
}

void PersistentSession::SetAppClientSecret(const wstring& p_Val)
{
	m_AppClientSecret = p_Val;
}

const wstring& PersistentSession::GetAppId() const
{
	return m_AppId;
}

void PersistentSession::SetAppId(const wstring& p_Val)
{
	m_AppId = p_Val;
}

void PersistentSession::SetCredentialsId(const int64_t& p_Val)
{
	m_CredentialsId = p_Val;
}

int64_t PersistentSession::GetCredentialsId() const
{
	return m_CredentialsId;
}

const wstring& PersistentSession::GetAccessToken() const
{
	return m_AccessToken;
}

void PersistentSession::SetAccessToken(const wstring& p_Val)
{
	m_AccessToken = p_Val;
}

int64_t PersistentSession::GetRoleId() const
{
	return m_RoleId;
}

void PersistentSession::SetRoleId(int64_t p_Val)
{
	m_RoleId = p_Val;
}

wstring PersistentSession::GetRoleName() const
{
	wstring roleName;

	if (GetRoleId() != 0)
	{
		ASSERT(RoleDelegationManager::GetInstance().IsReadyToRun());
		if (RoleDelegationManager::GetInstance().IsReadyToRun())
			roleName = RoleDelegationManager::GetInstance().GetDelegation(m_RoleId, MainFrameSapio365Session()).m_Name;
		ASSERT(!roleName.empty());
	}

	return roleName;
}

wstring PersistentSession::GetRoleNameInDb() const
{
	return m_RoleNameInDb;
}

void PersistentSession::SetTenant(const Organization& p_Organization)
{
	m_TenantDomain = p_Organization.GetInitialDomain();
	m_TenantName = p_Organization.DisplayName ? *p_Organization.DisplayName : _YTEXT("");
}

void PersistentSession::SetTenantDisplayName(const wstring& p_TenantDisplayName)
{
	m_TenantName = p_TenantDisplayName;
}

void PersistentSession::SetTenantName(const wstring& p_TenantName)
{
	m_TenantDomain = p_TenantName;
}

void PersistentSession::SetSessionType(const wstring& p_SessionType)
{
	m_SessionType = p_SessionType;
}

void PersistentSession::SetFavorite(bool p_Favorite)
{
	m_IsFavorite = p_Favorite;
}

void PersistentSession::SetUseDeltaUsers(bool p_UseDeltaUsers)
{
	m_UseDeltaUsers = p_UseDeltaUsers;
}

void PersistentSession::SetUseDeltaGroups(bool p_UseDeltaGroups)
{
	m_UseDeltaGroups = p_UseDeltaGroups;
}

void PersistentSession::SetAutoLoadOnPrem(bool p_Value)
{
	m_AutoLoadOnPrem = p_Value;
}

void PersistentSession::SetAskedAutoLoadOnPrem(bool p_Value)
{
	m_AskedAutoLoadOnPrem = p_Value;
}

void PersistentSession::SetUseOnPrem(bool p_Value)
{
	m_UseOnPrem = p_Value;
}

void PersistentSession::SetDontAskAgainUseOnPrem(bool p_Value)
{
	m_DontAskAgainUseOnPrem = p_Value;
}

void PersistentSession::initStatusStrings()
{
	static bool init = false;

	if (!init)
	{
		g_InvalidStatus = YtriaTranslate::Do(O365Session_initStatusStrings_1, _YLOC("Error")).c_str();
		g_ActiveStatus = YtriaTranslate::Do(O365Session_initStatusStrings_2, _YLOC("Active")).c_str();
		g_LockedStatus = YtriaTranslate::Do(O365Session_initStatusStrings_3, _YLOC("Locked")).c_str();
		g_StandbyStatus = YtriaTranslate::Do(O365Session_initStatusStrings_4, _YLOC("Standby")).c_str();
		g_CachedStatus = YtriaTranslate::Do(O365Session_initStatusStrings_5, _YLOC("Cached")).c_str();
		g_InactiveStatus = YtriaTranslate::Do(O365Session_initStatusStrings_6, _YLOC("Inactive")).c_str();
	}

	init = true;
}

PersistentSession PersistentSession::initEmptySession()
{
	return PersistentSession(_YTEXT(""), _YTEXT(""), {});
}
