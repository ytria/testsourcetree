#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "PhysicalAddress.h"

class PhysicalAddressDeserializer : public JsonObjectDeserializer, public Encapsulate<PhysicalAddress>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

