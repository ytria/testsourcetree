#pragma once

#include "CreatedObjectModification.h"
#include "DeletedObjectModification.h"
#include "RestoreObjectModification.h"
#include "FieldUpdate.h"
#include "HttpResultWithError.h"
#include "ISubItemDeletion.h"
#include "IModificationWithRequestFeedbackProvider.h"
#include "ModificationWithRequestFeedback.h"
#include "RowFieldUpdates.h"
#include "RowModification.h"
#include "RowSubItemFieldUpdates.h"
#include "Scope.h"
#include "SubItemFieldUpdate.h"

#include <memory>
#include <deque>

class GridUpdater;
class O365Grid;
class UserLicenseModification;

template <Scope scope>
class GridFieldModifications
{
public:
	GridFieldModifications(O365Grid& p_Grid);
	~GridFieldModifications();

	// Ark
	static constexpr Scope GetScope() { return scope;	}

	void Add(std::unique_ptr<FieldUpdate<scope>> p_FieldModification);

	using RowFieldModifications = std::deque<std::unique_ptr<RowFieldUpdates<scope>>>;
	const RowFieldModifications& GetRowFieldModifications() const;

	virtual void RefreshStates(GridUpdater& p_Updater);
	virtual void RefreshStatesPostProcess(GridUpdater& p_Updater);

	virtual void RemoveFinalStateModifications();
	virtual void RemoveErrorModifications();
	virtual void RemoveSentAndReceivedModifications();
	void UpdateShownValues();

	virtual bool HasModifications() const;
	virtual bool IsModified(GridBackendRow* p_Row) const; // Returns true if any modification concerns this row.
	// This doesn't handle subitems mods.
	virtual bool HasAnyRowModificationOnOrUnder(GridBackendRow* p_TopAncestorRow) const; // Returns true if any modified row has (or is) the given top ancestor.

	virtual bool RevertAll();
	virtual bool RevertAllRowModifications();
	bool Revert(const row_pk_t& rowKey, bool p_RevertOnDestruction = true);
	bool RevertSelectedRows(bool p_RevertOnDestruction = true);
	bool RevertFieldUpdates(const row_pk_t& rowKey, const std::vector<UINT>& p_ColIDs);

	virtual bool RemoveRowModifications(const row_pk_t& rowKey, bool p_RevertOnDestruction = false); // Unlike Revert, it just erases the modifications without reverting the display.
	virtual void RemoveFinalStateModificationsForParentRows(const boost::YOpt<set<GridBackendRow*>>& p_AncestorRows);

	RowFieldUpdates<scope>* GetRowFieldModification(const row_pk_t& rowKey);

	// if p_IncludeSubItems, this also checks sub items mods (not efficient, can be slow testing every single grid row)
	virtual set<GridBackendRow*> GetModifiedRows(bool p_OnlyAppliedLocally, bool p_IncludeExplosionSiblings = true) const;

	const O365Grid& GetGrid() const;

protected:
	O365Grid& m_Grid;

	RowFieldModifications	m_RowFieldModifications;
};

class GridModificationsO365 : public GridFieldModifications<Scope::O365>
{
public:
	using GridFieldModifications<Scope::O365>::GridFieldModifications;
	~GridModificationsO365();

	using GridFieldModifications<Scope::O365>::Add;
    bool Add(std::unique_ptr<ISubItemDeletion> p_Deletion);
	CreatedObjectModification* Add(std::unique_ptr<CreatedObjectModification> p_RowModification);
	DeletedObjectModification* Add(std::unique_ptr<DeletedObjectModification> p_RowModification);
	void Add(std::unique_ptr<RestoreObjectModification> p_RowModification);
    void Add(std::unique_ptr<UserLicenseModification> p_RowModification);
    void Add(std::unique_ptr<SubItemFieldUpdate> p_FieldUpdate, GridBackendRow* p_Row);

	using RowModifications		= std::deque<std::unique_ptr<RowModification>>;
	using SubItemDeletions		= std::deque<std::unique_ptr<ISubItemDeletion>>;
	using SubItemUpdates		= std::deque<std::unique_ptr<RowSubItemFieldUpdates>>;

	const RowModifications&					GetRowModifications() const;
	vector<const Modification*>				GetModsRequestFeedback() const;
	const SubItemDeletions&					GetSubItemDeletions() const;
	const SubItemUpdates&					GetSubItemUpdates() const;

	void ShowSubItemModificationAppliedLocally(GridBackendRow* p_Row);
    
    void RefreshSubItemModificationsStates(GridUpdater& p_Updater);

    bool HasSubItemDeletion(const SubItemKey& p_Key, bool p_OnlyAppliedLocally) const;
    bool HasSubItemUpdate(const SubItemKey& p_Key, bool p_OnlyAppliedLocally) const; 
	bool HasSubItemModifications() const;
	bool HasSubItemResults() const;

	bool RevertRowModifications(const row_pk_t& rowKey);
	bool RemoveRowModifications(const row_pk_t& rowKey, bool p_RevertOnDestruction = false); // Unlike Revert, it just erases the modifications without reverting the display.

	bool Revert(const row_pk_t& rowKey, bool alsoRevertSubItems);
	bool RevertAllSubItemModifications();
	bool RevertSubItem(const row_pk_t& rowKey);
	void RevertSubItemDeletion(const SubItemKey& p_Key);
	void RevertSubItemUpdate(const SubItemKey& p_Key);

	bool RemoveSubItem(const row_pk_t& rowKey); // Unlike Revert, it just erases the modifications without reverting the display.
    void RemoveSubItemDeletionErrors();
    void RemoveSubItemUpdateErrors();

	void SetSubItemDeletionsErrors(const std::map<SubItemKey, HttpResultWithError>& p_Errors);
	void SetSubItemUpdateErrors(const std::map<SubItemKey, HttpResultWithError>& p_Errors);
	
	bool IsModInSelectedRows(ISubItemModification* p_Mod);

	template <class T>
	T*							GetRowModification(const row_pk_t& rowKey);
    vector<RowModification*>	GetRowModifications(std::function<bool(RowModification*)> p_Criteria);

	// Only returns CreatedObjectModification rows
	set<GridBackendRow*> GetCreatedRows(bool p_OnlyAppliedLocally) const;

	void SetModsWithRequestFeedbackProvider(std::unique_ptr<IModificationWithRequestFeedbackProvider> p_Provider);

public: // GridFieldModifications<Scope::O365>
	void RefreshStates(GridUpdater& p_Updater) override;
	void RefreshStatesPostProcess(GridUpdater& p_Updater) override;

	void RemoveFinalStateModifications() override;
	void RemoveErrorModifications() override;
	void RemoveSentAndReceivedModifications() override;

	bool HasModifications() const override;
	bool IsModified(GridBackendRow* p_Row) const override; // Returns true if any modification concerns this row.
	// This doesn't handle subitems mods.
	bool HasAnyRowModificationOnOrUnder(GridBackendRow* p_TopAncestorRow) const override; // Returns true if any modified row has (or is) the given top ancestor.

	bool RevertAll() override;
	bool RevertAllRowModifications() override;

	void RemoveFinalStateModificationsForParentRows(const boost::YOpt<set<GridBackendRow*>>& p_AncestorRows) override;

	// if p_IncludeSubItems, this also checks sub items mods (not efficient, can be slow testing every single grid row)
	set<GridBackendRow*> GetModifiedRows(bool p_OnlyAppliedLocally, bool p_IncludeSubItems) const override;

private:
	RowModification* add(std::unique_ptr<RowModification> p_RowModification);

	RowModifications		m_RowModifications;
	SubItemDeletions		m_SubItemDeletions;
	SubItemUpdates			m_SubItemUpdates;
	std::unique_ptr<IModificationWithRequestFeedbackProvider> m_ModsRequestFeedbackProvider;

    std::map<SubItemKey, HttpResultWithError> m_SubItemDeletionErrors;
    std::map<SubItemKey, HttpResultWithError> m_SubItemUpdateErrors;
};

template <class T>
T* GridModificationsO365::GetRowModification(const row_pk_t& rowKey)
{
	auto it = std::find_if(m_RowModifications.begin(), m_RowModifications.end(), [=](const std::unique_ptr<RowModification>& item)
		{
			return item->IsCorrespondingRow(rowKey) && nullptr != dynamic_cast<T*>(item.get());
		});

	if (m_RowModifications.end() != it)
		return dynamic_cast<T*>(it->get());
	return nullptr;
}

using GridFieldModificationsOnPrem = GridFieldModifications<Scope::ONPREM>;
