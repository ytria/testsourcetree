#include "BusinessMessage.h"
#include "MsGraphFieldNames.h"
#include "MSGraphSession.h"
#include "O365AdminUtil.h"
#include "Sapio365Session.h"
#include "TimeUtil.h"
#include "safeTaskCall.h"
#include "MsGraphHttpRequestLogger.h"

using namespace Util;

RTTR_REGISTRATION
{
    using namespace rttr;
	registration::class_<BusinessMessage>("Message") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Message"))))
		.constructor()(policy::ctor::as_object)

// **** Recipient
		.property(O365_MESSAGE_SENDER, &BusinessMessage::m_SenderUnused) // TODO: Unused
		.property(O365_MESSAGE_SENDERADDRESS, &BusinessMessage::m_SenderAddress)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_MESSAGE_SENDERNAME, &BusinessMessage::m_SenderName)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_MESSAGE_FROM, &BusinessMessage::m_FromUnused)
		.property(O365_MESSAGE_FROMADDRESS, &BusinessMessage::m_FromAddress)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_MESSAGE_FROMNAME, &BusinessMessage::m_FromName)(
			metadata(MetadataKeys::ValidForCreation, true)
			)

		//// ItemBody
		//// TODO: This field is to big for a grid cell.
		//// Property = O365_MESSAGE_BODY in Message
		//.property(O365_MESSAGE_BODYCONTENT, &BusinessMessage::m_BodyContent)
		//.property(O365_MESSAGE_BODYCONTENTTYPE, &BusinessMessage::m_BodyContentType)

		.property(O365_MESSAGE_UNIQUEBODY, &BusinessMessage::m_UniqueUnused)
		.property(O365_MESSAGE_UNIQUEBODYCONTENT, &BusinessMessage::m_UniqueBodyContent)
		.property(O365_MESSAGE_UNIQUEBODYCONTENTTYPE, &BusinessMessage::m_UniqueBodyContentType)

// **** PooledString
		.property(O365_MESSAGE_SUBJECT, &BusinessMessage::m_Subject)
		.property(O365_MESSAGE_BODYPREVIEW, &BusinessMessage::m_BodyPreview)
		.property(O365_MESSAGE_CHANGEKEY, &BusinessMessage::m_ChangeKey)
		.property(O365_MESSAGE_CONVERSATIONID, &BusinessMessage::m_ConversationId)
		.property(O365_MESSAGE_CREATEDDATETIME, &BusinessMessage::m_CreatedDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_MESSAGE_IMPORTANCE, &BusinessMessage::m_Importance)
		.property(O365_MESSAGE_INFERENCECLASSIFICATION, &BusinessMessage::m_InferenceClassification)
		.property(O365_MESSAGE_INTERNETMESSAGEID, &BusinessMessage::m_InternetMessageId)
		.property(O365_MESSAGE_LASTMODIFIEDDATETIME, &BusinessMessage::m_LastModifiedDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_MESSAGE_PARENTFOLDERID, &BusinessMessage::m_ParentFolderId)
		.property(O365_MESSAGE_RECEIVEDDATETIME, &BusinessMessage::m_ReceivedDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_MESSAGE_SENTDATETIME, &BusinessMessage::m_SentDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_MESSAGE_WEBLINK, &BusinessMessage::m_WebLink)

// **** bool
		.property(O365_MESSAGE_HASATTACHMENTS, &BusinessMessage::m_HasAttachments)
		.property(O365_MESSAGE_ISDELIVERYRECEIPTREQUESTED, &BusinessMessage::m_IsDeliveryReceiptRequested)
		.property(O365_MESSAGE_ISDRAFT, &BusinessMessage::m_IsDraft)
		.property(O365_MESSAGE_ISREAD, &BusinessMessage::m_IsRead)
		.property(O365_MESSAGE_ISREADRECEIPTREQUESTED, &BusinessMessage::m_IsReadReceiptRequested)

// **** vector<PooledString>
		.property(O365_MESSAGE_CATEGORIES, &BusinessMessage::m_Categories)

// **** TODO:
		//.property("BCC Recipients Address", &BusinessMessage::m_BccRecipientsAddress)
		//.property("BCC Recipients Name", &BusinessMessage::m_BccRecipientsName)
		//.property("CC Recipients Address", &BusinessMessage::m_CcRecipientsAddress)
		//.property("CC Recipients Name", &BusinessMessage::m_CcRecipientsName)
		//.property("Reply To Address", &BusinessMessage::m_ReplyToAddress)
		//.property("Reply To Name", &BusinessMessage::m_ReplyToName)
		//.property("To Recipients Address", &BusinessMessage::m_ToRecipientsAddress)
		//.property("To Recipients Name", &BusinessMessage::m_ToRecipientsName)
		;
}

BusinessMessage::BusinessMessage(const Message & p_Message)
{
    SetID(p_Message.m_Id);

    if (boost::none != p_Message.Categories)
    {
        for (const auto& Cat : p_Message.Categories.get())
            m_Categories.push_back(Cat);
    }

    m_BccRecipients = p_Message.BccRecipients;
    m_CcRecipients = p_Message.CcRecipients;
    m_ReplyTo = p_Message.ReplyTo;
    m_ToRecipients = p_Message.ToRecipients;
	//     for (const auto& bcc : p_Message.BccRecipients)
	//     {
	//         if (boost::none != bcc.m_EmailAddress)
	//         {
	//             if (boost::none != bcc.m_EmailAddress.get().m_Address)
	//                 m_BccRecipientsAddress.push_back(bcc.m_EmailAddress.get().m_Address.get());
	// 
	//             if (boost::none != bcc.m_EmailAddress.get().m_Name)
	//                 m_BccRecipientsName.push_back(bcc.m_EmailAddress.get().m_Name.get());
	//         }
	//     }
	// 
	//     for (const auto& cc : p_Message.CcRecipients)
	//     {
	//         if (boost::none != cc.m_EmailAddress)
	//         {
	//             if (boost::none != cc.m_EmailAddress.get().m_Address)
	//                 m_CcRecipientsAddress.push_back(cc.m_EmailAddress.get().m_Address.get());
	// 
	//             if (boost::none != cc.m_EmailAddress.get().m_Name)
	//                 m_CcRecipientsName.push_back(cc.m_EmailAddress.get().m_Name.get());
	//         }
	//     }
	// 
	//     for (const auto& repto : p_Message.ReplyTo)
	//     {
	//         if (boost::none != repto.m_EmailAddress)
	//         {
	//             if (boost::none != repto.m_EmailAddress.get().m_Address)
	//                 m_ReplyToAddress.push_back(repto.m_EmailAddress.get().m_Address.get());
	// 
	//             if (boost::none != repto.m_EmailAddress.get().m_Name)
	//                 m_ReplyToName.push_back(repto.m_EmailAddress.get().m_Name.get());
	//         }
	//     }
	// 
	//     for (const auto& torec : p_Message.ToRecipients)
	//     {
	//         if (boost::none != torec.m_EmailAddress)
	//         {
	//             if (boost::none != torec.m_EmailAddress.get().m_Address)
	//                 m_ToRecipientsAddress.push_back(torec.m_EmailAddress.get().m_Address.get());
	// 
	//             if (boost::none != torec.m_EmailAddress.get().m_Name)
	//                 m_ToRecipientsName.push_back(torec.m_EmailAddress.get().m_Name.get());
	//         }
	//     }

	m_InternetMessageHeaders = p_Message.InternetMessageHeaders;

    if (boost::none != p_Message.Sender && boost::none != p_Message.Sender->m_EmailAddress)
    {
        if (boost::none != p_Message.Sender->m_EmailAddress->m_Address)
            m_SenderAddress = *p_Message.Sender->m_EmailAddress->m_Address;

        if (boost::none != p_Message.Sender->m_EmailAddress->m_Name)
            m_SenderName = *p_Message.Sender->m_EmailAddress->m_Name;
    }

    if (boost::none != p_Message.Body)
    {
        if (boost::none != p_Message.Body->Content)
            m_BodyContent = *p_Message.Body->Content;

        if (boost::none != p_Message.Body->ContentType)
            m_BodyContentType = *p_Message.Body->ContentType;
    }

    if (boost::none != p_Message.BodyPreview)
        m_BodyPreview = *p_Message.BodyPreview;

    if (boost::none != p_Message.ChangeKey)
        m_ChangeKey = *p_Message.ChangeKey;

    if (boost::none != p_Message.ConversationId)
        m_ConversationId = *p_Message.ConversationId;

    if (boost::none != p_Message.CreatedDateTime)
        m_CreatedDateTime = p_Message.CreatedDateTime;

    if (boost::none != p_Message.From && boost::none != p_Message.From->m_EmailAddress)
    {
        if (boost::none != p_Message.From->m_EmailAddress->m_Address)
            m_FromAddress = *p_Message.From->m_EmailAddress->m_Address;

        if (boost::none != p_Message.From->m_EmailAddress->m_Name)
            m_FromName = *p_Message.From->m_EmailAddress->m_Name;
    }

    if (boost::none != p_Message.HasAttachments)
        m_HasAttachments = *p_Message.HasAttachments;

    if (boost::none != p_Message.Importance)
        m_Importance = *p_Message.Importance;

    if (boost::none != p_Message.InferenceClassification)
        m_InferenceClassification = *p_Message.InferenceClassification;

    if (boost::none != p_Message.InternetMessageId)
        m_InternetMessageId = *p_Message.InternetMessageId;

    if (boost::none != p_Message.IsDeliveryReceiptRequested)
        m_IsDeliveryReceiptRequested = *p_Message.IsDeliveryReceiptRequested;

    if (boost::none != p_Message.IsDraft)
        m_IsDraft = *p_Message.IsDraft;

    if (boost::none != p_Message.IsRead)
        m_IsRead = *p_Message.IsRead;

    if (boost::none != p_Message.IsReadReceiptRequested)
        m_IsReadReceiptRequested = *p_Message.IsReadReceiptRequested;

    if (boost::none != p_Message.LastModifiedDateTime)
        m_LastModifiedDateTime = *p_Message.LastModifiedDateTime;
    
    if (boost::none != p_Message.ParentFolderId)
        m_ParentFolderId = *p_Message.ParentFolderId;

    if (boost::none != p_Message.ReceivedDateTime)
        m_ReceivedDateTime = *p_Message.ReceivedDateTime;

    if (boost::none != p_Message.SentDateTime)
        m_SentDateTime = *p_Message.SentDateTime;

    if (boost::none != p_Message.UniqueBody)
    {
        if (boost::none != p_Message.UniqueBody->Content)
            m_UniqueBodyContent = *p_Message.UniqueBody->Content;

        if (boost::none != p_Message.UniqueBody->ContentType)
            m_UniqueBodyContentType = *p_Message.UniqueBody->ContentType;
    }

    if (boost::none != p_Message.WebLink)
        m_WebLink = *p_Message.WebLink;

    if (boost::none != p_Message.Subject)
        m_Subject = *p_Message.Subject;
}

bool BusinessMessage::ConvertProperty(Message& p_Message, const rttr::property& p_BizProp, vector<rttr::property>& p_Properties) const
{
	bool Result = false;

	// The following do not exist in Message.
	if (O365_MESSAGE_SENDERADDRESS != p_BizProp.get_name() &&
		O365_MESSAGE_SENDERNAME != p_BizProp.get_name() &&
		O365_MESSAGE_FROMADDRESS != p_BizProp.get_name() &&
		O365_MESSAGE_FROMNAME != p_BizProp.get_name())
	{
		if (O365_MESSAGE_SENDER == p_BizProp.get_name())
		{
			// Two properties in BusinessMessage is one in Message.
			// BusinessMessage -> O365_MESSAGE_SENDERADDRESS
			// BusinessMessage -> O365_MESSAGE_SENDERNAME
			// Message -> O365_MESSAGE_SENDER

			rttr::property PropSenderAddress = rttr::type::get(*this).get_property(O365_MESSAGE_SENDERADDRESS);
			rttr::variant var_SenderAddress = PropSenderAddress.get_value(*this);

            boost::YOpt<PooledString> address;
			ASSERT(var_SenderAddress.is_valid());
            if (var_SenderAddress.is_valid())
			    address = var_SenderAddress.get_value<boost::YOpt<PooledString>>();

			rttr::property PropSenderName = rttr::type::get(*this).get_property(O365_MESSAGE_SENDERNAME);
			rttr::variant var_SenderName = PropSenderName.get_value(*this);

            boost::YOpt<PooledString> name;
			ASSERT(var_SenderName.is_valid());
            if (var_SenderName.is_valid())
			    name = var_SenderName.get_value<boost::YOpt<PooledString>>();

			/////////////////////////////////////
			
			Recipient RTempt;
			EmailAddress ETemp;
			ETemp.m_Address = address;
			ETemp.m_Name = name;
			RTempt.m_EmailAddress = ETemp;

			boost::YOpt<Recipient> Toto(RTempt);

			rttr::property PropSender = rttr::type::get(p_Message).get_property(O365_MESSAGE_SENDER);
			PropSender.set_value(p_Message, Toto);
			p_Properties.push_back(PropSender);
		}
		else if (O365_MESSAGE_FROM == p_BizProp.get_name())
		{
			// Two properties in BusinessMessage is one in Message.
			// BusinessMessage -> O365_MESSAGE_FROMADDRESS
			// BusinessMessage -> O365_MESSAGE_FROMNAME
			// Message -> O365_MESSAGE_FROM

			rttr::property PropFromAddress = rttr::type::get(*this).get_property(O365_MESSAGE_FROMADDRESS);
			rttr::variant var_FromAddress = PropFromAddress.get_value(*this);
			ASSERT(var_FromAddress.is_valid());
			auto Address = var_FromAddress.get_value<boost::YOpt<PooledString>>();

			rttr::property PropFromName = rttr::type::get(*this).get_property(O365_MESSAGE_FROMNAME);
			rttr::variant var_FromName = PropFromName.get_value(*this);
			ASSERT(var_FromName.is_valid());
			auto Name = var_FromName.get_value<boost::YOpt<PooledString>>();

			/////////////////////////////////////

			Recipient RTempt;
			EmailAddress ETemp;
			ETemp.m_Address = Address;
			ETemp.m_Name = Name;
			RTempt.m_EmailAddress = ETemp;

			boost::YOpt<Recipient> Toto(RTempt);

			rttr::property PropFrom = rttr::type::get(p_Message).get_property(O365_MESSAGE_FROM);
			PropFrom.set_value(p_Message, Toto);
			p_Properties.push_back(PropFrom);
		}
		else
		{
			// Get the property value from the BusinessUser.
			rttr::variant var_bizprop = p_BizProp.get_value(*this);

			// TODO: More types.
			if (var_bizprop.is_valid() && var_bizprop.get_type() == rttr::type::get<boost::YOpt<PooledString>>())
			{
				auto Temp = var_bizprop.get_value<boost::YOpt<PooledString>>();
				if (boost::none != Temp)
				{
					// Get the User property and set value obtained in the BusinessUser.
					rttr::property PropConvert = rttr::type::get(p_Message).get_property(p_BizProp.get_name());
					PropConvert.set_value(p_Message, Temp);
					p_Properties.push_back(PropConvert);

					// Success.
					Result = true;
				}
			}
		}
	}

	return Result;
}

wstring GetMessageFolderContext(const BusinessMessage& p_Msg)
{
	wstring context;

	if (p_Msg.m_ParentFolderName)
		context = *p_Msg.m_ParentFolderName;
	else if (p_Msg.m_Subject)
		context = *p_Msg.m_Subject;

	return context;
}

Message BusinessMessage::ToMessage(const BusinessMessage & p_BusinessMessage)
{
    Message message;

    message.m_Id = p_BusinessMessage.GetID();

	message.Categories.emplace(p_BusinessMessage.m_Categories);
	message.ToRecipients = p_BusinessMessage.m_ToRecipients;
	message.ToRecipients = p_BusinessMessage.m_ToRecipients;
	message.CcRecipients = p_BusinessMessage.m_CcRecipients;
	message.BccRecipients = p_BusinessMessage.m_BccRecipients;
	message.ReplyTo = p_BusinessMessage.m_ReplyTo;
	message.InternetMessageHeaders = p_BusinessMessage.m_InternetMessageHeaders;

	// ItemBody
	ItemBody BTemp;
	BTemp.Content = p_BusinessMessage.m_BodyContent;
	BTemp.ContentType = p_BusinessMessage.m_BodyContentType;
	message.Body = BTemp;
	BTemp.Content = p_BusinessMessage.m_UniqueBodyContent;
	BTemp.ContentType = p_BusinessMessage.m_UniqueBodyContentType;
	message.UniqueBody = BTemp;

	// Recipient
	Recipient RTempt;
	EmailAddress ETemp;
	ETemp.m_Address = p_BusinessMessage.m_SenderAddress;
	ETemp.m_Name = p_BusinessMessage.m_SenderName;
	RTempt.m_EmailAddress = ETemp;
	message.Sender = RTempt;
	ETemp.m_Address = p_BusinessMessage.m_FromAddress;
	ETemp.m_Name = p_BusinessMessage.m_FromName;
	RTempt.m_EmailAddress = ETemp;
	message.From = RTempt;

	// PooledString
    message.BodyPreview = p_BusinessMessage.m_BodyPreview;
    message.ChangeKey = p_BusinessMessage.m_ChangeKey;
    message.ConversationId = p_BusinessMessage.m_ConversationId;
    message.CreatedDateTime = p_BusinessMessage.m_CreatedDateTime;
    message.Importance = p_BusinessMessage.m_Importance;
    message.InferenceClassification = p_BusinessMessage.m_InferenceClassification;
    message.InternetMessageId = p_BusinessMessage.m_InternetMessageId;
    message.LastModifiedDateTime = p_BusinessMessage.m_LastModifiedDateTime;
    message.ParentFolderId = p_BusinessMessage.m_ParentFolderId;
    message.ReceivedDateTime = p_BusinessMessage.m_ReceivedDateTime;
	message.SentDateTime = p_BusinessMessage.m_SentDateTime;
	message.Subject = p_BusinessMessage.m_Subject;
    message.WebLink = p_BusinessMessage.m_WebLink;

	// bool
    message.HasAttachments = p_BusinessMessage.m_HasAttachments;
    message.IsDeliveryReceiptRequested = p_BusinessMessage.m_IsDeliveryReceiptRequested;
    message.IsDraft = p_BusinessMessage.m_IsDraft;
    message.IsRead = p_BusinessMessage.m_IsRead;
    message.IsReadReceiptRequested = p_BusinessMessage.m_IsDeliveryReceiptRequested;

	message.m_UserId = p_BusinessMessage.m_UserId;

    return message;
}

TaskWrapper<HttpResultWithError> BusinessMessage::SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	auto message = BusinessMessage::ToMessage(*this);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(message.Delete(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}
