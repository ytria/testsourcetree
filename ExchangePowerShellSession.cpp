#include "ExchangePowerShellSession.h"

#include "InvokeResultWrapper.h"
#include "ExchangeAuthenticationCollectionDeserializer.h"
#include "LoggerService.h"
#include "PowerShellDeleter.h"

ExchangePowerShellSession::ExchangePowerShellSession(const wstring& p_DefaultDomain)
	: BasicPowerShellSession(true),
	m_HasLiveSession(false),
	m_DefaultDomain(p_DefaultDomain),
	m_ExchangeTokenExpiration(0)
{
}

ExchangePowerShellSession::~ExchangePowerShellSession()
{
	if (m_HasLiveSession && nullptr != GetPowerShell())
	{
		AddScript(_YTEXT("Get-PSSession|Remove-PSSession"), 0);
		Invoke(0);
	}
}

void ExchangePowerShellSession::AddScript(const wchar_t* p_Script, HWND p_Originator)
{
	if (m_ExchangeTokenExpiration != 0 && (GetExchangeTokenExpiration() - time(0)) <= (5 * 60)) // Expires in 5 minutes or less?
		CreateExchangeToken(p_Originator, true);

	wstring finalScript = _YTEXTFORMAT(LR"(
		[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12;
		%s
	)", p_Script);
	BasicPowerShellSession::AddScript(finalScript.c_str(), p_Originator);
}

void ExchangePowerShellSession::SetHasLiveSession(bool p_HasLiveSession)
{
	m_HasLiveSession = p_HasLiveSession;
}

int64_t ExchangePowerShellSession::GetExchangeTokenExpiration() const
{
	return m_ExchangeTokenExpiration;
}

void ExchangePowerShellSession::SetExchangeTokenExpiration(int64_t p_Expiration)
{
	m_ExchangeTokenExpiration = p_Expiration;
}

std::shared_ptr<InvokeResultWrapper> ExchangePowerShellSession::CreateExchangeToken(HWND p_Originator, bool p_Renew)
{
	if (!p_Renew)
		LoggerService::User(_T("Authenticating..."), p_Originator);
	else
		LoggerService::User(_T("Renewing token..."), p_Originator);

	wstring getMsalToken = _YTEXTFORMAT(LR"(Get-MsalToken -ClientId "a0c73c16-a7e3-4564-9a95-2bdf47383716" -RedirectUri "urn:ietf:wg:oauth:2.0:oob" -Scopes "https://outlook.office365.com/AdminApi.AccessAsUser.All","https://outlook.office365.com/FfoPowerShell.AccessAsUser.All","https://outlook.office365.com/RemotePowerShell.AccessAsUser.All" -TenantId %s -OutVariable token)", m_DefaultDomain.c_str());
	if (p_Renew)
		getMsalToken += _YTEXT(" -ForceRefresh");
	else
		getMsalToken += _YTEXT(" -Interactive"); // Force the authentication window to popup (potentially useful when single-logon is deployed)

	wstring script = _YTEXTFORMAT(LR"(
		$oldActionPreference = $ErrorActionPreference
		$ErrorActionPreference='Stop'
		Get-PSSession|Remove-PSSession
		%s
		$auth=ConvertTo-SecureString -AsPlainText -Force -String $token.CreateAuthorizationHeader()
		$cred=New-Object -Typename System.Management.Automation.PSCredential -Argumentlist $token.account.username,$auth
		$session=New-PSSession -Name ExchangeOnline -ConnectionUri "https://outlook.office365.com/PowerShell-LiveId?BasicAuthToOAuthConversion=true" -ConfigurationName Microsoft.Exchange -Credential $cred[0] -Authentication Basic
		Import-PSSession -Session $session -WarningAction Ignore -AllowClobber
		$ErrorActionPreference=$oldActionPreference
	)", getMsalToken.c_str()).c_str();

	m_BufferedScript += script;
	m_PowerShell->AddScript(script.c_str());

	auto result = std::make_shared<InvokeResultWrapper>(Invoke(p_Originator));

	ASSERT(result->Get().m_Success);
	if (result->Get().m_Success)
	{
		ExchangeAuthenticationCollectionDeserializer deserializer;
		result->Get().m_Collection->Deserialize(deserializer);
		SetExchangeTokenExpiration(deserializer.GetTokenExpiration());
	}

	return result;
}
