#include "LinkHandlerSignIns.h"

#include "CommandDispatcher.h"
#include "CommandInfo.h"
#include "Office365Admin.h"

void LinkHandlerSignIns::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	info.SetOrigin(Origin::Tenant);
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::SignIns, Command::ModuleTask::List, info, { nullptr, g_ActionNameShowSignIns, nullptr, p_Link.GetAutomationAction() }));
}
