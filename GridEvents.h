#pragma once

#include "AttachmentsInfo.h"
#include "BaseO365Grid.h"
#include "BusinessEvent.h"
#include "CommandInfo.h"
#include "MyDataMeRowHandler.h"

class FrameEvents;
class GridTemplateGroups;
class GridTemplateUsers;
class MessageBodyViewer;

class GridEvents : public ModuleO365Grid<BusinessEvent>
{
public:
	GridEvents(Origin p_Origin, bool p_IsMyData = false);
    ~GridEvents();

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_FullPurge, bool p_NoPurge);
	void BuildTreeView(const O365DataMap<BusinessGroup, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_FullPurge, bool p_NoPurge);
	void BuildTreeViewWithAttachments(const O365DataMap<BusinessUser, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_Updates, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge);
	void BuildTreeViewWithAttachments(const O365DataMap<BusinessGroup, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_Updates, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge);
    void ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_RefreshModificationStates, bool p_ShowPostUpdateError, bool p_UpdateGrid);
    void ProcessDownloadedAttachments(const AttachmentsInfo& p_DownloadedAttachments);
	std::set<AttachmentsInfo::IDs> GetAttachmentsRequestInfo(const std::vector<GridBackendRow *>& p_Rows, const O365IdsContainer& p_EventIdsFilter, const bool p_ShowInlineWarningDialog);

    void SetShowInlineAttachments(const bool& p_Val);

	void HideMessageViewer();

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	RoleDelegationUtil::RBAC_Privilege getPermission(const int p_CommandID) const;

	std::vector<SubItemKey> GetDeleteAttachmentsData(bool p_Selected);

	GridBackendColumn* GetColMetaID() const;

	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeDelete() const override;

	bool IsMyData() const override;
    O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

protected:
	virtual void customizeGrid() override;
	virtual void customizeGridPostProcess() override;

	BusinessEvent			getBusinessObject(GridBackendRow*) const override;
	virtual void			UpdateBusinessObjects(const vector<BusinessEvent>& p_Events, bool p_SetModifiedStatus) override;
	virtual void			RemoveBusinessObjects(const vector<BusinessEvent>& p_Events) override;

    virtual void InitializeCommands() override;
    virtual void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart) override;
    virtual void OnGbwSelectionChangedSpecific() override;
    DECLARE_MESSAGE_MAP()

	virtual BOOL PreTranslateMessage(MSG* pMsg) override;

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;
	bool GetSnapshotAdditionalValue(wstring& p_Value, GridBackendRow& p_Row) override;
	bool SetSnapshotAdditionalValue(const wstring& p_Value, GridBackendRow& p_Row) override;

private:
	afx_msg void OnChangeOptions();
    afx_msg void OnShowEventMessageBody();
    afx_msg void OnShowEventAttachments();
    afx_msg void OnDownloadEventAttachments();
    afx_msg void OnViewItemAttachment();
    afx_msg void OnDeleteAttachment();
    afx_msg void OnToggleInlineAttachments();
	afx_msg void OnUpdateChangeOptions(CCmdUI* pCmdUI);
    afx_msg void OnUpdateShowEventMessageBody(CCmdUI* pCmdUI);
    afx_msg void OnUpdateShowEventAttachments(CCmdUI* pCmdUI);
    afx_msg void OnUpdateDownloadEventAttachments(CCmdUI* pCmdUI);
    afx_msg void OnUpdateViewItemAttachment(CCmdUI* pCmdUI);
    afx_msg void OnUpdateDeleteAttachment(CCmdUI* pCmdUI);
    afx_msg void OnUpdateToggleInlineAttachments(CCmdUI* pCmdUI);
    void loadMoreImpl(const std::vector<GridBackendRow*>& p_Rows);

	void onPostExplodeMultiValueSisterhood(GridBackendRow* p_Row, const std::set<GridBackendColumn*>& p_Columns) override;

    GridBackendRow* fillRow(GridBackendRow* p_ParentRow, const BusinessEvent& p_Event);
	void fillEventFields(GridBackendRow* p_Row, const BusinessEvent& p_Event);
	void fillAttachmentFields(GridBackendRow* p_Row, const vector<BusinessAttachment>& p_Attachments);
	void loadAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, GridUpdater& updater);

	template<class BusinessObjectType>
	void buildTreeViewImpl(const O365DataMap<BusinessObjectType, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_Updates, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge, bool p_NoPurge);

	void buildTreeItem(const BusinessUser& p_User, const vector<BusinessEvent>& p_Events, GridBackendRow* p_ParentRow, GridUpdater& p_GridUpdater);
	void buildTreeItem(const BusinessGroup& p_Group, const vector<BusinessEvent>& p_Events, GridBackendRow* p_ParentRow, GridUpdater& p_GridUpdater);

	GridBackendColumn*& getColMetaDisplayName() const;
	GridBackendColumn*& getColMetaID() const;

	GridBackendRow* addParentRow(const BusinessUser& p_User, GridUpdater& p_Updater);
	GridBackendRow* addParentRow(const BusinessGroup& p_Group, GridUpdater& p_Updater);

private:
    DECLARE_CacheGrid_DeleteRowLParam

    std::unique_ptr<MessageBodyViewer> m_MessageViewer;

	std::unique_ptr<GridTemplateUsers> m_TemplateUsers;
	std::unique_ptr<GridTemplateGroups> m_TemplateGroups;

    GridBackendColumn* m_ColBodyPreview;
    GridBackendColumn* m_ColCategories;
    GridBackendColumn* m_ColChangeKey;
    GridBackendColumn* m_ColCreatedDateTime;
    GridBackendColumn* m_ColHasAnyAttachment;
    GridBackendColumn* m_ColHasGraphAttachment;
    GridBackendColumn* m_ColICalUid;
    GridBackendColumn* m_ColImportance;
    GridBackendColumn* m_ColIsAllDay;
    GridBackendColumn* m_ColIsCancelled;
    GridBackendColumn* m_ColIsOrganizer;
    GridBackendColumn* m_ColAttendeesType;
    GridBackendColumn* m_ColAttendeesEmailAddressName;
    GridBackendColumn* m_ColAttendeesEmailAddressAddress;
    GridBackendColumn* m_ColAttendeesStatusResponse;
    GridBackendColumn* m_ColAttendeesStatusTime;
    GridBackendColumn* m_ColOrganizerEmailAddressName;
    GridBackendColumn* m_ColOrganizerEmailAddressAddress;
    GridBackendColumn* m_ColResponseStatusResponse;
    GridBackendColumn* m_ColResponseStatusTime;
    GridBackendColumn* m_ColStartDateTime;
    GridBackendColumn* m_ColStartTimeZone;
    GridBackendColumn* m_ColEndDateTime;
    GridBackendColumn* m_ColEndTimeZone;
    GridBackendColumn* m_ColLocationDisplayName;
    GridBackendColumn* m_ColLocationLocationEmailAddress;
    GridBackendColumn* m_ColLocationAddressStreet;
    GridBackendColumn* m_ColLocationAddressCity;
    GridBackendColumn* m_ColLocationAddressState;
    GridBackendColumn* m_ColLocationAddressCountryOrRegion;
    GridBackendColumn* m_ColLocationAddressPostalCode;

    // Recurrence
    GridBackendColumn* m_ColRecurrencePatternType;
    GridBackendColumn* m_ColRecurrencePatternInterval;
    GridBackendColumn* m_ColRecurrencePatternMonth;
    GridBackendColumn* m_ColRecurrencePatternDayOfMonth;
    GridBackendColumn* m_ColRecurrencePatternDaysOfWeek;
    GridBackendColumn* m_ColRecurrencePatternFirstDayWeek;
    GridBackendColumn* m_ColRecurrencePatternIndex;
    GridBackendColumn* m_ColRecurrenceRangeType;
    GridBackendColumn* m_ColRecurrenceRangeStartDate;
    GridBackendColumn* m_ColRecurrenceRangeEndDate;
    GridBackendColumn* m_ColRecurrenceRecurrenceTimeZone;
    GridBackendColumn* m_ColRecurrenceNumberOfOccurences;

    GridBackendColumn* m_ColIsReminderOn;
    GridBackendColumn* m_ColLastModifiedDateTime;
    GridBackendColumn* m_ColOnlineMeetingUrl;
    GridBackendColumn* m_ColOriginalEndTimeZone;
    GridBackendColumn* m_ColOriginalStart;
    GridBackendColumn* m_ColOriginalStartTimeZone;
    GridBackendColumn* m_ColReminderMinutesBeforeStart;
    GridBackendColumn* m_ColResponseRequested;
    GridBackendColumn* m_ColSensitivity;
    GridBackendColumn* m_ColSeriesMasterId;
    GridBackendColumn* m_ColShowAs;
    GridBackendColumn* m_ColSubject;
    GridBackendColumn* m_ColBodyContentType;
    GridBackendColumn* m_ColType;
    GridBackendColumn* m_ColWebLink;

    GridBackendColumn* m_ColAttachmentType;
    GridBackendColumn* m_ColAttachmentId;
    GridBackendColumn* m_ColAttachmentName;
	GridBackendColumn* m_ColAttachmentNameExtOnly;
    GridBackendColumn* m_ColAttachmentContentType;
    GridBackendColumn* m_ColAttachmentSize;
    GridBackendColumn* m_ColAttachmentLastModifiedDateTime;
    GridBackendColumn* m_ColAttachmentIsInline;

	vector<GridBackendColumn*>	m_MetaColumns;

    GridFrameBase* m_FrameOrigin;
	FrameEvents* m_FrameEvents;

    GridUtil::AttachmentIcons m_Icons;

    bool m_ShowInlineAttachments;
    ModuleBase::AttachmentsContainer m_AllAttachments;

	bool m_HasLoadMoreAdditionalInfo;
	O365IdsContainer m_MetaIDsMoreLoaded;

	bool m_IsMyData;
	boost::YOpt<MyDataMeRowHandler> m_MyDataMeHandler;

	HACCEL m_hAccelSpecific = nullptr;

    friend class EventAttachmentDeletion;
    friend class AttachmentInfo;
	friend class MessageBodyViewer;
};
