#include "TeamDeserializer.h"

#include "JsonSerializeUtil.h"
#include "TeamMemberSettingsDeserializer.h"
#include "TeamGuestSettingsDeserializer.h"
#include "TeamMessagingSettingsDeserializer.h"
#include "TeamFunSettingsDeserializer.h"

void TeamDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        TeamMemberSettingsDeserializer tmsd;
        if (JsonSerializeUtil::DeserializeAny(tmsd, _YTEXT("memberSettings"), p_Object))
            m_Data.MemberSettings = std::move(tmsd.GetData());
    }

    {
        TeamGuestSettingsDeserializer tgsd;
        if (JsonSerializeUtil::DeserializeAny(tgsd, _YTEXT("guestSettings"), p_Object))
            m_Data.GuestSettings = std::move(tgsd.GetData());
    }

    {
        TeamMessagingSettingsDeserializer tmsd;
        if (JsonSerializeUtil::DeserializeAny(tmsd, _YTEXT("messagingSettings"), p_Object))
            m_Data.MessagingSettings = std::move(tmsd.GetData());
    }

    {
        TeamFunSettingsDeserializer tfsd;
        if (JsonSerializeUtil::DeserializeAny(tfsd, _YTEXT("funSettings"), p_Object))
            m_Data.FunSettings = std::move(tfsd.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("webUrl"), m_Data.WebUrl, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isArchived"), m_Data.IsArchived, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("internalId"), m_Data.InternalId, p_Object);
}
