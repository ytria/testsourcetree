#pragma once

#include "IJsonSerializer.h"

#include "BusinessAssignedLicense.h"

class AssignedLicenseSerializer : public IJsonSerializer
{
public:
    AssignedLicenseSerializer(const BusinessAssignedLicense& p_AssignedLicense);
    void Serialize() override;

private:
    const BusinessAssignedLicense& m_AssignedLicense;
};

