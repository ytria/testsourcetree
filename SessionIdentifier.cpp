#include "SessionIdentifier.h"

#include "SessionTypes.h"
#include "Str.h"

SessionIdentifier::SessionIdentifier()
    :m_RoleID(g_NoRole)
{
}

SessionIdentifier::SessionIdentifier(const wstring& p_EmailOrAppId, const wstring& p_SessionType, const int64_t p_RoleID)
    : m_EmailOrAppId(p_EmailOrAppId)
	, m_SessionType(p_SessionType)
    , m_RoleID(p_RoleID)
{
}

bool SessionIdentifier::IsUltraAdmin() const
{
	return m_SessionType == SessionTypes::g_UltraAdmin;
}

bool SessionIdentifier::IsAdvanced() const
{
	return m_SessionType == SessionTypes::g_AdvancedSession;
}

bool SessionIdentifier::IsElevated() const
{
	return m_SessionType == SessionTypes::g_ElevatedSession;
}

bool SessionIdentifier::IsStandard() const
{
	return m_SessionType == SessionTypes::g_StandardSession;
}

bool SessionIdentifier::IsRole() const
{
	return m_SessionType == SessionTypes::g_Role;
}

bool SessionIdentifier::IsPartnerAdvanced() const
{
	return m_SessionType == SessionTypes::g_PartnerAdvancedSession;
}

bool SessionIdentifier::IsPartnerElevated() const
{
	return m_SessionType == SessionTypes::g_PartnerElevatedSession;
}

bool SessionIdentifier::operator<(const SessionIdentifier& p_Other) const
{
    if (m_EmailOrAppId == p_Other.m_EmailOrAppId)
    {
        if (m_SessionType == p_Other.m_SessionType)
            return m_RoleID < p_Other.m_RoleID;
        
        return m_SessionType < p_Other.m_SessionType;
    }
    return m_EmailOrAppId < p_Other.m_EmailOrAppId;

}

wstring SessionIdentifier::Serialize() const
{
    return _YFORMAT(L"%s|%s|%s", m_EmailOrAppId.c_str(), m_SessionType.c_str(), std::to_wstring(m_RoleID).c_str());
}

bool SessionIdentifier::Deserialize(const wstring& p_Serialized)
{
    auto values = Str::explodeIntoVector(p_Serialized, wstring(_YTEXT("|")), true, true);

    ASSERT(values.size() == 3);
    if (values.size() == 3)
    {
        m_EmailOrAppId = values[0];
        m_SessionType = values[1];
        m_RoleID = Str::getINT64FromString(values[2]);
        return true;
    }

    return false;
}

bool SessionIdentifier::operator==(const SessionIdentifier& p_Other) const
{
    return m_RoleID == p_Other.m_RoleID
        && m_EmailOrAppId == p_Other.m_EmailOrAppId
        && m_SessionType == p_Other.m_SessionType;
}
