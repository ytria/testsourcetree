#pragma once

#include "BusinessGroup.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class GroupNonPropDeserializer : public JsonObjectDeserializer
                               , public Encapsulate<BusinessGroup>
{
public:
    GroupNonPropDeserializer() = default;
    void DeserializeObject(const web::json::object& p_Object) override;
};

