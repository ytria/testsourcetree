#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "KeyCredential.h"

class KeyCredentialDeserializer : public JsonObjectDeserializer, public Encapsulate <KeyCredential>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

