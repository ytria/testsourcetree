#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "EducationClass.h"

class EducationClassDeserializer : public JsonObjectDeserializer, public Encapsulate<EducationClass>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

