#include "OptionalClaimDeserializer.h"
#include "ListDeserializer.h"

void OptionalClaimDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		ListStringDeserializer addPropsDeserializer;
		if (JsonSerializeUtil::DeserializeAny(addPropsDeserializer, _YTEXT("additionalProperties"), p_Object))
			m_Data.m_AdditionalProperties = std::move(addPropsDeserializer.GetData());
	}
	JsonSerializeUtil::DeserializeBool(_YTEXT("essential"), m_Data.m_Essential, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("source"), m_Data.m_Source, p_Object);
}
