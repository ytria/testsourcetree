#pragma once

#include "ISubItemModification.h"
#include "SubItemFieldUpdate.h"

class O365Grid;
class RowSubItemFieldUpdates : public ISubItemModification
{
public:
    RowSubItemFieldUpdates(O365Grid& p_Grid,
        const SubItemKey& p_Key,
        GridBackendColumn* p_ColSubItemElder,
        GridBackendColumn* p_ColSubItemPrimaryKey,
        GridBackendRow* p_Row);
    virtual ~RowSubItemFieldUpdates();

    virtual bool	IsCorrespondingRow(GridBackendRow* p_Row) const override;
    virtual bool	Refresh(GridBackendRow* p_Row, const std::map<SubItemKey, HttpResultWithError>& p_Errors) override;
    virtual void	RevertSpecific(const SubItemInfo& p_Info) override;

	virtual vector<ModificationLog> GetModificationLogs() const override;

	void AddFieldUpdate(std::unique_ptr<SubItemFieldUpdate> p_FieldUpdate);
	void ShowAppliedLocally(GridBackendRow* p_Row) const;

    const std::vector<std::unique_ptr<SubItemFieldUpdate>>& GetFieldUpdates() const;

private:
    State GetHighestPriorityState(GridBackendRow* p_Row, const std::map<SubItemKey, HttpResultWithError>& p_Errors) const;

    std::vector<std::unique_ptr<SubItemFieldUpdate>> m_FieldUpdates;
};

