#pragma once

#include "DllLoader.h"

#include <functional>

class ICollectionProperty;
class IDateTimeOffset;
class IPowerShell;
class IPSObjectCollection;
class IPSStream;

class YtriaPowerShellDll
{
private:
	YtriaPowerShellDll();
	YtriaPowerShellDll(const YtriaPowerShellDll& p_Dll) = delete;

public:
	IPowerShell* CreatePowerShell(bool p_ExecutionPolicyUnrestricted);
	void ReleasePowerShell(IPowerShell* p_Instance);
	void ReleasePSObjectCollection(IPSObjectCollection* p_Instance);
	void ReleasePSStream(IPSStream* p_Instance);
	void ReleaseDateTimeOffset(IDateTimeOffset* p_Offset);
	void ReleaseCollectionProperty(ICollectionProperty* p_Instance);

	static YtriaPowerShellDll& Get();

private:
	std::function<IPowerShell*(bool)> m_CreatePowerShell;
	std::function<void(IPowerShell*)> m_ReleasePowerShell;
	std::function<void(IPSObjectCollection*)> m_ReleasePSObjectCollection;
	std::function<void(IPSStream*)> m_ReleasePSStream;
	std::function<void(IDateTimeOffset*)> m_ReleaseDateTimeOffset;
	std::function<void(ICollectionProperty*)> m_CollectionProperty;

	DllLoader m_Loader;

	static const wstring g_DllPath;
};
