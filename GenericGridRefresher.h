#pragma once

#include "IGridRefresher.h"

class GridFrameBase;
class O365Grid;

class GenericGridRefresher : public IGridRefresher
{
public:
	GenericGridRefresher(O365Grid& p_Grid);

	void RefreshAll() override;
	void RefreshSelection() override;
	void Sync() override;

private:
	bool checkPendingModifications(GridFrameBase* p_Frame, bool p_SelectionOnly) const;

private:
	O365Grid& m_Grid;
};

