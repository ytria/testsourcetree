#include "BasePermissionsDeserializer.h"

#include "JsonSerializeUtil.h"

void Sp::BasePermissionsDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("High"), m_Data.High, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("Low"), m_Data.Low, p_Object);
}
