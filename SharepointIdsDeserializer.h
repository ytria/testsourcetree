#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "SharePointIds.h"

class SharepointIdsDeserializer : public JsonObjectDeserializer, public Encapsulate<SharepointIds>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

