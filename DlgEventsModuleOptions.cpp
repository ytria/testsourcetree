#include "DlgEventsModuleOptions.h"
#include "RoleDelegationManager.h"

void DlgEventsModuleOptions::generateJSONScriptData()
{
	initMain(_YTEXT("DlgEventModuleOptions"));

	getGenerator().addIconTextField(_YTEXT("iconText"), _T("Limit the number of events to load by selecting a date/time range. You can request more later."), _YTEXT("fas fa-calendar-alt"));

	addCutOffEditor(_T("Get events started within: "), _T("Started after (excluding)"));

	getGenerator().addIconTextField(_YTEXT("iconText2"), _T("Refine your selection"), _YTEXT("fas fa-search"));
	addFilterEditor();

	const auto roleId = m_Session->GetRoleDelegationID();
	bool hasPrivilege = RoleDelegationManager::GetInstance().DelegationHasPrivilege(roleId, RoleDelegationUtil::RBAC_USER_MESSAGES_SEE_MAIL_CONTENT, m_Session);
	const auto canSeeBody = roleId == 0 || (m_Session && hasPrivilege);
	const uint32_t flags = canSeeBody ? EditorFlags::DIRECT_EDIT : EditorFlags::READONLY;

	getGenerator().addIconTextField(_YTEXT("iconText"), _T("Select details to include (exclude all for fastest results)."), _YTEXT("fas fa-clipboard-list-check"));
	getGenerator().Add(BoolToggleEditor({ { g_AutoParamBodyPreview, _T("Body preview"), flags, canSeeBody && m_ModuleOpts.m_BodyPreview }, { YtriaTranslate::Do(DlgFilterTree_OnInitDialog_4, _YLOC("Include")).c_str(), YtriaTranslate::Do(DlgFilterTree_OnInitDialog_5, _YLOC("Exclude")).c_str() } }));
	getGenerator().Add(BoolToggleEditor({ { g_AutoParamFullBody, YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_28, _YLOC("Body content")).c_str(), flags, canSeeBody && m_ModuleOpts.m_BodyContent }, { YtriaTranslate::Do(DlgFilterTree_OnInitDialog_4, _YLOC("Include")).c_str(), YtriaTranslate::Do(DlgFilterTree_OnInitDialog_5, _YLOC("Exclude")).c_str() } }));

	getGenerator().addApplyButton(YtriaTranslate::Do(CDlg_OnInitDialog_2, _YLOC("Ok")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str());
}

wstring DlgEventsModuleOptions::getDialogTitle() const
{
	return _T("Load Events - Options");
}

std::map<wstring, DlgModuleOptions::FilterFieldProperties> DlgEventsModuleOptions::getFilterFields() const
{
	return
	{
			{ _YTEXT("subject"), { YtriaTranslate::Do(GridEvents_customizeGrid_2, _YLOC("Subject")).c_str(), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT("importance"), { YtriaTranslate::Do(GridEvents_customizeGrid_33, _YLOC("Importance")).c_str(), true, FilterFieldProperties::EnumType({ { _YTEXT("low"), _T("Low (0)") }, { _YTEXT("normal"), _T("Normal (1)") }, { _YTEXT("high"), _T("High (2)") } }), g_FilterOperatorsNoStartWith } }				// (enum)
		,	{ _YTEXT("sensitivity"), { YtriaTranslate::Do(GridEvents_customizeGrid_34, _YLOC("Sensitivity")).c_str(), true, FilterFieldProperties::EnumType({ { _YTEXT("normal"), _T("Normal (0)") }, { _YTEXT("personal"), _T("Personal (1)") }, { _YTEXT("private"), _T("Private (2)") }, { _YTEXT("confidential"), _T("Confidential (3)") } }), g_FilterOperatorsNoStartWith } }				// (enum)
		,	{ _YTEXT("showAs"), { YtriaTranslate::Do(GridEvents_customizeGrid_35, _YLOC("Show As")).c_str(), true, FilterFieldProperties::EnumType({ { _YTEXT("free"), _T("Free (0)") }, { _YTEXT("tentative"), _T("Tentative (1)") }, { _YTEXT("busy"), _T("Busy (2)") }, { _YTEXT("oof"), _T("OOF (3)") }, { _YTEXT("workingElsewhere"), _T("Working Elsewhere (4)") }, { _YTEXT("unknown"), _T("Unknown (5)") } }), g_FilterOperatorsNoStartWith } }				// (enum)
		,	{ _YTEXT("type"), { YtriaTranslate::Do(GridEvents_customizeGrid_32, _YLOC("Event Type")).c_str(), true, FilterFieldProperties::EnumType({ { _YTEXT("singleInstance"), _T("Single Instance (0)") }, { _YTEXT("occurence"), _T("Occurence (1)") }, { _YTEXT("exception"), _T("Exception (2)") }, { _YTEXT("seriesMaster"), _T("Series Master (3)") } }), g_FilterOperatorsNoStartWith } }				// (enum)
		
		// FIXME: Our DateTime editor is not good, better with Date (with calendar) but that prevent from using EQ or NE...
		//,	{ _YTEXT("start/dateTime"), { YtriaTranslate::Do(GridEvents_customizeGrid_9, _YLOC("Start On")).c_str(), true, FilterFieldProperties::DateTimeType(), {} }
		//,	{ _YTEXT("end/dateTime"), { YtriaTranslate::Do(GridEvents_customizeGrid_13, _YLOC("End On")).c_str(), true, FilterFieldProperties::DateTimeType(), {} }
		,	{ _YTEXT("start/dateTime"), { YtriaTranslate::Do(GridEvents_customizeGrid_9, _YLOC("Start On")).c_str(), true, FilterFieldProperties::DateType(), g_FilterOperatorsNoEQNE } }
		,	{ _YTEXT("end/dateTime"), { YtriaTranslate::Do(GridEvents_customizeGrid_13, _YLOC("End On")).c_str(), true, FilterFieldProperties::DateType(), g_FilterOperatorsNoEQNE } }

		,	{ _YTEXT("location/displayName"), { YtriaTranslate::Do(GridEvents_customizeGrid_40, _YLOC("Display Name - Location")).c_str(), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT("organizer/emailaddress/name"), { YtriaTranslate::Do(GridEvents_customizeGrid_24, _YLOC("Organizer - Name")).c_str(), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT("iCalUId"), { YtriaTranslate::Do(GridEvents_customizeGrid_62, _YLOC("Cross Calendar Unique ID")).c_str(), true, FilterFieldProperties::StringType(), {} } }
		
		// FIXME: Our DateTime editor is not good, better with Date (with calendar) but that prevent from using EQ or NE...
		//,	{ _YTEXT("createdDateTime"), { YtriaTranslate::Do(GridEvents_customizeGrid_37, _YLOC("Created On")).c_str(), false, FilterFieldProperties::DateTimeType(), {} } }
		//,	{ _YTEXT("lastModifiedDateTime"), { YtriaTranslate::Do(GridEvents_customizeGrid_38, _YLOC("Last Modified")).c_str(), false, FilterFieldProperties::DateTimeType(), {} } }
		,	{ _YTEXT("createdDateTime"), { YtriaTranslate::Do(GridEvents_customizeGrid_37, _YLOC("Created On")).c_str(), false, FilterFieldProperties::DateType(), g_FilterOperatorsNoEQNE } }
		,	{ _YTEXT("lastModifiedDateTime"), { YtriaTranslate::Do(GridEvents_customizeGrid_38, _YLOC("Last Modified")).c_str(), false, FilterFieldProperties::DateType(), g_FilterOperatorsNoEQNE } }

		,	{ _YTEXT("hasAttachments"), { YtriaTranslate::Do(GridEvents_customizeGrid_64, _YLOC("Has Attachment")).c_str(), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } }			// bool
		,	{ _YTEXT("isAllDay"), { YtriaTranslate::Do(GridEvents_customizeGrid_8, _YLOC("All Day")).c_str(), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } }				// bool
		,	{ _YTEXT("isCancelled"), { YtriaTranslate::Do(GridEvents_customizeGrid_1, _YLOC("Cancelled")).c_str(), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } }			// bool
		,	{ _YTEXT("isOrganizer"), { YtriaTranslate::Do(GridEvents_customizeGrid_23, _YLOC("Organizer")).c_str(), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } }			// bool
		,	{ _YTEXT("responseRequested"), { YtriaTranslate::Do(GridEvents_customizeGrid_4, _YLOC("Response Requested")).c_str(), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } }		// bool
		//,	{ _YTEXT("allowNewTimeProposals"), false }	// bool
	};
}
