#pragma once

#include "AddIn.h"
#include "ApiApplication.h"
#include "AppRole.h"
#include "BusinessObject.h"
#include "InformationalUrl.h"
#include "KeyCredential.h"
#include "OptionalClaims.h"
#include "ParentalControlSettings.h"
#include "PasswordCredential.h"
#include "PublicClientApplication.h"
#include "RequiredResourceAccess.h"
#include "WebApplication.h"

class Application : public BusinessObject
{
public:
	boost::YOpt<vector<AddIn>>		  m_AddIns;
	boost::YOpt<ApiApplication>		  m_Api;
	boost::YOpt<PooledString>		  m_AppId;
	boost::YOpt<vector<AppRole>>	  m_AppRoles;
	boost::YOpt<YTimeDate>			  m_CreatedDateTime;
	boost::YOpt<YTimeDate>			  m_DeletedDateTime;
	boost::YOpt<PooledString>		  m_DisplayName;
	boost::YOpt<PooledString>		  m_GroupMembershipClaims;
	boost::YOpt<PooledString>		  m_Id;
	boost::YOpt<vector<PooledString>> m_IdentifierUris;
	boost::YOpt<InformationalUrl>	  m_Info;
	boost::YOpt<bool>				  m_IsFallbackPublicClient;
	boost::YOpt<vector<KeyCredential>>  m_KeyCredentials;
	boost::YOpt<PooledString>		  m_Logo;
	boost::YOpt<OptionalClaims>		  m_OptionalClaims;
	boost::YOpt<ParentalControlSettings> m_ParentalControlSettings;
	boost::YOpt<vector<PasswordCredential>> m_PasswordCredentials;
	boost::YOpt<PublicClientApplication>    m_PublicClient;
	boost::YOpt<PooledString>		  m_PublisherDomain;
	boost::YOpt<vector<RequiredResourceAccess>> m_RequiredResourceAccess;
	boost::YOpt<PooledString> m_SignInAudience;
	boost::YOpt<vector<PooledString>> m_Tags;
	boost::YOpt<PooledString> m_TokenEncryptionKeyId;
	boost::YOpt<WebApplication> m_WebApplication;

	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
};
