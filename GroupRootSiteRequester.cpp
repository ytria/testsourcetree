#include "GroupRootSiteRequester.h"
#include "SiteDeserializer.h"
#include "MsGraphHttpRequestLogger.h"

GroupRootSiteRequester::GroupRootSiteRequester(const wstring& p_GroupId, const std::shared_ptr<IRequestLogger>& p_Logger)
	:m_GroupId(p_GroupId),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> GroupRootSiteRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<SiteDeserializer>(p_Session);

	web::uri_builder uri(_YTEXT("groups"));
	uri.append_path(m_GroupId);
	uri.append_path(_YTEXT("sites/root"));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, uri.to_uri(), false, m_Logger, httpLogger, p_TaskData);
}

const BusinessSite& GroupRootSiteRequester::GetData() const
{
	return m_Deserializer->GetData();
}

BusinessSite& GroupRootSiteRequester::GetData()
{
	return m_Deserializer->GetData();
}
