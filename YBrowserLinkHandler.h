#pragma once

#include "IBrowserLinkHandler.h"

class YBrowserLinkHandler : public IBrowserLinkHandler
{
public:
    YBrowserLinkHandler();
    virtual void Handle(YBrowserLink& link) const override;

private:
    void registerLinkHandlers();

private:
    std::unordered_map<wstring, std::unique_ptr<IBrowserLinkHandler>> m_LinkHandlers;
};
