#include "SiteCollectionDeserializer.h"

#include "JsonSerializeUtil.h"

void SiteCollectionDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("hostname"), m_Data.Hostname, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("dataLocationCode"), m_Data.DataLocationCode, p_Object);
}
