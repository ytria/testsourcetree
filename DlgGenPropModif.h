#pragma once

#include "CacheGrid.h"
#include "BusinessObject.h"
#include "Resource.h"

enum class DlgGenPropModifAction
{
	CREATE,
	EDIT,
	EDIT_NOLOADMORE,
	READ
};

template<class T>
class DlgGenPropModif : public ResizableDialog
{
public:
	// Will generate a compile time error if T is not derived from the correct class.
	static_assert(std::is_base_of<BusinessObject, T>::value, "T must inherit from BusinessObject");

	virtual ~DlgGenPropModif() = default;

protected:
	DlgGenPropModif(vector<T>& p_Objects, UINT dlgID, DlgGenPropModifAction p_Type, CWnd* p_Parent);

	DlgGenPropModifAction GetAction() const;
	vector<T>& getObjects();

private:
	DlgGenPropModifAction m_Action;

	vector<T>& m_Objects;
};

#include "DlgGenPropModif.hpp"
