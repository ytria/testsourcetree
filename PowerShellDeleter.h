#pragma once

#include "IPowerShell.h"

class PowerShellDeleter
{
public:
	PowerShellDeleter();
	void operator()(IPowerShell* p_PowerShell);
};
