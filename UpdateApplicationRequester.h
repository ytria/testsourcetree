#pragma once

#include "IRequester.h"
#include "SingleRequestResult.h"

class ApplicationDeserializer;

class UpdateApplicationRequester : public IRequester
{
public:
	UpdateApplicationRequester(const wstring& p_AppId);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	void SetDisplayName(const wstring& p_DisplayName);

private:
	wstring m_AppId;
	std::shared_ptr<SingleRequestResult> m_Result;
	std::shared_ptr<ApplicationDeserializer> m_Deserializer;

	web::json::value m_Values;
};

