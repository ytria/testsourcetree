#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessFileAttachment.h"

class FileAttachmentDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessFileAttachment>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

