#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Shared.h"

class SharedDeserializer : public JsonObjectDeserializer, public Encapsulate<Shared>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

