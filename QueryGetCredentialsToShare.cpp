#include "QueryGetCredentialsToShare.h"
#include "SessionsSqlEngine.h"
#include "SqlQuerySelect.h"
#include "SessionTypes.h"
#include "InlineSqlQueryRowHandler.h"

QueryGetCredentialsToShare::QueryGetCredentialsToShare(const SessionIdentifier& p_Id)
	:m_Id(p_Id),
	m_CredsId(0)
{
}

void QueryGetCredentialsToShare::Run()
{
	if (m_Id.m_SessionType == SessionTypes::g_Role || m_Id.m_SessionType == SessionTypes::g_StandardSession)
	{
		auto handler = [this](sqlite3_stmt* p_Stmt) {
			m_CredsId = sqlite3_column_int64(p_Stmt, 0);
		};

		SqlQuerySelect query(SessionsSqlEngine::Get(), InlineSqlQueryRowHandler<decltype(handler)>(handler), _YTEXT(R"(SELECT CredentialsId FROM Sessions WHERE EmailOrAppId=? AND SessionType IN(?,?) AND CredentialsId IS NOT NULL )"));
		query.BindString(1, m_Id.m_EmailOrAppId);
		query.BindString(2, SessionTypes::g_StandardSession);
		query.BindString(3, SessionTypes::g_Role);
		query.Run();
		m_Status = query.GetStatus();
	}
}

int64_t QueryGetCredentialsToShare::GetCredentialsId() const
{
	return m_CredsId;
}
