#pragma once

class CommaSeparatedString
{
public:
	CommaSeparatedString() = default;
	CommaSeparatedString(const wstring& p_InitialStr);

	void Add(const wstring& p_Str);
	const wstring& Get() const;

private:
	wstring m_Str;
};

