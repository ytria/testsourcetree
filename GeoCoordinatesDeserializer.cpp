#include "GeoCoordinatesDeserializer.h"

void GeoCoordinatesDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeDouble(_YTEXT("altitude"), m_Data.Altitude, p_Object);
	JsonSerializeUtil::DeserializeDouble(_YTEXT("latitude"), m_Data.Latitude, p_Object);
	JsonSerializeUtil::DeserializeDouble(_YTEXT("longitude"), m_Data.Longitude, p_Object);
}
