#include "SiteDeserializer.h"

#include "DbSiteSerializer.h"
#include "JsonSerializeUtil.h"
#include "MsGraphFieldNames.h" 
#include "RootDeserializer.h"
#include "SharepointIdsDeserializer.h"
#include "SiteCollectionDeserializer.h"

SiteDeserializer::SiteDeserializer()
{
}

SiteDeserializer::SiteDeserializer(std::shared_ptr<const Sapio365Session> p_Session) : RoleDelegationTagger(p_Session, &m_Data)
{
}

#ifdef _DEBUG
SiteDeserializer::SiteDeserializer(std::shared_ptr<const Sapio365Session> p_Session, bool p_NoSelectUsed)
	: SiteDeserializer(p_Session)
{
	m_NoSelectUsed = p_NoSelectUsed;
}
#endif

void SiteDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	ASSERT(m_NoSelectUsed || containsAllRequiredProperties<BusinessSite>(p_Object));

	JsonSerializeUtil::DeserializeId(_YUID(O365_ID), m_Data.m_Id, p_Object, true);
	deserializeTimeDateAndTagObject(_YUID(O365_SITE_CREATEDDATETIME), m_Data.m_CreatedDateTime, p_Object);
	deserializeStringAndTagObject(_YUID(O365_SITE_DESCRIPTION), m_Data.m_Description, p_Object);
	deserializeStringAndTagObject(_YUID(O365_SITE_DISPLAYNAME), m_Data.m_DisplayName, p_Object);
	deserializeTimeDateAndTagObject(_YUID(O365_SITE_LASTMODIFIEDDATETIME), m_Data.m_LastModifiedDateTime, p_Object);
	deserializeStringAndTagObject(_YUID(O365_SITE_NAME), m_Data.m_Name, p_Object);

    {
        RootDeserializer rd;
        const bool hasRoot = JsonSerializeUtil::DeserializeAny(rd, _YUID(O365_SITE_ROOT), p_Object);
        if (hasRoot)
            m_Data.SetRoot(rd.GetData());

        tagObject(_YUID(O365_SITE_ROOT), hasRoot);
    }

    {
        SharepointIdsDeserializer sid;
        if (JsonSerializeUtil::DeserializeAny(sid, _YUID(O365_SITE_SHAREPOINTIDS), p_Object))
            m_Data.SetSharepointIds(sid.GetData());

        tagObject(_YUID(O365_SITE_SHAREPOINTIDSLISTID), sid.GetData().ListId);
        tagObject(_YUID(O365_SITE_SHAREPOINTIDSLISTITEMID), sid.GetData().ListItemId);
        tagObject(_YUID(O365_SITE_SHAREPOINTIDSLISTITEMUNIQUEID), sid.GetData().ListItemUniqueId);
        tagObject(_YUID(O365_SITE_SHAREPOINTIDSSITEID), sid.GetData().SiteId);
        tagObject(_YUID(O365_SITE_SHAREPOINTIDSSITEURL), sid.GetData().SiteUrl);
        tagObject(_YUID(O365_SITE_SHAREPOINTIDSWEBID), sid.GetData().WebId);
    }

    {
        SiteCollectionDeserializer scd;
        if (JsonSerializeUtil::DeserializeAny(scd, _YUID(O365_SITE_SITECOLLECTION), p_Object))
            m_Data.SetSiteCollection(scd.GetData());
    }

	deserializeStringAndTagObject(_YUID(O365_SITE_WEBURL), m_Data.m_WebUrl, p_Object);

	tagObjectFromComputedProperties();
}
