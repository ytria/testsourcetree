#pragma once

#include "IXmlSerializer.h"

namespace Ex
{
    class UserId;
}

namespace Ex
{
class UserIdSerializer : public IXmlSerializer
{
public:
    UserIdSerializer(XERCES_CPP_NAMESPACE::DOMDocument& p_Document, const Ex::UserId& p_UserId);

protected:
    void SerializeImpl(DOMElement& p_Element) override;

private:
    const Ex::UserId& m_UserId;
};
}

