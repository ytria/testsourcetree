#pragma once

#include "ModuleBase.h"

#include "BusinessChannel.h"
#include "CommandInfo.h"

class ModuleChannel : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

private:
    virtual void executeImpl(const Command& p_Command);
    void showChannels(const Command& p_Command);
	void loadMore(const Command& p_Command);

	O365DataMap<BusinessGroup, vector<BusinessChannel>> retrieveChannels(std::shared_ptr<BusinessObjectManager> p_BOM, const ModuleCriteria& p_Criteria, bool p_UseSQLCacheForGroups, YtriaTaskData p_TaskData) const;
};

