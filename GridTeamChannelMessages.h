#pragma once

#include "BaseO365Grid.h"
#include "BusinessChatMessage.h"
#include "ModuleUtil.h"

class FrameTeamChannelMessages;
class MessageBodyViewer;

class GridTeamChannelMessages : public ModuleO365Grid<BusinessChatMessage>
{
public:
    GridTeamChannelMessages();
    ~GridTeamChannelMessages();

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildView(const O365DataMap<BusinessGroup, std::map<BusinessChannel, vector<BusinessChatMessage>>>& p_Messages, bool p_Refresh, bool p_FullPurge);

	void HideMessageViewer();
     
	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
    virtual BusinessChatMessage getBusinessObject(GridBackendRow* p_Row) const;
	virtual void customizeGrid() override;

private:
    virtual void OnGbwSelectionChangedSpecific() override;
    
    DECLARE_MESSAGE_MAP()

	virtual BOOL PreTranslateMessage(MSG* pMsg) override;

    virtual void InitializeCommands() override;
    virtual void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart) override;

	GridBackendRow* addGroup(const BusinessGroup& p_Group, GridUpdater& p_Updater);
	void addChannel(const BusinessChannel& p_Channel, GridBackendRow* p_ParentRow, const vector<BusinessChatMessage>& p_Messages, GridUpdater& p_Updater);
	GridBackendRow* addChatMessage(const BusinessChatMessage& p_ChatMessage, GridBackendRow* p_ParentRow, GridUpdater& p_Updater, size_t p_Index);

    afx_msg void OnShowBody();
    afx_msg void OnUpdateShowBody(CCmdUI* pCmdUI);

private:
    DECLARE_CacheGrid_DeleteRowLParam

    std::unique_ptr<MessageBodyViewer> m_MessageViewer;

	GridBackendColumn* m_ColMetaChannelDisplayName; // channel display name
    GridBackendColumn* m_ColMetaGroupName;

	GridBackendColumn* m_ColMetaChannelId; // channel id, Tech hide
    GridBackendColumn* m_ColMetaGroupId; // Tech hide

	GridBackendColumn* m_ColMessageIndex;
	GridBackendColumn* m_ColMessageCount;
	GridBackendColumn* m_ColReplyTo;
	GridBackendColumn* m_ColETag;
	GridBackendColumn* m_ColMessageType;
	GridBackendColumn* m_ColCreatedDateTime;
	GridBackendColumn* m_ColLastModifiedDateTime;
	GridBackendColumn* m_ColDeleted;
	GridBackendColumn* m_ColDeletedDateTime;
	GridBackendColumn* m_ColSubject;
	GridBackendColumn* m_ColSummary;
	GridBackendColumn* m_ColImportance;
	GridBackendColumn* m_ColLocale;

	GridBackendColumn* m_ColFromAppId;
	GridBackendColumn* m_ColFromAppName;
	GridBackendColumn* m_ColFromDeviceId;
	GridBackendColumn* m_ColFromDeviceName;
	GridBackendColumn* m_ColFromUserId;
	GridBackendColumn* m_ColFromUserDisplayName;
	GridBackendColumn* m_ColFromUserIdentityProvider;

	GridBackendColumn* m_ColBodyContentPreview;
	GridBackendColumn* m_ColBodyContentType;

	GridBackendColumn* m_ColAttachmentId;
	GridBackendColumn* m_ColAttachmentContentType;
	GridBackendColumn* m_ColAttachmentContentURL;
	GridBackendColumn* m_ColAttachmentName;
	GridBackendColumn* m_ColAttachmentThumbnailURL;

	GridBackendColumn* m_ColMentionType;
	GridBackendColumn* m_ColMentionId;
	GridBackendColumn* m_ColMentionText;

	GridBackendColumn* m_ColReactionType;
	GridBackendColumn* m_ColReactionCreatedDateTime;
	GridBackendColumn* m_ColReactionContent;
	GridBackendColumn* m_ColReactionAppId;
	GridBackendColumn* m_ColReactionAppName;
	GridBackendColumn* m_ColReactionDeviceId;
	GridBackendColumn* m_ColReactionDeviceName;
	GridBackendColumn* m_ColReactionUserId;
	GridBackendColumn* m_ColReactionUserDisplayName;
	GridBackendColumn* m_ColReactionUserIdentityProvider;

    FrameTeamChannelMessages*				m_FrameTeamChannelMessages;

    GridUtil::AttachmentIcons m_Icons;

	HACCEL m_hAccelSpecific = nullptr;

    friend class AttachmentInfo;
	friend class MessageBodyViewer;
};
