#include "WorkingHoursDeserializer.h"

#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "TimeZoneBaseDeserializer.h"

void WorkingHoursDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(lsd, _YUID("daysOfWeek"), p_Object))
			m_Data.DaysOfWeek = std::move(lsd.GetData());
	}
    JsonSerializeUtil::DeserializeTimeOnly(_YTEXT("startTime"), m_Data.StartTime, p_Object);
    JsonSerializeUtil::DeserializeTimeOnly(_YTEXT("endTime"), m_Data.EndTime, p_Object);
    {
        TimeZoneBaseDeserializer tzbd;
        if (JsonSerializeUtil::DeserializeAny(tzbd, _YTEXT("timeZone"), p_Object))
            m_Data.TimeZone = tzbd.GetData();
    }
}
