#pragma once

#include "Command.h"
#include "O365UpdateOperation.h"

class GridFrameBase;

// To be used by FrameUsers and FrameGroups
class FlatObjectListFrameRefresher
{
public:
	// p_DirectGridUpdate is only used when update operations contain only successful deletion (no request necessary)
	void operator()(GridFrameBase& p_Frame, Command::ModuleTarget p_ModuleTarget, Origin p_DetailOrigin, AutomationAction* p_CurrentAction, std::function<void(const vector<O365UpdateOperation>&, bool, bool)> p_DirectGridUpdate);
};
