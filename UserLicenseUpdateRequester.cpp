#include "UserLicenseUpdateRequester.h"

#include "MSGraphSession.h"
#include "MSGraphCommonData.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "MsGraphHttpRequestLogger.h"

UserLicenseUpdateRequester::UserLicenseUpdateRequester(PooledString p_UserId)
    :m_UserId(p_UserId)
{
}

void UserLicenseUpdateRequester::SetDisabledSkus(const std::set<PooledString>& p_DisabledSkus)
{
    m_DisabledSkus = p_DisabledSkus;
}

void UserLicenseUpdateRequester::SetModifiedSkus(const map<PooledString, std::set<PooledString>>& p_ModifiedSkus)
{
    m_ModifiedSkus = p_ModifiedSkus;
}

TaskWrapper<void> UserLicenseUpdateRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri;
    uri.append_path(Rest::USERS_PATH);
    uri.append_path(GetUserId());
    uri.append_path(_YTEXT("assignLicense"));

    json::value json = json::value::object({
        { _YTEXT("addLicenses"), json::value::array() },
        { _YTEXT("removeLicenses"), json::value::array() },
    });

    json::value addLicensesJson = json::value::array();
    json::value removeLicensesJson = json::value::array();

    size_t indexDisabled = 0;
    for (const auto& sku : m_DisabledSkus)
        removeLicensesJson.as_array()[indexDisabled++] = json::value::string(sku);

    size_t indexModified = 0;
    for (const auto& sku : m_ModifiedSkus)
    {
        const auto& skuId = sku.first;
        const auto& disabledPlans = sku.second;

        json::value skuInfo = json::value::object();
        skuInfo[_YTEXT("skuId")] = json::value::string(skuId);

        json::value disabledPlansJson = json::value::array();
        size_t indexPlans = 0;
        for (const auto& plan : disabledPlans)
            disabledPlansJson.as_array()[indexPlans++] = json::value::string(plan);
        skuInfo[_YTEXT("disabledPlans")] = disabledPlansJson;

        addLicensesJson.as_array()[indexModified] = skuInfo;

        ++indexModified;
    }

	const WebPayloadJSON payload(json::value::object({
        { _YTEXT("addLicenses"), addLicensesJson },
        { _YTEXT("removeLicenses"), removeLicensesJson },
    }));

    LoggerService::User(YtriaTranslate::Do(UserLicenseUpdateRequester_Send_1, _YLOC("Updating user licenses with id \"%1\""), Str::MakeMidEllipsis(m_UserId).c_str()), p_TaskData.GetOriginator());

    auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
    return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_string(), httpLogger, p_TaskData, payload).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResultInfo) {
        try
        {
            result->SetResult(p_ResultInfo.get());
        }
        catch (const RestException& e)
        {
            RestResultInfo info(e.GetRequestResult());
            info.m_RestException = std::make_shared<RestException>(e);
            result->SetResult(info);
        }
        catch (const std::exception& e)
        {
            RestResultInfo info;
            info.m_Exception = e;
            result->SetResult(info);
        }
        catch (...)
        {
            result->SetResult(RestResultInfo());
        }
    });
}

const SingleRequestResult& UserLicenseUpdateRequester::GetResult() const
{
    return *m_Result;
}

PooledString UserLicenseUpdateRequester::GetUserId() const
{
    return m_UserId;
}
