#include "ModuleUtil.h"

#include "AnyItemAttachment.h"
#include "BaseO365Grid.h"
#include "O365AdminUtil.h"
#include "TimeUtil.h"

#include <tuple>
#include "BasicPowerShellSession.h"

bool ModuleUtil::ConfirmApply(GridFrameBase& p_Frame)
{
	auto applyConfirmationConfig = p_Frame.GetApplyConfirmationConfig(true);
	YCodeJockMessageBox confirmation(&p_Frame,
		applyConfirmationConfig.m_Icon,
		_T("Save Selected Changes"),
		_T("Are you sure?"),
		applyConfirmationConfig.m_AdditionalWarning,
		{ { IDOK, _T("Yes") },{ IDCANCEL, _T("No") } });
	return confirmation.DoModal() == IDOK;
}

ModuleUtil::eConfirmRefreshPendingChanges ModuleUtil::ConfirmRefreshPendingChanges(GridFrameBase& p_Frame)
{
	eConfirmRefreshPendingChanges result = eConfirmRefreshPendingChanges::Cancel;

	auto applyConfirmationConfig = p_Frame.GetApplyConfirmationConfig(false);
	YCodeJockMessageBox confirmation(&p_Frame,
		applyConfirmationConfig.m_Icon,
		_T("Changes pending"),
		_T("Some changes have not been saved and will be lost if you refresh. Do you want to save them now?"),
		applyConfirmationConfig.m_AdditionalWarning,
		{ { IDOK, _T("Save and Refresh") },
			{ IDABORT, _T("Refresh Only") },
			{ IDCANCEL, _T("Cancel") } });

	switch (confirmation.DoModal())
	{
	case IDOK:
		result = eConfirmRefreshPendingChanges::SaveAndRefresh;
		break;
	case IDCANCEL:
		result = eConfirmRefreshPendingChanges::Cancel;
		break;
	case IDABORT:
		result = eConfirmRefreshPendingChanges::Refresh;
		break;
	}
	return result;
}

wstring ModuleUtil::ToString(const Recipient& recipient)
{
	wstring result;
	if (recipient.m_EmailAddress.is_initialized())
	{
		if (recipient.m_EmailAddress->m_Name.is_initialized() && !recipient.m_EmailAddress->m_Name->IsEmpty())
			result = recipient.m_EmailAddress->m_Name->c_str();
		else if (recipient.m_EmailAddress->m_Address.is_initialized())
			result = recipient.m_EmailAddress->m_Address->c_str();
	}
	return result;
}

wstring ModuleUtil::ToString(const vector<Recipient>& recipients)
{
	wstring result;

	for (const auto& recipient : recipients)
	{
		result += ToString(recipient);
		result += _YTEXT("; ");
	}

	return result;
}

DlgContentViewer::HeaderConfig ModuleUtil::GetDlgContentViewerHeaderConfig(const AnyItemAttachment& p_AnyItemAttachment)
{
	DlgContentViewer::HeaderConfig hc;

	const auto attachmentKind = p_AnyItemAttachment.GetKind();

	// We don't handle contact right now.
	ASSERT(AnyItemAttachment::Kind::Unknown != attachmentKind && AnyItemAttachment::Kind::Contact != attachmentKind);

	if (AnyItemAttachment::Kind::Event == attachmentKind)
	{
		hc = GetDlgContentViewerHeaderConfig(p_AnyItemAttachment.ToEvent());
	}
	else if (AnyItemAttachment::Kind::Message == attachmentKind)
	{
		hc = GetDlgContentViewerHeaderConfig(p_AnyItemAttachment.ToMessage());
	}

	return hc;
}

DlgContentViewer::HeaderConfig ModuleUtil::GetDlgContentViewerHeaderConfig(const Message& p_Message)
{
	DlgContentViewer::HeaderConfig hc;

	if (p_Message.From.is_initialized())
		hc.m_FirstLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_1, _YLOC("From:")).c_str(), ToString(*p_Message.From) });

	if (p_Message.ReceivedDateTime.is_initialized())
	{
		CString dateStr;
		TimeUtil::GetInstance().ConvertDateToText(DateTimeFormat(), *p_Message.ReceivedDateTime, dateStr);
		ASSERT(!dateStr.IsEmpty());
		hc.m_FirstLine.push_back({ _YTEXT(""), (LPCTSTR)dateStr });
	}

	const auto to = ModuleUtil::ToString(p_Message.ToRecipients);
	if (!to.empty())
		hc.m_SecondLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_2, _YLOC("To:")).c_str(), to });
	
	const auto cc = ModuleUtil::ToString(p_Message.CcRecipients);
	if (!cc.empty())
		hc.m_SecondLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_3, _YLOC("CC:")).c_str(), cc });

	const auto bcc = ModuleUtil::ToString(p_Message.BccRecipients);
	if (!bcc.empty())
		hc.m_SecondLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_4, _YLOC("BCC:")).c_str(), bcc });

	if (p_Message.Subject.is_initialized())
		hc.m_Subject = *p_Message.Subject;

	return hc;
}

DlgContentViewer::HeaderConfig ModuleUtil::GetDlgContentViewerHeaderConfig(const Event& p_Event)
{
	DlgContentViewer::HeaderConfig hc;

	if (p_Event.Organizer.is_initialized())
		hc.m_FirstLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_5, _YLOC("Organizer:")).c_str(), ToString(*p_Event.Organizer) });

	if (p_Event.Start.is_initialized() && p_Event.Start->DateTime.is_initialized())
	{
		CString dateStr;
		TimeUtil::GetInstance().ConvertDateToText(DateTimeFormat(), *p_Event.Start->DateTime, dateStr);
		ASSERT(!dateStr.IsEmpty());
		hc.m_FirstLine.push_back({ _YTEXT(""), (LPCTSTR)dateStr });
	}

	if (p_Event.Subject.is_initialized())
		hc.m_Subject = *p_Event.Subject;

	return hc;
}

DlgContentViewer::HeaderConfig ModuleUtil::GetDlgContentViewerHeaderConfig(const Post& p_Post)
{
	DlgContentViewer::HeaderConfig hc;

	if (p_Post.From.is_initialized())
		hc.m_FirstLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_6, _YLOC("From:")).c_str(), ToString(*p_Post.From) });

	if (p_Post.ReceivedDateTime.is_initialized())
	{
		CString dateStr;
		TimeUtil::GetInstance().ConvertDateToText(DateTimeFormat(), *p_Post.ReceivedDateTime, dateStr);
		ASSERT(!dateStr.IsEmpty());
		hc.m_FirstLine.push_back({ _YTEXT(""), (LPCTSTR)dateStr });
	}

	if (p_Post.Sender.is_initialized() && (!p_Post.From.is_initialized() || !(*p_Post.Sender == *p_Post.From)))
		hc.m_SecondLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_7, _YLOC("Sender:")).c_str(), ModuleUtil::ToString(*p_Post.Sender) });

	{
		const auto participants = ModuleUtil::ToString(p_Post.NewParticipants);
		if (!participants.empty())
			hc.m_SecondLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_8, _YLOC("New Participants:")).c_str(), participants });
	}

	return hc;
}

DlgContentViewer::HeaderConfig ModuleUtil::GetDlgContentViewerHeaderConfig(const BusinessMessage& p_Message)
{
	return GetDlgContentViewerHeaderConfig(BusinessMessage::ToMessage(p_Message));
}

DlgContentViewer::HeaderConfig ModuleUtil::GetDlgContentViewerHeaderConfig(const BusinessEvent& p_Event)
{
	return GetDlgContentViewerHeaderConfig(BusinessEvent::ToEvent(p_Event));
}

DlgContentViewer::HeaderConfig ModuleUtil::GetDlgContentViewerHeaderConfig(const BusinessPost& p_Post)
{
	return GetDlgContentViewerHeaderConfig(BusinessPost::ToPost(p_Post));
}

DlgContentViewer::HeaderConfig ModuleUtil::GetDlgContentViewerHeaderConfig(const BusinessChatMessage& p_ChatMessage)
{
	DlgContentViewer::HeaderConfig hc;

	if (p_ChatMessage.GetFrom())
	{
		if (p_ChatMessage.GetFrom()->User)
		{
			if (p_ChatMessage.GetFrom()->User->DisplayName && !p_ChatMessage.GetFrom()->User->DisplayName->IsEmpty())
				hc.m_FirstLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_1, _YLOC("From:")).c_str(), *p_ChatMessage.GetFrom()->User->DisplayName });
			else if (p_ChatMessage.GetFrom()->User->Email)
				hc.m_FirstLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_1, _YLOC("From:")).c_str(), *p_ChatMessage.GetFrom()->User->Email });
		}
		else if (p_ChatMessage.GetFrom()->Application)
		{
			if (p_ChatMessage.GetFrom()->Application->DisplayName && !p_ChatMessage.GetFrom()->Application->DisplayName->IsEmpty())
				hc.m_FirstLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_1, _YLOC("From:")).c_str(), *p_ChatMessage.GetFrom()->Application->DisplayName });
			else if (p_ChatMessage.GetFrom()->Application->Email)
				hc.m_FirstLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_1, _YLOC("From:")).c_str(), *p_ChatMessage.GetFrom()->Application->Email });
		}
		else if (p_ChatMessage.GetFrom()->Device)
		{
			if (p_ChatMessage.GetFrom()->Device->DisplayName && !p_ChatMessage.GetFrom()->Device->DisplayName->IsEmpty())
				hc.m_FirstLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_1, _YLOC("From:")).c_str(), *p_ChatMessage.GetFrom()->Device->DisplayName });
			else if (p_ChatMessage.GetFrom()->Device->Email)
				hc.m_FirstLine.push_back({ YtriaTranslate::Do(ModuleUtil_GetDlgContentViewerHeaderConfig_1, _YLOC("From:")).c_str(), *p_ChatMessage.GetFrom()->Device->Email });
		}
	}

	if (p_ChatMessage.GetCreatedDateTime())
	{
		CString dateStr;
		TimeUtil::GetInstance().ConvertDateToText(DateTimeFormat(), *p_ChatMessage.GetCreatedDateTime(), dateStr);
		ASSERT(!dateStr.IsEmpty());
		hc.m_FirstLine.push_back({ _YTEXT("Created On:"), (LPCTSTR)dateStr });
	}

	if (p_ChatMessage.GetSubject())
		hc.m_Subject = *p_ChatMessage.GetSubject();

	return hc;
}

void ModuleUtil::ClearModifiedRowsStatus(O365Grid& p_Grid)
{
	O365Grid::RowStatusHandler<Scope::O365> o365Handler(p_Grid);

	if (nullptr != p_Grid.GetSecondaryStatusColumn())
	{
		O365Grid::RowStatusHandler<Scope::ONPREM> onPremHandler(p_Grid);
		for (auto row : p_Grid.GetBackendRows())
		{
			if (o365Handler.IsDeleted(row) || o365Handler.IsModified(row))
				o365Handler.ClearStatus(row);
			if (onPremHandler.IsDeleted(row) || onPremHandler.IsModified(row))
				onPremHandler.ClearStatus(row);
		}
	}
	else
	{
		for (auto row : p_Grid.GetBackendRows())
		{
			if (o365Handler.IsDeleted(row) || o365Handler.IsModified(row))
				o365Handler.ClearStatus(row);
		}
	}
}

void ModuleUtil::ClearErrorRowsStatus(O365Grid& p_Grid)
{
	O365Grid::RowStatusHandler<Scope::O365> o365Handler(p_Grid);
	if (nullptr != p_Grid.GetSecondaryStatusColumn())
	{
		O365Grid::RowStatusHandler<Scope::ONPREM> onPremHandler(p_Grid);
		for (auto row : p_Grid.GetBackendRows())
		{
			if (o365Handler.IsError(row))
				o365Handler.ClearStatus(row);
			if (onPremHandler.IsError(row))
				onPremHandler.ClearStatus(row);
		}
	}
	else
	{
		for (auto row : p_Grid.GetBackendRows())
		{
			if (o365Handler.IsError(row))
				o365Handler.ClearStatus(row);
		}
	}
}

void ModuleUtil::MergeAttachments(ModuleBase::AttachmentsContainer& p_OldAttachments, const ModuleBase::AttachmentsContainer& p_NewAttachments)
{
    for (const auto& keyVal : p_NewAttachments)
    {
        const auto& userOrGroupId = keyVal.first;
        const auto& newAttachmentsMap = keyVal.second;

        auto& oldAttachmentsMap = p_OldAttachments[userOrGroupId];
        for (const auto& newAttachmentKeyVal : newAttachmentsMap)
            oldAttachmentsMap[newAttachmentKeyVal.first] = newAttachmentKeyVal.second;
    }
}

std::vector<BusinessAttachment> ModuleUtil::SortAttachments(const std::vector<BusinessAttachment>& p_Attachments)
{
    vector<BusinessAttachment> sorted;

    // To be optimized with just one loop
    for (const auto& attachment : p_Attachments)
    {
        if (attachment.GetError().is_initialized() || (attachment.GetIsInline().is_initialized() && !*attachment.GetIsInline()))
            sorted.push_back(attachment);
    }
    for (const auto& attachment : p_Attachments)
        if (attachment.GetIsInline().is_initialized() && *attachment.GetIsInline())
            sorted.push_back(attachment);

    return sorted;
}

bool ModuleUtil::WarnIfPowerShellHostIncompatible(CWnd* p_Window)
{
	bool compatible = BasicPowerShellSession::IsHostCompatible();
	if (!compatible)
	{
		YCallbackMessage::DoPost([sourceCWnd = p_Window]() {
			YCodeJockMessageBox confirmation(
				sourceCWnd,
				DlgMessageBox::eIcon_ExclamationWarning,
				_T("PowerShell error"),
				_T("This module requires PowerShell 5.1 or later. Please make sure it is installed before using this module."),
				_YTEXT(""),
				{ { IDOK, _T("Ok") } });
			confirmation.DoModal();
		});
		compatible = false;
	}

	return compatible;
}

bool ModuleUtil::AskUserPowerShellAuth(const std::shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent)
{
	if (!p_Session->HasExchangePowerShellSession())
	{
		DlgRestrictedAccess dlg(DlgRestrictedAccess::Image::POWERSHELL_WARNING, p_Parent, 60, 60);
		dlg.SetOkButtonText(_T("Continue"));
		dlg.SetCancelButtonText(_T("Cancel"));
		dlg.SetTitle(_T("PowerShell authentication required"));
		dlg.SetText(_T("Retrieving this information requires access to PowerShell. When prompted, please login with the same user that you are using in your sapio365 session.\n\nDo you want to continue?"));
		dlg.HideNoShowCheckbox();

		return dlg.DoModal() == IDOK;
	}

	return true;
}

// =================================================================================================

bool ModuleUtil::PostMetaInfo::operator<(const PostMetaInfo& p_Other) const
{
	return std::tie(GroupId, ThreadId, ConversationId, ConversationTopic, PostId)
		< std::tie(p_Other.GroupId, p_Other.ThreadId, p_Other.ConversationId, p_Other.ConversationTopic, p_Other.PostId);
}

bool ModuleUtil::PostMetaInfo::operator==(const PostMetaInfo& p_Other) const
{
	return std::tie(GroupId, ThreadId, ConversationId, ConversationTopic, PostId)
		== std::tie(p_Other.GroupId, p_Other.ThreadId, p_Other.ConversationId, p_Other.ConversationTopic, p_Other.PostId);
}
