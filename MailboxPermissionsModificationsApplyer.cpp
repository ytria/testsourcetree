#include "MailboxPermissionsModificationsApplyer.h"
#include "GridMailboxPermissions.h"
#include "MailboxPermissionsUpdaterCommand.h"
#include "FrameMailboxPermissions.h"

MailboxPermissionsModificationsApplyer::MailboxPermissionsModificationsApplyer(GridMailboxPermissions& p_Grid)
	: m_Grid(p_Grid),
	m_Confirm(true),
	m_RefreshAfterApply(true)
{
}

void MailboxPermissionsModificationsApplyer::ApplyAll()
{
	auto actionCmd = std::make_shared<MailboxPermissionsUpdaterCommand>(*m_Grid.m_Frame);
	if (m_RefreshAfterApply)
	{
		actionCmd->SetCallback([this, frame = m_Grid.m_Frame]() {
			frame->RefreshIdsSpecific(GetRefreshData(false), frame->GetAutomationAction());
		});
	}

	if (!m_Confirm || (m_Confirm && ModuleUtil::ConfirmApply(*m_Grid.m_Frame)))
	{
		for (auto& mod : m_Grid.m_MailboxPermEditMods)
			actionCmd->AddEdition(mod.second);
		for (auto& mod : m_Grid.m_MailboxPermAddMods)
			actionCmd->AddAddition(mod.second);
		for (auto& mod : m_Grid.m_MailboxPermRemoveMods)
			actionCmd->AddRemoval(mod.second);
		
		CommandDispatcher::GetInstance().Execute(Command::CreateCommand(actionCmd, *m_Grid.m_Frame, Command::ModuleTarget::MailboxPermissions));
	}
}

void MailboxPermissionsModificationsApplyer::ApplySelected()
{
	auto actionCmd = std::make_shared<MailboxPermissionsUpdaterCommand>(*m_Grid.m_Frame);
	if (m_RefreshAfterApply)
	{
		actionCmd->SetCallback([frame = m_Grid.m_Frame]() {
			frame->RefreshIdsSpecific(frame->GetRefreshSelectedData(), frame->GetAutomationAction());
		});
	}

	if (!m_Confirm || (m_Confirm && ModuleUtil::ConfirmApply(*m_Grid.m_Frame)))
	{
		vector<GridBackendRow*> selectedRows;
		m_Grid.GetSelectedNonGroupRows(selectedRows);

		for (auto itt = m_Grid.m_MailboxPermEditMods.begin(); itt != m_Grid.m_MailboxPermEditMods.end(); ++itt)
			if (IsSelected(itt->first))
				actionCmd->AddEdition(itt->second);

		for (auto itt = m_Grid.m_MailboxPermAddMods.begin(); itt != m_Grid.m_MailboxPermAddMods.end(); ++itt)
			if (IsSelected(itt->first))
				actionCmd->AddAddition(itt->second);

		for (auto itt = m_Grid.m_MailboxPermRemoveMods.begin(); itt != m_Grid.m_MailboxPermRemoveMods.end(); ++itt)
			if (IsSelected(itt->first))
				actionCmd->AddRemoval(itt->second);
		
		CommandDispatcher::GetInstance().Execute(Command::CreateCommand(actionCmd, *m_Grid.m_Frame, Command::ModuleTarget::MailboxPermissions));
	}
}

void MailboxPermissionsModificationsApplyer::SetConfirm(bool p_Confirm)
{
	m_Confirm = p_Confirm;
}

void MailboxPermissionsModificationsApplyer::SetRefreshAfterApply(bool p_Refresh)
{
	m_RefreshAfterApply = p_Refresh;
}

bool MailboxPermissionsModificationsApplyer::IsSelected(const row_pk_t& p_Pk)
{
	bool isSelected = false;
	auto row = m_Grid.GetRowIndex().GetExistingRow(p_Pk);
	ASSERT(nullptr != row);
	if (nullptr != row)
		isSelected = row->IsSelected();
	return isSelected;
}

RefreshSpecificData MailboxPermissionsModificationsApplyer::GetRefreshData(bool p_SelectedOnly) const
{
	RefreshSpecificData data;
	data.m_SelectedRowsAncestors.emplace();

	auto addToRefreshData = [this, &data, p_SelectedOnly](const row_pk_t& p_RowPk) {
		auto row = m_Grid.GetRowIndex().GetExistingRow(p_RowPk);
		if (nullptr != row)
		{
			if (!row->IsGroupRow() && (!p_SelectedOnly || (p_SelectedOnly && row->IsSelected())))
				data.m_SelectedRowsAncestors->insert(row->GetTopAncestorOrThis());
		}
	};

	for (const auto& editions : m_Grid.m_MailboxPermEditMods)
		addToRefreshData(editions.second.GetRowKey());
	for (const auto& additions : m_Grid.m_MailboxPermAddMods)
		addToRefreshData(additions.second.GetRowKey());
	for (const auto& removals : m_Grid.m_MailboxPermRemoveMods)
		addToRefreshData(removals.second.GetRowKey());

	data.m_Criteria = m_Grid.GetModuleCriteriaFromTopRows(std::vector<GridBackendRow*>(data.m_SelectedRowsAncestors->begin(), data.m_SelectedRowsAncestors->end()));

	return data;
}
