#include "GridSignIns.h"

#include "AutomationWizardSignIns.h"
#include "FrameSignIns.h"
#include "BasicGridSetup.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"
#include "DlgSigninsModuleOptions.h"

wstring GridSignIns::g_Success;
wstring GridSignIns::g_Error;

BEGIN_MESSAGE_MAP(GridSignIns, ModuleO365Grid<SignIn>)
	ON_COMMAND(ID_SIGNINGRID_CHANGEOPTIONS, OnChangeOptions)
	ON_UPDATE_COMMAND_UI(ID_SIGNINGRID_CHANGEOPTIONS, OnUpdateChangeOptions)
END_MESSAGE_MAP()

GridSignIns::GridSignIns()
	: m_Frame(nullptr)
	, m_ColAppDisplayName(nullptr)
	, m_ColAppId(nullptr)
	, m_ColAppliedConditionalAccessPolicyDisplayName(nullptr)
	, m_ColAppliedConditionalAccessPolicyEnforcedGrantControls(nullptr)
	, m_ColAppliedConditionalAccessPolicyEnforcedSessionControls(nullptr)
	, m_ColAppliedConditionalAccessPolicyId(nullptr)
	, m_ColAppliedConditionalAccessPolicyResult(nullptr)
	, m_ColClientAppUsed(nullptr)
	, m_ColConditionalAccessStatus(nullptr)
	, m_ColCorrelationId(nullptr)
	, m_ColCreatedDateTime(nullptr)
	, m_ColDeviceDetailBrowser(nullptr)
	, m_ColDeviceDetailDeviceId(nullptr)
	, m_ColDeviceDetailDisplayName(nullptr)
	, m_ColDeviceDetailIsCompliant(nullptr)
	, m_ColDeviceDetailIsManaged(nullptr)
	, m_ColDeviceDetailOperatingSystem(nullptr)
	, m_ColDeviceDetailTrustType(nullptr)
	, m_ColIpAddress(nullptr)
	, m_ColIsInteractive(nullptr)
	, m_ColLocationCity(nullptr)
	, m_ColLocationCountryOrRegion(nullptr)
	, m_ColLocationGeoCoordinatesAltitude(nullptr)
	, m_ColLocationGeoCoordinatesLatitude(nullptr)
	, m_ColLocationGeoCoordinatesLongitude(nullptr)
	, m_ColLocationState(nullptr)
	, m_ColResourceDisplayName(nullptr)
	, m_ColResourceId(nullptr)
	, m_ColRiskDetail(nullptr)
	, m_ColRiskEventTypes(nullptr)
	, m_ColRiskLevelAggregated(nullptr)
	, m_ColRiskLevelDuringSignIn(nullptr)
	, m_ColRiskState(nullptr)
	, m_ColStatusAdditionalDetails(nullptr)
	, m_ColStatusErrorCode(nullptr)
	, m_ColStatusFailureReason(nullptr)
	, m_ColUserDisplayName(nullptr)
	, m_ColUserId(nullptr)
	, m_ColUserPrincipalName(nullptr)
{
	if (g_Success.empty())
		g_Success = YtriaTranslate::Do(DlgRegConnectionCheck_successState_1, _YLOC("Success")).c_str();
	if (g_Error.empty())
		g_Error = YtriaTranslate::Do(DlgError_DlgError_1, _YLOC("Error")).c_str();

	UseDefaultActions(0);
	initWizard<AutomationWizardSignIns>(_YTEXT("Automation\\SignIns"));
}

GridSignIns::~GridSignIns()
{
	GetBackend().Clear();
}

void GridSignIns::BuildView(const vector<SignIn>& p_SignIns, bool p_FullPurge, bool p_NoPurge)
{
	if (p_SignIns.size() == 1 && p_SignIns[0].GetError() != boost::none && p_SignIns[0].GetID().IsEmpty())
	{
		HandlePostUpdateError(p_SignIns[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_SignIns[0].GetError()->GetFullErrorMessage());
	}
	else
	{
		GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
			| (p_NoPurge ? 0 : (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE))
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			| GridUpdaterOptions::REFRESH_PROCESS_DATA
		);
		GridUpdater updater(*this, options);

		for (const auto& signIn : p_SignIns)
		{
			GridBackendField fID(signIn.GetID(), GetColId());
			GridBackendRow* row = m_RowIndex.GetRow({ fID }, true, true);

			SetRowObjectType(row, signIn);
			FillSignInFields(row, signIn);
		}
	}
}

void GridSignIns::FillSignInFields(GridBackendRow* p_Row, const SignIn& p_SignIn)
{
	p_Row->AddField(p_SignIn.m_AppDisplayName, m_ColAppDisplayName);
	p_Row->AddField(p_SignIn.m_AppId, m_ColAppId);
	if (p_SignIn.m_AppliedConditionalAccessPolicy != boost::none)
	{
		const auto& policy = p_SignIn.m_AppliedConditionalAccessPolicy;

		p_Row->AddField(policy->m_DisplayName, m_ColAppliedConditionalAccessPolicyDisplayName);
		p_Row->AddField(policy->m_EnforcedGrantControls, m_ColAppliedConditionalAccessPolicyEnforcedGrantControls);
		p_Row->AddField(policy->m_EnforcedSessionControls, m_ColAppliedConditionalAccessPolicyEnforcedSessionControls);
		p_Row->AddField(policy->m_Id, m_ColAppliedConditionalAccessPolicyId);
		p_Row->AddField(policy->m_Result, m_ColAppliedConditionalAccessPolicyResult);
	}
	p_Row->AddField(p_SignIn.m_ClientAppUsed, m_ColClientAppUsed);
	p_Row->AddField(p_SignIn.m_ConditionalAccessStatus, m_ColConditionalAccessStatus);
	p_Row->AddField(p_SignIn.m_CorrelationId, m_ColCorrelationId);
	p_Row->AddField(p_SignIn.m_CreatedDateTime, m_ColCreatedDateTime);
	if (p_SignIn.m_DeviceDetail != boost::none)
	{
		const auto& detail = p_SignIn.m_DeviceDetail;
		p_Row->AddField(detail->m_Browser, m_ColDeviceDetailBrowser);
		p_Row->AddField(detail->m_DeviceId, m_ColDeviceDetailDeviceId);
		p_Row->AddField(detail->m_DisplayName, m_ColDeviceDetailDisplayName);
		p_Row->AddField(detail->m_IsCompliant, m_ColDeviceDetailIsCompliant);
		p_Row->AddField(detail->m_IsManaged, m_ColDeviceDetailIsManaged);
		p_Row->AddField(detail->m_OperatingSystem, m_ColDeviceDetailOperatingSystem);
		p_Row->AddField(detail->m_TrustType, m_ColDeviceDetailTrustType);
	}
	p_Row->AddField(p_SignIn.m_IpAddress, m_ColIpAddress);
	p_Row->AddField(p_SignIn.m_IsInteractive, m_ColIsInteractive);

	if (p_SignIn.m_Location != boost::none)
	{
		const auto& location = p_SignIn.m_Location;
		if (location->m_GeoCoordinates != boost::none)
		{
			const auto& geo = location->m_GeoCoordinates;
			p_Row->AddField(geo->Altitude, m_ColLocationGeoCoordinatesAltitude);
			double lat = geo->Latitude ? *geo->Latitude : 0.0;
			p_Row->AddField(geo->Latitude, m_ColLocationGeoCoordinatesLatitude);
			p_Row->AddField(geo->Longitude, m_ColLocationGeoCoordinatesLongitude);
		}
		p_Row->AddField(location->m_City, m_ColLocationCity);
		p_Row->AddField(location->m_CountryOrRegion, m_ColLocationCountryOrRegion);
		p_Row->AddField(location->m_State, m_ColLocationState);
	}
	p_Row->AddField(p_SignIn.m_ResourceDisplayName, m_ColResourceDisplayName);
	p_Row->AddField(p_SignIn.m_ResourceId, m_ColResourceId);
	p_Row->AddField(p_SignIn.m_RiskDetail, m_ColRiskDetail);
	p_Row->AddField(p_SignIn.m_RiskEventTypes, m_ColRiskEventTypes);
	p_Row->AddField(p_SignIn.m_RiskLevelAggregated, m_ColRiskLevelAggregated);
	p_Row->AddField(p_SignIn.m_RiskLevelDuringSignIn, m_ColRiskLevelDuringSignIn);
	p_Row->AddField(p_SignIn.m_RiskState, m_ColRiskState);
	if (p_SignIn.m_Status != boost::none)
	{
		const auto& status = p_SignIn.m_Status;
		p_Row->AddField(status->m_AdditionalDetails, m_ColStatusAdditionalDetails);

		if (0 == status->m_ErrorCode)
			p_Row->AddField(g_Success, m_ColStatusError);
		else
			p_Row->AddField(g_Error, m_ColStatusError);

		p_Row->AddField(status->m_ErrorCode, m_ColStatusErrorCode);
		p_Row->AddField(status->m_FailureReason, m_ColStatusFailureReason);
	}
	p_Row->AddField(p_SignIn.m_UserDisplayName, m_ColUserDisplayName);
	p_Row->AddField(p_SignIn.m_UserId, m_ColUserId);
	p_Row->AddField(p_SignIn.m_UserPrincipalName, m_ColUserPrincipalName);
}

wstring GridSignIns::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { { _T("User"), m_ColUserPrincipalName}, { _T("Application"), m_ColAppDisplayName } }, m_ColCreatedDateTime);
}

O365Grid::SnapshotCaps GridSignIns::GetSnapshotCapabilities() const
{
	// This module has no refresh/edit capabilities, Restore Point is useless.
	return { true, false };
}

void GridSignIns::customizeGrid()
{
	m_Frame = dynamic_cast<FrameSignIns*>(GetParentFrame());

	{
		BasicGridSetup setup(GridUtil::g_AutoNameSignIns, LicenseUtil::g_codeSapio365signIns/*,
			{
				{ &m_ColMetaDisplayName, g_TitleOwnerDisplayName, g_FamilyOwner },
				{ &m_ColMetaUserPrincipalName, g_TitleOwnerUserName, g_FamilyOwner },
				{ &m_ColMetaId, g_TitleOwnerID, g_FamilyOwner }
			}*/);
		setup.Setup(*this, false);
	}

	addColumnGraphID();
	m_ColCreatedDateTime = AddColumnDate(_YUID("createdDateTime"), YtriaTranslate::Do(GridSignIns_customizeGrid_11, _YLOC("Date")).c_str(), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColUserDisplayName = AddColumn(_YUID("userDisplayName"), YtriaTranslate::Do(GridSignIns_customizeGrid_36, _YLOC("User display name")).c_str(), g_FamilyInfo, g_ColumnSize22);
	m_ColUserPrincipalName = AddColumn(_YUID("userPrincipalName"), YtriaTranslate::Do(GridSignIns_customizeGrid_38, _YLOC("Username")).c_str(), g_FamilyInfo, g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColUserId = AddColumn(_YUID("userId"), YtriaTranslate::Do(GridSignIns_customizeGrid_37, _YLOC("User ID")).c_str(), g_FamilyInfo, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColAppDisplayName = AddColumn(_YUID("appDisplayName"), YtriaTranslate::Do(GridSignIns_customizeGrid_1, _YLOC("Application")).c_str(), g_FamilyInfo, g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColAppId = AddColumn(_YUID("appId"), YtriaTranslate::Do(GridSignIns_customizeGrid_2, _YLOC("Application ID")).c_str(), g_FamilyInfo, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColIpAddress = AddColumn(_YUID("ipAddress"), YtriaTranslate::Do(GridSignIns_customizeGrid_19, _YLOC("IP Address")).c_str(), g_FamilyInfo, g_ColumnSize12);

	{
		const wstring familyLocation = YtriaTranslate::Do(GridEvents_customizeGrid_39, _YLOC("Location")).c_str();
		m_ColLocationCity = AddColumn(_YUID("location.city"), YtriaTranslate::Do(GridSignIns_customizeGrid_21, _YLOC("City")).c_str(), familyLocation, g_ColumnSize12, { g_ColumnsPresetDefault });
		m_ColLocationState = AddColumn(_YUID("location.state"), YtriaTranslate::Do(GridSignIns_customizeGrid_26, _YLOC("State")).c_str(), familyLocation, g_ColumnSize12);
		m_ColLocationCountryOrRegion = AddColumn(_YUID("location.countryOrRegion"), YtriaTranslate::Do(GridSignIns_customizeGrid_22, _YLOC("Country")).c_str(), familyLocation, g_ColumnSize12);
		m_ColLocationGeoCoordinatesLatitude = AddColumnNumber(_YUID("location.geoCoordinates.latitude"), YtriaTranslate::Do(GridSignIns_customizeGrid_24, _YLOC("Latitude")).c_str(), familyLocation, g_ColumnSize16);
		m_ColLocationGeoCoordinatesLatitude->SetNumberFormatDouble(NumberFormat::g_DecimalLimit);
		m_ColLocationGeoCoordinatesLongitude = AddColumnNumber(_YUID("location.geoCoordinates.longitude"), YtriaTranslate::Do(GridSignIns_customizeGrid_25, _YLOC("Longitude")).c_str(), familyLocation, g_ColumnSize16);
		m_ColLocationGeoCoordinatesLongitude->SetNumberFormatDouble(NumberFormat::g_DecimalLimit);
		m_ColLocationGeoCoordinatesAltitude = AddColumnNumber(_YUID("location.geoCoordinatesaltitude"), YtriaTranslate::Do(GridSignIns_customizeGrid_23, _YLOC("Altitude")).c_str(), familyLocation, g_ColumnSize16);
		m_ColLocationGeoCoordinatesAltitude->SetNumberFormatDouble(NumberFormat::g_DecimalLimit);
	}

	m_ColClientAppUsed = AddColumn(_YUID("clientAppUsed"), YtriaTranslate::Do(GridSignIns_customizeGrid_8, _YLOC("Client app")).c_str(), g_FamilyInfo, g_ColumnSize12);
	m_ColIsInteractive = AddColumnCheckBox(_YUID("isInteractive"), YtriaTranslate::Do(GridSignIns_customizeGrid_20, _YLOC("Interactive")).c_str(), g_FamilyInfo, g_ColumnSize6);
	m_ColCorrelationId = AddColumn(_YUID("correlationId"), YtriaTranslate::Do(GridSignIns_customizeGrid_10, _YLOC("Request ID sent")).c_str(), g_FamilyInfo, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColResourceDisplayName = AddColumn(_YUID("resourceDisplayName"), YtriaTranslate::Do(GridSignIns_customizeGrid_27, _YLOC("Resource")).c_str(), g_FamilyInfo, g_ColumnSize22);
	m_ColResourceId = AddColumn(_YUID("resourceId"), YtriaTranslate::Do(GridSignIns_customizeGrid_28, _YLOC("Resource ID")).c_str(), g_FamilyInfo, g_ColumnSize12, { g_ColumnsPresetTech });

	{
		const wstring familyStatus = YtriaTranslate::Do(GridSignIns_customizeGrid_34, _YLOC("Status")).c_str();
		m_ColStatusError = AddColumn(_YUID("status.error"), YtriaTranslate::Do(GridSignIns_customizeGrid_34, _YLOC("Status error")).c_str(), familyStatus, g_ColumnSize8, { g_ColumnsPresetDefault });
		m_ColStatusError->SetFlag(GridBackendUtil::IGNOREINSNAPSHOT); // m_ColStatusErrorCode is enough
		m_ColStatusErrorCode = AddColumnNumber(_YUID("status.errorCode"), YtriaTranslate::Do(GridSignIns_customizeGrid_42, _YLOC("Status error code")).c_str(), familyStatus, g_ColumnSize8, { g_ColumnsPresetTech });
		m_ColStatusFailureReason = AddColumn(_YUID("status.failureReason"), YtriaTranslate::Do(GridSignIns_customizeGrid_35, _YLOC("Failure reason of status")).c_str(), familyStatus, g_ColumnSize22, { g_ColumnsPresetDefault });
		m_ColStatusAdditionalDetails = AddColumn(_YUID("status.additionalDetails"), YtriaTranslate::Do(GridSignIns_customizeGrid_33, _YLOC("Additional details of status")).c_str(), familyStatus, g_ColumnSize22, { g_ColumnsPresetDefault });
	}

	{
		const wstring familyRisk = YtriaTranslate::Do(GridSignIns_customizeGrid_31, _YLOC("Risk")).c_str();
		m_ColRiskState = AddColumn(_YUID("riskState"), YtriaTranslate::Do(GridSignIns_customizeGrid_32, _YLOC("Risk status")).c_str(), familyRisk, g_ColumnSize12, { g_ColumnsPresetDefault });
		m_ColRiskDetail = AddColumn(_YUID("riskDetail"), YtriaTranslate::Do(GridSignIns_customizeGrid_29, _YLOC("Risk status detail")).c_str(), familyRisk, g_ColumnSize12);
		m_ColRiskLevelAggregated = AddColumn(_YUID("riskLevelAggregated"), YtriaTranslate::Do(GridSignIns_customizeGrid_39, _YLOC("Aggregated risk level")).c_str(), familyRisk, g_ColumnSize8);
		m_ColRiskLevelDuringSignIn = AddColumn(_YUID("riskLevelDuringSignIn"), YtriaTranslate::Do(GridSignIns_customizeGrid_31, _YLOC("Risk level during sign-in")).c_str(), familyRisk, g_ColumnSize8);
		m_ColRiskEventTypes = AddColumn(_YUID("riskEventTypes"), YtriaTranslate::Do(GridSignIns_customizeGrid_30, _YLOC("Risk event types")).c_str(), familyRisk, g_ColumnSize12);
	}

	{
		const wstring familyDevice = YtriaTranslate::Do(GridSignIns_customizeGrid_14, _YLOC("Device")).c_str();
		m_ColDeviceDetailDisplayName = AddColumn(_YUID("deviceDetail.displayName"), YtriaTranslate::Do(GridSignIns_customizeGrid_14, _YLOC("Device")).c_str(), familyDevice, g_ColumnSize12);
		m_ColDeviceDetailDeviceId = AddColumn(_YUID("deviceDetail.deviceId"), YtriaTranslate::Do(GridSignIns_customizeGrid_13, _YLOC("Device ID")).c_str(), familyDevice, g_ColumnSize12, { g_ColumnsPresetTech });
		m_ColDeviceDetailOperatingSystem = AddColumn(_YUID("deviceDetail.operatingSystem"), YtriaTranslate::Do(GridSignIns_customizeGrid_17, _YLOC("Operating system")).c_str(), familyDevice, g_ColumnSize12);
		m_ColDeviceDetailBrowser = AddColumn(_YUID("deviceDetail.browser"), YtriaTranslate::Do(GridSignIns_customizeGrid_12, _YLOC("Browser")).c_str(), familyDevice, g_ColumnSize12);
		m_ColDeviceDetailIsCompliant = AddColumnCheckBox(_YUID("deviceDetail.isCompliant"), YtriaTranslate::Do(GridSignIns_customizeGrid_15, _YLOC("Compliant device")).c_str(), familyDevice, g_ColumnSize6);
		m_ColDeviceDetailIsManaged = AddColumnCheckBox(_YUID("deviceDetail.isManaged"), YtriaTranslate::Do(GridSignIns_customizeGrid_16, _YLOC("Managed device")).c_str(), familyDevice, g_ColumnSize6);
		m_ColDeviceDetailTrustType = AddColumn(_YUID("deviceDetail.trustType"), YtriaTranslate::Do(GridSignIns_customizeGrid_18, _YLOC("Device trust type")).c_str(), familyDevice, g_ColumnSize12);
	}

	m_ColConditionalAccessStatus = AddColumn(_YUID("conditionalAccessStatus"), YtriaTranslate::Do(GridSignIns_customizeGrid_9, _YLOC("Conditional Access")).c_str(), g_FamilyInfo, g_ColumnSize12, { g_ColumnsPresetDefault });

	{
		const wstring familyConditionalAccessPolicies = YtriaTranslate::Do(GridSignIns_customizeGrid_45, _YLOC("Conditional access policies")).c_str();
		m_ColAppliedConditionalAccessPolicyId = AddColumn(_YUID("appliedConditionalAccessPolicies.id"), YtriaTranslate::Do(GridSignIns_customizeGrid_6, _YLOC("Conditional access policy ID")).c_str(), familyConditionalAccessPolicies, g_ColumnSize12, { g_ColumnsPresetTech });
		
		m_ColAppliedConditionalAccessPolicyDisplayName = AddColumn(_YUID("appliedConditionalAccessPolicies.displayName"), YtriaTranslate::Do(GridSignIns_customizeGrid_3, _YLOC("Conditional access policy")).c_str(), familyConditionalAccessPolicies, g_ColumnSize12);
		
		m_ColAppliedConditionalAccessPolicyEnforcedGrantControls = AddColumn(_YUID("appliedConditionalAccessPolicies.enforcedGrantControls"), YtriaTranslate::Do(GridSignIns_customizeGrid_4, _YLOC("Policy enforced grant control")).c_str(), familyConditionalAccessPolicies, g_ColumnSize12);
		m_ColAppliedConditionalAccessPolicyEnforcedSessionControls = AddColumn(_YUID("appliedConditionalAccessPolicies.EnforcedSessionControls"), YtriaTranslate::Do(GridSignIns_customizeGrid_5, _YLOC("Policy enforced session controls")).c_str(), familyConditionalAccessPolicies, g_ColumnSize12);
		m_ColAppliedConditionalAccessPolicyResult = AddColumn(_YUID("appliedConditionalAccessPolicies.result"), YtriaTranslate::Do(GridSignIns_customizeGrid_7, _YLOC("Policy result")).c_str(), familyConditionalAccessPolicies, g_ColumnSize12);
	}

	AddColumnForRowPK(GetColId());

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameSignIns*>(GetParentFrame()));
}

SignIn GridSignIns::getBusinessObject(GridBackendRow*) const
{
	return SignIn();
}

void GridSignIns::UpdateBusinessObjects(const vector<SignIn>& p_SignIns, bool p_SetModifiedStatus)
{

}

void GridSignIns::RemoveBusinessObjects(const vector<SignIn>& p_SignIns)
{

}

bool GridSignIns::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	return ModuleO365Grid<SignIn>::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridSignIns::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (&p_Column == m_ColStatusErrorCode)
	{
		if (Str::getIntFromString(p_Value) == 0)
			p_Row.AddField(g_Success, m_ColStatusError);
		else
			p_Row.AddField(g_Error, m_ColStatusError);
	}

	return ModuleO365Grid<SignIn>::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

void GridSignIns::OnChangeOptions()
{
	if (hasNoTaskRunning() && IsFrameConnected())
	{
		ASSERT(m_Frame->GetOptions());
		DlgSigninsModuleOptions dlgOptions(m_Frame, GetSession(), m_Frame->GetOptions() ? *m_Frame->GetOptions() : ModuleOptions());
		if (dlgOptions.DoModal() == IDOK)
		{
			CommandInfo info;
			info.SetFrame(m_Frame);
			info.SetOrigin(GetOrigin());
			info.SetRBACPrivilege(m_Frame->GetModuleCriteria().m_Privilege);
			info.Data2() = dlgOptions.GetOptions();

			Command command(m_Frame->GetLicenseContext(), m_Frame->GetSafeHwnd(), Command::ModuleTarget::SignIns, Command::ModuleTask::List, info, { this, g_ActionNameRefreshAll, GetAutomationActionRecording(), nullptr });
			CommandDispatcher::GetInstance().Execute(command);
		}
	}
}

void GridSignIns::OnUpdateChangeOptions(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridSignIns::InitializeCommands()
{
	ModuleO365Grid<SignIn>::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_SIGNINGRID_CHANGEOPTIONS;
		_cmd.m_sMenuText = _T("Change Options");
		_cmd.m_sToolbarText = _T("Change Options");
		_cmd.m_sTipTool = _T("Adjust cut-off date and filter.");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANGEMODULEOPTIONS_16x16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_SIGNINGRID_CHANGEOPTIONS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Change Options"),
			_T("Adjust cut-off date and filter."),
			_cmd.m_sAccelText);
	}
}

void GridSignIns::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ModuleO365Grid<SignIn>::OnCustomPopupMenu(pPopup, nStart);

	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_SIGNINGRID_CHANGEOPTIONS);
	}
}
