#include "SubscriptionDeserializer.h"

#include "JsonSerializeUtil.h"

void Azure::SubscriptionDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("subscriptionId"), m_Data.m_SubscriptionId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("state"), m_Data.m_State, p_Object);
}
