#pragma once

class PasswordCredential
{
public:
	boost::YOpt<PooledString> m_CustomKeyIdentifier;
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<YTimeDate> m_EndDateTime;
	boost::YOpt<PooledString> m_KeyId;
	boost::YOpt<YTimeDate> m_StartDateTime;
	boost::YOpt<PooledString> m_SecretText;
	boost::YOpt<PooledString> m_Hint;
};

