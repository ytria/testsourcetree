#include "ModuleBase.h"

#include "AutomationDataStructure.h"
#include "AutomationRecording.h"
#include "BaseO365Grid.h"
#include "BusinessObjectManager.h"
#include "DlgContentViewer.h"
#include "FileUtil.h"
#include "GraphCache.h"
#include "GridFrameBase.h"
#include "GridTemplateGroups.h"
#include "GridTemplateUsers.h"
#include "LoggerService.h"
#include "MainFrameSapio365Session.h"
#include "MsGraphFieldNames.h"
#include "MSGraphSession.h"
#include "MultiObjectsPageRequestLogger.h"
#include "MultiObjectsRequestLogger.h"
#include "Office365Admin.h"
#include "PersistentSession.h"
#include "RESTUtils.h"
#include "RunOnScopeEnd.h"
#include "safeTaskCall.h"
#include "TaskDataManager.h"
#include "YCallbackMessage.h"
#include "YDataCallbackMessage.h"
#include "YSafeCreateTask.h"
#include "YtriaTranslate.h"

ModuleBase::ModuleBase(const PooledString& p_Name)
	: m_Name(p_Name)
{
}

ModuleBase::~ModuleBase()
{
}

void ModuleBase::AddGridFrame(GridFrameBase* p_Frame)
{
	m_GridFrames.insert(p_Frame);
}

void ModuleBase::RemoveGridFrame(GridFrameBase* p_Frame)
{
	m_GridFrames.erase(p_Frame);
}

const PooledString& ModuleBase::GetName() const
{
	return m_Name;
}

const std::unordered_set<GridFrameBase*>& ModuleBase::GetGridFrames() const
{
	return m_GridFrames;
}

const PersistentSession& ModuleBase::GetConnectedSession()
{
	auto mainFrame = dynamic_cast<MainFrame*>(::AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		return mainFrame->GetSessionInfo();
	return PersistentSession::g_EmptySession;
}

std::shared_ptr<Sapio365Session> ModuleBase::GetConnectedSapio365Session()
{
	return MainFrameSapio365Session();
}

void ModuleBase::Execute(const Command& p_Command)
{
	ASSERT(!p_Command.GetSessionIdentifier().m_EmailOrAppId.empty());
	auto graphSession = Sapio365Session::Find(p_Command.GetSessionIdentifier());
	ASSERT(graphSession
		&& RestSession::AuthenticationState::Ready == graphSession->GetMainMSGraphSession()->GetAuthenticationState() 
		&& RestSession::AuthenticationState::Ready == graphSession->GetMSGraphSession(Sapio365Session::APP_SESSION)->GetAuthenticationState()
		&& RestSession::AuthenticationState::Ready == graphSession->GetMSGraphSession(Sapio365Session::USER_SESSION)->GetAuthenticationState());

    if (graphSession && RestSession::AuthenticationState::Ready == graphSession->GetMainMSGraphSession()->GetAuthenticationState())
	{
		p_Command.AutomationRecord();// does nothing if not in recording mode
		if (RoleDelegationManager::GetInstance().UpdateSQLiteFromCloudIfLeaseExpiredAndInvalidateDelegation(graphSession, p_Command.GetSourceCWnd()))
		{
			YCallbackMessage::DoSend([graphSession, p_Command]()
			{
				YCodeJockMessageBox dlg(p_Command.GetSourceCWnd(),
										DlgMessageBox::eIcon_ExclamationWarning,
										YtriaTranslate::Do(ModuleBase_Execute_1, _YLOC("Obsolete RBAC delegation in current session")).c_str(),
										YtriaTranslate::Do(ModuleBase_Execute_2, _YLOC("The RBAC delegation associated with the current session was modified.\nThe current session must now logout. All pending modifications are lost.")).c_str(),
										_YTEXT(""),
										{ { IDOK, YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str() } });
				dlg.DoModal();
				Sapio365Session::SignOut(graphSession, p_Command.GetSourceCWnd());
			});
		}
		else
		{
			std::shared_ptr<IActionCommand> actionCmd = p_Command.GetCommandInfo().GetActionCommand();
			if (nullptr != actionCmd)
				actionCmd->Execute();
			else
				executeImpl(p_Command);
		}
	}
}

void ModuleBase::CreateFrameLogger(GridFrameBase* p_GridFrame, const wstring& p_Message)
{
	p_GridFrame->GetGrid().AddLog(0, p_Message.c_str(), false, true);
    p_GridFrame->GetGrid().RedrawWindow();
}

YtriaTaskData ModuleBase::addTaskData(const wstring& p_Name, CFrameWnd* p_Originator)
{
	ASSERT(nullptr != p_Originator);

	wstring sessionName;

	if (p_Originator == ::AfxGetApp()->m_pMainWnd)
	{
		std::shared_ptr<Sapio365Session> session = MainFrameSapio365Session();
		ASSERT(session);
		if (session)
			sessionName = session->GetEmailOrAppId();
	}
	else
	{
		auto frame = dynamic_cast<GridFrameBase*>(p_Originator);
		ASSERT(nullptr != frame);
		if (nullptr != frame)
			sessionName = frame->GetSessionInfo().GetEmailOrAppId();
	}

    return TaskDataManager::Get().AddTaskData(p_Name, sessionName, p_Originator->m_hWnd);
}

bool ModuleBase::ShouldDoListRequests(size_t p_CacheSize, size_t p_NbElementsToRefresh, size_t p_NbElementsPerPage)
{
	ASSERT(0 != p_NbElementsToRefresh);
    const size_t numRequestsForFullList = p_CacheSize / p_NbElementsPerPage + ((p_CacheSize % p_NbElementsPerPage) > 0 ? 1 : 0);
    return p_NbElementsToRefresh > numRequestsForFullList;
}

std::pair<boost::YOpt<SapioError>, std::map<PooledString, boost::YOpt<PooledString>>> ModuleBase::getSubscribedSkus(const std::shared_ptr<BusinessObjectManager>& p_BOM, YtriaTaskData p_TaskData, boost::YOpt<vector<BusinessSubscribedSku>>& p_sskus)
{
	std::pair<boost::YOpt<SapioError>, std::map<PooledString, boost::YOpt<PooledString>>> result;

	vector<BusinessSubscribedSku> localSubscribedSkus;
	auto& subscribedSkus = p_sskus ? *p_sskus : localSubscribedSkus;

	subscribedSkus = std::move(p_BOM->GetSubscribedSkus(p_TaskData).GetTask().get());

	if (subscribedSkus.size() == 1 && subscribedSkus[0].GetError())
	{
		result.first = subscribedSkus[0].GetError();
	}
	else
	{
		auto& sskus = result.second;
		for (auto& sku : subscribedSkus)
		{
			ASSERT(sku.GetSkuId());
			if (sku.GetSkuId())
				sskus.insert({ *sku.GetSkuId(), sku.GetSkuPartNumber() });
		}
	}

	return result;
}

std::pair<boost::YOpt<SapioError>, std::map<PooledString, boost::YOpt<PooledString>>> ModuleBase::getSubscribedSkus(const std::shared_ptr<BusinessObjectManager>& p_BOM, YtriaTaskData p_TaskData)
{
	boost::YOpt<vector<BusinessSubscribedSku>> sskus;
	return getSubscribedSkus(p_BOM, p_TaskData, sskus);
}

void ModuleBase::TaskFinished(GridFrameBase* p_Frame, const YtriaTaskData& taskData, AutomationAction* p_ActionToGreenlight)
{
	if (nullptr != p_Frame)
	{
		p_Frame->TaskFinished(taskData, p_ActionToGreenlight, p_Frame->GetSafeHwnd());
	}
	else
	{
		TaskDataManager::Get().RemoveTaskData(taskData.GetId(), p_Frame->GetSafeHwnd());
		SetAutomationGreenLight(p_ActionToGreenlight);
	}
}

LinearMap<PooledString, PooledString> ModuleBase::GetOriginIdsAndNames(const O365IdsContainer& p_IDs, O365Grid& p_Grid)
{
	LinearMap<PooledString, PooledString> names;

	GridBackendColumn* nameCol = p_Grid.GetColumnByUniqueID(_YUID(O365_METADISPLAYNAME));
	if (nullptr == nameCol)
	{
		ASSERT(strcmp(O365_USER_DISPLAYNAME, O365_GROUP_DISPLAYNAME) == 0);
		nameCol = p_Grid.GetColumnByUniqueID(_YUID(O365_USER_DISPLAYNAME));
	}

	GridBackendColumn* metaIdCol = p_Grid.GetColumnByTitle(O365Grid::g_TitleGraphID);
	ASSERT(nullptr != metaIdCol && nullptr != nameCol);
	if (nullptr != metaIdCol && nullptr != nameCol)
	{
		for (auto& row : p_Grid.GetBackendRows())
		{
			if (p_Grid.IsQueryReferenceRow(row))
			{
				wstring id = row->GetField(metaIdCol).GetValueStr();
				if (p_IDs.find(id) != p_IDs.end())
				{
					const wstring name(row->GetField(nameCol).GetValueStr());
					names.push_back(id, name);
				}
			}
		}
	}

	return names;
}

void ModuleBase::SelectOriginId(CacheGrid& p_Grid, const wstring& p_Id)
{
	// Get Id col
	GridBackendColumn* metaIdCol	= p_Grid.GetColumnByUniqueID(_YUID(O365_METAID));
	GridBackendColumn* idCol		= p_Grid.GetColumnByUniqueID(_YUID(O365_ID));

	ASSERT(nullptr != idCol);
	if (nullptr != idCol)
	{
		for (auto& row : p_Grid.GetBackend().GetBackendRowsForDisplay())
		{
			wstring id(row->GetField(metaIdCol).GetValueStr());
			if (id.empty())
				id = row->GetField(idCol).GetValueStr();

			if (!id.empty() && MFCUtil::StringMatchW(id, p_Id))
			{
				p_Grid.GetBackend().SelectAllRows(false);
				p_Grid.SelectRowAndExpandParentGroups(row);
				p_Grid.ScrollToRow(row);
				break;
			}
		}
	}
}

bool ModuleBase::ShouldBuildView(HWND p_FrameHwnd, bool p_HasBeenCanceled, const YtriaTaskData& p_TaskData, bool p_AskToKeepPartial)
{
	bool result = false;

	if (::IsWindow(p_FrameHwnd))
	{
		if (p_HasBeenCanceled)
		{
			auto frame = dynamic_cast<GridFrameBase*>(CWnd::FromHandle(p_FrameHwnd));
			const auto firstLoad = nullptr != frame && frame->IsFirstLoad();

			if (firstLoad || !p_AskToKeepPartial)
			{
				result = firstLoad;
				if (!result) // Otherwise, module does it.
				{
					frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
					frame->GetGrid().ClearLog(p_TaskData.GetId());
				}
			}
			else if (nullptr != frame)
			{
				frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
				frame->GetGrid().ClearLog(p_TaskData.GetId());
				YCodeJockMessageBox dlg(frame, DlgMessageBox::eIcon_Question
					, /*p_TaskName*/YtriaTranslate::Do(ModuleBase_ShouldBuildView_1, _YLOC("Request Canceled")).c_str()
					, YtriaTranslate::Do(ModuleBase_ShouldBuildView_2, _YLOC("This request has been canceled.\nDo you want to update the grid with the partial results?")).c_str()
					, frame->GetGrid().HasLastQueryColumn()
						? YtriaTranslate::Do(ModuleBase_ShouldBuildView_3, _YLOC("To see which rows have been updated, use the button \"Show 'Last Queried On' Column\" (Options tab).")).c_str()
						: _YTEXT("")
					, { {IDOK, YtriaTranslate::Do(DlgHelpMain_OnInitDialog_5, _YLOC("Update")).c_str() }, { IDCANCEL, YtriaTranslate::Do(DlgViewAdvanced_OnInitDialog_13, _YLOC("Discard")).c_str() } });
				result = IDOK == dlg.DoModal();
			}
		}
		else
		{
			result = true;
		}
	}

	return result;
}

YtriaTaskData ModuleBase::addFrameTask(const wstring& p_LogMessage, GridFrameBase* p_Frame, const Command& p_Command, bool p_IsMotherTask)
{
	ASSERT(nullptr != p_Frame);
	if (nullptr != p_Frame)
	{
		HWND hwnd = p_Frame->GetSafeHwnd();
		auto taskData = addTaskData(p_LogMessage, p_Frame);
		p_Frame->SetTaskData(taskData);
        
        p_Frame->GetGrid().SetProcessText(YtriaTranslate::Do(ModuleBase_addFrameTask_1, _YLOC("Preparing requests...")).c_str());
		// FIXME: This call to RedrawWindow() was probably here so that we can see the above text but doing it seems to fuck grid init (assert due to header not existing in prof-uis)...
        //p_Frame->GetGrid().RedrawWindow();
		
		auto& p_AutomationSetup = p_Command.GetAutomationSetup();
		if (p_IsMotherTask)
			p_Frame->SetMotherAutomationAction(p_AutomationSetup.m_ActionToExecute);
		p_Frame->AutomationRecord(p_AutomationSetup);
		p_Frame->GetGrid().SetAutomationActionRecording(p_AutomationSetup.m_ActionToRecord);

		p_Frame->SetLicenseContext(p_Command.GetLicenseContext());

		return taskData;
	}
	return YtriaTaskData();
}

void ModuleBase::AddCanceledObjects(vector<BusinessGroup>& p_Objects, const O365IdsContainer& p_Ids, GraphCache& p_GraphCache)
{
	auto wantedIds = p_Ids;

	for (const auto& gp : p_Objects)
	{
		auto count = wantedIds.erase(gp.GetID());
		ASSERT(1 == count);
	}

	for (const auto& id : wantedIds)
	{
		auto gp = BusinessGroup(p_GraphCache.GetGroupInCache(id));
		ASSERT(gp.GetID() == id);
		gp.SetID(id);
		p_Objects.emplace_back(gp);
		p_Objects.back().SetFlags(p_Objects.back().GetFlags() | BusinessObject::Flag::CANCELED);
	}
}

void ModuleBase::SetAutomationGreenLight(AutomationAction* p_Action, const wstring& p_ErrorMessage)
{
	if (nullptr != p_Action && !p_Action->IsExecuted())
	{
		ASSERT(AutomatedApp::IsAutomationRunning());
		if (AutomatedApp::IsAutomationRunning())
		{
			Office365AdminApp* app = reinterpret_cast<Office365AdminApp*>(::AfxGetApp());
			ASSERT(nullptr != app);
			if (nullptr != app)
			{
				if (!p_ErrorMessage.empty())
					p_Action->AddError(p_ErrorMessage.c_str());
				app->SetAutomationGreenLight(p_Action);
			}
		}
	}
}

void ModuleBase::SetAutomationGreenLight(AutomationAction* p_Action)
{
	SetAutomationGreenLight(p_Action, wstring());
}

void ModuleBase::SetActionCanceledByUser(AutomationAction* p_Action)
{
	if (nullptr != p_Action && !p_Action->IsExecuted())
	{
		ASSERT(AutomatedApp::IsAutomationRunning());
		if (AutomatedApp::IsAutomationRunning())
		{
			Office365AdminApp* app = reinterpret_cast<Office365AdminApp*>(::AfxGetApp());
			ASSERT(nullptr != app);
			if (nullptr != app)
				app->SetActionCanceledByUser(p_Action); // Module won't open, we have to remove child actions, they won't be executed.
		}
	}
}

void ModuleBase::downloadInlineAttachments(const AttachmentsInfo& p_AttachmentInfos, DownloadInlineAttachmentsCallback* p_Callback, const Command& p_Cmd)
{
    ASSERT(nullptr != p_Callback);
    if (nullptr == p_Callback)
        return;

    const auto& cmdInfo = p_Cmd.GetCommandInfo();
    auto taskData = addFrameTask(YtriaTranslate::Do(ModuleBase_showImagesInViewer_1, _YLOC("Get Attachments Images")).c_str(), cmdInfo.GetFrame(), p_Cmd, false);

	YSafeCreateTask([bom = cmdInfo.GetFrame()->GetBusinessObjectManager(), cmdInfo, p_AttachmentInfos, p_Callback, frameHwnd = p_Cmd.GetCommandInfo().GetFrame()->GetSafeHwnd(), taskData]()
	{
		std::map<AttachmentsInfo::IDs, std::map<wstring, wstring>> idsToCidsToPaths;

		auto basePath = p_AttachmentInfos.m_DownloadFolderName;
		if (basePath.empty()) // If download folder is not specified, use temp directory
			FileUtil::GetYtriaTempDirPath(basePath);

		auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("attachment list"), p_AttachmentInfos.m_AttachmentInfos.size());

		for (const auto& attachmentInfo : p_AttachmentInfos.m_AttachmentInfos)
		{
			// Init empty
			idsToCidsToPaths[attachmentInfo.m_IDs];

			logger->IncrementObjCount();
			logger->SetContextualInfo(attachmentInfo.m_IDs.EventOrMessageOrConversationID());

			std::vector<BusinessAttachment> attachments;
			// Get attachments list
			if (cmdInfo.GetOrigin() == Origin::Message)
				attachments = bom->GetMessageAttachments(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), logger, taskData).GetTask().get();
			else if (cmdInfo.GetOrigin() == Origin::User)
				attachments = bom->GetUserEventAttachments(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), logger, taskData).GetTask().get();
			else if (cmdInfo.GetOrigin() == Origin::Group)
				attachments = bom->GetGroupEventAttachments(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), logger, taskData).GetTask().get();
			else if (cmdInfo.GetOrigin() == Origin::Conversations)
				attachments = bom->GetPostAttachments(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), attachmentInfo.m_IDs.ThreadID(), attachmentInfo.m_IDs.PostID(), logger, taskData).GetTask().get();

			wstring folderPath;
			// For each attachment in the list, download inline file attachments
			for (const auto& attachment : attachments)
			{
				const bool isInline = attachment.GetIsInline() != boost::none && *attachment.GetIsInline();
				if (attachment.GetDataType() && isInline &&
					*attachment.GetDataType() == Util::GetFileAttachmentStr())
				{
					if (folderPath.empty())
					{
						folderPath = basePath;
						if (attachmentInfo.m_ColumnsPath.IsEmpty())
						{
							// Don't use whole entity id in file path, it makes too long filepaths...
							folderPath += wstring(_YTEXT("\\")) + wstring(attachmentInfo.m_IDs.UserOrGroupID()) + wstring(_YTEXT("\\")) + Str::MakeMidEllipsis(attachmentInfo.m_IDs.EventOrMessageOrConversationID());
						}
						else
						{
							folderPath += wstring(_YTEXT("\\")) + attachmentInfo.m_ColumnsPath.c_str();
						}

						folderPath += attachmentInfo.m_FolderNameSuffix.c_str();

						if (!p_AttachmentInfos.m_UseExistingFolder && FileUtil::FileExists(folderPath))
							folderPath = FileUtil::MakeUniqueFilepath(folderPath);

						FileUtil::CreateFolderHierarchy(folderPath);
					}

					// FIXME: We should find a way to get ContentId without loading the File Attachment so that we can check if the file
					// is already downloaded.
					// It seems we can't get contentId without contentBytes right now... (see MSGraphSession::ListMessageAttachments)
					RunOnScopeEnd _([objName = logger->GetLogMessage(), &logger]() { logger->SetLogMessage(objName); });
					logger->SetLogMessage(_T("inline file attachments"));

					auto nonPageLogger = std::make_shared<MultiObjectsRequestLogger>(*logger);

					BusinessFileAttachment fileAttachment;
					if (cmdInfo.GetOrigin() == Origin::Message)
						fileAttachment = bom->GetMessageFileAttachment(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), attachment.GetID(), nonPageLogger, taskData).GetTask().get();
					else if (cmdInfo.GetOrigin() == Origin::User)
						fileAttachment = bom->GetUserEventFileAttachment(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), attachment.GetID(), nonPageLogger, taskData).GetTask().get();
					else if (cmdInfo.GetOrigin() == Origin::Group)
						fileAttachment = bom->GetGroupEventFileAttachment(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), attachment.GetID(), nonPageLogger, taskData).GetTask().get();
					else if (cmdInfo.GetOrigin() == Origin::Conversations)
						fileAttachment = bom->GetPostFileAttachment(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), attachmentInfo.m_IDs.ThreadID(), attachmentInfo.m_IDs.PostID(), attachment.GetID(), nonPageLogger, taskData).GetTask().get();

					const wstring fileName = fileAttachment.GetContentId() ? *fileAttachment.GetContentId() : (fileAttachment.GetName() ? *fileAttachment.GetName() : _YTEXT(""));
					const wstring fullFilePath = fileAttachment.ToFile(folderPath, FileExistsAction::Overwrite, fileName);
					ASSERT(!fullFilePath.empty());

					idsToCidsToPaths[attachmentInfo.m_IDs][fileAttachment.GetContentId()->c_str()] = fullFilePath;
				}
			}
		}

        // Get html view, and set its new modified html
        YCallbackMessage::DoPost([p_Callback, idsToCidsToPaths, frameHwnd, taskData]()
        {
            if (::IsWindow(frameHwnd))
            {
                GridFrameBase* frame = dynamic_cast<GridFrameBase*>(CWnd::FromHandle(frameHwnd));
                if (nullptr != frame)
                    frame->GetGrid().ClearLog(taskData.GetId());
            }

			if (nullptr != p_Callback)
				p_Callback->processInlineAttachments(idsToCidsToPaths);

            TaskDataManager::Get().RemoveTaskData(taskData.GetId(), frameHwnd);
        });
    });
}

ModuleBase::AttachmentsContainer ModuleBase::retrieveAttachments(std::shared_ptr<BusinessObjectManager> p_BOM, const std::vector<AttachmentsInfo::IDs>& p_IDs, Origin p_Origin, YtriaTaskData taskData)
{
	AttachmentsContainer allAttachments;

	auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("attachments"), p_IDs.size());

    for (const auto& id : p_IDs)
    {
		logger->IncrementObjCount();
		logger->SetContextualInfo(id.Context());

		if (!taskData.IsCanceled())
		{
			if (p_Origin == Origin::Message)
			{
				auto taskResult = p_BOM->GetMessageAttachments(id.UserOrGroupID(), id.EventOrMessageOrConversationID(), logger, taskData).Then([id, taskData](const vector<BusinessAttachment>& p_Attachments)
				{
					return std::make_tuple(id.UserOrGroupID(), id.EventOrMessageOrConversationID(), p_Attachments);
				}).GetTask().get();
				allAttachments[std::get<0>(taskResult)][std::get<1>(taskResult)] = std::get<2>(taskResult);
			}
			else if (p_Origin == Origin::User)
			{
				auto taskResult = p_BOM->GetUserEventAttachments(id.UserOrGroupID(), id.EventOrMessageOrConversationID(), logger, taskData).Then([id, taskData](const vector<BusinessAttachment>& p_Attachments)
				{
					return std::make_tuple(id.UserOrGroupID(), id.EventOrMessageOrConversationID(), p_Attachments);
				}).GetTask().get();
				allAttachments[std::get<0>(taskResult)][std::get<1>(taskResult)] = std::get<2>(taskResult);
			}
			else if (p_Origin == Origin::Group)
			{
				auto taskResult = p_BOM->GetGroupEventAttachments(id.UserOrGroupID(), id.EventOrMessageOrConversationID(), logger, taskData).Then([id, taskData](const vector<BusinessAttachment>& p_Attachments)
				{
					return std::make_tuple(id.UserOrGroupID(), id.EventOrMessageOrConversationID(), p_Attachments);
				}).GetTask().get();
				allAttachments[std::get<0>(taskResult)][std::get<1>(taskResult)] = std::get<2>(taskResult);
			}
			else if (p_Origin == Origin::Conversations)
			{
				auto taskResult = p_BOM->GetPostAttachments(id.UserOrGroupID(), id.EventOrMessageOrConversationID(), id.ThreadID(), id.PostID(), logger, taskData).Then([id, taskData](const vector<BusinessAttachment>& p_Attachments)
				{
					// second.first: conversationId@threadId@postId
					return std::make_tuple(id.UserOrGroupID(), id.EventOrMessageOrConversationID() + _YTEXT("@") + id.ThreadID() + _YTEXT("@") + id.PostID(), p_Attachments);
				}).GetTask().get();
				allAttachments[std::get<0>(taskResult)][std::get<1>(taskResult)] = std::get<2>(taskResult);
			}
		}
    }

    for (auto& userGroupKeyVal : allAttachments)
    {
        for (auto& subObjMap : userGroupKeyVal.second)
            subObjMap.second = ModuleUtil::SortAttachments(subObjMap.second);
    }

    return allAttachments;
}
