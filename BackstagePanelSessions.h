#pragma once

#include "BackstagePanelWithGrid.h"
#include "GridSessionsBackstage.h"

class BackstagePanelSessions	: public BackstagePanelWithGrid<GridSessionsBackstage>
								, public GridSelectionObserver
{
public:
    BackstagePanelSessions();
	virtual ~BackstagePanelSessions() override;

    enum { IDD = IDD_BACKSTAGEPAGE_SESSIONS};

    virtual BOOL OnInitDialog() override;
    virtual BOOL OnSetActive() override;

    void DoDataExchange(CDataExchange* pDX) override;

    afx_msg void OnLoadSession();
    afx_msg void OnDeleteSession();
	afx_msg void OnSetFavorite();
    afx_msg void OnSignout();
	afx_msg void OnRemoveUltraAdminPart();

    void UpdateButtons();
    
	void SessionChanged();

protected:
	DECLARE_MESSAGE_MAP();

	virtual void SetTheme(const XTPControlTheme nTheme) override;

	virtual void SelectionChanged(const CacheGrid& grid);

private:
    CXTPRibbonBackstageButton m_BtnLoad;
    CXTPRibbonBackstageButton m_BtnDelete;
	CXTPRibbonBackstageButton m_BtnFavorite;
    CXTPRibbonBackstageButton m_BtnSignOut;
    CXTPRibbonBackstageButton m_BtnRemoveUltraAdminPart;
	CXTPImageManager m_imagelist;

	bool m_IsSigningOut;
};

