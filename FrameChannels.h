#pragma once

#include "GridFrameBase.h"
#include "GridChannels.h"

class FrameChannels : public GridFrameBase
{
public:
	FrameChannels(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
    
    DECLARE_DYNAMIC(FrameChannels)
    virtual ~FrameChannels() = default;

    void ShowChannels(const O365DataMap<BusinessGroup, std::vector<BusinessChannel>>& p_Channels, const std::vector<BusinessChannel>& p_LoadedMoreChannels, bool p_FullPurge);
	void UpdateChannelsLoadMore(const vector<BusinessChannel>& p_Channels);

    virtual O365Grid& GetGrid() override;
    virtual void RefreshSpecific(AutomationAction* p_CurrentAction);
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

protected:
    virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

    virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

    virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
    GridChannels m_GridChannels;
};

