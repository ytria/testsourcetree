#include "AttachmentDeserializer.h"

#include "JsonSerializeUtil.h"

void AttachmentDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("contentType"), m_Data.ContentType, p_Object);
	//JsonSerializeUtil::DeserializeString(_YTEXT("contentId"), m_Data.ContentId, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.Id, p_Object, true);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isInline"), m_Data.IsInline, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.LastModifiedDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.Name, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YTEXT("size"), m_Data.Size, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("@odata.type"), m_Data.DataType, p_Object);
}
