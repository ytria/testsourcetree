#pragma once

#include "ISessionsMigrationVersion.h"

class SQLiteEngine;

class SessionsMigrationVersion1To2 : public ISessionsMigrationVersion
{
public:
	void Build() override;
	VersionSourceTarget GetVersionInfo() const override;

private:
	bool CreateNewFromOld();
	bool Exec();
};

