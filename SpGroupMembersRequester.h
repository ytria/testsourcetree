#pragma once

#include "IRequester.h"
#include "SpUtils.h"

#include "RoleDefinition.h"
#include "RoleDefinitionDeserializer.h"

class SingleRequestResult;

template <class T, class U>
class ValueListDeserializer;

namespace Sp
{
    class User;
    class UserDeserializer;
}

namespace Sp
{
    class SpGroupMembersRequester : public IRequester
    {
    public:
        SpGroupMembersRequester(PooledString p_SiteUrl, PooledString p_GroupId);
        TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

        const vector<Sp::User>& GetData() const;

    private:
        PooledString m_SiteUrl;
        PooledString m_GroupId;
        std::shared_ptr<ValueListDeserializer<Sp::User, Sp::UserDeserializer>> m_Deserializer;
        std::shared_ptr<SingleRequestResult> m_Result;
    };
}
