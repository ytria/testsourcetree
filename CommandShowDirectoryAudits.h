#pragma once

#include "IActionCommand.h"
#include "ModuleOptions.h"

struct AutomationSetup;
class AutomationAction;
class BusinessObjectManager;
class FrameDirectoryAudits;
class LicenseContext;

class CommandShowDirectoryAudits : public IActionCommand
{
public:
	CommandShowDirectoryAudits(FrameDirectoryAudits& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, const ModuleOptions& p_Options, bool p_IsUpdate);

protected:
	void ExecuteImpl() const override;

	AutomationSetup& m_AutoSetup;
	FrameDirectoryAudits& m_Frame;
	const LicenseContext& m_LicenseCtx;
	AutomationAction* m_AutoAction;
	ModuleOptions m_Options;
	bool m_IsUpdate;
};

