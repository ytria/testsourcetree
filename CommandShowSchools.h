#pragma once

#include "IActionCommand.h"
#include "EducationSchool.h"

struct AutomationSetup;
class AutomationAction;
class BusinessObjectManager;
class FrameSchools;
class LicenseContext;

class CommandShowSchools : public IActionCommand
{
public:
	CommandShowSchools(FrameSchools& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, bool p_IsUpdate);
	CommandShowSchools(const vector<EducationSchool>& p_Schools, FrameSchools& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, bool p_IsUpdate);

protected:
	void ExecuteImpl() const override;

	AutomationSetup& m_AutoSetup;
	FrameSchools& m_Frame;
	const LicenseContext& m_LicenseCtx;
	AutomationAction* m_AutoAction;
	bool m_IsUpdate;
	vector<EducationSchool> m_Schools;
};

