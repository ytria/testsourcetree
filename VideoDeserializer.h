#pragma once
#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Video.h"

class VideoDeserializer : public JsonObjectDeserializer, public Encapsulate<Video>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

