#pragma once

#include "IGraphSessionCreator.h"

class UserGraphSessionCreator : public IGraphSessionCreator
{
public:
	UserGraphSessionCreator(const wstring& p_Tenant, const wstring& p_Username, const wstring& p_Password);

	std::shared_ptr<MSGraphSession> Create();

	const wstring& GetTenant() const;
	const wstring& GetUsername() const;
	const wstring& GetPassword() const;

private:
	wstring m_Tenant;
	wstring m_Username;
	wstring m_Password;
};

