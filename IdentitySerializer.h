#pragma once

#include "IJsonSerializer.h"
#include "Identity.h"

class IdentitySerializer : public IJsonSerializer
{
public:
	IdentitySerializer(const Identity& p_Identity);
	void Serialize() override;

private:
	Identity m_Identity;
};

