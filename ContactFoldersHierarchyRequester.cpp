#include "ContactFoldersHierarchyRequester.h"

#include "ContactFolderDeserializer.h"
#include "MSGraphCommonData.h"
#include "ValueListDeserializer.h"
#include "MSGraphSession.h"
#include "PaginatedRequestResults.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

using namespace Rest;

ContactFoldersHierarchyRequester::ContactFoldersHierarchyRequester(const PooledString& p_UserId, const PooledString& p_ContactFolderId, const std::shared_ptr<IPageRequestLogger>& p_Logger)
    : m_UserId(p_UserId),
    m_ContactFolderId(p_ContactFolderId),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> ContactFoldersHierarchyRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<BusinessContactFolder, ContactFolderDeserializer>>();

	web::uri_builder uri;
	uri.append_path(USERS_PATH);
	uri.append_path(m_UserId);
	uri.append_path(CONTACTFOLDERS_PATH);
	uri.append_path(m_ContactFolderId);
	uri.append_path(CHILDFOLDERS_PATH);

    auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Deserializer, m_Logger, httpLogger, p_TaskData.GetOriginator()).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData).Then([logger = m_Logger, userId = m_UserId, deserializer = m_Deserializer, p_Session, p_TaskData]() {
        for (auto& contactFolder : deserializer->GetData())
        {
            ContactFoldersHierarchyRequester childrenRequest(userId, contactFolder.GetID(), logger);
            childrenRequest.Send(p_Session, p_TaskData).GetTask().wait();
            contactFolder.SetChildrenContactFolders(childrenRequest.GetData());
        }
    }, p_TaskData.GetToken());
}

vector<BusinessContactFolder>& ContactFoldersHierarchyRequester::GetData()
{
    return m_Deserializer->GetData();
}
