#pragma once

#include "ObjectActionModification.h"

class O365Grid;

class RestoreObjectModification : public ObjectActionModification
{
public:
	RestoreObjectModification(O365Grid& p_Grid, const row_pk_t& p_RowKey);

	virtual void RefreshState() override;
	virtual vector<ModificationLog> GetModificationLogs() const override;
};
