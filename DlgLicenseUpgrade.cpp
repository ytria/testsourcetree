#include "DlgLicenseUpgrade.h"

#include "DlgLicenseMessageBoxHTML.h"
#include "JSONUtil.h"
#include "LicenseUtil.h"
#include "MainFrame.h"
#include "MFCUtil.h"

BEGIN_DISPATCH_MAP(Simulator, CCmdTarget)
	DISP_FUNCTION(Simulator, "Simulate", Simulate, VT_BSTR, VTS_WBSTR VTS_WBSTR VTS_WBSTR) // JS pipeline
END_DISPATCH_MAP()

Simulator::Simulator(DlgLicenseUpgrade* p_DlgUpgrade):
	m_DlgUpgrade(p_DlgUpgrade)
{
	EnableAutomation();
	ASSERT(nullptr != m_DlgUpgrade);
}

BSTR Simulator::Simulate(LPCWSTR p_NewQty, LPCWSTR p_NowOrNextBilling, LPCWSTR p_ValidationKey)
{
	_bstr_t rv;

	// get values from endpoint
	auto app = reinterpret_cast<AutomatedApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	ASSERT(nullptr != p_NewQty);
	ASSERT(nullptr != p_NowOrNextBilling);
	if (nullptr != app)
	{
		auto licenseManager = app->GetLicenseManager();
		ASSERT(nullptr != licenseManager);
		if (nullptr != licenseManager)
		{
			static const wstring g_BillNow	= _YTEXT("now");

			const uint32_t	userCapacity	= nullptr != p_NewQty ? static_cast<uint32_t>(Str::GetNumber(p_NewQty)) : 0;
			const bool		applyNow		= nullptr != p_NowOrNextBilling ? MFCUtil::StringMatchW(g_BillNow, p_NowOrNextBilling) : true;
			LicenseUpgradeConfig licenseUCfg(userCapacity, applyNow, true, wstring()/*p_ValidationKey*/);// as of 10.10.2018: validation key is not used in simulation mode
			CRMpipe::RequestResponse rr = licenseManager->CRMExtraLicenseUpgrade(licenseUCfg);
			if (rr.IsError())
			{
				DlgLicenseMessageBoxHTML dlg(m_DlgUpgrade,
											 DlgMessageBox::eIcon_Error,
											 YtriaTranslate::Do(Simulator_Simulate_1, _YLOC("Could not perform license upgrade simulation")).c_str(),
											 YtriaTranslate::Do(Simulator_Simulate_3, _YLOC("%1 Please try again."), rr.GetCRMError().c_str()),
											 _YTEXT(""),
											 { { IDCANCEL, YtriaTranslate::Do(Simulator_Simulate_2, _YLOC("OK")).c_str() } },
											 { 0, _YTEXT("") },
											 licenseManager->GetApplicationColor());
				dlg.DoModal();
			}
			else
			{
				auto result = rr.GetMap(CRMpipe::g_CRMfieldKey_UpgradeResult);
				json::value root = 
					JSON_BEGIN_OBJECT
						JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerGrossPrice)		JSON_PAIR_VALUE(JSON_STRING(result[CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerGrossPrice])),
						JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerVatPrice)		JSON_PAIR_VALUE(JSON_STRING(result[CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerVatPrice])),
						JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerNetPrice)		JSON_PAIR_VALUE(JSON_STRING(result[CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerNetPrice])),
						JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerGrossPrice)	JSON_PAIR_VALUE(JSON_STRING(result[CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerGrossPrice])),
						JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerVatPrice)		JSON_PAIR_VALUE(JSON_STRING(result[CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerVatPrice])),
						JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerNetPrice)		JSON_PAIR_VALUE(JSON_STRING(result[CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerNetPrice])),
						JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultPriceCurrencyId)					JSON_PAIR_VALUE(JSON_STRING(result[CRMpipe::g_CRMfieldKey_UpgradeResultPriceCurrencyId])),
						JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultResultMessage)					JSON_PAIR_VALUE(JSON_STRING(result[CRMpipe::g_CRMfieldKey_UpgradeResultResultMessage])),
						JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingDate)					JSON_PAIR_VALUE(JSON_STRING(result[CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingDate]))
					JSON_END_OBJECT;
				rv = _bstr_t(root.serialize().c_str());
			}
		}
	}

	return rv;
}

std::map<wstring, int, Str::keyLessInsensitive> DlgLicenseUpgrade::g_Sizes;
const wstring DlgLicenseUpgrade::g_JSONBtnNameCancel	= _YTEXT("BTN_CANCEL");
const wstring DlgLicenseUpgrade::g_JSONBtnNameOK		= _YTEXT("BTN_OK");
const wstring DlgLicenseUpgrade::g_JSONKeyValidation	= _YTEXT("KeyValidation");
const wstring DlgLicenseUpgrade::g_JSONNewUserCapacity	= _YTEXT("NewUserCapacity");
const wstring DlgLicenseUpgrade::g_JSONnext				= _YTEXT("next");
const wstring DlgLicenseUpgrade::g_JSONnow				= _YTEXT("now");

DlgLicenseUpgrade::DlgLicenseUpgrade(const VendorLicense& p_License, uint32_t p_Min, CWnd* p_Parent) :
	ResizableDialog(IDD, p_Parent),
	m_License(p_License),
	m_Min(p_Min),
	m_HtmlView(make_unique<YAdvancedHtmlView>(true))
{
	const bool success = Str::LoadSizeFile(IDR_LICENSEUSERCOUNTCHANGE_SIZE, g_Sizes);
	ASSERT(success);
}

bool DlgLicenseUpgrade::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	bool RetVal = true;

	ASSERT(data.size() >= 1);
	if (data.size() >= 1)
	{
		int endID = IDCANCEL;
		for (const auto& p : data)
		{
			if (MFCUtil::StringMatchW(p.first, g_JSONNewUserCapacity))
				m_UpgradeConfig.m_UserCapacity = Str::getLongFromString(p.second);
			else if (MFCUtil::StringMatchW(p.first, g_JSONKeyValidation))
				m_UpgradeConfig.m_KeyValidation = p.second;
			else if (MFCUtil::StringMatchW(p.first, _YTEXT("nextNow")))// another failure - not configured from JSON
				m_UpgradeConfig.m_ApplyNow = MFCUtil::StringMatchW(g_JSONnow, p.second);
			else if (MFCUtil::StringMatchW(p.first, g_JSONBtnNameCancel))
				endID = IDCANCEL;
			else if (MFCUtil::StringMatchW(p.first, g_JSONBtnNameOK))
				endID = IDOK;
		}

		if (IDOK == endID)
			m_UpgradeConfig.m_Preview = false;

		endDialogFixed(endID);
		RetVal = false;// this is true only to pop a browser window with a URL
	}

	return RetVal;
}

const LicenseUpgradeConfig& DlgLicenseUpgrade::GetUpgradeCfg() const
{
	return m_UpgradeConfig;
}

bool DlgLicenseUpgrade::OnNewPostedURL(const wstring& url)
{
	ASSERT(false);
	return true;
}

BOOL DlgLicenseUpgrade::OnInitDialogSpecificResizable()
{
	SetWindowText(YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_11, _YLOC("License Modification")).c_str());

	CWnd* pCtrl = GetDlgItem(IDC_DLG_LICENSEUPGRADEHTML);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_DLG_LICENSEUPGRADEHTML, NULL);

	// FIXME: Temporary - Remove when no more needed.
	//m_HtmlView->SetDebugDisplayPostedData(true);

	m_HtmlView->SetPostedDataTarget(this);
	m_HtmlView->SetPostedURLTarget(this);
	m_HtmlView->SetExternal(std::make_shared<Simulator>(this));

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
	{
		{ _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } },
		{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-licenseUserCountChange.js"), _YTEXT("") } }, // This file is in resources
		{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("licenseUserCountChange.js"), _YTEXT("") } } // This file is in resources
	}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("licenseUserCountChange.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-height"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-width"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);

	return TRUE;
}

void DlgLicenseUpgrade::generateJSONScriptData(wstring& p_Output)
{
	const bool userCapByTenant = m_License.UsesCaps();
	ASSERT(userCapByTenant);
	// noUserCount has priority
	wstring intro		= userCapByTenant ? YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_1, _YLOC("Modify your license capacity")).c_str() : YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_2, _YLOC("Your license is not capped by user count!")).c_str();
	wstring subIntro	= userCapByTenant ? YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_3, _YLOC("Raise or reduce the number of Office 365 tenant users allowed with your license.")).c_str() : YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_4, _YLOC("Does not apply")).c_str();

	vector<web::json::value> dlgJsonItems;
	dlgJsonItems.push_back(	JSON_BEGIN_OBJECT
								JSON_PAIR_KEY(_YTEXT("hbs"))					JSON_PAIR_VALUE(JSON_STRING(_YTEXT("licenseUserCountChange"))),
								JSON_PAIR_KEY(_YTEXT("labelIntro"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(intro))),
								JSON_PAIR_KEY(_YTEXT("labelSubIntro"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(subIntro))),
								JSON_PAIR_KEY(_YTEXT("currentUsrLabel"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_5, _YLOC("Your current license's user capacity:")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("currentUsrQuantity"))		JSON_PAIR_VALUE(JSON_STRING(Str::getStringFromNumber(m_License.GetFeatureGlobalUserCount().GetPoolMax()))),
								JSON_PAIR_KEY(_YTEXT("newUsrLabel"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_6, _YLOC("Requested user capacity:")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("newUsrName"))				JSON_PAIR_VALUE(JSON_STRING(g_JSONNewUserCapacity)),
								JSON_PAIR_KEY(_YTEXT("minNewUsr"))				JSON_PAIR_VALUE(JSON_STRING(Str::getStringFromNumber(m_Min))),
								JSON_PAIR_KEY(_YTEXT("addRemoveLabel"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_7, _YLOC("User capacity difference:")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("updateWhenLabel"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_8, _YLOC("When do you want your new license capacity to take effect?")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("chargeNow"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_9, _YLOC("Remaining balance due immediately")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("chargeNext"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_10, _YLOC("Your next invoice amount")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("labelNet"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_17, _YLOC("NET")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("labelVat"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_18, _YLOC("VAT")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("labelTotal"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_19, _YLOC("Total")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("validateLabel"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_11, _YLOC("Preview Pricing")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("warningIcon"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("fas fa-sign-in"))),
								JSON_PAIR_KEY(_YTEXT("keyValidation"))			JSON_PAIR_VALUE(JSON_STRING(g_JSONKeyValidation)),
								JSON_PAIR_KEY(_YTEXT("keyVal_popup"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Sxxxxxxxx"))),
								JSON_PAIR_KEY(_YTEXT("warningMsg"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_9, _YLOC("To verify you are the authorised user, please enter the subscription code (Sxxxxxxxx) for this license.")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("warningSubMsg"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_20, _YLOC("Your subscription code is located on your invoice below the cleverbridge reference number.")).c_str()))),
								//JSON_PAIR_KEY(_YTEXT("labelFooter"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
								JSON_BEGIN_PAIR
									_YTEXT("choices"),
								   JSON_BEGIN_ARRAY
									   JSON_BEGIN_OBJECT
										   JSON_PAIR_KEY(_YTEXT("id"))			JSON_PAIR_VALUE(JSON_STRING(g_JSONnext)),
										   JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_13, _YLOC("Start of next billing cycle")).c_str()))),
										   JSON_PAIR_KEY(_YTEXT("attribute"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("checked")))
									   JSON_END_OBJECT,
									   JSON_BEGIN_OBJECT
										   JSON_PAIR_KEY(_YTEXT("id"))			JSON_PAIR_VALUE(JSON_STRING(g_JSONnow)),
										   JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_14, _YLOC("Immediately")).c_str())))
									   JSON_END_OBJECT
								   JSON_END_ARRAY
								JSON_END_PAIR
							JSON_END_OBJECT);

	dlgJsonItems.push_back(JSON_BEGIN_OBJECT
								JSON_PAIR_KEY(_YTEXT("hbs")) JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Button"))),
								JSON_BEGIN_PAIR
										_YTEXT("choices"),
										JSON_BEGIN_ARRAY
											JSON_BEGIN_OBJECT
												JSON_PAIR_KEY(_YTEXT("type"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
												JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_15, _YLOC("Apply")).c_str()))),
												JSON_PAIR_KEY(_YTEXT("attribute"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("post"))),
												JSON_PAIR_KEY(_YTEXT("name"))		JSON_PAIR_VALUE(JSON_STRING(g_JSONBtnNameOK)),
											JSON_END_OBJECT,
											JSON_BEGIN_OBJECT
												JSON_PAIR_KEY(_YTEXT("type"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
												JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_16, _YLOC("Cancel")).c_str()))),
												JSON_PAIR_KEY(_YTEXT("attribute"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("cancel"))),
												JSON_PAIR_KEY(_YTEXT("name"))		JSON_PAIR_VALUE(JSON_STRING(g_JSONBtnNameCancel)),
										   JSON_END_OBJECT
									   JSON_END_ARRAY
								   JSON_END_PAIR
						   JSON_END_OBJECT);


	json::value root =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("main"),
				JSON_BEGIN_OBJECT
					JSON_PAIR_KEY(_YTEXT("id"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("DlgLicenseUpgrade"))),
					JSON_PAIR_KEY(_YTEXT("method"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("POST"))),
					JSON_PAIR_KEY(_YTEXT("action"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("#"))),
				JSON_END_OBJECT
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("items"),
				web::json::value::array(dlgJsonItems)
			JSON_END_PAIR
		JSON_END_OBJECT;

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}