#pragma once

#include "BaseO365Grid.h"
#include "SpGroup.h"

class FrameSpGroups;

class GridSpGroups : public ModuleO365Grid<Sp::Group>
{
public:
    GridSpGroups();
    virtual ~GridSpGroups() override;

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildView(const O365DataMap<BusinessSite, vector<Sp::Group>>& p_SpGroups, bool p_FullPurge);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
    void customizeGrid() override;

    Sp::Group        getBusinessObject(GridBackendRow*) const override;
    void			UpdateBusinessObjects(const vector<Sp::Group>& p_SpGroups, bool p_SetModifiedStatus) override;
    void			RemoveBusinessObjects(const vector<Sp::Group>& p_SpGroups) override;

private:
    DECLARE_MESSAGE_MAP()

    void InitializeCommands() override;
    void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart) override;

    void FillSiteFields(const BusinessSite& p_Site, const Sp::Group& p_SpGroup, GridBackendRow* p_Row, GridUpdater& p_Updater);

    GridBackendColumn* m_ColMetaId;
    GridBackendColumn* m_ColMetaDisplayName;
    GridBackendColumn* m_ColMetaUserDisplayName;

    GridBackendColumn* m_ColAllowMembersEditMembership;
    GridBackendColumn* m_ColAllowRequestToJoinLeave;
    GridBackendColumn* m_ColAutoAcceptRequestToJoinLeave;
    GridBackendColumn* m_ColCanCurrentUserEditMembership;
    GridBackendColumn* m_ColCanCurrentUserManageGroup;
    GridBackendColumn* m_ColCanCurrentUserViewMembership;
    GridBackendColumn* m_ColDescription;
    GridBackendColumn* m_ColGroupId; // FIXME: Shouldn't we use O365Grid::m_ColId instead?
    GridBackendColumn* m_ColIsHiddenInUI;
    GridBackendColumn* m_ColLoginName;
    GridBackendColumn* m_ColOnlyAllowMembersViewMembership;
    GridBackendColumn* m_ColOwnerTitle;
    GridBackendColumn* m_ColRequestToJoinLeaveEmailSetting;
    GridBackendColumn* m_ColPrincipalType;
    GridBackendColumn* m_ColTitle;

    FrameSpGroups* m_Frame;
};

