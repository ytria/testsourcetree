#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessColumnDefinition.h"

class ColumnDefinitionDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessColumnDefinition>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

