#pragma once

#include "IRequester.h"

class SingleRequestResult;

class DriveItemUpdateRequester : public IRequester
{
public:
    DriveItemUpdateRequester(PooledString p_DriveId, PooledString p_DriveItemId);
    
    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
    
    const SingleRequestResult& GetResult() const;

    void SetName(PooledString p_NewName);

private:
    PooledString m_DriveId;
    PooledString m_DriveItemId;

    boost::YOpt<PooledString> m_NewName;

    std::shared_ptr<SingleRequestResult> m_Result;

};

