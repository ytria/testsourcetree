#pragma once

#include "IRequester.h"

#include "SingleRequestResult.h"

class RemoveApplicationPasswordRequester : public IRequester
{
public:
	RemoveApplicationPasswordRequester(const wstring& p_AppId, const wstring& p_KeyId);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const SingleRequestResult& GetResult() const;

private:
	wstring m_AppId;
	wstring m_KeyId;

	std::shared_ptr<SingleRequestResult> m_Result;
};

