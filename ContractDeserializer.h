#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "Contract.h"

class ContractDeserializer : public JsonObjectDeserializer, public Encapsulate<Contract>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};
