#include "PowerShellDeleter.h"

#include "YtriaPowerShellDll.h"

PowerShellDeleter::PowerShellDeleter()
{
}

void PowerShellDeleter::operator()(IPowerShell* p_PowerShell)
{
	YtriaPowerShellDll::Get().ReleasePowerShell(p_PowerShell);
}
