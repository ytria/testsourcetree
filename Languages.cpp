#include "Languages.h"

const boost::bimap<wstring, wstring>& Languages::Get()
{
	static boost::bimap<wstring, wstring> languages;

	if (languages.empty())
	{
#define DEFINE_LANGUAGE_CODE(LanguageCode, Language, Region) \
	if (wstring(Region).empty()) \
		languages.insert({ _YTEXTFORMAT(L"%s", wstring(Language).c_str()), LanguageCode }); \
	else \
		languages.insert({ _YTEXTFORMAT(L"%s (%s)", wstring(Language).c_str(), wstring(Region).c_str()), LanguageCode }); \

#include "XMacroTrick_LanguageCodeDefinition.h"

#undef DEFINE_LANGUAGE_CODE
	}

	return languages;
}
