#include "BusinessChatMessage.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessChatMessage>("ChatMessage")
		(metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Message"))))
		.constructor()(policy::ctor::as_object);
}

BusinessChatMessage::BusinessChatMessage()
{
}

//BusinessChatMessage::BusinessChatMessage(const ChatMessage& p_Post)
//{
//}

const boost::YOpt<PooledString>& BusinessChatMessage::GetReplyToId() const
{
	return m_ReplyToId;
}

void BusinessChatMessage::SetReplyToId(const boost::YOpt<PooledString>& p_Val)
{
	m_ReplyToId = p_Val;
}

const boost::YOpt<IdentitySet>& BusinessChatMessage::GetFrom() const
{
	return m_From;
}

void BusinessChatMessage::SetFrom(const boost::YOpt<IdentitySet>& p_Val)
{
	m_From = p_Val;
}

const boost::YOpt<PooledString>& BusinessChatMessage::GetEtag() const
{
	return m_Etag;
}

void BusinessChatMessage::SetEtag(const boost::YOpt<PooledString>& p_Val)
{
	m_Etag = p_Val;
}

const boost::YOpt<PooledString>& BusinessChatMessage::GetMessageType() const
{
	return m_MessageType;
}

void BusinessChatMessage::SetMessageType(const boost::YOpt<PooledString>& p_Val)
{
	m_MessageType = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessChatMessage::GetCreatedDateTime() const
{
	return m_CreatedDateTime;
}

void BusinessChatMessage::SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
	m_CreatedDateTime = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessChatMessage::GetLastModifiedDateTime() const
{
	return m_LastModifiedDateTime;
}

void BusinessChatMessage::SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
	m_LastModifiedDateTime = p_Val;
}

const boost::YOpt<bool>& BusinessChatMessage::GetDeleted() const
{
	return m_Deleted;
}

void BusinessChatMessage::SetDeleted(const boost::YOpt<bool>& p_Val)
{
	m_Deleted = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessChatMessage::GetDeletedDateTime() const
{
	return m_DeletedDateTime;
}

void BusinessChatMessage::SetDeletedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
	m_DeletedDateTime = p_Val;
}

const boost::YOpt<PooledString>& BusinessChatMessage::GetSubject() const
{
	return m_Subject;
}

void BusinessChatMessage::SetSubject(const boost::YOpt<PooledString>& p_Val)
{
	m_Subject = p_Val;
}

const boost::YOpt<ItemBody>& BusinessChatMessage::GetBody() const
{
	return m_Body;
}

void BusinessChatMessage::SetBody(const boost::YOpt<ItemBody>& p_Val)
{
	m_Body = p_Val;
}

const boost::YOpt<PooledString>& BusinessChatMessage::GetSummary() const
{
	return m_Summary;
}

void BusinessChatMessage::SetSummary(const boost::YOpt<PooledString>& p_Val)
{
	m_Summary = p_Val;
}

const vector<ChatAttachment>& BusinessChatMessage::GetAttachments() const
{
	return m_Attachments;
}

void BusinessChatMessage::SetAttachments(const vector<ChatAttachment>& p_Val)
{
	m_Attachments = p_Val;
}

const vector<ChatMention>& BusinessChatMessage::GetMentions() const
{
	return m_Mentions;
}

void BusinessChatMessage::SetMentions(const vector<ChatMention>& p_Val)
{
	m_Mentions = p_Val;
}

const boost::YOpt<PooledString>& BusinessChatMessage::GetImportance() const
{
	return m_Importance;
}

void BusinessChatMessage::SetImportance(const boost::YOpt<PooledString>& p_Val)
{
	m_Importance = p_Val;
}

const vector<ChatReaction>& BusinessChatMessage::GetReactions() const
{
	return m_Reactions;
}

void BusinessChatMessage::SetReactions(const vector<ChatReaction>& p_Val)
{
	m_Reactions = p_Val;
}

const boost::YOpt<PooledString>& BusinessChatMessage::GetLocale() const
{
	return m_Locale;
}

void BusinessChatMessage::SetLocale(const boost::YOpt<PooledString>& p_Val)
{
	m_Locale = p_Val;
}

const vector<BusinessChatMessage>& BusinessChatMessage::GetReplies() const
{
	return m_Replies;
}

void BusinessChatMessage::SetReplies(const vector<BusinessChatMessage>& p_Val)
{
	m_Replies = p_Val;
}

void BusinessChatMessage::AddReply(const BusinessChatMessage& p_Val)
{
	m_Replies.push_back(p_Val);
}

//ChatMessage BusinessChatMessage::ToChatMessage(const BusinessChatMessage& p_BusinessChatMessage)
//{
//
//}
