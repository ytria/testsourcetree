#include "IMigrationsProvisioner.h"

const std::vector<std::shared_ptr<IMigrationVersion>>& IMigrationsProvisioner::GetMigrations() const
{
	return m_Migrations;
}
