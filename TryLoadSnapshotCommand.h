#pragma once

#include "Command.h"
#include "IActionCommand.h"
#include "GridSnapshotLoader.h"
#include "SessionIdentifier.h"

class TryLoadSnapshotCommand
	: public IActionCommand
{
public:
	TryLoadSnapshotCommand();
	void SetSnapshotLoader(std::shared_ptr<GridSnapshot::Loader> p_Loader);
	
	// To be set right before calling Execute only. Pointer kept till ExecuteImpl() is finished.
	void SetParentCommandForNextExecute(const Command* p_Command);

protected:
	void ExecuteImpl() const override;

private:
	const Command* m_ParentCommand = nullptr;
	std::shared_ptr<GridSnapshot::Loader> m_Loader;
};