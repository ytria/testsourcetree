#include "EducationTeacherDeserializer.h"

void EducationTeacherDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("externalId"), m_Data.m_ExternalId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("teacherNumber"), m_Data.m_TeacherNumber, p_Object);
}
