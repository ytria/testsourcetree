#include "FrameDirectoryAudits.h"

IMPLEMENT_DYNAMIC(FrameDirectoryAudits, GridFrameBase)

FrameDirectoryAudits::FrameDirectoryAudits(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
	m_CreateRefreshPartial = false;
}

void FrameDirectoryAudits::BuildView(const vector<DirectoryAudit>& p_DirAudits, bool p_FullPurge, bool p_NoPurge)
{
	m_Grid.BuildView(p_DirAudits, p_FullPurge, p_NoPurge);
}

O365Grid& FrameDirectoryAudits::GetGrid()
{
	return m_Grid;
}

void FrameDirectoryAudits::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::DirectoryAudit, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameDirectoryAudits::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{

}

bool FrameDirectoryAudits::HasApplyButton()
{
	return false;
}

void FrameDirectoryAudits::createGrid()
{
	m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FrameDirectoryAudits::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(tab);
	CXTPRibbonGroup* refreshGroup = CreateRefreshGroup(tab);

	ASSERT(nullptr != refreshGroup);
	if (nullptr != refreshGroup)
	{
		CXTPControl* ctrl = refreshGroup->Add(xtpControlButton, ID_DIRECTORYAUDITGRID_CHANGEOPTIONS);
		setImage({ ID_DIRECTORYAUDITGRID_CHANGEOPTIONS }, IDB_CHANGEMODULEOPTIONS, xtpImageNormal);
		setImage({ ID_DIRECTORYAUDITGRID_CHANGEOPTIONS }, IDB_CHANGEMODULEOPTIONS_16x16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	automationAddCommandIDToGreenLight(ID_DIRECTORYAUDITGRID_CHANGEOPTIONS);

	return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameDirectoryAudits::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;
	return statusRV;
}
