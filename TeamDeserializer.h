#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Team.h"

class TeamDeserializer : public JsonObjectDeserializer, public Encapsulate<Team>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

