#include "DelegatesRequester.h"

#include "ExchangeOnlineSession.h"
#include "GetDelegatesResponseDeserializer.h"
#include "ResponseDeserializer.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "SoapDocument.h"
#include "XMLUtil.h"
#include "GetDelegatesResponseDeserializer.h"
#include "BasicHttpRequestLogger.h"

using Ex::SoapDocument;

Ex::DelegatesRequester::DelegatesRequester(PooledString p_UserMail)
    :m_UserMail(p_UserMail)
{
}

TaskWrapper<void> Ex::DelegatesRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Result = std::make_shared<SingleRequestResult>();
    m_Deserializer = std::make_shared<GetDelegatesResponseDeserializer>();

    SoapDocument soapDoc;
    CreateXml(soapDoc);

    LoggerService::User(YtriaTranslate::Do(Ex__DelegatesRequester_Send_1, _YLOC("Requesting delegates for user with email %1"), m_UserMail.c_str()), p_TaskData.GetOriginator());

    const WebPayloadText payload(XMLUtil::GetDocumentAsString(soapDoc.GetDocument()));

    return p_Session->GetExchangeSession()->Send(m_Deserializer, std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier()), payload, p_TaskData);
}

void Ex::DelegatesRequester::CreateXml(SoapDocument &soapDoc)
{
    auto xmlDoc = soapDoc.GetDocument();
    ASSERT(nullptr != xmlDoc);
    if (nullptr != xmlDoc)
    {
        auto getDelegate = xmlDoc->createElement(_YTEXT("GetDelegate"));
        ASSERT(nullptr != getDelegate);
        if (nullptr != getDelegate)
        {
            getDelegate->setAttribute(_YTEXT("xmlns"), SoapDocument::MessagesNs.c_str());
            getDelegate->setAttribute(_YTEXT("xmlns:t"), SoapDocument::TypesNs.c_str());
            getDelegate->setAttribute(_YTEXT("IncludePermissions"), _YTEXT("true"));
            soapDoc.AddToBody(getDelegate);
            auto mailbox = xmlDoc->createElement(_YTEXT("Mailbox"));
            ASSERT(nullptr != mailbox);
            if (nullptr != mailbox)
            {
                getDelegate->appendChild(mailbox);
                auto emailAddress = xmlDoc->createElement(_YTEXT("t:EmailAddress"));
                ASSERT(nullptr != emailAddress);
                if (emailAddress)
                {
                    mailbox->appendChild(emailAddress);
                    emailAddress->setTextContent(m_UserMail.c_str());
                }
            }
        }
    }
}

vector<Ex::DelegateUser> Ex::DelegatesRequester::GetResult() const
{
    return m_Deserializer->GetDelegateUsers();
}
