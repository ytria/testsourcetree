#pragma once

#include "Encapsulate.h"
#include "IPSObjectCollectionDeserializer.h"
#include "OnPremiseUser.h"

#include <string>
#include <vector>

class OnPremiseUsersCollectionDeserializer : public IPSObjectCollectionDeserializer, public Encapsulate<std::vector<OnPremiseUser>>
{
public:
	void Deserialize(IPSObjectPropertyReader& p_Reader) override;
	void SetCount(size_t p_Count) override;
};

