#include "AzureADOAuth2BrowserAuthenticator.h"
#include "AzureSession.h"
#include "CosmosUtil.h"
#include "CreateDeploymentRequester.h"
#include "DbAccountListRequester.h"
#include "DbAccountUsageRequester.h"
#include "DbListRequester.h"
#include "DeleteDbAccountRequester.h"
#include "DlgBrowserOpener.h"
#include "DlgLoading.h"
#include "DlgSelectCosmosDbAccount.h"
#include "DlgSelectSubscriptionAndResourceGroup.h"
#include "GetDbAccountKeysRequester.h"
#include "GetDbAccountRequester.h"
#include "GetDeploymentRequester.h"
#include "MFCUtil.h"
#include "NetworkUtils.h"
#include "NumberUtil.h"
#include "ResourceGroupListRequester.h"
#include "RESTAuthenticationResult.h"
#include "RestUtils.h"
#include "RunOnScopeEnd.h"
#include "Sapio365Session.h"
#include "SubscriptionsListRequester.h"
#include "YCallbackMessage.h"
#include "YCodeJockMessageBox.h"
#include "YSafeCreateTask.h"

#include "sha256/md5-lib.h"

void CosmosUtil::CreateCosmosDbAccount(const shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent, YAdvancedHtmlView* p_HtmlView)
{
	p_Parent->EnableWindow(FALSE);

	auto azureSession = p_Session->GetAzureSession();

	auto taskFinisher = std::make_shared<RunOnScopeEnd>([p_Session, p_Parent]() {
		// Remove remaining reference to azure session
		p_Session->RemoveAzureSession();

		// Re-enable disabled window
		YCallbackMessage::DoPost([p_Parent]() {
			p_Parent->EnableWindow(TRUE);
		});
	});

	auto result = azureSession->Start(p_Parent).Then([p_Session, taskFinisher, p_Parent, p_HtmlView](RestAuthenticationResult p_Result) {
		if (p_Result.state == RestAuthenticationResult::State::Success)
		{
			try
			{
				// Get all subscriptions
				SubscriptionsListRequester subscriptionsReq;
				subscriptionsReq.Send(p_Session, YtriaTaskData()).GetTask().wait();
				
				// Get all resourceGroups
				bool hasSubscriptions = false;
				bool hasResGroup = false;
				map<Azure::Subscription, vector<Azure::ResourceGroup>> dlgChoices;
				for (const auto& subscription : subscriptionsReq.GetData())
				{
					if (subscription.m_State && *subscription.m_State == _YTEXT("Enabled"))
					{
						hasSubscriptions = true;

						ResourceGroupListRequester resReq(subscription.m_SubscriptionId ? *subscription.m_SubscriptionId : _YTEXT(""));
						resReq.Send(p_Session, YtriaTaskData()).GetTask().wait();
						for (const auto& resGroup : resReq.GetData())
						{
							dlgChoices[subscription].push_back(resGroup);
							hasResGroup = true;
						}
					}
				}

				if (!hasSubscriptions || !hasResGroup)
				{
					YCallbackMessage::DoPost([p_Parent]() {
						YCodeJockMessageBox dlg(p_Parent,
							DlgMessageBox::eIcon_ExclamationWarning,
							YtriaTranslate::DoError(DlgError_DlgError_1, _YLOC("Error"),_YR("Y2407")).c_str(),
							YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_11, _YLOC("This tenant has no subscriptions and/or resource groups.")).c_str(),
							_YTEXT(""),
							{ { IDOK, YtriaTranslate::Do(CosmosUtil__detail_DeleteCosmosDbAccountImpl_1, _YLOC("OK")).c_str() } });
						dlg.DoModal();
					});
					return;
				}

				YCallbackMessage::DoPost([dlgChoices, p_Session, p_Parent, p_HtmlView, taskFinisher]() {
					DlgSelectSubscriptionAndResourceGroup dlgNewAccount(DlgSelectSubscriptionAndResourceGroup::Mode::CREATION, dlgChoices, p_Session, p_Parent);
					auto ret = dlgNewAccount.DoModal();
					if (ret == IDOK)
					{
						YCallbackMessage::DoPost([p_Parent]() {
							p_Parent->EnableWindow(FALSE);
						});
						
						MD5 md5;
						wstring deploymentName = wstring(_YTEXT("ytria-cosmosDB-create-")) +
							MFCUtil::convertASCII_to_UNICODE(md5(MFCUtil::convertUNICODE_to_ASCII(detail::GetDateTimeNow() + Str::GeneratePassword(8, true))));
						
						wstring subscriptionId = dlgNewAccount.GetSubscriptionId();
						wstring resGroupName = dlgNewAccount.GetResGroupName();

						auto dlgLoading = std::make_shared<DlgLoading>(YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_12, _YLOC("Creating deployment...")).c_str());
						YSafeCreateTask([=]() {
							try
							{
								YCallbackMessage::DoPost([dlgLoading]() {
									dlgLoading->DoModal();
								});
								// Create CosmosDB account
								Azure::CreateDeploymentRequester createDepReq(subscriptionId, resGroupName, deploymentName);
								createDepReq.Send(p_Session, YtriaTaskData()).GetTask().wait();

								wstring cosmosAccountName;
								wstring endpoint;
								wstring primaryKey;

								dlgLoading->SetText(YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_13, _YLOC("Waiting for deployment to complete (this might take a few minutes)...")).c_str());
								// Wait for creation to be done
								bool created = false;
								while (!created)
								{
									Azure::GetDeploymentRequester getDepReq(subscriptionId, resGroupName, deploymentName);
									getDepReq.Send(p_Session, YtriaTaskData()).GetTask().wait();

									wstring state = getDepReq.GetData().m_ProvisioningState ? *getDepReq.GetData().m_ProvisioningState : _YTEXT("");
									if (state == _YTEXT("Failed"))
										throw std::exception(MFCUtil::convertUNICODE_to_UTF8(getDepReq.GetData().m_ErrorMessage ? *getDepReq.GetData().m_ErrorMessage : _YTEXT("")).c_str());

									created = state == _YTEXT("Succeeded");
									if (created)
										cosmosAccountName = getDepReq.GetData().m_CosmosDbAccountName ? *getDepReq.GetData().m_CosmosDbAccountName : _YTEXT("");
									else
										pplx::wait(2500);
								}

								// Get info about the cosmosdb account
								dlgLoading->SetText(YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_14, _YLOC("Querying info about the new database account...")).c_str());
								Azure::GetDbAccountRequester dbAccRequester(subscriptionId, resGroupName, cosmosAccountName);
								dbAccRequester.Send(p_Session, YtriaTaskData()).GetTask().wait();
								auto data = dbAccRequester.GetData();
								ASSERT(data.m_WriteLocations != boost::none);
								if (data.m_WriteLocations != boost::none)
								{
									auto writeLocs = *data.m_WriteLocations;
									ASSERT(!writeLocs.empty());
									if (!writeLocs.empty())
										endpoint = writeLocs[0].m_DocumentEndpoint ? *writeLocs[0].m_DocumentEndpoint : _YTEXT("");

								}
								// Get keys for this account
								dlgLoading->SetText(YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_15, _YLOC("Querying keys for the new database account...")).c_str());
								Azure::GetDbAccountKeysRequester accKeysReq(subscriptionId, resGroupName, cosmosAccountName);
								accKeysReq.Send(p_Session, YtriaTaskData()).GetTask().wait();
								primaryKey = accKeysReq.GetData().m_PrimaryMasterKey ? *accKeysReq.GetData().m_PrimaryMasterKey : _YTEXT("");

								YCallbackMessage::DoPost([dlgLoading, p_Parent, endpoint, primaryKey, p_HtmlView, taskFinisher]() {
									// Update credentials dialog
									p_HtmlView->ExecuteScript(_YTEXTFORMAT(LR"~(setCredentials("%s", "%s");)~", endpoint.c_str(), primaryKey.c_str()));
									dlgLoading->EndDialog(IDOK);
								});
							}
							catch (const RestException & e)
							{
								YCallbackMessage::DoPost([e, p_Parent]() {
									YCodeJockMessageBox dlg(p_Parent,
										DlgMessageBox::eIcon_Error,
										YtriaTranslate::DoError(CosmosUtil_CreateCosmosDbAccount_17, _YLOC("Error occurred"),_YR("Y2408")).c_str(),
										e.WhatUnicode(),
										_YTEXT(""),
										{ { IDOK, YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_1, _YLOC("OK")).c_str() } });
									dlg.DoModal();
								});
							}
							catch (const std::exception & e)
							{
								YCallbackMessage::DoPost([e, p_Parent]() {
									YCodeJockMessageBox dlg(p_Parent,
										DlgMessageBox::eIcon_Error,
										YtriaTranslate::DoError(CosmosUtil_CreateCosmosDbAccount_17, _YLOC("Error occurred"),_YR("Y2409")).c_str(),
										MFCUtil::convertUTF8_to_UNICODE(e.what()),
										_YTEXT(""),
										{ { IDOK, YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_1, _YLOC("OK")).c_str() } });
									dlg.DoModal();
								});
							}
						});
					}
				});
			}
			catch (const RestException& e)
			{
				YCodeJockMessageBox dlg(p_Parent,
					DlgMessageBox::eIcon_Error,
					YtriaTranslate::DoError(CosmosUtil_CreateCosmosDbAccount_17, _YLOC("Error occurred"),_YR("Y2410")).c_str(),
					e.WhatUnicode(),
					_YTEXT(""),
					{ { IDOK, YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_1, _YLOC("OK")).c_str() } });
				dlg.DoModal();
			}
			catch (const std::exception& e)
			{
				YCodeJockMessageBox dlg(p_Parent,
					DlgMessageBox::eIcon_Error,
					YtriaTranslate::DoError(CosmosUtil_CreateCosmosDbAccount_17, _YLOC("Error occurred"),_YR("Y2411")).c_str(),
					MFCUtil::convertUTF8_to_UNICODE(e.what()),
					_YTEXT(""),
					{ { IDOK, YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_1, _YLOC("OK")).c_str() } });
				dlg.DoModal();
			}
		}
	});
}

TaskWrapper<void> CosmosUtil::DeleteCosmosDbAccount(const shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent, YAdvancedHtmlView* p_HtmlView)
{
	auto azureSession = p_Session->GetAzureSession();

	return azureSession->Start(p_Parent).Then([p_Session, p_Parent, p_HtmlView](RestAuthenticationResult p_Result) {
		if (p_Result.state == RestAuthenticationResult::State::Success)
		{
			detail::DeleteCosmosDbAccountImpl(p_Session, p_Parent, p_HtmlView);
			p_Session->RemoveAzureSession(); // Remove remaining reference to azure session
		}
	});
}

TaskWrapper<void> CosmosUtil::SelectCosmosDbAccount(const shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent, YAdvancedHtmlView* p_HtmlView)
{
	auto azureSession = p_Session->GetAzureSession();

	return azureSession->Start(p_Parent).Then([p_Session, p_Parent, p_HtmlView](RestAuthenticationResult p_Result) {
		if (p_Result.state == RestAuthenticationResult::State::Success)
		{
			detail::SelectCosmosDbAccountImpl(p_Session, p_Parent, p_HtmlView);
			p_Session->RemoveAzureSession(); // Remove remaining reference to azure session
		}
	});
}

wstring CosmosUtil::detail::GetDateTimeNow()
{
	YTimeDate now = YTimeDate::GetCurrentTimeDate();
	CString nowText;
	DateTimeFormat f;
	TimeUtil::GetInstance().ConvertDateToText(DateTimeFormat(), now, nowText);

	return wstring(nowText);
}

void CosmosUtil::detail::DeleteCosmosDbAccountImpl(const shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent, YAdvancedHtmlView* p_HtmlView)
{
	YCallbackMessage::DoSend(p_Parent, [p_Session, p_Parent, p_HtmlView]()
	{
		try
		{
			// Get all subscriptions
			SubscriptionsListRequester subscriptionsReq;
			subscriptionsReq.Send(p_Session, YtriaTaskData()).GetTask().wait();

			// Get all resourceGroups
			bool hasSubscriptions = false;
			bool hasResGroup = false;
			map<Azure::Subscription, vector<Azure::ResourceGroup>> dlgChoices;
			for (const auto& subscription : subscriptionsReq.GetData())
			{
				if (subscription.m_State && *subscription.m_State == _YTEXT("Enabled"))
				{
					hasSubscriptions = true;

					ResourceGroupListRequester resReq(subscription.m_SubscriptionId ? *subscription.m_SubscriptionId : _YTEXT(""));
					resReq.Send(p_Session, YtriaTaskData()).GetTask().wait();
					for (const auto& resGroup : resReq.GetData())
					{
						dlgChoices[subscription].push_back(resGroup);
						hasResGroup = true;
					}
				}
			}

			if (!hasSubscriptions || !hasResGroup)
			{
				YCallbackMessage::DoPost([p_Parent]() {
					YCodeJockMessageBox dlg(p_Parent,
						DlgMessageBox::eIcon_ExclamationWarning,
						YtriaTranslate::DoError(DlgError_DlgError_1, _YLOC("Error"),_YR("Y2412")).c_str(),
						YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_11, _YLOC("This tenant has no subscriptions and/or resource groups.")).c_str(),
						_YTEXT(""),
						{ { IDOK, YtriaTranslate::Do(CosmosUtil__detail_DeleteCosmosDbAccountImpl_1, _YLOC("OK")).c_str() } });
					dlg.DoModal();
					});
				return;
			}
			DlgSelectSubscriptionAndResourceGroup dlgSelectSubResGroup(DlgSelectSubscriptionAndResourceGroup::Mode::DELETION, dlgChoices, p_Session, p_Parent);

			dlgSelectSubResGroup.SetOnOkValidation([p_Session, p_Parent, p_HtmlView](auto& p_Dlg)
				{
					const auto& subscriptionId = p_Dlg.GetSubscriptionId();
					const auto& resGroupName = p_Dlg.GetResGroupName();
					// For each db account, get size info and number of databases
					Azure::DbAccountListRequester dbAccListRequester(subscriptionId, resGroupName);

					try
					{
						dbAccListRequester.Send(p_Session, YtriaTaskData()).GetTask().wait();
						return detail::ShowCosmosDbDeleteDialog(subscriptionId, resGroupName, p_Session, dbAccListRequester.GetData(), p_Parent, p_HtmlView);
					}
					catch (const RestException & e)
					{
						YCodeJockMessageBox dlg(p_Parent,
							DlgMessageBox::eIcon_Error,
							YtriaTranslate::DoError(CosmosUtil_CreateCosmosDbAccount_17, _YLOC("Error occurred"), _YR("Y2413")).c_str(),
							e.WhatUnicode(),
							_YTEXT(""),
							{ { IDOK, YtriaTranslate::Do(CosmosUtil__detail_DeleteCosmosDbAccountImpl_1, _YLOC("OK")).c_str() } });
						dlg.DoModal();
					}
					catch (const std::exception & e)
					{
						YCodeJockMessageBox dlg(p_Parent,
							DlgMessageBox::eIcon_Error,
							YtriaTranslate::DoError(CosmosUtil_CreateCosmosDbAccount_17, _YLOC("Error occurred"), _YR("Y2414")).c_str(),
							MFCUtil::convertUTF8_to_UNICODE(e.what()),
							_YTEXT(""),
							{ { IDOK, YtriaTranslate::Do(CosmosUtil__detail_DeleteCosmosDbAccountImpl_1, _YLOC("OK")).c_str() } });
						dlg.DoModal();
					}

					return false;
				});

			dlgSelectSubResGroup.DoModal();
		}
		catch (const RestException & e)
		{
			YCodeJockMessageBox dlg(p_Parent,
				DlgMessageBox::eIcon_Error,
				YtriaTranslate::DoError(CosmosUtil_CreateCosmosDbAccount_17, _YLOC("Error occurred"),_YR("Y2413")).c_str(),
				e.WhatUnicode(),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(CosmosUtil__detail_DeleteCosmosDbAccountImpl_1, _YLOC("OK")).c_str() } });
			dlg.DoModal();
		}
		catch (const std::exception & e)
		{
			YCodeJockMessageBox dlg(p_Parent,
				DlgMessageBox::eIcon_Error,
				YtriaTranslate::DoError(CosmosUtil_CreateCosmosDbAccount_17, _YLOC("Error occurred"),_YR("Y2414")).c_str(),
				MFCUtil::convertUTF8_to_UNICODE(e.what()),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(CosmosUtil__detail_DeleteCosmosDbAccountImpl_1, _YLOC("OK")).c_str() } });
			dlg.DoModal();
		}
	});
}

void CosmosUtil::detail::SelectCosmosDbAccountImpl(const shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent, YAdvancedHtmlView* p_HtmlView)
{
	YCallbackMessage::DoSend(p_Parent, [p_Session, p_Parent, p_HtmlView]()
	{
		try
		{
			// Get all subscriptions
			SubscriptionsListRequester subscriptionsReq;
			subscriptionsReq.Send(p_Session, YtriaTaskData()).GetTask().wait();

			// Get all resourceGroups
			bool hasSubscriptions = false;
			bool hasResGroup = false;
			map<Azure::Subscription, vector<Azure::ResourceGroup>> dlgChoices;
			for (const auto& subscription : subscriptionsReq.GetData())
			{
				if (subscription.m_State && *subscription.m_State == _YTEXT("Enabled"))
				{
					hasSubscriptions = true;

					ResourceGroupListRequester resReq(subscription.m_SubscriptionId ? *subscription.m_SubscriptionId : _YTEXT(""));
					resReq.Send(p_Session, YtriaTaskData()).GetTask().wait();
					for (const auto& resGroup : resReq.GetData())
					{
						dlgChoices[subscription].push_back(resGroup);
						hasResGroup = true;
					}
				}
			}

			if (!hasSubscriptions || !hasResGroup)
			{
				YCodeJockMessageBox dlg(p_Parent,
					DlgMessageBox::eIcon_ExclamationWarning,
					_T("Error"),
					_T("This tenant has no subscriptions and/or resource groups."),
					_YTEXT(""),
					{ { IDOK, _T("OK") } });
				dlg.DoModal();
				return;
			}

			DlgSelectSubscriptionAndResourceGroup dlgSelectSubResGroup(DlgSelectSubscriptionAndResourceGroup::Mode::SELECTION, dlgChoices, p_Session, p_Parent);

			dlgSelectSubResGroup.SetOnOkValidation([p_Session, p_HtmlView](auto& p_Dlg)
				{
					const auto& subscriptionId = p_Dlg.GetSubscriptionId();
					const auto& resGroupName = p_Dlg.GetResGroupName();

					Azure::DbAccountListRequester dbAccListRequester(subscriptionId, resGroupName);
					try
					{
						dbAccListRequester.Send(p_Session, YtriaTaskData()).GetTask().wait();
						return detail::ShowCosmosDbSelectDialog(subscriptionId, resGroupName, p_Session, dbAccListRequester.GetData(), &p_Dlg, p_HtmlView);
					}
					catch (const RestException & e)
					{
						YCodeJockMessageBox dlg(&p_Dlg,
							DlgMessageBox::eIcon_Error,
							_T("Error occurred"),
							e.WhatUnicode(),
							_YTEXT(""),
							{ { IDOK, _T("OK") } });
						dlg.DoModal();
					}
					catch (const std::exception & e)
					{
						YCodeJockMessageBox dlg(&p_Dlg,
							DlgMessageBox::eIcon_Error,
							_T("Error occurred"),
							MFCUtil::convertUTF8_to_UNICODE(e.what()),
							_YTEXT(""),
							{ { IDOK, _T("OK") } });
						dlg.DoModal();
					}
					return false;
				});

			dlgSelectSubResGroup.DoModal();
		}
		catch (const RestException& e)
		{
			YCodeJockMessageBox dlg(p_Parent,
				DlgMessageBox::eIcon_Error,
				_T("Error occurred"),
				e.WhatUnicode(),
				_YTEXT(""),
				{ { IDOK, _T("OK") } });
			dlg.DoModal();
		}
		catch (const std::exception& e)
		{
			YCodeJockMessageBox dlg(p_Parent,
				DlgMessageBox::eIcon_Error,
				_T("Error occurred"),
				MFCUtil::convertUTF8_to_UNICODE(e.what()),
				_YTEXT(""),
				{ { IDOK, _T("OK") } });
			dlg.DoModal();
		}
	});
}

bool CosmosUtil::detail::ShowCosmosDbDeleteDialog(const wstring& p_SubscriptionId,
	const wstring& p_ResGroup,
	const shared_ptr<Sapio365Session>& p_Session,
	const vector<Azure::CosmosDbAccount>& p_Accounts,
	CWnd* p_Parent,
	YAdvancedHtmlView* p_HtmlView)
{
	bool cancelOrError = false;

	wstring error;
	vector<CosmosDbAccountInfo> accountInfos;
	DlgLoading::RunModal(YtriaTranslate::Do(CosmosUtil__detail_ShowCosmosDbDeleteDialog_12, _YLOC("Querying info from the databases...")).c_str()
		, p_Parent
		, [&accountInfos, &error, accList = p_Accounts, subscriptionId = p_SubscriptionId, resGroupName = p_ResGroup, p_Session, p_Parent, p_HtmlView](auto dlgLoading)
		{
			try
			{
				
				for (const auto& account : accList)
				{
					wstring name = account.m_Name ? *account.m_Name : _YTEXT("");

					// get size info
					Azure::DbAccountUsageRequester usageRequester(subscriptionId, resGroupName, name);
					usageRequester.Send(p_Session, YtriaTaskData()).GetTask().wait();

					// get number of databases
					Azure::DbListRequester dbListReq(subscriptionId, resGroupName, name);
					dbListReq.Send(p_Session, YtriaTaskData()).GetTask().wait();

					CosmosDbAccountInfo info;
					info.m_Name = name;
					info.m_HasDatabase = !dbListReq.GetData().empty();

					info.m_OnlyHasYtriaDatabase = std::all_of(dbListReq.GetData().begin(), dbListReq.GetData().end(), [](const Azure::CosmosDb & p_Db) 
					{
						ASSERT(p_Db.m_Id);
						auto dbId = p_Db.m_Id ? *p_Db.m_Id : _YTEXT("");
						return Str::endsWith(dbId, _YTEXT(".ytr"));
					});

					const auto& usage = usageRequester.GetData();
					if (usage.size() == 1)
					{
						ASSERT(usage[0].m_Name != boost::none && *usage[0].m_Name == _YTEXT("Storage"));
						if (usage[0].m_Name != boost::none && *usage[0].m_Name == _YTEXT("Storage"))
						{
							NumberFormat fmt;
							fmt.SetDisplay(NumberFormat::BYTES);
							fmt.ClearFlags();
							ASSERT(usage[0].m_CurrentValue != boost::none);
							if (usage[0].m_CurrentValue != boost::none)
								info.m_Info = wstring(YtriaTranslate::Do(CosmosUtil__detail_ShowCosmosDbDeleteDialog_13, _YLOC("Size of data: ")).c_str()) + NumberUtil::GetInstance().ConvertNumberToText(fmt, *usage[0].m_CurrentValue);
						}

					}
					accountInfos.push_back(info);
				}
			}
			catch (const RestException & e)
			{
				error = e.WhatUnicode();
			}
			catch (const std::exception & e)
			{
				error = MFCUtil::convertUTF8_to_UNICODE(e.what());
			}
		});

	if (error.empty())
	{
		// Select db accounts to delete
		DlgSelectCosmosDbAccount dlgSelectAccount(DlgSelectCosmosDbAccount::Mode::DELETION, accountInfos, p_Session, p_Parent);
		dlgSelectAccount.SetOnOkValidation([](auto& dlg)
			{
				const auto& dbNameToDelete = dlg.GetDbAccountName();

				YCodeJockMessageBox conf(&dlg,
					DlgMessageBox::eIcon_ExclamationWarning,
					_YTEXT("Confirm database deletion"),
					YtriaTranslate::Do(CosmosUtil__detail_ShowCosmosDbDeleteDialog_18, _YLOC("Are you sure you want to delete database \"%1\"?\r\nPlease type the name of the database to confirm."), dbNameToDelete.c_str()),
					_YTEXT(""), { {IDOK, YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_1, _YLOC("OK")).c_str()}, {IDCANCEL, YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str()} });
				conf.AddInputBox();

				auto res = conf.DoModal();
				while (res == IDOK && dbNameToDelete != wstring(conf.GetInputBoxText()))
					res = conf.DoModal();

				return res == IDOK;
			});

		if (dlgSelectAccount.DoModal() == IDOK)
		{
			try
			{
				Azure::DeleteDbAccountRequester deleteReq(p_SubscriptionId, p_ResGroup, dlgSelectAccount.GetDbAccountName());
				deleteReq.Send(p_Session, YtriaTaskData()).GetTask().wait();

				auto updatedAccList = p_Accounts;
				for (auto itt = updatedAccList.begin(); itt != updatedAccList.end(); ++itt)
				{
					if (itt->m_Name != boost::none && *itt->m_Name == dlgSelectAccount.GetDbAccountName())
					{
						updatedAccList.erase(itt);
						break;
					}
				}

				cancelOrError = detail::ShowCosmosDbDeleteDialog(p_SubscriptionId, p_ResGroup, p_Session, updatedAccList, p_Parent, p_HtmlView);
			}
			catch (const RestException & e)
			{
				error = e.WhatUnicode();
			}
			catch (const std::exception & e)
			{
				error = MFCUtil::convertUTF8_to_UNICODE(e.what());
			}
		}
		else
		{
			cancelOrError = true;
		}
	}

	if (!error.empty())
	{
		YCodeJockMessageBox dlg(p_Parent,
			DlgMessageBox::eIcon_Error,
			YtriaTranslate::DoError(CosmosUtil_CreateCosmosDbAccount_17, _YLOC("Error occurred"), _YR("Y2417")).c_str(),
			error,
			_YTEXT(""),
			{ { IDOK, YtriaTranslate::Do(CosmosUtil__detail_DeleteCosmosDbAccountImpl_1, _YLOC("OK")).c_str() } });
		dlg.DoModal();
		cancelOrError = true;
	}

	return !cancelOrError;
}

bool CosmosUtil::detail::ShowCosmosDbSelectDialog(const wstring& p_SubscriptionId, const wstring& p_ResGroup, const shared_ptr<Sapio365Session>& p_Session, const vector<Azure::CosmosDbAccount>& p_Accounts, CWnd* p_Parent, YAdvancedHtmlView* p_HtmlView)
{
	bool cancelOrError = false;

	wstring error;
	vector<CosmosDbAccountInfo> accountInfos;
	DlgLoading::RunModal(YtriaTranslate::Do(CosmosUtil__detail_ShowCosmosDbDeleteDialog_12, _YLOC("Querying info from the databases...")).c_str()
		, p_Parent
		, [&error , &accountInfos, accList = p_Accounts, subscriptionId = p_SubscriptionId, resGroupName = p_ResGroup, p_Session, p_Parent, p_HtmlView](auto dlgLoading) mutable
		{
			try
			{
				for (const auto& account : accList)
				{
					wstring name = account.m_Name ? *account.m_Name : _YTEXT("");

					// get size info
					Azure::DbAccountUsageRequester usageRequester(subscriptionId, resGroupName, name);
					usageRequester.Send(p_Session, YtriaTaskData()).GetTask().wait();

					// get number of databases
					Azure::DbListRequester dbListReq(subscriptionId, resGroupName, name);
					dbListReq.Send(p_Session, YtriaTaskData()).GetTask().wait();

					CosmosDbAccountInfo info;
					info.m_Name = name;
					info.m_HasDatabase = !dbListReq.GetData().empty();

					info.m_OnlyHasYtriaDatabase = std::all_of(dbListReq.GetData().begin(), dbListReq.GetData().end(), [](const Azure::CosmosDb& p_Db) 
					{
						ASSERT(p_Db.m_Id);
						auto dbId = p_Db.m_Id ? *p_Db.m_Id : _YTEXT("");
						return Str::endsWith(dbId, _YTEXT(".ytr"));
					});

					const auto& usage = usageRequester.GetData();
					if (usage.size() == 1)
					{
						ASSERT(usage[0].m_Name != boost::none && *usage[0].m_Name == _YTEXT("Storage"));
						if (usage[0].m_Name != boost::none && *usage[0].m_Name == _YTEXT("Storage"))
						{
							NumberFormat fmt;
							fmt.SetDisplay(NumberFormat::BYTES);
							fmt.ClearFlags();
							ASSERT(usage[0].m_CurrentValue != boost::none);
							if (usage[0].m_CurrentValue != boost::none)
								info.m_Info = wstring(YtriaTranslate::Do(CosmosUtil__detail_ShowCosmosDbDeleteDialog_13, _YLOC("Size of data: ")).c_str()) + NumberUtil::GetInstance().ConvertNumberToText(fmt, *usage[0].m_CurrentValue);
						}
					}
					accountInfos.push_back(info);
				}
			}
			catch (const RestException & e)
			{
				error = e.WhatUnicode();
			}
			catch (const std::exception & e)
			{
				error = MFCUtil::convertUTF8_to_UNICODE(e.what());
			}
		}
	);

	if (error.empty())
	{
		bool hasAtLeastOneYtriaDb = std::any_of(accountInfos.begin(), accountInfos.end(), [](const CosmosDbAccountInfo& info) { return info.m_HasDatabase && info.m_OnlyHasYtriaDatabase; });
		if (!hasAtLeastOneYtriaDb)
		{
			YCodeJockMessageBox dlg(p_Parent,
				DlgMessageBox::eIcon_Information,
				_T("No account with Ytria databases found"),
				_T("Please create a new CosmosDB account from sapio365."),
				_YTEXT(""),
				{ { IDOK, _T("OK") } });
			dlg.DoModal();
			cancelOrError = true;
		}
		else
		{
			DlgSelectCosmosDbAccount dlgSelectAccount(DlgSelectCosmosDbAccount::Mode::SELECTION, accountInfos, p_Session, p_Parent, true);

			if (dlgSelectAccount.DoModal() == IDOK)
			{
				const auto dbAccountNameSelected = dlgSelectAccount.GetDbAccountName();

				wstring url;
				wstring primaryKey;
				DlgLoading::RunModal(_T("Querying info for selected database account...")
					, p_Parent
					, [&error, &url, &primaryKey, subscriptionId = p_SubscriptionId, resGroupName = p_ResGroup, p_Session, p_Parent, p_HtmlView, dbAccountNameSelected](auto dlgLoading) mutable
					{
						try
						{
							Azure::GetDbAccountRequester dbAccRequester(subscriptionId, resGroupName, dbAccountNameSelected);
							dbAccRequester.Send(p_Session, YtriaTaskData()).GetTask().wait();
							auto data = dbAccRequester.GetData();
							ASSERT(data.m_WriteLocations != boost::none);
							if (data.m_WriteLocations != boost::none)
							{
								auto writeLocs = *data.m_WriteLocations;
								ASSERT(!writeLocs.empty());
								if (!writeLocs.empty())
									url = writeLocs[0].m_DocumentEndpoint ? *writeLocs[0].m_DocumentEndpoint : _YTEXT("");
							}
							// Get keys for this account
							dlgLoading->SetText(_T("Querying keys for selected database account..."));
							Azure::GetDbAccountKeysRequester accKeysReq(subscriptionId, resGroupName, dbAccountNameSelected);
							accKeysReq.Send(p_Session, YtriaTaskData()).GetTask().wait();
							primaryKey = accKeysReq.GetData().m_PrimaryMasterKey ? *accKeysReq.GetData().m_PrimaryMasterKey : _YTEXT("");
						}
						catch (const RestException & e)
						{
							error = e.WhatUnicode();
						}
						catch (const std::exception & e)
						{
							error = MFCUtil::convertUTF8_to_UNICODE(e.what());
						}
					});

				if (error.empty())
				{
					p_HtmlView->ExecuteScript(_YTEXTFORMAT(LR"~(setCredentials("%s", "%s");)~", url.c_str(), primaryKey.c_str()));
					p_Session->RemoveAzureSession();
				}
			}
			else
			{
				cancelOrError = true;
			}
		}
	}

	if (!error.empty())
	{
		YCodeJockMessageBox dlg(p_Parent,
			DlgMessageBox::eIcon_Error,
			YtriaTranslate::DoError(CosmosUtil_CreateCosmosDbAccount_17, _YLOC("Error occurred"), _YR("Y2417")).c_str(),
			error,
			_YTEXT(""),
			{ { IDOK, YtriaTranslate::Do(CosmosUtil__detail_DeleteCosmosDbAccountImpl_1, _YLOC("OK")).c_str() } });
		dlg.DoModal();

		cancelOrError = true;
	}

	return !cancelOrError;
}
