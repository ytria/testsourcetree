#pragma once

#include "ISqlQuery.h"
#include "SessionIdentifier.h"

class SessionsSqlEngine;

class QuerySessionExists : public ISqlQuery
{
public:
	QuerySessionExists(const SessionIdentifier& p_Identifier, SessionsSqlEngine& m_Engine);
	void Run() override;

	bool Exists() const;
	int64_t GetSessionId() const;

private:
	SessionsSqlEngine& m_Engine;
	SessionIdentifier m_Identifier;
	int64_t m_SessionId;
	bool m_Exists;
};

