#pragma once

template <typename T>
void AttachmentInfo::Construct(T& p_Grid, const wstring& p_AttachmentId, GridBackendRow* p_Row)
{
	m_Row = p_Row;
	ASSERT(nullptr != m_Row);
	if (nullptr != m_Row)
	{
		UserOrGroupID() = m_Row->GetField(p_Grid.GetColMetaID()).GetValueStr();
		EventOrMessageOrConversationID() = m_Row->GetField(p_Grid.m_ColId).GetValueStr();

		if (!p_AttachmentId.empty())
		{
			PooledString attachmentId;
			std::vector<PooledString>* attachmentIds = m_Row->GetField(p_Grid.m_ColAttachmentId).GetValuesMulti<PooledString>();
			if (nullptr != attachmentIds)
			{
				bool found = false;
				size_t index = 0;
				for (size_t i = 0; index < attachmentIds->size() && !found; ++i)
				{
					if (attachmentIds->operator[](i) == p_AttachmentId)
					{
						index = i;
						found = true;
					}
				}
				ASSERT(found);
				if (found)
					attachmentId = p_AttachmentId;
			}
			else // Already exploded row
			{
				attachmentId = p_AttachmentId;
			}

			AttachmentID() = attachmentId;
		}
	}
}


