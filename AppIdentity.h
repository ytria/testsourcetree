#pragma once

class AppIdentity
{
public:
	boost::YOpt<PooledString> m_AppId;
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<PooledString> m_ServicePrincipalId;
	boost::YOpt<PooledString> m_ServicePrincipalName;
};

