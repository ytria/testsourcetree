#pragma once

class EventStartCutOffCondition
{
public:
	EventStartCutOffCondition(const wstring& p_DateCutOffIso8601);
	bool operator()(const web::json::value& p_Json) const;

private:
	wstring m_DateCutOffIso8601;
};

