#include "ModuleContact.h"

#include "AutomationDataStructure.h"
#include "BusinessContact.h"
#include "BusinessObjectManager.h"
#include "FrameContacts.h"
#include "GraphCache.h"
#include "MSGraphCommonData.h"
#include "MSGraphUtil.h"
#include "Office365Admin.h"
#include "RefreshSpecificData.h"
#include "RESTUtils.h"
#include "RunOnScopeEnd.h"
#include "safeTaskCall.h"
#include "TaskDataManager.h"
#include "YDataCallbackMessage.h"
#include "BasicRequestLogger.h"
#include "MultiObjectsPageRequestLogger.h"

using namespace Rest;

void ModuleContact::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
    {
    case Command::ModuleTask::ListMe:
    {
        Command newCmd = p_Command;
        newCmd.GetCommandInfo().GetIds() = { PooledString(GetConnectedSession().GetConnectedUserId()) };
        showContacts(newCmd);
        break;
    }
    case Command::ModuleTask::List:
		showContacts(p_Command);
        break;
	case Command::ModuleTask::UpdateRefresh:
		refresh(p_Command);
		break;
    default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
        break;
    }
}

void ModuleContact::showContacts(const Command& p_Command)
{
    const auto&	info = p_Command.GetCommandInfo();
    auto&	p_IDs = info.GetIds();
    auto	p_Origin = info.GetOrigin();
    auto	p_SourceWindow = p_Command.GetSourceWindow();
    auto	p_Action = p_Command.GetAutomationAction();

    ASSERT(p_Origin != Origin::NotSet);
    if (p_Origin != Origin::NotSet)
    {
        auto sourceCWnd = p_SourceWindow.GetCWnd();
        if (!FrameContacts::CanCreateNewFrame(true, sourceCWnd, p_Action))
            return;

        ModuleCriteria moduleCriteria;
        moduleCriteria.m_Origin				= p_Origin;
        moduleCriteria.m_UsedContainer		= ModuleCriteria::UsedContainer::IDS;
        moduleCriteria.m_IDs				= p_IDs;
		moduleCriteria.m_Privilege			= info.GetRBACPrivilege();
		moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

        if (ShouldCreateFrame<FrameContacts>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
        {
            CWaitCursor _;
            const wstring title = p_Command.GetTask() == Command::ModuleTask::ListMe
                ? _T("My Contacts")
                : YtriaTranslate::Do(ModuleContact_showContacts_1, _YLOC("Contacts")).c_str();
			FrameContacts* newFrame = new FrameContacts(p_Command.GetTask() == Command::ModuleTask::ListMe, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), title, *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
            newFrame->InitModuleCriteria(moduleCriteria);
            AddGridFrame(newFrame);
            newFrame->Erect();
            auto taskData = addFrameTask(YtriaTranslate::Do(ModuleContact_showContacts_2, _YLOC("Show Contacts")).c_str(), newFrame, p_Command, true);
            doRefresh(newFrame, moduleCriteria, p_Action, taskData, false, RefreshSpecificData());
        }
    }
    else
    {
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleContact_showContacts_3, _YLOC(" - origin not set"),_YR("Y2406")).c_str());
    }
}

void ModuleContact::refresh(const Command& p_Command)
{
	auto p_Action = p_Command.GetAutomationAction();

    auto existingFrame = dynamic_cast<FrameContacts*>(p_Command.GetCommandInfo().GetFrame());
    ASSERT(nullptr != existingFrame);
    if (nullptr != existingFrame)
    {
        auto taskData = addFrameTask(YtriaTranslate::Do(ModuleContact_refresh_1, _YLOC("Show Contacts")).c_str(), existingFrame, p_Command, false);

        RefreshSpecificData refreshSelectedData;
        if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
            refreshSelectedData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

        doRefresh(existingFrame, existingFrame->GetModuleCriteria(), p_Action, taskData, true, refreshSelectedData);
    }
    else
        SetAutomationGreenLight(p_Action, _YTEXT(""));
}

void ModuleContact::doRefresh(FrameContacts* p_pFrame, const ModuleCriteria& p_ModuleCriteria, AutomationAction * p_Action, YtriaTaskData p_TaskData, bool isUpdate, const RefreshSpecificData& p_RefreshData)
{
	YSafeCreateTask([this, p_TaskData,
        hwnd = p_pFrame->GetSafeHwnd(), p_Action, isUpdate, p_RefreshData, p_ModuleCriteria, bom = p_pFrame->GetBusinessObjectManager(), wantsAdditionalMetadata = (bool)p_pFrame->GetMetaDataColumnInfo()]()
    {
		ASSERT(!wantsAdditionalMetadata || p_ModuleCriteria.m_Origin == Origin::User);

		O365DataMap<BusinessUser, vector<BusinessContact>> contacts;

        const bool partialRefresh = !p_RefreshData.m_Criteria.m_IDs.empty();
        const auto& ids = partialRefresh ? p_RefreshData.m_Criteria.m_IDs : p_ModuleCriteria.m_IDs;

		auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("contacts"), ids.size());

		auto processUser = [&contacts, &bom, &p_TaskData, &logger, this](BusinessUser& user)
		{
			vector<BusinessContact> userContacts;
			if (!p_TaskData.IsCanceled())
			{
				logger->SetLogMessage(_T("contacts"));
				userContacts = bom->GetBusinessContacts(user.GetID(), logger, p_TaskData).GetTask().get();

				RunOnScopeEnd _([objName = logger->GetLogMessage(), logger]() { logger->SetLogMessage(objName); });
				logger->SetLogMessage(_T("contact folders"));

				auto userContactFolders = bom->GetBusinessContactFoldersHierarchy(user.GetID(), logger, p_TaskData).GetTask().get();

				for (const auto& innerContact : extractContacts(userContactFolders))
					userContacts.push_back(innerContact);
			}

			if (p_TaskData.IsCanceled())
			{
				user.SetFlags(BusinessObject::CANCELED);
				contacts[user] = {};
			}
			else
			{
				contacts[user] = std::move(userContacts);
			}
		};

		Util::ProcessSubUserItems(processUser, ids, p_ModuleCriteria.m_Origin, bom->GetGraphCache(), wantsAdditionalMetadata, logger, p_TaskData);

		YDataCallbackMessage<O365DataMap<BusinessUser, vector<BusinessContact>>>::DoPost(contacts, [hwnd, partialRefresh, p_TaskData, p_Action, isUpdate, isCanceled = p_TaskData.IsCanceled()](const O365DataMap<BusinessUser, vector<BusinessContact>>& p_Contacts)
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameContacts*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, p_TaskData, isCanceled && !everythingCanceled(p_Contacts)))
			{
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting contacts for %s users...", Str::getStringFromNumber(p_Contacts.size()).c_str()));
				frame->GetGrid().ClearLog(p_TaskData.GetId());

				{
					CWaitCursor _;
					frame->ShowContacts(p_Contacts, !partialRefresh);
				}

				if (!isUpdate)
				{
					frame->UpdateContext(p_TaskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, p_TaskData, p_Action);
			}
		});
    });
}

vector<BusinessContact> ModuleContact::extractContacts(const vector<BusinessContactFolder>& p_ContactFolders)
{
    vector<BusinessContact> contacts;

    for (const auto& contactFolder : p_ContactFolders)
    {
        const auto innerContacts = extractContactsInner(contactFolder);
		contacts.insert(contacts.begin(), innerContacts.begin(), innerContacts.end());
    }

    return contacts;
}

vector<BusinessContact> ModuleContact::extractContactsInner(const BusinessContactFolder& p_ContactFolder)
{
    vector<BusinessContact> contacts;

    for (const auto& contact : p_ContactFolder.GetContacts())
    {
		contacts.push_back(contact);
		contacts.back().m_ContactFolderName = p_ContactFolder.GetDisplayName();        
    }

    for (const auto& contactFolder : p_ContactFolder.GetChildrenContactFolders())
    {
		const auto innerContacts = extractContactsInner(contactFolder);
		contacts.insert(contacts.begin(), innerContacts.begin(), innerContacts.end());
    }

    return contacts;
}
