#pragma once

#include "BaseO365Grid.h"
#include "BusinessContact.h"
#include "GridTemplateUsers.h"
#include "MyDataMeRowHandler.h"

class FrameContacts;

class GridContacts : public ModuleO365Grid<BusinessContact>
{
public:
	GridContacts(bool p_IsMyData = false);

	virtual void customizeGrid() override;

    void BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessContact>>& p_Contacts, bool p_FullPurge);
    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;
	virtual wstring GetName(const GridBackendRow* p_Row) const override;

    bool IsMyData() const override;

protected:
	virtual BusinessContact getBusinessObject(GridBackendRow*) const;
	virtual void UpdateBusinessObjects(const vector<BusinessContact>& p_Contacts, bool p_SetModifiedStatus);
	virtual void RemoveBusinessObjects(const vector<BusinessContact>& p_Contacts);

private:
	GridBackendRow* FillRow(const BusinessContact& p_BusinessContact, GridBackendRow* p_ParentUserRow);
	void UpdateRow(const BusinessContact& p_BusinessContact, GridBackendRow* p_Row);
	bool UpdateRowForBusinessContact(const BusinessContact& p_Contact, bool p_SetModifiedStatus);
	bool RemoveRowForBusinessContact(const BusinessContact& p_Contact);

	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

private:
	GridTemplateUsers m_Template;

	GridBackendColumn* m_ColMetaContactFolderDisplayName;

    GridBackendColumn* m_ColFirstName;
    GridBackendColumn* m_ColDisplayName;
    GridBackendColumn* m_ColLastName;
	GridBackendColumn* m_ColMiddleName;
	GridBackendColumn* m_ColEmailAddresses;
	GridBackendColumn* m_ColEmailDisplayName;
	GridBackendColumn* m_ColManager;
    GridBackendColumn* m_ColJobTitle;
    GridBackendColumn* m_ColMobilePhone;
    GridBackendColumn* m_ColAssistantName;
    GridBackendColumn* m_ColBirthday;
    GridBackendColumn* m_ColBusinessAddressCity;
    GridBackendColumn* m_ColBusinessAddressCountry;
    GridBackendColumn* m_ColBusinessAddressPCode;
    GridBackendColumn* m_ColBusinessAddressState;
    GridBackendColumn* m_ColBusinessAddressStreet;
    GridBackendColumn* m_ColBusinessHomePage;
    GridBackendColumn* m_ColHomeAddressCity;
    GridBackendColumn* m_ColHomeAddressCountry;
    GridBackendColumn* m_ColHomeAddressPostalCode;
    GridBackendColumn* m_ColHomeAddressState;
    GridBackendColumn* m_ColHomeAddressStreet;
    GridBackendColumn* m_ColOtherAddressCity;
    GridBackendColumn* m_ColOtherAddressCountry;
    GridBackendColumn* m_ColOtherAddressPostalCode;
    GridBackendColumn* m_ColOtherAddressState;
    GridBackendColumn* m_ColOtherAddressStreet;
    GridBackendColumn* m_ColChangeKey;
    GridBackendColumn* m_ColCompanyName;
    GridBackendColumn* m_ColCreatedDateTime;
    GridBackendColumn* m_ColDepartment;
    GridBackendColumn* m_ColFileAs;
    GridBackendColumn* m_ColGeneration;
    GridBackendColumn* m_ColInitials;
    GridBackendColumn* m_ColLastModifiedDateTime;
    GridBackendColumn* m_ColNickname;
    GridBackendColumn* m_ColOfficeLocation;
    GridBackendColumn* m_ColParentFolderId;
    GridBackendColumn* m_ColPersonalNotes;
    GridBackendColumn* m_ColProfession;
    GridBackendColumn* m_ColSpouseName;
    GridBackendColumn* m_ColTitle;
    GridBackendColumn* m_ColYomiCompanyName;
    GridBackendColumn* m_ColYomiGivenName;
    GridBackendColumn* m_ColYomiSurnameName;
    GridBackendColumn* m_ColHomePhones;
    GridBackendColumn* m_ColIMAddresses;
    GridBackendColumn* m_ColChildren;
    GridBackendColumn* m_ColBusinessPhones;
    GridBackendColumn* m_ColCategories;

	GridBackendColumn* m_ColumnLastRequestDateTime;

    FrameContacts* m_Frame;

	bool m_IsMyData;

	bool m_HasLoadMoreAdditionalInfo;
	O365IdsContainer m_MetaIDsMoreLoaded;
	vector<GridBackendColumn*>	m_MetaColumns;

	boost::YOpt<MyDataMeRowHandler> m_MyDataMeHandler;
};
