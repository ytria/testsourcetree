#pragma once

#include "ModuleBase.h"

#include "BusinessContactFolder.h"
#include "CommandInfo.h"

class AutomationAction;
class FrameContacts;
class BusinessContact;
struct RefreshSpecificData;

class ModuleContact : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	virtual void executeImpl(const Command& p_Command);
	void showContacts(const Command& p_Command);
	void refresh(const Command& p_Command);
	void doRefresh(FrameContacts* p_pFrame, const ModuleCriteria& p_ModuleCriteria, AutomationAction * p_Action, YtriaTaskData p_TaskData, bool isUpdate, const RefreshSpecificData& p_RefreshData);

    vector<BusinessContact> extractContacts(const vector<BusinessContactFolder>& p_ContactFolders);
    vector<BusinessContact> extractContactsInner(const BusinessContactFolder& p_ContactFolders);
};