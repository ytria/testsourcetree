#include "Office365Admin.h"

#include "CommandDispatcher.h"
#include "FrameContacts.h"
#include "UpdatedObjectsGenerator.h"

IMPLEMENT_DYNAMIC(FrameContacts, GridFrameBase)

//BEGIN_MESSAGE_MAP(FrameContacts, GridFrameBase)
//END_MESSAGE_MAP()

FrameContacts::FrameContacts(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
    , m_contactsGrid(p_IsMyData)
{
    m_CreateAddRemoveToSelection = !p_IsMyData;
    m_CreateRefreshPartial = !p_IsMyData;
	m_CreateShowContext = !p_IsMyData;
}

void FrameContacts::ShowContacts(const O365DataMap<BusinessUser, vector<BusinessContact>>& p_Contacts, bool p_FullPurge)
{
	m_contactsGrid.BuildTreeView(p_Contacts, p_FullPurge);
}

void FrameContacts::createGrid()
{
	m_contactsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameContacts::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigUsers();
}

// returns false if no frame specific tab is needed.
bool FrameContacts::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	return GridFrameBase::customizeActionsRibbonTab(tab, !m_contactsGrid.IsMyData() && GetOrigin() == Origin::User, GetOrigin() == Origin::Group);
}

O365Grid& FrameContacts::GetGrid()
{
	return m_contactsGrid;
}

void FrameContacts::ApplyAllSpecific()
{
	ASSERT(false);
    /*auto updatedObjectsGen = UpdatedObjectsGenerator<BusinessContact>();
    updatedObjectsGen.BuildUpdatedObjects(GetGrid(), false);

    CommandInfo info;
    info.Data() = updatedObjectsGen;
    info.SetFrame(this);

    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Contact, Command::ModuleTask::UpdateModified, info, {}));*/
}

void FrameContacts::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    auto updatedObjectsGen = UpdatedObjectsGenerator<BusinessContact>();

    CommandInfo info;
    info.Data() = updatedObjectsGen;
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Contact, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameContacts::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Contact, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

AutomatedApp::AUTOMATIONSTATUS FrameContacts::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	return statusRV;
}