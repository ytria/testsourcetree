#include "GridContacts.h"

#include "AutomationWizardContacts.h"
#include "BasicGridSetup.h"
#include "BusinessUser.h"
#include "FrameContacts.h"
#include "MsGraphFieldNames.h"
#include "O365AdminUtil.h"
#include "GridUpdater.h"

#include "Gridutil.h"

GridContacts::GridContacts(bool p_IsMyData/* = false*/)
    : m_Frame(nullptr)
	, m_ColMetaContactFolderDisplayName(nullptr)
	, m_HasLoadMoreAdditionalInfo(false)
	, m_IsMyData(p_IsMyData)
{
	m_WithOwnerAsTopAncestor = true;

	/*UINT flags = 0;//O365Grid::ACTION_VIEW;
	if (CRMpipe().IsFeatureSandboxed())
		flags |= (O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT);*/
	UseDefaultActions(0/*flags*//*O365Grid::ACTION_CREATE | O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT | O365Grid::ACTION_VIEW*/);

	initWizard<AutomationWizardContacts>(_YTEXT("Automation\\Contacts"));

	if (IsMyData())
		m_MyDataMeHandler.emplace();
}

void GridContacts::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameContacts, LicenseUtil::g_codeSapio365contacts,
		{
			{ &m_Template.m_ColumnDisplayName, g_TitleOwnerDisplayName, g_FamilyOwner },
			{ &m_Template.m_ColumnUserPrincipalName, g_TitleOwnerUserName, g_FamilyOwner },
			{ &m_Template.m_ColumnID, g_TitleOwnerID, g_FamilyOwner }
		});
		setup.Setup(*this, true);
		m_ColumnLastRequestDateTime = setup.GetColumnLastRequestDateTime();
	}

	if (IsMyData())
		m_Template.m_ColumnDisplayName->RemovePresetFlags(g_ColumnsPresetDefault);

	m_MetaColumns.push_back(m_Template.m_ColumnID);
	m_MetaColumns.push_back(m_Template.m_ColumnDisplayName);
	m_MetaColumns.push_back(m_Template.m_ColumnUserPrincipalName);

	/*Add additional meta columns*/
	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		for (const auto& c : *metaDataColumnInfos)
		{
			ASSERT(nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()));
			if (nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()))
			{
				auto col = c.AddTo(*this, g_FamilyOwner);
				m_Template.GetColumnForProperty(c.GetPropertyName()) = col;
				m_MetaColumns.push_back(col);
				m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
			}
		}
	}

	setBasicGridSetupHierarchy({ { { m_Template.m_ColumnUserPrincipalName, 0 } }
								,{ rttr::type::get<BusinessContact>().get_id() }
								,{ rttr::type::get<BusinessContact>().get_id(), rttr::type::get<BusinessUser>().get_id() } });

	m_ColFirstName				= AddColumn(_YUID(O365_CONTACT_GIVENNAME),							YtriaTranslate::Do(GridContacts_customizeGrid_57, _YLOC("First name")).c_str(),					g_FamilyName,		g_ColumnSize22,	{ g_ColumnsPresetDefault });
    m_ColMiddleName				= AddColumn(_YUID(O365_CONTACT_MIDDLENAME),							YtriaTranslate::Do(GridContacts_customizeGrid_58, _YLOC("Middle name")).c_str(),					g_FamilyName,		g_ColumnSize22,	{ g_ColumnsPresetDefault });
    m_ColLastName				= AddColumn(_YUID(O365_CONTACT_SURNAME),							YtriaTranslate::Do(GridContacts_customizeGrid_59, _YLOC("Last name")).c_str(),					g_FamilyName,		g_ColumnSize22,	{ g_ColumnsPresetDefault });
    m_ColDisplayName			= AddColumn(_YUID(O365_CONTACT_DISPLAYNAME),						YtriaTranslate::Do(GridContacts_customizeGrid_60, _YLOC("Display Name")).c_str(),					g_FamilyName,		g_ColumnSize22);
 
	m_ColCreatedDateTime				= AddColumnDate(_YUID(O365_CONTACT_CREATEDDATETIME),		YtriaTranslate::Do(GridContacts_customizeGrid_61, _YLOC("Created On")).c_str(),					g_FamilyInfo,		g_ColumnSize16,	{ g_ColumnsPresetDefault });
	m_ColMetaContactFolderDisplayName	= AddColumn(_YUID("metaContactFolderDisplayName"),			YtriaTranslate::Do(GridContacts_customizeGrid_62, _YLOC("Contact Folder Display Nme")).c_str(),	g_FamilyInfo,		g_ColumnSize22,	{ g_ColumnsPresetDefault });
    m_ColParentFolderId					= AddColumn(_YUID(O365_CONTACT_PARENTFOLDERID),				YtriaTranslate::Do(GridContacts_customizeGrid_63, _YLOC("Parent Folder ID")).c_str(),				g_FamilyInfo,		g_ColumnSize12,	{ g_ColumnsPresetTech });
	m_ColCategories						= AddColumn(_YUID(O365_CONTACT_CATEGORIES),					YtriaTranslate::Do(GridContacts_customizeGrid_64, _YLOC("Categories")).c_str(),					g_FamilyInfo,		g_ColumnSize12);
	m_ColFileAs							= AddColumn(_YUID(O365_CONTACT_FILEAS),						YtriaTranslate::Do(GridContacts_customizeGrid_65, _YLOC("File As Display Name")).c_str(),			g_FamilyInfo,		g_ColumnSize12);
	m_ColLastModifiedDateTime			= AddColumnDate(_YUID(O365_CONTACT_LASTMODIFIEDDATETIME),	YtriaTranslate::Do(GridContacts_customizeGrid_66, _YLOC("Last Modified")).c_str(),				g_FamilyInfo,		g_ColumnSize16,	{ g_ColumnsPresetDefault });
	m_ColEmailDisplayName				= AddColumn(_YUID(O365_CONTACT_EMAILADDRESSESNAME),			YtriaTranslate::Do(GridContacts_customizeGrid_67, _YLOC("Email Display Name")).c_str(),			g_FamilyInfo,		g_ColumnSize22);
	m_ColEmailAddresses					= AddColumn(_YUID(O365_CONTACT_EMAILADDRESSES),				YtriaTranslate::Do(GridContacts_customizeGrid_68, _YLOC("Email Address")).c_str(),				g_FamilyInfo,		g_ColumnSize32);
	m_ColMobilePhone					= AddColumn(_YUID(O365_CONTACT_MOBILEPHONE),				YtriaTranslate::Do(GridContacts_customizeGrid_69, _YLOC("Mobile phone")).c_str(),					g_FamilyInfo,		g_ColumnSize12,	{ g_ColumnsPresetDefault });
	m_ColIMAddresses					= AddColumn(_YUID(O365_CONTACT_IMADDRESSES),				YtriaTranslate::Do(GridContacts_customizeGrid_70, _YLOC("IM Addresses")).c_str(),					g_FamilyInfo,		g_ColumnSize22,	{ g_ColumnsPresetDefault });
	static const wstring g_FamilyBusiness = YtriaTranslate::Do(GridContacts_customizeGrid_71, _YLOC("Business")).c_str();
	m_ColCompanyName			= AddColumn(_YUID(O365_CONTACT_COMPANYNAME),						YtriaTranslate::Do(GridContacts_customizeGrid_16, _YLOC("Company Name")).c_str(),					g_FamilyBusiness,	g_ColumnSize22,	{ g_ColumnsPresetDefault });
	m_ColOfficeLocation			= AddColumn(_YUID(O365_CONTACT_OFFICELOCATION),						YtriaTranslate::Do(GridContacts_customizeGrid_17, _YLOC("Office location")).c_str(),				g_FamilyBusiness,	g_ColumnSize12,	{ g_ColumnsPresetDefault });
	m_ColDepartment				= AddColumn(_YUID(O365_CONTACT_DEPARTMENT),							YtriaTranslate::Do(GridContacts_customizeGrid_18, _YLOC("Department")).c_str(),					g_FamilyBusiness,	g_ColumnSize12);
	m_ColJobTitle				= AddColumn(_YUID(O365_CONTACT_JOBTITLE),							YtriaTranslate::Do(GridContacts_customizeGrid_19, _YLOC("Job title")).c_str(),					g_FamilyBusiness,	g_ColumnSize12,	{ g_ColumnsPresetDefault });
	static const wstring g_FamilyPersonal = YtriaTranslate::Do(GridContacts_customizeGrid_20, _YLOC("Personal")).c_str();
 	m_ColTitle					= AddColumn(_YUID(O365_CONTACT_TITLE),								YtriaTranslate::Do(GridContacts_customizeGrid_21, _YLOC("Title")).c_str(),						g_FamilyPersonal,	g_ColumnSize12);// personal?
	m_ColProfession				= AddColumn(_YUID(O365_CONTACT_PROFESSION),							YtriaTranslate::Do(GridContacts_customizeGrid_22, _YLOC("Profession")).c_str(),					g_FamilyBusiness,	g_ColumnSize22);
	m_ColManager				= AddColumn(_YUID(O365_CONTACT_MANAGER),							YtriaTranslate::Do(GridContacts_customizeGrid_23, _YLOC("Manager")).c_str(),						g_FamilyBusiness,	g_ColumnSize12);
	m_ColAssistantName			= AddColumn(_YUID(O365_CONTACT_ASSISTANTNAME),						YtriaTranslate::Do(GridContacts_customizeGrid_24, _YLOC("Assistant name")).c_str(),				g_FamilyBusiness,	g_ColumnSize22);
	m_ColBusinessPhones			= AddColumn(_YUID(O365_CONTACT_BUSINESSPHONES),						YtriaTranslate::Do(GridContacts_customizeGrid_25, _YLOC("Phones - Business")).c_str(),			g_FamilyBusiness,	g_ColumnSize22,	{ g_ColumnsPresetDefault });
    m_ColBusinessAddressStreet  = AddColumn(_YUID("businessAddress.street"),						YtriaTranslate::Do(GridContacts_customizeGrid_26, _YLOC("Street - Business")).c_str(),			g_FamilyBusiness,	g_ColumnSize22,	{ g_ColumnsPresetDefault });
    m_ColBusinessAddressCity    = AddColumn(_YUID("businessAddress.city"),							YtriaTranslate::Do(GridContacts_customizeGrid_27, _YLOC("City - Business")).c_str(),				g_FamilyBusiness,	g_ColumnSize12,	{ g_ColumnsPresetDefault });
    m_ColBusinessAddressPCode	= AddColumn(_YUID("businessAddress.postalCode"),					YtriaTranslate::Do(GridContacts_customizeGrid_28, _YLOC("Postal Code - Business")).c_str(),		g_FamilyBusiness,	g_ColumnSize12,	{ g_ColumnsPresetDefault });
	m_ColBusinessAddressState	= AddColumn(_YUID("businessAddress.state"),							YtriaTranslate::Do(GridContacts_customizeGrid_29, _YLOC("State - Business")).c_str(),				g_FamilyBusiness,	g_ColumnSize12,	{ g_ColumnsPresetDefault });
    m_ColBusinessAddressCountry = AddColumn(_YUID("businessAddress.countryOrRegion"),				YtriaTranslate::Do(GridContacts_customizeGrid_30, _YLOC("Country - Business")).c_str(),			g_FamilyBusiness,	g_ColumnSize12,	{ g_ColumnsPresetDefault });
    m_ColBusinessHomePage		= AddColumn(_YUID(O365_CONTACT_BUSINESSHOMEPAGE),					YtriaTranslate::Do(GridContacts_customizeGrid_31, _YLOC("Home page - Business")).c_str(),			g_FamilyBusiness,	g_ColumnSize8);
	static wstring g_FamilyHome = YtriaTranslate::Do(GridContacts_customizeGrid_32, _YLOC("Home")).c_str();
	m_ColHomePhones				= AddColumn(_YUID(O365_CONTACT_HOMEPHONES),							YtriaTranslate::Do(GridContacts_customizeGrid_33, _YLOC("Phones - Home")).c_str(),				g_FamilyHome,		g_ColumnSize22);
    m_ColHomeAddressStreet      = AddColumn(_YUID("homeAddress.street"),							YtriaTranslate::Do(GridContacts_customizeGrid_34, _YLOC("Street - Home")).c_str(),				g_FamilyHome,		g_ColumnSize22);
    m_ColHomeAddressCity        = AddColumn(_YUID("homeAddress.city"),								YtriaTranslate::Do(GridContacts_customizeGrid_35, _YLOC("City - Home")).c_str(),					g_FamilyHome,		g_ColumnSize12);
    m_ColHomeAddressPostalCode  = AddColumn(_YUID("homeAddress.postalCode"),						YtriaTranslate::Do(GridContacts_customizeGrid_36, _YLOC("Postal Code - Home")).c_str(),			g_FamilyHome,		g_ColumnSize12);
    m_ColHomeAddressState       = AddColumn(_YUID("homeAddress.state"),								YtriaTranslate::Do(GridContacts_customizeGrid_37, _YLOC("State - Home")).c_str(),					g_FamilyHome,		g_ColumnSize12);
    m_ColHomeAddressCountry		= AddColumn(_YUID("homeAddress.countryOrRegion"),					YtriaTranslate::Do(GridContacts_customizeGrid_38, _YLOC("Country - Home")).c_str(),				g_FamilyHome,		g_ColumnSize12);
	static wstring g_FamilyOther = YtriaTranslate::Do(GridContacts_customizeGrid_39, _YLOC("Other")).c_str();
    m_ColOtherAddressStreet     = AddColumn(_YUID("otherAddress.street"),							YtriaTranslate::Do(GridContacts_customizeGrid_40, _YLOC("Street - Other")).c_str(),				g_FamilyOther,		g_ColumnSize22);
    m_ColOtherAddressCity		= AddColumn(_YUID("otherAddress.city"),								YtriaTranslate::Do(GridContacts_customizeGrid_41, _YLOC("City - Other")).c_str(),					g_FamilyOther,		g_ColumnSize12);
	m_ColOtherAddressPostalCode = AddColumn(_YUID("otherAddress.postalCode"),						YtriaTranslate::Do(GridContacts_customizeGrid_42, _YLOC("Postal Code - Other")).c_str(),			g_FamilyOther,		g_ColumnSize12);
    m_ColOtherAddressState      = AddColumn(_YUID("otherAddress.state"),							YtriaTranslate::Do(GridContacts_customizeGrid_43, _YLOC("State - Other")).c_str(),				g_FamilyOther,		g_ColumnSize12);
    m_ColOtherAddressCountry	= AddColumn(_YUID("otherAddress.countryOrRegion"),					YtriaTranslate::Do(GridContacts_customizeGrid_44, _YLOC("Country - Other")).c_str(),				g_FamilyOther,		g_ColumnSize12);
	//g_FamilyPersonal
	m_ColNickname			= AddColumn(_YUID(O365_CONTACT_NICKNAME),								YtriaTranslate::Do(GridContacts_customizeGrid_45, _YLOC("Nickname")).c_str(),						g_FamilyPersonal,	g_ColumnSize22);
	m_ColInitials			= AddColumn(_YUID(O365_CONTACT_INITIALS),								YtriaTranslate::Do(GridContacts_customizeGrid_46, _YLOC("Initials")).c_str(),						g_FamilyPersonal,	g_ColumnSize12);
	m_ColBirthday			= AddColumnDate(_YUID(O365_CONTACT_BIRTHDAY),							YtriaTranslate::Do(GridContacts_customizeGrid_47, _YLOC("Birthday")).c_str(),						g_FamilyPersonal,	g_ColumnSize16);
	m_ColSpouseName			= AddColumn(_YUID(O365_CONTACT_SPOUSENAME),								YtriaTranslate::Do(GridContacts_customizeGrid_48, _YLOC("Spouse")).c_str(),						g_FamilyPersonal,	g_ColumnSize22);
	m_ColChildren			= AddColumn(_YUID(O365_CONTACT_CHILDREN),								YtriaTranslate::Do(GridContacts_customizeGrid_49, _YLOC("Children ")).c_str(),					g_FamilyPersonal,	g_ColumnSize22);
	m_ColGeneration			= AddColumn(_YUID(O365_CONTACT_GENERATION),								YtriaTranslate::Do(GridContacts_customizeGrid_50, _YLOC("Generation")).c_str(),					g_FamilyPersonal,	g_ColumnSize12);
	m_ColPersonalNotes		= AddColumn(_YUID(O365_CONTACT_PERSONALNOTES),							YtriaTranslate::Do(GridContacts_customizeGrid_51, _YLOC("Personal Notes")).c_str(),				g_FamilyInfo,		g_ColumnSize12,	{ g_ColumnsPresetDefault });
	m_ColChangeKey			= AddColumn(_YUID(O365_CONTACT_CHANGEKEY),								YtriaTranslate::Do(GridContacts_customizeGrid_52, _YLOC("Change Key")).c_str(),					g_FamilyInfo,		g_ColumnSize12);
	static wstring g_FamilyYomi = YtriaTranslate::Do(GridContacts_customizeGrid_53, _YLOC("Yomi")).c_str();
	m_ColYomiGivenName		= AddColumn(_YUID(O365_CONTACT_YOMIGIVENNAME),							YtriaTranslate::Do(GridContacts_customizeGrid_54, _YLOC("Yomi Given name")).c_str(),				g_FamilyYomi,		g_ColumnSize22);
	m_ColYomiSurnameName	= AddColumn(_YUID(O365_CONTACT_YOMISURNAME),					    	YtriaTranslate::Do(GridContacts_customizeGrid_55, _YLOC("Yomi Surname")).c_str(),					g_FamilyYomi,		g_ColumnSize22);
	m_ColYomiCompanyName	= AddColumn(_YUID(O365_CONTACT_YOMICOMPANYNAME),						YtriaTranslate::Do(GridContacts_customizeGrid_56, _YLOC("Yomi Company name")).c_str(),			g_FamilyYomi,		g_ColumnSize22);
	addColumnGraphID();

	SetColumnDateFormatNoTime(m_ColBirthday);

	AddColumnForRowPK(m_Template.m_ColumnID);
	AddColumnForRowPK(GetColId());

	m_Template.CustomizeGrid(*this);

    m_Frame = dynamic_cast<FrameContacts*>(GetParentFrame());
    ASSERT(nullptr != m_Frame);
	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_Frame);
}

wstring GridContacts::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, m_ColDisplayName);
}

bool GridContacts::IsMyData() const
{
	return m_IsMyData;
}

void GridContacts::BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessContact>>& p_Contacts, bool p_FullPurge)
{
	ASSERT(!IsMyData() || p_Contacts.size() == 1);

	GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID
			| (p_FullPurge || IsMyData() ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			| GridUpdaterOptions::REFRESH_PROCESS_DATA
		)
	);

    GridIgnoreModification ignoramus(*this);

	bool oneCanceled = false;
	bool oneOK = false;
	for (const auto& keyVal : p_Contacts)
	{
		if (m_HasLoadMoreAdditionalInfo)
		{
			if (keyVal.first.IsMoreLoaded())
				m_MetaIDsMoreLoaded.insert(keyVal.first.GetID());
			else
				m_MetaIDsMoreLoaded.erase(keyVal.first.GetID());
		}

		auto parentUserRow = m_Template.AddRow(*this, keyVal.first, {}, updater, false, true);
		ASSERT(nullptr != parentUserRow);
		if (nullptr != parentUserRow)
		{
			if (!IsMyData())
				updater.GetOptions().AddRowWithRefreshedValues(parentUserRow);
			parentUserRow->SetHierarchyParentToHide(true);

			if (keyVal.first.HasFlag(BusinessObject::CANCELED))
			{
				oneCanceled = true;

				// Could be uncommented if we notice missing explosion refresh on canceled rows
				//auto children = GetChildRows(parentUserRow);
				//for (auto child : children)
				//	updater.GetOptions().AddRowWithRefreshedValues(child);
			}
			else
			{
				oneOK = true;

				if (!IsMyData()) // Row will be deleted below
					updater.GetOptions().AddPartialPurgeParentRow(parentUserRow);

				// Remove assert and uncomment the line below if enabling GridModifications
				ASSERT(!IsGridModificationsEnabled());
				//updater.AddUpdatedRowPk(userRowPk);

				if (keyVal.second.size() == 1 && keyVal.second[0].GetError())
				{
					updater.OnLoadingError(parentUserRow, *keyVal.second[0].GetError());
				}
				else
				{
					// Clear previous error
					if (parentUserRow->IsError())
						parentUserRow->ClearStatus();

					for (const auto& contact : keyVal.second)
					{
						auto contactRow = FillRow(contact, parentUserRow);
						ASSERT(nullptr != contactRow);
						if (nullptr != contactRow)
							updater.GetOptions().AddRowWithRefreshedValues(contactRow);
					}
				}
			}

			AddRoleDelegationFlag(parentUserRow, keyVal.first);

			if (IsMyData())
			{
				ASSERT(m_MyDataMeHandler);
				ASSERT(parentUserRow->HasChildrenRows() || keyVal.second.empty()); 
				m_MyDataMeHandler->GetRidOfMeRow(*this, parentUserRow);
			}
		}
	}

	if (oneCanceled)
	{
		if (oneOK)
			updater.GetOptions().SetPurgeFlag(GridUpdaterOptions::PARTIALPURGE); // Only purge not-canceled top rows
		else
			updater.GetOptions().SetPurgeFlag(0); // Everything canceled, don't purge anything.
	}
}

ModuleCriteria GridContacts::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria;

    auto frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
    ASSERT(nullptr != frame);
    if (nullptr != frame)
    {
        criteria = frame->GetModuleCriteria();
        criteria.m_IDs.clear();
        for (auto row : p_Rows)
            criteria.m_IDs.insert(row->GetField(GetColId()).GetValueStr());
    }

    return criteria;
}

BusinessContact GridContacts::getBusinessObject(GridBackendRow* row) const
{
	BusinessContact businessContact;

	{
		const auto& field = row->GetField(GetColId());
		if (field.HasValue())
			businessContact.SetID(field.GetValueStr());
	}

	{
		const auto& field = row->GetField(m_ColFirstName);
		if (field.HasValue())
			businessContact.m_GivenName = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColDisplayName);
		if (field.HasValue())
			businessContact.m_DisplayName = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColLastName);
		if (field.HasValue())
			businessContact.m_Surname = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColMiddleName);
		if (field.HasValue())
			businessContact.m_MiddleName = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColManager);
		if (field.HasValue())
			businessContact.m_Manager = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColJobTitle);
		if (field.HasValue())
			businessContact.m_JobTitle = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColMobilePhone);
		if (field.HasValue())
			businessContact.m_MobilePhone = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColAssistantName);
		if (field.HasValue())
			businessContact.m_AssistantName = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColBirthday);
		if (field.HasValue())
			businessContact.m_Birthday = field.GetValueTimeDate();
	}

	{
		const auto& field = row->GetField(m_ColBusinessHomePage);
		if (field.HasValue())
			businessContact.m_BusinessHomePage = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColChangeKey);
		if (field.HasValue())
			businessContact.m_ChangeKey = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColCompanyName);
		if (field.HasValue())
			businessContact.m_CompanyName = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColCreatedDateTime);
		if (field.HasValue())
			businessContact.m_CreatedDateTime = field.GetValueTimeDate();
	}

	{
		const auto& field = row->GetField(m_ColDepartment);
		if (field.HasValue())
			businessContact.m_Department = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColFileAs);
		if (field.HasValue())
			businessContact.m_FileAs = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColGeneration);
		if (field.HasValue())
			businessContact.m_Generation = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColInitials);
		if (field.HasValue())
			businessContact.m_Initials = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColLastModifiedDateTime);
		if (field.HasValue())
			businessContact.m_LastModifiedDateTime = field.GetValueTimeDate();
	}

	{
		const auto& field = row->GetField(m_ColNickname);
		if (field.HasValue())
			businessContact.m_Nickname = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColOfficeLocation);
		if (field.HasValue())
			businessContact.m_OfficeLocation = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColParentFolderId);
		if (field.HasValue())
			businessContact.m_ParentFolderId = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColPersonalNotes);
		if (field.HasValue())
			businessContact.m_PersonalNotes = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColProfession);
		if (field.HasValue())
			businessContact.m_Profession = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColSpouseName);
		if (field.HasValue())
			businessContact.m_SpouseName = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColTitle);
		if (field.HasValue())
			businessContact.m_Title = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColYomiCompanyName);
		if (field.HasValue())
			businessContact.m_YomiCompanyName = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColYomiGivenName);
		if (field.HasValue())
			businessContact.m_YomiGivenName = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColYomiSurnameName);
		if (field.HasValue())
			businessContact.m_YomiSurnameName = field.GetValueStr();
	}

	{
		const auto& field = row->GetField(m_ColHomePhones);
		if (field.HasValue())
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				businessContact.m_HomePhones = *field.GetValuesMulti<PooledString>();
			else
				businessContact.m_HomePhones.emplace_back(field.GetValueStr());
		}
	}

	{
		const auto& field = row->GetField(m_ColIMAddresses);
		if (field.HasValue())
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				businessContact.m_ImAddresses = *field.GetValuesMulti<PooledString>();
			else
				businessContact.m_ImAddresses.emplace_back(field.GetValueStr());
		}
	}

	{
		const auto& field = row->GetField(m_ColChildren);
		if (field.HasValue())
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				businessContact.m_Children = *field.GetValuesMulti<PooledString>();
			else
				businessContact.m_Children.emplace_back(field.GetValueStr());
		}
	}

	{
		const auto& field = row->GetField(m_ColBusinessPhones);
		if (field.HasValue())
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				businessContact.m_BusinessPhones = *field.GetValuesMulti<PooledString>();
			else
				businessContact.m_BusinessPhones.emplace_back(field.GetValueStr());
		}
	}

	{
		const auto& field = row->GetField(m_ColCategories);
		if (field.HasValue())
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				businessContact.m_Categories = *field.GetValuesMulti<PooledString>();
			else
				businessContact.m_Categories.emplace_back(field.GetValueStr());
		}
	}

	return businessContact;
}

GridBackendRow* GridContacts::FillRow(const BusinessContact& p_BusinessContact, GridBackendRow* p_ParentUserRow)
{
	GridBackendField fID(p_BusinessContact.GetID(), GetColId());
	GridBackendRow* row = m_RowIndex.GetRow({ fID, p_ParentUserRow->GetField(m_Template.m_ColumnID) }, true, true);

    ASSERT(nullptr != row);
    if (nullptr != row)
    {
		for (auto c : m_MetaColumns)
		{
			if (p_ParentUserRow->HasField(c))
				row->AddField(p_ParentUserRow->GetField(c));
			else
				row->RemoveField(c);
		}

        row->AddField(p_BusinessContact.m_ContactFolderName, m_ColMetaContactFolderDisplayName);
		SetRowObjectType(row, p_BusinessContact, p_BusinessContact.HasFlag(BusinessObject::CANCELED));
		UpdateRow(p_BusinessContact, row);

		row->SetParentRow(p_ParentUserRow);

		if (p_ParentUserRow->HasField(m_ColumnLastRequestDateTime))
			row->AddField(p_ParentUserRow->GetField(m_ColumnLastRequestDateTime));
		else
			row->RemoveField(m_ColumnLastRequestDateTime);
    }

    return row;
}

void GridContacts::UpdateRow(const BusinessContact& p_BusinessContact, GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		p_Row->AddField(p_BusinessContact.GetID(), GetColId());
		p_Row->AddField(p_BusinessContact.m_GivenName, m_ColFirstName);
		p_Row->AddField(p_BusinessContact.m_DisplayName, m_ColDisplayName);
		p_Row->AddField(p_BusinessContact.m_Surname, m_ColLastName);
		p_Row->AddField(p_BusinessContact.m_MiddleName, m_ColMiddleName);
		p_Row->AddField(p_BusinessContact.m_EmailAddressesName, m_ColEmailDisplayName);
		p_Row->AddField(p_BusinessContact.m_EmailAddressesAddress, m_ColEmailAddresses);
		p_Row->AddField(p_BusinessContact.m_Manager, m_ColManager);
		p_Row->AddField(p_BusinessContact.m_JobTitle, m_ColJobTitle);
		p_Row->AddField(p_BusinessContact.m_MobilePhone, m_ColMobilePhone);
		p_Row->AddField(p_BusinessContact.m_AssistantName, m_ColAssistantName);
		p_Row->AddField(p_BusinessContact.m_Birthday, m_ColBirthday);
		p_Row->AddField(p_BusinessContact.m_BusinessHomePage, m_ColBusinessHomePage);
		p_Row->AddField(p_BusinessContact.m_ChangeKey, m_ColChangeKey);
		p_Row->AddField(p_BusinessContact.m_CompanyName, m_ColCompanyName);
		p_Row->AddField(p_BusinessContact.m_CreatedDateTime, m_ColCreatedDateTime);
		p_Row->AddField(p_BusinessContact.m_Department, m_ColDepartment);
		p_Row->AddField(p_BusinessContact.m_FileAs, m_ColFileAs);
		p_Row->AddField(p_BusinessContact.m_Generation, m_ColGeneration);
		p_Row->AddField(p_BusinessContact.m_Initials, m_ColInitials);
		p_Row->AddField(p_BusinessContact.m_LastModifiedDateTime, m_ColLastModifiedDateTime);
		p_Row->AddField(p_BusinessContact.m_Nickname, m_ColNickname);
		p_Row->AddField(p_BusinessContact.m_OfficeLocation, m_ColOfficeLocation);
		p_Row->AddField(p_BusinessContact.m_ParentFolderId, m_ColParentFolderId);
		p_Row->AddField(p_BusinessContact.m_PersonalNotes, m_ColPersonalNotes);
		p_Row->AddField(p_BusinessContact.m_Profession, m_ColProfession);
		p_Row->AddField(p_BusinessContact.m_SpouseName, m_ColSpouseName);
		p_Row->AddField(p_BusinessContact.m_Title, m_ColTitle);
		p_Row->AddField(p_BusinessContact.m_YomiCompanyName, m_ColYomiCompanyName);
		p_Row->AddField(p_BusinessContact.m_YomiGivenName, m_ColYomiGivenName);
		p_Row->AddField(p_BusinessContact.m_YomiSurnameName, m_ColYomiSurnameName);
		p_Row->AddField(p_BusinessContact.m_HomePhones, m_ColHomePhones);
		p_Row->AddField(p_BusinessContact.m_ImAddresses, m_ColIMAddresses);
		p_Row->AddField(p_BusinessContact.m_Children, m_ColChildren);
		p_Row->AddField(p_BusinessContact.m_BusinessPhones, m_ColBusinessPhones);
		p_Row->AddField(p_BusinessContact.m_Categories, m_ColCategories);

        p_Row->AddField(p_BusinessContact.m_BusinessAddressCity, m_ColBusinessAddressCity);
        p_Row->AddField(p_BusinessContact.m_BusinessAddressCountryOrRegion, m_ColBusinessAddressCountry);
        p_Row->AddField(p_BusinessContact.m_BusinessAddressPostalCode, m_ColBusinessAddressPCode);
        p_Row->AddField(p_BusinessContact.m_BusinessAddressState, m_ColBusinessAddressState);
        p_Row->AddField(p_BusinessContact.m_BusinessAddressStreet, m_ColBusinessAddressStreet);

        p_Row->AddField(p_BusinessContact.m_HomeAddressCity, m_ColHomeAddressCity);
        p_Row->AddField(p_BusinessContact.m_HomeAddressCountryOrRegion, m_ColHomeAddressCountry);
        p_Row->AddField(p_BusinessContact.m_HomeAddressPostalCode, m_ColHomeAddressPostalCode);
        p_Row->AddField(p_BusinessContact.m_HomeAddressState, m_ColHomeAddressState);
        p_Row->AddField(p_BusinessContact.m_HomeAddressStreet, m_ColHomeAddressStreet);
        
        p_Row->AddField(p_BusinessContact.m_OtherAddressCity, m_ColOtherAddressCity);
        p_Row->AddField(p_BusinessContact.m_OtherAddressCountryOrRegion, m_ColOtherAddressCountry);
        p_Row->AddField(p_BusinessContact.m_OtherAddressPostalCode, m_ColOtherAddressPostalCode);
        p_Row->AddField(p_BusinessContact.m_OtherAddressState, m_ColOtherAddressState);
        p_Row->AddField(p_BusinessContact.m_OtherAddressStreet, m_ColOtherAddressStreet);
	}
}

void GridContacts::UpdateBusinessObjects(const vector<BusinessContact>& p_Contacts, bool p_SetModifiedStatus)
{
	bool modified = false;
	for (const auto& contact : p_Contacts)
		modified = UpdateRowForBusinessContact(contact, p_SetModifiedStatus) || modified;

	if (modified)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

bool GridContacts::UpdateRowForBusinessContact(const BusinessContact& p_Contact, bool p_SetModifiedStatus)
{
	bool modified = false;
	GridBackendRow* rowToUpdate = nullptr;

	vector< GridBackendRow* > ListRows;
	GetAllNonGroupRows(ListRows);
	for (const auto& Row : ListRows)
	{
		const GridBackendField& IDField = Row->GetField(GetColId());
		if (p_Contact.GetID() == IDField.GetValueStr())
		{
			rowToUpdate = Row;
			break;
		}
	}

	if (nullptr != rowToUpdate)
	{
		UpdateRow(p_Contact, rowToUpdate);
		if (rowToUpdate->HasModifiedField() && p_SetModifiedStatus)
			OnRowModified(rowToUpdate);
		else
			rowToUpdate->SetEditModified(false);

		modified = rowToUpdate->HasModifiedField();
	}

	return modified;
}

void GridContacts::RemoveBusinessObjects(const vector<BusinessContact>& p_Contacts)
{
	bool removed = false;
	for (const auto& contact : p_Contacts)
		removed = RemoveRowForBusinessContact(contact) || removed;

	if (removed)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
}

bool GridContacts::RemoveRowForBusinessContact(const BusinessContact& p_Contact)
{
	bool removed = false;

	vector< GridBackendRow* > ListRows;
	GetAllNonGroupRows(ListRows);
	for (const auto& Row : ListRows)
	{
		const GridBackendField& IDField = Row->GetField(GetColId());
		if (p_Contact.GetID() == IDField.GetValueStr())
		{
			removed = RemoveRow(Row);
			break;
		}
	}

	return removed;
}

bool GridContacts::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return ModuleO365Grid<BusinessContact>::IsRowValidForNoFieldText(p_Row, p_Col)
		&& (!p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore)
			|| m_MetaIDsMoreLoaded.end() == m_MetaIDsMoreLoaded.find(p_Row->GetField(m_Template.m_ColumnID).GetValueStr()));
}
