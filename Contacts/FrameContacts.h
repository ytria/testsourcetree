#pragma once

#include "GridFrameBase.h"
#include "GridContacts.h"

#include "BusinessContact.h"

class FrameContacts : public GridFrameBase
{
public:
	FrameContacts(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

	DECLARE_DYNAMIC(FrameContacts)
	virtual ~FrameContacts() = default;

	void ShowContacts(const O365DataMap<BusinessUser, vector<BusinessContact>>& p_Contacts, bool p_FullPurge);

	virtual void ApplyAllSpecific();
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

	
	virtual O365Grid& GetGrid() override;

protected:
	virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;
	
	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	GridContacts m_contactsGrid;
};
