#include "UserListLicensePropertySet.h"

#include "MsGraphFieldNames.h"
#include "UserListFullPropertySet.h"

vector<rttr::property> UserListLicensePropertySet::GetPropertySet() const
{
    // FIXME: Don't use "rttr::property
	UserListFullPropertySet fullPropSet;

	auto propSet = std::move(fullPropSet.GetPropertySet());

	std::set<rttr::string_view> propertiesToKeep
	{
		O365_ID,
		O365_USER_USERPRINCIPALNAME,
		O365_USER_DISPLAYNAME,
		O365_USER_USAGELOCATION,
		O365_USER_USERTYPE,
		O365_USER_ASSIGNEDLICENSES,
		O365_USER_ASSIGNEDPLANS,
		O365_USER_PROVISIONEDPLANS,
	};

	propSet.erase(std::remove_if(propSet.begin(), propSet.end(), [&propertiesToKeep](const rttr::property& p_Property) {
		return propertiesToKeep.end() == propertiesToKeep.find(p_Property.get_name());
	}), propSet.end());

	return propSet;
}

vector<rttr::property> DeletedUserListLicensePropertySet::GetPropertySet() const
{
	auto propSet = UserListLicensePropertySet::GetPropertySet();

	// Same as UserListLicensePropertySet::GetPropertySet(), just add O365_USER_DELETEDDATETIME
	propSet.push_back(rttr::type::get<User>().get_property(O365_USER_DELETEDDATETIME));

	return propSet;
}

vector<rttr::property> UserListLicenseOnlyPropertySet::GetPropertySet() const
{
	// FIXME: Don't use "rttr::property
	UserListFullPropertySet fullPropSet;

	auto propSet = std::move(fullPropSet.GetPropertySet());

	std::set<rttr::string_view> propertiesToKeep
	{
		O365_ID,
		O365_USER_ASSIGNEDLICENSES,
		O365_USER_ASSIGNEDPLANS,
		O365_USER_PROVISIONEDPLANS,
	};

	propSet.erase(std::remove_if(propSet.begin(), propSet.end(), [&propertiesToKeep](const rttr::property& p_Property) {
		return propertiesToKeep.end() == propertiesToKeep.find(p_Property.get_name());
		}), propSet.end());

	return propSet;
}
