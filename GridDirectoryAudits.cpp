#include "GridDirectoryAudits.h"
#include "FrameDirectoryAudits.h"
#include "AutomationWizardDirectoryAudits.h"
#include "BasicGridSetup.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"
#include "DlgAuditLogsModuleOptions.h"

BEGIN_MESSAGE_MAP(GridDirectoryAudits, ModuleO365Grid<DirectoryAudit>)
	ON_COMMAND(ID_DIRECTORYAUDITGRID_CHANGEOPTIONS, OnChangeOptions)
	ON_UPDATE_COMMAND_UI(ID_DIRECTORYAUDITGRID_CHANGEOPTIONS, OnUpdateChangeOptions)
END_MESSAGE_MAP()

GridDirectoryAudits::GridDirectoryAudits()
	: m_ColActivityDateTime(nullptr),
	m_ColLoggedByService(nullptr),
	m_ColOperationType(nullptr),
	m_ColCategory(nullptr),
	m_ColCorrelationId(nullptr),
	m_ColActivityDisplayName(nullptr),
	m_ColResult(nullptr),
	m_ColResultReason(nullptr),
	m_ColAdditionalDetailsKey(nullptr),
	m_ColAdditionalDetailsValue(nullptr),
	m_ColTargetResourcesDisplayName(nullptr),
	m_ColTargetResourcesType(nullptr),
	m_ColTargetResourcesUserPrincipalName(nullptr),
	m_ColTargetResourcesGroupType(nullptr),
	m_ColTargetResourcesModifiedPropertiesDisplayName(nullptr),
	m_ColTargetResourcesModifiedPropertiesOldValue(nullptr),
	m_ColTargetResourcesModifiedPropertiesNewValue(nullptr),
	m_ColTargetResourcesId(nullptr),
	m_ColInitiatedBy(nullptr),
	m_ColInitiatedByAppAppId(nullptr),
	m_ColInitiatedByAppDisplayName(nullptr),
	m_ColInitiatedByAppServicePrincipalId(nullptr),
	m_ColInitiatedByAppServicePrincipalName(nullptr),
	m_ColInitiatedByUserId(nullptr),
	m_ColInitiatedByUserDisplayName(nullptr),
	m_ColInitiatedByUserUserPrincipalName(nullptr),
	m_ColInitiatedByUserIpAddress(nullptr),
	m_Frame(nullptr)
{
	UseDefaultActions(0);
	initWizard<AutomationWizardDirectoryAudits>(_YTEXT("Automation\\DirectoryAudits"));
}

GridDirectoryAudits::~GridDirectoryAudits()
{
	GetBackend().Clear();
}

void GridDirectoryAudits::BuildView(const vector<DirectoryAudit>& p_DirAudits, bool p_FullPurge, bool p_NoPurge)
{
	if (p_DirAudits.size() == 1 && p_DirAudits[0].GetError() != boost::none && p_DirAudits[0].GetID().IsEmpty())
	{
		HandlePostUpdateError(p_DirAudits[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_DirAudits[0].GetError()->GetFullErrorMessage());
	}
	else
	{
		GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
			| (p_NoPurge ? 0 : (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE))
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			| GridUpdaterOptions::REFRESH_PROCESS_DATA
		);
		GridUpdater updater(*this, options);

		for (const auto& dirAudit : p_DirAudits)
		{
			GridBackendField fID(dirAudit.GetID(), GetColId());
			GridBackendRow* row = m_RowIndex.GetRow({ fID }, true, true);

			SetRowObjectType(row, dirAudit);
			FillDirAuditFields(row, dirAudit);
		}
	}
}

wstring GridDirectoryAudits::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { { _T("User"), m_ColInitiatedBy } }, _YFORMAT(L"%s on %s", p_Row->GetField(m_ColActivityDisplayName).ToString().c_str(), p_Row->GetField(m_ColActivityDateTime).ToString().c_str()));
}

O365Grid::SnapshotCaps GridDirectoryAudits::GetSnapshotCapabilities() const
{
	// This module has no refresh/edit capabilities, Restore Point is useless.
	return { true, false };
}

void GridDirectoryAudits::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameDirectoryAudits, LicenseUtil::g_codeSapio365directoryAudits/*,
			{
				{ &m_ColMetaDisplayName, g_TitleOwnerDisplayName, g_FamilyOwner },
				{ &m_ColMetaUserPrincipalName, g_TitleOwnerUserName, g_FamilyOwner },
				{ &m_ColMetaId, g_TitleOwnerID, g_FamilyOwner }
			}*/);
		setup.Setup(*this, false);
	}

	static const wstring g_FamilyTarget			= _T("Target");
	static const wstring g_FamilyInitiatedBy	= _T("Initiated by");

	m_ColActivityDateTime								= AddColumnDate(_YUID("activityDateTime"), _T("Date"), g_FamilyInfo, g_ColumnSize16,																	{ g_ColumnsPresetDefault });
	m_ColLoggedByService								= AddColumn(_YUID("loggedByService"), _T("Service"), g_FamilyInfo, g_ColumnSize22,																		{ g_ColumnsPresetDefault });
	m_ColOperationType									= AddColumn(_YUID("operationType"), _T("Operation type"), g_FamilyInfo, g_ColumnSize12);
	m_ColCategory										= AddColumn(_YUID("category"), _T("Category"), g_FamilyInfo, g_ColumnSize22,																			{ g_ColumnsPresetDefault });
	m_ColCorrelationId									= AddColumn(_YUID("correlationId"), _T("Correlation ID"), g_FamilyInfo, g_ColumnSize12,																	{ g_ColumnsPresetTech });
	m_ColActivityDisplayName							= AddColumn(_YUID("activityDisplayName"), _T("Activity"), g_FamilyInfo, g_ColumnSize32,																	{ g_ColumnsPresetDefault });
	m_ColResult											= AddColumn(_YUID("result"), _T("Result status"), g_FamilyInfo, g_ColumnSize8,																			{ g_ColumnsPresetDefault });
	m_ColResultReason									= AddColumn(_YUID("resultReason"), _T("Result status reason"), g_FamilyInfo, g_ColumnSize12);
	addColumnGraphID();
	m_ColAdditionalDetailsKey							= AddColumn(_YUID("additionalDetails.key"), _T("Key - Additional details"), g_FamilyInfo, g_ColumnSize12);
	m_ColAdditionalDetailsValue							= AddColumn(_YUID("additionalDetails.value"), _T("Value - Additional details"), g_FamilyInfo, g_ColumnSize12);
	m_ColTargetResourcesDisplayName						= AddColumn(_YUID("targetResources.displayName"), _T("Target"), g_FamilyTarget, g_ColumnSize32,															{ g_ColumnsPresetDefault });
	m_ColTargetResourcesType							= AddColumn(_YUID("targetResources.type"), _T("Resource type - Target"), g_FamilyTarget, g_ColumnSize12);
	m_ColTargetResourcesUserPrincipalName				= AddColumn(_YUID("targetResources.userPrincipalName"), _T("Username - Target"), g_FamilyTarget, g_ColumnSize32);
	m_ColTargetResourcesGroupType						= AddColumn(_YUID("targetResources.groupType"), _T("Group type - Target"), g_FamilyTarget, g_ColumnSize12);
	m_ColTargetResourcesModifiedPropertiesDisplayName	= AddColumn(_YUID("targetResources.modifiedProperties.displayName"), _T("Modified properties - Target"), g_FamilyTarget, g_ColumnSize22,				{ g_ColumnsPresetDefault });
	m_ColTargetResourcesModifiedPropertiesOldValue		= AddColumn(_YUID("targetResources.modifiedProperties.oldValue"), _T("Old value of modified properties - Target"), g_FamilyTarget, g_ColumnSize12);
	m_ColTargetResourcesModifiedPropertiesNewValue		= AddColumn(_YUID("targetResources.modifiedProperties.newValue"), _T("New value of modified properties - Target"), g_FamilyTarget, g_ColumnSize12);
	m_ColTargetResourcesId								= AddColumn(_YUID("targetResources.id"), _T("Target ID"), g_FamilyTarget, g_ColumnSize12,																{ g_ColumnsPresetTech });
	m_ColInitiatedBy									= AddColumn(_YUID("initiatedBy"), _T("Initiated By"), g_FamilyInitiatedBy, g_ColumnSize32,																{ g_ColumnsPresetDefault });
	m_ColInitiatedByAppAppId							= AddColumn(_YUID("initiatedBy.app.appId"), _T("Application ID - Initiated by"), g_FamilyInitiatedBy, g_ColumnSize12,									{ g_ColumnsPresetTech });
	m_ColInitiatedByAppDisplayName						= AddColumn(_YUID("initiatedBy.app.displayName"), _T("Application name displayed in Azure Portal - Initiated by"), g_FamilyInitiatedBy, g_ColumnSize22);
	m_ColInitiatedByAppServicePrincipalId				= AddColumn(_YUID("initiatedBy.app.servicePrincipalId"), _T("Service Principal ID in AAD - Initiated By"), g_FamilyInitiatedBy, g_ColumnSize12,			{ g_ColumnsPresetTech });
	m_ColInitiatedByAppServicePrincipalName				= AddColumn(_YUID("initiatedBy.app.servicePrincipalName"), _T("Application name in tenant - Initiated by"), g_FamilyInitiatedBy, g_ColumnSize22);
	m_ColInitiatedByUserId								= AddColumn(_YUID("initiatedBy.user.id"), _T("User ID - Initiated by"), g_FamilyInitiatedBy, g_ColumnSize12,											{ g_ColumnsPresetTech });
	m_ColInitiatedByUserDisplayName						= AddColumn(_YUID("initiatedBy.user.displayName"), _T("User Display Name - Initiated by"), g_FamilyInitiatedBy, g_ColumnSize22);
	m_ColInitiatedByUserUserPrincipalName				= AddColumn(_YUID("initiatedBy.user.userPrincipalName"), _T("Username - Initiated by"), g_FamilyInitiatedBy, g_ColumnSize32);
	m_ColInitiatedByUserIpAddress						= AddColumn(_YUID("initiatedBy.user.ipAddress"), _T("User IP address - Initiated by"), g_FamilyInitiatedBy, g_ColumnSize12);

	AddColumnForRowPK(GetColId());

	m_ColAdditionalDetailsKey->AddMultiValueExplosionSister(m_ColAdditionalDetailsValue);

	m_ColTargetResourcesType->AddMultiValueExplosionSister(m_ColTargetResourcesDisplayName);
	m_ColTargetResourcesType->AddMultiValueExplosionSister(m_ColTargetResourcesUserPrincipalName);
	m_ColTargetResourcesType->AddMultiValueExplosionSister(m_ColTargetResourcesGroupType);
	m_ColTargetResourcesType->AddMultiValueExplosionSister(m_ColTargetResourcesModifiedPropertiesDisplayName);
	m_ColTargetResourcesType->AddMultiValueExplosionSister(m_ColTargetResourcesModifiedPropertiesOldValue);
	m_ColTargetResourcesType->AddMultiValueExplosionSister(m_ColTargetResourcesModifiedPropertiesNewValue);
	m_ColTargetResourcesType->AddMultiValueExplosionSister(m_ColTargetResourcesId);

	m_Frame = dynamic_cast<FrameDirectoryAudits*>(GetParentFrame());

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameDirectoryAudits*>(GetParentFrame()));
}

DirectoryAudit GridDirectoryAudits::getBusinessObject(GridBackendRow*) const
{
	return DirectoryAudit();
}

void GridDirectoryAudits::UpdateBusinessObjects(const vector<DirectoryAudit>& p_DirAudits, bool p_SetModifiedStatus)
{

}

void GridDirectoryAudits::RemoveBusinessObjects(const vector<DirectoryAudit>& p_DirAudits)
{

}

bool GridDirectoryAudits::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	return ModuleO365Grid<DirectoryAudit>::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridDirectoryAudits::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	return ModuleO365Grid<DirectoryAudit>::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

void GridDirectoryAudits::OnChangeOptions()
{
	if (hasNoTaskRunning() && IsFrameConnected())
	{
		ASSERT(m_Frame->GetOptions());
		DlgAuditLogsModuleOptions dlgOptions(m_Frame, GetSession(), m_Frame->GetOptions() ? *m_Frame->GetOptions() : ModuleOptions());
		if (dlgOptions.DoModal() == IDOK)
		{
			CommandInfo info;
			info.SetFrame(m_Frame);
			info.SetOrigin(GetOrigin());
			info.SetRBACPrivilege(m_Frame->GetModuleCriteria().m_Privilege);
			info.Data2() = dlgOptions.GetOptions();

			Command command(m_Frame->GetLicenseContext(), m_Frame->GetSafeHwnd(), Command::ModuleTarget::DirectoryAudit, Command::ModuleTask::List, info, { this, g_ActionNameRefreshAll, GetAutomationActionRecording(), nullptr });
			CommandDispatcher::GetInstance().Execute(command);
		}
	}
}

void GridDirectoryAudits::OnUpdateChangeOptions(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridDirectoryAudits::InitializeCommands()
{
	ModuleO365Grid<DirectoryAudit>::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_DIRECTORYAUDITGRID_CHANGEOPTIONS;
		_cmd.m_sMenuText = _T("Change Options");
		_cmd.m_sToolbarText = _T("Change Options");
		_cmd.m_sTipTool = _T("Adjust cut-off date and filter.");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANGEMODULEOPTIONS_16x16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DIRECTORYAUDITGRID_CHANGEOPTIONS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Change Options"),
			_T("Adjust cut-off date and filter."),
			_cmd.m_sAccelText);
	}
}

void GridDirectoryAudits::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ModuleO365Grid<DirectoryAudit>::OnCustomPopupMenu(pPopup, nStart);

	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_DIRECTORYAUDITGRID_CHANGEOPTIONS);
	}
}

void GridDirectoryAudits::FillDirAuditFields(GridBackendRow* p_Row, const DirectoryAudit& p_DirAudit)
{
	p_Row->AddField(p_DirAudit.m_ActivityDateTime, m_ColActivityDateTime);
	p_Row->AddField(p_DirAudit.m_LoggedByService, m_ColLoggedByService);
	p_Row->AddField(p_DirAudit.m_OperationType, m_ColOperationType);
	p_Row->AddField(p_DirAudit.m_Category, m_ColCategory);
	p_Row->AddField(p_DirAudit.m_CorrelationId, m_ColCorrelationId);
	p_Row->AddField(p_DirAudit.m_ActivityDisplayName, m_ColActivityDisplayName);
	p_Row->AddField(p_DirAudit.m_Result, m_ColResult);
	p_Row->AddField(p_DirAudit.m_ResultReason, m_ColResultReason);

	// Additional details
	vector<PooledString> addDetailsKey;
	vector<PooledString> addDetailsValue;
	if (p_DirAudit.m_AdditionalDetails != boost::none)
	{
		for (const auto& detail : *p_DirAudit.m_AdditionalDetails)
		{
			addDetailsKey.push_back(detail.m_Key != boost::none ? *detail.m_Key : PooledString(GridBackendUtil::g_NoValueString));
			addDetailsValue.push_back(detail.m_Value != boost::none ? *detail.m_Value : PooledString(GridBackendUtil::g_NoValueString));
		}
		ASSERT(addDetailsKey.size() == addDetailsValue.size());
		p_Row->AddField(addDetailsKey, m_ColAdditionalDetailsKey);
		p_Row->AddField(addDetailsValue, m_ColAdditionalDetailsValue);
	}
	
	if (p_DirAudit.m_InitiatedBy != boost::none)
	{
		if (p_DirAudit.m_InitiatedBy->m_App != boost::none)
		{
			p_Row->AddField(p_DirAudit.m_InitiatedBy->m_App->m_AppId, m_ColInitiatedByAppAppId);
			p_Row->AddField(p_DirAudit.m_InitiatedBy->m_App->m_DisplayName, m_ColInitiatedByAppDisplayName);
			p_Row->AddField(p_DirAudit.m_InitiatedBy->m_App->m_ServicePrincipalId, m_ColInitiatedByAppServicePrincipalId);
			p_Row->AddField(p_DirAudit.m_InitiatedBy->m_App->m_ServicePrincipalName, m_ColInitiatedByAppServicePrincipalName);

			p_Row->AddField(p_DirAudit.m_InitiatedBy->m_App->m_DisplayName, m_ColInitiatedBy);
		}
		// else ?
		if (p_DirAudit.m_InitiatedBy->m_User != boost::none)
		{
			p_Row->AddField(p_DirAudit.m_InitiatedBy->m_User->m_Id, m_ColInitiatedByUserId);
			p_Row->AddField(p_DirAudit.m_InitiatedBy->m_User->m_DisplayName, m_ColInitiatedByUserDisplayName);
			p_Row->AddField(p_DirAudit.m_InitiatedBy->m_User->m_UserPrincipalName, m_ColInitiatedByUserUserPrincipalName);
			p_Row->AddField(p_DirAudit.m_InitiatedBy->m_User->m_IpAddress, m_ColInitiatedByUserIpAddress);

			p_Row->AddField(p_DirAudit.m_InitiatedBy->m_User->m_UserPrincipalName, m_ColInitiatedBy);
		}
	}

	if (p_DirAudit.m_TargetResources != boost::none)
	{
		vector<PooledString> vID;
		vector<PooledString> vDisplayName;
		vector<PooledString> vType;
		vector<PooledString> vUserPrincipalName;
		vector<PooledString> vGroupType;
		vector<PooledString> vModifiedPropertiesName;
		vector<PooledString> vModifiedPropertiesOld;
		vector<PooledString> vModifiedPropertiesNew;

		const bool singleResource = p_DirAudit.m_TargetResources.get().size() == 1;
		for (const auto& tr : p_DirAudit.m_TargetResources.get())
		{
			vID.push_back				(tr.m_Id				? tr.m_Id.get()					: GridBackendUtil::g_NoValueString);
			vDisplayName.push_back		(tr.m_DisplayName		? tr.m_DisplayName.get()		: GridBackendUtil::g_NoValueString);
			vType.push_back				(tr.m_Type				? tr.m_Type.get()				: GridBackendUtil::g_NoValueString);
			vUserPrincipalName.push_back(tr.m_UserPrincipalName ? tr.m_UserPrincipalName.get()	: GridBackendUtil::g_NoValueString);
			vGroupType.push_back		(tr.m_GroupType			? tr.m_GroupType.get()			: GridBackendUtil::g_NoValueString);

			if (tr.m_ModifiedProperties)
			{
				vector<PooledString> vSubModifiedPropertiesName;
				vector<PooledString> vSubModifiedPropertiesOld;
				vector<PooledString> vSubModifiedPropertiesNew;
				for (const auto& mp : tr.m_ModifiedProperties.get())
				{
					vSubModifiedPropertiesName.	push_back(mp.m_DisplayName	? mp.m_DisplayName.get()	: GridBackendUtil::g_NoValueString);
					vSubModifiedPropertiesOld.	push_back(mp.m_OldValue		? mp.m_OldValue.get()		: GridBackendUtil::g_NoValueString);
					vSubModifiedPropertiesNew.	push_back(mp.m_NewValue		? mp.m_NewValue.get()		: GridBackendUtil::g_NoValueString);
				}

				if (singleResource)
				{
					vModifiedPropertiesName = vSubModifiedPropertiesName;
					vModifiedPropertiesOld	= vSubModifiedPropertiesOld;
					vModifiedPropertiesNew	= vSubModifiedPropertiesNew;
				}
				else
				{
					vModifiedPropertiesName.push_back(GridBackendUtil::MakeMultivalueString(vSubModifiedPropertiesName));
					vModifiedPropertiesOld.	push_back(GridBackendUtil::MakeMultivalueString(vSubModifiedPropertiesOld));
					vModifiedPropertiesNew.	push_back(GridBackendUtil::MakeMultivalueString(vSubModifiedPropertiesNew));
				}
			}
		}

		ASSERT(vID.size() == vDisplayName.size());
		ASSERT(vID.size() == vType.size());
		ASSERT(vID.size() == vUserPrincipalName.size());
		ASSERT(vID.size() == vGroupType.size());
		ASSERT(singleResource || vID.size() == vModifiedPropertiesName.size());
		ASSERT(vModifiedPropertiesName.size() == vModifiedPropertiesOld.size());
		ASSERT(vModifiedPropertiesName.size() == vModifiedPropertiesNew.size());

		p_Row->AddField(vID,				m_ColTargetResourcesId);
		p_Row->AddField(vDisplayName,		m_ColTargetResourcesDisplayName);
		p_Row->AddField(vType,				m_ColTargetResourcesType);
		p_Row->AddField(vUserPrincipalName,	m_ColTargetResourcesUserPrincipalName);
		p_Row->AddField(vGroupType,			m_ColTargetResourcesGroupType);

		if (!vModifiedPropertiesName.empty())
			p_Row->AddField(vModifiedPropertiesName, m_ColTargetResourcesModifiedPropertiesDisplayName).SetIsFromMultivalueNested(singleResource && vModifiedPropertiesName.size() > 1);
		else
			p_Row->RemoveField(m_ColTargetResourcesModifiedPropertiesDisplayName);

		if (!vModifiedPropertiesOld.empty())
			p_Row->AddField(vModifiedPropertiesOld, m_ColTargetResourcesModifiedPropertiesOldValue).SetIsFromMultivalueNested(singleResource && vModifiedPropertiesOld.size() > 1);
		else
			p_Row->RemoveField(m_ColTargetResourcesModifiedPropertiesOldValue);

		if (!vModifiedPropertiesNew.empty())
			p_Row->AddField(vModifiedPropertiesNew, m_ColTargetResourcesModifiedPropertiesNewValue).SetIsFromMultivalueNested(singleResource && vModifiedPropertiesNew.size() > 1);
		else
			p_Row->RemoveField(m_ColTargetResourcesModifiedPropertiesNewValue);
	}
}