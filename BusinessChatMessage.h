#pragma once

#include "ChatAttachment.h"
#include "ChatMention.h"
#include "ChatReaction.h"
#include "BusinessObject.h"
#include "IdentitySet.h"
#include "ItemBody.h"
//#include "ChatMessage.h"

class BusinessChatMessage : public BusinessObject
{
public:
    BusinessChatMessage();
	//BusinessChatMessage(const ChatMessage& p_ChatMessage);

	const boost::YOpt<PooledString>& GetReplyToId() const;
	void SetReplyToId(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<IdentitySet>& GetFrom() const;
	void SetFrom(const boost::YOpt<IdentitySet>& p_Val);

	const boost::YOpt<PooledString>& GetEtag() const;
	void SetEtag(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetMessageType() const;
	void SetMessageType(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<YTimeDate>& GetCreatedDateTime() const;
	void SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_Val);

	const boost::YOpt<YTimeDate>& GetLastModifiedDateTime() const;
	void SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val);

	const boost::YOpt<bool>& GetDeleted() const;
	void SetDeleted(const boost::YOpt<bool>& p_Val);

	const boost::YOpt<YTimeDate>& GetDeletedDateTime() const;
	void SetDeletedDateTime(const boost::YOpt<YTimeDate>& p_Val);

	const boost::YOpt<PooledString>& GetSubject() const;
	void SetSubject(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<ItemBody>& GetBody() const;
	void SetBody(const boost::YOpt<ItemBody>& p_Val);

	const boost::YOpt<PooledString>& GetSummary() const;
	void SetSummary(const boost::YOpt<PooledString>& p_Val);

	const vector<ChatAttachment>& GetAttachments() const;
	void SetAttachments(const vector<ChatAttachment>& p_Val);

	const vector<ChatMention>& GetMentions() const;
	void SetMentions(const vector<ChatMention>& p_Val);

	const boost::YOpt<PooledString>& GetImportance() const;
	void SetImportance(const boost::YOpt<PooledString>& p_Val);

	const vector<ChatReaction>& GetReactions() const;
	void SetReactions(const vector<ChatReaction>& p_Val);

	const boost::YOpt<PooledString>& GetLocale() const;
	void SetLocale(const boost::YOpt<PooledString>& p_Val);

	const vector<BusinessChatMessage>& GetReplies() const;
	void SetReplies(const vector<BusinessChatMessage>& p_Val);
	void AddReply(const BusinessChatMessage& p_Val);

	//static ChatMessage ToChatMessage(const BusinessChatMessage& p_BusinessChatMessage);

protected:
	boost::YOpt<PooledString> m_ReplyToId;
	boost::YOpt<IdentitySet> m_From;
	boost::YOpt<PooledString> m_Etag;
	boost::YOpt<PooledString> m_MessageType;
	boost::YOpt<YTimeDate> m_CreatedDateTime;
	boost::YOpt<YTimeDate> m_LastModifiedDateTime;
	boost::YOpt<bool> m_Deleted;
	boost::YOpt<YTimeDate> m_DeletedDateTime;
	boost::YOpt<PooledString> m_Subject;
	boost::YOpt<ItemBody> m_Body;
	boost::YOpt<PooledString> m_Summary;
	vector<ChatAttachment> m_Attachments;
	vector<ChatMention> m_Mentions;
	boost::YOpt<PooledString> m_Importance;
	vector<ChatReaction> m_Reactions;
	boost::YOpt<PooledString> m_Locale;

	vector<BusinessChatMessage> m_Replies;

    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND
    friend class ChatMessageDeserializer;
};
