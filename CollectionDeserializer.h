#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "Collection.h"

namespace Cosmos
{
    class CollectionDeserializer : public JsonObjectDeserializer, public Encapsulate<Cosmos::Collection>
    {
    public:
        void DeserializeObject(const web::json::object& p_Object) override;
    };
}

