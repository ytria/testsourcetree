#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "AppIdentity.h"

class AppIdentityDeserializer : public JsonObjectDeserializer, public Encapsulate<AppIdentity>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

