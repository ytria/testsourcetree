#pragma once

#include "IPropertySetBuilder.h"

class EventPropertySet : public IPropertySetBuilder
{
public:
	EventPropertySet();

	void SetBodyPreview(bool p_Set);
	void SetBodyContent(bool p_Set);

	vector<rttr::property> GetPropertySet() const override;
	vector<PooledString> GetStringPropertySet() const override;

private:
	bool m_BodyPreview;
	bool m_FullBody;
};

