#include "MainFrame.h"

#include "ActivityLogger.h"
#include "AutomationNames.h"
#include "AutomationRecording.h"
#include "AutomationWizardMainFrame.h"
#include "BackstagePanelSessions.h"
#include "BasicPageRequestLogger.h"
#include "Command.h"
#include "CommandDispatcher.h"
#include "ContractListRequester.h"
#include "DlgBrowser.h"
#include "DlgCreateLoadSessionHTML.h"
#include "DlgInputRequest.h"
#include "DlgLicenseMessageBoxHTML.h"
#include "DlgLicenseTenantUserCaps.h"
#include "DlgLicenseUpgrade.h"
#include "DlgLicenseUpgradeReport.h"
#include "DlgPasswordInput.h"
#include "DlgProtectedApiHTML.h"
#include "DlgSelectCustomerHTML.h"
#include "DlgTenantInfo.h"
#include "DoModalHooker.h"
#include "FileUtil.h"
#include "GraphCache.h"
#include "GridFrameBase.h"
#include "GridSnapshot.h"
#include "InitAfterSessionOrLicenseWasSet.h"
#include "IRibbonSystemMenuBuilder.h"
#include "DlgJobCenterGridDebug.h"
#include "LoggerService.h"
#include "Messages.h"
#include "MFCUtil.h"
#include "MSGraphSession.h"
#include "OAuth2Authenticators.h"
#include "O365AdminUtil.h"
#include "O365GridSnapshot.h"
#include "O365HtmlMainView.h"
#include "O365LicenseValidator.h"
#include "Office365Admin.h"
#include "Product.h"
#include "ProductUpdate.h"
#include "RESTException.h"
#include "RibbonSystemMenuBuilders.h"
#include "RoleDelegationManager.h"
#include "SessionTypes.h"
#include "SharedResource.h"
#include "SkuAndPlanReference.h"
#include "SubscribedSkusRequester.h"
#include "SugarCRMSession.h"
#include "TaskDataManager.h"
#include "TimeUtil.h"
#include "YCallbackMessage.h"
#include "YCodeJockMessageBox.h"
#include "YFileDialog.h"
#include "YtriaTranslate.h"
#include "YTestHtmlDlg.h"

#include "DlgSelectRoleSessionHTML.h"
#include "BaseO365Grid.h"
#include "GridSchools.h"
#include "EducationSchool.h"
#include "SessionsSqlEngine.h"
#include "UISessionListSorter.h"
#include "DbSessionPersister.h"
#include "DlgSpecialExtAdminAccess.h"
#include "QuerySetToElevatedSession.h"
#include "QueryRoleExists.h"
#include "QueryCreateRole.h"
#include "QuerySessionExists.h"
#include "QueryGetCredentialsToShare.h"
#include "DlgSpecialExtAdminUltraAdminJsonGenerator.h"
#include "CommandBars/XTPControls.h"
#include "BasicHttpRequestLogger.h"
#include "MsGraphHttpRequestLogger.h"
#include "QueryDeleteSession.h"
#include "ApplicationRequester.h"
#include "BasicRequestLogger.h"
#include "ApplicationListRequester.h"
#include "DlgLoading.h"
#include "UpdateApplicationRequester.h"
#include "DlgSpecialExtAdminManageElevatedJsonGenerator.h"
#include "DlgSpecialExtAdminNewElevatedJsonGenerator.h"
#include "DeleteAppRequester.h"
#include "SessionLoaderFactory.h"
#include "SessionSwitcher.h"
#include "RoleSessionLoader.h"
#include "UltraAdminSessionLoader.h"
#include "ElevatedSessionLoader.h"
#include "AppGraphSessionCreator.h"
#include "DlgOnPremServerConfig.h"
#include "DlgOnPremisesOptions.h"

const wstring MainFrame::g_homePageResourceName = _YTEXT("hbshandler.html");
const wstring MainFrame::g_debug_homePageResourceName = _YTEXT("modules.html");

const wstring MainFrame::g_debug_debugGroup = _YTEXT("!! YTRIA DEBUG !!");

wstring MainFrame::g_AvailableSessionGroupName;

const UINT MainFrame::g_UpdateNotificationCode = 1;

wstring MainFrame::g_DumpModeText;
wstring MainFrame::g_DumpModeTooltip;

bool MainFrame::askToCombineWithUltraAdmin(std::shared_ptr<Sapio365Session> p_Session, PersistentSession& p_PersistentSession, bool p_JustCreatedFullSession, CFrameWnd* p_FrameSource)
{
	bool sessionChanged = false;

	auto allSessions = SessionsSqlEngine::Get().GetList();
	std::vector<PersistentSession> ultraAdminMatches;
	std::copy_if(allSessions.begin(), allSessions.end(), std::back_inserter(ultraAdminMatches), [tenantDomain = p_PersistentSession.GetTenantName()](const PersistentSession& p_Session) {
		return p_Session.IsUltraAdmin() && p_Session.GetTenantName() == tenantDomain;
	});

	if (ultraAdminMatches.size() != 0)
	{
		bool askAgain = true;
		while (askAgain)
		{
			DlgCreateLoadSessionHTML dlg(ultraAdminMatches);
			dlg.SetIsElevating(true);
			if (dlg.DoModal() == IDOK)
			{
				if (dlg.GetWantedAction() == DlgCreateLoadSessionHTML::Mode::LOAD_SESSION)
				{
					SessionIdentifier selectedSession = dlg.GetSelectedSession();
					auto itt = std::find_if(ultraAdminMatches.begin(), ultraAdminMatches.end(), [selectedSession](const PersistentSession& p_Session) { return p_Session.GetIdentifier() == selectedSession; });
					ASSERT(itt != ultraAdminMatches.end());
					if (itt != ultraAdminMatches.end())
					{
						AppGraphSessionCreator creator(itt->GetTenantName(), itt->GetAppId(), itt->GetAppClientSecret(), itt->GetSessionName());
						auto appSession = creator.Create();
						auto testResult = RoleDelegationUtil::TestSessionWithDialog(appSession, itt->GetTenantName(), p_FrameSource);
						if (testResult.first) // Success?
						{
							SaveAsElevatedSession(p_PersistentSession, itt->GetAppId(), itt->GetAppClientSecret());
							p_Session->GetGateways().SetUserGraphSession(p_Session->GetMainMSGraphSession());
							p_Session->GetGateways().SetAppGraphSession(appSession);
							p_Session->GetGateways().SetAppGraphSessionCredentials(itt->GetAppId(), itt->GetAppClientSecret());
							p_Session->GetGateways().SetTenant(itt->GetTenantName());
							p_Session->SetSessionType(p_PersistentSession.GetSessionType());
							YCodeJockMessageBox msgBox(p_FrameSource, DlgMessageBox::eIcon_Question, _T("Remove this Ultra Admin session?"), _T("It is now safe to do so since its privileges have been successfully merged with this Advanced session."), _YTEXT(""), { {IDOK, _T("Yes")}, {IDCANCEL, _T("No")} });
							if (msgBox.DoModal() == IDOK)
							{
								QueryDeleteSession sessionDeleter(SessionsSqlEngine::Get(), selectedSession);
								sessionDeleter.Run();
							}
							sessionChanged = true;
							askAgain = false;
						}
						else
						{
							Util::ShowApplicationConnectionError(_T("Unable to use the selected Ultra Admin session for privileges elevation."), testResult.second, p_FrameSource);
						}
					}
				}
				else if (dlg.GetWantedAction() == DlgCreateLoadSessionHTML::Mode::NEW_ULTRA_ADMIN_OR_SKIP)
				{
					askAgain = false;
					sessionChanged = elevate(p_Session, p_PersistentSession, p_JustCreatedFullSession, p_FrameSource);
				}
			}
			else
				askAgain = false;
		}
	}
	else
	{
		sessionChanged = elevate(p_Session, p_PersistentSession, p_JustCreatedFullSession, p_FrameSource);
	}

	return sessionChanged;
}

bool MainFrame::elevate(std::shared_ptr<Sapio365Session> p_Session, PersistentSession& p_PersistentSession, bool p_JustCreatedFullSession, CFrameWnd* p_FrameSource)
{
	bool sessionChanged = false;

	ElevatedPrivilegesDlgResult action = ElevatedPrivilegesDlgResult::Elevate;
	if (p_PersistentSession.GetAppId().empty())
		action = Util::ShowExplainElevatedPrivilegesDialog(p_FrameSource, p_JustCreatedFullSession);
	if (action == ElevatedPrivilegesDlgResult::Elevate)
	{
		sessionChanged = createUltraAdminForElevatedSession(p_Session, p_PersistentSession, false, p_FrameSource);
	}
	else if (action == ElevatedPrivilegesDlgResult::ElevateManually)
	{
		bool askAgain = true;
		wstring tenantName = p_PersistentSession.GetTenantName();
		wstring appId = p_PersistentSession.GetAppId();
		wstring appClientSecret = p_PersistentSession.GetAppClientSecret();
		wstring appDisplayName;

		if (!appId.empty())
		{
			DlgLoading::RunModal(_T("Querying application data..."), p_FrameSource, [appId, sapioSession = p_Session, &appDisplayName](const std::shared_ptr<DlgLoading>& p_DlgLoading) {
				auto application = ApplicationListRequester::GetAppFromAppId(sapioSession, appId);
				if (application.m_DisplayName)
					appDisplayName = *application.m_DisplayName;
				// FIXME: What if we didn't find the app?? we let appDisplayName empty so default generated name will be shown in DlgSpecialExtAdminAccess along with appID, which is wrong.
			});
		}

		while (askAgain)
		{
			DlgSpecialExtAdminAccess dlgElevateManually(tenantName, appId, appClientSecret, appDisplayName, p_FrameSource, p_Session, std::make_unique<DlgSpecialExtAdminNewElevatedJsonGenerator>(p_Session), false);
			dlgElevateManually.SetTitle(_T("Custom Manage Elevated Privileges"));
			dlgElevateManually.SetAdditionalHeight(20);

			if (dlgElevateManually.DoModal() == IDOK)
			{
				AppGraphSessionCreator creator(dlgElevateManually.GetTenant(), dlgElevateManually.GetAppId(), dlgElevateManually.GetClientSecret(), appDisplayName);
				auto appSession = creator.Create();
				auto testResult = RoleDelegationUtil::TestSessionWithDialog(appSession, dlgElevateManually.GetTenant(), p_FrameSource);
				if (testResult.first) // Success?
				{
					SaveAsElevatedSession(p_PersistentSession, dlgElevateManually.GetAppId(), dlgElevateManually.GetClientSecret());
					p_Session->GetGateways().SetUserGraphSession(p_Session->GetMainMSGraphSession());
					p_Session->GetGateways().SetAppGraphSession(appSession);
					p_Session->SetSessionType(p_PersistentSession.GetSessionType());
					sessionChanged = true;
					askAgain = false;
				}
				else
				{
					Util::ShowApplicationConnectionError(_T("Unable to use the provided Application parameters for privileges elevation."), testResult.second, p_FrameSource);
					tenantName = dlgElevateManually.GetTenant();
					appId = dlgElevateManually.GetAppId();
					appClientSecret = dlgElevateManually.GetClientSecret();
					appDisplayName = dlgElevateManually.GetDisplayName();
				}
			}
			else
			{
				askAgain = false;
			}
		}
	}

	return sessionChanged;
}

void MainFrame::SaveAsElevatedSession(PersistentSession& p_Session, const wstring& p_AppId, const wstring& p_ClientSecret)
{
	if (p_Session.IsAdvanced())
		p_Session.SetSessionType(SessionTypes::g_ElevatedSession);
	else if (p_Session.IsPartnerAdvanced())
		p_Session.SetSessionType(SessionTypes::g_PartnerElevatedSession);
	p_Session.SetAppId(p_AppId);
	p_Session.SetAppClientSecret(p_ClientSecret);

	QuerySetToElevatedSession query(p_Session, SessionsSqlEngine::Get());
	query.Run();
}

bool MainFrame::createUltraAdminForElevatedSession(std::shared_ptr<Sapio365Session> p_Session, PersistentSession& p_PersistentSession, bool p_ShowCreateUAdminDlg, CFrameWnd* p_FrameSource)
{
	bool sessionChanged = false;

	wstring tenantName = p_PersistentSession.GetTenantName();
	wstring appId;
	wstring appClientSecret;
	wstring appDisplayName;
	wstring createdAppId;

	// Attempt to create it without any user intervention
	if (p_PersistentSession.GetAppId().empty())
	{
		appDisplayName = Util::PromptForNewApplicationName(Util::GetElevatedSessionAppName(p_PersistentSession), p_FrameSource);
		if (appDisplayName.empty()) // canceled
			return false;
		RoleDelegationUtil::CreateApplication(p_Session, tenantName, p_PersistentSession, appDisplayName, p_FrameSource);
	}
	else
	{
		appId = p_PersistentSession.GetAppId();
		appClientSecret = p_PersistentSession.GetAppClientSecret();
		p_ShowCreateUAdminDlg = true;
	}
	
	if (p_ShowCreateUAdminDlg)
	{
		if (!appId.empty())
		{
			DlgLoading::RunModal(_T("Querying application data..."), p_FrameSource, [appId, p_Session, &appDisplayName](const std::shared_ptr<DlgLoading>& p_DlgLoading) {
				auto application = ApplicationListRequester::GetAppFromAppId(p_Session, appId);
				if (application.m_DisplayName)
					appDisplayName = *application.m_DisplayName;
				// FIXME: What if we didn't find the app?? we let appDisplayName empty so default generated name will be shown in DlgSpecialExtAdminAccess along with appID, which is wrong.
			});
		}
		bool askAgain = true;
		while (askAgain)
		{
			DlgSpecialExtAdminAccess dlg(tenantName, appId, appClientSecret, appDisplayName, nullptr, p_Session, std::make_unique<DlgSpecialExtAdminNewElevatedJsonGenerator>(p_Session), false);
			dlg.SetTitle(_T("Add Elevated Privileges"));
			dlg.SetAdditionalHeight(25);
			dlg.SetCreatedAppId(createdAppId);
			if (dlg.DoModal() == IDOK)
			{
				AppGraphSessionCreator creator(dlg.GetTenant(), dlg.GetAppId(), dlg.GetClientSecret(), appDisplayName);
				auto appSession = creator.Create();
				auto testResult = RoleDelegationUtil::TestSessionWithDialog(appSession, dlg.GetTenant(), p_FrameSource);
				if (testResult.first) // Success?
				{
					SaveAsElevatedSession(p_PersistentSession, dlg.GetAppId(), dlg.GetClientSecret());
					p_Session->GetGateways().SetUserGraphSession(p_Session->GetMainMSGraphSession());
					p_Session->GetGateways().SetAppGraphSession(appSession);
					p_Session->GetGateways().SetAppGraphSessionCredentials(dlg.GetAppId(), dlg.GetClientSecret());
					p_Session->GetGateways().SetTenant(dlg.GetTenant());
					p_Session->SetSessionType(p_PersistentSession.GetSessionType());
					sessionChanged = true;
					askAgain = false;
				}
				else
				{
					Util::ShowApplicationConnectionError(_T("Unable to use the new Application parameters for privileges elevation."), testResult.second, p_FrameSource);
					tenantName = dlg.GetTenant();
					appId = dlg.GetAppId();
					appClientSecret = dlg.GetClientSecret();
					appDisplayName = dlg.GetDisplayName();
					createdAppId = dlg.GetCreatedAppId();
				}
			}
			else
			{
				DeleteNewlyCreatedApp(dlg, p_Session, p_FrameSource);
				askAgain = false;
			}
		}
	}

	return sessionChanged;
}

void MainFrame::UpdateCanOpenWithRole(const wstring& p_ConnectedUserId, const wstring& p_SessionType, int64_t p_RoleId)
{
    bool hasRoleAvailable = false;

	if (p_SessionType == SessionTypes::g_StandardSession ||  p_SessionType == SessionTypes::g_Role)
	{
		try
		{
			std::vector<RoleDelegation> roles;
			if (RoleDelegationManager::GetInstance().GetRoleDelegationsForThisUser(p_ConnectedUserId, roles, GetSapio365Session()))
				hasRoleAvailable = !roles.empty();
			if (roles.size() == 1 && roles[0].GetID() == p_RoleId)
				hasRoleAvailable = false;
		}
		catch (const std::exception& e)
		{
			LoggerService::Debug(_YTEXTFORMAT(L"OnUpdateApplyRole: Unable to load role delegations: %s", Str::convertFromUTF8(e.what()).c_str()));
		}
	}

    m_CanOpenWithRole = hasRoleAvailable;
}

IMPLEMENT_DYNCREATE(MainFrame, AutomatedMainFrame);

// =================================================================================================
class MainFrame::DoModalHook : public IDoModalHook
{
public:
	DoModalHook(MainFrame* p_MainFrame)
		: m_MainFrame(p_MainFrame)
	{}

public: // IDoModalHook
	virtual bool OnBeginDoModal(CDialog* p_Dlg, CWnd* p_Parent) override
	{
		auto& frames = m_FramesByDialog[p_Dlg];

		ASSERT(frames.empty());
		const auto parent = MFCUtil::FindFirstParentOfType<CFrameWnd>(p_Parent);

		auto mainFrame = dynamic_cast<CFrameWnd*>(m_MainFrame);
		if (nullptr != mainFrame
			&& ::IsWindow(mainFrame->m_hWnd)
			&& mainFrame != parent
			&& mainFrame->IsWindowVisible()
			&& mainFrame->IsWindowEnabled())
		{
			mainFrame->EnableWindow(FALSE);
			frames.insert(mainFrame);
		}

		auto gridFrames = CommandDispatcher::GetInstance().GetAllGridFrames();
		for (auto frame : gridFrames)
		{
			if (::IsWindow(frame->m_hWnd)
				&& dynamic_cast<CFrameWnd*>(frame) != parent
				&& frame->IsWindowVisible()
				&& frame->IsWindowEnabled())
			{
				frame->EnableWindow(FALSE);
				frames.insert(frame);
			}
		}

		// Ensure the parent window is not minimized
		if (nullptr != parent && parent->IsIconic())
			parent->ShowWindow(SW_RESTORE);

		// Ensure the parent window is in foreground
		if (nullptr != parent)
			parent->SetForegroundWindow();

		return true;
	}

	virtual void OnEndDoModal(CDialog* p_Dlg, CWnd* p_Parent) override
	{
		auto& frames = m_FramesByDialog[p_Dlg];

		for (auto fr : frames)
		{
			auto frame = dynamic_cast<CFrameWnd*>(fr);
			if (nullptr != frame && ::IsWindow(frame->m_hWnd))
			{
				ASSERT(frame->IsWindowVisible());
				frame->EnableWindow(TRUE);
			}
		}

		m_FramesByDialog.erase(p_Dlg);

		// Ensure modal parent keeps the focus (EnableWindow above grabs focus).
		if (nullptr != p_Parent && ::IsWindow(p_Parent->m_hWnd))
			p_Parent->SetFocus();
	}

	bool ShowBlockingFrameIfDisabledByHook(CFrameWnd* p_Frame)
	{
		for (auto& item : m_FramesByDialog)
		{
			if (item.second.end() != item.second.find(p_Frame))
			{
				const auto parentFrame = MFCUtil::FindFirstParentOfType<CFrameWnd>(item.first->GetParent());
				if (nullptr != parentFrame && parentFrame != p_Frame)
				{
					// Ensure the parent window is not minimized
					if (parentFrame->IsIconic())
						parentFrame->ShowWindow(SW_RESTORE);

					// Ensure the parent window is in foreground
					parentFrame->SetForegroundWindow();
					// Focus and flash modal dialog to catch user attention
					item.first->ShowWindow(SW_SHOW); // Ensure modal dialog is still visible (see #181012.RL.008D06)
					item.first->SetFocus();
					item.first->FlashWindowEx(FLASHW_CAPTION, 7, 75); // Attempt to reproduce mfc modal window flashing.
				}
				return true;
			}
		}
		return false;
	}

private:
	std::map<CDialog*, std::set<CFrameWnd*>> m_FramesByDialog;
	MainFrame* m_MainFrame;
};

// =================================================================================================
class MainFrame::MouseInputHook
{
public:
	MouseInputHook(shared_ptr<MainFrame::DoModalHook> p_DoModalHook)
		: m_DoModalHook(p_DoModalHook)
	{
		ASSERT(nullptr == m_instance);
		m_Hook = SetWindowsHookEx(WH_MOUSE, callback, AfxGetApp()->m_hInstance, AfxGetApp()->m_nThreadID);

		m_instance = this;
	}

	~MouseInputHook()
	{
		ASSERT(m_instance == this);
		m_instance = nullptr;
	}

	static LRESULT CALLBACK callback(int code, WPARAM wparam, LPARAM lparam)
	{
		if (HC_ACTION == code && WM_NCLBUTTONDOWN == wparam)
		{
			auto mouseHookStruct = (MOUSEHOOKSTRUCT*)lparam;
			auto parentFrame = MFCUtil::FindFirstParentOfType<CFrameWnd>(CWnd::FromHandle(mouseHookStruct->hwnd));
			if (nullptr != parentFrame && !parentFrame->IsWindowEnabled())
			{
				ASSERT(nullptr != m_instance);
				m_instance->m_DoModalHook->ShowBlockingFrameIfDisabledByHook(parentFrame);
			}
		}
		return 0;
	}

private:
	HHOOK m_Hook;
	static MouseInputHook* m_instance;
	shared_ptr<MainFrame::DoModalHook> m_DoModalHook;
};

MainFrame::MouseInputHook* MainFrame::MouseInputHook::m_instance = nullptr;

// =================================================================================================

MainFrame::MainFrame()
	: MainFrameBase(true)
	, m_oauth2Browser(nullptr)
	, m_HtmlView(nullptr)
	, m_connectTab(nullptr)
	, m_DebugMode(false)
	, m_SyncInProgress(false)
	, m_UpdateOnQuit(false)
	, m_DoModalHook(std::make_shared<DoModalHook>(this))
	, m_SyncCacheEvent(FALSE, TRUE)
	, m_BackstagePanelSessions(nullptr)
	, m_MouseInputHook(std::make_unique<MouseInputHook>(m_DoModalHook))
	, m_LicenseManager(nullptr)
    , m_CanOpenWithRole(false)
	, m_RoleInfoControl(nullptr)
	, m_RoleSelectControl(nullptr)
	, m_AlreadyShownMessageBar(false)
	, m_HasContracts(false)
	, m_ConnectToCustomerControl(nullptr)
{
	DoModalHooker::SetHook(m_DoModalHook);

	if (g_AvailableSessionGroupName.empty())
		g_AvailableSessionGroupName = YtriaTranslate::Do(MainFrame_MainFrame_2, _YLOC("Recent Sessions")).c_str();

	if (g_DumpModeText.empty())
		g_DumpModeText = _YTEXT("\x26a0 ") + YtriaTranslate::Do(MainFrame_MainFrame_3, _YLOC("DEBUG MODE")) + _YTEXT(" \x26a0");

	if (g_DumpModeTooltip.empty())
		g_DumpModeTooltip = YtriaTranslate::Do(MainFrame_MainFrame_4, _YLOC("Debug Mode is enabled.\nThis means every Graph request will be dumped to a log file.\nDisable this mode from the Main window / sapio365 tab / Logs.")).c_str();

	FrameBase<AutomatedMainFrame>::Init();

	setMenuBuilder(make_unique<MainFrameSystemMenuBuilder>());

	AutomatedMainFrame::Init();

	// If LicenseManager doesn't exist, it just does nothing.
	LicenseManager::AddLicenseManagerObserver(this);
}

MainFrame::~MainFrame()
{
	// If LicenseManager doesn't exist, it just does nothing.
	LicenseManager::RemoveLicenseManagerObserver(this);

	trackSessionUsageStop();
}

BEGIN_MESSAGE_MAP(MainFrame, MainFrameBase)
	ON_MESSAGE(WM_CLOSE, OnClose)
	ON_WM_GETMINMAXINFO()
    ON_COMMAND(ID_NEW_STANDARD_SESSION,						OnNewStandardSession)
    ON_UPDATE_COMMAND_UI(ID_NEW_STANDARD_SESSION,			OnUpdateNewStandardSession)
	ON_COMMAND(ID_NEW_ADVANCED_SESSION,						OnNewDelegatedAdminSession)
	ON_UPDATE_COMMAND_UI(ID_NEW_ADVANCED_SESSION,			OnUpdateNewDelegatedAdminSession)
	ON_COMMAND(ID_CONNECT_TO_CUSTOMER,						OnConnectToCustomer)
	ON_UPDATE_COMMAND_UI(ID_CONNECT_TO_CUSTOMER,			OnUpdateConnectToCustomer)
	ON_COMMAND(ID_NEW_ULTRAADMIN_SESSION,					OnNewUltraAdminSession)
    ON_UPDATE_COMMAND_UI(ID_NEW_ULTRAADMIN_SESSION,			OnUpdateNewUltraAdminSession)
	ON_COMMAND(ID_VIEW_SESSIONS,							OnViewSessions)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SESSIONS,					OnUpdateViewSessions)
	ON_COMMAND(ID_EDIT_ONPREM_SETTINGS_RIBBON,				OnEditOnPremSettings)
	ON_UPDATE_COMMAND_UI(ID_EDIT_ONPREM_SETTINGS_RIBBON,	OnUpdateEditOnPremSettings)
	ON_COMMAND(ID_VIEW_SCHOOLS,								OnViewSchools)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SCHOOLS,					OnUpdateViewSchools)
	ON_COMMAND(ID_VIEW_EDU_USERS,							OnViewEduUsers)
	ON_UPDATE_COMMAND_UI(ID_VIEW_EDU_USERS,					OnUpdateViewEduUsers)
	ON_COMMAND(ID_VIEW_APPLICATIONS,						OnViewApplications)
	ON_UPDATE_COMMAND_UI(ID_VIEW_APPLICATIONS,				OnUpdateViewApplications)
    ON_COMMAND(ID_VIEW_WEBCONSOLE,							OnViewWebConsole)
    ON_UPDATE_COMMAND_UI(ID_VIEW_WEBCONSOLE,				OnUpdateViewWebConsole)
	ON_COMMAND(ID_LOAD_SESSION,								OnLoadSession)
	ON_UPDATE_COMMAND_UI(ID_LOAD_SESSION,					OnUpdateLoadSession)
	ON_COMMAND(ID_SELECT_SESSION,							OnSelectSession)
	ON_UPDATE_COMMAND_UI(ID_SELECT_SESSION,					OnUpdateSelectSession)
	ON_UPDATE_COMMAND_UI(ID_LOGOUT,							OnUpdateLogout)
	ON_COMMAND(ID_SESSION_APPLY_ROLE,						OnApplyRole)
	ON_UPDATE_COMMAND_UI(ID_SESSION_APPLY_ROLE,				OnUpdateApplyRole)
	ON_COMMAND(ID_SESSION_ULTRAADMIN_EDIT,					OnEditAndLoadUltraAdmin)
	ON_UPDATE_COMMAND_UI(ID_SESSION_ULTRAADMIN_EDIT,		OnUpdateEditUltraAdmin)

	ON_COMMAND(ID_DEBUG_LOAD_USER_HTML,					OnLoadUserHtml_debug)
	ON_UPDATE_COMMAND_UI(ID_DEBUG_LOAD_USER_HTML,		OnUpdateLoadUserHtml_debug)
	ON_COMMAND(ID_DEBUG_RELOAD_CURRENT_HTML,			OnReloadCurrentHtml_debug)
	ON_UPDATE_COMMAND_UI(ID_DEBUG_RELOAD_CURRENT_HTML,	OnUpdateReloadCurrentHtml_debug)
    ON_COMMAND(ID_DEBUG_REINIT_HOMEPAGE,				OnReinitHomePage_debug)
    ON_UPDATE_COMMAND_UI(ID_DEBUG_REINIT_HOMEPAGE,		OnUpdateReinitHomePage_debug)
	ON_COMMAND(ID_DEBUG_LOAD_MODULES_HTML,				OnLoadModulesHtml_debug)
	ON_UPDATE_COMMAND_UI(ID_DEBUG_LOAD_MODULES_HTML,	OnUpdateLoadModulesHtml_debug)
	ON_COMMAND(ID_DEBUG_LOAD_HTML_IN_DLG,				OnLoadHtmlInDlg_debug)
	ON_UPDATE_COMMAND_UI(ID_DEBUG_LOAD_HTML_IN_DLG,		OnUpdateLoadHtmlInDlg_debug)
	ON_COMMAND(ID_DEBUG_SEND_RULES_TO_CLIPBOARD,		OnSendRulesToClipboard_debug)
	ON_UPDATE_COMMAND_UI(ID_DEBUG_SEND_RULES_TO_CLIPBOARD, OnUpdateSendRulesToClipboard_debug)
	ON_COMMAND(ID_DEBUG_SEND_SYSTEMVAR_TO_CLIPBOARD,	OnSendSystemVarToClipboard_debug)
	ON_UPDATE_COMMAND_UI(ID_DEBUG_SEND_SYSTEMVAR_TO_CLIPBOARD, OnUpdateSendSystemVarToClipboard_debug)
	ON_COMMAND(ID_DEBUG_SHOW_REQUEST_TESTER,			OnShowRequestTester_debug)
	ON_UPDATE_COMMAND_UI(ID_DEBUG_SHOW_REQUEST_TESTER,	OnUpdateShowRequestTester_debug)

	ON_COMMAND(ID_DEBUG_MODE,			OnDebugMode)
	ON_UPDATE_COMMAND_UI(ID_DEBUG_MODE, OnUpdateDebugMode)

    ON_COMMAND(ID_NEW_STANDARD_SESSION_HELP_WHAT_CAN_I_DO,				OnNewStandardSessionHelpWhatCanIDo)
    ON_UPDATE_COMMAND_UI(ID_NEW_STANDARD_SESSION_HELP_WHAT_CAN_I_DO,	OnUpdateNewStandardSessionHelpWhatCanIDo)
    ON_COMMAND(ID_NEW_STANDARD_SESSION_REPROVIDE_CONSENT,				OnNewStandardSessionReprovideConsent)
    ON_UPDATE_COMMAND_UI(ID_NEW_STANDARD_SESSION_REPROVIDE_CONSENT,		OnUpdateNewStandardSessionReprovideConsent)

    ON_COMMAND(ID_NEW_ADVANCED_USER_SESSION_HELP_WHAT_CAN_I_DO,				OnNewAdvancedUserSessionHelpWhatCanIDo)
    ON_UPDATE_COMMAND_UI(ID_NEW_ADVANCED_USER_SESSION_HELP_WHAT_CAN_I_DO,	OnUpdateNewAdvancedUserSessionHelpWhatCanIDo)
    ON_COMMAND(ID_NEW_ADVANCED_USER_SESSION_GIVE_ADMIN_CONSENT,				OnNewAdvancedUserSessionGiveAdminConsent)
    ON_UPDATE_COMMAND_UI(ID_NEW_ADVANCED_USER_SESSION_GIVE_ADMIN_CONSENT,	OnUpdateNewAdvancedUserSessionGiveAdminConsent)
    //ON_COMMAND(ID_NEW_ADVANCED_USER_SESSION_SEND_EMAIL_TO_GET_CONSENT, OnNewAdvancedUserSessionSendEmailToGetConsent)
    //ON_UPDATE_COMMAND_UI(ID_NEW_ADVANCED_USER_SESSION_SEND_EMAIL_TO_GET_CONSENT, OnUpdateNewAdvancedUserSessionSendEmailToGetConsent)

    ON_COMMAND(ID_NEW_ULTRAADMIN_SESSION_HELP_WHAT_CAN_I_DO,			OnNewUltraAdminSessionHelpWhatCanIDo)
    ON_UPDATE_COMMAND_UI(ID_NEW_ULTRAADMIN_SESSION_HELP_WHAT_CAN_I_DO,	OnUpdateNewUltraAdminSessionHelpWhatCanIDo)
    ON_COMMAND(ID_NEW_ULTRAADMIN_SESSION_HOW_TO_CREATE_APP,				OnNewUltraAdminSessionHowToCreateAnApp)
    ON_UPDATE_COMMAND_UI(ID_NEW_ULTRAADMIN_SESSION_HOW_TO_CREATE_APP,	OnUpdateNewUltraAdminSessionHowToCreateAnApp)
    ON_COMMAND(ID_NEW_ULTRAADMIN_SESSION_GIVE_ADMIN_CONSENT,			OnNewUltraAdminSessionGiveAdminConsent)
    ON_UPDATE_COMMAND_UI(ID_NEW_ULTRAADMIN_SESSION_GIVE_ADMIN_CONSENT,	OnUpdateNewUltraAdminSessionGiveAdminConsent)
	ON_COMMAND(ID_ULTRAADMIN_REQUEST_ACCESS_PROTECTED_API,					OnUltraAdminRequestAccessProtectedApi)
	ON_UPDATE_COMMAND_UI(ID_ULTRAADMIN_REQUEST_ACCESS_PROTECTED_API,		OnUpdateUltraAdminRequestAccessProtectedApi)
    //ON_COMMAND(ID_NEW_ULTRAADMIN_SESSION_SEND_EMAIL_TO_GET_CONSENT, OnNewUltraAdminSessionSendEmailToGetConsent)
    //ON_UPDATE_COMMAND_UI(ID_NEW_ULTRAADMIN_SESSION_SEND_EMAIL_TO_GET_CONSENT, OnUpdateNewUltraAdminSessionSendEmailToGetConsent)

    ON_REGISTERED_MESSAGE(WM_CLOSE_OAUTH2_BROWSER,				OnOAuth2BrowserClose)
    ON_REGISTERED_MESSAGE(WM_AUTHENTICATION_SUCCESS,			OnUserChange)
	ON_REGISTERED_MESSAGE(WM_AUTHENTICATION_CANCELED,			OnAuthenticationCanceled)
	ON_REGISTERED_MESSAGE(WM_AUTHENTICATION_FAILURE,			OnAuthenticationFailure)
	ON_REGISTERED_MESSAGE(WM_READY_TO_TEST_AUTHENTICATION,		OnReadyToTestAuthentication)
	ON_REGISTERED_MESSAGE(PRODUCTUPDATE_AVAILABLE_NOTIFICATION, OnProductUpdateAvailable)
	ON_REGISTERED_MESSAGE(PRODUCTUPDATE_QUIT_AND_INSTALL,		OnProductUpdateQuitAndInstall)
	ON_REGISTERED_MESSAGE(WM_SET_OAUTH2_BROWSER,				OnSetOAuth2Browser)
	ON_REGISTERED_MESSAGE(WM_GET_OAUTH2_BROWSER,				OnGetOAuth2Browser)

	ON_MESSAGE(TIN_XTP_TRAYICON, OnTrayIconNotify)

	ON_COMMAND(ID_MESSAGE_BAR_BUTTON, OnMessageBarButton)
	ON_COMMAND(ID_AUTOMATION_RECORDER, OnAutomationRecorder)

	ON_COMMAND(ID_JOBCENTER_DEBUG, OnViewJobCenterGridDebug)

	ON_COMMAND(ID_LOAD_SESSION_GALLERY_RESIZE_S, OnResizeSessionGalleryS)
	ON_COMMAND(ID_LOAD_SESSION_GALLERY_RESIZE_M, OnResizeSessionGalleryM)
	ON_COMMAND(ID_LOAD_SESSION_GALLERY_RESIZE_L, OnResizeSessionGalleryL)

	ON_COMMAND(ID_LOAD_GRID_PHOTO_SNAPSHOT, OnLoadPhotoSnapshot)
	ON_UPDATE_COMMAND_UI(ID_LOAD_GRID_PHOTO_SNAPSHOT, OnUpdateLoadPhotoSnapshot)

	ON_COMMAND(ID_LOAD_GRID_RESTORE_SNAPSHOT, OnLoadRestoreSnapshot)
	ON_UPDATE_COMMAND_UI(ID_LOAD_GRID_RESTORE_SNAPSHOT, OnUpdateLoadRestoreSnapshot)

	ON_COMMAND(ID_RIBBON_TOKENBUY, OnTokenBuy)

END_MESSAGE_MAP()

BOOL MainFrame::OnFrameLoaded()
{
	if (MainFrameBase::OnFrameLoaded())
	{
		SetAutomationName(_YTEXT("MainFrame"));

		// Create the HTML view.
		// Must be created this way or the view won't be automatically resized by the frame.
		CCreateContext cc;
		cc.m_pNewViewClass = RUNTIME_CLASS(O365HtmlMainView);
		cc.m_pCurrentDoc = NULL;
		cc.m_pNewDocTemplate = NULL;
		cc.m_pLastView = NULL;
		cc.m_pCurrentFrame = NULL;
		O365HtmlMainView* pHTMLView = dynamic_cast<O365HtmlMainView*>(CreateView(&cc));
		ASSERT(nullptr != pHTMLView);
		if (nullptr != pHTMLView)
		{
			m_HtmlView = pHTMLView;

			if (m_HtmlView->GetExStyle() & WS_EX_CLIENTEDGE)
				m_HtmlView->ModifyStyleEx(WS_EX_CLIENTEDGE, 0, SWP_FRAMECHANGED);

			m_HtmlView->YLoadFromResource(g_homePageResourceName.c_str());
		}

		enableUITheme();

		LoadCommandBars((wstring(g_ProfUisVersionSetting) + L"\\main\\").c_str());

		// Create the tray icon.
		if (!m_TrayIcon.Create(
			Product::getInstance().getApplication().c_str(), // Tooltip text
			this,                       // Parent window
			IDR_MAINFRAME,               // Icon resource ID
			0,             // Resource ID of popup menu
			0,                // Default menu item for popup menu
			false))                     // True if default menu item is located by position
		{
			ASSERT(false);
			TRACE0("Failed to create tray icon\n");
		}
		m_TrayIcon.HideIcon();

		return TRUE;
	}

	return FALSE;
}

void MainFrame::HTMLRefreshAll()
{
	ASSERT(nullptr != m_HtmlView);
	if (nullptr != m_HtmlView)
	{
		ASSERT(m_HtmlView->HasNoJob());
		if (m_HtmlView->HasNoJob())
			m_HtmlView->UpdateAll();// RefreshHTMLBulldozer();
	}
}

void MainFrame::HTMLRemoveOneJob(const Script& p_Job)
{
	ASSERT(nullptr != m_HtmlView);
	if (nullptr != m_HtmlView)
		m_HtmlView->RemoveOneJob(p_Job);
}

void MainFrame::HTMLRemoveOneSchedule(const ScriptSchedule& p_JobSchedule)
{
	ASSERT(nullptr != m_HtmlView);
	if (nullptr != m_HtmlView)
		m_HtmlView->RemoveOneSchedule(p_JobSchedule);
}

void MainFrame::HTMLUpdateJobs()
{
	ASSERT(nullptr != m_HtmlView);
	if (nullptr != m_HtmlView)
		m_HtmlView->UpdateAllJobs();
}

void MainFrame::HTMLUpdateSchedules()
{
	ASSERT(nullptr != m_HtmlView);
	if (nullptr != m_HtmlView)
		m_HtmlView->UpdateAllSchedules();
}

void MainFrame::HTMLUpdateAJL()
{
	ASSERT(nullptr != m_HtmlView);
	if (nullptr != m_HtmlView)
		m_HtmlView->UpdateAJLInfo();
}

void MainFrame::HTMLUpdateAJLcanRemoveJobs()
{
	ASSERT(nullptr != m_HtmlView);
	if (nullptr != m_HtmlView)
		m_HtmlView->UpdateCanRemoveJobs();
}

void MainFrame::TokenBuy()
{
	ConfigureLicense(getLicenseManager()->GetLicense(false));
}

LRESULT MainFrame::OnClose(WPARAM wParam, LPARAM lParam)
{
	bool shouldProceed = true;

	const auto askForConfirmation = !static_cast<bool>(wParam);
	if (askForConfirmation && !m_UpdateOnQuit)
	{
		const wstring message = YtriaTranslate::Do(MainFrame_OnClose_16, _YLOC("Are you sure you want to quit %1?"), Product::getInstance().getApplication().c_str());
		const wstring extendedMessage = YtriaTranslate::Do(MainFrame_OnClose_17, _YLOC("All %1 windows will be closed."), Product::getInstance().getApplication().c_str());
		YCodeJockMessageBox confirmation(this,
			DlgMessageBox::eIcon_ExclamationWarning,
			_YTEXT(""),
			message,
			extendedMessage,
			{ { IDOK, YtriaTranslate::Do(MainFrame_OnClose_11, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(MainFrame_OnClose_12, _YLOC("No")).c_str() } });
		shouldProceed = confirmation.DoModal() == IDOK;
	}

	if (shouldProceed)
	{
		// Seems we don't want to clean dump files on app exit anymore.
		/*if (Sapio365Session::IsDumpGloballyEnabled())
			ToggleDumpMode(this, false);*/

		auto gridFrames = CommandDispatcher::GetInstance().GetAllGridFrames();

		// Remove frames that are part of history (i.e. not active!)
		gridFrames.erase(
			std::remove_if(gridFrames.begin(), gridFrames.end(), [](auto& gridFrame)
			{
				// gridFrame->IsWindowVisible() => Temporary way to handle new back/forward system. We should maybe do it differently.
				return nullptr == gridFrame || !::IsWindow(gridFrame->m_hWnd) || !gridFrame->IsWindowVisible();
			}), gridFrames.end());


		for (const auto& gridFrame : gridFrames) // If we have a frame with an active modal or a running task, bring it to top instead of quitting
		{
			ASSERT(gridFrame->IsWindowVisible());
			if (!gridFrame->IsWindowEnabled() // If blocked by a modal
				//|| !gridFrame->HasNoTaskRunning() // If a task is still running // We don't want to block exit in this case anymore (Bug #180808.RL.00893E)
				)
			{
				::BringWindowToTop(gridFrame->GetSafeHwnd());
				m_UpdateOnQuit = false;
				return 0;
			}
		}

		if (AutomatedApp::IsAutomationRunning())
		{
			auto app = dynamic_cast<AutomatedApp*>(AfxGetApp());
			ASSERT(nullptr != app);
			if (nullptr != app)
				app->StopAutomation();
		}

		vector<GridFrameBase*> remainingFrames;
		for (const auto& gridFrame : gridFrames)
		{
			ASSERT(nullptr != gridFrame);				
			if (nullptr != gridFrame && ::IsWindow(gridFrame->m_hWnd) && gridFrame->IsWindowVisible())
			{
				if (gridFrame->CanBeClosedWithoutConfirmation())
				{
					const auto res = gridFrame->CloseFrame();
					ASSERT(res);
				}
				else
				{
					remainingFrames.push_back(gridFrame);
				}
			}
		}

		for (const auto& gridFrame : remainingFrames)
		{
			ASSERT(nullptr != gridFrame && gridFrame->IsWindowVisible());
			// gridFrame->IsWindowVisible() => Temporary way to handle new back/forward system. We should maybe do it differently.
			if (nullptr != gridFrame && gridFrame->IsWindowVisible() && !gridFrame->CloseFrame())
				return 0; // Cancel app closing
		}

		ASSERT(nullptr == getStatusBar() || NULL == getStatusBar()->GetSafeHwnd() || getStatusBar()->IsVisible()); // If this assert fails, CXTPStatusBar::SaveState will save a setting that hide the frame status bar.
		SaveCommandBars(g_ProfUisVersionSetting);

		bool cancelQuit = false;
		if (m_UpdateOnQuit)
		{
			UpdateWindow(); // Avoid half-painted backstage

			bool res = false;
			{
				CWaitCursor _; // This might take a while (sometimes more than 1 sec) to wait for the installer to run.
				res = ProductUpdate::GetInstance().RunInstaller();
			}

			if (!res)
			{
				YCodeJockMessageBox(this, DlgMessageBox::eIcon_Error, YtriaTranslate::Do(MainFrame_OnClose_13, _YLOC("ERROR")).c_str(), YtriaTranslate::Do(MainFrame_OnClose_14, _YLOC("Unable to launch update installer.")).c_str(), _YTEXT(""), { { IDOK, YtriaTranslate::Do(MainFrame_OnClose_15, _YLOC("OK")).c_str() } }).DoModal();
				cancelQuit = true;
			}

			m_UpdateOnQuit = false;
		}

		if (!cancelQuit)
		{
			Sapio365Session::CancelAsyncUserCountTaskIfNeeded(GetSapio365Session());

			const auto previousHook = DoModalHooker::SetHook(std::shared_ptr<IDoModalHook>());
			ASSERT(previousHook == m_DoModalHook);

			MainFrameBase::OnClose();
		}
	}

	return 0;
}

void MainFrame::OnNewStandardSession()
{
    Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
    if (nullptr != app)
        app->CreateStandardSession(boost::none);
}

void MainFrame::OnNewDelegatedAdminSession()
{
    Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
    if (nullptr != app)
        app->CreateAdvancedSession(boost::none);
}

void MainFrame::OnConnectToCustomer()
{
	if (m_HasContracts)
	{
		Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
		if (nullptr != app)
		{
			const auto contracts = GetSapio365Session()->GetContracts();
			if (!contracts.empty())
			{
				DlgSelectCustomerHTML dlg(contracts);
				if (IDOK == dlg.DoModal())
				{
					auto sel = dlg.GetSelectedContractIndex();
					if (0 <= sel && size_t(sel) < contracts.size())
					{
						const auto contract = contracts[sel];
						if (contract.m_DefaultDomainName)
							app->ConnectPartnerToCustomer(GetSapio365Session(), *contract.m_DefaultDomainName);
					}
					else
					{
						YCodeJockMessageBox(this
							, DlgMessageBox::eIcon_Error
							, _T("Error")
							, _T("No customer selected.")
							, _YTEXT("")
							, { { IDOK, YtriaTranslate::Do(MainFrame_OnLoadSession_3, _YLOC("OK")).c_str() } })
							.DoModal();
					}
				}
			}
			else
			{
				YCodeJockMessageBox(this
					, DlgMessageBox::eIcon_Information
					, _T("No Customer")
					, _T("No contract found.")
					, _YTEXT("")
					, { { IDOK, YtriaTranslate::Do(MainFrame_OnLoadSession_3, _YLOC("OK")).c_str() } })
					.DoModal();

				/*DlgInput dlg(_T("Enter customer tenant name"), _T("Customer Tenant"), _YTEXT(""), this);
				if (IDOK == dlg.DoModal())
				{
					app->ConnectPartnerSession(GetSapio365Session(), dlg.GetText());
				}*/
			}
		}
	}
}

void MainFrame::OnNewUltraAdminSession()
{
	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
	if (nullptr != app)
		app->CreateUltraAdminSession(_YTEXT(""), _YTEXT(""), _YTEXT(""), _YTEXT(""), boost::none, GetSapio365Session());
}

void MainFrame::OnViewSessions()
{
	const bool success = showBackstageTab(ID_BACKSTAGE_SESSIONS);
	ASSERT(success);
}

void MainFrame::OnEditOnPremSettings()
{
	auto sapio365Session = GetSapio365Session();
	ASSERT(nullptr != sapio365Session);
	if (nullptr != sapio365Session)
		Util::InitOnPrem(sapio365Session, true);
}

void MainFrame::OnViewSchools()
{
	CommandInfo commandInfo;
	commandInfo.SetOrigin(Origin::Tenant);
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Schools, Command::ModuleTask::List, commandInfo, { nullptr, g_ActionNameShowSchools, nullptr, nullptr }));
}

void MainFrame::OnViewEduUsers()
{
	if (GetSapio365Session())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::Tenant);
		HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

		if (GetSapio365Session()->GetSessionType() != SessionTypes::g_UltraAdmin)
		{
			if (Util::ShowRestrictedAccessDialog(
				DlgRestrictedAccess::Image::GRAPHAPI_SUPPORT
				, { YtriaTranslate::Do(O365Grid_showUserRestrictedAccessDialog_2, _YLOC("Reminder: The Microsoft Graph API may not fully support this yet in Admin mode.")).c_str(), YtriaTranslate::Do(O365Grid_showUserRestrictedAccessDialog_1, _YLOC("At the moment this query may require an Ultra Admin session to work as intended.")).c_str() }
				, HideUltraAdminRequirementDialogSetting()
				, this))
				CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::EduUsers, Command::ModuleTask::List, commandInfo, { nullptr, g_ActionNameShowSchools, nullptr, nullptr }));
		}
		else
		{
			CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::EduUsers, Command::ModuleTask::List, commandInfo, { nullptr, g_ActionNameShowSchools, nullptr, nullptr }));
		}
	}
}

void MainFrame::OnViewApplications()
{
	if (GetSapio365Session())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::Tenant);
		HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);
		
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Applications, Command::ModuleTask::List, commandInfo, { nullptr, g_ActionNameShowApplications, nullptr, nullptr }));
	}
}

void MainFrame::OnViewWebConsole()
{
	ASSERT(IsDebugMode());
	if (IsDebugMode())
	{
		DlgBrowser browser(L"https://portal.office.com/adminportal/home#/users", L"", this);
		browser.DoModal();
	}
}

namespace
{
	int GetWidth(int p_GallerySizeEnumVal)
	{
		int width = 200;

		switch (p_GallerySizeEnumVal)
		{
		case MainFrameSessionListSizeSetting::SMALL_GALLERY:
			width = 100;
			break;
		case MainFrameSessionListSizeSetting::MEDIUM_GALLERY:
			width = 200;
			break;
		case MainFrameSessionListSizeSetting::LARGE_GALLERY:
			width = 300;
			break;
		default:
			ASSERT(false);
			break;
		}

		return width;
	}
}

void MainFrame::resizeSessionGallery(int p_GallerySizeEnumVal, bool recalcLayout)
{
	auto availableSessionGroup = findGroup(*m_connectTab, g_AvailableSessionGroupName.c_str());
	ASSERT(nullptr != availableSessionGroup);
	if (nullptr != availableSessionGroup)
	{
		CXTPControlGallery* gallery = dynamic_cast<CXTPControlGallery*>(availableSessionGroup->FindControl(ID_LOAD_SESSION));
		ASSERT(nullptr != gallery && gallery->GetID() == ID_LOAD_SESSION);
		if (nullptr != gallery && gallery->GetID() == ID_LOAD_SESSION)
		{
			gallery->SetControlSize(CSize(GetWidth(p_GallerySizeEnumVal), 70));
			MainFrameSessionListSizeSetting().Set(p_GallerySizeEnumVal);

			auto buttonS = availableSessionGroup->FindControl(ID_LOAD_SESSION_GALLERY_RESIZE_S);
			buttonS->SetEnabled(MainFrameSessionListSizeSetting::SMALL_GALLERY != p_GallerySizeEnumVal);
			auto buttonM = availableSessionGroup->FindControl(ID_LOAD_SESSION_GALLERY_RESIZE_M);
			buttonM->SetEnabled(MainFrameSessionListSizeSetting::MEDIUM_GALLERY != p_GallerySizeEnumVal);
			auto buttonL = availableSessionGroup->FindControl(ID_LOAD_SESSION_GALLERY_RESIZE_L);
			buttonL->SetEnabled(MainFrameSessionListSizeSetting::LARGE_GALLERY != p_GallerySizeEnumVal);
			
			if (recalcLayout)
				getRibbonBar().DelayLayout();
		}
	}
}

void MainFrame::OnResizeSessionGalleryS()
{
	resizeSessionGallery(MainFrameSessionListSizeSetting::SMALL_GALLERY, true);
}

void MainFrame::OnResizeSessionGalleryM()
{
	resizeSessionGallery(MainFrameSessionListSizeSetting::MEDIUM_GALLERY, true);
}

void MainFrame::OnResizeSessionGalleryL()
{
	resizeSessionGallery(MainFrameSessionListSizeSetting::LARGE_GALLERY, true);
}

void MainFrame::OnLoadSession()
{
    Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
    ASSERT(nullptr != app);
    if (nullptr == app)
        return;

	int64_t roleId = SessionIdentifier::g_NoRole;

    CXTPCommandBars* pCommandBars = GetCommandBars();

	auto* m_cjRibbonBar = dynamic_cast<CXTPRibbonBar*>(pCommandBars->GetMenuBar());

	int selectedSessionIndex = -1;
	boost::YOpt<PersistentSession> selectedSession;

    wstring sessionType;
	auto availableSessionGroup = findGroup(*m_connectTab, g_AvailableSessionGroupName.c_str());
	ASSERT(nullptr != availableSessionGroup);
	if (nullptr != availableSessionGroup)
	{
		CXTPControlGallery* gallery = dynamic_cast<CXTPControlGallery*>(availableSessionGroup->FindControl(ID_LOAD_SESSION));
		ASSERT(nullptr != gallery && gallery->GetID() == ID_LOAD_SESSION);
		if (nullptr != gallery && gallery->GetID() == ID_LOAD_SESSION)
		{
			selectedSessionIndex = gallery->GetSelectedItem();
            CXTPControlGalleryItem* item = gallery->GetItem(selectedSessionIndex);
            ASSERT(nullptr != item);
            if (nullptr != item)
            {
                auto itemData = reinterpret_cast<SessionIdentifier*>(item->GetData());
                if (itemData->m_SessionType == SessionTypes::g_Role)
                    roleId = itemData->m_RoleID;

				const auto sessions = SessionsSqlEngine::Get().GetList();
			    for (const auto& session : sessions)
			    {
				    if (session.GetIdentifier() == *itemData)
				    {
					    selectedSession = session;
					    break;
				    }
			    }
            }
			ASSERT(selectedSession);
		}
	}

	if (!selectedSession)
	{
        LoggerService::Debug(_YFORMAT(L"MainFrame::OnLoadSession: The session could not be loaded."));
		YCodeJockMessageBox(this, DlgMessageBox::eIcon_Error, YtriaTranslate::Do(MainFrame_OnLoadSession_1, _YLOC("ERROR")).c_str(), YtriaTranslate::Do(MainFrame_OnLoadSession_2, _YLOC("Session not found.")).c_str(), _YTEXT(""), { { IDOK, YtriaTranslate::Do(MainFrame_OnLoadSession_3, _YLOC("OK")).c_str() } }).DoModal();
	}
	else
	{
		if (Sapio365Session::CheckCompatibilityWithActiveRBAC(selectedSession->GetTenantName(), this, {}))
		{
			auto loader = SessionLoaderFactory(selectedSession->GetIdentifier()).CreateLoader();
			SessionSwitcher(std::move(loader)).Switch();
		}
	}
}

void MainFrame::OnSelectSession()
{
	auto sessions = SessionsSqlEngine::Get().GetList();
	if (!CRMpipe().IsFeatureSandboxed())
	{
		auto isUltraAdmin = [](const PersistentSession& p_Session) {
			return p_Session.IsUltraAdmin();
		};
		sessions.erase(std::remove_if(sessions.begin(), sessions.end(), isUltraAdmin), sessions.end());
	}

	DlgCreateLoadSessionHTML dlg(sessions);
	if (dlg.DoModal() == IDOK)
	{
		if (dlg.ShouldCreateAdvancedSession())
		{
			OnNewDelegatedAdminSession();
		}
		else if (dlg.ShouldCreateUltraAdminSession())
		{
			OnNewUltraAdminSession();
		}
        else if (dlg.ShouldCreateStandardSession())
        {
            OnNewStandardSession();
        }
		else
		{
			ASSERT(!dlg.ShouldCreateAdvancedSession() && !dlg.ShouldCreateUltraAdminSession());
			auto sessionId = dlg.GetSelectedSession();
			Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
			ASSERT(nullptr != app);
            if (nullptr != app)
            {
				boost::YOpt<PersistentSession> selectedSession;
				const auto sessions = SessionsSqlEngine::Get().GetList();
				for (const auto& session : sessions)
				{
					if (session.GetEmailOrAppId() == sessionId.m_EmailOrAppId)
					{
						selectedSession = session;
						break;
					}
				}

				ASSERT(selectedSession);

				if (selectedSession && Sapio365Session::CheckCompatibilityWithActiveRBAC(selectedSession->GetTenantName(), this, {}))
				{
					auto loader = SessionLoaderFactory(sessionId).CreateLoader();
					SessionSwitcher(std::move(loader)).Switch();
				}
            }
		}
	}
}

void MainFrame::OnApplyRole()
{
    vector<RoleDelegation> roles;
	RoleDelegationManager::GetInstance().GetRoleDelegationsForThisUser(GetSessionInfo().GetConnectedUserId(), roles, GetSapio365Session());
    DlgSelectRoleSessionHTML dlg(roles, this, true);
    auto result = dlg.DoModal();
    if (result == IDOK)
    {
        if (dlg.GetSelectedAction() == DlgSelectRoleSessionHTML::Action::LoadRole)
        {
            auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
            ASSERT(nullptr != app);
			if (nullptr != app)
			{
				auto sessionId = SessionIdentifier(GetSessionInfo().GetEmailOrAppId(), SessionTypes::g_Role, dlg.GetSelectedRole());
				auto loader = std::make_unique<RoleSessionLoader>(sessionId);
				loader->SetHideRoleUnscopedObjectsOverride(dlg.GetHideUnscoped());
				SessionSwitcher(std::move(loader)).Switch();
			}
        }
    }
}

void MainFrame::EditAndLoadUltraAdmin(GridFrameBase* p_ParentGridFrameBase)
{
	auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != app)
	{
		MainFrame* mainFrame = dynamic_cast<MainFrame*>(app->m_pMainWnd);
		auto parentFrame = nullptr == p_ParentGridFrameBase ? static_cast<CFrameWnd*>(mainFrame) : static_cast<CFrameWnd*>(p_ParentGridFrameBase);
		auto persistentSession = nullptr == p_ParentGridFrameBase ? mainFrame->GetSessionInfo() : p_ParentGridFrameBase->GetSessionInfo();
		auto session = nullptr == p_ParentGridFrameBase ? mainFrame->GetSapio365Session() : p_ParentGridFrameBase->GetSapio365Session();
		ASSERT(session);

		persistentSession = SessionsSqlEngine::Get().Load(persistentSession.GetIdentifier()); // Reload

		bool resetSignedIn = false;
		if (persistentSession.IsUltraAdmin())
		{
			ASSERT(nullptr == p_ParentGridFrameBase); // Don't handle this case from module frames.

			auto loader = std::make_unique<UltraAdminSessionLoader>(persistentSession.GetIdentifier());
			loader->SetIsFirstAttempt(false);
			SessionSwitcher switcher(std::move(loader));
			switcher.SetForceReloadExisting(true);
			switcher.Switch();
		}
		else if (persistentSession.IsAdvanced() || persistentSession.IsPartnerAdvanced())
		{
			resetSignedIn = askToCombineWithUltraAdmin(session, persistentSession, false, parentFrame);
		}
		else if (persistentSession.IsElevated() || persistentSession.IsPartnerElevated())
		{
			bool done = false;

			bool saveSession = false;
			wstring appId = persistentSession.GetAppId();
			wstring appClientSecret = persistentSession.GetAppClientSecret();
			wstring createdAppId;

			const wstring originalAppId = appId;
			const wstring originalAppClientSecret = appClientSecret;

			auto saveAndUseAppInfo = [&persistentSession, session](const std::shared_ptr<MSGraphSession>& p_AppSession, const wstring& p_AppId, const wstring& p_ClientSecret) {
				session->GetGateways().SetAppGraphSession(p_AppSession);
				persistentSession.SetAppId(p_AppId);
				persistentSession.SetAppClientSecret(p_ClientSecret);
				SessionsSqlEngine::Get().Save(persistentSession);
			};

			auto downgrade = [&persistentSession, session]() {
				SessionsSqlEngine::Get().DowngradeElevatedSession(persistentSession.GetIdentifier());
				if (persistentSession.IsElevated())
				{
					persistentSession.SetSessionType(SessionTypes::g_AdvancedSession);
					session->SetSessionType(SessionTypes::g_AdvancedSession);
				}
				else if (persistentSession.IsPartnerElevated())
				{
					persistentSession.SetSessionType(SessionTypes::g_PartnerAdvancedSession);
					session->SetSessionType(SessionTypes::g_PartnerAdvancedSession);
				}
				else
				{
					ASSERT(false);
				}
				session->GetGateways().SetAppGraphSession(nullptr);
			};

			while (!done)
			{
				wstring appDispName = persistentSession.GetSessionName();
				DlgLoading::RunModal(_T("Querying application data..."), parentFrame, [appId, &appDispName, session](const std::shared_ptr<DlgLoading>& p_DlgLoading) {
					auto logger = std::make_shared<BasicRequestLogger>(_T("application"));
					auto application = ApplicationListRequester::GetAppFromAppId(session, appId);
					ASSERT(application.m_DisplayName);
					if (application.m_DisplayName)
						appDispName = *application.m_DisplayName;
					// FIXME: What if we didn't find the app?? we let appDisplayName empty so default generated name will be shown in DlgSpecialExtAdminAccess along with appID, which is wrong.
					});

				DlgSpecialExtAdminAccess dlg(persistentSession.GetTenantName(), appId, appClientSecret, appDispName, parentFrame, session, std::make_unique<DlgSpecialExtAdminManageElevatedJsonGenerator>(session), false);
				dlg.SetTitle(_T("Manage Elevated Privileges"));
				dlg.SetAdditionalHeight(45);
				dlg.SetCreatedAppId(createdAppId);
				bool ok = dlg.DoModal() == IDOK;

				appId = dlg.GetAppId();
				appClientSecret = dlg.GetClientSecret();
				createdAppId = dlg.GetCreatedAppId();

				if (ok)
				{
					if (dlg.GetDisplayName() != appDispName)
					{
						DlgLoading::RunModal(_T("Updating application data..."), parentFrame, [appId, newName = dlg.GetDisplayName(), session](const std::shared_ptr<DlgLoading>& p_DlgLoading) {
							auto application = ApplicationListRequester::GetAppFromAppId(session, appId);
							ASSERT(application.m_Id);
							if (application.m_Id)
							{
								auto updateRequester = std::make_shared<UpdateApplicationRequester>(*application.m_Id);
								updateRequester->SetDisplayName(newName);
								updateRequester->Send(session, YtriaTaskData()).GetTask().wait();
							}
							else
							{
								// FIXME: Show error
							}
						});
					}

					if (dlg.IsRemoveRequested())
					{
						downgrade();
						resetSignedIn = true;
						done = true;
					}
					else
					{
						AppGraphSessionCreator creator(persistentSession.GetTenantName(), dlg.GetAppId(), dlg.GetClientSecret(), appDispName);
						auto appSession = creator.Create();
						auto testResult = RoleDelegationUtil::TestSessionWithDialog(appSession, persistentSession.GetTenantName(), parentFrame);
						if (testResult.first)
						{
							saveAndUseAppInfo(appSession, dlg.GetAppId(), dlg.GetClientSecret());
							resetSignedIn = true;
							done = true;
						}
						else
						{
							Util::ShowApplicationConnectionError(_T("Unable to use the provided application"), testResult.second, parentFrame);
						}
					}
				}
				else
				{
					DeleteNewlyCreatedApp(dlg, session, parentFrame);
					done = true;
				}
			}
		}

		if (resetSignedIn)
			Sapio365Session::SetSignedIn(session, parentFrame);
	}
}

void MainFrame::LicenseChanged()
{
	refreshFrameTitle();
	HTMLUpdateAJL();
	ShowTokenMenu(getLicenseManager()->GetCachedLicense().GetFeatureToken().IsAuthorized());
	static wstring lastLicense = getLicenseManager()->GetCachedLicense().GetCodeForDisplay();
	if (lastLicense != getLicenseManager()->GetCachedLicense().GetCodeForDisplay())
		getLicenseManager()->ClearTokenTransactions();
	lastLicense = getLicenseManager()->GetCachedLicense().GetCodeForDisplay();
}

void MainFrame::OpenFile(const wstring& p_FilePath)
{
	auto ext = FileUtil::FileGetExtension(p_FilePath).substr(1);
	if (O365GridSnapshot::g_FileExtensionPhoto == ext || O365GridSnapshot::g_FileExtensionRestore == ext)
		O365GridSnapshot::Load(p_FilePath);
}

void MainFrame::OnEditAndLoadUltraAdmin()
{
	EditAndLoadUltraAdmin(nullptr);
}

void MainFrame::OnLoadPhotoSnapshot()
{
	O365GridSnapshot::Load(GridSnapshot::Options::Mode::Photo);
}

void MainFrame::OnLoadRestoreSnapshot()
{
	if (Office365AdminApp::CheckAndWarn<LicenseTag::LoadRestorePoint>(this))
		O365GridSnapshot::Load(GridSnapshot::Options::Mode::RestorePoint);
}

void MainFrame::DeleteNewlyCreatedApp(DlgSpecialExtAdminAccess& dlg, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent)
{
	// Ask to keep new or old app
	if (!dlg.GetCreatedAppId().empty())
	{
		YCodeJockMessageBox dlgMsg(p_Parent, DlgMessageBox::eIcon_Information, _T("sapio365"), _T("Do you want to delete the newly created application in Azure AD?"), _YTEXT(""), { {IDOK, _T("Delete")}, {IDCANCEL, _T("Ignore")} });
		if (dlgMsg.DoModal() == IDOK)
		{
			Application app;
			DlgLoading::RunModal(_T("Querying list of applications..."), p_Parent, [&app, p_Session, &dlg](const std::shared_ptr<DlgLoading>& p_DlgLoading) {
				app = ApplicationListRequester::GetAppFromAppId(p_Session, dlg.GetCreatedAppId());
			});
			ASSERT(app.m_Id && app.m_AppId);
			if (app.m_Id && app.m_AppId)
			{
				bool success = true;
				DlgLoading::RunModal(_T("Deleting application..."), p_Parent, [app, p_Session, &success](const std::shared_ptr<DlgLoading>& p_DlgLoading) {
					DeleteAppRequester requester(*app.m_Id);
					requester.Send(p_Session, YtriaTaskData()).GetTask().get();
					int status = requester.GetResult().GetResult().Response.status_code();
					success = status >= 200 && status < 300;
				});
				if (!success)
				{
					YCodeJockMessageBox dlgMsg(p_Parent, DlgMessageBox::eIcon_ExclamationWarning, _T("sapio365"), _YFORMAT(L"Unable to delete application with id \"%s\"", app.m_AppId->c_str()), _YTEXT(""), { {IDOK, _T("Ok")} });
					dlgMsg.DoModal();
				}
			}
		}
	}
}

void MainFrame::OnUpdateNewStandardSession(CCmdUI* pCmdUI)
{
	auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
    pCmdUI->Enable(!IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning());
}

void MainFrame::OnUpdateNewDelegatedAdminSession(CCmdUI * pCmdUI)
{
    pCmdUI->Enable(!IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning());
}

void MainFrame::OnUpdateConnectToCustomer(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(HasSession()
		&& m_HasContracts
		&& !IsSyncInProgresss()
		&& !TaskDataManager::Get().HasTaskRunning()
		&& !AutomatedApp::IsAutomationRunning());
}

void MainFrame::OnUpdateNewUltraAdminSession(CCmdUI * pCmdUI)
{
    pCmdUI->Enable(!IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning());
}

void MainFrame::OnUpdateEditOnPremSettings(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning() && HasSession() && GetSapio365Session()->GetGraphCache().GetHybridCheckSuccess());
}

void MainFrame::OnUpdateViewSessions(CCmdUI * pCmdUI)
{
    pCmdUI->Enable(!IsSyncInProgresss());
}

void MainFrame::OnUpdateViewSchools(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(true);
}

void MainFrame::OnUpdateViewEduUsers(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(true);
}

void MainFrame::OnUpdateEditPowerShellADDSCredentials(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(true);
}

void MainFrame::OnUpdateViewApplications(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(true);
}

void MainFrame::OnUpdateViewWebConsole(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(!IsSyncInProgresss() && IsDebugMode());
}

void MainFrame::OnUpdateSelectSession(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning());
}

void MainFrame::OnUpdateLoadSession(CCmdUI * pCmdUI)
{
	pCmdUI->Enable(!IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning());
}

void MainFrame::OnUpdateApplyRole(CCmdUI * pCmdUI)
{
    pCmdUI->Enable(!IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && m_CanOpenWithRole && !AutomatedApp::IsAutomationRunning());
}

void MainFrame::OnUpdateEditUltraAdmin(CCmdUI* pCmdUI)
{
	const auto s = GetSapio365Session();
	pCmdUI->Enable(!IsSyncInProgresss()
		&& !TaskDataManager::Get().HasTaskRunning()
		&& s
		&& (s->IsUltraAdmin() || s->IsAdvanced() || s->IsElevated() || s->IsPartnerAdvanced() || s->IsPartnerElevated())
		&& !AutomatedApp::IsAutomationRunning());
}

void MainFrame::OnUpdateLoadPhotoSnapshot(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning());
}

void MainFrame::OnUpdateLoadRestoreSnapshot(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning());
}

void MainFrame::syncSessionCache()
{
	m_SyncCacheEvent.ResetEvent();

	showStatusBarProgress(true, YtriaTranslate::Do(MainFrame_syncSessionCache_7, _YLOC("Caching basic data...")).c_str());

	m_SyncInProgress = true;
	m_HtmlView->EnableWindow(FALSE);

	auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
	if (nullptr != app)
	{
		YSafeCreateTask([this, app]()
		{
			auto sapioSession = app->GetSapioSessionBeingLoaded();
			ASSERT(sapioSession);

			if (!sapioSession)
			{
				// FIXME: Bad design, bad fix: when DlgSpecialExtAdminAccess is opened to change the password of a connected
				// Ultra Admin session, the 'session being loaded' used to be reset (see DlgBrowserOpener.cpp,45)
				// This shouldn't happen anymore, but this code avoid the crash it occurred again.
				sapioSession = GetSapio365Session();
				ASSERT(sapioSession && sapioSession->IsUltraAdmin());
				if (sapioSession && !sapioSession->IsUltraAdmin())
					sapioSession = nullptr;
			}

			if (!sapioSession)
				return;

			// Do not clear objects cache so that existing RBAC info is kept (Made in Sapio365Session::SetUseRoleDelegation)
			// But clear basic info.
			sapioSession->GetGraphCache().ClearOrgAndConnectedUser();

			sapioSession->SetSessionType(app->GetSessionType());// embed saveSessionInfo? - session type is needed in RoleDelegationManager::GetInstance().InitAfterSessionWasSet

			auto updateSessionObjOp = [this, app, sapioSession]()
			{
				if (sapioSession
					&& sapioSession->IsStandard()
					&& sapioSession->IsCompanyAdmin()
					&& SessionIdentifier::g_NoRole >= app->GetSessionBeingLoadedRoleID())
				{
					SessionIdentifier advId;

					if (SessionsSqlEngine::Get().SessionExists({ sapioSession->GetEmailOrAppId(), SessionTypes::g_ElevatedSession, 0 }))
						advId = { sapioSession->GetEmailOrAppId(), SessionTypes::g_ElevatedSession, 0 };
					else if (SessionsSqlEngine::Get().SessionExists({ sapioSession->GetEmailOrAppId(), SessionTypes::g_AdvancedSession, 0 }))
						advId = { sapioSession->GetEmailOrAppId(), SessionTypes::g_AdvancedSession, 0 };

					if (advId.m_EmailOrAppId.empty())
					{
						auto goAdv = Util::ShowRestrictedAccessDialog(DlgRestrictedAccess::Image::STANDARD_TO_ADVANCED,
							{
								_T("Create an Advanced session for more access?"),
								_T("Your global admin (company administrator) role lets you consent to admin-only permissions used in an Advanced session."),
								_YTEXT(""),
								_YTEXT(""),
								_YTEXT(""),
								_T("Advanced"),
								_T("Skip")
							},
							HideStandardToNewAdvancedReminderSetting(), this);

						if (goAdv)
						{
							app->ResetSapioSessionBeingLoaded();
							app->SetIsCreatingSession(false);
							YCallbackMessage::DoPost([email = sapioSession->GetEmailOrAppId(), app]()
								{
									app->CreateAdvancedSession(boost::none, email);
								}
							);
							return;
						}
					}
					else
					{
						auto goAdv = Util::ShowRestrictedAccessDialog(DlgRestrictedAccess::Image::STANDARD_TO_ADVANCED,
							{
								_T("Switch to an existing Advanced session?"),
								_T("You get more access using your available Advanced session which has already been granted consent."),
								_YTEXT(""),
								_YTEXT(""),
								_YTEXT(""),
								_T("Switch"),
								_T("Skip")
							},
							HideStandardToExistingAdvancedReminderSetting(), this);

						if (goAdv)
						{
							app->ResetSapioSessionBeingLoaded();
							app->SetIsCreatingSession(false);
							YCallbackMessage::DoPost([advId]()
								{
									auto loader = SessionLoaderFactory(advId).CreateLoader();
									SessionSwitcher(std::move(loader)).Switch();
								}
							);
							return;
						}
					}
				}

				bool error = false;

				InitAfterSessionOrLicenseWasSet::Do(sapioSession, this);

				PersistentSession session;

				if (!app->IsSessionBeingLoadedUltraAdmin())
				{
					const bool isPartner = SessionTypes::g_PartnerAdvancedSession == app->GetSessionBeingLoadedSessionType()
										|| SessionTypes::g_PartnerElevatedSession == app->GetSessionBeingLoadedSessionType();

                    const auto& connectedUser	= !isPartner ? sapioSession->GetGraphCache().GetCachedConnectedUser() : boost::none;
                    const auto& profilePicture	= !isPartner ? sapioSession->GetGraphCache().GetCachedProfilePicture() : boost::none;
                    const auto& org				= sapioSession->GetGraphCache().GetCachedOrganization();

					// If we have a valid connected user
					if (isPartner || boost::none != connectedUser && boost::none != connectedUser->GetUserPrincipalName())
					{
						int64_t roleID = SessionIdentifier::g_NoRole;
						int64_t roleIdToDelete = SessionIdentifier::g_NoRole;
						bool hideUnscoped = false;
						if (!isPartner)
						{
							roleID = app->GetSessionBeingLoadedRoleID();
							bool roleIsForced = false;
							hideUnscoped = sapioSession->IsRBACHideUnscopedObjects();

							vector<RoleDelegation> rolesAvailable;
							RoleDelegationManager::GetInstance().GetRoleDelegationsForThisUser(connectedUser->GetID(), rolesAvailable, sapioSession);

							if (roleID <= SessionIdentifier::g_NoRole)
							{
								for (const auto& ra : rolesAvailable)
								{
									if (ra.GetDelegation().IsEnforceRole())
									{
										ASSERT(ra.GetDelegation().m_ID);
										if (ra.GetDelegation().m_ID)
										{
											TraceIntoFile::trace(_YDUMP("MainFrame"), _YDUMP("syncSessionCache"), _YDUMPFORMAT(L"Session: %s - found enforced role: %s", sapioSession->GetEmailOrAppId().c_str(), ra.GetDelegation().m_Name.c_str()).c_str());
											if (sapioSession->GetSessionType() == SessionTypes::g_StandardSession)
											{
												roleID = ra.GetDelegation().m_ID.get();
												roleIsForced = true;
												break;
											}
											else if (!Sapio365Session::IsAdminRBAC(sapioSession))
											{
												TraceIntoFile::trace(_YDUMP("MainFrame"), _YDUMP("syncSessionCache"), _YDUMPFORMAT(L"Session: %s - cannot login as admin because of mandatory role: %s", sapioSession->GetEmailOrAppId().c_str(), ra.GetDelegation().m_Name.c_str()).c_str());
												YCodeJockMessageBox msgBox(this, DlgMessageBox::eIcon::eIcon_ExclamationWarning,
													_YFORMAT(L"Please launch a Standard session."),
													_YFORMAT(L"Unfortunately, you may not use an Advanced session because you have been assigned a role '%s' which has been enforced by your admin.", ra.GetDelegation().m_Name.c_str()),
													_YTEXT(""),
													{ { IDOK, _T("Ok") } });
												msgBox.DoModal();
												app->ResetSapioSessionBeingLoaded();
												app->SetIsCreatingSession(false);
												return;
											}
										}
									}
								}
							}
							else
							{
								for (const auto& ra : rolesAvailable)
								{
									if (ra.GetDelegation().m_ID && roleID == *ra.GetDelegation().m_ID)
									{
										roleIsForced = ra.GetDelegation().IsEnforceRole();
										break;
									}
								}
							}

							// Bug #190312.SR.009B36: Restrict role usage to standard sessions only.
							if (sapioSession->GetSessionType() != SessionTypes::g_StandardSession && sapioSession->GetSessionType() != SessionTypes::g_Role)
							{
								roleID = SessionIdentifier::g_NoRole;
							}
							else
							{
								if (roleID > SessionIdentifier::g_NoRole && !RoleDelegationManager::GetInstance().GetDelegation(roleID, sapioSession).IsValid())
								{
									roleIdToDelete = roleID;

									YCodeJockMessageBox msgBox(this,
										DlgMessageBox::eIcon::eIcon_ExclamationWarning,
										_T("Role not found"),
										YtriaTranslate::Do(MainFrame_syncSessionCache_18, _YLOC("It seems this role no longer exists.\nPlease try again.")).c_str(),
										_YTEXT(""),
										{ { IDYES, YtriaTranslate::Do(consoleEZApp_InitInstanceSpecific_1, _YLOC("Continue")).c_str() } });
									msgBox.DoModal();

									roleID = SessionIdentifier::g_NoRole;
									sapioSession->SetUseRoleDelegation(roleID, nullptr);
									return;
								}
							}

							if (roleID > SessionIdentifier::g_NoRole)
							{
								if (!sapioSession->SetUseRoleDelegation(roleID, this))
								{
									if (roleIsForced)
									{
										// Should we display a message box?
										// The user already had the Background Sessions error. Merge them?
										app->ResetSapioSessionBeingLoaded();
										app->SetIsCreatingSession(false);
										return;
									}

									// Invalid role: Abort session loading.
									roleID = SessionIdentifier::g_NoRole;
									sapioSession->SetUseRoleDelegation(roleID, nullptr);
									return;
								}
								else
								{
									sapioSession->SetRBACHideUnscopedObjects(hideUnscoped);
									app->SetSessionType(SessionTypes::g_Role);
									sapioSession->SetSessionType(SessionTypes::g_Role);
								}
							}
						}

                        YTimeDate now = YTimeDate::GetCurrentTimeDate();

                        const wstring userPrincipalName = isPartner
							? sapioSession->GetEmailOrAppId()
							: connectedUser->GetUserPrincipalName().get();
						ASSERT(!userPrincipalName.empty());
						if (!SessionsSqlEngine::Get().SessionExists({ userPrincipalName, app->GetSessionType(), roleID }))
						{
							if (roleID > SessionIdentifier::g_NoRole)
							{
								session.SetRoleId(roleID);
								session.SetRoleNameInDb(RoleDelegationManager::GetInstance().GetDelegation(roleID, sapioSession).m_Name);
								session.SetRoleShowOwnScopeOnly(hideUnscoped);
							}
						    session.SetAccessCount(1);
                            session.SetCreatedOn(now);
						}
						else
						{
							session = SessionsSqlEngine::Get().Load({ userPrincipalName, app->GetSessionType(), roleID });
							ASSERT(session.IsValid());
							if (session.IsValid())
								session.SetAccessCount(session.GetAccessCount() + 1);
						}

						QueryGetCredentialsToShare queryCreds({ userPrincipalName, app->GetSessionType(), roleID });
						queryCreds.Run();
						if (session.GetSessionType() == SessionTypes::g_StandardSession || session.GetSessionType() == SessionTypes::g_Role)
							session.SetCredentialsId(queryCreds.GetCredentialsId());

						if (SessionIdentifier::g_NoRole != roleIdToDelete)
							SessionsSqlEngine::Get().DeleteCredentials(session.GetIdentifier());

						if (!isPartner)
							session.SetConnectedUserId(connectedUser->m_Id);
                        session.SetTechnicalName(userPrincipalName);
						if (!isPartner && !session.IsElevated())
							session.SetSessionName(connectedUser->GetUserPrincipalName().get());
						session.SetLastUsedOn(now);
						if (!isPartner) 
							session.SetFullname(connectedUser->GetDisplayName() ? *connectedUser->GetDisplayName() : _YTEXT(""));
                        ASSERT(org);
                        if (org)
                            session.SetTenant(*org);
                        ASSERT(app->GetSessionType() == SessionTypes::g_StandardSession
							|| app->GetSessionType() == SessionTypes::g_AdvancedSession 
							|| app->GetSessionType() == SessionTypes::g_Role
							|| app->GetSessionType() == SessionTypes::g_ElevatedSession
							|| app->GetSessionType() == SessionTypes::g_PartnerAdvancedSession
							|| app->GetSessionType() == SessionTypes::g_PartnerElevatedSession);
                        session.SetSessionType(app->GetSessionType());

						if (boost::none != profilePicture)
							session.SetProfilePhoto(*profilePicture);

						if (!app->GetIsCreatingSession() || !IsOverwritingExistingElevated(sapioSession->GetEmailOrAppId(), sapioSession->GetSessionType()).first)
							SessionsSqlEngine::Get().Save(session);
						OAuth2AuthenticatorBase* auth = dynamic_cast<OAuth2AuthenticatorBase*>(sapioSession->GetMainMSGraphSession()->GetAuthenticator().get());
						ASSERT(nullptr != auth);
						if (nullptr != auth)
						{
							sapioSession->GetMainMSGraphSession()->SetSessionPersister(std::make_shared<DbSessionPersister>(session));
							sapioSession->GetMainMSGraphSession()->GetSessionPersister()->Save(auth->GetOAuth2Config());
						}

						sapioSession->SetEmailOrAppId(session.GetEmailOrAppId());

						YCallbackMessage::DoSend([session, sapioSession, app, this]
						{
							auto newSession = sapioSession;
							if (app->GetIsCreatingSession())
							{
								auto sibling = Sapio365Session::Find(newSession->GetIdentifier());
								if (sibling && sibling != newSession)
								{
									LoggerService::Debug(_YTEXT("MainFrame::syncSessionCache: Found a session with the same Identifier: reusing."));
									sibling->SwapSessionsWith(newSession);
									newSession = sibling;
								}
							}

							if (newSession && Sapio365Session::CheckCompatibilityWithActiveRBAC(newSession->GetTenantName(true), this, {}))
							{
								Sapio365Session::SetSignedIn(newSession, this);
							}
							else
							{
								// If new session refused due to RBAC incompatibility, it can't be an already loaded session...
								ASSERT(!newSession || newSession == sapioSession);
							}

							{
								if (app->GetIsCreatingSession())
									TraceIntoFile::trace(_YDUMP("MainFrame"), _YDUMP("syncSessionCache"), _YDUMPFORMAT(L"New session: Successfully synced %s session: %s (role %s)", sapioSession->GetSessionType().c_str(), sapioSession->GetEmailOrAppId().c_str(), Str::getStringFromNumber(sapioSession->GetRoleDelegationID()).c_str()));
								else
									TraceIntoFile::trace(_YDUMP("MainFrame"), _YDUMP("syncSessionCache"), _YDUMPFORMAT(L"Load session: Successfully synced %s session: %s (role %s)", sapioSession->GetSessionType().c_str(), sapioSession->GetEmailOrAppId().c_str(), Str::getStringFromNumber(sapioSession->GetRoleDelegationID()).c_str()));

								// FIXME: We should use the logger service.
								// We need to rework logger channels to allow trace-only logging.
								//if (app->GetIsCreatingSession())
								//	LoggerService::Debug(YtriaTranslate::Do(MainFrame_syncSessionCache_20, _YLOC("MainFrame::syncSessionCache: New session: Successfully synced %1 session: %2 (role %3)"), sapioSession->GetSessionType().c_str(), sapioSession->GetTechnicalSessionName().c_str(), Str::getStringFromNumber(sapioSession->GetRoleDelegationID()).c_str()));
								//else
								//	LoggerService::Debug(YtriaTranslate::Do(MainFrame_syncSessionCache_21, _YLOC("MainFrame::syncSessionCache: Load session: Successfully synced %1 session: %2 (role %3)"), sapioSession->GetSessionType().c_str(), sapioSession->GetTechnicalSessionName().c_str(), Str::getStringFromNumber(sapioSession->GetRoleDelegationID()).c_str()));
							}

							app->ResetSapioSessionBeingLoaded();
						});
					}
					else
					{
						error = true;

						YCallbackMessage::DoSend([sapioSession, app, this]
						{
							if (GetSapio365Session())
								Sapio365Session::SetSignedIn(GetSapio365Session(), this); // Reset to update list.
							app->ResetSapioSessionBeingLoaded();

							{
								if (app->GetIsCreatingSession())
									TraceIntoFile::trace(_YDUMP("MainFrame"), _YDUMP("syncSessionCache"), _YDUMPFORMAT(L"New session: An error occurred while synchronizing %s session %s: %s", sapioSession->GetSessionType().c_str(), sapioSession->GetEmailOrAppId().c_str(), sapioSession->GetGraphCache().GetSyncError().c_str()));
								else
									TraceIntoFile::trace(_YDUMP("MainFrame"), _YDUMP("syncSessionCache"), _YDUMPFORMAT(L"Load session: An error occurred while synchronizing %s session %s (role %s): %s", sapioSession->GetSessionType().c_str(), sapioSession->GetEmailOrAppId().c_str(), sapioSession->GetGraphCache().GetSyncError().c_str(), Str::getStringFromNumber(sapioSession->GetRoleDelegationID()).c_str()));

								// FIXME: We should use the logger service.
								// We need to rework logger channels to allow trace-only logging.
								//if (app->GetIsCreatingSession())
								//	LoggerService::Debug(YtriaTranslate::Do(MainFrame_syncSessionCache_22, _YLOC("New session: An error occurred while synchronizing %1 session %2: %3"), sapioSession->GetSessionType().c_str(), sapioSession->GetTechnicalSessionName().c_str(), sapioSession->GetGraphCache().GetSyncError().c_str()));
								//else
								//	LoggerService::Debug(YtriaTranslate::Do(MainFrame_syncSessionCache_23, _YLOC("Load session: An error occurred while synchronizing %1 session %2: %3"), sapioSession->GetSessionType().c_str(), sapioSession->GetTechnicalSessionName().c_str(), sapioSession->GetGraphCache().GetSyncError().c_str()));
							}

							YCodeJockMessageBox msgBox(this,
								DlgMessageBox::eIcon::eIcon_Error,
								YtriaTranslate::Do(MainFrame_syncSessionCache_1, _YLOC("Error")).c_str(),
								YtriaTranslate::Do(MainFrame_syncSessionCache_2, _YLOC("An error occurred while synchronizing.")).c_str(),
								sapioSession->GetGraphCache().GetSyncError(),
								{ { IDOK, YtriaTranslate::Do(MainFrame_syncSessionCache_3, _YLOC("OK")).c_str() } });
							msgBox.DoModal();

						});
					}
				}
				else // Ultra Admin session
				{
                	const auto& org = sapioSession->GetGraphCache().GetCachedOrganization();

					// 4/4/2019: Even if we don't have a valid connected Organization (meaning unable to retrieve data, probably HTTP 403 due to missing admin consent)
					// We store the new Ultra Admin session. So that the user can easily retry to connect later.
					// The reason is, after investigating Bug #190403.RO.009C92, it seems admin consent is not effective right away.
					{
						const int accessCountInc = org ? 1 : 0;
						if (!SessionsSqlEngine::Get().SessionExists({ app->GetFullAdminAppId(), SessionTypes::g_UltraAdmin, 0 }))
						{
							session.SetAccessCount(accessCountInc);
						}
						else
						{
							SessionIdentifier identifier(app->GetFullAdminAppId(), SessionTypes::g_UltraAdmin, 0);

							auto loadedSession = SessionsSqlEngine::Get().Load(identifier);
							ASSERT(loadedSession.IsValid());
							if (loadedSession.IsValid())
							{
								session = loadedSession;
								session.SetAccessCount(session.GetAccessCount() + accessCountInc);
							}
						}
						session.SetTechnicalName(app->GetFullAdminAppId());
						session.SetSessionName(app->GetFullAdminDisplayName());
						session.SetLastUsedOn(YTimeDate::GetCurrentTimeDate());
						if (org)
							session.SetTenant(*org);
						ASSERT(app->GetSessionType() == SessionTypes::g_UltraAdmin);
						session.SetSessionType(app->GetSessionType());

						const auto appId = app->GetFullAdminAppId();
						ASSERT(!appId.empty());
						session.SetAppId(appId);
						if (!app->GetFullAdminAppPassword().empty())
							session.SetAppClientSecret(app->GetFullAdminAppPassword());
						if (!app->GetFullAdminTenant().empty())
							session.SetTenantName(app->GetFullAdminTenant());

						SessionsSqlEngine::Get().Save(session);
						if (org)
							sapioSession->SetEmailOrAppId(session.GetEmailOrAppId());

						OAuth2AuthenticatorBase* auth = dynamic_cast<OAuth2AuthenticatorBase*>(sapioSession->GetMainMSGraphSession()->GetAuthenticator().get());
						ASSERT(nullptr != auth);
						if (nullptr != auth)
						{
							sapioSession->GetMainMSGraphSession()->SetSessionPersister(std::make_shared<DbSessionPersister>(session));
							sapioSession->GetMainMSGraphSession()->GetSessionPersister()->Save(auth->GetOAuth2Config());
						}

						// Password has been saved, reset it in app (see "wstring appClientSecret = GetFullAdminAppPassword();" in Office365AdminApp)
						app->SetFullAdminAppPassword(_YTEXT(""));

						if (org)
						{
							// Connection successful

							YCallbackMessage::DoSend([session, sapioSession, app, appId = app->GetFullAdminAppId(), this]
							{
								auto newSession = sapioSession;
								if (app->GetIsCreatingSession())
								{
									auto sibling = Sapio365Session::Find(newSession->GetIdentifier());
									if (sibling && sibling != newSession)
									{
										LoggerService::Debug(_YTEXT("MainFrame::syncSessionCache: Found a session with the same Identifier: reusing."));
										newSession = sibling;
									}
								}

								if (newSession && Sapio365Session::CheckCompatibilityWithActiveRBAC(newSession->GetTenantName(true), this, {}))
								{
									Sapio365Session::SetSignedIn(newSession, this);
								}
								else
								{
									// If new session refused due to RBAC incompatibility, it can't be an already loaded session...
									ASSERT(!newSession || newSession == sapioSession);
								}

								{
									if (app->GetIsCreatingSession())
										TraceIntoFile::trace(_YDUMP("MainFrame"), _YDUMP("syncSessionCache"), _YDUMPFORMAT(L"New session: Successfully synced %s session: %s %s", sapioSession->GetSessionType().c_str(), sapioSession->GetEmailOrAppId().c_str(), appId.c_str()));
									else
										TraceIntoFile::trace(_YDUMP("MainFrame"), _YDUMP("syncSessionCache"), _YDUMPFORMAT(L"Load session: Successfully synced %s session: %s %s", sapioSession->GetSessionType().c_str(), sapioSession->GetEmailOrAppId().c_str(), appId.c_str()));

									// FIXME: We should use the logger service.
									// We need to rework logger channels to allow trace-only logging.
									//if (app->GetIsCreatingSession())
									//	LoggerService::Debug(YtriaTranslate::Do(MainFrame_syncSessionCache_24, _YLOC("MainFrame::syncSessionCache: New session: Successfully synced %1 session: %2 %3"), sapioSession->GetSessionType().c_str(), sapioSession->GetTechnicalSessionName().c_str(), sapioSession->GetAppId().c_str()));
									//else
									//	LoggerService::Debug(YtriaTranslate::Do(MainFrame_syncSessionCache_25, _YLOC("MainFrame::syncSessionCache: Load session: Successfully synced %1 session: %2 %3"), sapioSession->GetSessionType().c_str(), sapioSession->GetTechnicalSessionName().c_str(), sapioSession->GetAppId().c_str()));
								}

								app->ResetSapioSessionBeingLoaded();
							});
						}
						else
						{
							// Connection failed (probably admin consent issue)

							error = true;
							YCallbackMessage::DoSend([sapioSession, app, appId = app->GetFullAdminAppId(), this]
							{
								if (GetSapio365Session())
									Sapio365Session::SetSignedIn(GetSapio365Session(), this); // Reset to update list.
								app->ResetSapioSessionBeingLoaded();

								{
									if (app->GetIsCreatingSession())
										TraceIntoFile::trace(_YDUMP("MainFrame"), _YDUMP("syncSessionCache"), _YDUMPFORMAT(L"New session: An error occurred while synchronizing %s session %s (%s) (It may be an Admin Consent issue): %s", sapioSession->GetSessionType().c_str(), sapioSession->GetEmailOrAppId().c_str(), appId.c_str(), sapioSession->GetGraphCache().GetSyncError().c_str()));
									else
										TraceIntoFile::trace(_YDUMP("MainFrame"), _YDUMP("syncSessionCache"), _YDUMPFORMAT(L"Load session: An error occurred while synchronizing %s session %s (%s) (It may be an Admin Consent issue): %s", sapioSession->GetSessionType().c_str(), sapioSession->GetEmailOrAppId().c_str(), appId.c_str(), sapioSession->GetGraphCache().GetSyncError().c_str()));

									// FIXME: We should use the logger service.
									// We need to rework logger channels to allow trace-only logging.
									//if (app->GetIsCreatingSession())
									//	LoggerService::Debug(YtriaTranslate::Do(MainFrame_syncSessionCache_26, _YLOC("New session: An error occurred while synchronizing %1 session %2 (%3) (It may be an Admin Consent issue): %4"), sapioSession->GetSessionType().c_str(), sapioSession->GetTechnicalSessionName().c_str(), appId.c_str(), sapioSession->GetGraphCache().GetSyncError().c_str()));
									//else
									//	LoggerService::Debug(YtriaTranslate::Do(MainFrame_syncSessionCache_27, _YLOC("Load session: An error occurred while synchronizing %1 session %2 (%3) (It may be an Admin Consent issue): %4"), sapioSession->GetSessionType().c_str(), sapioSession->GetTechnicalSessionName().c_str(), appId.c_str(), sapioSession->GetGraphCache().GetSyncError().c_str()));
								}

								YCodeJockMessageBox msgBox(this, DlgMessageBox::eIcon::eIcon_Error,
																YtriaTranslate::Do(MainFrame_syncSessionCache_4, _YLOC("Error")).c_str(),
																YtriaTranslate::Do(MainFrame_syncSessionCache_5, _YLOC("An error occurred while synchronizing. It may be an Admin Consent issue.")).c_str(),
																sapioSession->GetGraphCache().GetSyncError(),
																{ { IDOK, YtriaTranslate::Do(MainFrame_syncSessionCache_6, _YLOC("OK")).c_str() } });
								msgBox.DoModal();
							});
						}
					}
				}
			};

            bool syncSuccess = false;
			wstring error;
			try
			{
				sapioSession->GetGraphCache().Sync();
                syncSuccess = true;
			}
			catch (const RestException& e)
			{
				auto handler = sapioSession->GetMainMSGraphSession()->GetErrorHandler();
				ASSERT(handler);
				if (handler)
					error = sapioSession->GetMainMSGraphSession()->GetErrorHandler()->HandleRestException(e, nullptr);
			}
			catch (const web::http::http_exception& e)
			{
				auto handler = sapioSession->GetMainMSGraphSession()->GetErrorHandler();
				ASSERT(handler);
				if (handler)
					error = sapioSession->GetMainMSGraphSession()->GetErrorHandler()->HandleHttpException(e, nullptr);
			}
			catch (const web::http::oauth2::experimental::oauth2_exception& e)
			{
				auto handler = sapioSession->GetMainMSGraphSession()->GetErrorHandler();
				ASSERT(handler);
				if (handler)
					sapioSession->GetMainMSGraphSession()->GetErrorHandler()->HandleOAuth2Exception(e, YtriaTaskData());

				LoggerService::Debug(_YTEXT("MainFrame::syncSessionCache: Refresh token expired. Authentication needed."));

				auto authenticator = dynamic_cast<OAuth2AuthenticatorBase*>(sapioSession->GetMainMSGraphSession()->GetAuthenticator().get());
				ASSERT(nullptr != authenticator);
                if (nullptr != authenticator)
					authenticator->SetPreferredLogin(authenticator->GetLastLoadedLogin());

				sapioSession->GetMainMSGraphSession()->Start(this).Then([this, updateSessionObjOp](const RestAuthenticationResult& p_Result)
				{
					if (p_Result.state == RestAuthenticationResult::State::Success)
					{
						LoggerService::Debug(_YTEXT("MainFrame::syncSessionCache: Retrying after expired refresh token."));
						YCallbackMessage::DoPost(updateSessionObjOp);
					}
				});
			}
			catch (const std::exception& e)
			{
				auto handler = sapioSession->GetMainMSGraphSession()->GetErrorHandler();
				ASSERT(handler);
				if (handler)
					error = sapioSession->GetMainMSGraphSession()->GetErrorHandler()->HandleStdException(e, nullptr);
			}
			catch (...)
			{
				auto handler = sapioSession->GetMainMSGraphSession()->GetErrorHandler();
				ASSERT(handler);
				if (handler)
					error = sapioSession->GetMainMSGraphSession()->GetErrorHandler()->HandleUnknownException(nullptr);
			}

			if (!syncSuccess)
			{
				sapioSession->GetGraphCache().SetSyncError(error);
			}
			else
			{
				LoggerService::Debug(_YTEXT("MainFrame::syncSessionCache: Getting SKUs"));
				auto requester = std::make_shared<SubscribedSkusRequester>(std::make_shared<BasicRequestLogger>(_YTEXT("subscribed skus")));

				// Role or Elevated sessions are not fully created here, use the main MsGraphSession of the Sapio365Session
				requester->SetForceUseMainMsGraphSession(true);

				bool error = false;
				try
				{
					requester->Send(sapioSession, YtriaTaskData()).GetTask().wait();
				}
				catch (const RestException& e)
				{
					LoggerService::Debug(_YTEXTFORMAT(L"MainFrame::syncSessionCache: Error getting SKUs -- %s", e.WhatUnicode().c_str()));
					error = true;
				}
				catch (const std::exception& e)
				{
					LoggerService::Debug(_YTEXTFORMAT(L"MainFrame::syncSessionCache: Error getting SKUs -- %s", Str::convertFromUTF8(e.what()).c_str()));
					error = true;
				}

				if (!error)
					SkuAndPlanReference::GetInstance().Update(requester->GetData());
			}

			if (!sapioSession->IsUltraAdmin() && !sapioSession->IsPartnerAdvanced() && !sapioSession->IsPartnerElevated())
			{
				const auto& connectedUser = sapioSession->GetGraphCache().GetCachedConnectedUser();
				ASSERT(connectedUser && connectedUser->GetUserPrincipalName());
				if (connectedUser && connectedUser->GetUserPrincipalName())
					sapioSession->SetEmailOrAppId(*connectedUser->GetUserPrincipalName());
			}
			if (!app->GetIsCreatingSession() || !IsOverwritingExistingElevated(sapioSession->GetEmailOrAppId(), sapioSession->GetSessionType()).first)
				YCallbackMessage::DoSend(updateSessionObjOp);
		}
		).ThenByTask([this](pplx::task<void> p_PreviousTask) 
		{
            try
            {
                p_PreviousTask.wait();
				m_SyncCacheEvent.SetEvent();
                YDataCallbackMessage<MainFrame*>::DoPost(this, [](MainFrame* mainFrame)
                {
					Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
					ASSERT(app);
					if (app)
					{
						std::pair<bool, PersistentSession> hasConflict;
						{
							auto sessionBeingLoaded = app->GetSapioSessionBeingLoaded();
							if (sessionBeingLoaded && app->GetIsCreatingSession())
								hasConflict = MainFrame::IsOverwritingExistingElevated(sessionBeingLoaded->GetEmailOrAppId(), sessionBeingLoaded->GetSessionType());
						}

						if (!hasConflict.first)
						{
							mainFrame->postSyncCache();
						}
						else
						{
							mainFrame->postSyncCache(false);

							SessionIdentifier sessionId = { hasConflict.second.GetEmailOrAppId(), SessionTypes::g_ElevatedSession, 0 };
							auto loader = std::make_unique<ElevatedSessionLoader>(sessionId);
							SessionSwitcher(std::move(loader)).Switch();
						}
					}
                });
            }
            catch (const std::exception& e)
            {
                std::string ee(e.what());
                ASSERT(false);
            }			
		});
	}
}

void MainFrame::asyncCountUsers(const shared_ptr<O365LicenseValidator> p_Validator, std::function<void(int32_t)> p_Callback)
{
	ASSERT(p_Validator);
	if (p_Validator)
	{
		auto licenseManager = getLicenseManager();
		ASSERT(licenseManager);
		if (licenseManager)
		{
			p_Validator->GetMaxCountForFreeAndTokenRates(licenseManager->LicenseWasLoadedFromServerAtApplicationStart());

			int32_t stopCountAt = [p_Validator, licenseManager] // tenant user limit is now 2.15 billion
			{
				int32_t maxCount = -1;// count all

				auto license = licenseManager->GetLicense(false);
				if (license.IsFeatureSet(LicenseUtil::g_codeSapio365FeatureNoUserCount) || license.IsTrial() || license.UsesTokens())
					maxCount = 0;// count none
				else if (license.IsNone())
					maxCount = p_Validator->GetUserCountForFreeAccess();// count Dracula

				return maxCount;
			}();

			if (stopCountAt != 0)
			{
				YtriaTaskData taskData;
				auto& sapioSession = GetSapio365Session();
				sapioSession->SetAsyncUserCountTaskData(taskData);
				std::thread countingThread([sapioSession, p_Callback, taskData, stopCountAt]()
					{
#if CRASH_RPT && !defined(DEBUG)
						CrThreadAutoInstallHelper scopedHelper;
#endif
						SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_IDLE);
						boost::YOpt<SapioError> error;
						auto count = safeTaskCall(sapioSession->GetMSGraphSession(Sapio365Session::SessionType::USER_SESSION)->CountUsers(std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, sapioSession->GetIdentifier()), taskData, true, stopCountAt), sapioSession->GetMainMSGraphSession(), [&error, sapioSession, taskData](const std::exception_ptr& p_ExceptPtr)
							{
								if (!taskData.IsCanceled())
								{
									ASSERT(p_ExceptPtr);
									if (p_ExceptPtr)
									{
										try
										{
											std::rethrow_exception(p_ExceptPtr);
										}
										catch (const RestException& e)
										{
											error = HttpError(e);
										}
										catch (const std::exception& e)
										{
											error = SapioError(e);
										}
									}
								}
								return 0;
							}, YtriaTaskData()).get();

							if (error)
							{
								// Do something specific? (count is 0)
								ASSERT(false);
							}

							if (taskData.IsCanceled())
								LoggerService::Debug(_YTEXT("MainFrame::asyncCountUsers: Canceled"));
							else
								p_Callback(count);
					});

				if (AutomatedApp::IsAutomationRunning() || sapioSession->IsCommandLineAutomationInProgress())
					countingThread.join();
				else
					countingThread.detach();
			}
		}
	}
}

LicenseManager* MainFrame::getLicenseManager() const
{
#ifndef  _DEBUG
	// Only in Release
	if (nullptr == m_LicenseManager)
	{
#endif
		LicenseManager* licenseManager = nullptr;

		auto app = GetAutomatedApp();
		ASSERT(nullptr != app);
		if (nullptr != app)
			licenseManager = app->GetLicenseManager();

		if (nullptr != m_LicenseManager)
			ASSERT(m_LicenseManager == licenseManager);
		else
			ASSERT(nullptr != licenseManager);

		m_LicenseManager = licenseManager;
#ifndef  _DEBUG
}
#endif
	return m_LicenseManager;
}

const shared_ptr<O365LicenseValidator>& MainFrame::getLicenseValidator() const
{
#ifndef  _DEBUG
	// Only in Release
	if (!m_Validator)
	{
#endif
		LicenseManager* licenseManager = getLicenseManager();
		if (nullptr != licenseManager)
		{
			auto validator = std::dynamic_pointer_cast<O365LicenseValidator>(licenseManager->GetLicenseValidator());
			ASSERT(!m_Validator || m_Validator == validator);
			m_Validator = validator;
		}
#ifndef  _DEBUG
	}
#endif
	
	ASSERT(m_Validator);
	return m_Validator;
}

bool MainFrame::updateLicenseContext()
{
	bool rvComplies = false;

	// license is already validated - cf AutomatedApp::initLicense()

	const auto tenantName = GetSapio365Session()->GetRBACTenantName(true);
	SetLicenseContext(ExtraLicenseConfig::Type::TENANT, tenantName);

	{
		auto context = GetLicenseContext();
		context.SetSessionReferenceForTokenCharging(GetSessionInfo().GetIdentifier().Serialize());
		SetLicenseContext(context);
	}

	auto& licenseValidator = getLicenseValidator();
	if (licenseValidator)
	{
		licenseValidator->SetLoadingUserCount(tenantName);
		asyncCountUsers(licenseValidator, [licenseValidator, tenantName, graphSession = GetSapio365Session(), hWnd = m_hWnd, mainFrame = this](int32_t p_Count)
		{
			ASSERT(::IsWindow(hWnd));
			if (::IsWindow(hWnd) && !graphSession->IsAsyncUserCountTaskCancel())
			{
				YDataCallbackMessage<int32_t>::DoPost(p_Count, [licenseValidator, tenantName, graphSession, mainFrame](int32_t p_Count)
					{
						graphSession->SetAsyncUserCountTaskData(boost::none);
						// As we now take license into account, we can't use this count to init graph cache counter anymore.
						//graphSession->GetGraphCache().InitUserCount(p_Count);
						licenseValidator->SetTenantUserCount(tenantName, p_Count);
						mainFrame->refreshFrameTitle();
					});
			}
		});
	}

	return rvComplies;
}

ILicenseValidator::ErrorStatus MainFrame::GetLicenseValidatorError(wstring& p_ErrMsg) const
{
	ILicenseValidator::ErrorStatus rvStatus = ILicenseValidator::ErrorStatus::STATUSOK;
	p_ErrMsg.clear();

	auto& licenseValidator = getLicenseValidator();
	if (licenseValidator)
	{
		rvStatus = licenseValidator->GetErrorStatus();
		p_ErrMsg = licenseValidator->GetErrorMessage();
	}

	return rvStatus;
}

bool MainFrame::IgnoreLicenseError() const
{
	auto& licenseValidator = getLicenseValidator();
	if (licenseValidator)
		return licenseValidator->IsCancelConfig();

	return false;
}

LicenseManager::ProcessLicenseContextResult MainFrame::ProcessLicenseContext(const LicenseContext& p_LC)
{
	// charge tokens according to usage on relevant licenses
	LicenseManager* licenseManager = getLicenseManager();
	ASSERT(nullptr != licenseManager);
	if (nullptr != licenseManager)
		return licenseManager->ProcessLicenseContext(p_LC);
	return LicenseManager::ProcessLicenseContextResult();
}

void MainFrame::SendTrace(const vector<wstring>& p_AdditionnalFiles, CWnd* p_ParentForDialogs)
{
	AutomatedApp* o365App = GetAutomatedApp();
	ASSERT(nullptr != o365App);
	if (nullptr != o365App)
		o365App->SendTrace(p_AdditionnalFiles, p_ParentForDialogs);
}

void MainFrame::ToggleDumpMode(CWnd* p_ParentForDialogs, bool p_SaveDisableToRegistry)
{
	if (Sapio365Session::IsDumpGloballyEnabled())
	{
		/*auto files = */Sapio365Session::SetDumpGloballyDisabled(p_SaveDisableToRegistry);

		// List all files in the Dump folder in case of previous crash.
		auto files = FileUtil::ListFiles(Sapio365Session::GetDumpPath(), _YTEXT("log"));

		if (files.empty())
		{
			if (p_SaveDisableToRegistry)
			{
				YCodeJockMessageBox msgBox(p_ParentForDialogs, DlgMessageBox::eIcon_Information, YtriaTranslate::Do(MainFrame_ToggleDumpMode_14, _YLOC("Debug mode has been disabled.")).c_str()
					, _YTEXT("")
					, _YTEXT("")
					, { { IDOK, YtriaTranslate::Do(MainFrame_ToggleDumpMode_13, _YLOC("OK")).c_str() } });
				msgBox.DoModal();
			}
		}
		else
		{
			wstring questionMessage = TraceIntoFile::IsEnabled()	? YtriaTranslate::Do(MainFrame_ToggleDumpMode_1, _YLOC("Would you like to send the resulting debug files along with your trace files now?")).c_str()
																	: YtriaTranslate::Do(MainFrame_ToggleDumpMode_2, _YLOC("Would you like to send the resulting debug files now?")).c_str();

			YCodeJockMessageBox msgBox(p_ParentForDialogs, DlgMessageBox::eIcon_Question
				, p_SaveDisableToRegistry ? YtriaTranslate::Do(MainFrame_ToggleDumpMode_3, _YLOC("Debug mode has been disabled.")).c_str() : YtriaTranslate::Do(MainFrame_ToggleDumpMode_15, _YLOC("Debug mode")).c_str()
				, questionMessage
				, YtriaTranslate::Do(MainFrame_ToggleDumpMode_4, _YLOC("[Note: Either choice will also delete these temporary files from your drive.]")).c_str()
				, { { IDOK, YtriaTranslate::Do(MainFrame_ToggleDumpMode_5, _YLOC("YES")).c_str() },{ IDCANCEL, YtriaTranslate::Do(MainFrame_ToggleDumpMode_6, _YLOC("NO")).c_str() },{ IDRETRY, YtriaTranslate::Do(MainFrame_ToggleDumpMode_12, _YLOC("Show me those files")).c_str() } });

			auto res = msgBox.DoModal();
			while (IDRETRY == res)
			{
				// Reveal files
				FileUtil::OpenFolderAndSelectFile(files[0]);
				res = msgBox.DoModal();
			}

			if (IDOK == res)
				SendTrace(files, p_ParentForDialogs);

			FileUtil::DeleteFiles(files, false, false);
			ASSERT(FileUtil::ListFiles(Sapio365Session::GetDumpPath(), _YTEXT("log")).empty());
		}
	}
	else
	{
		YCodeJockMessageBox msgBox(p_ParentForDialogs, DlgMessageBox::eIcon_ExclamationWarning, YtriaTranslate::Do(MainFrame_ToggleDumpMode_7, _YLOC("Are you sure you want to enable Debug mode?")).c_str()
			, YtriaTranslate::Do(MainFrame_ToggleDumpMode_8, _YLOC("This option is only recommended if asked to do so by Ytria support.\n\nIn regular use, this mode will slow down all communication with Microsoft servers, and the results of all Graph API requests will be dumped to log files - which can take up space unnecessarily.")).c_str()
			, YtriaTranslate::Do(MainFrame_ToggleDumpMode_9, _YLOC("Note:\n-These files are only temporary and will be deleted once you choose Send / Do Not Send\n-To disable this option, you must re-click the Debug mode button. Simply closing sapio365 will not disable this option.")).c_str()
			, { { IDOK, YtriaTranslate::Do(MainFrame_ToggleDumpMode_10, _YLOC("Enable")).c_str() },{ IDCANCEL, YtriaTranslate::Do(MainFrame_ToggleDumpMode_11, _YLOC("Cancel")).c_str() } });
		if (IDOK == msgBox.DoModal())
			Sapio365Session::SetDumpGloballyEnabled();
	}
}

bool MainFrame::ConfigureLicense(const VendorLicense& p_License)
{
	bool rvCfgSuccess = false;

	auto&			licenseValidator	= getLicenseValidator();
	LicenseManager*	licenseManager		= getLicenseManager();	
	if (licenseValidator && nullptr != licenseManager)
	{
		licenseValidator->SetConfigCancel(false);
		if (p_License.UsesTokens())
		{
			if (AutomatedApp::IsAutomationRunning())
				licenseValidator->SetError(ILicenseValidator::ErrorStatus::LICENSE_OVERFLOW, _YERROR("Not enough tokens in license."));
			else
			{
				// open token market web page
				CRMpipe::RequestResponse rr = ([licenseManager]()
				{
					CWaitCursor _;
					ExtraLicenseConfig licenseXCfg;
					licenseXCfg.m_Type = ExtraLicenseConfig::Type::TOKEN;
					return licenseManager->CRMExtraLicenseConfig(licenseXCfg);
				})();

				auto it = rr.m_Response.find(CRMpipe::g_CRMfieldKey_URLtoken);
				wstring CRMerror;
				wstring TokenURL;
				if (!rr.IsContinue())
					CRMerror = rr.GetCRMError();
				else if (rr.m_Response.end() != it)
					TokenURL = it->second;
				if (TokenURL.empty())
					CRMerror = YtriaTranslate::DoError(MainFrame_ConfigureLicense_21, _YLOC("Token acquisition URL not supplied"), _YR("Y2493")).c_str();

				if (!CRMerror.empty())
				{
					licenseValidator->SetError(ILicenseValidator::ErrorStatus::LICENSE_INCOMPLETE, YtriaTranslate::DoError(MainFrame_ConfigureLicense_27, _YLOC("Rejected: %1"), _YR("Y2510"), rr.GetCRMError().c_str()));
					if (!AutomatedApp::IsAutomationRunning())
					{
						DlgLicenseMessageBoxHTML dlg(	this,
														DlgMessageBox::eIcon_Error,
														YtriaTranslate::Do(MainFrame_ConfigureLicense_12, _YLOC("Sorry, your license could not be configured")).c_str(),
														CRMerror,
														YtriaTranslate::Do(MainFrame_ConfigureLicense_13, _YLOC("Please check the Subscription Code for this license code and try again")).c_str(),
														{ { IDCANCEL, YtriaTranslate::Do(MainFrame_ConfigureLicense_14, _YLOC("OK")).c_str() } },
														{ 0, _YTEXT("") },
														licenseManager->GetApplicationColor());
						dlg.DoModal();
					}
				}
				else
				{
					// open token market web page
					Product::getInstance().gotoAbsoluteUrl(TokenURL);
				}
			}
		}
		else
		{
			const auto fguc = p_License.GetFeatureGlobalUserCount();
			if (fguc.IsAuthorized() || p_License.IsFeatureSet(LicenseUtil::g_codeSapio365FeatureNoUserCount))
			{
				if (fguc.GetPoolType() == ExtraLicenseConfig::Type::TENANT)
				{
					if (fguc.GetPoolMax() == 0)
					{
						licenseValidator->SetError(ILicenseValidator::ErrorStatus::LICENSE_INVALID, YtriaTranslate::DoError(MainFrame_ConfigureLicense_22, _YLOC("License is canceled"),_YR("Y2494")).c_str());
						if (!AutomatedApp::IsAutomationRunning())
						{
							DlgLicenseMessageBoxHTML dlg(	this,
															DlgMessageBox::eIcon_Error,
															YtriaTranslate::Do(MainFrame_ConfigureLicense_5, _YLOC("Sorry, your license was canceled")).c_str(),
															YtriaTranslate::Do(MainFrame_ConfigureLicense_6, _YLOC("Your license has NO user allowed")).c_str(),
															YtriaTranslate::Do(MainFrame_ConfigureLicense_7, _YLOC("Contact your administrator")).c_str(),
															{ { IDCANCEL, YtriaTranslate::Do(MainFrame_ConfigureLicense_8, _YLOC("OK")).c_str() } },
															{ 0, _YTEXT("") },
															licenseManager->GetApplicationColor());
							dlg.DoModal();
						}
					}
					else
					{
						if (AutomatedApp::IsAutomationRunning())
							licenseValidator->SetError(ILicenseValidator::ErrorStatus::LICENSE_INCOMPLETE, _YERROR("Tenant users capacity not configured in license."));
						else
						{
							DlgLicenseTenantUserCaps dlg(p_License, fguc.GetPoolMax(), this);
							if (dlg.DoModal() == IDOK)
							{
								const bool noUserCount = p_License.IsFeatureSet(LicenseUtil::g_codeSapio365FeatureNoUserCount);

								const auto& utc = dlg.GetUserTenantCaps();
								const auto& kv = dlg.GetKeyValidation();
								uint32_t total = 0;
								for (size_t k = 0; k < utc.size(); k++)
									total += utc[utc.at(k)];
								if (!noUserCount && (total > fguc.GetPoolMax() || 0 == total))
								{
									// the dialog sucks - should never land here if the js in the dlg performs the right validations
									ASSERT(false);
									licenseValidator->SetError(ILicenseValidator::ErrorStatus::LICENSE_OVERFLOW, YtriaTranslate::DoError(MainFrame_ConfigureLicense_28, _YLOC("Invalid license setup: %1 users entered (max: %2)"), _YR("Y2511"), Str::getStringFromNumber(total).c_str(), Str::getStringFromNumber(fguc.GetPoolMax()).c_str()));
									DlgLicenseMessageBoxHTML dlg(this,
										DlgMessageBox::eIcon_Error,
										YtriaTranslate::Do(MainFrame_ConfigureLicense_9, _YLOC("Sorry, your license could not be configured")).c_str(),
										YtriaTranslate::DoError(MainFrame_ConfigureLicense_28, _YLOC("Invalid license setup: %1 users entered (max: %2)"), _YR("Y2512"), Str::getStringFromNumber(total).c_str(), Str::getStringFromNumber(fguc.GetPoolMax()).c_str()),
										YtriaTranslate::Do(MainFrame_ConfigureLicense_10, _YLOC("Please check the Subscription Code for this license code and try again")).c_str(),
										{ { IDCANCEL, YtriaTranslate::Do(MainFrame_ConfigureLicense_11, _YLOC("OK")).c_str() } },
										{ 0, _YTEXT("") },
										licenseManager->GetApplicationColor());
									dlg.DoModal();
								}
								else
								{
									CRMpipe::RequestResponse rr = ([utc, kv, licenseManager]()
										{
											CWaitCursor _;
											ExtraLicenseConfig licenseXCfg;
											licenseXCfg.m_Type = ExtraLicenseConfig::Type::TENANT;
											licenseXCfg.m_Caps = utc;
											licenseXCfg.m_KeyValidation = kv;
											return licenseManager->CRMExtraLicenseConfig(licenseXCfg);
										})();
										if (rr.IsError())
										{
											licenseValidator->SetError(ILicenseValidator::ErrorStatus::LICENSE_INCOMPLETE, YtriaTranslate::DoError(MainFrame_ConfigureLicense_27, _YLOC("Rejected: %1"), _YR("Y2513"), rr.GetCRMError().c_str()));
											DlgLicenseMessageBoxHTML dlg(this,
												DlgMessageBox::eIcon_Error,
												YtriaTranslate::Do(MainFrame_ConfigureLicense_12, _YLOC("Sorry, your license could not be configured")).c_str(),
												rr.GetCRMError(),
												YtriaTranslate::Do(MainFrame_ConfigureLicense_13, _YLOC("Please check the Subscription Code for this license code and try again")).c_str(),
												{ { IDCANCEL, YtriaTranslate::Do(MainFrame_ConfigureLicense_14, _YLOC("OK")).c_str() } },
												{ 0, _YTEXT("") },
												licenseManager->GetApplicationColor());
											dlg.DoModal();
										}
										else
										{
											CWaitCursor _;
											rvCfgSuccess = true;
											auto license = licenseManager->RefreshLicense();// from server
											auto fv = license.GetFeatures(Counter, ExtraLicenseConfig::Type::TENANT);

											wstring newCfg;
											if (noUserCount)
											{
												newCfg = YtriaTranslate::Do(MainFrame_ConfigureLicense_24, _YLOC("Unlimited access set for tenant(s):")).c_str();
												for (const auto& f : fv)
												{
													if (!f.IsUnset() && fguc.m_Code != f.m_Code)
														newCfg += YtriaTranslate::Do(MainFrame_ConfigureLicense_23, _YLOC("\n - '%1'"), f.GetPoolContextValue().c_str());
												}
											}
											else
											{
												newCfg = YtriaTranslate::Do(MainFrame_ConfigureLicense_20, _YLOC("Maximum tenant user capacity for this license: %1\nCurrently set:"), Str::getStringFromNumber(fguc.GetPoolMax()).c_str());
												for (const auto& f : fv)
												{
													if (!f.IsUnset() && fguc.m_Code != f.m_Code)
														newCfg += YtriaTranslate::Do(MainFrame_ConfigureLicense_23, _YLOC("\n - %1 users for '%2'"), Str::getStringFromNumber(f.GetPoolMax()).c_str(), f.GetPoolContextValue().c_str()); // f.m_StatusStr.c_str() -- feature status not really useful here
												}
											}

											DlgLicenseMessageBoxHTML dlg(this,
												DlgMessageBox::eIcon_Information,
												YtriaTranslate::Do(MainFrame_ConfigureLicense_15, _YLOC("Success! Your license is now configured")).c_str(),
												newCfg,
												license.m_StatusStr,
												{ { IDCANCEL, YtriaTranslate::Do(MainFrame_ConfigureLicense_16, _YLOC("OK")).c_str() } },
												{ 0, _YTEXT("") },
												licenseManager->GetApplicationColor());
											dlg.DoModal();
											// at this point if for some reason the license has become invalid (expired, canceled etc.), the next call to IsLicenseNone (next command, next grid use) will block the user

											refreshFrameTitle();
										}
								}
							}
							else
								licenseValidator->SetConfigCancel(true);
						}
					}
				}
				else
				{
					ASSERT(false);// global user count not a tenant feature = qlb
					licenseValidator->SetError(ILicenseValidator::ErrorStatus::CONTEXT_INVALID, _YFORMAT(L"License is malformed"));
				}
			}
			else if (fguc.IsUnset())
			{
				// feature not present: open license
				rvCfgSuccess = true;
			}
			else
			{
				if (fguc.IsDenied())
					licenseValidator->SetError(ILicenseValidator::ErrorStatus::LICENSE_INVALID, YtriaTranslate::DoError(MainFrame_ConfigureLicense_31, _YLOC("Denied: %1"),_YR("Y2514"), fguc.m_StatusStr.c_str()));
				else if (fguc.IsLicenseExpired())
					licenseValidator->SetError(ILicenseValidator::ErrorStatus::LICENSE_INVALID, YtriaTranslate::DoError(MainFrame_ConfigureLicense_32, _YLOC("License expired: %1"),_YR("Y2515"), fguc.m_StatusStr.c_str()));
				else if (fguc.IsUnauthorized())
					licenseValidator->SetError(ILicenseValidator::ErrorStatus::LICENSE_INVALID, YtriaTranslate::DoError(MainFrame_ConfigureLicense_33, _YLOC("Unauthorized: %1"),_YR("Y2516"), fguc.m_StatusStr.c_str()));
				else if (fguc.IsUnknown())
					licenseValidator->SetError(ILicenseValidator::ErrorStatus::LICENSE_INVALID, YtriaTranslate::DoError(MainFrame_ConfigureLicense_34, _YLOC("Unknown: %1"),_YR("Y2517"), fguc.m_StatusStr.c_str()));
				else if (fguc.IsError())
					licenseValidator->SetError(ILicenseValidator::ErrorStatus::LICENSE_INVALID, YtriaTranslate::DoError(MainFrame_ConfigureLicense_35, _YLOC("Error: %1"),_YR("Y2518"), fguc.m_StatusStr.c_str()));
			
				if (!AutomatedApp::IsAutomationRunning())
				{
					DlgLicenseMessageBoxHTML dlg(	this,
													DlgMessageBox::eIcon_Error,
													YtriaTranslate::Do(MainFrame_ConfigureLicense_17, _YLOC("Sorry, your license is invalid")).c_str(),
													licenseValidator->GetErrorMessage(),
													YtriaTranslate::Do(MainFrame_ConfigureLicense_18, _YLOC("Contact your administrator")).c_str(),
													{ { IDCANCEL, YtriaTranslate::Do(MainFrame_ConfigureLicense_19, _YLOC("OK")).c_str() } },
													{ 0, _YTEXT("") },
													licenseManager->GetApplicationColor());
					dlg.DoModal();
				}
			}
		}
	}

	return rvCfgSuccess;
}

bool MainFrame::UpgradeLicense(const VendorLicense& p_License)
{
	bool rvCfgSuccess = false;

	auto lv = getLicenseValidator();
	ASSERT(nullptr != lv);
	uint32_t minUser = (nullptr != lv) ? lv->GetUserCountForFreeAccess() : 50;
	
	DlgLicenseUpgrade dlgUpgrade(p_License, minUser, this);
	if (IDOK == dlgUpgrade.DoModal())
	{
		LicenseManager*	licenseManager = getLicenseManager();
		if (nullptr != licenseManager)
		{
			LicenseUpgradeConfig licenseUCfg = dlgUpgrade.GetUpgradeCfg();
			{
				CWaitCursor _;
				CRMpipe::RequestResponse rr = licenseManager->CRMExtraLicenseUpgrade(licenseUCfg);
				if (rr.IsError())
				{
					DlgLicenseMessageBoxHTML dlgError(	this,
														 DlgMessageBox::eIcon_Error,
														 YtriaTranslate::Do(MainFrame_UpgradeLicense_1, _YLOC("License upgrade could not go through")).c_str(),
														 _YTEXTFORMAT(L"%s\n", rr.GetCRMError().c_str()),
														 YtriaTranslate::Do(MainFrame_UpgradeLicense_4, _YLOC("Please try again.")).c_str(),
														 { { IDCANCEL, YtriaTranslate::Do(MainFrame_UpgradeLicense_2, _YLOC("OK")).c_str() } },
														 { 0, _YTEXT("") },
														 licenseManager->GetApplicationColor());
					dlgError.DoModal();
				}
				else
				{
					rvCfgSuccess = true;
					DlgLicenseUpgradeReport dlgReport(licenseUCfg.m_UserCapacity, rr, this);
					dlgReport.DoModal();
				}
			}
		}
	}

	return rvCfgSuccess;
}

void MainFrame::trackSessionUsageStop()
{
	if (!m_LoginSessionTrackName.empty())
	{
		LicenseManager::TrackStop(m_LoginSessionTrackName);
		m_LoginSessionTrackName.clear();
	}
}

void MainFrame::postSyncCache(bool p_CreateElevatedIfNeeded)
{
	auto app = dynamic_cast<Office365AdminApp*>(AutomatedApp::GetAutomatedApp());

	bool wasCreatingSession = false;
	ASSERT(nullptr != app);
	if (nullptr != app)
	{
		wasCreatingSession = app->GetIsCreatingSession();
		app->SetIsCreatingSession(false);
	}

	m_SyncInProgress = false;
	if (nullptr != m_HtmlView)
		m_HtmlView->EnableWindow(TRUE);

	// Update list after load, order might have changed.
	PopulateRecentSessions();

	hideStatusBarProgress();
	// makes progress really disappear now.
	UpdateWindow();

	CreateMissingRoles();

	if (p_CreateElevatedIfNeeded && wasCreatingSession && nullptr != app)
	{
		auto sessionBeingCreated = app->GetSapioSessionBeingLoaded();
		if (sessionBeingCreated)
			CreateElevatedSession(sessionBeingCreated);
		else
			CreateElevatedSession(GetSapio365Session());
	}

	if (AutomatedApp::IsAutomationRunning())
	{
		ASSERT(nullptr != app);
		if (nullptr != app)
		{
			if (IsActionLoadSession(app->GetCurrentAction()))
				app->SetAutomationGreenLight(app->GetCurrentAction());
		}
	}

	if (AutomationRecording::IsRecording())
	{
		ASSERT(nullptr != app);
		if (nullptr != app)
		{
			auto& session = GetSessionInfo();
			ASSERT(!session.GetSessionName().empty());
			if (!session.GetSessionName().empty())
			{
				if (session.GetRoleNameInDb().empty())
					AutomationRecording::AddActionX(g_ActionNameLoadSession, { { AutomationConstant::val().m_ParamNameName, session.GetSessionName() },{ AutomationConstant::val().m_ParamNameType, session.GetSessionType() } });
				else
					AutomationRecording::AddActionX(g_ActionNameLoadSession, { { AutomationConstant::val().m_ParamNameName, session.GetSessionName() },{ AutomationConstant::val().m_ParamNameType, session.GetSessionType() },{ g_ParamNameRole, session.GetRoleNameInDb() } });
			}
		}
	}

	{
		auto app = dynamic_cast<Office365AdminApp*>(AutomatedApp::GetAutomatedApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
			app->RunPendingCommandLineAutomation();
	}
}

void MainFrame::CreateElevatedSession(std::shared_ptr<Sapio365Session> p_SessionBeingCreated)
{
	ASSERT(p_SessionBeingCreated);
	if (p_SessionBeingCreated)
	{
		auto idFullSession = p_SessionBeingCreated->GetIdentifier();
		if (idFullSession.IsAdvanced())
		{
			idFullSession.m_SessionType = SessionTypes::g_ElevatedSession;
			QuerySessionExists query(idFullSession, SessionsSqlEngine::Get());
			query.Run();

			if (!query.Exists())
			{
				auto persistentSession = SessionsSqlEngine::Get().Load(p_SessionBeingCreated->GetIdentifier());
				if (askToCombineWithUltraAdmin(p_SessionBeingCreated, persistentSession, true, this))
					Sapio365Session::SetSignedIn(p_SessionBeingCreated, this);
			}
		}
	}
}

void MainFrame::CreateMissingRoles()
{
	if (!GetSessionInfo().IsUltraAdmin())
	{
		vector<RoleDelegation> delegations;
		RoleDelegationManager::GetInstance().GetRoleDelegationsForThisUser(GetSessionInfo().GetConnectedUserId(), delegations, GetSapio365Session());

		for (const auto& delegation : delegations)
		{
			QueryRoleExists queryExists(delegation.GetID(), SessionsSqlEngine::Get());
			queryExists.Run();

			if (!queryExists.Exists())
			{
				QueryCreateRole queryCreate(delegation.GetID(), delegation.GetDelegation().m_Name, SessionsSqlEngine::Get());
				queryCreate.Run();
				ASSERT(queryCreate.IsSuccess());
			}
		}
	}
}

LRESULT MainFrame::OnOAuth2BrowserClose(WPARAM wParam, LPARAM lParam)
{
	if (nullptr != m_oauth2Browser)
		m_oauth2Browser->EndDialog(IDOK);
    return 0;
}

LRESULT MainFrame::OnUserChange(WPARAM wParam, LPARAM lParam)
{
    CWaitCursor cursor;
    LoggerService::Debug(_YTEXT("MainFrame::OnUserChange: Entering OnUserChange"));

    syncSessionCache();
    
	if (0 != wParam)
	{
		// This contains API Name
		wstring* pTemp = reinterpret_cast<wstring*>(wParam);
		delete pTemp;
	}

	WindowsListUpdater::GetInstance().Update();

    return 0;
}

LRESULT MainFrame::OnAuthenticationCanceled(WPARAM wParam, LPARAM lParam)
{
/*
// the greenlight here corrupts the error management on the load session

	if (AutomatedApp::IsAutomationRunning())
	{
		auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
		{
			auto action = app->GetCurrentAction();
			if (IsActionLoadSession(action))
			{
				// Bug #190925.RO.00A4A6: Now all session loading dialogs are automatized. Do not display this error.
				//action->AddError(YtriaTranslate::DoError(MainFrame_OnAuthenticationCanceled_1, _YLOC("Session loading canceled by user"),_YR("Y2495")).c_str());
				app->SetAutomationGreenLight(action);
			}
		}
	}
*/

	if (0 != wParam)
	{
		// This contains API Name
		wstring* pTemp = reinterpret_cast<wstring*>(wParam);
		delete pTemp;
	}

	return 0;
}

LRESULT MainFrame::OnAuthenticationFailure(WPARAM wParam, LPARAM lParam)
{
	if (AutomatedApp::IsAutomationRunning())
	{
		auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
		{
			auto action = app->GetCurrentAction();
			if (IsActionLoadSession(action))
			{
				action->AddError(YtriaTranslate::DoError(MainFrame_OnAuthenticationFailure_1, _YLOC("Unable to load session"),_YR("Y2496")).c_str());
				app->SetAutomationGreenLight(action);
			}
		}
	}

	if (0 != wParam)
	{
		// This contains API Name
		wstring* pTemp = reinterpret_cast<wstring*>(wParam);
		delete pTemp;
	}

	return 0;
}

LRESULT MainFrame::OnReadyToTestAuthentication(WPARAM wParam, LPARAM lParam)
{
	LoggerService::Debug(_YTEXT("MainFrame::OnReadyToTestAuthentication: Entering OnReadyToTestAuthentication"));

	if (0 != wParam)
	{
		// This contains API Name
		wstring* pTemp = reinterpret_cast<wstring*>(wParam);
		delete pTemp;
	}

	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
	ASSERT(nullptr != app);
	auto sapioSession = nullptr != app ? app->GetSapioSessionBeingLoaded() : shared_ptr<Sapio365Session>();
	ASSERT(sapioSession);

	try
	{
		sapioSession->GetGraphCache().SetOrganization(sapioSession->GetGraphCache().GetOrganization(YtriaTaskData()).get());
		sapioSession->GetMainMSGraphSession()->SetAuthenticationState(RestSession::AuthenticationState::Ready);
		SendMessage(WM_AUTHENTICATION_SUCCESS, (WPARAM) new wstring(sapioSession->GetMainMSGraphSession()->GetName()), 0);
	}
	catch (const web::http::oauth2::experimental::oauth2_exception& e)
	{
		LoggerService::Debug(_YTEXT("MainFrame::OnReadyToTestAuthentication: oauth2_exception"));

		auto handler = sapioSession->GetMainMSGraphSession()->GetErrorHandler();
		ASSERT(handler);
		if (handler)
			handler->HandleOAuth2Exception(e, YtriaTaskData());

		const auto error = Sapio365Session::ParseGraphJsonError(Str::convertFromUTF8(e.what())).m_ErrorDescription;

		LoggerService::Debug(_YTEXTFORMAT(L"MainFrame::OnReadyToTestAuthentication: Refresh token expired. Authentication needed. %s", error.c_str()));

		if (AutomatedApp::IsAutomationRunning() && IsActionLoadSession(app->GetCurrentAction()))
		{
			app->GetCurrentAction()->AddError(_YFORMAT(L"An error occurred while authenticating: %s", error.c_str()).c_str());
			app->SetAutomationGreenLight(app->GetCurrentAction());
			sapioSession->GetMainMSGraphSession()->SetAuthenticationState(RestSession::AuthenticationState::Error);
		}
		else if (sapioSession->GetIdentifier().m_SessionType != SessionTypes::g_ElevatedSession)
		{
			auto authenticator = dynamic_cast<OAuth2AuthenticatorBase*>(sapioSession->GetMainMSGraphSession()->GetAuthenticator().get());
			ASSERT(nullptr != authenticator);
			if (nullptr != authenticator && !authenticator->GetLastLoadedLogin().empty())
				authenticator->SetPreferredLogin(authenticator->GetLastLoadedLogin());

			sapioSession->GetMainMSGraphSession()->Start(this);
		}
		else if (sapioSession->GetIdentifier().m_SessionType == SessionTypes::g_ElevatedSession)
		{
			PersistentSession persistentSession = SessionsSqlEngine::Get().Load(sapioSession->GetIdentifier());
			app->RelogElevatedSession(sapioSession, persistentSession, sapioSession->GetIdentifier(), false, this, false);
		}
	}
	catch (const std::exception& e)
	{
		const auto error = Str::convertFromUTF8(e.what());

		LoggerService::Debug(_YTEXTFORMAT(L"MainFrame::OnReadyToTestAuthentication: Failed %s", error.c_str()));

		auto handler = sapioSession->GetMainMSGraphSession()->GetErrorHandler();
		ASSERT(handler);
		if (handler)
			handler->HandleStdException(e, nullptr);

		if (AutomatedApp::IsAutomationRunning() && IsActionLoadSession(app->GetCurrentAction()))
		{
			app->GetCurrentAction()->AddError(_YFORMAT(L"An error occurred while authenticating: %s", error.c_str()).c_str());
			app->SetAutomationGreenLight(app->GetCurrentAction());
		}

		sapioSession->GetMainMSGraphSession()->SetAuthenticationState(RestSession::AuthenticationState::Error);
	}
	catch (...)
	{
		LoggerService::Debug(_YTEXT("MainFrame::OnReadyToTestAuthentication: Failed"));

		if (AutomatedApp::IsAutomationRunning() && IsActionLoadSession(app->GetCurrentAction()))
		{
			app->GetCurrentAction()->AddError(YtriaTranslate::Do(Office365AdminApp_LoadSession_2, _YLOC("An error occurred while authenticating. Please make sure your credentials are correct.")).c_str());
			app->SetAutomationGreenLight(app->GetCurrentAction());
		}

		sapioSession->GetMainMSGraphSession()->SetAuthenticationState(RestSession::AuthenticationState::Error);
	}

	return 0;
}

void MainFrame::showBackstageMenuUpdateEntry(CXTPRibbonBar& p_RibbonBar, bool p_Show)
{
	CXTPRibbonBackstageView* backstage = dynamic_cast<CXTPRibbonBackstageView*>(p_RibbonBar.GetSystemButton()->GetCommandBar());
	ASSERT(nullptr != backstage);
	if (nullptr != backstage)
	{
		bool done = false;
		for (int i = 0; i < backstage->GetControlCount(); ++i)
		{
			auto ctrl = backstage->GetControl(i);
			if (nullptr != ctrl && ctrl->GetID() == ID_PRODUCT_UPDATE)
			{
				ctrl->SetVisible(p_Show ? TRUE : FALSE);
				done = true;
				break;
			}
		}

		ASSERT(done);

		if (done)
		{
			auto* ctrl = backstage->GetControls()->GetFirst();

			// If first is back button (XTP_ID_RIBBONBACKSTAGE_BACK) automatically added by codejock theme, get next
			if (ctrl->GetID() == XTP_ID_RIBBONBACKSTAGE_BACK)
				backstage->GetControls()->GetNext(ctrl);

			ASSERT(ctrl->GetID() == ID_PRODUCT_UPDATE);

			if (p_Show)
			{
				auto nextCtrl = ctrl;
				backstage->GetControls()->GetNext(nextCtrl);
				ASSERT(nextCtrl->GetID() == ID_JOBCENTERCFG);
				nextCtrl->SetItemDefault(FALSE);
			}
			else
			{
				// "Update" is the first page. In case we don't show the Update menu entry, the page can't be the default one.
				// Use the next one (probably "Recent Sessions") as default
				ctrl->SetItemDefault(FALSE);
				backstage->GetControls()->GetNext(ctrl);
				ASSERT(ctrl->GetID() == ID_JOBCENTERCFG);
			}

			ctrl->SetItemDefault(TRUE);

			if (::IsWindow(backstage->m_hWnd) && backstage->IsWindowVisible())
			{
				// Re-show current page to refresh the backstage menu.
				auto activeTab = backstage->GetActiveTab();
				if (nullptr != activeTab)
					showBackstageTab(activeTab->GetID());
			}
		}
	}
}

CXTPRibbonBar* MainFrame::createRibbonBar()
{
	CXTPRibbonBar* myRibbonBar = FrameBase::createRibbonBar();
	ASSERT(nullptr != myRibbonBar);
	if (nullptr == myRibbonBar)
		return myRibbonBar;

	showBackstageMenuUpdateEntry(*myRibbonBar, false);

	m_connectTab = myRibbonBar->AddTab(YtriaTranslate::Do(MainFrame_createRibbonBar_1, _YLOC("Session")).c_str());
	ASSERT(nullptr != m_connectTab);
	if (nullptr != m_connectTab)
	{
		auto groupConnect = m_connectTab->AddGroup(YtriaTranslate::Do(MainFrame_createRibbonBar_2, _YLOC("New Session")).c_str());

        fillSessionGroup(groupConnect);

		{
			auto groupCurrent = addSessionGroupToRibbonTab(*m_connectTab, false);

			auto ctrlEditElevated = getCtrlEditElevated();
			ASSERT(nullptr != ctrlEditElevated);
			{
				m_RoleInfoControl = groupCurrent->Add(xtpControlButton, ID_SESSION_SEE_ROLEINFO, nullptr, ctrlEditElevated->GetIndex());
				setImage({ ID_SESSION_SEE_ROLEINFO }, IDB_SESSION_SEE_ROLEINFO, xtpImageNormal);
				setImage({ ID_SESSION_SEE_ROLEINFO }, IDB_SESSION_SEE_ROLEINFO_16X16, xtpImageNormal);
				setControlTexts(m_RoleInfoControl,
					YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_22, _YLOC("Role Info")).c_str(),
					YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_23, _YLOC("Current Role Info")).c_str(),
					YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_24, _YLOC("Show details of the current Role.")).c_str());
				ASSERT(nullptr != m_RoleInfoControl);
				if (nullptr != m_RoleInfoControl)
					m_RoleInfoControl->SetVisible(FALSE);
			}

			{
				m_RoleSelectControl = groupCurrent->Add(xtpControlButton, ID_SESSION_APPLY_ROLE, nullptr, ctrlEditElevated->GetIndex());
				setImage({ ID_SESSION_APPLY_ROLE }, IDB_SESSION_APPLY_ROLE, xtpImageNormal);
				setImage({ ID_SESSION_APPLY_ROLE }, IDB_SESSION_APPLY_ROLE_16X16, xtpImageNormal);
				setControlTexts(m_RoleSelectControl,
					YtriaTranslate::Do(MainFrame_createRibbonBar_9, _YLOC("Choose Role")).c_str(),
					YtriaTranslate::Do(MainFrame_createRibbonBar_10, _YLOC("Open a new session with a different role.")).c_str(),
					YtriaTranslate::Do(MainFrame_createRibbonBar_11, _YLOC("This will not close the current session. You can therefore open multiple sessions with a different role for each.")).c_str());

				ASSERT(nullptr != m_RoleSelectControl);
				if (nullptr != m_RoleSelectControl)
					m_RoleSelectControl->SetVisible(FALSE);
			}

			{
				m_ConnectToCustomerControl = groupCurrent->Add(xtpControlButton, ID_CONNECT_TO_CUSTOMER, nullptr, ctrlEditElevated->GetIndex());
				setImage({ ID_CONNECT_TO_CUSTOMER }, IDB_CONNECT_TO_CUSTOMER, xtpImageNormal);
				setImage({ ID_CONNECT_TO_CUSTOMER }, IDB_CONNECT_TO_CUSTOMER_16X16, xtpImageNormal);
				setControlTexts(m_ConnectToCustomerControl,
					_T("Partner Access"),
					_T("Partner Access"),
					_T("If you have access to the Microsoft Partner Center, this will let you select and connect to a customer tenant with an advanced session."));

				ASSERT(nullptr != m_ConnectToCustomerControl);
				if (nullptr != m_ConnectToCustomerControl)
					m_ConnectToCustomerControl->SetVisible(FALSE);
			}
		}

		{
			auto groupOpenedSessions = m_connectTab->AddGroup(g_AvailableSessionGroupName.c_str());
			groupOpenedSessions->AllowReduce(0); // Bug #200124.SR.00B3E2: When sapio365 window is small or session name is too lomg, the whole Recent Sessions list disappear.
		}
		
		{
			auto groupManagement = m_connectTab->AddGroup(_YTEXT(""));

			auto ctrl = groupManagement->Add(xtpControlButton, ID_VIEW_SESSIONS);
			setControlTexts(ctrl,
				YtriaTranslate::Do(MainFrame_createRibbonBar_3, _YLOC("Manage Sessions")).c_str(),
				YtriaTranslate::Do(MainFrame_createRibbonBar_4, _YLOC("Manage Recent Sessions")).c_str(),
				YtriaTranslate::Do(MainFrame_createRibbonBar_5, _YLOC("Load, delete, and lock recent sessions.\nSee a detailed list of all recent session information, including the name of the user and tenant, session status, date when the session was created and last used, and access count.")).c_str());

			auto ctrlEditOnPrem = groupManagement->Add(xtpControlButton, ID_EDIT_ONPREM_SETTINGS_RIBBON);
			setControlTexts(ctrlEditOnPrem,
				_T("Edit On-Prem settings"),
				_T("Edit On-Prem settings for the current session"),
				_T("Edit On-Prem settings for the current session"));
		}

		{
			auto group = m_connectTab->AddGroup(_T("Open"));

			{
				auto ctrl = group->Add(xtpControlButton, ID_LOAD_GRID_PHOTO_SNAPSHOT);
				setImage({ ID_LOAD_GRID_PHOTO_SNAPSHOT }, IDB_GRID_LOAD_PHOTO_SNAPSHOT, xtpImageNormal);
				setImage({ ID_LOAD_GRID_PHOTO_SNAPSHOT }, IDB_GRID_LOAD_PHOTO_SNAPSHOT_16X16, xtpImageNormal);
				setControlTexts(ctrl
					, _T("Snapshot")
					, _T("Load snapshot from file.")
					, _T("Snapshots save the state of a given data set as a file. They can be shared and read by others, but will not allow modifications to be saved to the server.\nThey may also require a password to open. "));
			}

			{
				auto ctrl = group->Add(xtpControlButton, ID_LOAD_GRID_RESTORE_SNAPSHOT);
				setImage({ ID_LOAD_GRID_RESTORE_SNAPSHOT }, IDB_GRID_LOAD_RESTORE_SNAPSHOT, xtpImageNormal);
				setImage({ ID_LOAD_GRID_RESTORE_SNAPSHOT }, IDB_GRID_LOAD_RESTORE_SNAPSHOT_16X16, xtpImageNormal);
				setControlTexts(ctrl
					, _T("Restore Point")
					, _T("Load restore point from file")
					, _T("A restore point is a file that lets you pause and save your work so you can resume where you left off.\nIt may also require a password to open. "));
			}
		}

		if (CRMpipe().IsFeatureSandboxed())
		{
			auto group = m_connectTab->AddGroup(_YTEXT(""));

			{
				auto ctrl = group->Add(xtpControlButton, ID_VIEW_SCHOOLS);
				setControlTexts(ctrl, _T("View Schools"), _T(""), _T(""));
				ctrl->SetStyle(xtpButtonIconAndCaption);
			}

			{
				auto ctrl = group->Add(xtpControlButton, ID_VIEW_EDU_USERS);
				setControlTexts(ctrl, _T("View Education Users"), _T(""), _T(""));
				ctrl->SetStyle(xtpButtonIconAndCaption);
			}

			{
				auto ctrl = group->Add(xtpControlButton, ID_VIEW_APPLICATIONS);
				setControlTexts(ctrl, _T("View Applications"), _T(""), _T(""));
				ctrl->SetStyle(xtpButtonIconAndCaption);
			}

			{
				auto ctrl = group->Add(xtpControlSplitButtonPopup, ID_NEW_ULTRAADMIN_SESSION);
				setControlTexts(ctrl,
					YtriaTranslate::Do(MainFrame_FillSessionGroup_19, _YLOC("New Ultra Admin Session")).c_str(),
					YtriaTranslate::Do(MainFrame_FillSessionGroup_20, _YLOC("New Ultra Admin Session")).c_str(),
					YtriaTranslate::Do(MainFrame_FillSessionGroup_21, _YLOC("Additional access (with full permissions assigned):\n\n    - Complete account info for all users\n    - All mail for all users, including attachments\n    - All mailbox settings for all mailboxes\n    - All drive items for all groups, users, and sites\n    - All calendar information for all users and groups\n    - All SharePoint site information, including lists and list items\n    - All contacts for all users\n\nAdmin consent required\nThis requires an app registration on the Azure v2 Active Directory Endpoint.\nAll sapio365 app permissions are assigned by the creator of the app ID.")).c_str());
				ctrl->SetStyle(xtpButtonIconAndCaption);

				auto popup = dynamic_cast<CXTPControlPopup*>(ctrl);
				ASSERT(nullptr != popup);
				if (nullptr != popup)
				{
					{
						auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ctrl->GetID());
						menuItem->SetCaption(ctrl->GetCaption());
						menuItem->SetDescription(ctrl->GetDescription());
					}

					{
						auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_NEW_ULTRAADMIN_SESSION_HELP_WHAT_CAN_I_DO);
						menuItem->SetCaption(YtriaTranslate::Do(MainFrame_FillSessionGroup_22, _YLOC("Learn more about Ultra Admin sessions")).c_str());
						menuItem->SetTooltip(YtriaTranslate::Do(MainFrame_FillSessionGroup_23, _YLOC("Learn more about Ultra Admin sessions")).c_str());
						menuItem->SetDescription(YtriaTranslate::Do(MainFrame_FillSessionGroup_24, _YLOC("Learn more about Ultra Admin sessions")).c_str()); // show in status bar
						setImage({ ID_NEW_ULTRAADMIN_SESSION_HELP_WHAT_CAN_I_DO }, IDB_NEW_SESSION_MENU_WHAT_CAN_I_DO_16X16, xtpImageNormal);
					}

					{
						auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_NEW_ULTRAADMIN_SESSION_HOW_TO_CREATE_APP);
						menuItem->SetCaption(YtriaTranslate::Do(MainFrame_FillSessionGroup_25, _YLOC("How do I create an application ID?")).c_str());
						menuItem->SetTooltip(YtriaTranslate::Do(MainFrame_FillSessionGroup_26, _YLOC("How do I create an application ID?")).c_str());
						menuItem->SetDescription(YtriaTranslate::Do(MainFrame_FillSessionGroup_27, _YLOC("How do I create an application ID?")).c_str()); // show in status bar
						setImage({ ID_NEW_ULTRAADMIN_SESSION_HOW_TO_CREATE_APP }, IDB_NEW_SESSION_MENU_HOW_TO_CREATE_APP_16X16, xtpImageNormal);
					}

					{
						auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_NEW_ULTRAADMIN_SESSION_GIVE_ADMIN_CONSENT);
						menuItem->SetCaption(YtriaTranslate::Do(MainFrame_FillSessionGroup_28, _YLOC("Provide admin consent for your tenant")).c_str());
						menuItem->SetTooltip(YtriaTranslate::Do(MainFrame_FillSessionGroup_29, _YLOC("Provide admin consent for your tenant")).c_str());
						menuItem->SetDescription(YtriaTranslate::Do(MainFrame_FillSessionGroup_33, _YLOC("Description needed")).c_str()); // show in status bar
						setImage({ ID_NEW_ULTRAADMIN_SESSION_GIVE_ADMIN_CONSENT }, IDB_NEW_SESSION_MENU_GIVE_ADMIN_CONSENT_16X16, xtpImageNormal);
					}

					//{
					//	auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_NEW_ULTRAADMIN_SESSION_SEND_EMAIL_TO_GET_CONSENT);
					//	menuItem->SetCaption(_TLOC("Send email to your admin to get Admin Consent"));
					//	menuItem->SetTooltip(_TLOC("Send email to your admin to get Admin Consent"));
					//	menuItem->SetDescription(_TLOC("Description needed")); // show in status bar
					//	setImage({ ID_NEW_ULTRAADMIN_SESSION_SEND_EMAIL_TO_GET_CONSENT }, IDB_NEW_SESSION_MENU_SEND_EMAIL_TO_ADMIN_16X16, xtpImageNormal);
					//}
				}
			}
		}

		{
			auto debugGroup = m_connectTab->AddGroup(g_debug_debugGroup.c_str());

			{
				auto ctrl = debugGroup->Add(xtpControlButton, ID_DEBUG_SEND_RULES_TO_CLIPBOARD);
				ctrl->SetCaption(_YTEXT("Copy Automation Rules"));
				ctrl->SetTooltip(_YTEXT("Copy Automation Rules"));
			}

			{
				auto ctrl = debugGroup->Add(xtpControlButton, ID_DEBUG_SEND_SYSTEMVAR_TO_CLIPBOARD);
				ctrl->SetCaption(_YTEXT("Copy System Vars"));
				ctrl->SetTooltip(_YTEXT("Copy System Vars"));
			}

			{
				auto ctrl = debugGroup->Add(xtpControlButton, ID_DEBUG_SHOW_REQUEST_TESTER);
				ctrl->SetCaption(_YTEXT("Graph Request Tester"));
				ctrl->SetTooltip(_YTEXT("Graph Request Tester"));
			}
			
			{
				auto ctrl = debugGroup->Add(xtpControlButton, ID_DEBUG_LOAD_HTML_IN_DLG);
				ctrl->SetCaption(_YTEXT("Dialog - Load User html"));
				ctrl->SetTooltip(_YTEXT("Dialog - Load User html"));
			}

			{
				auto ctrl = debugGroup->Add(xtpControlButton, ID_DEBUG_LOAD_USER_HTML);
				ctrl->SetCaption(_YTEXT("Load User HTML"));
				ctrl->SetTooltip(_YTEXT("Load User HTML"));
			}

			{
				auto ctrl = debugGroup->Add(xtpControlButton, ID_DEBUG_RELOAD_CURRENT_HTML);
				ctrl->SetCaption(_YTEXT("Reload"));
				ctrl->SetTooltip(_YTEXT("Reload"));
			}
			{
				auto ctrl = debugGroup->Add(xtpControlButton, ID_DEBUG_REINIT_HOMEPAGE);
				ctrl->SetCaption(_YTEXT("Re-init (default)"));
				ctrl->SetTooltip(_YTEXT("Re-init (default)"));
			}
			{
				auto ctrl = debugGroup->Add(xtpControlButton, ID_DEBUG_LOAD_MODULES_HTML);
				ctrl->SetCaption(_YTEXT("Load Modules.html"));
				ctrl->SetTooltip(_YTEXT("Load Modules.html"));
			}

			{
				auto ctrl = debugGroup->Add(xtpControlButton, ID_VIEW_WEBCONSOLE);
				ctrl->SetCaption(_YTEXT("Open web console"));
				ctrl->SetTooltip(_YTEXT("Open web console"));
			}

			debugGroup->SetVisible(FALSE);
		}
	}

	// Done here, to be sure it's the last tab
	m_feedbackTab = addFeedbackRibbonTab(*myRibbonBar);
    
    setImage({ ID_NEW_STANDARD_SESSION }, IDB_NEW_STANDARD_SESSION, xtpImageNormal);
    setImage({ ID_NEW_STANDARD_SESSION }, IDB_NEW_STANDARD_SESSION_16X16, xtpImageNormal);
	setImage({ ID_NEW_ADVANCED_SESSION }, IDB_NEW_SESSION, xtpImageNormal);
	setImage({ ID_NEW_ADVANCED_SESSION }, IDB_NEW_SESSION_16X16, xtpImageNormal);
	setImage({ ID_VIEW_SESSIONS }, IDB_VIEW_SESSIONS, xtpImageNormal);
	setImage({ ID_VIEW_SESSIONS }, IDB_VIEW_SESSIONS_16X16, xtpImageNormal);
	setImage({ ID_EDIT_ONPREM_SETTINGS_RIBBON }, IDB_EDIT_ONPREM_SETTINGS_RIBBON, xtpImageNormal);
	setImage({ ID_EDIT_ONPREM_SETTINGS_RIBBON }, IDB_EDIT_ONPREM_SETTINGS_RIBBON_16X16, xtpImageNormal);
	setImage({ ID_SELECT_SESSION }, IDB_SELECT_SESSION, xtpImageNormal);
	setImage({ ID_SELECT_SESSION }, IDB_SELECT_SESSION_16X16, xtpImageNormal);

	GetCommandBars()->GetShortcutManager()->SetAccelerators(IDR_MAINFRAME);

	return myRibbonBar;
}

void MainFrame::fillSessionGroup(CXTPRibbonGroup * groupConnect)
{
    // Basic user session
    auto ctrl = groupConnect->Add(xtpControlSplitButtonPopup, ID_NEW_STANDARD_SESSION);

	setControlTexts(ctrl,
		YtriaTranslate::Do(MainFrame_FillSessionGroup_1, _YLOC("New Standard Session")).c_str(),
		YtriaTranslate::Do(MainFrame_FillSessionGroup_2, _YLOC("New Standard Session")).c_str(),
		YtriaTranslate::Do(MainFrame_FillSessionGroup_3, _YLOC("You can access all your own data, public data, and data shared with you by other users including:\n\n    - Messages\n    - Contacts\n    - Drive items and their permissions\n    - Group listings, group owners, and memberships\n    - Calendar events for accessible mailboxes\n    - Site and site list information\n\nNo admin consent is needed.")).c_str());

    CXTPControlPopup* popup = dynamic_cast<CXTPControlPopup*>(ctrl);
    ASSERT(nullptr != popup);
    if (nullptr != popup)
    {
		{
			auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ctrl->GetID());
			menuItem->SetCaption(ctrl->GetCaption());
			menuItem->SetDescription(ctrl->GetDescription());
		}

		{
			auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_NEW_STANDARD_SESSION_HELP_WHAT_CAN_I_DO);
			menuItem->SetCaption(YtriaTranslate::Do(MainFrame_FillSessionGroup_4, _YLOC("Learn more about Standard sessions")).c_str());
			menuItem->SetTooltip(YtriaTranslate::Do(MainFrame_FillSessionGroup_5, _YLOC("Learn more about Standard sessions")).c_str());
			menuItem->SetDescription(YtriaTranslate::Do(MainFrame_FillSessionGroup_6, _YLOC("Learn more about Standard sessions")).c_str()); // show in status bar
			setImage({ ID_NEW_STANDARD_SESSION_HELP_WHAT_CAN_I_DO }, IDB_NEW_SESSION_MENU_WHAT_CAN_I_DO_16X16, xtpImageNormal);
		}

		{
			auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_NEW_STANDARD_SESSION_REPROVIDE_CONSENT);
			menuItem->SetCaption(YtriaTranslate::Do(MainFrame_FillSessionGroup_7, _YLOC("Re-provide consent to the application")).c_str());
			menuItem->SetTooltip(YtriaTranslate::Do(MainFrame_FillSessionGroup_8, _YLOC("Re-provide consent to the application")).c_str());
			menuItem->SetDescription(YtriaTranslate::Do(MainFrame_FillSessionGroup_9, _YLOC("Re-provide consent to the application")).c_str()); // show in status bar
			setImage({ ID_NEW_STANDARD_SESSION_REPROVIDE_CONSENT }, IDB_NEW_SESSION_MENU_REPROVIDE_CONSENT_16X16, xtpImageNormal);
		}
    }

    // Advanced session
    ctrl = groupConnect->Add(xtpControlSplitButtonPopup, ID_NEW_ADVANCED_SESSION);
	setControlTexts(ctrl,
		YtriaTranslate::Do(MainFrame_FillSessionGroup_10, _YLOC("New Advanced Session")).c_str(),
		YtriaTranslate::Do(MainFrame_FillSessionGroup_11, _YLOC("New Advanced Session")).c_str(),
		YtriaTranslate::Do(MainFrame_FillSessionGroup_12, _YLOC("Additional access (according to user rights):\n    - All users' directory properties/ info\n    - Memberships of hidden groups\n    - Group conversations and extended group information\n    - All calendar events for public groups and private you have access to\n    - All drive items you have access to through groups, sites, or user sharing\n\nAdmin consent is required.\n\n                                        --------------------------------------------------\n\nA note about elevated privileges:\nAdding elevated privileges to an Advanced session will grant access to all personal data for calendars, mailboxes, drive items, etc...")).c_str());

    popup = dynamic_cast<CXTPControlPopup*>(ctrl);
    ASSERT(nullptr != popup);
    if (nullptr != popup)
    {
		{
			auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ctrl->GetID());
			menuItem->SetCaption(ctrl->GetCaption());
			menuItem->SetDescription(ctrl->GetDescription());
		}

		{
			auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_NEW_ADVANCED_USER_SESSION_HELP_WHAT_CAN_I_DO);
			menuItem->SetCaption(YtriaTranslate::Do(MainFrame_FillSessionGroup_13, _YLOC("Learn more about Advanced sessions")).c_str());
			menuItem->SetTooltip(YtriaTranslate::Do(MainFrame_FillSessionGroup_14, _YLOC("Learn more about Advanced sessions")).c_str());
			menuItem->SetDescription(YtriaTranslate::Do(MainFrame_FillSessionGroup_15, _YLOC("Learn more about Advanced sessions")).c_str()); // show in status bar
			setImage({ ID_NEW_ADVANCED_USER_SESSION_HELP_WHAT_CAN_I_DO }, IDB_NEW_SESSION_MENU_WHAT_CAN_I_DO_16X16, xtpImageNormal);
		}

		{
			auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_NEW_ADVANCED_USER_SESSION_GIVE_ADMIN_CONSENT);
			menuItem->SetCaption(YtriaTranslate::Do(MainFrame_FillSessionGroup_16, _YLOC("Provide admin consent for your tenant")).c_str());
			menuItem->SetTooltip(YtriaTranslate::Do(MainFrame_FillSessionGroup_17, _YLOC("Provide admin consent for your tenant")).c_str());
			menuItem->SetDescription(YtriaTranslate::Do(MainFrame_FillSessionGroup_18, _YLOC("Provide admin consent for your tenant")).c_str()); // show in status bar
			setImage({ ID_NEW_ADVANCED_USER_SESSION_GIVE_ADMIN_CONSENT }, IDB_NEW_SESSION_MENU_GIVE_ADMIN_CONSENT_16X16, xtpImageNormal);
		}

		{
			auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_ULTRAADMIN_REQUEST_ACCESS_PROTECTED_API);
			menuItem->SetCaption(_T("Request Access (Private Channels chats)"));
			menuItem->SetTooltip(_T("Request Access (Private Channels chats)"));
			menuItem->SetDescription(_T("Request Access (Private Channels chats)")); // show in status bar
			setImage({ ID_ULTRAADMIN_REQUEST_ACCESS_PROTECTED_API }, IDB_ULTRAADMIN_REQUEST_ACCESS_PROTECTED_API_16X16, xtpImageNormal);
		}

		//{
		//	auto menuItem = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_NEW_ADVANCED_USER_SESSION_SEND_EMAIL_TO_GET_CONSENT);
		//	menuItem->SetCaption(_TLOC("Send email to your Admin to get Admin Consent"));
		//	menuItem->SetTooltip(_TLOC("Send email to your Admin to get Admin Consent"));
		//	menuItem->SetDescription(_TLOC("Description needed")); // show in status bar
		//	setImage({ ID_NEW_ADVANCED_USER_SESSION_SEND_EMAIL_TO_GET_CONSENT }, IDB_NEW_SESSION_MENU_SEND_EMAIL_TO_ADMIN_16X16, xtpImageNormal);
		//}
    }

    ctrl = groupConnect->Add(xtpControlButton, ID_SELECT_SESSION);
	setControlTexts(ctrl,
		YtriaTranslate::Do(MainFrame_FillSessionGroup_30, _YLOC("Select Session")).c_str(),
		YtriaTranslate::Do(MainFrame_FillSessionGroup_31, _YLOC("Select Session")).c_str(),
		YtriaTranslate::Do(MainFrame_FillSessionGroup_32, _YLOC("Choose from a list of recently used sessions, or create a new session.")).c_str());
}

const wstring MainFrame::getFrameTitle() const
{
	wstring title;

	title += AutomatedApp::GetTitleWithVersion(GetLicenseContext());

	return title;
}

void MainFrame::refreshFrameTitle()
{
	const wstring prefix = Sapio365Session::IsDumpGloballyEnabled() ? (g_DumpModeText + _YTEXT(" | ")) : _YTEXT("");
	SetWindowText((prefix + getFrameTitle()).c_str());
}

PersistentSession MainFrame::PopulateRecentSessions()
{
	// Show waiting cursor as reading all the session files from the disk can be a bit long.
	CWaitCursor _;

    boost::YOpt<PersistentSession> lastSessionUsed;

	ASSERT(nullptr != m_connectTab);
	if (nullptr != m_connectTab)
	{
		auto availableSessionGroup = findGroup(*m_connectTab, g_AvailableSessionGroupName.c_str());
		if (nullptr != availableSessionGroup)
		{
			CXTPControlGallery* gallery = dynamic_cast<CXTPControlGallery*>(availableSessionGroup->GetAt(0));
			if (gallery == nullptr || gallery->GetID() != ID_LOAD_SESSION)
			{
				gallery = dynamic_cast<CXTPControlGallery*>(availableSessionGroup->Add(xtpControlGallery, ID_LOAD_SESSION, nullptr, 0));
				ASSERT(gallery != nullptr && gallery->GetID() == ID_LOAD_SESSION);

				if (nullptr != gallery)
				{
					gallery->SetStyle(xtpButtonIconAndCaption);
					gallery->SetItems(CXTPControlGalleryItems::CreateItems(GetCommandBars(), ID_LOAD_SESSION));

					{
						auto ctrl = availableSessionGroup->Add(xtpControlButton, ID_LOAD_SESSION_GALLERY_RESIZE_S);
						setImage({ ID_LOAD_SESSION_GALLERY_RESIZE_S }, IDB_LOAD_SESSION_GALLERY_RESIZE_S_16X16, xtpImageNormal);
						setControlTexts(ctrl, _YTEXT(""), _YTEXT("Narrow"), _YTEXT("Make Recent Sessions list narrower"));
						ctrl->SetStyle(xtpButtonIcon);
						ctrl->SetFlags(xtpFlagManualUpdate);
					}
					{
						auto ctrl = availableSessionGroup->Add(xtpControlButton, ID_LOAD_SESSION_GALLERY_RESIZE_M);
						setImage({ ID_LOAD_SESSION_GALLERY_RESIZE_M }, IDB_LOAD_SESSION_GALLERY_RESIZE_M_16X16, xtpImageNormal);
						setControlTexts(ctrl, _YTEXT(""), _YTEXT("Default"), _YTEXT("Restore Recent Sessions list default size"));
						ctrl->SetStyle(xtpButtonIcon);
						ctrl->SetFlags(xtpFlagManualUpdate);
					}
					{
						auto ctrl = availableSessionGroup->Add(xtpControlButton, ID_LOAD_SESSION_GALLERY_RESIZE_L);
						setImage({ ID_LOAD_SESSION_GALLERY_RESIZE_L }, IDB_LOAD_SESSION_GALLERY_RESIZE_L_16X16, xtpImageNormal);
						setControlTexts(ctrl, _YTEXT(""), _YTEXT("Wide"), _YTEXT("Make Recent Sessions list wider"));
						ctrl->SetStyle(xtpButtonIcon);
						ctrl->SetFlags(xtpFlagManualUpdate);
					}
				}
			}

			auto items = gallery->GetItems();
			ASSERT(nullptr != items);
			if (nullptr != items)
			{
				auto imManag = items->GetImageManager();
				ASSERT(nullptr != imManag);
				if (nullptr != imManag)
				{

					for (UINT imageId
						: { IDB_ICON_STANDARD_SESSION
						,	IDB_ICON_STANDARD_SESSION_ACTIVE
						,	IDB_ICON_ADVANCED_SESSION
						,	IDB_ICON_ADVANCED_SESSION_ACTIVE
						,	IDB_ICON_ULTRAADMIN_SESSION
						,	IDB_ICON_ULTRAADMIN_SESSION_ACTIVE
						,	IDB_ICON_ROLE_SESSION
						,	IDB_ICON_ROLE_SESSION_ACTIVE
						,	IDB_ICON_ROLE_ENFORCED_SESSION
						,	IDB_ICON_ROLE_ENFORCED_SESSION_ENFORCED
						,	IDB_ICON_ELEVATED_SESSION
						,	IDB_ICON_ELEVATED_SESSION_ACTIVE
						,	IDB_ICON_PARTNER_SESSION
						,	IDB_ICON_PARTNER_SESSION_ACTIVE
						,	IDB_ICON_PARTNER_ELEVATED_SESSION
						,	IDB_ICON_PARTNER_ELEVATED_SESSION_ACTIVE
							})
					{
						UINT commands[]{ imageId };
						imManag->SetIcons(imageId, commands, 1, CSize(0, 0));
					}
				}

				items->RemoveAll();
				m_SessionIdentifiers.clear();

				auto sessions = SessionsSqlEngine::Get().GetList();

				if (!CRMpipe().IsFeatureSandboxed())
				{
					auto isUltraAdmin = [](const PersistentSession& p_Session) {
						return p_Session.GetSessionType() == SessionTypes::g_UltraAdmin;
					};
					sessions.erase(std::remove_if(sessions.begin(), sessions.end(), isUltraAdmin), sessions.end());
				}

				std::sort(sessions.begin(), sessions.end(), UISessionListSorter());
				for (auto& session : sessions)
				{
                    int icon = session.GetSessionTypeResourcePng();

                    CXTPControlGalleryItem* item = nullptr;
					if (session.GetRoleId() == SessionIdentifier::g_NoRole)
                    {
                        item = items->AddItem(session.GetSessionName().c_str(), icon);
                    }
                    else
                    {
						const auto roleName = session.GetRoleNameInDb();

						if (RoleDelegationManager::GetInstance().IsRoleEnforced(session.GetRoleId(), GetSapio365Session()))
							icon = IDB_ICON_ROLE_ENFORCED_SESSION;

                        item = items->AddItem(
                            _YTEXTFORMAT(L"[%s] %s",
                                roleName.c_str(),
                                session.GetSessionName().c_str()).c_str(), 
                            icon);
                    }

                    const auto res = m_SessionIdentifiers.insert(session.GetIdentifier());
                    ASSERT(res.first != m_SessionIdentifiers.end());
                    if (res.first != m_SessionIdentifiers.end())
                        item->SetData(reinterpret_cast<DWORD_PTR>(&(*res.first)));
					
					const auto status = session.GetStatus(session.GetRoleId());
					if (status == SessionStatus::ACTIVE)
					{
						// Change image
						switch (session.GetSessionTypeResourcePng())
						{
						case IDB_ICON_STANDARD_SESSION:
							item->SetImageIndex(IDB_ICON_STANDARD_SESSION_ACTIVE);
							break;
						case IDB_ICON_ADVANCED_SESSION:
							item->SetImageIndex(IDB_ICON_ADVANCED_SESSION_ACTIVE);
							break;
						case IDB_ICON_ULTRAADMIN_SESSION:
							item->SetImageIndex(IDB_ICON_ULTRAADMIN_SESSION_ACTIVE);
							break;
                        case IDB_ICON_ROLE_SESSION:
							item->SetImageIndex(RoleDelegationManager::GetInstance().IsRoleEnforced(session.GetRoleId(), GetSapio365Session()) ? IDB_ICON_ROLE_ENFORCED_SESSION_ENFORCED : IDB_ICON_ROLE_SESSION_ACTIVE);
							break;
						case IDB_ICON_ELEVATED_SESSION:
							item->SetImageIndex(IDB_ICON_ELEVATED_SESSION_ACTIVE);
							break;
						case IDB_ICON_PARTNER_SESSION:
							item->SetImageIndex(IDB_ICON_PARTNER_SESSION_ACTIVE);
							break;
						case IDB_ICON_PARTNER_ELEVATED_SESSION:
							item->SetImageIndex(IDB_ICON_PARTNER_ELEVATED_SESSION_ACTIVE);
							break;
						default:
							ASSERT(false);
						}
					}

                    if (GetSessionInfo() == session)
						item->SetEnabled(FALSE);

					// First available cached session is the one we want to reload at startup
					/*if (status == SessionStatus::CACHED && !lastSessionUsed)
						lastSessionUsed = session;*/
					// As default sorting prioritize favorite sessions, we have to compare dates to find the most recent cached session
					if (status == SessionStatus::CACHED)
					{
						YTimeDate thisSessionTimeLastUsed = session.GetLastUsedOn();

						if (!lastSessionUsed ||
							thisSessionTimeLastUsed > lastSessionUsed->GetLastUsedOn() ||
							(session.GetRoleId() > SessionIdentifier::g_NoRole&& thisSessionTimeLastUsed == lastSessionUsed->GetLastUsedOn())) // Prioritize role if equal
							lastSessionUsed = session;
					}
				}

				resizeSessionGallery(*MainFrameSessionListSizeSetting().Get(), false);
				gallery->InvalidateItems();
			}
		}
	}

    return lastSessionUsed ? *lastSessionUsed : PersistentSession();
}

void MainFrame::enableUITheme()
{
	// Prof-UIs most matching theme
	// This is the default in AutomatedApp, but we force it as we don't want any registry hack to change it.
	g_PaintManager.InstallPaintManager(RUNTIME_CLASS(CExtPaintManagerOffice2013_OrangeWhite));

	XTPBackImage backImage = XTPNone;

	// Random ribbon back image for testing!
	srand((unsigned)time(nullptr));
	backImage = XTPBackImage((rand() % 14) + 1);

	SetTheme(CodeJockAppTheme::themeYtriaPurple, backImage);
	EnableShadow(true);
}

wstring MainFrame::getPositionAndSizeRegistrySectionName() const
{
	return _YTEXT("");
}

bool MainFrame::IsDebugMode()
{
	return CRMpipe().IsFeatureSandboxed() && m_DebugMode;
}

bool MainFrame::IsSyncInProgresss() const
{
	return m_SyncInProgress;
}

void MainFrame::logout()
{
	if (IDOK == YCodeJockMessageBox(this,
									DlgMessageBox::eIcon_Question,
									YtriaTranslate::Do(MainFrame_logout_1, _YLOC("Sign out")).c_str(),
									YtriaTranslate::Do(MainFrame_logout_2, _YLOC("Do you really want to close the current session?")).c_str(),
									YtriaTranslate::Do(MainFrame_logout_3, _YLOC("Your password for this session will be lost. You will need to reenter it when signing in.")).c_str(),
									{ { IDOK, YtriaTranslate::Do(MainFrame_logout_4, _YLOC("Ok")).c_str() }, { IDCANCEL, YtriaTranslate::Do(MainFrame_logout_5, _YLOC("Cancel")).c_str() } }
									).DoModal())
	{
		trackSessionUsageStop();

		Sapio365Session::SignOut(GetSapio365Session(), this);
	}
}

wstring MainFrame::getInitFrameTitle() const
{
	return getFrameTitle();
}

AutomatedApp* MainFrame::GetAutomatedApp() const
{
	return reinterpret_cast<Office365AdminApp*>(AfxGetApp());
}

const bool MainFrame::HasModifiedItems()
{
	return false;
}

CacheGrid* MainFrame::GetGrid(const wstring& i_GridName)
{
	ASSERT(false);
	return nullptr;
}

AutomatedApp::AUTOMATIONSTATUS MainFrame::automationProcessOption(AutomationAction* i_Action)
{
	ASSERT(false);
	return AutomatedApp::AUTOMATIONSTATUS_OK;
}

void MainFrame::automationSave(AutomationAction* i_Action)
{
	ASSERT(false);
}

void MainFrame::automationSaveSelected(AutomationAction* i_Action)
{
	ASSERT(false);
}

bool MainFrame::IsThreadRunning()
{
	return IsSyncInProgresss();
}

CacheGrid* MainFrame::GetMainGrid()
{
	// No main grid in Office365Admin
	return nullptr;
}

void MainFrame::initJobCenter()
{
	if (IsDebugMode())
		m_AutomationWizard = std::make_unique<AutomationWizardO365Folder>(_YTEXT("Automation\\MainFrame"));

	if (!m_AutomationWizard)
		m_AutomationWizard = std::make_unique<AutomationWizardMainFrame>();

	if (m_AutomationWizard)
		m_AutomationWizard->SetFrame(this);
}

AutomationWizardO365* MainFrame::GetAutomationWizard() const
{
	return m_AutomationWizard.get();
}

void MainFrame::SetupElevatedSessionFromAdvancedSession(std::shared_ptr<Sapio365Session> p_Session, PersistentSession p_PersistentSession, const wstring& p_AppId, const wstring& p_AppClientSecret, const std::shared_ptr<MSGraphSession>& p_AppSession, CFrameWnd* p_FrameSource)
{
	ASSERT(p_PersistentSession.IsAdvanced() || p_PersistentSession.IsPartnerAdvanced());
	SaveAsElevatedSession(p_PersistentSession, p_AppId, p_AppClientSecret);
	p_Session->GetGateways().SetUserGraphSession(p_Session->GetMainMSGraphSession());
	p_Session->GetGateways().SetAppGraphSession(p_AppSession);
	p_Session->GetGateways().SetAppGraphSessionCredentials(p_AppId, p_AppClientSecret);
	p_Session->GetGateways().SetTenant(p_PersistentSession.GetTenantName());
	p_Session->SetSessionType(p_PersistentSession.GetSessionType());
	Sapio365Session::SetSignedIn(p_Session, p_FrameSource);
}

void MainFrame::InitApp()
{
	initJobCenter();
}

void MainFrame::LoadXMLSelectionSpecific(const wstring& i_XmlFilePath)
{
	ASSERT(false);
}

void MainFrame::GetItemsToAddToFavoriteList(set<wstring>& items)
{
	ASSERT(false);
}

void MainFrame::OnUpdateFavoriteAddToList(CCmdUI* pCmdUI)
{
	ASSERT(false);
	pCmdUI->Enable(FALSE);
}

void MainFrame::ProcessFavoriteOrRecent(const wstring& favoriteOrRecent)
{
	ASSERT(false);
}

void MainFrame::anotherInstanceTriedToStart()
{
	ShowWindow(SW_SHOW);
	if (IsIconic())
		ShowWindow(SW_RESTORE);
	SetForegroundWindow();
}

DlgBrowser* MainFrame::getOauth2Browser() const
{
    return m_oauth2Browser;
}

void MainFrame::setOauth2Browser(DlgBrowser* val)
{
    m_oauth2Browser = val;
}

CXTPRibbonGroup* MainFrame::getSessionGroup()
{
	return nullptr != m_connectTab	? findGroup(*m_connectTab, g_InfoSessionGroup.c_str())
									: nullptr;
}

bool MainFrame::sessionWasSet(bool p_SessionHasChanged)
{
	ASSERT(m_HtmlView);
	m_HtmlView->SetReadyForExecuteScript();

	if (HasSession() && (GetSessionInfo().IsUltraAdmin() || GetSapio365Session()->IsUseRoleDelegation() || GetSapio365Session()->IsPartnerAdvanced() || GetSapio365Session()->IsPartnerElevated()))
		setDisplayMode(DisplayMode::ADMIN);
	else
		setDisplayMode(DisplayMode::USER);

	if (CRMpipe().IsFeatureSandboxed())
		m_HtmlView->EnableOnPremise();

	auto sessionGroup = getSessionGroup();
	if (nullptr != sessionGroup)
		updateSessionGroup(*sessionGroup);
	PopulateRecentSessions();

	if (p_SessionHasChanged && HasSession())
		updateLicenseContext();

	if (HasSession())
		MainFrameSessionSetting().Set(GetSapio365Session()->GetIdentifier());
	else
		MainFrameSessionSetting().Reset();

	if (HasSession())
	{
		const bool hasConnectedUser = !GetSapio365Session()->IsUltraAdmin() && !GetSapio365Session()->IsPartnerAdvanced() && !GetSapio365Session()->IsPartnerElevated();
		UpdateCanOpenWithRole(hasConnectedUser ? GetSapio365Session()->GetConnectedUserID() : _YTEXT(""), GetSapio365Session()->GetSessionType(), GetSapio365Session()->GetRoleDelegationID());
	}
	else
		UpdateCanOpenWithRole(_YTEXT(""), _YTEXT(""), SessionIdentifier::g_NoRole);

	ASSERT(nullptr != m_RoleInfoControl);
	if (nullptr != m_RoleInfoControl)
		m_RoleInfoControl->SetVisible(HasSession() && GetSapio365Session()->IsUseRoleDelegation());

	updateEditElevatedButton();
	updateConnectToCustomerButton();

	ASSERT(nullptr != m_RoleSelectControl);
	if (nullptr != m_RoleSelectControl)
	{
		m_RoleSelectControl->SetVisible(HasSession() && (GetSapio365Session()->IsStandard() || GetSapio365Session()->IsRole()));
		m_RoleSelectControl->SetEnabled(m_CanOpenWithRole);
	}

	updateLogoutControlTexts();

	if (HasSession() && GetSapio365Session()->IsUseRoleDelegation())
	{
		auto role = RoleDelegationManager::GetInstance().GetDelegation(GetSapio365Session()->GetRoleDelegationID(), GetSapio365Session());
		if (role.IsLogSessionLogin())
			ActivityLogger::GetInstance().LogBasicAction(_YFORMAT(L"Use role: %s", role.m_Name.c_str()), _T("OK"), _T("Session"), GetSapio365Session());
	}

	auto licenseManager = getLicenseManager();
	ASSERT(licenseManager);
	if (licenseManager)
	{
		const bool hasConnectedUser = HasSession() && !GetSapio365Session()->IsUltraAdmin() && !GetSapio365Session()->IsPartnerAdvanced() && !GetSapio365Session()->IsPartnerElevated();
		if (HasSession() && hasConnectedUser)
		{
			wstring loadedLicense;
			licenseManager->LoadDefaultLicense(GetSapio365Session()->GetConnectedUserPrincipalName(), loadedLicense);// from server
			// TODO test license changed and if so, reload job center UI HTML
		}
	}

	refreshFrameTitle();

	temporaryChangeStatusbarBackgroundColor(Sapio365Session::IsDumpGloballyEnabled(), RGB(255, 0, 0));
	temporarySetStatusbarLeftText(Sapio365Session::IsDumpGloballyEnabled(), g_DumpModeText, g_DumpModeTooltip);

	sessionWasSetHMTLupdate();

	return true;
}

void MainFrame::sessionWasSetHMTLupdate()
{
	ASSERT(m_HtmlView);
	if (m_HtmlView && m_HtmlView->HasNoJob())
		HTMLRefreshAll();
	else
	{
		HTMLUpdateJobs();
		HTMLUpdateAJL();
	}

	HTMLUpdateAJLcanRemoveJobs();
}

void MainFrame::setDisplayMode(DisplayMode p_DisplayMode)
{
	switch (p_DisplayMode)
	{
	case MainFrame::DisplayMode::USER:
        m_HtmlView->EnableUserMode();
		break;
	case MainFrame::DisplayMode::ADMIN:
        m_HtmlView->EnableAdminMode();
		break;
	default:
		ASSERT(false);
		break;
	}
}

void MainFrame::updateConnectToCustomerButton()
{
	auto session = GetSapio365Session();
	m_HasContracts = session
		&& (session->IsAdvanced() || session->IsElevated())
		&& session->HasContracts();

	if (nullptr != m_ConnectToCustomerControl)
		m_ConnectToCustomerControl->SetVisible(m_HasContracts);
}

void MainFrame::UpdateSessionNSATracking()
{
	if (GetSapio365Session())
	{
		static const wstring g_SessionTypeName = _YTEXT("Name");

		trackSessionUsageStop();

		m_LoginSessionTrackName =
			GetSessionInfo().IsStandard() ? LicenseUtil::g_codeSapio365sessionTypeStandard :
			GetSessionInfo().IsAdvanced() ? LicenseUtil::g_codeSapio365sessionTypeAdvanced :
			GetSessionInfo().IsUltraAdmin() ? LicenseUtil::g_codeSapio365sessionTypeUltraAdmin :
			GetSessionInfo().IsRole() ? LicenseUtil::g_codeSapio365sessionTypeRole :
			GetSessionInfo().IsElevated() ? LicenseUtil::g_codeSapio365sessionTypeElevated :
			GetSessionInfo().IsPartnerAdvanced() ? LicenseUtil::g_codeSapio365sessionTypePartnerAdvanced :
			GetSessionInfo().IsPartnerElevated() ? LicenseUtil::g_codeSapio365sessionTypePartnerElevated :
			LicenseUtil::g_codeSapio365sessionTypeFreeRider;
		ASSERT(m_LoginSessionTrackName != LicenseUtil::g_codeSapio365sessionTypeFreeRider);// hacked?

		LicenseManager::TrackStart(m_LoginSessionTrackName);
	}
}

void MainFrame::SetBackstagePanelSessions(BackstagePanelSessions* p_BackstagePanelSessions)
{
	m_BackstagePanelSessions = p_BackstagePanelSessions;
}

void MainFrame::NotifyBackstagePanelSessions()
{
	if (nullptr != m_BackstagePanelSessions)
		m_BackstagePanelSessions->SessionChanged();
}

void MainFrame::GiveConsentForApp(const wstring& p_Tenant, const wstring& p_AppID, CWnd* p_Parent)
{
	if (Util::ShowRestrictedAccessDialog(
			DlgRestrictedAccess::CONSENT_REMINDER
			, { YtriaTranslate::Do(DlgRoleEditKey_OnNewPostedURL_1, _YLOC("You will be asked to log-in and give 'app consent'.")).c_str()
			, YtriaTranslate::Do(Office365AdminApp_createUserSession_2, _YLOC("Rest assured that your information will never pass through any external servers, and will not be accessible to any other instances of sapio365.")).c_str()
			, YtriaTranslate::Do(MainFrame_GiveUltraAdminConsent_3, _YLOC("Note that this consent will only be for the selected application.")).c_str()
			, YtriaTranslate::Do(DlgRoleEditKey_OnNewPostedURL_4, _YLOC("(more...)")).c_str()
			, Product::getURLHelpWebSitePage(_YTEXT("Help/give-consent")) }
			, NoSetting<bool>() /// FIXME -- TO set
			, p_Parent
		))
	{
		bool done = false;
		while (!done)
		{
			DlgTenantInfo dlg(p_Tenant, p_AppID, SessionTypes::GetDefaultUltraAdminConsentRedirectUri(), p_Parent);
			if (dlg.DoModal() == IDOK)
			{
				auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
				ASSERT(nullptr != app);
				if (nullptr != app)
				{
					TraceIntoFile::trace(_YDUMP("MainFrame"), _YDUMP("GiveUltraAdminConsent"), _YDUMPFORMAT(L"Getting Ultra Admin consent for %s - %s - %s", dlg.GetTenantName().c_str(), dlg.GetAppId().c_str(), dlg.GetRedirectURL().c_str()));
					// FIXME: We should use the logger service.
					// We need to rework logger channels to allow trace-only logging.
					//LoggerService::Debug(YtriaTranslate::Do(MainFrame_GiveUltraAdminConsent_5, _YLOC("Getting Ultra Admin consent for %1 - %2 - %3"), dlg.GetTenantName().c_str(), dlg.GetAppId().c_str(), dlg.GetRedirectURL().c_str()));

					done = app->GetConsent(SessionTypes::GetUltraAdminConsentUrl(dlg.GetTenantName(), dlg.GetAppId(), dlg.GetRedirectURL()).to_uri(), SessionTypes::g_UltraAdmin, p_Parent, _T("App administrator consent - application for elevated privileges"));
				}
				/*When done is true, it only means the listener has been created and the browser will open. It doesn't mean consent if effectively given.*/
			}
			else
			{
				done = true;
			}
		}
	}
}

bool MainFrame::WaitSyncCacheEvent() const
{
	return WAIT_OBJECT_0 == ::WaitForSingleObject(m_SyncCacheEvent, INFINITE);
}

std::pair<bool, PersistentSession> MainFrame::IsOverwritingExistingElevated(const wstring& p_SessionBeingLoadedEmail, const wstring& p_SessionBeingLoadedType)
{
	// FIXME and remove
	ASSERT(p_SessionBeingLoadedType != SessionTypes::g_PartnerAdvancedSession);

	std::pair<bool, PersistentSession> retVal;
	retVal.first = false;

	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
	ASSERT(app && app->GetIsCreatingSession());
	if (app && app->GetIsCreatingSession() && p_SessionBeingLoadedType == SessionTypes::g_AdvancedSession)
	{
		auto listUsers = SessionsSqlEngine::Get().GetList();
		auto fullSessionWithSameUser = std::find_if(listUsers.begin(), listUsers.end(), [p_SessionBeingLoadedEmail](const PersistentSession& p_Session) {
			return p_Session.IsElevated() && p_Session.GetEmailOrAppId() == p_SessionBeingLoadedEmail;
		});

		if (fullSessionWithSameUser != listUsers.end())
			retVal.second = *fullSessionWithSameUser;

		retVal.first = fullSessionWithSameUser != listUsers.end();
	}

	return retVal;
}

LRESULT MainFrame::OnProductUpdateAvailable(WPARAM wParam, LPARAM lParam)
{
	wstring* title = (wstring*)wParam;
	wstring* message = (wstring*)lParam;
	showWindowsNotification(nullptr != title ? *title : _YTEXT(""), nullptr != message ? *message : _YTEXT(""), g_UpdateNotificationCode);
	showBackstageMenuUpdateEntry(getRibbonBar(), true);
	if (!m_AlreadyShownMessageBar)
	{
		createProductUpdateMessageBar(_T("Update Available"), *title);
		m_AlreadyShownMessageBar = true;
	}
	return 0;
}

LRESULT MainFrame::OnProductUpdateQuitAndInstall(WPARAM, LPARAM)
{
	m_UpdateOnQuit = true;
	PostMessage(WM_CLOSE);
	return 0;
}

LRESULT MainFrame::OnSetOAuth2Browser(WPARAM wParam, LPARAM lParam)
{
	setOauth2Browser(reinterpret_cast<DlgBrowser*>(wParam));
	return 0;
}

LRESULT MainFrame::OnGetOAuth2Browser(WPARAM wParam, LPARAM lParam)
{
	return reinterpret_cast<LRESULT>(getOauth2Browser());
}

void MainFrame::showWindowsNotification(const wstring& p_Title, const wstring& p_Message, UINT notificationID)
{
	ASSERT(0 == m_CurrentWindowsNotificationID || notificationID == m_CurrentWindowsNotificationID);

	if (notificationID == m_CurrentWindowsNotificationID) // Don't notify twice.
		return;

	m_TrayIcon.ShowIcon();
	m_CurrentWindowsNotificationID = notificationID;
	auto icon = static_cast<HICON>(LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME), IMAGE_ICON, HIDPI_XW(64), HIDPI_YH(64), LR_SHARED));
	m_TrayIcon.ShowBalloonTip(p_Message.c_str(), p_Title.c_str(), NIIF_USER | NIIF_LARGE_ICON, 30, icon);
}

LRESULT MainFrame::OnTrayIconNotify(WPARAM wParam, LPARAM lParam)
{
	if (NIN_BALLOONUSERCLICK == (UINT)lParam && m_CurrentWindowsNotificationID == g_UpdateNotificationCode)
	{
		const bool success = showBackstageTab(ID_PRODUCT_UPDATE);
		ASSERT(success);
	}

	if (NIN_BALLOONUSERCLICK == (UINT)lParam
		|| NIN_BALLOONHIDE == (UINT)lParam
		|| NIN_BALLOONTIMEOUT == (UINT)lParam)
	{
		m_CurrentWindowsNotificationID = 0;
		m_TrayIcon.HideIcon();
	}

	return 1;
}

// =================================================================================================

void MainFrame::OnUpdateLoadUserHtml_debug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsDebugMode());
}

void MainFrame::OnUpdateReloadCurrentHtml_debug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsDebugMode());
}

void MainFrame::OnUpdateReinitHomePage_debug(CCmdUI * pCmdUI)
{
	pCmdUI->Enable(IsDebugMode());
}


void MainFrame::OnUpdateLoadModulesHtml_debug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsDebugMode());
}

void MainFrame::OnUpdateLoadHtmlInDlg_debug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsDebugMode());
}

void MainFrame::OnUpdateSendRulesToClipboard_debug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsDebugMode());
}

void MainFrame::OnUpdateSendSystemVarToClipboard_debug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsDebugMode());
}

void MainFrame::OnUpdateShowRequestTester_debug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsDebugMode() && IsConnected());
	pCmdUI->SetCheck((bool)m_DlgInputRequest);
}

void MainFrame::OnUpdateLogout(CCmdUI* pCmdUI)
{
	YTestCmdUI cmdUI(pCmdUI->m_nID);
	MainFrameBase::OnUpdateLogout(&cmdUI);

	if (!cmdUI.m_szText.IsEmpty())
		pCmdUI->SetText(cmdUI.m_szText);
	pCmdUI->Enable(!IsSyncInProgresss() && cmdUI.m_bEnabled);
}

void MainFrame::OnDebugMode()
{
	if (CRMpipe().IsFeatureSandboxed())
	{
		m_DebugMode = !m_DebugMode;

		ASSERT(nullptr != m_connectTab);
		if (nullptr != m_connectTab)
		{
			auto group = findGroup(*m_connectTab, g_debug_debugGroup.c_str());
			group->SetVisible(m_DebugMode);
		}

		initJobCenter();

		m_DlgInputRequest.reset();
	}
}

void MainFrame::OnUpdateDebugMode(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(CRMpipe().IsFeatureSandboxed() ? TRUE : FALSE);
}

void MainFrame::OnNewStandardSessionHelpWhatCanIDo()
{
	::ShellExecute(NULL, L"open", Product::getURLHelpWebSitePage(_YTEXT("Help/standard-session")).c_str(), NULL, NULL, SW_SHOW);
}

void MainFrame::OnUpdateNewStandardSessionHelpWhatCanIDo(CCmdUI* pCmdUI)
{
    pCmdUI->Enable();
}

void MainFrame::OnNewStandardSessionReprovideConsent()
{
    auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
    ASSERT(nullptr != app);
    if (nullptr != app)
        app->GetConsent(SessionTypes::GetStandardSessionConsentUrl().to_uri(), SessionTypes::g_StandardSession, this);
}

void MainFrame::OnUpdateNewStandardSessionReprovideConsent(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(!IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning());
}

void MainFrame::OnNewAdvancedUserSessionHelpWhatCanIDo()
{
    ::ShellExecute(NULL, L"open", Product::getURLHelpWebSitePage(_YTEXT("Help/advanced-session")).c_str(), NULL, NULL, SW_SHOW);
}

void MainFrame::OnUpdateNewAdvancedUserSessionHelpWhatCanIDo(CCmdUI* pCmdUI)
{
    pCmdUI->Enable();
}

void MainFrame::OnNewAdvancedUserSessionGiveAdminConsent()
{
    auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
    ASSERT(nullptr != app);
    if (nullptr != app)
        app->GetConsent(SessionTypes::GetAdvancedSessionConsentUrl(true).to_uri(), SessionTypes::g_AdvancedSession, this);
}

void MainFrame::OnUpdateNewAdvancedUserSessionGiveAdminConsent(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(!IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning());
}

//void MainFrame::OnNewAdvancedUserSessionSendEmailToGetConsent()
//{
//	CommandDispatcher::GetInstance().Execute(Command(nullptr, Command::ModuleTarget::User, Command::ModuleTask::SendEmailAdvancedSession, CommandInfo(), {}));
//}
//
//void MainFrame::OnUpdateNewAdvancedUserSessionSendEmailToGetConsent(CCmdUI* pCmdUI)
//{
//    pCmdUI->Enable(FALSE);
//}

void MainFrame::OnNewUltraAdminSessionHelpWhatCanIDo()
{
    ::ShellExecute(NULL, L"open", Product::getURLHelpWebSitePage(_YTEXT("Help/ultra-admin-session-what-can-I-do")).c_str(), NULL, NULL, SW_SHOW);
}

void MainFrame::OnUpdateNewUltraAdminSessionHelpWhatCanIDo(CCmdUI* pCmdUI)
{
    pCmdUI->Enable();
}

void MainFrame::OnNewUltraAdminSessionHowToCreateAnApp()
{
	::ShellExecute(NULL, L"open", Product::getURLHelpWebSitePage(_YTEXT("Help/menu-how-to-create-application-ID")).c_str(), NULL, NULL, SW_SHOW);
}

void MainFrame::OnUpdateNewUltraAdminSessionHowToCreateAnApp(CCmdUI* pCmdUI)
{
    pCmdUI->Enable();
}

void MainFrame::OnNewUltraAdminSessionGiveAdminConsent()
{
	ASSERT(GetSapio365Session() && GetSapio365Session()->IsUltraAdmin());
	if (GetSapio365Session() && GetSapio365Session()->IsUltraAdmin())
		GiveConsentForApp(GetSapio365Session()->GetTenantName(true), GetSapio365Session()->GetEmailOrAppId(), this);
}

void MainFrame::OnUpdateNewUltraAdminSessionGiveAdminConsent(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(GetSapio365Session() && GetSapio365Session()->IsUltraAdmin() && !IsSyncInProgresss() && !TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning());
}

void MainFrame::OnUltraAdminRequestAccessProtectedApi()
{
	ASSERT(GetSapio365Session());
	if (GetSapio365Session())
	{
		DlgProtectedApiHTML dlg(GetSapio365Session(), this);
		dlg.DoModal();
	}
}

void MainFrame::OnUpdateUltraAdminRequestAccessProtectedApi(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(GetSapio365Session() && GetSapio365Session()->IsElevated());
}

void MainFrame::OnTokenBuy()
{
	TokenBuy();
}

//void MainFrame::OnNewUltraAdminSessionSendEmailToGetConsent()
//{
//    CommandDispatcher::GetInstance().Execute(Command(nullptr, Command::ModuleTarget::User, Command::ModuleTask::SendEmailUltraAdminSession, CommandInfo(), {}));
//}
//
//void MainFrame::OnUpdateNewUltraAdminSessionSendEmailToGetConsent(CCmdUI* pCmdUI)
//{
//    pCmdUI->Enable(FALSE);
//}

void MainFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// Allow almost any size
	lpMMI->ptMinTrackSize.x = XTP_DPI_X(50);
	lpMMI->ptMinTrackSize.y = XTP_DPI_Y(40);

	/*lpMMI->ptMinTrackSize.x = XTP_DPI_X(1109);
	lpMMI->ptMinTrackSize.y = XTP_DPI_Y(680);*/
}

void MainFrame::OnMessageBarButton()
{
	const bool success = showBackstageTab(ID_PRODUCT_UPDATE);
	ASSERT(success);
	if (success)
		removeMessageBar();
}

void MainFrame::OnAutomationRecorder()
{
//	if (Office365AdminApp::ShowFeatureUnfinishedDialog(this))
		MainFrameBase::OnAutomationRecorder();
}

void MainFrame::OnLoadUserHtml_debug()
{
	ASSERT(IsDebugMode());
	if (IsDebugMode())
	{
		CString aFilesFilter;
		aFilesFilter.Format(_YTEXT("%s (*.html)|*.html"), _YTEXT("HTML Files"));
		YFileDialog fileDialog(TRUE, _YTEXT("html"), nullptr, OFN_HIDEREADONLY | OFN_PATHMUSTEXIST, aFilesFilter, this);

		// If the user chose a valid file
		if (IDOK == fileDialog.DoModal())
		{
			m_UserHTML = fileDialog.GetPathName();
			m_HtmlView->Navigate(m_UserHTML.c_str());
		}
	}
}

void MainFrame::OnReloadCurrentHtml_debug()
{
	ASSERT(IsDebugMode());
	if (IsDebugMode())
		m_HtmlView->Refresh();
}

void MainFrame::OnReinitHomePage_debug()
{
	ASSERT(IsDebugMode());
	if (IsDebugMode())
	{
		m_HtmlView->YLoadFromResource(g_homePageResourceName.c_str());
		m_UserHTML.clear();
	}
}

void MainFrame::OnLoadModulesHtml_debug()
{
	ASSERT(IsDebugMode());
	if (IsDebugMode())
	{
		m_HtmlView->YLoadFromResource(g_debug_homePageResourceName.c_str());
		m_UserHTML.clear();
	}
}

void MainFrame::OnLoadHtmlInDlg_debug()
{
	ASSERT(IsDebugMode());
	if (IsDebugMode())
	{
		CString aFilesFilter;
		aFilesFilter.Format(_YTEXT("%s (*.html)|*.html"), _YTEXT("HTML Files"));
		YFileDialog fileDialog(TRUE, _YTEXT("html"), nullptr, OFN_HIDEREADONLY | OFN_PATHMUSTEXIST, aFilesFilter, this);

		// If the user chose a valid file
		if (IDOK == fileDialog.DoModal())
		{
			const auto userHtml = fileDialog.GetPathName();
			YTestHtmlDlg dlg((LPCTSTR)userHtml, this);
			dlg.DoModal();
		}
	}
}

void MainFrame::OnSendRulesToClipboard_debug()
{
	AutomatedApp::SendAutomationRulesToClipboard();
}

void MainFrame::OnSendSystemVarToClipboard_debug()
{
	theApp.SendAutomationSystemVarToClipboard();
}

void MainFrame::OnShowRequestTester_debug()
{
	if (m_DlgInputRequest)
	{
		m_DlgInputRequest.reset();
	}
	else
	{
		m_DlgInputRequest = std::make_unique<DlgInputRequest>(GetSapio365Session(), _YTEXTFORMAT(L"%s - %s", GetSessionInfo().GetSessionName().c_str(), SessionTypes::GetDisplayedSessionType(GetSessionInfo().GetSessionType()).c_str()), this);
		m_DlgInputRequest->DoModeless();
		m_DlgInputRequest->ShowWindow(SW_SHOW);
	}
}

void MainFrame::showStatusBarProgress(bool p_UseMarquee, const wstring& p_Text)
{
	auto statusBar = getStatusBar();
	ASSERT(nullptr != statusBar);
	if (nullptr != statusBar)
	{
		if (!::IsWindow(m_wndProgCtrl.m_hWnd))
		{
			if (FALSE == m_wndProgCtrl.Create(WS_CHILD | WS_VISIBLE | PBS_SMOOTH,
				CRect(0, 0, 0, 0), statusBar, 0))
			{
				ASSERT(false);
				return;
			}
		}

		if (p_UseMarquee)
		{
			m_wndProgCtrl.ModifyStyle(0, PBS_MARQUEE);
			m_wndProgCtrl.SetMarquee(TRUE, 1);
		}
		else
		{
			m_wndProgCtrl.SetMarquee(FALSE, 1);
			m_wndProgCtrl.ModifyStyle(PBS_MARQUEE, 0);
		}

		auto pPane = statusBar->AddIndicator(IDC_STATUS_BAR_PROGRESS, 1);

		if (nullptr != pPane)
		{
			const int nIndex = statusBar->CommandToIndex(IDC_STATUS_BAR_PROGRESS);
			statusBar->SetPaneWidth(nIndex, XTP_DPI_X(150));

			statusBar->SetPaneStyle(nIndex, statusBar->GetPaneStyle(nIndex) | SBPS_NOBORDERS);
			statusBar->AddControl(&m_wndProgCtrl, IDC_STATUS_BAR_PROGRESS, FALSE);

			m_wndProgCtrl.SetRange(0, 100);
			m_wndProgCtrl.SetPos(0);
			m_wndProgCtrl.SetStep(1);
		}
	}

	SetMessageText(CString(p_Text.c_str()));
}

void MainFrame::hideStatusBarProgress()
{
	auto statusBar = getStatusBar();
	ASSERT(nullptr != statusBar);
	if (nullptr != statusBar)
	{
		const int nIndex = statusBar->CommandToIndex(IDC_STATUS_BAR_PROGRESS);
		if (nIndex > -1)
			statusBar->RemoveAt(nIndex);
	}

	SetMessageText(CString(L""));
}

void MainFrame::DownloadAutomationFileCallback(const wstring& p_Message)
{
	if (p_Message.empty())
		hideStatusBarProgress();
	else
		showStatusBarProgress(true, p_Message);

	// makes status bar update for real.
	UpdateWindow();
}

void MainFrame::OnViewJobCenterGridDebug()
{
	if (IsDebugMode())
	{
		DlgJobCenterGridDebug dlg(this);
		dlg.DoModal();
	}
}

bool MainFrame::createProductUpdateMessageBar(const wstring& title, const wstring& message)
{
#define UPDATE_XAML \
	L"<StackPanel Orientation='Horizontal'>" \
	L"		<TextBlock Padding='10, 0, 0, 0' VerticalAlignment='Center'><Bold>%s</Bold></TextBlock>" \
	L"		<TextBlock Padding='10, 0, 0, 0' VerticalAlignment='Center'>%s</TextBlock>" \
	L"</StackPanel>" \

	CString text;
	text.Format(UPDATE_XAML, title.c_str(), message.c_str());

	return showMessageBar(text, { { ID_MESSAGE_BAR_BUTTON, _T("More info...") } });
}

wstring MainFrame::GetApplicationColor() const
{
	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
	return nullptr != app ? app->GetApplicationColor() : _YTEXT("000000");
}

bool MainFrame::IsLicensePro() const
{
	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
	return nullptr != app && app->IsLicensePro();
}
