#pragma once

#include "DlgBusinessUserEditHTML.h"

class DlgUserPasswordHTML : public BaseDlgBusinessUserEditHTML
{
public:
	DlgUserPasswordHTML(vector<BusinessUser>& p_Users, CWnd* p_Parent);
	virtual ~DlgUserPasswordHTML();

	//Returns true if processed
	virtual bool processPostedDataSpecific(const IPostedDataTarget::PostedData::value_type& p_Value);

	virtual void postProcessPostedData();

	bool ShouldAutogeneratePasswords() const;

protected:
	virtual wstring getDialogTitle() const override;

private:
	virtual void generateJSONScriptData() override;

	bool m_AutoGeneratePasswords;
};
