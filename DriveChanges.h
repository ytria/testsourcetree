#pragma once

#include "BusinessPermission.h"
#include "SubItemKey.h"

class DriveChanges
{
public:
	struct Updates
	{
		vector<BusinessDriveItem> m_ObjectsToUpdate;
		vector<vector<GridBackendField>> m_PrimaryKeys;
	} m_Updates;
    std::vector<SubItemKey> m_PermsToDelete;
    std::vector<std::pair<BusinessPermission, SubItemKey>> m_PermsToUpdate;
};

