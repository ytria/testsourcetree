#pragma once

#include "BusinessUser.h"
#include "GridGroupsCommon.h"
#include "GridTemplateGroups.h"
#include "GridTemplateUsers.h"

class BusinessGroup;
class FrameGroupAuthorsHierarchy;
class GridGroupsAuthorsHierarchy : public GridGroupsCommon
{
public:
	GridGroupsAuthorsHierarchy();
    virtual ~GridGroupsAuthorsHierarchy() override = default;

	void AddGroups(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
	virtual void customizeGrid() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
	virtual void InitializeCommands() override;

	afx_msg void OnAddRejectedToGroups();
	afx_msg void OnAddAcceptedToGroups();
	afx_msg void OnUpdateAddItemsToGroups(CCmdUI* pCmdUI);
	afx_msg void OnRemoveItemsFromGroups();
	afx_msg void OnUpdateRemoveItemsFromGroups(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()

	virtual void addFieldSpecific(GridBackendRow* p_pRow, BusinessObject::UPDATE_TYPE p_UpdateType);

private:

	GridBackendRow* addRow(const BusinessGroup& p_Group, GridUpdater& p_Updater);
	GridBackendRow* addRowGroupMembersHierarchy(GridBackendRow* p_ParentRow, bool p_TopLevelIsCanceled, const BusinessGroup& p_Group, const bool p_Accepted, GridUpdater& p_Updater);
	void setRowAuhorization(GridBackendRow* p_ChildRow, const bool p_Accepted);

	GridBackendColumn*				m_ColumnAcceptedStatus;
	FrameGroupAuthorsHierarchy*		m_Frame;
	vector<PooledString>			m_BufferHierarchyGroupIDs;

	int m_IconIDauthorizationRestricted;
	int m_IconIDauthorizationAcceptedAll;
	int m_IconIDauthorizationAccepted;
	int m_IconIDauthorizationRejected;

	static wstring g_AcceptedStatus;
	static wstring g_RejectedStatus;
};
