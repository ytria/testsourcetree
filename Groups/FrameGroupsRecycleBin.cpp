#include "FrameGroupsRecycleBin.h"
#include "ModuleGroup.h"
#include "UpdatedObjectsGenerator.h"

IMPLEMENT_DYNAMIC(FrameGroupsRecycleBin, GridFrameBase)

void FrameGroupsRecycleBin::ShowGroupsRecycleBin(const vector<BusinessGroup>& p_Groups, const vector<BusinessGroup>& p_LoadedMoreGroups, bool p_Refresh, const vector<O365UpdateOperation>& p_UpdateOperations)
{
	if (CompliesWithDataLoadPolicy())
		m_groupsGrid.BuildView(p_Groups, p_LoadedMoreGroups, p_Refresh, p_UpdateOperations);
}

void FrameGroupsRecycleBin::UpdateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh)
{
	m_groupsGrid.UpdateGroupsLoadMore(p_Groups, p_FromRefresh, false);
}

void FrameGroupsRecycleBin::createGrid()
{
	m_AddRBACHideOutOfScope = true;
	BOOL gridCreated = m_groupsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
	ASSERT(gridCreated);
}

bool FrameGroupsRecycleBin::customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(p_Tab);
	CreateRefreshGroup(p_Tab);
	CreateApplyGroup(p_Tab);
	CreateRevertGroup(p_Tab);

	auto editGroup = p_Tab.AddGroup(YtriaTranslate::Do(FrameGroupsRecycleBin_customizeActionsRibbonTab_1, _YLOC("Manage Deleted Groups")).c_str());
	setGroupImage(*editGroup, 0);

	{
		auto ctrl = editGroup->Add(xtpControlButton, ID_GROUPSRECYCLEBIN_RESTORE);
		GridFrameBase::setImage({ ID_GROUPSRECYCLEBIN_RESTORE }, IDB_RESTORE, xtpImageNormal);
		GridFrameBase::setImage({ ID_GROUPSRECYCLEBIN_RESTORE }, IDB_RESTORE_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	{
		auto ctrl = editGroup->Add(xtpControlButton, ID_GROUPSRECYCLEBIN_HARDDELETE);
		GridFrameBase::setImage({ ID_GROUPSRECYCLEBIN_HARDDELETE }, IDB_HARDDELETE, xtpImageNormal);
		GridFrameBase::setImage({ ID_GROUPSRECYCLEBIN_HARDDELETE }, IDB_HARDDELETE_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}
	
	{
		CXTPRibbonGroup* dataGroup = findGroup(p_Tab, g_ActionsDataGroup.c_str());
		ASSERT(nullptr != dataGroup);
		if (nullptr != dataGroup)
		{
			auto ctrl = dataGroup->Add(xtpControlButton, ID_GROUPSRECYCLEBIN_LOADMORE);
			GridFrameBase::setImage({ ID_GROUPSRECYCLEBIN_LOADMORE }, IDB_LOADMORE, xtpImageNormal);
			GridFrameBase::setImage({ ID_GROUPSRECYCLEBIN_LOADMORE }, IDB_LOADMORE_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	automationAddCommandIDToGreenLight(ID_GROUPSRECYCLEBIN_RESTORE);
	automationAddCommandIDToGreenLight(ID_GROUPSRECYCLEBIN_HARDDELETE);

	return true;
}

O365Grid& FrameGroupsRecycleBin::GetGrid()
{
	return m_groupsGrid;
}

void FrameGroupsRecycleBin::ApplyAllSpecific()
{
	ApplySpecific(false);
}

void FrameGroupsRecycleBin::ApplySpecific(bool p_Selected)
{
	UpdatedObjectsGenerator<BusinessGroup> updatedUsersGenerator;
	updatedUsersGenerator.BuildUpdatedObjectsRecycleBin(GetGrid(), p_Selected);

	CommandInfo info;
	info.Data() = updatedUsersGenerator;
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateRecycleBin, info, { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
}

void FrameGroupsRecycleBin::ApplySelectedSpecific()
{
	ApplySpecific(true);
}

bool FrameGroupsRecycleBin::HasApplyButton()
{
	return true;
}

void FrameGroupsRecycleBin::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.SetOrigin(GetModuleCriteria().m_Origin);
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	if (HasLastUpdateOperations() && !ShouldForceFullRefreshAfterApply())
	{
		O365IdsContainer ids;
		for (const auto& updateOp : GetLastUpdateOperations())
			ids.insert(updateOp.GetPkFieldStr(m_groupsGrid.GetColId()));

		info.GetIds() = ids;
	}

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::ListRecycleBin, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

AutomatedApp::AUTOMATIONSTATUS FrameGroupsRecycleBin::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionSelectedDelete(p_Action))
	{
		p_CommandID = ID_GROUPSRECYCLEBIN_HARDDELETE;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedRestore(p_Action))
	{
		p_CommandID = ID_GROUPSRECYCLEBIN_RESTORE;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}

	return statusRV;
}
