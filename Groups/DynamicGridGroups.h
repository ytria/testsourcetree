#pragma once

#include "YTriaDynamicGrid.h"

class DynamicGridGroups : public YtriaDynamicGrid
{
public:
    DynamicGridGroups();
    virtual ~DynamicGridGroups() override = default;
};