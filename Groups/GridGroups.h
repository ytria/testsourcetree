#pragma once

#include "BaseO365Grid.h"
#include "BusinessGroup.h"
#include "BusinessGroupSetting.h"
#include "BusinessLifeCyclePolicies.h"
#include "GridTemplateGroups.h"
#include "GridUpdater.h"
#include "OnPremiseGroup.h"

class FrameGroups;

using GridGroupsBaseClass = ModuleO365Grid<BusinessGroup>;
class GridGroups : public GridGroupsBaseClass
{
public:
    GridGroups();

	virtual void	AddCreatedBusinessObjects(const vector<BusinessGroup>& p_Groups, bool p_ScrollToNew) override;
	virtual void	UpdateBusinessObjects(const vector<BusinessGroup>& p_Groups, bool p_SetModifiedStatus) override;
	virtual void	RemoveBusinessObjects(const vector<BusinessGroup>& p_Groups) override;
	void UpdateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh, bool p_DoNotShowLoadingErrors);

	void BuildView(const vector<BusinessGroup>& p_Groups, const vector<BusinessGroup>& p_LoadedMoreGroups, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_AllowFullPurge);
    vector<BusinessGroup> GetLoadMoreRequestInfo(const std::vector<GridBackendRow *>& p_Rows);
	vector<BusinessGroup> GetLoadMoreRequestInfo(const std::vector<GridBackendRow*>& rowsWithMoreLoaded, YtriaTaskData p_TaskData);
	bool CanRefreshOnPrem() const;

	void UpdateGroupLCPStatus(const vector<BusinessGroup>& p_Groups);
	bool SetLCP(const BusinessLifeCyclePolicies& p_LCP, bool p_RefreshGrid); // return true if status has changed

	virtual row_pk_t UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody) override;

	void GetSelectedGroupsForActionOnLCP(vector<BusinessGroup>& p_ListO365Groups, vector<row_pk_t>& p_RowPKs, BusinessObject::UPDATE_TYPE p_UpdateType = BusinessObject::UPDATE_TYPE_NONE);

	void ShowOnPremiseGroups(const vector<OnPremiseGroup>& p_Groups);
	void SetOnPremiseGroupsUpdateResult(const vector<O365UpdateOperation>& p_UpdateResults);

	virtual bool IsRowForLoadMore(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

	bool HasSelectedRowWithPolicyChangedStatus() const;
	bool HasSelectedRowReadyForSelectedPolicyUpdate(BusinessObject::UPDATE_TYPE p_UpdateType) const;

	bool HasNonOffice365GroupsMarkedForDeletion(bool p_InSelection) const;

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeEdit() const override;
	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeCreate() const override;
	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeDelete() const override;

	void SetTenantGroupSettings(boost::YOpt<std::vector<BusinessGroupSetting>>& p_GroupSettings, bool p_UpdateGrid);
	const boost::YOpt<std::vector<BusinessGroupSetting>>& GetTenantGroupSettings() const;

	const boost::YOpt<Team> GetTeam(const GridBackendRow* p_Row) const;
	bool IsTeamColumn(GridBackendColumn* p_Column) const;
	bool IsArchiveTeamColumn(GridBackendColumn* p_Column) const;
	GridBackendColumn* GetIsTeamColumn() const;

	void SetForceSorting(bool p_ForceSorting);

	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

	struct OnPremiseChanges
	{
		std::vector<std::pair<row_pk_t, OnPremiseGroup>> m_GroupsToUpdate;
	};

	OnPremiseChanges GetOnPremiseChanges(bool p_SelectedRowsOnly = false);
	bool HasOnPremiseChanges(bool p_SelectedRowsOnly = false) const;

	GridFieldModificationsOnPrem& GetOnPremModifications();
	const GridFieldModificationsOnPrem& GetOnPremModifications() const;

	void RevertOnPrem(bool p_SelectedRowsOnly = false, bool p_UpdateGridIfNeeded = true);

	bool HasRevertableModificationsPending(bool inSelectionOnly = false) override;

	void EmptyGrid() override;

protected:
	virtual void customizeGrid() override;

    //void AddCategories();

    virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
    virtual void InitializeCommands() override;

	afx_msg void OnEdit();
	afx_msg void OnUpdateEdit(CCmdUI* pCmdUI);

	afx_msg void OnRefreshOnPrem();
	afx_msg void OnUpdateRefreshOnPrem(CCmdUI* pCmdUI);
    afx_msg void OnGroupLoadMore();
	afx_msg void OnUpdateGroupLoadMore(CCmdUI* pCmdUI);

	afx_msg void OnShowRecycleBin();
	afx_msg void OnUpdateShowRecycleBin(CCmdUI* pCmdUI);
	afx_msg void OnCreateTeam();
	afx_msg void OnUpdateCreateTeam(CCmdUI* pCmdUI);
	afx_msg void OnBatchImport();
	afx_msg void OnUpdateBatchImport(CCmdUI* pCmdUI);
	
	afx_msg void OnChangePreFilter();
	afx_msg void OnUpdateChangePreFilter(CCmdUI* pCmdUI);

	afx_msg void OnEditOnPrem();
	afx_msg void OnUpdateEditOnPrem(CCmdUI* pCmdUI);
	afx_msg void OnEditO365();
	afx_msg void OnUpdateEditO365(CCmdUI* pCmdUI);

	DECLARE_MESSAGE_MAP()

	virtual BusinessGroup getBusinessObject(GridBackendRow*) const;
	virtual OnPremiseGroup getOnPremiseGroup(GridBackendRow* row) const;

	virtual void getFieldsThatHaveErrors(GridBackendRow* p_Row, std::set<wstring>& p_ListFieldErrors) const;

	virtual BOOL PreTranslateMessage(MSG* pMsg) override;

	virtual INT_PTR showDialog(vector<BusinessGroup>& p_Objects, const std::set<wstring>& p_ListFieldErrors, DlgFormsHTML::Action p_Action, CWnd* p_Parent) override;

	virtual void customizeGridPostProcess() override;

	void setUpLoadMoreColumn(GridBackendColumn* p_Col);
	void setUpNoLoadMoreColumn(GridBackendColumn* p_Col);

	void updateAddToGuests(GridBackendRow* p_Row, const boost::YOpt<bool>& p_TenantAddToGuest);
	const boost::YOpt<bool> getTenantAllowAddToGuests() const;

	void updateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh, bool p_DoNotShowLoadingErrors, const boost::YOpt<GridUpdater::PostUpdateError>& p_UnprocessedPostUpdateError);

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

	virtual void OnCustomDoubleClick() override;
	void onPostExplodeMultiValueSisterhood(GridBackendRow* p_Row, const std::set<GridBackendColumn*>& p_Columns) override;

private:
	void updateExpirationDate(GridBackendRow* p_Row);
	void updateGroupLCPStatus(GridBackendRow* p_Row, const BusinessGroup& p_BG);

    void LoadMoreImpl(const std::vector<GridBackendRow*>& p_Rows);

	bool isOffice365Group(const GridBackendRow* p_Row) const;
	bool isDynamicMembershipGroup(const GridBackendRow* p_Row) const;
	bool isOwnersColumn(const GridBackendColumn* p_Col) const;
	bool isSettingsColumn(const GridBackendColumn* p_Col) const;
	bool isMembersCountColumn(const GridBackendColumn* p_Col) const;
	bool isMembershipRuleColumn(const GridBackendColumn* p_Col) const;

	bool isYammer(const GridBackendRow* p_Row) const;

	bool				m_ForceSorting;

	bool				m_SyncNow = false;

	GridTemplateGroups	m_Template;
	FrameGroups*		m_Frame;

	GridBackendColumn* m_ColumnComputedAllowToAddGuests;

	boost::YOpt<std::vector<BusinessGroupSetting>> m_GroupSettings;

	map<wstring, row_pk_t> m_O365RowPk; // Everything with an immutable id that is at least O365
	map<wstring, row_pk_t> m_OnPremRowPk; // Everything with an immutable id that is at least OnPrem

	boost::YOpt<PooledString> m_LastLCPError;
	wstring m_LastGlobalLCPStatus;
	int m_PolicyDays;
	
	static wstring g_PolicyHasChangedStatus;
	static wstring g_GroupNeverExpire;
	static wstring g_PolicyErrorStatus;

	struct LoadMoreConfig
	{
		wstring m_Tooltip;
		wstring m_Text;
		int m_Icon = 0;
	};
	LoadMoreConfig m_LoadMoreConfig;

	HACCEL m_hAccelSpecific = nullptr;

	std::unique_ptr<GridFieldModificationsOnPrem> m_OnPremModifications;

	template<class T> friend class OnPremModificationsApplyer;
	template<class T> friend class OnPremModificationsReverter;
};