#include "FrameGroupMembersHierarchy.h"

#include "CommandDispatcher.h"

IMPLEMENT_DYNAMIC(FrameGroupMembersHierarchy, FrameGroupCommon)

void FrameGroupMembersHierarchy::createGrid()
{
	m_groupsGrid.SetAutomationName(GridUtil::g_AutoNameGroupsHierarchy);
	BOOL gridCreated = m_groupsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FrameGroupMembersHierarchy::customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(p_Tab);
	CreateRefreshGroup(p_Tab);
	CreateApplyGroup(p_Tab);
	CreateRevertGroup(p_Tab);

	{
		auto groupCtrl = p_Tab.AddGroup(YtriaTranslate::Do(FrameGroupMembersHierarchy_customizeActionsRibbonTab_2, _YLOC("Group")).c_str());
		setGroupImage(*groupCtrl, 0);

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_MEMBERSGRID_ADD);
			GridFrameBase::setImage({ ID_MEMBERSGRID_ADD }, IDB_GROUPS_ADDUSER, xtpImageNormal);
			GridFrameBase::setImage({ ID_MEMBERSGRID_ADD }, IDB_GROUPS_ADDUSER_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_OWNERSSGRID_ADD);
			GridFrameBase::setImage({ ID_OWNERSSGRID_ADD }, IDB_GROUPS_ADDOWNER, xtpImageNormal);
			GridFrameBase::setImage({ ID_OWNERSSGRID_ADD }, IDB_GROUPS_ADDOWNER_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	{
		auto groupCtrl = p_Tab.AddGroup(YtriaTranslate::Do(FrameGroupMembersHierarchy_customizeActionsRibbonTab_1, _YLOC("Members")).c_str());
		setGroupImage(*groupCtrl, 0);

#if MEMBERS_GRID_HIDE_NESTED
		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_MEMBERSGRID_SHOWNESTED);
			GridFrameBase::setImage({ ID_MEMBERSGRID_SHOWNESTED }, IDB_MEMBERSGRID_SHOWNESTED, xtpImageNormal);
			GridFrameBase::setImage({ ID_MEMBERSGRID_SHOWNESTED }, IDB_MEMBERSGRID_SHOWNESTED_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
#endif

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_MEMBERSGRID_DELETE);
			GridFrameBase::setImage({ ID_MEMBERSGRID_DELETE }, IDB_GROUPS_REMOVEUSER, xtpImageNormal);
			GridFrameBase::setImage({ ID_MEMBERSGRID_DELETE }, IDB_GROUPS_REMOVEUSER_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_MEMBERSGRID_COPYTO);
			GridFrameBase::setImage({ ID_MEMBERSGRID_COPYTO }, IDB_GROUPS_COPYUSERTO, xtpImageNormal);
			GridFrameBase::setImage({ ID_MEMBERSGRID_COPYTO }, IDB_GROUPS_COPYUSERTO_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_MEMBERSGRID_MOVETO);
			GridFrameBase::setImage({ ID_MEMBERSGRID_MOVETO }, IDB_GROUPS_MOVEUSERTO, xtpImageNormal);
			GridFrameBase::setImage({ ID_MEMBERSGRID_MOVETO }, IDB_GROUPS_MOVEUSERTO_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_MEMBERSGRID_UPGRADETOOWNER);
			GridFrameBase::setImage({ ID_MEMBERSGRID_UPGRADETOOWNER }, IDB_GROUPS_UPGRADETOOWNER, xtpImageNormal);
			GridFrameBase::setImage({ ID_MEMBERSGRID_UPGRADETOOWNER }, IDB_GROUPS_UPGRADETOOWNER_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	{
		auto groupCtrl = p_Tab.AddGroup(_T("Owners"));
		setGroupImage(*groupCtrl, 0);

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_MEMBERSGRID_DOWNGRADETOMEMBER);
			GridFrameBase::setImage({ ID_MEMBERSGRID_DOWNGRADETOMEMBER }, IDB_GROUPS_DOWNGRADETOMEMBER, xtpImageNormal);
			GridFrameBase::setImage({ ID_MEMBERSGRID_DOWNGRADETOMEMBER }, IDB_GROUPS_DOWNGRADETOMEMBER_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_MEMBERSGRID_ADDTOMEMBERS);
			GridFrameBase::setImage({ ID_MEMBERSGRID_ADDTOMEMBERS }, IDB_GROUPS_ADDUSER, xtpImageNormal);
			GridFrameBase::setImage({ ID_MEMBERSGRID_ADDTOMEMBERS }, IDB_GROUPS_ADDUSER_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	CreateLinkGroups(p_Tab, true, true);

	automationAddCommandIDToGreenLight(ID_MEMBERSGRID_ADD);
	automationAddCommandIDToGreenLight(ID_MEMBERSGRID_DELETE);
	automationAddCommandIDToGreenLight(ID_MEMBERSGRID_COPYTO);
	automationAddCommandIDToGreenLight(ID_MEMBERSGRID_MOVETO);

	return true;
}

void FrameGroupMembersHierarchy::ShowHierarchies(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge)
{
	m_groupsGrid.AddGroups(p_Groups, p_UpdateOperations, p_FullPurge);
}

O365Grid& FrameGroupMembersHierarchy::GetGrid()
{
	return m_groupsGrid;
}

AutomatedApp::AUTOMATIONSTATUS FrameGroupMembersHierarchy::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionGroupMembersAdd(p_Action))
		p_CommandID = ID_MEMBERSGRID_ADD;
	else if (IsActionGroupMembersRemove(p_Action))
		p_CommandID = ID_MEMBERSGRID_DELETE;
	else if (IsActionGroupMembersCopy(p_Action))
		p_CommandID = ID_MEMBERSGRID_COPYTO;
	else if (IsActionGroupMembersMove(p_Action))
		p_CommandID = ID_MEMBERSGRID_MOVETO;
	else if (IsActionSetNestedGroup(p_Action))
	{
		m_groupsGrid.SetShowNested(p_Action->IsParamTrue(AutomationConstant::val().m_ParamNameValue));
		setAutomationGreenLight(p_Action);
		statusRV = AutomatedApp::AUTOMATIONSTATUS_OK;
	}

	return statusRV;
}