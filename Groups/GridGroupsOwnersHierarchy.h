#pragma once

#include "BusinessUser.h"
#include "GridGroupsCommon.h"
#include "GridTemplateGroups.h"
#include "GridTemplateUsers.h"

class BusinessGroup;
class FrameGroupOwnersHierarchy;
class GridGroupsOwnersHierarchy : public GridGroupsCommon
{
public:
	GridGroupsOwnersHierarchy();
    virtual ~GridGroupsOwnersHierarchy() override = default;

	void AddGroups(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Purge);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
	virtual void customizeGrid() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
	virtual void InitializeCommands() override;

	afx_msg void OnAddItemsToGroups();
	afx_msg void OnUpdateAddItemsToGroups(CCmdUI* pCmdUI);
	afx_msg void OnRemoveItemsFromGroups();
	afx_msg void OnUpdateRemoveItemsFromGroups(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()

private:
	GridBackendRow* addRowHierarchy(const BusinessGroup& p_Group, const BusinessGroup* p_ParentGroup, GridUpdater& p_Updater, bool p_KeepExistingIfGroupCanceled);

	FrameGroupOwnersHierarchy*		m_Frame;
};
