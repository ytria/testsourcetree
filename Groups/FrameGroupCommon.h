#pragma once

#include "GridFrameBase.h"

#include "BaseO365Grid.h"
#include "ModuleGroup.h"
#include "GridGroupsHierarchy.h"

class GridGroupsCommon;

class FrameGroupCommon : public GridFrameBase
{
public:
    DECLARE_DYNAMIC(FrameGroupCommon)
	FrameGroupCommon(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
    virtual ~FrameGroupCommon() override = default;

    virtual void ShowHierarchies(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge) = 0;

	virtual void ApplyAllSpecific() override;
    virtual void ApplySelectedSpecific() override;

    void ApplySpecific(bool p_Selected);

	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

    void SetGrid(GridGroupsCommon* p_Grid);

protected:
	virtual O365ControlConfig GetRefreshPartialControlConfig() override;
	virtual O365ControlConfig GetApplyPartialControlConfig() override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
    GridGroupsCommon* m_Grid = nullptr;
};

