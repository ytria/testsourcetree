#include "FrameGroups.h"

#include "AutomationNames.h"
#include "BusinessObjectManager.h"
#include "Command.h"
#include "CommandDispatcher.h"
#include "CommandInfo.h"
#include "DlgBusinessGroupSettingsEditHTML.h"
#include "DlgBusinessLCPEditHTML.h"
#include "DlgOnPremServerConfig.h"
#include "FlatObjectListFrameRefresher.h"
#include "ForceSyncOnPremCommand.h"
#include "GridUpdater.h"
#include "ModuleGroup.h"
#include "OnPremRevertSelectedButtonUpdater.h"
#include "OnPremRevertAllButtonUpdater.h"
#include "OnPremSaveSelectedButtonUpdater.h"
#include "OnPremSaveAllButtonUpdater.h"
#include "UpdatedObjectsGenerator.h"
#include "YCodeJockMessageBox.h"
#include "DlgOnPremAADConnectServer.h"

IMPLEMENT_DYNAMIC(FrameGroups, GridFrameBase)

BEGIN_MESSAGE_MAP(FrameGroups, GridFrameBase)
	ON_COMMAND(ID_GROUPLIFECYCLE_POLICYCREATE,				OnPolicyCreate)
	ON_UPDATE_COMMAND_UI(ID_GROUPLIFECYCLE_POLICYCREATE,	OnUpdatePolicyCreate)
	ON_COMMAND(ID_GROUPLIFECYCLE_POLICYUPDATE,				OnPolicyUpdate)
	ON_UPDATE_COMMAND_UI(ID_GROUPLIFECYCLE_POLICYUPDATE,	OnUpdatePolicyUpdate)
	ON_COMMAND(ID_GROUPLIFECYCLE_POLICYDELETE,				OnPolicyDelete)
	ON_UPDATE_COMMAND_UI(ID_GROUPLIFECYCLE_POLICYDELETE,	OnUpdatePolicyDelete)

	ON_COMMAND(ID_GROUPSGRID_SHOW_TENANT_SETTINGS,				OnShowTenantSettings)
	ON_UPDATE_COMMAND_UI(ID_GROUPSGRID_SHOW_TENANT_SETTINGS,	OnUpdateShowTenantSettings)

	/*ON_COMMAND(ID_GROUPLIFECYCLE_PERGROUPSTATUS, OnPolicyPerGroupStatus)
	ON_UPDATE_COMMAND_UI(ID_GROUPLIFECYCLE_PERGROUPSTATUS, OnUpdatePolicyPerGroupStatus)*/
	ON_COMMAND(ID_GROUPLIFECYCLE_PERGROUPRENEW,					OnPolicyPerGroupRenew)
	ON_UPDATE_COMMAND_UI(ID_GROUPLIFECYCLE_PERGROUPRENEW,		OnUpdatePolicyPerGroupRenew)
	ON_COMMAND(ID_GROUPLIFECYCLE_PERGROUPADDGROUP,				OnPolicyPerGroupAddGroup)
	ON_UPDATE_COMMAND_UI(ID_GROUPLIFECYCLE_PERGROUPADDGROUP,	OnUpdatePolicyPerGroupAddGroup)
	ON_COMMAND(ID_GROUPLIFECYCLE_PERGROUPREMOVEGROUP,			OnPolicyPerGroupRemoveGroup)
	ON_UPDATE_COMMAND_UI(ID_GROUPLIFECYCLE_PERGROUPREMOVEGROUP, OnUpdatePolicyPerGroupRemoveGroup)

	/*ON_COMMAND(ID_APPLYALLONPREM, OnApplyAllOnPrem)
	ON_UPDATE_COMMAND_UI(ID_APPLYALLONPREM, OnUpdateApplyAllOnPrem)
	ON_COMMAND(ID_APPLYSELECTEDONPREM, OnApplySelectedOnPrem)
	ON_UPDATE_COMMAND_UI(ID_APPLYSELECTEDONPREM, OnUpdateApplySelectedOnPrem)
	ON_COMMAND(ID_REVERTALLONPREM, OnRevertAllOnPrem)
	ON_UPDATE_COMMAND_UI(ID_REVERTALLONPREM, OnUpdateRevertAllOnPrem)
	ON_COMMAND(ID_REVERTSELECTEDONPREM, OnRevertSelectedOnPrem)
	ON_UPDATE_COMMAND_UI(ID_REVERTSELECTEDONPREM, OnUpdateRevertSelectedOnPrem)*/
	ON_COMMAND(ID_FORCESYNCONPREM, OnForceSyncOnPrem)
	ON_UPDATE_COMMAND_UI(ID_FORCESYNCONPREM, OnUpdateForceSyncOnPrem)
END_MESSAGE_MAP()

FrameGroups::FrameGroups(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame, true)
{
	m_ModBtnsUpdater.SetRevertAllEnableUpdater(std::make_unique<OnPremRevertAllButtonUpdater<FrameGroups>>(*this));
	m_ModBtnsUpdater.SetRevertSelectedEnableUpdater(std::make_unique<OnPremRevertSelectedButtonUpdater<FrameGroups>>(*this));
	m_ModBtnsUpdater.SetSaveAllEnableUpdater(std::make_unique<OnPremSaveAllButtonUpdater<FrameGroups>>(*this));
	m_ModBtnsUpdater.SetSaveSelectedEnableUpdater(std::make_unique<OnPremSaveSelectedButtonUpdater<FrameGroups>>(*this));
}

FrameGroups::~FrameGroups()
{
    PreAllocatedIDS::ReleaseGroupPolicyIconID(m_GroupPolicyIconID);
}

void FrameGroups::ShowGroups(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_AllowFullPurge)
{
	m_groupsGrid.BuildView(p_Groups, {}, p_UpdateOperations, p_Refresh, p_AllowFullPurge);
}

void FrameGroups::ShowGroups(const vector<BusinessGroup>& p_Groups, const vector<BusinessGroup>& p_LoadedMoreGroups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_AllowFullPurge)
{
	m_groupsGrid.BuildView(p_Groups, p_LoadedMoreGroups, p_UpdateOperations, p_Refresh, p_AllowFullPurge);
}

void FrameGroups::RefreshGroupLCPStatus(const BusinessLifeCyclePolicies& p_LCP, bool p_RefreshGrid)
{
	m_groupsGrid.SetLCP(p_LCP, p_RefreshGrid);
}

void FrameGroups::UpdateGroupLCPStatus(const vector<BusinessGroup>& p_Groups)
{
	m_groupsGrid.UpdateGroupLCPStatus(p_Groups);
}

void FrameGroups::createGrid()
{
	m_AddRBACHideOutOfScope = true;

	if (GetModuleCriteria().m_IDs.empty())
		m_groupsGrid.SetForceSorting(true);

	BOOL gridCreated = m_groupsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
	ASSERT(gridCreated);
}

bool FrameGroups::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTab, bool p_ShowLinkTabGroup)
{
	GridFrameBase::customizeActionsRibbonTab(tab, false, true);

	/*{
		CXTPRibbonGroup* applyGroup = findGroup(tab, g_ActionsApplyGroup.c_str());
		ASSERT(nullptr != applyGroup);
		if (nullptr != applyGroup)
		{
			addControlTo(
				{
					ID_APPLYALLONPREM,
					IDB_APPLY,
					IDB_APPLY_16X16,
					_YTEXT("Save All On-Premises"),
					_YTEXT("Save All On-Premises"),
					_YTEXT("Save your On-Premises changes.\nAny pending changes such as edited or deleted items will be indicated in the status column.\nTip: Grouping by the status column is a quick way to check for all pending changes.\nNote: The Undo button will NOT undo changes that have already been saved.")
				}, *applyGroup, ID_APPLYALLONPREM);

			addControlTo(
				{
					ID_APPLYSELECTEDONPREM,
					IDB_APPLY_SELECTED,
					IDB_APPLY_SELECTED_16X16,
					_YTEXT("Save Selected On-Premises"),
					_YTEXT("Save Selected On-Premises"),
					_YTEXT("Save selected On-Premises changes.")
				}, *applyGroup, ID_APPLYSELECTEDONPREM);
		}
	}

	{
		CXTPRibbonGroup* revertGroup = findGroup(tab, g_ActionsRevertGroup.c_str());
		ASSERT(nullptr != revertGroup);
		if (nullptr != revertGroup)
		{
			auto ctrl = addControlTo(
				{
					ID_REVERTALLONPREM,
					IDB_REVERT_ALL,
					IDB_REVERT_ALL_16X16,
					_YTEXT("Undo All On-Premises"),
					_YTEXT("Undo All On-Premises"),
					_YTEXT("Undo all On-Premises changes currently pending in the grid.ip: Grouping by the status column is a quick way to check for all pending changes.\nNote: This button will NOT undo changes that have already been saved to the server.")
				}, *revertGroup, ID_REVERTALLONPREM);

			ctrl->SetBeginGroup(TRUE);

			addControlTo(
				{
					ID_REVERTSELECTEDONPREM,
					IDB_REVERT_SELECTED,
					IDB_REVERT_SELECTED_16X16,
					_YTEXT("Selected On-Premises"),
					_YTEXT("Selected On-Premises"),
					_YTEXT("Undo any selected On-Premises changes currently pending in the grid.\nTip: Grouping by the status column is a quick way to check for all pending changes.\nNote: This button will NOT undo changes that have already been saved to the server.")
				}, *revertGroup, ID_REVERTSELECTEDONPREM);
		}
	}*/

	{
		CXTPRibbonGroup* dataGroup = findGroup(tab, g_ActionsDataGroup.c_str());
		ASSERT(nullptr != dataGroup);
		if (nullptr != dataGroup)
		{
			if (GetOptions() && !GetOptions()->m_CustomFilter.IsEmpty())
			{
				auto ctrl = dataGroup->Add(xtpControlButton, ID_GROUPGRID_CHANGEPREFILTER);
				setImage({ ID_GROUPGRID_CHANGEPREFILTER }, IDB_CHANGEMODULEOPTIONS, xtpImageNormal);
				setImage({ ID_GROUPGRID_CHANGEPREFILTER }, IDB_CHANGEMODULEOPTIONS_16x16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			if (!GetOptions() || GetOptions()->m_CustomFilter.IsEmpty())
			{
				auto ctrl = dataGroup->Add(xtpControlSplitButtonPopup, ID_GROUPGRID_REFRESH_ONPREM);
				GridFrameBase::setImage({ ID_GROUPGRID_REFRESH_ONPREM }, IDB_GROUPGRID_REFRESH_ONPREM, xtpImageNormal);
				GridFrameBase::setImage({ ID_GROUPGRID_REFRESH_ONPREM }, IDB_GROUPGRID_REFRESH_ONPREM_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);

				auto popup = dynamic_cast<CXTPControlPopup*>(ctrl);
				ASSERT(nullptr != popup);
				if (nullptr != popup)
				{
					{
						auto ctrl = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_GROUPGRID_REFRESH_ONPREM);
					}

					{
						auto forceSynCtrl = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_FORCESYNCONPREM);

						setControlTexts(
							forceSynCtrl,
							_T("Sync AD DS/Azure AD"),
							_T("Sync AD DS/Azure AD"),
							_T("Sync AD DS/Azure AD"));
					}
				}
			}
			{
				auto ctrl = dataGroup->Add(xtpControlButton, ID_GROUPGRID_LOADMORE);
				GridFrameBase::setImage({ ID_GROUPGRID_LOADMORE }, IDB_LOADMORE, xtpImageNormal);
				GridFrameBase::setImage({ ID_GROUPGRID_LOADMORE }, IDB_LOADMORE_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}
		}
	}

	{
		CXTPRibbonGroup* editGroup = findGroup(tab, g_ActionsEditGroup.c_str());
		ASSERT(nullptr != editGroup);
		if (nullptr != editGroup)
		{
			{
				auto editCtrl = editGroup->FindControl(ID_MODULEGRID_EDIT);
				const auto pos = editGroup->IndexOf(editCtrl);
				editGroup->Remove(editCtrl);
				editCtrl = editGroup->Add(xtpControlSplitButtonPopup, ID_MODULEGRID_EDIT, nullptr, pos);
				setGridControlTooltip(editCtrl);

				auto popup = dynamic_cast<CXTPControlPopup*>(editCtrl);
				ASSERT(nullptr != popup);
				if (nullptr != popup)
				{
					{
						auto editOCtrl = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_GROUPGRID_EDITO365);

						setControlTexts(
							editOCtrl,
							_T("Edit O365"),
							_T("Edit O365"),
							_T("Edit O365"));
					}
					{
						auto editOPCtrl = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_GROUPGRID_EDITONPREM);

						setControlTexts(
							editOPCtrl,
							_T("Edit On-Premises"),
							_T("Edit On-Premises"),
							_T("Edit On-Premises"));
					}
				}
			}

			/*{
				auto ctrl = editGroup->Add(xtpControlButton, ID_GROUPGRID_EDITONPREM, nullptr, GetGrid().HasView() ? 2 : 1);
				GridFrameBase::setImage({ ID_GROUPGRID_EDITONPREM }, IDB_GROUPS_EDITONPREM, xtpImageNormal);
				GridFrameBase::setImage({ ID_GROUPGRID_EDITONPREM }, IDB_GROUPS_EDITONPREM_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}*/

			{
				auto ctrl = editGroup->Add(xtpControlButton, ID_GROUPGRID_CREATETEAM);
				GridFrameBase::setImage({ ID_GROUPGRID_CREATETEAM }, IDB_GROUPS_CONVERTTOTEAM, xtpImageNormal);
				GridFrameBase::setImage({ ID_GROUPGRID_CREATETEAM }, IDB_GROUPS_CONVERTTOTEAM_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			{
				auto ctrl = editGroup->Add(xtpControlButton, ID_GROUPGRID_BATCHIMPORT);
				GridFrameBase::setImage({ ID_GROUPGRID_BATCHIMPORT }, IDB_BATCHIMPORT, xtpImageNormal);
				GridFrameBase::setImage({ ID_GROUPGRID_BATCHIMPORT }, IDB_BATCHIMPORT_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}
		}
	}

	{
		CXTPRibbonGroup* linkGroup = findGroup(tab, g_ActionsLinkGroup.c_str());
		ASSERT(nullptr != linkGroup);
		if (nullptr != linkGroup)
			addNewModuleCommandControl(*linkGroup, ID_GROUPSGRID_SHOW_RECYCLEBIN, { ID_GROUPSGRID_SHOW_RECYCLEBIN_FRAME, ID_GROUPSGRID_SHOW_RECYCLEBIN_NEWFRAME }, { IDB_RECYCLEBIN , IDB_RECYCLEBIN_16X16 });
	}

	automationAddCommandIDToGreenLight(ID_GROUPGRID_CHANGEPREFILTER);

    return true;
}

void FrameGroups::createFrameSpecificTabs()
{
	auto& myRibbonBar = getRibbonBar();
	CXTPRibbonTab* tab = myRibbonBar.AddTab(YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_1, _YLOC("Group Settings")).c_str());
	ASSERT(nullptr != tab);
	if (nullptr != tab)
	{
		CXTPRibbonGroup* pGroup = tab->AddGroup(YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_2, _YLOC("Global Expiration Policy")).c_str());
		if (nullptr != pGroup)
		{
			setGroupImage(*pGroup, 0);

			ASSERT(nullptr == m_PolicyIconControl);
			m_PolicyIconControl = dynamic_cast<CXTPControlBitmap*>(pGroup->Add(new CXTPControlBitmap, m_GroupPolicyIconID));
			ASSERT(nullptr != m_PolicyIconControl);
			m_PolicyIconControl->SetIconId(m_GroupPolicyIconID);
			setImage({ m_GroupPolicyIconID }, IDB_GROUPLIFECYCLE_POLICYNO, xtpImageNormal);

			m_pLCPStatus = pGroup->Add(xtpControlLabel, ID_GROUPLIFECYCLE_POLICYSTATUS);
			m_pLCPStatus->SetCaption(YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_3, _YLOC("No Policy")).c_str());
			m_pLCPLifetime = pGroup->Add(xtpControlLabel, ID_GROUPLIFECYCLE_POLICYDAYS);
			m_pLCPNotification = pGroup->Add(xtpControlLabel, ID_GROUPLIFECYCLE_POLICYNOTIFICATION);

			{
				auto btn = pGroup->Add(xtpControlButton, ID_GROUPLIFECYCLE_POLICYCREATE);
				btn->SetBeginGroup(TRUE);
				setImage({ ID_GROUPLIFECYCLE_POLICYCREATE }, IDB_GROUPLIFECYCLE_POLICYCREATE, xtpImageNormal);
				setImage({ ID_GROUPLIFECYCLE_POLICYCREATE }, IDB_GROUPLIFECYCLE_POLICYCREATE_16X16, xtpImageNormal);
				FrameBase::setControlTexts(btn,
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_4, _YLOC("Create")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_5, _YLOC("Create")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_6, _YLOC("Create a group expiration policy if one does not exist.")).c_str());
				btn->SetStyle(xtpButtonIconAndCaption);
			}

			{
				auto btn = pGroup->Add(xtpControlButton, ID_GROUPLIFECYCLE_POLICYUPDATE);
				setImage({ ID_GROUPLIFECYCLE_POLICYUPDATE }, IDB_GROUPLIFECYCLE_POLICYUPDATE, xtpImageNormal);
				setImage({ ID_GROUPLIFECYCLE_POLICYUPDATE }, IDB_GROUPLIFECYCLE_POLICYUPDATE_16X16, xtpImageNormal);
				FrameBase::setControlTexts(btn,
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_7, _YLOC("Update")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_8, _YLOC("Update")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_9, _YLOC("Update the current group expiration policy.")).c_str());
				btn->SetStyle(xtpButtonIconAndCaption);
			}

			{
				auto btn = pGroup->Add(xtpControlButton, ID_GROUPLIFECYCLE_POLICYDELETE);
				setImage({ ID_GROUPLIFECYCLE_POLICYDELETE }, IDB_GROUPLIFECYCLE_POLICYDELETE, xtpImageNormal);
				setImage({ ID_GROUPLIFECYCLE_POLICYDELETE }, IDB_GROUPLIFECYCLE_POLICYDELETE_16X16, xtpImageNormal);
				FrameBase::setControlTexts(btn,
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_10, _YLOC("Delete")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_11, _YLOC("Delete")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_12, _YLOC("Delete the current group expiration policy.")).c_str());
				btn->SetStyle(xtpButtonIconAndCaption);
			}
		}

		pGroup = tab->AddGroup(YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_13, _YLOC("Group Policy Management")).c_str());
		if (nullptr != pGroup)
		{
			setGroupImage(*pGroup, 0);

			/*{
				auto btn = pGroup->Add(xtpControlButton, ID_GROUPLIFECYCLE_PERGROUPSTATUS);
				btn->SetBeginGroup(TRUE);
				setImage({ ID_GROUPLIFECYCLE_PERGROUPSTATUS }, IDB_GROUPLIFECYCLE_PERGROUPSTATUS, xtpImageNormal);
				setImage({ ID_GROUPLIFECYCLE_PERGROUPSTATUS }, IDB_GROUPLIFECYCLE_PERGROUPSTATUS_16X16, xtpImageNormal);
				FrameBase::setControlTexts(btn,
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_14, _TYLOC("Get Group Status")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_15, _TYLOC("Get Policy Status")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_16, _TYLOC("Show the group policy expiration status for the selected groups.")).c_str());
				btn->SetStyle(xtpButtonIconAndCaptionBelow);
			}*/

			{
				auto btn = pGroup->Add(xtpControlButton, ID_GROUPLIFECYCLE_PERGROUPRENEW);
				setImage({ ID_GROUPLIFECYCLE_PERGROUPRENEW }, IDB_GROUPLIFECYCLE_PERGROUPRENEW, xtpImageNormal);
				setImage({ ID_GROUPLIFECYCLE_PERGROUPRENEW }, IDB_GROUPLIFECYCLE_PERGROUPRENEW_16X16, xtpImageNormal);
				FrameBase::setControlTexts(btn,
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_17, _YLOC("Renew")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_18, _YLOC("Renew Expiration Date")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_19, _YLOC("Renews a group's expiration date. When a group is renewed, the group expiration is extended by the number of days defined in the policy.")).c_str());
				btn->SetStyle(xtpButtonIconAndCaptionBelow);
			}

			{
				auto btn = pGroup->Add(xtpControlButton, ID_GROUPLIFECYCLE_PERGROUPADDGROUP);
				btn->SetBeginGroup(TRUE);
				setImage({ ID_GROUPLIFECYCLE_PERGROUPADDGROUP }, IDB_GROUPLIFECYCLE_PERGROUPADD, xtpImageNormal);
				setImage({ ID_GROUPLIFECYCLE_PERGROUPADDGROUP }, IDB_GROUPLIFECYCLE_PERGROUPADD_16X16, xtpImageNormal);
				FrameBase::setControlTexts(btn,
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_20, _YLOC("Apply")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_21, _YLOC("Apply")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_22, _YLOC("Apply the current group expiration policy to the selected groups.")).c_str());
				btn->SetStyle(xtpButtonIconAndCaptionBelow);
			}

			{
				auto btn = pGroup->Add(xtpControlButton, ID_GROUPLIFECYCLE_PERGROUPREMOVEGROUP);
				setImage({ ID_GROUPLIFECYCLE_PERGROUPREMOVEGROUP }, IDB_GROUPLIFECYCLE_PERGROUPREMOVE, xtpImageNormal);
				setImage({ ID_GROUPLIFECYCLE_PERGROUPREMOVEGROUP }, IDB_GROUPLIFECYCLE_PERGROUPREMOVE_16X16, xtpImageNormal);
				FrameBase::setControlTexts(btn,
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_23, _YLOC("Remove")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_24, _YLOC("Remove")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_25, _YLOC("Remove the current group expiration policy from the selected groups.")).c_str());
				btn->SetStyle(xtpButtonIconAndCaptionBelow);
			}
		}

		pGroup = tab->AddGroup(YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_26, _YLOC("Global Settings")).c_str());
		if (nullptr != pGroup)
		{
			setGroupImage(*pGroup, 0);

			{
				auto btn = pGroup->Add(xtpControlButton, ID_GROUPSGRID_SHOW_TENANT_SETTINGS);
				btn->SetBeginGroup(TRUE);
				setImage({ ID_GROUPSGRID_SHOW_TENANT_SETTINGS }, IDB_TENANTSETTINGS, xtpImageNormal);
				setImage({ ID_GROUPSGRID_SHOW_TENANT_SETTINGS }, IDB_TENANTSETTINGS_16X16, xtpImageNormal);
				FrameBase::setControlTexts(btn,
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_27, _YLOC("Edit Group Settings...")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_28, _YLOC("Edit Group Settings")).c_str(),
					YtriaTranslate::Do(FrameGroups_createFrameSpecificTabs_29, _YLOC("Change tenant-wide group settings.")).c_str());
				btn->SetStyle(xtpButtonIconAndCaptionBelow);
			}

			m_GroupSettingsUnifiedStatus = pGroup->Add(xtpControlLabel, ID_GROUPSETTINGS_UNIFIED);
			m_GroupSettingsUnifiedStatus->SetCaption(YtriaTranslate::Do(FrameGroups_SetTenantGroupSettings_6, _YLOC("Group.Unified: %1"), _YTEXT("-")).c_str());
				
			m_GroupSettingsAddToGuestStatus = pGroup->Add(xtpControlLabel, ID_GROUPSETTINGS_ADDTOGUEST);
			m_GroupSettingsAddToGuestStatus->SetCaption(YtriaTranslate::Do(FrameGroups_SetTenantGroupSettings_7, _YLOC("Allow To Add Guests: %1"), _YTEXT("-")).c_str());
		}

		addNavigationGroup(*tab);
	}
}

O365Grid& FrameGroups::GetGrid()
{
	return m_groupsGrid;
}

void FrameGroups::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	FlatObjectListFrameRefresher()(*this, Command::ModuleTarget::Group, Origin::Group, p_CurrentAction, [this](const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Update, bool p_ForceSortingIfEmptyGrid)
	{
		// Only reached when update operations contain only successful deletion (== no request necessary)
		this->ShowGroups({}, p_UpdateOperations, p_Update, false);
	});
}

void FrameGroups::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameGroups::ApplySpecific(bool p_Selected)
{
    UpdatedObjectsGenerator<BusinessGroup> updatedGroupsGenerator;
    updatedGroupsGenerator.BuildUpdatedObjects(GetGrid(), p_Selected);

    CommandInfo info;
    info.Data() = updatedGroupsGenerator;
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateModified, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), GetAutomationAction() }));
}

void FrameGroups::ShowOnPremiseGroups(const vector<OnPremiseGroup>& p_Groups)
{
	m_groupsGrid.ShowOnPremiseGroups(p_Groups);
}

void FrameGroups::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

AutomatedApp::AUTOMATIONSTATUS FrameGroups::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionSelectedGroupLoadMore(p_Action))
	{
		p_CommandID = ID_GROUPGRID_LOADMORE;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedEditGroup(p_Action))
		p_CommandID = ID_MODULEGRID_EDIT;
	else if (IsActionCreateGroup(p_Action))
		p_CommandID = ID_MODULEGRID_CREATE;

	return statusRV;
}

GridFrameBase::ApplyConfirmationConfig FrameGroups::GetApplyConfirmationConfig(bool p_ApplySelected) const
{
	if (m_groupsGrid.HasNonOffice365GroupsMarkedForDeletion(p_ApplySelected))
	{
		ApplyConfirmationConfig applyConfirmationConfig;
		applyConfirmationConfig.m_AdditionalWarning = YtriaTranslate::Do(FrameGroups_getApplyConfirmationConfig_1, _YLOC("Some of the groups you're about to delete are not Office 365 groups and then won't be put in the Recycle bin.")).c_str();
		applyConfirmationConfig.m_Icon = DlgMessageBox::eIcon_ExclamationWarning;
		return applyConfirmationConfig;
	}

	return GridFrameBase::GetApplyConfirmationConfig(p_ApplySelected);
}

bool FrameGroups::hasLoseableModificationsPending(bool inSelectionOnly /*= false*/)
{
	return hasModificationsPending(inSelectionOnly) || hasOnPremiseChanges(inSelectionOnly);
}

bool FrameGroups::hasOnPremiseChanges(bool inSelectionOnly /*= false*/)
{
	return m_groupsGrid.HasOnPremiseChanges(inSelectionOnly);
}

UINT FrameGroups::initGroupPolicyIconID()
{
	return PreAllocatedIDS::GetFreeGroupPolicyIconID();
}

void FrameGroups::UpdateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh)
{
    m_groupsGrid.UpdateGroupsLoadMore(p_Groups, p_FromRefresh, false);
}

void FrameGroups::ShowLifecyclePolicies(bool p_RefreshGrid)
{
	ModuleGroup* pModGroup = dynamic_cast<ModuleGroup*>(&GetModuleBase());
	ASSERT(nullptr != pModGroup);
	if (nullptr != pModGroup)
	{
		const BusinessLifeCyclePolicies& BizLCP = pModGroup->GetGroupLifeCyclePolicies();

		ASSERT(nullptr != m_PolicyIconControl && nullptr != m_pLCPStatus && nullptr != m_pLCPLifetime && nullptr != m_pLCPNotification);

		if (BizLCP.GetError())
		{
			setImage({ m_GroupPolicyIconID }, IDB_GROUPLIFECYCLE_POLICYERROR, xtpImageNormal);

			if (nullptr != m_PolicyIconControl)
				m_PolicyIconControl->SetTooltip(BizLCP.GetError()->GetFullErrorMessage().c_str());

			if (nullptr != m_pLCPStatus)
				m_pLCPStatus->SetCaption(YtriaTranslate::Do(FrameGroups_ShowLifecyclePolicies_2, _YLOC("Unable to get Policy")).c_str());

			if (nullptr != m_pLCPLifetime)
				m_pLCPLifetime->SetCaption(_YTEXT(""));

			if (nullptr != m_pLCPNotification)
				m_pLCPNotification->SetCaption(_YTEXT(""));
		}
		else if (!BizLCP.IsPresent())
		{
			setImage({ m_GroupPolicyIconID }, IDB_GROUPLIFECYCLE_POLICYNO, xtpImageNormal);

			if (nullptr != m_PolicyIconControl)
				m_PolicyIconControl->SetTooltip(_YTEXT(""));

			if (nullptr != m_pLCPStatus)
				m_pLCPStatus->SetCaption(YtriaTranslate::Do(FrameGroups_ShowLifecyclePolicies_1, _YLOC("No Policy Set")).c_str());
			
			if (nullptr != m_pLCPLifetime)
				m_pLCPLifetime->SetCaption(_YTEXT(""));

			if (nullptr != m_pLCPNotification)
				m_pLCPNotification->SetCaption(_YTEXT(""));
		} 
		else
		{
			ASSERT(BizLCP.IsPresent() && boost::none != BizLCP.m_ManagedGroupTypes && boost::none != BizLCP.m_NumberOfDays && boost::none != BizLCP.m_AlternativeNotificationEmails);

			UINT PolicyIconID = IDB_GROUPLIFECYCLE_POLICYNO;
			if (BizLCP.m_ManagedGroupTypes.get() == _YTEXT("None"))
				PolicyIconID = IDB_GROUPLIFECYCLE_POLICYNONE;
			else if (BizLCP.m_ManagedGroupTypes.get() == _YTEXT("All"))
				PolicyIconID = IDB_GROUPLIFECYCLE_POLICYALL;
			else if (BizLCP.m_ManagedGroupTypes.get() == _YTEXT("Selected"))
				PolicyIconID = IDB_GROUPLIFECYCLE_POLICYSELECTED;
			else
				ASSERT(FALSE);

			setImage({ m_GroupPolicyIconID }, PolicyIconID, xtpImageNormal);

			if (nullptr != m_PolicyIconControl)
				m_PolicyIconControl->SetTooltip(_YTEXT(""));

			if (nullptr != m_pLCPStatus)
				m_pLCPStatus->SetCaption(YtriaTranslate::Do(FrameGroups_ShowLifecyclePolicies_3, _YLOC("Group Types: %1"), BizLCP.m_ManagedGroupTypes.get().c_str()).c_str());

			if (nullptr != m_pLCPLifetime)
				m_pLCPLifetime->SetCaption(YtriaTranslate::Do(FrameGroups_ShowLifecyclePolicies_4, _YLOC("Lifetime: %1"), Str::getStringFromNumber(BizLCP.m_NumberOfDays.get()).c_str()).c_str());

			if (nullptr != m_pLCPNotification)
				m_pLCPNotification->SetCaption(YtriaTranslate::Do(FrameGroups_ShowLifecyclePolicies_5, _YLOC("Notification: %1"), BizLCP.m_AlternativeNotificationEmails.get().c_str()).c_str());
		}

		// Initializes the LCP status column based on the current lifecycle policy.
		RefreshGroupLCPStatus(BizLCP, p_RefreshGrid);
	}
}

void FrameGroups::SetOnPremiseGroupsUpdateResult(const vector<O365UpdateOperation>& p_UpdateResults)
{
	m_groupsGrid.SetOnPremiseGroupsUpdateResult(p_UpdateResults);
}

void FrameGroups::ShowRecycleBin()
{
	CommandInfo info;
	info.SetOrigin(GetModuleCriteria().m_Origin);

	CommandDispatcher::GetInstance().Execute(Command(Util::ForSubModule(GetLicenseContext()), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::ListRecycleBin, info, { &GetGrid(),_YTEXT("RecycleBin"), GetAutomationActionRecording(), GetAutomationAction() }));
}

bool FrameGroups::IsThereLCP()
{
	ModuleGroup* pModGroup = dynamic_cast<ModuleGroup*>(&GetModuleBase());
	ASSERT(nullptr != pModGroup);
	return nullptr != pModGroup && pModGroup->GetGroupLifeCyclePolicies().IsPresent();
}

bool FrameGroups::IsThereLCPError()
{
	ModuleGroup* pModGroup = dynamic_cast<ModuleGroup*>(&GetModuleBase());
	ASSERT(nullptr != pModGroup);
	return nullptr != pModGroup && pModGroup->GetGroupLifeCyclePolicies().GetError();
}

bool FrameGroups::IsLCPTypeSelected()
{
	bool Enable = false;
	ModuleGroup* pModGroup = dynamic_cast<ModuleGroup*>(&GetModuleBase());
	ASSERT(nullptr != pModGroup);
	if (nullptr != pModGroup)
	{
		const BusinessLifeCyclePolicies& BizLCP = pModGroup->GetGroupLifeCyclePolicies();
		Enable = BizLCP.IsPresent() && BizLCP.GetManagedGroupTypes() && BizLCP.GetManagedGroupTypes().get() == _YTEXT("Selected");
	}

	return Enable;
}

const boost::YOpt<std::vector<BusinessGroupSettingTemplate>>& FrameGroups::GetGroupSettingTemplates() const
{
    return m_GroupSettingTemplates;
}

void FrameGroups::SetGroupSettingTemplates(const boost::YOpt<std::vector<BusinessGroupSettingTemplate>>& p_Val)
{
    m_GroupSettingTemplates = p_Val;
}

const boost::YOpt<std::vector<BusinessGroupSetting>>& FrameGroups::GetTenantGroupSettings() const
{
    return m_TenantGroupSettings;
}

void FrameGroups::SetTenantGroupSettings(const boost::YOpt<std::vector<BusinessGroupSetting>>& p_Val, bool p_UpdateGrid)
{
    m_TenantGroupSettings = p_Val;

	if (nullptr != m_GroupSettingsUnifiedStatus || nullptr != m_GroupSettingsAddToGuestStatus)
	{
		auto it = m_TenantGroupSettings
			? std::find_if(m_TenantGroupSettings->begin(), m_TenantGroupSettings->end(), [](const auto& p_Setting)
				{
					return p_Setting.GetDisplayName() && *p_Setting.GetDisplayName() == _YTEXT("Group.Unified");
				})
			: m_TenantGroupSettings->end();

		if (nullptr != m_GroupSettingsUnifiedStatus)
		{
			const wstring status = (m_TenantGroupSettings && m_TenantGroupSettings->end() != it) ? YtriaTranslate::Do(DlgBusinessGroupSettingsEditHTML_generateJSONScriptData_1, _YLOC("Activated")).c_str() : YtriaTranslate::Do(YTriaGridCellPercent_TextGet_1, _YLOC("N/A")).c_str();
			m_GroupSettingsUnifiedStatus->SetCaption(YtriaTranslate::Do(FrameGroups_SetTenantGroupSettings_6, _YLOC("Group.Unified: %1"), status.c_str()).c_str());
		}

		if (nullptr != m_GroupSettingsAddToGuestStatus)
		{
			wstring status = YtriaTranslate::Do(YTriaGridCellPercent_TextGet_1, _YLOC("N/A")).c_str();
			if (m_TenantGroupSettings && m_TenantGroupSettings->end() != it)
			{
				const auto& values = it->GetValues();
				auto it = std::find_if(values.begin(), values.end(), [](const auto& p_Value)
				{
					return p_Value.Name && *p_Value.Name == _YTEXT("AllowToAddGuests");
				});

				if (values.end() != it)
				{
					if (it->Value && *it->Value == _YTEXT("true"))
					{
						status = YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_28, _YLOC("On")).c_str();
					}
					else if (it->Value && *it->Value == _YTEXT("false"))
					{
						status = YtriaTranslate::Do(NotesDatabase__FullTextIndexOptions_GetAttachementText_2, _YLOC("Off")).c_str();
					}
				}
			}

			m_GroupSettingsAddToGuestStatus->SetCaption(YtriaTranslate::Do(FrameGroups_SetTenantGroupSettings_7, _YLOC("Allow To Add Guests: %1"), status.c_str()).c_str());
		}
	}

	m_groupsGrid.SetTenantGroupSettings(m_TenantGroupSettings, p_UpdateGrid);
}

void FrameGroups::ShowTenantSettings()
{
	auto session = GetSapio365Session();
	if (session && (!session->IsUseRoleDelegation() || RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(session->GetRoleDelegationID(), GetSapio365Session()).HasPrivilege(RoleDelegationUtil::RBAC_TENANT_GROUP_EDITSETTINGS)))
	{
		ASSERT(m_TenantGroupSettings.is_initialized());
		ASSERT(m_GroupSettingTemplates.is_initialized());
		if (m_TenantGroupSettings.is_initialized() && m_GroupSettingTemplates.is_initialized())
		{
			bool hasError = false;
			wstring errorDetails;
			if (m_TenantGroupSettings->size() == 1 && m_TenantGroupSettings->at(0).GetError().is_initialized())
			{
				hasError = true;
				errorDetails += m_TenantGroupSettings->at(0).GetError()->GetFullErrorMessage() + wstring(_YTEXT("\n"));
			}

			if (m_GroupSettingTemplates->size() == 1 && m_GroupSettingTemplates->at(0).GetError().is_initialized())
			{
				hasError = true;
				errorDetails += m_GroupSettingTemplates->at(0).GetError()->GetFullErrorMessage() + wstring(_YTEXT("\n"));
			}
        
			if (hasError)
			{
				YCodeJockMessageBox errorMsg(this,
					DlgMessageBox::eIcon_Error,
					YtriaTranslate::Do(FrameGroups_ShowTenantSettings_1, _YLOC("Group Settings")).c_str(),
					YtriaTranslate::Do(FrameGroups_ShowTenantSettings_2, _YLOC("An error occurred while retrieving group settings.")).c_str(),
					errorDetails,
					{ { IDOK, YtriaTranslate::Do(FrameGroups_ShowTenantSettings_3, _YLOC("OK")).c_str() } });
				errorMsg.DoModal();
			}
			else
			{
				DlgBusinessGroupSettingsEditHTML dlg(*m_TenantGroupSettings, *m_GroupSettingTemplates, DlgFormsHTML::Action::EDIT, this);
				auto ret = dlg.DoModal();

				if (ret == IDOK)
				{
					CommandInfo info;
					info.Data() = dlg.GetUpdatedSettings();
					info.SetFrame(this);
					info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
					CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateSettings, info, { &GetGrid(), g_ActionNameUpdateTenantSettings/*TODO*/, GetAutomationActionRecording(), GetAutomationAction() }));
				}
			}
		}
	}
}

bool FrameGroups::HasDeltaFeature() const
{
	return GetSessionInfo().UseDeltaGroups();
}

void FrameGroups::OnPolicyCreate()
{
	auto session = GetSapio365Session();
	if (session && (!session->IsUseRoleDelegation() || RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(session->GetRoleDelegationID(), GetSapio365Session()).HasPrivilege(RoleDelegationUtil::RBAC_TENANT_GROUP_EDITSETTINGS)))
	{
		// POST /groupLifecyclePolicies

		ModuleGroup* pModGroup = dynamic_cast<ModuleGroup*>(&GetModuleBase());
		ASSERT(nullptr != pModGroup);
		if (nullptr != pModGroup)
		{
			BusinessLifeCyclePolicies DummyLCP;
			DummyLCP.SetNumberOfDays(180);
			DummyLCP.SetManagedGroupTypes(boost::YOpt<PooledString>(_YTEXT("All")));

			DlgBusinessLCPEditHTML TheDlg(DummyLCP, DlgFormsHTML::Action::CREATE, this);
			if (IDOK == TheDlg.DoModal())
			{
				DummyLCP.SetUpdateStatus(BusinessObject::UPDATE_TYPE_CREATE);

				CommandInfo info;
				info.Data() = DummyLCP;
				info.SetFrame(this);
				info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
				CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateLCP, info, { &GetGrid(), g_ActionNamePolicyCreate, GetAutomationActionRecording(), GetAutomationAction() }));
			}
		}
	}
}

void FrameGroups::OnUpdatePolicyCreate(CCmdUI* pCmdUI)
{
	auto session = GetSapio365Session();
	pCmdUI->Enable(session && session->IsConnected() && !IsThereLCP() && !IsThereLCPError() &&
					(!session->IsUseRoleDelegation() || RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(session->GetRoleDelegationID(), GetSapio365Session()).HasPrivilege(RoleDelegationUtil::RBAC_TENANT_GROUP_EDITSETTINGS)));
}

void FrameGroups::OnPolicyUpdate()
{
	auto session = GetSapio365Session();
	if (session && (!session->IsUseRoleDelegation() || RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(session->GetRoleDelegationID(), GetSapio365Session()).HasPrivilege(RoleDelegationUtil::RBAC_TENANT_GROUP_EDITSETTINGS)))
	{
		// PATCH /groupLifecyclePolicies/{id}
		ModuleGroup* pModGroup = dynamic_cast<ModuleGroup*>(&GetModuleBase());
		ASSERT(nullptr != pModGroup);
		if (nullptr != pModGroup)
		{
			BusinessLifeCyclePolicies UpdateLCP = pModGroup->GetGroupLifeCyclePolicies();

			DlgBusinessLCPEditHTML TheDlg(UpdateLCP, DlgFormsHTML::Action::EDIT, this);
			if (IDOK == TheDlg.DoModal()) 
			{
				UpdateLCP.SetUpdateStatus(BusinessObject::UPDATE_TYPE_MODIFY);

				CommandInfo info;
				info.Data() = UpdateLCP;
				info.SetFrame(this);
				info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
				CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateLCP, info, { &GetGrid(), g_ActionNamePolicyCreate/*TODO*/ , GetAutomationActionRecording(), GetAutomationAction() }));
			}
		}
	}
}

void FrameGroups::OnUpdatePolicyUpdate(CCmdUI* pCmdUI)
{
	auto session = GetSapio365Session();
	pCmdUI->Enable(session && session->IsConnected() && IsThereLCP() &&
					(!session->IsUseRoleDelegation() || RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(session->GetRoleDelegationID(), GetSapio365Session()).HasPrivilege(RoleDelegationUtil::RBAC_TENANT_GROUP_EDITSETTINGS)));
}

void FrameGroups::OnPolicyDelete()
{
	auto session = GetSapio365Session();
	if (session && (!session->IsUseRoleDelegation() || RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(session->GetRoleDelegationID(), GetSapio365Session()).HasPrivilege(RoleDelegationUtil::RBAC_TENANT_GROUP_EDITSETTINGS)))
	{	
		// Are you sure?
		// DELETE /groupLifecyclePolicies/{id}
		YCodeJockMessageBox dlgAreYouSure(
			this,
			DlgMessageBox::eIcon_ExclamationWarning,
			_YTEXT(""),
			YtriaTranslate::Do(FrameGroups_OnPolicyDelete_1, _YLOC("Are you sure?")).c_str(),
			YtriaTranslate::Do(FrameGroups_OnPolicyDelete_2, _YLOC("Group Lifecycle Policy will be deleted.")).c_str(),
			{ { IDOK, YtriaTranslate::Do(FrameGroups_OnPolicyDelete_3, _YLOC("Yes")).c_str() }, { IDCANCEL, YtriaTranslate::Do(FrameGroups_OnPolicyDelete_4, _YLOC("No")).c_str() } });

		if (dlgAreYouSure.DoModal() == IDOK)
		{
			ModuleGroup* pModGroup = dynamic_cast<ModuleGroup*>(&GetModuleBase());
			ASSERT(nullptr != pModGroup);
			if (nullptr != pModGroup)
			{
				BusinessLifeCyclePolicies DeleteLCP = pModGroup->GetGroupLifeCyclePolicies();
				DeleteLCP.SetUpdateStatus(BusinessObject::UPDATE_TYPE_DELETE);
			
				CommandInfo info;
				info.Data() = DeleteLCP;
				info.SetFrame(this);
				info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
				CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateLCP, info, { &GetGrid(), g_ActionNamePolicyDelete/*TODO*/ , GetAutomationActionRecording(), GetAutomationAction() }));
			}
		}
	}
}

void FrameGroups::OnUpdatePolicyDelete(CCmdUI* pCmdUI)
{
	auto session = GetSapio365Session();
	pCmdUI->Enable(session && session->IsConnected() && IsThereLCP() &&
		(!session->IsUseRoleDelegation() || RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(session->GetRoleDelegationID(), GetSapio365Session()).HasPrivilege(RoleDelegationUtil::RBAC_TENANT_GROUP_EDITSETTINGS)));
}

//void FrameGroups::OnPolicyPerGroupStatus()
//{
//	vector<BusinessGroup> groups;
//	vector<row_primary_key_t> pks;
//	m_groupsGrid.GetSelectedGroupsForActionOnLCP(groups, pks);
//
//	ASSERT(groups.size() == pks.size());
//
//	ASSERT(!groups.empty());
//	if (!groups.empty())
//	{
//		UpdatedObjectsGenerator<BusinessGroup> updatedGroupsGenerator;
//		updatedGroupsGenerator.SetObjects(groups);
//		updatedGroupsGenerator.SetPrimaryKeys(pks);
//
//		CommandInfo info;
//		info.Data() = updatedGroupsGenerator;
//		info.SetFrame(this);
//		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), nullptr, Command::ModuleTarget::Group, Command::ModuleTask::ListLCPGroup, info, {}));
//	}
//}
//
//void FrameGroups::OnUpdatePolicyPerGroupStatus(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable(IsConnected() && IsThereLCP() && m_groupsGrid.HasSelectedRowWithPolicyChangedStatus());
//}

void FrameGroups::OnPolicyPerGroupRenew()
{
	vector<BusinessGroup> groups;
	vector<row_pk_t> pks;
	m_groupsGrid.GetSelectedGroupsForActionOnLCP(groups, pks, BusinessObject::UPDATE_TYPE_LCPRENEW);

	ASSERT(groups.size() == pks.size());

	{
		ModuleGroup* pModGroup = dynamic_cast<ModuleGroup*>(&GetModuleBase());
		ASSERT(nullptr != pModGroup);
		if (nullptr != pModGroup)
		{
			const BusinessLifeCyclePolicies& BizLCP = pModGroup->GetGroupLifeCyclePolicies();
			ASSERT(BizLCP.IsPresent());
			for (auto& bg : groups)
				bg.SetLCPId(BizLCP.GetID());
		}
	}

	ASSERT(!groups.empty());
	if (!groups.empty())
	{
		UpdatedObjectsGenerator<BusinessGroup> updatedGroupsGenerator;
		updatedGroupsGenerator.SetObjects(groups);
		updatedGroupsGenerator.SetPrimaryKeys(pks);

		CommandInfo info;
		info.Data() = updatedGroupsGenerator;
		info.SetFrame(this);
		info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateLCPGroup, info, { &GetGrid(), g_ActionNamePolicyUpdate/*TODO*/ , GetAutomationActionRecording(), GetAutomationAction() }));
	}
}

void FrameGroups::OnUpdatePolicyPerGroupRenew(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsConnected() && IsThereLCP() && m_groupsGrid.HasSelectedRowReadyForSelectedPolicyUpdate(BusinessObject::UPDATE_TYPE_LCPRENEW));
}

void FrameGroups::OnShowTenantSettings()
{
    ShowTenantSettings();
}

void FrameGroups::OnUpdateShowTenantSettings(CCmdUI* pCmdUI)
{
	auto session = GetSapio365Session();
    pCmdUI->Enable(session && session->IsConnected() && (!session->IsUseRoleDelegation() || 
														 RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(session->GetRoleDelegationID(), GetSapio365Session()).HasPrivilege(RoleDelegationUtil::RBAC_TENANT_GROUP_EDITSETTINGS)));
}

void FrameGroups::OnPolicyPerGroupAddGroup()
{
	vector<BusinessGroup> groups;
	vector<row_pk_t> pks;
	m_groupsGrid.GetSelectedGroupsForActionOnLCP(groups, pks, BusinessObject::UPDATE_TYPE_LCPADDGROUP);

	ASSERT(groups.size() == pks.size());

	{
		ModuleGroup* pModGroup = dynamic_cast<ModuleGroup*>(&GetModuleBase());
		ASSERT(nullptr != pModGroup);
		if (nullptr != pModGroup)
		{
			const BusinessLifeCyclePolicies& bizLCP = pModGroup->GetGroupLifeCyclePolicies();
			ASSERT(bizLCP.IsPresent());
			for (auto& bg : groups)
				bg.SetLCPId(bizLCP.GetID());
		}
	}

	ASSERT(!groups.empty());
	if (!groups.empty())
	{
		UpdatedObjectsGenerator<BusinessGroup> updatedGroupsGenerator;
		updatedGroupsGenerator.SetObjects(groups);
		updatedGroupsGenerator.SetPrimaryKeys(pks);

		CommandInfo info;
		info.Data() = updatedGroupsGenerator;
		info.SetFrame(this);
		info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateLCPGroup, info, {&GetGrid(), g_ActionNamePolicyCreate/*TODO*/ , GetAutomationActionRecording(), GetAutomationAction() }));
	}
}

void FrameGroups::OnUpdatePolicyPerGroupAddGroup(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsConnected() && IsThereLCP() && IsLCPTypeSelected() && m_groupsGrid.HasSelectedRowReadyForSelectedPolicyUpdate(BusinessObject::UPDATE_TYPE_LCPADDGROUP));
}

void FrameGroups::OnPolicyPerGroupRemoveGroup()
{
	vector<BusinessGroup> groups;
	vector<row_pk_t> pks;
	m_groupsGrid.GetSelectedGroupsForActionOnLCP(groups, pks, BusinessObject::UPDATE_TYPE_LCPREMOVEGROUP);

	ASSERT(groups.size() == pks.size());

	{
		ModuleGroup* pModGroup = dynamic_cast<ModuleGroup*>(&GetModuleBase());
		ASSERT(nullptr != pModGroup);
		if (nullptr != pModGroup)
		{
			const BusinessLifeCyclePolicies& bizLCP = pModGroup->GetGroupLifeCyclePolicies();
			ASSERT(bizLCP.IsPresent());
			for (auto& bg : groups)
				bg.SetLCPId(bizLCP.GetID());
		}
	}

	ASSERT(!groups.empty());
	if (!groups.empty())
	{
		UpdatedObjectsGenerator<BusinessGroup> updatedGroupsGenerator;
		updatedGroupsGenerator.SetObjects(groups);
		updatedGroupsGenerator.SetPrimaryKeys(pks);

		CommandInfo info;
		info.Data() = updatedGroupsGenerator;
		info.SetFrame(this);
		info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
		CommandDispatcher::GetInstance().Execute( Command(GetLicenseContext(), nullptr, Command::ModuleTarget::Group, Command::ModuleTask::UpdateLCPGroup, info, {&GetGrid(), g_ActionNamePolicyUpdate/*TODO*/ , GetAutomationActionRecording(), GetAutomationAction() }));
	}
}

void FrameGroups::OnUpdatePolicyPerGroupRemoveGroup(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsThereLCP() && IsLCPTypeSelected() && m_groupsGrid.HasSelectedRowReadyForSelectedPolicyUpdate(BusinessObject::UPDATE_TYPE_LCPREMOVEGROUP));
}

void FrameGroups::applyOnPremSpecific(bool p_SelectednOnly)
{
	GetGrid().StoreSelection();

	{
		CommandInfo info;
		info.Data() = m_groupsGrid.GetOnPremiseChanges(p_SelectednOnly);
		info.SetFrame(this);
		info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

		CommandDispatcher::GetInstance().Execute(
			Command(GetLicenseContext(),
				GetSafeHwnd(),
				Command::ModuleTarget::Group,
				Command::ModuleTask::ApplyOnPremChanges,
				info,
				{ &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
	}

	GetGrid().RestoreSelection();
}


//void FrameGroups::OnApplyAllOnPrem()
//{
//	YCodeJockMessageBox confirmation(this,
//		DlgMessageBox::eIcon_Question,
//		_YTEXT("Save All On-Premises Changes"),
//		_YTEXT("Are you sure?"),
//		_YTEXT(""),
//		{ { IDOK, _YTEXT("Yes") },{ IDCANCEL, _YTEXT("No") } });
//	if (confirmation.DoModal() == IDOK)
//		applyOnPremSpecific(false);
//}
//
//void FrameGroups::OnUpdateApplyAllOnPrem(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable( IsConnected() && HasNoTaskRunning() && m_groupsGrid.HasOnPremiseChanges(false));
//}
//
//void FrameGroups::OnApplySelectedOnPrem()
//{
//	YCodeJockMessageBox confirmation(this,
//		DlgMessageBox::eIcon_Question,
//		_YTEXT("Save Selected On-Premises Changes"),
//		_YTEXT("Are you sure?"),
//		_YTEXT(""),
//		{ { IDOK, _YEXT("Yes") },{ IDCANCEL, _YTEXT("No") } });
//	if (confirmation.DoModal() == IDOK)
//		applyOnPremSpecific(true);
//}
//
//void FrameGroups::OnUpdateApplySelectedOnPrem(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable(IsConnected() && HasNoTaskRunning() && m_groupsGrid.HasOnPremiseChanges(true));
//}
//
//void FrameGroups::OnRevertAllOnPrem()
//{
//	YCodeJockMessageBox confirmation(
//		this,
//		DlgMessageBox::eIcon_ExclamationWarning,
//		_T("Undo all On-Premises changes"),
//		_YTEXT("Are you sure?"),
//		_T("All the On-Premises pending changes will be lost."),
//		{ { IDOK, _YTEXT("Yes") },{ IDCANCEL, _YTEXT("No") } }
//	);
//
//	if (IDOK == confirmation.DoModal())
//		m_groupsGrid.RevertOnPrem();
//}
//
//void FrameGroups::OnUpdateRevertAllOnPrem(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable(m_groupsGrid.HasOnPremiseChanges() && HasNoTaskRunning());
//}
//
//void FrameGroups::OnRevertSelectedOnPrem()
//{
//	YCodeJockMessageBox confirmation(
//		this,
//		DlgMessageBox::eIcon_ExclamationWarning,
//		_T("Undo selected On-Premises changes"),
//		_YTEXT("Are you sure?"),
//		_TYTEXT("Pending On-Premises changes in selection will be lost."),
//		{ { IDOK, _YETXT("Yes") },{ IDCANCEL, _YTEXT("No") } }
//	);
//
//	if (IDOK == confirmation.DoModal())
//		m_groupsGrid.RevertOnPrem(true);
//}
//
//void FrameGroups::OnUpdateRevertSelectedOnPrem(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable(m_groupsGrid.HasOnPremiseChanges(true) && HasNoTaskRunning());
//}

void FrameGroups::OnUpdateForceSyncOnPrem(CCmdUI* pCmdUI)
{	
	bool hasComputerName = !GetBusinessObjectManager()->GetGraphCache().GetAADComputerName().empty();
	pCmdUI->Enable(HasNoTaskRunning() && hasComputerName);
}

void FrameGroups::OnForceSyncOnPrem()
{
	auto actionCmd = std::make_shared<ForceSyncOnPremCommand>(*this);
	CommandDispatcher::GetInstance().Execute(Command::CreateCommand(actionCmd, *this, Command::ModuleTarget::Group));
}