#pragma once

#include "FrameGroupCommon.h"
#include "ModuleGroup.h"
#include "GridGroupsAuthorsHierarchy.h"

class FrameGroupAuthorsHierarchy : public FrameGroupCommon
{
public:

    DECLARE_DYNAMIC(FrameGroupAuthorsHierarchy)
	using FrameGroupCommon::FrameGroupCommon;
    virtual ~FrameGroupAuthorsHierarchy() override = default;

	virtual void ShowHierarchies(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge) override;

	virtual O365Grid& GetGrid() override;

protected:
	virtual void createGrid() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:

	GridGroupsAuthorsHierarchy m_groupsGrid;
};

