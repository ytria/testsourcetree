#include "GridGroups.h"

#include "AutomationWizardGroups.h"
#include "BasicGridSetup.h"
#include "BatchImporter.h"
#include "BusinessGroupConfiguration.h"
#include "CacheGridTools.h"
#include "DlgBusinessGroupEditHTML.h"
#include "DlgGroupsModuleOptions.h"
#include "DlgOnPremAADConnectServer.h"
#include "DlgOnPremGroupEditHTML.h"
#include "FlatObjectListFrameRefresher.h"
#include "FrameGroups.h"
#include "GraphCache.h"
#include "GroupDeserializer.h"
#include "GridUpdater.h"
#include "GridUtil.h"
#include "OnPremModificationsApplyer.h"
#include "OnPremModificationsReverter.h"
#include "ShowOnPremGroupsCommand.h"
#include "UpdatedObjectsGenerator.h"
#include "YCallbackMessage.h"
#include "YtriaFieldsConstants.h"
#include "../Resource.h"

BEGIN_MESSAGE_MAP(GridGroups, ModuleO365Grid)
    ON_COMMAND(ID_GROUPGRID_LOADMORE,			OnGroupLoadMore)
	ON_UPDATE_COMMAND_UI(ID_GROUPGRID_LOADMORE, OnUpdateGroupLoadMore)
	ON_COMMAND(ID_GROUPGRID_REFRESH_ONPREM,		OnRefreshOnPrem)
	ON_UPDATE_COMMAND_UI(ID_GROUPGRID_REFRESH_ONPREM, OnUpdateRefreshOnPrem)

	ON_COMMAND(ID_GROUPGRID_CREATETEAM, OnCreateTeam)
	ON_UPDATE_COMMAND_UI(ID_GROUPGRID_CREATETEAM, OnUpdateCreateTeam)

	ON_UPDATE_COMMAND_UI(ID_GROUPGRID_BATCHIMPORT, OnUpdateBatchImport)
	ON_COMMAND(ID_GROUPGRID_BATCHIMPORT, OnBatchImport)

	ON_UPDATE_COMMAND_UI(ID_GROUPGRID_CHANGEPREFILTER, OnUpdateChangePreFilter)
	ON_COMMAND(ID_GROUPGRID_CHANGEPREFILTER, OnChangePreFilter)

	ON_COMMAND(ID_MODULEGRID_EDIT, OnEdit)
	ON_UPDATE_COMMAND_UI(ID_MODULEGRID_EDIT, OnUpdateEdit)
	ON_COMMAND(ID_GROUPGRID_EDITONPREM, OnEditOnPrem)
	ON_UPDATE_COMMAND_UI(ID_GROUPGRID_EDITONPREM, OnUpdateEditOnPrem)
	ON_COMMAND(ID_GROUPGRID_EDITO365, OnEditO365)
	ON_UPDATE_COMMAND_UI(ID_GROUPGRID_EDITO365, OnUpdateEditO365)

	NEWMODULE_COMMAND(ID_GROUPSGRID_SHOW_RECYCLEBIN, ID_GROUPSGRID_SHOW_RECYCLEBIN_FRAME, ID_GROUPSGRID_SHOW_RECYCLEBIN_NEWFRAME,	OnShowRecycleBin,	OnUpdateShowRecycleBin)
END_MESSAGE_MAP()

wstring GridGroups::g_PolicyHasChangedStatus;
wstring GridGroups::g_GroupNeverExpire;
wstring GridGroups::g_PolicyErrorStatus;

GridGroups::GridGroups()
	: m_Frame(nullptr)
	, m_ColumnComputedAllowToAddGuests(nullptr)
	, m_ForceSorting(false)
{
	m_MayContainUnscopedUserGroupOrSite = true;

	UseDefaultActions(O365Grid::ACTION_CREATE | O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT/* | O365Grid::ACTION_VIEW*/);
	EnableGridModifications();

	SetModApplyer(std::make_shared<OnPremModificationsApplyer<GridGroups>>(*this));
	SetModReverter(std::make_shared<OnPremModificationsReverter<GridGroups>>(*this));

	if (g_PolicyHasChangedStatus.empty())
		g_PolicyHasChangedStatus = YtriaTranslate::Do(GridGroups_GridGroups_1, _YLOC("-- Policy has changed, use the 'Load Info' button to refresh --")).c_str();

	if (g_GroupNeverExpire.empty())
		g_GroupNeverExpire = YtriaTranslate::Do(GridGroups_GridGroups_2, _YLOC("Never Expire")).c_str();

	if (g_PolicyErrorStatus.empty())
		g_PolicyErrorStatus = YtriaTranslate::Do(GridGroups_GridGroups_3, _YLOC("N/A")).c_str();

	initWizard<AutomationWizardGroups>(_YTEXT("Automation\\Groups"));
}

void GridGroups::SetForceSorting(bool p_ForceSorting)
{
	m_ForceSorting = p_ForceSorting;
}

O365Grid::SnapshotCaps GridGroups::GetSnapshotCapabilities() const
{
	return { true };
}

GridGroups::OnPremiseChanges GridGroups::GetOnPremiseChanges(bool p_SelectedRowsOnly /*= false*/)
{
	OnPremiseChanges onPremiseChanges;

	if (m_OnPremModifications && m_OnPremModifications->HasModifications())
	{
		for (const auto& fieldMod : m_OnPremModifications->GetRowFieldModifications())
		{
			if (Modification::IsLocal()(fieldMod))
			{
				const auto& rowKey = fieldMod->GetRowKey();

				// FIXME: We have no choice that finding the row to get the ObjectGUID. 
				// Eventually consider putting it in PK so that we have it in field update directly.
				auto row = /*p_SelectedRowsOnly ? */GetRowIndex().GetExistingRow(rowKey)/* : nullptr*/;
				if (!p_SelectedRowsOnly || nullptr != row && row->IsSelected())
				{
					onPremiseChanges.m_GroupsToUpdate.emplace_back();
					onPremiseChanges.m_GroupsToUpdate.back().first = rowKey;
					auto& group = onPremiseChanges.m_GroupsToUpdate.back().second;
					ASSERT(nullptr != row);
					m_Template.m_OnPremCols.SetValue(group, m_Template.m_OnPremCols.m_ColObjectGUID, row->GetField(m_Template.m_OnPremCols.m_ColObjectGUID));
					for (const auto& fu : fieldMod->GetFieldUpdates())
					{
						if (fu->GetNewValue())
							m_Template.m_OnPremCols.SetValue(group, GetColumnByID(fu->GetColumnID()), *fu->GetNewValue());
					}
				}
			}
		}
	}

	return onPremiseChanges;
}

bool GridGroups::HasOnPremiseChanges(bool p_SelectedRowsOnly /*= false*/) const
{
	return m_OnPremModifications
		&& (p_SelectedRowsOnly	? std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& row) {return m_OnPremModifications->IsModified(row); })
								:  m_OnPremModifications->HasModifications());
}

GridFieldModificationsOnPrem& GridGroups::GetOnPremModifications()
{
	ASSERT(m_OnPremModifications);
	return *m_OnPremModifications;
}

const GridFieldModificationsOnPrem& GridGroups::GetOnPremModifications() const
{
	ASSERT(m_OnPremModifications);
	return *m_OnPremModifications;
}

void GridGroups::RevertOnPrem(bool p_SelectedRowsOnly /*= false*/, bool p_UpdateGridIfNeeded/* = true*/)
{
	if (HasOnPremiseChanges())
	{
		CWaitCursor c;
		bool shouldUpdate = false;

		if (p_SelectedRowsOnly)
		{
			TemporarilyImplodeNonGroupRowsAndExec(*this
				, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS
				, [this, &shouldUpdate, p_UpdateGridIfNeeded](const bool p_HasRowsToReExplode)
				{
					shouldUpdate = GetOnPremModifications().RevertSelectedRows() && p_UpdateGridIfNeeded && !p_HasRowsToReExplode;
				})();
		}
		else
		{
			ScopedImplodeRows scopedImplodeRows(*this, GetOnPremModifications().GetModifiedRows(true, true), p_UpdateGridIfNeeded);
			shouldUpdate = GetOnPremModifications().RevertAll() && p_UpdateGridIfNeeded && !scopedImplodeRows.HasExplosionsToRestore();
		}

		if (shouldUpdate)
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}
}

bool GridGroups::HasRevertableModificationsPending(bool inSelectionOnly /*= false*/)
{
	return HasModificationsPending(inSelectionOnly) || HasOnPremiseChanges(inSelectionOnly);
}

void GridGroups::EmptyGrid()
{
	ASSERT(!HasModificationsPending());
	RemoveAllRows();
	m_O365RowPk.clear();
	m_OnPremRowPk.clear();
}

void GridGroups::customizeGrid()
{
	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_GRIDGROUPS_ACCELERATOR));

	{
		BasicGridSetup setup(GridUtil::g_AutoNameGroups, LicenseUtil::g_codeSapio365groups);
		setup.Setup(*this, true);
		m_Template.m_ColumnLastRequestDateTime = setup.GetColumnLastRequestDateTime();
		m_Template.GetRequestDateColumn(GBI::LIST) = m_Template.m_ColumnLastRequestDateTime;
	}

	const auto& gcfg = BusinessGroupConfiguration::GetInstance();

	{
		auto cat = AddCategoryTop(_T("Group Info"), {});

		{
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_DISPLAYNAME), g_FamilyInfo, { g_ColumnsPresetDefault }));
			
			// Right now loadmore is a block combination, so use one block
			// Be careful, some blocks are not requested for every group type.
			m_Template.GetRequestDateColumn(GBI::OWNERS) = addColumnIsLoadMore(_YUID("additionalInfoStatus"), YtriaTranslate::Do(GridGroups_customizeGrid_2, _YLOC("Status - Load Additional Information")).c_str(), true);
			AddColumnToCategory(cat, m_Template.GetRequestDateColumn(GBI::OWNERS));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_ISTEAM), g_FamilyInfo, { g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(YTRIA_GROUP_ISYAMMER), g_FamilyInfo, { g_ColumnsPresetMore, g_ColumnsPresetDefault }));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_GROUPTYPE), g_FamilyInfo, { g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_DYNAMICMEMBERSHIP), g_FamilyInfo, { g_ColumnsPresetDefault }));
			
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE), g_FamilyInfo, { g_ColumnsPresetMore }));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_DESCRIPTION), g_FamilyInfo, { g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_VISIBILITY), g_FamilyInfo, { g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_CREATEDDATETIME), g_FamilyInfo, { g_ColumnsPresetDefault }));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_MAILNICKNAME), g_FamilyInfo, {}));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_MAIL), g_FamilyInfo, { g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("memberCount"), g_FamilyInfo, { g_ColumnsPresetDefault, g_ColumnsPresetMore }));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_CLASSIFICATION), g_FamilyInfo, { g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_PROXYADDRESSES), g_FamilyInfo, {}));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_RESOURCEBEHAVIOROPTIONS), g_FamilyInfo, {}));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_RESOURCEPROVISIONINGOPTIONS), g_FamilyInfo, {}));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_PREFERREDDATALOCATION), g_FamilyInfo, {}));

			/* Beta API */
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_MEMBERSHIPRULE), g_FamilyInfo, { g_ColumnsPresetMore }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_THEME), g_FamilyInfo, { g_ColumnsPresetMore }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_PREFERREDLANGUAGE), g_FamilyInfo, { g_ColumnsPresetMore }));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_RENEWEDDATETIME), g_FamilyInfo, { g_ColumnsPresetDefault }));
			
			{
				static const wstring g_FamilyLicensing = _T("Licensing");
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_LICENSEPROCESSINGSTATE), g_FamilyLicensing, {}));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("ASSIGNEDLICENSENAMES"), g_FamilyLicensing, { g_ColumnsPresetDefault }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_ASSIGNEDLICENSES), g_FamilyLicensing, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("assignedLicenses.skuId"), g_FamilyLicensing, { g_ColumnsPresetTech }));
			}

			{
				static const wstring g_FamilyTypeInfo = YtriaTranslate::Do(GridGroups_customizeGrid_18, _YLOC("Type Info")).c_str();
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_GROUPTYPES), g_FamilyTypeInfo, {}));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_SECURITYENABLED), g_FamilyTypeInfo, {}));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_MAILENABLED), g_FamilyTypeInfo, {}));
			}

			{
				static const wstring g_FamilyLoadMore = YtriaTranslate::Do(GridGroups_customizeGrid_22, _YLOC("More Info")).c_str();
				//AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_ISSUBSCRIBEDBYMAIL),					g_FamilyLoadMore, { g_ColumnsPresetMore, g_ColumnsPresetDefault }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_ALLOWEXTERNALSENDERS), g_FamilyLoadMore, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_UNSEENCOUNT), g_FamilyLoadMore, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_AUTOSUBSCRIBENEWMEMBERS), g_FamilyLoadMore, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_HIDEFROMOUTLOOKCLIENTS), g_FamilyLoadMore, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_HIDEFROMADDRESSLISTS), g_FamilyLoadMore, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("ownerusernames"), g_FamilyLoadMore, { g_ColumnsPresetMore, g_ColumnsPresetDefault }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("owneruserids"), g_FamilyLoadMore, { g_ColumnsPresetMore, g_ColumnsPresetTech }));

				m_ColumnComputedAllowToAddGuests = AddColumnCheckBox(_YUID("statusAllowToAddGuests"), gcfg.GetTitle(_YUID("statusAllowToAddGuests")), g_FamilyLoadMore, g_ColumnSize6, { g_ColumnsPresetMore, g_ColumnsPresetDefault });
				AddColumnToCategory(cat, m_ColumnComputedAllowToAddGuests);

				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(YTRIA_GROUP_GROUPUNIFIEDGUESTSETTINGACTIVATED), g_FamilyLoadMore, { g_ColumnsPresetMore, g_ColumnsPresetTech }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTS), g_FamilyLoadMore, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_SECURITYIDENTIFIER), g_FamilyLoadMore, {}));
			}

			{
				static const wstring g_FamilyCreatedOnBehalfOf = YtriaTranslate::Do(GridGroups_customizeGrid_30, _YLOC("Created On Behalf Of")).c_str();
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("createdOnBehalfOfUserId"), g_FamilyCreatedOnBehalfOf, { g_ColumnsPresetMore, g_ColumnsPresetTech }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("createdOnBehalfOfUserDisplayName"), g_FamilyCreatedOnBehalfOf, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("createdOnBehalfOfUserPrincipalName"), g_FamilyCreatedOnBehalfOf, { g_ColumnsPresetMore }));
			}
			
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("expirationPolicyStatus"), g_FamilyInfo, { g_ColumnsPresetMore, g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("expireOn"), g_FamilyInfo, { g_ColumnsPresetMore, g_ColumnsPresetDefault }));

			AddColumnToCategory(cat, addColumnGraphID());
		}
	}

	{
		auto cat = AddCategoryTop(_T("Team Info"), {});

		{
			static const wstring g_FamilyTeam = YtriaTranslate::Do(GridGroups_customizeGrid_54, _YLOC("Team")).c_str();
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("isArchived"), g_FamilyTeam, { g_ColumnsPresetMore, g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("setSpoSiteReadOnlyForMembers"), g_FamilyTeam, {}));
			
			{
				static const wstring g_FamilyTeamMemberSettings = YtriaTranslate::Do(GridGroups_customizeGrid_34, _YLOC("Team Member Settings")).c_str();
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("teamMemberSettingsAllowCreateUpdateChannels"), g_FamilyTeamMemberSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("teamMemberSettingsAllowCreatePrivateChannels"), g_FamilyTeamMemberSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("teamMemberSettingsAllowDeleteChannels"), g_FamilyTeamMemberSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("teamMemberSettingsAllowAddRemoveApps"), g_FamilyTeamMemberSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("teamMemberSettingsAllowCreateUpdateRemoveTabs"), g_FamilyTeamMemberSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("teamMemberSettingsAllowCreateUpdateRemoveConnectors"), g_FamilyTeamMemberSettings, { g_ColumnsPresetMore }));
			}

			{
				static const wstring g_FamilyTeamGuestSettings = YtriaTranslate::Do(GridGroups_customizeGrid_40, _YLOC("Team Guest Settings")).c_str();
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("guestSettingsAllowCreateUpdateChannels"), g_FamilyTeamGuestSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("guestSettingsAllowDeleteChannels"), g_FamilyTeamGuestSettings, { g_ColumnsPresetMore }));
			}

			{
				static const wstring g_FamilyTeamMessagingSettings = YtriaTranslate::Do(GridGroups_customizeGrid_43, _YLOC("Team Messaging Settings")).c_str();
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("messagingSettingsAllowUserEditMessages"), g_FamilyTeamMessagingSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("messagingSettingsAllowUserDeleteMessages"), g_FamilyTeamMessagingSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("messagingSettingsAllowOwnerDeleteMessages"), g_FamilyTeamMessagingSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("messagingSettingsAllowTeamMentions"), g_FamilyTeamMessagingSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("messagingSettingsAllowChannelMentions"), g_FamilyTeamMessagingSettings, { g_ColumnsPresetMore }));
			}

			{
				static const wstring g_FamilyTeamFunSettings = YtriaTranslate::Do(GridGroups_customizeGrid_49, _YLOC("Team Fun Settings")).c_str();
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("funSettingsAllowGiphy"), g_FamilyTeamFunSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("funSettingsGiphyContentRating"), g_FamilyTeamFunSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("funSettingsAllowStickersAndMemes"), g_FamilyTeamFunSettings, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("funSettingsAllowCustomMemes"), g_FamilyTeamFunSettings, { g_ColumnsPresetMore }));
			}
			
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("webUrl"), g_FamilyTeam, { g_ColumnsPresetMore }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("internalId"), g_FamilyTeam, { g_ColumnsPresetMore, g_ColumnsPresetTech }));
		}		
	}
    
	{
		static const wstring g_FamilyOnPremises = YtriaTranslate::Do(GridGroups_customizeGrid_14, _YLOC("OnPremises")).c_str();
		AddCategoryTop(_T("On-Premises Info"),
			{
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_ONPREMISESLASTSYNCDATETIME),		g_FamilyOnPremises, {}),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_ONPREMISESSECURITYIDENTIFIER),	g_FamilyOnPremises, { g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_GROUP_ONPREMISESSYNCENABLED),			g_FamilyOnPremises, {}),

				m_Template.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY),				g_FamilyOnPremises, {}),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME),		g_FamilyOnPremises, {}),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR),	g_FamilyOnPremises, {}),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE),				g_FamilyOnPremises, {}),
			});
	}

	{
		static const wstring g_FamilyDrive = _T("Doc Library");
		AddCategoryTop(_T("Doc Library Info"),
			{
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVENAME),					g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEID),					g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEDESCRIPTION),			g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEQUOTASTATE),			g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEQUOTAUSED),			g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetDefault }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEQUOTAREMAINING),		g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEQUOTADELETED),			g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEQUOTATOTAL),			g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetDefault }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATIONTIME),			g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVELASTMODIFIEDDATETIME), g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVETYPE),					g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEWEBURL),				g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYUSEREMAIL),	g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYUSERID),		g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYAPPNAME),		g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYAPPID),		g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYDEVICENAME),	g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYDEVICEID),	g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERUSERNAME),		g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERUSERID),			g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERUSEREMAIL),		g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERAPPNAME),			g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERAPPID),			g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERDEVICENAME),		g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERDEVICEID),		g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
			});
	}
	
	AddColumnForRowPK(GetColId());
	m_Template.m_ColumnID = GetColId();
	m_Template.CustomizeGrid(*this);

	addSystemTopCategory();

	if (m_ForceSorting)
		AddSorting(m_Template.m_ColumnDisplayName, GridBackendUtil::ASC);

	m_Frame = dynamic_cast<FrameGroups*>(GetParentFrame());

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_Frame);
}

wstring GridGroups::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, nullptr != m_Template.m_OnPremCols.m_ColCommonDisplayName ? m_Template.m_OnPremCols.m_ColCommonDisplayName : m_Template.m_ColumnDisplayName);
}

void GridGroups::customizeGridPostProcess()
{
	O365Grid::customizeGridPostProcess();
	m_LoadMoreConfig.m_Tooltip	= YtriaTranslate::Do(GridGroups_customizeGridPostProcess_1, _YLOC("To see this information, use the 'Load Info' button")).c_str();
	m_LoadMoreConfig.m_Text		= YtriaTranslate::Do(GridGroups_customizeGridPostProcess_2, _YLOC(" - Use the 'Load Info' button to display additional information - ")).c_str();
	m_LoadMoreConfig.m_Icon		= SetNoFieldTextAndIcon(GetColumnsByPresets({ g_ColumnsPresetMore }), IDB_ICON_LM, m_LoadMoreConfig.m_Tooltip, m_LoadMoreConfig.m_Text);

	SetLoadMoreStatus(true,		YtriaTranslate::Do(GridGroups_customizeGridPostProcess_3, _YLOC("Loaded")).c_str(),		IDB_ICON_LMSTATUS_LOADED);
	SetLoadMoreStatus(false,	YtriaTranslate::Do(GridGroups_customizeGridPostProcess_4, _YLOC("Not Loaded")).c_str(),	IDB_ICON_LMSTATUS_NOTLOADED);
}

void GridGroups::setUpLoadMoreColumn(GridBackendColumn* p_Col)
{
	ASSERT(nullptr != p_Col);
	if (nullptr != p_Col)
	{
		p_Col->SetNoFieldTooltip(m_LoadMoreConfig.m_Tooltip);
		p_Col->SetNoFieldText(m_LoadMoreConfig.m_Text);
		p_Col->SetNoFieldIcon(m_LoadMoreConfig.m_Icon);
	}
}

void GridGroups::setUpNoLoadMoreColumn(GridBackendColumn* p_Col)
{
	ASSERT(nullptr != p_Col);
	if (nullptr != p_Col)
	{
		p_Col->SetNoFieldTooltip(_YTEXT(""));
		p_Col->SetNoFieldText(_YTEXT(""));
		p_Col->SetNoFieldIcon(0);
	}
}

bool GridGroups::HasSelectedRowWithPolicyChangedStatus() const
{
	for (auto row : GetSelectedRows())
	{
		if (isOffice365Group(row)
			&& row->GetField(m_Template.m_ColumnExpirationPolicyStatus).GetValueStr() == g_PolicyHasChangedStatus)
			return true;
	}

	return false;
}

bool GridGroups::HasSelectedRowReadyForSelectedPolicyUpdate(BusinessObject::UPDATE_TYPE p_UpdateType) const
{
	if (BusinessObject::UPDATE_TYPE_LCPADDGROUP == p_UpdateType)
	{
		for (auto row : GetSelectedRows())
		{
			if (isOffice365Group(row)
				&& !row->IsCreated() && !row->IsDeleted() && !row->IsModified() // Warn and revert instead?
				&& row->IsMoreLoaded()
				&& row->GetField(m_Template.m_ColumnExpirationPolicyStatus).GetValueStr() != BusinessLifeCyclePolicies::g_StatusApplied
				&& row->GetField(m_Template.m_ColumnExpirationPolicyStatus).GetValueStr() != g_PolicyHasChangedStatus
				&& !row->IsExplosionAdded()
				&& IsGroupAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_GROUP_EDIT))
				return true;
		}
	}
	else if (BusinessObject::UPDATE_TYPE_LCPREMOVEGROUP == p_UpdateType)
	{
		for (auto row : GetSelectedRows())
		{
			if (isOffice365Group(row)
				&& !row->IsCreated() && !row->IsDeleted() && !row->IsModified() // Warn and revert instead?
				&& row->IsMoreLoaded()
				&& row->GetField(m_Template.m_ColumnExpirationPolicyStatus).GetValueStr() != BusinessLifeCyclePolicies::g_StatusNotApplied
				&& row->GetField(m_Template.m_ColumnExpirationPolicyStatus).GetValueStr() != g_PolicyHasChangedStatus
				&& !row->IsExplosionAdded()
				&& IsGroupAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_GROUP_EDIT))
				return true;
		}
	}
	else if (BusinessObject::UPDATE_TYPE_LCPRENEW == p_UpdateType)
	{
		for (auto row : GetSelectedRows())
		{
			if (!row->IsGroupRow()
				&& !row->IsCreated() && !row->IsDeleted() && !row->IsModified() // Warn and revert instead?
				&& !row->IsExplosionAdded()
				&& IsGroupAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_GROUP_EDIT))
				return true;
		}
	}
	else
	{
		ASSERT(false);
	}

	return false;
}

bool GridGroups::HasNonOffice365GroupsMarkedForDeletion(bool p_InSelection) const
{
	for (auto row : GetBackendRows())
	{
		if ((!p_InSelection || row->IsSelected())
			&& row->IsDeleted() && !isOffice365Group(row))
			return true;
	}

	return false;
}

/*void GridGroups::AddCategories()
{
    auto systemColumns		= addSystemTopCategory();
    auto remainingColumns	= GetBackendColumns();

    vector<GridBackendColumn*> loadMoreColumns;
    //loadMoreColumns.push_back(m_Template.m_ColumnIsSubscribedByMail);
    loadMoreColumns.push_back(m_Template.m_ColumnAllowExternalSenders);
    loadMoreColumns.push_back(m_Template.m_ColumnUnseenCount);
    loadMoreColumns.push_back(m_Template.m_ColumnAutoSubscribeNewMembers);
    loadMoreColumns.push_back(m_Template.m_ColumnOwnerUserNames);
    loadMoreColumns.push_back(m_Template.m_ColumnOwnerUserIDs);
    loadMoreColumns.push_back(m_Template.m_ColumnCreatedOnBehalfOfUserId);
    loadMoreColumns.push_back(m_Template.m_ColumnCreatedOnBehalfOfUserDisplayName);
    loadMoreColumns.push_back(m_Template.m_ColumnCreatedOnBehalfOfUserPrincipalName);
    loadMoreColumns.push_back(m_Template.m_ColumnSettingAllowToAddGuests);
    loadMoreColumns.push_back(m_Template.m_ColumnGroupUnifiedSettingActivated);
	loadMoreColumns.push_back(m_Template.m_ColumnMembersCount);

    vector<GridBackendColumn*> teamColumns;
    teamColumns.push_back(m_Template.m_ColumnTeamMemberSettingsAllowCreateUpdateChannels);
	teamColumns.push_back(m_Template.m_ColumnTeamMemberSettingsAllowCreatePrivateChannels);
    teamColumns.push_back(m_Template.m_ColumnTeamMemberSettingsAllowDeleteChannels);
    teamColumns.push_back(m_Template.m_ColumnTeamMemberSettingsAllowAddRemoveApps);
    teamColumns.push_back(m_Template.m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs);
    teamColumns.push_back(m_Template.m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors);
    teamColumns.push_back(m_Template.m_ColumnTeamGuestSettingsAllowCreateUpdateChannels);
    teamColumns.push_back(m_Template.m_ColumnTeamGuestSettingsAllowDeleteChannels);
    teamColumns.push_back(m_Template.m_ColumnTeamMessagingSettingsAllowUserEditMessages);
    teamColumns.push_back(m_Template.m_ColumnTeamMessagingSettingsAllowUserDeleteMessages);
    teamColumns.push_back(m_Template.m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages);
    teamColumns.push_back(m_Template.m_ColumnTeamMessagingSettingsAllowTeamMentions);
    teamColumns.push_back(m_Template.m_ColumnTeamMessagingSettingsAllowChannelMentions);
    teamColumns.push_back(m_Template.m_ColumnTeamFunSettingsAllowGiphy);
    teamColumns.push_back(m_Template.m_ColumnTeamFunSettingsGiphyContentRating);
    teamColumns.push_back(m_Template.m_ColumnTeamFunSettingsAllowStickersAndMemes);
    teamColumns.push_back(m_Template.m_ColumnTeamFunSettingsAllowCustomMemes);
    teamColumns.push_back(m_Template.m_ColumnTeamWebUrl);

	vector<GridBackendColumn*> policyColumns;
	policyColumns.push_back(m_Template.m_ColumnRenewedDateTime);
	policyColumns.push_back(m_Template.m_ColumnExpireOnDateTime);
	policyColumns.push_back(m_Template.m_ColumnExpirationPolicyStatus);

    remainingColumns.erase(std::remove_if(remainingColumns.begin(), remainingColumns.end(), [=](GridBackendColumn* c)
    {
        return std::find(systemColumns.begin(), systemColumns.end(), c) != systemColumns.end() || std::find(loadMoreColumns.begin(), loadMoreColumns.end(), c) != loadMoreColumns.end()
            || std::find(teamColumns.begin(), teamColumns.end(), c) != teamColumns.end()
			|| std::find(policyColumns.begin(), policyColumns.end(), c) != policyColumns.end();
    }), remainingColumns.end());
    AddCategoryTop(YtriaTranslate::Do(GridGroups_AddCategories_1, _YLOC("Group Info")).c_str(), remainingColumns);
    AddCategoryTop(YtriaTranslate::Do(GridGroups_AddCategories_2, _YLOC("Additional Info")).c_str(), loadMoreColumns);
    AddCategoryTop(YtriaTranslate::Do(GridGroups_AddCategories_3, _YLOC("Team Info")).c_str(), teamColumns);
	AddCategoryTop(YtriaTranslate::Do(GridGroups_AddCategories_4, _YLOC("Expiration Policy")).c_str(), policyColumns);
}*/

void GridGroups::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart /*= 0*/)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_GROUPGRID_CHANGEPREFILTER);

		pPopup->ItemInsert(ID_GROUPGRID_LOADMORE);
		pPopup->ItemInsert(); // Sep

		GridGroupsBaseClass::OnCustomPopupMenu(pPopup, nStart);

		{
			auto pos = pPopup->ItemFindPosForCmdID(ID_MODULEGRID_DELETE);
			if (-1 == pos)
				pos = pPopup->ItemFindPosForCmdID(ID_MODULEGRID_CREATE);
			ASSERT(-1 != pos);
			if (-1 != pos)
			{
				pPopup->ItemInsert(ID_GROUPGRID_CREATETEAM, pos + 1);
				pPopup->ItemInsert(ID_GROUPGRID_BATCHIMPORT, pos + 2);
				//pPopup->ItemInsert(ID_GROUPGRID_EDITONPREM, pos + 3);
			}
		}

		{
			auto pos = pPopup->ItemFindPosForCmdID(ID_GROUPSGRID_SHOWSITES);
			if (-1 != pos)
			{
				pPopup->ItemInsert(ID_GROUPSGRID_SHOW_RECYCLEBIN, pos + 1);
				pPopup->ItemInsert(ID_GROUPSGRID_SHOW_CHANNELS, pos + 2);
			}
			else
			{
				ASSERT(!GridUtil::IsSelectionAtLeastOneOfType<BusinessGroup>(*this));
				pPopup->ItemInsert(ID_GROUPSGRID_SHOW_RECYCLEBIN);
				pPopup->ItemInsert(ID_GROUPSGRID_SHOW_CHANNELS);
				pPopup->ItemInsert(); // Sep
			}
		}
	}
}

void GridGroups::InitializeCommands()
{
    GridGroupsBaseClass::InitializeCommands();

	//m_RegisteredBusinessTypes.insert(rttr::type::get<HybridGroup>().get_id());

	std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_GROUPGRID_REFRESH_ONPREM;
		_cmd.m_sMenuText = _T("On-premises groups");
		_cmd.m_sToolbarText = _T("On-premises groups");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_GROUPGRID_REFRESH_ONPREM, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_GROUPGRID_REFRESH_ONPREM_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Load on-premises info"),
			_T("Retrieve on-premises group properties from Active Directory.\nUse submenu 'AAD Connect options' to allow syncing of AD DS/Azure AD directly from sapio365."),
			_cmd.m_sAccelText);
	}

    {
        CExtCmdItem _cmd;

        _cmd.m_nCmdID = ID_GROUPGRID_LOADMORE;
        _cmd.m_sMenuText = YtriaTranslate::Do(GridGroups_InitializeCommands_15, _YLOC("Load Info")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridGroups_InitializeCommands_16, _YLOC("Load Info")).c_str();
        _cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
        g_CmdManager->CmdSetup(profileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LOADMORE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_GROUPGRID_LOADMORE, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridGroups_InitializeCommands_17, _YLOC("Load Additional Information")).c_str(),
			YtriaTranslate::Do(GridGroups_InitializeCommands_19, _YLOC("Load detailed group information for the selected entries only.\nThis includes information such as external sender status, unseen count, Team information, expiration policy status and more.")).c_str(),
			_cmd.m_sAccelText);
    }

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_GROUPSGRID_SHOW_RECYCLEBIN;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroups_InitializeCommands_18, _YLOC("Show Recycle Bin...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroups_InitializeCommands_20, _YLOC("Recycle Bin...")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_RECYCLEBIN_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_GROUPSGRID_SHOW_RECYCLEBIN, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridGroups_InitializeCommands_21, _YLOC("Show Recycle Bin")).c_str(),
			YtriaTranslate::Do(GridGroups_InitializeCommands_22, _YLOC("This will show all deleted groups.")).c_str(),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_GROUPSGRID_SHOW_RECYCLEBIN_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroups_InitializeCommands_23, _YLOC("Show Recycle Bin...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_GROUPSGRID_SHOW_RECYCLEBIN_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroups_InitializeCommands_24, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_GROUPGRID_CREATETEAM;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroups_InitializeCommands_29, _YLOC("Convert to Team")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroups_InitializeCommands_29, _YLOC("Convert to Team")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_CONVERTTOTEAM_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_GROUPGRID_CREATETEAM, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridGroups_InitializeCommands_29, _YLOC("Convert to Team")).c_str(),
			YtriaTranslate::Do(GridGroups_InitializeCommands_32, _YLOC("This will create a Team for the selected groups")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_GROUPGRID_BATCHIMPORT;
		_cmd.m_sMenuText = _T("Import Groups");
		_cmd.m_sToolbarText = _T("Import Groups");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_GROUPGRID_BATCHIMPORT, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_BATCHIMPORT_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Import Multiple Groups"),
			_T("Add multiple groups to Office 365 by importing their properties from a file (Excel or CSV).\nNote: Only imports the first sheet of a workbook.\nColumns can be auto mapped to the grid if the imported file already contains column IDs or column titles in their headings."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_GROUPGRID_CHANGEPREFILTER;
		_cmd.m_sMenuText = _T("Change Options");
		_cmd.m_sToolbarText = _T("Change Options");
		_cmd.m_sTipTool = _T("Adjust filter.");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		// FIXME: specific image
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANGEMODULEOPTIONS_16x16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_GROUPGRID_CHANGEPREFILTER, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Change Options"),
			_T("Adjust filter."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_GROUPGRID_EDITO365;
		_cmd.m_sMenuText = _T("Edit O365");
		_cmd.m_sToolbarText = _T("Edit O365");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_GROUPGRID_EDITO365, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_EDIT_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Edit O365"),
			_T("Edit O365"),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_GROUPGRID_EDITONPREM;
		_cmd.m_sMenuText = _T("Edit On-Premises");
		_cmd.m_sToolbarText = _T("Edit On-Premises");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_GROUPGRID_EDITONPREM, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_GROUPS_EDITONPREM_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Edit On-Premises"),
			_T("Edit On-Premises"),
			_cmd.m_sAccelText);
	}
}

void GridGroups::OnEdit()
{
	bool regular = false;
	YTestCmdUI testUI;
	{
		GridGroupsBaseClass::OnUpdateEdit(&testUI);
		regular = testUI.m_bEnabled;
	}

	bool onPrem = false;
	{
		OnUpdateEditOnPrem(&testUI);
		onPrem = testUI.m_bEnabled;
	}
	ASSERT(regular || onPrem);

	enum : INT_PTR
	{
		CANCEL = IDCANCEL,
		EDITREGULAR = IDOK,
		EDITONPREM = IDYES,
	};
	const auto result = [=] {
		if (regular && onPrem)
		{
			YCodeJockMessageBox confirmation(this,
				DlgMessageBox::eIcon_Question,
				_T("What would you like to edit?"),
				_YTEXT(""),
				_YTEXT(""),
				{ { EDITREGULAR, _T("Edit O365") }, { EDITONPREM, _T("Edit On-Premises") }, { CANCEL, _T("Cancel") } });
			return confirmation.DoModal();
		}
		return regular ? (INT_PTR)EDITREGULAR : (INT_PTR)EDITONPREM;
	}();

	if (EDITREGULAR == result)
	{
		GridGroupsBaseClass::OnEdit();
	}
	else if (EDITONPREM == result)
	{
		OnEditOnPrem();
	}
}

void GridGroups::OnUpdateEdit(CCmdUI* pCmdUI)
{
	BOOL enabled = FALSE;

	YTestCmdUI testUI;
	if (!enabled)
	{
		GridGroupsBaseClass::OnUpdateEdit(&testUI);
		enabled = testUI.m_bEnabled;
	}

	if (!enabled)
	{
		OnUpdateEditOnPrem(&testUI);
		enabled = testUI.m_bEnabled;
	}

	pCmdUI->Enable(enabled);

	setTextFromProfUISCommand(*pCmdUI);
} 

void GridGroups::OnRefreshOnPrem()
{
	ModuleUtil::ClearErrorRowsStatus(*this);

	auto actionCmd = std::make_shared<ShowOnPremGroupsCommand>(*m_Frame);
	CommandDispatcher::GetInstance().Execute(Command::CreateCommand(actionCmd, *m_Frame, Command::ModuleTarget::Group));
}

void GridGroups::OnUpdateRefreshOnPrem(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(CanRefreshOnPrem());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroups::OnEditOnPrem()
{
	TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool /*p_HasRowsToReExplode*/)
		{
			std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, GetPrivilegeEdit(), [this](GridBackendRow* p_Row)
				{
					return nullptr != p_Row && (m_Template.IsOnPremMetatype(*p_Row) || m_Template.IsHybridMetatype(*p_Row));
				});

			ASSERT(!selectedRows.empty());
			if (!selectedRows.empty())
			{
				bool isEditCreate = true;
				vector<OnPremiseGroup> onPremiseGroups;
				std::set<wstring> fieldErrors;

				for (auto row : selectedRows)
				{
					if (nullptr != row)
					{
						onPremiseGroups.emplace_back(getOnPremiseGroup(row));
						//isEditCreate = isEditCreate && (objectRow->IsCreated() || businessObjects.back().HasFlag(BusinessObject::CREATED));

						getFieldsThatHaveErrors(row, fieldErrors);
					}
				}

				DlgOnPremGroupEditHTML dlg(onPremiseGroups, DlgFormsHTML::Action::EDIT, this);
				if (IDOK == dlg.DoModal())
				{
					GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
					for (const auto& group : onPremiseGroups)
						m_Template.UpdateRow(*this, group, m_O365RowPk, m_OnPremRowPk, GridTemplate::UpdateFieldOption::IS_MODIFICATION);
				}
			}
		})();
}

void GridGroups::OnUpdateEditOnPrem(CCmdUI* pCmdUI)
{
	const auto condition = [this](GridBackendRow* p_Row)
	{
		return nullptr != p_Row && (m_Template.IsOnPremMetatype(*p_Row) || m_Template.IsHybridMetatype(*p_Row));
	};

	pCmdUI->Enable(hasNoTaskRunning() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, GetPrivilegeEdit(), condition, true));

	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroups::OnEditO365()
{
	GridGroupsBaseClass::OnEdit();
}

void GridGroups::OnUpdateEditO365(CCmdUI* pCmdUI)
{
	GridGroupsBaseClass::OnUpdateEdit(pCmdUI);
}

void GridGroups::updateExpirationDate(GridBackendRow* p_Row)
{
	ASSERT(isOffice365Group(p_Row));

	if (m_LastGlobalLCPStatus == BusinessLifeCyclePolicies::g_StatusApplied
		|| m_LastGlobalLCPStatus.empty() && p_Row->GetField(m_Template.m_ColumnExpirationPolicyStatus).GetValueStr() == BusinessLifeCyclePolicies::g_StatusApplied) //Selected
	{
		//ASSERT(p_Row->IsMoreLoaded() || !m_LastGlobalLCPStatus.empty()); // Load more status is not necessary already set at this point, so just cross fingers...
		if (p_Row->HasField(m_Template.m_ColumnRenewedDateTime))
		{
			if (p_Row->GetField(m_Template.m_ColumnRenewedDateTime).GetIcon() == GridBackendUtil::ICON_ERROR) // Is Error
			{
				p_Row->AddField(p_Row->GetField(m_Template.m_ColumnRenewedDateTime), m_Template.m_ColumnExpireOnDateTime->GetID());
			}
			else
			{
				auto expireDateTime = p_Row->GetField(m_Template.m_ColumnRenewedDateTime).GetValueTimeDate();
				expireDateTime.Adjust(0, 0, m_PolicyDays, 0, 0, 0);
				p_Row->AddField(expireDateTime, m_Template.m_ColumnExpireOnDateTime);
			}
		}
		else
		{
			p_Row->RemoveField(m_Template.m_ColumnExpireOnDateTime);
		}
	}
	else if (m_LastGlobalLCPStatus == BusinessLifeCyclePolicies::g_StatusPolicyError)
	{
		if (m_LastLCPError)
			p_Row->AddField(m_LastLCPError, m_Template.m_ColumnExpireOnDateTime, GridBackendUtil::ICON_ERROR);
		else
			p_Row->AddField(g_PolicyErrorStatus, m_Template.m_ColumnExpireOnDateTime);
	}
	else if (p_Row->GetField(m_Template.m_ColumnExpirationPolicyStatus).GetValueStr() != g_PolicyHasChangedStatus)
	{
		ASSERT(m_LastGlobalLCPStatus == BusinessLifeCyclePolicies::g_StatusNoPolicyAvailable || m_LastGlobalLCPStatus == BusinessLifeCyclePolicies::g_StatusPolicyIsNone || m_LastGlobalLCPStatus.empty() && p_Row->GetField(m_Template.m_ColumnExpirationPolicyStatus).GetValueStr() != BusinessLifeCyclePolicies::g_StatusApplied);
		p_Row->AddField(g_GroupNeverExpire, m_Template.m_ColumnExpireOnDateTime);
	}
}

void GridGroups::updateGroupLCPStatus(GridBackendRow* p_Row, const BusinessGroup& p_BG)
{
	ASSERT(isOffice365Group(p_Row));

	if (p_BG.GetLCPStatusError())
	{
		p_Row->AddField(p_BG.GetLCPStatusError()->GetFullErrorMessage(), m_Template.m_ColumnExpirationPolicyStatus, GridBackendUtil::ICON_ERROR);
	}
	else if (p_BG.GetLCPStatus())
	{
		p_Row->AddField(p_BG.GetLCPStatus().get(), m_Template.m_ColumnExpirationPolicyStatus);
	}
	else if (p_BG.GetCombinedGroupType() && p_BG.GetCombinedGroupType().get() == BusinessGroup::g_Office365Group)
	{
		if (m_LastGlobalLCPStatus == BusinessLifeCyclePolicies::g_StatusNoPolicyAvailable)
			p_Row->AddField(BusinessLifeCyclePolicies::g_StatusNoPolicyAvailable, m_Template.m_ColumnExpirationPolicyStatus);
		else
			p_Row->RemoveField(m_Template.m_ColumnExpirationPolicyStatus);
	}
	else
	{
		p_Row->RemoveField(m_Template.m_ColumnExpirationPolicyStatus);
	}
}

void GridGroups::LoadMoreImpl(const std::vector<GridBackendRow*>& p_Rows)
{
    ASSERT(IsFrameConnected());

	bool proceed = true;

	std::vector<GridBackendRow*> modifiedRows;
	for (auto row : p_Rows)
	{
		// FIXME: Use GridModifications?
		if (row->IsModified())
		{
			// If this assert fails, row status is not correctly synced to GridModifications
			ASSERT(([this, row]()
			{
				row_pk_t rowKey;
				GetRowPK(row, rowKey);
				auto mod = GetModifications().GetRowFieldModification(rowKey);
				return nullptr != mod && mod->GetState() == Modification::State::AppliedLocally;
			}()));

			modifiedRows.push_back(row);			
		}
	}

	if (!modifiedRows.empty())
	{
		// FIXME: Modify this to propose partial update when this feature is available.
		YCodeJockMessageBox confirmation(
			this,
			DlgMessageBox::eIcon_Question,
			YtriaTranslate::Do(GridGroups_LoadMoreImpl_1, _YLOC("Changes pending")).c_str(),
			YtriaTranslate::Do(GridGroups_LoadMoreImpl_2, _YLOC("The selected rows contain unsaved changes that will be lost.\r\nDo you want to undo them and continue?")).c_str(),
			L"",
			{ { IDOK, YtriaTranslate::Do(GridGroups_LoadMoreImpl_3, _YLOC("Undo and continue")).c_str() },/*{ IDABORT, _TLOC("Refresh only") },*/{ IDCANCEL, YtriaTranslate::Do(GridGroups_LoadMoreImpl_4, _YLOC("Cancel")).c_str() } });

		switch (confirmation.DoModal())
		{
		case IDOK:
			proceed = true;
			break;
		case IDCANCEL:
			proceed = false;
			break;
		case IDABORT:
			ASSERT(false);
			break;
		}

		if (proceed)
		{
			auto& mods = GetModifications();
			row_pk_t pk;
			bool update = false;
			for (auto row : modifiedRows)
			{
				GetRowPK(row, pk);
				if (mods.Revert(pk, true))
					update = true;
				else
					ASSERT(false);
			}

			if (update)
				UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		}
	}

	if (proceed)
	{
		auto updaterGroupsGen = UpdatedObjectsGenerator<BusinessGroup>();
		if (nullptr != m_Frame)
		{
			updaterGroupsGen.SetObjects(GetLoadMoreRequestInfo(p_Rows));
            
            CommandInfo info;
            info.Data() = updaterGroupsGen;
            info.SetFrame(m_Frame);
			info.SetRBACPrivilege(m_Frame->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateMoreInfo, info, { this, g_ActionNameSelectedGroupLoadMore, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
	}
}

bool GridGroups::isOffice365Group(const GridBackendRow* p_Row) const
{
	return nullptr != p_Row && !p_Row->IsGroupRow() && p_Row->GetField(m_Template.m_ColumnCombinedGroupType).GetValueStr() == BusinessGroup::g_Office365Group;
}

bool GridGroups::isDynamicMembershipGroup(const GridBackendRow* p_Row) const
{
	return nullptr != p_Row && !p_Row->IsGroupRow() && p_Row->GetField(m_Template.m_ColumnDynamicMembership).GetValueBool();
}

bool GridGroups::isOwnersColumn(const GridBackendColumn* p_Col) const
{
	return nullptr != p_Col && (m_Template.m_ColumnOwnerUserIDs == p_Col || m_Template.m_ColumnOwnerUserNames == p_Col);
}

bool GridGroups::isSettingsColumn(const GridBackendColumn* p_Col) const
{
	return nullptr != p_Col && (m_Template.m_ColumnSettingAllowToAddGuests == p_Col || m_Template.m_ColumnGroupUnifiedSettingActivated == p_Col);
}

bool GridGroups::isMembersCountColumn(const GridBackendColumn* p_Col) const
{
	return nullptr != p_Col && m_Template.m_ColumnMembersCount == p_Col;
}

bool GridGroups::isMembershipRuleColumn(const GridBackendColumn* p_Col) const
{
	return nullptr != p_Col && (m_Template.m_ColumnMembershipRuleProcessingState == p_Col || m_Template.m_ColumnMembershipRule == p_Col);
}

bool GridGroups::isYammer(const GridBackendRow* p_Row) const
{
	return nullptr != p_Row && p_Row->GetField(m_Template.m_ColumnIsYammer).GetIcon() != NO_ICON;
}

vector<BusinessGroup> GridGroups::GetLoadMoreRequestInfo(const std::vector<GridBackendRow *>& rowsWithMoreLoaded, YtriaTaskData p_TaskData)
{
    throw std::logic_error("The method or operation is not implemented.");
}

vector<BusinessGroup> GridGroups::GetLoadMoreRequestInfo(const std::vector<GridBackendRow *>& p_Rows)
{
    vector<BusinessGroup> groups;
    for (auto row : p_Rows)
    {
		if (IsRowForLoadMore(row, nullptr))
		{
			BusinessGroup businessGroup;
			m_Template.GetBusinessGroup(*this, row, businessGroup);

			// avoid useless queries (multivalue explosion side effect)
			if (std::none_of(groups.begin(), groups.end(), [id = businessGroup.GetID()](const BusinessGroup& bg){return id == bg.GetID(); }))
				groups.push_back(businessGroup);
		}
    }

    return groups;
}

bool GridGroups::CanRefreshOnPrem() const
{
	return hasNoTaskRunning()
		&& IsFrameConnected()
		&& GetSession()
		&& GetSession()->GetGraphCache().HasGroupsDeltaCapabilities()
		&& GetSession()->GetGraphCache().GetUseOnPrem()
		&& !GetSession()->GetForceDisableLoadOnPrem()
		&& !GetBackendRows().empty()
		&& !(GetBackendRows().size() == 1 && GetBackendRows()[0]->IsError())
		&& (!m_Frame->GetOptions() || m_Frame->GetOptions()->m_CustomFilter.IsEmpty());
}

row_pk_t GridGroups::UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody)
{
	ASSERT(IsGridModificationsEnabled());
	if (IsGridModificationsEnabled())
	{
		auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPk);
		ASSERT(nullptr != mod);
		if (nullptr != mod)
		{
			// Check Create call that generated this data: Need to request RBAC props?
			GroupDeserializer gd(GetSession());
			gd.Deserialize(json::value::parse(creationResultBody));
			auto bg = gd.GetData();
			row_pk_t newPk;
			m_Template.GetBusinessGroupPK(*this, bg, newPk, row_pk_t{});

			mod->SetNewPK(newPk);

			return newPk;
		}
	}

	return rowPk;
}

void GridGroups::GetSelectedGroupsForActionOnLCP(vector<BusinessGroup>& p_ListO365Groups, vector<row_pk_t>& p_RowPKs, BusinessObject::UPDATE_TYPE p_UpdateType)
{
	const auto isRowValidForAction = [p_UpdateType, this](GridBackendRow* row)
	{
		if (!row->IsGroupRow()
			&& !row->IsCreated() && !row->IsDeleted() && !row->IsModified() // Warn and revert instead?
			&& !row->IsExplosionAdded()
			&& IsGroupAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_GROUP_EDIT))
		{
			if (BusinessObject::UPDATE_TYPE_LCPRENEW == p_UpdateType)
				return true;

			const PooledString value = row->GetField(m_Template.m_ColumnExpirationPolicyStatus).GetValueStr();

			if (isOffice365Group(row))
			{
				if (BusinessObject::UPDATE_TYPE_NONE == p_UpdateType)
					return value == g_PolicyHasChangedStatus;

				if (BusinessObject::UPDATE_TYPE_LCPADDGROUP == p_UpdateType)
					return row->IsMoreLoaded() && value != BusinessLifeCyclePolicies::g_StatusApplied && value != g_PolicyHasChangedStatus;

				if (BusinessObject::UPDATE_TYPE_LCPREMOVEGROUP == p_UpdateType)
					return row->IsMoreLoaded() && value != BusinessLifeCyclePolicies::g_StatusNotApplied && value != g_PolicyHasChangedStatus;

				ASSERT(false);
			}
		}

		return false;
	};

	for (auto row : GetSelectedRows())
	{
		if (isRowValidForAction(row))
		{
			BusinessGroup bg;
			m_Template.GetBusinessGroup(*this, row, bg);
			if (BusinessObject::UPDATE_TYPE_NONE != p_UpdateType)
				bg.SetUpdateStatus(p_UpdateType);
			p_ListO365Groups.push_back(bg);

			p_RowPKs.emplace_back();
			GetRowPK(row, p_RowPKs.back());
		}
	}
}

void GridGroups::ShowOnPremiseGroups(const vector<OnPremiseGroup>& p_Groups)
{
	GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS);
	GridUpdater updater(*this, options);

	bool firstOnPremLoad = false;
	if (!p_Groups.empty())
	{
		if (m_Template.m_OnPremCols.Create(*this))
		{
			if (nullptr == GetSecondaryStatusColumn())
				AddSecondaryColumnStatus(_T("On-Premises status"));

			firstOnPremLoad = true;
			ASSERT(nullptr != m_Template.m_ColumnDisplayName && nullptr != m_Template.m_OnPremCols.m_ColCommonDisplayName);
			if (nullptr != m_Template.m_ColumnDisplayName && nullptr != m_Template.m_OnPremCols.m_ColCommonDisplayName)
			{
				auto cat1 = m_Template.m_ColumnDisplayName->GetTopCategory();
				auto cat2 = m_Template.m_OnPremCols.m_ColCommonDisplayName->GetTopCategory();
				ASSERT(nullptr != cat1 && nullptr != cat2);
				if (nullptr != cat1 && nullptr != cat2)
					GetBackend().SwapCategories(cat2, cat1); // Order here is very important, opposite doesn't work ...

				m_Template.m_ColumnMetaType = m_Template.m_OnPremCols.m_ColCommonMetaType;
			}
		}
	}

	bool isHybridEnvironment = false;

	std::set<PooledString> newObjectSIDs;
	for (const auto& group : p_Groups)
	{
		auto row = m_Template.UpdateRow(*this, group, m_O365RowPk, m_OnPremRowPk, newObjectSIDs);
		isHybridEnvironment = isHybridEnvironment || nullptr != row && (m_Template.IsOnPremMetatype(*row) || m_Template.IsHybridMetatype(*row));
	}

	std::set<PooledString> removedOnPremObjSIDs;
	for (const auto& kv : m_OnPremRowPk)
	{
		const wstring& objectSID = kv.first;
		if (newObjectSIDs.find(objectSID) == newObjectSIDs.end())
			removedOnPremObjSIDs.insert(objectSID);
	}

	for (const auto& removedId : removedOnPremObjSIDs)
	{
		auto ittO365 = m_O365RowPk.find(removedId);
		if (ittO365 != m_O365RowPk.end())
		{
			// Removing onPrem group that was hybrid
			GridBackendRow* toRemove = GetRowIndex().GetExistingRow(ittO365->second);
			ASSERT(nullptr != toRemove);
			if (nullptr != toRemove)
			{
				m_Template.SetO365(*this, *toRemove);
				m_Template.m_OnPremCols.SetBlankFields(*toRemove);
				m_OnPremRowPk.erase(removedId);
			}
		}
		else
		{
			// Removing onPrem group that was only onPrem
			auto ittOnPrem = m_OnPremRowPk.find(removedId);
			ASSERT(ittOnPrem != m_OnPremRowPk.end()); // Removing onPrem group that was not onPrem - should not happen
			if (ittOnPrem != m_OnPremRowPk.end())
			{
				GridBackendRow* toRemove = GetRowIndex().GetExistingRow(ittOnPrem->second);
				ASSERT(nullptr != toRemove);
				if (nullptr != toRemove)
				{
					RemoveRow(toRemove, false);
					m_OnPremRowPk.erase(removedId);
				}
			}
		}
	}

	ASSERT(isHybridEnvironment == !p_Groups.empty()); // If we have at least one OnPrem group, we are hybrid, aren't we?
	if (isHybridEnvironment)
	{
		if (!m_OnPremModifications)
			m_OnPremModifications = make_unique<GridFieldModificationsOnPrem>(*this);

		if (firstOnPremLoad)
		{
			// Show meta type column
			SetColumnsVisible(GetColumnsByPresets({ g_ColumnsPresetOnPremDefault }), true, false);

			// Fill new cells in o365 rows
			for (auto r : GetBackendRows())
			{
				if (!r->HasField(GetSecondaryStatusColumn()))
				{
					r->AddField(PooledString::g_EmptyString, GetSecondaryStatusColumn()).RemoveModified();
					m_Template.UpdateCommonInfo(r);
					m_Template.SetO365Metatype(*r);
				}
			}
		}
	}
}

void GridGroups::SetOnPremiseGroupsUpdateResult(const vector<O365UpdateOperation>& p_UpdateResults)
{
	if (!p_UpdateResults.empty())
	{
		// On O365 side, it's made by the refresh triggered after apply.
		ModuleUtil::ClearErrorRowsStatus(*this);

		const uint32_t options = GridUpdaterOptions::UPDATE_GRID
			//| GridUpdaterOptions::FULLPURGE
			| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			//| GridUpdaterOptions::REFRESH_PROCESS_DATA
			| GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG
			//| GridUpdaterOptions::PROCESS_LOADMORE
			;

		GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateResults, m_OnPremModifications.get());
	}
}

void GridGroups::OnGroupLoadMore()
{
	if (IsFrameConnected())
	{
		std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_GROUP_LOADMORE, [this](GridBackendRow* p_Row)
			{
				return IsRowForLoadMore(p_Row, nullptr);
			});

		if (!selectedRows.empty() && showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT))
			LoadMoreImpl(selectedRows);
	}
}

void GridGroups::OnUpdateGroupLoadMore(CCmdUI* pCmdUI)
{
	const auto condition = [this](GridBackendRow* p_Row)
	{
		return IsRowForLoadMore(p_Row, nullptr);
	};

	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_GROUP_LOADMORE, condition));
    setTextFromProfUISCommand(*pCmdUI);
}

void GridGroups::OnShowRecycleBin()
{
	if (IsFrameConnected())
	{
		if (openModuleConfirmation())
		{
			FrameGroups* pFrame = dynamic_cast<FrameGroups*>(GetParent());
			ASSERT(nullptr != pFrame);
			if (nullptr != pFrame)
				pFrame->ShowRecycleBin();
		}
		else
		{
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"), _YR("Y2449")).c_str());
		}
	}
}

void GridGroups::OnUpdateShowRecycleBin(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroups::OnCreateTeam()
{
	// Do we want to show the group edit dialog?

	TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool /*p_HasRowsToReExplode*/)
	{
		std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_GROUP_EDIT, [this](GridBackendRow* p_Row)
		{
			return isOffice365Group(p_Row) && !isTeam(p_Row) && !isYammer(p_Row) && !IsTemporaryCreatedObjectID(p_Row->GetField(m_ColId).GetValueStr());
		});

		BusinessGroup tempGroup;
		tempGroup.SetEditHasTeam(true);

		GridUpdater gridUpdater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID));
		for (auto row : selectedRows)
			m_Template.SetRowIsTeam(tempGroup, row, m_Template.m_ColumnIsTeam, GridTemplate::UpdateFieldOption::IS_MODIFICATION);
	})();
}

void GridGroups::OnUpdateCreateTeam(CCmdUI* pCmdUI)
{
	const auto condition = [this](GridBackendRow* p_Row)
	{
		return isOffice365Group(p_Row) && !isTeam(p_Row) && !isYammer(p_Row) && !IsTemporaryCreatedObjectID(p_Row->GetField(m_ColId).GetValueStr());
	};

	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_GROUP_EDIT, condition));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroups::OnBatchImport()
{
	if (!hasNoTaskRunning())
		return;

	static const wstring visibilityTip = _T("Accepted values include: 'Public', 'Private', 'HiddenMembership'. If no value is provided, 'Public' will be applied.");

	BatchImporter::MappingProperties mappingProperties
	{
		{ _YTEXT(O365_GROUP_GROUPTYPE)		, { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_GROUPTYPE))	, true	, {} }},
		{ _YTEXT(O365_GROUP_DISPLAYNAME)	, { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_DISPLAYNAME))	, true	, {} }},
		{ _YTEXT(O365_GROUP_MAILNICKNAME)	, { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_MAILNICKNAME))	, true	, {} }},
		//{ _YTEXT(O365_GROUP_MAIL)			, { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_MAIL))			, false	, {} }},
		{ _YTEXT(O365_GROUP_DESCRIPTION)	, { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_DESCRIPTION))	, false	, {} }},
		{ _YTEXT(O365_GROUP_VISIBILITY)		, { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_VISIBILITY))	, true	, { visibilityTip, true } }},
#if 0 // Finish this when available in create dialog
		// Team Guest Settings //
		_T("Allow Create Update Channels"),
		_T("Allow Delete Channels"),

		// Team Messaging Settings //
		_T("Allow User Edit Messages"),
		_T("Allow User Delete Messages"),
		_T("Allow Owner Delete Messages"),
		_T("Allow Team Mentions"),
		_T("Allow Channel Mentions"),

		// Team Fun Settings //
		_T("Allow Giphy"),
		_T("Giphy Content Rating"),
		_T("Allow Stickers and Memes"),
		_T("Allow Custom Memes"),
#endif
	};

	vector<BusinessGroup> newGroups;
	BusinessGroup templateGroup;

	auto& connectedUser = GetGraphCache().GetCachedConnectedUser();
	ASSERT(connectedUser);
	if (connectedUser)
		templateGroup.AddOwner(BusinessUser(*connectedUser));

	auto org = GetGraphCache().GetRBACOrganization();
	ASSERT(org);
	if (org)
		templateGroup.SetMailDomain(PooledString(org->GetInitialDomain()));

	templateGroup.SetFlags(BusinessObject::CREATED);
	templateGroup.SetVisibility(PooledString(_YTEXT("Public")));

	INT_PTR invalidDomainAction = 0;
	INT_PTR missingMandatoryAction = 0;
	auto callback = [this, &newGroups, &templateGroup, &mappingProperties, &invalidDomainAction, &missingMandatoryAction](const std::map<wstring, wstring>& p_RowValues)
	{
		wstring error;

		bool missingMandatoryValue = false;
		auto action = missingMandatoryAction;

		// Hack: some mandatory values are only mandatory for Office 365 groups but not Security Groups
		wstring groupType;
		auto it = p_RowValues.find(_YTEXT(O365_GROUP_GROUPTYPE));
		ASSERT(p_RowValues.end() != it);
		if (p_RowValues.end() != it)
			groupType = it->second;

		// If we get to handle Distribution Lists, modify below lambda accordingly
		ASSERT(groupType == BusinessGroup::g_Office365Group || groupType == BusinessGroup::g_SecurityGroup);

		auto isMandatory = [&groupType](BatchImporter::MappingProperties::value_type mapProp)
		{
			return std::get<1>(mapProp.second)
				&& mapProp.first != _YTEXT(O365_GROUP_GROUPTYPE)
				&& (groupType == BusinessGroup::g_Office365Group // All mandatory props apply to o365 groups
					|| mapProp.first != _YTEXT(O365_GROUP_MAILNICKNAME) && mapProp.first != _YTEXT(O365_GROUP_VISIBILITY) // those mandatory props don't apply to security groups - they are hidden in config dialog (see BatchImporter.cpp)
					);
		};

		for (const auto& prop : mappingProperties)
		{
			if (isMandatory(prop))
			{
				if (p_RowValues.end() == p_RowValues.find(prop.first))
				{
					missingMandatoryValue = true;
					if (0 == action)
					{
						YCodeJockMessageBox dlg(this
							, DlgMessageBox::eIcon_Question
							, _YFORMAT(L"'%s' is missing whereas it's mandatory for %s creation.", std::get<0>(prop.second).c_str(), groupType.c_str())
							, _T("Would you like to skip this group or cancel the whole import?")
							, _YTEXT("")
							, { { IDABORT, _T("Skip") }, { IDCANCEL, _T("Cancel") } });
						dlg.SetVerificationText(_T("Do this for all groups."));
						action = dlg.DoModal();
						if (dlg.IsVerificiationChecked())
							missingMandatoryAction = action;
					}

					break;
				}
			}
		}

		if (missingMandatoryValue)
		{
			if (IDCANCEL == action)
				return wstring(_T("Canceled by user."));

			return Str::g_EmptyString; // Continue
			//return wstring(_T("Missing mandatory value.")); // Stop
		}

		newGroups.emplace_back(templateGroup);
		auto& newGroup = newGroups.back();
		newGroup.SetID(GetNextTemporaryCreatedObjectID());

		for (const auto& rowValue : p_RowValues)
		{
			// Specific strings
			if (rowValue.first == _YTEXT(O365_GROUP_VISIBILITY))
			{
				// Should it be an error?
				if (MFCUtil::StringMatchW(rowValue.second ,_YTEXT("Public"))
				 || MFCUtil::StringMatchW(rowValue.second, _YTEXT("Private"))
				 || MFCUtil::StringMatchW(rowValue.second, _YTEXT("HiddenMembership")))
					newGroup.SetVisibility(PooledString(rowValue.second));
			}
			else if (rowValue.first == _YTEXT(O365_GROUP_GROUPTYPE))
			{
				ASSERT(rowValue.second == groupType);
				newGroup.SetCombinedGroupType(PooledString(rowValue.second));
			}
			else if (rowValue.first == _YTEXT(O365_GROUP_MAILNICKNAME))
			{
				newGroup.SetMailWithoutDomain(PooledString(rowValue.second)); // Also set nickname
			}
			// Regular strings
			else
			{
				// FIXME: Should we do it with ifs instead of generic impl?
				if (mappingProperties.end() != mappingProperties.find(rowValue.first))
				{
					rttr::type classType = rttr::type::get<BusinessGroup>();
					rttr::property prop = classType.get_property(Str::convertToASCII(rowValue.first));

					ASSERT(prop.get_type() == rttr::type::get<boost::YOpt<PooledString>>());
					if (prop.get_type() == rttr::type::get<boost::YOpt<PooledString>>())
					{
						bool success = prop.set_value(newGroup, boost::YOpt<PooledString>(rowValue.second));
						ASSERT(success);
					}
				}
			}
		}

		if (newGroup.GetCombinedGroupType() != boost::YOpt<PooledString>(BusinessGroup::g_Office365Group))
		{
			// Alert user?
			
			newGroup.SetMailNickname(boost::none);
			newGroup.SetMail(boost::none);

			// FIXME: We should allow it for security group (in edit dialog too), just Private or Public
			newGroup.SetVisibility(boost::none);
		}

		return error;
	};

	if (BatchImporter::DoCreate(BatchImporter::Group, mappingProperties, callback, this))
	{
		ASSERT(!newGroups.empty());
		AddCreatedBusinessObjects(newGroups, false);
	}
}

void GridGroups::OnUpdateBatchImport(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && HasCreate() && IsAuthorizedByRoleDelegation(GetPrivilegeCreate()));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroups::OnChangePreFilter()
{
	if (IsFrameConnected())
	{
		ASSERT(m_Frame->GetOptions());
		DlgGroupsModuleOptions dlgOptions(m_Frame, GetSession(), m_Frame->GetOptions() ? *m_Frame->GetOptions() : ModuleOptions());
		if (dlgOptions.DoModal() == IDOK)
		{
			CommandInfo info;
			info.SetFrame(m_Frame);
			info.SetOrigin(GetOrigin());
			info.SetRBACPrivilege(m_Frame->GetModuleCriteria().m_Privilege);
			info.Data2() = dlgOptions.GetOptions();

			// FIXME: Automation action name !!!
			Command command(m_Frame->GetLicenseContext(), m_Frame->GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::ListWithPrefilter, info, { this, g_ActionNameRefreshAll, GetAutomationActionRecording(), nullptr });
			CommandDispatcher::GetInstance().Execute(command);
		}
	}
}

void GridGroups::OnUpdateChangePreFilter(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && m_Frame->GetOptions() && !m_Frame->GetOptions()->m_CustomFilter.IsEmpty());
	setTextFromProfUISCommand(*pCmdUI);
}

BusinessGroup GridGroups::getBusinessObject(GridBackendRow* row) const
{
	BusinessGroup bg;
	m_Template.GetBusinessGroup(*this, row, bg);
	if (IsTemporaryCreatedObjectID(bg.GetID()) || row->IsCreated())
	{
		// This assert fails after a Group creation fails. Remove it eventually.
		ASSERT(IsTemporaryCreatedObjectID(bg.GetID()) && row->IsCreated());
		bg.SetFlags(bg.GetFlags() | BusinessObject::CREATED);
	}
	if (row->IsMoreLoaded())
	{
		// FIXME: New modular cache
		bg.SetFlags(bg.GetFlags() | BusinessObject::MORE_LOADED);
	}
	return bg;
}

OnPremiseGroup GridGroups::getOnPremiseGroup(GridBackendRow* row) const
{
	OnPremiseGroup group;
	auto res = m_Template.GetOnPremiseGroup(*this, row, group);
	ASSERT(res);

	//if (IsTemporaryCreatedObjectID(bu.GetID()) || row->IsCreated())
	//{
	//	// This assert fails after a User creation fails. Remove it eventually.
	//	// ASSERT(IsTemporaryCreatedObjectID(bu.GetID()) && row->IsCreated());
	//	bu.SetFlags(bu.GetFlags() | BusinessObject::CREATED);
	//}

	//if (row->IsMoreLoaded())
	//{
	//	// FIXME: New modular cache.
	//	bu.SetFlags(bu.GetFlags() | BusinessObject::MORE_LOADED);
	//}

	return group;
}

void GridGroups::getFieldsThatHaveErrors(GridBackendRow* p_Row, std::set<wstring>& p_ListFieldErrors) const
{
	m_Template.GetListOfFieldErrors(*this, p_Row, p_ListFieldErrors);
}

BOOL GridGroups::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return GridGroupsBaseClass::PreTranslateMessage(pMsg);
}

INT_PTR GridGroups::showDialog(vector<BusinessGroup>& p_Objects, const std::set<wstring>& p_ListFieldErrors, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
{
	if (DlgFormsHTML::Action::CREATE == p_Action)
	{
		ASSERT(1 == p_Objects.size());

		// We can't put this assert because of no operator== but the lone object here should be a default constructed one.
		// ASSERT(p_Objects.back() == BusinessGroup());

		auto& clearUnusedValues = [](BusinessGroup& p_Group)
		{
			p_Group.SetIsAllowExternalSenders(boost::none);
			p_Group.SetIsAutoSubscribeNewMembers(boost::none);
			p_Group.SetClassification(boost::none);
			p_Group.SetCreatedDateTime(boost::YOpt<YTimeDate>());
			p_Group.SetRenewedDateTime(boost::YOpt<YTimeDate>());
			p_Group.SetProxyAddresses(boost::none);
			//p_Group.SetIsMailEnabled(boost::none); // Part of Combined Group Type
			p_Group.SetMailNickname(boost::none);
			p_Group.SetOnPremisesLastSyncDateTime(boost::YOpt<YTimeDate>());
			p_Group.SetOnPremisesSecurityIdentifier(boost::none);
			p_Group.SetOnPremisesProvisioningErrors(boost::none);
			p_Group.SetOnPremisesSyncEnabled(boost::none);
			//p_Group.SetIsSecurityEnabled(boost::none); // Part of Combined Group Type
			p_Group.SetUnseenCount(boost::none);
			p_Group.SetLCPStatus(boost::none);
			p_Group.SetLCPStatusError(boost::none);
			p_Group.SetUserCreatedOnBehalfOf(boost::none);
			p_Group.SetUserCreatedOnBehalfOfError(boost::none);
			p_Group.ClearOwners();
			p_Group.SetChildrenUsers({});
			p_Group.SetChildrenGroups({});
			p_Group.SetChildrenOrgContacts({});
			p_Group.SetHasTeamInfo(false);
			p_Group.SetTeam(boost::none);
			p_Group.SetTeamError(boost::none);
			p_Group.SetSettingAllowToAddGuests(boost::none);
			p_Group.SetGroupUnifiedGuestSettingTemplateActivated(boost::none);
			p_Group.SetGroupSettingsError(boost::none);
			p_Group.SetDrive(boost::none);
			p_Group.SetResourceBehaviorOptions({});
			p_Group.SetResourceProvisioningOptions({});
			p_Group.SetPreferredDataLocation(boost::none);
			p_Group.SetMembersCount(boost::none);
			p_Group.SetMembershipRuleProcessingState(boost::none);
			p_Group.SetMembershipRule(boost::none);
			p_Group.SetTheme(boost::none);
			p_Group.SetDeletedDateTime(boost::YOpt<YTimeDate>());
			p_Group.SetLCPId(PooledString());
			p_Group.SetDriveError(boost::none);
			p_Group.SetAssignedLicenses(vector<BusinessAssignedLicense>{});
			p_Group.SetHideFromAddressLists(boost::none);
			p_Group.SetHideFromOutlookClients(boost::none);
			p_Group.SetLicenseProcessingState(boost::none);
			p_Group.SetSecurityIdentifier(boost::none);

			return p_Group;
		};

		auto keepOnlyCommonValues = [](BusinessGroup& p_First, const BusinessGroup& p_Second)
		{
			if (p_First.GetDisplayName() != p_Second.GetDisplayName())
				p_First.SetDisplayName(boost::none);
			if (p_First.GetCombinedGroupType() != p_Second.GetCombinedGroupType())
				p_First.SetCombinedGroupType(boost::none);
			if (p_First.GetMail() != p_Second.GetMail())
				p_First.SetMail(boost::none);
			if (p_First.GetDescription() != p_Second.GetDescription())
				p_First.SetDescription(boost::none);
			if (p_First.GetVisibility() != p_Second.GetVisibility())
				p_First.SetVisibility(boost::none);
		};

		boost::YOpt<BusinessGroup> templateGroup;
		TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this, &templateGroup, clearUnusedValues, keepOnlyCommonValues](const bool /*p_HasRowsToReExplode*/)
			{
				for (auto& row : GetSelectedRows())
				{
					if (GridUtil::IsBusinessGroup(row))
					{
						auto group = getBusinessObject(row);
						if (!templateGroup)
						{
							templateGroup = clearUnusedValues(group);
						}
						else
						{
							keepOnlyCommonValues(*templateGroup, group);
						}
					}
				}
			})();

		if (templateGroup)
		{
			p_Objects.back() = *templateGroup;
			p_Action = DlgFormsHTML::Action::CREATE_PREFILLED;
		}

		// Set default owner
		ASSERT(p_Objects.back().GetOwnerUsers().empty()/* && p_Objects.back().GetOwnerGroups().empty()*/);
		auto& connectedUser = GetGraphCache().GetCachedConnectedUser();
		ASSERT(connectedUser);
		if (connectedUser)
			p_Objects.back().AddOwner(BusinessUser(*connectedUser));

		// Set default group type
		/*if (!p_Objects.back().GetCombinedGroupType()
			|| p_Objects.back().GetCombinedGroupType() != PooledString(BusinessGroup::g_Office365Group) && p_Objects.back().GetCombinedGroupType() != PooledString(BusinessGroup::g_SecurityGroup))
			p_Objects.back().SetCombinedGroupType(PooledString(BusinessGroup::g_Office365Group));*/

		// Set default visibility
		if (!p_Objects.back().GetVisibility())
			p_Objects.back().SetVisibility(PooledString(_YTEXT("Public")));

		// Set default domain.
		// WARNING
		// Graph API doesn't allow domain setting, as the o365 portal.
		// But unlike o365 portal, the domain used is not the organization's default: it's the organization's initial.
		auto org = GetGraphCache().GetRBACOrganization();
		ASSERT(org);
		if (org)
			p_Objects.back().SetMailDomain(PooledString(org->GetInitialDomain()));//p_Objects.back().SetMailDomain(PooledString(org->GetDefaultDomain()));
	}
	else if (DlgFormsHTML::Action::EDIT_CREATE == p_Action)
	{
		ASSERT([p_Objects](){
			for (auto& bg : p_Objects)
			{
				if (bg.GetID().IsEmpty() || !IsTemporaryCreatedObjectID(bg.GetID()))
					return false;
				if (0 == (bg.GetFlags() & BusinessObject::CREATED))
					return false;
			}
			return true;
		}());

		// Set default domain.
		// WARNING
		// Graph API doesn't allow domain setting, as the o365 portal.
		// But unlike o365 portal, the domain used is not the organization's default: it's the organization's initial.
		auto org = GetGraphCache().GetRBACOrganization();
		ASSERT(org);
		if (org)
		{
			for (auto& bg : p_Objects)
				bg.SetMailDomain(PooledString(org->GetInitialDomain()));//bg.SetMailDomain(PooledString(org->GetDefaultDomain()));
		}

		// For created groups, the email is not displayed in the grid (read-only on graph side)
		// But, we know it will be mailnickname@initialdomain
		// Correctly set it in created groups to be edited
		for (auto& bg : p_Objects)
			if (bg.GetMailNickname())
				bg.SetMailWithoutDomain(bg.GetMailNickname());
	}

	const auto ret = DlgBusinessGroupEditHTML(p_Objects, p_ListFieldErrors, GetGraphCache().GetRBACOrganization(), GetGraphCache(), p_Action, p_Parent).DoModal();

	if (IDOK == ret)
	{
		if (DlgFormsHTML::Action::CREATE == p_Action || DlgFormsHTML::Action::CREATE_PREFILLED == p_Action)
		{
			// Create a temporary but unique ID to please the grid PK
			ASSERT(1 == p_Objects.size());
			ASSERT(!IsTemporaryCreatedObjectID(p_Objects.back().GetID()));
			p_Objects.back().SetID(GetNextTemporaryCreatedObjectID());
			p_Objects.back().SetFlags(p_Objects.back().GetFlags() | BusinessObject::CREATED);

			// Even if a domain can be set in there, e-mail is readonly. So clear it to avoid having it in the grid.
			p_Objects.back().SetMail(boost::none);
		}
		else if (DlgFormsHTML::Action::EDIT_CREATE == p_Action)
		{
			ASSERT([&p_Objects]() { for (const auto& obj : p_Objects) if (!IsTemporaryCreatedObjectID(obj.GetID())) return false; return true; }());

			// Even if a domain can be set in there, e-mail is readonly. So clear it to avoid having it in the grid.
			for (auto& bg : p_Objects)
				bg.SetMail(boost::none);
		}
	}

	return ret;
}

void GridGroups::BuildView(const vector<BusinessGroup>& p_Groups, const vector<BusinessGroup>& p_LoadedMoreGroups, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_AllowFullPurge)
{
	// p_Groups must also contain the p_LoadedMoreGroups groups. (test GetCreatedDateTime() equality as it's a load more property)
	ASSERT(p_LoadedMoreGroups.empty()
			|| !p_Groups.empty() && p_Groups.end() != std::find_if(p_Groups.begin(), p_Groups.end(), [&p_LoadedMoreGroups](const BusinessGroup& bg) {return bg.GetID() == p_LoadedMoreGroups.front().GetID() && bg.GetCreatedDateTime() == p_LoadedMoreGroups.front().GetCreatedDateTime(); }));

	// Did we get an error during groups listing?
    if (p_Groups.size() == 1 && p_Groups[0].GetError() && p_Groups[0].GetID().IsEmpty())
    {
		HandlePostUpdateError(p_Groups[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_Groups[0].GetError()->GetFullErrorMessage());
    }
	else
	{
		boost::YOpt<GridUpdater::PostUpdateError> unprocessedPostUpdateError;
		{
			resetRBACHiddenCounter();

			const auto fullPurge = p_AllowFullPurge && p_Updates.empty();
			const auto willProceedLoadMore = !(p_Refresh && p_LoadedMoreGroups.empty());
			const uint32_t options	= (!willProceedLoadMore ? GridUpdaterOptions::UPDATE_GRID : 0)
									| (fullPurge ? GridUpdaterOptions::FULLPURGE : 0)
									| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
									| (!willProceedLoadMore ? GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS : 0)
									| GridUpdaterOptions::REFRESH_PROCESS_DATA
									| (!p_Updates.empty() ? GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG : 0)
									//| GridUpdaterOptions::PROCESS_LOADMORE
									;
			GridUpdater updater(*this, GridUpdaterOptions(options), p_Updates);
			ASSERT(!updater.GetOptions().IsPartialPurge());

			if (willProceedLoadMore)
			{
				unprocessedPostUpdateError.emplace();
				updater.SetPostUpdateErrorBackupTarget(&*unprocessedPostUpdateError);
			}

			GridIgnoreModification ignoramus(*this);

			const auto tenantAllowAddToGuest = getTenantAllowAddToGuests();

			if (fullPurge && p_Groups.empty())
				GetRowIndex().LoadRows(false);

			bool isHybridEnvironment = false;
			for (const auto& bg : p_Groups)
			{
				if (bg.HasFlag(BusinessObject::Flag::CANCELED))
				{
					// If object is canceled, just get the row from the index to prevent it from being removed on purge.
					row_pk_t BGK;
					m_Template.GetBusinessGroupPK(*this, bg, BGK, {});
					auto row = GetRowIndex().GetRow(BGK, false, updater.GetOptions().IsFullPurge());
					if (nullptr != row)
					{
						AddRoleDelegationFlag(row, bg);
						if (isOffice365Group(row))
							updateAddToGuests(row, tenantAllowAddToGuest);
					}
				}
				else
				{
					if (bg.HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
					{
						// We are removing the user because it was explicitly deleted by delta
						bool handled = false;

						row_pk_t existingPk;
						m_Template.GetBusinessGroupPK(*this, bg, existingPk, {});
						GridBackendRow* rowBeingRemoved = GetRowIndex().GetExistingRow(existingPk);
						if (rowBeingRemoved != nullptr)
						{
							wstring securityId = rowBeingRemoved->GetField(m_Template.m_ColumnOPSecurityID).GetValueStr();
							auto ittO365 = m_O365RowPk.find(securityId);
							auto ittOnPrem = m_OnPremRowPk.find(securityId);
							if (ittO365 != m_O365RowPk.end() && ittOnPrem != m_OnPremRowPk.end())
							{
								// The O365 part of an hybrid user is being removed
								GridBackendRow* o365Row = GetRowIndex().GetExistingRow(ittO365->second);
								ASSERT(nullptr != o365Row);
								if (nullptr != o365Row)
								{
									auto objSIDCol = m_Template.m_OnPremCols.m_ColObjectSid;
									row_pk_t newPk;
									newPk.emplace_back(o365Row->GetField(objSIDCol).GetValueStr(), m_Template.m_ColumnID->GetID());

									auto newRow = GetRowIndex().GetRow(newPk, true, false);
									ASSERT(nullptr != newRow);
									if (nullptr != newRow)
									{
										auto existingRow = GetRowIndex().GetExistingRow(newPk);
										m_OnPremRowPk[securityId] = newPk;

										m_Template.SetOnPrem(*this, *newRow);
										newRow->AddFields(m_Template.m_OnPremCols.GetFields(*o365Row));
										existingRow = GetRowIndex().GetExistingRow(newPk);
										RemoveRow(o365Row);
										existingRow = GetRowIndex().GetExistingRow(newPk);

										handled = true;
									}
								}
							}

							if (ittO365 != m_O365RowPk.end())
								m_O365RowPk.erase(ittO365);
						}

						if (!handled && nullptr != rowBeingRemoved)
							m_Template.RemoveRow(*this, bg);
					}
					else
					{
						wstring objectSID = bg.GetOnPremisesSecurityIdentifier() ? *bg.GetOnPremisesSecurityIdentifier() : PooledString();
						if (!objectSID.empty())
						{
							vector<GridBackendField> pk;
							m_Template.GetBusinessGroupPK(*this, bg, pk, {});
							m_O365RowPk[objectSID] = pk;
						}

						auto newRow = m_Template.AddRow(*this, bg, vector<GridBackendField>(), updater, false, nullptr, {}, false);
						ASSERT(nullptr != newRow);
						if (nullptr != newRow)
						{
							auto ittOnPremRow = m_OnPremRowPk.find(objectSID);
							if (ittOnPremRow != m_OnPremRowPk.end())
							{
								auto onPremRow = GetRowIndex().GetExistingRow(ittOnPremRow->second);
								ASSERT(nullptr != onPremRow);
								if (nullptr != onPremRow && onPremRow != newRow)
								{
									// New o365 group, already has on-prem row, so hybrid group

									// Copy on-prem part from on-prem row
									newRow->AddFields(m_Template.m_OnPremCols.GetFields(*onPremRow));

									m_Template.SetHybrid(*this, *newRow);

									RemoveRow(onPremRow);

									row_pk_t newPk;
									GetRowPK(newRow, newPk);
									m_OnPremRowPk[objectSID] = newPk;
								}
							}
							else
							{
								// New o365 group, but not on-prem
							}

							updater.GetOptions().AddRowWithRefreshedValues(newRow);
							if (isOffice365Group(newRow))
							{
								if (m_LastGlobalLCPStatus.empty())
								{
									newRow->RemoveField(m_Template.m_ColumnExpirationPolicyStatus);
									newRow->RemoveField(m_Template.m_ColumnExpireOnDateTime);
								}
								else
								{
									if (m_LastLCPError)
										newRow->AddField(m_LastLCPError, m_Template.m_ColumnExpirationPolicyStatus, GridBackendUtil::ICON_ERROR);
									else
										newRow->AddField(m_LastGlobalLCPStatus, m_Template.m_ColumnExpirationPolicyStatus);
									updateExpirationDate(newRow);
								}

								updateAddToGuests(newRow, tenantAllowAddToGuest);
							}

							AddRoleDelegationFlag(newRow, bg);
						}
						isHybridEnvironment = isHybridEnvironment || (nullptr != newRow && m_Template.IsOnPremMetatype(*newRow) || m_Template.IsHybridMetatype(*newRow));
					}					
				}
			}
			if (isHybridEnvironment)
			{
				// Show meta type column
				SetColumnVisible(m_Template.m_ColumnMetaType, true, false);
			}

			updateRBACStatusBarText();
		}


		if (!p_LoadedMoreGroups.empty())
			updateGroupsLoadMore(p_LoadedMoreGroups, p_Refresh, !p_Updates.empty(), unprocessedPostUpdateError);
	}
}

void GridGroups::updateAddToGuests(GridBackendRow* p_Row, const boost::YOpt<bool>& p_TenantAddToGuest)
{
	ASSERT(isOffice365Group(p_Row));
	ASSERT(nullptr != p_Row && nullptr != m_ColumnComputedAllowToAddGuests);
	if (nullptr != p_Row && nullptr != m_ColumnComputedAllowToAddGuests)
	{
		if (p_TenantAddToGuest && !*p_TenantAddToGuest)
		{
			// If blocked (false) by tenant => false
			p_Row->AddFieldForCheckBox(false, m_ColumnComputedAllowToAddGuests);
		}
		else
		{
			if (p_Row->HasField(m_Template.m_ColumnSettingAllowToAddGuests))
			{
				// If not blocked (false) by tenant => group value wins
				p_Row->AddFieldForCheckBox(p_Row->GetField(m_Template.m_ColumnSettingAllowToAddGuests).GetValueBool(), m_ColumnComputedAllowToAddGuests);
			}
			else
			{
				if (p_Row->IsMoreLoaded())
				{
					// If no value defined => true
					p_Row->AddFieldForCheckBox(true, m_ColumnComputedAllowToAddGuests);
				}
				else
				{
					// Still waiting for load more
					p_Row->RemoveField(m_ColumnComputedAllowToAddGuests);
				}
			}
		}
	}
}

const boost::YOpt<bool> GridGroups::getTenantAllowAddToGuests() const
{
	boost::YOpt<bool> tenantAllowAddToGuest;

	auto groupSettings = GetTenantGroupSettings();
	if (groupSettings)
	{
		auto it = std::find_if(groupSettings->begin(), groupSettings->end(), [](const auto& p_Setting)
		{
			return p_Setting.GetDisplayName() && *p_Setting.GetDisplayName() == _YTEXT("Group.Unified");
		});

		if (it != groupSettings->end())
		{
			const auto& values = it->GetValues();
			auto it = std::find_if(values.begin(), values.end(), [](const auto& p_Value)
			{
				return p_Value.Name && *p_Value.Name == _YTEXT("AllowToAddGuests");
			});

			if (values.end() != it)
			{
				if (it->Value && *it->Value == _YTEXT("true"))
				{
					tenantAllowAddToGuest = true;
				}
				else if (it->Value && *it->Value == _YTEXT("false"))
				{
					tenantAllowAddToGuest = false;
				}
			}
		}
	}

	return tenantAllowAddToGuest;
}

void GridGroups::UpdateGroupLCPStatus(const vector<BusinessGroup>& p_Groups)
{
	GridIgnoreModification ignoramus(*this);
	vector<GridBackendRow*> rows;
	GetAllNonGroupRows(rows);
	for (GridBackendRow* row : rows)
	{
		PooledString id = row->GetField(m_Template.m_ColumnID).GetValueStr();
		if (isOffice365Group(row))
		{
			for (const auto& bg : p_Groups)
			{
				if (bg.GetID() == id)
				{
					updateGroupLCPStatus(row, bg);
					updateExpirationDate(row);
					break;
				}
			}
		}
	}

	UpdateMegaSharkOptimized(vector<GridBackendColumn*>{ m_Template.m_ColumnExpirationPolicyStatus });
}

namespace
{
	wstring GetLCPStatus(const boost::YOpt<BusinessLifeCyclePolicies>& p_LCP)
	{
		if (p_LCP)
		{
			if (p_LCP->GetError())
			{
				return BusinessLifeCyclePolicies::g_StatusPolicyError;
			}
			else if (p_LCP->GetManagedGroupTypes())
			{
				if (p_LCP->GetManagedGroupTypes().get() == _YTEXT("None"))
				{
					return BusinessLifeCyclePolicies::g_StatusPolicyIsNone;
				}
				else if (p_LCP->GetManagedGroupTypes().get() == _YTEXT("All"))
				{
					return BusinessLifeCyclePolicies::g_StatusApplied;
				}
				else if (p_LCP->GetManagedGroupTypes().get() == _YTEXT("Selected"))
				{
					return _YTEXT("");
				}
				else
				{
					ASSERT(false);
				}
			}
		}

		return BusinessLifeCyclePolicies::g_StatusNoPolicyAvailable;
	}
}

bool GridGroups::SetLCP(const BusinessLifeCyclePolicies& p_LCP, bool p_RefreshGrid)
{
	bool update = false;

	const auto newStatus = GetLCPStatus(p_LCP);
	const auto newDays = p_LCP.GetNumberOfDays() ? *p_LCP.GetNumberOfDays() : 0;
	const bool statusChanged = m_LastGlobalLCPStatus != newStatus;
	const bool daysChanged = m_PolicyDays != newDays;
	m_LastGlobalLCPStatus = newStatus;
	m_PolicyDays = newDays;

	if (BusinessLifeCyclePolicies::g_StatusPolicyError == m_LastGlobalLCPStatus)
	{
		if (p_LCP.GetError())
			m_LastLCPError = p_LCP.GetError()->GetFullErrorMessage();
	}
	else
	{
		m_LastLCPError.reset();
	}

	int askForReloadCounter = 0;

	if (statusChanged || daysChanged)
	{
		if (m_LastGlobalLCPStatus.empty()) // "Selected" policy
		{
			m_Template.m_ColumnExpirationPolicyStatus->AddPresetFlags(g_ColumnsPresetMore);
			m_Template.m_ColumnExpireOnDateTime->AddPresetFlags(g_ColumnsPresetMore);
			setUpLoadMoreColumn(m_Template.m_ColumnExpirationPolicyStatus);
			setUpLoadMoreColumn(m_Template.m_ColumnExpireOnDateTime);

			GridIgnoreModification ignoramus(*this);
			vector<GridBackendRow*> rows;
			GetBackend().GetRows(GridBackendUtil::SELECTION_ALLNONGROUPNOTEXPLOSIONADDED, rows);
			ScopedImplodeRows scopedImplodeRows(*this, rows, p_RefreshGrid);
			for (auto row : rows)
			{
				if (isOffice365Group(row))
				{
					if (row->IsMoreLoaded())
					{
						if (statusChanged)
						{
							row->AddField(g_PolicyHasChangedStatus, m_Template.m_ColumnExpirationPolicyStatus, m_LoadMoreConfig.m_Icon);
							row->AddField(g_PolicyHasChangedStatus, m_Template.m_ColumnExpireOnDateTime, m_LoadMoreConfig.m_Icon);
							++askForReloadCounter;
						}

						if (statusChanged || daysChanged)
							updateExpirationDate(row);
					}
					else
					{
						row->RemoveField(m_Template.m_ColumnExpirationPolicyStatus);
						row->RemoveField(m_Template.m_ColumnExpireOnDateTime);
					}
					update = true;
				}
			}

			if (scopedImplodeRows.HasExplosionsToRestore())
				update = false;
		}
		else
		{
			ASSERT(!m_LastGlobalLCPStatus.empty());

			m_Template.m_ColumnExpirationPolicyStatus->RemovePresetFlags(g_ColumnsPresetMore);
			m_Template.m_ColumnExpireOnDateTime->RemovePresetFlags(g_ColumnsPresetMore);
			setUpNoLoadMoreColumn(m_Template.m_ColumnExpirationPolicyStatus);
			setUpNoLoadMoreColumn(m_Template.m_ColumnExpireOnDateTime);

			GridIgnoreModification ignoramus(*this);
			vector<GridBackendRow*> rows;
			GetBackend().GetRows(GridBackendUtil::SELECTION_ALLNONGROUPNOTEXPLOSIONADDED, rows);
			ScopedImplodeRows scopedImplodeRows(*this, rows, p_RefreshGrid);
			for (auto row : rows)
			{
				if (isOffice365Group(row))
				{
					if (m_LastLCPError)
						row->AddField(m_LastLCPError, m_Template.m_ColumnExpirationPolicyStatus, GridBackendUtil::ICON_ERROR);
					else
						row->AddField(m_LastGlobalLCPStatus, m_Template.m_ColumnExpirationPolicyStatus);
					updateExpirationDate(row);
					update = true;
				}
			}
			if (scopedImplodeRows.HasExplosionsToRestore())
				update = false;
		}
	}

	if (update && p_RefreshGrid)
		UpdateMegaSharkOptimized(vector<GridBackendColumn*>{ m_Template.m_ColumnExpirationPolicyStatus, m_Template.m_ColumnExpireOnDateTime });

	if (askForReloadCounter > 0)
	{
		YCallbackMessage::DoPost([this, askForReloadCounter]()
		{
			YCodeJockMessageBox dlg(
				this,
				DlgMessageBox::eIcon_Question,
				YtriaTranslate::Do(GridGroups_SetLCP_1, _YLOC("Group Expiration policy")).c_str(),
				YtriaTranslate::Do(GridGroups_SetLCP_4, _YLOC("The Group Expiration Policy has changed to 'Selected', %1 groups' status require a reload.\nProceed?"), Str::getStringFromNumber(askForReloadCounter).c_str()).c_str(),
				L"",
				{ { IDOK, YtriaTranslate::Do(GridGroups_SetLCP_2, _YLOC("Yes")).c_str() }, { IDCANCEL, YtriaTranslate::Do(GridGroups_SetLCP_3, _YLOC("No")).c_str()} });

			if (IDOK == dlg.DoModal())
			{
				vector<BusinessGroup> groups;
				vector<row_pk_t> pks;

				for (auto& row : GetBackendRows())
				{
					if (isOffice365Group(row)
						&& !row->IsCreated() && !row->IsDeleted() && !row->IsModified() // Warn and revert instead?
						&& !row->IsExplosionAdded()
						&& g_PolicyHasChangedStatus == row->GetField(m_Template.m_ColumnExpirationPolicyStatus).GetValueStr()
						)
					{
						groups.emplace_back();
						m_Template.GetBusinessGroup(*this, row, groups.back());
						pks.emplace_back();
						GetRowPK(row, pks.back());
					}
				}
				
				ASSERT(groups.size() == pks.size());
				
				ASSERT(!groups.empty());
				if (!groups.empty())
				{
					UpdatedObjectsGenerator<BusinessGroup> updatedGroupsGenerator;
					updatedGroupsGenerator.SetObjects(groups);
					updatedGroupsGenerator.SetPrimaryKeys(pks);
				
					CommandInfo info;
					info.Data() = updatedGroupsGenerator;
					info.SetFrame(m_Frame);
					info.SetRBACPrivilege(m_Frame->GetModuleCriteria().m_Privilege);
					CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::ListLCPGroup, info, { this, g_ActionNamePolicyCreate/*TODO*/, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
				}
			}
		});
	}

	return statusChanged || daysChanged;
}

void GridGroups::AddCreatedBusinessObjects(const vector<BusinessGroup>& p_Groups, bool p_ScrollToNew)
{
	GridBackendRow* rowToFocus = nullptr;

	{
		GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
		for (const auto& group : p_Groups)
		{
			ASSERT(group.HasFlag(BusinessObject::CREATED) && IsTemporaryCreatedObjectID(group.GetID()));

			boost::YOpt<BusinessGroup> fixedSecurityGroup;
			// For an unknown reason Graph API requires a Mail Nickname even for Security groups (whereas O365 web interface doesn't)
			// Just put what the web interface puts in background.
			if (group.GetCombinedGroupType() && *group.GetCombinedGroupType() == BusinessGroup::g_SecurityGroup && !group.GetMailNickname())
			{
				fixedSecurityGroup = group;
				fixedSecurityGroup->SetMailNickname(PooledString(_YTEXT("00000000-0000-0000-0000-000000000000")));
			}

			auto row = m_Template.AddRow(*this, fixedSecurityGroup ? *fixedSecurityGroup : group, vector<GridBackendField>(), updater, true, nullptr, {}, false);
			ASSERT(nullptr != row && !row->IsModified() && !row->IsDeleted());
			if (nullptr != row && row->IsCreated() && (row->IsError() || row->IsDeleted()))
			{
				row->SetEditError(false);
				row->SetEditDeleted(false);
			}

			if (p_ScrollToNew)
			{
				// Shouldn't have several users in this case
				ASSERT(nullptr == rowToFocus);
				rowToFocus = row;
			}
			else
			{
				row->SetSelected(true);
			}
		}
	}

	if (p_ScrollToNew && nullptr != rowToFocus)
	{
		OnUnSelectAll();

		//FIXME: This call doesn't work when grouped. As it's not refreshed yet, the new row is not grouped yet. Only select the row as a fallback.
		//SelectRowAndExpandParentGroups(rowToFocus, false);
		rowToFocus->SetSelected(true);

		ScrollToRow(rowToFocus);
	}
}

void GridGroups::RemoveBusinessObjects(const vector<BusinessGroup>& p_Groups)
{
	bool removed = false;
	for (const auto& group : p_Groups)
		removed = m_Template.RemoveRow(*this, group) || removed;

	if (removed)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
}

void GridGroups::UpdateBusinessObjects(const vector<BusinessGroup>& p_Groups, bool p_SetModifiedStatus)
{
	GridUpdater errorHelper(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
	for (const auto& group : p_Groups)
	{
		ASSERT(!group.HasFlag(BusinessObject::CREATED) && !IsTemporaryCreatedObjectID(group.GetID()));
		auto row = m_Template.UpdateRow(*this, group, p_SetModifiedStatus && !group.HasFlag(BusinessObject::CREATED), errorHelper, false);
		ASSERT(!row->IsCreated() && !row->IsDeleted());
		if (row->IsModified() && (row->IsError() || row->IsDeleted()))
		{
			row->SetEditError(false);
			row->SetEditDeleted(false);
		}
	}
}

void GridGroups::UpdateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh, bool p_DoNotShowLoadingErrors)
{
	updateGroupsLoadMore(p_Groups, p_FromRefresh, p_DoNotShowLoadingErrors, boost::none);
}

void GridGroups::updateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh, bool p_DoNotShowLoadingErrors, const boost::YOpt<GridUpdater::PostUpdateError>& p_UnprocessedPostUpdateError)
{
	GridIgnoreModification ignoramus(*this);
	GridUpdaterOptions updateOptions(GridUpdaterOptions::UPDATE_GRID
		//| GridUpdaterOptions::FULLPURGE
		| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		//| GridUpdaterOptions::REFRESH_PROCESS_DATA
		| GridUpdaterOptions::PROCESS_LOADMORE
		| (p_DoNotShowLoadingErrors ? GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG : 0));
	GridUpdater updater(*this, updateOptions);

	if (p_UnprocessedPostUpdateError)
		updater.InitPostUpdateError(*p_UnprocessedPostUpdateError);

	const auto tenantAllowAddToGuest = getTenantAllowAddToGuests();

	for (const auto& group : p_Groups)
	{
		if (!p_FromRefresh)
		{
			vector<GridBackendField> BGpk;
			m_Template.GetBusinessGroupPK(*this, group, BGpk, {});
			GridBackendRow* rowToUpdate = GetRowIndex().GetExistingRow(BGpk);
			ASSERT(nullptr != rowToUpdate);
			if (nullptr != rowToUpdate)
				rowToUpdate->ClearStatus();
		}

		auto row = m_Template.UpdateRow(*this, group, false, updater, true);
		ASSERT(nullptr != row);
		if (isOffice365Group(row))
		{
			ASSERT(group.GetLCPStatus() || group.GetLCPStatusError() || m_LastGlobalLCPStatus.empty());
			updateGroupLCPStatus(row, group);
			updateExpirationDate(row);
			LoadMorePostProcessRow(row); // This call will be made (again) later in GridUpdater::processLoadMore(), but updateAddToGuests() needs this state right now...
			updateAddToGuests(row, tenantAllowAddToGuest);
		}

		if (group.HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
			m_Template.RemoveRow(*this, group);
		else
			updater.GetOptions().AddRowWithRefreshedValues(row);
	}
}

bool GridGroups::IsRowForLoadMore(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	const bool isTeamArchivedCol = m_Template.m_ColumnTeamIsArchived == p_Col;
	const bool isSpoSiteReadOnlyForMembersCol = m_Template.m_ColumnSpoSiteReadOnlyForMembers == p_Col;
	const bool isYammerCol = m_Template.m_ColumnIsYammer == p_Col;
	const bool isMembershipRuleCol = isMembershipRuleColumn(p_Col);
	return CacheGrid::IsRowForLoadMore(p_Row, p_Col)
		&& !p_Row->IsDeleted()
		&& !p_Row->IsCreated()
		&& GridUtil::IsBusinessGroup(p_Row)
		&& !IsTemporaryCreatedObjectID(p_Row->GetField(GetColId()).GetValueStr())
		&& (nullptr == p_Col
			|| isTeamArchivedCol && isTeam(p_Row) // this info is only for teams
			|| isYammerCol && isOffice365Group(p_Row) && !isTeam(p_Row)// this info is only for office65 but not teams
			|| isMembershipRuleCol && isDynamicMembershipGroup(p_Row) // only for groups with Dynamic Membership
			|| isOwnersColumn(p_Col) // apply on all groups
			|| isSettingsColumn(p_Col) // apply on all groups
			|| isMembersCountColumn(p_Col) // apply on all groups
			|| !isTeamArchivedCol && !isYammerCol && !isMembershipRuleCol && !isSpoSiteReadOnlyForMembersCol && isOffice365Group(p_Row) // apply on office365 groups
			);
}

RoleDelegationUtil::RBAC_Privilege GridGroups::GetPrivilegeEdit() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_EDIT;
}

RoleDelegationUtil::RBAC_Privilege GridGroups::GetPrivilegeCreate() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CREATE;
}

RoleDelegationUtil::RBAC_Privilege GridGroups::GetPrivilegeDelete() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_DELETE;
}

void GridGroups::SetTenantGroupSettings(boost::YOpt<std::vector<BusinessGroupSetting>>& p_GroupSettings, bool p_UpdateGrid)
{
	const bool changed = p_GroupSettings != m_GroupSettings;

	if (changed)
	{
		m_GroupSettings = p_GroupSettings;

		if (p_UpdateGrid)
		{
			const auto tenantAllowAddToGuest = getTenantAllowAddToGuests();
			GridIgnoreModification ignoramus(*this);
			vector<GridBackendRow*> rows;
			GetBackend().GetRows(GridBackendUtil::SELECTION_ALLNONGROUPNOTEXPLOSIONADDED, rows);
			ScopedImplodeRows scopedImplodeRows(*this, rows, true);
			bool update = false;
			for (auto row : rows)
			{
				if (isOffice365Group(row))
				{
					updateAddToGuests(row, tenantAllowAddToGuest);
					update = true;
				}
			}
			if (update && !scopedImplodeRows.HasExplosionsToRestore())
				UpdateMegaSharkOptimized(vector<GridBackendColumn*>{ m_ColumnComputedAllowToAddGuests });
		}
	}
}

const boost::YOpt<std::vector<BusinessGroupSetting>>& GridGroups::GetTenantGroupSettings() const
{
	return m_GroupSettings;
}

const boost::YOpt<Team> GridGroups::GetTeam(const GridBackendRow* p_Row) const
{
	boost::YOpt<Team> team;

	if (isTeam(p_Row))
		team = m_Template.GetTeam(p_Row);

	return team;
}

bool GridGroups::IsTeamColumn(GridBackendColumn* p_Column) const
{
	return m_Template.IsTeamColumn(p_Column);
}

bool GridGroups::IsArchiveTeamColumn(GridBackendColumn* p_Column) const
{
	return p_Column == m_Template.m_ColumnTeamIsArchived
		|| p_Column == m_Template.m_ColumnSpoSiteReadOnlyForMembers;
}

GridBackendColumn* GridGroups::GetIsTeamColumn() const
{
	return m_Template.m_ColumnIsTeam;
}

bool GridGroups::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	return m_Template.GetSnapshotValue(p_Value, p_Field, p_Column)
		|| GridGroupsBaseClass::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridGroups::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	return m_Template.LoadSnapshotValue(p_Value, p_Row, p_Column)
		|| GridGroupsBaseClass::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

void GridGroups::OnCustomDoubleClick()
{
	YTestCmdUI testAvailability(ID_MODULEGRID_EDIT);
	testAvailability.Enable(FALSE);
	OnCmdMsg(ID_MODULEGRID_EDIT, CN_UPDATE_COMMAND_UI, &testAvailability, nullptr);
	if (testAvailability.m_bEnabled)
		OnCmdMsg(ID_MODULEGRID_EDIT, CN_COMMAND, nullptr, nullptr);
}

void GridGroups::onPostExplodeMultiValueSisterhood(GridBackendRow* p_Row, const std::set<GridBackendColumn*>& p_Columns)
{
	if (nullptr != GetSecondaryStatusColumn())
	{
		const auto src = GetMultivalueManager().GetTopSourceRow(p_Row);
		if (nullptr != src && src != p_Row)
		{
			// Replicate on prem status
			if (src->HasField(GetSecondaryStatusColumn()))
				p_Row->AddField(src->GetField(GetSecondaryStatusColumn()));
		}
	}
}
