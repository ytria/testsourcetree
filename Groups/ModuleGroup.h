#pragma once

#include "ModuleBase.h"
#include "BusinessLifeCyclePolicies.h"
#include "GridGroupsRecycleBin.h"

class AutomationAction;
class FrameGroups;
class FrameGroupsRecycleBin;
class O365Grid;
class O365UpdateOperation;

class ModuleGroup : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

	const BusinessLifeCyclePolicies& GetGroupLifeCyclePolicies() const;
	void SetGroupLifeCyclePolicies(const BusinessLifeCyclePolicies& p_LCP);

private:
	virtual void executeImpl(const Command& p_Command) override;
	void show(Command p_Command);// show groups from IDs

	template <class FrameClass>
	void showItems(const Command& p_Command);// show these groups from IDs with their members

	void showRecycleBin(const Command& p_Command);
	void updateGroupItems(const Command& p_Command);
	void update(const Command& p_Command, const wstring& p_TaskText);
	void more(const Command& p_Command);
	void updateLCP(const Command& p_Command); // Create, update and delete a Lifecycle policy.
	void showGroupLCPStatus(const Command& p_Command);
	void updateLCPGroup(const Command& p_Command);
    void updateSettings(const Command& p_Command);
	void loadSnapshot(const Command& p_Command);
	void saveOnPrem(const Command& p_Command);

	void loadGroups(const Command& p_Command, const CommandInfo& p_CommandInfo, FrameGroups* p_Frame, bool p_IsRefresh, bool p_Sync);
    void getGroupsWithIndividualRequests(const Command& p_Command, const CommandInfo& p_Info, FrameGroups* p_Frame, bool p_IsRefresh);

	void ensureOwnersAndCreatedOnBehalfOfAreInCache(vector<BusinessGroup>& p_BusinessGroups, const std::shared_ptr<BusinessObjectManager>& p_BOM, YtriaTaskData p_TaskData);

	static const wstring& getTaskname(BusinessObject::UPDATE_TYPE p_UpdateType);

private:
	BusinessLifeCyclePolicies m_GroupLifeCyclePolicies;

	void moreImpl(FrameGroups* p_Frame, const Command& p_Command);
	void moreImpl(FrameGroupsRecycleBin* p_Frame, const Command& p_Command);
};
