#pragma once

#include "BaseO365Grid.h"
#include "GridTemplateGroups.h"
#include "GridUpdater.h"

class FrameGroupsRecycleBin;

class GridGroupsRecycleBin : public O365Grid
{
public:
	GridGroupsRecycleBin();
	virtual ~GridGroupsRecycleBin();

	void BuildView(vector<BusinessGroup> p_Groups, const vector<BusinessGroup>& p_LoadedMoreGroups, bool p_Refresh, const vector<O365UpdateOperation>& p_UpdateOperations);
	void UpdateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh, bool p_DoNotShowLoadingErrors);
	void UpdateGroupLCPStatus(const vector<BusinessGroup>& p_Groups);

	afx_msg void OnRestore();
	afx_msg void OnUpdateRestore(CCmdUI* pCmdUI);

	afx_msg void OnHardDelete();
	afx_msg void OnUpdateHardDelete(CCmdUI* pCmdUI);

	afx_msg void OnLoadMore();
	afx_msg void OnUpdateLoadMore(CCmdUI* pCmdUI);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	vector<BusinessGroup> GetLoadMoreRequestInfo(const std::vector<GridBackendRow*>& p_Rows);

protected:
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
	virtual void customizeGrid() override;
	virtual void InitializeCommands() override;
	virtual BOOL PreTranslateMessage(MSG* pMsg) override;
	virtual void customizeGridPostProcess() override;

	DECLARE_MESSAGE_MAP()

private:
    void RemoveGroupsNotModified(vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations);
	bool isOffice365Group(const GridBackendRow* p_Row) const;
	void updateGroupLCPStatus(GridBackendRow* p_Row, const BusinessGroup& p_BG);

	void LoadMoreImpl(const std::vector<GridBackendRow*>& p_Rows);
	void updateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh, bool p_DoNotShowLoadingErrors, const boost::YOpt<GridUpdater::PostUpdateError>& p_UnprocessedPostUpdateError);

	void setUpLoadMoreColumn(GridBackendColumn* p_Col);
	void setUpNoLoadMoreColumn(GridBackendColumn* p_Col);

	virtual const wstring&	annotationGetModuleName() const override;
	virtual void			annotationModuleSetDefault(GridAnnotation& p_NewAnnotation) const override;

	GridTemplateGroups m_TemplateGroups;
	FrameGroupsRecycleBin* m_Frame;

	struct LoadMoreConfig
	{
		wstring m_Tooltip;
		wstring m_Text;
		int m_Icon = 0;
	};
	LoadMoreConfig m_LoadMoreConfig;

	HACCEL m_hAccelSpecific = nullptr;
};
