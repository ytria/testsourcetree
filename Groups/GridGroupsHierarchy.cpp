#include "GridGroupsHierarchy.h"

#include "AutomationWizardGroupsMembers.h"
#include "BasicGridSetup.h"
#include "BusinessGroup.h"
#include "BusinessGroupConfiguration.h"
#include "DlgAddItemsToGroups.h"
#include "FrameGroupMembersHierarchy.h"
#include "GridUpdater.h"
#include "../Resource.h"
#include "BusinessUserGuest.h"

BEGIN_MESSAGE_MAP(GridGroupsHierarchy, GridGroupsCommon)
	ON_COMMAND(ID_MEMBERSGRID_ADD,					OnAddItemsToGroups)
	ON_UPDATE_COMMAND_UI(ID_MEMBERSGRID_ADD,		OnUpdateAddItemsToGroups)
	ON_COMMAND(ID_MEMBERSGRID_DELETE,				OnRemoveItemsFromGroups)
	ON_UPDATE_COMMAND_UI(ID_MEMBERSGRID_DELETE,		OnUpdateRemoveItemsFromGroups)
	ON_COMMAND(ID_MEMBERSGRID_COPYTO,				OnCopySelectionToGroups)
	ON_UPDATE_COMMAND_UI(ID_MEMBERSGRID_COPYTO,		OnUpdateCopySelectionToGroups)
	ON_COMMAND(ID_MEMBERSGRID_MOVETO,				OnMoveSelectionToGroups)
	ON_UPDATE_COMMAND_UI(ID_MEMBERSGRID_MOVETO,		OnUpdateMoveSelectionToGroups)
#if MEMBERS_GRID_HIDE_NESTED
	ON_COMMAND(ID_MEMBERSGRID_SHOWNESTED,			OnShowNested)
	ON_UPDATE_COMMAND_UI(ID_MEMBERSGRID_SHOWNESTED, OnUpdateShowNested)
#endif
	ON_COMMAND(ID_OWNERSSGRID_ADD,				OnAddOwnersToGroups)
	ON_UPDATE_COMMAND_UI(ID_OWNERSSGRID_ADD,	OnUpdateAddOwnersToGroups)

	ON_COMMAND(ID_MEMBERSGRID_UPGRADETOOWNER, OnUpgradeToOwner)
	ON_UPDATE_COMMAND_UI(ID_MEMBERSGRID_UPGRADETOOWNER, OnUpdateUpgradeToOwner)
	ON_COMMAND(ID_MEMBERSGRID_DOWNGRADETOMEMBER, OnDowngradeToMember)
	ON_UPDATE_COMMAND_UI(ID_MEMBERSGRID_DOWNGRADETOMEMBER, OnUpdateDowngradeToMember)
	ON_COMMAND(ID_MEMBERSGRID_ADDTOMEMBERS, OnAddToMembers)
	ON_UPDATE_COMMAND_UI(ID_MEMBERSGRID_ADDTOMEMBERS, OnUpdateAddToMembers)
END_MESSAGE_MAP()

GridGroupsHierarchy::GridGroupsHierarchy()
	: GridGroupsCommon()
	, m_Frame(nullptr)
#if MEMBERS_GRID_HIDE_NESTED
	, m_ShowNestedGroupMembers(false)
	, m_HasNestedGroupsMembers(false)
#endif
	, m_ColumnMemberKind(nullptr)
	, m_ColumnHierarchyPath(nullptr)
	, m_ColumnHasLicensesAssigned(nullptr)
{
	m_MayContainUnscopedUserGroupOrSite = true;

	initWizard<AutomationWizardGroupsMembers>(_YTEXT("Automation\\GroupsMembers"));
}

void GridGroupsHierarchy::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameGroupsHierarchy, LicenseUtil::g_codeSapio365groupMembers,
		{
			{ nullptr, _YTEXT(""), _YTEXT("") },
			{ nullptr, _YTEXT(""), _YTEXT("") },
			{ &m_TemplateUsers.m_ColumnMetaID, YtriaTranslate::Do(GridGroupsHierarchy_customizeGrid_1, _YLOC("Group ID")).c_str(), GridTemplate::g_TypeGroup }
		});
		setup.Setup(*this, true);
		m_TemplateGroups.m_ColumnLastRequestDateTime = setup.GetColumnLastRequestDateTime();
	}

	// We create multivalue-ids for child-groups.
	m_TemplateUsers.m_ColumnMetaID->SetExplosionEnabled(false);

	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_DISPLAYNAME), GridTemplate::g_TypeGroup, { g_ColumnsPresetDefault });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_ISTEAM), GridTemplate::g_TypeGroup, { g_ColumnsPresetDefault });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_GROUPTYPE), GridTemplate::g_TypeGroup, { g_ColumnsPresetDefault });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_DYNAMICMEMBERSHIP), GridTemplate::g_TypeGroup, { g_ColumnsPresetDefault });

	{
		static const wstring g_FamilyLicensing = _T("Licensing");
		m_ColumnHasLicensesAssigned = AddColumnCheckBox(_YUID("HASASSIGNEDLICENSES"), _T("Has licenses assigned"), g_FamilyLicensing);
		m_ColumnHasLicensesAssigned->SetPresetFlags({ g_ColumnsPresetDefault });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_LICENSEPROCESSINGSTATE), g_FamilyLicensing, {});
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_ASSIGNEDLICENSES), g_FamilyLicensing, { });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("assignedLicenses.skuId"), g_FamilyLicensing, { g_ColumnsPresetTech });
	}

	setBasicGridSetupHierarchy({ { { m_TemplateGroups.m_ColumnDisplayName, GridBackendUtil::g_AllHierarchyLevels } }
								, O365Grid::BasicGridSetupHierarchy::AllUserObjectTypes(true)
								,{ rttr::type::get<BusinessUser>().get_id(), rttr::type::get<BusinessGroup>().get_id()/*, rttr::type::get<BusinessOrgContact>().get_id()*/ } });

	// Do not insert columns that are also PK
	m_MetaColumns.insert(m_MetaColumns.end(),
		{
			//m_TemplateUsers.m_ColumnMetaID,
			m_TemplateGroups.m_ColumnDisplayName,
			m_TemplateGroups.m_ColumnIsTeam,
			m_TemplateGroups.m_ColumnCombinedGroupType,
			m_TemplateGroups.m_ColumnDynamicMembership,
			m_TemplateGroups.m_ColumnLicenseProcessingState,
			m_TemplateGroups.m_ColumnSkuIds,
			m_TemplateGroups.m_ColumnSkuPartNumbers,
			m_ColumnHasLicensesAssigned,
		});

	/*Add additional meta columns*/
	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		for (const auto& c : *metaDataColumnInfos)
		{
			ASSERT(c.GetPropertyName() == _YUID(O365_GROUP_DYNAMICMEMBERSHIP)
				|| c.GetPropertyName() == _YUID(O365_GROUP_LICENSEPROCESSINGSTATE)
				|| c.GetPropertyName() == _YUID(O365_GROUP_ASSIGNEDLICENSES)
				|| c.GetPropertyName() == _YUID("assignedLicenses.skuId")
				|| nullptr == m_TemplateGroups.GetColumnForProperty(c.GetPropertyName()));
			if (nullptr == m_TemplateGroups.GetColumnForProperty(c.GetPropertyName()))
			{
				auto col = c.AddTo(*this, GridTemplate::g_TypeGroup);
				m_TemplateGroups.GetColumnForProperty(c.GetPropertyName()) = col;
				m_MetaColumns.push_back(col);
				m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
			}
		}
	}

	static const wstring memberFamily = _T("Member");

	m_ColumnHierarchyPath = AddColumn(_YUID("groupHierarchy"), _T("Group Hierarchy"), memberFamily, g_ColumnSize22, { g_ColumnsPresetTech });
	m_ColumnMemberKind = AddColumnIcon(_YUID("membershipType"), _T("Membership Type"), memberFamily);
	SetColumnPresetFlags(m_ColumnMemberKind, { g_ColumnsPresetDefault });

	m_TemplateUsers.m_ColumnDisplayName			= AddColumn(_YUID("memberDisplayName"), _T("Member Display Name"), memberFamily,	g_ColumnSize22, { g_ColumnsPresetDefault });

	m_TemplateUsers.m_ColumnUserPrincipalName	= AddColumn(_YUID(O365_USER_USERPRINCIPALNAME), YtriaTranslate::Do(GridGroupsHierarchy_customizeGrid_5, _YLOC("Username")).c_str(),	memberFamily,	g_ColumnSize32);

	m_ColumnIsTeamForMember = AddColumnIcon(_YUID("subGroupIsTeam"), _T("Subgroup is a Team"), memberFamily);
	m_ColumnCombinedGroupTypeForMember = AddColumn(_YUID("subGroupType"), _T("Subgroup Type"), memberFamily, g_ColumnSize12);

	addColumnGraphID(GridTemplate::g_TypeUser);
	m_TemplateGroups.m_ColumnID = GetColId();
	m_TemplateUsers.m_ColumnID	= GetColId();
	m_TemplateGroups.CustomizeGrid(*this);

	AddColumnForRowPK(GetColId());
	AddColumnForRowPK(m_TemplateUsers.m_ColumnMetaID);

	if (nullptr != m_ColumnMemberKind)
	{
		ASSERT(m_KindInfos.empty());

		m_KindInfos =
		{ { MEMBER_USER					, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_MEMBER_USER))					, _T("Member") } }
		, { OWNER_MEMBER_USER			, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_OWNER))						, _T("Owner & Member") } }
		, { EXTERNAL_OWNER				, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_EXTERNAL_OWNER))				, _T("Owner ONLY") } }
		, { MEMBER_GUEST				, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_GUEST_MEMBER))					, _T("Guest Member") } }
		, { OWNER_MEMBER_GUEST			, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_OWNER_GUEST_MEMBER))			, _T("Owner & Guest Member") } }
		, { EXTERNAL_OWNER_GUEST		, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_EXTERNAL_OWNER_GUEST))			, _T("Owner ONLY (Guest)") } }
		, { MEMBER_GROUP				, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_MEMBER_GROUP))					, _T("Group") } }
		, { MEMBER_ORGCONTACT			, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_MEMBER_ORGCONTACT))			, _T("Org. Contact") } }
		, { INDIRECT_MEMBER_USER		, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_INDIRECT_MEMBER_USER))			, _T("Indirect Member") } }
		, { INDIRECT_MEMBER_GUEST		, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_INDIRECT_MEMBER_GUEST))		, _T("Indirect Guest Member") } }
		, { INDIRECT_MEMBER_GROUP		, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_INDIRECT_MEMBER_GROUP))		, _T("Indirect Member (Group)") } }
		, { INDIRECT_MEMBER_ORGCONTACT	, { AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_INDIRECT_MEMBER_ORGCONTACT))	, _T("Indirect Member (Org. Contact)") } } };
	}

	m_Frame = dynamic_cast<FrameGroupMembersHierarchy*>(GetParentFrame());
    m_Frame->SetGrid(this);
	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_Frame);
}

wstring GridGroupsHierarchy::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Group"), m_TemplateGroups.m_ColumnDisplayName } }, m_TemplateUsers.m_ColumnDisplayName);
}

O365Grid::SnapshotCaps GridGroupsHierarchy::GetSnapshotCapabilities() const
{
	return { true };
}

void GridGroupsHierarchy::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
#if MEMBERS_GRID_HIDE_NESTED
		pPopup->ItemInsert(ID_MEMBERSGRID_SHOWNESTED);
		pPopup->ItemInsert(); // Sep
#endif
		pPopup->ItemInsert(ID_MEMBERSGRID_ADD);
		pPopup->ItemInsert(ID_OWNERSSGRID_ADD);
		pPopup->ItemInsert(); // Sep
		pPopup->ItemInsert(ID_MEMBERSGRID_DELETE);
		pPopup->ItemInsert(ID_MEMBERSGRID_COPYTO);
		pPopup->ItemInsert(ID_MEMBERSGRID_MOVETO);
		pPopup->ItemInsert(ID_MEMBERSGRID_UPGRADETOOWNER);
		pPopup->ItemInsert(); // Sep
		pPopup->ItemInsert(ID_MEMBERSGRID_DOWNGRADETOMEMBER);
		pPopup->ItemInsert(ID_MEMBERSGRID_ADDTOMEMBERS);
		pPopup->ItemInsert(); // Sep
	}

	O365Grid::OnCustomPopupMenu(pPopup, nStart);
}

void GridGroupsHierarchy::InitializeCommands()
{
	O365Grid::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MEMBERSGRID_ADD;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_1, _YLOC("Add Members�")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_2, _YLOC("Add Members�")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);

		g_CmdManager->CmdSetIcon(profileName, ID_MEMBERSGRID_ADD, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_GROUPS_ADDUSER_16X16));

		setRibbonTooltip(
			_cmd.m_nCmdID, 
			YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_3, _YLOC("Add to Groups")).c_str(), 
			YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_4, _YLOC("Add members to all selected groups.\nYou will be able to select users or groups from a directory list.\nNote: This function is only available in Hierarchy view (Ctrl+F11).")).c_str(), 
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MEMBERSGRID_DELETE;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_5, _YLOC("Remove From")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_6, _YLOC("Remove From")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);

		g_CmdManager->CmdSetIcon(profileName, ID_MEMBERSGRID_DELETE, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_GROUPS_REMOVEUSER_16X16));
		
		setRibbonTooltip(
			_cmd.m_nCmdID, 
			YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_7, _YLOC("Remove Selected Group Memberships")).c_str(), 
			YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_8, _YLOC("Remove all selected groups memberships. Clicking this button will add a trash can icon in the status column. To save your changes to the server, use one of the Save buttons.\nTip: Grouping by the status column is a quick way to review all your pending changes.")).c_str(), 
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MEMBERSGRID_COPYTO;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_9, _YLOC("Add To...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_10, _YLOC("Add To...")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);

		g_CmdManager->CmdSetIcon(profileName, ID_MEMBERSGRID_COPYTO, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_GROUPS_COPYUSERTO_16X16));

		setRibbonTooltip(
			_cmd.m_nCmdID, 
			YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_11, _YLOC("Add To...")).c_str(), 
			YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_12, _YLOC("Add the currently selected group members to additional groups.\nYou will be able to select additional groups from a directory list.")).c_str(), 
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MEMBERSGRID_MOVETO;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_13, _YLOC("Move To�")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_14, _YLOC("Move To�")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);

		g_CmdManager->CmdSetIcon(profileName, ID_MEMBERSGRID_MOVETO, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_GROUPS_MOVEUSERTO_16X16));

		setRibbonTooltip(
			_cmd.m_nCmdID, 
			YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_15, _YLOC("Move Members to Other Groups")).c_str(),
			YtriaTranslate::Do(GridGroupsHierarchy_InitializeCommands_16, _YLOC("Transfer the currently selected members to other groups.\nYou will be able to select groups from a directory list.\nNote: Unlike �Add To�, this will also remove these selected members from their currently assigned group. ")).c_str(), 
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_OWNERSSGRID_ADD;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_1, _YLOC("Add Owners...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_2, _YLOC("Add Owners...")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_ADDOWNER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_OWNERSSGRID_ADD, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_3, _YLOC("Add Owners to Groups")).c_str(), YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_4, _YLOC("Add users as owners to all selected groups. You will be able to select users from a directory list.")).c_str(), _cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MEMBERSGRID_UPGRADETOOWNER;
		_cmd.m_sMenuText = _T("Promote to Owner");
		_cmd.m_sToolbarText = _T("Promote to Owner");
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_UPGRADETOOWNER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MEMBERSGRID_UPGRADETOOWNER, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, _T("Promote to Owner"), _T("Promote the selected members to owner of their group"), _cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MEMBERSGRID_DOWNGRADETOMEMBER;
		_cmd.m_sMenuText = _T("Demote to Member");
		_cmd.m_sToolbarText = _T("Demote to Member");
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_DOWNGRADETOMEMBER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MEMBERSGRID_DOWNGRADETOMEMBER, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, _T("Demote to Member"), _T("Demote the selected owners to regular member of their group"), _cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MEMBERSGRID_ADDTOMEMBERS;
		_cmd.m_sMenuText = _T("Add to Members");
		_cmd.m_sToolbarText = _T("Add to Members");
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_ADDUSER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MEMBERSGRID_ADDTOMEMBERS, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, _T("Add to Members"), _T("Add the selected external owners to members of the group they own"), _cmd.m_sAccelText);
	}

#if MEMBERS_GRID_HIDE_NESTED
	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MEMBERSGRID_SHOWNESTED;
		_cmd.m_sMenuText = _T("Nested Group");
		_cmd.m_sToolbarText = _T("Nested Group");
		g_CmdManager->CmdSetup(profileName, _cmd);

		g_CmdManager->CmdSetIcon(profileName, ID_MEMBERSGRID_SHOWNESTED, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_MEMBERSGRID_SHOWNESTED_16X16));

		setRibbonTooltip(
			_cmd.m_nCmdID,
			_T("Show Nested Group Members"),
			_T("Show the nested group members info in distribution list groups and mail-enabled security groups."),
			_cmd.m_sAccelText);
	}
#endif
}

void GridGroupsHierarchy::OnAddItemsToGroups()
{
	if (IsFrameConnected())
	{
		set<GridBackendRow*> businessGroupRows;
		std::map<PooledString, std::pair<PooledString, std::set<PooledString>>> parentsForSelection; // <Group Id, <Group name, User ids>>
		bool modifyMembersWarn = true;

		auto addTargetGroup = [this, &businessGroupRows, &parentsForSelection, &modifyMembersWarn](GridBackendRow* row)
		{
			GridBackendRow* businessGroupRow = row->GetTopAncestorOrThis();
			if (nullptr != businessGroupRow && IsGroupAuthorizedByRoleDelegation(businessGroupRow, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT))
			{
				if (modifyMembersWarn)
				{
					const auto isGroup = GridUtil::IsBusinessGroup(row);
					if (isGroup && row != businessGroupRow
						|| !isGroup && row->GetParentRow() != businessGroupRow)
					{
						YCodeJockMessageBox dlg(this
							, DlgMessageBox::eIcon_Information
							, YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_9, _YLOC("Add Member")).c_str()
							, YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_10, _YLOC("New members will be added to top-level groups only!")).c_str()
							, _YTEXT("")
							, { { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });

						if (IDCANCEL == dlg.DoModal())
							return false;

						modifyMembersWarn = false;
					}
				}

				ASSERT(GridUtil::IsBusinessGroup(businessGroupRow));
				if (GridUtil::IsBusinessGroup(businessGroupRow) && businessGroupRows.insert(businessGroupRow).second)
					parentsForSelection[businessGroupRow->GetField(GetColId()).GetValueStr()] = { businessGroupRow->GetField(m_TemplateGroups.m_ColumnDisplayName).GetValueStr(), GetIdsOfChildRowsThatAllMatch({ businessGroupRow }) };
			}

			return true;
		};

		for (auto row : GetSelectedRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row && !row->IsGroupRow())
			{
				if (!addTargetGroup(row))
					return;
			}
		}

		ASSERT(!businessGroupRows.empty());
		if (!businessGroupRows.empty())
		{
			const auto loadType = DlgAddItemsToGroups::SELECT_USERSGROUPSANDORGCONTACTS_FOR_GROUPS;
			const auto title = YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_7, _YLOC("Add to %1 selected groups"), Str::getStringFromNumber(businessGroupRows.size()).c_str());
			const auto explanation = YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_8, _YLOC("These %1 groups will be modified by adding any members you select from the list below.\nTo see the complete list, use Load full directory."), Str::getStringFromNumber(businessGroupRows.size()).c_str());

			vector<BusinessUser>		users;
			vector<BusinessGroup>		groups;
			vector<BusinessOrgContact>	orgContacts;
			getUsersGroupsAndOrgContacts(
				users,
				true,
				groups,
				true,
				orgContacts,
				true);

			DlgAddItemsToGroups dlg(
				GetSession(),
				parentsForSelection,
				loadType,
				users,
				groups,
				orgContacts,
				title,
				explanation,
				IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE)
				? RoleDelegationUtil::RBAC_Privilege::RBAC_NONE
				: RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE,
				this);

			if (IDOK == dlg.DoModal())
			{
				GridBackendRow* rowToScrollTo = nullptr;
				bool update = false;

				if (nullptr != GetColId())
				{
					// List of all new users to add to selected groups.
					auto& newItems = dlg.GetSelectedItems();

					// Add new users.
					for (auto groupRow : businessGroupRows)
					{
						ASSERT(nullptr != groupRow);
						if (nullptr != groupRow)
						{
							const GridBackendField& fGroupGraphID = groupRow->GetField(GetColId());

							auto parentRows = GetRows(fGroupGraphID.GetValueStr(), GetColId()->GetID());

							vector<vector<GridBackendRow*>> addedRows;
							for (const auto& item : newItems)
							{
								if (addedRows.empty() || !addedRows.back().empty())
									addedRows.emplace_back();

								for (auto parentGroupRow : parentRows)
								{
									const GridBackendField& fGroupName = parentGroupRow->GetField(m_TemplateGroups.m_ColumnDisplayName);
									const GridBackendField& fGroupType = parentGroupRow->GetField(m_TemplateGroups.m_ColumnCombinedGroupType);

									// Recycle the row if already in.
									GridBackendRow* pURow = GetChildRow(parentGroupRow, item.GetID(), GetColId()->GetID());

									if (nullptr == pURow)
									{
										GridBackendField fieldId(item.GetID(), GetColId());

										// Meta ID is a multivalue with all the parents ids
										vector<PooledString> metaID;
										{
											auto* parent = parentGroupRow;
											while (nullptr != parent)
											{
												metaID.insert(metaID.begin(), parent->GetField(GetColId()).GetValueStr());
												parent = parent->GetParentRow();
											}
										}
										ASSERT(!metaID.empty());

										pURow = m_RowIndex.GetRow({ fieldId, m_TemplateUsers.GetMetaIDField(metaID) }, true, false);

										update = true;

										ASSERT(nullptr != pURow);
										if (nullptr != pURow)
										{
											pURow->SetParentRow(parentGroupRow);
											SetRowObjectType(pURow, item.GetObjectType());

											if (GridUtil::IsBusinessGroup(pURow))
											{
												for (auto c : m_MetaColumns)
												{
													if (parentGroupRow->HasField(c))
														pURow->AddField(parentGroupRow->GetField(c));
													else
														pURow->RemoveField(c);
												}

												if (nullptr != m_ColumnGroupNameForMember)
													pURow->AddField(item.GetPrincipalName(), m_ColumnGroupNameForMember);
												else
													pURow->AddField(item.GetPrincipalName(), m_TemplateGroups.m_ColumnDisplayName);

												if (nullptr != m_ColumnCombinedGroupTypeForMember)
													pURow->AddField(item.GetUserGroupType(), m_ColumnCombinedGroupTypeForMember);
												else if (!item.GetUserGroupType().IsEmpty())
													pURow->AddField(item.GetUserGroupType(), m_TemplateGroups.m_ColumnCombinedGroupType);

												if (nullptr != m_ColumnIsTeamForMember)
													GridUtil::SetRowIsTeam(item.GetGroupIsTeam(), pURow, m_ColumnIsTeamForMember, m_TemplateGroups.m_IconIsTeam);
												else
													GridUtil::SetRowIsTeam(item.GetGroupIsTeam(), pURow, m_TemplateGroups.m_ColumnIsTeam, m_TemplateGroups.m_IconIsTeam);
											}
											else if (GridUtil::IsBusinessUser(pURow))
											{
												for (auto c : m_MetaColumns)
												{
													if (parentGroupRow->HasField(c))
														pURow->AddField(parentGroupRow->GetField(c));
													else
														pURow->RemoveField(c);
												}

												pURow->AddField(item.GetPrincipalName(), m_TemplateUsers.m_ColumnUserPrincipalName);
												pURow->AddField(item.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
												if (nullptr != m_TemplateUsers.m_ColumnUserType)
													pURow->AddField(item.GetUserGroupType(), m_TemplateUsers.m_ColumnUserType);

												if (nullptr != m_ColumnGroupNameForMember)
												{
													const GridBackendField& field = groupRow->GetField(m_ColumnGroupNameForMember);
													if (field.HasValue())
														pURow->AddField(field.GetValueStr(), m_ColumnGroupNameForMember);
												}

												if (nullptr != m_ColumnCombinedGroupTypeForMember)
												{
													const GridBackendField& field = groupRow->GetField(m_ColumnCombinedGroupTypeForMember);
													if (field.HasValue())
														pURow->AddField(field.GetValueStr(), m_ColumnCombinedGroupTypeForMember);
												}
											}
											else if (GridUtil::IsBusinessOrgContact(pURow))
											{
												for (auto c : m_MetaColumns)
												{
													if (parentGroupRow->HasField(c))
														pURow->AddField(parentGroupRow->GetField(c));
													else
														pURow->RemoveField(c);
												}

												pURow->AddField(item.GetPrincipalName(), m_TemplateUsers.m_ColumnUserPrincipalName);
												pURow->AddField(item.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
											}
											else
											{
												ASSERT(false);
											}

											setParamAddMember(pURow);
											setMemberRowKind(pURow);

											addedRows.back().push_back(pURow);
										}
									}
									else
									{
										if (isExternalOwner(pURow))
										{
											if (upgradeExternalToRegularOwner(pURow))
												update = true;
										}
										else if (isDirectMember(pURow) && pURow->IsDeleted())
										{
											if (isOwner(pURow))
											{
												ASSERT(pURow->GetLParam() == BusinessObject::UPDATE_TYPE_DELETEOWNERS);
												row_pk_t rowPk;
												GetRowPK(pURow, rowPk);
												if (GetModifications().Revert(rowPk, false))
												{
													pURow->SetLParam(0);
													downgradeOwnerToMember(pURow);
													update = true;
												}
											}
											else
											{
												ASSERT(pURow->GetLParam() == BusinessObject::UPDATE_TYPE_DELETEMEMBERS);
												row_pk_t rowPk;
												GetRowPK(pURow, rowPk);
												if (GetModifications().Revert(rowPk, false))
												{
													pURow->SetLParam(0);
													update = true;
												}
											}
										}
									}
								}
							}

							if (nullptr == rowToScrollTo && !addedRows.empty() && !addedRows[0].empty())
							{
								OnUnSelectAll();
								rowToScrollTo = addedRows[0][0];
							}

							for (auto& rows : addedRows)
							{
								CreatedObjectModification* mainMod = nullptr;
								for (auto& row : rows)
								{
									row_pk_t rowPk;
									GetRowPK(row, rowPk);
									if (nullptr == mainMod)
									{
										mainMod = GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPk));
									}
									else
									{
										mainMod->AddSiblingModification(std::make_unique<CreatedObjectModification>(*this, rowPk));
									}
									SelectRowAndExpandParentGroups(row, false);
								}
							}
						}
					}

					if (update)
						UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

					if (nullptr != rowToScrollTo)
						ScrollToRow(rowToScrollTo);
				}
			}
		}
	}
}

void GridGroupsHierarchy::OnUpdateAddItemsToGroups(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& IsFrameConnected()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& p_Row)
		{
			return nullptr != p_Row
				&& !p_Row->IsGroupRow()
				&& IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis(), RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT);
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsHierarchy::OnRemoveItemsFromGroups()
{
	auto selectedRows = GetBackend().GetSelectedRows();
	for (auto it = selectedRows.begin(); it != selectedRows.end();)
	{
		if (!canRowBeDeleted(*it))
			it = selectedRows.erase(it);
		else
			++it;
	}

	bool update = false;
	const auto memberPrivilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT;
	const auto ownerPrivilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT;
	const bool hasOutOfScopeMemberPrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE);
	const bool hasOutOfScopeOwnerPrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE);

	map<row_pk_t, vector<GridBackendRow*>> rowsToRevert;
	std::set<GridBackendRow*> processedRows;
	std::set<GridBackendRow*> modifiedRows;
	bool cancelAll = false;
	for (auto row : selectedRows)
	{
		ASSERT(nullptr != row);

		if (nullptr != row && !row->IsGroupRow() && nullptr != row->GetParentRow())
		{
			ASSERT(isDirectMember(row) || isExternalOwner(row));
			const auto rowIsOwner = isOwner(row);
			const auto rowIsExtOwner = isExternalOwner(row);

			auto privileges =
				rowIsOwner && !rowIsExtOwner
				?	vector<RoleDelegationUtil::RBAC_Privilege>{ownerPrivilege, memberPrivilege}
				: rowIsOwner && rowIsExtOwner
					? vector<RoleDelegationUtil::RBAC_Privilege>{ ownerPrivilege , memberPrivilege }
					: vector<RoleDelegationUtil::RBAC_Privilege>{ memberPrivilege };

			auto selectedRow = row->GetAncestorAtLevel(1);
			if (std::all_of(privileges.begin(), privileges.end(), [this, selectedRow](auto& privilege)
				{
					return IsGroupAuthorizedByRoleDelegation(selectedRow->GetParentRow(), privilege);
				}))
			{
				const bool confirmNestedDeletion = selectedRow != row;
				boost::YOpt<int> deleteModified;

				vector<GridBackendRow*> siblingRows;
				// If item row (same ID / same parent ID) appears twice or more in the grid, mark all of them as deleted!
				siblingRows = GetChildRows(selectedRow->GetParentRow()->GetField(GetColId()).GetValueStr(), GetColId()->GetID(), selectedRow->GetField(GetColId()).GetValueStr(), GetColId()->GetID());

				ASSERT(siblingRows.size() == 1 || !rowIsOwner);

				DeletedObjectModification* mainMod = nullptr;
				vector<GridBackendRow*>* currentRevertList = nullptr;
				for (auto siblingRow : siblingRows)
				{
					if (processedRows.end() == processedRows.find(siblingRow))
					{
						ASSERT(GridUtil::IsBusinessUser(siblingRow) || GridUtil::IsBusinessGroup(siblingRow) || GridUtil::IsBusinessOrgContact(siblingRow));

						auto allowedOrgContact = [siblingRow]()
						{
							return GridUtil::IsBusinessOrgContact(siblingRow);
						};
						auto allowedRegularDirectMember = [this, siblingRow, hasOutOfScopeMemberPrivilege]()
						{
							return isDirectMember(siblingRow)
								&& !isOwner(siblingRow)
								&& (hasOutOfScopeMemberPrivilege
									|| IsUserAuthorizedByRoleDelegation(siblingRow, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)
									|| IsGroupAuthorizedByRoleDelegation(siblingRow, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
						};
						auto allowedOwner = [this, siblingRow, hasOutOfScopeMemberPrivilege, hasOutOfScopeOwnerPrivilege]()
						{
							return isOwner(siblingRow)
								&& !isExternalOwner(siblingRow)
								&& (hasOutOfScopeMemberPrivilege && hasOutOfScopeOwnerPrivilege
									|| IsUserAuthorizedByRoleDelegation(siblingRow, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
						};
						auto allowedExtOwner = [this, siblingRow, hasOutOfScopeOwnerPrivilege]()
						{
							return isExternalOwner(siblingRow)
								&& (hasOutOfScopeOwnerPrivilege
									|| IsUserAuthorizedByRoleDelegation(siblingRow, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
						};

						if (allowedOrgContact() || allowedRegularDirectMember() || allowedOwner() || allowedExtOwner())
						{
							row_pk_t siblingRowPk;
							GetRowPK(siblingRow, siblingRowPk);

							if (!siblingRow->IsDeleted())
							{
								bool processEntity = true;
								if (siblingRow->IsModified())
								{
									if (!deleteModified)
									{
										YCodeJockMessageBox confirmDlg(this
											, DlgMessageBox::eIcon_Question
											, YtriaTranslate::Do(YTriaGridGE_OnCustomPopupMenu_3, _YLOC("Remove Member")).c_str()
											, YtriaTranslate::Do(GridDriveItems_OnDeleteItem_2, _YLOC("The selected rows contain unsaved changes. Do you want to delete them instead?")).c_str()
											, YtriaTranslate::Do(GridDriveItems_OnDeleteItem_3, _YLOC("Pending changes will be lost.")).c_str()
											, { { IDOK, YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str() }
												, { IDCANCEL, YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str() }
												, { IDIGNORE, YtriaTranslate::Do(GridGroupsCommon_removeItemsFromGroup_4, _YLOC("Skip")).c_str() } }
										, IDOK);

										const auto res = confirmDlg.DoModal();
										deleteModified = res;
										if (res == IDCANCEL)
											cancelAll = true;
									}

									processEntity = IDOK == *deleteModified;
								}

								if (processEntity)
								{
									ASSERT(nullptr == GetModifications().GetRowModification<DeletedObjectModification>(siblingRowPk));
									// warn user - only once
									if (confirmNestedDeletion)
									{
										YCodeJockMessageBox confirmDlg(this
											, DlgMessageBox::eIcon_Information
											, YtriaTranslate::Do(YTriaGridGE_OnCustomPopupMenu_3, _YLOC("Remove Member")).c_str()
											, YtriaTranslate::Do(GridGroupsCommon_removeItemsFromGroup_7, _YLOC("'%1' will be removed from '%2'"), siblingRow->GetField(m_ColumnGroupNameForMember).GetValueStr(), siblingRow->GetField(m_TemplateGroups.m_ColumnDisplayName).GetValueStr())
											, _YTEXT("")
											, { { IDOK, YtriaTranslate::Do(GridGroupsCommon_removeItemsFromGroup_2, _YLOC("OK")).c_str() }
												, { IDCANCEL, YtriaTranslate::Do(GridGroupsCommon_removeItemsFromGroup_3, _YLOC("Cancel")).c_str() }
												, { IDIGNORE, YtriaTranslate::Do(GridGroupsCommon_removeItemsFromGroup_4, _YLOC("Skip")).c_str() } }
										, IDOK);

										const auto res = confirmDlg.DoModal();
										processEntity = IDOK == res;
										if (res == IDCANCEL)
											cancelAll = true;
									}

									if (processEntity)
									{
										if (row->IsCreated())
										{
											ASSERT(nullptr == mainMod);
											if (nullptr == currentRevertList)
												currentRevertList = &rowsToRevert[siblingRowPk];

											currentRevertList->push_back(siblingRow);
										}
										else
										{
											ASSERT(nullptr == currentRevertList);

											if (row->IsModified())
												GetModifications().Revert(siblingRowPk, false);

											if (nullptr == mainMod)
											{
												mainMod = GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, siblingRowPk));
											}
											else
											{
												ASSERT(!rowIsOwner);
												mainMod->AddSiblingModification(std::make_unique<DeletedObjectModification>(*this, siblingRowPk));
											}

											if (rowIsExtOwner)
												setParamDeleteOwner(siblingRow);
											else if (rowIsOwner)
												setParamDeleteMemberDeleteOwner(siblingRow);
											else
												setParamDeleteMember(siblingRow);

											update = true;
										}

										modifiedRows.insert(siblingRow);
									}
								}
							}
							else
							{
								ASSERT(nullptr != GetModifications().GetRowModification<DeletedObjectModification>(siblingRowPk));
							}
						}

						processedRows.insert(siblingRow);
					}
				}
			}
		}

		if (cancelAll)
			break;
	}

	if (!modifiedRows.empty())
	{
		OnUnSelectAll();
		for (auto row : modifiedRows)
			row->SetSelected(true);
	}

	for (auto item : rowsToRevert)
	{
		for (auto& row : item.second)
			row->SetLParam(0);
		if (GetModifications().Revert(item.first, false))
		{
			update = true;
		}
		else
		{
			ASSERT(false);
		}
	}

	if (update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridGroupsHierarchy::OnUpdateRemoveItemsFromGroups(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& p_Row)
		{
			return canRowBeDeleted(p_Row);
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsHierarchy::OnAddOwnersToGroups()
{
	if (IsFrameConnected())
	{
		// <Group Id, <Group name, User ids>>
		std::map<PooledString, std::pair<PooledString, std::set<PooledString>>> parentsForSelection;
		set<GridBackendRow*>	businessGroupRows;

		auto addTargetGroup = [this, &businessGroupRows, &parentsForSelection](GridBackendRow* row)
		{
			GridBackendRow* businessGroupRow = row->GetTopAncestorOrThis();
			if (nullptr != businessGroupRow && IsGroupAuthorizedByRoleDelegation(businessGroupRow, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT))
			{
				ASSERT(GridUtil::IsBusinessGroup(businessGroupRow));
				if (GridUtil::IsBusinessGroup(businessGroupRow) && businessGroupRows.insert(businessGroupRow).second)
					parentsForSelection[businessGroupRow->GetField(GetColId()).GetValueStr()] = { businessGroupRow->GetField(m_TemplateGroups.m_ColumnDisplayName).GetValueStr(), GetIdsOfChildRowsThatAllMatch({ businessGroupRow }) };
			}

			return true;
		};

		for (auto row : GetSelectedRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row && !row->IsGroupRow())
			{
				if (!addTargetGroup(row))
					return;
			}
		}

		ASSERT(!businessGroupRows.empty());
		if (!businessGroupRows.empty())
		{
			const auto loadType = DlgAddItemsToGroups::SELECT_USERS_FOR_GROUPS;
			const auto title = YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_5, _YLOC("Add users as owners to %1 selected groups"), Str::getStringFromNumber(businessGroupRows.size()).c_str());
			const auto explanation = YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_6, _YLOC("These %1 groups will have the users you select below added as owners.\nTo see the complete list, use Load full directory. "), Str::getStringFromNumber(businessGroupRows.size()).c_str());

			vector<BusinessUser>		users;
			vector<BusinessGroup>		groups;
			vector<BusinessOrgContact>	orgContacts;
			getUsersGroupsAndOrgContacts(
				users,
				true,
				groups,
				false,
				orgContacts,
				false);

			DlgAddItemsToGroups dlg( // DONE
				GetSession(),
				parentsForSelection,
				loadType,
				users,
				groups,
				orgContacts,
				title,
				explanation,
				IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE)
				? RoleDelegationUtil::RBAC_Privilege::RBAC_NONE
				: RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE,
				this);

			if (IDOK == dlg.DoModal())
			{
				GridBackendRow* rowToScrollTo = nullptr;
				bool update = false;

				if (nullptr != GetColId())
				{
					// List of all new users to add to selected groups.
					auto& newItems = dlg.GetSelectedItems();

					// Add new users.
					for (auto groupRow : businessGroupRows)
					{
						ASSERT(nullptr != groupRow);
						if (nullptr != groupRow)
						{
							const GridBackendField& fGroupGraphID = groupRow->GetField(GetColId());

							vector<GridBackendRow*> parentRows;
							parentRows.push_back(groupRow);

							vector<vector<GridBackendRow*>> addedRows;
							for (const auto& item : newItems)
							{
								if (addedRows.empty() || !addedRows.back().empty())
									addedRows.emplace_back();

								for (auto parentGroupRow : parentRows)
								{
									const GridBackendField& fGroupName = parentGroupRow->GetField(m_TemplateGroups.m_ColumnDisplayName);
									const GridBackendField& fGroupType = parentGroupRow->GetField(m_TemplateGroups.m_ColumnCombinedGroupType);

									// Recycle the row if already in.
									GridBackendRow* pURow = GetChildRow(parentGroupRow, item.GetID(), GetColId()->GetID());

									if (nullptr == pURow)
									{
										GridBackendField fieldId(item.GetID(), GetColId());

										// Meta ID is a multivalue with all the parents ids
										vector<PooledString> metaID;
										{
											auto* parent = parentGroupRow;
											while (nullptr != parent)
											{
												metaID.insert(metaID.begin(), parent->GetField(GetColId()).GetValueStr());
												parent = parent->GetParentRow();
											}
										}
										ASSERT(!metaID.empty());

										pURow = m_RowIndex.GetRow({ fieldId, m_TemplateUsers.GetMetaIDField(metaID) }, true, false);

										update = true;

										ASSERT(nullptr != pURow);
										if (nullptr != pURow)
										{
											pURow->SetParentRow(parentGroupRow);
											SetRowObjectType(pURow, item.GetObjectType());

											if (GridUtil::IsBusinessGroup(pURow))
											{
												for (auto c : m_MetaColumns)
												{
													if (parentGroupRow->HasField(c))
														pURow->AddField(parentGroupRow->GetField(c));
													else
														pURow->RemoveField(c);
												}

												if (nullptr != m_ColumnGroupNameForMember)
													pURow->AddField(item.GetPrincipalName(), m_ColumnGroupNameForMember);
												else
													pURow->AddField(item.GetPrincipalName(), m_TemplateGroups.m_ColumnDisplayName);

												if (nullptr != m_ColumnCombinedGroupTypeForMember)
													pURow->AddField(item.GetUserGroupType(), m_ColumnCombinedGroupTypeForMember);
												else if (!item.GetUserGroupType().IsEmpty())
													pURow->AddField(item.GetUserGroupType(), m_TemplateGroups.m_ColumnCombinedGroupType);

												if (nullptr != m_ColumnIsTeamForMember)
													GridUtil::SetRowIsTeam(item.GetGroupIsTeam(), pURow, m_ColumnIsTeamForMember, m_TemplateGroups.m_IconIsTeam);
												else
													GridUtil::SetRowIsTeam(item.GetGroupIsTeam(), pURow, m_TemplateGroups.m_ColumnIsTeam, m_TemplateGroups.m_IconIsTeam);
											}
											else if (GridUtil::IsBusinessUser(pURow))
											{
												for (auto c : m_MetaColumns)
												{
													if (parentGroupRow->HasField(c))
														pURow->AddField(parentGroupRow->GetField(c));
													else
														pURow->RemoveField(c);
												}

												pURow->AddField(item.GetPrincipalName(), m_TemplateUsers.m_ColumnUserPrincipalName);
												pURow->AddField(item.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
												if (nullptr != m_TemplateUsers.m_ColumnUserType)
													pURow->AddField(item.GetUserGroupType(), m_TemplateUsers.m_ColumnUserType);
											}
											else
											{
												ASSERT(false);
											}

											if (GridUtil::IsBusinessUser(pURow, false))
												setRowKind(pURow, EXTERNAL_OWNER);
											else if (GridUtil::IsBusinessUser(pURow, true))
												setRowKind(pURow, EXTERNAL_OWNER_GUEST);
											else
												ASSERT(false);

											setParamAddOwner(pURow);

											addedRows.back().push_back(pURow);
											update = true;
										}
									}
									else
									{
										if (pURow->IsDeleted())
										{
											if (isOwnerCandidate(pURow))
											{
												// Revert
												row_pk_t rowPk;
												GetRowPK(pURow, rowPk);
												if (GetModifications().Revert(rowPk, false))
												{
													pURow->SetLParam(0);
													if (upgradeMemberToOwner(pURow))
														update = true;
												}
											}
											else if (isExternalOwner(pURow))
											{
												// Revert
												row_pk_t rowPk;
												GetRowPK(pURow, rowPk);
												update = GetModifications().Revert(rowPk, false);
												pURow->SetLParam(0);
											}
											else if (isOwner(pURow))
											{
												// Revert
												row_pk_t rowPk;
												GetRowPK(pURow, rowPk);
												update = GetModifications().Revert(rowPk, false);
												pURow->SetLParam(0);
											}
											else
											{
												ASSERT(false);
											}
										}
										else if (isOwnerCandidate(pURow))
										{
											// Make owner
											if (upgradeMemberToOwner(pURow))
												update = true;
										}
									}
								}
							}

							if (nullptr == rowToScrollTo && !addedRows.empty() && !addedRows[0].empty())
							{
								OnUnSelectAll();
								rowToScrollTo = addedRows[0][0];
							}

							for (auto& rows : addedRows)
							{
								ASSERT(rows.size() <= 1);
								for (auto& row : rows)
								{
									row_pk_t rowPk;
									GetRowPK(row, rowPk);
									GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPk));
									SelectRowAndExpandParentGroups(row, false);
								}
							}
						}
					}

					if (update)
						UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

					if (nullptr != rowToScrollTo)
						ScrollToRow(rowToScrollTo);
				}
			}
		}
	}
}

void GridGroupsHierarchy::OnUpdateAddOwnersToGroups(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& IsFrameConnected()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& p_Row)
			{
				auto ref = nullptr != p_Row ? p_Row->GetTopAncestorOrThis() : nullptr;
				return nullptr != ref
					&& !ref->IsGroupRow()
					&& IsGroupAuthorizedByRoleDelegation(ref, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT);
			}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsHierarchy::OnUpgradeToOwner()
{
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE);
	auto canBeUpgraded = [this, hasOutOfScopePrivilege](auto& p_Row)
		{
			return nullptr != p_Row
				&& !p_Row->IsGroupRow()
				&& nullptr != p_Row->GetParentRow()
				&& !p_Row->IsDeleted()
				&& isOwnerCandidate(p_Row)
				&& IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis(), RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT)
				&& (hasOutOfScopePrivilege
					|| IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)
					|| IsGroupAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
		};

	auto selectedRows = GetBackend().GetSelectedRows();
	for (auto it = selectedRows.begin(); it != selectedRows.end();)
	{
		if (!canBeUpgraded(*it))
			it = selectedRows.erase(it);
		else
			++it;
	}

	bool update = false;
	for (auto& row : selectedRows)
		if (upgradeMemberToOwner(row))
			update = true;

	if (!selectedRows.empty())
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridGroupsHierarchy::OnUpdateUpgradeToOwner(CCmdUI* pCmdUI)
{
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE);

	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, hasOutOfScopePrivilege](auto& p_Row)
			{
				return nullptr != p_Row
					&& !p_Row->IsGroupRow()
					&& nullptr != p_Row->GetParentRow()
					&& !p_Row->IsDeleted()
					&& isOwnerCandidate(p_Row)
					&& IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis(), RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT)
					&& (hasOutOfScopePrivilege
						|| IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)
						|| IsGroupAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
			}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsHierarchy::OnDowngradeToMember()
{
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE);
	auto canBeDowngraded = [this, hasOutOfScopePrivilege](auto& p_Row)
	{
		return nullptr != p_Row
			&& !p_Row->IsGroupRow()
			&& nullptr != p_Row->GetParentRow()
			&& !p_Row->IsDeleted()
			&& isOwner(p_Row) && !isExternalOwner(p_Row)
			&& IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis(), RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT)
			&& (hasOutOfScopePrivilege
				|| IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)
				|| IsGroupAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
	};

	auto selectedRows = GetBackend().GetSelectedRows();
	for (auto it = selectedRows.begin(); it != selectedRows.end();)
	{
		if (!canBeDowngraded(*it))
			it = selectedRows.erase(it);
		else
			++it;
	}

	bool update = false;
	for (auto& row : selectedRows)
	{
		if (row->IsModified() && row->GetLParam() == BusinessObject::UPDATE_TYPE_ADDOWNERS)
		{
			// Revert
			row_pk_t rowPk;
			GetRowPK(row, rowPk);
			if (GetModifications().Revert(rowPk, false))
			{
				row->SetLParam(0);
				update = true;
			}
		}
		else
		{
			if (downgradeOwnerToMember(row))
				update = true;
		}
	}

	if (update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridGroupsHierarchy::OnUpdateDowngradeToMember(CCmdUI* pCmdUI)
{
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE);

	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, hasOutOfScopePrivilege](auto& p_Row)
			{
				return nullptr != p_Row
					&& !p_Row->IsGroupRow()
					&& nullptr != p_Row->GetParentRow()
					&& !p_Row->IsDeleted()
					&& isOwner(p_Row) && !isExternalOwner(p_Row)
					&& IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis(), RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT)
					&& (hasOutOfScopePrivilege
						|| IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)
						|| IsGroupAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
			}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsHierarchy::OnAddToMembers()
{
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE);
	auto canBeUpgraded = [this, hasOutOfScopePrivilege](auto& p_Row)
	{
		return nullptr != p_Row
			&& !p_Row->IsGroupRow()
			&& nullptr != p_Row->GetParentRow()
			&& !p_Row->IsDeleted()
			&& isExternalOwner(p_Row)
			&& IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis(), RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT)
			&& (hasOutOfScopePrivilege
				|| IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)
				|| IsGroupAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
	};

	auto selectedRows = GetBackend().GetSelectedRows();
	for (auto it = selectedRows.begin(); it != selectedRows.end();)
	{
		if (!canBeUpgraded(*it))
			it = selectedRows.erase(it);
		else
			++it;
	}

	bool update = false;
	for (auto& row : selectedRows)
		if (upgradeExternalToRegularOwner(row))
			update = true;

	if (update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridGroupsHierarchy::OnUpdateAddToMembers(CCmdUI* pCmdUI)
{
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE);

	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, hasOutOfScopePrivilege](auto& p_Row)
			{
				return nullptr != p_Row
					&& !p_Row->IsGroupRow()
					&& nullptr != p_Row->GetParentRow()
					&& !p_Row->IsDeleted()
					&& isExternalOwner(p_Row)
					&& IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis(), RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT)
					&& (hasOutOfScopePrivilege
						|| IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)
						|| IsGroupAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
			}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsHierarchy::OnCopySelectionToGroups()
{
	if (IsFrameConnected())
	{
		// FIXME: handle roles if not done in copySelectedMembersToGroups
		copySelectedMembersToGroups(false);
	}
}

void GridGroupsHierarchy::OnUpdateCopySelectionToGroups(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT) && HasOneNonGroupRowSelectedAtLeast());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsHierarchy::OnMoveSelectionToGroups()
{
	if (IsFrameConnected())
	{
		// FIXME: handle roles if not done in copySelectedMembersToGroups
		copySelectedMembersToGroups(true);
	}
}

void GridGroupsHierarchy::OnUpdateMoveSelectionToGroups(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& IsFrameConnected()
		&& IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT)
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& p_Row)
			{
				return nullptr != p_Row
					&& !p_Row->IsGroupRow()
					&& (isDirectMember(p_Row) || (p_Row->GetHierarchyLevel() == 0 && p_Row->HasChildrenRows()))
					&& IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis(), RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT);
			}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

#if MEMBERS_GRID_HIDE_NESTED
void GridGroupsHierarchy::OnShowNested()
{
	SetShowNested(!m_ShowNestedGroupMembers);
	refreshNestedGroupsRowDisplay(true);
}

void GridGroupsHierarchy::OnUpdateShowNested(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && m_HasNestedGroupsMembers);
	pCmdUI->SetCheck(m_ShowNestedGroupMembers);
	setTextFromProfUISCommand(*pCmdUI);
}
#endif

void GridGroupsHierarchy::SetShowNested(const bool p_ShowNested)
{
	m_ShowNestedGroupMembers = p_ShowNested;
}

bool GridGroupsHierarchy::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	if (m_TemplateUsers.GetSnapshotValue(p_Value, p_Field, p_Column))
		return true;

	if (m_TemplateGroups.GetSnapshotValue(p_Value, p_Field, p_Column))
		return true;

	if (p_Field.HasValue())
	{
		if (&p_Column == m_ColumnMemberKind)
		{
			const auto icon = p_Field.GetIcon();
			const auto it = std::find_if(m_KindInfos.begin(), m_KindInfos.end(),
				[icon](const auto& item)
				{
					return icon == item.second.first;
				});

			ASSERT(m_KindInfos.end() != it);
			if (m_KindInfos.end() != it)
			{
				p_Value = std::to_wstring(it->first);
				return true;
			}
		}
	}

	return GridGroupsCommon::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridGroupsHierarchy::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (m_TemplateUsers.LoadSnapshotValue(p_Value, p_Row, p_Column))
		return true;

	if (m_TemplateGroups.LoadSnapshotValue(p_Value, p_Row, p_Column))
		return true;

	if (m_ColumnMemberKind == &p_Column)
	{
		const auto val = Str::getIntFromString(p_Value);
		ASSERT(0 <= val && val < numKinds);
		if (0 <= val && val < numKinds)
		{
			setRowKind(&p_Row, (Kind)val);
			return true;
		}
	}

	return GridGroupsCommon::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

void GridGroupsHierarchy::AddGroups(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge)
{
#if MEMBERS_GRID_HIDE_NESTED
	m_HasNestedGroupsMembers = false;
#endif

	{
		const uint32_t options = GridUpdaterOptions::UPDATE_GRID
			| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
			| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			/*| GridUpdaterOptions::REFRESH_PROCESS_DATA*/
			/*| GridUpdaterOptions::PROCESS_LOADMORE*/;

		GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateOperations);
		GridIgnoreModification ignoramus(*this);

		bool oneCanceled = false;
		bool oneOK = false;
		for (const auto& group : p_Groups)
		{
			ASSERT(m_BufferHierarchyGroupIDs.empty());
			ASSERT(m_BufferHierarchyGroupNames.empty());

			if (m_HasLoadMoreAdditionalInfo)
			{
				if (group.IsMoreLoaded())
					m_MetaIDsMoreLoaded.insert(group.GetID());
				else
					m_MetaIDsMoreLoaded.erase(group.GetID());
			}

			auto row = addRowHierarchy(group, nullptr, vector<PooledString>{}, updater, true);
			ASSERT(nullptr != row);
			if (nullptr != row)
			{
				updater.GetOptions().AddRowWithRefreshedValues(row);
				if (group.HasFlag(BusinessObject::CANCELED))
				{
					oneCanceled = true;
				}
				else
				{
					oneOK = true;
					updater.GetOptions().AddPartialPurgeParentRow(row);
				}

				AddRoleDelegationFlag(row, group);
			}
		}

		if (oneCanceled)
		{
			if (oneOK)
				updater.GetOptions().SetPurgeFlag(GridUpdaterOptions::PARTIALPURGE);
			else
				updater.GetOptions().SetPurgeFlag(0); // Everything canceled, don't purge anything.
		}
	}

#if MEMBERS_GRID_HIDE_NESTED
	if (!p_FullPurge)
		refreshNestedGroupsRowDisplay(false); // Will also call refreshNestedGroupsColumnDisplay()
	else
		refreshNestedGroupsColumnDisplay(false);
#endif

	if (m_PendingAddMembers)
	{
		ASSERT(p_Groups.size() == m_PendingAddMembers->m_Groups.size());

		auto pendingAdd = *m_PendingAddMembers;
		m_PendingAddMembers.reset();
		addMembersToGroupsImpl(pendingAdd.m_Groups, pendingAdd.m_MembersToAddTo, pendingAdd.m_ShouldRemoveFromSource, pendingAdd.m_ShouldScroll);
		ASSERT(!m_PendingAddMembers);
	}
}

GridBackendRow* GridGroupsHierarchy::addRowHierarchy(const BusinessGroup& p_Group, GridBackendRow* p_ParentRow, vector<PooledString>& p_ParentIds, GridUpdater& p_Updater, bool p_KeepExistingIfGroupCanceled)
{
	vector<GridBackendField> BGK;
	BGK.emplace_back(p_Group.GetID(), GetColId());
	if (nullptr != m_TemplateUsers.m_ColumnMetaID)
	{
		auto metaField = m_TemplateUsers.GetMetaIDField(m_BufferHierarchyGroupIDs);
		if (metaField.HasValue())
			BGK.push_back(metaField);
		else
			BGK.emplace_back(/*p_Group.GetID()*/PooledString::g_EmptyString, m_TemplateUsers.m_ColumnMetaID);
	}

	GridBackendRow* row = m_TemplateGroups.AddRow(*this, p_Group, BGK, p_Updater, false, nullptr, {}, p_KeepExistingIfGroupCanceled);
	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		row->AddField(row->HasField(m_TemplateGroups.m_ColumnSkuIds), m_ColumnHasLicensesAssigned);

		const bool topLevelGroup = nullptr == p_ParentRow;
		p_Updater.GetOptions().AddRowWithRefreshedValues(row);

		row->SetHierarchyParentToHide(true);
		row->SetParentRow(p_ParentRow);
		addCommonFields(row);
		m_BufferHierarchyGroupIDs.push_back(p_Group.GetID());
		ASSERT(p_Group.GetDisplayName());
		if (p_Group.GetDisplayName())
			m_BufferHierarchyGroupNames.push_back(*p_Group.GetDisplayName());
		else
			m_BufferHierarchyGroupNames.push_back(PooledString());

		if (topLevelGroup)
		{
		}
		else if (0 == p_ParentRow->GetHierarchyLevel()) // Parent is top level
		{
			setRowKind(row, MEMBER_GROUP);
		}
		else
		{
			setRowKind(row, INDIRECT_MEMBER_GROUP);
		}

		if (p_Group.HasFlag(BusinessObject::CANCELED))
		{
			// Could be uncommented if we notice missing explosion refresh on canceled rows
			//auto children = GetChildRows(row);
			//for (auto child : children)
			//	p_Updater.GetOptions().AddRowWithRefreshedValues(child);
		}
		else
		{
			// Clear previous error
			if (row->IsError())
				row->ClearStatus();

			vector<GridBackendField> BGK{ m_TemplateUsers.GetMetaIDField(m_BufferHierarchyGroupIDs) };
			for (const auto& childUserID : p_Group.GetChildrenUsers())
			{
				auto childUser = BusinessUser(GetGraphCache().GetUserInCache(childUserID, false));
				ASSERT(childUser.GetID() == childUserID);
				GridBackendRow* childRow = m_TemplateUsers.AddRow(*this, childUser, BGK, p_Updater, false, false);
				ASSERT(nullptr != childRow);
				if (nullptr != childRow)
				{
					p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);

					childRow->SetParentRow(row);

					addCommonFields(childRow);
					if (GridUtil::IsBusinessUser(childRow, false))
					{
						if (topLevelGroup)
							setRowKind(childRow, MEMBER_USER);
						else
							setRowKind(childRow, INDIRECT_MEMBER_USER);
					}
					else if (GridUtil::IsBusinessUser(childRow, true))
					{
						if (topLevelGroup)
							setRowKind(childRow, MEMBER_GUEST);
						else
							setRowKind(childRow, INDIRECT_MEMBER_GUEST);
					}
					else
					{
						ASSERT(false);
					}
					AddRoleDelegationFlag(childRow, childUser);

#if MEMBERS_GRID_HIDE_NESTED
					if (childRow->GetHierarchyLevel() == 2)
					{
						m_HasNestedGroupsMembers = true;
						childRow->SetTechHidden(!m_ShowNestedGroupMembers);
					}
#endif
				}
			}
				
			for (const auto& childUserID : p_Group.GetOwnerUsers())
			{
				ASSERT(topLevelGroup);
				auto childUser = BusinessUser(GetGraphCache().GetUserInCache(childUserID, false));
				ASSERT(childUser.GetID() == childUserID);

				GridBackendRow* childRow = nullptr;
				const auto& users = p_Group.GetChildrenUsers();
				if (users.end() == std::find(users.begin(), users.end(), childUserID)) // Not a member == External Owner
				{
					childRow = m_TemplateUsers.AddRow(*this, childUser, BGK, p_Updater, false, false);
					ASSERT(nullptr != childRow);
					if (nullptr != childRow)
					{
						if (GridUtil::IsBusinessUser(childRow, false))
							setRowKind(childRow, EXTERNAL_OWNER);
						else if (GridUtil::IsBusinessUser(childRow, true))
							setRowKind(childRow, EXTERNAL_OWNER_GUEST);
						else
							ASSERT(false);
					}
				}
				else
				{
					childRow = GetChildRow(row, childUser.GetID(), GetColId()->GetID());
					ASSERT(nullptr != childRow);
					if (nullptr != childRow)
					{
						if (GridUtil::IsBusinessUser(childRow, false))
							setRowKind(childRow, OWNER_MEMBER_USER);
						else if (GridUtil::IsBusinessUser(childRow, true))
							setRowKind(childRow, OWNER_MEMBER_GUEST);
						else
							ASSERT(false);
					}
				}

				ASSERT(nullptr != childRow);
				if (nullptr != childRow)
				{
					p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);

					childRow->SetParentRow(row);

					addCommonFields(childRow);

					AddRoleDelegationFlag(childRow, childUser);
				}

#if MEMBERS_GRID_HIDE_NESTED
				if (childRow->GetHierarchyLevel() == 2)
				{
					m_HasNestedGroupsMembers = true;
					childRow->SetTechHidden(!m_ShowNestedGroupMembers);
				}
#endif
			}

			for (const auto& childOrgContactId : p_Group.GetChildrenOrgContacts())
			{
				auto childOrgContact = BusinessOrgContact(GetGraphCache().GetCachedOrgContact(childOrgContactId));
				ASSERT(childOrgContact.GetID() == childOrgContactId);
				auto contactPK = BGK;
				contactPK.emplace_back(childOrgContact.GetID(), GetColId());

				auto childRow = GetRowIndex().GetRow(contactPK, true, (p_Updater.GetOptions().IsFullPurge() || p_Updater.GetOptions().IsPartialPurge()));
				ASSERT(nullptr != childRow);
				if (nullptr != childRow)
				{
					SetRowObjectType(childRow, childOrgContact, childOrgContact.HasFlag(BusinessObject::CANCELED));
					childRow->AddField(childOrgContact.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
					childRow->AddField(childOrgContact.GetMail(), m_TemplateUsers.m_ColumnUserPrincipalName);

					p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);

					childRow->SetParentRow(row);

					addCommonFields(childRow);
					if (topLevelGroup)
						setRowKind(childRow, MEMBER_ORGCONTACT);
					else
						setRowKind(childRow, INDIRECT_MEMBER_ORGCONTACT);
				}

#if MEMBERS_GRID_HIDE_NESTED
				if (childRow->GetHierarchyLevel() == 2)
				{
					m_HasNestedGroupsMembers = true;
					childRow->SetTechHidden(!m_ShowNestedGroupMembers);
				}
#endif
			}

			for (const auto& childGroupId : p_Group.GetChildrenGroups())
			{
				auto childGroup = BusinessGroup(GetGraphCache().GetGroupInCache(childGroupId));
				ASSERT(childGroup.GetID() == childGroupId);

				bool selfNested = false;
				if (p_Group.GetID() == childGroupId)
				{
					childGroup.SetError(SapioError(YtriaTranslate::DoError(GridGroupsHierarchy_addRowHierarchy_1, _YLOC("Self-Nested Group"),_YR("Y2451")).c_str(), 0));
					selfNested = true;
				}
				else if (p_ParentIds.end() != std::find(p_ParentIds.begin(), p_ParentIds.end(), childGroupId))
				{
					selfNested = true;
					childGroup.SetError(SapioError(YtriaTranslate::DoError(GridGroupsHierarchy_addRowHierarchy_2, _YLOC("Self-Nested Group (Indirect)"),_YR("Y2452")).c_str(), 0));
				}
				else
				{
					GetGraphCache().GetGroupMembers(childGroup);
				}

				p_ParentIds.push_back(p_Group.GetID());
				GridBackendRow* childRow = addRowHierarchy(childGroup, row, p_ParentIds, p_Updater, false);
				ASSERT(p_ParentIds.back() == p_Group.GetID());
				p_ParentIds.pop_back();

				ASSERT(nullptr != childRow);
				if (nullptr != childRow)
				{
					p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);

					AddRoleDelegationFlag(childRow, childGroup);
				}

#if MEMBERS_GRID_HIDE_NESTED
				if (childRow->GetHierarchyLevel() == 2)
				{
					m_HasNestedGroupsMembers = true;
					childRow->SetTechHidden(!m_ShowNestedGroupMembers);
				}
#endif
			}

			GridUtil::HandleMultiError(row
				,	{ { p_Group.GetChildrenError(), _YFORMAT(L"Members: %s", _YTEXT("%s")) }
					, { p_Group.GetOwnersError(), _YFORMAT(L"Owners: %s", _YTEXT("%s")) }
					}
				,	p_Updater);
		}

#if MEMBERS_GRID_HIDE_NESTED
		if (row->GetHierarchyLevel() == 2)
		{
			m_HasNestedGroupsMembers = true;
			row->SetTechHidden(!m_ShowNestedGroupMembers);
		}
#endif
	}

	m_BufferHierarchyGroupIDs.pop_back();
	m_BufferHierarchyGroupNames.pop_back();

	return row;
}

void GridGroupsHierarchy::copySelectedMembersToGroups(bool removeFromSourceGroup)
{
	// FIXME: Handle role delegation here.
	// If removeFromSourceGroup is true, check both destination AND source.

	if (IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT))
	{
		// <Child Id, <Child name, Parent ids>>
		std::map<PooledString, std::pair<PooledString, std::set<PooledString>>> parentsForSelection;
		set<GridBackendRow*> contextMemberRows;

		const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE);

		const auto addContextUser = [this, &contextMemberRows, &parentsForSelection, removeFromSourceGroup, hasOutOfScopePrivilege](GridBackendRow* p_Row)
		{
			ASSERT(GridUtil::IsBusinessUser(p_Row) || GridUtil::IsBusinessOrgContact(p_Row));
			if ((GridUtil::IsBusinessUser(p_Row) || GridUtil::IsBusinessOrgContact(p_Row))
				&&
				(
					hasOutOfScopePrivilege
				||	IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)
				||	GridUtil::IsBusinessOrgContact(p_Row))
				)
			{
				auto parentRow = p_Row->GetParentRow();
				ASSERT(GridUtil::IsBusinessGroup(parentRow));
				if (!removeFromSourceGroup || IsAuthorizedByRoleDelegation(parentRow, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT))
				{
					const PooledString id = p_Row->GetField(GetColId()).GetValueStr();
					auto it = std::find_if(contextMemberRows.begin(), contextMemberRows.end(), [&id, this](GridBackendRow* row) { return id == row->GetField(GetColId()).GetValueStr(); });
					if (contextMemberRows.end() == it && contextMemberRows.insert(p_Row).second)
					{
						std::set<PooledString> groupIds;
						vector<GridBackendRow*> siblings;
						GetRowsByCriteria(siblings, [&id, this](GridBackendRow* row) { return id == row->GetField(GetColId()).GetValueStr(); });
						ASSERT(!siblings.empty()); // At least 'p_Row'

						for (auto& sibling : siblings)
						{
							ASSERT(GridUtil::IsBusinessUser(sibling) || GridUtil::IsBusinessOrgContact(sibling));
							if (GridUtil::IsBusinessUser(sibling) || GridUtil::IsBusinessOrgContact(sibling))
							{
								ASSERT(GridUtil::IsBusinessGroup(sibling->GetParentRow()));
								groupIds.insert(sibling->GetParentRow()->GetField(GetColId()).GetValueStr());
							}
						}

						parentsForSelection[id] = { p_Row->GetField(m_TemplateUsers.m_ColumnDisplayName).GetValueStr(), groupIds };
					}
				}
			}
		};

		const auto addContextGroup = [this, &contextMemberRows, &parentsForSelection, removeFromSourceGroup, hasOutOfScopePrivilege](GridBackendRow* p_Row)
		{
			ASSERT(GridUtil::IsBusinessGroup(p_Row));
			if (hasOutOfScopePrivilege
				|| IsGroupAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE))
			{
				auto parentRow = p_Row->GetParentRow();
				ASSERT(nullptr == parentRow || GridUtil::IsBusinessGroup(parentRow));
				if (!removeFromSourceGroup || nullptr != parentRow && IsAuthorizedByRoleDelegation(parentRow, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT))
				{
					const PooledString id = p_Row->GetField(GetColId()).GetValueStr();
					auto it = std::find_if(contextMemberRows.begin(), contextMemberRows.end(), [&id, this](GridBackendRow* row) { return id == row->GetField(GetColId()).GetValueStr(); });
					if (contextMemberRows.end() == it && contextMemberRows.insert(p_Row).second)
					{
						std::set<PooledString> parentGroupIds;
						vector<GridBackendRow*> siblings;
						GetRowsByCriteria(siblings, [&id, this](GridBackendRow* row) { return id == row->GetField(GetColId()).GetValueStr(); });
						ASSERT(!siblings.empty()); // At least 'p_Row'

						for (auto& sibling : siblings)
						{
							ASSERT(GridUtil::IsBusinessGroup(sibling));
							if (GridUtil::IsBusinessGroup(sibling))
							{
								if (nullptr != sibling->GetParentRow())
								{
									ASSERT(GridUtil::IsBusinessGroup(sibling->GetParentRow()));
									parentGroupIds.insert(sibling->GetParentRow()->GetField(GetColId()).GetValueStr());
								}
							}
						}

						parentsForSelection[id] = { p_Row->GetField(m_TemplateGroups.m_ColumnDisplayName).GetValueStr(), parentGroupIds };
					}
				}
			}
		};

		set<GridBackendRow*> selectedGroupRows;
		bool onlyTopLevelGroups = true;
		bool onlyEmptyGroups = true;
		for (auto row : GetSelectedRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row && !row->IsGroupRow() && (!removeFromSourceGroup || isDirectMember(row) || row->GetHierarchyLevel() == 0))
			{
				if (GridUtil::IsBusinessUser(row) || GridUtil::IsBusinessOrgContact(row))
				{
					addContextUser(row);
				}
				else if (GridUtil::IsBusinessGroup(row))
				{
					onlyTopLevelGroups = onlyTopLevelGroups && row->GetHierarchyLevel() == 0;
					onlyEmptyGroups = onlyEmptyGroups && !row->HasChildrenRowsVisible();
					selectedGroupRows.insert(row);
				}
			}
		}

		onlyTopLevelGroups = onlyTopLevelGroups && !selectedGroupRows.empty();

		if (!selectedGroupRows.empty())
		{
			bool processSelectedGroupMembers = false;

			if (!onlyEmptyGroups)
			{
				std::unordered_map<UINT, wstring> buttons;

				if (removeFromSourceGroup && onlyTopLevelGroups)
				{
					buttons = {
						{ IDYES, YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_8, _YLOC("Move Members")).c_str() },
						{ IDCANCEL, YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_10, _YLOC("Cancel")).c_str() }
					};
				}
				else
				{
					buttons = {
						{ IDOK,
							removeFromSourceGroup ? YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_6, _YLOC("Move Groups")).c_str()
												: YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_7, _YLOC("Add Groups")).c_str()
						},
						{ IDYES,
							removeFromSourceGroup ? YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_8, _YLOC("Move Members")).c_str()
												: YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_9, _YLOC("Add Members")).c_str()
						},
						{ IDCANCEL, YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_10, _YLOC("Cancel")).c_str() }
					};
				}


				const auto ret = YCodeJockMessageBox(this,
					DlgMessageBox::eIcon_Question,
					removeFromSourceGroup ? YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_1, _YLOC("Move To")).c_str()
					: YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_2, _YLOC("Add To")).c_str(),
					removeFromSourceGroup ? YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_3, _YLOC("You have selected group members as well as groups. Do you want to include these groups, or only their members?")).c_str()
					: YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_4, _YLOC("You have selected group members as well as groups. Do you want to include these groups, or only their members?")).c_str(),
					removeFromSourceGroup ? YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_5, _YLOC("[Note: Top-level groups cannot be moved - only copied.]")).c_str()
					: _YTEXT(""),
					buttons
				).DoModal();

				// ACHTUNG automation depends on the IDs of the buttons: cf YCodeJockMessageBox::DoModal and Office365AdminApp::TranslateActionSpecific - if they need to change, you must update Office365AdminApp::TranslateActionSpecific

				switch (ret)
				{
				case IDOK:
					processSelectedGroupMembers = false;
					break;
				case IDYES:
					processSelectedGroupMembers = true;
					break;
				case IDCANCEL:
					return;
				default:
					ASSERT(false);
					break;
				}
			}

			if (processSelectedGroupMembers)
			{
				CWaitCursor _;
				for (auto row : GetBackendRows())
				{
					if (nullptr != row->GetParentRow() && 1 == selectedGroupRows.erase(row->GetParentRow()))
					{
						if (GridUtil::IsBusinessGroup(row))
							addContextGroup(row);
						else
							addContextUser(row);
					}

					if (selectedGroupRows.empty())
						break;
				}
			}
			else
			{
				CWaitCursor _;
				for (auto row : selectedGroupRows)
					addContextGroup(row);
			}
		}

		ASSERT(parentsForSelection.size() == contextMemberRows.size());
		ASSERT(!contextMemberRows.empty());
		if (!contextMemberRows.empty())
		{
			vector<BusinessUser>		users;
			vector<BusinessGroup>		groups;
			vector<BusinessOrgContact>	orgContacts;
			getUsersGroupsAndOrgContacts(users, false, groups, true, orgContacts, false);

			wstring title = removeFromSourceGroup
				? YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_11, _YLOC("Move %1 selected members"), Str::getStringFromNumber(contextMemberRows.size()).c_str())
				: YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_12, _YLOC("Add %1 selected members"), Str::getStringFromNumber(contextMemberRows.size()).c_str());

			wstring explanation = removeFromSourceGroup
				? YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_13, _YLOC("These %1 members will be moved to the groups you select below.\nTo see the complete list, use Load full directory.\nNote: Unlike �Add To�, this will also remove these selected members from their currently assigned group."), Str::getStringFromNumber(contextMemberRows.size()).c_str())
				: YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_14, _YLOC("These %1 members will be added to the groups you select below.\nTo see the complete list, use Load full directory."), Str::getStringFromNumber(contextMemberRows.size()).c_str());

			// if removeFromSourceGroup , should we restrict dialog to single row selection?
			DlgAddItemsToGroups dlg( // DONE
				GetSession(),
				parentsForSelection,
				DlgAddItemsToGroups::SELECT_GROUPS_FOR_MEMBERS,
				{},
				groups,
				{},
				title,
				explanation,
				RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT,
				this);
			if (IDOK == dlg.DoModal())
			{
				GridBackendRow* rowToScrollTo = nullptr;
				bool update = false;

				if (nullptr != GetColId())
				{
					// List of all new groups to add to selected members to.
					auto& newGroups = dlg.GetSelectedItems();
					OnUnSelectAll();
					addMembersToGroupsImpl(newGroups, contextMemberRows, removeFromSourceGroup, true);
				}
			}
		}

		if (m_PendingAddMembers)
		{
			ASSERT(nullptr != m_Frame);
			if (nullptr != m_Frame)
				m_Frame->AddToSelection(m_PendingAddMembers->m_Groups);
			else
				m_PendingAddMembers.reset();
		}
	}
}

void GridGroupsHierarchy::addMembersToGroupsImpl(const vector<SelectedItem>& p_NewGroups, const std::set<GridBackendRow*>& p_Rows, bool removeFromSourceGroup, bool shouldScrollToFirstAddedRow)
{
	ASSERT(!m_PendingAddMembers);

	GridBackendRow* rowToScrollTo = nullptr;
	bool update = false;
	set<GridBackendRow*> contextMemberRowsToRemove;
	set<GridBackendRow*> contextOwnerRowsToMakeExternal;

	GridUpdater updater(*this, GridUpdaterOptions(/*GridUpdaterOptions::UPDATE_GRID | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES | */GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));

	for (auto contextMemberRow : p_Rows)
	{
		ASSERT(nullptr != contextMemberRow);
		if (nullptr != contextMemberRow)
		{
			bool hasBeenAdded = false;

			const bool isUser = GridUtil::IsBusinessUser(contextMemberRow);
			const bool isOrgContact = GridUtil::IsBusinessOrgContact(contextMemberRow);
			const GridBackendField& memberFieldID = contextMemberRow->GetField(GetColId());

			vector<GridBackendRow*> addedRows;
			for (const auto& parentGroup : p_NewGroups)
			{
				if (m_PendingAddMembers && m_PendingAddMembers->m_Groups.end() != std::find(m_PendingAddMembers->m_Groups.begin(), m_PendingAddMembers->m_Groups.end(), parentGroup))
				{
					m_PendingAddMembers->m_MembersToAddTo.insert(contextMemberRow);
					continue;
				}

				auto parentRows = GetRows(parentGroup.GetID(), GetColId()->GetID());

				if (parentRows.end() == std::find_if(parentRows.begin(), parentRows.end(), [](GridBackendRow* p_Row) {return nullptr != p_Row && 0 == p_Row->GetHierarchyLevel(); }))
				{
					// We need to add it to selection before proceeding.
					if (!m_PendingAddMembers)
						m_PendingAddMembers.emplace();

					m_PendingAddMembers->m_Groups.push_back(parentGroup);
					m_PendingAddMembers->m_MembersToAddTo.insert(contextMemberRow);
					continue;
				}

				for (auto parentRow : parentRows)
				{
					// Recycle the row if already in.
					GridBackendRow* existingMemberRow = GetChildRow(parentRow, memberFieldID.GetValueStr(), GetColId()->GetID());

					if (nullptr == existingMemberRow)
					{
						// Meta ID is a multivalue with all the parents ids
						vector<PooledString> metaID;
						{
							auto* parent = parentRow;
							while (nullptr != parent)
							{
								metaID.insert(metaID.begin(), parent->GetField(GetColId()).GetValueStr());
								parent = parent->GetParentRow();
							}
						}
						ASSERT(!metaID.empty());

						vector<GridBackendField> memberPK{ memberFieldID, GridBackendField(metaID, m_TemplateUsers.m_ColumnMetaID->GetID()) };
						rttr::type::type_id typeId;
						BusinessUser bu;
						BusinessOrgContact boc;
						BusinessGroup bg;
						if (isUser)
						{
							// use cache so that we can have the real "group types" value.
							bu = BusinessUser(GetGraphCache().GetUserInCache(memberFieldID.GetValueStr(), false));
							ASSERT(bu.GetID() == memberFieldID.GetValueStr());
							typeId = bu.get_type().get_id();
						}
						else if (isOrgContact)
						{
							// use cache so that we can have the real "group types" value.
							boc = BusinessOrgContact(GetGraphCache().GetCachedOrgContact(memberFieldID.GetValueStr()));
							ASSERT(boc.GetID() == memberFieldID.GetValueStr());
							typeId = boc.get_type().get_id();
						}
						else
						{
							// use cache so that we can have the real "group types" value.
							bg = BusinessGroup(GetGraphCache().GetGroupInCache(memberFieldID.GetValueStr()));
							ASSERT(bg.GetID() == memberFieldID.GetValueStr());
							typeId = bg.get_type().get_id();
						}

						GridBackendRow* newMemberRow = GetRowIndex().GetRow(memberPK, true, false);
						ASSERT(nullptr != newMemberRow);
						if (nullptr != newMemberRow)
						{
							update = true;

							SetRowObjectType(newMemberRow, typeId);

							for (auto c : m_MetaColumns)
							{
								if (parentRow->HasField(c))
									newMemberRow->AddField(parentRow->GetField(c));
								else
									newMemberRow->RemoveField(c);
							}

							if (isUser)
							{
								ASSERT(GridUtil::IsBusinessUser(newMemberRow));
								newMemberRow->AddField(bu.GetUserPrincipalName(), m_TemplateUsers.m_ColumnUserPrincipalName);
								newMemberRow->AddField(bu.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
								if (nullptr != m_TemplateUsers.m_ColumnUserType)
									newMemberRow->AddField(bu.GetUserType(), m_TemplateUsers.m_ColumnUserType);
							}
							else if (isOrgContact)
							{
								ASSERT(GridUtil::IsBusinessOrgContact(newMemberRow));
								newMemberRow->AddField(boc.GetMail(), m_TemplateUsers.m_ColumnUserPrincipalName);
								newMemberRow->AddField(boc.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
							}
							else
							{
								ASSERT(GridUtil::IsBusinessGroup(newMemberRow));
								newMemberRow->AddField(bg.GetCombinedGroupType(), m_ColumnCombinedGroupTypeForMember);
								newMemberRow->AddField(bg.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
								GridUtil::SetRowIsTeam(bg.GetValue(_YUID(O365_GROUP_ISTEAM)), newMemberRow, m_ColumnIsTeamForMember, m_TemplateGroups.m_IconIsTeam);
							}

							newMemberRow->SetParentRow(parentRow);

							setParamAddMember(newMemberRow);
							setMemberRowKind(newMemberRow);

							addedRows.push_back(newMemberRow);
							hasBeenAdded = true;
						}
					}
					else
					{
						if (existingMemberRow->IsDeleted())
						{
							row_pk_t rowPk;
							GetRowPK(existingMemberRow, rowPk);
							if (GetModifications().Revert(rowPk, false))
							{
								existingMemberRow->SetLParam(0);
								update = true;
							}
						}
						hasBeenAdded = false;
					}
				}
			}

			if (nullptr == rowToScrollTo && addedRows.size() > 0)
				rowToScrollTo = addedRows[0];

			for (auto row : addedRows)
			{
				row_pk_t rowPk;
				GetRowPK(row, rowPk);
				GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPk));
				SelectRowAndExpandParentGroups(row, false);
			}

			if (hasBeenAdded && removeFromSourceGroup)
			{
				if (isOwner(contextMemberRow))
					contextOwnerRowsToMakeExternal.insert(contextMemberRow);
				else
					contextMemberRowsToRemove.insert(contextMemberRow);
			}
		}
	}

	if (removeFromSourceGroup)
	{
		if (!contextMemberRowsToRemove.empty())
			removeItemsFromGroup(BusinessObject::UPDATE_TYPE_DELETEMEMBERS, contextMemberRowsToRemove, !update && !m_PendingAddMembers, true);

		if (!contextOwnerRowsToMakeExternal.empty())
		{
			for (auto row : contextOwnerRowsToMakeExternal)
				changeRegularToExternalOwner(row);
		}
	}

	if (update && !m_PendingAddMembers)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

	if (nullptr != rowToScrollTo)
	{
		if (shouldScrollToFirstAddedRow)
			ScrollToRow(rowToScrollTo);
	}
	else
	{
		if (m_PendingAddMembers)
		{
			m_PendingAddMembers->m_ShouldScroll = true;
			m_PendingAddMembers->m_ShouldRemoveFromSource = removeFromSourceGroup;
		}
	}
}

void GridGroupsHierarchy::addCommonFields(GridBackendRow* p_ChildRow)
{
	auto parentRow = p_ChildRow->GetParentRow();
	ASSERT(nullptr == parentRow || GridUtil::IsBusinessGroup(parentRow));

	// Move some Group information to right columns
	if (GridUtil::IsBusinessGroup(p_ChildRow))
	{
		if (nullptr != parentRow) // Not top level group
		{
			p_ChildRow->AddField(p_ChildRow->GetField(m_TemplateGroups.m_ColumnDisplayName).GetValueStr(), m_TemplateUsers.m_ColumnDisplayName);
			p_ChildRow->RemoveField(m_TemplateGroups.m_ColumnDisplayName);
			p_ChildRow->AddField(p_ChildRow->GetField(m_TemplateGroups.m_ColumnCombinedGroupType).GetValueStr(), m_ColumnCombinedGroupTypeForMember);
			p_ChildRow->RemoveField(m_TemplateGroups.m_ColumnCombinedGroupType);

			if (p_ChildRow->HasField(m_TemplateGroups.m_ColumnIsTeam))
				p_ChildRow->AddField(p_ChildRow->GetField(m_TemplateGroups.m_ColumnIsTeam), m_ColumnIsTeamForMember->GetID());
			else
				p_ChildRow->RemoveField(m_ColumnIsTeamForMember);
			p_ChildRow->RemoveField(m_TemplateGroups.m_ColumnIsTeam);
		}
	}
	else
	{
		ASSERT(nullptr != parentRow && (GridUtil::IsBusinessUser(p_ChildRow) || GridUtil::IsBusinessOrgContact(p_ChildRow)));
	}

	addHierarchyPathField(p_ChildRow);

	if (nullptr != parentRow)
	{
		for (auto c : m_MetaColumns)
		{
			if (parentRow->HasField(c))
				p_ChildRow->AddField(parentRow->GetField(c));
			else
				p_ChildRow->RemoveField(c);
		}

		if (parentRow->HasField(m_TemplateGroups.m_ColumnLastRequestDateTime))
			p_ChildRow->AddField(parentRow->GetField(m_TemplateGroups.m_ColumnLastRequestDateTime));
		else
			p_ChildRow->RemoveField(m_TemplateGroups.m_ColumnLastRequestDateTime);
	}
}

void GridGroupsHierarchy::setRowKind(GridBackendRow* p_Row, Kind p_Kind)
{
	if (nullptr != m_ColumnMemberKind)
		p_Row->AddField(m_KindInfos[p_Kind].second, m_ColumnMemberKind, m_KindInfos[p_Kind].first);
}

void GridGroupsHierarchy::setMemberRowKind(GridBackendRow* p_pRow)
{
	ASSERT(nullptr != p_pRow->GetParentRow());
	if (nullptr != p_pRow->GetParentRow())
	{
		if (p_pRow->GetParentRow()->GetHierarchyLevel() == 0)
			setDirectMemberRowKind(p_pRow);
		else
			setIndirectMemberRowKind(p_pRow);
	}
}

void GridGroupsHierarchy::setDirectMemberRowKind(GridBackendRow* p_ChildRow)
{
	if (GridUtil::IsBusinessUser(p_ChildRow, false))
		setRowKind(p_ChildRow, MEMBER_USER);
	else if (GridUtil::IsBusinessUser(p_ChildRow, true))
		setRowKind(p_ChildRow, MEMBER_GUEST);
	else if (GridUtil::IsBusinessGroup(p_ChildRow))
		setRowKind(p_ChildRow, MEMBER_GROUP);
	else if (GridUtil::IsBusinessOrgContact(p_ChildRow))
		setRowKind(p_ChildRow, MEMBER_ORGCONTACT);
	else
		ASSERT(false);
}

void GridGroupsHierarchy::setIndirectMemberRowKind(GridBackendRow* p_ChildRow)
{
	if (GridUtil::IsBusinessUser(p_ChildRow, false))
		setRowKind(p_ChildRow, INDIRECT_MEMBER_USER);
	else if (GridUtil::IsBusinessUser(p_ChildRow, true))
		setRowKind(p_ChildRow, INDIRECT_MEMBER_GUEST);
	else if (GridUtil::IsBusinessGroup(p_ChildRow))
		setRowKind(p_ChildRow, INDIRECT_MEMBER_GROUP);
	else if (GridUtil::IsBusinessOrgContact(p_ChildRow))
		setRowKind(p_ChildRow, INDIRECT_MEMBER_ORGCONTACT);
	else
		ASSERT(false);
}

bool GridGroupsHierarchy::isDirectChild(GridBackendRow* p_ChildRow) const
{
	auto isDirectChild = [this](int iconId)
	{
		return 0 != iconId
			&& (iconId == m_KindInfos.at(MEMBER_USER).first
				|| iconId == m_KindInfos.at(OWNER_MEMBER_USER).first
				|| iconId == m_KindInfos.at(EXTERNAL_OWNER).first
				|| iconId == m_KindInfos.at(MEMBER_GUEST).first
				|| iconId == m_KindInfos.at(OWNER_MEMBER_GUEST).first
				|| iconId == m_KindInfos.at(EXTERNAL_OWNER_GUEST).first
				|| iconId == m_KindInfos.at(MEMBER_GROUP).first
				|| iconId == m_KindInfos.at(MEMBER_ORGCONTACT).first);
	};
	return nullptr != p_ChildRow && nullptr != m_ColumnMemberKind && isDirectChild(p_ChildRow->GetField(m_ColumnMemberKind).GetIcon());
}

bool GridGroupsHierarchy::isDirectMember(GridBackendRow* p_ChildRow) const
{
	auto isDirectMemberIcon = [this](int iconId)
	{
		return 0 != iconId
			&& (iconId == m_KindInfos.at(MEMBER_USER).first
				|| iconId == m_KindInfos.at(OWNER_MEMBER_USER).first
				|| iconId == m_KindInfos.at(MEMBER_GUEST).first
				|| iconId == m_KindInfos.at(OWNER_MEMBER_GUEST).first
				|| iconId == m_KindInfos.at(MEMBER_GROUP).first
				|| iconId == m_KindInfos.at(MEMBER_ORGCONTACT).first);
	};
	return nullptr != p_ChildRow && nullptr != m_ColumnMemberKind && isDirectMemberIcon(p_ChildRow->GetField(m_ColumnMemberKind).GetIcon());
}

bool GridGroupsHierarchy::isOwnerCandidate(GridBackendRow* p_ChildRow) const
{
	auto isOwnerCandidateIcon = [this](int iconId)
	{
		return 0 != iconId
			&& (	iconId == m_KindInfos.at(MEMBER_USER).first
				||	iconId == m_KindInfos.at(MEMBER_GUEST).first
				||	iconId == m_KindInfos.at(MEMBER_GROUP).first);
	};
	return nullptr != p_ChildRow && nullptr != m_ColumnMemberKind && isOwnerCandidateIcon(p_ChildRow->GetField(m_ColumnMemberKind).GetIcon());
}

bool GridGroupsHierarchy::isOwner(GridBackendRow* p_ChildRow) const
{
	auto isOwnerIcon = [this](auto iconId)
	{
		return 0 != iconId
			&& (	iconId == m_KindInfos.at(OWNER_MEMBER_USER).first
				||	iconId == m_KindInfos.at(EXTERNAL_OWNER).first
				||	iconId == m_KindInfos.at(OWNER_MEMBER_GUEST).first
				||	iconId == m_KindInfos.at(EXTERNAL_OWNER_GUEST).first);
	};
	return nullptr != p_ChildRow && nullptr != m_ColumnMemberKind && isOwnerIcon(p_ChildRow->GetField(m_ColumnMemberKind).GetIcon());
}

bool GridGroupsHierarchy::isExternalOwner(GridBackendRow* p_ChildRow) const
{
	auto isExternalOwnerIcon = [this](auto iconId)
	{
		return 0 != iconId
			&& (	iconId == m_KindInfos.at(EXTERNAL_OWNER).first
				||	iconId == m_KindInfos.at(EXTERNAL_OWNER_GUEST).first);
	};

	return nullptr != p_ChildRow && nullptr != m_ColumnMemberKind && isExternalOwnerIcon(p_ChildRow->GetField(m_ColumnMemberKind).GetIcon());
}

void GridGroupsHierarchy::addHierarchyPathField(GridBackendRow* p_ChildRow)
{
	if (nullptr != p_ChildRow && nullptr != m_ColumnHierarchyPath)
	{
		const auto path = Str::implode(m_BufferHierarchyGroupNames, _YTEXT("/"), false);
		if (!path.IsEmpty())
			p_ChildRow->AddField(path, m_ColumnHierarchyPath);
	}
}

bool GridGroupsHierarchy::upgradeExternalToRegularOwner(GridBackendRow* p_Row)
{
	bool applied = false;

	ASSERT(isExternalOwner(p_Row));

	row_pk_t rowPK;
	GetRowPK(p_Row, rowPK);

	if (p_Row->IsCreated())
	{
		auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPK);
		ASSERT(nullptr != mod);
		if (nullptr != mod)
		{
			GetModifications().RemoveRowModifications(rowPK, false);

			ASSERT(p_Row->GetLParam() == BusinessObject::UPDATE_TYPE_ADDOWNERS);
			if (GridUtil::IsBusinessUser(p_Row, false))
				setRowKind(p_Row, OWNER_MEMBER_USER);
			else if (GridUtil::IsBusinessUser(p_Row, true))
				setRowKind(p_Row, OWNER_MEMBER_GUEST);
			setParamAddMemberAddOwner(p_Row);
			GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPK));

			applied = true;
		}
	}
	else if (nullptr != m_ColumnMemberKind)
	{
		ASSERT(!p_Row->IsModified());
		auto hadField = p_Row->HasField(m_ColumnMemberKind);
		auto oldField = p_Row->GetField(m_ColumnMemberKind);

		if (GridUtil::IsBusinessUser(p_Row, false))
			setRowKind(p_Row, OWNER_MEMBER_USER);
		else if (GridUtil::IsBusinessUser(p_Row, true))
			setRowKind(p_Row, OWNER_MEMBER_GUEST);
		else
			ASSERT(false);

		auto& newField = p_Row->GetField(m_ColumnMemberKind);

		setParamAddMember(p_Row);

		GetModifications().Add(std::make_unique<FieldUpdateO365>(*this, rowPK, m_ColumnMemberKind->GetID(), hadField ? oldField : GridBackendField::g_GenericConstField, newField));

		applied = true;
	}

	return applied;
}

bool GridGroupsHierarchy::upgradeMemberToOwner(GridBackendRow* p_Row)
{
	bool applied = false;

	ASSERT(isOwnerCandidate(p_Row));

	row_pk_t rowPK;
	GetRowPK(p_Row, rowPK);

	if (p_Row->IsCreated())
	{
		auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPK);
		ASSERT(nullptr != mod);
		if (nullptr != mod)
		{
			GetModifications().RemoveRowModifications(rowPK, false);

			ASSERT(p_Row->GetLParam() == BusinessObject::UPDATE_TYPE_ADDMEMBERS);
			if (GridUtil::IsBusinessUser(p_Row, false))
				setRowKind(p_Row, OWNER_MEMBER_USER);
			else if (GridUtil::IsBusinessUser(p_Row, true))
				setRowKind(p_Row, OWNER_MEMBER_GUEST);
			else
				ASSERT(false);

			setParamAddMemberAddOwner(p_Row);
			GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPK));

			applied = true;
		}
	}
	else if (nullptr != m_ColumnMemberKind)
	{
		auto hadField = p_Row->HasField(m_ColumnMemberKind);
		auto oldField = p_Row->GetField(m_ColumnMemberKind);

		if (GridUtil::IsBusinessUser(p_Row, false))
			setRowKind(p_Row, OWNER_MEMBER_USER);
		else if (GridUtil::IsBusinessUser(p_Row, true))
			setRowKind(p_Row, OWNER_MEMBER_GUEST);
		else
			ASSERT(false);

		auto& newField = p_Row->GetField(m_ColumnMemberKind);

		setParamAddOwner(p_Row);

		GetModifications().Add(std::make_unique<FieldUpdateO365>(*this, rowPK, m_ColumnMemberKind->GetID(), hadField ? oldField : GridBackendField::g_GenericConstField, newField));
		applied = true;
	}

	return applied;
}

bool GridGroupsHierarchy::downgradeOwnerToMember(GridBackendRow* p_Row)
{
	bool applied = false;

	ASSERT(isOwner(p_Row) && !isExternalOwner(p_Row));

	row_pk_t rowPK;
	GetRowPK(p_Row, rowPK);

	if (p_Row->IsCreated())
	{
		auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPK);
		ASSERT(nullptr != mod);
		if (nullptr != mod)
		{
			GetModifications().RemoveRowModifications(rowPK, false);

			ASSERT(p_Row->GetLParam() == (BusinessObject::UPDATE_TYPE_ADDMEMBERS | BusinessObject::UPDATE_TYPE_ADDOWNERS));
			setDirectMemberRowKind(p_Row);
			setParamAddMember(p_Row);
			GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPK));

			applied = true;
		}
	}
	else if (nullptr != m_ColumnMemberKind)
	{
		auto hadField = p_Row->HasField(m_ColumnMemberKind);
		auto oldField = p_Row->GetField(m_ColumnMemberKind);

		if (GridUtil::IsBusinessUser(p_Row, false))
			setRowKind(p_Row, MEMBER_USER);
		else if (GridUtil::IsBusinessUser(p_Row, true))
			setRowKind(p_Row, MEMBER_GUEST);
		else
			ASSERT(false);

		auto& newField = p_Row->GetField(m_ColumnMemberKind);

		if (p_Row->GetLParam() == BusinessObject::UPDATE_TYPE_ADDMEMBERS)
			setParamAddMemberDeleteOwner(p_Row);
		else
			setParamDeleteOwner(p_Row);

		GetModifications().Add(std::make_unique<FieldUpdateO365>(*this, rowPK, m_ColumnMemberKind->GetID(), hadField ? oldField : GridBackendField::g_GenericConstField, newField));

		applied = true;
	}

	return applied;
}

bool GridGroupsHierarchy::changeRegularToExternalOwner(GridBackendRow* p_Row)
{
	bool applied = false;

	ASSERT(isOwner(p_Row) && !isExternalOwner(p_Row));

	row_pk_t rowPK;
	GetRowPK(p_Row, rowPK);

	if (p_Row->IsCreated())
	{
		auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPK);
		ASSERT(nullptr != mod);
		if (nullptr != mod)
		{
			GetModifications().RemoveRowModifications(rowPK, false);

			ASSERT(p_Row->GetLParam() == (BusinessObject::UPDATE_TYPE_ADDMEMBERS | BusinessObject::UPDATE_TYPE_ADDOWNERS));
			setRowKind(p_Row, EXTERNAL_OWNER);
			setParamAddOwner(p_Row);
			GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPK));

			applied = true;
		}
	}
	else if (nullptr != m_ColumnMemberKind)
	{
		auto hadField = p_Row->HasField(m_ColumnMemberKind);
		auto oldField = p_Row->GetField(m_ColumnMemberKind);

		if (GridUtil::IsBusinessUser(p_Row, false))
			setRowKind(p_Row, EXTERNAL_OWNER);
		else if (GridUtil::IsBusinessUser(p_Row, true))
			setRowKind(p_Row, EXTERNAL_OWNER_GUEST);
		else
			ASSERT(false);

		auto& newField = p_Row->GetField(m_ColumnMemberKind);

		if (p_Row->GetLParam() == BusinessObject::UPDATE_TYPE_ADDMEMBERS)
		{
			auto mod = GetModifications().Revert(rowPK, false);
		}
		else
		{
			setParamDeleteMember(p_Row);

			GetModifications().Add(std::make_unique<FieldUpdateO365>(*this, rowPK, m_ColumnMemberKind->GetID(), hadField ? oldField : GridBackendField::g_GenericConstField, newField));
		}

		applied = true;
	}

	return applied;
}

void GridGroupsHierarchy::setParamAddMember(GridBackendRow* p_Row)
{
	p_Row->SetLParam(BusinessObject::UPDATE_TYPE_ADDMEMBERS);
}

void GridGroupsHierarchy::setParamDeleteMember(GridBackendRow* p_Row)
{
	p_Row->SetLParam(BusinessObject::UPDATE_TYPE_DELETEMEMBERS);
}

void GridGroupsHierarchy::setParamAddOwner(GridBackendRow* p_Row)
{
	p_Row->SetLParam(BusinessObject::UPDATE_TYPE_ADDOWNERS);
}

void GridGroupsHierarchy::setParamAddMemberAddOwner(GridBackendRow* p_Row)
{
	p_Row->SetLParam(BusinessObject::UPDATE_TYPE_ADDMEMBERS | BusinessObject::UPDATE_TYPE_ADDOWNERS);
}

void GridGroupsHierarchy::setParamDeleteOwner(GridBackendRow* p_Row)
{
	p_Row->SetLParam(BusinessObject::UPDATE_TYPE_DELETEOWNERS);
}

void GridGroupsHierarchy::setParamAddMemberDeleteOwner(GridBackendRow* p_Row)
{
	p_Row->SetLParam(BusinessObject::UPDATE_TYPE_ADDMEMBERS | BusinessObject::UPDATE_TYPE_DELETEOWNERS);
}

void GridGroupsHierarchy::setParamDeleteMemberDeleteOwner(GridBackendRow* p_Row)
{
	p_Row->SetLParam(BusinessObject::UPDATE_TYPE_DELETEMEMBERS | BusinessObject::UPDATE_TYPE_DELETEOWNERS);
}

bool GridGroupsHierarchy::canRowBeDeleted(GridBackendRow* p_Row)
{
	const auto memberPrivilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT;
	const auto ownerPrivilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT;
	const bool hasOutOfScopeMemberPrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE);
	const bool hasOutOfScopeOwnerPrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE);

	return nullptr != p_Row
			&& !p_Row->IsGroupRow()
			&& nullptr != p_Row->GetParentRow()
			&& !p_Row->IsDeleted()
			&& p_Row->GetHierarchyLevel() == 1

			&& (isDirectMember(p_Row) && IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis(), memberPrivilege)
				|| isExternalOwner(p_Row) && IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis(), ownerPrivilege))

			&& (isDirectMember(p_Row) && (hasOutOfScopeMemberPrivilege
				|| IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)
				|| IsGroupAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE))
				||
				isExternalOwner(p_Row) && (hasOutOfScopeOwnerPrivilege || IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)))
			;
}

#if MEMBERS_GRID_HIDE_NESTED
void GridGroupsHierarchy::refreshNestedGroupsRowDisplay(bool p_Update)
{
	m_HasNestedGroupsMembers = false;
	for (auto& row : GetBackendRows())
	{
		if (row->GetHierarchyLevel() == 2)
		{
			m_HasNestedGroupsMembers = true;
			row->SetTechHidden(!m_ShowNestedGroupMembers);
		}
	}

	refreshNestedGroupsColumnDisplay(false);

	if (p_Update && m_HasNestedGroupsMembers)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridGroupsHierarchy::refreshNestedGroupsColumnDisplay(bool p_Update)
{
	if (nullptr != m_ColumnHierarchyPath)
		SetColumnsVisible({ m_ColumnHierarchyPath }, m_ShowNestedGroupMembers && m_HasNestedGroupsMembers, p_Update);
}
#endif
