#pragma once

#include "BaseO365Grid.h"
#include "GridTemplate.h"
#include "GridUtil.h"
#include "OnPremiseGroupsColumns.h"

#define GROUP_DISPLAY_NAME_COL_UID "groupDisplayName"

class BusinessGroup;
class GridUpdater;
class GridTemplateGroups : public GridTemplate
{
public:
    GridTemplateGroups();
	// p_AdditionalFields corresponds to data not contained in BusinessGroup.
	// if p_SetCreatedStatus is true, p_ParentRow and p_AdditionalFields are really important.
	// Without them, parent and fields won't be correctly restored after an error (they're stored in CreatedObjectModification).
	// if p_SetCreatedStatus is false, p_ParentRow and p_AdditionalFields are honored but not really useful.
	GridBackendRow* AddRow(O365Grid& p_Grid, const BusinessGroup& p_BG, const vector<GridBackendField>& p_ExtraRowPKValues, GridUpdater& p_Updater, bool p_SetCreatedStatus, GridBackendRow* p_ParentRow, const vector<GridBackendField>& p_AdditionalFields, bool p_KeepExistingIfGroupCanceled);
	bool			RemoveRow(O365Grid& p_Grid, const BusinessGroup& p_BG);
	GridBackendRow*	UpdateRow(O365Grid& p_Grid, const BusinessGroup& p_BG, bool p_SetModifiedStatus, GridUpdater& p_ErrorHelper, bool p_FromLoadMore);
	GridBackendRow* UpdateRow(O365Grid& p_Grid, const OnPremiseGroup& p_OnPremiseGroup, const map<wstring, row_pk_t>& p_O365RowPk, map<wstring, row_pk_t>& p_OnPremRowPk, UpdateFieldOption p_UpdateFieldOption = UpdateFieldOption::NONE);
	GridBackendRow* UpdateRow(O365Grid& p_Grid, const OnPremiseGroup& p_OnPremiseGroup, const map<wstring, row_pk_t>& p_O365RowPk, map<wstring, row_pk_t>& p_OnPremRowPk, std::set<PooledString>& p_NewObjectSIDs, UpdateFieldOption p_UpdateFieldOption = UpdateFieldOption::NONE);

	void			GetBusinessGroup(const O365Grid& p_Grid, GridBackendRow* p_Row, BusinessGroup& p_BG) const;
	void			GetListOfFieldErrors(const O365Grid& p_Grid, GridBackendRow* p_Row, std::set<wstring>& p_ListFieldErrors) const;
	row_pk_t		CreateRowPk(const OnPremiseGroup& p_OnPremGroup);

	bool GetOnPremiseGroup(const O365Grid& p_Grid, GridBackendRow* p_Row, OnPremiseGroup& p_OnPremGroup) const;

	bool GetBusinessGroupPK(O365Grid& p_Grid, const BusinessGroup& p_BG, vector<GridBackendField>& BGpk, const vector<GridBackendField>& p_ExtraRowPKValue) const;
	void updateRow(O365Grid& p_Grid, const BusinessGroup& p_BG, GridBackendRow* p_Row, const GraphCache& p_GraphCache, bool p_StoreModification);

	void SetDebugMode(bool p_DebugMode);

	void SetRowIsTeam(const BusinessGroup& p_BG, GridBackendRow* p_RowToUpdate, GridBackendColumn* p_IsTeamColumn, UpdateFieldOption p_UpdateFieldOption);

	const boost::YOpt<Team> GetTeam(const GridBackendRow* p_Row) const;
	bool IsTeamColumn(GridBackendColumn* p_Column) const;

	GridBackendColumn*& GetColumnForProperty(const wstring& p_PropertyName);
	GridBackendColumn* AddDefaultColumnFor(O365Grid& p_Grid, const wstring& p_PropertyName, const wstring& p_FamilyName, const set<GridBackendUtil::COLUMNPRESETFLAGS>& p_Flags);
	GridBackendColumn* AddDefaultColumnFor(O365Grid& p_Grid, const wstring& p_PropertyName, const wstring& p_FamilyName, const set<GridBackendUtil::COLUMNPRESETFLAGS>& p_Flags, const wstring& p_CustomName, const wstring& p_CustomUniqueID);

	virtual void CustomizeGrid(O365Grid& p_Grid);

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column);
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column);

	void SetO365(O365Grid& p_Grid, GridBackendRow& p_Row);
	void SetOnPrem(O365Grid& p_Grid, GridBackendRow& p_Row);
	void SetHybrid(O365Grid& p_Grid, GridBackendRow& p_Row);

	void UpdateCommonInfo(GridBackendRow* p_Row);

public:
	GridBackendColumn*& GetRequestDateColumn(GBI p_BlockId);

private:
	void AddIconTeam(O365Grid& p_Grid);

    void updateRowCreatedOnBehalfOf(const BusinessGroup& p_BG, GridBackendRow* p_RowToUpdatee, const GraphCache& p_GraphCache);
	void updateRowTeam(const BusinessGroup& p_BG, GridBackendRow* p_RowToUpdate, bool p_StoreModification);
	void updateRowGroupSettings(const BusinessGroup& p_BG, GridBackendRow* p_RowToUpdate, bool p_StoreModification);
	void updateRowOwners(const BusinessGroup& p_BG, GridBackendRow* p_RowToUpdate, const GraphCache& p_GraphCache, UpdateFieldOption p_UpdateFieldOption);
	void updateRowDrive(const BusinessGroup& p_BG, GridBackendRow* p_RowToUpdate, UpdateFieldOption p_UpdateFieldOption);
	void updateRowIsYammer(const BusinessGroup& p_BG, GridBackendRow* p_RowToUpdate);
	void archiveTeam(GridBackendRow* p_Row, bool p_ShouldArchive, bool p_SetSpoReadOnlyForMembers);

	template<typename T>
	GridBackendField* updateField(const T& p_Value, GridBackendRow* p_Row, GridBackendColumn* p_Col, UpdateFieldOption p_UpdateFieldOption);

	template<typename T>
	GridBackendField* updateFieldOnPrem(const T& p_Value, GridBackendRow* p_Row, GridBackendColumn* p_Col, UpdateFieldOption p_UpdateFieldOption);

	void setLastRequestDateTime(O365Grid& p_Grid, const BusinessGroup& p_BG, GridBackendRow* p_Row);

	bool isTeamMemberSettingsColumn(GridBackendColumn* p_Column) const;
	bool isTeamGuestSettingsColumn(GridBackendColumn* p_Column) const;
	bool isTeamMessagingSettingsColumn(GridBackendColumn* p_Column) const;
	bool isTeamFunSettingsColumn(GridBackendColumn* p_Column) const;

	bool hasTeamMemberSettingsColumn() const;
	bool hasTeamGuestSettingsColumn() const;
	bool hasTeamMessagingSettingsColumn() const;
	bool hasTeamFunSettingsColumn() const;

	bool isTeam(const GridBackendRow* p_Row) const;

	GridBackendColumn* getRequestDateColumn(GBI p_BlockId) const;

	static GridBackendColumn*& getColumnForProperty(GridTemplateGroups& p_That, const wstring& p_PropertyName);

public:
	static COLORREF g_ColorRowGroup;

	GridBackendColumn* m_ColumnClassification;
	GridBackendColumn* m_ColumnCreatedDateTime;
	GridBackendColumn* m_ColumnRenewedDateTime;
	GridBackendColumn* m_ColumnExpireOnDateTime;
	GridBackendColumn* m_ColumnDeletedDateTime;
	GridBackendColumn* m_ColumnExpirationPolicyStatus;
	GridBackendColumn* m_ColumnDescription;
	GridBackendColumn* m_ColumnGroupTypes;
	GridBackendColumn* m_ColumnUnseenCount;
	GridBackendColumn* m_ColumnVisibility;
	GridBackendColumn* m_ColumnAllowExternalSenders;
	GridBackendColumn* m_ColumnAutoSubscribeNewMembers;
	//GridBackendColumn* m_ColumnIsSubscribedByMail;
	GridBackendColumn* m_ColumnSecurityEnabled;

	GridBackendColumn* m_ColumnOPLastSync;
	GridBackendColumn* m_ColumnOPSecurityID;
	GridBackendColumn* m_ColumnOPSyncEnabled;
	GridBackendColumn* m_ColumnOPPECategory;
	GridBackendColumn* m_ColumnOPPEOccuredDateTime;
	GridBackendColumn* m_ColumnOPPEProperty;
	GridBackendColumn* m_ColumnOPPEValue;

	GridBackendColumn* m_ColumnMail;
	GridBackendColumn* m_ColumnMailEnabled;
	GridBackendColumn* m_ColumnMailNickname;
	GridBackendColumn* m_ColumnProxyAddresses;
	GridBackendColumn* m_ColumnCombinedGroupType;
	GridBackendColumn* m_ColumnDynamicMembership;
    GridBackendColumn* m_ColumnCreatedOnBehalfOfUserId;
    GridBackendColumn* m_ColumnCreatedOnBehalfOfUserDisplayName;
    GridBackendColumn* m_ColumnCreatedOnBehalfOfUserPrincipalName;
	GridBackendColumn* m_ColumnOwnerUserIDs;
	GridBackendColumn* m_ColumnOwnerUserNames;
    GridBackendColumn* m_ColumnIsTeam;
	GridBackendColumn* m_ColumnIsYammer;
    GridBackendColumn* m_ColumnGroupUnifiedSettingActivated;
    GridBackendColumn* m_ColumnSettingAllowToAddGuests;
	GridBackendColumn* m_ColumnPreferredDataLocation;


    // Team columns
    GridBackendColumn* m_ColumnTeamMemberSettingsAllowCreateUpdateChannels;
	GridBackendColumn* m_ColumnTeamMemberSettingsAllowCreatePrivateChannels;
    GridBackendColumn* m_ColumnTeamMemberSettingsAllowDeleteChannels;
    GridBackendColumn* m_ColumnTeamMemberSettingsAllowAddRemoveApps;
    GridBackendColumn* m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs;
    GridBackendColumn* m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors;

    GridBackendColumn* m_ColumnTeamGuestSettingsAllowCreateUpdateChannels;
    GridBackendColumn* m_ColumnTeamGuestSettingsAllowDeleteChannels;

    GridBackendColumn* m_ColumnTeamMessagingSettingsAllowUserEditMessages;
    GridBackendColumn* m_ColumnTeamMessagingSettingsAllowUserDeleteMessages;
    GridBackendColumn* m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages;
    GridBackendColumn* m_ColumnTeamMessagingSettingsAllowTeamMentions;
    GridBackendColumn* m_ColumnTeamMessagingSettingsAllowChannelMentions;

    GridBackendColumn* m_ColumnTeamFunSettingsAllowGiphy;
    GridBackendColumn* m_ColumnTeamFunSettingsGiphyContentRating;
    GridBackendColumn* m_ColumnTeamFunSettingsAllowStickersAndMemes;
    GridBackendColumn* m_ColumnTeamFunSettingsAllowCustomMemes;

    GridBackendColumn* m_ColumnTeamWebUrl;

	GridBackendColumn* m_ColumnTeamIsArchived;
	GridBackendColumn* m_ColumnSpoSiteReadOnlyForMembers;
	GridBackendColumn* m_ColumnTeamInternalId;

	GridBackendColumn* m_ColumnResourceBehaviorOptions;
	GridBackendColumn* m_ColumnResourceProvisioningOptions;

	GridBackendColumn* m_ColumnMembersCount;

	GridBackendColumn* m_ColumnHideFromOutlookClients;
	GridBackendColumn* m_ColumnHideFromAddressLists;
	GridBackendColumn* m_ColumnLicenseProcessingState;
	GridBackendColumn* m_ColumnSecurityIdentifier;

	GridBackendColumn* m_ColumnSkuIds;
	GridBackendColumn* m_ColumnSkuPartNumbers;
	GridBackendColumn* m_ColumnSkuNames;

	/* Beta API */
	GridBackendColumn*	m_ColumnMembershipRuleProcessingState;
	GridBackendColumn*	m_ColumnMembershipRule;
	GridBackendColumn*	m_ColumnTheme;
	GridBackendColumn*	m_ColumnPreferredLanguage;

	GridBackendColumn* m_ColumnLastRequestDateTime;
	std::map<GBI, GridBackendColumn*> m_ColumnsRequestDateTime;

	// Drive columns
	GridBackendColumn* m_ColumnDriveName;
	GridBackendColumn* m_ColumnDriveId;
	GridBackendColumn* m_ColumnDriveDescription;
	GridBackendColumn* m_ColumnDriveQuotaState;
	GridBackendColumn* m_ColumnDriveQuotaUsed;
	GridBackendColumn* m_ColumnDriveQuotaRemaining;
	GridBackendColumn* m_ColumnDriveQuotaDeleted;
	GridBackendColumn* m_ColumnDriveQuotaTotal;
	GridBackendColumn* m_ColumnDriveCreationTime;
	GridBackendColumn* m_ColumnDriveLastModifiedTime;
	GridBackendColumn* m_ColumnDriveType;
	GridBackendColumn* m_ColumnDriveWebUrl;
	GridBackendColumn* m_ColumnDriveCreatedByUserEmail;
	GridBackendColumn* m_ColumnDriveCreatedByUserId;
	GridBackendColumn* m_ColumnDriveCreatedByAppName;
	GridBackendColumn* m_ColumnDriveCreatedByAppId;
	GridBackendColumn* m_ColumnDriveCreatedByDeviceName;
	GridBackendColumn* m_ColumnDriveCreatedByDeviceId;
	GridBackendColumn* m_ColumnDriveOwnerUserName;
	GridBackendColumn* m_ColumnDriveOwnerUserId;
	GridBackendColumn* m_ColumnDriveOwnerUserEmail;
	GridBackendColumn* m_ColumnDriveOwnerAppName;
	GridBackendColumn* m_ColumnDriveOwnerAppId;
	GridBackendColumn* m_ColumnDriveOwnerDeviceName;
	GridBackendColumn* m_ColumnDriveOwnerDeviceId;

	OnPremiseGroupsColumns m_OnPremCols;

    int m_IconIsTeam;
	int m_IconIsArchived;
	//int m_IconIsNotArchived;
	int m_IconIsYammer;
	bool m_DebugMode;
};
