#include "GridGroupsAuthorsHierarchy.h"

#include "AutomationWizardGroupsAuthors.h"
#include "BasicGridSetup.h"
#include "BusinessGroup.h"
#include "BusinessGroupConfiguration.h"
#include "FrameGroupAuthorsHierarchy.h"
#include "GridUpdater.h"
#include "../Resource.h"
#include "BusinessUserGuest.h"

wstring GridGroupsAuthorsHierarchy::g_AcceptedStatus;
wstring GridGroupsAuthorsHierarchy::g_RejectedStatus;

BEGIN_MESSAGE_MAP(GridGroupsAuthorsHierarchy, GridGroupsCommon)
	ON_COMMAND(ID_AUTHORSGRID_ADDREJECTED,						OnAddRejectedToGroups)
	ON_UPDATE_COMMAND_UI(ID_AUTHORSGRID_ADDREJECTED,			OnUpdateAddItemsToGroups)
	ON_COMMAND(ID_AUTHORSGRID_ADDACCEPTED,						OnAddAcceptedToGroups)
	ON_UPDATE_COMMAND_UI(ID_AUTHORSGRID_ADDACCEPTED,			OnUpdateAddItemsToGroups)
	ON_COMMAND(ID_AUTHORSGRID_DELETEAUTHORIZATION,				OnRemoveItemsFromGroups)
	ON_UPDATE_COMMAND_UI(ID_AUTHORSGRID_DELETEAUTHORIZATION,	OnUpdateRemoveItemsFromGroups)
END_MESSAGE_MAP()

GridGroupsAuthorsHierarchy::GridGroupsAuthorsHierarchy()
	: GridGroupsCommon()
	, m_Frame(nullptr)
	, m_IconIDauthorizationAcceptedAll(NO_ICON)
	, m_IconIDauthorizationRestricted(NO_ICON)
	, m_IconIDauthorizationAccepted(NO_ICON)
	, m_IconIDauthorizationRejected(NO_ICON)
{
	m_MayContainUnscopedUserGroupOrSite = true;

	initWizard<AutomationWizardGroupsAuthors>(_YTEXT("Automation\\GroupsAuthors"));

	if (g_AcceptedStatus.empty())
		g_AcceptedStatus = YtriaTranslate::Do(GridGroupsAuthorsHierarchy_setRowAuhorization_1, _YLOC("Accepted")).c_str();
		
	if (g_RejectedStatus.empty())
		g_RejectedStatus = YtriaTranslate::Do(GridGroupsAuthorsHierarchy_setRowAuhorization_2, _YLOC("Rejected")).c_str();
}

void GridGroupsAuthorsHierarchy::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameGroupsAuthorsHierarchy, LicenseUtil::g_codeSapio365groupAuhors,
		{
			{ nullptr, _YTEXT(""), _YTEXT("") },
			{ nullptr, _YTEXT(""), _YTEXT("") },
			{ &m_TemplateUsers.m_ColumnMetaID, YtriaTranslate::Do(GridGroupsAuthorsHierarchy_customizeGrid_1, _YLOC("Group ID")).c_str(), GridTemplate::g_TypeGroup }
		});
		setup.Setup(*this, true);
		m_TemplateGroups.m_ColumnLastRequestDateTime = setup.GetColumnLastRequestDateTime();
	}

	// We create multivalue-ids for child-groups.
	m_TemplateUsers.m_ColumnMetaID->SetExplosionEnabled(false);

	m_TemplateGroups.m_ColumnDisplayName		= AddColumn(_YUID(O365_GROUP_DISPLAYNAME),		YtriaTranslate::Do(GridGroupsAuthorsHierarchy_customizeGrid_2, _YLOC("Recipient Group")).c_str(),		GridTemplate::g_TypeGroup,	g_ColumnSize22, { g_ColumnsPresetDefault });
	m_TemplateGroups.m_ColumnIsTeam				= AddColumnIcon(_YUID("recipientIsTeam"),		YtriaTranslate::Do(GridGroupsAuthorsHierarchy_customizeGrid_10, _YLOC("Recipient Is a Team")).c_str(),	GridTemplate::g_TypeGroup);
	SetColumnPresetFlags(m_TemplateGroups.m_ColumnIsTeam, { g_ColumnsPresetDefault });
	m_TemplateGroups.m_ColumnCombinedGroupType	= AddColumn(_YUID("groupType"),					YtriaTranslate::Do(GridGroupsAuthorsHierarchy_customizeGrid_3, _YLOC("Recipient Group Type")).c_str(),	GridTemplate::g_TypeGroup,	g_ColumnSize12, { g_ColumnsPresetDefault });
	SetColumnPresetFlags(m_TemplateGroups.m_ColumnCombinedGroupType, { g_ColumnsPresetDefault });

	// Do not insert columns that are also PK
	m_MetaColumns.insert(m_MetaColumns.end(), { /*m_TemplateUsers.m_ColumnMetaID, */m_TemplateGroups.m_ColumnDisplayName, m_TemplateGroups.m_ColumnIsTeam, m_TemplateGroups.m_ColumnCombinedGroupType });

	/*Add additional meta columns*/
	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		for (const auto& c : *metaDataColumnInfos)
		{
			ASSERT(nullptr == m_TemplateGroups.GetColumnForProperty(c.GetPropertyName()));
			if (nullptr == m_TemplateGroups.GetColumnForProperty(c.GetPropertyName()))
			{
				auto col = c.AddTo(*this, GridTemplate::g_TypeGroup);
				m_TemplateGroups.GetColumnForProperty(c.GetPropertyName()) = col;
				m_MetaColumns.push_back(col);
				m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
			}
		}
	}

	m_ColumnGroupNameForMember					= AddColumn(_YUID("PGN"),						YtriaTranslate::Do(GridGroupsAuthorsHierarchy_customizeGrid_4, _YLOC("Group Display Name")).c_str(),	GridTemplate::g_TypeGroup,	g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColumnIsTeamForMember						= AddColumnIcon(_YUID(O365_GROUP_ISTEAM),			YtriaTranslate::Do(GridGroups_customizeGrid_3, _YLOC("Is a Team")).c_str(), GridTemplate::g_TypeGroup);
	SetColumnPresetFlags(m_ColumnIsTeamForMember, { g_ColumnsPresetDefault });
	m_ColumnCombinedGroupTypeForMember			= AddColumn(_YUID("PGT"),						YtriaTranslate::Do(GridGroupsAuthorsHierarchy_customizeGrid_5, _YLOC("Group Type")).c_str(),			GridTemplate::g_TypeGroup,	g_ColumnSize12, { g_ColumnsPresetDefault });
	setBasicGridSetupHierarchy({ { { m_TemplateGroups.m_ColumnDisplayName, GridBackendUtil::g_AllHierarchyLevels } }
								, O365Grid::BasicGridSetupHierarchy::AllUserObjectTypes(true)
								,{ rttr::type::get<BusinessUser>().get_id(), rttr::type::get<BusinessGroup>().get_id()/*, rttr::type::get<BusinessOrgContact>().get_id()*/ } });

	m_TemplateUsers.m_ColumnDisplayName			= AddColumn(_YUID(USER_DISPLAY_NAME_COL_UID),	YtriaTranslate::Do(GridGroupsAuthorsHierarchy_customizeGrid_6, _YLOC("User Display Name")).c_str(),	GridTemplate::g_TypeUser,	g_ColumnSize22, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnUserPrincipalName	= AddColumn(_YUID(O365_USER_USERPRINCIPALNAME), YtriaTranslate::Do(GridGroupsAuthorsHierarchy_customizeGrid_7, _YLOC("Username")).c_str(),			GridTemplate::g_TypeUser,	g_ColumnSize32);
	m_TemplateUsers.m_ColumnUserType			= AddColumn(_YUID(O365_USER_USERTYPE),			YtriaTranslate::Do(GridUsers_customizeGrid_3, _YLOC("User Type")).c_str(),							GridTemplate::g_TypeUser,	g_ColumnSize12);
	addColumnGraphID(GridTemplate::g_TypeUser);
	m_TemplateGroups.m_ColumnID = GetColId();
	m_TemplateUsers.m_ColumnID	= GetColId();
	m_ColumnAcceptedStatus						= AddColumn(_YUID("ACC"),						YtriaTranslate::Do(GridGroupsAuthorsHierarchy_customizeGrid_8, _YLOC("Accepted/Rejected")).c_str(),	YtriaTranslate::Do(GridGroupsAuthorsHierarchy_customizeGrid_9, _YLOC("Status")).c_str(),				g_ColumnSize22, { g_ColumnsPresetDefault });
	m_TemplateGroups.CustomizeGrid(*this);

	AddColumnForRowPK(GetColId());
	AddColumnForRowPK(m_TemplateUsers.m_ColumnMetaID);

	m_Frame = dynamic_cast<FrameGroupAuthorsHierarchy*>(GetParentFrame());
    m_Frame->SetGrid(this);
	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_Frame);

	{
		AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SENDERSRULESSET));
		m_IconIDauthorizationRestricted = GetImageCount() - 1;
	}

	m_IconIDauthorizationAcceptedAll	= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SENDERSACCEPTEDALL));
	m_IconIDauthorizationRejected		= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SENDERSREFUSED));
	m_IconIDauthorizationAccepted		= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SENDERSACCEPTED));
}

void GridGroupsAuthorsHierarchy::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_AUTHORSGRID_ADDACCEPTED);
		pPopup->ItemInsert(ID_AUTHORSGRID_ADDREJECTED);
		pPopup->ItemInsert(); // Sep
		pPopup->ItemInsert(ID_AUTHORSGRID_DELETEAUTHORIZATION);
		pPopup->ItemInsert(); // Sep
	}

	GridGroupsCommon::OnCustomPopupMenu(pPopup, nStart);
}

wstring GridGroupsAuthorsHierarchy::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Group"), m_TemplateGroups.m_ColumnDisplayName } }, m_TemplateUsers.m_ColumnDisplayName);
}

void GridGroupsAuthorsHierarchy::InitializeCommands()
{
	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_AUTHORSGRID_ADDREJECTED;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_1, _YLOC("Add Rejected...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_2, _YLOC("Add Rejected...")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_ADDAUTHORSREJECTED_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_AUTHORSGRID_ADDREJECTED, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_3, _YLOC("Add Rejected Senders")).c_str(), YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_4, _YLOC("Add users to the rejected senders list for the selected groups. You will be able to select users from a directory list.")).c_str(), _cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_AUTHORSGRID_ADDACCEPTED;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_5, _YLOC("Add Accepted...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_6, _YLOC("Add Accepted...")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_ADDAUTHORSACCEPTED_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_AUTHORSGRID_ADDACCEPTED, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_7, _YLOC("Add Accepted Senders")).c_str(), YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_8, _YLOC("Add users to the accepted senders list for the selected groups. You will be able to select users from a directory list.")).c_str(), _cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_AUTHORSGRID_DELETEAUTHORIZATION;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_9, _YLOC("Delete Restriction")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_10, _YLOC("Delete Restriction")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_REMOVEAUTHORIZATION_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_AUTHORSGRID_DELETEAUTHORIZATION, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_11, _YLOC("Delete Message Delivery Restriction")).c_str(), YtriaTranslate::Do(GridGroupsAuthorsHierarchy_InitializeCommands_12, _YLOC("Delete all selected group message delivery restrictions from the list.")).c_str(), _cmd.m_sAccelText);
	}

	GridGroupsCommon::InitializeCommands();
}

void GridGroupsAuthorsHierarchy::OnAddAcceptedToGroups()
{
	if (IsFrameConnected())
	{
		// FIXME: handle roles if not done in showDlgItemsToGroup
		showDlgItemsToGroup(BusinessObject::UPDATE_TYPE_ADDAUTHORACCEPTED);
	}
}

void GridGroupsAuthorsHierarchy::OnAddRejectedToGroups()
{
	if (IsFrameConnected())
	{
		// FIXME: handle roles if not done in showDlgItemsToGroup
		showDlgItemsToGroup(BusinessObject::UPDATE_TYPE_ADDAUTHORREJECTED);
	}
}

void GridGroupsAuthorsHierarchy::OnUpdateAddItemsToGroups(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& IsFrameConnected()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& p_Row)
			{
				return nullptr != p_Row && !p_Row->IsGroupRow()
					&& IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis(), RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_DELIVERYMANAGEMENT_EDIT);
			}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsAuthorsHierarchy::OnRemoveItemsFromGroups()
{
	// FIXME: handle roles if not done in removeSelectedItemsFromGroup
	removeSelectedItemsFromGroup(BusinessObject::UPDATE_TYPE_DELETEAUTHORIZATION);
}

void GridGroupsAuthorsHierarchy::OnUpdateRemoveItemsFromGroups(CCmdUI* pCmdUI)
{
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_DELIVERYMANAGEMENT_EDIT_OUT_OF_SCOPE);

	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, hasOutOfScopePrivilege](auto& p_Row)
		{
			auto ref = (nullptr != p_Row && nullptr != p_Row->GetParentRow()) ? p_Row->GetAncestorAtLevel(1) : nullptr;
			return nullptr != ref
				&& !p_Row->IsGroupRow()
				&& !ref->IsDeleted()
				&& IsGroupAuthorizedByRoleDelegation(ref->GetTopAncestor(), RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_DELIVERYMANAGEMENT_EDIT)
				&& (hasOutOfScopePrivilege
					|| GridUtil::IsBusinessOrgContact(ref)
					|| IsUserAuthorizedByRoleDelegation(ref, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)
					|| IsGroupAuthorizedByRoleDelegation(ref, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsAuthorsHierarchy::addFieldSpecific(GridBackendRow* p_pRow, BusinessObject::UPDATE_TYPE p_UpdateType)
{
	if (p_UpdateType == BusinessObject::UPDATE_TYPE_ADDAUTHORACCEPTED)
	{
		setRowAuhorization(p_pRow, true);
	}
	else if (p_UpdateType == BusinessObject::UPDATE_TYPE_ADDAUTHORREJECTED)
	{
		setRowAuhorization(p_pRow, false);
	}
	else if (p_UpdateType == BusinessObject::UPDATE_TYPE_DELETEAUTHORIZATION)
	{
		const auto icon = p_pRow->GetField(m_ColumnAcceptedStatus).GetIcon();

		if (icon == m_IconIDauthorizationAccepted || icon == m_IconIDauthorizationAcceptedAll)
		{
			p_pRow->SetLParam(BusinessObject::UPDATE_TYPE_DELETEACCEPTEDAUTHORS);
		}
		else if (icon == m_IconIDauthorizationRestricted || icon == m_IconIDauthorizationRejected)
		{
			p_pRow->SetLParam(BusinessObject::UPDATE_TYPE_DELETEREJECTEDAUTHORS);
		}
		else
		{
			ASSERT(FALSE);
			p_pRow->SetLParam(BusinessObject::UPDATE_TYPE_NONE);
		}
	}
	else
	{
		GridGroupsCommon::addFieldSpecific(p_pRow, p_UpdateType);
	}
}

void GridGroupsAuthorsHierarchy::AddGroups(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge)
{
	const uint32_t options	= GridUpdaterOptions::UPDATE_GRID
							| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
							| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
							| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
							/*| GridUpdaterOptions::REFRESH_PROCESS_DATA*/
							/*| GridUpdaterOptions::PROCESS_LOADMORE*/;
	GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateOperations);
    GridIgnoreModification ignoramus(*this);

	bool oneCanceled = false;
	bool oneOK = false;
	for (const auto& group : p_Groups)
	{
		if (m_HasLoadMoreAdditionalInfo)
		{
			if (group.IsMoreLoaded())
				m_MetaIDsMoreLoaded.insert(group.GetID());
			else
				m_MetaIDsMoreLoaded.erase(group.GetID());
		}

		auto row = addRow(group, updater);
		ASSERT(nullptr != row);
        if (nullptr != row)
        {
			updater.GetOptions().AddRowWithRefreshedValues(row);
			if (group.HasFlag(BusinessObject::CANCELED))
			{
				oneCanceled = true;
			}
			else
			{
				oneOK = true;
				updater.GetOptions().AddPartialPurgeParentRow(row);
			}

			AddRoleDelegationFlag(row, group);
        }
	}

	if (oneCanceled)
	{
		if (oneOK)
			updater.GetOptions().SetPurgeFlag(GridUpdaterOptions::PARTIALPURGE);
		else
			updater.GetOptions().SetPurgeFlag(0); // Everything canceled, don't purge anything.
	}
}

GridBackendRow* GridGroupsAuthorsHierarchy::addRow(const BusinessGroup& p_Group, GridUpdater& p_Updater)
{
	GridBackendRow* row = m_TemplateGroups.AddRow(*this, p_Group, { m_TemplateUsers.GetMetaIDField(PooledString()) }, p_Updater, false, nullptr, {}, true);

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		row->SetHierarchyParentToHide(true);

		if (p_Group.HasFlag(BusinessObject::CANCELED))
		{
			// Could be uncommented if we notice missing explosion refresh on canceled rows
			//auto children = GetChildRows(row);
			//for (auto child : children)
			//	p_Updater.GetOptions().AddRowWithRefreshedValues(child);
		}
		else
		{
			// Clear previous error
			if (row->IsError())
				row->ClearStatus();

			vector<GridBackendField> BGK{ m_TemplateUsers.GetMetaIDField(p_Group.GetID()) };

			bool allAccepted = true;
			for (const auto& authorID : p_Group.GetAuthorAcceptedUsers())
			{
				allAccepted = false;

				const auto author = BusinessUser(GetGraphCache().GetUserInCache(authorID, false));
				ASSERT(author.GetID() == authorID);

				GridBackendRow* childRow = m_TemplateUsers.AddRow(*this, author, BGK, p_Updater, false, false);
				ASSERT(nullptr != childRow);
				if (nullptr != childRow)
				{
					p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);
					childRow->SetParentRow(row);

					setRowAuhorization(childRow, true);

					for (auto c : m_MetaColumns)
					{
						if (row->HasField(c))
							childRow->AddField(row->GetField(c));
						else
							childRow->RemoveField(c);
					}
					childRow->AddField(row->GetField(m_TemplateGroups.m_ColumnLastRequestDateTime).GetValueTimeDate(), m_TemplateGroups.m_ColumnLastRequestDateTime);

					AddRoleDelegationFlag(childRow, author);
				}
			}

			for (const auto& authorID : p_Group.GetAuthorRejectedUsers())
			{
				allAccepted = false;

				const auto author = BusinessUser(GetGraphCache().GetUserInCache(authorID, false));
				ASSERT(author.GetID() == authorID);

				GridBackendRow* childRow = m_TemplateUsers.AddRow(*this, author, BGK, p_Updater, false, false);
				ASSERT(nullptr != childRow);
				if (nullptr != childRow)
				{
					p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);
					childRow->SetParentRow(row);

					setRowAuhorization(childRow, false);

					for (auto c : m_MetaColumns)
					{
						if (row->HasField(c))
							childRow->AddField(row->GetField(c));
						else
							childRow->RemoveField(c);
					}
					childRow->AddField(row->GetField(m_TemplateGroups.m_ColumnLastRequestDateTime).GetValueTimeDate(), m_TemplateGroups.m_ColumnLastRequestDateTime);

					AddRoleDelegationFlag(childRow, author);
				}
			}

			for (const auto& authorID : p_Group.GetAuthorAcceptedOrgContacts())
			{
				allAccepted = false;

				const auto author = BusinessOrgContact(GetGraphCache().GetCachedOrgContact(authorID));
				ASSERT(author.GetID() == authorID);

				auto contactPK = BGK;
				contactPK.emplace_back(author.GetID(), GetColId());

				auto childRow = GetRowIndex().GetRow(contactPK, true, (p_Updater.GetOptions().IsFullPurge() || p_Updater.GetOptions().IsPartialPurge()));
				ASSERT(nullptr != childRow);
				if (nullptr != childRow)
				{
					SetRowObjectType(childRow, author, author.HasFlag(BusinessObject::CANCELED));
					childRow->AddField(author.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
					childRow->AddField(author.GetMail(), m_TemplateUsers.m_ColumnUserPrincipalName);

					p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);
					childRow->SetParentRow(row);

					setRowAuhorization(childRow, true);

					for (auto c : m_MetaColumns)
					{
						if (row->HasField(c))
							childRow->AddField(row->GetField(c));
						else
							childRow->RemoveField(c);
					}
					childRow->AddField(row->GetField(m_TemplateGroups.m_ColumnLastRequestDateTime).GetValueTimeDate(), m_TemplateGroups.m_ColumnLastRequestDateTime);
				}
			}

			for (const auto& authorID : p_Group.GetAuthorRejectedOrgContacts())
			{
				allAccepted = false;

				const auto author = BusinessOrgContact(GetGraphCache().GetCachedOrgContact(authorID));
				ASSERT(author.GetID() == authorID);

				auto contactPK = BGK;
				contactPK.emplace_back(author.GetID(), GetColId());

				auto childRow = GetRowIndex().GetRow(contactPK, true, (p_Updater.GetOptions().IsFullPurge() || p_Updater.GetOptions().IsPartialPurge()));
				ASSERT(nullptr != childRow);
				if (nullptr != childRow)
				{
					SetRowObjectType(childRow, author, author.HasFlag(BusinessObject::CANCELED));
					childRow->AddField(author.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
					childRow->AddField(author.GetMail(), m_TemplateUsers.m_ColumnUserPrincipalName);

					p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);
					childRow->SetParentRow(row);

					setRowAuhorization(childRow, false);

					for (auto c : m_MetaColumns)
					{
						if (row->HasField(c))
							childRow->AddField(row->GetField(c));
						else
							childRow->RemoveField(c);
					}
					childRow->AddField(row->GetField(m_TemplateGroups.m_ColumnLastRequestDateTime).GetValueTimeDate(), m_TemplateGroups.m_ColumnLastRequestDateTime);
				}
			}

			for (auto& authorID : p_Group.GetAuthorAcceptedGroups())
			{
				allAccepted = false;

				auto author = BusinessGroup(GetGraphCache().GetGroupInCache(authorID));
				ASSERT(author.GetID() == authorID);

				GetGraphCache().GetGroupMembers(author);

				m_BufferHierarchyGroupIDs = { p_Group.GetID() };
				GridBackendRow* childRow = addRowGroupMembersHierarchy(row, p_Group.HasFlag(BusinessObject::CANCELED), author, true, p_Updater);
				ASSERT(nullptr != childRow);
				if (nullptr != childRow)
					p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);
			}

			for (auto& authorID : p_Group.GetAuthorRejectedGroups())
			{
				allAccepted = false;

				auto author = BusinessGroup(GetGraphCache().GetGroupInCache(authorID));
				ASSERT(author.GetID() == authorID);

				GetGraphCache().GetGroupMembers(author);

				m_BufferHierarchyGroupIDs = { p_Group.GetID() };
				GridBackendRow* childRow = addRowGroupMembersHierarchy(row, p_Group.HasFlag(BusinessObject::CANCELED), author, false, p_Updater);
				ASSERT(nullptr != childRow);
				if (nullptr != childRow)
					p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);
			}

			GridUtil::HandleMultiError(row
				,	{ { p_Group.GetAuthorsAcceptedError(), _YFORMAT(L"Accepted Authors: %s", _YTEXT("%s")) }
					, { p_Group.GetAuthorsRejectedError(), _YFORMAT(L"Rejected Authors: %s", _YTEXT("%s")) }
					}
				,	p_Updater);

			ASSERT(!p_Group.HasFlag(BusinessObject::CANCELED));

			if (!row->IsError())
			{
				if (allAccepted)
					row->AddField(YtriaTranslate::Do(GridGroupsAuthorsHierarchy_addRow_4, _YLOC("Everybody has been accepted")).c_str(), m_ColumnAcceptedStatus, m_IconIDauthorizationAcceptedAll);// kumbaya
				else
					row->AddField(YtriaTranslate::Do(GridGroupsAuthorsHierarchy_addRow_5, _YLOC("Restricted access, see below:")).c_str(), m_ColumnAcceptedStatus, m_IconIDauthorizationRestricted);
			}
		}
	}

	return row;
}

void GridGroupsAuthorsHierarchy::setRowAuhorization(GridBackendRow* p_ChildRow, const bool p_Accepted)
{
	ASSERT(nullptr != p_ChildRow);
	if (nullptr != p_ChildRow)
	{
		if (p_Accepted)
			p_ChildRow->AddField(g_AcceptedStatus, m_ColumnAcceptedStatus, m_IconIDauthorizationAccepted);
		else
			p_ChildRow->AddField(g_RejectedStatus, m_ColumnAcceptedStatus, m_IconIDauthorizationRejected);
	}
}

GridBackendRow* GridGroupsAuthorsHierarchy::addRowGroupMembersHierarchy(GridBackendRow* p_ParentRow, bool p_TopLevelIsCanceled, const BusinessGroup& p_GroupForMembership, const bool p_Accepted, GridUpdater& p_Updater)
{
	vector<GridBackendField> BGK = { m_TemplateUsers.GetMetaIDField(m_BufferHierarchyGroupIDs) };
	GridBackendRow* row = m_TemplateGroups.AddRow(*this, p_GroupForMembership, BGK, p_Updater, false, nullptr, {}, false);
	m_BufferHierarchyGroupIDs.push_back(p_GroupForMembership.GetID());
	
	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		setRowAuhorization(row, p_Accepted);

		for (auto c : m_MetaColumns)
		{
			if (p_ParentRow->HasField(c))
				row->AddField(p_ParentRow->GetField(c));
			else
				row->RemoveField(c);
		}

		if (p_ParentRow->HasField(m_TemplateGroups.m_ColumnLastRequestDateTime))
			row->AddField(p_ParentRow->GetField(m_TemplateGroups.m_ColumnLastRequestDateTime));

		row->AddField(p_GroupForMembership.GetDisplayName(), m_ColumnGroupNameForMember);
		row->AddField(p_GroupForMembership.GetCombinedGroupType(), m_ColumnCombinedGroupTypeForMember);
		GridUtil::SetRowIsTeam(p_GroupForMembership.GetValue(_YUID(O365_GROUP_ISTEAM)), row, m_ColumnIsTeamForMember, m_TemplateGroups.m_IconIsTeam);

		if (p_TopLevelIsCanceled)
		{
			// Could be uncommented if we notice missing explosion refresh on canceled rows
			//auto children = GetChildRows(row);
			//for (auto child : children)
			//	p_Updater.GetOptions().AddRowWithRefreshedValues(child);
		}
		else
		{
			if (p_GroupForMembership.GetError())
			{
				p_Updater.OnLoadingError(row, *p_GroupForMembership.GetError());
			}
			else
			{
				// Clear previous error
				if (row->IsError())
					row->ClearStatus();

				vector<GridBackendField> BUK = { m_TemplateUsers.GetMetaIDField(m_BufferHierarchyGroupIDs) };
				for (const auto& childUserID : p_GroupForMembership.GetChildrenUsers())
				{
					auto child = BusinessUser(GetGraphCache().GetUserInCache(childUserID, false));
					ASSERT(child.GetID() == childUserID);
					GridBackendRow* childRow = m_TemplateUsers.AddRow(*this, child, BUK, p_Updater, false, false);
					ASSERT(nullptr != childRow);
					if (nullptr != childRow)
					{
						childRow->SetParentRow(row);

						for (auto c : m_MetaColumns)
						{
							if (row->HasField(c))
								childRow->AddField(row->GetField(c));
							else
								childRow->RemoveField(c);
						}

						if (row->HasField(m_TemplateGroups.m_ColumnLastRequestDateTime))
							childRow->AddField(row->GetField(m_TemplateGroups.m_ColumnLastRequestDateTime));

						childRow->AddField(p_GroupForMembership.GetDisplayName(), m_ColumnGroupNameForMember);

						childRow->AddField(p_GroupForMembership.GetCombinedGroupType(), m_ColumnCombinedGroupTypeForMember);

						GridUtil::SetRowIsTeam(p_GroupForMembership.GetValue(_YUID(O365_GROUP_ISTEAM)), childRow, m_ColumnIsTeamForMember, m_TemplateGroups.m_IconIsTeam);

						setRowAuhorization(childRow, p_Accepted);

						AddRoleDelegationFlag(childRow, child);
					}
				}
				for (const auto& childOrgContactId : p_GroupForMembership.GetChildrenOrgContacts())
				{
					auto child = BusinessOrgContact(GetGraphCache().GetCachedOrgContact(childOrgContactId));
					ASSERT(child.GetID() == childOrgContactId);
					auto contactPK = BGK;
					contactPK.emplace_back(child.GetID(), GetColId());
					contactPK.insert(contactPK.end(), BUK.begin(), BUK.end());

					auto childRow = GetRowIndex().GetRow(contactPK, true, (p_Updater.GetOptions().IsFullPurge() || p_Updater.GetOptions().IsPartialPurge()));
					ASSERT(nullptr != childRow);
					if (nullptr != childRow)
					{
						SetRowObjectType(childRow, child, child.HasFlag(BusinessObject::CANCELED));
						childRow->AddField(child.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
						childRow->AddField(child.GetMail(), m_TemplateUsers.m_ColumnUserPrincipalName);

						p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);

						childRow->SetParentRow(row);

						for (auto c : m_MetaColumns)
						{
							if (row->HasField(c))
								childRow->AddField(row->GetField(c));
							else
								childRow->RemoveField(c);
						}

						if (row->HasField(m_TemplateGroups.m_ColumnLastRequestDateTime))
							childRow->AddField(row->GetField(m_TemplateGroups.m_ColumnLastRequestDateTime));

						childRow->AddField(p_GroupForMembership.GetDisplayName(), m_ColumnGroupNameForMember);
						childRow->AddField(p_GroupForMembership.GetCombinedGroupType(), m_ColumnCombinedGroupTypeForMember);
						GridUtil::SetRowIsTeam(p_GroupForMembership.GetValue(_YUID(O365_GROUP_ISTEAM)), childRow, m_ColumnIsTeamForMember, m_TemplateGroups.m_IconIsTeam);

						setRowAuhorization(childRow, p_Accepted);
					}
				}
				for (const auto& childGroupId : p_GroupForMembership.GetChildrenGroups())
				{
					auto child = BusinessGroup(GetGraphCache().GetGroupInCache(childGroupId));
					ASSERT(child.GetID() == childGroupId);

					GetGraphCache().GetGroupMembers(child);

					addRowGroupMembersHierarchy(row, p_TopLevelIsCanceled, child, p_Accepted, p_Updater);
				}
			}
		}

		row->SetParentRow(p_ParentRow);
		AddRoleDelegationFlagFromParent(row);
	}

	return row;
}