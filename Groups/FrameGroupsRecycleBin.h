#pragma once

#include "GridFrameBase.h"
#include "GridGroupsRecycleBin.h"

class FrameGroupsRecycleBin : public GridFrameBase
{
public:
    DECLARE_DYNAMIC(FrameGroupsRecycleBin)
	using GridFrameBase::GridFrameBase;
    virtual ~FrameGroupsRecycleBin() override = default;

	void ShowGroupsRecycleBin(const vector<BusinessGroup>& p_Groups, const vector<BusinessGroup>& p_LoadedMoreGroups, bool p_Refresh, const vector<O365UpdateOperation>& p_UpdateOperations);

	void UpdateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh);

	virtual O365Grid& GetGrid() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction);
    void ApplySpecific(bool p_Selected);
	virtual void ApplyAllSpecific() override;
    virtual void ApplySelectedSpecific() override;

	virtual bool HasApplyButton() override;

protected:
	virtual void createGrid() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	GridGroupsRecycleBin m_groupsGrid;
};

