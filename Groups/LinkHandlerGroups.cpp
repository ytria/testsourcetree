#include "LinkHandlerGroups.h"

#include "Command.h"
#include "CommandDispatcher.h"

void LinkHandlerGroups::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::MainModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Group, Command::ModuleTask::List, info, { nullptr, g_ActionNameShowGroups, nullptr, p_Link.GetAutomationAction() }));
}

// =================================================================================================

void LinkHandlerPrefilteredGroups::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	// New automation name?
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::MainModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Group, Command::ModuleTask::ListWithPrefilter, info, { nullptr, g_ActionNameShowGroups, nullptr, p_Link.GetAutomationAction() }));
}

// =================================================================================================

void LinkHandlerGroupsRecycleBin::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	info.SetOrigin(Origin::Tenant);
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Group, Command::ModuleTask::ListRecycleBin, info, { nullptr, g_ActionNameShowGroupsRecycleBin, nullptr, p_Link.GetAutomationAction() }));
}
