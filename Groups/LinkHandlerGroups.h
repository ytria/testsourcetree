#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerGroups : public IBrowserLinkHandler
{
public:
    virtual void Handle(YBrowserLink& p_Link) const override;
};

// =================================================================================================

class LinkHandlerPrefilteredGroups : public IBrowserLinkHandler
{
public:
	virtual void Handle(YBrowserLink& p_Link) const override;
};

// =================================================================================================

class LinkHandlerGroupsRecycleBin : public IBrowserLinkHandler
{
public:
	virtual void Handle(YBrowserLink& p_Link) const override;
};
