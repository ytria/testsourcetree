#include "GridGroupsRecycleBin.h"

#include "AutomationWizardGroupsRecycleBin.h"
#include "BasicGridSetup.h"
#include "BusinessGroupConfiguration.h"
#include "FrameGroupsRecycleBin.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"
#include "RestoreObjectModification.h"
#include "UpdatedObjectsGenerator.h"

BEGIN_MESSAGE_MAP(GridGroupsRecycleBin, O365Grid)
	ON_COMMAND(ID_GROUPSRECYCLEBIN_RESTORE,					OnRestore)
	ON_UPDATE_COMMAND_UI(ID_GROUPSRECYCLEBIN_RESTORE,		OnUpdateRestore)
	ON_COMMAND(ID_GROUPSRECYCLEBIN_HARDDELETE,				OnHardDelete)
	ON_UPDATE_COMMAND_UI(ID_GROUPSRECYCLEBIN_HARDDELETE,	OnUpdateHardDelete)
	ON_COMMAND(ID_GROUPSRECYCLEBIN_LOADMORE,				OnLoadMore)
	ON_UPDATE_COMMAND_UI(ID_GROUPSRECYCLEBIN_LOADMORE,		OnUpdateLoadMore)
END_MESSAGE_MAP()

GridGroupsRecycleBin::GridGroupsRecycleBin()
	: m_Frame(nullptr)
{
	m_MayContainUnscopedUserGroupOrSite = true;

	EnableGridModifications();

	initWizard<AutomationWizardGroupsRecycleBin>(_YTEXT("Automation\\GroupsRecycleBin"));
}

GridGroupsRecycleBin::~GridGroupsRecycleBin()
{}

void GridGroupsRecycleBin::BuildView(vector<BusinessGroup> p_Groups, const vector<BusinessGroup>& p_LoadedMoreGroups, bool p_Refresh, const vector<O365UpdateOperation>& p_UpdateOperations)
{
	// p_Groups must also contain the p_LoadedMoreGroups groups. (test GetCreatedDateTime() equality as it's a load more property)
	ASSERT(p_LoadedMoreGroups.empty()
		|| !p_Groups.empty() && p_Groups.end() != std::find_if(p_Groups.begin(), p_Groups.end(), [&p_LoadedMoreGroups](const BusinessGroup& bg) {return bg.GetID() == p_LoadedMoreGroups.front().GetID() && bg.GetCreatedDateTime() == p_LoadedMoreGroups.front().GetCreatedDateTime(); }));

	if (p_Groups.size() == 1 && p_Groups[0].GetError())
	{
		HandlePostUpdateError(p_Groups[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_Groups[0].GetError()->GetFullErrorMessage());
	}
	else
	{
		boost::YOpt<GridUpdater::PostUpdateError> unprocessedPostUpdateError;

		{
			const auto fullPurge = p_UpdateOperations.empty();
			const auto willProceedLoadMore = p_Refresh && !p_LoadedMoreGroups.empty();
			const uint32_t options = GridUpdaterOptions::UPDATE_GRID
				| (fullPurge ? GridUpdaterOptions::FULLPURGE : 0)
				| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
				| (!willProceedLoadMore ? GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS : 0)
				| GridUpdaterOptions::REFRESH_PROCESS_DATA
				/*| GridUpdaterOptions::PROCESS_LOADMORE*/;
			GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateOperations);

			if (willProceedLoadMore)
			{
				unprocessedPostUpdateError.emplace();
				updater.SetPostUpdateErrorBackupTarget(&*unprocessedPostUpdateError);
			}

			GridIgnoreModification ignoramus(*this);

			// Update only groups on which we applied modifications (in case we did apply some)
			if (!p_UpdateOperations.empty())
				RemoveGroupsNotModified(p_Groups, p_UpdateOperations);

			for (const auto& bg : p_Groups)
			{
				auto row = m_TemplateGroups.AddRow(*this, bg, vector<GridBackendField>(), updater, false, nullptr, {}, false);
				ASSERT(nullptr != row);
				if (nullptr != row)
				{
					AddRoleDelegationFlag(row, bg);
					updater.GetOptions().AddRowWithRefreshedValues(row);
				}
			}
		}

		if (!p_LoadedMoreGroups.empty())
		{
			ASSERT(p_Refresh);
			updateGroupsLoadMore(p_LoadedMoreGroups, p_Refresh, !p_UpdateOperations.empty(), unprocessedPostUpdateError);
		}
	}
}

void GridGroupsRecycleBin::UpdateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh, bool p_DoNotShowLoadingErrors)
{
	updateGroupsLoadMore(p_Groups, p_FromRefresh, p_DoNotShowLoadingErrors, boost::none);
}

void GridGroupsRecycleBin::UpdateGroupLCPStatus(const vector<BusinessGroup>& p_Groups)
{
	GridIgnoreModification ignoramus(*this);
	vector<GridBackendRow*> rows;
	GetAllNonGroupRows(rows);
	for (GridBackendRow* row : rows)
	{
		PooledString id = row->GetField(m_TemplateGroups.m_ColumnID).GetValueStr();
		if (isOffice365Group(row))
		{
			for (const auto& bg : p_Groups)
			{
				if (bg.GetID() == id)
				{
					updateGroupLCPStatus(row, bg);
					break;
				}
			}
		}
	}

	UpdateMegaSharkOptimized(vector<GridBackendColumn*>{ m_TemplateGroups.m_ColumnExpirationPolicyStatus });
}

void GridGroupsRecycleBin::OnRestore()
{
	TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool p_HasRowsToReExplode)
	{
		std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_RECYCLEBIN_MANAGE, [](GridBackendRow* p_Row)
		{
			return nullptr != p_Row && !p_Row->IsGroupRow();
		});

		ASSERT(!selectedRows.empty());
		if (!selectedRows.empty())
		{
			bool update = false;
			for (auto selectedRow : selectedRows)
			{
				ASSERT(nullptr != selectedRow);
				if (nullptr != selectedRow)
				{
					row_pk_t rowPk;
					GetRowPK(selectedRow, rowPk);

					ASSERT(!selectedRow->IsCreated());
					auto hardDeleteMod = GetModifications().GetRowModification<DeletedObjectModification>(rowPk);
					if (nullptr != hardDeleteMod)
						GetModifications().Revert(rowPk, false);
					GetModifications().Add(std::make_unique<RestoreObjectModification>(*this, rowPk));
					update = true;
				}
			}

			if (update && !p_HasRowsToReExplode)
				UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		}
	})();
}

void GridGroupsRecycleBin::OnUpdateRestore(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_RECYCLEBIN_MANAGE, [](GridBackendRow* p_Row)
		{
			return nullptr != p_Row && !p_Row->IsGroupRow();
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsRecycleBin::OnHardDelete()
{
	TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool p_HasRowsToReExplode)
	{
		std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_RECYCLEBIN_MANAGE, [](GridBackendRow* p_Row)
		{
			return nullptr != p_Row && !p_Row->IsGroupRow();
		});

		ASSERT(!selectedRows.empty());
		if (!selectedRows.empty())
		{
			bool update = false;
			for (auto selectedRow : selectedRows)
			{
				ASSERT(nullptr != selectedRow);
				if (nullptr != selectedRow)
				{
					row_pk_t rowPk;
					GetRowPK(selectedRow, rowPk);

					ASSERT(!selectedRow->IsCreated());
					auto restoreMod = GetModifications().GetRowModification<RestoreObjectModification>(rowPk);
					if (nullptr != restoreMod)
						GetModifications().Revert(rowPk, false);
					GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, rowPk));
					update = true;
				}
			}

			if (update && !p_HasRowsToReExplode)
				UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		}
	})();
}

void GridGroupsRecycleBin::OnUpdateHardDelete(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_RECYCLEBIN_MANAGE, [](GridBackendRow* p_Row)
		{
			return nullptr != p_Row && !p_Row->IsGroupRow();
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsRecycleBin::OnLoadMore()
{
	std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_GROUP_LOADMORE, [this](GridBackendRow* p_Row)
		{
			return IsRowForLoadMore(p_Row, nullptr);
		});

	if (!selectedRows.empty() && showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT))
		LoadMoreImpl(selectedRows);
}

void GridGroupsRecycleBin::OnUpdateLoadMore(CCmdUI* pCmdUI)
{
	const auto condition = [this](GridBackendRow* p_Row)
	{
		return IsRowForLoadMore(p_Row, nullptr);
	};

	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_GROUP_LOADMORE, condition));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsRecycleBin::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart /*= 0*/)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		if (GridUtil::IsSelectionAtLeastOneOfType<BusinessGroup>(*this))
		{
			pPopup->ItemInsert(ID_GROUPSRECYCLEBIN_RESTORE);
			pPopup->ItemInsert(ID_GROUPSRECYCLEBIN_HARDDELETE);
			pPopup->ItemInsert(); // Sep
		}

		// Bypass O365Grid as we don't want default group actions.
		CacheGrid::OnCustomPopupMenu(pPopup, nStart);
	}
}

void GridGroupsRecycleBin::customizeGrid()
{
	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_GRIDGROUPSRECYCLEBIN_ACCELERATOR));

	{
		BasicGridSetup setup(GridUtil::g_AutoNameGroupsRecycleBin, LicenseUtil::g_codeSapio365groups);
		setup.Setup(*this, false);
	}

	const auto& gcfg = BusinessGroupConfiguration::GetInstance();

	static const wstring g_FamilyTeam = YtriaTranslate::Do(GridGroups_customizeGrid_54, _YLOC("Team")).c_str();

	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_DISPLAYNAME),									g_FamilyInfo,	{ g_ColumnsPresetDefault });
	addColumnIsLoadMore(_YUID("additionalInfoStatus"), YtriaTranslate::Do(GridGroups_customizeGrid_2, _YLOC("Is Loaded")).c_str());
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_ISTEAM),										g_FamilyInfo,	{ g_ColumnsPresetDefault });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("isArchived"),											g_FamilyTeam,	{ g_ColumnsPresetMore, g_ColumnsPresetDefault });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_GROUPTYPE),									g_FamilyInfo,	{ g_ColumnsPresetDefault });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_DYNAMICMEMBERSHIP),							g_FamilyInfo,	{ g_ColumnsPresetDefault });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_DESCRIPTION),									g_FamilyInfo,	{ g_ColumnsPresetDefault });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_VISIBILITY),									g_FamilyInfo,	{ g_ColumnsPresetDefault });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_CREATEDDATETIME),								g_FamilyInfo,	{ g_ColumnsPresetDefault });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_RENEWEDDATETIME),								g_FamilyInfo,	{ g_ColumnsPresetDefault });
	
	m_TemplateGroups.m_ColumnDeletedDateTime		= AddColumnDate(_YUID(O365_GROUP_DELETEDDATETIME), gcfg.GetTitle(_YUID(O365_GROUP_DELETEDDATETIME)),					g_FamilyInfo, g_ColumnSize16,	{ g_ColumnsPresetDefault });
	
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILNICKNAME),									g_FamilyInfo,	{});
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_USER_MAIL),											g_FamilyInfo,	{ g_ColumnsPresetDefault });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_CLASSIFICATION),								g_FamilyInfo,	{});
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_PROXYADDRESSES),								g_FamilyInfo,	{});
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_PREFERREDDATALOCATION),						g_FamilyInfo,	{});

	static const wstring g_FamilyOnPremises = YtriaTranslate::Do(GridGroupsRecycleBin_customizeGrid_12, _YLOC("OnPremises"));
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_USER_ONPREMISESLASTSYNCDATETIME),					g_FamilyOnPremises,	{});
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER),					g_FamilyOnPremises,	{ g_ColumnsPresetTech });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_USER_ONPREMISESSYNCENABLED),							g_FamilyOnPremises,	{});
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY),				g_FamilyOnPremises,	{});
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME),		g_FamilyOnPremises,	{});
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR),	g_FamilyOnPremises,	{});
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE),					g_FamilyOnPremises,	{});

	static const wstring g_FamilyTypeInfo = YtriaTranslate::Do(GridGroupsRecycleBin_customizeGrid_16, _YLOC("Type Info"));
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_GROUPTYPES),									g_FamilyTypeInfo,	{});
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_SECURITYENABLED),								g_FamilyTypeInfo,	{});
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID(O365_GROUP_MAILENABLED),									g_FamilyTypeInfo,	{});

	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("expirationPolicyStatus"),								g_FamilyInfo,		{ g_ColumnsPresetMore, g_ColumnsPresetDefault });

	{
		static const wstring g_FamilyTeamMemberSettings = YtriaTranslate::Do(GridGroups_customizeGrid_34, _YLOC("Team Member Settings")).c_str();
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("teamMemberSettingsAllowCreateUpdateChannels"),			g_FamilyTeamMemberSettings,	{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("teamMemberSettingsAllowDeleteChannels"),					g_FamilyTeamMemberSettings,	{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("teamMemberSettingsAllowAddRemoveApps"),					g_FamilyTeamMemberSettings,	{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("teamMemberSettingsAllowCreateUpdateRemoveTabs"),			g_FamilyTeamMemberSettings,	{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("teamMemberSettingsAllowCreateUpdateRemoveConnectors"),	g_FamilyTeamMemberSettings,	{ g_ColumnsPresetMore });
	}

	{
		static const wstring g_FamilyTeamGuestSettings = YtriaTranslate::Do(GridGroups_customizeGrid_40, _YLOC("Team Guest Settings")).c_str();
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("guestSettingsAllowCreateUpdateChannels"),				g_FamilyTeamGuestSettings,	{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("guestSettingsAllowDeleteChannels"),						g_FamilyTeamGuestSettings,	{ g_ColumnsPresetMore });
	}

	{
		static const wstring g_FamilyTeamMessagingSettings = YtriaTranslate::Do(GridGroups_customizeGrid_43, _YLOC("Team Messaging Settings")).c_str();
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("messagingSettingsAllowUserEditMessages"),				g_FamilyTeamMessagingSettings,	{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("messagingSettingsAllowUserDeleteMessages"),				g_FamilyTeamMessagingSettings,	{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("messagingSettingsAllowOwnerDeleteMessages"),				g_FamilyTeamMessagingSettings,	{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("messagingSettingsAllowTeamMentions"),					g_FamilyTeamMessagingSettings,	{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("messagingSettingsAllowChannelMentions"),					g_FamilyTeamMessagingSettings,	{ g_ColumnsPresetMore });
	}

	{
		static const wstring g_FamilyTeamFunSettings = YtriaTranslate::Do(GridGroups_customizeGrid_49, _YLOC("Team Fun Settings")).c_str();
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("funSettingsAllowGiphy"),									g_FamilyTeamFunSettings,		{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("funSettingsGiphyContentRating"),							g_FamilyTeamFunSettings,		{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("funSettingsAllowStickersAndMemes"),						g_FamilyTeamFunSettings,		{ g_ColumnsPresetMore });
		m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("funSettingsAllowCustomMemes"),							g_FamilyTeamFunSettings,		{ g_ColumnsPresetMore });
	}

	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("webUrl"),												g_FamilyTeam,	{ g_ColumnsPresetMore });
	m_TemplateGroups.AddDefaultColumnFor(*this, _YUID("internalId"),											g_FamilyTeam,	{ g_ColumnsPresetMore, g_ColumnsPresetTech });

	addColumnGraphID();

	AddColumnForRowPK(GetColId());
	m_TemplateGroups.m_ColumnID = GetColId();
	m_TemplateGroups.CustomizeGrid(*this);

	AddSorting(m_TemplateGroups.m_ColumnDisplayName, GridBackendUtil::ASC);

	m_Frame = dynamic_cast<FrameGroupsRecycleBin*>(GetParentFrame());

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_Frame);
}

wstring GridGroupsRecycleBin::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, m_TemplateGroups.m_ColumnDisplayName);
}

vector<BusinessGroup> GridGroupsRecycleBin::GetLoadMoreRequestInfo(const std::vector<GridBackendRow*>& p_Rows)
{
	vector<BusinessGroup> groups;
	for (auto row : p_Rows)
	{
		if (IsRowForLoadMore(row, nullptr))
		{
			BusinessGroup businessGroup;
			m_TemplateGroups.GetBusinessGroup(*this, row, businessGroup);

			// avoid useless queries (multivalue explosion side effect)
			if (std::none_of(groups.begin(), groups.end(), [id = businessGroup.GetID()](const BusinessGroup& bg){return id == bg.GetID(); }))
				groups.push_back(businessGroup);
		}
	}

	return groups;
}

void GridGroupsRecycleBin::InitializeCommands()
{
	O365Grid::InitializeCommands();

	std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

	const auto profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_GROUPSRECYCLEBIN_RESTORE;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsRecycleBin_InitializeCommands_1, _YLOC("Restore Selected")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsRecycleBin_InitializeCommands_2, _YLOC("Restore Selected")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_RESTORE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_GROUPSRECYCLEBIN_RESTORE, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridGroupsRecycleBin_InitializeCommands_3, _YLOC("Restore Selected Groups")).c_str(),
			YtriaTranslate::Do(GridGroupsRecycleBin_InitializeCommands_4, _YLOC("Restore Selected Groups.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_GROUPSRECYCLEBIN_HARDDELETE;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsRecycleBin_InitializeCommands_5, _YLOC("Permanently Delete")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsRecycleBin_InitializeCommands_6, _YLOC("Permanently Delete")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_HARDDELETE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_GROUPSRECYCLEBIN_HARDDELETE, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridGroupsRecycleBin_InitializeCommands_7, _YLOC("Permanently Delete Groups")).c_str(),
			YtriaTranslate::Do(GridGroupsRecycleBin_InitializeCommands_8, _YLOC("Permanently Delete Groups.")).c_str(),
			_cmd.m_sAccelText);
	}
	
	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_GROUPSRECYCLEBIN_LOADMORE;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroups_InitializeCommands_15, _YLOC("Load Info")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroups_InitializeCommands_16, _YLOC("Load Info")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LOADMORE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_GROUPSRECYCLEBIN_LOADMORE, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridGroups_InitializeCommands_17, _YLOC("Load Additional Information")).c_str(),
			_T("Load detailed group information for the selected entries only.\nThis includes information such as Team information and expiration policy status."),
			_cmd.m_sAccelText);
	}
}

BOOL GridGroupsRecycleBin::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return O365Grid::PreTranslateMessage(pMsg);
}

void GridGroupsRecycleBin::customizeGridPostProcess()
{
	O365Grid::customizeGridPostProcess();
	m_LoadMoreConfig.m_Tooltip = YtriaTranslate::Do(GridGroups_customizeGridPostProcess_1, _YLOC("To see this information, use the 'Load Info' button")).c_str();
	m_LoadMoreConfig.m_Text = YtriaTranslate::Do(GridGroups_customizeGridPostProcess_2, _YLOC(" - Use the 'Load Info' button to display additional information - ")).c_str();
	m_LoadMoreConfig.m_Icon = SetNoFieldTextAndIcon(GetColumnsByPresets({ g_ColumnsPresetMore }), IDB_ICON_LM, m_LoadMoreConfig.m_Tooltip, m_LoadMoreConfig.m_Text);

	SetLoadMoreStatus(true, YtriaTranslate::Do(GridGroups_customizeGridPostProcess_3, _YLOC("Loaded")).c_str(), IDB_ICON_LMSTATUS_LOADED);
	SetLoadMoreStatus(false, YtriaTranslate::Do(GridGroups_customizeGridPostProcess_4, _YLOC("Not Loaded")).c_str(), IDB_ICON_LMSTATUS_NOTLOADED);
}

void GridGroupsRecycleBin::RemoveGroupsNotModified(vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations)
{
    p_Groups.erase(std::remove_if(p_Groups.begin(), p_Groups.end(), [this, &p_UpdateOperations](const BusinessGroup& p_Group) {
        vector<GridBackendField> pk;
		m_TemplateGroups.GetBusinessGroupPK(*this, p_Group, pk, {});

        for (const auto& update : p_UpdateOperations)
        {
            if (update.GetPrimaryKey() == pk)
                return false;
        }
        return true;
    }), p_Groups.end());
}

bool GridGroupsRecycleBin::isOffice365Group(const GridBackendRow* p_Row) const
{
	return nullptr != p_Row && !p_Row->IsGroupRow() && p_Row->GetField(m_TemplateGroups.m_ColumnCombinedGroupType).GetValueStr() == BusinessGroup::g_Office365Group;
}

void GridGroupsRecycleBin::updateGroupLCPStatus(GridBackendRow* p_Row, const BusinessGroup& p_BG)
{
	ASSERT(isOffice365Group(p_Row));

	if (p_BG.GetLCPStatusError())
	{
		p_Row->AddField(p_BG.GetLCPStatusError()->GetFullErrorMessage(), m_TemplateGroups.m_ColumnExpirationPolicyStatus, GridBackendUtil::ICON_ERROR);
	}
	else if (p_BG.GetLCPStatus())
	{
		p_Row->AddField(p_BG.GetLCPStatus().get(), m_TemplateGroups.m_ColumnExpirationPolicyStatus);
	}
	else
	{
		p_Row->RemoveField(m_TemplateGroups.m_ColumnExpirationPolicyStatus);
	}
}

void GridGroupsRecycleBin::LoadMoreImpl(const std::vector<GridBackendRow*>& p_Rows)
{
	auto updaterGroupsGen = UpdatedObjectsGenerator<BusinessGroup>();
	if (nullptr != m_Frame)
	{
		updaterGroupsGen.SetObjects(GetLoadMoreRequestInfo(p_Rows));

		CommandInfo info;
		info.Data() = updaterGroupsGen;
		info.SetFrame(m_Frame);
		info.SetRBACPrivilege(m_Frame->GetModuleCriteria().m_Privilege);

		// FIXME: Do not call regular group load more
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateMoreInfo, info, { this, g_ActionNameSelectedGroupLoadMore, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
}

void GridGroupsRecycleBin::updateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh, bool p_DoNotShowLoadingErrors, const boost::YOpt<GridUpdater::PostUpdateError>& p_UnprocessedPostUpdateError)
{
	GridIgnoreModification ignoramus(*this);
	GridUpdaterOptions updateOptions(GridUpdaterOptions::UPDATE_GRID
		//| GridUpdaterOptions::FULLPURGE
		| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		//| GridUpdaterOptions::REFRESH_PROCESS_DATA
		| GridUpdaterOptions::PROCESS_LOADMORE
		| (p_DoNotShowLoadingErrors ? GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG : 0));
	GridUpdater updater(*this, updateOptions);

	if (p_UnprocessedPostUpdateError)
		updater.InitPostUpdateError(*p_UnprocessedPostUpdateError);

	//const auto tenantAllowAddToGuest = getTenantAllowAddToGuests();

	for (const auto& group : p_Groups)
	{
		if (!p_FromRefresh)
		{
			vector<GridBackendField> BGpk;
			m_TemplateGroups.GetBusinessGroupPK(*this, group, BGpk, {});
			GridBackendRow* rowToUpdate = GetRowIndex().GetExistingRow(BGpk);
			ASSERT(nullptr != rowToUpdate);
			if (nullptr != rowToUpdate)
				rowToUpdate->ClearStatus();
		}

		auto row = m_TemplateGroups.UpdateRow(*this, group, false, updater, true);
		ASSERT(nullptr != row);
		if (isOffice365Group(row))
		{
			ASSERT(group.GetLCPStatus() || group.GetLCPStatusError());
			updateGroupLCPStatus(row, group);
			LoadMorePostProcessRow(row); // This call will be made (again) later in GridUpdater::processLoadMore(), but updateAddToGuests() needs this state right now...
		}
		updater.GetOptions().AddRowWithRefreshedValues(row);
	}
}

void GridGroupsRecycleBin::setUpLoadMoreColumn(GridBackendColumn* p_Col)
{
	ASSERT(nullptr != p_Col);
	if (nullptr != p_Col)
	{
		p_Col->SetNoFieldTooltip(m_LoadMoreConfig.m_Tooltip);
		p_Col->SetNoFieldText(m_LoadMoreConfig.m_Text);
		p_Col->SetNoFieldIcon(m_LoadMoreConfig.m_Icon);
	}
}

void GridGroupsRecycleBin::setUpNoLoadMoreColumn(GridBackendColumn* p_Col)
{
	ASSERT(nullptr != p_Col);
	if (nullptr != p_Col)
	{
		p_Col->SetNoFieldTooltip(_YTEXT(""));
		p_Col->SetNoFieldText(_YTEXT(""));
		p_Col->SetNoFieldIcon(0);
	}
}

const wstring& GridGroupsRecycleBin::annotationGetModuleName() const
{
	return GridUtil::g_AutoNameGroups;
}

void GridGroupsRecycleBin::annotationModuleSetDefault(GridAnnotation& p_NewAnnotation) const
{
	p_NewAnnotation.SetRefDeleted(true);
}