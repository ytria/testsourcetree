#include "GridTemplateGroups.h"

#include "BasicGridSetup.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessGroup.h"
#include "GraphCache.h"
#include "GridGroups.h"
#include "GridSetupMetaColumn.h"
#include "GridUpdater.h"
#include "GridBackendUtil.h"
#include "SkuAndPlanReference.h"
#include "YtriaFieldsConstants.h"

COLORREF GridTemplateGroups::g_ColorRowGroup = RGB(240, 248, 255);// alice

GridTemplateGroups::GridTemplateGroups()
	: m_ColumnClassification(nullptr)
	, m_ColumnCreatedDateTime(nullptr)
	, m_ColumnRenewedDateTime(nullptr)
	, m_ColumnExpireOnDateTime(nullptr)
	, m_ColumnDeletedDateTime(nullptr)
	, m_ColumnExpirationPolicyStatus(nullptr)
	, m_ColumnDescription(nullptr)
	, m_ColumnGroupTypes(nullptr)
	, m_ColumnUnseenCount(nullptr)
	, m_ColumnVisibility(nullptr)
	, m_ColumnAllowExternalSenders(nullptr)
	, m_ColumnAutoSubscribeNewMembers(nullptr)
	//, m_ColumnIsSubscribedByMail(nullptr)
	, m_ColumnSecurityEnabled(nullptr)

	, m_ColumnOPLastSync(nullptr)
	, m_ColumnOPSecurityID(nullptr)
	, m_ColumnOPSyncEnabled(nullptr)
	, m_ColumnOPPECategory(nullptr)
	, m_ColumnOPPEOccuredDateTime(nullptr)
	, m_ColumnOPPEProperty(nullptr)
	, m_ColumnOPPEValue(nullptr)

	, m_ColumnMail(nullptr)
	, m_ColumnMailEnabled(nullptr)
	, m_ColumnMailNickname(nullptr)
	, m_ColumnProxyAddresses(nullptr)
	, m_ColumnCombinedGroupType(nullptr)
	, m_ColumnDynamicMembership(nullptr)
    , m_ColumnCreatedOnBehalfOfUserId(nullptr)
    , m_ColumnCreatedOnBehalfOfUserDisplayName(nullptr)
    , m_ColumnCreatedOnBehalfOfUserPrincipalName(nullptr)
	, m_ColumnOwnerUserIDs(nullptr)
	, m_ColumnOwnerUserNames(nullptr)
	, m_ColumnIsTeam(nullptr)
	, m_ColumnIsYammer(nullptr)
	, m_ColumnGroupUnifiedSettingActivated(nullptr)
	, m_ColumnSettingAllowToAddGuests(nullptr)
	, m_ColumnPreferredDataLocation(nullptr)

    , m_ColumnTeamMemberSettingsAllowCreateUpdateChannels(nullptr)
	, m_ColumnTeamMemberSettingsAllowCreatePrivateChannels(nullptr)
    , m_ColumnTeamMemberSettingsAllowDeleteChannels(nullptr)
    , m_ColumnTeamMemberSettingsAllowAddRemoveApps(nullptr)
    , m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs(nullptr)
    , m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors(nullptr)

    , m_ColumnTeamGuestSettingsAllowCreateUpdateChannels(nullptr)
    , m_ColumnTeamGuestSettingsAllowDeleteChannels(nullptr)
    , m_ColumnTeamMessagingSettingsAllowUserEditMessages(nullptr)
    , m_ColumnTeamMessagingSettingsAllowUserDeleteMessages(nullptr)
    , m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages(nullptr)
    , m_ColumnTeamMessagingSettingsAllowTeamMentions(nullptr)
    , m_ColumnTeamMessagingSettingsAllowChannelMentions(nullptr)

    , m_ColumnTeamFunSettingsAllowGiphy(nullptr)
    , m_ColumnTeamFunSettingsGiphyContentRating(nullptr)
    , m_ColumnTeamFunSettingsAllowStickersAndMemes(nullptr)
    , m_ColumnTeamFunSettingsAllowCustomMemes(nullptr)

	, m_ColumnTeamWebUrl(nullptr)

	, m_ColumnTeamIsArchived(nullptr)
	, m_ColumnSpoSiteReadOnlyForMembers(nullptr)
	, m_ColumnTeamInternalId(nullptr)

	, m_ColumnResourceBehaviorOptions(nullptr)
	, m_ColumnResourceProvisioningOptions(nullptr)

	, m_ColumnMembersCount(nullptr)

	, m_ColumnHideFromOutlookClients(nullptr)
	, m_ColumnHideFromAddressLists(nullptr)
	, m_ColumnLicenseProcessingState(nullptr)
	, m_ColumnSecurityIdentifier(nullptr)

	, m_ColumnSkuIds(nullptr)
	, m_ColumnSkuPartNumbers(nullptr)
	, m_ColumnSkuNames(nullptr)

	/* Beta API */
	, m_ColumnMembershipRuleProcessingState(nullptr)
	, m_ColumnMembershipRule(nullptr)
	, m_ColumnTheme(nullptr)
	, m_ColumnPreferredLanguage(nullptr)

	, m_ColumnLastRequestDateTime(nullptr)

	/* Drive columns */
	, m_ColumnDriveName(nullptr)
	, m_ColumnDriveId(nullptr)
	, m_ColumnDriveDescription(nullptr)
	, m_ColumnDriveQuotaState(nullptr)
	, m_ColumnDriveQuotaUsed(nullptr)
	, m_ColumnDriveQuotaRemaining(nullptr)
	, m_ColumnDriveQuotaDeleted(nullptr)
	, m_ColumnDriveQuotaTotal(nullptr)
	, m_ColumnDriveCreationTime(nullptr)
	, m_ColumnDriveLastModifiedTime(nullptr)
	, m_ColumnDriveType(nullptr)
	, m_ColumnDriveWebUrl(nullptr)
	, m_ColumnDriveCreatedByUserEmail(nullptr)
	, m_ColumnDriveCreatedByUserId(nullptr)
	, m_ColumnDriveCreatedByAppName(nullptr)
	, m_ColumnDriveCreatedByAppId(nullptr)
	, m_ColumnDriveCreatedByDeviceName(nullptr)
	, m_ColumnDriveCreatedByDeviceId(nullptr)
	, m_ColumnDriveOwnerUserName(nullptr)
	, m_ColumnDriveOwnerUserId(nullptr)
	, m_ColumnDriveOwnerUserEmail(nullptr)
	, m_ColumnDriveOwnerAppName(nullptr)
	, m_ColumnDriveOwnerAppId(nullptr)
	, m_ColumnDriveOwnerDeviceName(nullptr)
	, m_ColumnDriveOwnerDeviceId(nullptr)

	, m_DebugMode(false)
	, m_IconIsTeam(0)
	, m_IconIsArchived(0)
	//, m_IconIsNotArchived(0)
	, m_IconIsYammer(0)
{
}

GridBackendRow* GridTemplateGroups::AddRow(O365Grid& p_Grid, const BusinessGroup& p_BG, const vector<GridBackendField>& p_ExtraRowPKValues, GridUpdater& p_Updater, bool p_SetCreatedStatus, GridBackendRow* p_ParentRow, const vector<GridBackendField>& p_AdditionalFields, bool p_KeepExistingIfGroupCanceled)
{
	ASSERT(!p_SetCreatedStatus || !p_KeepExistingIfGroupCanceled);

	vector<GridBackendField> BGK;
	GetBusinessGroupPK(p_Grid, p_BG, BGK, p_ExtraRowPKValues);

	if (p_SetCreatedStatus)
	{
		bool wasError = false;
		{
			auto mod = p_Grid.GetModifications().GetRowModification<CreatedObjectModification>(BGK);
			if (nullptr != mod)
			{
				wasError = mod->GetState() == Modification::State::RemoteError;
				p_Grid.GetModifications().Revert(BGK, false);
			}
		}

		{
			auto row = p_Grid.GetRowIndex().GetExistingRow(BGK);
			ASSERT(nullptr == row || wasError);
			if (nullptr != row)
				p_Grid.RemoveRow(row);
		}
	}

	ASSERT(!p_SetCreatedStatus || nullptr == p_Grid.GetRowIndex().GetExistingRow(BGK));

	GridBackendRow* row = nullptr;
	if (p_KeepExistingIfGroupCanceled && p_BG.HasFlag(BusinessObject::CANCELED)
		&& nullptr != p_Grid.GetRowIndex().GetExistingRow(BGK))
	{
		row = p_Grid.GetRowIndex().GetRow(BGK, false, !p_SetCreatedStatus && (p_Updater.GetOptions().IsFullPurge() || p_Updater.GetOptions().IsPartialPurge()));
		ASSERT(nullptr != row);
		setLastRequestDateTime(p_Grid, p_BG, row);
	}
	else
	{
		row = p_Grid.GetRowIndex().GetRow(BGK, true, !p_SetCreatedStatus && (p_Updater.GetOptions().IsFullPurge() || p_Updater.GetOptions().IsPartialPurge()));
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			GridUtil::SetGroupRowObjectType(p_Grid, row, p_BG);

			SetDebugMode(p_Grid.isDebugMode());
			updateRow(p_Grid, p_BG, row, p_Grid.GetGraphCache(), false);

			if (nullptr != p_ParentRow)
			{
				ASSERT(nullptr == row->GetParentRow() || p_ParentRow == row->GetParentRow());
				row->SetParentRow(p_ParentRow);
			}
			row->AddFields(p_AdditionalFields);

			if (p_SetCreatedStatus)
			{
				auto mod = std::make_unique<CreatedObjectModification>(p_Grid, BGK);
				mod->SetShouldRemoveRowOnRevertError(true); // Because refresh is using delta Sync!
				p_Grid.GetModifications().Add(std::move(mod));
			}

			if (p_Grid.IsGridModificationsEnabled())
				p_Updater.AddUpdatedRowPk(BGK);
		}
	}

	return row;
}

bool GridTemplateGroups::RemoveRow(O365Grid& p_Grid, const BusinessGroup& p_BG)
{
	bool removed = false;

	vector< GridBackendRow* > ListRows;
	p_Grid.GetAllNonGroupRows(ListRows);
	for (const auto& Row : ListRows)
	{
		const GridBackendField& IDField = Row->GetField(m_ColumnID);
		if (IDField.GetValueStr() == (wstring)p_BG.GetID())
		{
			removed = p_Grid.RemoveRow(Row);
			break;
		}
	}

	return removed;
}

GridBackendRow*	GridTemplateGroups::UpdateRow(O365Grid& p_Grid, const BusinessGroup& p_BG, bool p_SetModifiedStatus, GridUpdater& p_Updater, bool p_FromLoadMore)
{
	vector<GridBackendField> BGpk;
	GetBusinessGroupPK(p_Grid, p_BG, BGpk, {});

	if (p_SetModifiedStatus)
	{
		auto mod = p_Grid.GetModifications().GetRowModification<DeletedObjectModification>(BGpk);
		if (nullptr != mod)
		{
			ASSERT(nullptr == p_Grid.GetModifications().GetRowModification<CreatedObjectModification>(BGpk)
				&& nullptr == p_Grid.GetModifications().GetRowFieldModification(BGpk));
			p_Grid.GetModifications().Revert(BGpk, false);
		}
	}

	GridBackendRow* rowToUpdate = p_Grid.GetRowIndex().GetExistingRow(BGpk);
    if (nullptr != rowToUpdate)
    {
        if (p_BG.GetError())
        {
			ASSERT(!p_SetModifiedStatus && !p_BG.HasFlag(BusinessObject::CREATED));
			if (p_FromLoadMore)
			{
				//p_Grid.ClearFieldsByPreset(rowToUpdate, O365Grid::g_ColumnsPresetMore);
				for (const auto& col : p_Grid.GetColumnsByPresets({ O365Grid::g_ColumnsPresetMore }))
				{
					// Created on behalf of family?
					if (col->GetUniqueID() == _YUID("createdOnBehalfOfUserId") ||
						col->GetUniqueID() == _YUID("createdOnBehalfOfUserDisplayName") ||
						col->GetUniqueID() == _YUID("createdOnBehalfOfUserPrincipalName"))
					{
						if (p_BG.GetUserCreatedOnBehalfOfError())
							rowToUpdate->AddField(p_BG.GetUserCreatedOnBehalfOfError()->GetFullErrorMessage(), col, GridBackendUtil::ICON_ERROR);
					}
					else if (col == m_ColumnOwnerUserIDs || col == m_ColumnOwnerUserNames)
					{
						if (p_BG.GetOwnersError())
							rowToUpdate->AddField(p_BG.GetOwnersError()->GetFullErrorMessage(), col, GridBackendUtil::ICON_ERROR);
					}
					else if (col != m_ColumnMembershipRule && col != m_ColumnTheme && col != m_ColumnPreferredLanguage && col != m_ColumnMembershipRuleProcessingState  // Beta columns
								&& col != m_ColumnMembersCount
								&& col != m_ColumnExpirationPolicyStatus && col != m_ColumnExpireOnDateTime // => LCP status is updated in GridGroups::UpdateGroupsLoadMore
							)
					{
						rowToUpdate->AddField(p_BG.GetError()->GetFullErrorMessage(), col, GridBackendUtil::ICON_ERROR);
					}
				}
			}

			// Update fields that don't correspond to main load more request, they should contain a "Skipped" error.
			updateRowDrive(p_BG, rowToUpdate, UpdateFieldOption::NONE);
			updateRowTeam(p_BG, rowToUpdate, false);
			updateRowCreatedOnBehalfOf(p_BG, rowToUpdate, p_Grid.GetGraphCache());
			updateRowGroupSettings(p_BG, rowToUpdate, false);
			// updateRowLCP(); => LCP status is updated in GridGroups::UpdateGroupsLoadMore
			updateRowOwners(p_BG, rowToUpdate, p_Grid.GetGraphCache(), UpdateFieldOption::NONE);
			updateRowIsYammer(p_BG, rowToUpdate);

			p_Updater.OnLoadingError(rowToUpdate, *p_BG.GetError());
        }
        else
        {
			// Clear previous error
			if (!p_SetModifiedStatus && !p_FromLoadMore && rowToUpdate->IsError() && !p_BG.HasFlag(BusinessObject::CREATED))
				rowToUpdate->ClearStatus();

			SetDebugMode(p_Grid.isDebugMode());
            updateRow(p_Grid, p_BG, rowToUpdate, p_Grid.GetGraphCache(), p_SetModifiedStatus);
			if (rowToUpdate->HasModifiedField(p_Grid.GetBackend().GetStatusColumn()->GetID()) && p_SetModifiedStatus)
			{
				p_Grid.OnRowModified(rowToUpdate);
			}
			else
			{
				GridUtil::HandleMultiError(rowToUpdate
					,	{ { p_BG.GetUserCreatedOnBehalfOfError(), YtriaTranslate::Do(GridTemplateGroups_UpdateRow_1, _YLOC("Created on behalf of: %1"), _YTEXT("%s")) }
						, { p_BG.GetTeamError(), YtriaTranslate::Do(GridTemplateGroups_UpdateRow_2, _YLOC("Team: %1"), _YTEXT("%s")) }
						, { p_BG.GetGroupSettingsError(), YtriaTranslate::Do(GridTemplateGroups_UpdateRow_3, _YLOC("Group Settings: %1"), _YTEXT("%s")) }
						, { p_BG.GetLCPStatusError(), YtriaTranslate::Do(GridTemplateGroups_UpdateRow_4, _YLOC("Expiration Policy: %1"), _YTEXT("%s")) }
						, { p_BG.GetOwnersError(), YtriaTranslate::Do(GridTemplateGroups_UpdateRow_5, _YLOC("Owners: %1"), _YTEXT("%s")) }
						, { p_BG.GetDriveError(), YtriaTranslate::Do(GridTemplateGroups_UpdateRow_6, _YLOC("Drive: %1"), _YTEXT("%s")) }
						, { p_BG.GetIsYammerError(), _YFORMAT(L"Is Yammer: %s", _YTEXT("%s")) }
						}
					,	p_Updater);
			}
        }

		if (p_Grid.IsGridModificationsEnabled())
			p_Updater.AddUpdatedRowPk(BGpk);
	}

	return rowToUpdate;
}

GridBackendRow* GridTemplateGroups::UpdateRow(O365Grid& p_Grid, const OnPremiseGroup& p_OnPremiseGroup, const map<wstring, row_pk_t>& p_O365RowPk, map<wstring, row_pk_t>& p_OnPremRowPk, UpdateFieldOption p_UpdateFieldOption /*= UpdateFieldOption::NONE*/)
{
	std::set<PooledString> dummy;
	return UpdateRow(p_Grid, p_OnPremiseGroup, p_O365RowPk, p_OnPremRowPk, dummy, p_UpdateFieldOption);
}

GridBackendRow* GridTemplateGroups::UpdateRow(O365Grid& p_Grid, const OnPremiseGroup& p_OnPremiseGroup, const map<wstring, row_pk_t>& p_O365RowPk, map<wstring, row_pk_t>& p_OnPremRowPk, std::set<PooledString>& p_NewObjectSIDs, UpdateFieldOption p_UpdateFieldOption /*= UpdateFieldOption::NONE*/)
{
	GridBackendRow* row = nullptr;

	wstring securityIdentifier = p_OnPremiseGroup.GetObjectSid() ? *p_OnPremiseGroup.GetObjectSid() : PooledString();
	p_NewObjectSIDs.insert(securityIdentifier);

	auto itt = p_O365RowPk.find(securityIdentifier);
	if (itt != p_O365RowPk.end())
	{
		// Refreshing on-prem group which has an o365 equivalent
		// In that case, fill the existing o365 row
		row = p_Grid.GetRowIndex().GetExistingRow(itt->second);
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			SetHybrid(p_Grid, *row);
			p_OnPremRowPk[securityIdentifier] = itt->second;
		}
	}

	if (nullptr == row && p_OnPremiseGroup.GetObjectSid())
	{
		// Refreshing on-prem group which has no o365 equivalent
		// In that case, create a new row
		row_pk_t pk = CreateRowPk(p_OnPremiseGroup);
		static const bool willPurge = false; // We never purge when refreshing OnPrem
		row = p_Grid.GetRowIndex().GetRow(pk, true, willPurge);
		SetOnPrem(p_Grid, *row);
		p_OnPremRowPk[securityIdentifier] = pk;
	}

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		if (nullptr != p_Grid.GetSecondaryStatusColumn() && !row->HasField(p_Grid.GetSecondaryStatusColumn()))
			row->AddField(PooledString::g_EmptyString, p_Grid.GetSecondaryStatusColumn()).RemoveModified();

		updateFieldOnPrem(p_OnPremiseGroup.GetAdminCount(), row, m_OnPremCols.m_ColAdminCount, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetCanonicalName(), row, m_OnPremCols.m_ColCanonicalName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetCN(), row, m_OnPremCols.m_ColCN, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetCreated(), row, m_OnPremCols.m_ColCreated, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetCreateTimeStamp(), row, m_OnPremCols.m_ColCreateTimeStamp, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetDeleted(), row, m_OnPremCols.m_ColDeleted, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetDescription(), row, m_OnPremCols.m_ColDescription, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetDisplayName(), row, m_OnPremCols.m_ColDisplayName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetDistinguishedName(), row, m_OnPremCols.m_ColDistinguishedName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetDSCorePropagationData(), row, m_OnPremCols.m_ColDSCorePropagationData, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetGroupCategory(), row, m_OnPremCols.m_ColGroupCategory, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetGroupScope(), row, m_OnPremCols.m_ColGroupScope, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetGroupType(), row, m_OnPremCols.m_ColGroupType, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetHomePage(), row, m_OnPremCols.m_ColHomePage, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetInstanceType(), row, m_OnPremCols.m_ColInstanceType, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetIsCriticalSystemObject(), row, m_OnPremCols.m_ColIsCriticalSystemObject, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetIsDeleted(), row, m_OnPremCols.m_ColIsDeleted, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetLastKnownParent(), row, m_OnPremCols.m_ColLastKnownParent, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetManagedBy(), row, m_OnPremCols.m_ColManagedBy, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetMember(), row, m_OnPremCols.m_ColMember, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetMemberOf(), row, m_OnPremCols.m_ColMemberOf, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetMembers(), row, m_OnPremCols.m_ColMembers, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetModified(), row, m_OnPremCols.m_ColModified, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetModifyTimeStamp(), row, m_OnPremCols.m_ColModifyTimeStamp, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetName(), row, m_OnPremCols.m_ColName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetObjectCategory(), row, m_OnPremCols.m_ColObjectCategory, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetObjectClass(), row, m_OnPremCols.m_ColObjectClass, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetObjectGUID(), row, m_OnPremCols.m_ColObjectGUID, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetObjectSid(), row, m_OnPremCols.m_ColObjectSid, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetProtectedFromAccidentalDeletion(), row, m_OnPremCols.m_ColProtectedFromAccidentalDeletion, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetSamAccountName(), row, m_OnPremCols.m_ColSamAccountName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetSAMAccountType(), row, m_OnPremCols.m_ColSAMAccountType, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetSDRightsEffective(), row, m_OnPremCols.m_ColSDRightsEffective, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetSID(), row, m_OnPremCols.m_ColSID, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetSystemFlags(), row, m_OnPremCols.m_ColSystemFlags, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetUSNChanged(), row, m_OnPremCols.m_ColUSNChanged, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetUSNCreated(), row, m_OnPremCols.m_ColUSNCreated, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetWhenChanged(), row, m_OnPremCols.m_ColWhenChanged, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremiseGroup.GetWhenCreated(), row, m_OnPremCols.m_ColWhenCreated, p_UpdateFieldOption);

		if (p_UpdateFieldOption == UpdateFieldOption::NONE) // Only load
			UpdateCommonInfo(row);
	}

	return row;
}

void GridTemplateGroups::updateRow(O365Grid& p_Grid, const BusinessGroup& p_BG, GridBackendRow* p_Row, const GraphCache& p_GraphCache, bool p_StoreModification)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		const UpdateFieldOption updateFieldOption = p_BG.HasFlag(BusinessObject::CREATED) ? UpdateFieldOption::IS_OBJECT_CREATION : (p_StoreModification ? UpdateFieldOption::IS_MODIFICATION : UpdateFieldOption::NONE);

		// For all groups
		{
			if (!p_BG.GetID().IsEmpty())
			{
				auto& idField = p_Row->GetField(m_ColumnID);
				ASSERT(!idField.HasValue() || p_BG.GetID() == idField.GetValueStr());
				if (!idField.HasValue())
					p_Row->AddField(p_BG.GetID(), m_ColumnID);
			}

			if (UpdateFieldOption::NONE == updateFieldOption)
				setLastRequestDateTime(p_Grid, p_BG, p_Row);

			if (m_ColumnMetaType)
				SetO365(p_Grid, *p_Row);
			updateField(p_BG.GetGroupTypes(), p_Row, m_ColumnGroupTypes, updateFieldOption);
			updateField(p_BG.GetCombinedGroupType(), p_Row, m_ColumnCombinedGroupType, updateFieldOption);
			updateField(p_BG.IsDynamicMembership(), p_Row, m_ColumnDynamicMembership, updateFieldOption);
			updateField(p_BG.GetDisplayName(), p_Row, m_ColumnDisplayName, updateFieldOption);
			if (nullptr != m_ColumnMailNickname && p_BG.GetMailNickname()
				&& (m_DebugMode
					|| (!p_BG.GetCombinedGroupType() || *p_BG.GetCombinedGroupType() != BusinessGroup::g_SecurityGroup)))
				updateField(p_BG.GetMailNickname(), p_Row, m_ColumnMailNickname, updateFieldOption);
			else if (nullptr != m_ColumnMailNickname)
				p_Row->RemoveField(m_ColumnMailNickname);

			updateField(p_BG.GetDescription(), p_Row, m_ColumnDescription, updateFieldOption);
			updateField(p_BG.GetVisibility(), p_Row, m_ColumnVisibility, updateFieldOption);
			updateField(p_BG.IsSecurityEnabled(), p_Row, m_ColumnSecurityEnabled, updateFieldOption);
			updateField(p_BG.IsMailEnabled(), p_Row, m_ColumnMailEnabled, updateFieldOption);
			updateField(p_BG.GetResourceBehaviorOptions(), p_Row, m_ColumnResourceBehaviorOptions, updateFieldOption);
			updateField(p_BG.GetResourceProvisioningOptions(), p_Row, m_ColumnResourceProvisioningOptions, updateFieldOption);
			updateField(p_BG.GetMembersCount(), p_Row, m_ColumnMembersCount, updateFieldOption);
			updateField(p_BG.GetPreferredDataLocation(), p_Row, m_ColumnPreferredDataLocation, updateFieldOption);

			updateField(p_BG.GetHideFromOutlookClients(), p_Row, m_ColumnHideFromOutlookClients, updateFieldOption);
			updateField(p_BG.GetHideFromAddressLists(), p_Row, m_ColumnHideFromAddressLists, updateFieldOption);

			if (p_BG.GetLicenseProcessingState())
				updateField(p_BG.GetLicenseProcessingState()->m_State, p_Row, m_ColumnLicenseProcessingState, updateFieldOption);

			updateField(p_BG.GetSecurityIdentifier(), p_Row, m_ColumnSecurityIdentifier, updateFieldOption);

			/* Beta API */
			updateField(p_BG.GetMembershipRuleProcessingState(), p_Row, m_ColumnMembershipRuleProcessingState, updateFieldOption);
			updateField(p_BG.GetMembershipRule(), p_Row, m_ColumnMembershipRule, updateFieldOption);
			updateField(p_BG.GetTheme(), p_Row, m_ColumnTheme, updateFieldOption);
			updateField(p_BG.GetPreferredLanguage(), p_Row, m_ColumnPreferredLanguage, updateFieldOption);

			updateRowDrive(p_BG, p_Row, updateFieldOption);
			updateRowOwners(p_BG, p_Row, p_GraphCache, updateFieldOption);
			updateRowTeam(p_BG, p_Row, p_StoreModification);
			updateRowIsYammer(p_BG, p_Row);

			if (nullptr != m_ColumnSkuIds || nullptr != m_ColumnSkuPartNumbers || nullptr != m_ColumnSkuNames)
			{
				if (!p_BG.GetAssignedLicenses() ||  (p_BG.GetAssignedLicenses() && p_BG.GetAssignedLicenses()->empty()))
				{
					updateField(boost::YOpt<PooledString>(), p_Row, m_ColumnSkuIds, updateFieldOption);
					updateField(BusinessGroupConfiguration::GetInstance().GetValueStringLicenses_Unlicensed(), p_Row, m_ColumnSkuNames, updateFieldOption);
					updateField(BusinessGroupConfiguration::GetInstance().GetValueStringLicenses_Unlicensed(), p_Row, m_ColumnSkuPartNumbers, updateFieldOption);
				}
				else if (p_BG.GetAssignedLicenses())
				{
					vector<PooledString> skuIds;
					vector<PooledString> skuPartNumbers;
					vector<PooledString> skuNames;
					skuIds.reserve(p_BG.GetAssignedLicenses()->size());
					skuPartNumbers.reserve(p_BG.GetAssignedLicenses()->size());
					skuNames.reserve(p_BG.GetAssignedLicenses()->size());

					bool error = false;
					static const wstring errorUnknown = _YERROR("-- Unknown SKU --"); // GridBackendUtil::g_NoValueString ??
					for (const auto& assignedLicense : *p_BG.GetAssignedLicenses())
					{
						if (nullptr != m_ColumnSkuIds)
							skuIds.emplace_back(assignedLicense.GetID());

						if (nullptr != m_ColumnSkuPartNumbers || nullptr != m_ColumnSkuNames)
						{
							const auto& skuPartNumber = SkuAndPlanReference::GetInstance().GetSkuPartNumber(assignedLicense.GetID());
							const auto& label = SkuAndPlanReference::GetInstance().GetSkuLabel(assignedLicense.GetID());
							if (skuPartNumber.IsEmpty())
							{
								error = true;
								skuPartNumbers.emplace_back(errorUnknown);
							}
							else
							{
								skuPartNumbers.emplace_back(skuPartNumber);
							}

							if (label.IsEmpty())
							{
								if (skuPartNumber.IsEmpty())
									skuNames.emplace_back(errorUnknown);
								else
									skuNames.emplace_back(skuPartNumber);
							}
							else
							{
								skuNames.emplace_back(label);
							}
						}
					}

					if (nullptr != m_ColumnSkuIds)
						updateField(skuIds, p_Row, m_ColumnSkuIds, updateFieldOption);
					if (nullptr != m_ColumnSkuPartNumbers)
					{
						auto field = updateField(skuPartNumbers, p_Row, m_ColumnSkuPartNumbers, updateFieldOption);
						ASSERT(nullptr != field && !field->IsGeneric());
						if (error && nullptr != field && !field->IsGeneric())
							field->SetIcon(GridBackendUtil::ICON_ERROR);

						updateField(skuNames, p_Row, m_ColumnSkuNames, updateFieldOption);
					}
				}
			}
			else
			{
				updateField(boost::YOpt<PooledString>(), p_Row, m_ColumnSkuIds, updateFieldOption);
				updateField(boost::YOpt<PooledString>(), p_Row, m_ColumnSkuPartNumbers, updateFieldOption);
				updateField(boost::YOpt<PooledString>(), p_Row, m_ColumnSkuNames, updateFieldOption);
			}
		}

		// Only for edited groups (not created)
		if (!p_BG.HasFlag(BusinessObject::CREATED))
		{
			updateRowGroupSettings(p_BG, p_Row, p_StoreModification);

			updateField(p_BG.GetClassification(), p_Row, m_ColumnClassification, updateFieldOption);
			updateField(p_BG.GetCreatedDateTime(), p_Row, m_ColumnCreatedDateTime, updateFieldOption);
			updateField(p_BG.GetRenewedDateTime(), p_Row, m_ColumnRenewedDateTime, updateFieldOption);
			updateField(p_BG.GetDeletedDateTime(), p_Row, m_ColumnDeletedDateTime, updateFieldOption);
			
			updateField(p_BG.GetUnseenCount(), p_Row, m_ColumnUnseenCount, updateFieldOption);

			updateField(p_BG.IsAutoSubscribeNewMembers(), p_Row, m_ColumnAutoSubscribeNewMembers, updateFieldOption);
			updateField(p_BG.IsAllowExternalSenders(), p_Row, m_ColumnAllowExternalSenders, updateFieldOption);
			//updateField(p_BG.IsSubscribedByMail(), p_Row, m_ColumnIsSubscribedByMail, updateFieldOption);
			updateField(p_BG.GetMail(), p_Row, m_ColumnMail, updateFieldOption);

			updateField(p_BG.GetOnPremisesLastSyncDateTime(), p_Row, m_ColumnOPLastSync, updateFieldOption);
			updateField(p_BG.GetOnPremisesSecurityIdentifier(), p_Row, m_ColumnOPSecurityID, updateFieldOption);
			updateField(p_BG.IsOnPremisesSyncEnabled(), p_Row, m_ColumnOPSyncEnabled, updateFieldOption);

			if (nullptr != m_ColumnOPPECategory
				|| nullptr != m_ColumnOPPEOccuredDateTime
				|| nullptr != m_ColumnOPPEProperty
				|| nullptr != m_ColumnOPPEValue)
			{
				boost::YOpt<vector<PooledString>> categories;
				boost::YOpt<vector<YTimeDate>> occurredDataTimes;
				boost::YOpt<vector<PooledString>> properties;
				boost::YOpt<vector<PooledString>> values;

				auto& oppe = p_BG.GetOnPremisesProvisioningErrors();
				if (oppe)
				{
					categories.emplace();
					occurredDataTimes.emplace();
					properties.emplace();
					values.emplace();

					for (auto& e : *oppe)
					{
						if (e.m_Category)
							categories->push_back(*e.m_Category);
						else
							categories->push_back(GridBackendUtil::g_NoValueString);

						if (e.m_OccurredDateTime)
							occurredDataTimes->push_back(*e.m_OccurredDateTime);
						else
							occurredDataTimes->push_back(GridBackendUtil::g_NoValueDate);

						if (e.m_PropertyCausingError)
							properties->push_back(*e.m_PropertyCausingError);
						else
							properties->push_back(GridBackendUtil::g_NoValueString);

						if (e.m_Value)
							values->push_back(*e.m_Value);
						else
							values->push_back(GridBackendUtil::g_NoValueString);
					}

					ASSERT(categories->size() == occurredDataTimes->size()
						&& categories->size() == properties->size()
						&& categories->size() == values->size()
						&& categories->size() == oppe->size());
				}

				// Do not clear fields if coming from Edit: it's not editable but empty because not handled in GetBusinessUser()
				if (categories || UpdateFieldOption::NONE == updateFieldOption)
				{
					updateField(categories, p_Row, m_ColumnOPPECategory, updateFieldOption);
					updateField(occurredDataTimes, p_Row, m_ColumnOPPEOccuredDateTime, updateFieldOption);
					updateField(properties, p_Row, m_ColumnOPPEProperty, updateFieldOption);
					updateField(values, p_Row, m_ColumnOPPEValue, updateFieldOption);
				}
			}

			updateField(p_BG.GetProxyAddresses(), p_Row, m_ColumnProxyAddresses, updateFieldOption);

			if (!p_StoreModification) // The following properties are not editable, do not risk to overwrite them.
				updateRowCreatedOnBehalfOf(p_BG, p_Row, p_GraphCache);

			// LCP status (finally handled in GridGroups::UpdateGroupsLoadMore)
			//updateField(p_BG.GetLCPStatus(), p_Row, m_ColumnExpirationPolicyStatus, p_StoreModification);
		}

		if (updateFieldOption == UpdateFieldOption::NONE) // Only load
			UpdateCommonInfo(p_Row);
	}
}

void GridTemplateGroups::AddIconTeam(O365Grid& p_Grid)
{
	m_IconIsTeam = GridUtil::AddIconTeam(p_Grid);
}

void GridTemplateGroups::SetDebugMode(bool p_DebugMode)
{
	m_DebugMode = p_DebugMode;
}

void GridTemplateGroups::GetBusinessGroup(const O365Grid& p_Grid, GridBackendRow* p_Row, BusinessGroup& p_BG) const
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		{
			const auto& field = p_Row->GetField(m_ColumnID);
			if (hasValidErrorlessValue(field))
			{
				p_BG.SetID(field.GetValueStr());
				p_BG.SetRBACDelegationID(p_Grid.GetGraphCache().GetGroupInCache(p_BG.GetID()).GetRBACDelegationID());
			}
		}

		{
			const auto& field = p_Row->GetField(m_ColumnDisplayName);
			if (hasValidErrorlessValue(field))
				p_BG.SetDisplayName(PooledString(field.GetValueStr()));
		}

		if (nullptr != m_ColumnClassification)
		{
			const auto& field = p_Row->GetField(m_ColumnClassification);
			if (hasValidErrorlessValue(field))
				p_BG.SetClassification(PooledString(field.GetValueStr()));
		}

		if (nullptr != m_ColumnCreatedDateTime)
		{
			const auto& field = p_Row->GetField(m_ColumnCreatedDateTime);
			if (hasValidErrorlessValue(field))
			{
				CString dateTime;
				p_BG.SetCreatedDateTime(field.GetValueTimeDate());
			}
		}

		if (nullptr != m_ColumnRenewedDateTime)
		{
			const auto& field = p_Row->GetField(m_ColumnRenewedDateTime);
			if (hasValidErrorlessValue(field))
			{
				CString dateTime;
				p_BG.SetRenewedDateTime(field.GetValueTimeDate());
			}
		}

		// FIXME: m_ColumnExpireOnDateTime?

		if (nullptr != m_ColumnDeletedDateTime)
		{
			const auto& field = p_Row->GetField(m_ColumnDeletedDateTime);
			if (hasValidErrorlessValue(field))
			{
				CString dateTime;
				p_BG.SetDeletedDateTime(field.GetValueTimeDate());
			}
		}

		if (nullptr != m_ColumnDescription)
		{
			const auto& field = p_Row->GetField(m_ColumnDescription);
			if (hasValidErrorlessValue(field))
				p_BG.SetDescription(PooledString(field.GetValueStr()));
		}		

		if (nullptr != m_ColumnGroupTypes)
		{
			const auto& field = p_Row->GetField(m_ColumnGroupTypes);
			if (hasValidErrorlessValue(field))
			{
				if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
					p_BG.SetGroupTypes(*field.GetValuesMulti<PooledString>());
				else
					p_BG.SetGroupTypes(vector<PooledString>{ field.GetValueStr() });
			}
		}
		else if (nullptr != m_ColumnCombinedGroupType)
		{
			const auto& field = p_Row->GetField(m_ColumnCombinedGroupType);
			if (hasValidErrorlessValue(field))
				p_BG.SetCombinedGroupType(PooledString(field.GetValueStr()));
		}

		if (nullptr != m_ColumnUnseenCount)
		{
			const auto& field = p_Row->GetField(m_ColumnUnseenCount);
			if (hasValidErrorlessValue(field))
				p_BG.SetUnseenCount(static_cast<int32_t>(field.GetValueLong()));
		}

		if (nullptr != m_ColumnVisibility)
		{
			const auto& field = p_Row->GetField(m_ColumnVisibility);
			if (hasValidErrorlessValue(field))
				p_BG.SetVisibility(PooledString(field.GetValueStr()));
		}

		if (nullptr != m_ColumnAllowExternalSenders)
		{
			const auto& field = p_Row->GetField(m_ColumnAllowExternalSenders);
			if (hasValidErrorlessValue(field))
				p_BG.SetIsAllowExternalSenders(field.GetValueBool());
		}

		if (nullptr != m_ColumnAutoSubscribeNewMembers)
		{
			const auto& field = p_Row->GetField(m_ColumnAutoSubscribeNewMembers);
			if (hasValidErrorlessValue(field))
				p_BG.SetIsAutoSubscribeNewMembers(field.GetValueBool());
		}

		/*{
			const auto& field = p_Row->GetField(m_ColumnIsSubscribedByMail);
			if (hasValidErrorlessValue(field))
				p_BG.SetIsSubscribedByMail(field.GetValueBool());
		}*/

		if (nullptr != m_ColumnSecurityEnabled)
		{
			const auto& field = p_Row->GetField(m_ColumnSecurityEnabled);
			if (hasValidErrorlessValue(field))
				p_BG.SetIsSecurityEnabled(field.GetValueBool());
		}

		if (nullptr != m_ColumnOPLastSync)
		{
			const auto& field = p_Row->GetField(m_ColumnOPLastSync);
			if (hasValidErrorlessValue(field))
				p_BG.SetOnPremisesLastSyncDateTime(PooledString(field.GetValueStr()));
		}

		if (nullptr != m_ColumnOPSecurityID)
		{
			const auto& field = p_Row->GetField(m_ColumnOPSecurityID);
			if (hasValidErrorlessValue(field))
				p_BG.SetOnPremisesSecurityIdentifier(PooledString(field.GetValueStr()));
		}

		if (nullptr != m_ColumnOPSyncEnabled)
		{
			const auto& field = p_Row->GetField(m_ColumnOPSyncEnabled);
			if (hasValidErrorlessValue(field))
				p_BG.SetOnPremisesSyncEnabled(field.GetValueBool());
		}

		// handle this?
		/*
		GridBackendColumn* m_ColumnOPPECategory;
		GridBackendColumn* m_ColumnOPPEOccuredDateTime;
		GridBackendColumn* m_ColumnOPPEProperty;
		GridBackendColumn* m_ColumnOPPEValue;
		*/

		if (nullptr != m_ColumnMail)
		{
			const auto& field = p_Row->GetField(m_ColumnMail);
			if (hasValidErrorlessValue(field))
				p_BG.SetMail(PooledString(field.GetValueStr()));
		}

		if (nullptr != m_ColumnMailEnabled)
		{
			const auto& field = p_Row->GetField(m_ColumnMailEnabled);
			if (hasValidErrorlessValue(field))
				p_BG.SetIsMailEnabled(field.GetValueBool());
		}

		if (nullptr != m_ColumnMailNickname)
		{
			const auto& field = p_Row->GetField(m_ColumnMailNickname);
			if (hasValidErrorlessValue(field))
				p_BG.SetMailNickname(PooledString(field.GetValueStr()));
			/*else if (p_BG.GetCombinedGroupType() && *p_BG.GetCombinedGroupType() == BusinessGroup::g_SecurityGroup)
				p_BG.SetMailNickname(PooledString(_YTEXT("00000000-0000-0000-0000-000000000000")));*/
		}

		if (nullptr != m_ColumnProxyAddresses)
		{
			const auto& field = p_Row->GetField(m_ColumnProxyAddresses);
			if (hasValidErrorlessValue(field))
			{
				if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
					p_BG.SetProxyAddresses(*field.GetValuesMulti<PooledString>());
				else
					p_BG.SetProxyAddresses(vector<PooledString>{ field.GetValueStr() });
			}
		}

		// FIXME:
		//m_ColumnCreatedOnBehalfOfUserId;
		//m_ColumnCreatedOnBehalfOfUserDisplayName;
		//m_ColumnCreatedOnBehalfOfUserPrincipalName;

		if (nullptr != m_ColumnOwnerUserIDs)
		{
			const auto& field = p_Row->GetField(m_ColumnOwnerUserIDs);
			if (hasValidErrorlessValue(field))
			{
				if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				{
					for (const auto& id : *field.GetValuesMulti<PooledString>())
					{
						auto user = BusinessUser(p_Grid.GetGraphCache().GetUserInCache(id, false));
						ASSERT(!user.GetID().IsEmpty());
						if (user.GetID().IsEmpty())
							user.SetID(id); // At least keep the id, even if the user is apparently not in cache.
						else
							ASSERT(user.GetID() == id);
						p_BG.AddOwner(user);
					}
				}
				else
				{
					auto user = BusinessUser(p_Grid.GetGraphCache().GetUserInCache(field.GetValueStr(), false));
					ASSERT(!user.GetID().IsEmpty());
					if (user.GetID().IsEmpty())
						user.SetID(field.GetValueStr()); // At least keep the id, even if the user is apparently not in cache.
					else
						ASSERT(user.GetID() == field.GetValueStr());
					p_BG.AddOwner(user);
				}
			}
		}

		if (nullptr != m_ColumnIsYammer)
		{
			const auto& field = p_Row->GetField(m_ColumnIsYammer);
			if (hasValidErrorlessValue(field))
				p_BG.SetIsYammer(field.GetValueBool());
		}

		// This column must be processed before m_ColumnIsTeam
		if (nullptr != m_ColumnResourceProvisioningOptions)
		{
			const auto& field = p_Row->GetField(m_ColumnResourceProvisioningOptions);
			if (hasValidErrorlessValue(field))
			{
				if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
					p_BG.SetResourceProvisioningOptions(*field.GetValuesMulti<PooledString>());
				else
					p_BG.SetResourceProvisioningOptions(vector<PooledString>{ field.GetValueStr() });
			}
		}

		if (nullptr != m_ColumnIsTeam)
		{
			const auto& field = p_Row->GetField(m_ColumnIsTeam);
			if (hasValidErrorlessValue(field))
			{
				if (m_IconIsTeam == field.GetIcon())
				{
					if (field.IsModified())
					{
						p_BG.SetEditHasTeam(true);
					}
					else
					{
						// FIXME: This assert fails after a "convert to team". Check it's relevant.
						ASSERT(nullptr == m_ColumnResourceProvisioningOptions || p_BG.GetIsTeam());
						if (nullptr == m_ColumnResourceProvisioningOptions)
							p_BG.SetResourceProvisioningOptions(vector<PooledString>{ BusinessGroup::g_TeamProvisionningOption });
					}
				}
			}
		}

		if (nullptr != m_ColumnGroupUnifiedSettingActivated)
        {
            const auto& field = p_Row->GetField(m_ColumnGroupUnifiedSettingActivated);
			if (hasValidErrorlessValue(field))
                p_BG.SetGroupUnifiedGuestSettingTemplateActivated(field.GetValueBool());
        }

		if (nullptr != m_ColumnSettingAllowToAddGuests)
        {
            const auto& field = p_Row->GetField(m_ColumnSettingAllowToAddGuests);
			if (hasValidErrorlessValue(field))
                p_BG.SetSettingAllowToAddGuests(field.GetValueBool());
        }

		if (nullptr != m_ColumnPreferredDataLocation)
		{
			const auto& field = p_Row->GetField(m_ColumnPreferredDataLocation);
			if (hasValidErrorlessValue(field))
				p_BG.SetPreferredDataLocation(PooledString(field.GetValueStr()));
		}

		if (p_BG.GetIsTeam() || (p_BG.GetEditIsTeam().is_initialized() && *p_BG.GetEditIsTeam()))
		{
			if (p_Grid.isTeam(p_Row))
				p_BG.SetTeam(GetTeam(p_Row));
			p_BG.SetHasTeamInfo(true);

			// FIXME: bad design
			{
				if (nullptr != m_ColumnTeamIsArchived)
				{
					const auto& field = p_Row->GetField(m_ColumnTeamIsArchived);
					if (hasValidErrorlessValue(field) && field.IsModified())
						p_BG.SetTeamArchived(m_IconIsArchived == field.GetIcon());
				}

				if (nullptr != m_ColumnSpoSiteReadOnlyForMembers)
				{
					const auto& field = p_Row->GetField(m_ColumnSpoSiteReadOnlyForMembers);
					if (hasValidErrorlessValue(field))
						p_BG.SetSpoSiteReadOnlyForMembers(field.GetValueBool());
				}
			}
		}		

		if (nullptr != m_ColumnResourceBehaviorOptions)
		{
			const auto& field = p_Row->GetField(m_ColumnResourceBehaviorOptions);
			if (hasValidErrorlessValue(field))
			{
				if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
					p_BG.SetResourceBehaviorOptions(*field.GetValuesMulti<PooledString>());
				else
					p_BG.SetResourceBehaviorOptions(vector<PooledString>{ field.GetValueStr() });
			}
		}

		if (nullptr != m_ColumnMembersCount)
		{
			const auto& field = p_Row->GetField(m_ColumnMembersCount);
			if (hasValidErrorlessValue(field))
				p_BG.SetMembersCount(static_cast<uint32_t>(field.GetValueLong()));
		}

		if (nullptr != m_ColumnHideFromOutlookClients)
		{
			const auto& field = p_Row->GetField(m_ColumnHideFromOutlookClients);
			if (hasValidErrorlessValue(field))
				p_BG.SetHideFromOutlookClients(field.GetValueBool());
		}

		if (nullptr != m_ColumnHideFromAddressLists)
		{
			const auto& field = p_Row->GetField(m_ColumnHideFromAddressLists);
			if (hasValidErrorlessValue(field))
				p_BG.SetHideFromAddressLists(field.GetValueBool());
		}

		if (nullptr != m_ColumnLicenseProcessingState)
		{
			const auto& field = p_Row->GetField(m_ColumnLicenseProcessingState);
			if (hasValidErrorlessValue(field))
			{
				LicenseProcessingState state;
				state.m_State = PooledString(field.GetValueStr());
				p_BG.SetLicenseProcessingState(state);
			}
		}

		if (nullptr != m_ColumnSecurityIdentifier)
		{
			const auto& field = p_Row->GetField(m_ColumnSecurityIdentifier);
			if (hasValidErrorlessValue(field))
				p_BG.SetSecurityIdentifier(PooledString(field.GetValueStr()));
		}

		if (nullptr != m_ColumnSkuIds && nullptr != m_ColumnSkuPartNumbers)
		{
			const auto& fieldIds = p_Row->GetField(m_ColumnSkuIds);
			const auto& fieldPNs = p_Row->GetField(m_ColumnSkuPartNumbers);
			if (hasValidErrorlessValue(fieldPNs))
			{
				if (fieldPNs.IsMulti() && nullptr != fieldPNs.GetValuesMulti<PooledString>())
				{
					ASSERT(nullptr != fieldIds.GetValuesMulti<PooledString>() && fieldIds.GetValuesMulti<PooledString>()->size() == fieldPNs.GetValuesMulti<PooledString>()->size());
					vector<BusinessAssignedLicense> assignedLicences;
					for (size_t i = 0; i < fieldPNs.GetValuesMulti<PooledString>()->size(); ++i)
					{
						assignedLicences.emplace_back();
						assignedLicences.back().SetSkuPartNumber((*fieldPNs.GetValuesMulti<PooledString>())[i]);
						assignedLicences.back().SetID((*fieldIds.GetValuesMulti<PooledString>())[i]);
					}
					p_BG.SetAssignedLicenses(assignedLicences);
				}
				else
				{
					if (BusinessGroupConfiguration::GetInstance().GetValueStringLicenses_Unlicensed() != fieldPNs.GetValueStr())
					{
						BusinessAssignedLicense assignedLicense;
						assignedLicense.SetSkuPartNumber(PooledString(fieldPNs.GetValueStr()));
						assignedLicense.SetID(fieldIds.GetValueStr());
						p_BG.SetAssignedLicenses(vector<BusinessAssignedLicense>{ assignedLicense });
					}
					else
					{
						p_BG.SetAssignedLicenses(vector<BusinessAssignedLicense>{ });
					}
				}
			}
		}

		/* Beta API */
		if (nullptr != m_ColumnMembershipRuleProcessingState)
		{
			const auto& field = p_Row->GetField(m_ColumnMembershipRuleProcessingState);
			if (hasValidErrorlessValue(field))
				p_BG.SetMembershipRuleProcessingState(PooledString(field.GetValueStr()));
		}

		if (nullptr != m_ColumnMembershipRule)
		{
			const auto& field = p_Row->GetField(m_ColumnMembershipRule);
			if (hasValidErrorlessValue(field))
				p_BG.SetMembershipRule(PooledString(field.GetValueStr()));
		}

		if (nullptr != m_ColumnTheme)
		{
			const auto& field = p_Row->GetField(m_ColumnTheme);
			if (hasValidErrorlessValue(field))
				p_BG.SetTheme(PooledString(field.GetValueStr()));
		}

		if (nullptr != m_ColumnPreferredLanguage)
		{
			const auto& field = p_Row->GetField(m_ColumnPreferredLanguage);
			if (hasValidErrorlessValue(field))
				p_BG.SetPreferredLanguage(PooledString(field.GetValueStr()));
		}
		/********/

		for (const auto& item : m_ColumnsRequestDateTime)
		{
			if (nullptr != item.second)
			{
				const auto& field = p_Row->GetField(item.second);
				if (hasValidErrorlessValue(field) && field.IsDate())
				{
					p_BG.SetDataDate(item.first, field.GetValueTimeDate());
				}
			}
		}

		if (p_BG.GetOnPremiseGroup())
		{
			// String properties
			if (nullptr != m_OnPremCols.m_ColCanonicalName)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColCanonicalName);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetCanonicalName(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColCN)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColCN);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetCN(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColDescription)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColDescription);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetDescription(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColDisplayName)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColDisplayName);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetDisplayName(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColDistinguishedName)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColDistinguishedName);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetDistinguishedName(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColGroupCategory)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColGroupCategory);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetGroupCategory(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColGroupScope)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColGroupScope);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetGroupScope(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColHomePage)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColHomePage);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetHomePage(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColLastKnownParent)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColLastKnownParent);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetLastKnownParent(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColManagedBy)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColManagedBy);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetManagedBy(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColName)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColName);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetName(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColObjectCategory)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColObjectCategory);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetObjectCategory(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColObjectClass)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColObjectClass);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetObjectClass(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColObjectGUID)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColObjectGUID);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetObjectGUID(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColObjectSid)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColObjectSid);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetObjectSid(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColSamAccountName)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColSamAccountName);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetSamAccountName(PooledString(field.GetValueStr()));
			}
			if (nullptr != m_OnPremCols.m_ColSID)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColSID);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetSID(PooledString(field.GetValueStr()));
			}

			// Bool properties
			if (nullptr != m_OnPremCols.m_ColDeleted)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColDeleted);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetDeleted(field.GetValueBool());
			}
			if (nullptr != m_OnPremCols.m_ColIsCriticalSystemObject)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColIsCriticalSystemObject);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetIsCriticalSystemObject(field.GetValueBool());
			}
			if (nullptr != m_OnPremCols.m_ColIsDeleted)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColIsDeleted);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetIsDeleted(field.GetValueBool());
			}
			if (nullptr != m_OnPremCols.m_ColProtectedFromAccidentalDeletion)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColProtectedFromAccidentalDeletion);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetProtectedFromAccidentalDeletion(field.GetValueBool());
			}

			// Int32 properties
			if (nullptr != m_OnPremCols.m_ColAdminCount)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColAdminCount);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetAdminCount(static_cast<int32_t>(field.GetValueDouble()));
			}
			if (nullptr != m_OnPremCols.m_ColGroupType)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColGroupType);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetGroupType(static_cast<int32_t>(field.GetValueDouble()));
			}
			if (nullptr != m_OnPremCols.m_ColInstanceType)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColInstanceType);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetInstanceType(static_cast<int32_t>(field.GetValueDouble()));
			}
			if (nullptr != m_OnPremCols.m_ColSAMAccountType)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColSAMAccountType);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetSAMAccountType(static_cast<int32_t>(field.GetValueDouble()));
			}
			if (nullptr != m_OnPremCols.m_ColSDRightsEffective)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColSDRightsEffective);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetSDRightsEffective(static_cast<int32_t>(field.GetValueDouble()));
			}
			if (nullptr != m_OnPremCols.m_ColSystemFlags)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColSystemFlags);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetSystemFlags(static_cast<int32_t>(field.GetValueDouble()));
			}

			// Date properties
			if (nullptr != m_OnPremCols.m_ColCreated)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColCreated);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetCreated(field.GetValueTimeDate());
			}
			if (nullptr != m_OnPremCols.m_ColCreateTimeStamp)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColCreateTimeStamp);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetCreateTimeStamp(field.GetValueTimeDate());
			}
			if (nullptr != m_OnPremCols.m_ColModified)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColModified);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetModified(field.GetValueTimeDate());
			}
			if (nullptr != m_OnPremCols.m_ColModifyTimeStamp)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColModifyTimeStamp);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetModifyTimeStamp(field.GetValueTimeDate());
			}
			if (nullptr != m_OnPremCols.m_ColUSNChanged)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColUSNChanged);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetUSNChanged(field.GetValueTimeDate());
			}
			if (nullptr != m_OnPremCols.m_ColUSNCreated)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColUSNCreated);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetUSNCreated(field.GetValueTimeDate());
			}
			if (nullptr != m_OnPremCols.m_ColWhenChanged)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColWhenChanged);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetWhenChanged(field.GetValueTimeDate());
			}
			if (nullptr != m_OnPremCols.m_ColWhenCreated)
			{
				const auto& field = p_Row->GetField(m_OnPremCols.m_ColWhenCreated);
				if (hasValidErrorlessValue(field))
					p_BG.GetOnPremiseGroup()->SetWhenCreated(field.GetValueTimeDate());
			}
		}

		// Do we still need this?
		/*if (nullptr != m_ColumnLastRequestDateTime)
		{
			const auto& field = p_Row->GetField(m_ColumnLastRequestDateTime);
			if (hasValidErrorlessValue(field))
				p_BG.SetLastRequestTime(field.GetValueTimeDate());
		}*/
	}
}

void GridTemplateGroups::GetListOfFieldErrors(const O365Grid& p_Grid, GridBackendRow* p_Row, std::set<wstring>& p_ListFieldErrors) const
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		// TODO: other fields?
		for (auto col : p_Grid.GetBackendColumns() /*{ m_ColumnDisplayName
						, m_ColumnDescription
						, m_ColumnVisibility
						, m_ColumnPreferredDataLocation
						, m_ColumnMembershipRuleProcessingState
						, m_ColumnMembershipRule
						, m_ColumnTheme
						, m_ColumnMailNickname
						, m_ColumnAllowExternalSenders
						, m_ColumnAutoSubscribeNewMembers
						, m_ColumnHideFromOutlookClients
						, m_ColumnHideFromAddressLists
						, m_ColumnGroupUnifiedSettingActivated
						, m_ColumnSettingAllowToAddGuests
						, m_ColumnTeamMemberSettingsAllowCreateUpdateChannels
						, m_ColumnTeamMemberSettingsAllowCreatePrivateChannels
						, m_ColumnTeamMemberSettingsAllowDeleteChannels
						, m_ColumnTeamMemberSettingsAllowAddRemoveApps
						, m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs
						, m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors

						, m_ColumnTeamGuestSettingsAllowCreateUpdateChannels
						, m_ColumnTeamGuestSettingsAllowDeleteChannels
						, m_ColumnTeamMessagingSettingsAllowUserEditMessages
						, m_ColumnTeamMessagingSettingsAllowUserDeleteMessages
						, m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages
						, m_ColumnTeamMessagingSettingsAllowTeamMentions
						, m_ColumnTeamMessagingSettingsAllowChannelMentions
						, m_ColumnTeamFunSettingsAllowGiphy
						, m_ColumnTeamFunSettingsGiphyContentRating
						, m_ColumnTeamFunSettingsAllowStickersAndMemes
						, m_ColumnTeamFunSettingsAllowCustomMemes
						}*/
			)
		{
			ASSERT(nullptr != col);
			//if (nullptr != col)
			{
				if (isError(p_Row->GetField(col)))
					p_ListFieldErrors.insert(col->GetUniqueID());
			}
		}
	}
}

row_pk_t GridTemplateGroups::CreateRowPk(const OnPremiseGroup& p_OnPremGroup)
{
	row_pk_t pk;
	pk.emplace_back(p_OnPremGroup.GetObjectSid() ? *p_OnPremGroup.GetObjectSid() : PooledString::g_EmptyString, m_ColumnID->GetID());

	return pk;
}

bool GridTemplateGroups::GetOnPremiseGroup(const O365Grid& p_Grid, GridBackendRow* p_Row, OnPremiseGroup& p_OnPremGroup) const
{
	if (nullptr != p_Row
		&& (IsOnPremMetatype(*p_Row) || IsHybridMetatype(*p_Row)))
	{
		for (auto c : m_OnPremCols.GetColumns())
			m_OnPremCols.SetValue(p_OnPremGroup, c, p_Row->GetField(c));
		return true;
	}

	return false;
}

void GridTemplateGroups::updateRowCreatedOnBehalfOf(const BusinessGroup& p_BG, GridBackendRow* p_RowToUpdate, const GraphCache& p_GraphCache)
{
	if (nullptr != m_ColumnCreatedOnBehalfOfUserId || nullptr != m_ColumnCreatedOnBehalfOfUserDisplayName || nullptr != m_ColumnCreatedOnBehalfOfUserPrincipalName)
	{
		if (p_BG.GetUserCreatedOnBehalfOfError())
		{
			if (nullptr != m_ColumnCreatedOnBehalfOfUserId)
				p_RowToUpdate->AddField(p_BG.GetUserCreatedOnBehalfOfError()->GetFullErrorMessage(), m_ColumnCreatedOnBehalfOfUserId, GridBackendUtil::ICON_ERROR);
			if (nullptr != m_ColumnCreatedOnBehalfOfUserDisplayName)
				p_RowToUpdate->AddField(p_BG.GetUserCreatedOnBehalfOfError()->GetFullErrorMessage(), m_ColumnCreatedOnBehalfOfUserDisplayName, GridBackendUtil::ICON_ERROR);
			if (nullptr != m_ColumnCreatedOnBehalfOfUserPrincipalName)
				p_RowToUpdate->AddField(p_BG.GetUserCreatedOnBehalfOfError()->GetFullErrorMessage(), m_ColumnCreatedOnBehalfOfUserPrincipalName, GridBackendUtil::ICON_ERROR);
		}
		else
		{
			if (p_BG.GetUserCreatedOnBehalfOf())
			{
				if (nullptr != m_ColumnCreatedOnBehalfOfUserId)
				{
					if (!p_BG.GetUserCreatedOnBehalfOf()->IsEmpty())
						p_RowToUpdate->AddField(*p_BG.GetUserCreatedOnBehalfOf(), m_ColumnCreatedOnBehalfOfUserId);
					else
						p_RowToUpdate->RemoveField(m_ColumnCreatedOnBehalfOfUserId);
				}

				if (nullptr != m_ColumnCreatedOnBehalfOfUserDisplayName || nullptr != m_ColumnCreatedOnBehalfOfUserPrincipalName)
				{
					auto userCreatedOnBehalf = p_GraphCache.GetUserInCache(*p_BG.GetUserCreatedOnBehalfOf(), false);
					ASSERT(userCreatedOnBehalf.GetID() == *p_BG.GetUserCreatedOnBehalfOf() && !userCreatedOnBehalf.GetUserPrincipalName().IsEmpty());
					if (userCreatedOnBehalf.GetID() != *p_BG.GetUserCreatedOnBehalfOf())
						userCreatedOnBehalf = CachedUser();
					if (nullptr != m_ColumnCreatedOnBehalfOfUserDisplayName)
					{
						if (!userCreatedOnBehalf.GetDisplayName().IsEmpty())
							p_RowToUpdate->AddField(userCreatedOnBehalf.GetDisplayName(), m_ColumnCreatedOnBehalfOfUserDisplayName);
						else
							p_RowToUpdate->RemoveField(m_ColumnCreatedOnBehalfOfUserDisplayName);
					}

					if (nullptr != m_ColumnCreatedOnBehalfOfUserPrincipalName)
					{
						if (!userCreatedOnBehalf.GetUserPrincipalName().IsEmpty())
							p_RowToUpdate->AddField(userCreatedOnBehalf.GetUserPrincipalName(), m_ColumnCreatedOnBehalfOfUserPrincipalName);
						else
							p_RowToUpdate->RemoveField(m_ColumnCreatedOnBehalfOfUserPrincipalName);
					}
				}
			}
			else
			{
				if (nullptr != m_ColumnCreatedOnBehalfOfUserId)
					p_RowToUpdate->RemoveField(m_ColumnCreatedOnBehalfOfUserId);
				if (nullptr != m_ColumnCreatedOnBehalfOfUserDisplayName)
					p_RowToUpdate->RemoveField(m_ColumnCreatedOnBehalfOfUserDisplayName);
				if (nullptr != m_ColumnCreatedOnBehalfOfUserPrincipalName)
					p_RowToUpdate->RemoveField(m_ColumnCreatedOnBehalfOfUserPrincipalName);
			}
		}
	}
}

void GridTemplateGroups::SetRowIsTeam(const BusinessGroup& p_BG, GridBackendRow* p_RowToUpdate, GridBackendColumn* p_IsTeamColumn, UpdateFieldOption p_UpdateFieldOption)
{
	if (p_BG.GetIsTeam() || (p_BG.GetEditIsTeam() && *p_BG.GetEditIsTeam()))
	{
		auto field = updateField(p_BG.GetValue(_YUID(O365_GROUP_ISTEAM)), p_RowToUpdate, p_IsTeamColumn, p_UpdateFieldOption);
		if (nullptr != field && !field->IsGeneric())
			field->SetIcon(m_IconIsTeam);
	}
	else if (nullptr != p_IsTeamColumn)
	{
		if (UpdateFieldOption::NONE != p_UpdateFieldOption
			&& p_RowToUpdate->HasField(p_IsTeamColumn)
			&& p_RowToUpdate->GetField(p_IsTeamColumn).IsModified())
		{
			O365Grid* grid = nullptr != p_RowToUpdate ? dynamic_cast<O365Grid*>(p_RowToUpdate->GetBackend()->GetCacheGrid()) : nullptr;
			ASSERT(nullptr != grid);
			if (nullptr != grid)
			{
				vector<GridBackendField> rowPK;
				if (UpdateFieldOption::NONE != p_UpdateFieldOption)
					grid->GetRowPK(p_RowToUpdate, rowPK);
				grid->GetModifications().RevertFieldUpdates(rowPK, { p_IsTeamColumn->GetID() });
			}
		}

		p_RowToUpdate->RemoveField(p_IsTeamColumn);
	}
}

const boost::YOpt<Team> GridTemplateGroups::GetTeam(const GridBackendRow* p_Row) const
{
	// FIXME: Finish this!

	ASSERT(isTeam(p_Row));

	boost::YOpt<Team> team;

	team.emplace();

	if (hasTeamMemberSettingsColumn())
	{
		team->MemberSettings.emplace();

		bool hasOne = false;
		if (nullptr != m_ColumnTeamMemberSettingsAllowCreateUpdateChannels && p_Row->HasField(m_ColumnTeamMemberSettingsAllowCreateUpdateChannels))
		{
			const auto& field = p_Row->GetField(m_ColumnTeamMemberSettingsAllowCreateUpdateChannels->GetID());
			team->MemberSettings->SetAllowCreateUpdateChannels(field.GetValueBool());
			hasOne = true;
		}

		if (nullptr != m_ColumnTeamMemberSettingsAllowCreatePrivateChannels && p_Row->HasField(m_ColumnTeamMemberSettingsAllowCreatePrivateChannels))
		{
			const auto& field = p_Row->GetField(m_ColumnTeamMemberSettingsAllowCreatePrivateChannels->GetID());
			team->MemberSettings->SetAllowCreatePrivateChannels(field.GetValueBool());
			hasOne = true;
		}

		if (nullptr != m_ColumnTeamMemberSettingsAllowDeleteChannels && p_Row->HasField(m_ColumnTeamMemberSettingsAllowDeleteChannels))
		{
			const auto& field = p_Row->GetField(m_ColumnTeamMemberSettingsAllowDeleteChannels->GetID());
			team->MemberSettings->SetAllowDeleteChannels(field.GetValueBool());
			hasOne = true;
		}

		if (nullptr != m_ColumnTeamMemberSettingsAllowAddRemoveApps && p_Row->HasField(m_ColumnTeamMemberSettingsAllowAddRemoveApps))
		{
			const auto& field = p_Row->GetField(m_ColumnTeamMemberSettingsAllowAddRemoveApps->GetID());
			team->MemberSettings->SetAllowAddRemoveApps(field.GetValueBool());
			hasOne = true;
		}

		if (nullptr != m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs && p_Row->HasField(m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs))
		{
			const auto& field = p_Row->GetField(m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs->GetID());
			team->MemberSettings->SetAllowCreateUpdateRemoveTabs(field.GetValueBool());
			hasOne = true;
		}

		if (nullptr != m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors && p_Row->HasField(m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors))
		{
			const auto& field = p_Row->GetField(m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors->GetID());
			team->MemberSettings->SetAllowCreateUpdateRemoveConnectors(field.GetValueBool());
			hasOne = true;
		}

		if (!hasOne)
			team->MemberSettings.reset();
	}

	if (hasTeamGuestSettingsColumn())
	{
		team->GuestSettings.emplace();

		bool hasOne = false;
		if (nullptr != m_ColumnTeamGuestSettingsAllowCreateUpdateChannels)
		{
			const auto& field = p_Row->GetField(m_ColumnTeamGuestSettingsAllowCreateUpdateChannels->GetID());
			if (hasValidErrorlessValue(field))
			{
				team->GuestSettings->SetAllowCreateUpdateChannels(field.GetValueBool());
				hasOne = true;
			}
		}

		if (nullptr != m_ColumnTeamGuestSettingsAllowDeleteChannels)
		{
			const auto& field = p_Row->GetField(m_ColumnTeamGuestSettingsAllowDeleteChannels->GetID());
			if (hasValidErrorlessValue(field))
			{
				team->GuestSettings->SetAllowDeleteChannels(field.GetValueBool());
				hasOne = true;
			}
		}

		if (!hasOne)
			team->GuestSettings.reset();
	}

	if (hasTeamMessagingSettingsColumn())
	{
		team->MessagingSettings.emplace();

		bool hasOne = false;
		if (nullptr != m_ColumnTeamMessagingSettingsAllowUserEditMessages)
		{
			const auto& field = p_Row->GetField(m_ColumnTeamMessagingSettingsAllowUserEditMessages->GetID());
			if (hasValidErrorlessValue(field))
			{
				team->MessagingSettings->SetAllowUserEditMessages(field.GetValueBool());
				hasOne = true;
			}
		}

		if (nullptr != m_ColumnTeamMessagingSettingsAllowUserDeleteMessages)
		{
			const auto& field = p_Row->GetField(m_ColumnTeamMessagingSettingsAllowUserDeleteMessages->GetID());
			if (hasValidErrorlessValue(field))
			{
				team->MessagingSettings->SetAllowUserDeleteMessages(field.GetValueBool());
				hasOne = true;
			}
		}

		if (nullptr != m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages)
		{
			const auto& field = p_Row->GetField(m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages->GetID());
			if (hasValidErrorlessValue(field))
			{
				team->MessagingSettings->SetAllowOwnerDeleteMessages(field.GetValueBool());
				hasOne = true;
			}
		}

		if (nullptr != m_ColumnTeamMessagingSettingsAllowTeamMentions)
		{
			const auto& field = p_Row->GetField(m_ColumnTeamMessagingSettingsAllowTeamMentions->GetID());
			if (hasValidErrorlessValue(field))
			{
				team->MessagingSettings->SetAllowTeamMentions(field.GetValueBool());
				hasOne = true;
			}
		}

		if (nullptr != m_ColumnTeamMessagingSettingsAllowChannelMentions)
		{
			const auto& field = p_Row->GetField(m_ColumnTeamMessagingSettingsAllowChannelMentions->GetID());
			if (hasValidErrorlessValue(field))
			{
				team->MessagingSettings->SetAllowChannelMentions(field.GetValueBool());
				hasOne = true;
			}
		}

		if (!hasOne)
			team->GuestSettings.reset();
	}

	if (hasTeamFunSettingsColumn())
	{
		team->FunSettings.emplace();

		bool hasOne = false;
		if (nullptr != m_ColumnTeamFunSettingsAllowCustomMemes)
		{
			const auto& field = p_Row->GetField(m_ColumnTeamFunSettingsAllowCustomMemes->GetID());
			if (hasValidErrorlessValue(field))
			{
				team->FunSettings->SetAllowCustomMemes(field.GetValueBool());
				hasOne = true;
			}
		}

		if (nullptr != m_ColumnTeamFunSettingsAllowGiphy)
		{
			const auto& field = p_Row->GetField(m_ColumnTeamFunSettingsAllowGiphy->GetID());
			if (hasValidErrorlessValue(field))
			{
				team->FunSettings->SetAllowGiphy(field.GetValueBool());
				hasOne = true;
			}
		}

		if (nullptr != m_ColumnTeamFunSettingsAllowStickersAndMemes)
		{
			const auto& field = p_Row->GetField(m_ColumnTeamFunSettingsAllowStickersAndMemes->GetID());
			if (hasValidErrorlessValue(field))
			{
				team->FunSettings->SetAllowStickersAndMemes(field.GetValueBool());
				hasOne = true;
			}
		}

		if (nullptr != m_ColumnTeamFunSettingsGiphyContentRating)
		{
			const auto& field = p_Row->GetField(m_ColumnTeamFunSettingsGiphyContentRating->GetID());
			if (hasValidErrorlessValue(field))
			{
				team->FunSettings->SetGiphyContentRating(PooledString(field.GetValueStr()));
				hasOne = true;
			}
		}

		if (!hasOne)
			team->FunSettings.reset();
	}

	if (nullptr != m_ColumnTeamWebUrl)
	{
		const auto& field = p_Row->GetField(m_ColumnTeamWebUrl->GetID());
		if (hasValidErrorlessValue(field))
			team->WebUrl = field.GetValueStr();
	}

	if (nullptr != m_ColumnTeamIsArchived)
	{
		const auto& field = p_Row->GetField(m_ColumnTeamIsArchived->GetID());
		if (hasValidErrorlessValue(field))
		{
			// FIXME: bad design
			if (field.IsModified())
				team->IsArchived = !(m_IconIsArchived == field.GetIcon());
			else
				team->IsArchived = m_IconIsArchived == field.GetIcon();
		}
	}

	if (nullptr != m_ColumnTeamInternalId)
	{
		const auto& field = p_Row->GetField(m_ColumnTeamInternalId->GetID());
		if (hasValidErrorlessValue(field))
			team->InternalId = field.GetValueStr();
	}

	// Don't do this, fails for team creation
	/*if (!team->MemberSettings
		&& !team->GuestSettings
		&& !team->FunSettings
		&& !team->WebUrl
		&& !team->IsArchived
		&& !team->InternalId)
		team.reset();*/

	return team;
}

bool GridTemplateGroups::IsTeamColumn(GridBackendColumn* p_Column) const
{
	return p_Column == m_ColumnIsTeam
		|| p_Column == m_ColumnTeamWebUrl
		|| isTeamMemberSettingsColumn(p_Column)
		|| isTeamGuestSettingsColumn(p_Column)
		|| isTeamMessagingSettingsColumn(p_Column)
		|| isTeamFunSettingsColumn(p_Column)
		|| p_Column == m_ColumnTeamInternalId
		// Only called by crappy UpdatedObjectsGenerator and in that context, those columns don't belong to 'Team'.
		/*|| p_Column == m_ColumnTeamIsArchived
		|| p_Column == m_ColumnSpoSiteReadOnlyForMembers*/
		;
}

GridBackendColumn*& GridTemplateGroups::GetColumnForProperty(const wstring& p_PropertyName)
{
	return getColumnForProperty(*this, p_PropertyName);
}

GridBackendColumn* GridTemplateGroups::AddDefaultColumnFor(O365Grid& p_Grid, const wstring& p_PropertyName, const wstring& p_FamilyName, const set<GridBackendUtil::COLUMNPRESETFLAGS>& p_Flags)
{
	return AddDefaultColumnFor(p_Grid, p_PropertyName, p_FamilyName, p_Flags, {}, {});
}

GridBackendColumn* GridTemplateGroups::AddDefaultColumnFor(O365Grid& p_Grid, const wstring& p_PropertyName, const wstring& p_FamilyName, const set<GridBackendUtil::COLUMNPRESETFLAGS>& p_Flags, const wstring& p_CustomName, const wstring& p_CustomUniqueID)
{
	auto& col = GetColumnForProperty(p_PropertyName);
	const auto& gcfg = BusinessGroupConfiguration::GetInstance();

	wstring title;
	if (p_CustomName.empty())
		title = gcfg.GetTitle(p_PropertyName);
	else if (Str::contains(p_CustomName, _YTEXT("%s")))
		title = _YTEXTFORMAT(p_CustomName.c_str(), gcfg.GetTitle(p_PropertyName).c_str());
	else
		title = p_CustomName;

	wstring uniqueID;
	if (p_CustomUniqueID.empty())
		uniqueID = p_PropertyName;
	else if (Str::contains(p_CustomUniqueID, _YTEXT("%s")))
		uniqueID = _YTEXTFORMAT(p_CustomUniqueID.c_str(), p_PropertyName.c_str());
	else
		uniqueID = p_CustomUniqueID;

	if (p_PropertyName == _YUID(O365_GROUP_DISPLAYNAME))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize22, p_Flags);
	else if (p_PropertyName == _YUID("metaType"))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize4, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_ISTEAM))
	{
		col = p_Grid.AddColumnIcon(uniqueID, title, p_FamilyName);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID(YTRIA_GROUP_ISYAMMER))
	{
		col = p_Grid.AddColumnIcon(uniqueID, title, p_FamilyName);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("isArchived"))
	{
		col =  p_Grid.AddColumnIcon(uniqueID, title, p_FamilyName);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("setSpoSiteReadOnlyForMembers"))
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_GROUPTYPE))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_DYNAMICMEMBERSHIP))
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_DESCRIPTION))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize32, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_VISIBILITY))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_CREATEDDATETIME))
		col = p_Grid.AddColumnDate(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize16, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_MAILNICKNAME))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize22, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_MAIL))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize32, p_Flags);
	else if (p_PropertyName == _YUID("memberCount"))
		col = p_Grid.AddColumnNumberInt(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize8, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_CLASSIFICATION))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize22, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_PROXYADDRESSES))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_RESOURCEBEHAVIOROPTIONS))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize22, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_RESOURCEPROVISIONINGOPTIONS))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize22, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_PREFERREDDATALOCATION))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_MEMBERSHIPRULE))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize22, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_THEME))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_PREFERREDLANGUAGE))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_ONPREMISESLASTSYNCDATETIME))
		col = p_Grid.AddColumnDate(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize16, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_ONPREMISESSECURITYIDENTIFIER))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_ONPREMISESSYNCENABLED))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY)
		|| p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME)
		|| p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR)
		|| p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE))
	{
		if (p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		else if (p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME))
			col = p_Grid.AddColumnDate(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize16, p_Flags);
		else if (p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		else if (p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);

		if (nullptr != m_ColumnOPPECategory && nullptr != m_ColumnOPPEOccuredDateTime && nullptr != m_ColumnOPPEProperty && nullptr != m_ColumnOPPEValue)
		{
			m_ColumnOPPECategory->SetMultivalueFamily(YtriaTranslate::Do(GridGroups_customizeGrid_71, _YLOC("On-Premises Provisioning Errors")).c_str());
			m_ColumnOPPECategory->AddMultiValueExplosionSister(m_ColumnOPPEOccuredDateTime);
			m_ColumnOPPECategory->AddMultiValueExplosionSister(m_ColumnOPPEProperty);
			m_ColumnOPPECategory->AddMultiValueExplosionSister(m_ColumnOPPEValue);
		}
	}
	else if (p_PropertyName == _YUID(O365_GROUP_GROUPTYPES))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_SECURITYENABLED))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID(O365_GROUP_MAILENABLED))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID(O365_GROUP_ALLOWEXTERNALSENDERS))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID(O365_GROUP_UNSEENCOUNT))
		col = p_Grid.AddColumnNumberInt(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_AUTOSUBSCRIBENEWMEMBERS))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("ownerusernames") || p_PropertyName == _YUID("owneruserids"))
	{
		if (p_PropertyName == _YUID("ownerusernames"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		if (p_PropertyName == _YUID("owneruserids"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);

		if (nullptr != m_ColumnOwnerUserNames && nullptr != m_ColumnOwnerUserIDs)
		{
			m_ColumnOwnerUserNames->SetMultivalueFamily(YtriaTranslate::Do(GridGroups_customizeGrid_58, _YLOC("Owner")).c_str());
			m_ColumnOwnerUserNames->AddMultiValueExplosionSister(m_ColumnOwnerUserIDs);
		}
	}
	else if (p_PropertyName == _YUID("statusAllowToAddGuests"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID(YTRIA_GROUP_GROUPUNIFIEDGUESTSETTINGACTIVATED))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTS))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("createdOnBehalfOfUserId"))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID("createdOnBehalfOfUserDisplayName"))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID("createdOnBehalfOfUserPrincipalName"))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_RENEWEDDATETIME))
		col = p_Grid.AddColumnDate(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize16, p_Flags);
	else if (p_PropertyName == _YUID("expirationPolicyStatus"))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize16, p_Flags);
	else if (p_PropertyName == _YUID("expireOn"))
		col = p_Grid.AddColumnDate(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize16, p_Flags);
	else if (p_PropertyName == _YUID("teamMemberSettingsAllowCreateUpdateChannels"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("teamMemberSettingsAllowCreatePrivateChannels"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("teamMemberSettingsAllowDeleteChannels"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("teamMemberSettingsAllowAddRemoveApps"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("teamMemberSettingsAllowCreateUpdateRemoveTabs"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("teamMemberSettingsAllowCreateUpdateRemoveConnectors"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("guestSettingsAllowCreateUpdateChannels"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("guestSettingsAllowDeleteChannels"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("messagingSettingsAllowUserEditMessages"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("messagingSettingsAllowUserDeleteMessages"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("messagingSettingsAllowOwnerDeleteMessages"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("messagingSettingsAllowTeamMentions"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("messagingSettingsAllowChannelMentions"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("funSettingsAllowGiphy"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("funSettingsGiphyContentRating"))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID("funSettingsAllowStickersAndMemes"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("funSettingsAllowCustomMemes"))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("webUrl"))
		col = p_Grid.AddColumnHyperlink(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID("internalId"))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVENAME))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEID))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEDESCRIPTION))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEDESCRIPTION))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEQUOTASTATE))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEQUOTAUSED))
	{
		col = p_Grid.AddColumnNumber(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		col->SetXBytesColumnFormat();
	}
	else if (p_PropertyName == _YUID(O365_COLDRIVEQUOTAREMAINING))
	{
		col = p_Grid.AddColumnNumber(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		col->SetXBytesColumnFormat();
	}
	else if (p_PropertyName == _YUID(O365_COLDRIVEQUOTADELETED))
	{
		col = p_Grid.AddColumnNumber(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		col->SetXBytesColumnFormat();
	}
	else if (p_PropertyName == _YUID(O365_COLDRIVEQUOTATOTAL))
	{
		col = p_Grid.AddColumnNumber(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		col->SetXBytesColumnFormat();
	}
	else if (p_PropertyName == _YUID(O365_COLDRIVECREATIONTIME))
		col = p_Grid.AddColumnDate(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVELASTMODIFIEDDATETIME))
		col = p_Grid.AddColumnDate(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVETYPE))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEWEBURL))
		col = p_Grid.AddColumnHyperlink(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVECREATEDBYUSEREMAIL))
		col	= p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVECREATEDBYUSERID))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVECREATEDBYAPPNAME))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVECREATEDBYAPPID))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVECREATEDBYDEVICENAME))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVECREATEDBYDEVICEID))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEOWNERUSERNAME))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEOWNERUSERID))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEOWNERUSEREMAIL))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEOWNERAPPNAME))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEOWNERAPPID))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEOWNERDEVICENAME))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEOWNERDEVICEID))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_HIDEFROMOUTLOOKCLIENTS))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID(O365_GROUP_HIDEFROMADDRESSLISTS))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID(O365_GROUP_LICENSEPROCESSINGSTATE))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_SECURITYIDENTIFIER))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_GROUP_ASSIGNEDLICENSES) || p_PropertyName == _YUID("assignedLicenses.skuId") || p_PropertyName == _YUID("ASSIGNEDLICENSENAMES"))
	{
		if (p_PropertyName == _YUID(O365_GROUP_ASSIGNEDLICENSES))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize32, p_Flags);
		else if (p_PropertyName == _YUID("ASSIGNEDLICENSENAMES"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize32, p_Flags);
		else if (p_PropertyName == _YUID("assignedLicenses.skuId"))
			col = p_Grid.AddColumnCaseSensitive(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);

		std::vector<GridBackendColumn*> cols{ m_ColumnSkuPartNumbers, m_ColumnSkuNames, m_ColumnSkuIds };
		cols.erase(std::remove_if(cols.begin(), cols.end(), [](auto& c) {return nullptr == c; }), cols.end());
		if (cols.size() > 1)
		{
			for (auto c : cols)
				c->ClearExplosionSisters();

			cols[0]->SetMultivalueFamily(YtriaTranslate::Do(GridUsers_customizeGrid_61, _YLOC("Assigned Licenses")).c_str());
			for (size_t i = 1; i < cols.size(); ++i)
				cols[0]->AddMultiValueExplosionSister(cols[i]);
		}
	}
	else
	{
		ASSERT(false);
		ASSERT(nullptr == col);
	}

	return col;
}

void GridTemplateGroups::CustomizeGrid(O365Grid& p_Grid)
{
	GridTemplate::CustomizeGrid(p_Grid);

	AddIconTeam(p_Grid);
	m_IconIsArchived = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_IS_ARCHIVED));
	//m_IconIsNotArchived = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_IS_NOT_ARCHIVED));
	m_IconIsYammer = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_IS_YAMMER));
}

bool GridTemplateGroups::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	if (p_Field.HasValue())
	{
		if (&p_Column == m_ColumnIsTeam)
		{
			if (p_Field.GetIcon() == m_IconIsTeam)
				p_Value = _YTEXT("1");
			else
				p_Value = _YTEXT("0");
			return true;
		}

		if (&p_Column == m_ColumnIsYammer)
		{
			if (p_Field.GetIcon() == m_IconIsYammer)
				p_Value = _YTEXT("1");
			else
				p_Value = _YTEXT("0");
			return true;
		}

		if (&p_Column == m_ColumnTeamIsArchived)
		{
			if (p_Field.GetIcon() == m_IconIsArchived)
				p_Value = _YTEXT("1");
			else
				p_Value = _YTEXT("0");
			return true;
		}

		if (&p_Column == m_ColumnSkuPartNumbers || &p_Column == m_ColumnSkuNames)
		{
			if (p_Field.GetValueStr() == BusinessGroupConfiguration::GetInstance().GetValueStringLicenses_Unlicensed())
			{
				p_Value = _YTEXT("0");
				return true;
			}
		}
	}

	return false;
}

bool GridTemplateGroups::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (&p_Column == m_ColumnIsTeam)
	{
		ASSERT(!p_Value.empty());
		if (p_Value == _YTEXT("1"))
		{
			auto& field = p_Row.AddField(BusinessGroup::g_Team, m_ColumnIsTeam, m_IconIsTeam);
			field.RemoveModified();
		}
		else
		{
			ASSERT(p_Value == _YTEXT("0"));
			p_Row.RemoveField(m_ColumnIsTeam);
		}

		return true;
	}

	if (&p_Column == m_ColumnIsYammer)
	{
		ASSERT(!p_Value.empty());
		if (p_Value == _YTEXT("1"))
		{
			auto& field = p_Row.AddField(BusinessGroup::g_Yammer, m_ColumnIsYammer, m_IconIsTeam);
			field.RemoveModified();
		}
		else
		{
			ASSERT(p_Value == _YTEXT("0"));
			p_Row.RemoveField(m_ColumnIsYammer);
		}

		return true;
	}

	if (&p_Column == m_ColumnTeamIsArchived)
	{
		ASSERT(!p_Value.empty());
		if (p_Value == _YTEXT("1"))
		{
			auto& field = p_Row.AddField(BusinessGroup::g_Archived, m_ColumnTeamIsArchived, m_IconIsArchived);
			field.RemoveModified();
		}
		else
		{
			ASSERT(p_Value == _YTEXT("0"));
			p_Row.RemoveField(m_ColumnTeamIsArchived);
		}

		return true;
	}

	if (&p_Column == m_ColumnSkuPartNumbers || &p_Column == m_ColumnSkuNames)
	{
		if (p_Value == _YTEXT("0"))
		{
			p_Row.AddField(BusinessGroupConfiguration::GetInstance().GetValueStringLicenses_Unlicensed(), &p_Column);
			return true;
		}
	}

	return false;
}

void GridTemplateGroups::SetO365(O365Grid& p_Grid, GridBackendRow& p_Row)
{
	GridUtil::SetGroupRowObjectType(p_Grid, &p_Row, BusinessGroup());
	SetO365Metatype(p_Row);
}

void GridTemplateGroups::SetOnPrem(O365Grid& p_Grid, GridBackendRow& p_Row)
{
	GridUtil::SetRowObjectType(p_Grid, &p_Row, OnPremiseGroup());
	SetOnPremMetatype(p_Row);
}

void GridTemplateGroups::SetHybrid(O365Grid& p_Grid, GridBackendRow& p_Row)
{
	GridUtil::SetRowObjectType(p_Grid, &p_Row, BusinessGroup());
	SetHybridMetatype(p_Row);
}

GridBackendColumn*& GridTemplateGroups::GetRequestDateColumn(GBI p_BlockId)
{
	return m_ColumnsRequestDateTime[p_BlockId];
}

GridBackendColumn* GridTemplateGroups::getRequestDateColumn(GBI p_BlockId) const
{
	auto it = m_ColumnsRequestDateTime.find(p_BlockId);
	if (m_ColumnsRequestDateTime.end() != it)
		return it->second;
	return nullptr;
}

void GridTemplateGroups::archiveTeam(GridBackendRow* p_Row, bool p_ShouldArchive, bool p_SetSpoReadOnlyForMembers)
{
	O365Grid* grid = nullptr != p_Row ? dynamic_cast<O365Grid*>(p_Row->GetBackend()->GetCacheGrid()) : nullptr;
	const auto& field = p_Row->GetField(m_ColumnTeamIsArchived);
	const bool isAlreadyArchived = m_IconIsArchived == field.GetIcon();
	if (isAlreadyArchived != p_ShouldArchive)
	{
		// State change
		if (field.IsModified())
		{
			// Revert
			vector<GridBackendField> rowPK;
			grid->GetRowPK(p_Row, rowPK);
			grid->GetModifications().RevertFieldUpdates(rowPK, { m_ColumnTeamIsArchived->GetID(), m_ColumnSpoSiteReadOnlyForMembers->GetID() });
		}
		else
		{
			if (p_ShouldArchive)
			{
				updateField(boost::YOpt<bool>(true), p_Row, m_ColumnTeamIsArchived, UpdateFieldOption::IS_MODIFICATION);
				updateField(p_SetSpoReadOnlyForMembers, p_Row, m_ColumnSpoSiteReadOnlyForMembers, UpdateFieldOption::IS_MODIFICATION);
			}
			else
				updateField(boost::YOpt<bool>(), p_Row, m_ColumnTeamIsArchived, UpdateFieldOption::IS_MODIFICATION);
		}
	}
	else if (p_ShouldArchive && field.IsModified())
		updateField(p_SetSpoReadOnlyForMembers, p_Row, m_ColumnSpoSiteReadOnlyForMembers, UpdateFieldOption::IS_MODIFICATION);
}

bool GridTemplateGroups::isTeamMemberSettingsColumn(GridBackendColumn* p_Column) const
{
	return p_Column == m_ColumnTeamMemberSettingsAllowCreateUpdateChannels
		|| p_Column == m_ColumnTeamMemberSettingsAllowCreatePrivateChannels
		|| p_Column == m_ColumnTeamMemberSettingsAllowDeleteChannels
		|| p_Column == m_ColumnTeamMemberSettingsAllowAddRemoveApps
		|| p_Column == m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs
		|| p_Column == m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors
		;
}

bool GridTemplateGroups::isTeamGuestSettingsColumn(GridBackendColumn* p_Column) const
{
	return p_Column == m_ColumnTeamGuestSettingsAllowCreateUpdateChannels
		|| p_Column == m_ColumnTeamGuestSettingsAllowDeleteChannels
		;
}

bool GridTemplateGroups::isTeamMessagingSettingsColumn(GridBackendColumn* p_Column) const
{
	return p_Column == m_ColumnTeamMessagingSettingsAllowUserEditMessages
		|| p_Column == m_ColumnTeamMessagingSettingsAllowUserDeleteMessages
		|| p_Column == m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages
		|| p_Column == m_ColumnTeamMessagingSettingsAllowTeamMentions
		|| p_Column == m_ColumnTeamMessagingSettingsAllowChannelMentions
		;
}

bool GridTemplateGroups::isTeamFunSettingsColumn(GridBackendColumn* p_Column) const
{
	return p_Column == m_ColumnTeamFunSettingsAllowGiphy
		|| p_Column == m_ColumnTeamFunSettingsGiphyContentRating
		|| p_Column == m_ColumnTeamFunSettingsAllowStickersAndMemes
		|| p_Column == m_ColumnTeamFunSettingsAllowCustomMemes
		;
}

bool GridTemplateGroups::hasTeamMemberSettingsColumn() const
{
	return nullptr != m_ColumnTeamMemberSettingsAllowCreateUpdateChannels
		|| nullptr != m_ColumnTeamMemberSettingsAllowCreatePrivateChannels
		|| nullptr != m_ColumnTeamMemberSettingsAllowDeleteChannels
		|| nullptr != m_ColumnTeamMemberSettingsAllowAddRemoveApps
		|| nullptr != m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs
		|| nullptr != m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors
		;
}

bool GridTemplateGroups::hasTeamGuestSettingsColumn() const
{
	return nullptr != m_ColumnTeamGuestSettingsAllowCreateUpdateChannels
		|| nullptr != m_ColumnTeamGuestSettingsAllowDeleteChannels
		;
}

bool GridTemplateGroups::hasTeamMessagingSettingsColumn() const
{
	return nullptr != m_ColumnTeamMessagingSettingsAllowUserEditMessages
		|| nullptr != m_ColumnTeamMessagingSettingsAllowUserDeleteMessages
		|| nullptr != m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages
		|| nullptr != m_ColumnTeamMessagingSettingsAllowTeamMentions
		|| nullptr != m_ColumnTeamMessagingSettingsAllowChannelMentions
		;
}

bool GridTemplateGroups::hasTeamFunSettingsColumn() const
{
	return nullptr != m_ColumnTeamFunSettingsAllowGiphy
		|| nullptr != m_ColumnTeamFunSettingsGiphyContentRating
		|| nullptr != m_ColumnTeamFunSettingsAllowStickersAndMemes
		|| nullptr != m_ColumnTeamFunSettingsAllowCustomMemes
		;
}

bool GridTemplateGroups::isTeam(const GridBackendRow* p_Row) const
{
	ASSERT(nullptr != p_Row);
	O365Grid* grid = nullptr != p_Row ? dynamic_cast<O365Grid*>(p_Row->GetBackend()->GetCacheGrid()) : nullptr;
	ASSERT(nullptr != grid);
	return nullptr != grid && grid->isTeam(p_Row);
}

GridBackendColumn*& GridTemplateGroups::getColumnForProperty(GridTemplateGroups& p_That, const wstring& p_PropertyName)
{
	using ColumnsByProperty = std::map<wstring, GridBackendColumn* GridTemplateGroups::*>;
	static const ColumnsByProperty m_RuleStringProps
	{
		{ _YUID("metaType"), &GridTemplateGroups::m_ColumnMetaType },
		{ _YUID(O365_ID), &GridTemplateGroups::m_ColumnID },
		{ _YUID(O365_GROUP_DISPLAYNAME), &GridTemplateGroups::m_ColumnDisplayName },

		{ _YUID(O365_GROUP_CLASSIFICATION), &GridTemplateGroups::m_ColumnClassification },
		{ _YUID(O365_GROUP_CREATEDDATETIME), &GridTemplateGroups::m_ColumnCreatedDateTime },
		{ _YUID(O365_GROUP_RENEWEDDATETIME), &GridTemplateGroups::m_ColumnRenewedDateTime },
		{ _YUID("expireOn"), &GridTemplateGroups::m_ColumnExpireOnDateTime },
		{ _YUID(O365_GROUP_DELETEDDATETIME), &GridTemplateGroups::m_ColumnDeletedDateTime },
		{ _YUID("expirationPolicyStatus"), &GridTemplateGroups::m_ColumnExpirationPolicyStatus },
		{ _YUID(O365_GROUP_DESCRIPTION), &GridTemplateGroups::m_ColumnDescription },
		{ _YUID(O365_GROUP_GROUPTYPES), &GridTemplateGroups::m_ColumnGroupTypes },
		{ _YUID(O365_GROUP_UNSEENCOUNT), &GridTemplateGroups::m_ColumnUnseenCount },
		{ _YUID(O365_GROUP_VISIBILITY), &GridTemplateGroups::m_ColumnVisibility },
		{ _YUID(O365_GROUP_ALLOWEXTERNALSENDERS), &GridTemplateGroups::m_ColumnAllowExternalSenders },
		{ _YUID(O365_GROUP_AUTOSUBSCRIBENEWMEMBERS), &GridTemplateGroups::m_ColumnAutoSubscribeNewMembers },
		//GridBackendColumn* m_ColumnIsSubscribedByMail },
		{ _YUID(O365_GROUP_SECURITYENABLED), &GridTemplateGroups::m_ColumnSecurityEnabled },
		{ _YUID(O365_GROUP_ONPREMISESLASTSYNCDATETIME), &GridTemplateGroups::m_ColumnOPLastSync },
		{ _YUID(O365_GROUP_ONPREMISESSECURITYIDENTIFIER), &GridTemplateGroups::m_ColumnOPSecurityID },
		{ _YUID(O365_GROUP_ONPREMISESSYNCENABLED), &GridTemplateGroups::m_ColumnOPSyncEnabled },
		{ _YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY), &GridTemplateGroups::m_ColumnOPPECategory },
		{ _YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME), &GridTemplateGroups::m_ColumnOPPEOccuredDateTime },
		{ _YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR), &GridTemplateGroups::m_ColumnOPPEProperty },
		{ _YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE), &GridTemplateGroups::m_ColumnOPPEValue },

		{ _YUID(O365_GROUP_MAIL), &GridTemplateGroups::m_ColumnMail },
		{ _YUID(O365_GROUP_MAILENABLED), &GridTemplateGroups::m_ColumnMailEnabled },
		{ _YUID(O365_GROUP_MAILNICKNAME), &GridTemplateGroups::m_ColumnMailNickname },
		{ _YUID(O365_GROUP_PROXYADDRESSES), &GridTemplateGroups::m_ColumnProxyAddresses },
		{ _YUID(O365_GROUP_GROUPTYPE), &GridTemplateGroups::m_ColumnCombinedGroupType },
		{ _YUID(O365_GROUP_DYNAMICMEMBERSHIP), &GridTemplateGroups::m_ColumnDynamicMembership },
		{ _YUID("createdOnBehalfOfUserId"), &GridTemplateGroups::m_ColumnCreatedOnBehalfOfUserId },
		{ _YUID("createdOnBehalfOfUserDisplayName"), &GridTemplateGroups::m_ColumnCreatedOnBehalfOfUserDisplayName },
		{ _YUID("createdOnBehalfOfUserPrincipalName"), &GridTemplateGroups::m_ColumnCreatedOnBehalfOfUserPrincipalName },
		{ _YUID("owneruserids"), &GridTemplateGroups::m_ColumnOwnerUserIDs },
		{ _YUID("ownerusernames"), &GridTemplateGroups::m_ColumnOwnerUserNames },
		{ _YUID(O365_GROUP_ISTEAM), &GridTemplateGroups::m_ColumnIsTeam },
		{ _YUID(YTRIA_GROUP_ISYAMMER), &GridTemplateGroups::m_ColumnIsYammer },
		{ _YUID(YTRIA_GROUP_GROUPUNIFIEDGUESTSETTINGACTIVATED), &GridTemplateGroups::m_ColumnGroupUnifiedSettingActivated },
		{ _YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTS), &GridTemplateGroups::m_ColumnSettingAllowToAddGuests },
		{ _YUID(O365_GROUP_PREFERREDDATALOCATION), &GridTemplateGroups::m_ColumnPreferredDataLocation },

	// Team columns
		{ _YUID("teamMemberSettingsAllowCreateUpdateChannels"), &GridTemplateGroups::m_ColumnTeamMemberSettingsAllowCreateUpdateChannels },
		{ _YUID("teamMemberSettingsAllowCreatePrivateChannels"), &GridTemplateGroups::m_ColumnTeamMemberSettingsAllowCreatePrivateChannels },
		{ _YUID("teamMemberSettingsAllowDeleteChannels"), &GridTemplateGroups::m_ColumnTeamMemberSettingsAllowDeleteChannels },
		{ _YUID("teamMemberSettingsAllowAddRemoveApps"), &GridTemplateGroups::m_ColumnTeamMemberSettingsAllowAddRemoveApps },
		{ _YUID("teamMemberSettingsAllowCreateUpdateRemoveTabs"), &GridTemplateGroups::m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs },
		{ _YUID("teamMemberSettingsAllowCreateUpdateRemoveConnectors"), &GridTemplateGroups::m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors },

		{ _YUID("guestSettingsAllowCreateUpdateChannels"), &GridTemplateGroups::m_ColumnTeamGuestSettingsAllowCreateUpdateChannels },
		{ _YUID("guestSettingsAllowDeleteChannels"), &GridTemplateGroups::m_ColumnTeamGuestSettingsAllowDeleteChannels },

		{ _YUID("messagingSettingsAllowUserEditMessages"), &GridTemplateGroups::m_ColumnTeamMessagingSettingsAllowUserEditMessages },
		{ _YUID("messagingSettingsAllowUserDeleteMessages"), &GridTemplateGroups::m_ColumnTeamMessagingSettingsAllowUserDeleteMessages },
		{ _YUID("messagingSettingsAllowOwnerDeleteMessages"), &GridTemplateGroups::m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages },
		{ _YUID("messagingSettingsAllowTeamMentions"), &GridTemplateGroups::m_ColumnTeamMessagingSettingsAllowTeamMentions },
		{ _YUID("messagingSettingsAllowChannelMentions"), &GridTemplateGroups::m_ColumnTeamMessagingSettingsAllowChannelMentions },

		{ _YUID("funSettingsAllowGiphy"), &GridTemplateGroups::m_ColumnTeamFunSettingsAllowGiphy },
		{ _YUID("funSettingsGiphyContentRating"), &GridTemplateGroups::m_ColumnTeamFunSettingsGiphyContentRating },
		{ _YUID("funSettingsAllowStickersAndMemes"), &GridTemplateGroups::m_ColumnTeamFunSettingsAllowStickersAndMemes },
		{ _YUID("funSettingsAllowCustomMemes"), &GridTemplateGroups::m_ColumnTeamFunSettingsAllowCustomMemes },

		{ _YUID("webUrl"), &GridTemplateGroups::m_ColumnTeamWebUrl },

		{ _YUID("isArchived"), &GridTemplateGroups::m_ColumnTeamIsArchived },
		{ _YUID("setSpoSiteReadOnlyForMembers"), &GridTemplateGroups::m_ColumnSpoSiteReadOnlyForMembers },
		{ _YUID("internalId"), &GridTemplateGroups::m_ColumnTeamInternalId },

		{ _YUID(O365_GROUP_RESOURCEBEHAVIOROPTIONS), &GridTemplateGroups::m_ColumnResourceBehaviorOptions },
		{ _YUID(O365_GROUP_RESOURCEPROVISIONINGOPTIONS), &GridTemplateGroups::m_ColumnResourceProvisioningOptions },

		{ _YUID("memberCount"), &GridTemplateGroups::m_ColumnMembersCount },

		{ _YUID(O365_GROUP_HIDEFROMOUTLOOKCLIENTS), &GridTemplateGroups::m_ColumnHideFromOutlookClients },
		{ _YUID(O365_GROUP_HIDEFROMADDRESSLISTS), &GridTemplateGroups::m_ColumnHideFromAddressLists },

		{ _YUID(O365_GROUP_LICENSEPROCESSINGSTATE), &GridTemplateGroups::m_ColumnLicenseProcessingState },
		{ _YUID(O365_GROUP_SECURITYIDENTIFIER), &GridTemplateGroups::m_ColumnSecurityIdentifier},
		{ _YUID("assignedLicenses.skuId"), &GridTemplateGroups::m_ColumnSkuIds },
		{ _YUID(O365_GROUP_ASSIGNEDLICENSES), &GridTemplateGroups::m_ColumnSkuPartNumbers },
		{ _YUID("ASSIGNEDLICENSENAMES"), &GridTemplateGroups::m_ColumnSkuNames },

	/* Beta API */
		{ _YUID(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE), &GridTemplateGroups::m_ColumnMembershipRuleProcessingState },
		{ _YUID(O365_GROUP_MEMBERSHIPRULE), &GridTemplateGroups::m_ColumnMembershipRule },
		{ _YUID(O365_GROUP_THEME), &GridTemplateGroups::m_ColumnTheme },
		{ _YUID(O365_GROUP_PREFERREDLANGUAGE), &GridTemplateGroups::m_ColumnPreferredLanguage },

		{ BasicGridSetup::g_LastQueryColumnUID, &GridTemplateGroups::m_ColumnLastRequestDateTime },

	// Drive columns
		{ _YUID(O365_COLDRIVENAME), &GridTemplateGroups::m_ColumnDriveName },
		{ _YUID(O365_COLDRIVEID), &GridTemplateGroups::m_ColumnDriveId },
		{ _YUID(O365_COLDRIVEDESCRIPTION), &GridTemplateGroups::m_ColumnDriveDescription },
		{ _YUID(O365_COLDRIVEQUOTASTATE), &GridTemplateGroups::m_ColumnDriveQuotaState },
		{ _YUID(O365_COLDRIVEQUOTAUSED), &GridTemplateGroups::m_ColumnDriveQuotaUsed },
		{ _YUID(O365_COLDRIVEQUOTAREMAINING), &GridTemplateGroups::m_ColumnDriveQuotaRemaining },
		{ _YUID(O365_COLDRIVEQUOTADELETED), &GridTemplateGroups::m_ColumnDriveQuotaDeleted },
		{ _YUID(O365_COLDRIVEQUOTATOTAL), &GridTemplateGroups::m_ColumnDriveQuotaTotal },
		{ _YUID(O365_COLDRIVECREATIONTIME), &GridTemplateGroups::m_ColumnDriveCreationTime },
		{ _YUID(O365_COLDRIVELASTMODIFIEDDATETIME), &GridTemplateGroups::m_ColumnDriveLastModifiedTime },
		{ _YUID(O365_COLDRIVETYPE), &GridTemplateGroups::m_ColumnDriveType },
		{ _YUID(O365_COLDRIVEWEBURL), &GridTemplateGroups::m_ColumnDriveWebUrl },
		{ _YUID(O365_COLDRIVECREATEDBYUSEREMAIL), &GridTemplateGroups::m_ColumnDriveCreatedByUserEmail },
		{ _YUID(O365_COLDRIVECREATEDBYUSERID), &GridTemplateGroups::m_ColumnDriveCreatedByUserId },
		{ _YUID(O365_COLDRIVECREATEDBYAPPNAME), &GridTemplateGroups::m_ColumnDriveCreatedByAppName },
		{ _YUID(O365_COLDRIVECREATEDBYAPPID), &GridTemplateGroups::m_ColumnDriveCreatedByAppId },
		{ _YUID(O365_COLDRIVECREATEDBYDEVICENAME), &GridTemplateGroups::m_ColumnDriveCreatedByDeviceName },
		{ _YUID(O365_COLDRIVECREATEDBYDEVICEID), &GridTemplateGroups::m_ColumnDriveCreatedByDeviceId },
		{ _YUID(O365_COLDRIVEOWNERUSERNAME), &GridTemplateGroups::m_ColumnDriveOwnerUserName },
		{ _YUID(O365_COLDRIVEOWNERUSERID), &GridTemplateGroups::m_ColumnDriveOwnerUserId },
		{ _YUID(O365_COLDRIVEOWNERUSEREMAIL), &GridTemplateGroups::m_ColumnDriveOwnerUserEmail },
		{ _YUID(O365_COLDRIVEOWNERAPPNAME), &GridTemplateGroups::m_ColumnDriveOwnerAppName },
		{ _YUID(O365_COLDRIVEOWNERAPPID), &GridTemplateGroups::m_ColumnDriveOwnerAppId },
		{ _YUID(O365_COLDRIVEOWNERDEVICENAME), &GridTemplateGroups::m_ColumnDriveOwnerDeviceName },
		{ _YUID(O365_COLDRIVEOWNERDEVICEID), &GridTemplateGroups::m_ColumnDriveOwnerDeviceId },
	};

	const auto it = m_RuleStringProps.find(p_PropertyName);
	if (m_RuleStringProps.end() != it)
		return p_That.*(it->second);

	ASSERT(false); // Either you forgot to add your new column in above map, or you're requesting an inexistent one!
	static GridBackendColumn* fakePtr = nullptr;
	return fakePtr;
}

void GridTemplateGroups::updateRowTeam(const BusinessGroup& p_BG, GridBackendRow* p_Row, bool p_StoreModification)
{
	//ASSERT(!p_BG.HasFlag(BusinessObject::CREATED));
	const UpdateFieldOption updateFieldOption = p_BG.HasFlag(BusinessObject::CREATED) ? UpdateFieldOption::IS_OBJECT_CREATION : (p_StoreModification ? UpdateFieldOption::IS_MODIFICATION : UpdateFieldOption::NONE);

	SetRowIsTeam(p_BG, p_Row, m_ColumnIsTeam, updateFieldOption);

	if (p_BG.GetTeamError())
	{
		ASSERT(UpdateFieldOption::NONE == updateFieldOption);

		// Set error to all team fields

		vector<GridBackendField*> errorTeamFields;

		auto pushBackIfNotNull = [&errorTeamFields](auto* fieldPtr)
		{
			if (nullptr != fieldPtr)
				errorTeamFields.push_back(fieldPtr);
		};

		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamMemberSettingsAllowCreateUpdateChannels, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamMemberSettingsAllowCreatePrivateChannels, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamMemberSettingsAllowDeleteChannels, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamMemberSettingsAllowAddRemoveApps, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors, updateFieldOption));

		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamGuestSettingsAllowCreateUpdateChannels, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamGuestSettingsAllowDeleteChannels, updateFieldOption));

		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamMessagingSettingsAllowUserEditMessages, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamMessagingSettingsAllowUserDeleteMessages, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamMessagingSettingsAllowTeamMentions, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamMessagingSettingsAllowChannelMentions, updateFieldOption));

		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamFunSettingsAllowGiphy, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamFunSettingsGiphyContentRating, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamFunSettingsAllowStickersAndMemes, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamFunSettingsAllowCustomMemes, updateFieldOption));

		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamWebUrl, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamIsArchived, updateFieldOption));
		pushBackIfNotNull(updateField(p_BG.GetTeamError()->GetFullErrorMessage(), p_Row, m_ColumnTeamInternalId, updateFieldOption));

		for (auto& field : errorTeamFields)
		{
			ASSERT(nullptr != field && !field->IsGeneric());
			if (nullptr != field && !field->IsGeneric())
				field->SetIcon(GridBackendUtil::ICON_ERROR);
		}

		updateField(boost::YOpt<bool>(), p_Row, m_ColumnSpoSiteReadOnlyForMembers, updateFieldOption);
	}
	else
	{
		if (p_BG.GetHasTeamInfo()) // We got some team information, update team fields
		{
			if (/*p_BG.GetIsTeam() && */boost::none != p_BG.GetTeam())
			{
				const auto& team = p_BG.GetTeam();
				ASSERT(boost::none != team);

				const auto& memberSettings = team->MemberSettings;
				if (boost::none != memberSettings)
				{
					updateField(memberSettings->GetAllowCreateUpdateChannels(), p_Row, m_ColumnTeamMemberSettingsAllowCreateUpdateChannels, updateFieldOption);
					updateField(memberSettings->GetAllowCreatePrivateChannels(), p_Row, m_ColumnTeamMemberSettingsAllowCreatePrivateChannels, updateFieldOption);
					updateField(memberSettings->GetAllowDeleteChannels(), p_Row, m_ColumnTeamMemberSettingsAllowDeleteChannels, updateFieldOption);
					updateField(memberSettings->GetAllowAddRemoveApps(), p_Row, m_ColumnTeamMemberSettingsAllowAddRemoveApps, updateFieldOption);
					updateField(memberSettings->GetAllowCreateUpdateRemoveTabs(), p_Row, m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs, updateFieldOption);
					updateField(memberSettings->GetAllowCreateUpdateRemoveConnectors(), p_Row, m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors, updateFieldOption);
				}
				else
				{
					for (auto c : { m_ColumnTeamMemberSettingsAllowCreateUpdateChannels
								, m_ColumnTeamMemberSettingsAllowCreatePrivateChannels
								, m_ColumnTeamMemberSettingsAllowDeleteChannels
								, m_ColumnTeamMemberSettingsAllowAddRemoveApps
								, m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs
								, m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors
						})
					{
						if (nullptr != c)
							p_Row->RemoveField(c);
					}
				}

				const auto& guestSettings = team->GuestSettings;
				if (boost::none != guestSettings)
				{
					updateField(guestSettings->GetAllowCreateUpdateChannels(), p_Row, m_ColumnTeamGuestSettingsAllowCreateUpdateChannels, updateFieldOption);
					updateField(guestSettings->GetAllowDeleteChannels(), p_Row, m_ColumnTeamGuestSettingsAllowDeleteChannels, updateFieldOption);
				}
				else
				{
					for (auto c : { m_ColumnTeamGuestSettingsAllowCreateUpdateChannels
								, m_ColumnTeamGuestSettingsAllowDeleteChannels
						})
					{
						if (nullptr != c)
							p_Row->RemoveField(c);
					}
				}

				const auto& messagingSettings = team->MessagingSettings;
				if (boost::none != messagingSettings)
				{
					updateField(messagingSettings->GetAllowUserEditMessages(), p_Row, m_ColumnTeamMessagingSettingsAllowUserEditMessages, updateFieldOption);
					updateField(messagingSettings->GetAllowUserDeleteMessages(), p_Row, m_ColumnTeamMessagingSettingsAllowUserDeleteMessages, updateFieldOption);
					updateField(messagingSettings->GetAllowOwnerDeleteMessages(), p_Row, m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages, updateFieldOption);
					updateField(messagingSettings->GetAllowTeamMentions(), p_Row, m_ColumnTeamMessagingSettingsAllowTeamMentions, updateFieldOption);
					updateField(messagingSettings->GetAllowChannelMentions(), p_Row, m_ColumnTeamMessagingSettingsAllowChannelMentions, updateFieldOption);
				}
				else
				{
					for (auto c : { m_ColumnTeamMessagingSettingsAllowUserEditMessages
								, m_ColumnTeamMessagingSettingsAllowUserDeleteMessages
								, m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages
								, m_ColumnTeamMessagingSettingsAllowTeamMentions
								, m_ColumnTeamMessagingSettingsAllowChannelMentions
						})
					{
						if (nullptr != c)
							p_Row->RemoveField(c);
					}
				}

				const auto& funSettings = team->FunSettings;
				if (boost::none != funSettings)
				{
					updateField(funSettings->GetAllowGiphy(), p_Row, m_ColumnTeamFunSettingsAllowGiphy, updateFieldOption);
					updateField(funSettings->GetGiphyContentRating(), p_Row, m_ColumnTeamFunSettingsGiphyContentRating, updateFieldOption);
					updateField(funSettings->GetAllowStickersAndMemes(), p_Row, m_ColumnTeamFunSettingsAllowStickersAndMemes, updateFieldOption);
					updateField(funSettings->GetAllowCustomMemes(), p_Row, m_ColumnTeamFunSettingsAllowCustomMemes, updateFieldOption);
				}
				else
				{
					for (auto c : {  m_ColumnTeamFunSettingsAllowGiphy
								, m_ColumnTeamFunSettingsGiphyContentRating
								, m_ColumnTeamFunSettingsAllowStickersAndMemes
								, m_ColumnTeamFunSettingsAllowCustomMemes
						})
					{
						if (nullptr != c)
							p_Row->RemoveField(c);
					}
				}

				if (updateFieldOption != UpdateFieldOption::IS_OBJECT_CREATION)
				{
					updateField(team->WebUrl, p_Row, m_ColumnTeamWebUrl, updateFieldOption);
					updateField(team->InternalId, p_Row, m_ColumnTeamInternalId, updateFieldOption);

					if (p_BG.GetPendingTeamArchiving())
						archiveTeam(p_Row, *p_BG.GetPendingTeamArchiving(), p_BG.GetSpoSiteReadOnlyForMembers() ? *p_BG.GetSpoSiteReadOnlyForMembers() : false);
					else if (p_BG.GetSpoSiteReadOnlyForMembers())
					{
						ASSERT(!*p_BG.GetSpoSiteReadOnlyForMembers());
						archiveTeam(p_Row, false, false);
					}
					else
					{
						updateField(team->IsArchived, p_Row, m_ColumnTeamIsArchived, UpdateFieldOption::NONE);
						if (nullptr != m_ColumnSpoSiteReadOnlyForMembers)
							p_Row->RemoveField(m_ColumnSpoSiteReadOnlyForMembers);
					}
				}
				else
				{
					for (auto c : { m_ColumnTeamWebUrl
								, m_ColumnTeamInternalId
								, m_ColumnTeamIsArchived
								, m_ColumnSpoSiteReadOnlyForMembers })
					{
						if (nullptr != c)
							p_Row->RemoveField(c);
					}
				}
			}
			else
			{
				for (auto c : { m_ColumnTeamMemberSettingsAllowCreateUpdateChannels
							, m_ColumnTeamMemberSettingsAllowCreatePrivateChannels
							, m_ColumnTeamMemberSettingsAllowDeleteChannels
							, m_ColumnTeamMemberSettingsAllowAddRemoveApps
							, m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveTabs
							, m_ColumnTeamMemberSettingsAllowCreateUpdateRemoveConnectors
							, m_ColumnTeamGuestSettingsAllowCreateUpdateChannels
							, m_ColumnTeamGuestSettingsAllowDeleteChannels
							, m_ColumnTeamMessagingSettingsAllowUserEditMessages
							, m_ColumnTeamMessagingSettingsAllowUserDeleteMessages
							, m_ColumnTeamMessagingSettingsAllowOwnerDeleteMessages
							, m_ColumnTeamMessagingSettingsAllowTeamMentions
							, m_ColumnTeamMessagingSettingsAllowChannelMentions
							, m_ColumnTeamFunSettingsAllowGiphy
							, m_ColumnTeamFunSettingsGiphyContentRating
							, m_ColumnTeamFunSettingsAllowStickersAndMemes
							, m_ColumnTeamFunSettingsAllowCustomMemes
							, m_ColumnTeamWebUrl
							, m_ColumnTeamInternalId
							, m_ColumnTeamIsArchived
							, m_ColumnSpoSiteReadOnlyForMembers
							})
				{
					if (nullptr != c)
						p_Row->RemoveField(c);
				}
			}
		}
		else
		{
			// We haven't got any team information, do not update team fields (it probably means we didn't request this information)					
		}
	}
}

void GridTemplateGroups::updateRowGroupSettings(const BusinessGroup& p_BG, GridBackendRow* p_Row, bool p_StoreModification)
{
	ASSERT(!p_BG.HasFlag(BusinessObject::CREATED));
	const UpdateFieldOption updateFieldOption = p_BG.HasFlag(BusinessObject::CREATED) ? UpdateFieldOption::IS_OBJECT_CREATION : (p_StoreModification ? UpdateFieldOption::IS_MODIFICATION : UpdateFieldOption::NONE);
	if (p_BG.GetGroupSettingsError())
	{
		vector<GridBackendField*> errorFields;
		errorFields.push_back(updateField(p_BG.GetGroupSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnSettingAllowToAddGuests, updateFieldOption));
		errorFields.push_back(updateField(p_BG.GetGroupSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnGroupUnifiedSettingActivated, updateFieldOption));

		for (auto& field : errorFields)
		{
			ASSERT(nullptr != field && !field->IsGeneric());
			if (nullptr != field && !field->IsGeneric())
				field->SetIcon(GridBackendUtil::ICON_ERROR);
		}
	}
	else
	{
		updateField(p_BG.GetSettingAllowToAddGuests(), p_Row, m_ColumnSettingAllowToAddGuests, updateFieldOption);
		updateField(p_BG.GetGroupUnifiedGuestSettingTemplateActivated(), p_Row, m_ColumnGroupUnifiedSettingActivated, updateFieldOption);
	}
}

void GridTemplateGroups::updateRowOwners(const BusinessGroup& p_BG, GridBackendRow* p_Row, const GraphCache& p_GraphCache, UpdateFieldOption p_UpdateFieldOption)
{
	vector<GridBackendField*> ownerErrorFields;
	if (nullptr != m_ColumnOwnerUserNames)
	{
		if (p_BG.GetOwnersError())
		{
			ownerErrorFields.push_back(updateField(p_BG.GetOwnersError()->GetFullErrorMessage(), p_Row, m_ColumnOwnerUserNames, p_UpdateFieldOption));
		}
		else
		{
			vector<PooledString> ownerNames;
			for (const auto& ownerId : p_BG.GetOwnerUsers())
			{
				const auto owner = p_GraphCache.GetUserInCache(ownerId, false);
				ASSERT(owner.GetID() == ownerId && !owner.GetUserPrincipalName().IsEmpty());
				if (owner.GetID() == ownerId)
					ownerNames.push_back(owner.GetUserPrincipalName());
			}
			updateField(ownerNames, p_Row, m_ColumnOwnerUserNames, p_UpdateFieldOption);
		}
	}

	if (nullptr != m_ColumnOwnerUserIDs)
	{
		if (p_BG.GetOwnersError())
		{
			ownerErrorFields.push_back(updateField(p_BG.GetOwnersError()->GetFullErrorMessage(), p_Row, m_ColumnOwnerUserIDs, p_UpdateFieldOption));
		}
		else
		{
			vector<PooledString> ownerIDs;
			for (const auto& ownerId : p_BG.GetOwnerUsers())
				ownerIDs.push_back(ownerId);

			updateField(ownerIDs, p_Row, m_ColumnOwnerUserIDs, p_UpdateFieldOption);
		}
	}

	for (auto& field : ownerErrorFields)
	{
		ASSERT(nullptr != field && !field->IsGeneric());
		if (nullptr != field && !field->IsGeneric())
			field->SetIcon(GridBackendUtil::ICON_ERROR);
	}
}

void GridTemplateGroups::updateRowDrive(const BusinessGroup& p_BG, GridBackendRow* p_Row, UpdateFieldOption p_UpdateFieldOption)
{
	if (p_BG.GetDrive())
	{
		if (p_BG.GetDriveError())
		{
			auto finalizeField = [](GridBackendField* p_Field) {
				if (nullptr != p_Field && !p_Field->IsGeneric())
					p_Field->SetIcon(GridBackendUtil::ICON_ERROR);
			};

			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveName, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveId, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveDescription, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreationTime, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveLastModifiedTime, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveType, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveWebUrl, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveQuotaState, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveQuotaUsed, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveQuotaRemaining, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveQuotaDeleted, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveQuotaTotal, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByUserEmail, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByUserId, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByAppName, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByAppId, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByDeviceName, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByDeviceId, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerUserName, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerUserId, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerAppName, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerAppId, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerDeviceName, p_UpdateFieldOption));
			finalizeField(updateField(p_BG.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerDeviceId, p_UpdateFieldOption));
		}
		else
		{
			updateField(p_BG.GetDrive()->GetName(), p_Row, m_ColumnDriveName, p_UpdateFieldOption);
			updateField(p_BG.GetDrive()->GetID(), p_Row, m_ColumnDriveId, p_UpdateFieldOption);
			updateField(p_BG.GetDrive()->GetDescription(), p_Row, m_ColumnDriveDescription, p_UpdateFieldOption);
			updateField(p_BG.GetDrive()->GetCreatedDateTime(), p_Row, m_ColumnDriveCreationTime, p_UpdateFieldOption);
			updateField(p_BG.GetDrive()->GetLastModifiedDateTime(), p_Row, m_ColumnDriveLastModifiedTime, p_UpdateFieldOption);
			updateField(p_BG.GetDrive()->GetDriveType(), p_Row, m_ColumnDriveType, p_UpdateFieldOption);
			updateField(p_BG.GetDrive()->GetWebUrl(), p_Row, m_ColumnDriveWebUrl, p_UpdateFieldOption);

			if (p_BG.GetDrive()->GetQuota())
			{
				updateField(p_BG.GetDrive()->GetQuota()->State, p_Row, m_ColumnDriveQuotaState, p_UpdateFieldOption);
				updateField(p_BG.GetDrive()->GetQuota()->Used, p_Row, m_ColumnDriveQuotaUsed, p_UpdateFieldOption);
				updateField(p_BG.GetDrive()->GetQuota()->Remaining, p_Row, m_ColumnDriveQuotaRemaining, p_UpdateFieldOption);
				updateField(p_BG.GetDrive()->GetQuota()->Deleted, p_Row, m_ColumnDriveQuotaDeleted, p_UpdateFieldOption);
				updateField(p_BG.GetDrive()->GetQuota()->Total, p_Row, m_ColumnDriveQuotaTotal, p_UpdateFieldOption);
			}
			if (p_BG.GetDrive()->GetCreatedBy())
			{
				if (p_BG.GetDrive()->GetCreatedBy()->User)
				{
					updateField(p_BG.GetDrive()->GetCreatedBy()->User->DisplayName, p_Row, m_ColumnDriveCreatedByUserEmail, p_UpdateFieldOption);
					updateField(p_BG.GetDrive()->GetCreatedBy()->User->Id, p_Row, m_ColumnDriveCreatedByUserId, p_UpdateFieldOption);
				}
				if (p_BG.GetDrive()->GetCreatedBy()->Application)
				{
					updateField(p_BG.GetDrive()->GetCreatedBy()->Application->DisplayName, p_Row, m_ColumnDriveCreatedByAppName, p_UpdateFieldOption);
					updateField(p_BG.GetDrive()->GetCreatedBy()->Application->Id, p_Row, m_ColumnDriveCreatedByAppId, p_UpdateFieldOption);
				}
				if (p_BG.GetDrive()->GetCreatedBy()->Device)
				{
					updateField(p_BG.GetDrive()->GetCreatedBy()->Device->DisplayName, p_Row, m_ColumnDriveCreatedByDeviceName, p_UpdateFieldOption);
					updateField(p_BG.GetDrive()->GetCreatedBy()->Device->Id, p_Row, m_ColumnDriveCreatedByDeviceId, p_UpdateFieldOption);
				}
			}
			if (p_BG.GetDrive()->GetOwner())
			{
				if (p_BG.GetDrive()->GetOwner()->User)
				{
					updateField(p_BG.GetDrive()->GetOwner()->User->DisplayName, p_Row, m_ColumnDriveOwnerUserName, p_UpdateFieldOption);
					updateField(p_BG.GetDrive()->GetOwner()->User->Id, p_Row, m_ColumnDriveOwnerUserId, p_UpdateFieldOption);
				}
				if (p_BG.GetDrive()->GetOwner()->Application)
				{
					updateField(p_BG.GetDrive()->GetOwner()->Application->DisplayName, p_Row, m_ColumnDriveOwnerAppName, p_UpdateFieldOption);
					updateField(p_BG.GetDrive()->GetOwner()->Application->Id, p_Row, m_ColumnDriveOwnerAppId, p_UpdateFieldOption);
				}
				if (p_BG.GetDrive()->GetOwner()->Device)
				{
					updateField(p_BG.GetDrive()->GetOwner()->Device->DisplayName, p_Row, m_ColumnDriveOwnerDeviceName, p_UpdateFieldOption);
					updateField(p_BG.GetDrive()->GetOwner()->Device->Id, p_Row, m_ColumnDriveOwnerDeviceId, p_UpdateFieldOption);
				}
			}
		}
	}
}

void GridTemplateGroups::updateRowIsYammer(const BusinessGroup& p_BG, GridBackendRow* p_RowToUpdate)
{
	if (nullptr != m_ColumnIsYammer)
	{
		if (p_BG.GetIsYammerError())
		{
			auto finalizeField = [](GridBackendField* p_Field) {
				if (nullptr != p_Field && !p_Field->IsGeneric())
					p_Field->SetIcon(GridBackendUtil::ICON_ERROR);
			};

			finalizeField(updateField(p_BG.GetIsYammerError()->GetFullErrorMessage(), p_RowToUpdate, m_ColumnIsYammer, UpdateFieldOption::NONE));
		}
		else
		{
			updateField(p_BG.GetIsYammer(), p_RowToUpdate, m_ColumnIsYammer, UpdateFieldOption::NONE);
		}
	}
}

bool GridTemplateGroups::GetBusinessGroupPK(O365Grid& p_Grid, const BusinessGroup& p_BG, vector<GridBackendField>& BGpk, const vector<GridBackendField>& p_ExtraRowPKValue) const
{
	BGpk.clear();

	vector<GridBackendColumn*> pkColumns;
	p_Grid.GetPKColumns(pkColumns, false);
	ASSERT(!pkColumns.empty());

	for (auto c : pkColumns)
	{
		ASSERT(nullptr != c);
		if (nullptr != c)
		{
			const wstring& UID = c->GetUniqueID();
			if (nullptr != m_ColumnID && UID == m_ColumnID->GetUniqueID() || UID == _YUID(O365_ID))
				BGpk.emplace_back(p_BG.GetID(), c->GetID());
			// add other pk fields here (those that are set in columns added as grid row pk via AddColumnForRowPK) - this is grid-dependent, cannot use business object metadata
			else
			{
				bool found = false;
				for (auto& field : p_ExtraRowPKValue)
				{
					if (field.GetColID() == c->GetID())
					{
						BGpk.emplace_back(field);
						found = true;
						break;
					}
				}
				// Top level rows don't have meta ID
				ASSERT(found || UID == GridSetupMetaColumn::g_ColUIDMetaId);
			}
		}
	}
	ASSERT(!BGpk.empty());
	return !BGpk.empty();
}

template<typename T>
GridBackendField* GridTemplateGroups::updateField(const T& p_Value, GridBackendRow* p_Row, GridBackendColumn* p_Col, UpdateFieldOption p_UpdateFieldOption)
{
	GridBackendField* newField{ nullptr };
	if (nullptr != p_Col)
	{
		ASSERT(nullptr != p_Row);
		O365Grid* grid = nullptr != p_Row ? dynamic_cast<O365Grid*>(p_Row->GetBackend()->GetCacheGrid()) : nullptr;
		ASSERT(nullptr != grid);
		if (nullptr != grid)
		{
			const auto colID = p_Col->GetID();
			const bool hadField = p_Row->HasField(colID);
			const GridBackendField oldField = UpdateFieldOption::NONE != p_UpdateFieldOption ? p_Row->GetField(colID) : GridBackendField();

			// Do not update a field which had an error
			if (UpdateFieldOption::NONE != p_UpdateFieldOption && oldField.GetIcon() == GridBackendUtil::ICON_ERROR)
			{
				newField = &p_Row->GetField(colID);
			}
			else
			{
				vector<GridBackendField> rowPK;
				if (UpdateFieldOption::NONE != p_UpdateFieldOption)
					grid->GetRowPK(p_Row, rowPK);

				if (p_Col == m_ColumnMailNickname)
				{
					/*// Graph returns a strange mailnickname ("00000000-0000-0000-0000-000000000000") for some groups.
					// These groups doesn't have email and are apparently Security Groups
					rttr::variant variantValue = p_Value;
					if (variantValue.is_type<boost::YOpt<PooledString>>())
					{
						auto val = variantValue.get_value<boost::YOpt<PooledString>>();
						if (val.is_initialized() && !MFCUtil::StringMatchW(val.get(), _YTEXT("00000000-0000-0000-0000-000000000000")))
							newField = &p_Row->AddField(val, m_ColumnMailNickname);
						else
							p_Row->RemoveField(m_ColumnMailNickname);
					}
					else  // In case it's an error*/
					{
						newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col);
					}
				}
				else if (p_Col == m_ColumnTeamWebUrl || p_Col == m_ColumnDriveWebUrl)
				{
					rttr::variant variantValue = p_Value;
					if (variantValue.is_type<boost::YOpt<PooledString>>())
					{
						auto val = variantValue.get_value<boost::YOpt<PooledString>>();
						newField = &p_Row->AddFieldForHyperlink(val, p_Col);
					}
					else if (variantValue.is_type<PooledString>())
					{
						auto val = variantValue.get_value<PooledString>();
						newField = &p_Row->AddFieldForHyperlink(val, p_Col);
					}
					else
					{
						newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col); // In case it's an error
					}
				}
				else if (p_Col == m_ColumnTeamIsArchived)
				{
					rttr::variant variantVal = p_Value;
					if (rttr::type::get<boost::YOpt<bool>>() == variantVal.get_type())
					{
						auto val = variantVal.get_value<boost::YOpt<bool>>();
						if (val && *val)
						{
							newField = &GridUtil::AddFieldImpl(p_Row, BusinessGroup::g_Archived, p_Col);
							if (!newField->IsGeneric())
								newField->SetIcon(m_IconIsArchived);
						}
						else
						{
							if (p_UpdateFieldOption != UpdateFieldOption::NONE)
							{
								newField = &GridUtil::AddFieldImpl(p_Row, Str::g_EmptyString, p_Col);
								newField->SetIcon(NO_ICON);
							}
							else
								p_Row->RemoveField(p_Col);
						}
					}
					else  // In case it's an error
					{
						newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col);
					}
				}
				else if (p_Col == m_ColumnIsYammer)
				{
					rttr::variant variantVal = p_Value;
					if (rttr::type::get<boost::YOpt<bool>>() == variantVal.get_type())
					{
						auto val = variantVal.get_value<boost::YOpt<bool>>();
						if (val != boost::none && *val)
						{
							newField = &GridUtil::AddFieldImpl(p_Row, BusinessGroup::g_Yammer, p_Col);
							if (!newField->IsGeneric())
								newField->SetIcon(m_IconIsYammer);
						}
						else
							p_Row->RemoveField(p_Col);
					}
					else
						newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col);
				}
				else
				{
					newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col);
				}

				if (UpdateFieldOption::NONE != p_UpdateFieldOption)
				{
					ASSERT(p_Row->HasField(m_ColumnID));
					ASSERT(nullptr != newField);
					if (nullptr != newField
						&& (UpdateFieldOption::IS_OBJECT_CREATION == p_UpdateFieldOption || newField->IsModified())
						&& (hadField || !hadField && !newField->IsGeneric()))
					{
						if (UpdateFieldOption::IS_MODIFICATION == p_UpdateFieldOption)
							grid->GetModifications().Add(std::make_unique<FieldUpdateO365>(*grid, rowPK, p_Col->GetID(), hadField ? oldField : GridBackendField::g_GenericConstField, *newField, p_Col == m_ColumnSpoSiteReadOnlyForMembers ? FieldUpdateFlags::IgnoreRemoteHasOldValueError : FieldUpdateFlags::None));

						// Show column containing modification if hidden (only if not technical)
						{
							auto column = grid->GetColumnByID(newField->GetColID());
							ASSERT(nullptr != column);
							if (nullptr != column && !column->IsVisible() && !column->HasPresetFlag(CacheGrid::g_ColumnsPresetTech))
								grid->GetBackend().SetColumnVisible(column, true, false);
						}
					}
				}
			}
		}
	}

	return newField;
}

template<typename T>
GridBackendField* GridTemplateGroups::updateFieldOnPrem(const T& p_Value, GridBackendRow* p_Row, GridBackendColumn* p_Col, UpdateFieldOption p_UpdateFieldOption)
{
	ASSERT(nullptr == p_Col || p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetOnPrem));

	GridBackendField* newField{ nullptr };
	if (nullptr != p_Col)
	{
		ASSERT(nullptr != p_Row);
		GridGroups* grid = nullptr != p_Row ? dynamic_cast<GridGroups*>(p_Row->GetBackend()->GetCacheGrid()) : nullptr;
		ASSERT(nullptr != grid);
		if (nullptr != grid)
		{
			const auto colID = p_Col->GetID();
			const bool hadField = p_Row->HasField(colID);
			const GridBackendField oldField = UpdateFieldOption::NONE != p_UpdateFieldOption ? p_Row->GetField(colID) : GridBackendField();

			// Do not update a field which had an error
			if (UpdateFieldOption::NONE != p_UpdateFieldOption && oldField.GetIcon() == GridBackendUtil::ICON_ERROR)
			{
				newField = &p_Row->GetField(colID);
			}
			else
			{
				vector<GridBackendField> rowPK;
				if (UpdateFieldOption::NONE != p_UpdateFieldOption)
					grid->GetRowPK(p_Row, rowPK);

				if (p_Col->IsDate())
				{
					rttr::variant variantVal = p_Value;
					if (rttr::type::get<boost::YOpt<YTimeDate>>() == variantVal.get_type())
					{
						auto val = variantVal.get_value<boost::YOpt<YTimeDate>>();
						newField = &AddDateOrNeverField(val, *p_Row, *p_Col);
					}
					else  // In case it's an error
					{
						newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col);
					}
				}
				else
				{
					newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col);
				}

				if (UpdateFieldOption::NONE != p_UpdateFieldOption)
				{
					ASSERT(p_Row->HasField(m_ColumnID));
					ASSERT(nullptr != newField);
					if (nullptr != newField
						&& (UpdateFieldOption::IS_OBJECT_CREATION == p_UpdateFieldOption || newField->IsModified())
						&& (hadField || !hadField && !newField->IsGeneric()))
					{
						if (UpdateFieldOption::IS_MODIFICATION == p_UpdateFieldOption)
							grid->GetOnPremModifications().Add(std::make_unique<FieldUpdateOnPrem>(*grid, rowPK, p_Col->GetID(), hadField ? oldField : GridBackendField::g_GenericConstField, *newField, FieldUpdateFlags::IgnoreRemoteValue));

						// Show column containing modification if hidden (only if not technical)
						{
							auto column = grid->GetColumnByID(newField->GetColID());
							ASSERT(nullptr != column);
							if (nullptr != column && !column->IsVisible() && !column->HasPresetFlag(CacheGrid::g_ColumnsPresetTech))
								grid->GetBackend().SetColumnVisible(column, true, false);
						}
					}
				}
			}
		}
	}

	return newField;
}

void GridTemplateGroups::setLastRequestDateTime(O365Grid& p_Grid, const BusinessGroup& p_BG, GridBackendRow* p_Row)
{
	if (nullptr != p_Row && /*nullptr != m_ColumnLastRequestDateTime*/!m_ColumnsRequestDateTime.empty())
	{
		if (!p_BG.HasFlag(BusinessObject::CANCELED))
		{
			for (const auto& item : m_ColumnsRequestDateTime)
			{
				if (nullptr != item.second)
				{
					const auto& date = p_BG.GetDataDate(item.first);
					if (date)
					{
						if (item.second == m_ColumnLastRequestDateTime)
							p_Row->AddFieldTimeDate(date, item.second);
						//else if (UBI::MAILBOX == item.first)
						//	p_Row->AddFieldTimeDate(date, item.second).SetIcon(m_MailboxLoadedIconId);
						else
							p_Row->AddFieldTimeDate(date, item.second).SetIcon(p_Grid.GetLoadMoreIcon(true)); // FIXME: We might want different icons for each column (only SYNCV1 for now)
					}
				}
			}
			//updateField(p_BU.GetLastRequestTime(), p_Row, m_ColumnLastRequestDateTime, UpdateFieldOption::NONE);
		}
	}
}

void GridTemplateGroups::UpdateCommonInfo(GridBackendRow* p_Row)
{
	if (nullptr != m_OnPremCols.m_ColCommonDisplayName)
	{
		if (p_Row->HasField(m_ColumnDisplayName))
			p_Row->AddField(p_Row->GetField(m_ColumnDisplayName), m_OnPremCols.m_ColCommonDisplayName->GetID());
		else if (p_Row->HasField(m_OnPremCols.m_ColCN))
			p_Row->AddField(p_Row->GetField(m_OnPremCols.m_ColCN), m_OnPremCols.m_ColCommonDisplayName->GetID());
	}
}
