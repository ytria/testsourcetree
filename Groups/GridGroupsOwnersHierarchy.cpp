#include "GridGroupsOwnersHierarchy.h"

#include "AutomationWizardGroupsOwners.h"
#include "BasicGridSetup.h"
#include "BusinessGroup.h"
#include "BusinessGroupConfiguration.h"
#include "FrameGroupOwnersHierarchy.h"
#include "../Resource.h"
#include "GridUpdater.h"
#include "BusinessUserGuest.h"

BEGIN_MESSAGE_MAP(GridGroupsOwnersHierarchy, GridGroupsCommon)
	ON_COMMAND(ID_OWNERSSGRID_ADD,				OnAddItemsToGroups)
	ON_UPDATE_COMMAND_UI(ID_OWNERSSGRID_ADD,	OnUpdateAddItemsToGroups)
	ON_COMMAND(ID_OWNERSSGRID_DELETE,			OnRemoveItemsFromGroups)
	ON_UPDATE_COMMAND_UI(ID_OWNERSSGRID_DELETE, OnUpdateRemoveItemsFromGroups)
END_MESSAGE_MAP()

GridGroupsOwnersHierarchy::GridGroupsOwnersHierarchy()
	: GridGroupsCommon()
	, m_Frame(nullptr)
{
	m_MayContainUnscopedUserGroupOrSite = true;

	initWizard<AutomationWizardGroupsOwners>(_YTEXT("Automation\\GroupsOwners"));
}

void GridGroupsOwnersHierarchy::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameGroupsOwnersHierarchy, LicenseUtil::g_codeSapio365groupOwners,
		{
			{ nullptr, _YTEXT(""), _YTEXT("") },
			{ nullptr, _YTEXT(""), _YTEXT("") },
			{ &m_TemplateUsers.m_ColumnMetaID, YtriaTranslate::Do(GridGroupsOwnersHierarchy_customizeGrid_1, _YLOC("Group ID")).c_str(), GridTemplate::g_TypeGroup }
		});
		setup.Setup(*this, true);
		m_TemplateGroups.m_ColumnLastRequestDateTime = setup.GetColumnLastRequestDateTime();
	}

	// We create multivalue-ids for child-groups.
	m_TemplateUsers.m_ColumnMetaID->SetExplosionEnabled(false);

	m_TemplateGroups.m_ColumnDisplayName		= AddColumn(_YUID(O365_GROUP_DISPLAYNAME),	YtriaTranslate::Do(GridGroupsOwnersHierarchy_customizeGrid_2, _YLOC("Group Display Name")).c_str(),	GridTemplate::g_TypeGroup,	g_ColumnSize22, { g_ColumnsPresetDefault });
	m_TemplateGroups.m_ColumnIsTeam				= AddColumnIcon(_YUID(O365_GROUP_ISTEAM),		YtriaTranslate::Do(GridGroups_customizeGrid_3, _YLOC("Is a Team")).c_str(),	GridTemplate::g_TypeGroup);
	SetColumnPresetFlags(m_TemplateGroups.m_ColumnIsTeam, { g_ColumnsPresetDefault }); 
	m_TemplateGroups.m_ColumnCombinedGroupType	= AddColumn(_YUID("groupType"),				YtriaTranslate::Do(GridGroupsOwnersHierarchy_customizeGrid_3, _YLOC("Group Type")).c_str(),			GridTemplate::g_TypeGroup,	g_ColumnSize12, { g_ColumnsPresetDefault });
	setBasicGridSetupHierarchy({ { { m_TemplateGroups.m_ColumnDisplayName, GridBackendUtil::g_AllHierarchyLevels } }
								, O365Grid::BasicGridSetupHierarchy::AllUserObjectTypes()
								,{ rttr::type::get<BusinessUser>().get_id(), rttr::type::get<BusinessGroup>().get_id() } });

	// Do not insert columns that are also PK
	m_MetaColumns.insert(m_MetaColumns.end(), { /*m_TemplateUsers.m_ColumnMetaID, */m_TemplateGroups.m_ColumnDisplayName, m_TemplateGroups.m_ColumnIsTeam, m_TemplateGroups.m_ColumnCombinedGroupType });

	/*Add additional meta columns*/
	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		for (const auto& c : *metaDataColumnInfos)
		{
			ASSERT(nullptr == m_TemplateGroups.GetColumnForProperty(c.GetPropertyName()));
			if (nullptr == m_TemplateGroups.GetColumnForProperty(c.GetPropertyName()))
			{
				auto col = c.AddTo(*this, GridTemplate::g_TypeGroup);
				m_TemplateGroups.GetColumnForProperty(c.GetPropertyName()) = col;
				m_MetaColumns.push_back(col);
				m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
			}
		}
	}

	m_TemplateUsers.m_ColumnDisplayName			= AddColumn(_YUID(USER_DISPLAY_NAME_COL_UID),	g_TitleOwnerDisplayName,	GridTemplate::g_TypeUser,	g_ColumnSize22, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnUserPrincipalName	= AddColumn(_YUID(O365_USER_USERPRINCIPALNAME), g_TitleOwnerUserName,		GridTemplate::g_TypeUser,	g_ColumnSize32);
	m_TemplateUsers.m_ColumnUserType			= AddColumn(_YUID(O365_USER_USERTYPE),			g_TitleOwnerUserType,		GridTemplate::g_TypeUser,	g_ColumnSize12);
	addColumnGraphID(GridTemplate::g_TypeUser);
	m_TemplateGroups.m_ColumnID = GetColId();
	m_TemplateUsers.m_ColumnID	= GetColId();
	m_TemplateGroups.CustomizeGrid(*this);

	AddColumnForRowPK(m_TemplateUsers.m_ColumnMetaID);
	AddColumnForRowPK(GetColId());

	m_Frame = dynamic_cast<FrameGroupOwnersHierarchy*>(GetParentFrame());
    m_Frame->SetGrid(this);
	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_Frame);
}

wstring GridGroupsOwnersHierarchy::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Group"), m_TemplateGroups.m_ColumnDisplayName } }, m_TemplateUsers.m_ColumnDisplayName);
}

void GridGroupsOwnersHierarchy::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_OWNERSSGRID_ADD);
		pPopup->ItemInsert(); // Sep
		pPopup->ItemInsert(ID_OWNERSSGRID_DELETE);
		pPopup->ItemInsert(); // Sep
	}

	GridGroupsCommon::OnCustomPopupMenu(pPopup, nStart);
}

void GridGroupsOwnersHierarchy::InitializeCommands()
{
	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_OWNERSSGRID_ADD;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_1, _YLOC("Add Owners...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_2, _YLOC("Add Owners...")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_ADDOWNER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_OWNERSSGRID_ADD, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_3, _YLOC("Add Owners to Groups")).c_str(), YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_4, _YLOC("Add users as owners to all selected groups. You will be able to select users from a directory list.")).c_str(), _cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_OWNERSSGRID_DELETE;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_5, _YLOC("Remove Owners...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_6, _YLOC("Remove Owners...")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_REMOVEOWNER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_OWNERSSGRID_DELETE, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_7, _YLOC("Remove Group Owners")).c_str(), YtriaTranslate::Do(GridGroupsOwnersHierarchy_InitializeCommands_8, _YLOC("Remove all selected group owners from their respective group owner list.")).c_str(), _cmd.m_sAccelText);
	}

	GridGroupsCommon::InitializeCommands();
}

void GridGroupsOwnersHierarchy::OnAddItemsToGroups()
{
	if (IsFrameConnected())
	{
		// FIXME: handle roles if not done in showDlgItemsToGroup
		showDlgItemsToGroup(BusinessObject::UPDATE_TYPE_ADDOWNERS);
	}
}

void GridGroupsOwnersHierarchy::OnUpdateAddItemsToGroups(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& IsFrameConnected()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& p_Row)
		{
			auto ref = nullptr != p_Row ? p_Row->GetTopAncestorOrThis() : nullptr;
			return nullptr != ref
				&& !ref->IsGroupRow()
				&& IsGroupAuthorizedByRoleDelegation(ref, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT);
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsOwnersHierarchy::OnRemoveItemsFromGroups()
{
	// FIXME: handle roles if not done in removeSelectedItemsFromGroup
	removeSelectedItemsFromGroup(BusinessObject::UPDATE_TYPE_DELETEOWNERS);
}

void GridGroupsOwnersHierarchy::OnUpdateRemoveItemsFromGroups(CCmdUI* pCmdUI)
{
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE);

	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, hasOutOfScopePrivilege](auto& p_Row)
		{
			auto ref = nullptr != p_Row ? p_Row->GetTopAncestor() : nullptr;
			return nullptr != ref
				&& !p_Row->IsGroupRow()
				&& !p_Row->IsDeleted()
				&& IsGroupAuthorizedByRoleDelegation(ref, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT)
				&& (hasOutOfScopePrivilege || IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridGroupsOwnersHierarchy::AddGroups(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge)
{
	const uint32_t options	= GridUpdaterOptions::UPDATE_GRID
							| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
							| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
							| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
							/*| GridUpdaterOptions::REFRESH_PROCESS_DATA*/
							/*| GridUpdaterOptions::PROCESS_LOADMORE*/
                            ;
	GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateOperations);
    GridIgnoreModification ignoramus(*this);

	bool oneCanceled = false;
	bool oneOK = false;
	for (const auto group : p_Groups)
	{
		if (m_HasLoadMoreAdditionalInfo)
		{
			if (group.IsMoreLoaded())
				m_MetaIDsMoreLoaded.insert(group.GetID());
			else
				m_MetaIDsMoreLoaded.erase(group.GetID());
		}

		auto row = addRowHierarchy(group, nullptr, updater, true);
		ASSERT(nullptr != row);
        if (nullptr != row)
        {
			updater.GetOptions().AddRowWithRefreshedValues(row);
			if (group.HasFlag(BusinessObject::CANCELED))
			{
				oneCanceled = true;
			}
			else
			{
				oneOK = true;
				updater.GetOptions().AddPartialPurgeParentRow(row);
			}
        }

		if (oneCanceled)
		{
			if (oneOK)
				updater.GetOptions().SetPurgeFlag(GridUpdaterOptions::PARTIALPURGE);
			else
				updater.GetOptions().SetPurgeFlag(0); // Everything canceled, don't purge anything.
		}
	}
}

GridBackendRow* GridGroupsOwnersHierarchy::addRowHierarchy(const BusinessGroup& p_Group, const BusinessGroup* p_ParentGroup, GridUpdater& p_Updater, bool p_KeepExistingIfGroupCanceled)
{
	GridBackendRow* row = m_TemplateGroups.AddRow(*this, p_Group, {}, p_Updater, false, nullptr, {}, p_KeepExistingIfGroupCanceled);

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		p_Updater.GetOptions().AddRowWithRefreshedValues(row);

		if (p_Group.HasFlag(BusinessObject::CANCELED))
		{
			// Could be uncommented if we notice missing explosion refresh on canceled rows
			//auto children = GetChildRows(row);
			//for (auto child : children)
			//	p_Updater.GetOptions().AddRowWithRefreshedValues(child);
		}
		else
		{
			const vector<GridBackendField> extraPK{ m_TemplateUsers.GetMetaIDField(p_Group.GetID()) };

			row->SetHierarchyParentToHide(true);
			if (p_Group.GetOwnersError())
			{
				p_Updater.OnLoadingError(row, *p_Group.GetOwnersError());
			}
			else
			{
				// Clear previous error
				if (row->IsError())
					row->ClearStatus();

				const auto handleChildRow = [&p_Updater, &row, &p_Group, this](GridBackendRow* childRow)
				{
					if (nullptr != childRow)
					{
						p_Updater.GetOptions().AddRowWithRefreshedValues(childRow);
						childRow->SetParentRow(row);

						for (auto c : m_MetaColumns)
						{
							if (row->HasField(c))
								childRow->AddField(row->GetField(c));
							else
								childRow->RemoveField(c);
						}

						if (row->HasField(m_TemplateGroups.m_ColumnLastRequestDateTime))
							childRow->AddField(row->GetField(m_TemplateGroups.m_ColumnLastRequestDateTime));
					}
				};

				for (const auto& childUserID : p_Group.GetOwnerUsers())
				{
					auto child = BusinessUser(GetGraphCache().GetUserInCache(childUserID, false));
					ASSERT(child.GetID() == childUserID);

					GridBackendRow* childRow = m_TemplateUsers.AddRow(*this, child, extraPK, p_Updater, false, false);
					ASSERT(nullptr != childRow);
					handleChildRow(childRow);
					AddRoleDelegationFlag(childRow, child);
				}

				/*for (const auto& childGroupId : p_Group.GetOwnerGroups())
				{
					auto child = BusinessGroup(GetGraphCache().GetGroupInCache(childGroupId));
					ASSERT(child.GetID() == childGroupId);

					GridBackendRow* childRow = m_TemplateGroups.AddRow(*this, p_Group, extraPK, p_Updater, false, nullptr, {}, p_KeepExistingIfGroupCanceled);
					ASSERT(nullptr != childRow);
					handleChildRow(childRow);
				}*/
			}
		}

		AddRoleDelegationFlag(row, p_Group);
	}

	return row;
}