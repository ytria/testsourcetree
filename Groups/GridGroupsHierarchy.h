#pragma once

#include "BusinessUser.h"
#include "GridGroupsCommon.h"
#include "GridTemplateGroups.h"
#include "GridTemplateUsers.h"

#define MEMBERS_GRID_HIDE_NESTED 1

class BusinessGroup;
class FrameGroupMembersHierarchy;
class GridGroupsHierarchy : public GridGroupsCommon
{
public:
	GridGroupsHierarchy();
    virtual ~GridGroupsHierarchy() override = default;

	void AddGroups(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;
	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

	void SetShowNested(const bool p_ShowNested);

protected:
	virtual void customizeGrid() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
	virtual void InitializeCommands() override;

	afx_msg void OnAddItemsToGroups();
	afx_msg void OnUpdateAddItemsToGroups(CCmdUI* pCmdUI);
	afx_msg void OnRemoveItemsFromGroups();
	afx_msg void OnUpdateRemoveItemsFromGroups(CCmdUI* pCmdUI);

	afx_msg void OnAddOwnersToGroups();
	afx_msg void OnUpdateAddOwnersToGroups(CCmdUI* pCmdUI);

	afx_msg void OnUpgradeToOwner();
	afx_msg void OnUpdateUpgradeToOwner(CCmdUI* pCmdUI);
	afx_msg void OnDowngradeToMember();
	afx_msg void OnUpdateDowngradeToMember(CCmdUI* pCmdUI);
	afx_msg void OnAddToMembers();
	afx_msg void OnUpdateAddToMembers(CCmdUI* pCmdUI);

	afx_msg void OnCopySelectionToGroups();
	afx_msg void OnUpdateCopySelectionToGroups(CCmdUI* pCmdUI);
	afx_msg void OnMoveSelectionToGroups();
	afx_msg void OnUpdateMoveSelectionToGroups(CCmdUI* pCmdUI);
#if MEMBERS_GRID_HIDE_NESTED
	afx_msg void OnShowNested();
	afx_msg void OnUpdateShowNested(CCmdUI* pCmdUI);
#endif

	DECLARE_MESSAGE_MAP()

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

private:
	GridBackendRow* addRowHierarchy(const BusinessGroup& p_Group, GridBackendRow* p_ParentRow, vector<PooledString>& p_ParentIds, GridUpdater& p_Updater, bool p_KeepExistingIfGroupCanceled);
	void copySelectedMembersToGroups(bool removeFromSourceGroup);
	void addMembersToGroupsImpl(const vector<SelectedItem>& p_NewGroups, const std::set<GridBackendRow*>& p_Rows, bool removeFromSourceGroup, bool shouldScrollToFirstAddedRow);

	void addCommonFields(GridBackendRow* p_ChildRow);

	enum Kind
	{
		MEMBER_USER,
		OWNER_MEMBER_USER,
		EXTERNAL_OWNER,
		MEMBER_GUEST,
		OWNER_MEMBER_GUEST,
		EXTERNAL_OWNER_GUEST,
		MEMBER_GROUP,
		MEMBER_ORGCONTACT,
		INDIRECT_MEMBER_USER,
		INDIRECT_MEMBER_GUEST,
		INDIRECT_MEMBER_GROUP,
		INDIRECT_MEMBER_ORGCONTACT,

		numKinds,
	};
	void setRowKind(GridBackendRow* p_ChildRow, Kind p_Kind);
	void setMemberRowKind(GridBackendRow* p_ChildRow);
	void setDirectMemberRowKind(GridBackendRow* p_ChildRow);
	void setIndirectMemberRowKind(GridBackendRow* p_ChildRow);

	bool isDirectChild(GridBackendRow* p_ChildRow) const;
	bool isDirectMember(GridBackendRow* p_ChildRow) const;
	bool isOwnerCandidate(GridBackendRow* p_ChildRow) const;
	bool isOwner(GridBackendRow* p_ChildRow) const;
	bool isExternalOwner(GridBackendRow* p_ChildRow) const;

	void addHierarchyPathField(GridBackendRow* p_ChildRow);

	bool upgradeExternalToRegularOwner(GridBackendRow* p_Row);
	bool upgradeMemberToOwner(GridBackendRow* p_Row);
	bool downgradeOwnerToMember(GridBackendRow* p_Row);
	bool changeRegularToExternalOwner(GridBackendRow* p_Row);

	void setParamAddMember(GridBackendRow* p_Row);
	void setParamDeleteMember(GridBackendRow* p_Row);
	void setParamAddOwner(GridBackendRow* p_Row);
	void setParamAddMemberAddOwner(GridBackendRow* p_Row);
	void setParamDeleteOwner(GridBackendRow* p_Row);
	void setParamAddMemberDeleteOwner(GridBackendRow* p_Row);
	void setParamDeleteMemberDeleteOwner(GridBackendRow* p_Row);

	bool canRowBeDeleted(GridBackendRow* p_Row);

#if MEMBERS_GRID_HIDE_NESTED
	void refreshNestedGroupsRowDisplay(bool p_Update);
	void refreshNestedGroupsColumnDisplay(bool p_Update);
#endif

	FrameGroupMembersHierarchy*		m_Frame;
	vector<PooledString>			m_BufferHierarchyGroupIDs;
	vector<PooledString>			m_BufferHierarchyGroupNames;

	GridBackendColumn* m_ColumnMemberKind;
	GridBackendColumn* m_ColumnHierarchyPath;
	GridBackendColumn* m_ColumnHasLicensesAssigned;
	using KindInfo = std::pair<int, wstring>;
	using KindInfos = std::map<Kind, KindInfo>;
	KindInfos m_KindInfos;

#if MEMBERS_GRID_HIDE_NESTED
	bool m_ShowNestedGroupMembers;
	bool m_HasNestedGroupsMembers;
#endif

	struct PendingAddMembers
	{
		vector<SelectedItem> m_Groups;
		std::set<GridBackendRow*> m_MembersToAddTo;
		bool m_ShouldScroll = false;
		bool m_ShouldRemoveFromSource = false;
	};
	boost::YOpt<PendingAddMembers> m_PendingAddMembers;
};
