#include "FrameGroupOwnersHierarchy.h"

#include "CommandDispatcher.h"

IMPLEMENT_DYNAMIC(FrameGroupOwnersHierarchy, FrameGroupCommon)

void FrameGroupOwnersHierarchy::createGrid()
{
	m_groupsGrid.SetAutomationName(GridUtil::g_AutoNameGroupsOwnersHierarchy);
	BOOL gridCreated = m_groupsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FrameGroupOwnersHierarchy::customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(p_Tab);
	CreateRefreshGroup(p_Tab);
	CreateApplyGroup(p_Tab);
	CreateRevertGroup(p_Tab);

	auto groupCtrl1 = p_Tab.AddGroup(YtriaTranslate::Do(FrameGroupOwnersHierarchy_customizeActionsRibbonTab_2, _YLOC("Group")).c_str());
	setGroupImage(*groupCtrl1, 0);

	{
		auto ctrl = groupCtrl1->Add(xtpControlButton, ID_OWNERSSGRID_ADD);
		GridFrameBase::setImage({ ID_OWNERSSGRID_ADD }, IDB_GROUPS_ADDOWNER, xtpImageNormal);
		GridFrameBase::setImage({ ID_OWNERSSGRID_ADD }, IDB_GROUPS_ADDOWNER_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	auto groupCtrl = p_Tab.AddGroup(YtriaTranslate::Do(FrameGroupOwnersHierarchy_customizeActionsRibbonTab_1, _YLOC("Members")).c_str());
	setGroupImage(*groupCtrl, 0);

	{
		auto ctrl = groupCtrl->Add(xtpControlButton, ID_OWNERSSGRID_DELETE);
		GridFrameBase::setImage({ ID_OWNERSSGRID_DELETE }, IDB_GROUPS_REMOVEOWNER, xtpImageNormal);
		GridFrameBase::setImage({ ID_OWNERSSGRID_DELETE }, IDB_GROUPS_REMOVEOWNER_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	CreateLinkGroups(p_Tab, true, true);

	automationAddCommandIDToGreenLight(ID_OWNERSSGRID_ADD);
	automationAddCommandIDToGreenLight(ID_OWNERSSGRID_DELETE);

	return true;
}

void FrameGroupOwnersHierarchy::ShowHierarchies(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge)
{
	m_groupsGrid.AddGroups(p_Groups, p_UpdateOperations, p_FullPurge);
}

O365Grid& FrameGroupOwnersHierarchy::GetGrid()
{
	return m_groupsGrid;
}

AutomatedApp::AUTOMATIONSTATUS FrameGroupOwnersHierarchy::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionOwnerAdd(p_Action))
		p_CommandID = ID_OWNERSSGRID_ADD;
	else if (IsActionOwnerRemove(p_Action))
		p_CommandID = ID_OWNERSSGRID_DELETE;

	return statusRV;
}