#pragma once

#include "BaseO365Grid.h"
#include "BusinessOrgContact.h"
#include "GridTemplateGroups.h"
#include "GridTemplateUsers.h"

class GridGroupsCommon : public O365Grid
{
public:
	GridGroupsCommon();
    virtual ~GridGroupsCommon() override = default;

    GridBackendColumn* GetColMetaID() const;

	struct GroupChange
	{
		PooledString m_GroupID;
		PooledString m_ItemID;
		enum
		{
			Unknown = 0,
			UserItem,
			GroupItem,
			OrgContactItem
		} m_ItemType = Unknown;

		boost::YOpt<row_pk_t> m_RowPrimaryKey;
		boost::YOpt<vector<row_pk_t>> m_SiblingRowPrimaryKeys;
		BusinessObject::UPDATE_TYPE m_UpdateType;
	};
	using GroupChanges = std::vector<GroupChange>;
	GroupChanges GetGroupChanges(bool p_Selected, bool p_TopLevel);

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

	virtual bool HasModificationsPending(bool inSelectionOnly = false) override;
	virtual bool HasRevertableModificationsPending(bool inSelectionOnly = false) override;

	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

protected:
	DECLARE_MESSAGE_MAP()

	virtual void showDlgItemsToGroup(BusinessObject::UPDATE_TYPE p_UpdateType);
	virtual void addFieldSpecific(GridBackendRow* p_pRow, BusinessObject::UPDATE_TYPE p_UpdateType);

	virtual bool isAddEnabledForSelection() const;

	void removeItemsFromGroup(BusinessObject::UPDATE_TYPE p_UpdateType, const set<GridBackendRow*>& p_Rows, bool p_Update, bool p_DoNotUnSelectAll);
	void removeSelectedItemsFromGroup(BusinessObject::UPDATE_TYPE p_UpdateType);

	void getUsersGroupsAndOrgContacts(vector<BusinessUser>& p_Users, bool fillUsers, vector<BusinessGroup>& p_Groups, bool fillGroups, vector<BusinessOrgContact>& p_OrgContacts, bool fillOrgContacts);

	static RoleDelegationUtil::RBAC_Privilege getPrivilege(BusinessObject::UPDATE_TYPE p_UpdateType);
	static RoleDelegationUtil::RBAC_Privilege getOutOfScopePrivilege(BusinessObject::UPDATE_TYPE p_UpdateType);

protected:
	GridTemplateGroups		m_TemplateGroups;
	GridTemplateUsers		m_TemplateUsers;

	GridBackendColumn*		m_ColumnGroupNameForMember; // Optional column used in GridGroupsAuthorsHierarchy/GridGroupsHierarchy
	GridBackendColumn*		m_ColumnCombinedGroupTypeForMember; // Optional column used in GridGroupsAuthorsHierarchy/GridGroupsHierarchy
	GridBackendColumn*		m_ColumnIsTeamForMember; // Optional column used in GridGroupsAuthorsHierarchy/GridGroupsHierarchy

	vector<GridBackendColumn*>	m_MetaColumns;
	bool m_HasLoadMoreAdditionalInfo;
	O365IdsContainer m_MetaIDsMoreLoaded;
};