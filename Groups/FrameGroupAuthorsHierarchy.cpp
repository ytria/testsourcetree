#include "FrameGroupAuthorsHierarchy.h"

#include "CommandDispatcher.h"

IMPLEMENT_DYNAMIC(FrameGroupAuthorsHierarchy, FrameGroupCommon)

void FrameGroupAuthorsHierarchy::createGrid()
{
	m_groupsGrid.SetAutomationName(GridUtil::g_AutoNameGroupsAuthorsHierarchy);
	BOOL gridCreated = m_groupsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FrameGroupAuthorsHierarchy::customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(p_Tab);
	CreateRefreshGroup(p_Tab);
	CreateApplyGroup(p_Tab);
	CreateRevertGroup(p_Tab);

	auto groupCtrl1 = p_Tab.AddGroup(YtriaTranslate::Do(FrameGroupAuthorsHierarchy_customizeActionsRibbonTab_2, _YLOC("Group")).c_str());
	setGroupImage(*groupCtrl1, 0);

	{
		auto ctrl = groupCtrl1->Add(xtpControlButton, ID_AUTHORSGRID_ADDACCEPTED);
		GridFrameBase::setImage({ ID_AUTHORSGRID_ADDACCEPTED }, IDB_GROUPS_ADDAUTHORSACCEPTED, xtpImageNormal);
		GridFrameBase::setImage({ ID_AUTHORSGRID_ADDACCEPTED }, IDB_GROUPS_ADDAUTHORSACCEPTED_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	{
		auto ctrl = groupCtrl1->Add(xtpControlButton, ID_AUTHORSGRID_ADDREJECTED);
		GridFrameBase::setImage({ ID_AUTHORSGRID_ADDREJECTED }, IDB_GROUPS_ADDAUTHORSREJECTED, xtpImageNormal);
		GridFrameBase::setImage({ ID_AUTHORSGRID_ADDREJECTED }, IDB_GROUPS_ADDAUTHORSREJECTED_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	auto groupCtrl = p_Tab.AddGroup(YtriaTranslate::Do(FrameGroupAuthorsHierarchy_customizeActionsRibbonTab_1, _YLOC("Members")).c_str());
	setGroupImage(*groupCtrl, 0);

	{
		auto ctrl = groupCtrl->Add(xtpControlButton, ID_AUTHORSGRID_DELETEAUTHORIZATION);
		GridFrameBase::setImage({ ID_AUTHORSGRID_DELETEAUTHORIZATION }, IDB_GROUPS_REMOVEAUTHORIZATION, xtpImageNormal);
		GridFrameBase::setImage({ ID_AUTHORSGRID_DELETEAUTHORIZATION }, IDB_GROUPS_REMOVEAUTHORIZATION_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	CreateLinkGroups(p_Tab, true, true);

	automationAddCommandIDToGreenLight(ID_AUTHORSGRID_ADDACCEPTED);
	automationAddCommandIDToGreenLight(ID_AUTHORSGRID_ADDREJECTED);

	return true;
}

void FrameGroupAuthorsHierarchy::ShowHierarchies(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge)
{
	m_groupsGrid.AddGroups(p_Groups, p_UpdateOperations, p_FullPurge);
}

O365Grid& FrameGroupAuthorsHierarchy::GetGrid()
{
	return m_groupsGrid;
}

AutomatedApp::AUTOMATIONSTATUS FrameGroupAuthorsHierarchy::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionGroupSendersAddAccepted(p_Action))
		p_CommandID = ID_AUTHORSGRID_ADDACCEPTED;
	else if (IsActionGroupSendersAddRejected(p_Action))
		p_CommandID = ID_AUTHORSGRID_ADDREJECTED;

	return statusRV;
}