#include "GridGroupsCommon.h"

#include "BusinessGroup.h"
#include "DlgAddItemsToGroups.h"
#include "../Resource.h"

BEGIN_MESSAGE_MAP(GridGroupsCommon, O365Grid)
END_MESSAGE_MAP()

GridGroupsCommon::GridGroupsCommon()
	: m_ColumnGroupNameForMember(nullptr)
	, m_ColumnCombinedGroupTypeForMember(nullptr)
	, m_ColumnIsTeamForMember(nullptr)
	, m_HasLoadMoreAdditionalInfo(false)
{
	EnableGridModifications();
}

GridBackendColumn* GridGroupsCommon::GetColMetaID() const
{
    return m_TemplateUsers.m_ColumnMetaID;
}

GridGroupsCommon::GroupChanges GridGroupsCommon::GetGroupChanges(bool p_Selected, bool p_TopLevel)
{
	GroupChanges groupChanges;

	vector<GridBackendRow*> rows;
	if (p_Selected && p_TopLevel)
	{
		std::set<GridBackendRow*> topLevelRows;
		for (auto row : GetSelectedRows())
		{
			auto topLevelRow = row->GetTopAncestorOrThis();
			topLevelRows.insert(topLevelRow);
		}

		OnUnSelectAll();
		auto modRows = GetModifications().GetModifiedRows(true, false);
		for (auto row : modRows)
		{
			if (topLevelRows.end() != topLevelRows.find(row->GetTopAncestorOrThis()))
			{
				row->SetSelected(true);
				rows.push_back(row);
			}
		}
	}
	else if (p_Selected)
    {
		GetSelectedNonGroupRows(rows);
	}

	for (auto row : (p_Selected ? rows : GetBackendRows()))
	{
		if (!row->IsGroupRow())
		{
			if (row->IsCreated() || row->IsDeleted() || row->IsModified())
			{
				auto parentRow = row->GetParentRow();
				ASSERT(nullptr != parentRow && GridUtil::IsBusinessGroup(parentRow));
				if (nullptr != parentRow && GridUtil::IsBusinessGroup(parentRow))
				{
					ASSERT(row->HasField(GetColId()));
					const PooledString itemID = row->GetField(GetColId()).GetValueStr();
					ASSERT(parentRow->HasField(GetColId()));
					const PooledString groupID = parentRow->GetField(GetColId()).GetValueStr();

					auto it = std::find_if(groupChanges.begin(), groupChanges.end(), [&groupID, &itemID](GroupChange& groupChange) {return groupChange.m_GroupID == groupID && groupChange.m_ItemID == itemID; });
					if (it == groupChanges.end())
					{
						groupChanges.emplace_back();
						GroupChange& groupChange = groupChanges.back();

						ASSERT(parentRow->HasField(GetColId()));
						groupChange.m_GroupID = groupID;

						groupChange.m_ItemType = GridUtil::IsBusinessUser(row) ? GroupChange::UserItem
							: GridUtil::IsBusinessGroup(row) ? GroupChange::GroupItem
							: GridUtil::IsBusinessOrgContact(row) ? GroupChange::OrgContactItem
							: GroupChange::Unknown;
						ASSERT(GroupChange::Unknown != groupChange.m_ItemType);

						ASSERT(row->HasField(GetColId()));
						groupChange.m_ItemID = itemID;
						groupChange.m_RowPrimaryKey.emplace();
						GetRowPK(row, *groupChange.m_RowPrimaryKey);

						auto val = row->GetLParam();
						ASSERT(BusinessObject::UPDATE_TYPE_ADDMEMBERS == val
							|| BusinessObject::UPDATE_TYPE_ADDOWNERS == val
							|| BusinessObject::UPDATE_TYPE_ADDAUTHORACCEPTED == val
							|| BusinessObject::UPDATE_TYPE_ADDAUTHORREJECTED == val
							|| BusinessObject::UPDATE_TYPE_DELETEMEMBERS == val
							|| BusinessObject::UPDATE_TYPE_DELETEOWNERS == val
							|| BusinessObject::UPDATE_TYPE_DELETEACCEPTEDAUTHORS == val
							|| BusinessObject::UPDATE_TYPE_DELETEREJECTEDAUTHORS == val
							|| (BusinessObject::UPDATE_TYPE_ADDMEMBERS | BusinessObject::UPDATE_TYPE_DELETEOWNERS) == val						
							|| (BusinessObject::UPDATE_TYPE_ADDMEMBERS | BusinessObject::UPDATE_TYPE_ADDOWNERS) == val
							|| (BusinessObject::UPDATE_TYPE_DELETEMEMBERS | BusinessObject::UPDATE_TYPE_DELETEOWNERS) == val
						);

						if ((BusinessObject::UPDATE_TYPE_ADDMEMBERS | BusinessObject::UPDATE_TYPE_DELETEOWNERS) == val
							|| (BusinessObject::UPDATE_TYPE_ADDMEMBERS | BusinessObject::UPDATE_TYPE_ADDOWNERS) == val
							|| (BusinessObject::UPDATE_TYPE_DELETEMEMBERS | BusinessObject::UPDATE_TYPE_DELETEOWNERS) == val)
						{
							if (BusinessObject::UPDATE_TYPE_ADDMEMBERS == (BusinessObject::UPDATE_TYPE_ADDMEMBERS & val))
								groupChange.m_UpdateType = BusinessObject::UPDATE_TYPE_ADDMEMBERS;
							else if (BusinessObject::UPDATE_TYPE_DELETEMEMBERS == (BusinessObject::UPDATE_TYPE_DELETEMEMBERS & val))
								groupChange.m_UpdateType = BusinessObject::UPDATE_TYPE_DELETEMEMBERS;

							GroupChange groupChange2 = groupChange;
							groupChange2.m_UpdateType = (BusinessObject::UPDATE_TYPE)(val & ~groupChange.m_UpdateType);
							groupChanges.push_back(groupChange2);
						}
						else
							groupChange.m_UpdateType = (BusinessObject::UPDATE_TYPE)val;
					}
					else
					{
						GroupChange& groupChange = *it;
						ASSERT(groupChange.m_ItemType == (GridUtil::IsBusinessUser(row) ? GroupChange::UserItem
														: GridUtil::IsBusinessGroup(row) ? GroupChange::GroupItem
														: GridUtil::IsBusinessOrgContact(row) ? GroupChange::OrgContactItem
														: GroupChange::Unknown)
						);
						ASSERT(row->HasField(GetColId()) && groupChange.m_ItemID == row->GetField(GetColId()).GetValueStr());
						ASSERT(parentRow->HasField(GetColId()) && groupChange.m_GroupID == parentRow->GetField(GetColId()).GetValueStr());
						ASSERT((BusinessObject::UPDATE_TYPE)row->GetLParam() == groupChange.m_UpdateType);
						if (!groupChange.m_SiblingRowPrimaryKeys)
							groupChange.m_SiblingRowPrimaryKeys.emplace();
						groupChange.m_SiblingRowPrimaryKeys->emplace_back();
						GetRowPK(row, groupChange.m_SiblingRowPrimaryKeys->back());
					}
				}
			}
		}
	}

	//std::sort(groupChanges.begin(), groupChanges.end(), [](const GroupChange& gc1, const GroupChange& gc2) {return gc1.m_GroupID < gc2.m_GroupID; });
	return groupChanges;
}

ModuleCriteria GridGroupsCommon::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    GridFrameBase* frame = dynamic_cast<GridFrameBase*>(GetParent());
    ASSERT(nullptr != frame);
    if (nullptr != frame)
    {
        ModuleCriteria criteria = frame->GetModuleCriteria();
        criteria.m_IDs.clear();

        for (auto row : p_Rows)
        {
            ASSERT(m_TemplateGroups.m_ColumnID != nullptr || m_TemplateUsers.m_ColumnID != nullptr);
            if (m_TemplateGroups.m_ColumnID != nullptr)
                criteria.m_IDs.insert(row->GetField(m_TemplateGroups.m_ColumnID).GetValueStr());
            else if (m_TemplateUsers.m_ColumnID != nullptr)
                criteria.m_IDs.insert(row->GetField(m_TemplateUsers.m_ColumnID).GetValueStr());
        }
        
        return criteria;
    }

    return ModuleCriteria();
}

bool GridGroupsCommon::HasModificationsPending(bool inSelectionOnly /*= false*/)
{
	if (inSelectionOnly)
		return HasModificationsPendingInParentOfSelected();
	return O365Grid::HasModificationsPending(false);
}

bool GridGroupsCommon::HasRevertableModificationsPending(bool inSelectionOnly /*= false*/)
{
	// No distinction like in GridGroupsCommon::HasModificationsPending as revert only concerns really selected rows.
	return O365Grid::HasModificationsPending(inSelectionOnly);
}

bool GridGroupsCommon::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	const auto groupID = [this, p_Row]()
	{
		// Until we fix it, some grids may have empty meta ID for meta rows...
		if (p_Row->HasField(GetColMetaID()))
			return p_Row->GetField(GetColMetaID()).GetValueStr();
		return p_Row->GetField(GetColId()).GetValueStr();
	};

	return O365Grid::IsRowValidForNoFieldText(p_Row, p_Col)
		&& (!p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore)
			|| m_MetaIDsMoreLoaded.end() == m_MetaIDsMoreLoaded.find(groupID()));
}

void GridGroupsCommon::showDlgItemsToGroup(BusinessObject::UPDATE_TYPE p_UpdateType)
{
	ASSERT(IsFrameConnected());
	ASSERT(GetAutomationName() != GridUtil::g_AutoNameGroupsHierarchy); // No used anymore for that grid
	ASSERT(BusinessObject::UPDATE_TYPE_ADDMEMBERS			== p_UpdateType
		|| BusinessObject::UPDATE_TYPE_ADDOWNERS			== p_UpdateType
		|| BusinessObject::UPDATE_TYPE_ADDAUTHORACCEPTED	== p_UpdateType
		|| BusinessObject::UPDATE_TYPE_ADDAUTHORREJECTED	== p_UpdateType);

	const auto privilege = getPrivilege(p_UpdateType);
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(getOutOfScopePrivilege(p_UpdateType));

	bool modifyMembersWarn = BusinessObject::UPDATE_TYPE_ADDMEMBERS == p_UpdateType;

	// <Group Id, <Group name, User ids>>
	std::map<PooledString, std::pair<PooledString, std::set<PooledString>>> parentsForSelection;
	set<GridBackendRow*>	businessGroupRows;

	auto addTargetGroup = [this, &businessGroupRows, &parentsForSelection, &modifyMembersWarn, &privilege](GridBackendRow* row)
	{
		GridBackendRow* businessGroupRow = row->GetTopAncestorOrThis();
		if (nullptr != businessGroupRow && IsGroupAuthorizedByRoleDelegation(businessGroupRow, privilege))
		{
			if (modifyMembersWarn)
			{
				const auto isGroup = GridUtil::IsBusinessGroup(row);
				if (isGroup && row != businessGroupRow
					|| !isGroup && row->GetParentRow() != businessGroupRow)
				{
					YCodeJockMessageBox dlg(this
						, DlgMessageBox::eIcon_Information
						, YtriaTranslate::Do(GridGroupsHierarchy_copySelectedMembersToGroups_9, _YLOC("Add Member")).c_str()
						, YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_10, _YLOC("New members will be added to top-level groups only!")).c_str()
						, _YTEXT("")
						, { { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });

					if (IDCANCEL == dlg.DoModal())
						return false;

					modifyMembersWarn = false;
				}
			}

			ASSERT(GridUtil::IsBusinessGroup(businessGroupRow));
			if (GridUtil::IsBusinessGroup(businessGroupRow) && businessGroupRows.insert(businessGroupRow).second)
				parentsForSelection[businessGroupRow->GetField(GetColId()).GetValueStr()] = { businessGroupRow->GetField(m_TemplateGroups.m_ColumnDisplayName).GetValueStr(), GetIdsOfChildRowsThatAllMatch({ businessGroupRow }) };
		}

		return true;
	};

	for (auto row : GetSelectedRows())
	{
		ASSERT(nullptr != row);
		if (nullptr != row && !row->IsGroupRow())
		{
			if (!addTargetGroup(row))
				return;
		}
	}

	ASSERT(!businessGroupRows.empty());
	if (!businessGroupRows.empty())
	{
		DlgAddItemsToGroups::LOADTYPE loadType = DlgAddItemsToGroups::SELECT_USERS_FOR_GROUPS;
		wstring	title;
		wstring	explanation;

		if (BusinessObject::UPDATE_TYPE_ADDMEMBERS == p_UpdateType)
		{
			ASSERT(GetAutomationName() == GridUtil::g_AutoNameGroupsHierarchy);
			loadType = DlgAddItemsToGroups::SELECT_USERSGROUPSANDORGCONTACTS_FOR_GROUPS;
			title			= YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_7, _YLOC("Add to %1 selected groups"), Str::getStringFromNumber(businessGroupRows.size()).c_str());
			explanation		= YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_8, _YLOC("These %1 groups will be modified by adding any members you select from the list below.\nTo see the complete list, use Load full directory."), Str::getStringFromNumber(businessGroupRows.size()).c_str());
		}
		else if (BusinessObject::UPDATE_TYPE_ADDOWNERS == p_UpdateType)
		{
			loadType = DlgAddItemsToGroups::SELECT_USERS_FOR_GROUPS;
			title			= YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_5, _YLOC("Add users as owners to %1 selected groups"), Str::getStringFromNumber(businessGroupRows.size()).c_str());
			explanation		= YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_6, _YLOC("These %1 groups will have the users you select below added as owners.\nTo see the complete list, use Load full directory. "), Str::getStringFromNumber(businessGroupRows.size()).c_str());
		}
		else if (BusinessObject::UPDATE_TYPE_ADDAUTHORACCEPTED == p_UpdateType)
		{
			ASSERT(GetAutomationName() == GridUtil::g_AutoNameGroupsAuthorsHierarchy);
			loadType = DlgAddItemsToGroups::SELECT_USERSGROUPSANDORGCONTACTS_FOR_GROUPS_SHOWEMAIL;
			title			= YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_1, _YLOC("Add accepted senders to %1 groups"), Str::getStringFromNumber(businessGroupRows.size()).c_str());
			explanation		= YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_2, _YLOC("These %1 groups will have the accepted senders which you select below added to them.\nTo see the complete list, use Load full directory. "), Str::getStringFromNumber(businessGroupRows.size()).c_str());
		}
		else if (BusinessObject::UPDATE_TYPE_ADDAUTHORREJECTED == p_UpdateType)
		{
			ASSERT(GetAutomationName() == GridUtil::g_AutoNameGroupsAuthorsHierarchy);
			loadType = DlgAddItemsToGroups::SELECT_USERSGROUPSANDORGCONTACTS_FOR_GROUPS_SHOWEMAIL;
			title			= YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_3, _YLOC("Add rejected senders to %1 groups"), Str::getStringFromNumber(businessGroupRows.size()).c_str());
			explanation		= YtriaTranslate::Do(GridGroupsCommon_showDlgItemsToGroup_4, _YLOC("These %1 groups will have the rejected senders which you select below added to them.\nTo see the complete list, use Load full directory. "), Str::getStringFromNumber(businessGroupRows.size()).c_str());
		}
		else
		{
			ASSERT(FALSE);
		}

		vector<BusinessUser>		users;
		vector<BusinessGroup>		groups;
		vector<BusinessOrgContact>	orgContacts;
		getUsersGroupsAndOrgContacts(
			users,
			true,
			groups,
			loadType != DlgAddItemsToGroups::SELECT_USERS_FOR_GROUPS,
			orgContacts,
			loadType == DlgAddItemsToGroups::SELECT_USERSGROUPSANDORGCONTACTS_FOR_GROUPS || loadType == DlgAddItemsToGroups::SELECT_USERSGROUPSANDORGCONTACTS_FOR_GROUPS_SHOWEMAIL);

		DlgAddItemsToGroups dlg( // DONE
			GetSession(),
			parentsForSelection,
			loadType,
			users,
			groups,
			orgContacts,
			title,
			explanation,
			hasOutOfScopePrivilege	? RoleDelegationUtil::RBAC_Privilege::RBAC_NONE
									: RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE,
			this);

		if (IDOK == dlg.DoModal())
		{
			GridBackendRow* rowToScrollTo = nullptr;
			bool update = false;

			if (nullptr != GetColId())
			{
				// List of all new users to add to selected groups.
				auto& newItems = dlg.GetSelectedItems();

				// Add new users.
				for (auto groupRow : businessGroupRows)
				{
					ASSERT(nullptr != groupRow);
					if (nullptr != groupRow)
					{
						const GridBackendField& fGroupGraphID = groupRow->GetField(GetColId());

						vector<GridBackendRow*> parentRows;
						if (BusinessObject::UPDATE_TYPE_ADDMEMBERS == p_UpdateType)
						{
							// If parent group appears twice or more in the grid, duplicate the added row!
							parentRows = GetRows(fGroupGraphID.GetValueStr(), GetColId()->GetID());
						}
						else
						{
							parentRows.push_back(groupRow);
						}

						vector<vector<GridBackendRow*>> addedRows;
						for (const auto& item : newItems)
						{
							if (addedRows.empty() || !addedRows.back().empty())
								addedRows.emplace_back();

							for (auto parentGroupRow : parentRows)
							{
								const GridBackendField& fGroupName = parentGroupRow->GetField(m_TemplateGroups.m_ColumnDisplayName);
								const GridBackendField& fGroupType = parentGroupRow->GetField(m_TemplateGroups.m_ColumnCombinedGroupType);

								// Recycle the row if already in.
								GridBackendRow* pURow = GetChildRow(parentGroupRow, item.GetID(), GetColId()->GetID());

								if (nullptr == pURow)
								{
									GridBackendField fieldId(item.GetID(), GetColId());

									// Meta ID is a multivalue with all the parents ids
									vector<PooledString> metaID;
									{
										auto* parent = parentGroupRow;
										while (nullptr != parent)
										{
											metaID.insert(metaID.begin(), parent->GetField(GetColId()).GetValueStr());
											parent = parent->GetParentRow();
										}
									}
									ASSERT(!metaID.empty());

									pURow = m_RowIndex.GetRow({ fieldId, m_TemplateUsers.GetMetaIDField(metaID) }, true, false);

									update = true;

									ASSERT(nullptr != pURow);
									if (nullptr != pURow)
									{
										pURow->SetParentRow(parentGroupRow);
										SetRowObjectType(pURow, item.GetObjectType());
										
										if (GridUtil::IsBusinessGroup(pURow))
										{ 
											for (auto c : m_MetaColumns)
											{
												if (parentGroupRow->HasField(c))
													pURow->AddField(parentGroupRow->GetField(c));
												else
													pURow->RemoveField(c);
											}

											if (nullptr != m_ColumnGroupNameForMember)
												pURow->AddField(item.GetPrincipalName(), m_ColumnGroupNameForMember);
											else
												pURow->AddField(item.GetPrincipalName(), m_TemplateGroups.m_ColumnDisplayName);

											if (nullptr != m_ColumnCombinedGroupTypeForMember)
												pURow->AddField(item.GetUserGroupType(), m_ColumnCombinedGroupTypeForMember);
											else if (!item.GetUserGroupType().IsEmpty())
												pURow->AddField(item.GetUserGroupType(), m_TemplateGroups.m_ColumnCombinedGroupType);

											if (nullptr != m_ColumnIsTeamForMember)
												GridUtil::SetRowIsTeam(item.GetGroupIsTeam(), pURow, m_ColumnIsTeamForMember, m_TemplateGroups.m_IconIsTeam);
											else
												GridUtil::SetRowIsTeam(item.GetGroupIsTeam(), pURow, m_TemplateGroups.m_ColumnIsTeam, m_TemplateGroups.m_IconIsTeam);
										}
										else if (GridUtil::IsBusinessUser(pURow))
										{
											for (auto c : m_MetaColumns)
											{
												if (parentGroupRow->HasField(c))
													pURow->AddField(parentGroupRow->GetField(c));
												else
													pURow->RemoveField(c);
											}

											pURow->AddField(item.GetPrincipalName(), m_TemplateUsers.m_ColumnUserPrincipalName);
											pURow->AddField(item.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
											if (nullptr != m_TemplateUsers.m_ColumnUserType)
												pURow->AddField(item.GetUserGroupType(), m_TemplateUsers.m_ColumnUserType);

											if (BusinessObject::UPDATE_TYPE_ADDMEMBERS == p_UpdateType)
											{
												if (nullptr != m_ColumnGroupNameForMember)
												{
													const GridBackendField& field = groupRow->GetField(m_ColumnGroupNameForMember);
													if (field.HasValue())
														pURow->AddField(field.GetValueStr(), m_ColumnGroupNameForMember);
												}

												if (nullptr != m_ColumnCombinedGroupTypeForMember)
												{
													const GridBackendField& field = groupRow->GetField(m_ColumnCombinedGroupTypeForMember);
													if (field.HasValue())
														pURow->AddField(field.GetValueStr(), m_ColumnCombinedGroupTypeForMember);
												}
											}
										}
										else if (GridUtil::IsBusinessOrgContact(pURow))
										{
											ASSERT(BusinessObject::UPDATE_TYPE_ADDMEMBERS			== p_UpdateType
												|| BusinessObject::UPDATE_TYPE_ADDAUTHORACCEPTED	== p_UpdateType
												|| BusinessObject::UPDATE_TYPE_ADDAUTHORREJECTED	== p_UpdateType);

											for (auto c : m_MetaColumns)
											{
												if (parentGroupRow->HasField(c))
													pURow->AddField(parentGroupRow->GetField(c));
												else
													pURow->RemoveField(c);
											}

											pURow->AddField(item.GetPrincipalName(), m_TemplateUsers.m_ColumnUserPrincipalName);
											pURow->AddField(item.GetDisplayName(), m_TemplateUsers.m_ColumnDisplayName);
										}
										else
										{
											ASSERT(false);
										}

										pURow->SetLParam(p_UpdateType);
										addFieldSpecific(pURow, p_UpdateType);

										addedRows.back().push_back(pURow);
									}
								}
								else
								{
									if (pURow->IsDeleted())
									{
										row_pk_t rowPk;
										GetRowPK(pURow, rowPk);
										update = GetModifications().Revert(rowPk, false);
									}
								}
							}
						}

						if (nullptr == rowToScrollTo && !addedRows.empty() && !addedRows[0].empty())
						{
							OnUnSelectAll();
							rowToScrollTo = addedRows[0][0];
						}

						for (auto& rows : addedRows)
						{
							CreatedObjectModification* mainMod = nullptr;
							for (auto& row : rows)
							{
								row_pk_t rowPk;
								GetRowPK(row, rowPk);
								if (nullptr == mainMod)
								{
									mainMod = GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPk));
								}
								else
								{
									ASSERT(BusinessObject::UPDATE_TYPE_ADDMEMBERS == p_UpdateType);
									mainMod->AddSiblingModification(std::make_unique<CreatedObjectModification>(*this, rowPk));
								}
								SelectRowAndExpandParentGroups(row, false);
							}
						}
					}
				}

				if (update)
					UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

				if (nullptr != rowToScrollTo)
					ScrollToRow(rowToScrollTo);
			}
		}
	}
}

void GridGroupsCommon::removeItemsFromGroup(BusinessObject::UPDATE_TYPE p_UpdateType, const set<GridBackendRow*>& p_Rows, bool p_Update, bool p_DoNotUnSelectAll)
{
	ASSERT(GetAutomationName() != GridUtil::g_AutoNameGroupsHierarchy); // No used anymore for that grid
	ASSERT(BusinessObject::UPDATE_TYPE_DELETEMEMBERS		== p_UpdateType
		|| BusinessObject::UPDATE_TYPE_DELETEOWNERS			== p_UpdateType
		|| BusinessObject::UPDATE_TYPE_DELETEAUTHORIZATION	== p_UpdateType);

	const auto privilege = getPrivilege(p_UpdateType);
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(getOutOfScopePrivilege(p_UpdateType));
	bool update = false;

	map<row_pk_t, vector<GridBackendRow*>> rowsToRevert;
	std::set<GridBackendRow*> processedRows;
	std::set<GridBackendRow*> modifiedRows;
	bool cancelAll = false;
	for (auto row : p_Rows)
	{
		ASSERT(nullptr != row);

		if (nullptr != row && !row->IsGroupRow() && nullptr != row->GetParentRow())
		{
			auto selectedRow = row->GetAncestorAtLevel(1);

			if (IsGroupAuthorizedByRoleDelegation(selectedRow->GetParentRow(), privilege))
			{
				const bool confirm = selectedRow != row;

				vector<GridBackendRow*> siblingRows;
				if (BusinessObject::UPDATE_TYPE_DELETEMEMBERS == p_UpdateType)
				{
					// If item row (same ID / same parent ID) appears twice or more in the grid, mark all of them as deleted!
					siblingRows = GetChildRows(selectedRow->GetParentRow()->GetField(GetColId()).GetValueStr(), GetColId()->GetID(), selectedRow->GetField(GetColId()).GetValueStr(), GetColId()->GetID());
				}
				else
				{
					siblingRows.push_back(selectedRow);
				}

				DeletedObjectModification* mainMod = nullptr;
				vector<GridBackendRow*>* currentRevertList = nullptr;
				for (auto siblingRow : siblingRows)
				{
					if (processedRows.end() == processedRows.find(siblingRow))
					{
						ASSERT(GridUtil::IsBusinessUser(siblingRow) || GridUtil::IsBusinessGroup(siblingRow) || GridUtil::IsBusinessOrgContact(siblingRow));

						if (hasOutOfScopePrivilege || GridUtil::IsBusinessOrgContact(siblingRow)
							|| IsUserAuthorizedByRoleDelegation(siblingRow, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)
							|| IsGroupAuthorizedByRoleDelegation(siblingRow, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE))
						{
							row_pk_t siblingRowPk;
							GetRowPK(siblingRow, siblingRowPk);

							if (!siblingRow->IsDeleted())
							{
								ASSERT(nullptr == GetModifications().GetRowModification<DeletedObjectModification>(siblingRowPk));
								bool processEntity = true;
								// warn user - only once
								if (confirm)
								{
									ASSERT(BusinessObject::UPDATE_TYPE_DELETEAUTHORIZATION == p_UpdateType
										|| BusinessObject::UPDATE_TYPE_DELETEMEMBERS == p_UpdateType);

									if (BusinessObject::UPDATE_TYPE_DELETEAUTHORIZATION == p_UpdateType
										|| BusinessObject::UPDATE_TYPE_DELETEMEMBERS == p_UpdateType)
									{
										wstring title;
										wstring message;

										if (BusinessObject::UPDATE_TYPE_DELETEAUTHORIZATION == p_UpdateType)
										{
											title = YtriaTranslate::Do(GridGroupsCommon_removeItemsFromGroup_1, _YLOC("Delete Restriction")).c_str();
											message = YtriaTranslate::Do(GridGroupsCommon_removeItemsFromGroup_5, _YLOC("Restrictions will be deleted for group: %1"), siblingRow->GetField(m_ColumnGroupNameForMember).GetValueStr());
										}
										else
										{
											title = YtriaTranslate::Do(YTriaGridGE_OnCustomPopupMenu_3, _YLOC("Remove Member")).c_str();
											message = YtriaTranslate::Do(GridGroupsCommon_removeItemsFromGroup_7, _YLOC("'%1' will be removed from '%2'"), siblingRow->GetField(m_ColumnGroupNameForMember).GetValueStr(), siblingRow->GetField(m_TemplateGroups.m_ColumnDisplayName).GetValueStr());
										}

										{
											YCodeJockMessageBox confirmDlg(this
												, DlgMessageBox::eIcon_Information
												, title
												, message
												, _YTEXT("")
												, { { IDOK, YtriaTranslate::Do(GridGroupsCommon_removeItemsFromGroup_2, _YLOC("OK")).c_str() }
													,	{ IDCANCEL, YtriaTranslate::Do(GridGroupsCommon_removeItemsFromGroup_3, _YLOC("Cancel")).c_str() }
													, { IDIGNORE, YtriaTranslate::Do(GridGroupsCommon_removeItemsFromGroup_4, _YLOC("Skip")).c_str() } }
											, IDOK);

											const auto res = confirmDlg.DoModal();
											processEntity = IDOK == res;
											if (res == IDCANCEL)
												cancelAll = true;
										}
									}
								}

								if (processEntity)
								{
									if (row->IsCreated())
									{
										ASSERT(nullptr == mainMod);
										if (nullptr == currentRevertList)
											currentRevertList = &rowsToRevert[siblingRowPk];

										currentRevertList->push_back(siblingRow);
									}
									else
									{
										ASSERT(nullptr == currentRevertList);
										if (nullptr == mainMod)
										{
											mainMod = GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, siblingRowPk));
										}
										else
										{
											ASSERT(BusinessObject::UPDATE_TYPE_DELETEMEMBERS == p_UpdateType);
											mainMod->AddSiblingModification(std::make_unique<DeletedObjectModification>(*this, siblingRowPk));
										}
										addFieldSpecific(siblingRow, p_UpdateType);
										update = true;
									}

									modifiedRows.insert(siblingRow);
								}
							}
							else
							{
								ASSERT(nullptr != GetModifications().GetRowModification<DeletedObjectModification>(siblingRowPk));
							}
						}

						processedRows.insert(siblingRow);
					}
				}
			}
		}

		if (cancelAll)
			break;
	}

	if (!modifiedRows.empty())
	{
		if (!p_DoNotUnSelectAll)
			OnUnSelectAll();
		for (auto row : modifiedRows)
			row->SetSelected(true);
	}

	for (auto item : rowsToRevert)
	{
		for (auto& row : item.second)
			row->SetLParam(0);
		if (GetModifications().Revert(item.first, false))
		{
			update = true;
		}
		else
		{
			ASSERT(false);
		}
	}

	if (update && p_Update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

	//AutomationGreenlight();
}

void GridGroupsCommon::removeSelectedItemsFromGroup(BusinessObject::UPDATE_TYPE p_UpdateType)
{
	removeItemsFromGroup(p_UpdateType, GetSelectedRows(), true, false);
}

void GridGroupsCommon::getUsersGroupsAndOrgContacts(vector<BusinessUser>& p_Users, bool fillUsers
													, vector<BusinessGroup>& p_Groups, bool fillGroups
													, vector<BusinessOrgContact>& p_OrgContacts, bool fillOrgContacts)
{
	ASSERT(fillUsers || fillGroups || fillOrgContacts);

	CWaitCursor _;
	for (auto& row : GetBackendRows())
	{
		if (!row->IsExplosionAdded())
		{
			if (fillUsers && GridUtil::IsBusinessUser(row))
			{
				BusinessUser user;
				m_TemplateUsers.GetBusinessUser(*this, row, user);

				if (p_Users.end() == std::find_if(p_Users.begin(), p_Users.end(), [&user](const BusinessUser& bu) { return bu.GetID() == user.GetID(); }))
					p_Users.emplace_back(user);
			}
			else if (fillGroups && GridUtil::IsBusinessGroup(row))
			{
				BusinessGroup group;
				m_TemplateGroups.GetBusinessGroup(*this, row, group);

				if (nullptr != m_ColumnCombinedGroupTypeForMember && row->GetField(m_ColumnCombinedGroupTypeForMember).HasValue())
					group.SetCombinedGroupType(PooledString(row->GetField(m_ColumnCombinedGroupTypeForMember).GetValueStr()));

				if (nullptr != m_ColumnGroupNameForMember && row->GetField(m_ColumnGroupNameForMember).HasValue())
					group.SetDisplayName(PooledString(row->GetField(m_ColumnGroupNameForMember).GetValueStr()));

				if (p_Groups.end() == std::find_if(p_Groups.begin(), p_Groups.end(), [&group](const BusinessGroup& bg) { return bg.GetID() == group.GetID(); }))
					p_Groups.emplace_back(group);
			}
			else if (fillOrgContacts && GridUtil::IsBusinessOrgContact(row))
			{
				BusinessOrgContact orgContact;
				{
					auto& idField = row->GetField(GetColId());
					if (idField.HasValue())
						orgContact.SetID(idField.GetValueStr());
				}
				{
					auto& displayNameField = row->GetField(m_TemplateUsers.m_ColumnDisplayName);
					if (displayNameField.HasValue())
						orgContact.SetDisplayName(PooledString(displayNameField.GetValueStr()));
				}
				{
					auto& mailField = row->GetField(m_TemplateUsers.m_ColumnUserPrincipalName);
					if (mailField.HasValue())
						orgContact.SetMail(PooledString(mailField.GetValueStr()));
				}

				// Do we need more info here?

				if (p_OrgContacts.end() == std::find_if(p_OrgContacts.begin(), p_OrgContacts.end(), [&orgContact](const BusinessOrgContact& boc) { return boc.GetID() == orgContact.GetID(); }))
					p_OrgContacts.emplace_back(orgContact);
			}
		}
	}
}

RoleDelegationUtil::RBAC_Privilege GridGroupsCommon::getPrivilege(BusinessObject::UPDATE_TYPE p_UpdateType)
{
	RoleDelegationUtil::RBAC_Privilege privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;
	switch (p_UpdateType)
	{
	case BusinessObject::UPDATE_TYPE_ADDMEMBERS:
	case BusinessObject::UPDATE_TYPE_DELETEMEMBERS:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT;
		break;

	case BusinessObject::UPDATE_TYPE_ADDOWNERS:
	case BusinessObject::UPDATE_TYPE_DELETEOWNERS:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT;
		break;

	case BusinessObject::UPDATE_TYPE_ADDAUTHORACCEPTED:
	case BusinessObject::UPDATE_TYPE_ADDAUTHORREJECTED:
	case BusinessObject::UPDATE_TYPE_DELETEACCEPTEDAUTHORS:
	case BusinessObject::UPDATE_TYPE_DELETEREJECTEDAUTHORS:
	case BusinessObject::UPDATE_TYPE_DELETEAUTHORIZATION:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_DELIVERYMANAGEMENT_EDIT;
		break;

	default:
		ASSERT(false);
		break;
	}

	return privilege;
}

RoleDelegationUtil::RBAC_Privilege GridGroupsCommon::getOutOfScopePrivilege(BusinessObject::UPDATE_TYPE p_UpdateType)
{
	RoleDelegationUtil::RBAC_Privilege privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;
	switch (p_UpdateType)
	{
	case BusinessObject::UPDATE_TYPE_ADDMEMBERS:
	case BusinessObject::UPDATE_TYPE_DELETEMEMBERS:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE;
		break;

	case BusinessObject::UPDATE_TYPE_ADDOWNERS:
	case BusinessObject::UPDATE_TYPE_DELETEOWNERS:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE;
		break;

	case BusinessObject::UPDATE_TYPE_ADDAUTHORACCEPTED:
	case BusinessObject::UPDATE_TYPE_ADDAUTHORREJECTED:
	case BusinessObject::UPDATE_TYPE_DELETEACCEPTEDAUTHORS:
	case BusinessObject::UPDATE_TYPE_DELETEREJECTEDAUTHORS:
	case BusinessObject::UPDATE_TYPE_DELETEAUTHORIZATION:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_DELIVERYMANAGEMENT_EDIT_OUT_OF_SCOPE;
		break;

	default:
		ASSERT(false);
		break;
	}

	return privilege;
}

void GridGroupsCommon::addFieldSpecific(GridBackendRow* p_pRow, BusinessObject::UPDATE_TYPE p_UpdateType)
{
	ASSERT(GetAutomationName() != GridUtil::g_AutoNameGroupsHierarchy); // No used anymore for that grid
	if (BusinessObject::UPDATE_TYPE_DELETEMEMBERS == p_UpdateType || BusinessObject::UPDATE_TYPE_DELETEOWNERS == p_UpdateType)
		p_pRow->SetLParam(p_UpdateType);
}

bool GridGroupsCommon::isAddEnabledForSelection() const
{
	for (auto row : GetSelectedRows())
	{
		ASSERT(nullptr != row);
		if (nullptr != row && !row->IsGroupRow())
		{
			GridBackendRow* groupRow = GridUtil::isBusinessType<BusinessGroup>(row) ? row : row->GetParentRow();
			ASSERT(nullptr != groupRow && GridUtil::isBusinessType<BusinessGroup>(groupRow));
			if (nullptr != groupRow)
				return true;
		}
	}

	return false;
}
