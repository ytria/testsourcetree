#pragma once

#include "GridFrameBase.h"
#include "GridGroups.h"
#include "BusinessGroupSettingTemplate.h"
#include "BusinessGroupSetting.h"

class FrameGroups : public GridFrameBase
{
public:
    DECLARE_DYNAMIC(FrameGroups)
	FrameGroups(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
    virtual ~FrameGroups();

	void ShowGroups(const vector<BusinessGroup>& p_Groups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_AllowFullPurge);
	void ShowGroups(const vector<BusinessGroup>& p_Groups, const vector<BusinessGroup>& p_LoadedMoreGroups, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_AllowFullPurge);
	void UpdateGroupLCPStatus(const vector<BusinessGroup>& p_Groups);
	void RefreshGroupLCPStatus(const BusinessLifeCyclePolicies& p_LCP, bool p_RefreshGrid);
	void UpdateGroupsLoadMore(const vector<BusinessGroup>& p_Groups, bool p_FromRefresh);
	void ShowLifecyclePolicies(bool p_RefreshGrid);
	void SetOnPremiseGroupsUpdateResult(const vector<O365UpdateOperation>& p_UpdateResults);

	virtual O365Grid& GetGrid() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction);
	virtual void ApplyAllSpecific() override;

    void ApplySpecific(bool p_Selected);
	void ShowOnPremiseGroups(const vector<OnPremiseGroup>& p_Groups);

    virtual void ApplySelectedSpecific() override;

	void ShowRecycleBin();

	bool IsThereLCP();
	bool IsThereLCPError();
	bool IsLCPTypeSelected();

    const boost::YOpt<std::vector<BusinessGroupSettingTemplate>>& GetGroupSettingTemplates() const;
    void SetGroupSettingTemplates(const boost::YOpt<std::vector<BusinessGroupSettingTemplate>>& p_Val);

    const boost::YOpt<std::vector<BusinessGroupSetting>>& GetTenantGroupSettings() const;
    void SetTenantGroupSettings(const boost::YOpt<std::vector<BusinessGroupSetting>>& p_Val, bool p_UpdateGrid);

    void ShowTenantSettings();

	bool HasDeltaFeature() const override;

protected:
    
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPolicyCreate();
	afx_msg void OnUpdatePolicyCreate(CCmdUI* pCmdUI);
	afx_msg void OnPolicyUpdate();
	afx_msg void OnUpdatePolicyUpdate(CCmdUI* pCmdUI);
	afx_msg void OnPolicyDelete();
	afx_msg void OnUpdatePolicyDelete(CCmdUI* pCmdUI);
	afx_msg void OnShowTenantSettings();
	afx_msg void OnUpdateShowTenantSettings(CCmdUI* pCmdUI);

	/*afx_msg void OnPolicyPerGroupStatus();
	afx_msg void OnUpdatePolicyPerGroupStatus(CCmdUI* pCmdUI);*/
	afx_msg void OnPolicyPerGroupRenew();
	afx_msg void OnUpdatePolicyPerGroupRenew(CCmdUI* pCmdUI);
	afx_msg void OnPolicyPerGroupAddGroup();
	afx_msg void OnUpdatePolicyPerGroupAddGroup(CCmdUI* pCmdUI);
	afx_msg void OnPolicyPerGroupRemoveGroup();
	afx_msg void OnUpdatePolicyPerGroupRemoveGroup(CCmdUI* pCmdUI);

	/*afx_msg void OnApplyAllOnPrem();
	afx_msg void OnUpdateApplyAllOnPrem(CCmdUI* pCmdUI);

	afx_msg void OnApplySelectedOnPrem();
	afx_msg void OnUpdateApplySelectedOnPrem(CCmdUI* pCmdUI);

	afx_msg void OnRevertAllOnPrem();
	afx_msg void OnUpdateRevertAllOnPrem(CCmdUI* pCmdUI);

	afx_msg void OnRevertSelectedOnPrem();
	afx_msg void OnUpdateRevertSelectedOnPrem(CCmdUI* pCmdUI);*/

	afx_msg void OnForceSyncOnPrem();
	afx_msg void OnUpdateForceSyncOnPrem(CCmdUI* pCmdUI);

	void applyOnPremSpecific(bool p_SelectednOnly);

	virtual void createGrid() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;
	virtual void createFrameSpecificTabs() override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

	virtual ApplyConfirmationConfig GetApplyConfirmationConfig(bool p_ApplySelected) const override;

	bool hasLoseableModificationsPending(bool inSelectionOnly = false) override; // for refresh, close, logout
	bool hasOnPremiseChanges(bool inSelectionOnly = false);

private:
	GridGroups m_groupsGrid;
	CXTPControl* m_pLCPStatus = nullptr;
	CXTPControl* m_pLCPLifetime = nullptr;
	CXTPControl* m_pLCPNotification = nullptr;
	CXTPControlBitmap* m_PolicyIconControl = nullptr;
	CXTPControl* m_GroupSettingsUnifiedStatus = nullptr;
	CXTPControl* m_GroupSettingsAddToGuestStatus = nullptr;

	const UINT m_GroupPolicyIconID = initGroupPolicyIconID();

	static UINT initGroupPolicyIconID();

    // Other data
    boost::YOpt<std::vector<BusinessGroupSettingTemplate>> m_GroupSettingTemplates;
    boost::YOpt<std::vector<BusinessGroupSetting>> m_TenantGroupSettings;

	template<class T> friend class OnPremModificationsApplyer;
	template<class T> friend class OnPremSaveSelectedButtonUpdater;
	template<class T> friend class OnPremSaveAllButtonUpdater;
	template<class T> friend class OnPremRevertSelectedButtonUpdater;
	template<class T> friend class OnPremRevertAllButtonUpdater;
};

