#include "ModuleGroup.h"

#include "AutomationDataStructure.h"
#include "BaseO365Grid.h"
#include "BasicPageRequestLogger.h"
#include "BasicRequestLogger.h"
#include "BusinessGroup.h"
#include "BusinessObjectManager.h"
#include "CommandDispatcher.h"
#include "CommandInfo.h"
#include "DlgBusinessGroupSettingsEditHTML.h"
#include "DlgGroupsModuleOptions.h"
#include "DlgUserCacheDataHTML.h"
#include "FrameGroups.h"
#include "FrameGroupsRecycleBin.h"
#include "FrameGroupAuthorsHierarchy.h"
#include "FrameDriveItems.h"
#include "FrameGroupMembersHierarchy.h"
#include "FrameGroupOwnersHierarchy.h"
#include "GraphCache.h"
#include "GridGroupsHierarchy.h"
#include "GridGroupsRecycleBin.h"
#include "GroupListFullPropertySet.h"
#include "MSGraphSession.h"
#include "O365UpdateOperation.h"
#include "Office365Admin.h"
#include "OnPremiseGroupUpdateRequester.h"
#include "RunOnScopeEnd.h"
#include "Sapio365Settings.h"
#include "TaskDataManager.h"
#include "UpdatedObjectsGenerator.h"
#include "YCallbackMessage.h"
#include "YDataCallbackMessage.h"
#include "YSafeCreateTask.h"
#include "ShowOnPremGroupsCommand.h"

const BusinessLifeCyclePolicies& ModuleGroup::GetGroupLifeCyclePolicies() const
{
	return m_GroupLifeCyclePolicies;
}

void ModuleGroup::SetGroupLifeCyclePolicies(const BusinessLifeCyclePolicies& p_LCP)
{
	m_GroupLifeCyclePolicies = p_LCP;
}

void ModuleGroup::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
		case Command::ModuleTask::List:
        case Command::ModuleTask::ListDetails:
		case Command::ModuleTask::ListWithPrefilter:
			show(p_Command);
			break;
		case Command::ModuleTask::ListSubItems:
			showItems<FrameGroupMembersHierarchy>(p_Command);
			break;
		case Command::ModuleTask::ListOwners:
			showItems<FrameGroupOwnersHierarchy>(p_Command);
			break;
		case Command::ModuleTask::ListAuthors:
			showItems<FrameGroupAuthorsHierarchy>(p_Command);
			break;
		case Command::ModuleTask::ListRecycleBin:
			showRecycleBin(p_Command);
			break;
        case Command::ModuleTask::UpdateMoreInfo:
            more(p_Command);
            break;
		case Command::ModuleTask::UpdateGroupItems:
			updateGroupItems(p_Command);
			break;
		case Command::ModuleTask::UpdateModified:
			update(p_Command, YtriaTranslate::Do(ModuleGroup_executeImpl_1, _YLOC("Update Groups")).c_str());
			break;
		case Command::ModuleTask::UpdateRecycleBin:
			update(p_Command, YtriaTranslate::Do(ModuleGroup_executeImpl_2, _YLOC("Update Groups Recycle Bin")).c_str());
			break;
		case Command::ModuleTask::UpdateLCP:
			updateLCP(p_Command);
			break;
        case Command::ModuleTask::UpdateSettings:
            updateSettings(p_Command);
            break;
		case Command::ModuleTask::ListLCPGroup:
			showGroupLCPStatus(p_Command);
			break;
		case Command::ModuleTask::UpdateLCPGroup:
			updateLCPGroup(p_Command);
			break;
		case Command::ModuleTask::LoadSnapshot:
			loadSnapshot(p_Command);
			break;
		case Command::ModuleTask::ApplyOnPremChanges:
			saveOnPrem(p_Command);
			break;
		default:
            ASSERT(false);
			SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
			break;
	}
}

void ModuleGroup::show(Command p_Command)
{
	auto		p_SourceWindow	= p_Command.GetSourceWindow();
	auto		p_Action		= p_Command.GetAutomationAction();

    auto& info	= p_Command.GetCommandInfo();
    FrameGroups* frame	= dynamic_cast<FrameGroups*>(info.GetFrame());

	const bool isRefresh = nullptr != frame;
	bool askAutoLoadOnPrem = false;
    if (nullptr == frame)
    {
        if (!FrameGroups::CanCreateNewFrame(true, p_SourceWindow.GetCWnd(), p_Action, PreAllocatedIDS::HasFreeUserPictureID() && PreAllocatedIDS::HasFreeGroupPolicyIconID()))
            return;

        ModuleCriteria moduleCriteria;
		moduleCriteria.m_Origin			= Command::ModuleTask::ListDetails == p_Command.GetTask() ? Origin::Group : Origin::Tenant;
        moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::IDS;
		moduleCriteria.m_Privilege		= info.GetRBACPrivilege();
		if (Command::ModuleTask::ListDetails == p_Command.GetTask())
			moduleCriteria.m_IDs		= info.GetIds();

		ASSERT(moduleCriteria.m_IDs.end() == std::find_if(moduleCriteria.m_IDs.begin(), moduleCriteria.m_IDs.end(), [](const PooledString& id) {return O365Grid::IsTemporaryCreatedObjectID(id); }));

		if (p_Command.GetTask() == Command::ModuleTask::ListWithPrefilter)
		{
			DlgGroupsModuleOptions opt(p_SourceWindow.GetCWnd(), Sapio365Session::Find(p_Command.GetSessionIdentifier()));
			if (IDCANCEL == opt.DoModal())
			{
				if (nullptr != p_Action)
					SetActionCanceledByUser(p_Action);
				return;
			}
			ASSERT(!p_Command.GetCommandInfo().Data2().is_valid());
			p_Command.GetCommandInfo().Data2() = opt.GetOptions();
		}
		else
			askAutoLoadOnPrem = true;

		const bool shouldCreate = p_Command.GetCommandInfo().Data2()
			? ShouldCreateFrame<FrameGroups>(moduleCriteria, p_SourceWindow, p_Action, p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>())
			: ShouldCreateFrame<FrameGroups>(moduleCriteria, p_SourceWindow, p_Action);

		if (shouldCreate && p_SourceWindow.UserConfirmation(p_Action))
        {
			CWaitCursor _;

            frame = new FrameGroups(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleGroup_show_1, _YLOC("Groups")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
			if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
				frame->SetOptions(p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>());
			frame->InitModuleCriteria(moduleCriteria);
            AddGridFrame(frame);
            frame->Erect();
        }
    }

	// No assert: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
	//ASSERT(nullptr != frame);

    if (nullptr != frame)
    {
		if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
			frame->SetOptions(p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>());

		const bool syncNow = frame->GetAndResetSyncNow();

		// FIXME: For group details, we should probably have a separate method that prevent from loading a whole cache.
		// It should at least use the existing cache (delta-synced) but do separate requests if cache is not init...
		// At best, it should init a 'local' delta with id selection and keep it within the frame.
		loadGroups(p_Command, info, frame, isRefresh, syncNow);
    }
	else
	{
		ASSERT(nullptr == p_Action);
	}
}

template <class FrameClass>
void ModuleGroup::showItems(const Command& p_Command)
{
	auto action = p_Command.GetAutomationAction();

	const auto& info = p_Command.GetCommandInfo();
	auto sourceWindow = p_Command.GetSourceWindow();

	FrameClass* frame = dynamic_cast<FrameClass*>(info.GetFrame());
	const bool isRefresh = nullptr != frame;
	if (nullptr == frame)
	{
		if (FrameClass::CanCreateNewFrame(true, sourceWindow.GetCWnd(), action))
		{
			ModuleCriteria moduleCriteria;
			ASSERT(Origin::Group == info.GetOrigin());
			moduleCriteria.m_Origin				= Origin::Group;
			moduleCriteria.m_UsedContainer		= ModuleCriteria::UsedContainer::IDS;
			moduleCriteria.m_IDs				= info.GetIds();
			moduleCriteria.m_Privilege			= info.GetRBACPrivilege();
			moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

			if (ShouldCreateFrame<FrameClass>(moduleCriteria, sourceWindow, action) && sourceWindow.UserConfirmation(action))
			{
				CWaitCursor _;

				wstring windowTitle;

				if (Command::ModuleTask::ListSubItems == p_Command.GetTask())
				{
					windowTitle = YtriaTranslate::Do(ModuleGroup_showItems_1, _YLOC("Group Members")).c_str();
				}
				else if (Command::ModuleTask::ListAuthors == p_Command.GetTask())
				{
					windowTitle = YtriaTranslate::Do(ModuleGroup_showItems_2, _YLOC("Group Delivery Management")).c_str();
				}
				else if (Command::ModuleTask::ListOwners == p_Command.GetTask())
				{
					windowTitle = YtriaTranslate::Do(ModuleGroup_showItems_3, _YLOC("Group Owners")).c_str();
				}

				frame = new FrameClass(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), windowTitle, *this, sourceWindow.GetHistoryMode(), sourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
			else
			{
				// No error: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
				SetAutomationGreenLight(action/*, _YTEXT("")*/);
				return;
			}
		}
	}

	if (nullptr == frame)
	{
		SetAutomationGreenLight(action, _YTEXT(""));
		return;
	}

	YtriaTaskData taskData;

	ASSERT(Origin::Group == info.GetOrigin());
	TaskWrapper<vector<BusinessGroup>> task;

    RefreshSpecificData refreshSpecificData;
    if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
        refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

	const auto& ids = refreshSpecificData.m_Criteria.m_IDs.empty() ? info.GetIds() : refreshSpecificData.m_Criteria.m_IDs;
	
	auto bom = frame->GetBusinessObjectManager();
	if (Command::ModuleTask::ListSubItems == p_Command.GetTask())
	{
		taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_showItems_4, _YLOC("Show Group Memberships...")).c_str(), frame, p_Command, !isRefresh);
        if (refreshSpecificData.m_Criteria.m_IDs.empty() && ids.empty())
			task = pplx::task_from_result(vector<BusinessGroup>());
		else
			task = bom->GetBusinessGroupMembersHierarchies(ids, (bool)frame->GetMetaDataColumnInfo(), taskData, true);
	}
	else if (Command::ModuleTask::ListAuthors == p_Command.GetTask())
	{
		taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_showItems_5, _YLOC("Show Group Delivery Management...")).c_str(), frame, p_Command, !isRefresh);
		if (refreshSpecificData.m_Criteria.m_IDs.empty() && ids.empty())
			task = pplx::task_from_result(vector<BusinessGroup>());
		else
			task = bom->GetBusinessGroupAuthorsHierarchies(ids, (bool)frame->GetMetaDataColumnInfo(), taskData);
	}
	else if (Command::ModuleTask::ListOwners == p_Command.GetTask())
	{
		taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_showItems_6, _YLOC("Show Group Owners...")).c_str(), frame, p_Command, !isRefresh);
		if (refreshSpecificData.m_Criteria.m_IDs.empty() && ids.empty())
			task = pplx::task_from_result(vector<BusinessGroup>());
		else
			task = bom->GetBusinessGroupOwnersHierarchies(ids, (bool)frame->GetMetaDataColumnInfo(), taskData, false);
	}
	else
	{
		ASSERT(false);
	}
			
	task.ThenByTask([hwnd = frame->GetSafeHwnd(), refreshSpecificData, taskData, action, ids, bom, isRefresh, taskId = p_Command.GetTask()](pplx::task<vector<BusinessGroup>> p_PreviousTask)
	{
		auto businessGroups = p_PreviousTask.get();

		if (taskData.IsCanceled())
		{
			// Add missing objects cause of cancellation
			AddCanceledObjects(businessGroups, ids, bom->GetGraphCache());
		}

		YDataCallbackMessage<vector<BusinessGroup>>::DoPost(businessGroups, [hwnd, refreshSpecificData, taskData, action, isCanceled = taskData.IsCanceled(), isRefresh, taskId](vector<BusinessGroup> p_Groups)
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameClass*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(p_Groups)))
			{
				ASSERT(nullptr != frame);
				if (Command::ModuleTask::ListSubItems == taskId)
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting members for %s groups...", Str::getStringFromNumber(p_Groups.size()).c_str()));
				else if (Command::ModuleTask::ListAuthors == taskId)
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting authors for %s groups...", Str::getStringFromNumber(p_Groups.size()).c_str()));
				else if (Command::ModuleTask::ListOwners == taskId)
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting owners for %s groups...", Str::getStringFromNumber(p_Groups.size()).c_str()));

				frame->GetGrid().ClearLog(taskData.GetId());
				//frame->GetGrid().ClearStatus();

				{
					CWaitCursor _;
					if (frame->HasLastUpdateOperations())
					{
						frame->ShowHierarchies(p_Groups, frame->GetLastUpdateOperations(), false);
						frame->ForgetLastUpdateOperations();
					}
					else
					{
						frame->ShowHierarchies(p_Groups, {}, refreshSpecificData.m_Criteria.m_IDs.empty());
					}
				}

				if (!isRefresh)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, taskData, action);
			}
		});
	});
}

void ModuleGroup::showRecycleBin(const Command& p_Command)
{
	auto				p_SourceWindow	= p_Command.GetSourceWindow();
	AutomationAction*	p_Action		= p_Command.GetAutomationAction();

	CommandInfo				info		= p_Command.GetCommandInfo();
	FrameGroupsRecycleBin*	frame		= dynamic_cast<FrameGroupsRecycleBin*>(info.GetFrame());
	const auto&				p_GroupIDs	= info.GetIds();

	const bool isRefresh = nullptr != frame;
	if (nullptr == frame)
	{
		auto sourceCWnd = p_SourceWindow.GetCWnd();
		if (!FrameGroupsRecycleBin::CanCreateNewFrame(true, p_SourceWindow.GetCWnd(), p_Action))
			return;

		ModuleCriteria moduleCriteria;
		moduleCriteria.m_Origin			= Origin::Group;
		moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::IDS;
		moduleCriteria.m_IDs			= p_GroupIDs;
		moduleCriteria.m_Privilege		= info.GetRBACPrivilege();
		if (ShouldCreateFrame<FrameGroupsRecycleBin>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
		{
			CWaitCursor _;
			frame = new FrameGroupsRecycleBin(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleGroup_showRecycleBin_1, _YLOC("Groups: Recycle Bin")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
			frame->InitModuleCriteria(moduleCriteria);
			AddGridFrame(frame);
			frame->Erect();
		}
	}

	if (nullptr != frame)
	{
		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_showRecycleBin_2, _YLOC("Show Groups Recycle Bin...")).c_str(), frame, p_Command, !isRefresh);

		auto grid = dynamic_cast<GridGroupsRecycleBin*>(&frame->GetGrid());
		std::vector<GridBackendRow*> rowsWithMoreLoaded;
		grid->GetRowsWithMoreLoaded(rowsWithMoreLoaded);

		std::vector<BusinessGroup> groupsLoadMoreRequestInfo;
		if (!rowsWithMoreLoaded.empty())
			groupsLoadMoreRequestInfo = grid->GetLoadMoreRequestInfo(rowsWithMoreLoaded);

		auto bom = frame->GetBusinessObjectManager();
		bom->GetBusinessGroupsRecycleBin(DeletedGroupListFullPropertySet(), taskData).ThenByTaskMutable([hwnd = frame->GetSafeHwnd(), taskData, p_Action, isRefresh, groupsLoadMoreRequestInfo, bom](pplx::task<vector<BusinessGroup>> p_PreviousTask) mutable
		{
			auto businessGroups = p_PreviousTask.get();
			vector<BusinessGroup> moreLoadedGroups;

			if (!groupsLoadMoreRequestInfo.empty())
			{
				ASSERT(isRefresh);
				for (auto it = groupsLoadMoreRequestInfo.begin(); it != groupsLoadMoreRequestInfo.end();)
				{
					auto it2 = std::find_if(businessGroups.begin(), businessGroups.end(), [&it](auto& bg) {return it->GetID() == bg.GetID(); });
					if (it2 != businessGroups.end())
					{
						businessGroups.erase(it2);
						++it;
					}
					else
					{
						it = groupsLoadMoreRequestInfo.erase(it);
					}
				}

				moreLoadedGroups = bom->MoreInfoDeletedBusinessGroups(groupsLoadMoreRequestInfo, false, taskData).GetTask().get();
				std::copy(std::begin(moreLoadedGroups), std::end(moreLoadedGroups), std::back_inserter(businessGroups));
			}

			YDataCallbackMessage<vector<BusinessGroup>>::DoPost(businessGroups, [hwnd, taskData, p_Action, isRefresh, isCanceled = taskData.IsCanceled(), moreLoadedGroups](vector<BusinessGroup>& p_Groups)
			{
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameGroupsRecycleBin*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, isCanceled, taskData, false))
				{
					auto frame = dynamic_cast<FrameGroupsRecycleBin*>(CWnd::FromHandle(hwnd));
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s deleted groups...", Str::getStringFromNumber(p_Groups.size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());

					{
						CWaitCursor _;
						if (frame->HasLastUpdateOperations())
						{
							frame->ShowGroupsRecycleBin(p_Groups, moreLoadedGroups, isRefresh, frame->GetLastUpdateOperations());
							frame->ForgetLastUpdateOperations();
						}
						else
						{
							frame->ShowGroupsRecycleBin(p_Groups, moreLoadedGroups, isRefresh, {});
						}
					}

					if (!isRefresh)
					{
						frame->UpdateContext(taskData, hwnd);
						shouldFinishTask = false;
					}
				}

				if (shouldFinishTask)
				{
					// Because we didn't (and can't) call UpdateContext here.
					ModuleBase::TaskFinished(frame, taskData, p_Action);
				}
			});
		});
	}
	else
	{
		ASSERT(nullptr == p_Action);
		SetAutomationGreenLight(p_Action);
	}
}

void ModuleGroup::updateGroupItems(const Command& p_Command)
{
	AutomationAction* action				= p_Command.GetAutomationAction();

	auto& info = p_Command.GetCommandInfo();
	auto existingFrame = info.GetFrame();

	ASSERT(info.Data().is_type<GridGroupsCommon::GroupChanges>());
	if (info.Data().is_type<GridGroupsCommon::GroupChanges>())
	{
		auto& groupChanges = info.Data().get_value<GridGroupsCommon::GroupChanges>();

		if (!groupChanges.empty())
		{
			auto taskData = addFrameTask(getTaskname(groupChanges.front().m_UpdateType), existingFrame, p_Command, false);
			YSafeCreateTask([existingFrame, taskData, action, groupChanges]()
			{
				vector<O365UpdateOperation> updateOperations;

				std::map<PooledString, std::map<BusinessObject::UPDATE_TYPE, vector<const GridGroupsCommon::GroupChange*>>> groupChangesByGroupID;
				for (auto& groupChange : groupChanges)
					groupChangesByGroupID[groupChange.m_GroupID][groupChange.m_UpdateType].push_back(&groupChange);

				BusinessUser tempUser;
				BusinessGroup tempGroup;
				BusinessOrgContact tempOrgContact;
				for (auto& itemByGroupId : groupChangesByGroupID)
				{
					for (auto& itemByUpdateType : itemByGroupId.second)
					{
						vector<row_pk_t> childPks;

						BusinessGroup bg;
						bg.SetID(itemByGroupId.first);
						bg.SetUpdateStatus(itemByUpdateType.first);

						enum
						{
							MEMBERS,
							OWNERS,
							ACCEPTEDAUTHORS,
							REJECTEDAUTHORS
						} kind;

						switch (itemByUpdateType.first)
						{
						case BusinessObject::UPDATE_TYPE_ADDMEMBERS:
						case BusinessObject::UPDATE_TYPE_DELETEMEMBERS:
							kind = MEMBERS;
							break;

						case BusinessObject::UPDATE_TYPE_ADDOWNERS:
						case BusinessObject::UPDATE_TYPE_DELETEOWNERS:
							kind = OWNERS;
							break;

						case BusinessObject::UPDATE_TYPE_ADDAUTHORACCEPTED:
						case BusinessObject::UPDATE_TYPE_DELETEACCEPTEDAUTHORS:
							kind = ACCEPTEDAUTHORS;
							break;

						case BusinessObject::UPDATE_TYPE_ADDAUTHORREJECTED:
						case BusinessObject::UPDATE_TYPE_DELETEREJECTEDAUTHORS:
							kind = REJECTEDAUTHORS;
							break;

						default:
							ASSERT(false);
						}

						std::map<row_pk_t, vector<row_pk_t>> siblings;
						// FIXME: Bad design. Type are handled in the forllowing order (see for instance BusinessGroup::SendAddMembersRequest): Users, Groups, OrgContacts
						// hence the following loop...
						for (const auto itemType : { GridGroupsCommon::GroupChange::UserItem, GridGroupsCommon::GroupChange::GroupItem, GridGroupsCommon::GroupChange::OrgContactItem })
						{
							for (auto& groupChange : itemByUpdateType.second)
							{
								if (itemType == groupChange->m_ItemType)
								{
									if (MEMBERS == kind)
									{
										if (GridGroupsCommon::GroupChange::UserItem == groupChange->m_ItemType)
										{
											tempUser.SetID(groupChange->m_ItemID);
											bg.AddChild(tempUser);
										}
										else if (GridGroupsCommon::GroupChange::GroupItem == groupChange->m_ItemType)
										{
											tempGroup.SetID(groupChange->m_ItemID);
											bg.AddChild(tempGroup);
										}
										else if (GridGroupsCommon::GroupChange::OrgContactItem == groupChange->m_ItemType)
										{
											tempOrgContact.SetID(groupChange->m_ItemID);
											bg.AddChild(tempOrgContact);
										}
									}
									else if (OWNERS == kind)
									{
										if (GridGroupsCommon::GroupChange::UserItem == groupChange->m_ItemType)
										{
											tempUser.SetID(groupChange->m_ItemID);
											bg.AddOwner(tempUser);
										}
										else if (GridGroupsCommon::GroupChange::GroupItem == groupChange->m_ItemType)
										{
											ASSERT(false);
											/*tempGroup.SetID(groupChange->m_ItemID);
											bg.AddOwner(tempGroup);*/
										}
										/*else if (GridGroupsCommon::GroupChange::OrgContactItem == groupChange->m_ItemType)
										{
											tempOrgContact.SetID(groupChange->m_ItemID);
											bg.AddOwner(tempOrgContact);
										}*/
									}
									else if (ACCEPTEDAUTHORS == kind)
									{
										if (GridGroupsCommon::GroupChange::UserItem == groupChange->m_ItemType)
										{
											tempUser.SetID(groupChange->m_ItemID);
											bg.AddAuthorAccepted(tempUser);
										}
										else if (GridGroupsCommon::GroupChange::GroupItem == groupChange->m_ItemType)
										{
											tempGroup.SetID(groupChange->m_ItemID);
											bg.AddAuthorAccepted(tempGroup);
										}
										else if (GridGroupsCommon::GroupChange::OrgContactItem == groupChange->m_ItemType)
										{
											tempOrgContact.SetID(groupChange->m_ItemID);
											bg.AddAuthorAccepted(tempOrgContact);
										}
									}
									else if (REJECTEDAUTHORS == kind)
									{
										if (GridGroupsCommon::GroupChange::UserItem == groupChange->m_ItemType)
										{
											tempUser.SetID(groupChange->m_ItemID);
											bg.AddAuthorRejected(tempUser);
										}
										else if (GridGroupsCommon::GroupChange::GroupItem == groupChange->m_ItemType)
										{
											tempGroup.SetID(groupChange->m_ItemID);
											bg.AddAuthorRejected(tempGroup);
										}
										else if (GridGroupsCommon::GroupChange::OrgContactItem == groupChange->m_ItemType)
										{
											tempOrgContact.SetID(groupChange->m_ItemID);
											bg.AddAuthorRejected(tempOrgContact);
										}
									}

									ASSERT(groupChange->m_RowPrimaryKey);
									if (groupChange->m_RowPrimaryKey)
									{
										childPks.push_back(*groupChange->m_RowPrimaryKey);
										if (groupChange->m_SiblingRowPrimaryKeys)
											siblings[*groupChange->m_RowPrimaryKey] = *groupChange->m_SiblingRowPrimaryKeys;
									}
								}
							}
						}

						auto updateOps = existingFrame->GetBusinessObjectManager()->WriteMultiRequests(vector<BusinessGroup>{ bg }, childPks, taskData);

						std::vector<O365UpdateOperation> siblingsUpdateOps;
						
						bool isError = false;
						for (auto& updateOp : updateOps)
						{
							if (!siblings.empty())
							{
								auto it = siblings.find(updateOp.GetPrimaryKey());
								if (siblings.end() != it)
									for (auto& sibling : it->second)
										siblingsUpdateOps.emplace_back(updateOp, sibling);
							}

							for (const auto& result : updateOp.GetResults())
							{
								// If no m_ResultInfo, something went bad (or operation was canceled). Handle it as error (0xffff)
								const auto status = result.m_ResultInfo ? result.m_ResultInfo->Response.status_code() : 0xffff;
								if (status < 200 || 300 <= status) // Is this an error?
								{
									isError = true;
									break;
								}
							}
						}

						updateOperations.insert(updateOperations.end(), std::make_move_iterator(updateOps.begin()), std::make_move_iterator(updateOps.end()));
						updateOperations.insert(updateOperations.end(), std::make_move_iterator(siblingsUpdateOps.begin()), std::make_move_iterator(siblingsUpdateOps.end()));

						if (isError)
							break; // Don't try remaining request(s) for current group
					}
				}

				// Merge results (can be several update ops for one row (like remove from members and owners)
				{
					std::multimap<vector<GridBackendField>, O365UpdateOperation> sortedUpdateOp;
					for (const auto& upOp : updateOperations)
					{
						sortedUpdateOp.insert({ upOp.GetPrimaryKey(), upOp });
					}
					updateOperations.clear();

					vector<GridBackendField> currentPk;
					for (auto& item : sortedUpdateOp)
					{
						const auto& pk = item.second.GetPrimaryKey();
						if (currentPk != pk)
						{
							updateOperations.push_back(item.second);
							currentPk = pk;
						}
						else
						{
							auto& updOP = updateOperations.back();
							updOP.StoreResults(item.second.GetResults());
						}
					}
				}

				YCallbackMessage::DoPost([existingFrame, action, taskData, updateOperations{ std::move(updateOperations) }]()
				{
					if (::IsWindow(existingFrame->GetSafeHwnd()))
					{
						existingFrame->GetGrid().ClearLog(taskData.GetId());
						existingFrame->RefreshAfterUpdate(vector<O365UpdateOperation>{ updateOperations }, action);
					}
					else
					{
						SetAutomationGreenLight(action);
					}

					TaskDataManager::Get().RemoveTaskData(taskData.GetId(), existingFrame->GetSafeHwnd());
				});
			});
		}
	}
	else
	{
		SetAutomationGreenLight(action, _YTEXT(""));
	}
}

void ModuleGroup::update(const Command& p_Command, const wstring& p_TaskText)
{
	AutomationAction* action = p_Command.GetAutomationAction();

	ASSERT(p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessGroup>>());
	if (p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessGroup>>())
	{
		auto updatedGroupsGenerator = p_Command.GetCommandInfo().Data().get_value<UpdatedObjectsGenerator<BusinessGroup>>();
		if (!updatedGroupsGenerator.GetObjects().empty())
		{
			// The frame to update once the data has been sent.
			auto existingFrame = dynamic_cast<GridFrameBase*>(p_Command.GetCommandInfo().GetFrame());
			ASSERT(nullptr != existingFrame);
			if (nullptr != existingFrame)
			{
				auto taskData = addFrameTask(p_TaskText, existingFrame, p_Command, false);
				YSafeCreateTask([taskData, existingFrame, objs = updatedGroupsGenerator, pks = updatedGroupsGenerator.GetPrimaryKeys(), action]()
				{
                    auto updateOperations = existingFrame->GetBusinessObjectManager()->WriteSingleRequest(objs.GetObjects(), objs.GetPrimaryKeys(), taskData);

					YCallbackMessage::DoPost([existingFrame, action, taskData, updateOperations{ std::move(updateOperations) }]()
					{
						if (::IsWindow(existingFrame->GetSafeHwnd()))
                        {
							existingFrame->GetGrid().ClearLog(taskData.GetId());
							existingFrame->RefreshAfterUpdate(vector<O365UpdateOperation>{ updateOperations }, action);
						}
						else
						{
							SetAutomationGreenLight(action);
						}
						TaskDataManager::Get().RemoveTaskData(taskData.GetId(), existingFrame->GetSafeHwnd());
					});
				});
			}
			else
				SetAutomationGreenLight(action, _YTEXT(""));
		}
		else
			SetAutomationGreenLight(action);
	}
	else
		SetAutomationGreenLight(action);
}

void ModuleGroup::more(const Command& p_Command)
{
	auto existingGroupFrame = dynamic_cast<FrameGroups*>(p_Command.GetCommandInfo().GetFrame());
	auto existingGroupRBFrame = dynamic_cast<FrameGroupsRecycleBin*>(p_Command.GetCommandInfo().GetFrame());
	ASSERT((nullptr == existingGroupFrame || nullptr == existingGroupRBFrame) && (nullptr != existingGroupFrame || nullptr != existingGroupRBFrame));

	if (nullptr != existingGroupFrame)
		moreImpl(existingGroupFrame, p_Command);
	else if (nullptr != existingGroupRBFrame)
		moreImpl(existingGroupRBFrame, p_Command);
	else
		SetAutomationGreenLight(p_Command.GetAutomationAction());
}

void ModuleGroup::moreImpl(FrameGroups* p_Frame, const Command& p_Command)
{
	auto p_Action = p_Command.GetAutomationAction();
	ASSERT(p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessGroup>>());
	if (p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessGroup>>())
	{
		auto& updatedGroupsGenerator = p_Command.GetCommandInfo().Data().get_value<UpdatedObjectsGenerator<BusinessGroup>>();
		if (!updatedGroupsGenerator.GetObjects().empty())
		{
			// The frame to update once the data has been sent.
			auto existingFrame = p_Frame;
			ASSERT(nullptr != existingFrame);
			if (nullptr != existingFrame)
			{
				auto taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_more_1, _YLOC("More Info")).c_str(), existingFrame, p_Command, false);

				auto logger = std::make_shared<BasicPageRequestLogger>(_T("lifecycle policies"));
				existingFrame->GetBusinessObjectManager()->GetBusinessLCP(PooledString(), logger, taskData).ThenByTask([this, hwnd = existingFrame->GetSafeHwnd(), taskData, p_Action, bom = existingFrame->GetBusinessObjectManager(), updatedGroupsGenerator, gridLogger = existingFrame->GetGridLogger()](pplx::task<BusinessLifeCyclePolicies> p_PreviousTask)
				{
					boost::YOpt<BusinessLifeCyclePolicies> lcp;
					if (!taskData.IsCanceled())
						lcp = p_PreviousTask.get();

					vector<BusinessGroup> businessGroups;

					{
						gridLogger->SetIgnoreHttp404Errors(true);
						RunOnScopeEnd _([&gridLogger]()
							{
								gridLogger->SetIgnoreHttp404Errors(false);
							});

						businessGroups = bom->MoreInfoBusinessGroups(updatedGroupsGenerator.GetObjects(), false, taskData, lcp).get();
					}

					ensureOwnersAndCreatedOnBehalfOfAreInCache(businessGroups, bom, taskData);

					// Save load more users.
					// FIXME: which blocks of data to save here?
					// MoreInfoBusinessGroups() is now not configurable, always loads: sync props + UserAdditionalRequest::RequestMailboxSettingsForNonGuest | UserAdditionalRequest::RequestDriveForNonGuest | UserAdditionalRequest::RequestManager
					// Update as soon as we make it a user choice!
					bom->GetGraphCache().SaveGroupsInSqlCache(businessGroups, { GBI::MIN, GBI::LIST, GBI::SYNCV1, GBI::SYNCBETA, GBI::CREATEDONBEHALFOF, GBI::TEAM, GBI::GROUPSETTINGS, GBI::LIFECYCLEPOLICIES, GBI::OWNERS, GBI::MEMBERSCOUNT, GBI::DRIVE, GBI::ISYAMMER }, boost::none, false);

					YDataCallbackMessage<vector<BusinessGroup>>::DoPost(businessGroups, [hwnd, taskData, p_Action, lcp, isCanceled = taskData.IsCanceled()](const vector<BusinessGroup>& p_Groups)
					{
						auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameGroups*>(CWnd::FromHandle(hwnd)) : nullptr;
						if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(p_Groups)))
						{
							ASSERT(nullptr != frame);
							try
							{
								auto& module = dynamic_cast<ModuleGroup&>(frame->GetModuleBase());
								if (lcp)
									module.SetGroupLifeCyclePolicies(*lcp);
							}
							catch (std::bad_cast&)
							{
								// dynamic_cast failed
								ASSERT(false);
							}

							frame->ShowLifecyclePolicies(false);
							frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
							frame->GetGrid().ClearLog(taskData.GetId());
							{
								CWaitCursor _;
								frame->UpdateGroupsLoadMore(p_Groups, false);
							}
						}

						// Because we didn't (and can't) call UpdateContext here.
						ModuleBase::TaskFinished(frame, taskData, p_Action);
					});
				});
			}
			else
				SetAutomationGreenLight(p_Action, _YTEXT(""));
		}
		else
			SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
	else
		SetAutomationGreenLight(p_Action);
}

void ModuleGroup::moreImpl(FrameGroupsRecycleBin* p_Frame, const Command& p_Command)
{
	auto p_Action = p_Command.GetAutomationAction();
	ASSERT(p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessGroup>>());
	if (p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessGroup>>())
	{
		auto& updatedGroupsGenerator = p_Command.GetCommandInfo().Data().get_value<UpdatedObjectsGenerator<BusinessGroup>>();
		if (!updatedGroupsGenerator.GetObjects().empty())
		{
			// The frame to update once the data has been sent.
			auto existingFrame = p_Frame;
			ASSERT(nullptr != existingFrame);
			if (nullptr != existingFrame)
			{
				auto taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_more_1, _YLOC("More Info")).c_str(), existingFrame, p_Command, false);

				existingFrame->GetBusinessObjectManager()->MoreInfoDeletedBusinessGroups(updatedGroupsGenerator.GetObjects(), false, taskData).ThenByTask([hwnd = existingFrame->GetSafeHwnd(), taskData, p_Action, bom = existingFrame->GetBusinessObjectManager(), updatedGroupsGenerator](pplx::task<vector<BusinessGroup>> p_PreviousTask)
					{
						auto businessGroups = p_PreviousTask.get();
						for (auto& group : businessGroups)
						{
							// FIXME: new modular cache
							group.SetFlags(BusinessObject::MORE_LOADED);
						}
						YDataCallbackMessage<vector<BusinessGroup>>::DoPost(businessGroups, [hwnd, taskData, p_Action, isCanceled = taskData.IsCanceled()](const vector<BusinessGroup>& p_Groups)
						{
							auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameGroupsRecycleBin*>(CWnd::FromHandle(hwnd)) : nullptr;
							if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(p_Groups)))
							{
								ASSERT(nullptr != frame);
								try
								{
									auto& module = dynamic_cast<ModuleGroup&>(frame->GetModuleBase());
								}
								catch (std::bad_cast&)
								{
									// dynamic_cast failed
									ASSERT(false);
								}

								frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
								frame->GetGrid().ClearLog(taskData.GetId());

								{
									CWaitCursor _;
									frame->UpdateGroupsLoadMore(p_Groups, false);
								}
							}

							// Because we didn't (and can't) call UpdateContext here.
							ModuleBase::TaskFinished(frame, taskData, p_Action);
						});
					});
			}
			else
				SetAutomationGreenLight(p_Action, _YTEXT(""));
		}
		else
			SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
	else
		SetAutomationGreenLight(p_Action);
}

void ModuleGroup::updateLCP(const Command& p_Command)
{
	AutomationAction* p_Action = p_Command.GetAutomationAction();

	ASSERT(p_Command.GetCommandInfo().Data().is_type<BusinessLifeCyclePolicies>());
	if (p_Command.GetCommandInfo().Data().is_type<BusinessLifeCyclePolicies>())
	{
		const BusinessLifeCyclePolicies& LCP = p_Command.GetCommandInfo().Data().get_value<BusinessLifeCyclePolicies>();

		// The frame to update once the data has been sent.
		FrameGroups* existingFrame = dynamic_cast<FrameGroups*>(p_Command.GetCommandInfo().GetFrame());
		ASSERT(nullptr != existingFrame);
        if (nullptr != existingFrame)
        {
            YtriaTaskData taskData;

            if (LCP.GetUpdateStatus() == BusinessObject::UPDATE_TYPE_CREATE)
                taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_updateLCP_1, _YLOC("Create Global Expiration Policy")).c_str(), existingFrame, p_Command, false);
            else if (LCP.GetUpdateStatus() == BusinessObject::UPDATE_TYPE_MODIFY)
                taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_updateLCP_2, _YLOC("Update Global Expiration Policy")).c_str(), existingFrame, p_Command, false);
            else if (LCP.GetUpdateStatus() == BusinessObject::UPDATE_TYPE_DELETE)
                taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_updateLCP_3, _YLOC("Delete Global Expiration Policy")).c_str(), existingFrame, p_Command, false);
            else
                ASSERT(FALSE);

            auto operations = existingFrame->GetBusinessObjectManager()->WriteSingleRequest(vector<BusinessLifeCyclePolicies>{ LCP }, { row_pk_t() }, taskData);

			// Should we use refreshAfterUpdate instead??
			existingFrame->GetBusinessObjectManager()->GetBusinessLCP(PooledString(), std::make_shared<BasicPageRequestLogger>(_T("lifecycle policies")), taskData).ThenByTask([operations, taskData, hwnd = existingFrame->GetSafeHwnd(), p_Action](pplx::task<BusinessLifeCyclePolicies> p_PreviousTask)
			{
				boost::YOpt<BusinessLifeCyclePolicies> lcp;
				if (!taskData.IsCanceled())
					lcp = p_PreviousTask.get();

				YDataCallbackMessage<boost::YOpt<BusinessLifeCyclePolicies>>::DoPost(lcp, [operations, taskData, lcp, hwnd, isCanceled = taskData.IsCanceled(), p_Action](const boost::YOpt<BusinessLifeCyclePolicies>& p_LCP)
                {
					auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameGroups*>(CWnd::FromHandle(hwnd)) : nullptr;
					if (ShouldBuildView(hwnd, isCanceled, taskData, false))
                    {
						ASSERT(nullptr != frame);
                        frame->GetGrid().ClearLog(taskData.GetId());

                        ASSERT(operations.size() == 1 && operations[0].GetResults().size() == 1);
                        if (operations.size() == 1 &&
                            operations[0].GetResults().size() == 1 &&
                            operations[0].GetResults()[0].m_Error.is_initialized())
                        {

                            YCallbackMessage::DoPost([frame, operations]() {
                                YCodeJockMessageBox errorMsg(frame,
                                    DlgMessageBox::eIcon_Error,
                                    YtriaTranslate::Do(ModuleGroup_updateLCP_4, _YLOC("Lifecycle Policies")).c_str(),
                                    YtriaTranslate::Do(ModuleGroup_updateLCP_5, _YLOC("An error occurred while updating life cycle policies.")).c_str(),
                                    operations[0].GetResults()[0].m_Error->GetFullErrorMessage(),
                                    { { IDOK, YtriaTranslate::Do(ModuleGroup_updateLCP_6, _YLOC("OK")).c_str() } });
                                errorMsg.DoModal();
                            });
                        }
                        else
                        {
                            try
                            {
                                auto& module = dynamic_cast<ModuleGroup&>(frame->GetModuleBase());
								if (lcp)
									module.SetGroupLifeCyclePolicies(*lcp);
                            }
                            catch (std::bad_cast&)
                            {
                                // dynamic_cast failed
                                ASSERT(false);
                            }
                            frame->ShowLifecyclePolicies(true);
                        }
                    }

					// Because we didn't (and can't) call UpdateContext here.
					ModuleBase::TaskFinished(frame, taskData, p_Action);
                });
            });
        }
	}
}

void ModuleGroup::updateSettings(const Command& p_Command)
{
    AutomationAction* p_Action = p_Command.GetAutomationAction();

    using expectedType = std::map<PooledString, BusinessGroupSetting>;
    ASSERT(p_Command.GetCommandInfo().Data().is_type<expectedType>());
    if (p_Command.GetCommandInfo().Data().is_type<expectedType>())
    {
        FrameGroups* existingFrame = dynamic_cast<FrameGroups*>(p_Command.GetCommandInfo().GetFrame());
        ASSERT(nullptr != existingFrame);
        if (nullptr != existingFrame)
        {
            auto taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_updateSettings_1, _YLOC("Update Group Global Settings")).c_str(), existingFrame, p_Command, false);
            const auto& updatedGroupSettings = p_Command.GetCommandInfo().Data().get_value<expectedType>();

            // Do all requests
			YSafeCreateTask([updatedGroupSettings, existingFrame, taskData, p_Action]() {
                bool error = false;

                wstring errorDetails;
                for (const auto& setting : updatedGroupSettings)
                {
                    HttpResultWithError resultWithError;
                    boost::YOpt<bool> createdOrDeleted = setting.second.GetCreatedOrDeleted();
                    if (createdOrDeleted == boost::none)
                        resultWithError = existingFrame->GetBusinessObjectManager()->UpdateGroupSetting(BusinessGroupSetting::ToGroupSetting(setting.second), taskData).GetTask().get();
                    else if (*createdOrDeleted == true) // Created
                        resultWithError = existingFrame->GetBusinessObjectManager()->CreateGroupSetting(BusinessGroupSetting::ToGroupSetting(setting.second), taskData).GetTask().get();
                    else if (*createdOrDeleted == false) // Deleted
                        resultWithError = existingFrame->GetBusinessObjectManager()->DeleteGroupSetting(setting.second.GetID(), taskData).GetTask().get();

                    if (resultWithError.m_Error)
                    {
                        error = true;
                        errorDetails += resultWithError.m_Error ? resultWithError.m_Error->GetFullErrorMessage() : wstring(_YTEXT("")) + wstring(_YTEXT("\n"));
                    }
                }

                // Do settings requests again so we can show the updated values
                auto groupSettingsTemplates = existingFrame->GetBusinessObjectManager()->GetBusinessGroupSettingTemplates(taskData).GetTask().get();
                auto tenantGroupSettings = existingFrame->GetBusinessObjectManager()->GetBusinessGroupSettings(taskData).GetTask().get();

                YCallbackMessage::DoPost([error, existingFrame, p_Action, taskData, errorDetails, groupSettingsTemplates, tenantGroupSettings]()
				{
                    if (::IsWindow(existingFrame->GetSafeHwnd()))
                    {
						existingFrame->SetGroupSettingTemplates(groupSettingsTemplates);

                        existingFrame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
                        existingFrame->GetGrid().ClearLog(taskData.GetId());

						// Do this after ClearLog so that grid is enabled and can be repaint.
						existingFrame->SetTenantGroupSettings(tenantGroupSettings, true);
                    }

                    TaskDataManager::Get().RemoveTaskData(taskData.GetId(), existingFrame->GetSafeHwnd());

                    if (!error)
                    {
                        YCodeJockMessageBox message(existingFrame,
                            DlgMessageBox::eIcon_Information,
                            YtriaTranslate::Do(ModuleGroup_updateSettings_2, _YLOC("Update Group Global Settings")).c_str(),
                            YtriaTranslate::Do(ModuleGroup_updateSettings_3, _YLOC("Update successful!")).c_str(),
                            _YTEXT(""),
                            { { IDOK, YtriaTranslate::Do(ModuleGroup_updateSettings_4, _YLOC("Ok")).c_str() } });
                        message.DoModal();
                    }
                    else
                    {
                        YCodeJockMessageBox message(existingFrame,
                            DlgMessageBox::eIcon_Error,
                            YtriaTranslate::Do(ModuleGroup_updateSettings_5, _YLOC("Update Group Global Settings")).c_str(),
                            YtriaTranslate::Do(ModuleGroup_updateSettings_6, _YLOC("One or more errors occurred!")).c_str(),
                            errorDetails.c_str(),
                            { { IDOK, YtriaTranslate::Do(ModuleGroup_updateSettings_7, _YLOC("Retry")).c_str() }, { IDCANCEL, YtriaTranslate::Do(ModuleGroup_updateSettings_8, _YLOC("Cancel")).c_str() } });
                        auto result = message.DoModal();

                        if (result == IDOK)
                            existingFrame->ShowTenantSettings();
                    }
                    SetAutomationGreenLight(p_Action);
                });
            });
        }
        else
            SetAutomationGreenLight(p_Action);
    }
    else
        SetAutomationGreenLight(p_Action);
}

void ModuleGroup::loadSnapshot(const Command& p_Command)
{
	using LoaderType = std::shared_ptr<GridSnapshot::Loader>;
	if (p_Command.GetCommandInfo().Data().is_type<LoaderType>())
	{
		auto loader = p_Command.GetCommandInfo().Data().get_value<LoaderType>();
		const auto& metadata = loader->GetMetadata();

		if (GridUtil::g_AutoNameGroups == *metadata.m_GridAutomationName)
			LoadSnapshot<FrameGroups>(p_Command, YtriaTranslate::Do(ModuleGroup_show_1, _YLOC("Groups")).c_str());
		else if (GridUtil::g_AutoNameGroupsHierarchy == *metadata.m_GridAutomationName)
			LoadSnapshot<FrameGroupMembersHierarchy>(p_Command, YtriaTranslate::Do(ModuleGroup_showItems_1, _YLOC("Group Members")).c_str());
	}
}

void ModuleGroup::saveOnPrem(const Command& p_Command)
{
	auto action = p_Command.GetAutomationAction();
	FrameGroups* existingFrame = dynamic_cast<FrameGroups*>(p_Command.GetCommandInfo().GetFrame());

	ASSERT(nullptr != existingFrame && p_Command.GetCommandInfo().Data().is_type<GridGroups::OnPremiseChanges>());
	if (nullptr != existingFrame && p_Command.GetCommandInfo().Data().is_type<GridGroups::OnPremiseChanges>())
	{
		auto& changes = p_Command.GetCommandInfo().Data().get_value<GridGroups::OnPremiseChanges>();
		auto taskData = addFrameTask(_T("Update On-Premises Groups"), existingFrame, p_Command, false);
		YSafeCreateTask([taskData, existingFrame, changes, action, sapio365Session = existingFrame->GetSapio365Session()]()
		{
			vector<O365UpdateOperation> upOp;
			upOp.reserve(changes.m_GroupsToUpdate.size());
			for (const auto& c : changes.m_GroupsToUpdate)
			{
				upOp.emplace_back(c.first);

				OnPremiseGroupUpdateRequester req(c.second);
				req.Send(sapio365Session, taskData).get();
				auto result = req.GetResult();
				ASSERT(result);
				if (result)
				{
					if (result->Get().m_Success)
					{
						// Handy to use a fake http_response with 200 code so that Grid code can see it's a success.
						// Consider updating code that uses this status code if you want not to use the http_response in that case.
						RestResultInfo successResult(true);
						successResult.Response.set_status_code(200);
						upOp.back().StoreResult({ successResult, boost::none });
					}
					else
					{
						SapioError err(GetPSErrorString(result), 0);
						upOp.back().StoreResult({ boost::none, err });
					}
				}
			}

			YCallbackMessage::DoPost([existingFrame, action, taskData, upOp{ std::move(upOp) }]()
			{
				if (::IsWindow(existingFrame->GetSafeHwnd()))
				{
					existingFrame->GetGrid().ClearLog(taskData.GetId());
					existingFrame->SetOnPremiseGroupsUpdateResult(upOp);
				}

				SetAutomationGreenLight(action);
				TaskDataManager::Get().RemoveTaskData(taskData.GetId(), existingFrame->GetSafeHwnd());
			});
		});
	}
	else
		SetAutomationGreenLight(action, _YTEXT(""));
}

void ModuleGroup::showGroupLCPStatus(const Command& p_Command)
{
	AutomationAction* p_Action = p_Command.GetAutomationAction();

	ASSERT(p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessGroup>>());
	if (p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessGroup>>())
	{
		auto& businessGroupsToUpdate = p_Command.GetCommandInfo().Data().get_value<UpdatedObjectsGenerator<BusinessGroup>>().GetObjects();
		if (!businessGroupsToUpdate.empty())
		{
			// The frame to update once the data has been sent.
			FrameGroups* existingFrame = dynamic_cast<FrameGroups*>(p_Command.GetCommandInfo().GetFrame());
			ASSERT(nullptr != existingFrame);
			if (nullptr != existingFrame)
			{
				auto taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_showGroupLCPStatus_1, _YLOC("Retrieving Group Expiration Status")).c_str(), existingFrame, p_Command, false);

				ASSERT(GetGroupLifeCyclePolicies().IsPresent() && GetGroupLifeCyclePolicies().m_ManagedGroupTypes && GetGroupLifeCyclePolicies().m_ManagedGroupTypes.get() == _YTEXT("Selected"));

				YSafeCreateTask(
					[hwnd = existingFrame->GetSafeHwnd(), taskData, businessGroupsToUpdate, lcp = GetGroupLifeCyclePolicies(), bom = existingFrame->GetBusinessObjectManager(), p_Action]()
				{
					vector<BusinessGroup> businessGroups = businessGroupsToUpdate;
					for (BusinessGroup& bg : businessGroups)
					{
						ASSERT(bg.GetCombinedGroupType() && bg.GetCombinedGroupType().get() == BusinessGroup::g_Office365Group);
						if (bg.GetCombinedGroupType() && bg.GetCombinedGroupType().get() == BusinessGroup::g_Office365Group)
						{
							if (boost::none != lcp.m_ManagedGroupTypes && lcp.m_ManagedGroupTypes.get() != _YTEXT("None"))
							{
								BusinessLifeCyclePolicies LCPGroup = bom->GetBusinessLCP(bg.GetID(), std::make_shared<BasicPageRequestLogger>(_T("group lifecycle policies")), taskData).GetTask().get();

								if (LCPGroup.IsPresent())
								{
									if (LCPGroup.m_ManagedGroupTypes && LCPGroup.m_ManagedGroupTypes.get() == _YTEXT("Selected"))
									{
										bg.SetLCPStatus(boost::YOpt<PooledString>(BusinessLifeCyclePolicies::g_StatusApplied));
									}
									else
									{
										ASSERT(!LCPGroup.m_ManagedGroupTypes || LCPGroup.m_ManagedGroupTypes.get().IsEmpty());
										bg.SetLCPStatus(boost::YOpt<PooledString>(BusinessLifeCyclePolicies::g_StatusNotApplied));
									}
								}
								else
								{
									bg.SetLCPStatus(boost::YOpt<PooledString>());
								}
							}
						}
					}

					YDataCallbackMessage<vector<BusinessGroup>>::DoPost(businessGroups, [hwnd, taskData, p_Action, isCanceled = taskData.IsCanceled()](const vector<BusinessGroup>& p_Groups)
					{
						bool shouldFinishTask = true;
						auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameGroups*>(CWnd::FromHandle(hwnd)) : nullptr;
						if (ShouldBuildView(hwnd, isCanceled, taskData, false))
						{
							ASSERT(nullptr != frame);
							frame->GetGrid().ClearLog(taskData.GetId());
							frame->UpdateGroupLCPStatus(p_Groups);
							// FIXME: Should we process License to consume one token (as for a submoule)?
							//frame->UpdateContext(taskData, hwnd);
							//shouldFinishTask = false;
						}

						if (shouldFinishTask)
						{
							// Because we didn't (and can't) call UpdateContext here.
							ModuleBase::TaskFinished(frame, taskData, p_Action);
						}
					});
				});

			}
		}
		else
		 	SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
	else
	 	SetAutomationGreenLight(p_Action);
}

void ModuleGroup::updateLCPGroup(const Command& p_Command)
{
	AutomationAction* p_Action = p_Command.GetAutomationAction();

	ASSERT(p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessGroup>>());
	if (p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessGroup>>())
	{
		// The frame to update once the data has been sent.
		FrameGroups* existingFrame = dynamic_cast<FrameGroups*>(p_Command.GetCommandInfo().GetFrame());
		ASSERT(nullptr != existingFrame);
		if (nullptr != existingFrame)
		{
			auto updatedGroupsGenerator = p_Command.GetCommandInfo().Data().get_value<UpdatedObjectsGenerator<BusinessGroup>>();

			// Update status will always be of one type for all BusinessGroups.
			YtriaTaskData taskData;
			const vector<BusinessGroup> Temp =  updatedGroupsGenerator.GetObjects();
			BusinessGroup Orgy = *Temp.begin();
 			ASSERT(!Temp.empty());
 			if (Orgy.GetUpdateStatus() == BusinessObject::UPDATE_TYPE_LCPRENEW)
 				taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_updateLCPGroup_1, _YLOC("Renewing group for Expiration Policy")).c_str(), existingFrame, p_Command, false);
			else if (Orgy.GetUpdateStatus() == BusinessObject::UPDATE_TYPE_LCPADDGROUP)
				taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_updateLCPGroup_2, _YLOC("Adding group to Expiration Policy")).c_str(), existingFrame, p_Command, false);
			else if (Orgy.GetUpdateStatus() == BusinessObject::UPDATE_TYPE_LCPREMOVEGROUP)
				taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_updateLCPGroup_3, _YLOC("Removing group from Expiration Policy")).c_str(), existingFrame, p_Command, false);
			else
				ASSERT(FALSE);

			YSafeCreateTask([taskData, hwnd = existingFrame->GetSafeHwnd(), bom = existingFrame->GetBusinessObjectManager(), updatedGroupsGenerator, p_Action, p_Command]()
			{
				// Apply
				auto updateOperations = bom->WriteSingleRequest(updatedGroupsGenerator.GetObjects(), updatedGroupsGenerator.GetPrimaryKeys(), taskData);

				// Refresh grid
				YCallbackMessage::DoPost([hwnd, p_Action, taskData, updateOperations{ std::move(updateOperations) }, p_Command]()
				{
					if (::IsWindow(hwnd))
					{
						auto frame = dynamic_cast<FrameGroups*>(CWnd::FromHandle(hwnd));
						frame->GetGrid().ClearLog(taskData.GetId());
						// Trigger refresh
						frame->RefreshAfterUpdate(vector<O365UpdateOperation>{ updateOperations }, p_Action);
					}
					SetAutomationGreenLight(p_Action);
					TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
				});
			});
		}
		else
		 	SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
	else
	 	SetAutomationGreenLight(p_Action);
}

void ModuleGroup::loadGroups(const Command& p_Command, const CommandInfo& p_CommandInfo, FrameGroups* p_Frame, bool p_IsRefresh, bool p_Sync)
{
	ASSERT(!p_Sync || p_IsRefresh); // Don't be a fool, can't sync a first load
	ASSERT(!p_Sync || !p_CommandInfo.Data2().is_valid()); // Cannot sync with pre-filtering

	auto taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_1, _YLOC("Show Groups")).c_str(), p_Frame, p_Command, !p_IsRefresh);

	const bool gridIsEmpty = p_Frame->GetGrid().GetBackendRows().empty();

	// Load Mores
	std::vector<BusinessGroup> groupsLoadMoreRequestInfo;

	if (p_IsRefresh && p_Frame->GetAndResetDoLoadMoreOnNextRefresh()) // Enable load mores on Sync?
	{
		GridGroups* gridGroups = dynamic_cast<GridGroups*>(&p_Frame->GetGrid());
		ASSERT(nullptr != gridGroups);
		if (nullptr != gridGroups)
		{
			std::vector<GridBackendRow*> rowsWithMoreLoaded;
			gridGroups->GetRowsWithMoreLoaded(rowsWithMoreLoaded);

			if (!rowsWithMoreLoaded.empty())
				groupsLoadMoreRequestInfo = gridGroups->GetLoadMoreRequestInfo(rowsWithMoreLoaded);

			if (p_Frame->HasLastUpdateOperations())
			{
				groupsLoadMoreRequestInfo.erase(std::remove_if(groupsLoadMoreRequestInfo.begin(), groupsLoadMoreRequestInfo.end(), [p_Frame](const BusinessGroup& p_Group) {
					GridGroups* gridGroups = dynamic_cast<GridGroups*>(&p_Frame->GetGrid());
					ASSERT(nullptr != gridGroups);
					if (nullptr != gridGroups)
					{
						for (const auto& update : p_Frame->GetLastUpdateOperations())
						{
							if (update.GetPkFieldStr(gridGroups->GetColId()) == p_Group.GetID())
								return false; // That group has just been updated, keep it for load more
						}
					}
					return true;
					})
					, groupsLoadMoreRequestInfo.end());
			}
		}
	}

	bool shouldUseSql = !p_IsRefresh; // not refreshing existing grid
						//&& (!moduleOptions || moduleOptions->m_CustomFilter.IsEmpty()); // not using pre-filtering

	// FIXME: should we show the following message with filters as we use SQL cache?

	if (shouldUseSql)
	{
		const auto useCacheSetting = UseCacheSetting().Get();

		if (useCacheSetting)
		{
			shouldUseSql = *useCacheSetting;
		}
		else
		{
			DlgUserCacheDataHTML dlg(p_Frame);
			dlg.SetTitleWindow(YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_6, _YLOC("Cached data available:")).c_str());
			dlg.SetIntroWindow(YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_8, _YLOC("You have data available in the cache. How do you want sapio365 to handle this?")).c_str());
			dlg.SetCacheTxtBold(YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_11, _YLOC("Always use cached data")).c_str());
			dlg.SetCacheTxtReg(YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_7, _YLOC("- faster but data may not be up to date")).c_str());
			dlg.SetCacheTxtLine(YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_15, _YLOC("(recommended for large tenants)")).c_str());
			dlg.SetServerBold(YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_10, _YLOC("Always load from Server")).c_str());
			dlg.SetServerReg(YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_16, _YLOC("- may be slower")).c_str());
			dlg.SetServerLine(_T("(recommended for smaller tenants)"));
			dlg.SetCommentTextReg(YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_19, _YLOC("You can change this in Settings")).c_str());

			if (IDOK == dlg.DoModal())
				shouldUseSql = false;
			else
				shouldUseSql = true;

			UseCacheSetting().Set(shouldUseSql);
		}
	}

	auto bom = p_Frame->GetBusinessObjectManager();
	ASSERT(bom);

	bom->GetGraphCache().UpdateGroupsDeltaLinkFromSql();

	struct State
	{
		const enum Mode { LOAD, SYNC, REFRESH } mode;
		const bool sqlCacheIsExpired;
		const bool sqlCacheIsFull;
		const bool hasSyncLink;
		const bool refreshMore;
		const bool hasDeltaCapabilities; // Doesn't exist for Germany https://docs.microsoft.com/en-us/graph/deployments, but this cloud is said not to be used anymore (by clients)...
		const std::shared_ptr<wstring> localDeltaLink;
		bool userAgreedToUpdateCache = true;
		bool sqlCacheHasMissingInfo = false;
	}
	state{ p_Sync ? State::SYNC : p_IsRefresh ? State::REFRESH : State::LOAD
		, bom->GetGraphCache().IsSqlGroupsCacheExpired()
		, bom->GetGraphCache().IsSqlGroupsCacheFull()
		, bom->GetGraphCache().IsGroupsDeltaSyncAvailable()
		, p_IsRefresh && !groupsLoadMoreRequestInfo.empty()
		, bom->GetGraphCache().HasGroupsDeltaCapabilities()
		, p_Frame->GetGroupsDeltaLink() ? std::make_shared<wstring>(*p_Frame->GetGroupsDeltaLink()) : std::make_shared<wstring>() };

	if (shouldUseSql
		&& p_CommandInfo.GetIds().empty() // load all
		&& (!p_CommandInfo.Data2().is_type<ModuleOptions>() || p_CommandInfo.Data2().get_value<ModuleOptions>().m_CustomFilter.IsEmpty()) // no pre-filter
		)
	{
		ASSERT(state.mode == State::LOAD);
		const auto fullListSize = bom->GetGraphCache().GetSqlGroupsCacheFullListSize();
		ASSERT(state.sqlCacheIsFull == (fullListSize >= 0));

		if (state.sqlCacheIsFull)
		{
			auto count = bom->GetGraphCache().CountSqlGroupsMissingRequirements({ GBI::MIN, GBI::LIST });

			state.sqlCacheHasMissingInfo = count > 0;

			wstring title, message, details;
			if (0 < count && state.sqlCacheIsExpired)
			{
				title = YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_11, _YLOC("Cached data is out of date")).c_str();
				message = YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_12, _YLOC("This cache is more than 24 hours old.")).c_str();
				details = YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_20, _YLOC("Also, %1 of the %2 groups found in the current cache have incomplete data. If you don't load from Server, they will not appear in your grid."), Str::getStringFromNumber(count).c_str(), Str::getStringFromNumber(fullListSize).c_str());
			}
			else if (0 < count)
			{
				title = YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_2, _YLOC("Incomplete data")).c_str();
				message = YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_3, _YLOC("Would you like to load the whole list from the server?")).c_str();
				details = YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_21, _YLOC("%1 of the %2 groups found in the current cache have incomplete data. If you don't load from Server, they will not appear in your grid."), Str::getStringFromNumber(count).c_str(), Str::getStringFromNumber(fullListSize).c_str());
			}
			else if (state.sqlCacheIsExpired)
			{
				title = YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_11, _YLOC("Cached data is out of date")).c_str();
				message = YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_12, _YLOC("This cache is more than 24 hours old.")).c_str();
				details.clear();
			}

			if (!title.empty())
			{
				YCodeJockMessageBox dlg(
					p_Frame,
					DlgMessageBox::eIcon_Question,
					title,
					message,
					details,
					{ { IDOK, YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_4, _YLOC("Load from Server")).c_str() },{ IDCANCEL, YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_5, _YLOC("Continue with cache")).c_str() } });

				if (IDCANCEL == dlg.DoModal())
				{
					state.userAgreedToUpdateCache = false;
				}
			}
		}
	}

	YSafeCreateTaskMutable([this, p_Command, gridIsEmpty, shouldUseSql, state, taskData, p_CommandInfo, p_Action = p_Command.GetAutomationAction(), hwnd = p_Frame->GetSafeHwnd(), groupsLoadMoreRequestInfo, bom, isUpdateAfterApply = p_Frame->HasLastUpdateOperations(), isGroupDetailsFrame = !p_Frame->GetModuleCriteria().m_IDs.empty(), p_IsRefresh, gridLogger = p_Frame->GetGridLogger()]() mutable
	{
		const auto ids = p_CommandInfo.GetIds();
		const auto& moduleOptions = p_CommandInfo.Data2().is_type<ModuleOptions>()
			? p_CommandInfo.Data2().get_value<ModuleOptions>()
			: boost::YOpt<ModuleOptions>();

		auto groupsPtr = std::make_shared<vector<BusinessGroup>>();
		auto groupsWithMoreLoadedPtr = std::make_shared<vector<BusinessGroup>>();

		boost::YOpt<YTimeDate> refreshDate;
		auto& groups = *groupsPtr;
		auto& groupsWithMoreLoaded = *groupsWithMoreLoadedPtr;
		vector<BusinessGroup> groupsToDoLoadMore/* = groupsLoadMoreRequestInfo*/;
		bool loadMoreOnlyReceivedGroups = true;

		boost::YOpt<BusinessLifeCyclePolicies> lcp = bom->GetBusinessLCP(PooledString(), std::make_shared<BasicPageRequestLogger>(_T("lifecycle policies")), taskData).GetTask().get();
		boost::YOpt<vector<BusinessGroupSettingTemplate>> groupSettingsTemplates = !taskData.IsCanceled() ? boost::YOpt<vector<BusinessGroupSettingTemplate>>(bom->GetBusinessGroupSettingTemplates(taskData).GetTask().get()) : boost::none;
		boost::YOpt<vector<BusinessGroupSetting>> tenantGroupSettings = !taskData.IsCanceled() ? boost::YOpt<vector<BusinessGroupSetting>>(bom->GetBusinessGroupSettings(taskData).GetTask().get()) : boost::none;

		const bool wantsAll = ids.empty();
		auto groupIds = ids;

		bool genericProcess = true;
		std::shared_ptr<wstring> lastDeltaLink;
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("groups"));

		// If true, group details uses a partial delta (on specific ids) if available, else group details use synced cache and/or individual requests
		// Delta cannot work if id selection can be changed in module (currently not the case).
		static constexpr bool usePartialDeltaForGroupDetails = true;
		if (moduleOptions && !moduleOptions->m_CustomFilter.IsEmpty())
		{
			if (shouldUseSql)
			{
				// Request only min info? If we do, we must ensure cache is up-to-date (see above dialog and userAgreedToUpdateCache value).
				groups = bom->GetGraphCache().GetPrefilteredGroups(taskData, moduleOptions->m_CustomFilter, logger).get();

				if (groups.size() > 0 && !groups[0].GetError())
				{
					O365IdsContainer ids;
					for (const auto& u : groups)
						ids.insert(u.GetID());

					if (!ids.empty())
						groups = bom->GetGraphCache().GetSqlCachedGroups(hwnd, ids, true, taskData);
				}
			}
			else
			{
				groups = bom->GetGraphCache().GetPrefilteredGroups(taskData, moduleOptions->m_CustomFilter, logger).get();
			}

			genericProcess = false;
		}
		else if (!groupIds.empty() && (usePartialDeltaForGroupDetails ? (isUpdateAfterApply && (!isGroupDetailsFrame || !state.hasDeltaCapabilities)) : (isUpdateAfterApply || isGroupDetailsFrame)))
		{
			ASSERT(state.mode != State::SYNC); // Specific case refresh after apply.

			// Load more requests will be done anyway, don't take them into account to decide
			const auto numElementsToRefresh = groupIds.size() - groupsLoadMoreRequestInfo.size();
			if (numElementsToRefresh <= 0 || !ShouldDoListRequests(bom->GetGraphCache().GetUserCount(), numElementsToRefresh, MSGraphSession::g_DefaultNbElementsPerPage))
			{
				// As it seems for efficient to do separate requests, do it:

				loadMoreOnlyReceivedGroups = false;

				ASSERT(groups.empty());
				for (const auto& id : groupIds)
				{
					auto it = std::find_if(groupsLoadMoreRequestInfo.begin(), groupsLoadMoreRequestInfo.end(), [&id](auto& g)
						{
							return id == g.GetID();
						});

					// Load more users will be requested later at the end of this method, because loadMoreOnlyReceivedGroups is false.

					if (groupsLoadMoreRequestInfo.end() == it)
					{
						groups.emplace_back();
						groups.back().m_Id = id;
					}
				}

				if (!groups.empty())
				{
					groups = bom->GetBusinessGroupsIndividualRequests(groups, taskData).GetTask().get();
					bom->GetGraphCache().SaveGroupsInSqlCache(groups, { GBI::MIN, GBI::LIST }, refreshDate, false);
				}
			}
			else
			{
				// Maybe change this eventually: we currently force a load of the whole list (updated with sync) to avoid issue in grid modifications.
				if (state.hasDeltaCapabilities)
					groups = bom->GetGraphCache().GetUpToDateGroups<BusinessGroup>(groupIds, logger, GraphCache::PreferredMode::DELTASYNC, taskData).get();
				else
					groups = bom->GetGraphCache().GetUpToDateGroups<BusinessGroup>(groupIds, logger, GraphCache::PreferredMode::LIST, taskData).get();
				refreshDate = bom->GetGraphCache().GetSqlUsersCacheLastFullRefreshDate();
			}

			genericProcess = false;
		}
		else if (usePartialDeltaForGroupDetails && !groupIds.empty() && isGroupDetailsFrame)
		{
			ASSERT(state.hasDeltaCapabilities);
			if (state.mode == State::LOAD || (state.mode == State::REFRESH && !isUpdateAfterApply) || !state.localDeltaLink)
			{
				ASSERT(!lastDeltaLink);
				lastDeltaLink = std::make_shared<wstring>(_YTEXT(""));
				groups = bom->GetGraphCache().InitPartialGroupsDelta(taskData, groupIds, lastDeltaLink, logger).get();

				if (!groups.empty())
				{
					O365IdsContainer ids;
					for (const auto& u : groups)
						ids.insert(u.GetID());

					groups = bom->GetGraphCache().GetSqlCachedGroups(hwnd, ids, true, taskData);
				}
			}
			else
			{
				ASSERT(state.mode == State::SYNC || state.mode == State::REFRESH);
				lastDeltaLink = state.localDeltaLink ? std::make_shared<wstring>(*state.localDeltaLink) : std::make_shared<wstring>();
				groups = bom->GetGraphCache().SyncGroupsFromDeltaLink(taskData, logger, lastDeltaLink, false).get();

				if (state.mode == State::REFRESH)
				{
					ASSERT(isUpdateAfterApply);
					if (!groups.empty())
					{
						O365IdsContainer ids;
						for (const auto& u : groups)
							ids.insert(u.GetID());

						groups = bom->GetGraphCache().GetSqlCachedGroups(hwnd, ids, true, taskData);
					}
				}
			}
			genericProcess = false;
		}

		if (genericProcess)
		{
			ASSERT(state.mode != State::SYNC || wantsAll);
			ASSERT(state.mode != State::SYNC || state.hasDeltaCapabilities);

			if (state.mode == State::SYNC && state.hasSyncLink && state.hasDeltaCapabilities)
			{
				// Sync (+ update sql)
				groups = bom->GetGraphCache().SyncGroupsFromDeltaLink(taskData, logger, state.localDeltaLink).get();

				// Don't: Sync + load sql
				//groups = bom->GetGraphCache().GetUpToDateGroups<BusinessGroup>(groupIds, logger, GraphCache::PreferredMode::DELTASYNC, taskData, state.localDeltaLink).get();

				refreshDate = bom->GetGraphCache().GetSqlGroupsCacheLastFullRefreshDate();
			}
			else if (shouldUseSql && state.sqlCacheIsFull && (!state.sqlCacheIsExpired && !state.sqlCacheHasMissingInfo || !state.userAgreedToUpdateCache))
			{
				ASSERT(state.mode == State::LOAD);

				// load sql
				groups = bom->GetGraphCache().GetSqlCachedGroups(hwnd, groupIds, false, taskData);
				refreshDate = bom->GetGraphCache().GetSqlGroupsCacheLastFullRefreshDate();
			}
			else if (shouldUseSql && (!state.sqlCacheIsFull || state.sqlCacheIsExpired || state.sqlCacheHasMissingInfo) && state.userAgreedToUpdateCache && state.hasDeltaCapabilities && state.hasSyncLink)
			{
				ASSERT(state.mode == State::LOAD);

				// Delta sync sql (sync + load sql)
				groups = bom->GetGraphCache().GetUpToDateGroups<BusinessGroup>(groupIds, logger, GraphCache::PreferredMode::DELTASYNC, taskData, state.localDeltaLink).get();
				refreshDate = bom->GetGraphCache().GetSqlGroupsCacheLastFullRefreshDate();
			}
			else if (state.mode == State::LOAD && (!state.sqlCacheIsFull || state.sqlCacheIsExpired || state.sqlCacheHasMissingInfo) && state.userAgreedToUpdateCache && state.hasDeltaCapabilities && !state.hasSyncLink)
			{
				// Init sql (delta)
				groups = bom->GetGraphCache().GetUpToDateGroups<BusinessGroup>(groupIds, logger, GraphCache::PreferredMode::INITDELTA, taskData).get();
				refreshDate = bom->GetGraphCache().GetSqlGroupsCacheLastFullRefreshDate();
			}
			else if (state.mode == State::LOAD && (!state.sqlCacheIsFull || state.sqlCacheIsExpired || state.sqlCacheHasMissingInfo) && state.userAgreedToUpdateCache && !state.hasDeltaCapabilities)
			{
				// Init sql (list)
				groups = bom->GetGraphCache().GetUpToDateGroups<BusinessGroup>(groupIds, logger, GraphCache::PreferredMode::LIST, taskData).get();
				refreshDate = bom->GetGraphCache().GetSqlGroupsCacheLastFullRefreshDate();
			}
			else if (state.hasDeltaCapabilities)
			{
				ASSERT(state.mode == State::REFRESH || state.mode == State::SYNC);
				// Init sql (delta)
				groups = bom->GetGraphCache().GetUpToDateGroups<BusinessGroup>(groupIds, logger, GraphCache::PreferredMode::INITDELTA, taskData).get();
				refreshDate = bom->GetGraphCache().GetSqlGroupsCacheLastFullRefreshDate();
			}
			else
			{
				ASSERT(state.mode == State::REFRESH);
				// Init sql (list)
				groups = bom->GetGraphCache().GetUpToDateGroups<BusinessGroup>(groupIds, logger, GraphCache::PreferredMode::LIST, taskData).get();
				refreshDate = bom->GetGraphCache().GetSqlGroupsCacheLastFullRefreshDate();
			}
		}

		if (!lastDeltaLink)
			lastDeltaLink = bom->GetGraphCache().GetGroupsDeltaLink();

		// Load mores
		if (state.refreshMore && !groupsLoadMoreRequestInfo.empty())
		{
			ASSERT(state.mode == State::REFRESH || state.mode == State::SYNC);

			// Move load more users to another vector
			{
				// This doesn't work for obscure reasons... bug in stl?
				// instead of moving the desired element to the end, it kinda duplicates another element.... WTF
				/*auto it = std::remove_if(groups.begin(), groups.end(), [&groupsLoadMoreRequestInfo](auto& u)
					{
						return groupsLoadMoreRequestInfo.end() != std::find_if(groupsLoadMoreRequestInfo.begin(), groupsLoadMoreRequestInfo.end(), [&u](const auto& uMore) {return uMore.GetID() == u.GetID(); });
					});

				if (groups.end() != it)
				{
					std::move(it, groups.end(), std::back_inserter(groupsToDoLoadMore));
					groups.erase(it, groups.end());
				}*/

				for (const auto& lmu : groupsLoadMoreRequestInfo)
				{
					auto it = std::find_if(groups.begin(), groups.end(), [&lmu](auto& u)
						{
							return lmu.GetID() == u.GetID();
						});

					if (groups.end() != it)
					{
						if (!it->HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
						{
							std::move(it, it + 1, std::back_inserter(groupsToDoLoadMore));
							groups.erase(it);
						}
					}
					else if (!loadMoreOnlyReceivedGroups)
					{
						groupsToDoLoadMore.push_back(lmu);
					}
				}
			}

			gridLogger->SetIgnoreHttp404Errors(true);
			RunOnScopeEnd _([&gridLogger]()
				{
					gridLogger->SetIgnoreHttp404Errors(false);
				});

			groupsWithMoreLoaded = bom->MoreInfoBusinessGroups(groupsToDoLoadMore, false, taskData, lcp).GetTask().get();

			std::copy(groupsWithMoreLoaded.begin(), groupsWithMoreLoaded.end(), std::back_inserter(groups));
		}
		else
		{
			ASSERT(groupsWithMoreLoaded.empty());
			// Copy more loaded users to another vector
			std::copy_if(groups.begin(), groups.end(), std::back_inserter(groupsWithMoreLoaded), [](const auto& u)
				{
					return u.IsMoreLoaded();
				});
		}

		ensureOwnersAndCreatedOnBehalfOfAreInCache(groups, bom, taskData);

		if (state.refreshMore && !groupsLoadMoreRequestInfo.empty() && !groupsWithMoreLoaded.empty())
		{
			LoggerService::User(_T("Writing cache to disk."), taskData.GetOriginator());

			// Useless, done in GetUpToDateGroups
			//bom->GetGraphCache().SaveGroupsInSqlCache(groups, { GBI::MIN, GBI::LIST }, refreshDate, clearCache);

			// Save load more users.
			// FIXME: which blocks of data to save here?
			// MoreInfoBusinessGroups() is now not configurable, always loads: sync props + UserAdditionalRequest::RequestMailboxSettingsForNonGuest | UserAdditionalRequest::RequestDriveForNonGuest | UserAdditionalRequest::RequestManager
			// Update as soon as we make it a user choice!
			bom->GetGraphCache().SaveGroupsInSqlCache(groupsWithMoreLoaded, { GBI::MIN, GBI::LIST, GBI::SYNCV1, GBI::SYNCBETA, GBI::CREATEDONBEHALFOF, GBI::TEAM, GBI::GROUPSETTINGS, GBI::LIFECYCLEPOLICIES, GBI::OWNERS, GBI::MEMBERSCOUNT, GBI::DRIVE, GBI::ISYAMMER }, boost::none, false);
		}

		if (!ids.empty())
		{
			// Ensure requested order is preserved (probably not very efficient when many ids)
			LoggerService::User(_T("Sorting groups..."), taskData.GetOriginator());
			std::sort(groups.begin(), groups.end(), [&ids](const auto& lhGroup, const auto& rhGroup)
				{
					return ids.find(lhGroup.GetID()) < ids.find(rhGroup.GetID());
				});
		}

		YDataCallbackMessage<std::shared_ptr<vector<BusinessGroup>>>::DoPost(groupsPtr, [p_Command, gridIsEmpty, taskData, sync = state.mode == State::SYNC, p_Action, hwnd, groupsWithMoreLoaded = groupsWithMoreLoadedPtr, p_IsRefresh, isCanceled = taskData.IsCanceled(), isLoadAll = wantsAll, refreshDate, groupSettingsTemplates, tenantGroupSettings, lcp, lastDeltaLink](std::shared_ptr<vector<BusinessGroup>> p_Groups)
		{
			ASSERT(p_Groups);
			ASSERT(groupsWithMoreLoaded);
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameGroups*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(*p_Groups)))
			{
				ASSERT(nullptr != frame);
				frame->SetRefreshMode(RefreshMode::ListRequest);

				if (groupSettingsTemplates)
					frame->SetGroupSettingTemplates(groupSettingsTemplates);
				if (tenantGroupSettings)
					frame->SetTenantGroupSettings(tenantGroupSettings, false);

				try
				{
					auto& module = dynamic_cast<ModuleGroup&>(frame->GetModuleBase());
					if (lcp)
						module.SetGroupLifeCyclePolicies(*lcp);
				}
				catch (std::bad_cast&)
				{
					// dynamic_cast failed
					ASSERT(false);
				}

				frame->ShowLifecyclePolicies(false);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s groups...", Str::getStringFromNumber(p_Groups->size()).c_str()));
				frame->GetGrid().ClearLog(taskData.GetId());
				//frame->GetGrid().ClearStatus();

				LoggerService::User(_T("Loading data into grid."), taskData.GetOriginator());
				{
					CWaitCursor _;
					bool allowFullPurge = true;
					if (frame->HasLastUpdateOperations())
					{
						allowFullPurge = !sync && !isCanceled && p_IsRefresh;
						frame->ShowGroups(*p_Groups, *groupsWithMoreLoaded, frame->GetLastUpdateOperations(), true, allowFullPurge);
						frame->ForgetLastUpdateOperations();
					}
					else
					{
						allowFullPurge = !sync && !isCanceled;
						frame->ShowGroups(*p_Groups, *groupsWithMoreLoaded, {}, true, allowFullPurge);
					}
				}

				if (refreshDate)
					frame->SetLastRefreshDate(*refreshDate);

				if (!p_IsRefresh)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}

				if (lastDeltaLink)
					frame->SetGroupsDeltaLink(*lastDeltaLink);
				else
					frame->SetGroupsDeltaLink(boost::none);
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.

				frame->ProcessLicenseContext(); // Let's consume license tokens if needed
				ModuleBase::TaskFinished(frame, taskData, p_Action);
			}


			if (frame)
			{
				auto& grid = dynamic_cast<GridGroups&>(frame->GetGrid());
				if (!AutomatedApp::IsAutomationRunning() && frame->GetBusinessObjectManager()->GetGraphCache().GetAutoLoadOnPrem() && grid.CanRefreshOnPrem() && gridIsEmpty)
				{
					auto actionCmd = std::make_shared<ShowOnPremGroupsCommand>(*frame);
					CommandDispatcher::GetInstance().Execute(Command::CreateCommand(actionCmd, *frame, Command::ModuleTarget::Group));
				}
			}
		});
	});
}

void ModuleGroup::getGroupsWithIndividualRequests(const Command& p_Command, const CommandInfo& p_Info, FrameGroups* p_Frame, bool p_IsRefresh)
{
	auto taskData = addFrameTask(YtriaTranslate::Do(ModuleGroup_getGroupsWithIndividualRequests_1, _YLOC("Show Groups (subset)")).c_str(), p_Frame, p_Command, !p_IsRefresh);

	// Load Mores
	std::vector<BusinessGroup> groupsLoadMoreRequestInfo;
	GridGroups* gridGroups = dynamic_cast<GridGroups*>(&p_Frame->GetGrid());
	ASSERT(nullptr != gridGroups);
	if (nullptr != gridGroups)
	{
		std::vector<GridBackendRow*> rowsWithMoreLoaded;
		gridGroups->GetRowsWithMoreLoaded(rowsWithMoreLoaded);

		if (!rowsWithMoreLoaded.empty())
		{
			groupsLoadMoreRequestInfo = gridGroups->GetLoadMoreRequestInfo(rowsWithMoreLoaded);
			groupsLoadMoreRequestInfo.erase(std::remove_if(groupsLoadMoreRequestInfo.begin(), groupsLoadMoreRequestInfo.end(), [&ids = p_Info.GetIds()](const BusinessGroup& group) { return ids.end() == ids.find(group.GetID()); }), groupsLoadMoreRequestInfo.end());
		}
	}

	// FIXME: Ensure this is right
	const bool refreshFromSql = !p_IsRefresh;

	YSafeCreateTask([this, refreshFromSql, taskData, p_Action = p_Command.GetAutomationAction(), hwnd = p_Frame->GetSafeHwnd(), groupsLoadMoreRequestInfo, bom = p_Frame->GetBusinessObjectManager(), ids = p_Info.GetIds(), p_IsRefresh, gridLogger = p_Frame->GetGridLogger()]()
    {
		auto groupsPtr = std::make_shared<vector<BusinessGroup>>();
		auto groupsWithMoreLoadedPtr = std::make_shared<vector<BusinessGroup>>();
		auto& groupsWithMoreLoaded = *groupsWithMoreLoadedPtr;
		auto& businessGroups = *groupsPtr;

		boost::YOpt<BusinessLifeCyclePolicies> lcp = bom->GetBusinessLCP(PooledString(), std::make_shared<BasicPageRequestLogger>(_T("lifecycle policies")), taskData).GetTask().get();
		boost::YOpt<vector<BusinessGroupSettingTemplate>> groupSettingsTemplates = !taskData.IsCanceled() ? boost::YOpt<vector<BusinessGroupSettingTemplate>>(bom->GetBusinessGroupSettingTemplates(taskData).GetTask().get()) : boost::none;
		boost::YOpt<vector<BusinessGroupSetting>> tenantGroupSettings = !taskData.IsCanceled() ? boost::YOpt<vector<BusinessGroupSetting>>(bom->GetBusinessGroupSettings(taskData).GetTask().get()) : boost::none;

		vector<BusinessGroup> groupsToDoLoadMore = groupsLoadMoreRequestInfo;
		vector<BusinessGroup> groupsToDoNoLoadMore;
		bool allRefreshedFromSql = refreshFromSql;
		auto groupIds = ids;

		for (auto& id : groupIds)
		{
			if (groupsLoadMoreRequestInfo.end() == std::find_if(groupsLoadMoreRequestInfo.begin(), groupsLoadMoreRequestInfo.end(), [id](const BusinessGroup& group) { return group.GetID() == id; }))
			{
				groupsToDoNoLoadMore.emplace_back();
				groupsToDoNoLoadMore.back().m_Id = id;
			}
		}

		if (refreshFromSql)
		{
			const auto sqlCachedGroups = bom->GetGraphCache().GetSqlCachedGroups(hwnd, groupIds, { GBI::MIN, GBI::LIST }, true, taskData);
			businessGroups.reserve(groupIds.size());
			for (auto& group : sqlCachedGroups)
			{
				const auto groupIt = groupIds.find(group.GetID());
				ASSERT(groupIds.end() != groupIt);
				if (groupIds.end() != groupIt)
				{
					groupIds.erase(groupIt);
					businessGroups.emplace_back(group);

					{
						auto it = std::find_if(groupsToDoNoLoadMore.cbegin(), groupsToDoNoLoadMore.cend(), [&group](const auto& p_Group)
						{
							return group.GetID() == p_Group.GetID();
						});

						if (groupsToDoNoLoadMore.end() != it)
							groupsToDoNoLoadMore.erase(it);
					}

					if (group.IsMoreLoaded())
					{
						groupsWithMoreLoaded.emplace_back(group);
						auto it = std::find_if(groupsToDoLoadMore.cbegin(), groupsToDoLoadMore.cend(), [&group](const auto& p_Group)
						{
							return p_Group.GetID() == group.GetID();
						});

						if (groupsToDoLoadMore.end() != it)
							groupsToDoLoadMore.erase(it);
					}
				}

				if (groupIds.empty())
					break;
			}

			ensureOwnersAndCreatedOnBehalfOfAreInCache(groupsWithMoreLoaded, bom, taskData);
			allRefreshedFromSql = groupsToDoLoadMore.empty() && groupsToDoNoLoadMore.empty();
		}

		if (!groupsToDoNoLoadMore.empty())
		{
			auto bGroups = bom->GetBusinessGroupsIndividualRequests(groupsToDoNoLoadMore, taskData).GetTask().get();
			std::copy(std::make_move_iterator(std::begin(bGroups)), std::make_move_iterator(std::end(bGroups)), std::back_inserter(businessGroups));
		}

		// Load mores
		if (!groupsLoadMoreRequestInfo.empty())
		{
			if (taskData.IsCanceled())
			{
				for (auto& u : groupsToDoLoadMore)
				{
					groupsWithMoreLoaded.emplace_back(u);
					groupsWithMoreLoaded.back().SetFlags(BusinessObject::CANCELED);
					businessGroups.emplace_back(groupsWithMoreLoaded.back());
				}
			}
			else if (!groupsToDoLoadMore.empty()) // If everything's loaded from cache, this vector is empty
			{
				ASSERT(!allRefreshedFromSql);

				gridLogger->SetIgnoreHttp404Errors(true);
				RunOnScopeEnd _([&gridLogger]()
					{
						gridLogger->SetIgnoreHttp404Errors(false);
					});

				auto moreLoadedGroups = bom->MoreInfoBusinessGroups(groupsToDoLoadMore, false, taskData, lcp).GetTask().get();
				ensureOwnersAndCreatedOnBehalfOfAreInCache(moreLoadedGroups, bom, taskData);
				std::copy(std::begin(moreLoadedGroups), std::end(moreLoadedGroups), std::back_inserter(businessGroups));
				std::copy(std::make_move_iterator(std::begin(moreLoadedGroups)), std::make_move_iterator(std::end(moreLoadedGroups)), std::back_inserter(groupsWithMoreLoaded));
			}

			if (!allRefreshedFromSql)
			{
				// FIXME: need to save here?
				bom->GetGraphCache().SaveGroupsInSqlCache(businessGroups, { GBI::MIN, GBI::LIST }, boost::none, false);
				// FIXME: which blocks of data to save here?
				// MoreInfoBusinessUsers() is now not configurable, always loads: sync props + UserAdditionalRequest::RequestMailboxSettingsForNonGuest | UserAdditionalRequest::RequestDriveForNonGuest | UserAdditionalRequest::RequestManager
				// Update as soon as we make it a user choice!
				bom->GetGraphCache().SaveGroupsInSqlCache(groupsWithMoreLoaded, { GBI::MIN, GBI::LIST, GBI::SYNCV1, GBI::SYNCBETA, GBI::CREATEDONBEHALFOF, GBI::TEAM, GBI::GROUPSETTINGS, GBI::LIFECYCLEPOLICIES, GBI::OWNERS, GBI::MEMBERSCOUNT, GBI::DRIVE, GBI::ISYAMMER }, boost::none, false);
			}

			ASSERT(groupsWithMoreLoaded.size() == groupsLoadMoreRequestInfo.size());
		}

		// Ensure requested order is preserved (probably not very efficient when many ids)
		LoggerService::User(_T("Sorting groups..."), taskData.GetOriginator());
		std::sort(businessGroups.begin(), businessGroups.end(), [&ids](const BusinessGroup& lhGroup, const BusinessGroup& rhGroup)
			{
				return ids.find(lhGroup.GetID()) < ids.find(rhGroup.GetID());
			});

		YDataCallbackMessage<std::shared_ptr<vector<BusinessGroup>>>::DoPost(groupsPtr, [taskData, p_Action, hwnd, groupsWithMoreLoaded = groupsWithMoreLoadedPtr, groupSettingsTemplates, tenantGroupSettings, lcp, p_IsRefresh, isCanceled = taskData.IsCanceled()](const std::shared_ptr<vector<BusinessGroup>>& p_Groups)
        {
			ASSERT(p_Groups);
			ASSERT(groupsWithMoreLoaded);
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameGroups*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(*p_Groups)))
            {
				ASSERT(nullptr != frame);
                frame->SetRefreshMode(RefreshMode::IndividualRequest);
				if (groupSettingsTemplates)
					frame->SetGroupSettingTemplates(groupSettingsTemplates);
				if (tenantGroupSettings)
					frame->SetTenantGroupSettings(tenantGroupSettings, false);

				try
				{
					auto& module = dynamic_cast<ModuleGroup&>(frame->GetModuleBase());
					if (lcp)
						module.SetGroupLifeCyclePolicies(*lcp);
				}
				catch (std::bad_cast&)
				{
					// dynamic_cast failed
					ASSERT(false);
				}

				frame->ShowLifecyclePolicies(false);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s groups...", Str::getStringFromNumber(p_Groups->size()).c_str()));
                frame->GetGrid().ClearLog(taskData.GetId());
				//frame->GetGrid().ClearStatus();

				{
					CWaitCursor _;
					if (frame->HasLastUpdateOperations())
					{
						frame->ShowGroups(*p_Groups, *groupsWithMoreLoaded, frame->GetLastUpdateOperations(), true, !isCanceled && p_IsRefresh);
						frame->ForgetLastUpdateOperations();
					}
					else
					{
						frame->ShowGroups(*p_Groups, *groupsWithMoreLoaded, {}, true, !isCanceled && p_IsRefresh);
					}
				}

				if (!p_IsRefresh)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}
            }

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, taskData, p_Action);
			}
        });       
    });
}

void ModuleGroup::ensureOwnersAndCreatedOnBehalfOfAreInCache(vector<BusinessGroup>& p_BusinessGroups, const std::shared_ptr<BusinessObjectManager>& p_BOM, YtriaTaskData p_TaskData)
{
	// Should we delta-sync?
	//p_BOM->GetGraphCache().GetUpToDateUsers<CachedUser>()

	O365IdsContainer relatedUserIds;
	for (auto& group : p_BusinessGroups)
	{
		if (group.GetUserCreatedOnBehalfOf()
			&& *group.GetUserCreatedOnBehalfOf() != p_BOM->GetGraphCache().GetUserInCache(*group.GetUserCreatedOnBehalfOf(), false).GetID())
			relatedUserIds.insert(*group.GetUserCreatedOnBehalfOf());

		for (auto& owner : group.GetOwnerUsers())
		{
			if (owner != p_BOM->GetGraphCache().GetUserInCache(owner, false).GetID())
				relatedUserIds.insert(owner);
		}
	}

	if (!relatedUserIds.empty())
	{
		// Do not pass p_TaskData if we don't want to allow cancellation of this step.
		p_BOM->GetGraphCache().GetSqlCachedUsers(p_TaskData.GetOriginator(), relatedUserIds, false, p_TaskData);

		// FIXME: This logger creates unwanted display
		//auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("related user"), relatedUserIds.size());
		auto logger = std::make_shared<BasicRequestLogger>(_T("related user"));

		// FIXME: Should we use delta sync here?
		// Ensure no group is missing...
		for (auto& id : relatedUserIds)
		{
			BusinessUser bu;
			bu.SetID(id);
			p_BOM->GetGraphCache().SyncUncachedUser(bu, logger, p_TaskData, false);
		}
		ASSERT(std::all_of(relatedUserIds.begin(), relatedUserIds.end(), [&p_BOM](const auto& id) { return id == p_BOM->GetGraphCache().GetUserInCache(id, false).GetID(); }));
	}
}

const wstring& ModuleGroup::getTaskname(BusinessObject::UPDATE_TYPE p_UpdateType)
{
	switch (p_UpdateType)
	{
	case BusinessObject::UPDATE_TYPE_ADDMEMBERS:
	case BusinessObject::UPDATE_TYPE_DELETEMEMBERS:
		{
			static wstring taskName = YtriaTranslate::Do(ModuleGroup_getTaskname_1, _YLOC("Updating group members.")).c_str();
			return taskName;
		}
	case BusinessObject::UPDATE_TYPE_DELETEOWNERS:
	case BusinessObject::UPDATE_TYPE_ADDOWNERS:
		{
			static wstring taskName = YtriaTranslate::Do(ModuleGroup_getTaskname_2, _YLOC("Updating group owners.")).c_str();
			return taskName;
		}
	case BusinessObject::UPDATE_TYPE_ADDAUTHORACCEPTED:
	case BusinessObject::UPDATE_TYPE_ADDAUTHORREJECTED:
	case BusinessObject::UPDATE_TYPE_DELETEACCEPTEDAUTHORS:
	case BusinessObject::UPDATE_TYPE_DELETEREJECTEDAUTHORS:
	//case BusinessObject::UPDATE_TYPE_DELETEAUTHORIZATION:
		{
			static wstring taskName = YtriaTranslate::Do(ModuleGroup_getTaskname_3, _YLOC("Updating group members' authorizations.")).c_str();
			return taskName;
		}
	default:
		ASSERT(false);
		break;
	}

	static wstring taskName = YtriaTranslate::Do(ModuleGroup_getTaskname_4, _YLOC("Unknown task [ERROR].")).c_str();
	return taskName;
}
