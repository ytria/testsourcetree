#include "FrameGroupCommon.h"

#include "BusinessGroup.h"
#include "CommandDispatcher.h"
#include "GridGroupsCommon.h"
#include "GridUpdater.h"

IMPLEMENT_DYNAMIC(FrameGroupCommon, GridFrameBase)

FrameGroupCommon::FrameGroupCommon(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
    m_CreateAddRemoveToSelection = true;
    m_CreateRefreshPartial = true;
	m_CreateShowContext = true;
}

void FrameGroupCommon::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameGroupCommon::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameGroupCommon::ApplySpecific(bool p_Selected)
{
    GridGroupsCommon* pGrid = dynamic_cast<GridGroupsCommon*>(&GetGrid());
    ASSERT(nullptr != pGrid);

    CommandInfo info;
    info.Data() = pGrid->GetGroupChanges(p_Selected, true);
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateGroupItems, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), GetAutomationAction() }));
}

void FrameGroupCommon::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	ASSERT(Origin::Group == GetModuleCriteria().m_Origin);
	ASSERT(ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer);

	CommandInfo info;
	info.SetOrigin(GetModuleCriteria().m_Origin);
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	if (nullptr != m_Grid && HasLastUpdateOperations())
		GridUpdater::HandleCreatedModifications(*m_Grid, AccessLastUpdateOperations());

	if (HasLastUpdateOperations() && !ShouldForceFullRefreshAfterApply())
    {
		O365IdsContainer ids;
        for (const auto& updateOp : GetLastUpdateOperations())
        {
			if (nullptr != m_Grid)
			{
				const auto& field = updateOp.GetPkField(m_Grid->GetColMetaID());
				if (field.IsMulti())
				{
					auto vect = field.GetValuesMulti<PooledString>();
					ASSERT(nullptr != vect);
					if (nullptr != vect)
					{
						// In case of group hierarchy, the direct parent is the last id in multivalue
						ids.insert(vect->back());
					}
				}
				else
				{
					ids.insert(field.GetValueStr());
				}
			}
        }

		ASSERT(ids.end() == ids.find(_YTEXT("")));

        info.GetIds() = ids;
    }
    else
    {
        info.GetIds() = GetModuleCriteria().m_IDs;
    }


	if (GetGrid().GetAutomationName() == GridUtil::g_AutoNameGroupsHierarchy)
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::ListSubItems, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
	else if (GetGrid().GetAutomationName() == GridUtil::g_AutoNameGroupsOwnersHierarchy)
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::ListOwners, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
	else if (GetGrid().GetAutomationName() == GridUtil::g_AutoNameGroupsAuthorsHierarchy)
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::ListAuthors, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
	else
		ASSERT(FALSE);
}

void FrameGroupCommon::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

    if (GetGrid().GetAutomationName() == GridUtil::g_AutoNameGroupsHierarchy)
        CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::ListSubItems, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
    else if (GetGrid().GetAutomationName() == GridUtil::g_AutoNameGroupsOwnersHierarchy)
        CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::ListOwners, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
    else if (GetGrid().GetAutomationName() == GridUtil::g_AutoNameGroupsAuthorsHierarchy)
        CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::ListAuthors, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
    else
        ASSERT(FALSE);
}

GridFrameBase::O365ControlConfig FrameGroupCommon::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigGroups();
}

GridFrameBase::O365ControlConfig FrameGroupCommon::GetApplyPartialControlConfig()
{
	return GetApplyPartialControlConfigGroups();
}

AutomatedApp::AUTOMATIONSTATUS FrameGroupCommon::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	return statusRV;
}

void FrameGroupCommon::SetGrid(GridGroupsCommon* p_Grid)
{
    m_Grid = p_Grid;
}
