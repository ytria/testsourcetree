#include "DynamicGridGroups.h"

#include "GridUtil.h"

DynamicGridGroups::DynamicGridGroups()
{
	GridUtil::Add365ObjectTypes(*this);
	m_renderer.SetConfiguration(std::make_unique<RendererConfiguration>());
}
