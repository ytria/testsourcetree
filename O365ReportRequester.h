#pragma once

#include "IRequester.h"
#include "O365ReportCriteria.h"
#include "YTimeDate.h"

class SingleRequestResult;

class O365ReportRequester : public IRequester
{
public:
	O365ReportRequester(const wstring& p_ReportApiCall, const boost::YOpt<O365ReportCriteria>& p_Criteria, size_t p_Index);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const wstring& GetReportFilePath() const;
	const wstring& GetReportOriginalFileName() const;
	const boost::YOpt<YTimeDate>& GetReportDate() const;
	const SingleRequestResult& GetResult() const;

	bool CouldwriteMetadata() const;

private:
	bool IsPeriod() const;

	wstring m_OutputFilePath;
	wstring m_OriginalFileName;
	bool m_MetadataWritten;
	const wstring m_ReportApiCall;
	boost::YOpt<O365ReportCriteria> m_Criteria;
	size_t m_Index;

	std::shared_ptr<SingleRequestResult> m_Result;
};