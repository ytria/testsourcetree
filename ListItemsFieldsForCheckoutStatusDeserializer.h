#pragma once

#include "CheckoutStatus.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class ListItemsFieldsForCheckoutStatusDeserializer : public JsonObjectDeserializer, public Encapsulate<CheckoutStatus>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};
