#include "BusinessEvent.h"

#include "Event.h"
#include "MSGraphCommonData.h"
#include "MsGraphHttpRequestLogger.h"
#include "O365AdminUtil.h"
#include "safeTaskCall.h"
#include "Sapio365Session.h"
#include "TimeUtil.h"

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessEvent>("Event") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Event"))))
		.constructor()(policy::ctor::as_object)
		//.property("attendees", &BusinessEvent::m_Attendees)
		//.property("body", &BusinessEvent::m_Body)
		.property("bodyPreview", &BusinessEvent::m_BodyPreview)
		.property("categories", &BusinessEvent::m_Categories)
		.property("changeKey", &BusinessEvent::m_ChangeKey)
		.property("createdDateTime", &BusinessEvent::m_CreatedDateTime)
		//.property("end", &BusinessEvent::m_End)
		.property("hasAttachments", &BusinessEvent::m_HasAttachments)
		.property("iCalUId", &BusinessEvent::m_ICalUId)
		//.property("id", &BusinessEvent::m_Id)
		.property("importance", &BusinessEvent::m_Importance)
		.property("isAllDay", &BusinessEvent::m_IsAllDay)
		.property("isCancelled", &BusinessEvent::m_IsCancelled)
		.property("isOrganizer", &BusinessEvent::m_IsOrganizer)
		.property("isReminderOn", &BusinessEvent::m_IsReminderOn)
		.property("lastModifiedDateTime", &BusinessEvent::m_LastModifiedDateTime)
		//.property("location", &BusinessEvent::m_Location)
		.property("onlineMeetingUrl", &BusinessEvent::m_OnlineMeetingUrl)
		//.property("organizer", &BusinessEvent::m_Organizer)
		.property("originalEndTimeZone", &BusinessEvent::m_OriginalEndTimeZone)
		.property("originalStart", &BusinessEvent::m_OriginalStart)
		.property("originalStartTimeZone", &BusinessEvent::m_OriginalStartTimeZone)
		//.property("recurrence", &BusinessEvent::m_Recurrence)
		.property("reminderMinutesBeforeStart", &BusinessEvent::m_ReminderMinutesBeforeStart)
		.property("responseRequested", &BusinessEvent::m_ResponseRequested)
		//.property("responseStatus", &BusinessEvent::m_ResponseStatus)
		.property("sensitivity", &BusinessEvent::m_Sensitivity)
		.property("seriesMasterId", &BusinessEvent::m_SeriesMasterId)
		.property("showAs", &BusinessEvent::m_ShowAs)
		//.property("start", &BusinessEvent::m_Start)
		.property("subject", &BusinessEvent::m_Subject)
		.property("type", &BusinessEvent::m_Type)
		.property("webLink", &BusinessEvent::m_WebLink)
		;
}

BusinessEvent::BusinessEvent(const Event & p_Event)
{
    SetAttendees(p_Event.Attendees);
    SetBody(p_Event.Body);
    SetBodyPreview(p_Event.BodyPreview);
    SetCategories(p_Event.Categories);
    SetChangeKey(p_Event.ChangeKey);
    SetCreatedDateTime(p_Event.CreatedDateTime);
    SetEnd(p_Event.End);
    SetHasAttachments(p_Event.HasAttachments);
    SetICalUId(p_Event.ICalUId);
    SetID(p_Event.Id);
    SetImportance(p_Event.Importance);
    SetIsAllDay(p_Event.IsAllDay);
    SetIsCancelled(p_Event.IsCancelled);
    SetIsOrganizer(p_Event.IsOrganizer);
    SetIsReminderOn(p_Event.IsReminderOn);
    SetLastModifiedDateTime(p_Event.LastModifiedDateTime);
    SetLocation(p_Event.Location);
    SetOnlineMeetingUrl(p_Event.OnlineMeetingUrl);
    SetOrganizer(p_Event.Organizer);
    SetOriginalEndTimeZone(p_Event.OriginalEndTimeZone);
    SetOriginalStart(p_Event.OriginalStart);
    SetOriginalStartTimeZone(p_Event.OriginalStartTimeZone);
    SetRecurrence(p_Event.Recurrence);
    SetReminderMinutesBeforeStart(p_Event.ReminderMinutesBeforeStart);
    SetResponseRequested(p_Event.ResponseRequested);
    SetResponseStatus(p_Event.ResponseStatus);
    SetSensitivity(p_Event.Sensitivity);
    SetSeriesMasterId(p_Event.SeriesMasterId);
    SetShowAs(p_Event.ShowAs);
    SetStart(p_Event.Start);
    SetSubject(p_Event.Subject);
    SetType(p_Event.Type);
    SetWebLink(p_Event.WebLink);
}

const vector<Attendee>& BusinessEvent::GetAttendees() const
{
    return m_Attendees;
}

const boost::YOpt<ItemBody>& BusinessEvent::GetBody() const
{
    return m_Body;
}

const boost::YOpt<PooledString>& BusinessEvent::GetBodyPreview() const
{
    return m_BodyPreview;
}

const boost::YOpt<vector<PooledString>>& BusinessEvent::GetCategories() const
{
    return m_Categories;
}

const boost::YOpt<PooledString>& BusinessEvent::GetChangeKey() const
{
    return m_ChangeKey;
}

const boost::YOpt<YTimeDate>& BusinessEvent::GetCreatedDateTime() const
{
    return m_CreatedDateTime;
}

const boost::YOpt<DateTimeTimeZone>& BusinessEvent::GetEnd() const
{
    return m_End;
}

const boost::YOpt<bool>& BusinessEvent::GetHasAttachments() const
{
    return m_HasAttachments;
}

const boost::YOpt<PooledString>& BusinessEvent::GetICalUId() const
{
    return m_ICalUId;
}

const boost::YOpt<PooledString>& BusinessEvent::GetImportance() const
{
    return m_Importance;
}

const boost::YOpt<bool>& BusinessEvent::GetIsAllDay() const
{
    return m_IsAllDay;
}

const boost::YOpt<bool>& BusinessEvent::GetIsCancelled() const
{
    return m_IsCancelled;
}

const boost::YOpt<bool>& BusinessEvent::GetIsOrganizer() const
{
    return m_IsOrganizer;
}

const boost::YOpt<bool>& BusinessEvent::GetIsReminderOn() const
{
    return m_IsReminderOn;
}

const boost::YOpt<YTimeDate>& BusinessEvent::GetLastModifiedDateTime() const
{
    return m_LastModifiedDateTime;
}

const boost::YOpt<Location>& BusinessEvent::GetLocation() const
{
    return m_Location;
}

const boost::YOpt<PooledString>& BusinessEvent::GetOnlineMeetingUrl() const
{
    return m_OnlineMeetingUrl;
}

const boost::YOpt<Recipient>& BusinessEvent::GetOrganizer() const
{
    return m_Organizer;
}

const boost::YOpt<PooledString>& BusinessEvent::GetOriginalEndTimeZone() const
{
    return m_OriginalEndTimeZone;
}

const boost::YOpt<YTimeDate>& BusinessEvent::GetOriginalStart() const
{
    return m_OriginalStart;
}

const boost::YOpt<PooledString>& BusinessEvent::GetOriginalStartTimeZone() const
{
    return m_OriginalStartTimeZone;
}

const boost::YOpt<PatternedRecurrence>& BusinessEvent::GetRecurrence() const
{
    return m_Recurrence;
}

const boost::YOpt<int32_t>& BusinessEvent::GetReminderMinutesBeforeStart() const
{
    return m_ReminderMinutesBeforeStart;
}

const boost::YOpt<bool>& BusinessEvent::GetResponseRequested() const
{
    return m_ResponseRequested;
}

const boost::YOpt<ResponseStatus>& BusinessEvent::GetResponseStatus() const
{
    return m_ResponseStatus;
}

const boost::YOpt<PooledString>& BusinessEvent::GetSensitivity() const
{
    return m_Sensitivity;
}

const boost::YOpt<PooledString>& BusinessEvent::GetSeriesMasterId() const
{
    return m_SeriesMasterId;
}

const boost::YOpt<PooledString>& BusinessEvent::GetShowAs() const
{
    return m_ShowAs;
}

const boost::YOpt<DateTimeTimeZone>& BusinessEvent::GetStart() const
{
    return m_Start;
}

const boost::YOpt<PooledString>& BusinessEvent::GetSubject() const
{
    return m_Subject;
}

const boost::YOpt<PooledString>& BusinessEvent::GetType() const
{
    return m_Type;
}

const boost::YOpt<PooledString>& BusinessEvent::GetWebLink() const
{
    return m_WebLink;
}

void BusinessEvent::SetAttendees(vector<Attendee> p_Attendees)
{
    for (auto& attendee : p_Attendees)
    {
        if (attendee.Status && attendee.Status->Time && TimeUtil::IsNullDate(*attendee.Status->Time))
            attendee.Status->Time = boost::none;
    }
    m_Attendees = p_Attendees;
}

void BusinessEvent::SetBody(const boost::YOpt<ItemBody>& p_Body)
{
    m_Body = p_Body;
}

void BusinessEvent::SetBodyPreview(const boost::YOpt<PooledString>& p_BodyPreview)
{
    m_BodyPreview = p_BodyPreview;
}

void BusinessEvent::SetCategories(const boost::YOpt<vector<PooledString>>& p_Categories)
{
    m_Categories = p_Categories;
}

void BusinessEvent::SetChangeKey(const boost::YOpt<PooledString>& p_ChangeKey)
{
    m_ChangeKey = p_ChangeKey;
}

void BusinessEvent::SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_CreatedDateTime)
{
    m_CreatedDateTime = p_CreatedDateTime;
}

void BusinessEvent::SetCreatedDateTime(const boost::YOpt<PooledString>& p_CreatedDateTime)
{
    if (boost::none != p_CreatedDateTime)
    {
        YTimeDate timeDate;
        if (TimeUtil::GetInstance().ConvertTextToDate(*p_CreatedDateTime, timeDate))
            m_CreatedDateTime = timeDate;
    }
}

void BusinessEvent::SetEnd(const boost::YOpt<DateTimeTimeZone>& p_End)
{
    m_End = p_End;
}

void BusinessEvent::SetHasAttachments(const boost::YOpt<bool>& p_HasAttachments)
{
    m_HasAttachments = p_HasAttachments;
}

void BusinessEvent::SetICalUId(const boost::YOpt<PooledString>& p_ICalUId)
{
    m_ICalUId = p_ICalUId;
}

void BusinessEvent::SetImportance(const boost::YOpt<PooledString>& p_Importance)
{
    m_Importance = p_Importance;
}

void BusinessEvent::SetIsAllDay(const boost::YOpt<bool>& p_IsAllDay)
{
    m_IsAllDay = p_IsAllDay;
}

void BusinessEvent::SetIsCancelled(const boost::YOpt<bool>& p_IsCancelled)
{
    m_IsCancelled = p_IsCancelled;
}

void BusinessEvent::SetIsOrganizer(const boost::YOpt<bool>& p_IsOrganizer)
{
    m_IsOrganizer = p_IsOrganizer;
}

void BusinessEvent::SetIsReminderOn(const boost::YOpt<bool>& p_IsReminderOn)
{
    m_IsReminderOn = p_IsReminderOn;
}

void BusinessEvent::SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_LastModifiedDateTime)
{
    m_LastModifiedDateTime = p_LastModifiedDateTime;
}

void BusinessEvent::SetLastModifiedDateTime(const boost::YOpt<PooledString>& p_LastModifiedDateTime)
{
    if (boost::none != p_LastModifiedDateTime)
    {
        YTimeDate timeDate;
        if (TimeUtil::GetInstance().ConvertTextToDate(*p_LastModifiedDateTime, timeDate))
            m_LastModifiedDateTime = timeDate;
    }
}

void BusinessEvent::SetLocation(const boost::YOpt<Location>& p_Location)
{
    m_Location = p_Location;
}

void BusinessEvent::SetOnlineMeetingUrl(const boost::YOpt<PooledString>& p_OnlineMeetingUrl)
{
    m_OnlineMeetingUrl = p_OnlineMeetingUrl;
}

void BusinessEvent::SetOrganizer(const boost::YOpt<Recipient>& p_Organizer)
{
    m_Organizer = p_Organizer;
}

void BusinessEvent::SetOriginalEndTimeZone(const boost::YOpt<PooledString>& p_OriginalEndTimeZone)
{
    m_OriginalEndTimeZone = p_OriginalEndTimeZone;
}

void BusinessEvent::SetOriginalStart(const boost::YOpt<YTimeDate>& p_OriginalStart)
{
    m_OriginalStart = p_OriginalStart;
}

void BusinessEvent::SetOriginalStart(const boost::YOpt<PooledString>& p_OriginalStart)
{
    if (boost::none != p_OriginalStart)
    {
        YTimeDate timeDate;
        if (TimeUtil::GetInstance().ConvertTextToDate(*p_OriginalStart, timeDate))
            m_OriginalStart = timeDate;
    }
}

void BusinessEvent::SetOriginalStartTimeZone(const boost::YOpt<PooledString>& p_OriginalStartTimeZone)
{
    m_OriginalStartTimeZone = p_OriginalStartTimeZone;
}

void BusinessEvent::SetRecurrence(const boost::YOpt<PatternedRecurrence>& p_Recurrence)
{
    m_Recurrence = p_Recurrence;
}

void BusinessEvent::SetReminderMinutesBeforeStart(const boost::YOpt<int32_t>& p_ReminderMinutesBeforeStart)
{
    m_ReminderMinutesBeforeStart = p_ReminderMinutesBeforeStart;
}

void BusinessEvent::SetResponseRequested(const boost::YOpt<bool>& p_ResponseRequested)
{
    m_ResponseRequested = p_ResponseRequested;
}

void BusinessEvent::SetResponseStatus(boost::YOpt<ResponseStatus> p_ResponseStatus)
{
    if (p_ResponseStatus->Response && p_ResponseStatus->Time && TimeUtil::IsNullDate(*p_ResponseStatus->Time))
        p_ResponseStatus->Time = boost::none;
    m_ResponseStatus = p_ResponseStatus;
}

void BusinessEvent::SetSensitivity(const boost::YOpt<PooledString>& p_Sensitivity)
{
    m_Sensitivity = p_Sensitivity;
}

void BusinessEvent::SetSeriesMasterId(const boost::YOpt<PooledString>& p_SeriesMasterId)
{
    m_SeriesMasterId = p_SeriesMasterId;
}

void BusinessEvent::SetShowAs(const boost::YOpt<PooledString>& p_ShowAs)
{
    m_ShowAs = p_ShowAs;
}

void BusinessEvent::SetStart(const boost::YOpt<DateTimeTimeZone>& p_Start)
{
    m_Start = p_Start;
}

void BusinessEvent::SetSubject(const boost::YOpt<PooledString>& p_Subject)
{
    m_Subject = p_Subject;
}

void BusinessEvent::SetType(const boost::YOpt<PooledString>& p_Type)
{
    m_Type = p_Type;
}

void BusinessEvent::SetWebLink(const boost::YOpt<PooledString>& p_WebLink)
{
    m_WebLink = p_WebLink;
}

void BusinessEvent::SetOwnerId(const boost::YOpt<PooledString>& p_OwnerId, bool p_IsUser)
{
    if (p_IsUser)
    {
        m_UserOwnerId = p_OwnerId;
        m_GroupOwnerId = boost::none;
    }
    else
    {
        m_UserOwnerId = boost::none;
        m_GroupOwnerId = p_OwnerId;
    }
}

Event BusinessEvent::ToEvent(const BusinessEvent& p_BusinessEvent)
{
	Event event;

	event.Attendees = p_BusinessEvent.m_Attendees;
	event.Body = p_BusinessEvent.m_Body;
	event.BodyPreview = p_BusinessEvent.m_BodyPreview;
	event.Categories = p_BusinessEvent.m_Categories;
	event.ChangeKey = p_BusinessEvent.m_ChangeKey;
	event.CreatedDateTime = p_BusinessEvent.m_CreatedDateTime;
	event.End = p_BusinessEvent.m_End;
	event.HasAttachments = p_BusinessEvent.m_HasAttachments;
	event.ICalUId = p_BusinessEvent.m_ICalUId;
	event.Id = p_BusinessEvent.m_Id;
	event.Importance = p_BusinessEvent.m_Importance;
	event.IsAllDay = p_BusinessEvent.m_IsAllDay;
	event.IsCancelled = p_BusinessEvent.m_IsCancelled;
	event.IsOrganizer = p_BusinessEvent.m_IsOrganizer;
	event.IsReminderOn = p_BusinessEvent.m_IsReminderOn;
	event.LastModifiedDateTime = p_BusinessEvent.m_LastModifiedDateTime;
	event.Location = p_BusinessEvent.m_Location;
	event.OnlineMeetingUrl = p_BusinessEvent.m_OnlineMeetingUrl;
	event.Organizer = p_BusinessEvent.m_Organizer;
	event.OriginalEndTimeZone = p_BusinessEvent.m_OriginalEndTimeZone;
	event.OriginalStart = p_BusinessEvent.m_OriginalStart;
	event.OriginalStartTimeZone = p_BusinessEvent.m_OriginalStartTimeZone;
	event.Recurrence = p_BusinessEvent.m_Recurrence;
	event.ReminderMinutesBeforeStart = p_BusinessEvent.m_ReminderMinutesBeforeStart;
	event.ResponseRequested = p_BusinessEvent.m_ResponseRequested;
	event.ResponseStatus = p_BusinessEvent.m_ResponseStatus;
	event.Sensitivity = p_BusinessEvent.m_Sensitivity;
	event.SeriesMasterId = p_BusinessEvent.m_SeriesMasterId;
	event.ShowAs = p_BusinessEvent.m_ShowAs;
	event.Start = p_BusinessEvent.m_Start;
	event.Subject = p_BusinessEvent.m_Subject;
	event.Type = p_BusinessEvent.m_Type;
	event.WebLink = p_BusinessEvent.m_WebLink;

	return event;
}

TaskWrapper<HttpResultWithError> BusinessEvent::SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
    ASSERT(m_UserOwnerId || m_GroupOwnerId);
	auto event = BusinessEvent::ToEvent(*this);

	web::uri_builder uri;
    if (m_UserOwnerId)
	{
		uri.append_path(Rest::USERS_PATH);
		uri.append_path(*m_UserOwnerId);
	}
    else
	{
		uri.append_path(Rest::GROUPS_PATH);
		uri.append_path(*m_GroupOwnerId);
	}
	uri.append_path(Rest::EVENTS_PATH);
	uri.append_path(m_Id);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(m_UserOwnerId ? Sapio365Session::APP_SESSION : Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
    auto session = p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType());
	return safeTaskCall(session->Delete(uri.to_uri(), httpLogger, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
		}), session, Util::WriteErrorHandler, p_TaskData);
}
