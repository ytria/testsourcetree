#pragma once

#include "GridFrameBase.h"
#include "GridSiteRoles.h"

#include "SpUser.h"

class FrameSiteRoles : public GridFrameBase
{
public:
	using GridFrameBase::GridFrameBase;
    DECLARE_DYNAMIC(FrameSiteRoles)
    virtual ~FrameSiteRoles() = default;

	void BuildView(const O365DataMap<BusinessSite, vector<Sp::RoleDefinition>>& p_SpSites, bool p_FullPurge);

    O365Grid& GetGrid() override;
    void ApplySelectedSpecific() override;
    void ApplyAllSpecific() override;

    void ApplySpecific(bool p_Selected);

    void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;
    bool HasApplyButton() override;

protected:
    virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

    // returns false if no frame specific tab is needed.
    bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

    AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
    GridSiteRoles m_Grid;
};

