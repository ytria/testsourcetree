#pragma once

#include "BusinessUser.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "RoleDelegationTagger.h"

class UserDeserializer	: public JsonObjectDeserializer
						, public Encapsulate<BusinessUser>
						, public RoleDelegationTagger
{
public: 
	UserDeserializer(std::shared_ptr<const Sapio365Session> p_Session);

#ifdef _DEBUG
	// ctor only used by DirectoryObjectVariantDeserializer and WholeUserDeserializer
	// to avoid assert when some properties are missing.
	UserDeserializer(std::shared_ptr<const Sapio365Session> p_Session, bool p_NoSelectUsed);
#endif

    virtual void DeserializeObject(const web::json::object& p_Object) override;

private:
#ifdef _DEBUG
	bool m_NoSelectUsed = false;
#endif

	friend class UserDeserializerFactory;
	UserDeserializer();
};