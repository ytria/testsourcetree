#pragma once

#include "Modification.h"

class IModificationWithRequestFeedbackProvider
{
public:
	virtual ~IModificationWithRequestFeedbackProvider() = default;
	virtual vector<const Modification*> Provide() = 0;
};

