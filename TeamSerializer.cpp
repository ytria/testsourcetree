#include "TeamSerializer.h"

#include "JsonSerializeUtil.h"
#include "NullableSerializer.h"
#include "TeamMemberSettingsSerializer.h"
#include "TeamGuestSettingsSerializer.h"
#include "TeamMessagingSettingsSerializer.h"
#include "TeamFunSettingsSerializer.h"

TeamSerializer::TeamSerializer(const Team& p_Team)
    : m_Team(p_Team)
{
}

void TeamSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeAny(_YTEXT("memberSettings"), NullableSerializer<TeamMemberSettings, TeamMemberSettingsSerializer>(m_Team.MemberSettings), obj);
    JsonSerializeUtil::SerializeAny(_YTEXT("guestSettings"), NullableSerializer<TeamGuestSettings, TeamGuestSettingsSerializer>(m_Team.GuestSettings), obj);
    JsonSerializeUtil::SerializeAny(_YTEXT("messagingSettings"), NullableSerializer<TeamMessagingSettings, TeamMessagingSettingsSerializer>(m_Team.MessagingSettings), obj);
    JsonSerializeUtil::SerializeAny(_YTEXT("funSettings"), NullableSerializer<TeamFunSettings, TeamFunSettingsSerializer>(m_Team.FunSettings), obj);
    JsonSerializeUtil::SerializeString(_YTEXT("webUrl"), m_Team.WebUrl, obj);
	JsonSerializeUtil::SerializeBool(_YTEXT("isArchived"), m_Team.IsArchived, obj);
	JsonSerializeUtil::SerializeString(_YTEXT("internalId"), m_Team.InternalId, obj);
}
