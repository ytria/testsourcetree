#pragma once

#include "IRequester.h"
#include "IRequestLogger.h"

template <class T, class U>
class ValueListDeserializer;

class BusinessUser;
class UserDeserializer;
class PaginatedRequestResults;
class Sapio365Session;
struct SessionIdentifier;

class DirectoryRoleMembersRequester : public IRequester
{
public:
    DirectoryRoleMembersRequester(PooledString p_DirectoryRoleId, const std::shared_ptr<IRequestLogger>& p_Logger);
    virtual TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	TaskWrapper<void> Send(std::shared_ptr<MSGraphSession> p_Session, const SessionIdentifier& p_Identifier, YtriaTaskData p_TaskData);

    const vector<BusinessUser>& GetData() const;
    vector<BusinessUser>& GetData();

private:
    std::shared_ptr<ValueListDeserializer<BusinessUser, UserDeserializer>> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
    PooledString m_DirectoryRoleId;
};

