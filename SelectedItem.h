#pragma once

class SelectedItem : public std::tuple<PooledString, PooledString, PooledString, int32_t, PooledString, PooledString>
{
public:
    using std::tuple<PooledString, PooledString, PooledString, int32_t, PooledString, PooledString>::tuple;

    const PooledString& GetID() const
    {
        return std::get<0>(*this);
    }
    PooledString& GetID()
    {
        return std::get<0>(*this);
    }

    const PooledString& GetPrincipalName() const
    {
        return std::get<1>(*this);
    }

    PooledString& GetPrincipalName()
    {
        return std::get<1>(*this);
    }

    const PooledString& GetDisplayName() const
    {
        return std::get<2>(*this);
    }

    PooledString& GetDisplayName()
    {
        return std::get<2>(*this);
    }

    int32_t GetObjectType() const
    {
        return std::get<3>(*this);
    }

    const PooledString& GetUserGroupType() const
    {
        return std::get<4>(*this);
    }

    PooledString& GetUserGroupType()
    {
        return std::get<4>(*this);
    }

	const PooledString& GetGroupIsTeam() const
	{
		return std::get<5>(*this);
	}

	PooledString& GetGroupIsTeam()
	{
		return std::get<5>(*this);
	}
};

