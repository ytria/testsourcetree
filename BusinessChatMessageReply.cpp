#include "BusinessChatMessageReply.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessChatMessageReply>("Reply")(metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Reply"))))
		.constructor()(policy::ctor::as_object);
}
