#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "SignInLocation.h"

class SignInLocationDeserializer : public JsonObjectDeserializer, public Encapsulate<SignInLocation>
{
public:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};

