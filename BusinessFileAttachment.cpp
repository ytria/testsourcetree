#include "BusinessFileAttachment.h"

#include "FileUtil.h"
#include "LoggerService.h"
#include "MFCUtil.h"
#include "YtriaTranslate.h"

#include <fstream>

BusinessFileAttachment::BusinessFileAttachment()
{
}

BusinessFileAttachment::BusinessFileAttachment(const FileAttachment& p_FileAttachment)
{
    m_ContentType = p_FileAttachment.ContentType;
    m_Id = p_FileAttachment.Id ? *p_FileAttachment.Id : _YTEXT("");
    m_IsInline = p_FileAttachment.IsInline;
    m_LastModifiedDateTime = p_FileAttachment.LastModifiedDateTime;
    m_Name = p_FileAttachment.Name;
    m_Size = p_FileAttachment.Size;
    m_ContentBytes = p_FileAttachment.ContentBytes;
    m_ContentId = p_FileAttachment.ContentId;
    m_ContentLocation = p_FileAttachment.ContentLocation;
}

const boost::YOpt<PooledString>& BusinessFileAttachment::GetContentBytes() const
{
    return m_ContentBytes;
}

void BusinessFileAttachment::SetContentBytes(const boost::YOpt<PooledString>& p_Val)
{
    m_ContentBytes = p_Val;
}

const boost::YOpt<PooledString>& BusinessFileAttachment::GetContentId() const
{
    return m_ContentId;
}

void BusinessFileAttachment::SetContentId(const boost::YOpt<PooledString>& p_Val)
{
    m_ContentId = p_Val;
}

const boost::YOpt<PooledString>& BusinessFileAttachment::GetContentLocation() const
{
    return m_ContentLocation;
}

void BusinessFileAttachment::SetContentLocation(const boost::YOpt<PooledString>& p_Val)
{
    m_ContentLocation = p_Val;
}

wstring BusinessFileAttachment::ToFile(const wstring& p_Path, FileExistsAction p_FileExistsAction, const wstring& p_OptionalFileName/* = _YTEXT("")*/)
{
    ASSERT(GetContentBytes());
    if (GetContentBytes())
    {
        auto binaryFile = utility::conversions::from_base64(*GetContentBytes());

        ASSERT(GetName());

        const wstring fileName = !p_OptionalFileName.empty() ? p_OptionalFileName : (GetName() ? *GetName() : _YTEXT(""));

		if (ERROR_SUCCESS != ::SHCreateDirectory(NULL, p_Path.c_str()) && (GetLastError() != ERROR_ALREADY_EXISTS))
        {
            const wstring lastError = Str::convertFromUTF8(MFCUtil::GetLastErrorString());
            LoggerService::Debug(_YTEXTFORMAT(L"Error creating directory \"%s\": %s", p_Path.c_str(), lastError.c_str()));
            return _YTEXT("");
        }
        wstring completeFilePath = p_Path + wstring(_YTEXT("\\")) + fileName;

		if (p_FileExistsAction == FileExistsAction::Skip && FileUtil::FileExists2(completeFilePath))
		{
			return _YTEXT("");
		}

		if (p_FileExistsAction != FileExistsAction::Overwrite)
			completeFilePath = FileUtil::GetNoOverwriteFileName(completeFilePath);

        try
        {
            std::ofstream file(completeFilePath, ios::out | ios::binary);
            file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
            file.write(reinterpret_cast<const char*>(&binaryFile[0]), binaryFile.size());
            file.close();

            SYSTEMTIME modifiedSystemTime;
            if (m_LastModifiedDateTime.is_initialized())
            {
                ::GetSystemTime(&modifiedSystemTime);
                modifiedSystemTime.wYear = m_LastModifiedDateTime->getYear();
                modifiedSystemTime.wMonth = m_LastModifiedDateTime->getMonth();
                modifiedSystemTime.wDay = m_LastModifiedDateTime->getDay();
                modifiedSystemTime.wHour = m_LastModifiedDateTime->getHour();
                modifiedSystemTime.wMinute = m_LastModifiedDateTime->getMin();
                modifiedSystemTime.wSecond = m_LastModifiedDateTime->getSec();

                HANDLE fileHandle = ::CreateFile(completeFilePath.c_str(),
                    FILE_WRITE_ATTRIBUTES,
                    FILE_SHARE_READ | FILE_SHARE_WRITE,
                    NULL,
                    OPEN_EXISTING,
                    FILE_ATTRIBUTE_NORMAL,
                    NULL);

                FILETIME modifiedTime;
                ::SystemTimeToFileTime(&modifiedSystemTime, &modifiedTime);

                BOOL success = ::SetFileTime(fileHandle, &modifiedTime, &modifiedTime, &modifiedTime);
                ASSERT(success);

                ::CloseHandle(fileHandle);
            }
        }
        catch (const std::exception& p_Exception)
        {
            LoggerService::Debug(_YTEXTFORMAT(L"Error writing to file \"%s\": %s", completeFilePath.c_str(), Str::convertFromUTF8(p_Exception.what()).c_str()));
        }

		return completeFilePath;
    }

	return _YTEXT("");
}
