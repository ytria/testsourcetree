#include "ApplicationListRequester.h"

#include "ApplicationDeserializer.h"
#include "GraphPageRequester.h"
#include "MsGraphPaginator.h"
#include "Sapio365Session.h"
#include "ValueListDeserializer.h"
#include "MsGraphHttpRequestLogger.h"
#include "BasicPageRequestLogger.h"

ApplicationListRequester::ApplicationListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger)
	:m_Logger(p_Logger)
{
}

TaskWrapper<void> ApplicationListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<Application, ApplicationDeserializer>>();
	m_Results = std::make_shared<PaginatedRequestResults>();

	web::uri_builder uri(_YTEXT("applications"));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	auto paginatorReq = std::make_shared<GraphPageRequester>(m_Deserializer, nullptr, httpLogger);
	return MsGraphPaginator(uri.to_uri(), paginatorReq, m_Logger).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData).ThenByTask([result = m_Results, deserializer = m_Deserializer](pplx::task<void> p_Task) {
			try
			{
				p_Task.wait();
			}
			catch (const RestException& e)
			{
				RestResultInfo info(e.GetRequestResult());
				info.m_RestException = std::make_shared<RestException>(e);
				result->AddResult(info);
			}
			catch (const std::exception& e)
			{
				RestResultInfo info;
				info.m_Exception = e;
				result->AddResult(info);
			}
			catch (...)
			{
				result->AddResult(RestResultInfo());
			}
		});
}

const PaginatedRequestResults& ApplicationListRequester::GetResult() const
{
	return *m_Results;
}

const vector<Application>& ApplicationListRequester::GetData() const
{
	return m_Deserializer->GetData();
}

Application ApplicationListRequester::GetAppFromAppId(const std::shared_ptr<Sapio365Session>& p_Session, const wstring& p_AppId)
{
	ApplicationListRequester listReq(std::make_shared<BasicPageRequestLogger>(_T("applications")));
	listReq.Send(p_Session, YtriaTaskData()).GetTask().wait();

	Application foundApp;
	auto apps = listReq.GetData();
	for (const auto& app : apps)
	{
		if (app.m_AppId && *app.m_AppId == p_AppId)
		{
			foundApp = app;
			break;
		}
	}

	return foundApp;
}
