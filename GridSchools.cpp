#include "GridSchools.h"

#include "AutomationWizardSchools.h"
#include "BasicGridSetup.h"
#include "EducationClassDeserializer.h"
#include "DlgAddItemsToGroups.h"
#include "DlgClassEditHTML.h"
#include "DlgSchoolEditHTML.h"
#include "FrameSchools.h"
#include "GridUpdater.h"
#include "GridUpdaterOptions.h"
#include "GridUtil.h"

BEGIN_MESSAGE_MAP(GridSchools, ModuleO365Grid<EducationSchool>)
	NEWMODULE_COMMAND(ID_SCHOOLSGRID_SHOWCLASS_MEMBERS, ID_SCHOOLSGRID_SHOWCLASS_MEMBERS_FRAME, ID_SCHOOLSGRID_SHOWCLASS_MEMBERS_NEWFRAME, OnShowClassMembers, OnUpdateShowClassMembers)
	ON_COMMAND(ID_MODULEGRID_DELETE, OnDelete)
	ON_UPDATE_COMMAND_UI(ID_MODULEGRID_DELETE, OnUpdateDelete)
	ON_COMMAND(ID_MODULEGRID_EDIT, OnEdit)
	ON_UPDATE_COMMAND_UI(ID_MODULEGRID_EDIT, OnUpdateEdit)
	ON_COMMAND(ID_SCHOOLSGRID_ADDCLASS, OnAddClass)
	ON_UPDATE_COMMAND_UI(ID_SCHOOLSGRID_ADDCLASS, OnUpdateAddClass)
	ON_COMMAND(ID_SCHOOLSGRID_REMOVECLASS, OnRemoveClass)
	ON_UPDATE_COMMAND_UI(ID_SCHOOLSGRID_REMOVECLASS, OnUpdateRemoveClass)
END_MESSAGE_MAP()

GridSchools::GridSchools()
	: m_ColDisplayName(nullptr)
	, m_ColSchoolDescription(nullptr)
	, m_ColSchoolStatus(nullptr)
	, m_ColSchoolExternalSource(nullptr)
	, m_ColSchoolPrincipalEmail(nullptr)
	, m_ColSchoolPrincipalName(nullptr)
	, m_ColSchoolExternalPrincipalId(nullptr)
	, m_ColSchoolHighestGrade(nullptr)
	, m_ColSchoolLowestGrade(nullptr)
	, m_ColSchoolNumber(nullptr)
	, m_ColSchoolExternalId(nullptr)
	, m_ColSchoolPhone(nullptr)
	, m_ColSchoolFax(nullptr)
	, m_ColSchoolAddressCity(nullptr)
	, m_ColSchoolAddressCountryOrRegion(nullptr)
	, m_ColSchoolAddressPostalCode(nullptr)
	, m_ColSchoolAddressState(nullptr)
	, m_ColSchoolAddressStreet(nullptr)
	, m_ColClassDescription(nullptr)
	, m_ColClassMailNickname(nullptr)
	, m_ColCreatedByAppDisplayName(nullptr)
	, m_ColCreatedByAppId(nullptr)
	, m_ColCreatedByAppEmail(nullptr)
	, m_ColCreatedByAppIdentityProvider(nullptr)
	, m_ColCreatedByDeviceDisplayName(nullptr)
	, m_ColCreatedByDeviceId(nullptr)
	, m_ColCreatedByDeviceEmail(nullptr)
	, m_ColCreatedByDeviceIdentityProvider(nullptr)
	, m_ColCreatedByUserDisplayName(nullptr)
	, m_ColCreatedByUserId(nullptr)
	, m_ColCreatedByUserEmail(nullptr)
	, m_ColCreatedByUserIdentityProvider(nullptr)
	, m_ColClassClassCode(nullptr)
	, m_ColClassExternalId(nullptr)
	, m_ColClassExternalName(nullptr)
	, m_ColClassExternalSource(nullptr)
	, m_ColClassTermDisplayName(nullptr)
	, m_ColClassTermExternalId(nullptr)
	, m_ColClassTermStartDate(nullptr)
	, m_ColClassTermEndDate(nullptr)
{
	UseDefaultActions(O365Grid::ACTION_DELETE|O365Grid::ACTION_EDIT);
	EnableGridModifications();

	initWizard<AutomationWizardSchools>(_YTEXT("Automation\\Schools"));
}

GridSchools::~GridSchools()
{
	GetBackend().Clear();
}

void GridSchools::BuildView(const vector<EducationSchool>& p_Schools, const vector<O365UpdateOperation>& p_Updates, bool p_FullPurge)
{
	if (p_Schools.size() == 1 && p_Schools[0].GetError() != boost::none && p_Schools[0].GetID().IsEmpty())
	{
		HandlePostUpdateError(p_Schools[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_Schools[0].GetError()->GetFullErrorMessage());
	}
	else
	{
		GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
			| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
			| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			| GridUpdaterOptions::REFRESH_PROCESS_DATA
		);
		GridUpdater updater(*this, options, p_Updates);

		for (const auto& school : p_Schools)
		{
			vector<GridBackendField> schoolRowPk = CreatePk(school.GetID(), _YTEXT(""));
			auto schoolRow = m_RowIndex.GetRow(schoolRowPk, true, true);

			updater.GetOptions().AddRowWithRefreshedValues(schoolRow);
			updater.GetOptions().AddPartialPurgeParentRow(schoolRow);

			SetRowObjectType(schoolRow, school);
			FillFields(schoolRow, school, updater);
		}
	}
}

SchoolsChanges GridSchools::GetChanges()
{
	SchoolsChanges changes;

	for (const auto& mod : GetModifications().GetRowModifications())
	{
		if (mod->GetState() == Modification::State::AppliedLocally)
		{
			auto delMod = dynamic_cast<DeletedObjectModification*>(mod.get());
			if (nullptr != delMod)
				AddDeleteMod(*delMod, changes);

			auto addMod = dynamic_cast<CreatedObjectModification*>(mod.get());
			if (nullptr != addMod)
				AddCreateMod(*addMod, changes);
		}
	}

	for (const auto& mod : GetModifications().GetRowFieldModifications())
	{
		if (mod->GetState() == Modification::State::AppliedLocally)
		{
			auto updateMod = mod.get();
			if (nullptr != updateMod)
				AddUpdateMod(*updateMod, changes);
		}
	}

	return changes;
}

SchoolsChanges GridSchools::GetChanges(const vector<GridBackendRow*>& p_Rows)
{
	SchoolsChanges changes;

	for (auto row : p_Rows)
	{
		vector<GridBackendField> rowPk;
		GetRowPK(row, rowPk);

		auto mod = GetModifications().GetRowModification<DeletedObjectModification>(rowPk);
		if (nullptr != mod)
			AddDeleteMod(*mod, changes);

		auto addMod = GetModifications().GetRowModification<CreatedObjectModification>(rowPk);
		if (nullptr != addMod)
			AddCreateMod(*addMod, changes);

		auto updateMod = GetModifications().GetRowFieldModification(rowPk);
		if (nullptr != updateMod)
			AddUpdateMod(*updateMod, changes);
	}

	return changes;
}

wstring GridSchools::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, m_ColDisplayName);
}

row_pk_t GridSchools::UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody)
{
	ASSERT(IsGridModificationsEnabled());
	if (IsGridModificationsEnabled())
	{
		auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPk);
		ASSERT(nullptr != mod);
		if (nullptr != mod)
		{
			wstring schoolId;
			auto row = GetRowIndex().GetExistingRow(rowPk);
			ASSERT(nullptr != row);
			if (nullptr != row)
				schoolId = row->GetField(m_ColSchoolId).GetValueStr();

			// Check Create call that generated this data: Need to request RBAC props?
			EducationClassDeserializer deserializer;
			deserializer.Deserialize(json::value::parse(creationResultBody));
			const auto& eduClass = deserializer.GetData();
			auto newPk = CreatePk(schoolId, eduClass.GetID());
			mod->SetNewPK(newPk);

			return newPk;
		}
	}

	return rowPk;
}

void GridSchools::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameSchools, LicenseUtil::g_codeSapio365schools/*,
			{
				{ &m_ColMetaDisplayName, g_TitleOwnerDisplayName, g_FamilyOwner },
				{ &m_ColMetaUserPrincipalName, g_TitleOwnerUserName, g_FamilyOwner },
				{ &m_ColMetaId, g_TitleOwnerID, g_FamilyOwner }
			}*/);
		setup.Setup(*this, false);
	}

	addColumnGraphID();
	setBasicGridSetupHierarchy({ { { m_ColId, 0 } }
								,{ rttr::type::get<EducationClass>().get_id() }
								,{ rttr::type::get<EducationClass>().get_id(), rttr::type::get<EducationSchool>().get_id() } });

	// Schools
	m_ColSchoolId = AddColumn(_YUID("schoolId"), _T("School Id"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetTech });
	m_ColDisplayName = AddColumn(_YUID("displayName"), _T("Display Name"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolDescription = AddColumn(_YUID("schoolDescription"), _T("Category - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolStatus = AddColumn(_YUID("schoolStatus"), _T("Status - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolExternalSource = AddColumn(_YUID("schoolExternalSource"), _T("External - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolPrincipalEmail = AddColumn(_YUID("schoolPrincipalEmail"), _T("Principal email - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolPrincipalName = AddColumn(_YUID("schoolPrincipalName"), _T("Principal name - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolExternalPrincipalId = AddColumn(_YUID("schoolExternalPrincipalId"), _T("External principal id - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolHighestGrade = AddColumn(_YUID("schoolHighestGrade"), _T("Highest grade - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolLowestGrade = AddColumn(_YUID("schoolLowestGrade"), _T("Lowest grade - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolNumber = AddColumn(_YUID("schoolNumber"), _T("Number - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolExternalId = AddColumn(_YUID("schoolExternalId"), _T("External id - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolPhone = AddColumn(_YUID("schoolPhone"), _T("Phone - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolFax = AddColumn(_YUID("schoolFax"), _T("Fax - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	// Address
	m_ColSchoolAddressCity = AddColumn(_YUID("schoolAddressCity"), _T("City - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolAddressCountryOrRegion = AddColumn(_YUID("schoolAddressCountryOrRegion"), _T("Country or region - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolAddressPostalCode = AddColumn(_YUID("schoolAddressPostalCode"), _T("Postal Code - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolAddressState = AddColumn(_YUID("schoolAddressState"), _T("State - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSchoolAddressStreet = AddColumn(_YUID("schoolAddressStreet"), _T("Street - School"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Created By
	m_ColCreatedByAppDisplayName = AddColumn(_YUID("createdByAppDisplayName"), _T("Created by App Display Name"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByAppId = AddColumn(_YUID("createdByAppId"), _T("Created by App Id"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByAppEmail = AddColumn(_YUID("createdByAppEmail"), _T("Created by App Email"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByAppIdentityProvider = AddColumn(_YUID("createdByAppIdentityProvider"), _T("Created by App Identity Provider"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceDisplayName = AddColumn(_YUID("createdByDeviceDisplayName"), _T("Created by Device Display Name"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceId = AddColumn(_YUID("createdByDeviceId"), _T("Created by Device Id"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceEmail = AddColumn(_YUID("createdByDeviceEmail"), _T("Created by Device Email"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceIdentityProvider = AddColumn(_YUID("createdByDeviceIdentityProvider"), _T("Created by Device Identity Provider"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserDisplayName = AddColumn(_YUID("createdByUserDisplayName"), _T("Created by User Display Name"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserId = AddColumn(_YUID("createdByUserId"), _T("Created by User Id"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserEmail = AddColumn(_YUID("createdByUserEmail"), _T("Created by User Email"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserIdentityProvider = AddColumn(_YUID("createdByUserIdentityProvider"), _T("Created by User Identity Provider"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Classes
	m_ColClassDescription = AddColumn(_YUID("classDescription"), _T("Description - Class"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColClassMailNickname = AddColumn(_YUID("classMailNickname"), _T("Mail nickname - Class"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColClassClassCode = AddColumn(_YUID("classCode"), _T("Class Code - Class"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColClassExternalId = AddColumn(_YUID("classExternalId"), _T("External Id - Class"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColClassExternalName = AddColumn(_YUID("classExternalName"), _T("External Name - Class"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColClassExternalSource = AddColumn(_YUID("classExternalSource"), _T("External Source - Class"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColClassTermDisplayName = AddColumn(_YUID("classTermDisplayName"), _T("Term Display Name - Class"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColClassTermExternalId = AddColumn(_YUID("classTermExternalId"), _T("Term External Id- Class"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColClassTermStartDate = AddColumnDate(_YUID("classTermStartDate"), _T("Term Start Date - Class"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColClassTermEndDate = AddColumnDate(_YUID("classTermEndDate"), _T("Term End Date - Class"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	AddColumnForRowPK(m_ColSchoolId);
	AddColumnForRowPK(GetColId());

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameSchools*>(GetParentFrame()));
}

void GridSchools::AddPendingChanges(const vector<EducationClass>& p_Classes)
{
	GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
	for (const auto& curClass : p_Classes)
	{
		AddPendingClassChanges(curClass);
	}
}

void GridSchools::AddPendingChanges(const vector<EducationSchool>& p_Schools)
{
	GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
	for (const auto& school : p_Schools)
	{
		AddPendingSchoolChanges(school);
	}
}

void GridSchools::AddPendingClassChanges(const EducationClass& p_Class)
{
	auto rowPk = CreatePk(p_Class.m_SchoolId, p_Class.GetID());
	auto row = GetRowIndex().GetExistingRow(rowPk);
	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		AddPendingClassChanges(*row, rowPk, p_Class);
	}
}

void GridSchools::AddPendingSchoolChanges(const EducationSchool& p_School)
{
	auto rowPk = CreatePk(p_School.GetID(), _YTEXT(""));
	auto row = GetRowIndex().GetExistingRow(rowPk);
	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		AddPendingSchoolChanges(*row, rowPk, p_School);
	}
}

void GridSchools::AddPendingSchoolChanges(GridBackendRow& p_SchoolRow, const row_pk_t& p_RowPk, const EducationSchool& p_School)
{
	if (p_School.GetDisplayName())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColDisplayName, *p_School.GetDisplayName());
	if (p_School.GetDescription())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolDescription, *p_School.GetDescription());
	if (p_School.GetStatus())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolStatus, *p_School.GetStatus());
	if (p_School.GetExternalSource())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolExternalSource, *p_School.GetExternalSource());
	if (p_School.GetPrincipalEmail())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolPrincipalEmail, *p_School.GetPrincipalEmail());
	if (p_School.GetPrincipalName())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolPrincipalName, *p_School.GetPrincipalName());
	if (p_School.GetExternalPrincipalId())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolExternalPrincipalId, *p_School.GetExternalPrincipalId());
	if (p_School.GetHighestGrade())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolHighestGrade, *p_School.GetHighestGrade());
	if (p_School.GetLowestGrade())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolLowestGrade, *p_School.GetLowestGrade());
	if (p_School.GetSchoolNumber())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolNumber, *p_School.GetSchoolNumber());
	if (p_School.GetExternalId())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolExternalId, *p_School.GetExternalId());
	if (p_School.GetPhone())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolPhone, *p_School.GetPhone());
	if (p_School.GetFax())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolFax, *p_School.GetFax());

	if (p_School.GetAddressCity())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolAddressCity, *p_School.GetAddressCity());
	if (p_School.GetAddressCountryOrRegion())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolAddressCountryOrRegion, *p_School.GetAddressCountryOrRegion());
	if (p_School.GetAddressPostalCode())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolAddressPostalCode, *p_School.GetAddressPostalCode());
	if (p_School.GetAddressState())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolAddressState, *p_School.GetAddressState());
	if (p_School.GetAddressStreet())
		AddFieldChange(p_SchoolRow, p_RowPk, m_ColSchoolAddressStreet, *p_School.GetAddressStreet());

	p_SchoolRow.SetEditModified(true);
}

EducationSchool GridSchools::getBusinessObject(GridBackendRow* p_Row) const
{
	EducationSchool edSchool;

	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		edSchool.SetID(p_Row->GetField(m_ColSchoolId).GetValueStr());
		edSchool.SetDisplayName(GetStrValue(*p_Row, m_ColDisplayName));
		edSchool.SetDescription(GetStrValue(*p_Row, m_ColSchoolDescription));
		edSchool.SetStatus(GetStrValue(*p_Row, m_ColSchoolStatus));
		edSchool.SetExternalSource(GetStrValue(*p_Row, m_ColSchoolExternalSource));
		edSchool.SetPrincipalEmail(GetStrValue(*p_Row, m_ColSchoolPrincipalEmail));
		edSchool.SetPrincipalName(GetStrValue(*p_Row, m_ColSchoolPrincipalName));
		edSchool.SetExternalPrincipalId(GetStrValue(*p_Row, m_ColSchoolExternalPrincipalId));
		edSchool.SetHighestGrade(GetStrValue(*p_Row, m_ColSchoolHighestGrade));
		edSchool.SetLowestGrade(GetStrValue(*p_Row, m_ColSchoolLowestGrade));
		edSchool.SetSchoolNumber(GetStrValue(*p_Row, m_ColSchoolNumber));
		edSchool.SetExternalId(GetStrValue(*p_Row, m_ColSchoolExternalId));
		edSchool.SetPhone(GetStrValue(*p_Row, m_ColSchoolPhone));
		edSchool.SetFax(GetStrValue(*p_Row, m_ColSchoolFax));

		PhysicalAddress physicalAddress;
		physicalAddress.City = GetStrValue(*p_Row, m_ColSchoolAddressCity);
		physicalAddress.CountryOrRegion = GetStrValue(*p_Row, m_ColSchoolAddressCountryOrRegion);
		physicalAddress.PostalCode = GetStrValue(*p_Row, m_ColSchoolAddressPostalCode);
		physicalAddress.State = GetStrValue(*p_Row, m_ColSchoolAddressState);
		physicalAddress.Street = GetStrValue(*p_Row, m_ColSchoolAddressStreet);
		edSchool.SetAddress(physicalAddress);
	}

	return edSchool;
}

EducationClass GridSchools::getEducationClass(GridBackendRow* p_Row) const
{
	EducationClass edClass;

	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		edClass.SetID(p_Row->GetField(m_ColId).GetValueStr());
		edClass.SetDescription(GetStrValue(*p_Row, m_ColClassDescription));
		edClass.SetDisplayName(GetStrValue(*p_Row, m_ColDisplayName));
		edClass.SetMailNickname(GetStrValue(*p_Row, m_ColClassMailNickname));

		edClass.SetClassCode(GetStrValue(*p_Row, m_ColClassClassCode));
		edClass.SetExternalId(GetStrValue(*p_Row, m_ColClassExternalId));
		edClass.SetExternalName(GetStrValue(*p_Row, m_ColClassExternalName));
		edClass.SetExternalSource(GetStrValue(*p_Row, m_ColClassExternalSource));
	}

	return edClass;
}

void GridSchools::UpdateAddressField(const wstring& p_FieldName, const wstring& p_NewValue, web::json::value& p_Value)
{
	auto ittCity = p_Value.as_object().find(_YTEXT("address"));
	if (ittCity == p_Value.as_object().end())
		p_Value.as_object()[_YTEXT("address")] = json::value::object({ { p_FieldName, json::value::string(p_NewValue) } });
	else
		p_Value.as_object()[_YTEXT("address")].as_object()[p_FieldName] = json::value::string(p_NewValue);
}

boost::YOpt<PooledString> GridSchools::GetStrValue(GridBackendRow& p_Row, GridBackendColumn* p_Col) const
{
	boost::YOpt<PooledString> val;

	PooledString strVal = p_Row.GetField(p_Col).GetValueStr();
	if (strVal != GridBackendUtil::g_NoValueString)
		val = strVal;

	return val;
}

void GridSchools::OnEdit()
{
	if (!showUserRestrictedAccessDialog(USER_ULTRA_ADMIN_REQUIREMENT))
		return;

	vector<EducationClass> classes;
	vector<EducationSchool> schools;

	vector<GridBackendRow*> selectedRows;
	GetSelectedNonGroupRows(selectedRows);

	for (auto row : selectedRows)
	{
		row_pk_t rowPk;
		GetRowPK(row, rowPk);
		if (GridUtil::isBusinessType<EducationClass>(row))
		{
			classes.push_back(getEducationClass(row));
			ASSERT(row->HasParent());
			if (row->HasParent())
			{
				if (row->IsDeleted())
					GetModifications().Revert(rowPk, false);

				auto schoolRow = row->GetParentRow();
				ASSERT(nullptr != schoolRow);
				if (nullptr != schoolRow)
					classes.back().m_SchoolId = schoolRow->GetField(m_ColSchoolId).GetValueStr();
			}
		}
		else if (GridUtil::isBusinessType<EducationSchool>(row))
		{
			if (row->IsDeleted())
				GetModifications().Revert(rowPk, false);

			schools.push_back(getBusinessObject(row));
		}
	}

	if (!classes.empty())
	{
		DlgClassEditHTML dlg(classes, DlgFormsHTML::Action::EDIT, this);
		if (dlg.DoModal() == IDOK)
		{
			AddPendingChanges(classes);
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		}
	}
	else if (!schools.empty())
	{
		DlgSchoolEditHTML dlg(schools, DlgFormsHTML::Action::EDIT, this);
		if (dlg.DoModal() == IDOK)
		{
			AddPendingChanges(schools);
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		}
	}
}

void GridSchools::OnUpdateEdit(CCmdUI* pCmdUI)
{
	bool enable = true;

	bool hasSingleTypeSelected = true;
	if (GetSelectedRows().size() > 1)
	{
		bool first = true;
		uint32_t typeId = 0;
		for (auto row : GetSelectedRows())
		{
			if (first)
			{
				typeId = GridUtil::getBusinessTypeId(row);
				first = false;
			}
			else if (GridUtil::getBusinessTypeId(row) != typeId)
			{
				hasSingleTypeSelected = false;
				break;
			}
		}
	}
	pCmdUI->Enable(hasNoTaskRunning() && hasSingleTypeSelected);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridSchools::OnDelete()
{
	if (!showUserRestrictedAccessDialog(USER_ULTRA_ADMIN_REQUIREMENT))
		return;

	vector<GridBackendRow*> rows;
	GetSelectedNonGroupRows(rows);

	bool update = false;
	for (auto row : rows)
	{
		if (GridUtil::isBusinessType<EducationSchool>(row))
		{
			row_pk_t rowPk;
			GetRowPK(row, rowPk);
			if (row->IsCreated())
			{
				if (GetModifications().Revert(rowPk, false))
					update = true;
			}
			else if(!row->IsDeleted())
			{
				if (row->IsModified())
					GetModifications().Revert(rowPk, false);
				GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, rowPk));
				update = true;
				if (GridUtil::isBusinessType<EducationSchool>(row))
					RemoveSchoolRelatedModifications(row);
			}
		}
	}

	if (update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridSchools::OnUpdateDelete(CCmdUI* pCmdUI)
{
	bool enable = false;
	for (auto selectedRow : GetSelectedRows())
	{
		if (!selectedRow->IsDeleted())
		{
			if (!selectedRow->HasParent())
			{
				enable = true;
				break;
			}
		}
	}
	pCmdUI->Enable(hasNoTaskRunning() && enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridSchools::OnAddClass()
{
	if (IsFrameConnected() && showUserRestrictedAccessDialog(USER_ULTRA_ADMIN_REQUIREMENT))
	{
		DlgAddItemsToGroups dlg(GetSession(),
			{},
			DlgAddItemsToGroups::SELECT_CLASSES,
			{}, {}, {},
			_T("Select classes"),
			_T(""),
			RoleDelegationUtil::RBAC_Privilege::RBAC_NONE, this);

		//dlg.SetInitClasses(std::vector<EducationClass>(m_Classes.begin(), m_Classes.end()));

		if (dlg.DoModal() == IDOK)
		{
			bool refresh = false;
			for (auto schoolRow : GetSelectedSchoolRows())
			{
				PooledString schoolId = schoolRow->GetField(m_ColSchoolId).GetValueStr();
				for (const auto& curClass : dlg.GetSelectedItems())
				{
					PooledString classId = curClass.GetID();
					auto classRowPk = CreatePk(schoolId, classId);
					auto classRow = m_RowIndex.GetExistingRow(classRowPk);
					if (classRow == nullptr)
					{
						auto newClassRow = CreateNewClassRow(schoolId, classId, curClass);
						ASSERT(nullptr != newClassRow);
						if (nullptr != newClassRow)
						{
							newClassRow->SetParentRow(schoolRow);
							vector<GridBackendField> rowPk;
							GetRowPK(newClassRow, rowPk);

							GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPk));
							refresh = true;
						}
					}
					else if (classRow->IsDeleted())
					{
						refresh = GetModifications().Revert(classRowPk, false);
					}
				}
			}

			if (refresh)
				UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		}
	}
}

void GridSchools::OnUpdateAddClass(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && !GetSelectedRows().empty());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridSchools::OnRemoveClass()
{
	if (!showUserRestrictedAccessDialog(USER_ULTRA_ADMIN_REQUIREMENT))
		return;

	vector<GridBackendRow*> rows;
	GetSelectedNonGroupRows(rows);

	bool update = false;
	for (auto row : rows)
	{
		if (GridUtil::isBusinessType<EducationClass>(row))
		{
			row_pk_t rowPk;
			GetRowPK(row, rowPk);
			if (row->IsCreated())
			{
				if (GetModifications().Revert(rowPk, false))
					update = true;
			}
			else if (!row->IsDeleted())
			{
				if (row->HasParent() && !row->GetParentRow()->IsDeleted())
				{
					if (row->IsModified())
						GetModifications().Revert(rowPk, false);
							
					GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, rowPk));
					update = true;
					if (GridUtil::isBusinessType<EducationSchool>(row))
						RemoveSchoolRelatedModifications(row);
				}
			}
		}
	}

	if (update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridSchools::OnUpdateRemoveClass(CCmdUI* pCmdUI)
{
	bool enable = false;
	for (auto selectedRow : GetSelectedRows())
	{
		if (!selectedRow->IsDeleted())
		{
			if (selectedRow->HasParent() && !selectedRow->GetParentRow()->IsDeleted())
			{
				enable = true;
				break;
			}
		}
	}
	pCmdUI->Enable(hasNoTaskRunning() && enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridSchools::OnShowClassMembers()
{
	if (openModuleConfirmation())
	{
		CommandInfo info;
		info.SetOrigin(Origin::Schools);
		info.GetIds() = GetSelectedClassIds();

		std::vector<EducationSchool> schools;
		for (auto row : GetBackend().GetSelectedRowsInOrder())
		{
			if (GridUtil::isBusinessType<EducationClass>(row))
			{
				EducationSchool school;
				school.m_Id = row->GetParentRow()->GetField(m_ColSchoolId).GetValueStr();
				school.SetDisplayName(PooledString(row->GetParentRow()->GetField(m_ColDisplayName).GetValueStr()));

				auto ittSchool = std::find(schools.begin(), schools.end(), school);
				if (ittSchool == schools.end())
				{
					schools.push_back(school);
					ittSchool = schools.end() - 1;
				}

				EducationClass edClass;
				edClass.SetID(row->GetField(m_ColId).GetValueStr());
				edClass.SetDisplayName(PooledString(row->GetField(m_ColDisplayName).GetValueStr()));

				auto ittClass = std::find(ittSchool->m_Classes.begin(), ittSchool->m_Classes.end(), edClass);
				if (ittClass == ittSchool->m_Classes.end())
					ittSchool->m_Classes.push_back(edClass);
			}
		}
		info.Data() = schools;
		CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::ClassMembers, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowClassMembers, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"), _YR("Y2447")).c_str());
	}
}

void GridSchools::OnUpdateShowClassMembers(CCmdUI* pCmdUI)
{
	bool hasClassSelected = false;

	vector<GridBackendRow*> selectedRows;
	GetSelectedNonGroupRows(selectedRows);

	for (auto row : selectedRows)
	{
		if (GridUtil::isBusinessType<EducationClass>(row))
		{
			hasClassSelected = true;
			break;
		}
	}

	pCmdUI->Enable(IsFrameConnected() && hasClassSelected);
	setTextFromProfUISCommand(*pCmdUI);
}

O365IdsContainer GridSchools::GetSelectedClassIds() const
{
	O365IdsContainer ids;

	for (auto row : GetBackend().GetSelectedRowsInOrder())
	{
		if (GridUtil::isBusinessType<EducationClass>(row))
			ids.insert(row->GetField(m_ColId).GetValueStr());
	}

	return ids;
}

void GridSchools::AddDeleteMod(const DeletedObjectModification& p_Mod, SchoolsChanges& p_Changes)
{
	auto rowPk = p_Mod.GetRowKey();

	auto row = m_RowIndex.GetRow(rowPk, false, false);
	if (nullptr != row)
	{
		if (GridUtil::isBusinessType<EducationSchool>(row))
		{
			SchoolDeletion deletion;
			deletion.m_RowPk = rowPk;
			deletion.m_SchoolId = row->GetField(m_ColSchoolId).GetValueStr();
			p_Changes.m_SchoolDeletions.push_back(deletion);
		}
		else if (GridUtil::isBusinessType<EducationClass>(row))
		{
			ClassRemoval removal;
			removal.m_RowPk = rowPk;
			removal.m_ClassId = row->GetField(m_ColId).GetValueStr();
			p_Changes.m_ClassRemovals.push_back(removal);
		}
		else
			ASSERT(false);
	}
}

void GridSchools::AddCreateMod(const CreatedObjectModification& p_Mod, SchoolsChanges& p_Changes)
{
	auto rowPk = p_Mod.GetRowKey();

	auto row = m_RowIndex.GetRow(rowPk, false, false);
	if (nullptr != row)
	{
		if (GridUtil::isBusinessType<EducationClass>(row))
		{
			ClassAddition addition;
			addition.m_RowPk = rowPk;
			addition.m_SchoolId = row->GetField(m_ColSchoolId).GetValueStr();
			addition.m_ClassId = row->GetField(m_ColId).GetValueStr();
			p_Changes.m_ClassAdditions.push_back(addition);
		}
		else ASSERT(false);
	}
}

void GridSchools::AddUpdateMod(const RowFieldUpdates<Scope::O365>& p_Mod, SchoolsChanges& p_Changes)
{
	auto rowPk = p_Mod.GetRowKey();

	auto row = m_RowIndex.GetRow(rowPk, false, false);
	if (nullptr != row)
	{
		if (GridUtil::isBusinessType<EducationClass>(row))
		{
			ClassUpdate update;
			update.m_RowPk = rowPk;
			update.m_ClassId = row->GetField(m_ColId).GetValueStr();
			auto newValues = json::value::object();
			
			for (const auto& fieldUpdate : p_Mod.GetFieldUpdates())
			{
				auto newField = fieldUpdate->GetNewValue();
				ASSERT(newField);
				if (newField)
				{
					auto thisModCol = GetColumnByID(fieldUpdate->GetColumnID());
					wstring newValueStr = fieldUpdate->GetNewValue()->GetValueStr();
					if (thisModCol == m_ColClassDescription)
						newValues.as_object()[_YTEXT("description")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColDisplayName)
						newValues.as_object()[_YTEXT("displayName")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColClassMailNickname)
						newValues.as_object()[_YTEXT("mailNickname")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColClassClassCode)
						newValues.as_object()[_YTEXT("classCode")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColClassExternalId)
						newValues.as_object()[_YTEXT("externalId")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColClassExternalName)
						newValues.as_object()[_YTEXT("externalName")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColClassExternalSource)
						newValues.as_object()[_YTEXT("externalSource")] = json::value::string(newValueStr);
				}
			}
			update.m_NewValues = newValues;

			p_Changes.m_ClassUpdates.push_back(update);
		}
		else if (GridUtil::isBusinessType<EducationSchool>(row))
		{
			SchoolUpdate update;
			update.m_RowPk = rowPk;
			update.m_SchoolId = row->GetField(m_ColSchoolId).GetValueStr();

			auto newValues = json::value::object();
			for (const auto& fieldUpdate : p_Mod.GetFieldUpdates())
			{
				auto newField = fieldUpdate->GetNewValue();
				ASSERT(newField);
				if (newField)
				{
					auto thisModCol = GetColumnByID(fieldUpdate->GetColumnID());
					wstring newValueStr = fieldUpdate->GetNewValue()->GetValueStr();

					if (thisModCol == m_ColDisplayName)
						newValues.as_object()[_YTEXT("displayName")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolDescription)
						newValues.as_object()[_YTEXT("description")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolStatus)
						newValues.as_object()[_YTEXT("status")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolExternalSource)
						newValues.as_object()[_YTEXT("externalSource")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolPrincipalEmail)
						newValues.as_object()[_YTEXT("principalEmail")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolPrincipalName)
						newValues.as_object()[_YTEXT("principalName")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolExternalPrincipalId)
						newValues.as_object()[_YTEXT("externalPrincipalId")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolHighestGrade)
						newValues.as_object()[_YTEXT("highestGrade")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolLowestGrade)
						newValues.as_object()[_YTEXT("lowestGrade")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolNumber)
						newValues.as_object()[_YTEXT("schoolNumber")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolExternalId)
						newValues.as_object()[_YTEXT("externalId")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolPhone)
						newValues.as_object()[_YTEXT("phone")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolFax)
						newValues.as_object()[_YTEXT("fax")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColSchoolAddressCity)
						UpdateAddressField(_YTEXT("city"), newValueStr, newValues);
					else if (thisModCol == m_ColSchoolAddressCountryOrRegion)
						UpdateAddressField(_YTEXT("countryOrRegion"), newValueStr, newValues);
					else if (thisModCol == m_ColSchoolAddressPostalCode)
						UpdateAddressField(_YTEXT("postalCode"), newValueStr, newValues);
					else if (thisModCol == m_ColSchoolAddressState)
						UpdateAddressField(_YTEXT("state"), newValueStr, newValues);
					else if (thisModCol == m_ColSchoolAddressStreet)
						UpdateAddressField(_YTEXT("street"), newValueStr, newValues);
				}
			}
			update.m_NewValues = newValues;
			p_Changes.m_SchoolUpdates.push_back(update);
		}
	}
}

void GridSchools::InitializeCommands()
{
	ModuleO365Grid<EducationSchool>::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem cmd;
		cmd.m_nCmdID = ID_SCHOOLSGRID_ADDCLASS;
		cmd.m_sMenuText = _T("Add class");
		cmd.m_sToolbarText = _T("Add class");
		cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, cmd, true);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_SCHOOLSGRID_ADDCLASS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_SCHOOLSGRID_ADDCLASS, hIcon, false);
		setRibbonTooltip(cmd.m_nCmdID,
			_T(""),
			_T(""),
			cmd.m_sAccelText);
	}

	{
		CExtCmdItem cmd;
		cmd.m_nCmdID = ID_SCHOOLSGRID_REMOVECLASS;
		cmd.m_sMenuText = _T("Remove class");
		cmd.m_sToolbarText = _T("Remove class");
		cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, cmd, true);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_SCHOOLSGRID_REMOVECLASS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_SCHOOLSGRID_REMOVECLASS, hIcon, false);
		setRibbonTooltip(cmd.m_nCmdID,
			_T(""),
			_T(""),
			cmd.m_sAccelText);
	}

	{
		CExtCmdItem cmd;
		cmd.m_nCmdID = ID_SCHOOLSGRID_SHOWCLASS_MEMBERS;
		cmd.m_sMenuText = _T("Show class members...");
		cmd.m_sToolbarText = _T("Show class members...");
		cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_SCHOOLSGRID_SHOW_CLASS_MEMBERS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_SCHOOLSGRID_SHOWCLASS_MEMBERS, hIcon, false);

		setRibbonTooltip(cmd.m_nCmdID,
			_T("Show class"),
			_T("Show class"),
			cmd.m_sAccelText);

		{
			cmd.m_nCmdID = ID_SCHOOLSGRID_SHOWCLASS_MEMBERS_FRAME;
			cmd.m_sMenuText = _YTEXT("");
			cmd.m_sToolbarText = _T("Show school members...");
			cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, cmd);

			cmd.m_nCmdID = ID_SCHOOLSGRID_SHOWCLASS_MEMBERS_NEWFRAME;
			cmd.m_sMenuText = _YTEXT("");
			cmd.m_sToolbarText = _T("Show in a new window");
			cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, cmd);
		}
	}
}

void GridSchools::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ModuleO365Grid<EducationSchool>::OnCustomPopupMenu(pPopup, nStart);

	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_SCHOOLSGRID_ADDCLASS);
		pPopup->ItemInsert(ID_SCHOOLSGRID_REMOVECLASS);
		pPopup->ItemInsert(ID_SCHOOLSGRID_SHOWCLASS_MEMBERS);
	}
}

void GridSchools::FillFields(GridBackendRow* p_Row, const EducationSchool& p_School, GridUpdater& p_Updater)
{
	p_Row->AddField(p_School.m_Id, m_ColSchoolId);
	p_Row->AddField(p_School.GetDisplayName(), m_ColDisplayName);
	p_Row->AddField(p_School.GetDescription(), m_ColSchoolDescription);
	p_Row->AddField(p_School.GetStatus(), m_ColSchoolStatus);
	p_Row->AddField(p_School.GetExternalSource(), m_ColSchoolExternalSource);
	p_Row->AddField(p_School.GetPrincipalEmail(), m_ColSchoolPrincipalEmail);
	p_Row->AddField(p_School.GetPrincipalName(), m_ColSchoolPrincipalName);
	p_Row->AddField(p_School.GetExternalPrincipalId(), m_ColSchoolExternalPrincipalId);
	p_Row->AddField(p_School.GetHighestGrade(), m_ColSchoolHighestGrade);
	p_Row->AddField(p_School.GetLowestGrade(), m_ColSchoolLowestGrade);
	p_Row->AddField(p_School.GetSchoolNumber(), m_ColSchoolNumber);
	p_Row->AddField(p_School.GetExternalId(), m_ColSchoolExternalId);
	p_Row->AddField(p_School.GetPhone(), m_ColSchoolPhone);
	p_Row->AddField(p_School.GetFax(), m_ColSchoolFax);

	if (p_School.GetAddress())
	{
		const auto& address = *p_School.GetAddress();
		p_Row->AddField(address.City, m_ColSchoolAddressCity);
		p_Row->AddField(address.CountryOrRegion, m_ColSchoolAddressCountryOrRegion);
		p_Row->AddField(address.PostalCode, m_ColSchoolAddressPostalCode);
		p_Row->AddField(address.State, m_ColSchoolAddressState);
		p_Row->AddField(address.Street, m_ColSchoolAddressStreet);
	}

	if (p_School.GetCreatedBy())
	{
		const auto& createdBy = *p_School.GetCreatedBy();
		if (createdBy.Application)
		{
			p_Row->AddField(createdBy.Application->DisplayName, m_ColCreatedByAppDisplayName);
			p_Row->AddField(createdBy.Application->Id, m_ColCreatedByAppId);
			p_Row->AddField(createdBy.Application->Email, m_ColCreatedByAppEmail);
			p_Row->AddField(createdBy.Application->IdentityProvider, m_ColCreatedByAppIdentityProvider);
		}

		if (createdBy.Device)
		{
			p_Row->AddField(createdBy.Device->DisplayName, m_ColCreatedByDeviceDisplayName);
			p_Row->AddField(createdBy.Device->Id, m_ColCreatedByDeviceId);
			p_Row->AddField(createdBy.Device->Email, m_ColCreatedByDeviceEmail);
			p_Row->AddField(createdBy.Device->IdentityProvider, m_ColCreatedByDeviceIdentityProvider);
		}

		if (createdBy.User)
		{
			p_Row->AddField(createdBy.User->DisplayName, m_ColCreatedByUserDisplayName);
			p_Row->AddField(createdBy.User->Id, m_ColCreatedByUserId);
			p_Row->AddField(createdBy.User->Email, m_ColCreatedByUserEmail);
			p_Row->AddField(createdBy.User->IdentityProvider, m_ColCreatedByUserIdentityProvider);
		}
	}

	for (const auto& curClass : p_School.m_Classes)
	{
		auto classRowPk = CreatePk(p_School.GetID(), curClass.GetID());
		auto classRow = m_RowIndex.GetRow(classRowPk, true, true);
		SetRowObjectType(classRow, curClass);

		p_Updater.GetOptions().AddRowWithRefreshedValues(classRow);

		UpdateClassRow(classRow, curClass);

		classRow->SetParentRow(p_Row);
	}

}

void GridSchools::AddPendingClassChanges(GridBackendRow& p_ClassRow, const row_pk_t& p_RowPk, const EducationClass& p_CurClass)
{
	if (p_CurClass.GetDescription())
		AddFieldChange(p_ClassRow, p_RowPk, m_ColClassDescription, *p_CurClass.GetDescription());
	if (p_CurClass.GetDisplayName())
		AddFieldChange(p_ClassRow, p_RowPk, m_ColDisplayName, *p_CurClass.GetDisplayName());
	if (p_CurClass.GetMailNickname())
		AddFieldChange(p_ClassRow, p_RowPk, m_ColClassMailNickname, *p_CurClass.GetMailNickname());
	if (p_CurClass.GetClassCode())
		AddFieldChange(p_ClassRow, p_RowPk, m_ColClassClassCode, *p_CurClass.GetClassCode());
	if (p_CurClass.GetExternalId())
		AddFieldChange(p_ClassRow, p_RowPk, m_ColClassExternalId, *p_CurClass.GetExternalId());
	if (p_CurClass.GetExternalName())
		AddFieldChange(p_ClassRow, p_RowPk, m_ColClassExternalName, *p_CurClass.GetExternalName());
	if (p_CurClass.GetExternalSource())
		AddFieldChange(p_ClassRow, p_RowPk, m_ColClassExternalSource, *p_CurClass.GetExternalSource());
	p_ClassRow.SetEditModified(true);
}

void GridSchools::UpdateClassRow(GridBackendRow* p_ClassRow, const EducationClass& p_CurClass)
{
	p_ClassRow->AddField(p_CurClass.GetDescription(), m_ColClassDescription);
	p_ClassRow->AddField(p_CurClass.GetDisplayName(), m_ColDisplayName);
	p_ClassRow->AddField(p_CurClass.GetMailNickname(), m_ColClassMailNickname);
	if (p_CurClass.m_CreatedBy)
	{
		const auto& createdBy = *p_CurClass.m_CreatedBy;
		if (createdBy.Application)
		{
			p_ClassRow->AddField(createdBy.Application->DisplayName, m_ColCreatedByAppDisplayName);
			p_ClassRow->AddField(createdBy.Application->Id, m_ColCreatedByAppId);
			p_ClassRow->AddField(createdBy.Application->Email, m_ColCreatedByAppEmail);
			p_ClassRow->AddField(createdBy.Application->IdentityProvider, m_ColCreatedByAppIdentityProvider);
		}

		if (createdBy.Device)
		{
			p_ClassRow->AddField(createdBy.Device->DisplayName, m_ColCreatedByDeviceDisplayName);
			p_ClassRow->AddField(createdBy.Device->Id, m_ColCreatedByDeviceId);
			p_ClassRow->AddField(createdBy.Device->Email, m_ColCreatedByDeviceEmail);
			p_ClassRow->AddField(createdBy.Device->IdentityProvider, m_ColCreatedByDeviceIdentityProvider);
		}

		if (createdBy.User)
		{
			p_ClassRow->AddField(createdBy.User->DisplayName, m_ColCreatedByUserDisplayName);
			p_ClassRow->AddField(createdBy.User->Id, m_ColCreatedByUserId);
			p_ClassRow->AddField(createdBy.User->Email, m_ColCreatedByUserEmail);
			p_ClassRow->AddField(createdBy.User->IdentityProvider, m_ColCreatedByUserIdentityProvider);
		}
	}
	p_ClassRow->AddField(p_CurClass.GetClassCode(), m_ColClassClassCode);
	p_ClassRow->AddField(p_CurClass.GetExternalId(), m_ColClassExternalId);
	p_ClassRow->AddField(p_CurClass.GetExternalName(), m_ColClassExternalName);
	p_ClassRow->AddField(p_CurClass.GetExternalSource(), m_ColClassExternalSource);

	if (p_CurClass.m_Term)
	{
		p_ClassRow->AddField(p_CurClass.m_Term->m_DisplayName, m_ColClassTermDisplayName);
		p_ClassRow->AddField(p_CurClass.m_Term->m_ExternalId, m_ColClassTermExternalId);
		p_ClassRow->AddField(p_CurClass.m_Term->m_StartDate, m_ColClassTermStartDate);
		p_ClassRow->AddField(p_CurClass.m_Term->m_EndDate, m_ColClassTermEndDate);
	}
}

void GridSchools::AddFieldChange(GridBackendRow& p_ClassRow, const row_pk_t& p_RowPk, GridBackendColumn* p_Col, PooledString p_Val)
{
	ASSERT(nullptr != p_Col);
	if (nullptr != p_Col)
	{
		const auto oldField = p_ClassRow.GetField(p_Col);
		const auto& newField = p_ClassRow.AddField(p_Val, p_Col);
		GetModifications().Add(std::make_unique<FieldUpdateO365>(*this, p_RowPk, p_Col->GetID(), oldField, newField));
	}
}

set<GridBackendRow*> GridSchools::GetSelectedSchoolRows() const
{
	set<GridBackendRow*> schoolRows;

	vector<GridBackendRow*> selectedRows;
	GetBackend().GetSelectedNonGroupRows(selectedRows);

	for (auto selectedRow : selectedRows)
	{
		if (GridUtil::isBusinessType<EducationSchool>(selectedRow))
		{
			schoolRows.insert(selectedRow);
		}
		else
		{
			auto parentRow = selectedRow->GetParentRow();
			if (GridUtil::isBusinessType<EducationSchool>(parentRow))
				schoolRows.insert(parentRow);
		}
	}

	return schoolRows;
}

GridBackendRow* GridSchools::CreateNewClassRow(PooledString p_SchoolId, PooledString p_ClassId, const SelectedItem& p_Class)
{
	auto row = m_RowIndex.GetRow({ CreatePk(p_SchoolId, p_ClassId) }, true, false);

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		row->AddField(p_Class.GetDisplayName(), m_ColDisplayName);
		SetRowObjectType(row, EducationClass());
	}

	return row;
}

vector<GridBackendField> GridSchools::CreatePk(PooledString p_SchoolId, PooledString p_ClassId) const
{
	if (p_ClassId.IsEmpty())
		p_ClassId = p_SchoolId;
	ASSERT(!p_ClassId.IsEmpty());
	return vector<GridBackendField> {
		GridBackendField(p_ClassId, GetColId()),
		GridBackendField(p_SchoolId, m_ColSchoolId)
	};
}

void GridSchools::RemoveSchoolRelatedModifications(GridBackendRow* p_SchoolRow)
{
	ASSERT(GridUtil::isBusinessType<EducationSchool>(p_SchoolRow));
	if (GridUtil::isBusinessType<EducationSchool>(p_SchoolRow))
	{
		for (auto childRow : GetChildRows(p_SchoolRow))
		{
			vector<GridBackendField> pk;
			GetRowPK(childRow, pk);

			if (GetModifications().GetRowModification<DeletedObjectModification>(pk))
				GetModifications().RemoveRowModifications(pk, true);
		}
	}
}
