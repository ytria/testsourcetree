#include "ResourceGroupDeserializer.h"

#include "JsonSerializeUtil.h"

void Azure::ResourceGroupDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);
}
