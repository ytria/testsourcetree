#include "GridUpdaterOptions.h"

GridUpdaterOptions::GridUpdaterOptions(int32_t p_Flags)
	: m_Flags(p_Flags)
{
    ASSERT(!(IsFullPurge() && IsPartialPurge()));
}

GridUpdaterOptions::GridUpdaterOptions(int32_t p_Flags, const vector<GridBackendColumn*>& p_ColumnsWithRefreshedValues)
	: GridUpdaterOptions(p_Flags)
{
    ASSERT(!(IsFullPurge() && IsPartialPurge()));
	m_ColumnsWithRefreshedValues.insert(p_ColumnsWithRefreshedValues.begin(), p_ColumnsWithRefreshedValues.end());
}

bool GridUpdaterOptions::IsUpdateGrid() const
{
    return UPDATE_GRID == (m_Flags & UPDATE_GRID);
}

bool GridUpdaterOptions::IsFullPurge() const
{
	return FULLPURGE == (m_Flags & FULLPURGE);
}

bool GridUpdaterOptions::IsRefreshModificationStates() const
{
	return REFRESH_MODIFICATION_STATES == (m_Flags & REFRESH_MODIFICATION_STATES);
}

bool GridUpdaterOptions::IsHandlePostUpdateError() const
{
	return HANDLE_POSTUPDATE_ERRORS == (m_Flags & HANDLE_POSTUPDATE_ERRORS);
}

bool GridUpdaterOptions::IsShowLoadingErrorDialog()
{
	return 0 == (m_Flags & DO_NOT_SHOW_LOADING_ERROR_DIALOG);
}

const set<GridBackendColumn*>& GridUpdaterOptions::GetColumnsWithRefreshedValues() const
{
	return m_ColumnsWithRefreshedValues;
}

const set<GridBackendRow*>& GridUpdaterOptions::GetRowsWithRefreshedValues() const
{
	return m_RowsWithRefreshedValues;
}

const set<GridBackendRow*>& GridUpdaterOptions::GetPartialPurgeParentRows() const
{
    return m_PartialPurgeParentRows;
}

void GridUpdaterOptions::AddRowWithRefreshedValues(GridBackendRow* p_RowWithRefreshedValues)
{
	ASSERT(nullptr != p_RowWithRefreshedValues);
	if (nullptr != p_RowWithRefreshedValues)
		m_RowsWithRefreshedValues.insert(p_RowWithRefreshedValues);
}

void GridUpdaterOptions::AddRowsWithRefreshedValues(const std::unordered_set<GridBackendRow*>& p_RowsWithRefreshedValues)
{
	m_RowsWithRefreshedValues.insert(p_RowsWithRefreshedValues.begin(), p_RowsWithRefreshedValues.end());
}

bool GridUpdaterOptions::IsRefreshProcessData() const
{
	return REFRESH_PROCESS_DATA == (m_Flags & REFRESH_PROCESS_DATA);
}

bool GridUpdaterOptions::IsProcessLoadMore() const
{
	return PROCESS_LOADMORE == (m_Flags & PROCESS_LOADMORE);
}

bool GridUpdaterOptions::IsPartialPurge() const
{
    return PARTIALPURGE == (m_Flags & PARTIALPURGE);
}

void GridUpdaterOptions::SetRefreshProcessData(bool p_Refresh)
{
	if (p_Refresh)
		m_Flags |= REFRESH_PROCESS_DATA;
	else
		m_Flags &= ~REFRESH_PROCESS_DATA;
}

void GridUpdaterOptions::SetPurgeFlag(int32_t p_Flag)
{
	ASSERT(0 == p_Flag || FULLPURGE == p_Flag || PARTIALPURGE == p_Flag);
	m_Flags &= ~(FULLPURGE | PARTIALPURGE);
	m_Flags |= p_Flag;
}

void GridUpdaterOptions::AddColumnWithRefreshedValues(GridBackendColumn* p_ColumnWithRefreshedValues)
{
	ASSERT(nullptr != p_ColumnWithRefreshedValues);
	if (nullptr != p_ColumnWithRefreshedValues)
		m_ColumnsWithRefreshedValues.insert(p_ColumnWithRefreshedValues);
}

void GridUpdaterOptions::AddPartialPurgeParentRow(GridBackendRow* p_Row)
{
    m_PartialPurgeParentRows.insert(p_Row);
}

void GridUpdaterOptions::AddAction(const GridBackendUtil::GRIDPOSTUPATEACTION p_Action)
{
	m_Actions.push_back(p_Action);
}

const vector<GridBackendUtil::GRIDPOSTUPATEACTION>& GridUpdaterOptions::GetActions() const
{
	return m_Actions;
}