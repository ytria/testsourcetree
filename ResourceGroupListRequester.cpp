#include "ResourceGroupListRequester.h"

#include "AzurePaginator.h"
#include "ResourceGroupDeserializer.h"
#include "AzureSession.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "ValueListDeserializer.h"
#include "LoggerService.h"
#include "BasicHttpRequestLogger.h"

ResourceGroupListRequester::ResourceGroupListRequester(PooledString p_SubscriptionId)
	:m_SubscriptionId(p_SubscriptionId)
{
}

TaskWrapper<void> ResourceGroupListRequester::Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<Azure::ResourceGroup, Azure::ResourceGroupDeserializer>>();
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri(_YTEXT("subscriptions"));
	uri.append_path(m_SubscriptionId);
	uri.append_path(_YTEXT("resourceGroups"));
	uri.append_query(AzureSession::g_ApiVersionKey, _YTEXT("2018-05-01"));

	LoggerService::User(YtriaTranslate::Do(ResourceGroupListRequester_Send_1, _YLOC("Retrieving resource groups")).c_str(), p_TaskData.GetOriginator());

	Azure::Paginator paginator(uri.to_uri(), [=](const web::uri & p_Uri) {
		auto json = std::make_shared<web::json::value>();
		auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
		p_Session->GetAzureSession()->Get(p_Uri, httpLogger, m_Result, m_Deserializer, json, p_TaskData).GetTask().wait();
		return *json;
	});
	
	return paginator.Paginate();
}

const vector<Azure::ResourceGroup>& ResourceGroupListRequester::GetData() const
{
	return m_Deserializer->GetData();
}
