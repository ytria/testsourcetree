#include "MailboxPermissionsAddModification.h"
#include "GridMailboxPermissions.h"

MailboxPermissionsAddModification::MailboxPermissionsAddModification(GridMailboxPermissions& p_Grid, const row_pk_t& p_RowKey, const wstring& p_UserId)
	: ModificationWithRequestFeedback(_YTEXT("")),
	m_UserId(p_UserId),
	m_Grid(p_Grid)
{
	m_RowKey = p_RowKey;
}

void MailboxPermissionsAddModification::Apply()
{
	auto row = m_Grid.get().GetRowIndex().GetExistingRow(GetRowKey());
	if (nullptr != row)
	{
		auto accessRights = row->GetField(m_Grid.get().GetColAccessRights()).GetValuesMulti<PooledString>();
		if (nullptr != accessRights)
			m_Permissions = Str::ToStrVector(*accessRights);
		else
			m_Permissions = { row->GetField(m_Grid.get().GetColAccessRights()).GetValueStr() };
		m_MailboxOwner = row->GetField(m_Grid.get().GetColOwnerId()).GetValueStr();

		m_Grid.get().OnRowCreated(row, false);

		m_PermissionsUserId = row->GetField(m_Grid.get().GetColPermissionsUser()).GetValueStr();
		m_InheritanceType = row->GetField(m_Grid.get().GetColInheritanceType()).GetValueStr();
		row_pk_t parentRowPk;
		m_Grid.get().GetRowPK(row->GetParentRow(), parentRowPk);
		m_MailboxOwnerRowPk = parentRowPk;

		SetObjectName(row->GetField(m_Grid.get().GetColOwnerDisplayName()).GetValueStr() + wstring(_YTEXT(" -> ")) + row->GetField(m_Grid.get().GetColPermissionsUser()).GetValueStr());
	}
	else
	{
		auto parentRowPk = m_Grid.get().GetRowIndex().GetExistingRow(m_MailboxOwnerRowPk);
		ASSERT(nullptr != parentRowPk);
		if (nullptr != parentRowPk)
		{
			auto respawnedRowPk = CreateRow(m_Grid, *parentRowPk, m_PermissionsUserId, m_InheritanceType, m_Permissions);
			ASSERT(m_RowKey == respawnedRowPk);
			row = m_Grid.get().GetRowIndex().GetExistingRow(respawnedRowPk);
			ASSERT(nullptr != row);
			if (nullptr != row)
				m_Grid.get().OnRowCreated(row, false);
		}
	}
}

vector<Modification::ModificationLog> MailboxPermissionsAddModification::GetModificationLogs() const
{
	vector<Modification::ModificationLog> modLogs;
	modLogs.emplace_back(GridBackendUtil::ToString(GetRowKey()), GetObjectName(), _YTEXT("Add mailbox permissions"), _YTEXT("Access Rights"), m_Permissions, GetState());
	return modLogs;
}

void MailboxPermissionsAddModification::Revert()
{
	auto row = m_Grid.get().GetRowIndex().GetExistingRow(GetRowKey());
	if (nullptr != row)
		m_Grid.get().RemoveRow(row, false);
}

void MailboxPermissionsAddModification::SetPermissions(const vector<wstring>& p_Permissions)
{
	m_Permissions = p_Permissions;
	auto row = m_Grid.get().GetRowIndex().GetExistingRow(GetRowKey());
	ASSERT(nullptr != row);
	if (nullptr != row)
		row->AddField(m_Permissions, m_Grid.get().GetColAccessRights());
}

const vector<wstring>& MailboxPermissionsAddModification::GetPermissions() const
{
	return m_Permissions;
}

const wstring& MailboxPermissionsAddModification::GetMailboxOwner() const
{
	return m_MailboxOwner;
}

const wstring& MailboxPermissionsAddModification::GetUserId() const
{
	return m_UserId;
}

row_pk_t MailboxPermissionsAddModification::CreateRow(GridMailboxPermissions& p_Grid, GridBackendRow& p_OwnerRow, const wstring& p_PermissionsUserId, const wstring& p_InheritanceType, const vector<wstring>& p_Rights)
{
	row_pk_t pk = p_Grid.CreatePk(p_PermissionsUserId,
		p_InheritanceType,
		p_OwnerRow.GetField(p_Grid.GetTemplateUsers().m_ColumnID).GetValueStr()
	);

	if (p_Grid.GetRowIndex().GetExistingRow(pk) == nullptr && !SimilarRowExists(p_Grid, pk, p_OwnerRow))
	{
		GridBackendRow* row = p_Grid.GetRowIndex().GetRow(pk, true, false);
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			p_Grid.SetRowObjectType(row, MailboxPermissions());

			wstring ownerUPN = p_OwnerRow.GetField(p_Grid.GetTemplateUsers().m_ColumnUserPrincipalName).GetValueStr();
			wstring ownerDispName = p_OwnerRow.GetField(p_Grid.GetTemplateUsers().m_ColumnDisplayName).GetValueStr();
			row->AddField(ownerUPN, p_Grid.GetTemplateUsers().m_ColumnUserPrincipalName);
			row->AddField(ownerDispName, p_Grid.GetTemplateUsers().m_ColumnDisplayName);
			row->AddField(p_Rights, p_Grid.GetColAccessRights());
			row->SetParentRow(&p_OwnerRow);
		}
	}
	else
		pk.clear();

	return pk;
}

bool MailboxPermissionsAddModification::SimilarRowExists(GridMailboxPermissions& p_Grid, const row_pk_t& p_Pk, GridBackendRow& p_UserRow)
{
	bool exists = false;

	if (p_Pk.size() == 3)
	{
		auto childRows = p_Grid.GetChildRows(&p_UserRow, true);
		for (auto itt = childRows.begin(); itt != childRows.end() && !exists; ++itt)
		{
			const wstring& user = (*itt)->GetField(p_Grid.GetColPermissionsUser()).GetValueStr();
			if (user == p_Pk[1].GetValueStr())
				exists = true;
		}
	}

	return exists;
}
