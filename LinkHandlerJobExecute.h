#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerJobExecute : public IBrowserLinkHandler
{
public:
	virtual void Handle(YBrowserLink& p_Link) const override;
};