#pragma once

#include "BusinessObject.h"
#include "Post.h"

class BusinessPost : public BusinessObject
{
public:
    BusinessPost();
    BusinessPost(const Post& p_Post);

    const boost::YOpt<ItemBody>& GetBody() const;
    void SetBody(const boost::YOpt<ItemBody>& p_Val);
    
    const boost::YOpt<vector<PooledString>>& GetCategories() const;
    void SetCategories(const boost::YOpt<vector<PooledString>>& p_Val);

    const boost::YOpt<PooledString>& GetChangeKey() const;
    void SetChangeKey(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<PooledString>& GetConversationId() const;
    void SetConversationId(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<PooledString>& GetConversationThreadId() const;
    void SetConversationThreadId(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<YTimeDate>& GetCreatedDateTime() const;
    void SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_Val);

    const boost::YOpt<Recipient>& GetFrom() const;
    void SetFrom(const boost::YOpt<Recipient>& p_Val);

    const boost::YOpt<bool>& GetHasAttachments() const;
    void SetHasAttachments(const boost::YOpt<bool>& p_Val);

    const boost::YOpt<YTimeDate>& GetLastModifiedDateTime() const;
    void SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val);

    const vector<Recipient>& GetNewParticipants() const;
    void SetNewParticipants(const vector<Recipient>& p_Val);

    const boost::YOpt<YTimeDate>& GetReceivedDateTime() const;
    void SetReceivedDateTime(const boost::YOpt<YTimeDate>& p_Val);

    const boost::YOpt<Recipient>& GetSender() const;
    void SetSender(const boost::YOpt<Recipient>& p_Val);

	static Post ToPost(const BusinessPost& p_BusinessPost);

private:
    boost::YOpt<ItemBody> m_Body;
    boost::YOpt<vector<PooledString>> m_Categories;
    boost::YOpt<PooledString> m_ChangeKey;
    boost::YOpt<PooledString> m_ConversationId;
    boost::YOpt<PooledString> m_ConversationThreadId;
    boost::YOpt<YTimeDate> m_CreatedDateTime;
    boost::YOpt<Recipient> m_From;
    boost::YOpt<bool> m_HasAttachments;
    boost::YOpt<YTimeDate> m_LastModifiedDateTime;
    vector<Recipient> m_NewParticipants;
    boost::YOpt<YTimeDate> m_ReceivedDateTime;
    boost::YOpt<Recipient> m_Sender;

    RTTR_ENABLE(BusinessObject)
        RTTR_REGISTRATION_FRIEND
        friend class PostDeserializer;
};

