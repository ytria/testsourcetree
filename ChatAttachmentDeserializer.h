#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ChatAttachment.h"

class ChatAttachmentDeserializer : public JsonObjectDeserializer, public Encapsulate<ChatAttachment>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};
