#include "ModuleBase.h"

#include "BasicRequestLogger.h"
#include "DlgOpenFrame.h"
#include "LoggerService.h"
#include "MultiObjectsRequestLogger.h"
#include "Registry.h"
#include "Resource.h"
#include "safeTaskCall.h"
#include "Sapio365Settings.h"
#include "TaskDataManager.h"

template<class T>
bool ModuleBase::ShouldCreateFrame(const ModuleCriteria& p_ModuleCriteria, Command::HistorySourceWindow& p_SourceWindow, AutomationAction* p_Action)
{
	return shouldCreateFrame<T>(p_ModuleCriteria, p_SourceWindow, p_Action, nullptr);
}

template<class T>
bool ModuleBase::ShouldCreateFrame(const ModuleCriteria& p_ModuleCriteria, Command::HistorySourceWindow& p_SourceWindow, AutomationAction* p_Action, const ModuleOptions& p_Options)
{
	return shouldCreateFrame<T>(p_ModuleCriteria, p_SourceWindow, p_Action, &p_Options);
}

template<class T>
bool ModuleBase::shouldCreateFrame(const ModuleCriteria& p_ModuleCriteria, Command::HistorySourceWindow& p_SourceWindow, AutomationAction* p_Action, const ModuleOptions* p_Options)
{
	bool shouldCreateNewFrame = true;

	if (nullptr == p_Action)// always create new frame if automated
	{
		Office365AdminApp* pApp = dynamic_cast<Office365AdminApp*>(AfxGetApp());
		ASSERT(nullptr != pApp);

		auto curSession = GetConnectedSapio365Session();

		auto frame = p_SourceWindow.GetCFrameWndForHistory();
		// The commented code below allow to re-use the Next frame when the same criteria is requested.
		// We don't want this anymore (Bug #180827.RL.008A36: after go back and load the module again (not in a new window), the previous formats should not be kept)
		/*{
			auto historyFrame = dynamic_cast<IHistoryFrame*>(frame);
			if (nullptr != historyFrame)
			{
				auto next = historyFrame->GetNext();
				auto nextGridFrame = dynamic_cast<T*>(next);
				if (nullptr != nextGridFrame)
				{
					ASSERT(!nextGridFrame->IsWindowVisible());
					ASSERT(curSession.IsValid());
					if (nextGridFrame->GetSessionInfo().m_SavedInfo.Matches(curSession)
						&& nextGridFrame->CheckModuleCriteria(p_ModuleCriteria))
					{
						frame->SendMessage(WM_COMMAND, ID_GRIDFRAMEBASE_NEXTANDREFRESH);
						return false;
					}
				}
			}
		}*/

		vector<T*> matchingFrames;
		for (const auto& pFrame : GetGridFrames())
		{
			T* pTestFrm = dynamic_cast<T*>(pFrame);
			// pTestFrm->IsWindowVisible() => Temporary way to handle new back/forward system. We should maybe do it differently.
			if (nullptr != pTestFrm && pTestFrm->IsWindowVisible())
			{
				if (curSession == pTestFrm->GetSapio365Session()
					&& pTestFrm->CheckModuleCriteria(p_ModuleCriteria)
					&& (nullptr == p_Options && !pTestFrm->GetOptions() || nullptr != p_Options && pTestFrm->GetOptions() == *p_Options)
					)
					matchingFrames.push_back(pTestFrm);
			}
		}

		std::sort(matchingFrames.begin(), matchingFrames.end(), [](const T* first, const T* second) { return first->GetCreationDate() > second->GetCreationDate(); });

		if (!matchingFrames.empty())
		{
			const auto& frameAutomationName = matchingFrames.front()->GetAutomationName();
			bool saveUserChoice = false;
			bool canceled = false;
			bool forceCreateNewFrame = false;
			if (!*HideNewWindowDialogSetting(frameAutomationName).Get())
			{
				DlgOpenFrameForReuse msgBox(p_SourceWindow.GetCWnd());

				const auto res = msgBox.DoModal();
				if (DlgOpenFrame::CreateNewWindow::g_ID == res)
					forceCreateNewFrame = true;
				else if (DlgOpenFrame::ReuseWindowFrame::g_ID == res)
					forceCreateNewFrame = false;
				else
					canceled = true;

				saveUserChoice = msgBox.GetCheckboxState();
			}
			else
			{
				forceCreateNewFrame = *CreateNewFrameSetting(frameAutomationName).Get();
			}

			if (canceled)
			{
				shouldCreateNewFrame = false;
			}
			else
			{
				if (forceCreateNewFrame)
					matchingFrames.clear();

				if (saveUserChoice)
				{
					HideNewWindowDialogSetting(frameAutomationName).Set(true);
					CreateNewFrameSetting(frameAutomationName).Set(forceCreateNewFrame);
				}

				if (!forceCreateNewFrame) // == Reuse existing frame
				{
					auto autoReload = AutoReloadSetting().Get();
					ASSERT(autoReload); // Default is false, see AutoReloadSetting ctor

					for (auto& matchingFrame : matchingFrames)
					{
						if (::IsWindow(matchingFrame->m_hWnd))
						{
							if (MFCUtil::IsMinimized(matchingFrame->m_hWnd))
								ShowWindow(matchingFrame->m_hWnd, SW_RESTORE);
							else
								matchingFrame->BringWindowToTop();
							if (*autoReload)
								matchingFrame->SendMessage(WM_COMMAND, MAKELONG(ID_365REFRESH, 0), NULL);
							shouldCreateNewFrame = false;
							break;
						}
					}
				}
			}
		}

		if (!shouldCreateNewFrame && matchingFrames.empty())
			LoggerService::User(YtriaTranslate::Do(ModuleBase_ShouldCreateFrame_1, _YLOC("The window that should have been reused has been closed. Creating a new one.")).c_str(), nullptr != frame ? frame->m_hWnd : nullptr);
	}

	return shouldCreateNewFrame;
}

template <class T>
void ModuleBase::showAttachments(std::shared_ptr<BusinessObjectManager> p_BOM, const std::vector<AttachmentsInfo::IDs>& p_IDs, T p_Frame, Origin p_Origin, const Command& p_Command)
{
	ASSERT(nullptr != p_Frame);
	if (nullptr != p_Frame)
	{
		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleBase_showAttachments_1, _YLOC("Show Attachments")).c_str(), p_Frame, p_Command, false);

		auto& p_AutomationSetup = p_Command.GetAutomationSetup();
		YSafeCreateTask([this, p_IDs, hwnd = p_Frame->GetSafeHwnd(), taskData, p_Origin, p_AutomationSetup, p_BOM]()
		{
            auto allAttachments = retrieveAttachments(p_BOM, p_IDs, p_Origin, taskData);

			YDataCallbackMessage<ModuleBase::AttachmentsContainer>::DoPost(allAttachments, [taskData, hwnd, p_AutomationSetup, isCanceled = taskData.IsCanceled()](const ModuleBase::AttachmentsContainer& p_BusinessAttachments)
			{
				if (ShouldBuildView(hwnd, isCanceled, taskData, false))
				{
					auto frame = dynamic_cast<T>(CWnd::FromHandle(hwnd));
					frame->GetGrid().ClearLog(taskData.GetId());
					frame->ViewAttachments(p_BusinessAttachments);
					frame->TaskFinished(taskData, p_AutomationSetup.m_ActionToExecute, hwnd);
				}
				else
				{
					TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
					SetAutomationGreenLight(p_AutomationSetup.m_ActionToExecute);
				}
			});			
		});
	}
}

template <class T>
void ModuleBase::downloadAttachments(std::shared_ptr<BusinessObjectManager> p_BOM, const AttachmentsInfo& p_AttachmentInfos, T p_Frame, Origin p_Origin, const Command& p_Command)
{
	ASSERT(nullptr != p_Frame);
	if (nullptr != p_Frame)
	{
		auto taskData	= addFrameTask(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_18, _YLOC("Download Attachments")).c_str(), p_Frame, p_Command, false);

		auto& p_AutomationSetup = p_Command.GetAutomationSetup();
        const wstring basePath	= !Str::endsWith(p_AttachmentInfos.m_DownloadFolderName, _YTEXT("\\")) ? p_AttachmentInfos.m_DownloadFolderName + _YTEXT("\\")
								: p_AttachmentInfos.m_DownloadFolderName;

		YSafeCreateTask([p_AttachmentInfos, hwnd = p_Frame->GetSafeHwnd(), taskData, p_Origin, p_AutomationSetup, p_BOM, basePath]()
		{
			auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("attachment"), p_AttachmentInfos.m_AttachmentInfos.size());
			for (const auto& attachmentInfo : p_AttachmentInfos.m_AttachmentInfos)
			{
				logger->IncrementObjCount();
				logger->SetContextualInfo(attachmentInfo.m_IDs.AttachmentID());

				ASSERT(attachmentInfo.m_IsFileAttachment);
				if (attachmentInfo.m_IsFileAttachment)
				{
					pplx::task<BusinessFileAttachment> taskResult;

					if (p_Origin == Origin::Message)
						taskResult = p_BOM->GetMessageFileAttachment(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), attachmentInfo.m_IDs.AttachmentID(), logger, taskData);
					else if (p_Origin == Origin::User)
						taskResult = p_BOM->GetUserEventFileAttachment(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), attachmentInfo.m_IDs.AttachmentID(), logger, taskData);
					else if (p_Origin == Origin::Group)
						taskResult = p_BOM->GetGroupEventFileAttachment(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), attachmentInfo.m_IDs.AttachmentID(), logger, taskData);
					else if (p_Origin == Origin::Conversations)
						taskResult = p_BOM->GetPostFileAttachment(attachmentInfo.m_IDs.UserOrGroupID(), attachmentInfo.m_IDs.EventOrMessageOrConversationID(), attachmentInfo.m_IDs.ThreadID(), attachmentInfo.m_IDs.PostID(), attachmentInfo.m_IDs.AttachmentID(), logger, taskData);

					AttachmentsInfo::Infos attachment = attachmentInfo;
					attachment.m_FileAttachment = taskResult.get();

					wstring columnsPath = attachmentInfo.m_ColumnsPath;
					attachment.m_FileAttachment.ToFile(basePath + columnsPath, p_AttachmentInfos.m_FileExistsAction);
				}
			}

			YCallbackMessage::DoPost([taskData, hwnd, p_AutomationSetup, openFolder = p_AttachmentInfos.m_OpenFolder, basePath]()
			{
                if (openFolder)
                    ::ShellExecute(NULL, _YTEXT("open"), basePath.c_str(), NULL, NULL, SW_SHOWNORMAL);

				if (::IsWindow(hwnd))
				{
					auto frame = dynamic_cast<T>(CWnd::FromHandle(hwnd));
					frame->GetGrid().ClearLog(taskData.GetId());
				}
				TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
				SetAutomationGreenLight(p_AutomationSetup.m_ActionToExecute);
			});
		});	
	}
}

template <class T>
void ModuleBase::showItemAttachment(std::shared_ptr<BusinessObjectManager> p_BOM, const AttachmentsInfo::IDs& p_ItemAttachment, Origin p_Origin, T p_Frame, const Command& p_Command)
{
	ASSERT(nullptr != p_Frame);
	if (nullptr != p_Frame)
	{
		HWND hwnd = p_Frame->GetSafeHwnd();
		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleBase_showItemAttachment_1, _YLOC("Get Item Attachment")).c_str(), p_Frame, p_Command, false);

		auto& p_AutomationSetup = p_Command.GetAutomationSetup();
		YSafeCreateTask([p_ItemAttachment, hwnd = p_Frame->GetSafeHwnd(), taskData, p_Origin, p_AutomationSetup, p_BOM]()
		{
			BusinessItemAttachment itemAttachment;

			auto logger = std::make_shared<BasicRequestLogger>(_T("attachment"));

			ASSERT(p_Origin == Origin::Message || p_Origin == Origin::User || p_Origin == Origin::Group || p_Origin == Origin::Post);
			if (p_Origin == Origin::Message)
				itemAttachment = p_BOM->GetMessageItemAttachment(p_ItemAttachment.UserOrGroupID(), p_ItemAttachment.EventOrMessageOrConversationID(), p_ItemAttachment.AttachmentID(), logger, taskData).GetTask().get();
			else if (p_Origin == Origin::User)
				itemAttachment = p_BOM->GetUserEventItemAttachment(p_ItemAttachment.UserOrGroupID(), p_ItemAttachment.EventOrMessageOrConversationID(), p_ItemAttachment.AttachmentID(), logger, taskData).GetTask().get();
			else if (p_Origin == Origin::Group)
				itemAttachment = p_BOM->GetGroupEventItemAttachment(p_ItemAttachment.UserOrGroupID(), p_ItemAttachment.EventOrMessageOrConversationID(), p_ItemAttachment.AttachmentID(), logger, taskData).GetTask().get();
			else if (p_Origin == Origin::Post)
				itemAttachment = p_BOM->GetGroupPostItemAttachment(p_ItemAttachment.UserOrGroupID(), p_ItemAttachment.EventOrMessageOrConversationID(), p_ItemAttachment.ThreadID(), p_ItemAttachment.PostID(), p_ItemAttachment.AttachmentID(), logger, taskData).GetTask().get();

			YDataCallbackMessage<BusinessItemAttachment>::DoPost(itemAttachment, [hwnd, taskData, p_AutomationSetup, isCanceled = taskData.IsCanceled()](const BusinessItemAttachment& p_Attachment)
			{
				if (ShouldBuildView(hwnd, isCanceled, taskData, false))
				{
					auto frame = dynamic_cast<T>(CWnd::FromHandle(hwnd));
					frame->GetGrid().ClearLog(taskData.GetId());
					frame->ShowItemAttachment(p_Attachment);
				}
				SetAutomationGreenLight(p_AutomationSetup.m_ActionToExecute);
				TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
			});
		});
	}
}

template <class T>
void ModuleBase::deleteAttachments(std::shared_ptr<BusinessObjectManager> p_BOM, const AttachmentsInfo& p_AttachmentInfos, Origin p_Origin, T p_Frame, const Command& p_Command)
{
	ASSERT(nullptr != p_Frame);
	if (nullptr != p_Frame)
	{
		auto taskData = addFrameTask(YtriaTranslate::Do(GridEvents_InitializeCommands_13, _YLOC("Delete Attachments")).c_str(), p_Frame, p_Command, false);

		auto& p_AutomationSetup = p_Command.GetAutomationSetup();
		YSafeCreateTask([p_AttachmentInfos, hwnd = p_Frame->GetSafeHwnd(), taskData, p_Origin, p_AutomationSetup, p_BOM]()
		{
			AttachmentsInfo attachmentsToDelete = p_AttachmentInfos;
			attachmentsToDelete.m_AttachmentInfos = {};

			for (const auto& attachmentInfo : p_AttachmentInfos.m_AttachmentInfos)
			{
				pplx::task<HttpResultWithError> taskResult;

				if (p_Origin == Origin::Message)
					taskResult = p_BOM->DeleteUserMessageAttachment(attachmentInfo.m_Id1, attachmentInfo.m_Id2, attachmentInfo.m_Id3, taskData);
				else if (p_Origin == Origin::User)
					taskResult = p_BOM->DeleteUserEventAttachment(attachmentInfo.m_Id1, attachmentInfo.m_Id2, attachmentInfo.m_Id3, taskData);
				else if (p_Origin == Origin::Group)
					taskResult = p_BOM->DeleteGroupEventAttachment(attachmentInfo.m_Id1, attachmentInfo.m_Id2, attachmentInfo.m_Id3, taskData);
				else if (p_Origin == Origin::Conversations)
					taskResult = p_BOM->DeleteGroupPostAttachment(attachmentInfo.m_Id1, attachmentInfo.m_Id2, attachmentInfo.m_Id3, attachmentInfo.m_Id4, taskData);

				taskResult.wait();
			}

			SetAutomationGreenLight(p_AutomationSetup.m_ActionToExecute);
			YDataCallbackMessage<AttachmentsInfo>::DoPost(attachmentsToDelete, [taskData, hwnd, p_AutomationSetup, attachmentsToDelete, isCanceled = taskData.IsCanceled()](const AttachmentsInfo& p_BusinessAttachments)
			{
				if (ShouldBuildView(hwnd, isCanceled, taskData, false))
				{
					auto frame = dynamic_cast<T>(CWnd::FromHandle(hwnd));
					frame->GetGrid().ClearLog(taskData.GetId());
				}
				TaskDataManager::Get().RemoveTaskData(taskData.GetId());
				SetAutomationGreenLight(p_AutomationSetup.m_ActionToExecute);
			});
		});
	}
}

template <class BusinessType>
bool ModuleBase::everythingCanceled(const vector<BusinessType>& p_Vec)
{
	return !std::any_of(p_Vec.begin(), p_Vec.end(), [](const BusinessType& p_Val)
	{
		return !p_Val.HasFlag(BusinessObject::Flag::CANCELED);
	});
}

template <class BusinessType1, class BusinessType2>
bool ModuleBase::everythingCanceled(const O365DataMap<BusinessType1, vector<BusinessType2>>& p_Map)
{
	return !std::any_of(p_Map.begin(), p_Map.end(), [](const O365DataMap<BusinessType1, vector<BusinessType2>>::value_type& p_Val)
	{
		return !p_Val.first.HasFlag(BusinessObject::Flag::CANCELED);
	});
}

template<class FrameT>
void ModuleBase::LoadSnapshot(const Command& p_Command, FrameCreator<FrameT> p_FrameCreator/* = nullptr*/)
{
	using LoaderType = std::shared_ptr<GridSnapshot::Loader>;
	if (p_Command.GetCommandInfo().Data().is_type<LoaderType>())
	{
		auto loader = p_Command.GetCommandInfo().Data().get_value<LoaderType>();
		auto sourceWindow = p_Command.GetSourceWindow();

		const auto& metadata = loader->GetMetadata();

		{
			CWaitCursor _;
			ModuleCriteria criteria;
			if (metadata.m_Criteria)
			{
				auto success = criteria.Deserialize(*metadata.m_Criteria);
			}

			boost::YOpt<ModuleOptions> moduleOptions;
			if (metadata.m_ModuleOptions)
			{
				moduleOptions.emplace();
				auto success = moduleOptions->Deserialize(*metadata.m_ModuleOptions);
			}

			// No session by default.
			ASSERT(p_FrameCreator);
			auto frame = p_FrameCreator(metadata, criteria, p_Command.GetLicenseContext(), /*p_Command.GetSessionIdentifier()*/{ _YTEXT(""), _YTEXT("") , -1 }, *this, sourceWindow.GetHistoryMode(), sourceWindow.GetCFrameWndForHistory());
			frame->InitModuleCriteria(criteria);
			if (moduleOptions)
				frame->SetOptions(*moduleOptions);
			AddGridFrame(frame);
			frame->SetSnapshotMode(true);
			frame->Erect();
			frame->LoadSnaphot(loader, (metadata.m_Mode && GridSnapshot::Options::Mode::RestorePoint == *metadata.m_Mode) ? p_Command.GetSessionIdentifier() : SessionIdentifier{ _YTEXT(""), _YTEXT(""), -1 });
			frame->UpdateContext(YtriaTaskData(), frame->m_hWnd);
		}
	}
	else
	{
		ASSERT(false);
	}
}

template<class FrameT>
void ModuleBase::LoadSnapshot(const Command& p_Command, const PooledString& p_FrameTitle)
{
	LoadSnapshot<FrameT>(p_Command, [&p_FrameTitle](const GridSnapshot::Metadata&/* p_SnapshotMeta*/, const ModuleCriteria&/* p_ModuleCriteria*/, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
		{
			return new FrameT(p_LicenseContext, p_SessionIdentifier, p_FrameTitle, p_Module, p_HistoryMode, p_PreviousFrame);
		});
}

