#pragma once

#include "IRequester.h"

#include "BusinessMailFolder.h"
#include "MailFolderDeserializer.h"
#include "MessageListRequester.h"

// Get all messages in all mailfolders for the specified user id
class MessagesDeepHierarchyRequester : public IRequester
{
public:
	using DeserializerType = ValueListDeserializer<BusinessMessage, MessageDeserializer>;

	MessagesDeepHierarchyRequester(const wstring& p_UserId, const std::shared_ptr<IPageRequestLogger>& p_Logger);

	void SetMessagesCutOffDateTime(const wstring& p_CutOffDateTime);
	void SetMessagesCustomFilter(const ODataFilter& p_CustomFilter);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	vector<BusinessMailFolder>& GetData();

private:
	std::shared_ptr<vector<BusinessMailFolder>> m_MailFolders;
	std::shared_ptr<IPageRequestLogger> m_Logger;

	wstring m_UserId;
	wstring m_CutOffDateTime;
	ODataFilter m_CustomFilter;
};

