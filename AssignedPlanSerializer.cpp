#include "AssignedPlanSerializer.h"

#include "JsonSerializeUtil.h"

AssignedPlanSerializer::AssignedPlanSerializer(const AssignedPlan& p_AssignedPlan)
    :m_AssignedPlan(p_AssignedPlan)
{
}

void AssignedPlanSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("assignedDateTime"), m_AssignedPlan.m_AssignedDateTime, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("capabilityStatus"), m_AssignedPlan.m_CapabilityStatus, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("service"), m_AssignedPlan.m_Service, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("servicePlanId"), m_AssignedPlan.m_ServicePlanId, obj);
}
