#pragma once

#include "ModuleBase.h"

class FrameSpGroups;

class ModuleSpGroups : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

private:
    virtual void executeImpl(const Command& p_Command) override;

    void doRefresh(FrameSpGroups* p_pFrame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command);

    void showSpGroups(const Command& p_Command);
};

