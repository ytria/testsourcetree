#include "LinkHandlerEvents.h"

#include "Command.h"
#include "CommandInfo.h"
#include "CommandDispatcher.h"
#include "FrameEvents.h"
#include "Office365Admin.h"

void LinkHandlerEvents::Handle(YBrowserLink & p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

    CommandInfo info;
    info.SetOrigin(Origin::User);
	p_Link.ENABLE_FREE_ACCESS();// free access
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Event, Command::ModuleTask::ListMe, info, { nullptr, g_ActionNameMyDataCalendar, nullptr, p_Link.GetAutomationAction() }));
}

bool LinkHandlerEvents::IsAllowedWithAJLLicense() const
{
	return true; // only MyData is allowed
}