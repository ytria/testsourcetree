#include "FrameSignIns.h"

IMPLEMENT_DYNAMIC(FrameSignIns, GridFrameBase)


FrameSignIns::FrameSignIns(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
	m_CreateRefreshPartial = false;
}

void FrameSignIns::BuildView(const vector<SignIn>& p_SignIns, bool p_FullPurge, bool p_NoPurge)
{
	m_Grid.BuildView(p_SignIns, p_FullPurge, p_NoPurge);
}

O365Grid& FrameSignIns::GetGrid()
{
	return m_Grid;
}

void FrameSignIns::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	if (GetOptions())
		info.Data2() = *GetOptions();

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::SignIns, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameSignIns::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{

}

bool FrameSignIns::HasApplyButton()
{
	return false;
}

void FrameSignIns::createGrid()
{
	m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

//GridFrameBase::O365ControlConfig FrameSignIns::GetRefreshPartialControlConfig()
//{
//	return GetRefreshPartialControlConfigUsers();
//}

bool FrameSignIns::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(tab);
	CXTPRibbonGroup* refreshGroup = CreateRefreshGroup(tab);

	ASSERT(nullptr != refreshGroup);
	if (nullptr != refreshGroup)
	{
		CXTPControl* ctrl = refreshGroup->Add(xtpControlButton, ID_SIGNINGRID_CHANGEOPTIONS);
		setImage({ ID_SIGNINGRID_CHANGEOPTIONS }, IDB_CHANGEMODULEOPTIONS, xtpImageNormal);
		setImage({ ID_SIGNINGRID_CHANGEOPTIONS }, IDB_CHANGEMODULEOPTIONS_16x16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	automationAddCommandIDToGreenLight(ID_SIGNINGRID_CHANGEOPTIONS);

	return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameSignIns::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	return statusRV;
}
