#pragma once

#include "PersistentSession.h"
#include "Sapio365Session.h"
#include "SessionIdentifier.h"
#include "YSafeCreateTask.h"

class ISessionLoader
{
public:
	ISessionLoader(const SessionIdentifier& p_Id);
	virtual ~ISessionLoader() = default;
	const SessionIdentifier& GetSessionId() const;

protected:
	virtual TaskWrapper<std::shared_ptr<Sapio365Session>> Load(const std::shared_ptr<Sapio365Session>& p_SapioSession) = 0;
	PersistentSession GetPersistentSession(const std::shared_ptr<Sapio365Session>& p_Session);

	SessionIdentifier& GetSessionId();

	friend class SessionSwitcher;

private:
	SessionIdentifier m_SessionId;
	boost::YOpt<Sapio365Session> m_ExistingSession;
};

