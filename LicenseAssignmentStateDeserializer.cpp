#include "LicenseAssignmentStateDeserializer.h"

#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"

void LicenseAssignmentStateDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("assignedByGroup"), m_Data.m_AssignedByGroup, p_Object);
	{
		ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("disabledPlans"), p_Object))
			m_Data.m_DisabledPlans = std::move(lsd.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("error"), m_Data.m_Error, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("skuId"), m_Data.m_SkuId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("state"), m_Data.m_State, p_Object);
}
