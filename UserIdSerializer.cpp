#include "UserIdSerializer.h"

#include "UserId.h"
#include "XmlSerializeUtil.h"

Ex::UserIdSerializer::UserIdSerializer(XERCES_CPP_NAMESPACE::DOMDocument& p_Document, const Ex::UserId& p_UserId)
    : IXmlSerializer(p_Document),
    m_UserId(p_UserId)
{
}

void Ex::UserIdSerializer::SerializeImpl(DOMElement& p_Element)
{
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("SID"), m_UserId.m_SID);
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("PrimarySmtpAddress"), m_UserId.m_PrimarySmtpAddress);
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("DisplayName"), m_UserId.m_DisplayName);
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("DistinguishedUser"), m_UserId.m_DistinguishedUser);
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("ExternalUserIdentity"), m_UserId.m_ExternalUserIdentity);
}
