#include "Command.h"

#include "AutomationRecording.h"
#include "BaseO365Grid.h"
#include "CacheGrid.h"
#include "DlgOpenFrame.h"
#include "GridFrameBase.h"
#include "GridUtil.h"
#include "HistoryFrame.h"
#include "IActionCommand.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "ModuleBase.h"
#include "Office365Admin.h"
#include "Registry.h"
#include "Sapio365Settings.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTranslate.h"

namespace
{
#define TARGET_TASK(_Target, _Task) (uint32_t(_Target) | (uint32_t(_Task) << 16))
	constexpr const wstring& GetModuleAutoName(const Command::ModuleTarget p_Target, const Command::ModuleTask p_Task)
	{
		switch (TARGET_TASK(p_Target, p_Task))
		{
		case TARGET_TASK(Command::ModuleTarget::Applications, Command::ModuleTask::List):
			return GridUtil::g_AutoNameApplications;
			break;
		case TARGET_TASK(Command::ModuleTarget::Contact, Command::ModuleTask::ListMe):
		case TARGET_TASK(Command::ModuleTarget::Contact, Command::ModuleTask::List):
			return GridUtil::g_AutoNameContacts;
			break;
		case TARGET_TASK(Command::ModuleTarget::Conversation, Command::ModuleTask::List):
			return GridUtil::g_AutoNameConversations;
			break;
		case TARGET_TASK(Command::ModuleTarget::Channel, Command::ModuleTask::List):
			return GridUtil::g_AutoNameChannels;
			break;
		case TARGET_TASK(Command::ModuleTarget::ChannelMembers, Command::ModuleTask::List):
			return GridUtil::g_AutoNamePrivateChannelMembers;
			break;
		case TARGET_TASK(Command::ModuleTarget::ChannelMessages, Command::ModuleTask::List):
			return GridUtil::g_AutoNameChannelMessages;
			break;
		case TARGET_TASK(Command::ModuleTarget::Columns, Command::ModuleTask::List):
			return GridUtil::g_AutoNameColumns;
			break;
		case TARGET_TASK(Command::ModuleTarget::ClassMembers, Command::ModuleTask::List):
			return GridUtil::g_AutoNameClassMembers;
			break;
		case TARGET_TASK(Command::ModuleTarget::DirectoryRoles, Command::ModuleTask::List):
			return GridUtil::g_AutoNameDirectoryRoles;
			break;
		case TARGET_TASK(Command::ModuleTarget::DirectoryAudit, Command::ModuleTask::List):
			return GridUtil::g_AutoNameDirectoryAudits;
			break;
		case TARGET_TASK(Command::ModuleTarget::Drive, Command::ModuleTask::ListMe):
		case TARGET_TASK(Command::ModuleTarget::Drive, Command::ModuleTask::ListSubItems):
		case TARGET_TASK(Command::ModuleTarget::Drive, Command::ModuleTask::Search):
		case TARGET_TASK(Command::ModuleTarget::Drive, Command::ModuleTask::SearchMe):
		case TARGET_TASK(Command::ModuleTarget::Drive, Command::ModuleTask::SearchSubItems):
			return GridUtil::g_AutoNameDriveItems;
			break;
		case TARGET_TASK(Command::ModuleTarget::EduUsers, Command::ModuleTask::List):
			return GridUtil::g_AutoNameEduUsers;
			break;
		case TARGET_TASK(Command::ModuleTarget::Event, Command::ModuleTask::List):
		case TARGET_TASK(Command::ModuleTarget::Event, Command::ModuleTask::ListMe):
			return GridUtil::g_AutoNameEvents;
			break;
		case TARGET_TASK(Command::ModuleTarget::Group, Command::ModuleTask::List):
		case TARGET_TASK(Command::ModuleTarget::Group, Command::ModuleTask::ListDetails):
		case TARGET_TASK(Command::ModuleTarget::Group, Command::ModuleTask::ListWithPrefilter):
			return GridUtil::g_AutoNameGroups;
			break;
		case TARGET_TASK(Command::ModuleTarget::Group, Command::ModuleTask::ListSubItems):
			return GridUtil::g_AutoNameGroupsHierarchy;
			break;
		case TARGET_TASK(Command::ModuleTarget::Group, Command::ModuleTask::ListOwners):
			return GridUtil::g_AutoNameGroupsOwnersHierarchy;
			break;
		case TARGET_TASK(Command::ModuleTarget::Group, Command::ModuleTask::ListAuthors):
			return GridUtil::g_AutoNameGroupsAuthorsHierarchy;
			break;
		case TARGET_TASK(Command::ModuleTarget::Group, Command::ModuleTask::ListRecycleBin):
			return GridUtil::g_AutoNameGroupsRecycleBin;
			break;
		case TARGET_TASK(Command::ModuleTarget::Licenses, Command::ModuleTask::List):
		case TARGET_TASK(Command::ModuleTarget::Licenses, Command::ModuleTask::ListMe):
			return GridUtil::g_AutoNameLicenses;
			break;
		case TARGET_TASK(Command::ModuleTarget::Lists, Command::ModuleTask::List):
			return GridUtil::g_AutoNameLists;
			break;
		case TARGET_TASK(Command::ModuleTarget::ListsItem, Command::ModuleTask::List):
			return GridUtil::g_AutoNameListsContent;
			break;
		case TARGET_TASK(Command::ModuleTarget::Message, Command::ModuleTask::List):
		case TARGET_TASK(Command::ModuleTarget::Message, Command::ModuleTask::ListMe):
			return GridUtil::g_AutoNameMessages;
			break;
		case TARGET_TASK(Command::ModuleTarget::MessageRules, Command::ModuleTask::List):
		case TARGET_TASK(Command::ModuleTarget::MessageRules, Command::ModuleTask::ListMe):
			return GridUtil::g_AutoNameMessageRules;
			break;
		case TARGET_TASK(Command::ModuleTarget::Post, Command::ModuleTask::List):
			return GridUtil::g_AutoNamePosts;
			break;
		case TARGET_TASK(Command::ModuleTarget::Reports, Command::ModuleTask::List):
			return GridUtil::g_AutoNameUsageReports;
			break;
		case TARGET_TASK(Command::ModuleTarget::SignIns, Command::ModuleTask::List):
			return GridUtil::g_AutoNameSignIns;
			break;
		case TARGET_TASK(Command::ModuleTarget::Sites, Command::ModuleTask::List):
			return GridUtil::g_AutoNameSites;
			break;
		case TARGET_TASK(Command::ModuleTarget::SiteRoles, Command::ModuleTask::List):
			return GridUtil::g_AutoNameSiteRoles;
			break;
		case TARGET_TASK(Command::ModuleTarget::User, Command::ModuleTask::List):
		case TARGET_TASK(Command::ModuleTarget::User, Command::ModuleTask::ListDetails):
		case TARGET_TASK(Command::ModuleTarget::User, Command::ModuleTask::ListWithPrefilter):
			return GridUtil::g_AutoNameUsers;
			break;
		case TARGET_TASK(Command::ModuleTarget::User, Command::ModuleTask::ListOwners):
		case TARGET_TASK(Command::ModuleTarget::User, Command::ModuleTask::ListMyOwners):
			return GridUtil::g_AutoNameUserGroups;
			break;
		case TARGET_TASK(Command::ModuleTarget::User, Command::ModuleTask::ListManagerHierarchy):
			return GridUtil::g_AutoNameManagerHierarchy;
			break;
		case TARGET_TASK(Command::ModuleTarget::User, Command::ModuleTask::ListRecycleBin):
			return GridUtil::g_AutoNameUsersRecycleBin;
			break;
		case TARGET_TASK(Command::ModuleTarget::Schools, Command::ModuleTask::List):
			return GridUtil::g_AutoNameSchools;
			break;
		case TARGET_TASK(Command::ModuleTarget::SpUser, Command::ModuleTask::List):
			return GridUtil::g_AutoNameSpUsers;
			break;
		case TARGET_TASK(Command::ModuleTarget::SpGroup, Command::ModuleTask::List):
			return GridUtil::g_AutoNameSpGroups;
			break;
		case TARGET_TASK(Command::ModuleTarget::SitePermissions, Command::ModuleTask::List):
			return GridUtil::g_AutoNameSitePermissions;
			break;
		case TARGET_TASK(Command::ModuleTarget::MailboxPermissions, Command::ModuleTask::List):
		case TARGET_TASK(Command::ModuleTarget::MailboxPermissions, Command::ModuleTask::ListMe):
			return GridUtil::g_AutoNameMailboxPermissions;
			break;
		case TARGET_TASK(Command::ModuleTarget::OnPremiseUsers, Command::ModuleTask::List):
			return GridUtil::g_AutoNameOnPremiseUsers;
			break;
		case TARGET_TASK(Command::ModuleTarget::OnPremiseGroups, Command::ModuleTask::List):
			return GridUtil::g_AutoNameOnPremiseGroups;
			break;
		};

		ASSERT(false);
		return Str::g_EmptyString;
	}
#undef TARGET_TASK
}

const wstring Command::g_KeyHistoryMode = _YTEXT("HistoryMode");

Command::Command(const LicenseContext& p_LicenseContext, HistorySourceWindow sourceHwnd, const ModuleTarget p_Target, const ModuleTask p_Task, const CommandInfo& p_CommandInfo, const AutomationSetup& p_AutomationSetup)
	: m_Target(p_Target)
	, m_Task(p_Task)
	, m_CommandInfo(p_CommandInfo)
	, m_SourceHwnd(sourceHwnd)
	, m_AutomationSetup(p_AutomationSetup)
	, m_LicenseContext(p_LicenseContext)
{
	ASSERT(m_SourceHwnd.GetHWND());// gib me dat - automation

	m_LicenseContext.SetModuleForTokenCharging(GetModuleAutoName(p_Target, p_Task));

	srand (static_cast<int>(time(NULL)));
}

const SessionIdentifier& Command::GetSessionIdentifier() const
{
	static const SessionIdentifier emptyId(_YTEXT(""), _YTEXT(""), 0);

	if (!m_SessionIdentifier)
	{
		auto sourceFrame = GetCommandInfo().GetFrame();
		ASSERT(nullptr == sourceFrame || nullptr == dynamic_cast<GridFrameBase*>(MFCUtil::FindFirstParentOfType<CFrameWnd>(GetSourceCWnd())) || dynamic_cast<GridFrameBase*>(MFCUtil::FindFirstParentOfType<CFrameWnd>(GetSourceCWnd())) == sourceFrame);
		if (nullptr == sourceFrame)
			sourceFrame = dynamic_cast<GridFrameBase*>(MFCUtil::FindFirstParentOfType<CFrameWnd>(GetSourceCWnd()));

		if (nullptr != sourceFrame)
		{
			auto graphSession = sourceFrame->GetSapio365Session();
			if (graphSession)
				m_SessionIdentifier = graphSession->GetIdentifier();
		}

		if (!m_SessionIdentifier)
		{
			std::shared_ptr<Sapio365Session> graphSession = MainFrameSapio365Session();
			if (graphSession)
				m_SessionIdentifier = graphSession->GetIdentifier();
		}
	}

	return m_SessionIdentifier ? *m_SessionIdentifier : emptyId;
}

void Command::SetSessionIdentifier(const boost::YOpt<SessionIdentifier>& p_SessionIdentifier) const
{
	m_SessionIdentifier = p_SessionIdentifier;
}

Command::ModuleTarget Command::GetTarget() const
{
	return m_Target;
}

Command::ModuleTask Command::GetTask() const
{
	return m_Task;
}

const CommandInfo& Command::GetCommandInfo() const
{
    return m_CommandInfo;
}

CommandInfo& Command::GetCommandInfo()
{
    return m_CommandInfo;
}

Command::HistorySourceWindow Command::GetSourceWindow() const
{
	return m_SourceHwnd;
}

CWnd* Command::GetSourceCWnd() const
{
	return GetSourceWindow().GetCWnd();
}

void Command::SetAutomationAction(AutomationAction* p_Action) const
{
	m_AutomationSetup.m_ActionToExecute = p_Action;
}

AutomationAction* Command::GetAutomationAction() const
{
	return m_AutomationSetup.m_ActionToExecute;
}

void Command::SetAutomationActionRecording(AutomationAction* p_ActionRecording)
{
	m_AutomationSetup.m_ActionToRecord = p_ActionRecording;
}

AutomationAction* Command::GetAutomationActionRecording() const
{
	return m_AutomationSetup.m_ActionToRecord;
}

const wstring& Command::GetAutomationRecordingActionName() const
{
	return m_AutomationSetup.m_ActionToRecordName;
}

AutomationSetup& Command::GetAutomationSetup() const
{
	return m_AutomationSetup;
}

void Command::SetAutomationSetup(const AutomationSetup& p_AutomationSetup)
{
	m_AutomationSetup = p_AutomationSetup;
}

AutomationAction* Command::AutomationRecord() const
{
	ASSERT(nullptr == m_AutomationSetup.m_ActionToRecord);// recording the same action more than once is gay
	m_AutomationSetup.Record();
	return GetAutomationActionRecording();
}

wstring	Command::ToString() const
{
	static bool labelsInit = false;
	static map<ModuleTarget, wstring>	moduleTargets;
	static map<ModuleTask, wstring>		moduleTasks;
	static map<Status, wstring>			statuses;
	if (!labelsInit)
	{
		moduleTargets[ModuleTarget::ClassMembers]		= _T("Class members");
		moduleTargets[ModuleTarget::Contact]		= YtriaTranslate::Do(Command_ToString_1, _YLOC("Contact")).c_str();
		moduleTargets[ModuleTarget::Conversation]	= YtriaTranslate::Do(Command_ToString_2, _YLOC("Conversation")).c_str();
		moduleTargets[ModuleTarget::Drive]			= YtriaTranslate::Do(Command_ToString_4, _YLOC("Drive")).c_str();
		moduleTargets[ModuleTarget::Event]			= YtriaTranslate::Do(Command_ToString_5, _YLOC("Event")).c_str();
		moduleTargets[ModuleTarget::Group]			= YtriaTranslate::Do(Command_ToString_6, _YLOC("Group")).c_str();
		moduleTargets[ModuleTarget::Mail]			= YtriaTranslate::Do(Command_ToString_7, _YLOC("Mail")).c_str();
		moduleTargets[ModuleTarget::Message]		= YtriaTranslate::Do(Command_ToString_8, _YLOC("Message")).c_str();
		moduleTargets[ModuleTarget::Organization]	= YtriaTranslate::Do(Command_ToString_9, _YLOC("Organization")).c_str();
		moduleTargets[ModuleTarget::User]			= YtriaTranslate::Do(Command_ToString_10, _YLOC("User")).c_str();
		moduleTargets[ModuleTarget::Sites]			= YtriaTranslate::Do(Command_ToString_12, _YLOC("Sites")).c_str();
		moduleTargets[ModuleTarget::Lists]			= YtriaTranslate::Do(Command_ToString_13, _YLOC("Lists")).c_str();
		moduleTargets[ModuleTarget::ListsItem]	    = YtriaTranslate::Do(Command_ToString_14, _YLOC("List Items")).c_str();
		moduleTargets[ModuleTarget::Licenses]		= YtriaTranslate::Do(Command_ToString_15, _YLOC("Licenses")).c_str();

		moduleTasks[ModuleTask::List]				= YtriaTranslate::Do(Command_ToString_20, _YLOC("Show")).c_str();
		moduleTasks[ModuleTask::ListByID]			= YtriaTranslate::Do(Command_ToString_21, _YLOC("Show By ID")).c_str();
		moduleTasks[ModuleTask::ListSubItems]		= YtriaTranslate::Do(Command_ToString_22, _YLOC("Show Items")).c_str();
		moduleTasks[ModuleTask::ListOwners]			= YtriaTranslate::Do(Command_ToString_23, _YLOC("Show Owners")).c_str();
		moduleTasks[ModuleTask::ListAuthors]		= YtriaTranslate::Do(Command_ToString_24, _YLOC("Show Authorized Senders ")).c_str();
		moduleTasks[ModuleTask::UpdateModified]		= YtriaTranslate::Do(Command_ToString_28, _YLOC("Show Modified")).c_str();
		moduleTasks[ModuleTask::UpdateMoreInfo]		= YtriaTranslate::Do(Command_ToString_25, _YLOC("Load Info")).c_str();
		moduleTasks[ModuleTask::UpdateRefresh]		= YtriaTranslate::Do(Command_ToString_26, _YLOC("Refresh")).c_str();

		statuses[Status::Init]				= YtriaTranslate::Do(Command_ToString_16, _YLOC("Initialized")).c_str();
		statuses[Status::Sent]				= YtriaTranslate::Do(Command_ToString_17, _YLOC("Sent")).c_str();
		statuses[Status::SendingFailure]	= YtriaTranslate::Do(Command_ToString_18, _YLOC("Failed to start - unknown task")).c_str();
		statuses[Status::ExecutionFailure]	= YtriaTranslate::Do(Command_ToString_19, _YLOC("Failed to execute")).c_str();

		labelsInit = true;
	}

	CString sourceTitle = YtriaTranslate::Do(Command_ToString_27, _YLOC("Undefined")).c_str();
	CWnd* w = GetSourceCWnd();
	if (nullptr != w)
		w->GetWindowText(sourceTitle);
	return YtriaTranslate::Do(Command_ToString_29, _YLOC("Session: %1 - Command from: %2 [Target: %3 - Task: %4]"), 
					GetSessionIdentifier().m_EmailOrAppId.c_str(),
					sourceTitle.GetBuffer(),
					moduleTargets[m_Target].c_str(), moduleTasks[m_Task].c_str());
}

const LicenseContext& Command::GetLicenseContext() const
{
	return m_LicenseContext;
}

void Command::SetLicenseContext(const LicenseContext& p_LicenseContext)
{
	m_LicenseContext = p_LicenseContext;
}

Command Command::CreateCommand(const std::shared_ptr<IActionCommand>& p_ActionCmd, GridFrameBase& p_Frame, Command::ModuleTarget p_Target)
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);

	CommandInfo cmdData;
	cmdData.GetActionCommand() = p_ActionCmd;

	AutomationSetup setup = { &p_Frame.GetGrid(), p_ActionCmd->GetName(), p_Frame.GetAutomationActionRecording(), nullptr };
	p_ActionCmd->SetAutomationSetup(setup);

	return Command(mainFrame->GetLicenseContext(), p_Frame.m_hWnd, p_Target, Command::ModuleTask::ActionCommand, cmdData, setup);
}

// =================================================================================================

Command::HistorySourceWindow::HistorySourceWindow(HWND p_Hwnd)
	: m_Hwnd(p_Hwnd)
	, m_HistoryMode(nullptr != p_Hwnd ? HistoryMode::GetAndResetMode() : HistoryMode::NOT_SET)
{

}

HWND Command::HistorySourceWindow::GetHWND() const
{
	return m_Hwnd;
}

CWnd* Command::HistorySourceWindow::GetCWnd() const
{
	return ::IsWindow(m_Hwnd) ? CWnd::FromHandle(m_Hwnd) : nullptr;
}

CFrameWnd* Command::HistorySourceWindow::GetCFrameWndForHistory() const
{
	if (ShouldKeepInHistoryIfNewWindow())
		return MFCUtil::FindFirstParentOfType<CFrameWnd>(GetCWnd());
	return nullptr;
}

bool Command::HistorySourceWindow::ShouldKeepInHistoryIfNewWindow() const
{
	ASSERT(HistoryMode::NOT_SET != m_HistoryMode);
	return HistoryMode::SAME_HISTORY == m_HistoryMode || HistoryMode::DETACH_HISTORY == m_HistoryMode;
}

bool Command::HistorySourceWindow::UserConfirmation(AutomationAction* p_Action)
{
	// Always true in automation (nullptr != p_Action)
	// Confirmation should have been requested once at automation startup

	IHistoryFrame* historyFrame = nullptr;
	if (nullptr == p_Action
		&& HistoryMode::SAME_HISTORY == m_HistoryMode
		&& nullptr != (historyFrame = dynamic_cast<IHistoryFrame*>(MFCUtil::FindFirstParentOfType<CFrameWnd>(GetCWnd())))
		&& historyFrame->HasNext())
	{
		bool showDialog = true;

		auto historyMode = HistoryModeSetting().Get();
		if (historyMode)
		{
			if (HistoryMode::g_RegNewHistoryMode == *historyMode)
			{
				m_HistoryMode = HistoryMode::NEW_HISTORY;
				showDialog = false;
			}
			else if (HistoryMode::g_RegDetachHistoryMode == *historyMode)
			{
				m_HistoryMode = HistoryMode::DETACH_HISTORY;
				showDialog = false;
			}
			else if (HistoryMode::HistoryMode::g_RegRetainHistoryMode == *historyMode)
			{
				m_HistoryMode = HistoryMode::SAME_HISTORY;
				showDialog = false;
			}
			else
			{
				ASSERT(false);
			}
		}

		if (showDialog)
		{
			DlgOpenFrameForHistory msgBox(GetCWnd());

			wstring regHistoryMode;
			switch (msgBox.DoModal())
			{
			case IDCANCEL:
				return false;
			case DlgOpenFrame::DetachNewWindow::g_ID:
				m_HistoryMode = HistoryMode::NEW_HISTORY;
				regHistoryMode = HistoryMode::g_RegNewHistoryMode;
				break;
			case DlgOpenFrame::DetachHistory::g_ID:
				m_HistoryMode = HistoryMode::DETACH_HISTORY;
				regHistoryMode = HistoryMode::g_RegDetachHistoryMode;
				break;
			case DlgOpenFrame::OverrideHistory::g_ID:
				regHistoryMode = HistoryMode::g_RegRetainHistoryMode;
				break;
			default:
				ASSERT(false);
				break;
			}

			if (msgBox.GetCheckboxState())
				HistoryModeSetting().Set(regHistoryMode);
		}
	}

	return true;
}

HistoryMode::Mode Command::HistorySourceWindow::GetHistoryMode() const
{
	return m_HistoryMode;
}