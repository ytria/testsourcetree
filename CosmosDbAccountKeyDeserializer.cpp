#include "CosmosDbAccountKeyDeserializer.h"
#include "JsonSerializeUtil.h"

void Azure::CosmosDbAccountKeyDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("primaryMasterKey"), m_Data.m_PrimaryMasterKey, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("secondaryMasterKey"), m_Data.m_SecondaryMasterKey, p_Object);
}
