#pragma once

#include "O365ReportID.h"

const wstring O365Report<O365ReportID::OneDriveActivityUserDetail>::g_AsString				= _YTEXT("OneDriveActivityUserDetail");
const wstring O365Report<O365ReportID::OneDriveUsageAccountDetail>::g_AsString				= _YTEXT("OneDriveUsageAccountDetail");
const wstring O365Report<O365ReportID::TeamsDeviceUsageUserDetail>::g_AsString				= _YTEXT("TeamsDeviceUsageUserDetail");
const wstring O365Report<O365ReportID::TeamsUserActivityUserDetail>::g_AsString				= _YTEXT("TeamsUserActivityUserDetail");
const wstring O365Report<O365ReportID::EmailActivityUserDetail>::g_AsString					= _YTEXT("EmailActivityUserDetail");
const wstring O365Report<O365ReportID::EmailAppUsageUserDetail>::g_AsString					= _YTEXT("EmailAppUsageUserDetail");
const wstring O365Report<O365ReportID::MailboxUsageDetail>::g_AsString						= _YTEXT("MailboxUsageDetail");
const wstring O365Report<O365ReportID::Office365GroupsActivityDetail>::g_AsString			= _YTEXT("Office365GroupsActivityDetail");
const wstring O365Report<O365ReportID::Office365ActiveUserDetail>::g_AsString				= _YTEXT("Office365ActiveUserDetail");
const wstring O365Report<O365ReportID::Office365ActivationsUserDetail>::g_AsString			= _YTEXT("Office365ActivationsUserDetail");
const wstring O365Report<O365ReportID::SharePointActivityUserDetail>::g_AsString			= _YTEXT("SharePointActivityUserDetail");
const wstring O365Report<O365ReportID::SharePointSiteUsageDetail>::g_AsString				= _YTEXT("SharePointSiteUsageDetail");
const wstring O365Report<O365ReportID::SkypeForBusinessActivityUserDetail>::g_AsString		= _YTEXT("SkypeForBusinessActivityUserDetail");
const wstring O365Report<O365ReportID::SkypeForBusinessDeviceUsageUserDetail>::g_AsString	= _YTEXT("SkypeForBusinessDeviceUsageUserDetail");
const wstring O365Report<O365ReportID::YammerActivityUserDetail>::g_AsString				= _YTEXT("YammerActivityUserDetail");
const wstring O365Report<O365ReportID::YammerDeviceUsageUserDetail>::g_AsString				= _YTEXT("YammerDeviceUsageUserDetail");
const wstring O365Report<O365ReportID::YammerGroupsActivityDetail>::g_AsString				= _YTEXT("YammerGroupsActivityDetail");

const Origin O365Report<O365ReportID::OneDriveActivityUserDetail>::g_Origin				= Origin::User;
const Origin O365Report<O365ReportID::OneDriveUsageAccountDetail>::g_Origin				= Origin::User;
const Origin O365Report<O365ReportID::TeamsDeviceUsageUserDetail>::g_Origin				= Origin::User;
const Origin O365Report<O365ReportID::TeamsUserActivityUserDetail>::g_Origin			= Origin::User;
const Origin O365Report<O365ReportID::EmailActivityUserDetail>::g_Origin				= Origin::User;
const Origin O365Report<O365ReportID::EmailAppUsageUserDetail>::g_Origin				= Origin::User;
const Origin O365Report<O365ReportID::MailboxUsageDetail>::g_Origin						= Origin::User;
const Origin O365Report<O365ReportID::Office365GroupsActivityDetail>::g_Origin			= Origin::Group;
const Origin O365Report<O365ReportID::Office365ActiveUserDetail>::g_Origin				= Origin::User;
const Origin O365Report<O365ReportID::Office365ActivationsUserDetail>::g_Origin			= Origin::User;
const Origin O365Report<O365ReportID::SharePointActivityUserDetail>::g_Origin			= Origin::User;
const Origin O365Report<O365ReportID::SharePointSiteUsageDetail>::g_Origin				= Origin::Site;
const Origin O365Report<O365ReportID::SkypeForBusinessActivityUserDetail>::g_Origin		= Origin::User;
const Origin O365Report<O365ReportID::SkypeForBusinessDeviceUsageUserDetail>::g_Origin	= Origin::User;
const Origin O365Report<O365ReportID::YammerActivityUserDetail>::g_Origin				= Origin::User;
const Origin O365Report<O365ReportID::YammerDeviceUsageUserDetail>::g_Origin			= Origin::User;
const Origin O365Report<O365ReportID::YammerGroupsActivityDetail>::g_Origin				= Origin::Group;

template<>
wstring O365Report<O365ReportID::OneDriveActivityUserDetail>::GetDisplayName()
{
	return _T("OneDrive activity");
}
template<>
wstring O365Report<O365ReportID::OneDriveActivityUserDetail>::GetDescription()
{
	return _T("Report of OneDrive activity by user.");
}

template<>
wstring O365Report<O365ReportID::OneDriveActivityUserDetail>::GetShortUniqueName()
{
	return _YTEXT("odaud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::OneDriveUsageAccountDetail>::GetDisplayName()
{
	return _T("OneDrive usage");
}
template<>
wstring O365Report<O365ReportID::OneDriveUsageAccountDetail>::GetDescription()
{
	return _T("Report of OneDrive usage by account.");
}

template<>
wstring O365Report<O365ReportID::OneDriveUsageAccountDetail>::GetShortUniqueName()
{
	return _YTEXT("oduad");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::TeamsDeviceUsageUserDetail>::GetDisplayName()
{
	return _T("Teams device usage");
}
template<>
wstring O365Report<O365ReportID::TeamsDeviceUsageUserDetail>::GetDescription()
{
	return _T("Report of Microsoft Teams device usage by user.");
}

template<>
wstring O365Report<O365ReportID::TeamsDeviceUsageUserDetail>::GetShortUniqueName()
{
	return _YTEXT("tduud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::TeamsUserActivityUserDetail>::GetDisplayName()
{
	return _T("Teams user activity");
}
template<>
wstring O365Report<O365ReportID::TeamsUserActivityUserDetail>::GetDescription()
{
	return _T("Report of Microsoft Teams user activity by user.");
}

template<>
wstring O365Report<O365ReportID::TeamsUserActivityUserDetail>::GetShortUniqueName()
{
	return _YTEXT("tuaud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::EmailActivityUserDetail>::GetDisplayName()
{
	return _T("Email activity");
}
template<>
wstring O365Report<O365ReportID::EmailActivityUserDetail>::GetDescription()
{
	return _T("Report of email activity users have performed.");
}

template<>
wstring O365Report<O365ReportID::EmailActivityUserDetail>::GetShortUniqueName()
{
	return _YTEXT("eaud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::EmailAppUsageUserDetail>::GetDisplayName()
{
	return _T("Email app activity");
}
template<>
wstring O365Report<O365ReportID::EmailAppUsageUserDetail>::GetDescription()
{
	return _T("Report of which activities users performed on the various email apps.");
}

template<>
wstring O365Report<O365ReportID::EmailAppUsageUserDetail>::GetShortUniqueName()
{
	return _YTEXT("eauud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::MailboxUsageDetail>::GetDisplayName()
{
	return _T("Mailbox usage");
}
template<>
wstring O365Report<O365ReportID::MailboxUsageDetail>::GetDescription()
{
	return _T("Report of mailbox usage.");
}

template<>
wstring O365Report<O365ReportID::MailboxUsageDetail>::GetShortUniqueName()
{
	return _YTEXT("mud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::Office365GroupsActivityDetail>::GetDisplayName()
{
	return _T("Office 365 groups activity");
}
template<>
wstring O365Report<O365ReportID::Office365GroupsActivityDetail>::GetDescription()
{
	return _T("Report of Office 365 Groups activity by group.");
}

template<>
wstring O365Report<O365ReportID::Office365GroupsActivityDetail>::GetShortUniqueName()
{
	return _YTEXT("ogad");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::Office365ActiveUserDetail>::GetDisplayName()
{
	return _T("Office 365 active users");
}
template<>
wstring O365Report<O365ReportID::Office365ActiveUserDetail>::GetDescription()
{
	return _T("Report of Office 365 active users.");
}

template<>
wstring O365Report<O365ReportID::Office365ActiveUserDetail>::GetShortUniqueName()
{
	return _YTEXT("oaud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::Office365ActivationsUserDetail>::GetDisplayName()
{
	return _T("Office 365 activations");
}
template<>
wstring O365Report<O365ReportID::Office365ActivationsUserDetail>::GetDescription()
{
	return _T("Report of users who have activated Office 365.");
}

template<>
wstring O365Report<O365ReportID::Office365ActivationsUserDetail>::GetShortUniqueName()
{
	return _YTEXT("oasud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::SharePointActivityUserDetail>::GetDisplayName()
{
	return _T("SharePoint activity");
}
template<>
wstring O365Report<O365ReportID::SharePointActivityUserDetail>::GetDescription()
{
	return _T("Report of SharePoint activity by user.");
}

template<>
wstring O365Report<O365ReportID::SharePointActivityUserDetail>::GetShortUniqueName()
{
	return _YTEXT("spaud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::SharePointSiteUsageDetail>::GetDisplayName()
{
	return _T("SharePoint site usage");
}
template<>
wstring O365Report<O365ReportID::SharePointSiteUsageDetail>::GetDescription()
{
	return _T("Report of SharePoint site usage.");
}

template<>
wstring O365Report<O365ReportID::SharePointSiteUsageDetail>::GetShortUniqueName()
{
	return _YTEXT("spsud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::SkypeForBusinessActivityUserDetail>::GetDisplayName()
{
	return _T("Skype for Business activity");
}
template<>
wstring O365Report<O365ReportID::SkypeForBusinessActivityUserDetail>::GetDescription()
{
	return _T("Report of Skype for Business activity by user.");
}

template<>
wstring O365Report<O365ReportID::SkypeForBusinessActivityUserDetail>::GetShortUniqueName()
{
	return _YTEXT("sfbaud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::SkypeForBusinessDeviceUsageUserDetail>::GetDisplayName()
{
	return _T("Skype for Business device usage");
}
template<>
wstring O365Report<O365ReportID::SkypeForBusinessDeviceUsageUserDetail>::GetDescription()
{
	return _T("Report of Skype for Business device usage by user.");
}

template<>
wstring O365Report<O365ReportID::SkypeForBusinessDeviceUsageUserDetail>::GetShortUniqueName()
{
	return _YTEXT("sfbduud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::YammerActivityUserDetail>::GetDisplayName()
{
	return _T("Yammer activity");
}
template<>
wstring O365Report<O365ReportID::YammerActivityUserDetail>::GetDescription()
{
	return _T("Report of Yammer activity by user.");
}

template<>
wstring O365Report<O365ReportID::YammerActivityUserDetail>::GetShortUniqueName()
{
	return _YTEXT("yaud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::YammerDeviceUsageUserDetail>::GetDisplayName()
{
	return _T("Yammer device usage");
}
template<>
wstring O365Report<O365ReportID::YammerDeviceUsageUserDetail>::GetDescription()
{
	return _T("Report of Yammer device usage by user.");
}

template<>
wstring O365Report<O365ReportID::YammerDeviceUsageUserDetail>::GetShortUniqueName()
{
	return _YTEXT("yduud");
}

// ================================================================================

template<>
wstring O365Report<O365ReportID::YammerGroupsActivityDetail>::GetDisplayName()
{
	return _T("Yammer groups activity");
}
template<>
wstring O365Report<O365ReportID::YammerGroupsActivityDetail>::GetDescription()
{
	return _T("Report of Yammer groups activity by group.");
}

template<>
wstring O365Report<O365ReportID::YammerGroupsActivityDetail>::GetShortUniqueName()
{
	return _YTEXT("ygad");
}
