#include "LinkHandlerO365Reports.h"

#include "Command.h"
#include "CommandDispatcher.h"

void LinkHandlerO365Reports::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);
	
	CommandInfo info;
	info.SetOrigin(Origin::Tenant);
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Reports, Command::ModuleTask::List, info, { nullptr, g_ActionNameShowUsageReport, nullptr, p_Link.GetAutomationAction() }));
}
