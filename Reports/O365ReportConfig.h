#pragma once

#include "BaseO365Grid.h"
#include "CommandInfo.h" // for O365DataMap

// We don't want this for now.
// Makes no sense with the saving of reports.
// Can't work with some Group report that don't have any unique column (as ID or technical name)
#define O365_REPORTS_ALLOW_GRID_REFRESH 0

class GridO365Report;
class GridTemplateGroups;
class GridTemplateUsers;

class O365ReportConfig
{
public:
	O365ReportConfig();
	virtual ~O365ReportConfig();

	struct Column
	{
		enum Type
		{
			BOOL = 0,
			BYTES,
			DATE,
			INTEGER,
			STRING,
			STRINGLIST,
			URL,
			VARIANT, // Only for internal purpose (missing columns added)
		};

		Column() = default;
		Column(const wstring& p_Name, const wstring& p_Title, const wstring& p_UID, Type p_Type, uint32_t p_Width);

		enum Flag : uint32_t
		{
			ADDITIONAL_PROPS_ANCHOR	= 0x1,
			SHARED					= 0x2,
			FOR_PK					= 0x4,
			INSERT_AT_BACK			= 0x8,
			FOR_COMMENT_REF			= 0x10,
			COMPUTED				= 0x20,
		};

		Column(const wstring& p_PropertyName, const wstring& p_Title, const wstring& p_UID, Type p_Type, uint32_t p_Width, uint32_t p_Flags);

		wstring m_Name;
		wstring m_Title;
		wstring m_UID;
		Type m_Type = STRING;
		uint32_t m_Width = 0;
		bool m_AdditionalPropsAnchor = false;
		bool m_Shared = false;
		bool m_ForPk = false;
		bool m_AtBack = false;
		bool m_ForCommentRef = false;
		bool m_Computed = false;

		bool IsValid() const;

		static const Column g_Empty;
	};

	void AddColumn(const Column&& p_Column);
	void AddIgnoredColumns(const vector<wstring>&& p_ColumnNames);
	void AddHiddenByDefaultColumns(const vector<wstring>&& p_ColumnNames);
	void SetColumnForAnonymousDataChecking(const wstring& p_ColumnName);
	void SetColumnsForAdditionalMetadataMatching(vector<std::pair<wstring, wstring>>&& p_ColumnNamesToObjectProperties);
	void SetConditionForAdditionalMetadataMatching(std::function<bool(BusinessObject*)> p_Condition);
	void Configure(GridO365Report& p_Grid, std::unique_ptr<GridTemplateUsers>& p_TemplateUsers, std::unique_ptr<GridTemplateGroups>& p_TemplateGroups, const vector<wstring>& p_AddtionalProperties, int p_NoFieldIconForAdditionalColumns, boost::YOpt<std::map<wstring, std::vector<GridBackendColumn*>>>& p_Categories);
	GridBackendRow* AddRow(GridO365Report& p_Grid, const vector<wstring>& p_Headers, const vector<wstring>& p_Values, bool p_ForRefresh);
	void AdaptColumnsToRealHeaders(GridO365Report& p_Grid, const vector<wstring>& p_Headers);
	void UpdateVariantColumnsTypes(GridO365Report& p_Grid);

	void SetDataOrigin(Origin p_Origin);
	Origin GetDataOrigin() const;

	void SetColumnForGraphId(const wstring& p_ColumnName);
	const wstring& GetColumnUniqueIdForGraphId() const;
	bool NeedsIdFromCache() const;
	GridBackendColumn* GetGraphIdColumn(GridO365Report& p_Grid) const;

	void SetRowType(const rttr::type::type_id& p_TypeID);
	void SetAutomationName(const wstring& p_AutomationName);

	GridBackendColumn* GetColumnForAnonymousDataChecking(GridO365Report& p_Grid) const;
	vector<std::pair<GridBackendColumn*, wstring>> GetColumnsForAdditionalMetadataMatching(GridO365Report& p_Grid) const;
	bool CheckConditionForAdditionalMetadataMatching(BusinessObject* p_Object);

	GridBackendColumn* GetColumnForCommentRef(GridO365Report& p_Grid) const;

	void SetUidPrefix(const wstring& p_Prefix);
	void SetCategoryName(const wstring& p_Category);

protected:
	void addNewRowPostProcess(std::function<void(GridBackendRow*)> p_NewRowPostProcess);
	const Column& getColumn(const wstring& p_ColumnName) const;

private:
	const vector<wstring>& GetPkColumnNames() const;
	bool addField(GridO365Report& p_Grid, GridBackendRow* p_Row, const wstring& p_ColumnName, const wstring& p_Value);
	bool isIgnored(const wstring& p_ColumnName) const;
	bool isHiddenByDefault(const wstring& p_ColumnName) const;
	GridBackendColumn* addColumn(const Column& p_Column, GridO365Report& p_Grid, bool p_AddCategories);

	O365DataMap<wstring, Column> m_Columns;
	vector<wstring> m_PkColumnNames;
	std::set<wstring> m_IgnoredColumnNames;
	std::set<wstring> m_HiddenByDefaultColumnNames;
	boost::YOpt<rttr::type::type_id> m_RowType;
	wstring p_ColumnNameForAnonymousData;
	wstring m_AutomationName;
	vector<std::pair<wstring, wstring>> m_ColumnNamesToObjectProperties;
	std::function<bool(BusinessObject*)> m_Condition;
	Origin m_Origin;
	wstring m_ColumnNameForGraphId;
	wstring m_UidPrefix;
	wstring m_Category;
#ifdef _DEBUG
	static std::set<wstring> g_UidPrefixes;
#endif
	static wstring g_DefaultFamily;
	static wstring g_DefaultFamilyWithCategories;

	std::function<void(GridBackendRow*)> m_NewRowPostProcess;
};

template <O365ReportID reportID>
class O365ReportConfigImpl : public O365ReportConfig
{
public:
	inline O365ReportConfigImpl();

private:
	void commonInit();
};

#include "O365ReportConfig.hpp"