#pragma once

#include "O365ReportID.h"

#include "CommandInfo.h"

struct O365ReportCriteria
{
	O365ReportCriteria() = default;
	O365ReportCriteria(const std::vector<O365ReportID>& p_O365ReportIDs, const wstring& p_PeriodOrDate);
	bool operator==(const O365ReportCriteria& p_Other) const;

	wstring Serialize(size_t p_Index) const;
	bool Deserialize(const wstring& p_Str);

	web::json::value Serialize() const;
	bool Deserialize(const web::json::value& p_Val);

	wstring GetReportName(size_t p_Index) const;
	wstring GetReportDisplayName(size_t p_Index) const;
	wstring GetReportShortName(size_t p_Index) const;
	wstring GetCombinedDisplayName() const;
	wstring GetCombinedDisplayName(const wstring& p_Separator) const;
	wstring GetCombinedShortName() const;
	wstring GetReportDescription(size_t p_Index) const;
	wstring GetPeriodForDisplay(bool p_AddPrefixIfNeeded) const;
	wstring GetCreationDateDisplay() const;

	Origin GetOrigin() const;

	//O365ReportID m_O365ReportID = O365ReportID::Undefined;
	std::vector<O365ReportID> m_O365ReportIDs;
	wstring m_PeriodOrDate;
	boost::YOpt<YTimeDate> m_CreationDate;
	vector<wstring> m_AddtionalProperties;

	static O365ReportID GetO365ReportIDForShortName(const wstring& p_ShortName);

private:
	bool deserialize(const wstring& p_Str, bool p_AddToExisting);
};

#define GET_O365Report_data_REPORT_CASE(_Id, _Member) \
			case _Id: \
				result = O365Report<_Id>::##_Member##; \
				break; \

#define GET_O365Report_data(_Id, _Member) \
	[id = _Id]() \
	{ \
		auto result = O365Report<O365ReportID::OneDriveActivityUserDetail>::##_Member##; \
 \
		switch (id) \
		{ \
			GET_O365Report_data_REPORT_CASE(O365ReportID::OneDriveActivityUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::OneDriveUsageAccountDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::TeamsDeviceUsageUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::TeamsUserActivityUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::EmailActivityUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::EmailAppUsageUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::MailboxUsageDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::Office365GroupsActivityDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::Office365ActiveUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::Office365ActivationsUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::SharePointActivityUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::SharePointSiteUsageDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::SkypeForBusinessActivityUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::SkypeForBusinessDeviceUsageUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::YammerActivityUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::YammerDeviceUsageUserDetail, _Member) \
			GET_O365Report_data_REPORT_CASE(O365ReportID::YammerGroupsActivityDetail, _Member) \
		default: \
			ASSERT(false); \
		} \
 \
		return result; \
	}() \
