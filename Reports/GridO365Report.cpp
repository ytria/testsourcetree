#include "GridO365Report.h"

#include "AutomationWizardO365Reports.h"
#include "csvparser.h"
#include "GridBackendColumnSorter.h"
#include "GridTemplateGroups.h"
#include "GridTemplateUsers.h"
#include "GridUpdater.h"
#include "Sapio365Settings.h"
#include "O365ReportConfig.h"

class GridO365Report::Impl
{
public:
	virtual ~Impl() = default;
	virtual O365ReportConfig& GetConfig() = 0;
};

template <O365ReportID reportID>
class SpecificImpl : public GridO365Report::Impl
{
public:
	SpecificImpl()
		: m_Config(std::make_unique<O365ReportConfigImpl<reportID>>())
	{

	}

	virtual O365ReportConfig& GetConfig()
	{
		ASSERT(m_Config);
		return *m_Config;
	}

private:
	std::unique_ptr<O365ReportConfig> m_Config;
};

GridO365Report::Impl* GridO365Report::createImpl(O365ReportID reportID)
{
	switch (reportID)
	{
	case O365ReportID::Undefined:
		break;
	case O365ReportID::OneDriveActivityUserDetail:
		return new SpecificImpl<O365ReportID::OneDriveActivityUserDetail>();
	case O365ReportID::OneDriveUsageAccountDetail:
		return new SpecificImpl<O365ReportID::OneDriveUsageAccountDetail>();
	case O365ReportID::TeamsDeviceUsageUserDetail:
		return new SpecificImpl<O365ReportID::TeamsDeviceUsageUserDetail>();
	case O365ReportID::TeamsUserActivityUserDetail:
		return new SpecificImpl<O365ReportID::TeamsUserActivityUserDetail>();
	case O365ReportID::EmailActivityUserDetail:
		return new SpecificImpl<O365ReportID::EmailActivityUserDetail>();
	case O365ReportID::EmailAppUsageUserDetail:
		return new SpecificImpl<O365ReportID::EmailAppUsageUserDetail>();
	case O365ReportID::MailboxUsageDetail:
		return new SpecificImpl<O365ReportID::MailboxUsageDetail>();
	case O365ReportID::Office365GroupsActivityDetail:
		return new SpecificImpl<O365ReportID::Office365GroupsActivityDetail>();
	case O365ReportID::Office365ActiveUserDetail:
		return new SpecificImpl<O365ReportID::Office365ActiveUserDetail>();
	case O365ReportID::Office365ActivationsUserDetail:
		return new SpecificImpl<O365ReportID::Office365ActivationsUserDetail>();
	case O365ReportID::SharePointActivityUserDetail:
		return new SpecificImpl<O365ReportID::SharePointActivityUserDetail>();
	case O365ReportID::SharePointSiteUsageDetail:
		return new SpecificImpl<O365ReportID::SharePointSiteUsageDetail>();
	case O365ReportID::SkypeForBusinessActivityUserDetail:
		return new SpecificImpl<O365ReportID::SkypeForBusinessActivityUserDetail>();
	case O365ReportID::SkypeForBusinessDeviceUsageUserDetail:
		return new SpecificImpl<O365ReportID::SkypeForBusinessDeviceUsageUserDetail>();
	case O365ReportID::YammerActivityUserDetail:
		return new SpecificImpl<O365ReportID::YammerActivityUserDetail>();
	case O365ReportID::YammerDeviceUsageUserDetail:
		return new SpecificImpl<O365ReportID::YammerDeviceUsageUserDetail>();
	case O365ReportID::YammerGroupsActivityDetail:
		return new SpecificImpl<O365ReportID::YammerGroupsActivityDetail>();
	default:
		break;
	}

	ASSERT(false);
	return nullptr;
}

GridO365Report::GridO365Report(std::vector<O365ReportID> p_ReportIDs)
	: m_MissingMetaDataIcon(NO_ICON)
	, m_ColForCommentRef(nullptr)
{
	m_Impls.reserve(p_ReportIDs.size());
	for (const auto& reportID : p_ReportIDs)
	{
		m_Impls.push_back(createImpl(reportID));

		if (m_Impls.back()->GetConfig().GetDataOrigin() == Origin::User && !m_TemplateUsers)
		{
			ASSERT(!m_TemplateGroups); // Handle origin mix?
			m_TemplateUsers = std::make_unique<GridTemplateUsers>();
		}
		else if (m_Impls.back()->GetConfig().GetDataOrigin() == Origin::Group && !m_TemplateGroups)
		{
			ASSERT(!m_TemplateUsers); // Handle origin mix?
			m_TemplateGroups = std::make_unique<GridTemplateGroups>();
		}
	}

	initWizard<AutomationWizardO365Reports>(_YTEXT("Automation\\O365Reports"));
}

GridO365Report::~GridO365Report()
{
	for (const auto& impl : m_Impls)
		delete impl;
}

void GridO365Report::customizeGrid()
{
	GetBackend().SetEnableColumnTitleDuplicates(true); // We use categories to differentiate them

	auto frame = dynamic_cast<GridFrameBase*>(GetParentFrame());

	SetAnalyticsTrackingCode(LicenseUtil::g_codeSapio365usageReports);

	// FIXME: Use a specific icon!
	m_MissingMetaDataIcon = GridBackendUtil::ICON_EMPTYFIELD;
	ASSERT(0 != m_MissingMetaDataIcon); // Also used as LPARAM
	boost::YOpt<std::map<wstring, std::vector<GridBackendColumn*>>> categories;
	if (m_Impls.size() > 1)
		categories.emplace();

	m_Impls[0]->GetConfig().Configure(*this, m_TemplateUsers, m_TemplateGroups, nullptr != frame ? frame->GetModuleCriteria().m_ReportCriteria.m_AddtionalProperties : vector<wstring>{}, m_MissingMetaDataIcon, categories);
	m_ColForCommentRef = m_Impls[0]->GetConfig().GetColumnForCommentRef(*this);

	for (size_t i = 1; i < m_Impls.size(); ++i)
	{
		m_Impls[i]->GetConfig().Configure(*this, m_TemplateUsers, m_TemplateGroups, {}, m_MissingMetaDataIcon, categories);
		ASSERT(m_ColForCommentRef == m_Impls[i]->GetConfig().GetColumnForCommentRef(*this));
	}

	if (categories)
	{
		addSystemTopCategory();
		for (auto& cat : *categories)
		{
			std::sort(cat.second.begin(), cat.second.end(), GridBackendColumnSorter<BYPOSITIONVIRTUAL>());
			AddCategoryTop(cat.first, cat.second);
		}
	}

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(frame);
}

bool GridO365Report::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return nullptr != p_Row && m_MissingMetaDataIcon == p_Row->GetLParam()
		&& nullptr != p_Col && m_MissingMetaDataIcon == p_Col->GetLParam();
}

wstring GridO365Report::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, m_ColForCommentRef);
}

void GridO365Report::LoadReports(const std::vector<Report>& p_Reports)
{
	bool hasAnonymousData = false;
	wstring errorsString;

	const bool refresh = !GetBackendRows().empty();
#if O365_REPORTS_ALLOW_GRID_REFRESH
#else
	ASSERT(!refresh);
#endif

	{
		std::unique_ptr<GridUpdater> updater;

		for (size_t i = 0; i < p_Reports.size(); ++i)
		{
			const auto& report = p_Reports[i];
			CsvParser parser(report.m_CsvFilePath, _YTEXT(','));

			if (!handleError(parser))
			{
				if (!updater)
				{
					updater = std::make_unique<GridUpdater>(*this,
						GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID
#if O365_REPORTS_ALLOW_GRID_REFRESH
							| (refresh ? GridUpdaterOptions::FULLPURGE : 0)
#endif
							| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
							| GridUpdaterOptions::REFRESH_PROCESS_DATA));
				}

				//auto encoding = parser.GetEncoding(); // Useful?

				auto headers = parser.GetCurrentRow().GetFields();
				// Skip metadata line
				if (!headers.empty() && Str::beginsWith(headers.front(), _YTEXT("##")))
					headers = parser.GetCurrentRow().GetFields();

				// Handle column addition/removal
				Impl* impl = nullptr;
				ASSERT(i < m_Impls.size());
				if (i < m_Impls.size())
					impl = m_Impls[i];
				ASSERT(nullptr != impl);
				if (nullptr != impl)
				{
					impl->GetConfig().AdaptColumnsToRealHeaders(*this, headers);

					while (!handleError(parser) && !parser.IsEOF())
					{
						const auto& fields = parser.GetCurrentRow().GetFields();
						ASSERT(fields.size() <= headers.size());

						if (fields.size() == headers.size())
						{
							auto row = impl->GetConfig().AddRow(*this, headers, fields, refresh);
							updater->GetOptions().AddRowWithRefreshedValues(row);
						}
					}

					// Check anonymous data
					GridBackendColumn* col = impl->GetConfig().GetColumnForAnonymousDataChecking(*this);
					if (nullptr != col)
					{
						int count = 0;
						for (auto row : GetBackendRows())
						{
							auto& field = row->GetField(col);
							if (field.HasValue() && looksLikeAnonymousData(field.GetValueStr())
								&& ++count > 1)
								break;
						}

						if (count > 1)
							hasAnonymousData = true;
					}
				}
			}

			if (!p_Reports[i].m_TargetPathError.empty())
			{
				if (!Str::endsWith(errorsString, _YTEXT("\n")))
					errorsString += _YTEXT("\n");
				errorsString += _YFORMAT(L"%s. %s", Str::getStringFromNumber(i).c_str(), p_Reports[i].m_TargetPathError.c_str());
			}
		}

		// additional data
		auto frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
		ASSERT(nullptr != frame);
		if (nullptr != frame)
		{
			auto& reportCriteria = frame->GetModuleCriteria().m_ReportCriteria;

			bool needsIdFromCache = true;
			for (auto& impl : m_Impls)
				needsIdFromCache = needsIdFromCache && impl->GetConfig().NeedsIdFromCache();

			auto origin = m_Impls[0]->GetConfig().GetDataOrigin();
			ASSERT(std::all_of(m_Impls.begin(), m_Impls.end(), [origin](auto& impl)
				{
					return origin == impl->GetConfig().GetDataOrigin();
				}));

			if ((Origin::User == origin || Origin::Group == origin)
				&& (!reportCriteria.m_AddtionalProperties.empty() || needsIdFromCache))
			{
				boost::YOpt<vector<BusinessUser>> cacheUsers;
				boost::YOpt<vector<BusinessGroup>> cacheGroups;

				if (Origin::User == origin)
				{
					cacheUsers = GetGraphCache().GetSqlCachedUsers(frame->m_hWnd, {}, false);
				}
				else if (Origin::Group == origin)
				{
					cacheGroups = GetGraphCache().GetSqlCachedGroups(frame->m_hWnd, {}, false);
				}

				for (auto& impl : m_Impls)
				{
					auto forMetadata = impl->GetConfig().GetColumnsForAdditionalMetadataMatching(*this);
					ASSERT(!forMetadata.empty());
					if (!forMetadata.empty())
					{
						std::map<vector<wstring>, GridBackendRow*> rows;
						vector<wstring> temp;
						temp.reserve(forMetadata.size());
						for (auto row : GetBackendRows())
						{
							temp.clear();
							for (const auto& col : forMetadata)
							{
								auto& field = row->GetField(col.first);

								// Might happen: e.g. in Office365GroupsActivityDetail, a deleted group has not always a name :-o
								ASSERT(field.HasValue() && field.IsString());
								if (field.HasValue() && field.IsString())
									temp.push_back(field.GetValueStr());
								else
									temp.emplace_back();
							}

							ASSERT(rows[temp] == nullptr);
							rows[temp] = row;
						}

						const auto origin = impl->GetConfig().GetDataOrigin();
						if (Origin::User == origin)
						{
							ASSERT(m_TemplateUsers && cacheUsers);
							if (m_TemplateUsers && cacheUsers)
							{
								std::map<std::vector<wstring>, std::set<GridBackendRow*>> conflicts;

								GridIgnoreModification ignoramus(*this);
								GridUpdater updater(*this, GridUpdaterOptions::UPDATE_GRID);
								GridTemplateUsers::DirectRowUpdater rowUpdater(*m_TemplateUsers);
								for (auto& user : *cacheUsers)
								{
									if (impl->GetConfig().CheckConditionForAdditionalMetadataMatching(&user))
									{
										temp.clear();
										for (const auto& col : forMetadata)
											temp.push_back(user.GetValue(col.second));

										auto it = rows.find(temp);
										if (rows.end() != it)
										{
											ASSERT(it->second->GetLParam() != m_MissingMetaDataIcon);
											if (it->second->GetLParam() == 0)
												rowUpdater(*this, user, it->second, GridTemplate::UpdateFieldOption::NONE);
											else
												conflicts[temp].insert(it->second);
											it->second->SetLParam(0xffffffff); // Mark as done.
											rows.erase(it);
										}
									}
								}

								if (!rows.empty())
								{
									for (auto row : rows)
									{
										ASSERT(row.second->GetLParam() == 0 || row.second->GetLParam() == m_MissingMetaDataIcon);
										row.second->SetLParam(m_MissingMetaDataIcon);
									}
								}

								if (!conflicts.empty())
								{
									// FIXME: Better handle this!

									for (auto& item : conflicts)
									{
										auto key = Str::implode(item.first, _YTEXT(","));
										LoggerService::Debug(_YDUMPFORMAT(L"GridO365Report::LoadReport Conflict: %s users matching key %s", Str::getStringFromNumber(item.second.size()).c_str(), key.c_str()));
									}
								}
							}
						}
						else if (Origin::Group == origin)
						{
							ASSERT(m_TemplateGroups && cacheGroups);
							if (m_TemplateGroups && cacheGroups)
							{
								std::map<std::vector<wstring>, std::set<GridBackendRow*>> conflicts;

								GridIgnoreModification ignoramus(*this);
								GridUpdater updater(*this, GridUpdaterOptions::UPDATE_GRID);
								for (auto& group : *cacheGroups)
								{
									if (impl->GetConfig().CheckConditionForAdditionalMetadataMatching(&group))
									{
										temp.clear();
										for (const auto& col : forMetadata)
											temp.push_back(group.GetValue(col.second));

										auto it = rows.find(temp);
										if (rows.end() != it)
										{
											ASSERT(it->second->GetLParam() != m_MissingMetaDataIcon);
											if (it->second->GetLParam() == 0)
												m_TemplateGroups->updateRow(*this, group, it->second, GetGraphCache(), false);
											else
												conflicts[temp].insert(it->second);
											it->second->SetLParam(0xffffffff); // Mark as done.
											rows.erase(it);
										}
									}
								}

								if (!rows.empty())
								{
									for (auto row : rows)
									{
										ASSERT(row.second->GetLParam() == 0 || row.second->GetLParam() == m_MissingMetaDataIcon);
										row.second->SetLParam(m_MissingMetaDataIcon);
									}
								}

								if (!conflicts.empty())
								{
									// FIXME: Better handle this!

									for (auto& item : conflicts)
									{
										auto key = Str::implode(item.first, _YTEXT(","));
										LoggerService::Debug(_YDUMPFORMAT(L"GridO365Report::LoadReport Conflict: %s groups matching key %s", Str::getStringFromNumber(item.second.size()).c_str(), key.c_str()));
									}
								}
							}
						}
						else
						{
							ASSERT(false);
						}
					}
				}
			}
		}
	}

	if (hasAnonymousData)
	{
		Util::ShowRestrictedAccessDialog(
			DlgRestrictedAccess::EXCLAMATION_MARK
			, { _T("Report contains anonymous identifiers")
				,	_T("Names are hidden due to a setting in Office 365. This can be changed in 'Microsoft 365 admin center' > 'Settings' > 'Services & add-ins' > 'Reports'.")
				,	_YTEXT("")
				,	_T("Go to admin center...")
				,	_YTEXT("https://admin.microsoft.com/Adminportal/Home?source=applauncher#/Settings/ServicesAndAddIns") }
			, NoSetting<bool>() // HideReportAnonymousDataWarningDialogSetting() // FIXME: show it back as soon as we can reset it (i.e. in the Settings panel)
			, this
			, true);
	}

	if (!errorsString.empty())
	{
		YCodeJockMessageBox errorMessage(this,
			DlgMessageBox::eIcon_ExclamationWarning,
			_YTEXT("sapio365"),
			_T("Report file could not be saved."),
			errorsString,
			{ { IDOK, _T("OK") } });
		errorMessage.DoModal();
	}
}

void GridO365Report::ShowLoadingError(const SapioError& p_Error)
{
	ASSERT(GetBackendRows().empty());
	if (GetBackendRows().empty())
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS); // Make column headers visible
	HandlePostUpdateError(p_Error.GetStatusCode() == 403, false, p_Error.GetFullErrorMessage());
}

void GridO365Report::GetGACustomText(CString& p_CustomText)
{
	// base class impl returns a text about cache if refresh date is older than creation date.
	// For reports, as the refresh date is the date returned by the request, it might happen that it's slightly older
	// than the window's creation date making the text be shown.
	// To prevent this from happening, just force no text here.
	return;
}

bool GridO365Report::handleError(const CsvParser& p_Parser)
{
	if (!p_Parser.GetError().empty())
	{
		//RemoveAllRows();
		LoggerService::Debug(_YDUMPFORMAT(L"GridO365Report::LoadReport %s", p_Parser.GetError().c_str()));
		HandlePostUpdateError(false, false, p_Parser.GetError());
		return true;
	}

	return false;
}

const bool GridO365Report::looksLikeAnonymousData(const PooledString& p_Str)
{
	const wstring str(p_Str);
	return str.size() == 32 && std::all_of(str.cbegin(), str.cend(), [](auto& c) {return std::isxdigit(c); });
}

GridBackendColumn* GridO365Report::MapColumnGraphID(GridBackendColumn* p_SpecificGraphIdColumn, const wstring& p_Family)
{
	if (nullptr == m_ColId)
	{
		if (nullptr == p_SpecificGraphIdColumn)
		{
			m_ColId = GetColumnByUniqueID(_YUID(O365_ID));
			if (nullptr == m_ColId)
				addColumnGraphID(p_Family);
		}
		else 
			m_ColId = p_SpecificGraphIdColumn;
	}

	ASSERT(nullptr != m_ColId);
	return m_ColId;
}

O365Grid::SnapshotCaps GridO365Report::GetSnapshotCapabilities() const
{
	// This module has no refresh/edit capabilities, Restore Point is useless.
	return { true, false };
}

bool GridO365Report::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	return m_TemplateUsers && m_TemplateUsers->GetSnapshotValue(p_Value, p_Field, p_Column)
		|| m_TemplateGroups && m_TemplateGroups->GetSnapshotValue(p_Value, p_Field, p_Column)
		|| O365Grid::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridO365Report::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	return m_TemplateUsers && m_TemplateUsers->LoadSnapshotValue(p_Value, p_Row, p_Column)
		|| m_TemplateGroups && m_TemplateGroups->LoadSnapshotValue(p_Value, p_Row, p_Column)
		|| O365Grid::LoadSnapshotValue(p_Value, p_Row, p_Column);

}

