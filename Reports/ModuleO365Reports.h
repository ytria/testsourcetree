#pragma once

#include "GridO365Report.h"
#include "ModuleBase.h"
#include "O365ReportCriteria.h"
#include "Sapio365Session.h"

class ModuleO365Reports : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

	// wstring is target file name
	using LoadingData = std::pair<O365ReportCriteria, std::vector<wstring>>;

	void loadReport(Command p_Command);
	void loadSnapshot(const Command& p_Command);

	static wstring GetReportsFolder(std::shared_ptr<Sapio365Session> p_Session);
	static const wstring g_ReportFileExtension;

private:
	virtual void executeImpl(const Command& p_Command) override;

	void loadReportImpl(const std::vector<O365ReportID>& reportIDs, const Command& p_Command, boost::YOpt<LoadingData> p_ExistingConfig);
};
