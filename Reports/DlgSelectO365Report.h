#pragma once

#include "DlgFormsHTML.h"
#include "O365ReportCriteria.h"
#include "O365ReportID.h"
#include "Sapio365Session.h"

class DlgSelectO365Report : public DlgFormsHTML
{
public:
	DlgSelectO365Report(std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent);

	bool IsCreateNew() const;
	const O365ReportCriteria& GetReportConfig() const;

	bool IsSaveReport() const;

	// If new report, returns target filename (vector size is 1), otherwise existing report filepaths (for multi reports)
	const std::vector<wstring>& GetReportFiles() const;

	static void TranslateAction(AutomatedApp* p_App, AutomationAction* p_Action, list<AutomationAction*>& p_ActionList);

protected:
	virtual void generateJSONScriptData() override;

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

	virtual wstring getDialogTitle() const override;

	static bool isNoPeriodReport(const wstring& p_NameOrID);
	static bool isPeriodOnlyReport(const wstring& p_NameOrID);
	static bool isUserReport(const wstring& p_NameOrID);
	static bool isGroupReport(const wstring& p_NameOrID);
	static bool isSiteReport(const wstring& p_NameOrID);
	template<O365ReportID reportID> static bool isReport(const wstring& p_NameOrID);

	virtual void GetMultiValueFields(vector < wstring >& o_MultiValueFieldNames) const;

	virtual bool	setParamSpecific(AutomationAction* i_Action) override;

private:
	shared_ptr<Sapio365Session> m_Session;
	O365ReportCriteria m_ReportConfig;
	std::vector<wstring> m_FileNames;
	bool m_LoadExisting;
	bool m_ShouldSaveNewReport;
	std::map<wstring, vector<wstring>> m_ExistingReportSeries;

private:
	AutomationAction* m_CurrentMotherAction; // Only for setParamSpecific
	size_t m_CurrentSetParamTotalCount;
};