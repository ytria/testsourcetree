#include "ModuleO365Reports.h"

#include "Command.h"
#include "CStdioFileEx.h"
#include "DlgSelectO365Report.h"
#include "FileUtil.h"
#include "FrameO365Reports.h"
#include "O365ReportRequester.h"

const wstring ModuleO365Reports::g_ReportFileExtension = _YTEXT("ysv");

void ModuleO365Reports::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
	case Command::ModuleTask::List:
		loadReport(p_Command);
		break;
	case Command::ModuleTask::LoadSnapshot:
		loadSnapshot(p_Command);
		break;
	default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
		break;
	}
}

wstring ModuleO365Reports::GetReportsFolder(std::shared_ptr<Sapio365Session> p_Session)
{
	static wstring folderPathBase;
	
	if (folderPathBase.empty())
		folderPathBase = MFCUtil::GetExpandedString(wstring(_YTEXT("%LOCALAPPDATA%\\Ytria\\sapio365\\Reports")));

	ASSERT(p_Session);
	if (p_Session)
		return folderPathBase + _YTEXT("\\") + p_Session->GetTenantName(true);
	return folderPathBase;
}

void ModuleO365Reports::loadReport(Command p_Command)
{
	auto& info = p_Command.GetCommandInfo();

	ASSERT(!info.Data());

	// Show dialog
	DlgSelectO365Report dlg(ModuleBase::GetConnectedSapio365Session(), AfxGetApp()->m_pMainWnd);
	if (IDOK == dlg.DoModal())
	{
		HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);
		if (dlg.IsCreateNew())
		{
			wstring targetFilename;
			if (dlg.IsSaveReport())
				targetFilename = dlg.GetReportFiles()[0];
			info.Data() = ModuleO365Reports::LoadingData{ dlg.GetReportConfig(), { targetFilename } };
		}
		else
		{
			info.Data() = ModuleO365Reports::LoadingData{ {}, dlg.GetReportFiles() };
		}
	}
	else
	{
		auto action = p_Command.GetAutomationAction();
		if (nullptr != action)
			SetActionCanceledByUser(action);
		return;
	}

	ASSERT(info.Data().is_type<LoadingData>());
	if (info.Data().is_type<LoadingData>())
	{
		const auto& reportConfig = info.Data().get_value<LoadingData>().first;

		auto reportIDs = reportConfig.m_O365ReportIDs;
		wstring existingFilePath;
		boost::YOpt<LoadingData> existingConfig;
		boost::YOpt<wstring> existingConfigLoadingError;

		if (reportIDs.empty() || reportIDs[0] == O365ReportID::Undefined)
		{
			auto session = nullptr != info.GetFrame() ? info.GetFrame()->GetSapio365Session() : GetConnectedSapio365Session();
			auto& filepaths = info.Data().get_value<LoadingData>().second;
			ASSERT(!filepaths.empty());

			wstring existingFilePathWithError;

			for (const auto& filePath : filepaths)
			{
				ASSERT(!Str::endsWith(filePath, _YTEXT(".") + g_ReportFileExtension));
				ASSERT(!Str::beginsWith(filePath, GetReportsFolder(session)));

				auto existingFilePath = GetReportsFolder(session) + _YTEXT("\\") + filePath + _YTEXT(".") + g_ReportFileExtension;
				{
					CStdioFileEx fileEx;
					CFileException e;
					if (TRUE == fileEx.Open(existingFilePath.c_str(), CFile::modeRead, &e))
					{
						CString line;
						if (TRUE == fileEx.ReadString(line))
						{
							O365ReportCriteria conf;
							if (conf.Deserialize((LPCTSTR)line))
							{
								ASSERT(conf.m_O365ReportIDs.size() == 1);
								if (!existingConfig)
								{
									reportIDs = conf.m_O365ReportIDs;
									existingConfig.emplace();
									existingConfig->first = conf;
								}
								else
								{
									existingConfig->first.m_O365ReportIDs.push_back(conf.m_O365ReportIDs[0]);
									reportIDs.push_back(conf.m_O365ReportIDs[0]);
								}
								existingConfig->second.push_back(existingFilePath);
							}
							else
							{
								existingConfigLoadingError = YtriaTranslate::DoError(ModuleO365Reports_loadReport_2, _YLOC("Invalid metadata."), _YR("Y2435")).c_str();
							}
						}
						else
						{
							existingConfigLoadingError = YtriaTranslate::DoError(ModuleO365Reports_loadReport_3, _YLOC("Unable to read metadata."), _YR("Y2436")).c_str();
						}
					}
					else
					{
						TCHAR   szCause[255];
						CString strFormatted;
						e.GetErrorMessage(szCause, 255);
						existingConfigLoadingError = szCause;
					}
				}

				if (existingConfigLoadingError)
				{
					existingFilePathWithError = existingFilePath;
					break;
				}
			}
		}

		if (reportIDs.empty() || reportIDs[0] == O365ReportID::Undefined)
		{
			const wstring errorMessage = !existingFilePath.empty() ? YtriaTranslate::Do(ModuleO365Reports_loadReport_5, _YLOC("Existing Report file %1 could not be loaded."), existingFilePath.c_str())
				: YtriaTranslate::Do(ModuleO365Reports_loadReport_4, _YLOC("Existing Report file could not be loaded.")).c_str();
			const wstring errorDetail = existingConfigLoadingError ? *existingConfigLoadingError : _YTEXT("");
			YCodeJockMessageBox errorDlg(info.GetFrame(),
				DlgMessageBox::eIcon_Error,
				_YTEXT("sapio365"),
				errorMessage,
				errorDetail,
				{ { IDOK, YtriaTranslate::Do(ModuleO365Reports_loadReport_1, _YLOC("OK")).c_str() } });
			errorDlg.DoModal();
		}
		else
		{
			loadReportImpl(reportIDs, p_Command, existingConfig);
		}
	}
}

void ModuleO365Reports::loadSnapshot(const Command& p_Command)
{
	LoadSnapshot<FrameO365Reports>(p_Command, [&p_Command](const GridSnapshot::Metadata& p_SnapshotMeta, const ModuleCriteria& p_ModuleCriteria, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
		{
			const auto title = p_ModuleCriteria.m_ReportCriteria.m_O365ReportIDs.size() > 1 ? _T("Multiple Usage Reports") : p_ModuleCriteria.m_ReportCriteria.GetCombinedDisplayName();
			return new FrameO365Reports(p_ModuleCriteria.m_ReportCriteria.m_O365ReportIDs, p_LicenseContext, p_SessionIdentifier, title, p_Module, p_HistoryMode, p_PreviousFrame);
		});
}

void ModuleO365Reports::loadReportImpl(const std::vector<O365ReportID>& reportIDs, const Command& p_Command, boost::YOpt<LoadingData> p_ExistingConfig)
{
	ASSERT(!reportIDs.empty());

	const auto& info = p_Command.GetCommandInfo();
	auto p_SourceWindow = p_Command.GetSourceWindow();
	auto p_Action = p_Command.GetAutomationAction();

	auto* frame = dynamic_cast<FrameO365Reports*>(info.GetFrame());

	O365ReportCriteria reportConfig;
	wstring targetReportFileName;

	ASSERT(info.Data().is_type<LoadingData>());
	if (info.Data().is_type<LoadingData>())
	{
		reportConfig = info.Data().get_value<LoadingData>().first;
		if (info.Data().get_value<LoadingData>().second.size() == 1)
			targetReportFileName = info.Data().get_value<LoadingData>().second[0];
	}

	const bool isRefresh = nullptr != frame;
	ASSERT(!isRefresh); // Not handled.
	if (!isRefresh)
	{
		if (!FrameO365Reports::CanCreateNewFrame(true, p_SourceWindow.GetCWnd(), p_Action))
			return;

		ModuleCriteria moduleCriteria;
		moduleCriteria.m_Origin = Origin::Tenant;
		moduleCriteria.m_UsedContainer = ModuleCriteria::UsedContainer::O365REPORTCRITERIA;
		moduleCriteria.m_ReportCriteria = p_ExistingConfig ? p_ExistingConfig->first : reportConfig;
		moduleCriteria.m_Privilege = info.GetRBACPrivilege();

		if (ShouldCreateFrame<FrameO365Reports>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
		{
			CWaitCursor _;
			const auto title = moduleCriteria.m_ReportCriteria.m_O365ReportIDs.size() > 1 ? _T("Multiple Usage Reports") : moduleCriteria.m_ReportCriteria.GetCombinedDisplayName();
			frame = new FrameO365Reports(reportIDs, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), title, *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
			frame->InitModuleCriteria(moduleCriteria);
			AddGridFrame(frame);
			frame->Erect();
		}
	}

	if (nullptr != frame)
	{
		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleO365Reports_loadReportImpl_1, _YLOC("Load Report")).c_str(), frame, p_Command, !isRefresh);

		if (p_ExistingConfig)
		{
			std::vector<GridO365Report::Report> reports;
			reports.reserve(p_ExistingConfig->second.size());
			for (auto& path : p_ExistingConfig->second)
				reports.push_back({ path, _YTEXT("") });
			frame->LoadReport(reports);
			frame->GetGrid().ClearLog(taskData.GetId());
			frame->UpdateContext(taskData, frame->GetSafeHwnd());
		}
		else
		{
			YSafeCreateTask([reportIDs, reportConfig, targetReportFileName, taskData, session = frame->GetSapio365Session(), p_Action, hwnd = frame->GetSafeHwnd(), isRefresh]()
			{
				std::vector<GridO365Report::Report> reports;
				boost::YOpt<SapioError> error;
				std::vector<boost::YOpt<YTimeDate>> reportDates;
				for (size_t i = 0; i < reportIDs.size(); ++i)
				{
					const auto& reportID = reportIDs[i];

					LoggerService::User(_YFORMAT(L"Loading usage report: %s", GET_O365Report_data(reportID, GetDisplayName()).c_str()), hwnd);

					O365ReportRequester requester(GET_O365Report_data(reportID, GetAPICall()), reportConfig, i);

					safeTaskCall(requester.Send(session, taskData), session->GetMSGraphSession(Sapio365Session::USER_SESSION), [&error](const std::exception_ptr& p_ExceptPtr)
					{
						ASSERT(p_ExceptPtr);
						if (p_ExceptPtr)
						{
							try
							{
								std::rethrow_exception(p_ExceptPtr);
							}
							catch (const RestException & e)
							{
								error = HttpError(e);
							}
							catch (const std::exception & e)
							{
								error = SapioError(e);
							}
							catch (...)
							{
								ASSERT(false);
								error = SapioError(_YTEXT("Unknown Error"), 0);
							}
						}
						else
						{
							ASSERT(false);
							error = SapioError(_YTEXT("Unknown Error"), 0);
						}
					}, taskData).GetTask().wait();

					if (error)
						break;

					auto filePath = requester.GetReportFilePath();
					//auto originalFileName = requester.GetReportOriginalFileName();

					// Do not assert, dates are slightly different (max a few seconds between to report requests)
					//ASSERT(reportDates.empty() || *reportDates[0] == requester.GetReportDate());
					reportDates.push_back(requester.GetReportDate());

					wstring targetFileError;
					if (!targetReportFileName.empty())
					{
						auto fileName = targetReportFileName;
						if (Str::endsWith(fileName, _YTEXT(".") + g_ReportFileExtension))
						{
							const auto dotPos = fileName.rfind(_YTEXT("."));
							fileName = fileName.substr(0, dotPos);
						}

						if (reportIDs.size() > 1)
							fileName += _YTEXT(".") + std::to_wstring(i);

						auto targetFilePath = GetReportsFolder(session) + _YTEXT("\\") + fileName;
						ASSERT(!Str::endsWith(targetFilePath, _YTEXT(".") + g_ReportFileExtension));
						if (!Str::endsWith(targetFilePath, _YTEXT(".") + g_ReportFileExtension))
							targetFilePath += _YTEXT(".") + g_ReportFileExtension;

						FileUtil::MakeFileNameValid(targetFilePath);
						targetFilePath = FileUtil::MakeUniqueFilepath(targetFilePath);
						FileUtil::CreateFolderHierarchy(targetFilePath);
						if (TRUE == MoveFile(filePath.c_str(), targetFilePath.c_str()))
						{
							filePath = targetFilePath;
						}
						else
						{
							DWORD err = ::GetLastError();
							if (err != 0)
							{
								TCHAR buff[256] = _YTEXT("\0");
								::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, // It's a system error
									nullptr,
									err,
									MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Do it in the standard language
									buff,
									sizeof(buff) / sizeof(TCHAR) - 1,
									nullptr);

								targetFileError = buff;
							}
						}
					}

					reports.push_back({ filePath, targetFileError });
				}

				YDataCallbackMessage<std::vector<GridO365Report::Report>>::DoPost(reports, [error, taskData, p_Action, hwnd, isRefresh, isCanceled = taskData.IsCanceled(), firstReportDate = reportDates.empty() ? boost::YOpt<YTimeDate>() : reportDates[0]](const std::vector<GridO365Report::Report>& p_Reports)
				{
					bool shouldFinishTask = true;
					auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameO365Reports*>(CWnd::FromHandle(hwnd)) : nullptr;
					if (ShouldBuildView(hwnd, isCanceled, taskData, false))
					{
						ASSERT(nullptr != frame);
						
						frame->GetGrid().SetProcessText(_T("Inserting usage report data..."));
						frame->GetGrid().ClearLog(taskData.GetId());
						frame->UpdateReportDate(firstReportDate);

						if (error)
						{
							frame->ShowLoadingError(*error);
						}
						else
						{
							CWaitCursor _;
							frame->LoadReport(p_Reports);
						}

						if (!isRefresh)
						{
							frame->UpdateContext(taskData, hwnd);
							shouldFinishTask = false;
						}
					}

					if (shouldFinishTask)
					{
						// Because we didn't (and can't) call UpdateContext here.
						ModuleBase::TaskFinished(frame, taskData, p_Action);
					}
				});
			});
		}
	}
}
