#include "O365ReportCriteria.h"

#include "GridDataFormat.h"
#include "Str.h"
#include "TimeUtil.h"

namespace
{
	O365ReportID GetReportID(const wstring& p_Str)
	{
#define REPORT_CASE_ID(_Id) \
		if (p_Str == O365Report<_Id>::g_AsString) \
			return _Id; \

		REPORT_CASE_ID(O365ReportID::OneDriveActivityUserDetail)
		REPORT_CASE_ID(O365ReportID::OneDriveUsageAccountDetail)
		REPORT_CASE_ID(O365ReportID::TeamsDeviceUsageUserDetail)
		REPORT_CASE_ID(O365ReportID::TeamsUserActivityUserDetail)
		REPORT_CASE_ID(O365ReportID::EmailActivityUserDetail)
		REPORT_CASE_ID(O365ReportID::EmailAppUsageUserDetail)
		REPORT_CASE_ID(O365ReportID::MailboxUsageDetail)
		REPORT_CASE_ID(O365ReportID::Office365GroupsActivityDetail)
		REPORT_CASE_ID(O365ReportID::Office365ActiveUserDetail)
		REPORT_CASE_ID(O365ReportID::Office365ActivationsUserDetail)
		REPORT_CASE_ID(O365ReportID::SharePointActivityUserDetail)
		REPORT_CASE_ID(O365ReportID::SharePointSiteUsageDetail)
		REPORT_CASE_ID(O365ReportID::SkypeForBusinessActivityUserDetail)
		REPORT_CASE_ID(O365ReportID::SkypeForBusinessDeviceUsageUserDetail)
		REPORT_CASE_ID(O365ReportID::YammerActivityUserDetail)
		REPORT_CASE_ID(O365ReportID::YammerDeviceUsageUserDetail)
		REPORT_CASE_ID(O365ReportID::YammerGroupsActivityDetail)

		ASSERT(false);
		return O365ReportID::Undefined;
	}

	wstring dateToString(const YTimeDate& p_Date)
	{
		wstring str;
		CString dateStr;
		DateTimeFormat format;
		format.m_TimeFormat.m_DisplayFlags = TimeFormat::HOURS | TimeFormat::MINUTES | TimeFormat::SECONDS;
		TimeUtil::GetInstance().ConvertDateToText(format, p_Date, dateStr);
		str = (LPCTSTR)dateStr;
		return str;
	}
}

O365ReportCriteria::O365ReportCriteria(const std::vector<O365ReportID>& p_O365ReportIDs, const wstring& p_PeriodOrDate)
	: m_O365ReportIDs(p_O365ReportIDs)
	, m_PeriodOrDate(p_PeriodOrDate)
{

}

bool O365ReportCriteria::operator==(const O365ReportCriteria& p_Other) const
{
	return m_O365ReportIDs == p_Other.m_O365ReportIDs
		&& m_PeriodOrDate == p_Other.m_PeriodOrDate
		&& m_AddtionalProperties == p_Other.m_AddtionalProperties;
}

wstring O365ReportCriteria::Serialize(size_t p_Index) const
{
	// Only serialize unitary reports. 
	ASSERT(p_Index < m_O365ReportIDs.size());

	const auto dateString = m_CreationDate ? TimeUtil::GetInstance().GetISO8601String(*m_CreationDate) : _YTEXT("");
	if (m_AddtionalProperties.empty())
		return _YTEXTFORMAT(L"##%s|%s|%s##", GetReportName(p_Index).c_str(), m_PeriodOrDate.c_str(), dateString.c_str());
	return _YTEXTFORMAT(L"##%s|%s|%s|%s##", GetReportName(p_Index).c_str(), m_PeriodOrDate.c_str(), dateString.c_str(), Str::implode(m_AddtionalProperties, _YTEXT(";"), false).c_str());
}


bool O365ReportCriteria::deserialize(const wstring& p_Str, bool p_AddToExisting)
{
	bool success = false;
	if (Str::beginsWith(p_Str, _YTEXT("##"))
		&& Str::endsWith(p_Str, _YTEXT("##")))
	{
		const auto str = p_Str.substr(2, p_Str.size() - 4);
		const auto data = Str::explodeIntoTokenVector(str, _YTEXT("|"), true);
		ASSERT(data.size() == 3 || data.size() == 4);
		if (data.size() >= 2)
		{
			if (p_AddToExisting)
				m_O365ReportIDs.push_back(GetReportID(data[0]));
			else
				m_O365ReportIDs = { GetReportID(data[0]) };
			if (O365ReportID::Undefined != m_O365ReportIDs.back())
			{
				ASSERT(!p_AddToExisting || m_PeriodOrDate.empty() || m_PeriodOrDate == data[1]);
				m_PeriodOrDate = data[1];
			}

			if (data.size() > 2)
			{
				YTimeDate td;
				TimeUtil::GetInstance().GetTimedateFromISO8601String(data[2], td);

				ASSERT(!p_AddToExisting || !m_CreationDate || *m_CreationDate == td);
				m_CreationDate = td;

				if (data.size() > 3)
				{
					auto v = Str::explodeIntoVector(data[3], wstring(_YTEXT(";")));
					ASSERT(!p_AddToExisting || m_AddtionalProperties.empty() || m_AddtionalProperties == v);
					m_AddtionalProperties = std::move(v);
				}

				success = true;
			}
		}
	}

	return success;
}

bool O365ReportCriteria::Deserialize(const wstring& p_Str)
{
	// Deserialize unitary reports.
	return deserialize(p_Str, false);
}

web::json::value O365ReportCriteria::Serialize() const
{
	auto arr = web::json::value::array();

	for (size_t i = 0; i < m_O365ReportIDs.size(); ++i)
		arr[i] = web::json::value(Serialize(i));

	return arr;
}

bool O365ReportCriteria::Deserialize(const web::json::value& p_Val)
{
	if (p_Val.is_array())
	{
		const auto& arr = p_Val.as_array();

		*this = O365ReportCriteria(); // reset

		for (auto& c : arr)
		{
			ASSERT(c.is_string());
			if (c.is_string())
				deserialize(c.as_string(), true);
		}
	}

	return false;
}

wstring O365ReportCriteria::GetReportName(size_t p_Index) const
{
	ASSERT(p_Index < m_O365ReportIDs.size());
	return GET_O365Report_data(m_O365ReportIDs[p_Index], g_AsString);
}

wstring O365ReportCriteria::GetReportDisplayName(size_t p_Index) const
{
	ASSERT(p_Index < m_O365ReportIDs.size());
	return GET_O365Report_data(m_O365ReportIDs[p_Index], GetDisplayName());
}

wstring O365ReportCriteria::GetReportShortName(size_t p_Index) const
{
	ASSERT(p_Index < m_O365ReportIDs.size());
	return GET_O365Report_data(m_O365ReportIDs[p_Index], GetShortUniqueName());
}

wstring O365ReportCriteria::GetCombinedDisplayName(const wstring& p_Separator) const
{
	ASSERT(!m_O365ReportIDs.empty());

	wstring tit;
	for (const auto& repId : m_O365ReportIDs)
		tit += p_Separator + GET_O365Report_data(repId, GetDisplayName());

	return tit.substr(p_Separator.size());
}

wstring O365ReportCriteria::GetCombinedDisplayName() const
{
	return GetCombinedDisplayName(_YTEXT(" / "));
}

wstring O365ReportCriteria::GetCombinedShortName() const
{
	ASSERT(!m_O365ReportIDs.empty());

	wstring tit;
	for (const auto& repId : m_O365ReportIDs)
		tit += _YTEXT("+") + GET_O365Report_data(repId, GetShortUniqueName());

	return tit.substr(1);
}

wstring O365ReportCriteria::GetReportDescription(size_t p_Index) const
{
	ASSERT(p_Index < m_O365ReportIDs.size());
	return GET_O365Report_data(m_O365ReportIDs[p_Index], GetDescription());
}

wstring O365ReportCriteria::GetPeriodForDisplay(bool p_AddPrefixIfNeeded) const
{
	wstring str = _YTEXT("???");

	if (m_PeriodOrDate.empty())
	{
		str.clear();
	}
	else
	{
		bool isPeriod = Str::beginsWith(m_PeriodOrDate, _YTEXT("D"));
		ASSERT(!isPeriod
			|| m_PeriodOrDate == _YTEXT("D7")
			|| m_PeriodOrDate == _YTEXT("D30")
			|| m_PeriodOrDate == _YTEXT("D90")
			|| m_PeriodOrDate == _YTEXT("D180"));

		if (isPeriod)
		{
			str = p_AddPrefixIfNeeded	? YtriaTranslate::Do(O365ReportCriteria_GetPeriodForDisplay_1, _YLOC("Period: %1 days"), m_PeriodOrDate.substr(1).c_str())
										: YtriaTranslate::Do(O365ReportCriteria_GetPeriodForDisplay_2, _YLOC("%1 days"), m_PeriodOrDate.substr(1).c_str());
		}
		else
		{
			auto dateParts = Str::explodeIntoVector(m_PeriodOrDate, wstring(_YTEXT("-")), true, true);

			ASSERT(dateParts.size() == 3);
			if (dateParts.size() == 3)
			{
				SYSTEMTIME st{};
				st.wYear = static_cast<WORD>(Str::GetNumber(dateParts[0]));
				st.wMonth = static_cast<WORD>(Str::GetNumber(dateParts[1]));
				st.wDay = static_cast<WORD>(Str::GetNumber(dateParts[2]));
				YTimeDate timeDate(st, 0, true, false);
				str = p_AddPrefixIfNeeded	? YtriaTranslate::Do(ChartScrollView_PrintChart_1, _YLOC("From %1"), dateToString(timeDate).c_str())
											: YtriaTranslate::Do(DlgImportDxl_OnBtnChoosepath_5, _YLOC("%1"), dateToString(timeDate).c_str());
			}
		}
	}

	return str;
}

wstring O365ReportCriteria::GetCreationDateDisplay() const
{
	return m_CreationDate ? dateToString(*m_CreationDate) : _YTEXT("???");
}

Origin O365ReportCriteria::GetOrigin() const
{
	ASSERT(!m_O365ReportIDs.empty());
	Origin origin = GET_O365Report_data(m_O365ReportIDs[0], g_Origin);
	ASSERT(std::all_of(m_O365ReportIDs.begin(), m_O365ReportIDs.end(), [origin](auto id) {return origin == GET_O365Report_data(id, g_Origin); }));
	return origin;
}

O365ReportID O365ReportCriteria::GetO365ReportIDForShortName(const wstring& p_ShortName)
{
	for (auto id = int32_t(O365ReportID::Undefined) + 1; id < int32_t(O365ReportID::Count); ++id)
	{
		if (p_ShortName == GET_O365Report_data(id, GetShortUniqueName()))
			return O365ReportID(id);
	}

	return O365ReportID::Undefined;
}
