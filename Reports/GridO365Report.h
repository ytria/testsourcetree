#pragma once

#include "BaseO365Grid.h"
#include "O365ReportID.h"

class CsvParser;
class GridTemplateGroups;
class GridTemplateUsers;

class GridO365Report : public O365Grid
{
public:
	GridO365Report(std::vector<O365ReportID> p_ReportIDs);
	~GridO365Report();

	virtual void customizeGrid() override;
	virtual wstring GetName(const GridBackendRow* p_Row) const override;
	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

	struct Report
	{
		wstring m_CsvFilePath;
		wstring m_TargetPathError;
	};
	void LoadReports(const std::vector<Report>& p_Reports);
	void ShowLoadingError(const SapioError& p_Error);

	virtual void GetGACustomText(CString& p_CustomText) override;
	
	GridBackendColumn* MapColumnGraphID(GridBackendColumn* p_SpecificGraphIdColumn, const wstring& p_Family);

	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

protected:
	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

private:
	bool handleError(const CsvParser& p_Parser);
	static const bool looksLikeAnonymousData(const PooledString& p_Str);

private:
	std::unique_ptr<GridTemplateGroups> m_TemplateGroups;
	std::unique_ptr<GridTemplateUsers> m_TemplateUsers;
	int m_MissingMetaDataIcon;
	GridBackendColumn* m_ColForCommentRef;

public:
	class Impl;
	friend class Impl;

private:
	std::vector<Impl*> m_Impls;
	static Impl* createImpl(O365ReportID reportID);
};
