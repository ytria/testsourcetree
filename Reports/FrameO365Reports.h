#pragma once

#include "GridFrameBase.h"
#include "GridO365Report.h"

class FrameO365Reports : public GridFrameBase
{
public:
	FrameO365Reports(std::vector<O365ReportID> p_ReportIDs, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
	virtual ~FrameO365Reports() = default;

	void LoadReport(const std::vector<GridO365Report::Report>& p_Reports);
	void ShowLoadingError(const SapioError& p_Error);

	virtual O365Grid& GetGrid() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;

	void UpdateReportDate(const boost::YOpt<YTimeDate>& p_ReportDate);

protected:
	virtual void createGrid() override;
	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

private:
	void updateDateDisplay();

	GridO365Report m_GridReport;
	CXTPControl* m_ReportDateLabel;
};
