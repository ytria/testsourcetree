#include "O365ReportConfig.h"

#include "GridO365Report.h"
#include "GridBackendColumnSorter.h"
#include "GridTemplateGroups.h"
#include "GridTemplateUsers.h"

const O365ReportConfig::Column O365ReportConfig::Column::g_Empty;
wstring O365ReportConfig::g_DefaultFamily;
wstring O365ReportConfig::g_DefaultFamilyWithCategories;
#ifdef _DEBUG
std::set<wstring> O365ReportConfig::g_UidPrefixes;
#endif

O365ReportConfig::Column::Column(const wstring& p_Name, const wstring& p_Title, const wstring& p_UID, Type p_Type, uint32_t p_Width)
	: m_Name(p_Name)
	, m_Title(p_Title)
	, m_UID(p_UID)
	, m_Type(p_Type)
	, m_Width(p_Width)
{

}

O365ReportConfig::Column::Column(const wstring& p_Name, const wstring& p_Title, const wstring& p_UID, Type p_Type, uint32_t p_Width, uint32_t p_Flags)
	: m_Name(p_Name)
	, m_Title(p_Title)
	, m_UID(p_UID)
	, m_Type(p_Type)
	, m_Width(p_Width)
	, m_AdditionalPropsAnchor(ADDITIONAL_PROPS_ANCHOR == (p_Flags & ADDITIONAL_PROPS_ANCHOR))
	, m_Shared(SHARED == (p_Flags & SHARED))
	, m_ForPk(FOR_PK == (p_Flags & FOR_PK))
	, m_AtBack(INSERT_AT_BACK == (p_Flags & INSERT_AT_BACK))
	, m_ForCommentRef(FOR_COMMENT_REF == (p_Flags & FOR_COMMENT_REF))
	, m_Computed(COMPUTED == (p_Flags & COMPUTED))
{

}

bool O365ReportConfig::Column::IsValid() const
{
	return !m_Name.empty() && !m_UID.empty();
}

// =================================================================================================

namespace
{
	template<typename RawType>
	struct ValueType
	{
		virtual boost::YOpt<RawType> Parse(const wstring& p_Value) const = 0;
	};

	struct BoolType : public ValueType<bool>
	{
		boost::YOpt<bool> Parse(const wstring& p_Value) const override
		{
			if (MFCUtil::StringMatchW(p_Value, _YTEXT("true"))
				|| MFCUtil::StringMatchW(p_Value, _YTEXT("yes")))
				return true;
			if (MFCUtil::StringMatchW(p_Value, _YTEXT("false"))
				|| MFCUtil::StringMatchW(p_Value, _YTEXT("no")))
				return false;
			return boost::none;
		}
	};

	struct IntegerType : public ValueType<int64_t>
	{
		boost::YOpt<int64_t> Parse(const wstring& p_Value) const override
		{
			if (Str::IsNumber(p_Value, true) && Str::IsInteger(p_Value))
				return static_cast<int64_t>(Str::GetNumber(p_Value));
			return boost::none;
		}
	};

	using BytesType = IntegerType;

	struct StringType : public ValueType<PooledString>
	{
		boost::YOpt<PooledString> Parse(const wstring& p_Value) const override
		{
			if (!p_Value.empty())
				return p_Value;
			return boost::none;
		}
	};

	// Should we check for URL validity in Parse()?
	template<bool CheckURL>
	struct URLType : public StringType
	{
		boost::YOpt<PooledString> Parse(const wstring& p_Value) const override
		{
			if (!p_Value.empty())
			{
				if (CheckURL)
				{
					// The code below is similar to what's done in  CacheGridCellHyperlink::OnClick.

					// In case URL contains spaces, to please web::uri ctor
					//const auto fixedUrl = Str::Replace(p_Value, _YTEXT(" "), _YTEXT("%20"));

					try
					{
						web::uri uri(/*fixedUrl*/p_Value);
						const auto& scheme = uri.scheme();
						if (scheme != _YTEXT("http") && scheme != _YTEXT("https"))
							return boost::none;
					}
					catch (const std::exception&)
					{
						return boost::none;
					}
				}
				return p_Value;
			}
			return boost::none;
		}
	};

	struct StringListType : public ValueType<vector<wstring>>
	{
		boost::YOpt<vector<wstring>> Parse(const wstring& p_Value) const override
		{
			// Bug #200617.SB.00C256: Because of MICROSOFT STUPIDITY, change the grid mapping rule of license data in Usage Reports (ignore "space+space")
			const auto fixedString = Str::Replace(p_Value, _YTEXT(" + "), _YTEXT("&%|"));
			auto stringList = Str::explodeIntoVector(fixedString, wstring(_YTEXT("+")), false, false);
			for (auto& str : stringList)
				str = Str::Replace(str, _YTEXT("&%|"), _YTEXT(" + "));

			return stringList;
		}
	};

	struct DateType : public ValueType<YTimeDate>
	{
		boost::YOpt<YTimeDate> Parse(const wstring& p_Value) const override
		{
			boost::YOpt<YTimeDate> timeDate;
			timeDate.emplace();
			if (!TimeUtil::GetInstance().ConvertTextToDate(p_Value, *timeDate))
				timeDate.reset();
			return timeDate;
		}
	};
}

O365ReportConfig::O365ReportConfig()
	: m_Origin(Origin::NotSet)
{
	if (g_DefaultFamily.empty())
		g_DefaultFamily = _T("Report Data");
	if (g_DefaultFamilyWithCategories.empty())
		g_DefaultFamilyWithCategories = _T("Common Data");

	/*AddIgnoredColumns*/AddHiddenByDefaultColumns({_YTEXT("Report Refresh Date"), _YTEXT("Report Period")});
}

O365ReportConfig::~O365ReportConfig()
{
#ifdef _DEBUG
	if (!m_UidPrefix.empty())
		g_UidPrefixes.erase(m_UidPrefix);
#endif
}

void O365ReportConfig::AddColumn(const Column&& p_Column)
{
	ASSERT(m_Columns.find(p_Column.m_Name) == m_Columns.end());
	ASSERT(!p_Column.m_AdditionalPropsAnchor || std::none_of(m_Columns.begin(), m_Columns.end(), [](auto& c) {return c.second.m_AdditionalPropsAnchor; }));

	auto it = m_Columns.insert({ p_Column.m_Name, std::move(p_Column) }).first;

	// add uid prefix if non-shared column
	if (!it.value().m_Shared && !m_UidPrefix.empty())
		it.value().m_UID = m_UidPrefix + _YTEXT(".") + it.value().m_UID;

	if (it.value().m_ForPk)
	{
		ASSERT(!it.value().m_Computed);
		m_PkColumnNames.push_back(it.value().m_Name);
	}
}

void O365ReportConfig::AddIgnoredColumns(const vector<wstring>&& p_ColumnNames)
{
	m_IgnoredColumnNames.insert(p_ColumnNames.begin(), p_ColumnNames.end());
}

void O365ReportConfig::AddHiddenByDefaultColumns(const vector<wstring>&& p_ColumnNames)
{
	m_HiddenByDefaultColumnNames.insert(p_ColumnNames.begin(), p_ColumnNames.end());
}

void O365ReportConfig::SetColumnForAnonymousDataChecking(const wstring& p_ColumnName)
{
	p_ColumnNameForAnonymousData = p_ColumnName;
}

void O365ReportConfig::SetColumnsForAdditionalMetadataMatching(vector<std::pair<wstring, wstring>>&& p_ColumnNamesToObjectProperties)
{
	m_ColumnNamesToObjectProperties = std::move(p_ColumnNamesToObjectProperties);
}

void O365ReportConfig::SetConditionForAdditionalMetadataMatching(std::function<bool(BusinessObject*)> p_Condition)
{
	m_Condition = p_Condition;
}

void O365ReportConfig::Configure(GridO365Report& p_Grid, std::unique_ptr<GridTemplateUsers>& p_TemplateUsers, std::unique_ptr<GridTemplateGroups>& p_TemplateGroups, const vector<wstring>& p_AdditionalProperties, int p_NoFieldIconForAdditionalColumns, boost::YOpt<std::map<wstring, std::vector<GridBackendColumn*>>>& p_Categories)
{
	ASSERT(!p_TemplateUsers || !p_TemplateGroups);

	ASSERT(!m_AutomationName.empty());
	// In case of several reports, auto name is a combination
	if (p_Grid.GetAutomationName().empty())
		p_Grid.SetAutomationName(m_AutomationName);
	else
		p_Grid.SetAutomationName(p_Grid.GetAutomationName() + _YTEXT("+") + m_AutomationName);

	if (m_RowType)
	{
		GridUtil::Add365ObjectTypes(p_Grid);
		if (nullptr == p_Grid.GetColumnObjectType())
		{
			GridBackendColumn* colObjType = p_Grid.AddColumnObjectType();
			ASSERT(nullptr != colObjType);
			if (nullptr != colObjType)
				colObjType->SetLocked(true);
		}
	}

	std::map<wstring, std::vector<GridBackendColumn*>> categories;
	GridBackendColumn* addDataAnchor = nullptr;
	for (const auto& item : m_Columns)
	{
		auto col = addColumn(item.second, p_Grid, p_Categories.is_initialized());
		if (item.second.m_AdditionalPropsAnchor)
			addDataAnchor = col;
		
		if (p_Categories.is_initialized())
		{
			auto& cols = (*p_Categories)[col->GetFamily()];
			if (cols.end() == std::find(cols.begin(), cols.end(), col))
				cols.push_back(col);
		}
	}

	if (p_TemplateUsers)
	{
		for (const auto& prop : p_AdditionalProperties)
		{
			auto column = p_TemplateUsers->AddDefaultColumnFor(p_Grid, prop, _T("User"), { O365Grid::g_ColumnsPresetDefault }, _YTEXT("\U00002B9E %s"), _YTEXT("addtnl.%s"));
			if (nullptr != column)
			{
				column->SetNoFieldText(_T("N/A"));
				column->SetNoFieldTooltip(_T("Not available in cache."));
				column->SetNoFieldIcon(p_NoFieldIconForAdditionalColumns);
				column->SetLParam(p_NoFieldIconForAdditionalColumns);
				if (nullptr != addDataAnchor)
				{
					p_Grid.MoveColumnAfter(column, addDataAnchor);
					addDataAnchor = addDataAnchor;
				}

				if (p_Categories.is_initialized())
				{
					auto& cols = (*p_Categories)[column->GetFamily()];
					if (cols.end() == std::find(cols.begin(), cols.end(), column))
						cols.push_back(column);
				}
			}
		}
		p_TemplateUsers->CustomizeGrid(p_Grid);
	}
	else if (p_TemplateGroups)
	{
		for (const auto& prop : p_AdditionalProperties)
		{
			auto column = p_TemplateGroups->AddDefaultColumnFor(p_Grid, prop, _T("Group"), { O365Grid::g_ColumnsPresetDefault }, _YTEXT("\U00002B9E %s"), _YTEXT("addtnl.%s"));
			if (nullptr != column)
			{
				column->SetNoFieldText(_T("N/A"));
				column->SetNoFieldTooltip(_T("Not available in cache."));
				column->SetNoFieldIcon(p_NoFieldIconForAdditionalColumns);
				column->SetLParam(p_NoFieldIconForAdditionalColumns);
				if (nullptr != addDataAnchor)
				{
					p_Grid.MoveColumnAfter(column, addDataAnchor);
					addDataAnchor = addDataAnchor;
				}

				if (p_Categories.is_initialized())
				{
					auto& cols = (*p_Categories)[column->GetFamily()];
					if (cols.end() == std::find(cols.begin(), cols.end(), column))
						cols.push_back(column);
				}
			}
		}
		p_TemplateGroups->CustomizeGrid(p_Grid);
	}
	else
	{
		ASSERT(p_AdditionalProperties.empty());
	}

	// Graph ID column
	GridBackendColumn* idColumn = nullptr;
	{
		idColumn = p_Grid.MapColumnGraphID(!NeedsIdFromCache() ? GetGraphIdColumn(p_Grid) : nullptr, p_Categories.is_initialized() ? g_DefaultFamilyWithCategories : g_DefaultFamily);
		if (p_Categories.is_initialized())
		{
			auto& cols = (*p_Categories)[idColumn->GetFamily()];
			if (cols.end() == std::find(cols.begin(), cols.end(), idColumn))
				cols.push_back(idColumn);
		}

		if (p_TemplateUsers)
			p_TemplateUsers->m_ColumnID = idColumn;
		else if (p_TemplateGroups)
			p_TemplateGroups->m_ColumnID = idColumn;
	}

	std::vector<GridBackendColumn*> colAtBack;
	for (const auto& item : m_Columns)
	{
		if (item.second.m_AtBack)
		{
			auto column = p_Grid.GetColumnByUniqueID(item.second.m_UID);
			if (nullptr != column)
				colAtBack.push_back(column);
		}
	}

	if (nullptr != idColumn && NeedsIdFromCache() && colAtBack.end() == std::find(colAtBack.begin(), colAtBack.end(), idColumn))
		colAtBack.push_back(idColumn);

	if (!colAtBack.empty())
	{
		std::sort(p_Grid.GetBackend().GetColumns().begin(), p_Grid.GetBackend().GetColumns().end(), GridBackendColumnSorter<BYPOSITIONVIRTUAL>());
		auto previousColumn = p_Grid.GetBackend().GetColumns().back();
		for (auto col : colAtBack)
		{
			if (col != previousColumn)
			{
				p_Grid.MoveColumnAfter(col, previousColumn, false);
				previousColumn = col;
			}
		}
	}
}

GridBackendRow* O365ReportConfig::AddRow(GridO365Report& p_Grid, const vector<wstring>& p_Headers, const vector<wstring>& p_Values, bool p_ForRefresh)
{
	GridBackendRow* row = nullptr;
	if (!GetPkColumnNames().empty())
	{
		vector<GridBackendField> rowPk;
		{
			for (const auto& pk : GetPkColumnNames())
			{
				auto& p_Column = getColumn(pk);
				auto it = std::find(p_Headers.begin(), p_Headers.end(), pk);
				ASSERT(p_Headers.end() != it);
				if (p_Headers.end() != it && p_Values.size() >= p_Headers.size())
				{
					auto& csvVal = p_Values[std::distance(p_Headers.begin(), it)];

					// Do we really want another type as PK?
					ASSERT(Column::STRING == p_Column.m_Type);
					const auto val = StringType().Parse(csvVal);
					if (val)
						rowPk.emplace_back(*val, p_Grid.GetColumnByUniqueID(p_Column.m_UID));
				}
			}
		}

		row = p_Grid.GetRowIndex().GetRow(rowPk, true, p_ForRefresh);
	}
	else
	{
		row = p_Grid.AddRow();
	}

	for (size_t i = 0; i < (std::min)(p_Values.size(), p_Headers.size()); ++i)
		addField(p_Grid, row, p_Headers[i], p_Values[i]);

	if (m_RowType)
		p_Grid.SetRowObjectType(row, *m_RowType);

	if (m_NewRowPostProcess)
		m_NewRowPostProcess(row);

	return row;
}

void O365ReportConfig::AdaptColumnsToRealHeaders(GridO365Report& p_Grid, const vector<wstring>& p_Headers)
{
	auto headers = p_Headers;
	auto columns = m_Columns;

	// Remove computed columns
	for (auto it = columns.begin(); it != columns.end(); )
	{
		if (it->second.m_Computed)
			it = columns.erase(it);
		else
			++it;
	}

	for (auto it = headers.begin(); it != headers.end(); )
	{
		const auto& header = *it;
		bool erase = isIgnored(header);
		if (!erase)
		{
			auto itC = columns.find(header);
			if (columns.end() != itC)
			{
				erase = true;
				columns.erase(itC);
			}
		}

		if (erase)
			it = headers.erase(it);
		else
			++it;
	}

	// Columns missing in the report
	if (!columns.empty())
	{
		ASSERT(false); // This assert exists only so that we can easily catch changes in reports.
		wstring trace = _YFORMAT(L"Missing %s columns in %s report: ", Str::getStringFromNumber(columns.size()).c_str(), m_AutomationName.c_str());
		vector<GridBackendColumn*> columnsToRemove;
		columnsToRemove.reserve(columns.size());
		for (auto& item : columns)
		{
			auto p_Column = p_Grid.GetColumnByUniqueID(item.second.m_UID);
			columnsToRemove.push_back(p_Column);
			m_Columns.erase(item.first);
			trace += item.first + _YTEXT(", ");
		}

		ASSERT(!columnsToRemove.empty());
		if (!columnsToRemove.empty())
			p_Grid.RemoveColumns(columnsToRemove);

		LoggerService::Debug(trace, p_Grid.m_hWnd);
	}

	// Columns unexpected in the report
	if (!headers.empty())
	{
		ASSERT(false); // This assert exists only so that we can easily catch changes in reports.
		wstring trace = _YFORMAT(L"%s columns unknown in %s report: ", Str::getStringFromNumber(headers.size()).c_str(), m_AutomationName.c_str());
		for (const auto& newHeader : headers)
		{
			// FIXME: We should probably do better and try to deduce the column type from the data...
			AddColumn({ newHeader, newHeader, Str::ToCamelCase(newHeader), Column::VARIANT, CacheGrid::g_ColumnSize22 });
			auto& newCol = getColumn(newHeader);
			ASSERT(newCol.IsValid());
			auto p_Column = addColumn(newCol, p_Grid, false);
			trace += newHeader + _YTEXT(", ");
		}

		LoggerService::Debug(trace, p_Grid.m_hWnd);
	}
}

void O365ReportConfig::UpdateVariantColumnsTypes(GridO365Report& p_Grid)
{
	for (auto& item : m_Columns)
	{
		if (item.second.m_Type == Column::VARIANT)
		{
			auto p_Column = p_Grid.GetColumnByUniqueID(item.second.m_UID);
			const auto actualColType = p_Column->GetLParam() - 1;
			ASSERT(Column::BOOL <= actualColType);
			if (Column::VARIANT != actualColType)
			{
				switch (actualColType)
				{
				case Column::BOOL:
					p_Column->SetType(GridBackendUtil::LONG);
					p_Column->SetDefaultCellType(GridBackendUtil::CELL_TYPE_CHECKBOX);
					p_Column->SetWidth(CacheGrid::g_ColumnSize3);
					break;
				case Column::BYTES:
					p_Column->SetType(GridBackendUtil::LONG);
					p_Column->SetDefaultCellType(GridBackendUtil::CELL_TYPE_LONG);
					p_Column->SetWidth(CacheGrid::g_ColumnSize8);
					break;
				case Column::DATE:
					p_Column->SetType(GridBackendUtil::DATE);
					p_Column->SetDefaultCellType(GridBackendUtil::CELL_TYPE_DATETIME);
					p_Column->SetWidth(CacheGrid::g_ColumnSize16);
					break;
				case Column::INTEGER:
					p_Column->SetType(GridBackendUtil::LONG);
					p_Column->SetDefaultCellType(GridBackendUtil::CELL_TYPE_LONG);
					p_Column->SetWidth(CacheGrid::g_ColumnSize8);
					break;
				case Column::STRING:
				case Column::STRINGLIST:
					p_Column->SetType(GridBackendUtil::STRING);
					p_Column->SetDefaultCellType(GridBackendUtil::CELL_TYPE_STRING);
					p_Column->SetWidth(CacheGrid::g_ColumnSize22);
					break;
				case Column::URL:
					p_Column->SetType(GridBackendUtil::HYPERLINK);
					p_Column->SetDefaultCellType(GridBackendUtil::CELL_TYPE_HYPERLINK);
					p_Column->SetWidth(CacheGrid::g_ColumnSize8);

					// FIXME: If we never called AddColumnHyperlink, those calls are missing...
					p_Grid.HoverEventsSet(true, false);
					p_Grid.HoverHighlightSet(true, false, false, false, false, false);
					break;
				default:
					ASSERT(false); // Handle other types when needed.
					break;
				}
			}
		}
	}
}

void O365ReportConfig::SetDataOrigin(Origin p_Origin)
{
	m_Origin = p_Origin;
	switch(m_Origin)
	{
	case Origin::User:
		SetRowType(BusinessUser().get_type().get_id());
		break;
	case Origin::Group:
		SetRowType(BusinessGroup().get_type().get_id());
		break;
	case Origin::Site:
		SetRowType(BusinessSite().get_type().get_id());
		break;
	}
}

Origin O365ReportConfig::GetDataOrigin() const
{
	return m_Origin;
}

void O365ReportConfig::SetColumnForGraphId(const wstring& p_ColumnName)
{
	m_ColumnNameForGraphId = p_ColumnName;
}

const wstring& O365ReportConfig::GetColumnUniqueIdForGraphId() const
{
	return m_ColumnNameForGraphId;
}

bool O365ReportConfig::NeedsIdFromCache() const
{
	return m_ColumnNameForGraphId.empty();
}

GridBackendColumn* O365ReportConfig::GetGraphIdColumn(GridO365Report& p_Grid) const
{
	ASSERT(!m_ColumnNameForGraphId.empty());
	return m_ColumnNameForGraphId.empty()
		? nullptr
		: p_Grid.GetColumnByUniqueID(getColumn(m_ColumnNameForGraphId).m_UID);
}

void O365ReportConfig::SetRowType(const rttr::type::type_id& p_TypeID)
{
	m_RowType = p_TypeID;
}

void O365ReportConfig::SetAutomationName(const wstring& p_AutomationName)
{
	m_AutomationName = p_AutomationName;
}

GridBackendColumn* O365ReportConfig::GetColumnForAnonymousDataChecking(GridO365Report& p_Grid) const
{
	ASSERT(!p_ColumnNameForAnonymousData.empty());
	return p_ColumnNameForAnonymousData.empty()
			? nullptr
			: p_Grid.GetColumnByUniqueID(getColumn(p_ColumnNameForAnonymousData).m_UID);
}

vector<std::pair<GridBackendColumn*, wstring>> O365ReportConfig::GetColumnsForAdditionalMetadataMatching(GridO365Report& p_Grid) const
{
	vector<std::pair<GridBackendColumn*, wstring>> columns;

	for (auto& addCol : m_ColumnNamesToObjectProperties)
	{
		auto& columnConfig = getColumn(addCol.first);
		ASSERT(columnConfig.IsValid());
		if (columnConfig.IsValid())
		{
			auto p_Column = p_Grid.GetColumnByUniqueID(columnConfig.m_UID);
			ASSERT(nullptr != p_Column);
			if (nullptr != p_Column)
				columns.emplace_back(p_Column, addCol.second);
		}
	}

	return columns;
}

bool O365ReportConfig::CheckConditionForAdditionalMetadataMatching(BusinessObject* p_Object)
{
	return !m_Condition || m_Condition(p_Object);
}

GridBackendColumn* O365ReportConfig::GetColumnForCommentRef(GridO365Report& p_Grid) const
{
	for (auto& col : m_Columns)
	{
		if (col.second.m_ForCommentRef)
			return p_Grid.GetColumnByUniqueID(col.second.m_UID);
	}

	return nullptr;
}

void O365ReportConfig::SetUidPrefix(const wstring& p_Prefix)
{
	ASSERT(m_UidPrefix.empty()); // Never change once it's set.
	ASSERT(g_UidPrefixes.end() == g_UidPrefixes.find(p_Prefix)); // Can't have prefixe duplicates
	m_UidPrefix = p_Prefix;
}

void O365ReportConfig::SetCategoryName(const wstring& p_Category)
{
	m_Category = p_Category;
}

void O365ReportConfig::addNewRowPostProcess(std::function<void(GridBackendRow*)> p_NewRowPostProcess)
{
	ASSERT(!m_NewRowPostProcess);
	m_NewRowPostProcess = p_NewRowPostProcess;
}

const O365ReportConfig::O365ReportConfig::Column& O365ReportConfig::getColumn(const wstring& p_ColumnName) const
{
	auto it = m_Columns.find(p_ColumnName);
	if (m_Columns.end() != it)
		return it->second;
	return Column::g_Empty;
}

const vector<wstring>& O365ReportConfig::GetPkColumnNames() const
{
	return m_PkColumnNames;
}

bool O365ReportConfig::addField(GridO365Report& p_Grid, GridBackendRow* p_Row, const wstring& p_ColumnName, const wstring& p_Value)
{
	bool success = true;

	if (!isIgnored(p_ColumnName))
	{
		auto& columnConfig = getColumn(p_ColumnName);
		ASSERT(columnConfig.IsValid());
		if (columnConfig.IsValid())
		{
			auto p_Column = p_Grid.GetColumnByUniqueID(columnConfig.m_UID);
			ASSERT(nullptr != p_Column);

			if (nullptr != p_Column)
			{
				auto fieldType = columnConfig.m_Type;
				if (fieldType == Column::VARIANT)
				{
					fieldType = Column::STRING;
					if (!p_Value.empty())
					{
						if (DateType().Parse(p_Value))
						{
							fieldType = Column::DATE;
						}
						else if (IntegerType().Parse(p_Value))
						{
							if (Str::contains(columnConfig.m_Name, _YTEXT("(Byte)")))
								fieldType = Column::BYTES;
							else
								fieldType = Column::INTEGER;
						}
						else if (BoolType().Parse(p_Value))
						{
							fieldType = Column::BOOL;
						}
						else if (URLType<true>().Parse(p_Value))
						{
							fieldType = Column::URL;
						}
					}
				}

				switch (fieldType)
				{
				case Column::BOOL:
					p_Row->AddFieldForCheckBox(BoolType().Parse(p_Value), p_Column);
					break;
				case Column::BYTES:
					p_Row->AddField(BytesType().Parse(p_Value), p_Column);
					break;
				case Column::DATE:
					p_Row->AddField(DateType().Parse(p_Value), p_Column);
					break;
				case Column::INTEGER:
					p_Row->AddField(IntegerType().Parse(p_Value), p_Column);
					break;
				case Column::STRING:
					p_Row->AddField(StringType().Parse(p_Value), p_Column);
					break;
				case Column::STRINGLIST:
					p_Row->AddField(StringListType().Parse(p_Value), p_Column);
					break;
				case Column::URL:
					p_Row->AddFieldForHyperlink(URLType<false>().Parse(p_Value), p_Column);
					break;
				default:
					ASSERT(false); // Handle other types when needed.
					success = false;
					break;
				}

				if (columnConfig.m_Type == Column::VARIANT)
				{
					if (p_Column->GetLParam() == 0)
						p_Column->SetLParam(fieldType + 1);
					else if (p_Column->GetLParam() != fieldType + 1)
						p_Column->SetLParam(Column::VARIANT + 1);
				}
			}
			else
			{
				LoggerService::Debug(_YDUMPFORMAT(L"GridO365Report::LoadReport Column %s not found", p_ColumnName.c_str()));
				success = false;
			}
		}
		else
		{
			LoggerService::Debug(_YDUMPFORMAT(L"GridO365Report::LoadReport Unknown column %s", p_ColumnName.c_str()));
			success = false;
		}
	}

	return success;
}

bool O365ReportConfig::isIgnored(const wstring& p_ColumnName) const
{
	return m_IgnoredColumnNames.end() != m_IgnoredColumnNames.find(p_ColumnName);
}

bool O365ReportConfig::isHiddenByDefault(const wstring& p_ColumnName) const
{
	return m_HiddenByDefaultColumnNames.end() != m_HiddenByDefaultColumnNames.find(p_ColumnName);
}

GridBackendColumn* O365ReportConfig::addColumn(const Column& p_Column, GridO365Report& p_Grid, bool p_AddCategories)
{
	auto column = p_Grid.GetColumnByUniqueID(p_Column.m_UID);
	if (nullptr == column || !p_Column.m_Shared)
	{
		// Theoretically UIDs are made unique across all reports
		ASSERT(nullptr == column);
		const auto uid = p_Column.m_UID;

		const wstring& family = p_AddCategories ? (!p_Column.m_Shared && !m_Category.empty() ? m_Category : g_DefaultFamilyWithCategories)
												: g_DefaultFamily;

		switch (p_Column.m_Type)
		{
		case Column::BOOL:
			column = p_Grid.AddColumnCheckBox(uid, p_Column.m_Title, family, p_Column.m_Width);
			break;
		case Column::BYTES:
			column = p_Grid.AddColumnNumberInt(uid, p_Column.m_Title, family, p_Column.m_Width);
			column->SetXBytesColumnFormat();
			break;
		case Column::DATE:
			column = p_Grid.AddColumnDate(uid, p_Column.m_Title, family, p_Column.m_Width);
			break;
		case Column::INTEGER:
			column = p_Grid.AddColumnNumberInt(uid, p_Column.m_Title, family, p_Column.m_Width);
			break;
		case Column::STRING:
		case Column::STRINGLIST:
			column = p_Grid.AddColumn(uid, p_Column.m_Title, family, p_Column.m_Width);
			break;
		case Column::URL:
			column = p_Grid.AddColumnHyperlink(uid, p_Column.m_Title, family, p_Column.m_Width);
			break;
		case Column::VARIANT:
			column = p_Grid.AddColumnVariant(uid, p_Column.m_Title, family, p_Column.m_Width);
			break;
		default:
			ASSERT(false); // Handle other types when needed.
			break;
		}

		if (nullptr != column && !isHiddenByDefault(p_Column.m_Name))
			column->SetPresetFlags({ O365Grid::g_ColumnsPresetDefault });

		if (nullptr != column && p_Column.m_ForPk)
			p_Grid.AddColumnForRowPK(column);
	}

	return column;
}
