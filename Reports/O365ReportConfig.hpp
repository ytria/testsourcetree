#pragma once

#include "O365ReportConfig.h"

#include "BusinessGroup.h"
#include "BusinessSite.h"
#include "BusinessUser.h"


template <O365ReportID reportID>
void O365ReportConfigImpl<reportID>::commonInit()
{
	SetDataOrigin(O365Report<reportID>::g_Origin);
	SetAutomationName(O365Report<reportID>::GetShortUniqueName());
	SetCategoryName(O365Report<reportID>::GetDisplayName());
	SetUidPrefix(O365Report<reportID>::GetShortUniqueName());
}

O365ReportConfigImpl<O365ReportID::OneDriveActivityUserDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));
	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Deleted Date"), _T("Deleted Date"),_YUID("deletedDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"),_YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Viewed Or Edited File Count"), _T("Viewed Or Edited File Count"), _YUID("viewedOrEditedFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Synced File Count"), _T("Synced File Count"), _YUID("syncedFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Shared Internally File Count"), _T("Shared Internally File Count"), _YUID("sharedInternallyFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Shared Externally File Count"), _T("Shared Externally File Count"), _YUID("sharedExternallyFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Assigned Products"), _T("Assigned Products"), _YUID("assignedProducts"), Column::STRINGLIST, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::OneDriveUsageAccountDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("Owner Display Name"), _T("Display Name"), _YUID("displayName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED });

	AddColumn({ _YTEXT("Owner Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("Owner Principal Name"));
	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("Owner Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Site URL"), _T("Site URL"), _YUID("siteURL"), Column::URL, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("File Count"), _T("File Count"), _YUID("fileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Active File Count"), _T("Active File Count"), _YUID("activeFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Storage Used (Byte)"), _T("Storage Used (Byte)"), _YUID("storageUsed"), Column::BYTES, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Storage Allocated (Byte)"), _T("Storage Allocated (Byte)"), _YUID("storageAllocated"), Column::BYTES, CacheGrid::g_ColumnSize8 });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::TeamsDeviceUsageUserDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));
	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Deleted Date"), _T("Deleted Date"), _YUID("deletedDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Used Web"), _T("Used Web"), _YUID("usedWeb"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used Windows Phone"), _T("Used Windows Phone"), _YUID("usedWindowsPhone"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used iOS"), _T("Used iOS"), _YUID("usediOS"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used Mac"), _T("Used Mac"), _YUID("usedMac"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used Android Phone"), _T("Used Android Phone"), _YUID("usedAndroidPhone"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used Windows"), _T("Used Windows"), _YUID("usedWindows"), Column::BOOL, CacheGrid::g_ColumnSize3 });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::TeamsUserActivityUserDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));
	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Deleted Date"), _T("Deleted Date"), _YUID("deletedDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Assigned Products"), _T("Assigned Products"), _YUID("assignedProducts"), Column::STRINGLIST, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED });
	AddColumn({ _YTEXT("Team Chat Message Count"), _T("Team Chat Message Count"), _YUID("teamChatMessageCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Private Chat Message Count"), _T("Private Chat Message Count"), _YUID("privateChatMessageCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Call Count"), _T("Call Count"), _YUID("callCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Meeting Count"), _T("Meeting Count"), _YUID("meetingCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Has Other Action"), _T("Has Other Action"), _YUID("hasOtherAction"), Column::BOOL, CacheGrid::g_ColumnSize3 });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::EmailActivityUserDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));

	AddColumn({ _YTEXT("Display Name"), _T("Display Name"), _YUID("displayName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR });

	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Deleted Date"), _T("Deleted Date"), _YUID("deletedDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Send Count"), _T("Send Count"), _YUID("sendCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Receive Count"), _T("Receive Count"), _YUID("receiveCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Read Count"), _T("Read Count"), _YUID("readCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Assigned Products"), _T("Assigned Products"), _YUID("assignedProducts"), Column::STRINGLIST, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::EmailAppUsageUserDetail>::O365ReportConfigImpl()
{
	// FIXME: Make it hierarchical

	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));

	AddColumn({ _YTEXT("Display Name"), _T("Display Name"), _YUID("displayName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR });

	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Deleted Date"), _T("Deleted Date"), _YUID("deletedDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Mail For Mac"), _T("Mail For Mac"), _YUID("mailForMac"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("Outlook For Mac"), _T("Outlook For Mac"), _YUID("outlookForMac"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("Outlook For Windows"), _T("Outlook For Windows"), _YUID("outlookForWindows"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("Outlook For Mobile"), _T("Outlook For Mobile"), _YUID("outlookForMobile"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("Other For Mobile"), _T("Other For Mobile"), _YUID("otherForMobile"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("Outlook For Web"), _T("Outlook For Web"), _YUID("outlookForWeb"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("POP3 App"), _T("POP3 App"), _YUID("pop3App"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("IMAP4 App"), _T("IMAP4 App"), _YUID("imap4App"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("SMTP App"), _T("SMTP App"), _YUID("smtpApp"), Column::STRING, CacheGrid::g_ColumnSize22 });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::MailboxUsageDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));

	AddColumn({ _YTEXT("Display Name"), _T("Display Name"), _YUID("displayName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR });

	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Deleted Date"), _T("Deleted Date"), _YUID("deletedDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Created Date"), _T("Created Date"), _YUID("createdDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Item Count"), _T("Item Count"), _YUID("itemCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Storage Used (Byte)"), _T("Storage Used (Byte)"), _YUID("storageUsed"), Column::BYTES, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Issue Warning Quota (Byte)"), _T("Issue Warning Quota (Byte)"), _YUID("issueWarningQuota"), Column::BYTES, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Prohibit Send Quota (Byte)"), _T("Prohibit Send Quota (Byte)"), _YUID("prohibitSendQuota"), Column::BYTES, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Prohibit Send/Receive Quota (Byte)"), _T("Prohibit Send/Receive Quota (Byte)"), _YUID("prohibitSendReceiveQuota"), Column::BYTES, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Deleted Item Count"), _T("Deleted Item Count"), _YUID("deletedItemCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Deleted Item Size (Byte)"), _T("Deleted Item Size (Byte)"), _YUID("deletedItemSize"), Column::BYTES, CacheGrid::g_ColumnSize8 });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::Office365GroupsActivityDetail>::O365ReportConfigImpl()
{
	commonInit();


	AddColumn({ _YTEXT("Group Display Name"), _T("Group Display Name"), _YUID("groupDisplayName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("Group Display Name"));

	AddColumn({ _YTEXT("Group Id"), _T("Group Id"), _YUID("groupID"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR | O365ReportConfig::Column::FOR_PK }); //Make it Tech
	SetColumnForGraphId(_YTEXT("Group Id"));
	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("Group Id"), _YTEXT(O365_ID) } });

	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Owner Principal Name"), _T("Owner Principal Name"), _YUID("ownerPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22});
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Group Type"), _T("Group Type"), _YUID("groupType"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("Member Count"), _T("Member Count"), _YUID("memberCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("External Member Count"), _T("External Member Count"), _YUID("externalMemberCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Exchange Received Email Count"), _T("Exchange Received Email Count"), _YUID("exchangeReceivedEmailCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("SharePoint Active File Count"), _T("SharePoint Active File Count"), _YUID("sharePointActiveFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Yammer Posted Message Count"), _T("Yammer Posted Message Count"), _YUID("yammerPostedMessageCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Yammer Read Message Count"), _T("Yammer Read Message Count"), _YUID("yammerReadMessageCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Yammer Liked Message Count"), _T("Yammer Liked Message Count"), _YUID("yammerLikedMessageCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Exchange Mailbox Total Item Count"), _T("Exchange Mailbox Total Item Count"), _YUID("exchangeMailboxTotalItemCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Exchange Mailbox Storage Used (Byte)"), _T("Exchange Mailbox Storage Used (Byte)"), _YUID("exchangeMailboxStorageUsed"), Column::BYTES, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("SharePoint Total File Count"), _T("SharePoint Total File Count"), _YUID("sharePointTotalFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("SharePoint Site Storage Used (Byte)"), _T("SharePoint Site Storage Used (Byte)"), _YUID("sharePointSiteStorageUsed"), Column::BYTES, CacheGrid::g_ColumnSize8 });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::Office365ActiveUserDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));

	AddColumn({ _YTEXT("Display Name"), _T("Display Name"), _YUID("displayName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR });

	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Deleted Date"), _T("Deleted Date"), _YUID("deletedDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Has Exchange License"), _T("Has Exchange License"), _YUID("hasExchangeLicense"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Has OneDrive License"), _T("Has OneDrive License"), _YUID("hasOneDriveLicense"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Has SharePoint License"), _T("Has SharePoint License"), _YUID("hasSharePointLicense"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Has Skype For Business License"), _T("Has Skype For Business License"), _YUID("hasSkypeForBusinessLicense"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Has Yammer License"), _T("Has Yammer License"), _YUID("hasYammerLicense"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Has Teams License"), _T("Has Teams License"), _YUID("hasTeamsLicense"), Column::BOOL, CacheGrid::g_ColumnSize3 });

	AddColumn({ _YTEXT("LASTACTIVITYSERVICE"), _T("Last Activity service"), _YUID("LASTACTIVITYSERVICE"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::COMPUTED });
	AddColumn({ _YTEXT("LASTACTIVITYDATE"), _T("Last Activity date"), _YUID("LASTACTIVITYDATE"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::COMPUTED });

	AddColumn({ _YTEXT("Exchange Last Activity Date"), _T("Exchange Last Activity Date"), _YUID("exchangeLastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("OneDrive Last Activity Date"), _T("OneDrive Last Activity Date"), _YUID("oneDriveLastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("SharePoint Last Activity Date"), _T("SharePoint Last Activity Date"), _YUID("sharePointLastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Skype For Business Last Activity Date"), _T("Skype For Business Last Activity Date"), _YUID("skypeForBusinessLastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Yammer Last Activity Date"), _T("Yammer Last Activity Date"), _YUID("yammerLastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Teams Last Activity Date"), _T("Teams Last Activity Date"), _YUID("teamsLastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Exchange License Assign Date"), _T("Exchange License Assign Date"), _YUID("exchangeLicenseAssignDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("OneDrive License Assign Date"), _T("OneDrive License Assign Date"), _YUID("oneDriveLicenseAssignDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("SharePoint License Assign Date"), _T("SharePoint License Assign Date"), _YUID("sharePointLicenseAssignDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Skype For Business License Assign Date"), _T("Skype For Business License Assign Date"), _YUID("skypeForBusinessLicenseAssignDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Yammer License Assign Date"), _T("Yammer License Assign Date"), _YUID("yammerLicenseAssignDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Teams License Assign Date"), _T("Teams License Assign Date"), _YUID("teamsLicenseAssignDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Assigned Products"), _T("Assigned Products"), _YUID("assignedProducts"), Column::STRINGLIST, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED });

	// Hidden by default
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED });

	addNewRowPostProcess([this](GridBackendRow* p_Row)
		{
			auto gridBackend = p_Row->GetBackend();
			ASSERT(nullptr != gridBackend);

			boost::YOpt<YTimeDate> lastActivityDate;
			boost::YOpt<wstring> lastActivityService;

			for (auto& columnName :
				{
					wstring(_YTEXT("Exchange Last Activity Date")),
					wstring(_YTEXT("OneDrive Last Activity Date")),
					wstring(_YTEXT("SharePoint Last Activity Date")),
					wstring(_YTEXT("Skype For Business Last Activity Date")),
					wstring(_YTEXT("Yammer Last Activity Date")),
					wstring(_YTEXT("Teams Last Activity Date")),
				})
			{
				auto& columnConfig = getColumn(columnName);
				ASSERT(columnConfig.IsValid());
				if (columnConfig.IsValid())
				{
					auto dateColumn = gridBackend->GetColumnByUniqueID(columnConfig.m_UID);
					if (p_Row->HasField(dateColumn) && p_Row->GetField(dateColumn).HasValue() && p_Row->GetField(dateColumn).IsDate())
					{
						auto date = p_Row->GetField(dateColumn).GetValueTimeDate();
						if (!lastActivityDate || *lastActivityDate < date)
						{
							lastActivityDate = date;
							const auto pos = Str::find(columnName, _YTEXT(" Last Activity Date"));
							ASSERT(pos != std::wstring::npos);
							lastActivityService = columnName.substr(0, pos);
						}
					}
				}
			}

			ASSERT(lastActivityDate.is_initialized() == lastActivityService.is_initialized());
			if (lastActivityDate)
			{
				{
					auto& columnConfig = getColumn(_YTEXT("LASTACTIVITYDATE"));
					ASSERT(columnConfig.IsValid());
					if (columnConfig.IsValid())
					{
						auto col = gridBackend->GetColumnByUniqueID(columnConfig.m_UID);
						p_Row->AddField(lastActivityDate, col);
					}
				}
				{
					auto& columnConfig = getColumn(_YTEXT("LASTACTIVITYSERVICE"));
					ASSERT(columnConfig.IsValid());
					if (columnConfig.IsValid())
					{
						auto col = gridBackend->GetColumnByUniqueID(columnConfig.m_UID);
						p_Row->AddField(lastActivityService, col);
					}
				}
			}			
		});
}

O365ReportConfigImpl<O365ReportID::Office365ActivationsUserDetail>::O365ReportConfigImpl()
{
	// FIXME: Make it hierarchical
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));

	AddColumn({ _YTEXT("Display Name"), _T("Display Name"), _YUID("displayName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR });

	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Product Type"), _T("Product Type"), _YUID("productType"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("Last Activated Date"), _T("Last Activated Date"), _YUID("lastActivatedDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Windows"), _T("Windows"), _YUID("windows"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Mac"), _T("Mac"), _YUID("mac"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Windows 10 Mobile"), _T("Windows 10 Mobile"), _YUID("windows10Mobile"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("iOS"), _T("iOS"), _YUID("iOS"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Android"), _T("Android"), _YUID("android"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Activated On Shared Computer"), _T("Activated On Shared Computer"), _YUID("activatedOnSharedComputer"), Column::BOOL, CacheGrid::g_ColumnSize3 });

	// Hidden by default
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED });
}

O365ReportConfigImpl<O365ReportID::SharePointActivityUserDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));

	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Deleted Date"), _T("Deleted Date"), _YUID("deletedDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Viewed Or Edited File Count"), _T("Viewed Or Edited File Count"), _YUID("viewedOrEditedFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Synced File Count"), _T("Synced File Count"), _YUID("syncedFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Shared Internally File Count"), _T("Shared Internally File Count"), _YUID("sharedInternallyFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Shared Externally File Count"), _T("Shared Externally File Count"), _YUID("sharedExternallyFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Visited Page Count"), _T("Visited Page Count"), _YUID("visitedPageCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Assigned Products"), _T("Assigned Products"), _YUID("assignedProducts"), Column::STRINGLIST, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::SharePointSiteUsageDetail>::O365ReportConfigImpl()
{
	commonInit();


	AddColumn({ _YTEXT("Site URL"), _T("Site URL"), _YUID("siteURL"), Column::URL, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("Site URL"));

	AddColumn({ _YTEXT("Site Id"), _T("Site Id"), _YUID("siteID"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR | O365ReportConfig::Column::FOR_PK }); //Make it Tech
	SetColumnForGraphId(_YTEXT("Site Id"));

	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("Site URL"), _YTEXT(O365_SITE_WEBURL) } , { _YTEXT("Site Id"), _YTEXT(O365_SITE_SHAREPOINTIDSSITEID) } });

	AddColumn({ _YTEXT("Owner Display Name"), _T("Owner Display Name"), _YUID("ownerDisplayName"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("Owner Principal Name"), _T("Owner Principal Name"), _YUID("ownerPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("File Count"), _T("File Count"), _YUID("fileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Active File Count"), _T("Active File Count"), _YUID("activeFileCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Page View Count"), _T("Page View Count"), _YUID("pageViewCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Visited Page Count"), _T("Visited Page Count"), _YUID("visitedPageCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Storage Used (Byte)"), _T("Storage Used (Byte)"), _YUID("storageUsed"), Column::BYTES, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Storage Allocated (Byte)"), _T("Storage Allocated (Byte)"), _YUID("storageAllocated"), Column::BYTES, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Root Web Template"), _T("Root Web Template"), _YUID("rootWebTemplate"), Column::STRING, CacheGrid::g_ColumnSize22 });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::SkypeForBusinessActivityUserDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));

	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Deleted Date"), _T("Deleted Date"), _YUID("deletedDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Total Peer-to-peer Session Count"), _T("Total Peer-to-peer Session Count"), _YUID("totalPeerToPeerSessionCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Total Organized Conference Count"), _T("Total Organized Conference Count"), _YUID("totalOrganizedConferenceCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Total Participated Conference Count"), _T("Total Participated Conference Count"), _YUID("totalParticipatedConferenceCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Peer-to-peer Last Activity Date"), _T("Peer-to-peer Last Activity Date"), _YUID("peerToPeerLastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Organized Conference Last Activity Date"), _T("Organized Conference Last Activity Date"), _YUID("organizedConferenceLastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Participated Conference Last Activity Date"), _T("Participated Conference Last Activity Date"), _YUID("participatedConferenceLastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Peer-to-peer IM Count"), _T("Peer-to-peer IM Count"), _YUID("peerToPeerIMCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Peer-to-peer Audio Count"), _T("Peer-to-peer Audio Count"), _YUID("peerToPeerAudioCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Peer-to-peer Audio Minutes"), _T("Peer-to-peer Audio Minutes"), _YUID("peerToPeerAudioMinutes"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Peer-to-peer Video Count"), _T("Peer-to-peer Video Count"), _YUID("peerToPeerVideoCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Peer-to-peer Video Minutes"), _T("Peer-to-peer Video Minutes"), _YUID("peerToPeerVideoMinutes"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Peer-to-peer App Sharing Count"), _T("Peer-to-peer App Sharing Count"), _YUID("peerToPeerAppSharingCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Peer-to-peer File Transfer Count"), _T("Peer-to-peer File Transfer Count"), _YUID("peerToPeerFileTransferCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Organized Conference IM Count"), _T("Organized Conference IM Count"), _YUID("organizedConferenceIMCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Organized Conference Audio/Video Count"), _T("Organized Conference Audio/Video Count"), _YUID("organizedConferenceAudioVideoCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Organized Conference Audio/Video Minutes"), _T("Organized Conference Audio/Video Minutes"), _YUID("organizedConferenceAudioVideoMinutes"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Organized Conference App Sharing Count"), _T("Organized Conference App Sharing Count"), _YUID("organizedConferenceAppSharingCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Organized Conference Web Count"), _T("Organized Conference Web Count"), _YUID("organizedConferenceWebCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Organized Conference Dial-in/out 3rd Party Count"), _T("Organized Conference Dial-in/out 3rd Party Count"), _YUID("organizedConferenceDialInOut3rdPartyCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Organized Conference Dial-in/out Microsoft Count"), _T("Organized Conference Dial-in/out Microsoft Count"), _YUID("organizedConferenceDialInOutMicrosoftCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Organized Conference Dial-in Microsoft Minutes"), _T("Organized Conference Dial-in Microsoft Minutes"), _YUID("organizedConferenceDialInMicrosoftMinutes"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Organized Conference Dial-out Microsoft Minutes"), _T("Organized Conference Dial-out Microsoft Minutes"), _YUID("organizedConferenceDialOutMicrosoftMinutes"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Participated Conference IM Count"), _T("Participated Conference IM Count"), _YUID("participatedConferenceIMCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Participated Conference Audio/Video Count"), _T("Participated Conference Audio/Video Count"), _YUID("participatedConferenceAudioVideoCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Participated Conference Audio/Video Minutes"), _T("Participated Conference Audio/Video Minutes"), _YUID("participatedConferenceAudioVideoMinutes"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Participated Conference App Sharing Count"), _T("Participated Conference App Sharing Count"), _YUID("participatedConferenceAppSharingCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Participated Conference Web Count"), _T("Participated Conference Web Count"), _YUID("participatedConferenceWebCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Participated Conference Dial-in/out 3rd Party Count"), _T("Participated Conference Dial-in/out 3rd Party Count"), _YUID("participatedConferenceDialInOut3rdPartyCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Assigned Products"), _T("Assigned Products"), _YUID("assignedProducts"), Column::STRINGLIST, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::SkypeForBusinessDeviceUsageUserDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));

	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Used Windows"), _T("Used Windows"), _YUID("usedWindows"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used Windows Phone"), _T("Used Windows Phone"), _YUID("usedWindowsPhone"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used Android Phone"), _T("Used Android Phone"), _YUID("usedAndroidPhone"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used iPhone"), _T("Used iPhone"), _YUID("usediPhone"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used iPad"), _T("Used iPad"), _YUID("usediPad"), Column::BOOL, CacheGrid::g_ColumnSize3 });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::YammerActivityUserDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));

	AddColumn({ _YTEXT("Display Name"), _T("Display Name"), _YUID("displayName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR });

	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("User State"), _T("User State"), _YUID("userState"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("State Change Date"), _T("State Change Date"), _YUID("stateChangeDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Posted Count"), _T("Posted Count"), _YUID("postedCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Read Count"), _T("Read Count"), _YUID("readCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Liked Count"), _T("Liked Count"), _YUID("likedCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Assigned Products"), _T("Assigned Products"), _YUID("assignedProducts"), Column::STRINGLIST, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::YammerDeviceUsageUserDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	AddColumn({ _YTEXT("User Principal Name"), _T("User Principal Name"), _YUID("userPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("User Principal Name"));

	AddColumn({ _YTEXT("Display Name"), _T("Display Name"), _YUID("displayName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR });

	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("User Principal Name"), _YTEXT(O365_USER_USERPRINCIPALNAME) } });

	AddColumn({ _YTEXT("User State"), _T("User State"), _YUID("userState"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("State Change Date"), _T("State Change Date"), _YUID("stateChangeDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Used Web"), _T("Used Web"), _YUID("usedWeb"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used Windows Phone"), _T("Used Windows Phone"), _YUID("usedWindowsPhone"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used Android Phone"), _T("Used Android Phone"), _YUID("usedAndroidPhone"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used iPhone"), _T("Used iPhone"), _YUID("usediPhone"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used iPad"), _T("Used iPad"), _YUID("usediPad"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Used Others"), _T("Used Others"), _YUID("usedOthers"), Column::BOOL, CacheGrid::g_ColumnSize3 });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::SHARED | O365ReportConfig::Column::INSERT_AT_BACK });
}

O365ReportConfigImpl<O365ReportID::YammerGroupsActivityDetail>::O365ReportConfigImpl()
{
	commonInit();

	//SetColumnForGraphId(_YUID(""));

	// FIXME: Group Display Name may not be unique :-o We use it as PK otherwise Comments are broken, but it might fail if yammer allows several groups with the same name!
	AddColumn({ _YTEXT("Group Display Name"), _T("Group Display Name"), _YUID("groupDisplayName"), Column::STRING, CacheGrid::g_ColumnSize22, O365ReportConfig::Column::ADDITIONAL_PROPS_ANCHOR | O365ReportConfig::Column::FOR_PK | O365ReportConfig::Column::FOR_COMMENT_REF });
	SetColumnForAnonymousDataChecking(_YTEXT("Group Display Name"));

	// FIXME: Group Display Name is not unique :-o
	SetColumnsForAdditionalMetadataMatching({ { _YTEXT("Group Display Name"), _YTEXT(O365_GROUP_DISPLAYNAME) } });
	SetConditionForAdditionalMetadataMatching([](BusinessObject* obj)
		{
			return nullptr != obj && static_cast<BusinessGroup*>(obj)->GetIsYammer() && *static_cast<BusinessGroup*>(obj)->GetIsYammer();
		});

	AddColumn({ _YTEXT("Is Deleted"), _T("Is Deleted"), _YUID("isDeleted"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Owner Principal Name"), _T("Owner Principal Name"), _YUID("ownerPrincipalName"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("Last Activity Date"), _T("Last Activity Date"), _YUID("lastActivityDate"), Column::DATE, CacheGrid::g_ColumnSize16 });
	AddColumn({ _YTEXT("Group Type"), _T("Group Type"), _YUID("groupType"), Column::STRING, CacheGrid::g_ColumnSize22 });
	AddColumn({ _YTEXT("Office 365 Connected"), _T("Office 365 Connected"), _YUID("office365Connected"), Column::BOOL, CacheGrid::g_ColumnSize3 });
	AddColumn({ _YTEXT("Member Count"), _T("Member Count"), _YUID("memberCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Posted Count"), _T("Posted Count"), _YUID("postedCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Read Count"), _T("Read Count"), _YUID("readCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });
	AddColumn({ _YTEXT("Liked Count"), _T("Liked Count"), _YUID("likedCount"), Column::INTEGER, CacheGrid::g_ColumnSize8 });

	// Hidden by default
	AddColumn({ _YTEXT("Report Period"), _T("Report Period"), _YUID("reportPeriod"), Column::INTEGER, CacheGrid::g_ColumnSize8, O365ReportConfig::Column::INSERT_AT_BACK });
	AddColumn({ _YTEXT("Report Refresh Date"), _T("Report Refresh Date"), _YUID("reportRefreshDate"), Column::DATE, CacheGrid::g_ColumnSize16, O365ReportConfig::Column::INSERT_AT_BACK });
}
