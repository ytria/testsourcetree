#include "FrameO365Reports.h"

FrameO365Reports::FrameO365Reports(std::vector<O365ReportID> p_ReportIDs, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
	, m_ReportDateLabel(nullptr)
	, m_GridReport(p_ReportIDs)
{
	m_CreateRefresh = false;
}

void FrameO365Reports::LoadReport(const std::vector<GridO365Report::Report>& p_Reports)
{
	m_GridReport.LoadReports(p_Reports);
}

void FrameO365Reports::ShowLoadingError(const SapioError& p_Error)
{
	m_GridReport.ShowLoadingError(p_Error);
}

AutomatedApp::AUTOMATIONSTATUS FrameO365Reports::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	return statusRV;
}

bool FrameO365Reports::customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(p_Tab);

	auto reportInfoGroup = p_Tab.AddGroup(_T("Report Info"));

	auto nameCtrl = reportInfoGroup->Add(xtpControlLabel, ID_REPORT_NAME);
	//auto descriptionCtrl = reportInfoGroup->Add(xtpControlLabel, ID_REPORT_DESCRIPTION);
	ASSERT(nullptr == m_ReportDateLabel);
	m_ReportDateLabel = reportInfoGroup->Add(xtpControlLabel, ID_REPORT_DATE);
	auto periodCtrl = reportInfoGroup->Add(xtpControlLabel, ID_REPORT_PERIOD);

	ASSERT(GetModuleCriteria().m_UsedContainer == ModuleCriteria::UsedContainer::O365REPORTCRITERIA);
	if (GetModuleCriteria().m_UsedContainer == ModuleCriteria::UsedContainer::O365REPORTCRITERIA)
	{
		ASSERT(!GetModuleCriteria().m_ReportCriteria.m_O365ReportIDs.empty());
		if (GetModuleCriteria().m_ReportCriteria.m_O365ReportIDs.size() == 1)
			nameCtrl->SetCaption(_YFORMAT(L"%s", GetModuleCriteria().m_ReportCriteria.GetCombinedDisplayName().c_str()).c_str());
		else
			nameCtrl->SetCaption(_T("Multiple Usage Reports"));
		nameCtrl->SetTooltip(GetModuleCriteria().m_ReportCriteria.GetCombinedDisplayName(_YTEXT("\n")).c_str());
		//descriptionCtrl->SetCaption(_YFORMATCOM(L"(%s)", GetModuleCriteria().m_ReportCriteria.GetReportDescription(0).c_str()).c_str());
		periodCtrl->SetCaption(GetModuleCriteria().m_ReportCriteria.GetPeriodForDisplay(true).c_str());
		updateDateDisplay();
		// FIXME: finish this (sites)!
		CreateLinkGroups(p_Tab, GetModuleCriteria().m_ReportCriteria.GetOrigin() == Origin::User, GetModuleCriteria().m_ReportCriteria.GetOrigin() == Origin::Group/*, GetModuleCriteria().m_ReportCriteria.GetOrigin() == Origin::Site*/);
	}

	return true;
}

void FrameO365Reports::RefreshSpecific(AutomationAction* p_CurrentAction)
{
#if O365_REPORTS_ALLOW_GRID_REFRESH
	CommandInfo info;
	info.SetOrigin(Origin::Tenant);
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	ASSERT(GetModuleCriteria().m_UsedContainer == ModuleCriteria::UsedContainer::O365REPORTCRITERIA);
	info.Data() = GetModuleCriteria().m_ReportCriteria;

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Reports, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
#else
	ASSERT(false);
#endif
}

O365Grid& FrameO365Reports::GetGrid()
{
	return m_GridReport;
}

void FrameO365Reports::createGrid()
{
	BOOL gridCreated = m_GridReport.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
	ASSERT(gridCreated);
}

void FrameO365Reports::UpdateReportDate(const boost::YOpt<YTimeDate>& p_ReportDate)
{
	ASSERT(GetModuleCriteria().m_UsedContainer == ModuleCriteria::UsedContainer::O365REPORTCRITERIA);
	GetModuleCriteria().m_ReportCriteria.m_CreationDate = p_ReportDate;
	updateDateDisplay();
	if (p_ReportDate)
		SetLastRefreshDate(*p_ReportDate);
}

void FrameO365Reports::updateDateDisplay()
{
	ASSERT(nullptr != m_ReportDateLabel);
	if (nullptr != m_ReportDateLabel)
		m_ReportDateLabel->SetCaption(_YFORMAT(L"Generated on %s", GetModuleCriteria().m_ReportCriteria.GetCreationDateDisplay().c_str()).c_str());
}