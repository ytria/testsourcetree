#pragma once

#include "CommandInfo.h"

enum class O365ReportID : int32_t
{
	Undefined = -1,

	OneDriveActivityUserDetail,
	OneDriveUsageAccountDetail,
	TeamsDeviceUsageUserDetail,
	TeamsUserActivityUserDetail,
	EmailActivityUserDetail,
	EmailAppUsageUserDetail,
	MailboxUsageDetail,
	Office365GroupsActivityDetail,
	Office365ActiveUserDetail,
	Office365ActivationsUserDetail,
	SharePointActivityUserDetail,
	SharePointSiteUsageDetail,
	SkypeForBusinessActivityUserDetail,
	SkypeForBusinessDeviceUsageUserDetail,
	YammerActivityUserDetail,
	YammerDeviceUsageUserDetail,
	YammerGroupsActivityDetail,

	Count
};

template <O365ReportID reportID>
struct O365Report
{
	static const wstring g_AsString;
	static const Origin g_Origin;
	static constexpr const wstring GetAPICall()
	{
		return _YTEXT("get") + g_AsString;
	}
	static inline wstring	GetDisplayName();
	static inline wstring	GetDescription();
	static inline wstring	GetShortUniqueName();
};

#include "O365ReportID.hpp"
