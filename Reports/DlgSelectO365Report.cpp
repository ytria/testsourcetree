#include "DlgSelectO365Report.h"

#include "BusinessGroupConfiguration.h"
#include "BusinessUserConfiguration.h"
#include "FileUtil.h"
#include "ModuleO365Reports.h"
#include "YtriaFieldsConstants.h"

DlgSelectO365Report::DlgSelectO365Report(std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent, g_ActionNameSelectReport)
	, m_Session(p_Session)
	, m_LoadExisting(false)
	, m_ShouldSaveNewReport(false)
	, m_CurrentMotherAction(nullptr)
	, m_CurrentSetParamTotalCount(0)
{

}

bool DlgSelectO365Report::IsCreateNew() const
{
	return !m_LoadExisting;
}

const O365ReportCriteria& DlgSelectO365Report::GetReportConfig() const
{
	return m_ReportConfig;
}

bool DlgSelectO365Report::IsSaveReport() const
{
	return m_ShouldSaveNewReport;
}

const std::vector<wstring>& DlgSelectO365Report::GetReportFiles() const
{
	return m_FileNames;
}

namespace
{
	static const wstring g_UseExistingReport = _YTEXT("useExistingReport");
	static const wstring g_GlobalDescription = _YTEXT("globalDescription");
	static const wstring g_ReportID = _YTEXT("reportID");
	static const wstring g_Date = _YTEXT("date");
	static const wstring g_SaveReport = _YTEXT("saveReport");
	static const wstring g_PeriodOrDate = _YTEXT("periodOrDate");
	static const wstring g_PeriodOnly = _YTEXT("periodOnly");
	static const wstring g_PeriodIsDate = _YTEXT("periodIsDate");
	static const wstring g_NoPeriod = _YTEXT("noPeriod");
	static const wstring g_TargetFileName = _YTEXT("targetFileName");
	static const wstring g_AddColumnIntro = _YTEXT("addColumnIntro");
	static const wstring g_AddColumnUser = _YTEXT("addColumnUser");
	static const wstring g_AddColumnGroup = _YTEXT("addColumnGroup");
	static const wstring g_AddColumnSite = _YTEXT("addColumnSite");
	static const wstring g_WarningCacheUser = _YTEXT("warningCacheUser");
	static const wstring g_WarningCacheGroup = _YTEXT("warningCacheGroup");

	static const wstring g_ReportFile = _YTEXT("reportFile");
	static const wstring g_NoFile = _YTEXT("noFile");

	static const wstring g_Description_OneDriveActivityUserDetail				= _YTEXT("d_OneDriveActivityUserDetail");
	static const wstring g_Description_OneDriveUsageAccountDetail				= _YTEXT("d_OneDriveUsageAccountDetail");
	static const wstring g_Description_TeamsDeviceUsageUserDetail				= _YTEXT("d_TeamsDeviceUsageUserDetail");
	static const wstring g_Description_TeamsUserActivityUserDetail				= _YTEXT("d_TeamsUserActivityUserDetail");
	static const wstring g_Description_EmailActivityUserDetail					= _YTEXT("d_EmailActivityUserDetail");
	static const wstring g_Description_EmailAppUsageUserDetail					= _YTEXT("d_EmailAppUsageUserDetail");
	static const wstring g_Description_MailboxUsageDetail						= _YTEXT("d_MailboxUsageDetail");
	static const wstring g_Description_Office365GroupsActivityDetail			= _YTEXT("d_Office365GroupsActivityDetail");
	static const wstring g_Description_Office365ActiveUserDetail				= _YTEXT("d_Office365ActiveUserDetail");
	static const wstring g_Description_Office365ActivationsUserDetail			= _YTEXT("d_Office365ActivationsUserDetail");
	static const wstring g_Description_SharePointActivityUserDetail				= _YTEXT("d_SharePointActivityUserDetail");
	static const wstring g_Description_SharePointSiteUsageDetail				= _YTEXT("d_SharePointSiteUsageDetail");
	static const wstring g_Description_SkypeForBusinessActivityUserDetail		= _YTEXT("d_SkypeForBusinessActivityUserDetail");
	static const wstring g_Description_SkypeForBusinessDeviceUsageUserDetail	= _YTEXT("d_SkypeForBusinessDeviceUsageUserDetail");
	static const wstring g_Description_YammerActivityUserDetail					= _YTEXT("d_YammerActivityUserDetail");
	static const wstring g_Description_YammerDeviceUsageUserDetail				= _YTEXT("d_YammerDeviceUsageUserDetail");
	static const wstring g_Description_YammerGroupsActivityDetail				= _YTEXT("d_YammerGroupsActivityDetail");
	static const wstring g_Description_MultiSelect								= _YTEXT("d_MultiSelect");

	void ListFiles(const wstring& p_Path, vector<wstring>& files)
	{
		CFileFind finder;
		if (finder.FindFile(p_Path.c_str()))
		{
			finder.FindNextFile();

			if (finder.IsDirectory())
			{
				CString strWildcard = finder.GetFilePath() + _YTEXT("\\*.") + ModuleO365Reports::g_ReportFileExtension.c_str();
				auto exploreMe = finder.FindFile(strWildcard);
				while (exploreMe)
				{
					exploreMe = finder.FindNextFile();
					if (!finder.IsDots())
						ListFiles(finder.GetFilePath().GetBuffer(), files);
				}
			}
			else
			{
				const wstring filePath = finder.GetFilePath();
				files.push_back(FileUtil::FileGetFileName(filePath));
			}
		}
		finder.Close();
	}
}

void DlgSelectO365Report::generateJSONScriptData()
{
	initMain(_YTEXT("DlgSelectO365Report"));

	const bool cacheWarningUsers = !m_Session->GetGraphCache().IsSqlUsersCacheFull();
	const bool cacheWarningGroups = !m_Session->GetGraphCache().IsSqlGroupsCacheFull();

	vector<wstring> existingReports;
	ListFiles(ModuleO365Reports::GetReportsFolder(m_Session), existingReports);

	// Handle multi reports
	{
		wstring currentSeries;
		for (const auto& file : existingReports)
		{
			if (!currentSeries.empty() && Str::beginsWith(file, currentSeries + _YTEXT("."))) // First of series
			{
				m_ExistingReportSeries[currentSeries].push_back(FileUtil::FileRemoveExtension(file));
			}
			else if (Str::endsWith(file, _YTEXT(".0.ysv"))) // First of series
			{
				currentSeries = file.substr(0, file.size() - 6);
				m_ExistingReportSeries[currentSeries].push_back(FileUtil::FileRemoveExtension(file));
			}
			else
			{
				currentSeries.clear();
				m_ExistingReportSeries[file.substr(0, file.size() - 4)].push_back(FileUtil::FileRemoveExtension(file));
			}
		}

		existingReports.clear();
		for (const auto& series : m_ExistingReportSeries)
			existingReports.push_back(series.first);
	}

	const uint32_t editFlags = EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED;

	{
		getGenerator().addCategory(YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_8, _YLOC("Open an existing report or select a new one below:")).c_str(), CategoryFlags::EXPAND_BY_DEFAULT | CategoryFlags::NO_COLLAPSE);

		{
			const wstring userFamily = _YTEXT("user");
			FamilyCheckListEditor::LabelsValuesAndFamilies labelAndvalues
			{
				{ O365Report<O365ReportID::OneDriveActivityUserDetail>::GetDisplayName()			, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveActivityUserDetail), 2)				, userFamily },
				{ O365Report<O365ReportID::OneDriveUsageAccountDetail>::GetDisplayName()			, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveUsageAccountDetail), 2)				, userFamily },
				{ O365Report<O365ReportID::TeamsDeviceUsageUserDetail>::GetDisplayName()			, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsDeviceUsageUserDetail), 2)				, userFamily },
				{ O365Report<O365ReportID::TeamsUserActivityUserDetail>::GetDisplayName()			, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsUserActivityUserDetail), 2)				, userFamily },
				{ O365Report<O365ReportID::EmailActivityUserDetail>::GetDisplayName()				, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailActivityUserDetail), 2)					, userFamily },
				{ O365Report<O365ReportID::EmailAppUsageUserDetail>::GetDisplayName()				, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailAppUsageUserDetail), 2)					, userFamily },
				{ O365Report<O365ReportID::MailboxUsageDetail>::GetDisplayName()					, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::MailboxUsageDetail), 2)						, userFamily },
				{ O365Report<O365ReportID::Office365GroupsActivityDetail>::GetDisplayName()			, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365GroupsActivityDetail), 2)			, _YTEXT("group") },
				{ O365Report<O365ReportID::Office365ActiveUserDetail>::GetDisplayName()				, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActiveUserDetail), 2)				, userFamily },
				{ O365Report<O365ReportID::Office365ActivationsUserDetail>::GetDisplayName()		, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActivationsUserDetail), 2)			, userFamily },
				{ O365Report<O365ReportID::SharePointActivityUserDetail>::GetDisplayName()			, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointActivityUserDetail), 2)				, userFamily },
				{ O365Report<O365ReportID::SharePointSiteUsageDetail>::GetDisplayName()				, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointSiteUsageDetail), 2)				, _YTEXT("site") },
				{ O365Report<O365ReportID::SkypeForBusinessActivityUserDetail>::GetDisplayName()	, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessActivityUserDetail), 2)		, userFamily },
				{ O365Report<O365ReportID::SkypeForBusinessDeviceUsageUserDetail>::GetDisplayName()	, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessDeviceUsageUserDetail), 2)	, userFamily },
				{ O365Report<O365ReportID::YammerActivityUserDetail>::GetDisplayName()				, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerActivityUserDetail), 2)					, userFamily },
				{ O365Report<O365ReportID::YammerDeviceUsageUserDetail>::GetDisplayName()			, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerDeviceUsageUserDetail), 2)				, userFamily },
				{ O365Report<O365ReportID::YammerGroupsActivityDetail>::GetDisplayName()			, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerGroupsActivityDetail), 2)				, _YTEXT("yammer") },
			};

			if (!existingReports.empty())
				labelAndvalues.insert(labelAndvalues.begin(), { _T("** EXISTING REPORT **"), g_UseExistingReport, _YTEXT("exist") });

			const wstring& defaultValue = Str::g_EmptyString;
			//const wstring& defaultValue = std::get<1>(labelAndvalues.front()); // Select first by default?

			getGenerator().Add(Expanded<WithTooltip<WithSubLabel<FamilyCheckListEditor>>>(
				{
					{
						{ g_ReportID
							, wstring(YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_9, _YLOC("Usage Report")).c_str()) + _YTEXT(" *")
							, editFlags
							, { defaultValue }
						}
						, labelAndvalues
					}
					, _T("Note: The data in each report is from up to, but not including, the last 24 to 48 hours.")
				}
				, YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_10, _YLOC("Open an existing report, or select a new one from the list of report types.")).c_str()
				));

			getGenerator().revertLastHbsHeightIncrease(); // Because we'll specify it manually below.
			if (!existingReports.empty())
			{
				ASSERT(labelAndvalues.size() == 18); // Adapt below size if number of reports changes.
				getGenerator().increaseTotalHbsHeight(338);
			}
			else
			{
				ASSERT(labelAndvalues.size() == 17); // Adapt below size if number of reports changes.
				getGenerator().increaseTotalHbsHeight(320);
			}
		}

		const wstring desciptionFieldLabel = YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_11, _YLOC("Report Description")).c_str();
		getGenerator().addIconTextField(g_Description_OneDriveActivityUserDetail			, O365Report<O365ReportID::OneDriveActivityUserDetail>::GetDescription()			, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().addIconTextField(g_Description_OneDriveUsageAccountDetail			, O365Report<O365ReportID::OneDriveUsageAccountDetail>::GetDescription()			, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_TeamsDeviceUsageUserDetail			, O365Report<O365ReportID::TeamsDeviceUsageUserDetail>::GetDescription()			, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_TeamsUserActivityUserDetail			, O365Report<O365ReportID::TeamsUserActivityUserDetail>::GetDescription()			, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_EmailActivityUserDetail				, O365Report<O365ReportID::EmailActivityUserDetail>::GetDescription()				, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_EmailAppUsageUserDetail				, O365Report<O365ReportID::EmailAppUsageUserDetail>::GetDescription()				, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_MailboxUsageDetail					, O365Report<O365ReportID::MailboxUsageDetail>::GetDescription()					, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_Office365GroupsActivityDetail		, O365Report<O365ReportID::Office365GroupsActivityDetail>::GetDescription()			, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_Office365ActiveUserDetail			, O365Report<O365ReportID::Office365ActiveUserDetail>::GetDescription()				, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_Office365ActivationsUserDetail		, O365Report<O365ReportID::Office365ActivationsUserDetail>::GetDescription()		, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_SharePointActivityUserDetail			, O365Report<O365ReportID::SharePointActivityUserDetail>::GetDescription()			, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_SharePointSiteUsageDetail			, O365Report<O365ReportID::SharePointSiteUsageDetail>::GetDescription()				, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_SkypeForBusinessActivityUserDetail	, O365Report<O365ReportID::SkypeForBusinessActivityUserDetail>::GetDescription()	, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_SkypeForBusinessDeviceUsageUserDetail, O365Report<O365ReportID::SkypeForBusinessDeviceUsageUserDetail>::GetDescription()	, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_YammerActivityUserDetail				, O365Report<O365ReportID::YammerActivityUserDetail>::GetDescription()				, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_YammerDeviceUsageUserDetail			, O365Report<O365ReportID::YammerDeviceUsageUserDetail>::GetDescription()			, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_YammerGroupsActivityDetail			, O365Report<O365ReportID::YammerGroupsActivityDetail>::GetDescription()			, desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default
		getGenerator().addIconTextField(g_Description_MultiSelect, _T("-multiple options selected-"), desciptionFieldLabel, _YTEXT("fas fa-info-circle"), GridBackendUtil::g_NoColor);
		getGenerator().revertLastHbsHeightIncrease(); // Because all hidden except one by default

		{
			// Same text for both, only one of them is displayed at a time.

			const wstring targetPeriodLabel = wstring(YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_12, _YLOC("Target Period")).c_str()) + _YTEXT(" *");
			CheckListEditor::LabelsAndValues labelAndvalues
			{
				{ YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_14, _YLOC("Last 7 days")).c_str(), _YTEXT("D7") },
				{ YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_15, _YLOC("Last 30 days")).c_str(), _YTEXT("D30") },
				{ YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_16, _YLOC("Last 90 days")).c_str(), _YTEXT("D90") },
				{ YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_17, _YLOC("Last 180 days")).c_str(), _YTEXT("D180") },
				{ YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_18, _YLOC("Date from past 30 days")).c_str(), g_PeriodIsDate },
			};

			getGenerator().Add(Expanded<AsRadio<WithTooltip<CheckListEditor>>>({ { g_PeriodOrDate, targetPeriodLabel, editFlags, { _YTEXT("D7") } }, labelAndvalues }, YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_19, _YLOC("Define the period of time to be reported on.")).c_str()));
			getGenerator().revertLastHbsHeightIncrease(); // Because we'll specify it manually below.
			ASSERT(labelAndvalues.size() == 5); // Adapt below size if number of periods changes.
			getGenerator().increaseTotalHbsHeight(110);

			labelAndvalues.pop_back();
			getGenerator().Add(Expanded<AsRadio<WithTooltip<CheckListEditor>>>({ { g_PeriodOnly, targetPeriodLabel, editFlags, { _YTEXT("D7") } }, labelAndvalues }, YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_19, _YLOC("Define the period of time to be reported on.")).c_str()));
			getGenerator().revertLastHbsHeightIncrease(); // Because hidden by default

			getGenerator().addIconTextField(g_NoPeriod, YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_21, _YLOC("No period selection for this report.")).c_str(), targetPeriodLabel, _YTEXT("fas fa-times-circle"), GridBackendUtil::g_NoColor);
			getGenerator().revertLastHbsHeightIncrease(); // Because hidden by default
		}

		getGenerator().Add(WithTooltip<DateEditor>({ { g_Date, YtriaTranslate::Do(GridSignIns_customizeGrid_11, _YLOC("Date")).c_str(), editFlags, Str::g_EmptyString }, Str::g_EmptyString }, YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_23, _YLOC("Input a date in the last 30 days")).c_str()));
		getGenerator().revertLastHbsHeightIncrease(); // Because hidden by default

		getGenerator().Add(WithTooltip<BoolToggleEditor>({ { { g_SaveReport, YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_24, _YLOC("Save Report?")).c_str(), editFlags, false }, { YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str(), YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str() } } }, YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_25, _YLOC("If you wish to save the report once it is generate, select 'Yes'. You will be prompted to name your file.")).c_str()));
		getGenerator().Add(WithTooltip<StringEditor>({ g_TargetFileName, YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_28, _YLOC("Name of the report")).c_str(), editFlags, _YTEXT("") }, YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_29, _YLOC("Create a name for your report to be saved as a file.")).c_str()));
		getGenerator().revertLastHbsHeightIncrease(); // Because hidden by default


		// Additional columns
		{
			getGenerator().addCategory(_T("Include additional info:"), CategoryFlags::EXPAND_BY_DEFAULT | CategoryFlags::NO_COLLAPSE);

			if (cacheWarningUsers)
				getGenerator().addIconTextField(g_WarningCacheUser, _T("This additional information is added from the local cache. You must launch Users at least once to generate a cache."), _YTEXT("fas fa-exclamation-triangle"));
			
			if (cacheWarningGroups)
				getGenerator().addIconTextField(g_WarningCacheGroup, _T("This additional information is added from the local cache. You must launch Groups at least once to generate a cache."), _YTEXT("fas fa-exclamation-triangle"));

			// Users
			{
				CheckListEditor::LabelsAndValues labelAndvalues;
				auto& ucfg = BusinessUserConfiguration::GetInstance();
				const vector<wstring> props
				{
					_YTEXT(O365_USER_DISPLAYNAME),
					_YTEXT(O365_USER_USERPRINCIPALNAME),
					_YTEXT(O365_USER_PASSWORDPOLICIES),
					_YTEXT(O365_USER_PASSWORDPROFILE_FORCECHANGE),
					_YTEXT(O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA),
					_YTEXT(O365_USER_USERTYPE),
					_YTEXT("identities.signInType"),
					_YTEXT("identities.issuer"),
					_YTEXT("identities.issuerAssignedId"),
					_YTEXT(O365_USER_ACCOUNTENABLED),
					_YTEXT(O365_USER_ISRESOURCEACCOUNT),
					_YTEXT(O365_USER_ASSIGNEDLICENSES),
					_YTEXT("ASSIGNEDBYGROUPNAME"),
					_YTEXT("ASSIGNEDBYGROUPEMAIL"),
					_YTEXT("ASSIGNEDSKUPARTNUMBER"),
					_YTEXT("assignedLicenses.skuId"),
					_YTEXT("licenseAssignmentStates.skuId"),
					_YTEXT("licenseAssignmentStates.assignedByGroup"),
					_YTEXT("licenseAssignmentStates.state"),
					_YTEXT("licenseAssignmentStates.error"),
					_YTEXT(O365_USER_MAIL),
					_YTEXT(O365_USER_MAILNICKNAME),
					_YTEXT(O365_USER_PROXYADDRESSES),
					_YTEXT(O365_USER_IMADDRESSES),
					_YTEXT(O365_USER_OTHERMAILS),
					_YTEXT(O365_USER_GIVENNAME),
					_YTEXT(O365_USER_SURNAME),
					_YTEXT(O365_USER_EMPLOYEEID),
					_YTEXT(O365_USER_JOBTITLE),
					_YTEXT(O365_USER_COMPANYNAME),
					_YTEXT(O365_USER_DEPARTMENT),
					_YTEXT(O365_USER_BUSINESSPHONES),
					_YTEXT(O365_USER_MOBILEPHONE),
					_YTEXT(O365_USER_FAXNUMBER),
					_YTEXT(O365_USER_OFFICELOCATION),
					_YTEXT(O365_USER_STREETADDRESS),
					_YTEXT(O365_USER_CITY),
					_YTEXT(O365_USER_STATE),
					_YTEXT(O365_USER_POSTALCODE),
					_YTEXT(O365_USER_COUNTRY),
					_YTEXT(O365_USER_PREFERREDLANGUAGE),
					_YTEXT(O365_USER_USAGELOCATION),
					_YTEXT(O365_USER_CREATEDDATETIME),
					_YTEXT(O365_USER_CREATIONTYPE),
					_YTEXT(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME),
					_YTEXT(O365_USER_REFRESTOKENVALIDFROMDATETIME),
					_YTEXT(O365_USER_SHOWINADDRESSLIST),
					_YTEXT(O365_USER_AGEGROUP),
					_YTEXT(O365_USER_CONSENTPROVIDEDFORMINOR),
					_YTEXT(O365_USER_LEGALAGEGROUPCLASSIFICATION),
					_YTEXT(O365_USER_ONPREMISESSYNCENABLED),
					_YTEXT(O365_USER_ONPREMISESDISTINGUISHEDNAME),
					_YTEXT(O365_USER_ONPREMISESIMMUTABLEID),
					_YTEXT(O365_USER_ONPREMISESLASTSYNCDATETIME),
					_YTEXT(O365_USER_ONPREMISESSECURITYIDENTIFIER),
					_YTEXT(O365_USER_ONPREMISESDOMAINNAME),
					_YTEXT(O365_USER_ONPREMISESSAMACCOUNTNAME),
					_YTEXT(O365_USER_ONPREMISESUSERPRINCIPALNAME),
					_YTEXT(O365_ONPREMISESPROVISIONINGERRORSCATEGORY),
					_YTEXT(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME),
					_YTEXT(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR),
					_YTEXT(O365_ONPREMISESPROVISIONINGERRORSVALUE),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute1"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute2"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute3"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute4"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute5"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute6"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute7"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute8"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute9"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute10"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute11"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute12"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute13"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute14"),
					_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute15"),
				};
				for (const auto& prop : props)
					labelAndvalues.emplace_back(ucfg.GetTitle(prop), prop);

				getGenerator().Add(Expanded<WithTooltip<CheckListEditor>>(
					{
						{
							g_AddColumnUser
							, _T("Additional Info")
							, EditorFlags::DIRECT_EDIT
							, { _YTEXT("") }
						}
						, labelAndvalues
					}
					, _T("Customise your report by including additional properties.")));
			}

			// Groups
			{
				CheckListEditor::LabelsAndValues labelAndvalues;
				auto& gcfg = BusinessGroupConfiguration::GetInstance();
				const vector<wstring> props
				{
					_YTEXT(O365_GROUP_ISTEAM),
					_YTEXT(O365_GROUP_DYNAMICMEMBERSHIP),
					_YTEXT(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE),
					_YTEXT(O365_GROUP_DESCRIPTION),
					_YTEXT(O365_GROUP_VISIBILITY),
					_YTEXT(O365_GROUP_CREATEDDATETIME),
					_YTEXT(O365_GROUP_MAIL),
					_YTEXT(O365_GROUP_CLASSIFICATION),
					_YTEXT(O365_GROUP_PROXYADDRESSES),
					_YTEXT(O365_GROUP_RESOURCEBEHAVIOROPTIONS),
					_YTEXT(O365_GROUP_RESOURCEPROVISIONINGOPTIONS),
					_YTEXT(O365_GROUP_PREFERREDDATALOCATION),
					_YTEXT(O365_GROUP_MEMBERSHIPRULE),
					_YTEXT(O365_GROUP_ONPREMISESLASTSYNCDATETIME),
					_YTEXT(O365_GROUP_ONPREMISESSYNCENABLED),
					_YTEXT(O365_ONPREMISESPROVISIONINGERRORSCATEGORY),
					_YTEXT(O365_GROUP_LICENSEPROCESSINGSTATE),
					_YTEXT(O365_GROUP_ASSIGNEDLICENSES),
					_YTEXT(O365_GROUP_GROUPTYPES),
					_YTEXT(O365_GROUP_SECURITYENABLED),
					_YTEXT(O365_GROUP_MAILENABLED),
					_YTEXT(O365_GROUP_SECURITYIDENTIFIER),
					_YTEXT(O365_GROUP_RENEWEDDATETIME),
				};
				for (const auto& prop : props)
					labelAndvalues.emplace_back(gcfg.GetTitle(prop), prop);

				getGenerator().Add(Expanded<WithTooltip<CheckListEditor>>(
					{
						{
							g_AddColumnGroup
							, _T("Additional Info")
							, EditorFlags::DIRECT_EDIT
							, { _YTEXT("") }
						}
						, labelAndvalues
					}
					, _T("Select the Information you want to add from the report:")));
			}

			// Sites
			/*{
				vector<std::tuple<wstring, wstring, bool>> labelAndvalues;
				auto& ucgf = BusinessSiteConfiguration::GetInstance();
				const auto& props = ucgf.GetAllProperties();
				for (const auto& prop : props)
					labelAndvalues.emplace_back(prop.second, prop.first, false);

				getGenerator().addCheckListEditor(g_AddColumnSite
					, _T("Additional Info")
					, _T("Select the Information you want to add from the report:")
					, EditorFlags::DIRECT_EDIT
					, labelAndvalues
					, false
					, false);
			}*/
		}

		getGenerator().addEvent(g_ReportID, { g_PeriodOrDate		, EventTarget::g_HideIfEmpty	, _YTEXT("") });
		getGenerator().addEvent(g_ReportID, { g_SaveReport			, EventTarget::g_HideIfEmpty	, _YTEXT("") });
		getGenerator().addEvent(g_ReportID, { g_AddColumnUser		, EventTarget::g_HideIfEmpty	, _YTEXT("") });
		getGenerator().addEvent(g_ReportID, { g_WarningCacheUser	, EventTarget::g_HideIfEmpty	, _YTEXT("") });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup		, EventTarget::g_HideIfEmpty	, _YTEXT("") });
		getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEmpty	, _YTEXT("") });
		getGenerator().addEvent(g_ReportID, { g_AddColumnIntro		, EventTarget::g_HideIfEmpty	, _YTEXT("") });
		getGenerator().addEvent(g_ReportID, { g_Date				, EventTarget::g_HideIfEmpty	, _YTEXT("") });
		
		getGenerator().addEvent(g_ReportID, { g_PeriodOrDate		, EventTarget::g_HideIfEqual	, g_UseExistingReport });
		getGenerator().addEvent(g_ReportID, { g_PeriodOnly			, EventTarget::g_HideIfEqual	, g_UseExistingReport });
		getGenerator().addEvent(g_ReportID, { g_Date				, EventTarget::g_HideIfEqual	, g_UseExistingReport });
		getGenerator().addEvent(g_ReportID, { g_NoPeriod			, EventTarget::g_HideIfEqual	, g_UseExistingReport });
		getGenerator().addEvent(g_ReportID, { g_SaveReport			, EventTarget::g_HideIfEqual	, g_UseExistingReport});
		getGenerator().addEvent(g_ReportID, { g_TargetFileName		, EventTarget::g_HideIfEqual	, g_UseExistingReport});
		getGenerator().addEvent(g_ReportID, { g_ReportFile			, EventTarget::g_HideIfNotEqual	, g_UseExistingReport });

		getGenerator().addEvent(g_ReportID, { g_Description_OneDriveActivityUserDetail				, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_OneDriveUsageAccountDetail				, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveUsageAccountDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_TeamsDeviceUsageUserDetail				, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsDeviceUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_TeamsUserActivityUserDetail			, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsUserActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_EmailActivityUserDetail				, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_EmailAppUsageUserDetail				, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailAppUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MailboxUsageDetail						, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::MailboxUsageDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_Office365GroupsActivityDetail			, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365GroupsActivityDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_Office365ActiveUserDetail				, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActiveUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_Office365ActivationsUserDetail			, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActivationsUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_SharePointActivityUserDetail			, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_SharePointSiteUsageDetail				, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointSiteUsageDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_SkypeForBusinessActivityUserDetail		, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_SkypeForBusinessDeviceUsageUserDetail	, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessDeviceUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_YammerActivityUserDetail				, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_YammerDeviceUsageUserDetail			, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerDeviceUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_YammerGroupsActivityDetail				, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerGroupsActivityDetail), 2) });

		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfEmpty	, _YTEXT("") });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, g_UseExistingReport });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveUsageAccountDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsDeviceUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsUserActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailAppUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::MailboxUsageDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365GroupsActivityDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActiveUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActivationsUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointSiteUsageDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessDeviceUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerDeviceUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Description_MultiSelect, EventTarget::g_HideIfUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerGroupsActivityDetail), 2) });

		getGenerator().addEvent(g_ReportID, { g_NoPeriod		, EventTarget::g_HideIfNotUnique, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActivationsUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_PeriodOnly		, EventTarget::g_HideIfNotEqual	, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::MailboxUsageDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_PeriodOrDate	, EventTarget::g_HideIfUnique	, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActivationsUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Date			, EventTarget::g_HideIfUnique	, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActivationsUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_PeriodOrDate	, EventTarget::g_HideIfEqual	, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::MailboxUsageDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_Date			, EventTarget::g_HideIfEqual	, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::MailboxUsageDetail), 2) });


		getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365GroupsActivityDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointSiteUsageDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerGroupsActivityDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_HideIfEqual, g_UseExistingReport });
		if (cacheWarningUsers)
		{
			getGenerator().addEvent(g_ReportID, { g_WarningCacheUser	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365GroupsActivityDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheUser	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointSiteUsageDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheUser	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerGroupsActivityDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheUser	, EventTarget::g_HideIfEqual, g_UseExistingReport });

			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveUsageAccountDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsDeviceUsageUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsUserActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailAppUsageUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::MailboxUsageDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActiveUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActivationsUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessDeviceUsageUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnUser	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerDeviceUsageUserDetail), 2) });
		}

		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveUsageAccountDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsDeviceUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsUserActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailAppUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::MailboxUsageDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActiveUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActivationsUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointSiteUsageDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessDeviceUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerActivityUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerDeviceUsageUserDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_HideIfEqual, g_UseExistingReport });
		if (cacheWarningGroups)
		{
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveUsageAccountDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsDeviceUsageUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsUserActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailAppUsageUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::MailboxUsageDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActiveUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActivationsUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointSiteUsageDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessDeviceUsageUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerActivityUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerDeviceUsageUserDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_WarningCacheGroup	, EventTarget::g_HideIfEqual, g_UseExistingReport });

			getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365GroupsActivityDetail), 2) });
			getGenerator().addEvent(g_ReportID, { g_AddColumnGroup	, EventTarget::g_DisableIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerGroupsActivityDetail), 2) });
		}

		// Temporary: When we have additional columns for sites, comment these lines and uncomment below ones.
		getGenerator().addEvent(g_ReportID, { g_AddColumnIntro	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointSiteUsageDetail), 2) });
		getGenerator().addEvent(g_ReportID, { g_AddColumnIntro	, EventTarget::g_HideIfEqual, g_UseExistingReport });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveActivityUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::OneDriveUsageAccountDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsDeviceUsageUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::TeamsUserActivityUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailActivityUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::EmailAppUsageUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::MailboxUsageDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365GroupsActivityDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActiveUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::Office365ActivationsUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SharePointActivityUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessActivityUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::SkypeForBusinessDeviceUsageUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerActivityUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerDeviceUsageUserDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, Str::getStringFromNumber(static_cast<int32_t>(O365ReportID::YammerGroupsActivityDetail), 2) });
		//getGenerator().addEvent(g_ReportID, { g_AddColumnSite	, EventTarget::g_HideIfEqual, g_UseExistingReport });

		getGenerator().addEvent(g_PeriodOrDate	, { g_Date, EventTarget::g_HideIfNotEqual	, g_PeriodIsDate });

		getGenerator().addEvent(g_SaveReport, { g_TargetFileName, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue });
	}

	// Existing reports
	if (!existingReports.empty())
	{
		getGenerator().addCategory(YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_30, _YLOC("Select the previously loaded report from the list:")).c_str(), CategoryFlags::EXPAND_BY_DEFAULT | CategoryFlags::NO_COLLAPSE);
		getGenerator().revertLastHbsHeightIncrease(); // Because hidden by default

		CheckListEditor::LabelsAndValues files;
		for (size_t i = 0; i < existingReports.size(); ++i)
			files.emplace_back(existingReports[i], existingReports[i]);

		ASSERT(!files.empty());

		auto flags = editFlags;
		getGenerator().Add(Expanded<AsRadio<WithTooltip<CheckListEditor>>>(
			{
				{
					g_ReportFile
					, wstring(YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_31, _YLOC("Existing report to use")).c_str()) + _YTEXT(" *")
					, flags
					, { std::get<1>(files.front()) }
				}
				, files
			}
			, YtriaTranslate::Do(DlgSelectO365Report_generateJSONScriptData_32, _YLOC("Select the wanted report")).c_str()));
		getGenerator().revertLastHbsHeightIncrease(); // Because hidden by default
	}

	getGenerator().addGlobalEventTrigger(g_ReportID);
	getGenerator().addGlobalEventTrigger(g_SaveReport);
	getGenerator().addGlobalEventTrigger(g_PeriodOrDate);

	getGenerator().addApplyButton(YtriaTranslate::Do(DlgGridCopySettings_OnInitDialog_12, _YLOC("OK")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str());
}

bool DlgSelectO365Report::processPostedData(const IPostedDataTarget::PostedData& data)
{
	ASSERT(!(data.end() != data.find(g_PeriodOrDate) && data.end() != data.find(g_PeriodOnly)));
	for (const auto& item : data)
	{
		if (g_ReportID == item.first)
		{
			if (item.second == g_UseExistingReport)
			{
				ASSERT(m_ReportConfig.m_O365ReportIDs.empty());
				m_LoadExisting = true;
			}
			else
			{
				ASSERT(!m_LoadExisting);
				const int32_t id = static_cast<int32_t>(Str::GetNumber(item.second));
				ASSERT(static_cast<int32_t>(O365ReportID::Undefined) < id && id < static_cast<int32_t>(O365ReportID::Count));
				if (static_cast<int32_t>(O365ReportID::Undefined) < id && id < static_cast<int32_t>(O365ReportID::Count))
					m_ReportConfig.m_O365ReportIDs.push_back(static_cast<O365ReportID>(id));
			}
		}
		else if (g_PeriodOrDate == item.first || g_PeriodOnly == item.first)
		{
			if (g_PeriodIsDate != item.second)
				m_ReportConfig.m_PeriodOrDate = item.second;
		}
		else if (g_Date == item.first)
			m_ReportConfig.m_PeriodOrDate = item.second;
		else if (g_SaveReport == item.first)
			m_ShouldSaveNewReport = Str::getBoolFromString(item.second);
		else if (g_ReportFile == item.first
			|| g_TargetFileName == item.first)
		{
			ASSERT(m_FileNames.empty());

			if (g_TargetFileName == item.first)
			{
				m_FileNames = { item.second };
			}
			else
			{
				ASSERT(m_ExistingReportSeries.end() != m_ExistingReportSeries.find(item.second));
				m_FileNames = m_ExistingReportSeries[item.second];
			}
		}
		else if (g_AddColumnUser == item.first
			|| g_AddColumnGroup == item.first
			|| g_AddColumnSite == item.first)
		{
			m_ReportConfig.m_AddtionalProperties.push_back(item.second);
		}
	}

	return true;
}

wstring DlgSelectO365Report::getDialogTitle() const
{
	return YtriaTranslate::Do(DlgSelectO365Report_getDialogTitle_1, _YLOC("Select Report")).c_str();
}

bool DlgSelectO365Report::isNoPeriodReport(const wstring& p_NameOrID)
{
	return isReport<O365ReportID::Office365ActivationsUserDetail>(p_NameOrID);
}

bool DlgSelectO365Report::isPeriodOnlyReport(const wstring& p_NameOrID)
{
	return isReport<O365ReportID::MailboxUsageDetail>(p_NameOrID);
}

bool DlgSelectO365Report::isUserReport(const wstring& p_NameOrID)
{
	return !isGroupReport(p_NameOrID) && !isSiteReport(p_NameOrID);
}

bool DlgSelectO365Report::isGroupReport(const wstring& p_NameOrID)
{
	return isReport<O365ReportID::Office365GroupsActivityDetail>(p_NameOrID)
		|| isReport<O365ReportID::YammerGroupsActivityDetail>(p_NameOrID);
}

bool DlgSelectO365Report::isSiteReport(const wstring& p_NameOrID)
{
	return isReport<O365ReportID::SharePointSiteUsageDetail>(p_NameOrID);
}

template <O365ReportID reportID>
bool DlgSelectO365Report::isReport(const wstring& p_NameOrID)
{
	return p_NameOrID == Str::getStringFromNumber(static_cast<int32_t>(reportID), 2)
		|| p_NameOrID == O365Report<reportID>::GetDisplayName();
}

void DlgSelectO365Report::GetMultiValueFields(vector < wstring >& o_MultiValueFieldNames) const
{
	o_MultiValueFieldNames = { g_AddColumnUser, g_AddColumnGroup/*, g_AddColumnSite*/ };
}

bool DlgSelectO365Report::setParamSpecific(AutomationAction* i_Action)
{
	ASSERT(nullptr == m_CurrentMotherAction || m_CurrentMotherAction == i_Action->GetMother());
	if (m_CurrentMotherAction != i_Action->GetMother())
	{
		m_CurrentMotherAction = i_Action->GetMother();
		ASSERT(IsActionShowUsageReport(m_CurrentMotherAction));

		m_CurrentSetParamTotalCount = 0;
		for (auto c : m_CurrentMotherAction->GetChildren())
		{
			if (IsActionSetParam(c) && MFCUtil::StringMatchW(c->GetParamValue(AutomationConstant::val().m_ParamNameTarget), GetAutomationTargetName())
				&& !MFCUtil::StringMatchW(c->GetParamValue(AutomationConstant::val().m_ParamNameName), AutomationConstant::val().m_ParamNameKeepAlive))
				++m_CurrentSetParamTotalCount;
		}
	}

	if (DlgFormsHTML::setParamSpecific(i_Action))
	{
		if (m_CurrentSetParamTotalCount == m_AutomationFieldValues.size())
		{
			const auto itReportName = m_AutomationFieldValues.find(AutomationConstant::val().m_ParamNameName);
			const auto hasParamReportName = m_AutomationFieldValues.end() != itReportName;
			auto reportName = hasParamReportName ? itReportName->second : Str::g_EmptyString;
			if (hasParamReportName)
				m_AutomationFieldValues.erase(itReportName);

			m_AutomationFieldValues.insert({ g_ReportID, reportName });

			if (reportName.empty())
				reportName = g_UseExistingReport;

			const bool useExistingReport = reportName == _T("** EXISTING REPORT **") || reportName == g_UseExistingReport;

			{
				const auto itReportFile = m_AutomationFieldValues.find(g_ParamNameReportFile);
				if (m_AutomationFieldValues.end() != itReportFile)
				{
					if (useExistingReport)
					{
						m_AutomationFieldValues.insert({ g_ReportFile, itReportFile->second });
					}
					else
					{
						m_AutomationFieldValues.insert({ g_TargetFileName, itReportFile->second });
						m_AutomationFieldValues.insert({ g_SaveReport, AutomationConstant::val().m_ValueTrue });
					}

					m_AutomationFieldValues.erase(itReportFile);
				}
			}

			{
				const auto itNameOrDate = m_AutomationFieldValues.find(g_ParamNamePeriodOrDate);
				if (m_AutomationFieldValues.end() != itNameOrDate)
				{
					if (isNoPeriodReport(reportName))
					{
						m_CurrentMotherAction->AddError(_YERROR("The selected Usage Report do not require any Period. Parameter ignored."), TraceOutput::TRACE_WARNING);
					}
					else
					{
						const auto& value = itNameOrDate->second;
						if (value == _YTEXT("D7") || value == _YTEXT("D30") || value == _YTEXT("D90") || value == _YTEXT("D180"))
						{
							m_AutomationFieldValues.insert({ g_PeriodOrDate, value });
						}
						else
						{
							if (isPeriodOnlyReport(reportName))
							{
								m_CurrentMotherAction->AddError(_YERROR("The selected Usage Report do not accept date. Period required."), TraceOutput::TRACE_ERROR);
							}
							else
							{
								m_AutomationFieldValues.insert({ g_PeriodOrDate, g_PeriodIsDate });
								m_AutomationFieldValues.insert({ g_Date, value });
							}
						}
					}

					m_AutomationFieldValues.erase(itNameOrDate);
				}
			}

			{
				const auto itAddInfo = m_AutomationFieldValues.find(g_ParamNameAdditionalInfo);
				if (m_AutomationFieldValues.end() != itAddInfo)
				{
					if (useExistingReport)
					{
						m_CurrentMotherAction->AddError(_YFORMATERROR(L"Invalid parameter %s when using an existing report.", g_ParamNameAdditionalInfo.c_str()).c_str(), TraceOutput::TRACE_ERROR);
					}
					else
					{
						auto values = Str::explodeIntoVector(itAddInfo->second, wstring(_YTEXT(";")), false, true);

						const wstring& field = isGroupReport(reportName)
							? g_AddColumnGroup
							: isSiteReport(reportName) ? g_AddColumnSite
							: isUserReport(reportName) ? g_AddColumnUser
							: Str::g_EmptyString;

						if (field == g_AddColumnSite)
						{
							m_CurrentMotherAction->AddError(_YFORMATERROR(L"%s not supported for report %s.", g_ParamNameAdditionalInfo.c_str(), reportName.c_str()).c_str(), TraceOutput::TRACE_ERROR);
						}
						else
						{
							ASSERT(!field.empty());
							if (field.empty())
							{
								m_CurrentMotherAction->AddError(_YFORMATERROR(L"Unknown report %s.", reportName.c_str()).c_str(), TraceOutput::TRACE_ERROR);
							}
							else
							{
								for (auto& val : values)
									m_AutomationFieldValues.insert({ field, val });
							}
						}
					}

					m_AutomationFieldValues.erase(itAddInfo);
				}
			}

			m_CurrentMotherAction = nullptr;
			m_CurrentSetParamTotalCount = 0;
		}

		return true;
	}

	return false;
}

void DlgSelectO365Report::TranslateAction(AutomatedApp* p_App, AutomationAction* p_Action, list<AutomationAction*>& p_ActionList)
{
	ASSERT(IsActionShowUsageReport(p_Action));

	p_ActionList.push_back(p_Action);

	// Uncomment this line if we want keepAlive to apply to this dialog too.
	//p_App->generateSetParamAction(AutomationConstant::val().m_ParamNameKeepAlive, g_ActionNameSelectReport, p_Action, p_ActionList);
	p_App->generateSetParamAction(AutomationConstant::val().m_ParamNameName, g_ActionNameSelectReport, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_ParamNameReportFile, g_ActionNameSelectReport, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_ParamNamePeriodOrDate, g_ActionNameSelectReport, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_ParamNameAdditionalInfo, g_ActionNameSelectReport, p_Action, p_ActionList);
}