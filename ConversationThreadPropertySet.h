#pragma once

#include "IPropertySetBuilder.h"

class ConversationThreadPropertySet : public IPropertySetBuilder
{
public:
    virtual vector<rttr::property> GetPropertySet() const override;
	virtual vector<PooledString> GetStringPropertySet() const override;
};

