#pragma once

#include "RowModification.h"
#include "FieldUpdate.h"

class O365Grid;

template<Scope scope>
class RowFieldUpdates : public RowModification
{
public:
    RowFieldUpdates(O365Grid& p_Grid, const row_pk_t& p_RowKey);
    virtual ~RowFieldUpdates();

    void RefreshState() override;
    void RefreshStatesPostProcess(GridUpdater& p_GridUpdater) override;

    void AddFieldUpdate(std::unique_ptr<FieldUpdate<scope>> p_FieldUpdate);
    void UpdateShownValues();
	bool RevertFieldUpdate(UINT p_ColId);

	const std::vector<std::unique_ptr<FieldUpdate<scope>>>& GetFieldUpdates() const;

	vector<ModificationLog> GetModificationLogs() const override;

protected:
	void setAppliedLocally(GridBackendRow* p_Row) override;

private:
    State GetHighestPriorityState() const;

    std::vector<std::unique_ptr<FieldUpdate<scope>>> m_FieldUpdates;
};

template<Scope scope>
RowFieldUpdates<scope>::RowFieldUpdates(O365Grid& p_Grid, const row_pk_t& p_RowKey)
	: RowModification(p_Grid, p_RowKey)
{
	GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

	ASSERT(nullptr != row);
	if (nullptr != row)
		O365Grid::RowStatusHandler<scope>(getGrid()).OnRowModified(row); // Display the "Row Edited" icon.
}

template<Scope scope>
RowFieldUpdates<scope>::~RowFieldUpdates()
{
	if (shouldRevert())
	{
		GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			// Remove all status flags from the row.
			O365Grid::RowStatusHandler<scope>(getGrid()).OnRowNothingToSave(row, false);
		}

		if (State::AppliedLocally == GetState())
		{
			m_FieldUpdates.clear(); // Revert field updates before grid notification

			m_State = Modification::State::BeingReverted;
			getGrid().ModificationStateChanged(row, *this);
		}
	}
}


template<Scope scope>
void RowFieldUpdates<scope>::setAppliedLocally(GridBackendRow* p_Row)
{
	m_State = IMainItemModification::State::AppliedLocally;
	O365Grid::RowStatusHandler<scope>(getGrid()).OnRowModified(p_Row);
}

template<Scope scope>
void RowFieldUpdates<scope>::AddFieldUpdate(std::unique_ptr<FieldUpdate<scope>> p_FieldUpdate)
{
	setAppliedLocally(getGrid().GetRowIndex().GetExistingRow(p_FieldUpdate->GetRowKey()));

	// Merge with existing field modification if it already exists. Else, overwrite
	bool fieldExisted = false;
	for (auto& fieldMod : m_FieldUpdates)
	{
		if (fieldMod->GetColumnID() == p_FieldUpdate->GetColumnID())
		{
			fieldMod = fieldMod->CreateReplacement(std::move(p_FieldUpdate));
			fieldExisted = true;
			break;
		}
	}

	if (!fieldExisted)
		m_FieldUpdates.push_back(std::move(p_FieldUpdate));
}

template<Scope scope>
void RowFieldUpdates<scope>::RefreshState()
{
	if (m_FieldUpdates.empty())
		return;

	m_State = GetHighestPriorityState();
	GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());
	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		switch (m_State)
		{
		case Modification::State::RemoteError:
			// Nothing to do. Row has already been marked as an error
			break;
		case Modification::State::AppliedLocally:
			setAppliedLocally(row);
			break;
		case Modification::State::RemoteHasOldValue:
			O365Grid::RowStatusHandler<scope>(getGrid()).OnRowSavedNotReceived(row, false, YtriaTranslate::Do(RowFieldUpdates_RefreshState_1, _YLOC("One or more modified value(s) have not been received.")).c_str());
			break;
		case Modification::State::RemoteHasNewValue:
			O365Grid::RowStatusHandler<scope>(getGrid()).OnRowSaved(row, false, YtriaTranslate::Do(RowFieldUpdates_RefreshState_2, _YLOC("All values have been modified successfully.")).c_str());
			break;
		case Modification::State::RemoteHasOtherValue:
			O365Grid::RowStatusHandler<scope>(getGrid()).OnRowSavedOverwritten(row, false, YtriaTranslate::Do(RowFieldUpdates_RefreshState_3, _YLOC("One or more value(s) sent have been overwritten.")).c_str());
			break;
		default:
			break;
		}

		getGrid().ModificationStateChanged(row, *this);
	}
}

template<Scope scope>
void RowFieldUpdates<scope>::RefreshStatesPostProcess(GridUpdater& /*p_GridUpdater*/)
{
	// In order to show "Final" status icons only once, we remove the modifications having a "Final" status
	m_FieldUpdates.erase(std::remove_if(m_FieldUpdates.begin(), m_FieldUpdates.end(), MustSurviveRefresh()), m_FieldUpdates.end());
}

template<Scope scope>
IMainItemModification::State RowFieldUpdates<scope>::GetHighestPriorityState() const
{
	int highestPriorityState = 0;
	for (const auto& modification : m_FieldUpdates)
	{
		// All fields modifications should belong to the same row
		ASSERT(modification->GetRowKey() == GetRowKey());
		if (modification->GetRowKey() == GetRowKey())
		{
			modification->RefreshState();
			if (static_cast<int>(modification->GetState()) > highestPriorityState)
				highestPriorityState = static_cast<int>(modification->GetState());
		}
	}

	return static_cast<State>(highestPriorityState);
}

template<Scope scope>
void RowFieldUpdates<scope>::UpdateShownValues()
{
	for (const auto& modification : m_FieldUpdates)
		modification->UpdateShownValue();
}

template<Scope scope>
bool RowFieldUpdates<scope>::RevertFieldUpdate(UINT p_ColId)
{
	bool fieldExisted = false;

	for (auto it = m_FieldUpdates.begin(); it != m_FieldUpdates.end(); ++it)
	{
		if ((*it)->GetColumnID() == p_ColId)
		{
			m_FieldUpdates.erase(it);
			fieldExisted = true;
			break;
		}
	}

	return fieldExisted;
}

template<Scope scope>
const std::vector<std::unique_ptr<FieldUpdate<scope>>>& RowFieldUpdates<scope>::GetFieldUpdates() const
{
	return m_FieldUpdates;
}

template<Scope scope>
vector<Modification::ModificationLog> RowFieldUpdates<scope>::GetModificationLogs() const
{
	vector<Modification::ModificationLog> mlogs;

	for (const auto& fu : GetFieldUpdates())
	{
		const auto fumLogs = fu->GetModificationLogs();
		mlogs.insert(mlogs.end(), fumLogs.begin(), fumLogs.end());
	}

	return mlogs;
}
