#include "DlgDownloadAttachments.h"

#include "AutomationNames.h"
#include "FileUtil.h"
#include "GridSetupMetaColumn.h"
#include "GridUtil.h"
#include "MsGraphFieldNames.h"
#include "Sapio365Settings.h"
#include "YFileDialog.h"
#include "YCodeJockMessageBox.h"

BEGIN_MESSAGE_MAP(DlgDownloadAttachments, ResizableDialog)
    ON_BN_CLICKED(IDC_BTN_CHOOSE_FOLDER,							OnSelectFolder)
    ON_BN_CLICKED(IDC_BTN_ADDCOLUMN,								OnAddColumn)
    ON_BN_CLICKED(IDC_BTN_REMOVECOLUMN,								OnRemoveColumn)
	ON_BN_CLICKED(IDC_DLG_ATTACHMENTS_APPEND,						OnSelectAppend)
	ON_BN_CLICKED(IDC_DLG_ATTACHMENTS_SKIP,							OnSelectSkip)
	ON_BN_CLICKED(IDC_DLG_ATTACHMENTS_OVERWRITE,					OnSelectOverwrite)
	ON_BN_CLICKED(IDC_DLG_ATTACHMENTS_CHECK_MAILFOLDERHIERARCHY,	OnMailFolderHierarchy)
	ON_BN_CLICKED(IDC_DLG_ATTACHMENTS_CHECK_USEROWNERNAME,			OnUseOwnerName)
	ON_BN_CLICKED(IDC_DLG_ATTACHMENTS_SHOWHIDDENCOL,				OnShowHiddenColumns)
	ON_BN_CLICKED(IDC_DLG_ATTACHMENTS_BTN_PRESETCOL,				OnPresetCol)
	ON_BN_CLICKED(IDC_DLG_ATTACHMENTS_BTN_SETASDEFAULT,				OnSetAsDefault)
    ON_BN_CLICKED(IDC_DLG_ATTACHMENTS_OPEN_FOLDER,                  OnSelectOpenFolder)
	ON_BN_CLICKED(IDC_DLG_ATTACHMENTS_BTN_TOGGLEINFO,				OnToggleInfo)
	ON_EN_CHANGE(IDC_EDIT_CHOSEN_FOLDER,							OnChangeChosenFolder)
END_MESSAGE_MAP()

const wstring DlgDownloadAttachments::g_AutomationName	= g_ActionNameSelectedAttachmentDownload;
const wstring DlgDownloadAttachments::m_ActionNameGridDownloadAttachmentsDefault = _YTEXT("SetGridAttachmentDownloadPivot");

FileExistsAction DlgDownloadAttachments::g_FileExistsAction = FileExistsAction::Append;

DlgDownloadAttachments::DlgDownloadAttachments(CacheGrid* p_Parent, DlgDownloadAttachments::CONTEXT p_Context)
    : DlgDownloadAttachments(p_Parent, p_Context, false, g_AutomationName)
{

}

DlgDownloadAttachments::DlgDownloadAttachments(CacheGrid* p_Parent, CONTEXT p_Context, bool p_UsedForMessageExport, const wstring& p_AutoName)
	: ResizableDialog(IDD, p_Parent, p_AutoName)
	, GridCrust(m_ActionNameGridDownloadAttachmentsDefault)
	, m_MotherGrid(p_Parent)
	, m_UseMFHierarchy(p_Context == CONTEXT::MESSAGE)
	, m_UseOwnerName(true)
	, m_Context(p_Context)
	, m_OpenFolder(false)
	, m_IsCustom(false)
{
	ASSERT(nullptr != m_MotherGrid);
	registerGrid(_YTEXT("GridColumnsList"), &m_GridColumnsSelected);
}

FileExistsAction DlgDownloadAttachments::GetFileExistsAction() const
{
    return m_FileExistsAction;
}

const wstring& DlgDownloadAttachments::GetFolderPath() const
{
    return m_FolderPath;
}

const vector<GridBackendColumn*>& DlgDownloadAttachments::GetSelectedColumnsTitleDisplayed() const
{
    return m_SelectedColumnsTitleDisplayed;
}

bool DlgDownloadAttachments::GetUseMFHierarchy() const
{
	return m_UseMFHierarchy;
}

bool DlgDownloadAttachments::GetUseOwnerName() const
{
	return m_UseOwnerName;
}

bool DlgDownloadAttachments::GetOpenFolder() const
{
    return m_OpenFolder;
}

void DlgDownloadAttachments::DoDataExchange(CDataExchange* pDX)
{
    ResizableDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_GROUP,						m_GroupIfFileExists);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_APPEND,					m_RadioCreateNew);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_OVERWRITE,					m_RadioOverwrite);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_SKIP,						m_RadioSkip);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_CHECK_MAILFOLDERHIERARCHY, m_CheckMailFolderHierarchy);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_CHECK_USEROWNERNAME,		m_CheckUseOwnerName);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_BTN_TOGGLEINFO,			m_BtnToggleInfo);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_STATIC_INTROTITLE,			m_StaticIntroTitle);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_STATIC_INTRO_BASIC,		m_StaticIntroBasic);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_STATIC_INTRO_CUSTOM,		m_StaticIntroCustom);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_STATIC_TEXT,				m_StaticText);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_STATIC_INTROGRIDLEFT,		m_GroupIntroGridLeft);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_STATIC_INTROGRIDRIGHT,		m_GroupIntroGridRight);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_SHOWHIDDENCOL,				m_CheckShowHiddenCol);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_STATIC_EXAMPLETITLE,		m_StaticExampleTitle);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_STATIC_EXAMPLE,			m_StaticExample);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_BTN_PRESETCOL,				m_BtnPresetCol);
	DDX_Control(pDX, IDC_DLG_ATTACHMENTS_BTN_SETASDEFAULT,			m_BtnSetAsDefault);
    DDX_Control(pDX, IDC_EDIT_CHOSEN_FOLDER,						m_TxtFolderPath);
    DDX_Control(pDX, IDC_BTN_CHOOSE_FOLDER,							m_BtnChooseFolder);
    DDX_Control(pDX, IDC_BTN_ADDCOLUMN,								m_BtnAddToSelection);
    DDX_Control(pDX, IDC_BTN_REMOVECOLUMN,							m_BtnRemoveFromSelection);
    DDX_Control(pDX, IDC_DLG_ATTACHMENTS_OPEN_FOLDER,               m_CheckOpenFolder);
    DDX_Control(pDX, IDOK,											m_BtnOk);
    DDX_Control(pDX, IDCANCEL,										m_BtnCancel);
}

BOOL DlgDownloadAttachments::OnInitDialogSpecificResizable()
{
	SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_18, _YLOC("Download Attachments")).c_str());

    m_BtnChooseFolder.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_1, _YLOC("Select Destination")).c_str());

	m_FolderPath = *DownloadFolderSetting().Get();

	m_TxtFolderPath.SetWindowText(m_FolderPath.c_str());
    m_TxtFolderPath.SetBkColor(RGB(255, 255, 255));

	m_GroupIfFileExists.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_2, _YLOC("If destination file already exists")).c_str());
    m_GroupIfFileExists.SetBkColor(RGB(255, 255, 255));
	m_RadioCreateNew.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_3, _YLOC("Create New")).c_str());
    m_RadioCreateNew.SetBkColor(RGB(255, 255, 255));
	m_RadioOverwrite.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_4, _YLOC("Overwrite")).c_str());
    m_RadioOverwrite.SetBkColor(RGB(255, 255, 255));
	m_RadioSkip.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_5, _YLOC("Skip")).c_str());
    m_RadioSkip.SetBkColor(RGB(255, 255, 255));
	m_CheckMailFolderHierarchy.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_6, _YLOC("Use Mail Folder Hierarchy")).c_str());
    m_CheckMailFolderHierarchy.SetBkColor(RGB(255, 255, 255));
	m_CheckUseOwnerName.SetWindowText(_T("Use Owner Display Name"));
	m_CheckUseOwnerName.SetBkColor(RGB(255, 255, 255));
	m_GroupIntroGridLeft.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_9, _YLOC("Available columns")).c_str());
    m_GroupIntroGridLeft.SetBkColor(RGB(255, 255, 255));
	m_GroupIntroGridRight.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_10, _YLOC("Selected columns")).c_str());
    m_GroupIntroGridRight.SetBkColor(RGB(255, 255, 255));
	m_CheckShowHiddenCol.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_11, _YLOC("Show Hidden Columns")).c_str());
    m_CheckShowHiddenCol.SetBkColor(RGB(255, 255, 255));
    m_CheckOpenFolder.SetBkColor(RGB(255, 255, 255));
    m_CheckOpenFolder.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_12, _YLOC("Open folder after download")).c_str());
	m_StaticExampleTitle.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_13, _YLOC("Your files will be saved as:")).c_str());
	m_StaticExampleTitle.SetFontBold();
    m_StaticExampleTitle.SetBkColor(RGB(255, 255, 255));
    m_StaticExample.SetBkColor(RGB(255, 255, 255));
	m_BtnPresetCol.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_14, _YLOC("Load default columns")).c_str());
	m_BtnSetAsDefault.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_15, _YLOC("Set As Default")).c_str());

	m_BtnToggleInfo.SetWindowText(m_IsCustom ? YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_16, _YLOC("Basic")).c_str() : YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_17, _YLOC("Customize")).c_str());

    UpdateFromGrid(m_MotherGrid);
    
    CRect rect;

    CWnd* gridColumnsSelected = GetDlgItem(IDC_ATTACHMENTS_SELECTEDCOLUMNS);
    gridColumnsSelected->ShowWindow(SW_HIDE);
    ASSERT(nullptr != gridColumnsSelected);
    if (nullptr != gridColumnsSelected)
    {
        gridColumnsSelected->GetWindowRect(&rect);
        ScreenToClient(&rect);
        if (!m_GridColumnsSelected.Create(this, rect))
        {
            ASSERT(false);
            return FALSE;
        }
		m_GridColumnsSelected.ShowWindow(SW_SHOW);
		m_GridColumnsSelected.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
    }

    CWnd* gridColumnsList = GetDlgItem(IDC_ATTACHMENTS_GRIDCOLUMNSLIST);
    gridColumnsList->ShowWindow(SW_HIDE);
    ASSERT(nullptr != gridColumnsList);
    if (nullptr != gridColumnsList)
    {
        gridColumnsList->GetWindowRect(&rect);
        ScreenToClient(&rect);
		if (!GetGridColumns().Create(this, rect))
        {
            ASSERT(false);
            return FALSE;
        }
		GetGridColumns().ShowWindow(SW_SHOW);
		GetGridColumns().SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
    }

	GetGridColumns().InitGrid(m_MotherGrid);

	m_CheckMailFolderHierarchy.SetCheck(BST_CHECKED);

	if (m_Context != CONTEXT::MESSAGE)
	{
		m_CheckMailFolderHierarchy.SetCheck(BST_UNCHECKED);
		m_CheckMailFolderHierarchy.ShowWindow(SW_HIDE);
	}

	m_CheckUseOwnerName.SetCheck(BST_CHECKED);

	{
		CRect rect;
		m_StaticText.GetWindowRect(&rect);
		ScreenToClient(&rect);
		m_ScrollableText.Create(ES_MULTILINE /*| ES_AUTOVSCROLL*/ | ES_READONLY | WS_CHILD | WS_VISIBLE/* | WS_BORDER*/, rect, this, IDC_DLG_ATTACHMENTS_SCROLLABLE_TEXT);
		m_ScrollableText.SetMargins(20, 20);
		m_ScrollableText.SetReadOnly(TRUE);
		m_ScrollableText.ShowWindow(SW_HIDE);
		m_ScrollableText.ShowScrollBar(SB_VERT);
		m_ScrollableText.SetFont(m_StaticText.GetFont(), FALSE);
	}

	AddAnchor(m_BtnToggleInfo, CSize(100, 0), CSize(100, 0));
	AddAnchor(m_TxtFolderPath, CSize(0, 0), CSize(100, 0));
	AddAnchor(m_BtnChooseFolder, CSize(100, 0), CSize(100, 0));
	AddAnchor(m_GroupIfFileExists, CSize(0, 0), CSize(100, 0));
	AddAnchor(m_RadioCreateNew, CSize(0, 0), CSize(33, 0));
	AddAnchor(m_RadioOverwrite, CSize(33, 0), CSize(67, 0));
	AddAnchor(m_RadioSkip, CSize(67, 0), CSize(100, 0));
	AddAnchor(m_CheckUseOwnerName, CSize(0, 0), CSize(50, 0));
	AddAnchor(m_CheckMailFolderHierarchy, CSize(50, 0), CSize(100, 0));
	AddAnchor(m_StaticIntroTitle, CSize(0, 0), CSize(100, 0));
	AddAnchor(m_StaticIntroBasic, CSize(0, 0), CSize(100, 0));
	AddAnchor(m_StaticIntroCustom, CSize(0, 0), CSize(100, 0));

	AddAnchor(m_StaticText, CSize(0, 0), CSize(100, 100));
	AddAnchor(m_ScrollableText, CSize(0, 0), CSize(100, 100));

	AddAnchor(m_BtnAddToSelection, CSize(50, 50), CSize(50, 50));
	AddAnchor(m_BtnRemoveFromSelection, CSize(50, 50), CSize(50, 50));
	AddAnchor(GetGridColumns(), CSize(0, 0), CSize(50, 100));
	AddAnchor(m_GridColumnsSelected, CSize(50, 0), CSize(100, 100));
	AddAnchor(m_GroupIntroGridLeft, CSize(0, 0), CSize(50, 100));
	AddAnchor(m_GroupIntroGridRight, CSize(50, 0), CSize(100, 100));
	AddAnchor(m_CheckOpenFolder, CSize(0, 100), CSize(100, 100));
	AddAnchor(m_BtnOk, CSize(100, 100), CSize(100, 100));
	AddAnchor(m_BtnCancel, CSize(100, 100), CSize(100, 100));

	AddAnchor(m_CheckShowHiddenCol, CSize(0, 100), CSize(50, 100));
	AddAnchor(m_StaticExampleTitle, CSize(0, 100), CSize(100, 100));
	AddAnchor(m_StaticExample, CSize(0, 100), CSize(100, 100));
	AddAnchor(m_BtnPresetCol, CSize(50, 100), CSize(75, 100));
	AddAnchor(m_BtnSetAsDefault, CSize(75, 100), CSize(100, 100));

	ShowSizeGrip();

	if (g_FileExistsAction == FileExistsAction::Append)
		m_RadioCreateNew.SetCheck(BST_CHECKED);
	else if (g_FileExistsAction == FileExistsAction::Overwrite)
		m_RadioOverwrite.SetCheck(BST_CHECKED);
	else if (g_FileExistsAction == FileExistsAction::Skip)
		m_RadioSkip.SetCheck(BST_CHECKED);
	m_FileExistsAction = g_FileExistsAction;

	m_StaticIntroTitle.SetFontBold();
	m_StaticIntroTitle.SetBkColor(RGB(255, 255, 255));
	m_StaticIntroBasic.SetBkColor(RGB(255, 255, 255));
	m_StaticIntroCustom.SetBkColor(RGB(255, 255, 255));
	m_StaticText.SetBkColor(RGB(255, 255, 255));
	m_ScrollableText.SetBkColor(RGB(255, 255, 255));
    SetBkColor(RGB(255, 255, 255));

	loadSavedDefaultColumns();

	showExample();

	showCustomizePanel();

	// Shitty tricky piece of shitty code to force the left grid (available columns) to be correctly drawn (without scrollbars).
	{
		CRect rect;
		GetWindowRect(&rect);
		rect.InflateRect(10, 10, 10, 10);
		MoveWindow(rect);
		rect.DeflateRect(10, 10, 10, 10);
		MoveWindow(rect);
	}

    return TRUE;
}

void DlgDownloadAttachments::OnOK()
{
    CString folderPath;
    m_TxtFolderPath.GetWindowText(folderPath);
	m_FolderPath = FileUtil::MakeValidFilePath(folderPath.GetBuffer(), _YTEXT(""));
    if (m_FolderPath.empty())
    {
        YCodeJockMessageBox dlg(this, DlgMessageBox::eIcon_Error, YtriaTranslate::Do(DlgDownloadAttachments_OnOK_1, _YLOC("Error")).c_str(), YtriaTranslate::Do(DlgDownloadAttachments_OnOK_2, _YLOC("Please choose a valid destination folder.")).c_str(), _YTEXT(""), { { IDOK, YtriaTranslate::Do(DlgDownloadAttachments_OnOK_3, _YLOC("OK")).c_str() } });
        dlg.DoModal();
        return;
    }
	else
		DownloadFolderSetting().Set(m_FolderPath);

    vector<GridBackendRow*> rows;
    m_GridColumnsSelected.GetAllNonGroupRows(rows);
    auto column = m_GridColumnsSelected.GetColumnByUniqueID(_YUID("S4"));

    m_SelectedColumnsTitleDisplayed.clear();
    for (auto row : rows)
    {
		GridBackendColumn* pMotherCol = (GridBackendColumn*)row->GetLParam();
		ASSERT(nullptr != pMotherCol);
		if (nullptr != pMotherCol)
			m_SelectedColumnsTitleDisplayed.push_back(pMotherCol);
    }

    ResizableDialog::OnOK();
}

CacheGrid* DlgDownloadAttachments::GetMotherGrid() const
{
    return m_MotherGrid;
}

void DlgDownloadAttachments::UpdateFromGrid(CacheGrid* i_FromGrid)
{
    const bool enable = GetGridColumns().HasOneNonGroupRowSelectedAtLeast();
    m_BtnAddToSelection.EnableWindow(enable);
    m_BtnRemoveFromSelection.EnableWindow(m_GridColumnsSelected.HasOneNonGroupRowSelectedAtLeast());
    setButtonArrayBitmap(m_BtnAddToSelection, true);
    setButtonArrayBitmap(m_BtnRemoveFromSelection, false);
}

void DlgDownloadAttachments::setupClearSpecific()
{}

const bool DlgDownloadAttachments::setupLoadSpecific(AutomationAction* i_settingsAction, list <wstring>& o_Errors)
{
    return true;
}

void DlgDownloadAttachments::OnSelectFolder()
{
	YFileDialog folderChooser(GetFolderPath().c_str(), 0, this);
    if (folderChooser.DoModal() == IDOK)
    {
        m_FolderPath = folderChooser.GetPathName()/*GetFolderPath()*/; //Bug #190201.RL.0099CE

		if (!Str::endsWith(m_FolderPath, _YTEXT("\\")))
			m_FolderPath.append(_YTEXT("\\"));

        m_TxtFolderPath.SetWindowText(GetFolderPath().c_str());
		showExample();
    }
}

void DlgDownloadAttachments::OnAddColumn()
{
	addSelectedColumnsToGrid(m_GridColumnsSelected);
	showExample();
}

void DlgDownloadAttachments::OnRemoveColumn()
{
    m_GridColumnsSelected.OnRemove();
    m_GridColumnsSelected.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	UpdateFromGrid(&m_GridColumnsSelected);
	showExample();
}

void DlgDownloadAttachments::OnMailFolderHierarchy()
{
	m_UseMFHierarchy = m_CheckMailFolderHierarchy.GetCheck() == BST_CHECKED;
	showExample();
}

void DlgDownloadAttachments::OnUseOwnerName()
{
	m_UseOwnerName = m_CheckUseOwnerName.GetCheck() == BST_CHECKED;
	showExample();
}

void DlgDownloadAttachments::OnToggleInfo()
{
	m_IsCustom = !m_IsCustom;
	showCustomizePanel();
}

void DlgDownloadAttachments::OnChangeChosenFolder()
{
	CString Temp;
	m_TxtFolderPath.GetWindowText(Temp);
	m_FolderPath = (LPCTSTR)Temp;
	showExample();
}

void DlgDownloadAttachments::OnShowHiddenColumns()
{
	GetGridColumns().ShowHiddenColumns(m_CheckShowHiddenCol.GetCheck() == BST_CHECKED, true);
}

void DlgDownloadAttachments::OnPresetCol()
{
	GridBackendRow* pRowDefault1 = nullptr;
	GridBackendRow* pRowDefault2 = nullptr;
	if (m_Context == CONTEXT::MESSAGE)
	{
		pRowDefault1 = GetGridColumns().GetColumnRow(_YUID(O365_MESSAGE_SENTDATETIME));
		pRowDefault2 = GetGridColumns().GetColumnRow(_YUID(O365_MESSAGE_FROMNAME));
	}
	else if (m_Context == CONTEXT::EVENT)
	{
		pRowDefault1 = GetGridColumns().GetColumnRow(_YUID("start.dateTime"));
		pRowDefault2 = GetGridColumns().GetColumnRow(_YUID("organizer.emailAddress.Name"));
	}
	else if (m_Context == CONTEXT::POST)
	{
		pRowDefault1 = GetGridColumns().GetColumnRow(_YUID("receivedDateTime"));
		pRowDefault2 = GetGridColumns().GetColumnRow(_YUID("fromName"));
	}
	else
		ASSERT(FALSE);

	if (nullptr != pRowDefault1 || nullptr != pRowDefault2)
	{
		m_GridColumnsSelected.OnSelectAll();
		m_GridColumnsSelected.OnRemove();
		m_GridColumnsSelected.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		UpdateFromGrid(&m_GridColumnsSelected);

		// The following keeps the order of insertion.
		GetGridColumns().OnUnSelectAll();

		if (nullptr != pRowDefault1)
			pRowDefault1->SetSelected(true);

		OnAddColumn();
		GetGridColumns().OnUnSelectAll();

		if (nullptr != pRowDefault2)
			pRowDefault2->SetSelected(true);

		OnAddColumn();
		GetGridColumns().OnUnSelectAll();
	}
}

void DlgDownloadAttachments::OnSetAsDefault()
{
	wstring defaultColValue;

	std::vector<GridBackendRow*> ListRows;
	m_GridColumnsSelected.GetAllNonGroupRows(ListRows);
	for (GridBackendRow* pRow : ListRows)
	{
		GridBackendColumn* pCol = (GridBackendColumn*)pRow->GetLParam();
		ASSERT(nullptr != pCol);
		if (nullptr != pCol)
		{
			if (!defaultColValue.empty())
				defaultColValue.append(_YTEXT(";"));
			defaultColValue.append(pCol->GetUniqueID());
		}
	}

	if (m_Context == CONTEXT::MESSAGE)
		DownloadMessageDefaultColumnsSetting().Set(defaultColValue);
	else if (m_Context == CONTEXT::EVENT)
		DownloadEventDefaultColumnsSetting().Set(defaultColValue);
	else if (m_Context == CONTEXT::POST)
		DownloadPostDefaultColumnsSetting().Set(defaultColValue);
	else
		ASSERT(FALSE);
}

void DlgDownloadAttachments::OnSelectOpenFolder()
{
    m_OpenFolder = m_CheckOpenFolder.GetCheck() == BST_CHECKED;
}

////////////////////// Automation

void DlgDownloadAttachments::setAutomationFieldMap()
{
	addAutomationFieldMap(AutomationConstant::val().m_ParamNameFilePath,	&m_TxtFolderPath);
	addAutomationFieldMap(g_OwnerName,										&m_CheckUseOwnerName);
	addAutomationFieldMap(g_Hierarchy,										&m_CheckMailFolderHierarchy);
	addAutomationFieldMap(g_OpenFolderAfterDownload,						&m_CheckOpenFolder);
	//addAutomationFieldMap(AutomationConstant::val().m_Click,				&m_BtnChooseFolder);

	addAutomationFieldMap(g_CreateNew,	&m_RadioCreateNew);
	addAutomationFieldMap(g_Overwrite,	&m_RadioOverwrite);
	addAutomationFieldMap(g_Skip,		&m_RadioSkip);

	fieldAutomationOverridesReadOnly(&m_TxtFolderPath);
}

void DlgDownloadAttachments::setAutomationRadioButtonGroups()
{
	addAutomationRadioButtonToGroup(g_IfFileExists, &m_RadioCreateNew);
	addAutomationRadioButtonToGroup(g_IfFileExists, &m_RadioOverwrite);
	addAutomationRadioButtonToGroup(g_IfFileExists, &m_RadioSkip);
}

void DlgDownloadAttachments::setAutomationListChoicesGroups()
{}

void DlgDownloadAttachments::OnSelectAppend()
{
	m_FileExistsAction = FileExistsAction::Append;
	g_FileExistsAction = m_FileExistsAction;
}

void DlgDownloadAttachments::OnSelectSkip()
{
	m_FileExistsAction = FileExistsAction::Skip;
	g_FileExistsAction = m_FileExistsAction;
}

void DlgDownloadAttachments::OnSelectOverwrite()
{
	m_FileExistsAction = FileExistsAction::Overwrite;
	g_FileExistsAction = m_FileExistsAction;
}

void DlgDownloadAttachments::moveControl(CWnd* p_pCtrl)
{
	if (nullptr != p_pCtrl)
	{
		CRect rcTemp;
		p_pCtrl->GetWindowRect(&rcTemp);
		ScreenToClient(&rcTemp);
		rcTemp.OffsetRect(0, m_IsCustom ? HIDPI_YH(280) : HIDPI_YH(-280));
		p_pCtrl->MoveWindow(rcTemp);
	}
}

void DlgDownloadAttachments::showExample()
{
	GridBackendRow* pFirstSelRow = nullptr;
	for (auto& row : m_MotherGrid->GetSelectedRows())
	{
		if (!row->IsGroupRow() && m_MotherGrid->IsHierarchyLowestChildType(row))
		{
			pFirstSelRow = row;
			break;
		}
	}

	ASSERT(nullptr != pFirstSelRow);

	wstring pathExample(m_FolderPath);

	if (GetUseOwnerName())
	{
		// Parent Folder
		GridBackendColumn* pPFCol = m_MotherGrid->GetColumnByUniqueID(GridSetupMetaColumn::g_ColUIDMetaDisplayName);
		ASSERT(nullptr != pPFCol);
		if (nullptr != pPFCol)
		{
			GridBackendField& field = pFirstSelRow->GetField(pPFCol);
			ASSERT(field.HasValue());
			if (field.HasValue())
				pathExample.append(field.GetValueStr());
		}
	}

	if (GetUseMFHierarchy())
	{
		// Parent Folder
		GridBackendColumn* pPFCol = m_MotherGrid->GetColumnByUniqueID(_YUID(O365_MESSAGE_PARENTFOLDER));
		ASSERT(nullptr != pPFCol);
		if (nullptr != pPFCol)
		{
			GridBackendField& field = pFirstSelRow->GetField(pPFCol);
			ASSERT(field.HasValue());
			if (field.HasValue())
			{
				if (!Str::endsWith(pathExample, _YTEXT("\\")))
					pathExample.append(_YTEXT("\\"));
				pathExample.append(field.GetValueStr());
			}
		}
	}

	{
		auto column = m_GridColumnsSelected.GetColumnByUniqueID(_YUID("S4"));
		vector<GridBackendColumn*> TempList;
		for (auto row : m_GridColumnsSelected.GetBackend().GetBackendRowsForDisplay())
		{
			if (!row->IsGroupRow())
			{
				GridBackendColumn* pMotherCol = (GridBackendColumn*)row->GetLParam();
				ASSERT(nullptr != pMotherCol);
				if (nullptr != pMotherCol)
					TempList.push_back(pMotherCol);
			}
		}

		if (!Str::endsWith(pathExample, _YTEXT("\\")))
			pathExample.append(_YTEXT("\\"));
		const auto columnsPath = GridUtil::GetColumnsPath(*m_MotherGrid, pFirstSelRow, TempList);
		if (!columnsPath.empty())
		{
			pathExample.append(columnsPath);
			if (!Str::endsWith(pathExample, _YTEXT("\\")))
				pathExample.append(_YTEXT("\\"));
		}
	}

	if (pathExample == m_FolderPath)
		m_StaticExample.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_showExample_1, _YLOC("Warning, all attachments of all messages will be placed in the following folder: %1"), pathExample.c_str()).c_str());
	else
		m_StaticExample.SetWindowText(pathExample.c_str());
}

void DlgDownloadAttachments::showCustomizePanel()
{
	if (m_IsCustom)
	{
		wstring Temp(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_8, _YLOC("You can add attributes to your file name by selecting from the columns below. These attributes will be appended to the current path and file name.")));
		m_BtnToggleInfo.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_showCustomizePanel_1, _YLOC("Basic")).c_str());
		m_StaticIntroTitle.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_7, _YLOC("Optional:")).c_str());
		
		m_StaticIntroBasic.ShowWindow(SW_HIDE);
		m_StaticIntroCustom.ShowWindow(SW_SHOW);
		m_StaticIntroCustom.SetWindowText(Temp.c_str());

		m_StaticText.SetWindowText(CString(""));
		m_StaticText.ShowWindow(SW_HIDE);
		m_ScrollableText.SetWindowText(CString(""));
		m_ScrollableText.ShowWindow(SW_HIDE);
	}
	else
	{
		wstring title;
		wstring columnsText;
		vector<GridBackendRow*> rows;
		m_GridColumnsSelected.GetAllNonGroupRows(rows);
		if (rows.empty())
		{
			title.append(YtriaTranslate::Do(DlgDownloadAttachments_showCustomizePanel_2, _YLOC("No attributes selected for current path and file name:")).c_str());
		}
		else
		{
			title.append(YtriaTranslate::Do(DlgDownloadAttachments_showCustomizePanel_3, _YLOC("The following attributes will be appended to the current path and file name:")).c_str());

			auto column = m_GridColumnsSelected.GetColumnByUniqueID(_YUID("S4"));
			for (auto row : rows)
			{
				columnsText.append(_YTEXT("  "));
				columnsText.append(row->GetField(column).GetValueStr());
				columnsText.append(_YTEXT("\r\n"));
			}
		}

		m_BtnToggleInfo.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_showCustomizePanel_4, _YLOC("Customize")).c_str());
		m_StaticIntroTitle.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_showCustomizePanel_5, _YLOC("Basic Info")).c_str());
		m_StaticIntroBasic.SetWindowText(title.c_str());
		m_StaticIntroBasic.ShowWindow(SW_SHOW);
		m_StaticIntroCustom.ShowWindow(SW_HIDE);

		m_StaticText.SetWindowText(columnsText.c_str());
		
		CDC* pDC = m_StaticText.GetDC();
		CSize SizeText = pDC->GetTextExtent(CString(columnsText.c_str()));
		m_StaticText.ReleaseDC(pDC);

		const auto height = static_cast<int>(rows.size()) * SizeText.cy;

		CRect rcTemp;
		m_StaticText.GetWindowRect(&rcTemp);

		if (height > rcTemp.Height())
		{
			// Text to big for static text, use scroll
			m_ScrollableText.SetWindowText(columnsText.c_str());
			m_StaticText.ShowWindow(SW_HIDE);
			m_ScrollableText.ShowWindow(SW_SHOW);
		}
		else
		{
			// Use static text
			m_StaticText.ShowWindow(SW_SHOW);
			m_ScrollableText.ShowWindow(SW_HIDE);
		}
	}

	m_BtnAddToSelection.ShowWindow(m_IsCustom ? SW_SHOW : SW_HIDE);
	m_BtnRemoveFromSelection.ShowWindow(m_IsCustom ? SW_SHOW : SW_HIDE);
	m_GroupIntroGridLeft.ShowWindow(m_IsCustom ? SW_SHOW : SW_HIDE);
	m_GroupIntroGridRight.ShowWindow(m_IsCustom ? SW_SHOW : SW_HIDE);
	m_GridColumnsSelected.ShowWindow(m_IsCustom ? SW_SHOW : SW_HIDE);
	GetGridColumns().ShowWindow(m_IsCustom ? SW_SHOW : SW_HIDE);
	m_CheckShowHiddenCol.ShowWindow(m_IsCustom ? SW_SHOW : SW_HIDE);
	m_BtnPresetCol.ShowWindow(m_IsCustom ? SW_SHOW : SW_HIDE);
	m_BtnSetAsDefault.ShowWindow(m_IsCustom ? SW_SHOW : SW_HIDE);
}

void DlgDownloadAttachments::loadSavedDefaultColumns()
{
	ASSERT(m_Context == CONTEXT::MESSAGE || m_Context == CONTEXT::EVENT || m_Context == CONTEXT::POST);
	auto defaultCols = m_Context == CONTEXT::MESSAGE
		? DownloadMessageDefaultColumnsSetting().Get()
		: m_Context == CONTEXT::EVENT	? DownloadEventDefaultColumnsSetting().Get()
										: m_Context == CONTEXT::POST	? DownloadPostDefaultColumnsSetting().Get()
																		: boost::none;

	// Temporarily load the hidden columns.
	// Could have been saved as default.
	m_CheckShowHiddenCol.SetCheck(BST_CHECKED);
	OnShowHiddenColumns();

	if (defaultCols)
	{
		list<wstring> ListSavedColumns = Str::explode(*defaultCols, wstring(_YTEXT(";")));
		for (const wstring& ColUniqueID : ListSavedColumns)
		{
			GridBackendRow* pRowDefault = GetGridColumns().GetColumnRow(ColUniqueID);

			ASSERT(nullptr != pRowDefault);

			GetGridColumns().OnUnSelectAll();

			if (nullptr != pRowDefault)
				pRowDefault->SetSelected(true);

			OnAddColumn();
		}
	}
	else
		OnPresetCol();

	// Unload the hidden columns.
	m_CheckShowHiddenCol.SetCheck(BST_UNCHECKED);
	OnShowHiddenColumns();
}

void DlgDownloadAttachments::automationSetParamPostProcess()
{
	// cf revision 128097
	// 	CString fp;
	// 	m_TxtFolderPath.GetWindowText(fp);
	// 	m_FolderPath = fp;
}

// =================================================================================================

const wstring DlgExportMessage::g_AutomationName = g_ActionNameSelectedDownloadAsEML;

DlgExportMessage::DlgExportMessage(CacheGrid* p_Parent)
	: DlgDownloadAttachments(p_Parent, DlgDownloadAttachments::CONTEXT::MESSAGE, true, g_AutomationName)
{
}

BOOL DlgExportMessage::OnInitDialogSpecificResizable()
{
	auto ret = DlgDownloadAttachments::OnInitDialogSpecificResizable();
	SetWindowText(_T("Download as EML"));
	return ret;
}

