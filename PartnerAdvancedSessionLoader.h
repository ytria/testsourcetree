#pragma once

#include "ISessionLoader.h"

class PartnerAdvancedSessionLoader : public ISessionLoader
{
public:
	PartnerAdvancedSessionLoader(const SessionIdentifier& p_Identifier);

protected:
	TaskWrapper<std::shared_ptr<Sapio365Session>> Load(const std::shared_ptr<Sapio365Session>& p_SapioSession) override;
};
