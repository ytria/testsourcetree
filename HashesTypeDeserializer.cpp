#include "HashesTypeDeserializer.h"

#include "JsonSerializeUtil.h"

void HashesTypeDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("sha1Hash"), m_Data.Sha1Hash, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("crc32Hash"), m_Data.Crc32Hash, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("quickXorHash"), m_Data.QuickXorHash, p_Object);
}
