#include "LinkHandlerMessages.h"

#include "CommandDispatcher.h"
#include "CommandInfo.h"

void LinkHandlerMessages::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);
    
	CommandInfo info;
    info.SetOrigin(Origin::User);
	p_Link.ENABLE_FREE_ACCESS();// free access
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Message, Command::ModuleTask::ListMe, info, { nullptr, g_ActionNameMyDataMail, nullptr, p_Link.GetAutomationAction() }));
}

bool LinkHandlerMessages::IsAllowedWithAJLLicense() const
{
	return true; // only MyData is allowed
}