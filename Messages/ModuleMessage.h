#pragma once

#include "ModuleBase.h"

#include "BusinessMailFolder.h"
#include "CommandInfo.h"
#include "AttachmentsInfo.h"
#include "ModuleOptions.h"
#include "RefreshSpecificData.h"
#include "MessageListRequester.h"

class AutomationAction;
class FrameMessages;
class BusinessMessage;
struct EmlDownloadInfo;

class ModuleMessage : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	virtual void executeImpl(const Command& p_Command) override;
	void showMessages(Command p_Command);

	void update(const Command& p_Command);
	void doRefresh(FrameMessages* p_pFrame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command);

	void showAttachments(const Command& p_Command);
    void downloadAttachments(const Command& p_Command);
    void showItemAttachment(const Command& p_Command);
    void deleteAttachments(const Command& p_Command);

	void loadSnapshot(const Command& p_Command);

	O365DataMap<BusinessUser, vector<BusinessMailFolder>> retrieveMessages(std::shared_ptr<BusinessObjectManager> p_BOM, const ModuleCriteria& p_Criteria, bool p_LoadUsersFromSQlCache, const ModuleOptions& p_Options, std::map<BusinessUser, std::shared_ptr<MessageListRequester>>& p_Requesters, YtriaTaskData p_TaskData) const;

	static TaskWrapper<vector<BusinessMailFolder>> GetMailFolders(const std::shared_ptr<BusinessObjectManager>& p_Bom, const PooledString& p_UserId, const ModuleOptions& p_Options, std::map<BusinessUser, std::shared_ptr<MessageListRequester>>& p_Requesters, const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData);

    void downloadInlineAttachments(const Command& p_Command);
	void downloadAndSaveAsEml(const Command& p_Command);
	void downloadAndSaveAsEml(std::shared_ptr<BusinessObjectManager> p_BOM, const EmlDownloadInfo& p_EmlDownloadInfo, FrameMessages* p_Frame, const Command& p_Command);
	
	static void addToFlatMailFolders(map<PooledString, BusinessMailFolder*>& flatMailFolders, vector<BusinessMailFolder>& pMailFolders);
};
