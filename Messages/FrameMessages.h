#pragma once

#include "GridFrameBase.h"
#include "GridMessages.h"

#include "BusinessMessage.h"
#include "MessageListRequester.h"

class FrameMessages : public GridFrameBase
{
public:
	FrameMessages(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
	DECLARE_DYNAMIC(FrameMessages)
	virtual ~FrameMessages() = default;

	void ShowMessages(const O365DataMap<BusinessUser, vector<BusinessMailFolder>>& p_Messages, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_FullPurge, bool p_NoPurge);
	void ShowMessagesAttachements(const O365DataMap<BusinessUser, vector<BusinessMailFolder>>& p_Messages, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_FullPurge, const ModuleBase::AttachmentsContainer& p_BusinessAttachments);
    void ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments);
    void ShowItemAttachment(const BusinessItemAttachment& p_ItemAttachment);

	virtual O365Grid& GetGrid() override;
	
    virtual void ApplySelectedSpecific() override;
    virtual void ApplyAllSpecific() override;

    void ApplySpecific(bool p_Selected);

	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

	virtual void HiddenViaHistory() override;
    virtual bool HasApplyButton() override;

	virtual void InitModuleCriteria(const ModuleCriteria& p_ModuleCriteria) override;
	void UpdateModuleCriteria(const ModuleCriteria& p_ModuleCriteria);

protected:
	virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;
	virtual O365ControlConfig GetApplyPartialControlConfig() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	GridMessages m_messagesGrid;
};
