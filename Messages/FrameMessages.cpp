#include "Office365Admin.h"

#include "AutomationNames.h"
#include "CommandDispatcher.h"
#include "FrameMessages.h"
#include "LoggerService.h"
#include "UpdatedObjectsGenerator.h"

// For asynchronous
#include <ppltasks.h>
using namespace concurrency;

IMPLEMENT_DYNAMIC(FrameMessages, GridFrameBase)

//BEGIN_MESSAGE_MAP(FrameMessages, GridFrameBase)
//END_MESSAGE_MAP()

FrameMessages::FrameMessages(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
	, m_messagesGrid(p_IsMyData)
{
    m_CreateAddRemoveToSelection = !p_IsMyData;
    m_CreateRefreshPartial = !p_IsMyData;
	m_CreateShowContext = !p_IsMyData;
	m_CreateApplyPartial = !p_IsMyData;
}

void FrameMessages::ShowMessagesAttachements(const O365DataMap<BusinessUser, vector<BusinessMailFolder>>& p_Messages, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_FullPurge, const ModuleBase::AttachmentsContainer& p_BusinessAttachments)
{
	if (CompliesWithDataLoadPolicy())
		m_messagesGrid.BuildTreeViewAttachements(p_Messages, GetOrigin(), p_UpdateOperations, p_Refresh, p_FullPurge, false, p_BusinessAttachments);
}

void FrameMessages::ShowMessages(const O365DataMap<BusinessUser, vector<BusinessMailFolder>>& p_Messages, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_FullPurge, bool p_NoPurge)
{
	if (CompliesWithDataLoadPolicy())
		m_messagesGrid.BuildTreeView(p_Messages, GetOrigin(), p_UpdateOperations, p_Refresh, p_FullPurge, p_NoPurge);
}

void FrameMessages::createGrid()
{
	m_messagesGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameMessages::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigUsers();
}

GridFrameBase::O365ControlConfig FrameMessages::GetApplyPartialControlConfig()
{
	return GetApplyPartialControlConfigUsers();
}

// returns false if no frame specific tab is needed.
bool FrameMessages::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
    CreateViewGroup(tab);
	CreateApplyGroup(tab);
	CreateRevertGroup(tab);
	CXTPRibbonGroup* refreshGroup = CreateRefreshGroup(tab);
	ASSERT(nullptr != refreshGroup);
	if (nullptr != refreshGroup)
	{
		CXTPControl* ctrl = refreshGroup->Add(xtpControlButton, ID_MESSAGEGRID_CHANGEOPTIONS);
		setImage({ ID_MESSAGEGRID_CHANGEOPTIONS }, IDB_CHANGEMODULEOPTIONS, xtpImageNormal);
		setImage({ ID_MESSAGEGRID_CHANGEOPTIONS }, IDB_CHANGEMODULEOPTIONS_16x16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	CXTPRibbonGroup* dataGroup = tab.AddGroup(YtriaTranslate::Do(FrameMessages_customizeActionsRibbonTab_2, _YLOC("Preview")).c_str());
	ASSERT(nullptr != dataGroup);
	if (nullptr != dataGroup)
	{
		{
			auto ctrl = dataGroup->Add(xtpControlButton, ID_MESSAGEGRID_SHOWBODY);
			setImage({ ID_MESSAGEGRID_SHOWBODY }, IDB_MESSAGE_CONTENT, xtpImageNormal);
			setImage({ ID_MESSAGEGRID_SHOWBODY }, IDB_MESSAGE_CONTENT_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = dataGroup->Add(xtpControlButton, ID_MESSAGEGRID_DOWNLOADASEML);
			setImage({ ID_MESSAGEGRID_DOWNLOADASEML }, IDB_MESSAGE_ID_MESSAGEGRID_DOWNLOADASEML, xtpImageNormal);
			setImage({ ID_MESSAGEGRID_DOWNLOADASEML }, IDB_MESSAGE_ID_MESSAGEGRID_DOWNLOADASEML_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	CreateEditGroup(tab, false);

	auto group = tab.AddGroup(YtriaTranslate::Do(FrameMessages_customizeActionsRibbonTab_1, _YLOC("Attachments")).c_str());
	ASSERT(nullptr != group);
	if (nullptr != group)
	{
		setGroupImage(*group, 0);

		{
			auto ctrl = group->Add(xtpControlButton, ID_MESSAGEGRID_SHOWATTACHMENTS);
			setImage({ ID_MESSAGEGRID_SHOWATTACHMENTS }, IDB_MESSAGES_SHOWATTACHMENTS, xtpImageNormal);
			setImage({ ID_MESSAGEGRID_SHOWATTACHMENTS }, IDB_MESSAGES_SHOWATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_MESSAGEGRID_TOGGLEINLINEATTACHMENTS);
			GridFrameBase::setImage({ ID_MESSAGEGRID_TOGGLEINLINEATTACHMENTS }, IDB_MESSAGES_TOGGLEINLINEATTACHMENTS, xtpImageNormal);
			GridFrameBase::setImage({ ID_MESSAGEGRID_TOGGLEINLINEATTACHMENTS }, IDB_MESSAGES_TOGGLEINLINEATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_MESSAGEGRID_DOWNLOADATTACHMENTS);
			setImage({ ID_MESSAGEGRID_DOWNLOADATTACHMENTS }, IDB_MESSAGES_DOWNLOADATTACHMENTS, xtpImageNormal);
			setImage({ ID_MESSAGEGRID_DOWNLOADATTACHMENTS }, IDB_MESSAGES_DOWNLOADATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_MESSAGEGRID_VIEW_ITEM_ATTACHMENT);
			setImage({ ID_MESSAGEGRID_VIEW_ITEM_ATTACHMENT }, IDB_MESSAGES_VIEWITEMATTACHMENT, xtpImageNormal);
			setImage({ ID_MESSAGEGRID_VIEW_ITEM_ATTACHMENT }, IDB_MESSAGES_VIEWITEMATTACHMENT_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_MESSAGEGRID_DELETEATTACHMENT);
			setImage({ ID_MESSAGEGRID_DELETEATTACHMENT }, IDB_MESSAGES_DELETEATTACHMENTS, xtpImageNormal);
			setImage({ ID_MESSAGEGRID_DELETEATTACHMENT }, IDB_MESSAGES_DELETEATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

    CreateLinkGroups(tab, !m_messagesGrid.IsMyData() && (GetOrigin() == Origin::User || GetOrigin() == Origin::DeletedUser), GetOrigin() == Origin::Group);

	automationAddCommandIDToGreenLight(ID_MESSAGEGRID_CHANGEOPTIONS);
	automationAddCommandIDToGreenLight(ID_MESSAGEGRID_SHOWBODY);
	automationAddCommandIDToGreenLight(ID_MESSAGEGRID_TOGGLEINLINEATTACHMENTS);
	automationAddCommandIDToGreenLight(ID_MESSAGEGRID_DELETEATTACHMENT);

	return true;
}

O365Grid& FrameMessages::GetGrid()
{
	return m_messagesGrid;
}

void FrameMessages::ApplySelectedSpecific()
{
	ApplySpecific(true);
}

void FrameMessages::ApplyAllSpecific()
{
	ApplySpecific(false);
}

void FrameMessages::ApplySpecific(bool p_Selected)
{
	// Main items (message deletion)
	UpdatedObjectsGenerator<BusinessMessage> updatedGroupsGenerator;
	updatedGroupsGenerator.BuildUpdatedObjects(GetGrid(), p_Selected, UOGFlags::ACTION_DELETE);

	// Sub items (attachment deletion)
    const auto data = m_messagesGrid.GetDeleteAttachmentsData(p_Selected);

	CommandInfo info;
	info.Data() = std::make_pair(updatedGroupsGenerator, data);
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Message, Command::ModuleTask::UpdateModified, info, { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
}

void FrameMessages::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	if (GetOptions())
		info.Data2() = *GetOptions();
	if (!HasLastUpdateOperations())
	{
		try
		{
			GridMessages& grid = dynamic_cast<GridMessages&>(GetGrid());
			grid.SetShowInlineAttachments(false);
		}
		catch (std::bad_cast)
		{
			ASSERT(false);
		}
	}

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Message, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameMessages::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	if (GetOptions())
		info.Data2() = *GetOptions();
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Message, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

AutomatedApp::AUTOMATIONSTATUS FrameMessages::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionSelectedPreviewBody(p_Action))
		p_CommandID = ID_MESSAGEGRID_SHOWBODY;
	else if (IsActionSelectedAttachmentLoadInfo(p_Action))
	{
		p_CommandID = ID_MESSAGEGRID_SHOWATTACHMENTS;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedAttachmentDownload(p_Action))
	{
		p_CommandID = ID_MESSAGEGRID_DOWNLOADATTACHMENTS;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedPreviewItem(p_Action))
		p_CommandID = ID_MESSAGEGRID_VIEW_ITEM_ATTACHMENT;
	else if (IsActionSelectedAttachmentDelete(p_Action))
		p_CommandID = ID_MESSAGEGRID_DELETEATTACHMENT;
	else if (IsActionSelectedDownloadAsEML(p_Action))
		p_CommandID = ID_MESSAGEGRID_DOWNLOADASEML;

	return statusRV;
}

void FrameMessages::ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments)
{
    m_messagesGrid.ViewAttachments(p_BusinessAttachments, true, true, true);
}

void FrameMessages::ShowItemAttachment(const BusinessItemAttachment& p_ItemAttachment)
{
    m_messagesGrid.ShowItemAttachment(p_ItemAttachment);
}

void FrameMessages::HiddenViaHistory()
{
	m_messagesGrid.HideMessageViewer();
}

bool FrameMessages::HasApplyButton()
{
    return true;
}

void FrameMessages::InitModuleCriteria(const ModuleCriteria& p_ModuleCriteria)
{
	GridFrameBase::InitModuleCriteria(p_ModuleCriteria);
	if (!p_ModuleCriteria.m_SubIDs.empty())
	{
		ASSERT(p_ModuleCriteria.m_Origin == Origin::User);
		m_CreateAddRemoveToSelection = false;
	}
}

void FrameMessages::UpdateModuleCriteria(const ModuleCriteria& p_ModuleCriteria)
{
	GetModuleCriteria() = p_ModuleCriteria;
}
