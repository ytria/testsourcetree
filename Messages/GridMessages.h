#pragma once

#include "BaseO365Grid.h"

#include "BusinessFileAttachment.h"
#include "BusinessMessage.h"
#include "AttachmentsInfo.h"
#include "GridTemplateUsers.h"
#include "GridUtil.h"
#include "MyDataMeRowHandler.h"

class FrameMessages;
class MessageBodyViewer;

using MessageIdAndPathByUser = O365DataMap<wstring, std::vector<std::pair<wstring, wstring>>>;
struct EmlDownloadInfo
{
	MessageIdAndPathByUser m_MessageIdAndPathByUser;
	wstring m_PathToOpen; // If not empty, this path will be open at the end of the download process. Usually the parent folder.
	FileExistsAction m_FileExistsAction = FileExistsAction::Append;
};

class GridMessages : public ModuleO365Grid<BusinessMessage>
{
public:
	GridMessages(bool p_IsMyData = false);
	virtual ~GridMessages() override;

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessMailFolder>>& p_Messages, Origin p_Origin, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_FullPurge, bool p_NoPurge);
	void BuildTreeViewAttachements(const O365DataMap<BusinessUser, vector<BusinessMailFolder>>& p_Messages, Origin p_Origin, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_FullPurge, bool p_NoPurge, const ModuleBase::AttachmentsContainer& p_BusinessAttachments);

	void BuildMailFolderView(const BusinessMailFolder& p_MailFolder, GridBackendRow& p_ParentRow, GridUpdater& p_Updater);

    void ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_RefreshModificationStates, bool p_ShowPostUpdateError, bool p_UpdateGrid);
    std::set<AttachmentsInfo::IDs> GetAttachmentsRequestInfo(const std::vector<GridBackendRow*>& p_Rows, const O365IdsContainer& p_MessageIdsFilter);

    void SetShowInlineAttachments(const bool& p_Val);

	std::vector<SubItemKey> GetDeleteAttachmentsData(bool p_Selected);

	GridBackendColumn* GetColMetaID() const;

	void HideMessageViewer();

	GridBackendColumn* GetColumnAttachmentId() const;

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeDelete() const override;

	bool IsMyData() const override;
    O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

protected:
	virtual void customizeGrid() override;
	virtual void customizeGridPostProcess() override;

	virtual BusinessMessage getBusinessObject(GridBackendRow*) const override;
	virtual void			UpdateBusinessObjects(const vector<BusinessMessage>& p_Messages, bool p_SetModifiedStatus) override;
	virtual void			RemoveBusinessObjects(const vector<BusinessMessage>& p_Messages) override;
	virtual void			OnGbwSelectionChangedSpecific() override;

    virtual bool HasModificationsPending(bool inSelectionOnly = false) override;

	virtual BOOL PreTranslateMessage(MSG* pMsg) override;

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;
	bool GetSnapshotAdditionalValue(wstring& p_Value, GridBackendRow& p_Row) override;
	bool SetSnapshotAdditionalValue(const wstring& p_Value, GridBackendRow& p_Row) override;

private:
	afx_msg void OnShowMessageBody();
    afx_msg void OnShowMessageAttachments();
	afx_msg void OnChangeOptions();

    afx_msg void OnDownloadMessageAttachments();
    afx_msg void OnViewItemAttachment();
    afx_msg void OnDeleteAttachment();
	afx_msg void OnToggleInlineAttachments();
	afx_msg void OnDownloadAsEml();
	afx_msg void OnUpdateChangeOptions(CCmdUI* pCmdUI);
    afx_msg void OnUpdateShowMessageBody(CCmdUI* pCmdUI);
    afx_msg void OnUpdateShowMessageAttachments(CCmdUI* pCmdUI);
    afx_msg void OnUpdateDownloadMessageAttachments(CCmdUI* pCmdUI);
    afx_msg void OnUpdateViewItemAttachment(CCmdUI* pCmdUI);
    afx_msg void OnUpdateDeleteAttachment(CCmdUI* pCmdUI);
    afx_msg void OnUpdateToggleInlineAttachments(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDownloadAsEml(CCmdUI* pCmdUI);
    DECLARE_MESSAGE_MAP()

	virtual void InitializeCommands() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart);

    GridBackendRow* fillMessageRow(GridBackendRow* p_ParentRow, const BusinessMessage& p_BusinessMessages);
    void fillMailFolderRow(const BusinessMailFolder& p_MailFolder, GridBackendRow& p_MailFolderRow);

	void loadAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, GridUpdater& updater);
    void fillAttachmentFields(GridBackendRow* row, const vector<BusinessAttachment>& p_Attachments);

	virtual void onPostExplodeMultiValueSisterhood(GridBackendRow* p_Row, const std::set<GridBackendColumn*>& p_Columns) override;
	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

private:
    void loadMoreImpl(const std::vector<GridBackendRow*>& p_Rows);

	GridBackendColumn* getColMetaDisplayName() const;
	GridBackendColumn* getColMetaUserPrincipalName() const;

	std::unique_ptr<MessageBodyViewer> m_MessageViewer;

	DECLARE_CacheGrid_DeleteRowLParam

	GridTemplateUsers m_Template;

	GridBackendColumn* m_ColChildFolderCount;
	GridBackendColumn* m_ColTotalItemCount;
	GridBackendColumn* m_ColUnreadItemCount;

    GridBackendColumn* m_ColSenderAddress;
    GridBackendColumn* m_ColSenderName;
    GridBackendColumn* m_ColFromAddress;
    GridBackendColumn* m_ColFromName;

    GridBackendColumn* m_ColCcRecipientsAddresses;
    GridBackendColumn* m_ColCcRecipientsNames;
    GridBackendColumn* m_ColBccRecipientsAddresses;
    GridBackendColumn* m_ColBccRecipientsNames;
    GridBackendColumn* m_ColReplyToAddresses;
    GridBackendColumn* m_ColReplyToNames;
    GridBackendColumn* m_ColToRecipientsAddresses;
    GridBackendColumn* m_ColToRecipientsNames;

	GridBackendColumn* m_ColBodyContentType;
    GridBackendColumn* m_ColSubject;
    GridBackendColumn* m_ColBodyPreview;
    GridBackendColumn* m_ColChangeKey;
    GridBackendColumn* m_ColConversationId;
    GridBackendColumn* m_ColCreatedDate;
    GridBackendColumn* m_ColImportance;
    GridBackendColumn* m_ColInferenceClassification;
    GridBackendColumn* m_ColInternetMessageId;
    GridBackendColumn* m_ColLastModifiedDateTime;
    GridBackendColumn* m_ColParentFolderName;
	GridBackendColumn* m_ColParentFolderId;
    GridBackendColumn* m_ColReceivedDateTime;
    GridBackendColumn* m_ColSentDateTime;
    GridBackendColumn* m_ColWebLink;
    GridBackendColumn* m_ColHasGraphAttachment;
    GridBackendColumn* m_ColHasAnyAttachment;
    GridBackendColumn* m_ColIsDeliveryReceiptRequested;
    GridBackendColumn* m_ColIsDraft;
    GridBackendColumn* m_ColIsRead;
    GridBackendColumn* m_ColIsReadReceiptRequested;
    GridBackendColumn* m_ColCategories;
	GridBackendColumn* m_ColUniqueBodyContentType;

    GridBackendColumn* m_ColAttachmentType;
    GridBackendColumn* m_ColAttachmentId;
    GridBackendColumn* m_ColAttachmentName;
	GridBackendColumn* m_ColAttachmentNameExtOnly;
    GridBackendColumn* m_ColAttachmentContentType;
	//GridBackendColumn* m_ColAttachmentContentId;
    GridBackendColumn* m_ColAttachmentSize;
    GridBackendColumn* m_ColAttachmentLastModifiedDateTime;
    GridBackendColumn* m_ColAttachmentIsInline;

	GridBackendColumn* m_ColHeaderName;
	GridBackendColumn* m_ColHeaderValue;

	vector<GridBackendColumn*>	m_MetaColumns;

    FrameMessages*				m_FrameMessages;

	bool m_HasLoadMoreAdditionalInfo;
	O365IdsContainer m_MetaIDsMoreLoaded;

    GridUtil::AttachmentIcons m_Icons;

    bool m_ShowInlineAttachments;
    ModuleBase::AttachmentsContainer m_AllAttachments;

	bool m_IsMyData;
	boost::YOpt<MyDataMeRowHandler> m_MyDataMeHandler;

	HACCEL m_hAccelSpecific = nullptr;

    friend class MessageAttachmentDeletion;
    friend class AttachmentInfo;
	friend class MessageBodyViewer;
};