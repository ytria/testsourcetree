#include "ModuleMessage.h"

#include "AttachmentsInfo.h"
#include "AutomationDataStructure.h"
#include "BusinessFileAttachment.h"
#include "BusinessMessage.h"
#include "BusinessObjectManager.h"
#include "DlgModuleOptions.h"
#include "FrameMessages.h"
#include "GraphCache.h"
#include "IMsGraphPageRequester.h"
#include "ModuleUtil.h"
#include "MSGraphCommonData.h"
#include "MSGraphUtil.h"
#include "Office365Admin.h"
#include "RESTUtils.h"
#include "safeTaskCall.h"
#include "TaskDataManager.h"
#include "UpdatedObjectsGenerator.h"
#include "YCallbackMessage.h"
#include "YDataCallbackMessage.h"
#include "ModuleOptions.h"
#include "DlgMessagesModuleOptions.h"
#include "MultiObjectsPageRequestLogger.h"
#include "MultiObjectsRequestLogger.h"
#include "YSafeCreateTask.h"
#include "MessagePropertySet.h"
#include "MsGraphHttpRequestLogger.h"
#include "RunOnScopeEnd.h"
#include "MailFoldersTopLevelRequester.h"
#include "ChildrenMailFoldersRequester.h"
#include "BasicPageRequestLogger.h"
#include "MailFolderHierarchyRequester.h"

using namespace Rest;

void ModuleMessage::executeImpl(const Command& p_Command)
{
	Command cmd = p_Command;
    switch (cmd.GetTask())
    {
    case Command::ModuleTask::ListMe:
		cmd.GetCommandInfo().GetIds() = { PooledString(GetConnectedSession().GetConnectedUserId()) };
		// Fallthrough
    case Command::ModuleTask::List:
		showMessages(cmd);
        break;
    case Command::ModuleTask::ListAttachments:
		showAttachments(cmd);
        break;
    case Command::ModuleTask::DownloadAttachments:
		downloadAttachments(cmd);
        break;
    case Command::ModuleTask::DownloadInlineAttachments:
        downloadInlineAttachments(cmd);
        break;
	case Command::ModuleTask::DownloadAndSaveAsEml:
		downloadAndSaveAsEml(cmd);
		break;
    case Command::ModuleTask::ViewItemAttachment:
		showItemAttachment(cmd);
        break;
    case Command::ModuleTask::DeleteAttachment:
		deleteAttachments(cmd);
		break;
	case Command::ModuleTask::UpdateModified:
		update(cmd);
		break;
	case Command::ModuleTask::LoadSnapshot:
		loadSnapshot(p_Command);
		break;
    default:
		ASSERT(false);
		SetAutomationGreenLight(cmd.GetAutomationAction(), cmd.ToString());
		break;
    }
}

void ModuleMessage::showMessages(Command p_Command)
{
	AutomationAction*	p_Action	= p_Command.GetAutomationAction();
    const CommandInfo&	info		= p_Command.GetCommandInfo();
    const auto			p_Origin	= info.GetOrigin();

    ASSERT(p_Origin != Origin::NotSet);
    if (p_Origin != Origin::NotSet)
    {
		bool isUpdate = false;

        const auto&	p_UserIDs		= info.GetIds();
        auto		p_SourceWindow	= p_Command.GetSourceWindow();

        auto sourceCWnd = p_SourceWindow.GetCWnd();

		auto frame = dynamic_cast<FrameMessages*>(info.GetFrame());

		if (nullptr == frame)
		{
			ASSERT(!p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>());

			if (FrameMessages::CanCreateNewFrame(true, sourceCWnd, p_Action))
			{
				ModuleCriteria moduleCriteria;
				moduleCriteria.m_Origin				= p_Origin;
				moduleCriteria.m_UsedContainer		= ModuleCriteria::UsedContainer::IDS;
				moduleCriteria.m_IDs				= p_UserIDs;
				moduleCriteria.m_Privilege			= info.GetRBACPrivilege();
				moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

				if (moduleCriteria.m_IDs.empty())
				{
					auto& id = GetConnectedSession().GetConnectedUserId();
					ASSERT(!id.empty());
					if (!id.empty())
						moduleCriteria.m_IDs.insert(id);
				}

				if (ShouldCreateFrame<FrameMessages>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
				{
					DlgMessagesModuleOptions opt(p_SourceWindow.GetCWnd(), Sapio365Session::Find(p_Command.GetSessionIdentifier()));
					if (IDCANCEL == opt.DoModal())
					{
						if (nullptr != p_Action)
							SetActionCanceledByUser(p_Action);
						return;
					}
					ASSERT(!p_Command.GetCommandInfo().Data2().is_valid());
					p_Command.GetCommandInfo().Data2() = opt.GetOptions();

					CWaitCursor _;

					const wstring title = p_Command.GetTask() == Command::ModuleTask::ListMe
						? _T("My Messages")
						: YtriaTranslate::Do(ModuleMessage_showMessages_1, _YLOC("Messages")).c_str();
					frame = new FrameMessages(p_Command.GetTask() == Command::ModuleTask::ListMe, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), title, *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
					if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
						frame->SetOptions(p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>());
					frame->InitModuleCriteria(moduleCriteria);
					AddGridFrame(frame);
					frame->Erect();
				}
			}
		}
		else
		{
			isUpdate = true;
		}

		if (nullptr != frame)
		{
			if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
				frame->SetOptions(p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>());
			auto taskData = addFrameTask(YtriaTranslate::Do(ModuleMessage_showMessages_2, _YLOC("Show Messages...")).c_str(), frame, p_Command, !isUpdate);
			doRefresh(frame, p_Action, taskData, isUpdate, p_Command);
		}
        else
        {
            SetAutomationGreenLight(p_Action, _YTEXT(""));
        }
    }
    else
    {
        SetAutomationGreenLight(p_Action, p_Command.ToString() + YtriaTranslate::DoError(ModuleContact_showContacts_3, _YLOC(" - origin not set"),_YR("Y2460")).c_str());
    }
}

void ModuleMessage::addToFlatMailFolders(map<PooledString, BusinessMailFolder*>& p_FlatMailFolders, vector<BusinessMailFolder>& p_MailFolders)
{
	for (auto& childMailFolder : p_MailFolders)
	{
		p_FlatMailFolders[childMailFolder.GetID()] = &childMailFolder;
		addToFlatMailFolders(p_FlatMailFolders, childMailFolder.m_ChildrenMailFolders);
	}
}

void ModuleMessage::showAttachments(const Command& p_Command)
{
	auto p_Action		= p_Command.GetAutomationAction();
	auto info       	= p_Command.GetCommandInfo();

	FrameMessages*	p_Frame	= dynamic_cast<FrameMessages*>(info.GetFrame());

	if (info.Data().is_type<std::set<AttachmentsInfo::IDs>>())
	{
		auto ids = info.Data().get_value<std::set<AttachmentsInfo::IDs>>();
		ModuleBase::showAttachments(p_Frame->GetBusinessObjectManager(), ModuleUtil::SetToVector(ids), p_Frame, Origin::Message, p_Command);
	}
	else
	{
		ASSERT(false);
		SetAutomationGreenLight(p_Action, p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2461")).c_str());
	}
}

void ModuleMessage::update(const Command& p_Command)
{
	AutomationAction* action = p_Command.GetAutomationAction();

	using DataType = std::pair<UpdatedObjectsGenerator<BusinessMessage>, std::vector<SubItemKey>>;

	ASSERT(p_Command.GetCommandInfo().Data().is_type<DataType>());
	if (p_Command.GetCommandInfo().Data().is_type<DataType>())
	{
		auto updatedMessagesGenerator = p_Command.GetCommandInfo().Data().get_value<DataType>().first;
		auto attachmentsToDelete = p_Command.GetCommandInfo().Data().get_value<DataType>().second;
		ASSERT(!updatedMessagesGenerator.GetObjects().empty() || !attachmentsToDelete.empty());
		if (!updatedMessagesGenerator.GetObjects().empty() || !attachmentsToDelete.empty())
		{
			// The frame to update once the data has been sent.
			GridFrameBase* existingFrame = dynamic_cast<GridFrameBase*>(p_Command.GetCommandInfo().GetFrame());
			ASSERT(nullptr != existingFrame);
			if (nullptr != existingFrame)
			{
				wstring taskTitle;
				if (updatedMessagesGenerator.GetObjects().empty())
					taskTitle = YtriaTranslate::Do(ModuleMessage_update_1, _YLOC("Delete Attachments")).c_str();
				else if (attachmentsToDelete.empty())
					taskTitle = YtriaTranslate::Do(ModuleMessage_update_2, _YLOC("Update Messages")).c_str();
				else
					taskTitle = YtriaTranslate::Do(ModuleMessage_update_3, _YLOC("Update Messages and Delete Attachments")).c_str();

				GridMessages* gridMessages = dynamic_cast<GridMessages*>(&existingFrame->GetGrid());

				auto taskData = addFrameTask(taskTitle, existingFrame, p_Command, false);
				YSafeCreateTask([taskData, hwnd = existingFrame->GetSafeHwnd(), metaIDColID = gridMessages->GetColMetaID()->GetID(), graphIDColID = gridMessages->GetColId()->GetID(), criteria = existingFrame->GetModuleCriteria(), bom = existingFrame->GetBusinessObjectManager(), updatedMessagesGenerator, attachmentsToDelete, action]()
				{
					RefreshSpecificData refreshData;
					refreshData.m_Criteria = criteria;
					auto newframeCriteria = criteria;

					O365SubIdsContainer subIdsBackup;

					if (refreshData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::IDS)
					{
						refreshData.m_Criteria.m_IDs.clear();
					}
					else
					{
						ASSERT(refreshData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::SUBIDS);
						subIdsBackup = refreshData.m_Criteria.m_SubIDs;
						refreshData.m_Criteria.m_SubIDs.clear();
					}

					std::vector<O365UpdateOperation> updateOperations;
					if (!updatedMessagesGenerator.GetObjects().empty())
					{
						updateOperations = bom->WriteSingleRequest(updatedMessagesGenerator.GetObjects(), updatedMessagesGenerator.GetPrimaryKeys(), taskData);

						for (auto& updateOp : updateOperations)
						{
							auto userId = updateOp.GetPkFieldStr(metaIDColID);
							if (refreshData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::IDS)
							{
								refreshData.m_Criteria.m_IDs.insert(userId);
							}
							else
							{
								auto messageId = updateOp.GetPkFieldStr(graphIDColID);
								if (subIdsBackup.end() != subIdsBackup.find(userId))
								{
									if (refreshData.m_Criteria.m_SubIDs.end() == refreshData.m_Criteria.m_SubIDs.find(userId))
										refreshData.m_Criteria.m_SubIDs[userId] = subIdsBackup[userId];

									ASSERT(updateOp.GetResults().size() == 1);
									for (const auto& result : updateOp.GetResults())
									{
										if (result.m_ResultInfo)
										{
											ASSERT(web::http::methods::DEL == result.m_ResultInfo->Request.method());
											if (204 == result.m_ResultInfo->Response.status_code())
											{
												// Success deletion
												refreshData.m_Criteria.m_SubIDs[userId].erase(messageId);
												newframeCriteria.m_SubIDs[userId].erase(messageId);
												break;
											}
										}
									}
								}
							}
						}
					}

					std::map<SubItemKey, HttpResultWithError> attachmentResults;
					// Delete attachments
					for (auto& attachment : attachmentsToDelete)
					{
						attachmentResults[attachment] = bom->DeleteUserMessageAttachment(attachment.m_Id0, attachment.m_Id1, attachment.m_Id2, taskData).GetTask().get();

						if (refreshData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::IDS)
						{
							refreshData.m_Criteria.m_IDs.insert(attachment.m_Id0);
						}
						else
						{
							if (refreshData.m_Criteria.m_SubIDs.end() == refreshData.m_Criteria.m_SubIDs.find(PooledString(attachment.m_Id0)))
								refreshData.m_Criteria.m_SubIDs[PooledString(attachment.m_Id0)] = subIdsBackup[PooledString(attachment.m_Id0)];
						}
					}

					YCallbackMessage::DoPost([hwnd, action, taskData, refreshData, updateOperations{ std::move(updateOperations) }, attachmentResults{ std::move(attachmentResults) }, newframeCriteria]()
					{
						if (::IsWindow(hwnd))
						{
							auto frame = dynamic_cast<FrameMessages*>(CWnd::FromHandle(hwnd));
							frame->UpdateModuleCriteria(newframeCriteria);
							frame->GetGrid().ClearLog(taskData.GetId());
							frame->GetGrid().GetModifications().SetSubItemDeletionsErrors(attachmentResults);
							frame->RefreshAfterUpdate(vector<O365UpdateOperation>{ updateOperations }, refreshData, action);
						}
						else
						{
							SetAutomationGreenLight(action);
						}
						TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
					});
				});
			}
			else
				SetAutomationGreenLight(action, _YTEXT(""));
		}
		else
			SetAutomationGreenLight(action);
	}
	else
		SetAutomationGreenLight(action);
}

void ModuleMessage::doRefresh(FrameMessages* p_pFrame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command)
{
    GridMessages* gridMessages = dynamic_cast<GridMessages*>(&p_pFrame->GetGrid());

    std::vector<GridBackendRow*> rowsForLoadMore;

    RefreshSpecificData refreshSpecificData;

	if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
	{
		ASSERT(p_IsUpdate);
		refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();
	}
	else
	{
		gridMessages->GetRowsWithMoreLoaded(rowsForLoadMore);
	}
    
    if (refreshSpecificData.m_SelectedRowsAncestors && !refreshSpecificData.m_SelectedRowsAncestors->empty())
    {
		ASSERT(rowsForLoadMore.empty());
        gridMessages->GetRowsByCriteria(rowsForLoadMore, [&rowAncestors = *refreshSpecificData.m_SelectedRowsAncestors](GridBackendRow* row) {
            return row->IsMoreLoaded() && rowAncestors.find(row->GetTopAncestorOrThis()) != rowAncestors.end();
        });
    }

    auto attachmentsRequestInfo = ModuleUtil::SetToVector(gridMessages->GetAttachmentsRequestInfo(rowsForLoadMore, O365IdsContainer{}));
	YSafeCreateTask([this, p_TaskData, hwnd = p_pFrame->GetSafeHwnd(), currentModuleCriteria = p_pFrame->GetModuleCriteria(), p_Action, p_IsUpdate, gridMessages, refreshSpecificData, bom = p_pFrame->GetBusinessObjectManager(), attachmentsRequestInfo, wantsAdditionalMetadata = (bool)p_pFrame->GetMetaDataColumnInfo(), p_Command]()
	{
        // Get messages
        const bool partialRefresh = (refreshSpecificData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::SUBIDS)
			? !refreshSpecificData.m_Criteria.m_SubIDs.empty()
			: !refreshSpecificData.m_Criteria.m_IDs.empty();

		const auto& criteria = partialRefresh ? refreshSpecificData.m_Criteria : currentModuleCriteria;
		ModuleOptions moduleOptions;
		if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
			moduleOptions = p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>();

		map<BusinessUser, std::shared_ptr<MessageListRequester>> requesters;
        const auto messages = retrieveMessages(bom, criteria, wantsAdditionalMetadata, moduleOptions, requesters, p_TaskData);

        // Get attachments
		ModuleBase::AttachmentsContainer attachments;
        bool showAttachments = false;
        if (!attachmentsRequestInfo.empty() && !p_TaskData.IsCanceled())
        {
            attachments = retrieveAttachments(bom, attachmentsRequestInfo, Origin::Message, p_TaskData);
            showAttachments = true;
        }

		YCallbackMessage::DoPost([messages, hwnd, partialRefresh, p_TaskData, p_Action, requesters, attachments, showAttachments, p_IsUpdate, isCanceled = p_TaskData.IsCanceled()]
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameMessages*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, p_TaskData, isCanceled && !everythingCanceled(messages)))
			{
				ASSERT(nullptr != frame);

				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting messages for %s users...", Str::getStringFromNumber(messages.size()).c_str()));
				frame->GetGrid().ClearLog(p_TaskData.GetId());
                //frame->GetGrid().ClearStatus();

				{
					CWaitCursor _;
					if (frame->HasLastUpdateOperations())
					{
						if (showAttachments)
							frame->ShowMessagesAttachements(messages, frame->GetLastUpdateOperations(), true, !showAttachments && !partialRefresh, attachments);
						else
							frame->ShowMessages(messages, frame->GetLastUpdateOperations(), !showAttachments, !showAttachments && !partialRefresh, false);
						frame->ForgetLastUpdateOperations();
					}
					else
					{
						if (showAttachments)
							frame->ShowMessagesAttachements(messages, {}, true, !showAttachments && !partialRefresh, attachments);
						else
							frame->ShowMessages(messages, {}, !showAttachments, !showAttachments && !partialRefresh, false);
					}
				}

				if (!p_IsUpdate)
				{
					frame->UpdateContext(p_TaskData, hwnd);
					shouldFinishTask = false;
				}
			}
			
			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, p_TaskData, p_Action);
			}
		});
	});
}

void ModuleMessage::downloadAttachments(const Command& p_Command)
{
	auto p_AutomationAction = p_Command.GetAutomationAction();
	auto info       		= p_Command.GetCommandInfo();
	
	FrameMessages* existingFrame = dynamic_cast<FrameMessages*>(info.GetFrame());

	if (info.Data().is_type<AttachmentsInfo>())
	{
		auto attachmentInfos = info.Data().get_value<AttachmentsInfo>();
		ModuleBase::downloadAttachments(existingFrame->GetBusinessObjectManager(), attachmentInfos, existingFrame, Origin::Message, p_Command);
	}
	else
	{
		SetAutomationGreenLight(p_AutomationAction, p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2462")).c_str());
	}
}

void ModuleMessage::showItemAttachment(const Command& p_Command)
{
    CommandInfo info = p_Command.GetCommandInfo();
	FrameMessages* existingFrame = dynamic_cast<FrameMessages*>(info.GetFrame());

	ASSERT(info.Data().is_type<AttachmentsInfo::IDs>());
	if (info.Data().is_type<AttachmentsInfo::IDs>())
	{
		auto p_AutomationAction = p_Command.GetAutomationAction();
		auto existingFrame = dynamic_cast<FrameMessages*>(info.GetFrame());
		ModuleBase::showItemAttachment(existingFrame->GetBusinessObjectManager(), info.Data().get_value<AttachmentsInfo::IDs>(), Origin::Message, existingFrame, p_Command);
	}
	else
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2463")).c_str());
}

void ModuleMessage::deleteAttachments(const Command& p_Command)
{
    CommandInfo info = p_Command.GetCommandInfo();
    AutomationAction* action = p_Command.GetAutomationAction();
    FrameMessages* frame = dynamic_cast<FrameMessages*>(info.GetFrame());

    GridMessages* gridMessages = dynamic_cast<GridMessages*>(&frame->GetGrid());

    ASSERT(info.Data().is_type<std::vector<SubItemKey>>());
    if (info.Data().is_type<std::vector<SubItemKey>>())
    {
        std::vector<SubItemKey> attachments = info.Data().get_value<std::vector<SubItemKey>>();

		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleMessage_deleteAttachments_1, _YLOC("Delete Attachments")).c_str(), frame, p_Command, false);
		YSafeCreateTask([this, attachments, hwnd = frame->GetSafeHwnd(), taskData, action, gridMessages, criteria = frame->GetModuleCriteria(), bom = frame->GetBusinessObjectManager()]()
		{
			RefreshSpecificData refreshData;
			refreshData.m_Criteria = criteria;
			ASSERT(refreshData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::IDS);
			refreshData.m_Criteria.m_IDs.clear();

            std::map<SubItemKey, HttpResultWithError> results;

            // Delete attachments
            for (auto& attachment : attachments)
            {
                results[attachment] = bom->DeleteUserMessageAttachment(attachment.m_Id0, attachment.m_Id1, attachment.m_Id2, taskData).GetTask().get();
				refreshData.m_Criteria.m_IDs.insert(attachment.m_Id0);
            }

            YCallbackMessage::DoPost([hwnd, gridMessages, refreshData, action, taskData, results]()
            {
                if (::IsWindow(hwnd))
                {
					auto frame = dynamic_cast<FrameMessages*>(CWnd::FromHandle(hwnd));
					frame->GetGrid().GetModifications().SetSubItemDeletionsErrors(results);
					frame->GetGrid().ClearLog(taskData.GetId());
					frame->RefreshAfterSubItemUpdates(refreshData, action);
                }
                else
                {
                    SetAutomationGreenLight(action);
                }
                TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
            });
        });
    }
    else
    {
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2464")).c_str());
    }
}

void ModuleMessage::loadSnapshot(const Command& p_Command)
{
	LoadSnapshot<FrameMessages>(p_Command, [&p_Command](const GridSnapshot::Metadata& p_SnapshotMeta, const ModuleCriteria& p_ModuleCriteria, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
		{
			const bool isMyData = p_SnapshotMeta.m_Flags && GridSnapshot::Flags::MyData == (GridSnapshot::Flags::MyData & *p_SnapshotMeta.m_Flags);
			const wstring title = isMyData
				? _T("My Messages")
				: YtriaTranslate::Do(ModuleMessage_showMessages_1, _YLOC("Messages")).c_str();
			return new FrameMessages(isMyData, p_LicenseContext, p_SessionIdentifier, title, p_Module, p_HistoryMode, p_PreviousFrame);
		});
}

O365DataMap<BusinessUser, vector<BusinessMailFolder>> ModuleMessage::retrieveMessages(std::shared_ptr<BusinessObjectManager> p_BOM, const ModuleCriteria& p_Criteria, bool p_LoadUsersFromSQlCache, const ModuleOptions& p_Options, std::map<BusinessUser, std::shared_ptr<MessageListRequester>>& p_Requesters, YtriaTaskData p_TaskData) const
{
	ASSERT(Origin::User == p_Criteria.m_Origin || Origin::DeletedUser == p_Criteria.m_Origin);
	ASSERT(!p_LoadUsersFromSQlCache || Origin::User == p_Criteria.m_Origin);

	O365DataMap<BusinessUser, vector<BusinessMailFolder>> mailFolders;

	auto processUser = [&mailFolders, &p_BOM, &p_TaskData, &p_Options, &p_Requesters](BusinessUser& bu, const O365SubIdsContainer& p_SubIds, const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger)
	{
		vector<BusinessMailFolder> userMailFolders;
		if (!p_TaskData.IsCanceled())
		{
			ASSERT(p_SubIds.empty());
			if (p_SubIds.empty())
				userMailFolders = GetMailFolders(p_BOM, bu.GetID(), p_Options, p_Requesters, p_Logger, p_TaskData).GetTask().get();
		}

		if (p_TaskData.IsCanceled())
		{
			bu.SetFlags(bu.GetFlags() | BusinessObject::Flag::CANCELED);
			mailFolders[bu] = {};
		}
		else
		{
			mailFolders[bu] = std::move(userMailFolders);
		}
	};

    if (ModuleCriteria::UsedContainer::IDS == p_Criteria.m_UsedContainer)
    {
		auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("messages"), p_Criteria.m_IDs.size());

		auto process = [processUser, logger](auto& user)
		{
			processUser(user, {}, logger);
		};
		
		Util::ProcessSubUserItems(process, p_Criteria.m_IDs, p_Criteria.m_Origin, p_BOM->GetGraphCache(), p_LoadUsersFromSQlCache, logger, p_TaskData);
    }
    else if (ModuleCriteria::UsedContainer::SUBIDS == p_Criteria.m_UsedContainer)
    {
		auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("messages"), p_Criteria.m_SubIDs.size());

		auto process = [processUser, logger, &subIds = p_Criteria.m_SubIDs](auto& user)
		{
			processUser(user, subIds, logger);
		};

		O365IdsContainer userIds;
		std::transform(p_Criteria.m_SubIDs.cbegin(), p_Criteria.m_SubIDs.cend(),
			std::inserter(userIds, userIds.begin()),
			[](const auto& val)
			{ return val.first.GetId(); });

		Util::ProcessSubUserItems(process, userIds, p_Criteria.m_Origin, p_BOM->GetGraphCache(), p_LoadUsersFromSQlCache, logger, p_TaskData);
    }

    return mailFolders;
}

TaskWrapper<vector<BusinessMailFolder>> ModuleMessage::GetMailFolders(const std::shared_ptr<BusinessObjectManager>& p_Bom, const PooledString& p_UserId, const ModuleOptions& p_Options, std::map<BusinessUser, std::shared_ptr<MessageListRequester>>& p_Requesters, const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	MessagePropertySet properties;
	properties.SetMailPreview(p_Options.m_BodyPreview);
	properties.SetBodyContent(p_Options.m_BodyContent);
	properties.SetMailHeaders(p_Options.m_MailHeaders);

	BusinessUser user;
	user.m_Id = p_UserId;

	p_Requesters[user] = std::make_shared<MessageListRequester>(properties, p_UserId, p_Logger);
	auto& requester = p_Requesters.at(user);
	if (p_Options.m_CutOffDateTime)
		requester->SetCutOffDateTime(*p_Options.m_CutOffDateTime);
	requester->SetCustomFilter(p_Options.m_CustomFilter);

	return safeTaskCall(requester->Send(p_Bom->GetSapio365Session(), p_TaskData).Then([requester, p_UserId, p_TaskData, bom = p_Bom, logger = p_Logger, p_Options]()
		{
			// Now that we have all messages, request every mailFolders (top level).
			logger->SetLogMessage(_T("mail folders"));

			auto topMailFoldersReq = std::make_shared<MailFoldersTopLevelRequester>(p_UserId, logger);
			safeTaskCall(topMailFoldersReq->Send(bom->GetSapio365Session(), p_TaskData), bom->GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData).get();

			map<PooledString, BusinessMailFolder*> flatMailFolders; // Key: Mailfolder Id

			// Request children for every top mailfolder
			for (auto& mailFolder : topMailFoldersReq->GetData())
			{
				logger->SetContextualInfo(mailFolder.m_DisplayName ? *mailFolder.m_DisplayName : _YTEXT(""));
				flatMailFolders[mailFolder.GetID()] = &mailFolder;

				auto childrenMailFoldersReq = std::make_shared<ChildrenMailFoldersRequester>(p_UserId, mailFolder.GetID(), false, logger->Clone());
				safeTaskCall(childrenMailFoldersReq->Send(bom->GetSapio365Session(), p_TaskData), bom->GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData).get();

				mailFolder.m_ChildrenMailFolders = std::move(childrenMailFoldersReq->GetData());
				addToFlatMailFolders(flatMailFolders, mailFolder.m_ChildrenMailFolders);
			}

			// Merge messages into their respective mailfolder
			for (auto& message : requester->GetData())
			{
				if (message.m_ParentFolderId)
				{
					auto itt = flatMailFolders.find(*message.m_ParentFolderId);
					ASSERT(itt != flatMailFolders.end());
					if (itt != flatMailFolders.end())
					{
						message.m_ParentFolderName = (*itt->second).m_DisplayName;
						(*itt->second).m_Messages.push_back(std::move(message));
					}
				}
			}

			auto topMailFolderLogger = std::make_shared<BasicRequestLogger>(_YTEXT(""));
			topMailFolderLogger->SetContextualInfo(logger->GetContextualInfo());
			auto topMailFolderMessagesLogger = std::make_shared<BasicPageRequestLogger>(_T("messages"));
			auto subFoldersLogger = std::make_shared<BasicPageRequestLogger>(_T("mail subfolders"));

			if (p_Options.m_RequestSoftDeletedFolders)
			{
				topMailFolderLogger->SetLogMessage(_T("soft-deleted folder"));
				auto req = std::make_shared<MailFolderHierarchyRequester>(p_UserId, _YTEXT("recoverableitemsdeletions"), topMailFolderLogger, topMailFolderMessagesLogger, subFoldersLogger);
				safeTaskCall(req->Send(bom->GetSapio365Session(), p_TaskData), bom->GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData).get();
				topMailFoldersReq->GetData().push_back(req->GetData());
			}
			
			return topMailFoldersReq->GetData();
	}), p_Bom->GetMSGraphSession(Sapio365Session::APP_SESSION), ListErrorHandler<BusinessMailFolder>, p_TaskData);
}

void ModuleMessage::downloadInlineAttachments(const Command& p_Command)
{
	using DataType = std::pair<AttachmentsInfo, DownloadInlineAttachmentsCallback*>;
    const bool dataOk = p_Command.GetCommandInfo().Data().is_type<DataType>();
    ASSERT(dataOk);
    if (dataOk)
    {
        auto data = p_Command.GetCommandInfo().Data().get_value<DataType>();
        ModuleBase::downloadInlineAttachments(data.first, data.second, p_Command);
    }
}

void ModuleMessage::downloadAndSaveAsEml(const Command& p_Command)
{
	auto p_AutomationAction = p_Command.GetAutomationAction();
	auto info = p_Command.GetCommandInfo();

	FrameMessages* existingFrame = dynamic_cast<FrameMessages*>(info.GetFrame());

	if (info.Data().is_type<EmlDownloadInfo>())
	{
		auto ids = info.Data().get_value<EmlDownloadInfo>();
		downloadAndSaveAsEml(existingFrame->GetBusinessObjectManager(), ids, existingFrame, p_Command);
	}
	else
	{
		SetAutomationGreenLight(p_AutomationAction, p_Command.ToString() + _YERROR(" - invalid command"));
	}
}

void ModuleMessage::downloadAndSaveAsEml(std::shared_ptr<BusinessObjectManager> p_BOM, const EmlDownloadInfo& p_EmlDownloadInfo, FrameMessages* p_Frame, const Command& p_Command)
{
	ASSERT(nullptr != p_Frame);
	if (nullptr != p_Frame)
	{
		auto taskData = addFrameTask(_T("Save as EML"), p_Frame, p_Command, false);

		auto& p_AutomationSetup = p_Command.GetAutomationSetup();

		YSafeCreateTask([hwnd = p_Frame->GetSafeHwnd(), taskData, p_AutomationSetup, p_BOM, p_EmlDownloadInfo]()
		{
			auto& ids = p_EmlDownloadInfo.m_MessageIdAndPathByUser;
			size_t count = 0;
			for (const auto& byUser : ids)
				count += byUser.second.size();

			auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("message"), count);
			for (const auto& byUser : ids)
			{
				logger->SetContextualInfo(p_BOM->GetGraphCache().GetUserContextualInfo(byUser.first));

				for (const auto& messageAndPath : byUser.second)
				{
					const auto& messageId = messageAndPath.first;
					auto completeFilePath = messageAndPath.second;
					logger->IncrementObjCount();

					auto mimeContent = p_BOM->GetMessageMIMEContent(byUser.first, messageId, logger, taskData).GetTask().get();

					if (!FileUtil::CreateFolderHierarchy(completeFilePath))
					{
						const auto lastError = Str::convertFromUTF8(MFCUtil::GetLastErrorString());
						LoggerService::Debug(_YTEXTFORMAT(L"Error creating folder hierarchy for \"%s\": %s", completeFilePath.c_str(), lastError.c_str()));
					}
					else
					{
						if (p_EmlDownloadInfo.m_FileExistsAction != FileExistsAction::Skip || !FileUtil::FileExists2(completeFilePath))
						{
							if (p_EmlDownloadInfo.m_FileExistsAction != FileExistsAction::Overwrite)
								completeFilePath = FileUtil::GetNoOverwriteFileName(completeFilePath);

							// FIXME: Crashes when path too long (exception)
							std::ofstream file(completeFilePath, ios::out);
							file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
							file << Str::convertToUTF8(Str::replace(mimeContent, _YTEXT("\r\n"), _YTEXT("\n")));
							file.close();

							//return completeFilePath;
						}
					}
				}
			}

			YCallbackMessage::DoPost([taskData, hwnd, p_AutomationSetup, pathToOpen = p_EmlDownloadInfo.m_PathToOpen]()
			{
				if (!pathToOpen.empty())
					::ShellExecute(NULL, _YTEXT("open"), pathToOpen.c_str(), NULL, NULL, SW_SHOWNORMAL);

				if (::IsWindow(hwnd))
				{
					auto frame = dynamic_cast<FrameMessages*>(CWnd::FromHandle(hwnd));
					frame->GetGrid().ClearLog(taskData.GetId());
				}
				TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
				SetAutomationGreenLight(p_AutomationSetup.m_ActionToExecute);
			});
		});
	}
}
