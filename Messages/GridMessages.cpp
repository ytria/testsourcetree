#include "GridMessages.h"

#include "AttachmentsInfo.h"
#include "AutomationWizardMessages.h"
#include "BasicGridSetup.h"
#include "BusinessUser.h"
#include "DlgDownloadAttachments.h"
#include "DlgDownloadMessages.h"
#include "DlgExportSetup.h"
#include "DlgMessagesModuleOptions.h"
#include "FileUtil.h"
#include "FrameMessages.h"
#include "GridHelpers.h"
#include "GridUpdater.h"
#include "GridUtil.h"
#include "MessageAttachmentDeletion.h"
#include "MessageBodyData.h"
#include "MessageBodyViewer.h"
#include "MsGraphFieldNames.h"
#include "O365AdminUtil.h"
#include "ScopedTemporarySelection.h"
#include "SubItemsFlagGetter.h"
#include "YCallbackMessage.h"
#include "YFileDialog.h"

IMPLEMENT_CacheGrid_DeleteRowLParam(GridMessages, MessageBodyData)

BEGIN_MESSAGE_MAP(GridMessages, ModuleO365Grid<BusinessMessage>)
    ON_COMMAND(ID_MESSAGEGRID_SHOWBODY,					OnShowMessageBody)
	ON_COMMAND(ID_MESSAGEGRID_CHANGEOPTIONS,			OnChangeOptions)
    ON_COMMAND(ID_MESSAGEGRID_SHOWATTACHMENTS,			OnShowMessageAttachments)// LOAD MORE
    ON_COMMAND(ID_MESSAGEGRID_DOWNLOADATTACHMENTS,		OnDownloadMessageAttachments)
    ON_COMMAND(ID_MESSAGEGRID_VIEW_ITEM_ATTACHMENT,		OnViewItemAttachment)// PREVIEW
    ON_COMMAND(ID_MESSAGEGRID_DELETEATTACHMENT,			OnDeleteAttachment)
    ON_COMMAND(ID_MESSAGEGRID_TOGGLEINLINEATTACHMENTS,	OnToggleInlineAttachments)
	ON_COMMAND(ID_MESSAGEGRID_DOWNLOADASEML,			OnDownloadAsEml)

	ON_UPDATE_COMMAND_UI(ID_MESSAGEGRID_SHOWBODY,					OnUpdateShowMessageBody)
	ON_UPDATE_COMMAND_UI(ID_MESSAGEGRID_CHANGEOPTIONS,				OnUpdateChangeOptions)
    ON_UPDATE_COMMAND_UI(ID_MESSAGEGRID_SHOWATTACHMENTS,			OnUpdateShowMessageAttachments)
    ON_UPDATE_COMMAND_UI(ID_MESSAGEGRID_DOWNLOADATTACHMENTS,		OnUpdateDownloadMessageAttachments)
    ON_UPDATE_COMMAND_UI(ID_MESSAGEGRID_VIEW_ITEM_ATTACHMENT,		OnUpdateViewItemAttachment)
    ON_UPDATE_COMMAND_UI(ID_MESSAGEGRID_DELETEATTACHMENT,			OnUpdateDeleteAttachment)
    ON_UPDATE_COMMAND_UI(ID_MESSAGEGRID_TOGGLEINLINEATTACHMENTS,	OnUpdateToggleInlineAttachments)
	ON_UPDATE_COMMAND_UI(ID_MESSAGEGRID_DOWNLOADASEML,				OnUpdateDownloadAsEml)
END_MESSAGE_MAP()

GridMessages::GridMessages(bool p_IsMyData/* = false*/)
	: m_FrameMessages(nullptr)
	, m_ColHeaderName(nullptr)
	, m_ColHeaderValue(nullptr)
	, m_HasLoadMoreAdditionalInfo(false)
	, m_ShowInlineAttachments(false)
	, m_IsMyData(p_IsMyData)
	, m_ColChildFolderCount(nullptr)
	, m_ColTotalItemCount(nullptr)
	, m_ColUnreadItemCount(nullptr)
{
	m_WithOwnerAsTopAncestor = true; // FIXME: Should it be false in mydata?

	// FIXME: Add desired actions when the required methods are implemented below!
    UseDefaultActions(/*O365Grid::ACTION_CREATE | */O365Grid::ACTION_DELETE /*| O365Grid::ACTION_EDIT | O365Grid::ACTION_VIEW*/);
	EnableGridModifications();
    SetShowInlineAttachments(false);

	initWizard<AutomationWizardMessages>(_YTEXT("Automation\\Messages"));
	
	if (IsMyData())
		m_MyDataMeHandler.emplace();
}

GridMessages::~GridMessages()
{
	GetBackend().Clear();
}

ModuleCriteria GridMessages::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_FrameMessages->GetModuleCriteria();

    criteria.m_IDs.clear();
    for (auto row : p_Rows)
        criteria.m_IDs.insert(row->GetField(GetColMetaID()).GetValueStr());

    return criteria;
}

void GridMessages::customizeGrid()
{
	m_FrameMessages = dynamic_cast<FrameMessages*>(GetParentFrame());

	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_GRIDMESSAGES_ACCELERATOR));

	{
		BasicGridSetup setup(GridUtil::g_AutoNameMessages, LicenseUtil::g_codeSapio365messages,
		{
			{ &m_Template.m_ColumnDisplayName, _T("Display Name"), g_FamilyOwner },
			{ &m_Template.m_ColumnUserPrincipalName, g_TitleOwnerUserName, g_FamilyOwner },
			{ &m_Template.m_ColumnID, g_TitleOwnerID, g_FamilyOwner }
		}, { BusinessMessage() });

		setup.Setup(*this, false);
	}

	m_MetaColumns.push_back(GetColMetaID());
	m_MetaColumns.push_back(getColMetaDisplayName());
	m_MetaColumns.push_back(getColMetaUserPrincipalName());

	setBasicGridSetupHierarchy({ { { m_Template.m_ColumnUserPrincipalName, 0 } }
								,{ rttr::type::get<BusinessMessage>().get_id() }
								,{ rttr::type::get<BusinessMailFolder>().get_id(), rttr::type::get<BusinessMessage>().get_id(), rttr::type::get<BusinessUser>().get_id() } });

	/*Add additional meta columns*/
	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		for (const auto& c : *metaDataColumnInfos)
		{
			ASSERT(nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()));
			if (nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()))
			{
				auto col = c.AddTo(*this, g_FamilyOwner);
				m_Template.GetColumnForProperty(c.GetPropertyName()) = col;
				m_MetaColumns.push_back(col);
				m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
			}
		}
	}

	static const wstring g_FamilyParentFolder = YtriaTranslate::Do(GridMessages_customizeGrid_1, _YLOC("Parent Folder")).c_str();
	m_ColParentFolderName				= AddColumn(_YUID(O365_MESSAGE_PARENTFOLDER),						YtriaTranslate::Do(GridMessages_customizeGrid_2, _YLOC("Parent Folder")).c_str(),						g_FamilyParentFolder,	g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColParentFolderId					= AddColumn(_YUID(O365_MESSAGE_PARENTFOLDERID),						YtriaTranslate::Do(GridMessages_customizeGrid_3, _YLOC("Parent Folder ID")).c_str(),						g_FamilyParentFolder,	g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColInferenceClassification		= AddColumn(_YUID(O365_MESSAGE_INFERENCECLASSIFICATION),			YtriaTranslate::Do(GridMessages_customizeGrid_4, _YLOC("Classification")).c_str(),						g_FamilyInfo,			g_ColumnSize8,	{ g_ColumnsPresetDefault });
    m_ColIsDraft						= AddColumnCheckBox(_YUID(O365_MESSAGE_ISDRAFT),					YtriaTranslate::Do(GridMessages_customizeGrid_5, _YLOC("Draft")).c_str(),								g_FamilyInfo,			g_ColumnSize6,	{ g_ColumnsPresetDefault });
    m_ColFromName						= AddColumn(_YUID(O365_MESSAGE_FROMNAME),							YtriaTranslate::Do(GridMessages_customizeGrid_6, _YLOC("From - Name")).c_str(),							g_FamilyInfo,			g_ColumnSize22, { g_ColumnsPresetDefault });
    m_ColFromAddress					= AddColumn(_YUID(O365_MESSAGE_FROMADDRESS),						YtriaTranslate::Do(GridMessages_customizeGrid_7, _YLOC("From - Email")).c_str(),							g_FamilyInfo,			g_ColumnSize32);
    m_ColSenderName						= AddColumn(_YUID(O365_MESSAGE_SENDERNAME),							YtriaTranslate::Do(GridMessages_customizeGrid_8, _YLOC("Sender - Name")).c_str(),						g_FamilyInfo,			g_ColumnSize22);
    m_ColSenderAddress					= AddColumn(_YUID(O365_MESSAGE_SENDER),								YtriaTranslate::Do(GridMessages_customizeGrid_9, _YLOC("Sender - Email")).c_str(),						g_FamilyInfo,			g_ColumnSize32);
    m_ColSubject						= AddColumn(_YUID(O365_MESSAGE_SUBJECT),							YtriaTranslate::Do(GridMessages_customizeGrid_10, _YLOC("Subject")).c_str(),								g_FamilyInfo,			g_ColumnSize32, { g_ColumnsPresetDefault });
    m_ColSentDateTime					= AddColumnDate(_YUID(O365_MESSAGE_SENTDATETIME),					YtriaTranslate::Do(GridMessages_customizeGrid_11, _YLOC("Sent On")).c_str(),								g_FamilyInfo,			g_ColumnSize16, { g_ColumnsPresetDefault });
    m_ColReceivedDateTime				= AddColumnDate(_YUID(O365_MESSAGE_RECEIVEDDATETIME),				YtriaTranslate::Do(GridMessages_customizeGrid_12, _YLOC("Received On")).c_str(),							g_FamilyInfo,			g_ColumnSize16, { g_ColumnsPresetDefault });
    m_ColCreatedDate					= AddColumnDate(_YUID(O365_MESSAGE_CREATEDDATETIME),				YtriaTranslate::Do(GridMessages_customizeGrid_13, _YLOC("Created On")).c_str(),							g_FamilyInfo,			g_ColumnSize16, { g_ColumnsPresetDefault });
    m_ColLastModifiedDateTime			= AddColumnDate(_YUID(O365_MESSAGE_LASTMODIFIEDDATETIME),			YtriaTranslate::Do(GridMessages_customizeGrid_14, _YLOC("Last Modified On")).c_str(),						g_FamilyInfo,			g_ColumnSize16);
	static const wstring g_FamilyAllRecipients = YtriaTranslate::Do(GridMessages_customizeGrid_15, _YLOC("All Recipients")).c_str();
	m_ColToRecipientsNames				= AddColumn(_YUID(O365_MESSAGE_TORECIPIENTSNAMES),					YtriaTranslate::Do(GridMessages_customizeGrid_16, _YLOC("Recipients - Name")).c_str(),					g_FamilyAllRecipients,	g_ColumnSize22, { g_ColumnsPresetDefault });
    m_ColToRecipientsAddresses			= AddColumn(_YUID(O365_MESSAGE_TORECIPIENTSADDRESSES),				YtriaTranslate::Do(GridMessages_customizeGrid_17, _YLOC("Recipients - Email")).c_str(),					g_FamilyAllRecipients,	g_ColumnSize32);
    m_ColCcRecipientsNames				= AddColumn(_YUID(O365_MESSAGE_CCRECIPIENTSNAMES),					YtriaTranslate::Do(GridMessages_customizeGrid_18, _YLOC("Copy To - Name")).c_str(),						g_FamilyAllRecipients,	g_ColumnSize22, { g_ColumnsPresetDefault });
    m_ColCcRecipientsAddresses			= AddColumn(_YUID(O365_MESSAGE_CCRECIPIENTSADDRESSES),				YtriaTranslate::Do(GridMessages_customizeGrid_19, _YLOC("Copy To - Email")).c_str(),						g_FamilyAllRecipients,	g_ColumnSize32);
    m_ColBccRecipientsNames				= AddColumn(_YUID(O365_MESSAGE_BCCRECIPIENTSNAMES),					YtriaTranslate::Do(GridMessages_customizeGrid_20, _YLOC("BCC - Name")).c_str(),							g_FamilyAllRecipients,	g_ColumnSize22, { g_ColumnsPresetDefault });
    m_ColBccRecipientsAddresses			= AddColumn(_YUID(O365_MESSAGE_BCCRECIPIENTSADDRESSES),				YtriaTranslate::Do(GridMessages_customizeGrid_21, _YLOC("BCC - Email")).c_str(),							g_FamilyAllRecipients,	g_ColumnSize32);
    m_ColReplyToNames					= AddColumn(_YUID(O365_MESSAGE_REPLYTONAMES),						YtriaTranslate::Do(GridMessages_customizeGrid_22, _YLOC("Reply To - Name")).c_str(),						g_FamilyInfo,			g_ColumnSize22);
    m_ColReplyToAddresses				= AddColumn(_YUID(O365_MESSAGE_REPLYTOADDRESSES),					YtriaTranslate::Do(GridMessages_customizeGrid_23, _YLOC("Reply To - Email")).c_str(),						g_FamilyInfo,			g_ColumnSize32);
    m_ColBodyPreview					= AddColumnRichText(_YUID(O365_MESSAGE_BODYPREVIEW),				YtriaTranslate::Do(GridMessages_customizeGrid_24, _YLOC("Mail Preview")).c_str(),							g_FamilyInfo,			g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColBodyContentType				= AddColumn(_YUID(O365_MESSAGE_BODYCONTENTTYPE),					YtriaTranslate::Do(GridMessages_customizeGrid_25, _YLOC("Body Content Type")).c_str(),					g_FamilyInfo,			g_ColumnSize6);
    m_ColImportance						= AddColumn(_YUID(O365_MESSAGE_IMPORTANCE),							YtriaTranslate::Do(GridMessages_customizeGrid_26, _YLOC("Importance")).c_str(),							g_FamilyInfo,			g_ColumnSize8,	{ g_ColumnsPresetDefault });
    m_ColInternetMessageId				= AddColumn(_YUID(O365_MESSAGE_INTERNETMESSAGEID),					YtriaTranslate::Do(GridMessages_customizeGrid_27, _YLOC("Internet Message ID")).c_str(),					g_FamilyInfo,			g_ColumnSize12);
    m_ColIsRead							= AddColumnCheckBox(_YUID(O365_MESSAGE_ISREAD),						YtriaTranslate::Do(GridMessages_customizeGrid_28, _YLOC("Read")).c_str(),								    g_FamilyInfo,			g_ColumnSize6);
    m_ColIsReadReceiptRequested			= AddColumnCheckBox(_YUID(O365_MESSAGE_ISREADRECEIPTREQUESTED),		YtriaTranslate::Do(GridMessages_customizeGrid_29, _YLOC("Read Receipt Requested")).c_str(),			    g_FamilyInfo,			g_ColumnSize6);
    m_ColIsDeliveryReceiptRequested		= AddColumnCheckBox(_YUID(O365_MESSAGE_ISDELIVERYRECEIPTREQUESTED), YtriaTranslate::Do(GridMessages_customizeGrid_30, _YLOC("Delivery Receipt Requested")).c_str(),		    g_FamilyInfo,			g_ColumnSize6);
    m_ColCategories						= AddColumn(_YUID(O365_MESSAGE_CATEGORIES),							YtriaTranslate::Do(GridMessages_customizeGrid_31, _YLOC("Categories")).c_str(),							g_FamilyInfo,			g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColWebLink						= AddColumnHyperlink(_YUID(O365_MESSAGE_WEBLINK),					YtriaTranslate::Do(GridMessages_customizeGrid_32, _YLOC("URL")).c_str(),									g_FamilyInfo,			g_ColumnSize8,	{ g_ColumnsPresetDefault });
    m_ColChangeKey						= AddColumn(_YUID(O365_MESSAGE_CHANGEKEY),							YtriaTranslate::Do(GridMessages_customizeGrid_33, _YLOC("Change Key")).c_str(),							g_FamilyInfo,			g_ColumnSize12);
    m_ColConversationId					= AddColumn(_YUID(O365_MESSAGE_CONVERSATIONID),						YtriaTranslate::Do(GridMessages_customizeGrid_34, _YLOC("Conversation ID")).c_str(),						g_FamilyInfo,			g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColUniqueBodyContentType			= AddColumn(_YUID(O365_MESSAGE_UNIQUEBODYCONTENTTYPE),				YtriaTranslate::Do(GridMessages_customizeGrid_35, _YLOC("Unique body content type")).c_str(),				g_FamilyInfo,			g_ColumnSize6);
	m_ColChildFolderCount				= AddColumnNumber(_YUID(O365_MAILFOLDER_CHILDFOLDERCOUNT),			YtriaTranslate::Do(GridMailFolders_customizeGrid_13, _YLOC("Child Folder Count")).c_str(),				g_FamilyInfo,			g_ColumnSize6, { g_ColumnsPresetDefault });
	m_ColTotalItemCount					= AddColumnNumber(_YUID(O365_MAILFOLDER_TOTALITEMCOUNT),			YtriaTranslate::Do(GridMailFolders_customizeGrid_14, _YLOC("Total item count")).c_str(),				g_FamilyInfo,			g_ColumnSize6, { g_ColumnsPresetDefault });
	m_ColUnreadItemCount				= AddColumnNumber(_YUID(O365_MAILFOLDER_UNREADITEMCOUNT),			YtriaTranslate::Do(GridMailFolders_customizeGrid_15, _YLOC("Unread item count")).c_str(),				g_FamilyInfo,			g_ColumnSize6, { g_ColumnsPresetDefault });

	static const wstring g_FamilyHeader = YtriaTranslate::Do(GridMessages_customizeGrid_58, _YLOC("Headers")).c_str();
	m_ColHeaderName = AddColumn(_YUID("internetMessageHeaders.name"), YtriaTranslate::Do(GridMessages_customizeGrid_59, _YLOC("Header Name")).c_str(), g_FamilyHeader, g_ColumnSize12);
	m_ColHeaderValue = AddColumn(_YUID("internetMessageHeaders.value"), YtriaTranslate::Do(GridMessages_customizeGrid_60, _YLOC("Header Value")).c_str(), g_FamilyHeader, g_ColumnSize12);

	ASSERT(m_FrameMessages->GetOptions());
	if (m_FrameMessages->GetOptions() && !m_FrameMessages->GetOptions()->m_MailHeaders)
	{
		m_ColHeaderName->SetPresetFlags(static_cast<UINT>(CacheGrid::g_ColumnsPresetTech));
		m_ColHeaderValue->SetPresetFlags(static_cast<UINT>(CacheGrid::g_ColumnsPresetTech));
	}

	addColumnGraphID();// added here to not break the main category

	static const wstring g_FamilyAttachment = YtriaTranslate::Do(GridMessages_customizeGrid_36, _YLOC("Attachment")).c_str();
	m_ColHasGraphAttachment				= AddColumnCheckBox(_YUID(O365_MESSAGE_HASATTACHMENTS),				YtriaTranslate::Do(GridMessages_customizeGrid_37, _YLOC("Has Attachment")).c_str(),						g_FamilyAttachment,		g_ColumnSize6, { g_ColumnsPresetDefault });
	addColumnIsLoadMore(_YUID("loaded"), YtriaTranslate::Do(GridMessages_customizeGrid_38, _YLOC("Status - Load Attachment Info")).c_str());
    m_ColHasAnyAttachment               = AddColumnCheckBox(_YUID("containsAttachments"),					YtriaTranslate::Do(GridMessages_customizeGrid_39, _YLOC("Contains Attachment incl. inline")).c_str(),   g_FamilyAttachment,     g_ColumnSize6);
    m_ColAttachmentType					= AddColumn(_YUID("attachment.type"),								YtriaTranslate::Do(GridMessages_customizeGrid_40, _YLOC("Type")).c_str(),					            g_FamilyAttachment,		g_ColumnSize12, { g_ColumnsPresetDefault });
	SetColumnPresetFlags(AddColumnFileType(_T("Icon"), g_FamilyAttachment), { g_ColumnsPresetDefault });
    m_ColAttachmentName					= AddColumn(_YUID("attachment.name"),								YtriaTranslate::Do(GridMessages_customizeGrid_41, _YLOC("Name")).c_str(),					            g_FamilyAttachment,		g_ColumnSize32, { g_ColumnsPresetDefault });
	if (nullptr != m_ColAttachmentName)
		m_ColAttachmentName->SetManageFileFormat(true);
	m_ColAttachmentNameExtOnly = AddColumn(_YUID("ATTACHMENTNAMEEXTONLY"), _T("Extension"), g_FamilyAttachment, g_ColumnSize6);
    m_ColAttachmentContentType			= AddColumn(_YUID("attachment.contentType"),						YtriaTranslate::Do(GridMessages_customizeGrid_42, _YLOC("Content Type")).c_str(),			            g_FamilyAttachment,		g_ColumnSize12);
	//m_ColAttachmentContentId			= AddColumn(_YUID("attachment.contentId"), _YTEXT("Content ID"), g_FamilyAttachment, g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColAttachmentSize					= AddColumnNumber(_YUID("attachment.size"),							YtriaTranslate::Do(GridMessages_customizeGrid_43, _YLOC("Size")).c_str(), 					            g_FamilyAttachment,		g_ColumnSize8,	{ g_ColumnsPresetDefault });
    m_ColAttachmentLastModifiedDateTime = AddColumnDate(_YUID("attachment.lastModifiedDateTime"),			YtriaTranslate::Do(GridMessages_customizeGrid_44, _YLOC("Last Modified Date")).c_str(),			        g_FamilyAttachment,		g_ColumnSize16);
    m_ColAttachmentIsInline				= AddColumnCheckBox(_YUID("attachment.isInline"),					YtriaTranslate::Do(GridMessages_customizeGrid_45, _YLOC("Inline")).c_str(),				                g_FamilyAttachment,		g_ColumnSize6);
	m_ColAttachmentIsInline->SetDisplayedColumnType(YtriaTranslate::Do(GridMessages_customizeGrid_46, _YLOC("Boolean")).c_str());
	m_ColAttachmentId					= AddColumn(_YUID("attachment.id"),									YtriaTranslate::Do(GridMessages_customizeGrid_49, _YLOC("Attachment ID")).c_str(),						g_FamilyAttachment,		g_ColumnSize12, { g_ColumnsPresetTech });

	AddColumnForRowPK(m_Template.m_ColumnID);
	AddColumnForRowPK(GetColId());

	m_ColToRecipientsNames->SetMultivalueFamily(YtriaTranslate::Do(GridMessages_customizeGrid_52, _YLOC("Recipients")).c_str());
	m_ColToRecipientsNames->AddMultiValueExplosionSister(m_ColToRecipientsAddresses);
	m_ColReplyToNames->SetMultivalueFamily(YtriaTranslate::Do(GridMessages_customizeGrid_53, _YLOC("ReplyTo")).c_str());
	m_ColReplyToNames->AddMultiValueExplosionSister(m_ColReplyToAddresses);
	m_ColCcRecipientsNames->SetMultivalueFamily(YtriaTranslate::Do(GridMessages_customizeGrid_54, _YLOC("CopyTo")).c_str());
	m_ColCcRecipientsNames->AddMultiValueExplosionSister(m_ColCcRecipientsAddresses);
	m_ColBccRecipientsNames->SetMultivalueFamily(YtriaTranslate::Do(GridMessages_customizeGrid_55, _YLOC("BCC")).c_str());
	m_ColBccRecipientsNames->AddMultiValueExplosionSister(m_ColBccRecipientsAddresses);

	// FIXME: We need to be sure that adding another explosion family in this grid won't break attachments edition...
	if (nullptr != m_ColHeaderName)
	{
		m_ColHeaderName->SetMultivalueFamily(YtriaTranslate::Do(GridMessages_customizeGrid_57, _YLOC("Headers")).c_str());
		m_ColHeaderName->AddMultiValueExplosionSister(m_ColHeaderValue);
	}

    m_ColAttachmentSize->SetXBytesColumnFormat();

	// This column will be the elder of the sisterhood. Keep it the same as the one used in row flagger to identify subitems. 
	m_ColAttachmentId->SetMultivalueFamily(YtriaTranslate::Do(GridMessages_customizeGrid_56, _YLOC("Attachments")).c_str());
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentName);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentType);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentContentType);
	//m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentContentId);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentSize);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentLastModifiedDateTime);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentIsInline);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentNameExtOnly);
	m_ColAttachmentId->AddMultiValueExplosionSister(GetColumnFileType());

	m_Template.CustomizeGrid(*this);

    SetColumnsPresetFlags({ m_ColHasAnyAttachment,
                            m_ColAttachmentType,
                            m_ColAttachmentId,
                            m_ColAttachmentName,
                            m_ColAttachmentContentType,
							//m_ColAttachmentContentId,
                            m_ColAttachmentSize,
                            m_ColAttachmentLastModifiedDateTime,
                            m_ColAttachmentIsInline,
							m_ColAttachmentNameExtOnly,
							GetColumnFileType() },
						  { g_ColumnsPresetMore });

	addTopCategories(YtriaTranslate::Do(GridMessages_customizeGrid_50, _YLOC("Message Info")).c_str(), YtriaTranslate::Do(GridMessages_customizeGrid_51, _YLOC("Attachment Info")).c_str(), true);

    m_Icons			= GridUtil::LoadAttachmentIcons(*this);
	m_MessageViewer = std::make_unique<MessageBodyViewer>(*this, m_ColBodyContentType, m_ColSubject, m_ColIsDraft);
    SetRowFlagger(std::make_unique<SubItemsFlagGetter>(GetModifications(), m_ColAttachmentId->GetMultiValueElderColumn()));

	// MessageListRequester add an orderBy in message request, no need to filter here
	// Plus, it fixes Bug #200115.SR.00B312 (exploded rows order)
	/*if (m_FrameMessages->GetOptions().m_CutOffDateTime)
		AddSorting(m_ColReceivedDateTime, GridBackendUtil::DESC);*/

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_FrameMessages);
}

wstring GridMessages::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Owner"), getColMetaDisplayName() } },
		_YFORMAT(L"%s (sent on %s)", p_Row->GetField(m_ColSubject).ToString().c_str(), p_Row->GetField(m_ColSentDateTime).ToString().c_str()));
}

void GridMessages::customizeGridPostProcess()
{
	O365Grid::customizeGridPostProcess();
	SetNoFieldTextAndIcon(GetColumnsByPresets({ g_ColumnsPresetMore }), IDB_ICON_LM_FILE_ATTACHMENT, YtriaTranslate::Do(GridMessages_customizeGridPostProcess_1, _YLOC("To see this information, use the 'Load Attachment Info' button")).c_str(), YtriaTranslate::Do(GridMessages_customizeGridPostProcess_2, _YLOC(" - Use the 'Load Attachment Info' button to display additional information - ")).c_str());
	SetLoadMoreStatus(true,		YtriaTranslate::Do(GridMessages_customizeGridPostProcess_3, _YLOC("Loaded")).c_str(),		IDB_ICON_LMSTATUS_LOADED_ATTACHMENTS);
	SetLoadMoreStatus(false,	YtriaTranslate::Do(GridMessages_customizeGridPostProcess_4, _YLOC("Not loaded")).c_str(),	IDB_ICON_LMSTATUS_NOTLOADED_ATTACHMENTS);
}

void GridMessages::BuildTreeViewAttachements(const O365DataMap<BusinessUser, vector<BusinessMailFolder>>& p_MailFolders, Origin p_Origin, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_FullPurge, bool p_NoPurge, const ModuleBase::AttachmentsContainer& p_BusinessAttachments)
{
	ASSERT(!IsMyData() || p_MailFolders.size() == 1);

	m_Origin = p_Origin;

	// Full purge with updates will fail.
	ASSERT(!p_FullPurge || p_Updates.empty());

	const uint32_t options = (p_Refresh ? GridUpdaterOptions::UPDATE_GRID : 0)
		| (p_NoPurge ? 0 : (p_FullPurge || IsMyData() ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE))
		| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		| GridUpdaterOptions::REFRESH_PROCESS_DATA
		/*| GridUpdaterOptions::PROCESS_LOADMORE*/
		;

	GridUpdater updater(*this, GridUpdaterOptions(options), p_Updates);
	GridIgnoreModification ignoramus(*this);

	GetRowIndex().SetResetRowInitIndex(true);

	for (const auto& keyVal : p_MailFolders)
	{
		const auto& user = keyVal.first;
		const auto& mailFolders = keyVal.second;

		if (m_HasLoadMoreAdditionalInfo)
		{
			if (keyVal.first.IsMoreLoaded())
				m_MetaIDsMoreLoaded.insert(user.GetID());
			else
				m_MetaIDsMoreLoaded.erase(user.GetID());
		}

		auto userRow = m_Template.AddRow(*this, user, {}, updater, false, true);
		ASSERT(nullptr != userRow);
		if (nullptr != userRow)
		{
			if (!IsMyData()) // Row will be deleted below
			{
				updater.GetOptions().AddRowWithRefreshedValues(userRow);
				updater.GetOptions().AddPartialPurgeParentRow(userRow);
			}
			userRow->SetHierarchyParentToHide(true);

			if (!user.HasFlag(BusinessObject::CANCELED))
			{
				if (mailFolders.size() == 1 && mailFolders[0].GetError())
				{
					updater.OnLoadingError(userRow, *mailFolders[0].GetError());
				}
				else
				{
					// Clear previous error
					if (userRow->IsError())
						userRow->ClearStatus();

					// Recursively display mailFolder content
					for (const auto& mailFolder : mailFolders)
						BuildMailFolderView(mailFolder, *userRow, updater);
				}
			}

			AddRoleDelegationFlag(userRow, user);
			if (IsMyData())
			{
				ASSERT(m_MyDataMeHandler);
				m_MyDataMeHandler->GetRidOfMeRow(*this, userRow);
			}
		}
	}

	loadAttachments(p_BusinessAttachments, updater);

	GetRowIndex().SetResetRowInitIndex(false);
}

void GridMessages::BuildMailFolderView(const BusinessMailFolder& p_MailFolder, GridBackendRow& p_ParentRow, GridUpdater& p_Updater)
{
	GridBackendField fID(p_MailFolder.GetID(), GetColId());
	GridBackendRow* folderRow = m_RowIndex.GetRow({ p_ParentRow.GetField(m_Template.m_ColumnID), fID }, true, true);
	ASSERT(nullptr != folderRow);
	if (nullptr != folderRow)
	{
		p_Updater.GetOptions().AddRowWithRefreshedValues(folderRow);

		folderRow->AddField(0, m_ColTotalItemCount).RemoveModified();
		folderRow->AddField(0, m_ColUnreadItemCount).RemoveModified();
		
		fillMailFolderRow(p_MailFolder, *folderRow);

		folderRow->SetParentRow(&p_ParentRow);
		for (const auto& message : p_MailFolder.m_Messages)
		{
			auto messageRow = fillMessageRow(folderRow, message);
			ASSERT(nullptr != messageRow);
			if (nullptr != messageRow)
			{
				p_Updater.GetOptions().AddRowWithRefreshedValues(messageRow);
			}
		}

		for (const auto& subFolder : p_MailFolder.m_ChildrenMailFolders)
			BuildMailFolderView(subFolder, *folderRow, p_Updater);
	}
}

void GridMessages::BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessMailFolder>>& p_Messages, Origin p_Origin, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_FullPurge, bool p_NoPurge)
{
	BuildTreeViewAttachements(p_Messages, p_Origin, p_Updates, p_Refresh, p_FullPurge, p_NoPurge, {});
}

void GridMessages::loadAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, GridUpdater& updater)
{
	for (const auto& byUser : p_BusinessAttachments)
	{
		GridBackendField fMetaID(byUser.first, GetColMetaID()); // user

		for (const auto& keyVal : byUser.second)
		{
			const auto& attachments = keyVal.second;

			vector<GridBackendField> pk = { fMetaID, {keyVal.first, GetColId()} };

			GridBackendRow* row = GetRowIndex().GetExistingRow(pk, updater);

			// Row might not exist anymore if the corresponding message has been deleted
			if (nullptr != row)
			{
				updater.GetOptions().AddRowWithRefreshedValues(row);

				if (attachments.size() == 1 && attachments[0].GetError() != boost::none)
				{
					ClearFieldsByPreset(row, O365Grid::g_ColumnsPresetMore);
					updater.OnLoadingError(row, *attachments[0].GetError());
				}
				else
				{
					// Should we?
					// Clear previous error
					// NB: this is commented out in GridEvents && GridPosts

					if (row->IsError())
						row->ClearStatus();

					fillAttachmentFields(row, attachments);
				}
			}
		}
	}
}

void GridMessages::ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_RefreshModificationStates, bool p_ShowPostUpdateError, bool p_UpdateGrid)
{
    ModuleUtil::MergeAttachments(m_AllAttachments, p_BusinessAttachments);

	{
		GridIgnoreModification ignoramus(*this);

		const uint32_t options = (p_UpdateGrid ? GridUpdaterOptions::UPDATE_GRID : 0)
			/*| GridUpdaterOptions::FULLPURGE*/
			| (p_RefreshModificationStates ? GridUpdaterOptions::REFRESH_MODIFICATION_STATES : 0)
			| (p_ShowPostUpdateError ? GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS : 0)
			/*| GridUpdaterOptions::REFRESH_PROCESS_DATA*/
			| GridUpdaterOptions::PROCESS_LOADMORE;

		GridUpdaterOptions updateOptions(options, GetColumnsByPresets({ g_ColumnsPresetMore }));
		GridUpdater updater(*this, updateOptions);

		loadAttachments(p_BusinessAttachments, updater);
	}
}

void GridMessages::OnShowMessageBody()
{
	ASSERT(nullptr != m_MessageViewer);
	if (nullptr != m_MessageViewer)
		m_MessageViewer->ToggleView();
}

void GridMessages::OnShowMessageAttachments()
{
	std::vector<GridBackendRow*> selectedRows;
	GetSelectedNonGroupRows(selectedRows);
	loadMoreImpl(selectedRows);
}

void GridMessages::OnChangeOptions()
{
	if (hasNoTaskRunning() && IsFrameConnected())
	{
		ASSERT(m_FrameMessages->GetOptions());
		DlgMessagesModuleOptions dlgOptions(m_FrameMessages, GetSession(), m_FrameMessages->GetOptions() ? *m_FrameMessages->GetOptions() : ModuleOptions());
		if (dlgOptions.DoModal() == IDOK)
		{
			CommandInfo info;
			info.SetFrame(m_FrameMessages);
			info.SetOrigin(GetOrigin());
			info.SetRBACPrivilege(m_FrameMessages->GetModuleCriteria().m_Privilege);
			info.Data2() = dlgOptions.GetOptions();

			// FIXME: Automation action name !!!
			Command command(m_FrameMessages->GetLicenseContext(), m_FrameMessages->GetSafeHwnd(), Command::ModuleTarget::Message, Command::ModuleTask::List, info, { this, g_ActionNameRefreshAll, GetAutomationActionRecording(), nullptr });
			CommandDispatcher::GetInstance().Execute(command);
		}
	}
}

std::set<AttachmentsInfo::IDs> GridMessages::GetAttachmentsRequestInfo(const std::vector<GridBackendRow*>& p_Rows, const O365IdsContainer& p_MessageIdsFilter)
{
	std::set<AttachmentsInfo::IDs> ids;

    for (auto& row : p_Rows)
    {
        if (GridUtil::isBusinessType<BusinessMessage>(row))
        {
            const PooledString strMetaId = row->GetField(GetColMetaID()).GetValueStr();
            if (!strMetaId.IsEmpty())
            {
				const PooledString strMessageId = row->GetField(GetColId()).GetValueStr();
				const PooledString subject = row->GetField(m_ColSubject).GetValueStr();
                ASSERT(!strMessageId.IsEmpty());
                if (p_MessageIdsFilter.empty() || p_MessageIdsFilter.find(strMessageId) != p_MessageIdsFilter.end())
                    ids.emplace(strMetaId, strMessageId, PooledString(), subject);
            }
        }
    }

    return ids;
}

void GridMessages::SetShowInlineAttachments(const bool& p_Val)
{
    m_ShowInlineAttachments = p_Val;
}

void GridMessages::OnDownloadMessageAttachments()
{
    DlgDownloadAttachments dlg(this, DlgDownloadAttachments::CONTEXT::MESSAGE);
    if (dlg.DoModal() != IDOK)
        return;

    AttachmentsInfo attachmentsMetaInfo;
    attachmentsMetaInfo.m_FileExistsAction		= dlg.GetFileExistsAction();
    attachmentsMetaInfo.m_DownloadFolderName	= dlg.GetFolderPath();
    attachmentsMetaInfo.m_OpenFolder			= dlg.GetOpenFolder();

	std::set<AttachmentsInfo::IDs> alreadyProcessedAttachmentIDs;
    for (auto row : GetBackend().GetSelectedRowsInOrder())
    {
        if (GridUtil::isBusinessType<BusinessMessage>(row) && O365Grid::IsAuthorizedByRoleDelegation(row->GetTopAncestor(), RoleDelegationUtil::RBAC_USER_MESSAGES_ATTACHMENTS_DOWNLOAD))
        {
			if (row->GetField(m_ColAttachmentId).HasValue())
			{
				const PooledString userId = row->GetField(GetColMetaID()).GetValueStr();
				const PooledString messageId = row->GetField(GetColId()).GetValueStr();

				wstring columnsPath;
				if (dlg.GetUseOwnerName())
				{
					columnsPath.append(row->GetField(m_Template.m_ColumnDisplayName).GetValueStr());
					columnsPath.append(_YTEXT("\\"));
				}
				if (dlg.GetUseMFHierarchy())
				{
					columnsPath.append(row->GetField(m_ColParentFolderName).GetValueStr());
					columnsPath.append(_YTEXT("\\"));
				}
				columnsPath.append(GridUtil::GetColumnsPath(*this, row, dlg.GetSelectedColumnsTitleDisplayed()));

				if (nullptr != row->GetField(m_ColAttachmentId).GetValuesMulti<PooledString>())
				{
					auto attachmentIds = row->GetField(m_ColAttachmentId).GetValuesMulti<PooledString>();
					auto attachmentTypes = row->GetField(m_ColAttachmentType).GetValuesMulti<PooledString>();
					ASSERT(nullptr != attachmentIds && nullptr != attachmentTypes);
					if (nullptr != attachmentIds && nullptr != attachmentTypes)
					{
						ASSERT(attachmentTypes->size() == attachmentIds->size());
						if (attachmentTypes->size() == attachmentIds->size())
						{
							size_t index = 0;
							for (const auto& attachmentId : *attachmentIds)
							{
								if ((*attachmentTypes)[index] == Util::GetFileAttachmentStr())
								{
									AttachmentsInfo::Infos infos;
									infos.m_IDs.UserOrGroupID() = userId;
									infos.m_IDs.EventOrMessageOrConversationID() = messageId;
									infos.m_IDs.AttachmentID() = attachmentId;

									if (alreadyProcessedAttachmentIDs.insert(infos.m_IDs).second)
									{
										infos.m_ColumnsPath = columnsPath;
										attachmentsMetaInfo.m_AttachmentInfos.push_back(infos);
									}
								}
								++index;
							}
						}
					}
				}
				else
				{
					const wstring attachmentId = row->GetField(m_ColAttachmentId).GetValueStr();
					const wstring attachmentType = row->GetField(m_ColAttachmentType).GetValueStr();
					if (attachmentType == Util::GetFileAttachmentStr())
					{
						AttachmentsInfo::Infos infos;
						infos.m_IDs.UserOrGroupID() = userId;
						infos.m_IDs.EventOrMessageOrConversationID() = messageId;
						infos.m_IDs.AttachmentID() = attachmentId;
						if (alreadyProcessedAttachmentIDs.insert(infos.m_IDs).second)
						{
							infos.m_ColumnsPath = columnsPath;
							attachmentsMetaInfo.m_AttachmentInfos.push_back(infos);
						}
					}
				}
			}
        }
    }

	CommandInfo info;
    info.Data() = attachmentsMetaInfo;
    info.SetFrame(m_FrameMessages);
	info.SetRBACPrivilege(m_FrameMessages->GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Message, Command::ModuleTask::DownloadAttachments, info, { this, g_ActionNameSelectedAttachmentDownload, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
}

void GridMessages::OnViewItemAttachment()
{
	CommandInfo info;

	ASSERT(GetSelectedRows().size() == 1);
	if (GetSelectedRows().size() == 1)
	{
		auto& row = *(GetSelectedRows().begin());

		GridBackendField& idField			= row->GetField(GetColMetaID());
		GridBackendField& eventIdField		= row->GetField(GetColId());
		GridBackendField& attachmentIdField = row->GetField(m_ColAttachmentId);
		GridBackendField& subjectField = row->GetField(m_ColSubject);

		if (attachmentIdField.HasValue() && eventIdField.HasValue() && idField.HasValue())
		{
			info.Data() = AttachmentsInfo::IDs(idField.GetValueStr(), eventIdField.GetValueStr(), attachmentIdField.GetValueStr(), subjectField.GetValueStr());
			info.SetFrame(m_FrameMessages);
			info.SetOrigin(m_Origin);
			info.SetRBACPrivilege(m_FrameMessages->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Message, Command::ModuleTask::ViewItemAttachment, info, { this, g_ActionNameSelectedPreviewItem, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
	}
}

void GridMessages::OnDeleteAttachment()
{
	const auto deleteAttachment = [this](GridBackendRow* p_Row, const PooledString& p_attId)
	{
		AttachmentInfo info(*this, p_attId, p_Row);

		auto deletion = std::make_unique<MessageAttachmentDeletion>(*this, info);
		deletion->ShowAppliedLocally(p_Row);
		if (GetModifications().Add(std::move(deletion)))
		{
			// Successfully added deletion? Remove error flag in case we just overwrote an old error with the new deletion
			p_Row->SetEditError(false);
		}
	};

	std::map<GridBackendRow*, bool> alreadyCheckedAncestors;
	boost::YOpt<bool> shouldRevertExistingModifications;
	const auto checkExistingMod = [this, &alreadyCheckedAncestors, &shouldRevertExistingModifications](GridBackendRow* p_Row)
	{
		if (nullptr != p_Row && alreadyCheckedAncestors.end() == alreadyCheckedAncestors.find(p_Row))
		{
			bool proceedWithDeletion = true;
			row_pk_t pk = GetRowPKNoDynamicColumns(*p_Row);

			auto mod = GetModifications().GetRowModification<RowModification>(pk);

			if (nullptr != mod)
			{
				if (!shouldRevertExistingModifications)
				{
					YCodeJockMessageBox errorMessage(this,
						DlgMessageBox::eIcon_Question,
						YtriaTranslate::Do(GridMessages_OnDeleteAttachment_1, _YLOC("Delete Attachments")).c_str(),
						YtriaTranslate::Do(GridMessages_OnDeleteAttachment_2, _YLOC("Some message are flagged for delete. Do you want to restore them and delete attachments instead?")).c_str(),
						_YTEXT(""),
						{ { IDOK, YtriaTranslate::Do(GridMessages_OnDeleteAttachment_3, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridMessages_OnDeleteAttachment_4, _YLOC("No")).c_str() } });

					shouldRevertExistingModifications = errorMessage.DoModal() == IDOK;
				}

				ASSERT(shouldRevertExistingModifications);
				if (shouldRevertExistingModifications && *shouldRevertExistingModifications)
				{
					GetModifications().RevertRowModifications(pk);
					proceedWithDeletion = true;
				}
				else
				{
					proceedWithDeletion = false;
				}
			}

			alreadyCheckedAncestors[p_Row] = proceedWithDeletion;
		}

		return alreadyCheckedAncestors[p_Row];
	};

	std::vector<GridBackendRow*> rows;
	GetSelectedNonGroupRows(rows);
	TemporarilyImplodeOutOfSisterhoodColumns(*this, m_ColAttachmentId, rows, [=](const bool p_HasRowsToReExplode)
	{
		for (auto row : GetSelectedRows())
		{
			if (GridUtil::IsBusinessMessage(row) && O365Grid::IsAuthorizedByRoleDelegation(row->GetTopAncestor(), RoleDelegationUtil::RBAC_USER_MESSAGES_ATTACHMENTS_EDIT))
			{
				auto& field = row->GetField(m_ColAttachmentId);
				if (field.HasValue())
				{
					if (GetMultivalueManager().IsRowExploded(row, m_ColAttachmentId))
					{
						ASSERT(row->IsExplosionFragment());
						ASSERT(!field.IsMulti());
						if (field.HasValue())
						{
							auto topRow = GetMultivalueManager().GetTopSourceRow(row);
							if (checkExistingMod(topRow))
								deleteAttachment(row, field.GetValueStr());
						}
					}
					else
					{
						if (field.IsMulti())
						{
							std::vector<PooledString>* attachmentIds = row->GetField(m_ColAttachmentId).GetValuesMulti<PooledString>();
							ASSERT(nullptr != attachmentIds);
							if (nullptr != attachmentIds)
							{
								if (checkExistingMod(row))
								{
									for (const auto& attachmentId : *attachmentIds)
										deleteAttachment(row, attachmentId);
								}
							}
						}
						else
						{
							if (checkExistingMod(row))
								deleteAttachment(row, field.GetValueStr());
						}
					}

					GetModifications().ShowSubItemModificationAppliedLocally(row);
				}
			}
		}

		if (!p_HasRowsToReExplode)
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	})();
}

void GridMessages::OnToggleInlineAttachments()
{
    if (!m_ShowInlineAttachments)
    {
		bool proceed = true;
		if (!GetModifications().GetSubItemDeletions().empty())
		{
			YCodeJockMessageBox dlg(this,
				DlgMessageBox::eIcon_ExclamationWarning,
				YtriaTranslate::Do(GridMessages_OnToggleInlineAttachments_3, _YLOC("Show Inline Attachments")).c_str(),
				YtriaTranslate::Do(GridMessages_OnToggleInlineAttachments_1, _YLOC("Some attachments are marked for deletions. Those unsaved modifications will be undone.")).c_str(),
				YtriaTranslate::Do(GridMessages_OnToggleInlineAttachments_2, _YLOC("If you want to keep them, you can cancel and save them before showing inline attachments.")).c_str(),
				{ { IDOK, YtriaTranslate::Do(GridMessages_OnToggleInlineAttachments_4, _YLOC("OK")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridMessages_OnToggleInlineAttachments_5, _YLOC("Cancel")).c_str() } });

			proceed = IDOK == dlg.DoModal();
			if (proceed)
				GetModifications().RevertAllSubItemModifications();
		}

		if (proceed)
		{
			TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::ALL_ROWS, [this](const bool p_HasRowsToReExplode)
			{
				SetShowInlineAttachments(true);
				ModuleUtil::ClearModifiedRowsStatus(*this);
				ViewAttachments(m_AllAttachments, false, false, !p_HasRowsToReExplode);
			})();
		}
    }
}

void GridMessages::OnDownloadAsEml()
{
	DlgExportMessage dlg(this);
	if (dlg.DoModal() != IDOK)
		return;

	EmlDownloadInfo emlDownloadInfo;

	const auto& folderPath = dlg.GetFolderPath();
	ASSERT(Str::endsWith(folderPath, _YTEXT("\\")));
	emlDownloadInfo.m_FileExistsAction = dlg.GetFileExistsAction();
	if (dlg.GetOpenFolder())
		emlDownloadInfo.m_PathToOpen = folderPath;

	for (auto row : GetSelectedRows())
	{
		if (GridUtil::isBusinessType<BusinessMessage>(row))
		{
			wstring path = folderPath;
			if (dlg.GetUseOwnerName())
			{
				path.append(row->GetField(m_Template.m_ColumnDisplayName).GetValueStr());
				path.append(_YTEXT("\\"));
			}
			if (dlg.GetUseMFHierarchy())
			{
				path.append(row->GetField(m_ColParentFolderName).GetValueStr());
				path.append(_YTEXT("\\"));
			}
			auto columnsPath = GridUtil::GetColumnsPath(*this, row, dlg.GetSelectedColumnsTitleDisplayed());
			if (!columnsPath.empty())
			{
				path.append(columnsPath);
				path.append(_YTEXT("\\"));
			}

			ASSERT(row->GetField(m_ColSubject).HasValue());
			if (row->GetField(m_ColSubject).HasValue())
			{
				wstring fileName = row->GetField(m_ColSubject).GetValueStr();
				FileUtil::MakeFileNameValid(fileName, false);
				path += fileName + _YTEXT(".eml");
			}

			emlDownloadInfo.m_MessageIdAndPathByUser[row->GetField(GetColMetaID()).GetValueStr()].emplace_back(row->GetField(GetColId()).GetValueStr(), path);
		}
	}

	if (!emlDownloadInfo.m_MessageIdAndPathByUser.empty())
	{
		CommandInfo info;
		info.Data() = emlDownloadInfo;
		info.SetFrame(m_FrameMessages);
		info.SetRBACPrivilege(m_FrameMessages->GetModuleCriteria().m_Privilege);
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Message, Command::ModuleTask::DownloadAndSaveAsEml, info, { this, /*g_ActionNameSelectedAttachmentDownload*/_YTEXT(""), GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
}

void GridMessages::OnUpdateChangeOptions(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridMessages::OnUpdateShowMessageBody(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning() && m_FrameMessages->GetOptions() && m_FrameMessages->GetOptions()->m_BodyContent)
	{
		const auto& sapioSession = m_FrameMessages->GetSapio365Session();

		const int64_t roleId = sapioSession ? sapioSession->GetRoleDelegationID() : 0;
		const bool hasRights = roleId != 0	? RoleDelegationManager::GetInstance().DelegationHasPrivilege(roleId, RoleDelegationUtil::RBAC_USER_MESSAGES_SEE_MAIL_CONTENT, sapioSession)
											: (bool)sapioSession;

		enable = (hasRights && GridUtil::IsSelectionAtLeastOneOfType<BusinessMessage>(*this)) ? TRUE : FALSE;
	}
	pCmdUI->Enable(enable);
	pCmdUI->SetCheck(nullptr != m_MessageViewer && m_MessageViewer->IsShowing());

	setTextFromProfUISCommand(*pCmdUI);
}

void GridMessages::OnUpdateShowMessageAttachments(CCmdUI* pCmdUI)
{
    bool enable = false;
    if (hasNoTaskRunning() && IsFrameConnected())
    {
		for (auto& row : GetSelectedRows())
		{
			if (GridUtil::isBusinessType<BusinessMessage>(row) && O365Grid::IsAuthorizedByRoleDelegation(row->GetTopAncestor(), RoleDelegationUtil::RBAC_USER_MESSAGES_LOADMORE))
			{
				const PooledString strMetaId = row->GetField(GetColMetaID()).GetValueStr();
				if (!row->IsMoreLoaded() && !strMetaId.IsEmpty())
				{
					enable = true;
					break;
				}
			}
	    }
    }

    pCmdUI->Enable(enable ? TRUE : FALSE);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridMessages::OnUpdateDownloadMessageAttachments(CCmdUI* pCmdUI)
{
	bool enable = false;

	if (IsFrameConnected() && hasNoTaskRunning())
	{
		// Enable only if attachment(s) are loaded
		for (auto& row : GetSelectedRows())
		{
			if (GridUtil::isBusinessType<BusinessMessage>(row) && O365Grid::IsAuthorizedByRoleDelegation(row->GetTopAncestor(), RoleDelegationUtil::RBAC_USER_MESSAGES_ATTACHMENTS_DOWNLOAD))
			{
				const auto& aType = row->GetField(m_ColAttachmentType);
				if (aType.HasValue())
				{
					if (aType.IsMulti())
					{
						if (aType.MultiValueContains<PooledString>(Util::GetFileAttachmentStr()))
						{
							enable = true;
							break;
						}
					}
					else if (Util::GetFileAttachmentStr() == aType.GetValueStr())
					{
						enable = true;
						break;
					}
				}
			}
		}
	}

    pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridMessages::OnUpdateViewItemAttachment(CCmdUI* pCmdUI)
{
    BOOL enable = FALSE;

    // Enable only if attachment(s) are loaded
    if (hasNoTaskRunning() && IsFrameConnected() && GetSelectedRows().size() == 1)
    {
        GridBackendRow* row = *(GetSelectedRows().begin());
        if (GridUtil::isBusinessType<BusinessMessage>(row))
        {
            BusinessAttachment attachment;

            const PooledString userId			= row->GetField(GetColMetaID()).GetValueStr();
            const PooledString messageId		= row->GetField(GetColId()).GetValueStr();
            const PooledString attachmentId		= row->GetField(m_ColAttachmentId).GetValueStr();
            const PooledString attachmentType	= row->GetField(m_ColAttachmentType).GetValueStr();

            if (!userId.IsEmpty() && !messageId.IsEmpty() && !attachmentId.IsEmpty() && attachmentType == Util::GetItemAttachmentStr())
                enable = TRUE;
        }
    }

    pCmdUI->Enable(enable);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridMessages::OnUpdateDeleteAttachment(CCmdUI* pCmdUI)
{
    BOOL enable = FALSE;

	if (hasNoTaskRunning())
	{
		// Enable only if all necessary info for deleting the attachment(s) are loaded
		for (auto& row : GetSelectedRows())
		{
			if (GridUtil::isBusinessType<BusinessMessage>(row) && O365Grid::IsAuthorizedByRoleDelegation(row->GetTopAncestor(), RoleDelegationUtil::RBAC_USER_MESSAGES_ATTACHMENTS_EDIT))
			{
				if (row->GetField(m_ColAttachmentId).HasValue())
				{
					const PooledString userId = row->GetField(GetColMetaID()).GetValueStr();
					const PooledString messageId = row->GetField(GetColId()).GetValueStr();
					if (!userId.IsEmpty() && !messageId.IsEmpty())
					{
						enable = TRUE;
						break;
					}
				}
			}
		}
	}

    pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridMessages::OnUpdateToggleInlineAttachments(CCmdUI* pCmdUI)
{
    setTextFromProfUISCommand(*pCmdUI);
	pCmdUI->Enable(hasNoTaskRunning() && !m_ShowInlineAttachments && !m_AllAttachments.empty());
}

void GridMessages::OnUpdateDownloadAsEml(CCmdUI* pCmdUI)
{
	setTextFromProfUISCommand(*pCmdUI);
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAtLeastOneOfType<BusinessMessage>(*this));
}

void GridMessages::InitializeCommands()
{
	ModuleO365Grid<BusinessMessage>::InitializeCommands();

	std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MESSAGEGRID_CHANGEOPTIONS;
		_cmd.m_sMenuText = _T("Change Options");
		_cmd.m_sToolbarText = _T("Change Options");
		_cmd.m_sTipTool = _T("Adjust cut-off date, optional data and filter.");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANGEMODULEOPTIONS_16x16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MESSAGEGRID_CHANGEOPTIONS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Change Options"),
			_T("Adjust cut-off date, optional data and filter."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_MESSAGEGRID_SHOWBODY;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridMessages_InitializeCommands_1, _YLOC("Preview Body...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridMessages_InitializeCommands_2, _YLOC("Preview Body...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGE_CONTENT_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MESSAGEGRID_SHOWBODY, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridMessages_InitializeCommands_3, _YLOC("Preview Message Body")).c_str(),
			YtriaTranslate::Do(GridMessages_InitializeCommands_4, _YLOC("Preview the body of a selected message.\nOnly one message can be previewed at a time.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_MESSAGEGRID_SHOWATTACHMENTS;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridMessages_InitializeCommands_5, _YLOC("Load Attachment Info")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridMessages_InitializeCommands_6, _YLOC("Load Attachment Info")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGES_SHOWATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MESSAGEGRID_SHOWATTACHMENTS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridMessages_InitializeCommands_7, _YLOC("Load Attachment Info")).c_str(),
			YtriaTranslate::Do(GridMessages_InitializeCommands_8, _YLOC("Load the additional attachment info for all selected rows.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_MESSAGEGRID_DOWNLOADATTACHMENTS;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridMessages_InitializeCommands_9, _YLOC("Download Attachments...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridMessages_InitializeCommands_10, _YLOC("Download Attachments...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGES_DOWNLOADATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MESSAGEGRID_DOWNLOADATTACHMENTS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridMessages_InitializeCommands_11, _YLOC("Download Attachments")).c_str(),
			YtriaTranslate::Do(GridMessages_InitializeCommands_12, _YLOC("Download the attachment files of all selected items to your computer.\r\nYou can select a destination folder in the resulting dialog.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_MESSAGEGRID_VIEW_ITEM_ATTACHMENT;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridMessages_InitializeCommands_13, _YLOC("Preview Item...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridMessages_InitializeCommands_14, _YLOC("Preview Item...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGES_VIEWITEMATTACHMENT_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MESSAGEGRID_VIEW_ITEM_ATTACHMENT, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridMessages_InitializeCommands_15, _YLOC("Preview Attached Item")).c_str(),
			YtriaTranslate::Do(GridMessages_InitializeCommands_16, _YLOC("Preview the body of an attached email, vCard, or calendar.\nYou must first load the attachment info and select the item in the grid for this function to be active.\nOnly one item can be previewed at a time.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
        _cmd.m_nCmdID		= ID_MESSAGEGRID_DELETEATTACHMENT;
        _cmd.m_sMenuText	= YtriaTranslate::Do(GridMessages_InitializeCommands_17, _YLOC("Delete Attachments")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridMessages_InitializeCommands_18, _YLOC("Delete Attachments")).c_str();
        _cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGES_DELETEATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MESSAGEGRID_DELETEATTACHMENT, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridMessages_InitializeCommands_19, _YLOC("Delete Attachments")).c_str(),
			YtriaTranslate::Do(GridMessages_InitializeCommands_20, _YLOC("Delete all selected attachment files.")).c_str(),
			_cmd.m_sAccelText);
	}

    {
        CExtCmdItem _cmd;
        _cmd.m_nCmdID = ID_MESSAGEGRID_TOGGLEINLINEATTACHMENTS;
        _cmd.m_sMenuText = YtriaTranslate::Do(GridMessages_InitializeCommands_21, _YLOC("Show Inline Attachments")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridMessages_InitializeCommands_22, _YLOC("Show Inline Attachments")).c_str();
        _cmd.m_sAccelText = _YTEXT("");
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

        HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGES_TOGGLEINLINEATTACHMENTS_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_MESSAGEGRID_TOGGLEINLINEATTACHMENTS, hIcon, false);

        setRibbonTooltip(_cmd.m_nCmdID,
            YtriaTranslate::Do(GridMessages_InitializeCommands_23, _YLOC("Show Inline Attachments")).c_str(),
            YtriaTranslate::Do(GridMessages_InitializeCommands_24, _YLOC("Include inline attachments in the grid.\nBy default, inline attachments, like embedded images, are not listed.\nTo go back to default, use the Refresh All button.")).c_str(),
            _cmd.m_sAccelText);
    }

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MESSAGEGRID_DOWNLOADASEML;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridMessages_InitializeCommands_25, _YLOC("Download as EML")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridMessages_InitializeCommands_25, _YLOC("Download as EML")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGE_ID_MESSAGEGRID_DOWNLOADASEML_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MESSAGEGRID_DOWNLOADASEML, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridMessages_InitializeCommands_25, _YLOC("Download as EML")).c_str(),
			_T("Download selected messages as EML files."),
			_cmd.m_sAccelText);
	}
}

void GridMessages::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_MESSAGEGRID_CHANGEOPTIONS);
		pPopup->ItemInsert(ID_MESSAGEGRID_SHOWBODY);
		pPopup->ItemInsert(ID_MESSAGEGRID_SHOWATTACHMENTS);
		pPopup->ItemInsert(ID_MESSAGEGRID_TOGGLEINLINEATTACHMENTS);
		pPopup->ItemInsert(ID_MESSAGEGRID_DOWNLOADATTACHMENTS);
		pPopup->ItemInsert(ID_MESSAGEGRID_VIEW_ITEM_ATTACHMENT);
		pPopup->ItemInsert(ID_MESSAGEGRID_DELETEATTACHMENT);
		pPopup->ItemInsert(ID_MESSAGEGRID_DOWNLOADASEML);
		pPopup->ItemInsert(); // Sep
	}

	ModuleO365Grid<BusinessMessage>::OnCustomPopupMenu(pPopup, nStart);

	// We want to move "Delete" to a different position.
	{
		auto pos = pPopup->ItemFindPosForCmdID(ID_MODULEGRID_DELETE);
		if (-1 != pos)
		{
			pPopup->ItemRemove(pos);
			if (pPopup->ItemGetInfo(pos).IsSeparator())
				pPopup->ItemRemove(pos);
		}

		pos = pPopup->ItemFindPosForCmdID(ID_MESSAGEGRID_SHOWBODY);
		pPopup->ItemInsert(ID_MODULEGRID_DELETE, pos + 1);
	}
}

void GridMessages::loadMoreImpl(const std::vector<GridBackendRow*>& p_Rows)
{
	std::set<AttachmentsInfo::IDs> ids;

	boost::YOpt<bool> ignoreNoAttachment;
	for (auto& row : p_Rows)
	{
		if (GridUtil::isBusinessType<BusinessMessage>(row) && O365Grid::IsAuthorizedByRoleDelegation(row->GetTopAncestor(), RoleDelegationUtil::RBAC_USER_MESSAGES_LOADMORE))
		{
			if (!row->GetField(m_ColHasGraphAttachment).GetValueBool())
			{
				if (!ignoreNoAttachment)
				{
					YCodeJockMessageBox dlg(this,
						DlgMessageBox::eIcon_Question,
						YtriaTranslate::Do(GridMessages_LoadMoreImpl_1, _YLOC("Load Attachment Info")).c_str(),
						YtriaTranslate::Do(GridMessages_LoadMoreImpl_2, _YLOC("Some selected items only contain inline attachments. Do you want to load them too?")).c_str(),
						_YTEXT(""),
						{ { IDYES, YtriaTranslate::Do(GridMessages_LoadMoreImpl_3, _YLOC("Yes")).c_str() },{ IDNO, YtriaTranslate::Do(GridMessages_LoadMoreImpl_4, _YLOC("No")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridMessages_LoadMoreImpl_5, _YLOC("Cancel")).c_str() } });

					const auto res = dlg.DoModal();
					if (IDCANCEL == res)
						return;

					ignoreNoAttachment = IDNO == res;
				}

				if (*ignoreNoAttachment)
					continue;
			}

			const PooledString strMetaId = row->GetField(GetColMetaID()).GetValueStr();
			if (!strMetaId.IsEmpty())
			{
				const PooledString strMessageId = row->GetField(GetColId()).GetValueStr();
				ASSERT(!strMessageId.IsEmpty());
				ids.emplace(strMetaId, strMessageId, PooledString(), row->GetField(m_ColSubject).GetValueStr());
			}
		}
	}

	if (!ids.empty())
	{
		CommandInfo info;
		info.Data() = ids;
		info.SetFrame(m_FrameMessages);
		info.SetRBACPrivilege(m_FrameMessages->GetModuleCriteria().m_Privilege);
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Message, Command::ModuleTask::ListAttachments, info, { this, g_ActionNameSelectedAttachmentLoadInfo, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
}

std::vector<SubItemKey> GridMessages::GetDeleteAttachmentsData(bool p_Selected)
{
	CWaitCursor c;

	// Remove if we don't want to apply whole users but single messages.
	auto sts = p_Selected	? std::make_unique<ScopedTemporarySelection>(*this, ScopedTemporarySelection::Mode::SELECT_SELECTION_TOP_LEVEL_AND_ALL_CHILDREN, true)
							: std::unique_ptr<ScopedTemporarySelection>();

    std::vector<SubItemKey> attachments;
    if (!p_Selected)
    {
        for (const auto& deletedAttachment : GetModifications().GetSubItemDeletions())
            attachments.push_back(deletedAttachment->GetKey());
    }
    else
    {
        for (auto row : GetSelectedRows())
        {
			if (!row->IsGroupRow())
			{
				for (const auto& deletedAttachment : GetModifications().GetSubItemDeletions())
				{
					if (deletedAttachment->IsCorrespondingRow(row))
						attachments.push_back(deletedAttachment->GetKey());
				}
			}
        }
    }

	return attachments;
}

GridBackendColumn* GridMessages::GetColMetaID() const
{
	return m_Template.m_ColumnID;
}

GridBackendColumn* GridMessages::getColMetaDisplayName() const
{
	return m_Template.m_ColumnDisplayName;
}

GridBackendColumn* GridMessages::getColMetaUserPrincipalName() const
{
	return m_Template.m_ColumnUserPrincipalName;
}

void GridMessages::HideMessageViewer()
{
	if (m_MessageViewer && m_MessageViewer->IsShowing())
		m_MessageViewer->ToggleView();
}

GridBackendColumn* GridMessages::GetColumnAttachmentId() const
{
	return m_ColAttachmentId;
}

void GridMessages::OnGbwSelectionChangedSpecific()
{
	if (m_MessageViewer && m_MessageViewer->IsShowing())
	{
		// Try to freeze less upon selection change by posting a message.
		YCallbackMessage::DoPost([this]()
			{
				if (::IsWindow(m_hWnd) && m_MessageViewer && m_MessageViewer->IsShowing())
					m_MessageViewer->ShowSelectedMessageBody();
			}
		);
	}
}

bool GridMessages::HasModificationsPending(bool inSelectionOnly /*= false*/)
{
	if (inSelectionOnly)
		return HasModificationsPendingInParentOfSelected();
	return ModuleO365Grid<BusinessMessage>::HasModificationsPending(false);
}

GridBackendRow* GridMessages::fillMessageRow(GridBackendRow* p_ParentUserRow, const BusinessMessage& p_BusinessMessage)
{
	GridBackendField fMetaID(p_ParentUserRow->GetField(GetColMetaID()));
	GridBackendField fID(p_BusinessMessage.GetID(), GetColId());
	GridBackendRow* row = GetRowIndex().GetRow({ fMetaID, fID}, true, true);

    ASSERT(nullptr != row);
    if (nullptr != row)
    {
		row->SetParentRow(p_ParentUserRow);

		SetRowObjectType(row, p_BusinessMessage, p_BusinessMessage.HasFlag(BusinessObject::CANCELED));

		for (auto c : m_MetaColumns)
		{
			if (p_ParentUserRow->HasField(c))
				row->AddField(p_ParentUserRow->GetField(c));
			else
				row->RemoveField(c);
		}

        row->AddField(p_BusinessMessage.m_SenderAddress, m_ColSenderAddress);
        row->AddField(p_BusinessMessage.m_SenderName, m_ColSenderName);
        row->AddField(p_BusinessMessage.m_FromAddress, m_ColFromAddress);
        row->AddField(p_BusinessMessage.m_FromName, m_ColFromName);

		{
			auto ccRecipients = GridHelpers::ExtractEmailAddresses(p_BusinessMessage.m_CcRecipients);
			row->AddField(ccRecipients.first, m_ColCcRecipientsAddresses);
			row->AddField(ccRecipients.second, m_ColCcRecipientsNames);
		}

		{
			auto bccRecipients = GridHelpers::ExtractEmailAddresses(p_BusinessMessage.m_BccRecipients);
			row->AddField(bccRecipients.first, m_ColBccRecipientsAddresses);
			row->AddField(bccRecipients.second, m_ColBccRecipientsNames);
		}

		{
			auto replyTo = GridHelpers::ExtractEmailAddresses(p_BusinessMessage.m_ReplyTo);
			row->AddField(replyTo.first, m_ColReplyToAddresses);
			row->AddField(replyTo.second, m_ColReplyToNames);
		}

		{
			auto toRecipients = GridHelpers::ExtractEmailAddresses(p_BusinessMessage.m_ToRecipients);
			row->AddField(toRecipients.first, m_ColToRecipientsAddresses);
			row->AddField(toRecipients.second, m_ColToRecipientsNames);
		}

		// FIXME: We need to be sure that adding another explosion family in this grid won't break attachments edition...
		if (nullptr != m_ColHeaderName)
		{
			auto headers = GridHelpers::ExtractHeaders(p_BusinessMessage.m_InternetMessageHeaders);
			row->AddField(headers.first, m_ColHeaderName);
			row->AddField(headers.second, m_ColHeaderValue);
		}

		row->AddField(p_BusinessMessage.m_BodyContentType, m_ColBodyContentType);
        row->AddField(p_BusinessMessage.m_Subject, m_ColSubject);

		const auto& session = m_FrameMessages->GetBusinessObjectManager()->GetSapio365Session();
		int64_t roleId = session->GetRoleDelegationID();
		if (roleId == 0 || (roleId != 0 && RoleDelegationManager::GetInstance().DelegationHasPrivilege(roleId, RoleDelegationUtil::RBAC_USER_MESSAGES_SEE_MAIL_CONTENT, session)))
			row->AddFieldForRichText(p_BusinessMessage.m_BodyPreview, m_ColBodyPreview);
        row->AddField(p_BusinessMessage.m_ChangeKey, m_ColChangeKey);
        row->AddField(p_BusinessMessage.m_ConversationId, m_ColConversationId);
        row->AddField(p_BusinessMessage.m_CreatedDateTime, m_ColCreatedDate);
        row->AddField(p_BusinessMessage.m_Importance, m_ColImportance);
        row->AddField(p_BusinessMessage.m_InferenceClassification, m_ColInferenceClassification);
        row->AddField(p_BusinessMessage.m_InternetMessageId, m_ColInternetMessageId);
        row->AddField(p_BusinessMessage.m_LastModifiedDateTime, m_ColLastModifiedDateTime);
        row->AddField(p_BusinessMessage.m_ParentFolderId, m_ColParentFolderId);
        row->AddField(p_BusinessMessage.m_ParentFolderName, m_ColParentFolderName);
        row->AddField(p_BusinessMessage.m_ReceivedDateTime, m_ColReceivedDateTime);
        row->AddField(p_BusinessMessage.m_SentDateTime, m_ColSentDateTime);
		row->AddFieldForHyperlink(p_BusinessMessage.m_WebLink, m_ColWebLink);
        row->AddField(p_BusinessMessage.m_IsDeliveryReceiptRequested, m_ColIsDeliveryReceiptRequested);
        row->AddField(p_BusinessMessage.m_IsDraft, m_ColIsDraft);
        row->AddField(p_BusinessMessage.m_IsRead, m_ColIsRead);
        row->AddField(p_BusinessMessage.m_IsReadReceiptRequested, m_ColIsReadReceiptRequested);
        row->AddField(p_BusinessMessage.m_Categories, m_ColCategories);
        row->AddField(p_BusinessMessage.m_HasAttachments, m_ColHasGraphAttachment);

		auto messageBodyData = (MessageBodyData*)row->GetLParam();
		if (nullptr != messageBodyData)
		{
			if (p_BusinessMessage.m_BodyContent.is_initialized())
			{
				messageBodyData->m_BodyContent = p_BusinessMessage.m_BodyContent;
			}
			else
			{
				row->SetLParam(NULL);
				delete messageBodyData;
			}
		}
		else
		{
			if (p_BusinessMessage.m_BodyContent.is_initialized())
				row->SetLParam((LPARAM) new MessageBodyData(p_BusinessMessage.m_BodyContent));
		}
    }

    return row;
}

void GridMessages::fillMailFolderRow(const BusinessMailFolder& p_MailFolder, GridBackendRow& p_MailFolderRow)
{
	SetRowObjectType(&p_MailFolderRow, p_MailFolder, p_MailFolder.HasFlag(BusinessObject::CANCELED));
	p_MailFolderRow.SetHierarchyParentToHide(true);

	p_MailFolderRow.AddField(p_MailFolder.m_DisplayName, m_Template.m_ColumnDisplayName);
	p_MailFolderRow.AddField(p_MailFolder.m_ParentFolderId, m_ColParentFolderId);
	p_MailFolderRow.AddField(p_MailFolder.m_ChildFolderCount, m_ColChildFolderCount);
	p_MailFolderRow.AddField(p_MailFolder.m_TotalItemCount, m_ColTotalItemCount).RemoveModified();
	p_MailFolderRow.AddField(p_MailFolder.m_UnreadItemCount, m_ColUnreadItemCount).RemoveModified();

	auto parentRow = p_MailFolderRow.GetParentRow();
	while (nullptr != parentRow)
	{
		if (p_MailFolder.m_TotalItemCount.is_initialized())
		{
			__int3264 val = 0;
			if (parentRow->HasField(m_ColTotalItemCount))
				val = parentRow->GetField(m_ColTotalItemCount).GetValueLong();
			parentRow->AddField(val + *p_MailFolder.m_TotalItemCount, m_ColTotalItemCount).RemoveModified();
		}

		if (p_MailFolder.m_UnreadItemCount.is_initialized())
		{
			__int3264 val = 0;
			if (parentRow->HasField(m_ColUnreadItemCount))
				val = parentRow->GetField(m_ColUnreadItemCount).GetValueLong();
			parentRow->AddField(val + *p_MailFolder.m_UnreadItemCount, m_ColUnreadItemCount).RemoveModified();
		}
		parentRow = parentRow->GetParentRow();
	}
}

void GridMessages::fillAttachmentFields(GridBackendRow* p_Row, const vector<BusinessAttachment>& p_Attachments)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		vector<PooledString>	attachmentType;
		vector<PooledString>	attachmentId;
		vector<PooledString>	attachmentName;
		vector<PooledString>	attachmentContentType;
		//vector<PooledString>	attachmentContentId;
		vector<__int3264>		attachmentSize;
		vector<YTimeDate>		attachmentLastModifiedDateTime;
		vector<BOOLItem>		attachmentIsInline;
		vector<PooledString>	attachmentExt;
		vector<PooledString>	attachmentNoDotExt;
		int32_t					fileTypeIcon = NO_ICON;

        for (const auto& attachment : p_Attachments)
        {
            ASSERT(attachment.GetIsInline().is_initialized());
            if (attachment.GetIsInline().is_initialized())
            {
                if (m_ShowInlineAttachments || (!m_ShowInlineAttachments && attachment.GetDataType() &&
                    !(*attachment.GetIsInline() == true &&
                    *attachment.GetDataType() == Util::GetFileAttachmentStr())))
                {
                    attachmentType.push_back(attachment.GetDataType() ? *attachment.GetDataType() : GridBackendUtil::g_NoValueString);
                    attachmentId.push_back(attachment.GetID());
                    attachmentName.push_back(attachment.GetName() ? *attachment.GetName() : GridBackendUtil::g_NoValueString);
                    attachmentContentType.push_back(attachment.GetContentType() ? *attachment.GetContentType() : GridBackendUtil::g_NoValueString);
					//attachmentContentId.push_back(attachment.GetContentId() ? *attachment.GetContentId() : GridBackendUtil::g_NoValueString);
                    attachmentSize.push_back(attachment.GetSize() ? static_cast<__int3264>(*attachment.GetSize()) : GridBackendUtil::g_NoValueNumberInt);
                    attachmentLastModifiedDateTime.push_back(attachment.GetLastModifiedDateTime() ? *attachment.GetLastModifiedDateTime() : GridBackendUtil::g_NoValueDate);
                    if (attachment.GetIsInline())
                        attachmentIsInline.emplace_back(*attachment.GetIsInline() ? TRUE : FALSE);
                    else
                        attachmentIsInline.push_back(GridBackendUtil::g_NoValueBoolItem);

					if (attachment.GetName())
					{
						const auto data = GetFileTypeAndIcon(*attachment.GetName(), false);
						attachmentNoDotExt.push_back(std::get<0>(data));
						attachmentExt.push_back(std::get<1>(data));
						if (p_Attachments.size() == 1)
							fileTypeIcon = std::get<2>(data);
					}
					else
					{
						attachmentNoDotExt.push_back(GridBackendUtil::g_NoValueString);
						attachmentExt.push_back(GridBackendUtil::g_NoValueString);
					}
                }
            }
		}

        p_Row->AddField(!p_Attachments.empty(), m_ColHasAnyAttachment);
		if (!p_Attachments.empty())
		{
			auto& attachmentTypeField = p_Row->AddField(attachmentType, m_ColAttachmentType);
			if (attachmentType.size() == 1)
                GridUtil::SetAttachmentIcon(attachmentTypeField, m_Icons);
		
			p_Row->AddField(attachmentId, m_ColAttachmentId);
			p_Row->AddField(attachmentName, m_ColAttachmentName);
			p_Row->AddField(attachmentContentType, m_ColAttachmentContentType);
			//p_Row->AddField(attachmentContentId, m_ColAttachmentContentId);
			p_Row->AddField(attachmentSize, m_ColAttachmentSize);
			p_Row->AddField(attachmentLastModifiedDateTime, m_ColAttachmentLastModifiedDateTime);
			if (attachmentIsInline.size() == 1)
				p_Row->AddFieldForCheckBox(TRUE == attachmentIsInline.back().m_BOOL ? true : false, m_ColAttachmentIsInline);
			else
				p_Row->AddField(attachmentIsInline, m_ColAttachmentIsInline);

			p_Row->AddField(attachmentNoDotExt, GetColumnFileType(), fileTypeIcon);
			p_Row->AddField(attachmentExt, m_ColAttachmentNameExtOnly);
		}
		else
		{
			p_Row->RemoveField(m_ColAttachmentType);
			p_Row->RemoveField(m_ColAttachmentId);
			p_Row->RemoveField(m_ColAttachmentName);
			p_Row->RemoveField(m_ColAttachmentContentType);
			//p_Row->RemoveField(m_ColAttachmentContentId);
			p_Row->RemoveField(m_ColAttachmentSize);
			p_Row->RemoveField(m_ColAttachmentLastModifiedDateTime);
			p_Row->RemoveField(m_ColAttachmentIsInline);
			p_Row->RemoveField(GetColumnFileType());
			p_Row->RemoveField(m_ColAttachmentNameExtOnly);
		}
	}
}

void GridMessages::onPostExplodeMultiValueSisterhood(GridBackendRow* p_Row, const std::set<GridBackendColumn*>& p_Columns)
{
    if (p_Columns.end() != p_Columns.find(m_ColAttachmentType))
	{
		GridUtil::SetAttachmentIcon(p_Row->GetField(m_ColAttachmentType), m_Icons);
		SetRowFileType(p_Row, p_Row->GetField(m_ColAttachmentName).GetValueStr(), false);
	}
}

bool GridMessages::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return ModuleO365Grid<BusinessMessage>::IsRowValidForNoFieldText(p_Row, p_Col)
		&& (!p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore)
			|| m_MetaIDsMoreLoaded.end() == m_MetaIDsMoreLoaded.find(p_Row->GetField(GetColMetaID()).GetValueStr()));
}

BOOL GridMessages::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return ModuleO365Grid<BusinessMessage>::PreTranslateMessage(pMsg);
}

BusinessMessage GridMessages::getBusinessObject(GridBackendRow* p_Row) const
{
	ASSERT(GridUtil::IsBusinessMessage(p_Row));

	BusinessMessage bm;

	{
		const auto& field = p_Row->GetField(GetColId());
		if (field.HasValue())
			bm.m_Id = field.GetValueStr();
	}

	{
		const auto& field = p_Row->GetField(m_ColSenderAddress);
		if (field.HasValue())
			bm.m_SenderAddress = field.GetValueStr();
	}

	{
		const auto& field = p_Row->GetField(m_ColSenderName);
		if (field.HasValue())
			bm.m_SenderName = field.GetValueStr();
	}

	{
		const auto& field = p_Row->GetField(m_ColFromAddress);
		if (field.HasValue())
			bm.m_FromAddress = field.GetValueStr();
	}
	
	{
		const auto& field = p_Row->GetField(m_ColFromName);
		if (field.HasValue())
			bm.m_FromName = field.GetValueStr();
	}

	if(p_Row->IsExplosionFragment())
	{
		// FIXME: Handle message header explosion... (remove the assert when done).
		ASSERT(nullptr == m_ColHeaderName);

		auto pGrid = const_cast<GridMessages*>(this);
		auto ListSibs = pGrid->GetMultivalueManager().GetExplosionSiblings(p_Row);
		ListSibs.insert(pGrid->GetMultivalueManager().GetTopSourceRow(p_Row));
		for (GridBackendRow* pRowSib : ListSibs)
		{
			vector<Recipient> Temp1 = GridHelpers::ExtractRecipients(pRowSib, m_ColCcRecipientsAddresses, m_ColCcRecipientsNames);
			bm.m_CcRecipients.insert(bm.m_CcRecipients.end(), Temp1.begin(), Temp1.end());
			vector<Recipient> Temp2 = GridHelpers::ExtractRecipients(pRowSib, m_ColBccRecipientsAddresses, m_ColBccRecipientsNames);
			bm.m_BccRecipients.insert(bm.m_BccRecipients.end(), Temp2.begin(), Temp2.end());
			vector<Recipient> Temp3 = GridHelpers::ExtractRecipients(pRowSib, m_ColReplyToAddresses, m_ColReplyToNames);
			bm.m_ReplyTo.insert(bm.m_ReplyTo.end(), Temp3.begin(), Temp3.end());
			vector<Recipient> Temp4 = GridHelpers::ExtractRecipients(pRowSib, m_ColToRecipientsAddresses, m_ColToRecipientsNames);
			bm.m_ToRecipients.insert(bm.m_ToRecipients.end(), Temp4.begin(), Temp4.end());
		}

		// Remove duplicates. operator== is defined in Recipient.
		sort(bm.m_CcRecipients.begin(), bm.m_CcRecipients.end());
		bm.m_CcRecipients.erase(unique(bm.m_CcRecipients.begin(), bm.m_CcRecipients.end()), bm.m_CcRecipients.end());
		sort(bm.m_BccRecipients.begin(), bm.m_BccRecipients.end());
		bm.m_BccRecipients.erase(unique(bm.m_BccRecipients.begin(), bm.m_BccRecipients.end()), bm.m_BccRecipients.end());
		sort(bm.m_ReplyTo.begin(), bm.m_ReplyTo.end());
		bm.m_ReplyTo.erase(unique(bm.m_ReplyTo.begin(), bm.m_ReplyTo.end()), bm.m_ReplyTo.end());
		sort(bm.m_ToRecipients.begin(), bm.m_ToRecipients.end());
		bm.m_ToRecipients.erase(unique(bm.m_ToRecipients.begin(), bm.m_ToRecipients.end()), bm.m_ToRecipients.end());
	}
	else 
	{
		bm.m_CcRecipients	= GridHelpers::ExtractRecipients(p_Row, m_ColCcRecipientsAddresses,		m_ColCcRecipientsNames);
		bm.m_BccRecipients	= GridHelpers::ExtractRecipients(p_Row, m_ColBccRecipientsAddresses,	m_ColBccRecipientsNames);
		bm.m_ReplyTo		= GridHelpers::ExtractRecipients(p_Row, m_ColReplyToAddresses,			m_ColReplyToNames);
		bm.m_ToRecipients	= GridHelpers::ExtractRecipients(p_Row, m_ColToRecipientsAddresses,		m_ColToRecipientsNames);

		if (nullptr != m_ColHeaderName)
			bm.m_InternetMessageHeaders = GridHelpers::ExtractInternetMessageHeaders(p_Row, m_ColHeaderName, m_ColHeaderValue);
	}

	{
		const auto& field = p_Row->GetField(m_ColBodyContentType);
		if (field.HasValue())
			bm.m_BodyContentType = field.GetValueStr();
	}

	{
		const auto& field = p_Row->GetField(m_ColSubject);
		if (field.HasValue())
			bm.m_Subject = field.GetValueStr();
	}

	
	{
		const auto& field = p_Row->GetField(m_ColBodyPreview);
		if (field.HasValue())
			bm.m_BodyPreview = field.GetValueStr();
	}

	{
		const auto& field = p_Row->GetField(m_ColChangeKey);
		if (field.HasValue())
			bm.m_ChangeKey = field.GetValueStr();
	}

	{
		const auto& field = p_Row->GetField(m_ColConversationId);
		if (field.HasValue())
			bm.m_ConversationId = field.GetValueStr();
	}

	{
		const auto& field = p_Row->GetField(m_ColCreatedDate);
		if (field.HasValue())
			bm.m_CreatedDateTime = field.GetValueTimeDate();
	}
	
	{
		const auto& field = p_Row->GetField(m_ColImportance);
		if (field.HasValue())
			bm.m_Importance = field.GetValueStr();
	}
	
	{
		const auto& field = p_Row->GetField(m_ColInferenceClassification);
		if (field.HasValue())
			bm.m_InferenceClassification = field.GetValueStr();
	}

	{
		const auto& field = p_Row->GetField(m_ColInternetMessageId);
		if (field.HasValue())
			bm.m_InternetMessageId = field.GetValueStr();
	}

	{
		const auto& field = p_Row->GetField(m_ColLastModifiedDateTime);
		if (field.HasValue())
			bm.m_LastModifiedDateTime = field.GetValueTimeDate();
	}
	
	{
		const auto& field = p_Row->GetField(m_ColParentFolderId);
		if (field.HasValue())
			bm.m_ParentFolderId = field.GetValueStr();
	}

	{
		const auto& field = p_Row->GetField(m_ColParentFolderName);
		if (field.HasValue())
			bm.m_ParentFolderName = field.GetValueStr();
	}
	
	{
		const auto& field = p_Row->GetField(m_ColReceivedDateTime);
		if (field.HasValue())
			bm.m_ReceivedDateTime = field.GetValueTimeDate();
	}
	
	{
		const auto& field = p_Row->GetField(m_ColSentDateTime);
		if (field.HasValue())
			bm.m_SentDateTime = field.GetValueTimeDate();
	}

	{
		const auto& field = p_Row->GetField(m_ColWebLink);
		if (field.HasValue())
			bm.m_WebLink = field.GetValueStr();
	}
	
	{
		const auto& field = p_Row->GetField(m_ColIsDeliveryReceiptRequested);
		if (field.HasValue())
			bm.m_IsDeliveryReceiptRequested = field.GetValueBool();
	}

	{
		const auto& field = p_Row->GetField(m_ColIsDraft);
		if (field.HasValue())
			bm.m_IsDraft = field.GetValueBool();
	}

	{
		const auto& field = p_Row->GetField(m_ColIsRead);
		if (field.HasValue())
			bm.m_IsRead = field.GetValueBool();
	}

	{
		const auto& field = p_Row->GetField(m_ColIsReadReceiptRequested);
		if (field.HasValue())
			bm.m_IsReadReceiptRequested = field.GetValueBool();
	}

	{
		const auto& field = p_Row->GetField(m_ColCategories);
		if (field.HasValue())
		{
			if (field.IsMulti())
				bm.m_Categories = *field.GetValuesMulti<PooledString>();
			else
				bm.m_Categories.emplace_back(field.GetValueStr());
		}
	}

	{
		const auto& field = p_Row->GetField(m_ColHasGraphAttachment);
		if (field.HasValue())
			bm.m_HasAttachments = field.GetValueBool();
	}

	{
		const auto& field = p_Row->GetField(m_Template.m_ColumnID);
		if (field.HasValue())
			bm.m_UserId = field.GetValueStr();
	}

	return bm;
}

void GridMessages::RemoveBusinessObjects(const vector<BusinessMessage>& p_Messages)
{
	// Implement ME!
	ASSERT(false);
}

void GridMessages::UpdateBusinessObjects(const vector<BusinessMessage>& p_Messages, bool p_SetModifiedStatus)
{
	// Implement ME!
	ASSERT(false);
}

RoleDelegationUtil::RBAC_Privilege GridMessages::GetPrivilegeDelete() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MESSAGES_DELETE;
}

bool GridMessages::IsMyData() const
{
	return m_IsMyData;
}

O365Grid::SnapshotCaps GridMessages::GetSnapshotCapabilities() const
{
	return { true };
}

bool GridMessages::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	return m_Template.GetSnapshotValue(p_Value, p_Field, p_Column)
		|| ModuleO365Grid<BusinessMessage>::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridMessages::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	return m_Template.LoadSnapshotValue(p_Value, p_Row, p_Column)
		|| ModuleO365Grid<BusinessMessage>::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

bool GridMessages::GetSnapshotAdditionalValue(wstring& p_Value, GridBackendRow& p_Row)
{
	MessageBodyData* messageBodyData = reinterpret_cast<MessageBodyData*>(p_Row.GetLParam());
	if (nullptr != messageBodyData && messageBodyData->m_BodyContent)
	{
		p_Value = *messageBodyData->m_BodyContent;
		return true;
	}

	return false;
}

bool GridMessages::SetSnapshotAdditionalValue(const wstring& p_Value, GridBackendRow& p_Row)
{
	MessageBodyData* messageBodyData = reinterpret_cast<MessageBodyData*>(p_Row.GetLParam());
	if (nullptr != messageBodyData)
		messageBodyData->m_BodyContent = p_Value;
	else
		p_Row.SetLParam((LPARAM) new MessageBodyData(PooledString(p_Value)));

	return true;
}
