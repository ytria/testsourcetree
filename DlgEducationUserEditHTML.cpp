#include "DlgEducationUserEditHTML.h"
#include "AutomationNames.h"
#include "TimeUtil.h"

DlgEducationUserEditHTML::DlgEducationUserEditHTML(vector<EducationUser>& p_Users, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
	: DlgFormsHTML(p_Action, p_Parent, p_Action == DlgFormsHTML::Action::CREATE ? g_ActionNameCreateEducationUser : g_ActionNameSelectedEditEducationUser),
	m_Users(p_Users),
	m_AllStudents(false),
	m_AllTeachers(false)
{
	const auto& isTeacher = [](const EducationUser& p_User) {
		return p_User.GetPrimaryRole() && *p_User.GetPrimaryRole() == _YTEXT("teacher");
	};

	if (std::any_of(m_Users.begin(), m_Users.end(), isTeacher))
		m_AllTeachers = std::all_of(m_Users.begin(), m_Users.end(), isTeacher);
	else
		m_AllStudents = true;
}

void DlgEducationUserEditHTML::generateJSONScriptData()
{
	initMain(_YTEXT("Edit education users"), false, false, true);

	getGenerator().addCategory(_T("General Info"), CategoryFlags::EXPAND_BY_DEFAULT);
	addComboEditor(&EducationUser::GetExternalSource, &EducationUser::SetExternalSource, _YTEXT("externalSource"), _T("External Source"), 0, { { _YTEXT("sis"), _YTEXT("sis") }, { _YTEXT("manual"), _YTEXT("manual") }});
	// Mailing address
	addStringEditor(&EducationUser::GetMailingAddressCity, &EducationUser::SetMailingAddressCity, _YTEXT("mailingAddressCity"), _T("Mailing Address City"), 0);
	addStringEditor(&EducationUser::GetMailingAddressCountryOrRegion, &EducationUser::SetMailingAddressCountryOrRegion, _YTEXT("mailingAddressCountryOrRegion"), _T("Mailing Address Country or region"), 0);
	addStringEditor(&EducationUser::GetMailingAddressPostalCode, &EducationUser::SetMailingAddressPostalCode, _YTEXT("mailingAddressPostalCode"), _T("Mailing Address Postal Code"), 0);
	addStringEditor(&EducationUser::GetMailingAddressState, &EducationUser::SetMailingAddressState, _YTEXT("mailingAddressState"), _T("Mailing Address State"), 0);
	addStringEditor(&EducationUser::GetMailingAddressStreet, &EducationUser::SetMailingAddressStreet, _YTEXT("mailingAddressStreet"), _T("Mailing Address Street"), 0);

	// Residence address
	addStringEditor(&EducationUser::GetResidenceAddressCity, &EducationUser::SetResidenceAddressCity, _YTEXT("residenceAddressCity"), _T("Residence Address City"), 0);
	addStringEditor(&EducationUser::GetResidenceAddressCountryOrRegion, &EducationUser::SetResidenceAddressCountryOrRegion, _YTEXT("residenceAddressCountryOrRegion"), _T("Residence Address Country or region"), 0);
	addStringEditor(&EducationUser::GetResidenceAddressPostalCode, &EducationUser::SetResidenceAddressPostalCode, _YTEXT("residenceAddressPostalCode"), _T("Residence Address Postal Code"), 0);
	addStringEditor(&EducationUser::GetResidenceAddressState, &EducationUser::SetResidenceAddressState, _YTEXT("residenceAddressState"), _T("Residence Address State"), 0);
	addStringEditor(&EducationUser::GetResidenceAddressStreet, &EducationUser::SetResidenceAddressStreet, _YTEXT("residenceAddressStreet"), _T("Residence Address Street"), 0);

	addStringEditor(&EducationUser::GetMiddleName, &EducationUser::SetMiddleName, _YTEXT("middleName"), _T("Middle Name"), 0);
	addComboEditor(&EducationUser::GetPrimaryRole, &EducationUser::SetPrimaryRole, _YTEXT("primaryRole"), _T("Primary Role"), 0, { { _YTEXT("Student"), _YTEXT("student") }, { _YTEXT("Teacher"), _YTEXT("teacher") } });
	// Related contacts

	if (m_AllStudents)
	{
		getGenerator().addCategory(_T("Student Info"), CategoryFlags::EXPAND_BY_DEFAULT);
		addDateEditor(&EducationUser::GetStudentBirthDate, &EducationUser::SetStudentBirthDate, _YTEXT("birthDate"), _T("Birth Date"), 0, Str::g_EmptyString);
		addStringEditor(&EducationUser::GetStudentExternalId, &EducationUser::SetStudentExternalId, _YTEXT("studentExternalId"), _T("External Id"), 0);
		addComboEditor(&EducationUser::GetStudentGender, &EducationUser::SetStudentGender, _YTEXT("studentGender"), _T("Gender"), 0, { { _YTEXT("Female"), _YTEXT("female") }, { _YTEXT("Male"), _YTEXT("male") }, { _YTEXT("Other"), _YTEXT("other") }, { _YTEXT("Unknown future value"), _YTEXT("unknownFutureValue") } });
		addStringEditor(&EducationUser::GetStudentGrade, &EducationUser::SetStudentGrade, _YTEXT("studentGrade"), _T("Grade"), 0);
		addStringEditor(&EducationUser::GetStudentGraduationYear, &EducationUser::SetStudentGraduationYear, _YTEXT("studentGraduationYear"), _T("Graduation Year"), 0);
		addStringEditor(&EducationUser::GetStudentNumber, &EducationUser::SetStudentNumber, _YTEXT("studentNumber"), _T("Student Number"), 0);
	}

	if (m_AllTeachers)
	{
		getGenerator().addCategory(_T("Teacher Info"), CategoryFlags::EXPAND_BY_DEFAULT);
		addStringEditor(&EducationUser::GetTeacherExternalId, &EducationUser::SetTeacherExternalId, _YTEXT("teacherExternalId"), _T("External Id"), 0);
		addStringEditor(&EducationUser::GetTeacherNumber, &EducationUser::SetTeacherNumber, _YTEXT("teacherNumber"), _T("Teacher Number"), 0);
	}

	getGenerator().addApplyButton(isEditDialog() ? LocalizedStrings::g_ApplyButtonText : LocalizedStrings::g_CreateButtonText);
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
	getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);

	for (auto& user : m_Users)
	{
		user.SetCreatedBy(boost::none);
		user.SetExternalSource(boost::none);
		user.SetMailingAddress(boost::none);
		user.SetResidenceAddress(boost::none);
		user.SetMiddleName(boost::none);
		user.SetPrimaryRole(boost::none);
		user.SetRelatedContacts(boost::none);
		user.SetStudent(boost::none);
		user.SetTeacher(boost::none);
	}
}

wstring DlgEducationUserEditHTML::getDialogTitle() const
{
	return _T("Edit education users");
}

bool DlgEducationUserEditHTML::processPostedData(const IPostedDataTarget::PostedData& properties)
{
	for (const auto& property : properties)
	{
		const auto& propName = property.first;
		const auto& propVal = property.second;
		ASSERT(!propName.empty());

		auto itt = m_StringSetters.find(propName);
		if (itt != m_StringSetters.end())
		{
			for (auto& user : m_Users)
			{
				const auto& setter = itt->second;
				setter(user, boost::YOpt<PooledString>(propVal));
			}
		}
		else
		{
			auto ittDateSetter = m_DateSetters.find(propName);
			ASSERT(ittDateSetter != m_DateSetters.end());
			if (ittDateSetter != m_DateSetters.end())
			{
				YTimeDate propTimeDateVal;
				TimeUtil::GetInstance().GetTimedateFromISO8601String(propVal, propTimeDateVal);
				for (auto& user : m_Users)
				{
					const auto& setter = ittDateSetter->second;
					setter(user, boost::YOpt<YTimeDate>(propTimeDateVal));
				}
			}
		}
	}

	return true;
}

void DlgEducationUserEditHTML::addDateEditor(DateGetter p_Getter, DateSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const boost::YOpt<wstring>& p_MaxDate)
{
	addObjectDateEditor<EducationUser>(m_Users
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags
		, [p_Flags](const EducationUser& curUser)
		{
			return 0; // TODO
			// getRestrictionsImpl(bg, p_Flags, p_ApplicableGroupTypes);
		}
		, false
		, p_MaxDate
	);

	ASSERT(!hasProperty(p_PropName));
	m_DateSetters[p_PropName] = p_Setter;
}

void DlgEducationUserEditHTML::addStringEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags)
{
	addObjectStringEditor<EducationUser>(m_Users
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags
		, [p_Flags](const EducationUser& curUser)
		{
			return 0; // TODO
			// getRestrictionsImpl(bg, p_Flags, p_ApplicableGroupTypes);
		}
	);

	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

void DlgEducationUserEditHTML::addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues)
{
	addObjectListEditor<EducationUser>(m_Users
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags
		, p_LabelsAndValues
		, [p_Flags](const EducationUser& curUser)
		{
			return 0; // TODO
			//return getRestrictionsImpl(bu, p_Flags, p_ApplicableUserTypes);
		}
	);
	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

bool DlgEducationUserEditHTML::hasProperty(const wstring& p_PropertyName) const
{
	return m_StringSetters.find(p_PropertyName) != m_StringSetters.end() ||
		m_DateSetters.find(p_PropertyName) != m_DateSetters.end();
}
