#include "GroupListFullPropertySet.h"

#include "Group.h"
#include "MFCUtil.h"
#include "MsGraphFieldNames.h"

vector<rttr::property> GroupListFullPropertySet::GetPropertySet() const
{
    // FIXME: Don't use "Group" class neither rttr::property
    Group dummy;
    return dummy.GetListProperties(dummy);
}

vector<rttr::property> DeletedGroupListFullPropertySet::GetPropertySet() const
{
	auto propSet = GroupListFullPropertySet::GetPropertySet();

	// Same as GroupListFullPropertySet::GetPropertySet(), just add O365_GROUP_DELETEDDATETIME
	propSet.push_back(rttr::type::get<User>().get_property(O365_GROUP_DELETEDDATETIME));

	return propSet;
}

