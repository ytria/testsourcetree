#include "AddApplicationPasswordRequester.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

using namespace web;

AddApplicationPasswordRequester::AddApplicationPasswordRequester(const wstring& p_PwdDisplayName, const wstring& p_Id)
	:m_Id(p_Id),
	m_PwdDisplayName(p_PwdDisplayName)
{
}

TaskWrapper<void> AddApplicationPasswordRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<SingleRequestResult>();
	m_Deserializer = std::make_shared<PasswordCredentialDeserializer>();

	web::uri_builder uri;
	uri.append_path(_YTEXT("applications"));
	uri.append_path(m_Id);
	uri.append_path(_YTEXT("addPassword"));

	json::value payloadJson = json::value::object();

	json::value content = json::value::object();
	content.as_object()[_YTEXT("displayName")] = json::value::string(m_PwdDisplayName);
	content.as_object()[_YTEXT("endDateTime")] = json::value::string(_YTEXT("2200-01-01T12:00:00Z"));

	payloadJson.as_object()[_YTEXT("passwordCredential")] = content;

	WebPayloadJSON payload(payloadJson);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_uri(), httpLogger, p_TaskData, payload).ThenByTask([result = m_Result, deserializer = m_Deserializer](pplx::task<RestResultInfo> p_ResInfo) {
		try
		{
			result->SetResult(p_ResInfo.get());
			auto body = result->GetResult().GetBodyAsString();
			ASSERT(body.is_initialized());
			if (body.is_initialized())
				deserializer->Deserialize(web::json::value::parse(*body));
		}
		catch (const RestException& e)
		{
			RestResultInfo info(e.GetRequestResult());
			info.m_RestException = std::make_shared<RestException>(e);
			result->SetResult(info);
		}
		catch (const std::exception& e)
		{
			RestResultInfo info;
			info.m_Exception = e;
			result->SetResult(info);
		}
	});
}

const SingleRequestResult& AddApplicationPasswordRequester::GetResult() const
{
	return *m_Result;
}

const PasswordCredential& AddApplicationPasswordRequester::GetData() const
{
	return m_Deserializer->GetData();
}
