#include "BusinessLifeCyclePolicies.h"
#include "MSGraphSession.h"
#include "O365AdminUtil.h"
#include "safeTaskCall.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

using namespace Util;

const wstring& BusinessLifeCyclePolicies::g_StatusNoPolicyAvailable = BusinessLifeCyclePolicies::g_StatusNoPolicyAvailableValue;
const wstring& BusinessLifeCyclePolicies::g_StatusPolicyIsNone = BusinessLifeCyclePolicies::g_StatusPolicyIsNoneValue;
const wstring& BusinessLifeCyclePolicies::g_StatusApplied = BusinessLifeCyclePolicies::g_StatusAppliedValue;
const wstring& BusinessLifeCyclePolicies::g_StatusNotApplied = BusinessLifeCyclePolicies::g_StatusNotAppliedValue;
const wstring& BusinessLifeCyclePolicies::g_StatusPolicyError = BusinessLifeCyclePolicies::g_StatusPolicyErrorValue;

wstring BusinessLifeCyclePolicies::g_StatusNoPolicyAvailableValue;
wstring BusinessLifeCyclePolicies::g_StatusPolicyIsNoneValue;
wstring BusinessLifeCyclePolicies::g_StatusAppliedValue;
wstring BusinessLifeCyclePolicies::g_StatusNotAppliedValue;
wstring BusinessLifeCyclePolicies::g_StatusPolicyErrorValue;

RTTR_REGISTRATION
{
    using namespace rttr;

registration::class_<BusinessLifeCyclePolicies>("Groups Expiration Policy") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Groups Expiration Policy"))))
		.constructor()(policy::ctor::as_object)
		.property("groupLifetimeInDays", &BusinessLifeCyclePolicies::m_NumberOfDays)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property("managedGroupTypes", &BusinessLifeCyclePolicies::m_ManagedGroupTypes)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property("alternateNotificationEmails", &BusinessLifeCyclePolicies::m_AlternativeNotificationEmails)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		;
}

BusinessLifeCyclePolicies::BusinessLifeCyclePolicies()
{
	// Localize statics
	if (g_StatusAppliedValue.empty())
	{
		g_StatusNoPolicyAvailableValue = YtriaTranslate::Do(BusinessLifeCyclePolicies_BusinessLifeCyclePolicies_1, _YLOC("No Policy")).c_str();
		g_StatusPolicyIsNoneValue = YtriaTranslate::Do(BusinessLifeCyclePolicies_BusinessLifeCyclePolicies_2, _YLOC("None")).c_str();
		g_StatusAppliedValue = YtriaTranslate::Do(BusinessLifeCyclePolicies_BusinessLifeCyclePolicies_3, _YLOC("Applied")).c_str();
		g_StatusNotAppliedValue = YtriaTranslate::Do(BusinessLifeCyclePolicies_BusinessLifeCyclePolicies_4, _YLOC("Not applied")).c_str();
		g_StatusPolicyErrorValue = YtriaTranslate::Do(BusinessLifeCyclePolicies_BusinessLifeCyclePolicies_5, _YLOC("Error")).c_str();
	}
}

BusinessLifeCyclePolicies::BusinessLifeCyclePolicies(const LifeCyclePolicies& p_LCP)
	: BusinessLifeCyclePolicies()
{
    SetID(p_LCP.m_Id);
	m_NumberOfDays = p_LCP.m_NumberOfDays;
	m_ManagedGroupTypes = p_LCP.m_ManagedGroupTypes;
	m_AlternativeNotificationEmails = p_LCP.m_AlternativeNotificationEmails;
}

const boost::YOpt<PooledString>& BusinessLifeCyclePolicies::GetNumberOfDaysAsString() const
{
	if (m_NumberOfDays)
		m_NumberOfDaysAsString = Str::getStringFromNumber(*m_NumberOfDays);
	else
		m_NumberOfDaysAsString = boost::none;
	return m_NumberOfDaysAsString;
}

void BusinessLifeCyclePolicies::SetNumberOfDaysAsString(const boost::YOpt<PooledString>& p_Days)
{
	if (p_Days)
		SetNumberOfDays(Str::getIntFromString(*p_Days));
	else
		SetNumberOfDays(boost::none);
}

const boost::YOpt<int32_t>& BusinessLifeCyclePolicies::GetNumberOfDays() const
{
	return m_NumberOfDays;
}

void BusinessLifeCyclePolicies::SetNumberOfDays(const boost::YOpt<int32_t>& p_Days)
{
	m_NumberOfDays = p_Days;
}

const boost::YOpt<PooledString>& BusinessLifeCyclePolicies::GetManagedGroupTypes() const
{
	return m_ManagedGroupTypes;
}

void BusinessLifeCyclePolicies::SetManagedGroupTypes(const boost::YOpt<PooledString>& p_Types)
{
	m_ManagedGroupTypes = p_Types;
}

const boost::YOpt<PooledString>& BusinessLifeCyclePolicies::GetAlternativeNotificationEmails() const
{
	return m_AlternativeNotificationEmails;
}

void BusinessLifeCyclePolicies::SetAlternativeNotificationEmails(const boost::YOpt<PooledString>& p_Emails)
{
	m_AlternativeNotificationEmails = p_Emails;
}

bool BusinessLifeCyclePolicies::IsPresent() const
{
	return !m_Id.IsEmpty() && !GetError();
}

LifeCyclePolicies BusinessLifeCyclePolicies::ToLifeCyclePolicies(const BusinessLifeCyclePolicies & p_BusinessLifeCyclePolicies)
{
	LifeCyclePolicies LCP;

	LCP.m_Id = p_BusinessLifeCyclePolicies.GetID();
	LCP.m_NumberOfDays = p_BusinessLifeCyclePolicies.m_NumberOfDays;
	LCP.m_ManagedGroupTypes = p_BusinessLifeCyclePolicies.m_ManagedGroupTypes;
	LCP.m_AlternativeNotificationEmails = p_BusinessLifeCyclePolicies.m_AlternativeNotificationEmails;

    return LCP;
}

pplx::task<vector<HttpResultWithError>> BusinessLifeCyclePolicies::SendEditRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	LifeCyclePolicies LCP;
	const auto creatableProperties = GetUpdatableProperties<LifeCyclePolicies, BusinessLifeCyclePolicies>(LCP, { { { MetadataKeys::ReadOnly, false } } }, {});
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	auto task = safeTaskCall(LCP.Update(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);

	return pplx::task_from_result(vector<HttpResultWithError>{ task.get() });
}

TaskWrapper<HttpResultWithError> BusinessLifeCyclePolicies::SendCreateRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	LifeCyclePolicies LCP;
	const auto creatableProperties = GetUpdatableProperties<LifeCyclePolicies, BusinessLifeCyclePolicies>(LCP, { { { MetadataKeys::ValidForCreation, true } } }, {});
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
    return safeTaskCall(LCP.Create(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, creatableProperties).Then([](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessLifeCyclePolicies::SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	LifeCyclePolicies LCP = BusinessLifeCyclePolicies::ToLifeCyclePolicies(*this);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
    return safeTaskCall(LCP.Delete(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}
