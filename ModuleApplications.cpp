#include "ModuleApplications.h"
#include "GridApplications.h"
#include "RefreshSpecificData.h"
#include "FrameApplications.h"
#include "BasicPageRequestLogger.h"
#include "ApplicationListRequester.h"
#include "Application.h"

void ModuleApplications::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
	case Command::ModuleTask::List:
		showApplications(p_Command);
		break;
	}
}

void ModuleApplications::doRefresh(FrameApplications* p_Frame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command)
{
	GridApplications* grid = dynamic_cast<GridApplications*>(&p_Frame->GetGrid());
	RefreshSpecificData refreshSpecificData;
	if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
	{
		ASSERT(p_IsUpdate);
		refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();
	}

	YSafeCreateTask([p_Command, p_Frame, p_TaskData, hwnd = p_Frame->GetSafeHwnd(), moduleCriteria = p_Frame->GetModuleCriteria(), p_Action, p_IsUpdate, grid, refreshSpecificData, bom = p_Frame->GetBusinessObjectManager()]() {
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("applications"));
		auto requester = std::make_shared<ApplicationListRequester>(logger);

		vector<Application> apps = safeTaskCall(requester->Send(bom->GetSapio365Session(), p_TaskData).Then([requester]() {
			return requester->GetData();
			}), bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ListErrorHandler<Application>, p_TaskData).get();

			bool partialRefresh = !refreshSpecificData.m_Criteria.m_IDs.empty();
			const auto& criteria = partialRefresh ? refreshSpecificData.m_Criteria : moduleCriteria;
			YCallbackMessage::DoPost([=]() {
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameApplications*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, p_TaskData.IsCanceled(), p_TaskData, false))
				{
					ASSERT(nullptr != frame);
					if (nullptr != frame)
					{
						frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s applications...", Str::getStringFromNumber(apps.size()).c_str()));
						frame->GetGrid().ClearLog(p_TaskData.GetId());

						{
							CWaitCursor _;
							frame->BuildView(apps, !partialRefresh, false);
						}

						if (!p_IsUpdate)
						{
							frame->UpdateContext(p_TaskData, hwnd);
							shouldFinishTask = false;
						}
					}
				}

				if (shouldFinishTask)
				{
					// Because we didn't (and can't) call UpdateContext here.
					ModuleBase::TaskFinished(frame, p_TaskData, p_Action);
				}
			});
	});
}

void ModuleApplications::showApplications(Command p_Command)
{
	AutomationAction* p_Action = p_Command.GetAutomationAction();
	const CommandInfo& info = p_Command.GetCommandInfo();
	const auto p_Origin = info.GetOrigin();

	RefreshSpecificData refreshSpecificData;
	bool isUpdate = false;

	auto p_SourceWindow = p_Command.GetSourceWindow();

	auto sourceCWnd = p_SourceWindow.GetCWnd();

	FrameApplications* frame = dynamic_cast<FrameApplications*>(info.GetFrame());
	if (nullptr == frame)
	{
		ASSERT(!p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>());

		if (FrameApplications::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin = p_Origin;
			moduleCriteria.m_Privilege = info.GetRBACPrivilege();

			if (ShouldCreateFrame<FrameApplications>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				frame = new FrameApplications(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), _T("Applications"), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
		}
	}
	else
	{
		isUpdate = true;
	}

	if (nullptr != frame)
	{
		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleSignIns_showSignIns_2, _YLOC("Show Applications")).c_str(), frame, p_Command, !isUpdate);
		doRefresh(frame, p_Action, taskData, isUpdate, p_Command);
	}
	else
	{
		SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
}
