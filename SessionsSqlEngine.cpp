#include "SessionsSqlEngine.h"
#include "SqlQuerySelect.h"
#include "ExistsRowHandler.h"
#include "SqlQueryPreparedStatement.h"
#include "InlineSqlQueryRowHandler.h"
#include "TimeUtil.h"
#include "SessionTypes.h"
#include "OAuth2AuthenticatorBase.h"
#include "Sapio365Session.h"
#include "MigrationUtil.h"
#include "QueryGetCredentialsId.h"
#include "QueryUpdateSessionType.h"
#include "QueryDeleteSession.h"

std::unique_ptr<SessionsSqlEngine> SessionsSqlEngine::g_Instance;

SessionsSqlEngine::SessionsSqlEngine(const wstring& p_DBfile)
	: O365SQLiteEngine(p_DBfile)
{
}

vector<SQLiteUtil::Table> SessionsSqlEngine::GetTables() const
{
	return vector<SQLiteUtil::Table>();
}

vector<PersistentSession> SessionsSqlEngine::GetList()
{
	return GetList(wstring());
}

vector<PersistentSession> SessionsSqlEngine::GetList(const wstring& p_Type)
{
	vector<PersistentSession> result;

	wstring queryStr = _YTEXT(R"(SELECT EmailOrAppId, SessionType, DisplayName, AccessCount, CreatedOn, IsFavorite, UserFullName, LastUsed, ProfilePicture, TenantName, TenantDomain, ShowOwnScopeOnly, UseDeltaUsers, UseDeltaGroups, AutoLoadOnPrem, AskedAutoLoadOnPrem, UseOnPrem, DontAskAgainUseOnPrem, AADComputerName, UseRemoteRSAT, UseMsDsConsistencyGuid, ADDSServer, ADDSUsername, ADDSPassword, RoleId, Roles.Name, Credentials.Id, Credentials.AccessToken, Credentials.RefreshToken, Credentials.AppId, Credentials.AppTenant, Credentials.AppClientSecret FROM Sessions LEFT JOIN Roles ON Sessions.RoleId=Roles.Id LEFT JOIN Credentials ON Sessions.CredentialsId=Credentials.Id)");
	if (!p_Type.empty())
		queryStr += _YTEXTFORMAT(L" WHERE SessionType = '%s'", p_Type.c_str());

	auto handler = [&result, this](sqlite3_stmt* p_Stmt) {
		result.push_back(RowToSession(p_Stmt));
	};

	SqlQuerySelect query(*this, InlineSqlQueryRowHandler<decltype(handler)>(handler), queryStr);
	query.Run();

	return result;
}

PersistentSession SessionsSqlEngine::Load(const SessionIdentifier& p_Identifier)
{
	PersistentSession session;

	auto handler = [&session, this](sqlite3_stmt* p_Stmt) {
		session = RowToSession(p_Stmt);
	};

	wstring queryStr = _YTEXT(R"(SELECT EmailOrAppId, SessionType, DisplayName, AccessCount, CreatedOn, IsFavorite, UserFullName, LastUsed, ProfilePicture, TenantName, TenantDomain, ShowOwnScopeOnly, UseDeltaUsers, UseDeltaGroups, AutoLoadOnPrem, AskedAutoLoadOnPrem, UseOnPrem, DontAskAgainUseOnPrem, AADComputerName, UseRemoteRSAT, UseMsDsConsistencyGuid, ADDSServer, ADDSUsername, ADDSPassword, RoleId, Roles.Name, Credentials.Id, Credentials.AccessToken, Credentials.RefreshToken, Credentials.AppId, Credentials.AppTenant, Credentials.AppClientSecret FROM Sessions LEFT JOIN Roles ON Sessions.RoleId=Roles.Id LEFT JOIN Credentials ON Sessions.CredentialsId=Credentials.Id WHERE EmailOrAppId=? AND SessionType=? AND RoleId=?)");
	SqlQuerySelect query(*this, InlineSqlQueryRowHandler<decltype(handler)>(handler), queryStr);
	query.BindString(1, p_Identifier.m_EmailOrAppId);
	query.BindString(2, p_Identifier.m_SessionType);
	query.BindInt64(3, p_Identifier.m_RoleID);
	query.Run();

	return session;
}

PersistentSession SessionsSqlEngine::Load(int64_t p_SessionId)
{
	PersistentSession session;

	auto handler = [&session, this](sqlite3_stmt* p_Stmt) {
		session = RowToSession(p_Stmt);
	};

	wstring queryStr = _YTEXT(R"(SELECT EmailOrAppId, SessionType, DisplayName, AccessCount, CreatedOn, IsFavorite, UserFullName, LastUsed, ProfilePicture, TenantName, TenantDomain, ShowOwnScopeOnly, UseDeltaUsers, UseDeltaGroups, AutoLoadOnPrem, AskedAutoLoadOnPrem, UseOnPrem, DontAskAgainUseOnPrem, AADComputerName, UseRemoteRSAT, UseMsDsConsistencyGuid, ADDSServer, ADDSUsername, ADDSPassword, RoleId, Roles.Name, Credentials.Id, Credentials.AccessToken, Credentials.RefreshToken, Credentials.AppId, Credentials.AppTenant, Credentials.AppClientSecret FROM Sessions LEFT JOIN Roles ON Sessions.RoleId=Roles.Id LEFT JOIN Credentials ON Sessions.CredentialsId=Credentials.Id WHERE Sessions.Id=?)");
	SqlQuerySelect query(*this, InlineSqlQueryRowHandler<decltype(handler)>(handler), queryStr);
	query.BindInt64(1, p_SessionId);
	query.Run();

	return session;
}

PersistentSession SessionsSqlEngine::LoadWithAnyRole(const SessionIdentifier& p_Identifier)
{
	ASSERT(p_Identifier.m_SessionType == SessionTypes::g_StandardSession || p_Identifier.m_SessionType == SessionTypes::g_Role);
	PersistentSession session;

	auto handler = [&session, this](sqlite3_stmt* p_Stmt) {
		// Only keeping the first result.
		if (!session.IsValid())
		{
			session = RowToSession(p_Stmt);
		}
		else
		{
			ASSERT(session.GetCredentialsId() == RowToSession(p_Stmt).GetCredentialsId());
		}
	};

	wstring queryStr = _YTEXT(R"(SELECT EmailOrAppId, SessionType, DisplayName, AccessCount, CreatedOn, IsFavorite, UserFullName, LastUsed, ProfilePicture, TenantName, TenantDomain, ShowOwnScopeOnly, UseDeltaUsers, UseDeltaGroups, AutoLoadOnPrem, AskedAutoLoadOnPrem, UseOnPrem, DontAskAgainUseOnPrem, AADComputerName, UseRemoteRSAT, UseMsDsConsistencyGuid, ADDSServer, ADDSUsername, ADDSPassword, RoleId, Roles.Name, Credentials.Id, Credentials.AccessToken, Credentials.RefreshToken, Credentials.AppId, Credentials.AppTenant, Credentials.AppClientSecret FROM Sessions LEFT JOIN Roles ON Sessions.RoleId=Roles.Id LEFT JOIN Credentials ON Sessions.CredentialsId=Credentials.Id WHERE EmailOrAppId=? AND (SessionType=? OR SessionType=?) )");
	SqlQuerySelect query(*this, InlineSqlQueryRowHandler<decltype(handler)>(handler), queryStr);
	query.BindString(1, p_Identifier.m_EmailOrAppId);
	query.BindString(2, SessionTypes::g_StandardSession);
	query.BindString(3, SessionTypes::g_Role);
	query.Run();

	return session;
}

void SessionsSqlEngine::Save(const PersistentSession& p_Session)
{
	StartTransaction();
	
	// Role table
	if (p_Session.IsRole())
	{
		ASSERT(p_Session.GetRoleId() != 0);
		if (p_Session.GetRoleId() != 0)
		{
			wstring queryStr = _YTEXT(R"(INSERT INTO "Roles"(Id, Name) VALUES(?, ?) ON CONFLICT(id) DO UPDATE SET Name=excluded.Name)");
			SqlQueryPreparedStatement query(*this, queryStr);
			query.BindInt64(1, p_Session.GetRoleId());
			query.BindString(2, p_Session.GetRoleNameInDb());
			query.Run();
		}
	}

	// Credentials table
	if (p_Session.GetCredentialsId() != 0)
	{
		wstring queryStr = _YTEXT(R"(INSERT INTO "Credentials"(Id, AccessToken, RefreshToken, AppId, AppTenant, AppClientSecret) VALUES(?, ?, ?, ?, ?, ?) ON CONFLICT(id) DO UPDATE SET AccessToken=excluded.AccessToken, RefreshToken=excluded.RefreshToken, AppId=excluded.AppId, AppTenant=excluded.AppTenant, AppClientSecret=excluded.AppClientSecret)");
		SqlQueryPreparedStatement query(*this, queryStr);
		query.BindInt64(1, p_Session.GetCredentialsId());
		query.BindString(2, p_Session.GetAccessToken());
		query.BindString(3, p_Session.GetRefreshToken());
		query.BindString(4, p_Session.GetAppId());
		query.BindString(5, p_Session.GetTenantName());
		query.BindString(6, p_Session.GetAppClientSecret());
		query.Run();
	}

	// Sessions table
	wstring queryStr = _YTEXT(R"(INSERT INTO "Sessions"(EmailOrAppId, SessionType, DisplayName, AccessCount, CreatedOn, IsFavorite, UserFullName, LastUsed, ProfilePicture, TenantName, TenantDomain, ShowOwnScopeOnly, UseDeltaUsers, UseDeltaGroups, AutoLoadOnPrem, AskedAutoLoadOnPrem, UseOnPrem, DontAskAgainUseOnPrem, AADComputerName, UseRemoteRSAT, UseMsDsConsistencyGuid, ADDSServer, ADDSUsername, ADDSPassword, RoleId, CredentialsId) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON CONFLICT(EmailOrAppId, SessionType, RoleId) DO UPDATE SET DisplayName=excluded.DisplayName, AccessCount=excluded.AccessCount, CreatedOn=excluded.CreatedOn, IsFavorite=excluded.IsFavorite, UserFullName=excluded.UserFullName, LastUsed=excluded.LastUsed, ProfilePicture=excluded.ProfilePicture, TenantName=excluded.TenantName, TenantDomain=excluded.TenantDomain, CredentialsId=excluded.CredentialsId, ShowOwnScopeOnly=excluded.ShowOwnScopeOnly,UseDeltaUsers=excluded.UseDeltaUsers,UseDeltaGroups=excluded.UseDeltaGroups,AutoLoadOnPrem=excluded.AutoLoadOnPrem, AskedAutoLoadOnPrem=excluded.AskedAutoLoadOnPrem,UseOnPrem=excluded.UseOnPrem, DontAskAgainUseOnPrem=excluded.DontAskAgainUseOnPrem, AADComputerName=excluded.AADComputerName, UseRemoteRSAT=excluded.UseRemoteRSAT, UseMsDsConsistencyGuid=excluded.UseMsDsConsistencyGuid, ADDSServer=excluded.ADDSServer, ADDSUserName=excluded.ADDSUserName, ADDSPassword=excluded.ADDSPassword)");

	SqlQueryPreparedStatement query(*this, queryStr);
	query.BindString(1, p_Session.GetEmailOrAppId());
	query.BindString(2, p_Session.GetSessionType());
	query.BindString(3, p_Session.GetSessionName());
	query.BindInt(4, p_Session.GetAccessCount());
	query.BindString(5, TimeUtil::GetInstance().GetISO8601String(p_Session.GetCreatedOn()));
	query.BindInt(6, p_Session.IsFavorite());
	query.BindString(7, p_Session.GetFullname());
	query.BindString(8, TimeUtil::GetInstance().GetISO8601String(p_Session.GetLastUsedOn()));
	query.BindBlob(9, p_Session.GetProfilePhoto().data(), static_cast<int>(p_Session.GetProfilePhoto().size()));
	query.BindString(10, p_Session.GetTenantDisplayName());
	query.BindString(11, p_Session.GetTenantName());
	query.BindInt(12, p_Session.GetRoleShowOwnScopeOnly());
	query.BindInt(13, p_Session.UseDeltaUsers());
	query.BindInt(14, p_Session.UseDeltaGroups());
	query.BindInt(15, p_Session.GetAutoLoadOnPrem());
	query.BindInt(16, p_Session.GetAskedAutoLoadOnPrem());
	query.BindInt(17, p_Session.GetUseOnPrem());
	query.BindInt(18, p_Session.GetDontAskAgainUseOnPrem());
	query.BindString(19, p_Session.GetAADComputerName());
	query.BindInt(20, p_Session.GetUseRemoteRSAT());
	query.BindInt(21, p_Session.GetUseMsDsConsistencyGuid());
	query.BindString(22, p_Session.GetADDSServer());
	query.BindString(23, p_Session.GetADDSUsername());
	query.BindString(24, p_Session.GetADDSPassword());
	query.BindInt64(25, p_Session.GetRoleId());
	if (p_Session.GetCredentialsId() != 0)
		query.BindInt64(26, p_Session.GetCredentialsId());
	else
		query.BindNull(26);
	query.Run();

	EndTransaction();
}

void SessionsSqlEngine::Logout(const SessionIdentifier& p_Identifier)
{
	if (p_Identifier.m_SessionType != SessionTypes::g_ElevatedSession)
	{
		DeleteCredentials(p_Identifier);
	}
	else
	{
		QueryGetCredentialsId queryGetCreds(SessionsSqlEngine::Get(), p_Identifier);
		queryGetCreds.Run();

		// Just remove tokens and password, but keep app id.
		const wstring updateQueryStr = _YTEXT(R"(UPDATE "Credentials" SET AccessToken="",RefreshToken="",AppClientSecret="" WHERE Id=?)");
		SqlQueryPreparedStatement queryUpdate(*this, updateQueryStr);
		queryUpdate.BindInt64(1, queryGetCreds.GetCredentialsId());
		queryUpdate.Run();

		// Convert back to advanced session
		QueryUpdateSessionType queryDowngrade(*this, p_Identifier, SessionTypes::g_AdvancedSession);
		queryDowngrade.Run();
	}
}

void SessionsSqlEngine::DeleteCredentials(const SessionIdentifier& p_Identifier)
{
	// Get credentials id
	int64_t sessionId = 0;
	int64_t credId = 0;
	auto credIdHandler = [&sessionId, &credId](sqlite3_stmt* p_Stmt) {
		sessionId = sqlite3_column_int64(p_Stmt, 0);
		credId = sqlite3_column_int64(p_Stmt, 1);
	};
	wstring queryGetCredIdStr(_YTEXT(R"(SELECT Id, CredentialsId FROM "Sessions" WHERE EmailOrAppId=? AND SessionType=? AND RoleId=?)"));
	SqlQuerySelect queryGetCredId(*this, InlineSqlQueryRowHandler<decltype(credIdHandler)>(credIdHandler), queryGetCredIdStr);
	queryGetCredId.BindString(1, p_Identifier.m_EmailOrAppId);
	queryGetCredId.BindString(2, p_Identifier.m_SessionType);
	queryGetCredId.BindInt64(3, p_Identifier.m_RoleID);
	queryGetCredId.Run();

	// Update credentials field in session
	wstring updateQueryStr(_YTEXT(R"(UPDATE "Sessions" SET CredentialsId=NULL WHERE CredentialsId=?)"));
	SqlQueryPreparedStatement updateQuery(*this, updateQueryStr);
	updateQuery.BindInt64(1, credId);
	updateQuery.Run();
	ASSERT(updateQuery.GetNbRowsChanged() >= 1);

	// Delete credentials from credentials table
	const wstring deleteQueryStr = _YTEXT(R"(DELETE FROM "Credentials" WHERE Id=?)");
	SqlQueryPreparedStatement deleteQuery(*this, deleteQueryStr);
	deleteQuery.BindInt64(1, credId);
	deleteQuery.Run();
	ASSERT(deleteQuery.GetNbRowsChanged() == 1);
}

void SessionsSqlEngine::DowngradeElevatedSession(const SessionIdentifier& p_Identifier)
{
	auto session = Load(p_Identifier);

	if (session.IsElevated())
		session.SetSessionType(SessionTypes::g_AdvancedSession);
	else if (session.IsPartnerElevated())
		session.SetSessionType(SessionTypes::g_PartnerAdvancedSession);
	else
		ASSERT(false);
	SessionsSqlEngine::Get().Save(session);

	QueryDeleteSession queryDelete(SessionsSqlEngine::Get(), p_Identifier);
	queryDelete.Run();
}

bool SessionsSqlEngine::SessionExists(const SessionIdentifier& p_Identifier)
{
	// Check if a session exists with the corresponding email or app id (with or without a role)
	ExistsRowHandler handler;
	SqlQuerySelect query(*this, handler, _YTEXT(R"(SELECT 1 FROM "Sessions" WHERE EmailOrAppId = ? AND SessionType=? AND RoleId=?)"));
	query.BindString(1, p_Identifier.m_EmailOrAppId);
	query.BindString(2, p_Identifier.m_SessionType);
	query.BindInt64(3, p_Identifier.m_RoleID);
	query.Run();

	return handler.Exists();
}

bool SessionsSqlEngine::HasSharedCredentials(const SessionIdentifier& p_Identifier)
{
	int credId = 0;
	auto handler = [&credId](sqlite3_stmt* p_Stmt) {
		credId = sqlite3_column_int(p_Stmt, 0);
	};

	const wstring queryStr = _YTEXT(R"(SELECT CredentialsId FROM "Sessions" WHERE EmailOrAppId=? AND SessionType=? AND RoleId=?)");
	SqlQuerySelect query(*this, InlineSqlQueryRowHandler<decltype(handler)>(handler), queryStr);
	query.BindString(1, p_Identifier.m_EmailOrAppId);
	query.BindString(2, p_Identifier.m_SessionType);
	query.BindInt64(3, p_Identifier.m_RoleID);
	query.Run();

	return HasSharedCredentials(credId);
}

bool SessionsSqlEngine::HasSharedCredentials(int64_t p_CredId)
{
	int count = 0;
	auto handler = [&count](sqlite3_stmt* p_Stmt) {
		count = sqlite3_column_int(p_Stmt, 0);
	};

	const wstring queryStr = _YTEXT(R"(SELECT COUNT(*) FROM "Sessions" WHERE CredentialsId=?)");
	SqlQuerySelect query(*this, InlineSqlQueryRowHandler<decltype(handler)>(handler), queryStr);
	query.BindInt64(1, p_CredId);
	query.Run();
	ASSERT(count != 0);

	return count > 1;
}

int64_t SessionsSqlEngine::GetRoleIdFromRoleName(const wstring& p_RoleName)
{
	const wstring queryStr = _YTEXT(R"(SELECT Id FROM "Roles" WHERE "Name" = ?)");

	int64_t roleId = 0;
	auto handler = [&roleId](sqlite3_stmt* p_Stmt) {
		roleId = sqlite3_column_int(p_Stmt, 1);
	};

	SqlQuerySelect query(*this, InlineSqlQueryRowHandler<decltype(handler)>(handler), queryStr);
	query.BindString(1, p_RoleName);
	query.Run();

	ASSERT(roleId != 0);
	return roleId;
}

SessionsSqlEngine& SessionsSqlEngine::Get()
{
	ASSERT(IsInit());
	return *g_Instance;
}

void SessionsSqlEngine::Init()
{
	ASSERT(!IsInit());
	g_Instance = CreateEngine(MigrationUtil::GetCurrentMigrationVersion());
}

bool SessionsSqlEngine::IsInit()
{
	return (bool)g_Instance;
}

std::unique_ptr<SessionsSqlEngine> SessionsSqlEngine::CreateEngine(int32_t p_Version)
{
	return std::make_unique<SessionsSqlEngine>(MigrationUtil::GetMigrationFileName(p_Version));
}

PersistentSession SessionsSqlEngine::RowToSession(sqlite3_stmt* p_Stmt)
{
	PersistentSession session;

	session.SetTechnicalName(GetStringFromColumn(0, p_Stmt));
	session.SetSessionType(GetStringFromColumn(1, p_Stmt));
	session.SetSessionName(GetStringFromColumn(2, p_Stmt));
	session.SetAccessCount(sqlite3_column_int(p_Stmt, 3));

	YTimeDate createdOn;
	TimeUtil::GetInstance().GetTimedateFromISO8601String(GetStringFromColumn(4, p_Stmt), createdOn);
	session.SetCreatedOn(createdOn);

	session.SetFavorite(sqlite3_column_int(p_Stmt, 5));
	session.SetFullname(GetStringFromColumn(6, p_Stmt));

	YTimeDate lastUsedOn;
	TimeUtil::GetInstance().GetTimedateFromISO8601String(GetStringFromColumn(7, p_Stmt), lastUsedOn);
	session.SetLastUsedOn(lastUsedOn);

	int blobSize = sqlite3_column_bytes(p_Stmt, 8);
	const uint8_t* blobAddr = reinterpret_cast<const uint8_t*>(sqlite3_column_blob(p_Stmt, 8));
	session.SetProfilePhoto(vector<uint8_t>(blobAddr, blobAddr + blobSize));


	session.SetTenantDisplayName(GetStringFromColumn(9, p_Stmt));
	session.SetTenantName(GetStringFromColumn(10, p_Stmt));
	session.SetRoleShowOwnScopeOnly(sqlite3_column_int64(p_Stmt, 11));
	session.SetUseDeltaUsers(sqlite3_column_int(p_Stmt, 12));
	session.SetUseDeltaGroups(sqlite3_column_int(p_Stmt, 13));
	session.SetAutoLoadOnPrem(sqlite3_column_int(p_Stmt, 14));
	session.SetAskedAutoLoadOnPrem(sqlite3_column_int(p_Stmt, 15));
	session.SetUseOnPrem(sqlite3_column_int(p_Stmt, 16));
	session.SetDontAskAgainUseOnPrem(sqlite3_column_int(p_Stmt, 17));
	session.SetAADComputerName(GetStringFromColumn(18, p_Stmt));
	session.SetUseRemoteRSAT(sqlite3_column_int(p_Stmt, 19));
	session.SetUseMsDsConsistencyGuid(sqlite3_column_int(p_Stmt, 20));
	session.SetADDSServer(GetStringFromColumn(21, p_Stmt));
	session.SetADDSUsername(GetStringFromColumn(22, p_Stmt));
	session.SetADDSPassword(GetStringFromColumn(23, p_Stmt));

	session.SetRoleId(sqlite3_column_int64(p_Stmt, 24));
	session.SetRoleNameInDb(GetStringFromColumn(25, p_Stmt));
	session.SetCredentialsId(sqlite3_column_int64(p_Stmt, 26));

	if (sqlite3_column_type(p_Stmt, 27) != SQLITE_NULL)
		session.SetAccessToken(GetStringFromColumn(27, p_Stmt));
	if (sqlite3_column_type(p_Stmt, 28) != SQLITE_NULL)
		session.SetRefreshToken(GetStringFromColumn(28, p_Stmt));

	session.SetAppId(GetStringFromColumn(29, p_Stmt));
	// TODO: App tenant?
	session.SetAppClientSecret(GetStringFromColumn(31, p_Stmt));

	return session;
}
