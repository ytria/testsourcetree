#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "SharingLink.h"

class SharingLinkDeserializer : public JsonObjectDeserializer, public Encapsulate<SharingLink>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

