#pragma once
#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ResourceAccess.h"

class ResourceAccessDeserializer : public JsonObjectDeserializer, public Encapsulate<ResourceAccess>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

