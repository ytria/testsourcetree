#pragma once

#include "GridFrameBase.h"

#include "BusinessList.h"
#include "GridListItems.h"

class FrameListItems : public GridFrameBase
{
public:
	FrameListItems(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
    virtual ~FrameListItems() = default;
    DECLARE_DYNAMIC(FrameListItems)

    void ShowListItems(const O365DataMap<wstring, vector<BusinessList>>& p_Lists, Origin p_Origin, bool p_FullPurge);

    virtual O365Grid& GetGrid() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;
    virtual void createGrid() override;

protected:
	virtual O365ControlConfig GetRefreshPartialControlConfig() override;
	virtual AutomatedApp::AUTOMATIONSTATUS automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
    GridListItems m_ListItemsGrid;
};

