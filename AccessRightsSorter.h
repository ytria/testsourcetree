#pragma once

class AccessRightsSorter
{
public:
	enum SortOrder
	{
		FullAccess = 0, ExternalAccount, DeleteItem, ReadPermission, ChangePermission, ChangeOwner
	};

	bool operator()(const wstring& p_Str1, const wstring& p_Str2);
};

class AccessRightsFormatter
{
public:
	AccessRightsFormatter(const wstring& p_AccessRights);

	wstring Format();

private:
	wstring m_AccessRights;
};

