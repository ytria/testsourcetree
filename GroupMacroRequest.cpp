#include "GroupMacroRequest.h"

#include "DeletedGroupRequester.h"
#include "GroupEndpointsRequester.h"
#include "GroupSettingsRequester.h"
#include "GroupRequester.h"
#include "MsGraphHttpRequestLogger.h"
#include "MultiObjectsPageRequestLogger.h"
#include "MultiObjectsRequestLogger.h"
#include "RESTUtils.h"
#include "RunOnScopeEnd.h"
#include "safeTaskCall.h"
#include "Str.h"
#include "TeamDeserializer.h"
#include "TeamRequester.h"
#include "UserDeserializer.h"

using namespace Rest;

GroupMacroRequest::GroupMacroRequest(BusinessGroup& p_Group, const std::unordered_map<std::vector<GBI>, std::vector<rttr::property>>& p_Properties, boost::YOpt<BusinessLifeCyclePolicies> p_LCP, uint32_t p_GroupAdditionnalInfoFlags, std::shared_ptr<BusinessObjectManager> p_BOM, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
	: m_Group(p_Group)
	, m_Properties(p_Properties)
	, m_BOM(p_BOM)
	, m_TaskData(p_TaskData)
	, m_LCP(p_LCP)
	, m_Logger(p_Logger)
{
	ASSERT(!m_Group.GetID().IsEmpty());

	// We MUST pass at least one property list
	ASSERT(!m_Properties.empty() && !m_Properties.begin()->second.empty());
	size_t i = 0;
	for (auto& p : m_Properties)
	{
		addPropertiesStep(i, p.second, i > 0, p.first);
		++i;
	}

	const bool requestOnBehalf = isFlagSet(BusinessObjectManager::GroupAdditionalRequest::RequestOnBehalf, p_GroupAdditionnalInfoFlags);
	if (requestOnBehalf && m_Group.GetCombinedGroupType() && *m_Group.GetCombinedGroupType() == BusinessGroup::g_Office365Group)
		AddStep(CreateCreatedOnBehalfOfStep(m_Group, p_GroupAdditionnalInfoFlags));

	const bool requestTeam = isFlagSet(BusinessObjectManager::GroupAdditionalRequest::RequestTeam, p_GroupAdditionnalInfoFlags);
	if (requestTeam && m_Group.GetCombinedGroupType() && *m_Group.GetCombinedGroupType() == BusinessGroup::g_Office365Group)
		AddStep(CreateTeamStep(m_Group, p_GroupAdditionnalInfoFlags));

	// Request Group Settings for "Allow to add guests" information.
	if (isFlagSet(BusinessObjectManager::GroupAdditionalRequest::RequestGroupSettings, p_GroupAdditionnalInfoFlags))
		AddStep(CreateGroupSettingsStep(m_Group, p_GroupAdditionnalInfoFlags));

	// Request Group Life Cycle Policy Status
	if (isFlagSet(BusinessObjectManager::GroupAdditionalRequest::RequestGroupLCP, p_GroupAdditionnalInfoFlags))
	{
		if (m_LCP && m_LCP->IsPresent() && m_LCP->m_ManagedGroupTypes)
		{
			if (*m_LCP->m_ManagedGroupTypes == _YTEXT("All"))
			{
				m_Group.SetLCPStatus(boost::YOpt<PooledString>(BusinessLifeCyclePolicies::g_StatusApplied));
			}
			else if (*m_LCP->m_ManagedGroupTypes == _YTEXT("None"))
			{
				m_Group.SetLCPStatus(boost::YOpt<PooledString>(BusinessLifeCyclePolicies::g_StatusPolicyIsNone));
			}
			else if (*m_LCP->m_ManagedGroupTypes == _YTEXT("Selected"))
			{
				// Set default value
				m_Group.SetLCPStatus(boost::YOpt<PooledString>());
				AddStep(CreateLifeCyclePoliciesStep(m_Group, p_GroupAdditionnalInfoFlags));
			}
		}
		else
		{
			if (m_Group.GetCombinedGroupType() && m_Group.GetCombinedGroupType().get() == BusinessGroup::g_Office365Group)
			{
				if (m_Group.IsFromRecycleBin())
				{
					// Set default value
					m_Group.SetLCPStatus(boost::YOpt<PooledString>());
					AddStep(CreateLifeCyclePoliciesStep(m_Group, p_GroupAdditionnalInfoFlags));
				}
				else
				{
					if (m_LCP && m_LCP->GetError())
						m_Group.SetLCPStatusError(m_LCP->GetError());
					else
						m_Group.SetLCPStatus(boost::YOpt<PooledString>(BusinessLifeCyclePolicies::g_StatusNoPolicyAvailable));
				}
			}
		}
	}

	if (isFlagSet(BusinessObjectManager::GroupAdditionalRequest::RequestOwners, p_GroupAdditionnalInfoFlags))
		AddStep(CreateOwnersStep(m_Group, p_GroupAdditionnalInfoFlags));

	if (isFlagSet(BusinessObjectManager::GroupAdditionalRequest::RequestMembersCount, p_GroupAdditionnalInfoFlags))
		AddStep(CreateMembersCountStep(m_Group, p_GroupAdditionnalInfoFlags));

	if (m_Group.GetCombinedGroupType() && *m_Group.GetCombinedGroupType() == BusinessGroup::g_Office365Group)
	{
		if (isFlagSet(BusinessObjectManager::GroupAdditionalRequest::RequestDrive, p_GroupAdditionnalInfoFlags))
			AddStep(CreateDriveStep(m_Group, p_GroupAdditionnalInfoFlags));

		if (isFlagSet(BusinessObjectManager::GroupAdditionalRequest::RequestIsYammer, p_GroupAdditionnalInfoFlags)
			&& !m_Group.GetIsTeam())
			AddStep(CreateIsYammerStep(m_Group, p_GroupAdditionnalInfoFlags));
	}
}

std::unique_ptr<MacroRequest::Step> GroupMacroRequest::CreateCreatedOnBehalfOfStep(BusinessGroup& p_Group, int p_Flags) const
{
	return std::make_unique<MacroRequest::Step>([&p_Group]()
	{
#if SKIP_NEXT_REQUESTS_ON_ERROR
		if (p_Group.GetError())
		{
			p_Group.SetUserCreatedOnBehalfOfError(SkippedError());
			return false;
		}
#endif
		return true; // Always process this step if no error previously
	},
		[&p_Group, session = m_BOM->GetSapio365Session(), &cache = m_BOM->GetGraphCache(), taskData = m_TaskData, logger = m_Logger]()
	{
		if (taskData.IsCanceled())
			return false;

		RunOnScopeEnd _([objName = logger->GetLogMessage(), logger]() { logger->SetLogMessage(objName); });
		logger->SetLogMessage(_T("group \"created on behalf of\" info"));

		boost::YOpt<SapioError> behalfError;
		auto deserializer = std::make_shared<UserDeserializer>(session);
		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, session->GetIdentifier());
		safeTaskCall(session->GetMSGraphSession(httpLogger->GetSessionType())->GetGroupCreatedOnBehalfOf(deserializer, p_Group.GetID(), logger, httpLogger, taskData), session->GetMSGraphSession(httpLogger->GetSessionType()), [&behalfError](const std::exception_ptr& p_ExceptPtr)
			{
				ASSERT(p_ExceptPtr);
				if (p_ExceptPtr)
				{
					try
					{
						std::rethrow_exception(p_ExceptPtr);
					}
					catch (const RestException& e)
					{
						behalfError = HttpError(e);
					}
					catch (const std::exception& e)
					{
						behalfError = SapioError(e);
					}
				}
			}, taskData).GetTask().wait();

			if (behalfError && behalfError->GetStatusCode() != 404)
			{
				//p_Group.SetDataError(GBI::CREATEDONBEHALFOF, behalfErrors);
				p_Group.SetUserCreatedOnBehalfOfError(behalfError);
			}
			else
			{
				const auto& createdOnBehalfOf = deserializer->GetData();
				if (!createdOnBehalfOf.m_Id.IsEmpty())
				{
					cache.AddUser(createdOnBehalfOf, true, { UBI::MIN });
					p_Group.SetUserCreatedOnBehalfOf(createdOnBehalfOf.GetID());
				}
			}

			p_Group.SetDataDate(GBI::CREATEDONBEHALFOF, taskData.GetLastRequestOn());

			return !taskData.IsCanceled();
		}
	);
}

std::unique_ptr<MacroRequest::Step> GroupMacroRequest::CreateTeamStep(BusinessGroup& p_Group, int p_Flags) const
{
	return std::make_unique<Step>
		(
			[&p_Group]()
			{
#if SKIP_NEXT_REQUESTS_ON_ERROR
				if (p_Group.GetError())
				{
					p_Group.SetTeamError(SkippedError());
					return false;
				}
#endif
				return true; // Always process this step if no error previously
			},
			[&p_Group, session = m_BOM->GetSapio365Session(), taskData = m_TaskData, logger = m_Logger]()
			{
				if (taskData.IsCanceled())
					return false;

				boost::YOpt<SapioError> teamError;
				auto requester = std::make_shared<TeamRequester>(p_Group.GetID(), logger);

				RunOnScopeEnd _([objName = logger->GetLogMessage(), logger]() { logger->SetLogMessage(objName); });
				logger->SetLogMessage(_T("group team"));

				bool hasReceivedTeamData = true;
				safeTaskCall(requester->Send(session, taskData), session->GetMSGraphSession(Sapio365Session::USER_SESSION), [&teamError, &hasReceivedTeamData](const std::exception_ptr& p_ExceptPtr)
					{
						ASSERT(p_ExceptPtr);
						if (p_ExceptPtr)
						{
							try
							{
								std::rethrow_exception(p_ExceptPtr);
							}
							catch (const RestException& e)
							{
								hasReceivedTeamData = false;
								HttpError httpErr(e);
								if (httpErr.GetStatusCode() != 404)
									teamError = httpErr;
							}
							catch (const std::exception& e)
							{
								hasReceivedTeamData = false;
								teamError = SapioError(e);
							}
						}
					}, taskData).get();

				p_Group.SetHasTeamInfo(!teamError);

				if (teamError)
				{
					//p_Group.SetDataError(GBI::TEAM, error);
					p_Group.SetTeamError(teamError);
				}
				else if (hasReceivedTeamData)
					p_Group.SetTeam(requester->GetData());

				p_Group.SetDataDate(GBI::TEAM, taskData.GetLastRequestOn());

				return !taskData.IsCanceled();
			}
		);
}

std::unique_ptr<MacroRequest::Step> GroupMacroRequest::CreateGroupSettingsStep(BusinessGroup & p_Group, int p_Flags) const
{
	return std::make_unique<Step>
		(
			[&p_Group]()
			{
#if SKIP_NEXT_REQUESTS_ON_ERROR
				if (p_Group.GetError())
				{
					p_Group.SetGroupSettingsError(SkippedError());
					return false;
				}
#endif
				return true; // Always process this step if no error previously
			},
			[&p_Group, session = m_BOM->GetSapio365Session(), taskData = m_TaskData, logger = m_Logger]()
			{
				if (taskData.IsCanceled())
					return false;

				boost::YOpt<SapioError> groupSettingsError;

				auto settingsLogger = std::make_shared<MultiObjectsPageRequestLogger>(*logger);
				settingsLogger->SetLogMessage(_T("group settings"));
				auto requester = std::make_shared<GroupSettingsRequester>(p_Group.GetID(), settingsLogger);
				safeTaskCall(requester->Send(session, taskData), session->GetMSGraphSession(Sapio365Session::USER_SESSION), [&groupSettingsError](const std::exception_ptr& p_ExceptPtr)
					{
						ASSERT(p_ExceptPtr);
						if (p_ExceptPtr)
						{
							try
							{
								std::rethrow_exception(p_ExceptPtr);
							}
							catch (const RestException& e)
							{
								HttpError err(e);
								if (err.GetStatusCode() != 404)
									groupSettingsError = err;
							}
							catch (const std::exception& e)
							{
								groupSettingsError = SapioError(e);
							}
						}
					}, taskData).GetTask().wait();

				if (groupSettingsError)
				{
					//p_Group.SetDataError(GBI::GROUPSETTINGS, groupSettingsError);
					p_Group.SetGroupSettingsError(groupSettingsError);
				}
				else
				{
					bool groupUnifiedGuestSettingTemplateActivated = false;
					for (const auto& groupSetting : requester->GetData())
					{
						ASSERT(groupSetting.GetTemplateId().is_initialized());
						if (groupSetting.GetTemplateId().is_initialized() && *groupSetting.GetTemplateId() == BusinessGroupSettingTemplate::g_GroupUnifiedGuestSettingTemplateId)
						{
							groupUnifiedGuestSettingTemplateActivated = true;
							p_Group.SetSettingAllowToAddGuestsId(groupSetting.GetID());
							for (const auto& property : groupSetting.GetValues())
							{
								if (property.Name.is_initialized() && *property.Name == BusinessGroupSettingTemplate::g_AllowToAddGuestsName)
								{
									ASSERT(property.Value.is_initialized());
									if (property.Value.is_initialized())
										p_Group.SetSettingAllowToAddGuests(Str::toLower(*property.Value) == _YTEXT("true"));
								}
							}
						}
					}
					p_Group.SetGroupUnifiedGuestSettingTemplateActivated(groupUnifiedGuestSettingTemplateActivated);
				}

				p_Group.SetDataDate(GBI::GROUPSETTINGS, taskData.GetLastRequestOn());


				return !taskData.IsCanceled();
			}
		);
}

std::unique_ptr<MacroRequest::Step> GroupMacroRequest::CreateLifeCyclePoliciesStep(BusinessGroup& p_Group, int p_Flags) const
{
	return std::make_unique<Step>
		(
			[&p_Group, lcp = m_LCP]()
			{
#if SKIP_NEXT_REQUESTS_ON_ERROR
				if (p_Group.GetError())
				{
					p_Group.SetLCPStatusError(SkippedError());
					return false;
				}
#endif
				return (p_Group.IsFromRecycleBin() || lcp && lcp->m_ManagedGroupTypes.get() == _YTEXT("Selected")) && (!p_Group.GetCombinedGroupType() || p_Group.GetCombinedGroupType().get() == BusinessGroup::g_Office365Group);
			},
			[&p_Group, bom = m_BOM, taskData = m_TaskData, lcp = m_LCP, logger = m_Logger]()
			{
				if (taskData.IsCanceled())
					return false;
				auto pageLogger = std::make_shared<MultiObjectsPageRequestLogger>(*logger);
				pageLogger->SetLogMessage(_T("group lifecycle policies"));
				const auto lifeCycleGroup = bom->GetBusinessLCP(p_Group.GetID(), pageLogger, taskData).GetTask().get();

				if (lifeCycleGroup.GetError() && lifeCycleGroup.GetError()->GetStatusCode() != 404)
				{
					//p_Group.SetDataError(GBI::LIFECYCLEPOLICIES, lifeCycleGroup.GetError());
					p_Group.SetLCPStatusError(lifeCycleGroup.GetError());
				}
				else if (lifeCycleGroup.IsPresent())
				{
					if (lifeCycleGroup.m_ManagedGroupTypes && lifeCycleGroup.m_ManagedGroupTypes.get() == _YTEXT("Selected"))
					{
						p_Group.SetLCPStatus(boost::YOpt<PooledString>(BusinessLifeCyclePolicies::g_StatusApplied));
					}
					else
					{
						ASSERT(false);
						p_Group.SetLCPStatus(boost::YOpt<PooledString>(BusinessLifeCyclePolicies::g_StatusNotApplied));
					}
				}
				else
				{
					p_Group.SetLCPStatus(boost::YOpt<PooledString>(BusinessLifeCyclePolicies::g_StatusNotApplied));
				}

				p_Group.SetDataDate(GBI::LIFECYCLEPOLICIES, taskData.GetLastRequestOn());

				return !taskData.IsCanceled();
			}
		);
}

std::unique_ptr<MacroRequest::Step> GroupMacroRequest::CreateOwnersStep(BusinessGroup& p_Group, int p_Flags) const
{
	return std::make_unique<Step>
		(
			[&p_Group]()
			{
#if SKIP_NEXT_REQUESTS_ON_ERROR
				if (p_Group.GetError())
				{
					p_Group.SetOwnersError(SkippedError());
					return false;
				}
#endif
				return true; // Always process this step if no error previously
			},
			[&p_Group, bom = m_BOM, taskData = m_TaskData, logger = m_Logger]()
			{
				if (taskData.IsCanceled())
					return false;
				
				BusinessGroup group;
				group.SetID(p_Group.GetID());

				auto pageLogger = std::make_shared<MultiObjectsPageRequestLogger>(*logger);
				pageLogger->SetLogMessage(_T("group owners hierarchy"));
				bom->GetBusinessGroupOwnersHierarchy(group, pageLogger, taskData, true);
				if (taskData.IsCanceled())
					return false;

				p_Group.ClearOwners();
				if (group.GetError() && group.GetError()->GetStatusCode() != 404)
				{
					p_Group.SetOwnersError(group.GetError());
				}
				else
				{
					BusinessUser tempUser;
					for (auto& ownerUser : group.GetOwnerUsers())
					{
						tempUser.SetID(ownerUser);
						p_Group.AddOwner(tempUser);
					}
				}

				p_Group.SetDataDate(GBI::OWNERS, taskData.GetLastRequestOn());


				return true;
			}
		);
}

std::unique_ptr<MacroRequest::Step> GroupMacroRequest::CreateMembersCountStep(BusinessGroup& p_Group, int p_Flags) const
{
	return std::make_unique<Step>
		(
			[&p_Group]()
			{
#if SKIP_NEXT_REQUESTS_ON_ERROR
				if (p_Group.GetError())
				{
					p_Group.SetOwnersError(SkippedError());
					return false;
				}
#endif
				return true; // Always process this step if no error previously
			},
			[&p_Group, bom = m_BOM, taskData = m_TaskData, logger = m_Logger]()
			{
				if (taskData.IsCanceled())
					return false;

				auto pageLogger = std::make_shared<MultiObjectsPageRequestLogger>(*logger);
				pageLogger->SetLogMessage(_T("group members"));

				boost::YOpt<SapioError> error;
				auto count = safeTaskCall(bom->CountGroupMembers(p_Group.GetID(), pageLogger, taskData), bom->GetMSGraphSession(Sapio365Session::USER_SESSION), [&error](const std::exception_ptr& p_ExceptPtr)
					{
						ASSERT(p_ExceptPtr);
						if (p_ExceptPtr)
						{
							try
							{
								std::rethrow_exception(p_ExceptPtr);
							}
							catch (const RestException& e)
							{
								error = HttpError(e);
							}
							catch (const std::exception& e)
							{
								error = SapioError(e);
							}
						}
						return 0;
					}, taskData).get();

					if (taskData.IsCanceled())
						return false;

					if (error)
					{
						//p_Group.SetDataError(GBI::MEMBERSCOUNT, error);
						ASSERT(false); // Handle?
					}
					else
						p_Group.SetMembersCount(count);

					p_Group.SetDataDate(GBI::MEMBERSCOUNT, taskData.GetLastRequestOn());

					return true;
			}
			);
}

std::unique_ptr<MacroRequest::Step> GroupMacroRequest::CreateDriveStep(BusinessGroup& p_Group, int p_Flags) const
{
	return std::make_unique<Step>
		(
			[&p_Group]()
			{
#if SKIP_NEXT_REQUESTS_ON_ERROR
				if (p_Group.GetError())
				{
					p_Group.SetDriveError(SkippedError());
					return false;
				}
#endif
				return true; // Always process this step if no error previously
			},
			[session = m_BOM->GetSapio365Session(), &p_Group, bom = m_BOM, taskData = m_TaskData, logger = m_Logger]()
			{
				logger->SetLogMessage(_T("group drive"));
				auto drive = bom->GetBusinessGroupDrive(p_Group.GetID(), logger, taskData).GetTask().get();
				if (taskData.IsCanceled())
					return false;
				p_Group.SetDrive(drive);
				if (drive.GetError())
				{
					//p_Group.SetDataError(GBI::DRIVE, drive.GetError());
					p_Group.SetDriveError(drive.GetError());
				}

				p_Group.SetDataDate(GBI::DRIVE, taskData.GetLastRequestOn());

				return true;
			}
		);
}

std::unique_ptr<MacroRequest::Step> GroupMacroRequest::CreateIsYammerStep(BusinessGroup& p_Group, int p_Flags) const
{
	return std::make_unique<Step>
		(
			[&p_Group]()
			{
#if SKIP_NEXT_REQUESTS_ON_ERROR
				if (p_Group.GetError())
				{
					p_Group.SetIsYammerError(SkippedError());
					return false;
				}
#endif
				return true; // Always process this step if no error previously
			},
			[session = m_BOM->GetSapio365Session(), &p_Group, bom = m_BOM, taskData = m_TaskData, logger = m_Logger]()
			{
				auto pageLogger = std::make_shared<MultiObjectsPageRequestLogger>(*logger);
				pageLogger->SetLogMessage(_T("group yammer endpoint"));
				boost::YOpt<SapioError> error;
				auto requester = std::make_shared<GroupEndpointsRequester>(p_Group.GetID(), pageLogger);
				safeTaskCall(requester->Send(session, taskData).Then([requester]() {
					// anything?
					}), session->GetMSGraphSession(Sapio365Session::APP_SESSION), 
						[&error](const std::exception_ptr& p_ExceptPtr)
					{
						ASSERT(p_ExceptPtr);
						if (p_ExceptPtr)
						{
							try
							{
								std::rethrow_exception(p_ExceptPtr);
							}
							catch (const RestException & e)
							{
								error = HttpError(e);
							}
							catch (const std::exception & e)
							{
								error = SapioError(e);
							}
							catch (...)
							{
								ASSERT(false);
							}
						}
					}
					, taskData).GetTask().wait();

				if (taskData.IsCanceled())
					return false;

				if (error)
				{
					//p_Group.SetDataError(GBI::ISYAMMER, error);
					p_Group.SetIsYammerError(error);
				}
				else
				{
					const auto isYammerGroup = std::any_of(requester->GetData().begin(), requester->GetData().end(), [](auto& ep)
						{
							return *ep.m_ProviderName == _YTEXT("Yammer")
								|| Str::beginsWith(*ep.m_ProviderResourceId, _YTEXT("Yammer."));
						});

					p_Group.SetIsYammer(isYammerGroup);
				}

				p_Group.SetDataDate(GBI::ISYAMMER, taskData.GetLastRequestOn());

				return true;
			}
		);
}

void GroupMacroRequest::addPropertiesStep(size_t index, const vector<rttr::property>& p_Properties, bool p_KeepOnlyNotNullvalues, const std::vector<GBI>& p_BlockIds)
{
	ASSERT(!p_Properties.empty());
	if (p_Properties.empty())
		return;

	const auto betaApi = p_Properties[0].get_metadata(PropertyMetaDataKey::BetaAPI).is_valid() && p_Properties[0].get_metadata(PropertyMetaDataKey::BetaAPI).get_value<bool>();
	// Property set must be consistent (no mix of beta/normal p_Properties).
	ASSERT(std::none_of(p_Properties.begin(), p_Properties.end(), [betaApi](const auto& p) { return betaApi != (p.get_metadata(PropertyMetaDataKey::BetaAPI).is_valid() && p.get_metadata(PropertyMetaDataKey::BetaAPI).get_value<bool>()); }));

	auto session = m_BOM->GetSapio365Session();
	auto& bg = m_Group;
	AddStep(std::make_unique<Step>
		(
			[&bg]()
			{
#if SKIP_NEXT_REQUESTS_ON_ERROR
				return !bg.GetError();
#else
				return true;
#endif
			}, // Always process this step if no error previously


			[&bg, session, p_Properties, p_KeepOnlyNotNullvalues, betaApi, taskData = m_TaskData, logger = m_Logger, index, p_BlockIds]()
			{
				if (taskData.IsCanceled())
					return false;

				boost::YOpt<SapioError> error;

				RunOnScopeEnd _([objName = logger->GetLogMessage(), logger]() { logger->SetLogMessage(objName); });
				logger->SetLogMessage(_YFORMAT(L"group properties (batch #%s)", std::to_wstring(index + 1).c_str()));

				std::unique_ptr<GroupRequester> groupSync(bg.IsFromRecycleBin()
															? new DeletedGroupRequester(bg.GetID(), p_Properties, logger)
															: new GroupRequester(bg.GetID(), p_Properties, logger));
				groupSync->SetUseBetaEndpoint(betaApi);
				safeTaskCall(groupSync->Send(session, taskData), session->GetMSGraphSession(Sapio365Session::USER_SESSION), [&error](const std::exception_ptr& p_ExceptPtr)
					{
						ASSERT(p_ExceptPtr);
						if (p_ExceptPtr)
						{
							try
							{
								std::rethrow_exception(p_ExceptPtr);
							}
							catch (const RestException& e)
							{
								error = HttpError(e);
							}
							catch (const std::exception& e)
							{
								error = SapioError(e);
							}
						}
					}, taskData).GetTask().wait();

				if (taskData.IsCanceled())
					return false;

				if (p_KeepOnlyNotNullvalues)
				{
					bg.MergeWith(groupSync->GetData());
				}
				else
				{
					const auto lcpStatus = bg.GetLCPStatus();
					const auto lcpError = bg.GetLCPStatusError();
					// FIXME: This is really bad, we can lose information previously set (reason for the two lines above).
					// We should only set values concerned by the request.
					bg = std::move(groupSync->GetData());
					bg.SetLCPStatus(lcpStatus);
					bg.SetLCPStatusError(lcpError);
				}
				if (error)
					bg.SetError(error);

				for (auto b : p_BlockIds)
				{
					/*if (error)
						bg.SetDataError(b, error);*/
					bg.SetDataDate(b, taskData.GetLastRequestOn());
				}

				return true;
			}
		)
	);
}
