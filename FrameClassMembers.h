#pragma once

#include "GridFrameBase.h"
#include "GridClassMembers.h"
#include "EducationClass.h"

class FrameClassMembers : public GridFrameBase
{
public:
	DECLARE_DYNAMIC(FrameClassMembers)
	FrameClassMembers(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
	virtual ~FrameClassMembers() = default;

	void BuildView(const vector<EducationSchool>& p_Schools, const vector<O365UpdateOperation>& p_Updates, bool p_FullPurge);

	O365Grid& GetGrid() override;

	void RefreshSpecific(AutomationAction* p_CurrentAction) override;
	void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;
	bool HasApplyButton() override;

	void ApplySpecific(bool p_Selected);
	void ApplyAllSpecific() override;
	void ApplySelectedSpecific() override;

protected:
	void createGrid() override;
	bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;
	AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	vector<EducationSchool> GetInfoForRefreshAll();
	vector<EducationSchool> GetInfoForRefreshAfterUpdate();

	GridClassMembers m_Grid;
};

