#pragma once

#include "IRequester.h"
#include "IPageRequestLogger.h"

template <class T, class U>
class ValueListDeserializer;

class BusinessGroupSetting;
class GroupSettingDeserializer;
class PaginatedRequestResults;

class GroupSettingsRequester : public IRequester
{
public:
    GroupSettingsRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger);
    GroupSettingsRequester(PooledString p_GroupId, const std::shared_ptr<IPageRequestLogger>& p_Logger);

    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    const vector<BusinessGroupSetting>& GetData() const;
    const PaginatedRequestResults& GetResults() const;

private:
    std::shared_ptr<ValueListDeserializer<BusinessGroupSetting, GroupSettingDeserializer>> m_Deserializer;
    std::shared_ptr<PaginatedRequestResults> m_Result;
	std::shared_ptr<IPageRequestLogger> m_Logger;

    PooledString m_GroupId;
};

