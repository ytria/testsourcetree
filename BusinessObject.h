#pragma once

#include "fnv1_hash_bytes.h"
#include "HttpResultWithError.h"
#include "RoleDelegationUtil.h"
#include "RttrIncludes.h"
#include "YTimeDate.h"

class Sapio365Session;

// See below some specialization for BusinessUser.
class BusinessUser;

class BusinessObject
{
public:
	enum UPDATE_TYPE
	{
		UPDATE_TYPE_NONE					= 0x0000,
		UPDATE_TYPE_MODIFY					= 0x0001,
		UPDATE_TYPE_ADDMEMBERS				= 0x0002,
		UPDATE_TYPE_ADDOWNERS				= 0x0004,
		UPDATE_TYPE_ADDAUTHORACCEPTED		= 0x0008,
		UPDATE_TYPE_ADDAUTHORREJECTED		= 0x0010,
		UPDATE_TYPE_DELETEMEMBERS			= 0x0020,
		UPDATE_TYPE_DELETEOWNERS			= 0x0040,
		UPDATE_TYPE_DELETEACCEPTEDAUTHORS	= 0x0080,
		UPDATE_TYPE_DELETEREJECTEDAUTHORS	= 0x0100,
		UPDATE_TYPE_CREATE					= 0x0200,
		UPDATE_TYPE_DELETE					= 0x0300,
		UPDATE_TYPE_HARDDELETE				= 0x0400,
		UPDATE_TYPE_RESTORE					= 0x0800,
		UPDATE_TYPE_LCPRENEW				= 0x1000,
		UPDATE_TYPE_LCPADDGROUP				= 0x2000,
		UPDATE_TYPE_LCPREMOVEGROUP			= 0x4000,

		UPDATE_TYPE_DELETEAUTHORIZATION		= 0x8000, // Only convenient for GridGroupsCommon::removeItemsFromGroup, avoid handling both UPDATE_TYPE_DELETEACCEPTEDAUTHORS and UPDATE_TYPE_DELETEREJECTEDAUTHORS
	};
    
    BusinessObject();
    virtual ~BusinessObject() = default;

	const boost::YOpt<YTimeDate>& GetLastRequestTime() const;
	void SetLastRequestTime(const boost::YOpt<YTimeDate>& p_Time);

	const boost::YOpt<YTimeDate>& GetDataDate(const uint32_t p_DataBlockId) const;
	void SetDataDate(const uint32_t p_DataBlockId, const boost::YOpt<YTimeDate>& p_Date);

	// FIXME: Finish this!
	//const boost::YOpt<SapioError>& GetDataError(const uint32_t p_DataBlockId) const;
	//void SetDataError(const uint32_t p_DataBlockId, const boost::YOpt<SapioError>& p_Error);

	void				SetID(const PooledString& p_ID);
	const PooledString&	GetID() const;

	UPDATE_TYPE GetUpdateStatus() const;
	void SetUpdateStatus(UPDATE_TYPE p_UpdateStatus);

	template < class T >
	static map<PooledString, T*> ToIDMap(vector<T>& p_BusObjects);
	template < class T >
	static vector<PooledString> ToIDs(const vector<T>& p_BusObjects);

	TaskWrapper<vector<HttpResultWithError>> SendWriteSingleRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const;
	vector<TaskWrapper<HttpResultWithError>> SendWriteMultipleRequests(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const;

    void SetError(const boost::YOpt<SapioError>& p_Error);
    boost::YOpt<SapioError>& GetError();
    const boost::YOpt<SapioError>& GetError() const;

	virtual wstring GetValue(const wstring& p_PropName) const;
	virtual vector<PooledString> GetProperties(const wstring& p_PropName) const;

	virtual wstring GetObjectName() const;

    PooledString m_Id;

	// Custom flags to be used by any client of this class.
	enum Flag
	{
		CANCELED			= 0x1,

		MORE_LOADED			= CANCELED		<< 1,
		CREATED				= MORE_LOADED	<< 1,
		REMOVEDBYDELTASYNC	= CREATED		<< 1,
		SHOULDBEIGNORED		= REMOVEDBYDELTASYNC << 1,

		// Derived class flags should start at:
		CUSTOM_FLAG_BEGIN	= SHOULDBEIGNORED << 1
	};

	void		SetFlags(uint32_t p_Flags);
	uint32_t	GetFlags() const;
	bool		HasFlag(uint32_t p_Flag) const;

	virtual bool IsMoreLoaded() const; // Default impl checks for flag MORE_LOADED. But with new modular cache, condition may be more complicated.

	void	SetRBACDelegationID(const int64_t p_RoleDelegationID);
	int64_t GetRBACDelegationID() const;
	bool	HasPrivilege(const RoleDelegationUtil::RBAC_Privilege p_Privilege, std::shared_ptr<Sapio365Session> p_Session) const;
	bool	IsRBACAuthorized() const;

	static wstring ObjectGuidToImmutableID(const wstring& p_ObjectGuid);

protected:
	virtual TaskWrapper<HttpResultWithError> SendCreateRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual pplx::task<vector<HttpResultWithError>> SendEditRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual TaskWrapper<HttpResultWithError> SendDeleteRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual TaskWrapper<HttpResultWithError> SendRestoreRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual TaskWrapper<HttpResultWithError> SendHardDeleteRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual TaskWrapper<HttpResultWithError> SendLCPRenewRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual TaskWrapper<HttpResultWithError> SendLCPAddGroupRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual TaskWrapper<HttpResultWithError> SendLCPRemoveGroupRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;

	virtual vector<TaskWrapper<HttpResultWithError>> SendAddMembersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual vector<TaskWrapper<HttpResultWithError>> SendDeleteMembersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual vector<TaskWrapper<HttpResultWithError>> SendAddOwnersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual vector<TaskWrapper<HttpResultWithError>> SendDeleteOwnersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual vector<TaskWrapper<HttpResultWithError>> SendAddAcceptedRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual vector<TaskWrapper<HttpResultWithError>> SendAddRejectedRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
	virtual vector<TaskWrapper<HttpResultWithError>> SendDeleteAuthorizationRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;
    virtual vector<TaskWrapper<HttpResultWithError>> SendGroupSettingsModify(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const;

	struct BusinessObjectBoolMetadataFilters
	{
	public:
		struct Filter
		{
			Filter(MetadataKeys p_Key, bool p_Value)
				: m_Key(p_Key)
				, m_Value(p_Value)
			{}
			MetadataKeys m_Key;
			bool m_Value;
		};

		BusinessObjectBoolMetadataFilters(const vector<Filter>& p_Filters)
			: m_Filters(p_Filters)
		{}

		// Returns false if filtered out
		bool operator()(const rttr::property& prop) const
		{
			ASSERT(prop.is_valid());

			for (const auto& filter : m_Filters)
				if (filter.m_Value != (prop.get_metadata(filter.m_Key).is_valid() && prop.get_metadata(filter.m_Key).get_value<bool>()))
					return false;
			return true;
		}

	private:
		vector<Filter> m_Filters;
	};

	struct JsonObjectRequestWhenFilter
	{
	public:
		JsonObjectRequestWhenFilter()
			: m_Include(0), m_Exclude(0)
		{}

		JsonObjectRequestWhenFilter(int p_Include, int p_Exclude)
			: m_Include(p_Include), m_Exclude(p_Exclude)
		{}

		// Returns false if filtered out
		bool operator()(const rttr::property& prop) const
		{
			if (0 != m_Include || 0 != m_Exclude)
			{
				ASSERT(prop.is_valid());
				auto metadata = prop.get_metadata(PropertyMetaDataKey::RequestWhen);
				if (metadata.is_valid())
				{
					const auto val = metadata.get_value<int>();
					return (0 == m_Include || (m_Include & val) == m_Include)
						&& (0 == m_Exclude || (m_Exclude & val) == 0);
				}
			}
			return true;
		}

	private:
		int m_Include;
		int m_Exclude;
	};

    template <typename JsonType, typename BusinessType>
    vector<rttr::property> GetUpdatableProperties(JsonType& p_JsonObject, const BusinessObjectBoolMetadataFilters& p_BusinessObjectMetadataFilter, const JsonObjectRequestWhenFilter& p_JsonObjectRequestWhenFilter) const;

	// Kind of a hack, specialization for BusinessUser.
	template <>
	vector<rttr::property> GetUpdatableProperties<User, BusinessUser>(User& p_JsonObject, const BusinessObjectBoolMetadataFilters& p_BusinessObjectMetadataFilter, const JsonObjectRequestWhenFilter& p_JsonObjectRequestWhenFilter) const;


	UPDATE_TYPE m_UpdateStatus;

    boost::YOpt<SapioError> m_ErrorMessage;
	boost::YOpt<YTimeDate> m_LastRequestTime;
	std::map<uint32_t, boost::YOpt<YTimeDate>> m_DataBlockDates;
	// FIXME: Finish this!
	//std::map<uint32_t, boost::YOpt<SapioError>> m_DataBlockErrors;

	uint32_t m_Flags;

	enum DelegationIDCodes : int64_t
	{
		NO_RBAC = -1,		// means no active Role Delegation
		OUT_OF_SCOPE = 0	// means doesn't match active Role Delegation scope,
	};

	int64_t m_RBAC_DelegationID;// ID of delegation whose scope is valid for this object (see enum above)

private:
    RTTR_ENABLE()
    RTTR_REGISTRATION_FRIEND
};

#define DECLARE_BUSINESSOBJECT_EQUALTO_AND_HASH_SPECIALIZATION(_BusinessObjectType) \
template<> \
struct std::equal_to<_BusinessObjectType> \
{ \
	bool operator()(const _BusinessObjectType& _Left, const _BusinessObjectType& _Right) const \
	{ \
		return (_Left.GetID() == _Right.GetID()); \
	} \
}; \
 \
template<> \
struct std::hash<_BusinessObjectType> \
{ \
	size_t operator()(const _BusinessObjectType& _Keyval) const \
	{ \
		return (fnv1a_hash_bytes((const unsigned char*)_Keyval.GetID().c_str(), ((const wstring&)_Keyval.GetID()).size() * sizeof(wchar_t))); \
	} \
}; \

#include "BusinessObject.hpp"
