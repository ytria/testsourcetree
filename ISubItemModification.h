#pragma once

#include "Modification.h"

#include "HttpResultWithError.h"
#include "SubItemKey.h"

class O365Grid;
class GridBackendColumn;
class GridBackendRow;

class SubItemInfo
{
public:
    SubItemInfo()
    {
        m_Row = nullptr;
        m_Collapsed = false;
        m_Index = 0;
    }

    GridBackendRow* m_Row;
    bool m_Collapsed;
    size_t m_Index;
};

class ISubItemModification : public Modification
{
public:
    ISubItemModification(O365Grid& p_Grid, const SubItemKey& p_Key, GridBackendColumn* p_ColSubItemElder, GridBackendColumn* p_ColSubItemPrimaryKey, const wstring& p_ObjectName);
    virtual ~ISubItemModification();

    virtual bool IsCorrespondingRow(GridBackendRow* p_Row) const = 0;
    virtual bool ShouldCreateFormatter(GridBackendRow* p_Row, size_t p_Index) const;
    virtual bool Refresh(GridBackendRow* p_Row, const std::map<SubItemKey, HttpResultWithError>& p_Errors) = 0;
    bool RefreshIfCorrespondingRow(GridBackendRow* p_Row, const std::map<SubItemKey, HttpResultWithError>& p_Errors);

    const SubItemKey& GetKey() const;
    O365Grid& GetGrid() const;

    const boost::YOpt<SapioError>& GetError() const;
    void SetError(const boost::YOpt<SapioError>& p_Error);

    GridBackendColumn* GetSubItemElderCol() const;
    GridBackendColumn* GetSubItemPkCol() const;

	size_t GetMultiValueIndex(GridBackendRow* p_Row) const;
	virtual void ShowAppliedLocally(GridBackendRow* p_Row) const = 0;

    void Revert(GridBackendRow* p_Row = nullptr);
    virtual void RevertSpecific(const SubItemInfo& p_Info);
    bool IsCollapsedRow(GridBackendRow* p_Row) const;

protected:
    wstring			getMostSpecificSubItemPk() const;
	vector<wstring> getPk() const;

    void	removeIndexFromFormatter(GridBackendRow* p_Row, size_t p_Index, size_t p_FormatterId, GridBackendColumn* p_Column);

    GridBackendColumn* m_ColSubItemElder;
    GridBackendColumn* m_ColSubItemPrimaryKey;

    size_t m_FormatterId;

private:
    SubItemKey m_Key;
    O365Grid& m_Grid;
    boost::YOpt<SapioError> m_Error;
};
