#pragma once

#include "IRequester.h"
#include "Application.h"
#include "SingleRequestResult.h"

class ApplicationDeserializer;

class CreateApplicationRequester : public IRequester
{
public:
	CreateApplicationRequester(const wstring& p_DisplayName, const web::json::value& p_ResourceAccess);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const SingleRequestResult& GetResult() const;
	const Application& GetData() const;

private:
	std::shared_ptr<SingleRequestResult> m_Result;
	std::shared_ptr<ApplicationDeserializer> m_Deserializer;

	wstring m_DisplayName;
	web::json::value m_ResourceAccess;
};

