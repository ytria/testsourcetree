#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class PasswordProfileDeserializer : public JsonObjectDeserializer, public Encapsulate<PasswordProfile>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

