#include "MainFrameSapio365Session.h"

#include "MainFrame.h"

MainFrameSapio365Session::operator shared_ptr<Sapio365Session>()
{
	std::shared_ptr<Sapio365Session> session;

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		session = mainFrame->GetSapio365Session();// MainFrame connected user: only the MainFrame backstage should offer this functionality, otherwise gib' me a Sapio365Session

	return session;
}

MainFrameSapio365Session::operator shared_ptr<const Sapio365Session>()
{
	std::shared_ptr<const Sapio365Session> session;

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		session = mainFrame->GetSapio365Session();// MainFrame connected user: only the MainFrame backstage should offer this functionality, otherwise gib' me a Sapio365Session

	return session;
}
