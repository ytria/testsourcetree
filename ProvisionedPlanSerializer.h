#pragma once

#include "IJsonSerializer.h"

class ProvisionedPlanSerializer : public IJsonSerializer
{
public:
    ProvisionedPlanSerializer(const ProvisionedPlan& p_ProvisionedPlan);
    void Serialize() override;

private:
    const ProvisionedPlan& m_ProvisionedPlan;
};

