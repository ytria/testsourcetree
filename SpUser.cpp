#include "SpUser.h"

RTTR_REGISTRATION
{
    using namespace rttr;
registration::class_<Sp::User>("SpUser") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("SharePoint User"))))
.constructor()(policy::ctor::as_object);
}
