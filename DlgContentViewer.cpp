#include "DlgContentViewer.h"

#include "CodeJockFrameBase.h"
#include "FrameMessages.h"
#include "MFCUtil.h"
#include "Controls/Button/Themes/XTPButtonThemeUltraFlat.h"
#include "Controls/Button/Themes/XTPButtonThemeOffice2013.h"
#include "BaseO365Grid.h"
#include "Command.h"

TCHAR DlgContentViewer::s_savedPositionKey[] = _YTEXT("DlgContentViewer-Saved-Position");

BEGIN_MESSAGE_MAP(DlgContentViewer, ResizableDialog)
	ON_BN_CLICKED(IDC_CONTENTVIEWER_BTN_PREVIOUS, OnPrevious)
	ON_BN_CLICKED(IDC_CONTENTVIEWER_BTN_NEXT, OnNext)
    ON_BN_CLICKED(IDC_CONTENTVIEWER_BTN_LOAD_IMAGES, OnLoadImages)
	ON_BN_CLICKED(IDC_CONTENTVIEWER_BTN_VIEW_SOURCE, OnViewSource)
END_MESSAGE_MAP()

DlgContentViewer::DlgContentViewer(O365Grid* p_Grid, NavigationCallback* p_NavigationCallback, AttachmentIDGetter* p_AttachmentIDGetter, const wstring& p_Title, CWnd* p_Parent, bool p_AddBoilerplateHtml)
	: ResizableDialog(IDD, p_Parent)
	, m_NavigationCallback(p_NavigationCallback)
	, m_AttachmentIDGetter(p_AttachmentIDGetter)
	, m_LoadAsHtml(false)
	, m_ShowLoadImagesButton(false)
	, m_Title(p_Title)
    , m_Grid(p_Grid)
	, m_AddBoilerplateHtml(p_AddBoilerplateHtml)
{
}

void DlgContentViewer::SetLoadImagesCommandTarget(Command::ModuleTarget p_Target)
{
    m_ShowImagesCmdTarget = p_Target;
}

void DlgContentViewer::SetLoadImagesCommandTask(Command::ModuleTask p_Task)
{
    m_ShowImagesCmdTask = p_Task;
}

void DlgContentViewer::SetContent(const HeaderConfig& p_HeaderConfig, const wstring& p_Content, bool p_LoadAsHtml, bool p_ShowLoadImagesButton)
{
	m_HeaderConfig = p_HeaderConfig;
	m_LoadAsHtml = p_LoadAsHtml;
	m_Content = p_Content;
	m_ShowLoadImagesButton = p_ShowLoadImagesButton;

	if (m_HtmlView)
	{
		updateHeader();

		if (m_LoadAsHtml)
		{
			m_TextView.ShowWindow(SW_HIDE);

			// For View Source: Insert before adding <script> tag below.
			m_TextView.SetWindowText(CString(m_Content.c_str()));

			if (m_ShowLoadImagesButton)
			{
				// Ensure there's at least one <script> tag so that we can execute script on this page (for image loading)
				if (wstring::npos == Str::find(m_Content, _YTEXT("<script"), 0, true))
				{
					auto pos = Str::find(m_Content, _YTEXT("</body>"), 0, true);
					if (wstring::npos == pos)
						pos = Str::find(m_Content, _YTEXT("</html>"), 0, true);
					if (wstring::npos != pos)
						m_Content.insert(pos, _YTEXT("<script></script>"));
				}
			}

			m_ViewSourceButton.ShowWindow(SW_SHOW);
			m_ViewSourceButton.EnableWindow(TRUE);
			m_ViewSourceButton.SetCheck(BST_UNCHECKED);

			m_LoadImagesButton.ShowWindow(p_ShowLoadImagesButton ? SW_SHOW : SW_HIDE);
			m_LoadImagesButton.EnableWindow(p_ShowLoadImagesButton ? TRUE : FALSE);

			m_HtmlView->ShowWindow(SW_SHOW);
			if (m_AddBoilerplateHtml)
				m_HtmlView->SetHtmlContent(wstring(_YTEXT("<html><head><meta charset=\"utf-8\"></head><body>")) + m_Content + wstring(_YTEXT("</body></html>")));
			else
				m_HtmlView->SetHtmlContent(m_Content);
		}
		else
		{
			ASSERT(!p_ShowLoadImagesButton);
			m_ViewSourceButton.ShowWindow(SW_HIDE);
			m_ViewSourceButton.EnableWindow(FALSE);
			m_LoadImagesButton.ShowWindow(SW_HIDE);
			m_LoadImagesButton.EnableWindow(FALSE);
			m_HtmlView->ShowWindow(SW_HIDE);
			m_TextView.ShowWindow(SW_SHOW);
			m_TextView.SetWindowText(CString(m_Content.c_str()));
		}
	}
}

StringHtmlView* DlgContentViewer::GetHtmlView() const
{
    return m_HtmlView.get();
}

void DlgContentViewer::DoDataExchange(CDataExchange* pDX)
{
	ResizableDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_CONTENTVIEWER_TEXTVIEWER, m_TextView);
	DDX_Control(pDX, IDC_CONTENTVIEWER_HEADER, m_Header);
    DDX_Control(pDX, IDC_CONTENTVIEWER_BTN_LOAD_IMAGES, m_LoadImagesButton);
	DDX_Control(pDX, IDC_CONTENTVIEWER_BTN_VIEW_SOURCE, m_ViewSourceButton);
	if (nullptr != m_NavigationCallback)
	{
		DDX_Control(pDX, IDC_CONTENTVIEWER_BTN_PREVIOUS, m_PreviousButton);
		DDX_Control(pDX, IDC_CONTENTVIEWER_BTN_NEXT, m_NextButton);
	}
}

namespace
{
	class CXTPButtonThemeOffice2013Exposer : public CXTPButtonThemeOffice2013
	{
	public:
		// Access CXTPButtonThemeOffice2013 protected member.
		static void SetBorderColor(CXTPButtonThemeOffice2013* theme, COLORREF color)
		{
			ASSERT(nullptr != theme);
			if (nullptr != theme)
				theme->*&CXTPButtonThemeOffice2013Exposer::m_crBorder = color;
		}
	};
}

BOOL DlgContentViewer::OnInitDialogSpecificResizable()
{
	SetBkColor(RGB(255, 255, 255));

	SetWindowText(CString(m_Title.c_str()));

	CRect htmlRect;
	m_TextView.GetWindowRect(&htmlRect);
	ScreenToClient(&htmlRect);

	m_HtmlView = make_unique<StringHtmlView>();
	ASSERT(m_HtmlView);
	m_HtmlView->SetOpenLinksExternally(true);
	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_HTMLVIEWER, NULL);

	AddAnchor(m_Header, CSize(0, 0), CSize(100, 0));
	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));
	AddAnchor(m_TextView, CSize(0, 0), CSize(100, 100));

	m_TextView.ShowScrollBar(SB_VERT);

	for (auto button : { &m_ViewSourceButton, &m_LoadImagesButton })
	{
		button->SetTheme(xtpControlThemeOffice2013);
		button->m_pTheme->m_crBorderHilite = RGB(255, 255, 255);
		button->m_pTheme->m_crBorderShadow = RGB(255, 255, 255);
		button->m_pTheme->m_crBorder3DHilite = RGB(255, 255, 255);
		button->m_pTheme->m_crBorder3DShadow = RGB(255, 255, 255);
		button->m_pTheme->m_crBack = RGB(255, 255, 255);
		CXTPButtonThemeOffice2013Exposer::SetBorderColor(dynamic_cast<CXTPButtonThemeOffice2013*>(button->m_pTheme), RGB(255, 255, 255));
	}

	AddAnchor(m_ViewSourceButton, CSize(0, 0), CSize(0, 0));
	AddAnchor(m_LoadImagesButton, CSize(100, 0), CSize(100, 0));

	m_ViewSourceButton.SetWindowText(YtriaTranslate::Do(DlgContentViewer_OnInitDialogSpecificResizable_2, _YLOC("View Source")).c_str());
	m_LoadImagesButton.SetWindowText(YtriaTranslate::Do(DlgContentViewer_OnInitDialogSpecificResizable_1, _YLOC("Load Images")).c_str());

	SetContent(m_HeaderConfig, m_Content, m_LoadAsHtml, m_ShowLoadImagesButton);

	if (nullptr != m_NavigationCallback)
	{
		AddAnchor(m_PreviousButton, CSize(0, 0), CSize(0, 0));
		AddAnchor(m_NextButton, CSize(100, 0), CSize(100, 0));
		m_NextButton.EnableMarkup(TRUE);
		m_NextButton.SetTheme(xtpControlThemeOffice2013);
		m_NextButton.m_pTheme->m_crBorderHilite = RGB(255, 255, 255);
		m_NextButton.m_pTheme->m_crBorderShadow = RGB(255, 255, 255);
		m_NextButton.m_pTheme->m_crBorder3DHilite = RGB(255, 255, 255);
		m_NextButton.m_pTheme->m_crBorder3DShadow = RGB(255, 255, 255);
		m_NextButton.m_pTheme->m_crBack = RGB(255, 255, 255);
		CXTPButtonThemeOffice2013Exposer::SetBorderColor(dynamic_cast<CXTPButtonThemeOffice2013*>(m_NextButton.m_pTheme), RGB(255, 255, 255));

		m_PreviousButton.EnableMarkup(TRUE);
		m_PreviousButton.SetTheme(xtpControlThemeOffice2013);
		m_PreviousButton.m_pTheme->m_crBorderHilite = RGB(255, 255, 255);
		m_PreviousButton.m_pTheme->m_crBorderShadow = RGB(255, 255, 255);
		m_PreviousButton.m_pTheme->m_crBorder3DHilite = RGB(255, 255, 255);
		m_PreviousButton.m_pTheme->m_crBorder3DShadow = RGB(255, 255, 255);
		m_PreviousButton.m_pTheme->m_crBack = RGB(255, 255, 255);
		CXTPButtonThemeOffice2013Exposer::SetBorderColor(dynamic_cast<CXTPButtonThemeOffice2013*>(m_PreviousButton.m_pTheme), RGB(255, 255, 255));
	}
	else
	{
		int left = -1, right = -1;
		auto prevItem = GetDlgItem(IDC_CONTENTVIEWER_BTN_PREVIOUS);
		if (nullptr != prevItem)
		{
			CRect rect;
			prevItem->GetWindowRect(&rect);
			ScreenToClient(&rect);
			prevItem->DestroyWindow();
			left = rect.left;
		}

		auto nextItem = GetDlgItem(IDC_CONTENTVIEWER_BTN_NEXT);
		if (nullptr != nextItem)
		{
			CRect rect;
			nextItem->GetWindowRect(&rect);
			ScreenToClient(&rect);
			nextItem->DestroyWindow();
			right = rect.right;
		}

		if (left >= 0 && right >= 0)
		{
			CRect rect;
			m_Header.GetWindowRect(&rect);
			ScreenToClient(&rect);
			m_Header.SetWindowPos(nullptr, left, rect.top, right - left, rect.Height(), SWP_NOZORDER);
		}
	}

	SetMinSize(CPoint(HIDPI_XW(200), HIDPI_YH(200)));
	// Enable dialog position saving/restoring in registry.
	restoreSavedSizePosition(s_savedPositionKey);
	return TRUE;
}

void DlgContentViewer::OnCancel()
{
	ResizableDialog::OnCancel();
	if (IsModeless())
		DestroyWindow();
}

void DlgContentViewer::OnOK()
{
	// Prevent closing on enter.
}

void DlgContentViewer::OnPrevious()
{
	ASSERT(nullptr != m_NavigationCallback);
	if (nullptr != m_NavigationCallback)
		m_NavigationCallback->Previous();
}

void DlgContentViewer::OnNext()
{
	ASSERT(nullptr != m_NavigationCallback);
	if (nullptr != m_NavigationCallback)
		m_NavigationCallback->Next();
}

void DlgContentViewer::OnLoadImages()
{
	if (nullptr != m_Grid && m_Grid->IsFrameConnected())
	{
		const auto& selectedRows = m_Grid->GetSelectedRows();
		auto& row = *selectedRows.begin();
		ASSERT(selectedRows.size() == 1 || row->IsExplosionFragment());

		ASSERT(nullptr != m_AttachmentIDGetter);
		auto ids = m_AttachmentIDGetter->GetIds(row);
		ASSERT(ids.IsValid());
		if (ids.IsValid())
		{
			GridFrameBase* frame = dynamic_cast<GridFrameBase*>(m_Grid->GetParentFrame());
			ASSERT(nullptr != frame);
			CommandInfo cmd;
			cmd.SetFrame(frame);
			if (nullptr != dynamic_cast<FrameMessages*>(frame))
				cmd.SetOrigin(Origin::Message);
			else
				cmd.SetOrigin(frame->GetOrigin());
			cmd.SetRBACPrivilege(frame->GetModuleCriteria().m_Privilege);

			AttachmentsInfo info;
			info.m_AttachmentInfos.emplace_back();
			info.m_AttachmentInfos.back().m_IDs = ids;
			cmd.Data() = std::make_pair(info, (DownloadInlineAttachmentsCallback*)this);
			CommandDispatcher::GetInstance().Execute(Command(m_Grid->GetLicenseContext(), nullptr, m_ShowImagesCmdTarget, m_ShowImagesCmdTask, cmd, { m_Grid, g_ActionNameSelectedPreviewItem, m_Grid->GetAutomationActionRecording(), m_Grid->GetAutomationActionFromFrame() }));
		}
	}
}

void DlgContentViewer::OnViewSource()
{
	ASSERT(m_LoadAsHtml);
	if (m_LoadAsHtml)
	{
		if (TRUE == m_TextView.IsWindowVisible())
		{
			m_TextView.ShowWindow(SW_HIDE);
			m_HtmlView->ShowWindow(SW_SHOW);
			m_ViewSourceButton.SetCheck(BST_UNCHECKED);
		}
		else
		{
			m_TextView.ShowWindow(SW_SHOW);
			m_HtmlView->ShowWindow(SW_HIDE);
			m_ViewSourceButton.SetCheck(BST_CHECKED);
		}
	}
}

COLORREF DlgContentViewer::MyTextEdit::OnQueryBackColor() const
{
	return RGB(255, 255, 255);
}

void DlgContentViewer::updateHeader()
{
	if (nullptr != m_NavigationCallback)
	{
		static const CString leftButtonMarkup = L"<Path Data='m 9.5 8.86464 -8.208513 10.03263 8.208513 9.68184' Stroke='#FF888888' StrokeThickness='2' />";
		static const CString rightButtonMarkup = L"<Path Data='m 0.5 28.86464 8.208513 -10.03263 -8.208513 -9.68184' Stroke='#FF888888' StrokeThickness='2' />";

		m_PreviousButton.SetWindowText(leftButtonMarkup);
		m_NextButton.SetWindowText(rightButtonMarkup);
	}

	static const CString markupFormatFirstLineItem =	L"<TextBlock Margin='0, 5, 10, 5' Foreground='#262626' FontWeight='Bold' FontFamily='Segoe UI' FontSize='12'>%s</TextBlock>"
														L"<TextBlock Margin='0, 5, 10, 5' Foreground='#6a6a6a' FontFamily='Segoe UI' FontSize='12'>%s</TextBlock>"
														;
	static const CString markupFormatSecondLineItem =	L"<TextBlock Margin='0, 0, 10, 0' Foreground='#262626' FontWeight='Bold' FontFamily='Segoe UI' FontSize='12'>%s</TextBlock>"
														L"<TextBlock Margin='0, 0, 10, 0' Foreground='#6a6a6a' FontFamily='Segoe UI' FontSize='12'>%s</TextBlock>"
														;

	static const CString markupFormatFirstLineItemSeparator = L"<TextBlock Margin='0, 5, 10, 5' Foreground='#262626' FontWeight='Bold' FontFamily='Segoe UI' FontSize='12'> | </TextBlock>";
	static const CString markupFormatSecondLineItemSeparator = L"<TextBlock Margin='0, 0, 10, 0' Foreground='#262626' FontWeight='Bold' FontFamily='Segoe UI' FontSize='12'> | </TextBlock>";


	static CString markupFormatBegin =
		L"<Border BorderBrush='#e1e1e1' BorderThickness='0, 0, 0, 1' Background='#ffffff'>"
		L"	<StackPanel>"
		//L"		<Border BorderBrush='#e1e1e1' BorderThickness='0, 0, 0, 1'>"
		L"			<StackPanel>"
		;

	static CString markupFormatEnd =
		L"			</StackPanel>"
		//L"		</Border>"
		L"		<StackPanel Margin='20, 0, 20, 0'>"
		L"			<TextBlock Margin='0, 5, 0, 5' Foreground='#262626' FontWeight='Bold' FontFamily='Segoe UI' FontSize='13'>%s</TextBlock>"			// Subject
		L"		</StackPanel>"
		L"	</StackPanel>"
		L"</Border>"
		;

	CString buffer;
	CString markupText;

	markupText += markupFormatBegin;
	markupText += L"<StackPanel Margin='20, 0, 20, 0' Orientation='Horizontal'>";
	for (size_t i = 0; i < m_HeaderConfig.m_FirstLine.size(); ++i)
	{
		if (i > 0)
			markupText += markupFormatFirstLineItemSeparator;
		if (!m_HeaderConfig.m_FirstLine[i].first.empty() && m_HeaderConfig.m_FirstLine[i].first.back() != _YTEXT(' '))
			m_HeaderConfig.m_FirstLine[i].first += _YTEXT(' ');
		buffer.Format(markupFormatFirstLineItem, m_HeaderConfig.m_FirstLine[i].first.c_str(), m_HeaderConfig.m_FirstLine[i].second.c_str());
		markupText += buffer;
	}
	markupText += L"</StackPanel>";
	markupText += L"<StackPanel Margin='20, 0, 20, 5' Orientation='Horizontal'>";
	for (size_t i = 0; i < m_HeaderConfig.m_SecondLine.size(); ++i)
	{
		if (i > 0)
			markupText += markupFormatSecondLineItemSeparator;
		if (!m_HeaderConfig.m_SecondLine[i].first.empty() && m_HeaderConfig.m_SecondLine[i].first.back() != _YTEXT(' '))
			m_HeaderConfig.m_SecondLine[i].first += _YTEXT(' ');
		buffer.Format(markupFormatSecondLineItem, m_HeaderConfig.m_SecondLine[i].first.c_str(), m_HeaderConfig.m_SecondLine[i].second.c_str());
		markupText += buffer;
	}
	markupText += L"</StackPanel>";

	buffer.Format(markupFormatEnd, m_HeaderConfig.m_Subject.c_str());
	markupText += buffer;

	m_Header.SetMarkupText(markupText);
}

void DlgContentViewer::processInlineAttachments(const std::map<AttachmentsInfo::IDs, std::map<wstring, wstring>>& p_IdsToCidsToPaths)
{
	ASSERT(p_IdsToCidsToPaths.size() == 1);

	ASSERT(m_HtmlView);
	if (m_HtmlView)
	{
		auto& cidsToPaths = p_IdsToCidsToPaths.begin()->second;
		web::json::value filePathsArray = web::json::value::array();
		web::json::value cidsArray = web::json::value::array();
		{
			auto& paths = filePathsArray.as_array();
			auto& cids = cidsArray.as_array();
			size_t i = 0;
			for (const auto& item : cidsToPaths)
			{
				cids[i] = json::value::string(item.first);
				paths[i] = json::value::string(item.second);
				++i;
			}
		}

		wstring script = wstring(_YTEXT(R"delim(
								var imgPaths = )delim"))
			+ filePathsArray.serialize() + _YTEXT(";")
			+ _YTEXT(R"delim(
								var imgCids = )delim")
			+ cidsArray.serialize() + _YTEXT(";");

		script += _YTEXT(R"delim(
								if (!String.prototype.startsWith) {
									String.prototype.startsWith = function(searchString, position) {
									    position = position || 0;
									    return this.indexOf(searchString, position) === position;
									};
								}
									
								function trimPrefix(str, prefix) {
									if (str.startsWith(prefix))
									    return str.slice(prefix.length);
									else
									    return str;
								};
									
								var images = document.getElementsByTagName("img");
								for (var i = 0; i < images.length; i++)
								{
									if (images[i].src.startsWith("cid:")) {
									    var cidName = trimPrefix(images[i].src, "cid:");
									    var inFilenames = false, index = -1;
									    for (var j = 0; j < imgCids.length && !inFilenames; ++j)
										{
											var id = imgCids[j];
											if (cidName === id)
											{
												inFilenames = true;
												index = j;
											}
									    }
									    if (inFilenames)
									        images[i].src = imgPaths[index];
									}
								}
							)delim");
		m_HtmlView->ExecuteScript(script);
	}
}