#pragma once

#include "IJsonSerializer.h"
#include "TeamFunSettings.h"

class TeamFunSettingsSerializer : public IJsonSerializer
{
public:
    TeamFunSettingsSerializer(const TeamFunSettings& p_Settings);
    void Serialize() override;

private:
    const TeamFunSettings& m_Settings;
};

