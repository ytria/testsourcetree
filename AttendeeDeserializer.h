#pragma once
#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Attendee.h"

class AttendeeDeserializer : public JsonObjectDeserializer, public Encapsulate<Attendee>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

