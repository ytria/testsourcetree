#include "SpGroupsRequester.h"

#include "Sapio365Session.h"
#include "SharepointOnlineSession.h"
#include "SingleRequestResult.h"
#include "BasicHttpRequestLogger.h"

Sp::SpGroupsRequester::SpGroupsRequester(PooledString p_SiteUrl)
    : m_SiteUrl(p_SiteUrl)
{
}

TaskWrapper<void> Sp::SpGroupsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<Sp::Group, Sp::GroupDeserializer>>();
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("sites"));
    uri.append_path(m_SiteUrl);
    uri.append_path(_YTEXT("_api"));
    uri.append_path(_YTEXT("web"));
    uri.append_path(_YTEXT("siteGroups"));

    LoggerService::User(YtriaTranslate::Do(Sp__SpGroupsRequester_Send_1, _YLOC("Requesting groups for site \"%1\""), m_SiteUrl.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetSharepointSession()->Get(uri.to_uri(), httpLogger, m_Deserializer, p_TaskData);
}

const vector<Sp::Group>& Sp::SpGroupsRequester::GetData() const
{
    return m_Deserializer->GetData();
}
