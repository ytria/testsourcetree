#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "Database.h"
#include "BaseObjDeserializer.h"

namespace Cosmos
{
    class DatabaseDeserializer : public JsonObjectDeserializer, public Encapsulate<Cosmos::Database>
    {
    public:
        void DeserializeObject(const web::json::object& p_Object) override;
    };
}
