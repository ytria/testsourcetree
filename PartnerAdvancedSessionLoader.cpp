#include "PartnerAdvancedSessionLoader.h"
#include "OAuth2BrowserSessionLoader.h"

PartnerAdvancedSessionLoader::PartnerAdvancedSessionLoader(const SessionIdentifier& p_Identifier)
	: ISessionLoader(p_Identifier)
{
}

TaskWrapper<std::shared_ptr<Sapio365Session>> PartnerAdvancedSessionLoader::Load(const std::shared_ptr<Sapio365Session>& p_SapioSession)
{
	OAuth2BrowserSessionLoader loader(GetSessionId(), GetPersistentSession(p_SapioSession));
	return loader.Load(p_SapioSession);
}
