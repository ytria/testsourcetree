#include "SessionMigrationUtil.h"
#include "MigrationUtil.h"
#include "FileUtil.h"

bool SessionMigrationUtil::CopyDatabase(const VersionSourceTarget& p_SourceTarget)
{
	auto previousFilePath = MigrationUtil::GetMigrationFilePath(p_SourceTarget.m_SourceVersion);
	if (FileUtil::FileExists(previousFilePath))
	{
		auto newFilePath = MigrationUtil::GetMigrationFilePath(p_SourceTarget.m_TargetVersion);
		if (!FileUtil::FileExists(newFilePath))
		{
			return 0 == FileUtil::ProcessFile({ previousFilePath }, { newFilePath }, FO_COPY, FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY);
		}
		else
		{
			ASSERT(false); // WTF are we migrating?
			throw std::exception("Target file already exists.");
			return false;
		}
	}
	else
	{
		ASSERT(false); // WTF are we migrating?
		throw std::exception("Source file doesn't exist.");
		return false;
	}

	return true;
}
