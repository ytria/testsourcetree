#pragma once

#include "IRequester.h"

#include "CosmosDbAccount.h"
#include "CosmosDbAccountDeserializer.h"

class SingleRequestResult;

template <class T, class U>
class ValueListDeserializer;

namespace Azure
{
	class DbAccountListRequester : public IRequester
	{
	public:
		DbAccountListRequester(const wstring& p_SubscriptionId, const wstring& p_ResGroup);
		TaskWrapper<void> Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

		const vector<Azure::CosmosDbAccount>& GetData() const;

	private:
		std::shared_ptr<ValueListDeserializer<Azure::CosmosDbAccount, Azure::CosmosDbAccountDeserializer>> m_Deserializer;
		shared_ptr<SingleRequestResult> m_Result;

		wstring m_SubscriptionId;
		wstring m_ResGroup;
	};
}
