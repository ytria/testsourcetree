#include "CachedUserPropertySet.h"
#include "MsGraphFieldNames.h"
#include "User.h"

vector<rttr::property> CachedUserPropertySet::GetPropertySet() const
{
    // FIXME: Don't use "User" class neither rttr::property
    const vector<rttr::property> properties = {
		rttr::type::get<User>().get_property(O365_ID),
		rttr::type::get<User>().get_property(O365_USER_DISPLAYNAME),
		rttr::type::get<User>().get_property(O365_USER_MAIL),
        rttr::type::get<User>().get_property(O365_USER_USERPRINCIPALNAME),
		rttr::type::get<User>().get_property(O365_USER_USERTYPE)
    };
	// If you add properties, make sure that they are in the same order or you will get the assert.
    ASSERT(properties == User().GetCacheProperties(User()));
    return properties;
}

vector<rttr::property> CachedDeletedUserPropertySet::GetPropertySet() const
{
	auto propSet = CachedUserPropertySet::GetPropertySet();

	// Same as CachedUserPropertySet::GetPropertySet(), just add O365_USER_DELETEDDATETIME
	propSet.push_back(rttr::type::get<User>().get_property(O365_USER_DELETEDDATETIME));

	return propSet;
}

