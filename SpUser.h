#pragma once

#include "BusinessObject.h"
#include "UserIdInfo.h"

namespace Sp
{
class User : public BusinessObject
{
public:
    boost::YOpt<PooledString> Email;
    boost::YOpt<int32_t> Id;
    boost::YOpt<bool> IsHiddenInUI;
    boost::YOpt<bool> IsSiteAdmin;
    boost::YOpt<PooledString> LoginName;
    boost::YOpt<int32_t> PrincipalType;
    boost::YOpt<PooledString> Title;
    boost::YOpt<Sp::UserIdInfo> UserId;

private:
    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND
};
}

