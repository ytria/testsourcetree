#pragma once

#include "IModificationApplyer.h"
#include "FieldUpdate.h"
#include "RefreshSpecificData.h"

class GridMailboxPermissions;

class MailboxPermissionsModificationsApplyer : public IModificationApplyer
{
public:
	MailboxPermissionsModificationsApplyer(GridMailboxPermissions& p_Grid);

	void ApplyAll() override;
	void ApplySelected() override;

	void SetConfirm(bool p_Confirm);
	void SetRefreshAfterApply(bool p_Refresh);

private:
	bool IsSelected(const row_pk_t& p_Pk);
	RefreshSpecificData GetRefreshData(bool p_SelectedOnly) const;

	GridMailboxPermissions& m_Grid;
	
	bool m_Confirm;
	bool m_RefreshAfterApply;
};

