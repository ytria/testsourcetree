#include "RoleDefinitionBindingsRequester.h"
#include "LoggerService.h"
#include "Sapio365Session.h"
#include "SharepointOnlineSession.h"
#include "SingleRequestResult.h"
#include "RoleDefinition.h"
#include "ValueListDeserializer.h"
#include "BasicHttpRequestLogger.h"

Sp::RoleDefinitionBindingsRequester::RoleDefinitionBindingsRequester(PooledString p_SiteUrl, PooledString p_PrincipalId)
    :m_SiteUrl(p_SiteUrl),
    m_PrincipalId(p_PrincipalId)
{
}

TaskWrapper<void> Sp::RoleDefinitionBindingsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<Sp::RoleDefinition, Sp::RoleDefinitionDeserializer>>();
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("sites"));
    uri.append_path(m_SiteUrl);
    uri.append_path(_YTEXT("_api"));
    uri.append_path(_YTEXT("web"));
    uri.append_path(web::uri::encode_uri(wstring(_YTEXT("roleAssignments(")) + wstring(m_PrincipalId) + wstring(_YTEXT(")"))));
    uri.append_path(_YTEXT("roleDefinitionBindings"));

    LoggerService::User(YtriaTranslate::Do(Sp__RoleDefinitionBindingsRequester_Send_1, _YLOC("Requesting role definition bindings for user \"%1\" in site \"%2\""), m_PrincipalId.c_str(), m_SiteUrl.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetSharepointSession()->Get(uri.to_uri(), httpLogger, m_Result, m_Deserializer, p_TaskData);
}

const vector<Sp::RoleDefinition>& Sp::RoleDefinitionBindingsRequester::GetData() const
{
    return m_Deserializer->GetData();
}

const SingleRequestResult& Sp::RoleDefinitionBindingsRequester::GetResult() const
{
    return *m_Result;
}
