#include "GraphFolderDeserializer.h"

#include "JsonSerializeUtil.h"

void GraphFolderDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeInt64(_YTEXT("childCount"), m_Data.ChildCount, p_Object);
}
