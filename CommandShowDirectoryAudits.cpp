#include "CommandShowDirectoryAudits.h"

#include "FrameDirectoryAudits.h"
#include "GridDirectoryAudits.h"
#include "DirectoryAuditsRequester.h"
#include "ModuleBase.h"
#include "O365AdminUtil.h"
#include "BasicPageRequestLogger.h"

CommandShowDirectoryAudits::CommandShowDirectoryAudits(FrameDirectoryAudits& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, const ModuleOptions& p_Options, bool p_IsUpdate)
:IActionCommand(_YTEXT("")),
m_Frame(p_Frame),
m_AutoSetup(p_AutoSetup),
m_LicenseCtx(p_LicenseCtx),
m_AutoAction(p_AutoAction),
m_IsUpdate(p_IsUpdate),
m_Options(p_Options)
{
}

void CommandShowDirectoryAudits::ExecuteImpl() const
{
	GridDirectoryAudits* grid = dynamic_cast<GridDirectoryAudits*>(&m_Frame.GetGrid());

	auto taskData = Util::AddFrameTask(_T("Show audit Logs"), &m_Frame, m_AutoSetup, m_LicenseCtx, !m_IsUpdate);

	YSafeCreateTask([options = m_Options, isUpdate = m_IsUpdate, taskData, hwnd = m_Frame.GetSafeHwnd(), moduleCriteria = m_Frame.GetModuleCriteria(), autoAction = m_AutoAction, grid, bom = m_Frame.GetBusinessObjectManager()]() {
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("audit logs"));
		auto requester = std::make_shared<DirectoryAuditsRequester>(logger);

		requester->SetCutOffDateTime(options.m_CutOffDateTime);
		requester->SetCustomFilter(options.m_CustomFilter);

		vector<DirectoryAudit> dirAudits = safeTaskCall(requester->Send(bom->GetSapio365Session(), taskData).Then([requester]() {
			return requester->GetData();
		}), bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ListErrorHandler<DirectoryAudit>, taskData).get();

		YCallbackMessage::DoPost([=]() {
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameDirectoryAudits*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ModuleBase::ShouldBuildView(hwnd, taskData.IsCanceled(), taskData, false))
			{
				ASSERT(nullptr != frame);
				if (nullptr != frame)
				{
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s audit logs...", Str::getStringFromNumber(dirAudits.size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());

					{
						CWaitCursor _;
						frame->BuildView(dirAudits, true, false);
					}

					if (!isUpdate)
					{
						frame->UpdateContext(taskData, hwnd);
						shouldFinishTask = false;
					}
				}
			}

			if (shouldFinishTask)
				frame->TaskFinished(taskData, nullptr, hwnd);
		});
	});
}
