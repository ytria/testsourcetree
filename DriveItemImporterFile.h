#pragma once

#include "DriveItemImporter.h"

class DriveItemImporterFile : public DriveItemImporter
{
public:
	using DriveItemImporter::DriveItemImporter;

	virtual void Run(const wstring& p_FilePath, const std::set<GridBackendRow*>& p_ParentRows, std::shared_ptr<DlgLoading>& p_Dlg = std::shared_ptr<DlgLoading>()) override;

private:
	wstring generateUniqueFileName(const wstring& desiredFileName, GridBackendRow* p_ParentRow);
	//void updateParentCounters(const uintmax_t p_FileSize, GridBackendRow* p_FileRow, bool p_IsNewRow);

	friend class DriveItemImporterFolder;
};