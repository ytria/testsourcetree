#include "BasicHttpRequestLogger.h"
#include "LoggerService.h"
#include "SessionIdentifier.h"
#include "SessionIdsEmitter.h"

BasicHttpRequestLogger::BasicHttpRequestLogger(const SessionIdentifier& p_Identifier)
{
	m_SessionId = SessionIdsEmitter(p_Identifier).GetId();
}

void BasicHttpRequestLogger::LogRequest(const wstring& p_Uri, const wstring& p_Method, const WebPayload& p_Payload, HWND p_Originator)
{
	wostringstream oss;
	oss << _YTEXT("[[") << m_SessionId << _YTEXT("]] - ");
	oss << p_Method << _YTEXT(" ") << p_Uri;
	if (!p_Payload.IsEmpty())
	{
		oss << _YTEXT(" Content-Type: ") << p_Payload.GetContentType();
		oss << std::endl << p_Payload.GetDump();
	}
	LoggerService::Debug(oss.str(), p_Originator);
}

void BasicHttpRequestLogger::LogResponse(const wstring& p_Uri, web::http::status_code p_StatusCode, const wstring& p_Method, HWND p_Originator)
{
	LoggerService::Debug(_YTEXTFORMAT(L"[[%s]] - HTTP %s for %s %s", m_SessionId.c_str(), std::to_wstring(p_StatusCode).c_str(), p_Method.c_str(), p_Uri.c_str()), p_Originator);
}

shared_ptr<IHttpRequestLogger> BasicHttpRequestLogger::Clone() const
{
	return std::make_unique<BasicHttpRequestLogger>(*this);
}
