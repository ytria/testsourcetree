#pragma once

#include "IJsonDeserializer.h"
#include "Encapsulate.h"

class DumpDeserializer : public IJsonDeserializer, public Encapsulate<wstring>
{
public:
    void Deserialize(const web::json::value& p_Json) override;
};

