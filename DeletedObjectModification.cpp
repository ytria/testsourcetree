#include "DeletedObjectModification.h"

#include "ActivityLoggerUtil.h"
#include "BaseO365Grid.h"

DeletedObjectModification::DeletedObjectModification(O365Grid& p_Grid, const row_pk_t& p_RowKey)
    : WithSiblings<ObjectActionModification, DeletedObjectModification>(p_Grid, p_RowKey, true, _YTEXT(""))
{
}

void DeletedObjectModification::RefreshState()
{
	GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

	if (nullptr == row)
	{
		m_State = IMainItemModification::State::RemoteHasNewValue;
	}
	else if (row->IsError())
	{
		m_State = IMainItemModification::State::RemoteError;
	}
	else
	{
		m_State = IMainItemModification::State::RemoteHasOldValue;
		getGrid().OnRowSaved(row, false, YtriaTranslate::Do(DeletedObjectModification_RefreshState_1, _YLOC("Deletion has not been applied yet.")).c_str());
	}
}

vector<Modification::ModificationLog> DeletedObjectModification::GetModificationLogs() const
{
	return{ ModificationLog(GridBackendUtil::ToString(GetRowKey()), GetObjectName(), ActivityLoggerUtil::g_ActionDelete, getGrid().GetAutomationName(), GetState()) };
}

// =================================================================================================

DeletedFolderModification::DeletedFolderModification(O365Grid& p_Grid, const row_pk_t& p_RowKey)
	: DeletedObjectModification(p_Grid, p_RowKey)
{
	GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());
	recursiveSetParentDeletedState(row, true);
}

DeletedFolderModification::~DeletedFolderModification()
{
	if (shouldRevert())
	{
		// Here we presume that base class will indeed do the revert...

		GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());
		recursiveSetParentDeletedState(row, false);
	}
}

void DeletedFolderModification::recursiveSetParentDeletedState(GridBackendRow* p_ParentRow, bool deleted)
{
	ASSERT(nullptr != p_ParentRow || GetState() == Modification::State::RemoteHasNewValue);
	if (nullptr != p_ParentRow)
	{
		vector<GridBackendRow*> childRows;
		getGrid().GetBackend().GetHierarchyChildren(p_ParentRow, childRows, GridBackend::GHCflags::ONLY_DIRECT);
		for (auto childRow : childRows)
		{
			ASSERT(deleted && !childRow->HasFlag(GridBackendUtil::ROWFLAGS::PARENTDELETED)
				|| !deleted && childRow->HasFlag(GridBackendUtil::ROWFLAGS::PARENTDELETED));
			getGrid().OnRowNothingToSave(childRow, false);
			if (deleted)
			{
				childRow->SetFlag(GridBackendUtil::ROWFLAGS::PARENTDELETED);
				getGrid().GetBackend().OnRowChange(childRow, YtriaTranslate::Do(DeletedFolderModification_recursiveSetParentDeletedState_1, _YLOC("Parent Folder is Deleted")).c_str(), GridBackendUtil::ICON_DELETED);
				if (p_ParentRow->IsSelected())
					childRow->SetSelected(true);
			}

			if (childRow->HasChildrenRows())
				recursiveSetParentDeletedState(childRow, deleted);
		}
	}
}