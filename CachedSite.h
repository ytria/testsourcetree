#pragma once

#include "CachedObject.h"
#include "PooledString.h"
#include "RttrIncludes.h"

#include "Site.h"
#include "BusinessSite.h"

class CachedSite : public CachedObject
{
public:
	CachedSite() = default;
	CachedSite(const Site& site);
    CachedSite(const BusinessSite& site);

    const PooledString& GetHierarchyDisplayName() const;
    void				SetHierarchyDisplayName(const PooledString& p_Val);

    const boost::YOpt<PooledString>& GetWebUrl() const;

	PooledString GetDisplayName() const;
	const boost::YOpt<PooledString>&	GetName() const;

private:
    boost::YOpt<PooledString>	m_DisplayName;
    boost::YOpt<PooledString>	m_Name;
    boost::YOpt<PooledString>	m_WebUrl;
    PooledString				m_HierarchyDisplayName;
    // TODO: More fields   

    RTTR_ENABLE(CachedObject)
	RTTR_REGISTRATION_FRIEND
};

