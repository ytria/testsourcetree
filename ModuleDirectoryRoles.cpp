#include "ModuleDirectoryRoles.h"

#include "BasicRequestLogger.h"
#include "BusinessObjectManager.h"
#include "CommandInfo.h"
#include "FrameDirectoryRoles.h"
#include "GridDirectoryRoles.h"
#include "GridSnapshot.h"
#include "MSGraphSession.h"
#include "O365AdminUtil.h"
#include "safeTaskCall.h"
#include "SapioError.h"
#include "YCallbackMessage.h"

void ModuleDirectoryRoles::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
    {
    case Command::ModuleTask::List:
        showDirectoryRoles(p_Command);
        break;

	case Command::ModuleTask::UpdateModified:
		updateDirectoryRoles(p_Command);
		break;

	case Command::ModuleTask::LoadSnapshot:
		loadSnapshot(p_Command);
		break;

	default:
		ASSERT(false);
    }
}

void ModuleDirectoryRoles::showDirectoryRoles(const Command& p_Command)
{
    AutomationAction* action = p_Command.GetAutomationAction();

    const auto& info = p_Command.GetCommandInfo();

    auto sourceWindow = p_Command.GetSourceWindow();

    FrameDirectoryRoles* frame = dynamic_cast<FrameDirectoryRoles*>(info.GetFrame());
	const bool isRefresh = nullptr != frame;
    if (nullptr == frame)
    {
        if (FrameDirectoryRoles::CanCreateNewFrame(true, sourceWindow.GetCWnd(), action))
        {
            ModuleCriteria criteria;
            criteria.m_Origin			= Origin::Tenant;
            criteria.m_UsedContainer	= ModuleCriteria::UsedContainer::IDS;
			criteria.m_Privilege		= info.GetRBACPrivilege();

            if (ShouldCreateFrame<FrameDirectoryRoles>(criteria, sourceWindow, action) && sourceWindow.UserConfirmation(action))
            {
				CWaitCursor _;

                frame = new FrameDirectoryRoles(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleDirectoryRoles_showDirectoryRoles_1, _YLOC("Directory Roles")).c_str(), *this, sourceWindow.GetHistoryMode(), sourceWindow.GetCFrameWndForHistory());
                frame->InitModuleCriteria(criteria);
                AddGridFrame(frame);
                frame->Erect();
            }
            else
            {
				ASSERT(nullptr == action);
                SetAutomationGreenLight(action, _YTEXT(""));
                return;
            }
        }
    }

	if (nullptr == frame)
	{
		// No error: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
		ASSERT(nullptr == action);
		SetAutomationGreenLight(action/*, _YTEXT("")*/);
		return;
	}

    auto taskData = addFrameTask(YtriaTranslate::Do(ModuleDirectoryRoles_showDirectoryRoles_2, _YLOC("Show Directory Roles")).c_str(), frame, p_Command, !isRefresh);

    RefreshSpecificData refreshSpecificData;
    if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
        refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

	YSafeCreateTask([taskData, action, currentModuleCriteriaIds = frame->GetModuleCriteria().m_IDs, hasUpdateOps = frame->HasLastUpdateOperations(), info, refreshSpecificData, hwnd = frame->GetSafeHwnd(), bom = frame->GetBusinessObjectManager(), isRefresh]()
	{
        DirectoryRolesInfo dirInfo;

        const bool partialRefresh = !refreshSpecificData.m_Criteria.m_IDs.empty();
        const auto& ids = partialRefresh ? refreshSpecificData.m_Criteria.m_IDs : currentModuleCriteriaIds;

		auto templates = bom->GetDirectoryRoleTemplates(taskData).GetTask().get();

		if (hasUpdateOps)
		{
			for (const auto& id : info.GetIds())
			{
				auto it = std::find_if(templates.begin(), templates.end(), [id](const BusinessDirectoryRoleTemplate& bdr) { return id == bdr.GetID(); });
				ASSERT(dirInfo.m_Templates.end() == std::find_if(dirInfo.m_Templates.begin(), dirInfo.m_Templates.end(), [id](const BusinessDirectoryRoleTemplate& bdr) {return id == bdr.GetID(); }));
				dirInfo.m_Templates.push_back(*it);
			}
		}
		else
		{
			dirInfo.m_Templates = templates;

			if (partialRefresh)
				dirInfo.m_Templates.erase(std::remove_if(dirInfo.m_Templates.begin(), dirInfo.m_Templates.end(), [&ids](const auto& p_Template) {return ids.end() == ids.find(p_Template.GetID()); }), dirInfo.m_Templates.end());
		}

		auto roles = bom->GetDirectoryRoles(taskData).GetTask().get();

		auto membersLogger = std::make_shared<BasicRequestLogger>(_T("directory role members"));
		for (auto& roleTemplate : dirInfo.m_Templates)
		{
			auto it = std::find_if(roles.begin(), roles.end(), [&roleTemplate](const BusinessDirectoryRole& bdr) { return bdr.GetRoleTemplateId() && *bdr.GetRoleTemplateId() == roleTemplate.GetID(); });
			if (roles.end() != it)
			{
				auto& role = *it;
				if (!partialRefresh ||
					role.GetRoleTemplateId() && ids.find(*role.GetRoleTemplateId()) != ids.end())
				{
					vector<BusinessUser> members;
					if (!taskData.IsCanceled())
					{
						membersLogger->SetContextualInfo(GetDisplayNameOrId(roleTemplate));
						members = bom->GetDirectoryRoleMembers(role.GetID(), membersLogger, taskData).GetTask().get();
					}

					if (taskData.IsCanceled())
					{
						role.SetFlags(BusinessObject::CANCELED);
						roleTemplate.SetFlags(BusinessObject::CANCELED);
					}
					else
					{
						role.SetLastRequestTime(taskData.GetLastRequestOn());
						roleTemplate.SetLastRequestTime(taskData.GetLastRequestOn());
						for (const auto& member : members)
							role.AddMember(member);
					}

					dirInfo.m_Roles.push_back(role);
				}
			}
			else
			{
				if (taskData.IsCanceled())
				{
					roleTemplate.SetFlags(BusinessObject::CANCELED);
				}
				else
				{
					roleTemplate.SetLastRequestTime(taskData.GetLastRequestOn());
				}
			}
		}
            
		YDataCallbackMessage<DirectoryRolesInfo>::DoPost(dirInfo, [hwnd, partialRefresh, refreshSpecificData, taskData, action, isRefresh, isCanceled = taskData.IsCanceled()](const DirectoryRolesInfo& p_DirRolesInfo)
        {
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameDirectoryRoles*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(p_DirRolesInfo.m_Roles)))
            {
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s directory roles...", Str::getStringFromNumber(p_DirRolesInfo.m_Templates.size()).c_str()));
                frame->GetGrid().ClearLog(taskData.GetId());
                //frame->GetGrid().ClearStatus();

				{
					CWaitCursor _;
					if (frame->HasLastUpdateOperations())
					{
						frame->BuildView(p_DirRolesInfo, frame->GetLastUpdateOperations(), refreshSpecificData, false);
						frame->ForgetLastUpdateOperations();
					}
					else
					{
						frame->BuildView(p_DirRolesInfo, {}, refreshSpecificData, !partialRefresh);
					}
				}

				if (!isRefresh)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}
            }

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, taskData, action);
			}
        });
    });
}

void ModuleDirectoryRoles::updateDirectoryRoles(const Command& p_Command)
{
	auto action = p_Command.GetAutomationAction();
	
    auto& info = p_Command.GetCommandInfo();
	auto* existingFrame = dynamic_cast<FrameDirectoryRoles*>(info.GetFrame());

	ASSERT(info.Data().is_type<GridDirectoryRoles::RoleChanges>());
	if (info.Data().is_type<GridDirectoryRoles::RoleChanges>())
	{
		auto& changes = info.Data().get_value<GridDirectoryRoles::RoleChanges>();
		ASSERT(!changes.empty());
		if (!changes.empty())
		{
			auto taskData = addFrameTask(YtriaTranslate::Do(ModuleDirectoryRoles_updateDirectoryRoles_1, _YLOC("Updating Directory Roles")).c_str(), existingFrame, p_Command, false);
			YSafeCreateTask([existingFrame, taskData, action, changes]()
			{
				vector<O365UpdateOperation> updateOperations;

				auto bom = existingFrame->GetBusinessObjectManager();
				for (auto& change : changes)
				{
					if (taskData.IsCanceled())
						break;

					// In case we activate the role below.
					boost::YOpt<PooledString> roleID = change.m_RoleID;

					if (change.m_UsersToRemove.is_initialized())
					{
						ASSERT(roleID.is_initialized());
						if (roleID.is_initialized())
						{
							for (auto& userToRemove : *change.m_UsersToRemove)
							{
								if (taskData.IsCanceled())
									break;

								const HttpResultWithError err = bom->DeleteDirectoryRoleMember(userToRemove.first, *roleID, taskData).GetTask().get();

								updateOperations.emplace_back(userToRemove.second);
								updateOperations.back().StoreResult(err);
							}
						}
					}

					if (taskData.IsCanceled())
						break;

					boost::YOpt<HttpResultWithError> activationResult;
					if (change.m_Activate.is_initialized())
					{
						if (*change.m_Activate)
						{
							activationResult = bom->CreateDirectoryRoleFromTemplate(change.m_TemplateID, taskData).GetTask().get();

							if (!activationResult->m_Error)
							{
								const auto jsonObject = activationResult->m_ResultInfo->Response.extract_json().get().as_object();
								if (jsonObject.end() != jsonObject.find(_YTEXT("roleTemplateId")))
								{
									ASSERT(!roleID.is_initialized());
									roleID = jsonObject.at(_YTEXT("id")).as_string();
								}
							}

							ASSERT(change.m_RowPrimaryKey.is_initialized());
							if (change.m_RowPrimaryKey.is_initialized())
							{
								updateOperations.emplace_back(*change.m_RowPrimaryKey);
								updateOperations.back().StoreResult(*activationResult);
							}
						}
						else
						{
							// FIXME: This doesn't work yet.
							ASSERT(roleID.is_initialized());
							if (roleID.is_initialized())
							{
								const HttpResultWithError err = bom->DeleteDirectoryRole(*roleID, taskData).GetTask().get();

								ASSERT(change.m_RowPrimaryKey.is_initialized());
								if (change.m_RowPrimaryKey.is_initialized())
								{
									updateOperations.emplace_back(*change.m_RowPrimaryKey);
									updateOperations.back().StoreResult(err);
								}
							}
						}
					}

					if (taskData.IsCanceled())
						break;

					if (change.m_UsersToAdd.is_initialized())
					{
						ASSERT(roleID.is_initialized() || activationResult && activationResult->m_Error);
						for (auto& userToAdd : *change.m_UsersToAdd)
						{
							if (taskData.IsCanceled())
								break;

							if (roleID.is_initialized())
							{
								const auto result = bom->AddDirectoryRoleMember(userToAdd.first, *roleID, taskData).GetTask().get();
								updateOperations.emplace_back(userToAdd.second);
								updateOperations.back().StoreResult(result);
							}
							else
							{
								ASSERT(activationResult && activationResult->m_Error);
								updateOperations.emplace_back(userToAdd.second);
								updateOperations.back().StoreResult(*activationResult);
							}
						}
					}
				}

				YCallbackMessage::DoPost([existingFrame, action, taskData, updateOperations{ std::move(updateOperations) }]()
				{
					if (::IsWindow(existingFrame->GetSafeHwnd()))
					{
						existingFrame->GetGrid().ClearLog(taskData.GetId());
						existingFrame->RefreshAfterUpdate(vector<O365UpdateOperation>{ updateOperations }, action);
					}
					else
						SetAutomationGreenLight(action);
					TaskDataManager::Get().RemoveTaskData(taskData.GetId(), existingFrame->GetSafeHwnd());
				});
			});
		}
		else
		{
			SetAutomationGreenLight(action, _YTEXT(""));
		}
	}
}

void ModuleDirectoryRoles::loadSnapshot(const Command& p_Command)
{
	LoadSnapshot<FrameDirectoryRoles>(p_Command, YtriaTranslate::Do(ModuleDirectoryRoles_showDirectoryRoles_1, _YLOC("Directory Roles")).c_str());
}
