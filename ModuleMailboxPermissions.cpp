#include "ModuleMailboxPermissions.h"
#include "FrameMailboxPermissions.h"
#include "RefreshSpecificData.h"
#include "YCallbackMessage.h"
#include "MailboxPermissionsRequester.h"
#include "MailboxPermissionsCollectionDeserializer.h"
#include "MultiObjectsRequestLogger.h"
#include "InvokeResultWrapper.h"
#include "ModuleUtil.h"

void ModuleMailboxPermissions::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
	case Command::ModuleTask::ListMe:
	{
		Command newCmd = p_Command;
		newCmd.GetCommandInfo().GetIds() = { PooledString(GetConnectedSession().GetConnectedUserId()) };
		showMailboxPermissions(newCmd);
		break;
	}
	case Command::ModuleTask::List:
		showMailboxPermissions(p_Command);
		break;
	case Command::ModuleTask::UpdateRefresh:
		refresh(p_Command);
		break;
	case Command::ModuleTask::LoadSnapshot:
		loadSnapshot(p_Command);
		break;
	default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
		break;
	}
}

void ModuleMailboxPermissions::showMailboxPermissions(const Command& p_Command)
{
	const auto& info = p_Command.GetCommandInfo();
	auto& p_IDs = info.GetIds();
	auto	p_Origin = info.GetOrigin();
	auto	p_SourceWindow = p_Command.GetSourceWindow();
	auto	p_Action = p_Command.GetAutomationAction();

	auto sourceCWnd = p_SourceWindow.GetCWnd();
	if (!FrameMailboxPermissions::CanCreateNewFrame(true, sourceCWnd, p_Action))
		return;

	if (!ModuleUtil::WarnIfPowerShellHostIncompatible(sourceCWnd))
		return;

	auto session = Sapio365Session::Find(p_Command.GetSessionIdentifier());
	ASSERT(session);
	if (session)
	{
		bool hasDll = session->HasPowerShellDll();
		if (!hasDll)
		{
			YCallbackMessage::DoPost([sourceCWnd = p_SourceWindow.GetCWnd()]() {
				YCodeJockMessageBox confirmation(
					sourceCWnd,
					DlgMessageBox::eIcon_Error,
					_T("Unable to find YtriaPowerShell dll"),
					_T("Reinstalling the product might fix this problem."),
					_YTEXT(""),
					{ { IDOK, _T("Ok") } });
				confirmation.DoModal();
				});
			return;
		}
	}
	
	ModuleCriteria moduleCriteria;
	moduleCriteria.m_Origin = p_Origin;
	moduleCriteria.m_UsedContainer = ModuleCriteria::UsedContainer::IDS;
	moduleCriteria.m_IDs = p_IDs;
	moduleCriteria.m_Privilege = info.GetRBACPrivilege();
	moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

	if (ShouldCreateFrame<FrameMailboxPermissions>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
	{
		CWaitCursor _;
		FrameMailboxPermissions* newFrame = new FrameMailboxPermissions(p_Command.GetTask() == Command::ModuleTask::List, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), _T("Mailbox Permissions"), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
		newFrame->InitModuleCriteria(moduleCriteria);
		AddGridFrame(newFrame);
		newFrame->Erect();
		auto taskData = addFrameTask(_T("Show Mailbox Permissions"), newFrame, p_Command, true);

		doRefresh(newFrame, moduleCriteria, p_Action, taskData, false, RefreshSpecificData());
	}
}

void ModuleMailboxPermissions::refresh(const Command& p_Command)
{
	auto p_Action = p_Command.GetAutomationAction();

	auto existingFrame = dynamic_cast<FrameMailboxPermissions*>(p_Command.GetCommandInfo().GetFrame());
	ASSERT(nullptr != existingFrame);
	if (nullptr != existingFrame)
	{
		auto taskData = addFrameTask(_T("Show Mailbox Permissions"), existingFrame, p_Command, false);

		RefreshSpecificData refreshSelectedData;
		if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
			refreshSelectedData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

		doRefresh(existingFrame, existingFrame->GetModuleCriteria(), p_Action, taskData, true, refreshSelectedData);
	}
	else
		SetAutomationGreenLight(p_Action, _YTEXT(""));
}

void ModuleMailboxPermissions::doRefresh(FrameMailboxPermissions* p_pFrame, const ModuleCriteria& p_ModuleCriteria, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool isUpdate, const RefreshSpecificData& p_RefreshData)
{
	YSafeCreateTask([this, p_TaskData,
		hwnd = p_pFrame->GetSafeHwnd(), p_Action, isUpdate, p_RefreshData, p_ModuleCriteria, bom = p_pFrame->GetBusinessObjectManager(), wantsAdditionalMetadata = (bool)p_pFrame->GetMetaDataColumnInfo()]()
	{
		ASSERT(!wantsAdditionalMetadata || p_ModuleCriteria.m_Origin == Origin::User);

		O365DataMap<BusinessUser, vector<MailboxPermissions>> mailboxPermissions;

		const bool partialRefresh = !p_RefreshData.m_Criteria.m_IDs.empty();
		const auto& ids = partialRefresh ? p_RefreshData.m_Criteria.m_IDs : p_ModuleCriteria.m_IDs;

		auto initResult = bom->GetSapio365Session()->InitExchangePowerShell(p_TaskData.GetOriginator());
		wstring initErrors = GetPSErrorString(initResult);

		auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("mailbox permissions"), ids.size());
		auto processUser = [&mailboxPermissions, &bom, &p_TaskData, &logger, initErrors, this](BusinessUser& user)
		{
			vector<MailboxPermissions> userMailboxPermissions;
			if (!p_TaskData.IsCanceled())
			{
				logger->SetLogMessage(_T("mailbox permissions"));
				auto deserializer = std::make_shared<MailboxPermissionsCollectionDeserializer>();

				if (initErrors.empty())
				{
					MailboxPermissionsRequester permsRequester(deserializer, *user.GetUserPrincipalName(), logger);
					permsRequester.Send(bom->GetSapio365Session(), p_TaskData).GetTask().wait();
					auto errStream = permsRequester.GetResult()->Get().m_ErrorStream;
					if (errStream->GetSize() > 0)
					{
						wstring error;
						for (int i = 0; i < errStream->GetSize(); ++i)
						{
							error += errStream->GetAsString(i);
							error += _YTEXT("\r\n");
						}

						MailboxPermissions errorObj;

						SapioError sapioErr(error, 0);
						errorObj.SetError(sapioErr);
						userMailboxPermissions = { errorObj };
					}
					else
						userMailboxPermissions = deserializer->GetData();
				}
				else
				{
					MailboxPermissions errorObj;

					SapioError sapioErr(initErrors, 0);
					errorObj.SetError(sapioErr);
					userMailboxPermissions = { errorObj };
				}
			}

			if (p_TaskData.IsCanceled())
			{
				user.SetFlags(BusinessObject::CANCELED);
				mailboxPermissions[user] = {};
			}
			else
			{
				mailboxPermissions[user] = std::move(userMailboxPermissions);
			}
		};

		Util::ProcessSubUserItems(processUser, ids, p_ModuleCriteria.m_Origin, bom->GetGraphCache(), wantsAdditionalMetadata, logger, p_TaskData);

		//if (bom->GetSapio365Session()->HasPowershellSession())
		//{
		//	auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("mailbox permissions"), ids.size());

		//	Util::ProcessSubUserItems(processUser, ids, p_ModuleCriteria.m_Origin, bom->GetGraphCache(), wantsAdditionalMetadata, logger, p_TaskData);
		//}
		//else
		//{
		//	Util::ProcessSubUserItems(processUser, ids, p_ModuleCriteria.m_Origin, bom->GetGraphCache(), wantsAdditionalMetadata, logger, p_TaskData);
		//}

		YCallbackMessage::DoPost([mailboxPermissions, hwnd, partialRefresh, p_TaskData, p_Action, isUpdate, isCanceled = p_TaskData.IsCanceled()]()
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameMailboxPermissions*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, p_TaskData, isCanceled && !everythingCanceled(mailboxPermissions)))
			{
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting mailbox permissions for %s users...", Str::getStringFromNumber(mailboxPermissions.size()).c_str()));
				frame->GetGrid().ClearLog(p_TaskData.GetId());

				{
					CWaitCursor _;
					frame->ShowMailboxPermissions(mailboxPermissions, !partialRefresh);
				}

				if (!isUpdate)
				{
					frame->UpdateContext(p_TaskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, p_TaskData, p_Action);
			}
		});
	});
}

void ModuleMailboxPermissions::loadSnapshot(const Command& p_Command)
{
	LoadSnapshot<FrameMailboxPermissions>(p_Command, [&p_Command](const GridSnapshot::Metadata& p_SnapshotMeta, const ModuleCriteria& p_ModuleCriteria, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
		{
			const bool isMyData = p_SnapshotMeta.m_Flags && GridSnapshot::Flags::MyData == (GridSnapshot::Flags::MyData & *p_SnapshotMeta.m_Flags);
			return new FrameMailboxPermissions(isMyData, p_LicenseContext, p_SessionIdentifier, _T("Mailbox Permissions"), p_Module, p_HistoryMode, p_PreviousFrame);
		});
}
