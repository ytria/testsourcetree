#pragma once

#include "BusinessObject.h"

class OnPremiseUser : public BusinessObject
{
public:
	OnPremiseUser();

	// Collection properties
	const boost::YOpt<vector<PooledString>>& GetAuthenticationPolicy() const;
	void SetAuthenticationPolicy(const boost::YOpt<vector<PooledString>>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetAuthenticationPolicySilo() const;
	void SetAuthenticationPolicySilo(const boost::YOpt<vector<PooledString>>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetCertificates() const;
	void SetCertificates(const boost::YOpt<vector<PooledString>>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetCompoundIdentitySupported() const;
	void SetCompoundIdentitySupported(const boost::YOpt<vector<PooledString>>& p_Val);
	const boost::YOpt<vector<YTimeDate>>& GetdSCorePropagationData() const;
	void SetdSCorePropagationData(const boost::YOpt<vector<YTimeDate>>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetKerberosEncryptionType() const;
	void SetKerberosEncryptionType(const boost::YOpt<vector<PooledString>>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetLogonHours() const;
	void SetLogonHours(const boost::YOpt<vector<PooledString>>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetMemberOf() const;
	void SetMemberOf(const boost::YOpt<vector<PooledString>>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetPrincipalsAllowedToDelegateToAccount() const;
	void SetPrincipalsAllowedToDelegateToAccount(const boost::YOpt<vector<PooledString>>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetServicePrincipalNames() const;
	void SetServicePrincipalNames(const boost::YOpt<vector<PooledString>>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetSIDHistory() const;
	void SetSIDHistory(const boost::YOpt<vector<PooledString>>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetUserCertificate() const;
	void SetUserCertificate(const boost::YOpt<vector<PooledString>>& p_Val);

	// Int64 date properties
	const boost::YOpt<YTimeDate>& GetAccountExpires() const;
	void SetAccountExpires(const boost::YOpt<YTimeDate>& p_Val);
	const boost::YOpt<YTimeDate>& GetBadPasswordTime() const;
	void SetBadPasswordTime(const boost::YOpt<YTimeDate>& p_Val);
	const boost::YOpt<YTimeDate>& GetLastLogoff() const;
	void SetLastLogoff(const boost::YOpt<YTimeDate>& p_Val);
	const boost::YOpt<YTimeDate>& GetLastLogon() const;
	void SetLastLogon(const boost::YOpt<YTimeDate>& p_Val);

	// DateTime properties
	const boost::YOpt<YTimeDate>& GetAccountLockoutTime() const;
	void SetAccountLockoutTime(const boost::YOpt<YTimeDate>& p_Val);
	const boost::YOpt<YTimeDate>& GetCreated() const;
	void SetCreated(const boost::YOpt<YTimeDate>& p_Val);
	const boost::YOpt<YTimeDate>& GetLastBadPasswordAttempt() const;
	void SetLastBadPasswordAttempt(const boost::YOpt<YTimeDate>& p_Val);
	const boost::YOpt<YTimeDate>& GetLastLogonDate() const;
	void SetLastLogonDate(const boost::YOpt<YTimeDate>& p_Val);
	const boost::YOpt<YTimeDate>& GetModified() const;
	void SetModified(const boost::YOpt<YTimeDate>& p_Val);
	const boost::YOpt<YTimeDate>& GetPasswordLastSet() const;
	void SetPasswordLastSet(const boost::YOpt<YTimeDate>& p_Val);
	const boost::YOpt<YTimeDate>& GetWhenChanged() const;
	void SetWhenChanged(const boost::YOpt<YTimeDate>& p_Val);
	const boost::YOpt<YTimeDate>& GetWhenCreated() const;
	void SetWhenCreated(const boost::YOpt<YTimeDate>& p_Val);

	// String properties
	const boost::YOpt<PooledString>& GetCanonicalName() const;
	void SetCanonicalName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetCity() const;
	void SetCity(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetCN() const;
	void SetCN(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetCompany() const;
	void SetCompany(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetCountry() const;
	void SetCountry(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetDepartment() const;
	void SetDepartment(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetDescription() const;
	void SetDescription(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetDistinguishedName() const;
	void SetDistinguishedName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetDivision() const;
	void SetDivision(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetEmailAddress() const;
	void SetEmailAddress(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetEmployeeID() const;
	void SetEmployeeID(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetEmployeeNumber() const;
	void SetEmployeeNumber(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetFax() const;
	void SetFax(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetGivenName() const;
	void SetGivenName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetHomeDirectory() const;
	void SetHomeDirectory(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetHomeDrive() const;
	void SetHomeDrive(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetHomePage() const;
	void SetHomePage(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetHomePhone() const;
	void SetHomePhone(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetInitials() const;
	void SetInitials(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetLastKnownParent() const;
	void SetLastKnownParent(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetLogonWorkstations() const;
	void SetLogonWorkstations(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetManager() const;
	void SetManager(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetMobilePhone() const;
	void SetMobilePhone(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetName() const;
	void SetName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetTSecurityDescriptor() const;
	void SetNTSecurityDescriptor(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetObjectGUID() const;
	void SetObjectGUID(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetObjectCategory() const;
	void SetObjectCategory(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetObjectClass() const;
	void SetObjectClass(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetObjectSid() const;
	void SetObjectSid(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetOffice() const;
	void SetOffice(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetOfficePhone() const;
	void SetOfficePhone(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetOrganization() const;
	void SetOrganization(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetOtherName() const;
	void SetOtherName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetPOBox() const;
	void SetPOBox(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetPostalCode() const;
	void SetPostalCode(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetPrimaryGroup() const;
	void SetPrimaryGroup(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetProfilePath() const;
	void SetProfilePath(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetSamAccountName() const;
	void SetSamAccountName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetSID() const;
	void SetSID(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetScriptPath() const;
	void SetScriptPath(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetState() const;
	void SetState(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetStreetAddress() const;
	void SetStreetAddress(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetSurname() const;
	void SetSurname(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetTitle() const;
	void SetTitle(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetUserPrincipalName() const;
	void SetUserPrincipalName(const boost::YOpt<PooledString>& val);

	// Int64 properties
	const boost::YOpt<int64_t>& GttUSNChanged() const;
	void SetUSNChanged(const boost::YOpt<int64_t>& val);
	const boost::YOpt<int64_t>& GetUSNCreated() const;
	void SetUSNCreated(const boost::YOpt<int64_t>& val);

	// Int32 Properties
	const boost::YOpt<int32_t>& GetAdminCount() const;
	void SetAdminCount(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetBadLogonCount() const;
	void SetBadLogonCount(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetBadPwdCount() const;
	void SetBadPwdCount(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetCodePage() const;
	void SetCodePage(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetCountryCode() const;
	void SetCountryCode(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetInstanceType() const;
	void SetInstanceType(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetLogonCount() const;
	void SetLogonCount(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetMSDSUserAccountControlComputed() const;
	void SetMSDSUserAccountControlComputed(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetPrimaryGroupId() const;
	void SetPrimaryGroupId(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetSAMAccountType() const;
	void SetSAMAccountType(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetSDRightsEffective() const;
	void SetSDRightsEffective(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetUserAccountControl() const;
	void SetUserAccountControl(const boost::YOpt<int32_t>& val);

	// Bool properties
	const boost::YOpt<bool>& GetAccountNotDelegated() const;
	void SetAccountNotDelegated(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetAllowReversiblePasswordEncryption() const;
	void SetAllowReversiblePasswordEncryption(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetCannotChangePassword() const;
	void SetCannotChangePassword(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetDeleted() const;
	void SetDeleted(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetDoesNotRequirePreAuth() const;
	void SetDoesNotRequirePreAuth(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetEnabled() const;
	void SetEnabled(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetHomedirRequired() const;
	void SetHomedirRequired(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetIsCriticalSystemObject() const;
	void SetIsCriticalSystemObject(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetIsDeleted() const;
	void SetIsDeleted(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetLockedOut() const;
	void SetLockedOut(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetMNSLogonAccount() const;
	void SetMNSLogonAccount(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetPasswordExpired() const;
	void SetPasswordExpired(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetPasswordNeverExpires() const;
	void SetPasswordNeverExpires(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetPasswordNotRequired() const;
	void SetPasswordNotRequired(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetProtectedFromAccidentalDeletion() const;
	void SetProtectedFromAccidentalDeletion(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetSmartcardLogonRequired() const;
	void SetSmartcardLogonRequired(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetTrustedForDelegation() const;
	void SetTrustedForDelegation(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetTrustedToAuthForDelegation() const;
	void SetTrustedToAuthForDelegation(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetUseDESKeyOnly() const;
	void SetUseDESKeyOnly(const boost::YOpt<bool>& val);
	const boost::YOpt<vector<uint8_t>>& GetMsDsConsistencyGuid() const;
	wstring GetMsDsConsistencyGuidAsString() const;
	void SetMsDsConsistencyGuid(const boost::YOpt<vector<uint8_t>>& val);
	const boost::YOpt<vector<PooledString>>& GetProxyAddresses() const;
	void SetProxyAddresses(const boost::YOpt<vector<PooledString>>& val);

	static wstring ToImmutableID(const OnPremiseUser& p_User, bool p_UseConsistencyGuid);

	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND

private:
	// Collection properties
	boost::YOpt<vector<PooledString>> m_AuthenticationPolicy;
	boost::YOpt<vector<PooledString>> m_AuthenticationPolicySilo;
	boost::YOpt<vector<PooledString>> m_Certificates;
	boost::YOpt<vector<PooledString>> m_CompoundIdentitySupported;
	boost::YOpt<vector<YTimeDate>> m_dSCorePropagationData;
	boost::YOpt<vector<PooledString>> m_KerberosEncryptionType;
	boost::YOpt<vector<PooledString>> m_LogonHours;
	boost::YOpt<vector<PooledString>> m_MemberOf;
	boost::YOpt<vector<PooledString>> m_PrincipalsAllowedToDelegateToAccount;
	boost::YOpt<vector<PooledString>> m_ProxyAddresses;
	boost::YOpt<vector<PooledString>> m_ServicePrincipalNames;
	boost::YOpt<vector<PooledString>> m_SIDHistory;
	boost::YOpt<vector<PooledString>> m_UserCertificate;

	boost::YOpt<PooledString> m_CanonicalName;
	boost::YOpt<PooledString> m_City;
	boost::YOpt<PooledString> m_CN;
	boost::YOpt<PooledString> m_Company;
	boost::YOpt<PooledString> m_Country;
	boost::YOpt<PooledString> m_Department;
	boost::YOpt<PooledString> m_Description;
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<PooledString> m_DistinguishedName;
	boost::YOpt<PooledString> m_Division;
	boost::YOpt<PooledString> m_EmailAddress;
	boost::YOpt<PooledString> m_EmployeeID;
	boost::YOpt<PooledString> m_EmployeeNumber;
	boost::YOpt<PooledString> m_Fax;
	boost::YOpt<PooledString> m_GivenName;
	boost::YOpt<PooledString> m_HomeDirectory;
	boost::YOpt<PooledString> m_HomeDrive;
	boost::YOpt<PooledString> m_HomePage;
	boost::YOpt<PooledString> m_HomePhone;
	boost::YOpt<PooledString> m_Initials;
	boost::YOpt<PooledString> m_LastKnownParent;
	boost::YOpt<PooledString> m_LogonWorkstations;
	boost::YOpt<PooledString> m_Manager;
	boost::YOpt<PooledString> m_MobilePhone;
	boost::YOpt<vector<uint8_t>> m_MsDsConsistencyGuid;
	boost::YOpt<PooledString> m_Name;
	boost::YOpt<PooledString> m_nTSecurityDescriptor;
	boost::YOpt<PooledString> m_ObjectGUID;
	boost::YOpt<PooledString> m_ObjectCategory;
	boost::YOpt<PooledString> m_ObjectClass;
	boost::YOpt<PooledString> m_ObjectSid;
	boost::YOpt<PooledString> m_Office;
	boost::YOpt<PooledString> m_OfficePhone;
	boost::YOpt<PooledString> m_Organization;
	boost::YOpt<PooledString> m_OtherName;
	boost::YOpt<PooledString> m_POBox;
	boost::YOpt<PooledString> m_PostalCode;
	boost::YOpt<PooledString> m_PrimaryGroup;
	boost::YOpt<PooledString> m_ProfilePath;
	boost::YOpt<PooledString> m_SamAccountName;
	boost::YOpt<PooledString> m_SID;
	boost::YOpt<PooledString> m_ScriptPath;
	boost::YOpt<PooledString> m_State;
	boost::YOpt<PooledString> m_StreetAddress;
	boost::YOpt<PooledString> m_Surname;
	boost::YOpt<PooledString> m_Title;
	boost::YOpt<PooledString> m_UserPrincipalName;

	boost::YOpt<bool> m_AccountNotDelegated;
	boost::YOpt<bool> m_AllowReversiblePasswordEncryption;
	boost::YOpt<bool> m_CannotChangePassword;
	boost::YOpt<bool> m_Deleted;
	boost::YOpt<bool> m_DoesNotRequirePreAuth;
	boost::YOpt<bool> m_Enabled;
	boost::YOpt<bool> m_HomedirRequired;
	boost::YOpt<bool> m_IsCriticalSystemObject;
	boost::YOpt<bool> m_IsDeleted;
	boost::YOpt<bool> m_LockedOut;
	boost::YOpt<bool> m_MNSLogonAccount;
	boost::YOpt<bool> m_PasswordExpired;
	boost::YOpt<bool> m_PasswordNeverExpires;
	boost::YOpt<bool> m_PasswordNotRequired;
	boost::YOpt<bool> m_ProtectedFromAccidentalDeletion;
	boost::YOpt<bool> m_SmartcardLogonRequired;
	boost::YOpt<bool> m_TrustedForDelegation;
	boost::YOpt<bool> m_TrustedToAuthForDelegation;
	boost::YOpt<bool> m_UseDESKeyOnly;

	boost::YOpt<int32_t> m_AdminCount;
	boost::YOpt<int32_t> m_BadLogonCount;
	boost::YOpt<int32_t> m_BadPwdCount;
	boost::YOpt<int32_t> m_CodePage;
	boost::YOpt<int32_t> m_CountryCode;
	boost::YOpt<int32_t> m_InstanceType;
	boost::YOpt<int32_t> m_LogonCount;
	boost::YOpt<int32_t> m_MSDSUserAccountControlComputed;
	boost::YOpt<int32_t> m_PrimaryGroupId;
	boost::YOpt<int32_t> m_SAMAccountType;
	boost::YOpt<int32_t> m_SDRightsEffective;
	boost::YOpt<int64_t> m_USNChanged;
	boost::YOpt<int64_t> m_USNCreated;
	boost::YOpt<int32_t> m_UserAccountControl;

	boost::YOpt<YTimeDate> m_AccountExpires;
	boost::YOpt<YTimeDate> m_BadPasswordTime;
	boost::YOpt<YTimeDate> m_LastLogoff;
	boost::YOpt<YTimeDate> m_LastLogon;
	boost::YOpt<YTimeDate> m_AccountLockoutTime;
	boost::YOpt<YTimeDate> m_Created;
	boost::YOpt<YTimeDate> m_LastBadPasswordAttempt;
	boost::YOpt<YTimeDate> m_LastLogonDate;
	boost::YOpt<YTimeDate> m_Modified;
	boost::YOpt<YTimeDate> m_PasswordLastSet;
	boost::YOpt<YTimeDate> m_WhenChanged;
	boost::YOpt<YTimeDate> m_WhenCreated;

	friend class OnPremiseUsersCollectionDeserializer;
};

// only for identification issues in grids
// Finally unused.
//class HybridUser : public BusinessObject
//{
//public:
//	using BusinessObject::BusinessObject;
//
//private:
//	RTTR_ENABLE(BusinessObject)
//};
