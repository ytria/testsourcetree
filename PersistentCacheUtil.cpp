#include "PersistentCacheUtil.h"

#include "CachedObjectsSqlEngine.h"
#include "FileUtil.h"
#include "LoggerService.h"
#include "SessionIdentifier.h"
#include "SQLiteEngine.h"
#include "Str.h"
#include "YCodeJockMessageBox.h"
#include "SessionTypes.h"

bool PersistentCacheUtil::TableEmpty(const CachedObjectsSqlEngine & p_Sql, const wstring & p_TableName)
{
	return p_Sql.GetRowCount(p_TableName) == 0;
}

bool PersistentCacheUtil::databaseExists(const SessionIdentifier& p_SessionId, const int p_Version)
{
	return FileUtil::FileExists2(SQLiteEngine::getLocalAppDataDBFileFolder() + getFilenameFromSession(p_SessionId, p_Version));
}

bool PersistentCacheUtil::DatabaseExists(const CachedObjectsSqlEngine& p_Sql, const SessionIdentifier& p_SessionId)
{
	return databaseExists(p_SessionId, CachedObjectsSqlEngine::g_Version);
}

wstring PersistentCacheUtil::getFilenameFromSession(const SessionIdentifier& p_SessionId, const int p_Version)
{
	wstring emailOrAppId = p_SessionId.m_EmailOrAppId;
	wstring prefix, suffix;

	if (p_SessionId.m_SessionType == SessionTypes::g_StandardSession)
	{
		prefix = _YTEXT("_cb");
	}
	else if (p_SessionId.m_SessionType == SessionTypes::g_AdvancedSession)
	{
		prefix = _YTEXT("_ca");
	}
	else if (p_SessionId.m_SessionType == SessionTypes::g_ElevatedSession)
	{
		prefix = _YTEXT("_cf");
	}
	else if (p_SessionId.m_SessionType == SessionTypes::g_UltraAdmin)
	{
		prefix = _YTEXT("_cu");
	}
	else if (p_SessionId.m_SessionType == SessionTypes::g_Role)
	{
		prefix = _YTEXT("_cr");
		suffix = std::to_wstring(p_SessionId.m_RoleID);
	}
	else if (p_SessionId.m_SessionType == SessionTypes::g_PartnerAdvancedSession)
	{
		prefix = _YTEXT("_pa");
	}

	return _YTEXTFORMAT(L"%s%s_%s%s.ytr", prefix.c_str(), std::to_wstring(p_Version).c_str(), emailOrAppId.c_str(), suffix.c_str());
}

int PersistentCacheUtil::deleteDatabase(const SessionIdentifier& p_SessionId, const int p_Version)
{
	const auto cacheFileName = getFilenameFromSession(p_SessionId, p_Version);
	const auto cacheFilePath = SQLiteEngine::getLocalAppDataDBFileFolder() + cacheFileName;
	if (FileUtil::FileExists2(cacheFilePath))
	{
		const auto error = FileUtil::DeleteFiles({ cacheFilePath }, false, false);
		if (0 == error)
			LoggerService::Debug(_YTEXTFORMAT(L"Deleted SQL cache file %s", cacheFileName.c_str()));
		else
			LoggerService::Debug(_YTEXTFORMAT(L"Failed to delete SQL cache file %s (error %s)", cacheFileName.c_str(), Str::getStringHexaFromDWORD(error).c_str()));
		return error;
	}

	return 0;
}

int PersistentCacheUtil::DeleteDatabase(const SessionIdentifier& p_SessionId)
{
	return deleteDatabase(p_SessionId, CachedObjectsSqlEngine::g_Version);
}

wstring PersistentCacheUtil::GetFilenameFromSessionAndMigrateIfNecessary(const SessionIdentifier& p_SessionId, const int p_CurrentVersion)
{
	wstring fileName;

	const auto folder = SQLiteEngine::getLocalAppDataDBFileFolder();

	fileName = getFilenameFromSession(p_SessionId, p_CurrentVersion);

	if (!FileUtil::FileExists(folder + fileName))
	{
		int versionToMigrateFrom = p_CurrentVersion;
		for (auto v = p_CurrentVersion; v >= 0; --v)
		{
			if (FileUtil::FileExists(folder + getFilenameFromSession(p_SessionId, v)))
			{
				versionToMigrateFrom = v;
				break;
			}
		}

		for (auto v = versionToMigrateFrom; v < p_CurrentVersion; ++v)
		{
			switch (v)
			{
			case 0:
			case 1:
				// v2 is a brand new cache, nothing to migrate
				//migrateDatabase<v, v + 1>(p_SessionId);
				break;
			default:
				ASSERT(false);
			}
		}
	}

	return fileName;
}

template<int vFrom, int vTo>
void PersistentCacheUtil::migrateDatabase(const SessionIdentifier& p_SessionId)
{
	// FIXME: Not used anymore since cache v2, might be needed if we have to make a v3.
	ASSERT(vFrom >= 2 && vTo > 2);

	const auto folder = SQLiteEngine::getLocalAppDataDBFileFolder();
	ASSERT(FileUtil::FileExists(folder + getFilenameFromSession(p_SessionId, vFrom)));

	const auto fromFilePath = folder + getFilenameFromSession(p_SessionId, vFrom);
	const auto toFilePath = folder + getFilenameFromSession(p_SessionId, vTo);
	const auto err = FileUtil::RenameFiles({ fromFilePath }, { toFilePath });

	if (0 != err)
	{
		TCHAR buff[256] = _YTEXT("\0");
		::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, // It's a system error
			nullptr,
			err,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Do it in the standard language
			buff,
			sizeof(buff) / sizeof(TCHAR) - 1,
			nullptr);

		LoggerService::Debug(_YDUMPFORMAT(L"Unable to move Cache DB file from %s to %s.", fromFilePath.c_str(), toFilePath.c_str()));
		LoggerService::Debug(buff);
			
		YCodeJockMessageBox dlg(nullptr
				, DlgMessageBox::eIcon_Error
				, _T("Error")
				, _YFORMATERROR(L"Unable to move Cache DB file from %s to %s.", fromFilePath.c_str(), toFilePath.c_str())
				, buff
				, { {IDOK, _T("OK")} }
			);
		dlg.DoModal();
	}
}
