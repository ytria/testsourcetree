#pragma once

class BusinessSubscribedSku;

struct CachedSubscribedSku
{
public:
    CachedSubscribedSku() = default;
    CachedSubscribedSku(const BusinessSubscribedSku& p_SubscribedSku);
    
    PooledString m_Id;
    PooledString m_SkuPartNumber;

    RTTR_ENABLE()
    RTTR_REGISTRATION_FRIEND
};

