#pragma once

#include "DlgFormsHTML.h"
#include "OnPremiseGroup.h"

class DlgOnPremGroupEditHTML : public DlgFormsHTML
{
public:
	DlgOnPremGroupEditHTML(vector<OnPremiseGroup>& p_Groups, DlgFormsHTML::Action p_Action, CWnd* p_Parent);

	bool processPostedData(const IPostedDataTarget::PostedData& data) override;

protected:
	using StringGetter = std::function <const boost::YOpt<PooledString>&(const OnPremiseGroup&)>;
	using StringSetter = std::function<void(OnPremiseGroup&, const boost::YOpt<PooledString>&)>;
	using BoolGetter = std::function<const boost::YOpt<bool>& (const OnPremiseGroup&)>;
	using BoolSetter = std::function<void(OnPremiseGroup&, const boost::YOpt<bool>&)>;

	void addEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, const vector<wstring>& p_ApplicableUserTypes);
	void addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues, const vector<wstring>& p_ApplicableUserTypes);
	void addEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides = BoolEditor::CheckUncheckedLabelOverrides());

	void generateJSONScriptData() override;

	bool hasProperty(const wstring& p_PropertyName) const;

protected:
	vector<OnPremiseGroup>& m_Groups;

	std::map<wstring, StringSetter> m_StringSetters;
	std::map<wstring, BoolSetter>	m_BoolSetters;
};	

