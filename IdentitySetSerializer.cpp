#include "IdentitySetSerializer.h"
#include "Identity.h"
#include "NullableSerializer.h"
#include "IdentitySerializer.h"

IdentitySetSerializer::IdentitySetSerializer(const IdentitySet& p_IdentitySet)
	:m_IdentitySet(p_IdentitySet)
{
}

void IdentitySetSerializer::Serialize()
{
	m_Json = web::json::value::object();
	auto& obj = m_Json.as_object();

	JsonSerializeUtil::SerializeAny(_YUID("application"), NullableSerializer<Identity, IdentitySerializer>(m_IdentitySet.Application), obj);
	JsonSerializeUtil::SerializeAny(_YUID("device"), NullableSerializer<Identity, IdentitySerializer>(m_IdentitySet.Device), obj);
	JsonSerializeUtil::SerializeAny(_YUID("user"), NullableSerializer<Identity, IdentitySerializer>(m_IdentitySet.User), obj);
}
