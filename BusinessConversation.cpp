#include "BusinessConversation.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessConversation>("Conversation") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Conversation"))))
		.constructor()(policy::ctor::as_object)
		.property("DisplayName", &BusinessConversation::m_DisplayName)
		;
}

BusinessConversation::BusinessConversation(const Conversation& p_Conversation)
{
	ASSERT(p_Conversation.Id.is_initialized());
	if (p_Conversation.Id.is_initialized())
		SetID(*p_Conversation.Id);

    m_HasAttachments = p_Conversation.HasAttachments;
    m_LastDeliveredDateTime = p_Conversation.LastDeliveredDateTime;
    m_Preview = p_Conversation.Preview;
    m_Topic = p_Conversation.Topic;
    m_UniqueSenders = p_Conversation.UniqueSenders;
    for (const auto& thread : p_Conversation.Threads)
        m_Threads.emplace_back(thread);
}

const boost::YOpt<bool>& BusinessConversation::GetHasAttachments() const
{
    return m_HasAttachments;
}

void BusinessConversation::SetHasAttachments(const boost::YOpt<bool>& p_Val)
{
    m_HasAttachments = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessConversation::GetLastDeliveredDateTime() const
{
    return m_LastDeliveredDateTime;
}

void BusinessConversation::SetLastDeliveredDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_LastDeliveredDateTime = p_Val;
}

const boost::YOpt<PooledString>& BusinessConversation::GetPreview() const
{
    return m_Preview;
}

void BusinessConversation::SetPreview(const boost::YOpt<PooledString>& p_Val)
{
    m_Preview = p_Val;
}

const boost::YOpt<PooledString>& BusinessConversation::GetTopic() const
{
    return m_Topic;
}

void BusinessConversation::SetTopic(const boost::YOpt<PooledString>& p_Val)
{
    m_Topic = p_Val;
}

const boost::YOpt<vector<PooledString>>& BusinessConversation::GetUniqueSenders() const
{
    return m_UniqueSenders;
}

void BusinessConversation::SetUniqueSenders(const boost::YOpt<vector<PooledString>>& p_Val)
{
    m_UniqueSenders = p_Val;
}

const vector<BusinessConversationThread>& BusinessConversation::GetConversationThreads() const
{
    return m_Threads;
}

void BusinessConversation::SetConversationThreads(const vector<BusinessConversationThread>& p_Val)
{
    m_Threads = p_Val;
}

const boost::YOpt<PooledString>& BusinessConversation::GetDisplayName() const
{
	return m_DisplayName;
}

void BusinessConversation::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}
