#pragma once

#include "BusinessChatMessage.h"

// only for identification issues in grids
class BusinessChatMessageReply : public BusinessChatMessage
{
public:
	using BusinessChatMessage::BusinessChatMessage;

private:
	RTTR_ENABLE(BusinessChatMessage)
	RTTR_REGISTRATION_FRIEND
};
