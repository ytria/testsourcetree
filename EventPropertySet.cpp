#include "EventPropertySet.h"

EventPropertySet::EventPropertySet()
{
}

void EventPropertySet::SetBodyPreview(bool p_Set)
{
	m_BodyPreview = p_Set;
}

void EventPropertySet::SetBodyContent(bool p_Set)
{
	m_FullBody = p_Set;
}

vector<rttr::property> EventPropertySet::GetPropertySet() const
{
	ASSERT(false);
	return {};
}

vector<PooledString> EventPropertySet::GetStringPropertySet() const
{
	vector<PooledString> properties = {
		_YTEXT("id"),
		_YTEXT("createdDateTime"),
		_YTEXT("lastModifiedDateTime"),
		_YTEXT("changeKey"),
		_YTEXT("categories"),
		_YTEXT("originalStartTimeZone"),
		_YTEXT("originalEndTimeZone"),
		_YTEXT("iCalUId"),
		_YTEXT("reminderMinutesBeforeStart"),
		_YTEXT("isReminderOn"),
		_YTEXT("hasAttachments"),
		_YTEXT("subject"),
		_YTEXT("importance"),
		_YTEXT("sensitivity"),
		_YTEXT("isAllDay"),
		_YTEXT("isCancelled"),
		_YTEXT("isOrganizer"),
		_YTEXT("responseRequested"),
		_YTEXT("seriesMasterId"),
		_YTEXT("showAs"),
		_YTEXT("type"),
		_YTEXT("webLink"),
		_YTEXT("onlineMeetingUrl"),
		_YTEXT("responseStatus"),
		_YTEXT("start"),
		_YTEXT("end"),
		_YTEXT("location"),
		_YTEXT("locations"),
		_YTEXT("recurrence"),
		_YTEXT("attendees"),
		_YTEXT("organizer"),
	};

	if (m_BodyPreview)
		properties.push_back(_YTEXT("bodyPreview"));
	if (m_FullBody)
		properties.push_back(_YTEXT("body"));

	return properties;
}
