#include "MetaDataColumnInfo.h"

#include "BaseO365Grid.h"
#include "CommandInfo.h"
#include "JsonSerializeUtil.h"

MetaDataColumnInfo::MetaDataColumnInfo(GridBackendColumn* p_SrcColumn)
{
	ASSERT(nullptr != p_SrcColumn);
	if (nullptr != p_SrcColumn)
	{
		m_PropertyName		= p_SrcColumn->GetUniqueID();
		m_Title				= p_SrcColumn->GetTitle();
		m_IsLoadMore		= p_SrcColumn->HasPresetFlag(O365Grid::g_ColumnsPresetMore);
		m_DefaultWidth		= p_SrcColumn->GetDefaultWidth();
		m_MinWidth			= p_SrcColumn->GetWidthMin();
		m_DataType			= p_SrcColumn->GetType();
		m_CellType			= p_SrcColumn->GetDefaultCellType();
		m_GroupCellType		= p_SrcColumn->GetGroupCellType();
		m_CellFormat		= p_SrcColumn->GetColumnFormat(false);
		m_GroupFormat		= p_SrcColumn->GetColumnFormat(true);
		m_IsCaseSensitive	= p_SrcColumn->HasFlag(GridBackendUtil::CASESENSITIVEFIELDS);
	}
}

MetaDataColumnInfo::MetaDataColumnInfo(GridBackendColumn* p_SrcColumn, const PooledString& p_PropertyName)
	: MetaDataColumnInfo(p_SrcColumn)
{
	m_PropertyName = p_PropertyName;
}

MetaDataColumnInfo::MetaDataColumnInfo()
	: m_DefaultWidth(0)
	, m_MinWidth(0)
	, m_DataType(GridBackendUtil::UNKNOWN)
	, m_CellType(GridBackendUtil::CELL_TYPE_STRING)
	, m_GroupCellType(GridBackendUtil::CELL_TYPE_STRING)
	, m_IsCaseSensitive(false)
	, m_IsLoadMore(false)
{

}

const PooledString& MetaDataColumnInfo::GetPropertyName() const
{
	return m_PropertyName;
}

bool MetaDataColumnInfo::IsLoadMore() const
{
	return m_IsLoadMore;
}

web::json::value MetaDataColumnInfo::Serialize() const
{
	auto obj = web::json::value::object();
	JsonSerializeUtil::SerializeString(_YTEXT("propertyName"), m_PropertyName, obj.as_object());
	JsonSerializeUtil::SerializeString(_YTEXT("title"), m_Title, obj.as_object());
	JsonSerializeUtil::SerializeInt32(_YTEXT("defaultWidth"), m_DefaultWidth, obj.as_object());
	JsonSerializeUtil::SerializeInt32(_YTEXT("minWidth"), m_MinWidth, obj.as_object());
	JsonSerializeUtil::SerializeInt32(_YTEXT("dataType"), m_DataType, obj.as_object());
	JsonSerializeUtil::SerializeInt32(_YTEXT("cellType"), m_CellType, obj.as_object());
	JsonSerializeUtil::SerializeInt32(_YTEXT("groupCellType"), m_GroupCellType, obj.as_object());
	JsonSerializeUtil::SerializeBool(_YTEXT("isCaseSensitive"), m_IsCaseSensitive, obj.as_object());
	JsonSerializeUtil::SerializeBool(_YTEXT("isLoadMore"), m_IsLoadMore, obj.as_object());

	return obj;
}

boost::YOpt<MetaDataColumnInfo> MetaDataColumnInfo::Deserialize(const web::json::value& p_Serialized)
{
	boost::YOpt<MetaDataColumnInfo> result;

	ASSERT(p_Serialized.is_object());
	if (p_Serialized.is_object())
	{
		auto obj = p_Serialized.as_object();
		result.emplace();

		if (result)
		{
			boost::YOpt<PooledString> ret;
			JsonSerializeUtil::DeserializeString(_YTEXT("propertyName"), ret, obj);
			if (ret)
				result->m_PropertyName = *ret;
			else
				result.reset();
		}
		if (result)
		{
			boost::YOpt<PooledString> ret;
			JsonSerializeUtil::DeserializeString(_YTEXT("title"), ret, obj);
			if (ret)
				result->m_Title = *ret;
			else
				result.reset();
		}
		if (result)
		{
			boost::YOpt<int32_t> ret;
			JsonSerializeUtil::DeserializeInt32(_YTEXT("defaultWidth"), ret, obj);
			if (ret)
				result->m_DefaultWidth = *ret;
			else
				result.reset();
		}
		if (result)
		{
			boost::YOpt<int32_t> ret;
			JsonSerializeUtil::DeserializeInt32(_YTEXT("minWidth"), ret, obj);
			if (ret)
				result->m_MinWidth = *ret;
			else
				result.reset();
		}
		if (result)
		{
			boost::YOpt<int32_t> ret;
			JsonSerializeUtil::DeserializeInt32(_YTEXT("dataType"), ret, obj);
			if (ret)
				result->m_DataType = (GridBackendUtil::DATATYPE)*ret;
			else
				result.reset();
		}
		if (result)
		{
			boost::YOpt<int32_t> ret;
			JsonSerializeUtil::DeserializeInt32(_YTEXT("cellType"), ret, obj);
			if (ret)
				result->m_CellType = (GridBackendUtil::GRIDCELLTYPE)*ret;
			else
				result.reset();
		}
		if (result)
		{
			boost::YOpt<int32_t> ret;
			JsonSerializeUtil::DeserializeInt32(_YTEXT("groupCellType"), ret, obj);
			if (ret)
				result->m_GroupCellType = (GridBackendUtil::GRIDCELLTYPE)*ret;
			else
				result.reset();
		}

		// Those aren't serialized
		//obj[_YTEXT("cellFormat")];
		//obj[_YTEXT("groupFormat")];

		if (result)
		{
			boost::YOpt<bool> ret;
			JsonSerializeUtil::DeserializeBool(_YTEXT("isCaseSensitive"), ret, obj);
			if (ret)
				result->m_IsCaseSensitive = *ret;
			else
				result.reset();
		}
		if (result)
		{
			boost::YOpt<bool> ret;
			JsonSerializeUtil::DeserializeBool(_YTEXT("isLoadMore"), ret, obj);
			if (ret)
				result->m_IsLoadMore = *ret;
			else
				result.reset();
		}
	}

	if (!result)
		LoggerService::Debug(_YDUMPFORMAT(L"MetaDataColumnInfo couldn't be deserialized:\n%s", p_Serialized.serialize().c_str()));

	return result;
}

bool MetaDataColumnInfo::operator==(const MetaDataColumnInfo& p_Other) const
{
	return m_PropertyName == p_Other.m_PropertyName;
}

GridBackendColumn* MetaDataColumnInfo::AddTo(CacheGrid& p_Grid, const wstring& p_Family, const wstring& p_TitleFormat, const wstring& p_UniqueIDPrefix) const
{
	auto col = p_Grid.AddColumn(p_UniqueIDPrefix + m_PropertyName.c_str(), _YTEXTFORMAT(p_TitleFormat.c_str(), m_Title.c_str()), p_Family, m_DataType, m_CellType, m_DefaultWidth, m_MinWidth, true, m_GroupCellType);

	if (m_CellFormat) // can be uninitialized in snapshot (not serialized)
		col->SetColumnFormat(*m_CellFormat, false);
	if (m_GroupFormat) // can be uninitialized in snapshot (not serialized)
		col->SetColumnFormat(*m_GroupFormat, true);

	if (m_IsCaseSensitive)
		col->SetFlag(GridBackendUtil::CASESENSITIVEFIELDS);

	if (IsLoadMore())
	{
		col->SetPresetFlags({ O365Grid::g_ColumnsPresetDefault, O365Grid::g_ColumnsPresetMetaMore });
		col->SetNoFieldText(YtriaTranslate::Do(GridGroups_GridGroups_3, _YLOC("N/A")).c_str());
		col->SetNoFieldTooltip(YtriaTranslate::Do(MetaDataColumnInfo_AddTo_2, _YLOC("Not available in cache.")).c_str());
		// FIXME: Use a specific icon!
		col->SetNoFieldIcon(GridBackendUtil::ICON_EMPTYFIELD);
	}
	else
	{
		col->SetPresetFlags({ O365Grid::g_ColumnsPresetDefault });
	}

	return col;
}

GridBackendColumn* MetaDataColumnInfo::AddTo(CacheGrid& p_Grid, const wstring& p_Family) const
{
	return AddTo(p_Grid, p_Family, _YTEXT("\U00002B9E %s"), _YTEXT("addtnl."));
}

// ==================================================================================================

MetaDataColumnInfosSetting::MetaDataColumnInfosSetting(const Origin& p_Origin)
	: m_RegistryKey(_YTEXT("Software\\Ytria\\sapio365\\User_Settings\\AdditionalMetadata"))
	, m_RegistryValue(p_Origin == Origin::User ? GridUtil::g_AutoNameUsers : p_Origin == Origin::Group ? GridUtil::g_AutoNameGroups : _YTEXT(""))
{
	ASSERT(!m_RegistryValue.empty()); // Origin should be either Users or Groups.
}

bool MetaDataColumnInfosSetting::ShouldAskAgain() const
{
	wstring aStrReturnedValue;
	return !exists()
		|| !Registry::readInteger(HKEY_CURRENT_USER, m_RegistryKey, m_RegistryValue + _YTEXT("_set"), aStrReturnedValue)
		|| aStrReturnedValue.empty();
}

bool MetaDataColumnInfosSetting::exists() const
{
	wstring aStrReturnedValue;
	return Registry::readString(HKEY_CURRENT_USER, m_RegistryKey, m_RegistryValue, aStrReturnedValue);
}

boost::YOpt<vector<wstring>> MetaDataColumnInfosSetting::get() const
{
	boost::YOpt<vector<wstring>> val;
	wstring aStrReturnedValue;
	if (Registry::readString(HKEY_CURRENT_USER, m_RegistryKey, m_RegistryValue, aStrReturnedValue))
		val = Str::explodeIntoVector(aStrReturnedValue, wstring(_YTEXT("|")), false, true);
	return val;
}

boost::YOpt<MetaDataColumnInfos> MetaDataColumnInfosSetting::Get(const CacheGrid& p_SourceGrid, std::function<bool(GridBackendColumn*)> p_ColumnFilter) const
{
	boost::YOpt<MetaDataColumnInfos> ret;
	auto columns = get();
	if (columns)
	{
		ret.emplace();
		for (auto& c : *columns)
		{
			auto col = p_SourceGrid.GetColumnByUniqueID(c);
			ASSERT(p_ColumnFilter(col));
			if (p_ColumnFilter(col))
				ret->emplace_back(col);
		}
	}
	return ret;
}

bool MetaDataColumnInfosSetting::Set(const boost::YOpt<MetaDataColumnInfos>& p_MetaDataColumnInfos, const boost::YOpt<bool>& p_DontAskAgain)
{
	bool success = false;

	if (p_MetaDataColumnInfos)
	{
		wstring str;
		for (const auto& c : *p_MetaDataColumnInfos)
			str += c.GetPropertyName() + _YTEXT("|");
		if (!str.empty())
			str.pop_back(); // remove trailing '|'
		success = Registry::writeString(HKEY_CURRENT_USER, m_RegistryKey, m_RegistryValue, str);
	}
	else
	{
		success = Registry::erase(HKEY_CURRENT_USER, m_RegistryKey, m_RegistryValue);
	}

	if (p_DontAskAgain.is_initialized())
	{
		if (*p_DontAskAgain)
			success = Registry::writeInteger(HKEY_CURRENT_USER, m_RegistryKey, m_RegistryValue + _YTEXT("_set"), _YTEXT("1")) && success;
		else
			success = Registry::erase(HKEY_CURRENT_USER, m_RegistryKey, m_RegistryValue + _YTEXT("_set")) && success;
	}

	return success;
}

Origin MetaDataColumnInfosSetting::GetOrigin() const
{
	Origin origin = Origin::NotSet;

	if (GridUtil::g_AutoNameUsers == m_RegistryValue)
		origin = Origin::User;
	else if (GridUtil::g_AutoNameGroups == m_RegistryValue)
		origin = Origin::Group;

	return origin;
}
