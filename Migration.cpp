#include "Migration.h"

void Migration::AddStep(const StepType& p_Operation)
{
	m_Steps.push_back(std::move(p_Operation));
}

bool Migration::Apply()
{
	for (const auto& step : m_Steps)
	{
		if (!step())
			return false;
	}

	return true;
}
