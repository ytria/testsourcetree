#pragma once

#include "BusinessObject.h"
#include "BusinessUser.h"

class DirectoryRole;

class BusinessDirectoryRole : public BusinessObject
{
public:
    BusinessDirectoryRole() = default;
    BusinessDirectoryRole(const DirectoryRole& p_DirectoryRole);

    const boost::YOpt<PooledString>& GetDescription() const;
    void SetDescription(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<PooledString>& GetDisplayName() const;
    void SetDisplayName(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<PooledString>& GetRoleTemplateId() const;
    void SetRoleTemplateId(const boost::YOpt<PooledString>& p_Val);

    void AddMember(const BusinessUser& p_Member);
    const std::vector<BusinessUser>& GetMembers() const;

private:
    boost::YOpt<PooledString> m_Description;
    boost::YOpt<PooledString> m_DisplayName;
    boost::YOpt<PooledString> m_RoleTemplateId;

    std::vector<BusinessUser> m_Members;

    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND
    friend class DirectoryRoleDeserializer;
};

