#pragma once

#include "BusinessObjectManager.h"
#include "BusinessUser.h"
#include "MacroRequest.h"

#include <boost/functional/hash/hash.hpp>

template<>
struct std::hash<std::vector<UBI>>
{
	size_t operator()(const std::vector<UBI>& _Keyval) const
	{
		return boost::hash_range(_Keyval.begin(), _Keyval.end());
	}
};

class UserMacroRequest : public MacroRequest
{
public:
	UserMacroRequest(BusinessUser& p_User, const std::unordered_map<std::vector<UBI>, std::vector<rttr::property>>& p_Properties, uint32_t p_Flags, std::shared_ptr<BusinessObjectManager> p_BOM, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData);

private:
	enum KeepOnlyNullValuesMode
	{
		NO,
		YES,
		ONLYIFERROR,
	};
	void addPropertiesStep(size_t index, const vector<rttr::property>& p_Properties, KeepOnlyNullValuesMode p_KeepOnlyNullValuesMode, const std::vector<UBI>& p_BlockIds);

	std::unique_ptr<MacroRequest::Step> CreateMailboxSettingsPropertiesStep(BusinessUser& p_User, int p_Flags) const;
	std::unique_ptr<MacroRequest::Step> CreateMailFolderStep(BusinessUser& p_User, int p_Flags) const;
	std::unique_ptr<MacroRequest::Step> CreateDriveStep(BusinessUser& p_User, int p_Flags) const;
	std::unique_ptr<MacroRequest::Step> CreateManagerStep(BusinessUser& p_User, int p_Flags) const;

	BusinessUser& m_User;
	const std::unordered_map<std::vector<UBI>, std::vector<rttr::property>>& m_Properties;
	std::shared_ptr<BusinessObjectManager> m_BOM;
	std::shared_ptr<IRequestLogger> m_Logger;
	YtriaTaskData m_TaskData;
};
