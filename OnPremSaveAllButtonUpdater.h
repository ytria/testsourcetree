#pragma once

#include "IButtonUpdater.h"

template<class FrameClass>
class OnPremSaveAllButtonUpdater : public IButtonUpdater
{
public:
	OnPremSaveAllButtonUpdater(FrameClass& p_Frame);
	void Update(CCmdUI* p_CmdUi) override;

private:
	FrameClass& m_Frame;
};

template<class FrameClass>
OnPremSaveAllButtonUpdater<FrameClass>::OnPremSaveAllButtonUpdater(FrameClass& p_Frame)
	: m_Frame(p_Frame)
{

}

template<class FrameClass>
void OnPremSaveAllButtonUpdater<FrameClass>::Update(CCmdUI* p_CmdUi)
{
	p_CmdUi->Enable(m_Frame.IsConnected()
		&& m_Frame.HasNoTaskRunning()
		&& (m_Frame.hasModificationsPending(false) || m_Frame.hasOnPremiseChanges(false))
	);
}

