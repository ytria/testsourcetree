#include "ProvisionedPlanDeserializer.h"

#include "JsonSerializeUtil.h"

void ProvisionedPlanDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("capabilityStatus"), m_Data.m_CapabilityStatus, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("provisioningStatus"), m_Data.m_ProvisioningStatus, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("service"), m_Data.m_Service, p_Object);
}
