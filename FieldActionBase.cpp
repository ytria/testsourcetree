#include "FieldActionBase.h"

FieldActionBase::FieldActionBase(O365Grid& p_Grid, GridBackendRow* p_Row, GridBackendColumn* p_Column)
    :IRestorableGridAction(p_Grid)
{
    p_Grid.GetRowPK(p_Row, m_PrimaryKey);
    m_ColId = p_Column->GetID();
}

const FieldActionBase::row_primary_key_t& FieldActionBase::GetPrimaryKey() const
{
    return m_PrimaryKey;
}

UINT FieldActionBase::GetColumnId() const
{
    return m_ColId;
}
