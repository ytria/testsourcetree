#pragma once

class InformationalUrl
{
public:
	boost::YOpt<PooledString> m_LogoUrl;
	boost::YOpt<PooledString> m_MarketingUrl;
	boost::YOpt<PooledString> m_PrivacyStatementUrl;
	boost::YOpt<PooledString> m_SupportUrl;
	boost::YOpt<PooledString> m_TermsOfServiceUrl;
};

