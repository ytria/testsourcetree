#pragma once

#include "BasicRowInitializer.h"

class BaseO365RowInitializer : public BasicRowInitializer
{
public:
    virtual void InitializeRow(GridBackendRow& p_Row, const rttr::type& p_Type, const rttr::property* p_Property = nullptr) const override;
};

