#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "PasswordCredential.h"

class PasswordCredentialDeserializer : public JsonObjectDeserializer, public Encapsulate<PasswordCredential>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

