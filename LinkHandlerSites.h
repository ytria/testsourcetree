#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerSites : public IBrowserLinkHandler
{
public:
    virtual void Handle(YBrowserLink& p_Link) const override;
};