#pragma once

#include "PooledString.h"
#include "RttrIncludes.h"

class BusinessOrgContact;

struct CachedOrgContact
{
public:
	CachedOrgContact() = default;
    CachedOrgContact(const BusinessOrgContact& p_OrgContact);
	BusinessOrgContact ToBusinessOrgContact() const;

	PooledString GetId() const;
	PooledString GetDisplayName() const;
    PooledString GetMail() const;

private:
    PooledString m_Id;
    boost::YOpt<PooledString> m_DisplayName;
    boost::YOpt<PooledString> m_Mail;
    // TODO: More fields

    RTTR_ENABLE()
	RTTR_REGISTRATION_FRIEND
};
