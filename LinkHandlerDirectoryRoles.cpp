#include "LinkHandlerDirectoryRoles.h"

#include "Command.h"
#include "CommandInfo.h"
#include "CommandDispatcher.h"

void LinkHandlerDirectoryRoles::Handle(YBrowserLink& p_Link) const
{
    HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);
 
	CommandInfo info;
    info.SetOrigin(Origin::Tenant);
    CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::DirectoryRoles, Command::ModuleTask::List, info, { nullptr, g_ActionNameShowDirectoryRoles, nullptr, p_Link.GetAutomationAction() }));
}
