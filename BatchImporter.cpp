#include "BatchImporter.h"

#include "BusinessGroup.h"
#include "BusinessGroupConfiguration.h"
#include "DlgFormsHTML.h"
#include "ExportPreferences.h"
#include "FileInput.h"
#include "MsGraphFieldNames.h"
#include "YCodeJockMessageBox.h"
#include "YFileDialog.h"

#define ALLOW_IMPORT_GROUPTYPE 0

namespace
{
	static const wstring g_Skip = _YTEXT("skip");
	static const wstring g_EnableSkip = _YTEXT("enableSkip");
	static const wstring g_DataHasHeaders = _YTEXT("dataHasHeaders");
	static const wstring g_ManualGroupType = _YTEXT("_GroupType_");
	static const wstring g_MakeSelectionValue = _YTEXT("empty");
	static const wstring g_KeyProperty = _YTEXT("keyProperty");
	static const wstring g_KeyHeader = _YTEXT("keyHeader");
#if ALLOW_IMPORT_GROUPTYPE
	static const wstring g_ImportGroupType = _YTEXT("ImportGroupType");
#endif

	const wstring& GetName(const BatchImporter::MappingProperties::value_type& p_Val)
	{
		return p_Val.first;
	}

	const wstring& GetDisplayName(const BatchImporter::MappingProperties::value_type& p_Val)
	{
		return std::get<0>(p_Val.second);
	}

	bool LooksLike(const wstring& p_Name, const BatchImporter::MappingProperties::value_type& p_Val)
	{
		const auto name = Str::KeepOnlyAlphaNum(p_Name);
		return MFCUtil::StringMatchW(name, Str::KeepOnlyAlphaNum(GetName(p_Val))) || MFCUtil::StringMatchW(name, Str::KeepOnlyAlphaNum(GetDisplayName(p_Val)));
	}

	bool IsMandatory(const BatchImporter::MappingProperties::value_type& p_Val)
	{
		return std::get<1>(p_Val.second);
	}

	const BatchImporter::Tip& GetTip(const BatchImporter::MappingProperties::value_type& p_Val)
	{
		return std::get<2>(p_Val.second);
	}

	struct BatchImportConfigFile
	{
	public:
		BatchImportConfigFile(const wstring& p_FilePath)
			: m_FilePath(p_FilePath)
			, m_Val(web::json::value::object(false))
			, m_Changed(false)
		{
			if (CRMpipe().IsFeatureSandboxed())
			{
				std::string str;
				{
					std::ifstream file(m_FilePath, std::ios::in | std::ios::binary);
					if (file)
					{
						file.seekg(0, std::ios::end);
						auto pos = file.tellg();
						if (pos >= 0)
							str.resize(static_cast<size_t>(pos));
						file.seekg(0, std::ios::beg);
						file.read(&str[0], str.size());
					}
				}

				if (!str.empty())
					load(str);
			}
		}

		~BatchImportConfigFile()
		{
			if (m_Changed)
			{
				if (CRMpipe().IsFeatureSandboxed())
				{
					std::ofstream file(m_FilePath, ios::out | ios::binary);
					file << Str::convertToUTF8(m_Val.to_string());
				}
			}
		}

		boost::YOpt<wstring> GetLastMappingFor(const wstring& p_ColId)
		{
			boost::YOpt<wstring> val;
			if (m_Val.has_string_field(p_ColId))
				val = m_Val[p_ColId].as_string();
			return val;
		}

		void SetLastMappingFor(const wstring& p_ColId, const boost::YOpt<wstring>& p_Source)
		{
			if (p_Source)
				m_Val[p_ColId] = web::json::value::string(*p_Source);
			else
				m_Val.erase(p_ColId);

			m_Changed = true;
		}

	private:
		bool load(const char* p_UTF8JsonString, size_t p_Length)
		{
			if (p_Length >= 3 && 0 == strncmp(p_UTF8JsonString, (const char*)UTF8_BOM, 3))
			{
				// Remove UTF8 bom
				p_UTF8JsonString += 3;
			}

			wstring unicodeString;
			MFCUtil::convertUTF8_to_UNICODE(p_UTF8JsonString, p_Length, unicodeString);
			return load(unicodeString);
		}

		bool load(std::string& p_UTF8JsonString)
		{
			return load(p_UTF8JsonString.c_str(), p_UTF8JsonString.size());
		}

		bool load(const wstring& p_JsonString)
		{
			bool success = true;
			std::error_code err;
			auto val = web::json::value::parse(p_JsonString, err);
			if (val.is_null() || err)
			{
				// error
				ASSERT(false);
				TraceIntoFile::trace(_YDUMP("BatchImportConfigFile"), _YDUMP("load"), _YDUMPFORMAT(L"ERROR %s", Str::convertFromUTF8(err.message()).c_str()));
				success = false;
			}
			else
			{
				ASSERT(val.is_object());
				if (val.is_object())
					m_Val = val;
				else
					success = false;
			}

			return success;
		}

	private:
		wstring m_FilePath;
		web::json::value m_Val;
		bool m_Changed;
	};

	class DlgImportMappingHTML : public DlgFormsHTML
	{
	public:
		using Header = std::pair<wstring, wstring>; // <name, display name>
		using Headers = std::map<size_t, Header>; // <index, Header>

		struct IConfig
		{
			virtual const Headers& GetSourceHeaders() = 0;
			virtual const BatchImporter::MappingProperties& GetMappingProperties() = 0;
			virtual const std::map<wstring, wstring>& GetPropsToMicrosoftCSVImportHeaders() = 0;
			virtual const BatchImporter::KeyProperties& GetKeyProperties() = 0;
		};

		void SetConfig(std::shared_ptr<IConfig> p_Config)
		{
			ASSERT(!m_Config); // Should we be able to change config?
			m_Config = p_Config;
		}

		const std::multimap<size_t, wstring>& GetMapping() const
		{
			return m_Mapping;
		}

		const boost::YOpt<std::pair<size_t, wstring>>& GetKeyMatching() const
		{
			return m_KeyMatching;
		}

		bool DataHasHeaders() const
		{
			return m_HasHeaders;
		}

		void SetSkipValue(const wstring& p_DefaultSkipValue)
		{
			m_SkipValue = p_DefaultSkipValue;
		}

		const boost::YOpt<wstring>& GetSkipValue() const
		{
			return m_SkipValue;
		}

	protected:
		DlgImportMappingHTML(std::unique_ptr<BatchImportConfigFile>&& p_BatchImportConfigFile, CWnd* p_Parent)
			: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
			, m_HasHeaders(true)
			, m_BatchImportConfigFile(std::move(p_BatchImportConfigFile))
		{
		}

		wstring getDialogTitle() const override
		{
			return _T("Import Setup");
		}

		void initJson(const wstring& p_DlgName)
		{
			initMain(p_DlgName, false, false);

			getGenerator().Add(BoolToggleEditor({ { g_DataHasHeaders, _T("My data has headers"), EditorFlags::DIRECT_EDIT, m_HasHeaders }, { _YTEXT(" "), _YTEXT(" ") } }));
			getGenerator().Add(BoolToggleEditor({ { g_EnableSkip, _T("Enable Value Skipping"), EditorFlags::DIRECT_EDIT, true }, { _YTEXT(" "), _YTEXT(" ") } }));
			getGenerator().Add(WithTooltip<StringEditor>({ g_Skip, _T("Skip value"), EditorFlags::DIRECT_EDIT, m_SkipValue ? *m_SkipValue : _YTEXT("") }, _T("Fields with that particular value will be ignored during import.")));
			getGenerator().addEvent(g_EnableSkip, { g_Skip, EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueFalse });
			getGenerator().addIconTextField(_YTEXT("text"), _T("Select which columns to import."), _YTEXT("fas fa-info-circle"), _YTEXT("#e4f0fb"), _YTEXT("#5882bc"));

			const auto& sourceHeaders = m_Config->GetSourceHeaders();

			if (!sourceHeaders.empty())
			{
				auto itLast = sourceHeaders.end();
				--itLast;
				auto maxNum = itLast->first;
				m_NumberPadding = maxNum > 0 ? static_cast<int>(std::floor(std::log10(maxNum))) : 1;
				for (const auto& sourceHeader : sourceHeaders)
				{
					ASSERT(!sourceHeader.second.second.empty());
					m_HeaderLabelAndValues.emplace_back(sourceHeader.second.second, Str::getStringFromNumber(sourceHeader.first, m_NumberPadding));
				}
			}

			static const wstring makeSelectionLabel = YtriaTranslate::Do(DlgBusinessEditHTML_addListEditor_1, _YLOC("-- Please select an option --")).c_str();
			ASSERT(m_HeaderLabelAndValuesWithEmptyChoice.empty());
			m_HeaderLabelAndValuesWithEmptyChoice.emplace_back(makeSelectionLabel, g_MakeSelectionValue);
			m_HeaderLabelAndValuesWithEmptyChoice.insert(m_HeaderLabelAndValuesWithEmptyChoice.end(), m_HeaderLabelAndValues.begin(), m_HeaderLabelAndValues.end());

			m_DefaultValue = [&sourceHeaders, &microsoftHeaders = m_Config->GetPropsToMicrosoftCSVImportHeaders(), numberPadding = m_NumberPadding, &batchImportConfigFile = m_BatchImportConfigFile](const auto& p_Property, const wstring& emptyValue) -> const wstring&
			{
				const auto itt = microsoftHeaders.find(GetName(p_Property));
				const auto& msName = (microsoftHeaders.end() != itt) ? itt->second : Str::g_EmptyString;

				const auto lastUserMatch = batchImportConfigFile->GetLastMappingFor(GetName(p_Property));

				auto it = sourceHeaders.end();
				if (lastUserMatch && !lastUserMatch->empty())
				{
					it = std::find_if(sourceHeaders.begin(), sourceHeaders.end(), [&userMatch = *lastUserMatch](const auto& item)
						{
							return item.second.first == userMatch;
						});
				}

				if (it == sourceHeaders.end())
				{
					it = std::find_if(sourceHeaders.begin(), sourceHeaders.end(), [&msName, &p_Property](const auto& item)
						{
							return (!msName.empty() && item.second.first == msName) || LooksLike(item.second.first, p_Property);
						});
				}

				if (it != sourceHeaders.end())
				{
					static wstring val;
					val = Str::getStringFromNumber(it->first, numberPadding);
					return val;
				}

				return emptyValue;
			};
		}

		void addKeyEditors()
		{
			ASSERT(m_Config);

			getGenerator().addCategory(_T("Existing object matching"), CategoryFlags::EXPAND_BY_DEFAULT);
			{
				getGenerator().addIconTextField(_YTEXT("keytext"), _T("Select which data will be used to match existing object."), _YTEXT("fas fa-info-circle"), _YTEXT("#e4f0fb"), _YTEXT("#5882bc"));
				auto defaultColumnValue = Str::g_EmptyString;
				{
					const auto& keyProps = m_Config->GetKeyProperties();
					const auto& sourceHeaders = m_Config->GetSourceHeaders();
					ComboEditor::LabelsAndValues labelAndValues;
					auto defaultValue = Str::g_EmptyString;
					for (auto& item : keyProps)
					{
						labelAndValues.emplace_back(item.second, item.first);

						if (defaultValue.empty())
						{
							auto it = std::find_if(sourceHeaders.begin(), sourceHeaders.end(), [&dispName = item.second, &propName = item.first](const auto& item)
							{
								return item.second.first == dispName || item.second.first == propName;
							});

							if (it != sourceHeaders.end())
							{
								defaultValue = item.first;
								defaultColumnValue = Str::getStringFromNumber(it->first, m_NumberPadding);
							}
						}
					}
					getGenerator().Add(ComboEditor({ g_KeyProperty, _T("Property"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, defaultValue }, labelAndValues));
				}

				getGenerator().Add(ComboEditor({g_KeyHeader, _T("Column"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, defaultColumnValue}, m_HeaderLabelAndValues));
			}
		}

		void addMappingEditors()
		{
			ASSERT(m_Config);				
			ASSERT(m_DefaultValue);

			bool hasRequired = false;
			const auto& mappingProperties = m_Config->GetMappingProperties();
			getGenerator().addCategory(_T("Required Properties"), CategoryFlags::EXPAND_BY_DEFAULT);
			{
				for (const auto& item : mappingProperties)
				{
					if (IsMandatory(item))
					{
						getGenerator().Add(ComboEditor({ item.first, GetDisplayName(item), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, m_DefaultValue(item, Str::g_EmptyString) }, m_HeaderLabelAndValues));
						const auto& tip = GetTip(item);
						if (!tip.m_Text.empty())
						{
							getGenerator().addIconTextField(item.first + _YTEXT("_tip"), tip.m_Text, _YTEXT("fas fa-info-circle"));
							if (!tip.m_AlwaysShown)
								getGenerator().addEvent(item.first, { item.first + _YTEXT("_tip"), EventTarget::g_HideIfEqual, _YTEXT("") });
						}

						hasRequired = true;
					}
				}
			}

			getGenerator().addCategory(hasRequired ? _T("Other Properties") : _T("Properties"), /*CategoryFlags::NOFLAG*/CategoryFlags::EXPAND_BY_DEFAULT);
			{
				for (const auto& item : mappingProperties)
				{
					if (!IsMandatory(item))
					{
						getGenerator().Add(WithCustomResetValue<ComboEditor>({ { item.first, GetDisplayName(item), EditorFlags::DIRECT_EDIT, m_DefaultValue(item, g_MakeSelectionValue) }, m_HeaderLabelAndValuesWithEmptyChoice }, g_MakeSelectionValue));
						const auto& tip = GetTip(item);
						if (!tip.m_Text.empty())
						{
							getGenerator().addIconTextField(item.first + _YTEXT("_tip"), tip.m_Text, _YTEXT("fas fa-info-circle"));
							if (!tip.m_AlwaysShown)
								getGenerator().addEvent(item.first, { item.first + _YTEXT("_tip"), EventTarget::g_HideIfEqual, g_MakeSelectionValue });
						}
					}
				}
			}
		}

		void addButtons()
		{
			getGenerator().addApplyButton(_T("OK"));
			getGenerator().addCancelButton(_T("Cancel"));
			getGenerator().addResetButton(_T("Reset"));
		}

		virtual bool processValue(const IPostedDataTarget::PostedData::value_type& p_Value)
		{
			return false;
		}

		// Returns true if the dialog can be closed.
		bool processPostedData(const IPostedDataTarget::PostedData& data) override
		{
			m_SkipValue.reset();
			for (const auto& item : data)
			{
				if (item.second == g_MakeSelectionValue || item.second == g_EnableSkip)
					continue;
				else if (g_DataHasHeaders == item.first)
					m_HasHeaders = Str::getBoolFromString(item.second);
				else if (g_Skip == item.first)
					m_SkipValue = item.second;
				else if (g_KeyHeader == item.first)
				{
					if (!m_KeyMatching)
						m_KeyMatching.emplace();
					m_KeyMatching->first = Str::getUINT32FromString(item.second);
				}
				else if (g_KeyProperty == item.first)
				{
					if (!m_KeyMatching)
						m_KeyMatching.emplace();
					m_KeyMatching->second = item.second;
				}
				else if (!processValue(item))
				{
					auto i = Str::getUINT32FromString(item.second);
					m_Mapping.emplace(i, item.first);
					const auto& headers = m_Config->GetSourceHeaders();
					auto it = headers.find(i);
					ASSERT(headers.end() != it);
					m_BatchImportConfigFile->SetLastMappingFor(item.first, it->second.first);
				}
			}

			return true;
		}

	private:
		std::shared_ptr<IConfig> m_Config;
		std::multimap<size_t, wstring> m_Mapping;
		bool m_HasHeaders;
		std::unique_ptr<BatchImportConfigFile> m_BatchImportConfigFile;
		boost::YOpt<std::pair<size_t, wstring>> m_KeyMatching;
		boost::YOpt<wstring> m_SkipValue;

		ComboEditor::LabelsAndValues m_HeaderLabelAndValues;
		ComboEditor::LabelsAndValues m_HeaderLabelAndValuesWithEmptyChoice;
		int m_NumberPadding;

		std::function<const wstring & (const BatchImporter::MappingProperties::value_type&, const wstring&)> m_DefaultValue;
	};

	struct Config : public DlgImportMappingHTML::IConfig
	{
	public:
		Config(const BatchImporter::KeyProperties& p_KeyProperties, const DlgImportMappingHTML::Headers& p_Headers, const BatchImporter::MappingProperties& p_MappingProperties)
			: m_Headers(p_Headers)
			, m_MappingProperties(p_MappingProperties)
			, m_KeyProperties(p_KeyProperties)
		{
		}

		const DlgImportMappingHTML::Headers& GetSourceHeaders() override
		{
			return m_Headers;
		}

		const BatchImporter::MappingProperties& GetMappingProperties()override
		{
			return m_MappingProperties;
		}

		const BatchImporter::KeyProperties& GetKeyProperties() override
		{
			return m_KeyProperties;
		}

	private:
		const DlgImportMappingHTML::Headers& m_Headers;
		const BatchImporter::MappingProperties& m_MappingProperties;
		const BatchImporter::KeyProperties& m_KeyProperties;
	};

	struct UserConfig : public Config
	{
	public:
		using Config::Config;

		const std::map<wstring, wstring>& GetPropsToMicrosoftCSVImportHeaders() override
		{
			// https://docs.microsoft.com/en-us/office365/enterprise/add-several-users-at-the-same-time
			static std::map<wstring, wstring> propsToMicrosoftUserCSVImportHeaders
			{
				{_YTEXT(O365_USER_USERPRINCIPALNAME), _YTEXT("User Name")			},
				{_YTEXT(O365_USER_GIVENNAME)		, _YTEXT("First Name")			},
				{_YTEXT(O365_USER_SURNAME)			, _YTEXT("Last Name")			},
				{_YTEXT(O365_USER_DISPLAYNAME)		, _YTEXT("Display Name")		},
				{_YTEXT(O365_USER_JOBTITLE)			, _YTEXT("Job Title")			},
				{_YTEXT(O365_USER_DEPARTMENT)		, _YTEXT("Department")			},
				{_YTEXT(O365_USER_OFFICELOCATION)	, _YTEXT("Office Number")		},
				{_YTEXT(O365_USER_BUSINESSPHONES)	, _YTEXT("Office Phone")		},
				{_YTEXT(O365_USER_MOBILEPHONE)		, _YTEXT("Mobile Phone")		},
				{_YTEXT(O365_USER_FAXNUMBER)		, _YTEXT("Fax")					},
				{_YTEXT(O365_USER_STREETADDRESS)	, _YTEXT("Address")				},
				{_YTEXT(O365_USER_CITY)				, _YTEXT("City")				},
				{_YTEXT(O365_USER_STATE)			, _YTEXT("State or Province")	},
				{_YTEXT(O365_USER_POSTALCODE)		, _YTEXT("ZIP or Postal Code")	},
				{_YTEXT(O365_USER_COUNTRY)			, _YTEXT("Country or Region")	},
			};

			return propsToMicrosoftUserCSVImportHeaders;
		}
	};

	struct GroupConfig : public Config
	{
	public:
		using Config::Config;

		const std::map<wstring, wstring>& GetPropsToMicrosoftCSVImportHeaders() override
		{
			static const std::map<wstring, wstring> empty{};
			return empty;
		}
	};

	class DlgUserImportMappingHTML : public DlgImportMappingHTML
	{
	public:
		DlgUserImportMappingHTML(CWnd* p_Parent)
			: DlgImportMappingHTML(std::make_unique<BatchImportConfigFile>(SQLiteEngine::getLocalAppDataDBFileFolder() + _YTEXT("users_import_config.json")),  p_Parent)
		{
		}

		void generateJSONScriptData() override
		{
			initJson(_YTEXT("DlgUserImportMappingHTML"));
			addMappingEditors();
			addButtons();
		}
	};

	class DlgUserUpdateMappingHTML : public DlgImportMappingHTML
	{
	public:
		DlgUserUpdateMappingHTML(CWnd* p_Parent)
			: DlgImportMappingHTML(std::make_unique<BatchImportConfigFile>(SQLiteEngine::getLocalAppDataDBFileFolder() + _YTEXT("users_import_config.json")), p_Parent)
		{
		}

		void generateJSONScriptData() override
		{
			initJson(_YTEXT("DlgUserUpdateMappingHTML"));
			addKeyEditors();
			addMappingEditors();
			addButtons();
		}
	};

	class DlgGroupImportMappingHTML : public DlgImportMappingHTML
	{
	public:
		DlgGroupImportMappingHTML(CWnd* p_Parent)
			: DlgImportMappingHTML(std::make_unique<BatchImportConfigFile>(SQLiteEngine::getLocalAppDataDBFileFolder() + _YTEXT("groups_import_config.json")), p_Parent)
		{
		}

		const boost::YOpt<wstring>& GetGroupType() const
		{
			ASSERT(m_GroupType);
			return m_GroupType;
		}

	protected:
		void generateJSONScriptData() override
		{
			initJson(_YTEXT("DlgGroupImportMappingHTML"));

			static ComboEditor::LabelsAndValues g_GroupTypeLabelsAndValues
			{
				{ BusinessGroup::g_Office365Group, BusinessGroup::g_Office365Group },
				{ BusinessGroup::g_SecurityGroup, BusinessGroup::g_SecurityGroup },
#if ALLOW_IMPORT_GROUPTYPE
				{ _T("Specified in source file"), g_ImportGroupType },
#endif
			};
			getGenerator().Add(ComboEditor({ g_ManualGroupType, _T("Group Type"), EditorFlags::DIRECT_EDIT, BusinessGroup::g_Office365Group }, g_GroupTypeLabelsAndValues));

#if ALLOW_IMPORT_GROUPTYPE
			getGenerator().addEvent(g_ManualGroupType, { _YTEXT(O365_GROUP_GROUPTYPE), EventTarget::g_HideIfNotEqual, g_ImportGroupType });
#else
			// Event with fake value so that Group Type mapping editor is always hidden
			getGenerator().addEvent(g_ManualGroupType, { _YTEXT(O365_GROUP_GROUPTYPE), EventTarget::g_HideIfNotEqual, _YTEXT("ThisValueDoesntExist") });
#endif
			getGenerator().addEvent(g_ManualGroupType, { _YTEXT(O365_GROUP_MAILNICKNAME), EventTarget::g_HideIfEqual, BusinessGroup::g_SecurityGroup });
			getGenerator().addEvent(g_ManualGroupType, { _YTEXT(O365_GROUP_VISIBILITY), EventTarget::g_HideIfEqual, BusinessGroup::g_SecurityGroup });
			getGenerator().addEvent(g_ManualGroupType, { _YTEXT(O365_GROUP_VISIBILITY) _YTEXT("_tip"), EventTarget::g_HideIfEqual, BusinessGroup::g_SecurityGroup });

			addMappingEditors();
			addButtons();
		}

		bool processPostedData(const IPostedDataTarget::PostedData& data) override
		{
			auto res = DlgImportMappingHTML::processPostedData(data);
#if ALLOW_IMPORT_GROUPTYPE
			ASSERT(!m_GroupType || m_Mapping.end() == std::find_if(m_Mapping.begin(), m_Mapping.end(), [](const auto& v) { return v.second == _YTEXT(O365_GROUP_GROUPTYPE); }));
#else
			ASSERT(m_GroupType);
#endif
			return res;
		}

		bool processValue(const IPostedDataTarget::PostedData::value_type& p_Value) override
		{
			const auto& propName = p_Value.first;
			const auto& headerName = p_Value.second;

			if (g_ManualGroupType == propName)
			{
#if ALLOW_IMPORT_GROUPTYPE
				if (g_ImportGroupType != headerName)
#endif
				{
					ASSERT(BusinessGroup::g_Office365Group == headerName || BusinessGroup::g_SecurityGroup == headerName);
					m_GroupType = headerName;
				}
				return true;
			}

			return false;
		}

	private:
		boost::YOpt<wstring> m_GroupType;
	};

	class DlgGroupUpdateMappingHTML : public DlgGroupImportMappingHTML
	{
	public:
		DlgGroupUpdateMappingHTML(CWnd* p_Parent)
			: DlgGroupImportMappingHTML(p_Parent)
		{
		}

	protected:
		void generateJSONScriptData() override
		{
			initJson(_YTEXT("DlgGroupUpdateMappingHTML"));

			static ComboEditor::LabelsAndValues g_GroupTypeLabelsAndValues
			{
				{ BusinessGroup::g_Office365Group, BusinessGroup::g_Office365Group },
				{ BusinessGroup::g_SecurityGroup, BusinessGroup::g_SecurityGroup },
#if ALLOW_IMPORT_GROUPTYPE
				{ _T("Specified in source file"), g_ImportGroupType },
#endif
			};
			getGenerator().Add(ComboEditor({ g_ManualGroupType, _T("Group Type"), EditorFlags::DIRECT_EDIT, BusinessGroup::g_Office365Group }, g_GroupTypeLabelsAndValues));

#if ALLOW_IMPORT_GROUPTYPE
			getGenerator().addEvent(g_ManualGroupType, { _YTEXT(O365_GROUP_GROUPTYPE), EventTarget::g_HideIfNotEqual, g_ImportGroupType });
#else
			// Event with fake value so that Group Type mapping editor is always hidden
			getGenerator().addEvent(g_ManualGroupType, { _YTEXT(O365_GROUP_GROUPTYPE), EventTarget::g_HideIfNotEqual, _YTEXT("ThisValueDoesntExist") });
#endif
			getGenerator().addEvent(g_ManualGroupType, { _YTEXT(O365_GROUP_MAILNICKNAME), EventTarget::g_HideIfEqual, BusinessGroup::g_SecurityGroup });
			getGenerator().addEvent(g_ManualGroupType, { _YTEXT(O365_GROUP_VISIBILITY), EventTarget::g_HideIfEqual, BusinessGroup::g_SecurityGroup });
			getGenerator().addEvent(g_ManualGroupType, { _YTEXT(O365_GROUP_VISIBILITY) _YTEXT("_tip"), EventTarget::g_HideIfEqual, BusinessGroup::g_SecurityGroup });

			addKeyEditors();
			addMappingEditors();
			addButtons();
		}
	};
	   
	template<BatchImporter::ObjectType objType, typename InvokeCallback>
	struct DialogSelector {
		using Type = void; using Config = void;
		static const bool isGroup = false;
	};

	template<>
	struct DialogSelector<BatchImporter::User, std::function<wstring(std::map<wstring, wstring>)>> {
		using Type = DlgUserImportMappingHTML; using Config = UserConfig;
		static const bool isGroup = false;
	};

	template<>
	struct DialogSelector<BatchImporter::User, std::function<wstring(std::pair<wstring, wstring>, std::map<wstring, wstring>)>> {
		using Type = DlgUserUpdateMappingHTML; using Config = UserConfig;
		static const bool isGroup = false;
	};

	template<>
	struct DialogSelector<BatchImporter::Group, std::function<wstring(std::map<wstring, wstring>)>> {
		using Type = DlgGroupImportMappingHTML; using Config = GroupConfig;
		static const bool isGroup = true;
	};

	template<>
	struct DialogSelector<BatchImporter::Group, std::function<wstring(std::pair<wstring, wstring>, std::map<wstring, wstring>)>> {
		using Type = DlgGroupUpdateMappingHTML; using Config = GroupConfig;
		static const bool isGroup = true;
	};

	template<typename InvokeCallback>
	boost::YOpt<std::pair<wstring, wstring>> GroupTypePair(std::unique_ptr<typename DialogSelector<BatchImporter::User, InvokeCallback>::Type>& dlg)
	{
		return boost::none;
	}

	template<typename InvokeCallback>
	boost::YOpt<std::pair<wstring, wstring>> GroupTypePair(std::unique_ptr<typename DialogSelector<BatchImporter::Group, InvokeCallback>::Type>& dlg)
	{
		return std::pair<wstring, wstring>{ _YTEXT(O365_GROUP_GROUPTYPE), *dlg->GetGroupType() };
	}

	wstring invokeCallback(
		std::function<wstring(std::map<wstring, wstring>)> cb
		, const std::map<wstring, wstring>& rowValues
		, const std::vector<wstring>& /*row*/
		, const boost::YOpt<std::pair<size_t, wstring>>& /*keyMatching*/)
	{
		return cb(rowValues);
	}

	wstring invokeCallback(
		std::function<wstring(std::pair<wstring, wstring>, std::map<wstring, wstring>)> cb
		, const std::map<wstring, wstring>& rowValues
		, const std::vector<wstring>& row
		, const boost::YOpt<std::pair<size_t, wstring>>& keyMatching)
	{
		ASSERT(keyMatching);
		return cb({ keyMatching->second, row[keyMatching->first] }, rowValues);
	}

	template<BatchImporter::ObjectType objType, typename CallbackType>
	bool ShowDialog(const BatchImporter::KeyProperties& p_KeyProperties, const BatchImporter::MappingProperties& p_MappingProperties, CallbackType p_Callback, CWnd* p_Parent)
	{
		using DialogType = DialogSelector<objType, CallbackType>::Type;
		using ConfigType = DialogSelector<objType, CallbackType>::Config;

		wstring error;
		wstring filePath;
		const auto fileFilter = []()
		{
			CString filter;
			filter.Format(_YTEXT("%s (*.csv;*.xlsx;*.xls)|*.csv;*.xlsx;*.xls|%s (*.csv)|*.csv|%s (*.xlsx;*.xls)|*.xlsx;*.xls|%s (*.*)|*.*||"),
				_T("Compatible Files"),
				YtriaTranslate::Do(YFileDialog_GetCSVFilter_1, _YLOC("CSV Files")).c_str(),
				_T("Excel Workbooks"),
				YtriaTranslate::Do(DlgFiles_OnBtnDbselect_3, _YLOC("All Files")).c_str());
			return filter;
		};

		YFileDialog dlgFile(TRUE, _YTEXT("csv"), NULL, 0, fileFilter(), p_Parent);
		if (IDOK == dlgFile.DoModal())
		{
			filePath = dlgFile.GetPathName();
			if (!filePath.empty())
			{
				auto fileInput = InputFileFactory::CreateInput(filePath);
				if (fileInput)
				{
					if (fileInput->GetLastError().empty())
					{
						if (!fileInput->IsDone())
						{
							DlgImportMappingHTML::Headers headers;
							const auto fileHeaders = fileInput->ReadNextRow();
							for (size_t i = 0; i < fileHeaders.size(); ++i)
								headers[i] = { fileHeaders[i], fileInput->FormatHeader(i, fileHeaders) };

							auto dlg = std::make_unique<DialogType>(p_Parent);
							auto config = std::make_shared<ConfigType>(p_KeyProperties, headers, p_MappingProperties);

							dlg->SetConfig(config);
							const bool isExcel = nullptr != dynamic_cast<ExcelFileInput*>(fileInput.get());
							dlg->SetSkipValue(ExportPreferences::GetExportPreferences(isExcel ? ExportPreferences::REPLACE_UNAVAILABLEVALUE_EXCEL : ExportPreferences::REPLACE_UNAVAILABLEVALUE_CSV).GetValue());

							if (IDOK == dlg->DoModal())
							{
								auto handleRowCheckError = [&headers](const auto& row, const auto& error, auto& lambda) -> wstring
								{
									if (row.size() == 1 && row.back().empty()) // Empty line
										return Str::g_EmptyString;

									if (row.size() != headers.size())
									{
										if (error.empty())
											return wstring(_YERROR("Invalid file."));
										return error;
									}

									return lambda();
								};

								auto handleRow = [handleRowCheckError, &headers, callback = p_Callback, &dlg](const auto& row, const auto& error, const boost::YOpt<std::pair<wstring, wstring>>& p_AdditionalValue = boost::none)
								{
									const auto& mapping = dlg->GetMapping();
									const auto& keyMatching = dlg->GetKeyMatching();
									const auto& skipValue = dlg->GetSkipValue();
									return handleRowCheckError(row, error, [&headers, &mapping, &keyMatching, &skipValue, callback, &row, &p_AdditionalValue]()
										{
											std::map<wstring, wstring> rowValues;
											for (size_t i = 0; i < headers.size(); ++i)
											{
												auto& rowValue = row[i];
												auto range = mapping.equal_range(i);
												if (range.first != range.second)
												{
													for (auto it = range.first; it != range.second; ++it)
													{
														const auto& propName = it->second;
														if (!propName.empty() && (!skipValue || *skipValue != rowValue))
															rowValues[propName] = rowValue;
													}
												}
											}

											if (p_AdditionalValue)
												rowValues.insert(*p_AdditionalValue);

											if (!rowValues.empty())
												return ::invokeCallback(callback, rowValues, row, keyMatching);

											return Str::g_EmptyString;
										});
								};

								error = fileInput->GetLastError();

								auto groupTypePair = GroupTypePair<CallbackType>(dlg);

								if (!dlg->DataHasHeaders())
									error = handleRow(fileHeaders, error, groupTypePair);

								while (error.empty() && !fileInput->IsDone())
								{
									auto& row = fileInput->ReadNextRow();
									error = handleRow(row, error, groupTypePair);
									if (!error.empty())
										break;
								}
							}
							else
							{
								// Canceled.
								return false;
							}
						}
						else
						{
							error = _YERROR("Empty file.");
						}

						if (error.empty())
							error = fileInput->GetLastError();
					}
					else
					{
						error = fileInput->GetLastError();
					}
				}
				else
				{
					error = _YERROR("Unsupported format.");
				}
			}
			else
			{
				error = _YERROR("No file path.");
			}

			if (!error.empty())
			{
				YCodeJockMessageBox(p_Parent, DlgMessageBox::eIcon_Error, _T("Multiple Import error"), error, _YTEXT(""), { {IDOK, _T("OK")} }).DoModal();
			}

			return error.empty();
		}

		return false;
	}
}

bool BatchImporter::DoCreate(ObjectType p_ObjectType, const MappingProperties& p_MappingProperties, std::function<wstring(std::map<wstring, wstring>)> p_Callback, CWnd* p_Parent)
{
	switch (p_ObjectType)
	{
	case User:
		return ::ShowDialog<User, decltype(p_Callback)>({}, p_MappingProperties, p_Callback, p_Parent);
	case Group:
		return ::ShowDialog<Group, decltype(p_Callback)>({}, p_MappingProperties, p_Callback, p_Parent);
	default:
		ASSERT(false);
		break;
	}

	return false;
}

bool BatchImporter::DoUpdate(ObjectType p_ObjectType, const KeyProperties& p_KeyProperties, const MappingProperties& p_MappingProperties, std::function<wstring(std::pair<wstring, wstring>, std::map<wstring, wstring>)> p_Callback, CWnd* p_Parent)
{
	switch (p_ObjectType)
	{
	case User:
		return ::ShowDialog<User, decltype(p_Callback)>(p_KeyProperties, p_MappingProperties, p_Callback, p_Parent);
	case Group:
		return ::ShowDialog<Group, decltype(p_Callback)>(p_KeyProperties, p_MappingProperties, p_Callback, p_Parent);
	default:
		ASSERT(false);
		break;
	}

	return false;
}
