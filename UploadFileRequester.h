#pragma once

#include "BusinessDriveItem.h"
#include "IRequester.h"

class DriveItemDeserializer;

class UploadFileRequester : public IRequester
{
public:
	UploadFileRequester(const wstring& p_FilePath, const wstring& p_DriveId, const wstring& p_ParentFolderId, const wstring& p_OptionalFileName = Str::g_EmptyString);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const HttpResultWithError& GetResult() const;

private:
	const wstring m_FilePath;
	const wstring m_DriveId;
	const wstring m_FolderId;
	const wstring m_OptionalFileName;

	std::shared_ptr<HttpResultWithError> m_Result;
};
