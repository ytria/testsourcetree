#pragma once

#include "BusinessObject.h"

enum UOGFlags : uint32_t
{
	ACTION_MODIFY = 0x1,
	ACTION_CREATE = 0x2,
	ACTION_DELETE = 0x4
};

template<class BusinessType>
class UpdatedObjectsGenerator
{
	// Will generate a compile time error if T is not derived from the correct class.
	static_assert(std::is_base_of<BusinessObject, BusinessType>::value, "T must inherit from BusinessObject");

public:
    using primary_key_t = vector<GridBackendField>;

public:
	UpdatedObjectsGenerator();

	void BuildUpdatedObjects(CacheGrid& p_Grid, bool p_FromSelection, uint32_t p_ActionFlags = UOGFlags::ACTION_MODIFY | UOGFlags::ACTION_CREATE | UOGFlags::ACTION_DELETE);
	void BuildUpdatedObjectsRecycleBin(CacheGrid& p_Grid, bool p_FromSelection);

    const vector<BusinessType>& GetObjects() const;
    void SetObjects(const vector<BusinessType>& p_Objects);
	vector<BusinessType>& GetObjectsNoConst(); // DO NOT USE! Really bad! Only made for/used in FrameUsers::ApplySpecific. Remove it eventually.

    const vector<primary_key_t>& GetPrimaryKeys() const;
	void SetPrimaryKeys(const vector<primary_key_t>& p_PrimaryKeys);

private:
    void BuildObjToModify(const CacheGrid& p_Grid, GridBackendRow* p_pRow);
    void BuildObjToCreate(const CacheGrid& p_Grid, GridBackendRow* p_pRow);
    void BuildObjToDelete(const CacheGrid& p_Grid, GridBackendRow* p_pRow);

private:
    vector<BusinessType> m_ObjectsToUpdate;
    vector<primary_key_t> m_PrimaryKeys;
};

#include "UpdatedObjectsGenerator.hpp"
