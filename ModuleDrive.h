#pragma once

#include "ModuleBase.h"

#include "BusinessChannel.h"
#include "BusinessDriveItem.h"
#include "BusinessDriveItemFolder.h"
#include "BusinessPermission.h"
#include "BusinessSite.h"
#include "CommandInfo.h"
#include "DriveItemDownloadInfo.h"
#include "YTaskPool.h"

#include <cpprest/http_client.h>
#include <cpprest/streams.h>

class AutomationAction;
class FrameDriveItems;
class GridDriveItems;

class ModuleDrive : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

private:
    virtual void executeImpl(const Command& p_Command) override;
    void applyChanges(const Command& p_Command);
	void showDriveItems(Command p_Command);
    void viewPermissions(const Command& p_Command);
    void downloadDriveItems(const Command& p_Command);
	void downloadFailed(const DriveItemDownloadInfo& p_DownloadInfo, GridDriveItems& p_Grid, GridBackendRow& p_Row, const SapioError& p_Error);
	void loadCheckoutStatus(const Command& p_Command);
	void loadSnapshot(const Command& p_Command);

    
	// Blocking call (use p_ChunkDownloadedCallback for progress)
    boost::YOpt<wstring> downloadStreamingFile( web::http::http_response p_Response, const DriveItemDownloadInfo& p_DownloadInfo, YtriaTaskData p_TaskData, std::function<void(int32_t p_ProgressPercent)> p_ChunkDownloadedCallback);
    void SetCreatedAndModifiedTime(const DriveItemDownloadInfo &p_DownloadInfo, const wstring & p_FinalFullPath);

	template<class BusinessType>
	static std::shared_ptr<O365DataMap<BusinessType, BusinessDriveItemFolder>>		retrieveDriveItems(std::shared_ptr<BusinessObjectManager> p_BOM, const O365IdsContainer& p_IDs, const boost::YOpt<wstring>& p_SearchCriteria, Origin p_Origin, bool p_LoadFromSQlCache, YtriaTaskData p_TaskData);

	template<>
	static std::shared_ptr<O365DataMap<BusinessUser, BusinessDriveItemFolder>>		retrieveDriveItems<BusinessUser>(std::shared_ptr<BusinessObjectManager> p_BOM, const O365IdsContainer& p_IDs, const boost::YOpt<wstring>& p_SearchCriteria, Origin p_Origin, bool p_LoadFromSQlCache, YtriaTaskData p_TaskData);
	template<>
	static std::shared_ptr<O365DataMap<BusinessGroup, BusinessDriveItemFolder>>		retrieveDriveItems<BusinessGroup>(std::shared_ptr<BusinessObjectManager> p_BOM, const O365IdsContainer& p_IDs, const boost::YOpt<wstring>& p_SearchCriteria, Origin p_Origin, bool p_LoadFromSQlCache, YtriaTaskData p_TaskData);
	template<>
	static std::shared_ptr<O365DataMap<BusinessSite, BusinessDriveItemFolder>>		retrieveDriveItems<BusinessSite>(std::shared_ptr<BusinessObjectManager> p_BOM, const O365IdsContainer& p_IDs, const boost::YOpt<wstring>& p_SearchCriteria, Origin p_Origin, bool p_LoadFromSQlCache, YtriaTaskData p_TaskData);

	static std::shared_ptr<O365DataMap<BusinessChannel, BusinessDriveItemFolder>>	retrieveDriveItems(std::shared_ptr<BusinessObjectManager> p_BOM, const O365SubIdsContainer& p_SubIDs, Origin p_Origin, YtriaTaskData p_TaskData);

	template<class BusinessType>
	static void showDriveItemsIds(const O365IdsContainer& p_Ids, const boost::YOpt<wstring>& p_SearchCriteria, const DriveSubInfoIds& p_PermissionRequestInfo, const DriveSubInfoIds& p_CheckoutStatusRequestInfo, const bool p_PartialRefresh, const Origin p_Origin, const std::shared_ptr<BusinessObjectManager>& p_BOM, HWND p_Hwnd, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsRefresh, bool p_LoadFromSQlCache);

	static void showDriveItemsSubIds(const O365SubIdsContainer& p_SubIds, const DriveSubInfoIds& p_PermissionRequestInfo, const DriveSubInfoIds& p_CheckoutStatusRequestInfo, const bool p_PartialRefresh, const Origin p_Origin, const std::shared_ptr<BusinessObjectManager>& p_BOM, HWND p_Hwnd, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsRefresh, const std::map<PooledString, PooledString>& p_ChannelNames);

	template<class BusinessType>
	static void showDriveItemsCommonImpl(const std::shared_ptr<O365DataMap<BusinessType, BusinessDriveItemFolder>>& p_DriveItems, const DriveSubInfoIds& p_PermissionRequestInfo, const DriveSubInfoIds& p_CheckoutStatusRequestInfo, const bool p_PartialRefresh, const Origin p_Origin, const std::shared_ptr<BusinessObjectManager>& p_BOM, HWND p_Hwnd, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsRefresh, const std::map<PooledString, PooledString>& p_ChannelNames);


	using AllPermissions = map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>;
	static AllPermissions retrievePermissions(std::shared_ptr<BusinessObjectManager> p_BOM, const DriveSubInfoIds& p_RequestInfo, YtriaTaskData p_TaskData);

	using CheckoutStatuses = std::map<std::pair<PooledString, PooledString>, CheckoutStatus>;
    static CheckoutStatuses retrieveCheckoutStatus(std::shared_ptr<BusinessObjectManager> p_BOM, const DriveSubInfoIds& p_RequestInfo, YtriaTaskData p_TaskData);
};
