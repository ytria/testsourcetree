#pragma once

#include "IRequester.h"

class DumpDeserializer;
class SingleRequestResult;

namespace Cosmos
{
    class CreateCollectionRequester : public IRequester
    {
    public:
        CreateCollectionRequester(const wstring& p_DatabaseName, const wstring& p_CollectionName);
        TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

        const wstring& GetData() const;
        const SingleRequestResult& GetResult() const;


    private:
        std::shared_ptr<DumpDeserializer> m_Deserializer;
        std::shared_ptr<SingleRequestResult> m_Result;

        wstring m_DbName;
        wstring m_CollectionName;
    };
}
