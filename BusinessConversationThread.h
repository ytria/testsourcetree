#pragma once

#include "BusinessObject.h"
#include "ConversationThread.h"

class BusinessConversationThread : public BusinessObject
{
public:
    BusinessConversationThread();
    BusinessConversationThread(const ConversationThread& p_Thread);
    
    const vector<Recipient>& GetToRecipients() const;
    void SetToRecipients(const vector<Recipient>& p_Val);

    const vector<Recipient>& GetCcRecipients() const;
    void SetCcRecipients(const vector<Recipient>& p_Val);

    const boost::YOpt<PooledString>& GetTopic() const;
    void SetTopic(const boost::YOpt<PooledString>& p_Val);
    
    const boost::YOpt<bool>& GetHasAttachments() const;
    void SetHasAttachments(const boost::YOpt<bool>& p_Val);
    
    const boost::YOpt<YTimeDate>& GetLastDeliveredDateTime() const;
    void SetLastDeliveredDateTime(const boost::YOpt<YTimeDate>& p_Val);
    
    const boost::YOpt<vector<PooledString>>& GetUniqueSenders() const;
    void SetUniqueSenders(const boost::YOpt<vector<PooledString>>& p_Val);
    
    const boost::YOpt<PooledString>& GetPreview() const;
    void SetPreview(const boost::YOpt<PooledString>& p_Val);
    
    const boost::YOpt<bool>& GetIsLocked() const;
    void SetIsLocked(const boost::YOpt<bool>& p_Val);

private:
    vector<Recipient> m_ToRecipients;
    vector<Recipient> m_CcRecipients;
    boost::YOpt<PooledString> m_Topic;
    boost::YOpt<bool> m_HasAttachments;
    boost::YOpt<YTimeDate> m_LastDeliveredDateTime;
    boost::YOpt<vector<PooledString>> m_UniqueSenders;
    boost::YOpt<PooledString> m_Preview;
    boost::YOpt<bool> m_IsLocked;
    
    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND
    friend class ConversationThreadDeserializer;
};

