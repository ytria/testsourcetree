#pragma once

#include "IRequester.h"
#include "DelegateUser.h"

class SingleRequestResult;

namespace Ex
{

class GetDelegatesResponseDeserializer;
class SoapDocument;

class DelegatesRequester : public IRequester
{
public:
    DelegatesRequester(PooledString p_UserMail);

    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    void CreateXml(SoapDocument &soapDoc);

    vector<DelegateUser> GetResult() const;

private:
    std::shared_ptr<GetDelegatesResponseDeserializer> m_Deserializer;
    std::shared_ptr<SingleRequestResult> m_Result;

    PooledString m_UserMail;
};
}
