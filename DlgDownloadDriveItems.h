#pragma once

#include "ResizableDialog.h"
#include "Resource.h"

#include "FileExistsAction.h"

class DlgDownloadDriveItems : public ResizableDialog
{
public:
    enum { IDD = IDD_DLG_DOWNLOAD_DRIVEITEMS };
    DlgDownloadDriveItems(CWnd* p_Parent);
    
    const wstring&		GetFolderPath() const;
    bool				GetPreserveHierarchy() const;
    bool				GetUseOwnerName() const;
    FileExistsAction	GetFileExistsAction() const;
    bool                GetOpenAfterDownload() const;

	static const wstring g_AutomationName;

protected:
    virtual void DoDataExchange(CDataExchange* pDX) override;
    virtual BOOL OnInitDialogSpecific();
    virtual void OnOK() override;

	void			setAutomationFieldMap() override;
	void			setAutomationRadioButtonGroups() override;
	void			setAutomationListChoicesGroups() override;
	virtual void	automationSetParamPostProcess() override;

    afx_msg void OnSelectFolder();
    afx_msg void OnPreserveHierarchyChanged();
    afx_msg void OnUseOwnerNameChanged();
    afx_msg void OnOpenAfterDownloadChanged();
    afx_msg void OnSelectAppend();
    afx_msg void OnSelectSkip();
    afx_msg void OnSelectOverwrite();
    DECLARE_MESSAGE_MAP()

private:
    CExtEdit		m_TxtFolderPath;
    CXTPButton		m_BtnChooseFolder;
    CExtCheckBox	m_ChkPreserveHierarchy;
    CExtCheckBox	m_CheckUseOwnerName;
    CExtCheckBox	m_ChkOpenAfterDownload;

    CExtGroupBox	m_GroupBox;
    CExtRadioButton m_RadioBtnCreateNew;
    CExtRadioButton m_RadioBtnSkip;
    CExtRadioButton m_RadioBtnOverwrite;
    CExtLabel       m_ButtonLine;

    CXTPButton m_BtnOk;
    CXTPButton m_BtnCancel;

    wstring				m_FolderPath;
    bool				m_PreserveHierarchy;
    bool                m_UseOwnerName;
    bool                m_OpenAfterDownload;
    FileExistsAction	m_FileExistsAction;

    static wstring			g_LastFolderPath;
    static bool				g_PreserveHierarchy;
    static bool				g_UserOwnerName;
    static bool				g_OpenAfterDownload;
	static FileExistsAction g_FileExistsAction;
};

