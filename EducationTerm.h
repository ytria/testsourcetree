#pragma once

class EducationTerm
{
public:
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<PooledString> m_ExternalId;
	boost::YOpt<YTimeDate> m_StartDate;
	boost::YOpt<YTimeDate> m_EndDate;
};

