#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Quota.h"

class QuotaDeserializer : public JsonObjectDeserializer, public Encapsulate<Quota>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

