#pragma once

#include "IRequester.h"

#include "BusinessMailFolder.h"
#include "MailFolderDeserializer.h"
#include "MessageListRequester.h"

class IRequestLogger;

// Get all messages in all mailfolders for the specified user id
class MailFolderHierarchyRequester : public IRequester
{
public:
	using DeserializerType = ValueListDeserializer<BusinessMessage, MessageDeserializer>;

	MailFolderHierarchyRequester(const wstring& p_UserId, const wstring& p_FolderName, const std::shared_ptr<IRequestLogger>& p_TopMailFolderLogger, const std::shared_ptr<IPageRequestLogger>& p_TopMailFolderMessagesLogger, const std::shared_ptr<IPageRequestLogger>& p_SubMailFoldersLogger);

	void SetMessagesCutOffDateTime(const wstring& p_CutOffDateTime);
	void SetMessagesCustomFilter(const ODataFilter& p_CustomFilter);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	BusinessMailFolder& GetData();

private:
	std::shared_ptr<BusinessMailFolder> m_MailFolder;

	std::shared_ptr<IRequestLogger> m_TopMailFolderLogger;
	std::shared_ptr<IPageRequestLogger> m_TopMailFolderMessagesLogger;
	std::shared_ptr<IPageRequestLogger> m_SubMailFoldersLogger;

	wstring m_UserId;
	wstring m_FolderName;

	wstring m_CutOffDateTime;
	ODataFilter m_CustomFilter;
};

