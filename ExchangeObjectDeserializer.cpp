#include "ExchangeObjectDeserializer.h"

#include "TodoXmlErrorReporter.h"

#include "xercesc\framework\MemBufInputSource.hpp"
using XERCES_CPP_NAMESPACE::MemBufInputSource;
#include "xercesc\parsers\XercesDOMParser.hpp"
using XERCES_CPP_NAMESPACE::XercesDOMParser;
#include "xercesc\dom\DOMNode.hpp"
using XERCES_CPP_NAMESPACE::DOMNode;
#include "xercesc\dom\DOMNodeList.hpp"

Ex::ExchangeObjectDeserializer::ExchangeObjectDeserializer()
    :m_HasGlobalError(false)
{
}

bool Ex::ExchangeObjectDeserializer::HasGlobalError() const
{
    return m_HasGlobalError;
}

const wstring& Ex::ExchangeObjectDeserializer::GetGlobalErrorMessage() const
{
    return m_GlobalErrorMessage;
}

const wstring& Ex::ExchangeObjectDeserializer::GetGlobalErrorResponseCode() const
{
    return m_GlobalErrorResponseCode;
}

void Ex::ExchangeObjectDeserializer::DeserializeImpl(const vector<uint8_t>& p_XmlBytes)
{
    XercesDOMParser parser;
    parser.setDoSchema(false);
    parser.setValidationScheme(XercesDOMParser::Val_Never);
    parser.setDoNamespaces(true);
    parser.setLoadExternalDTD(false);

    TodoXmlErrorReporter errorHandler;
    parser.setErrorHandler(&errorHandler);

    MemBufInputSource inputSource(p_XmlBytes.data(), p_XmlBytes.size(), "ExchangeObjectDeserializer");
    parser.parse(inputSource);
    
    auto document = parser.getDocument();
    ASSERT(nullptr != document);
    if (nullptr != document)
    {
        // Root element is always the soap envelope.
        auto soapEnvelope = document->getDocumentElement();
        ASSERT(nullptr != soapEnvelope);
        if (nullptr != soapEnvelope)
            DeserializeObject(*soapEnvelope);
    }
}

DOMElement* Ex::ExchangeObjectDeserializer::GetSoapBody(const DOMElement& p_Envelope) const
{
    auto body = p_Envelope.getLastElementChild();
    if (nullptr != body)
    {
        auto localName = body->getLocalName();
        if (wcscmp(localName, _YTEXT("Body")) == 0)
            return body;
    }

    ASSERT(nullptr != body);
    return nullptr;
}

DOMElement* Ex::ExchangeObjectDeserializer::GetElementByTagName(const DOMElement& p_ParentElem, const wstring& p_TagName) const
{
    auto elements = p_ParentElem.getElementsByTagName(p_TagName.c_str());
    ASSERT(nullptr != elements);
    if (nullptr != elements)
    {
        if (elements->getLength() == 1)
        {
            auto node = elements->item(0);
            ASSERT(nullptr != node);
            if (nullptr != node)
            {
                ASSERT(node->getNodeType() == DOMNode::ELEMENT_NODE);
                if (node->getNodeType() == DOMNode::ELEMENT_NODE)
                    return dynamic_cast<DOMElement*>(node);
            }
        }
    }
    return nullptr;
}

bool Ex::ExchangeObjectDeserializer::ElementHasError(const DOMElement& p_ParentElem) const
{
    return wcscmp(p_ParentElem.getAttribute(_YTEXT("ResponseClass")), _YTEXT("Success")) != 0;
}

std::pair<wstring, wstring> Ex::ExchangeObjectDeserializer::GetError(const DOMElement& p_ParentElem) const
{
    std::pair<wstring, wstring> errorCodeAndMessage;
    auto children = p_ParentElem.getChildNodes();
    ASSERT(nullptr != children);
    if (nullptr != children)
    {
        size_t childrenCount = children->getLength();

        bool foundErrorMessage = false;
        bool foundErrorResponseCode = false;

        for (size_t i = 0; i < childrenCount && !(foundErrorMessage && foundErrorResponseCode); ++i)
        {
            const auto& item = children->item(i);
            if (item->getNodeType() == XERCES_CPP_NAMESPACE::DOMNode::ELEMENT_NODE)
            {
                auto childElement = dynamic_cast<DOMElement*>(children->item(i));
                ASSERT(nullptr != childElement);
                if (nullptr != childElement)
                {
                    auto childName = childElement->getLocalName();
                    if (wcscmp(childName, _YTEXT("ResponseCode")) == 0)
                    {
                        errorCodeAndMessage.first = childElement->getTextContent();
                        foundErrorResponseCode = true;
                    }
                    else if (wcscmp(childName, _YTEXT("MessageText")) == 0)
                    {
                        errorCodeAndMessage.second = childElement->getTextContent();
                        foundErrorMessage = true;
                    }
                }
            }
        }
    }

    return errorCodeAndMessage;
}

boost::YOpt<bool> Ex::ExchangeObjectDeserializer::DeserializeBool(const DOMElement& p_ParentElem, const wstring& p_TagName) const
{
    boost::YOpt<bool> val = false;

    auto element = GetElementByTagName(p_ParentElem, p_TagName.c_str());
    if (nullptr != element)
        val = wcscmp(element->getTextContent(), _YTEXT("true")) == 0;

    return val;
}

boost::YOpt<wstring> Ex::ExchangeObjectDeserializer::DeserializeString(const DOMElement& p_ParentElem, const wstring& p_TagName) const
{
    boost::YOpt<wstring> str;

    auto element = GetElementByTagName(p_ParentElem, p_TagName.c_str());
    if (nullptr != element)
        str = element->getTextContent();

    return str;
}
