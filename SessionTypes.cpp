#include "SessionTypes.h"

#include "MSGraphCommonData.h"
#include "OAuth2AuthenticatorBase.h"
#include "CRMpipe.h"

const wstring SessionTypes::g_StandardSession{ _YTEXT("standard") };
const wstring SessionTypes::g_AdvancedSession{ _YTEXT("advanced") };
const wstring SessionTypes::g_UltraAdmin{ _YTEXT("ultraadmin") };
const wstring SessionTypes::g_Role{ _YTEXT("role") };
const wstring SessionTypes::g_ElevatedSession{ _YTEXT("elevated") };
const wstring SessionTypes::g_PartnerAdvancedSession{ _YTEXT("partneradvanced") };
const wstring SessionTypes::g_PartnerElevatedSession{ _YTEXT("partnerelevated") };

const wchar_t SessionTypes::g_StandardUserSuffix{ L'@' };

wstring SessionTypes::GetAppId(const wstring& p_SessionType)
{
    ASSERT(	p_SessionType == g_StandardSession
		||	p_SessionType == g_AdvancedSession
		||	p_SessionType == g_UltraAdmin
		||	p_SessionType == g_Role
		||	p_SessionType == g_ElevatedSession
		||	p_SessionType == g_PartnerAdvancedSession
		||	p_SessionType == g_PartnerElevatedSession);

	if (	p_SessionType == g_StandardSession
		||	p_SessionType == g_Role)
		return GetStandardSessionAppID();
	
	if (	p_SessionType == g_AdvancedSession
		||	p_SessionType == g_ElevatedSession
		||	p_SessionType == g_PartnerAdvancedSession
		||	p_SessionType == g_PartnerElevatedSession)
		return GetAdvancedSessionAppID();
	
	if (	p_SessionType == g_UltraAdmin)
		return _YTEXT("");
	
	ASSERT(false);
    return _YTEXT("");
}

wstring SessionTypes::GetDisplayedSessionType(const wstring& p_SessionType)
{
	wstring shownSessionType;

	if (p_SessionType == SessionTypes::g_StandardSession)
		shownSessionType = YtriaTranslate::Do(O365Session_GetDisplayedSessionType_1, _YLOC("Standard Session")).c_str();
	else if (p_SessionType == SessionTypes::g_AdvancedSession)
		shownSessionType = YtriaTranslate::Do(O365Session_GetDisplayedSessionType_2, _YLOC("Advanced Session")).c_str();
	else if (p_SessionType == SessionTypes::g_UltraAdmin)
		shownSessionType = YtriaTranslate::Do(O365Session_GetDisplayedSessionType_3, _YLOC("Ultra Admin Session")).c_str();
	else if (p_SessionType == SessionTypes::g_Role)
		shownSessionType = YtriaTranslate::Do(GridFrameBase_updateConnectedControl_3, _YLOC("Role-Based Session")).c_str();
	else if (p_SessionType == SessionTypes::g_ElevatedSession)
		shownSessionType = _T("Advanced Session - Elevated");
	else if (p_SessionType == SessionTypes::g_PartnerAdvancedSession)
		shownSessionType = _T("Partner: Advanced Session");
	else if (p_SessionType == SessionTypes::g_PartnerElevatedSession)
		shownSessionType = _T("Partner: Advanced Session - Elevated");

	ASSERT(!shownSessionType.empty());
	return shownSessionType;
}

web::uri_builder SessionTypes::GetStandardSessionConsentUrl()
{
    web::uri_builder browserUri(Rest::GetAzureADEndpoint());
    browserUri.append_path(_YTEXT("common"));
    browserUri.append_path(_YTEXT("oauth2"));
    browserUri.append_path(_YTEXT("authorize"));
    browserUri.append_query(_YTEXT("response_type"), _YTEXT("code"));
    browserUri.append_query(_YTEXT("prompt"), _YTEXT("consent"));
    browserUri.append_query(_YTEXT("client_id"), GetAppId(g_StandardSession));
    browserUri.append_query(_YTEXT("redirect_uri"), GetBasicUserRedirectUri());

    return browserUri;
}

web::uri_builder SessionTypes::GetAdvancedSessionConsentUrl(bool p_AdminConsent, bool p_PartnerAlternativeRedirectURI/* = false*/)
{
    web::uri_builder browserUri(Rest::GetAzureADEndpoint());
    browserUri.append_path(_YTEXT("common"));
    browserUri.append_path(_YTEXT("oauth2"));
    browserUri.append_path(_YTEXT("authorize"));
    browserUri.append_query(_YTEXT("response_type"), _YTEXT("code"));
    if (p_AdminConsent)
        browserUri.append_query(_YTEXT("prompt"), _YTEXT("admin_consent"));
    else
        browserUri.append_query(_YTEXT("prompt"), _YTEXT("consent"));
    browserUri.append_query(_YTEXT("client_id"), GetAppId(g_AdvancedSession));
	if (p_PartnerAlternativeRedirectURI)
		browserUri.append_query(_YTEXT("redirect_uri"), GetPartnerAlternativeAdvancedUserRedirectUri());
	else
		browserUri.append_query(_YTEXT("redirect_uri"), GetAdvancedUserRedirectUri());

    return browserUri;
}

web::uri_builder SessionTypes::GetUltraAdminConsentUrl(const wstring& p_TenantName, const wstring& p_ApplicationId, const wstring& p_RedirectURL)
{
    web::uri_builder browserUri(Rest::GetAzureADEndpoint());
    browserUri.append_path(p_TenantName);
    browserUri.append_path(_YTEXT("adminconsent"));
    browserUri.append_query(_YTEXT("redirect_uri"), p_RedirectURL);
    browserUri.append_query(_YTEXT("client_id"), p_ApplicationId);

    return browserUri;
}

wstring SessionTypes::GetUserRedirectUri(const wstring& p_SessionType)
{
	ASSERT(	p_SessionType == g_StandardSession
		||	p_SessionType == g_AdvancedSession
		||	p_SessionType == g_Role
		||	p_SessionType == g_ElevatedSession
		||	p_SessionType == g_PartnerAdvancedSession
		||	p_SessionType == g_PartnerElevatedSession);

	if (	p_SessionType == g_StandardSession
		||	p_SessionType == g_Role)
		return GetBasicUserRedirectUri();

	if (	p_SessionType == g_AdvancedSession
		||	p_SessionType == g_ElevatedSession
		||	p_SessionType == g_PartnerAdvancedSession
		||	p_SessionType == g_PartnerElevatedSession)
		return GetAdvancedUserRedirectUri();

	return _YTEXT("");
}

wstring SessionTypes::GetBasicUserRedirectUri()
{
	static wstring uri;
	if (uri.empty())
	{
		const wstring path = Registry::GetBaseUserSettingsPath(_YTEXT(""), false) + _YTEXT("\\") + _YTEXT("MSGraphApi") + _YTEXT("\\");
		bool read = Registry::readString(HKEY_CURRENT_USER, path, _YTEXT("RedirectURL_User"), uri);
		if (read && !uri.empty())
		{
			if (Str::endsWith(uri, _YTEXT("/")))
				uri = uri.substr(0, uri.size() - 1);
			LoggerService::Debug(_YTEXTFORMAT(L"User Redirect URL - OVERRIDE: %s", uri.c_str()));
		}
		else
		{
			uri = _YTEXT("http://localhost:33366");
			LoggerService::Debug(_YTEXTFORMAT(L"User Redirect URL - regular: %s", uri.c_str()));
		}

		uri += _YTEXT("/");
	}
	else
		LoggerService::Debug(_YTEXTFORMAT(L"User Redirect URL - cached: %s", uri.c_str()));

	return uri;
}

wstring SessionTypes::GetAdvancedUserRedirectUri()
{
	static wstring uri;
	if (uri.empty())
	{
		const wstring path = Registry::GetBaseUserSettingsPath(_YTEXT(""), false) + _YTEXT("\\") + _YTEXT("MSGraphApi") + _YTEXT("\\");
		bool read = Registry::readString(HKEY_CURRENT_USER, path, _YTEXT("RedirectURL_Admin"), uri);
		if (read && !uri.empty())
		{
			if (Str::endsWith(uri, _YTEXT("/")))
				uri = uri.substr(0, uri.size() - 1);
			LoggerService::Debug(_YTEXTFORMAT(L"Admin Redirect URL - OVERRIDE: %s", uri.c_str()));
		}
		else
		{
			uri = _YTEXT("http://localhost:33366");
			LoggerService::Debug(_YTEXTFORMAT(L"Admin Redirect URL - regular: %s", uri.c_str()));
		}

		uri += _YTEXT("/");
	}
	else
		LoggerService::Debug(_YTEXTFORMAT(L"Admin Redirect URL - cached: %s", uri.c_str()));

	return uri;
}

wstring SessionTypes::GetPartnerAlternativeAdvancedUserRedirectUri()
{
	return _YTEXT("https://www.ytria.com/consent-partner-successful");
}

wstring SessionTypes::GetDefaultUltraAdminConsentRedirectUri()
{
	return _YTEXT("http://localhost:33366");
}

wstring SessionTypes::GetPartnerAlternativeUltraAdminConsentRedirectUri()
{
	return _YTEXT("https://www.ytria.com/consent-partnerelevated-successful");
}

wstring SessionTypes::GetStandardSessionAppID()
{
	static wstring appID;
	if (appID.empty())
	{
		const wstring path = Registry::GetBaseUserSettingsPath(_YTEXT(""), false) + _YTEXT("\\") + _YTEXT("MSGraphApi") + _YTEXT("\\");
		const auto read = Registry::readString(HKEY_CURRENT_USER, path, _YTEXT("AppId_User"), appID);
		if (read && !appID.empty())
		{
			LoggerService::Debug(_YTEXTFORMAT(L"Standard Application ID - OVERRIDE: \"%s\"", appID.c_str()));
		}
		else
		{
			// Application:  Ytria sapio365 - Regular Access (1.2)
			appID = _YTEXT("5783d7d8-c1b8-4c4f-89cc-6e0b38060fb0");
			LoggerService::Debug(_YTEXTFORMAT(L"Standard Application ID - regular - 1.2: \"%s\"", appID.c_str()));
		}
	}
	else
		LoggerService::Debug(_YTEXTFORMAT(L"Standard Application ID - cached: \"%s\"", appID.c_str()));

	return appID;
}

wstring SessionTypes::GetAdvancedSessionAppID()
{
	static wstring appID;
	if (appID.empty())
	{
		const wstring path = Registry::GetBaseUserSettingsPath(_YTEXT(""), false) + _YTEXT("\\") + _YTEXT("MSGraphApi") + _YTEXT("\\");
		const auto read = Registry::readString(HKEY_CURRENT_USER, path, _YTEXT("AppId_Admin"), appID);
		if (read && !appID.empty())
		{
			LoggerService::Debug(_YTEXTFORMAT(L"Advanced Application ID - OVERRIDE: \"%s\"", appID.c_str()));
		}
		else
		{
            if (CRMpipe().IsFeatureSandboxed())
			{
				// Application: Ytria sapio365 - with Admin Consent (2.0) -- with SharePoint and EWS API
				appID = _YTEXT("e86f0ccb-a292-492e-b3fa-7b8dc48a1db2");
				LoggerService::Debug(_YTEXTFORMAT(L"Advanced Application ID [2.0!] - regular: \"%s\"", appID.c_str()));
			}
            else
			{
				// Application: Ytria sapio365 - with Admin Consent (1.4)
				appID = _YTEXT("4f71340e-31de-467c-8b02-1e39c294c66f");
				// Application: Ytria sapio365 - with Admin Consent (1.3)
				// appID = _YTEXT("370a1795-cf97-4f7c-b903-e912445971d1");
				LoggerService::Debug(_YTEXTFORMAT(L"Advanced Application ID - regular - 1.4: \"%s\"", appID.c_str()));
			}
		}
	}
	else
		LoggerService::Debug(_YTEXTFORMAT(L"Admin Application ID - cached: \"%s\"", appID.c_str()));

	return appID;
}

const set<wstring, Str::keyLessInsensitive>& SessionTypes::GetTypes()
{
	static set<wstring, Str::keyLessInsensitive> types
	{	g_StandardSession
	,	g_AdvancedSession
	,	g_UltraAdmin
	,	g_Role
	,	g_ElevatedSession 
	,	g_PartnerAdvancedSession
	,	g_PartnerElevatedSession };

	return types;
}

bool SessionTypes::IsValidType(const wstring p_Type)
{
	auto types = GetTypes();
	return types.find(p_Type) != types.end();
}
