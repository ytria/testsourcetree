#pragma once

#include "IModificationReverter.h"

class GridMailboxPermissions;

class MailboxPermissionsModificationsReverter : public IModificationReverter
{
public:
	MailboxPermissionsModificationsReverter(GridMailboxPermissions& p_Grid);

	void RevertAll() override;
	void RevertSelected() override;

	void SetConfirm(bool p_Confirm);

private:
	template <class ContainerType, class IttType>
	IttType Revert(ContainerType& p_Container, IttType p_Itt)
	{
		const auto& pk = p_Itt->first;
		auto& mod = p_Itt->second;
		auto row = m_Grid.GetRowIndex().GetExistingRow(pk);
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			if (row->IsSelected())
			{
				mod.Revert();
				p_Itt = p_Container.erase(p_Itt);
			}
			else
			{
				++p_Itt;
			}
		}
		return p_Itt;
	}

	GridMailboxPermissions& m_Grid;
	bool m_Confirm;
};

