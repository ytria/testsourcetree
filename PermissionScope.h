#pragma once

class PermissionScope
{
public:
	boost::YOpt<PooledString> m_AdminConsentDescription;
	boost::YOpt<PooledString> m_AdminConsentDisplayName;
	boost::YOpt<PooledString> m_Id;
	boost::YOpt<bool> m_IsEnabled;
	boost::YOpt<PooledString> m_Origin;
	boost::YOpt<PooledString> m_Type;
	boost::YOpt<PooledString> m_UserConsentDescription;
	boost::YOpt<PooledString> m_UserConsentDisplayName;
	boost::YOpt<PooledString> m_Value;
};

