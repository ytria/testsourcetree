#include "DriveItemsRequester.h"

#include "DriveItemDeserializer.h"
#include "DriveItemChildrenRequester.h"
#include "MSGraphCommonData.h"
#include "MsGraphHttpRequestLogger.h"
#include "MSGraphSession.h"
#include "MSGraphUtil.h"
#include "PaginatorUtil.h"
#include "RESTUtils.h"
#include "RunOnScopeEnd.h"
#include "Sapio365Session.h"
#include "ValueListDeserializer.h"
#include "YSafeCreateTask.h"

using namespace Rest;

vector<PooledString> DriveItemsRequester::GetSelectProperties()
{
	return
	{
		//_YTEXT("audio"),
		//_YTEXT("content"),
		_YTEXT("createdBy"),
		_YTEXT("createdDateTime"),
		_YTEXT("cTag"),
		_YTEXT("deleted"),
		_YTEXT("description"),
		_YTEXT("eTag"),
		_YTEXT("file"),
		_YTEXT("fileSystemInfo"),
		_YTEXT("folder"),
		_YTEXT("id"),
		//_YTEXT("image"),
		_YTEXT("lastModifiedBy"),
		_YTEXT("lastModifiedDateTime"),
		//_YTEXT("location"),
		_YTEXT("name"),
		_YTEXT("package"),
		_YTEXT("parentReference"),
		//_YTEXT("photo"),
		_YTEXT("publication"),
		//_YTEXT("remoteItem"),
		//_YTEXT("searchResult"),
		_YTEXT("shared"),
		_YTEXT("sharepointIds"),
		_YTEXT("size"),
		//_YTEXT("specialFolder"),
		//_YTEXT("video"),
		_YTEXT("webDavUrl"),
		_YTEXT("webUrl"),
	};
}

DriveItemsRequester::DriveItemsRequester(PooledString p_DriveId, bool p_SyncChildren, bool p_QueryCheckoutStatuses, const std::shared_ptr<IPageRequestLogger>& p_Logger)
    : m_DriveId(p_DriveId)
	, m_SyncChildren(p_SyncChildren)
	, m_Logger(p_Logger)
	, m_QueryCheckoutStatuses(p_QueryCheckoutStatuses)
{
}

TaskWrapper<void> DriveItemsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<BusinessDriveItem, DriveItemDeserializer>>();
    return YSafeCreateTask([logger = m_Logger, driveId = m_DriveId, deserializer = m_Deserializer, syncChildren = m_SyncChildren, queryCheckoutStatus = m_QueryCheckoutStatuses, searchCriteria = m_SearchCriteria, p_Session, p_TaskData] {
        web::uri_builder uri;
        uri.append_path(DRIVES_PATH);
        uri.append_path(driveId);
        uri.append_path(_YTEXT("root"));
		if (searchCriteria)
			uri.append_path(_YTEXTFORMAT(L"search(q='{%s}')", searchCriteria->c_str()), true);
		else
			uri.append_path(CHILDREN_PATH);
		uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(GetSelectProperties()));
		if (queryCheckoutStatus)
		{
			ASSERT(false); // Do not use. Only available using beta API. We won't risk it for now.
			uri.append_query(_YTEXT("$expand"), _YTEXT("listitem(expand%3Dfields(select=CheckoutUser,_CheckinComment,_ComplianceTag,_ComplianceTagWrittenTime))"), false);
		}

		RunOnScopeEnd _([objName = logger->GetLogMessage(), logger]() { logger->SetLogMessage(objName); });
		logger->SetLogMessage(_T("drive items"));

		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
		auto paginator = Util::CreateDefaultGraphPaginator(uri.to_uri(), deserializer, logger, httpLogger, p_TaskData.GetOriginator());

		if (queryCheckoutStatus)
			paginator.GetPageRequester().SetUseBetaEndpoint(true);

		return paginator.Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData).Then([logger, session = p_Session, deserializer, syncChildren, driveId, p_TaskData]() {
			if (syncChildren)
			{
				auto& driveItems = deserializer->GetData();
				try
				{
					for (auto& driveItem : driveItems)
					{
						DriveItemChildrenRequester childrenRequester(driveId, driveItem, true, logger);
						if (childrenRequester.Send(session, p_TaskData).GetTask().wait() == pplx::task_status::canceled)
							pplx::cancel_current_task();

						driveItem.SetDriveItemChildren(std::move(childrenRequester.GetData()));
					}
				}
				catch (pplx::task_canceled&)
				{
				}
			}
		}).GetTask();
    });
}

vector<BusinessDriveItem>& DriveItemsRequester::GetData()
{
    return m_Deserializer->GetData();
}

void DriveItemsRequester::SetSearchCriteria(const boost::YOpt<wstring>& p_SearchCriteria)
{
	ASSERT(!m_SyncChildren || !p_SearchCriteria);
	m_SearchCriteria = p_SearchCriteria;
}
