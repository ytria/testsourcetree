#include "BusinessConfigurationBase.h"

const wstring& BusinessConfigurationBase::GetTitle(const wstring& p_YUID) const
{
	static wstring g_Empty;

	const auto findIt = m_YUIDandTitles.find(p_YUID);
	if (findIt != m_YUIDandTitles.end())
		return findIt->second;

	ASSERT(false);
	return g_Empty;
}

bool BusinessConfigurationBase::IsProperty4RoleDelegation(const wstring& p_YUID) const
{
	return m_Properties4RoleDelegation.find(p_YUID) != m_Properties4RoleDelegation.end();
}

bool BusinessConfigurationBase::IsComputedProperty(const wstring& p_YUID) const
{
	return m_ComputedProperties.find(p_YUID) != m_ComputedProperties.end();
}

const tsl::ordered_set<wstring>& BusinessConfigurationBase::GetProperties4RoleDelegation() const
{
	return m_Properties4RoleDelegation;
}
