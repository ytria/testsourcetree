#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessLicenseDetail.h"

class LicenseDetailDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessLicenseDetail>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

