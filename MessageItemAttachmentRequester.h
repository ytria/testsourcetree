#pragma once

#include "IRequester.h"
#include "IRequestLogger.h"

class BusinessItemAttachment;
class ItemAttachmentDeserializer;

class MessageItemAttachmentRequester : public IRequester
{
public:
    MessageItemAttachmentRequester(PooledString p_UserId, PooledString p_MessageId, PooledString p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger);

    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
    const BusinessItemAttachment& GetData() const;

private:
    std::shared_ptr<ItemAttachmentDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;

    PooledString m_UserId;
    PooledString m_MessageId;
    PooledString m_AttachmentId;
};

