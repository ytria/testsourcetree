#include "FrameBase.h"
#include "MainFrame.h"

set<UINT> PreAllocatedIDS::g_usedPictureIds;
set<UINT> PreAllocatedIDS::g_usedGroupPolicyIconIds;
set<UINT> PreAllocatedIDS::g_usedEditUAorElevatedIds;

IMPLEMENT_XTP_CONTROL(FixedSizeControlBitmap, CXTPControlBitmap)

FixedSizeControlBitmap::FixedSizeControlBitmap(const CSize& p_Size)
	: m_Size(p_Size)
{

}

FixedSizeControlBitmap::FixedSizeControlBitmap()
	: FixedSizeControlBitmap({0, 0})
{

}

CSize FixedSizeControlBitmap::GetSize(CDC* /*pDC*/)
{
	return m_Size;
}

void FixedSizeControlBitmap::Draw(CDC* pDC)
{
	CXTPImageManagerIcon* pIcon = GetImageManager()->GetImage(m_nId, ICON_BIG);
	if (pIcon)
		pIcon->Draw(pDC, GetRect().TopLeft(), m_Size);
}

void FixedSizeControlBitmap::Copy(CXTPControl* pControl, BOOL bRecursive /*= FALSE*/)
{
	CXTPControl::Copy(pControl, bRecursive);
	auto other = dynamic_cast<FixedSizeControlBitmap*>(pControl);
	ASSERT(nullptr != other);
	if (nullptr != other)
		m_Size = other->m_Size;
}

// ==================================================================================

namespace
{
	class TempBackColor
	{
	private:
		class MessageBarPaintManagerAccessor : public CXTPMessageBarPaintManager
		{
		public:
			static CXTPPaintManagerColor& ClrBack(CXTPMessageBarPaintManager* p_mbpm)
			{
				auto c = (MessageBarPaintManagerAccessor*)p_mbpm;
				return c->m_clrBack;
			}

			static CXTPPaintManagerColor& ClrBorder(CXTPMessageBarPaintManager* p_mbpm)
			{
				auto c = (MessageBarPaintManagerAccessor*)p_mbpm;
				return c->m_clrBorder;
			}
		};

	public:
		TempBackColor(CXTPMessageBarPaintManager* p_mbpm, CXTPPaintManagerColor p_Color)
			: m_mbpm(p_mbpm)
			, m_ColorBackBackup(MessageBarPaintManagerAccessor::ClrBack(m_mbpm))
			, m_ColorBorderBackup(MessageBarPaintManagerAccessor::ClrBorder(m_mbpm))
		{
			if (p_Color != COLORREF(-1))
			{
				MessageBarPaintManagerAccessor::ClrBack(m_mbpm) = p_Color;
				// Use same ratio as in CXTPMessageBarThemeOffice2013::RefreshMetrics()
				MessageBarPaintManagerAccessor::ClrBorder(m_mbpm) = RGB(GetRValue(p_Color) * (215. / 252), GetGValue(p_Color) * (216. / 247), GetBValue(p_Color) * (137. / 182));
			}
		}

		~TempBackColor()
		{
			MessageBarPaintManagerAccessor::ClrBack(m_mbpm) = m_ColorBackBackup;
			MessageBarPaintManagerAccessor::ClrBorder(m_mbpm) = m_ColorBorderBackup;
		}

	private:
		CXTPMessageBarPaintManager* m_mbpm;
		CXTPPaintManagerColor m_ColorBackBackup;
		CXTPPaintManagerColor m_ColorBorderBackup;
	};
};

void ColoredMessageBar::SetBackgroundColor(COLORREF p_Color)
{
	m_clrBack = p_Color;
}

void ColoredMessageBar::FillMessageBar(CDC* pDC)
{
	TempBackColor _(GetMessageBarPaintManager(), m_clrBack);
	CXTPMessageBar::FillMessageBar(pDC);
}

//void ColoredMessageBar::DrawContent(CDC * pDC)
//{
//	CXTPMessageBar::DrawContent(pDC);
//}
//
//void ColoredMessageBar::DrawButton(CDC * pDC, CXTPMessageBarButton * pButton)
//{
//	CXTPMessageBar::DrawButton(pDC, pButton);
//}
