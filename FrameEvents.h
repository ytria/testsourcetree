#pragma once

#include "GridFrameBase.h"

#include "BusinessEvent.h"
#include "EventListRequester.h"
#include "GridEvents.h"
#include "ModuleBase.h"

class FrameEvents : public GridFrameBase
{
public:
	FrameEvents(Origin p_Origin, bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

    DECLARE_DYNAMIC(FrameEvents)
    virtual ~FrameEvents() = default;

    void ShowEvents(const O365DataMap<BusinessUser, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_FullPurge, bool p_NoPurge);
	void ShowEvents(const O365DataMap<BusinessGroup, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_FullPurge, bool p_NoPurge);
	void ShowEventsWithAttachments(const O365DataMap<BusinessUser, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_UpdateOperations, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge);
	void ShowEventsWithAttachments(const O365DataMap<BusinessGroup, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_UpdateOperations, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge);
	void ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments);

	virtual O365Grid& GetGrid() override;

    void ApplySpecific(bool p_Selected);

    virtual void ApplyAllSpecific() override;
    virtual void ApplySelectedSpecific() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

    void ShowItemAttachment(const BusinessItemAttachment& p_Attachment);

	virtual void HiddenViaHistory() override;
    virtual bool HasApplyButton() override;

protected:
    //DECLARE_MESSAGE_MAP()

	virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;
	
	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
    GridEvents m_EventsGrid;
	Origin m_Origin;
};

