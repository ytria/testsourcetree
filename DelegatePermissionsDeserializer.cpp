#include "DelegatePermissionsDeserializer.h"

using namespace Ex;

void DelegatePermissionsDeserializer::DeserializeObject(const XERCES_CPP_NAMESPACE::DOMElement& p_Element)
{
    m_DelegatePermissions.m_CalendarFolder = DeserializeString(p_Element, _YTEXT("CalendarFolderPermissionLevel"));
    m_DelegatePermissions.m_TasksFolder = DeserializeString(p_Element, _YTEXT("TasksFolderPermissionLevel"));
    m_DelegatePermissions.m_InboxFolder = DeserializeString(p_Element, _YTEXT("InboxFolderPermissionLevel"));
    m_DelegatePermissions.m_ContactsFolder = DeserializeString(p_Element, _YTEXT("ContactsFolderPermissionLevel"));
    m_DelegatePermissions.m_NotesFolder = DeserializeString(p_Element, _YTEXT("NotesFolderPermissionLevel"));
    m_DelegatePermissions.m_JournalFolder = DeserializeString(p_Element, _YTEXT("JournalFolderPermissionLevel"));

}

const Ex::DelegatePermissions& DelegatePermissionsDeserializer::GetObject() const
{
    return m_DelegatePermissions;
}
