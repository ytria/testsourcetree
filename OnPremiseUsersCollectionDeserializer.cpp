#include "OnPremiseUsersCollectionDeserializer.h"

#include "PSSerializeUtil.h"
#include "LoggerService.h"

void OnPremiseUsersCollectionDeserializer::Deserialize(IPSObjectPropertyReader& p_Reader)
{
	OnPremiseUser user;

	user.m_AccountLockoutTime = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("AccountLockoutTime"));
	user.m_AccountExpires = PSSerializeUtil::DeserializeInt64AsTimeDate(p_Reader, _YTEXT("accountExpires"));
	user.m_AccountNotDelegated = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("AccountNotDelegated"));
	user.m_AdminCount = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("adminCount"));
	user.m_AllowReversiblePasswordEncryption = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("AllowReversiblePasswordEncryption"));
	user.m_AuthenticationPolicy = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("AuthenticationPolicy"));
	user.m_AuthenticationPolicySilo = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("AuthenticationPolicySilo"));
	user.m_BadLogonCount = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("BadLogonCount"));
	user.m_BadPasswordTime = PSSerializeUtil::DeserializeInt64AsTimeDate(p_Reader, _YTEXT("badPasswordTime"));
	user.m_BadPwdCount = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("badPwdCount"));
	user.m_CannotChangePassword = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("CannotChangePassword"));
	user.m_CanonicalName = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("CanonicalName"));
	user.m_Certificates = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("Certificates"));
	user.m_City = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("City"));
	user.m_CN = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("CN"));
	user.m_CodePage = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("codePage"));
	user.m_Company = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Company"));
	user.m_CompoundIdentitySupported = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("CompoundIdentitySupported"));
	user.m_Country = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Country"));
	user.m_CountryCode = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("countryCode"));
	user.m_Created = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("Created"));
	user.m_Deleted = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("Deleted"));
	user.m_Department = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Department"));
	user.m_Description = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Description"));
	user.m_DisplayName = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("DisplayName"));
	user.m_DistinguishedName = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("DistinguishedName"));
	user.m_Division = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Division"));
	user.m_DoesNotRequirePreAuth = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("DoesNotRequirePreAuth"));
	user.m_dSCorePropagationData = PSSerializeUtil::DeserializeDateTimeCollection(p_Reader, _YTEXT("dSCorePropagationData"));
	user.m_EmailAddress = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("EmailAddress"));
	user.m_EmployeeID = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("EmployeeID"));
	user.m_EmployeeNumber = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("EmployeeNumber"));
	user.m_Enabled = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("Enabled"));
	user.m_Fax = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Fax"));
	user.m_GivenName = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("GivenName"));
	user.m_HomeDirectory = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("HomeDirectory"));
	user.m_HomedirRequired = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("HomedirRequired"));
	user.m_HomeDrive = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("HomeDrive"));
	user.m_HomePage = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("HomePage"));
	user.m_HomePhone = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("HomePhone"));
	user.m_Initials = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Initials"));
	user.m_InstanceType = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("instanceType"));
	user.m_IsCriticalSystemObject = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("isCriticalSystemObject"));
	user.m_IsDeleted = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("IsDeleted"));
	user.m_KerberosEncryptionType = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("KerberosEncryptionType"));
	user.m_LastBadPasswordAttempt = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("LastBadPasswordAttempt"));
	user.m_LastKnownParent = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("LastKnownParent"));
	user.m_LastLogoff = PSSerializeUtil::DeserializeInt64AsTimeDate(p_Reader, _YTEXT("lastLogoff"));
	user.m_LastLogon = PSSerializeUtil::DeserializeInt64AsTimeDate(p_Reader, _YTEXT("lastLogon"));
	user.m_LastLogonDate = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("LastLogonDate"));
	user.m_LockedOut = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("LockedOut"));
	user.m_LogonCount = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("logonCount"));
	//user.m_LogonHours = PSSerializeUtil::DeserializeCollectionAsString(p_Reader, _YTEXT("logonHours"));
	user.m_LogonWorkstations = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("LogonWorkstations"));
	user.m_Manager = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Manager"));
	user.m_MemberOf = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("MemberOf"));
	user.m_MNSLogonAccount = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("MNSLogonAccount"));
	user.m_MobilePhone = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("MobilePhone"));
	user.m_Modified = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("Modified"));
	user.m_MSDSUserAccountControlComputed = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("msDS-User-Account-Control-Computed"));
	user.m_MsDsConsistencyGuid = PSSerializeUtil::DeserializeByteArray(p_Reader, _YTEXT("ms-DS-ConsistencyGuid"));
	user.m_Name = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Name"));
	//user.m_nTSecurityDescriptor = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("nTSecurityDescriptor"));
	user.m_ObjectCategory = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("ObjectCategory"));
	user.m_ObjectClass = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("ObjectClass"));
	user.m_ObjectGUID = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("ObjectGUID"));
	user.m_ObjectSid = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("objectSid"));
	user.m_Office = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Office"));
	user.m_OfficePhone = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("OfficePhone"));
	user.m_Organization = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Organization"));
	user.m_OtherName = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("OtherName"));
	user.m_PasswordExpired = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("PasswordExpired"));
	user.m_PasswordLastSet = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("PasswordLastSet"));
	user.m_PasswordNeverExpires = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("PasswordNeverExpires"));
	user.m_PasswordNotRequired = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("PasswordNotRequired"));
	user.m_POBox = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("POBox"));
	user.m_PostalCode = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("PostalCode"));
	user.m_PrimaryGroup = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("PrimaryGroup"));
	user.m_PrimaryGroupId = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("primaryGroupID"));
	user.m_PrincipalsAllowedToDelegateToAccount = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("PrincipalsAllowedToDelegateToAccount"));
	user.m_ProfilePath = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("ProfilePath"));
	user.m_ProtectedFromAccidentalDeletion = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("ProtectedFromAccidentalDeletion"));
	user.m_ProxyAddresses = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("proxyAddresses"));
	user.m_PrincipalsAllowedToDelegateToAccount = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("PrincipalsAllowedToDelegateToAccount"));
	user.m_SamAccountName = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("SamAccountName"));
	user.m_SAMAccountType = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("sAMAccountType"));
	user.m_ServicePrincipalNames = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("ServicePrincipalNames"));
	user.m_ScriptPath = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("ScriptPath"));
	user.m_SDRightsEffective = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("sDRightsEffective"));
	user.m_SID = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("SID"));
	user.m_SIDHistory = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("SIDHistory"));
	user.m_SmartcardLogonRequired = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("SmartcardLogonRequired"));
	user.m_State = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("State"));
	user.m_StreetAddress = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("StreetAddress"));
	user.m_Surname = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Surname"));
	user.m_Title = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Title"));
	user.m_TrustedForDelegation = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("TrustedForDelegation"));
	user.m_TrustedToAuthForDelegation = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("TrustedToAuthForDelegation"));
	user.m_UseDESKeyOnly = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("UseDESKeyOnly"));
	user.m_UserAccountControl = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("userAccountControl"));
	user.m_UserCertificate = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("userCertificate"));
	user.m_UserPrincipalName = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("UserPrincipalName"));
	user.m_USNChanged = PSSerializeUtil::DeserializeInt64(p_Reader, _YTEXT("uSNChanged"));
	user.m_USNCreated = PSSerializeUtil::DeserializeInt64(p_Reader, _YTEXT("uSNCreated"));
	user.m_WhenChanged = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("whenChanged"));
	user.m_WhenCreated = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("whenCreated"));

	m_Data.push_back(user);
}

void OnPremiseUsersCollectionDeserializer::SetCount(size_t p_Count)
{
	m_Data.reserve(p_Count);
}
