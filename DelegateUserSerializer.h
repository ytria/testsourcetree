#pragma once

#include "IXmlSerializer.h"

namespace Ex
{
    class DelegateUser;
}

namespace Ex
{
class DelegateUserSerializer : public IXmlSerializer
{
public:
    DelegateUserSerializer(XERCES_CPP_NAMESPACE::DOMDocument& p_Document, const Ex::DelegateUser& p_DelegateUser);

protected:
    void SerializeImpl(DOMElement& p_Element) override;

private:
    const Ex::DelegateUser& m_DelegateUser;
};
}

