#pragma once

#include "IMainItemModification.h"

#include "ActivityLoggerUtil.h"
#include "GridBackendField.h"
#include "MsGraphFieldNames.h"
#include "Scope.h"
#include <boost/optional.hpp>

#include <vector>

class O365Grid;

enum FieldUpdateFlags : uint32_t
{
	None = 0,
	IgnoreRemoteValue = 0x1,
	IgnoreRemoteHasOldValueError = 0x2,
};

template <Scope scope>
class FieldUpdate : public IMainItemModification
{
public:
    FieldUpdate(O365Grid&					p_Grid,
				const row_pk_t&				p_RowKey,
				const UINT&					p_ColumnId,
				const GridBackendField&		p_OldValue,
				const GridBackendField&		p_NewValue,
				uint32_t p_Flags = FieldUpdateFlags::None);
    virtual ~FieldUpdate();

	virtual void Revert();

    virtual void RefreshState() override;

    row_pk_t		GetRowKey() const;
    UINT			GetColumnID() const;

	void UpdateShownValue(); // Just for internal use

	virtual vector<ModificationLog> GetModificationLogs() const override;

	const boost::YOpt<GridBackendField>& GetActualValue() const;
	const boost::YOpt<GridBackendField>& GetOldValue() const;
	const boost::YOpt<GridBackendField>& GetNewValue() const;

	virtual std::unique_ptr<FieldUpdate<scope>> CreateReplacement(std::unique_ptr<FieldUpdate<scope>> p_Replacement);

	// BAD! Keep?
	const O365Grid& GetGrid() const;

protected:
	O365Grid& getGrid();
	const O365Grid& getGrid() const;
	bool shoulRevertOnDestruction();
    void dontRevertOnDestruction();
	boost::YOpt<GridBackendField>& getActualValue();

private:
    void UpdateFieldShownSentValue(GridBackendRow* p_Row, UINT p_ColumnID);

private:
    O365Grid& m_Grid;

    row_pk_t	m_RowKey;
	UINT		m_ColumnID;

	boost::YOpt<GridBackendField> m_ActualValue;
    boost::YOpt<GridBackendField> m_OldValue;
	boost::YOpt<GridBackendField> m_NewValue;

	bool m_RevertOnDestruction;
	uint32_t m_Flags;

	bool m_AlreadyResetAfterError;
};

using FieldUpdateO365 = FieldUpdate<Scope::O365>;
using FieldUpdateOnPrem = FieldUpdate<Scope::ONPREM>;
