#pragma once

#include "IRequester.h"
#include "Application.h"
#include "SingleRequestResult.h"
#include "IRequestLogger.h"

class ApplicationDeserializer;

class ApplicationRequester : public IRequester
{
public:
	ApplicationRequester(const wstring& p_Id, const std::shared_ptr<IRequestLogger>& p_Logger);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const SingleRequestResult& GetResult() const;
	const Application& GetData() const;

private:
	std::shared_ptr<SingleRequestResult> m_Result;
	std::shared_ptr<ApplicationDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;

	wstring m_Id;
};

