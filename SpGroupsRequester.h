#pragma once

#include "IRequester.h"
#include "SpUtils.h"

#include "SpGroup.h"
#include "SpGroupDeserializer.h"
#include "ValueListDeserializer.h"

class SingleRequestResult;

namespace Sp
{
    class SpGroupsRequester : public IRequester
    {
    public:
        SpGroupsRequester(PooledString p_SiteUrl);
        TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

        const vector<Sp::Group>& GetData() const;

    private:
        PooledString m_SiteUrl;
        std::shared_ptr<ValueListDeserializer<Sp::Group, Sp::GroupDeserializer>> m_Deserializer;
        std::shared_ptr<SingleRequestResult> m_Result;
    };
}

