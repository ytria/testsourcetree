#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessList.h"

class GraphListDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessList>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

