#pragma once

namespace Azure
{
	class CosmosLocation
	{
	public:
		boost::YOpt<PooledString> m_DocumentEndpoint;
		boost::YOpt<PooledString> m_Id;
		boost::YOpt<PooledString> m_LocationName;
		boost::YOpt<PooledString> m_ProvisioningState;
		boost::YOpt<int32_t> m_FailoverPriority;
	};
}
