#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "EducationUser.h"

class EducationUserDeserializer : public JsonObjectDeserializer, public Encapsulate<EducationUser>
{
public:
	EducationUserDeserializer();
	EducationUserDeserializer(std::shared_ptr<const Sapio365Session> p_Session);
	void DeserializeObject(const web::json::object& p_Object) override;

private:
	std::shared_ptr<const Sapio365Session> m_Session;
};

