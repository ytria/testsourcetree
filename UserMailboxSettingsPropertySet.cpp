#include "UserMailboxSettingsPropertySet.h"
#include "MsGraphFieldNames.h"
#include "User.h"

vector<rttr::property> UserMailboxSettingsPropertySet::GetPropertySet() const
{
    return { rttr::type::get<User>().get_property(O365_USER_MAILBOXSETTINGS) };
}
