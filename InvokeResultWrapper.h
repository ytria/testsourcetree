#pragma once

#include "InvokeResult.h"

class InvokeResultWrapper
{
public:
	InvokeResultWrapper() = default;
	InvokeResultWrapper(const InvokeResult& p_Result);
	InvokeResultWrapper(const InvokeResultWrapper& p_Other) = delete;
	InvokeResultWrapper& operator=(InvokeResultWrapper&& p_Other) = default;
	InvokeResultWrapper& operator=(const InvokeResultWrapper& p_Other) = delete;
	~InvokeResultWrapper();

	const InvokeResult& Get() const;
	InvokeResult& Get();

	void SetResult(const InvokeResult& p_Result);

private:
	InvokeResult m_InvokeResult;
};

wstring GetPSErrorString(const std::shared_ptr<InvokeResultWrapper>& p_InitResult);