#pragma once

#include <memory>

using row_pk_t = std::vector<GridBackendField>;

class Modification
{
public:
    // Order of this enum specifies priority
    enum class State
    {
        RemoteHasNewValue,
        RemoteHasOldValue,
        RemoteHasOtherValue,
        RemoteError,
        AppliedLocally,

		BeingReverted, // For notification purpose
    };

	static const wstring g_HiddenValueFromLog;

    class MustSurviveRefresh
    {
    public:
        template <typename T> // T must inherit from Modification
        bool operator()(const std::unique_ptr<T>& p_Modification) const
        {
            return HasFinalStatusOrLocallyApplied(p_Modification.get());
        }

    private:
        bool HasFinalStatusOrLocallyApplied(Modification* p_Modification) const
        {
            return	p_Modification->GetState() == Modification::State::AppliedLocally
				||	p_Modification->GetState() == Modification::State::RemoteHasNewValue
				||	p_Modification->GetState() == Modification::State::RemoteHasOtherValue
				||	p_Modification->GetState() == Modification::State::RemoteError
				;
        }
    };

	class IsLocal
	{
	public:
		template <typename T> // T must inherit from Modification
		bool operator()(const std::unique_ptr<T>& p_Modification) const
		{
			return p_Modification->GetState() == Modification::State::AppliedLocally;
		}
	};

	class IsSent
	{
	public:
		template <typename T> // T must inherit from Modification
		bool operator()(const std::unique_ptr<T>& p_Modification) const
		{
			return HasSentStatus(p_Modification.get());
		}

	private:
		bool HasSentStatus(Modification* p_Modification) const
		{
			return	p_Modification->GetState() == Modification::State::RemoteHasNewValue
				||	p_Modification->GetState() == Modification::State::RemoteHasOtherValue
				||	p_Modification->GetState() == Modification::State::RemoteError
				;
		}
	};

	class IsSentAndReceived
	{
	public:
		template <typename T> // T must inherit from Modification
		bool operator()(const std::unique_ptr<T>& p_Modification) const
		{
			return HasSentStatus(p_Modification.get());
		}

		template <typename T> // T must inherit from Modification
		bool operator()(T* p_Modification) const
		{
			return HasSentStatus(p_Modification);
		}

	private:
		bool HasSentStatus(Modification* p_Modification) const
		{
			return	p_Modification->GetState() == Modification::State::RemoteHasNewValue
				//||	p_Modification->GetState() == Modification::State::RemoteHasOtherValue
				;
		}
	};

	class IsError
	{
	public:
		template <typename T> // T must inherit from Modification
		bool operator()(const std::unique_ptr<T>& p_Modification) const
		{
			return HasErrorStatus(p_Modification.get());
		}

	private:
		bool HasErrorStatus(Modification* p_Modification) const
		{
			return p_Modification->GetState() == Modification::State::RemoteError;
		}
	};

    virtual ~Modification() = default;

    State GetState() const;

	struct ModificationLog // for user activity log database
	{
		ModificationLog(const vector<wstring>& p_PK, const wstring& p_Name, const wstring& p_Action, const wstring& p_Property, State p_State):
			m_ObjectPK(p_PK),
			m_ObjectName(p_Name),
			m_Action(p_Action),
			m_Property(p_Property),
			m_State(p_State)
		{}

		ModificationLog(const vector<wstring>& p_PK, const wstring& p_Name, const wstring& p_Action, const wstring& p_Property, const vector<wstring>& p_NewMultiValue, State p_State) :
			m_ObjectPK(p_PK),
			m_ObjectName(p_Name),
			m_Action(p_Action),
			m_Property(p_Property),
			m_NewMultiValue(p_NewMultiValue),
			m_State(p_State)
		{
		}

		ModificationLog(const vector<wstring>& p_PK, const wstring& p_Name, const wstring& p_Action, const wstring& p_Property, const wstring& p_OldValue, const wstring& p_NewValue, State p_State) :
			m_ObjectPK(p_PK),
			m_ObjectName(p_Name),
			m_Action(p_Action),
			m_Property(p_Property),
			m_OldValue(p_OldValue),
			m_NewValue(p_NewValue),
			m_State(p_State)
		{
		}

		vector<wstring>					m_ObjectPK;
		wstring							m_ObjectName;
		wstring							m_Action;// update, delete, create...
		wstring							m_Property;// grid column
		boost::YOpt<wstring>			m_OldValue;
		boost::YOpt<wstring>			m_NewValue;
		boost::YOpt<vector<wstring>>	m_NewMultiValue;
		State							m_State;

		static const wstring g_NotSet;
	};

	virtual vector<ModificationLog> GetModificationLogs() const = 0;
	const wstring&	GetObjectName() const;
	void			SetObjectName(const wstring& p_ObjectName);

protected:
    Modification(State p_State, const wstring& p_ObjectName);
    State m_State;
	wstring m_ObjectName;
};