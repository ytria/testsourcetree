#include "MessageRuleDeserializer.h"

#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "MsGraphFieldNames.h"
#include "RecipientDeserializer.h"

void MessageRuleDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		MessageRuleActionsDeserializer mrad;
		if (JsonSerializeUtil::DeserializeAny(mrad, _YTEXT(O365_MESSAGERULE_ACTIONS), p_Object))
			m_Data.m_Actions = mrad.GetData();
	}

	{
		MessageRulePredicatesDeserializer mrpd;
		if (JsonSerializeUtil::DeserializeAny(mrpd, _YTEXT(O365_MESSAGERULE_CONDITIONS), p_Object))
			m_Data.m_Conditions = mrpd.GetData();
	}

	JsonSerializeUtil::DeserializeString(_YTEXT(O365_MESSAGERULE_DISPLAYNAME), m_Data.m_DisplayName, p_Object);

	{
		MessageRulePredicatesDeserializer mrpd;
		if (JsonSerializeUtil::DeserializeAny(mrpd, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), p_Object))
			m_Data.m_Exceptions = mrpd.GetData();
	}

    JsonSerializeUtil::DeserializeBool(_YTEXT(O365_MESSAGERULE_HASERROR), m_Data.m_HasError, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT(O365_ID), m_Data.m_Id, p_Object, true);
	JsonSerializeUtil::DeserializeBool(_YTEXT(O365_MESSAGERULE_ISENABLED), m_Data.m_IsEnabled, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT(O365_MESSAGERULE_ISREADONLY), m_Data.m_IsReadOnly, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YTEXT(O365_MESSAGERULE_SEQUENCE), m_Data.m_Sequence, p_Object);
}

// =================================================================================================

void MessageRuleActionsDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		ListStringDeserializer lsd;
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("assignCategories"), p_Object))
			m_Data.m_AssignCategories = lsd.GetData();
	}

	JsonSerializeUtil::DeserializeString(_YTEXT("copyToFolder"), m_Data.m_CopyToFolderID, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("delete"), m_Data.m_Delete, p_Object);

	{
		ListDeserializer<Recipient, RecipientDeserializer> rld;
		if (JsonSerializeUtil::DeserializeAny(rld, _YTEXT("forwardAsAttachmentTo"), p_Object))
			m_Data.m_ForwardAsAttachmentTo = rld.GetData();
	}

	{
		ListDeserializer<Recipient, RecipientDeserializer> rld;
		if (JsonSerializeUtil::DeserializeAny(rld, _YTEXT("forwardTo"), p_Object))
			m_Data.m_ForwardTo = rld.GetData();
	}

	JsonSerializeUtil::DeserializeBool(_YTEXT("markAsRead"), m_Data.m_MarkAsRead, p_Object);
	
	// low, normal, high
	JsonSerializeUtil::DeserializeString(_YTEXT("markImportance"), m_Data.m_MarkImportance, p_Object);

	JsonSerializeUtil::DeserializeString(_YTEXT("moveToFolder"), m_Data.m_MoveToFolderID, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("permanentDelete"), m_Data.m_PermanentDelete, p_Object);

	{
		ListDeserializer<Recipient, RecipientDeserializer> rld;
		if (JsonSerializeUtil::DeserializeAny(rld, _YTEXT("redirectTo"), p_Object))
			m_Data.m_RedirectTo = rld.GetData();
	}

	JsonSerializeUtil::DeserializeBool(_YTEXT("stopProcessingRules"), m_Data.m_StopProcessingRules, p_Object);
}

// =================================================================================================

void MessageRulePredicatesDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		ListStringDeserializer lsd;
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("bodyContains"), p_Object))
			m_Data.m_BodyContains = lsd.GetData();
	}

	{
		ListStringDeserializer lsd;
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("bodyOrSubjectContains"), p_Object))
			m_Data.m_BodyOrSubjectContains = lsd.GetData();
	}

	{
		ListStringDeserializer lsd;
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("categories"), p_Object))
			m_Data.m_Categories = lsd.GetData();
	}

	{
		ListDeserializer<Recipient, RecipientDeserializer> rld;
		if (JsonSerializeUtil::DeserializeAny(rld, _YTEXT("fromAddresses"), p_Object))
			m_Data.m_FromAddresses = rld.GetData();
	}

	JsonSerializeUtil::DeserializeBool(_YTEXT("hasAttachments"), m_Data.m_HasAttachments, p_Object);

	{
		ListStringDeserializer lsd;
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("headerContains"), p_Object))
			m_Data.m_HeaderContains = lsd.GetData();
	}

	// low, normal, high
	JsonSerializeUtil::DeserializeString(_YTEXT("importance"), m_Data.m_Importance, p_Object);

	JsonSerializeUtil::DeserializeBool(_YTEXT("isApprovalRequest"), m_Data.m_IsApprovalRequest, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isAutomaticForward"), m_Data.m_IsAutomaticForward, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isAutomaticReply"), m_Data.m_IsAutomaticReply, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isEncrypted"), m_Data.m_IsEncrypted, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isMeetingRequest"), m_Data.m_IsMeetingRequest, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isMeetingResponse"), m_Data.m_IsMeetingResponse, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isNonDeliveryReport"), m_Data.m_IsNonDeliveryReport, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isPermissionControlled"), m_Data.m_IsPermissionControlled, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isReadReceipt"), m_Data.m_IsReadReceipt, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isSigned"), m_Data.m_IsSigned, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isVoicemail"), m_Data.m_IsVoicemail, p_Object);

	// any, call, doNotForward, followUp, fyi, forward, noResponseNecessary, read, reply, replyToAll, review
	JsonSerializeUtil::DeserializeString(_YTEXT("messageActionFlag"), m_Data.m_MessageActionFlag, p_Object);

	JsonSerializeUtil::DeserializeBool(_YTEXT("notSentToMe"), m_Data.m_NotSentToMe, p_Object);

	{
		ListStringDeserializer lsd;
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("recipientContains"), p_Object))
			m_Data.m_RecipientContains = lsd.GetData();
	}

	{
		ListStringDeserializer lsd;
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("senderContains"), p_Object))
			m_Data.m_SenderContains = lsd.GetData();
	}

	// normal, personal, private, confidential
	JsonSerializeUtil::DeserializeString(_YTEXT("sensitivity"), m_Data.m_Sensitivity, p_Object);

	JsonSerializeUtil::DeserializeBool(_YTEXT("sentCcMe"), m_Data.m_SentCCMe, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("sentOnlyToMe"), m_Data.m_SentOnlyToMe, p_Object);

	{
		ListDeserializer<Recipient, RecipientDeserializer> rld;
		if (JsonSerializeUtil::DeserializeAny(rld, _YTEXT("sentToAddresses"), p_Object))
			m_Data.m_SentToAddresses = rld.GetData();
	}

	JsonSerializeUtil::DeserializeBool(_YTEXT("sentToMe"), m_Data.m_SentToMe, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("sentToOrCcMe"), m_Data.m_SentToOrCCMe, p_Object);

	{
		ListStringDeserializer lsd;
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("subjectContains"), p_Object))
			m_Data.m_SubjectContains = lsd.GetData();
	}

	{
		SizeRangeDeserializer srd;
		if (JsonSerializeUtil::DeserializeAny(srd, _YTEXT("withinSizeRange"), p_Object))
			m_Data.m_WithinSizeRange = srd.GetData();
	}
}

// =================================================================================================

void SizeRangeDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeInt32(_YTEXT("maximumSize"), m_Data.m_MaximumSize, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YTEXT("minimumSize"), m_Data.m_MinimumSize, p_Object);
}
