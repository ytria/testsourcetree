#include "GridUpdater.h"

#include "ActivityLogger.h"
#include "BaseO365Grid.h"
#include "GridBackendRow.h"
#include "SapioError.h"
#include "IRowFlagsGetter.h"

struct GridUpdater::IModHandlerImpl
{
public:
	virtual CreatedObjectModification* GetCreatedObjectModification(const row_pk_t& p_PK) const = 0;
	virtual vector<RowModification*> GetDeletionRowModifications() const = 0;
	virtual void RefreshModificationsStatus(GridUpdater& p_GU) = 0;
	virtual void RefreshSubItemModificationsStates(GridUpdater& p_GU) = 0;
	virtual Scope GetScope() const = 0;
};

template<class Mods>
struct GridUpdater::ModHandler : public GridUpdater::IModHandlerImpl
{
public:
	ModHandler(Mods* p_Mods)
		: m_Mods(p_Mods)
	{
		ASSERT(nullptr != m_Mods);
	}

	CreatedObjectModification* GetCreatedObjectModification(const row_pk_t& p_PK) const override
	{
		return nullptr;
	}
	vector<RowModification*> GetDeletionRowModifications() const override
	{
		return {};
	}

	void RefreshModificationsStatus(GridUpdater& p_GU) override
	{
		// Deal with created objects
		// !! Should now be called from frame, in RefreshSpecific()
		//HandleCreatedModifications(m_Grid, m_UpdateOperations);

		//m_Grid.GetRowIndex().LoadRows();
		m_Mods->RefreshStates(p_GU);
		// log'n'loll
		ActivityLogger::GetInstance().LogGridModifications(true, *m_Mods);
		m_Mods->RefreshStatesPostProcess(p_GU);
	}

	void RefreshSubItemModificationsStates(GridUpdater& p_GU) override
	{

	}

	Scope GetScope() const override
	{
		return m_Mods->GetScope();
	}

private:
	Mods* m_Mods;
};

template<>
CreatedObjectModification* GridUpdater::ModHandler<GridModificationsO365>::GetCreatedObjectModification(const row_pk_t& p_PK) const
{
	return m_Mods->GetRowModification<CreatedObjectModification>(p_PK);
}

template<>
vector<RowModification*> GridUpdater::ModHandler<GridModificationsO365>::GetDeletionRowModifications() const
{
	return m_Mods->GetRowModifications([](RowModification* p_Mod) {
		return dynamic_cast<DeletedObjectModification*>(p_Mod) != nullptr || dynamic_cast<RestoreObjectModification*>(p_Mod) != nullptr;
		});
}

template<>
void GridUpdater::ModHandler<GridModificationsO365>::RefreshSubItemModificationsStates(GridUpdater& p_GU)
{
	m_Mods->RefreshSubItemModificationsStates(p_GU);
	ActivityLogger::GetInstance().LogGridModifications(false, *m_Mods);
}

template<class Mods>
GridUpdater::IModHandlerImpl* createImpl(Mods* p_Mods)
{
	if (nullptr != p_Mods)
		return new GridUpdater::ModHandler<Mods>(p_Mods);
	return nullptr;
}

// ============================================================================

GridUpdater::GridUpdater(O365Grid& p_Grid, const GridUpdaterOptions& p_Options)
    : m_Grid(p_Grid)
	, m_Options(p_Options)
	, m_PostUpdateErrorBackup(nullptr)
	, m_ModHandlerImpl(createImpl(p_Grid.IsGridModificationsEnabled() ? &p_Grid.GetModifications() : nullptr))
{
	if (!m_Grid.GetPKColumns().empty())
		m_Grid.GetRowIndex().LoadRows();
}

GridUpdater::GridUpdater(O365Grid& p_Grid, const GridUpdaterOptions& p_Options, const vector<O365UpdateOperation>& p_Updates)
	: GridUpdater(p_Grid, p_Options, p_Updates, p_Grid.IsGridModificationsEnabled() ? &p_Grid.GetModifications() : nullptr)
{
}

GridUpdater::GridUpdater(O365Grid& p_Grid, const GridUpdaterOptions& p_Options, const vector<O365UpdateOperation>& p_Updates, GridModificationsO365* p_GridModification)
	: m_Grid(p_Grid)
	, m_UpdateOperations(p_Updates)
	, m_Options(p_Options)
	, m_PostUpdateErrorBackup(nullptr)
	, m_ModHandlerImpl(createImpl(p_GridModification))
{
	// no grid index load here?

	for (auto& upOp : m_UpdateOperations)
		AddUpdatedRowPk(upOp.GetPrimaryKey());
}

GridUpdater::GridUpdater(O365Grid& p_Grid, const GridUpdaterOptions& p_Options, const vector<O365UpdateOperation>& p_Updates, GridFieldModificationsOnPrem* p_GridModification)
	: m_Grid(p_Grid)
	, m_UpdateOperations(p_Updates)
	, m_Options(p_Options)
	, m_PostUpdateErrorBackup(nullptr)
	, m_ModHandlerImpl(createImpl(p_GridModification))
{
	// no grid index load here?

	for (auto& upOp : m_UpdateOperations)
		AddUpdatedRowPk(upOp.GetPrimaryKey());
}

GridUpdater::~GridUpdater()
{
	m_Grid.ResetHasSavedWarningFlags();

	if (m_Options.IsFullPurge() || m_Options.IsPartialPurge())
		m_Grid.GetRowIndex().Purge(m_Options.GetColumnsWithRefreshedValues(), m_Options.GetPartialPurgeParentRows(), m_Options.IsPartialPurge());

	if (m_Options.IsProcessLoadMore())
		processLoadMore();// must remain independent from update grid flag

	processExplosionSiblingUpdate();

	processActions();// must remain independent from update grid flag, after processLoadMore

	markErrorsForUpdates();// if update operations have been provided, it will set errors on rows.
    removeDeletionRows();

	// Refresh modifications (except subitem ones) before restoring explosions (made in refreshProcessData()).
	if (m_Options.IsRefreshModificationStates())
		refreshModificationsStatus();

	refreshProcessData();// must remain independent from update grid flag, after processActions

    if (m_Options.IsRefreshModificationStates())
		refreshSubItemModificationsStates();

	refreshCollapsedRowsErrors();

	if (m_Options.IsUpdateGrid())
		m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

	if (m_Options.IsHandlePostUpdateError())
		handlePostUpdateError();

	if (nullptr != m_PostUpdateErrorBackup)
		*m_PostUpdateErrorBackup = m_PostUpdateError;
}

void GridUpdater::OnLoadingError(GridBackendRow* p_Row, const SapioError& p_Error)
{
	onError(p_Row, p_Error, false);
}

void GridUpdater::OnLoadingError(GridBackendRow* p_Row, const SapioError& p_Error, const wstring& p_ErrorPrefix)
{
	onError(p_Row, p_Error, false, p_ErrorPrefix);
}

void GridUpdater::Set403LoadingError()
{
	set403Error(false);
}

void GridUpdater::Set403LoadingError(const wstring& p_ErrorDescription)
{
	set403Error(false, p_ErrorDescription);
}

void GridUpdater::OnSavingError(GridBackendRow* p_Row, const SapioError& p_Error)
{
	onError(p_Row, p_Error, true);
}

void GridUpdater::OnSavingError(GridBackendRow* p_Row, const SapioError& p_Error, const wstring& p_ErrorPrefix)
{
	onError(p_Row, p_Error, true, p_ErrorPrefix);
}

void GridUpdater::Set403SavingError()
{
	set403Error(true);
}

void GridUpdater::Set403SavingError(const wstring& p_ErrorDescription)
{
	set403Error(true, p_ErrorDescription);
}

void GridUpdater::processLoadMore()
{
	auto& columns	= m_Options.GetColumnsWithRefreshedValues();
	auto& rows		= m_Options.GetRowsWithRefreshedValues();

	bool isProcessLoadMore = false;
	auto columnsToCheckForLoadMore = columns;
	if (columnsToCheckForLoadMore.empty() && !rows.empty())// when "load more" refreshes all columns
		columnsToCheckForLoadMore.insert(m_Grid.GetBackendColumns().begin(), m_Grid.GetBackendColumns().end());
	for (auto cIt = columnsToCheckForLoadMore.begin(); !isProcessLoadMore && cIt != columnsToCheckForLoadMore.end(); cIt++)
		isProcessLoadMore = m_Grid.IsColumnLoadMore(*cIt);
	if (isProcessLoadMore)
	{
		ASSERT(m_Grid.WithLoadMore());
		for (auto row : rows)
			m_Grid.LoadMorePostProcessRow(row);// sets LM flags, updates LM data on exploded rows
	}
}

void GridUpdater::processActions()
{
	auto& columns	= m_Options.GetColumnsWithRefreshedValues();
	auto& rows		= m_Options.GetRowsWithRefreshedValues();

	for (auto action : m_Options.GetActions())
	{
		switch (action)
		{
			case GridBackendUtil::GRIDPOSTUPATEACTION::SETFLATVIEW:
				if (m_Grid.GetBackend().IsHierarchyView())
					m_Grid.SetHierarchyView(false, false);
				break;

			case GridBackendUtil::GRIDPOSTUPATEACTION::EXPLODEMULTIVALUES:
				// presumptions: 
				// - a set of column with refreshed data was explicitely set (eg. Load More - cf. GetColumnsByPresets({g_ColumnsPresetMore}))
				// - this set of columns contains a single multivalue sisterhood
				ASSERT(!columns.empty());
				if (!columns.empty() && !rows.empty())
				{
					GridBackendColumn* xColumn = *columns.begin();
					m_Grid.MultivalueExplode(xColumn, vector<GridBackendRow*>(rows.begin(), rows.end()), false);
				}
				break;

			default:
				// https://www.youtube.com/watch?v=fbGkxcY7YFU
				ASSERT(false);
		}
	}
}

void GridUpdater::processExplosionSiblingUpdate()
{
	const auto& columns = m_Options.GetColumnsWithRefreshedValues();
	const auto& rows = m_Options.GetRowsWithRefreshedValues();

	if (!columns.empty() && !rows.empty())
	{
		for (auto row : rows)
		{
			const auto siblings = m_Grid.GetMultivalueManager().GetExplosionSiblings(row);

			for (auto sibling : siblings)
			{
				ASSERT(nullptr != sibling);
				if (nullptr != sibling)
				{
					if (sibling != row)
					{
						auto fields = sibling->SetFields(row, columns);// new data must already be added to original row
						for (auto f : fields)
							if (nullptr != f && !f->IsExplosionFragment())
								f->BlendFontColor(GridBackendUtil::g_ColorDimmed);
					}
				}
			}
		}
	}
}

void GridUpdater::refreshProcessData()
{
	const auto& columns	= m_Options.GetColumnsWithRefreshedValues();
	const auto& rows	= m_Options.GetRowsWithRefreshedValues();

	if (!rows.empty())// if rows is empty, either none or all rows were updated - for all, set the refresh flag to true (5th in GridUpdaterOptions)
		m_Options.SetRefreshProcessData(true);
	
	if (m_Options.IsRefreshProcessData())
	{
		m_Grid.RefreshProcessedData(columns, rows, m_Options.GetActions());

		bool updatedMultivalueColumn = false;
		for (auto col : columns)
		{
			if (col->IsMultivalueExploded())
			{
				updatedMultivalueColumn = true;
				break;
			}
		}

		if (updatedMultivalueColumn)
		{
			for (auto row : rows)
				m_Options.AddRowsWithRefreshedValues(m_Grid.GetMultivalueManager().GetExplosionSiblings(row));
		}
	}
}

void GridUpdater::markErrorsForUpdates()
{
    for (auto& update : m_UpdateOperations)
    {
		// FIXME: Handle the case where we have several errors to display.
		boost::YOpt<SapioError> error;
		for (const auto& result : update.GetResults())
		{
			// If no m_ResultInfo, something went bad (or operation was canceled). Handle it as error (0xffff)
			const auto status = result.m_ResultInfo ? result.m_ResultInfo->Response.status_code() : 0xffff;
			if (status < 200 || 300 <= status) // Is this an error?
			{
				error = result.m_Error;
				ASSERT(error || 0xffff == status);
				if (!error)
				{
					if (0xffff == status)
						error = SapioError(_T("Canceled"), status);
					else // Shouldn't happen
						error = SapioError(_T("Unspecified Error"), status);
				}
				break;
			}
		}

		if (error)
		{
			GridBackendRow* row = m_Grid.GetRowIndex().GetExistingRow(update.GetPrimaryKey());

			if (nullptr == row || O365Grid::IsTemporaryCreatedObjectID(row->GetField(m_Grid.GetColumnByUniqueID(_YUID(O365_ID))).GetValueStr()))
			{
                if (m_ModHandlerImpl)
                {
					auto createdModification = m_ModHandlerImpl->GetCreatedObjectModification(update.GetPrimaryKey());
					if (nullptr != createdModification)
					{
						row = createdModification->RestoreRow(row);
					}
					else
					{
						// We have an update error but can't display it in the grid.
						ASSERT(false);
					}
                }
			}
			ASSERT(nullptr != row);
			if (nullptr != row)
				OnSavingError(row, *error);
		}
		
		m_AppliedRows.insert(update.GetPrimaryKey());
    }
}

void GridUpdater::removeDeletionRows()
{
    if (m_ModHandlerImpl && !m_Options.IsFullPurge())
    {
		auto deletedMods = m_ModHandlerImpl->GetDeletionRowModifications();
        for (const auto& mod : deletedMods)
        {
            for (auto& update : m_UpdateOperations)
            {
                // Is it the modification we're looking for?
                if (mod->IsCorrespondingRow(update.GetPrimaryKey()))
                {
                    // If not an error, remove row
					bool isError = false;
					for (const auto& result : update.GetResults())
						isError = result.m_Error.is_initialized() || isError;

					bool removed = false;
                    if (!isError)
                    {
                        GridBackendRow* row = m_Grid.GetRowIndex().GetExistingRow(update.GetPrimaryKey());
						if (nullptr != row)
						{
							if (row->IsExplosionFragment())
							{
								ASSERT(!row->IsExplosionAdded());
								m_Grid.RemoveRowsAndTagAnnotations(m_Grid.GetMultivalueManager().GetExplosionSiblings(row));
								removed = true;
							}
							else
							{
								std::unordered_set<GridBackendRow*> rowsToRemove;
								for (auto child : m_Grid.GetChildRows(row))
									rowsToRemove.insert(child);
								rowsToRemove.insert(row);
								m_Grid.RemoveRowsAndTagAnnotations(rowsToRemove);
								removed = true;
							}
						}
                    }

					// FIXME: We should probably break without condition. Is it possible a mod corresponds to several UpdateOperation?
					if (removed)
					{
						// mod has been destroyed by O365Grid::removeRowPreprocess, indirectly called by above RemoveRowsAndTagAnnotations.
						// So break this loop to jump to the next mod and avoid using a dangling pointer...
						break;
					}
                }
            }
        }
    }
}

void GridUpdater::refreshModificationsStatus()
{
	if (m_ModHandlerImpl)
		m_ModHandlerImpl->RefreshModificationsStatus(*this);
}

void GridUpdater::refreshSubItemModificationsStates()
{
	if (m_ModHandlerImpl)
		m_ModHandlerImpl->RefreshSubItemModificationsStates(*this);
}

void GridUpdater::refreshCollapsedRowsErrors()
{
	const auto& flagger = m_Grid.GetRowFlagger();
	if (flagger)
	{
		// Shouldn't happen with OnPrem mods... As is, it doesn't work.
		ASSERT(nullptr != dynamic_cast<GridUpdater::ModHandler<GridModificationsO365>*>(m_ModHandlerImpl.get()));

		// Add error icons for collapsed rows
		const auto& rows = m_Grid.GetBackendRows();
		for (const auto& row : rows)
		{
			if (!row->IsExplosionFragment())
			{
				const auto error = flagger->IsFlaggedAsError(row, nullptr);
				if (!error.empty())
					m_Grid.OnRowError(row, false, error);
			}
		}
	}
}

void GridUpdater::handlePostUpdateError()
{
	if (m_PostUpdateError.m_HasAtLeastOneError)
	{
		ASSERT(m_PostUpdateError.m_ErrorOccuredDuringSave || GetOptions().IsShowLoadingErrorDialog());
		m_Grid.HandlePostUpdateError(m_PostUpdateError.m_HasAtLeastOne403Error, m_PostUpdateError.m_ErrorOccuredDuringSave, m_PostUpdateError.m_HasErrorDescription ? m_PostUpdateError.m_ErrorDescription : _YTEXT(""));
		m_PostUpdateError = PostUpdateError();
	}
	else
	{
		m_Grid.HandlePostUpdateNoError(!m_UpdateOperations.empty() || !m_Options.IsShowLoadingErrorDialog());
	}
}

void GridUpdater::onError(GridBackendRow* p_Row, const SapioError& p_Error, bool p_ErrorOccuredDuringSave)
{
    onError(p_Row, p_Error, p_ErrorOccuredDuringSave, _YTEXT(""));
}

void GridUpdater::onError(GridBackendRow* p_Row, const SapioError& p_Error, bool p_ErrorOccuredDuringSave, const wstring& p_ErrorPrefix)
{
	if (p_ErrorOccuredDuringSave || GetOptions().IsShowLoadingErrorDialog())
	{
		m_PostUpdateError.m_HasAtLeastOneError = true;

		if (p_Error.GetStatusCode() == web::http::status_codes::Forbidden)
			set403Error(p_ErrorOccuredDuringSave);
		else
			m_PostUpdateError.m_ErrorOccuredDuringSave = p_ErrorOccuredDuringSave;

		setErrorDescription(p_Error.GetFullErrorMessage(), p_ErrorOccuredDuringSave);
	}

	// Ark
#define SET_ERROR(_Scope) \
	/* Keep existing saved or error status that may be the result of apply. */ \
	if (nullptr != p_Row && !O365Grid::RowStatusHandler<_Scope>(m_Grid).IsChangesSaved(p_Row) && !O365Grid::RowStatusHandler<_Scope>(m_Grid).IsError(p_Row)) \
		O365Grid::RowStatusHandler<_Scope>(m_Grid).SetRowError(p_Row, p_Error, p_ErrorPrefix); \

	if (!m_ModHandlerImpl || Scope::O365 == m_ModHandlerImpl->GetScope())
	{
		SET_ERROR(Scope::O365);
	}
	else if (Scope::ONPREM == m_ModHandlerImpl->GetScope())
	{
		SET_ERROR(Scope::ONPREM);
	}

#undef SET_ERROR
}

void GridUpdater::set403Error(bool p_ErrorOccuredDuringSave)
{
	if (p_ErrorOccuredDuringSave || GetOptions().IsShowLoadingErrorDialog())
	{
		m_PostUpdateError.m_ErrorOccuredDuringSave = p_ErrorOccuredDuringSave;
		m_PostUpdateError.m_HasAtLeastOneError = true;
		m_PostUpdateError.m_HasAtLeastOne403Error = true;
	}
}

void GridUpdater::set403Error(bool p_ErrorOccuredDuringSave, const wstring& p_ErrorDescription)
{
	set403Error(p_ErrorOccuredDuringSave);
	setErrorDescription(p_ErrorDescription, p_ErrorOccuredDuringSave);
}

void GridUpdater::setErrorDescription(const wstring& p_ErrorDescription, bool p_ErrorOccuredDuringSave)
{	
	if (p_ErrorOccuredDuringSave || GetOptions().IsShowLoadingErrorDialog())
	{
		if (!m_PostUpdateError.m_HasErrorDescription && !p_ErrorDescription.empty())
		{
			m_PostUpdateError.m_HasErrorDescription = true;
			m_PostUpdateError.m_ErrorDescription = p_ErrorDescription;
		}
		else if (p_ErrorDescription.empty())
		{
			m_PostUpdateError.m_HasErrorDescription = false;
		}
	}
}

GridUpdaterOptions& GridUpdater::GetOptions()
{
	return m_Options;
}

void GridUpdater::AddUpdatedRowPk(row_pk_t p_PK)
{
	// Useless otherwise.
	ASSERT(m_Grid.IsGridModificationsEnabled());

	m_UpdatedRows.insert(p_PK);
}

bool GridUpdater::IsUpdatedRowPK(row_pk_t p_PK) const
{
	return m_UpdatedRows.end() != m_UpdatedRows.find(p_PK);
}

bool GridUpdater::HasUpdatedRowPk() const
{
	return !m_UpdatedRows.empty();
}

bool GridUpdater::IsAppliedRowPK(row_pk_t p_PK) const
{
	return m_AppliedRows.end() != m_AppliedRows.find(p_PK);
}

bool GridUpdater::HasAppliedRowPk() const
{
	return !m_AppliedRows.empty();
}

void GridUpdater::HandleCreatedModifications(O365Grid& p_Grid, std::vector<O365UpdateOperation>& p_UpdateOperations)
{
	if (p_Grid.IsGridModificationsEnabled())
	{
		// Deal with created objects
		{
			for (auto& update : p_UpdateOperations)
			{
				boost::YOpt<wstring> successfulCreationBody;
				for (const auto& result : update.GetResults())
				{
					if (result.m_ResultInfo)
					{
                        auto statusCode = result.m_ResultInfo->Response.status_code();
                        if (statusCode == http::status_codes::Created)
                        {
                            successfulCreationBody = result.m_ResultInfo->GetBodyAsString();
                            break;
                        }
					}
				}

				if (successfulCreationBody) // Is this a http 'Created' success?
				{
					if (p_Grid.GetModifications().GetRowModification<CreatedObjectModification>(update.GetPrimaryKey()))
					{
						ASSERT(update.GetResults().size() == 1);

						row_pk_t newPk;

						newPk = p_Grid.UpdateCreatedModification(update.GetPrimaryKey(), *successfulCreationBody);

						ASSERT(!newPk.empty());
						if (!newPk.empty())
						{
							if (update.GetPrimaryKey() != newPk)
							{
								auto row = p_Grid.GetRowIndex().GetExistingRow(update.GetPrimaryKey());
								ASSERT(nullptr != row);
								p_Grid.RemoveRow(row);
							}
							update = O365UpdateOperation(update, newPk);
						}
					}
				}
			}
		}
	}
}

void GridUpdater::InitPostUpdateError(const PostUpdateError& p_PostUpdateError)
{
	if (p_PostUpdateError.m_HasAtLeastOneError)
		m_PostUpdateError = p_PostUpdateError;
}

void GridUpdater::SetPostUpdateErrorBackupTarget(PostUpdateError* p_PostUpdateErrorBackup)
{
	m_PostUpdateErrorBackup = p_PostUpdateErrorBackup;
}

GridBackendRow* GridUpdater::GetExistingRow(const vector<GridBackendField>& p_PK)
{
	// "refresh all" with "load more" data: the purge flag eliminates rows from the grid row index when filling basic data; need to search this updater - Bug #190628.EH.00A182 (R)

	const auto& rows = GetOptions().GetRowsWithRefreshedValues();	

	for (auto r : rows)
	{
		if (p_PK == m_Grid.GetRowPKNoDynamicColumns(*r))
			return r;
	}

	return nullptr;
}
