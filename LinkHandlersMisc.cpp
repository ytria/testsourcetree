#include "LinkHandlersMisc.h"

#include "MainFrame.h"

void LinkHandlerJobCenter::Handle(YBrowserLink& p_Link) const
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		mainFrame->showBackstageTab(ID_JOBCENTERCFG);
}
