#include "ISessionsMigrationVersion.h"

#include "MigrationUtil.h"
#include "SessionsSqlEngine.h"

namespace
{
	class ConcreteSessionsSqlEngine : public SessionsSqlEngine
	{
	public:
		ConcreteSessionsSqlEngine(uint32_t p_Version, const wstring& p_DBfile)
			: SessionsSqlEngine(p_DBfile)
			, m_Version(p_Version)
		{

		}

		uint32_t GetVersion() const override
		{
			return m_Version;
		}

		const uint32_t m_Version;
	};
}

SessionsSqlEngine& ISessionsMigrationVersion::GetSourceSQLEngine()
{
	ASSERT(GetVersionInfo().m_SourceVersion > 0);
	if (!m_SourceEngine)
		m_SourceEngine = std::make_unique<ConcreteSessionsSqlEngine>(GetVersionInfo().m_SourceVersion, MigrationUtil::GetMigrationFileName(GetVersionInfo().m_SourceVersion));
	return *m_SourceEngine;
}

SessionsSqlEngine& ISessionsMigrationVersion::GetTargetSQLEngine()
{
	if (!m_TargetEngine)
		m_TargetEngine = std::make_unique<ConcreteSessionsSqlEngine>(GetVersionInfo().m_TargetVersion, MigrationUtil::GetMigrationFileName(GetVersionInfo().m_TargetVersion));
	return *m_TargetEngine;
}
