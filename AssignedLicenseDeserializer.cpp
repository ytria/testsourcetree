#include "AssignedLicenseDeserializer.h"

#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "YtriaFieldsConstants.h"

void AssignedLicenseDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("skuId"), std::bind(&BusinessAssignedLicense::SetSkuId, &m_Data, std::placeholders::_1), p_Object, false);
    ListStringDeserializer svd;
    if (JsonSerializeUtil::DeserializeAny(svd, _YTEXT("disabledPlans"), p_Object))
        m_Data.m_AssignedLicense.m_DisabledPlans = svd.GetData();
	JsonSerializeUtil::DeserializeString(_YTEXT(YTRIA_BUSINESSASSIGNEDLICENSE_SKUPARTNUMBER), m_Data.m_SkuPartNumber, p_Object);
}

