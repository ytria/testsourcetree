#pragma once

#include "HttpResultWithError.h"
#include "ISubItemModification.h"
#include "SubElementPrefixFormatter.h"
#include "SubItemKey.h"

#include <map>

class O365Grid;

class ISubItemDeletion : public ISubItemModification
{
public:
    ISubItemDeletion(O365Grid& p_Grid, const SubItemKey& p_Key, GridBackendColumn* p_ColSubItemElder, GridBackendColumn* p_ColSubItemPrimaryKey, const wstring& p_ObjectName);
	virtual ~ISubItemDeletion() = default;

    void ShowAppliedLocally(GridBackendRow* p_Row) const override;
    virtual bool Refresh(GridBackendRow* p_Row, const std::map<SubItemKey, HttpResultWithError>& p_Errors) override;

	void SetState(Modification::State p_State);

	virtual GridBackendRow* GetMainRowIfCollapsed() const = 0;

private:
    void AddFormatterForCollapsedRow(GridBackendRow* p_Row) const;
    void AddFormatterForExpandedRow(GridBackendRow* p_Row) const;
    void CreateOrUpdateFormatter(GridBackendRow* p_Row, size_t p_Index) const;

    std::unique_ptr<SubElementPrefixFormatter> CreatePrefixFormatter() const;

    static const size_t g_FormatterId = 1;
};

