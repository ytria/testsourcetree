#pragma once

#include "DlgFormsHTML.h"
#include "EducationUser.h"

class DlgEducationUserEditHTML : public DlgFormsHTML
{
public:
	DlgEducationUserEditHTML(vector<EducationUser>& p_Users, DlgFormsHTML::Action p_Action, CWnd* p_Parent);

protected:
	void generateJSONScriptData() override;
	wstring getDialogTitle() const override;
	bool processPostedData(const IPostedDataTarget::PostedData& properties) override;

private:
	using StringGetter = std::function<const boost::YOpt<PooledString>& (const EducationUser&)>;
	using DateGetter = std::function<const boost::YOpt<YTimeDate>& (const EducationUser&)>;
	using StringSetter = std::function<void(EducationUser&, const boost::YOpt<PooledString>&)>;
	using DateSetter = std::function<void(EducationUser&, const boost::YOpt<YTimeDate>&)>;

	// If p_MaxDate contains an empty string, today's date is used
	void addDateEditor(DateGetter p_Getter, DateSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const boost::YOpt<wstring>& p_MaxDate);
	void addStringEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags);
	void addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues);

	bool hasProperty(const wstring& p_PropertyName) const;

	vector<EducationUser>& m_Users;
	std::map<wstring, StringSetter> m_StringSetters;
	std::map<wstring, DateSetter> m_DateSetters;

	bool m_AllStudents;
	bool m_AllTeachers;
};

