#include "DlgUsersModuleOptions.h"

#include "BusinessUserConfiguration.h"
#include "MsGraphFieldNames.h"
#include "RoleDelegationManager.h"

void DlgUsersModuleOptions::generateJSONScriptData()
{
	initMain(_YTEXT("DlgUsersModuleOptions"));

	getGenerator().addIconTextField(_YTEXT("iconText"), _T("Limit the number of users to load by applying some filters. You can request more later."), _YTEXT("fas fa-users-cog"));

	addFilterEditor(true);

	getGenerator().addApplyButton(YtriaTranslate::Do(CDlg_OnInitDialog_2, _YLOC("Ok")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str());
}

wstring DlgUsersModuleOptions::getDialogTitle() const
{
	return _T("Load Users - Options");
}

std::map<wstring, DlgModuleOptions::FilterFieldProperties> DlgUsersModuleOptions::getFilterFields() const
{
	return
	{
			{ _YTEXT(O365_USER_ACCOUNTENABLED), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_ACCOUNTENABLED)), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } } // bool
		,	{ _YTEXT(O365_USER_CITY), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_CITY)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_COUNTRY), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_COUNTRY)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_DEPARTMENT), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_DEPARTMENT)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_DISPLAYNAME), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_DISPLAYNAME)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_EMPLOYEEID), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_EMPLOYEEID)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_EXTERNALUSERSTATUS), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_EXTERNALUSERSTATUS)), true, FilterFieldProperties::EnumType({ { _YTEXT("PendingAcceptance"), _T("Pending Acceptance") }, { _YTEXT("Accepted"), _T("Accepted") } }), g_FilterOperatorsNoStartWith } }
		,	{ _YTEXT(O365_USER_GIVENNAME), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_GIVENNAME)), true, FilterFieldProperties::StringType(), {} } }
		// O365_USER_IDENTITIES // TODO - complex
		,	{ _YTEXT(O365_USER_JOBTITLE), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_JOBTITLE)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_MAIL), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_MAIL)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_MAILNICKNAME), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_MAILNICKNAME)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_ONPREMISESIMMUTABLEID), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_ONPREMISESIMMUTABLEID)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_OTHERMAILS), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_OTHERMAILS)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_PROXYADDRESSES), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_PROXYADDRESSES)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_STATE), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_STATE)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_SURNAME), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_SURNAME)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_USAGELOCATION), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_USAGELOCATION)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_USERPRINCIPALNAME), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_USERPRINCIPALNAME)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_USER_USERTYPE), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_USERTYPE)), true, FilterFieldProperties::StringType(), {} } }
	};
}