#include "DlgSelectColumns.h"
#include "Resource.h"

const wstring DlgSelectColumns::g_AutomationName = _YTEXT("");

BEGIN_MESSAGE_MAP(DlgSelectColumns, ResizableDialog)
	ON_BN_CLICKED(IDC_DLG_SELECT_COLUMNS_BTN_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DLG_SELECT_COLUMNS_BTN_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_DLG_SELECT_COLUMNS_CHECKBOX, OnCheck)
	ON_EN_CHANGE(IDC_DLG_SELECT_COLUMNS_FILTER_EDIT, OnFilterChange)
	ON_BN_CLICKED(IDC_DLG_SELECT_COLUMNS_FILTER_CLEAR, OnFilterClear)
END_MESSAGE_MAP()

namespace
{
	struct Customizer : public ColumnListGrid::ICustomizer
	{
		Customizer(std::function<bool(GridBackendColumn*)> p_ColumnFilter)
			: m_ColumnFilter(p_ColumnFilter)
			, m_ColumnLoadMore(nullptr)
			, m_ColumnDefault(nullptr)
			, m_ColumnTechnical(nullptr)
			, m_IconLoadMore(NO_ICON)
			, m_IconDefault(NO_ICON)
			, m_IconTechnical(NO_ICON)
		{

		}

		void CustomizeGrid(ColumnListGrid& p_Grid) override
		{
			m_IconLoadMore = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(RPROD_BMP_COLINFO_LOADMORE));
			m_IconDefault = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(RPROD_BMP_COLINFO_DEFAULT));
			m_IconTechnical = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(RPROD_BMP_COLINFO_TECHNICAL));

			m_ColumnDefault = p_Grid.AddColumnIcon(_YUID("DEFAULT"), YtriaTranslate::Do(DlgColumnInfo__ColumnGrid_customizeGrid_4, _YLOC("Is In Default Setup")).c_str(), GridBackendUtil::g_DummyString);
			m_ColumnLoadMore = p_Grid.AddColumnIcon(_YUID("LOADMORE"), YtriaTranslate::Do(DlgColumnInfo__ColumnGrid_customizeGrid_5, _YLOC("Load More Info")).c_str(), GridBackendUtil::g_DummyString);
			m_ColumnTechnical = p_Grid.AddColumnIcon(_YUID("TECHNICAL"), YtriaTranslate::Do(DlgColumnInfo__ColumnGrid_customizeGrid_6, _YLOC("Contains Technical Data")).c_str(), GridBackendUtil::g_DummyString);
		}

		bool ShouldInclude(GridBackendColumn* p_Column) override
		{
			return m_ColumnFilter(p_Column);
		}

		void AddCustomFields(GridBackendRow* p_Row, GridBackendColumn* p_Column) override
		{
			ASSERT(m_ColumnFilter(p_Column));

			auto grid = p_Column->GetGridBackend()->GetCacheGrid();
			const auto isDefault	= grid->IsColumnDefault(p_Column);
			const auto isLoadMore	= grid->IsColumnLoadMore(p_Column);
			const auto isTechnical	= grid->IsColumnTechnical(p_Column);
			p_Row->AddField(isDefault ? YtriaTranslate::Do(DlgColumnInfo__ColumnGrid_fillGrid_6, _YLOC("Column is included in main view by default")).c_str() : _YTEXT(""), m_ColumnDefault, isDefault ? m_IconDefault : NO_ICON);
			p_Row->AddField(isLoadMore ? YtriaTranslate::Do(DlgColumnInfo__ColumnGrid_fillGrid_7, _YLOC("This column is affected by the �Load Info� button.\nIt has additional info that can be loaded from the server by clicking the �Load Info� button (Ctrl-L)")).c_str() : _YTEXT(""), m_ColumnLoadMore, isLoadMore ? m_IconLoadMore : NO_ICON);
			p_Row->AddField(isTechnical ? YtriaTranslate::Do(DlgColumnInfo__ColumnGrid_fillGrid_8, _YLOC("Columns is of a technical nature.\nCan contain values that act as unique identifiers;\noften used with PowerShell, Graph API etc...")).c_str() : _YTEXT(""), m_ColumnTechnical, isTechnical ? m_IconTechnical : NO_ICON);
		}

	private:
		std::function<bool(GridBackendColumn*)> m_ColumnFilter;

		GridBackendColumn* m_ColumnLoadMore;
		GridBackendColumn* m_ColumnDefault;
		GridBackendColumn* m_ColumnTechnical;

		int m_IconLoadMore;
		int m_IconDefault;
		int m_IconTechnical;
	};
}

DlgSelectColumns::DlgSelectColumns(const vector<GridBackendColumn*>& p_SelectedColumns, std::function<bool(GridBackendColumn*)> p_ColumnFilter, CacheGrid* p_ParentGrid)
	: ResizableDialog(IDD_DLG_SELECT_COLUMNS, p_ParentGrid, g_AutomationName)
	, GridCrust(_YTEXT(""), std::make_unique<Customizer>(p_ColumnFilter))
	, m_MotherGrid(p_ParentGrid)
	, m_EnableIfEmpty(false)
	, m_SelectedColumns(p_SelectedColumns)
	, m_Check(false)
	, m_HideCancel(false)
	, m_ShouldCheckOnFirstAdditionIfEmpty(false)
{
	registerGrid(_YTEXT(""), &m_GridSelectedColumns);
}

DlgSelectColumns::~DlgSelectColumns()
{

}

CacheGrid* DlgSelectColumns::GetMotherGrid() const
{
	return m_MotherGrid;
}

const vector<GridBackendColumn*>& DlgSelectColumns::GetSelectedColumns() const
{
	return m_SelectedColumns;
}

void DlgSelectColumns::SetTitle(const wstring& p_Title)
{
	m_Title = p_Title;
}

void DlgSelectColumns::SetIntroText(const wstring& p_IntroText)
{
	m_IntroText = p_IntroText;
}

void DlgSelectColumns::SetCancelButtonText(const wstring& p_CancelText)
{
	m_CustomCancelText = p_CancelText;
}

void DlgSelectColumns::SetOkButtonText(const wstring& p_OkText)
{
	m_CustomOkText = p_OkText;
}

void DlgSelectColumns::SetHideCancelButton(bool p_Hide)
{
	m_HideCancel = p_Hide;
}

void DlgSelectColumns::SetVerificationText(const wstring& p_VerificationText, bool p_DefaultState, bool p_CheckOnFirstAdditionIfEmpty)
{
	ASSERT(p_VerificationText.empty() ||  m_ResetText.empty());
	ASSERT(!p_CheckOnFirstAdditionIfEmpty || !p_DefaultState);
	m_VerificationText = p_VerificationText;
	m_Check = p_DefaultState;
	m_ShouldCheckOnFirstAdditionIfEmpty = p_CheckOnFirstAdditionIfEmpty;
}

bool DlgSelectColumns::IsVerificationChecked() const
{
	return !m_VerificationText.empty() && m_Check;
}

void DlgSelectColumns::SetEnableOkIfNoSelection(bool p_EnableIfEmpty)
{
	m_EnableIfEmpty = p_EnableIfEmpty;
}

void DlgSelectColumns::DoDataExchange(CDataExchange* pDX)
{
	ResizableDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_DLG_SELECT_COLUMNS_SRC_GROUPBOX, m_GroupBoxSrc);
	DDX_Control(pDX, IDC_DLG_SELECT_COLUMNS_DEST_GROUPBOX, m_GroupBoxDest);
	DDX_Control(pDX, IDCANCEL, m_BtnCancel);
	DDX_Control(pDX, IDOK, m_BtnOK);
	DDX_Control(pDX, IDC_DLG_SELECT_COLUMNS_BTN_ADD, m_BtnAdd);
	DDX_Control(pDX, IDC_DLG_SELECT_COLUMNS_BTN_REMOVE, m_BtnRemove);
	DDX_Control(pDX, IDC_DLG_SELECT_COLUMNS_TEXT, m_Text);
	DDX_Control(pDX, IDC_DLG_SELECT_COLUMNS_CHECKBOX, m_Checkbox);
	DDX_Control(pDX, IDC_DLG_SELECT_COLUMNS_FILTER_EDIT, m_FilterEdit);
	DDX_Control(pDX, IDC_DLG_SELECT_COLUMNS_FILTER_CLEAR, m_FilterClear);
}

BOOL DlgSelectColumns::OnInitDialogSpecificResizable()
{
	if (m_Title.empty())
		SetWindowText(CString("Select Columns"));
	else
		SetWindowText(CString(m_Title.c_str()));

	if (m_IntroText.empty())
		m_Text.ShowWindow(SW_HIDE);
	else
		m_Text.SetWindowText(CString(m_IntroText.c_str()));

	m_FilterClear.SetWindowText(_YTEXT(""));
	m_FilterClear.SetBitmap({16, 16}, RPROD_BMP_RIBBON_CACHEGRID_MENU_CLEARFILTER_16X16);
	m_FilterClear.SetTooltip(YtriaTranslate::Do(CacheGrid_InitializeCommands_381, _YLOC("Clear")).c_str());
	m_FilterEdit.SetCueBanner(YtriaTranslate::Do(DlgSelectColumns_OnInitDialogSpecificResizable_5, _YLOC("Filter columns")).c_str());
	m_FilterEdit.SetTooltipText(YtriaTranslate::Do(DlgSelectColumns_OnInitDialogSpecificResizable_5, _YLOC("Filter columns")).c_str());

	m_GroupBoxSrc.SetWindowText(YtriaTranslate::Do(DlgDownloadAttachments_OnInitDialogSpecificResizable_9, _YLOC("Available columns")).c_str());
	m_GroupBoxDest.SetWindowText(YtriaTranslate::Do(ColumnSelectionGrid_customizeGrid_3, _YLOC("Selected columns")).c_str());

	if (m_CustomOkText.empty())
		m_BtnOK.SetWindowText(YtriaTranslate::Do(DlgGridCopySettings_OnInitDialog_12, _YLOC("OK")).c_str());
	else
		m_BtnOK.SetWindowText(CString(m_CustomOkText.c_str()));

	if (m_HideCancel)
	{
		ASSERT(m_CustomCancelText.empty());

		CRect rect;
		m_BtnCancel.GetWindowRect(rect);
		m_BtnCancel.ShowWindow(SW_HIDE);
		ScreenToClient(&rect);
		m_BtnOK.MoveWindow(rect);
	}
	else
	{
		if (m_CustomCancelText.empty())
			m_BtnCancel.SetWindowText(YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str());
		else
			m_BtnCancel.SetWindowText(CString(m_CustomCancelText.c_str()));
	}

	if (!m_VerificationText.empty())
	{
		m_Checkbox.ShowWindow(SW_SHOW);
		m_Checkbox.SetWindowText(CString(m_VerificationText.c_str()));
		m_Checkbox.SetCheck(m_Check ? BST_CHECKED : BST_UNCHECKED);
	}

	setButtonArrayBitmap(m_BtnAdd, true);
	setButtonArrayBitmap(m_BtnRemove, false);

	float gridWidthRatio = .625f;
	CRect aRect;
	CWnd* gridPlacement = GetDlgItem(IDC_DLG_SELECT_COLUMNS_DEST_GRID);
	ASSERT(nullptr != gridPlacement);
	if (nullptr != gridPlacement)
	{
		gridPlacement->GetWindowRect(&aRect);
		ScreenToClient(&aRect);
		if (!m_GridSelectedColumns.Create(this, aRect))
		{
			// Creation of grid failed.
			ASSERT(FALSE);
			return -1;
		}
		m_GridSelectedColumns.SetColumnSize(gridWidthRatio);
		m_GridSelectedColumns.UpdateMegaShark();
	}

	gridPlacement = GetDlgItem(IDC_DLG_SELECT_COLUMNS_SRC_GRID);
	ASSERT(nullptr != gridPlacement);
	if (nullptr != gridPlacement)
	{
		gridPlacement->GetWindowRect(&aRect);
		ScreenToClient(&aRect);
		if (!GetGridColumns().Create(this, aRect))
		{
			// Creation of grid failed.
			ASSERT(FALSE);
			return -1;
		}
		gridWidthRatio = 1.25f;
		GetGridColumns().SetColumnSize(gridWidthRatio);
	}

	GetGridColumns().SetWithCompareColumns(false);
	GetGridColumns().InitGrid(m_MotherGrid);
	GetGridColumns().ShowHiddenColumns(true, true);

	m_GridSelectedColumns.ShowWindow(SW_SHOW);
	m_GridSelectedColumns.BringWindowToTop();

	GetGridColumns().ShowWindow(SW_SHOW);
	GetGridColumns().BringWindowToTop();
	if (nullptr != gridPlacement)
	{
		// Force resize to trigger column auto size update.
		aRect.InflateRect(0, 0, 5, 0);
		GetGridColumns().MoveWindow(aRect);
		aRect.InflateRect(0, 0, -5, 0);
		GetGridColumns().MoveWindow(aRect);
	}

	m_GridSelectedColumns.AddColumnRows(m_SelectedColumns, false);
	m_SelectedColumns.clear();
	updateOkButton();

	AddAnchor(m_Text, CSize(0, 0), CSize(100, 0));
	AddAnchor(m_FilterEdit, CSize(0, 0), CSize(50, 0));
	AddAnchor(m_FilterClear, CSize(50, 0), CSize(50, 0));
	AddAnchor(GetGridColumns(), CSize(0, 0), CSize(50, 100));
	AddAnchor(m_GroupBoxSrc, CSize(0, 0), CSize(50, 100));
	AddAnchor(m_GridSelectedColumns, CSize(50, 0), CSize(100, 100));
	AddAnchor(m_GroupBoxDest, CSize(50, 0), CSize(100, 100));
	AddAnchor(m_BtnAdd, CSize(50, 50), CSize(50, 50));
	AddAnchor(m_BtnRemove, CSize(50, 50), CSize(50, 50));
	AddAnchor(m_BtnOK, __RDA_XY, __RDA_XY);
	AddAnchor(m_BtnCancel, __RDA_XY, __RDA_XY);
	AddAnchor(m_Checkbox, CSize(0, 100), CSize(100, 100));

	SetBkColor(RGB(255, 255, 255));
	m_Text.SetBkColor(RGB(255, 255, 255));
	m_GroupBoxSrc.SetBkColor(RGB(255, 255, 255));
	m_GroupBoxDest.SetBkColor(RGB(255, 255, 255));
	m_Checkbox.SetBkColor(RGB(255, 255, 255));
	m_BtnAdd.SetBkColor(RGB(255, 255, 255));
	m_BtnRemove.SetBkColor(RGB(255, 255, 255));
	m_BtnAdd.EnableWindow(FALSE);
	m_BtnRemove.EnableWindow(FALSE);

	GetGridColumns().AddDoubleClickObserver(this);
	m_GridSelectedColumns.AddDoubleClickObserver(this);

	return FALSE;
}

void DlgSelectColumns::OnOK()
{
	for (auto& row : m_GridSelectedColumns.GetBackendRows())
	{
		ASSERT(row != nullptr);
		if (row != nullptr && !row->IsGroupRow())
			m_SelectedColumns.push_back((GridBackendColumn*)row->GetLParam());
	}

	ResizableDialog::OnOK();
}

void DlgSelectColumns::OnAdd()
{
	auto wasEmpty = m_GridSelectedColumns.GetBackendRows().empty();

	vector<GridBackendRow*> rows;
	GetGridColumns().GetSelectedNonGroupRows(rows); // Don't use GetSelectedRows() to preserve order

	vector<GridBackendColumn*> columns;
	for (auto row : rows)
	{
		ASSERT(row != nullptr);
		if (row != nullptr && !row->IsGroupRow() && !m_GridSelectedColumns.HasColumn((GridBackendColumn*)row->GetLParam()))
			columns.push_back((GridBackendColumn*)row->GetLParam());
	}

	m_GridSelectedColumns.AddColumnRows(columns, false);

	updateOkButton();

	if (wasEmpty && m_ShouldCheckOnFirstAdditionIfEmpty && !m_Check)
	{
		m_ShouldCheckOnFirstAdditionIfEmpty = false;
		m_Checkbox.SetCheck(BST_CHECKED);
	}
}

void DlgSelectColumns::OnRemove()
{
	vector<GridBackendColumn*> columns;
	for (auto row : m_GridSelectedColumns.GetSelectedRows())
	{
		ASSERT(row != nullptr);
		if (row != nullptr && !row->IsGroupRow())
			columns.push_back((GridBackendColumn*)row->GetLParam());
	}

	if (!columns.empty())
		m_GridSelectedColumns.RemoveColumnRows(columns);

	updateOkButton();

	UpdateFromGrid(&m_GridSelectedColumns);
}

void DlgSelectColumns::OnCheck()
{
	m_Check = m_Checkbox.GetCheck() == BST_CHECKED;
}

void DlgSelectColumns::OnFilterClear()
{
	m_FilterEdit.SetWindowText(CString());
}

void DlgSelectColumns::OnFilterChange()
{
	CString csFilter;
	m_FilterEdit.GetWindowText(csFilter);
	GetGridColumns().Filter((LPCTSTR)csFilter);
	m_FilterEdit.SetFocus();
}

void DlgSelectColumns::UpdateFromGrid(CacheGrid* i_FromGrid /*= nullptr*/)
{
	if (i_FromGrid == &GetGridColumns())
		m_BtnAdd.EnableWindow(i_FromGrid->GetSelectedRows().empty() ? FALSE : TRUE);
	else if (i_FromGrid == &m_GridSelectedColumns)
		m_BtnRemove.EnableWindow(i_FromGrid->GetSelectedRows().empty() ? FALSE : TRUE);
}

void DlgSelectColumns::DoubleClicked(const CacheGrid& grid)
{
	if (&grid == &GetGridColumns())
		OnAdd();
	else if (&grid == &m_GridSelectedColumns)
		OnRemove();
}

void DlgSelectColumns::setupClearSpecific()
{
	// Anything?
}

const bool DlgSelectColumns::setupLoadSpecific(AutomationAction* i_settingsAction, list < wstring >& o_Errors)
{
	// Anything?
	return false;
}

void DlgSelectColumns::updateOkButton()
{
	m_BtnOK.EnableWindow(m_EnableIfEmpty || !m_GridSelectedColumns.GetBackendRows().empty());
}
