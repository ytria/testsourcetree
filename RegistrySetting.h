#pragma once

#include "ISetting.h"

class GenericRegistrySetting
{
public:
	virtual ~GenericRegistrySetting() = default;

protected:
	GenericRegistrySetting(const wstring& p_RegistryKey, const wstring& p_RegistryValue);

	std::pair<bool, wstring> readVal() const;
	bool writeVal(const wstring& p_Value) const;
	bool removeVal() const;

private:
	const wstring m_RegistryKey;
	const wstring m_RegistryValue;
};

template<typename T>
class RegistrySetting : public ISetting<T>, public GenericRegistrySetting
{
public:
	using GenericRegistrySetting::GenericRegistrySetting;
	RegistrySetting(const boost::YOpt<T> p_Default, const wstring& p_RegistryKey, const wstring& p_RegistryValue)
		: RegistrySetting(p_Default, p_RegistryKey, p_RegistryValue, true)
	{
	}

	boost::YOpt<T> Get() const override;
	bool Set(boost::YOpt<T> p_Val) const override;

protected:
	RegistrySetting(const boost::YOpt<T> p_Default, const wstring& p_RegistryKey, const wstring& p_RegistryValue, bool p_Init)
		: ISetting<T>(p_Default)
		, GenericRegistrySetting(p_RegistryKey, p_RegistryValue)
	{
		if (p_Init)
			init();
	}
};

template<typename T>
class RegistrySettingNoInit : public RegistrySetting<T>
{
public:
	RegistrySettingNoInit(const boost::YOpt<T> p_Default, const wstring& p_RegistryKey, const wstring& p_RegistryValue)
		: RegistrySetting(p_Default, p_RegistryKey, p_RegistryValue, false)
	{
	}
};
