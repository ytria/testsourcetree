#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessDrive.h"

class DriveDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessDrive>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

