#pragma once

#include "BusinessObject.h"
#include "DelegatePermissions.h"
#include "UserId.h"

namespace Ex
{
class DelegateUser : public BusinessObject
{
public:
    DelegateUser() = default;
    DelegateUser(const Ex::UserId& p_UserId);

    Ex::UserId m_UserId;
    boost::YOpt<Ex::DelegatePermissions> m_DelegatePermissions;
    boost::YOpt<bool> m_ReceiveCopiesOfMeetingMessages;
    boost::YOpt<bool> m_ViewPrivateItems;

private:
    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND
};
}
