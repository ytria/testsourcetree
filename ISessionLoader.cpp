#include "ISessionLoader.h"
#include "SessionsSqlEngine.h"

ISessionLoader::ISessionLoader(const SessionIdentifier& p_Id)
	: m_SessionId(p_Id)
{
}

const SessionIdentifier& ISessionLoader::GetSessionId() const
{
	return m_SessionId;
}

SessionIdentifier& ISessionLoader::GetSessionId()
{
	return m_SessionId;
}

PersistentSession ISessionLoader::GetPersistentSession(const std::shared_ptr<Sapio365Session>& p_SapioSession)
{
	OAuth2AuthenticatorBase* oauth2Auth = p_SapioSession->GetMainMSGraphSession()->GetAuthenticator().get();
	PersistentSession persistentSession;
	if (nullptr != oauth2Auth)
	{
		persistentSession = SessionsSqlEngine::Get().Load(m_SessionId);

		oauth2_token token;
		token.set_access_token(persistentSession.GetAccessToken());
		token.set_refresh_token(persistentSession.GetRefreshToken());
		oauth2Auth->LoadToken(GetSessionId().m_EmailOrAppId, token);
	}

	return persistentSession;
}
