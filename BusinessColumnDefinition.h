#pragma once

#include "BusinessObject.h"

#include "BooleanColumn.h"
#include "ChoiceColumn.h"
#include "CalculatedColumn.h"
#include "CurrencyColumn.h"
#include "DateTimeColumn.h"
#include "DefaultValueColumn.h"
#include "LookupColumn.h"
#include "NumberColumn.h"
#include "PersonOrGroupColumn.h"
#include "TextColumn.h"

#include "ColumnDefinition.h"

class BusinessColumnDefinition : public BusinessObject
{
public:
    BusinessColumnDefinition();
    BusinessColumnDefinition(const ColumnDefinition& p_Column);

    const boost::YOpt<PooledString>& GetColumnGroup() const;
    void SetColumnGroup(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<PooledString>& GetDescription() const;
    void SetDescription(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<PooledString>& GetDisplayName() const;
    void SetDisplayName(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<bool>& GetEnforceUniqueValues() const;
    void SetEnforceUniqueValues(const boost::YOpt<bool>& p_Val);
    
    const boost::YOpt<bool>& GetHidden() const;
    void SetHidden(const boost::YOpt<bool>& p_Val);

    const boost::YOpt<bool>& GetIndexed() const;
    void SetIndexed(const boost::YOpt<bool>& p_Val);
    
    const boost::YOpt<PooledString>& GetName() const;
    void SetName(const boost::YOpt<PooledString>& p_Val);
    
    const boost::YOpt<bool>& GetReadOnly() const;
    void SetReadOnly(const boost::YOpt<bool>& p_Val);
    
    const boost::YOpt<bool>& GetRequired() const;
    void SetRequired(const boost::YOpt<bool>& p_Val);

    const boost::YOpt<BooleanColumn>& GetBooleanFacet() const;
    void SetBooleanFacet(const boost::YOpt<BooleanColumn>& p_Val);
    
    const boost::YOpt<CalculatedColumn>& GetCalculatedFacet() const;
    void SetCalculatedFacet(const boost::YOpt<CalculatedColumn>& p_Val);
    
    const boost::YOpt<ChoiceColumn>& GetChoiceFacet() const;
    void SetChoiceFacet(const boost::YOpt<ChoiceColumn>& p_Val);
    
    const boost::YOpt<CurrencyColumn>& GetCurrencyFacet() const;
    void SetCurrencyFacet(const boost::YOpt<CurrencyColumn>& p_Val);
    
    const boost::YOpt<DateTimeColumn>& GetDateTimeFacet() const;
    void SetDateTimeFacet(const boost::YOpt<DateTimeColumn>& p_Val);
    
    const boost::YOpt<DefaultValueColumn>& GetDefaultValueFacet() const;
    void SetDefaultValueFacet(const boost::YOpt<DefaultValueColumn>& p_Val);
    
    const boost::YOpt<LookupColumn>& GetLookupFacet() const;
    void SetLookupFacet(const boost::YOpt<LookupColumn>& p_Val);
    
    const boost::YOpt<NumberColumn>& GetNumberFacet() const;
    void SetNumberFacet(const boost::YOpt<NumberColumn>& p_Val);
    
    const boost::YOpt<PersonOrGroupColumn>& GetPersonOrGroupFacet() const;
    void SetPersonOrGroupFacet(const boost::YOpt<PersonOrGroupColumn>& p_Val);
    
    const boost::YOpt<TextColumn>& GetTextFacet() const;
    void SetTextFacet(const boost::YOpt<TextColumn>& p_Val);

private:
    boost::YOpt<PooledString>        m_ColumnGroup;
    boost::YOpt<PooledString>        m_Description;
    boost::YOpt<PooledString>        m_DisplayName;
    boost::YOpt<bool>                m_EnforceUniqueValues;
    boost::YOpt<bool>                m_Hidden;
    boost::YOpt<bool>                m_Indexed;
    boost::YOpt<PooledString>        m_Name;
    boost::YOpt<bool>                m_ReadOnly;
    boost::YOpt<bool>                m_Required;

    boost::YOpt<BooleanColumn>       m_Boolean;
    boost::YOpt<CalculatedColumn>    m_Calculated;
    boost::YOpt<ChoiceColumn>        m_Choice;
    boost::YOpt<CurrencyColumn>      m_Currency;
    boost::YOpt<DateTimeColumn>      m_DateTime;
    boost::YOpt<DefaultValueColumn>  m_DefaultValue;
    boost::YOpt<LookupColumn>        m_Lookup;
    boost::YOpt<NumberColumn>        m_Number;
    boost::YOpt<PersonOrGroupColumn> m_PersonOrGroup;
    boost::YOpt<TextColumn>          m_Text;

    RTTR_ENABLE(BusinessObject)
        RTTR_REGISTRATION_FRIEND

        friend class ColumnDefinitionDeserializer;
};

