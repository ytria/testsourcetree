#pragma once

#include "GridUpdaterOptions.h"
#include "GridModifications.h"
#include "O365UpdateOperation.h"

#include <vector>

class GridBackendRow;
class HttpError;
class O365Grid;

class GridUpdater : public IRowFinder
{
public:
    GridUpdater(O365Grid& p_Grid, const GridUpdaterOptions& p_Options);
	GridUpdater(O365Grid& p_Grid, const GridUpdaterOptions& p_Options, const vector<O365UpdateOperation>& p_Updates);
	// Use this ctor if you want to handle an alternative set of GridModifications (ctor above use the generic O365Grid::GetGridModifications)
	GridUpdater(O365Grid& p_Grid, const GridUpdaterOptions& p_Options, const vector<O365UpdateOperation>& p_Updates, GridModificationsO365* p_GridModification);
	GridUpdater(O365Grid& p_Grid, const GridUpdaterOptions& p_Options, const vector<O365UpdateOperation>& p_Updates, GridFieldModificationsOnPrem* p_GridModification);
    ~GridUpdater();

	void OnLoadingError(GridBackendRow* p_Row, const SapioError& p_Error);
	void OnLoadingError(GridBackendRow* p_Row, const SapioError& p_Error, const wstring& p_ErrorPrefix);
	void Set403LoadingError();
	void Set403LoadingError(const wstring& p_ErrorDescription);

	void OnSavingError(GridBackendRow* p_Row, const SapioError& p_Error);
	void OnSavingError(GridBackendRow* p_Row, const SapioError& p_Error, const wstring& p_ErrorPrefix);
	void Set403SavingError();
	void Set403SavingError(const wstring& p_ErrorDescription);

	virtual GridBackendRow* GetExistingRow(const vector<GridBackendField>& p_PK) override;

private:
    void onError(GridBackendRow* p_Row, const SapioError& p_Error, bool p_ErrorOccuredDuringSave);
    void onError(GridBackendRow* p_Row, const SapioError& p_Error, bool p_ErrorOccuredDuringSave, const wstring& p_ErrorPrefix);
	void set403Error(bool p_ErrorOccuredDuringSave);
	void set403Error(bool p_ErrorOccuredDuringSave, const wstring& p_ErrorDescription);

public:
	GridUpdaterOptions& GetOptions();

	/* Those methods are only used if the grid is using GridModifications*/
	void AddUpdatedRowPk(row_pk_t p_PK);
	bool IsUpdatedRowPK(row_pk_t p_PK) const;
	bool HasUpdatedRowPk() const;
	bool IsAppliedRowPK(row_pk_t p_PK) const;
	bool HasAppliedRowPk() const;

	static void HandleCreatedModifications(O365Grid& p_Grid, std::vector<O365UpdateOperation>& p_UpdateOperations);

	struct PostUpdateError
	{
		bool m_HasAtLeastOneError = false;
		bool m_HasAtLeastOne403Error = false;
		bool m_HasErrorDescription = false;
		wstring m_ErrorDescription;
		bool m_ErrorOccuredDuringSave = false;
	};

	// If pointer is not null, it'll be used to store error status at the end of GridUpdater dtor, so that errors can be displayed later.
	void SetPostUpdateErrorBackupTarget(PostUpdateError* p_PostUpdateErrorBackup);

	// Use this to initialize with some error that could have been saved (see SetPostUpdateErrorBackupTarget) from another GridUpdater instance
	void InitPostUpdateError(const PostUpdateError& p_PostUpdateError);

private:
    void markErrorsForUpdates();
    void removeDeletionRows();
	void refreshModificationsStatus();
	void refreshSubItemModificationsStates();
	void refreshCollapsedRowsErrors();
	void handlePostUpdateError();
	void refreshProcessData();
	void processLoadMore();
	void processActions();
	void processExplosionSiblingUpdate();

	void setErrorDescription(const wstring& p_ErrorDescription, bool p_ErrorOccuredDuringSave);

    O365Grid& m_Grid;
    std::vector<O365UpdateOperation> m_UpdateOperations;

	PostUpdateError m_PostUpdateError;
	PostUpdateError* m_PostUpdateErrorBackup; // Will be used in dtor and hod a copy of m_PostUpdateError for the caller.

    GridUpdaterOptions m_Options;

	std::set<row_pk_t> m_UpdatedRows;
	std::set<row_pk_t> m_AppliedRows;

	struct IModHandlerImpl;
	std::unique_ptr<IModHandlerImpl> m_ModHandlerImpl;
	template<class T> struct ModHandler;
	template<class T> friend IModHandlerImpl* createImpl(T*);
};
