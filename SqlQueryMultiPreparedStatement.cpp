#include "SqlQueryMultiPreparedStatement.h"

#include "MFCUtil.h"
#include "SQLiteEngine.h"
#include "Str.h"
#include <exception>

SqlQueryMultiPreparedStatement::SqlQueryMultiPreparedStatement(SQLiteEngine& p_Engine, const std::vector<wstring>& p_Statements)
	: m_Engine(p_Engine)
	, m_StatementsWstring(p_Statements)
{
	m_Statements.reserve(m_StatementsWstring.size());
	for (const auto& statement : m_StatementsWstring)
	{
		ASSERT(Str::endsWith(statement, _YTEXT(";")));
		m_Statements.push_back(MFCUtil::convertUNICODE_to_UTF8(statement));
	}

	m_Stmts.resize(m_Statements.size());
	m_Bindings.resize(m_Statements.size());
}

SqlQueryMultiPreparedStatement::~SqlQueryMultiPreparedStatement()
{
	for (auto& stmt : m_Stmts)
		sqlite3_finalize(stmt);
}

void SqlQueryMultiPreparedStatement::Run()
{
	for (size_t i = 0; i < m_Statements.size(); ++i)
	{
		auto& statement = m_Statements[i];
		auto& stmt = m_Stmts[i];
		auto& bindings = m_Bindings[i];

		const int querySize = static_cast<int>(statement.size());
		m_Status = sqlite3_prepare_v2(m_Engine.GetDbHandle(), statement.c_str(), querySize, &stmt, nullptr);
		if (m_Status != SQLITE_OK)
		{
			ASSERT(false);
			m_Engine.ProcessError(m_Status, nullptr, m_StatementsWstring[i]);

			std::string err = std::string("SqlQueryMultiPreparedStatement: Malformed query: ") + statement;
			throw std::exception(err.c_str());
		}

		for (auto& b : bindings)
		{
			m_Status = b.second.bind(stmt, b.first);
			if (!IsSuccess())
				break;
		}

		if (IsSuccess())
			m_Status = m_Engine.sqlite3_step(stmt);

		if (!IsSuccess())
		{
			HandleError(i);
			break;
		}
	}
}

int SqlQueryMultiPreparedStatement::GetNbRowsChanged() const
{
	return sqlite3_changes(m_Engine.GetDbHandle()); // TODO: Not safe because of multithreading
}

void SqlQueryMultiPreparedStatement::BindString(size_t p_StatementIndex, int p_ParamIndex, const wstring& p_Value)
{
	ASSERT(p_StatementIndex < m_Stmts.size());
	if (p_StatementIndex < m_Stmts.size())
		m_Bindings[p_StatementIndex][p_ParamIndex] = Binding(p_Value);
}

void SqlQueryMultiPreparedStatement::BindInt(size_t p_StatementIndex, int p_ParamIndex, int p_Value)
{
	ASSERT(p_StatementIndex < m_Stmts.size());
	if (p_StatementIndex < m_Stmts.size())
		m_Bindings[p_StatementIndex][p_ParamIndex] = Binding(p_Value);
}

void SqlQueryMultiPreparedStatement::BindNull(size_t p_StatementIndex, int p_ParamIndex)
{
	ASSERT(p_StatementIndex < m_Stmts.size());
	if (p_StatementIndex < m_Stmts.size())
		m_Bindings[p_StatementIndex][p_ParamIndex] = Binding();
}

void SqlQueryMultiPreparedStatement::BindInt64(size_t p_StatementIndex, int p_ParamIndex, int64_t p_Value)
{
	ASSERT(p_StatementIndex < m_Stmts.size());
	ASSERT(p_StatementIndex < m_Stmts.size());
	if (p_StatementIndex < m_Stmts.size())
		m_Bindings[p_StatementIndex][p_ParamIndex] = Binding(p_Value);
}

void SqlQueryMultiPreparedStatement::BindBlob(size_t p_StatementIndex, int p_ParamIndex, const void* p_Data, int p_DataSize)
{
	ASSERT(p_StatementIndex < m_Stmts.size());
	ASSERT(p_StatementIndex < m_Stmts.size());
	if (p_StatementIndex < m_Stmts.size())
		m_Bindings[p_StatementIndex][p_ParamIndex] = Binding(p_Data, p_DataSize);
}

void SqlQueryMultiPreparedStatement::HandleError(size_t p_StatementIndex) const
{
	m_Engine.ProcessError(m_Status, nullptr, m_StatementsWstring[p_StatementIndex]);
}

SqlQueryMultiPreparedStatement::Binding::Binding(const wstring& p_Value)
	: m_Type(BIND_STRING)
	, p_StrValue(&p_Value)
{

}

SqlQueryMultiPreparedStatement::Binding::Binding(int p_Value)
	: m_Type(BIND_INT)
	, m_IntValue(p_Value)
{

}

SqlQueryMultiPreparedStatement::Binding::Binding()
	: m_Type(BIND_NULL)
{

}

SqlQueryMultiPreparedStatement::Binding::Binding(int64_t p_Value)
	: m_Type(BIND_INT64)
	, m_Int64Value(p_Value)
{

}

SqlQueryMultiPreparedStatement::Binding::Binding(const void* p_Data, int p_DataSize)
	: m_Type(BIND_BLOB)
	, m_Data(p_Data)
	, m_IntValue(p_DataSize)
{

}

int SqlQueryMultiPreparedStatement::Binding::bind(sqlite3_stmt* p_Stmt, int p_ParamIndex)
{
	switch (m_Type)
	{
	case BIND_STRING:
		ASSERT(nullptr != p_StrValue);
		if (nullptr == p_StrValue)
			break;
		return sqlite3_bind_text(p_Stmt, p_ParamIndex, MFCUtil::convertUNICODE_to_UTF8(*p_StrValue).c_str(), -1, SQLITE_TRANSIENT);
	case BIND_INT:
		return sqlite3_bind_int(p_Stmt, p_ParamIndex, m_IntValue);
	case BIND_NULL:
		return sqlite3_bind_null(p_Stmt, p_ParamIndex);
	case BIND_INT64:
		return sqlite3_bind_int64(p_Stmt, p_ParamIndex, m_Int64Value);
	case BIND_BLOB:
		return sqlite3_bind_blob(p_Stmt, p_ParamIndex, m_Data, m_IntValue, SQLITE_TRANSIENT);
	default:
		break;
	}
	ASSERT(false);
	return 0;
}
