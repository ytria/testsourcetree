#include "SitesRequester.h"
#include "SiteDeserializerFactory.h"
#include "IPropertySetBuilder.h"
#include "MSGraphUtil.h"
#include "MsGraphPaginator.h"
#include "PaginatorUtil.h"
#include "MsGraphHttpRequestLogger.h"

SitesRequester::SitesRequester(IPropertySetBuilder& p_Properties, const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_Properties(p_Properties.GetPropertySet()),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> SitesRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	auto factory = std::make_unique<SiteDeserializerFactory>(p_Session);
	m_Deserializer = std::make_shared<ValueListDeserializer<BusinessSite, SiteDeserializer>>(std::move(factory));

	web::uri_builder uri(_YTEXT("sites"));
	uri.append_query(_YTEXT("search"), _YTEXT("*"));
	uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(m_Properties));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Deserializer, m_Logger, httpLogger, p_TaskData.GetOriginator()).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

const vector<BusinessSite>& SitesRequester::GetData() const
{
	return m_Deserializer->GetData();
}
