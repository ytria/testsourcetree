#include "Sapio365Settings.h"

#include "FileUtil.h"
#include "RegistryPassword.h"

static const wstring g_RegPathUserSettings = _YTEXT("SOFTWARE\\Ytria\\sapio365\\User_Settings");

UseCacheSetting::UseCacheSetting()
    : RegistrySetting<bool>(g_RegPathUserSettings + _YTEXT("\\Cache"), _YTEXT("Use"))
{

}

AutoReloadSetting::AutoReloadSetting()
    : RegistrySetting<bool>(false, g_RegPathUserSettings + _YTEXT("\\NewWindow"), _YTEXT("AutoReloadExistingFrm"))
{

}

AutoLoadLastSessionSetting::AutoLoadLastSessionSetting()
    : RegistrySetting<bool>(true, g_RegPathUserSettings + _YTEXT("\\NewWindow"), _YTEXT("AutoLoadLastSession"))
{

}

HistoryModeSetting::HistoryModeSetting()
    : RegistrySetting<wstring>(boost::none, g_RegPathUserSettings + _YTEXT("\\NewWindow"), _YTEXT("HistoryMode"))
{

}

CreateNewFrameSetting::CreateNewFrameSetting(const wstring& p_FrameAutomationName)
    : RegistrySetting<bool>(boost::none, g_RegPathUserSettings + _YTEXT("\\NewWindow"), _YTEXT("OpenType") + p_FrameAutomationName)
{

}

HideNewWindowDialogSetting::HideNewWindowDialogSetting(const wstring& p_FrameAutomationName)
    : RegistrySetting<bool>(false, g_RegPathUserSettings + _YTEXT("\\NewWindow"), _YTEXT("NewWinDlg") + p_FrameAutomationName)
{

}

AllNewWindowKeySettings::AllNewWindowKeySettings()
    : RegistrySettingNoInit<bool>(boost::none, g_RegPathUserSettings + _YTEXT("\\NewWindow"), _YTEXT(""))
{

}

HideGroupAdminRestrictedAccessDialogSetting::HideGroupAdminRestrictedAccessDialogSetting()
    : RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXT("HideDLGRAGroups"))
{

}

HideGroupLicenseRestrictedAccessDialogSetting::HideGroupLicenseRestrictedAccessDialogSetting()
    : RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXT("HideDLGRAGroupsL"))
{

}

HideUserPermissionRequirementDialogSetting::HideUserPermissionRequirementDialogSetting()
    : RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXT("HideDLGRAUsers"))
{

}

HideUltraAdminRequirementDialogSetting::HideUltraAdminRequirementDialogSetting()
    : RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXT("HideDLGRAUsersUA"))
{

}

HideConsentReminderDialogSetting::HideConsentReminderDialogSetting()
    : RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXT("HideDLGRAConsentRem"))
{

}

HideUltraAdminDeprecatedReminderSetting::HideUltraAdminDeprecatedReminderSetting()
	: RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXT("HideUltraAdminDeprecatedRem"))
{

}

HideReportAnonymousDataWarningDialogSetting::HideReportAnonymousDataWarningDialogSetting()
    : RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXT("HideDLGAnonymousReport"))
{

}

HideStandardToExistingAdvancedReminderSetting::HideStandardToExistingAdvancedReminderSetting()
	: RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXTFORMAT(L"HideStdToExistingAdvRem"))
{

}

HideStandardToNewAdvancedReminderSetting::HideStandardToNewAdvancedReminderSetting()
	: RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXTFORMAT(L"HideStdToNewAdvRem"))
{

}

HideJobCenterLoginRequiredDialogSetting::HideJobCenterLoginRequiredDialogSetting(const wstring& p_ScriptName)
    : RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXTFORMAT(L"JobCenter-LoginRequired-%s", p_ScriptName.c_str()))
{

}

HideHideJobCenterAdminRequiredDialogSetting::HideHideJobCenterAdminRequiredDialogSetting(const wstring& p_ScriptName)
    : RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXTFORMAT(L"JobCenter-AdminIfAdmin-%s", p_ScriptName.c_str()))
{

}

HideJobCenterUltraAdminRequiredDialogSetting::HideJobCenterUltraAdminRequiredDialogSetting(const wstring& p_ScriptName)
    : RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXTFORMAT(L"JobCenter-WarningAdminTech-%s", p_ScriptName.c_str()))
{

}

HideHideJobCenterNoUltraAdminRequiredDialogSetting::HideHideJobCenterNoUltraAdminRequiredDialogSetting(const wstring& p_ScriptName)
    : RegistrySetting<bool>(boost::none, g_RegPathUserSettings, _YTEXTFORMAT(L"JobCenter-WarningUltraAdmin-%s", p_ScriptName.c_str()))
{

}

DownloadFolderSetting::DownloadFolderSetting()
    : RegistrySetting<wstring>(FileUtil::getMyDocuments(), g_RegPathUserSettings, _YTEXT("DLFolder"))
{

}

DownloadMessageDefaultColumnsSetting::DownloadMessageDefaultColumnsSetting()
    : RegistrySetting<wstring>(boost::none, g_RegPathUserSettings, _YTEXT("DLMessageDefaultCol"))
{

}

DownloadEventDefaultColumnsSetting::DownloadEventDefaultColumnsSetting()
    : RegistrySetting<wstring>(boost::none, g_RegPathUserSettings, _YTEXT("DLEventDefaultCol"))
{

}

DownloadPostDefaultColumnsSetting::DownloadPostDefaultColumnsSetting()
    : RegistrySetting<wstring>(boost::none, g_RegPathUserSettings, _YTEXT("DLPostDefaultCol"))
{

}

MainFrameSessionSetting::MainFrameSessionSetting()
    : RegistrySettingNoInit<SessionIdentifier>(SessionIdentifier(), g_RegPathUserSettings, _YTEXT("LastSessionUsed"))
{

}

MainFrameSessionOldSetting::MainFrameSessionOldSetting()
    : RegistrySettingNoInit<SessionIdentifier>(SessionIdentifier(), g_RegPathUserSettings, _YTEXT("LastSession"))
{

}

MainFrameSessionListSizeSetting::MainFrameSessionListSizeSetting()
    : RegistrySetting<int>(MEDIUM_GALLERY, g_RegPathUserSettings, _YTEXT("SessionListSize"))
{

}

OnlineSkuSetting::OnlineSkuSetting()
	: RegistrySetting<wstring>(boost::none, g_RegPathUserSettings, _YTEXT("LastOnlineSkuCheck"))
{

}

DeltaTemporisatorSetting::DeltaTemporisatorSetting()
    : RegistrySetting<int>(boost::none, g_RegPathUserSettings, _YTEXT("DeltaTempoMs"))
{

}

OnPremServerSetting::OnPremServerSetting()
	: RegistrySetting<wstring>(boost::none, _YTEXT("SOFTWARE\\Ytria"), _YTEXT("PowerShellOnPremServer"))
{

}

OnPremUsernameSetting::OnPremUsernameSetting()
    : RegistrySetting<wstring>(boost::none, _YTEXT("SOFTWARE\\Ytria"), _YTEXT("PowerShellOnPremUsername"))
{

}

OnPremEncryptedPasswordSetting::OnPremEncryptedPasswordSetting(const wstring& p_OnPremUsername)
    : RegistrySetting<wstring>(boost::none, _YTEXT("SOFTWARE\\Ytria"), _YTEXT("PowerShellOnPremPasswordEnc"))
    , m_OnPremUsername(p_OnPremUsername)
{

}

boost::YOpt<wstring> OnPremEncryptedPasswordSetting::Get() const
{
    auto v = RegistrySetting<wstring>::Get();
    if (v)
    {
        wstring decoded;
        if (RegistryPassword().decode(*v, decoded, m_OnPremUsername))
            v = decoded;
        else
            v.reset();
    }
    return v;
}

bool OnPremEncryptedPasswordSetting::Set(boost::YOpt<wstring> p_Val) const
{
    if (p_Val.is_initialized())
    {
		wstring encoded;
        if (RegistryPassword().encode(*p_Val, encoded, m_OnPremUsername))
            p_Val = encoded;
        else
            p_Val.reset();
    }

    return RegistrySetting<wstring>::Set(p_Val);
}

OnPremPasswordSetting::OnPremPasswordSetting()
    : RegistrySetting<wstring>(boost::none, _YTEXT("SOFTWARE\\Ytria"), _YTEXT("PowerShellOnPremPassword"))
{

}

OnPremAADComputerNameSetting::OnPremAADComputerNameSetting()
	: RegistrySetting<wstring>(boost::none, _YTEXT("SOFTWARE\\Ytria"), _YTEXT("AADComputerName"))
{

}

OnPremAskedForAADComputerName::OnPremAskedForAADComputerName()
    : RegistrySetting<bool>(boost::none, _YTEXT("SOFTWARE\\Ytria"), _YTEXT("AskedAADComputerName"))
{

}

OnPremUseMsDsConsistencyGuidSetting::OnPremUseMsDsConsistencyGuidSetting()
    : RegistrySetting<bool>(boost::none, _YTEXT("SOFTWARE\\Ytria"), _YTEXT("UseMsDsConsistencyGuid"))
{
}
