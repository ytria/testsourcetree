#include "DirectoryRolesRequester.h"

#include "BusinessDirectoryRole.h"
#include "DirectoryRoleDeserializer.h"
#include "MSGraphSession.h"
#include "PaginatedRequestResults.h"
#include "Sapio365Session.h"
#include "ValueListDeserializer.h"
#include "MsGraphHttpRequestLogger.h"
#include "SessionIdentifier.h"

DirectoryRolesRequester::DirectoryRolesRequester(const std::shared_ptr<IRequestLogger>& p_Logger)
	:m_Logger(p_Logger)
{
}

TaskWrapper<void> DirectoryRolesRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	return Send(p_Session->GetMSGraphSession(Sapio365Session::USER_SESSION), p_Session->GetIdentifier(), p_TaskData);
}

TaskWrapper<void> DirectoryRolesRequester::Send(std::shared_ptr<MSGraphSession> p_Session, const SessionIdentifier& p_Identifier, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<BusinessDirectoryRole, DirectoryRoleDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Identifier);
	return p_Session->getObjects(m_Deserializer, { _YTEXT("directoryRoles") }, {}, false, m_Logger, httpLogger, p_TaskData);
}

const vector<BusinessDirectoryRole>& DirectoryRolesRequester::GetData() const
{
    return m_Deserializer->GetData();
}
