#include "FieldUpdate.h"

#include "BaseO365Grid.h"

template <Scope scope>
FieldUpdate<scope>::FieldUpdate(O365Grid& p_Grid
	, const row_pk_t& p_RowKey
	, const UINT& p_ColumnId
	, const GridBackendField& p_OldValue
	, const GridBackendField& p_NewValue
	, uint32_t p_Flags/* = None*/)
	: IMainItemModification(IMainItemModification::State::AppliedLocally, p_Grid.GetName(p_Grid.GetRowIndex().GetExistingRow(p_RowKey)))
	, m_Grid(p_Grid)
	, m_RowKey(p_RowKey)
	, m_ColumnID(p_ColumnId)
	, m_OldValue(!p_OldValue.HasValue() ? boost::none : boost::YOpt<GridBackendField>(p_OldValue))
	, m_NewValue(!p_NewValue.HasValue() ? boost::none : boost::YOpt<GridBackendField>(p_NewValue))
	, m_RevertOnDestruction(true)
	, m_Flags(p_Flags)
	, m_AlreadyResetAfterError(false)
{
	// Don't put both flags.
	ASSERT((FieldUpdateFlags::IgnoreRemoteValue | FieldUpdateFlags::IgnoreRemoteHasOldValueError) != (m_Flags & (FieldUpdateFlags::IgnoreRemoteValue | FieldUpdateFlags::IgnoreRemoteHasOldValueError)));
}

template <Scope scope>
FieldUpdate<scope>::~FieldUpdate()
{
	if (m_RevertOnDestruction)
		Revert();
}

template <Scope scope>
void FieldUpdate<scope>::Revert()
{
	if (m_State == State::AppliedLocally)
	{
		GridBackendRow* row = m_Grid.GetRowIndex().GetExistingRow(m_RowKey);
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			// Put back old value
			if (m_OldValue)
				row->AddField(*m_OldValue, m_ColumnID).RemoveModified();
			else
				row->RemoveField(m_ColumnID);
		}
	}
}

template <Scope scope>
void FieldUpdate<scope>::RefreshState()
{
	GridBackendRow* row = m_Grid.GetRowIndex().GetExistingRow(m_RowKey);

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		if (FieldUpdateFlags::IgnoreRemoteValue == (m_Flags & FieldUpdateFlags::IgnoreRemoteValue))
		{
			if (row->IsError())
			{
				m_State = State::RemoteError;

				ASSERT(!m_AlreadyResetAfterError); //Eventually remove if it never fails

				if (m_NewValue && row->GetField(m_ColumnID) == *m_NewValue
					|| !m_NewValue && !row->HasField(m_ColumnID))
				{
					// Put back old value
					if (m_OldValue)
						row->AddField(*m_OldValue, m_ColumnID).RemoveModified();
					else
						row->RemoveField(m_ColumnID);
				}

				m_AlreadyResetAfterError = true;
			}
			else
			{
				m_State = State::RemoteHasNewValue;
				if (row->HasField(m_ColumnID))
					row->GetField(m_ColumnID).RemoveModified();
			}
		}
		else
		{
			GridBackendField& gridField = row->GetField(m_ColumnID);
			if (!gridField.HasValue())
				m_ActualValue = boost::none;
			else
				m_ActualValue = gridField;

			if (m_ActualValue)
				m_ActualValue->SetCaseSensitive(true); // Force field comparison to be case-sensitive (as the modified state is).

			// For date fields, when the user sets an empty date we tried to send an 'invalid' date to the server (0001-01-01T00:00:00Z, default one received for new objects)
			// Unfortunately, server refuses such date.
			//const bool newIsEmptyTimeDate = m_NewValue && m_NewValue->IsDate() && m_NewValue->GetValueTimeDate().IsEmpty();
			const bool newIsEmptyString = m_NewValue && m_NewValue->IsString() && wstring(m_NewValue->GetValueStr()).empty();
			const bool actualIsNoValue = !m_ActualValue;

			if (m_OldValue != m_NewValue && m_ActualValue == m_OldValue)
			{
				if (row->IsError())
				{
					m_State = State::RemoteError;
				}
				else if (FieldUpdateFlags::IgnoreRemoteHasOldValueError == (m_Flags & FieldUpdateFlags::IgnoreRemoteHasOldValueError))
				{
					m_State = State::RemoteHasNewValue;
				}
				else
				{
					m_State = State::RemoteHasOldValue;
					UpdateFieldShownSentValue(row, m_ColumnID);
				}
			}
			else if (m_ActualValue == m_NewValue
				|| newIsEmptyString && actualIsNoValue // For some fields, when the user sets an empty string we send null to the server (#181031.RL.008E26)
				//|| newIsEmptyTimeDate && actualIsNoValue
				)
			{
				if (row->IsError())
				{
					// FIXME: Should we have another state in that case ?
					m_State = State::RemoteHasNewValue;
				}
				else
				{
					m_State = State::RemoteHasNewValue;
				}
			}
			else
			{
				if (row->IsError())
				{
					m_State = State::RemoteError;
				}
				else
				{
					m_State = State::RemoteHasOtherValue;
					UpdateFieldShownSentValue(row, m_ColumnID);
				}
			}
		}
	}
}

template <Scope scope>
row_pk_t FieldUpdate<scope>::GetRowKey() const
{
	return m_RowKey;
}

template <Scope scope>
UINT FieldUpdate<scope>::GetColumnID() const
{
	return m_ColumnID;
}

template <Scope scope>
void FieldUpdate<scope>::dontRevertOnDestruction()
{
	m_RevertOnDestruction = false;
}

template <Scope scope>
boost::YOpt<GridBackendField>& FieldUpdate<scope>::getActualValue()
{
	return m_ActualValue;
}

template <Scope scope>
void FieldUpdate<scope>::UpdateShownValue()
{
	GridBackendRow* row = m_Grid.GetRowIndex().GetExistingRow(m_RowKey);

	ASSERT(nullptr != row);
	if (nullptr != row)
		UpdateFieldShownSentValue(row, m_ColumnID);
}

template <Scope scope>
std::unique_ptr<FieldUpdate<scope>> FieldUpdate<scope>::CreateReplacement(std::unique_ptr<FieldUpdate<scope>> p_Replacement)
{
	ASSERT(p_Replacement->m_OldValue == m_NewValue);
	dontRevertOnDestruction();
	p_Replacement->m_OldValue = m_OldValue;
	return p_Replacement;
}

template <Scope scope>
const O365Grid& FieldUpdate<scope>::GetGrid() const
{
	return m_Grid;
}

template <Scope scope>
O365Grid& FieldUpdate<scope>::getGrid()
{
	return m_Grid;
}

template <Scope scope>
const O365Grid& FieldUpdate<scope>::getGrid() const
{
	return m_Grid;
}

template <Scope scope>
bool FieldUpdate<scope>::shoulRevertOnDestruction()
{
	return m_RevertOnDestruction;
}

template <Scope scope>
void FieldUpdate<scope>::UpdateFieldShownSentValue(GridBackendRow* p_Row, UINT p_ColumnID)
{
	// We change the displayed value in the cell only for modifications that have been sent, not the ones edited locally.
	if (m_State != State::AppliedLocally)
	{
		auto& value = m_Grid.GetShowSentValue() ? m_NewValue : m_ActualValue;
		if (!value)
		{
			p_Row->RemoveField(p_ColumnID);
		}
		else
		{
			ASSERT(value->GetColID() == p_ColumnID);
			p_Row->AddField(*value);
		}
	}
}

template <Scope scope>
vector<Modification::ModificationLog> FieldUpdate<scope>::GetModificationLogs() const
{
	auto column = m_Grid.GetColumnByID(GetColumnID());
	ASSERT(nullptr != column);

	static const set<wstring> hideValuesForYUIDs = { _YUID(O365_USER_PASSWORDPROFILE_PASSWORD) };
	const bool hideValues = nullptr != column && hideValuesForYUIDs.find(column->GetUniqueID()) != hideValuesForYUIDs.end();

	ModificationLog mlog(GridBackendUtil::ToString(GetRowKey()), GetObjectName(), ActivityLoggerUtil::g_ActionUpdate, nullptr != column ? column->GetTitleHierarchy() : ModificationLog::g_NotSet, GetState());

	if (GetNewValue().is_initialized())
	{
		const auto v = GetNewValue().get().ToString();
		if (!MFCUtil::StringMatch(GridBackendUtil::g_NoValueString.c_str(), v.c_str()))
			mlog.m_NewValue = hideValues ? g_HiddenValueFromLog : v;
	}

	if (GetOldValue().is_initialized())
	{
		const auto v = GetOldValue().get().ToString();
		if (!MFCUtil::StringMatch(GridBackendUtil::g_NoValueString.c_str(), v.c_str()))
			mlog.m_OldValue = hideValues ? g_HiddenValueFromLog : v;
	}

	return { mlog };
}

template <Scope scope>
const boost::YOpt<GridBackendField>& FieldUpdate<scope>::GetActualValue() const
{
	return m_ActualValue;
}

template <Scope scope>
const boost::YOpt<GridBackendField>& FieldUpdate<scope>::GetOldValue() const
{
	return m_OldValue;
}

template <Scope scope>
const boost::YOpt<GridBackendField>& FieldUpdate<scope>::GetNewValue() const
{
	return m_NewValue;
}

// Explicit template instantiation
template FieldUpdateO365;
template FieldUpdateOnPrem;