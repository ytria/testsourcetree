#include "ShowOnPremUsersCommand.h"
#include "AutomationNames.h"
#include "FrameUsers.h"
#include "OnPremiseUsersRequester.h"
#include "OnPremiseUsersCollectionDeserializer.h"
#include "Sapio365Settings.h"
#include "O365AdminUtil.h"

ShowOnPremUsersCommand::ShowOnPremUsersCommand(FrameUsers& p_Frame)
	: IActionCommand(g_ActionNameShowOnPremUsers),
	m_Frame(p_Frame)
{
}

void ShowOnPremUsersCommand::ExecuteImpl() const
{
	ASSERT(m_Setup);
	if (!m_Setup)
		return;

	if (!Office365AdminApp::CheckAndWarn<LicenseTag::OnPremise>(&m_Frame))
		return;

	auto setup = *m_Setup;
	auto hwnd = m_Frame.GetSafeHwnd();
	if (!ModuleUtil::WarnIfPowerShellHostIncompatible(CWnd::FromHandle(hwnd)))
		return;

	auto taskData = Util::AddFrameTask(_T("Show on-premises users"), &m_Frame, setup, m_Frame.GetLicenseContext(), false);

	YSafeCreateTaskMutable([bom = m_Frame.GetBusinessObjectManager(), hwnd, taskData]() mutable {

		auto deserializer = std::make_shared<OnPremiseUsersCollectionDeserializer>();

		auto initResult = bom->GetSapio365Session()->InitBasicPowerShell(taskData.GetOriginator());
		wstring initErrors = GetPSErrorString(initResult);

		wstring error;
		if (initErrors.empty())
		{
			auto logger = std::make_shared<BasicRequestLogger>(_T("on-premises users"));
			auto requester = std::make_shared<OnPremiseUsersRequester>(deserializer, logger);
			requester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();

			error = GetPSErrorString(requester->GetResult());
		}
		else
			error = initErrors;

		YCallbackMessage::DoPost([=]()
		{
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameUsers*>(CWnd::FromHandle(hwnd)) : nullptr;

			if (ModuleBase::ShouldBuildView(hwnd, taskData.IsCanceled(), taskData, false))
			{
				ASSERT(nullptr != frame);
				if (nullptr != frame)
				{
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s on-premises users...", Str::getStringFromNumber(deserializer->GetData().size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());

					if (!error.empty())
					{
						OnPremiseUser errorUser;
						errorUser.SetError(SapioError(error, 0));

						deserializer->GetData() = { errorUser };

						frame->GetGrid().HandlePostUpdateError(false, false, errorUser.GetError()->GetFullErrorMessage());
					}
					else
					{
						CWaitCursor _;
						 frame->ShowOnPremiseUsers(deserializer->GetData());
					}
				}
			}

			frame->ProcessLicenseContext(); // Let's consume license tokens if needed
			ModuleBase::TaskFinished(frame, taskData, nullptr);
		});
	});
}
