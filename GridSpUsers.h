#pragma once

#include "BaseO365Grid.h"
#include "SpUser.h"

class FrameSpUsers;

class GridSpUsers : public ModuleO365Grid<Sp::User>
{
public:
    GridSpUsers();
    virtual ~GridSpUsers() override;

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildView(const O365DataMap<BusinessSite, vector<Sp::User>>& p_SpUsers, bool p_FullPurge);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
    void customizeGrid() override;

    Sp::User        getBusinessObject(GridBackendRow*) const override;
    void			UpdateBusinessObjects(const vector<Sp::User>& p_SpUsers, bool p_SetModifiedStatus) override;
    void			RemoveBusinessObjects(const vector<Sp::User>& p_SpUsers) override;

private:
    DECLARE_MESSAGE_MAP()

    void InitializeCommands() override;
    void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart) override;

    void FillUserFields(const BusinessSite& p_Site, const Sp::User& p_SpUser, GridBackendRow* p_Row, GridUpdater& p_Updater);

    GridBackendColumn* m_ColMetaId;
    GridBackendColumn* m_ColMetaDisplayName;
    GridBackendColumn* m_ColMetaUserDisplayName;

    GridBackendColumn* m_ColEmail;
    GridBackendColumn* m_ColUserId; // FIXME: Shouldn't we use O365Grid::m_ColId instead?
    GridBackendColumn* m_ColIsHiddenInUI;
    GridBackendColumn* m_ColIsSiteAdmin;
    GridBackendColumn* m_ColLoginName;
    GridBackendColumn* m_ColPrincipalType;
    GridBackendColumn* m_ColTitle;
    GridBackendColumn* m_ColUserIdInfoNameId;
    GridBackendColumn* m_ColUserIdInfoNameIdIssuer;

    FrameSpUsers* m_Frame;
};

