#include "OnPremisesExtensionAttributesSerializer.h"

#include "JsonSerializeUtil.h"

OnPremisesExtensionAttributesSerializer::OnPremisesExtensionAttributesSerializer(const OnPremisesExtensionAttributes& p_OnPremExtAttr)
    : m_OnPremExtAttr(p_OnPremExtAttr)
{
}

void OnPremisesExtensionAttributesSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute1"), m_OnPremExtAttr.m_ExtensionAttribute1, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute2"), m_OnPremExtAttr.m_ExtensionAttribute2, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute3"), m_OnPremExtAttr.m_ExtensionAttribute3, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute4"), m_OnPremExtAttr.m_ExtensionAttribute4, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute5"), m_OnPremExtAttr.m_ExtensionAttribute5, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute6"), m_OnPremExtAttr.m_ExtensionAttribute6, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute7"), m_OnPremExtAttr.m_ExtensionAttribute7, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute8"), m_OnPremExtAttr.m_ExtensionAttribute8, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute9"), m_OnPremExtAttr.m_ExtensionAttribute9, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute10"), m_OnPremExtAttr.m_ExtensionAttribute10, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute11"), m_OnPremExtAttr.m_ExtensionAttribute11, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute12"), m_OnPremExtAttr.m_ExtensionAttribute12, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute13"), m_OnPremExtAttr.m_ExtensionAttribute13, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute14"), m_OnPremExtAttr.m_ExtensionAttribute14, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("extensionAttribute15"), m_OnPremExtAttr.m_ExtensionAttribute15, obj);
}
