#pragma once

#include "IRequester.h"
#include "EventDeserializer.h"
#include "MsGraphPaginator.h"
#include "ODataFilter.h"
#include "ValueListDeserializer.h"

class BusinessEvent;
class IPropertySetBuilder;

class EventListRequester : public IRequester
{
public:
	enum class Context { Users, Groups };
	using DeserializerType = ValueListDeserializer<BusinessEvent, EventDeserializer>;

	EventListRequester(IPropertySetBuilder& p_Properties, const wstring& p_Id, Context p_Ctx, const std::shared_ptr<IPageRequestLogger>& p_Logger);

	void SetCutOffDateTime(const boost::YOpt<wstring>& p_CutOff);

	void SetCustomFilter(const ODataFilter& p_CustomFilter);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	vector<BusinessEvent>& GetData();

private:
	std::shared_ptr<DeserializerType> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;

	// Request info
	wstring m_Id;
	Context m_Ctx;
	IPropertySetBuilder& m_Properties;

	// Cutoff related
	boost::YOpt<wstring> m_CutOffDateTime;
	boost::YOpt<MsGraphPaginator> m_Paginator;

	ODataFilter m_CustomFilter;
};

