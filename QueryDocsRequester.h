#pragma once

#include "IRequester.h"
#include "ValueListDeserializer.h"
#include "Document.h"
#include "DocumentDeserializer.h"

class SingleRequestResult;

namespace Cosmos
{
    // Query example: SELECT * FROM table2 where table2.sapioId=1
    class QueryDocsRequester : public IRequester
    {
    public:
        QueryDocsRequester(const wstring& p_DbName,
            const wstring& p_CollectionName,
            const wstring& p_Query,
            const vector<pair<wstring, wstring>>& p_QueryArgs
        );
        TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

        const vector<Cosmos::Document>& GetData() const;
        const SingleRequestResult& GetResult() const;

    private:
        std::shared_ptr<ValueListDeserializer<Cosmos::Document, Cosmos::DocumentDeserializer>> m_Deserializer;
        std::shared_ptr<SingleRequestResult> m_Result;

        wstring m_DbName;
        wstring m_CollectionName;
        wstring m_Query;
        vector<pair<wstring, wstring>> m_QueryArgs;
    };
}
