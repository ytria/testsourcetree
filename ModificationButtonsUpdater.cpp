#include "ModificationButtonsUpdater.h"
#include "IButtonUpdater.h"

ModificationButtonsUpdater::ModificationButtonsUpdater(std::unique_ptr<IButtonUpdater> p_EnableSaveAll, std::unique_ptr<IButtonUpdater> p_EnableSaveSelected, std::unique_ptr<IButtonUpdater> p_EnableRevertAll, std::unique_ptr<IButtonUpdater> p_EnableRevertSelected)
	: m_EnableSaveAll(std::move(p_EnableSaveAll)),
	m_EnableSaveSelected(std::move(p_EnableSaveSelected)),
	m_EnableRevertAll(std::move(p_EnableRevertAll)),
	m_EnableRevertSelected(std::move(p_EnableRevertSelected))
{
	if (!m_EnableSaveAll || !m_EnableSaveSelected || !m_EnableRevertAll || !m_EnableRevertSelected)
		throw std::exception("Button updaters cannot be null");
}

void ModificationButtonsUpdater::UpdateSaveAll(CCmdUI* pCmdUI) const
{
	m_EnableSaveAll->Update(pCmdUI);
}

void ModificationButtonsUpdater::UpdateSaveSelected(CCmdUI* pCmdUI) const
{
	m_EnableSaveSelected->Update(pCmdUI);
}

void ModificationButtonsUpdater::UpdateRevertAll(CCmdUI* pCmdUI) const
{
	m_EnableRevertAll->Update(pCmdUI);
}

void ModificationButtonsUpdater::UpdateRevertSelected(CCmdUI* pCmdUI) const
{
	m_EnableRevertSelected->Update(pCmdUI);
}

void ModificationButtonsUpdater::SetSaveAllEnableUpdater(std::unique_ptr<IButtonUpdater> p_EnableSaveAll)
{
	ASSERT(p_EnableSaveAll);
	if (p_EnableSaveAll)
		m_EnableSaveAll = std::move(p_EnableSaveAll);
}

void ModificationButtonsUpdater::SetSaveSelectedEnableUpdater(std::unique_ptr<IButtonUpdater> p_EnableSaveSelected)
{
	ASSERT(p_EnableSaveSelected);
	if (p_EnableSaveSelected)
		m_EnableSaveSelected = std::move(p_EnableSaveSelected);
}

void ModificationButtonsUpdater::SetRevertAllEnableUpdater(std::unique_ptr<IButtonUpdater> p_EnableRevertAll)
{
	ASSERT(p_EnableRevertAll);
	if (p_EnableRevertAll)
		m_EnableRevertAll = std::move(p_EnableRevertAll);
}

void ModificationButtonsUpdater::SetRevertSelectedEnableUpdater(std::unique_ptr<IButtonUpdater> p_EnableRevertSelected)
{
	ASSERT(p_EnableRevertSelected);
	if (p_EnableRevertSelected)
		m_EnableRevertSelected = std::move(p_EnableRevertSelected);
}
