#pragma once

#include "IJsonSerializer.h"
#include "BusinessGroup.h"
#include "SqlCacheConfig.h"

class DbGroupSerializer : public IJsonSerializer
{
public:
	// Memory Cache
	static const std::vector<PooledString>& MemoryCacheRequestFields();

    // List
    static const std::vector<PooledString>& NotInMemoryCacheListRequestFields();

    // Load more(s)
	static const std::vector<PooledString>& SyncOnlyRequestFields_v1();
	static const std::vector<PooledString>& SyncOnlyRequestFields_beta();


	static const std::vector<PooledString>& CreatedOnBehalfOfRequestFields();
	static const std::vector<PooledString>& TeamRequestFields();
	static const std::vector<PooledString>& GroupSettingsRequestFields();
	static const std::vector<PooledString>& LifeCyclePoliciesRequestFields();
	static const std::vector<PooledString>& OwnersRequestFields();
	static const std::vector<PooledString>& MembersCountRequestFields();
	static const std::vector<PooledString>& DriveRequestFields();
	static const std::vector<PooledString>& IsYammerRequestFields();

public:
    DbGroupSerializer(const BusinessGroup& p_Group, const GBI p_BlockId);
    void Serialize() override;

protected:
	DbGroupSerializer(const BusinessGroup& p_User, const std::vector<PooledString>& p_Fields);

private:
    BusinessGroup m_Group;
	const std::vector<PooledString>& m_Fields;
	PooledString m_DataDateField;
};
