#pragma once

#include "BusinessObject.h"
#include "GroupSettingTemplate.h"
#include "SettingTemplateValue.h"

class BusinessGroupSettingTemplate : public BusinessObject
{
public:
    BusinessGroupSettingTemplate();
    BusinessGroupSettingTemplate(const GroupSettingTemplate& p_GroupSettingTemplate);

	const boost::YOpt<PooledString>& GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<PooledString>& GetDescription() const;
    void SetDescription(const boost::YOpt<PooledString>& p_Val);
    
    const std::vector<SettingTemplateValue>& GetValues() const;
    void SetValues(const std::vector<SettingTemplateValue>& p_Val);

    static const wstring g_StringProperty;
    static const wstring g_BoolProperty;
    static const wstring g_GuidProperty;
    static const wstring g_Int32Property;

    static const wstring g_GroupUnifiedGuestSettingTemplateId;
    static const wstring g_AllowToAddGuestsName;

private:
	boost::YOpt<PooledString> m_DisplayName;
    boost::YOpt<PooledString> m_Description;
    std::vector<SettingTemplateValue> m_Values;

    RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
    friend class GroupSettingTemplateDeserializer;
};

