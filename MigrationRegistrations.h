#pragma once

class IMigrationVersion;

class MigrationRegistrations
{
public:
	MigrationRegistrations();
	std::shared_ptr<IMigrationVersion> GetCurrentVersionMigration() const;
	std::shared_ptr<IMigrationVersion> GetVersionMigration(int p_Version) const;

private:
	void RegisterMigrations();

private:
	std::vector<std::shared_ptr<IMigrationVersion>> m_Migrations;
};

