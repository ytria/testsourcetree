#pragma once

#include "ISubItemDeletion.h"

#include "GridSitePermissions.h"
#include "RoleAssignmentPermission.h"

class RoleAssignmentPermissionDeletion : public ISubItemDeletion
{
public:
    RoleAssignmentPermissionDeletion(GridSitePermissions& p_Grid, const RoleAssignmentPermission& p_Info);

    virtual bool ShouldCreateFormatter(GridBackendRow* p_Row, size_t p_Index) const override;
    virtual bool IsCorrespondingRow(GridBackendRow* p_Row) const override;

	virtual vector<ModificationLog> GetModificationLogs() const override;

    bool IsUserPermissionDeletion() const;
    bool IsGroupPermissionDeletion() const;

	GridBackendRow* GetMainRowIfCollapsed() const override;

private:
    GridSitePermissions& m_GridSitePermissions;
    RoleAssignmentPermission m_Info;
};