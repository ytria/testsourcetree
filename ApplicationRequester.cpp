#include "ApplicationRequester.h"
#include "Sapio365Session.h"
#include "ApplicationDeserializer.h"
#include "MsGraphHttpRequestLogger.h"

ApplicationRequester::ApplicationRequester(const wstring& p_Id, const std::shared_ptr<IRequestLogger>& p_Logger)
	:m_Id(p_Id),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> ApplicationRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<SingleRequestResult>();
	m_Deserializer = std::make_shared<ApplicationDeserializer>();

	web::uri_builder uri;
	uri.append_path(_YTEXT("applications"));
	uri.append_path(m_Id);
	
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, m_Result, uri.to_uri(), false, m_Logger, httpLogger, p_TaskData).ThenByTask([result = m_Result, deserializer = m_Deserializer](pplx::task<void> p_Task) {
		try
		{
			p_Task.wait();
		}
		catch (const RestException& e)
		{
			RestResultInfo info(e.GetRequestResult());
			info.m_RestException = std::make_shared<RestException>(e);
			result->SetResult(info);
		}
		catch (const std::exception& e)
		{
			RestResultInfo info;
			info.m_Exception = e;
			result->SetResult(info);
		}
		catch (...)
		{
			result->SetResult(RestResultInfo());
		}
	});
}

const SingleRequestResult& ApplicationRequester::GetResult() const
{
	return *m_Result;
}

const Application& ApplicationRequester::GetData() const
{
	return m_Deserializer->GetData();
}
