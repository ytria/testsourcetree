#include "MessageRulesRequester.h"

#include "MailFolderDeserializer.h"
#include "MessageRuleDeserializer.h"
#include "MSGraphCommonData.h"
#include "MSGraphSession.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "ValueListDeserializer.h"
#include "MsGraphHttpRequestLogger.h"

using namespace Rest;

MessageRulesRequester::MessageRulesRequester(const PooledString& p_UserId, const std::shared_ptr<IRequestLogger>& p_Logger)
    : m_UserId(p_UserId),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> MessageRulesRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<BusinessMessageRule, MessageRuleDeserializer>>();
    m_Result = std::make_shared<SingleRequestResult>();

	return YSafeCreateTask([p_Session, deserializer = m_Deserializer, userId = m_UserId, logger = m_Logger, p_TaskData]() {

		boost::YOpt<SapioError> error;
		try
		{
			auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
			p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObjects(deserializer, { USERS_PATH, userId, MAILFOLDERS_PATH, L"inbox/messageRules" }, {}, false, logger, httpLogger, p_TaskData).GetTask().wait();
		}
		catch (const RestException& e)
		{
			error = HttpError(e);
		}
		catch (const std::exception& e)
		{
			error = SapioError(e);
		}

		if (error)
		{
			auto& data = deserializer->GetData();
			ASSERT(data.empty());
			data.emplace_back();
			data.back().SetError(error);
		}
		else
		{
			// TODO: All this block belongs outside the class
			logger->SetLogMessage(_T("mail folder"));
			std::map<PooledString, std::pair<PooledString, bool>> folders;
			for (auto& rules : deserializer->GetData())
			{
				/*if (p_TaskData.IsCanceled())
					pplx::cancel_current_task();*/

				if (rules.m_Actions.m_CopyToFolderID)
				{
					if (folders.find(*rules.m_Actions.m_CopyToFolderID) == folders.end())
					{
						auto deserializer = std::make_shared<MailFolderDeserializer>();

						BusinessMailFolder mailFolder;

						if (p_TaskData.IsCanceled())
						{
							mailFolder.SetError(SkippedError());
						}
						else
						{
							try
							{
								auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
								p_Session->GetMSGraphSession(httpLogger->GetSessionType())->GetMailFolder(deserializer, userId, *rules.m_Actions.m_CopyToFolderID, logger, httpLogger, p_TaskData).GetTask().wait();
								mailFolder = deserializer->GetData();
							}
							catch (const RestException& e)
							{
								mailFolder.SetError(HttpError(e));
							}
							catch (const std::exception& e)
							{
								mailFolder.SetError(SapioError(e));
							}
						}

						if (mailFolder.GetError())
						{
							// Store error
							folders[*rules.m_Actions.m_CopyToFolderID].first = mailFolder.GetError()->GetFullErrorMessage();
							folders[*rules.m_Actions.m_CopyToFolderID].second = true;
						}
						else
						{
							ASSERT(mailFolder.m_DisplayName);
							if (mailFolder.m_DisplayName)
								folders[*rules.m_Actions.m_CopyToFolderID].first = *mailFolder.m_DisplayName;
							folders[*rules.m_Actions.m_CopyToFolderID].second = false;
						}
					}

					rules.m_Actions.m_CopyToFolderNameError = folders[*rules.m_Actions.m_CopyToFolderID].second;
					rules.m_Actions.m_CopyToFolderName = folders[*rules.m_Actions.m_CopyToFolderID].first;
				}

				/*if (p_TaskData.IsCanceled())
				pplx::cancel_current_task();*/

				if (rules.m_Actions.m_MoveToFolderID)
				{
					if (folders.find(*rules.m_Actions.m_MoveToFolderID) == folders.end())
					{
						auto deserializer = std::make_shared<MailFolderDeserializer>();

						BusinessMailFolder mailFolder;
						if (p_TaskData.IsCanceled())
						{
							mailFolder.SetError(SkippedError());
						}
						
						try
						{
							auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
							p_Session->GetMSGraphSession(httpLogger->GetSessionType())->GetMailFolder(deserializer, userId, *rules.m_Actions.m_MoveToFolderID, logger, httpLogger, p_TaskData).GetTask().wait();
							mailFolder = deserializer->GetData();
						}
						catch (const RestException& e)
						{
							mailFolder.SetError(HttpError(e));
						}
						catch (const std::exception& e)
						{
							mailFolder.SetError(SapioError(e));
						}

						if (mailFolder.GetError())
						{
							// Store error
							folders[*rules.m_Actions.m_MoveToFolderID].first = mailFolder.GetError()->GetFullErrorMessage();
							folders[*rules.m_Actions.m_MoveToFolderID].second = true;
						}
						else
						{
							ASSERT(mailFolder.m_DisplayName);
							if (mailFolder.m_DisplayName)
								folders[*rules.m_Actions.m_MoveToFolderID].first = *mailFolder.m_DisplayName;
							folders[*rules.m_Actions.m_MoveToFolderID].second = false;
						}
					}

					rules.m_Actions.m_MoveToFolderName = folders[*rules.m_Actions.m_MoveToFolderID].first;
					rules.m_Actions.m_MoveToFolderNameError = folders[*rules.m_Actions.m_MoveToFolderID].second;
				}
			}
		}
	});
}

vector<BusinessMessageRule>& MessageRulesRequester::GetData()
{
    return m_Deserializer->GetData();
}

const SingleRequestResult& MessageRulesRequester::GetResult() const
{
    return *m_Result;
}
