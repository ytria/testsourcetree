#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessSite.h"

class Sapio365Session;

class WholeSiteDeserializer : public JsonObjectDeserializer
                            , public Encapsulate<BusinessSite>
{
public:
    WholeSiteDeserializer(const std::shared_ptr<Sapio365Session>& p_Session);
    void DeserializeObject(const web::json::object& p_Object) override;

private:
    const std::shared_ptr<Sapio365Session>& m_Session;
};

