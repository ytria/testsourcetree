#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "TeamMemberSettings.h"

class TeamMemberSettingsDeserializer : public JsonObjectDeserializer, public Encapsulate<TeamMemberSettings>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

