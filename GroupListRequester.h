#pragma once

#include "IRequester.h"
#include "IPageRequestLogger.h"
#include "ODataFilter.h"

template <class T, class U>
class ValueListDeserializer;

class BusinessGroup;
class GroupDeserializer;
class IPropertySetBuilder;
class PaginatedRequestResults;
class MsGraphHttpRequestLogger;

class GroupListRequester : public IRequester
{
public:
	class GroupListSubRequester : public IRequester
	{
	public:
		GroupListSubRequester();
		void SetParentRequester(GroupListRequester* p_ParentRequester);
		void SetProperties(const vector<rttr::property>& p_Properties);
		void SetLogger(const std::shared_ptr<MsGraphHttpRequestLogger>& p_Logger);

	protected:
		GroupListRequester* m_Requester;
		vector<rttr::property> m_Properties;
		std::shared_ptr<MsGraphHttpRequestLogger> m_Logger;
	};

	class RegularRequester : public GroupListSubRequester
	{
	public:
		TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	};

	class InitialDeltaRequester : public GroupListSubRequester
	{
	public:
		InitialDeltaRequester();
		TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

		wstring GetNextDeltalink() const;

	private:
		std::shared_ptr<wstring> m_NextDeltaLink;
	};

	class DeltaLinkRequester : public GroupListSubRequester
	{
	public:
		DeltaLinkRequester(const wstring& p_DeltaLink);
		TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

		wstring GetNextDeltalink() const;

	private:
		std::shared_ptr<wstring> m_NextDeltaLink;
		wstring m_DeltaLink;
	};

    GroupListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger, std::unique_ptr<GroupListSubRequester> p_SpecificRequester);

    virtual TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    vector<BusinessGroup>& GetData();

    void SetCustomFilter(const ODataFilter& p_CustomFilter);

private:
    std::shared_ptr<ValueListDeserializer<BusinessGroup, GroupDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;

	std::unique_ptr<GroupListSubRequester> m_SubRequester;

	ODataFilter m_CustomFilter;
};

