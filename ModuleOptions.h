#pragma once

#include "LoadingScope.h"
#include "ODataFilter.h"

struct ModuleOptions
{
	ModuleOptions();

	boost::YOpt<YTimeDate> m_ReferenceTimeDate;
	boost::YOpt<wstring> m_CutOffDateTime;

	bool m_RequestSoftDeletedFolders;

	bool m_BodyPreview;
	bool m_BodyContent;
	bool m_MailHeaders;
	LoadingScope m_LoadingScope;

	ODataFilter m_CustomFilter;

	wstring Serialize() const;
	bool Deserialize(const wstring& p_String);
	
	bool operator==(const ModuleOptions& p_Other) const;
};
