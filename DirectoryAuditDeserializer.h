#pragma once

#include "DirectoryAudit.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class DirectoryAuditDeserializer : public JsonObjectDeserializer, public Encapsulate<DirectoryAudit>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

