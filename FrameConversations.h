#pragma once

#include "GridFrameBase.h"
#include "GridConversations.h"

class FrameConversations : public GridFrameBase
{
public:
	FrameConversations(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

    DECLARE_DYNAMIC(FrameConversations)
    virtual ~FrameConversations() = default;


    virtual O365Grid& GetGrid() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;
    virtual void ApplyAllSpecific() override;

    void ShowConversations(const O365DataMap<BusinessGroup, vector<BusinessConversation>>& p_Conversations, bool p_FullPurge);


protected:
    //DECLARE_MESSAGE_MAP()

    virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
    GridConversations m_ConversationsGrid;
};

