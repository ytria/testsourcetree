#pragma once

#include "GridBackendUtil.h"
#include "YTimeDate.h"

class GridBackendField;

class SubItemFieldValue
{
public:
    SubItemFieldValue();
    SubItemFieldValue(const GridBackendField& p_Field);
    SubItemFieldValue(const GridBackendField& p_Field, size_t p_MultiValueIndex);
    
    bool operator==(const SubItemFieldValue& p_Rhs) const;
    bool operator!=(const SubItemFieldValue& p_Rhs) const;

    const PooledString& GetValueStr() const;
    const YTimeDate& GetValueTimeDate() const;
    bool GetValueBool() const;

    bool HasValue() const;
    GridBackendUtil::DATATYPE GetFieldDataType() const;

	wstring ToString() const;

private:
    PooledString m_StrValue;
    YTimeDate m_TimeDateValue;
    bool m_BoolValue;

    bool m_HasValue;

    GridBackendUtil::DATATYPE m_FieldType;
};

