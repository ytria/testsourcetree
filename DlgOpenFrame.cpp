#include "DlgOpenFrame.h"

#include "Office365Admin.h"
#include "SessionTypes.h"
#include "JSONUtil.h"

std::map<wstring, int, Str::keyLessInsensitive> DlgOpenFrame::g_Sizes;
const wstring DlgOpenFrame::g_CheckboxName = _YTEXT("checkbox");
const wstring DlgOpenFrame::g_CancelName = _YTEXT("cancel");

DlgOpenFrame::DlgOpenFrame(const wstring& p_Title, const vector<IFrameItem>& p_Items, const CheckboxText& p_CheckBoxText, CWnd* p_Parent)
	: ResizableDialog(IDD, p_Parent)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_checkBoxChecked(false)
	, m_Title(p_Title)
	, m_Items(p_Items)
	, m_CheckBoxText(p_CheckBoxText)
{
	ASSERT(checkNoDuplicates());

	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_OPENFRAME_SIZE, g_Sizes);
		ASSERT(success);
	}
}

bool DlgOpenFrame::GetCheckboxState() const
{
	return m_checkBoxChecked;
}

bool DlgOpenFrame::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	ASSERT(data.size() == 1 || data.size() == 2);

	UINT endDialogVal = 0;
	m_checkBoxChecked = false;
	for (auto& d : data)
	{
		if (g_CheckboxName == d.first)
		{
			m_checkBoxChecked = true;
		}
		else if (g_CancelName == d.first)
		{
			ASSERT(0 == endDialogVal);
			endDialogVal = IDCANCEL;
			break;
		}
		else
		{
			for (const auto& item : m_Items)
			{
				if (item.getHBSName() == d.first)
				{
					ASSERT(0 == endDialogVal);
					endDialogVal = item.GetID();
					break;
				}
			}
		}
	}

	bool RetVal = true;
	if (0 != endDialogVal)
	{
		endDialogFixed(endDialogVal);
		RetVal = false;
	}
	else
	{
		ASSERT(false);
	}

	return RetVal;
}

BOOL DlgOpenFrame::OnInitDialogSpecificResizable()
{
	SetWindowText(YtriaTranslate::Do(DlgOpenFrame_OnInitDialogSpecificResizable_1, _YLOC("Confirmation")).c_str());

	CWnd* pCtrl = GetDlgItem(IDC_GRID_PLACEHOLDER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, NULL);

	// FIXME: Temporary - Remove when no more needed.
	//m_HtmlView->SetDebugDisplayPostedData(true);

	m_HtmlView->SetPostedDataTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
	{
		{ _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
		,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-openFrame.js"), _YTEXT("") } } // This file is in resources
		,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("openFrame.js"), _YTEXT("") } }, // This file is in resources
	}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("openFrame.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	ASSERT(2 == m_Items.size() || 3 == m_Items.size());
	const bool has2buttons = m_Items.size() == 2;

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const auto it = has2buttons ? g_Sizes.find(_YTEXT("min-height-2buttons")) : g_Sizes.find(_YTEXT("min-height-3buttons"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = has2buttons ? g_Sizes.find(_YTEXT("min-width-2buttons")) : g_Sizes.find(_YTEXT("min-width-3buttons"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
	BlockVResize(true);

	return TRUE;
}

void DlgOpenFrame::generateJSONScriptData(wstring& p_Output)
{
	const wstring cancelButtonText = YtriaTranslate::Do(DlgOpenFrame_generateJSONScriptData_1, _YLOC("Cancel")).c_str();

	json::value root =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("main"),
				JSON_BEGIN_OBJECT
					JSON_PAIR_KEY(_YTEXT("id"))				JSON_PAIR_VALUE(JSON_STRING(_YTEXT("DlgOpenFrame"))),
					JSON_PAIR_KEY(_YTEXT("method"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("POST"))),
					JSON_PAIR_KEY(_YTEXT("action"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("#"))),
					JSON_PAIR_KEY(_YTEXT("title"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(m_Title))),
					JSON_PAIR_KEY(_YTEXT("checkboxName"))	JSON_PAIR_VALUE(JSON_STRING(g_CheckboxName)),
					JSON_PAIR_KEY(_YTEXT("checkboxTextBold"))   JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(m_CheckBoxText.m_Main))),
					JSON_PAIR_KEY(_YTEXT("checkboxText"))   JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(m_CheckBoxText.m_Sub))),
					JSON_PAIR_KEY(_YTEXT("checkBoxAttr"))	JSON_PAIR_VALUE(JSON_STRING(/*m_CheckboxState ? _YTEXT("checked") : */_YTEXT(""))),
				JSON_END_OBJECT
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("items"),
				JSON_BEGIN_ARRAY
					JSON_BEGIN_OBJECT
						JSON_PAIR_KEY(_YTEXT("hbs"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Button"))),
						JSON_BEGIN_PAIR
							_YTEXT("choices"),
							JSON_BEGIN_ARRAY
								JSON_BEGIN_OBJECT
									JSON_PAIR_KEY(_YTEXT("type"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
									JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(cancelButtonText)),
									JSON_PAIR_KEY(_YTEXT("attribute"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("cancel"))),
									JSON_PAIR_KEY(_YTEXT("name"))		JSON_PAIR_VALUE(JSON_STRING(g_CancelName)),
								JSON_END_OBJECT
							JSON_END_ARRAY
						JSON_END_PAIR,
					JSON_END_OBJECT,
					/* Will be filled below. */
				JSON_END_ARRAY
			JSON_END_PAIR
		JSON_END_OBJECT
		;

	web::json::value& jsonItems = root.as_object()[_YTEXT("items")];
	ASSERT(1 == jsonItems.size());

	size_t index = jsonItems.size();
	for (const auto& item : m_Items)
	{
		jsonItems[index++] =
			JSON_BEGIN_OBJECT
                JSON_PAIR_KEY(_YTEXT("hbs"))         JSON_PAIR_VALUE(JSON_STRING(_YTEXT("openFrame"))),
                JSON_PAIR_KEY(_YTEXT("buttonTitle")) JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(item.getTitle()))),
                JSON_PAIR_KEY(_YTEXT("buttonText"))  JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(item.getText()))),
                JSON_PAIR_KEY(_YTEXT("buttonName"))  JSON_PAIR_VALUE(JSON_STRING(item.getHBSName())),
				JSON_BEGIN_PAIR
					_YTEXT("subItems"),
					JSON_BEGIN_ARRAY
						JSON_BEGIN_OBJECT
							JSON_PAIR_KEY(_YTEXT("hbs"))	JSON_PAIR_VALUE(JSON_STRING(item.getHBSName())),
						JSON_END_OBJECT
					JSON_END_ARRAY
				JSON_END_PAIR,
            JSON_END_OBJECT
			;
	}

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}

bool DlgOpenFrame::checkNoDuplicates()
{
	set<UINT> ids;
	set<wstring> names;
	for (const auto& item : m_Items)
	{
		{
			auto res = ids.insert(item.GetID());
			if (!res.second)
				return false;
		}

		{
			auto res = names.insert(item.getHBSName());
			if (!res.second)
				return false;
		}
	}

	return true;
}

// =================================================================================================
DlgOpenFrame::IFrameItem::IFrameItem(UINT p_ID, const wstring& p_HBSName, const wstring& p_Title, const wstring& p_Text)
	: m_ID(p_ID)
	, m_HBSName(p_HBSName)
	, m_Title(p_Title)
	, m_Text(p_Text)
{

}

UINT DlgOpenFrame::IFrameItem::GetID() const
{
	return m_ID;
}

const wstring& DlgOpenFrame::IFrameItem::getHBSName() const
{
	return m_HBSName;
}

const wstring& DlgOpenFrame::IFrameItem::getTitle() const
{
	return m_Title;
}

const wstring& DlgOpenFrame::IFrameItem::getText() const
{
	return m_Text;
}

// =================================================================================================

DlgOpenFrame::DetachNewWindow::DetachNewWindow()
	: IFrameItem(g_ID, _YTEXT("link-new-window")
		, YtriaTranslate::Do(DlgOpenFrame__DetachNewWindow_DetachNewWindow_1, _YLOC("Open results in a NEW window.")).c_str()
		, YtriaTranslate::Do(DlgOpenFrame__DetachNewWindow_DetachNewWindow_2, _YLOC("Your history will remain unaltered.")).c_str())
{
}

// =================================================================================================

DlgOpenFrame::DetachHistory::DetachHistory()
	: IFrameItem(g_ID, _YTEXT("history-new-window")
		, YtriaTranslate::Do(DlgOpenFrame__DetachHistory_DetachHistory_1, _YLOC("Open in CURRENT window but SAVE history.")).c_str()
		, YtriaTranslate::Do(DlgOpenFrame__DetachHistory_DetachHistory_2, _YLOC("Your current Forward history will be maintained in a separate window.")).c_str())
{
}

// =================================================================================================

DlgOpenFrame::OverrideHistory::OverrideHistory()
	: IFrameItem(g_ID, _YTEXT("delete-history")
		, YtriaTranslate::Do(DlgOpenFrame__OverrideHistory_OverrideHistory_2, _YLOC("Open results in CURRENT window.")).c_str()
		, YtriaTranslate::Do(DlgOpenFrame__OverrideHistory_OverrideHistory_1, _YLOC("Your current Forward history will be replaced.")).c_str())
{
}

// =================================================================================================

DlgOpenFrame::CreateNewWindow::CreateNewWindow()
	: IFrameItem(g_ID, _YTEXT("create-new-window")
		, YtriaTranslate::Do(DlgOpenFrame__CreateNewWindow_CreateNewWindow_2, _YLOC("Open results in a NEW window.")).c_str()
		, YtriaTranslate::Do(DlgOpenFrame__CreateNewWindow_CreateNewWindow_1, _YLOC("Your information will be retrieved as requested.")).c_str())
{
}

// =================================================================================================

DlgOpenFrame::ReuseWindowFrame::ReuseWindowFrame()
	: IFrameItem(g_ID, _YTEXT("reuse-existing-window")
		, YtriaTranslate::Do(DlgOpenFrame__ReuseWindowFrame_ReuseWindowFrame_2, _YLOC("Open results in existing window.")).c_str()
		, YtriaTranslate::Do(DlgOpenFrame__ReuseWindowFrame_ReuseWindowFrame_1, _YLOC("Use your results that have already been retrieved.")).c_str())
{

}

// =================================================================================================

DlgOpenFrameForHistory::DlgOpenFrameForHistory(CWnd* p_Parent)
	: DlgOpenFrame(YtriaTranslate::Do(DlgOpenFrameForHistory_DlgOpenFrameForHistory_3, _YLOC("Do you want to preserve your navigation history?\nChoose one of the following options:")).c_str()
		, { OverrideHistory(), DetachHistory(), DetachNewWindow() }
		, { YtriaTranslate::Do(DlgOpenFrameForHistory_DlgOpenFrameForHistory_1, _YLOC("Set as default and don't ask again.")).c_str(), YtriaTranslate::Do(DlgOpenFrameForHistory_DlgOpenFrameForHistory_2, _YLOC("You can change this later in <b>Settings</b>.")).c_str() }
		, p_Parent)
{}

// =================================================================================================

DlgOpenFrameForReuse::DlgOpenFrameForReuse(CWnd* p_Parent)
	: DlgOpenFrame(YtriaTranslate::Do(DlgOpenFrameForReuse_DlgOpenFrameForReuse_3, _YLOC("This information is already open in an existing window.\nChoose one of the following options:")).c_str()
		, { CreateNewWindow(), ReuseWindowFrame() }
		, { YtriaTranslate::Do(DlgOpenFrameForReuse_DlgOpenFrameForReuse_1, _YLOC("Set as default and don't ask again.")).c_str(), YtriaTranslate::Do(DlgOpenFrameForReuse_DlgOpenFrameForReuse_2, _YLOC("You can change this later in <b>Settings</b>.")).c_str() }
		, p_Parent)
{}
