#pragma once

#include "YTimeDate.h"

class IPSObjectPropertyReader;

class PSSerializeUtil
{
public:
	static wstring DeserializeString(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName);
	static vector<uint8_t> DeserializeByteArray(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName);
	static vector<PooledString> DeserializeStringCollection(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName);
	static vector<YTimeDate> DeserializeDateTimeCollection(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName);
	static bool DeserializeBool(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName);
	static int32_t DeserializeInt32(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName);
	static int64_t DeserializeInt64(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName);
	static boost::YOpt<YTimeDate> DeserializeInt64AsTimeDate(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName);
	static boost::YOpt<YTimeDate> DeserializeDateTime(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName);

private:
	static boost::YOpt<YTimeDate> DeserializeDateTime(int64_t p_DateTime);
};

