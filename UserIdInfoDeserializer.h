#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "UserIdInfo.h"

namespace Sp
{
    class UserIdInfoDeserializer : public JsonObjectDeserializer, public Encapsulate<Sp::UserIdInfo>
    {
    protected:
        void DeserializeObject(const web::json::object& p_Object) override;
    };
}

