#pragma once

#include "BusinessMailFolder.h"
#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"

class MailFolderDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessMailFolder>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

