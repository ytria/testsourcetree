#include "SubscriptionsListRequester.h"

#include "AzurePaginator.h"
#include "AzureSession.h"
#include "LoggerService.h"
#include "SingleRequestResult.h"
#include "Sapio365Session.h"
#include "ValueListDeserializer.h"
#include "BasicHttpRequestLogger.h"

TaskWrapper<void> SubscriptionsListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<Azure::Subscription, Azure::SubscriptionDeserializer>>();
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri(_YTEXT("subscriptions"));
	uri.append_query(AzureSession::g_ApiVersionKey, _YTEXT("2016-06-01"));

	LoggerService::User(YtriaTranslate::Do(SubscriptionsListRequester_Send_1, _YLOC("Retrieving Azure subscriptions")).c_str(), p_TaskData.GetOriginator());

	Azure::Paginator paginator(uri.to_uri(), [=](const web::uri & p_Uri) {
		auto json = std::make_shared<web::json::value>();
		auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
		p_Session->GetAzureSession()->Get(p_Uri, httpLogger, m_Result, m_Deserializer, json, p_TaskData).GetTask().wait();
		return *json;
	});

	return paginator.Paginate();
}

const vector<Azure::Subscription>& SubscriptionsListRequester::GetData() const
{
	return m_Deserializer->GetData();
}
