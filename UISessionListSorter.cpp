#include "UISessionListSorter.h"
#include "PersistentSession.h"

bool UISessionListSorter::operator()(const PersistentSession& p_Elem1, const PersistentSession& p_Elem2) const
{
	const auto status1 = p_Elem1.GetStatus(p_Elem1.GetRoleId());
	const auto status2 = p_Elem2.GetStatus(p_Elem2.GetRoleId());

	if (status1 != status2 && (status1 == SessionStatus::ACTIVE || status2 == SessionStatus::ACTIVE))
		return status1 == SessionStatus::ACTIVE;

	if (p_Elem1.IsFavorite() != p_Elem2.IsFavorite())
		return p_Elem1.IsFavorite() && !p_Elem2.IsFavorite();

	// Prioritize role if same lastused
	if (p_Elem1.GetLastUsedOn() == p_Elem2.GetLastUsedOn() && p_Elem1.GetRoleId() != 0 && p_Elem2.GetRoleId() == 0)
		return true;

	return p_Elem1.GetLastUsedOn() > p_Elem2.GetLastUsedOn();
}
