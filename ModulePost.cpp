#include "ModulePost.h"

#include "BusinessObjectManager.h"
#include "FramePosts.h"
#include "GraphCache.h"
#include "ModuleUtil.h"
#include "O365AdminUtil.h"
#include "YCallbackMessage.h"
#include "MultiObjectsPageRequestLogger.h"

void ModulePost::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
	{
    case Command::ModuleTask::List:
		showPosts(p_Command);
		break;
    case Command::ModuleTask::ListAttachments:
		showAttachments(p_Command);
		break;
    case Command::ModuleTask::DownloadAttachments:
		downloadAttachments(p_Command);
	    break;
    case Command::ModuleTask::ViewItemAttachment:
		showItemAttachment(p_Command);
	    break;
    case Command::ModuleTask::DeleteAttachment:
		deleteAttachments(p_Command);
	    break;
	default:
		ASSERT(false);
    }
}

void ModulePost::showPosts(const Command& p_Command)
{
	auto		p_Action	= p_Command.GetAutomationAction();
    const auto& info		= p_Command.GetCommandInfo();
    const auto&	subSubIds	= info.GetSubSubIds();

    RefreshSpecificData refreshSpecificData;

    using DataType = map<PooledString, PooledString>;
	DataType threadsTopics;
    if (info.Data().is_type<DataType>()) // Only happens when the window is first opened
        threadsTopics = info.Data().get_value<DataType>();
    else if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>()) // Only during a partial refresh
        refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

	auto			postsFrame		= dynamic_cast<FramePosts*>(info.GetFrame());
	const Origin	origin			= info.GetOrigin();
	auto			sourceWindow	= p_Command.GetSourceWindow();

	auto sourceCWnd = sourceWindow.GetCWnd();
	if (FramePosts::CanCreateNewFrame(true, sourceCWnd, p_Action))
	{
		auto frame = postsFrame;
		if (nullptr == frame)
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin			= origin;
			moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::SUBSUBIDS;
			moduleCriteria.m_SubSubIDs		= info.GetSubSubIds();
			moduleCriteria.m_Privilege		= info.GetRBACPrivilege();

			if (ShouldCreateFrame<FramePosts>(moduleCriteria, sourceWindow, p_Action) && sourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				auto newFrame = new FramePosts(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModulePost_showPosts_1, _YLOC("Group Conversation Posts")).c_str(), *this, sourceWindow.GetHistoryMode(), sourceWindow.GetCFrameWndForHistory());
				newFrame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(newFrame);
				newFrame->Erect();
				frame = newFrame;
			}
			else
			{
				// No error: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
				SetAutomationGreenLight(p_Action);
				return;
			}
		}

		const bool isUpdate = nullptr != postsFrame;
		auto taskData = addFrameTask(YtriaTranslate::Do(ModulePost_showPosts_2, _YLOC("Show Group Conversation Posts")).c_str(), frame, p_Command, !isUpdate);

        GridPosts* gridPosts = dynamic_cast<GridPosts*>(&frame->GetGrid());
		if (gridPosts->GetThreadsTopics().empty())
		{
			ASSERT(!threadsTopics.empty());
			gridPosts->SetThreadsTopics(threadsTopics);
		}
        
        std::vector<GridBackendRow*> rowsWithMoreLoaded;
        gridPosts->GetRowsWithMoreLoaded(rowsWithMoreLoaded);

        auto attachmentsRequestInfo = gridPosts->GetAttachmentsRequestInfo(rowsWithMoreLoaded, {});
		YSafeCreateTask([this, threadsTopics = gridPosts->GetThreadsTopics(), subSubIds, taskData, p_Action, gridPosts, refreshSpecificData, hwnd = frame->GetSafeHwnd(), isUpdate, origin, attachmentsRequestInfo, bom = frame->GetBusinessObjectManager()]()
		{
            // Get posts
			const bool partialRefresh = !refreshSpecificData.m_Criteria.m_SubSubIDs.empty();
			const auto& ids = partialRefresh ? refreshSpecificData.m_Criteria.m_SubSubIDs : subSubIds;
            const auto postsMetaInfo = SubSubIdsToPostMetaInfos(ids);

			O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>> posts = retrievePosts(bom, threadsTopics, postsMetaInfo, taskData);

			O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>> postsWithTopic;
            for (const auto& postInfo : posts)
            {
                const auto& threadsTopics = gridPosts->GetThreadsTopics();
                auto itt = threadsTopics.find(postInfo.first.ThreadId);
                ASSERT(itt != threadsTopics.end());
                if (itt != threadsTopics.end())
                {
                    auto newKey = postInfo.first;
                    newKey.ConversationTopic = itt->second;
                    postsWithTopic[newKey] = postInfo.second;
                }
            }

            // Get attachments
			ModuleBase::AttachmentsContainer attachments;
            bool showAttachments = false;
            if (!attachmentsRequestInfo.empty())
            {
                std::vector<AttachmentsInfo::IDs> ids;
                for (const auto& postInfo : attachmentsRequestInfo)
                    ids.emplace_back(postInfo.GroupId, postInfo.ConversationId, postInfo.ThreadId, postInfo.PostId, PooledString(), postInfo.ConversationId);

				attachments = retrieveAttachments(bom, ids, origin, taskData);
                showAttachments = true;
            }

			YDataCallbackMessage<O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>>>::DoPost(posts, [hwnd, partialRefresh, postsWithTopic, taskData, p_Action, attachments, showAttachments, isUpdate, isCanceled = taskData.IsCanceled()](const O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>>& p_Posts)
			{
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FramePosts*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, isCanceled, taskData, false))
				{
					ASSERT(nullptr != frame);
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting posts for %s conversations...", Str::getStringFromNumber(postsWithTopic.size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());
                    //frame->GetGrid().ClearStatus();

					{
						CWaitCursor _;
						if (frame->HasLastUpdateOperations())
						{
							if (showAttachments)
								frame->ShowPostsWithAttachments(postsWithTopic, frame->GetLastUpdateOperations(), attachments, true, !showAttachments && !partialRefresh);
							else
								frame->ShowPosts(postsWithTopic, frame->GetLastUpdateOperations(), !showAttachments, !showAttachments && !partialRefresh);
						}
						else
						{
							if (showAttachments)
								frame->ShowPostsWithAttachments(postsWithTopic, {}, attachments, true, !showAttachments && !partialRefresh);
							else
								frame->ShowPosts(postsWithTopic, {}, !showAttachments, !showAttachments && !partialRefresh);
						}
					}

					if (!isUpdate)
					{
						frame->UpdateContext(taskData, hwnd);
						shouldFinishTask = false;
					}
				}

				if (shouldFinishTask)
				{
					// Because we didn't (and can't) call UpdateContext here.
					ModuleBase::TaskFinished(frame, taskData, p_Action);
				}
			});
		});
	}
	else
	{
		SetAutomationGreenLight(p_Command.GetAutomationAction(), _YTEXT(""));
	}
}

void ModulePost::showAttachments(const Command& p_Command)
{
	auto p_Action		= p_Command.GetAutomationAction();
	CommandInfo info	= p_Command.GetCommandInfo();

	if (info.Data().is_type<std::set<ModuleUtil::PostMetaInfo>>())
	{
		auto postsInfo		= info.Data().get_value<std::set<ModuleUtil::PostMetaInfo>>();
		FramePosts* p_Frame = dynamic_cast<FramePosts*>(info.GetFrame());

		std::vector<AttachmentsInfo::IDs> ids;
		for (const auto& postInfo : postsInfo)
			ids.emplace_back(postInfo.GroupId, postInfo.ConversationId, postInfo.ThreadId, postInfo.PostId, PooledString(), postInfo.m_Context);

		ModuleBase::showAttachments(p_Frame->GetBusinessObjectManager(), ids, p_Frame, Origin::Conversations, p_Command);
	}
	else
	{
		ASSERT(false);
		SetAutomationGreenLight(p_Action, p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2476")).c_str());
	}
}

void ModulePost::downloadAttachments(const Command& p_Command)
{
	auto p_AutomationAction = p_Command.GetAutomationAction();

    CommandInfo info = p_Command.GetCommandInfo();
	FramePosts* p_ExistingFrame = dynamic_cast<FramePosts*>(info.GetFrame());

	if (info.Data().is_type<AttachmentsInfo>())
	{
		auto& p_AttachmentInfos = info.Data().get_value<AttachmentsInfo>();
		ModuleBase::downloadAttachments(p_ExistingFrame->GetBusinessObjectManager(), p_AttachmentInfos, p_ExistingFrame, Origin::Conversations, p_Command);
	}
	else
	{
		SetAutomationGreenLight(p_AutomationAction, p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2477")).c_str());
	}
}

void ModulePost::showItemAttachment(const Command& p_Command)
{			
	auto p_AutomationAction = p_Command.GetAutomationAction();
	
	CommandInfo info	= p_Command.GetCommandInfo();
	FramePosts* p_Frame = dynamic_cast<FramePosts*>(info.GetFrame());

	ASSERT(info.Data().is_type<AttachmentsInfo::IDs>());
	if (info.Data().is_type<AttachmentsInfo::IDs>())
		ModuleBase::showItemAttachment(p_Frame->GetBusinessObjectManager(), info.Data().get_value<AttachmentsInfo::IDs>(), info.GetOrigin(), p_Frame, p_Command);
	else
		SetAutomationGreenLight(p_AutomationAction, p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2478")).c_str());

}

void ModulePost::deleteAttachments(const Command& p_Command)
{
    CommandInfo info = p_Command.GetCommandInfo();
    AutomationAction* action = p_Command.GetAutomationAction();
    FramePosts* frame = dynamic_cast<FramePosts*>(info.GetFrame());

    GridPosts* gridPosts = dynamic_cast<GridPosts*>(&frame->GetGrid());
    std::vector<GridBackendRow*> rowsWithMoreLoaded;
    gridPosts->GetRowsWithMoreLoaded(rowsWithMoreLoaded);

    ASSERT(info.Data().is_type<std::vector<SubItemKey>>());
    if (info.Data().is_type<std::vector<SubItemKey>>())
    {
        std::vector<SubItemKey> attachments = info.Data().get_value<std::vector<SubItemKey>>();

		auto taskData = addFrameTask(YtriaTranslate::Do(ModulePost_deleteAttachments_1, _YLOC("Delete Attachments")).c_str(), frame, p_Command, false);
        RefreshSpecificData refreshData = frame->GetRefreshSelectedData();
		YSafeCreateTask([this, attachments, frame, refreshData, taskData, action, gridPosts, rowsWithMoreLoaded]()
		{
            std::map<SubItemKey, HttpResultWithError> results;

            // Delete attachments
            for (auto& attachment : attachments)
                results[attachment] = frame->GetBusinessObjectManager()->DeleteGroupPostAttachment(attachment.m_Id0, attachment.m_Id1, attachment.m_Id2, attachment.m_Id3, attachment.m_Id4, taskData).GetTask().get();

            YCallbackMessage::DoPost([frame, gridPosts, refreshData, action, taskData, results]()
            {
                if (::IsWindow(frame->GetSafeHwnd()))
                {
                    gridPosts->GetModifications().SetSubItemDeletionsErrors(results);
                    gridPosts->ClearLog(taskData.GetId());
                    frame->RefreshAfterSubItemUpdates(refreshData, action);
                }
                else
                {
                    SetAutomationGreenLight(action);
                }
                TaskDataManager::Get().RemoveTaskData(taskData.GetId(), frame->GetSafeHwnd());
            });
        });
    }
    else
    {
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2479")).c_str());
    }
}

O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>> ModulePost::retrievePosts(std::shared_ptr<BusinessObjectManager> p_BOM, const std::map<PooledString, PooledString>& p_ThreadTopics, const std::vector<ModuleUtil::PostMetaInfo>& p_ViewPostInfos, YtriaTaskData taskData)
{
	O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>> posts;

	auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("posts"), p_ViewPostInfos.size());
	
    for (const auto& viewPostInfo : p_ViewPostInfos)
    {
		logger->IncrementObjCount();

		BusinessGroup group;
		group.SetID(viewPostInfo.GroupId);
		logger->SetContextualInfo(p_BOM->GetGraphCache().GetGroupContextualInfo(group.GetID()));
		p_BOM->GetGraphCache().SyncUncachedGroup(group, std::make_shared<MultiObjectsRequestLogger>(*logger), taskData);

		ASSERT(p_ThreadTopics.find(viewPostInfo.ThreadId) != p_ThreadTopics.end());
		if (p_ThreadTopics.find(viewPostInfo.ThreadId) != p_ThreadTopics.end())
			logger->SetContextualInfo(p_ThreadTopics.at(viewPostInfo.ThreadId));

        auto groupPosts = p_BOM->GetBusinessPosts(viewPostInfo.GroupId, viewPostInfo.ConversationId, viewPostInfo.ThreadId, logger, taskData).GetTask().get();
        posts[viewPostInfo] = groupPosts;
    }

    return posts;
}

vector<ModuleUtil::PostMetaInfo> ModulePost::SubSubIdsToPostMetaInfos(const O365SubSubIdsContainer& p_SubSubIds)
{
    vector<ModuleUtil::PostMetaInfo> postMetaInfos;

    for (const auto& groupConv : p_SubSubIds)
    {
        const auto& groupId = groupConv.first;
        for (const auto& convThread : groupConv.second)
        {
            const auto& convId = convThread.first.GetId();
            const auto& threadIds = convThread.second;
            for (const auto& threadId : threadIds)
            {
                ModuleUtil::PostMetaInfo postInfo;
                postInfo.ConversationId = convId;
                postInfo.GroupId = groupId;
                postInfo.ThreadId = threadId;
                postMetaInfos.push_back(postInfo);
            }
        }
    }

    return postMetaInfos;
}
