#pragma once

#include "ISessionsMigrationVersion.h"

class SQLiteEngine;

class SessionsMigrationVersion2To3 : public ISessionsMigrationVersion
{
public:
	void Build() override;
	VersionSourceTarget GetVersionInfo() const override;

private:
	bool CreateNewFromOld();
	bool Exec();
	bool UpgradeLastSessionUsed();
};

