#pragma once

#include "BusinessObject.h"

#include "KeyValue.h"
#include "AuditActivityInitiator.h"
#include "TargetResource.h"

class DirectoryAudit : public BusinessObject
{
public:
	boost::YOpt<YTimeDate>				m_ActivityDateTime;
	boost::YOpt<PooledString>			m_ActivityDisplayName;
	boost::YOpt<vector<KeyValue>>		m_AdditionalDetails;
	boost::YOpt<PooledString>			m_Category;
	boost::YOpt<PooledString>			m_CorrelationId;
	boost::YOpt<AuditActivityInitiator> m_InitiatedBy;/*XXXXXXX*/
	boost::YOpt<PooledString>			m_LoggedByService;
	boost::YOpt<PooledString>			m_OperationType;
	boost::YOpt<PooledString>			m_Result;
	boost::YOpt<PooledString>			m_ResultReason;
	boost::YOpt<vector<TargetResource>> m_TargetResources;/*XXXXXXX*/

	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
};

