#pragma once

#include "IDeserializerFactory.h"
#include "UserDeserializer.h"

class UserDeserializerFactory : public IDeserializerFactory<UserDeserializer>
{
public:
    UserDeserializerFactory(std::shared_ptr<const Sapio365Session> p_Session);
    UserDeserializer Create() override;

private:
	std::shared_ptr<const Sapio365Session>	m_Session;
};
