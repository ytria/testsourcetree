#pragma once

#include "Str.h"
#include "YtriaTranslate.h"
#include <string>

struct SessionTypes
{
    static wstring GetAppId(const wstring& p_SessionType);
    static wstring GetDisplayedSessionType(const wstring& p_SessionType);

    static web::uri_builder GetStandardSessionConsentUrl();
    static web::uri_builder GetAdvancedSessionConsentUrl(bool p_AdminConsent, bool p_PartnerAlternativeRedirectURI = false);
    static web::uri_builder GetUltraAdminConsentUrl(const wstring& p_TenantName, const wstring& p_ApplicationId, const wstring& p_RedirectURL);

	static wstring GetUserRedirectUri(const wstring& p_SessionType);
	static wstring GetBasicUserRedirectUri();
	static wstring GetAdvancedUserRedirectUri();
	static wstring GetPartnerAlternativeAdvancedUserRedirectUri();
	static wstring GetDefaultUltraAdminConsentRedirectUri();
	static wstring GetPartnerAlternativeUltraAdminConsentRedirectUri();

    static const wstring g_StandardSession;
    static const wstring g_AdvancedSession;
    static const wstring g_UltraAdmin;
    static const wstring g_Role;
    static const wstring g_ElevatedSession;
	static const wstring g_PartnerAdvancedSession;
	static const wstring g_PartnerElevatedSession;

	static bool										IsValidType(const wstring p_Type);
	static const set<wstring, Str::keyLessInsensitive>&	GetTypes();

    static const wchar_t g_StandardUserSuffix;

	static wstring GetStandardSessionAppID();
	static wstring GetAdvancedSessionAppID();
};

