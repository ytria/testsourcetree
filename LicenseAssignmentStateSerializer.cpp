#include "LicenseAssignmentStateSerializer.h"

LicenseAssignmentStateSerializer::LicenseAssignmentStateSerializer(const LicenseAssignmentState& p_LicenseAssignmentState)
	: m_LicenseAssignmentState(p_LicenseAssignmentState)
{
}

void LicenseAssignmentStateSerializer::Serialize()
{
	m_Json = web::json::value::object();
	auto& obj = m_Json.as_object();

	JsonSerializeUtil::SerializeString(_YUID("assignedByGroup"), m_LicenseAssignmentState.m_AssignedByGroup, obj);
	JsonSerializeUtil::SerializeListString(_YUID("disabledPlans"), m_LicenseAssignmentState.m_DisabledPlans, obj);
	JsonSerializeUtil::SerializeString(_YUID("error"), m_LicenseAssignmentState.m_Error, obj);
	JsonSerializeUtil::SerializeString(_YUID("skuId"), m_LicenseAssignmentState.m_SkuId, obj);
	JsonSerializeUtil::SerializeString(_YUID("state"), m_LicenseAssignmentState.m_State, obj);
}
