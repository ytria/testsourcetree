#include "IndexingPolicyDeserializer.h"

#include "IncludedPath.h"
#include "IncludedPathDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"

void Cosmos::IndexingPolicyDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("automatic"), m_Data.Automatic, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("indexingMode"), m_Data.IndexingMode, p_Object);

    {
        ListDeserializer<Cosmos::IncludedPath, Cosmos::IncludedPathDeserializer> deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("includedPaths"), p_Object))
            m_Data.IncludedPaths = std::move(deserializer.GetData());
    }
}
