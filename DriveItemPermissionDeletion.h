#pragma once

#include "ISubItemDeletion.h"

#include "GridDriveItems.h"
#include "SapioError.h"


class DriveItemPermissionDeletion : public ISubItemDeletion
{
public:
    DriveItemPermissionDeletion(GridDriveItems& p_Grid, const GridDriveItems::DriveItemPermissionInfo& p_PermissionInfo);

    virtual bool ShouldCreateFormatter(GridBackendRow* p_Row, size_t p_Index) const override;
    virtual bool IsCorrespondingRow(GridBackendRow* p_Row) const override;
	
	virtual vector<ModificationLog> GetModificationLogs() const override;

	GridBackendRow* GetMainRowIfCollapsed() const override;

private:
    GridDriveItems& m_GridDriveItems;
    GridDriveItems::DriveItemPermissionInfo m_PermissionInfo;
};

