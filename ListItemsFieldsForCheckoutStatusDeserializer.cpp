#include "ListItemsFieldsForCheckoutStatusDeserializer.h"

namespace
{
	class CheckoutStatusDeserializer : public JsonObjectDeserializer, public Encapsulate<CheckoutStatus>
	{
	protected:
		virtual void DeserializeObject(const web::json::object& p_Object) override
		{
			JsonSerializeUtil::DeserializeString(_YTEXT("CheckoutUser"), m_Data.m_CheckoutUser, p_Object);
			JsonSerializeUtil::DeserializeString(_YTEXT("_CheckinComment"), m_Data.m_CheckinComment, p_Object);
			JsonSerializeUtil::DeserializeString(_YTEXT("_ComplianceTag"), m_Data.m_ComplicanceTag, p_Object);
			JsonSerializeUtil::DeserializeTimeDate(_YTEXT("_ComplianceTagWrittenTime"), m_Data.m_ComplicanceTagWrittenTime, p_Object);
		}
	};
}

void ListItemsFieldsForCheckoutStatusDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	CheckoutStatusDeserializer csd;
	if (JsonSerializeUtil::DeserializeAny(csd, _YTEXT("fields"), p_Object))
		m_Data = std::move(csd.GetData());
}
