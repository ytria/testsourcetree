#pragma once

#include "ISqlQuery.h"
#include "SessionIdentifier.h"
#include "SessionsSqlEngine.h"

class QueryUpdateSessionType : public ISqlQuery
{
public:
	QueryUpdateSessionType(SessionsSqlEngine& p_Engine, const SessionIdentifier& p_Identifier, const wstring& p_NewSessionType);
	void Run() override;

private:
	SessionIdentifier m_Identifier;
	SessionsSqlEngine& m_Engine;
	wstring m_NewSessionType;
};

