#include "ChannelFilesFolderRequester.h"

#include "DriveItemDeserializer.h"
#include "IRequestLogger.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

ChannelFilesFolderRequester::ChannelFilesFolderRequester(const wstring& p_TeamId, const wstring& p_ChannelId, const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_TeamId(p_TeamId)
	, m_ChannelId(p_ChannelId)
	, m_Logger(p_Logger)
{

}

TaskWrapper<void> ChannelFilesFolderRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<DriveItemDeserializer>();

	web::uri_builder uri;
	uri.append_path(_YTEXT("teams"));
	uri.append_path(m_TeamId);
	uri.append_path(_YTEXT("channels"));
	uri.append_path(m_ChannelId);
	uri.append_path(_YTEXT("filesFolder"));
	//uri.append_query(_YTEXT("$select"), _YTEXT("id,webUrl,createdDateTime,lastModifiedDateTime,sharepointIds"));

	m_Logger->SetLogMessage(_T("files folder"));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, uri.to_uri(), true, m_Logger, httpLogger, p_TaskData);
}

const BusinessDriveItem& ChannelFilesFolderRequester::GetData() const
{
	ASSERT(m_Deserializer);
	return m_Deserializer->GetData();
}
