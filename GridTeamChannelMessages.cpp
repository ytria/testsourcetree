#include "GridTeamChannelMessages.h"

#include "AttachmentInfo.h"
#include "AutomationWizardTeamChannelMessages.h"
#include "BasicGridSetup.h"
#include "BusinessChatMessage.h"
#include "BusinessChatMessageReply.h"
#include "BusinessGroup.h"
#include "DlgDownloadAttachments.h"
#include "FrameTeamChannelMessages.h"
#include "GridHelpers.h"
#include "MessageBodyData.h"
#include "MessageBodyViewer.h"
#include "GridUpdater.h"
#include "O365AdminUtil.h"
#include "YCallbackMessage.h"
#include "SubItemsFlagGetter.h"
#include "PostAttachmentDeletion.h"
#include "Resource.h"

IMPLEMENT_CacheGrid_DeleteRowLParam(GridTeamChannelMessages, MessageBodyData)

BEGIN_MESSAGE_MAP(GridTeamChannelMessages, ModuleO365Grid<BusinessChatMessage>)
    ON_COMMAND(ID_GRIDTEAMCHANNELMESSAGES_SHOWBODY,				OnShowBody)
    ON_UPDATE_COMMAND_UI(ID_GRIDTEAMCHANNELMESSAGES_SHOWBODY,	OnUpdateShowBody)
END_MESSAGE_MAP()

GridTeamChannelMessages::GridTeamChannelMessages()
    : m_FrameTeamChannelMessages(nullptr)
{
    //UseDefaultActions(/*O365Grid::ACTION_CREATE | */O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT/* | O365Grid::ACTION_VIEW*/);
	//EnableGridModifications();

	initWizard<AutomationWizardTeamChannelMessages>(_YTEXT("Automation\\TeamChannelMessages"));
}

GridTeamChannelMessages::~GridTeamChannelMessages()
{
    GetBackend().Clear();
}

ModuleCriteria GridTeamChannelMessages::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_FrameTeamChannelMessages->GetModuleCriteria();
	ASSERT(criteria.m_UsedContainer == ModuleCriteria::UsedContainer::SUBIDS);

    criteria.m_SubIDs.clear();
    for (auto row : p_Rows)
    {
		if (GridUtil::IsBusinessChannel(row))
		{
			const auto& groupId = row->GetField(m_ColMetaGroupId).GetValueStr();
			const auto& channelId = row->GetField(m_ColMetaChannelId).GetValueStr();
			criteria.m_SubIDs[PooledString(groupId)].insert(channelId);
		}
    }

    return criteria;
}

void GridTeamChannelMessages::customizeGrid()
{
	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_GRIDTEAMCHANNELMESSAGES_ACCELERATOR));

	static const wstring g_FamilyGroup		= YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_1, _YLOC("Group")).c_str();
	static const wstring g_FamilyChannel	= YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_2, _YLOC("Channel")).c_str();
	static const wstring g_FamilyMessage	= YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_3, _YLOC("Message")).c_str();

	{
		BasicGridSetup setup(GridUtil::g_AutoNameChannelMessages, LicenseUtil::g_codeSapio365channelMessages,
		{
			{ &m_ColMetaGroupName, YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_4, _YLOC("Group Name")).c_str(), g_FamilyGroup },
			{ &m_ColMetaGroupId, YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_5, _YLOC("Group ID")).c_str(), g_FamilyGroup },
			{ &m_ColMetaChannelDisplayName, YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_6, _YLOC("Channel Display Name")).c_str(), g_FamilyChannel },
		});

		setup.AddMetaColumn({ &m_ColMetaChannelId, YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_7, _YLOC("Channel ID")).c_str(), g_FamilyChannel });
		setup.Setup(*this, false);
	}

	m_ColMetaChannelDisplayName->SetPresetFlags({ O365Grid::g_ColumnsPresetDefault });

	setBasicGridSetupHierarchy({ { { m_ColMetaChannelDisplayName, 0 } }
								,{ rttr::type::get<BusinessChatMessageReply>().get_id() }
								,{ rttr::type::get<BusinessChatMessageReply>().get_id(), rttr::type::get<BusinessChatMessage>().get_id(), rttr::type::get<BusinessChannel>().get_id(), rttr::type::get<BusinessGroup>().get_id() } });

	m_ColMessageIndex = AddColumnNumberInt(_YUID("index"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_8, _YLOC("#")).c_str(), g_FamilyMessage, g_ColumnSize4);
	m_ColMessageCount = AddColumnNumberInt(_YUID("count"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_9, _YLOC("Total")).c_str(), g_FamilyMessage, g_ColumnSize4);

	m_ColReplyTo	= AddColumn(_YUID("replyToId"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_10, _YLOC("Reply To")).c_str(), g_FamilyMessage, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColETag		= AddColumn(_YUID("etag"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_11, _YLOC("etag")).c_str(), g_FamilyMessage, g_ColumnSize12, { g_ColumnsPresetTech });
	// enum: message,chatevent,typing
	m_ColMessageType			= AddColumn(_YUID("messageType"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_12, _YLOC("Type")).c_str(), g_FamilyMessage, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColCreatedDateTime		= AddColumnDate(_YUID("createdDateTime"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_13, _YLOC("Created On")).c_str(), g_FamilyMessage, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColLastModifiedDateTime	= AddColumnDate(_YUID("lastModifiedDateTime"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_14, _YLOC("Last Modified On")).c_str(), g_FamilyMessage, g_ColumnSize16);
	m_ColDeleted				= AddColumnCheckBox(_YUID("deleted"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_15, _YLOC("Deleted")).c_str(), g_FamilyMessage, g_ColumnSize6);
	m_ColDeletedDateTime		= AddColumnDate(_YUID("deletedDateTime"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_16, _YLOC("Deleted On")).c_str(), g_FamilyMessage, g_ColumnSize16);
	m_ColSubject				= AddColumn(_YUID("subject"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_17, _YLOC("Subject")).c_str(), g_FamilyMessage, g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColBodyContentPreview		= AddColumnRichText(_YUID("bodyContentPreview"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_53, _YLOC("Preview")).c_str(), g_FamilyMessage, g_ColumnSize32, { g_ColumnsPresetDefault });
	// enum: text, HTML
	m_ColBodyContentType	= AddColumn(_YUID("body.contentType"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_28, _YLOC("Body - Content Type")).c_str(), g_FamilyMessage, g_ColumnSize8);
	m_ColSummary			= AddColumn(_YUID("summary"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_18, _YLOC("Summary")).c_str(), g_FamilyMessage, g_ColumnSize22, { g_ColumnsPresetDefault });
	// enum: normal, high
	m_ColImportance			= AddColumn(_YUID("importance"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_19, _YLOC("Importance")).c_str(), g_FamilyMessage, g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColLocale				= AddColumn(_YUID("locale"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_20, _YLOC("Locale")).c_str(), g_FamilyMessage, g_ColumnSize8);
	addColumnGraphID();// do not break categories

	m_ColFromAppId					= AddColumn(_YUID("from.application.id"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_21, _YLOC("From - Application ID")).c_str(), g_FamilyMessage, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColFromAppName				= AddColumn(_YUID("from.application.name"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_22, _YLOC("From - Application Name")).c_str(), g_FamilyMessage, g_ColumnSize12);
	m_ColFromDeviceId				= AddColumn(_YUID("from.device.id"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_23, _YLOC("From - Device ID")).c_str(), g_FamilyMessage, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColFromDeviceName				= AddColumn(_YUID("from.device.name"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_24, _YLOC("From - Device Name")).c_str(), g_FamilyMessage, g_ColumnSize12);
	m_ColFromUserId					= AddColumn(_YUID("from.user.id"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_25, _YLOC("From - User ID")).c_str(), g_FamilyMessage, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColFromUserDisplayName		= AddColumn(_YUID("from.user.displayName"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_26, _YLOC("From - User Display Name")).c_str(), g_FamilyMessage, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColFromUserIdentityProvider	= AddColumn(_YUID("from.user.identityProvider"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_27, _YLOC("From - User Identity Provider")).c_str(), g_FamilyMessage, g_ColumnSize12);

	static const wstring g_FamilyAttachment = YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_29, _YLOC("Attachment")).c_str();
	m_ColAttachmentId			= AddColumn(_YUID("attachment.id"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_30, _YLOC("Attachments - ID")).c_str(), g_FamilyAttachment, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColAttachmentContentType	= AddColumn(_YUID("attachment.contentType"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_31, _YLOC("Attachments - Content Type")).c_str(), g_FamilyAttachment, g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColAttachmentContentURL	= AddColumnHyperlink(_YUID("attachment.contentUrl"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_32, _YLOC("Attachments - Content URL")).c_str(), g_FamilyAttachment, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColAttachmentName			= AddColumn(_YUID("attachment.name"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_33, _YLOC("Attachments - Name")).c_str(), g_FamilyAttachment, g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColAttachmentThumbnailURL = AddColumnHyperlink(_YUID("attachment.thumbnailUrl"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_34, _YLOC("Attachments - Thumbnail URL")).c_str(), g_FamilyAttachment, g_ColumnSize8);
	m_ColAttachmentId->SetMultivalueFamily(YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_35, _YLOC("Attachments")).c_str());
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentContentType);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentContentURL);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentName);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentThumbnailURL);

	static const wstring g_FamilyMention = YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_36, _YLOC("Mention")).c_str();
	// enum: user, bot, channel, team
	m_ColMentionType	= AddColumn(_YUID("mentions.type"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_37, _YLOC("Mentions - Type")).c_str(), g_FamilyMention, g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColMentionId		= AddColumn(_YUID("mentions.id"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_38, _YLOC("Mentions - ID")).c_str(), g_FamilyMention, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColMentionText	= AddColumn(_YUID("mentions.mentionText"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_39, _YLOC("Mentions - Text")).c_str(), g_FamilyMention, g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColMentionType->SetMultivalueFamily(YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_40, _YLOC("Mentions")).c_str());
	m_ColMentionType->AddMultiValueExplosionSister(m_ColMentionId);
	m_ColMentionType->AddMultiValueExplosionSister(m_ColMentionText);

	static const wstring g_FamilyReaction = YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_41, _YLOC("Reaction")).c_str();
	// enum: like, emoji, label
	m_ColReactionType					= AddColumn(_YUID("reactions.type"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_42, _YLOC("Reactions - Type")).c_str(), g_FamilyReaction, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColReactionCreatedDateTime		= AddColumnDate(_YUID("reactions.createdDateTime"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_43, _YLOC("Reactions - Created On")).c_str(), g_FamilyReaction, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColReactionContent				= AddColumn(_YUID("reactions.content"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_44, _YLOC("Reactions - Content")).c_str(), g_FamilyReaction, g_ColumnSize22);
	m_ColReactionAppId					= AddColumn(_YUID("reactions.application.id"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_45, _YLOC("Reactions - Application ID")).c_str(), g_FamilyReaction, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColReactionAppName				= AddColumn(_YUID("reactions.application.name"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_46, _YLOC("Reactions - Application Name")).c_str(), g_FamilyReaction, g_ColumnSize12);
	m_ColReactionDeviceId				= AddColumn(_YUID("reactions.device.id"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_47, _YLOC("Reactions - Device ID")).c_str(), g_FamilyReaction, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColReactionDeviceName				= AddColumn(_YUID("reactions.device.name"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_48, _YLOC("Reactions - Device Name")).c_str(), g_FamilyReaction, g_ColumnSize12);
	m_ColReactionUserId					= AddColumn(_YUID("reactions.user.id"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_49, _YLOC("Reactions - User ID")).c_str(), g_FamilyReaction, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColReactionUserDisplayName		= AddColumn(_YUID("reactions.user.displayName"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_50, _YLOC("Reactions - User Display Name")).c_str(), g_FamilyReaction, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColReactionUserIdentityProvider	= AddColumn(_YUID("reactions.user.identityProvider"), YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_52, _YLOC("Reactions - User Identity Provider")).c_str(), g_FamilyReaction, g_ColumnSize12);
	m_ColReactionType->SetMultivalueFamily(YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_51, _YLOC("Reactions")).c_str());
	m_ColReactionType->AddMultiValueExplosionSister(m_ColReactionCreatedDateTime);
	m_ColReactionType->AddMultiValueExplosionSister(m_ColReactionContent);
	m_ColReactionType->AddMultiValueExplosionSister(m_ColReactionAppId);
	m_ColReactionType->AddMultiValueExplosionSister(m_ColReactionAppName);
	m_ColReactionType->AddMultiValueExplosionSister(m_ColReactionDeviceId);
	m_ColReactionType->AddMultiValueExplosionSister(m_ColReactionDeviceName);
	m_ColReactionType->AddMultiValueExplosionSister(m_ColReactionUserId);
	m_ColReactionType->AddMultiValueExplosionSister(m_ColReactionUserDisplayName);
	m_ColReactionType->AddMultiValueExplosionSister(m_ColReactionUserIdentityProvider);
	
	AddColumnForRowPK(m_ColMetaGroupId);
	AddColumnForRowPK(m_ColMetaChannelId);
	AddColumnForRowPK(GetColId());

	m_FrameTeamChannelMessages	= dynamic_cast<FrameTeamChannelMessages*>(GetParentFrame());
	m_Icons			= GridUtil::LoadAttachmentIcons(*this);
	m_MessageViewer = std::make_unique<MessageBodyViewer>(*this, m_ColBodyContentType, nullptr);
	m_MessageViewer->SetAddBoilerplateHtml(true);

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_FrameTeamChannelMessages);
}

wstring GridTeamChannelMessages::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Group"), m_ColMetaGroupName }, { _T("Channel"), m_ColMetaChannelDisplayName } },
			_YFORMAT(L"%s (created on %s)", p_Row->GetField(m_ColSubject).ToString().c_str(), p_Row->GetField(m_ColCreatedDateTime).ToString().c_str()));
}

BusinessChatMessage GridTeamChannelMessages::getBusinessObject(GridBackendRow* p_Row) const
{
	BusinessChatMessage message;

	{
		const auto& field = p_Row->GetField(m_ColReplyTo);
		if (field.HasValue())
			message.SetReplyToId(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = p_Row->GetField(m_ColETag);
		if (field.HasValue())
			message.SetEtag(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = p_Row->GetField(m_ColMessageType);
		if (field.HasValue())
			message.SetMessageType(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = p_Row->GetField(m_ColCreatedDateTime);
		if (field.HasValue())
			message.SetCreatedDateTime(field.GetValueTimeDate());
	}

	{
		const auto& field = p_Row->GetField(m_ColLastModifiedDateTime);
		if (field.HasValue())
			message.SetLastModifiedDateTime(field.GetValueTimeDate());
	}

	{
		const auto& field = p_Row->GetField(m_ColDeleted);
		if (field.HasValue())
			message.SetDeleted(field.GetValueBool());
	}

	{
		const auto& field = p_Row->GetField(m_ColDeletedDateTime);
		if (field.HasValue())
			message.SetDeletedDateTime(field.GetValueTimeDate());
	}

	{
		const auto& field = p_Row->GetField(m_ColSubject);
		if (field.HasValue())
			message.SetSubject(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = p_Row->GetField(m_ColSummary);
		if (field.HasValue())
			message.SetSummary(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = p_Row->GetField(m_ColImportance);
		if (field.HasValue())
			message.SetImportance(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = p_Row->GetField(m_ColLocale);
		if (field.HasValue())
			message.SetLocale(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	// From
	{
		boost::YOpt<IdentitySet> from;
		{
			const auto& field = p_Row->GetField(m_ColFromAppId);
			if (field.HasValue())
			{
				if (!from)
					from.emplace();

				if (!from->Application)
					from->Application.emplace();

				from->Application->Id = field.GetValueStr();
			}
		}

		{
			const auto& field = p_Row->GetField(m_ColFromAppName);
			if (field.HasValue())
			{
				if (!from)
					from.emplace();

				if (!from->Application)
					from->Application.emplace();

				from->Application->DisplayName = field.GetValueStr();
			}
		}

		{
			const auto& field = p_Row->GetField(m_ColFromDeviceId);
			if (field.HasValue())
			{
				if (!from)
					from.emplace();

				if (!from->Device)
					from->Device.emplace();

				from->Device->Id = field.GetValueStr();
			}
		}

		{
			const auto& field = p_Row->GetField(m_ColFromDeviceName);
			if (field.HasValue())
			{
				if (!from)
					from.emplace();

				if (!from->Device)
					from->Device.emplace();

				from->Device->DisplayName = field.GetValueStr();
			}
		}

		{
			const auto& field = p_Row->GetField(m_ColFromUserId);
			if (field.HasValue())
			{
				if (!from)
					from.emplace();

				if (!from->User)
					from->User.emplace();

				from->User->Id = field.GetValueStr();
			}
		}

		{
			const auto& field = p_Row->GetField(m_ColFromUserDisplayName);
			if (field.HasValue())
			{
				if (!from)
					from.emplace();

				if (!from->User)
					from->User.emplace();

				from->User->DisplayName = field.GetValueStr();
			}
		}

		{
			const auto& field = p_Row->GetField(m_ColFromUserIdentityProvider);
			if (field.HasValue())
			{
				if (!from)
					from.emplace();

				if (!from->User)
					from->User.emplace();

				from->User->IdentityProvider = field.GetValueStr();
			}
		}

		if (from)
			message.SetFrom(from);
	}

	{
		const auto& field = p_Row->GetField(m_ColBodyContentType);
		if (field.HasValue())
		{
			ItemBody itemBody;
			itemBody.ContentType = field.GetValueStr();
			message.SetBody(itemBody);
		}
	}

	// TODO ?
	// Attachments
	// Mentions
	// Reactions

    return message;
}

void GridTeamChannelMessages::OnGbwSelectionChangedSpecific()
{
    if (m_MessageViewer && m_MessageViewer->IsShowing())
    {
        // Try to less freeze on selection change by posting a message.
        YCallbackMessage::DoPost([this]()
        {
            if (::IsWindow(m_hWnd) && m_MessageViewer->IsShowing())
                m_MessageViewer->ShowSelectedMessageBody();
        });
    }
}

BOOL GridTeamChannelMessages::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return ModuleO365Grid<BusinessChatMessage>::PreTranslateMessage(pMsg);
}

void GridTeamChannelMessages::InitializeCommands()
{
    ModuleO365Grid<BusinessChatMessage>::InitializeCommands();

	std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

    const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_GRIDTEAMCHANNELMESSAGES_SHOWBODY;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_1, _YLOC("Preview Body...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_2, _YLOC("Preview Body...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGE_CONTENT_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_GRIDTEAMCHANNELMESSAGES_SHOWBODY, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_3, _YLOC("Preview Message Body")).c_str(),
			YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_4, _YLOC("Preview the message body.\nOnly one message can be previewed at a time.")).c_str(),
			_cmd.m_sAccelText);
	}

	/*{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_POSTSGRID_SHOWATTACHMENTS;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_5, _TYLOC("Load Attachment Info")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_6, _TYLOC("Load Attachment Info")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromBITMAPID(IDB_MESSAGES_SHOWATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_POSTSGRID_SHOWATTACHMENTS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_7, _TYLOC("Load Attachment Info")).c_str(),
			YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_8, _TYLOC("Load the additional attachment info for all selected rows.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_POSTSGRID_DOWNLOADATTACHMENTS;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_9, _TYLOC("Download Attachments...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_10, _TYLOC("Download Attachments...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromBITMAPID(IDB_MESSAGES_DOWNLOADATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_POSTSGRID_DOWNLOADATTACHMENTS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_11, _TYLOC("Download Attachments")).c_str(),
			YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_12, _TYLOC("Download the attachment files of all selected items to your computer.\r\nYou can select a destination folder in the resulting dialog.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_POSTSGRID_VIEWITEMATTACHMENT;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_13, _TYLOC("Preview Item...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_14, _TYLOC("Preview Item...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromBITMAPID(IDB_MESSAGES_VIEWITEMATTACHMENT_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_POSTSGRID_VIEWITEMATTACHMENT, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_15, _TYLOC("Preview Attached Item")).c_str(),
			YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_16, _TYLOC("Preview the body of an attached email, vCard, or calendar.\nYou must first load the attachment info and select the item in the grid for this function to be active.\nOnly one item can be previewed at a time.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
        _cmd.m_nCmdID		= ID_POSTSGRID_DELETEATTACHMENT;
        _cmd.m_sMenuText	= YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_17, _TYLOC("Delete Attachment")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_18, _TYLOC("Delete Attachment")).c_str();
        _cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromBITMAPID(IDB_MESSAGES_DELETEATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_POSTSGRID_DELETEATTACHMENT, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_19, _TYLOC("Delete Attachment")).c_str(),
			YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_20, _TYLOC("Delete all selected attachment files.")).c_str(),
			_cmd.m_sAccelText);
    }

    {
        CExtCmdItem _cmd;
        _cmd.m_nCmdID = ID_POSTSGRID_TOGGLEINLINEATTACHMENTS;
        _cmd.m_sMenuText = YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_21, _TYLOC("Show Inline Attachments")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_22, _TYLOC("Show Inline Attachments")).c_str();
        _cmd.m_sAccelText = _YTEXT("");
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

        HICON hIcon = MFCUtil::getHICONFromBITMAPID(IDB_POSTS_TOGGLEINLINEATTACHMENTS_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_POSTSGRID_TOGGLEINLINEATTACHMENTS, hIcon, false);

        setRibbonTooltip(_cmd.m_nCmdID,
            YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_23, _TYLOC("Show Inline Attachments")).c_str(),
            YtriaTranslate::Do(GridTeamChannelMessages_InitializeCommands_24, _TYLOC("Include inline attachments in the grid.\nBy default, inline attachments, like embedded images, are not listed.\nTo go back to default, use the Refresh All button.")).c_str(),
            _cmd.m_sAccelText);
    }*/
}

void GridTeamChannelMessages::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
    ModuleO365Grid<BusinessChatMessage>::OnCustomPopupMenu(pPopup, nStart);

    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {
		pPopup->ItemInsert(ID_GRIDTEAMCHANNELMESSAGES_SHOWBODY);
		pPopup->ItemInsert(); // Separator
  //      pPopup->ItemInsert(ID_POSTSGRID_SHOWATTACHMENTS);
		//pPopup->ItemInsert(ID_POSTSGRID_TOGGLEINLINEATTACHMENTS);
  //      pPopup->ItemInsert(ID_POSTSGRID_DOWNLOADATTACHMENTS);
  //      pPopup->ItemInsert(ID_POSTSGRID_VIEWITEMATTACHMENT);
  //      pPopup->ItemInsert(ID_POSTSGRID_DELETEATTACHMENT);
  //      pPopup->ItemInsert(); // Separator
    }
}

GridBackendRow* GridTeamChannelMessages::addGroup(const BusinessGroup& p_Group, GridUpdater& p_Updater)
{
	GridBackendField fieldGroupGroupId(p_Group.GetID(), m_ColMetaGroupId);
	GridBackendField fieldGroupGraphId(p_Group.GetID(), GetColId());
	GridBackendRow* groupRow = m_RowIndex.GetRow({ fieldGroupGroupId, fieldGroupGraphId }, true, true);

	ASSERT(nullptr != groupRow);
	if (nullptr != groupRow)
	{
		groupRow->SetHierarchyParentToHide(true);
		p_Updater.GetOptions().AddRowWithRefreshedValues(groupRow);
		p_Updater.GetOptions().AddPartialPurgeParentRow(groupRow);

		SetRowObjectType(groupRow, p_Group, p_Group.HasFlag(BusinessObject::CANCELED));
		groupRow->AddField(p_Group.GetDisplayName(), m_ColMetaGroupName);
	}

	return groupRow;
}

void GridTeamChannelMessages::addChannel(const BusinessChannel& p_Channel, GridBackendRow* p_ParentRow, const vector<BusinessChatMessage>& p_ChatMessages, GridUpdater& p_Updater)
{
	ASSERT(nullptr != p_ParentRow);
	if (nullptr != p_ParentRow)
	{
		GridBackendField fieldChannelGroupId = p_ParentRow->GetField(m_ColMetaGroupId);
		GridBackendField fieldChannelChannelId(p_Channel.GetID(), m_ColMetaChannelId);
		GridBackendField fieldChannelGraphId(p_Channel.GetID(), GetColId());

		GridBackendRow* channelRow = m_RowIndex.GetRow({ fieldChannelGroupId, fieldChannelChannelId, fieldChannelGraphId }, true, true);
		ASSERT(nullptr != channelRow);
		if (nullptr != channelRow)
		{
			p_Updater.GetOptions().AddRowWithRefreshedValues(channelRow);

			channelRow->SetParentRow(p_ParentRow);

			channelRow->AddField(p_ParentRow->GetField(m_ColMetaGroupName).GetValueStr(), m_ColMetaGroupName);
			channelRow->AddField(p_Channel.GetDisplayName(), m_ColMetaChannelDisplayName);

			SetRowObjectType(channelRow, p_Channel, p_Channel.HasFlag(BusinessObject::CANCELED));

			if (p_ChatMessages.size() == 1 && p_ChatMessages[0].GetError())
			{
				p_Updater.OnLoadingError(channelRow, *p_ChatMessages[0].GetError());
			}
			else
			{
				// Clear previous error
				if (channelRow->IsError())
					channelRow->ClearStatus();

				for (const auto& chatMessage : p_ChatMessages)
				{
					GridBackendRow* messageRow = addChatMessage(chatMessage, channelRow, p_Updater, 1);
					ASSERT(nullptr != messageRow);
					if (nullptr != messageRow)
						p_Updater.GetOptions().AddRowWithRefreshedValues(messageRow);
				}
			}
		}
	}
}

namespace
{
	template<class DataType>
	void pushBackOptional(const boost::YOpt<DataType>& p_Data, vector<DataType>& p_Vect)
	{
		p_Vect.push_back(p_Data ? *p_Data : DataType(GridBackendUtil::g_NoValueString));
	};

	template<>
	void pushBackOptional(const boost::YOpt<YTimeDate>& p_Data, vector<YTimeDate>& p_Vect)
	{
		p_Vect.push_back(p_Data ? *p_Data : GridBackendUtil::g_NoValueDate);
	};
}

GridBackendRow* GridTeamChannelMessages::addChatMessage(const BusinessChatMessage& p_Message, GridBackendRow* p_ParentRow, GridUpdater& p_Updater, size_t p_Index)
{
	GridBackendField fieldGroupId = p_ParentRow->GetField(m_ColMetaGroupId);
	GridBackendField fieldChannelId = p_ParentRow->GetField(m_ColMetaChannelId);
	GridBackendField fieldGraphId(p_Message.GetID(), GetColId());

	GridBackendRow* messageRow = m_RowIndex.GetRow({ fieldGroupId, fieldChannelId, fieldGraphId }, true, true);
	ASSERT(nullptr != messageRow);
	if (nullptr != messageRow)
	{
		messageRow->SetParentRow(p_ParentRow);

		messageRow->AddField(p_Index, m_ColMessageIndex);

		messageRow->AddField(p_ParentRow->GetField(m_ColMetaGroupName).GetValueStr(), m_ColMetaGroupName);
		messageRow->AddField(p_ParentRow->GetField(m_ColMetaChannelDisplayName).GetValueStr(), m_ColMetaChannelDisplayName);

		const bool isReply = p_Message.GetReplyToId().is_initialized() && !p_Message.GetReplyToId()->IsEmpty();
		if (isReply)
			SetRowObjectType(messageRow, BusinessChatMessageReply(), p_Message.HasFlag(BusinessObject::CANCELED));
		else
			SetRowObjectType(messageRow, p_Message, p_Message.HasFlag(BusinessObject::CANCELED));

		messageRow->AddField(p_Message.GetReplyToId(), m_ColReplyTo);
		messageRow->AddField(p_Message.GetEtag(), m_ColETag);
		messageRow->AddField(p_Message.GetMessageType(), m_ColMessageType);
		messageRow->AddField(p_Message.GetCreatedDateTime(), m_ColCreatedDateTime);
		messageRow->AddField(p_Message.GetLastModifiedDateTime(), m_ColLastModifiedDateTime);
		messageRow->AddField(p_Message.GetDeleted(), m_ColDeleted);
		messageRow->AddField(p_Message.GetDeletedDateTime(), m_ColDeletedDateTime);
		messageRow->AddField(p_Message.GetSubject(), m_ColSubject);
		messageRow->AddField(p_Message.GetSummary(), m_ColSummary);
		messageRow->AddField(p_Message.GetImportance(), m_ColImportance);
		messageRow->AddField(p_Message.GetLocale(), m_ColLocale);

		const auto& from = p_Message.GetFrom();
		if (from)
		{
			if (from->Application)
			{
				messageRow->AddField(from->Application->Id, m_ColFromAppId);
				messageRow->AddField(from->Application->DisplayName, m_ColFromAppName);
			}

			if (from->Device)
			{
				messageRow->AddField(from->Device->Id, m_ColFromDeviceId);
				messageRow->AddField(from->Device->DisplayName, m_ColFromDeviceName);
			}

			if (from->User)
			{
				messageRow->AddField(from->User->Id, m_ColFromUserId);
				messageRow->AddField(from->User->DisplayName, m_ColFromUserDisplayName);
				messageRow->AddField(from->User->IdentityProvider, m_ColFromUserIdentityProvider);
			}
		}

		const boost::YOpt<PooledString>& bodyContent = p_Message.GetBody() ? p_Message.GetBody()->Content : boost::none;
		const boost::YOpt<PooledString>& bodyContentType = p_Message.GetBody() ? p_Message.GetBody()->ContentType : boost::none;
		messageRow->AddField(bodyContentType, m_ColBodyContentType);
		messageRow->AddField((bodyContentType && *bodyContentType == _YTEXT("html")) ? GridUtil::ExtractHtmlBodyPreview(bodyContent) : bodyContent, m_ColBodyContentPreview);
		
		MessageBodyData* messageBodyData = (MessageBodyData*)messageRow->GetLParam();
		if (nullptr != messageBodyData)
		{
			if (bodyContent)
			{
				messageBodyData->m_BodyContent = bodyContent;
			}
			else
			{
				messageRow->SetLParam(NULL);
				delete messageBodyData;
			}
		}
		else
		{
			if (bodyContent)
				messageRow->SetLParam((LPARAM)new MessageBodyData(bodyContent));
		}

		{
			vector<PooledString> attachmentIDs;
			vector<PooledString> attachmentContentTypes;
			vector<HyperlinkItem> attachmentContentURLs;
			vector<PooledString> attachmentNames;
			vector<HyperlinkItem> attachmentThumbnailURLs;

			for (const auto& att : p_Message.GetAttachments())
			{
				pushBackOptional(att.m_Id, attachmentIDs);
				pushBackOptional(att.m_ContentType, attachmentContentTypes);
				pushBackOptional(att.m_ContentUrl ? boost::YOpt<HyperlinkItem>(*att.m_ContentUrl) : boost::none, attachmentContentURLs);
				pushBackOptional(att.m_Name, attachmentNames);
				pushBackOptional(att.m_ThumbnailUrl ? boost::YOpt<HyperlinkItem>(*att.m_ThumbnailUrl) : boost::none, attachmentThumbnailURLs);
			}

			if (!attachmentIDs.empty())
			{
				messageRow->AddField(attachmentIDs, m_ColAttachmentId);
				messageRow->AddField(attachmentContentTypes, m_ColAttachmentContentType);
				messageRow->AddFieldForHyperlink(attachmentContentURLs, m_ColAttachmentContentURL);
				messageRow->AddField(attachmentNames, m_ColAttachmentName);
				messageRow->AddFieldForHyperlink(attachmentThumbnailURLs, m_ColAttachmentThumbnailURL);
			}
		}

		{
			vector<PooledString> mentionTypes;
			vector<PooledString> mentionIds;
			vector<PooledString> mentionTexts;

			for (const auto& mention : p_Message.GetMentions())
			{
				pushBackOptional(mention.m_Type, mentionTypes);
				pushBackOptional(mention.m_Id, mentionIds);
				pushBackOptional(mention.m_MentionText, mentionTexts);
			}

			if (!mentionTypes.empty())
			{
				messageRow->AddField(mentionTypes, m_ColMentionType);
				messageRow->AddField(mentionIds, m_ColMentionId);
				messageRow->AddField(mentionTexts, m_ColMentionText);
			}
		}

		{
			vector<PooledString>	reactionTypes;
			vector<YTimeDate>		reactionCreatedDateTimes;
			vector<PooledString>	reactionContents;
			vector<PooledString>	reactionAppIds;
			vector<PooledString>	reactionAppNames;
			vector<PooledString>	reactionDeviceIds;
			vector<PooledString>	reactionDeviceNames;
			vector<PooledString>	reactionUserIds;
			vector<PooledString>	reactionUserDisplayNames;
			vector<PooledString>	reactionUserIdentityProvider;
			
			for (const auto& reaction : p_Message.GetReactions())
			{
				pushBackOptional(reaction.m_Type, reactionTypes);
				pushBackOptional(reaction.m_CreatedDateTime, reactionCreatedDateTimes);
				pushBackOptional(reaction.m_Content, reactionContents);
				if (reaction.m_User && reaction.m_User->m_Identity)
				{
					if (reaction.m_User->m_Identity->Application)
					{
						pushBackOptional(reaction.m_User->m_Identity->Application->Id, reactionAppIds);
						pushBackOptional(reaction.m_User->m_Identity->Application->DisplayName, reactionAppNames);
					}
					if (reaction.m_User->m_Identity->Device)
					{
						pushBackOptional(reaction.m_User->m_Identity->Device->Id, reactionDeviceIds);
						pushBackOptional(reaction.m_User->m_Identity->Device->DisplayName, reactionDeviceNames);
					}
					if (reaction.m_User->m_Identity->User)
					{
						pushBackOptional(reaction.m_User->m_Identity->User->Id, reactionUserIds);
						pushBackOptional(reaction.m_User->m_Identity->User->DisplayName, reactionUserDisplayNames);
						pushBackOptional(reaction.m_User->m_Identity->User->IdentityProvider, reactionUserIdentityProvider);
					}
				}
			}

			if (!reactionTypes.empty())
			{
				messageRow->AddField(reactionTypes, m_ColReactionType);
				messageRow->AddField(reactionCreatedDateTimes, m_ColReactionCreatedDateTime);
				messageRow->AddField(reactionContents, m_ColReactionContent);
				messageRow->AddField(reactionAppIds, m_ColReactionAppId);
				messageRow->AddField(reactionAppNames, m_ColReactionAppName);
				messageRow->AddField(reactionDeviceIds, m_ColReactionDeviceId);
				messageRow->AddField(reactionDeviceNames, m_ColReactionDeviceName);
				messageRow->AddField(reactionUserIds, m_ColReactionUserId);
				messageRow->AddField(reactionUserDisplayNames, m_ColReactionUserDisplayName);
				messageRow->AddField(reactionUserIdentityProvider, m_ColReactionUserIdentityProvider);
			}
		}

		if (p_ParentRow->HasField(m_ColMessageCount))
		{
			messageRow->AddField(p_ParentRow->GetField(m_ColMessageCount).GetValueLong(), m_ColMessageCount);
		}
		else if (!isReply)
		{
			size_t index = p_Index + 1;
			const auto& replies = p_Message.GetReplies();

			if (replies.size() == 1 && replies[0].GetError())
			{
				p_Updater.OnLoadingError(messageRow, *replies[0].GetError());
			}
			else
			{
				// Clear previous error
				if (messageRow->IsError())
					messageRow->ClearStatus();

				messageRow->AddField(replies.size() + 1, m_ColMessageCount);
				// Replies are sorted from the most to the least recent.
				for (auto it = replies.crbegin(); it != replies.crend(); ++it)
				{
					const auto& reply = *it;
					auto replyRow = addChatMessage(reply, messageRow, p_Updater, index++);
					ASSERT(nullptr != replyRow);
					if (nullptr != replyRow)
						p_Updater.GetOptions().AddRowWithRefreshedValues(replyRow);
				}
			}
		}
	}

	return messageRow;
}

void GridTeamChannelMessages::OnShowBody()
{
    if (nullptr != m_MessageViewer)
        m_MessageViewer->ToggleView();
}

void GridTeamChannelMessages::OnUpdateShowBody(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && GridUtil::IsSelectionAtLeastOneOfType<BusinessChatMessage>(*this) || GridUtil::IsSelectionAtLeastOneOfType<BusinessChatMessageReply>(*this));
	pCmdUI->SetCheck(nullptr != m_MessageViewer && m_MessageViewer->IsShowing());

	setTextFromProfUISCommand(*pCmdUI);
}

void GridTeamChannelMessages::BuildView(const O365DataMap<BusinessGroup, std::map<BusinessChannel, vector<BusinessChatMessage>>>& p_Messages, bool p_Refresh, bool p_FullPurge)
{
	const uint32_t options	= (p_Refresh ? GridUpdaterOptions::UPDATE_GRID : 0)
							| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
							/*| GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/
							| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
							| GridUpdaterOptions::REFRESH_PROCESS_DATA
							/*| GridUpdaterOptions::PROCESS_LOADMORE*/;

	GridUpdater updater(*this, GridUpdaterOptions(options));
    GridIgnoreModification ignoramus(*this);
	for (const auto& byGroup : p_Messages)
	{
		auto groupRow = addGroup(byGroup.first, updater);
		for (const auto& byChannel : byGroup.second)
			addChannel(byChannel.first, groupRow, byChannel.second, updater);
	}
}

void GridTeamChannelMessages::HideMessageViewer()
{
	if (m_MessageViewer && m_MessageViewer->IsShowing())
		m_MessageViewer->ToggleView();
}
