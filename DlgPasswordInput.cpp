#include "DlgPasswordInput.h"

#include "YCodeJockMessageBox.h"

DlgPasswordInput::DlgPasswordInput(CWnd* p_Parent, bool p_CreatePassword, const wstring& p_WarningIntroText/* = Str::g_EmptyString*/)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
	, m_CreatePassword(p_CreatePassword)
	, m_WarningIntroText(p_WarningIntroText)
{
}

DlgPasswordInput::~DlgPasswordInput()
{
}

wstring DlgPasswordInput::getDialogTitle() const
{
	if (m_CreatePassword)
		return _T("Create Password");
	else
		return _T("Enter Password");
}

wstring DlgPasswordInput::externalValidationCallback(const wstring p_PropertyName, const wstring p_CurrentValue)
{
	ASSERT(m_CreatePassword && p_PropertyName == _YTEXT("pwd"));

	auto asciiPwd = Str::convertToASCII(p_CurrentValue);
	return p_CurrentValue == Str::convertFromUTF8(asciiPwd) ? _YTEXT("") : _YTEXT("NotAscii");
}

void DlgPasswordInput::generateJSONScriptData()
{
	initMain(_YTEXT("DlgPasswordInput"));

	ASSERT(isEditDialog());

	if (!m_WarningIntroText.empty())
		getGenerator().addIconTextField(_YTEXT("intro"), m_WarningIntroText, _YTEXT("fas fa-exclamation-triangle"), _YTEXT("#ffcccc"), _YTEXT("#ff0000"));

	if (m_CreatePassword)
	{
		getGenerator().Add(WithExternalValidation<WithTooltip<PasswordEditor>>({ _YTEXT("pwd"), _T("Password"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, _YTEXT("") }, _YTEXT("Password must contain only ASCII characters.")));
		getGenerator().Add(PasswordEditor(_YTEXT("pwd2"), _T("Confirm Password"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, _YTEXT("")));
	}
	else
		getGenerator().Add(PasswordEditor(_YTEXT("pwd"), _T("Enter Password:"), EditorFlags::DIRECT_EDIT/* | EditorFlags::REQUIRED*/, _YTEXT("")));


	getGenerator().addApplyButton(_T("OK"));
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
}

bool DlgPasswordInput::processPostedData(const IPostedDataTarget::PostedData& data)
{
	ASSERT(m_CreatePassword && data.size() == 2 || !m_CreatePassword && data.size() == 1);

	if (m_CreatePassword && data.size() == 2 || !m_CreatePassword && data.size() == 1)
	{
		m_Password = Str::convertToUTF8(data.begin()->second);
		if (m_CreatePassword)
		{
			auto it = data.begin();
			std::advance(it, 1);
			const auto confirm = Str::convertToUTF8(it->second);

			if (confirm != m_Password)
			{
				YCodeJockMessageBox(this, DlgMessageBox::eIcon_Error, _T("Error"), _T("Password mismatch"), _YTEXT(""), { {IDOK, _T("OK")} }).DoModal();
				return false;
			}
		}
	}
	return true;
}

const std::string& DlgPasswordInput::GetPassword()
{
	return m_Password;
}
