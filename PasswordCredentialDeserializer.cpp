#include "PasswordCredentialDeserializer.h"
#include "JsonSerializeUtil.h"

void PasswordCredentialDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("customKeyIdentifier"), m_Data.m_CustomKeyIdentifier, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("endDateTime"), m_Data.m_EndDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("keyId"), m_Data.m_KeyId, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("startDateTime"), m_Data.m_StartDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("secretText"), m_Data.m_SecretText, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("hint"), m_Data.m_Hint, p_Object);
}
