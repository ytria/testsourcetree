#include "BaseO365RowInitializer.h"

#include "TypeMetadataKeys.h"

void BaseO365RowInitializer::InitializeRow(GridBackendRow& p_Row, const rttr::type& p_Type, const rttr::property* p_Property) const
{
    BasicRowInitializer::InitializeRow(p_Row, p_Type, p_Property);

    if (p_Type.is_valid())
    {
		ASSERT(FALSE); // The following code is deprecated
        auto colorMetadata = p_Type.get_metadata(MetadataKeys::CacheGridRowColor);
        if (colorMetadata.is_valid())
            p_Row.SetColor(colorMetadata.get_value<COLORREF>());
    }
}
