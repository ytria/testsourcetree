#include "OrgContactRequester.h"

#include "BusinessGroup.h" // Circular dependency mess, required for #include "BusinessOrgContact.h"
#include "BusinessOrgContact.h"
#include "IPropertySetBuilder.h"
#include "MSGraphCommonData.h"
#include "MSGraphUtil.h"
#include "MsGraphFieldNames.h"
#include "MSGraphSession.h"
#include "OrgContactDeserializer.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "BasicRequestLogger.h"
#include "MsGraphHttpRequestLogger.h"

OrgContactRequester::OrgContactRequester(PooledString p_OrgContactId, IPropertySetBuilder&& p_PropertySet, const std::shared_ptr<IRequestLogger>& p_Logger)
    : m_OrgContactId(p_OrgContactId)
	, m_Properties(std::move(p_PropertySet.GetPropertySet()))
	, m_Logger(p_Logger)
{
}

TaskWrapper<void> OrgContactRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<OrgContactDeserializer>();
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri;
    uri.append_path(Rest::CONTACTS_PATH);
    uri.append_path(m_OrgContactId);
    uri.append_query(U("$select"), Rest::GetSelectQuery(m_Properties));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
    return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, m_Result, uri.to_uri(), true, m_Logger, httpLogger, p_TaskData);
}

BusinessOrgContact& OrgContactRequester::GetData()
{
    m_Deserializer->GetData().SetID(m_OrgContactId);
    return m_Deserializer->GetData();
}

const SingleRequestResult& OrgContactRequester::GetResult() const
{
    return *m_Result;
}
