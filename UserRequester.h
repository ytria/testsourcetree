#pragma once

#include "IRequester.h"
#include "IPropertySetBuilder.h"
#include "IRequestLogger.h"
#include "Sapio365Session.h"

class BusinessUser;
class IPropertySetBuilder;
class SingleRequestResult;
class UserDeserializer;

class UserRequester : public IRequester
{
public:
	UserRequester(const PooledString& p_UserId, IPropertySetBuilder&& p_PropertySet, bool p_SwallowExceptions, const std::shared_ptr<IRequestLogger>& p_Logger);
	UserRequester(const PooledString& p_UserId, const std::vector<rttr::property>& p_Properties, const std::shared_ptr<IRequestLogger>& p_Logger); // FIXME: Remove this constructor by creating the appropriate IPropertySetBuilder(s) for group

	void SetUseBetaEndpoint(bool p_UseBetaEndpoint);

	// Default is Sapio365Session::RBAC_USER
	void SetRbacSessionMode(Sapio365Session::SessionType p_RbacSessionMode);

    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    virtual BusinessUser& GetData();
    const SingleRequestResult& GetResult() const;

protected:
	std::shared_ptr<UserDeserializer> m_Deserializer;

private:
    PooledString m_UserId;
    vector<rttr::property> m_Properties;

    std::shared_ptr<SingleRequestResult> m_Result;

	bool m_UseBetaEndpoint;
	bool m_SwallowExceptions;
	Sapio365Session::SessionType m_RbacSessionMode;

	std::shared_ptr<IRequestLogger> m_Logger;
};

using UserRequesterByEmail = UserRequester;
