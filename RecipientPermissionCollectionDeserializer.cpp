#include "RecipientPermissionCollectionDeserializer.h"

#include "RecipientPermission.h"

void RecipientPermissionCollectionDeserializer::Deserialize(IPSObjectPropertyReader& p_Reader)
{
	RecipientPermission object;
	if (p_Reader.HasProperty(L"AccessRights"))
		object.m_AccessRights = Str::explodeIntoVector<wstring>(p_Reader.GetStringProperty(L"AccessRights"), _YTEXT(" "));
	if (p_Reader.HasProperty(L"Trustee"))
		object.m_Trustee = p_Reader.GetStringProperty(L"Trustee");
	m_Data.push_back(object);
}

void RecipientPermissionCollectionDeserializer::SetCount(size_t p_Count)
{
	m_Data.reserve(p_Count);
}
