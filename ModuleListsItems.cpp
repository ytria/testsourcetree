#include "ModuleListsItems.h"

#include "AutomationDataStructure.h"
#include "BusinessObjectManager.h"
#include "CommandInfo.h"
#include "FrameListItems.h"
#include "TaskDataManager.h"
#include <utility>
#include "MultiObjectsRequestLogger.h"

void ModuleListsItems::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
    {
    case Command::ModuleTask::List:
		showListsItems(p_Command);
		break;
    default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
		break;
    }
}

void ModuleListsItems::showListsItems(const Command& p_Command)
{
    const auto& info = p_Command.GetCommandInfo();
	const auto& sitesAndLists	= info.GetSubIds();

	auto			p_ExistingFrame	= dynamic_cast<FrameListItems*>(info.GetFrame());
	auto			p_SourceWindow	= p_Command.GetSourceWindow();
	auto			p_Action		= p_Command.GetAutomationAction();
    const auto&		p_Lists         = sitesAndLists;

	auto sourceCWnd = p_SourceWindow.GetCWnd();
	auto frame = p_ExistingFrame;
	if (nullptr == p_ExistingFrame)
	{
		if (FrameListItems::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin			= Origin::Site;
			moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::SUBIDS;
			moduleCriteria.m_SubIDs			= p_Lists;
			moduleCriteria.m_Privilege		= info.GetRBACPrivilege();

			if (ShouldCreateFrame<FrameListItems>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				frame = new FrameListItems(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleListsItems_showListsItems_1, _YLOC("SharePoint Site List Items")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
		}
	}

	if (nullptr != frame)
	{
		doRefresh(frame, p_Lists, frame->GetOrigin(), p_Command, nullptr != p_ExistingFrame);
	}
	else
	{
		ASSERT(nullptr == p_Action);
		// No error: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
		SetAutomationGreenLight(p_Action/*, _YTEXT("")*/);
	}
}

pplx::task<vector<BusinessListItem>> ModuleListsItems::getListItemsWithAllFields(std::shared_ptr<BusinessObjectManager> p_BOM, BusinessList p_List, const PooledString& p_SiteId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
    return p_BOM->GetBusinessListItems(p_SiteId, p_List.GetID(), GetAllFields(p_List), p_Logger, p_TaskData);
}

vector<wstring> ModuleListsItems::GetAllFields(BusinessList &p_List)
{
    set<wstring> fieldsToLookup;
    for (const auto& listItem : p_List.GetListItems())
    {
        const auto& fields = listItem.GetFields();
        if (fields)
        {
            for (const auto& keyVal : fields->GetUnknownProperties())
            {
                wstring propName = keyVal.first;

                static const wstring g_LookupStr = _YTEXT("LookupId");
                if (Str::endsWith(propName, g_LookupStr))
                    fieldsToLookup.emplace(std::begin(propName), std::end(propName) - g_LookupStr.size());
                else
                    fieldsToLookup.insert(propName);
            }
        }
    }

    return vector<wstring>{ std::begin(fieldsToLookup), std::end(fieldsToLookup) };
}

void ModuleListsItems::doRefresh(FrameListItems* p_NewFrame, const O365SubIdsContainer& p_Lists, Origin p_Origin, const Command& p_Command, bool p_IsRefresh)
{
    auto taskData = addFrameTask(YtriaTranslate::Do(ModuleListsItems_doRefresh_1, _YLOC("Show SharePoint Site List Items...")).c_str(), p_NewFrame, p_Command, !p_IsRefresh);

    RefreshSpecificData refreshSpecificData;
    if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
        refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();
	YSafeCreateTask([this, taskData, currentModuleCriteriaIds = p_NewFrame->GetModuleCriteria().m_SubIDs, hwnd = p_NewFrame->GetSafeHwnd(), p_Origin, refreshSpecificData, p_Action = p_Command.GetAutomationSetup().m_ActionToExecute, p_Lists, bom = p_NewFrame->GetBusinessObjectManager(), p_IsRefresh]()
    {
        const bool partialRefresh = !refreshSpecificData.m_Criteria.m_SubIDs.empty();
		const O365SubIdsContainer subIds = partialRefresh ? refreshSpecificData.m_Criteria.m_SubIDs : currentModuleCriteriaIds;

		auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("list"), subIds.size());

		O365DataMap<wstring, vector<BusinessList>> lists;
        for (const auto& list : subIds)
        {
			logger->IncrementObjCount();
            PooledString siteId = list.first.GetId();
			PooledString displayName = list.first.GetDisplayName();
            for (const auto& listId : list.second)
            {
				BusinessList finalList;
				finalList.SetID(listId);
				finalList.SetDisplayName(displayName);
				logger->SetContextualInfo(GetDisplayNameOrId(finalList));

				std::vector<BusinessListItem> listItems;
				if (!taskData.IsCanceled())
				{
					finalList = bom->GetBusinessList(siteId, listId, {}, logger, taskData).GetTask().get();

					auto nonPageLogger = std::make_shared<MultiObjectsPageRequestLogger>(*logger);
					nonPageLogger->SetLogMessage(_T("list items"));
					listItems = getListItemsWithAllFields(bom, finalList, siteId, nonPageLogger, taskData).get();
				}

				if (taskData.IsCanceled())
					finalList.SetFlags(BusinessObject::CANCELED);
				else
					finalList.SetListItems(listItems);
                    
                lists[siteId].push_back(finalList);
            }
        }


		YDataCallbackMessage<O365DataMap<wstring, vector<BusinessList>>>::DoPost(lists, [hwnd, taskData, p_Origin, partialRefresh, p_Action, p_IsRefresh, isCanceled = taskData.IsCanceled()](const O365DataMap<wstring, vector<BusinessList>>& p_Lists)
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameListItems*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, false))
			{
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting list items for %s sites...", Str::getStringFromNumber(p_Lists.size()).c_str()));
				frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
				frame->GetGrid().ClearLog(taskData.GetId());

				{
					CWaitCursor _;
					frame->ShowListItems(p_Lists, p_Origin, !partialRefresh);
				}

				if (!p_IsRefresh)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, taskData, p_Action);
			}
		});
    });
}