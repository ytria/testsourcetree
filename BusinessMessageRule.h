#pragma once

#include "BusinessObject.h"
#include "BusinessMessage.h"
#include "Recipient.h"
#include "SizeRange.h"
#include <vector>

class BusinessMessageRuleActions
{
public:
	BusinessMessageRuleActions() = default;

	boost::YOpt<vector<PooledString>> m_AssignCategories;
	boost::YOpt<PooledString> m_CopyToFolderID;
	boost::YOpt<PooledString> m_CopyToFolderName;
	boost::YOpt<bool> m_Delete;
	boost::YOpt<vector<Recipient>> m_ForwardAsAttachmentTo;
	boost::YOpt<vector<Recipient>> m_ForwardTo;
	boost::YOpt<bool> m_MarkAsRead;
	boost::YOpt<PooledString> m_MarkImportance; // low, normal, high
	boost::YOpt<PooledString> m_MoveToFolderID;
	boost::YOpt<PooledString> m_MoveToFolderName;
	boost::YOpt<bool> m_PermanentDelete;
	boost::YOpt<vector<Recipient>> m_RedirectTo;
	boost::YOpt<bool> m_StopProcessingRules;

	bool m_CopyToFolderNameError = false;
	bool m_MoveToFolderNameError = false;
};

class BusinessMessageRulePredicates
{
public:
	BusinessMessageRulePredicates() = default;

	boost::YOpt<vector<PooledString>> m_BodyContains;
	boost::YOpt<vector<PooledString>> m_BodyOrSubjectContains;
	boost::YOpt<vector<PooledString>> m_Categories;
	boost::YOpt<vector<Recipient>> m_FromAddresses;
	boost::YOpt<bool> m_HasAttachments;
	boost::YOpt<vector<PooledString>> m_HeaderContains;
	boost::YOpt<PooledString> m_Importance; // low, normal, high
	boost::YOpt<bool> m_IsApprovalRequest;
	boost::YOpt<bool> m_IsAutomaticForward;
	boost::YOpt<bool> m_IsAutomaticReply;
	boost::YOpt<bool> m_IsEncrypted;
	boost::YOpt<bool> m_IsMeetingRequest;
	boost::YOpt<bool> m_IsMeetingResponse;
	boost::YOpt<bool> m_IsNonDeliveryReport;
	boost::YOpt<bool> m_IsPermissionControlled;
	boost::YOpt<bool> m_IsReadReceipt;
	boost::YOpt<bool> m_IsSigned;
	boost::YOpt<bool> m_IsVoicemail;
	boost::YOpt<PooledString> m_MessageActionFlag; // any, call, doNotForward, followUp, fyi, forward, noResponseNecessary, read, reply, replyToAll, review
	boost::YOpt<bool> m_NotSentToMe;
	boost::YOpt<vector<PooledString>> m_RecipientContains;
	boost::YOpt<vector<PooledString>> m_SenderContains;
	boost::YOpt<PooledString> m_Sensitivity; // normal, personal, private, confidential
	boost::YOpt<bool> m_SentCCMe;
	boost::YOpt<bool> m_SentOnlyToMe;
	boost::YOpt<vector<Recipient>> m_SentToAddresses;
	boost::YOpt<bool> m_SentToMe;
	boost::YOpt<bool> m_SentToOrCCMe;
	boost::YOpt<vector<PooledString>> m_SubjectContains;
	boost::YOpt<SizeRange> m_WithinSizeRange;
};

// Created by reducing BusinessObject at its minimum, get rid of this eventually.
class SapioGraphObject
{
public:
	SapioGraphObject() = default;

	void SetError(const boost::YOpt<SapioError>& p_Error);
	const boost::YOpt<SapioError>& GetError() const;

	// Custom flags to be used by any client of this class.
	enum Flag
	{
		CANCELED = 0x1,

		// Derived class flags should start at:
		CUSTOM_FLAG_BEGIN = CANCELED << 1
	};

	void SetFlags(uint32_t p_Flags);
	uint32_t GetFlags() const;
	bool HasFlag(uint32_t p_Flag) const;

private:
	uint32_t m_Flags = 0;
	boost::YOpt<SapioError> m_Error;
};

class BusinessMessageRule : public SapioGraphObject
{
public:
    BusinessMessageRule() = default;

	BusinessMessageRuleActions m_Actions;
	BusinessMessageRulePredicates m_Conditions;
	boost::YOpt<PooledString> m_DisplayName;
	BusinessMessageRulePredicates m_Exceptions;
	boost::YOpt<bool> m_HasError;
	PooledString m_Id;
	boost::YOpt<bool> m_IsEnabled;
	boost::YOpt<bool> m_IsReadOnly;
	boost::YOpt<int32_t> m_Sequence;

	// Just for editing purposes
	boost::YOpt<PooledString> m_UserId;
	bool m_ShouldDelete = false;

	RTTR_ENABLE()
	RTTR_REGISTRATION_FRIEND
};

// Only used to have a specific icon in grids (same for Actions, Conditions and Exceptions)
class BusinessMessageRuleComponent
{
public:
	RTTR_ENABLE()
	RTTR_REGISTRATION_FRIEND
};
