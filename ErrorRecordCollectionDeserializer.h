#pragma once

#include "IPSObjectCollectionDeserializer.h"
#include "ErrorRecord.h"

class ErrorRecordCollectionDeserializer : public IPSObjectCollectionDeserializer
{
public:
	void Deserialize(IPSObjectPropertyReader& p_Reader) override;
	void SetCount(size_t p_Count) override;

	const std::vector<ErrorRecord>& GetData() const;

private:
	std::vector<ErrorRecord> m_Data;
};

