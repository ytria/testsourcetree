#include "ChannelDeserializer.h"

#include "JsonSerializeUtil.h"

void ChannelDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.m_Description, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("email"), m_Data.m_Email, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isFavoriteByDefault"), m_Data.m_IsFavoriteByDefault, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	JsonSerializeUtil::DeserializeString(_YTEXT("webUrl"), m_Data.m_WebUrl, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("membershipType"), m_Data.m_MembershipType, p_Object);
}
