#pragma once

class IButtonUpdater
{
public:
	virtual ~IButtonUpdater() = default;
	virtual void Update(CCmdUI* p_CmdUi) = 0;
};

