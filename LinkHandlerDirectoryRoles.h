#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerDirectoryRoles : public IBrowserLinkHandler
{
public:
    virtual void Handle(YBrowserLink& p_Link) const override;
};

