#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ChoiceColumn.h"

class ChoiceColumnDeserializer : public JsonObjectDeserializer, public Encapsulate<ChoiceColumn>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

