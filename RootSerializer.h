#pragma once

#include "IJsonSerializer.h"

#include "Root.h"

class RootSerializer : public IJsonSerializer
{
public:
    RootSerializer(const Root& p_Root);
    void Serialize() override;

private:
    const Root& m_Root;
};

