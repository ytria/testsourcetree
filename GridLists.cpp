#include "GridLists.h"

#include "AutomationRecording.h"
#include "AutomationWizardLists.h"
#include "BasicGridSetup.h"
#include "BusinessSite.h"
#include "FrameLists.h"
#include "O365AdminUtil.h"
#include "GridUpdater.h"
#include "GraphCache.h"

BEGIN_MESSAGE_MAP(GridLists, ModuleO365Grid<BusinessList>)
	NEWMODULE_COMMAND(ID_LISTSGRID_SHOWLISTITEMS, ID_LISTSGRID_SHOWLISTITEMS_FRAME, ID_LISTSGRID_SHOWLISTITEMS_NEWFRAME, OnShowListItems, OnUpdateShowListItems)
	NEWMODULE_COMMAND(ID_LISTSGRID_SHOWCOLUMNS, ID_LISTSGRID_SHOWCOLUMNS_FRAME, ID_LISTSGRID_SHOWCOLUMNS_NEWFRAME, OnShowListColumns, OnUpdateShowListColumns)

	ON_COMMAND(ID_LISTSGRID_TOGGLESYSTEMROWSVISIBILITY,				OnToggleSystemRowsVisibility)
	ON_UPDATE_COMMAND_UI(ID_LISTSGRID_TOGGLESYSTEMROWSVISIBILITY,	OnUpdateToggleSystemRowsVisibility)
END_MESSAGE_MAP()

GridLists::GridLists()
	: m_ShowSystemRows(false)
{
    //UseDefaultActions(/*O365Grid::ACTION_CREATE | */O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT/* | O365Grid::ACTION_VIEW*/);

	initWizard<AutomationWizardLists>(_YTEXT("Automation\\Lists"));
}

void GridLists::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameLists, LicenseUtil::g_codeSapio365lists,
		{
			{ &m_ColMetaDisplayName, YtriaTranslate::Do(GridLists_customizeGrid_1, _YLOC("Site Display Name")).c_str(), g_FamilySite },
			{ &m_ColDisplayName, YtriaTranslate::Do(GridLists_customizeGrid_2, _YLOC("List Display Name")).c_str(), g_FamilyInfo },
			{ &m_ColMetaId, YtriaTranslate::Do(GridLists_customizeGrid_3, _YLOC("Site ID")).c_str(), g_FamilySite }
		});
		setup.Setup(*this, false);
	}

    m_ColDisplayName->SetPresetFlags({ g_ColumnsPresetDefault });

	m_ColDisplayName->SetUniqueID(_YUID("displayName"));
	setBasicGridSetupHierarchy({ { { m_ColMetaDisplayName, 0 } }
								, { rttr::type::get<BusinessList>().get_id() }
								, { rttr::type::get<BusinessList>().get_id(), rttr::type::get<BusinessSite>().get_id() } });
		
	m_ColListName							= AddColumn(_YUID("name"),									YtriaTranslate::Do(GridLists_customizeGrid_4, _YLOC("List Name")).c_str(),													g_FamilyInfo,					g_ColumnSize22, { g_ColumnsPresetDefault });
    m_ColDescription						= AddColumn(_YUID("description"),							YtriaTranslate::Do(GridLists_customizeGrid_5, _YLOC("Description")).c_str(),												g_FamilyInfo,					g_ColumnSize32, { g_ColumnsPresetDefault });
	static const wstring g_FamilyCreation = YtriaTranslate::Do(GridLists_customizeGrid_6, _YLOC("Creation")).c_str();
    m_ColCreatedDateTime					= AddColumnDate(_YUID("createdDateTime"),					YtriaTranslate::Do(GridLists_customizeGrid_7, _YLOC("Created On")).c_str(),													g_FamilyCreation,				g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserName					= AddColumn(_YUID("createdBy.user.name"),					YtriaTranslate::Do(GridLists_customizeGrid_8, _YLOC("Created By - Username")).c_str(),										g_FamilyCreation,				g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColCreatedByUserEmail					= AddColumn(_YUID("createdBy.user.email"),					YtriaTranslate::Do(GridLists_customizeGrid_9, _YLOC("Created By - Email")).c_str(),											g_FamilyCreation,				g_ColumnSize32);
	m_ColCreatedByUserId					= AddColumn(_YUID("createdBy.user.id"),						YtriaTranslate::Do(GridLists_customizeGrid_10, _YLOC("Created By - User ID")).c_str(),										g_FamilyCreation,				g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColCreatedByApplicationName			= AddColumn(_YUID("createdBy.application.name"),			YtriaTranslate::Do(GridLists_customizeGrid_11, _YLOC("Created By - Application Name")).c_str(),								g_FamilyCreation,				g_ColumnSize22);
    m_ColCreatedByApplicationId				= AddColumn(_YUID("createdBy.application.id"),				YtriaTranslate::Do(GridLists_customizeGrid_12, _YLOC("Created By - Application ID")).c_str(),								g_FamilyCreation,				g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColCreatedByDeviceName				= AddColumn(_YUID("createdBy.device.name"),					YtriaTranslate::Do(GridLists_customizeGrid_13, _YLOC("Created By - Device Name")).c_str(),									g_FamilyCreation,				g_ColumnSize22);
    m_ColCreatedByDeviceId					= AddColumn(_YUID("createdBy.device.id"),					YtriaTranslate::Do(GridLists_customizeGrid_14, _YLOC("Created By - Device ID")).c_str(),									g_FamilyCreation,				g_ColumnSize12, { g_ColumnsPresetTech });
	static const wstring g_FamilyLastModification = YtriaTranslate::Do(GridLists_customizeGrid_15, _YLOC("Last Modification")).c_str();
    m_ColLastModifiedDateTime				= AddColumnDate(_YUID("lastModifiedDateTime"),				YtriaTranslate::Do(GridLists_customizeGrid_16, _YLOC("Last Modified")).c_str(),												g_FamilyLastModification,		g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColLastModifiedByUserName				= AddColumn(_YUID("lastModifiedBy.user.name"),				YtriaTranslate::Do(GridLists_customizeGrid_17, _YLOC("Last Modified - Username")).c_str(),									g_FamilyLastModification,		g_ColumnSize32, { g_ColumnsPresetDefault });
    m_ColLastModifiedByUserId				= AddColumn(_YUID("lastModifiedBy.user.id"),				YtriaTranslate::Do(GridLists_customizeGrid_18, _YLOC("Last Modified - User ID")).c_str(),									g_FamilyLastModification,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColLastModifiedByUserEmail			= AddColumn(_YUID("lastModifiedBy.user.email"),				YtriaTranslate::Do(GridLists_customizeGrid_19, _YLOC("Last Modified - Email")).c_str(),										g_FamilyLastModification,		g_ColumnSize32);
    m_ColLastModifiedByApplicationName		= AddColumn(_YUID("lastModifiedBy.application.name"),		YtriaTranslate::Do(GridLists_customizeGrid_20, _YLOC("Last Modified - Application Name")).c_str(),							g_FamilyLastModification,		g_ColumnSize22);
	m_ColLastModifiedByApplicationId		= AddColumn(_YUID("lastModifiedBy.application.id"),			YtriaTranslate::Do(GridLists_customizeGrid_21, _YLOC("Last Modified - Application ID")).c_str(),							g_FamilyLastModification,		g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColLastModifiedByDeviceName			= AddColumn(_YUID("lastModifiedBy.device.name"),			YtriaTranslate::Do(GridLists_customizeGrid_22, _YLOC("Last Modified - Device Name")).c_str(),								g_FamilyLastModification,		g_ColumnSize22);
    m_ColLastModifiedByDeviceId				= AddColumn(_YUID("lastModifiedBy.device.id"),				YtriaTranslate::Do(GridLists_customizeGrid_23, _YLOC("Last Modified - Device ID")).c_str(),									g_FamilyLastModification,		g_ColumnSize12,	{ g_ColumnsPresetTech });
	m_ColListInfoHidden						= AddColumnCheckBox(_YUID("listInfo.hidden"),				YtriaTranslate::Do(GridLists_customizeGrid_24, _YLOC("Is Hidden")).c_str(),													g_FamilyInfo,					g_ColumnSize6,	{ g_ColumnsPresetDefault });
    m_ColListInfoContentTypesEnabled		= AddColumnCheckBox(_YUID("listInfo.contentTypesEnabled"),	YtriaTranslate::Do(GridLists_customizeGrid_25, _YLOC("Is Content Types Enabled")).c_str(),									g_FamilyInfo,					g_ColumnSize6,	{ g_ColumnsPresetDefault });
    m_ColListInfoTemplate					= AddColumn(_YUID("listInfo.template"),						YtriaTranslate::Do(GridLists_customizeGrid_26, _YLOC("Template")).c_str(),													g_FamilyInfo,					g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColWebUrl								= AddColumnHyperlink(_YUID("webUrl"),						YtriaTranslate::Do(GridLists_customizeGrid_27, _YLOC("URL")).c_str(),														g_FamilyInfo,					g_ColumnSize8,	{ g_ColumnsPresetDefault });
	m_ColEtag								= AddColumn(_YUID("eTag"),									YtriaTranslate::Do(GridLists_customizeGrid_28, _YLOC("eTag")).c_str(),														g_FamilyInfo,					g_ColumnSize12);
    m_ColSystem								= AddColumnCheckBox(_YUID("system"),						YtriaTranslate::Do(GridLists_customizeGrid_29, _YLOC("Is System")).c_str(),													g_FamilyInfo,					g_ColumnSize6,	{ g_ColumnsPresetDefault });
	static const wstring g_FamilyParentReference = YtriaTranslate::Do(GridLists_customizeGrid_30, _YLOC("Parent Reference")).c_str();

	m_ColParentDriveType					= AddColumn(_YUID("parentRefDriveType"),					YtriaTranslate::Do(GridLists_customizeGrid_31, _YLOC("Parent Reference - Drive Type")).c_str(),								g_FamilyParentReference,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColParentDriveID						= AddColumn(_YUID("parentRefDriveID"),						YtriaTranslate::Do(GridLists_customizeGrid_32, _YLOC("Parent Reference - Drive ID")).c_str(),								g_FamilyParentReference,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColParentName							= AddColumn(_YUID("parentRefName"),							YtriaTranslate::Do(GridLists_customizeGrid_33, _YLOC("Parent Reference - Name")).c_str(),									g_FamilyParentReference,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColParentPath							= AddColumn(_YUID("parentRefPath"),							YtriaTranslate::Do(GridLists_customizeGrid_34, _YLOC("Parent Reference - Path")).c_str(),									g_FamilyParentReference,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColParentID							= AddColumn(_YUID("parentRefID"),							YtriaTranslate::Do(GridLists_customizeGrid_35, _YLOC("Parent Reference - ID")).c_str(),										g_FamilyParentReference,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColParentShareID						= AddColumn(_YUID("parentRefShareID"),						YtriaTranslate::Do(GridLists_customizeGrid_36, _YLOC("Parent Reference - Share ID")).c_str(),								g_FamilyParentReference,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColParentSharePointListID				= AddColumn(_YUID("parentRefShPtListID"),					YtriaTranslate::Do(GridLists_customizeGrid_37, _YLOC("Parent Reference - SharePoint IDs - List ID")).c_str(),				g_FamilyParentReference,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColParentSharePointListItemID			= AddColumn(_YUID("parentRefShPtListItemID"),				YtriaTranslate::Do(GridLists_customizeGrid_38, _YLOC("Parent Reference - SharePoint IDs - List Item ID")).c_str(),			g_FamilyParentReference,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColParentSharePointListItemUniqueID	= AddColumn(_YUID("parentRefShPtListItemUniqueID"),			YtriaTranslate::Do(GridLists_customizeGrid_39, _YLOC("Parent Reference - SharePoint IDs - List Item Unique ID")).c_str(),	g_FamilyParentReference,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColParentSharePointSiteURL			= AddColumnHyperlink(_YUID("parentRefShPtSiteURL"),			YtriaTranslate::Do(GridLists_customizeGrid_40, _YLOC("Parent Reference - SharePoint IDs - Site URL")).c_str(),				g_FamilyParentReference,		g_ColumnSize8,	{ g_ColumnsPresetTech });
    m_ColParentSharePointSiteID             = AddColumn(_YUID("parentRefShPtSiteID"),					YtriaTranslate::Do(GridLists_customizeGrid_41, _YLOC("Parent Reference - SharePoint IDs - Site ID")).c_str(),				g_FamilyParentReference,		g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColParentSharePointWebID              = AddColumn(_YUID("parentRefShPtWebID"),					YtriaTranslate::Do(GridLists_customizeGrid_42, _YLOC("Parent Reference - SharePoint IDs - Web ID")).c_str(),				g_FamilyParentReference,		g_ColumnSize12, { g_ColumnsPresetTech });
	addColumnGraphID();
	AddColumnForRowPK(GetColId());
	AddColumnForRowPK(m_ColMetaId);

    m_FrameLists = dynamic_cast<FrameLists*>(GetParentFrame());
	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_FrameLists);
}

wstring GridLists::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Site"), m_ColMetaDisplayName } }, m_ColListName);
}

ModuleCriteria GridLists::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria;

    GridFrameBase* frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
    ASSERT(nullptr != frame);
    if (nullptr != frame)
    {
        criteria = frame->GetModuleCriteria();
        criteria.m_IDs.clear();
        for (auto row : p_Rows)
            criteria.m_IDs.insert(row->GetField(m_ColMetaId).GetValueStr());
    }

    return criteria;
}

void GridLists::BuildView(const O365DataMap<BusinessSite, vector<BusinessList>>& p_Lists, Origin p_Origin, bool p_FullPurge)
{
	GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID
		| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		| GridUpdaterOptions::REFRESH_PROCESS_DATA
    ));
    GridIgnoreModification ignoramus(*this);
	for (const auto& keyVal : p_Lists)
    {
		auto& site = keyVal.first;
		GridBackendRow* parentRow = m_RowIndex.GetRow({ GridBackendField(site.GetID(), m_ColId), GridBackendField(site.GetID(), m_ColMetaId) }, true, true);
        ASSERT(nullptr != parentRow);
        if (nullptr != parentRow)
        {
			updater.GetOptions().AddRowWithRefreshedValues(parentRow);
            updater.GetOptions().AddPartialPurgeParentRow(parentRow);

            parentRow->SetHierarchyParentToHide(true);

			parentRow->AddField(GetGraphCache().GetSiteHierarchyDisplayName(site.GetID()), m_ColMetaDisplayName);
            parentRow->AddField(site.GetID(), m_ColMetaId);
			SetRowObjectType(parentRow, site, site.HasFlag(BusinessObject::CANCELED));
            
            if (keyVal.second.size() == 1 && keyVal.second[0].GetError())
            {
                updater.OnLoadingError(parentRow, *keyVal.second[0].GetError());
            }
            else
            {
				// Clear previous error
				if (parentRow->IsError())
					parentRow->ClearStatus();

                for (const auto& list : keyVal.second)
                {
                    auto listRow = FillRow(site, list);
					ASSERT(nullptr != listRow);
					if (nullptr != listRow)
					{
						updater.GetOptions().AddRowWithRefreshedValues(listRow);
						listRow->SetParentRow(parentRow);
					}					
                }
            }
        }
    }
}

BusinessList GridLists::getBusinessObject(GridBackendRow* p_Row) const
{
    BusinessList businessList;
    
    const auto& field		= p_Row->GetField(m_ColId);
	const auto& fieldMeta	= p_Row->GetField(m_ColMetaId);
	if (field.HasValue() && fieldMeta.HasValue())
        businessList.SetID(field.GetValueStr());

    return businessList;
}

GridBackendRow* GridLists::FillRow(const BusinessSite& p_Site, const BusinessList& p_List)
{
	GridBackendRow* row = m_RowIndex.GetRow({ GridBackendField(p_List.GetID(), m_ColId) , GridBackendField(p_Site.GetID(), m_ColMetaId) }, true, true);
    ASSERT(nullptr != row);
    if (nullptr != row)
    {
		SetRowObjectType(row, p_List, p_List.HasFlag(BusinessObject::CANCELED));

        row->AddField(p_Site.GetID(), m_ColMetaId);
        row->AddField(GetGraphCache().GetSiteHierarchyDisplayName(p_Site.GetID()), m_ColMetaDisplayName);

        if (p_List.GetCreatedBy())
        {
            auto createdBy = p_List.GetCreatedBy();
            if (createdBy->User)
            {
                row->AddField(createdBy->User->DisplayName, m_ColCreatedByUserName);
                row->AddField(createdBy->User->Id, m_ColCreatedByUserId);
                row->AddField(createdBy->User->Email, m_ColCreatedByUserEmail);
            }
			else
			{
				row->RemoveField(m_ColCreatedByUserName);
				row->RemoveField(m_ColCreatedByUserId);
				row->RemoveField(m_ColCreatedByUserEmail);
			}

            if (createdBy->Application)
            {
                row->AddField(createdBy->Application->DisplayName, m_ColCreatedByApplicationName);
                row->AddField(createdBy->Application->Id, m_ColCreatedByApplicationId);
            }
			else
			{
				row->RemoveField(m_ColCreatedByApplicationName);
				row->RemoveField(m_ColCreatedByApplicationId);
			}

            if (createdBy->Device)
            {
                row->AddField(createdBy->Device->DisplayName, m_ColCreatedByDeviceName);
                row->AddField(createdBy->Device->Id, m_ColCreatedByDeviceId);
            }
			else
			{
				row->RemoveField(m_ColCreatedByDeviceName);
				row->RemoveField(m_ColCreatedByDeviceId);
			}
        }
		else
		{
			row->RemoveField(m_ColCreatedByUserName);
			row->RemoveField(m_ColCreatedByUserId);
			row->RemoveField(m_ColCreatedByUserEmail);
			row->RemoveField(m_ColCreatedByApplicationName);
			row->RemoveField(m_ColCreatedByApplicationId);
			row->RemoveField(m_ColCreatedByDeviceName);
			row->RemoveField(m_ColCreatedByDeviceId);
		}

        row->AddField(p_List.GetName(), m_ColListName);
        row->AddField(p_List.GetCreatedDateTime(), m_ColCreatedDateTime);
        row->AddField(p_List.GetDescription(), m_ColDescription);
        if (p_List.GetLastModifiedBy())
        {
            auto lastModifiedBy = p_List.GetLastModifiedBy();
            if (lastModifiedBy->User)
            {
                row->AddField(lastModifiedBy->User->DisplayName, m_ColLastModifiedByUserName);
                row->AddField(lastModifiedBy->User->Id, m_ColLastModifiedByUserId);
                row->AddField(lastModifiedBy->User->Email, m_ColLastModifiedByUserEmail);
            }
			else
			{
				row->RemoveField(m_ColLastModifiedByUserName);
				row->RemoveField(m_ColLastModifiedByUserId);
				row->RemoveField(m_ColLastModifiedByUserEmail);
			}

            if (lastModifiedBy->Application)
            {
                row->AddField(lastModifiedBy->Application->DisplayName, m_ColLastModifiedByApplicationName);
                row->AddField(lastModifiedBy->Application->Id, m_ColLastModifiedByApplicationId);
            }
			else
			{
				row->RemoveField(m_ColLastModifiedByApplicationName);
				row->RemoveField(m_ColLastModifiedByApplicationId);
			}

            if (lastModifiedBy->Device)
            {
                row->AddField(lastModifiedBy->Device->DisplayName, m_ColLastModifiedByDeviceName);
                row->AddField(lastModifiedBy->Device->Id, m_ColLastModifiedByDeviceId);
            }
			else
			{
				row->RemoveField(m_ColLastModifiedByDeviceName);
				row->RemoveField(m_ColLastModifiedByDeviceId);
			}
        }
		else
		{
			row->RemoveField(m_ColLastModifiedByUserName);
			row->RemoveField(m_ColLastModifiedByUserId);
			row->RemoveField(m_ColLastModifiedByUserEmail);
			row->RemoveField(m_ColLastModifiedByApplicationName);
			row->RemoveField(m_ColLastModifiedByApplicationId);
			row->RemoveField(m_ColLastModifiedByDeviceName);
			row->RemoveField(m_ColLastModifiedByDeviceId);
		}

        row->AddField(p_List.GetLastModifiedDateTime(), m_ColLastModifiedDateTime);
        row->AddFieldForHyperlink(p_List.GetWebUrl(), m_ColWebUrl);
        row->AddField(p_List.GetDisplayName(), m_ColDisplayName);
        if (p_List.GetListInfo())
        {
            const auto& listInfo = *p_List.GetListInfo();
            row->AddField(listInfo.ContentTypesEnabled, m_ColListInfoContentTypesEnabled);
            row->AddField(listInfo.Hidden, m_ColListInfoHidden);
            row->AddField(listInfo.Template, m_ColListInfoTemplate);
        }
		else
		{
			row->RemoveField(m_ColListInfoContentTypesEnabled);
			row->RemoveField(m_ColListInfoHidden);
			row->RemoveField(m_ColListInfoTemplate);
		}

        row->AddField(p_List.GetEtag(), m_ColEtag);
		if (p_List.GetSystem())
		{
			row->AddField(true, m_ColSystem);
			row->SetTechHidden(!m_ShowSystemRows);
		}
		else
		{
			row->RemoveField(m_ColSystem);
		}

        if (p_List.GetParentReference())
        {
            const auto& parentRef = p_List.GetParentReference();
            row->AddField(parentRef->DriveType, m_ColParentDriveType);
            row->AddField(parentRef->DriveId, m_ColParentDriveID);
            row->AddField(parentRef->Name, m_ColParentName);
            row->AddField(parentRef->Path, m_ColParentPath);
            row->AddField(parentRef->Id, m_ColParentID);
            row->AddField(parentRef->ShareId, m_ColParentShareID);
            if (parentRef->SharepointIds)
            {
                row->AddField(parentRef->SharepointIds->ListId, m_ColParentSharePointListID);
                row->AddField(parentRef->SharepointIds->ListItemId, m_ColParentSharePointListItemID);
                row->AddField(parentRef->SharepointIds->ListItemUniqueId, m_ColParentSharePointListItemUniqueID);
                row->AddField(parentRef->SharepointIds->SiteUrl, m_ColParentSharePointSiteURL);
                row->AddField(parentRef->SharepointIds->SiteId, m_ColParentSharePointSiteID);
                row->AddField(parentRef->SharepointIds->WebId, m_ColParentSharePointWebID);
            }
			else
			{
				row->RemoveField(m_ColParentSharePointListID);
				row->RemoveField(m_ColParentSharePointListItemID);
				row->RemoveField(m_ColParentSharePointListItemUniqueID);
				row->RemoveField(m_ColParentSharePointSiteURL);
				row->RemoveField(m_ColParentSharePointSiteID);
				row->RemoveField(m_ColParentSharePointWebID);
			}
        }
		else
		{
			row->RemoveField(m_ColParentDriveType);
			row->RemoveField(m_ColParentDriveID);
			row->RemoveField(m_ColParentName);
			row->RemoveField(m_ColParentPath);
			row->RemoveField(m_ColParentID);
			row->RemoveField(m_ColParentShareID);
			row->RemoveField(m_ColParentSharePointListID);
			row->RemoveField(m_ColParentSharePointListItemID);
			row->RemoveField(m_ColParentSharePointListItemUniqueID);
			row->RemoveField(m_ColParentSharePointSiteURL);
			row->RemoveField(m_ColParentSharePointSiteID);
			row->RemoveField(m_ColParentSharePointWebID);
		}
    }

    return row;
}

void GridLists::InitializeCommands()
{
    ModuleO365Grid<BusinessList>::InitializeCommands();

    const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_LISTSGRID_SHOWLISTITEMS;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridLists_InitializeCommands_1, _YLOC("Show List Items...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridLists_InitializeCommands_2, _YLOC("Show List Items...")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LISTS_SHOWLISTITEMS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_LISTSGRID_SHOWLISTITEMS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridLists_InitializeCommands_3, _YLOC("Show List Items")).c_str(),
			YtriaTranslate::Do(GridLists_InitializeCommands_4, _YLOC("Show all the items for the selected lists.")).c_str(),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_LISTSGRID_SHOWLISTITEMS_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridLists_InitializeCommands_5, _YLOC("Show List Items...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_LISTSGRID_SHOWLISTITEMS_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridLists_InitializeCommands_15, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;
        _cmd.m_nCmdID = ID_LISTSGRID_SHOWCOLUMNS;
        _cmd.m_sMenuText = YtriaTranslate::Do(GridLists_InitializeCommands_6, _YLOC("Show List Columns...")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridLists_InitializeCommands_7, _YLOC("Show List Columns...")).c_str();
        _cmd.m_sAccelText = _YTEXT("");
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LISTS_SHOWCOLUMNS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_LISTSGRID_SHOWCOLUMNS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridLists_InitializeCommands_8, _YLOC("Show List Columns")).c_str(),
			YtriaTranslate::Do(GridLists_InitializeCommands_9, _YLOC("Show the columns for the selected lists.")).c_str(),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_LISTSGRID_SHOWCOLUMNS_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridLists_InitializeCommands_10, _YLOC("Show List Columns...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_LISTSGRID_SHOWCOLUMNS_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridLists_InitializeCommands_16, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_LISTSGRID_TOGGLESYSTEMROWSVISIBILITY;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridLists_InitializeCommands_11, _YLOC("Show System-managed Lists")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridLists_InitializeCommands_12, _YLOC("Show System Lists")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LISTS_TOGGLE_SYSTEMROWS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_LISTSGRID_TOGGLESYSTEMROWSVISIBILITY, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridLists_InitializeCommands_13, _YLOC("Show System Lists")).c_str(),
			YtriaTranslate::Do(GridLists_InitializeCommands_14, _YLOC("This will toggle the display of all system lists in the current grid.\r\nThese rows are hidden by default.\r\nThe 'Is System' column can be managed in the Grid Manager.")).c_str(),
			_cmd.m_sAccelText);
    }
}

void GridLists::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
    ModuleO365Grid<BusinessList>::OnCustomPopupMenu(pPopup, nStart);

    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {
		pPopup->ItemInsert(ID_LISTSGRID_TOGGLESYSTEMROWSVISIBILITY);
		pPopup->ItemInsert(); // Sep
		pPopup->ItemInsert(ID_LISTSGRID_SHOWLISTITEMS);
        pPopup->ItemInsert(ID_LISTSGRID_SHOWCOLUMNS);
        pPopup->ItemInsert(); // Sep		
    }
}

void GridLists::OnShowListItems()
{
	if (openModuleConfirmation())
	{
		auto lists = GetSelectedListsIDs();
		CommandInfo info;
		info.GetSubIds() = lists;
		info.SetRBACPrivilege(m_FrameLists->GetModuleCriteria().m_Privilege);
		CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::ListsItem, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowListItems, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2444")).c_str());
	}
}

void GridLists::OnUpdateShowListItems(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(IsFrameConnected() && GridUtil::IsSelectionAtLeastOneOfType<BusinessList>(*this));
    setTextFromProfUISCommand(*pCmdUI);
}

void GridLists::OnShowListColumns()
{
	if (openModuleConfirmation())
	{
		CommandInfo info;
		info.SetOrigin(Origin::Lists);
		info.GetSubIds() = GetSelectedListsIDs();
		info.SetRBACPrivilege(m_FrameLists->GetModuleCriteria().m_Privilege);
		CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Columns, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowListColumns, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2445")).c_str());
	}
}

void GridLists::OnUpdateShowListColumns(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(IsFrameConnected() && GridUtil::IsSelectionAtLeastOneOfType<BusinessList>(*this));
    setTextFromProfUISCommand(*pCmdUI);
}

void GridLists::ShowSystemLists(const bool p_Show)
{
	m_ShowSystemRows = p_Show;
	const vector< GridBackendRow* >& Rows = GetBackend().GetBackendRows();
	for (auto& row : Rows)
	{
		ASSERT(nullptr != row);
		if (nullptr != row && !row->IsGroupRow())
		{
			const GridBackendField&	Field = row->GetField(m_ColSystem);
			if (Field.HasValue() && Field.GetValueBool())
				row->SetTechHidden(!m_ShowSystemRows);
		}
	}

	UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);

	AutomationRecording::AddGridAction(p_Show ? g_ActionNameShowSystemLists : g_ActionNameHideSystemLists, this, true);
}

void GridLists::OnToggleSystemRowsVisibility()
{
	ShowSystemLists(!m_ShowSystemRows);
}

void GridLists::OnUpdateToggleSystemRowsVisibility(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_ShowSystemRows);
	setTextFromProfUISCommand(*pCmdUI);
}

O365IdsContainer GridLists::GetSelectedIds() const
{
	O365IdsContainer result;

    for (auto row : GetBackend().GetSelectedRowsInOrder())
    {
        ASSERT(nullptr != row);
        if (nullptr != row && !row->IsGroupRow())
        {
            const GridBackendField& idField = row->GetField(m_ColId);
            if (idField.HasValue() && GridUtil::isBusinessType<BusinessList>(row))
                result.insert(idField.GetValueStr());
        }
    }

    return result;
}

O365SubIdsContainer GridLists::GetSelectedListsIDs()
{
	O365SubIdsContainer list;

    for (auto row : GetBackend().GetSelectedRowsInOrder())
    {
        ASSERT(nullptr != row);
        if (GridUtil::isBusinessType<BusinessList>(row))
        {
			PooledString id, displayName;

			const auto& siteIdField = row->GetField(m_ColMetaId);
			ASSERT(siteIdField.HasValue());
			if (siteIdField.HasValue())
				id = siteIdField.GetValueStr();

			const auto& dispNameField = row->GetField(m_ColDisplayName);
			ASSERT(dispNameField.HasValue());
			if (dispNameField.HasValue())
				displayName = dispNameField.GetValueStr();

            const GridBackendField& listIdField = row->GetField(m_ColId);
            ASSERT(listIdField.HasValue());
            if (listIdField.HasValue())
				list[SubIdKey(id, displayName)].insert(PooledString(listIdField.GetValueStr()));
        }
    }
    return list;
}
