#include "MailboxPermissionsUpdaterCommand.h"

#include "AddMailboxPermissionRequester.h"
#include "AutomationNames.h"
#include "AutomationUtil.h"
#include "FrameMailboxPermissions.h"
#include "AutomationDataStructure.h"
#include "O365AdminUtil.h"
#include "RemoveMailboxPermissionRequester.h"
#include "safeTaskCall.h"
#include "MultiObjectsRequestLogger.h"

MailboxPermissionsUpdaterCommand::MailboxPermissionsUpdaterCommand(FrameMailboxPermissions& p_Frame)
	:IActionCommand(g_ActionNameUpdateMailboxPermissions),
	m_Frame(p_Frame)
{
}

void MailboxPermissionsUpdaterCommand::AddAddition(const MailboxPermissionsAddModification& p_Mod)
{
	m_Additions.push_back(p_Mod);
}

void MailboxPermissionsUpdaterCommand::AddEdition(const MailboxPermissionsEditModification& p_Mod)
{
	m_Editions.push_back(p_Mod);
}

void MailboxPermissionsUpdaterCommand::AddRemoval(const MailboxPermissionsRemoveModification& p_Mod)
{
	m_Removals.push_back(p_Mod);
}

void MailboxPermissionsUpdaterCommand::ExecuteImpl() const
{
	ASSERT(m_Setup);
	if (!m_Setup)
		return;

	auto setup = *m_Setup;

	auto taskData = Util::AddFrameTask(_T("Update mailbox permissions"), &m_Frame, setup, m_Frame.GetLicenseContext(), false);

	YSafeCreateTaskMutable([callback = m_Callback, editions = m_Editions, additions = m_Additions, removals = m_Removals, bom = m_Frame.GetBusinessObjectManager(), hwnd = m_Frame.GetSafeHwnd(), taskData]() mutable {
		
		auto loggerEditions = std::make_shared<MultiObjectsRequestLogger>(_T("permission editions"), editions.size(), _T("Processing"));
		ApplyEditions(editions, bom, loggerEditions, taskData);
		auto loggerAdditions = std::make_shared<MultiObjectsRequestLogger>(_T("permission additions"), additions.size(), _T("Processing"));
		ApplyAdditions(additions, bom, loggerAdditions, taskData);
		auto loggerRemovals = std::make_shared<MultiObjectsRequestLogger>(_T("permission removals"), removals.size(), _T("Processing"));
		ApplyRemovals(removals, bom, loggerRemovals, taskData);

		YCallbackMessage::DoPost([=]() mutable {
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameMailboxPermissions*>(CWnd::FromHandle(hwnd)) : nullptr;

			ASSERT(nullptr != frame);
			if (nullptr != frame)
			{
				//frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s education users...", Str::getStringFromNumber(eduUsers->size()).c_str()));
				frame->GetGrid().ClearLog(taskData.GetId());

				frame->GetMailboxGrid().SetUpdatedEditions(std::move(editions));
				frame->GetMailboxGrid().SetUpdatedAdditions(std::move(additions));
				frame->GetMailboxGrid().SetUpdatedRemovals(std::move(removals));

				if (shouldFinishTask)
					frame->TaskFinished(taskData, nullptr, hwnd);

				if (callback)
					callback();
			}
		});
	});
}

void MailboxPermissionsUpdaterCommand::ApplyAdditions(vector<MailboxPermissionsAddModification>& p_Additions, const std::shared_ptr<BusinessObjectManager>& p_Bom, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData taskData)
{
	for (auto& mod : p_Additions)
	{
		if (taskData.IsCanceled())
			return;

		const wstring& owner = mod.GetMailboxOwner();
		const wstring& user = mod.GetUserId();

		p_Logger->IncrementObjCount();
		p_Logger->SetContextualInfo(user);

		ASSERT(!mod.GetPermissions().empty());
		if (!mod.GetPermissions().empty())
		{
			auto addRequester = std::make_shared<AddMailboxPermissionRequester>(owner, user, Str::implode(mod.GetPermissions(), _YTEXT(",")), p_Logger);
			safeTaskCall(addRequester->Send(p_Bom->GetSapio365Session(), taskData),
				p_Bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION),
				taskData).get();
			if (!addRequester->GetResult()->Get().m_Success)
				mod.SetResultError(GetPSErrorString(addRequester->GetResult()));
		}
	}
}

void MailboxPermissionsUpdaterCommand::ApplyEditions(vector<MailboxPermissionsEditModification>& p_Editions, const std::shared_ptr<BusinessObjectManager>& p_Bom, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData taskData)
{
	for (auto& mod : p_Editions)
	{
		if (taskData.IsCanceled())
			return;

		const wstring& owner = mod.GetMailboxOwner();
		const wstring& user = mod.GetUser();
	
		p_Logger->IncrementObjCount();
		p_Logger->SetContextualInfo(user);

		ASSERT(!mod.GetRightsToRemove().empty() || !mod.GetRightsToAdd().empty());
		
		bool success = true;
		wstring error;
		if (!mod.GetRightsToRemove().empty())
		{
			auto removeRequester = std::make_shared<RemoveMailboxPermissionRequester>(owner, user, mod.GetRightsToRemove(), p_Logger);
			safeTaskCall(removeRequester->Send(p_Bom->GetSapio365Session(), taskData),
				p_Bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION),
				taskData).get();
			success = removeRequester->GetResult()->Get().m_Success;
			if (!success)
				error = GetPSErrorString(removeRequester->GetResult());
		}
		if (success && !mod.GetRightsToAdd().empty())
		{
			auto addRequester = std::make_shared<AddMailboxPermissionRequester>(owner, user, mod.GetRightsToAdd(), p_Logger);
			safeTaskCall(addRequester->Send(p_Bom->GetSapio365Session(), taskData),
				p_Bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION),
				taskData).get();
			success = addRequester->GetResult()->Get().m_Success;
			if (!success)
				error = GetPSErrorString(addRequester->GetResult());
		}

		if (!success)
			mod.SetResultError(error);
	}
}

void MailboxPermissionsUpdaterCommand::ApplyRemovals(vector<MailboxPermissionsRemoveModification>& p_Removals, const std::shared_ptr<BusinessObjectManager>& p_Bom, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData taskData)
{
	for (auto& mod : p_Removals)
	{
		if (taskData.IsCanceled())
			return;

		const wstring& owner = mod.GetMailboxOwner();
		const wstring& user = mod.GetUserId();

		p_Logger->IncrementObjCount();
		p_Logger->SetContextualInfo(user);

		ASSERT(!mod.GetPermissions().empty());
		if (!mod.GetPermissions().empty())
		{
			auto removeRequester = std::make_shared<RemoveMailboxPermissionRequester>(owner, user, Str::implode(mod.GetPermissions(), _YTEXT(",")), p_Logger);
			safeTaskCall(removeRequester->Send(p_Bom->GetSapio365Session(), taskData),
				p_Bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION),
				taskData).get();
			if (!removeRequester->GetResult()->Get().m_Success)
				mod.SetResultError(GetPSErrorString(removeRequester->GetResult()));
		}
	}
}

