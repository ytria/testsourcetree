#include "DriveDeserializer.h"

#include "IdentitySetDeserializer.h"
#include "JsonSerializeUtil.h"
#include "MsGraphFieldNames.h"
#include "QuotaDeserializer.h"
#include "SharepointIdsDeserializer.h"

void DriveDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		IdentitySetDeserializer deserializer;
		if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT(O365_DRIVECREATEDBY), p_Object))
			m_Data.SetCreatedBy(deserializer.GetData());
	}
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT(O365_DRIVECREATEDDATETIME), m_Data.m_CreatedDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT(O365_DRIVEDESCRIPTION), m_Data.m_Description, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_DRIVETYPE), m_Data.m_DriveType, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT(O365_ID), m_Data.m_Id, p_Object, true);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT(O365_DRIVELASTMODIFIEDDATETIME), m_Data.m_LastModifiedDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT(O365_DRIVENAME), m_Data.m_Name, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT(O365_DRIVEWEBURL), m_Data.m_WebUrl, p_Object);

	{
		IdentitySetDeserializer deserializer;
		if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT(O365_DRIVEOWNER), p_Object))
			m_Data.SetOwner(deserializer.GetData());
	}

	{
		QuotaDeserializer deserializer;
		if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT(O365_DRIVEQUOTA), p_Object))
			m_Data.SetQuota(deserializer.GetData());
	}

	{
		SharepointIdsDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("sharePointIds"), p_Object))
			m_Data.SetSharepointIds(d.GetData());
	}
}
