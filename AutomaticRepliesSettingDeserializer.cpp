#include "AutomaticRepliesSettingDeserializer.h"

#include "DateTimeTimeZoneDeserializer.h"
#include "JsonSerializeUtil.h"

void AutomaticRepliesSettingDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("externalAudience"), m_Data.ExternalAudience, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("externalReplyMessage"), m_Data.ExternalReplyMessage, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("internalReplyMessage"), m_Data.InternalReplyMessage, p_Object);
    {
        DateTimeTimeZoneDeserializer dttzd;
        if (JsonSerializeUtil::DeserializeAny(dttzd, _YTEXT("scheduledEndDateTime"), p_Object))
            m_Data.ScheduledEndDateTime = dttzd.GetData();
    }
    {
        DateTimeTimeZoneDeserializer dttzd;
        if (JsonSerializeUtil::DeserializeAny(dttzd, _YTEXT("scheduledStartDateTime"), p_Object))
            m_Data.ScheduledStartDateTime = dttzd.GetData();
    }
	JsonSerializeUtil::DeserializeString(_YTEXT("status"), m_Data.Status, p_Object);
}
