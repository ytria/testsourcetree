#include "SpGroup.h"

RTTR_REGISTRATION
{
    using namespace rttr;
registration::class_<Sp::Group>("SpGroup") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("SharePoint Group"))))
.constructor()(policy::ctor::as_object);
}
