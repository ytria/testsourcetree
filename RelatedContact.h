#pragma once

class RelatedContact
{
public:
	boost::YOpt<PooledString> m_Id;
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<PooledString> m_EmailAddress;
	boost::YOpt<PooledString> m_MobilePhone;
	boost::YOpt<PooledString> m_Relationship;
	boost::YOpt<bool> m_AccessConsent;
};

