#include "BusinessAssignedLicense.h"

BusinessAssignedLicense::BusinessAssignedLicense()
{
}

BusinessAssignedLicense::BusinessAssignedLicense(const AssignedLicense& p_AssignedLicense)
	: m_AssignedLicense(p_AssignedLicense)
{
	SetID(p_AssignedLicense.m_SkuId);
}

const AssignedLicense& BusinessAssignedLicense::ToAssignedLicense() const
{
	m_AssignedLicense.m_SkuId = GetID();
	return m_AssignedLicense;
}

const boost::YOpt<vector<PooledString>>& BusinessAssignedLicense::GetDisabledPlans() const
{
	return m_AssignedLicense.m_DisabledPlans;
}

void BusinessAssignedLicense::SetDisabledPlans(const boost::YOpt<vector<PooledString>>& p_DisabledPlans)
{
	m_AssignedLicense.m_DisabledPlans = p_DisabledPlans;
}

const boost::YOpt<PooledString>& BusinessAssignedLicense::GetSkuPartNumber() const
{
	return m_SkuPartNumber;
}

void BusinessAssignedLicense::SetSkuPartNumber(const boost::YOpt<PooledString>& p_SkuPartNumber)
{
	m_SkuPartNumber = p_SkuPartNumber;
}

const PooledString& BusinessAssignedLicense::GetSkuId() const
{
	return GetID();
}

void BusinessAssignedLicense::SetSkuId(const PooledString& p_SkuId)
{
	SetID(p_SkuId);
}

