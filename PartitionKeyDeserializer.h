#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "PartitionKey.h"

namespace Cosmos
{
    class PartitionKeyDeserializer : public JsonObjectDeserializer, public Encapsulate<Cosmos::PartitionKey>
    {
    public:
        void DeserializeObject(const web::json::object& p_Object) override;
    };
}
