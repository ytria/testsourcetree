#include "BasePermissions.h"

RTTR_REGISTRATION
{
    using namespace rttr;
registration::class_<Sp::BasePermissions>("BasePermissions") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Base Permissions"))))
.constructor()(policy::ctor::as_object);
}
