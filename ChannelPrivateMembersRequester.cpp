#include "ChannelPrivateMembersRequester.h"

#include "BusinessAADUserConversationMember.h"
#include "BusinessAADUserConversationMemberDeserializer.h"
#include "MsGraphHttpRequestLogger.h"
#include "Sapio365Session.h"

ChannelPrivateMembersRequester::ChannelPrivateMembersRequester(const boost::YOpt<PooledString>& p_TeamId, const wstring& p_ChannelId, const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_TeamId(p_TeamId ? *p_TeamId : _YTEXT(""))
	, m_ChannelId(p_ChannelId)
	, m_Logger(p_Logger)
{
}

TaskWrapper<void> ChannelPrivateMembersRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<BusinessAADUserConversationMember, BusinessAADUserConversationMemberDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObjects(m_Deserializer, { _YTEXT("teams"), m_TeamId, _YTEXT("channels"), m_ChannelId, _YTEXT("members") }, {}, true, m_Logger, httpLogger, p_TaskData);
}

const vector<BusinessAADUserConversationMember>& ChannelPrivateMembersRequester::GetData() const
{
	ASSERT(m_Deserializer);
	return m_Deserializer->GetData();
}
