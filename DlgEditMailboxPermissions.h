#pragma once

#include "DlgFormsHTML.h"
#include "GridBackendField.h"

class GridMailboxPermissions;

class DlgEditMailboxPermissions : public DlgFormsHTML
{
public:
	DlgEditMailboxPermissions(GridMailboxPermissions& p_Grid, CWnd& p_Parent);

	const map<wstring, bool>& GetChanges() const;
	const vector<vector<GridBackendField>>& GetRowPks() const;

protected:
	void generateJSONScriptData() override;
	bool processPostedData(const IPostedDataTarget::PostedData& data) override;

private:
	vector<GridBackendRow*> getSelectedMailboxPermissionsRows();
	void getAccessRightsValues(boost::YOpt<bool>& p_ChangeOwner, boost::YOpt<bool>& p_ChangePermission, boost::YOpt<bool>& p_DeleteItem, boost::YOpt<bool>& p_ExternalAccount, boost::YOpt<bool>& p_FullAccess, boost::YOpt<bool>& p_ReadPermission);
	void addToPks(GridBackendRow* p_Row);

	GridMailboxPermissions& m_Grid;

	vector<vector<GridBackendField>> m_Pks;
	map<wstring, bool> m_Changes;
};

