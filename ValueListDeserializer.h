#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "ListDeserializer.h"
#include "IDeserializerFactory.h"

class IValueListDeserializer : public JsonObjectDeserializer
{
public:
	IValueListDeserializer(PooledString p_Key)
		: m_Key(p_Key)
		, m_CutOffCondition([](const web::json::value&) { return false; })
		, m_FilterOutCondition([](const web::json::value&) { return false; })
		, m_IsCutOff(false)
	{}
	virtual ~IValueListDeserializer() = default;

	const std::function<bool(const web::json::value&)>& GetCutOffCondition() const
	{
		return m_CutOffCondition;
	}

	const std::function<bool(const web::json::value&)>& GetFilterOutCondition() const
	{
		return m_FilterOutCondition;
	}

	void SetCutOffCondition(const std::function<bool(const web::json::value&)>& p_Condition)
	{
		m_CutOffCondition = p_Condition;
	}

	void SetFilterOutCondition(const std::function<bool(const web::json::value&)>& p_Condition)
	{
		m_FilterOutCondition = p_Condition;
	}

	bool GetIsCutOff() const
	{
		return m_IsCutOff;
	}

	virtual size_t GetRunningDeserializedCount() const = 0;

protected:
	const PooledString m_Key;
	std::function<bool(const web::json::value&)> m_CutOffCondition; // Return value: true = CutOff
	std::function<bool(const web::json::value&)> m_FilterOutCondition; // Return value: true = Filter out
	bool m_IsCutOff;
};

template <typename ObjType, typename DeserializerType>
class ValueListDeserializer : public IValueListDeserializer, public Encapsulate<vector<ObjType>>
{
    static unique_ptr<DefaultFactory<DeserializerType>> MakeDefaultFactory()
    {
        return std::make_unique<DefaultFactory<DeserializerType>>();
    }

public:
    ValueListDeserializer()
        :ValueListDeserializer(MakeDefaultFactory())
    {}
    ValueListDeserializer(PooledString p_Key)
        : ValueListDeserializer(p_Key, MakeDefaultFactory())
    {}
    ValueListDeserializer(unique_ptr<IDeserializerFactory<DeserializerType>> p_DeserializerFactory)
        : ValueListDeserializer(_YTEXT("value"), std::move(p_DeserializerFactory))
    {}
    ValueListDeserializer(PooledString p_Key, unique_ptr<IDeserializerFactory<DeserializerType>> p_DeserializerFactory)
        : IValueListDeserializer(p_Key),
		m_DeserializerFactory(std::move(p_DeserializerFactory))
    {}

	virtual size_t GetRunningDeserializedCount() const
	{
		return m_DeserializedCount;
	}

protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override
    {
        auto itt = p_Object.find(m_Key);
        ASSERT(itt != p_Object.end());
        if (itt != p_Object.end())
        {
            ListMultiPassDeserializer<ObjType, DeserializerType> deserializer(*m_DeserializerFactory, GetData());

			deserializer.SetCutOffCondition(m_CutOffCondition);
			deserializer.SetFilterOutCondition(m_FilterOutCondition);

            deserializer.SetDate(GetDate());
            deserializer.Deserialize(itt->second);
			m_IsCutOff = deserializer.IsCutOff();
			m_DeserializedCount = deserializer.GetObjectCount();
        }
    }

private:
    unique_ptr<IDeserializerFactory<DeserializerType>> m_DeserializerFactory;
	size_t m_DeserializedCount = 0;
};
