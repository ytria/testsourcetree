#include "DateFieldCutOffCondition.h"

DateFieldCutOffCondition::DateFieldCutOffCondition(const wstring& p_FieldName, const wstring& p_DateCutOffIso8601)
	:m_FieldName(p_FieldName),
	m_DateCutOffIso8601(p_DateCutOffIso8601)
{
}

bool DateFieldCutOffCondition::operator()(const web::json::value& p_Json) const
{
	bool isBeforeCutOff = false;

	ASSERT(p_Json.has_string_field(m_FieldName));
	if (p_Json.has_string_field(m_FieldName))
		isBeforeCutOff = p_Json.as_object().at(m_FieldName).as_string() < m_DateCutOffIso8601;

	return isBeforeCutOff;
}
