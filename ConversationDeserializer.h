#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessConversation.h"

class ConversationDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessConversation>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

