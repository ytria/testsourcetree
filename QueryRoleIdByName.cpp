#include "QueryRoleIdByName.h"

#include "InlineSqlQueryRowHandler.h"
#include "O365SQLiteEngine.h"
#include "SessionTypes.h"
#include "SqlQuerySelect.h"

QueryRoleIdByName::QueryRoleIdByName(O365SQLiteEngine& p_Engine, const wstring& p_EmailOrAppId, const wstring& p_RoleName)
	: m_Engine(p_Engine)
	, m_EmailOrAppId(p_EmailOrAppId)
	, m_RoleName(p_RoleName)
	, m_RoleId(0)
{
}

void QueryRoleIdByName::Run()
{
	auto roleHandler = [this](sqlite3_stmt* p_Stmt) {
		m_RoleId = sqlite3_column_int64(p_Stmt, 0);
	};

	wstring queryRoleIdStr = _YTEXT(R"(SELECT r.Id FROM Roles r INNER JOIN Sessions s ON s.RoleId=r.Id WHERE s.SessionType=? AND s.EmailOrAppId=? AND r.name=?)");
	SqlQuerySelect queryRoleId(m_Engine, InlineSqlQueryRowHandler<decltype(roleHandler)>(roleHandler), queryRoleIdStr);
	queryRoleId.BindString(1, SessionTypes::g_Role);
	queryRoleId.BindString(2, m_EmailOrAppId);
	queryRoleId.BindString(3, m_RoleName);
	queryRoleId.Run();
}

int64_t QueryRoleIdByName::GetRoleId() const
{
	return m_RoleId;
}
