#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ModifiedProperty.h"

class ModifiedPropertyDeserializer : public JsonObjectDeserializer, public Encapsulate<ModifiedProperty>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

