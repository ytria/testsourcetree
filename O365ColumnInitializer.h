#pragma once

#include "BasicColumnInitializer.h"

class O365ColumnInitializer : public BasicColumnInitializer
{
public:
    virtual void InitializeColumn(GridBackendColumn& column, const rttr::property& property) const override;
};

