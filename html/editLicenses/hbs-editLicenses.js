(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Button'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<div class=\"control\">\r\n		<input class=\"button "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"post",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" type=\""
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.type : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "\" \r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"mode",{"name":"get_main","hash":{},"data":data}),"edit",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"mode",{"name":"get_main","hash":{},"data":data}),"read",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"cancel",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		>\r\n	</div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return " post ";
},"4":function(container,depth0,helpers,partials,data) {
    var helper;

  return container.escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"type","hash":{},"data":data}) : helper)));
},"6":function(container,depth0,helpers,partials,data) {
    return "submit";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.attribute : depth0),"post",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    return " disabled ";
},"11":function(container,depth0,helpers,partials,data) {
    return " formnovalidate ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"field has-addons row-btns\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n</div>";
},"useData":true});
templates['Main'] = template({"1":function(container,depth0,helpers,partials,data) {
    return " doAutomation ";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "			<form id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" method=\""
    + alias4(((helper = (helper = helpers.method || (depth0 != null ? depth0.method : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"method","hash":{},"data":data}) : helper)))
    + "\" action=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" data-mode=\""
    + alias4(((helper = (helper = helpers.mode || (depth0 != null ? depth0.mode : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"mode","hash":{},"data":data}) : helper)))
    + "\" data-noChange=\""
    + alias4(((helper = (helper = helpers["no-change"] || (depth0 != null ? depth0["no-change"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"no-change","hash":{},"data":data}) : helper)))
    + "\">\r\n			<h4> "
    + ((stack1 = ((helper = (helper = helpers.formTitle || (depth0 != null ? depth0.formTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"formTitle","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</h4>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers.unless.call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"unless","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    return "							"
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    return "						"
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"11":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "\r\n		var arr= [], \r\n		elt = [];\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.onchange : depth0),{"name":"each","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		if (arr.length != 0 ) addConditions (arr);	\r\n\r\n";
},"12":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "			elt = ['"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "','"
    + alias2(alias1((depth0 != null ? depth0.state : depth0), depth0))
    + "'];\r\n			arr.push( elt );\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"section "
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.automationSubmit : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" ondragstart=\"return false;\">\r\n	<div class=\"containerY\">\r\n"
    + ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.main : depth0),{"name":"with","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		<div id=\"licenseList\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</div>\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</form>\r\n	</div>\r\n\r\n</div>\r\n<script language=\"javascript\">\r\n"
    + ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.main : depth0),{"name":"with","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n</script>\r\n";
},"useData":true});
templates['licensesList'] = template({"1":function(container,depth0,helpers,partials,data) {
    return " noOn ";
},"3":function(container,depth0,helpers,partials,data) {
    return " fullRed ";
},"5":function(container,depth0,helpers,partials,data) {
    return " fullOrange ";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                            <span class=\"subLabel\">"
    + ((stack1 = ((helper = (helper = helpers.subLabelParentSection || (depth0 != null ? depth0.subLabelParentSection : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"subLabelParentSection","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\r\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "					<div class=\"switch-toggle mix "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.full : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.offMixMode : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" data-state=\"mix\">\r\n						<input class=\"off\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"off\">\r\n						<label class=\"off\"><i class=\"far fa-times\"></i></label>\r\n\r\n						<input class=\"mix\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"mix\" checked=\"checked\" >\r\n						<label class=\"mix\"></label>\r\n\r\n						<input class=\"on\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"on\">\r\n						<label class=\"on\"><i class=\"far fa-check\"></i></label>\r\n					</div>\r\n";
},"10":function(container,depth0,helpers,partials,data) {
    return " full ";
},"12":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "					<div class=\"switch-toggle twoSt on\" data-state=\"on twoSt\">\r\n						<input class=\"off\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"off\">\r\n						<label class=\"off\"><i class=\"far fa-times\"></i></label>\r\n\r\n						<input class=\"mix\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"mix\">\r\n						<label class=\"mix\"></label>\r\n\r\n						<input class=\"on\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"on\"  checked=\"checked\">\r\n						<label class=\"on\"><i class=\"far fa-check\"></i></label>\r\n					</div>\r\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "					<div class=\"switch-toggle twoSt off  "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.full : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.offMixMode : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" data-state=\"off twoSt\">\r\n						<input class=\"off\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"off\" checked=\"checked\">\r\n						<label class=\"off\"><i class=\"far fa-times\"></i></label>\r\n\r\n						<input class=\"mix\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"mix\">\r\n						<label class=\"mix\"></label>\r\n\r\n						<input class=\"on\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"on\">\r\n						<label class=\"on\"><i class=\"far fa-check\"></i></label>\r\n					</div>\r\n";
},"16":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "				<div class=\"sectionSubPanels\">\r\n                    <div class=\"mainLabel\">\r\n                        <div>"
    + ((stack1 = ((helper = (helper = helpers.labelField || (depth0 != null ? depth0.labelField : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"labelField","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.subLabelField : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </div>\r\n	\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depths[1] != null ? depths[1].state : depths[1]),"mix",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(19, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depths[1] != null ? depths[1].state : depths[1]),"on",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(21, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depths[1] != null ? depths[1].state : depths[1]),"off",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(36, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n				</div>\r\n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                            <div class=\"subLabel\">"
    + ((stack1 = ((helper = (helper = helpers.subLabelField || (depth0 != null ? depth0.subLabelField : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"subLabelField","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n";
},"19":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "						<div class=\"switch-toggle twoSt mix\" data-state=\"naMix\">\r\n							<input class=\"off\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"off\" disabled=\"disabled\">\r\n							<label class=\"off\"><i class=\"far fa-times\"></i></label>\r\n\r\n							<input class=\"mix\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"mix\" disabled=\"disabled\">\r\n							<label class=\"mix\"></label>\r\n\r\n							<input class=\"on\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"on\" disabled=\"disabled\">\r\n							<label class=\"on\"><i class=\"far fa-check\"></i></label>\r\n						</div>\r\n";
},"21":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "						<div class=\"switch-toggle "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"on",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(22, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"off",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(24, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"mix",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(26, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" \r\n							data-state=\""
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"on",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(28, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"off",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(30, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"mix",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(32, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">\r\n\r\n							<input class=\"off\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"off\" "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"off",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(34, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\r\n							<label class=\"off\"><i class=\"far fa-times\"></i></label>\r\n\r\n							<input class=\"mix\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"mix\" "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"mix",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(34, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\r\n							<label class=\"mix\"></label>\r\n\r\n							<input class=\"on\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"on\" "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"on",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(34, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " >\r\n							<label class=\"on\"><i class=\"far fa-check\"></i></label>\r\n						</div>\r\n";
},"22":function(container,depth0,helpers,partials,data) {
    return " on twoSt";
},"24":function(container,depth0,helpers,partials,data) {
    return " off twoSt";
},"26":function(container,depth0,helpers,partials,data) {
    return " mix ";
},"28":function(container,depth0,helpers,partials,data) {
    return "on twoSt";
},"30":function(container,depth0,helpers,partials,data) {
    return "off twoSt";
},"32":function(container,depth0,helpers,partials,data) {
    return "mix";
},"34":function(container,depth0,helpers,partials,data) {
    return " checked=\"checked\" ";
},"36":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "						<div class=\"switch-toggle twoSt na\" data-state=\"na\">\r\n							<input class=\"off\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"off\" disabled=\"disabled\">\r\n							<label class=\"off\"><i class=\"far fa-times\"></i></label>\r\n\r\n							<input class=\"mix\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"mix\" disabled=\"disabled\">\r\n							<label class=\"mix\"></label>\r\n\r\n							<input class=\"on\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" type=\"checkbox\" value=\"on\" disabled=\"disabled\">\r\n							<label class=\"on\"><i class=\"far fa-check\"></i></label>\r\n						</div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "\r\n<div class=\"switchSection\">\r\n		<div class=\"parenSectionTitle "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.full : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.offMixMode : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">\r\n			<span>	<i class=\""
    + container.escapeExpression(((helper = (helper = helpers.fontIcon || (depth0 != null ? depth0.fontIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"fontIcon","hash":{},"data":data}) : helper)))
    + "\" aria-hidden=\"true\"> </i> \r\n					<div class=\"title "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.full : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.offMixMode : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">	\r\n						<span  class=\"currentUsed \"></span>\r\n						<div class=\"mainLabel\">\r\n							"
    + ((stack1 = ((helper = (helper = helpers.labelParentSection || (depth0 != null ? depth0.labelParentSection : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelParentSection","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " \r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.subLabelParentSection : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "							<span class=\"usedTotal\">"
    + ((stack1 = ((helper = (helper = helpers.currentUsed || (depth0 != null ? depth0.currentUsed : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"currentUsed","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\r\n						</div> \r\n					</div>	\r\n			</span>\r\n\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"mix",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(9, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"on",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.state : depth0),"off",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(14, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		</div>\r\n		<div class=\"subPanel\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(16, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	\r\n	\r\n\r\n		</div>\r\n</div>	\r\n";
},"useData":true,"useDepths":true});
})();