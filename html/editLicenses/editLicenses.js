// Javascript for Forms Switch

function addConditions (arr){ 

	for(var x=0; x< arr.length; ++x ) {
		var name = arr[x][0];
		var state = arr[x][1]
		
		var elmt = $('#licenseList').find('input[name="'+ name+'"]').closest('.switch-toggle');

		if (state == 'on') { 
			elmt.removeClass('off');
			elmt.removeClass('mix');
			elmt.addClass('on');
			if (!elmt.hasClass('twoSt')) elmt.addClass('twoSt'); // twoSt disable mix from appearing
			$('#licenseList').find('input[name="'+ name+'"].off').removeAttr('checked');
			$('#licenseList').find('input[name="'+ name+'"].mix').removeAttr('checked');
			$('#licenseList').find('input[name="'+ name+'"].on').attr('checked','checked');

			if (elmt.closest('.switch-toggle').parent().hasClass('parenSectionTitle')) { 
				elmt.closest('.switchSection').find('.subPanel .switch-toggle').each(function(){
					
					$(this).removeClass('na');
					$(this).removeClass('off');
					$(this).removeClass('mix');
					$(this).addClass('on');

					$(this).find('input').removeAttr('disabled')
					$(this).find('input').removeAttr('checked')
					$(this).find('input.on').attr('checked','checked');
				})
			}
		}
			
		if (state == 'off') { 
			elmt.removeClass('on');
			elmt.removeClass('mix');
			elmt.addClass('off');

			$('#licenseList').find('input[name="'+ name+'"].off').attr('checked','checked');
			$('#licenseList').find('input[name="'+ name+'"].mix').removeAttr('checked');
			$('#licenseList').find('input[name="'+ name+'"].on').removeAttr('checked');

			if (elmt.closest('.switch-toggle').parent().hasClass('parenSectionTitle')) { 
				elmt.closest('.switchSection').find('.subPanel .switch-toggle').each(function(){
					
					$(this).removeClass('on');
					$(this).removeClass('off');
					$(this).removeClass('mix');
					$(this).addClass('na');

					$(this).find('input').attr('disabled',"disabled")
				})
			}
		}
		

	} // for(var x=0; x< arr.length; ++x ) {
		$('.button.post').removeAttr('disabled');
	
} //addConditions


$(document).ready(function() {
$('#clickBtn').on('click',function(){
	alert('www')
})
	var directionSwitch = 'On';
	
	/** License Buttons */
	$('.switch-toggle').click(function(){

	if ( !$(this).find('input').attr("disabled")){
		if ( $(this).hasClass('twoSt')) {
			if(!$(this).hasClass('full')) {
				$(this).toggleClass('on');
				$(this).toggleClass('off');
				if ($(this).hasClass('off')) {
					$(this).find('input.off').attr('checked','checked');
					$(this).find('input.on').removeAttr('checked');
				}else {
					$(this).find('input.off').removeAttr('checked');
					$(this).find('input.on').attr('checked','checked');
				}	
			}
		} else {

			if ($(this).hasClass('full')) {
				if(directionSwitch == "On") directionSwitch = 'Off'
				if(directionSwitch == "mixOn") directionSwitch = 'mixOff'
			}
			//alert(directionSwitch)
			switch (directionSwitch) {
				case 'mixOn' :  
					$(this).attr( 'class', 'switch-toggle mix'); 
					$(this).find('input.mix').attr('checked','checked');
					$(this).find('input.on').removeAttr('checked');
					$(this).find('input.off').removeAttr('checked');
					directionSwitch = 'On'; 
					break;
				case 'mixOff' :  
					if ($(this).hasClass('full')) {
						$(this).attr( 'class', 'switch-toggle mix full');  
					}else{
						$(this).attr( 'class', 'switch-toggle mix');  
					}	
					$(this).find('input.mix').attr('checked','checked');
					$(this).find('input.on').removeAttr('checked');
					$(this).find('input.off').removeAttr('checked');
					directionSwitch = 'Off'; 
					break;
				case 'Off' :
					if ($(this).hasClass('full')) {
						$(this).attr( 'class', 'switch-toggle off full');  
					}else {
						$(this).attr( 'class', 'switch-toggle off');  
					}
					$(this).find('input.off').attr('checked','checked');
					$(this).find('input.mix').removeAttr('checked');
					$(this).find('input.on').removeAttr('checked');
					directionSwitch = 'mixOn'; 
					break;
				case 'On' :  
					$(this).attr( 'class', 'switch-toggle on');  
					$(this).find('input.on').attr('checked','checked');
					$(this).find('input.mix').removeAttr('checked');
					$(this).find('input.off').removeAttr('checked');
					directionSwitch = 'mixOff'; 
					break;
			}
		}
/** **************************************** */
var modified = true; 
$(this).closest('.switchSection').find('.switch-toggle').each(function(){
 var state = $(this).attr('data-state'); 
 switch (state) {
	case 'mix' :  			if ( !$(this).hasClass('mix')) 	modified= false; 		break;
	case 'on twoSt' :   	if ( !$(this).hasClass('on')) 	modified= false;		break;
	case 'off twoSt' :		if ( !$(this).hasClass('off')) 	modified= false;		break;
	case 'naMix' :  		if ( !$(this).hasClass('mix')) 	modified= false;		break;
	case 'na' :  			if ( !$(this).hasClass('na')) 	modified= false;		break;
}
});
//alert(modified)
if (modified) {
	if ($(this).closest('.switchSection').find('.parenSectionTitle span').find('.modified').length)    
			$(this).closest('.switchSection').find('.parenSectionTitle span').find('.modified').remove()
} else { 
	if ($(this).closest('.switchSection').find('.parenSectionTitle span').find('.modified').length == 0) 
		$("<i class='fas fa-edit modified'></i>").insertBefore($(this).closest('.switchSection').find('.parenSectionTitle span svg'))
	

}
/*********************************************** */
	}

	// for each parents if there is modified class name then apply active
	/* Activate apply button */
	modified = true;
	$('.parenSectionTitle').each(function(){
		if ($(this).find('.modified').length) { 
			$('.button.post').removeAttr('disabled');
			modified = false;
		}	
	})
	if (modified) $('.button.post').attr('disabled', "disabled");
});

	$('.parenSectionTitle >.switch-toggle').click(function(){  //For the parent switch , then the children also changes
		if($(this).hasClass('off')){
	
			$(this).closest('.switchSection').find(".subPanel .switch-toggle").each(function(){
				$(this).removeClass('off');
				$(this).removeClass('on');
				$(this).addClass('na');
				$(this).removeClass('mix');
				$(this).find('input').attr('disabled','disabled');
			})	
		}
		if($(this).hasClass('on')){
	
			$(this).closest('.parenSectionTitle').find('span:first-child').find('svg').last().addClass('rotateIcon'); 
			$(this).closest('.switchSection').find('.subPanel').slideDown()
	
			$(this).closest('.switchSection').find(".subPanel input").removeAttr('disabled')
			if($(this).attr('data-state')=='on twoSt'){
				$(this).closest('.switchSection').find(".subPanel .switch-toggle").each(function(){
					$(this).attr('class','switch-toggle ' + $(this).attr('data-state'));
					if ($(this).hasClass('on')) {
						$(this).find('input.on').attr('checked','checked');
						$(this).find('input.mix').removeAttr('checked');
						$(this).find('input.off').removeAttr('checked');
						$(this).find('input.na').removeAttr('checked');
					}						
					if ($(this).hasClass('off')) {
						$(this).find('input.on').removeAttr('checked');
						$(this).find('input.mix').removeAttr('checked');
						$(this).find('input.off').attr('checked','checked');
						$(this).find('input.na').removeAttr('checked');
					}							
					if ($(this).hasClass('mix')) {
						$(this).find('input.on').removeAttr('checked');
						$(this).find('input.mix').attr('checked','checked');
						$(this).find('input.off').removeAttr('checked');
						$(this).find('input.na').removeAttr('checked');
					}
				})
			} else {
				$(this).closest('.switchSection').find(".subPanel .switch-toggle").each(function(){
					$(this).addClass('on twoSt');
					$(this).removeClass('off')
					$(this).removeClass('mix')
					$(this).removeClass('na')
					$(this).find('input.on').attr('checked','checked');
					$(this).find('input.mix').removeAttr('checked');
					$(this).find('input.off').removeAttr('checked');
					$(this).find('input.na').removeAttr('checked');
				})
			}
		}		
		if($(this).hasClass('mix')){
			$(this).closest('.switchSection').find(".subPanel .switch-toggle").each(function(){
				$(this).removeClass('on');
				$(this).removeClass('off')
				$(this).removeClass('na')
				$(this).addClass('mix')
				$(this).find('input').attr('disabled','disabled');
		})
	}	

/** Modified switch parent */
var modified = true; 
$(this).closest('.switchSection').find('.switch-toggle').each(function(){
 var state = $(this).attr('data-state'); 
 switch (state) {
	case 'mix' :  			if ( !$(this).hasClass('mix')) 	modified= false; 		break;
	case 'on twoSt' :   	if ( !$(this).hasClass('on')) 	modified= false;		break;
	case 'off twoSt' :		if ( !$(this).hasClass('off')) 	modified= false;		break;
	case 'naMix' :  		if ( !$(this).hasClass('mix')) 	modified= false;		break;
	case 'na' :  			if ( !$(this).hasClass('na')) 	modified= false;		break;
}
});

if (modified) {
	if ($(this).closest('.parenSectionTitle').find('span').find('.modified').length)    
			$(this).closest('.parenSectionTitle').find('span').find('.modified').remove()
} else { 
	if ($(this).closest('.parenSectionTitle').find('span').find('.modified').length == 0) 
			$("<i class='fas fa-edit modified'></i>").insertBefore($(this).closest('.parenSectionTitle').find('span svg'))
}
/* Activate apply button */

modified = true;
$('.parenSectionTitle').each(function(){
	if ($(this).find('.modified').length) { 
		$('.button.post').removeAttr('disabled');
		modified = false;
	}	
})
if (modified) $('.button.post').attr('disabled', "disabled");

});

$('input:reset').click(function(){
	$('.switch-toggle').each(function(){
		$(this).attr('class','switch-toggle ' + $(this).attr('data-state'));
		if ($(this).hasClass('on')) {
			$(this).find('input.on').attr('checked','checked');
			$(this).find('input.mix').removeAttr('checked');
			$(this).find('input.off').removeAttr('checked');
			$(this).find('input.na').removeAttr('checked');
			
			$(this).find('input.on').removeAttr('disabled');
			$(this).find('input.mix').removeAttr('disabled');
			$(this).find('input.off').removeAttr('disabled');
			$(this).find('input.na').removeAttr('disabled');
		}						
		if ($(this).hasClass('off')) {
			$(this).find('input.on').removeAttr('checked');
			$(this).find('input.mix').removeAttr('checked');
			$(this).find('input.off').attr('checked','checked');
			$(this).find('input.na').removeAttr('checked');
		}							
		if ($(this).hasClass('mix')) {
			$(this).find('input.on').removeAttr('checked');
			$(this).find('input.mix').attr('checked','checked');
			$(this).find('input.off').removeAttr('checked');
			$(this).find('input.na').removeAttr('checked');
		}
		if ($(this).hasClass('na')) {
			$(this).find('input').attr('disabled','disabled');
		}
		if ($(this).attr('data-state')=='naMix') {
			$(this).attr('class','switch-toggle ' + 'mix');
			$(this).find('input').attr('disabled','disabled');
		}
	})

	$('.parenSectionTitle').each(function(){ $(this).find('.modified').remove() })
	$('.button.post').attr('disabled', "disabled");
})

	/******************************************************* */

	/*
	$('.switch.parent').on('change' , function(){
		var checkedValue = $(this).find('input').prop("checked");


		$(this).closest('.switchSection').find('.subPanel input').each(function(){
			$(this).prop("checked", checkedValue)
		})

		if (checkedValue) {
			$(this).closest('.parenSectionTitle').find('svg').addClass('rotateIcon'); 
			$(this).closest('.switchSection').find('.subPanel').slideDown()
		}
	});
*/
	$('.parenSectionTitle span:first-child').click(function(){
	 if ($(this).closest('.switchSection').find('.subPanel').css('display')=='none') {
		$(this).find('svg').last().addClass('rotateIcon'); 
		$(this).closest('.switchSection').find('.subPanel').slideDown()
	 } else {
		$(this).find('svg').last().removeClass('rotateIcon');
		$(this).closest('.switchSection').find('.subPanel').slideUp()
	 }
	});
/*
	$('.subPanel .switch').on('change' , function(){
		var checkedValue = $(this).find('input').prop("checked");
		var compare = true;

		$(this).closest('.switchSection').find('.subPanel input').each(function(){
			if (checkedValue != $(this).prop("checked")) 
			{
				compare = false;
			}
		});
		if (compare) {
			$(this).closest('.switchSection').find('.parenSectionTitle input').prop("checked", checkedValue)
		}
	});
*/

	if ($("div.doAutomation").length > 0)
		$("form").submit();
}) // document.ready