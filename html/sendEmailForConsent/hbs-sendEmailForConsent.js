(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Button'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<div class=\"control\">\r\n		<input class=\"button "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"post",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" type=\""
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.type : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "\" \r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"mode",{"name":"get_main","hash":{},"data":data}),"edit",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"mode",{"name":"get_main","hash":{},"data":data}),"read",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"cancel",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		>\r\n	</div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return " post ";
},"4":function(container,depth0,helpers,partials,data) {
    var helper;

  return container.escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"type","hash":{},"data":data}) : helper)));
},"6":function(container,depth0,helpers,partials,data) {
    return "submit";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.attribute : depth0),"post",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    return " disabled ";
},"11":function(container,depth0,helpers,partials,data) {
    return " formnovalidate ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"field has-addons row-btns\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n</div>";
},"useData":true});
templates['Main'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<form id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" method=\""
    + alias4(((helper = (helper = helpers.method || (depth0 != null ? depth0.method : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"method","hash":{},"data":data}) : helper)))
    + "\" action=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" ondragstart=\"return false;\">\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers.unless.call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"unless","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    return "                    "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    return "                "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.main : depth0),{"name":"with","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n    <div id=\"containerFields\">\r\n    \r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n </form>";
},"useData":true});
templates['sendEmailForConsent'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"control\">\r\n                <label> "
    + ((stack1 = ((helper = (helper = helpers.tenantLabel || (depth0 != null ? depth0.tenantLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n                <input id=\"tenantName\" class=\"input "
    + alias4(((helper = (helper = helpers.tenantClass || (depth0 != null ? depth0.tenantClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantClass","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.tenantPopup || (depth0 != null ? depth0.tenantPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantPopup","hash":{},"data":data}) : helper)))
    + "\"  type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.tenantField || (depth0 != null ? depth0.tenantField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.tenantPlaceholder || (depth0 != null ? depth0.tenantPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantPlaceholder","hash":{},"data":data}) : helper)))
    + "\" required value=\""
    + alias4(((helper = (helper = helpers.tenantValue || (depth0 != null ? depth0.tenantValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantValue","hash":{},"data":data}) : helper)))
    + "\" pattern=\"^[a-zA-Z0-9][a-zA-Z0-9-.]*[a-zA-Z0-9]$\" >\r\n            </div>\r\n            <div class=\"control\">\r\n                <label> "
    + ((stack1 = ((helper = (helper = helpers.appIdLabel || (depth0 != null ? depth0.appIdLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIdLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n                <input id=\"appId\" class=\"input "
    + alias4(((helper = (helper = helpers.appIdClass || (depth0 != null ? depth0.appIdClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIdClass","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.appIdPopup || (depth0 != null ? depth0.appIdPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIdPopup","hash":{},"data":data}) : helper)))
    + "\"  type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.appIdField || (depth0 != null ? depth0.appIdField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIdField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.appIdPlaceholder || (depth0 != null ? depth0.appIdPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIdPlaceholder","hash":{},"data":data}) : helper)))
    + "\" required value=\""
    + alias4(((helper = (helper = helpers.appIdValue || (depth0 != null ? depth0.appIdValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIdValue","hash":{},"data":data}) : helper)))
    + "\" pattern=\"^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$\" >\r\n            </div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.RedirectURLField : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "				<div class=\"control has-icons-right\">\r\n					<div>\r\n						<label> "
    + ((stack1 = ((helper = (helper = helpers.RedirectURLLabel || (depth0 != null ? depth0.RedirectURLLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RedirectURLLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n						<input id=\"redirectURL\" class=\"input "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.RedirectURLPopup || (depth0 != null ? depth0.RedirectURLPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RedirectURLPopup","hash":{},"data":data}) : helper)))
    + "\" type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.RedirectURLField || (depth0 != null ? depth0.RedirectURLField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RedirectURLField","hash":{},"data":data}) : helper)))
    + "\"  disabled required value=\""
    + alias4(((helper = (helper = helpers.RedirectURLValue || (depth0 != null ? depth0.RedirectURLValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RedirectURLValue","hash":{},"data":data}) : helper)))
    + "\" >\r\n						<span id=\"lock\"><i class=\"fa fa-lock-alt\"></i></span>\r\n					</div>\r\n				</div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div>\r\n    <div id=\"headerSection\">\r\n        <p class=\"bold gray\"> "
    + ((stack1 = ((helper = (helper = helpers.introTitle || (depth0 != null ? depth0.introTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"introTitle","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </p>\r\n        <p> "
    + ((stack1 = ((helper = (helper = helpers.introText || (depth0 != null ? depth0.introText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"introText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </p>\r\n    </div>\r\n    <div id=\"inputSection\">\r\n        <div class=\"control\">\r\n            <label> "
    + ((stack1 = ((helper = (helper = helpers.emailLabel || (depth0 != null ? depth0.emailLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"emailLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n            <input id=\"emailID\" class=\"input\" title=\""
    + alias4(((helper = (helper = helpers.emailPopup || (depth0 != null ? depth0.emailPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"emailPopup","hash":{},"data":data}) : helper)))
    + "\" type=\"email\" name=\""
    + alias4(((helper = (helper = helpers.emailField || (depth0 != null ? depth0.emailField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"emailField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.emailPlaceholder || (depth0 != null ? depth0.emailPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"emailPlaceholder","hash":{},"data":data}) : helper)))
    + "\" required value=\""
    + alias4(((helper = (helper = helpers.emailValue || (depth0 != null ? depth0.emailValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"emailValue","hash":{},"data":data}) : helper)))
    + "\">\r\n        </div>\r\n        <div class=\"control\">\r\n            <label> "
    + ((stack1 = ((helper = (helper = helpers.forwardLabel || (depth0 != null ? depth0.forwardLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"forwardLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n            <input id=\"forwardID\" class=\"input\" title=\""
    + alias4(((helper = (helper = helpers.forwardPopup || (depth0 != null ? depth0.forwardPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"forwardPopup","hash":{},"data":data}) : helper)))
    + "\"  type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.forwardField || (depth0 != null ? depth0.forwardField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"forwardField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.forwardPlaceholder || (depth0 != null ? depth0.forwardPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"forwardPlaceholder","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.forwardValue || (depth0 != null ? depth0.forwardValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"forwardValue","hash":{},"data":data}) : helper)))
    + "\" pattern=\"[a-zA-Z0-9!#$%&'*+-/=?^_`{|}~.]+@[a-z0-9.-]+\\.[a-z]{2,4}[;, ]?+$\">\r\n        </div>\r\n        <div class=\"control\">\r\n            <label> "
    + ((stack1 = ((helper = (helper = helpers.subjectLabel || (depth0 != null ? depth0.subjectLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"subjectLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n            <input id=\"subjectID\" class=\"input\" title=\""
    + alias4(((helper = (helper = helpers.subjectPopup || (depth0 != null ? depth0.subjectPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"subjectPopup","hash":{},"data":data}) : helper)))
    + "\"  type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.subjectField || (depth0 != null ? depth0.subjectField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"subjectField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.subjectPlaceholder || (depth0 != null ? depth0.subjectPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"subjectPlaceholder","hash":{},"data":data}) : helper)))
    + "\" required value=\""
    + alias4(((helper = (helper = helpers.subjectValue || (depth0 != null ? depth0.subjectValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"subjectValue","hash":{},"data":data}) : helper)))
    + "\">\r\n        </div>\r\n"
    + ((stack1 = helpers.unless.call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.showTenant : depth0),"false",{"name":"eq","hash":{},"data":data}),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        <div class=\"control\">\r\n            <textarea  id=\"contentID\" class=\"textarea\" title=\""
    + alias4(((helper = (helper = helpers.contentPopup || (depth0 != null ? depth0.contentPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"contentPopup","hash":{},"data":data}) : helper)))
    + "\" name=\""
    + alias4(((helper = (helper = helpers.contentField || (depth0 != null ? depth0.contentField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"contentField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.contentPlaceholder || (depth0 != null ? depth0.contentPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"contentPlaceholder","hash":{},"data":data}) : helper)))
    + "\" required >"
    + alias4(((helper = (helper = helpers.contentValue || (depth0 != null ? depth0.contentValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"contentValue","hash":{},"data":data}) : helper)))
    + "</textarea>\r\n        </div>\r\n\r\n    <div id=\"footerSection\">\r\n        <p> "
    + ((stack1 = ((helper = (helper = helpers.footerInf || (depth0 != null ? depth0.footerInf : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"footerInf","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </p>\r\n    </div>\r\n    \r\n    </div>  \r\n</div>";
},"useData":true});
})();