function setAdminConsent() {
    if ( $('#appID').val()=="" ) {
        $('#UltraAdminConsentLink a').hide(); 
        $('#UltraAdminConsentLink .button-Disabled').show(); 
    } else {
        $('#UltraAdminConsentLink a').show();
        $('#UltraAdminConsentLink .button-Disabled').hide(); 
    }
    if ( $('#adminName').val()=="" ) {
        $('#adminConsentLink a').hide(); 
        $('#adminConsentLink .button-Disabled').show(); 
    } else {
        $('#adminConsentLink a').show();
        $('#adminConsentLink .button-Disabled').hide(); 
    }
}
function setInfo(user, pwd, id, key, tenant) {
    // This is only shown on an Empty Form
    // so we disable the names to avoid post edit from there

    $('#adminName').val(user);
    $('#adminName').prop('disabled', true);
    $('#adminPwd').val(pwd);
    
    $('#appID').val(id);
    $('#appID').prop('disabled', true);
    $('#secretKey').val(key);
    
    $('#tenantName').val(tenant);
    $('#tenantName').prop('disabled', true);
    
    setAdminConsent();
}

$(document).ready(function(){
   
    // Show the create new button all the time.
    // Should be either one or the other
    $('#createNewPairTop a').show();
    $('#createNewPair a').show();

    $('#createNewPairTop a, #createNewPair a').click( function() {
        try {
            window.external.createNewPair($('#tenantName').val());
        } catch (err) {
            alert("Create New Pair - Error on Submit: \n\n"+err.message);
            setInfo("theAdmin","thePWD","theID","theKEY","theTenant"); // DEBUG
        }
        setAdminConsent();
    });


    // all sections *******************************
    $('.tabSectionTitle').click(function(){
        $(this).closest('.tabSection').find('.tabSectionBody').toggleClass('display-no');
        $(this).find('svg').last().toggleClass('rotate');
    })
    if (!$('#collaps-expand-all').text()) {
        $('.section').css({'top':'0','height':'calc(100% - 33px)', 'margin': '10px 5px'});
    }
    $('#collapseAll').click(function(){
        $('.tabSectionBody').addClass('display-no');
        $('.tabSectionTitle').find('svg').removeClass('rotate');
    })
    $('#expandAll').click(function(){
        $('.tabSectionBody').removeClass('display-no');
        $('.tabSectionTitle svg').addClass('rotate');
    })

    // IE does not put a red square on a required radio if not set
    // so we intercept the submit here and do the work if needed
    $(document).on('click', 'input[type=submit]', function(event){
        // Cancel, no check to do.
        if (jQuery.type($(this).attr('formnovalidate'))==='string') {return true;}

        // Check all the Radios
        var allRadios = new Array();
        $("input[type='radio']").each(function() {
            if ($(this).prop('required') == true){
                if ($(this).is(':checked')) {
                    allRadios[$(this).attr("name")] = true;
                } else {
                    if ( 'undefined' == typeof allRadios[$(this).attr("name")] ) {
                        allRadios[$(this).attr("name")] = false;
                    }
                }
            }
        })
        for (var key in allRadios) {
            if (!allRadios[key]) {
                $("#"+key+" .radiobox").addClass('isRequired');
                event.preventDefault();
            } else {
                $("#"+key+" .radiobox").removeClass('isRequired');
            }
        }

        // Check all CheckBoxes
        var allCheckboxes = new Array();
        $("input[type='checkbox']").each(function() {
            if ($(this).attr('one-required') == 'yes'){
                if ($(this).is(':checked')) {
                    allCheckboxes[$(this).attr("name")] = true;
                } else {
                    if ( 'undefined' == typeof allCheckboxes[$(this).attr("name")] ) {
                        allCheckboxes[$(this).attr("name")] = false;
                    }
                }
            }
        })
        for (var key in allCheckboxes) {
            if (!allCheckboxes[key]) {
                $("#"+key+" .radiobox").addClass('isRequired');
                event.preventDefault();
            } else {
                $("#"+key+" .radiobox").removeClass('isRequired');
            }
        }

        // We trim all input text
        var mustStop = false;
        $("input[type='text'], input[type='password']").each(function(){
            $(this).val($.trim($(this).val()));
            if ($(this).prop('required') == true){
                if (!$(this).val()) {
                    // one of the required text is empty -  it will be handle by the Submit
                    mustStop = true;
                    // This is to handle the field that may be disabled but is still required...
                    if($(this).prop('disabled') == true){
                        // we need to handle the highlight ourselves, bcose the submit won't
                        $(this).addClass('isRequired');
                        event.preventDefault();
                    }
                } else {
                    if($(this).prop('disabled') == true){
                        $(this).removeClass('isRequired');
                    }
                }
            }
        });
        if (mustStop) { return; }

        if ($('#ultraAdminInfo').length) {
            // Credentials Edit --- test their validity
            var goSave = true;
            try {
                if("OK" != window.external.testAdminSession(
                    $('#tenantName').val(),
                    $('#adminName').val(),
                    $('#adminPwd').val()
                )) {
                    goSave = false;
                    event.preventDefault();
                }
                if("OK" != window.external.testUltraAdmin(
                    $('#tenantName').val(),
                    $('#appID').val(),
                    $('#secretKey').val()
                )) {
                    goSave = false;
                    event.preventDefault();
                }
            } catch (err) {
                alert("Credentials - Error on Submit: \n\n"+err.message);
                // DEBUG -- let go // event.preventDefault();
            }
            if (goSave) {
                // If the adminName, the appID or the tenantName is disabled
                // we must enable and set the value on its hidden counterpart
                // so the value get Posted...
                if ($('#adminName').prop('disabled')) {
                    $('#adminNameVal').val($('#adminName').val());
                    $('#adminNameVal').prop('disabled', false);
                }
                if ($('#appID').prop('disabled')) {
                    $('#appIDVal').val($('#appID').val());
                    $('#appIDVal').prop('disabled', false);
                }
                if ($('#tenantName').prop('disabled')) {
                    $('#tenantNameVal').val($('#tenantName').val());
                    $('#tenantNameVal').prop('disabled', false);
                }
            }
        }
    })

    // rbacAdmin && rbacUltra *******************************
    if($('#appID').length) {
        setAdminConsent();
    }
    $('#appID').on("input",function(){
        setAdminConsent();
    })
    $('#UltraAdminConsentLink a').on('click', function(e) {
        var theUrl = $('#UltraAdminConsentLink a').attr('base_href')+$('#appID').val()+'/'+$('#tenantName').val();
        e.originalEvent.currentTarget.href = theUrl;
    });
    if($('#adminName').length) {
        setAdminConsent();
    }
    $('#adminName').on("input",function(){
        setAdminConsent();
    })
    $('#adminConsentLink a').on('click', function(e) {
        var theUrl = $('#adminConsentLink a').attr('base_href');
        e.originalEvent.currentTarget.href = theUrl;
    });
    $('#iconAdmin')
    .mousedown(function(){
        $('#adminPwd').attr('type', 'text');
        $('#iconAdmin').html('<i class="fa fa-lock"></i>');
    })
    .mouseup(function(){
        $('#adminPwd').attr('type', 'password');
        $('#iconAdmin').html('<i class="fa fa-eye"></i>');
    })
    $('#iconUltra')
    .mousedown(function(){
        $('#secretKey').attr('type', 'text');
        $('#iconUltra').html('<i class="fa fa-lock"></i>');
    })
    .mouseup(function(){
        $('#secretKey').attr('type', 'password');
        $('#iconUltra').html('<i class="fa fa-eye"></i>');
    })
    $('#lockTenant').click(function(){
        if ($(this).find('svg').hasClass("fa-lock-alt")) {
            $('#tenantName').attr('disabled',false);
            $('#lockTenant').html('<i class="fa fa-unlock-alt"></i>');
        }
        else {
            $('#tenantName').attr('disabled',true);
            $('#lockTenant').html('<i class="fa fa-lock-alt"></i>');
            $('#tenantName').val($('#tenantName').attr('value'));
        }
    })
    $('#lockAdmin').click(function(){
        if ($(this).find('svg').hasClass("fa-lock-alt")) {
            $('#adminName').removeClass('isRequired');
            $('#adminName').attr('disabled',false);
            $('#lockAdmin').html('<i class="fa fa-unlock-alt"></i>');
        }
        else {
            $('#adminName').removeClass('isRequired');
            $('#adminName').attr('disabled',true);
            $('#lockAdmin').html('<i class="fa fa-lock-alt"></i>');
            $('#adminName').val($('#adminName').attr('value'));
        }
    })
    $('#lockUltra').click(function(){
        if ($(this).find('svg').hasClass("fa-lock-alt")) {
            $('#appID').removeClass('isRequired');
            $('#appID').attr('disabled',false);
            $('#lockUltra').html('<i class="fa fa-unlock-alt"></i>');
        }
        else {
            $('#appID').removeClass('isRequired');
            $('#appID').attr('disabled',true);
            $('#lockUltra').html('<i class="fa fa-lock-alt"></i>');
            $('#appID').val($('#appID').attr('value'));
        }
    })

    // rbacLicensePools - set No restrictions (used in 2 places)
    function set_LicensePools_NoRestriction() {
        $('.licensePool input[type=checkbox]').each(function(){
            $(this).prop("checked", false);
            tgt = $(this).attr('tgt-field');
            $('input[name='+tgt+']').removeAttr('required');
            $('input[name='+tgt+']').addClass('display-no');
            $('input[name='+tgt+']').attr({'disabled':true});
            $('input[name=NOEDIT_'+tgt+']').removeClass('display-no');
            $(this).setEltStatus('no'); 
        })
    }

    // Nice radio button - Just to avoid repeating some code again and again
    $.fn.setEltStatus = function(status) {
        if (status==='yes') {
            $(this).parent().find('span.radioYes').removeClass('display-no');
            $(this).parent().find('span.radioNo').addClass('display-no');
            $(this).parent().find('span.radioMix').addClass('display-no');
        } else if (status==='no') {
            $(this).parent().find('span.radioYes').addClass('display-no');
            $(this).parent().find('span.radioNo').removeClass('display-no');
            $(this).parent().find('span.radioMix').addClass('display-no');
        } else {
            $(this).parent().find('span.radioYes').addClass('display-no');
            $(this).parent().find('span.radioNo').addClass('display-no');
            $(this).parent().find('span.radioMix').removeClass('display-no');
        }
    };

    // rbacPrivileges - count of selected checkboxes - set category checkbox
    function updatePrivCount() {
        // Check that we are in a case with a counter...
        if($('span.privcount').length === 0) { return true; }
        
        var catState = new Object();
        var tgtState = new Object();
        var finalCount = 0;
        var allCount = 0;
        // get the setAll button config
        $('.setAll').each(function(){
            var tgt = $(this).attr('tgt').split(',');
            $.each(tgt, function(idx, aTgt){
                if (aTgt!=='') {
                    tgtState[aTgt] = true;
                }
            });
        })
        // Parse all the checkboxes
        $("input[type='checkbox'].subitem").each(function() {
            var curItem = $(this);
            var curCat = curItem.attr('subitem');
            var isSet = curItem.is(':checked');
            // This is for the License Pool : we must have the proper privilege set
            if ($('.missingPrivilege').attr('idAccess') === curItem.val()) {
                if (isSet) {
                    $('#licensePools .missingPrivilege').addClass('display-no');
                    $('#licensePools .setAllButtons button:not(.debugLicenseList)').prop('disabled', false);
                    $('#licensePools .licensePool input[type=checkbox]').prop('disabled', false);
                } else {
                    $('#licensePools .missingPrivilege').removeClass('display-no');
                    $('#licensePools .setAllButtons button:not(.debugLicenseList)').prop('disabled', true);
                    $('#licensePools .licensePool input').prop('disabled', true);
                    // We need to remove any restriction set here
                    //set_LicensePools_NoRestriction();
                }
            }
            if (isSet) {
                finalCount++;
                allCount++;
            } else {
                allCount++;
                // Not checked, see if it could be set by a setAll button
                // so we build a table of status of the different requirements (isread, etc)
                $.each(tgtState, function(idx){
                    if (curItem.attr(idx) === 'yes') {
                        tgtState[idx] = false;
                    }
                });
            }
            // This will be used to know if the state of the corresponding category
            if (!catState.hasOwnProperty(curCat)) {
                catState[curCat] = isSet;
            } else {
                if (catState[curCat] !== isSet) {
                    catState[curCat] = null;
                }
            }
        })
        // First set the counter section
        $('span.privcount').each(function(){
            var str = $(this).attr('original');
            $(this).html(str.replace('%',finalCount+"/"+allCount));
        })
        // Then set the Category Checkboxes
        $.each(catState, function(onecat, toset){
            theMain = $('input[category='+onecat+']');
            if (toset === null) {
                theMain.prop('indeterminate', true);
                theMain.setEltStatus('mix');
            } else {
                theMain.prop("checked", toset);
                if (toset) {
                    theMain.setEltStatus('yes');
                } else {
                    theMain.setEltStatus('no');
                }
            }
        })
        // Finaly, set the setAll buttons status
        $('.setAll').each(function(){
            if ($(this).attr('tgt') === '') {
                // Set to nothing: if count is 0 is equivalent to button
                $(this).prop('disabled', finalCount === 0);
            } else {
                // other buttons
                var setTgtState = new Object();
                var isAllSet = true;
                var tgt = $(this).attr('tgt').split(',');
                // we look at the current button requirement
                // and see if they are set from the table of status
                $.each(tgt, function(idx, aTgt){
                    isAllSet = isAllSet & tgtState[aTgt];
                    setTgtState[aTgt] = isAllSet;
                });
                // now we look to the other status
                // if one there is a true, then it is not a proper case
                $.each(tgtState, function(idx, valx){
                    if (!setTgtState.hasOwnProperty(idx)) {
                        isAllSet = isAllSet & (!valx);
                    }
                });
                // Now we now if we want to disable the button or not....
                $(this).prop('disabled', isAllSet);
            }
        })
    }

    // Nice radio button
    $(document).on('click', 'input.label-override', function(){
        if ($(this).prop('readonly') === true) {
            return false;
        }
        if ($(this).attr('type') === 'radio') {
            // Radio: only one can be checked
            $('input[name='+$(this).attr('name')+']').each(function(){
                if ($(this).is(':checked') === true) {
                    $(this).setEltStatus('yes');
                } else {
                    $(this).setEltStatus('no');
                }
            });
        } else {
            // Checkboxes: just handle the current one
            // also handle the indeterminate for rbacPrivileges
            if ($(this).prop('indeterminate') === true) { // Only category can be like this
                $(this).setEltStatus('mix');
            } else if ($(this).is(':checked') === true) {
                $(this).setEltStatus('yes');
                // Now we will handle the "musthave" if any
                if ($(this).hasClass('subitem')) {
                    var allMustHave = $(this).attr('musthave').split(',');
                    $.each(allMustHave, function(idx, val){
                        $('input#access'+val).prop("checked", true);
                        $('input#access'+val).setEltStatus('yes');
                    })
                }
            } else {
                $(this).setEltStatus('no');
                // Now we will handle the "musthave" if any
                if ($(this).hasClass('subitem')) {
                    $('input.subitem[musthave*='+$(this).val()+']').each(function(){
                        $(this).prop("checked", false);
                        $(this).setEltStatus('no');
                    })
                }
            }
        }
        // rbacPrivileges - manage the category of checkboxes
        if ($(this).hasClass('mainitem')) {
            tgt = ( $(this).is(':checked') === true );
            $('input[subitem='+$(this).attr('category')+']').each(function(){
                if (tgt) {
                    $(this).prop("checked", true);
                    $(this).setEltStatus('yes');
                } else {
                    $(this).prop("checked", false);
                    $(this).setEltStatus('no');
                }
            });
        } else if ($(this).hasClass('subitem')) {
            curState = $(this).is(':checked');
            $('input[subitem='+$(this).attr('subitem')+']').each(function(){
                if ($(this).is(':checked') !== curState) {
                    // At least one is different
                    theMain = $('input[category='+$(this).attr('subitem')+']');
                    theMain.prop('indeterminate', true);
                    theMain.setEltStatus('mix');
                    curState = null;
                    return false;
                }
            });
            if (curState !== null) {
                // all the same
                theMain = $('input[category='+$(this).attr('subitem')+']');
                theMain.prop("checked", curState);
                if (curState) {
                    theMain.setEltStatus('yes');
                } else {
                    theMain.setEltStatus('no');
                }
            }
        }
        updatePrivCount();
    })
    // Handle the keyboard on the Nice Radio Buttons
    $(document).on('keydown', '.oneradio, .setonesku', function(event) {
        if ( event.which == 13 || event.which == 32 ) {
            // Enter Key - Space Key
            theInput = $(this).find('input');
            if (theInput.attr('type') === 'radio') {
                // No idea why, but we need 2 clicks to effectively
                // change the checked situation on a radio ?!?
                theInput.click();
            }
            theInput.click();
            event.preventDefault();
        }
    });

    // rbacPrivileges - Buttons to set multiples
    $(document).on('click', '.setAll', function(event){
        var tgt = $(this).attr('tgt').split(',');
        $('.allPrivileges input.subitem').each(function(){
            var isSet = false;
            var curItem = $(this);
            $.each(tgt, function(idx, toset){
                if (curItem.attr(toset) === 'yes') {
                    isSet = true;
                    return false;
                }
            });
            if (isSet) {
                curItem.prop("checked", true);
                curItem.setEltStatus('yes');
            } else {
                curItem.prop("checked", false);
                curItem.setEltStatus('no');
            }
        })
        updatePrivCount();
        event.preventDefault();
    })

    // rbacPrivileges -- DEBUG Button -- to see the effective list
    $(document).on('click', '.debugPrivList', function(event){
        if($(this).closest('.tabSection').find('.showDebugPrivList').text() == '') {
            var $tgt = $('<table border=1>');
            $('.allPrivileges input').each(function(){
                $tgt.append('<tr>');
                if ($(this).hasClass('mainitem')) {
                    $tgt.append('<td colspan=5>'+$(this).val()+'</td>');
                    $tgt.append('<td colspan=5> - must have - </td>');
                } else if ($(this).hasClass('subitem')) {
                    $tgt.append('<td>'+$(this).val()+'</td>');
                    $tgt.append('<td>'+($(this).attr('isread')!=''?'isRead':'')+'</td>');
                    $tgt.append('<td>'+($(this).attr('isedit')!=''?'isEdit':'')+'</td>');
                    $tgt.append('<td>'+($(this).attr('isdelete')!=''?'isDelete':'')+'</td>');
                    $tgt.append('<td>'+($(this).attr('iscreate')!=''?'isCreate':'')+'</td>');
                    var allMustHave = $(this).attr('musthave').split(',');
                    $.each(allMustHave, function(idx, val){
                        $tgt.append('<td>'+val+'</td>');
                    })
                    for(i=allMustHave.length; i < 5; i++ ) {
                        $tgt.append('<td>&nbsp;</td>');
                    }
                }
                $tgt.append('</tr>');
            })
            $tgt.append($('</table>'))
            $(this).closest('.tabSection').find('.showDebugPrivList').html($tgt);
            $(this).find('svg').addClass('isLoaded');
        } else {
            $(this).closest('.tabSection').find('.showDebugPrivList').html('');
            $(this).find('svg').removeClass('isLoaded');
        }
        event.preventDefault();
    })
    
    
    
    // **** Quick Search field on key
    $(document).on('keyup', '.control-top .radiobox .sortradio input' , function() {
        tgtVal = $(this).val().toLowerCase();
        var hasHiddenSelected = false;
        var theList = '';
        $(this).parent().parent().parent().find('.allradio').find('.oneradio').each(function(){ // AAAAAAAAAAAA
            if (!$(this).hasClass('mainchoice')) { // we don't hide the main categories...
                theVal = $(this).attr('label-sort');
                if (~theVal.toLowerCase().indexOf(tgtVal)) {
                    $(this).removeClass('display-no');
                } else {
                    $(this).addClass('display-no');
                    if ($(this).find('input').is(':checked')) {
                        theList = theList +" > "+theVal+"<br>";
                        hasHiddenSelected = true;
                    }
                }
            }
        })
        if (!hasHiddenSelected) {
            $(this).parent().parent().parent().find('label div.curselect').html("");
            $(this).parent().parent().parent().find('label div.curselect').addClass('display-no');
        } else {
            $(this).parent().parent().parent().find('label div.curselect').html(theList);
            $(this).parent().parent().parent().find('label div.curselect').removeClass('display-no');
        }
    })
    
    // Click on Icons for quick search, sort and expand/collapse
    $(document).on('click', '.control-top .radiobox .sortradio svg', function() {
        if ($(this).hasClass('fa-search')) {
            // Search Icon
            $(this).parent().find('input').toggleClass('display-no');
            if ( !$(this).parent().find('input').hasClass('display-no') ) {
                $(this).parent().find('input').focus();
            } else {
                $(this).parent().find('input').val('');
                $(this).parent().parent().find('.allradio').find('.oneradio').each(function(){
                    $(this).removeClass('display-no');
                })
            }
        } else if ($(this).hasClass('isexpand')) {
            // expand the section
            $(this).parent().parent().parent().parent().removeClass('heightlimited');
            $(this).parent().addClass('display-no');
            $(this).parent().next().removeClass('display-no');
        } else if ($(this).hasClass('iscollapse')) {
            // collapse the section
            $(this).parent().parent().parent().parent().addClass('heightlimited');
            $(this).parent().addClass('display-no');
            $(this).parent().prev().removeClass('display-no');
        } else {
            // 3 sort Icons
            var order = 'default';
            if ($(this).hasClass('fa-sort-alpha-up')) { order = 'up'; }
            if ($(this).hasClass('fa-sort-alpha-down')) { order = 'down'; }
            $(this).siblings().removeClass('sort-current');
            $(this).addClass('sort-current');

            var theDiv = $(this).parent().parent().parent().find('.allradio');
            // special for rbacPrivileges - we sort each categories
            if (theDiv.find('.allsubchoices').length) {
                theDiv.find('.allsubchoices').each(function(){
                    var subOrder = $(this).find('.oneradio.subchoice').sort(function(a,b) {
                        if (order == 'up') {
                            return $(a).attr('label-sort') < $(b).attr('label-sort')  ? 1 : -1;
                        } else if (order == 'down') {
                            return $(a).attr('label-sort') > $(b).attr('label-sort')  ? 1 : -1;
                        } else {
                            return Number($(a).attr('idx')) > Number($(b).attr('idx'))  ? 1 : -1;
                        }
                    })
                    $(this).html(subOrder);
                });
            } else {
                var newOrder = theDiv.find('.oneradio').sort(function(a,b) {
                    if (order == 'up') {
                        return $(a).attr('label-sort') < $(b).attr('label-sort')  ? 1 : -1;
                    } else if (order == 'down') {
                        return $(a).attr('label-sort') > $(b).attr('label-sort')  ? 1 : -1;
                    } else {
                        return Number($(a).attr('idx')) > Number($(b).attr('idx'))  ? 1 : -1;
                    }
                })
                theDiv.html(newOrder);
            }
        }
    });
    // We don't want the Enter to trigg in this quick search field
    $(document).on('keydown', '.control-top .radiobox .sortradio input', function(event) {
        if ( event.which == 13 ) {
            // Enter Key
            event.preventDefault();
        } else if ( event.which == 27 ) {
            // Esc Key
            $(this).parent().find('.fa-search').click();
            event.preventDefault();
        }
    });
    
    // rbacFilter *******************************
    // Showing the property list related to the target
    function displayPropertyList() {
        $('input[name='+$('#objectType').attr('srcField')+']').each(function(){
            if ($(this).is(':checked') === true) {
                $('#'+$(this).val()).removeClass('display-no');
                $('#'+$(this).val()).find('.radiobox .allradio input').prop({'required': true});
            } else {
                $('#'+$(this).val()).addClass('display-no');
                $('#'+$(this).val()).find('.radiobox .allradio input').prop({'required': false});
            }
        });
    }
    if ($('#objectType').length) {
        displayPropertyList();
        $('input[name='+$('#objectType').attr('srcField')+']').click(function(){
            displayPropertyList();
        })
    }




    


    // rbacLicensePools - checkbox to enable/disable a SKU edit
    $(document).on('change', '.control-select input', function(){
        tgt = $(this).attr('tgt-field');
        if ($(this).is(':checked') === true) {
            $('input[name='+tgt+']').prop('disabled', false);
            $('input[name='+tgt+']').removeClass('display-no');
            $('input[name='+tgt+']').prop('required',true);
            $('input[name='+tgt+']').focus();
            $('input[name=NOEDIT_'+tgt+']').addClass('display-no');
        } else {
            $('input[name='+tgt+']').prop('required', false);
            $('input[name='+tgt+']').addClass('display-no');
            $('input[name='+tgt+']').prop('disabled',true);
            $('input[name=NOEDIT_'+tgt+']').removeClass('display-no');
        }
    })
    // rbacLicensePools - set all to 0
    $(document).on('click', '#lockAll', function(event){
        $('.licensePool input[type=checkbox]').each(function(){
            $(this).prop("checked", true);
            tgt = $(this).attr('tgt-field');
            $('input[name='+tgt+']').prop('disabled', false);
            $('input[name='+tgt+']').removeClass('display-no');
            $('input[name='+tgt+']').prop('required',true);
            $('input[name='+tgt+']').val('0');
            $('input[name=NOEDIT_'+tgt+']').addClass('display-no');
            $(this).setEltStatus('yes');
        })
        event.preventDefault();
    })
    // rbacLicensePools - unset all (no restriction)
    $(document).on('click', '#unlockAll', function(event){
        // we use this also in updatePrivCount()
        set_LicensePools_NoRestriction();
        event.preventDefault();
    })
    // rbacLicensePools - set missing privilege Button
    $(document).on('click', '#setMissingPrivilege', function(){
        event.preventDefault();
        $('input[name='+$('.allPrivileges').attr('id')+'][value='+$(this).parent().attr('idAccess')+']').click();
    })

    $('input[name='+$('.allPrivileges').attr('id')+'][value='+$("#setMissingPrivilege").parent().attr('idAccess')+']').on('click', function () {
        if ($(this).is(':checked')) {
            $('.skuinput input:nth-child(1)').prop('disabled', false);
        } else {
            $('.skuinput input:nth-child(1)').prop('disabled', true);
        }
    });

    // rbacRoleDef - button with external function call
    $('#newCredential a').click(function(event){
        event.preventDefault();
        var retVal = "";
        try {
            $('#errorNewCred span').addClass('display-no');
            retVal = window.external.createNewCredential();

        } catch (err) {
            alert("New Credential - Error on Click: \n\n"+err.message);
            retVal = "keyNew1§The NEW KEY";
        }
        
        if (retVal) {
            var fullCred = retVal.split('§');
            var tgtField = $('#credential_radio_template .oneradio input').attr('tgtname');
            var isAlreadyThere = false;
            $('#roleDef .control-top .radiobox input[name='+tgtField+']').each(function(){
                if ($(this).attr('value') === fullCred[0]) {
                    isAlreadyThere = true;
                    return false;
                }
            });
            if (isAlreadyThere) {
                $('#errorNewCred span').removeClass('display-no');
                return false;
            }
            var newEltHtml = $('#credential_radio_template').html();
            newEltHtml = newEltHtml.replace(/%%value%%/g, fullCred[0]);
            newEltHtml = newEltHtml.replace(/%%label%%/g, fullCred[1]);
            // Now let's build a new jQuery element from this string, so we can manipulate it
            var newElt = $(newEltHtml);
            newElt.find('input').prop('required', true);
            newElt.find('input').prop('disabled', false);
            newElt.find('input').attr('name', newElt.find('input').attr('tgtname'));
            newElt.find('input').removeAttr('tgtname');
            if ($('.control-top .radiobox .nokeyset').length) {
                $('.control-top .radiobox .nokeyset').remove();
            }
            $('#roleDef .control-top .radiobox').append(newElt);
            
            // automatically select the entry - Note: we need to check it first
            // not sure why, but if we don't, it won't work (sees as a click for false...)
            newElt.find('input').prop('checked', true);
            newElt.find('input').click();
            addLicensePools();
        }
    })

     /** 
      * * External Function * * getLicensePool(credentialName)
      * The function has an access to the license pools for each credential.
      * It will give the stringified json array corresponding to the credentialName.
      * 
      *  Example :
      * getLicensePool(keyref1) should return something like :
      * [ { "select": "HQSKU1SEL", "default": "X", "label": "HQSKUSKU #1", "field": "HQSKU1VAL", "value": "1234567890", "info": "900 available, 700 assigned to role"}
      * , { "select": "HQSKU2SEL", "default": "", "label": "HQSKUSKUSKUSKU #2", "field": "HQSKU2VAL", "value": "100", "info": "900 available, 700 assigned to role"}
      * ]
      * 
      * @param credentialName name of the license pools to be returned.
      * @return the stringified json array containing the license pools for the credential given by the parameters.
      */
    
      
    // Initialization (only if RbacLicensePools exists)
	if ($("div #licensePools").length > 0) {
        addLicensePools();
    }
    // On Click - selection of another credential
    $("#roleDef .control-top .radiobox input").change(function() {
        addLicensePools();
    });
    // We need this here for the debug view.
    var licensePool;
    function addLicensePools() {
        var selectedCrednential = $("#roleDef .control-top .radiobox .oneradio input:checked");
        if (selectedCrednential.length == 0) { return; } // No Credential set
        var credentialName = selectedCrednential.val();
        if (credentialName == "" || credentialName == $('#roleDef .control-top').attr('noValue')) { return; } // No Credential set
        // Clean any previous entry
        $(".licensePool:not(.licenseTemplate)").remove();
        // Get the value from sapio365
        var licensePoolStr = '';
        try {
            licensePoolStr = window.external.getLicensePool(credentialName);
        } catch (err) {
            licensePoolStr = '[ { "select": "HQSKU1SEL", "default": "X", "label": "HQSKUSKU #1", "field": "HQSKU1VAL", "value": "1234567890", "info": "900 available, 700 assigned to role"}            , { "select": "HQSKU2SEL", "default": "", "label": "HQSKUSKUSKUSKU #2", "field": "HQSKU2VAL", "value": "100", "info": "900 available, 700 assigned to role"} ]';
            alert("GET LICENSE POOL - '"+credentialName+"' - Error:\n" + err.message);
        }
        try {
            licensePool = JSON.parse(licensePoolStr);
        } catch (err) {
            alert("LICENSE POOL - INVALID JSON FORMAT - '"+credentialName+"' - Error:\n" + err.message + "\n\nJSON:\n\n"+licensePoolStr);
        }
        // Build the list of element from the JSON
        $.each(licensePool, function(index, value) {
            // clone the template div.
            var clone = $(".licensePool.licenseTemplate").clone();
            
            // change the values of the cloned div.
            clone.removeClass("licenseTemplate");
            clone.children().attr("id", value.field);
            clone.find(".setonesku input").attr({
                "id" : "sel"+value.select,
                "tgt-field" : value.field,
                "name" : value.select,
                "value" : value.select
            }).prop("checked", value.default != "" ? true : false);
            
            var firstInput = clone.find(".skuinput input:nth-child(1)");
            var secondInput = clone.find(".skuinput input:nth-child(2)");

            if (value.default != "") {
                clone.find(".radioYes").removeClass("display-no");
                clone.find(".radioNo").addClass("display-no");
                firstInput.prop("required", true);
                secondInput.addClass("display-no");
                firstInput.prop("disabled", false);
            } else { 
                clone.find(".radioNo").removeClass("display-no");
                clone.find(".radioYes").addClass("display-no");
                firstInput.addClass("display-no");
                firstInput.prop("disabled", false); 
            }
            
            clone.find(".setonesku label").attr("for", "sel" + value.select);
            
            clone.find(".labelSKU").html(value.label);

            firstInput.attr({
                "id" : "val" + value.field,
                "name" : value.field,
                "value" : value.value
            })
            secondInput.attr({
                "id" : "valNoEdit_" + value.field,
                "name" : "NOEDIT_" + value.field
            })
            clone.find(".skuinfo span").html(value.info);
            $("#licensePoolElts").append(clone);
        })
    }
    updatePrivCount();

    // rbacLicensePools -- DEBUG Button -- to see the effective list
    $(document).on('click', '.debugLicenseList', function(event){
        event.preventDefault();
        if($(this).closest('.tabSection').find('.showDebugLicenseList').text() == '') {
            var tgt = '<table border="1">';
            tgt += '<tr>'
                    + '<td style="padding: 5px 5px 5px 5px"><center> DATA </center></td>'
                +  '</tr>';
            $.each(licensePool, function(index, value) {
                console.log(value.select);
                tgt += '<tr>' + 
                           '<td style="padding : 5px 5px 5px 5px;"><pre>' + JSON.stringify(value, null, 2) + '</pre></td>' +
                       '</tr>';
            });
            tgt += '</table>';
            $(this).closest('.tabSection').find('.showDebugLicenseList').html(tgt);
            $(this).find('svg').addClass('isLoaded');
        } else {
            $(this).closest('.tabSection').find('.showDebugLicenseList').html('');
            $(this).find('svg').removeClass('isLoaded');
        }
    })

    /* Responsive filter bar */ 
    $(window).resize(function() {
        var currentBar = $(".sortradio .quicksearch:not(.display-no)").closest(".sortradio");
        var isBtnClicked = !currentBar.find("input").hasClass("display-no");
        var isSmallScreen = window.innerWidth <= 700;

        if (isSmallScreen)
            currentBar.css("width", "");
            
        isSmallScreen && isBtnClicked ? addResponsiveBar(currentBar) : removeResponsiveBar(currentBar);
    }); 

    $(".contentSortRadio").on("click", ".fa-search", function() {
        var currentBar = $(this).closest(".sortradio");
        var isBtnClicked = !currentBar.find("input").hasClass("display-no");
        var isSmallScreen = window.innerWidth <= 700;

        isSmallScreen && !isBtnClicked ? addResponsiveBar(currentBar) : removeResponsiveBar(currentBar);
    });

    function addResponsiveBar(currentBar) {
        currentBar.addClass("smallScreenFilterBar").css({"width" : "100%", "margin-top" : 0, "margin-right" : 0});
        currentBar.parent().find("div.allradio").css("padding-top", "25px");
        currentBar.find(".contentSortRadio").css("float","right");
        currentBar.parent().find("span.icon.is-left.switch.locked").css("z-index","101");
    }
    function removeResponsiveBar(currentBar) {
        currentBar.removeAttr("style").removeClass("smallScreenFilterBar");
        currentBar.parent().find("div.allradio").removeAttr("style");
        currentBar.find(".contentSortRadio").css("float","");
    }
})