(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Button'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<div class=\"control\">\r\n		<input class=\"button "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"post",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" type=\""
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.type : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "\" \r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"cancel",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		>\r\n	</div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return " post ";
},"4":function(container,depth0,helpers,partials,data) {
    var helper;

  return container.escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"type","hash":{},"data":data}) : helper)));
},"6":function(container,depth0,helpers,partials,data) {
    return "submit";
},"8":function(container,depth0,helpers,partials,data) {
    return " formnovalidate ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"field has-addons row-btns\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n</div>";
},"useData":true});
templates['Main'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <form id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" method=\""
    + alias4(((helper = (helper = helpers.method || (depth0 != null ? depth0.method : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"method","hash":{},"data":data}) : helper)))
    + "\" action=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" ondragstart=\"return false;\">\r\n        <div id=\"header\">\r\n            <h2 data-exceedMsg=\""
    + alias4(((helper = (helper = helpers.exceedMessage || (depth0 != null ? depth0.exceedMessage : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"exceedMessage","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.labelIntro || (depth0 != null ? depth0.labelIntro : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelIntro","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</h2>\r\n            <h3>"
    + ((stack1 = ((helper = (helper = helpers.labelSubIntro || (depth0 != null ? depth0.labelSubIntro : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelSubIntro","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</h3>\r\n        </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers.unless.call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"unless","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    return "                    "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"addRemove columns is-mobile\">\r\n                 <div id=\"addTenant\" class=\"disabled column is-4\"><i class=\"fas fa-plus\"></i>"
    + ((stack1 = ((helper = (helper = helpers.adduserMsg || (depth0 != null ? depth0.adduserMsg : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"adduserMsg","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                <div class=\"column is-4 columns is-mobile totMax\">\r\n                    <div class=\"column\">\r\n                        <label>"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.noQuantity : depth0),{"name":"unless","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</label>\r\n                    </div>\r\n                    <div class=\"column is-4\">\r\n                        &nbsp;<input id=\"total\" class=\"input"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.noQuantity : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" type=\"text\" readonly>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"addRemove columns is-mobile marginTop\">\r\n                <div id=\"removeTenant\" class=\"disabled column is-4\"><i class=\"fas fa-minus\"></i>"
    + ((stack1 = ((helper = (helper = helpers.removeusrMsg || (depth0 != null ? depth0.removeusrMsg : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"removeusrMsg","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                <div class=\"column is-4 columns is-mobile totMax\">\r\n                    <div class=\"column\">\r\n                        <label for=\"\">"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.noQuantity : depth0),{"name":"unless","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</label>\r\n                    </div>\r\n                    <div class=\"column is-4\">\r\n                        <input id=\"maxQty\" class=\"input"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.noQuantity : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" type=\"text\" value="
    + alias4(((helper = (helper = helpers.maxQuantity || (depth0 != null ? depth0.maxQuantity : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"maxQuantity","hash":{},"data":data}) : helper)))
    + " readonly> \r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div id=\"warningMsg\">\r\n                <i class=\""
    + alias4(((helper = (helper = helpers.warningIcon || (depth0 != null ? depth0.warningIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"warningIcon","hash":{},"data":data}) : helper)))
    + "\"></i>\r\n                <p>"
    + ((stack1 = ((helper = (helper = helpers.warningMsg || (depth0 != null ? depth0.warningMsg : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"warningMsg","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " <span class=\"subInfo\"><br>"
    + ((stack1 = ((helper = (helper = helpers.warningSubMsg || (depth0 != null ? depth0.warningSubMsg : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"warningSubMsg","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span></p>\r\n                <input class=\"input\" type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.keyValidation || (depth0 != null ? depth0.keyValidation : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyValidation","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + ((stack1 = ((helper = (helper = helpers.keyVal_popup || (depth0 != null ? depth0.keyVal_popup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyVal_popup","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" required pattern=\"[A-Za-z0-9]{5,}\">\r\n            </div>\r\n            <div id=\"msgTxt\">"
    + ((stack1 = ((helper = (helper = helpers.labelFooter || (depth0 != null ? depth0.labelFooter : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelFooter","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return ((stack1 = ((helper = (helper = helpers.totalMsg || (depth0 != null ? depth0.totalMsg : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"totalMsg","hash":{},"data":data}) : helper))) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    return " display-no";
},"11":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return ((stack1 = ((helper = (helper = helpers.maxMsg || (depth0 != null ? depth0.maxMsg : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"maxMsg","hash":{},"data":data}) : helper))) != null ? stack1 : "");
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"14":function(container,depth0,helpers,partials,data) {
    return "                "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "            <div id=\"addTenantField\" class=\"display-no\">\r\n                <div class=\"control columns is-mobile\">\r\n                    <div class=\"column tenant\">\r\n                        <input class=\"input \" type=\"text\" name=\"\" value=\""
    + container.escapeExpression(((helper = (helper = helpers.currentTenant || (depth0 != null ? depth0.currentTenant : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"currentTenant","hash":{},"data":data}) : helper)))
    + "\" required disabled \r\n                            placeholder=\""
    + ((stack1 = (helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"tenantName_PlaceHolder",{"name":"get_main","hash":{},"data":data})) != null ? stack1 : "")
    + "\" title=\""
    + ((stack1 = ((helper = (helper = helpers.tenantName_popup || (depth0 != null ? depth0.tenantName_popup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantName_popup","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\"\r\n                            pattern=\"[a-zA-Z0-9._%+-]+\\.[a-zA-Z0-9.-]+\\.[a-zA-Z0-9._%+-]+\">\r\n                    </div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.noQuantity : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.program(19, data, 0),"data":data})) != null ? stack1 : "")
    + "                </div>                \r\n            </div>\r\n";
},"17":function(container,depth0,helpers,partials,data) {
    return "                        <input class=\"input display-no\" type=\"text\" disabled name=\"\" value=\"0\">\r\n";
},"19":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "                    <div class=\"column is-2 quantity\">\r\n                        <input class=\"input\" type=\"number\" name=\"\" required disabled\r\n                            placeholder=\""
    + ((stack1 = (helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"quantity_PlaceHolder",{"name":"get_main","hash":{},"data":data})) != null ? stack1 : "")
    + "\" min=\"1\" max=\""
    + container.escapeExpression((helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"maxQuantity",{"name":"get_main","hash":{},"data":data}))
    + "\">\r\n                    </div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.main : depth0),{"name":"with","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n    <div id=\"containerFields\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        <div id=\"control\">\r\n"
    + ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.main : depth0),{"name":"with","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\r\n       \r\n    </div>\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.main : depth0),{"name":"with","hash":{},"fn":container.program(16, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  \r\n </form>\r\n";
},"useData":true});
templates['oneTenant'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "        <input class=\"input display-no\" type=\"text\" disabled name=\"\" value=\"0\">\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <div class=\"column is-2 quantity\">\r\n        <input class=\"input\" type=\"number\" name=\"quantity"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" required value=\""
    + alias4(((helper = (helper = helpers.quantity || (depth0 != null ? depth0.quantity : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quantity","hash":{},"data":data}) : helper)))
    + "\" \r\n            placeholder=\""
    + ((stack1 = (helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"quantity_PlaceHolder",{"name":"get_main","hash":{},"data":data})) != null ? stack1 : "")
    + "\" min=\"1\" max=\""
    + alias4((helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"maxQuantity",{"name":"get_main","hash":{},"data":data}))
    + "\">\r\n    </div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"control columns is-mobile\">\r\n    <div class=\"column\">\r\n        <input class=\"input \" type=\"text\" name=\"tenant"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" required value=\""
    + alias4(((helper = (helper = helpers.tenant || (depth0 != null ? depth0.tenant : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenant","hash":{},"data":data}) : helper)))
    + "\" \r\n            placeholder=\""
    + ((stack1 = (helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"tenantName_PlaceHolder",{"name":"get_main","hash":{},"data":data})) != null ? stack1 : "")
    + "\" title=\""
    + ((stack1 = (helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"tenantName_popup",{"name":"get_main","hash":{},"data":data})) != null ? stack1 : "")
    + "\"\r\n            pattern=\"[a-zA-Z0-9._%+-]+\\.[a-zA-Z0-9.-]+\\.[a-zA-Z0-9._%+-]+\">\r\n    </div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.neq || (depth0 && depth0.neq) || alias2).call(alias1,(helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"noQuantity",{"name":"get_main","hash":{},"data":data}),"",{"name":"neq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
})();