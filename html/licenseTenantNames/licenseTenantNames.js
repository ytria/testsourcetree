var isDebug = !isInApp; // always Debug when in IE - isInApp is set in hbshandler.js

$(document).ready(function(){

    // This won't move: it is set once and for all
    var maxTenant = 10;
    if ($('#maxQty').hasClass('display-no')) {
        // Tenant only case: the max is the number of tenants that can be set
        maxTenant = Number($('#maxQty').val());
    }
    if (isDebug) console.log("maxTenant: "+maxTenant);

    function setTotal() {
        var sumQuantity= 0 ;
        $('#containerFields input[type="number"]').each(function(){
            sumQuantity += Number($(this).val())
        });
        $('#total').val(sumQuantity)
        return sumQuantity;
    }

    function showHideButtons() {
        var nbElt = $('#containerFields > .control').length;
        if (isDebug) console.log("nbElt: "+nbElt);
        switch (nbElt){
            case 1:
                if (isDebug) console.log("show addTenant - hide removeTenant");
                $('#removeTenant').addClass('disabled');
                $('#addTenant').removeClass('disabled');
               break;
            case maxTenant: 
                if (isDebug) console.log("show removeTenant - hide addTenant");
                $('#addTenant').addClass('disabled');
                $('#removeTenant').removeClass('disabled');
                break;
            default:
                if (isDebug) console.log("show BOTH!");
                $('#addTenant').removeClass('disabled');
                $('#removeTenant').removeClass('disabled');
        }
        setTotal();
    }

    $('#addTenant').click(function(){
        if (!$('#maxQty').hasClass('display-no')) {
            var maxVal = Number($('#maxQty').val());
            var sumQuantity= 0 ;
            $('#containerFields input[type="number"]').each(function(){
                sumQuantity += Number($(this).val())
            });
            if (sumQuantity > maxVal) {
                alert($('h2').attr('data-exceedMsg'))
                return false;
            }
        }
        if ($('#containerFields > .control').length < maxTenant) {
            $(this).closest("#control").before($('#addTenantField').html());
            var id= $('#containerFields > .control').length-1;
            $('#containerFields > .control').last().find('.tenant input').attr('name', 'tenant'+id);
            $('#containerFields > .control').last().find('.tenant input').attr('disabled', false);
            $('#containerFields > .control').last().find('.quantity input').attr('name', 'quantity'+id);
            $('#containerFields > .control').last().find('.quantity input').attr('disabled', false);
        }
        showHideButtons();
    });

    $('#removeTenant').click(function(){
        var id= $('#containerFields > .control').length;
        if (id > 1) {
            $('#containerFields .control.columns.is-mobile').last().remove();
        }
        showHideButtons();
    })

    showHideButtons();

    $(document).on({
        'keyup': function(){
            var sumQuantity= setTotal();
            var maxVal = $('input[type="number"]').attr('max');
            if (sumQuantity > maxVal) {
                alert($('h2').attr('data-exceedMsg'));
                $('#total').val(maxVal);
                $(this).val( maxVal - (sumQuantity - $(this).val()));
                return false;
            }
        }    
    }, 'input[name^="quantity"]');

    $(document).on('click', 'input[type=submit]', function(event){
        // Cancel, no check to do.
        if (jQuery.type($(this).attr('formnovalidate'))==='string') {return true;}

        // We trim all input text
        $("input[type='text']").each(function(){
            $(this).val($.trim($(this).val()));
        });
    })
})