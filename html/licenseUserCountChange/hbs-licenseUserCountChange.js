(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Button'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<div class=\"control\">\r\n		<input class=\"button "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"post",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" type=\""
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.type : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "\" \r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"cancel",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		>\r\n	</div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return " post ";
},"4":function(container,depth0,helpers,partials,data) {
    var helper;

  return container.escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"type","hash":{},"data":data}) : helper)));
},"6":function(container,depth0,helpers,partials,data) {
    return "submit";
},"8":function(container,depth0,helpers,partials,data) {
    return " formnovalidate ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"field has-addons row-btns\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n</div>";
},"useData":true});
templates['Main'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <form id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" method=\""
    + alias4(((helper = (helper = helpers.method || (depth0 != null ? depth0.method : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"method","hash":{},"data":data}) : helper)))
    + "\" action=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" ondragstart=\"return false;\">\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers.unless.call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"unless","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    return "                    "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    return "            "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.main : depth0),{"name":"with","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n    <div id=\"containerFields\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n</form>\r\n";
},"useData":true});
templates['licenseUserCountChange'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "            <label class=\"radio\"><input type=\"radio\" name=\"nextNow\" value=\""
    + container.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"checked",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "><span>"
    + ((stack1 = ((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span></label>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return " checked ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div id=\"header\">\r\n    <h2>"
    + ((stack1 = ((helper = (helper = helpers.labelIntro || (depth0 != null ? depth0.labelIntro : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelIntro","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</h2>\r\n    <h3>"
    + ((stack1 = ((helper = (helper = helpers.labelSubIntro || (depth0 != null ? depth0.labelSubIntro : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelSubIntro","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</h3>\r\n</div>\r\n\r\n<div class=\"currentUsr columns is-mobile\">\r\n    <label for=\"\" class=\"column\">"
    + ((stack1 = ((helper = (helper = helpers.currentUsrLabel || (depth0 != null ? depth0.currentUsrLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"currentUsrLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\r\n    <input id=\"currentUsr\" class=\"input column is-2\" type=\"number\" value=\""
    + alias4(((helper = (helper = helpers.currentUsrQuantity || (depth0 != null ? depth0.currentUsrQuantity : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"currentUsrQuantity","hash":{},"data":data}) : helper)))
    + "\" readonly>\r\n</div>\r\n<div class=\"newUsr columns is-mobile\">\r\n    <label for=\"\" class=\"column\">"
    + ((stack1 = ((helper = (helper = helpers.newUsrLabel || (depth0 != null ? depth0.newUsrLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"newUsrLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\r\n    <input id=\"newUrs\" class=\"input column is-2\" type=\"number\" name=\""
    + alias4(((helper = (helper = helpers.newUsrName || (depth0 != null ? depth0.newUsrName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"newUsrName","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + ((stack1 = ((helper = (helper = helpers.currentUsrQuantity || (depth0 != null ? depth0.currentUsrQuantity : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"currentUsrQuantity","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" required min=\""
    + alias4(((helper = (helper = helpers.minNewUsr || (depth0 != null ? depth0.minNewUsr : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"minNewUsr","hash":{},"data":data}) : helper)))
    + "\">\r\n</div>\r\n<div class=\"addRemove columns is-mobile\">\r\n    <label for=\"\" class=\"column\">"
    + ((stack1 = ((helper = (helper = helpers.addRemoveLabel || (depth0 != null ? depth0.addRemoveLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"addRemoveLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\r\n    <input id=\"addRemove\" class=\"input column is-2\" type=\"number\"  readonly value=\"0\">\r\n</div>\r\n\r\n<div class=\"columns is-mobile\">\r\n    <div class=\"column\">"
    + ((stack1 = ((helper = (helper = helpers.updateWhenLabel || (depth0 != null ? depth0.updateWhenLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"updateWhenLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>    \r\n    <div class=\"control column is-2\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n</div>\r\n\r\n<div class=\"validateWrapper\">\r\n    <div><div id=\"validate\">"
    + ((stack1 = ((helper = (helper = helpers.validateLabel || (depth0 != null ? depth0.validateLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"validateLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div></div>\r\n    <div class=\"next display-no\">\r\n        <div class=\"header\">"
    + alias4(((helper = (helper = helpers.chargeNext || (depth0 != null ? depth0.chargeNext : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"chargeNext","hash":{},"data":data}) : helper)))
    + "</div>\r\n        <div class=\"body columns is-mobile\">\r\n            <div class=\"net column\">\r\n                <label>"
    + ((stack1 = ((helper = (helper = helpers.labelNet || (depth0 != null ? depth0.labelNet : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelNet","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\r\n                <div></div>\r\n            </div>\r\n            <div class=\"vat column\">\r\n                <label>"
    + ((stack1 = ((helper = (helper = helpers.labelVat || (depth0 != null ? depth0.labelVat : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelVat","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\r\n                <div></div>\r\n            </div>\r\n            <div class=\"total column\">\r\n                <label>"
    + ((stack1 = ((helper = (helper = helpers.labelTotal || (depth0 != null ? depth0.labelTotal : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelTotal","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\r\n                <div></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"now display-no\">\r\n        <div class=\"header\">"
    + alias4(((helper = (helper = helpers.chargeNow || (depth0 != null ? depth0.chargeNow : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"chargeNow","hash":{},"data":data}) : helper)))
    + "</div>\r\n        <div class=\"body columns is-mobile\">\r\n            <div class=\"net column\">\r\n                <label>"
    + ((stack1 = ((helper = (helper = helpers.labelNet || (depth0 != null ? depth0.labelNet : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelNet","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\r\n                <div></div>\r\n            </div>\r\n            <div class=\"vat column\">\r\n                <label>"
    + ((stack1 = ((helper = (helper = helpers.labelVat || (depth0 != null ? depth0.labelVat : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelVat","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\r\n                <div></div>\r\n            </div>\r\n            <div class=\"total column\">\r\n                <label>"
    + ((stack1 = ((helper = (helper = helpers.labelTotal || (depth0 != null ? depth0.labelTotal : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelTotal","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\r\n                <div></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n<div id=\"control\">\r\n    <div id=\"warningMsg\">\r\n        <i class=\""
    + alias4(((helper = (helper = helpers.warningIcon || (depth0 != null ? depth0.warningIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"warningIcon","hash":{},"data":data}) : helper)))
    + "\"></i>\r\n        <p>"
    + ((stack1 = ((helper = (helper = helpers.warningMsg || (depth0 != null ? depth0.warningMsg : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"warningMsg","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " <span class=\"subInfo\"><br>"
    + ((stack1 = ((helper = (helper = helpers.warningSubMsg || (depth0 != null ? depth0.warningSubMsg : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"warningSubMsg","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span></p>\r\n        <input id=\"keyValidation\" class=\"input\" type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.keyValidation || (depth0 != null ? depth0.keyValidation : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyValidation","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + ((stack1 = ((helper = (helper = helpers.keyVal_popup || (depth0 != null ? depth0.keyVal_popup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyVal_popup","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" required pattern=\"[A-Za-z0-9]{5,}\">\r\n    </div>\r\n    <div id=\"msgTxt\">"
    + ((stack1 = ((helper = (helper = helpers.labelFooter || (depth0 != null ? depth0.labelFooter : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelFooter","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n</div>\r\n";
},"useData":true});
})();