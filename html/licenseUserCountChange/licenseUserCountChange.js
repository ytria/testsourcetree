$(document).ready(function(){
 
    $(document).on({
        'keyup': function(){
            var newUrs = $("#newUrs").val();
            var currentUsr = $("#currentUsr").val();
            if ( newUrs - currentUsr < 0) {
                $('input[name="nextNow"][value="now"]').attr('disabled',true);
                $('input[name="nextNow"][value="next"]').prop('checked',true)
            } 
            else $('input[name="nextNow"][value="now"]').attr('disabled',false);
            $("#addRemove").val(newUrs - currentUsr);
             if ( parseInt(currentUsr) > parseInt(newUrs) ) { 
                $('#now').attr("disabled", "true")
            } else { 
                $('#now').removeAttr("disabled")
            }
        }    
    }, '#newUrs');

    $("#validate").click(function()
	{
        var newUsr 			= $("#newUrs").val();
        var nextNow 		= $("input[name='nextNow']:checked").val();
        var validationKey 	= $("#keyValidation").val();

        var arrValINT = window.external.Simulate(newUsr, nextNow, validationKey);
        if (arrValINT.length > 0)
        {
            var arrVal = JSON.parse(arrValINT);
        
            if (arrVal.AlignmentCustomerNetPrice != 0) {
                $('.now').removeClass('display-no'); //header
                $('.now .net div').text(arrVal.AlignmentCustomerNetPrice)
                $('.now .vat div').text(arrVal.AlignmentCustomerVatPrice)
                $('.now .total div').html("<span class='currency'>"+ arrVal.PriceCurrencyId + "</span> " + arrVal.AlignmentCustomerGrossPrice)
            }

            $('.next').removeClass('display-no');
            if (arrVal.next_billing_date && ($('.next .header').text().slice(-1) != ")")) $('.next .header').text($('.next .header').text() + " (" + arrVal.next_billing_date + ")")
            $('.next .net div').text(arrVal.NextBillingCustomerNetPrice)
            $('.next .vat div').text(arrVal.NextBillingCustomerVatPrice)
            $('.next .total div').html("<span class='currency'>"+ arrVal.PriceCurrencyId + "</span> " + arrVal.NextBillingCustomerGrossPrice)			
        } 

    })

    $(document).on('click', 'input[type=submit]', function(event){
        // Cancel, no check to do.
        if (jQuery.type($(this).attr('formnovalidate'))==='string') {return true;}

        // We trim all input text
        $("input[type='text']").each(function(){
            $(this).val($.trim($(this).val()));
        });
    })
 
}) //$(document).ready

