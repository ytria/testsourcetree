var ready = false;
$(document).ready(function(){

	$('#mainBlockBorder').click( function(){
		$(this).find('.globe').toggleClass(' rotateIcon' );
		$('#mainInfId').toggleClass('hiddenBlock');
		$('#mainInfId').toggleClass('shownBlock');
		$('#blockRight').toggleClass('blockRightHidden');
		$('#blockRight').toggleClass('blockRightShown');
	});
	$(".row-header").on('click', function () {
		if ($(".jobs-container").hasClass("displayNO")) {
			$(".blockLeft:not(.jobs-container)").addClass("displayNO");
			$(".blockLeft:not(.jobs-container)").find(".row-header, .row-block").addClass("displayNO");
			$(".jobs-container").removeClass("displayNO");
			$(".jobs-container").find(".row-header, .row-block").removeClass("displayNO");
		}
		else {
			$(".jobs-container").addClass("displayNO");
			$(".blockLeft.jobs-container").addClass("displayNO");
			$(".blockLeft.jobs-container").find(".row-header, .row-block").addClass("displayNO");
			$(".blockLeft:not(.jobs-container)").removeClass("displayNO");
			$(".blockLeft:not(.jobs-container)").find(".row-header, .row-block").removeClass("displayNO");
		}
	});
    ready = true;
    
    $(document).on({
        'mouseover': function(){
            $(this).closest('.blockInfOneCol').find('.noPadding').stop(true).fadeOut();
            $(this).closest('.blockInfOneCol').find('.onHoverBlock').stop(true).fadeIn();
        } ,   
        'mouseleave': function(){
            $(this).closest('.blockInfOneCol').find('.noPadding').stop(true).fadeIn();
            $(this).closest('.blockInfOneCol').find('.onHoverBlock').stop(true).fadeOut();
        }    
    }, '.questionMark');

    $('.jobInfOneCol.PrevNext.active').click( function() {
        // $(this).parent().hide('slide', { direction: $(this).attr('goto') }, 'slow');
        // $('#'+$(this).attr('tgt')).show('slide', { direction: $(this).attr('goto')=='left'?'right':'left' }, 'slow');
        $(this).parent().toggle();
        var curId = $(this).parent().attr('id');
        $('#current-'+curId).toggle();
        $('#other-'+curId).toggle();
        var otherID = $(this).attr('tgt');
        $('#'+otherID).toggle();
        $('#current-'+otherID).toggle();
        $('#other-'+otherID).toggle();

    })

});

function  externalFunction (functName, obj) {
	if (!ready)	{
		tmpObj = obj;
		window.setTimeout("externalFunction('"+functName+"',tmpObj)", 500);	
	}
	else {
		if (functName == 'switchToAdminMode' ) { 
			switchToAdminMode(); 
		}	
		if (functName == 'switchToUserMode' ) {
			switchToUserMode();
		}
		if (functName == 'enableOnPremise' ) {
			enableOnPremise();
		}
	}
}
function switchToAdminMode () {
	$('#blockRight').addClass('adminMode');
	$('#mainInfRightTitle').html(config.items[0]['mainInfTitle_Admin']);
}
function switchToUserMode () {
	$('#blockRight').removeClass('adminMode');
	$('#mainInfRightTitle').html(config.items[0]['mainInfTitle']);
}
function enableOnPremise () {
	$('.singleText').removeClass('displayNO');
}
