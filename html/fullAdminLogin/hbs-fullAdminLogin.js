(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Button'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<div class=\"control "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\r\n		<button class=\"button "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"post",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" type=\""
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.type : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "\" \r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"mode",{"name":"get_main","hash":{},"data":data}),"edit",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"mode",{"name":"get_main","hash":{},"data":data}),"read",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"cancel",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		>\r\n            "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.icon : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            <span>"
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "</span>\r\n        </button>\r\n	</div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return " post ";
},"4":function(container,depth0,helpers,partials,data) {
    var helper;

  return container.escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"type","hash":{},"data":data}) : helper)));
},"6":function(container,depth0,helpers,partials,data) {
    return "submit";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.attribute : depth0),"post",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    return " disabled ";
},"11":function(container,depth0,helpers,partials,data) {
    return " formnovalidate ";
},"13":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<span class=\"icon\"><i class=\""
    + container.escapeExpression(((helper = (helper = helpers.icon || (depth0 != null ? depth0.icon : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"icon","hash":{},"data":data}) : helper)))
    + "\"></i></span>";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"field has-addons row-btns "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
templates['Main'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<form id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" method=\""
    + alias4(((helper = (helper = helpers.method || (depth0 != null ? depth0.method : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"method","hash":{},"data":data}) : helper)))
    + "\" action=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" ondragstart=\"return false;\">\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers.unless.call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"unless","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    return "                    "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    return "                "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "        // AUTOMATION auto submit\r\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.automationSubmit : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"10":function(container,depth0,helpers,partials,data) {
    return "            g_AutomationSubmit = true;\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.main : depth0),{"name":"with","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n    <div id=\"fullAdminkBlock\">\r\n    \r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n </form>\r\n <script language=\"javascript\">\r\n    var g_AutomationSubmit = false;\r\n"
    + ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.main : depth0),{"name":"with","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</script>\r\n";
},"useData":true});
templates['fullAdminLogin'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "            <br> "
    + ((stack1 = ((helper = (helper = helpers.introTextTwo || (depth0 != null ? depth0.introTextTwo : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"introTextTwo","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "  <a href=\""
    + container.escapeExpression(((helper = (helper = helpers.linkRegister || (depth0 != null ? depth0.linkRegister : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkRegister","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.linkRegisterText || (depth0 != null ? depth0.linkRegisterText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkRegisterText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + ".</a>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "<br> <br>\r\n            <i class=\"fas fa-question-square\"></i> "
    + ((stack1 = ((helper = (helper = helpers.introTextThree || (depth0 != null ? depth0.introTextThree : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"introTextThree","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " &quot;<a href=\""
    + container.escapeExpression(((helper = (helper = helpers.linkThree || (depth0 != null ? depth0.linkThree : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkThree","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.linkTextThree || (depth0 != null ? depth0.linkTextThree : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkTextThree","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "&quot;.</a>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "            <div id=\"createNewApp\">\r\n                <a class='button-Fill-Blue' href=\"#\"><i class=\"fas fa-plus\"></i>"
    + ((stack1 = ((helper = (helper = helpers.createNewAppButton || (depth0 != null ? depth0.createNewAppButton : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"createNewAppButton","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\r\n            </div>\r\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "            <p  class=\"bold gray\"> "
    + ((stack1 = ((helper = (helper = helpers.introTitle || (depth0 != null ? depth0.introTitle : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"introTitle","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</p>\r\n";
},"9":function(container,depth0,helpers,partials,data) {
    return " disabled ";
},"11":function(container,depth0,helpers,partials,data) {
    return "disabled_";
},"13":function(container,depth0,helpers,partials,data) {
    return "disabled";
},"15":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.tenantReadOnly : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"17":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<input id=\"tenantNameVal\" class=\"input\" type=\"hidden\" name=\""
    + alias4(((helper = (helper = helpers.tenantField || (depth0 != null ? depth0.tenantField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantField","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.tenantValue || (depth0 != null ? depth0.tenantValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantValue","hash":{},"data":data}) : helper)))
    + "\">";
},"19":function(container,depth0,helpers,partials,data) {
    return "disabled ";
},"21":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.REMOVED_createNewAppButton : depth0),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"23":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<input id=\"appIDval\" type=\"hidden\" disabled name=\""
    + alias4(((helper = (helper = helpers.appIDField || (depth0 != null ? depth0.appIDField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIDField","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.appIDValue || (depth0 != null ? depth0.appIDValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIDValue","hash":{},"data":data}) : helper)))
    + "\">";
},"25":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <!-- globally never used because not needed here -->\r\n        <div class=\"control has-icons-right\">\r\n            <div>\r\n                <label> "
    + ((stack1 = ((helper = (helper = helpers.RedirectURLLabel || (depth0 != null ? depth0.RedirectURLLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RedirectURLLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n                <input id=\"redirectURL\" class=\"input\" title=\""
    + alias4(((helper = (helper = helpers.RedirectURLPopup || (depth0 != null ? depth0.RedirectURLPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RedirectURLPopup","hash":{},"data":data}) : helper)))
    + "\" type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.RedirectURLField || (depth0 != null ? depth0.RedirectURLField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RedirectURLField","hash":{},"data":data}) : helper)))
    + "\" disabled required value=\""
    + alias4(((helper = (helper = helpers.RedirectURLValue || (depth0 != null ? depth0.RedirectURLValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RedirectURLValue","hash":{},"data":data}) : helper)))
    + "\" >\r\n                <span id=\"lock\"><i class=\"fa fa-lock-alt\"></i></span>\r\n            </div>\r\n        </div>\r\n";
},"27":function(container,depth0,helpers,partials,data) {
    return "checked disabled";
},"29":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.keepSecretKeySelected : depth0),{"name":"if","hash":{},"fn":container.program(30, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"30":function(container,depth0,helpers,partials,data) {
    return "checked";
},"32":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                <div id=\"resetKey\"><a class=\"resetKeyAcc\" href=\"#\">\r\n                "
    + ((stack1 = ((helper = (helper = helpers.resetSecretKey || (depth0 != null ? depth0.resetSecretKey : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"resetSecretKey","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "<i class=\"fas fa-key\"></i>"
    + ((stack1 = ((helper = (helper = helpers.resetSecretKeyLink || (depth0 != null ? depth0.resetSecretKeyLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"resetSecretKeyLink","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\r\n                </div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return " <div>\r\n    <div id=\"headerSection\">\r\n        "
    + ((stack1 = ((helper = (helper = helpers.introTextOne || (depth0 != null ? depth0.introTextOne : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"introTextOne","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.introTextTwo : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.introTextThree : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.createNewAppButton : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.introTitle : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n\r\n    <div id=\"inputSection\">\r\n        <div class=\"control\">\r\n            <div>\r\n                <label> "
    + ((stack1 = ((helper = (helper = helpers.displayNameLabel || (depth0 != null ? depth0.displayNameLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"displayNameLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n                <input id=\"displayName\" class=\"input\" title=\""
    + alias4(((helper = (helper = helpers.displayNamePopup || (depth0 != null ? depth0.displayNamePopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"displayNamePopup","hash":{},"data":data}) : helper)))
    + "\"  type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.displayNameField || (depth0 != null ? depth0.displayNameField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"displayNameField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.displayNamePlaceholder || (depth0 != null ? depth0.displayNamePlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"displayNamePlaceholder","hash":{},"data":data}) : helper)))
    + "\" required value=\""
    + alias4(((helper = (helper = helpers.displayNameValue || (depth0 != null ? depth0.displayNameValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"displayNameValue","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.displayNameReadOnly : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"control\">\r\n            <div>\r\n                <label> "
    + ((stack1 = ((helper = (helper = helpers.tenantLabel || (depth0 != null ? depth0.tenantLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n                <input id=\"tenantName\" class=\"input\" title=\""
    + alias4(((helper = (helper = helpers.tenantPopup || (depth0 != null ? depth0.tenantPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantPopup","hash":{},"data":data}) : helper)))
    + "\"  type=\"text\" name=\""
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.tenantValue : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + alias4(((helper = (helper = helpers.tenantField || (depth0 != null ? depth0.tenantField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.tenantPlaceholder || (depth0 != null ? depth0.tenantPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantPlaceholder","hash":{},"data":data}) : helper)))
    + "\" required "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.tenantValue : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.program(15, data, 0),"data":data})) != null ? stack1 : "")
    + " value=\""
    + alias4(((helper = (helper = helpers.tenantValue || (depth0 != null ? depth0.tenantValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tenantValue","hash":{},"data":data}) : helper)))
    + "\" pattern=\"^[a-zA-Z0-9][a-zA-Z0-9-.]*[a-zA-Z0-9]$\">\r\n                "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.tenantValue : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"control\">\r\n            <div>\r\n                <label> "
    + ((stack1 = ((helper = (helper = helpers.appIDLabel || (depth0 != null ? depth0.appIDLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIDLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</label>\r\n                <input id=\"appID\" class=\"input\" title=\""
    + alias4(((helper = (helper = helpers.appIDPopup || (depth0 != null ? depth0.appIDPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIDPopup","hash":{},"data":data}) : helper)))
    + "\"  type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.appIDField || (depth0 != null ? depth0.appIDField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIDField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.appIDPlaceholder || (depth0 != null ? depth0.appIDPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIDPlaceholder","hash":{},"data":data}) : helper)))
    + "\" required value=\""
    + alias4(((helper = (helper = helpers.appIDValue || (depth0 != null ? depth0.appIDValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"appIDValue","hash":{},"data":data}) : helper)))
    + "\" pattern=\"[a-zA-Z0-9-]+\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.appIDReadOnly : depth0),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.program(21, data, 0),"data":data})) != null ? stack1 : "")
    + ">\r\n                "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.createNewAppButton : depth0),{"name":"if","hash":{},"fn":container.program(23, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            </div>\r\n        </div>\r\n\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.RedirectURLField : depth0),{"name":"if","hash":{},"fn":container.program(25, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n        <div class=\"control has-icons-right\">\r\n            <div>\r\n                <label> "
    + ((stack1 = ((helper = (helper = helpers.secretKeyLabel || (depth0 != null ? depth0.secretKeyLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"secretKeyLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n                <input id=\"secretKey\" class=\"input\" title=\""
    + alias4(((helper = (helper = helpers.secretKeyPopup || (depth0 != null ? depth0.secretKeyPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"secretKeyPopup","hash":{},"data":data}) : helper)))
    + "\" type=\"password\" name=\""
    + alias4(((helper = (helper = helpers.secretKeyField || (depth0 != null ? depth0.secretKeyField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"secretKeyField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.secretKeyPlaceholder || (depth0 != null ? depth0.secretKeyPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"secretKeyPlaceholder","hash":{},"data":data}) : helper)))
    + "\" required value=\""
    + alias4(((helper = (helper = helpers.secretKeyValue || (depth0 != null ? depth0.secretKeyValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"secretKeyValue","hash":{},"data":data}) : helper)))
    + "\" >\r\n                <span id=\"icon\"><i class=\"fa fa-eye\"></i></span>\r\n            </div>\r\n            <div class=\"controlMessage gray\" >\r\n                <div>\r\n                  <input id=\"keepCheck\" type=\"checkbox\" name=\""
    + alias4(((helper = (helper = helpers.keepSecretKeyField || (depth0 != null ? depth0.keepSecretKeyField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keepSecretKeyField","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.keepSecretKeyValue || (depth0 != null ? depth0.keepSecretKeyValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keepSecretKeyValue","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.createNewAppButton : depth0),{"name":"if","hash":{},"fn":container.program(27, data, 0),"inverse":container.program(29, data, 0),"data":data})) != null ? stack1 : "")
    + "> "
    + ((stack1 = ((helper = (helper = helpers.keepSecretKeyText || (depth0 != null ? depth0.keepSecretKeyText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keepSecretKeyText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n                </div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.resetSecretKey : depth0),{"name":"if","hash":{},"fn":container.program(32, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\r\n\r\n        </div>\r\n\r\n        <div id=\"adminConsentLink\">\r\n            <div>\r\n                <i class=\"fas fa-exclamation-triangle\"></i>\r\n                <p> "
    + ((stack1 = ((helper = (helper = helpers.adminConsentInfoOne || (depth0 != null ? depth0.adminConsentInfoOne : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"adminConsentInfoOne","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "<br>"
    + ((stack1 = ((helper = (helper = helpers.adminConsentInfoTwo || (depth0 != null ? depth0.adminConsentInfoTwo : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"adminConsentInfoTwo","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</p>\r\n            </div>\r\n            <a class='button-Fill-Blue' base_href=\""
    + alias4(((helper = (helper = helpers.adminConsentAction || (depth0 != null ? depth0.adminConsentAction : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"adminConsentAction","hash":{},"data":data}) : helper)))
    + "\" href=\"#\">"
    + ((stack1 = ((helper = (helper = helpers.adminConsentButton || (depth0 != null ? depth0.adminConsentButton : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"adminConsentButton","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\r\n            <span class=\"button-Disabled\" id='disabledGetAdmin'>"
    + ((stack1 = ((helper = (helper = helpers.adminConsentButton || (depth0 != null ? depth0.adminConsentButton : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"adminConsentButton","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\r\n        </div>\r\n       \r\n\r\n    </div>  \r\n</div>";
},"useData":true});
})();