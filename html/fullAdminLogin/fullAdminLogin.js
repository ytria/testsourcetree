function setAdminConsent() {
    $("#appID").removeClass('isRequired');
    if ( ($('#appID').val()=="") || ($('#tenantName').val()=="")|| ($('#redirectURL').val()=="") ) {
        $('#adminConsentLink a').hide(); 
        $('#disabledGetAdmin').show(); 
    } else {
        $('#adminConsentLink a').show();
        $('#disabledGetAdmin').hide(); 
    }
    if ($('#appID').val()=="") {
        $('#resetKey').hide();
    } else {
        $('#resetKey').show();
    }

    // We disabled by default the Key input if the App ID is empty and we have a Create button
    if($('#createNewApp').length) {
        if ($.trim($('#appID').val())) {
            $('#secretKey').prop('disabled', false);
        } else {
            $('#secretKey').prop('disabled', true);
        }
    }
    // Be sure to enforece the eye as soon as we can
    if($('#secretKey').val()) {
        $('#secretKey').attr('type', 'password')
        $('#icon').html('<i class="fa fa-eye"></i>')
    }
}

function setApplicationInfo(id, key, tenant, displayName) {
    $('#appID').val(id);
    $('#secretKey').val(key);
    if (tenant) {
        $('#tenantName').val(tenant);
        $('#tenantNameVal').val(tenant);
    }
    if (displayName)
        $('#displayName').val(displayName);	
    // we force the save of the secret key
    $('#keepCheck').prop('checked', true);
    $('#appID').prop('disabled', true);
    // Used for the POST - since the other one is Disabled
    $('#appIDval').prop('disabled', false);
    $('#appIDval').val(id);
    setAdminConsent();
}

function setNewKey(key) {
    $('#secretKey').val(key);
    // we force the save of the secret key
    $('#keepCheck').prop('checked', true);
    setAdminConsent();
}

$(document).ready(function(){

    // Show the create new button all the time.
    $('#createNewApp a').show();

    if ($('#secretKey').val()=="") {
        if (!$('#createNewApp').length) {
            $('#keepCheck').removeAttr("checked");
        }
    }

    setAdminConsent();
    $('#appID').on("input",function(){
        // direct input -- we need to disable the Hidden one to avoid a double POST
        $('#appIDval').prop('disabled', true);
        setAdminConsent();
    })
    $('#tenantName').on("input",function(){
        setAdminConsent();
    })
    $('#redirectURL').on("input",function(){
        setAdminConsent();
    })

    $('#resetKey a').click( function() {
        try {
            window.external.resetAppKey($('#appID').val(), $('#tenantName').val());
        } catch (err) {
            alert("Reset New Key - Error on Submit: \n\n"+err.message);
            setNewKey("theKEY"); // DEBUG
        }
        setAdminConsent();
    });

    $('#createNewApp a').click( function() {
        try {
            window.external.createNewApplication();
        } catch (err) {
            alert("Create New Application - Error on Submit: \n\n"+err.message);
            setApplicationInfo("theID","theKEY", "theTenant", "theDisplayName"); // DEBUG
        }
        setAdminConsent();
    });

    $('#adminConsentLink a').on('click', function(e) {
        e.originalEvent.currentTarget.href = $('#adminConsentLink a').attr('base_href')+$('#tenantName').val()+"/"+$('#appID').val()+ "/" +$('#redirectURL').val();
    });

    $('#icon')
    .mousedown(function(){
        $('#secretKey').attr('type', 'text')
        $('#icon').html('<i class="fa fa-lock"></i>')
    })
    .mouseup(function(){
        $('#secretKey').attr('type', 'password')
        $('#icon').html('<i class="fa fa-eye"></i>')
    })
    $('#secretKey').focusout(function(){
        $('#secretKey').attr('type', 'password')
        $('#icon').html('<i class="fa fa-eye"></i>')
    })

    $('#lock').click(function(){
        if ($(this).find('svg').hasClass("fa-lock-alt")) {
            $('#redirectURL').attr('disabled',false)
            $('#lock').html('<i class="fa fa-unlock-alt"></i>')
        }
        else {
            $('#redirectURL').attr('disabled',true);
            $('#lock').html('<i class="fa fa-lock-alt"></i>');
            $('#redirectURL').val($('#redirectURL').attr('value'))
        }
    })

    if (g_AutomationSubmit) {
        console.log("!! AUTOMATION AUTO_SUBMIT !!");
        $('input.post').click();
    }

})