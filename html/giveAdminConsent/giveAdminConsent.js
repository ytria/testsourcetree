$(document).ready(function(){
    
    $('#lock').click(function(){
        if ($(this).find('svg').hasClass("fa-lock-alt")) {
            $('#redirectURL').attr('disabled',false)
            $('#lock').html('<i class="fa fa-unlock-alt"></i>')
        }
        else {
            $('#redirectURL').attr('disabled',true);
            $('#lock').html('<i class="fa fa-lock-alt"></i>');
            $('#redirectURL').val($('#redirectURL').attr('value'))
        }
    })

})