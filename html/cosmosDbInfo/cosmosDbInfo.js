function setTestConnect() {
    $("#cosmosuri").removeClass('isRequired');
    if ($.trim($('#cosmosurival').val()) && $.trim($('#cosmoskey').val())) {
        $('#warningInfo a').show();
        $('#disabledTestConnect').hide();
    } else {
        $('#warningInfo a').hide();
        $('#disabledTestConnect').show();
    }
    // We disabled by default the Key input if the URI is empty and we have a Create button
    if($('#createNewInfo').length) {
        if ($.trim($('#cosmosurival').val())) {
            $('#cosmoskey').prop('disabled', false);
        } else {
            $('#cosmoskey').prop('disabled', true);
        }
    }
}

function setCredentials(uri, key) {
    // Value shown to user
    $('#cosmosuri').val(uri);
    $('#cosmoskey').val(key);
    // Used for the POST
    $('#cosmosurival').val(uri);
    setTestConnect();
}

$(document).ready(function () {

    // Show the create new button all the time.
    $('#createNewInfo a').show();

    setTestConnect();
    $('#cosmosuri').on("input",function(){
        // cosmosURI won't post: pass new value to cosmosurival
        $('#cosmosurival').val($('#cosmosuri').val());
        setTestConnect();
    })
    $('#cosmoskey').on("input",function(){
        setTestConnect();
    })

    $('#createNewInfo a.newAccount').click( function() {
        try {
            window.external.createNewAccount();
        } catch (err) {
            alert("Create New Account - Error on Submit: \n\n"+err.message);
            setCredentials("theURI", "theKEY"); // DEBUG
        }
        setTestConnect();
    });

    $('#createNewInfo a.selAccount').click( function() {
        try {
            window.external.selectExistingAccount();
        } catch (err) {
            alert("Select Existing Account - Error on Submit: \n\n"+err.message);
            setCredentials("theURI", "theKEY"); // DEBUG
        }
        setTestConnect();
    });

    $('#deleteOldAccount a').click( function() {
        try {
            window.external.deleteOldAccount();

        } catch (err) {
            alert("Delete Old Account - Error on Submit: \n\n"+err.message);
        }
    });


    $('#warningInfo a').click( function() {
        var uri = $("#cosmosurival").val();
        var key = $("#cosmoskey").val();
        try {
            // The last "YES" is to tell C++ that we need a dialog
            // even when all is correct
            var retVal = window.external.testConnect(uri, key, "YES");
            // No return is needed here
        } catch (err) {
            alert("Test Connection - Error on Submit: \n\n"+err.message);
        }
    });

    $('#icon')
    .mousedown(function(){
        $('#cosmoskey').attr('type', 'text')
        $('#icon').html('<i class="fa fa-lock"></i>')
    })
    .mouseup(function(){
        $('#cosmoskey').attr('type', 'password')
        $('#icon').html('<i class="fa fa-eye"></i>')
    })

    $('#lock').click(function(){
        if ($(this).find('svg').hasClass("fa-lock-alt")) {
            $('#cosmosuri').attr('disabled',false)
            $('#lock').html('<i class="fa fa-unlock-alt"></i>')
        }
        else {
            $('#cosmosuri').attr('disabled',true);
            $('#lock').html('<i class="fa fa-lock-alt"></i>');
            // reinit value shown to user
            $('#cosmosuri').val($('#cosmosuri').attr('value'));
            // reinit value that will be posted
            $('#cosmosurival').val($('#cosmosuri').val());
        }
    })

    // *************************************************************************
    // cosmosDBSelect

    // IE does not put a red square on a required radio if not set
    // so we intercept the submit here and do the work if needed
    $(document).on('click', 'input[type=submit]', function(event){
        // Cancel, no check to do.
        if (jQuery.type($(this).attr('formnovalidate'))==='string') {return true;}

        // Check all the Radios
        var allRadios = new Array();
        $("input[type='radio']").each(function() {
            if ($(this).prop('required') == true){
                if ($(this).is(':checked')) {
                    allRadios[$(this).attr("name")] = true;
                } else {
                    if ( 'undefined' == typeof allRadios[$(this).attr("name")] ) {
                        allRadios[$(this).attr("name")] = false;
                    }
                }
            }
        })
        for (var key in allRadios) {
            if (!allRadios[key]) {
                $("#"+key+" .radiobox").addClass('isRequired');
                event.preventDefault();
            } else {
                $("#"+key+" .radiobox").removeClass('isRequired');
            }
        }

        // We trim all input text
        var mustStop = false;
        $("input[type='text'], input[type='password']").each(function(){
            $(this).val($.trim($(this).val()));
            if ($(this).prop('required') == true){
                if (!$(this).val()) {
                    // one of the required text is empty -  it will be handle by the Submit
                    mustStop = true;
                    // This is to handle the URI field that may be disabled but is still required...
                    if($(this).prop('disabled') == true){
                        // we need to handle the highlight ourselves, bcose the submit won't
                        $(this).addClass('isRequired');
                        event.preventDefault();
                    }
                } else {
                    if($(this).prop('disabled') == true){
                        $(this).removeClass('isRequired');
                    }
                }

            }
        });
        if (mustStop) { return; }

        if ($('#warningInfo a').is(':visible')) {
            // Credentials Edit --- test their validity
            var uri = $("#cosmosurival").val();
            var key = $("#cosmoskey").val();
            try {
                // we don't want a dialog if all is fine, so the 3rd param ""
                if( "OK" != window.external.testConnect(uri, key, "")) {
                    event.preventDefault();
                };
            } catch (err) {
                alert("Test Connection - Error on Submit: \n\n"+err.message);
                // DEBUG - we let go // event.preventDefault();
            }
        }
    });

    // Nice radio button
    $(document).on('click', 'input.label-override', function(){
        if ($(this).attr('type') === 'radio') {
            // Radio: only one can be checked
            $('input[name='+$(this).attr('name')+']').each(function(){
                if ($(this).is(':checked') === true) {
                    $(this).parent().find('span.radioYes').removeClass('display-no');
                    $(this).parent().find('span.radioNo').addClass('display-no');
                } else {
                    $(this).parent().find('span.radioYes').addClass('display-no');
                    $(this).parent().find('span.radioNo').removeClass('display-no');
                }
            });
        }
    })
    // Handle the keyboard on the Nice Radio Buttons
    $(document).on('keydown', '.oneradio, .setonesku', function(event) {
        if ( event.which == 13 || event.which == 32 ) {
            // Enter Key - Space Key
            theInput = $(this).find('input');
            if (theInput.attr('type') === 'radio') {
                // No idea why, but we need 2 clicks to effectively
                // change the checked situation on a radio ?!?
                theInput.click();
            }
            theInput.click();
            event.preventDefault();
        }
    });

    // **** Quick Search field on key
    $(document).on('keyup', '.control-top .radiobox .sortradio input' , function() {
        tgtVal = $(this).val().toLowerCase();
        var hasHiddenSelected = false;
        var theList = '';
        $(this).parent().parent().find('.radiobox').find('.oneradio').each(function(){
            if (!$(this).hasClass('mainchoice')) { // we don't hide the main categories...
                theVal = $(this).attr('label-sort');
                if (~theVal.toLowerCase().indexOf(tgtVal)) {
                    $(this).removeClass('display-no');
                } else {
                    $(this).addClass('display-no');
                    if ($(this).find('input').is(':checked')) {
                        theList = theList +" > "+theVal+"<br>";
                        hasHiddenSelected = true;
                    }
                }
            }
        })
        if (!hasHiddenSelected) {
            $(this).parent().parent().parent().find('label div.curselect').html("");
            $(this).parent().parent().parent().find('label div.curselect').addClass('display-no');
        } else {
            $(this).parent().parent().parent().find('label div.curselect').html(theList);
            $(this).parent().parent().parent().find('label div.curselect').removeClass('display-no');
        }
    })

    // Click on Icons for quick search, sort and expand/collapse
    $(document).on('click', '.control-top .sortradio svg', function() {
        if ($(this).hasClass('fa-search')) {
            // Search Icon
            $(this).parent().find('input').toggleClass('display-no');
            if ( !$(this).parent().find('input').hasClass('display-no') ) {
                $(this).parent().find('input').focus();
            } else {
                $(this).parent().find('input').val('');
                $(this).parent().parent().find('.radiobox').find('.oneradio').each(function(){
                    $(this).removeClass('display-no');
                })
            }
        } else if ($(this).hasClass('isexpand')) {
            // expand the section
            $(this).parent().parent().parent().removeClass('heightlimited');
            $(this).parent().addClass('display-no');
            $(this).parent().next().removeClass('display-no');
        } else if ($(this).hasClass('iscollapse')) {
            // collapse the section
            $(this).parent().parent().parent().addClass('heightlimited');
            $(this).parent().addClass('display-no');
            $(this).parent().prev().removeClass('display-no');
        } else {
            // 3 sort Icons
            var order = 'default';
            if ($(this).hasClass('fa-sort-alpha-up')) { order = 'up'; }
            if ($(this).hasClass('fa-sort-alpha-down')) { order = 'down'; }
            $(this).siblings().removeClass('sort-current');
            $(this).addClass('sort-current');

            var theDiv = $(this).parent().parent().find('.radiobox');
            var newOrder = theDiv.find('.oneradio').sort(function(a,b) {
                if (order == 'up') {
                    return $(a).attr('label-sort') < $(b).attr('label-sort')  ? 1 : -1;
                } else if (order == 'down') {
                    return $(a).attr('label-sort') > $(b).attr('label-sort')  ? 1 : -1;
                } else {
                    return Number($(a).attr('idx')) > Number($(b).attr('idx'))  ? 1 : -1;
                }
            });
            theDiv.html(newOrder);
            }
        });
    // We don't want the Enter to trigg in this quick search field
    $(document).on('keydown', '.control-top .radiobox .sortradio input', function(event) {
        if ( event.which == 13 ) {
            // Enter Key
            event.preventDefault();
        } else if ( event.which == 27 ) {
            // Esc Key
            $(this).parent().find('.fa-search').click();
            event.preventDefault();
        }
    });
    // rbacFilter *******************************
    // Showing the property list related to the target
    function displayResGroupList() {
        $('input[name='+$('#subscriptions').attr('srcField')+']').each(function(){
            if ($(this).is(':checked') === true) {
                $('#'+$(this).val()).removeClass('display-no');
                $('#'+$(this).val()).find('.radiobox .allradio input').prop({'required': true});
            } else {
                $('#'+$(this).val()).addClass('display-no');
                $('#'+$(this).val()).find('.radiobox .allradio input').prop({'required': false});
            }
        });
    }
    if ($('#subscriptions').length) {
        displayResGroupList();
        $('input[name='+$('#subscriptions').attr('srcField')+']').click(function(){
            displayResGroupList();
        })
    }
})