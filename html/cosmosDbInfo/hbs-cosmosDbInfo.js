(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Button'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<div class=\"control "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\r\n		<button class=\"button "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"post",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" type=\""
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.type : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "\" \r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"mode",{"name":"get_main","hash":{},"data":data}),"edit",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"mode",{"name":"get_main","hash":{},"data":data}),"read",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"cancel",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		>\r\n            "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.icon : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            <span>"
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "</span>\r\n        </button>\r\n	</div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return " post ";
},"4":function(container,depth0,helpers,partials,data) {
    var helper;

  return container.escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"type","hash":{},"data":data}) : helper)));
},"6":function(container,depth0,helpers,partials,data) {
    return "submit";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.attribute : depth0),"post",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    return " disabled ";
},"11":function(container,depth0,helpers,partials,data) {
    return " formnovalidate ";
},"13":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<span class=\"icon\"><i class=\""
    + container.escapeExpression(((helper = (helper = helpers.icon || (depth0 != null ? depth0.icon : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"icon","hash":{},"data":data}) : helper)))
    + "\"></i></span>";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"field has-addons row-btns "
    + container.escapeExpression(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true});
templates['Main'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<form id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" method=\""
    + alias4(((helper = (helper = helpers.method || (depth0 != null ? depth0.method : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"method","hash":{},"data":data}) : helper)))
    + "\" action=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" ondragstart=\"return false;\">\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers.unless.call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"unless","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    return "                    "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    return "            "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.main : depth0),{"name":"with","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n    <div id=\"mainBlock\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n </form>";
},"useData":true});
templates['cosmosDbInfo'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "            "
    + ((stack1 = ((helper = (helper = helpers.introTextOne || (depth0 != null ? depth0.introTextOne : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"introTextOne","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " <br>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "            "
    + ((stack1 = ((helper = (helper = helpers.introTextTwo || (depth0 != null ? depth0.introTextTwo : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"introTextTwo","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " <a href=\""
    + container.escapeExpression(((helper = (helper = helpers.linkRegister || (depth0 != null ? depth0.linkRegister : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkRegister","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.linkRegisterText || (depth0 != null ? depth0.linkRegisterText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkRegisterText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>. <br>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "            <br>\r\n            <i class=\"fas fa-question-square\" focusable=\"false\"></i> "
    + ((stack1 = ((helper = (helper = helpers.introTextThree || (depth0 != null ? depth0.introTextThree : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"introTextThree","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " &quot;<a href=\""
    + container.escapeExpression(((helper = (helper = helpers.linkThree || (depth0 != null ? depth0.linkThree : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkThree","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.linkTextThree || (depth0 != null ? depth0.linkTextThree : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkTextThree","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "&quot;</a>.\r\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "        <div id=\"createNewInfo\">\r\n            <a class='button-Fill-Blue newAccount' href=\"#\"><i class=\"fas fa-plus\"></i>"
    + ((stack1 = ((helper = (helper = helpers.createNewButton || (depth0 != null ? depth0.createNewButton : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"createNewButton","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.selectExistButton : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "            <a class='button-Fill-Blue selAccount' href=\"#\"><i class=\"fas fa-list\"></i>"
    + ((stack1 = ((helper = (helper = helpers.selectExistButton || (depth0 != null ? depth0.selectExistButton : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"selectExistButton","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\r\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"control has-icons-right\">\r\n                <div>\r\n                    <label> "
    + ((stack1 = ((helper = (helper = helpers.uriLabel || (depth0 != null ? depth0.uriLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uriLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n                    <input id=\"cosmosuri\" class=\"input "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.uriPopup || (depth0 != null ? depth0.uriPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uriPopup","hash":{},"data":data}) : helper)))
    + "\" type=\"text\" placeholder=\""
    + ((stack1 = ((helper = (helper = helpers.uriPlaceholder || (depth0 != null ? depth0.uriPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uriPlaceholder","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" required disabled value=\""
    + alias4(((helper = (helper = helpers.uriValue || (depth0 != null ? depth0.uriValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uriValue","hash":{},"data":data}) : helper)))
    + "\">\r\n                    "
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.uriNoUnLock : depth0),{"name":"unless","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                    <input id=\"cosmosurival\" type=\"hidden\" name=\""
    + alias4(((helper = (helper = helpers.uriField || (depth0 != null ? depth0.uriField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uriField","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.uriValue || (depth0 != null ? depth0.uriValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uriValue","hash":{},"data":data}) : helper)))
    + "\">\r\n                </div>\r\n            </div>\r\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "<span id=\"lock\"><i class=\"fa fa-lock-alt\"></i></span>";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"control\">\r\n                <div>\r\n                    <label> "
    + ((stack1 = ((helper = (helper = helpers.uriLabel || (depth0 != null ? depth0.uriLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uriLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n                    <input id=\"cosmosuri\" class=\"input\" title=\""
    + alias4(((helper = (helper = helpers.uriPopup || (depth0 != null ? depth0.uriPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uriPopup","hash":{},"data":data}) : helper)))
    + "\"  type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.uriField || (depth0 != null ? depth0.uriField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uriField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + ((stack1 = ((helper = (helper = helpers.uriPlaceholder || (depth0 != null ? depth0.uriPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uriPlaceholder","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" required value=\""
    + alias4(((helper = (helper = helpers.uriValue || (depth0 != null ? depth0.uriValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uriValue","hash":{},"data":data}) : helper)))
    + "\" >\r\n                </div>\r\n            </div>\r\n";
},"15":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "            <div id=\"deleteOldAccount\">\r\n                "
    + ((stack1 = ((helper = (helper = helpers.deleteOldAccount || (depth0 != null ? depth0.deleteOldAccount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"deleteOldAccount","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "<a class=\"deleteOldAcc\" href=\"#\"><i class=\"fas fa-broom\"></i>"
    + ((stack1 = ((helper = (helper = helpers.deleteOldAccountLink || (depth0 != null ? depth0.deleteOldAccountLink : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"deleteOldAccountLink","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\r\n            </div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return " <div>\r\n    <div id=\"headerSection\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.introTextOne : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.introTextTwo : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.introTextThree : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n    <div id=\"inputSection\">\r\n\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.createNewButton : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.uriReadOnly : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.program(13, data, 0),"data":data})) != null ? stack1 : "")
    + "        <div class=\"control has-icons-right\">\r\n            <div>\r\n                <label> "
    + ((stack1 = ((helper = (helper = helpers.keyLabel || (depth0 != null ? depth0.keyLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " </label>\r\n                <input id=\"cosmoskey\" class=\"input\" title=\""
    + alias4(((helper = (helper = helpers.keyPopup || (depth0 != null ? depth0.keyPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyPopup","hash":{},"data":data}) : helper)))
    + "\" type=\"password\" name=\""
    + alias4(((helper = (helper = helpers.keyField || (depth0 != null ? depth0.keyField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyField","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + ((stack1 = ((helper = (helper = helpers.keyPlaceholder || (depth0 != null ? depth0.keyPlaceholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyPlaceholder","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" required value=\""
    + alias4(((helper = (helper = helpers.keyValue || (depth0 != null ? depth0.keyValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyValue","hash":{},"data":data}) : helper)))
    + "\" >\r\n                <span id=\"icon\"><i class=\"fa fa-eye\" focusable=\"false\"></i></span>\r\n            </div>\r\n        </div>\r\n\r\n        <div id=\"warningInfo\">\r\n            <div>\r\n                <i class=\""
    + alias4(((helper = (helper = helpers.warningInfoIcon || (depth0 != null ? depth0.warningInfoIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"warningInfoIcon","hash":{},"data":data}) : helper)))
    + "\"></i>\r\n                <p> "
    + ((stack1 = ((helper = (helper = helpers.warningInfoOne || (depth0 != null ? depth0.warningInfoOne : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"warningInfoOne","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "<br>"
    + ((stack1 = ((helper = (helper = helpers.warningInfoTwo || (depth0 != null ? depth0.warningInfoTwo : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"warningInfoTwo","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</p>\r\n            </div>\r\n            <a class='button-Fill-Blue' href=\"#\"><i class=\"fa fa-check-circle\"></i>"
    + ((stack1 = ((helper = (helper = helpers.testConnectButton || (depth0 != null ? depth0.testConnectButton : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"testConnectButton","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>\r\n            <span class=\"button-Disabled\" id='disabledTestConnect'><i class=\"fa fa-check-circle\"></i>"
    + ((stack1 = ((helper = (helper = helpers.testConnectButton || (depth0 != null ? depth0.testConnectButton : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"testConnectButton","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\r\n        </div>\r\n\r\n        <div id=\"control\">\r\n            <div id=\"keyValidationMsg\">\r\n                <i class=\""
    + alias4(((helper = (helper = helpers.keyValidationIcon || (depth0 != null ? depth0.keyValidationIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyValidationIcon","hash":{},"data":data}) : helper)))
    + "\"></i>\r\n                <p>"
    + ((stack1 = ((helper = (helper = helpers.keyValidationMsg || (depth0 != null ? depth0.keyValidationMsg : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyValidationMsg","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " <span class=\"subInfo\"><br>"
    + ((stack1 = ((helper = (helper = helpers.keyValidationSubMsg || (depth0 != null ? depth0.keyValidationSubMsg : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyValidationSubMsg","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span></p>\r\n                <input class=\"input\" type=\"text\" name=\""
    + alias4(((helper = (helper = helpers.keyValidationField || (depth0 != null ? depth0.keyValidationField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyValidationField","hash":{},"data":data}) : helper)))
    + "\" required id=\"keyValidation\"\r\n                     title=\""
    + ((stack1 = ((helper = (helper = helpers.keyValidationPopup || (depth0 != null ? depth0.keyValidationPopup : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"keyValidationPopup","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" required pattern=\"[A-Za-z0-9]{5,}\"> \r\n            </div>\r\n            <div id=\"msgTxt\">"
    + ((stack1 = ((helper = (helper = helpers.labelFooter || (depth0 != null ? depth0.labelFooter : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelFooter","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.deleteOldAccount : depth0),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\r\n    </div>  \r\n</div>";
},"useData":true});
templates['cosmosDbSelect'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "            "
    + ((stack1 = ((helper = (helper = helpers.introTextOne || (depth0 != null ? depth0.introTextOne : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"introTextOne","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " <br>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "            "
    + ((stack1 = ((helper = (helper = helpers.introTextTwo || (depth0 != null ? depth0.introTextTwo : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"introTextTwo","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " <a href=\""
    + container.escapeExpression(((helper = (helper = helpers.linkRegister || (depth0 != null ? depth0.linkRegister : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkRegister","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.linkRegisterText || (depth0 != null ? depth0.linkRegisterText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkRegisterText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</a>. <br>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "            <br>\r\n            <i class=\"fas fa-question-square\" focusable=\"false\"></i> "
    + ((stack1 = ((helper = (helper = helpers.introTextThree || (depth0 != null ? depth0.introTextThree : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"introTextThree","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " &quot;<a href=\""
    + container.escapeExpression(((helper = (helper = helpers.linkThree || (depth0 != null ? depth0.linkThree : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkThree","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers.linkTextThree || (depth0 != null ? depth0.linkTextThree : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"linkTextThree","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "&quot;</a>.\r\n";
},"7":function(container,depth0,helpers,partials,data) {
    return " heightlimited";
},"9":function(container,depth0,helpers,partials,data) {
    return "&nbsp;<span class=\"isexpand\"><i class=\"fas fa-expand isexpand\" focusable=\"false\"></i></span><span class=\"iscollapse display-no\"><i class=\"fas fa-compress iscollapse\" focusable=\"false\"></i></span>";
},"11":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <div tabindex=\"0\" class=\"oneradio\" label-sort=\""
    + ((stack1 = ((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" idx=\""
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\">\r\n                        <input tabindex=\"-1\" id=\"subs"
    + alias4(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data}) : helper)))
    + "\" name=\""
    + alias4(container.lambda((depths[1] != null ? depths[1].subscriptionField : depths[1]), depth0))
    + "\" required type=\"radio\" class=\"label-override\" value=\""
    + alias4(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data}) : helper)))
    + "\"  "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0["default"] : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "/>\r\n                        <label for=\"subs"
    + alias4(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data}) : helper)))
    + "\">\r\n                            <span class=\"radioYes "
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0["default"] : depth0),{"name":"unless","hash":{},"fn":container.program(14, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\"><i class=\"fa fa-check-circle\" focusable=\"false\"></i></span>\r\n                            <span class=\"radioNo "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0["default"] : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\"><i class=\"fal fa-circle\" focusable=\"false\"></i></span>\r\n                             "
    + ((stack1 = ((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n                        </label>\r\n                    </div>\r\n";
},"12":function(container,depth0,helpers,partials,data) {
    return " checked ";
},"14":function(container,depth0,helpers,partials,data) {
    return "display-no";
},"16":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "        <div id=\""
    + container.escapeExpression(((helper = (helper = helpers.resgroupField || (depth0 != null ? depth0.resgroupField : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"resgroupField","hash":{},"data":data}) : helper)))
    + "\" class=\"control-top display-no\">\r\n            <label> "
    + ((stack1 = container.lambda((depths[1] != null ? depths[1].allResGroupLabel : depths[1]), depth0)) != null ? stack1 : "")
    + " *<div class=\"curselect display-no\"></div></label>\r\n            <div class=\"radiobox"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.noSizeLimit : depth0),{"name":"unless","hash":{},"fn":container.program(7, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\"> \r\n                <div class=\"sortradio\">\r\n                      <input class=\"quicksearch display-no\" type=\"text\"> <i class=\"fas fa-search\" focusable=\"false\"></i>&nbsp;&nbsp;&nbsp;<i class=\"fas fa-undo sort-current\" focusable=\"false\"></i> <i class=\"fas fa-sort-alpha-down\" focusable=\"false\"></i> <i class=\"fas fa-sort-alpha-up\" focusable=\"false\"></i>\r\n                     "
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.noSizeLimit : depth0),{"name":"unless","hash":{},"fn":container.program(9, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                </div>\r\n                <div class=\"allradio\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.resgroupChoice : depth0),{"name":"each","hash":{},"fn":container.program(17, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\r\n            </div>\r\n        </div>\r\n";
},"17":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <div tabindex=\"0\" class=\"oneradio\" label-sort=\""
    + ((stack1 = ((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\" idx=\""
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\">\r\n                        <input tabindex=\"-1\" id=\"resGroup"
    + alias4(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data}) : helper)))
    + "\" name=\""
    + alias4(container.lambda((depths[1] != null ? depths[1].resgroupField : depths[1]), depth0))
    + "\" required type=\"radio\" class=\"label-override\" value=\""
    + alias4(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data}) : helper)))
    + "\"  "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0["default"] : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "/>\r\n                        <label for=\"resGroup"
    + alias4(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data}) : helper)))
    + "\">\r\n                            <span class=\"radioYes "
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0["default"] : depth0),{"name":"unless","hash":{},"fn":container.program(14, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\"><i class=\"fa fa-check-circle\" focusable=\"false\"></i></span>\r\n                            <span class=\"radioNo "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0["default"] : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\"><i class=\"fal fa-circle\" focusable=\"false\"></i></span>\r\n                            "
    + ((stack1 = ((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n                        </label>\r\n                    </div>\r\n";
},"19":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "        <div id=\""
    + container.escapeExpression(((helper = (helper = helpers.cosmosDBField || (depth0 != null ? depth0.cosmosDBField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"cosmosDBField","hash":{},"data":data}) : helper)))
    + "\" class=\"control-top display-no\">\r\n            <label> "
    + ((stack1 = container.lambda((depths[1] != null ? depths[1].allCosmosDBLabel : depths[1]), depth0)) != null ? stack1 : "")
    + "</label>\r\n            <div class=\"info\">"
    + ((stack1 = ((helper = (helper = helpers.cosmosDBLabel || (depth0 != null ? depth0.cosmosDBLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"cosmosDBLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n        </div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return " <div>\r\n    <div id=\"headerSection\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.introTextOne : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.introTextTwo : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.introTextThree : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n    <div id=\"inputSection\">\r\n\r\n        <!-- Azure Subscriptions / Cosmos DB Account-->\r\n        <div id=\"subscriptions\" srcField=\""
    + container.escapeExpression(((helper = (helper = helpers.subscriptionField || (depth0 != null ? depth0.subscriptionField : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"subscriptionField","hash":{},"data":data}) : helper)))
    + "\" class=\"control-top\">\r\n            <label> "
    + ((stack1 = ((helper = (helper = helpers.subscriptionLabel || (depth0 != null ? depth0.subscriptionLabel : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"subscriptionLabel","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + " *<div class=\"curselect display-no\"></div></label>\r\n                <div class=\"radiobox"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.noSizeLimit : depth0),{"name":"unless","hash":{},"fn":container.program(7, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\"> \r\n                <div class=\"sortradio\">\r\n                      <input class=\"quicksearch display-no\" type=\"text\"> <i class=\"fas fa-search\" focusable=\"false\"></i>&nbsp;&nbsp;&nbsp;<i class=\"fas fa-undo sort-current\" focusable=\"false\"></i> <i class=\"fas fa-sort-alpha-down\" focusable=\"false\"></i> <i class=\"fas fa-sort-alpha-up\" focusable=\"false\"></i>\r\n                     "
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.noSizeLimit : depth0),{"name":"unless","hash":{},"fn":container.program(9, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                </div>\r\n                <div class=\"radiobox\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.subscriptionChoice : depth0),{"name":"each","hash":{},"fn":container.program(11, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\r\n            </div>\r\n        </div>\r\n\r\n\r\n        <!-- Azure Resource Groups -->\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.allResGroupChoice : depth0),{"name":"each","hash":{},"fn":container.program(16, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n        <!-- Azure Resource Groups -->\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.allCosmosDBChoice : depth0),{"name":"each","hash":{},"fn":container.program(19, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n    </div>  \r\n</div>";
},"useData":true,"useDepths":true});
})();