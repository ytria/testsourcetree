(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Button'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<div class=\"control\">\r\n		<input class=\"button "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"post",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" type=\""
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.type : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "\" name=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "\" \r\n		"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.attribute : depth0),"cancel",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		>\r\n	</div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return " post ";
},"4":function(container,depth0,helpers,partials,data) {
    var helper;

  return container.escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"type","hash":{},"data":data}) : helper)));
},"6":function(container,depth0,helpers,partials,data) {
    return "submit";
},"8":function(container,depth0,helpers,partials,data) {
    return " formnovalidate ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"field has-addons row-btns\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n</div>";
},"useData":true});
templates['Main'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <form id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" method=\""
    + alias4(((helper = (helper = helpers.method || (depth0 != null ? depth0.method : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"method","hash":{},"data":data}) : helper)))
    + "\" action=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\" ondragstart=\"return false;\">\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers.unless.call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"unless","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    return "                    "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"Button",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    return "            "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.main : depth0),{"name":"with","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n    <div id=\"containerFields\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n</form>\r\n";
},"useData":true});
templates['licensePostUserCountChange'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                <div class=\"next\">\r\n                    <div class=\"header\">"
    + alias4(((helper = (helper = helpers.NextBillingCustomerTitle || (depth0 != null ? depth0.NextBillingCustomerTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"NextBillingCustomerTitle","hash":{},"data":data}) : helper)))
    + " ("
    + ((stack1 = ((helper = (helper = helpers.next_billing_date || (depth0 != null ? depth0.next_billing_date : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"next_billing_date","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + ")</div>\r\n                    <div class=\"body columns is-mobile\">\r\n                        <div class=\"net column\">\r\n                            <label>"
    + alias4(((helper = (helper = helpers.labelNet || (depth0 != null ? depth0.labelNet : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelNet","hash":{},"data":data}) : helper)))
    + "</label>\r\n                            <div>"
    + ((stack1 = ((helper = (helper = helpers.NextBillingCustomerNetPrice || (depth0 != null ? depth0.NextBillingCustomerNetPrice : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"NextBillingCustomerNetPrice","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                        </div>\r\n                        <div class=\"vat column\">\r\n                            <label>"
    + alias4(((helper = (helper = helpers.labelVat || (depth0 != null ? depth0.labelVat : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelVat","hash":{},"data":data}) : helper)))
    + "</label>\r\n                            <div>"
    + ((stack1 = ((helper = (helper = helpers.NextBillingCustomerVatPrice || (depth0 != null ? depth0.NextBillingCustomerVatPrice : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"NextBillingCustomerVatPrice","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                        </div>\r\n                        <div class=\"total column\">\r\n                            <label>"
    + alias4(((helper = (helper = helpers.labelTotal || (depth0 != null ? depth0.labelTotal : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelTotal","hash":{},"data":data}) : helper)))
    + "</label>\r\n                            <div><span class=\"currency\">"
    + ((stack1 = ((helper = (helper = helpers.PriceCurrencyId || (depth0 != null ? depth0.PriceCurrencyId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"PriceCurrencyId","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span> "
    + ((stack1 = ((helper = (helper = helpers.NextBillingCustomerGrossPrice || (depth0 != null ? depth0.NextBillingCustomerGrossPrice : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"NextBillingCustomerGrossPrice","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                <div class=\"now\">\r\n                    <div class=\"header\">"
    + alias4(((helper = (helper = helpers.AlignmentCustomerTitle || (depth0 != null ? depth0.AlignmentCustomerTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AlignmentCustomerTitle","hash":{},"data":data}) : helper)))
    + "</div>\r\n                    <div class=\"body columns is-mobile\">\r\n                        <div class=\"net column\">\r\n                            <label>"
    + alias4(((helper = (helper = helpers.labelNet || (depth0 != null ? depth0.labelNet : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelNet","hash":{},"data":data}) : helper)))
    + "</label>\r\n                            <div>"
    + ((stack1 = ((helper = (helper = helpers.AlignmentCustomerNetPrice || (depth0 != null ? depth0.AlignmentCustomerNetPrice : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AlignmentCustomerNetPrice","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                        </div>\r\n                        <div class=\"vat column\">\r\n                            <label>"
    + alias4(((helper = (helper = helpers.labelVat || (depth0 != null ? depth0.labelVat : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelVat","hash":{},"data":data}) : helper)))
    + "</label>\r\n                            <div>"
    + ((stack1 = ((helper = (helper = helpers.AlignmentCustomerVatPrice || (depth0 != null ? depth0.AlignmentCustomerVatPrice : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AlignmentCustomerVatPrice","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                        </div>\r\n                        <div class=\"total column\">\r\n                            <label>"
    + alias4(((helper = (helper = helpers.labelTotal || (depth0 != null ? depth0.labelTotal : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelTotal","hash":{},"data":data}) : helper)))
    + "</label>\r\n                            <div><span class=\"currency\">"
    + ((stack1 = ((helper = (helper = helpers.PriceCurrencyId || (depth0 != null ? depth0.PriceCurrencyId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"PriceCurrencyId","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span> "
    + ((stack1 = ((helper = (helper = helpers.AlignmentCustomerGrossPrice || (depth0 != null ? depth0.AlignmentCustomerGrossPrice : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AlignmentCustomerGrossPrice","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "        <div id=\"header\">\r\n            <h2>"
    + ((stack1 = ((helper = (helper = helpers.labelIntro || (depth0 != null ? depth0.labelIntro : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelIntro","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</h2>\r\n            <p>"
    + ((stack1 = ((helper = (helper = helpers.labelText || (depth0 != null ? depth0.labelText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelText","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</p>\r\n        </div>\r\n\r\n        <div class=\"validateWrapper\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.NextBillingCustomerGrossPrice : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.AlignmentCustomerGrossPrice : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            <div id=\"msgTxt\">"
    + ((stack1 = ((helper = (helper = helpers.labelFooter || (depth0 != null ? depth0.labelFooter : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelFooter","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n\r\n        </div>\r\n\r\n";
},"useData":true});
})();