var isDebug = !isInApp; // always Debug when in IE - isInApp is set in hbshandler.js
var ready = false;

// The following are provided by DATA-JS
// var allJobs = [];
// var allCategories = [];
// var allScheduled = [];
// var allSelectedAJL = [];
// var maxSelectedAJL = 0;

// These are set here so we can access them in IE console
var catalog_data = '';
var box_data = '';
function initBoxData() {
    box_data = {'favorite':'', 'custom':'', 'lastused':'', 'newjobs':'', 'ajl':'', 'scheduled':'', 'sandbox':'' };
}
initBoxData();

var searchID = ['', 'FAV-', 'CUST-', 'LAST-', 'NEW-', 'AJL-', 'SAND-']; // div #id modifier for selecting Jobs
var box_searchID = ['main', 'favorite', 'custom', 'lastused', 'newjobs', 'ajl', 'sandbox']; 
var ajlTgtCode = 'ajl';
var schedTgtCode = 'scheduled';
var mainTgtCode = 'main';
var NoFamilySet = "ZNoFamilySetZ";
var NoActionSet = "ZNoActionSetZ";
var allBoxID = [];
var allBoxBaseTitle = [];

// NOTE: We will add a '_' to each ID, unless it tries to use id as a number, and nothing get added (too big?)
var allCategoriesLabelByID = [];
var allKeyJobsByID = [];
var allKeySchedJobsByID = [];
var allSchedByJobID = [];

// constant for AJL
var AJL_NA = 0;
var AJL_IN = 1;
var AJL_OUT = -1;
// working array for jobs
var curSelectionAJL = [];
function initSelectedAJL() {
    // we use slice() else the array will be copied as a reference...
    curSelectionAJL = allSelectedAJL.slice();
}

// The DIV content
var array_family = [];
var array_action = [];
var array_action_data = [];
var array_isNew = [];
var array_isLastUse = [];
var array_sandbox = [];

// BEWARE! These are now Arrays
var searchCat = [];
var searchAct = [];
var searchFam = [];

// ************************************************************************
// Generators for the Job elements in the Catalog
function getSafeClass(arr) {
    var ret = arr.toString().replace(/ /g, '_');
    ret = ret.toString().replace(/,/g, ' ');
    return ret;
}

function createTooltip(curElt) {
    var data = '<div class="tooltipTile">';
    var str = curElt.label;
    if (str != '') {
        data += '<div class="info_title">'+ str +'</div>';
    }
    str = curElt.description;
    if (str != '') {
        data += '<div class="info_desc">'+ str +'</div>';
    }
    var catLabel = [];
    $.each(curElt.categories, function(key, val){
        catLabel.push(allCategoriesLabelByID['_'+val]);
    }) 
    str = catLabel.join(', ');
    if (str != '') {
        data += '<div class="info_cat">'+ str +'</div>';
    }
    str = curElt.keywords.replace(/,/g,', ');
    if (str != '') {
        data += '<div class="info_keyword">'+ str +'</div>';
    }

    data += '</div>';
    return data;
}

function createSubMenu(curElt) {
    var isEmpty = true;
    var isScheduled = ( 'undefined' !== typeof curElt.idJob );
    var data = '<div class="threedotcontent">';
        data += '<ul>';
        if (curElt.isInAJL == AJL_OUT) {
            if ( !isScheduled ) {
                if (g_AJL_labelAdd != '') {
                    isEmpty = false;
                    var isFull = (curSelectionAJL.length >= maxSelectedAJL);
                    data += '<li class="submenu mgtAJL addJob'+(isFull?' notactive':'')+(curElt.isEditedAJL?' display-no':'')+'"'
                    data += ' jobID="'+curElt.id+'" tgtID="'+curElt.ajlid+'">'+g_AJL_labelAdd+'</li>';
                }
                if (g_AJL_labelRemowe != '') {
                    isEmpty = false;
                    data += '<li class="submenu mgtAJL removeJob '+(curElt.isEditedAJL?'':' display-no')+'"'
                    data += ' jobID="'+curElt.id+'" tgtID="'+curElt.ajlid+'">'+g_AJL_labelRemowe+'</li>';
                }
            }
        } else {
            var theList = (isScheduled)?g_allSchedSubMenuItems:g_allSubMenuItems;
            $.each(theList, function(){
                if (this.tgt != '') {
                    if (curElt[this.tgt] != this.val) { return; }
                }
                if (this.url) {
                    isEmpty = false;
                    var url = (this.url).replace('[url]', curElt.link).replace('[id]', curElt.id);
                    data += '<a href="'+ url +'">'
                    data += '<li class="submenu">'+this.label+'</li>';
                    data += '</a>';
                } else {
                    isEmpty = false;
                    data += '<li class="submenu notactive">'+this.label+'</li>';
                }
            });
        }
        if (curElt.isInAJL == AJL_IN && g_AJL_canRemoveJob  && !isScheduled) {
            if (g_AJL_labelRemowe != '') {
                isEmpty = false;
                data += '<li class="submenu mgtAJL removeJob'+(curElt.isEditedAJL?' display-no':'')+'"'
                data += 'jobID="'+curElt.id+'" tgtID="'+curElt.ajlid+'">'+g_AJL_labelRemowe+'</li>';
            }
            if (g_AJL_labelAdd != '') {
                isEmpty = false;
                var isFull = (curSelectionAJL.length >= maxSelectedAJL);
                data += '<li class="submenu mgtAJL addJob'+(isFull?' notactive':'')+(curElt.isEditedAJL?'':' display-no')+'"'
                data += 'jobID="'+curElt.id+'" tgtID="'+curElt.ajlid+'">'+g_AJL_labelAdd+'</li>';
            }
        }
        data += '</ul>';
    data += '</div>';
    if (isEmpty) {
        data = '';
    }
    return data;
}
function createBigBox(curElt, allCatClass) {
    var data = '<div id="'+curElt.id+'" class="list filterDiv ' + ((curElt.isInAJL == AJL_OUT)? 'notInAJL ': '') + curElt.family + ' ' + allCatClass +'" jobid="'+curElt.id+'">';
    // if( curElt.isfavorite == true ) {
    //     data += '<i class="fad fa-star full"></i>';
    // } else {
    //     data += '<i class="fad fa-star empty"></i>';
    // }
    if(curElt.source == 'custom') {
        data += '<i class="fas fa-copyright"></i>';
    }
    // if( curElt.hasselection == true ) {
    //     data += '<i class="fas fa-bookmark"></i>';
    // }

    data += '<i class="far fa-ellipsis-v'+((curElt.subMenu == '')?' isEmpty':'')+'"></i>';
    data += curElt.subMenu;
    data += createTooltip(curElt);

    // List Tile
    data += '<div class="content_list">';
        if (curElt.isInAJL != AJL_OUT) {
            var url = g_tileAction.replace('[url]', curElt.link).replace('[id]', curElt.id);
            data += '<a href="'+ url +'">';
        }
        data += '<div class="content_list_main">';
            data += '<div class="svg">';
                data += '<i class="'+((curElt.icon!=NoActionSet)?curElt.icon:'far fa-question-square')+'"></i>';
                data += '<i class="fas fa-plus addJob display-no"></i>';
                data += '<i class="fas fa-times removeJob display-no"></i>';
            data += '</div>';
            data += '<div class="contentlist">';
                data += '<div class="title_list">'+ curElt.label +'</div>';
                data += '<div class="description">'+ curElt.description +'</div>';
            data += '</div>';
        data += '</div>';
        if (curElt.isInAJL != AJL_OUT) { data += '</a>'; }
    data += '</div>';
    
    data += '</div>';
    return data;
}
function createMiniBox(curElt, finalID, jobID, allCatClass, sortInfo, introText) {
    var isScheduled = ( 'undefined' != typeof curElt.idJob );

    var data = '<div class="fav filterDiv'+((curElt.isInAJL == AJL_OUT)? ' notInAJL': '')+(isScheduled?' scheduledJob':'');
    data += ' '+curElt.family+' '+allCatClass+'" id="'+finalID+'" jobid="'+jobID+'"';
    if ('undefined' !== typeof sortInfo) {
        if (sortInfo != '') {
            data += ' sortInfo="'+sortInfo+'"';
        }
    }
    data += '>';
    if (curElt.isInAJL != AJL_OUT) { 
        var url = g_tileAction.replace('[url]', curElt.link).replace('[id]', curElt.id);
        data +='<a href="'+ url +'">';
    }
    data += '<div class="content_fav favoris">';
        data += '<div class="svg">';
            data += '<i class="'+((curElt.icon!=NoActionSet)?curElt.icon:'far fa-question-square')+'"></i>';
            data += '<i class="fas fa-plus addJob display-no"></i>';
            data += '<i class="fas fa-times removeJob display-no"></i>';
        data += '</div>';
        data += '<div class="fav_content">';
            if ('undefined' !== typeof introText) {
                    data += '<div>'+introText+'</div>';
                    data += '<div class="fav_corps">'+ curElt.label +'</div>';
                } else {
                    data += '<div class="fav_titleOnly">'+ curElt.label +'</div>';
            }
        data += '</div>';
    data += '</div>';
    if (curElt.isInAJL != AJL_OUT) { data += '</a>'; }
    data += '</div>';
    return data;
}

function updateOneBoxTitle(boxTgtCode) {
    if (boxTgtCode == mainTgtCode) {
        // Nothing to do here....
    } else if (boxTgtCode == ajlTgtCode) {
        updateAJLBoxTitle();
    } else {
        if ('undefined' != typeof allBoxID[boxTgtCode]) {
            var theDiv = $('#'+allBoxID[boxTgtCode]).closest('.oneBox').find('.title');
            var str = ''
            var count = $('#'+allBoxID[boxTgtCode]).find('.fav').length;
            if (count > 0) {
                str = g_title_currentCount.replace('%C%', count);
            }
            var finalStr = allBoxBaseTitle[boxTgtCode].replace('%I%', str);
            console.log(finalStr);
            theDiv.html(finalStr);
        }
    }
}
function updateAJLBoxTitle() {
    // AJL specific title
    if (allBoxID[ajlTgtCode] != '') {
        var theDiv = $('#'+allBoxID[ajlTgtCode]).closest('.oneBox').find('.title');
        var str = ''
        if( maxSelectedAJL > 0) {
            str = g_AJL_currentState.replace('%T%',maxSelectedAJL);
            str = str.replace('%C%',allSelectedAJL.length );
      
            var subStr = '';
            if ($('#AJL_applyChange').hasClass('display-no')) {
                // no change currently
            } else {
                subStr = g_AJL_newState.replace('%N%',$('#'+allBoxID[ajlTgtCode]).find('.svg .addJob').not('.display-no').length);
                subStr = subStr.replace('%D%',$('#'+allBoxID[ajlTgtCode]).find('.svg .removeJob').not('.display-no').length);
            }
            str = str.replace('%M%', subStr);
        }
        var finalStr = allBoxBaseTitle[ajlTgtCode].replace('%I%', str);
        console.log(finalStr);
        theDiv.html(finalStr);
    }
}
function updateAllBoxTitle() {
    $.each(box_searchID, function(key, tgtCode){
        if (isDebug) console.log('>> update Box Title for '+tgtCode);
        updateOneBoxTitle(tgtCode);
    })
    // Special Scheduled (not part of searchID)
    updateOneBoxTitle(schedTgtCode);
}
function globalResetAJL() {
    // global reset of the AJL situation
    $('#AJL_applyChange').addClass('display-no');
    $('#AJL_finalValue').val('');
    if (curSelectionAJL.length >= maxSelectedAJL) {
        // this is a no more situation...
        $('.mgtAJL.addJob').addClass('notactive');
    } else {
        // we have some loose again...
        $('.mgtAJL.addJob').removeClass('notactive');
    }
    // this will un-edit any jobs
    errorContext = 'update Jobs if needed';
    $.each(allJobs, function(key,jobData) {
        var curAJLState = jobData.isInAJL;
        var curEditState = jobData.isEditedAJL;
        // copy the jobData object so we don't update the current Job
        var newJobData = Object.create(jobData);
        initJobDefault(newJobData);
        // note: newJobData.isEditedAJL will always be false here...
        if ( curAJLState == newJobData.isInAJL && curEditState == newJobData.isEditedAJL ) {
            // nothing to do here...
        } else {
            // The Job status change... update it
            errorContext = 'NEED UPDATE - Job changed '+newJobData.id;
            console_log_and_trace(errorContext);
            processOneJob(newJobData);
        }
    })
    updateAJLBoxTitle();
}

function initJobDefault(oneJob) {
    // Unset Attributes
    var fontawesome = /^fa[srld]? fa-[a-z-0-9]*$/i;
    if ('undefined' == typeof oneJob.icon) {
            oneJob.icon = NoActionSet;
    } else if ('' == oneJob.icon) { 
        oneJob.icon = NoActionSet;
    } else if ( !fontawesome.test(oneJob.icon.toLowerCase()) ) {
        oneJob.icon = NoActionSet;
    }
    if ('undefined' == typeof oneJob.source) {
            oneJob.source = 'custom';
    } else if ('' == oneJob.source) {
        oneJob.source = 'custom';
    }
    if ('undefined' == typeof oneJob.family) {
        oneJob.family = NoFamilySet;
    } else if ('' == oneJob.family) {
        oneJob.family = NoFamilySet;
    }
    // AJL state of the current Job
    oneJob.isInAJL = AJL_NA;
    if( maxSelectedAJL > 0 && oneJob.source != 'custom') {
        if ( allSelectedAJL.indexOf(oneJob.ajlid) > -1 ) {
            oneJob.isInAJL = AJL_IN;
        } else {
            oneJob.isInAJL = AJL_OUT;
        }
        oneJob.isEditedAJL = false;
    }
}

// ************************************************************************
// Update for a given Job (external call)
function processOneJob(jobData) {
    initBoxData();
    var idxJob = allKeyJobsByID['_'+jobData.id];

    // we need to set the AJL state of what has been provided
    errorContext = 'Get AJL State';
    initJobDefault(jobData);
    console_log_and_trace(errorContext+' :: '+jobData.isInAJL);
    // and we need the submenu Contextual Menu
    errorContext = 'Create SubMenu';
    jobData.subMenu = createSubMenu(jobData);

    var isNewJob = false;
    if ('undefined' == typeof idxJob) {
        // This Job doesn't exist yet (this is a new one)
        errorContext = 'CREATE - New Job '+jobData.id;
        console_log_and_trace(errorContext);
        isNewJob = true;
        idxJob = allJobs.push(jobData)-1;
        allKeyJobsByID['_'+jobData.id] = idxJob;
    } else {
        // This Job is already present -- to update?
        if ( JSON.stringify(allJobs[idxJob]) == JSON.stringify(jobData) ) {
            errorContext = 'SKIP - Existing Identical Job '+jobData.id;
            console_log_and_trace(errorContext);
            return true;
        }
        errorContext = 'UPDATE - Existing Job '+jobData.id;
        console_log_and_trace(errorContext);
        allJobs[idxJob] = jobData;
    }
    // --- THE NEW/TO UPDATE JOB ----
    errorContext = 'Get Job Object';
    var curElt = allJobs[idxJob];

    // Get all Job's categories
    errorContext = 'Process Categories';
    var curCategoriesClass = getSafeClass(curElt.categories);
    // console_log_and_trace(errorContext+' :: '+curCategoriesClass);

    // Big Box -- Catalog (full list)
    errorContext = 'Create BigBox';
    box_data.main = createBigBox(curElt, curCategoriesClass )

    // Get Jobs tagged as Is New
    if( curElt.isnew == true ) {
        errorContext = 'Create MiniBox NEW';
        box_data.newjobs = createMiniBox(curElt, 'NEW-'+curElt.id, curElt.id, curCategoriesClass, curElt.created.on )
        // console_log_and_trace(errorContext);
    }
    // Mini Box - Favorite
    if (curElt.isfavorite == true) {
        errorContext = 'Create MiniBox FAV';
        box_data.favorite = createMiniBox(curElt, 'FAV-'+curElt.id, curElt.id, curCategoriesClass )
        // console_log_and_trace(errorContext);
    }
    // Mini Box - Custom
    if (curElt.source == 'custom') {
        errorContext = 'Create MiniBox CUST';
        box_data.custom = createMiniBox(curElt, 'CUST-'+curElt.id, curElt.id, curCategoriesClass)
        // console_log_and_trace(errorContext);
    }
    // Mini Box - Last Use
    if (curElt.lastuse) {
        errorContext = 'Create MiniBox LAST';
        box_data.lastused = createMiniBox(curElt, 'LAST-'+curElt.id, curElt.id, curCategoriesClass, curElt.lastuse, moment(curElt.lastuse).format('lll') )
        // console_log_and_trace(errorContext);
    }
    // If we have any AJL license set - build the list
    if (curElt.isInAJL == AJL_IN) {
        errorContext = 'Create MiniBox AJL';
        box_data.ajl = createMiniBox(curElt, 'AJL-'+curElt.id, curElt.id, curCategoriesClass )
        // console_log_and_trace(errorContext);
    }
    // Mini Box - Sandbox
    if (curElt.isSandbox) {
        errorContext = 'Create MiniBox SAND';
        box_data.sandbox = createMiniBox(curElt, 'SAND-'+curElt.id, curElt.id, curCategoriesClass)
        // console_log_and_trace(errorContext);
    }

    if (isNewJob) {
        errorContext = 'Append in BigBox';
        // console_log_and_trace(errorContext);
        $("#catalogJobs").append(box_data['main']);
        $('.oneJobBox').each(function() {
            errorContext = 'Append in MiniBox '+$(this).attr('tgt');
            $(this).append(box_data[$(this).attr('tgt')]);
        })
    } else {
        $.each(searchID, function(key, id) {
            var boxTgtCode = box_searchID[key];
            errorContext = 'Update in Box '+boxTgtCode;
            // console_log_and_trace(errorContext);
            var html = box_data[boxTgtCode];
            if (html != '') {
                // console_log_and_trace('#'+id+curElt.id+' :: UPDATED');
                if ($('#'+id+curElt.id).length > 0) {
                    $('#'+id+curElt.id).replaceWith(html);
                } else {
                    // this does not exist (yet) -- minibox case
                    errorContext = 'Append in MiniBox '+$(this).attr('tgt');
                    $('#'+allBoxID[boxTgtCode]).append(box_data[boxTgtCode]);
                }
            } else {
                // console_log_and_trace('#'+id+curElt.id+' :: REMOVED');
                $('#'+id+curElt.id).remove();
            }
        })
        // Update any Scheduled (if any) of the Job
        errorContext = 'Update in Scheduled Box ';
        if ( 'undefined' != typeof allSchedByJobID['_'+curElt.id] ) {
            $.each(allSchedByJobID['_'+curElt.id], function(key, id) {
                var theSched = allScheduled[allKeySchedJobsByID['_'+id]];
                console_log_and_trace(theSched);
                theSched.family = curElt.family;
                theSched.icon = curElt.icon;
                theSched.label = curElt.label;
                theSched.isInAJL = curElt.isInAJL;
                var html = createMiniBox(theSched, theSched.id, curElt.id, curCategoriesClass, '', '<i class="far fa-clock"></i>'+theSched.frequency )
                $('#'+theSched.id).replaceWith(html);
            })
        }
    }
}
function processOneSched(schedData) {
    var idxSched = allKeySchedJobsByID['_'+schedData.id];
    var theJobID = schedData.idJob;
    var idxJob = allKeyJobsByID['_'+theJobID];
    if ('undefined' == typeof idxJob) {
        // we need to be sure a Job exist first, else this is pointless
        errorContext = 'ERROR - NO Existing Job for Scheduled '+schedData.id;
        console_log_and_trace(errorContext);
    } else {
        var theJob = allJobs[idxJob];
        // Init the info from the Job
        schedData.family = theJob.family;
        schedData.icon = theJob.icon;
        schedData.label = theJob.label;
        schedData.isInAJL = theJob.isInAJL;

        var isNewSched = false;
        if ('undefined' == typeof idxSched) {
            errorContext = 'CREATE - New Scheduled '+schedData.id;
            console_log_and_trace(errorContext);
            isNewSched = true;
            idxSched = allScheduled.push(schedData)-1;
            allKeySchedJobsByID['_'+schedData.id] = idxSched;
        } else {
            // This Scheduled Job is already present -- to update?
            if ( JSON.stringify(allScheduled[idxSched]) == JSON.stringify(schedData) ) {
                errorContext = 'SKIP - Existing Identical Scheduled Job '+schedData.id;
                console_log_and_trace(errorContext);
                return true;
            }
            errorContext = 'UPDATE - Existing Scheduled Job '+schedData.id;
            console_log_and_trace(errorContext);
            allScheduled[idxSched] = schedData;
        }

        errorContext = 'Create MiniBox';
        var theSched = allScheduled[idxSched];
        var html = createMiniBox(theSched, theSched.id, theJob.id, getSafeClass(theJob.categories), '', '<i class="far fa-clock"></i>'+theSched.frequency );

        if (isNewSched) {
            $('.oneJobBox').each(function() {
                if ($(this).attr('tgt') == 'scheduled') {
                    errorContext = 'Append in Box '+$(this).attr('tgt');
                    $(this).append(html);
                }
            })
            if ( 'undefined' == typeof allSchedByJobID['_'+theJob.id] ) {
                // We must initialize the Array before using it.
                allSchedByJobID['_'+theJob.id] = []
            }
            allSchedByJobID['_'+theJob.id].push(''+theSched.id); // force a string?
        } else {
            errorContext = 'Update in Box scheduled';
            $('#'+theSched.id).replaceWith(html);
        }    
    }
}

// ************************************************************************
// SEARCH AND FILTERS
// we use ID instead of Class to select items because it is way faster !!
var searchKey = '';
var searchAtt = ['label', 'description', 'keywords']; // attributs of Job to use for Key Search
function applySearch() {
    $.each(allJobs, function(key, job) {
        var isIn = true;
        if (searchKey != '') {
            isIn &= searchKey.split(' ').every(function(elt) {
                return searchAtt.some(function(att){
                    var v = job[att];
                    if (Array.isArray(v)) { v = v.join() }
                    return v.toLowerCase().indexOf(elt) > -1
                })
            })
        }
        if (isIn && ( searchFam.length > 0 || job.family == NoFamilySet )) {
            isIn &= ( searchFam.indexOf(job.family) > -1 || job.family == NoFamilySet );
        } else {
            isIn = false;
        }
        if (isIn && ( searchAct.length > 0  || job.icon == NoActionSet )) {
            isIn &= ( searchAct.indexOf(job.icon) > -1 || job.icon == NoActionSet );
        } else {
            isIn = false;
        }
        if (isIn && searchCat.length > 0) {
            // Note: this checks TWO arrays - we want an OR
            var isInCat = false;
            $.each(searchCat, function(idx, val) {
                isInCat |= ( job.categories.indexOf(val) > -1 );
            });
            isIn &= isInCat;
        }
        $.each(searchID, function(key, id) {
            if (isIn) {
                $('#'+id+job.id).removeClass('keyFilterOut');
            } else {
                $('#'+id+job.id).addClass('keyFilterOut');
            }
        })
        // handle any related Scheduled Job
        if ( 'undefined' != typeof allSchedByJobID['_'+job.id] ) {
            $.each(allSchedByJobID['_'+job.id], function(key, id) {
                if (isIn) {
                    $('#'+id).removeClass('keyFilterOut');
                } else {
                    $('#'+id).addClass('keyFilterOut');
                }
            })
        }
})
}
// the search handler
function doTheSearchByKey() {
    var searchforOriginal = $('#search_string').val();
    // clean any special characters
    searchKey = searchforOriginal.replace(/[|&;.-/\\$%@''"<>()+,]/g, "");
    searchKey = searchKey.toLowerCase();
    applySearch();
    $('#search_string').focus();
}
// Search on Category / family / action
function filterGroup(type, value) {
    console.log('## search ## filterGroup: '+type+' -- '+value)
    if (type =='cleanCats') {
        $('#'+value).next('ul').find('li').each(function(){
            var idx = searchCat.indexOf($(this).attr('id'));
            if (idx >= 0) { searchCat.splice(idx,1); }
            $(this).removeClass("active");
        });
    } else if (type =='allCats') {
        $('#'+value).next('ul').find('li').each(function(){
            var idx = searchCat.indexOf($(this).attr('id'));
            if (idx < 0) { searchCat.push($(this).attr('id')); }
            $(this).addClass("active");
        })
    } else if (type =='cat') {
        var idx = searchCat.indexOf(value);
        if (idx < 0) {
            searchCat.push(value);
        } else {
            searchCat.splice(idx,1);
        }
        if ( $('#'+value).hasClass('isChildren') ) {
            if ( $('#'+value).parent().find('li.active').length == 0 ) {
                $('#'+value).parent().prev().removeClass("active");
            }
        }
    } else if (type =='fam') {
        var idx = searchFam.indexOf(value);
        if (idx < 0) {
            searchFam.push(value);
        } else {
            searchFam.splice(idx,1);
        }
    } else if (type =='act') {
        var idx = searchAct.indexOf(value);
        if (idx < 0) {
            searchAct.push(value);
        } else {
            searchAct.splice(idx,1);
        }
    }
    console.log('## search ## '+searchFam+'('+searchFam.length+') ## '+searchAct+'('+searchAct.length+') ## '+searchCat+'('+searchCat.length+')')
    applySearch();
}

// ************************************************************************
// FULL Catalog Initialization
function initializeCatalog() {

    // Building array to get category's label from ID
    $.each(allCategories, function(){
        allCategoriesLabelByID['_'+this.id] = this.label;
        $.each(this.children, function(){
            allCategoriesLabelByID['_'+this.id] = this.label;
        });
    });

    initSelectedAJL();
    initializeSubMenuItems();

    // BUILD ALL THE BOXES (list and fav)
    $.each(allJobs, function(key,curElt) {
        // Get all Job's categories
		var curCategoriesClass = getSafeClass(curElt.categories);
        
        // AJL state of the current Job
        initJobDefault(curElt);

        // Build all Jobs Key by their ID
        allKeyJobsByID['_'+curElt.id] = key;

        // Get Jobs tagged as Is New
        if( curElt.isnew == true ) {
            array_isNew.push(curElt);
        }

        // Build the list of all the UNIQUE available families
        if (curElt.family != NoFamilySet) {
            if (array_family.indexOf(curElt.family) < 0)
                array_family.push(curElt.family);
        }

        // Build the list of all the UNIQUE available actions
        if (curElt.icon != NoActionSet) {
            if (array_action.indexOf(curElt.icon) < 0) {
                array_action.push(curElt.icon);
                if (curElt.source == 'custom') {
                    // we don't want to mess up label from a custom
                    array_action_data[curElt.icon] = '';
                } else {
                    if ('undefined' != typeof curElt.action) {
                        array_action_data[curElt.icon] = curElt.action;
                    } else {
                        array_action_data[curElt.icon] = '';
                    }
                }
            } else {
                // if action not set yet, get it if not custom
                if ( array_action_data[curElt.icon] == '' ) {
                    if (curElt.source != 'custom') {
                        if ('undefined' != typeof curElt.action) {
                            array_action_data[curElt.icon] = curElt.action;
                        }
                    }
                }
            }
        }

        // Contextual Menu
        curElt.subMenu = createSubMenu(curElt);
        
        // Big Box -- Catalog (full list)
        catalog_data += createBigBox(curElt, curCategoriesClass )
            
        // Mini Box - Favorite
        if (curElt.isfavorite == true) {
            box_data.favorite += createMiniBox(curElt, 'FAV-'+curElt.id, curElt.id, curCategoriesClass )
        }
        // Mini Box - Custom
        if (curElt.source == 'custom') {
            box_data.custom += createMiniBox(curElt, 'CUST-'+curElt.id, curElt.id, curCategoriesClass)
        }
        // Mini Box - Last Use
        if (curElt.lastuse) {
            array_isLastUse.push(curElt);
        }
        // If we have any AJL license set - build the list
        if (curElt.isInAJL == AJL_IN) {
            box_data.ajl += createMiniBox(curElt, 'AJL-'+curElt.id, curElt.id, curCategoriesClass )
        }
        // Sandbox Jobs
        if (curElt.isSandbox) {
            box_data.sandbox += createMiniBox(curElt, 'SAND-'+curElt.id, curElt.id, curCategoriesClass);
        }
    });

    // Last Use Jobs - array has been created in former loop
    array_isLastUse.sort(function(a, b) {
        var c = new Date(a.lastuse);
        var d = new Date(b.lastuse);
        return d-c;
    });
    // Mini Box - Last USe
    for(var i = 0; i < array_isLastUse.length; i++) {
        var curElt = array_isLastUse[i];
        box_data.lastused += createMiniBox(curElt, 'LAST-'+curElt.id, curElt.id, getSafeClass(curElt.categories), curElt.lastuse, moment(curElt.lastuse).format('lll') )
    }

    // isNew Jobs - array has been created in former Loop
    // Sort by date of creation
    array_isNew.sort(function(a, b) {
        var c = new Date(a.created.on);
        var d = new Date(b.created.on);
        return d-c;
    });
    // Mini Box - New
    for(var i = 0; i < array_isNew.length; i++) {
        var curElt = array_isNew[i];
        box_data.newjobs += createMiniBox(curElt, 'NEW-'+curElt.id, curElt.id, getSafeClass(curElt.categories), curElt.created.on)
    }

    // Mini Box - Schedule
    $.each(allScheduled, function(key, oneElt) {
        // add the id to the map to  be able to find it
        allKeySchedJobsByID['_'+oneElt.id] = key;
        // Get the corresponding Jobs
        var theJob = allJobs[allKeyJobsByID['_'+oneElt.idJob]];
        if ( 'undefined' == typeof allSchedByJobID['_'+theJob.id] ) {
            // We must initialize the Array before using it.
            allSchedByJobID['_'+theJob.id] = []
        }
        allSchedByJobID['_'+theJob.id].push(''+oneElt.id); // force a string?
        // and we enrich the object with some related Job data
        oneElt.family = theJob.family;
        oneElt.icon = theJob.icon;
        oneElt.label = theJob.label;
        oneElt.isInAJL = theJob.isInAJL;

        box_data.scheduled += createMiniBox(oneElt, oneElt.id, theJob.id, getSafeClass(theJob.categories), '', '<i class="far fa-clock"></i>'+oneElt.frequency )
    });

    //  Fill the DIV with the content
    $("#catalogJobs").append(catalog_data);
    $('.oneJobBox').each(function() {
        $(this).append(box_data[$(this).attr('tgt')]);
        allBoxID[$(this).attr('tgt')] = $(this).attr('id');
        allBoxBaseTitle[$(this).attr('tgt')] = $('#'+$(this).attr('id')).closest('.oneBox').find('.title').html();
    })
    updateAllBoxTitle();

    // Build the Family Header - initialize the array
    array_family.sort();
    $.each(array_family, function(key,curElt) {
        searchFam.push(curElt);
        var oneFam = $("<button />", {
            "id"    :   curElt,
            "class" :   'familyBtn activefam btn'
        }).html(curElt).click(function(event) {
            filterGroup('fam', this.id);
            $(this).toggleClass("activefam");
            event.preventDefault();
        });
        $("#allFamilyBtns").append(oneFam);
    })
    
    // Build the Action header - initialize the array
    array_action.sort();
    $.each(array_action, function(key,curElt) {
        searchAct.push(curElt);
        var tooltipStr = (array_action_data[curElt] =='')?'':'<span class="tooltipBtn">'+array_action_data[curElt]+'</span>';
        var oneAction = $("<button />", {
            "id"        : curElt,
            "class"     : 'familyBtn activeact btn' })
        .html('<i class="'+curElt+'"></i>'+tooltipStr)
        .click(function(event) {
            filterGroup('act', this.id);
            $(this).toggleClass("activeact");
            event.preventDefault();})
        .hover(function(event) {
            $(this).children('.tooltipBtn').show();})
        .on('mouseleave', function(event) {
            $(this).children('.tooltipBtn').hide();});
        $("#allActionBtns").append(oneAction);
    })

    // Build the Category Menu on Left
	$.each(allCategories, function(key,curElt) {
        var liLabel = curElt.label;
        var liId = curElt.id;

        if(curElt.children) {
            // Category with Sub-categoy - expand/collapse ONLY
            var liElt = $("<li />", {
                "id" : "ZZZ_"+liId,
                "class" : 'listDynamic visible withChildren'
            }).html(liLabel).click(function() {            
                if ( $(this).next('ul').is(':visible') ) {
                    $(this).next('ul').hide("blind");
                    $(this).next('ul').find("svg.checkbox").addClass("fa-square").removeClass("fa-check-square");
                    $(this).addClass('visible');
                    $(this).removeClass('hidden');
                    $(this).removeClass("active");
                    $(this).find("svg.checkbox").addClass("fa-square").removeClass("fa-minus-square").removeClass("fa-check-square");
                    $(this).find("svg.caret").addClass("fa-caret-down").removeClass("fa-caret-up");
                    filterGroup('cleanCats', "ZZZ_"+liId);
                } else {
                    $(this).next('ul').show("blind");
                    $(this).next('ul').find("svg.checkbox").addClass("fa-check-square").removeClass("fa-square");
                    $(this).removeClass('visible');
                    $(this).addClass('hidden');
                    $(this).addClass("active");
                    $(this).find("svg.checkbox").addClass("fa-check-square").removeClass("fa-minus-square").removeClass("fa-square");
                    $(this).find("svg.caret").addClass("fa-caret-up").removeClass("fa-caret-down");
                    filterGroup('allCats', "ZZZ_"+liId);
                };
            });
            $("#categories").append(liElt);

            var ulSubElt = $("<ul />");
            $("#categories").append(ulSubElt);
            for (j = 0; j< curElt.children.length; j++) {
                var liSubLabel = curElt.children[j].label;
                var liSubId = curElt.children[j].id;
                var liSubElt = $("<li />", {
                    "id" : liSubId,
                    "class" : 'listDynamic isChildren'
                }).html(liSubLabel).click(function() {
                    $(this).toggleClass("active");
                    if ($(this).hasClass('active')) {
                        $(this).find("svg.checkbox").addClass("fa-check-square").removeClass("fa-square");
                    } else {
                        $(this).find("svg.checkbox").addClass("fa-square").removeClass("fa-check-square");
                    }
                    var allCount = $(this).parent().find('li').length;
                    var onCount = $(this).parent().find('li svg.checkbox.fa-check-square').length;
                    if (allCount == onCount) { // Full Select
                        $(this).parent().prev('li').find("svg.checkbox").addClass("fa-check-square").removeClass("fa-minus-square").removeClass("fa-square");
                        $(this).parent().prev('li').addClass('active');
                    } else if (onCount == 0) { // None Select
                        $(this).parent().prev('li').find("svg.checkbox").addClass("fa-square").removeClass("fa-minus-square").removeClass("fa-check-square");
                        $(this).parent().prev('li').removeClass('active');
                    } else {  // Partial Select
                        $(this).parent().prev('li').find("svg.checkbox").addClass("fa-minus-square").removeClass("fa-check-square").removeClass("fa-square");
                        $(this).parent().prev('li').addClass('active');
                    }
                    filterGroup('cat', this.id);
                });
                $(liSubElt).prepend('<i class="fal fa-square checkbox"></i>')
                $(ulSubElt).append(liSubElt);
            } 
            $(liElt).prepend('<i class="fal fa-square checkbox"></i>');
            $(liElt).append('<i class="fal fa-caret-down caret"></i>');

        } else {
            var liElt = $("<li />", {
                "id" : liId,
                "class" : 'listDynamic'
            }).html(liLabel).click(function() {            
                $(this).toggleClass("active");
                if ($(this).hasClass('active')) {
                    $(this).find("svg.checkbox").addClass("fa-check-square").removeClass("fa-square");
                } else {
                    $(this).find("svg.checkbox").addClass("fa-square").removeClass("fa-check-square");
                }
                filterGroup('cat', this.id);
            });
            $(liElt).prepend('<i class="fal fa-square checkbox"></i>')
            $("#categories").append(liElt);
        }
    });    
}

// *************************************************************************************************
$(document).ready(function(){

    // ======= SPECIAL DEBUG ======================
    if (isDebug) {
        $(document).on('click', 'a', function(event) {
            event.preventDefault();
            alert('** CLICK ** '+$(this).attr('href'))
        })
    }
    // ============================================

    // *************************************************************************************************
    // ****** FirstScreen
    $(document).on('click', '#myDataMenu', function() {
        if (!$(this).hasClass('myDataMenuDisabled')) {
            $(this).find('.menuIcon').toggleClass('display-no');
            $('.leftSectMenuItems').toggleClass('display-no');
        }
    });

    $('.oneTileHelp').hover( function() {
        $(this).prev().toggleClass('display-no');
    })
	
    // *************************************************************************************************
    // ****** Catalog HTML Builders
    initializeCatalog()

    // ************************************************************************
    // ** Events
    // ************************************************************************

    // ************************************************************************
    // *** 3 dots click in Card Box to show MENU

    // THis will destroy the submenu when leaving the fav
    $(document).on('mouseleave', '.fav', function(event){
        if ( $('#specialInFav').length > 0 ) {
            var y = parseInt($('#specialInFav').offset().top);
            var x = parseInt($('#specialInFav').offset().left);
            var h = parseInt($('#specialInFav').height());
            var w = parseInt($('#specialInFav').width());
            if ( ( event.pageX >= x && event.pageX <= (x+w) ) && ( event.pageY >= y && event.pageY <= (y+h) ) ) {
                // in the  submenu - nothing to do
            } else {
                $(this).removeClass('pseudoHover');
                $('#specialInFav').remove();
            }
        }
	})
    // Same when leaving the sub menu
    $(document).on('mouseleave', '#specialInFav', function(){
        $('.pseudoHover').removeClass('pseudoHover');
		$(this).remove();
	})
    // THis will hide the submenu when leaving the list
    $(document).on('mouseleave', '.list', function(){
        var threedot = $(this).children(".threedotcontent");
        threedot.css('display','none');
        $(this).children('.tooltipTile').removeClass('NoShow');
	})
    // Same when leaving the sub menu
    $(document).on('mouseleave', '.threedotcontent', function(){
		$(this).css('display','none');
    })
    function showSubMenu(threedot, event) {
        if(threedot.is(':hidden')) {
            threedot.css('display','block');
        } else{
            threedot.css('display','none');
        }
    }
    // Tiles 3 dot click
    $(document).on('click','.fa-ellipsis-v',function(event) {
        event.preventDefault();
        theTile = $(this).closest('.list');
        var threedot = theTile.children(".threedotcontent");
        showSubMenu(threedot, event);
        theTile.children('.tooltipTile').addClass('NoShow');
    });
    // BigBox right click
    $(document).on('contextmenu','.list', function(event){
        event.preventDefault();
        var threedot = $(this).children(".threedotcontent");
        showSubMenu(threedot, event);
        $(this).children('.tooltipTile').addClass('NoShow');
    })
    // MiniBox right click
    $(document).on('contextmenu','.fav', function(event){
        event.preventDefault();
        // due to the overflow-y: scroll, the z-index is unable to show the menu outside of the oneBox.
        // So we have to create the menu on the fly OUTSIDE of the box,
        // with all the sh*t it brings to it...
        var theElt;
        if ($(this).hasClass('scheduledJob')) {
            theElt = allScheduled[allKeySchedJobsByID['_'+$(this).attr('id')]];
        } else {
            theElt = allJobs[allKeyJobsByID['_'+$(this).attr('jobid')]];
        }
        if (isDebug) console.log(theElt);
        var thesubMenu = createSubMenu(theElt);
        if (thesubMenu != '') {
            var theBox = $(this).closest('.oneBox'); // get the box 
            theBox.after(thesubMenu); // add the mnu just after the box
            var theDiv = theBox.next(); // select the newly created div
            theDiv.css('display','block'); // show the submenu
            theDiv.prop('id','specialInFav'); // so we can find it
            $(this).addClass('pseudoHover'); // so the fv act as if it was hovered hen we are in the submenu
            var y = $(this).offset().top + ( $(this).height() * 0.7);
            var x = $(this).offset().left + 20;
            theDiv.offset({top: y, left: x});
        }
    })
    // discard any other right click
    $(document).on('contextmenu','div', function(event){
        event.preventDefault();
    })

    // ************************************************************************
    // AJL Management -- Add / Remove Jobs in AJL selection
    function setJobStatus(jobID, isAdded ) {
        var theJob = allJobs[allKeyJobsByID['_'+jobID]]
        if ($('#'+jobID).hasClass('notInAJL')) {
            if (isAdded) {
                // we are adding he Job in the AJL box
                var html = createMiniBox(theJob, 'AJL-'+theJob.id, theJob.id, getSafeClass(theJob.categories) );
                $('#'+allBoxID[ajlTgtCode]).append(html);
                theJob.isEditedAJL = true;
            } else {
                // we are removing he Job in the AJL box
                $('#AJL-'+jobID).remove();
                theJob.isEditedAJL = false;
            }
        } else {
            if (isAdded) {
                theJob.isEditedAJL = false;
            } else {
                theJob.isEditedAJL = true;
            }
        }
        $.each(searchID, function(key, id) {
            if (isAdded) {
                if ($('#'+id+jobID).hasClass('notInAJL')) {
                    $('#'+id+jobID).find('.svg .addJob').removeClass('display-no')
                } else {
                    $('#'+id+jobID).find('.svg .removeJob').addClass('display-no')
                }
                $('#'+id+jobID).find('.submenu.mgtAJL.removeJob').removeClass('display-no')
                $('#'+id+jobID).find('.submenu.mgtAJL.addJob').addClass('display-no')
            } else {
                if ($('#'+id+jobID).hasClass('notInAJL')) {
                    $('#'+id+jobID).find('.svg .addJob').addClass('display-no')
                } else {
                    $('#'+id+jobID).find('.svg .removeJob').removeClass('display-no')
                }
                $('#'+id+jobID).find('.submenu.mgtAJL.removeJob').addClass('display-no')
                $('#'+id+jobID).find('.submenu.mgtAJL.addJob').removeClass('display-no')
            }
        })
    }
    function setApplyJobButton() {
        var isSame = false;
        if (curSelectionAJL.length == allSelectedAJL.length) {
            // we may have gone back to origin...
            isSame = true;
            $.each(curSelectionAJL, function(idx, val) {
                if (allSelectedAJL.indexOf(val) < 0 ) {
                    isSame = false;
                    return false; // break
                }
            })
        }
        if (isSame) {
            if (isDebug) console.log('BACK TO ORIGIN');
            $('#AJL_applyChange').addClass('display-no');
            $('#AJL_finalValue').val('');
        } else {
            $('#AJL_applyChange').removeClass('display-no');
            $('#AJL_finalValue').val(curSelectionAJL.join(','));
        }
    }
    $(document).on('click', '.mgtAJL', function() {
        // init the array
        var jobID = $(this).attr('jobID');
        var tgtID = $(this).attr('tgtID');
        if (isDebug) console.log('Initial AJL Selection: '+curSelectionAJL);
    
        if ($(this).hasClass('notactive')) {
            if (isDebug) console.log('AJL >>'+jobID+'>>'+tgtID+' -- Nothing to do here....');
            // Nothing to do here...
        } else if ($(this).hasClass('removeJob')) {
            if (isDebug) console.log('AJL >>'+jobID+'>>'+tgtID+' -- Remove Job');
            var idx = curSelectionAJL.indexOf(tgtID);
            if (idx >= 0) {
                curSelectionAJL.splice(idx,1);
                // we just freed a spot....
                $('.mgtAJL.addJob').removeClass('notactive');
                setJobStatus(jobID, false);
                setApplyJobButton();
            }
        } else if ($(this).hasClass('addJob')) {
            if (isDebug) console.log('AJL >>'+jobID+'>>'+tgtID+' -- Add New Job');
            if (curSelectionAJL.length < maxSelectedAJL) {
                // just a security
                var idx = curSelectionAJL.indexOf(tgtID);
                if (idx < 0) {
                    curSelectionAJL.push(tgtID);
                    setJobStatus(jobID, true);
                    setApplyJobButton();
                }
                // this is a no more...
                if (curSelectionAJL.length >= maxSelectedAJL) {
                    $('.mgtAJL.addJob').addClass('notactive');
                }
            }
        }
        updateAJLBoxTitle();
        if (isDebug) console.log('New AJL Selection: '+curSelectionAJL);
    });

    // ************************************************************************
    // TODO *** Click on FAVORITE
    // $(document).on('click', '.fa-star', function() {
    //     var text = 'add to fav';
    //     if($(this).hasClass('empty')) {
    //         // TODO - to set 
    //         alert(g_allActions.setfav.label);
    //         $(this).removeClass('empty');
    //         $(this).addClass('full');
    //     }
    //     else if($(this).hasClass('full')) {
    //         // TODO - to set 
    //         alert(g_allActions.unsetfav.label)
    //         $(this).removeClass('full');
    //         $(this).addClass('empty');
    //     }
    // });
    // ***  Click on New Schedule
    // $('.add_scheduled').click(function(){
    //     alert('Do you want to add a scheduled request ?')
    // });
    // *** Click on New Custom
    // $('.create_custom').click(function(){
    //     alert('Do you want to create a new request ?')
    // });

    // ************************************************************************
    // **** MAIN SWITCH BETWEEN Home & Big Tiles
    $(document).on('click', '.header-title h2',function() {
        $('.title-active').removeClass("title-active");
        $(this).addClass("title-active");
        if($(this).hasClass('title-catalog')) {
            $('#home_content').css('display','none');
            $('#big_tiles_content').css('display','block');
            $('#catalogJobs .filterDiv .content').css("display","none");
            $('#catalogJobs .filterDiv .content_list_main').css("display","block");
        } else if($(this).hasClass('title-dashboard')) {
            $('#home_content').css('display','block');
            $('#big_tiles_content').css('display','none');
        }
    });
 
    // ************************************************************************
    // SEARCH AND FILTERS
    $(document).on('click', '#search_submit', function(e) {
        e.preventDefault();
        doTheSearchByKey();
    });
    // search on any key input on the field
    $(document).on('keyup', '#search_string', function(){
        doTheSearchByKey();
    });
    //***** disable key ENTER on form
	$(document).on('keyup keypress', 'form', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) {
			e.preventDefault();
			return false;
		}
	});


    // ************************************************************************
	// Scroll down 100% on click
	$('.scrolldown, #leftSectAction').click(function(){
		var y = $(window).scrollTop();  //your current y position on the page
        var h = $(window).height();
        
		$('html, body').animate({
			scrollTop: y + h
		}, 500, function() {
            $('#top-header').css({position: 'fixed'});
            $('#shadow-background').css({position: 'fixed'});
            $('#left-side').css({position: 'fixed'});
            // $('left-side-feedback').css({position: 'fixed'});
            $('#button_bar').css({position: 'fixed', width: '76%', top: '80px'});
            $('#button_bar-background').css({position: 'fixed', width: '80%', top: '80px'});
            $('#home-container').hide();
        });

        $('html, body').animate({ scrollTop: 0 }, 500);
    }); 

    // Switch back and scroll Up 100% on click
	$('#back-home').click(function(){
        $('#top-header').css({position: 'absolute'});
        $('#shadow-background').css({position: 'absolute'});
        $('#left-side').css({position: 'absolute'});
        // $('left-side-feedback').css({position: 'absolute'});
        $('#button_bar').css({position: 'absolute', width: '95%', top: '0'});
        $('#button_bar-background').css({position: 'absolute', width: '100%', top: '0'});
        $('#home-container').slideDown(500);
        
		$('html, body').animate({ scrollTop: 0 }, 100);
    }); 
    
    // Fix the catalog navigation
    $(window).scroll(function(){
        var y = $(window).scrollTop();  //your current y position on the page
        var h = $(window).height();
        
        if( y >= h && $('#home-container').is(":visible") ) {
            $(this).delay(300).queue(function() {
                $('#top-header').css({position: 'fixed'});
                $('#shadow-background').css({position: 'fixed'});
                $('#left-side').css({position: 'fixed'});
                // $('left-side-feedback').css({position: 'fixed'});
                $('#button_bar').css({position: 'fixed', width: '76%', top: '80px'});
                $('#button_bar-background').css({position: 'fixed', width: '80%', top: '80px'});

                $('#home-container').hide();

                $(this).dequeue();
            });
        }

    });

    $(document).on('click', '#show_debug_trace', function(){
        $('#debug_trace').toggleClass('display-no');
    })

    ready = true;
});


// ************************************************************************
// External Functions ( from sapio365 )
// this is protected so it runs only once the document is fully loaded

function  externalFunction(funcName, obj) {
	if (!ready)	{
		tmpObj = obj;
		window.setTimeout("externalFunction('"+funcName+"',tmpObj)", 500);	
	}
	else {
		if (funcName == 'disableMyDataMenu' ) { 
			disableMyDataMenu(); 
		}	
		if (funcName == 'enableMyDataMenu' ) {
			enableMyDataMenu();
		}
		if (funcName == 'specialShow' ) {
			specialShow(obj);
        }
		if (funcName == 'removeOneJob' ) {
			removeOneJob(obj);
        }
		if (funcName == 'updateAllJobs' ) {
			updateAllJobs(obj);
        }
		if (funcName == 'updateAllScheds' ) {
			updateAllScheds(obj);
        }
		if (funcName == 'removeOneSched' ) {
			removeOneSched(obj);
        }
		if (funcName == 'updateAJLInfo' ) {
			updateAJLInfo(obj);
        }
		if (funcName == 'changeAJLCanRemove' ) {
			changeAJLCanRemove(obj);
        }
		if (funcName == 'fullInitialization' ) {
			fullInitialization(obj);
        }
	}
}
function disableMyDataMenu() {
    $('#myDataMenu').addClass('myDataMenuDisabled');
    $('#myDataMenu').find('.menuIcon').addClass('display-no');
    $('#myDataMenu').find('.menuIconNo').removeClass('display-no');
    $('.leftSectMenuItems').addClass('display-no');
}
function enableMyDataMenu() {
    // Needed only if currently disabled
    if( $('#myDataMenu').hasClass('myDataMenuDisabled') ) {
        $('#myDataMenu').removeClass('myDataMenuDisabled');
        $('#myDataMenu').find('.menuIcon').first().removeClass('display-no');
        $('#myDataMenu').find('.menuIconNo').addClass('display-no');
    }
}
function specialShow(code) {
	$('.spShow_'+code).removeClass('display-no');
}

// *************************************************************************
// External function to update the Catalog

var errorContext = '';
// Debug function
function DebugUPDATE() {
    var jsonFile = getFile('file:///C:/Dev/VS2015/Office365Admin/html/welcome/allUpdate.json');
    // var jsonFile = getFile('file:///C:/Dev/VS2015/Office365Admin/html/welcome/update.json');
    return updateAllJobs(jsonFile);
}
function DebugUPDATESched() {
    var jsonFile = getFile('file:///C:/Dev/VS2015/Office365Admin/html/welcome/updateSched.json');
    // var jsonFile = getFile('file:///C:/Dev/VS2015/Office365Admin/html/welcome/update.json');
    return updateAllScheds(jsonFile);
}

// error handling function for all external functions
function errorHandling(err) {
    console_log_and_trace('!!ERROR!! ['+errorContext+']');
    console_log_and_trace(err);
    html = '<b>updateOneJob Error!</b><br>['+errorContext+'] '+err+'<br><br><div id="debug_trace">'+fullTrace+'</div>';
    $('#ytria-body').html(html);
    return '['+errorContext+'] '+err;
}

// External call to Update the AJL Information
// @input: string - AJL ids + string - max AJL number
function updateAJLInfo(AJLids, maxAJL) {
    fullTrace = '';
    $('#debug_trace').html('');

    errorContext = '';
    console_log_and_trace('********* updateAJLInfo *********');
	try {
        errorContext = 'set new Max '+maxAJL;
        maxSelectedAJL = parseInt(maxAJL);
        errorContext = 'set new List '+AJLids;
        allSelectedAJL = AJLids.split(',');
        initSelectedAJL();

        // we reset the global situation
        errorContext = 'reset global context '+maxAJL+' with '+AJLids;
        console_log_and_trace(errorContext);
        globalResetAJL()
    }
    catch(err) {
        console_log_and_trace('!!ERROR!! ['+errorContext+']');
        console_log_and_trace(err);
        html = '<b>updateOneJob Error!</b><br>['+errorContext+'] '+err+'<br><br><div id="debug_trace">'+fullTrace+'</div>';
        $('#ytria-body').html(html);
        return '['+errorContext+'] '+err;
    }
}

// External call to change the AJL Remove Ability
// @input: string - AJL_canRemoveJob + string - AJL_newState
function changeAJLCanRemove(AJL_canRemoveJob, AJL_newState) {
    fullTrace = '';
    $('#debug_trace').html('');

    errorContext = '';
    console_log_and_trace('********* Reset All in AJL Jobs *********');
	try {
        g_AJL_newState = AJL_newState;
        g_AJL_canRemoveJob = ( AJL_canRemoveJob != '' );
        // we reset the global situation
        errorContext = 'reset global context - now '+(g_AJL_canRemoveJob?'can':'CANNOT')+' remove Jobs';
        console_log_and_trace(errorContext);
        globalResetAJL()

        // Recreate the contextual menu in Lists
        // note: I agree, a Job may have been already regenerated by globalResetAJL()
        // but it's easier this way.....
        $('.list').not('.notInAJL').each(function(){
            var jobId = $(this).attr('id');
            var idxJob = allKeyJobsByID['_'+jobId];
            var curJob = allJobs[idxJob];

            // we need to set the AJL state of what has been provided
            errorContext = 'Reset AJL State';
            initJobDefault(curJob);
            // recreate the submenu Contextual Menu
            errorContext = 'recreate SubMenu';
            var html = createSubMenu(curJob);
            $(this).find('.threedotcontent').replaceWith(html);
        })
    }
	catch(err) {
        return errorHandling(err);
    }
}

// External call to kill a given JOB
// @input: string - Job ID
function removeOneJob(jobID) {
	try {
        var idxJob = allKeyJobsByID['_'+jobID];
        if ('undefined' != typeof idxJob) {
            var theJob = allJobs[idxJob];
            console_log_and_trace("REMOVE: "+theJob.id);
            $.each(searchID, function(key, id) {
                $('#'+id+jobID).remove();
            });

            // Remove any Scheduled (if any) of the Job
            errorContext = 'Remove in Scheduled Box ';
            if ( 'undefined' != typeof allSchedByJobID['_'+theJob.id] ) {
                $.each(allSchedByJobID['_'+theJob.id], function(key, val) {
                    var theSched = allScheduled[allKeySchedJobsByID['_'+val]];
                    console_log_and_trace(theSched.id);
                    $('#'+theSched.id).remove();
                    allScheduled.splice(allKeySchedJobsByID['_'+val],1);
                    allSchedByJobID['_'+theJob.id].splice(key, 1);
                })
                // we need to rebuild the table of index 
                allKeySchedJobsByID = [];
                $.each(allScheduled, function(key, theSched){
                    allKeySchedJobsByID['_'+theSched.id] = key;
                })
            }

            allJobs.splice(idxJob,1);
            // we need to rebuild the table of index 
            allKeyJobsByID = [];
            $.each(allJobs, function(key, theJob){
                allKeyJobsByID['_'+theJob.id] = key;
            })
        }
        updateAllBoxTitle();
    }
	catch(err) {
        return errorHandling(err);
    }
}

// External call to Create OR Update several Job
// @input: string - json of Multiple Jobs
function updateAllJobs(newJson) {
    fullTrace = '';
    $('#debug_trace').html('');

    errorContext = '';
    console_log_and_trace('********* updateAllJobs *********');
	try {
        errorContext = 'Parse Json';
        var allJobData = JSON.parse(newJson);
        errorContext = 'Loop On Jobs';
        $.each(allJobData, function(key, jobData) {
            errorContext = 'processOneJob';
            processOneJob(jobData);
        })
        updateAllBoxTitle();
   }
	catch(err) {
        return errorHandling(err);
    }
}

// External call to Create OR Update several Scheduled Jobs
// @input: string - json of multiple Scheduled Jobs
function updateAllScheds(newJson) {
    fullTrace = '';
    $('#debug_trace').html('');

    errorContext = '';
    console_log_and_trace('********* updateAllScheds *********');
	try {
        errorContext = 'Parse Json';
        var allSchedData = JSON.parse(newJson);
        errorContext = 'Loop On Jobs';
        $.each(allSchedData, function(key, schedData) {
            errorContext = 'processOneSched';
            processOneSched(schedData);
        })
        updateOneBoxTitle(schedTgtCode);
   }
	catch(err) {
        return errorHandling(err);
    }
}

// External call to kill a given Scheduled Job
// @input: string - schedule ID
function removeOneSched(scheduleID) {
	try {
        var idxSched = allKeySchedJobsByID['_'+scheduleID];
        if ('undefined' != typeof idxSched) {
            var theSched = allScheduled[idxSched];
            console_log_and_trace(idxSched+" : "+theSched.id);
            $('#'+theSched.id).remove();
            var theJobID = theSched.idJob;
            $.each(allSchedByJobID['_'+theJobID], function(key, val) {
                if (val == theSched.id) {
                    console_log_and_trace('remove in Job '+theJobID+' scheduled list:');
                    allSchedByJobID['_'+theJobID].splice(key,1);
                }
            })
            allScheduled.splice(idxSched,1);
            // we need to rebuild the table of index 
            allKeySchedJobsByID = [];
            $.each(allScheduled, function(key, theSched){
                allKeySchedJobsByID['_'+theSched.id] = key;
            })
        }
        updateOneBoxTitle(schedTgtCode);
    }
	catch(err) {
        return errorHandling(err);
    }
}

// External call to Initialize the Catalog
// MUST BE ONLY CALLED IF PREVIOUS JS_DATA was empty
// @input: string - JS-DATA
function fullInitialization(newJSData) {
	try {
        errorContext = "apply new DATA-JS";
        window.eval(newJSData);
        console.log("** loaded external Data JavaScript file");
        initializeCatalog();
    }
	catch(err) {
        return errorHandling(err);
    }
}
