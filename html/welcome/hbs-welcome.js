(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Main'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"firstscreen",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "		<form id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" method=\""
    + alias4(((helper = (helper = helpers.method || (depth0 != null ? depth0.method : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"method","hash":{},"data":data}) : helper)))
    + "\" action=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\">\r\n			<div class=\"wrap-form\"> \r\n                    "
    + alias4((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || alias2).call(alias1,depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n        	</div>\r\n		</form>\r\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"catalog",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"5":function(container,depth0,helpers,partials,data) {
    return "                "
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"section subsectionExist\" id=\"home-container\" ondragstart=\"return false;\">\r\n	<div class=\"wrapper\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n</div>\r\n<div class=\"section subsectionExist\" id=\"catalog-container\">\r\n    <div class=\"wrapper\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n</div>";
},"useData":true});
templates['catalog'] = template({"1":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "    /* SVG */\r\n    ."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + " .svg { color: rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",1); border-color: rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",1)}\r\n    ."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ".notInAJL .svg { color: rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",0.5); border-color: rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",0.5)}\r\n    /* :hover */\r\n    .fav.filterDiv."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ":hover a { color: rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",1); }\r\n    /* .fav.filterDiv."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ".notInAJL:hover a { color: rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",0.5); } */\r\n    .fav.filterDiv."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ":hover .fav_content { color: rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",1); }\r\n    .fav.filterDiv."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ".notInAJL:hover .fav_content { color: #999999; }\r\n    ."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ":hover { border: 1.5px solid rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",1); }\r\n    ."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ".notInAJL:hover { border: 1px solid rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",0.5); }\r\n    /* special pseudoHover for fav... */\r\n    .fav.filterDiv."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ".pseudoHover a { color: rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",1); }\r\n    /* .fav.filterDiv."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ".notInAJL.pseudoHover a { color: rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",0.5); } */\r\n    .fav.filterDiv."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ".pseudoHover .fav_content { color: rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",1); }\r\n    /* .fav.filterDiv."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ".notInAJL.pseudoHover .fav_content { color: rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",0.5); } */\r\n    ."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ".pseudoHover { border: 1.5px solid rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",1); }\r\n    ."
    + alias2(alias1((depth0 != null ? depth0.family : depth0), depth0))
    + ".notInAJL.pseudoHover { border: 1px solid rgba("
    + alias2(alias1((depth0 != null ? depth0.color : depth0), depth0))
    + ",0.5); }\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                <div id=\"top-header\">\r\n                    <div id=\"back-home\">\r\n                        <i class=\""
    + alias4(((helper = (helper = helpers["back-home-icon"] || (depth0 != null ? depth0["back-home-icon"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"back-home-icon","hash":{},"data":data}) : helper)))
    + "\"></i>\r\n                    </div>\r\n                    <div class=\"apply-change\">\r\n                        <button id=\"AJL_applyChange\" class=\"display-no\" name=\""
    + alias4(((helper = (helper = helpers.AJL_buttonValue || (depth0 != null ? depth0.AJL_buttonValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AJL_buttonValue","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers.AJL_buttonValue || (depth0 != null ? depth0.AJL_buttonValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AJL_buttonValue","hash":{},"data":data}) : helper)))
    + "\">\r\n                            <i class=\"fas fa-save\"></i>\r\n                            <span>"
    + ((stack1 = ((helper = (helper = helpers.AJL_labelSubmit || (depth0 != null ? depth0.AJL_labelSubmit : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AJL_labelSubmit","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\r\n                        </button>\r\n                        <input id=\"AJL_finalValue\" type=\"hidden\" name=\""
    + alias4(((helper = (helper = helpers.AJL_fieldName || (depth0 != null ? depth0.AJL_fieldName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AJL_fieldName","hash":{},"data":data}) : helper)))
    + "\" value=\"\">\r\n                    </div>\r\n                    <div class=\"header-title\">\r\n                        <h2 class=\"title-catalog\">"
    + ((stack1 = ((helper = (helper = helpers["title-catalog"] || (depth0 != null ? depth0["title-catalog"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title-catalog","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</h2>\r\n                        <h2 class=\"title-dashboard title-active\">"
    + ((stack1 = ((helper = (helper = helpers["title-dashboard"] || (depth0 != null ? depth0["title-dashboard"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title-dashboard","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</h2>\r\n                    </div>\r\n                    <ul id=\"search_bar_content\">\r\n                        <li class=\"input_container\"> <input id=\"search_string\" type=\"text\" placeholder=\""
    + alias4(((helper = (helper = helpers["search-placeholder"] || (depth0 != null ? depth0["search-placeholder"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"search-placeholder","hash":{},"data":data}) : helper)))
    + "\"> </li>\r\n                        <li class=\"input_button\"><button id=\"search_submit\" type=\"submit\"><i class=\"fa fa-search\"></i></button></li>\r\n                    </ul>\r\n                </div>\r\n                <div id=\"shadow-background\"></div>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                <div id=\"left-side\">\r\n                    <ul id=\"categories\" class=\"leftbar\"></ul>\r\n                    <div id=\"left-side-feedback\">\r\n                        <button id=\"button-feedback\" name=\""
    + alias4(((helper = (helper = helpers["value-feedback"] || (depth0 != null ? depth0["value-feedback"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value-feedback","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + alias4(((helper = (helper = helpers["value-feedback"] || (depth0 != null ? depth0["value-feedback"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value-feedback","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = ((helper = (helper = helpers["label-feedback"] || (depth0 != null ? depth0["label-feedback"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label-feedback","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</button>\r\n                    </div>\r\n                </div>\r\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                <div id=\"right-side\">\r\n                    <div id=\"button_bar\">\r\n                        <div class=\"main_button_contain introfilters\">\r\n                            <span>"
    + ((stack1 = ((helper = (helper = helpers["intro-filters"] || (depth0 != null ? depth0["intro-filters"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"intro-filters","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\r\n                        </div>\r\n                        <div id=\"allFamilyBtns\">\r\n                            <span>"
    + ((stack1 = ((helper = (helper = helpers["intro-scope"] || (depth0 != null ? depth0["intro-scope"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"intro-scope","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\r\n                        </div>\r\n                        <div class=\"divider\"></div>\r\n                        <div class=\"main_button_contain jobcategory\">\r\n                            <span>"
    + ((stack1 = ((helper = (helper = helpers["intro-job"] || (depth0 != null ? depth0["intro-job"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"intro-job","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\r\n                        </div>\r\n                        <div id=\"allActionBtns\"></div>\r\n                    </div>\r\n                    <div id=\"button_bar-background\"></div>\r\n\r\n                    <!--Main page content-->\r\n                    <div id=\"home_content\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0["left-first-title"] : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0["right-first-title"] : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0["left-second-title"] : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0["right-second-title"] : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0["left-third-title"] : depth0),{"name":"if","hash":{},"fn":container.program(16, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0["right-third-title"] : depth0),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </div>\r\n                    \r\n                    <!--big tiles content-->\r\n                    <div id=\"big_tiles_content\">\r\n                        <div id=\"catalogJobs\"></div>\r\n                    </div>\r\n                    \r\n                </div>\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div id=\"left_first_box\" class=\"oneBox\">\r\n                            <div class=\"title\">"
    + ((stack1 = ((helper = (helper = helpers["left-first-title"] || (depth0 != null ? depth0["left-first-title"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"left-first-title","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                            <div id=\"left_first_content\">\r\n                                <div id=\"left_first_Jobs\" class=\"oneJobBox bigBox\" tgt=\""
    + container.escapeExpression(((helper = (helper = helpers["left-first-data"] || (depth0 != null ? depth0["left-first-data"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"left-first-data","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n                            </div>\r\n                        </div>\r\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div id=\"right_first_box\" class=\"oneBox\">\r\n                            <div class=\"title\">"
    + ((stack1 = ((helper = (helper = helpers["right-first-title"] || (depth0 != null ? depth0["right-first-title"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"right-first-title","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                            <div id=\"right_first_content\">\r\n                                <div id=\"right_first_Jobs\" class=\"oneJobBox smallBox\" tgt=\""
    + container.escapeExpression(((helper = (helper = helpers["right-first-data"] || (depth0 != null ? depth0["right-first-data"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"right-first-data","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n                            </div>\r\n                        </div>\r\n";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div id=\"left_second_box\" class=\"oneBox\">\r\n                            <div class=\"title\">"
    + ((stack1 = ((helper = (helper = helpers["left-second-title"] || (depth0 != null ? depth0["left-second-title"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"left-second-title","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                            <div id=\"left_second_content\">\r\n                                <div id=\"left_second_Jobs\" class=\"oneJobBox bigBox\" tgt=\""
    + container.escapeExpression(((helper = (helper = helpers["left-second-data"] || (depth0 != null ? depth0["left-second-data"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"left-second-data","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n                            </div>\r\n                        </div>\r\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div id=\"right_second_box\" class=\"oneBox\">\r\n                            <div class=\"title\">"
    + ((stack1 = ((helper = (helper = helpers["right-second-title"] || (depth0 != null ? depth0["right-second-title"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"right-second-title","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                            <div id=\"right_second_content\">\r\n                                <div id=\"right_second_Jobs\" class=\"oneJobBox smallBox\" tgt=\""
    + container.escapeExpression(((helper = (helper = helpers["right-second-data"] || (depth0 != null ? depth0["right-second-data"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"right-second-data","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n                            </div>\r\n                        </div>\r\n";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div id=\"left_third_box\" class=\"oneBox\">\r\n                            <div class=\"title\">"
    + ((stack1 = ((helper = (helper = helpers["left-third-title"] || (depth0 != null ? depth0["left-third-title"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"left-third-title","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                            <div id=\"left_third_content\">\r\n                                <div id=\"left_third_Jobs\" class=\"oneJobBox bigBox\" tgt=\""
    + container.escapeExpression(((helper = (helper = helpers["left-third-data"] || (depth0 != null ? depth0["left-third-data"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"left-third-data","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n                            </div>\r\n                            "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isDebug : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                        </div>\r\n";
},"17":function(container,depth0,helpers,partials,data) {
    return "<div id=\"show_debug_trace\"> &gt;&gt; See/Hide Trace &lt;&lt; </div><div id=\"debug_trace\" class=\"display-no\"></div>";
},"19":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                        <div id=\"right_third_box\" class=\"oneBox\">\r\n                            <div class=\"title\">"
    + ((stack1 = ((helper = (helper = helpers["right-third-title"] || (depth0 != null ? depth0["right-third-title"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"right-third-title","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                            <div id=\"right_third_content\">\r\n                                <div id=\"right_third_Jobs\" class=\"oneJobBox smallBox\" tgt=\""
    + container.escapeExpression(((helper = (helper = helpers["right-third-data"] || (depth0 != null ? depth0["right-third-data"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"right-third-data","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n                            </div>\r\n                        </div>\r\n";
},"21":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "            var oneMenuItem = {label:\""
    + ((stack1 = alias1((depth0 != null ? depth0.label : depth0), depth0)) != null ? stack1 : "")
    + "\", url:\""
    + alias2(alias1((depth0 != null ? depth0.url : depth0), depth0))
    + "\", tgt:\""
    + alias2(alias1((depth0 != null ? depth0.tgt : depth0), depth0))
    + "\", val:\""
    + alias2(alias1((depth0 != null ? depth0.val : depth0), depth0))
    + "\"};\r\n            g_allSubMenuItems.push(oneMenuItem);\r\n";
},"23":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "            var oneMenuItem = {label:\""
    + ((stack1 = alias1((depth0 != null ? depth0.label : depth0), depth0)) != null ? stack1 : "")
    + "\", url:\""
    + alias2(alias1((depth0 != null ? depth0.url : depth0), depth0))
    + "\", tgt:\""
    + alias2(alias1((depth0 != null ? depth0.tgt : depth0), depth0))
    + "\", val:\""
    + alias2(alias1((depth0 != null ? depth0.val : depth0), depth0))
    + "\"};\r\n            g_allSchedSubMenuItems.push(oneMenuItem);\r\n";
},"25":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "            g_allActions['"
    + alias2(alias1((depth0 != null ? depth0.key : depth0), depth0))
    + "'] = {label:\""
    + ((stack1 = alias1((depth0 != null ? depth0.label : depth0), depth0)) != null ? stack1 : "")
    + "\", url:\""
    + alias2(alias1((depth0 != null ? depth0.url : depth0), depth0))
    + "\" } ;\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<style>\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.colors : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    /* When No Family has been set on the Job */\r\n    /* SVG */\r\n    .ZNoFamilySetZ .svg { color: rgba(79,79,79,1); border-color: rgba(79,79,79,1)}\r\n    .ZNoFamilySetZ.notInAJL .svg { color:  rgba(79,79,79,0.5); border-color:  rgba(79,79,79,0.5); }\r\n    /* :hover */\r\n    .fav.filterDiv.ZNoFamilySetZ:hover a { color: rgba(79,79,79,1); }\r\n    /* .fav.filterDiv.ZNoFamilySetZ.notInAJL:hover a { color: rgba(79,79,79,0.5); } */\r\n    .fav.filterDiv.ZNoFamilySetZ:hover .fav_content { color: rgba(79,79,79,1); }\r\n    .fav.filterDiv.ZNoFamilySetZ.notInAJL:hover .fav_content { color: #999999; }\r\n    .ZNoFamilySetZ:hover { border: 1.5px solid rgba(79,79,79,1); }\r\n    .ZNoFamilySetZ.notInAJL:hover { border: 1px solid rgba(79,79,79,0.5); }\r\n    /* special pseudoHover for fav... */\r\n    .fav.filterDiv.ZNoFamilySetZ.pseudoHover a { color: rgba(79,79,79,1); }\r\n    /* .fav.filterDiv.ZNoFamilySetZ.notInAJL.pseudoHover a { color: rgba(79,79,79,0.5); } */\r\n    .fav.filterDiv.ZNoFamilySetZ.pseudoHover .fav_content { color: rgba(79,79,79,1); }\r\n    /* .fav.filterDiv.ZNoFamilySetZ.notInAJL.pseudoHover .fav_content { color: rgba(79,79,79,0.5); } */\r\n    .ZNoFamilySetZ.pseudoHover { border: 1.5px solid rgba(79,79,79,1); }\r\n    .ZNoFamilySetZ.notInAJL.pseudoHover { border: 1px solid rgba(79,79,79,0.5); }\r\n</style>\r\n<div class=\"\"  ondragstart=\"return false;\">\r\n    <form id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" method=\""
    + alias4(((helper = (helper = helpers.method || (depth0 != null ? depth0.method : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"method","hash":{},"data":data}) : helper)))
    + "\" action=\""
    + alias4(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"action","hash":{},"data":data}) : helper)))
    + "\">\r\n        <div class=\"catalog-wrapper\" style=\"height:100%;\">\r\n\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.topHeader : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.leftSide : depth0),{"name":"each","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.rightSide : depth0),{"name":"each","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n        </div>\r\n    </form>\r\n</div>\r\n<script language=\"javascript\"> \r\n    var g_allSubMenuItems = [];\r\n    var g_allSchedSubMenuItems = [];\r\n    var g_allActions = [];\r\n    var g_tileAction = \""
    + alias4(((helper = (helper = helpers.tileAction || (depth0 != null ? depth0.tileAction : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tileAction","hash":{},"data":data}) : helper)))
    + "\";\r\n    var g_title_currentCount = \""
    + alias4(((helper = (helper = helpers.title_currentCount || (depth0 != null ? depth0.title_currentCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title_currentCount","hash":{},"data":data}) : helper)))
    + "\";\r\n    // Following may be empty - this is for AJL management\r\n    var g_AJL_labelAdd = \""
    + alias4(((helper = (helper = helpers.AJL_labelAdd || (depth0 != null ? depth0.AJL_labelAdd : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AJL_labelAdd","hash":{},"data":data}) : helper)))
    + "\";\r\n    var g_AJL_labelRemowe = \""
    + alias4(((helper = (helper = helpers.AJL_labelRemowe || (depth0 != null ? depth0.AJL_labelRemowe : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AJL_labelRemowe","hash":{},"data":data}) : helper)))
    + "\";\r\n    var g_AJL_canRemoveJob = ( \""
    + alias4(((helper = (helper = helpers.AJL_canRemoveJob || (depth0 != null ? depth0.AJL_canRemoveJob : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AJL_canRemoveJob","hash":{},"data":data}) : helper)))
    + "\" != \"\" );\r\n    var g_AJL_currentState = \""
    + ((stack1 = ((helper = (helper = helpers.AJL_currentState || (depth0 != null ? depth0.AJL_currentState : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AJL_currentState","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\";\r\n    var g_AJL_newState = \""
    + ((stack1 = ((helper = (helper = helpers.AJL_newState || (depth0 != null ? depth0.AJL_newState : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AJL_newState","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\";\r\n\r\n    // Create the global Array of Menu Items\r\n    function initializeSubMenuItems() {\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.menuItems : depth0),{"name":"each","hash":{},"fn":container.program(21, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.menuSheduledItems : depth0),{"name":"each","hash":{},"fn":container.program(23, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.actions : depth0),{"name":"each","hash":{},"fn":container.program(25, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    }\r\n</script>>";
},"useData":true});
templates['firstscreen'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.cells : depth0),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "#oneTile_"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + " .oneTileAction.withAction:hover .oneTileTextIntro { /*text-decoration: underline;*/ color: "
    + alias4(((helper = (helper = helpers.color || (depth0 != null ? depth0.color : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"color","hash":{},"data":data}) : helper)))
    + "; }\r\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                    <a href='"
    + container.escapeExpression(((helper = (helper = helpers.buttonUrl || (depth0 != null ? depth0.buttonUrl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonUrl","hash":{},"data":data}) : helper)))
    + "'><div class=\"oneMenuButton\">"
    + ((stack1 = ((helper = (helper = helpers.buttonTitle || (depth0 != null ? depth0.buttonTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonTitle","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div></a>\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "        <div class=\"rightSectionRow\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.cells : depth0),{"name":"each","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\r\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div id=\"oneTile_"
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"oneTile\">\r\n                <div>\r\n                    <div class=\"oneTileAction"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.action : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">\r\n                        "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.action : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "<div class=\"oneTileIcon\" style=\"color: "
    + alias4(((helper = (helper = helpers.color || (depth0 != null ? depth0.color : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"color","hash":{},"data":data}) : helper)))
    + ";\"><i class=\""
    + alias4(((helper = (helper = helpers.icon || (depth0 != null ? depth0.icon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"icon","hash":{},"data":data}) : helper)))
    + "\"></i></div>"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.action : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                        "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.action : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "<div class=\"oneTileTitle\" style=\"color: "
    + alias4(((helper = (helper = helpers.color || (depth0 != null ? depth0.color : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"color","hash":{},"data":data}) : helper)))
    + ";\">"
    + ((stack1 = ((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.action : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                        <div class=\"oneTileLine\"  style=\"color: "
    + alias4(((helper = (helper = helpers.color || (depth0 != null ? depth0.color : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"color","hash":{},"data":data}) : helper)))
    + ";\"></div>\r\n                        "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.action : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "<div class=\"oneTileTextIntro\">"
    + ((stack1 = ((helper = (helper = helpers.intro || (depth0 != null ? depth0.intro : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"intro","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.action : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n                    </div>\r\n                    <div class=\"oneTileIntro\">\r\n                        <div class=\"oneTileAllButtons\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.buttons : depth0),{"name":"each","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                        </div>\r\n                    </div>\r\n                    <div class=\"oneTilePopup display-no\">\r\n                        <div class=\"oneTilePopupIntro\">"
    + ((stack1 = ((helper = (helper = helpers.helpIntro || (depth0 != null ? depth0.helpIntro : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"helpIntro","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                        <div class=\"oneTilePopupTitle\">"
    + ((stack1 = ((helper = (helper = helpers.helpTitle || (depth0 != null ? depth0.helpTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"helpTitle","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n                        <ul class=\"oneTilePopupItemList\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.helpItemList : depth0),{"name":"each","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                        </ul>\r\n                    </div>\r\n                    <div class=\"oneTileHelp\">?</div>\r\n                </div>\r\n            </div>\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    return " withAction";
},"10":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<a href='"
    + container.escapeExpression(((helper = (helper = helpers.action || (depth0 != null ? depth0.action : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"action","hash":{},"data":data}) : helper)))
    + "'>";
},"12":function(container,depth0,helpers,partials,data) {
    return "</a>";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "                            <a class=\""
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.specialShow : depth0),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" href='"
    + container.escapeExpression(((helper = (helper = helpers.buttonUrl || (depth0 != null ? depth0.buttonUrl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonUrl","hash":{},"data":data}) : helper)))
    + "'><div class=\"oneTileButton\">"
    + ((stack1 = ((helper = (helper = helpers.buttonTitle || (depth0 != null ? depth0.buttonTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonTitle","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div></a>\r\n";
},"15":function(container,depth0,helpers,partials,data) {
    var helper;

  return "spShow_"
    + container.escapeExpression(((helper = (helper = helpers.specialShow || (depth0 != null ? depth0.specialShow : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"specialShow","hash":{},"data":data}) : helper)))
    + " display-no";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "                            <li class=\"oneTilePopupItem\">"
    + ((stack1 = ((helper = (helper = helpers.helpItem || (depth0 != null ? depth0.helpItem : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"helpItem","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</li>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.lambda;

  return "<style>\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.tiles : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</style>\r\n<div id=\"firstScreen\">\r\n    <div id=\"leftSection\">\r\n        <div id=\"leftSectionTop\">\r\n            <div class=\"leftSectHeader\">\r\n                <div class=\"leftSectMainTitle\">"
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0.leftSection : depth0)) != null ? stack1.title : stack1), depth0)) != null ? stack1 : "")
    + "</div>\r\n                <div class=\"leftSectSubTitle\">\r\n                    <div class=\"leftSectIcon\">\r\n                        <?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n                        <!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\r\n                        <svg version=\"1.1\" id=\"Livello_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r\n                            viewBox=\"0 0 95 105\" style=\"enable-background:new 0 0 95 105;\" xml:space=\"preserve\">\r\n                        <g>\r\n                            <path class=\"fillPurple\" d=\"M51.8,47.5c0-5.2-4.3-9.4-9.5-9.4c-5.2,0-9.4,4.3-9.4,9.5c0,5.2,4.3,9.4,9.5,9.4C47.6,57,51.8,52.7,51.8,47.5z\r\n                                M42.4,52.9c-3,0-5.4-2.4-5.4-5.3c0-3,2.4-5.4,5.3-5.4c3,0,5.4,2.4,5.4,5.3C47.7,50.5,45.3,52.9,42.4,52.9z\"/>\r\n                            <path class=\"fillPurple\" d=\"M40.4,71c0.2-0.4,0.4-0.7,0.8-0.9c0.4-0.2,0.8-0.3,1.2-0.3c0,0,0.1,0,0.1,0c0.4,0,0.8,0.1,1.2,0.3\r\n                                c0.4,0.2,0.6,0.5,0.8,0.9l0.1,0.2c0.7,1.7,2.1,3.1,3.8,3.8c1.7,0.7,3.7,0.7,5.4,0c1.7-0.7,3.1-2.1,3.8-3.8c0.7-1.7,0.7-3.7,0-5.4\r\n                                l-0.2-0.4c-0.2-0.4-0.2-0.8-0.1-1.2c0.1-0.4,0.3-0.8,0.6-1.1l0,0c0.3-0.3,0.7-0.5,1.1-0.6c0.4-0.1,0.8-0.1,1.2,0.1l0.2,0.1\r\n                                c0.9,0.4,1.8,0.5,2.7,0.5c0.9,0,1.9-0.2,2.7-0.5c0.9-0.4,1.6-0.9,2.3-1.5c0.7-0.7,1.2-1.4,1.5-2.3l0,0c0,0,0,0,0,0c0,0,0,0,0,0h0\r\n                                c0.4-0.9,0.5-1.8,0.5-2.7c0-0.9-0.2-1.9-0.5-2.7c-0.4-0.9-0.9-1.6-1.5-2.3c-0.7-0.7-1.4-1.2-2.3-1.5l-0.2-0.1\r\n                                c-0.4-0.2-0.7-0.4-0.9-0.8c-0.2-0.4-0.3-0.8-0.3-1.2l-2-0.1l2,0c0-0.4,0.1-0.8,0.4-1.2c0.2-0.3,0.6-0.6,0.9-0.8l0.2-0.1\r\n                                c1.7-0.7,3.1-2.1,3.8-3.8c0.7-1.7,0.7-3.7,0-5.4c-0.7-1.7-2.1-3.1-3.8-3.8c-1.7-0.7-3.7-0.7-5.4,0l-0.4,0.2\r\n                                c-0.4,0.2-0.8,0.2-1.2,0.1c-0.3-0.1-0.6-0.2-0.8-0.4c-0.1-0.1-0.2-0.3-0.3-0.4c-0.3-0.3-0.5-0.7-0.6-1.1c-0.1-0.4-0.1-0.8,0.1-1.2\r\n                                l0.1-0.2c0,0,0,0,0,0c0.4-0.9,0.5-1.8,0.5-2.7c0-0.9-0.2-1.9-0.5-2.7c-0.4-0.9-0.9-1.6-1.5-2.3c-0.7-0.7-1.4-1.2-2.3-1.5\r\n                                c0,0,0,0,0,0c-0.9-0.4-1.8-0.5-2.7-0.5c-0.9,0-1.9,0.2-2.7,0.5c-0.9,0.4-1.6,0.9-2.3,1.5c-0.7,0.7-1.2,1.4-1.5,2.3c0,0,0,0,0,0\r\n                                l-0.1,0.2c-0.2,0.4-0.4,0.7-0.8,0.9c-0.4,0.2-0.8,0.3-1.2,0.3l0,0c-0.4,0-0.8-0.1-1.2-0.4c-0.3-0.2-0.6-0.6-0.8-0.9l-0.1-0.2\r\n                                c-0.7-1.7-2.1-3.1-3.8-3.8c-1.7-0.7-3.7-0.7-5.4,0c-1.7,0.7-3.1,2.1-3.8,3.8c-0.7,1.7-0.7,3.7,0,5.4l0.2,0.4\r\n                                c0.2,0.4,0.2,0.8,0.1,1.2c-0.1,0.3-0.2,0.6-0.4,0.8c-0.1,0.1-0.3,0.2-0.4,0.3c-0.3,0.3-0.7,0.5-1.1,0.6c-0.4,0.1-0.8,0.1-1.2-0.1\r\n                                l-0.2-0.1c-0.9-0.4-1.8-0.5-2.7-0.5c-0.9,0-1.9,0.2-2.7,0.5c-0.9,0.4-1.6,0.9-2.3,1.5c-0.7,0.7-1.2,1.4-1.5,2.3\r\n                                c-0.4,0.9-0.5,1.8-0.5,2.7c0,0.9,0.2,1.9,0.5,2.7c0.4,0.9,0.9,1.6,1.5,2.3c0.7,0.7,1.4,1.2,2.3,1.5c0,0,0,0,0,0l0.2,0.1\r\n                                c0.4,0.2,0.7,0.4,0.9,0.8c0.2,0.4,0.3,0.8,0.3,1.2c0,0,0,0.1,0,0.1c0,0.4-0.1,0.8-0.3,1.2c-0.2,0.4-0.5,0.6-0.9,0.8l-0.2,0.1\r\n                                c-1.7,0.7-3.1,2.1-3.8,3.8c-0.7,1.7-0.7,3.7,0,5.4c0.7,1.7,2.1,3.1,3.8,3.8c1.7,0.7,3.7,0.7,5.4,0l0.4-0.2c0.4-0.2,0.8-0.2,1.2-0.1\r\n                                c0.4,0.1,0.8,0.3,1.1,0.6l0,0c0.3,0.3,0.5,0.7,0.6,1.1c0.1,0.4,0.1,0.8-0.1,1.2l-0.1,0.2c0,0,0,0,0,0c-0.4,0.9-0.5,1.8-0.5,2.7\r\n                                c0,0.9,0.2,1.9,0.5,2.7c0.4,0.9,0.9,1.6,1.5,2.3c0.7,0.7,1.4,1.2,2.3,1.5c0.9,0.4,1.8,0.5,2.7,0.5c0.9,0,1.9-0.2,2.7-0.5\r\n                                c0.9-0.4,1.6-0.9,2.3-1.5c0.7-0.7,1.2-1.4,1.5-2.3L40.4,71z M36.6,69.4L36.6,69.4l-0.1,0.2l0,0c-0.2,0.4-0.4,0.7-0.7,1\r\n                                s-0.6,0.5-1,0.6s-0.8,0.2-1.2,0.2c-0.4,0-0.8-0.1-1.1-0.2l0,0c-0.4-0.2-0.7-0.4-1-0.7c-0.3-0.3-0.5-0.6-0.6-1s-0.2-0.8-0.2-1.2\r\n                                c0-0.4,0.1-0.8,0.2-1.1l0.1-0.2l0,0c0.5-1.1,0.6-2.4,0.3-3.6c-0.3-1.2-0.9-2.3-1.8-3.2c-0.9-0.8-1.9-1.4-3.1-1.6\r\n                                c-1.2-0.2-2.4-0.1-3.5,0.3l-0.4,0.2c-0.7,0.3-1.6,0.3-2.3,0c-0.7-0.3-1.3-0.9-1.6-1.6c-0.3-0.7-0.3-1.6,0-2.3\r\n                                c0.3-0.7,0.9-1.3,1.6-1.6l0.2-0.1c0,0,0,0,0,0c1.1-0.5,2.1-1.3,2.8-2.4c0.6-1,1-2.2,0.9-3.5c0-1.2-0.3-2.4-1-3.4\r\n                                c-0.7-1-1.6-1.9-2.8-2.4l0,0l-0.2-0.1c-0.4-0.2-0.7-0.4-1-0.7c-0.3-0.3-0.5-0.6-0.6-1s-0.2-0.8-0.2-1.2c0-0.4,0.1-0.8,0.2-1.1l0,0\r\n                                c0.2-0.4,0.4-0.7,0.7-1s0.6-0.5,1-0.6s0.8-0.2,1.2-0.2c0.4,0,0.8,0.1,1.1,0.2l0.2,0.1l0,0c1.1,0.5,2.4,0.6,3.6,0.3\r\n                                c1.1-0.2,2.1-0.7,2.9-1.5c0.2-0.1,0.3-0.2,0.5-0.4c0.8-0.9,1.4-2,1.7-3.1c0.2-1.2,0.1-2.4-0.3-3.5l0,0L31,27.7\r\n                                c-0.3-0.7-0.3-1.6,0-2.3c0.3-0.7,0.9-1.3,1.6-1.6s1.6-0.3,2.3,0c0.7,0.3,1.3,0.9,1.6,1.6l0.1,0.2l0,0c0.5,1.1,1.2,2.1,2.2,2.7\r\n                                c1,0.7,2.2,1,3.4,1.1c1.2,0,2.4-0.3,3.5-1c1-0.7,1.9-1.6,2.4-2.8l0,0l0.1-0.2c0.2-0.4,0.4-0.7,0.7-1c0.3-0.3,0.6-0.5,1-0.6\r\n                                s0.8-0.2,1.2-0.2c0.4,0,0.8,0.1,1.1,0.2l0,0c0.4,0.2,0.7,0.4,1,0.7c0.3,0.3,0.5,0.6,0.6,1c0.1,0.4,0.2,0.8,0.2,1.2\r\n                                c0,0.4-0.1,0.8-0.2,1.1L53.6,28l0,0c-0.5,1.1-0.6,2.4-0.3,3.6c0.2,1.1,0.7,2.1,1.5,2.9c0.1,0.2,0.2,0.3,0.4,0.5\r\n                                c0.9,0.8,2,1.4,3.1,1.7s2.4,0.1,3.5-0.3l0,0l0.4-0.2c0.7-0.3,1.6-0.3,2.3,0c0.7,0.3,1.3,0.9,1.6,1.6c0.3,0.7,0.3,1.6,0,2.3\r\n                                c-0.3,0.7-0.9,1.3-1.6,1.6l-0.2,0.1l0,0c-1.1,0.5-2.1,1.2-2.7,2.2c-0.7,1-1,2.2-1.1,3.4c0,1.2,0.3,2.4,1,3.5c0.7,1,1.6,1.9,2.8,2.4\r\n                                l0,0l0.2,0.1l0,0c0.4,0.2,0.7,0.4,1,0.7c0.3,0.3,0.5,0.6,0.6,1s0.2,0.8,0.2,1.2c0,0.4-0.1,0.8-0.2,1.1l0,0c-0.2,0.4-0.4,0.7-0.7,1\r\n                                c-0.3,0.3-0.6,0.5-1,0.6c-0.4,0.1-0.8,0.2-1.2,0.2c-0.4,0-0.8-0.1-1.1-0.2l0,0l-0.2-0.1l0,0c-1.1-0.5-2.4-0.6-3.6-0.3\r\n                                c-1.2,0.3-2.3,0.9-3.2,1.8c-0.8,0.9-1.4,1.9-1.6,3.1c-0.2,1.2-0.1,2.4,0.3,3.5l0,0l0.2,0.4c0.3,0.7,0.3,1.6,0,2.3\r\n                                c-0.3,0.7-0.9,1.3-1.6,1.6c-0.7,0.3-1.6,0.3-2.3,0c-0.7-0.3-1.3-0.9-1.6-1.6l-0.1-0.2c0,0,0,0,0,0c-0.5-1.1-1.3-2.1-2.4-2.8\r\n                                c-1-0.6-2.2-1-3.5-0.9c-1.2,0-2.4,0.3-3.4,1C38,67.3,37.1,68.2,36.6,69.4z\"/>\r\n                            <path class=\"fillPurple\" d=\"M4,47.2c0-19.5,14.9-35.6,33.8-37l-2.7,3.4c-0.7,0.9-0.5,2.2,0.3,2.9c0.9,0.7,2.2,0.5,2.9-0.3L44,8.9\r\n                                c0.7-0.9,0.5-2.2-0.3-2.9l-7.2-5.6c-0.9-0.7-2.2-0.5-2.9,0.3C33,1.7,33.1,2.9,34,3.6l3.2,2.5C16.4,7.9,0,25.6,0,47.2\r\n                                C0,68,15.1,85.1,34.8,88l0.8-4C17.8,81.5,4,66,4,47.2z\"/>\r\n                            <path class=\"fillPurple\" d=\"M84.5,47c0-20.7-15.3-37.7-35.2-40.6l-0.8,4c18,2.4,32,17.9,32,36.6c0,19.2-14.6,35.1-33.4,36.9l2.3-3\r\n                                c0.7-0.9,0.5-2.2-0.3-2.9c-0.9-0.7-2.2-0.5-2.9,0.3l-5.6,7.2c-0.7,0.9-0.5,2.2,0.3,2.9l7.2,5.6c0.9,0.7,2.2,0.5,2.9-0.3\r\n                                c0.7-0.9,0.5-2.2-0.3-2.9L47,88C68,86.2,84.6,68.5,84.5,47z\"/>\r\n                        </g>\r\n                        </svg>\r\n                    </div>\r\n                    <div class=\"leftSectIntro\">\r\n                        <div class=\"leftSectIntroTxt\">"
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0.leftSection : depth0)) != null ? stack1.intro : stack1), depth0)) != null ? stack1 : "")
    + "</div>\r\n                        <div id=\"leftSectAction\">"
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0.leftSection : depth0)) != null ? stack1.action : stack1), depth0)) != null ? stack1 : "")
    + "</div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"leftSectFooter\">\r\n                <div id=\"myDataMenu\" class=\"leftSectMenuToggle\">\r\n                    <div class=\"menuLabel\">"
    + ((stack1 = alias2(((stack1 = (depth0 != null ? depth0.leftSection : depth0)) != null ? stack1.menuLabel : stack1), depth0)) != null ? stack1 : "")
    + "</div>\r\n                    <div class=\"menuIcon\"><i class=\"fas fa-chevron-down\"></i></div>\r\n                    <div class=\"menuIcon display-no\"><i class=\"fas fa-chevron-up\"></i></div>\r\n                    <div class=\"menuIconNo display-no\"><i class=\"fas fa-times\"></i></div>\r\n                </div>\r\n                <div class=\"leftSectMenuItems display-no\">\r\n"
    + ((stack1 = helpers.each.call(alias1,((stack1 = (depth0 != null ? depth0.leftSection : depth0)) != null ? stack1.menuActions : stack1),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\r\n            </div>\r\n        </div>\r\n        <div id=\"leftSectionBottom\">\r\n            <div>\r\n                <?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n                <svg version=\"1.1\" id=\"Livello_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r\n                    viewBox=\"0 0 210 210\" style=\"enable-background:new 0 0 210 210;\" xml:space=\"preserve\">\r\n                <g>\r\n                    <path class=\"fillWhite\" d=\"M16.8,123c0-0.8-0.4-1.5-1.2-1.9c-0.8-0.5-2.1-0.9-3.9-1.3c-6-1.3-9-3.8-9-7.7c0-2.2,0.9-4.1,2.8-5.6\r\n                        c1.9-1.5,4.3-2.3,7.3-2.3c3.2,0,5.8,0.8,7.7,2.3c1.9,1.5,2.9,3.5,2.9,5.9h-6.8c0-1-0.3-1.8-0.9-2.4c-0.6-0.6-1.6-1-2.9-1\r\n                        c-1.1,0-2,0.3-2.6,0.8c-0.6,0.5-0.9,1.2-0.9,2c0,0.8,0.4,1.4,1.1,1.8c0.7,0.5,1.9,0.9,3.6,1.2c1.7,0.3,3.1,0.7,4.3,1.1\r\n                        c3.6,1.3,5.4,3.6,5.4,6.8c0,2.3-1,4.2-3,5.6c-2,1.4-4.5,2.1-7.7,2.1c-2.1,0-4-0.4-5.7-1.1c-1.6-0.8-2.9-1.8-3.9-3.1\r\n                        c-0.9-1.3-1.4-2.8-1.4-4.3h6.4c0.1,1.2,0.5,2.1,1.3,2.8c0.8,0.6,1.9,1,3.3,1c1.3,0,2.3-0.2,2.9-0.7C16.5,124.4,16.8,123.8,16.8,123\r\n                        z M42.5,130c-0.3-0.6-0.5-1.4-0.7-2.3c-1.6,1.8-3.8,2.7-6.4,2.7c-2.5,0-4.5-0.7-6.2-2.2c-1.6-1.4-2.4-3.3-2.4-5.4\r\n                        c0-2.7,1-4.8,3-6.2c2-1.4,4.9-2.2,8.6-2.2h3.1v-1.5c0-1.2-0.3-2.1-0.9-2.8c-0.6-0.7-1.5-1.1-2.8-1.1c-1.1,0-2,0.3-2.7,0.8\r\n                        c-0.6,0.5-1,1.3-1,2.2h-6.8c0-1.5,0.5-2.8,1.4-4.1s2.2-2.2,3.8-2.9c1.7-0.7,3.5-1.1,5.6-1.1c3.1,0,5.6,0.8,7.4,2.4\r\n                        c1.8,1.6,2.8,3.8,2.8,6.6v11c0,2.4,0.4,4.2,1,5.5v0.4H42.5z M36.9,125.3c1,0,1.9-0.2,2.8-0.7c0.8-0.5,1.5-1.1,1.9-1.8v-4.4H39\r\n                        c-3.4,0-5.2,1.2-5.4,3.5l0,0.4c0,0.8,0.3,1.5,0.9,2.1C35,125,35.8,125.3,36.9,125.3z M76.3,117.6c0,3.9-0.9,7-2.7,9.4\r\n                        c-1.8,2.3-4.2,3.5-7.2,3.5c-2.6,0-4.6-0.9-6.2-2.7v12h-6.8v-35.1h6.3l0.2,2.5c1.6-2,3.8-3,6.4-3c3.1,0,5.6,1.2,7.3,3.5\r\n                        c1.7,2.3,2.6,5.5,2.6,9.6V117.6z M69.6,117.1c0-2.4-0.4-4.2-1.3-5.5c-0.8-1.3-2-1.9-3.6-1.9c-2.1,0-3.6,0.8-4.4,2.4v10.4\r\n                        c0.8,1.7,2.3,2.5,4.4,2.5C67.9,125,69.6,122.4,69.6,117.1z M87.7,130h-6.8v-25.4h6.8V130z M80.5,98.1c0-1,0.3-1.9,1-2.5\r\n                        c0.7-0.7,1.6-1,2.8-1c1.2,0,2.1,0.3,2.8,1c0.7,0.7,1,1.5,1,2.5c0,1-0.4,1.9-1.1,2.5c-0.7,0.7-1.6,1-2.7,1c-1.1,0-2.1-0.3-2.8-1\r\n                        C80.9,100,80.5,99.1,80.5,98.1z M92.2,117.1c0-2.5,0.5-4.8,1.5-6.7c1-2,2.4-3.5,4.2-4.6c1.8-1.1,3.9-1.6,6.4-1.6\r\n                        c3.4,0,6.2,1,8.4,3.1c2.2,2.1,3.4,4.9,3.6,8.5l0,1.7c0,3.9-1.1,7-3.3,9.4c-2.2,2.3-5.1,3.5-8.7,3.5c-3.7,0-6.6-1.2-8.8-3.5\r\n                        c-2.2-2.3-3.3-5.5-3.3-9.6V117.1z M99,117.6c0,2.4,0.5,4.2,1.4,5.5c0.9,1.3,2.2,1.9,3.9,1.9c1.6,0,2.9-0.6,3.8-1.9\r\n                        c0.9-1.3,1.4-3.3,1.4-6c0-2.4-0.5-4.2-1.4-5.5c-0.9-1.3-2.2-1.9-3.9-1.9c-1.7,0-2.9,0.6-3.8,1.9C99.5,112.9,99,114.9,99,117.6z\r\n                        M127.1,109.9h3.6c1.7,0,3-0.4,3.8-1.3c0.8-0.9,1.2-2,1.2-3.4c0-1.4-0.4-2.4-1.2-3.2c-0.8-0.8-1.9-1.1-3.4-1.1\r\n                        c-1.3,0-2.4,0.4-3.3,1.1c-0.9,0.7-1.3,1.6-1.3,2.8h-6.8c0-1.8,0.5-3.4,1.4-4.8c1-1.4,2.3-2.5,4-3.3c1.7-0.8,3.6-1.2,5.7-1.2\r\n                        c3.6,0,6.4,0.9,8.5,2.6c2,1.7,3.1,4.1,3.1,7.1c0,1.6-0.5,3-1.4,4.3c-1,1.3-2.2,2.3-3.8,3c1.9,0.7,3.4,1.7,4.3,3.1\r\n                        c1,1.4,1.4,3,1.4,4.9c0,3-1.1,5.5-3.3,7.3c-2.2,1.8-5.1,2.7-8.8,2.7c-3.4,0-6.2-0.9-8.4-2.7c-2.2-1.8-3.2-4.2-3.2-7.1h6.8\r\n                        c0,1.3,0.5,2.3,1.4,3.1c1,0.8,2.2,1.2,3.6,1.2c1.6,0,2.9-0.4,3.8-1.3c0.9-0.9,1.4-2,1.4-3.4c0-3.4-1.9-5.1-5.6-5.1h-3.6V109.9z\r\n                        M166.1,95.5v5.6h-0.7c-3.1,0-5.5,0.8-7.4,2.4c-1.9,1.5-3,3.7-3.4,6.4c1.8-1.8,4.1-2.8,6.9-2.8c3,0,5.3,1.1,7.1,3.2\r\n                        s2.6,4.9,2.6,8.4c0,2.2-0.5,4.2-1.5,6c-1,1.8-2.3,3.2-4.1,4.2c-1.8,1-3.7,1.5-6,1.5c-3.6,0-6.5-1.2-8.7-3.8\r\n                        c-2.2-2.5-3.3-5.8-3.3-10v-2.4c0-3.7,0.7-7,2.1-9.8c1.4-2.8,3.4-5,6-6.6c2.6-1.6,5.7-2.4,9.1-2.4H166.1z M159.5,112.6\r\n                        c-1.1,0-2.1,0.3-3,0.9c-0.9,0.6-1.5,1.3-2,2.2v2.1c0,2.3,0.4,4,1.3,5.3c0.9,1.3,2.1,1.9,3.8,1.9c1.5,0,2.6-0.6,3.5-1.7\r\n                        c0.9-1.2,1.4-2.6,1.4-4.5c0-1.9-0.5-3.4-1.4-4.5C162.2,113.2,161,112.6,159.5,112.6z M176.3,113.2l2-17.3h19.1v5.6h-13.6l-0.8,7.3\r\n                        c1.6-0.9,3.3-1.3,5.1-1.3c3.2,0,5.8,1,7.6,3c1.8,2,2.8,4.8,2.8,8.5c0,2.2-0.5,4.2-1.4,5.9c-0.9,1.7-2.2,3.1-4,4.1\r\n                        c-1.7,1-3.8,1.4-6.1,1.4c-2.1,0-4-0.4-5.7-1.2c-1.8-0.8-3.2-2-4.2-3.5c-1-1.5-1.6-3.2-1.6-5.2h6.7c0.1,1.4,0.6,2.5,1.5,3.3\r\n                        c0.9,0.8,2,1.2,3.4,1.2c1.5,0,2.7-0.5,3.5-1.6c0.8-1.1,1.2-2.7,1.2-4.7c0-1.9-0.5-3.4-1.4-4.5c-1-1-2.3-1.5-4.1-1.5\r\n                        c-1.6,0-2.9,0.4-3.9,1.3l-0.7,0.6L176.3,113.2z\"/>\r\n                    <g>\r\n                        <defs>\r\n                            <rect id=\"SVGID_1_\" x=\"69\" y=\"0\" width=\"62\" height=\"62\"/>\r\n                        </defs>\r\n                        <clipPath id=\"SVGID_2_\">\r\n                            <use xlink:href=\"#SVGID_1_\"  style=\"overflow:visible;\"/>\r\n                        </clipPath>\r\n                        <g class=\"clipPath\">\r\n                            <path class=\"fillWhite\" d=\"M100,58.4c15.1,0,27.4-12.3,27.4-27.4S115.2,3.6,100,3.6C84.9,3.6,72.6,15.9,72.6,31S84.9,58.4,100,58.4z\"/>\r\n                            <path class=\"fillWhite\" d=\"M100,62c-17.1,0-31-13.9-31-31c0-17.1,13.9-31,31-31c17.1,0,31,13.9,31,31C131.1,48.2,117.1,62,100,62z\r\n                                M100,4.5C85.4,4.5,73.5,16.4,73.5,31c0,14.6,11.9,26.4,26.5,26.4c14.6,0,26.4-11.9,26.4-26.4S114.6,4.5,100,4.5z\"/>\r\n                            <path class=\"fillPurple\" d=\"M100,4.5C85.4,4.5,73.5,16.4,73.5,31c0,14.6,11.9,26.5,26.5,26.5c14.6,0,26.5-11.9,26.5-26.5\r\n                                C126.5,16.4,114.6,4.5,100,4.5z M95.1,9.6c-0.3,0.6-0.5,1.4-0.5,2.2c0,3,2.4,5.4,5.4,5.4c3,0,5.4-2.4,5.4-5.4\r\n                                c0-0.8-0.2-1.6-0.5-2.2c5.9,1.4,10.9,5.1,14,10.2H81.2C84.2,14.7,89.2,11,95.1,9.6z M78.1,31c0-2.6,0.5-5,1.3-7.4h14.5\r\n                                c0.6,0,1,0.3,1.2,0.8c1,2.3,2.4,9.6-6.7,25.1C82.2,45.7,78.1,38.9,78.1,31z M100,52.9c-2.8,0-5.5-0.5-8-1.5\r\n                                c0-0.1,5.4-12.6,8-12.6c2.5,0,7.9,12.5,8,12.6C105.5,52.4,102.8,52.9,100,52.9z M111.7,49.6c-9.2-15.8-7.6-23-6.7-25.2\r\n                                c0.2-0.5,0.6-0.7,1.1-0.7h14.6c0.8,2.3,1.3,4.8,1.3,7.4C121.9,38.9,117.9,45.7,111.7,49.6z\"/>\r\n                        </g>\r\n                    </g>\r\n                </g>\r\n                </svg>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    \r\n    <div id=\"rightSection\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.tiles : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\r\n</div>";
},"useData":true});
})();