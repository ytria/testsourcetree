var allJobs = [
	{
		"id": "7257619582290806892",
		"ajlid": "03B",
		"link": "Mainframe/001-ViewLicensedUsers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-20T18:46:29Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-20T18:46:29Z"
		},
		"categories": [
			"3488082250975908789",
			"6064935782377127699"
		],
		"label": "View licensed users",
		"keywords": "assigned licenses,billing,licenses,licenses consumed,service plans,users,identity",
		"description": "Get a list of users and their properties, including sign-in blocked status, who have MS 365 licenses assigned to them."
	},
	{
		"id": "104722653360166356",
		"ajlid": "03E",
		"link": "Mainframe/002-ViewUnLicensedUsers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-21T17:59:36Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-21T17:59:36Z"
		},
		"categories": [
			"10772695832097869471",
			"3488082250975908789",
			"6064935782377127699"
		],
		"label": "View unlicensed active users",
		"keywords": "guests,identity,licenses,unlicensed,users,rooms,equipment,shared mailboxes",
		"description": "Get a list of users and their properties without any assigned Microsoft 365 licenses. This includes guests, shared mailboxes, rooms and equipment."
	},
	{
		"id": "2264439221327425664",
		"ajlid": "03A",
		"link": "Mainframe/003-ViewGuestUsers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-22T19:20:55Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-22T19:20:55Z"
		},
		"categories": [
			"6064935782377127699",
			"7429392130877336095"
		],
		"label": "View guest users",
		"keywords": "guests,users",
		"description": "List guests and their user properties."
	},
	{
		"id": "7870796044274272333",
		"ajlid": "03D",
		"link": "Mainframe/004-ViewSign-inAllowedUsers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-22T19:33:05Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-22T19:33:05Z"
		},
		"categories": [
			"10772695832097869471",
			"3488082250975908789",
			"6064935782377127699"
		],
		"label": "View sign-in allowed users",
		"keywords": "identity,licenses,new users,sign-ins,users",
		"description": "List users and their properties who are set to be able to sign in to Microsoft 365."
	},
	{
		"id": "3867235454641864467",
		"ajlid": "03C",
		"link": "Mainframe/005-ViewSign-inBlockedUsers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-22T19:43:38Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-22T19:43:38Z"
		},
		"categories": [
			"14138584524611318201",
			"3488082250975908789",
			"6064935782377127699"
		],
		"label": "View sign-in blocked users",
		"keywords": "blocked sign-in,identity,licenses,remove licenses,sign-ins,users",
		"description": "List users who are blocked from signing in to Microsoft 365."
	},
	{
		"id": "7911760541431253905",
		"ajlid": "085",
		"link": "Mainframe/006-Launch_ProvisionedServicePlan.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-12T19:01:51Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-12T19:01:51Z"
		},
		"categories": [
			"3488082250975908789"
		],
		"label": "View pending service plans for selected users",
		"keywords": "licenses,service plans",
		"description": "Identify and list users who are assigned licenses that include service plans with a pending status."
	},
	{
		"id": "1700645363475389495",
		"ajlid": "043",
		"link": "Mainframe/007-ViewAllGroupsandTeams.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T17:55:22Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "View groups and Teams",
		"keywords": "group document library,group licensing,groups,Teams",
		"description": "This job list all groups and Teams .From there, you can edit groups' properties, and perform various other identity management actions."
	},
	{
		"id": "5565224454516245864",
		"ajlid": "066",
		"link": "Mainframe/008-DeleteUsers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-pen-nib",
		"action": "Edit",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-19T17:48:11Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"14138584524611318201"
		],
		"label": "Delete users",
		"keywords": "identity,inactive users,delete users",
		"description": "Delete selected users. You can restore deleted users within 30 days from the recycle bin."
	},
	{
		"id": "2738859910205046258",
		"ajlid": "069",
		"link": "Mainframe/009-DeleteGroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-pen-nib",
		"action": "Edit",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-19T17:40:59Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "Delete groups",
		"keywords": "groups,delete,inactive,unused,clean",
		"description": "Delete selected groups, including Teams, distribution groups, security groups and email-enabled security groups. Office 365 type groups (Teams) will be available to restore from the recycle bin. All other groups are deleted permanently."
	},
	{
		"id": "8356139538914761814",
		"ajlid": "009",
		"link": "Mainframe/010-ExportAllSelGroupContent.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-09T22:15:53Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-09T22:15:53Z"
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682",
			"8813166633134608907"
		],
		"label": "Export content of selected groups",
		"keywords": "attachments,back up,documents,download,files,membership,owners,Teams,reports,Excel,XML,channels,calendar events,conversations,posts,chats",
		"description": "Select groups or Teams, and download their document library files and group conversation attachments to your local machine. Export Excel or XML reports on group members and owners, Team channels and chats, group conversation threads and calendar events, and document library file information."
	},
	{
		"id": "1086712530598530936",
		"ajlid": "00A",
		"link": "Mainframe/011-ExportAllSelUserContents.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-09T22:18:27Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-09T22:18:27Z"
		},
		"categories": [
			"14138584524611318201",
			"3488082250975908789",
			"5616715603626924449",
			"7429392130877336095",
			"8813166633134608907"
		],
		"label": "Export content of selected users",
		"keywords": "back up,document library,download,mail,messages,OneDrive,reports,rules,group memberships,contacts,administrator roles",
		"description": "Select users and download their OneDrive files to your local machine. This job also exports Excel or XML reports on their group memberships, roles, assigned licenses, personal contacts, messages and inbox rules, and OneDrive file information."
	},
	{
		"id": "7457446328990984336",
		"ajlid": "041",
		"link": "Mainframe/012-ViewActiveUsers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T20:28:24Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"10772695832097869471",
			"14138584524611318201",
			"2841833117921732997",
			"7429392130877336095",
			"8821249389923942631"
		],
		"label": "View active users",
		"keywords": "creation,delete,equipment,identity,reset password,revoke access,rooms,shared mailboxes,users",
		"description": "List all users and their properties, including licensed and unlicensed users, rooms, equipment, and shared mailboxes. From there, you can edit users' properties, reset passwords, and perform various other identity management actions."
	},
	{
		"id": "2030932141095536788",
		"ajlid": "020",
		"link": "Mainframe/013-Mailattachmentsbynameortype.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T21:28:49Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T21:28:49Z"
		},
		"categories": [
			"1905368263408905136",
			"4427780615769315441",
			"7429392130877336095"
		],
		"label": "Search all mailboxes and find attachments by name or type",
		"keywords": "attachments,mail,emails,find,search,extension,files",
		"description": "Search all users' mailboxes and lists attachments that match a specified file name and extension type."
	},
	{
		"id": "5401976419069577013",
		"ajlid": "042",
		"link": "Mainframe/013-ViewDeletedUsers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T20:20:35Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"14138584524611318201"
		],
		"label": "View deleted users",
		"keywords": "identity,users,restore,permanently delete",
		"description": "List users deleted in the last 30 days. From there you can restore or delete them permanently."
	},
	{
		"id": "1809609843064631748",
		"ajlid": "088",
		"link": "Mainframe/014-ViewDeletedGroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T20:25:35Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "View deleted groups",
		"keywords": "groups,deleted,Office 365 group,permanently delete,recycle bin,restore,Teams",
		"description": "List groups deleted in the last 30 days. From there you can restore or delete them permanently."
	},
	{
		"id": "6535161185219047288",
		"ajlid": "048",
		"link": "Mainframe/015-ViewArchivedTeams.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T16:10:53Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "View Archived Teams",
		"keywords": "groups,teams,management",
		"description": "List Teams that have been archived. You may restore them from this view."
	},
	{
		"id": "2905858807429086753",
		"ajlid": "079",
		"link": "Mainframe/016-ViewByStatus.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-04-30T14:33:52Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"3488082250975908789",
			"6064935782377127699"
		],
		"label": "View by status: guest, sign-in, unlicensed",
		"keywords": "blocked sign-in,guests,licenses,unlicensed,users",
		"description": "Filter down to a list of guests users, users allowed/blocked from signing-in, or licensed/unlicensed users."
	},
	{
		"id": "7264706992551547269",
		"ajlid": "084",
		"link": "Mainframe/017-GROUPS-GetGroupMembershipsof1Group.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-12T18:48:43Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-12T18:48:43Z"
		},
		"categories": [
			"7899644567658873682"
		],
		"label": "Get memberships of a selected group",
		"keywords": "groups,members,nested groups,memberships",
		"description": "List the groups that your selected group (nested) is a member of."
	},
	{
		"id": "3440496584799702697",
		"ajlid": "067",
		"link": "Mainframe/018-RestoreDeletedUsers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-pen-nib",
		"action": "Edit",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T14:29:11Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"10772695832097869471",
			"14138584524611318201",
			"7429392130877336095"
		],
		"label": "Restore deleted users",
		"keywords": "delete users,deleted,recycle bin,restore",
		"description": "Restore a selection of user accounts deleted in the last 30 days and which can still be restored."
	},
	{
		"id": "5062885639700699011",
		"ajlid": "068",
		"link": "Mainframe/019-RestoreDeletedGroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-pen-nib",
		"action": "Edit",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T21:31:13Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"7899644567658873682"
		],
		"label": "Restore deleted groups",
		"keywords": "groups,restore,Teams,deleted,recycle bin",
		"description": "Restore a selection of groups or Teams deleted in the last 30 days and which can still be restored."
	},
	{
		"id": "7541541289678205761",
		"ajlid": "01F",
		"link": "Mainframe/020-LicensesAssignedToDisabledAccounts.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T22:34:04Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T22:34:04Z"
		},
		"categories": [
			"3488082250975908789"
		],
		"label": "Licenses of sign-in blocked users",
		"keywords": "blocked sign-in,licenses,unused licenses",
		"description": "List users whose sign-in is blocked and who also have assigned licenses. You're then able to remove licenses from users within the resulting window."
	},
	{
		"id": "3529183581536109787",
		"ajlid": "080",
		"link": "Mainframe/021-removelicensefromblockedusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-pen-nib",
		"action": "Edit",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-07-17T19:39:41Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-07-17T19:39:41Z"
		},
		"categories": [
			"14138584524611318201",
			"3488082250975908789",
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "Remove all licenses from users with blocked sign-ins",
		"keywords": "blocked sign-in,licenses,remove licenses,unused licenses,group licensing",
		"description": "List assigned licenses of users whose sign-in is blocked and choose to remove their licenses. If licenses are assigned via a group, users will be also removed from those groups."
	},
	{
		"id": "3899399209312168676",
		"ajlid": "070",
		"link": "Mainframe/022-findEmptyGroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-04-17T21:21:12Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"7899644567658873682"
		],
		"label": "Find ownerless/memberless groups",
		"keywords": "groups,memberless,members,ownerless,owners,Teams",
		"description": "List groups without owners or without members or neither."
	},
	{
		"id": "3939996043270697214",
		"ajlid": "015",
		"link": "Mainframe/023-GroupCreationReport.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-21T20:25:48Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-21T20:25:48Z"
		},
		"categories": [
			"5156495196829516889",
			"7899644567658873682"
		],
		"label": "View groups created within a date range",
		"keywords": "groups,new groups,creation date,old groups",
		"description": "List groups with a creation date that falls between the dates (inclusively) you specify."
	},
	{
		"id": "8665320329597988641",
		"ajlid": "016",
		"link": "Mainframe/024-GroupsAboutToExpire.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-21T20:40:29Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-21T20:40:29Z"
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "Find groups about to expire",
		"keywords": "groups,Teams,expiring,expiration,Office 365",
		"description": "List Office 365 groups and Teams that are set to expire within a specific date range."
	},
	{
		"id": "8552312311351099187",
		"ajlid": "010",
		"link": "Mainframe/025-findOwnerlessgroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T21:53:41Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T21:53:41Z"
		},
		"categories": [
			"11566107507702475288",
			"5616715603626924449",
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "View ownerless groups",
		"keywords": "ownerless,owners,no owners,group access,orphan",
		"description": "List groups without assigned owners. From there, you can add new owners or promote members to those groups to ensure a healthy collaborative environment."
	},
	{
		"id": "5786680238342006058",
		"ajlid": "011",
		"link": "Mainframe/026-findMemberlessgroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T21:57:48Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T21:57:48Z"
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "Find memberless groups",
		"keywords": "groups,members,Teams,governance",
		"description": "List groups without any members. This includes Office 365 groups, Teams, distribution groups, security groups and email-enabled security groups."
	},
	{
		"id": "6969039764646593972",
		"ajlid": "012",
		"link": "Mainframe/027-findOwnerlessANDmemberlessgroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T22:01:05Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T22:01:05Z"
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "Find ownerless AND memberless groups",
		"keywords": "groups,members,Teams,governance",
		"description": "List groups that have no owners nor members. This includes Office 365 groups, Teams, distribution groups, security groups and email-enabled security groups. "
	},
	{
		"id": "3059203519754009248",
		"ajlid": "02E",
		"link": "Mainframe/030-ListOfUsersByAssignedAndUnassignedLicenses.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T22:36:27Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T22:36:27Z"
		},
		"categories": [
			"3488082250975908789"
		],
		"label": "Users by assigned & unassigned licenses",
		"keywords": "assigned licenses,licenses",
		"description": "Categorize active users by their assigned and unassigned Office 365 licenses."
	},
	{
		"id": "6072508968464007147",
		"ajlid": "013",
		"link": "Mainframe/031-FindServicePlansAssignedBefore.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-20T17:50:41Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-20T17:50:41Z"
		},
		"categories": [
			"10772695832097869471",
			"3488082250975908789",
			"7429392130877336095"
		],
		"label": "Find users with a service plan assigned before a specific date",
		"keywords": "blocked sign-in,licenses,services,unused licenses,plans,access",
		"description": "List users who were assigned a selected service plan before a specific date."
	},
	{
		"id": "8960510429228453516",
		"ajlid": "032",
		"link": "Mainframe/035-USERS-DuplicatePlans.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T22:39:42Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T22:39:42Z"
		},
		"categories": [
			"3488082250975908789"
		],
		"label": "Users with duplicate plans",
		"keywords": "licenses,licenses consumed,service plans",
		"description": "List users with licenses that have overlapping service plans."
	},
	{
		"id": "2266316360254818047",
		"ajlid": "07D",
		"link": "Mainframe/036-USERS-Unlicensed-users.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-06-10T20:34:21Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"10772695832097869471",
			"14138584524611318201",
			"3488082250975908789"
		],
		"label": "View unlicensed users",
		"keywords": "licenses,unlicensed,users",
		"description": "List users without any licenses assigned to them."
	},
	{
		"id": "6661162143658649358",
		"ajlid": "030",
		"link": "Mainframe/040-UsersLocationForLicenseUsage.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T19:54:57Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T19:54:57Z"
		},
		"categories": [
			"3488082250975908789"
		],
		"label": "Users by license usage location",
		"keywords": "licenses,location,users",
		"description": "List users grouped by their license usage location."
	},
	{
		"id": "821064819408060730",
		"ajlid": "081",
		"link": "Mainframe/041-DeactivateTheUser.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-09T22:23:50Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-09T22:23:50Z"
		},
		"categories": [
			"14138584524611318201",
			"6064935782377127699",
			"7429392130877336095"
		],
		"label": "Deactivate selected users",
		"keywords": "access,delete users,licenses,remove licenses,reset password,revoke access,block sign-in,accounts,remove",
		"description": "Apply various offboarding options to selected users: block sign-in, revoke access, reset password, remove licenses and delete accounts."
	},
	{
		"id": "4041578171420124102",
		"ajlid": "064",
		"link": "Mainframe/042-Userswithoutmanagers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T18:17:47Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"14138584524611318201"
		],
		"label": "Users without managers",
		"keywords": "manager,users,access",
		"description": "List users without an assigned manager. From there you can set a manager to ensure continued access to users' content."
	},
	{
		"id": "9006776657582824963",
        "ajlid": "065",
        "isSandbox": "X",
		"link": "Mainframe/043-Userswithmanagers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T18:18:14Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"14138584524611318201"
		],
		"label": "View users with managers",
		"keywords": "manager,security,users",
		"description": "List users who have a set manager."
	},
	{
		"id": "773151617874560948",
		"ajlid": "006",
		"link": "Mainframe/047-EmailsSentWithinDateAndTimeRange.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-07-01T02:47:43Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-07-01T02:47:43Z"
		},
		"categories": [
			"1905368263408905136",
			"4427780615769315441",
			"6064935782377127699",
			"8500565687032910346"
		],
		"label": "View emails sent from select mailbox owners within a date range",
		"keywords": "ediscovery,email,email recipient,email sender,email subject,find,mail,messages,search,sent",
		"description": "Retrieve email messages sent from selected mailboxes within a specified date range."
	},
	{
		"id": "4752585155869037740",
		"ajlid": "02A",
		"link": "Mainframe/048-ShowMessageRulesForwardingEmailsToOutsideAddresses.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T20:30:37Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T20:30:37Z"
		},
		"categories": [
			"4427780615769315441"
		],
		"label": "Show external email forwarding rules for all users",
		"keywords": "security,rules,mail,inbox,forward,external",
		"description": "List users and their inbox rules forwarding mail to external addresses."
	},
	{
		"id": "4204863899959131698",
		"ajlid": "021",
		"link": "Mainframe/049-Mailattachmentsbynameortypeforselectedusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-09T22:12:03Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-09T22:12:03Z"
		},
		"categories": [
			"1905368263408905136",
			"4427780615769315441",
			"7429392130877336095"
		],
		"label": "Search selected users' mailboxes and find attachments by name or type",
		"keywords": "attachments,mail,emails,find,search,extension,files",
		"description": "Search mailboxes of selected users and list attachments that match a specified file name and extension type."
	},
	{
		"id": "8527391083536078314",
		"ajlid": "01A",
		"link": "Mainframe/050-sharedFilesByTypeforgroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-04-02T20:57:47Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-04-02T20:57:47Z"
		},
		"categories": [
			"11566107507702475288",
			"5616715603626924449",
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "Groups/Teams with shared files (anonymous, guests, etc)",
		"keywords": "documents,files,anonymous share links,shared with guests,organization share links",
		"description": "List Office 365 groups/Teams and their library files shared anonymously, directly with guests, or within the organization."
	},
	{
		"id": "4423516364106328650",
		"ajlid": "039",
		"link": "Mainframe/051-sharedFilesByType.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-03-24T21:50:39Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-03-24T21:50:39Z"
		},
		"categories": [
			"5616715603626924449",
			"6064935782377127699"
		],
		"label": "Users with shared files (anonymous, guests, etc)",
		"keywords": "documents,files,anonymous share links,shared with guests,organization share links",
		"description": "List users and their OneDrive files shared anonymously, directly with guests, or within the organization."
	},
	{
		"id": "2853015349336161525",
		"ajlid": "025",
		"link": "Mainframe/052-sharedFilesByTypeforsites.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-04-02T21:14:34Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-04-02T21:14:34Z"
		},
		"categories": [
			"6064935782377127699"
		],
		"label": "SharePoint Sites with shared files (anonymous, guests, etc)",
		"keywords": "anonymous share links,documents,files,organization share links,shared with guests",
		"description": "List SharePoint sites and their library files shared anonymously, directly with guests, or within the organization."
	},
	{
		"id": "1173496867366146470",
		"ajlid": "00E",
		"link": "Mainframe/053-SiteFilesCreatedInTheLastXDays.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T21:42:49Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T21:42:49Z"
		},
		"categories": [
			"8500565687032910346"
		],
		"label": "SharePoint Sites files created within the date/time range",
		"keywords": "sites,search,find,ediscovery,document,library,files,creation",
		"description": "List SharePoint sites files created within the date/time range."
	},
	{
		"id": "7011309285504030755",
		"ajlid": "00D",
		"link": "Mainframe/054-GroupFilesCreatedInTheLastXDays.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T21:38:43Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T21:38:43Z"
		},
		"categories": [
			"8500565687032910346"
		],
		"label": "Group files created within the date/time range",
		"keywords": "groups,search,find,ediscovery,document,library,files,creation",
		"description": "List groups files created within the date/time range."
	},
	{
		"id": "7457559650587562109",
		"ajlid": "00C",
		"link": "Mainframe/055-UserFilesCreatedInTheLastXDays.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T21:35:20Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T21:35:20Z"
		},
		"categories": [
			"5616715603626924449",
			"7429392130877336095",
			"8500565687032910346"
		],
		"label": "Find OneDrive files created within a date range",
		"keywords": "search,find,ediscovery,OneDrive,creation,files,documents",
		"description": "Search users' OneDrive and list files created within a specific date range."
	},
	{
		"id": "5996873872175148791",
		"ajlid": "024",
		"link": "Mainframe/057-Filequerybynameortypeforselectedusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-09T22:05:34Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-09T22:05:34Z"
		},
		"categories": [
			"5616715603626924449",
			"7429392130877336095",
			"8500565687032910346"
		],
		"label": "Find OneDrive files by name or type for selected users",
		"keywords": "extension,files,find,OneDrive,search,file name",
		"description": "Search selected users' OneDrive for files that match a specified name and extension type."
	},
	{
		"id": "8306650958620893332",
		"ajlid": "023",
		"link": "Mainframe/058-Filequerybynameortypeforselectedsites.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-14T23:17:50Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-14T23:17:50Z"
		},
		"categories": [
			"11566107507702475288",
			"5616715603626924449",
			"7899644567658873682",
			"8500565687032910346"
		],
		"label": "Find files by name or type for selected sites",
		"keywords": "extension,files,find,search,query,file",
		"description": "Search selected SharePoint site document libraries for files that match a specified name and extension type."
	},
	{
		"id": "5097903731696336986",
		"ajlid": "014",
		"link": "Mainframe/060-Finduserswithoutsign-insinlastxdays.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-04-15T21:10:58Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-04-15T21:10:58Z"
		},
		"categories": [
			"6064935782377127699"
		],
		"label": "List users with & without recent sign-in activity",
		"keywords": "activity,sign-ins,inactive users",
		"description": "List users sign-ins activity for a specific period in the last 30 days."
	},
	{
		"id": "1353085432325879739",
		"ajlid": "083",
		"link": "Mainframe/061-GROUPS-OwnersNotMembers.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-pen-nib",
		"action": "Edit",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-09T22:32:29Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "Add non-member owners as members for selected groups or Teams",
		"keywords": "groups,members,memberships,owners,Teams",
		"description": "Ensure the current owners of the groups or Teams you select are also members to ensure access to all Microsoft services. If they are not already members, they are added to their relative groups."
	},
	{
		"id": "3890921911533785251",
		"ajlid": "08D",
		"link": "Mainframe/062-Showgroupsites.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-11-03T01:15:01Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"13667317956576447761",
			"16652443265771410676"
		],
		"label": "View group sites",
		"keywords": "groups,quota,SharePoint,sites,storage,Teams,subsites",
		"description": "List SharePoint sites and subsites of every group. From this view, you can access site information like usage quotas, lists, document library files, etc."
	},
	{
		"id": "969580374025530372",
		"ajlid": "08E",
		"link": "Mainframe/063-Showgroupsitelists.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-11-02T23:59:32Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"13667317956576447761"
		],
		"label": "View lists of all group sites",
		"keywords": "groups,SharePoint,sites,lists",
		"description": "List SharePoint list information of all group sites and subsites. In this view, you can access list item information and list column information."
	},
	{
		"id": "7107469906614874855",
		"ajlid": "08F",
		"link": "Mainframe/064-Showgroupsitelistitems.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-11-03T14:34:47Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"16652443265771410676"
		],
		"label": "View list items of all group site lists",
		"keywords": "groups,list item,SharePoint",
		"description": "List every list item for every group SharePoint site list. Display groups' site list item information like last modified by, creation date, checked out to, version, retention label, etc."
	},
	{
		"id": "5189207880743867353",
		"ajlid": "090",
		"link": "Mainframe/065-Showgroupsitelistcolumns.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-11-03T15:16:10Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"16652443265771410676"
		],
		"label": "View list columns of all group sites",
		"keywords": "groups,SharePoint,list column",
		"description": "List every list column and its information of all SharePoint site lists. Get list column information of group sites like if columns are hidden, text type, calculated formulas, etc."
	},
	{
		"id": "4439188221827398782",
		"ajlid": "086",
		"link": "Mainframe/066-List owned teams for selected users.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-12T19:15:33Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-12T19:15:33Z"
		},
		"categories": [
			"11566107507702475288",
			"7429392130877336095"
		],
		"label": "View owned Teams for selected users",
		"keywords": "ownership,Teams",
		"description": "Identify and list Teams owned for each user you select."
	},
	{
		"id": "3882961876057753449",
		"ajlid": "082",
		"link": "Mainframe/066-adduserasowner.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-09T22:27:26Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "Add selected users as owners to groups",
		"keywords": "groups,membership,Office 365,security,Teams,ownership,distribution",
		"description": "Add selected users as owners to all the groups in your tenant. Applicable groups types include: distribution groups, Office 365 groups or email-enabled security groups."
	},
	{
		"id": "4721204221328784127",
		"ajlid": "087",
		"link": "Mainframe/067-ListTeamsOfSelectedUsers.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-12T19:26:23Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-12T19:26:23Z"
		},
		"categories": [
			"11566107507702475288",
			"7429392130877336095"
		],
		"label": "View Teams of selected users",
		"keywords": "membership,Teams",
		"description": "List all the Teams each selected user belongs to."
	},
	{
		"id": "1629290364338764862",
		"ajlid": "00F",
		"link": "Mainframe/068-Findlastdategroupswereemailed.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T21:45:19Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T21:45:19Z"
		},
		"categories": [
			"11566107507702475288",
			"1905368263408905136",
			"4427780615769315441",
			"7899644567658873682"
		],
		"label": "Find the last time groups were emailed",
		"keywords": "inactive,Teams,distribution lists,distribution groups,last,unused groups",
		"description": "Search members' mailboxes to find the last time selected groups were sent an email. Select groups from distribution groups, Teams, Office 365 groups or email-enabled security groups."
	},
	{
		"id": "8454248739198737054",
		"ajlid": "018",
		"link": "Mainframe/069-ListOfGroupsWithExternalMembers.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-07-03T20:20:09Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-07-03T20:20:09Z"
		},
		"categories": [
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "Groups with guest members",
		"keywords": "external,groups,guest,security,Teams,guests",
		"description": "List groups/Teams with members who are guests (external)."
	},
	{
		"id": "2821115427451098841",
		"ajlid": "002",
		"link": "Mainframe/070-chartRolesByUserCount.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-chart-pie",
		"action": "Chart",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T20:36:13Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T20:36:13Z"
		},
		"categories": [
			"16928775336606739569"
		],
		"label": "Chart admin roles by user count",
		"keywords": "administration roles,roles,global admin",
		"description": "Create a chart of user count based on the assigned administration role."
	},
	{
		"id": "3539006236566682042",
		"ajlid": "00B",
		"link": "Mainframe/071-ExportRolesByUser.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-06-09T00:30:55Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-06-09T00:30:55Z"
		},
		"categories": [
			"16928775336606739569"
		],
		"label": "Export admin roles by users",
		"keywords": "administration roles,users",
		"description": "Export a list of directory roles by users."
	},
	{
		"id": "4794608809354155843",
		"ajlid": "05A",
		"link": "Mainframe/072-Allauditlogentries.xml",
		"source": "system",
		"family": "Tenant",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T16:53:44Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-27T16:53:44Z"
		},
		"categories": [
			"11566107507702475288",
			"6064935782377127699",
			"7429392130877336095",
			"7899644567658873682"
		],
		"label": "View all Admin Audit Log entries",
		"keywords": "creation,audit logs,changes,user updates,account deletions,addition",
		"description": "List all available event entries from the Admin Audit Log. You can find who initiated changes to users' information, group and Teams memberships, and other areas of interest."
	},
	{
		"id": "3758580241703374535",
		"ajlid": "05B",
		"link": "Mainframe/073-Allsign-ins.xml",
		"source": "system",
		"family": "Tenant",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T16:55:23Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-27T16:55:23Z"
		},
		"categories": [
			"14138584524611318201",
			"6064935782377127699",
			"7429392130877336095"
		],
		"label": "View all sign-in activity",
		"keywords": "access,location,sign-ins,logins",
		"description": "List all available sign-in activity entries in Azure Active Directory. You can see who has attempted to sign in, the frequency of sign in attempts, sign-in location and more."
	},
	{
		"id": "8780924328391642885",
		"ajlid": "05C",
		"link": "Mainframe/074-Alladministratorroles.xml",
		"source": "system",
		"family": "Tenant",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T21:02:02Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-27T21:02:02Z"
		},
		"categories": [
			"6064935782377127699",
			"7429392130877336095"
		],
		"label": "View all administrator roles",
		"keywords": "administration roles,administrator roles,global admin,directory roles,help desk",
		"description": "List all administrator roles in Azure Active Directory and their assigned users. From there you can add or remove assignments."
	},
	{
		"id": "204950374519851409",
		"ajlid": "001",
		"link": "Mainframe/075-ChartlGlobalLicenseConsumption.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-chart-pie",
		"action": "Chart",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T20:24:56Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T20:24:56Z"
		},
		"categories": [
			"3488082250975908789",
			"7429392130877336095"
		],
		"label": "Chart of global license consumption",
		"keywords": "chart,licenses consumed,assigned licenses,available licenses",
		"description": "Show chart of global license consumption."
	},
	{
		"id": "681359276046891522",
		"ajlid": "031",
		"link": "Mainframe/076-UsersForSelectedLicenses.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-14T23:22:02Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-14T23:22:02Z"
		},
		"categories": [
			"10772695832097869471",
			"14138584524611318201",
			"3488082250975908789",
			"7429392130877336095"
		],
		"label": "View users with selected licenses.",
		"keywords": "assigned licenses,inactive users,licenses,remove licenses",
		"description": "Lists users who have selected licenses assigned. From there, you can remove or assign new licenses to those users."
	},
	{
		"id": "7794926781723961279",
		"ajlid": "08C",
		"link": "Mainframe/078-Vieweventsforallgroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-11-02T23:34:35Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"4861527419968962856",
			"7899644567658873682"
		],
		"label": "View calender events of all groups",
		"keywords": "attachments,calendar events,ediscovery,groups",
		"description": "List every group's calendar events and event information. In this view, you can also see attachment information and delete attachments."
	},
	{
		"id": "5475589725395502153",
		"ajlid": "08B",
		"link": "Mainframe/079-Viewfilesofallgroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-11-02T23:25:41Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"5616715603626924449",
			"7899644567658873682"
		],
		"label": "View files of all groups",
		"keywords": "documents,files,group document library,groups,shared,SharePoint,sharing,folders,permissions",
		"description": "List documents and folders in a hierarchical view for every group and Team document library. From this view, you can upload files or delete them, and you can see file information like who last modified it, the creation date, if it was shared and how it was shared and with whom. You can also change or remove file permissions."
	},
	{
		"id": "7120513323686138704",
		"ajlid": "02D",
		"link": "Mainframe/080-tracemailsinusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-03-20T22:12:54Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-03-20T22:12:54Z"
		},
		"categories": [
			"4427780615769315441",
			"6064935782377127699",
			"8500565687032910346"
		],
		"label": "Trace email using subject, sender and recipient info",
		"keywords": "ediscovery,email,mail,search,email subject,email sender,email recipient",
		"description": "Search all mailboxes for a string in the name of the sender, recipent or subject in the latest messages."
	},
	{
		"id": "4671996614201671868",
		"ajlid": "044",
		"link": "Mainframe/081-Office365groups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T21:06:53Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "View all Office 365 groups",
		"keywords": "groups,management,properties,Teams",
		"description": "List all Office 365 groups, including Teams, with over 100 properties all in one place. In this view, you can edit groups' properties and settings, and perform various other group management actions."
	},
	{
		"id": "5638040156754154587",
		"ajlid": "045",
		"link": "Mainframe/082-Distributionlists.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T22:27:18Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"7899644567658873682"
		],
		"label": "View all distribution lists",
		"keywords": "distribution lists,groups,management",
		"description": "List all distribution groups in Azure Active Directory with over 100 properties all in one place. In this view, you can edit groups' properties and settings, and perform various other group management actions."
	},
	{
		"id": "753675476346004663",
		"ajlid": "046",
		"link": "Mainframe/083-Mail-enabledSecuritygroup.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T22:26:26Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "View all email-enabled security groups",
		"keywords": "groups,management,security groups",
		"description": "This job lists all email-enabled security groups with over 100 properties all in one place. In this view, you can edit groups' properties and settings, and perform various other group management actions."
	},
	{
		"id": "6774651685644351682",
		"ajlid": "047",
		"link": "Mainframe/084-SecurityGroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T22:10:02Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "View all security groups",
		"keywords": "groups,management,security",
		"description": "This job lists all security groups with over 100 properties all in one place. In this view, you can edit groups' properties and settings, and perform various other group management actions."
	},
	{
		"id": "6357959125051547168",
		"ajlid": "06A",
		"link": "Mainframe/085-ShowTeamschannels.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-31T13:40:25Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"7899644567658873682"
		],
		"label": "View channels of all Teams",
		"keywords": "channels,private channels,Teams",
		"description": "List channels and channel information of all Teams. In this view, access chats, private channel memberships, private channel sites and files."
	},
	{
		"id": "4880320107990558068",
		"ajlid": "06B",
		"link": "Mainframe/086-ShowOffice365groupconversations.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-31T15:10:24Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "View group conversations of all Office 365 groups",
		"keywords": "attachments,conversations,groups,Office 365 group,posts",
		"description": "List conversations of every Office 365 group, including Teams. In this view, you can delete posts, download attachments or delete them."
	},
	{
		"id": "4414738554300594773",
		"ajlid": "06C",
		"link": "Mainframe/087-ShowOffice365groupconversationposts.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-11-02T16:56:51Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"13667317956576447761"
		],
		"label": "View conversations of all Office 365 groups",
		"keywords": "attachments,conversations,groups,messages,Office 365,posts,received,sent,replies,mailboxes",
		"description": "List conversation posts and replies of every Office 365 group. From this view, you can delete posts, or download or delete attachments."
	},
	{
		"id": "6051979376247260312",
		"ajlid": "089",
		"link": "Mainframe/088-ShowTeamschats.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-11-02T18:54:32Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"8500565687032910346"
		],
		"label": "Show Teams'chats",
		"keywords": "chats,private channels,Teams",
		"description": "List chats of all Teams. Private channel chats are included if you are part of the channel or if you have received approval from Microsoft for sapio365 to request private channel chats."
	},
	{
		"id": "8646306781957375148",
		"ajlid": "08A",
		"link": "Mainframe/089-Viewmembersofallgroups.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-11-02T23:06:21Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"13667317956576447761"
		],
		"label": "View members of all groups and Teams",
		"keywords": "distribution groups,distribution lists,governance,group memberships,groups,Office 365,security,Teams",
		"description": "List members of every group, including Teams, Office 365 groups, security groups, email-enabled security groups, and distribution groups. From this view, you can add or remove members and owners."
	},
	{
		"id": "6774887499536745006",
		"ajlid": "03F",
		"link": "Mainframe/090-find-files-0-bytes-SharePoint-sites.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T22:44:40Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T22:44:40Z"
		},
		"categories": [
			"8252361139241865393",
			"8500565687032910346"
		],
		"label": "Find 0-byte files in SharePoint libraries",
		"keywords": "document library,documents,ediscovery,files,site library,sites,synch issues",
		"description": "Search and list all SharePoint document libraries for 0-byte sizes files."
	},
	{
		"id": "9206079648950521761",
		"ajlid": "02B",
		"link": "Mainframe/091-ShowGroupsWithGuestOwners.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-14T21:30:43Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-14T21:30:43Z"
		},
		"categories": [
			"11566107507702475288",
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "Show groups/Teams with guest owners",
		"keywords": "external,groups,guests,Office 365 group,owners,Teams",
		"description": "List groups/Teams that have owners who are guests."
	},
	{
		"id": "5199290570144164377",
		"ajlid": "040",
		"link": "Mainframe/095-Teams-where-members-can-add-edit-delete-channels-.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T22:46:37Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"7899644567658873682"
		],
		"label": "Find Teams where members can add, edit or delete channels",
		"keywords": "Teams",
		"description": "List Teams with member settings enabled to add, edit or delete channels."
	},
	{
		"id": "6910677225107741698",
		"ajlid": "02F",
		"link": "Mainframe/099-Usersbyauto-replystatus.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T22:38:26Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T22:38:26Z"
		},
		"categories": [
			"4427780615769315441"
		],
		"label": "Users by auto-reply status",
		"keywords": "email,mail,out-of-office replies,messages,automatic replies",
		"description": "Categorize users by their out-of-office (auto-reply) status."
	},
	{
		"id": "5716718668618624106",
		"ajlid": "071",
		"link": "Mainframe/100-UpdateUsersandGroupsCache.xml",
		"source": "system",
		"family": "Tenant",
		"icon": "far fa-pen-nib",
		"action": "Edit",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T22:18:09Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T22:18:09Z"
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682",
			"8821249389923942631"
		],
		"label": "Update cache of Users and Groups",
		"keywords": "synch issues,new users,new groups,new Teams,refresh",
		"description": "Update the local cache of the Users and Groups modules."
	},
	{
		"id": "1382611771474960311",
		"ajlid": "003",
		"link": "Mainframe/101-GroupsWithduplicatefiles.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T21:02:10Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T21:02:10Z"
		},
		"categories": [
			"10614786637372574868",
			"5616715603626924449"
		],
		"label": "Groups with duplicate files",
		"keywords": "documents,duplicates,files,groups,SharePoint,storage,Teams",
		"description": "List groups that have duplicate files."
	},
	{
		"id": "1385098420964739699",
		"ajlid": "005",
		"link": "Mainframe/102-SitesWithduplicatefiles.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T21:33:28Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T21:33:28Z"
		},
		"categories": [
			"10614786637372574868",
			"5616715603626924449"
		],
		"label": "Sites with duplicate files",
		"keywords": "documents,duplicates,files,SharePoint,storage,document library",
		"description": "List sites that have duplicate files in their document library."
	},
	{
		"id": "5954895133092825544",
		"ajlid": "004",
		"link": "Mainframe/103-UsersWithduplicatefiles.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-01-22T21:30:34Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-01-22T21:30:34Z"
		},
		"categories": [
			"10614786637372574868",
			"5616715603626924449"
		],
		"label": "Users with duplicate files",
		"keywords": "documents,duplicates,files,OneDrive,storage,space",
		"description": "List users who have duplicate OneDrive files."
	},
	{
		"id": "5857802530172371584",
		"ajlid": "01C",
		"link": "Mainframe/104-Groupsanonymoussharedfiles.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-06-11T22:28:51Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-06-11T22:28:51Z"
		},
		"categories": [
			"6064935782377127699"
		],
		"label": "Groups with anonymously shared files",
		"keywords": "anonymous share links,document library,documents,files,groups,security,site library,space,storage,Teams",
		"description": "List groups/Teams with files that have been shared using an anonymous share link."
	},
	{
		"id": "5343679575593617392",
		"ajlid": "033",
		"link": "Mainframe/105-UsersWithEmptyOneDrive.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T22:40:46Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T22:40:46Z"
		},
		"categories": [
			"3488082250975908789"
		],
		"label": "Users with empty OneDrive accounts",
		"keywords": "activity,document library,documents,empty,files,inactive users,OneDrive,usage",
		"description": "List all users without any documents in their OneDrive."
	},
	{
		"id": "8911421447064338431",
		"ajlid": "017",
		"link": "Mainframe/106-GroupsWithEmptyOneDrive.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T22:30:40Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T22:30:40Z"
		},
		"categories": [
			"7899644567658873682"
		],
		"label": "Groups with empty document library",
		"keywords": "documents,empty,files,site library,usage",
		"description": "List groups/Teams that have a document library with no documents."
	},
	{
		"id": "6811338796789661274",
		"ajlid": "02C",
		"link": "Mainframe/107-SitesWithEmptyOneDrive.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-06-05T20:24:50Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-06-05T20:24:50Z"
		},
		"categories": [
			"10614786637372574868",
			"16652443265771410676",
			"5616715603626924449"
		],
		"label": "SharePoint sites without any files",
		"keywords": "document library,documents,empty,files,site library,space,storage,usage",
		"description": "List SharePoint sites whose document library does not contain any files."
	},
	{
		"id": "6414328435808208923",
		"ajlid": "027",
		"link": "Mainframe/108-Sitesanonymoussharedfiles.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-06-11T22:27:26Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-06-11T22:27:26Z"
		},
		"categories": [
			"6064935782377127699"
		],
		"label": "SharePoint sites with anonymously shared files",
		"keywords": "anonymous share links,document library,documents,files,groups,security,site library,space,storage,Teams",
		"description": "List SharePoint sites with files that have been shared using an anonymous share link."
	},
	{
		"id": "1521141782206074096",
		"ajlid": "036",
		"link": "Mainframe/109-Usersanonymoussharedfiles.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-06-11T22:25:45Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-06-11T22:25:45Z"
		},
		"categories": [
			"6064935782377127699"
		],
		"label": "Users with anonymously shared files",
		"keywords": "anonymous share links,document library,documents,files,OneDrive,security,site library,space,storage",
		"description": "List users with OneDrive files that have been shared using an anonymous share link."
	},
	{
		"id": "1929946926591831871",
		"ajlid": "034",
		"link": "Mainframe/110-UsersWithNoCalendarEvents.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T22:42:01Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T22:42:01Z"
		},
		"categories": [
			"3488082250975908789"
		],
		"label": "Users with no calendar events",
		"keywords": "activity,calendar,events,empty",
		"description": "List all users without calendar events."
	},
	{
		"id": "7333944323160486453",
		"ajlid": "019",
		"link": "Mainframe/111-GroupsWithNoCalendarEvents.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-13T20:52:51Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-13T20:52:51Z"
		},
		"categories": [
			"7899644567658873682"
		],
		"label": "Groups with no calendar events",
		"keywords": "activity,calendar,events,empty",
		"description": "List all groups/Teams without any calendar events."
	},
	{
		"id": "8661425418273207328",
		"ajlid": "008",
		"link": "Mainframe/112-GroupseventsWithinDateRange.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-19T15:37:27Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-19T15:37:27Z"
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "View groups' calendar events within a date range",
		"keywords": "calendar,events,groups,Teams,search",
		"description": "List calendar events for selected groups that are set between dates you specify."
	},
	{
		"id": "2181996068330930785",
		"ajlid": "007",
		"link": "Mainframe/113-UserseventsWithinDateRange.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-15T19:59:03Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-15T19:59:03Z"
		},
		"categories": [
			"4861527419968962856"
		],
		"label": "View users' calendar events for a date range",
		"keywords": "calendar,ediscovery,events,identity",
		"description": "List calendar events and their properties for selected users for a specific date range."
	},
	{
		"id": "5764098667329832661",
		"ajlid": "072",
		"link": "Mainframe/120-reportusersbydisabledserviceplan.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-02-18T15:34:34Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"3488082250975908789"
		],
		"label": "Report users by disabled service plan",
		"keywords": "licenses,service plans,usage",
		"description": "List disabled service plans and the users affected by the plan."
	},
	{
		"id": "576504566436730718",
		"ajlid": "00E",
		"link": "Mainframe/121-ShowallSharePointsites.xml",
		"source": "system",
		"family": "Tenant",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-11-03T20:58:25Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-11-03T20:58:25Z"
		},
		"categories": [
			"16652443265771410676"
		],
		"label": "View all SharePoint sites",
		"keywords": "quota,SharePoint,sites,storage,subsites",
		"description": "List all SharePoint sites and subsites, and their information such as storage usage and quotas, creation dates, links, etc. From this view, you can access document library files and site lists."
	},
	{
		"id": "1106106347079696283",
		"ajlid": "091",
		"link": "Mainframe/122-Showtenantlicenses.xml",
		"source": "system",
		"family": "Tenant",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-11-03T21:02:06Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-11-03T21:02:06Z"
		},
		"categories": [
			"3488082250975908789"
		],
		"label": "View summary of all licenses and service plans",
		"keywords": "licenses,service plans,warning,consumed,suspended",
		"description": "List all purchased licenses and the service plans within each. In this view, you will see how many licenses are consumed and if there are any suspended licenses."
	},
	{
		"id": "9188395547067240320",
		"ajlid": "073",
		"link": "Mainframe/125-viewprivatechannelsites.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-02-18T15:43:03Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-02-18T15:43:03Z"
		},
		"categories": [
			"10614786637372574868",
			"11566107507702475288",
			"16652443265771410676"
		],
		"label": "View private channel sites",
		"keywords": "SharePoint,sites,Teams,private channels",
		"description": "List SharePoint sites and site information of every private channel."
	},
	{
		"id": "1140406531696601336",
		"ajlid": "074",
		"link": "Mainframe/130-Viewlastactivitybylocation.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-04-03T00:13:08Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-04-03T00:13:08Z"
		},
		"categories": [
			"10772695832097869471",
			"16781446285434712341"
		],
		"label": "View users' last activity by location or department",
		"keywords": "activity,inactive users,new users,usage,users",
		"description": "List the last time each user was active in Office 365, and categorize them by department or location."
	},
	{
		"id": "5630752721608958614",
		"ajlid": "077",
		"link": "Mainframe/135-ListuserswithSigninactivityerrors.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-04-21T18:42:18Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"6064935782377127699"
		],
		"label": "List users with sign-in errors",
		"keywords": "activity,error,security,sign-ins,usage,users",
		"description": "Get a view by location of users who have triggered errors upon signing in to Office 365."
	},
	{
		"id": "7443039249645849063",
		"ajlid": "075",
		"link": "Mainframe/140-findusersunabletouseteams.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-04-03T00:23:31Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-04-03T00:23:31Z"
		},
		"categories": [
			"14138584524611318201",
			"6064935782377127699"
		],
		"label": "Find users unable to use Teams",
		"keywords": "blocked sign-in,files,new users,service plans,Teams,unlicensed,usage,users",
		"description": "List users for whom sign-in is blocked, no Teams license is assigned, or their Teams service is disabled."
	},
	{
		"id": "8005365366886796257",
		"ajlid": "05E",
		"link": "Mainframe/141-Membershipsofallusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
        "isnew": true,
        "lastuse": "2020-10-30T18:14:21Z", 
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T18:14:21Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-30T18:14:21Z"
		},
		"categories": [
			"11566107507702475288",
			"7429392130877336095",
			"7899644567658873682"
		],
		"label": "View group memberships of all users",
		"keywords": "members,owners,remove,group membership,add",
		"description": "List every users' group membership. From this view, you can add or remove group memberships."
	},
	{
		"id": "5675643344091602581",
		"ajlid": "05F",
		"link": "Mainframe/142-Licensesofallusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T18:47:57Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-30T18:47:57Z"
		},
		"categories": [
			"3488082250975908789",
			"7429392130877336095"
		],
		"label": "View assigned licenses of all users",
		"keywords": "assigned licenses,inactive users,licenses,remove licenses",
		"description": "List licenses assigned per user. In this view, you can add or remove licenses, and enable or disable service plans."
	},
	{
		"id": "6292115739475570572",
		"ajlid": "060",
		"link": "Mainframe/143-Licensesandserviceplansofallusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T19:32:25Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-30T19:32:25Z"
		},
		"categories": [
			"3488082250975908789",
			"7429392130877336095"
		],
		"label": "View Licenses and service plans of all users",
		"keywords": "assigned licenses,inactive users,licenses,remove licenses",
		"description": "List licenses and service plans assigned per user. In this view, you can add or remove licenses, and enable or disable service plans."
	},
	{
		"id": "3666010109264653106",
		"ajlid": "061",
		"link": "Mainframe/144-OneDrivefilesofallusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T19:35:48Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-30T19:35:48Z"
		},
		"categories": [
			"5616715603626924449",
			"6064935782377127699"
		],
		"label": "View OneDrive files of all users",
		"keywords": "anonymous share links,documents,download,files,organization share links,shared,shared with guests,sharing,upload",
		"description": "List files and file information for all users' OneDrive. In this view, you can download, upload or delete files, and you can manage file permissions."
	},
	{
		"id": "8132263066976258087",
		"ajlid": "05D",
		"link": "Mainframe/145-Messagesinallusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T19:38:07Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-30T19:38:07Z"
		},
		"categories": [
			"1905368263408905136",
			"4427780615769315441",
			"7429392130877336095"
		],
		"label": "View email messages of all users",
		"keywords": "email,find,messages,search,sent,string,recipient,trace",
		"description": "List email messages of all users in one place. Choose a date range and use advanced filters to limit your results. From this view, you can delete messages, dowload attachments and messages."
	},
	{
		"id": "5465946708539863730",
		"ajlid": "062",
		"link": "Mainframe/146-Calendareventsforallusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T19:42:33Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-30T19:42:33Z"
		},
		"categories": [
			"4861527419968962856"
		],
		"label": "View calendar events for all users",
		"keywords": "calendar,ediscovery,events,identity",
		"description": "List every users' calendar events and event information. In this view, you can also see attachment information and delete attachments."
	},
	{
		"id": "1633167337762803107",
		"ajlid": "063",
		"link": "Mainframe/147-Contactsofallusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-30T19:43:56Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-30T19:43:56Z"
		},
		"categories": [
			"4427780615769315441",
			"7429392130877336095"
		],
		"label": "View personal contacts of all users",
		"keywords": "personal contacts,mail contacts",
		"description": "List all the personal contacts and their information for every user."
	},
	{
		"id": "6402948896844314749",
		"ajlid": "076",
		"link": "Mainframe/150-ViewTeamschannelscreatedinthelast.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-04-07T15:47:33Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-04-07T15:47:33Z"
		},
		"categories": [
			"11566107507702475288",
			"5156495196829516889"
		],
		"label": "List recently created Teams channels",
		"keywords": "activity,Teams,usage",
		"description": "List the Teams channels that were created on and after a specific date and categorize them by their creation date and the Team they belong to."
	},
	{
		"id": "9127281000316552381",
		"ajlid": "078",
		"link": "Mainframe/155-FindTeamsWithOneOwnerOrLess.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-04-24T15:00:20Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288"
		],
		"label": "Find Teams with less than 2 owners",
		"keywords": "find,groups,owners,Teams",
		"description": "List all groups with fewer than 2 owners (i.e. groups with no owner or one owner)."
	},
	{
		"id": "7471588419223922596",
		"ajlid": "07B",
		"link": "Mainframe/160-ListTeamscreatedperdepartmentinthelast.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-08T20:01:55Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-08T20:01:55Z"
		},
		"categories": [
			"11566107507702475288"
		],
		"label": "List recently created Teams by department",
		"keywords": "activity,new Teams,Teams",
		"description": "List the Teams that were created since the date you specify and categorize them by the department of the user who created it."
	},
	{
		"id": "2330355843631247074",
		"ajlid": "07A",
		"link": "Mainframe/170-TeamsGuestCleanup.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-pen-nib",
		"action": "Edit",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-01T22:53:27Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-01T22:53:27Z"
		},
		"categories": [
			"11566107507702475288",
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "Clean up Teams with guest members",
		"keywords": "guests,new Teams,Teams",
		"description": "List Teams with guests and Teams where guests can be added, with options to change this group setting and to remove guests from those Teams."
	},
	{
		"id": "3650823903435757410",
		"ajlid": "07C",
		"link": "Mainframe/180-DisableGiphyinTeams.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-pen-nib",
		"action": "Edit",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-05-20T15:00:14Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-05-20T15:00:14Z"
		},
		"categories": [
			"11566107507702475288",
			"7899644567658873682"
		],
		"label": "Disable Giphy in Teams",
		"keywords": "Giphy,security,Teams",
		"description": "Disable the setting that allows the use of Giphy in all existing Teams."
	},
	{
		"id": "8200200934785614712",
		"ajlid": "07E",
		"link": "Mainframe/190-getSitesData.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-07-06T21:41:57Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": ""
		},
		"categories": [
			"11566107507702475288",
			"16652443265771410676",
			"7899644567658873682"
		],
		"label": "View all SharePoint sites & subsites by type (Teams, Yammer, private channels, etc)",
		"keywords": "groups,Office 365 group,SharePoint,sites,Teams,Yammer",
		"description": "Get a clearer view of your SharePoint site topology. See which sites belong to Teams, private channels, regular Office 365 groups or Yammer-linked groups."
	},
	{
		"id": "3586334574117169825",
		"ajlid": "07F",
		"link": "Mainframe/200-emailinactiveteamsowners.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-07-09T17:22:34Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-07-09T17:22:34Z"
		},
		"categories": [
			"7899644567658873682"
		],
		"label": "Email owners of inactive Teams",
		"keywords": "activity,Teams,usage",
		"description": "This job finds Teams without any activity during a period and sends a notification email to each owner."
	},
	{
		"id": "2547012490284621036",
		"ajlid": "035",
		"link": "Mainframe/201-UserswithOneDrivefilessharedwithintheorganizationwithlink.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-20T21:35:42Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-20T21:35:42Z"
		},
		"categories": [
			"5616715603626924449",
			"6064935782377127699",
			"7429392130877336095"
		],
		"label": "Find users with OneDrive files shared within the organization with a link",
		"keywords": "documents,files,OneDrive,organization share links,shared,sharing",
		"description": "List all users with OneDrive files shared within the organization using a sharing link. From there, you can delete the link."
	},
	{
		"id": "6750997008340794301",
		"ajlid": "037",
		"link": "Mainframe/202-UserswithOneDrivefilessharedwithguests.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-20T21:52:36Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-20T21:52:36Z"
		},
		"categories": [
			"5616715603626924449",
			"6064935782377127699"
		],
		"label": "Find users' OneDrive files shared with guests",
		"keywords": "documents,files,OneDrive,shared,shared with guests,sharing",
		"description": "Search users' OneDrive and list files shared with guests."
	},
	{
		"id": "4341787166682303213",
		"ajlid": "038",
		"link": "Mainframe/203-UserswithOneDrivefilessharedwithXpeople.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T21:17:20Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T21:17:20Z"
		},
		"categories": [
			"5616715603626924449",
			"6064935782377127699"
		],
		"label": "Find users' OneDrive files shared with many",
		"keywords": "documents,files,OneDrive,shared with many,sharing",
		"description": "Search all users' OneDrive and list files that have been shared with more than a specified number of people."
	},
	{
		"id": "6636564152563127739",
		"ajlid": "01B",
		"link": "Mainframe/204-GroupswithOneDrivefilessharedwithintheorganizationwithlink.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-21T22:25:17Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-21T22:25:17Z"
		},
		"categories": [
			"11566107507702475288",
			"16652443265771410676",
			"5616715603626924449",
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "Find group/Teams files shared within the organization with link",
		"keywords": "documents,files,groups,organization share links,Teams,sharing,shared",
		"description": "List all groups/Teams and their document library files shared within the organization using a sharing link. From there, you can delete the link."
	},
	{
		"id": "8983332799582111357",
		"ajlid": "01D",
		"link": "Mainframe/205-GroupswithOneDrivefilessharedwithguests.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-21T22:26:49Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-21T22:26:49Z"
		},
		"categories": [
			"11566107507702475288",
			"5616715603626924449",
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "Find groups and Teams files shared with guests",
		"keywords": "documents,files,groups,shared,shared with guests,sharing,site library,Teams",
		"description": "Search group and Teams document libraries and list files shared with guests."
	},
	{
		"id": "1599235183750842011",
		"ajlid": "01E",
		"link": "Mainframe/206-GroupswithOneDrivefilessharedwithXpeople.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T21:07:31Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T21:07:31Z"
		},
		"categories": [
			"11566107507702475288",
			"5616715603626924449",
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "Find group and Teams files shared with many",
		"keywords": "documents,files,shared with many,sharing",
		"description": "Search all group and Teams document libraries and list files that have been shared with more than a specified number of people."
	},
	{
		"id": "7137105887097150178",
		"ajlid": "026",
		"link": "Mainframe/207-SiteswithOneDrivefilessharedwithintheorganizationwithlink.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-21T22:29:58Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-21T22:29:58Z"
		},
		"categories": [
			"16652443265771410676",
			"5616715603626924449",
			"6064935782377127699"
		],
		"label": "Find site document library files shared within the organization with a link",
		"keywords": "documents,files,organization share links,shared,SharePoint,sharing,site library,sites",
		"description": "List all sites with document library files shared within the organization using a sharing link. From there, you can delete the link."
	},
	{
		"id": "4313975701051501579",
		"ajlid": "028",
		"link": "Mainframe/208-SiteswithOneDrivefilessharedwithguests.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-21T22:31:17Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-21T22:31:17Z"
		},
		"categories": [
			"16652443265771410676",
			"5616715603626924449",
			"6064935782377127699"
		],
		"label": "Find site files shared with guests",
		"keywords": "documents,files,shared,shared with guests,SharePoint,sharing,site library,sites",
		"description": "Search SharePoint site document libraries and list files shared with guests."
	},
	{
		"id": "1006589031143766729",
		"ajlid": "029",
		"link": "Mainframe/209-SiteswithOneDrivefilessharedwithXpeople.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-search",
		"action": "Search",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T21:08:39Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T21:08:39Z"
		},
		"categories": [
			"16652443265771410676",
			"5616715603626924449",
			"6064935782377127699"
		],
		"label": "Find site document library files shared with many",
		"keywords": "anonymous share links,documents,files,organization share links,shared with guests,shared with many",
		"description": "Search all SharePoint sites and list files that have been shared with more than a specified number of people."
	},
	{
		"id": "506112321852597914",
		"ajlid": "049",
		"link": "Mainframe/211-OneDriveactivity.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T21:47:00Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T21:47:00Z"
		},
		"categories": [
			"16781446285434712341",
			"5616715603626924449",
			"7429392130877336095"
		],
		"label": "Get OneDrive activity usage report",
		"keywords": "activity,OneDrive,reports,storage,usage",
		"description": "List OneDrive activity in the last 180 days."
	},
	{
		"id": "3995684427420123143",
		"ajlid": "04A",
		"link": "Mainframe/212-OneDriveusage.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T21:56:56Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T21:56:56Z"
		},
		"categories": [
			"16781446285434712341",
			"5616715603626924449"
		],
		"label": "Get OneDrive usage report",
		"keywords": "activity,OneDrive,reports,usage",
		"description": "List OneDrive usage in the last 180 days."
	},
	{
		"id": "4549152456637344058",
		"ajlid": "04B",
		"link": "Mainframe/213-Teamsdeviceusage.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T21:57:19Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T21:57:19Z"
		},
		"categories": [
			"11566107507702475288",
			"6064935782377127699"
		],
		"label": "Get Teams device usage report",
		"keywords": "Teams,usage,devices,reports",
		"description": "List Teams device usage in the last 180 days."
	},
	{
		"id": "1881728664799539700",
		"ajlid": "04C",
		"link": "Mainframe/214-Teamsuseractivity.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T21:59:12Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T21:59:12Z"
		},
		"categories": [
			"11566107507702475288",
			"16781446285434712341",
			"7429392130877336095"
		],
		"label": "Get usage report of Teams user activity",
		"keywords": "active,activity,reports,Teams,usage,users",
		"description": "List Teams user activity in the last 180 days."
	},
	{
		"id": "2738631389571811327",
		"ajlid": "04D",
		"link": "Mainframe/215-Emailactivity.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:00:51Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:00:51Z"
		},
		"categories": [
			"16781446285434712341",
			"4427780615769315441"
		],
		"label": "Get usage report of Email activity",
		"keywords": "messages,mail,usage,reports,activity",
		"description": "List Email activity in the last 180 days."
	},
	{
		"id": "4145200737721597561",
		"ajlid": "04E",
		"link": "Mainframe/216-Emailappactivity.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:05:13Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:05:13Z"
		},
		"categories": [
			"16781446285434712341",
			"4427780615769315441"
		],
		"label": "Get usage report of Email app activity",
		"keywords": "activity,emails,messages,report,usage",
		"description": "List Email app activity in the last 180 days."
	},
	{
		"id": "567526381204225980",
		"ajlid": "04F",
		"link": "Mainframe/217-Mailboxusage.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:05:58Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:05:58Z"
		},
		"categories": [
			"16781446285434712341",
			"4427780615769315441"
		],
		"label": "Get Mailbox usage report",
		"keywords": "activity,mailbox,messages,reports,usage,messaging",
		"description": "List Mailbox usage in the last 180 days."
	},
	{
		"id": "6619298737481255415",
		"ajlid": "050",
		"link": "Mainframe/218-Office365groupsactivity.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:09:28Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:09:28Z"
		},
		"categories": [
			"11566107507702475288",
			"16781446285434712341",
			"7899644567658873682"
		],
		"label": "Get usage report of Office 365 groups activity",
		"keywords": "usage,reports,activity,Teams,groups,active",
		"description": "List Office 365 groups activity in the last 180 days."
	},
	{
		"id": "8056914960162837711",
		"ajlid": "051",
		"link": "Mainframe/219-Office365activeusers.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:12:25Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:12:25Z"
		},
		"categories": [
			"16781446285434712341",
			"7429392130877336095"
		],
		"label": "Get usage report of Office 365 active users",
		"keywords": "usage,reports,activity,inactive",
		"description": "List Office 365 active users in the last 180 days."
	},
	{
		"id": "1706366520441516332",
		"ajlid": "052",
		"link": "Mainframe/220-Office365activations.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:14:10Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:14:10Z"
		},
		"categories": [
			"16781446285434712341",
			"7429392130877336095"
		],
		"label": "Get usage report of Office 365 activations",
		"keywords": "usage,reports,services,activity,inactive",
		"description": "List Office 365 activations."
	},
	{
		"id": "6860316852439998285",
		"ajlid": "053",
		"link": "Mainframe/221-SharePointactivity.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:16:18Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:16:18Z"
		},
		"categories": [
			"16652443265771410676",
			"16781446285434712341"
		],
		"label": "Get usage report of SharePoint activity",
		"keywords": "usage,reports,SharePoint,activity",
		"description": "List SharePoint activity in the last 180 days."
	},
	{
		"id": "2402880333847259327",
		"ajlid": "054",
		"link": "Mainframe/222-SharePointsiteusage.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:19:05Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:19:05Z"
		},
		"categories": [
			"10614786637372574868",
			"16652443265771410676",
			"16781446285434712341"
		],
		"label": "Get SharePoint site usage report",
		"keywords": "SharePoint,sites,storage,usage,quota",
		"description": "List SharePoint site usage in the last 180 days."
	},
	{
		"id": "8832584952097706462",
		"ajlid": "055",
		"link": "Mainframe/223-SkypeforBusinessactivity.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:21:29Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:21:29Z"
		},
		"categories": [
			"16781446285434712341",
			"7429392130877336095"
		],
		"label": "Get usage report of Skype for Business activity",
		"keywords": "activity,reports,Skype for Business,usage",
		"description": "List Skype for Business activity in the last 180 days."
	},
	{
		"id": "7131246736215843239",
		"ajlid": "056",
		"link": "Mainframe/224-SkypeforBusinessdeviceusage.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:23:47Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:23:47Z"
		},
		"categories": [
			"16781446285434712341",
			"7429392130877336095"
		],
		"label": "Get usage report of Skype for Business device usage",
		"keywords": "Skype for Business,devices,usage,reports",
		"description": "List Skype for Business device usage in the last 180 days."
	},
	{
		"id": "8272433561171063994",
		"ajlid": "057",
		"link": "Mainframe/225-Yammeractivity.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:26:28Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:26:28Z"
		},
		"categories": [
			"16781446285434712341",
			"7899644567658873682"
		],
		"label": "Get usage report of Yammer activity",
		"keywords": "Yammer,groups,activity,usage,reports",
		"description": "List Yammer activity in the last 180 days."
	},
	{
		"id": "4023426582178168497",
		"ajlid": "058",
		"link": "Mainframe/226-Yammerdeviceusage.xml",
		"source": "system",
		"family": "Users",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:28:16Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:28:16Z"
		},
		"categories": [
			"16781446285434712341",
			"6064935782377127699",
			"7899644567658873682"
		],
		"label": "Get Yammer device usage report",
		"keywords": "groups,reports,usage,Yammer",
		"description": "List Yammer device usage in the last 180 days."
	},
	{
		"id": "1740552907185481828",
		"ajlid": "059",
		"link": "Mainframe/227-Yammergroupsactivity.xml",
		"source": "system",
		"family": "Groups",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-26T22:31:08Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-26T22:31:08Z"
		},
		"categories": [
			"16781446285434712341",
			"7899644567658873682"
		],
		"label": "Get usage report on Yammer groups activity",
		"keywords": "Yammer,groups,usage,reports",
		"description": "List Yammer groups activity in the last 180 days."
	},
	{
		"id": "2296929639886697322",
		"ajlid": "06D",
		"link": "Mainframe/228-ShowSharePointsitelist.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T14:05:17Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-27T14:05:17Z"
		},
		"categories": [
			"16652443265771410676",
			"5616715603626924449"
		],
		"label": "View lists of all SharePoint sites",
		"keywords": "SharePoint",
		"description": "List SharePoint list information of all sites and subsites. In this view, you can access list item information and list column information."
	},
	{
		"id": "7609901753723711137",
		"ajlid": "06E",
		"link": "Mainframe/229-ShowSharePointsitelistitems.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T14:12:54Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-27T14:12:54Z"
		},
		"categories": [
			"16652443265771410676",
			"5616715603626924449"
		],
		"label": "View list items of all SharePoint site lists",
		"keywords": "list,SharePoint,list item",
		"description": "List every list item for every SharePoint site list. Display site list item information like last modified by, creation date, checked out to, version, retention label, etc."
	},
	{
		"id": "6237606249406298527",
		"ajlid": "06F",
		"link": "Mainframe/230-ShowSharePointsitelistcolumns.xml",
		"source": "system",
		"family": "Sites",
		"icon": "far fa-file-alt",
		"action": "Report",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-27T14:13:47Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-27T14:13:47Z"
		},
		"categories": [
			"16652443265771410676"
		],
		"label": "View list columns for all SharePoint site lists",
		"keywords": "list,SharePoint,design,columns",
		"description": "List every list column and its information of all SharePoint site lists."
	},
	{
		"id": "6670966324418463213",
		"ajlid": "022",
		"link": "Mainframe/56-Filequerybynameortypeforselected.xml",
		"source": "system",
		"family": "Groups",
		"icon": "nimportequoi",
		"isnew": true,
		"created": {
			"by": "Ytria",
			"on": "2020-10-09T22:02:41Z"
		},
		"lastedit": {
			"by": "Ytria",
			"on": "2020-10-09T22:02:41Z"
		},
		"categories": [
			"11566107507702475288",
			"5616715603626924449",
			"7899644567658873682",
			"8500565687032910346"
		],
		"label": "Find files by name or type for selected groups and Teams",
		"keywords": "extension,files,find,search,query,file",
		"description": "Search selected group and Teams document libraries for files that match a specified name and extension type."
    },
    {
        "id": "4778794288608529591",
        "ajlid": "",
        "link": "Mainframe/OneSchoolGlobal_sharedFilesByType-1.1.xml",
        "source": "custom",
        "family": "",
        "icon": "far fa-alicorn",
        "isnew": false,
        "created": {
          "by": "Eric Houvenaghel",
          "on": ""
        },
        "lastedit": {
          "by": "Eric Houvenaghel",
          "on": "2020-11-15T23:06:07Z"
        },
        "categories": [
        ],
        "label": "Users with shared files",
        "keywords": "",
        "description": "List users and their OneDrive files shared anonymously, directly with guests, or within the organization."
      },
      {
        "id": "7236510865145124308",
        "ajlid": "",
        "link": "Mainframe/OneSchoolGlobal_FilesByCampus.xml",
        "source": "custom",
        "family": "",
        "icon": "",
        "isnew": false,
        "created": {
          "by": "Eric Houvenaghel",
          "on": ""
        },
        "lastedit": {
          "by": "Eric Houvenaghel",
          "on": "2020-11-15T23:12:00Z"
        },
        "categories": [
        ],
        "label": "Inappropriate File Type Report for a Given Campus",
        "keywords": "",
        "description": ""
      }
];
var allCategories = [
	{
		"id": "16191365668128396744",
		"label": "Auditing",
		"children": [
			{
				"id": "16781446285434712341",
				"label": "Usage reports"
			},
			{
				"id": "1905368263408905136",
				"label": "Email trace"
			},
			{
				"id": "5156495196829516889",
				"label": "Creation reports"
			},
			{
				"id": "8500565687032910346",
				"label": "eDiscovery"
			}
		]
	},
	{
		"id": "17614500312402591793",
		"label": "Teams and group governance",
		"children": [
			{
				"id": "11566107507702475288",
				"label": "Teams management"
			},
			{
				"id": "13667317956576447761",
				"label": "Group management"
			},
			{
				"id": "7899644567658873682",
				"label": "Groups management"
			},
			{
				"id": "8813166633134608907",
				"label": "Back up"
			}
		]
	},
	{
		"id": "3488082250975908789",
		"label": "License management"
	},
	{
		"id": "5616715603626924449",
		"label": "Document management"
	},
	{
		"id": "5832464602439441496",
		"label": "Identity management",
		"children": [
			{
				"id": "10772695832097869471",
				"label": "User onboarding"
			},
			{
				"id": "14138584524611318201",
				"label": "User offboarding"
			},
			{
				"id": "2841833117921732997",
				"label": "Help desk"
			},
			{
				"id": "7429392130877336095",
				"label": "User management"
			},
			{
				"id": "8252361139241865393",
				"label": "Migration"
			},
			{
				"id": "8813166633134608907",
				"label": "Back up"
			},
			{
				"id": "8821249389923942631",
				"label": "Synchronization"
			}
		]
	},
	{
		"id": "6064935782377127699",
		"label": "Security",
		"children": [
			{
				"id": "16928775336606739569",
				"label": "Administrator roles"
			},
			{
				"id": "6064935782377127699",
				"label": "Security"
			}
		]
	},
	{
		"id": "6377165284567306566",
		"label": "Mail and calendar management",
		"children": [
			{
				"id": "4427780615769315441",
				"label": "Email messages"
			},
			{
				"id": "4861527419968962856",
				"label": "Calendar events"
			}
		]
	},
	{
		"id": "9484505177378900396",
		"label": "SharePoint site management",
		"children": [
			{
				"id": "10614786637372574868",
				"label": "Storage"
			},
			{
				"id": "16652443265771410676",
				"label": "SharePoint sites"
			}
		]
	}
];
var allScheduled = [ 
  {
    "id": "961208973199561249",
    "link": "MainFrame/145-Messagesinallusers.xml",
    "idJob": "8132263066976258087",
    "created": { "by": "John Doe", "on": "2019-10-08T17:33Z" },
    "lastedit": { "by": "John Doe", "on": "2019-10-08T17:33Z" },
    "startson": "2019-11-27T17:33Z",
    "frequency": "Daily",
    "recur_every": "2"
  },
  {
    "id": "884847050772196655",
    "link": "MainFrame/214-Teamsuseractivity.xml",
    "idJob": "1881728664799539700",
    "created": { "by": "John Doe", "on": "2019-10-12T17:33Z" },
    "lastedit": { "by": "John Doe", "on": "2019-10-12T17:33Z" },
    "startson": "2019-11-22T17:33Z",
    "frequency": "Daily",
    "recur_every": "1"
  },
  {
    "id": "788497781411757857",
    "link": "MainFrame/217-Mailboxusage.xml",
    "idJob": "567526381204225980",
    "created": { "by": "John Doe", "on": "2019-09-12T17:33Z" },
    "lastedit": {
      "by": "John Doe",
      "on": "2019-09-15T17:33Z"
    },
    "startson": "2019-10-27T17:33Z",
    "frequency": "Weekly",
    "recur_every": "Monday"
  },
  {
    "id": "616369746483933044",
    "link": "MainFrame/220-Office365activations.xml",
    "idJob": "1706366520441516332",
    "created": { "by": "John Doe", "on": "2019-12-07T17:33Z" },
    "lastedit": { "by": "John Doe", "on": "2019-12-10T17:33Z" },
    "startson": "2019-11-14T17:33Z",
    "frequency": "Monthly",
    "recur_every": "Last"
  },
  {
    "id": "420012837407069272",
    "link": "MainFrame/221-SharePointactivity.xml",
    "idJob": "6860316852439998285",
    "created": { "by": "John Doe", "on": "2019-10-10T17:33Z" },
    "lastedit": { "by": "John Doe", "on": "2019-10-14T17:33Z" },
    "startson": "2019-10-21T17:33Z",
    "frequency": "Monthly",
    "recur_every": "15"
  }
 ];

// var allSelectedAJL = [ ];
// var maxSelectedAJL = 0;
// DEBUG ---- this is set by test-data.js
var allSelectedAJL = [ "03B", "07B", "008", "019" ];
var maxSelectedAJL = 4;
