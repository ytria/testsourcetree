(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['ExitSession'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "<button name=\""
    + container.escapeExpression(((helper = (helper = helpers.btnValue || (depth0 != null ? depth0.btnValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"btnValue","hash":{},"data":data}) : helper)))
    + "\" type=\"submit\">\r\n    <div class=\"cardBlock columns is-full is-mobile\">\r\n        <div class=\"column is-2 leftColIcon alignCenter\">\r\n            <i class=\"far fa-times-square\" focusable=\"false\"></i>\r\n        </div>\r\n        <div class=\"column rghtColInf alignCenter has-text-left\">\r\n            <h5 class=\"sessionEmail blue\"> "
    + ((stack1 = ((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "  </h5>\r\n            <div class=\"sessionDate gray\">"
    + ((stack1 = ((helper = (helper = helpers.desc || (depth0 != null ? depth0.desc : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"desc","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n        </div>\r\n    </div>\r\n</button>\r\n<div class=\"introSelRole\">"
    + ((stack1 = ((helper = (helper = helpers.introrole || (depth0 != null ? depth0.introrole : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"introrole","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n";
},"useData":true});
templates['Main'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "			<div id=\"searchSession\">\r\n                <div class=\"searchBox\">\r\n                    <div><i class=\"fas fa-search\" focusable=\"false\"></i></div>\r\n                    <div><input class=\"quicksearch\" type=\"text\"></div>\r\n                </div>\r\n			</div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "			<div id=\"selUltraSession\">\r\n                <div class=\"title\">"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.introTitle : stack1), depth0)) != null ? stack1 : "")
    + "</div>\r\n                <div class=\"description\">"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.introDesc : stack1), depth0)) != null ? stack1 : "")
    + "</div>\r\n			</div>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "			<div id=\"selCustomerSession\">\r\n                <div class=\"title\">"
    + ((stack1 = container.lambda(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.introTitle : stack1), depth0)) != null ? stack1 : "")
    + "</div>\r\n                <div class=\"description\">\r\n                    <div><i class=\"fas fa-search\" focusable=\"false\"></i></div>\r\n                    <div><input class=\"quicksearch\" type=\"text\"></div>\r\n                </div>\r\n			</div>\r\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "			<div id=\"regularSession\" class=\""
    + ((stack1 = helpers.unless.call(alias1,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isRoleSelect : stack1),{"name":"unless","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isWithCancel : stack1),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">\r\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    return "display-no";
},"10":function(container,depth0,helpers,partials,data) {
    return "withExitSession";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.hbs : depth0),"RegularSession",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.hbs : depth0),"ExitSession",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"13":function(container,depth0,helpers,partials,data) {
    return "						"
    + container.escapeExpression((helpers.sub_hbs || (depth0 && depth0.sub_hbs) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"sub_hbs","hash":{},"data":data}))
    + "\r\n";
},"15":function(container,depth0,helpers,partials,data) {
    return "withSearchSession";
},"17":function(container,depth0,helpers,partials,data) {
    return "customerSelect";
},"19":function(container,depth0,helpers,partials,data) {
    return "ultraSelect";
},"21":function(container,depth0,helpers,partials,data) {
    return "roleSelect";
},"23":function(container,depth0,helpers,partials,data) {
    return " withExitSession";
},"25":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"SessionCard",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"27":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.hbs : depth0),"SessionAdd-User-Or-Admin",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"29":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "   			<div id=\"skipUltraSession\">\r\n                <button name=\""
    + container.escapeExpression(alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.btnValue : stack1), depth0))
    + "\" type=\"submit\">\r\n                    <div class=\"cardBlock columns is-full is-mobile\">\r\n                        <div class=\"column is-2 leftColIcon alignCenter\">\r\n                            <i class=\"fas fa-fast-forward\" focusable=\"false\"></i>\r\n                        </div>\r\n                        <div class=\"column rghtColInf alignCenter has-text-left\">\r\n                            <h5 class=\"sessionEmail blue\"> "
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.btnLabel : stack1), depth0)) != null ? stack1 : "")
    + "  </h5>\r\n                            <div class=\"sessionDate gray\">"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.btnDesc : stack1), depth0)) != null ? stack1 : "")
    + "</div>\r\n                        </div>\r\n                    </div>\r\n                </button>\r\n            </div>\r\n";
},"31":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "			<div class=\"footerSelRole\">\r\n				<div class=\"control\">\r\n					<input id=\"showScopeData\" name=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.optionField : stack1), depth0))
    + "\" type=\"checkbox\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.optionVal : stack1), depth0))
    + "\" checked>\r\n					<label for=\"showScopeData\">\r\n						<div>\r\n							<span class=\"iconYes\">\r\n								<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"31px\"\r\n									height=\"28px\" viewBox=\"0 0 31 28\" style=\"enable-background:new 0 0 31 28;\" xml:space=\"preserve\">\r\n									<!--<style type=\"text/css\">.st0{fill:#FFFFFF;stroke:#3471B0;stroke-width:1.5;stroke-miterlimit:10;} .st1{fill:#3471B0;}.st2{fill:#787878;}.st3	{fill:#FFFFFF;}</style>-->\r\n									<defs></defs>\r\n									<circle fill=\"#FFFFFF\" stroke=\"#3471B0\" stroke-width=\"1.5\" stroke-miterlimit=\"10\" cx=\"22.5\" cy=\"17.35\" r=\"6.65\"/>\r\n									<rect x=\"22\" y=\"22\" fill=\"#3471B0\" width=\"1\" height=\"4\"/>\r\n									<rect x=\"22\" y=\"9\" fill=\"#3471B0\" width=\"1\" height=\"4\"/>\r\n									<rect x=\"15.5\" y=\"15.5\" transform=\"matrix(-1.836970e-16 1 -1 -1.836970e-16 33.5 1.5)\" fill=\"#3471B0\" width=\"1\" height=\"4\"/>\r\n									<rect x=\"28.5\" y=\"15.5\" transform=\"matrix(-1.836970e-16 1 -1 -1.836970e-16 46.5 -11.5)\" fill=\"#3471B0\" width=\"1\" height=\"4\"/>\r\n									<rect x=\"22\" y=\"14\" fill=\"#3471B0\" width=\"1\" height=\"7\"/>\r\n									<rect x=\"22\" y=\"14\" transform=\"matrix(-1.836970e-16 1 -1 -1.836970e-16 40 -5)\" fill=\"#3471B0\" width=\"1\" height=\"7\"/>\r\n									<g>\r\n									<polygon fill=\"#787878\" points=\"16,27 15,27 15,22 14,22 14,12 16,12 20,2 25,2 19.27,9 21.66,9 29,1.84 29,0 0,0 0,2 12,13.59 12,28 \r\n									17,28 17,23 16,23\"/>\r\n									<polygon fill=\"#FFFFFF\" points=\"20,2 15.45,12 16,12 16,11 17,11 17,10 18,10 18,9 19.27,9 25,2\"/>\r\n									<polygon fill=\"#FFFFFF\" points=\"15,27 16,27 16,24 16,22 15,22\" />\r\n									</g>\r\n								</svg>\r\n							</span>\r\n							<span class=\"iconNo display-no\">\r\n								<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"31px\"\r\n									height=\"28px\" viewBox=\"0 0 31 28\" style=\"enable-background:new 0 0 31 28;\" xml:space=\"preserve\">\r\n									<!--<style type=\"text/css\">.st0{fill:#FFFFFF;stroke:#CDCDCD;stroke-width:1.5;stroke-miterlimit:10;} .st1{fill:#CDCDCD;}.st2{fill:#CDCDCD;}.st3	{fill:#CDCDCD;}</style>-->\r\n									<defs></defs>\r\n									<circle class=\"st0\" cx=\"22.5\" cy=\"17.35\" r=\"6.65\"/>\r\n									<rect x=\"22\" y=\"22\" class=\"st1\" width=\"1\" height=\"4\"/>\r\n									<rect x=\"22\" y=\"9\" class=\"st1\" width=\"1\" height=\"4\"/>\r\n									<rect x=\"15.5\" y=\"15.5\" transform=\"matrix(-1.836970e-16 1 -1 -1.836970e-16 33.5 1.5)\" class=\"st1\" width=\"1\" height=\"4\"/>\r\n									<rect x=\"28.5\" y=\"15.5\" transform=\"matrix(-1.836970e-16 1 -1 -1.836970e-16 46.5 -11.5)\" class=\"st1\" width=\"1\" height=\"4\"/>\r\n									<rect x=\"22\" y=\"14\" class=\"st1\" width=\"1\" height=\"7\"/>\r\n									<rect x=\"22\" y=\"14\" transform=\"matrix(-1.836970e-16 1 -1 -1.836970e-16 40 -5)\" class=\"st1\" width=\"1\" height=\"7\"/>\r\n									<g>\r\n									<polygon class=\"st2\" points=\"16,27 15,27 15,22 14,22 14,12 16,12 20,2 25,2 19.27,9 21.66,9 29,1.84 29,0 0,0 0,2 12,13.59 12,28 \r\n									17,28 17,23 16,23 	\"/>\r\n									<polygon class=\"st3\" points=\"20,2 15.45,12 16,12 16,11 17,11 17,10 18,10 18,9 19.27,9 25,2 	\"/>\r\n									<polygon class=\"st3\" points=\"15,27 16,27 16,24 16,22 15,22 	\"/>\r\n									</g>\r\n								</svg>\r\n							</span>\r\n							<div class=\"afterLabel\"><span>"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.optionLabel : stack1), depth0)) != null ? stack1 : "")
    + "</span>"
    + ((stack1 = alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.optionLabelSecondLine : stack1), depth0)) != null ? stack1 : "")
    + "</div>\r\n						</div>\r\n					</label>\r\n				</div>\r\n			</div>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"section\" ondragstart=\"return false;\">\r\n	<div id=\"mainContainer\" class=\"containerY\">\r\n		<form id=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.id : stack1), depth0))
    + "\" method=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.method : stack1), depth0))
    + "\" action=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.action : stack1), depth0))
    + "\">\r\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isSearch : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isUltraSelect : stack1),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isCustomerSelect : stack1),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isRoleSelect : stack1),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            \r\n			<div id=\"sessionList\" class=\""
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isSearch : stack1),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isCustomerSelect : stack1),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isUltraSelect : stack1),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isRoleSelect : stack1),{"name":"if","hash":{},"fn":container.program(21, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isWithCancel : stack1),{"name":"if","hash":{},"fn":container.program(23, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">\r\n"
    + ((stack1 = helpers.each.call(alias3,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(25, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	   		</div>\r\n			<div id=\"newsession\" class=\""
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isCustomerSelect : stack1),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isUltraSelect : stack1),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isRoleSelect : stack1),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\">\r\n"
    + ((stack1 = helpers.each.call(alias3,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(27, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	   		</div>\r\n\r\n"
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isUltraSelect : stack1),{"name":"if","hash":{},"fn":container.program(29, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias3,((stack1 = (depth0 != null ? depth0.main : depth0)) != null ? stack1.isRoleSelect : stack1),{"name":"if","hash":{},"fn":container.program(31, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	\r\n		</form>\r\n	</div> \r\n</div>\r\n<script language=\"javascript\">\r\n	$(document).ready(function() {\r\n        // Compute the effective size of the dialog\r\n        // and provide that to the application\r\n        checkContentHeight(true);\r\n	});\r\n</script>\r\n\r\n\r\n";
},"useData":true});
templates['RegularSession'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "                <i class=\"fal fa-portrait\" focusable=\"false\"></i>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                <i class=\"fal fa-user\" focusable=\"false\"></i>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "<button name=\""
    + container.escapeExpression(((helper = (helper = helpers.btnValue || (depth0 != null ? depth0.btnValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"btnValue","hash":{},"data":data}) : helper)))
    + "\" type=\"submit\">\r\n    <div class=\"cardBlock columns is-full is-mobile\">\r\n        <div class=\"column is-2 leftColIcon alignCenter\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"standard",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "        </div>\r\n        <div class=\"column rghtColInf alignCenter has-text-left\">\r\n            <h5 class=\"sessionEmail blue\"> "
    + ((stack1 = ((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "  </h5>\r\n            <div class=\"sessionDate gray\">"
    + ((stack1 = ((helper = (helper = helpers.desc || (depth0 != null ? depth0.desc : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"desc","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n        </div>\r\n    </div>\r\n</button>\r\n<div class=\"introSelRole\">"
    + ((stack1 = ((helper = (helper = helpers.introrole || (depth0 != null ? depth0.introrole : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"introrole","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</div>\r\n";
},"useData":true});
templates['SessionAdd-User-Or-Admin'] = template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <button name=\""
    + alias4(((helper = (helper = helpers.buttonName || (depth0 != null ? depth0.buttonName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonName","hash":{},"data":data}) : helper)))
    + "\" type=\"submit\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.disabled : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " "
    + ((stack1 = helpers["if"].call(alias1,(helpers.count_greaterthan || (depth0 && depth0.count_greaterthan) || alias2).call(alias1,(depths[1] != null ? depths[1].choices : depths[1]),2,{"name":"count_greaterthan","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(4, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\r\n            <div class=\"leftColIcon\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"ultraadmin",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(6, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"advanced",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(8, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"standard",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(10, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"elevated",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(12, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\r\n            <div class=\"gray\">\r\n                "
    + alias4(((helper = (helper = helpers.buttonText || (depth0 != null ? depth0.buttonText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"buttonText","hash":{},"data":data}) : helper)))
    + "\r\n            </div>\r\n    </button>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "disabled";
},"4":function(container,depth0,helpers,partials,data) {
    return "class=\"fixSize\"";
},"6":function(container,depth0,helpers,partials,data) {
    return "                    <i class=\"fal fa-shield-alt\" focusable=\"false\"></i>\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    return "                    <i class=\"fal fa-user\" focusable=\"false\"></i>\r\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "                    <i class=\"fal fa-portrait\" focusable=\"false\"></i>\r\n";
},"12":function(container,depth0,helpers,partials,data) {
    return "                    <i class=\"fal fa-user-shield\" focusable=\"false\"></i>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.choices : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['SessionCard'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "disabled";
},"3":function(container,depth0,helpers,partials,data) {
    return "                <i class=\"fal fa-portrait\" focusable=\"false\"></i>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.type : depth0),"advanced",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.program(8, data, 0),"data":data})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    return "                <i class=\"fas fa-user\" focusable=\"false\"></i>\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.type : depth0),"elevated",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.program(11, data, 0),"data":data})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    return "                <i class=\"fas fa-user-shield\" focusable=\"false\"></i>\r\n";
},"11":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.type : depth0),"ultraadmin",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.program(14, data, 0),"data":data})) != null ? stack1 : "");
},"12":function(container,depth0,helpers,partials,data) {
    return "                <i class=\"fas fa-shield-alt\" focusable=\"false\"></i>\r\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.type : depth0),"role_user",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.program(17, data, 0),"data":data})) != null ? stack1 : "");
},"15":function(container,depth0,helpers,partials,data) {
    return "                <i class=\"far fa-user-tag\" focusable=\"false\"></i>\r\n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.type : depth0),"role_select",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(18, data, 0),"inverse":container.program(20, data, 0),"data":data})) != null ? stack1 : "");
},"18":function(container,depth0,helpers,partials,data) {
    return "                <i class=\"fas fa-tag\" focusable=\"false\"></i>\r\n";
},"20":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.type : depth0),"partneradvanced",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(21, data, 0),"inverse":container.program(23, data, 0),"data":data})) != null ? stack1 : "");
},"21":function(container,depth0,helpers,partials,data) {
    return "                <i class=\"far fa-handshake\" focusable=\"false\"></i>\r\n";
},"23":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.type : depth0),"partnerelevated",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(24, data, 0),"inverse":container.program(26, data, 0),"data":data})) != null ? stack1 : "");
},"24":function(container,depth0,helpers,partials,data) {
    return "                <i class=\"fad fa-handshake\" focusable=\"false\"></i>\r\n";
},"26":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.type : depth0),"customer",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(21, data, 0),"inverse":container.program(27, data, 0),"data":data})) != null ? stack1 : "");
},"27":function(container,depth0,helpers,partials,data) {
    return "                <i class=\"fal fa-question-circle\" focusable=\"false\"></i>\r\n            ";
},"29":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.domainName : depth0),{"name":"if","hash":{},"fn":container.program(30, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"30":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "\r\n            <div class=\"realusername\">\r\n                <span class=\"realusernameIntro\">"
    + ((stack1 = (helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"introDOmain",{"name":"get_main","hash":{},"data":data})) != null ? stack1 : "")
    + "</span><span class=\"searchcontent\">"
    + ((stack1 = ((helper = (helper = helpers.domainName || (depth0 != null ? depth0.domainName : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"domainName","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\r\n            </div>\r\n            ";
},"32":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div class=\"realusername\"><span class=\"searchcontent\">"
    + ((stack1 = ((helper = (helper = helpers.realusername || (depth0 != null ? depth0.realusername : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"realusername","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span></div>";
},"34":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "<div class=\"sessionDate gray\"> <span> "
    + container.escapeExpression((helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"label",{"name":"get_main","hash":{},"data":data}))
    + " </span> <span class=\"searchcontent\">"
    + ((stack1 = ((helper = (helper = helpers["last-session"] || (depth0 != null ? depth0["last-session"] : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"last-session","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span> </div>";
},"36":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "<div class=\"roleDesc\"><span class=\"searchcontent\">"
    + ((stack1 = ((helper = (helper = helpers.roledesc || (depth0 != null ? depth0.roledesc : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"roledesc","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span></div>";
},"38":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.contractType : depth0),{"name":"if","hash":{},"fn":container.program(39, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"39":function(container,depth0,helpers,partials,data) {
    var stack1, helper;

  return "\r\n            <div class=\"roleDesc\"><span class=\"searchcontent\">"
    + ((stack1 = ((helper = (helper = helpers.contractType || (depth0 != null ? depth0.contractType : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"contractType","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span></div>\r\n            ";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "<button name=\""
    + container.escapeExpression(((helper = (helper = helpers.btnValue || (depth0 != null ? depth0.btnValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"btnValue","hash":{},"data":data}) : helper)))
    + "\" type=\"submit\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.disabled : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\r\n    <div class=\"cardBlock columns is-full is-mobile\">\r\n        <div class=\"column is-2 leftColIcon alignCenter\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"standard",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "        </div>\r\n        <div class=\"column rghtColInf alignCenter has-text-left\">\r\n            <h5 class=\"sessionEmail blue\">\r\n                <span class=\"sessionEmailIntro\">"
    + ((stack1 = (helpers.get_main || (depth0 && depth0.get_main) || alias2).call(alias1,"introSession",{"name":"get_main","hash":{},"data":data})) != null ? stack1 : "")
    + "</span><span class=\"searchcontent\">"
    + ((stack1 = ((helper = (helper = helpers.sessionName || (depth0 != null ? depth0.sessionName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"sessionName","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "</span>\r\n            </h5>\r\n            "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"customer",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(29, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isrole : depth0),{"name":"if","hash":{},"fn":container.program(32, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0["last-session"] : depth0),{"name":"if","hash":{},"fn":container.program(34, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.roledesc : depth0),{"name":"if","hash":{},"fn":container.program(36, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = helpers["if"].call(alias1,(helpers.eq || (depth0 && depth0.eq) || alias2).call(alias1,(depth0 != null ? depth0.type : depth0),"customer",{"name":"eq","hash":{},"data":data}),{"name":"if","hash":{},"fn":container.program(38, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n        </div>\r\n    </div>\r\n</button>";
},"useData":true});
})();