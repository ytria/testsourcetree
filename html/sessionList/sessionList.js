var isDebug = !isInApp; // always Debug when in IE - isInApp is set in hbshandler.js

// This is called at the end of Ready
function checkContentHeight(isInit) {
    // We need to wait that fontawesome did finish...
    isFontAwesome = false;
    if (!isInit) { // if this is not the first time, then we are fontawesome for sure...
        if (isDebug) console.log('--- Content Height --- NOT Init');
        isFontAwesome = true;
    } else if( ($('.fa').first().length + $('.far').first().length + $('.fas').first().length + $('.fad').first().length + $('.fal').first().length) > 0 ) {
        if (isDebug) console.log('--- Content Height --- IS FontAwesome');
        isFontAwesome = true;
    } else {
        if (isDebug) console.log('--- Content Height --- NOT FontAwesome ');
    }

    if (isFontAwesome && !$('html').hasClass('fontawesome-i2svg-complete'))	{
        // fontawesome-i2svg-active - fontawesome-i2svg-pending
        if (isDebug) console.log('--- Content Height --- Fontawesome NOT ready - call again');
        window.setTimeout('checkContentHeight(false)', 100);	
    } else {
        // compute the size of all containers
        var coreheight = $('#mainContainer').outerHeight();
        try {
            window.external.setContentHeight(coreheight);
        } catch (err) {
            if (isDebug) console.log('--- Content Height --- Error External - Real Height:'+ coreheight);
        }    
    }
}


$(document).ready(function(){

	$(document).on('click', '#showScopeData', function() {
        if ( $(this).prop('checked') )  {
            console.log("YES");
            $(this).parent().find('span.iconYes').removeClass('display-no');
            $(this).parent().find('span.iconNo').addClass('display-no');
        } else {
            console.log("NOT");
            $(this).parent().find('span.iconYes').addClass('display-no');
            $(this).parent().find('span.iconNo').removeClass('display-no');
        }
    });

	// $(document).on('mouseover', '.footerSelRole', function() {
    //     if ( !$('#showScopeData').prop('checked') )  {
    //         console.log("in - not");
    //         $(this).parent().find('span.iconYes').removeClass('display-no');
    //         $(this).parent().find('span.iconNo').addClass('display-no');
    //     }
    // });

	// $(document).on('mouseout', '.footerSelRole', function() {
    //     if ( !$('#showScopeData').prop('checked') )  {
    //         console.log("out - not");
    //         $(this).parent().find('span.iconYes').addClass('display-no');
    //         $(this).parent().find('span.iconNo').removeClass('display-no');
    //     }
    // });

    // **** Quick Search field on key
    $(document).on('keyup', 'input.quicksearch' , function() {
        tgtVal = $(this).val().toLowerCase();
        $('#sessionList button').each(function() {
            var theBtn = $(this);
            var isIn = true;
            if ('' != tgtVal) {
                isIn &= tgtVal.split(' ').every(function(elt) {
                    var isInBtn = false;
                    theBtn.find('.searchcontent').each(function(){
                        theVal = $(this).html().toLowerCase();
                        if (isDebug) console.log(theVal+" :: "+elt+" -- "+theVal.toLowerCase().indexOf(elt));
                        if(theVal.toLowerCase().indexOf(elt) > -1) { isInBtn  = true; }
                    })
                    return isInBtn;
                })
            }
            if (isIn) {
                theBtn.removeClass('display-no');
            } else {
                theBtn.addClass('display-no');
            }
        })
    })
    // We don't want the Enter to trigg in this quick search field
    $(document).on('keydown', 'input.quicksearch', function(event) {
        if ( event.which == 13 ) {
            // Enter Key
            event.preventDefault();
        } else if ( event.which == 27 ) {
            // Esc Key
            $(this).val('');
            event.preventDefault();
        }
    });

})