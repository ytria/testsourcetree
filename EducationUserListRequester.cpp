#include "EducationUserListRequester.h"

#include "EducationUser.h"
#include "EducationUserDeserializer.h"
#include "LoggerService.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "EducationUserDeserializerFactory.h"
#include "BasicPageRequestLogger.h"
#include "ODataFilter.h"
#include "MsGraphHttpRequestLogger.h"

EducationUserListRequester::EducationUserListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger)
	:m_Logger(p_Logger)
{
}

TaskWrapper<void> EducationUserListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	auto factory = std::make_unique<EducationUserDeserializerFactory>(p_Session);
	m_Deserializer = std::make_shared<ValueListDeserializer<EducationUser, EducationUserDeserializer>>(std::move(factory));

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("users"));

	ODataFilter filter;
	filter	.Equal(_YTEXT("primaryRole"), _YTEXT("student"), true)
			.Or()
			.Equal(_YTEXT("primaryRole"), _YTEXT("teacher"), true);
	filter.ApplyTo(uri);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	MsGraphPaginator paginator(uri.to_uri(), Util::CreateDefaultGraphPageRequester(m_Deserializer, httpLogger, p_TaskData.GetOriginator()), m_Logger);
	return paginator.Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

const vector<EducationUser>& EducationUserListRequester::GetData() const
{
	return m_Deserializer->GetData();
}
