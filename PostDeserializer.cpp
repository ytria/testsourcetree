#include "PostDeserializer.h"

#include "ItemBodyDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "RecipientDeserializer.h"

void PostDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        ItemBodyDeserializer ibd;
        if (JsonSerializeUtil::DeserializeAny(ibd, _YTEXT("body"), p_Object))
            m_Data.SetBody(ibd.GetData());
    }

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("categories"), p_Object))
            m_Data.SetCategories(lsd.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("changeKey"), m_Data.m_ChangeKey, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("conversationId"), m_Data.m_ConversationId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("conversationThreadId"), m_Data.m_ConversationThreadId, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.m_CreatedDateTime, p_Object);

    {
        RecipientDeserializer rd;
        if (JsonSerializeUtil::DeserializeAny(rd, _YTEXT("from"), p_Object))
            m_Data.SetFrom(rd.GetData());
    }

    JsonSerializeUtil::DeserializeBool(_YTEXT("hasAttachments"), m_Data.m_HasAttachments, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.m_LastModifiedDateTime, p_Object);

    {
        ListDeserializer<Recipient, RecipientDeserializer> recDeserializer;
        if (JsonSerializeUtil::DeserializeAny(recDeserializer, _YTEXT("newParticipants"), p_Object))
            m_Data.SetNewParticipants(recDeserializer.GetData());
    }

	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("receivedDateTime"), m_Data.m_ReceivedDateTime, p_Object);

    {
        RecipientDeserializer rd;
        if (JsonSerializeUtil::DeserializeAny(rd, _YTEXT("sender"), p_Object))
            m_Data.SetSender(rd.GetData());
    }
}
