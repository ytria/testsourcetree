#include "MailboxPermissionsRequester.h"

#include "IPowerShell.h"
#include "IPSObjectCollectionDeserializer.h"
#include "IRequestLogger.h"
#include "MailboxPermissionsCollectionDeserializer.h"
#include "Sapio365Session.h"

MailboxPermissionsRequester::MailboxPermissionsRequester(const std::shared_ptr<IPSObjectCollectionDeserializer>& p_Deserializer, const wstring& p_UserPrincipalName, const std::shared_ptr<IRequestLogger>& p_RequestLogger)
    : m_Deserializer(p_Deserializer),
    m_UserPrincipalName(p_UserPrincipalName),
    m_Logger(p_RequestLogger),
    m_Result(std::make_shared<InvokeResultWrapper>())
{
    if (!m_Deserializer)
        throw std::exception("MailboxPermissionsRequester: Deserializer cannot be nullptr");
}

TaskWrapper<void> MailboxPermissionsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    if (!p_Session->HasExchangePowerShellSession())
        return YSafeCreateTask([]() {});

    return YSafeCreateTaskMutable([result = m_Result, deserializer = m_Deserializer, session = p_Session->GetExchangePowershellSession(), userPrincipalName = m_UserPrincipalName, logger = m_Logger, taskData = p_TaskData]() {
        if (nullptr != session)
        {
            logger->Log(taskData.GetOriginator());
		
            wstring script(_YTEXT("Get-MailboxPermission "));
		    script += userPrincipalName;
		    session->AddScript(script.c_str(), taskData.GetOriginator());

            result->SetResult(session->Invoke(taskData.GetOriginator()));
            if (result->Get().m_Success && nullptr != result->Get().m_Collection)
                result->Get().m_Collection->Deserialize(*deserializer);
        }
    });
}

const std::shared_ptr<InvokeResultWrapper>& MailboxPermissionsRequester::GetResult() const
{
    return m_Result;
}
