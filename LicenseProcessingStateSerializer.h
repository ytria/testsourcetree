#pragma once

#include "IJsonSerializer.h"
#include "LicenseProcessingState.h"

class LicenseProcessingStateSerializer : public IJsonSerializer
{
public:
    LicenseProcessingStateSerializer(const LicenseProcessingState& p_LicenseProcessingState);
    void Serialize() override;

private:
    const LicenseProcessingState& m_LicenseProcessingState;
};
