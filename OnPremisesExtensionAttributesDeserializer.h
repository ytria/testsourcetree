#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "OnPremisesExtensionAttributes.h"

class OnPremisesExtensionAttributesDeserializer : public JsonObjectDeserializer, public Encapsulate<OnPremisesExtensionAttributes>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};
