#include "DlgSelectSubscriptionAndResourceGroup.h"

#include "Product.h"

DlgSelectSubscriptionAndResourceGroup::DlgSelectSubscriptionAndResourceGroup(const Mode p_Mode, const map<Azure::Subscription, vector<Azure::ResourceGroup>>& p_Choices, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent)
	: ResizableDialog(IDD, p_Parent)
	, m_Choices(p_Choices)
	, m_Session(p_Session)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_Mode(p_Mode)
{
	// TODO: Load hbs sizes (see DlgEnterCosmosCNX.cpp)
	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_COSMOSDBINFO_SIZE, g_Sizes);
		ASSERT(success);
	}
}

bool DlgSelectSubscriptionAndResourceGroup::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	const auto it = data.find(_YTEXT("cancel"));
	if (data.end() != it)
	{
		endDialogFixed(IDCANCEL);
		return false;
	}

	auto itt = data.find(g_SubscriptionIdKey);
	ASSERT(itt != data.end());
	if (itt != data.end())
	{
		auto resGroupNameItt = data.find(itt->second);
		ASSERT(resGroupNameItt != data.end());
		if (resGroupNameItt != data.end())
		{
			m_SubscriptionId = itt->second;
			m_ResGroupName = resGroupNameItt->second;
		}
	}

	if (!m_OnOkValidation || m_OnOkValidation(*this))
		endDialogFixed(IDOK);
	return false;
}

const wstring& DlgSelectSubscriptionAndResourceGroup::GetSubscriptionId() const
{
	return m_SubscriptionId;
}

const wstring& DlgSelectSubscriptionAndResourceGroup::GetResGroupName() const
{
	return m_ResGroupName;
}

void DlgSelectSubscriptionAndResourceGroup::SetOnOkValidation(OnOkValidation p_OnOkValid)
{
	m_OnOkValidation = p_OnOkValid;
}

BOOL DlgSelectSubscriptionAndResourceGroup::OnInitDialogSpecificResizable()
{
	switch (m_Mode)
	{
	case Mode::CREATION:
		SetWindowText(YtriaTranslate::Do(DlgSelectSubscriptionAndResourceGroup_OnInitDialogSpecificResizable_1, _YLOC("Set Cosmos DB Account - Select Subscription")).c_str());
		break;
	case Mode::DELETION:
		SetWindowText(YtriaTranslate::Do(DlgDeleteCosmosDbAccount_OnInitDialogSpecificResizable_1, _YLOC("Delete Cosmos DB Account")).c_str());
		break;
	case Mode::SELECTION:
		SetWindowText(YtriaTranslate::Do(DlgSelectSubscriptionAndResourceGroup_OnInitDialogSpecificResizable_1, _YLOC("Set Cosmos DB Account - Select Subscription")).c_str());
		break;
	default:
		ASSERT(false);
		break;
	}

	CWnd* pCtrl = GetDlgItem(IDC_GRID_PLACEHOLDER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, NULL);

	m_HtmlView->SetLinkOpeningPolicy(true, true);
	m_HtmlView->SetPostedDataTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
		{
			{ _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
			,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-cosmosDbInfo.js"), _YTEXT("") } } // This file is in resources
			,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("cosmosDbInfo.js"), _YTEXT("") } }, // This file is in resources
		}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("cosmosDbInfo.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-height"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-width"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);

	BlockVResize(true);

	return TRUE;
}

void DlgSelectSubscriptionAndResourceGroup::generateJSONScriptData(wstring& p_Output)
{
	auto main = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("DlgNewCosmosDbAccount")) },
		{ _YTEXT("method"), web::json::value::string(_YTEXT("POST")) },
		{ _YTEXT("action"), web::json::value::string(_YTEXT("#")) },
		});

	wstring text;
	switch (m_Mode)
	{
	case Mode::CREATION:
		text = _T("Please choose the subscription for which the Cosmos DB account will be created:");
		break;
	case Mode::DELETION:
		text = _T("Please choose the subscription from which the Cosmos DB account will be deleted:");
		break;
	case Mode::SELECTION:
		text = _T("Please choose the subscription from which the Cosmos DB account will be selected:");
		break;
	default:
		ASSERT(false);
		break;
	}

	auto items = web::json::value::array();
		items.as_array()[0] = web::json::value::object({
			{ _YTEXT("hbs"), web::json::value::string(_YTEXT("cosmosDbSelect")) },
			{ _YTEXT("introTextOne"), web::json::value::string(text) },
			{ _YTEXT("subscriptionField"), web::json::value::string(g_SubscriptionIdKey) },
			{ _YTEXT("subscriptionLabel"), web::json::value::string(YtriaTranslate::Do(DlgSelectSubscriptionAndResourceGroup_generateJSONScriptData_6, _YLOC("Available Subscriptions")).c_str()) },
			{ _YTEXT("noSizeLimit"), web::json::value::string(_YTEXT("X")) },
			{ _YTEXT("allResGroupLabel"), web::json::value::string(YtriaTranslate::Do(DlgSelectSubscriptionAndResourceGroup_generateJSONScriptData_7, _YLOC("Available Resource Groups")).c_str()) },
	});

	AddDynamicFields(items.as_array()[0].as_object());

	items.as_array()[1] = web::json::value::object({
		{ _YTEXT("hbs"), web::json::value::string(_YTEXT("Button")) },
		{
			_YTEXT("choices"), web::json::value::array({
				web::json::value::object({
					{ _YTEXT("type"), web::json::value::string(_YTEXT("submit")) },
					{ _YTEXT("label"), web::json::value::string(_YTEXT("OK")) },
					{ _YTEXT("attribute"), web::json::value::string(_YTEXT("post")) },
					{ _YTEXT("name"), web::json::value::string(_YTEXT("ok")) },
					{ _YTEXT("class"), web::json::value::string(_YTEXT("is-primary")) }
				}),
				web::json::value::object({
					{ _YTEXT("type"), web::json::value::string(_YTEXT("submit")) },
					{ _YTEXT("label"), web::json::value::string(_YTEXT("Cancel")) },
					{ _YTEXT("name"), web::json::value::string(_YTEXT("cancel")) },
					{ _YTEXT("attribute"), web::json::value::string(_YTEXT("cancel")) },
				})
			})
		}
	});

	auto root = web::json::value::object({
		{ _YTEXT("main"), main },
		{ _YTEXT("items"), items }
	});

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}

void DlgSelectSubscriptionAndResourceGroup::AddDynamicFields(web::json::object& p_Object)
{
	p_Object[_YTEXT("subscriptionChoice")] = web::json::value::array();
	auto& arrSubscriptions = p_Object[_YTEXT("subscriptionChoice")].as_array();

	p_Object[_YTEXT("allResGroupChoice")] = web::json::value::array();
	auto& arrResGroups = p_Object[_YTEXT("allResGroupChoice")].as_array();

	size_t iSubscriptions = 0;
	for (const auto& keyVal : m_Choices)
	{
		const auto& subscription = keyVal.first;
		const auto& resGroups = keyVal.second;

		arrSubscriptions[iSubscriptions] = web::json::value::object({
			{ _YTEXT("value"), web::json::value::string(subscription.m_SubscriptionId ? *subscription.m_SubscriptionId : _YTEXT("")) },
			{ _YTEXT("label"), web::json::value::string(subscription.m_DisplayName ? *subscription.m_DisplayName : _YTEXT("")) },
			{ _YTEXT("default"), web::json::value::string(iSubscriptions == 0 ? _YTEXT("X") : _YTEXT("")) }
		}, true);

		arrResGroups[iSubscriptions] = web::json::value::object({
			{ _YTEXT("resgroupField"), web::json::value::string(subscription.m_SubscriptionId ? *subscription.m_SubscriptionId : _YTEXT("")) },
			{ _YTEXT("noSizeLimit"), web::json::value::string(_YTEXT("X")) },
			{ _YTEXT("resgroupChoice"), web::json::value::array() }
		}, true);

		auto& choices = arrResGroups[iSubscriptions].as_object()[_YTEXT("resgroupChoice")].as_array();
		iSubscriptions++;

		bool first = true;
		size_t iResGroups = 0;
		for (const auto& resGroup : resGroups)
		{
			choices[iResGroups++] = web::json::value::object({
				{ _YTEXT("value"), web::json::value::string(resGroup.m_Name ? *resGroup.m_Name : _YTEXT("")) },
				{ _YTEXT("label"), web::json::value::string(resGroup.m_Name ? *resGroup.m_Name : _YTEXT("")) },
				{ _YTEXT("default"), web::json::value::string(first ? _YTEXT("X") : _YTEXT("")) }
			}, true);
			first = false;
		}
	}
}

const wstring DlgSelectSubscriptionAndResourceGroup::g_SubscriptionIdKey = _YTEXT("SUBSCRIPTIONID");

std::map<wstring, int, Str::keyLessInsensitive> DlgSelectSubscriptionAndResourceGroup::g_Sizes;
