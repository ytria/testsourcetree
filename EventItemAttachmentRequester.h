#pragma once

#include "IRequester.h"
#include "IRequestLogger.h"

class BusinessItemAttachment;
class ItemAttachmentDeserializer;

class EventItemAttachmentRequester : public IRequester
{
public:
    enum class EntityType { User, Group };
    EventItemAttachmentRequester(EntityType p_Type, PooledString p_EntityId, PooledString p_EventId, PooledString p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger);

    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
    const BusinessItemAttachment& GetData() const;

private:
    std::shared_ptr<ItemAttachmentDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;

    EntityType m_Type;
    PooledString m_EntityId;
    PooledString m_EventId;
    PooledString m_AttachmentId;
};

