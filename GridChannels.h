#pragma once

#include "BaseO365Grid.h"
#include "BusinessChannel.h"
#include "GridTemplateGroups.h"

class FrameChannels;

class GridChannels : public ModuleO365Grid<BusinessChannel>
{
public:
    GridChannels();

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;
    void BuildView(const O365DataMap<BusinessGroup, std::vector<BusinessChannel>>& p_Channels, const vector<BusinessChannel>& p_LoadedMoreChannels, bool p_FullPurge);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	void UpdateChannelsLoadMore(const vector<BusinessChannel>& p_Channels);
	vector<BusinessChannel> GetLoadMoreRequestInfo(const std::vector<GridBackendRow*>& p_Rows);

protected:
    virtual void customizeGrid() override;
	void customizeGridPostProcess() override;

    virtual BusinessChannel getBusinessObject(GridBackendRow* p_Row) const override;

protected:
	HACCEL m_hAccelSpecific = nullptr;

private:
    GridBackendRow* FillRow(GridBackendRow* p_ParentRow, const BusinessChannel& p_Channel);
    DECLARE_MESSAGE_MAP()

	afx_msg void OnShowMessages();
	afx_msg void OnUpdateShowMessages(CCmdUI* pCmdUI);

	afx_msg void OnShowMembers();
	afx_msg void OnUpdateShowMembers(CCmdUI* pCmdUI);

	afx_msg void OnShowSites();
	afx_msg void OnUpdateShowSites(CCmdUI* pCmdUI);

	afx_msg void OnShowDriveItems();
	afx_msg void OnUpdateShowDriveItems(CCmdUI* pCmdUI);

	afx_msg void OnChannelLoadMore();
	afx_msg void OnUpdateChannelLoadMore(CCmdUI* pCmdUI);

	virtual BOOL PreTranslateMessage(MSG* pMsg) override;

	virtual void InitializeCommands() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart) override;
	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;
	virtual bool IsRowForLoadMore(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

private:
	GridTemplateGroups m_Template;
	GridBackendColumn* m_ColDisplayName;
    GridBackendColumn* m_ColDescription;
	GridBackendColumn* m_ColEmail;
	GridBackendColumn* m_ColIsFavoriteByDefault;
	GridBackendColumn* m_ColWebUrl;
	GridBackendColumn* m_ColMembershipType;

	GridBackendColumn* m_ColPrivateChannelMembersDisplayName;
	GridBackendColumn* m_ColPrivateChannelMembersUserID;
	//GridBackendColumn* m_ColPrivateChannelMembersEmail;

	GridBackendColumn* m_ColFolderId;
	GridBackendColumn* m_ColFolderWebURL;
	GridBackendColumn* m_ColFolderCreatedDateTime;
	GridBackendColumn* m_ColFolderLastModifiedDateTime;
	//GridBackendColumn* m_ColFolderSharepointIDsListID;
	//GridBackendColumn* m_ColFolderSharepointIDsSiteID;
	//GridBackendColumn* m_ColFolderSharepointIDsSiteURL;
	//GridBackendColumn* m_ColFolderSharepointIDsWebID;

	GridBackendColumn* m_ColDriveName;
	GridBackendColumn* m_ColDriveId;
	GridBackendColumn* m_ColDriveDescription;
	GridBackendColumn* m_ColDriveQuotaState;
	GridBackendColumn* m_ColDriveQuotaUsed;
	GridBackendColumn* m_ColDriveQuotaRemaining;
	GridBackendColumn* m_ColDriveQuotaDeleted;
	GridBackendColumn* m_ColDriveQuotaTotal;
	GridBackendColumn* m_ColDriveCreationTime;
	GridBackendColumn* m_ColDriveLastModifiedDateTime;
	GridBackendColumn* m_ColDriveType;
	GridBackendColumn* m_ColDriveWebUrl;
	GridBackendColumn* m_ColDriveCreatedByUserEmail;
	GridBackendColumn* m_ColDriveCreatedByUserId;
	GridBackendColumn* m_ColDriveCreatedByAppName;
	GridBackendColumn* m_ColDriveCreatedByAppId;
	GridBackendColumn* m_ColDriveCreatedByDeviceName;
	GridBackendColumn* m_ColDriveCreatedByDeviceId;
	GridBackendColumn* m_ColDriveOwnerUserName;
	GridBackendColumn* m_ColDriveOwnerUserId;
	GridBackendColumn* m_ColDriveOwnerUserEmail;
	GridBackendColumn* m_ColDriveOwnerAppName;
	GridBackendColumn* m_ColDriveOwnerAppId;
	GridBackendColumn* m_ColDriveOwnerDeviceName;
	GridBackendColumn* m_ColDriveOwnerDeviceId;

	vector<GridBackendColumn*>	m_MetaColumns;
	bool m_HasLoadMoreAdditionalInfo;
	O365IdsContainer m_MetaIDsMoreLoaded;

    FrameChannels* m_FrameChannels;
};

