#include "FakeModification.h"

#include "BaseO365Grid.h"

FakeModification::FakeModification(O365Grid& p_Grid, const row_pk_t& p_RowKey) : RowModification(p_Grid, p_RowKey)
{
	m_State = Modification::State::RemoteHasNewValue;
}

FakeModification::~FakeModification()
{
	GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

	ASSERT(nullptr != row);
	if (nullptr != row && row->IsChangesSaved())
	{
		// Remove all status flags from the row.
		getGrid().OnRowNothingToSave(row, false);
	}
}

vector<Modification::ModificationLog> FakeModification::GetModificationLogs() const
{
	ASSERT(false);
	return {};
}

void FakeModification::RefreshState()
{
	// Nothing to do.
}
