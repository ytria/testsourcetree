#include "WebApplicationDeserializer.h"
#include "ListDeserializer.h"
#include "ImplicitGrantSettingsDeserializer.h"

void WebApplicationDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("homePageUrl"), m_Data.m_HomePageUrl, p_Object);
	{
		ImplicitGrantSettingsDeserializer grantSettingsDeserializer;
		if (JsonSerializeUtil::DeserializeAny(grantSettingsDeserializer, _YTEXT("implicitGrantSettings"), p_Object))
			m_Data.m_ImplicitGrantSettings = std::move(grantSettingsDeserializer.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("logoutUrl"), m_Data.m_LogoutUrl, p_Object);
	{
		ListStringDeserializer redirectUrisDeserializer;
		if (JsonSerializeUtil::DeserializeAny(redirectUrisDeserializer, _YTEXT("redirectUris"), p_Object))
			m_Data.m_RedirectUris = std::move(redirectUrisDeserializer.GetData());
	}
}
