#pragma once

#include "BaseO365Grid.h"
#include "RoleDefinition.h"

class FrameSiteRoles;

class GridSiteRoles : public ModuleO365Grid<Sp::RoleDefinition>
{
public:
    GridSiteRoles();
    virtual ~GridSiteRoles() override;

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildView(const O365DataMap<BusinessSite, vector<Sp::RoleDefinition>>& p_SiteRoles, bool p_FullPurge);

    virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
    void customizeGrid() override;

    Sp::RoleDefinition getBusinessObject(GridBackendRow*) const override;
    void			   UpdateBusinessObjects(const vector<Sp::RoleDefinition>& p_SiteRoles, bool p_SetModifiedStatus) override;
    void			   RemoveBusinessObjects(const vector<Sp::RoleDefinition>& p_SiteRoles) override;

private:
    DECLARE_MESSAGE_MAP()

    void InitializeCommands() override;
    void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart) override;

    void FillSiteRolefields(const BusinessSite& p_Site, const Sp::RoleDefinition& p_SpRole, GridBackendRow* p_Row, GridUpdater& p_Updater);

    GridBackendColumn* m_ColMetaId;
    GridBackendColumn* m_ColMetaDisplayName;
    GridBackendColumn* m_ColMetaUserDisplayName;

    GridBackendColumn* m_ColDescription;
    GridBackendColumn* m_ColHidden;
    GridBackendColumn* m_ColName;
    GridBackendColumn* m_ColOrder;
    GridBackendColumn* m_ColRoleTypeKind;
    GridBackendColumn* m_ColBasePermissionsHigh;
    GridBackendColumn* m_ColBasePermissionsLow;

    FrameSiteRoles* m_Frame;
};
