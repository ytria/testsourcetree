#pragma once

namespace Azure
{
	class Usage
	{
	public:
		boost::YOpt<int32_t> m_CurrentValue;
		boost::YOpt<int32_t> m_Limit;
		boost::YOpt<PooledString> m_Name;
		boost::YOpt<PooledString> m_QuotaPeriod;
		boost::YOpt<PooledString> m_Unit;
	};
}