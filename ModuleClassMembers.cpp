#include "ModuleClassMembers.h"
#include "CommandShowClassMembers.h"
#include "FrameClassMembers.h"
#include "RefreshSpecificData.h"
#include "AddClassMemberRequester.h"
#include "RemoveClassMemberRequester.h"

void ModuleClassMembers::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
	case Command::ModuleTask::List:
		showClassMembers(p_Command);
		break;
	case Command::ModuleTask::ApplyChanges:
		applyChanges(p_Command);
	}
}

void ModuleClassMembers::showClassMembers(const Command& p_Command)
{
	AutomationAction* p_Action = p_Command.GetAutomationAction();
	const CommandInfo& info = p_Command.GetCommandInfo();
	const auto p_Origin = info.GetOrigin();

	RefreshSpecificData refreshSpecificData;
	bool isUpdate = true;

	auto p_SourceWindow = p_Command.GetSourceWindow();
	auto sourceCWnd = p_SourceWindow.GetCWnd();

	FrameClassMembers* frame = dynamic_cast<FrameClassMembers*>(info.GetFrame());
	if (nullptr == frame)
	{
		isUpdate = false;
		ASSERT(!p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>());

		if (FrameClassMembers::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin = p_Origin;
			moduleCriteria.m_UsedContainer = ModuleCriteria::UsedContainer::IDS;
			moduleCriteria.m_IDs = p_Command.GetCommandInfo().GetIds();
			moduleCriteria.m_Privilege = info.GetRBACPrivilege();

			if (ShouldCreateFrame<FrameClassMembers>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				frame = new FrameClassMembers(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), _T("Class Members"), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
		}
	}

	if (nullptr != frame)
	{
		ASSERT(p_Command.GetCommandInfo().Data().is_type<vector<EducationSchool>>());
		if (p_Command.GetCommandInfo().Data().is_type<vector<EducationSchool>>())
		{
			auto schools = p_Command.GetCommandInfo().Data().get_value<vector<EducationSchool>>();
			CommandShowClassMembers command(schools, *frame, p_Command.GetAutomationSetup(), p_Command.GetAutomationAction(), p_Command.GetLicenseContext(), isUpdate);
			command.Execute();
		}
	}
	else
	{
		SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
}

void ModuleClassMembers::applyChanges(const Command& p_Command)
{
	auto action = p_Command.GetAutomationAction();

	const auto& info = p_Command.GetCommandInfo();
	auto frame = dynamic_cast<FrameClassMembers*>(info.GetFrame());

	ASSERT(info.Data().is_type<ClassMembersChanges>());
	if (info.Data().is_type<ClassMembersChanges>())
	{
		const auto& changes = info.Data().get_value<ClassMembersChanges>();
		
		YtriaTaskData taskData;
		if (!changes.m_MemberAdditions.empty() || !changes.m_MemberRemovals.empty())
			taskData = addFrameTask(_T("Applying changes"), frame, p_Command, false);

		YSafeCreateTask([bom = frame->GetBusinessObjectManager(), hwnd = frame->GetSafeHwnd(), taskData, changes, action] ()
		{
			vector<O365UpdateOperation> updates;
			for (const auto& memberAdd : changes.m_MemberAdditions)
			{
				if (taskData.IsCanceled())
					break;

				AddClassMemberRequester requester(memberAdd.m_ClassId, memberAdd.m_UserId);
				requester.Send(bom->GetSapio365Session(), taskData).GetTask().get();

				updates.emplace_back(memberAdd.m_RowPk);
				updates.back().StoreResult(requester.GetResult());
			}

			for (const auto& memberRem : changes.m_MemberRemovals)
			{
				if (taskData.IsCanceled())
					break;

				RemoveClassMemberRequester requester(memberRem.m_ClassId, memberRem.m_UserId);
				requester.Send(bom->GetSapio365Session(), taskData).GetTask().get();

				updates.emplace_back(memberRem.m_RowPk);
				updates.back().StoreResult(requester.GetResult());
			}

			YCallbackMessage::DoPost([action, taskData, updates, hwnd]() mutable
			{
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameClassMembers*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (nullptr != frame)
				{
					frame->GetGrid().ClearLog(taskData.GetId());
					frame->RefreshAfterUpdate(std::move(updates), action);
				}
				else
					SetAutomationGreenLight(action);
				TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
			});
		});
	}
}
