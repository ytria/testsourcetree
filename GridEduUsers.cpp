#include "GridEduUsers.h"
#include "AutomationWizardEduUsers.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"
#include "BasicGridSetup.h"
#include "FrameEduUsers.h"
#include "DlgEducationUserEditHTML.h"

BEGIN_MESSAGE_MAP(GridEduUsers, ModuleO365Grid<EducationUser>)
	ON_COMMAND(ID_MODULEGRID_EDIT, OnEdit)
	ON_UPDATE_COMMAND_UI(ID_MODULEGRID_EDIT, OnUpdateEdit)
END_MESSAGE_MAP()

GridEduUsers::GridEduUsers()
	:m_ColDisplayName(nullptr),
	m_ColMiddleName(nullptr),
	m_ColPrimaryRole(nullptr),
	m_ColExternalSource(nullptr),
	m_ColCreatedByAppDisplayName(nullptr),
	m_ColCreatedByAppId(nullptr),
	m_ColCreatedByAppEmail(nullptr),
	m_ColCreatedByAppIdentityProvider(nullptr),
	m_ColCreatedByDeviceDisplayName(nullptr),
	m_ColCreatedByDeviceId(nullptr),
	m_ColCreatedByDeviceEmail(nullptr),
	m_ColCreatedByDeviceIdentityProvider(nullptr),
	m_ColCreatedByUserDisplayName(nullptr),
	m_ColCreatedByUserId(nullptr),
	m_ColCreatedByUserEmail(nullptr),
	m_ColCreatedByUserIdentityProvider(nullptr),
	m_ColMailingAddressCity(nullptr),
	m_ColMailingAddressCountryOrRegion(nullptr),
	m_ColMailingAddressPostalCode(nullptr),
	m_ColMailingAddressState(nullptr),
	m_ColMailingAddressStreet(nullptr),
	m_ColResidenceAddressCity(nullptr),
	m_ColResidenceAddressCountryOrRegion(nullptr),
	m_ColResidenceAddressPostalCode(nullptr),
	m_ColResidenceAddressState(nullptr),
	m_ColResidenceAddressStreet(nullptr),
	m_ColRelatedContactId(nullptr),
	m_ColRelatedContactDisplayName(nullptr),
	m_ColRelatedContactEmailAddress(nullptr),
	m_ColRelatedContactMobilePhone(nullptr),
	m_ColRelatedContactRelationship(nullptr),
	m_ColRelatedContactAccessContent(nullptr),
	m_ColStudentBirthDate(nullptr),
	m_ColStudentExternalId(nullptr),
	m_ColStudentGender(nullptr),
	m_ColStudentGrade(nullptr),
	m_ColStudentGraduationYear(nullptr),
	m_ColStudentNumber(nullptr),
	m_ColTeacherExternalId(nullptr),
	m_ColTeacherNumber(nullptr)
{
	UseDefaultActions(O365Grid::ACTION_EDIT);
	EnableGridModifications();

	initWizard<AutomationWizardEduUsers>(_YTEXT("Automation\\EduUsers"));
}

GridEduUsers::~GridEduUsers()
{
	GetBackend().Clear();
}

void GridEduUsers::BuildView(const vector<EducationUser>& p_EduUsers, const vector<O365UpdateOperation>& p_Updates, bool p_FullPurge)
{
	if (p_EduUsers.size() == 1 && p_EduUsers[0].GetError() != boost::none && p_EduUsers[0].GetID().IsEmpty())
	{
		HandlePostUpdateError(p_EduUsers[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_EduUsers[0].GetError()->GetFullErrorMessage());
	}
	else
	{
		GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
			| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
			| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			| GridUpdaterOptions::REFRESH_PROCESS_DATA
		);
		GridUpdater updater(*this, options, p_Updates);

		for (const auto& eduUser : p_EduUsers)
		{
			auto rowPk = vector<GridBackendField>{ GridBackendField(eduUser.GetID(), m_ColId) };
			auto userRow = m_RowIndex.GetRow(rowPk, true, true);
			updater.GetOptions().AddRowWithRefreshedValues(userRow);
			updater.GetOptions().AddPartialPurgeParentRow(userRow);
			updater.AddUpdatedRowPk(rowPk);

			FillEducationUserFields(userRow, eduUser);

		}
	}
}

void GridEduUsers::FillEducationUserFields(GridBackendRow* userRow, const EducationUser& eduUser)
{
	// Simple values
	userRow->AddField(eduUser.GetID(), m_ColId);
	userRow->AddField(eduUser.GetDisplayName(), m_ColDisplayName);
	userRow->AddField(eduUser.GetMiddleName(), m_ColMiddleName);
	userRow->AddField(eduUser.GetPrimaryRole(), m_ColPrimaryRole);

	// Created By
	if (eduUser.GetCreatedBy())
	{
		if (eduUser.GetCreatedBy()->Application)
		{
			const auto& app = *eduUser.GetCreatedBy()->Application;
			userRow->AddField(app.DisplayName, m_ColDisplayName);
			userRow->AddField(app.Id, m_ColCreatedByAppId);
			userRow->AddField(app.Email, m_ColCreatedByAppDisplayName);
			userRow->AddField(app.IdentityProvider, m_ColCreatedByAppIdentityProvider);
		}
		if (eduUser.GetCreatedBy()->Device)
		{
			const auto& device = *eduUser.GetCreatedBy()->Device;
			userRow->AddField(device.DisplayName, m_ColDisplayName);
			userRow->AddField(device.Id, m_ColCreatedByAppId);
			userRow->AddField(device.Email, m_ColCreatedByAppDisplayName);
			userRow->AddField(device.IdentityProvider, m_ColCreatedByAppIdentityProvider);
		}
		if (eduUser.GetCreatedBy()->User)
		{
			const auto& user = *eduUser.GetCreatedBy()->User;
			userRow->AddField(user.DisplayName, m_ColDisplayName);
			userRow->AddField(user.Id, m_ColCreatedByAppId);
			userRow->AddField(user.Email, m_ColCreatedByAppDisplayName);
			userRow->AddField(user.IdentityProvider, m_ColCreatedByAppIdentityProvider);
		}
	}

	// Mailing address
	if (eduUser.GetMailingAddress())
	{
		const auto& mailingAddr = *eduUser.GetMailingAddress();
		userRow->AddField(mailingAddr.City, m_ColMailingAddressCity);
		userRow->AddField(mailingAddr.CountryOrRegion, m_ColMailingAddressCountryOrRegion);
		userRow->AddField(mailingAddr.PostalCode, m_ColMailingAddressPostalCode);
		userRow->AddField(mailingAddr.State, m_ColMailingAddressState);
		userRow->AddField(mailingAddr.Street, m_ColMailingAddressStreet);
	}

	// Residence address
	if (eduUser.GetResidenceAddress())
	{
		const auto& residenceAddr = *eduUser.GetResidenceAddress();
		userRow->AddField(residenceAddr.City, m_ColResidenceAddressCity);
		userRow->AddField(residenceAddr.CountryOrRegion, m_ColResidenceAddressCountryOrRegion);
		userRow->AddField(residenceAddr.PostalCode, m_ColResidenceAddressPostalCode);
		userRow->AddField(residenceAddr.State, m_ColResidenceAddressState);
		userRow->AddField(residenceAddr.Street, m_ColResidenceAddressStreet);
	}

	// Related contacts
	if (eduUser.GetRelatedContacts())
	{
		const auto& relatedContacts = *eduUser.GetRelatedContacts();

		vector<boost::YOpt<PooledString>> ids;
		vector<boost::YOpt<PooledString>> displayNames;
		vector<boost::YOpt<PooledString>> emailAddresses;
		vector<boost::YOpt<PooledString>> mobilePhones;
		vector<boost::YOpt<PooledString>> relationships;
		vector<boost::YOpt<bool>> accessConsents;

		for (const auto& contact : relatedContacts)
		{
			ids.push_back(contact.m_Id);
			displayNames.push_back(contact.m_DisplayName);
			emailAddresses.push_back(contact.m_EmailAddress);
			mobilePhones.push_back(contact.m_MobilePhone);
			relationships.push_back(contact.m_Relationship);
			accessConsents.push_back(contact.m_AccessConsent);
		}
	}

	// Education student
	if (eduUser.GetStudent())
	{
		const auto& student = *eduUser.GetStudent();
		userRow->AddField(student.m_BirthDate, m_ColStudentBirthDate);
		userRow->AddField(student.m_ExternalId, m_ColStudentExternalId);
		userRow->AddField(student.m_Gender, m_ColStudentGender);
		userRow->AddField(student.m_Grade, m_ColStudentGrade);
		userRow->AddField(student.m_GraduationYear, m_ColStudentGraduationYear);
		userRow->AddField(student.m_StudentNumber, m_ColStudentNumber);
		SetRowObjectType(userRow, student);
	}

	// Education teacher
	if (eduUser.GetTeacher())
	{
		const auto& teacher = *eduUser.GetTeacher();
		userRow->AddField(teacher.m_ExternalId, m_ColTeacherExternalId);
		userRow->AddField(teacher.m_TeacherNumber, m_ColTeacherNumber);
		SetRowObjectType(userRow, teacher);
	}
}

EduUsersChanges GridEduUsers::GetChanges(const vector<GridBackendRow*>& p_Rows)
{
	EduUsersChanges changes;

	for (auto row : p_Rows)
	{
		vector<GridBackendField> rowPk;
		GetRowPK(row, rowPk);

		auto updateMod = GetModifications().GetRowFieldModification(rowPk);
		ASSERT(nullptr != updateMod);
		if (nullptr != updateMod)
			AddUpdateMod(*updateMod, changes);
	}

	return changes;
}

EduUsersChanges GridEduUsers::GetChanges()
{
	EduUsersChanges changes;

	for (const auto& mod : GetModifications().GetRowFieldModifications())
	{
		if (mod->GetState() == Modification::State::AppliedLocally)
		{
			auto updateMod = mod.get();
			if (nullptr != updateMod)
				AddUpdateMod(*updateMod, changes);
		}
	}

	return changes;
}

void GridEduUsers::AddUpdateMod(const RowFieldUpdates<Scope::O365>& p_Mod, EduUsersChanges& p_Changes)
{
	auto rowPk = p_Mod.GetRowKey();

	auto row = m_RowIndex.GetRow(rowPk, false, false);
	if (nullptr != row)
	{
		ASSERT(GridUtil::isBusinessType<EducationUser>(row));
		if (GridUtil::isBusinessType<EducationUser>(row))
		{
			EduUserUpdate update;
			update.m_RowPk = rowPk;
			update.m_UserId = row->GetField(m_ColId).GetValueStr();

			auto newValues = json::value::object();
			for (const auto& fieldUpdate : p_Mod.GetFieldUpdates())
			{
				auto newField = fieldUpdate->GetNewValue();
				ASSERT(newField);
				if (newField)
				{
					auto thisModCol = GetColumnByID(fieldUpdate->GetColumnID());
					wstring newValueStr = fieldUpdate->GetNewValue()->GetValueStr();
					if (thisModCol == m_ColStudentBirthDate)
					{
						const auto& timeDate = fieldUpdate->GetNewValue()->GetValueTimeDate();
						newValueStr = TimeUtil::GetInstance().GetISO8601String(timeDate);
					}

					if (thisModCol == m_ColExternalSource)
						newValues.as_object()[_YTEXT("id")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColMailingAddressCity)
						UpdateMailingAddressField(_YTEXT("city"), newValueStr, newValues);
					else if (thisModCol == m_ColMailingAddressCountryOrRegion)
						UpdateMailingAddressField(_YTEXT("countryOrRegion"), newValueStr, newValues);
					else if (thisModCol == m_ColMailingAddressPostalCode)
						UpdateMailingAddressField(_YTEXT("postalCode"), newValueStr, newValues);
					else if (thisModCol == m_ColMailingAddressState)
						UpdateMailingAddressField(_YTEXT("state"), newValueStr, newValues);
					else if (thisModCol == m_ColMailingAddressStreet)
						UpdateMailingAddressField(_YTEXT("street"), newValueStr, newValues);
					else if (thisModCol == m_ColResidenceAddressCity)
						UpdateResidenceAddressField(_YTEXT("city"), newValueStr, newValues);
					else if (thisModCol == m_ColResidenceAddressCountryOrRegion)
						UpdateResidenceAddressField(_YTEXT("countryOrRegion"), newValueStr, newValues);
					else if (thisModCol == m_ColResidenceAddressPostalCode)
						UpdateResidenceAddressField(_YTEXT("postalCode"), newValueStr, newValues);
					else if (thisModCol == m_ColResidenceAddressState)
						UpdateResidenceAddressField(_YTEXT("state"), newValueStr, newValues);
					else if (thisModCol == m_ColResidenceAddressStreet)
						UpdateResidenceAddressField(_YTEXT("street"), newValueStr, newValues);
					else if (thisModCol == m_ColMiddleName)
						newValues.as_object()[_YTEXT("middleName")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColPrimaryRole)
						newValues.as_object()[_YTEXT("primaryRole")] = json::value::string(newValueStr);
					else if (thisModCol == m_ColStudentBirthDate)
						UpdateStudentField(_YTEXT("birthDate"), newValueStr, newValues);
					else if (thisModCol == m_ColStudentExternalId)
						UpdateStudentField(_YTEXT("externalId"), newValueStr, newValues);
					else if (thisModCol == m_ColStudentGender)
						UpdateStudentField(_YTEXT("gender"), newValueStr, newValues);
					else if (thisModCol == m_ColStudentGrade)
						UpdateStudentField(_YTEXT("grade"), newValueStr, newValues);
					else if (thisModCol == m_ColStudentGraduationYear)
						UpdateStudentField(_YTEXT("graduationYear"), newValueStr, newValues);
					else if (thisModCol == m_ColStudentNumber)
						UpdateStudentField(_YTEXT("studentNumber"), newValueStr, newValues);
					else if (thisModCol == m_ColTeacherExternalId)
						UpdateTeacherField(_YTEXT("externalId"), newValueStr, newValues);
					else if (thisModCol == m_ColTeacherNumber)
						UpdateTeacherField(_YTEXT("teacherNumber"), newValueStr, newValues);
				}
			}
			update.m_NewValues = newValues;
			p_Changes.m_Updates.push_back(update);
		}
	}
}

wstring GridEduUsers::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, m_ColDisplayName);
}

boost::YOpt<PooledString> GridEduUsers::GetStrValue(GridBackendRow& p_Row, GridBackendColumn* p_Col) const
{
	boost::YOpt<PooledString> val;

	PooledString strVal = p_Row.GetField(p_Col).GetValueStr();
	if (strVal != GridBackendUtil::g_NoValueString)
		val = strVal;

	return val;
}

boost::YOpt<YTimeDate> GridEduUsers::GetDateValue(GridBackendRow& p_Row, GridBackendColumn* p_Col) const
{
	boost::YOpt<YTimeDate> val;

	const auto& dateVal = p_Row.GetField(p_Col).GetValueTimeDate();
	if (dateVal != GridBackendUtil::g_NoValueDate)
		val = dateVal;

	return val;
}

void GridEduUsers::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameEduUsers, LicenseUtil::g_codeSapio365eduUsers/*,
			{
				{ &m_ColMetaDisplayName, g_TitleOwnerDisplayName, g_FamilyOwner },
				{ &m_ColMetaUserPrincipalName, g_TitleOwnerUserName, g_FamilyOwner },
				{ &m_ColMetaId, g_TitleOwnerID, g_FamilyOwner }
			}*/);
		setup.Setup(*this, false);
	}

	addColumnGraphID();
	setBasicGridSetupHierarchy({ { { m_ColId, 0 } } ,{ rttr::type::get<EducationUser>().get_id() }});

	// Basic
	m_ColDisplayName = AddColumn(_YUID("displayName"), _T("Display Name"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMiddleName = AddColumn(_YUID("middleName"), _T("Middle Name"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColPrimaryRole = AddColumn(_YUID("primaryRole"), _T("Primary role"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColExternalSource = AddColumn(_YUID("externalSource"), _T("External source"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Mailing Address
	m_ColMailingAddressCity = AddColumn(_YUID("mailingAddressCity"), _T("City - Mailing address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressCountryOrRegion = AddColumn(_YUID("mailingAddressCountryOrRegion"), _T("Country or region - Mailing address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressPostalCode = AddColumn(_YUID("mailingAddressPostalCode"), _T("Postal code - Mailing address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressState = AddColumn(_YUID("mailingAddressState"), _T("State - Mailing address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressStreet = AddColumn(_YUID("mailingAddressStreet"), _T("Street - Mailing address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Residence Address
	m_ColMailingAddressCity = AddColumn(_YUID("residenceAddressCity"), _T("City - Residence address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressCountryOrRegion = AddColumn(_YUID("residenceAddressCountryOrRegion"), _T("Country or region - Residence address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressPostalCode = AddColumn(_YUID("residenceAddressPostalCode"), _T("Postal code - Residence address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressState = AddColumn(_YUID("residenceAddressState"), _T("State - Residence address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressStreet = AddColumn(_YUID("residenceAddressStreet"), _T("Street - Residence address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Created By
	m_ColCreatedByAppDisplayName = AddColumn(_YUID("createdByAppDisplayName"), _T("Display name - Created by - App"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByAppId = AddColumn(_YUID("createdByAppId"), _T("Id - Created by - App"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByAppEmail = AddColumn(_YUID("createdByAppEmail"), _T("Email - Created by - App"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByAppIdentityProvider = AddColumn(_YUID("createdByAppIdentityProvider"), _T("Identity Provider - Created by - App"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceDisplayName = AddColumn(_YUID("createdByDeviceDisplayName"), _T("Display name - Created by - Device"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceId = AddColumn(_YUID("createdByDeviceId"), _T("Id - Created by - Device"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceEmail = AddColumn(_YUID("createdByDeviceEmail"), _T("Email - Created by - Device"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceIdentityProvider = AddColumn(_YUID("createdByDeviceIdentityProvider"), _T("Identity provider - Created by - Device"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserDisplayName = AddColumn(_YUID("createdByUserDisplayName"), _T("Display name - Created by - User"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserId = AddColumn(_YUID("createdByUserId"), _T("Id - Created by - User"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserEmail = AddColumn(_YUID("createdByUserEmail"), _T("Email - Created by - User"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserIdentityProvider = AddColumn(_YUID("createdByUserIdentityProvider"), _T("Identity provider - Created by - User"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Related Contacts
	m_ColRelatedContactId = AddColumn(_YUID("relatedContactId"), _T("Id - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColRelatedContactDisplayName = AddColumn(_YUID("relatedContactDisplayName"), _T("Display name - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColRelatedContactEmailAddress = AddColumn(_YUID("relatedContactEmailAddress"), _T("Email address - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColRelatedContactMobilePhone = AddColumn(_YUID("relatedContactMobilePhone"), _T("Mobile phone - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColRelatedContactRelationship = AddColumn(_YUID("relatedContactRelationship"), _T("Relationship - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColRelatedContactAccessContent = AddColumn(_YUID("relatedContactAccessContent"), _T("Access content - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Education Student
	m_ColStudentBirthDate = AddColumn(_YUID("studentBirthDate"), _T("Birth date - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColStudentExternalId = AddColumn(_YUID("studentExternalId"), _T("External id - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColStudentGender = AddColumn(_YUID("studentGender"), _T("Gender - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColStudentGrade = AddColumn(_YUID("studentGrade"), _T("Grade - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColStudentGraduationYear = AddColumn(_YUID("studentGraduationYear"), _T("Graduation year - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColStudentNumber = AddColumn(_YUID("studentNumber"), _T("Number - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Education Teacher
	m_ColTeacherExternalId = AddColumn(_YUID("teacherExternalId"), _T("External id - Teacher"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColTeacherNumber = AddColumn(_YUID("teacherNumber"), _T("Number - Teacher"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	AddColumnForRowPK(m_ColId);

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameEduUsers*>(GetParentFrame()));
}

EducationUser GridEduUsers::getBusinessObject(GridBackendRow* p_Row) const
{
	EducationUser user;

	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		user.SetID(p_Row->GetField(m_ColId).GetValueStr());
		user.SetDisplayName(GetStrValue(*p_Row, m_ColDisplayName));
		user.SetMiddleName(GetStrValue(*p_Row, m_ColMiddleName));
		user.SetPrimaryRole(GetStrValue(*p_Row, m_ColPrimaryRole));
		user.SetExternalSource(GetStrValue(*p_Row, m_ColExternalSource));

		// TODO: Created By

		PhysicalAddress mailingAddress;
		mailingAddress.City = GetStrValue(*p_Row, m_ColMailingAddressCity);
		mailingAddress.CountryOrRegion = GetStrValue(*p_Row, m_ColMailingAddressCountryOrRegion);
		mailingAddress.PostalCode = GetStrValue(*p_Row, m_ColMailingAddressPostalCode);
		mailingAddress.State = GetStrValue(*p_Row, m_ColMailingAddressState);
		mailingAddress.Street = GetStrValue(*p_Row, m_ColMailingAddressStreet);
		user.SetMailingAddress(mailingAddress);

		PhysicalAddress residenceAddress;
		residenceAddress.City = GetStrValue(*p_Row, m_ColMailingAddressCity);
		residenceAddress.CountryOrRegion = GetStrValue(*p_Row, m_ColMailingAddressCountryOrRegion);
		residenceAddress.PostalCode = GetStrValue(*p_Row, m_ColMailingAddressPostalCode);
		residenceAddress.State = GetStrValue(*p_Row, m_ColMailingAddressState);
		residenceAddress.Street = GetStrValue(*p_Row, m_ColMailingAddressStreet);
		user.SetResidenceAddress(residenceAddress);

		EducationStudent student;
		student.m_BirthDate = GetDateValue(*p_Row, m_ColStudentBirthDate);
		student.m_ExternalId = GetStrValue(*p_Row, m_ColStudentExternalId);
		student.m_Gender = GetStrValue(*p_Row, m_ColStudentGender);
		student.m_Grade = GetStrValue(*p_Row, m_ColStudentGrade);
		student.m_GraduationYear = GetStrValue(*p_Row, m_ColStudentGraduationYear);
		student.m_StudentNumber = GetStrValue(*p_Row, m_ColStudentNumber);
		user.SetStudent(student);

		EducationTeacher teacher;
		teacher.m_ExternalId = GetStrValue(*p_Row, m_ColTeacherExternalId);
		teacher.m_TeacherNumber = GetStrValue(*p_Row, m_ColTeacherNumber);
		user.SetTeacher(teacher);
	}

	return user;
}

void GridEduUsers::UpdateMailingAddressField(const wstring& p_FieldName, const wstring& p_NewValue, web::json::value& p_Value)
{
	auto ittCity = p_Value.as_object().find(_YTEXT("mailingAddress"));
	if (ittCity == p_Value.as_object().end())
		p_Value.as_object()[_YTEXT("mailingAddress")] = json::value::object({ { p_FieldName, json::value::string(p_NewValue) } });
	else
		p_Value.as_object()[_YTEXT("mailingAddress")].as_object()[p_FieldName] = json::value::string(p_NewValue);
}

void GridEduUsers::UpdateResidenceAddressField(const wstring& p_FieldName, const wstring& p_NewValue, web::json::value& p_Value)
{
	auto ittCity = p_Value.as_object().find(_YTEXT("residenceAddress"));
	if (ittCity == p_Value.as_object().end())
		p_Value.as_object()[_YTEXT("residenceAddress")] = json::value::object({ { p_FieldName, json::value::string(p_NewValue) } });
	else
		p_Value.as_object()[_YTEXT("residenceAddress")].as_object()[p_FieldName] = json::value::string(p_NewValue);
}

void GridEduUsers::UpdateStudentField(const wstring& p_FieldName, const wstring& p_NewValue, web::json::value& p_Value)
{
	auto ittCity = p_Value.as_object().find(_YTEXT("student"));
	if (ittCity == p_Value.as_object().end())
		p_Value.as_object()[_YTEXT("student")] = json::value::object({ { p_FieldName, json::value::string(p_NewValue) } });
	else
		p_Value.as_object()[_YTEXT("student")].as_object()[p_FieldName] = json::value::string(p_NewValue);
}

void GridEduUsers::UpdateTeacherField(const wstring& p_FieldName, const wstring& p_NewValue, web::json::value& p_Value)
{
	auto ittCity = p_Value.as_object().find(_YTEXT("teacher"));
	if (ittCity == p_Value.as_object().end())
		p_Value.as_object()[_YTEXT("teacher")] = json::value::object({ { p_FieldName, json::value::string(p_NewValue) } });
	else
		p_Value.as_object()[_YTEXT("teacher")].as_object()[p_FieldName] = json::value::string(p_NewValue);
}

void GridEduUsers::OnEdit()
{
	if (!showUserRestrictedAccessDialog(USER_ULTRA_ADMIN_REQUIREMENT))
		return;

	vector<EducationUser> users;

	vector<GridBackendRow*> selectedRows;
	GetSelectedNonGroupRows(selectedRows);

	for (auto row : selectedRows)
		users.push_back(getBusinessObject(row));

	DlgEducationUserEditHTML dlg(users, DlgFormsHTML::Action::EDIT, this);
	if (dlg.DoModal() == IDOK)
	{
		AddPendingChanges(users);
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}
}

void GridEduUsers::OnUpdateEdit(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!GetSelectedRows().empty());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridEduUsers::AddPendingChanges(const vector<EducationUser>& p_Users)
{
	GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
	for (const auto& user : p_Users)
	{
		AddPendingUserChanges(user);
	}
}

void GridEduUsers::AddPendingUserChanges(const EducationUser& p_User)
{
	auto rowPk = vector<GridBackendField>{ GridBackendField(p_User.GetID(), m_ColId) };
	auto row = GetRowIndex().GetExistingRow(rowPk);
	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		AddPendingUserChanges(*row, rowPk, p_User);
	}
}

void GridEduUsers::AddPendingUserChanges(GridBackendRow& p_Row, const row_pk_t& p_RowPk, const EducationUser& p_User)
{
	if (p_User.GetExternalSource())
		AddFieldChange(p_Row, p_RowPk, m_ColExternalSource, *p_User.GetExternalSource());
	
	// Mailing address
	if (p_User.GetMailingAddressCity())
		AddFieldChange(p_Row, p_RowPk, m_ColMailingAddressCity, *p_User.GetMailingAddressCity());
	if (p_User.GetMailingAddressCountryOrRegion())
		AddFieldChange(p_Row, p_RowPk, m_ColMailingAddressCountryOrRegion, *p_User.GetMailingAddressCountryOrRegion());
	if (p_User.GetMailingAddressPostalCode())
		AddFieldChange(p_Row, p_RowPk, m_ColMailingAddressPostalCode, *p_User.GetMailingAddressPostalCode());
	if (p_User.GetMailingAddressState())
		AddFieldChange(p_Row, p_RowPk, m_ColMailingAddressState, *p_User.GetMailingAddressState());
	if (p_User.GetMailingAddressStreet())
		AddFieldChange(p_Row, p_RowPk, m_ColMailingAddressStreet, *p_User.GetMailingAddressStreet());
	
	// Residence address
	if (p_User.GetResidenceAddressCity())
		AddFieldChange(p_Row, p_RowPk, m_ColResidenceAddressCity, *p_User.GetResidenceAddressCity());
	if (p_User.GetResidenceAddressCountryOrRegion())
		AddFieldChange(p_Row, p_RowPk, m_ColResidenceAddressCountryOrRegion, *p_User.GetResidenceAddressCountryOrRegion());
	if (p_User.GetResidenceAddressPostalCode())
		AddFieldChange(p_Row, p_RowPk, m_ColResidenceAddressPostalCode, *p_User.GetResidenceAddressPostalCode());
	if (p_User.GetResidenceAddressState())
		AddFieldChange(p_Row, p_RowPk, m_ColResidenceAddressState, *p_User.GetResidenceAddressState());
	if (p_User.GetResidenceAddressStreet())
		AddFieldChange(p_Row, p_RowPk, m_ColResidenceAddressStreet, *p_User.GetResidenceAddressStreet());

	if (p_User.GetMiddleName())
		AddFieldChange(p_Row, p_RowPk, m_ColMiddleName, *p_User.GetMiddleName());
	if (p_User.GetPrimaryRole())
		AddFieldChange(p_Row, p_RowPk, m_ColPrimaryRole, *p_User.GetPrimaryRole());

	if (p_User.GetStudentBirthDate())
		AddFieldChange(p_Row, p_RowPk, m_ColStudentBirthDate, *p_User.GetStudentBirthDate());
	if (p_User.GetStudentExternalId())
		AddFieldChange(p_Row, p_RowPk, m_ColStudentExternalId, *p_User.GetStudentExternalId());
	if (p_User.GetStudentGender())
		AddFieldChange(p_Row, p_RowPk, m_ColStudentGender, *p_User.GetStudentGender());
	if (p_User.GetStudentGrade())
		AddFieldChange(p_Row, p_RowPk, m_ColStudentGrade, *p_User.GetStudentGrade());
	if (p_User.GetStudentGraduationYear())
		AddFieldChange(p_Row, p_RowPk, m_ColStudentGraduationYear, *p_User.GetStudentGraduationYear());
	if (p_User.GetStudentNumber())
		AddFieldChange(p_Row, p_RowPk, m_ColStudentNumber, *p_User.GetStudentNumber());
	if (p_User.GetTeacherExternalId())
		AddFieldChange(p_Row, p_RowPk, m_ColTeacherExternalId, *p_User.GetTeacherExternalId());
	if (p_User.GetTeacherNumber())
		AddFieldChange(p_Row, p_RowPk, m_ColTeacherNumber, *p_User.GetStudentNumber());

	p_Row.SetEditModified(true);
}

void GridEduUsers::AddFieldChange(GridBackendRow& p_Row, const row_pk_t& p_RowPk, GridBackendColumn* p_Col, PooledString p_Val)
{
	ASSERT(nullptr != p_Col);
	if (nullptr != p_Col)
	{
		auto oldField = p_Row.GetField(p_Col);
		const auto& newField = p_Row.AddField(p_Val, p_Col);
		GetModifications().Add(std::make_unique<FieldUpdateO365>(*this, p_RowPk, p_Col->GetID(), oldField, newField));
	}
}

void GridEduUsers::AddFieldChange(GridBackendRow& p_Row, const row_pk_t& p_RowPk, GridBackendColumn* p_Col, const YTimeDate& p_Val)
{
	ASSERT(nullptr != p_Col);
	if (nullptr != p_Col)
	{
		const auto oldField = p_Row.GetField(p_Col);
		const auto& newField = p_Row.AddField(p_Val, p_Col);
		GetModifications().Add(std::make_unique<FieldUpdateO365>(*this, p_RowPk, p_Col->GetID(), oldField, newField));
	}
}

void GridEduUsers::InitializeCommands()
{
	ModuleO365Grid<EducationUser>::InitializeCommands();

	//const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);
}

void GridEduUsers::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ModuleO365Grid<EducationUser>::OnCustomPopupMenu(pPopup, nStart);

	/*if (nullptr != pPopup)
	{
	}*/
}

