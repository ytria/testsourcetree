#include "YtriaPowerShellDll.h"
#include "ICollectionProperty.h"

YtriaPowerShellDll::YtriaPowerShellDll()
	:m_Loader(g_DllPath)
{
}

IPowerShell* YtriaPowerShellDll::CreatePowerShell(bool p_ExecutionPolicyUnrestricted)
{
	auto& fct = m_Loader.LoadFunction(_YTEXT("CreatePowerShell"), m_CreatePowerShell);
	if (fct)
		return fct(p_ExecutionPolicyUnrestricted);

	return nullptr;
}

void YtriaPowerShellDll::ReleasePowerShell(IPowerShell* p_Instance)
{
	auto& fct = m_Loader.LoadFunction(_YTEXT("ReleasePowerShell"), m_ReleasePowerShell);
	if (fct)
		fct(p_Instance);
}

void YtriaPowerShellDll::ReleasePSObjectCollection(IPSObjectCollection* p_Instance)
{
	auto& fct = m_Loader.LoadFunction(_YTEXT("ReleasePSObjectCollection"), m_ReleasePSObjectCollection);
	if (fct)
		fct(p_Instance);
}

void YtriaPowerShellDll::ReleasePSStream(IPSStream* p_Instance)
{
	auto& fct = m_Loader.LoadFunction(_YTEXT("ReleasePSStream"), m_ReleasePSStream);
	if (fct)
		fct(p_Instance);
}


void YtriaPowerShellDll::ReleaseDateTimeOffset(IDateTimeOffset* p_Instance)
{
	auto& fct = m_Loader.LoadFunction(_YTEXT("ReleaseDateTimeOffset"), m_ReleaseDateTimeOffset);
	if (fct)
		fct(p_Instance);
}

void YtriaPowerShellDll::ReleaseCollectionProperty(ICollectionProperty* p_Instance)
{
	auto& fct = m_Loader.LoadFunction(_YTEXT("ReleaseCollectionProperty"), m_CollectionProperty);
	if (fct)
		fct(p_Instance);
}

YtriaPowerShellDll& YtriaPowerShellDll::Get()
{
	static YtriaPowerShellDll dll;
	return dll;
}

const wstring YtriaPowerShellDll::g_DllPath =
#ifdef _WIN64
#ifdef DEBUG
_YTEXT("YtriaPowerShell64.dll");
#else
Str::ExpandEnvStrings(_YTEXT("%LOCALAPPDATA%\\Ytria\\YtriaPowerShell64.dll"));
#endif
#else // Win32
#ifdef DEBUG
_YTEXT("YtriaPowerShell32.dll");
#else
Str::ExpandEnvStrings(_YTEXT("%LOCALAPPDATA%\\Ytria\\YtriaPowerShell32.dll"));
#endif
#endif
