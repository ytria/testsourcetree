#include "ShowOnPremGroupsCommand.h"
#include "FrameGroups.h"
#include "OnPremiseGroupsRequester.h"
#include "OnPremiseGroupsCollectionDeserializer.h"

ShowOnPremGroupsCommand::ShowOnPremGroupsCommand(FrameGroups& p_Frame)
	:IActionCommand(g_ActionNameShowOnPremGroups),
	m_Frame(p_Frame)
{
}

void ShowOnPremGroupsCommand::ExecuteImpl() const
{
	ASSERT(m_Setup);
	if (!m_Setup)
		return;

	if (!Office365AdminApp::CheckAndWarn<LicenseTag::OnPremise>(&m_Frame))
		return;

	auto setup = *m_Setup;
	auto hwnd = m_Frame.GetSafeHwnd();
	if (!ModuleUtil::WarnIfPowerShellHostIncompatible(CWnd::FromHandle(hwnd)))
		return;

	auto taskData = Util::AddFrameTask(_T("Show on-premises groups"), &m_Frame, setup, m_Frame.GetLicenseContext(), false);

	YSafeCreateTaskMutable([bom = m_Frame.GetBusinessObjectManager(), hwnd, taskData]() mutable {

		auto deserializer = std::make_shared<OnPremiseGroupsCollectionDeserializer>();

		auto initResult = bom->GetSapio365Session()->InitBasicPowerShell(taskData.GetOriginator());
		wstring initErrors = GetPSErrorString(initResult);

		wstring error;
		if (initErrors.empty())
		{
			auto logger = std::make_shared<BasicRequestLogger>(_T("on-premises groups"));
			auto requester = std::make_shared<OnPremiseGroupsRequester>(deserializer, logger);
			requester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();

			error = GetPSErrorString(requester->GetResult());
		}
		else
			error = initErrors;


		YCallbackMessage::DoPost([=]()
		{
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameGroups*>(CWnd::FromHandle(hwnd)) : nullptr;

			if (ModuleBase::ShouldBuildView(hwnd, taskData.IsCanceled(), taskData, false))
			{
				ASSERT(nullptr != frame);
				if (nullptr != frame)
				{
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s on-premises groups...", Str::getStringFromNumber(deserializer->GetData().size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());

					if (!error.empty())
					{
						OnPremiseGroup errorGroup;
						errorGroup.SetError(SapioError(error, 0));

						deserializer->GetData() = { errorGroup };

						frame->GetGrid().HandlePostUpdateError(false, false, errorGroup.GetError()->GetFullErrorMessage());
					}
					else
					{
						CWaitCursor _;
						frame->ShowOnPremiseGroups(deserializer->GetData());
					}
				}
			}

			frame->ProcessLicenseContext(); // Let's consume license tokens if needed
			ModuleBase::TaskFinished(frame, taskData, nullptr);
		});
	});
}
