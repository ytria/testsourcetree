#include "AddDelegateSerializer.h"

#include "AddDelegate.h"
#include "DelegateUserSerializer.h"
#include "SoapDocument.h"
#include "XmlSerializeUtil.h"

Ex::AddDelegateSerializer::AddDelegateSerializer(XERCES_CPP_NAMESPACE::DOMDocument& p_Document, const Ex::AddDelegate& p_AddDelegate)
    :IXmlSerializer(p_Document),
    m_AddDelegate(p_AddDelegate)
{
}

void Ex::AddDelegateSerializer::SerializeImpl(DOMElement& p_Element)
{
    auto root = m_Document.createElement(_YTEXT("m:AddDelegate"));
    ASSERT(nullptr != root);
    if (nullptr != root)
    {
        auto mailbox = m_Document.createElement(_YTEXT("m:Mailbox"));
        ASSERT(nullptr != mailbox);
        if (nullptr != mailbox)
        {
            XmlSerializeUtil::SerializePrimitive(m_Document, *mailbox, _YTEXT("t:EmailAddress"), boost::YOpt<PooledString>(m_AddDelegate.m_Mailbox));
            auto appended = root->appendChild(mailbox);
            ASSERT(nullptr != appended);
        }

        auto delegateUsers = m_Document.createElement(_YTEXT("m:DelegateUsers"));
        ASSERT(nullptr != delegateUsers);
        if (nullptr != delegateUsers)
        {
            XmlSerializeUtil::SerializeObjList<Ex::DelegateUserSerializer>(m_Document, *delegateUsers, _YTEXT("t:DelegateUser"), m_AddDelegate.m_DelegateUsers);
            auto appended = root->appendChild(delegateUsers);
            ASSERT(nullptr != appended);
        }

        XmlSerializeUtil::SerializePrimitive(m_Document, *root, _YTEXT("m:DeliverMeetingRequests"), boost::YOpt<PooledString>(m_AddDelegate.m_DeliverMeetingRequests));

        auto appended = p_Element.appendChild(root);
        ASSERT(nullptr != appended);
    }
}
