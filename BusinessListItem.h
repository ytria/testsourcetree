#pragma once

#include "BusinessObject.h"

#include "ListItem.h"

class BusinessListItem : public BusinessObject
{
public:
    BusinessListItem() = default;
    BusinessListItem(const ListItem& p_JsonListItem);

    const boost::YOpt<PooledString>& GetName() const;
    void SetName(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<IdentitySet>& GetCreatedBy() const;
    void SetCreatedBy(const boost::YOpt<IdentitySet>& p_Val);

    const boost::YOpt<YTimeDate>& GetCreatedDateTime() const;
    void SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_Val);

    const boost::YOpt<PooledString>& GetDescription() const;
    void SetDescription(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<IdentitySet>& GetLastModifiedBy() const;
    void SetLastModifiedBy(const boost::YOpt<IdentitySet>& p_Val);

    const boost::YOpt<YTimeDate>& GetLastModifiedDateTime() const;
    void SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val);

    const boost::YOpt<PooledString>& GetWebUrl() const;
    void SetWebUrl(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<ContentTypeInfo>& GetContentType() const;
    void SetContentType(const boost::YOpt<ContentTypeInfo>& p_Val);
    
    const boost::YOpt<FieldValueSet>& GetFields() const;
    void SetFields(const boost::YOpt<FieldValueSet>& p_Val);

private:
    boost::YOpt<ContentTypeInfo> m_ContentType;
    boost::YOpt<FieldValueSet> m_Fields;

    boost::YOpt<PooledString> m_Name;
    boost::YOpt<IdentitySet>  m_CreatedBy;
    boost::YOpt<YTimeDate>    m_CreatedDateTime;
    boost::YOpt<PooledString> m_Description;
    boost::YOpt<IdentitySet>  m_LastModifiedBy;
    boost::YOpt<YTimeDate>    m_LastModifiedDateTime;
    boost::YOpt<PooledString> m_WebUrl;

    RTTR_ENABLE(BusinessObject)
    friend class ListItemDeserializer;
};

