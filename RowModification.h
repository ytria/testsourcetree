#pragma once

#include "IMainItemModification.h"

class O365Grid;

class RowModification : public IMainItemModification
{
public:
	RowModification(O365Grid& p_Grid, const row_pk_t& p_RowKey);
	virtual ~RowModification() = default;

	const row_pk_t& GetRowKey() const;
	virtual bool IsCorrespondingRow(const row_pk_t& p_RowPk) const;

	RowModification& operator=(const RowModification& p_Other);

	void DontRevertOnDestruction();

protected:
    O365Grid&		getGrid();
	const O365Grid& getGrid() const;
	virtual void	setAppliedLocally(GridBackendRow* p_Row);
	bool			shouldRevert() const;

private:
    std::reference_wrapper<O365Grid> m_Grid;
	row_pk_t m_RowKey;
	bool m_Revert;
};
