#pragma once

#include "ResizableDialog.h"

#include "BusinessAttachment.h"
#include "ColumnSelectionGrid.h"
#include "GridCrust.h"

#include "Resource.h"

#include "FileExistsAction.h"

class DlgDownloadMessages : public ResizableDialog, public GridCrust
{
public:
	enum class CONTEXT
	{
		MESSAGE,
		EVENT,
		POST
	};

    enum { IDD = IDD_DLG_DOWNLOAD_MESSAGES };
    DlgDownloadMessages(CacheGrid* p_Parent, CONTEXT p_Context);

	//FileExistsAction GetFileExistsAction() const;
	const wstring& GetPathFilename() const;
    const wstring& GetPathFolder() const;
	const wstring& GetPath() const;

    // const vector<wstring>& GetSelectedColumnsTitleDisplayed() const;
	const vector<GridBackendColumn*>& GetSelectedColumnsTitleDisplayed() const;

	bool GetExportGrid() const;
	bool GetUseMFHierarchy() const;
    bool GetOpenFolder() const;

	static const wstring g_AutomationName;
	static const wstring m_ActionNameGridDownloadAttachmentsDefault;

protected:
    virtual void DoDataExchange(CDataExchange* pDX) override;
    virtual BOOL OnInitDialogSpecificResizable() override;
    virtual void OnOK() override;

    virtual CacheGrid*	GetMotherGrid() const override;
    virtual void		UpdateFromGrid(CacheGrid* i_FromGrid) override; // TODO
    virtual void		setupClearSpecific() override; // TODO
    virtual const bool	setupLoadSpecific(AutomationAction* i_settingsAction, list <wstring>& o_Errors) override; // TODO

	void			setAutomationFieldMap() override;
	void			setAutomationRadioButtonGroups() override;
	void			setAutomationListChoicesGroups() override;
	virtual void	automationSetParamPostProcess() override;

    afx_msg void OnSelectFolder();
    afx_msg void OnAddColumn();
	afx_msg void OnRemoveColumn();
	/*afx_msg void OnSelectAppend();
	afx_msg void OnSelectSkip();
	afx_msg void OnSelectOverwrite();*/
	afx_msg void OnGridExport();
	afx_msg void OnMailFolderHierarchy();
	afx_msg void OnToggleInfo();
	afx_msg void OnChangeChosenFolder();
	afx_msg void OnShowHiddenColumns();
	afx_msg void OnPresetCol();
	afx_msg void OnSetAsDefault();
    afx_msg void OnSelectOpenFolder();
    DECLARE_MESSAGE_MAP()

private:

	void moveControl(CWnd* p_pCtrl);
	void showExample();
	void showCustomizePanel();
	void loadSavedDefaultColumns();

	CExtEdit   m_TxtFolderPath;
	CXTPButton m_BtnChooseFolder;

	/*CExtGroupBox	m_GroupIfFileExists;
	CExtRadioButton m_RadioCreateNew;
	CExtRadioButton m_RadioOverwrite;
	CExtRadioButton m_RadioSkip;*/
	CExtCheckBox	m_CheckMailFolderHierarchy;
	CExtCheckBox	m_CheckExportGrid;
	CXTPButton		m_BtnToggleInfo;
	int				m_IsCustom;

	CExtLabel			m_StaticIntroTitle;
	CExtLabel			m_StaticIntro;
	CExtGroupBox		m_GroupIntroGridLeft;
	CExtGroupBox		m_GroupIntroGridRight;
	ColumnSelectionGrid m_GridColumnsSelected;
	CacheGrid*			m_MotherGrid;
	CExtCheckBox		m_CheckShowHiddenCol;
	CXTPButton			m_BtnPresetCol;
	CXTPButton			m_BtnSetAsDefault;

	CExtLabel		m_StaticExampleTitle;
	CExtLabel		m_StaticExample;
    CExtCheckBox	m_CheckOpenFolder;

    CStatic    m_BtnAddToSelection;
    CStatic    m_BtnRemoveFromSelection;
    CXTPButton m_BtnOk;
    CXTPButton m_BtnCancel;

	//FileExistsAction	m_FileExistsAction;
    wstring				m_Path;
	wstring				m_PathFolder;
	wstring				m_PathFilename;
	vector<GridBackendColumn*> m_SelectedColumnsTitleDisplayed;

	bool m_ExportGrid;
	bool m_UseMFHierarchy;
    bool m_OpenFolder;

	CONTEXT m_Context;
};
