#include "AddInDeserializer.h"
#include "ListDeserializer.h"
#include "KeyValue.h"
#include "KeyValueDeserializer.h"
#include "JsonSerializeUtil.h"

void AddInDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("id"), m_Data.m_Id, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.m_Id, p_Object);

	{
		ListDeserializer<KeyValue, KeyValueDeserializer> propertiesDeserializer;
		if (JsonSerializeUtil::DeserializeAny(propertiesDeserializer, _YTEXT("properties"), p_Object))
			m_Data.m_Properties = std::move(propertiesDeserializer.GetData());
	}
}
