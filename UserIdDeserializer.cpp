#include "UserIdDeserializer.h"

using namespace Ex;

void UserIdDeserializer::DeserializeObject(const XERCES_CPP_NAMESPACE::DOMElement& p_Element)
{
    m_UserId.m_SID = DeserializeString(p_Element, _YTEXT("SID"));
    m_UserId.m_PrimarySmtpAddress = DeserializeString(p_Element, _YTEXT("PrimarySmtpAddress"));
    m_UserId.m_DisplayName = DeserializeString(p_Element, _YTEXT("DisplayName"));
    m_UserId.m_DistinguishedUser = DeserializeString(p_Element, _YTEXT("DistinguishedUser"));
    m_UserId.m_ExternalUserIdentity = DeserializeString(p_Element, _YTEXT("ExternalUserIdentity"));
}

const Ex::UserId& UserIdDeserializer::GetObject() const
{
    return m_UserId;
}
