#pragma once

#include "IButtonUpdater.h"

template<class FrameClass>
class OnPremRevertSelectedButtonUpdater : public IButtonUpdater
{
public:
	OnPremRevertSelectedButtonUpdater(FrameClass& p_Frame);
	void Update(CCmdUI* p_CmdUi) override;

private:
	FrameClass& m_Frame;
};

template<class FrameClass>
OnPremRevertSelectedButtonUpdater<FrameClass>::OnPremRevertSelectedButtonUpdater(FrameClass& p_Frame)
	: m_Frame(p_Frame)
{

}

template<class FrameClass>
void OnPremRevertSelectedButtonUpdater<FrameClass>::Update(CCmdUI* p_CmdUi)
{
	p_CmdUi->Enable(m_Frame.HasNoTaskRunning()
		&& (m_Frame.hasRevertableModificationsPending(true) || m_Frame.hasOnPremiseChanges(true)));
}

