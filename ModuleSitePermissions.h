#pragma once

#include "ModuleBase.h"
#include "SpUser.h"
#include "RoleDefinition.h"
#include "SpGroup.h"
#include "SubItemKey.h"

class FrameSitePermissions;

class ModuleSitePermissions : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

    struct PermissionInfo
    {
        PermissionInfo(PooledString p_SiteUrl, PooledString p_SiteId, PooledString p_GroupId, PooledString p_UserId, PooledString p_RoleDefId)
            :SiteUrl(p_SiteUrl),
            SiteId(p_SiteId),
            GroupId(p_GroupId),
            UserId(p_UserId),
            RoleDefId(p_RoleDefId)
        {}
        PooledString SiteId;
        PooledString SiteUrl;
        PooledString GroupId;
        PooledString UserId;
        PooledString RoleDefId;

        bool operator<(const PermissionInfo& p_Other) const
        {
            return std::tie(SiteId, SiteUrl, GroupId, UserId, RoleDefId) < std::tie(p_Other.SiteId, p_Other.SiteUrl, p_Other.GroupId, p_Other.UserId, p_Other.RoleDefId);
        }
    };

    struct SitePermissionsChanges
    {
        vector<SubItemKey> m_UserPermissionsToDelete;
        vector<SubItemKey> m_GroupPermissionsToDelete;
    };

    struct SitePermissionsInfo
    {
        set<Sp::RoleDefinition> m_RoleDefinitions;
        map<PooledString, Sp::User> m_Users; // Key: User Id
        vector<Sp::Group> m_Groups;
        O365SubIdsContainer m_GroupMembers; // Key: Group Id, Value: User ids
        map<PooledString, set<int32_t>> m_UserRoles; // Key: User Id, Value: Role Definition Ids
        map<PooledString, set<int32_t>> m_GroupRoles; // Key: Group Id, Value: Role Definition Ids
        boost::YOpt<SapioError> m_UsersListError;
        boost::YOpt<SapioError> m_RoleDefinitionsError;
        boost::YOpt<SapioError> m_GroupsListError;
    };

private:
    virtual void executeImpl(const Command& p_Command) override;

    void applyChanges(const Command& p_Command);
    void doRefresh(FrameSitePermissions* p_pFrame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command);

    void show(const Command& p_Command);
};
