#pragma once

#include "BusinessFileAttachment.h"
#include "BusinessItemAttachment.h"

#include <string>
#include <tuple>
#include <vector>

#include "FileExistsAction.h"

struct AttachmentsInfo
{
	// Inherits from tuple so that sorting is obvious
	struct IDs : public std::tuple<PooledString, PooledString, PooledString, PooledString, PooledString, PooledString>
	{
		IDs()
		{
		}

		IDs(const PooledString& p_UserOrGroupID,
			const PooledString& p_EventOrMessage,
			const PooledString& p_AttachmentID,
			const PooledString& p_Context)
		{
			UserOrGroupID()						= p_UserOrGroupID;
			EventOrMessageOrConversationID()	= p_EventOrMessage;
			AttachmentID()						= p_AttachmentID;
			Context()						    = p_Context;
		}

		IDs(const PooledString& p_GroupID,
			const PooledString& p_ConversationID,
			const PooledString& p_ThreadID,
			const PooledString& p_PostID,
			const PooledString& p_AttachmentID,
			const PooledString& p_Context)
		{
			UserOrGroupID()						= p_GroupID;
			EventOrMessageOrConversationID()	= p_ConversationID;
			ThreadID()							= p_ThreadID;
			PostID()							= p_PostID;
			AttachmentID()						= p_AttachmentID;
			Context()							= p_Context;
		}

		virtual ~IDs() = default;

		bool IsValid() const
		{
			return !UserOrGroupID().IsEmpty() && !EventOrMessageOrConversationID().IsEmpty();
		}

		PooledString& UserOrGroupID()
		{
			return std::get<0>(*this);
		}
		PooledString& EventOrMessageOrConversationID()
		{
			return std::get<1>(*this);
		}
		PooledString& ThreadID()
		{
			return std::get<2>(*this);
		}
		PooledString& PostID()
		{
			return std::get<3>(*this);
		}
		PooledString& AttachmentID()
		{
			return std::get<4>(*this);
		}
		PooledString& Context()
		{
			return std::get<5>(*this);
		}

		const PooledString& UserOrGroupID() const
		{
			return std::get<0>(*this);
		}
		const PooledString& EventOrMessageOrConversationID() const
		{
			return std::get<1>(*this);
		}
		const PooledString& ThreadID() const
		{
			return std::get<2>(*this);
		}
		const PooledString& PostID() const
		{
			return std::get<3>(*this);
		}
		const PooledString& AttachmentID() const
		{
			return std::get<4>(*this);
		}
		const PooledString& Context() const
		{
			return std::get<5>(*this);
		}
	};

    struct Infos
    {
		IDs m_IDs;
        PooledString m_ColumnsPath;
		PooledString m_FolderNameSuffix;
        BusinessFileAttachment m_FileAttachment;
		BusinessItemAttachment m_ItemAttachment;
		bool m_IsFileAttachment = true; // false == ItemAttachment
    };

    std::vector<Infos> m_AttachmentInfos;
    wstring m_DownloadFolderName;
	FileExistsAction m_FileExistsAction = FileExistsAction::Append;
    bool m_OpenFolder = false;
	bool m_UseExistingFolder = true;
};
