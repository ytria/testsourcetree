#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerJobDelete : public IBrowserLinkHandler
{
public:
	virtual void Handle(YBrowserLink& p_Link) const override;
};