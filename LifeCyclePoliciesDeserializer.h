#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "LifeCyclePolicies.h"

class LifeCyclePoliciesDeserializer : public JsonObjectDeserializer, public Encapsulate<LifeCyclePolicies>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

