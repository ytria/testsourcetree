#pragma once

#include "IRequester.h"
#include "ValueListDeserializer.h"

class EducationTeacher;
class EducationTeacherDeserializer;

class ClassTeachersListRequester : public IRequester
{
public:
	ClassTeachersListRequester(const wstring& p_ClassId);

	pplx::task<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const vector<EducationTeacher>& GetData() const;

private:
	std::shared_ptr<ValueListDeserializer<EducationTeacher, EducationTeacherDeserializer>> m_Deserializer;
	wstring m_ClassId;
};

