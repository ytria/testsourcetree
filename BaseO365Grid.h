#pragma once

#include "AnnotationUtil.h"
#include "AutomationNames.h"
#include "AutomationWizardO365.h"
#include "BusinessItemAttachment.h"
#include "BusinessObject.h"
#include "CacheGrid.h"
#include "CommandInfo.h"
#include "DlgRestrictedAccess.h"
#include "GenericGridRefresher.h"
#include "GenericModificationsReverter.h"
#include "GenericModificationsApplyer.h"
#include "GridModifications.h"
#include "GridSnapshotUtil.h"
#include "GridView.h"
#include "IGenericEditablePropertiesGrid.h"
#include "IGridRefresher.h"
#include "IModificationReverter.h"
#include "IModificationApplyer.h"
#include "ISetting.h"
#include "MetaDataColumnInfo.h"
#include "ModuleCriteria.h"
#include "Scope.h"

#include <atomic>

class GraphCache;

class O365Grid : public CacheGrid
{
public:
	O365Grid();

	// p_Selected is always true, remove it.
	virtual void GetIDs(O365IdsContainer& p_IDs, bool p_Selected, Origin p_Origin, RoleDelegationUtil::RBAC_Privilege p_Privilege, bool p_IncludeGuestsWhenOriginIsUser = true) const;
	void GetTeamIDs(O365IdsContainer& p_IDs, bool p_Selected) const;

	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
	virtual void InitializeCommands() override;
    virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) override;
	virtual bool MinimizeHTMLArea() override;

	void					ShowPendingPostUpdateError();
	// If one global error, pass the error description.
    virtual void			HandlePostUpdateError(bool m_HasForbiddenError, bool p_OccuredDuringSave, const wstring& p_ErrorDescription);
	virtual void			HandlePostUpdateNoError(bool p_OccuredDuringSave);
    virtual ModuleCriteria	GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows);
	virtual Origin			GetOrigin() const;

	void ShowErrorMessage(const wstring& p_Title, const wstring& p_Message, const wstring& p_Details, bool p_AddCheckbox = false);

	enum DefaultActions
	{
		ACTION_CREATE	= 0x01,
		ACTION_DELETE	= 0x02,
		ACTION_EDIT		= 0x04,
		ACTION_VIEW		= 0x08,
	};

	void UseDefaultActions(UINT defaultActions/* = ACTION_EDIT | ACTION_CREATE | ACTION_DELETE | ACTION_VIEW*/);
	bool HasEdit() const;
	bool HasCreate() const;
	bool HasDelete() const;
	bool HasView() const;

	virtual bool HasEditInVewer() const override;

	virtual UINT GetEditComamndID() const override;

	virtual bool IsMyData() const;

	struct SnapshotCaps
	{
	public:
		SnapshotCaps(bool p_HasBoth)
			: m_Photo(p_HasBoth)
			, m_RestorePoint(p_HasBoth)
		{

		}
		SnapshotCaps(bool p_HasPhoto, bool p_HasRestorePoint)
			: m_Photo(p_HasPhoto)
			, m_RestorePoint(p_HasRestorePoint)
		{

		}
		bool HasAny() { return m_Photo || m_RestorePoint; }
		bool HasPhoto() { return m_Photo; }
		bool HasRestore() { return m_RestorePoint; }

	private:
		bool m_Photo;
		bool m_RestorePoint;
	};
	virtual SnapshotCaps GetSnapshotCapabilities() const;

	void RevertAllModifications(bool p_UpdateGridIfNeeded);

	enum RevertFlags
	{
		UPDATE_GRID_IF_NEEDED			= 0x1,
		ONLY_ASSOCIATED_TOP_LEVEL_ROWS	= 0x2,
		TOP_LEVEL_REVERT_CHILDREN		= 0x4,
		ANY_LEVEL_REVERT_CHILDREN		= 0x8,
	};
	void RevertSelectedRows(uint32_t p_RevertFlags);

	void EnableGridModifications(); // Call only once at grid construction.
	bool IsGridModificationsEnabled() const;
    GridModificationsO365&          GetModifications();
    const GridModificationsO365&    GetModifications() const;
	virtual row_pk_t	UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody);

	// Only used for some RowFieldUpdates modifications. Could be generalized to IMainItemModification if necessary.
	virtual void ModificationStateChanged(GridBackendRow* p_Row, RowFieldUpdates<Scope::O365>& p_Modification);
	virtual void ModificationStateChanged(GridBackendRow* p_Row, RowFieldUpdates<Scope::ONPREM>& p_Modification);

    bool GetShowSentValue() const;
    void SetShowSentValue(bool p_Value);

	virtual void OnCancel(CancelContext& p_Context) override;

	void AutomationGreenlight(const wstring& p_ActionError);
//	void AutomationGreenlight();

	virtual bool IsColumnLoadMore(const GridBackendColumn* p_Column) const override;
	virtual bool IsColumnDefault(const GridBackendColumn* p_Column) const override;
	virtual bool IsColumnTechnical(const GridBackendColumn* p_Column) const override;
	virtual bool IsColumnObjectID(const GridBackendColumn* p_Column) const override;

	static const GridBackendUtil::COLUMNPRESETFLAGS g_ColumnsPresetDefault;
	static const GridBackendUtil::COLUMNPRESETFLAGS g_ColumnsPresetMore;
	static const GridBackendUtil::COLUMNPRESETFLAGS g_ColumnsPresetMetaMore;
	static const GridBackendUtil::COLUMNPRESETFLAGS g_ColumnsPresetOnPremDefault;
	static const GridBackendUtil::COLUMNPRESETFLAGS g_ColumnsPresetOnPrem;

	virtual const vector<wstring> GetPresetNames(const UINT p_PresetFlags) const override;
	virtual void AddGenericColumns() override final;

	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

	virtual void LoadMorePostProcessRow(GridBackendRow* p_Row);// flags row as "more" loaded, checks value in "is more loaded" column
	void InitNewRow(GridBackendRow* p_Row) override;// flags row as "more" not loaded

	afx_msg void OnShowColumnsDefault();
	afx_msg void OnUpdateShowDefaultColumns(CCmdUI* pCmdUI);
	afx_msg void OnShowColumnsAll();
	afx_msg void OnUpdateShowColumnsAll(CCmdUI* pCmdUI);
	afx_msg void OnShowLastQueryColumn();
	afx_msg void OnUpdateShowLastQueryColumn(CCmdUI* pCmdUI);

	afx_msg void OnEditAdditionalDataConfig();
	afx_msg void OnUpdateEditAdditionalDataConfig(CCmdUI* pCmdUI);

	afx_msg void OnCreateSnapshotPhoto();
	afx_msg void OnCreateSnapshotRestorePoint();
	afx_msg void OnUpdateCreateSnapshotPhoto(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCreateSnapshotRestorePoint(CCmdUI* pCmdUI);

	bool isDebugMode() const;

	IModificationReverter* GetModReverter();
	IModificationApplyer* GetModApplyer();
	IGridRefresher* GetGridRefresher();

    std::shared_ptr<Sapio365Session> GetSession() const;

	static wstring g_FamilyOwner;
	static wstring g_TitleOwnerDisplayName;
	static wstring g_TitleOwnerUserName;
	static wstring g_TitleOwnerUserType;
	static wstring g_TitleOwnerID;
	static wstring g_TitleGraphID;
	static wstring g_TitleOwnerGroupType;
	static wstring g_TitleOwnerIsTeam;

	static wstring g_FamilyName;
	static wstring g_FamilyInfo;
	static wstring g_FamilyMail;
	static wstring g_FamilySite;
	static wstring g_FamilyOnPrem;
	static wstring g_FamilyCommonInfo;

	static wstring GetNextTemporaryCreatedObjectID();
	static bool IsTemporaryCreatedObjectID(const wstring& P_ID);

    GridBackendColumn* GetColId() const;
	GridBackendColumn* GetColRoleDelegationFlag() const;

	virtual bool GetHTMLAreaJSON(wstring& p_JSON) override;
	virtual void ProcessHTMLAreaData(const YAdvancedHtmlView::IPostedDataTarget::PostedData& p_Data) override;

	virtual UINT GetHTMLAreaMinizeImageID() override;
	virtual COLORREF GetHTMLAreaMinizeBackColour() override;

	virtual UINT GetViewerMinizeImageID() override;

	AutomationWizardO365*	GetAutomationWizard();
	AutomationAction*		GetAutomationActionFromFrame() const;

	virtual bool IsQueryReferenceRow(GridBackendRow* p_Row) const;

	bool IsLessThanTopHierarchyRowsIndirectlySelected(size_t p_LessThanNumTopRows) const;

	// Will create a list of child rows that are present in all the given parent rows.
	// Id Column (GetColId) is used to identify the child rows.
	// An optional field value can be tested too.
	std::set<PooledString> GetIdsOfChildRowsThatAllMatch(const set<GridBackendRow*>& p_ParentRows, GridBackendColumn* p_CustomIDColumn = nullptr);
	std::set<PooledString> GetIdsOfChildRowsThatAllMatch(const set<GridBackendRow*>& p_ParentRows, const UINT p_TestColumnID, const PooledString& p_TestValue, GridBackendColumn* p_CustomIDColumn = nullptr);

    GraphCache& GetGraphCache();
    const GraphCache& GetGraphCache() const;

	void			LogModifications(const bool p_MainModifications);// main vs. sub items
	
	virtual wstring GetName(const GridBackendRow* p_Row) const = 0;

	bool HasLastQueryColumn() const;

	bool IsFrameConnected() const;

	bool IsAuthorizedByRoleDelegation(const GridBackendRow* p_Row, const RoleDelegationUtil::RBAC_Privilege p_Privilege, const bool p_CheckAncestorPrivilegeIfPresent = false) const;
	bool IsUsingRoleDelegation() const;
	void AddRoleDelegationFlag(GridBackendRow* p_Row, const BusinessObject& p_Object);
	void AddRoleDelegationFlagFromParent(GridBackendRow* p_Row);
	bool InitRoleDelegation();
	void UpdateRBACinfo(const bool p_HideOutOfScope);

	virtual wstring GetUserID() const override;

	virtual GridAnnotation	AnnotationGetFromDB(const int64_t p_ID) override;
	virtual bool canRefreshEditOrDeleteAnnotations() const override;

	virtual bool isTeam(const GridBackendRow* p_Row) const;

	virtual void GetGACustomText(CString& p_CustomText) override;

	virtual bool HasModificationsPending(bool inSelectionOnly = false);
	virtual bool HasModificationsPendingInParentOfSelected(); // This doesn't handle subitems mods.
	virtual bool HasRevertableModificationsPending(bool inSelectionOnly = false);

	void SetMetaDataColumnInfo(const boost::YOpt<vector<MetaDataColumnInfo>>& p_MetaDataColumnInfo);

	std::function<wstring(const wstring&)> AnnotationsGetModuleNameTranslator() const override final;

	class SnapshotDataCustomizer : public GridSnapshot::IDataCustomizer
	{
	public:
		SnapshotDataCustomizer(O365Grid& p_Grid);

	public:
		virtual bool GetValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
		virtual bool SetValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;
		virtual bool GetAdditionalValue(wstring& p_Value, GridBackendRow& p_Row) override;
		virtual bool SetAdditionalValue(const wstring& p_Value, GridBackendRow& p_Row) override;

	private:
		O365Grid& m_Grid;
	};

	// For know only used by On-premises-equipped modules to handle On-premises edit status.
	template<Scope scope>
	struct RowStatusHandler
	{
	public:
		RowStatusHandler(O365Grid& p_Grid)
			: m_Grid(p_Grid)
		{}

		bool IsChangesSaved(GridBackendRow* p_Row) const;
		bool IsError(GridBackendRow* p_Row) const;
		bool IsModified(GridBackendRow* p_Row) const;
		bool IsDeleted(GridBackendRow* p_Row) const;

		void ClearStatus(GridBackendRow* p_Row) const;

		void OnRowModified(GridBackendRow* p_Row);
		void OnRowNothingToSave(GridBackendRow* p_Row, const bool i_RefreshGrid, const wstring& i_StatusText = _YTEXT(""));
		void OnRowSavedNotReceived(GridBackendRow* p_Row, bool i_RefreshGrid, const wstring& i_StatusText);
		void OnRowSavedOverwritten(GridBackendRow* p_Row, bool i_RefreshGrid, const wstring& i_StatusText);
		void OnRowSaved(GridBackendRow* p_Row, const bool i_RefreshGrid, const wstring& i_StatusText = _YTEXT(""));
		void OnRowError(GridBackendRow* p_Row, const bool i_RefreshGrid, const wstring& i_StatusText = _YTEXT(""));

		void SetRowError(GridBackendRow* p_Row, const SapioError& p_Error, const wstring& p_ErrorPrefix)
		{
			m_Grid.m_HasAtLeastOneError = true;
			if (!m_Grid.m_HasAtLeastOne403Error)
				m_Grid.m_HasAtLeastOne403Error = p_Error.GetStatusCode() == web::http::status_codes::Forbidden;

			OnRowError(p_Row, false, p_ErrorPrefix + p_Error.GetFullErrorMessage());
		}

	private:
		O365Grid& m_Grid;
	};

	void OnRowSavedNotReceived(GridBackendRow* p_Row, bool i_RefreshGrid, const wstring& i_StatusText);
	void OnRowSavedOverwritten(GridBackendRow* p_Row, bool i_RefreshGrid, const wstring& i_StatusText);
	void OnRowSaved(GridBackendRow* p_Row, const bool i_RefreshGrid, const wstring& i_StatusText = _YTEXT("")) override;

	void ResetHasSavedWarningFlags();

	bool IsRbacAuthorized(GridBackendRow* p_Row) const;

	GridBackendColumn* GetSecondaryStatusColumn() const;

	virtual void EmptyGrid();

	virtual wstring	GetViewModuleName() const override;
	virtual void	LoadViews(vector<GridView>& p_Views) override;
	virtual void	ManageViews() override;

protected:
	mutable Origin m_Origin = Origin::NotSet;
    bool m_ShowSentValue;

	bool m_WithOwnerAsTopAncestor;
	bool m_MayContainUnscopedUserGroupOrSite;

#ifdef _DEBUG
	int m_HasModificationsPendingAssertCounter;
#endif

	GridBackendColumn* AddSecondaryColumnStatus(const wstring& i_Title, const wstring& i_Family = _YTEXT(""));

	virtual bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column);
	virtual bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column);
	virtual bool GetSnapshotAdditionalValue(wstring& p_Value, GridBackendRow& p_Row);
	virtual bool SetSnapshotAdditionalValue(const wstring& p_Value, GridBackendRow& p_Row);

	struct BasicGridSetupHierarchy
	{
		map<GridBackendColumn*, int32_t>	m_ColumnsParentPKs;
        std::set<int32_t>                   m_LowestChildrenObjectTypes;
		std::set<int32_t>                   m_AllDefaultObjectTypes;

		static std::set<int32_t>            AllUserObjectTypes(bool p_IncludeOrgContact = false);
		static std::set<int32_t>&           AppendAllUserObjectTypes(std::set<int32_t>& p_ObjectTypes, bool p_IncludeOrgContact = false);
	};
	void setBasicGridSetupHierarchy(const BasicGridSetupHierarchy& p_Setup);
	GridBackendColumn* addColumnIsLoadMore(const wstring& p_UniqueID, const wstring& p_ColumnTitle, bool p_DateColumn = false);

	GridBackendColumn* addColumnDataBlockDate(const wstring& p_UniqueID, const wstring& p_ColumnTitle, const wstring& p_ColumnFamily);

	vector<GridBackendColumn*> addSystemTopCategory();// adds a category with hierarchy, object type, status columns
	void addTopCategories(const wstring& p_MainTitle, const wstring& p_LoadMoreTitle, bool p_IsMoreLoadedInLoadMore);// for grids with load more, separate columns in main & load more categories

	GridBackendColumn* m_ColId;
	GridBackendColumn* addColumnGraphID();
	GridBackendColumn* addColumnGraphID(const wstring& p_Family);

	GridBackendColumn* m_ColRoleDelegationFlag;
	void addColumnRoleDelegationFlag();
	void addColumnRoleDelegationFlag(const wstring& p_Family);

	bool hasUsersSelected() const;
	bool hasGroupsSelected() const;
	bool hasSitesSelected() const;
	bool hasNoTaskRunning() const;

	void ensureOneErrorRowIsOnScreen();
	void ensureOneSavedRowIsOnScreen();

	virtual void customizeGridPostProcess() override;

	afx_msg void OnShowUserDriveItems();
	afx_msg void OnShowMailboxPermissions();
	afx_msg void OnShowMessages();
	afx_msg void OnShowContacts();
	afx_msg void OnShowEvents();
	afx_msg void OnShowParentGroups();
	afx_msg void OnShowMembers();
	afx_msg void OnShowOwners();
	afx_msg void OnShowAuthors();
	afx_msg void OnShowGroupDriveItems();
	afx_msg void OnShowCalendars();
	afx_msg void OnShowConversations();
	afx_msg void OnShowSites();
	afx_msg void OnShowLicenses();
	afx_msg void OnShowManagerHierarchy();
	afx_msg void OnShowMessageRules();
	afx_msg void OnShowUserDetails();
	afx_msg void OnShowGroupDetails();
	afx_msg void OnShowChannels();
	afx_msg void OnShowSiteDetails();
	afx_msg void OnUpdateShowChannels(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowBusinessUser(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowBusinessGroup(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowBusinessSite(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()

	virtual BOOL PreTranslateMessage(MSG* pMsg) override;

	virtual bool isConfiguredForHTMLArea() override;

	template <class WizardO365Class>
	void initWizard(const wstring& p_DebugFolderPath);

	enum UserRestrictedAccessKind
	{
		USER_PERMISSION_REQUIREMENT,
		USER_ULTRA_ADMIN_REQUIREMENT,
		USER_ADMIN_REQUIREMENT,
	};
	bool showUserRestrictedAccessDialog(UserRestrictedAccessKind p_UserRestrictedAccessKind);

	enum GroupRestrictedAccessKind
	{
		GROUP_ADMIN_REQUIREMENT,
		GROUP_LICENSE_REQUIREMENT,
	};
	bool showGroupRestrictedAccessDialog(GroupRestrictedAccessKind p_GroupRestrictedAccessKind);

	bool IsUserAuthorizedByRoleDelegation(const GridBackendRow* p_Row, const RoleDelegationUtil::RBAC_Privilege p_Privilege) const;
	bool IsUserAuthorizedByRoleDelegation(const wstring& p_UserID, bool p_IsDeletedUser, const RoleDelegationUtil::RBAC_Privilege p_Privilege) const;
	bool IsGroupAuthorizedByRoleDelegation(const GridBackendRow* p_Row, const RoleDelegationUtil::RBAC_Privilege p_Privilege) const;
	bool IsGroupAuthorizedByRoleDelegation(const wstring& p_GroupID, bool p_IsDeletedGroup, const RoleDelegationUtil::RBAC_Privilege p_Privilege) const;
	bool IsSiteAuthorizedByRoleDelegation(const GridBackendRow* p_Row, const RoleDelegationUtil::RBAC_Privilege p_Privilege) const;

	bool IsAuthorizedByRoleDelegation(const RoleDelegationUtil::RBAC_Privilege p_Privilege) const;

	void updateRBACStatusBarText();
	void resetRBACHiddenCounter();

	void addMember(
		map<wstring, set<wstring>>& p_ItemsToMembers,
		GridBackendColumn* p_pColLeft,
		GridBackendColumn* p_pColRight,
		GridBackendRow* p_Row);

	GridFrameBase* GetParentGridFrameBase() const;
	bool openModuleConfirmation();

	boost::YOpt<vector<MetaDataColumnInfo>> getMetaDataColumnInfoFor(Origin p_Origin);

	boost::YOpt<MetaDataColumnInfosSetting>& getMetaDataColumnInfosSetting(Origin p_Origin);
	// only for GridUsers and GridGroups... FIXME
	boost::YOpt<vector<MetaDataColumnInfo>> editMetaDataColumnInfoSettings(MetaDataColumnInfosSetting& p_MetaDataColumnInfosSetting, bool p_ForModuleOpening);

	const boost::YOpt<vector<MetaDataColumnInfo>>& getMetaDataColumnInfo() const;	

	static RoleDelegationUtil::RBAC_Privilege getCommandRBACPrivilege(UINT p_CmdId, bool p_ForDeletedUser = false);

	wstring makeRowName(const GridBackendRow* p_Row, const GridBackendColumn* p_Column) const;
	wstring makeRowName(const wstring& p_Name) const;
	wstring makeRowName(const GridBackendRow* p_Row, const vector<pair<wstring, GridBackendColumn*>> p_HierarchyTypeAndNames, GridBackendColumn* p_ColumnMainName) const;
	wstring makeRowName(const GridBackendRow* p_Row, const vector<pair<wstring, GridBackendColumn*>> p_HierarchyTypeAndNames, const wstring& p_Name) const;
	wstring makeRowName(const vector<std::pair<wstring, wstring>>& p_NameHierarchy, const wstring& p_Name) const;// name hierarchy from top to bottom (e.g. group - member)

	LicenseContext getLicenseContextForSubModule() const;

	void SetModReverter(std::shared_ptr<IModificationReverter> p_NewReverter);
	void SetModApplyer(std::shared_ptr<IModificationApplyer> p_NewApplyer);
	void SetGridRefresher(std::shared_ptr<IGridRefresher> p_NewRefresher);

private:
	bool showRestrictedAccessDialog(const set<wstring>& p_RestrictedSessionTypes, DlgRestrictedAccess::Image p_Image, const wstring& p_Title, const wstring& p_Text, const ISetting<bool>& p_Setting);

	void createSnapshot(GridSnapshot::Options::Mode p_Mode);

	void onUpdateCommand(CCmdUI* pCmdUI);
	void init();

	static bool isCompatibleWithDeletedUser(UINT cmdID);
	static bool isCompatibleWithGuestUser(UINT cmdID);
	
	virtual	AutomatedApp::AUTOMATIONSTATUS processAutomationSetFilterTarget(AutomationAction* i_Action) override;

	virtual bool	annotationStoreInDB(GridAnnotation& p_Annotation) override;
	virtual bool	annotationsGetFromDB(const wstring& p_RowPK, const set<wstring>& p_ColumnIDs, vector<GridAnnotation>& p_Annotations) override;
	virtual bool	annotationsLoadAllModule(vector<GridAnnotation>& p_Annotations) override;
	virtual wstring	annotationGetReferenceName(const GridBackendRow* p_Row) const override;
	virtual void	annotationsRefreshFromCloud() override;
	virtual bool	annotationDeleteFromDB(GridAnnotation& p_Annotation) override;
	virtual void	annotationsWriteColumnSettings(const wstring& p_Module, const wstring& p_UID, const wstring& p_Settings) override;
	virtual wstring	annotationsGetColumnSettings(const wstring& p_Module, const wstring& p_UID) override;
	
	virtual void	saveView(GridView& p_View);

	virtual void	removeRowPreprocess(GridBackendRow* p_Row, RowRemovalReason p_RowRemovalReason) override;

	struct MetadataColumnFilter
	{
		std::set<PooledString> m_DefaultColumnIds;
		std::set<PooledString> m_IgnoredColumnIds;
		std::function<bool(const MetadataColumnFilter&, GridBackendColumn*)> m_Filter;

		bool Filter(GridBackendColumn* p_Col) const
		{
			return !m_Filter || m_Filter(*this, p_Col);
		}

		auto ToFunc() const
		{
			return std::bind(&MetadataColumnFilter::Filter, this, std::placeholders::_1);
		}
	};
	const MetadataColumnFilter& getMetadataColumnFilter(Origin p_Origin) const;

private:
	std::shared_ptr<IModificationReverter> m_ModReverter;
	std::shared_ptr<IModificationApplyer> m_ModApplyer;
	std::shared_ptr<IGridRefresher> m_GridRefresher;

	UINT m_defaultActions = 0;

    bool m_ShowErrors;
    bool m_HasAtLeastOneError;
    bool m_HasAtLeastOne403Error;

	bool m_PendingHasAtLeastOneError;
	bool m_PendingHasAtLeastOne403Error;
	wstring m_PendingErrorDescription;
	bool m_PendingErrorOccuredDuringSave;

	bool m_IsUsingRoleDelegation;
	int32_t m_IconRABCauthorized;
	int32_t m_IconRABCunauthorized;
	size_t	m_RBACHiddenCount;

	int32_t m_IconSavedNotReceived;
	bool m_HasSavedNotReceived;

	int32_t m_IconSavedOverwitten;
	bool m_HasSavedOverwitten;

	int32_t m_IconSaved;
	int32_t m_IconError;

	std::unique_ptr<GridModificationsO365> m_Modifications;

    size_t m_IdDeletionFormatter;
    size_t m_IdUpdateFormatter;

	static bool g_InitStatics;

	HACCEL m_hAccelSpecific = nullptr;

	std::unique_ptr<AutomationWizardO365> m_AutomationWizard;

	boost::YOpt<MetaDataColumnInfosSetting> m_MetaDataColumnInfosSettingUser;
	boost::YOpt<MetaDataColumnInfosSetting> m_MetaDataColumnInfosSettingGroup;
	boost::YOpt<vector<MetaDataColumnInfo>> m_MetaDataColumnInfo;

	vector<GridBackendColumn*>	m_LoadMoreColumns; // Just for LoadMorePostProcessRow

	GridBackendColumn* m_SecondaryStatusColumn;

	friend class GenericModificationsReverter;
	friend class GenericModificationsApplyer;
	friend class GenericGridRefresher;
};

template<class CacheGridClass, class BusinessObjectClass>
class BaseO365Grid : public IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>
{
protected:
	using BaseClass = IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>;

public:
	using BaseClass::BaseClass;
    virtual ~BaseO365Grid() = default;

    virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;

    afx_msg void OnEdit();
    afx_msg void OnCreate();
    afx_msg void OnDelete();
	afx_msg void OnView();
	afx_msg void OnUpdateBusinessType(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEdit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCreate(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDelete(CCmdUI* pCmdUI);

    DECLARE_MESSAGE_MAP()

	virtual BOOL PreTranslateMessage(MSG* pMsg) override;
	afx_msg int	OnCreate(LPCREATESTRUCT lpCreateStruct);

protected:
	virtual void InitializeCommands() override;

private:
	bool isDebugMode_hook() const;

private:
	HACCEL m_hAccelSpecific = nullptr;
};

template<class BusinessObjectClass>
class ModuleO365Grid : public BaseO365Grid<O365Grid, BusinessObjectClass>
{
public:
	using BaseO365Grid<O365Grid, BusinessObjectClass>::BaseO365Grid;

	void ShowItemAttachment(const BusinessItemAttachment& p_ItemAttachment);
};

#define NEWMODULE_COMMAND(_id, _frameId, _newFrameId, _onCommandMethod, _onUpdateUiMethod) \
	ON_COMMAND(_id, _onCommandMethod) \
	ON_COMMAND(_frameId, _onCommandMethod) \
	ON_COMMAND(_newFrameId, _onCommandMethod) \
	ON_UPDATE_COMMAND_UI(_id, _onUpdateUiMethod) \
	ON_UPDATE_COMMAND_UI(_frameId, _onUpdateUiMethod) \
	ON_UPDATE_COMMAND_UI(_newFrameId, _onUpdateUiMethod) \

#include "BaseO365Grid.hpp"
