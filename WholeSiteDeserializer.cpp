#include "WholeSiteDeserializer.h"

#include "JsonSerializeUtil.h"
#include "SiteDeserializer.h"
#include "SiteNonPropDeserializer.h"

WholeSiteDeserializer::WholeSiteDeserializer(const std::shared_ptr<Sapio365Session>& p_Session)
    : m_Session(p_Session)
{
}

void WholeSiteDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
#ifdef _DEBUG
        SiteDeserializer d(m_Session, true);
#else
		SiteDeserializer d(m_Session);
#endif
		JsonSerializeUtil::DeserializeObject(d, p_Object);
        m_Data = std::move(d.GetData());

    }

    {
        SiteNonPropDeserializer d(m_Session);
		JsonSerializeUtil::DeserializeObject(d, p_Object);
        m_Data.SetLastRequestTime(d.GetData().GetLastRequestTime());
        m_Data.SetSubSites(d.GetData().GetSubSites());
    }
}
