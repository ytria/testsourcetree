#include "KeyValueDeserializer.h"

void KeyValueDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("key"), m_Data.m_Key, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("value"), m_Data.m_Value, p_Object);
}
