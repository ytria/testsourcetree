#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ItemBody.h"

class ItemBodyDeserializer : public JsonObjectDeserializer, public Encapsulate<ItemBody>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

