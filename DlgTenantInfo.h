#pragma once

#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "YAdvancedHtmlView.h"

class ModuleBase;
class DlgTenantInfo : public ResizableDialog, public YAdvancedHtmlView::IPostedDataTarget, public YAdvancedHtmlView::IPostedURLTarget
{
public:
    DlgTenantInfo(const wstring& p_Tenant, const wstring& p_AppId, const wstring& p_RedirectURL, CWnd* p_Parent);
    virtual ~DlgTenantInfo() = default;

    enum { IDD = IDD_DLG_TENANT_NAME };

	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data);
	virtual bool OnNewPostedURL(const wstring& url);

    const wstring& GetTenantName() const;
    const wstring& GetAppId() const;
	const wstring& GetRedirectURL() const;

protected:
	virtual BOOL OnInitDialogSpecificResizable() override;

	ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);

private:
	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;
    wstring m_Tenant;
    wstring m_AppId;
	wstring m_RedirectURL;

	static const wstring g_OKButtonName;
	static const wstring g_CancelButtonName;
	static const wstring g_AppIdFieldName;
	static const wstring g_TenantFieldName;
	static const wstring g_RedirectURLFieldName;
};

