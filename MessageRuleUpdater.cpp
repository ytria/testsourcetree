#include "MessageRuleUpdater.h"

#include "MSGraphCommonData.h"
#include "MsGraphFieldNames.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "MsGraphHttpRequestLogger.h"

MessageRuleUpdater::MessageRuleUpdater(const PooledString& p_UserId, const BusinessMessageRule& p_Rule, bool p_DeleteRule)
	: m_UserId(p_UserId)
	, m_Rule(p_Rule)
	, m_DeleteRule(p_DeleteRule)
{

}

MessageRuleUpdater::MessageRuleUpdater(const PooledString& p_UserId, const BusinessMessageRule& p_Rule)
	: MessageRuleUpdater(p_UserId, p_Rule, false)
{

}

TaskWrapper<void> MessageRuleUpdater::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri;
	uri.append_path(Rest::USERS_PATH);
	uri.append_path(GetUserId());
	uri.append_path(_YTEXT("mailFolders"));
	uri.append_path(_YTEXT("inbox"));
	uri.append_path(_YTEXT("messageRules"));
	uri.append_path(GetRule().m_Id);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	TaskWrapper<RestResultInfo> task;
	if (m_DeleteRule)
	{
		task = p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Delete(uri.to_uri(), httpLogger, p_TaskData);
	}
	else
	{
		auto json = web::json::value::object();

		toJson(json, _YTEXT(O365_MESSAGERULE_DISPLAYNAME), _YTEXT(""), GetRule().m_DisplayName);
		toJson(json, _YTEXT(O365_MESSAGERULE_SEQUENCE), _YTEXT(""), GetRule().m_Sequence);
		toJson(json, _YTEXT(O365_MESSAGERULE_ISREADONLY), _YTEXT(""), GetRule().m_IsReadOnly);
		
		if (!GetRule().m_IsReadOnly)
		{
			toJson(json, _YTEXT(O365_MESSAGERULE_ISENABLED), _YTEXT(""), GetRule().m_IsEnabled);

			toJson(json, _YTEXT(O365_MESSAGERULE_ACTIONS), _YTEXT("assignCategories"), GetRule().m_Actions.m_AssignCategories);
			toJson(json, _YTEXT(O365_MESSAGERULE_ACTIONS), _YTEXT("copyToFolder"), GetRule().m_Actions.m_CopyToFolderID);
			toJson(json, _YTEXT(O365_MESSAGERULE_ACTIONS), _YTEXT("delete"), GetRule().m_Actions.m_Delete);
			//toJson(json, _YTEXT(O365_MESSAGERULE_ACTIONS), _YTEXT("forwardAsAttachmentTo")	, GetRule().m_Actions.m_ForwardAsAttachmentTo);
			//toJson(json, _YTEXT(O365_MESSAGERULE_ACTIONS), _YTEXT("forwardTo")				, GetRule().m_Actions.m_ForwardTo);
			toJson(json, _YTEXT(O365_MESSAGERULE_ACTIONS), _YTEXT("markAsRead"), GetRule().m_Actions.m_MarkAsRead);
			toJson(json, _YTEXT(O365_MESSAGERULE_ACTIONS), _YTEXT("markImportance"), GetRule().m_Actions.m_MarkImportance);
			toJson(json, _YTEXT(O365_MESSAGERULE_ACTIONS), _YTEXT("moveToFolder"), GetRule().m_Actions.m_MoveToFolderID);
			toJson(json, _YTEXT(O365_MESSAGERULE_ACTIONS), _YTEXT("permanentDelete"), GetRule().m_Actions.m_PermanentDelete);
			//toJson(json, _YTEXT(O365_MESSAGERULE_ACTIONS), _YTEXT("redirectTo")				, GetRule().m_Actions.m_RedirectTo);
			toJson(json, _YTEXT(O365_MESSAGERULE_ACTIONS), _YTEXT("stopProcessingRules"), GetRule().m_Actions.m_StopProcessingRules);

			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("bodyContains"), GetRule().m_Conditions.m_BodyContains);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("bodyOrSubjectContains"), GetRule().m_Conditions.m_BodyOrSubjectContains);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("categories"), GetRule().m_Conditions.m_Categories);
			//toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("fromAddresses")			, GetRule().m_Conditions.m_FromAddresses);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("hasAttachments"), GetRule().m_Conditions.m_HasAttachments);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("headerContains"), GetRule().m_Conditions.m_HeaderContains);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("importance"), GetRule().m_Conditions.m_Importance);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("isApprovalRequest"), GetRule().m_Conditions.m_IsApprovalRequest);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("isAutomaticForward"), GetRule().m_Conditions.m_IsAutomaticForward);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("isAutomaticReply"), GetRule().m_Conditions.m_IsAutomaticReply);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("isEncrypted"), GetRule().m_Conditions.m_IsEncrypted);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("isMeetingRequest"), GetRule().m_Conditions.m_IsMeetingRequest);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("isMeetingResponse"), GetRule().m_Conditions.m_IsMeetingResponse);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("isNonDeliveryReport"), GetRule().m_Conditions.m_IsNonDeliveryReport);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("isPermissionControlled"), GetRule().m_Conditions.m_IsPermissionControlled);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("isReadReceipt"), GetRule().m_Conditions.m_IsReadReceipt);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("isSigned"), GetRule().m_Conditions.m_IsSigned);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("isVoicemail"), GetRule().m_Conditions.m_IsVoicemail);
			toJson(json, _YTEXT(O365_MESSAGERULE_CONDITIONS), _YTEXT("messageActionFlag"), GetRule().m_Conditions.m_MessageActionFlag);

			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("bodyContains"), GetRule().m_Exceptions.m_BodyContains);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("bodyOrSubjectContains"), GetRule().m_Exceptions.m_BodyOrSubjectContains);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("categories"), GetRule().m_Exceptions.m_Categories);
			//toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("fromAddresses")			, GetRule().m_Exceptions.m_FromAddresses);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("hasAttachments"), GetRule().m_Exceptions.m_HasAttachments);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("headerContains"), GetRule().m_Exceptions.m_HeaderContains);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("importance"), GetRule().m_Exceptions.m_Importance);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("isApprovalRequest"), GetRule().m_Exceptions.m_IsApprovalRequest);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("isAutomaticForward"), GetRule().m_Exceptions.m_IsAutomaticForward);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("isAutomaticReply"), GetRule().m_Exceptions.m_IsAutomaticReply);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("isEncrypted"), GetRule().m_Exceptions.m_IsEncrypted);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("isMeetingRequest"), GetRule().m_Exceptions.m_IsMeetingRequest);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("isMeetingResponse"), GetRule().m_Exceptions.m_IsMeetingResponse);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("isNonDeliveryReport"), GetRule().m_Exceptions.m_IsNonDeliveryReport);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("isPermissionControlled"), GetRule().m_Exceptions.m_IsPermissionControlled);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("isReadReceipt"), GetRule().m_Exceptions.m_IsReadReceipt);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("isSigned"), GetRule().m_Exceptions.m_IsSigned);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("isVoicemail"), GetRule().m_Exceptions.m_IsVoicemail);
			toJson(json, _YTEXT(O365_MESSAGERULE_EXCEPTIONS), _YTEXT("messageActionFlag"), GetRule().m_Exceptions.m_MessageActionFlag);
		}

		const WebPayloadJSON payload(json);

		LoggerService::User(YtriaTranslate::Do(MessageRuleUpdater_Send_1, _YLOC("Updating Message Rule with id \"%1\" for user \"%2\""), Str::MakeMidEllipsis(GetRule().m_Id).c_str(), Str::MakeMidEllipsis(GetUserId()).c_str()), p_TaskData.GetOriginator());

		task = p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Patch(uri.to_string(), httpLogger, p_TaskData, payload);
	}

	return task.ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResultInfo) {
			try
			{
				result->SetResult(p_ResultInfo.get());
			}
			catch (const RestException& e)
			{
				RestResultInfo info(e.GetRequestResult());
				info.m_RestException = std::make_shared<RestException>(e);
				result->SetResult(info);
			}
			catch (const std::exception& e)
			{
				RestResultInfo info;
				info.m_Exception = e;
				result->SetResult(info);
			}
			catch (...)
			{
				result->SetResult(RestResultInfo());
			}
		});
}

const PooledString& MessageRuleUpdater::GetUserId() const
{
	return m_UserId;
}

const BusinessMessageRule& MessageRuleUpdater::GetRule() const
{
	return m_Rule;
}

const SingleRequestResult& MessageRuleUpdater::GetResult() const
{
	return *m_Result;
}

void MessageRuleUpdater::toJson(web::json::value& p_Json, const wstring& p_PropertyName, const wstring& p_SubPropertyName, const boost::YOpt<PooledString>& p_Val)
{
	if (p_Val)
	{
		auto& prop = p_Json[p_PropertyName];
		if (!p_SubPropertyName.empty())
		{
			prop[p_SubPropertyName] = web::json::value::string(p_Val->c_str());
		}
		else
		{
			prop = web::json::value::string(p_Val->c_str());
		}
	}
}

void MessageRuleUpdater::toJson(web::json::value& p_Json, const wstring& p_PropertyName, const wstring& p_SubPropertyName, const boost::YOpt<bool>& p_Val)
{
	if (p_Val)
	{
		auto& prop = p_Json[p_PropertyName];
		if (!p_SubPropertyName.empty())
		{
			prop[p_SubPropertyName] = *p_Val ? web::json::value::string(_YTEXT("true")) : web::json::value::string(_YTEXT("false"));
		}
		else
		{
			prop = *p_Val ? web::json::value::string(_YTEXT("true")) : web::json::value::string(_YTEXT("false"));
		}
	}
}

void MessageRuleUpdater::toJson(web::json::value& p_Json, const wstring& p_PropertyName, const wstring& p_SubPropertyName, const boost::YOpt<int32_t>& p_Val)
{
	if (p_Val)
	{
		auto& prop = p_Json[p_PropertyName];
		if (!p_SubPropertyName.empty())
		{
			prop[p_SubPropertyName] = web::json::value::string(std::to_wstring(*p_Val));
		}
		else
		{
			prop = web::json::value::string(std::to_wstring(*p_Val));;
		}
	}
}

void MessageRuleUpdater::toJson(web::json::value& p_Json, const wstring& p_PropertyName, const wstring& p_SubPropertyName, const boost::YOpt<vector<PooledString>>& p_Val)
{
	if (p_Val)
	{
		auto& prop = p_Json[p_PropertyName];
		if (!p_SubPropertyName.empty())
		{
			prop[p_SubPropertyName] = web::json::value::array(p_Val->size());

			size_t i = 0;
			for (auto& s : *p_Val)
				prop[p_SubPropertyName].as_array()[i++] = web::json::value::string(s);
		}
		else
		{
			prop = web::json::value::array(p_Val->size());

			size_t i = 0;
			for (auto& s : *p_Val)
				prop.as_array()[i++] = web::json::value::string(s);
		}
	}
}
