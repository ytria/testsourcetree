#pragma once

#include "IRequester.h"

#include "EducationSchool.h"
#include "ValueListDeserializer.h"
#include "IPageRequestLogger.h"

class EducationSchoolDeserializer;

class EducationSchoolsListRequester : public IRequester
{
public:
	EducationSchoolsListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const vector<EducationSchool>& GetData() const;

private:
	std::shared_ptr<ValueListDeserializer<EducationSchool, EducationSchoolDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;
};

