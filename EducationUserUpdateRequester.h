#pragma once

#include "IRequester.h"
#include "HttpResultWithError.h"

class EducationUserUpdateRequester : public IRequester
{
public:
	EducationUserUpdateRequester(const wstring& p_UserId, const web::json::value& p_UserChanges);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const HttpResultWithError& GetResult() const;

private:
	std::shared_ptr<HttpResultWithError> m_Result;

	wstring m_UserId;
	web::json::value m_UserChanges;
};

