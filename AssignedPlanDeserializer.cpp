#include "AssignedPlanDeserializer.h"

#include "JsonSerializeUtil.h"

void AssignedPlanDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("assignedDateTime"), m_Data.m_AssignedDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("capabilityStatus"), m_Data.m_CapabilityStatus, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("service"), m_Data.m_Service, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("servicePlanId"), m_Data.m_ServicePlanId, p_Object);
}
