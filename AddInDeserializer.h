#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "AddIn.h"

class AddInDeserializer : public JsonObjectDeserializer, public Encapsulate<AddIn>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

