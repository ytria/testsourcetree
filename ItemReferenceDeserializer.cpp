#include "ItemReferenceDeserializer.h"

#include "JsonSerializeUtil.h"
#include "SharepointIdsDeserializer.h"

void ItemReferenceDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("driveId"), m_Data.DriveId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("driveType"), m_Data.DriveType, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.Id, p_Object, true);
    JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.Name, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("path"), m_Data.Path, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("shareId"), m_Data.ShareId, p_Object);
    
    {
        SharepointIdsDeserializer isd;
        if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("sharepointIds"), p_Object))
            m_Data.SharepointIds = isd.GetData();
    }
}
