#pragma once

#include "IRequester.h"
#include "CosmosDbAccountKeyDeserializer.h"

class SingleRequestResult;

namespace Azure
{
	class GetDbAccountKeysRequester : public IRequester
	{
	public:
		GetDbAccountKeysRequester(PooledString p_SubscriptionId, PooledString p_ResourceGroup, PooledString p_DbAccountName);
		TaskWrapper<void> Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

		const Azure::CosmosDbAccountKeys& GetData() const;

	private:
		std::shared_ptr<Azure::CosmosDbAccountKeyDeserializer> m_Deserializer;
		std::shared_ptr<SingleRequestResult> m_Result;

		PooledString m_SubscriptionId;
		PooledString m_ResourceGroup;
		PooledString m_DbAccountName;
	};
}

