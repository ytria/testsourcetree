#include "DlgSelectCustomerHTML.h"

#include "Str.h"
#include "TimeUtil.h"
#include "JSONUtil.h"

BEGIN_MESSAGE_MAP(DlgSelectCustomerHTML, ResizableDialog)
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()

std::map<wstring, int, Str::keyLessInsensitive> DlgSelectCustomerHTML::g_HbsHeights;

DlgSelectCustomerHTML::DlgSelectCustomerHTML(const vector<Contract>& p_Contracts)
    : ResizableDialog(IDD)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_totalHbsHeight(0)
	, m_Contracts(p_Contracts)
	, m_Selected(-1)
{
	// Load hbs sizes
	if (g_HbsHeights.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_SESSIONLIST_SIZE, g_HbsHeights);
		ASSERT(success);
	}
}

int DlgSelectCustomerHTML::GetSelectedContractIndex() const
{
	return m_Selected;
}

bool DlgSelectCustomerHTML::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	ASSERT(data.size() == 1);

	if (data.size() == 1)
	{
		const wstring& selected = data.begin()->first;

		m_Selected = Str::getIntFromString(selected);
		ASSERT(0 <= m_Selected && size_t(m_Selected) < m_Contracts.size());

		endDialogFixed(IDOK);
		return false;
	}

	return true;
}

BOOL DlgSelectCustomerHTML::OnInitDialogSpecificResizable()
{
	SetWindowText(_T("Select Customer"));

	CWnd* pCtrl = GetDlgItem(IDC_GRID_PLACEHOLDER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(nullptr, nullptr, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, nullptr);

	// FIXME: Temporary - Remove when no more needed.
	//m_HtmlView->SetDebugDisplayPostedData(true);

	m_HtmlView->SetPostedDataTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
		{
			 { _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
			,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-sessionList.js"), _YTEXT("") } } // This file is in resources
			,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("sessionList.js"), _YTEXT("") } }, // This file is in resources
		}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("sessionList.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	int height = [=]()
	{
		const int maxHeight = [=]()
		{
			HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
			MONITORINFO info;
			info.cbSize = sizeof(MONITORINFO);
			GetMonitorInfo(monitor, &info);
			return 2 * (info.rcMonitor.bottom - info.rcMonitor.top) / 3;
		}();

		ASSERT(0 != m_totalHbsHeight);
		if (0 != m_totalHbsHeight)
		{
			{
				const auto it = g_HbsHeights.find(_YTEXT("marginTop"));
				if (g_HbsHeights.end() != it)
					m_totalHbsHeight += it->second;
			}
			{
				const auto it = g_HbsHeights.find(_YTEXT("marginBottom"));
				if (g_HbsHeights.end() != it)
					m_totalHbsHeight += it->second;
			}
			const int deltaH = HIDPI_YH(m_totalHbsHeight) - htmlRect.Height();
			return min(maxHeight, dlgRect.Height() + deltaH);
		}

		return maxHeight;
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_HbsHeights.find(_YTEXT("min-width-Window"));
			ASSERT(g_HbsHeights.end() != it);
			if (g_HbsHeights.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();
		const int delta = targetHtmlWidth - htmlRect.Width();
		return dlgRect.Width() + delta;
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);

	// Limit width to avoid erratic behavior that appears beyond this width.
	m_MaxWidth = HIDPI_XW(1040);

	BlockVResize(true);

    return TRUE;
}

void DlgSelectCustomerHTML::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)
{
	ResizableDialog::OnGetMinMaxInfo(lpMMI);

	lpMMI->ptMaxSize.x = m_MaxWidth;
	lpMMI->ptMaxTrackSize.x = m_MaxWidth;
}

void DlgSelectCustomerHTML::generateJSONScriptData(wstring& p_Output)
{    
	m_totalHbsHeight = 0;

	auto main = web::json::value::object({
		{ _YTEXT("introTitle"), web::json::value::string(_T("Select the customer you want")) }, // WARNING: That text must be short enough to stay on one line!
		{ _YTEXT("introSession"), web::json::value::string(_YTEXT("Display Name:")) },
		{ _YTEXT("introDOmain"), web::json::value::string(_YTEXT("Primary Domain:")) },
		{ _YTEXT("method"), web::json::value::string(_YTEXT("POST")) },
		{ _YTEXT("action"), web::json::value::string(_YTEXT("#")) },
		{ _YTEXT("isCustomerSelect"), web::json::value::string(_YTEXT("X")) }, // Remove footer
		});

	m_totalHbsHeight += 80; // Search area automatically added

    auto items = web::json::value::array();

    size_t itemIndex = 0;
	for (const auto& contract : m_Contracts)
	{
		items[itemIndex] = getJSONSessionEntry(contract, itemIndex);
		itemIndex++;
	}
    
    const auto root = web::json::value::object({
        { _YTEXT("main"), main },
        { _YTEXT("items"), items}
    });

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}

web::json::value DlgSelectCustomerHTML::getJSONSessionEntry(const Contract& p_Contract, size_t p_Index)
{
    auto entry = json::value::object();

    entry[_YTEXT("hbs")] = web::json::value::string(_YTEXT("SessionCard"));
	entry[_YTEXT("type")] = web::json::value::string(_YTEXT("customer"));
	entry[_YTEXT("btnValue")] = web::json::value::string(std::to_wstring(p_Index));
	if (p_Contract.m_DisplayName)
		entry[_YTEXT("sessionName")] = web::json::value::string(*p_Contract.m_DisplayName);
	if (p_Contract.m_DefaultDomainName)
		entry[_YTEXT("domainName")] = web::json::value::string(*p_Contract.m_DefaultDomainName);
	auto contractType = p_Contract.GetContractTypeDisplayName();
	if (contractType)
		entry[_YTEXT("contractType")] = web::json::value::string(*contractType);
	m_totalHbsHeight += getHbsHeight(_YTEXT("SessionCard"));

	return entry;
}

UINT DlgSelectCustomerHTML::getHbsHeight(const wstring& hbsName)
{
	auto it = g_HbsHeights.find(hbsName);
	ASSERT(g_HbsHeights.end() != it);
	if (g_HbsHeights.end() != it)
		return it->second;
	return 0;
}
