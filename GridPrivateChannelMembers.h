#pragma once

#include "BaseO365Grid.h"

#include "BusinessChannel.h"
#include "BusinessAADUserConversationMember.h"

class GridPrivateChannelMembers : public O365Grid
{
public:
	GridPrivateChannelMembers();
	virtual ~GridPrivateChannelMembers() override = default;

	void BuildView(const vector<BusinessChannel>& p_Channels, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	virtual ModuleCriteria	GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows);

	struct Changes
	{
		struct MemberRemoval
		{
			row_pk_t m_RowPk;
			wstring m_TeamId;
			wstring m_ChannelId;
			wstring m_MemberId;

			bool operator<(const MemberRemoval& p_Other) const
			{
				if (m_TeamId == p_Other.m_TeamId && m_ChannelId == p_Other.m_ChannelId)
					return m_MemberId < p_Other.m_MemberId;
				if (m_TeamId == p_Other.m_TeamId)
					return m_ChannelId < p_Other.m_ChannelId;
				return m_TeamId < p_Other.m_TeamId;
			}
		};

		struct MemberAddition
		{
			row_pk_t m_RowPk;
			wstring m_TeamId;
			wstring m_ChannelId;
			BusinessAADUserConversationMember m_Member;

			bool operator<(const MemberAddition& p_Other) const
			{
				if (m_TeamId == p_Other.m_TeamId && m_ChannelId == p_Other.m_ChannelId)
					return m_Member.GetID() < p_Other.m_Member.GetID();
				if (m_TeamId == p_Other.m_TeamId)
					return m_ChannelId < p_Other.m_ChannelId;
				return m_TeamId < p_Other.m_TeamId;
			}
		};

		struct MemberRoleUpdate
		{
			row_pk_t m_RowPk;
			wstring m_TeamId;
			wstring m_ChannelId;
			BusinessAADUserConversationMember m_Member;

			bool operator<(const MemberRoleUpdate& p_Other) const
			{
				if (m_TeamId == p_Other.m_TeamId && m_ChannelId == p_Other.m_ChannelId)
					return m_Member.GetID() < p_Other.m_Member.GetID();
				if (m_TeamId == p_Other.m_TeamId)
					return m_ChannelId < p_Other.m_ChannelId;
				return m_TeamId < p_Other.m_TeamId;
			}
		};

		const vector<MemberRemoval>& GetMemberRemovals() const
		{
			return m_MemberRemovals;
		}

		const vector<MemberAddition>& GetMemberAdditions() const
		{
			return m_MemberAdditions;
		}

		const vector<MemberRoleUpdate>& GetMemberRoleUpdates() const
		{
			return m_MemberRoleUpdates;
		}

		void Add(const MemberRemoval& p_MemberRemoval)
		{
			auto it = std::lower_bound(m_MemberRemovals.begin(), m_MemberRemovals.end(), p_MemberRemoval);
			m_MemberRemovals.insert(it, p_MemberRemoval);
		}

		void Add(const MemberAddition& p_MemberAddition)
		{
			auto it = std::lower_bound(m_MemberAdditions.begin(), m_MemberAdditions.end(), p_MemberAddition);
			m_MemberAdditions.insert(it, p_MemberAddition);
		}

		void Add(const MemberRoleUpdate& p_MemberRoleUpdate)
		{
			auto it = std::lower_bound(m_MemberRoleUpdates.begin(), m_MemberRoleUpdates.end(), p_MemberRoleUpdate);
			m_MemberRoleUpdates.insert(it, p_MemberRoleUpdate);
		}

	private:
		vector<MemberRemoval> m_MemberRemovals;
		vector<MemberAddition> m_MemberAdditions;
		vector<MemberRoleUpdate> m_MemberRoleUpdates;
	};

	Changes GetChanges(const std::set<GridBackendRow*>& p_Parents);
	virtual row_pk_t UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody) override;

protected:
	virtual void customizeGrid() override;

	afx_msg void OnShowUserDetails();
	afx_msg void OnShowParentGroups();
	afx_msg void OnShowLicenses();

	afx_msg void OnAddMember();
	afx_msg void OnUpdateAddMember(CCmdUI* pCmdUI);

	afx_msg void OnRemoveMember();
	afx_msg void OnUpdateRemoveMember(CCmdUI* pCmdUI);

	afx_msg void OnMakeOwner();
	afx_msg void OnUpdateMakeOwner(CCmdUI* pCmdUI);

	afx_msg void OnMakeMember();
	afx_msg void OnUpdateMakeMember(CCmdUI* pCmdUI);

	DECLARE_MESSAGE_MAP()

	virtual void InitializeCommands() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart) override;

private:
	void getUserIDs(O365IdsContainer& p_IDs, bool p_Selected, RoleDelegationUtil::RBAC_Privilege p_Privilege) const;

	vector<BusinessUser> getUsers() const;

	BusinessAADUserConversationMember getMember(GridBackendRow* p_Row) const;

protected:
	HACCEL m_hAccelSpecific = nullptr;

private:
	GridBackendColumn* m_ColTeamID;
	GridBackendColumn* m_ColTeamDisplayName;

	GridBackendColumn* m_ColChannelID;
	GridBackendColumn* m_ColChannelDisplayName;

	GridBackendColumn* m_ColMemberUserID;
	GridBackendColumn* m_ColMemberDisplayName;
	GridBackendColumn* m_ColMemberEmail;
	GridBackendColumn* m_ColMemberRoles;
	GridBackendColumn* m_ColMemberMembershipType;
};