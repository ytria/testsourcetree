#include "ModuleDirectoryAudits.h"

#include "Command.h"
#include "FrameDirectoryAudits.h"
#include "RefreshSpecificData.h"
#include "DirectoryAuditsRequester.h"
#include "safeTaskCall.h"
#include "CommandShowDirectoryAudits.h"
#include "DlgAuditLogsModuleOptions.h"

void ModuleDirectoryAudits::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
	case Command::ModuleTask::List:
		showAuditLogs(p_Command);
		break;
	case Command::ModuleTask::LoadSnapshot:
		loadSnapshot(p_Command);
		break;
	}
}

void ModuleDirectoryAudits::showAuditLogs(Command p_Command)
{
	AutomationAction* p_Action = p_Command.GetAutomationAction();
	const CommandInfo& info = p_Command.GetCommandInfo();
	const auto p_Origin = info.GetOrigin();

	RefreshSpecificData refreshSpecificData;
	bool isUpdate = false;

	auto p_SourceWindow = p_Command.GetSourceWindow();
	auto sourceCWnd = p_SourceWindow.GetCWnd();

	FrameDirectoryAudits* frame = dynamic_cast<FrameDirectoryAudits*>(info.GetFrame());
	if (nullptr == frame)
	{
		ASSERT(!p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>());

		if (FrameDirectoryAudits::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin = p_Origin;
			moduleCriteria.m_Privilege = info.GetRBACPrivilege();

			if (ShouldCreateFrame<FrameDirectoryAudits>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				DlgAuditLogsModuleOptions opt(p_SourceWindow.GetCWnd(), Sapio365Session::Find(p_Command.GetSessionIdentifier()));
				if (IDCANCEL == opt.DoModal())
				{
					if (nullptr != p_Action)
						SetActionCanceledByUser(p_Action);
					return;
				}
				p_Command.GetCommandInfo().Data2() = opt.GetOptions();

				CWaitCursor _;

				frame = new FrameDirectoryAudits(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), _T("Audit Logs"), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
					frame->SetOptions(p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
		}
	}
	else
	{
		isUpdate = true;
	}

	if (nullptr != frame)
	{
		ModuleOptions options;
		if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
			frame->SetOptions(p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>());
		CommandShowDirectoryAudits command(*frame, p_Command.GetAutomationSetup(), p_Command.GetAutomationAction(), p_Command.GetLicenseContext(), frame->GetOptions() ? *frame->GetOptions() : ModuleOptions(), isUpdate);
		command.Execute();
	}
	else
	{
		SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
}

void ModuleDirectoryAudits::loadSnapshot(const Command& p_Command)
{
	LoadSnapshot<FrameDirectoryAudits>(p_Command, _T("Audit Logs"));
}
