#include "BusinessMailFolder.h"
#include "BusinessMessage.h"
#include "MsGraphFieldNames.h"
#include "O365AdminUtil.h"
#include "safeTaskCall.h"

using namespace Util;

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessMailFolder>("Mail Folder") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Mail Folder"))))
		.constructor()(policy::ctor::as_object)
		.property(O365_MAILFOLDER_DISPLAYNAME, &BusinessMailFolder::m_DisplayName)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_MAILFOLDER_CHILDFOLDERCOUNT, &BusinessMailFolder::m_ChildFolderCount)
		.property(O365_MAILFOLDER_PARENTFOLDERID, &BusinessMailFolder::m_ParentFolderId)
		.property(O365_MAILFOLDER_TOTALITEMCOUNT, &BusinessMailFolder::m_TotalItemCount)
		.property(O365_MAILFOLDER_UNREADITEMCOUNT, &BusinessMailFolder::m_UnreadItemCount)
		;
}

BusinessMailFolder::BusinessMailFolder(const MailFolder& p_MailFolder)
{
    SetID(p_MailFolder.m_Id);

    if (boost::none != p_MailFolder.m_DisplayName)
        m_DisplayName = *p_MailFolder.m_DisplayName;
    if (boost::none != p_MailFolder.m_ChildFolderCount)
        m_ChildFolderCount = *p_MailFolder.m_ChildFolderCount;
    if (boost::none != p_MailFolder.m_ParentFolderId)
        m_ParentFolderId = *p_MailFolder.m_ParentFolderId;
    if (boost::none != p_MailFolder.m_TotalItemCount)
        m_TotalItemCount = *p_MailFolder.m_TotalItemCount;
    if (boost::none != p_MailFolder.m_UnreadItemCount)
        m_UnreadItemCount = *p_MailFolder.m_UnreadItemCount;

    vector<BusinessMailFolder> businessMailFolders;
    for (const auto& mailFolder : p_MailFolder.m_ChildrenMailFolders)
        businessMailFolders.push_back(mailFolder);

    vector<BusinessMessage> businessMessages;
    for (const auto& message : p_MailFolder.m_Messages)
        businessMessages.push_back(message);

    m_ChildrenMailFolders = businessMailFolders;
    m_Messages = businessMessages;
}

MailFolder BusinessMailFolder::ToMailFolder(const BusinessMailFolder & p_BusinessMailFolder)
{
    MailFolder mailfolder;

    mailfolder.m_Id = p_BusinessMailFolder.GetID();
    mailfolder.m_DisplayName = p_BusinessMailFolder.m_DisplayName;
    mailfolder.m_ChildFolderCount = p_BusinessMailFolder.m_ChildFolderCount;
    mailfolder.m_ParentFolderId = p_BusinessMailFolder.m_ParentFolderId;
    mailfolder.m_TotalItemCount = p_BusinessMailFolder.m_TotalItemCount;
    mailfolder.m_UnreadItemCount = p_BusinessMailFolder.m_UnreadItemCount;
    
    vector<MailFolder> children;
    for (const auto& businessMailFolder : p_BusinessMailFolder.m_ChildrenMailFolders)
        mailfolder.m_ChildrenMailFolders.push_back(ToMailFolder(businessMailFolder));
    mailfolder.m_ChildrenMailFolders = children;

    vector<Message> messages;
    for (const auto& businessMessage : p_BusinessMailFolder.m_Messages)
        mailfolder.m_Messages.push_back(BusinessMessage::ToMessage(businessMessage));
    mailfolder.m_Messages = messages;

    return mailfolder;
}
