#pragma once

#include "ModuleBase.h"

#include "CommandInfo.h"

class AutomationAction;
class FrameLists;
struct RefreshSpecificData;

class ModuleLists : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

private:
    virtual void executeImpl(const Command& p_Command) override;
	void showLists(const Command& p_Command);
    void doRefresh(FrameLists* newFrame, const ModuleCriteria& p_ModuleCriteria, const Command& p_Command, const RefreshSpecificData& p_RefreshData, bool p_IsRefresh);
};

