#pragma once

#include "SapioError.h"
#include "RestResultInfo.h"
#include "RttrIncludes.h"

struct HttpResultWithError
{
    HttpResultWithError();
    HttpResultWithError(const boost::YOpt<RestResultInfo>& p_ResultInfo, const boost::YOpt<SapioError>& p_Error);
	HttpResultWithError(const boost::YOpt<RestResultInfo>& p_ResultInfo);

    boost::YOpt<RestResultInfo> m_ResultInfo;
    boost::YOpt<SapioError> m_Error;
};

