#pragma once

#include <memory>

namespace Azure
{
	class Paginator
	{
	public:
		using FunctionType = std::function<web::json::value(const web::uri&)>;
		Paginator(const web::uri& p_Uri, FunctionType p_RequestFunction);

		pplx::task<void> Paginate();

	private:
		web::uri m_InitialUri;
		FunctionType m_RequestFunction;
	};
}
