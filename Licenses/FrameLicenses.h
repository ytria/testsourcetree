#pragma once

#include "GridFrameBase.h"
#include "GridLicenses.h"

class FrameLicenses : public GridFrameBase
{
public:
	FrameLicenses(bool p_IsMyData, bool p_IsHandleSkuCounters, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

	DECLARE_DYNAMIC(FrameLicenses)
	virtual ~FrameLicenses() = default;

	void ShowLicenses(const O365DataMap<BusinessUser, vector<BusinessLicenseDetail>>& p_LicenseDetails, const vector<BusinessSubscribedSku>& m_SubscribedSkus, bool p_FullPurge);

	virtual O365Grid& GetGrid() override;
    virtual void ApplySelectedSpecific() override;
	virtual void ApplyAllSpecific() override;

	virtual void RefreshSpecific(AutomationAction* p_CurrentAction);
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

    GridLicenses& GetGridLicenses();
    const GridLicenses& GetGridLicenses() const;

// Generated message map functions
protected:
	virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;
	virtual O365ControlConfig GetApplyPartialControlConfig() override;
	virtual O365ControlConfig GetRevertPartialControlConfig() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

	virtual void revertSelectedImpl() override;

// Members
private:
    void ApplySpecific(bool p_Selected);
	GridLicenses m_LicensesGrid;
};
