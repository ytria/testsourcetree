#pragma once

#include "BaseO365Grid.h"
#include "BusinessLicenseDetail.h"
#include "BusinessSubscribedSku.h"
#include "BusinessUser.h"
#include "GridTemplateUsers.h"
#include "GridUpdater.h"
#include "MyDataMeRowHandler.h"
#include "SkuInfo.h"

class GridLicenses : public O365Grid
{
public:
	GridLicenses(bool p_IsMyData, bool p_IsHandleSkuCounters);

	virtual void customizeGrid() override;
    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessLicenseDetail>>& p_LicensesData, const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus, bool p_FullPurge);
    map<PooledString, int32_t> ComputeDeltasFromMods();

	void ToggleShowNoLicenseRows();
	bool IsShowNoLicenseRows() const;

	void ToggleShowPlanRows();
	bool IsShowPlanRows() const;

    int32_t GetNbConsumedBySku(PooledString p_SkuId, bool p_Rbac) const;
	const map<PooledString, int32_t>& GetConsumedMap() const;

	PooledString GetSkuPartNumber(PooledString Id) const;

	PooledString GetPlanPartNumber(PooledString IdSku, PooledString IdPlan) const;

    static const wstring g_ProvisioningStatusDisabled;

	wstring GetDisplayName(GridBackendRow* p_Row) const;

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	virtual bool HasModificationsPending(bool inSelectionOnly = false) override;

	GridBackendColumn* GetColMetaId() const;
	GridBackendColumn* GetColMetaUsageLocation() const;
	GridBackendColumn* GetColMetaDisplayName() const;
	GridBackendColumn* GetColMetaUserPrincipalName() const;
	GridBackendColumn* GetColIsAssigned() const;
	GridBackendColumn* GetColIsAssignedBool() const;
	GridBackendColumn* GetColSkuId() const;
	GridBackendColumn* GetColSkuPartNumber() const;
	GridBackendColumn* GetColSkuName() const;
	GridBackendColumn* GetColServicePlanID() const;
	GridBackendColumn* GetColServicePlanName() const;
	GridBackendColumn* GetColServicePlanFriendlyName() const;
	GridBackendColumn* GetColConsumedUnits() const;
	GridBackendColumn* GetColConsumedUnitsInRbacScope() const;
	GridBackendColumn* GetColEnabledUnits() const;
	GridBackendColumn* GetColEnabledUnitsInRbacScope() const;
	int GetPlanMarkedForActivationId() const;
	int GetPlanMarkedForDeactivationId() const;
	int GetSkuMarkedForActivationId() const;
	int GetSkuMarkedForDeactivationId() const;

	bool IsMyData() const;

	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

private:
    DECLARE_MESSAGE_MAP()

	afx_msg void OnShowUnassigned();
	afx_msg void OnUpdateShowUnassigned(CCmdUI* pCmdUI);
    afx_msg void OnEditLicenses();
	afx_msg void OnUpdateEditLicenses(CCmdUI* pCmdUI);
	afx_msg void OnEditUsageLocation();
	afx_msg void OnUpdateEditUsageLocation(CCmdUI* pCmdUI);
	afx_msg void OnShowPlans();
	afx_msg void OnUpdateShowPlans(CCmdUI* pCmdUI);

	virtual void InitializeCommands() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart);

    void FillRows(GridBackendRow* p_ParentRow, const BusinessUser& p_User, const vector<BusinessLicenseDetail>& p_LicensesData, const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus, GridUpdater& p_Updater);
	const PooledString& getServicePlan(const boost::YOpt<PooledString>& p_SkuPartNumber, const boost::YOpt<PooledString>& m_ServicePlanName);
	const PooledString& getSubscriptionName(const boost::YOpt<PooledString>& p_SkuPartNumber, const boost::YOpt<PooledString>& m_ServicePlanName);

    void editLicenses();
    bool arePlansSetDifferent(const std::set<PlanInfo>& p_Plans1, const std::set<PlanInfo>& p_Plans2) const;
    void createModifications(const std::map<PooledString, std::set<SkuInfo>>& p_OldState, const std::map<PooledString, std::set<SkuInfo>>& p_WantedState);

    vector<GridBackendField> getUserPkFromSkuPk(const vector<GridBackendField> &p_SkuPk);

	void refreshCustomHiddenRows(bool p_UpdateGrid);
	void refreshCustomRowVisibility(GridBackendRow* p_Row) const;

	void setUsageLocation(GridBackendRow* p_Row, const boost::YOpt<PooledString>& p_UsageLocation);

	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

private:
	std::map<PooledString, PooledString> m_skuDetails;
	std::map< std::pair<PooledString, PooledString>, PooledString > m_planDetails;

	GridTemplateUsers m_Template;

	/*LEVEL 1*/
	GridBackendColumn* m_ColSkuId; // H
	GridBackendColumn* m_ColSkuPartNumber;
	GridBackendColumn* m_ColSkuLabel;

	//GridBackendColumn* m_ColSubscriptionName; // ?

	GridBackendColumn* m_ColIsAssigned;
    GridBackendColumn* m_ColIsAssignedBool;

	GridBackendColumn* m_ColIsAssignedByGroup;
	GridBackendColumn* m_ColAssignedByGroupId;
	GridBackendColumn* m_ColAssignedByGroupDisplayName;
	GridBackendColumn* m_ColAssignedByGroupMail;
	GridBackendColumn* m_ColAssignementState;
	GridBackendColumn* m_ColAssignementStateError;

	GridBackendColumn* m_ColCapabilityStatus;
	GridBackendColumn* m_ColAppliesTo;
	GridBackendColumn* m_ColConsumedUnits;
	GridBackendColumn* m_ColConsumedUnitsInRbacScope;
	GridBackendColumn* m_ColEnabledUnits;
	GridBackendColumn* m_ColEnabledUnitsInRbacScope;
	GridBackendColumn* m_ColSuspendedUnits;
	GridBackendColumn* m_ColWarningUnits;

	/*LEVEL 2*/
	GridBackendColumn* m_ColServicePlanID;
	GridBackendColumn* m_ColServicePlanName;
	GridBackendColumn* m_ColServicePlanFriendlyName;
	//GridBackendColumn* m_ColServicePlan;
	GridBackendColumn* m_ColGlobalServiceProvisionningStatus;
	GridBackendColumn* m_ColGlobalServiceAppliesTo;
	//GridBackendColumn* m_ColServiceName;
	GridBackendColumn* m_ColServiceCapabilityStatus;
	GridBackendColumn* m_ColServiceProvisioningStatus;
	GridBackendColumn* m_ColServiceAssignedDate;
	GridBackendColumn* m_ColServiceLicenseProvisioningStatus;
	GridBackendColumn* m_ColServiceLicenseProvisioningAppliesTo;

	GridBackendColumn* m_ColumnLastRequestDateTime;

	vector<GridBackendColumn*>	m_MetaColumns;
	bool m_HasLoadMoreAdditionalInfo;
	O365IdsContainer m_MetaIDsMoreLoaded;

    int m_PlanMarkedForActivationId;
    int m_PlanMarkedForDeactivationId;
    int m_SkuMarkedForActivationId;
    int m_SkuMarkedForDeactivationId;
	int m_HasLicenseIconId;
	int m_HasNotLicenseIconId;
	int m_SuccessPlanIconId;
	int m_DisabledPlanIconId;
	int m_NotApplicablePlanIconId;
	int m_PendingInputPlanIconId;
	int m_PendingActivationPlanIconId;
	int m_PendingProvisioningPlanIconId;
	int m_AssignedByGroupIconId;

	struct TextAndIcon
	{
		TextAndIcon() = default;
		TextAndIcon(const wstring& p_Text, int p_Icon) : m_Text(p_Text), m_Icon(p_Icon) {}
		const wstring m_Text;
		const int m_Icon = -1;
	};
	using TextAndIconMap = std::map<wstring, TextAndIcon, Str::keyLessInsensitive>;
	TextAndIconMap m_ProvisioningStatusTextAndIcon;

    map<PooledString, int32_t> m_ConsumedSkus; // Key: SkuId, Value: Nb consumed skus
	map<PooledString, int32_t> m_RbacConsumedSkus;

	bool m_ShowNoLicenseRows;
	bool m_ShowPlanRows;

	bool m_SkuError;

	const bool m_IsMyData;
	boost::YOpt<MyDataMeRowHandler> m_MyDataMeHandler;

	const bool m_IsHandleSkuCounters;

	std::set<GridBackendColumn*> m_ColumnsToRepeatOnPlans;

	static const wstring g_IdAssignedLicense;
	static const wstring g_IdUnassignedLicense;
	static const wstring g_IdNotApplicablePlan;
	static wstring g_AssignedByGroup;

	friend class GridLicensesSummary; // Give access to TextAndIconMap type
};
