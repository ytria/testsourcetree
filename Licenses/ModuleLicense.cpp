#include "YSafeCreateTask.h"
#include "ModuleLicense.h"

#include "AutomationDataStructure.h"
#include "BusinessMessage.h"
#include "BusinessObjectManager.h"
#include "FrameLicenses.h"
#include "FrameLicensesDeletedUsers.h"
#include "FrameLicensesSummary.h"
#include "ModuleUser.h"
#include "MSGraphCommonData.h"
#include "MSGraphUtil.h"
#include "Office365Admin.h"
#include "RESTUtils.h"
#include "RefreshSpecificData.h"
#include "safeTaskCall.h"
#include "TaskDataManager.h"
#include "UpdatedObjectsGenerator.h"
#include "UserListLicensePropertySet.h"
#include "UserLicenseModification.h"
#include "UserLicenseUpdateRequester.h"
#include "YCallbackMessage.h"
#include "YDataCallbackMessage.h"
#include "BasicPageRequestLogger.h"
#include "MultiObjectsPageRequestLogger.h"

using namespace Rest;

void ModuleLicense::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
    {
	case Command::ModuleTask::ListMe:
	{
		Command newCmd = p_Command;
		newCmd.GetCommandInfo().GetIds() = { PooledString(GetConnectedSession().GetConnectedUserId()) };
		showLicenses(newCmd);
		break;
	}
    case Command::ModuleTask::List:
		showLicenses(p_Command);
        break;
    case Command::ModuleTask::ApplyChanges:
        applyChanges(p_Command);
        break;
	case Command::ModuleTask::UpdateRefresh:
		refresh(p_Command);
		break;
	case Command::ModuleTask::LoadSnapshot:
		loadSnapshot(p_Command);
		break;
    default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
		break;
    }
}

void ModuleLicense::applyChanges(const Command& p_Command)
{
    CommandInfo info = p_Command.GetCommandInfo();
    AutomationAction* action = p_Command.GetAutomationAction();
    FrameLicenses* frame = dynamic_cast<FrameLicenses*>(info.GetFrame());
    ASSERT(nullptr != frame);

	using DataType = O365IdsContainer;
    ASSERT(info.Data().is_type<DataType>());
    if (nullptr != frame && info.Data().is_type<DataType>())
    {
		const auto& ids = info.Data().get_value<DataType>();

        // Prepare request objects
        const auto modifications = frame->GetGridLicenses().GetModifications().GetRowModifications([](RowModification*) { return true; });

        vector<UserLicenseUpdateRequester> requests;
        requests.reserve(modifications.size());
		map<std::pair<PooledString, row_pk_t>, BusinessUser> usersForUsageLocationUpdate;
        for (const auto& mod : modifications)
        {
            auto licenseMod = dynamic_cast<UserLicenseModification*>(mod); // Fail
            ASSERT(nullptr != licenseMod);
            if (nullptr != licenseMod)
            {
                if (ids.find(licenseMod->GetUserId()) != ids.end())
                {
                    ASSERT(!licenseMod->GetDisabledSkus().empty() || !licenseMod->GetModifiedSkus().empty() || licenseMod->HasUsageLocationChange());
                    if (!licenseMod->GetDisabledSkus().empty() || !licenseMod->GetModifiedSkus().empty())
                    {
                        requests.emplace_back(licenseMod->GetUserId());
                        requests.back().SetDisabledSkus(licenseMod->GetDisabledSkus());
                        requests.back().SetModifiedSkus(licenseMod->GetModifiedSkus());
                    }

					if (licenseMod->HasUsageLocationChange())
					{
						usersForUsageLocationUpdate[{licenseMod->GetUserId(), licenseMod->GetRowKey()}].SetID(licenseMod->GetUserId());
						usersForUsageLocationUpdate[{licenseMod->GetUserId(), licenseMod->GetRowKey()}].SetUsageLocation(licenseMod->GetNewUsageLocation());
						usersForUsageLocationUpdate[{licenseMod->GetUserId(), licenseMod->GetRowKey()}].SetUpdateStatus(BusinessObject::UPDATE_TYPE_MODIFY);
					}
                }
            }
        }

        RefreshSpecificData refreshData;
        refreshData.m_Criteria = frame->GetModuleCriteria();
        ASSERT(refreshData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::IDS);
        refreshData.m_Criteria.m_IDs = ids;

        auto taskData = addFrameTask(YtriaTranslate::Do(ModuleLicense_applyChanges_1, _YLOC("Applying changes")).c_str(), frame, p_Command, false);

		YSafeCreateTaskMutable([bom = frame->GetBusinessObjectManager(), p_Command, refreshData, requests, usersForUsageLocationUpdate, hwnd = frame->m_hWnd, taskData, action/*, objs = userUpdatedObjectsGenerator*/]() mutable
        {
			std::map<PooledString, O365UpdateOperation> updateOperationsByUserID;
			std::set<PooledString> userWithUsageLocationUpdateError;
			for (auto& item : usersForUsageLocationUpdate)
			{
				LoggerService::User(YtriaTranslate::Do(ModuleLicense_applyChanges_3, _YLOC("Updating user with id \"%1\""), item.second.GetID().c_str()), taskData.GetOriginator());
				updateOperationsByUserID.insert({ item.first.first, { item.first.second } });
				updateOperationsByUserID.at(item.first.first).StoreResults(item.second.SendWriteSingleRequest(bom->GetSapio365Session(), taskData).GetTask().get());

				if (updateOperationsByUserID.at(item.first.first).GetResults()[0].m_Error)
					userWithUsageLocationUpdateError.insert(item.first.first);
			}

			// Do not process requests if usage location update has failed for the user.
			for (auto it = requests.begin(); it != requests.end();)
			{
				auto& request = *it;
				if (userWithUsageLocationUpdateError.end() == userWithUsageLocationUpdateError.find(request.GetUserId()))
				{
					request.Send(bom->GetSapio365Session(), taskData).GetTask().wait();
					++it;
				}
				else
				{
					it = requests.erase(it);
				}
			}

            YCallbackMessage::DoPost([hwnd, refreshData, requests, action, taskData, updateOperationsByUserID{ std::move(updateOperationsByUserID) }]()
            {
                if (::IsWindow(hwnd))
                {
                    auto frame = dynamic_cast<FrameLicenses*>(CWnd::FromHandle(hwnd));
                    ASSERT(nullptr != frame);
                    if (nullptr != frame)
                    {
                        const auto& modifications = frame->GetGridLicenses().GetModifications().GetRowModifications([](RowModification*) { return true; });

						// Assign the result of the request to all applied modifications
						size_t nbRequestResultsSet = 0;
						size_t nbUsageLocResultsSet = 0;
						for (auto mod : modifications)
						{
							auto licenseMod = dynamic_cast<UserLicenseModification*>(mod);
							ASSERT(nullptr != licenseMod);
							if (nullptr != licenseMod)
							{
								const auto& userId = licenseMod->GetUserId();

								auto it = updateOperationsByUserID.find(userId);
								if (updateOperationsByUserID.end() != it)
								{
									ASSERT(it->second.GetResults().size() == 1);
									licenseMod->SetUsageLocationResult(it->second.GetResults()[0]);
									++nbUsageLocResultsSet;
								}

								// Might be too slow if many modifications, optimize if needed
								for (const auto& request : requests)
								{
									if (userId == request.GetUserId())
									{
										licenseMod->SetResult(request.GetResult());
										++nbRequestResultsSet;
									}
								}
							}
						}

                        ASSERT(nbRequestResultsSet == requests.size() && nbUsageLocResultsSet == updateOperationsByUserID.size());
                        frame->GetGrid().ClearLog(taskData.GetId());
						frame->RefreshAfterUpdate({}, refreshData, action);
                    }

                }
                TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
            });
        });
    }
    else
    {
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2453")).c_str());
    }
}

void ModuleLicense::showLicenses(const Command& p_Command)
{
    const auto& info = p_Command.GetCommandInfo();

	if (info.GetOrigin() == Origin::User)
	{
		const auto&	p_UserIDs		= info.GetIds();
		const auto	p_Origin		= info.GetOrigin();
		auto		p_SourceWindow	= p_Command.GetSourceWindow();
		auto		p_Action		= p_Command.GetAutomationAction();

		auto sourceCWnd = p_SourceWindow.GetCWnd();
		if (FrameLicenses::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin				= p_Origin;
			moduleCriteria.m_UsedContainer		= ModuleCriteria::UsedContainer::IDS;
			moduleCriteria.m_IDs				= p_UserIDs;
			moduleCriteria.m_Privilege			= info.GetRBACPrivilege();
			moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

			if (ShouldCreateFrame<FrameLicenses>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				const wstring title = p_Command.GetTask() == Command::ModuleTask::ListMe
					? _T("My Licenses and Service Plans")
					: YtriaTranslate::Do(ModuleLicense_showLicenses_1, _YLOC("Licenses and Service Plans")).c_str();
				auto session = Sapio365Session::Find(p_Command.GetSessionIdentifier());
				FrameLicenses* newFrame = new FrameLicenses(p_Command.GetTask() == Command::ModuleTask::ListMe, session && session->GetGraphCache().IsHandleSkuCounters(), p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), title, *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				newFrame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(newFrame);
				newFrame->Erect();
				doRefresh(newFrame, moduleCriteria, p_Command, false);
			}
		}
		else
			SetAutomationGreenLight(p_Action, YtriaTranslate::DoError(ModuleLicense_showLicenses_4, _YLOC("Unable to create frame"),_YR("Y2454")).c_str());
	}
	else if (info.GetOrigin() == Origin::DeletedUser)
	{
		const auto&	p_UserIDs = info.GetIds();
		const auto	p_Origin = info.GetOrigin();
		auto		p_SourceWindow = p_Command.GetSourceWindow();
		auto		p_Action = p_Command.GetAutomationAction();

		auto sourceCWnd = p_SourceWindow.GetCWnd();
		if (FrameLicensesDeletedUsers::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin			= p_Origin;
			moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::IDS;
			moduleCriteria.m_IDs			= p_UserIDs;
			moduleCriteria.m_Privilege		= info.GetRBACPrivilege();

			if (ShouldCreateFrame<FrameLicenses>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				FrameLicensesDeletedUsers* newFrame = new FrameLicensesDeletedUsers(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleLicense_showLicenses_3, _YLOC("Deleted Users Licenses")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				newFrame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(newFrame);
				newFrame->Erect();
				doRefresh(newFrame, moduleCriteria, p_Command, false);
			}
		}
		else
			SetAutomationGreenLight(p_Action, YtriaTranslate::DoError(ModuleLicense_showLicenses_4, _YLOC("Unable to create frame"),_YR("Y2455")).c_str());
	}
	else if (info.GetOrigin() == Origin::Tenant)
	{
		const auto	p_Origin		= info.GetOrigin();
		auto		p_SourceWindow	= p_Command.GetSourceWindow();
		auto		p_Action		= p_Command.GetAutomationAction();

		auto sourceCWnd = p_SourceWindow.GetCWnd();
		if (FrameLicensesSummary::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin		= p_Origin;
			moduleCriteria.m_Privilege	= info.GetRBACPrivilege();

			if (ShouldCreateFrame<FrameLicensesSummary>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				auto session = Sapio365Session::Find(p_Command.GetSessionIdentifier());
				FrameLicensesSummary* newFrame = new FrameLicensesSummary(session && session->GetGraphCache().IsHandleSkuCounters(), p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleLicense_showLicenses_2, _YLOC("Tenant Licenses and Service Plans")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				newFrame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(newFrame);
				newFrame->Erect();
				doRefresh(newFrame, moduleCriteria, p_Command, false);
			}
		}
		else
			SetAutomationGreenLight(p_Action, YtriaTranslate::DoError(ModuleLicense_showLicenses_4, _YLOC("Unable to create frame"),_YR("Y2456")).c_str());
	}
	else
	{
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), _YTEXT(""));
	}
}

void ModuleLicense::refresh(const Command& p_Command)
{
    CommandInfo				info								= p_Command.GetCommandInfo();
	FrameLicenses*			p_pFrameLicenses					= dynamic_cast<FrameLicenses*>(info.GetFrame());
	FrameLicensesSummary*	p_pFrameLicensesSummary				= dynamic_cast<FrameLicensesSummary*>(info.GetFrame());
	FrameLicensesDeletedUsers*	p_pFrameLicensesDeletedUsers	= dynamic_cast<FrameLicensesDeletedUsers*>(info.GetFrame());
	AutomationAction*		p_Action							= p_Command.GetAutomationAction();

	// The frame to update once the data has been sent.
	ASSERT(nullptr != p_pFrameLicenses || nullptr != p_pFrameLicensesSummary || nullptr != p_pFrameLicensesDeletedUsers);
	if (nullptr != p_pFrameLicenses)
		doRefresh(p_pFrameLicenses, p_pFrameLicenses->GetModuleCriteria(), p_Command, true);
	else if (nullptr != p_pFrameLicensesSummary)
		doRefresh(p_pFrameLicensesSummary, p_pFrameLicensesSummary->GetModuleCriteria(), p_Command, true);
	else if (nullptr != p_pFrameLicensesDeletedUsers)
		doRefresh(p_pFrameLicensesDeletedUsers, p_pFrameLicensesDeletedUsers->GetModuleCriteria(), p_Command, true);
	else
		SetAutomationGreenLight(p_Action, _YTEXT(""));
}

void ModuleLicense::doRefresh(FrameLicenses* p_pFrame, const ModuleCriteria& p_ModuleCriteria, const Command& p_Command, bool p_IsRefresh)
{
	auto taskData = addFrameTask(YtriaTranslate::Do(ModuleLicense_doRefresh_1, _YLOC("Show Licenses and Service Plans")).c_str(), p_pFrame, p_Command, !p_IsRefresh);

    RefreshSpecificData refreshSpecificData;
    if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
        refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

	ASSERT(ModuleCriteria::UsedContainer::IDS == p_ModuleCriteria.m_UsedContainer && Origin::User == p_ModuleCriteria.m_Origin);
	if (ModuleCriteria::UsedContainer::IDS == p_ModuleCriteria.m_UsedContainer && Origin::User == p_ModuleCriteria.m_Origin)
	{
		// We need to load users to have AssignedPlans, ProvisionedPlans and AssignedLicenses that aren't in cache.
        const bool partialRefresh = !refreshSpecificData.m_Criteria.m_IDs.empty();
        const auto& ids = partialRefresh ? refreshSpecificData.m_Criteria.m_IDs : p_ModuleCriteria.m_IDs;

		YSafeCreateTask([partialRefresh, taskData, hwnd = p_pFrame->GetSafeHwnd(), ids, oldConsumedMap = p_pFrame->GetGridLicenses().GetConsumedMap(), p_Action = p_Command.GetAutomationSetup().m_ActionToExecute, bom = p_pFrame->GetBusinessObjectManager(), p_IsRefresh, origin = p_ModuleCriteria.m_Origin]()
		{
			boost::YOpt<vector<BusinessSubscribedSku>> subscribedSkus(vector<BusinessSubscribedSku>{});
			auto sskus = ModuleUser::getSubscribedSkus(bom, taskData, subscribedSkus);

			// FIXME: In theory, we should only force a refresh
			// if !bom->GetGraphCache().IsSqlUsersCacheFull() OR if transferred columns relates to license OR if refresh after update
			// But it's not that simple to know this, and with Sync, refreshing users should be quite efficient.
			bool forceWholeListRefresh = true;// !bom->GetGraphCache().IsSqlUsersCacheFull();
			if (!forceWholeListRefresh && bom->GetGraphCache().IsHandleSkuCounters())
			{
				const auto newConsumedMap = Util::GetConsumedMap(*subscribedSkus);

				// Refresh whole list if some consumed count have changed.
				// FIXME: Would we want to only check specific RBAC-related skus?
				forceWholeListRefresh = oldConsumedMap != newConsumedMap;
			}

			auto licenseLogger = std::make_shared<MultiObjectsPageRequestLogger>(_T("license details"), ids.size());
			O365DataMap<BusinessUser, vector<BusinessLicenseDetail>> licenseDetails;
			auto processUser = [&licenseDetails, &licenseLogger, &bom, &taskData](BusinessUser& user)
			{
				if (!taskData.IsCanceled())
				{
					auto userLicenseDetails = bom->GetUserBusinessLicenseDetails(user.GetID(), licenseLogger, taskData).Then([](const vector<BusinessLicenseDetail>& p_LicenseDetails)
						{
							return p_LicenseDetails;
						}).GetTask().get();

						if (taskData.IsCanceled())
						{
							user.SetFlags(BusinessObject::CANCELED);
							licenseDetails[user] = {};
						}
						else
						{
							licenseDetails[user] = userLicenseDetails;
						}
				}
				else
				{
					user.SetFlags(BusinessObject::CANCELED);
					licenseDetails[user] = {};
				}
			};

			// Update cache if necessary
			if (forceWholeListRefresh)
			{
				std::vector<BusinessUser> users;
				auto usersLogger = std::make_shared<BasicPageRequestLogger>(_T("users"));

				users = bom->GetGraphCache().GetUpToDateUsers<BusinessUser>(ids, usersLogger, GraphCache::PreferredMode::DELTASYNC, taskData).get();
				std::sort(users.begin(), users.end(), [&ids](const auto& lhUser, const auto& rhUser)
					{
						return ids.find(lhUser.GetID()) < ids.find(rhUser.GetID());
					});

				for (auto& user : users)
				{
					licenseLogger->IncrementObjCount();
					licenseLogger->SetContextualInfo(Util::GetDisplayNameOrId(user));
					processUser(user);
				}
			}
			else
			{
				Util::ProcessSubUserItems(processUser, ids, origin, bom->GetGraphCache(), true, licenseLogger, taskData);
			}

			struct LicenceStuff
			{
				O365DataMap<BusinessUser, vector<BusinessLicenseDetail>> m_LicenseDetails;
				vector<BusinessSubscribedSku> m_SubscribedSkus;
			};

			YDataCallbackMessage<LicenceStuff>::DoPost({ licenseDetails, *subscribedSkus }, [hwnd, partialRefresh, taskData, p_Action, p_IsRefresh, isCanceled = taskData.IsCanceled()](const LicenceStuff& p_LicenseStuff)
			{
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameLicenses*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(p_LicenseStuff.m_LicenseDetails)))
				{
					ASSERT(nullptr != frame);
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting Service Plans for %s users (%s Subscribed SKUs)...", Str::getStringFromNumber(p_LicenseStuff.m_LicenseDetails.size()).c_str(), Str::getStringFromNumber(p_LicenseStuff.m_SubscribedSkus.size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());

					{
						CWaitCursor _;
						frame->ShowLicenses(p_LicenseStuff.m_LicenseDetails, p_LicenseStuff.m_SubscribedSkus, !partialRefresh);
					}
					
					if (!p_IsRefresh)
					{
						frame->UpdateContext(taskData, hwnd);
						shouldFinishTask = false;
					}
				}

				if (shouldFinishTask)
				{
					// Because we didn't (and can't) call UpdateContext here.
					ModuleBase::TaskFinished(frame, taskData, p_Action);
				}
			});
		});
	}
}

void ModuleLicense::doRefresh(FrameLicensesSummary* p_pFrame, const ModuleCriteria& p_ModuleCriteria, const Command& p_Command, bool p_IsRefresh)
{
	auto taskData = addFrameTask(YtriaTranslate::Do(ModuleLicense_doRefresh_2, _YLOC("Show Tenant Licenses and Service Plans")).c_str(), p_pFrame, p_Command, !p_IsRefresh);
	auto subscribedSkusTask = p_pFrame->GetBusinessObjectManager()->GetSubscribedSkus(taskData);

	ASSERT(Origin::Tenant == p_ModuleCriteria.m_Origin);
	if (Origin::Tenant == p_ModuleCriteria.m_Origin)
	{
		subscribedSkusTask.ThenByTask([taskData, hwnd = p_pFrame->GetSafeHwnd(), p_Action = p_Command.GetAutomationSetup().m_ActionToExecute, p_IsRefresh](pplx::task<vector<BusinessSubscribedSku>> p_PreviousTask)
		{
			auto subscribedSkus = p_PreviousTask.get();
			YDataCallbackMessage<vector<BusinessSubscribedSku>>::DoPost({ subscribedSkus }, [hwnd, taskData, p_Action, p_IsRefresh, isCanceled = taskData.IsCanceled()](const vector<BusinessSubscribedSku>& p_SubscribedSkus)
			{
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameLicensesSummary*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, isCanceled, taskData, false))
				{
					ASSERT(nullptr != frame);
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting Service Plans for %s Subscribed SKUs...", Str::getStringFromNumber(p_SubscribedSkus.size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());

					{
						CWaitCursor _;
						frame->ShowLicenses(p_SubscribedSkus);
					}

					if (!p_IsRefresh)
					{
						frame->UpdateContext(taskData, hwnd);
						shouldFinishTask = false;
					}
				}

				if (shouldFinishTask)
				{
					// Because we didn't (and can't) call UpdateContext here.
					ModuleBase::TaskFinished(frame, taskData, p_Action);
				}
			});
		});
	}
}

void ModuleLicense::doRefresh(FrameLicensesDeletedUsers* p_pFrame, const ModuleCriteria& p_ModuleCriteria, const Command& p_Command, bool p_IsRefresh)
{
	auto taskData = addFrameTask(YtriaTranslate::Do(ModuleLicense_doRefresh_1, _YLOC("Show Licenses and Service Plans")).c_str(), p_pFrame, p_Command, !p_IsRefresh);

	RefreshSpecificData refreshSpecificData;
	if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
		refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

	auto subscribedSkusTask = p_pFrame->GetBusinessObjectManager()->GetSubscribedSkus(taskData);

	ASSERT(ModuleCriteria::UsedContainer::IDS == p_ModuleCriteria.m_UsedContainer && Origin::DeletedUser == p_ModuleCriteria.m_Origin);
	if (ModuleCriteria::UsedContainer::IDS == p_ModuleCriteria.m_UsedContainer && Origin::DeletedUser == p_ModuleCriteria.m_Origin)
	{
		// We need to load users to have AssignedPlans, ProvisionedPlans and AssignedLicenses that aren't in cache.
		// FIXME: Should we do as in user module, i.e. chose between list and sync depending of number of IDs?
		const bool partialRefresh = !refreshSpecificData.m_Criteria.m_IDs.empty();
		const auto& ids = partialRefresh ? refreshSpecificData.m_Criteria.m_IDs : p_ModuleCriteria.m_IDs;

		pplx::task<vector<BusinessUser>> usersTask;
		if (!ids.empty())
		{
			usersTask = p_pFrame->GetBusinessObjectManager()->GetBusinessUsersRecycleBin(ids, DeletedUserListLicensePropertySet(), taskData);
		}
		else
		{
			usersTask = pplx::task_from_result(vector<BusinessUser>());
		}

		YSafeCreateTaskMutable([partialRefresh, taskData, hwnd = p_pFrame->GetSafeHwnd(), usersTask, subscribedSkusTask, p_Action = p_Command.GetAutomationSetup().m_ActionToExecute, bom = p_pFrame->GetBusinessObjectManager(), p_IsRefresh]() mutable
		{
			O365DataMap<BusinessUser, vector<BusinessLicenseDetail>> licenseDetails;
			auto subscribedSkus = subscribedSkusTask.GetTask().get();
			auto users = usersTask.get();

			for (const auto& user : users)
				licenseDetails[user] = {};

			struct LicenceStuff
			{
				O365DataMap<BusinessUser, vector<BusinessLicenseDetail>> m_LicenseDetails;
				vector<BusinessSubscribedSku> m_SubscribedSkus;
			};

			YDataCallbackMessage<LicenceStuff>::DoPost({ licenseDetails, subscribedSkus }, [hwnd, partialRefresh, taskData, p_Action, p_IsRefresh, isCanceled = taskData.IsCanceled()](const LicenceStuff& p_LicenseStuff)
			{
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameLicensesDeletedUsers*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, isCanceled, taskData, false))
				{
					auto frame = dynamic_cast<FrameLicensesDeletedUsers*>(CWnd::FromHandle(hwnd));
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting Service Plans for %s deleted users (%s Subscribed SKUs)...", Str::getStringFromNumber(p_LicenseStuff.m_LicenseDetails.size()).c_str(), Str::getStringFromNumber(p_LicenseStuff.m_SubscribedSkus.size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());

					{
						CWaitCursor _;
						frame->ShowLicenses(p_LicenseStuff.m_LicenseDetails, p_LicenseStuff.m_SubscribedSkus, !partialRefresh);
					}
					
					if (!p_IsRefresh)
					{
						frame->UpdateContext(taskData, hwnd);
						shouldFinishTask = false;
					}
				}

				if (shouldFinishTask)
				{
					// Because we didn't (and can't) call UpdateContext here.
					ModuleBase::TaskFinished(frame, taskData, p_Action);
				}
			});
		});
	}
}

void ModuleLicense::loadSnapshot(const Command& p_Command)
{
	using LoaderType = std::shared_ptr<GridSnapshot::Loader>;
	if (p_Command.GetCommandInfo().Data().is_type<LoaderType>())
	{
		auto loader = p_Command.GetCommandInfo().Data().get_value<LoaderType>();
		const auto& metadata = loader->GetMetadata();

		if (GridUtil::g_AutoNameLicenses == *metadata.m_GridAutomationName)
		{
			LoadSnapshot<FrameLicenses>(p_Command, [&p_Command](const GridSnapshot::Metadata& p_SnapshotMeta, const ModuleCriteria& p_ModuleCriteria, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
				{
					const bool isMyData = p_SnapshotMeta.m_Flags && GridSnapshot::Flags::MyData == (GridSnapshot::Flags::MyData & *p_SnapshotMeta.m_Flags);
					const bool isHandleSkuCounters = p_SnapshotMeta.m_IsHandleSkuCounters ? *p_SnapshotMeta.m_IsHandleSkuCounters : false;
					const wstring title = isMyData
						? _T("My Licenses and Service Plans")
						: YtriaTranslate::Do(ModuleLicense_showLicenses_1, _YLOC("Licenses and Service Plans")).c_str();
					return new FrameLicenses(isMyData, isHandleSkuCounters, p_LicenseContext, p_SessionIdentifier, title, p_Module, p_HistoryMode, p_PreviousFrame);
				});
		}
		else if (GridUtil::g_AutoNameLicensesSummary == *metadata.m_GridAutomationName)
		{
			LoadSnapshot<FrameLicensesSummary>(p_Command, [&p_Command](const GridSnapshot::Metadata& p_SnapshotMeta, const ModuleCriteria& p_ModuleCriteria, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
				{
					const bool isHandleSkuCounters = p_SnapshotMeta.m_IsHandleSkuCounters ? *p_SnapshotMeta.m_IsHandleSkuCounters : false;
					return new FrameLicensesSummary(isHandleSkuCounters, p_LicenseContext, p_SessionIdentifier, YtriaTranslate::Do(ModuleLicense_showLicenses_2, _YLOC("Tenant Licenses and Service Plans")).c_str(), p_Module, p_HistoryMode, p_PreviousFrame);
				});
		}
	}
}
