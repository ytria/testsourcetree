#include "GridLicensesSummary.h"

#include "AutomationWizardLicensesSummary.h"
#include "BasicGridSetup.h"
#include "FrameLicensesSummary.h"
#include "GridTemplate.h"
#include "GridUtil.h"
#include "O365AdminUtil.h"
#include "SkuAndPlanReference.h"
#include "YCallbackMessage.h"

const wstring GridLicensesSummary::g_IdAssignedLicense = _YTEXT("Lic1");
const wstring GridLicensesSummary::g_IdUnassignedLicense = _YTEXT("Lic0");

BEGIN_MESSAGE_MAP(GridLicensesSummary, O365Grid)
END_MESSAGE_MAP()

GridLicensesSummary::GridLicensesSummary(bool p_IsHandleSkuCounters)
	: m_ColEnabledUnitsInRbacScope(nullptr)
	, m_IsHandleSkuCounters(p_IsHandleSkuCounters)
{
	initWizard<AutomationWizardLicensesSummary>(_YTEXT("Automation\\LicensesSummary"));
}

void GridLicensesSummary::customizeGrid()
{
	static const wstring g_FamilyLicenses = YtriaTranslate::Do(GridLicensesSummary_customizeGrid_1, _YLOC("Licenses")).c_str();
	static const wstring g_FamilyServicePlans = YtriaTranslate::Do(GridLicensesSummary_customizeGrid_2, _YLOC("Service Plans")).c_str();

	{
		BasicGridSetup setup(GridUtil::g_AutoNameLicensesSummary, LicenseUtil::g_codeSapio365licensesSummary);
		setup.Setup(*this, false);
	}

	m_ColLicenseStatus	= AddColumn(_YUID("licenseStatus"), YtriaTranslate::Do(GridLicensesSummary_customizeGrid_3, _YLOC("License Status")).c_str(), g_FamilyLicenses, g_ColumnSize2, { g_ColumnsPresetDefault });
	m_ColSkuLabel		= AddColumn(_YUID("LICENSE"), _T("License"), g_FamilyLicenses, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColSkuPartNumber	= AddColumn(_YUID("subscribedSku.skuPartNumber"), YtriaTranslate::Do(GridLicensesSummary_customizeGrid_4, _YLOC("Sku Part Number")).c_str(), g_FamilyLicenses, g_ColumnSize16);

	setBasicGridSetupHierarchy({ { { m_ColSkuPartNumber, GridBackendUtil::g_AllHierarchyLevels } }
								,{ rttr::type::get<ServicePlanInfo>().get_id() } // FIXME: Do we need a BusinessServicePlanInfo? --> pourquoi non ? sale sinon
								,{ rttr::type::get<ServicePlanInfo>().get_id(), rttr::type::get<BusinessSubscribedSku>().get_id() } });

	m_ColServicePlanFriendlyName				= AddColumn(_YUID("SERVICEPLANFRIENDLYNAME"),						_T("Service Plan Friendly Name"),																							g_FamilyServicePlans,	g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColServicePlanName						= AddColumn(_YUID("servicePlan.servicePlanName"),					YtriaTranslate::Do(GridLicensesSummary_customizeGrid_5, _YLOC("Service Plan Name")).c_str(),								g_FamilyServicePlans,	g_ColumnSize32);
	
	m_ColConsumedUnits							= AddColumnNumber(_YUID("subscribedSku.consumedUnits"),				YtriaTranslate::Do(GridLicensesSummary_customizeGrid_6, _YLOC("Consumed")).c_str(),											g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });

	if (m_IsHandleSkuCounters/*GetGraphCache().IsHandleSkuCounters()*/)
		m_ColEnabledUnitsInRbacScope			= AddColumnNumber(_YUID("enabledInRbacScope"), YtriaTranslate::Do(GridLicenses_customizeGrid_33, _YLOC("RBAC Scoped Enabled")).c_str(), g_FamilyLicenses, g_ColumnSize6, { g_ColumnsPresetDefault });

	m_ColEnabledUnits							= AddColumnNumber(_YUID("subscribedSku.prepaidUnits.enabled"),		YtriaTranslate::Do(GridLicensesSummary_customizeGrid_7, _YLOC("Enabled")).c_str(),											g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColSuspendedUnits							= AddColumnNumber(_YUID("subscribedSku.prepaidUnits.suspended"),	YtriaTranslate::Do(GridLicensesSummary_customizeGrid_8, _YLOC("Suspended")).c_str(),										g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColWarningUnits							= AddColumnNumber(_YUID("subscribedSku.prepaidUnits.warning"),		YtriaTranslate::Do(GridLicensesSummary_customizeGrid_9, _YLOC("Warning")).c_str(),											g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });


	m_ColServicePlanID							= AddColumnCaseSensitive(_YUID("servicePlan.servicePlanId"),	YtriaTranslate::Do(GridLicensesSummary_customizeGrid_10, _YLOC("Service Plan ID")).c_str(),										g_FamilyServicePlans,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSkuId									= AddColumnCaseSensitive(_YUID("subscribedSku.skuID"),			YtriaTranslate::Do(GridLicensesSummary_customizeGrid_11, _YLOC("License Sku ID")).c_str(),										g_FamilyLicenses,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColGlobalServiceProvisionningStatus		= AddColumn(_YUID("servicePlan.provisioningStatus"),			YtriaTranslate::Do(GridLicensesSummary_customizeGrid_12, _YLOC("Service Provisioning Status")).c_str(),					g_FamilyServicePlans,	g_ColumnSize12);
	m_ColGlobalServiceAppliesTo					= AddColumn(_YUID("servicePlan.appliesTo"),						YtriaTranslate::Do(GridLicensesSummary_customizeGrid_13, _YLOC("Service Applies To")).c_str(),							g_FamilyServicePlans,	g_ColumnSize12);
	m_ColCapabilityStatus						= AddColumn(_YUID("subscribedSku.capabilityStatus"),			YtriaTranslate::Do(GridLicensesSummary_customizeGrid_14, _YLOC("License - Capability Status")).c_str(),					g_FamilyLicenses,		g_ColumnSize12);
	m_ColAppliesTo								= AddColumn(_YUID("subscribedSku.appliesTo"),					YtriaTranslate::Do(GridLicensesSummary_customizeGrid_15, _YLOC("License - Applies To")).c_str(),							g_FamilyLicenses,		g_ColumnSize12);
	m_ColSubscribedSkuId						= AddColumnCaseSensitive(_YUID("subscribedSku.id"),				YtriaTranslate::Do(GridLicensesSummary_customizeGrid_16, _YLOC("Subscribed Sku ID")).c_str(),									g_FamilyLicenses,		g_ColumnSize12, { g_ColumnsPresetTech });

	AddColumnForRowPK(m_ColSkuId);
	AddColumnForRowPK(m_ColServicePlanID);
	AddColumnForRowPK(m_ColSubscribedSkuId);

	// Add "has license" icons
	m_HasLicenseIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_HAS_LICENSE));
	m_HasNotLicenseIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_HAS_NOT_LICENSE));
	m_SuccessPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SUCCESS_PLAN));
	m_DisabledPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_DISABLED_PLAN));
	m_NotApplicablePlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_NOT_APPLICABLE_PLAN));
	m_PendingInputPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PENDING_INPUT_PLAN));
	m_PendingActivationPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PENDING_ACTIVATION_PLAN));
	m_PendingProvisioningPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PENDING_PROVISIONING_PLAN));

	m_ProvisioningStatusTextAndIcon = GridLicenses::TextAndIconMap(
	{
		{ _YTEXT("Success"),{ YtriaTranslate::Do(GridLicensesSummary_customizeGrid_17, _YLOC("Success")).c_str(), m_SuccessPlanIconId } },
		{ _YTEXT("Disabled"),{ YtriaTranslate::Do(GridLicensesSummary_customizeGrid_18, _YLOC("Disabled")).c_str(), m_DisabledPlanIconId } },
		{ _YTEXT("PendingInput"),{ YtriaTranslate::Do(GridLicensesSummary_customizeGrid_19, _YLOC("Pending Input")).c_str(), m_PendingInputPlanIconId } },
		{ _YTEXT("PendingActivation"),{ YtriaTranslate::Do(GridLicensesSummary_customizeGrid_20, _YLOC("Pending Activation")).c_str(), m_PendingActivationPlanIconId } },
		{ _YTEXT("PendingProvisioning"),{ YtriaTranslate::Do(GridLicensesSummary_customizeGrid_21, _YLOC("Pending Provisioning")).c_str(), m_PendingProvisioningPlanIconId } },

		// Those are not provisioning status, but using this map is handy.
		{ g_IdAssignedLicense, { YtriaTranslate::Do(GridLicensesSummary_BuildTreeView_1, _YLOC("YES - LICENSE")).c_str(), m_HasLicenseIconId} },
		{ g_IdUnassignedLicense, { YtriaTranslate::Do(GridLicensesSummary_BuildTreeView_2, _YLOC("NO - LICENSE")).c_str(), m_HasNotLicenseIconId } },
	});

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameLicensesSummary*>(GetParentFrame()));
}

wstring GridLicensesSummary::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("SKU"), m_ColSkuPartNumber } }, m_ColServicePlanName);
}

O365Grid::SnapshotCaps GridLicensesSummary::GetSnapshotCapabilities() const
{
	// No restore point
	return { true, false };
}

bool GridLicensesSummary::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	if (p_Field.HasValue())
	{
		if (m_ColLicenseStatus == &p_Column)
		{
			const auto icon = p_Field.GetIcon();

			auto it = std::find_if(m_ProvisioningStatusTextAndIcon.begin(), m_ProvisioningStatusTextAndIcon.end(),
				[icon](const auto& item)
				{
					return icon == item.second.m_Icon;
				});

			ASSERT(m_ProvisioningStatusTextAndIcon.end() != it);
			if (m_ProvisioningStatusTextAndIcon.end() != it)
			{
				p_Value = it->first;
				return true;
			}
		}
	}

	return O365Grid::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridLicensesSummary::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (m_ColLicenseStatus == &p_Column)
	{
		auto it = m_ProvisioningStatusTextAndIcon.find(p_Value);
		ASSERT(m_ProvisioningStatusTextAndIcon.end() != it);
		if (m_ProvisioningStatusTextAndIcon.end() != it)
		{
			p_Row.AddField(it->second.m_Text, m_ColLicenseStatus, it->second.m_Icon);
			return true;
		}
	}

	return O365Grid::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

void GridLicensesSummary::BuildTreeView(const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus)
{
	if (p_BusinessSubscribedSkus.size() == 1 && p_BusinessSubscribedSkus[0].GetError())
	{
		HandlePostUpdateError(p_BusinessSubscribedSkus[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_BusinessSubscribedSkus[0].GetError()->GetFullErrorMessage());
	}
	else
	{
		GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID | GridUpdaterOptions::FULLPURGE /*| GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS | GridUpdaterOptions::REFRESH_PROCESS_DATA /*| GridUpdaterOptions::PROCESS_LOADMORE*/));

		for (const auto& businessSubscribedSku : p_BusinessSubscribedSkus)
		{
			const GridBackendField fSubSkuID(businessSubscribedSku.GetID(), m_ColSubscribedSkuId);
			ASSERT(businessSubscribedSku.GetSkuId().is_initialized());
			const GridBackendField fSkuID(businessSubscribedSku.GetSkuId().is_initialized() ? businessSubscribedSku.GetSkuId().get() : _YTEXT(""), m_ColSkuId);
			GridBackendRow* licenseRow = m_RowIndex.GetRow({ fSubSkuID, fSkuID }, true, true);

			ASSERT(nullptr != licenseRow);
			if (nullptr != licenseRow)
			{
				updater.GetOptions().AddRowWithRefreshedValues(licenseRow);

				SetRowObjectType(licenseRow, businessSubscribedSku, businessSubscribedSku.HasFlag(BusinessObject::CANCELED));
				licenseRow->AddField(businessSubscribedSku.GetSkuPartNumber(), m_ColSkuPartNumber);
				if (businessSubscribedSku.GetSkuId())
				{
					const auto& label = SkuAndPlanReference::GetInstance().GetSkuLabel(*businessSubscribedSku.GetSkuId());
					if (!label.IsEmpty())
						licenseRow->AddField(label, m_ColSkuLabel);
					else
						licenseRow->AddField(businessSubscribedSku.GetSkuPartNumber(), m_ColSkuLabel);
				}

				{
					auto capabilityStatusField = licenseRow->AddField(businessSubscribedSku.GetCapabilityStatus(), m_ColCapabilityStatus);
					if (capabilityStatusField.HasValue() && MFCUtil::StringMatch(capabilityStatusField.GetValueStr(), _YTEXT("enabled")))
						licenseRow->AddField(m_ProvisioningStatusTextAndIcon[g_IdAssignedLicense].m_Text, m_ColLicenseStatus, m_ProvisioningStatusTextAndIcon[g_IdAssignedLicense].m_Icon);
					else
						licenseRow->AddField(m_ProvisioningStatusTextAndIcon[g_IdUnassignedLicense].m_Text, m_ColLicenseStatus, m_ProvisioningStatusTextAndIcon[g_IdUnassignedLicense].m_Icon);
				}

				licenseRow->AddField(businessSubscribedSku.GetAppliesTo(), m_ColAppliesTo);
				licenseRow->AddField(businessSubscribedSku.GetConsumedUnits(), m_ColConsumedUnits);
				if (businessSubscribedSku.GetPrepaidUnits().is_initialized())
				{
					licenseRow->AddField(businessSubscribedSku.GetPrepaidUnits()->m_Enabled, m_ColEnabledUnits);
					licenseRow->AddField(businessSubscribedSku.GetPrepaidUnits()->m_Suspended, m_ColSuspendedUnits);
					licenseRow->AddField(businessSubscribedSku.GetPrepaidUnits()->m_Warning, m_ColWarningUnits);

					if (nullptr != m_ColEnabledUnitsInRbacScope)
						licenseRow->AddField(businessSubscribedSku.GetPrepaidUnits()->m_Accessible, m_ColEnabledUnitsInRbacScope);
				}
				else
				{
					licenseRow->RemoveField(m_ColEnabledUnits);
					licenseRow->RemoveField(m_ColSuspendedUnits);
					licenseRow->RemoveField(m_ColWarningUnits);

					if (m_ColEnabledUnitsInRbacScope)
						licenseRow->RemoveField(m_ColEnabledUnitsInRbacScope);
				}

				for (const auto& servicePlan : businessSubscribedSku.GetServicePlans())
				{
					ASSERT(servicePlan.m_ServicePlanId.is_initialized());
					const GridBackendField fServicePlanID(servicePlan.m_ServicePlanId.is_initialized() ? servicePlan.m_ServicePlanId.get() : _YTEXT(""), m_ColServicePlanID);
					GridBackendRow* planRow = m_RowIndex.GetRow({ fSubSkuID, fSkuID, fServicePlanID }, true, true);
					ASSERT(nullptr != planRow);
					if (nullptr != planRow)
					{
						updater.GetOptions().AddRowWithRefreshedValues(planRow);

						planRow->SetParentRow(licenseRow);
						SetRowObjectType(planRow, ServicePlanInfo(), false, NO_ICON, YtriaTranslate::Do(GridLicensesSummary_BuildTreeView_3, _YLOC("Service Plan")).c_str());

						// Repeat license info on plan row
						{
							const std::set<UINT> columnsToRepeat{ m_ColSkuPartNumber->GetID()
																, m_ColSkuLabel->GetID()
																, m_ColCapabilityStatus->GetID()
																, m_ColAppliesTo->GetID() };
							for (const auto& item : licenseRow->GetFields())
							{
								if (columnsToRepeat.end() != columnsToRepeat.find(item.first))
									planRow->AddField(item.second);
							}
						}

						planRow->AddField(servicePlan.m_ServicePlanName, m_ColServicePlanName);
						if (servicePlan.m_ServicePlanId)
						{
							const auto& label = SkuAndPlanReference::GetInstance().GetPlanLabel(*servicePlan.m_ServicePlanId);
							if (!label.IsEmpty())
								planRow->AddField(label, m_ColServicePlanFriendlyName);
							else
								planRow->AddField(servicePlan.m_ServicePlanName, m_ColServicePlanFriendlyName);
						}
						planRow->AddField(servicePlan.m_ProvisioningStatus, m_ColGlobalServiceProvisionningStatus);

						if (servicePlan.m_ProvisioningStatus.is_initialized())
						{
							ASSERT(m_ProvisioningStatusTextAndIcon.end() != m_ProvisioningStatusTextAndIcon.find(*servicePlan.m_ProvisioningStatus));
							planRow->AddField(m_ProvisioningStatusTextAndIcon[*servicePlan.m_ProvisioningStatus].m_Text
								, m_ColLicenseStatus
								, m_ProvisioningStatusTextAndIcon[*servicePlan.m_ProvisioningStatus].m_Icon);
						}
						else
						{
							planRow->RemoveField(m_ColLicenseStatus);
						}

						planRow->AddField(servicePlan.m_AppliesTo, m_ColGlobalServiceAppliesTo);
					}
				}
			}
		}
	}
}

void GridLicensesSummary::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
	O365Grid::OnCustomPopupMenu(pPopup, nStart);

	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(); // Sep
	}
}

void GridLicensesSummary::InitializeCommands()
{
	O365Grid::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		//CExtCmdItem _cmd;

		//_cmd.m_nCmdID = ID_LICENSESGRID_SHOWUNASSIGNED;
		//_cmd.m_sMenuText = _TLOC("Show Unassigned Licenses");
		//_cmd.m_sToolbarText = _TLOC("Show Unassigned Licenses");
		//_cmd.m_sAccelText = _YTEXT("");
		//g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		//HICON hIcon = MFCUtil::getHICONFromBITMAPID(IDB_LICENSESGRID_SHOWUNASSIGNED_16X16);
		//ASSERT(hIcon);
		//g_CmdManager->CmdSetIcon(profileName, ID_LICENSESGRID_SHOWUNASSIGNED, hIcon, false);
		//setRibbonTooltip(_cmd.m_nCmdID, _TLOC("Show Unassigned Licenses"), _TLOC("Show Unassigned Licenses"), _cmd.m_sAccelText);
	}
}
