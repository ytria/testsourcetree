#pragma once

#include "BaseO365Grid.h"
#include "BusinessSubscribedSku.h"
#include "GridLicenses.h"
#include "GridUpdater.h"

class GridLicensesSummary : public O365Grid
{
public:
	GridLicensesSummary(bool p_IsHandleSkuCounters);

	virtual void customizeGrid() override;
    void BuildTreeView(const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

protected:
	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

private:
    DECLARE_MESSAGE_MAP()

	virtual void InitializeCommands() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart);

private:
	GridBackendColumn* m_ColSkuPartNumber;
	GridBackendColumn* m_ColSkuLabel;
	GridBackendColumn* m_ColServicePlanName;
	GridBackendColumn* m_ColServicePlanFriendlyName;
	GridBackendColumn* m_ColConsumedUnits;
	GridBackendColumn* m_ColEnabledUnits;
	GridBackendColumn* m_ColEnabledUnitsInRbacScope;
	GridBackendColumn* m_ColSuspendedUnits;
	GridBackendColumn* m_ColWarningUnits;
	GridBackendColumn* m_ColServicePlanID;
	GridBackendColumn* m_ColSkuId;
	GridBackendColumn* m_ColGlobalServiceProvisionningStatus;
	GridBackendColumn* m_ColGlobalServiceAppliesTo;
	GridBackendColumn* m_ColCapabilityStatus;
	GridBackendColumn* m_ColAppliesTo;
	GridBackendColumn* m_ColSubscribedSkuId;

	GridBackendColumn* m_ColLicenseStatus;

	int m_HasLicenseIconId;
	int m_HasNotLicenseIconId;
	int m_SuccessPlanIconId;
	int m_DisabledPlanIconId;
	int m_NotApplicablePlanIconId;
	int m_PendingInputPlanIconId;
	int m_PendingActivationPlanIconId;
	int m_PendingProvisioningPlanIconId;

	GridLicenses::TextAndIconMap m_ProvisioningStatusTextAndIcon;

	const bool m_IsHandleSkuCounters;

	static const wstring g_IdAssignedLicense;
	static const wstring g_IdUnassignedLicense;
};

