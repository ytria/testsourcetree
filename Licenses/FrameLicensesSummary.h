#pragma once

#include "GridFrameBase.h"
#include "GridLicensesSummary.h"

class FrameLicensesSummary : public GridFrameBase
{
public:
	FrameLicensesSummary(bool p_IsHandleSkuCounters
		, const LicenseContext& p_LicenseContext
		, const SessionIdentifier& p_SessionIdentifier
		, const PooledString& p_Title
		, ModuleBase& module
		, HistoryMode::Mode p_HistoryMode
		, CFrameWnd* p_PreviousFrame
	);

	DECLARE_DYNAMIC(FrameLicensesSummary)
	virtual ~FrameLicensesSummary() = default;

	void ShowLicenses(const vector<BusinessSubscribedSku>& m_SubscribedSkus);

	virtual O365Grid& GetGrid() override;
	virtual void ApplyAllSpecific();
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction);


// Generated message map functions
protected:
	virtual void createGrid() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

// Members
private:
	GridLicensesSummary m_licencesGrid;
};
