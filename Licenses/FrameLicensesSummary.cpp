#include "Office365Admin.h"

#include "CommandDispatcher.h"
#include "FrameLicensesSummary.h"
#include "LoggerService.h"

// For asynchronous
#include <ppltasks.h>
using namespace concurrency;

IMPLEMENT_DYNAMIC(FrameLicensesSummary, GridFrameBase)

FrameLicensesSummary::FrameLicensesSummary(bool p_IsHandleSkuCounters, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, p_Module, p_HistoryMode, p_PreviousFrame)
	, m_licencesGrid(p_IsHandleSkuCounters)
{

}

void FrameLicensesSummary::ShowLicenses(const vector<BusinessSubscribedSku>& m_SubscribedSkus)
{
	if (CompliesWithDataLoadPolicy())
		m_licencesGrid.BuildTreeView(m_SubscribedSkus);
}

void FrameLicensesSummary::createGrid()
{
	m_licencesGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

// returns false if no frame specific tab is needed.
bool FrameLicensesSummary::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	return GridFrameBase::customizeActionsRibbonTab(tab, GetOrigin() == Origin::User, GetOrigin() == Origin::Group);
}

O365Grid& FrameLicensesSummary::GetGrid()
{
	return m_licencesGrid;
}

void FrameLicensesSummary::ApplyAllSpecific()
{
	/*auto pTemp = std::make_shared<UpdaterGen<BusinessMessage>>();
	pTemp->BuildUpdater(GetGrid());
	pTemp->m_pFrameToUpdate = this;
	CommandManager::GetInstance().Execute(Command(Command::ModuleTarget::Licenses, Command::ModuleTask::UpdateModified, pTemp));*/
}

void FrameLicensesSummary::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	ASSERT(Origin::Tenant == GetModuleCriteria().m_Origin);
	if (Origin::Tenant == GetModuleCriteria().m_Origin)
		info.SetOrigin(GetModuleCriteria().m_Origin);
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Licenses, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

AutomatedApp::AUTOMATIONSTATUS FrameLicensesSummary::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	// TODO:
	ASSERT(FALSE);

	return statusRV;
}
