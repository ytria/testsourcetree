#pragma once

#include "GridFrameBase.h"
#include "GridLicensesDeletedUsers.h"

class FrameLicensesDeletedUsers : public GridFrameBase
{
public:
	FrameLicensesDeletedUsers(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

	DECLARE_DYNAMIC(FrameLicensesDeletedUsers)
	virtual ~FrameLicensesDeletedUsers() = default;

	void ShowLicenses(const O365DataMap<BusinessUser, vector<BusinessLicenseDetail>>& p_LicenseDetails, const vector<BusinessSubscribedSku>& m_SubscribedSkus, bool p_FullPurge);

	virtual O365Grid& GetGrid() override;

	virtual void RefreshSpecific(AutomationAction* p_CurrentAction);
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

	GridLicensesDeletedUsers& GetGridLicenses();
    const GridLicensesDeletedUsers& GetGridLicenses() const;

// Generated message map functions
protected:
	virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

// Members
private:
	GridLicensesDeletedUsers m_LicensesGrid;
};
