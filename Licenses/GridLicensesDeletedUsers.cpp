#include "GridLicensesDeletedUsers.h"

#include "AutomationWizardLicensesDeletedUsers.h"
#include "BasicGridSetup.h"
#include "BusinessUser.h"
#include "BusinessUserDeleted.h"
#include "DlgUserUsageLocationHTML.h"
#include "FrameLicenses.h"
#include "GridTemplate.h"
#include "GridUtil.h"
#include "MsGraphFieldNames.h"
#include "O365AdminUtil.h"
#include "YCallbackMessage.h"
#include "UserLicenseModification.h"
#include "SkuInfo.h"

BEGIN_MESSAGE_MAP(GridLicensesDeletedUsers, O365Grid)
	ON_COMMAND(ID_LICENSESGRID_SHOWUNASSIGNED,				OnShowUnassigned)
	ON_UPDATE_COMMAND_UI(ID_LICENSESGRID_SHOWUNASSIGNED,	OnUpdateShowUnassigned)
	ON_COMMAND(ID_LICENSESGRID_SHOWPLANS,					OnShowPlans)
	ON_UPDATE_COMMAND_UI(ID_LICENSESGRID_SHOWPLANS,			OnUpdateShowPlans)
END_MESSAGE_MAP()

const wstring GridLicensesDeletedUsers::g_ProvisioningStatusDisabled = _YTEXT("Disabled");

GridLicensesDeletedUsers::GridLicensesDeletedUsers()
	: m_ShowNoLicenseRows(false)
	, m_ShowPlanRows(false)
{
	initWizard<AutomationWizardLicensesDeletedUsers>(_YTEXT("Automation\\LicensesDeletedUsers"));
    //EnableGridModifications();
}

void GridLicensesDeletedUsers::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameLicensesDeletedUsers, LicenseUtil::g_codeSapio365licenses,
		{
			{ &m_ColMetaDisplayName, YtriaTranslate::Do(GridLicenses_customizeGrid_1, _YLOC("User Display Name")).c_str(), GridTemplate::g_TypeUser },
			{ &m_ColMetaUserPrincipalName, YtriaTranslate::Do(GridLicenses_customizeGrid_2, _YLOC("Username")).c_str(), GridTemplate::g_TypeUser },
			{ &m_ColMetaId, YtriaTranslate::Do(GridLicenses_customizeGrid_3, _YLOC("User ID")).c_str(), GridTemplate::g_TypeUser }
		});

		setup.AddMetaColumn({ &m_ColumnMetaUserUsageLocation, YtriaTranslate::Do(GridLicenses_customizeGrid_31, _YLOC("Location for License Usage")).c_str(), GridTemplate::g_TypeUser, _YTEXT(O365_USER_USAGELOCATION) });
		setup.Setup(*this, false);
	}

	m_ColumnMetaUserUsageLocation->SetPresetFlags({ g_ColumnsPresetDefault });
	m_ColumnMetaUserUsageLocation->SetWidth(30);

	setBasicGridSetupHierarchy({ { {m_ColMetaUserPrincipalName, GridBackendUtil::g_AllHierarchyLevels } }
								,{ rttr::type::get<ServicePlanInfo>().get_id() } // FIXME: Do we need a BusinessServicePlanInfo? --> pourquoi non ? sale sinon
								,{ rttr::type::get<ServicePlanInfo>().get_id(), rttr::type::get<BusinessSubscribedSku>().get_id(), rttr::type::get<BusinessUserDeleted>().get_id() } });

	static const wstring g_FamilyLicenses		= YtriaTranslate::Do(GridLicenses_customizeGrid_4, _YLOC("Licenses")).c_str();
	m_ColIsAssigned								= AddColumn(_YUID("isAssigned"),									YtriaTranslate::Do(GridLicenses_customizeGrid_5, _YLOC("License Status")).c_str(),										g_FamilyLicenses,		g_ColumnSize2,	{ g_ColumnsPresetDefault });
    m_ColIsAssignedBool                         = AddColumn(_YUID("isAssigned.bool"),                                YtriaTranslate::Do(GridLicenses_customizeGrid_30, _YLOC("Assigned")).c_str(),                                                                                                         g_FamilyLicenses, g_ColumnSize3, { g_ColumnsPresetTech });
    m_ColSkuPartNumber							= AddColumn(_YUID("licenseDetails.skuPartNumber"),					YtriaTranslate::Do(GridLicenses_customizeGrid_6, _YLOC("Sku Part Number")).c_str(),										g_FamilyLicenses,		g_ColumnSize16, { g_ColumnsPresetDefault });
	static const wstring g_FamilyServicePlans	= YtriaTranslate::Do(GridLicenses_customizeGrid_7, _YLOC("Service Plans")).c_str();
	m_ColServicePlanName						= AddColumn(_YUID("licenseDetails.servicePlanName"),				YtriaTranslate::Do(GridLicenses_customizeGrid_8, _YLOC("Service Plan Name")).c_str(),									g_FamilyServicePlans,	g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColConsumedUnits							= AddColumnNumber(_YUID("subscribedSku.consumedUnits"),				YtriaTranslate::Do(GridLicenses_customizeGrid_9, _YLOC("Consumed")).c_str(),												g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColEnabledUnits							= AddColumnNumber(_YUID("subscribedSku.prepaidUnits.enabled"),		YtriaTranslate::Do(GridLicenses_customizeGrid_10, _YLOC("Enabled")).c_str(),												g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColSuspendedUnits							= AddColumnNumber(_YUID("subscribedSku.prepaidUnits.suspended"),	YtriaTranslate::Do(GridLicenses_customizeGrid_11, _YLOC("Suspended")).c_str(),											g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColWarningUnits							= AddColumnNumber(_YUID("subscribedSku.prepaidUnits.warning"),		YtriaTranslate::Do(GridLicenses_customizeGrid_12, _YLOC("Warning")).c_str(),												g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColServiceAssignedDate					= AddColumnDate(_YUID("assignedPlans.AssignedDateTime"),			YtriaTranslate::Do(GridLicenses_customizeGrid_13, _YLOC("Service Assigned Date")).c_str(),								g_FamilyServicePlans,	g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColServiceCapabilityStatus				= AddColumn(_YUID("assignedPlans.capabilityStatus"),				YtriaTranslate::Do(GridLicenses_customizeGrid_14, _YLOC("Service Capabality Status")).c_str(),							g_FamilyServicePlans,	g_ColumnSize12);
	m_ColServiceProvisioningStatus				= AddColumn(_YUID("provisionedPlans.ProvisoningStatus"),			YtriaTranslate::Do(GridLicenses_customizeGrid_15, _YLOC("Service Provisioning Status")).c_str(),							g_FamilyServicePlans,	g_ColumnSize12);
	//m_ColServiceLicenseProvisioningStatus		= AddColumn(_YUID("licenseDetails.provisioningStatus"),				YtriaTranslate::Do(GridLicenses_customizeGrid_16, _YLOC("License Details - Service Provisioning Status")).c_str(),		g_FamilyServicePlans,	g_ColumnSize22);
	//m_ColServiceLicenseProvisioningAppliesTo	= AddColumn(_YUID("licenseDetails.appliesTo"),						YtriaTranslate::Do(GridLicenses_customizeGrid_17, _YLOC("License Details - Service Provisioning Applies To")).c_str(),	g_FamilyServicePlans,	g_ColumnSize12);
	m_ColServicePlanID							= AddColumnCaseSensitive(_YUID("licenseDetails.servicePlanId"),		YtriaTranslate::Do(GridLicenses_customizeGrid_18, _YLOC("Service Plan ID")).c_str(),										g_FamilyServicePlans,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSkuId									= AddColumnCaseSensitive(_YUID("licenseDetails.skuID"),				YtriaTranslate::Do(GridLicenses_customizeGrid_19, _YLOC("License Sku ID")).c_str(),										g_FamilyLicenses,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColGlobalServiceProvisionningStatus		= AddColumn(_YUID("subscribedSku.provisioningStatus"),				YtriaTranslate::Do(GridLicenses_customizeGrid_20, _YLOC("Global Service Provisioning Status")).c_str(),					g_FamilyServicePlans,	g_ColumnSize12);
	m_ColGlobalServiceAppliesTo					= AddColumn(_YUID("subscribedSku.GlobalServiceAppliesTo"),			YtriaTranslate::Do(GridLicenses_customizeGrid_21, _YLOC("Global Service Applies To")).c_str(),							g_FamilyServicePlans,	g_ColumnSize12);
	m_ColCapabilityStatus						= AddColumn(_YUID("subscribedSku.capabilityStatus"),				YtriaTranslate::Do(GridLicenses_customizeGrid_22, _YLOC("Global License - Capability Status")).c_str(),					g_FamilyLicenses,		g_ColumnSize12);
	m_ColAppliesTo								= AddColumn(_YUID("subscribedSku.appliesTo"),						YtriaTranslate::Do(GridLicenses_customizeGrid_23, _YLOC("Global License - Applies To")).c_str(),							g_FamilyLicenses,		g_ColumnSize12);
	m_ColSubscribedSkuId						= AddColumnCaseSensitive(_YUID("subscribedSku.id"),					YtriaTranslate::Do(GridLicenses_customizeGrid_24, _YLOC("Subscribed Sku ID")).c_str(),									g_FamilyLicenses,		g_ColumnSize12, { g_ColumnsPresetTech });
    
	addColumnGraphID();

	AddColumnForRowPK(m_ColMetaId);
	AddColumnForRowPK(m_ColServicePlanID);
	AddColumnForRowPK(m_ColSkuId);
	AddColumnForRowPK(m_ColSubscribedSkuId);

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameLicenses*>(GetParentFrame()));

	//m_ColSubscriptionName = AddColumn(_YUID("subscriptionName"), _TLOC("Subscription Name"), _TLOC("Level 1"));
	//m_ColServicePlan = AddColumn(_YUID("servicePlan"), _TLOC("Service Plan"), _TLOC("Level 2"));

	// Add icons
    
	m_PlanMarkedForActivationId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PLAN_MARKED_FOR_ACTIVATION));
	m_PlanMarkedForDeactivationId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PLAN_MARKED_FOR_DEACTIVATION));
	m_SkuMarkedForActivationId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SKU_MARKED_FOR_ACTIVATION));
	m_SkuMarkedForDeactivationId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SKU_MARKED_FOR_DEACTIVATION));
	m_HasLicenseIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_HAS_LICENSE));
	m_HasNotLicenseIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_HAS_NOT_LICENSE));
	m_SuccessPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SUCCESS_PLAN));
	m_DisabledPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_DISABLED_PLAN));
	m_NotApplicablePlanIconId =AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_NOT_APPLICABLE_PLAN));
	m_PendingInputPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PENDING_INPUT_PLAN));
	m_PendingActivationPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PENDING_ACTIVATION_PLAN));
	m_PendingProvisioningPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PENDING_PROVISIONING_PLAN));

	m_ProvisioningStatusTextAndIcon = TextAndIconMap(
	{
		{ _YTEXT("Success"),{ YtriaTranslate::Do(GridLicenses_customizeGrid_25, _YLOC("Success")).c_str(), m_SuccessPlanIconId } },
		{ g_ProvisioningStatusDisabled,{ YtriaTranslate::Do(GridLicenses_customizeGrid_26, _YLOC("Disabled")).c_str(), m_DisabledPlanIconId } },
		{ _YTEXT("PendingInput"),{ YtriaTranslate::Do(GridLicenses_customizeGrid_27, _YLOC("Pending Input")).c_str(), m_PendingInputPlanIconId } },
		{ _YTEXT("PendingActivation"),{ YtriaTranslate::Do(GridLicenses_customizeGrid_28, _YLOC("Pending Activation")).c_str(), m_PendingActivationPlanIconId } },
		{ _YTEXT("PendingProvisioning"),{ YtriaTranslate::Do(GridLicenses_customizeGrid_29, _YLOC("Pending Provisioning")).c_str(), m_PendingProvisioningPlanIconId } },
	});
}

wstring GridLicensesDeletedUsers::GetName(const GridBackendRow* p_Row) const
{
	ASSERT(nullptr != p_Row && nullptr != m_ColServicePlanName);
	if (nullptr == p_Row || nullptr == m_ColServicePlanName)
		return makeRowName(p_Row, m_ColServicePlanName);// plouf
	else
		return makeRowName(p_Row, { {_T("Owner"), m_ColMetaDisplayName } },
			p_Row->HasField(m_ColServicePlanName->GetID()) ?
			_YTEXTFORMAT(L"SKU %s PLAN %s", p_Row->GetField(m_ColSkuPartNumber).ToString().c_str(),
											p_Row->GetField(m_ColServicePlanName).ToString().c_str())
			:
			_YTEXTFORMAT(L"SKU %s", p_Row->GetField(m_ColMetaDisplayName).ToString().c_str(),
									p_Row->GetField(m_ColSkuPartNumber).ToString().c_str()));
}

ModuleCriteria GridLicensesDeletedUsers::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria;

    GridFrameBase* frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
    ASSERT(nullptr != frame);
    if (nullptr != frame)
    {
        criteria = frame->GetModuleCriteria();

        criteria.m_IDs.clear();
        for (auto row : p_Rows)
            criteria.m_IDs.insert(row->GetField(m_ColMetaId).GetValueStr());
    }

    return criteria;
}

void GridLicensesDeletedUsers::BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessLicenseDetail>>& p_LicensesData, const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus, bool p_FullPurge)
{
	GridUpdater updater(*this, GridUpdaterOptions(
        GridUpdaterOptions::UPDATE_GRID
        | (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
        | GridUpdaterOptions::REFRESH_MODIFICATION_STATES
        | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
        | GridUpdaterOptions::REFRESH_PROCESS_DATA
    ));
    for (const auto& keyVal : p_LicensesData)
    {
		const BusinessUser& user = keyVal.first;
		ASSERT(keyVal.second.empty());

		const GridBackendField fMetaID(user.GetID(), m_ColMetaId);
        vector<GridBackendField> userRowPk{ fMetaID };

		GridBackendRow* parentRow = m_RowIndex.GetRow(userRowPk, true, true);
    
		ASSERT(nullptr != parentRow);
        if (nullptr != parentRow)
        {
			updater.GetOptions().AddRowWithRefreshedValues(parentRow);
            updater.GetOptions().AddPartialPurgeParentRow(parentRow);

			// Remove assert and uncomment the line below if enabling GridModifications
			ASSERT(!IsGridModificationsEnabled());
			//updater.AddUpdatedRowPk(userRowPk);

			parentRow->SetHierarchyParentToHide(true);
			parentRow->AddField(user.GetID(), GetColId());
			parentRow->AddField(user.GetDisplayName(), m_ColMetaDisplayName);
			parentRow->AddField(user.GetUserPrincipalName(), m_ColMetaUserPrincipalName);
			parentRow->AddField(user.GetUsageLocation(), m_ColumnMetaUserUsageLocation);
			GridUtil::SetUserRowObjectType(*this, parentRow, user);

			FillRows(parentRow, user, p_BusinessSubscribedSkus, updater);
        }
    }

    m_ConsumedSkus.clear();
    // Second pass to update Consumed/Enabled/Suspended/Warning columns
    for (auto skuRow : GetBackendRows())
    {
        if (GridUtil::isBusinessType<BusinessSubscribedSku>(skuRow))
        {
            auto skuId = skuRow->GetField(m_ColSkuId).GetValueStr();
            auto subscribedSku = std::find_if(p_BusinessSubscribedSkus.begin(), p_BusinessSubscribedSkus.end(), [colSkuId = m_ColSkuId, skuId](const BusinessSubscribedSku& p_Sku) {
                return p_Sku.GetSkuId() != boost::none && *p_Sku.GetSkuId() == skuId;
            });

            if (subscribedSku->GetConsumedUnits() != boost::none)
                m_ConsumedSkus[skuId] = *subscribedSku->GetConsumedUnits();

            ASSERT(subscribedSku != p_BusinessSubscribedSkus.end());
            if (subscribedSku != p_BusinessSubscribedSkus.end())
            {
                skuRow->AddField(subscribedSku->GetConsumedUnits(), m_ColConsumedUnits);
                if (subscribedSku->GetPrepaidUnits().is_initialized())
                {
                    skuRow->AddField(subscribedSku->GetPrepaidUnits()->m_Enabled, m_ColEnabledUnits);
                    skuRow->AddField(subscribedSku->GetPrepaidUnits()->m_Suspended, m_ColSuspendedUnits);
                    skuRow->AddField(subscribedSku->GetPrepaidUnits()->m_Warning, m_ColWarningUnits);
                }
                else
                {
                    skuRow->RemoveField(m_ColEnabledUnits);
                    skuRow->RemoveField(m_ColSuspendedUnits);
                    skuRow->RemoveField(m_ColWarningUnits);
                }
            }
        }
    }
}

void GridLicensesDeletedUsers::ToggleShowNoLicenseRows()
{
	m_ShowNoLicenseRows = !m_ShowNoLicenseRows;
	refreshCustomHiddenRows(true);
}

void GridLicensesDeletedUsers::refreshCustomHiddenRows(bool p_UpdateGrid)
{
	const vector < GridBackendRow* >& rows = GetBackendRows();

	for (auto pRow : rows)
	{
		ASSERT(pRow != nullptr);
		if (pRow != nullptr && !pRow->IsGroupRow())
		{
			bool techHidden = false;
			if (!m_ShowPlanRows && GridUtil::isBusinessType<ServicePlanInfo>(pRow))
				techHidden = true;

			if (!techHidden && !m_ShowNoLicenseRows)
			{
				GridBackendField& field = pRow->GetField(m_ColIsAssigned);
				if (field.GetIcon() == m_HasNotLicenseIconId || field.GetIcon() == m_NotApplicablePlanIconId/* ||
					field.GetIcon() == m_SkuMarkedForDeactivationId || field.GetIcon() == m_PlanMarkedForDeactivationId*/)
					techHidden = true;
			}

			pRow->SetTechHidden(techHidden);
		}
	}

	if (p_UpdateGrid)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

bool GridLicensesDeletedUsers::IsShowNoLicenseRows() const
{
	return m_ShowNoLicenseRows;
}

void GridLicensesDeletedUsers::ToggleShowPlanRows()
{
	m_ShowPlanRows = !m_ShowPlanRows;
	refreshCustomHiddenRows(true);
}

bool GridLicensesDeletedUsers::IsShowPlanRows() const
{
	return m_ShowPlanRows;
}

int32_t GridLicensesDeletedUsers::GetNbConsumedBySku(PooledString p_SkuId) const
{
    auto itt = m_ConsumedSkus.find(p_SkuId);
    if (itt != m_ConsumedSkus.end())
        return itt->second;
    
    ASSERT(false);
    return -1;
}

GridBackendColumn* GridLicensesDeletedUsers::GetColMetaId() const
{
    return m_ColMetaId;
}

void GridLicensesDeletedUsers::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
	pPopup->ItemInsert(ID_LICENSESGRID_SHOWPLANS);
	pPopup->ItemInsert(ID_LICENSESGRID_SHOWUNASSIGNED);
	pPopup->ItemInsert();

	O365Grid::OnCustomPopupMenu(pPopup, nStart);
}

void GridLicensesDeletedUsers::FillRows(GridBackendRow* p_ParentRow, const BusinessUser& p_User, const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus, GridUpdater& p_Updater)
{
	if (p_BusinessSubscribedSkus.size() == 1 && p_BusinessSubscribedSkus[0].GetError())
	{
		p_Updater.OnLoadingError(p_ParentRow, *p_BusinessSubscribedSkus[0].GetError());
	}
	else
	{
		const GridBackendField fMetaID(p_User.GetID(), m_ColMetaId);
		for (const auto& businessSubscribedSku : p_BusinessSubscribedSkus)
		{
			const GridBackendField fSubSkuID(businessSubscribedSku.GetID(), m_ColSubscribedSkuId);
			ASSERT(businessSubscribedSku.GetSkuId().is_initialized());
			const GridBackendField fSkuID(businessSubscribedSku.GetSkuId().is_initialized() ? businessSubscribedSku.GetSkuId().get() : _YTEXT(""), m_ColSkuId);

			const bool licenseRowExisted = nullptr != m_RowIndex.GetExistingRow({ fMetaID, fSubSkuID, fSkuID });
			GridBackendRow* licenseRow = m_RowIndex.GetRow({ fMetaID, fSubSkuID, fSkuID }, true, true);

			ASSERT(nullptr != licenseRow);
			if (nullptr != licenseRow)
			{
				p_Updater.GetOptions().AddRowWithRefreshedValues(licenseRow);

				licenseRow->SetParentRow(p_ParentRow);
				SetRowObjectType(licenseRow, businessSubscribedSku, businessSubscribedSku.HasFlag(BusinessObject::CANCELED));

				// User meta data
				licenseRow->AddField(p_User.GetDisplayName(), m_ColMetaDisplayName);
				licenseRow->AddField(p_User.GetUserPrincipalName(), m_ColMetaUserPrincipalName);
				licenseRow->AddField(p_User.GetUsageLocation(), m_ColumnMetaUserUsageLocation);

				licenseRow->AddField(businessSubscribedSku.GetSkuPartNumber(), m_ColSkuPartNumber);
				licenseRow->AddField(businessSubscribedSku.GetCapabilityStatus(), m_ColCapabilityStatus);
				licenseRow->AddField(businessSubscribedSku.GetAppliesTo(), m_ColAppliesTo);


				boost::YOpt<BusinessAssignedLicense> matchingAssignedLicense;
				ASSERT(p_User.GetAssignedLicenses());
				if (p_User.GetAssignedLicenses())
				{
					for (const auto& assignedLicense : *p_User.GetAssignedLicenses())
					{
						if (assignedLicense.GetSkuId() == businessSubscribedSku.GetSkuId())
						{
							matchingAssignedLicense.emplace(assignedLicense);
							break;
						}
					}
				}

				bool shouldCollapseLicenceRow = false;
				if (matchingAssignedLicense)
				{
					licenseRow->AddField(YtriaTranslate::Do(GridLicenses_FillRows_1, _YLOC("YES - LICENSE")).c_str(), m_ColIsAssigned, m_HasLicenseIconId);
					licenseRow->AddField(true, m_ColIsAssignedBool);
					licenseRow->SetTechHidden(false);
				}
				else
				{
					licenseRow->AddField(YtriaTranslate::Do(GridLicenses_FillRows_2, _YLOC("NO - LICENSE")).c_str(), m_ColIsAssigned, m_HasNotLicenseIconId);
					licenseRow->AddField(false, m_ColIsAssignedBool);
					licenseRow->SetTechHidden(!m_ShowNoLicenseRows);
					shouldCollapseLicenceRow = true;
				}

				for (const auto& servicePlan : businessSubscribedSku.GetServicePlans())
				{
					ASSERT(servicePlan.m_ServicePlanId.is_initialized());
					const GridBackendField fServicePlanID(servicePlan.m_ServicePlanId.is_initialized() ? servicePlan.m_ServicePlanId.get() : _YTEXT(""), m_ColServicePlanID);
					GridBackendRow* planRow = m_RowIndex.GetRow({ fMetaID, fSubSkuID, fSkuID, fServicePlanID }, true, true);
					ASSERT(nullptr != planRow);
					if (nullptr != planRow)
					{
						if (matchingAssignedLicense)
						{
							if (matchingAssignedLicense->GetDisabledPlans() && matchingAssignedLicense->GetDisabledPlans()->end() != std::find(matchingAssignedLicense->GetDisabledPlans()->begin(), matchingAssignedLicense->GetDisabledPlans()->end(), servicePlan.m_ServicePlanId))
							{
								planRow->AddField(YtriaTranslate::Do(GridLicenses_FillRows_5, _YLOC("Disabled")).c_str(), m_ColServiceCapabilityStatus);
								planRow->AddField(false, m_ColIsAssignedBool);
							}
							else
							{
								planRow->RemoveField(m_ColServiceCapabilityStatus);
								planRow->AddField(true, m_ColIsAssignedBool);
							}
						}

						planRow->SetTechHidden(!m_ShowPlanRows);

						p_Updater.GetOptions().AddRowWithRefreshedValues(planRow);

						planRow->SetParentRow(licenseRow);
						if (!licenseRowExisted)
							licenseRow->SetCollapsed(shouldCollapseLicenceRow);
						SetRowObjectType(planRow, ServicePlanInfo(), false, NO_ICON, YtriaTranslate::Do(GridLicenses_FillRows_3, _YLOC("Service Plan")).c_str());// sale de dirty

																																								 // Repeat license info on plan row
						{
							const std::set<UINT> columnsToRepeat{ m_ColMetaDisplayName->GetID()
								, m_ColMetaUserPrincipalName->GetID()
								, m_ColumnMetaUserUsageLocation->GetID()
								, m_ColSkuPartNumber->GetID()
								, m_ColCapabilityStatus->GetID()
								, m_ColAppliesTo->GetID()
								, GetColId()->GetID() };
							for (const auto& item : licenseRow->GetFields())
							{
								if (columnsToRepeat.end() != columnsToRepeat.find(item.first))
									planRow->AddField(item.second);
							}
						}

						if (shouldCollapseLicenceRow)
						{
							planRow->AddField(YtriaTranslate::Do(GridLicenses_FillRows_4, _YLOC("N/A - PLAN")).c_str(), m_ColIsAssigned, m_NotApplicablePlanIconId);
							planRow->SetTechHidden(!m_ShowNoLicenseRows);
							planRow->AddField(false, m_ColIsAssignedBool);
						}

						planRow->AddField(servicePlan.m_ServicePlanName, m_ColServicePlanName);
						//planRow->AddField(getSubscriptionName(businessSubscribedSku.GetSkuPartNumber(), servicePlan.m_ServicePlanName), m_ColSubscriptionName);
						//planRow->AddField(getServicePlan(businessSubscribedSku.GetSkuPartNumber(), servicePlan.m_ServicePlanName), m_ColServicePlan);
						planRow->AddField(servicePlan.m_ProvisioningStatus, m_ColGlobalServiceProvisionningStatus);
						planRow->AddField(servicePlan.m_AppliesTo, m_ColGlobalServiceAppliesTo);

						planRow->RemoveField(m_ColIsAssigned);
						//planRow->RemoveField(m_ColServiceLicenseProvisioningStatus);
						//planRow->RemoveField(m_ColServiceLicenseProvisioningAppliesTo);

						bool found = false;
						ASSERT(p_User.GetAssignedPlans());
						if (p_User.GetAssignedPlans())
						{
							for (const auto& assignedPlan : *p_User.GetAssignedPlans())
							{
								if (assignedPlan.m_ServicePlanId == servicePlan.m_ServicePlanId)
								{
									planRow->AddField(assignedPlan.m_CapabilityStatus, m_ColServiceCapabilityStatus);

									YTimeDate timeDate;
									if (assignedPlan.m_AssignedDateTime.is_initialized()
										&& TimeUtil::GetInstance().ConvertTextToDate(assignedPlan.m_AssignedDateTime.get().c_str(), timeDate))
										planRow->AddField(timeDate, m_ColServiceAssignedDate);
									else
										planRow->RemoveField(m_ColServiceAssignedDate);

									ASSERT(p_User.GetProvisionedPlans());
									if (p_User.GetProvisionedPlans())
									{
										for (const auto& provisionedPlan : *p_User.GetProvisionedPlans())
										{
											if (provisionedPlan.m_Service == assignedPlan.m_Service
												&& provisionedPlan.m_CapabilityStatus == assignedPlan.m_CapabilityStatus)
											{
												planRow->AddField(provisionedPlan.m_ProvisioningStatus, m_ColServiceProvisioningStatus);
												found = true;
												break;
											}
										}
									}

									if (!found)
										planRow->RemoveField(m_ColServiceProvisioningStatus);

									found = true;
									break;
								}
							}
						}

						if (!found)
						{
							planRow->RemoveField(m_ColServiceAssignedDate);
							planRow->RemoveField(m_ColServiceProvisioningStatus);
						}
					}
				}
			}
		}
	}
}

const PooledString& GridLicensesDeletedUsers::getServicePlan(const boost::YOpt<PooledString>& p_SkuPartNumber, const boost::YOpt<PooledString>& m_ServicePlanName)
{
	static const PooledString ret = L"";
	return ret;
}

const PooledString& GridLicensesDeletedUsers::getSubscriptionName(const boost::YOpt<PooledString>& p_SkuPartNumber, const boost::YOpt<PooledString>& m_ServicePlanName)
{
	static const PooledString ret = L"";
	return ret;
}

bool GridLicensesDeletedUsers::arePlansSetDifferent(const std::set<PlanInfo>& p_Plans1, const std::set<PlanInfo>& p_Plans2) const
{
    bool different = false;
    for (const auto& plan1 : p_Plans1)
    {
        auto ittPlan2 = p_Plans2.find(plan1);
        ASSERT(ittPlan2 != p_Plans2.end());
        if (ittPlan2 != p_Plans2.end())
        {
            if (plan1.Enabled != ittPlan2->Enabled)
            {
                different = true;
                break;
            }
        }
    }
    return different;
}

vector<GridBackendField> GridLicensesDeletedUsers::getUserPkFromSkuPk(const vector<GridBackendField>& p_SkuPk)
{
    GridBackendRow* userRow = nullptr;
    auto skuRow = GetRowIndex().GetExistingRow(p_SkuPk);
    ASSERT(nullptr != skuRow);
    if (nullptr != skuRow)
    {
        userRow = skuRow->GetTopAncestorOrThis();
        ASSERT(nullptr != userRow);
    }

    vector<GridBackendField> userPk;
    GetRowPK(userRow, userPk);

    return userPk;
}

void GridLicensesDeletedUsers::OnShowUnassigned()
{
	ToggleShowNoLicenseRows();
}

void GridLicensesDeletedUsers::OnUpdateShowUnassigned(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(IsShowNoLicenseRows() ? TRUE : FALSE);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridLicensesDeletedUsers::OnShowPlans()
{
	ToggleShowPlanRows();
}

void GridLicensesDeletedUsers::OnUpdateShowPlans(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(IsShowPlanRows() ? TRUE : FALSE);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridLicensesDeletedUsers::InitializeCommands()
{
	O365Grid::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_LICENSESGRID_SHOWUNASSIGNED;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridLicenses_InitializeCommands_1, _YLOC("Show Unassigned Licenses")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridLicenses_InitializeCommands_2, _YLOC("Show Unassigned Licenses")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LICENSESGRID_SHOWUNASSIGNED_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_LICENSESGRID_SHOWUNASSIGNED, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridLicenses_InitializeCommands_3, _YLOC("Show Unassigned Licenses")).c_str(), YtriaTranslate::Do(GridLicenses_InitializeCommands_4, _YLOC("Display information about unassigned licenses (hidden by default).")).c_str(), _cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_LICENSESGRID_SHOWPLANS;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridLicenses_InitializeCommands_13, _YLOC("Show Plans")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridLicenses_InitializeCommands_14, _YLOC("Show Plans")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LICENSESGRID_SHOWPLANS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_LICENSESGRID_SHOWPLANS, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridLicenses_InitializeCommands_15, _YLOC("Show Plans")).c_str(), YtriaTranslate::Do(GridLicenses_InitializeCommands_16, _YLOC("Display information about Plans (hidden by default).")).c_str(), _cmd.m_sAccelText);
	}
}

wstring GridLicensesDeletedUsers::GetDisplayName(GridBackendRow* p_Row) const
{
	wstring displayName;

	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		const auto& field = p_Row->GetField(m_ColMetaDisplayName);
		if (field.HasValue())
			displayName = field.GetValueStr();
	}

	return displayName;
}