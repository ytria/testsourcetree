#include "GridLicenses.h"

#include "AutomationWizardLicenses.h"
#include "BasicGridSetup.h"
#include "BusinessUser.h"
#include "DlgEditLicenses.h"
#include "DlgUserUsageLocationHTML.h"
#include "FrameLicenses.h"
#include "GridTemplate.h"
#include "GridUtil.h"
#include "MsGraphFieldNames.h"
#include "O365AdminUtil.h"
#include "RunOnScopeEnd.h"
#include "SkuInfo.h"
#include "SkuAndPlanReference.h"
#include "UserLicenseModification.h"
#include "YCallbackMessage.h"

BEGIN_MESSAGE_MAP(GridLicenses, O365Grid)
	ON_COMMAND(ID_LICENSESGRID_SHOWUNASSIGNED,				OnShowUnassigned)
	ON_UPDATE_COMMAND_UI(ID_LICENSESGRID_SHOWUNASSIGNED,	OnUpdateShowUnassigned)
    ON_COMMAND(ID_LICENSESGRID_EDITLICENSES,                OnEditLicenses)
	ON_UPDATE_COMMAND_UI(ID_LICENSESGRID_EDITLICENSES,		OnUpdateEditLicenses)
	ON_COMMAND(ID_LICENSESGRID_EDITUSAGELOCATION,			OnEditUsageLocation)
	ON_UPDATE_COMMAND_UI(ID_LICENSESGRID_EDITUSAGELOCATION, OnUpdateEditUsageLocation)
	ON_COMMAND(ID_LICENSESGRID_SHOWPLANS,					OnShowPlans)
	ON_UPDATE_COMMAND_UI(ID_LICENSESGRID_SHOWPLANS,			OnUpdateShowPlans)
END_MESSAGE_MAP()

const wstring GridLicenses::g_ProvisioningStatusDisabled = _YTEXT("Disabled");
const wstring GridLicenses::g_IdAssignedLicense = _YTEXT("Lic1");
const wstring GridLicenses::g_IdUnassignedLicense = _YTEXT("Lic0");
const wstring GridLicenses::g_IdNotApplicablePlan = _YTEXT("PlanNA");
wstring GridLicenses::g_AssignedByGroup;

GridLicenses::GridLicenses(bool p_IsMyData, bool p_IsHandleSkuCounters)
	: m_ShowNoLicenseRows(false)
	, m_ShowPlanRows(false)
	, m_ColEnabledUnitsInRbacScope(nullptr)
	, m_ColConsumedUnitsInRbacScope(nullptr)
	, m_SkuError(false)
	, m_HasLoadMoreAdditionalInfo(false)
	, m_ColIsAssignedByGroup(nullptr)
	, m_ColAssignedByGroupId(nullptr)
	, m_ColAssignedByGroupDisplayName(nullptr)
	, m_ColAssignedByGroupMail(nullptr)
	, m_ColAssignementState(nullptr)
	, m_ColAssignementStateError(nullptr)
	, m_IsMyData(p_IsMyData)
	, m_IsHandleSkuCounters(p_IsHandleSkuCounters)
{
	initWizard<AutomationWizardLicenses>(_YTEXT("Automation\\Licenses"));
    EnableGridModifications();

	if (g_AssignedByGroup.empty())
		g_AssignedByGroup = _T("Assigned by group");

	if (IsMyData())
		m_MyDataMeHandler.emplace();
}

void GridLicenses::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameLicenses, LicenseUtil::g_codeSapio365licenses,
		{
			{ &m_Template.m_ColumnDisplayName, YtriaTranslate::Do(GridLicenses_customizeGrid_1, _YLOC("User Display Name")).c_str(), GridTemplate::g_TypeUser },
			{ &m_Template.m_ColumnUserPrincipalName, YtriaTranslate::Do(GridLicenses_customizeGrid_2, _YLOC("Username")).c_str(), GridTemplate::g_TypeUser },
			{ &m_Template.m_ColumnID, YtriaTranslate::Do(GridLicenses_customizeGrid_3, _YLOC("User ID")).c_str(), GridTemplate::g_TypeUser }
		});

		setup.AddMetaColumn({ &m_Template.m_ColumnUsageLocation, YtriaTranslate::Do(GridLicenses_customizeGrid_31, _YLOC("Location for License Usage")).c_str(), GridTemplate::g_TypeUser, _YUID(O365_USER_USAGELOCATION) });
		setup.Setup(*this, true);
		m_ColumnLastRequestDateTime = setup.GetColumnLastRequestDateTime();
	}


	if (IsMyData())
		m_Template.m_ColumnDisplayName->RemovePresetFlags(g_ColumnsPresetDefault);

	GetColMetaUsageLocation()->SetPresetFlags({ g_ColumnsPresetDefault });
	GetColMetaUsageLocation()->SetWidth(30);

	m_MetaColumns.push_back(GetColMetaId());
	m_MetaColumns.push_back(GetColMetaDisplayName());
	m_MetaColumns.push_back(GetColMetaUserPrincipalName());
	m_MetaColumns.push_back(GetColMetaUsageLocation());

	setBasicGridSetupHierarchy({ { { GetColMetaUserPrincipalName(), GridBackendUtil::g_AllHierarchyLevels } }
								,{ rttr::type::get<ServicePlanInfo>().get_id() } // FIXME: Do we need a BusinessServicePlanInfo? --> pourquoi non ? sale sinon
								,{ rttr::type::get<ServicePlanInfo>().get_id(), rttr::type::get<BusinessSubscribedSku>().get_id(), rttr::type::get<BusinessUser>().get_id() } });

	// Default hierarchy filter is the lowest (ServicePlanInfo), but by default those objects are hidden.
	// So set the next one as default.
	AddHierarchyObjectTypeToFilterOn(GetObjectTypeIndex(BusinessSubscribedSku()), false);
	RemoveHierarchyObjectTypeToFilterOn(GetObjectTypeIndex(ServicePlanInfo()), false);

	/*Add additional meta columns*/
	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		for (const auto& c : *metaDataColumnInfos)
		{
			auto& column = m_Template.GetColumnForProperty(c.GetPropertyName());

			// This is the only Users-derived grid that already has Usage Location column
			ASSERT(c.GetPropertyName() == _YUID(O365_USER_USAGELOCATION) || nullptr == column);
			if (nullptr == column)
			{
				auto col = c.AddTo(*this, GridTemplate::g_TypeUser);
				column = col;
				m_MetaColumns.push_back(col);
				m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
			}
		}
	}

	static const wstring g_FamilyLicenses		= YtriaTranslate::Do(GridLicenses_customizeGrid_4, _YLOC("Licenses")).c_str();
	m_ColIsAssigned								= AddColumn(_YUID("isAssigned"),									YtriaTranslate::Do(GridLicenses_customizeGrid_5, _YLOC("License Status")).c_str(),										g_FamilyLicenses, g_ColumnSize2,	{ g_ColumnsPresetDefault });
    m_ColIsAssignedBool                         = AddColumnCheckBox(_YUID("isAssigned.bool"),						YtriaTranslate::Do(GridLicenses_customizeGrid_30, _YLOC("Assigned")).c_str(),											g_FamilyLicenses, g_ColumnSize3,	{ g_ColumnsPresetTech });
	m_ColSkuLabel								= AddColumn(_YUID("LICENSE"),										_T("License"),																											g_FamilyLicenses, g_ColumnSize32,	{ g_ColumnsPresetDefault });
    m_ColSkuPartNumber							= AddColumn(_YUID("licenseDetails.skuPartNumber"),					YtriaTranslate::Do(GridLicenses_customizeGrid_6, _YLOC("Sku Part Number")).c_str(),										g_FamilyLicenses, g_ColumnSize16);

	m_ColAssignementState						= AddColumn(_YUID("licenseAssignmentStates.state"), _T("State - License assignment"), g_FamilyLicenses, CacheGrid::g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColAssignementStateError					= AddColumn(_YUID("licenseAssignmentStates.error"), _T("Error - License assignment"), g_FamilyLicenses, CacheGrid::g_ColumnSize12);
	m_ColIsAssignedByGroup						= AddColumnIcon(_YUID("isAssignedByGroup"), _T("Assigned by group"), g_FamilyLicenses);
	m_ColIsAssignedByGroup->SetPresetFlags({ g_ColumnsPresetDefault });
	m_ColAssignedByGroupDisplayName				= AddColumn(_YUID("assignedByGroupDisplayName"), _T("Group Name - Assigned by"), g_FamilyLicenses, CacheGrid::g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAssignedByGroupMail					= AddColumn(_YUID("assignedByGroupEMail"), _T("Group Email - Assigned by"), g_FamilyLicenses, CacheGrid::g_ColumnSize12);
	m_ColAssignedByGroupId						= AddColumnCaseSensitive(_YUID("licenseAssignmentStates.assignedByGroup"), _T("Group ID - Assigned by"), g_FamilyLicenses, CacheGrid::g_ColumnSize12, { g_ColumnsPresetTech });

	static const wstring g_FamilyServicePlans	= YtriaTranslate::Do(GridLicenses_customizeGrid_7, _YLOC("Service Plans")).c_str();
	m_ColServicePlanFriendlyName				= AddColumn(_YUID("SERVICEPLANFRIENDLYNAME"), _T("Service Plan Friendly Name"), g_FamilyServicePlans, g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColServicePlanName						= AddColumn(_YUID("licenseDetails.servicePlanName"),				YtriaTranslate::Do(GridLicenses_customizeGrid_8, _YLOC("Service Plan Name")).c_str(),									g_FamilyServicePlans,	g_ColumnSize32);

	InitRoleDelegation();

	if (m_IsHandleSkuCounters/*GetGraphCache().IsHandleSkuCounters()*/)
		m_ColConsumedUnitsInRbacScope			= AddColumnNumber(_YUID("consumedInRbacScope"), YtriaTranslate::Do(GridLicenses_customizeGrid_32, _YLOC("RBAC Scope Consumed")).c_str(), g_FamilyLicenses, g_ColumnSize6, { g_ColumnsPresetDefault });
	m_ColConsumedUnits							= AddColumnNumber(_YUID("subscribedSku.consumedUnits"),				YtriaTranslate::Do(GridLicenses_customizeGrid_9, _YLOC("Consumed")).c_str(),												g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });

	if (m_IsHandleSkuCounters/*GetGraphCache().IsHandleSkuCounters()*/)
		m_ColEnabledUnitsInRbacScope			= AddColumnNumber(_YUID("enabledInRbacScope"),							YtriaTranslate::Do(GridLicenses_customizeGrid_33, _YLOC("RBAC Scoped Enabled")).c_str(), g_FamilyLicenses, g_ColumnSize6, { g_ColumnsPresetDefault });
	m_ColEnabledUnits							= AddColumnNumber(_YUID("subscribedSku.prepaidUnits.enabled"),		YtriaTranslate::Do(GridLicenses_customizeGrid_10, _YLOC("Enabled")).c_str(),												g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });

	m_ColSuspendedUnits							= AddColumnNumber(_YUID("subscribedSku.prepaidUnits.suspended"),	YtriaTranslate::Do(GridLicenses_customizeGrid_11, _YLOC("Suspended")).c_str(),											g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColWarningUnits							= AddColumnNumber(_YUID("subscribedSku.prepaidUnits.warning"),		YtriaTranslate::Do(GridLicenses_customizeGrid_12, _YLOC("Warning")).c_str(),												g_FamilyLicenses,		g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColServiceAssignedDate					= AddColumnDate(_YUID("assignedPlans.AssignedDateTime"),			YtriaTranslate::Do(GridLicenses_customizeGrid_13, _YLOC("Service Assigned Date")).c_str(),								g_FamilyServicePlans,	g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColServiceCapabilityStatus				= AddColumn(_YUID("assignedPlans.capabilityStatus"),				YtriaTranslate::Do(GridLicenses_customizeGrid_14, _YLOC("Service Capabality Status")).c_str(),							g_FamilyServicePlans,	g_ColumnSize12);
	m_ColServiceProvisioningStatus				= AddColumn(_YUID("provisionedPlans.ProvisoningStatus"),			YtriaTranslate::Do(GridLicenses_customizeGrid_15, _YLOC("Service Provisioning Status")).c_str(),							g_FamilyServicePlans,	g_ColumnSize12);
	m_ColServiceLicenseProvisioningStatus		= AddColumn(_YUID("licenseDetails.provisioningStatus"),				YtriaTranslate::Do(GridLicenses_customizeGrid_16, _YLOC("License Details - Service Provisioning Status")).c_str(),		g_FamilyServicePlans,	g_ColumnSize22);
	m_ColServiceLicenseProvisioningAppliesTo	= AddColumn(_YUID("licenseDetails.appliesTo"),						YtriaTranslate::Do(GridLicenses_customizeGrid_17, _YLOC("License Details - Service Provisioning Applies To")).c_str(),	g_FamilyServicePlans,	g_ColumnSize12);
	m_ColServicePlanID							= AddColumnCaseSensitive(_YUID("licenseDetails.servicePlanId"),		YtriaTranslate::Do(GridLicenses_customizeGrid_18, _YLOC("Service Plan ID")).c_str(),										g_FamilyServicePlans,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSkuId									= AddColumnCaseSensitive(_YUID("licenseDetails.skuID"),				YtriaTranslate::Do(GridLicenses_customizeGrid_19, _YLOC("License Sku ID")).c_str(),										g_FamilyLicenses,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColGlobalServiceProvisionningStatus		= AddColumn(_YUID("subscribedSku.provisioningStatus"),				YtriaTranslate::Do(GridLicenses_customizeGrid_20, _YLOC("Global Service Provisioning Status")).c_str(),					g_FamilyServicePlans,	g_ColumnSize12);
	m_ColGlobalServiceAppliesTo					= AddColumn(_YUID("subscribedSku.GlobalServiceAppliesTo"),			YtriaTranslate::Do(GridLicenses_customizeGrid_21, _YLOC("Global Service Applies To")).c_str(),							g_FamilyServicePlans,	g_ColumnSize12);
	m_ColCapabilityStatus						= AddColumn(_YUID("subscribedSku.capabilityStatus"),				YtriaTranslate::Do(GridLicenses_customizeGrid_22, _YLOC("Global License - Capability Status")).c_str(),					g_FamilyLicenses,		g_ColumnSize12);
	m_ColAppliesTo								= AddColumn(_YUID("subscribedSku.appliesTo"),						YtriaTranslate::Do(GridLicenses_customizeGrid_23, _YLOC("Global License - Applies To")).c_str(),							g_FamilyLicenses,		g_ColumnSize12);
    
	addColumnGraphID();
	AddColumnForRowPK(GetColMetaId());
	AddColumnForRowPK(m_ColServicePlanID);
	AddColumnForRowPK(m_ColSkuId);

	m_Template.CustomizeGrid(*this);

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameLicenses*>(GetParentFrame()));

	//m_ColSubscriptionName = AddColumn(_YUID("subscriptionName"), _TLOC("Subscription Name"), _TLOC("Level 1"));
	//m_ColServicePlan = AddColumn(_YUID("servicePlan"), _TLOC("Service Plan"), _TLOC("Level 2"));

	// Add icons
    
	m_PlanMarkedForActivationId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PLAN_MARKED_FOR_ACTIVATION));
	m_PlanMarkedForDeactivationId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PLAN_MARKED_FOR_DEACTIVATION));
	m_SkuMarkedForActivationId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SKU_MARKED_FOR_ACTIVATION));
	m_SkuMarkedForDeactivationId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SKU_MARKED_FOR_DEACTIVATION));
	m_HasLicenseIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_HAS_LICENSE));
	m_HasNotLicenseIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_HAS_NOT_LICENSE));
	m_SuccessPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SUCCESS_PLAN));
	m_DisabledPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_DISABLED_PLAN));
	m_NotApplicablePlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_NOT_APPLICABLE_PLAN));
	m_PendingInputPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PENDING_INPUT_PLAN));
	m_PendingActivationPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PENDING_ACTIVATION_PLAN));
	m_PendingProvisioningPlanIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PENDING_PROVISIONING_PLAN));
	m_AssignedByGroupIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_GROUP));

	m_ProvisioningStatusTextAndIcon = TextAndIconMap(
	{
		{ _YTEXT("Success"),{ YtriaTranslate::Do(GridLicenses_customizeGrid_25, _YLOC("Success")).c_str(), m_SuccessPlanIconId } },
		{ g_ProvisioningStatusDisabled,{ YtriaTranslate::Do(GridLicenses_customizeGrid_26, _YLOC("Disabled")).c_str(), m_DisabledPlanIconId } },
		{ _YTEXT("PendingInput"),{ YtriaTranslate::Do(GridLicenses_customizeGrid_27, _YLOC("Pending Input")).c_str(), m_PendingInputPlanIconId } },
		{ _YTEXT("PendingActivation"),{ YtriaTranslate::Do(GridLicenses_customizeGrid_28, _YLOC("Pending Activation")).c_str(), m_PendingActivationPlanIconId } },
		{ _YTEXT("PendingProvisioning"),{ YtriaTranslate::Do(GridLicenses_customizeGrid_29, _YLOC("Pending Provisioning")).c_str(), m_PendingProvisioningPlanIconId } },

		// Those are not provisioning status, but using this map is handy.
		{ g_IdAssignedLicense, { YtriaTranslate::Do(GridLicenses_FillRows_1, _YLOC("YES - LICENSE")).c_str(), m_HasLicenseIconId} },
		{ g_IdUnassignedLicense, { YtriaTranslate::Do(GridLicenses_FillRows_2, _YLOC("NO - LICENSE")).c_str(), m_HasNotLicenseIconId } },
		{ g_IdNotApplicablePlan, { YtriaTranslate::Do(GridLicenses_FillRows_4, _YLOC("N/A - PLAN")).c_str(), m_NotApplicablePlanIconId } },
	});

	m_ColumnsToRepeatOnPlans =
		{
			m_ColSkuPartNumber,
			m_ColSkuLabel,
			m_ColCapabilityStatus,
			m_ColAppliesTo,
			GetColId(),
			m_ColIsAssignedByGroup,
			m_ColAssignedByGroupId,
			m_ColAssignedByGroupDisplayName,
			m_ColAssignedByGroupMail,
			m_ColAssignementState,
			m_ColAssignementStateError,
		};
	m_ColumnsToRepeatOnPlans.insert(m_MetaColumns.begin(), m_MetaColumns.end());
}

wstring GridLicenses::GetName(const GridBackendRow* p_Row) const
{
	if (nullptr == p_Row)
		return makeRowName(p_Row, GetColMetaDisplayName());// plouf
	else
		return makeRowName(p_Row
			, { {_T("Owner"), GetColMetaDisplayName() } }
			, p_Row->HasField(m_ColServicePlanName->GetID())
			? _YTEXTFORMAT(L"SKU %s PLAN %s", p_Row->GetField(m_ColSkuPartNumber).ToString().c_str(), p_Row->GetField(m_ColServicePlanName).ToString().c_str())
			: _YTEXTFORMAT(L"SKU %s", p_Row->GetField(m_ColSkuPartNumber).ToString().c_str())
		);
}

bool GridLicenses::HasModificationsPending(bool inSelectionOnly /*= false*/)
{
	if (inSelectionOnly)
		return HasModificationsPendingInParentOfSelected();
	return O365Grid::HasModificationsPending(false);
}

ModuleCriteria GridLicenses::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria;

    GridFrameBase* frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
    ASSERT(nullptr != frame);
    if (nullptr != frame)
    {
        criteria = frame->GetModuleCriteria();

        criteria.m_IDs.clear();
        for (auto row : p_Rows)
            criteria.m_IDs.insert(row->GetField(GetColMetaId()).GetValueStr());
    }

    return criteria;
}

void GridLicenses::BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessLicenseDetail>>& p_LicensesData, const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus, bool p_FullPurge)
{
	ASSERT(!IsMyData() || p_LicensesData.size() == 1);

	GridUpdater updater(*this, GridUpdaterOptions(
        GridUpdaterOptions::UPDATE_GRID
        | (p_FullPurge || IsMyData() ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
        | GridUpdaterOptions::REFRESH_MODIFICATION_STATES
        | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
        | GridUpdaterOptions::REFRESH_PROCESS_DATA
    ));

	bool oneCanceled = false;
	bool oneOK = false;

	for (const auto& licenseData : p_BusinessSubscribedSkus)
	{
		if (licenseData.GetSkuId() && licenseData.GetSkuPartNumber())
		{
			m_skuDetails.insert({ *licenseData.GetSkuId(), *licenseData.GetSkuPartNumber() });
			if (!licenseData.GetServicePlans().empty())
			{
				for (const auto& servicePlan : licenseData.GetServicePlans())
					m_planDetails.insert({ std::make_pair(*licenseData.GetSkuId(), *servicePlan.m_ServicePlanId), *servicePlan.m_ServicePlanName });
			}
		}
	}

    for (const auto& keyVal : p_LicensesData)
    {
		const BusinessUser& user = keyVal.first;

		if (m_HasLoadMoreAdditionalInfo)
		{
			if (user.IsMoreLoaded())
				m_MetaIDsMoreLoaded.insert(user.GetID());
			else
				m_MetaIDsMoreLoaded.erase(user.GetID());
		}

		auto parentUserRow = m_Template.AddRow(*this, user, {}, updater, false, true);

		ASSERT(nullptr != parentUserRow);
        if (nullptr != parentUserRow)
		{
			if (!IsMyData()) // Row will be deleted below
				updater.GetOptions().AddRowWithRefreshedValues(parentUserRow);
			parentUserRow->SetHierarchyParentToHide(true);

			parentUserRow->AddField(user.GetID(), GetColId());

			if (user.HasFlag(BusinessObject::CANCELED))
			{
				oneCanceled = true;

				// Could be uncommented if we notice missing explosion refresh on canceled rows
				//auto children = GetChildRows(parentUserRow);
				//for (auto child : children)
				//	updater.GetOptions().AddRowWithRefreshedValues(child);
			}
			else
			{
				oneOK = true;
				if (!IsMyData()) // Row will be deleted below
					updater.GetOptions().AddPartialPurgeParentRow(parentUserRow);

				FillRows(parentUserRow, user, keyVal.second, p_BusinessSubscribedSkus, updater);
			}

			// FIXME: Handle errors.

            //if (keyVal.second.size() == 1 && keyVal.second[0].GetError())
            //{
            //    wstring errorMessage = keyVal.second[0].GetError()->GetFullErrorMessage();
            //    OnRowError(parentRow, false, errorMessage);
            //}
            //else
            /*{
                for (const auto& message : keyVal.second)
                {
                    auto messageRow = FillRow(user, message);
                    messageRow->SetParentRow(parentRow);
                }
            }*/

			AddRoleDelegationFlag(parentUserRow, user);

			if (IsMyData())
			{
				ASSERT(m_MyDataMeHandler);
				ASSERT(parentUserRow->HasChildrenRows());
				m_MyDataMeHandler->GetRidOfMeRow(*this, parentUserRow);
			}
        }
    }

	if (oneCanceled)
	{
		if (oneOK)
			updater.GetOptions().SetPurgeFlag(GridUpdaterOptions::PARTIALPURGE); // Only purge not-canceled top rows
		else
			updater.GetOptions().SetPurgeFlag(0); // Everything canceled, don't purge anything.
	}

	m_ConsumedSkus = Util::GetConsumedMap(p_BusinessSubscribedSkus);
	m_RbacConsumedSkus = GetGraphCache().GetSkuCounters();

    // Second pass to update Consumed/Enabled/Suspended/Warning columns
    for (auto skuRow : GetBackendRows())
    {
        if (GridUtil::isBusinessType<BusinessSubscribedSku>(skuRow))
        {
            auto skuId = skuRow->GetField(m_ColSkuId).GetValueStr();
            auto subscribedSku = std::find_if(p_BusinessSubscribedSkus.begin(), p_BusinessSubscribedSkus.end(), [colSkuId = m_ColSkuId, skuId](const BusinessSubscribedSku& p_Sku) {
                return p_Sku.GetSkuId() != boost::none && *p_Sku.GetSkuId() == skuId;
            });

            ASSERT(subscribedSku != p_BusinessSubscribedSkus.end());
            if (subscribedSku != p_BusinessSubscribedSkus.end())
            {
				if (subscribedSku->GetConsumedUnits() != boost::none)
					m_ConsumedSkus[skuId] = *subscribedSku->GetConsumedUnits();

				skuRow->AddField(subscribedSku->GetConsumedUnits(), m_ColConsumedUnits);

				if (nullptr != m_ColConsumedUnitsInRbacScope)
				{
					if (subscribedSku->GetSkuId())
						skuRow->AddField(m_RbacConsumedSkus[*subscribedSku->GetSkuId()], m_ColConsumedUnitsInRbacScope);
					else
						skuRow->RemoveField(m_ColConsumedUnitsInRbacScope);
				}

                if (subscribedSku->GetPrepaidUnits().is_initialized())
                {
                    skuRow->AddField(subscribedSku->GetPrepaidUnits()->m_Enabled, m_ColEnabledUnits);
                    skuRow->AddField(subscribedSku->GetPrepaidUnits()->m_Suspended, m_ColSuspendedUnits);
                    skuRow->AddField(subscribedSku->GetPrepaidUnits()->m_Warning, m_ColWarningUnits);

					if (nullptr != m_ColEnabledUnitsInRbacScope)
						skuRow->AddField(subscribedSku->GetPrepaidUnits()->m_Accessible, m_ColEnabledUnitsInRbacScope);
                }
                else
                {
                    skuRow->RemoveField(m_ColEnabledUnits);
                    skuRow->RemoveField(m_ColSuspendedUnits);
                    skuRow->RemoveField(m_ColWarningUnits);

					if (m_ColEnabledUnitsInRbacScope)
						skuRow->RemoveField(m_ColEnabledUnitsInRbacScope);
                }
            }
        }
    }
}

void GridLicenses::ToggleShowNoLicenseRows()
{
	m_ShowNoLicenseRows = !m_ShowNoLicenseRows;
	refreshCustomHiddenRows(true);
}

void GridLicenses::refreshCustomHiddenRows(bool p_UpdateGrid)
{
	const vector < GridBackendRow* >& rows = GetBackendRows();

	for (auto pRow : rows)
		refreshCustomRowVisibility(pRow);

	if (p_UpdateGrid)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridLicenses::refreshCustomRowVisibility(GridBackendRow* p_Row) const
{
	ASSERT(p_Row != nullptr);
	if (p_Row != nullptr && !p_Row->IsGroupRow() && !GridUtil::IsBusinessUser(p_Row))
	{
		bool techHidden = false;
		if (!m_ShowPlanRows && GridUtil::isBusinessType<ServicePlanInfo>(p_Row))
			techHidden = true;

		if (!techHidden && !m_ShowNoLicenseRows)
		{
			GridBackendField& field = p_Row->GetField(m_ColIsAssigned);
			if (field.GetIcon() == m_HasNotLicenseIconId || field.GetIcon() == m_NotApplicablePlanIconId/* ||
				field.GetIcon() == m_SkuMarkedForDeactivationId || field.GetIcon() == m_PlanMarkedForDeactivationId*/)
				techHidden = true;
		}

		p_Row->SetTechHidden(techHidden);
	}
}

void GridLicenses::setUsageLocation(GridBackendRow* p_Row, const boost::YOpt<PooledString>& p_UsageLocation)
{
	const bool hadField = p_Row->HasField(GetColMetaUsageLocation());
	const GridBackendField oldField = p_Row->GetField(GetColMetaUsageLocation());

	const wstring userId = p_Row->GetField(GetColMetaId()).GetValueStr();

	auto& newField = p_Row->AddField(p_UsageLocation, GetColMetaUsageLocation());

	if (newField.IsModified() && (hadField || !hadField && !newField.IsGeneric()))
	{
		row_pk_t rowPK;
		GetRowPK(p_Row, rowPK);

		auto mod = std::make_unique<UserLicenseModification>(*this, rowPK);
		mod->AddUsageLocationChange(hadField ? boost::YOpt<PooledString>(oldField.GetValueStr()) : boost::none, boost::YOpt<PooledString>(newField.GetValueStr()));

		GetModifications().Add(std::move(mod));
		auto userLicMod = GetModifications().GetRowModification<UserLicenseModification>(rowPK);
		ASSERT(nullptr != userLicMod);
		if (nullptr != userLicMod)
			userLicMod->ShowUsageLocationAppliedLocally();
		if (!userLicMod->HasUsageLocationChange())
			newField.RemoveModified();

		// Show column containing modification if hidden
		{
			if (!GetColMetaUsageLocation()->IsVisible())
				GetBackend().SetColumnVisible(GetColMetaUsageLocation(), true, false);
		}
	}
}

bool GridLicenses::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return O365Grid::IsRowValidForNoFieldText(p_Row, p_Col)
		&& (!p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore)
			|| m_MetaIDsMoreLoaded.end() == m_MetaIDsMoreLoaded.find(p_Row->GetField(GetColMetaId()).GetValueStr()));
}

bool GridLicenses::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	if (m_Template.GetSnapshotValue(p_Value, p_Field, p_Column))
		return true;

	if (p_Field.HasValue())
	{
		if (m_ColIsAssigned == &p_Column)
		{
			const auto icon = p_Field.GetIcon();

			auto it = std::find_if(m_ProvisioningStatusTextAndIcon.begin(), m_ProvisioningStatusTextAndIcon.end(),
				[icon](const auto& item)
				{
					return icon == item.second.m_Icon;
				});

			ASSERT(m_ProvisioningStatusTextAndIcon.end() != it);
			if (m_ProvisioningStatusTextAndIcon.end() != it)
			{
				p_Value = it->first;
				return true;
			}
		}
		else if (m_ColIsAssignedByGroup == &p_Column)
		{
			if (m_AssignedByGroupIconId == p_Field.GetIcon())
			{
				p_Value = _YTEXT("1");
			}
			else
			{
				ASSERT(false);
				p_Value = _YTEXT("0");
			}
			return true;
		}
	}

	return O365Grid::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridLicenses::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	RunOnScopeEnd _([this, &p_Column, &p_Row]()
		{
			if (&p_Column == GetColumnObjectType() || &p_Column == m_ColIsAssigned)
				refreshCustomRowVisibility(&p_Row);

			if (&p_Column == GetColumnObjectType() && GridUtil::isBusinessType<BusinessSubscribedSku>(&p_Row))
				p_Row.SetFlag(GridBackendUtil::ACTASCHILDIFNOVISIBLECHILD);
		});

	if (m_Template.LoadSnapshotValue(p_Value, p_Row, p_Column))
		return true;
	
	if (m_ColIsAssigned == &p_Column)
	{
		auto it = m_ProvisioningStatusTextAndIcon.find(p_Value);
		ASSERT(m_ProvisioningStatusTextAndIcon.end() != it);
		if (m_ProvisioningStatusTextAndIcon.end() != it)
		{
			p_Row.AddField(it->second.m_Text, m_ColIsAssigned, it->second.m_Icon);
			return true;
		}
	}
	else if (m_ColIsAssignedByGroup == &p_Column)
	{
		ASSERT(_YTEXT("1") == p_Value);
		if (_YTEXT("1") == p_Value)
		{
			p_Row.AddField(g_AssignedByGroup, m_ColIsAssignedByGroup, m_AssignedByGroupIconId);
			return true;
		}
	}

	return O365Grid::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

bool GridLicenses::IsShowNoLicenseRows() const
{
	return m_ShowNoLicenseRows;
}

void GridLicenses::ToggleShowPlanRows()
{
	m_ShowPlanRows = !m_ShowPlanRows;

	if (m_ShowPlanRows)
	{
		AddHierarchyObjectTypeToFilterOn(GetObjectTypeIndex(ServicePlanInfo()), false);
		RemoveHierarchyObjectTypeToFilterOn(GetObjectTypeIndex(BusinessSubscribedSku()), false);
	}
	else
	{
		AddHierarchyObjectTypeToFilterOn(GetObjectTypeIndex(BusinessSubscribedSku()), false);
		RemoveHierarchyObjectTypeToFilterOn(GetObjectTypeIndex(ServicePlanInfo()), false);
	}

	refreshCustomHiddenRows(true);
}

bool GridLicenses::IsShowPlanRows() const
{
	return m_ShowPlanRows;
}

int32_t GridLicenses::GetNbConsumedBySku(PooledString p_SkuId, bool p_Rbac) const
{
	if (p_Rbac)
	{
		auto itt = m_RbacConsumedSkus.find(p_SkuId);
		if (itt != m_RbacConsumedSkus.end())
			return itt->second;
		ASSERT(false);
		return -1;
	}
	
	auto itt = m_ConsumedSkus.find(p_SkuId);
	if (itt != m_ConsumedSkus.end())
		return itt->second;
	ASSERT(false);
	return -1;
}

const map<PooledString, int32_t>& GridLicenses::GetConsumedMap() const
{
	return m_ConsumedSkus;
}

GridBackendColumn* GridLicenses::GetColMetaId() const
{
    return m_Template.m_ColumnID;
}

GridBackendColumn* GridLicenses::GetColMetaUsageLocation() const
{
	return m_Template.m_ColumnUsageLocation;
}

GridBackendColumn* GridLicenses::GetColMetaDisplayName() const
{
	return m_Template.m_ColumnDisplayName;
}

GridBackendColumn* GridLicenses::GetColMetaUserPrincipalName() const
{
	return m_Template.m_ColumnUserPrincipalName;
}

GridBackendColumn* GridLicenses::GetColIsAssigned() const
{
	return m_ColIsAssigned;
}

GridBackendColumn* GridLicenses::GetColIsAssignedBool() const
{
	return m_ColIsAssignedBool;
}

GridBackendColumn* GridLicenses::GetColSkuId() const
{
	return m_ColSkuId;
}

GridBackendColumn* GridLicenses::GetColSkuPartNumber() const
{
	return m_ColSkuPartNumber;
}

GridBackendColumn* GridLicenses::GetColSkuName() const
{
	return m_ColSkuLabel;
}

GridBackendColumn* GridLicenses::GetColServicePlanID() const
{
	return m_ColServicePlanID;
}

GridBackendColumn* GridLicenses::GetColServicePlanName() const
{
	return m_ColServicePlanName;
}

GridBackendColumn* GridLicenses::GetColServicePlanFriendlyName() const
{
	return m_ColServicePlanFriendlyName;
}

GridBackendColumn* GridLicenses::GetColConsumedUnits() const
{
	return m_ColConsumedUnits;
}

GridBackendColumn* GridLicenses::GetColConsumedUnitsInRbacScope() const
{
	return m_ColConsumedUnitsInRbacScope;
}

GridBackendColumn* GridLicenses::GetColEnabledUnits() const
{
	return m_ColEnabledUnits;
}

GridBackendColumn* GridLicenses::GetColEnabledUnitsInRbacScope() const
{
	return m_ColEnabledUnitsInRbacScope;
}

int GridLicenses::GetPlanMarkedForActivationId() const
{
	return m_PlanMarkedForActivationId;
}

int GridLicenses::GetPlanMarkedForDeactivationId() const
{
	return m_PlanMarkedForDeactivationId;
}

int GridLicenses::GetSkuMarkedForActivationId() const
{
	return m_SkuMarkedForActivationId;
}

int GridLicenses::GetSkuMarkedForDeactivationId() const
{
	return m_SkuMarkedForDeactivationId;
}

bool GridLicenses::IsMyData() const
{
	return m_IsMyData;
}

O365Grid::SnapshotCaps GridLicenses::GetSnapshotCapabilities() const
{
	return { true };
}

void GridLicenses::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
	pPopup->ItemInsert(ID_LICENSESGRID_EDITLICENSES);
	pPopup->ItemInsert(ID_LICENSESGRID_EDITUSAGELOCATION);
	pPopup->ItemInsert(ID_LICENSESGRID_SHOWPLANS);
	pPopup->ItemInsert(ID_LICENSESGRID_SHOWUNASSIGNED);
	pPopup->ItemInsert();

	O365Grid::OnCustomPopupMenu(pPopup, nStart);
}

PooledString GridLicenses::GetSkuPartNumber(PooledString p_Id) const
{
	auto it = m_skuDetails.find(p_Id);
	if (it != m_skuDetails.end())
		return it->second;
	return PooledString();
}

PooledString GridLicenses::GetPlanPartNumber(PooledString p_IdSku, PooledString p_IdPlan) const
{
	auto it = m_planDetails.find(std::make_pair(p_IdSku, p_IdPlan));
	if (it != m_planDetails.end())
		return it->second;
	return PooledString();
}

void GridLicenses::FillRows(GridBackendRow* p_ParentRow, const BusinessUser& p_User, const vector<BusinessLicenseDetail>& p_LicensesData, const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus, GridUpdater& p_Updater)
{
	if (p_BusinessSubscribedSkus.size() == 1 && p_BusinessSubscribedSkus[0].GetError())
	{
		m_SkuError = true;
		p_Updater.OnLoadingError(p_ParentRow, *p_BusinessSubscribedSkus[0].GetError());
	}
	else
	{
		const GridBackendField fMetaID(p_User.GetID(), GetColMetaId());
		for (const auto& businessSubscribedSku : p_BusinessSubscribedSkus)
		{
			ASSERT(businessSubscribedSku.GetSkuId().is_initialized());
			const GridBackendField fSkuID(businessSubscribedSku.GetSkuId().is_initialized() ? businessSubscribedSku.GetSkuId().get() : _YTEXT(""), m_ColSkuId);

			const bool licenseRowExisted = nullptr != GetRowIndex().GetExistingRow({ fMetaID, fSkuID });
			GridBackendRow* licenseRow = GetRowIndex().GetRow({ fMetaID, fSkuID }, true, true);

			ASSERT(nullptr != licenseRow);
			if (nullptr != licenseRow)
			{
				p_Updater.GetOptions().AddRowWithRefreshedValues(licenseRow);

				licenseRow->SetFlag(GridBackendUtil::ACTASCHILDIFNOVISIBLECHILD);
				licenseRow->SetParentRow(p_ParentRow);
				SetRowObjectType(licenseRow, businessSubscribedSku, businessSubscribedSku.HasFlag(BusinessObject::CANCELED));

				licenseRow->AddField(p_ParentRow->GetField(m_ColumnLastRequestDateTime), m_ColumnLastRequestDateTime->GetID());

				// User meta data
				for (auto c : m_MetaColumns)
				{
					if (p_ParentRow->HasField(c))
						licenseRow->AddField(p_ParentRow->GetField(c));
					else
						licenseRow->RemoveField(c);
				}

				licenseRow->AddField(businessSubscribedSku.GetSkuPartNumber(), m_ColSkuPartNumber);
				if (businessSubscribedSku.GetSkuId())
				{
					const auto& label = SkuAndPlanReference::GetInstance().GetSkuLabel(*businessSubscribedSku.GetSkuId());
					if (!label.IsEmpty())
						licenseRow->AddField(label, m_ColSkuLabel);
					else
						licenseRow->AddField(businessSubscribedSku.GetSkuPartNumber(), m_ColSkuLabel);
				}
				licenseRow->AddField(businessSubscribedSku.GetCapabilityStatus(), m_ColCapabilityStatus);
				licenseRow->AddField(businessSubscribedSku.GetAppliesTo(), m_ColAppliesTo);

				if (p_User.GetLicenseAssignmentStates())
				{
					auto it = std::find_if(
						p_User.GetLicenseAssignmentStates()->begin(), 
						p_User.GetLicenseAssignmentStates()->end(),
						[skuId = businessSubscribedSku.GetSkuId()](const auto& las)
						{
							return las.m_SkuId == skuId;
						});

					if (p_User.GetLicenseAssignmentStates()->end() != it)
					{
						if (it->m_AssignedByGroup.is_initialized())
						{
							licenseRow->AddField(g_AssignedByGroup, m_ColIsAssignedByGroup, m_AssignedByGroupIconId);
							auto g = GetGraphCache().GetGroupInCache(*it->m_AssignedByGroup);
							ASSERT(g.GetID() == *it->m_AssignedByGroup);
							if (g.GetID() == *it->m_AssignedByGroup)
							{
								licenseRow->AddField(g.GetDisplayName(), m_ColAssignedByGroupDisplayName);
								licenseRow->AddField(g.GetMail(), m_ColAssignedByGroupMail);
							}
							else
							{
								licenseRow->RemoveField(m_ColAssignedByGroupDisplayName);
								licenseRow->RemoveField(m_ColAssignedByGroupMail);
							}
						}
						else
						{
							licenseRow->RemoveField(m_ColIsAssignedByGroup);
							licenseRow->RemoveField(m_ColAssignedByGroupDisplayName);
							licenseRow->RemoveField(m_ColAssignedByGroupMail);
						}

						licenseRow->AddField(it->m_State, m_ColAssignementState);
						licenseRow->AddField(it->m_Error, m_ColAssignementStateError);
					}
					else
					{
						licenseRow->RemoveField(m_ColIsAssignedByGroup);
						licenseRow->RemoveField(m_ColAssignedByGroupId);
						licenseRow->RemoveField(m_ColAssignedByGroupDisplayName);
						licenseRow->RemoveField(m_ColAssignedByGroupMail);
						licenseRow->RemoveField(m_ColAssignementState);
						licenseRow->RemoveField(m_ColAssignementStateError);
					}
				}

				boost::YOpt<BusinessLicenseDetail> matchingLicenseDetail;

				if (p_LicensesData.size() == 1 && p_LicensesData[0].GetError())
				{
					p_Updater.OnLoadingError(licenseRow, *p_LicensesData[0].GetError());
				}
				else
				{
					// Clear previous error
					if (licenseRow->IsError())
						licenseRow->ClearStatus();

					bool found = false;
					for (const auto& licenseDetail : p_LicensesData)
					{
						if (licenseDetail.GetSkuId() == businessSubscribedSku.GetSkuId())
						{
							licenseRow->AddField(licenseDetail.GetID(), GetColId());
							matchingLicenseDetail.emplace(licenseDetail);
							found = true;
							break;
						}
					}
					if (!found)
						licenseRow->RemoveField(GetColId());
				}

				bool rowHasNoLicense = false;
				if (matchingLicenseDetail.is_initialized())
				{
					licenseRow->AddField(m_ProvisioningStatusTextAndIcon[g_IdAssignedLicense].m_Text, m_ColIsAssigned, m_ProvisioningStatusTextAndIcon[g_IdAssignedLicense].m_Icon);
					licenseRow->AddFieldForCheckBox(true, m_ColIsAssignedBool);
					licenseRow->SetTechHidden(false);
				}
				else
				{
					licenseRow->AddField(m_ProvisioningStatusTextAndIcon[g_IdUnassignedLicense].m_Text, m_ColIsAssigned, m_ProvisioningStatusTextAndIcon[g_IdUnassignedLicense].m_Icon);
					licenseRow->AddFieldForCheckBox(false, m_ColIsAssignedBool);
					licenseRow->SetTechHidden(!m_ShowNoLicenseRows);
					rowHasNoLicense = true;
				}

				for (const auto& servicePlan : businessSubscribedSku.GetServicePlans())
				{
					ASSERT(servicePlan.m_ServicePlanId.is_initialized());
					const GridBackendField fServicePlanID(servicePlan.m_ServicePlanId.is_initialized() ? servicePlan.m_ServicePlanId.get() : _YTEXT(""), m_ColServicePlanID);
					GridBackendRow* planRow = GetRowIndex().GetRow({ fMetaID, fSkuID, fServicePlanID }, true, true);
					ASSERT(nullptr != planRow);
					if (nullptr != planRow)
					{
						planRow->SetTechHidden(!m_ShowPlanRows || rowHasNoLicense && !m_ShowNoLicenseRows);

						p_Updater.GetOptions().AddRowWithRefreshedValues(planRow);

						planRow->SetParentRow(licenseRow);
						planRow->AddField(licenseRow->GetField(m_ColumnLastRequestDateTime), m_ColumnLastRequestDateTime->GetID());

						if (!licenseRowExisted) // If we just added this row
							licenseRow->SetCollapsed(rowHasNoLicense);

						SetRowObjectType(planRow, ServicePlanInfo(), false, NO_ICON, YtriaTranslate::Do(GridLicenses_FillRows_3, _YLOC("Service Plan")).c_str());// sale de dirty

						// Repeat license info on plan row
						for (auto c : m_ColumnsToRepeatOnPlans)
						{
							if (licenseRow->HasField(c))
								planRow->AddField(licenseRow->GetField(c));
							else
								planRow->RemoveField(c);
						}

						if (rowHasNoLicense)
							planRow->AddField(m_ProvisioningStatusTextAndIcon[g_IdNotApplicablePlan].m_Text, m_ColIsAssigned, m_ProvisioningStatusTextAndIcon[g_IdNotApplicablePlan].m_Icon);

						planRow->AddField(servicePlan.m_ServicePlanName, m_ColServicePlanName);
						
						if (servicePlan.m_ServicePlanId)
						{
							const auto& label = SkuAndPlanReference::GetInstance().GetPlanLabel(*servicePlan.m_ServicePlanId);
							if (!label.IsEmpty())
								planRow->AddField(label, m_ColServicePlanFriendlyName);
							else
								planRow->AddField(servicePlan.m_ServicePlanName, m_ColServicePlanFriendlyName);
						}

						//planRow->AddField(getSubscriptionName(businessSubscribedSku.GetSkuPartNumber(), servicePlan.m_ServicePlanName), m_ColSubscriptionName);
						//planRow->AddField(getServicePlan(businessSubscribedSku.GetSkuPartNumber(), servicePlan.m_ServicePlanName), m_ColServicePlan);
						planRow->AddField(servicePlan.m_ProvisioningStatus, m_ColGlobalServiceProvisionningStatus);
						planRow->AddField(servicePlan.m_AppliesTo, m_ColGlobalServiceAppliesTo);

						if (matchingLicenseDetail.is_initialized())
						{
							bool found = false;
							for (const auto& licenseDetailServicePlan : matchingLicenseDetail->GetServicePlanInfos())
							{
								if (licenseDetailServicePlan.m_ServicePlanId == servicePlan.m_ServicePlanId)
								{
									planRow->AddField(licenseDetailServicePlan.m_ProvisioningStatus, m_ColServiceLicenseProvisioningStatus);

									if (!rowHasNoLicense && licenseDetailServicePlan.m_ProvisioningStatus.is_initialized())
									{
										ASSERT(m_ProvisioningStatusTextAndIcon.end() != m_ProvisioningStatusTextAndIcon.find(*licenseDetailServicePlan.m_ProvisioningStatus));
										planRow->AddField(m_ProvisioningStatusTextAndIcon[*licenseDetailServicePlan.m_ProvisioningStatus].m_Text
											, m_ColIsAssigned
											, m_ProvisioningStatusTextAndIcon[*licenseDetailServicePlan.m_ProvisioningStatus].m_Icon);
									}
									else
									{
										planRow->RemoveField(m_ColIsAssigned);
									}

									planRow->AddField(licenseDetailServicePlan.m_AppliesTo, m_ColServiceLicenseProvisioningAppliesTo);
									found = true;
									break;
								}
							}
							if (!found)
							{
								planRow->RemoveField(m_ColServiceLicenseProvisioningStatus);
								planRow->RemoveField(m_ColServiceLicenseProvisioningAppliesTo);
								planRow->RemoveField(m_ColIsAssigned);
							}

							found = false;
							ASSERT(p_User.GetAssignedPlans());
							if (p_User.GetAssignedPlans())
							{
								for (const auto& assignedPlan : *p_User.GetAssignedPlans())
								{
									if (assignedPlan.m_ServicePlanId == servicePlan.m_ServicePlanId)
									{
										planRow->AddField(assignedPlan.m_CapabilityStatus, m_ColServiceCapabilityStatus);

										YTimeDate timeDate;
										if (assignedPlan.m_AssignedDateTime.is_initialized()
											&& TimeUtil::GetInstance().ConvertTextToDate(assignedPlan.m_AssignedDateTime.get().c_str(), timeDate))
											planRow->AddField(timeDate, m_ColServiceAssignedDate);
										else
											planRow->RemoveField(m_ColServiceAssignedDate);

										ASSERT(p_User.GetProvisionedPlans());
										if (p_User.GetProvisionedPlans())
										{
											for (const auto& provisionedPlan : *p_User.GetProvisionedPlans())
											{
												if (provisionedPlan.m_Service == assignedPlan.m_Service
													&& provisionedPlan.m_CapabilityStatus == assignedPlan.m_CapabilityStatus)
												{
													planRow->AddField(provisionedPlan.m_ProvisioningStatus, m_ColServiceProvisioningStatus);
													found = true;
													break;
												}
											}
										}

										if (!found)
											planRow->RemoveField(m_ColServiceProvisioningStatus);

										found = true;
										break;
									}
								}
							}

							if (!found)
							{
								planRow->RemoveField(m_ColServiceAssignedDate);
								planRow->RemoveField(m_ColServiceProvisioningStatus);
							}

							found = false;
							ASSERT(p_User.GetAssignedLicenses());
							if (p_User.GetAssignedLicenses())
							{
								for (const auto& assignedLicense : *p_User.GetAssignedLicenses())
								{
									if (assignedLicense.GetSkuId() == businessSubscribedSku.GetSkuId())
									{
										if (assignedLicense.GetDisabledPlans() && assignedLicense.GetDisabledPlans()->end() != std::find(assignedLicense.GetDisabledPlans()->begin(), assignedLicense.GetDisabledPlans()->end(), servicePlan.m_ServicePlanId))
											planRow->AddField(YtriaTranslate::Do(GridLicenses_FillRows_5, _YLOC("Disabled")).c_str(), m_ColServiceCapabilityStatus);
										else
											planRow->RemoveField(m_ColServiceCapabilityStatus);

										found = true;
										break;
									}
								}
							}

							if (!found)
								planRow->RemoveField(m_ColServiceCapabilityStatus);
						}
					}

					auto& field = planRow->GetField(m_ColIsAssigned);
					if (field.HasValue())
					{
						const bool isAssignedBool	= field.GetIcon() != m_PendingActivationPlanIconId
													&& field.GetIcon() != m_DisabledPlanIconId;
						planRow->AddFieldForCheckBox(!rowHasNoLicense && isAssignedBool, m_ColIsAssignedBool);
					}
				}
			}
		}
	}
}

const PooledString& GridLicenses::getServicePlan(const boost::YOpt<PooledString>& p_SkuPartNumber, const boost::YOpt<PooledString>& m_ServicePlanName)
{
	static const PooledString ret = L"";
	return ret;
}

const PooledString& GridLicenses::getSubscriptionName(const boost::YOpt<PooledString>& p_SkuPartNumber, const boost::YOpt<PooledString>& m_ServicePlanName)
{
	static const PooledString ret = L"";
	return ret;
}

void GridLicenses::editLicenses()
{
    DlgEditLicenses editor(*this, this->GetParentFrame());
    if (IDOK == editor.DoModal() && !editor.GetUsersSkus().empty() && !editor.GetNewUsersSkus().empty())
	{
		createModifications(editor.GetUsersSkus(), editor.GetNewUsersSkus());
		refreshCustomHiddenRows(false);
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}
}

bool GridLicenses::arePlansSetDifferent(const std::set<PlanInfo>& p_Plans1, const std::set<PlanInfo>& p_Plans2) const
{
    bool different = false;
    for (const auto& plan1 : p_Plans1)
    {
        auto ittPlan2 = p_Plans2.find(plan1);
        ASSERT(ittPlan2 != p_Plans2.end());
        if (ittPlan2 != p_Plans2.end())
        {
            if (plan1.Enabled != ittPlan2->Enabled)
            {
                different = true;
                break;
            }
        }
    }
    return different;
}

void GridLicenses::createModifications(const std::map<PooledString, std::set<SkuInfo>>& p_OldState, const std::map<PooledString, std::set<SkuInfo>>& p_WantedState)
{
	const auto isPlanModificationAllowed = [this](const auto& m_UserId, const auto& m_SkuId, const auto& m_PlanId)
	{
		row_pk_t pk {GridBackendField(m_UserId, GetColMetaId()), GridBackendField(m_SkuId, m_ColSkuId), GridBackendField(m_PlanId, m_ColServicePlanID)};
		auto planRow = GetRowIndex().GetExistingRow(pk);
		ASSERT(GridUtil::isBusinessType<ServicePlanInfo>(planRow));
		auto& field = planRow->GetField(m_ColIsAssigned);
		return field.HasValue()
			&& field.GetIcon() != m_PendingActivationPlanIconId
			&& field.GetIcon() != m_PendingProvisioningPlanIconId;
	};

	int forbiddenPlanModifications = 0;
    GetRowIndex().LoadRows();
    for (const auto& oldState : p_OldState)
    {
        const auto userId = oldState.first;
        const auto& oldSkus = oldState.second;
		const auto wantedItt = p_WantedState.find(oldState.first);
        ASSERT(wantedItt != p_WantedState.end());
        if (wantedItt != p_WantedState.end())
        {
            const auto& wantedSkus = wantedItt->second;
            for (const auto& wantedSku : wantedSkus)
            {
                const auto ittOldSku = oldSkus.find(wantedSku.Id);
                ASSERT(ittOldSku != oldSkus.end());
                if (ittOldSku != oldSkus.end())
                {
                    // If there is a difference between old values and wanted values, we create a modification
                    // containing the whole assignedLicense corresponding to the sku.
                    bool plansDifferent = arePlansSetDifferent(ittOldSku->Plans, wantedSku.Plans);
                    if (ittOldSku->Enabled != wantedSku.Enabled || plansDifferent)
                    {
						const auto userPk = getUserPkFromSkuPk(wantedSku.RowPk);
                        auto mod = std::make_unique<UserLicenseModification>(*this, userPk);
						bool isModified = false;

                        if (!wantedSku.Enabled)
                        {
                            mod->AddSkuToDisable(wantedSku.Id, wantedSku.RowPk, ittOldSku->Enabled);
							if (ittOldSku->Enabled)
								isModified = true;
                            // Mark plans as disabled. Those plans will not be sent along with the request
							for (const auto& planToMarkAsDisabled : wantedSku.Plans)
							{
								mod->AddPlanToDisable(wantedSku.Id, planToMarkAsDisabled.Id, planToMarkAsDisabled.RowPk, true);
								isModified = true;
							}
                        }
                        else
                        {
                            mod->AddSkuToEnable(wantedSku.Id, wantedSku.RowPk, !ittOldSku->Enabled);
							if (!ittOldSku->Enabled)
								isModified = true;
                            for (const auto& wantedPlan : wantedSku.Plans)
                            {
                                const auto ittOldPlan = ittOldSku->Plans.find(wantedPlan.Id);
                                ASSERT(ittOldPlan != ittOldSku->Plans.end());
                                if (ittOldPlan != ittOldSku->Plans.end())
								{
									const bool skuBeingEnabled = !ittOldSku->Enabled;
									const bool planWasEnabled = ittOldPlan->Enabled;
									const auto planModAllowed = isPlanModificationAllowed(userId, wantedSku.Id, ittOldPlan->Id);
									if (!wantedPlan.Enabled)
									{
										if (!planModAllowed && planWasEnabled)
											++forbiddenPlanModifications;
										mod->AddPlanToDisable(wantedSku.Id, wantedPlan.Id, wantedPlan.RowPk, planModAllowed && planWasEnabled);
										if (planModAllowed && planWasEnabled)
											isModified = true;
									}
									else
									{
										if (!planModAllowed && (skuBeingEnabled || !planWasEnabled))
											++forbiddenPlanModifications;
										mod->AddPlanToEnable(wantedSku.Id, wantedPlan.Id, wantedPlan.RowPk, planModAllowed && (skuBeingEnabled || !planWasEnabled));
										if (planModAllowed && (skuBeingEnabled || !planWasEnabled))
											isModified = true;
									}
                                }
                            }
                        }

						if (isModified)
							GetModifications().Add(std::move(mod));
						auto userLicMod = GetModifications().GetRowModification<UserLicenseModification>(userPk);
						ASSERT(!isModified || nullptr != userLicMod);
						if (nullptr != userLicMod)
						{
							auto row = GetRowIndex().GetExistingRow(userLicMod->GetRowKey());
							ASSERT(nullptr != row);
							if (!row->HasField(GetColMetaUsageLocation()))
								setUsageLocation(row, PooledString(_YTEXT("US"))); // Default usage location

							userLicMod->ShowUsageLocationAppliedLocally();
						}
                    }
                }
            }
        }
    }

	// FIXME: Should we display a warning message?
	if (forbiddenPlanModifications > 0)
	{
		YCodeJockMessageBox dlg(
			this,
			DlgMessageBox::eIcon_ExclamationWarning,
			_T("Read-only plans"),
			_YFORMAT(L"%s Service Plans couldn't be enabled/disabled.", Str::getStringFromNumber(forbiddenPlanModifications).c_str()),
			_T("\"Pending Provisioning\" and \"Pending Activation\" are read-only statuses."),
			{ { IDOK, _T("OK") } });

		dlg.DoModal();
	}

    UserLicenseModification::ComputeConsumedSkusWithMods(*this, ComputeDeltasFromMods());
}

map<PooledString, int32_t> GridLicenses::ComputeDeltasFromMods()
{
    map<PooledString, int32_t> deltas;
    for (auto mod : GetModifications().GetRowModifications([](RowModification* mod) { return true; }))
    {
        auto userMod = dynamic_cast<UserLicenseModification*>(mod);
        ASSERT(nullptr != userMod);
        if (userMod && userMod->GetState() == Modification::State::AppliedLocally)
        {
            for (const auto& modDelta : userMod->GetSkusDelta())
            {
                auto itt = deltas.find(modDelta.first);
                if (itt == deltas.end())
                    deltas[modDelta.first] = modDelta.second;
                else
                    itt->second += modDelta.second; // Add delta to existing delta
            }
        }
    }
    return deltas;
}

vector<GridBackendField> GridLicenses::getUserPkFromSkuPk(const vector<GridBackendField>& p_SkuPk)
{
    GridBackendRow* userRow = nullptr;
    auto skuRow = GetRowIndex().GetExistingRow(p_SkuPk);
    ASSERT(nullptr != skuRow);
    if (nullptr != skuRow)
    {
        userRow = skuRow->GetTopAncestorOrThis();
        ASSERT(nullptr != userRow);
    }

    vector<GridBackendField> userPk;
    GetRowPK(userRow, userPk);

    return userPk;
}

void GridLicenses::OnShowUnassigned()
{
	ToggleShowNoLicenseRows();
}

void GridLicenses::OnEditLicenses()
{
    editLicenses();
}

void GridLicenses::OnUpdateShowUnassigned(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && !m_SkuError);
	pCmdUI->SetCheck(IsShowNoLicenseRows() ? TRUE : FALSE);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridLicenses::OnUpdateEditLicenses(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!IsMyData() && hasNoTaskRunning() && !m_SkuError && HasOneNonGroupRowSelectedAtLeast());
    setTextFromProfUISCommand(*pCmdUI);
}

void GridLicenses::OnEditUsageLocation()
{
	vector<BusinessUser> users;
	vector<GridBackendRow*> rows;
	for (auto row : GetSelectedRows())
	{
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			auto userRow = row->GetTopAncestorOrThis();
			ASSERT(GridUtil::IsBusinessUser(userRow));
			if (GridUtil::IsBusinessUser(userRow) && std::find(rows.begin(), rows.end(), userRow) == rows.end())
			{
				rows.push_back(userRow);
				users.emplace_back();

				{
					const auto& field = userRow->GetField(GetColMetaId());
					if (field.HasValue())
						users.back().SetID(field.GetValueStr());
				}

				{
					const auto& field = userRow->GetField(m_Template.m_ColumnDisplayName);
					if (field.HasValue())
						users.back().SetDisplayName(PooledString(field.GetValueStr()));
				}

				{
					const auto& field = userRow->GetField(GetColMetaUsageLocation());
					if (field.HasValue())
						users.back().SetUsageLocation(PooledString(field.GetValueStr()));
				}

				{
					const auto& field = userRow->GetField(GetColMetaUserPrincipalName());
					if (field.HasValue())
						users.back().SetUserPrincipalName(PooledString(field.GetValueStr()));
				}
			}
		}

	}

	if (!users.empty())
	{
		ASSERT(users.size() == rows.size());
		DlgUserUsageLocationHTML dlg(users, this, g_ActionNameSelectedEditLicenseLocation);
		if (IDOK == dlg.DoModal())
		{
			GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID));
			for (size_t i = 0; i < users.size(); ++i)
			{
				auto& user = users[i];
				auto& row = rows[i];
				setUsageLocation(row, user.GetUsageLocation());				
			}
		}
	}
}

void GridLicenses::OnUpdateEditUsageLocation(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!IsMyData() && hasNoTaskRunning() && HasOneNonGroupRowSelectedAtLeast());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridLicenses::OnShowPlans()
{
	ToggleShowPlanRows();
}

void GridLicenses::OnUpdateShowPlans(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && !m_SkuError);
	pCmdUI->SetCheck(IsShowPlanRows() ? TRUE : FALSE);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridLicenses::InitializeCommands()
{
	O365Grid::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_LICENSESGRID_SHOWUNASSIGNED;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridLicenses_InitializeCommands_1, _YLOC("Show Unassigned Licenses")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridLicenses_InitializeCommands_2, _YLOC("Show Unassigned Licenses")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LICENSESGRID_SHOWUNASSIGNED_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_LICENSESGRID_SHOWUNASSIGNED, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridLicenses_InitializeCommands_3, _YLOC("Show Unassigned Licenses")).c_str(), YtriaTranslate::Do(GridLicenses_InitializeCommands_4, _YLOC("Display information about unassigned licenses (hidden by default).")).c_str(), _cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_LICENSESGRID_SHOWPLANS;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridLicenses_InitializeCommands_13, _YLOC("Show Plans")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridLicenses_InitializeCommands_14, _YLOC("Show Plans")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LICENSESGRID_SHOWPLANS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_LICENSESGRID_SHOWPLANS, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridLicenses_InitializeCommands_15, _YLOC("Show Plans")).c_str(), YtriaTranslate::Do(GridLicenses_InitializeCommands_16, _YLOC("Display information about Plans (hidden by default).")).c_str(), _cmd.m_sAccelText);
	}

    {
        CExtCmdItem _cmd;

        _cmd.m_nCmdID = ID_LICENSESGRID_EDITLICENSES;
        _cmd.m_sMenuText = YtriaTranslate::Do(GridLicenses_InitializeCommands_5, _YLOC("Edit Licenses")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridLicenses_InitializeCommands_6, _YLOC("Edit Licenses")).c_str();
        _cmd.m_sAccelText = _YTEXT("");
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
        HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LICENSESGRID_EDITLICENSES_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_LICENSESGRID_EDITLICENSES, hIcon, false);
        setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridLicenses_InitializeCommands_7, _YLOC("Edit Licenses")).c_str(), YtriaTranslate::Do(GridLicenses_InitializeCommands_8, _YLOC("Edit Licenses")).c_str(), _cmd.m_sAccelText);
    }

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_LICENSESGRID_EDITUSAGELOCATION;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridLicenses_InitializeCommands_9, _YLOC("Edit Usage location")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridLicenses_InitializeCommands_10, _YLOC("Edit Usage location")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		/*FIXME*/HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LICENSESGRID_EDITUSAGELOCATION_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_LICENSESGRID_EDITUSAGELOCATION, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridLicenses_InitializeCommands_11, _YLOC("Edit Usage location")).c_str(), YtriaTranslate::Do(GridLicenses_InitializeCommands_12, _YLOC("Edit Usage location")).c_str(), _cmd.m_sAccelText);
	}
}

wstring GridLicenses::GetDisplayName(GridBackendRow* p_Row) const
{
	wstring displayName;

	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		const auto& field = p_Row->GetField(m_Template.m_ColumnDisplayName);
		if (field.HasValue())
			displayName = field.GetValueStr();
	}

	return displayName;
}