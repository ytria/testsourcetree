#include "FrameLicensesDeletedUsers.h"

#include "CommandDispatcher.h"
#include "LoggerService.h"
#include "Office365Admin.h"

IMPLEMENT_DYNAMIC(FrameLicensesDeletedUsers, GridFrameBase)

FrameLicensesDeletedUsers::FrameLicensesDeletedUsers(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
    m_CreateAddRemoveToSelection = false;
    m_CreateRefreshPartial = false;
	m_CreateShowContext = false;
}

void FrameLicensesDeletedUsers::ShowLicenses(const O365DataMap<BusinessUser, vector<BusinessLicenseDetail>>& p_LicenseDetails, const vector<BusinessSubscribedSku>& m_SubscribedSkus, bool p_FullPurge)
{
    if (CompliesWithDataLoadPolicy())
    {
		m_LicensesGrid.BuildTreeView(p_LicenseDetails, m_SubscribedSkus, p_FullPurge);
        m_LicensesGrid.UpdateMegaShark();
    }
}

void FrameLicensesDeletedUsers::createGrid()
{
	m_LicensesGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameLicensesDeletedUsers::GetRefreshPartialControlConfig()
{
	ASSERT(false);
	return GridFrameBase::O365ControlConfig();
}

// returns false if no frame specific tab is needed.
bool FrameLicensesDeletedUsers::customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(p_Tab);
	CreateRefreshGroup(p_Tab);

	auto dataGroup = p_Tab.AddGroup(YtriaTranslate::Do(FrameLicenses_customizeActionsRibbonTab_1, _YLOC("Manage Licenses")).c_str());

	{
		auto ctrl = dataGroup->Add(xtpControlButton, ID_LICENSESGRID_SHOWPLANS);
		setImage({ ID_LICENSESGRID_SHOWPLANS }, IDB_LICENSESGRID_SHOWPLANS, xtpImageNormal);
		setImage({ ID_LICENSESGRID_SHOWPLANS }, IDB_LICENSESGRID_SHOWPLANS_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	{
		auto ctrl = dataGroup->Add(xtpControlButton, ID_LICENSESGRID_SHOWUNASSIGNED);
		setImage({ ID_LICENSESGRID_SHOWUNASSIGNED }, IDB_LICENSESGRID_SHOWUNASSIGNED, xtpImageNormal);
		setImage({ ID_LICENSESGRID_SHOWUNASSIGNED }, IDB_LICENSESGRID_SHOWUNASSIGNED_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	ASSERT(GetOrigin() == Origin::DeletedUser);
	CreateLinkGroups(p_Tab, true, false);

	automationAddCommandIDToGreenLight(ID_LICENSESGRID_SHOWUNASSIGNED);
	automationAddCommandIDToGreenLight(ID_LICENSESGRID_SHOWPLANS);

	return true;
}

O365Grid& FrameLicensesDeletedUsers::GetGrid()
{
	return m_LicensesGrid;
}

void FrameLicensesDeletedUsers::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	ASSERT(ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer && (Origin::User == GetModuleCriteria().m_Origin || Origin::DeletedUser == GetModuleCriteria().m_Origin));
	if (ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer && (Origin::User == GetModuleCriteria().m_Origin || Origin::DeletedUser == GetModuleCriteria().m_Origin))
	{
		info.SetOrigin(GetModuleCriteria().m_Origin);
		info.GetIds() = GetModuleCriteria().m_IDs;
	}
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Licenses, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameLicensesDeletedUsers::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Licenses, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

GridLicensesDeletedUsers& FrameLicensesDeletedUsers::GetGridLicenses()
{
    return m_LicensesGrid;
}

const GridLicensesDeletedUsers& FrameLicensesDeletedUsers::GetGridLicenses() const
{
    return m_LicensesGrid;
}

AutomatedApp::AUTOMATIONSTATUS FrameLicensesDeletedUsers::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionShowUnassignedLicenses(p_Action))
	{
		const bool isShowUL		= m_LicensesGrid.IsShowNoLicenseRows();
		const bool wantShowUL	= p_Action->IsParamTrue(AutomationConstant::val().m_ParamNameValue);
		p_CommandID				= ID_LICENSESGRID_SHOWUNASSIGNED; 
		// if already in wanted state, do nothing
		if (wantShowUL == isShowUL)
			statusRV = AutomatedApp::AUTOMATIONSTATUS_EXECUTED;
	}
	else if (IsActionShowServicePlans(p_Action))
	{
		const bool isShowPlans		= m_LicensesGrid.IsShowPlanRows();
		const bool wantShowPlans	= p_Action->IsParamTrue(AutomationConstant::val().m_ParamNameValue);
		p_CommandID = ID_LICENSESGRID_SHOWPLANS;
		// if already in wanted state, do nothing
		if (wantShowPlans == isShowPlans)
			statusRV = AutomatedApp::AUTOMATIONSTATUS_EXECUTED;
	}

	return statusRV;
}
