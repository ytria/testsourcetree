#pragma once

#include "ModuleBase.h"
#include "CommandInfo.h"

class AutomationAction;
class FrameLicenses;
class FrameLicensesSummary;
class FrameLicensesDeletedUsers;

class ModuleLicense : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	virtual void executeImpl(const Command& p_Command) override;
    void applyChanges(const Command& p_Command);
	void showLicenses(const Command& p_Command);
	//void update(std::shared_ptr<IUpdater> p_pUpdater, AutomationAction* p_Action);
	void refresh(const Command& p_Command);
	void doRefresh(FrameLicenses* p_pFrame, const ModuleCriteria& p_ModuleCriteria, const Command& p_Command, bool p_IsRefresh);
	void doRefresh(FrameLicensesSummary* p_pFrame, const ModuleCriteria& p_ModuleCriteria, const Command& p_Command, bool p_IsRefresh);
	void doRefresh(FrameLicensesDeletedUsers* p_pFrame, const ModuleCriteria& p_ModuleCriteria, const Command& p_Command, bool p_IsRefresh);

	void loadSnapshot(const Command& p_Command);
};
