#pragma once

#include "BaseO365Grid.h"
#include "BusinessLicenseDetail.h"
#include "BusinessSubscribedSku.h"
#include "BusinessUser.h"
#include "GridUpdater.h"
#include "SkuInfo.h"

class GridLicensesDeletedUsers : public O365Grid
{
public:
	GridLicensesDeletedUsers();

	virtual void customizeGrid() override;
    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessLicenseDetail>>& p_LicensesData, const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus, bool p_FullPurge);
    map<PooledString, int32_t> ComputeDeltasFromMods();

	void ToggleShowNoLicenseRows();
	bool IsShowNoLicenseRows() const;

	void ToggleShowPlanRows();
	bool IsShowPlanRows() const;

    int32_t GetNbConsumedBySku(PooledString p_SkuId) const;

    GridBackendColumn* GetColMetaId() const;

    static const wstring g_ProvisioningStatusDisabled;

	wstring GetDisplayName(GridBackendRow* p_Row) const;

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

private:
    DECLARE_MESSAGE_MAP()

	afx_msg void OnShowUnassigned();
	afx_msg void OnUpdateShowUnassigned(CCmdUI* pCmdUI);
	afx_msg void OnShowPlans();
	afx_msg void OnUpdateShowPlans(CCmdUI* pCmdUI);

	virtual void InitializeCommands() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart);

    void FillRows(GridBackendRow* p_ParentRow, const BusinessUser& p_User, const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus, GridUpdater& p_Updater);
	const PooledString& getServicePlan(const boost::YOpt<PooledString>& p_SkuPartNumber, const boost::YOpt<PooledString>& m_ServicePlanName);
	const PooledString& getSubscriptionName(const boost::YOpt<PooledString>& p_SkuPartNumber, const boost::YOpt<PooledString>& m_ServicePlanName);

    bool arePlansSetDifferent(const std::set<PlanInfo>& p_Plans1, const std::set<PlanInfo>& p_Plans2) const;

    vector<GridBackendField> getUserPkFromSkuPk(const vector<GridBackendField> &p_SkuPk);

	void refreshCustomHiddenRows(bool p_UpdateGrid);

private:
    GridBackendColumn* m_ColMetaId;
    GridBackendColumn* m_ColMetaDisplayName;
    GridBackendColumn* m_ColMetaUserPrincipalName;
	GridBackendColumn* m_ColumnMetaUserUsageLocation;

	/*LEVEL 1*/
	GridBackendColumn* m_ColSubscribedSkuId; // H

	GridBackendColumn* m_ColSkuId; // H
	GridBackendColumn* m_ColSkuPartNumber;

	//GridBackendColumn* m_ColSubscriptionName; // ?

	GridBackendColumn* m_ColIsAssigned;
    GridBackendColumn* m_ColIsAssignedBool;

	GridBackendColumn* m_ColCapabilityStatus;
	GridBackendColumn* m_ColAppliesTo;
	GridBackendColumn* m_ColConsumedUnits;
	GridBackendColumn* m_ColEnabledUnits;
	GridBackendColumn* m_ColSuspendedUnits;
	GridBackendColumn* m_ColWarningUnits;

	/*LEVEL 2*/
	GridBackendColumn* m_ColServicePlanID;
	GridBackendColumn* m_ColServicePlanName;
	//GridBackendColumn* m_ColServicePlan;
	GridBackendColumn* m_ColGlobalServiceProvisionningStatus;
	GridBackendColumn* m_ColGlobalServiceAppliesTo;
	//GridBackendColumn* m_ColServiceName;
	GridBackendColumn* m_ColServiceCapabilityStatus;
	GridBackendColumn* m_ColServiceProvisioningStatus;
	GridBackendColumn* m_ColServiceAssignedDate;
	//GridBackendColumn* m_ColServiceLicenseProvisioningStatus;
	//GridBackendColumn* m_ColServiceLicenseProvisioningAppliesTo;

    int m_PlanMarkedForActivationId;
    int m_PlanMarkedForDeactivationId;
    int m_SkuMarkedForActivationId;
    int m_SkuMarkedForDeactivationId;
	int m_HasLicenseIconId;
	int m_HasNotLicenseIconId;
	int m_SuccessPlanIconId;
	int m_DisabledPlanIconId;
	int m_NotApplicablePlanIconId;
	int m_PendingInputPlanIconId;
	int m_PendingActivationPlanIconId;
	int m_PendingProvisioningPlanIconId;

	struct TextAndIcon
	{
		TextAndIcon() = default;
		TextAndIcon(const wstring& p_Text, int p_Icon) : m_Text(p_Text), m_Icon(p_Icon) {}
		const wstring m_Text;
		const int m_Icon = -1;
	};
	using TextAndIconMap = std::map<wstring, TextAndIcon, Str::keyLessInsensitive>;
	TextAndIconMap m_ProvisioningStatusTextAndIcon;

    map<PooledString, int32_t> m_ConsumedSkus; // Key: SkuId, Value: Nb consumed skus

	bool m_ShowNoLicenseRows;
	bool m_ShowPlanRows;

	friend class GridLicensesDeletedUsersSummary; // Give access to TextAndIconMap type
    friend class UserLicenseModification; // Needs access to columns
};
