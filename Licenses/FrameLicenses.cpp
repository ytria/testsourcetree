#include "FrameLicenses.h"

#include "CommandDispatcher.h"
#include "LoggerService.h"
#include "Office365Admin.h"
#include "UserLicenseModification.h"

IMPLEMENT_DYNAMIC(FrameLicenses, GridFrameBase)

FrameLicenses::FrameLicenses(bool p_IsMyData, bool p_IsHandleSkuCounters, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, p_Module, p_HistoryMode, p_PreviousFrame)
	, m_LicensesGrid(p_IsMyData, p_IsHandleSkuCounters)
{
    m_CreateAddRemoveToSelection = !p_IsMyData;
    m_CreateRefreshPartial = !p_IsMyData;
	m_CreateShowContext = !p_IsMyData;
	m_CreateApplyPartial = !p_IsMyData;
}

void FrameLicenses::ShowLicenses(const O365DataMap<BusinessUser, vector<BusinessLicenseDetail>>& p_LicenseDetails, const vector<BusinessSubscribedSku>& m_SubscribedSkus, bool p_FullPurge)
{
	m_LicensesGrid.BuildTreeView(p_LicenseDetails, m_SubscribedSkus, p_FullPurge);
    UserLicenseModification::ComputeConsumedSkusWithMods(m_LicensesGrid, m_LicensesGrid.ComputeDeltasFromMods());
    m_LicensesGrid.UpdateMegaShark();
}

void FrameLicenses::createGrid()
{
	m_LicensesGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameLicenses::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigUsers();
}

GridFrameBase::O365ControlConfig FrameLicenses::GetApplyPartialControlConfig()
{
	return GetApplyPartialControlConfigUsers();
}

GridFrameBase::O365ControlConfig FrameLicenses::GetRevertPartialControlConfig()
{
	O365ControlConfig config;
	config.m_ControlID = ID_365REVERTSELECTED_USERS;
	config.m_Image16x16ResourceID = IDB_REVERT_SELECTED_USERS;
	config.m_Image32x32ResourceID = IDB_REVERT_SELECTED_USERS_16X16;
	config.m_ControlText = YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_9, _YLOC("Selected Users")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(FrameLicenses_GetRevertPartialControlConfig_2, _YLOC("Undo Selected Users")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(GridFrameBase_AddO365RevertSelectedControl_3, _YLOC("Undo any selected changes currently pending in the grid.\nTip: Grouping by the status column is a quick way to check for all pending changes.\nNote: This button will NOT undo changes that have already been saved to the server.")).c_str();
	return config;
}

// returns false if no frame specific tab is needed.
bool FrameLicenses::customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(p_Tab);
	CreateRefreshGroup(p_Tab);
	if (!m_LicensesGrid.IsMyData())
	{
		CreateApplyGroup(p_Tab);
		CreateRevertGroup(p_Tab);
	}

	auto dataGroup = p_Tab.AddGroup(YtriaTranslate::Do(FrameLicenses_customizeActionsRibbonTab_1, _YLOC("Manage Licenses")).c_str());
	{
		if (!m_LicensesGrid.IsMyData())
		{
			{
				auto ctrl = dataGroup->Add(xtpControlButton, ID_LICENSESGRID_EDITLICENSES);
				setImage({ ID_LICENSESGRID_EDITLICENSES }, IDB_LICENSESGRID_EDITLICENSES, xtpImageNormal);
				setImage({ ID_LICENSESGRID_EDITLICENSES }, IDB_LICENSESGRID_EDITLICENSES_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			{
				auto ctrl = dataGroup->Add(xtpControlButton, ID_LICENSESGRID_EDITUSAGELOCATION);
				setImage({ ID_LICENSESGRID_EDITUSAGELOCATION }, IDB_LICENSESGRID_EDITUSAGELOCATION, xtpImageNormal);
				setImage({ ID_LICENSESGRID_EDITUSAGELOCATION }, IDB_LICENSESGRID_EDITUSAGELOCATION_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}
		}

		{
			auto ctrl = dataGroup->Add(xtpControlButton, ID_LICENSESGRID_SHOWPLANS);
			setImage({ ID_LICENSESGRID_SHOWPLANS }, IDB_LICENSESGRID_SHOWPLANS, xtpImageNormal);
			setImage({ ID_LICENSESGRID_SHOWPLANS }, IDB_LICENSESGRID_SHOWPLANS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = dataGroup->Add(xtpControlButton, ID_LICENSESGRID_SHOWUNASSIGNED);
			setImage({ ID_LICENSESGRID_SHOWUNASSIGNED }, IDB_LICENSESGRID_SHOWUNASSIGNED, xtpImageNormal);
			setImage({ ID_LICENSESGRID_SHOWUNASSIGNED }, IDB_LICENSESGRID_SHOWUNASSIGNED_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	
	ASSERT(GetOrigin() == Origin::User);
	CreateLinkGroups(p_Tab, !m_LicensesGrid.IsMyData(), false);

	automationAddCommandIDToGreenLight(ID_LICENSESGRID_SHOWUNASSIGNED);
	automationAddCommandIDToGreenLight(ID_LICENSESGRID_SHOWPLANS);
	automationAddCommandIDToGreenLight(ID_LICENSESGRID_EDITLICENSES);
	automationAddCommandIDToGreenLight(ID_LICENSESGRID_EDITUSAGELOCATION);

	return true;
}

O365Grid& FrameLicenses::GetGrid()
{
	return m_LicensesGrid;
}

void FrameLicenses::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameLicenses::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameLicenses::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	ASSERT(ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer && Origin::User == GetModuleCriteria().m_Origin);
	if (ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer && Origin::User == GetModuleCriteria().m_Origin)
	{
		info.SetOrigin(Origin::User);
		info.GetIds() = GetModuleCriteria().m_IDs;
	}
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Licenses, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameLicenses::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Licenses, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

GridLicenses& FrameLicenses::GetGridLicenses()
{
    return m_LicensesGrid;
}

const GridLicenses& FrameLicenses::GetGridLicenses() const
{
    return m_LicensesGrid;
}

AutomatedApp::AUTOMATIONSTATUS FrameLicenses::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionShowUnassignedLicenses(p_Action))
	{
		const bool isShowUL		= m_LicensesGrid.IsShowNoLicenseRows();
		const bool wantShowUL	= p_Action->IsParamTrue(AutomationConstant::val().m_ParamNameValue);
		p_CommandID				= ID_LICENSESGRID_SHOWUNASSIGNED; 
		// if already in wanted state, do nothing
		if (wantShowUL == isShowUL)
			statusRV = AutomatedApp::AUTOMATIONSTATUS_EXECUTED;
	}
	else if (IsActionSelectedEditLicense(p_Action))
		p_CommandID = ID_LICENSESGRID_EDITLICENSES;
	else if (IsActionShowServicePlans(p_Action))
	{
		const bool isShowPlans		= m_LicensesGrid.IsShowPlanRows();
		const bool wantShowPlans	= p_Action->IsParamTrue(AutomationConstant::val().m_ParamNameValue);
		p_CommandID = ID_LICENSESGRID_SHOWPLANS;
		// if already in wanted state, do nothing
		if (wantShowPlans == isShowPlans)
			statusRV = AutomatedApp::AUTOMATIONSTATUS_EXECUTED;
	}
	else if (IsActionSelectedEditLicenseLocation(p_Action))
		p_CommandID = ID_LICENSESGRID_EDITUSAGELOCATION;

	return statusRV;
}

void FrameLicenses::revertSelectedImpl()
{
	GetGrid().RevertSelectedRows(O365Grid::RevertFlags::UPDATE_GRID_IF_NEEDED | O365Grid::RevertFlags::ONLY_ASSOCIATED_TOP_LEVEL_ROWS);
}

void FrameLicenses::ApplySpecific(bool p_Selected)
{
	O365IdsContainer ids;

    if (!p_Selected)
    {
        // Get ids for all users
        for (auto mod : m_LicensesGrid.GetModifications().GetRowModifications([](RowModification*) { return true; }))
        {
            auto licenseMod = dynamic_cast<UserLicenseModification*>(mod);
            ASSERT(nullptr != licenseMod);
            if (nullptr != licenseMod)
                ids.insert(licenseMod->GetUserId());
        }
    }
    else
    {
        // Get ids for selected users
        for (auto row : m_LicensesGrid.GetSelectedRows())
        {
			if (!row->IsGroupRow())
			{
				auto userRow = row->GetTopAncestorOrThis();
				if (userRow->IsModified()) // FIXME: Only works while we make sure row status is synced to RowModifications.
				{
					auto& field = userRow->GetField(m_LicensesGrid.GetColMetaId());
					if (field.HasValue())
						ids.insert(field.GetValueStr());
				}
			}
        }
    }

    CommandInfo info;
    info.Data() = ids;
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Licenses, Command::ModuleTask::ApplyChanges, info, { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
}
