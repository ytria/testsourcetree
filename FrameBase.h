#pragma once

#include "CodeJockFrameBase.h"
#include "DlgFeedback.h"
#include "IRibbonSystemMenuBuilder.h"
#include "PersistentSession.h"
#include "Sapio365Session.h"
#include "Resource.h"
#include "ResProduct.h"
#include "YObserver.h"

class CXTPRibbonBar;

class AutomationAction;

template <class CodeJockFrameClass>
class FrameBase : public CodeJockFrameClass
{
public:
	using CodeJockFrameClass::CodeJockFrameClass;
	virtual ~FrameBase() override;

	void Init();

	const wstring&	GetAutomationName() const override;
	void			SetAutomationName(const wstring& p_AutomationName);

	static bool CanCreateNewFrame(bool warnIfNot, CWnd* parentForDlg, AutomationAction* p_Action, bool p_HasFreeIdForNewFrame = hasFreeIdForNewFrame());

	bool showBackstageTab(UINT tabID);

	std::shared_ptr<Sapio365Session> GetSapio365Session() const;
	void SetSapio365Session(const std::shared_ptr<Sapio365Session>& p_O365GraphSession);
	bool IsConnected() const;
	bool HasSession() const;

	const PersistentSession& GetSessionInfo() const;

	bool IsSameTenant(const std::shared_ptr<Sapio365Session>& p_OtherSession) const;	

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg void OnClose();
	afx_msg void OnLogout();

	PersistentSession& getSessionInfo(); // only for GridFrameBase::OnToggleRemoveRBACOutOfScope()

	constexpr UINT getUserPictureID() const;
	constexpr UINT getEditUAorElevatedID() const;

	void ShowTokenMenu(bool p_Show);

	afx_msg void OnToggleRibbon();
	afx_msg void OnUpdateRibbonMinimize(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRibbonExpand(CCmdUI* pCmdUI);

	afx_msg void OnRibbonHelp();
	afx_msg void OnUpdateRibbonHelp(CCmdUI* pCmdUI);

	afx_msg void OnUpdateTokenManager(CCmdUI* pCmdUI);
	afx_msg void OnTokenRefresh();
	afx_msg void OnUpdateTokenRefresh(CCmdUI* pCmdUI);
	afx_msg void OnTokenLastConsume();
	afx_msg void OnUpdateTokenLastConsume(CCmdUI* pCmdUI);
	afx_msg void OnTokenBuy();
	afx_msg void OnUpdateTokenBuy(CCmdUI* pCmdUI);

	afx_msg void OnCustomizeQuickAccess();

	afx_msg void OnUpdateLogout(CCmdUI* pCmdUI);
	afx_msg void OnSeeRoleInfo();
	afx_msg void OnUpdateSeeRoleInfo(CCmdUI* pCmdUI);
	afx_msg void OnSendFeedbackHappy();
	afx_msg void OnUpdateSendFeedbackHappy(CCmdUI* pCmdUI);
	afx_msg void OnSendFeedbackAngry();
	afx_msg void OnUpdateSendFeedbackAngry(CCmdUI* pCmdUI);
	afx_msg void OnSendFeedbackQuestion();
	afx_msg void OnUpdateSendFeedbackQuestion(CCmdUI* pCmdUI);

	afx_msg void OnWindowPosChanged(WINDOWPOS* lpwndpos);

	afx_msg LRESULT OnRibbonContextMenu(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageBarClosed(WPARAM wParam, LPARAM lParam);

	virtual BOOL OnFrameLoaded() override;
	virtual void OnThemeWasSet();

	virtual bool customizeStatusBar() override;

	void setMenuBuilder(unique_ptr<IRibbonSystemMenuBuilder> menuBuilder)
	{
		m_menuBuilder = std::move(menuBuilder);
	}

	virtual CXTPRibbonBar* createRibbonBar() override;

	void setImage(const vector<UINT> commandIDs, UINT resourceID, XTPImageState imageState/* = xtpImageNormal*/);
	void setGroupImage(CXTPRibbonGroup& p_Group, UINT resourceID);
	CXTPRibbonGroup* findGroup(CXTPRibbonTab& tab, const CString& name);

	CXTPRibbonGroup* addSessionGroupToRibbonTab(CXTPRibbonTab& tab, bool addConnectedInfo);
	CXTPRibbonTab* addFeedbackRibbonTab(CXTPRibbonBar& p_RibbonBar);

	virtual void updateSessionGroup(CXTPRibbonGroup& sessionGroup);
	virtual void updateConnectedControl(CXTPControl& connectedControl);

	static void setControlTexts(CXTPControl* p_Control, const wstring& p_Caption, const wstring& p_TooltipTitle, const wstring& p_Description);

	void setDoNotRestorePositionAndSize(bool doNot);

	virtual bool sessionWasSet(bool p_SessionHasChanged) = 0; // Must only be called by FrameBase::SetSapio365Session  (exception made for GridFrameBase::OnFrameLoaded())
	virtual void logout() = 0;

	static bool hasFreeIdForNewFrame();

	void temporaryChangeStatusbarBackgroundColor(bool p_Condition, const COLORREF& p_TempColor);
	void temporarySetStatusbarLeftText(bool p_Condition, const wstring& p_Text, const wstring& p_Tooltip);

	// p_Tooltip is not handled yet
	bool showMessageBar(const CString& p_xamlContent, /*const CString& p_Tooltip,*/ const std::unordered_map<UINT, CString>& buttons/* = { {IDOK, _YTEXT("OK")} }*/, const COLORREF p_CustomBackgroundColor = COLORREF(-1));
	void removeMessageBar();

	void updateLogoutControlTexts();
	void updateEditElevatedButton();

	CXTPControl* getCtrlEditElevated();

	bool IsMessageBarOnScreen();

private:
	void sendFeedback(DlgFeedback::MOOD mood);

protected:
	static wstring	g_InfoSessionGroup;
	static wstring	g_FeedbackTabName;

private:
	unique_ptr<IRibbonSystemMenuBuilder>	m_menuBuilder;
	std::shared_ptr<Sapio365Session>		m_sapio365Session;
	PersistentSession  						m_SessionInfo;
	std::unique_ptr<CXTPMessageBar>			m_MessageBar;

	static bool g_staticInit;
	static void initStaticStrings();

	const UINT m_userPictureId = initUserPictureID();
	const UINT m_EditUAorElevatedID = initEditUAorElevatedID();

	static UINT initUserPictureID();
	static UINT initEditUAorElevatedID();

	bool m_ShouldRestorePositionAndSize = true;
	void storePositionAndSize();
	void restorePositionAndSize();
	virtual wstring getPositionAndSizeRegistrySectionName() const = 0;

	wstring m_AutomationName;

	CXTPControl* m_LogOutControl = nullptr;
	CXTPControl* m_CtrlEditElevated = nullptr;
	CXTPControl* m_TokenManagerControl = nullptr;
	CXTPControl* m_TokenStateControl = nullptr;

	static const wstring g_WindowPlacementFormat;
	static const wstring g_PositionAndSizeRegistryKey;
};

class ColoredMessageBar : public CXTPMessageBar
{
public:
	using CXTPMessageBar::CXTPMessageBar;

	void SetBackgroundColor(COLORREF p_Color);

protected:
	void FillMessageBar(CDC* pDC) override;
	//void DrawContent(CDC* pDC) override;
	//void DrawButton(CDC* pDC, CXTPMessageBarButton* pButton) override;

private:
	COLORREF m_clrBack = COLORREF(-1);
};

class FixedSizeControlBitmap : public CXTPControlBitmap
{
	DECLARE_XTP_CONTROL(FixedSizeControlBitmap);

public:
	FixedSizeControlBitmap(const CSize& p_Size);

protected: // For clone
	FixedSizeControlBitmap();

protected:
	virtual CSize GetSize(CDC* pDC) override;
	virtual void Draw(CDC* pDC) override;
	virtual void Copy(CXTPControl* pControl, BOOL bRecursive = FALSE) override;

private:
	CSize m_Size;
};



#include "FrameBase.hpp"