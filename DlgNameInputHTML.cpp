#include "DlgNameInputHTML.h"

#include "AutomationNames.h"

DlgNameInputHTML::DlgNameInputHTML(const wstring& p_AutomationName, const wstring& p_InputLabel, CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::CREATE, p_Parent, p_AutomationName)
	, m_Label(p_InputLabel)
{

}

void DlgNameInputHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgNameInputHTML"));

	getGenerator().Add(StringEditor(_YTEXT("name"), m_Label, EditorFlags::REQUIRED | EditorFlags::DIRECT_EDIT, m_NewName));

	getGenerator().addApplyButton(LocalizedStrings::g_CreateButtonText);
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
}

bool DlgNameInputHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	for (auto& d : data)
	{
		if (d.first == _YTEXT("name"))
			m_NewName = d.second;
	}
	return true;
}

const wstring& DlgNameInputHTML::GetNewName() const
{
	return m_NewName;
}

void DlgNameInputHTML::SetDefaultName(const wstring& p_Name)
{
	m_NewName = p_Name;
}

// =========================================================================================================

DlgCreateFolderHTML::DlgCreateFolderHTML(CWnd* p_Parent)
	: DlgNameInputHTML(g_ActionNameSelectedCreateFolder, _T("Folder Name"), p_Parent)
	//, m_LetGraphRenameOnConflict(true)
{

}

void DlgCreateFolderHTML::generateJSONScriptData()
{
	DlgNameInputHTML::generateJSONScriptData();

	//addBoolToggleEditor(_YTEXT("behavior")
	//	, _T("Behavior on conflict")
	//	, _T("Decide whether the remote service should automatically choose a new name for the folder in case an item already exists with the same name.")
	//	, EditorFlags::REQUIRED | EditorFlags::DIRECT_EDIT
	//	, m_LetGraphRenameOnConflict
	//	, { _T("Rename"), _T("Fail") });
}

bool DlgCreateFolderHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	DlgNameInputHTML::processPostedData(data);
	//for (auto& d : data)
	//{
	//	if (d.first == _YTEXT("behavior"))
	//		m_LetGraphRenameOnConflict = Str::getBoolFromString(d.second);
	//}
	return true;
}

//bool DlgCreateFolderHTML::ShouldRenameOnConflict() const
//{
//	return m_LetGraphRenameOnConflict;
//}
