#pragma once

#include "FieldUpdate.h"

class SisterhoodFieldUpdate : public FieldUpdateO365
{
public:
	SisterhoodFieldUpdate(O365Grid& p_Grid,
		const row_pk_t& p_RowKey,
		const UINT& p_ColumnId,
		const GridBackendField& p_OldValue,
		const GridBackendField& p_NewValue,
		const std::map<UINT, const GridBackendField*>& p_SisterOldValues,
		uint32_t p_Flags = FieldUpdateFlags::None);

	~SisterhoodFieldUpdate() override;

	virtual void Revert() override;
	virtual std::unique_ptr<FieldUpdateO365> CreateReplacement(std::unique_ptr<FieldUpdateO365> p_Replacement) override;

private:
	std::map<UINT, boost::YOpt<GridBackendField>> m_SisterOldValues;
};
