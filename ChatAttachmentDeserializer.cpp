#include "ChatAttachmentDeserializer.h"
#include "IdentitySetDeserializer.h"
#include "ItemBodyDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "RecipientDeserializer.h"

void ChatAttachmentDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
	JsonSerializeUtil::DeserializeString(_YTEXT("contentType"), m_Data.m_ContentType, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("contentUrl"), m_Data.m_ContentUrl, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("content"), m_Data.m_Content, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("thumbnailUrl"), m_Data.m_ThumbnailUrl, p_Object);
}
