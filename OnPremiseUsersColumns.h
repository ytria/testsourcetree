#pragma once

#include "OnPremiseUser.h"

class O365Grid;

class OnPremiseUsersColumns
{
public:
	bool Create(O365Grid& p_Grid);

	static const wstring& GetTitle(const wstring& p_YUID);
	static boost::YOpt<std::vector<PooledString>> ToFriendyNames(const boost::YOpt<std::vector<PooledString>>& p_DistinguishedNames);
	static PooledString ToFriendyName(const PooledString& p_DistinguishedName);
	static boost::YOpt<PooledString> ToFriendyName(const boost::YOpt<PooledString>& p_DistinguishedName);

	static PooledString GetOrganizationalUnit(const PooledString& p_DistinguishedName);
	static boost::YOpt<PooledString> GetOrganizationalUnit(const boost::YOpt<PooledString>& p_DistinguishedName);

	void SetValue(OnPremiseUser& p_OnPremUser, GridBackendColumn* p_Col, const GridBackendField& p_Field) const;

	vector<GridBackendField> GetFields(GridBackendRow& p_Row);
	void SetBlankFields(GridBackendRow& p_Row);

	const vector<GridBackendColumn*>& GetColumns() const;
	const vector<GridBackendColumn*>& GetCommonColumns() const;

	GridBackendColumn* m_ColCommonDisplayName = nullptr;
	GridBackendColumn* m_ColCommonUserName = nullptr;
	GridBackendColumn* m_ColCommonMetaType = nullptr;

	GridBackendColumn* m_ColAccountExpires = nullptr;
	GridBackendColumn* m_ColAccountLockoutTime = nullptr;
	GridBackendColumn* m_ColAccountNotDelegated = nullptr;
	GridBackendColumn* m_ColAdminCount = nullptr;
	GridBackendColumn* m_ColAllowReversiblePasswordEncryption = nullptr;
	GridBackendColumn* m_ColAuthenticationPolicy = nullptr;
	GridBackendColumn* m_ColAuthenticationPolicySilo = nullptr;
	GridBackendColumn* m_ColBadLogonCount = nullptr;
	GridBackendColumn* m_ColBadPasswordTime = nullptr;
	GridBackendColumn* m_ColBadPwdCount = nullptr;
	GridBackendColumn* m_ColCannotChangePassword = nullptr;
	GridBackendColumn* m_ColCanonicalName = nullptr;
	GridBackendColumn* m_ColCertificates = nullptr;
	GridBackendColumn* m_ColCity = nullptr;
	GridBackendColumn* m_ColCompoundIdentitySupported = nullptr;
	GridBackendColumn* m_ColCN = nullptr;
	GridBackendColumn* m_ColCodePage = nullptr;
	GridBackendColumn* m_ColCompany = nullptr;
	GridBackendColumn* m_ColCountry = nullptr;
	GridBackendColumn* m_ColCountryCode = nullptr;
	GridBackendColumn* m_ColCreated = nullptr;
	GridBackendColumn* m_ColDeleted = nullptr;
	GridBackendColumn* m_ColDepartment = nullptr;
	GridBackendColumn* m_ColDescription = nullptr;
	GridBackendColumn* m_ColDisplayName = nullptr;
	GridBackendColumn* m_ColDistinguishedName = nullptr;
	GridBackendColumn* m_ColOrganizationalUnit = nullptr;
	GridBackendColumn* m_ColDivision = nullptr;
	GridBackendColumn* m_ColDoesNotRequirePreAuth = nullptr;
	GridBackendColumn* m_ColDSCorePropagationData = nullptr;
	GridBackendColumn* m_ColEmailAddress = nullptr;
	GridBackendColumn* m_ColEmployeeID = nullptr;
	GridBackendColumn* m_ColEmployeeNumber = nullptr;
	GridBackendColumn* m_ColEnabled = nullptr;
	GridBackendColumn* m_ColFax = nullptr;
	GridBackendColumn* m_ColGivenName = nullptr;
	GridBackendColumn* m_ColHomeDirectory = nullptr;
	GridBackendColumn* m_ColHomedirRequired = nullptr;
	GridBackendColumn* m_ColHomeDrive = nullptr;
	GridBackendColumn* m_ColHomePage = nullptr;
	GridBackendColumn* m_ColHomePhone = nullptr;
	GridBackendColumn* m_ColInitials = nullptr;
	GridBackendColumn* m_ColInstanceType = nullptr;
	GridBackendColumn* m_ColIsCriticalSystemObject = nullptr;
	GridBackendColumn* m_ColIsDeleted = nullptr;
	GridBackendColumn* m_ColKerberosEncryptionType = nullptr;
	GridBackendColumn* m_ColLastBadPasswordAttempt = nullptr;
	GridBackendColumn* m_ColLastKnownParent = nullptr;
	GridBackendColumn* m_ColLastLogoff = nullptr;
	GridBackendColumn* m_ColLastLogon = nullptr;
	GridBackendColumn* m_ColLastLogonDate = nullptr;
	GridBackendColumn* m_ColLastLogonTimestamp = nullptr;
	GridBackendColumn* m_ColLockedOut = nullptr;
	GridBackendColumn* m_ColLogonCount = nullptr;
	GridBackendColumn* m_ColLogonHours = nullptr;
	GridBackendColumn* m_ColLogonWorkstations = nullptr;
	GridBackendColumn* m_ColManager = nullptr;
	GridBackendColumn* m_ColManagerFriendly = nullptr;
	GridBackendColumn* m_ColMemberOf = nullptr;
	GridBackendColumn* m_ColMemberOfFriendly = nullptr;
	GridBackendColumn* m_ColMNSLogonAccount = nullptr;
	GridBackendColumn* m_ColMobilePhone = nullptr;
	GridBackendColumn* m_ColModified = nullptr;
	GridBackendColumn* m_ColMSDSConsistencyGuid = nullptr;
	GridBackendColumn* m_ColMSDSUserAccountControlComputed = nullptr;
	GridBackendColumn* m_ColName = nullptr;
	GridBackendColumn* m_ColNTSecurityDescriptor = nullptr;
	GridBackendColumn* m_ColObjectCategory = nullptr;
	GridBackendColumn* m_ColObjectClass = nullptr;
	GridBackendColumn* m_ColObjectGUID = nullptr;
	GridBackendColumn* m_ColObjectSid = nullptr;
	GridBackendColumn* m_ColOffice = nullptr;
	GridBackendColumn* m_ColOfficePhone = nullptr;
	GridBackendColumn* m_ColOrganization = nullptr;
	GridBackendColumn* m_ColOtherName = nullptr;
	GridBackendColumn* m_ColPasswordExpired = nullptr;
	GridBackendColumn* m_ColPasswordLastSet = nullptr;
	GridBackendColumn* m_ColPasswordNeverExpires = nullptr;
	GridBackendColumn* m_ColPasswordNotRequired = nullptr;
	GridBackendColumn* m_ColPOBox = nullptr;
	GridBackendColumn* m_ColPostalCode = nullptr;
	GridBackendColumn* m_ColPrimaryGroup = nullptr;
	GridBackendColumn* m_ColPrimaryGroupId = nullptr;
	GridBackendColumn* m_ColPrincipalsAllowedToDelegateToAccount = nullptr;
	GridBackendColumn* m_ColProfilePath = nullptr;
	GridBackendColumn* m_ColProtectedFromAccidentalDeletion = nullptr;
	GridBackendColumn* m_ColProxyAddresses = nullptr;
	GridBackendColumn* m_ColPwdLastSet = nullptr;
	GridBackendColumn* m_ColSamAccountName = nullptr;
	GridBackendColumn* m_ColSAMAccountType = nullptr;
	GridBackendColumn* m_ColScriptPath = nullptr;
	GridBackendColumn* m_ColServicePrincipalNames = nullptr;
	GridBackendColumn* m_ColSDRightsEffective = nullptr;
	GridBackendColumn* m_ColSID = nullptr;
	GridBackendColumn* m_ColSIDHistory = nullptr;
	GridBackendColumn* m_ColSmartcardLogonRequired = nullptr;
	GridBackendColumn* m_ColState = nullptr;
	GridBackendColumn* m_ColStreetAddress = nullptr;
	GridBackendColumn* m_ColSurname = nullptr;
	GridBackendColumn* m_ColTitle = nullptr;
	GridBackendColumn* m_ColTrustedForDelegation = nullptr;
	GridBackendColumn* m_ColTrustedToAuthForDelegation = nullptr;
	GridBackendColumn* m_ColUseDESKeyOnly = nullptr;
	GridBackendColumn* m_ColUserAccountControl = nullptr;
	GridBackendColumn* m_ColUserCertificate = nullptr;
	GridBackendColumn* m_ColUserPrincipalName = nullptr;
	GridBackendColumn* m_ColUSNChanged = nullptr;
	GridBackendColumn* m_ColUSNCreated = nullptr;
	GridBackendColumn* m_ColWhenChanged = nullptr;
	GridBackendColumn* m_ColWhenCreated = nullptr;
	GridBackendColumn* m_ColImmutableId = nullptr;

private:
	vector<GridBackendColumn*> m_Columns;
	vector<GridBackendColumn*> m_CommonColumns;
	bool m_Created = false;

	std::map<GridBackendColumn*, std::function<void(OnPremiseUser&, const boost::YOpt<PooledString>&)>> m_StringSetters;
	std::map<GridBackendColumn*, std::function<void(OnPremiseUser&, const boost::YOpt<bool>&)>> m_BoolSetters;
	std::map<GridBackendColumn*, std::function<void(OnPremiseUser&, const boost::YOpt<YTimeDate>&)>> m_DateSetters;
	std::map<GridBackendColumn*, std::function<void(OnPremiseUser&, const boost::YOpt<int32_t>&)>> m_IntSetters;
	std::map<GridBackendColumn*, std::function<void(OnPremiseUser&, const boost::YOpt<int64_t>&)>> m_Int64Setters;
	std::map<GridBackendColumn*, std::function<void(OnPremiseUser&, const boost::YOpt<vector<PooledString>>&)>> m_StringListSetters;
	std::map<GridBackendColumn*, std::function<void(OnPremiseUser&, const boost::YOpt<vector<YTimeDate>>&)>> m_DateListSetters;
};

