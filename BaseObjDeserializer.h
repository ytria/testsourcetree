#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "BaseObj.h"

namespace Cosmos
{
    class BaseObjDeserializer : public JsonObjectDeserializer, public Encapsulate<Cosmos::BaseObj>
    {
    public:
        void DeserializeObject(const web::json::object& p_Object) override;
        static void Deserialize(const web::json::object& p_Object, Cosmos::BaseObj& p_BaseObj);
    };
}

