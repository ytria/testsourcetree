#include "MailboxRequester.h"

#include "IPowerShell.h"
#include "IPSObjectCollectionDeserializer.h"
#include "IRequestLogger.h"
#include "MailboxPermissionsCollectionDeserializer.h"
#include "Sapio365Session.h"

MailboxRequester::MailboxRequester(const std::shared_ptr<IPSObjectCollectionDeserializer>& p_Deserializer, const wstring& p_UserPrincipalName, const std::shared_ptr<IRequestLogger>& p_RequestLogger)
	: m_Deserializer(p_Deserializer),
	m_UserPrincipalName(p_UserPrincipalName),
	m_Logger(p_RequestLogger),
	m_Result(std::make_shared<InvokeResultWrapper>())
{
	if (!m_Deserializer)
		throw std::exception("MailboxRequester: Deserializer cannot be nullptr");
}

TaskWrapper<void> MailboxRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	if (!p_Session->HasExchangePowerShellSession())
		return YSafeCreateTask([]() {});

	return YSafeCreateTaskMutable([result = m_Result, deserializer = m_Deserializer, session = p_Session->GetExchangePowershellSession(), userPrincipalName = m_UserPrincipalName, logger = m_Logger, taskData = p_TaskData]() mutable
	{
		if (nullptr != session)
		{
			logger->Log(taskData.GetOriginator());

			wstring script(_YTEXT("Get-Mailbox "));
			script += userPrincipalName;
			session->AddScript(script.c_str(), taskData.GetOriginator());

			result->SetResult(session->Invoke(taskData.GetOriginator()));
			taskData.SetLastRequestOn(YTimeDate::GetCurrentTimeDate());
			if (result->Get().m_Success && nullptr != result->Get().m_Collection)
				result->Get().m_Collection->Deserialize(*deserializer);
		}
	});
}

const std::shared_ptr<InvokeResultWrapper>& MailboxRequester::GetResult() const
{
	return m_Result;
}
