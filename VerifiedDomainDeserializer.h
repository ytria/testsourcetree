#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "VerifiedDomain.h"

class VerifiedDomainDeserializer : public JsonObjectDeserializer, public Encapsulate<VerifiedDomain>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

