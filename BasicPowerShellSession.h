#pragma once

#include "InvokeResult.h"
#include "InvokeResultWrapper.h"
#include <memory>

class IPowerShell;

class BasicPowerShellSession
{
public:
	BasicPowerShellSession(bool p_ExecutionPolicyUnrestricted);

	virtual void AddScript(const wchar_t* p_Script, HWND p_Originator);
	void AddScriptStopOnError(const wchar_t* p_Script, HWND p_Originator);

	InvokeResult Invoke(HWND p_Originator);

	const std::shared_ptr<IPowerShell>& GetPowerShell() const;

	static bool IsHostCompatible();

protected:
	std::shared_ptr<IPowerShell> m_PowerShell;
	wstring m_BufferedScript;
};

