#include "GroupDeserializerFactory.h"
#include "Sapio365Session.h"

GroupDeserializerFactory::GroupDeserializerFactory(std::shared_ptr<const Sapio365Session> p_Session)
	: m_Session(p_Session)
{
}

GroupDeserializer GroupDeserializerFactory::Create()
{
    return GroupDeserializer(m_Session);
}
