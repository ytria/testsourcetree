#include "UpdatePrivateChannelMemberRequester.h"

#include "O365AdminUtil.h"
#include "MsGraphFieldNames.h"
#include "MsGraphHttpRequestLogger.h"

UpdatePrivateChannelMemberRequester::UpdatePrivateChannelMemberRequester(BusinessAADUserConversationMember p_Member, std::wstring p_TeamId, std::wstring p_ChannelId)
	: m_Member(p_Member)
	, m_TeamId(p_TeamId)
	, m_ChannelId(p_ChannelId)
{

}

TaskWrapper<void> UpdatePrivateChannelMemberRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	web::uri_builder uri(_YTEXT("teams"));
	uri.append_path(m_TeamId);
	uri.append_path(_YTEXT("channels"));
	uri.append_path(m_ChannelId);
	uri.append_path(_YTEXT("members"));
	uri.append_path(m_Member.GetID());

	auto body = web::json::value::object();
	body[_YTEXT("@odata.type")] = web::json::value::string(_YTEXT("#microsoft.graph.aadUserConversationMember"));
	// We only handle roles here!
	{
		auto array = web::json::value::array();
		if (m_Member.GetRoles())
		{
			int i = 0;
			for (auto& role : *m_Member.GetRoles())
			{
				if (!role.IsEmpty())
					array[i++] = web::json::value::string(role.c_str());
			}
		}
		body[_YTEXT("roles")] = array;
	}

	// Remove this bool eventually
	const bool useBeta = true;

	LoggerService::User(_YFORMAT(L"Updating member with id %s to channel id %s (group id %s)", m_Member.GetUserID()->c_str(), m_ChannelId.c_str(), m_TeamId.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Patch(uri.to_uri(), httpLogger, p_TaskData, WebPayloadJSON(body), useBeta ? _YTEXT("beta") : _YTEXT("")).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResInfo) {
		*result = Util::GetResult(p_ResInfo);
	});
}

const HttpResultWithError& UpdatePrivateChannelMemberRequester::GetResult() const
{
	ASSERT(m_Result);
	return *m_Result;
}
