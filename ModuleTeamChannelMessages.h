#pragma once

#include "BusinessChatMessage.h"
#include "BusinessChannel.h"
#include "BusinessGroup.h"
#include "ModuleBase.h"
#include "ModuleUtil.h"

class ModuleTeamChannelMessages : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

public:
    virtual void executeImpl(const Command& p_Command) override;

private:
	void showMessages(const Command& p_Command);

    void showAttachments(const Command& p_Command);
    void downloadAttachments(const Command& p_Command);
    void showItemAttachment(const Command& p_Command);
    void deleteAttachments(const Command& p_Command);

	O365DataMap<BusinessGroup, std::map<BusinessChannel, vector<BusinessChatMessage>>> retrieveMessages(std::shared_ptr<BusinessObjectManager> p_BOM, const O365SubIdsContainer& p_TeamChannelIds, std::map<PooledString, PooledString> p_ChannelNames, YtriaTaskData p_TaskData);

    //static vector<ModuleUtil::PostMetaInfo> SubSubIdsToPostMetaInfos(const map<PooledString, map<PooledString, set<PooledString>>>& p_SubSubIds);
};

