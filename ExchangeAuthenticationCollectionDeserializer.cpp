#include "ExchangeAuthenticationCollectionDeserializer.h"

#include "IDateTimeOffset.h"
#include "YtriaPowerShellDll.h"

ExchangeAuthenticationCollectionDeserializer::ExchangeAuthenticationCollectionDeserializer()
	:m_TokenExpiration(0)
{
}

void ExchangeAuthenticationCollectionDeserializer::Deserialize(IPSObjectPropertyReader& p_Reader)
{
	if (p_Reader.HasProperty(_YTEXT("ExpiresOn")))
	{
		auto expiresOn = p_Reader.GetDateTimeOffsetProperty(_YTEXT("ExpiresOn"));
		if (nullptr != expiresOn)
		{
			m_TokenExpiration = expiresOn->ToUnixTimeSeconds();
			YtriaPowerShellDll::Get().ReleaseDateTimeOffset(expiresOn);
		}
	}
}

void ExchangeAuthenticationCollectionDeserializer::SetCount(size_t p_Count)
{

}

int64_t ExchangeAuthenticationCollectionDeserializer::GetTokenExpiration() const
{
	return m_TokenExpiration;
}

