#include "OptionalClaim.h"
#include "OptionalClaimsDeserializer.h"
#include "ListDeserializer.h"
#include "OptionalClaimDeserializer.h"

void OptionalClaimsDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		ListDeserializer<OptionalClaim, OptionalClaimDeserializer> listDeserializer;
		if (JsonSerializeUtil::DeserializeAny(listDeserializer, _YTEXT("idToken"), p_Object))
			m_Data.m_IdToken = std::move(listDeserializer.GetData());
	}

	{
		ListDeserializer<OptionalClaim, OptionalClaimDeserializer> listDeserializer;
		if (JsonSerializeUtil::DeserializeAny(listDeserializer, _YTEXT("accessToken"), p_Object))
			m_Data.m_AccessToken = std::move(listDeserializer.GetData());
	}

	{
		ListDeserializer<OptionalClaim, OptionalClaimDeserializer> listDeserializer;
		if (JsonSerializeUtil::DeserializeAny(listDeserializer, _YTEXT("saml2Token"), p_Object))
			m_Data.m_AccessToken = std::move(listDeserializer.GetData());
	}
}
