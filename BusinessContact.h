#pragma once

#include "BusinessObject.h"
#include "Contact.h"
#include <vector>

class BusinessContact : public BusinessObject
{
public:
    BusinessContact();
    BusinessContact(const Contact& p_Contact);

	boost::YOpt<PooledString>	m_DisplayName;
	boost::YOpt<PooledString>	m_Surname;
	boost::YOpt<PooledString>	m_GivenName;
	boost::YOpt<PooledString>	m_MiddleName;
	boost::YOpt<PooledString>	m_Manager;
	boost::YOpt<PooledString>	m_JobTitle;
	boost::YOpt<PooledString>	m_MobilePhone;
	boost::YOpt<PooledString>	m_AssistantName;
	boost::YOpt<YTimeDate>		m_Birthday;
	boost::YOpt<PooledString>	m_BusinessHomePage;
	boost::YOpt<PooledString>	m_ChangeKey;
	boost::YOpt<PooledString>	m_CompanyName;
	boost::YOpt<YTimeDate>		m_CreatedDateTime;
	boost::YOpt<PooledString>	m_Department;
	boost::YOpt<PooledString>	m_FileAs;
	boost::YOpt<PooledString>	m_Generation;
	boost::YOpt<PooledString>	m_Initials;
	boost::YOpt<YTimeDate>		m_LastModifiedDateTime;
	boost::YOpt<PooledString>	m_Nickname;
	boost::YOpt<PooledString>	m_OfficeLocation;
	boost::YOpt<PooledString>	m_ParentFolderId;
	boost::YOpt<PooledString>	m_PersonalNotes;
	boost::YOpt<PooledString>	m_Profession;
	boost::YOpt<PooledString>	m_SpouseName;
	boost::YOpt<PooledString>	m_Title;
	boost::YOpt<PooledString>	m_YomiCompanyName;
	boost::YOpt<PooledString>	m_YomiGivenName;
	boost::YOpt<PooledString>	m_YomiSurnameName;

	vector<PooledString>	m_EmailAddressesAddress;
	vector<PooledString>	m_EmailAddressesName;
	vector<PooledString>	m_EmailAddressesAndName;

	vector<PooledString>	m_HomePhones;
	vector<PooledString>	m_ImAddresses;
	vector<PooledString>	m_Children;
	vector<PooledString>	m_BusinessPhones;
	vector<PooledString>	m_Categories;

	boost::YOpt<PooledString>	m_BusinessAddressCity;
	boost::YOpt<PooledString>	m_BusinessAddressCountryOrRegion;
	boost::YOpt<PooledString>	m_BusinessAddressPostalCode;
	boost::YOpt<PooledString>	m_BusinessAddressState;
	boost::YOpt<PooledString>	m_BusinessAddressStreet;

	boost::YOpt<PooledString>	m_OtherAddressCity;
	boost::YOpt<PooledString>	m_OtherAddressCountryOrRegion;
	boost::YOpt<PooledString>	m_OtherAddressPostalCode;
	boost::YOpt<PooledString>	m_OtherAddressState;
	boost::YOpt<PooledString>	m_OtherAddressStreet;

	boost::YOpt<PooledString>	m_HomeAddressCity;
	boost::YOpt<PooledString>	m_HomeAddressCountryOrRegion;
	boost::YOpt<PooledString>	m_HomeAddressPostalCode;
	boost::YOpt<PooledString>	m_HomeAddressState;
	boost::YOpt<PooledString>	m_HomeAddressStreet;

    // Non-property members
    boost::YOpt<PooledString>  m_ContactFolderName;

    static Contact ToContact(const BusinessContact& p_BusinessContact);

    RTTR_ENABLE(BusinessObject)
};

