#pragma once

#include "IActionCommand.h"

class FrameUsers;

class ShowOnPremUsersCommand : public IActionCommand
{
public:
	ShowOnPremUsersCommand(FrameUsers& p_Frame);

	void ExecuteImpl() const override;

private:
	FrameUsers& m_Frame;
};

