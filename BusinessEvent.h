#pragma once

#include "BusinessObject.h"

class Event;

class BusinessEvent : public BusinessObject
{
public:
    BusinessEvent() = default;
    BusinessEvent(const Event& p_Event);

    const vector<Attendee>& GetAttendees() const;
    const boost::YOpt<ItemBody>& GetBody() const;
    const boost::YOpt<PooledString>& GetBodyPreview() const;
    const boost::YOpt<vector<PooledString>>& GetCategories() const;
    const boost::YOpt<PooledString>& GetChangeKey() const;
    const boost::YOpt<YTimeDate>& GetCreatedDateTime() const;
    const boost::YOpt<DateTimeTimeZone>& GetEnd() const;
    const boost::YOpt<bool>& GetHasAttachments() const;
    const boost::YOpt<PooledString>& GetICalUId() const;
    const boost::YOpt<PooledString>& GetImportance() const;
    const boost::YOpt<bool>& GetIsAllDay() const;
    const boost::YOpt<bool>& GetIsCancelled() const;
    const boost::YOpt<bool>& GetIsOrganizer() const;
    const boost::YOpt<bool>& GetIsReminderOn() const;
    const boost::YOpt<YTimeDate>& GetLastModifiedDateTime() const;
    const boost::YOpt<Location>& GetLocation() const;
    const boost::YOpt<PooledString>& GetOnlineMeetingUrl() const;
    const boost::YOpt<Recipient>& GetOrganizer() const;
    const boost::YOpt<PooledString>& GetOriginalEndTimeZone() const;
    const boost::YOpt<YTimeDate>& GetOriginalStart() const;
    const boost::YOpt<PooledString>& GetOriginalStartTimeZone() const;
    const boost::YOpt<PatternedRecurrence>& GetRecurrence() const;
    const boost::YOpt<int32_t>& GetReminderMinutesBeforeStart() const;
    const boost::YOpt<bool>& GetResponseRequested() const;
    const boost::YOpt<ResponseStatus>& GetResponseStatus() const;
    const boost::YOpt<PooledString>& GetSensitivity() const;
    const boost::YOpt<PooledString>& GetSeriesMasterId() const;
    const boost::YOpt<PooledString>& GetShowAs() const;
    const boost::YOpt<DateTimeTimeZone>& GetStart() const;
    const boost::YOpt<PooledString>& GetSubject() const;
    const boost::YOpt<PooledString>& GetType() const;
    const boost::YOpt<PooledString>& GetWebLink() const;


    void SetAttendees(vector<Attendee> p_Attendees);
    void SetBody(const boost::YOpt<ItemBody>& p_Body);
    void SetBodyPreview(const boost::YOpt<PooledString>& p_BodyPreview);
    void SetCategories(const boost::YOpt<vector<PooledString>>& p_Categories);
    void SetChangeKey(const boost::YOpt<PooledString>& p_ChangeKey);
    void SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_CreatedDateTime);
    void SetCreatedDateTime(const boost::YOpt<PooledString>& p_CreatedDateTime);
    void SetEnd(const boost::YOpt<DateTimeTimeZone>& p_End);
    void SetHasAttachments(const boost::YOpt<bool>& p_HasAttachments);
    void SetICalUId(const boost::YOpt<PooledString>& p_ICalUId);
    void SetImportance(const boost::YOpt<PooledString>& p_Importance);
    void SetIsAllDay(const boost::YOpt<bool>& p_IsAllDay);
    void SetIsCancelled(const boost::YOpt<bool>& p_IsCancelled);
    void SetIsOrganizer(const boost::YOpt<bool>& p_IsOrganizer);
    void SetIsReminderOn(const boost::YOpt<bool>& p_IsReminderOn);
    void SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_LastModifiedDateTime);
    void SetLastModifiedDateTime(const boost::YOpt<PooledString>& p_LastModifiedDateTime);
    void SetLocation(const boost::YOpt<Location>& p_Location);
    void SetOnlineMeetingUrl(const boost::YOpt<PooledString>& p_OnlineMeetingUrl);
    void SetOrganizer(const boost::YOpt<Recipient>& p_Organizer);
    void SetOriginalEndTimeZone(const boost::YOpt<PooledString>& p_OriginalEndTimeZone);
    void SetOriginalStart(const boost::YOpt<YTimeDate>& p_OriginalStart);
    void SetOriginalStart(const boost::YOpt<PooledString>& p_OriginalStart);
    void SetOriginalStartTimeZone(const boost::YOpt<PooledString>& p_OriginalStartTimeZone);
    void SetRecurrence(const boost::YOpt<PatternedRecurrence>& p_Recurrence);
    void SetReminderMinutesBeforeStart(const boost::YOpt<int32_t>& p_ReminderMinutesBeforeStart);
    void SetResponseRequested(const boost::YOpt<bool>& p_ResponseRequested);
    void SetResponseStatus(boost::YOpt<ResponseStatus> p_ResponseStatus);
    void SetSensitivity(const boost::YOpt<PooledString>& p_Sensitivity);
    void SetSeriesMasterId(const boost::YOpt<PooledString>& p_SeriesMasterId);
    void SetShowAs(const boost::YOpt<PooledString>& p_ShowAs);
    void SetStart(const boost::YOpt<DateTimeTimeZone>& p_Start);
    void SetSubject(const boost::YOpt<PooledString>& p_Subject);
    void SetType(const boost::YOpt<PooledString>& p_Type);
	void SetWebLink(const boost::YOpt<PooledString>& p_WebLink);

	void SetOwnerId(const boost::YOpt<PooledString>& p_OwnerId, bool p_IsUser);

	static Event ToEvent(const BusinessEvent& p_BusinessEvent);

private:
    vector<Attendee> m_Attendees;
    boost::YOpt<ItemBody> m_Body;
    boost::YOpt<PooledString> m_BodyPreview;
    boost::YOpt<vector<PooledString>> m_Categories;
    boost::YOpt<PooledString> m_ChangeKey;
    boost::YOpt<YTimeDate> m_CreatedDateTime;
    boost::YOpt<DateTimeTimeZone> m_End;
    boost::YOpt<bool> m_HasAttachments;
    boost::YOpt<PooledString> m_ICalUId;
    boost::YOpt<PooledString> m_Importance;
    boost::YOpt<bool> m_IsAllDay;
    boost::YOpt<bool> m_IsCancelled;
    boost::YOpt<bool> m_IsOrganizer;
    boost::YOpt<bool> m_IsReminderOn;
    boost::YOpt<YTimeDate> m_LastModifiedDateTime;
    boost::YOpt<Location> m_Location;
    boost::YOpt<PooledString> m_OnlineMeetingUrl;
    boost::YOpt<Recipient> m_Organizer;
    boost::YOpt<PooledString> m_OriginalEndTimeZone;
    boost::YOpt<YTimeDate> m_OriginalStart;
    boost::YOpt<PooledString> m_OriginalStartTimeZone;
    boost::YOpt<PatternedRecurrence> m_Recurrence;
    boost::YOpt<int32_t> m_ReminderMinutesBeforeStart;
    boost::YOpt<bool> m_ResponseRequested;
    boost::YOpt<ResponseStatus> m_ResponseStatus;
    boost::YOpt<PooledString> m_Sensitivity;
    boost::YOpt<PooledString> m_SeriesMasterId;
    boost::YOpt<PooledString> m_ShowAs;
    boost::YOpt<DateTimeTimeZone> m_Start;
    boost::YOpt<PooledString> m_Subject;
    boost::YOpt<PooledString> m_Type;
    boost::YOpt<PooledString> m_WebLink;

	// Non property
	boost::YOpt<PooledString> m_UserOwnerId; // Owner
	boost::YOpt<PooledString> m_GroupOwnerId; // Owner

protected:
	virtual TaskWrapper<HttpResultWithError> SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;
    
private:
    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND
    friend class EventDeserializer;
};

