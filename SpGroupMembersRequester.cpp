#include "SpGroupMembersRequester.h"

#include "Sapio365Session.h"
#include "SharepointOnlineSession.h"
#include "SingleRequestResult.h"
#include "SpUser.h"
#include "SpUserDeserializer.h"
#include "ValueListDeserializer.h"
#include "BasicHttpRequestLogger.h"

Sp::SpGroupMembersRequester::SpGroupMembersRequester(PooledString p_SiteUrl, PooledString p_GroupId)
    : m_SiteUrl(p_SiteUrl)
	, m_GroupId(p_GroupId)
{
}

TaskWrapper<void> Sp::SpGroupMembersRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<Sp::User, Sp::UserDeserializer>>();
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("sites"));
    uri.append_path(m_SiteUrl);
    uri.append_path(_YTEXT("_api"));
    uri.append_path(_YTEXT("web"));
    uri.append_path(wstring(_YTEXT("siteGroups(")) + m_GroupId.c_str() + _YTEXT(")"));
    uri.append_path(_YTEXT("users"));

    LoggerService::User(YtriaTranslate::Do(Sp__SpGroupMembersRequester_Send_1, _YLOC("Requesting group members for group \"%1\" in site \"%2\""), m_GroupId.c_str(), m_SiteUrl.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetSharepointSession()->Get(uri.to_uri(), httpLogger, m_Deserializer, p_TaskData);
}

const vector<Sp::User>& Sp::SpGroupMembersRequester::GetData() const
{
    return m_Deserializer->GetData();
}
