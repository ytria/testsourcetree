#include "MsGraphPaginator.h"

#include "GraphPageRequester.h"
#include "IPageRequestLogger.h"

const wstring MsGraphPaginator::g_NextLinkKey = _YTEXT("@odata.nextLink");
const size_t MsGraphPaginator::g_DefaultPageSize = 999;

MsGraphPaginator::MsGraphPaginator(const web::uri& p_Uri, const shared_ptr<IMsGraphPageRequester>& p_Requester, const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_OriginalUri(p_Uri)
	, m_PageSize(g_DefaultPageSize)
	, m_Requester(p_Requester)
	, m_LatestRequest(std::make_shared<web::uri>())
	, m_RequestLogger(p_Logger)
{
	ValidateObj();
}

MsGraphPaginator::MsGraphPaginator(const web::uri& p_Uri, const shared_ptr<IMsGraphPageRequester>& p_Requester, const std::shared_ptr<IPageRequestLogger>& p_Logger, size_t p_PageSize)
	: m_OriginalUri(p_Uri)
	, m_PageSize(p_PageSize)
	, m_Requester(p_Requester)
	, m_LatestRequest(std::make_shared<web::uri>())
	, m_RequestLogger(p_Logger)
{
	ValidateObj();
}

void MsGraphPaginator::ValidateObj()
{
	if (nullptr == m_Requester || nullptr == m_RequestLogger)
	{
		ASSERT(false);
		throw std::invalid_argument("MsGraphPaginator: requester and requestLogger cannot be null");
	}
}

size_t MsGraphPaginator::GetPageSize() const
{
	return m_PageSize;
}

TaskWrapper<void> MsGraphPaginator::Paginate(const shared_ptr<MSGraphSession>& p_Session, YtriaTaskData p_TaskData)
{
	return YSafeCreateTask([p = *this, p_Session, p_TaskData, requestLogger = m_RequestLogger]() {
		web::uri initialUri = CreateInitialUri(p.m_OriginalUri, p.m_PageSize);
		*p.m_LatestRequest = initialUri;

		IMsGraphPageRequester& requester = *p.m_Requester;
		
		ASSERT(nullptr != requestLogger); // There should have a request logger!
		if (nullptr != requestLogger)
		{
			requestLogger->ResetPageCount();
			requestLogger->Log(initialUri, requester.GetDeserializedCount(), p_TaskData.GetOriginator());
		}
		auto json = requester.Request(initialUri, p_Session, p_TaskData);

		while (HasNextPage(json) && !requester.GetIsCutOff() && !p_TaskData.IsCanceled())
		{
			web::uri_builder nextPageUri(json.at(g_NextLinkKey).as_string());
			if (nextPageUri.is_valid())
			{
				Minimize(nextPageUri);
				*p.m_LatestRequest = nextPageUri.to_uri();

				if (nullptr != requestLogger)
				{
					requestLogger->IncrementPageCount();
					requestLogger->Log(nextPageUri.to_uri(), requester.GetDeserializedCount(), p_TaskData.GetOriginator());
				}
				json = requester.Request(nextPageUri.to_uri(), p_Session, p_TaskData);
			}
			else
				json = web::json::value();
		}
	});
}

const web::uri& MsGraphPaginator::GetLatestRequest() const
{
	web::uri latest = *m_LatestRequest;
	return *m_LatestRequest;
}

const IMsGraphPageRequester& MsGraphPaginator::GetPageRequester() const
{
	return *m_Requester;
}

IMsGraphPageRequester& MsGraphPaginator::GetPageRequester()
{
	return *m_Requester;
}

void MsGraphPaginator::SetLogger(const shared_ptr<IPageRequestLogger>& p_Logger)
{
	m_RequestLogger = p_Logger;
}

bool MsGraphPaginator::HasNextPage(const web::json::value& p_Json)
{
	return p_Json.has_string_field(g_NextLinkKey);
}

void MsGraphPaginator::Minimize(web::uri_builder& p_Uri)
{
	// Remove parts already included in base url
	p_Uri.set_scheme(_YTEXT(""));
	p_Uri.set_host(_YTEXT(""));

	vector<wstring> pathParts = web::uri::split_path(p_Uri.path());
	if (!pathParts.empty())
	{
		pathParts.erase(pathParts.begin());
		p_Uri.set_path(_YTEXT(""));
		for (const auto& part : pathParts)
			p_Uri.append_path(part);
	}
}

web::uri MsGraphPaginator::CreateInitialUri(const web::uri& p_OriginalUri, size_t p_PageSize)
{
	web::uri_builder uriBuilder(p_OriginalUri.to_string());

	auto oldQuery = web::uri::split_query(p_OriginalUri.query());
	if (oldQuery.find(_YTEXT("$top")) == oldQuery.end())
		uriBuilder.append_query(_YTEXT("$top"), std::to_wstring(p_PageSize));

	return uriBuilder.to_uri();
}

