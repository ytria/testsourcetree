#pragma once

#include "ISqlQuery.h"
#include "SessionIdentifier.h"

class O365SQLiteEngine;

class QueryRoleIdByName : public ISqlQuery
{
public:
	QueryRoleIdByName(O365SQLiteEngine& p_Engine, const wstring& p_EmailOrAppId, const wstring& p_RoleName);
	void Run() override;

	int64_t GetRoleId() const;

private:
	const wstring m_EmailOrAppId;
	const wstring m_RoleName;
	O365SQLiteEngine& m_Engine;

	int64_t m_RoleId;
};

