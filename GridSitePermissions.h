#pragma once

#include "BaseO365Grid.h"
#include "BusinessSite.h"
#include "ModuleSitePermissions.h"
#include "SpUser.h"
#include "RoleDefinition.h"

class FrameSitePermissions;
class RoleAssignmentPermission;

class GridSitePermissions : public ModuleO365Grid<BusinessSite>
{
public:
    GridSitePermissions();
    virtual ~GridSitePermissions() override;

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildView(const O365DataMap<BusinessSite, ModuleSitePermissions::SitePermissionsInfo>& p_SitePermissions, bool p_FullPurge);

    ModuleSitePermissions::SitePermissionsChanges GetSitePermissionsChanges(bool p_Selected);

    GridBackendColumn* GetColPermissionsElder() const;
    GridBackendColumn* GetColPermissionsPk() const;

	GridBackendColumn* GetColSiteUrl() const;
	GridBackendColumn* GetColGroupId() const;
	GridBackendColumn* GetColUserId() const;

    bool IsCorrespondingRow(GridBackendRow* p_Row, const RoleAssignmentPermission& p_AssigmentPermission);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
    afx_msg void OnDeletePermissions();
    afx_msg void OnUpdateDeletePermissions(CCmdUI* pCmdUI);

    void customizeGrid() override;

    void FillGroupFields(const BusinessSite& p_Site, const Sp::Group& p_SpGroup, GridBackendRow* p_Row, GridUpdater& p_Updater);
    void FillUserFields(const BusinessSite& p_Site, const Sp::User& p_SpUser, GridBackendRow* p_Row, GridUpdater& p_Updater);
    void FillPermissionFields(const vector<Sp::RoleDefinition>& p_RoleDefinitions, GridBackendRow* p_Row, GridUpdater& p_Updater);

    BusinessSite    getBusinessObject(GridBackendRow*) const override;
    void			UpdateBusinessObjects(const vector<BusinessSite>& p_Sites, bool p_SetModifiedStatus) override;
    void			RemoveBusinessObjects(const vector<BusinessSite>& p_Sites) override;

private:
    DECLARE_MESSAGE_MAP()

    void CreateDeletion(GridBackendRow* p_Row, const RoleAssignmentPermission& p_Info);

    void InitializeCommands() override;
    void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart) override;

    void FillLoginName(GridBackendRow* p_Row, const boost::YOpt<PooledString>& p_LoginName);

    GridBackendColumn* m_ColMetaId;
    GridBackendColumn* m_ColMetaDisplayName;
    GridBackendColumn* m_ColMetaUserDisplayName;
    GridBackendColumn* m_ColWebUrl;

    GridBackendColumn* m_ColSiteName;
    GridBackendColumn* m_ColSiteUrl;

    GridBackendColumn* m_ColGroupId;
    GridBackendColumn* m_ColUserId;

    // Common columns (users and groups)
    GridBackendColumn* m_ColIsHiddenInUI;
    GridBackendColumn* m_ColLoginName0;
    GridBackendColumn* m_ColLoginName1;
    GridBackendColumn* m_ColLoginName2;
    GridBackendColumn* m_ColPrincipalType;
    GridBackendColumn* m_ColTitle;
    GridBackendColumn* m_ColGroupName;

    // Group Columns
    GridBackendColumn* m_ColAllowMembersEditMembership;
    GridBackendColumn* m_ColAllowRequestToJoinLeave;
    GridBackendColumn* m_ColAutoAcceptRequestToJoinLeave;
    GridBackendColumn* m_ColCanCurrentUserEditMembership;
    GridBackendColumn* m_ColCanCurrentUserManageGroup;
    GridBackendColumn* m_ColCanCurrentUserViewMembership;
    GridBackendColumn* m_ColDescription;
    GridBackendColumn* m_ColOnlyAllowMembersViewMembership;
    GridBackendColumn* m_ColOwnerTitle;
    GridBackendColumn* m_ColRequestToJoinLeaveEmailSetting;

    // User columns
    GridBackendColumn* m_ColEmail;
    GridBackendColumn* m_ColIsSiteAdmin;

    
    // Permissions columns
    GridBackendColumn* m_ColPermissionsBasePermissionsHigh;
    GridBackendColumn* m_ColPermissionsBasePermissionsLow;
    GridBackendColumn* m_ColPermissionsDescription;
    GridBackendColumn* m_ColPermissionsHidden;
    GridBackendColumn* m_ColPermissionsId;
    GridBackendColumn* m_ColPermissionsName;
    GridBackendColumn* m_ColPermissionsOrder;
    GridBackendColumn* m_ColPermissionsRoleTypeKind;

    FrameSitePermissions* m_Frame;

    static wstring g_FamilyCommonInfo;
    static wstring g_FamilyUserInfo;
    static wstring g_FamilyGroupInfo;
    static wstring g_FamilyPermissionInfo;
};

