#include "GridSelectRecipients.h"
#include "BusinessObjectManager.h"
#include "GraphCache.h"

GridSelectRecipients::GridSelectRecipients(GraphCache& p_GraphCache)
    : CacheGrid(0, GridBackendUtil::CREATESORTING)
	, m_GraphCache(p_GraphCache)
	, m_ColEmail(nullptr)
{
}

void GridSelectRecipients::customizeGrid()
{
    m_ColEmail       = AddColumn(_YUID("email"), YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_11, _YLOC("Email")).c_str(), _YTEXT(""));
    m_ColDisplayName = AddColumn(_YUID("displayName"), YtriaTranslate::Do(DlgAddItemsToGroups_loadUsersToGrid_1, _YLOC("Display Name")).c_str(), _YTEXT(""));

    SetColumnsAutoSizeWithGridResize({ m_ColEmail, m_ColDisplayName }, true);

    SetAutomationName(_YTEXT("RecipientsGrid"));
}

void GridSelectRecipients::BuildView()
{
    auto users = m_GraphCache.GetCachedUsers();

    for (const auto& cachedUser : users)
    {
        GridBackendRow* row = AddRow();
        ASSERT(nullptr != row);
        if (nullptr != row)
        {
            row->AddField(cachedUser.GetUserPrincipalName(), m_ColEmail);
            row->AddField(cachedUser.GetDisplayName(), m_ColDisplayName);
        }
    }

    UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

vector<PooledString> GridSelectRecipients::GetSelectedEmails()
{
    vector<PooledString> emails;
    for (auto row : GetSelectedRows())
    {
		if (!row->IsGroupRow())
		{
			const auto& emailField = row->GetField(m_ColEmail);
			ASSERT(emailField.IsString());
			if (emailField.IsString())
				emails.push_back(emailField.GetValueStr());
		}
    }

    return emails;
}
