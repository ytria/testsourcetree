#include "DlgTokenHistory.h"

#include "CacheGrid.h"
#include "GridUtil.h"
#include "LicenseManager.h"
#include "Resource.h"
#include "SessionIdentifier.h"
#include "YtriaTranslate.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

class DlgTokenHistory::TokenHistoryGrid : public CacheGrid
{
public:
	TokenHistoryGrid()
		: CacheGrid(0, GridBackendUtil::CREATESORTING | GridBackendUtil::CREATEGROUPAREA | GridBackendUtil::CREATECOLORG)
	{

	}

	void customizeGrid() override
	{
		SetAutomationName(_YTEXT("TokenHistoryGrid"));

		static const wstring family = _T("Transaction");
		m_ColumnDate = AddColumnDate(_YUID("date"), _T("Date"), family, CacheGrid::g_ColumnSize12);
		m_ColumnCount = AddColumnNumberInt(_YUID("tokens"), _T("Tokens"), family, CacheGrid::g_ColumnSize6);
		m_ColumnReason = AddColumn(_YUID("reason"), _T("Reason"), family, CacheGrid::g_ColumnSize8);
		m_ColumnModule = AddColumn(_YUID("module"), _T("Module"), family, CacheGrid::g_ColumnSize6);
		m_ColumnSession = AddColumn(_YUID("session"), _T("Session"), family), CacheGrid::g_ColumnSize22;
		SetColumnsAutoSizeWithGridResize({ m_ColumnReason }, true);
	}

	void AddEntry(const LicenseManager::TokenTransaction& p_TokenTransaction)
	{
		auto row = AddRow();
		row->AddField(p_TokenTransaction.m_Date, m_ColumnDate);
		row->AddField(p_TokenTransaction.m_Count, m_ColumnCount);
		row->AddField(p_TokenTransaction.m_Reason, m_ColumnReason);
		
		auto modName = GridUtil::ModuleName(p_TokenTransaction.m_Module);
		if (modName.empty())
			modName = _YTEXT("??");
		row->AddField(modName, m_ColumnModule);
		
		SessionIdentifier si;
		si.Deserialize(p_TokenTransaction.m_SessionReference);
		row->AddField(si.m_EmailOrAppId, m_ColumnSession);
	}

private:
	GridBackendColumn* m_ColumnDate;
	GridBackendColumn* m_ColumnCount;
	GridBackendColumn* m_ColumnReason;
	GridBackendColumn* m_ColumnModule;
	GridBackendColumn* m_ColumnSession;
};
// ============================================================================

BEGIN_MESSAGE_MAP(DlgTokenHistory, ResizableDialog)
	ON_BN_CLICKED(IDOK,				OnOK)
END_MESSAGE_MAP()

DlgTokenHistory::DlgTokenHistory(LicenseManager* p_licenseManager, CWnd* p_Parent)
	: ResizableDialog(IDD_DLG_TOKEN_HISTORY, p_Parent)
	, m_LicenseManager(p_licenseManager)
	, m_Grid(std::make_unique<TokenHistoryGrid>())
{
	ASSERT(nullptr != m_LicenseManager);
}

DlgTokenHistory::~DlgTokenHistory() = default;

void DlgTokenHistory::DoDataExchange(CDataExchange* pDX)
{
	ResizableDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID_GRIDAREA,	m_LabelGridArea);
	DDX_Control(pDX, IDC_GRID_HEADER,	m_LabelGridHeader);
	DDX_Control(pDX, IDOK,					m_BtnOk);
}

BOOL DlgTokenHistory::OnInitDialogSpecificResizable()
{
	SetWindowText(CString(_T("Token consumption history")));

	m_BtnOk.SetWindowText(CString(_T("OK")));

	m_LabelGridHeader.SetWindowText(CString(_YDUMP("")));

	CRect aRect;
	m_LabelGridArea.GetWindowRect(&aRect);
	ScreenToClient(&aRect);
	ASSERT(m_Grid);
	if (!m_Grid->Create(this, aRect))
	{
		// Creation of grid failed.
		ASSERT(FALSE);
		return -1;
	}

	m_Grid->ShowWindow(SW_SHOW);
	m_Grid->BringWindowToTop();

	CWaitCursor c;
	fillGrid();

	AddAnchor(m_LabelGridHeader,	CSize(0, 0),		CSize(0, 10));
	AddAnchor(*m_Grid,				CSize(0, 10),		CSize(100, 100));
	AddAnchor(IDOK,					CSize(100, 100),	CSize(100, 100));
	AddAnchor(IDC_BUTTON_TEST,		CSize(0, 100),		CSize(0, 100));

	return TRUE;
}

void DlgTokenHistory::fillGrid()
{
	m_Grid->RemoveAllRows();
	if (nullptr != m_LicenseManager)
	{
		const auto& tokenTransactions = m_LicenseManager->GetTokenTransactions();
		for (const auto& tt : tokenTransactions)
			m_Grid->AddEntry(tt);

		m_Grid->UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}
}
