#pragma once

#include "IDeserializerFactory.h"
#include "Sapio365Session.h"
#include "GroupDeserializer.h"

class GroupDeserializerFactory : public IDeserializerFactory<GroupDeserializer>
{
public:
	GroupDeserializerFactory(std::shared_ptr<const Sapio365Session> p_Session);
	GroupDeserializer Create() override;

private:
	std::shared_ptr<const Sapio365Session>	m_Session;
};
