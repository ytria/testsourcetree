#include "SessionsMigrationVersion2To3.h"

#include "FileUtil.h"
#include "MigrationUtil.h"
#include "Sapio365Settings.h"
#include "SessionsSqlEngine.h"
#include "SessionTypes.h"
#include "SqlQueryMultiPreparedStatement.h"
#include "SQLSafeTransaction.h"
#include "SessionMigrationUtil.h"

void SessionsMigrationVersion2To3::Build()
{
	AddStep(std::bind(&SessionsMigrationVersion2To3::CreateNewFromOld, this));
	AddStep(std::bind(&SessionsMigrationVersion2To3::Exec, this));
	AddStep(std::bind(&SessionsMigrationVersion2To3::UpgradeLastSessionUsed, this));
}

VersionSourceTarget SessionsMigrationVersion2To3::GetVersionInfo() const
{
	return { 2, 3 };
}

bool SessionsMigrationVersion2To3::CreateNewFromOld()
{
	return SessionMigrationUtil::CopyDatabase(GetVersionInfo());
}

bool SessionsMigrationVersion2To3::Exec()
{
	// This migration rename Session Types

	// New names. Remove those asserts if a future migration changes those names again.
	ASSERT(SessionTypes::g_StandardSession == _YTEXT("standard"));
	ASSERT(SessionTypes::g_AdvancedSession == _YTEXT("advanced"));
	ASSERT(SessionTypes::g_UltraAdmin == _YTEXT("ultraadmin"));
	ASSERT(SessionTypes::g_ElevatedSession == _YTEXT("elevated"));

	// Role is not impacted
	//ASSERT(SessionTypes::g_Role == _YTEXT("role"));

	bool success = false;
	return SQLSafeTransaction(GetTargetSQLEngine(), [this, &success]()
		{
			const std::vector<wstring> statements =
			{
				LR"(UPDATE Sessions SET SessionType = REPLACE('basic_user', 'basic_user', 'standard') WHERE SessionType LIKE 'basic_user';)",
				LR"(UPDATE Sessions SET SessionType = REPLACE('delegated_admin', 'delegated_admin', 'advanced') WHERE SessionType LIKE 'delegated_admin';)",
				LR"(UPDATE Sessions SET SessionType = REPLACE('full_admin', 'full_admin', 'ultraadmin') WHERE SessionType LIKE 'full_admin';)",
				LR"(UPDATE Sessions SET SessionType = REPLACE('full_session', 'full_session', 'elevated') WHERE SessionType LIKE 'full_session';)",
			};

			SqlQueryMultiPreparedStatement query(GetTargetSQLEngine(), statements);
			query.Run();
			success = query.IsSuccess();
		}, true).Run() && success;
}

bool SessionsMigrationVersion2To3::UpgradeLastSessionUsed()
{
	auto oldLastSessionUsed = MainFrameSessionOldSetting().Get();
	auto lastSessionUsed = MainFrameSessionSetting().Get();
	if (oldLastSessionUsed && !lastSessionUsed)
	{
		lastSessionUsed = oldLastSessionUsed;
		auto& sessionType = lastSessionUsed->m_SessionType;
		if (sessionType == _YTEXT("basic_user"))
			sessionType = SessionTypes::g_StandardSession;
		else if (sessionType == _YTEXT("delegated_admin"))
			sessionType = SessionTypes::g_AdvancedSession;
		else if (sessionType == _YTEXT("full_admin"))
			sessionType = SessionTypes::g_UltraAdmin;
		else if (sessionType == _YTEXT("full_session"))
			sessionType = SessionTypes::g_ElevatedSession;

		return MainFrameSessionSetting().Set(lastSessionUsed);
	}

	return true;
}
