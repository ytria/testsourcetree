#pragma once

#include "IMigrationVersion.h"

class SessionsSqlEngine;

class ISessionsMigrationVersion : public IMigrationVersion
{
public:
	SessionsSqlEngine& GetSourceSQLEngine();
	SessionsSqlEngine& GetTargetSQLEngine();

private:
	std::unique_ptr<SessionsSqlEngine> m_SourceEngine;
	std::unique_ptr<SessionsSqlEngine> m_TargetEngine;
};