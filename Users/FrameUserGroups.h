#pragma once

#include "GridFrameBase.h"
#include "GridUserGroups.h"

class FrameUserGroups : public GridFrameBase
{
public:
	FrameUserGroups(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

	DECLARE_DYNAMIC(FrameUserGroups)
	virtual ~FrameUserGroups() = default;

	void ShowUserWithParentGroups(const vector<BusinessUser>& p_Users, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge);
	void AddUserWithParentGroups(const vector<BusinessUser>& p_Users, const bool p_IsRefresh, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge);

	virtual O365Grid& GetGrid() override;
	virtual void ApplyAllSpecific() override;
    virtual void ApplySelectedSpecific() override;
    void ApplySpecific(bool p_Selected);

	virtual void RefreshSpecific(AutomationAction* p_CurrentAction);
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

	// Generated message map functions
protected:

	virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;
	virtual O365ControlConfig GetApplyPartialControlConfig() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

// Members
private:
	GridUserGroups m_GridUserGroups;
};
