#pragma once

#include "ModuleBase.h"

class AutomationAction;
struct CommandInfo;
struct EmailInfo;
class FrameUsers;

class ModuleUser : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	virtual void executeImpl(const Command& p_Command) override;
	void show(Command p_Command);// show users from IDs
    void showParentGroups(const Command& p_Command);// show these users from IDs with their parent groups
	void showManagerHierarchy(const Command& p_Command);
	void showRecycleBin(const Command& p_Command);
	void update(const Command& p_Command, const wstring& p_TaskText);
	void more(const Command& p_Command);
	void moreMailbox(const Command& p_Command);
	void revokeAccess(const Command& p_Command);
	void loadSnapshot(const Command& p_Command);
	void saveOnPrem(const Command& p_Command);
    //void sendEmail(const bool p_UltraAdmin);

	void loadUsers(const Command& p_Command, const CommandInfo& p_CommandInfo, FrameUsers* p_Frame, bool p_IsRefresh, bool p_Sync);
    void getUsersWithIndividualRequests(const Command& p_Command, const CommandInfo& p_CommandInfo, FrameUsers* p_Frame, bool p_IsRefresh);

	void ensureAssignedByGroupsAreInCache(vector<BusinessUser>& p_BusinessUsers, const std::shared_ptr<BusinessObjectManager>& p_BOM, YtriaTaskData p_TaskData);
	void ensureManagersAreInCache(vector<BusinessUser>& p_BusinessUsers, const std::shared_ptr<BusinessObjectManager>& p_BOM, YtriaTaskData p_TaskData);

	friend class ModuleLicense; // For access to getSubscribedSkus
};
