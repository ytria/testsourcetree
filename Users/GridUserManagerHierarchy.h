#pragma once

#include "BusinessUser.h"
#include "BaseO365Grid.h"
#include "GridTemplateGroups.h"
#include "GridTemplateUsers.h"

#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
class FrameUserManagerHierarchy;
class O365GridErrorHelper;

class GridUserManagerHierarchy : public O365Grid
{
public:
	GridUserManagerHierarchy();
	virtual ~GridUserManagerHierarchy() = default;

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;
	void AddUserWithManager(vector<BusinessUser>& p_Users, const bool p_IsRefresh, bool p_FullPurge);

	virtual bool IsQueryReferenceRow(GridBackendRow* p_Row) const override;

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
	virtual void customizeGrid() override;
	void addUserRow(GridBackendRow* p_Row, const BusinessUser& p_User, const set<PooledString>& p_selectedUserIDs, GridUpdater& p_Updater);

private:
	GridTemplateUsers	m_TemplateUsers;
	GridBackendColumn*	m_ColumnSelectedUser;
	int					m_QueriedUserId;
};
#endif
