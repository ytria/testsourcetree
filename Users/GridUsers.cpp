#include "GridUsers.h"

#include "AutomationWizardUsers.h"
#include "BasicGridSetup.h"
#include "BatchImporter.h"
#include "BusinessUserConfiguration.h"
#include "BusinessUserGuest.h"
#include "Command.h"
#include "CommandDispatcher.h"
#include "DlgAddItemsToGroups.h"
#include "DlgBusinessUserEditHTML.h"
#include "DlgOnPremAADConnectServer.h"
#include "DlgOnPremUserEditHTML.h"
#include "DlgRegAccountRegistration.h"
#include "DlgSimpleEditHTML.h"
#include "DlgUserPasswordHTML.h"
#include "DlgUsersModuleOptions.h"
#include "FlatObjectListFrameRefresher.h"
#include "FrameUsers.h"
#include "GraphCache.h"
#include "GridUpdater.h"
#include "Languages.h"
#include "OnPremModificationsApplyer.h"
#include "OnPremModificationsReverter.h"
#include "OnPremiseUser.h"
#include "RevokeAccessFieldUpdate.h"
#include "ShowOnPremUsersCommand.h"
#include "UpdatedObjectsGenerator.h"
#include "UserDeserializer.h"
#include "../Resource.h"

BEGIN_MESSAGE_MAP(GridUsers, GridUsersBaseClass)
	ON_COMMAND(ID_USERGRID_REFRESH_ONPREM,				OnRefreshOnPrem)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_REFRESH_ONPREM,	OnUpdateRefreshOnPrem)
	ON_COMMAND(ID_USERGRID_LOADMORE,					OnUserLoadMore)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_LOADMORE,			OnUpdateUserLoadMore)
	ON_COMMAND(ID_USERGRID_GETMAILBOXINFO,				OnUserLoadMailboxInfo)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_GETMAILBOXINFO,	OnUpdateUserLoadMailboxInfo)
	ON_COMMAND(ID_USERGRID_SHOW_SENT_VALUES,			OnShowSentValues)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_SHOW_SENT_VALUES,	OnUpdateShowSentValues)
	ON_COMMAND(ID_USERGRID_RESETPASSWORD,				OnResetPassword)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_RESETPASSWORD,		OnUpdateResetPassword)
	ON_COMMAND(ID_USERGRID_SETMANAGER,					OnSetManager)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_SETMANAGER,		OnUpdateSetManager)
	ON_COMMAND(ID_USERGRID_EDITONPREM,					OnEditOnPrem)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_EDITONPREM,		OnUpdateEditOnPrem)
	ON_COMMAND(ID_USERGRID_REMOVEMANAGER,				OnRemoveManager)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_REMOVEMANAGER,		OnUpdateRemoveManager)
	ON_COMMAND(ID_MODULEGRID_DELETE,					OnDelete)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_REVOKESESSIONS,	OnUpdateRevokeSessions)
	ON_COMMAND(ID_USERGRID_REVOKESESSIONS,				OnRevokeSessions)

	ON_UPDATE_COMMAND_UI(ID_USERGRID_BATCHIMPORT,		OnUpdateBatchImport)
	ON_COMMAND(ID_USERGRID_BATCHIMPORT,					OnBatchImport)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_BATCHUPDATE,		OnUpdateBatchUpdate)
	ON_COMMAND(ID_USERGRID_BATCHUPDATE,					OnBatchUpdate)

	ON_UPDATE_COMMAND_UI(ID_USERGRID_CHANGEPREFILTER,	OnUpdateChangePreFilter)
	ON_COMMAND(ID_USERGRID_CHANGEPREFILTER,				OnChangePreFilter)

	ON_COMMAND(ID_MODULEGRID_EDIT, OnEdit)
	ON_UPDATE_COMMAND_UI(ID_MODULEGRID_EDIT, OnUpdateEdit)
	ON_COMMAND(ID_USERGRID_EDITONPREM, OnEditOnPrem)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_EDITONPREM, OnUpdateEditOnPrem)
	ON_COMMAND(ID_USERGRID_EDITO365, OnEditO365)
	ON_UPDATE_COMMAND_UI(ID_USERGRID_EDITO365, OnUpdateEditO365)

	NEWMODULE_COMMAND(ID_USERGRID_SHOW_RECYCLEBIN, ID_USERGRID_SHOW_RECYCLEBIN_FRAME, ID_USERGRID_SHOW_RECYCLEBIN_NEWFRAME, OnShowRecycleBin, OnUpdateShowRecycleBin)

	// Keep code.
	// 	ON_COMMAND(ID_USERGRID_SHOW_ADDTOGROUPS,			OnShowAddToGroups)
	// 	ON_UPDATE_COMMAND_UI(ID_USERGRID_SHOW_ADDTOGROUPS,	OnUpdateShowAddToGroups)
END_MESSAGE_MAP()

GridUsers::GridUsers()
	: m_Template(false)	
	, m_ForceSorting(false)
{
	m_MayContainUnscopedUserGroupOrSite = true;

	UseDefaultActions(O365Grid::ACTION_CREATE | O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT/* | O365Grid::ACTION_VIEW*/);
	EnableGridModifications();

	SetModApplyer(std::make_shared<OnPremModificationsApplyer<GridUsers>>(*this));
	SetModReverter(std::make_shared<OnPremModificationsReverter<GridUsers>>(*this));

	initWizard<AutomationWizardUsers>(_YTEXT("Automation\\Users"));
}

GridUsers::~GridUsers()
{
}

BusinessUser GridUsers::getBusinessObject(GridBackendRow* row) const
{
	BusinessUser bu;
	m_Template.GetBusinessUser(*this, row, bu);
	if (IsTemporaryCreatedObjectID(bu.GetID()) || row->IsCreated())
	{
		// This assert fails after a User creation fails. Remove it eventually.
		// ASSERT(IsTemporaryCreatedObjectID(bu.GetID()) && row->IsCreated());
		bu.SetFlags(bu.GetFlags() | BusinessObject::CREATED);
	}

	if (row->IsMoreLoaded())
	{
		// FIXME: New modular cache.
		bu.SetFlags(bu.GetFlags() | BusinessObject::MORE_LOADED);
	}
	return bu;
}

OnPremiseUser GridUsers::getOnPremiseUser(GridBackendRow* row) const
{
	OnPremiseUser user;
	auto res = m_Template.GetOnPremiseUser(*this, row, user);
	ASSERT(res);

	//if (IsTemporaryCreatedObjectID(bu.GetID()) || row->IsCreated())
	//{
	//	// This assert fails after a User creation fails. Remove it eventually.
	//	// ASSERT(IsTemporaryCreatedObjectID(bu.GetID()) && row->IsCreated());
	//	bu.SetFlags(bu.GetFlags() | BusinessObject::CREATED);
	//}

	//if (row->IsMoreLoaded())
	//{
	//	// FIXME: New modular cache.
	//	bu.SetFlags(bu.GetFlags() | BusinessObject::MORE_LOADED);
	//}

	return user;
}

void GridUsers::customizeGrid()
{
	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_GRIDUSERS_ACCELERATOR));

	{
		BasicGridSetup setup(GridUtil::g_AutoNameUsers, LicenseUtil::g_codeSapio365users);
		setup.Setup(*this, true);
		m_Template.m_ColumnLastRequestDateTime = setup.GetColumnLastRequestDateTime();
		m_Template.GetRequestDateColumn(UBI::LIST) = m_Template.m_ColumnLastRequestDateTime;
	}


	{
		auto cat = AddCategoryTop(_T("User Info"), {});
		{
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_DISPLAYNAME), g_FamilyInfo, { g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_USERPRINCIPALNAME), g_FamilyInfo, { g_ColumnsPresetDefault }));

			{
				static const wstring g_FamilyPassword = YtriaTranslate::Do(GridUsers_customizeGrid_27, _YLOC("Password Info")).c_str();

				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_PASSWORDPOLICIES), g_FamilyPassword, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_PASSWORDPROFILE_PASSWORD), g_FamilyPassword, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_LASTPASSWORDCHANGEDATETIME), g_FamilyPassword, { g_ColumnsPresetDefault }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_PASSWORDPROFILE_FORCECHANGE), g_FamilyPassword, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA), g_FamilyPassword, { }));
			}

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_USERTYPE), g_FamilyInfo, { g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("identities.signInType"), g_FamilyInfo, { g_ColumnsPresetMore }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("identities.issuer"), g_FamilyInfo, { g_ColumnsPresetMore }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("identities.issuerAssignedId"), g_FamilyInfo, { g_ColumnsPresetMore }));

			// Right now loadmore is a block combination, so use the first block of it
			m_Template.GetRequestDateColumn(UBI::SYNCV1) = addColumnIsLoadMore(_YUID("additionalInfoStatus"), YtriaTranslate::Do(GridUsers_customizeGrid_4, _YLOC("Status - Load Additional Information")).c_str(), true);
			AddColumnToCategory(cat, m_Template.GetRequestDateColumn(UBI::SYNCV1));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ACCOUNTENABLED), g_FamilyInfo, { g_ColumnsPresetDefault }));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ISRESOURCEACCOUNT), g_FamilyInfo, { }));

			{
				static const wstring g_FamilyLicenses = _T("Licenses");
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("ASSIGNEDLICENSENAMES"), g_FamilyLicenses, { g_ColumnsPresetDefault }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ASSIGNEDLICENSES), g_FamilyLicenses, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("assignedLicenses.skuId"), g_FamilyLicenses, { g_ColumnsPresetTech }));

				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("GROUPLICENSINGMEMBER"), g_FamilyLicenses, { g_ColumnsPresetDefault }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("ASSIGNEDBYGROUPNAME"), g_FamilyLicenses, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("ASSIGNEDBYGROUPEMAIL"), g_FamilyLicenses, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("licenseAssignmentStates.assignedByGroup"), g_FamilyLicenses, { g_ColumnsPresetTech }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("licenseAssignmentStates.skuId"), g_FamilyLicenses, { g_ColumnsPresetTech }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("ASSIGNEDBYGROUPLICENSENAME"), g_FamilyLicenses, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("ASSIGNEDSKUPARTNUMBER"), g_FamilyLicenses, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("licenseAssignmentStates.state"), g_FamilyLicenses, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("licenseAssignmentStates.error"), g_FamilyLicenses, { }));
			}

			{
				static const wstring g_FamilyMail = YtriaTranslate::Do(GridUsers_customizeGrid_6, _YLOC("Mail")).c_str();
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAIL), g_FamilyMail, { g_ColumnsPresetDefault }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILNICKNAME), g_FamilyMail, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_PROXYADDRESSES), g_FamilyMail, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_IMADDRESSES), g_FamilyMail, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_OTHERMAILS), g_FamilyMail, { }));
			}

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_GIVENNAME), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_SURNAME), g_FamilyInfo, { }));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_EMPLOYEEID), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_JOBTITLE), g_FamilyInfo, { g_ColumnsPresetDefault }));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("manager.displayName"), g_FamilyInfo, { g_ColumnsPresetMore, g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID("manager.userPrincipalName"), g_FamilyInfo, { g_ColumnsPresetMore }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MANAGER_ID), g_FamilyInfo, { g_ColumnsPresetMore, g_ColumnsPresetTech }));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_COMPANYNAME), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_DEPARTMENT), g_FamilyInfo, { g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_BUSINESSPHONES), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MOBILEPHONE), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_FAXNUMBER), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_OFFICELOCATION), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_STREETADDRESS), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_CITY), g_FamilyInfo, { g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_STATE), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_POSTALCODE), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_COUNTRY), g_FamilyInfo, { g_ColumnsPresetDefault }));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_PREFERREDLANGUAGE), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_USAGELOCATION), g_FamilyInfo, { }));

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MYSITE), g_FamilyInfo, { g_ColumnsPresetMore }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_CREATEDDATETIME), g_FamilyInfo, { g_ColumnsPresetDefault }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_CREATIONTYPE), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME), g_FamilyInfo, { }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_REFRESTOKENVALIDFROMDATETIME), g_FamilyInfo, { }));

			{
				static const wstring g_FamilyMoreInfo = YtriaTranslate::Do(GridUsers_customizeGrid_31, _YLOC("More Info")).c_str();

				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ABOUTME), g_FamilyMoreInfo, { g_ColumnsPresetMore, g_ColumnsPresetDefault }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_BIRTHDAY), g_FamilyMoreInfo, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_PASTPROJECTS), g_FamilyMoreInfo, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_SKILLS), g_FamilyMoreInfo, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_RESPONSIBILITIES), g_FamilyMoreInfo, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_SCHOOLS), g_FamilyMoreInfo, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_INTERESTS), g_FamilyMoreInfo, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_PREFERREDNAME), g_FamilyMoreInfo, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_HIREDATE), g_FamilyMoreInfo, { g_ColumnsPresetMore }));
			}

			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_PREFERREDDATALOCATION), g_FamilyInfo, { g_ColumnsPresetMore }));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_SHOWINADDRESSLIST), g_FamilyInfo, { }));

			{
				static const wstring g_FamilyAgeLegality = YtriaTranslate::Do(GridUsers_customizeGrid_62, _YLOC("Age Legality")).c_str();
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_AGEGROUP), g_FamilyAgeLegality, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_CONSENTPROVIDEDFORMINOR), g_FamilyAgeLegality, { }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_LEGALAGEGROUPCLASSIFICATION), g_FamilyAgeLegality, { }));
			}

			{
				static const wstring g_FamilyExternalUser = YtriaTranslate::Do(GridUsers_customizeGrid_63, _YLOC("External User")).c_str();
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_EXTERNALUSERSTATUS), g_FamilyExternalUser, { g_ColumnsPresetMore }));
				AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_EXTERNALUSERSTATUSCHANGEDON), g_FamilyExternalUser, { g_ColumnsPresetMore }));
			}

			AddColumnToCategory(cat, addColumnGraphID());
		}
	}

	{
		static const wstring g_FamilyMailboxSettings = YtriaTranslate::Do(GridUsers_customizeGrid_47, _YLOC("Mailbox settings")).c_str();

		m_ColMailboxInfoLoaded = addColumnDataBlockDate(_YUID("mailboxInfoStatus"), _T("Status - Get mailbox info"), g_FamilyMailboxSettings);
		m_Template.GetRequestDateColumn(UBI::MAILBOX) = m_ColMailboxInfoLoaded;

		ASSERT(m_PowerShellCols.empty());
		m_PowerShellCols.push_back(m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOX_TYPE), g_FamilyMailboxSettings, { g_ColumnsPresetDefault }));
		m_PowerShellCols.push_back(m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOX_DELIVERTOMAILBOXANDFORWARD), g_FamilyMailboxSettings, { }));
		m_PowerShellCols.push_back(m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOX_FORWARDINGSMTPADDRESS), g_FamilyMailboxSettings, { g_ColumnsPresetDefault }));
		m_PowerShellCols.push_back(m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOX_FORWARDINGADDRESS), g_FamilyMailboxSettings, { }));
		m_PowerShellCols.push_back(m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOX_CAN_SEND_ON_BEHALF), g_FamilyMailboxSettings, { }));
		m_PowerShellCols.push_back(m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOX_CAN_SEND_AS_USER), g_FamilyMailboxSettings, { }));

		auto cat = AddCategoryTop(_T("Mailbox Info"),
			{
				m_ColMailboxInfoLoaded,
				m_PowerShellCols[0],
				m_PowerShellCols[1],
				m_PowerShellCols[2],
				m_PowerShellCols[3],
				m_PowerShellCols[4],
				m_PowerShellCols[5],
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_ARCHIVEFOLDERNAME), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_ARCHIVEFOLDER), g_FamilyMailboxSettings, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_TIMEZONE), g_FamilyMailboxSettings, { g_ColumnsPresetMore, g_ColumnsPresetDefault }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_LANGUAGE_DISPLAYNAME), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_LANGUAGE_LOCALE), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_STATUS), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_EXTERNALAUDIENCE), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_EXTERNALREPLYMESSAGE), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_INTERNALREPLYMESSAGE), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDSTARTDATETIME_DATETIME), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDSTARTDATETIME_TIMEZONE), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDENDDATETIME_DATETIME), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDENDDATETIME_TIMEZONE), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_TIMEFORMAT), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_DATEFORMAT), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_DELEGATEMEETINGMESSAGEDELIVERYOPTIONS), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_DAYSOFWEEK), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_STARTTIME), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_ENDTIME), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_TIMEZONENAME), g_FamilyMailboxSettings, { g_ColumnsPresetMore }),
			});
	}

	{
		static const wstring g_FamilyOnPremises = YtriaTranslate::Do(GridUsers_customizeGrid_42, _YLOC("On-Premises")).c_str();
		auto cat = AddCategoryTop(_T("On-Premises Info"),
			{
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ONPREMISESSYNCENABLED),			g_FamilyOnPremises, { }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ONPREMISESDISTINGUISHEDNAME),		g_FamilyOnPremises, { }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ONPREMISESIMMUTABLEID),			g_FamilyOnPremises, { }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ONPREMISESLASTSYNCDATETIME),		g_FamilyOnPremises, { }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER),	g_FamilyOnPremises, { g_ColumnsPresetTech }),

				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ONPREMISESDOMAINNAME),						g_FamilyOnPremises, { }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ONPREMISESSAMACCOUNTNAME),					g_FamilyOnPremises, { }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_USER_ONPREMISESUSERPRINCIPALNAME),					g_FamilyOnPremises, { }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY),				g_FamilyOnPremises, { }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME),		g_FamilyOnPremises, { }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR),	g_FamilyOnPremises, { }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE),				g_FamilyOnPremises, { }),
			});

		for (size_t i = 0; i < GridTemplateUsers::g_NumOPExtensionAttributes; ++i)
		{
			const auto uid = wstring(_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES)) + _YUID(".extensionAttribute") + Str::getStringFromNumber(i + 1);
			ASSERT(nullptr == m_Template.GetColumnForProperty(uid));
			AddColumnToCategory(cat, m_Template.AddDefaultColumnFor(*this, uid, g_FamilyOnPremises, { }));
		}
	}

	{
		static const wstring g_FamilyDrive = _T("OneDrive");
		auto cat = AddCategoryTop(_T("OneDrive Info"),
			{
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVENAME),					g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEID),					g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEDESCRIPTION),			g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEQUOTASTATE),			g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetDefault }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEQUOTAUSED),			g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetDefault }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEQUOTAREMAINING),		g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEQUOTADELETED),			g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEQUOTATOTAL),			g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetDefault }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATIONTIME),			g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVELASTMODIFIEDDATETIME),	g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVETYPE),					g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEWEBURL),				g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYUSEREMAIL),	g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYUSERID),		g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYAPPNAME),		g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYAPPID),		g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYDEVICENAME),	g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVECREATEDBYDEVICEID),	g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERUSERNAME),		g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERUSERID),			g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERUSEREMAIL),		g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERAPPNAME),			g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERAPPID),			g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERDEVICENAME),		g_FamilyDrive, { g_ColumnsPresetMore }),
				m_Template.AddDefaultColumnFor(*this, _YUID(O365_COLDRIVEOWNERDEVICEID),		g_FamilyDrive, { g_ColumnsPresetMore, g_ColumnsPresetTech }),
			});
	}

	m_Template.m_ColumnID = GetColId();
	m_Template.CustomizeGrid(*this);

	AddColumnForRowPK(GetColId());
	//AddColumnForRowPK(m_Template.m_ColumnUserType);

	addSystemTopCategory();

	if (m_ForceSorting)
		AddSorting(m_Template.m_ColumnDisplayName, GridBackendUtil::ASC);

	m_Frame = dynamic_cast<FrameUsers*>(GetParentFrame());
	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_Frame);
}

wstring GridUsers::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, nullptr != m_Template.m_OnPremCols.m_ColCommonDisplayName ? m_Template.m_OnPremCols.m_ColCommonDisplayName : m_Template.m_ColumnDisplayName);
}

void GridUsers::customizeGridPostProcess()
{
	O365Grid::customizeGridPostProcess();
	SetNoFieldTextAndIcon(GetColumnsByPresets({ g_ColumnsPresetMore }), IDB_ICON_LM, YtriaTranslate::Do(GridUsers_customizeGridPostProcess_1, _YLOC("To see this information, use the 'Load Info' button ")).c_str(), YtriaTranslate::Do(GridUsers_customizeGridPostProcess_2, _YLOC(" - Use the 'Load Info' button to display additional information - ")).c_str());
	SetNoFieldTextAndIcon(m_PowerShellCols, IDB_ICON_GET_MAILBOX_INFO, _T("To see this information, use the 'Get Mailbox Info' button "), _T(" - Use the 'Get Mailbox Info' button to display mailbox information (login to PowerShell required)"));
	SetLoadMoreStatus(true,		YtriaTranslate::Do(GridUsers_customizeGridPostProcess_3, _YLOC("Loaded")).c_str(),		IDB_ICON_LMSTATUS_LOADED);
	SetLoadMoreStatus(false,	YtriaTranslate::Do(GridUsers_customizeGridPostProcess_4, _YLOC("Not Loaded")).c_str(),	IDB_ICON_LMSTATUS_NOTLOADED);
}

void GridUsers::SetForceSorting(bool p_ForceSorting)
{
	m_ForceSorting = p_ForceSorting;
}

GridBackendColumn* GridUsers::GetOPAttributeColumn(int p_Index) const
{
	return m_Template.GetOPAttributeColumn(p_Index);
}

void GridUsers::ShowOnPremiseUsers(const vector<OnPremiseUser>& p_Users)
{
	GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS);
	GridUpdater updater(*this, options);

	GridIgnoreModification ignoramus(*this);

	bool firstOnPremLoad = false;
	if (!p_Users.empty())
	{
		if (m_Template.m_OnPremCols.Create(*this))
		{
			if (nullptr == GetSecondaryStatusColumn())
				AddSecondaryColumnStatus(_T("On-Premises status"));

			firstOnPremLoad = true;
			ASSERT(nullptr != m_Template.m_ColumnDisplayName && nullptr != m_Template.m_OnPremCols.m_ColCommonDisplayName);
			if (nullptr != m_Template.m_ColumnDisplayName && nullptr != m_Template.m_OnPremCols.m_ColCommonDisplayName)
			{
				auto cat1 = m_Template.m_ColumnDisplayName->GetTopCategory();
				auto cat2 = m_Template.m_OnPremCols.m_ColCommonDisplayName->GetTopCategory();
				ASSERT(nullptr != cat1 && nullptr != cat2);
				if (nullptr != cat1 && nullptr != cat2)
					GetBackend().SwapCategories(cat2, cat1);

				m_Template.m_ColumnMetaType = m_Template.m_OnPremCols.m_ColCommonMetaType;
			}
		}
	}

	bool isHybridEnvironment = false;

	std::set<PooledString> newImmutableIds;
	for (const auto& user : p_Users)
	{
		auto row = m_Template.UpdateRow(*this, user, m_O365RowPk, m_OnPremRowPk, newImmutableIds);
		isHybridEnvironment = isHybridEnvironment || nullptr != row && (m_Template.IsOnPremMetatype(*row) || m_Template.IsHybridMetatype(*row));
	}

	std::set<PooledString> removedOnPremImmutableIds;
	for (const auto& kv : m_OnPremRowPk)
	{
		const wstring& immutableId = kv.first;
		if (newImmutableIds.find(immutableId) == newImmutableIds.end())
			removedOnPremImmutableIds.insert(immutableId);
	}

	for (const auto& removedId : removedOnPremImmutableIds)
	{
		auto ittO365 = m_O365RowPk.find(removedId);
		if (ittO365 != m_O365RowPk.end())
		{
			// Removing onPrem user that was hybrid
			GridBackendRow* toRemove = GetRowIndex().GetExistingRow(ittO365->second);
			ASSERT(nullptr != toRemove);
			if (nullptr != toRemove)
			{
				m_Template.SetO365(*this, *toRemove, BusinessUser::g_UserTypeGuest == toRemove->GetField(m_Template.m_ColumnUserType).GetValueStr());
				m_Template.m_OnPremCols.SetBlankFields(*toRemove);
				m_OnPremRowPk.erase(removedId);
			}
		}
		else
		{
			// Removing onPrem user that was only onPrem
			auto ittOnPrem = m_OnPremRowPk.find(removedId);
			ASSERT(ittOnPrem != m_OnPremRowPk.end()); // Removing onPrem user that was not onPrem - should not happen
			if (ittOnPrem != m_OnPremRowPk.end())
			{
				GridBackendRow* toRemove = GetRowIndex().GetExistingRow(ittOnPrem->second);
				ASSERT(nullptr != toRemove);
				if (nullptr != toRemove)
				{
					RemoveRow(toRemove, false);
					m_OnPremRowPk.erase(removedId);
				}
			}
		}
	}

	ASSERT(isHybridEnvironment == !p_Users.empty()); // If we have at least one OnPrem user, we are hybrid, aren't we?
	if (isHybridEnvironment)
	{
		if (!m_OnPremModifications)
			m_OnPremModifications = make_unique<GridFieldModificationsOnPrem>(*this);

		if (firstOnPremLoad)
		{
			// Show meta type column
			SetColumnsVisible(GetColumnsByPresets({ g_ColumnsPresetOnPremDefault }), true, false);

			// Fill new cells in o365 rows
			for (auto r : GetBackendRows())
			{
				if (!r->HasField(GetSecondaryStatusColumn()))
				{
					r->AddField(PooledString::g_EmptyString, GetSecondaryStatusColumn()).RemoveModified();
					m_Template.UpdateCommonInfo(r);
					m_Template.SetO365Metatype(*r);
				}
			}
		}
	}
}

void GridUsers::SetOnPremiseUsersUpdateResult(const vector<O365UpdateOperation>& p_UpdateResults)
{
	if (!p_UpdateResults.empty())
	{
		// On O365 side, it's made by the refresh triggered after apply.
		ModuleUtil::ClearErrorRowsStatus(*this);

		const uint32_t options = GridUpdaterOptions::UPDATE_GRID
			//| GridUpdaterOptions::FULLPURGE
			| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			//| GridUpdaterOptions::REFRESH_PROCESS_DATA
			| GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG
			//| GridUpdaterOptions::PROCESS_LOADMORE
			;

		GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateResults, m_OnPremModifications.get());
	}
}

GridUsers::OnPremiseChanges GridUsers::GetOnPremiseChanges(bool p_SelectedRowsOnly/* = false*/)
{
	OnPremiseChanges onPremiseChanges;

	if (m_OnPremModifications && m_OnPremModifications->HasModifications())
	{
		for (const auto& fieldMod : m_OnPremModifications->GetRowFieldModifications())
		{
			if (Modification::IsLocal()(fieldMod))
			{
				const auto& rowKey = fieldMod->GetRowKey();

				// FIXME: We have no choice that finding the row to get the ObjectGUID. 
				// Eventually consider putting it in PK so that we have it oni field update directly.
				auto row = /*p_SelectedRowsOnly ? */GetRowIndex().GetExistingRow(rowKey)/* : nullptr*/;
				if (!p_SelectedRowsOnly || nullptr != row && row->IsSelected())
				{
					onPremiseChanges.m_UsersToUpdate.emplace_back();
					onPremiseChanges.m_UsersToUpdate.back().first = rowKey;
					auto& user = onPremiseChanges.m_UsersToUpdate.back().second;
					ASSERT(nullptr != row);
					m_Template.m_OnPremCols.SetValue(user, m_Template.m_OnPremCols.m_ColObjectGUID, row->GetField(m_Template.m_OnPremCols.m_ColObjectGUID));
					for (const auto& fu : fieldMod->GetFieldUpdates())
					{
						if (fu->GetNewValue())
							m_Template.m_OnPremCols.SetValue(user, GetColumnByID(fu->GetColumnID()), *fu->GetNewValue());
						else if (fu->GetColumnID() == m_Template.m_OnPremCols.m_ColProxyAddresses->GetID())
							m_Template.m_OnPremCols.SetValue(user, GetColumnByID(fu->GetColumnID()), row->GetField(m_Template.m_OnPremCols.m_ColProxyAddresses));
					}
				}
			}
		}
	}

	return onPremiseChanges;
}

bool GridUsers::HasOnPremiseChanges(bool p_SelectedRowsOnly /*= false*/) const
{
	return m_OnPremModifications
		&& (p_SelectedRowsOnly	? std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& row) {return m_OnPremModifications->IsModified(row); })
								: m_OnPremModifications->HasModifications());
}

void GridUsers::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		if (CRMpipe().IsFeatureSandboxed())
		{
			pPopup->ItemInsert(ID_USERGRID_SHOW_SENT_VALUES);
			pPopup->ItemInsert(); // Sep
		}

		pPopup->ItemInsert(ID_USERGRID_CHANGEPREFILTER);

		pPopup->ItemInsert(ID_USERGRID_LOADMORE);
		pPopup->ItemInsert(ID_USERGRID_GETMAILBOXINFO);
		pPopup->ItemInsert(); // Sep

		GridUsersBaseClass::OnCustomPopupMenu(pPopup, nStart);

		{
			auto pos = pPopup->ItemFindPosForCmdID(ID_MODULEGRID_DELETE);
			if (-1 == pos)
				pos = pPopup->ItemFindPosForCmdID(ID_MODULEGRID_CREATE);
			ASSERT(-1 != pos);
			if (-1 != pos)
			{
				pPopup->ItemInsert(ID_USERGRID_SETMANAGER, pos + 1);
				pPopup->ItemInsert(ID_USERGRID_REMOVEMANAGER, pos + 2);
				pPopup->ItemInsert(ID_USERGRID_RESETPASSWORD, pos + 3);
				pPopup->ItemInsert(ID_USERGRID_REVOKESESSIONS, pos + 4);
				pPopup->ItemInsert(ID_USERGRID_BATCHIMPORT, pos + 5);
				pPopup->ItemInsert(ID_USERGRID_BATCHUPDATE, pos + 6);
				pPopup->ItemInsert(ID_USERGRID_EDITONPREM, pos + 7);
			}
		}

		if (hasUsersSelected())
		{
			const auto pos = pPopup->ItemFindPosForCmdID(ID_USERGRID_SHOWMESSAGERULES);
			ASSERT(-1 != pos);
			if (-1 != pos)
				pPopup->ItemInsert(ID_USERGRID_SHOW_RECYCLEBIN, pos + 1);
		}
	}
}

void GridUsers::InitializeCommands()
{
	GridUsersBaseClass::InitializeCommands();

	m_RegisteredBusinessTypes.insert(rttr::type::get<BusinessUserGuest>().get_id());
	//m_RegisteredBusinessTypes.insert(rttr::type::get<HybridUser>().get_id());

	std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_USERGRID_REFRESH_ONPREM;
		_cmd.m_sMenuText = _T("On-premises users");
		_cmd.m_sToolbarText = _T("On-premises users");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_REFRESH_ONPREM, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_USERGRID_REFRESH_ONPREM_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Load on-premises info"),
			_T("Retrieve on-premises user properties from Active Directory.\nUse submenu 'AAD Connect options' to allow syncing of AD DS/Azure AD directly from sapio365."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID		= ID_USERGRID_LOADMORE;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridUsers_InitializeCommands_1, _YLOC("Load Info")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUsers_InitializeCommands_2, _YLOC("Load Info")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_LOADMORE, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_LOADMORE_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridUsers_InitializeCommands_3, _YLOC("Load Additional Information")).c_str(),
			YtriaTranslate::Do(GridUsers_InitializeCommands_4, _YLOC("Load detailed information for the selected entries only.\nThis includes values for Mailbox type, Keep copy of forwarded message(SMTP), SMTP forwarding address, Forwarding addresses, 'Send on behalf of' delegates, and 'Send as' delegates.\nThis query requires PowerShell authentication.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_USERGRID_GETMAILBOXINFO;
		_cmd.m_sMenuText = _T("Get Mailbox Info");
		_cmd.m_sToolbarText = _T("Get Mailbox Info");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_GETMAILBOXINFO, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_LOADMAILBOXINFO_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Load additional mailbox information"),
			_T("Load additional information about the selected user's mailbox"),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID		= ID_USERGRID_SHOW_SENT_VALUES;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridUsers_InitializeCommands_15, _YLOC("Show Values Sent to Server")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUsers_InitializeCommands_16, _YLOC("Show Values Sent to Server")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_USERGRID_SHOW_RECYCLEBIN;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridUsers_InitializeCommands_5, _YLOC("Show Recycle Bin...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUsers_InitializeCommands_6, _YLOC("Recycle Bin...")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_SHOW_RECYCLEBIN, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_RECYCLEBIN_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridUsers_InitializeCommands_7, _YLOC("Show Recycle Bin")).c_str(),
			YtriaTranslate::Do(GridUsers_InitializeCommands_8, _YLOC("This will show all deleted users.")).c_str(),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_USERGRID_SHOW_RECYCLEBIN_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridUsers_InitializeCommands_9, _YLOC("Show Recycle Bin...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_USERGRID_SHOW_RECYCLEBIN_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridUsers_InitializeCommands_10, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_USERGRID_RESETPASSWORD;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridUsers_InitializeCommands_11, _YLOC("Reset Password")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUsers_InitializeCommands_12, _YLOC("Reset Password")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_RESETPASSWORD, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_USERS_RESET_PASSWORD_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridUsers_InitializeCommands_13, _YLOC("Reset Password for Selected Users")).c_str(),
			YtriaTranslate::Do(GridUsers_InitializeCommands_14, _YLOC("Select one or more users and reset their password.\nAvailable options:\n- Define or auto-generate a new password\n- Enforce one-time password change on next login (with or without MFA)")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_USERGRID_SETMANAGER;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridUsers_InitializeCommands_18, _YLOC("Set Manager")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUsers_InitializeCommands_19, _YLOC("Set Manager")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_SETMANAGER, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_USERS_SETMANAGER_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridUsers_InitializeCommands_20, _YLOC("Set Manager for Selected Users")).c_str(),
			YtriaTranslate::Do(GridUsers_InitializeCommands_21, _YLOC("Set a new manager for the currently selected users.\nNote: To see and edit managers, you must first select the users and click the 'Load Info' button (or Ctrl+ L).")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_USERGRID_REMOVEMANAGER;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridUsers_InitializeCommands_22, _YLOC("Remove Manager")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUsers_InitializeCommands_22, _YLOC("Remove Manager")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_REMOVEMANAGER, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_USERS_REMOVEMANAGER_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridUsers_InitializeCommands_24, _YLOC("Remove Managers from Selected Users")).c_str(),
			YtriaTranslate::Do(GridUsers_InitializeCommands_17, _YLOC("Remove managers from the currently selected users.\nSelect any number of users and remove their manager. Then click the 'Save' button to save your changes to the server.\nNote: To see and edit managers, you must first select the users and click on the 'Load Info' button (or Ctrl+ L).")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_USERGRID_REVOKESESSIONS;
		_cmd.m_sMenuText = _T("Revoke access");
		_cmd.m_sToolbarText = _T("Revoke access");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_REVOKESESSIONS, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_USERS_REVOKEACCESS_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Revoke access to Office 365 applications"),
			_T("Revoke access to all Office 365 applications for selected users.\nClick the 'Save Selected' button to apply your changes."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_USERGRID_BATCHIMPORT;
		_cmd.m_sMenuText = _T("Import Users");
		_cmd.m_sToolbarText = _T("Import Users");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_BATCHIMPORT, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_BATCHIMPORT_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Import Multiple Users"),
			_T("Add multiple users to Office 365 by importing their properties from a file (Excel or CSV).\nNote: Only imports the first sheet of a workbook.\nColumns can be auto mapped to the grid if:\n- the imported file already contains column IDs or column titles in their headings.\n- or if the imported file uses the recommended column headings from the Office 365 admin center�s CSV template."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_USERGRID_BATCHUPDATE;
		_cmd.m_sMenuText = _T("Update Users");
		_cmd.m_sToolbarText = _T("Update Users");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_BATCHUPDATE, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_BATCHUPDATE_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Update Multiple Users"),
			_T("Update multiple users to Office 365 by importing their properties from a file (Excel or CSV).\nNote: Only imports the first sheet of a workbook.\nColumns can be auto mapped to the grid if:\n- the imported file already contains column IDs or column titles in their headings.\n- or if the imported file uses the recommended column headings from the Office 365 admin center�s CSV template."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_USERGRID_CHANGEPREFILTER;
		_cmd.m_sMenuText = _T("Change Options");
		_cmd.m_sToolbarText = _T("Change Options");
		_cmd.m_sTipTool = _T("Adjust filter.");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		// FIXME: specific image
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANGEMODULEOPTIONS_16x16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_CHANGEPREFILTER, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Change Options"),
			_T("Adjust filter."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_USERGRID_EDITO365;
		_cmd.m_sMenuText = _T("Edit O365");
		_cmd.m_sToolbarText = _T("Edit O365");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_EDITO365, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_EDIT_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Edit O365"),
			_T("Edit O365"),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_USERGRID_EDITONPREM;
		_cmd.m_sMenuText = _T("Edit On-Premises");
		_cmd.m_sToolbarText = _T("Edit On-Premises");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_EDITONPREM, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_USERS_EDITONPREM_16X16));

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Edit On-Premises"),
			_T("Edit On-Premises"),
			_cmd.m_sAccelText);
	}
}

void GridUsers::OnEdit()
{
	bool regular = false;
	YTestCmdUI testUI;
	{
		GridUsersBaseClass::OnUpdateEdit(&testUI);
		regular = testUI.m_bEnabled;
	}

	bool onPrem = false;
	{
		OnUpdateEditOnPrem(&testUI);
		onPrem = testUI.m_bEnabled;
	}
	ASSERT(regular || onPrem);

	enum : INT_PTR
	{
		CANCEL = IDCANCEL,
		EDITREGULAR = IDOK,
		EDITONPREM = IDYES,
	};
	const auto result = [=] {
		if (regular && onPrem)
		{
			YCodeJockMessageBox confirmation(this,
				DlgMessageBox::eIcon_Question,
				_T("What would you like to edit?"),
				_YTEXT(""),
				_YTEXT(""),
				{ { EDITREGULAR, _T("Edit O365") }, { EDITONPREM, _T("Edit On-Premises") }, { CANCEL, _T("Cancel") } });
			return confirmation.DoModal();
		}
		return regular ? (INT_PTR)EDITREGULAR : (INT_PTR)EDITONPREM;
	}();

	if (EDITREGULAR == result)
	{
		GridUsersBaseClass::OnEdit();
	}
	else if (EDITONPREM == result)
	{
		OnEditOnPrem();
	}
}

void GridUsers::OnUpdateEdit(CCmdUI* pCmdUI)
{
	BOOL enabled = FALSE;

	YTestCmdUI testUI;
	if (!enabled)
	{
		GridUsersBaseClass::OnUpdateEdit(&testUI);
		enabled = testUI.m_bEnabled;
	}

	if (!enabled)
	{
		OnUpdateEditOnPrem(&testUI);
		enabled = testUI.m_bEnabled;
	}

	pCmdUI->Enable(enabled);

	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::OnRefreshOnPrem()
{
	ModuleUtil::ClearErrorRowsStatus(*this);

	auto actionCmd = std::make_shared<ShowOnPremUsersCommand>(*m_Frame);
	CommandDispatcher::GetInstance().Execute(Command::CreateCommand(actionCmd, *m_Frame, Command::ModuleTarget::User));
}

void GridUsers::OnUpdateRefreshOnPrem(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(CanRefreshOnPrem());
	setTextFromProfUISCommand(*pCmdUI);
}

bool GridUsers::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	return m_Template.GetSnapshotValue(p_Value, p_Field, p_Column)
		|| GridUsersBaseClass::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridUsers::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	return m_Template.LoadSnapshotValue(p_Value, p_Row, p_Column)
		|| GridUsersBaseClass::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

void GridUsers::InitNewRow(GridBackendRow* p_Row)
{
	GridUsersBaseClass::InitNewRow(p_Row);

	// Powershell
	// Can't be done here, row not filled yet.
	// Moved to GridTemplateUsers::AddRow
	/*if (IsRowForLoadMore(p_Row, nullptr) && GridUtil::IsBusinessUser(p_Row, false))
	{
		auto col = m_Template.GetRequestDateColumn(UBI::MAILBOX);
		ASSERT(nullptr != col);
		if (nullptr != col)
		{
			p_Row->AddField(GetLoadMoreText(false), col, m_MailboxNotLoadedIconId).RemoveModified();
		}
	}*/
}

GridBackendRow* GridUsers::FindRow(const wstring& p_UserId)
{
	GridBackendRow* row = nullptr;

	row_pk_t pk { GridBackendField(p_UserId, m_Template.m_ColumnID)/*, GridBackendField(BusinessUser::g_UserTypeMember, m_Template.m_ColumnUserType)*/ };
	row = GetRowIndex().GetExistingRow(pk);

	/*if (nullptr == row)
	{
		pk[1] = GridBackendField(BusinessUser::g_UserTypeGuest, m_Template.m_ColumnUserType);
		row = GetRowIndex().GetExistingRow(pk);
	}*/

	return row;
}

bool GridUsers::CanRefreshOnPrem() const
{
	return hasNoTaskRunning()
		&& IsFrameConnected()
		&& GetSession()
		&& GetSession()->GetGraphCache().HasUsersDeltaCapabilities()
		&& GetSession()->GetGraphCache().GetUseOnPrem()
		&& !GetSession()->GetForceDisableLoadOnPrem()
		&& !GetBackendRows().empty()
		&& !(GetBackendRows().size() == 1 && GetBackendRows()[0]->IsError())
		&& (!m_Frame->GetOptions() || m_Frame->GetOptions()->m_CustomFilter.IsEmpty());
}

void GridUsers::LoadMoreImpl(const std::vector<GridBackendRow*>& p_Rows)
{
    ASSERT(IsFrameConnected());

	bool proceed = AskApplyPending(p_Rows);

	if (proceed)
	{
		if (nullptr != m_Frame)
		{
		    UpdatedObjectsGenerator<BusinessUser> updatedUsersGenerator;
			updatedUsersGenerator.SetObjects(GetLoadMoreRequestInfo(p_Rows));

            CommandInfo info;
            info.Data() = updatedUsersGenerator;
            info.SetFrame(m_Frame);
			info.SetRBACPrivilege(m_Frame->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::User, Command::ModuleTask::UpdateMoreInfo, info, { this, g_ActionNameSelectedUserLoadMore, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
	}
}

bool GridUsers::AskApplyPending(const std::vector<GridBackendRow*>& p_Rows)
{
	bool proceed = true;

	std::vector<GridBackendRow*> modifiedRows;
	for (auto row : p_Rows)
	{
		// FIXME: Use GridModifications?
		if (row->IsModified())
			modifiedRows.push_back(row);
	}

	if (!modifiedRows.empty())
	{
		bool revert = false;
		// FIXME: Modify this to propose partial update when this feature is available.
		YCodeJockMessageBox confirmation(
			this,
			DlgMessageBox::eIcon_Question,
			YtriaTranslate::Do(GridUsers_LoadMoreImpl_1, _YLOC("Changes pending")).c_str(),
			YtriaTranslate::Do(GridUsers_LoadMoreImpl_2, _YLOC("The selected rows contain unsaved changes that will be lost.\r\nDo you want to undo them and continue?")).c_str(),
			L"",
			{ /*{ IDOK, _TLOC("Save and continue") },*/{ IDABORT, YtriaTranslate::Do(GridUsers_LoadMoreImpl_3, _YLOC("Undo and continue")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridUsers_LoadMoreImpl_4, _YLOC("Cancel")).c_str() } });

		switch (confirmation.DoModal())
		{
		case IDOK:
			proceed = true;
			break;
		case IDCANCEL:
			proceed = false;
			break;
		case IDABORT:
			revert = true;
			proceed = true;
			break;
		default:
			ASSERT(false);
		}

		if (proceed && revert)
		{
			auto& mods = GetModifications();
			row_pk_t pk;
			bool update = false;
			for (auto row : modifiedRows)
			{
				GetRowPK(row, pk);
				if (mods.Revert(pk, true))
					update = true;
				else
					ASSERT(false);
			}

			if (update)
				UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		}
		else if (proceed && !revert)
		{
			ASSERT(false);
			// TODO: Apply selected
		}
	}
	return proceed;
}

void GridUsers::LoadMailboxInfo(const std::vector<GridBackendRow*>& p_Rows)
{
	ASSERT(IsFrameConnected());

	bool proceed = AskApplyPending(p_Rows);

	if (proceed)
	{
		if (nullptr != m_Frame)
		{
			UpdatedObjectsGenerator<BusinessUser> updatedUsersGenerator;
			updatedUsersGenerator.SetObjects(GetLoadMoreRequestInfo(p_Rows));
			
			CommandInfo info;
			info.Data() = updatedUsersGenerator;
			info.SetFrame(m_Frame);
			info.SetRBACPrivilege(m_Frame->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::User, Command::ModuleTask::UpdateMailbox, info, { this, g_ActionNameSelectedUserLoadMore, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
	}
}

std::vector<BusinessUser> GridUsers::GetLoadMoreRequestInfo(const std::vector<GridBackendRow *>& p_Rows)
{
    std::vector<BusinessUser> users;
    for (auto selectedRow : p_Rows)
    {
		if (IsRowForLoadMore(selectedRow, nullptr)) // Bug #180515.BC.00843A
		{
			BusinessUser businessUser;
			m_Template.GetBusinessUser(*this, selectedRow, businessUser);

			// avoid useless queries (multivalue explosion side effect)
			if (std::none_of(users.begin(), users.end(), [&businessUser](auto& u)
				{
					return businessUser.GetID() == u.GetID();
				}))
				users.push_back(businessUser);
		}
    }

    return users;
}

std::vector<BusinessUser> GridUsers::GetMailboxRequestInfo(const std::vector<GridBackendRow*>& p_Rows)
{
	std::vector<BusinessUser> users;
	for (auto selectedRow : p_Rows)
	{
		if (IsRowForLoadMore(selectedRow, nullptr) && GridUtil::IsBusinessUser(selectedRow, false))
		{
			BusinessUser businessUser;
			m_Template.GetBusinessUser(*this, selectedRow, businessUser);

			// avoid useless queries (multivalue explosion side effect)
			if (std::none_of(users.begin(), users.end(), [&businessUser](auto& u)
				{
					return businessUser.GetID() == u.GetID();
				}))
				users.push_back(businessUser);
		}
	}

	return users;
}

bool GridUsers::HasPasswordModifications(bool p_InSelection) const
{
	for (auto row : GetBackendRows())
	{
		if ((!p_InSelection || row->IsSelected())
			&&
			(row->IsModified() && row->GetField(m_Template.m_ColumnPassword).IsModified()
						|| row->IsCreated() && row->HasField(m_Template.m_ColumnPassword))
			
			)
			return true;
	}

	return false;
}

row_pk_t GridUsers::UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody)
{
	ASSERT(IsGridModificationsEnabled());
	if (IsGridModificationsEnabled())
	{
		auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPk);
		ASSERT(nullptr != mod);
		if (nullptr != mod)
		{
			// Check Create call that generated this data: Need to request RBAC props?
			UserDeserializer ud(GetSession());
			ud.Deserialize(json::value::parse(creationResultBody));
			auto bu = ud.GetData();
			row_pk_t newPk;
			m_Template.GetBusinessUserPK(*this, bu, newPk);

			mod->SetNewPK(newPk);

			return newPk;
		}
	}

	return rowPk;
}

void GridUsers::OnUserLoadMore()
{
	if (IsFrameConnected())
	{
		const auto selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_LOADMORE, [this](GridBackendRow* p_Row)
			{
				return IsRowForLoadMore(p_Row, nullptr);
			});

		if (!selectedRows.empty() && showUserRestrictedAccessDialog(USER_PERMISSION_REQUIREMENT))
			LoadMoreImpl(selectedRows);
	}
}

void GridUsers::OnUpdateUserLoadMore(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_LOADMORE, [this](GridBackendRow* p_Row)
	{
		return IsRowForLoadMore(p_Row, nullptr);
	}));

	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::OnUserLoadMailboxInfo()
{
	if (IsFrameConnected())
	{
		const auto selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_LOADMAILBOX, [this](GridBackendRow* p_Row)
			{
				return IsRowForLoadMore(p_Row, nullptr) && GridUtil::IsBusinessUser(p_Row, false);
			});

		if (!selectedRows.empty())
			LoadMailboxInfo(selectedRows);
	}
}

void GridUsers::OnUpdateUserLoadMailboxInfo(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_LOADMAILBOX, [this](GridBackendRow* p_Row)
		{
			return IsRowForLoadMore(p_Row, nullptr) && GridUtil::IsBusinessUser(p_Row, false);
		}));

	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::OnShowSentValues()
{
	m_ShowSentValue = !m_ShowSentValue;
    GetModifications().UpdateShownValues();
    UpdateMegaShark();
}

void GridUsers::OnUpdateShowSentValues(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(CRMpipe().IsFeatureSandboxed() && hasNoTaskRunning());
	pCmdUI->SetCheck(m_ShowSentValue);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::OnShowRecycleBin()
{
	if (IsFrameConnected())
	{
		if (openModuleConfirmation())
		{
			auto pFrame = dynamic_cast<FrameUsers*>(GetParent());
			ASSERT(nullptr != pFrame);
			if (nullptr != pFrame)
				pFrame->ShowRecycleBin();
		}
		else
		{
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"), _YR("Y2419")).c_str());
		}
	}
}

void GridUsers::OnUpdateShowRecycleBin(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsFrameConnected());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::OnResetPassword()
{
	if (showUserRestrictedAccessDialog(USER_ADMIN_REQUIREMENT))
	{
		TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool /*p_HasRowsToReExplode*/)
		{
			std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_PWD_EDIT, [](GridBackendRow* p_Row)
			{
				return !p_Row->IsGroupRow() && !p_Row->IsDeleted() && GridUtil::IsBusinessUser(p_Row);
			});

			if (!selectedRows.empty())
			{
				vector<BusinessUser> users;
				for (auto row : selectedRows)
				{
					ASSERT(GridUtil::IsBusinessUser(row));
					users.emplace_back(getBusinessObject(row));
				}

				DlgUserPasswordHTML dlg(users, this);
				if (IDOK == dlg.DoModal())
				{
					if (dlg.ShouldAutogeneratePasswords())
					{
						for (auto& user : users)
						{
							const auto password = MFCUtil::GeneratePassword(4, true, 6, true);
							user.SetPassword(PooledString(password));
						}
					}

					UpdateBusinessObjects(users, true);

					if (users[0].GetPassword()) // If we put at least a password in the grid, password column has been shown, now select it.
						GotoColumn(m_Template.m_ColumnPassword);
				}
			}
		})();
	}
}

void GridUsers::OnUpdateResetPassword(CCmdUI* pCmdUI)
{
	const auto condition = [](GridBackendRow* p_Row)
	{
		return !p_Row->IsGroupRow() && !p_Row->IsDeleted() && GridUtil::IsBusinessUser(p_Row);
	};

	pCmdUI->Enable(hasNoTaskRunning() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_PWD_EDIT, condition));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::OnSetManager()
{
	if (IsFrameConnected())
	{
		TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool /*p_HasRowsToReExplode*/)
			{
				std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MANAGER_UPDATE, [colManagerID = m_Template.m_ColumnManagerID](GridBackendRow* p_Row)
				{
					return !p_Row->IsGroupRow() && !p_Row->IsDeleted() && GridUtil::IsBusinessUser(p_Row)
						&& p_Row->IsMoreLoaded() && p_Row->GetField(colManagerID).GetIcon() != GridBackendUtil::ICON_ERROR;
				});

				if (!selectedRows.empty())
				{
					// < User ID, <User Name, Manager Id>>
					std::map<PooledString, std::pair<PooledString, std::set<PooledString>>> parentsForSelection;

					vector<BusinessUser> users; // should contain whole grid
					vector<BusinessUser> usersToModify;
					for (auto row : GetBackendRows())
					{
						if (row->IsExplosionAdded())
							continue;

						ASSERT(GridUtil::IsBusinessUser(row));
						users.emplace_back(getBusinessObject(row));

						if (selectedRows.end() != std::find(selectedRows.begin(), selectedRows.end(), row))
						{
							ASSERT(users.back().GetDisplayName());
							if (users.back().GetDisplayName())
							{
								auto res = parentsForSelection.insert({ users.back().GetID(), {*users.back().GetDisplayName(), {}} });
								if (res.second)
								{
									if (users.back().GetManagerID() && !users.back().GetManagerID()->IsEmpty())
										res.first->second.second = { *users.back().GetManagerID() };

									usersToModify.push_back(users.back());
								}
							}
						}
					}

					ASSERT(selectedRows.size() == usersToModify.size() && selectedRows.size() == parentsForSelection.size());

					DlgAddItemsToGroups dlg( // DONE
						GetSession(),
						parentsForSelection,
						DlgAddItemsToGroups::LOADTYPE::SELECT_USER_FOR_MANAGER,
						users,
						{},
						{},
						YtriaTranslate::Do(GridUsers_OnSetManager_1, _YLOC("Choose Manager for %1 users"), Str::getStringFromNumber(usersToModify.size()).c_str()),
						YtriaTranslate::Do(GridUsers_OnSetManager_2, _YLOC("These %1 users will have as manager any person you select from the list below.\r\nTo see the complete list, use Load full directory."), Str::getStringFromNumber(usersToModify.size()).c_str()),
						RoleDelegationUtil::RBAC_Privilege::RBAC_NONE,
						this);

					if (IDOK == dlg.DoModal())
					{
						auto items = dlg.GetSelectedItems();
						ASSERT(items.size() == 1);

						if (items.size() == 1)
						{
							for (auto& user : usersToModify)
								user.SetManagerID(items.back().GetID());

							UpdateBusinessObjects(usersToModify, true);
						}
					}
				}
			})();
	}
}

void GridUsers::OnUpdateSetManager(CCmdUI* pCmdUI)
{
	const auto condition = [colManagerID = m_Template.m_ColumnManagerID](GridBackendRow* p_Row)
	{
		return !p_Row->IsGroupRow() && !p_Row->IsDeleted() && (GridUtil::IsBusinessUser(p_Row))
			&& p_Row->IsMoreLoaded() && p_Row->GetField(colManagerID).GetIcon() != GridBackendUtil::ICON_ERROR;
	};

	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MANAGER_UPDATE, condition));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::OnEditOnPrem()
{
	TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool /*p_HasRowsToReExplode*/)
		{
			std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, GetPrivilegeEdit(), [this](GridBackendRow* p_Row)
				{
					return nullptr != p_Row && (m_Template.IsOnPremMetatype(*p_Row) || m_Template.IsHybridMetatype(*p_Row));
				});

			ASSERT(!selectedRows.empty());
			if (!selectedRows.empty())
			{
				bool isEditCreate = true;
				vector<OnPremiseUser> onPremiseUsers;
				std::set<wstring> fieldErrors;

				for (auto row : selectedRows)
				{
					if (nullptr != row)
					{
						onPremiseUsers.emplace_back(getOnPremiseUser(row));
						//isEditCreate = isEditCreate && (objectRow->IsCreated() || businessObjects.back().HasFlag(BusinessObject::CREATED));

						getFieldsThatHaveErrors(row, fieldErrors);
					}
				}

				DlgOnPremUserEditHTML dlg(onPremiseUsers, DlgFormsHTML::Action::EDIT, this);
				if (IDOK == dlg.DoModal())
				{
					GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
					for (const auto& user : onPremiseUsers)
						m_Template.UpdateRow(*this, user, m_O365RowPk, m_OnPremRowPk, GridTemplate::UpdateFieldOption::IS_MODIFICATION);
				}
			}
		})();
}

void GridUsers::OnUpdateEditOnPrem(CCmdUI* pCmdUI)
{
	const auto condition = [this](GridBackendRow* p_Row)
	{
		return nullptr != p_Row && (m_Template.IsOnPremMetatype(*p_Row) || m_Template.IsHybridMetatype(*p_Row));
	};

	pCmdUI->Enable(hasNoTaskRunning() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, GetPrivilegeEdit(), condition, true));

	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::OnEditO365()
{
	GridUsersBaseClass::OnEdit();
}

void GridUsers::OnUpdateEditO365(CCmdUI* pCmdUI)
{
	GridUsersBaseClass::OnUpdateEdit(pCmdUI);
}

void GridUsers::OnRemoveManager()
{
	TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool /*p_HasRowsToReExplode*/)
	{
		std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MANAGER_UPDATE, [colManagerID = m_Template.m_ColumnManagerID](GridBackendRow* p_Row)
		{
			return !p_Row->IsGroupRow() && !p_Row->IsDeleted() && (GridUtil::IsBusinessUser(p_Row))
				&& p_Row->IsMoreLoaded()
				&& p_Row->GetField(colManagerID).GetIcon() != GridBackendUtil::ICON_ERROR
				&& p_Row->GetField(colManagerID).HasValue()
				&& !PooledString(p_Row->GetField(colManagerID).GetValueStr()).IsEmpty();
		});

		if (!selectedRows.empty())
		{
			vector<BusinessUser> users;
			set<PooledString> userDisplayNames;
			for (auto row : selectedRows)
			{
				ASSERT(GridUtil::IsBusinessUser(row));
				users.emplace_back(getBusinessObject(row));
				ASSERT(users.back().GetDisplayName());
				if (users.back().GetDisplayName())
					userDisplayNames.insert(*users.back().GetDisplayName());
			}

			for (auto& user : users)
				user.SetManagerID(PooledString());

			UpdateBusinessObjects(users, true);
		}
	})();
}

void GridUsers::OnUpdateRemoveManager(CCmdUI* pCmdUI)
{
	const auto condition = [colManagerID = m_Template.m_ColumnManagerID](GridBackendRow* p_Row)
	{
		return !p_Row->IsGroupRow() && !p_Row->IsDeleted() && (GridUtil::IsBusinessUser(p_Row))
			&& p_Row->IsMoreLoaded()
			&& p_Row->GetField(colManagerID).GetIcon() != GridBackendUtil::ICON_ERROR
			&& p_Row->GetField(colManagerID).HasValue()
			&& !PooledString(p_Row->GetField(colManagerID).GetValueStr()).IsEmpty();
	};

	pCmdUI->Enable(hasNoTaskRunning() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MANAGER_UPDATE, condition));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::OnDelete()
{
	if (hasNoTaskRunning() && showUserRestrictedAccessDialog(USER_ADMIN_REQUIREMENT))
		GridUsersBaseClass::OnDelete();
}

void GridUsers::OnRevokeSessions()
{
	TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool p_HasRowsToReExplode)
		{
			std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, GetPrivilegeEdit(), [this](GridBackendRow* p_Row)
				{
					return GridUtil::IsBusinessUser(p_Row) && !p_Row->IsGroupRow() && !p_Row->IsComparatorAdded() && !p_Row->IsCreated() && RevokeAccessFieldUpdate::GetPlaceHolder() != p_Row->GetField(m_Template.m_ColumnSignInSessionsValidFromDateTime).GetValueStr();
				});

			ASSERT(!selectedRows.empty());
			if (!selectedRows.empty())
			{
				//GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
				GridUpdater updater(*this, p_HasRowsToReExplode ? GridUpdaterOptions(0) : GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID));
				for (auto objectRow : selectedRows)
				{
					if (nullptr != objectRow)
						m_Template.MarkUserRowWithRevokePending(*this, objectRow, updater);
				}				
			}
		})();
}

void GridUsers::OnUpdateRevokeSessions(CCmdUI* pCmdUI)
{
	auto selectionCondition = [this](GridBackendRow* row)
	{
		return GridUtil::IsBusinessUser(row) && !row->IsGroupRow() && !row->IsComparatorAdded() && !row->IsCreated() && RevokeAccessFieldUpdate::GetPlaceHolder() != row->GetField(m_Template.m_ColumnSignInSessionsValidFromDateTime).GetValueStr();
	};

	pCmdUI->Enable(hasNoTaskRunning() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, GetPrivilegeEdit(), selectionCondition));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::batchProcess(bool p_UpdateExisting)
{
	static const wstring userPrincipalNameTip = _T("If provided values have a missing or invalid domain, you will be asked to decide what to do later.");
	static const wstring accountEnabledTip = _YFORMAT(L"Allowable values include: '%s', 'True', '1'.", BusinessUserConfiguration::GetInstance().GetValueStringSignInStatus_Allowed().c_str());
	static const wstring passwordPoliciesTip = _T("Accepted values include: 'None', 'DisablePasswordExpiration', 'DisableStrongPassword' or 'DisablePasswordExpiration, DisableStrongPassword'.");
	static const wstring passwordTip = _T("If no password is provided, one will be generated automatically.");
	static const wstring preferredLanguageTip = _YFORMAT(L"Accepted values include either the ISO 639-1 code (e.g. 'en-US') or the language fullname (e.g. '%s').", Languages::Get().right.at(_YTEXT("en-US")).c_str());
	static const wstring usageLocationTip = _YFORMAT(L"Accepted values include either the ISO 3166 two letter country code (e.g. 'US') or the country fullname (e.g. '%s').", DlgRegAccountRegistration::GetCountries().right.at(_YTEXT("US")).c_str());

	static const BatchImporter::MappingProperties importMappingProperties
	{
		{ _YTEXT(O365_USER_DISPLAYNAME)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_DISPLAYNAME))				, true	, {} }},
		{ _YTEXT(O365_USER_USERPRINCIPALNAME)		, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_USERPRINCIPALNAME))			, true	, { userPrincipalNameTip } }},
		{ _YTEXT(O365_USER_ACCOUNTENABLED)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_ACCOUNTENABLED))				, false	, { accountEnabledTip } }},
		{ _YTEXT(O365_USER_MAILNICKNAME)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_MAILNICKNAME))				, false	, {} }}, // Mandatory for user creation but we use the Principal Name to set it if not provided.
		{ _YTEXT(O365_USER_PASSWORDPROFILE_PASSWORD), {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_PASSWORDPROFILE_PASSWORD))	, false	, { passwordTip, true } }},
		{ _YTEXT(O365_USER_GIVENNAME)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_GIVENNAME))					, false	, {} }},
		{ _YTEXT(O365_USER_SURNAME)					, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_SURNAME))					, false	, {} }},
		{ _YTEXT(O365_USER_PREFERREDLANGUAGE)		, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_PREFERREDLANGUAGE))			, false	, { preferredLanguageTip } }},
		{ _YTEXT(O365_USER_USAGELOCATION)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_USAGELOCATION))				, false	, { usageLocationTip } }},
		{ _YTEXT(O365_USER_JOBTITLE)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_JOBTITLE))					, false	, {} }},
		{ _YTEXT(O365_USER_COMPANYNAME)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_COMPANYNAME))				, false	, {} }},
		{ _YTEXT(O365_USER_DEPARTMENT)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_DEPARTMENT))					, false	, {} }},
		{ _YTEXT(O365_USER_BUSINESSPHONES)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_BUSINESSPHONES))				, false	, {} }},
		{ _YTEXT(O365_USER_MOBILEPHONE)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_MOBILEPHONE))				, false	, {} }},
		{ _YTEXT(O365_USER_FAXNUMBER)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_FAXNUMBER))					, false	, {} }},
		{ _YTEXT(O365_USER_OFFICELOCATION)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_OFFICELOCATION))				, false	, {} }},
		{ _YTEXT(O365_USER_STREETADDRESS)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_STREETADDRESS))				, false	, {} }},
		{ _YTEXT(O365_USER_CITY)					, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_CITY))						, false	, {} }},
		{ _YTEXT(O365_USER_STATE)					, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_STATE))						, false	, {} }},
		{ _YTEXT(O365_USER_POSTALCODE)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_POSTALCODE))					, false	, {} }},
		{ _YTEXT(O365_USER_COUNTRY)					, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_COUNTRY))					, false	, {} }},
		{ _YTEXT(O365_USER_PASSWORDPOLICIES)		, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_PASSWORDPOLICIES))			, false	, { passwordPoliciesTip } }},
	};

	static const wstring showInAddressListTip = _T("Allowable values include: 'True', '1'.");
	static const wstring isResourceAccountTip = _T("Allowable values include: 'True', '1'.");

#define ONPREMISESEXTENSIONATTRIBUTES(_strNum) \
{ _YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute") _strNum, { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute") _strNum), false, {} }},

	static const BatchImporter::MappingProperties updateMappingProperties
	{
		{ _YTEXT(O365_USER_DISPLAYNAME)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_DISPLAYNAME))				, false	, {} }},
		{ _YTEXT(O365_USER_USERPRINCIPALNAME)		, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_USERPRINCIPALNAME))			, false	, { userPrincipalNameTip } }},
		{ _YTEXT(O365_USER_ACCOUNTENABLED)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_ACCOUNTENABLED))				, false	, { accountEnabledTip } }},
		{ _YTEXT(O365_USER_ISRESOURCEACCOUNT)		, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_ISRESOURCEACCOUNT))			, false , { isResourceAccountTip } }},
		{ _YTEXT(O365_USER_GIVENNAME)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_GIVENNAME))					, false	, {} }},
		{ _YTEXT(O365_USER_SURNAME)					, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_SURNAME))					, false	, {} }},
		{ _YTEXT(O365_USER_PREFERREDLANGUAGE)		, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_PREFERREDLANGUAGE))			, false	, { preferredLanguageTip } }},
		{ _YTEXT(O365_USER_USAGELOCATION)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_USAGELOCATION))				, false	, { usageLocationTip } }},
		{ _YTEXT(O365_USER_SHOWINADDRESSLIST)		, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_SHOWINADDRESSLIST))			, false	, { showInAddressListTip } }},
		{ _YTEXT(O365_USER_MAILNICKNAME)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_MAILNICKNAME))				, false	, {} }},
		{ _YTEXT(O365_USER_EMPLOYEEID)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_EMPLOYEEID))					, false	, {} }},
		{ _YTEXT(O365_USER_JOBTITLE)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_JOBTITLE))					, false	, {} }},
		{ _YTEXT(O365_USER_COMPANYNAME)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_COMPANYNAME))				, false	, {} }},
		{ _YTEXT(O365_USER_DEPARTMENT)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_DEPARTMENT))					, false	, {} }},
		{ _YTEXT(O365_USER_BUSINESSPHONES)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_BUSINESSPHONES))				, false	, {} }},
		{ _YTEXT(O365_USER_MOBILEPHONE)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_MOBILEPHONE))				, false	, {} }},
		{ _YTEXT(O365_USER_FAXNUMBER)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_FAXNUMBER))					, false	, {} }},
		{ _YTEXT(O365_USER_OFFICELOCATION)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_OFFICELOCATION))				, false	, {} }},
		{ _YTEXT(O365_USER_STREETADDRESS)			, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_STREETADDRESS))				, false	, {} }},
		{ _YTEXT(O365_USER_CITY)					, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_CITY))						, false	, {} }},
		{ _YTEXT(O365_USER_STATE)					, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_STATE))						, false	, {} }},
		{ _YTEXT(O365_USER_POSTALCODE)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_POSTALCODE))					, false	, {} }},
		{ _YTEXT(O365_USER_COUNTRY)					, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_COUNTRY))					, false	, {} }},
		// loadmore { _YTEXT(O365_USER_MYSITE)					, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_MYSITE))					, false	, {} }},
		{ _YTEXT(O365_USER_PASSWORDPOLICIES)		, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_PASSWORDPOLICIES))			, false	, { passwordPoliciesTip } }},
		// loadmore { _YTEXT(O365_USER_ABOUTME)					, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_ABOUTME))					, false	, {} }},
		// loadmore { _YTEXT(O365_USER_BIRTHDAY)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_BIRTHDAY))					, false	, {} }},
		// loadmore { _YTEXT(O365_USER_HIREDATE)				, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_HIREDATE))					, false	, {} }},
		{ _YTEXT(O365_USER_ONPREMISESIMMUTABLEID)	, {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_ONPREMISESIMMUTABLEID))					, false	, {} }},
		//{ _YTEXT(O365_USER_PASSWORDPROFILE_PASSWORD), {	BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_PASSWORDPROFILE_PASSWORD))	, false	, { passwordTip, true } }},
		ONPREMISESEXTENSIONATTRIBUTES(L"1")
		ONPREMISESEXTENSIONATTRIBUTES(L"2")
		ONPREMISESEXTENSIONATTRIBUTES(L"3")
		ONPREMISESEXTENSIONATTRIBUTES(L"4")
		ONPREMISESEXTENSIONATTRIBUTES(L"5")
		ONPREMISESEXTENSIONATTRIBUTES(L"6")
		ONPREMISESEXTENSIONATTRIBUTES(L"7")
		ONPREMISESEXTENSIONATTRIBUTES(L"8")
		ONPREMISESEXTENSIONATTRIBUTES(L"9")
		ONPREMISESEXTENSIONATTRIBUTES(L"10")
		ONPREMISESEXTENSIONATTRIBUTES(L"11")
		ONPREMISESEXTENSIONATTRIBUTES(L"12")
		ONPREMISESEXTENSIONATTRIBUTES(L"13")
		ONPREMISESEXTENSIONATTRIBUTES(L"14")
		ONPREMISESEXTENSIONATTRIBUTES(L"15")
	};

#undef ONPREMISESEXTENSIONATTRIBUTES

	const auto& mappingProperties = p_UpdateExisting ? updateMappingProperties : importMappingProperties;

	static const BatchImporter::KeyProperties updateKeyProperties
	{
		{ _YTEXT(O365_ID), { GetColId()->GetTitleDisplayed() }},
		{ _YTEXT(O365_USER_USERPRINCIPALNAME), { BusinessUserConfiguration::GetInstance().GetTitle(_YTEXT(O365_USER_USERPRINCIPALNAME)) }},
	};

	std::vector<wstring> domains;
	{
		auto org = GetGraphCache().GetRBACOrganization();
		ASSERT(org);
		if (org)
			domains = org->GetDomains();
	}

	INT_PTR invalidDomainAction = 0;
	auto callback = [this, &mappingProperties, &domains, &invalidDomainAction](BusinessUser& newUser, const std::map<wstring, wstring>& p_RowValues)
	{
		const bool isUpdate = !IsTemporaryCreatedObjectID(newUser.GetID()); // FIME: Maybe find a better solution.

		wstring error;
		bool cancelCurrent = false;		

		boost::YOpt<wstring> foundMailNickName;
		bool pnDone = false;
		for (const auto& rowValue : p_RowValues)
		{
			// Bools
			if (rowValue.first == _YTEXT(O365_USER_ACCOUNTENABLED))
			{
				const auto enabled = !MFCUtil::StringMatchW(rowValue.second, BusinessUserConfiguration::GetInstance().GetValueStringSignInStatus_Blocked())
					&& (MFCUtil::StringMatchW(rowValue.second, BusinessUserConfiguration::GetInstance().GetValueStringSignInStatus_Allowed())
						|| MFCUtil::StringMatchW(rowValue.second, _YTEXT("True"))
						|| MFCUtil::StringMatchW(rowValue.second, _YTEXT("1")));
				newUser.SetAccountEnabled(boost::YOpt<bool>(enabled));
			}
			else if (rowValue.first == _YTEXT(O365_USER_SHOWINADDRESSLIST))
			{
				const auto enabled = MFCUtil::StringMatchW(rowValue.second, _YTEXT("True"))
									|| MFCUtil::StringMatchW(rowValue.second, _YTEXT("1"));
				newUser.SetAccountEnabled(boost::YOpt<bool>(enabled));
			}
			// Specific strings
			else if (rowValue.first == _YTEXT(O365_USER_MAILNICKNAME))
			{
				ASSERT(!pnDone);
				foundMailNickName = rowValue.second;
				if (isUpdate)
					newUser.SetMailNickname(PooledString(rowValue.second));
			}
			else if (rowValue.first == _YTEXT(O365_USER_USERPRINCIPALNAME))
			{
				pnDone = true;

				auto defaultPN = newUser.GetUserPrincipalName();
				ASSERT(isUpdate || defaultPN && ((wstring)(*defaultPN))[0] == L'@');
				auto exploded = Str::explodeIntoVector(rowValue.second, wstring(_YTEXT("@")), true, true);

				if (exploded.size() > 2 || exploded.size() > 0 && exploded[0].empty())
				{
					error = _YFORMAT(L"Invalid Username '%s'", rowValue.second.c_str());
					break;
				}

				if (!isUpdate)
				{
					if (foundMailNickName)
						newUser.SetMailNickname(PooledString(*foundMailNickName));
					else
						newUser.SetMailNickname(PooledString(exploded[0]));
				}

				const auto emptyDomain = exploded.size() == 1 || exploded.size() == 2 && exploded[1].empty();
				const auto invalidDomain = (exploded.size() == 2 && domains.end() == std::find_if(domains.begin(), domains.end(), [&exploded](auto& val) {return MFCUtil::StringMatchW(val, exploded[1]); }));
				if (emptyDomain || invalidDomain)
				{
					wstring proposal = exploded[0] + defaultPN->c_str();

					auto action = invalidDomainAction;
					if (0 == action)
					{
						YCodeJockMessageBox dlg(this
							, DlgMessageBox::eIcon_Question
							, emptyDomain	? _YFORMAT(L"'%s' is an invalid username, domain is missing.", rowValue.second.c_str())
											: _YFORMAT(L"'%s' is an invalid username, domain is unknown.", rowValue.second.c_str())
							, _YFORMAT(L"Would you like to use the default domain (%s), skip this user or cancel the whole import?", proposal.c_str())
							, _YTEXT("")
							, { { IDOK, _T("Use default") }, { IDABORT, _T("Skip") }, { IDCANCEL, _T("Cancel") } });
						dlg.SetVerificationText(_T("Do this for all users."));
						action = dlg.DoModal();
						if (dlg.IsVerificiationChecked())
							invalidDomainAction = action;
					}

					switch (action)
					{
					case IDOK:
						newUser.SetUserPrincipalName(PooledString(proposal));
						break;
					case IDCANCEL:
						error = _T("Canceled by user.");
						break;
					case IDABORT:
						cancelCurrent = true;
						break;
					}

					if (cancelCurrent || !error.empty())
						break;
				}
				else
				{
					newUser.SetUserPrincipalName(PooledString(Str::implode(exploded, _YTEXT("@"), false)));
				}
			}
			else if (rowValue.first == _YTEXT(O365_USER_PASSWORDPOLICIES))
			{
				if (MFCUtil::StringMatchW(rowValue.second, _YTEXT("None"))
					|| MFCUtil::StringMatchW(rowValue.second, _YTEXT("DisablePasswordExpiration"))
					|| MFCUtil::StringMatchW(rowValue.second, _YTEXT("DisableStrongPassword"))
					|| MFCUtil::StringMatchW(rowValue.second, _YTEXT("DisablePasswordExpiration, DisableStrongPassword")))
				{
					newUser.SetPasswordPolicies(PooledString(rowValue.second));
				}
				else
				{
					error = _YFORMATERROR(L"Invalid password policy: %s", rowValue.second.c_str());
					break;
				}
			}
			else if (rowValue.first == _YTEXT(O365_USER_BUSINESSPHONES))
			{
				newUser.SetBusinessPhone(PooledString(rowValue.second));
			}
			else if (rowValue.first == _YTEXT(O365_USER_PREFERREDLANGUAGE))
			{
				const auto& languages = Languages::Get();
				auto itr = languages.right.find(rowValue.second);
				if (languages.right.end() != itr)
				{
					newUser.SetPreferredLanguage(PooledString(itr->first));
				}
				else
				{
					auto itl = languages.left.find(rowValue.second);
					if (languages.left.end() != itl)
					{
						newUser.SetPreferredLanguage(PooledString(itl->second));
					}
					else
					{
						// Alert user?
					}
				}
			}
			else if (rowValue.first == _YTEXT(O365_USER_USAGELOCATION))
			{
				const auto& countries = DlgRegAccountRegistration::GetCountries();
				auto itr = countries.right.find(rowValue.second);
				if (countries.right.end() != itr)
				{
					newUser.SetUsageLocation(PooledString(itr->first));
				}
				else
				{
					auto itl = countries.left.find(rowValue.second);
					if (countries.left.end() != itl)
					{
						newUser.SetUsageLocation(PooledString(itl->second));
					}
					else
					{
						// Alert user?
					}
				}
			}
			else if (Str::beginsWith(rowValue.first, _YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute")))
			{
				constexpr auto offset = Str::constexprLength((_YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute")));
				const auto index = Str::getIntFromString(rowValue.first.substr(offset));

#define CASE_SET_ATTRIBUTE(_num) \
			case _num: \
				newUser.SetOnPremisesExtensionAttribute##_num##(PooledString(rowValue.second)); \
				break; \

				switch (index)
				{
					CASE_SET_ATTRIBUTE(1)
					CASE_SET_ATTRIBUTE(2)
					CASE_SET_ATTRIBUTE(3)
					CASE_SET_ATTRIBUTE(4)
					CASE_SET_ATTRIBUTE(5)
					CASE_SET_ATTRIBUTE(6)
					CASE_SET_ATTRIBUTE(7)
					CASE_SET_ATTRIBUTE(8)
					CASE_SET_ATTRIBUTE(9)
					CASE_SET_ATTRIBUTE(10)
					CASE_SET_ATTRIBUTE(11)
					CASE_SET_ATTRIBUTE(12)
					CASE_SET_ATTRIBUTE(13)
					CASE_SET_ATTRIBUTE(14)
					CASE_SET_ATTRIBUTE(15)
				}
			}
			// Regular strings
			else
			{
				// FIXME: Should we do it with ifs instead of generic impl?
				if (mappingProperties.end() != mappingProperties.find(rowValue.first))
				{
					rttr::type classType = rttr::type::get<BusinessUser>();
					rttr::property prop = classType.get_property(Str::convertToASCII(rowValue.first));

					ASSERT(prop.get_type() == rttr::type::get<boost::YOpt<PooledString>>());
					if (prop.get_type() == rttr::type::get<boost::YOpt<PooledString>>())
					{
						bool success = prop.set_value(newUser, boost::YOpt<PooledString>(rowValue.second));
						ASSERT(success);
					}
				}
			}
		}

		if (!isUpdate && !cancelCurrent && error.empty() && !newUser.GetPassword())
		{
			const auto password = MFCUtil::GeneratePassword(4, true, 6, true);
			newUser.SetPassword(PooledString(password));
		}

		return std::pair<bool, wstring>{ error.empty() && !cancelCurrent, cancelCurrent ? _YTEXT("") : error };
	};


	if (p_UpdateExisting)
	{
		vector<BusinessUser> newUsers;

		INT_PTR conflictAction = 0;
		INT_PTR noMatchAction = 0;

		auto callbackUpdate = [this, callback, &newUsers, &mappingProperties, &domains, &invalidDomainAction, &conflictAction, &noMatchAction](const std::pair<wstring, wstring>& p_Key, const std::map<wstring, wstring>& p_RowValues)
		{
			ASSERT(!p_Key.first.empty() && !p_Key.second.empty());
			auto keyCol = GetColumnByUniqueID(p_Key.first);

			ASSERT(keyCol->IsString());

			std::vector<GridBackendRow*> rows;
			if (GetRowsByCriteria(rows, [keyCol, val = p_Key.second](auto row) {return val == row->GetField(keyCol).GetValueStr(); }))
			{
				if (rows.size() > 1) // Conflict.
				{
					auto action = conflictAction;
					if (0 == action)
					{
						YCodeJockMessageBox dlg(this
							, DlgMessageBox::eIcon_Question
							, _YFORMAT(L"%s users found for key '%s'", p_Key.second.c_str())
							, _T("Would you like to update them all, skip them or cancel the whole import?")
							, _YTEXT("")
							, { { IDOK, _T("Update all") }, { IDABORT, _T("Skip") }, { IDCANCEL, _T("Cancel") } });
						dlg.SetVerificationText(_T("Do this for all users."));
						action = dlg.DoModal();
						if (dlg.IsVerificiationChecked())
							conflictAction = action;
					}

					switch (action)
					{
					case IDOK:
						break;
					case IDCANCEL:
						return wstring(_T("Canceled by user."));
						break;
					case IDABORT:
						return Str::g_EmptyString;
					}
				}

				for (auto& r : rows)
				{
					auto user = getBusinessObject(r);
					newUsers.emplace_back(user);

					auto& newUser = newUsers.back();
					auto ret = callback(newUser, p_RowValues);
					if (!ret.first)
						newUsers.pop_back();
					if (!ret.second.empty()) // Stop on error
						return ret.second;
				}

				return Str::g_EmptyString;
			}

			auto action = noMatchAction;
			if (0 == action)
			{
				YCodeJockMessageBox dlg(this
					, DlgMessageBox::eIcon_Question
					, _YFORMAT(L"No user found for key '%s'", p_Key.second.c_str())
					, _T("Would you like to skip it or cancel the whole import?")
					, _YTEXT("")
					, { { IDOK, _T("Skip") }, { IDCANCEL, _T("Cancel") } });
				dlg.SetVerificationText(_T("Do this for all users."));
				action = dlg.DoModal();
				if (dlg.IsVerificiationChecked())
					noMatchAction = action;
			}

			if (IDCANCEL == action)
				return wstring(_T("Canceled by user."));

			// Can't find matching row. Ask user?
			return Str::g_EmptyString; // Continue
			//return wstring(_T("Can't find user to update.")); // Stop
		};

		if (BatchImporter::DoUpdate(BatchImporter::User, updateKeyProperties, mappingProperties, callbackUpdate, this) && !newUsers.empty())
			UpdateBusinessObjects(newUsers, true);
	}
	else
	{
		BusinessUser templateUser;
		templateUser.SetAccountEnabled(true);
		templateUser.SetFlags(BusinessObject::CREATED);
		templateUser.SetUserType(PooledString(BusinessUser::g_UserTypeMember));
		{
			auto org = GetGraphCache().GetRBACOrganization();
			ASSERT(org);
			if (org)
				templateUser.SetUserPrincipalNameDomain(PooledString(org->GetDefaultDomain()));
		}

		vector<BusinessUser> newUsers;

		INT_PTR missingMandatoryAction = 0;

		auto callbackImport = [this, callback, &newUsers, &templateUser, &mappingProperties, &domains, &invalidDomainAction, &missingMandatoryAction](const std::map<wstring, wstring>& p_RowValues)
		{
			bool missingMandatoryValue = false;
			auto action = missingMandatoryAction;

			for (const auto& prop : mappingProperties)
			{
				if (std::get<1>(prop.second))
				{
					if (p_RowValues.end() == p_RowValues.find(prop.first))
					{
						missingMandatoryValue = true;
						if (0 == action)
						{
							YCodeJockMessageBox dlg(this
								, DlgMessageBox::eIcon_Question
								, _YFORMAT(L"'%s' is missing whereas it's mandatory for user creation.", std::get<0>(prop.second).c_str())
								, _T("Would you like to skip this user or cancel the whole import?")
								, _YTEXT("")
								, { { IDABORT, _T("Skip") }, { IDCANCEL, _T("Cancel") } });
							dlg.SetVerificationText(_T("Do this for all users."));
							action = dlg.DoModal();
							if (dlg.IsVerificiationChecked())
								missingMandatoryAction = action;
						}

						break;
					}
				}
			}

			if (missingMandatoryValue)
			{
				if (IDCANCEL == action)
					return wstring(_T("Canceled by user."));

				return Str::g_EmptyString; // Continue
				//return wstring(_T("Missing mandatory value.")); // Stop
			}

			newUsers.emplace_back(templateUser);
			auto& newUser = newUsers.back();
			newUser.SetID(GetNextTemporaryCreatedObjectID());
			auto ret = callback(newUser, p_RowValues);
			if (!ret.first)
				newUsers.pop_back();
			return ret.second;
		};

		if (BatchImporter::DoCreate(BatchImporter::User, mappingProperties, callbackImport, this) && !newUsers.empty())
			AddCreatedBusinessObjects(newUsers, false);
	}
}

void GridUsers::OnBatchImport()
{
	batchProcess(false);
}

void GridUsers::OnUpdateBatchImport(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(HasCreate() && hasNoTaskRunning() && IsAuthorizedByRoleDelegation(GetPrivilegeCreate()));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::OnBatchUpdate()
{
	batchProcess(true);
}

void GridUsers::OnUpdateBatchUpdate(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(HasEdit() && !GetBackendRows().empty() && hasNoTaskRunning() && IsAuthorizedByRoleDelegation(GetPrivilegeEdit()));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsers::OnChangePreFilter()
{
	if (IsFrameConnected())
	{
		ASSERT(m_Frame->GetOptions());
		DlgUsersModuleOptions dlgOptions(m_Frame, GetSession(), m_Frame->GetOptions() ? *m_Frame->GetOptions() : ModuleOptions());
		if (dlgOptions.DoModal() == IDOK)
		{
			CommandInfo info;
			info.SetFrame(m_Frame);
			info.SetOrigin(GetOrigin());
			info.SetRBACPrivilege(m_Frame->GetModuleCriteria().m_Privilege);
			info.Data2() = dlgOptions.GetOptions();

			// FIXME: Automation action name !!!
			Command command(m_Frame->GetLicenseContext(), m_Frame->GetSafeHwnd(), Command::ModuleTarget::User, Command::ModuleTask::ListWithPrefilter, info, { this, g_ActionNameRefreshAll, GetAutomationActionRecording(), nullptr });
			CommandDispatcher::GetInstance().Execute(command);
		}
	}
}

void GridUsers::OnUpdateChangePreFilter(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && m_Frame->GetOptions() && !m_Frame->GetOptions()->m_CustomFilter.IsEmpty());
	setTextFromProfUISCommand(*pCmdUI);
}

//void GridUsers::OnCreateGuest()
//{
//	bool done = false;
//
//	DlgSimpleEditHTML dlg(this);
//
//	while (!done)
//	{
//		if (IDOK == dlg.DoModal())
//		{
//			const auto& text = dlg.GetText();
//
//			if (wstring::npos == Str::find(text, _YTEXT("@")))
//			{
//				// Invalid ! (dialog should handle this)
//				YCodeJockMessageBox msgBox(this
//					, DlgMessageBox::eIcon_Error
//					, YtriaTranslate::Do(AutomatedApp_ProcessWndProcException_9, _YLOC("Invalid e-mail")).c_str()
//					, YtriaTranslate::Do(DlgEnterNewEmailHTML_generateJSONScriptData_1, _YLOC("Please enter a valid e-mail")).c_str()
//					, _YTEXT("")
//					, { {IDOK, YtriaTranslate::Do(DlgExportTo_OnOK_8, _YLOC("Retry")).c_str()}, {IDCANCEL, YtriaTranslate::Do(GridUsers_OnDelete_4, _YLOC("Cancel")).c_str()} }
//				);
//				if (IDCANCEL == msgBox.DoModal())
//					done = true;
//			}
//			else
//			{
//				BusinessUserGuest newGuest;
//				newGuest.SetUserType(PooledString(BusinessUser::g_UserTypeGuest));
//			}
//		}
//	}
//}
//
//void GridUsers::OnUpdateCreateGuest(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable(IsFrameConnected() && hasNoTaskRunning());
//	setTextFromProfUISCommand(*pCmdUI);
//}
BOOL GridUsers::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return GridUsersBaseClass::PreTranslateMessage(pMsg);
}

INT_PTR GridUsers::showDialog(vector<BusinessUser>& p_Objects, const std::set<wstring>& /*p_ListFieldErrors*/, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
{
	if (DlgFormsHTML::Action::CREATE == p_Action)
	{
		ASSERT(1 == p_Objects.size());

		// We can't put this assert because of no operator== but the lone object here should be a default constructed one.
		// ASSERT(p_Objects.back() == BusinessUser());

		auto& clearUnusedValues = [](BusinessUser& p_User)
		{
			p_User.SetParentGroups({});
			p_User.SetFirstLevelParentGroups({});
			p_User.SetManagerID(boost::none);
			p_User.SetManagerError(boost::none);
			p_User.SetAboutMe(boost::none);
			p_User.SetAgeGroup(boost::none);
			p_User.SetAssignedLicenses(vector<BusinessAssignedLicense>{});
			p_User.SetAssignedPlans({});
			p_User.SetBirthday(boost::none);
			p_User.SetBusinessPhones({});
			p_User.SetConsentProvidedForMinor(boost::none);
			p_User.SetCreatedDateTime(boost::none);
			p_User.SetHireDate(boost::none);
			p_User.SetInterests(boost::none);
			p_User.SetLegalAgeGroupClassification(boost::none);
			p_User.SetMail(boost::none);
			p_User.SetDrive(boost::none);
			p_User.SetDriveError(boost::none);
			p_User.SetMailboxSettings(boost::none);
			p_User.SetMailboxSettingsError(boost::none);
			p_User.SetMySite(boost::none);
			p_User.SetOnPremisesDomainName(boost::none);
			p_User.SetOnPremisesImmutableId(boost::none);
			p_User.SetOnPremisesLastSyncDateTime(boost::none);
			p_User.SetOnPremisesProvisioningErrors(boost::none);
			p_User.SetOnPremisesSamAccountName(boost::none);
			p_User.SetOnPremisesSecurityIdentifier(boost::none);
			p_User.SetOnPremisesSyncEnabled(boost::none);
			p_User.SetOnPremisesUserPrincipalName(boost::none);
			p_User.SetPasswordProfile(boost::none);
			p_User.SetPastProjects(boost::none);
			p_User.SetPreferredName(boost::none);
			p_User.SetProvisionedPlans({});
			p_User.SetProxyAddresses({});
			p_User.SetSignInSessionsValidFromDateTime(boost::none);
			p_User.SetResponsibilities(boost::none);
			p_User.SetSchools(boost::none);
			p_User.SetSkills(boost::none);
			p_User.SetImAddresses(boost::none);
			p_User.SetOtherMails(boost::none);
			p_User.SetPreferredDataLocation(boost::none);
			p_User.SetShowInAddressList(boost::none);
			p_User.SetExternalUserStatus(boost::none);
			p_User.SetExternalUserStatusChangedOn(boost::none);
			p_User.SetEmployeeID(boost::none);
			p_User.SetFaxNumber(boost::none);
			p_User.SetIsResourceAccount(boost::none);
			p_User.SetOnPremisesDistinguishedName(boost::none);
			p_User.SetDeletedDateTime(boost::YOpt<YTimeDate>());
			p_User.SetArchiveFolderName(boost::none);
			p_User.SetArchiveFolderNameError(boost::none);

			return p_User;
		};

		auto keepOnlyCommonValues = [](BusinessUser& p_First, const BusinessUser& p_Second)
		{
			if (p_First.GetAccountEnabled() != p_Second.GetAccountEnabled())
				p_First.SetAccountEnabled(boost::none);
			if (p_First.GetCity() != p_Second.GetCity())
				p_First.SetCity(boost::none);
			if (p_First.GetCountry() != p_Second.GetCountry())
				p_First.SetCountry(boost::none);
			if (p_First.GetDepartment() != p_Second.GetDepartment())
				p_First.SetDepartment(boost::none);
			if (p_First.GetDisplayName() != p_Second.GetDisplayName())
				p_First.SetDisplayName(boost::none);
			if (p_First.GetGivenName() != p_Second.GetGivenName())
				p_First.SetGivenName(boost::none);
			if (p_First.GetJobTitle() != p_Second.GetJobTitle())
				p_First.SetJobTitle(boost::none);
			if (p_First.GetCompanyName() != p_Second.GetCompanyName())
				p_First.SetCompanyName(boost::none);
			if (p_First.GetMailNickname() != p_Second.GetMailNickname())
				p_First.SetMailNickname(boost::none);
			if (p_First.GetMobilePhone() != p_Second.GetMobilePhone())
				p_First.SetMobilePhone(boost::none);
			if (p_First.GetOfficeLocation() != p_Second.GetOfficeLocation())
				p_First.SetOfficeLocation(boost::none);
			if (p_First.GetPasswordPolicies() != p_Second.GetPasswordPolicies())
				p_First.SetPasswordPolicies(boost::none);
			if (p_First.GetPostalCode() != p_Second.GetPostalCode())
				p_First.SetPostalCode(boost::none);
			if (p_First.GetPreferredLanguage() != p_Second.GetPreferredLanguage())
				p_First.SetPreferredLanguage(boost::none);
			if (p_First.GetState() != p_Second.GetState())
				p_First.SetState(boost::none);
			if (p_First.GetStreetAddress() != p_Second.GetStreetAddress())
				p_First.SetStreetAddress(boost::none);
			if (p_First.GetSurname() != p_Second.GetSurname())
				p_First.SetSurname(boost::none);
			if (p_First.GetUsageLocation() != p_Second.GetUsageLocation())
				p_First.SetUsageLocation(boost::none);
			if (p_First.GetUserPrincipalName() != p_Second.GetUserPrincipalName())
				p_First.SetUserPrincipalName(boost::none);
			if (p_First.GetUserType() != p_Second.GetUserType())
				p_First.SetUserType(boost::none);
		};

		boost::YOpt<BusinessUser> templateUser;
		TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this, &templateUser, clearUnusedValues, keepOnlyCommonValues](const bool /*p_HasRowsToReExplode*/)
			{
				for (auto& row : GetSelectedRows())
				{
					if (GridUtil::IsBusinessUser(row, false))
					{
						auto user = getBusinessObject(row);
						if (!templateUser)
						{
							templateUser = clearUnusedValues(user);
						}
						else
						{
							keepOnlyCommonValues(*templateUser, user);
						}
					}
				}
			})();

		if (templateUser)
		{
			p_Objects.back() = *templateUser;
			p_Action = DlgFormsHTML::Action::CREATE_PREFILLED;
		}

		// Set default type
		if (!p_Objects.back().GetUserType())
		{
			p_Objects.back().SetUserType(PooledString(BusinessUser::g_UserTypeMember));
		}
		else
		{
			ASSERT(*p_Objects.back().GetUserType() == BusinessUser::g_UserTypeMember);
		}

		// Enable account by default
		if (!p_Objects.back().GetAccountEnabled())
			p_Objects.back().SetAccountEnabled(true);
		// Default domain
		if (!p_Objects.back().GetUserPrincipalName())
		{
			auto org = GetGraphCache().GetRBACOrganization();
			ASSERT(org);
			if (org)
				p_Objects.back().SetUserPrincipalNameDomain(PooledString(org->GetDefaultDomain()));
		}
	}
	else if (DlgFormsHTML::Action::EDIT_CREATE == p_Action)
	{
		ASSERT([p_Objects]() {
			for (auto& user : p_Objects)
			{
				if (user.GetID().IsEmpty() || !IsTemporaryCreatedObjectID(user.GetID()))
					return false;
				if (0 == (user.GetFlags() & BusinessObject::CREATED))
					return false;
			}
			return true;
		}());
	}

	DlgBusinessUserEditHTML dlg(p_Objects, GetGraphCache().GetRBACOrganization(), p_Action, p_Parent);
	const auto ret = dlg.DoModal();

	if (IDOK == ret && (DlgFormsHTML::Action::CREATE == p_Action || DlgFormsHTML::Action::CREATE_PREFILLED == p_Action || DlgFormsHTML::Action::EDIT_CREATE == p_Action))
	{
		if (dlg.GetAutoGeneratePassword())
		{
			for (auto& user : p_Objects)
			{
				const auto password = MFCUtil::GeneratePassword(4, true, 6, true);
				user.SetPassword(PooledString(password));
			}
		}

		if (DlgFormsHTML::Action::CREATE == p_Action || DlgFormsHTML::Action::CREATE_PREFILLED == p_Action)
		{
			// Create a temporary but unique ID to please the grid PK
			ASSERT(1 == p_Objects.size());
			p_Objects.back().SetID(GetNextTemporaryCreatedObjectID());
			p_Objects.back().SetFlags(p_Objects.back().GetFlags() | BusinessObject::CREATED);
		}
		else if (DlgFormsHTML::Action::EDIT_CREATE == p_Action)
		{
			ASSERT([&p_Objects]() { for (const auto& obj : p_Objects) if (!IsTemporaryCreatedObjectID(obj.GetID())) return false; return true; }());
		}
	}

	return ret;
}

void GridUsers::BuildView(const vector<BusinessUser>& p_Users, const vector<BusinessUser>& p_LoadedMoreUsers, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_AllowFullPurge)
{
	// p_Users must also contain the p_LoadedMoreUsers users. (test GetBirthday() equality as it's a load more property)
	ASSERT(p_LoadedMoreUsers.empty()
		|| !p_Users.empty() && p_Users.end() != std::find_if(p_Users.begin(), p_Users.end(), [&p_LoadedMoreUsers](const BusinessUser& bu) {return bu.GetID() == p_LoadedMoreUsers.front().GetID() && bu.GetBirthday() == p_LoadedMoreUsers.front().GetBirthday(); }));

	// Did we get an error during users listing?
    if (p_Users.size() == 1 && p_Users[0].GetError() && p_Users[0].GetID().IsEmpty())
    {
		HandlePostUpdateError(p_Users[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_Users[0].GetError()->GetFullErrorMessage());
    }
	else
	{
		boost::YOpt<GridUpdater::PostUpdateError> unprocessedPostUpdateError;
		{
			resetRBACHiddenCounter();

			const auto fullPurge = p_AllowFullPurge && p_Updates.empty();
			const auto willProceedLoadMore = !(p_Refresh && p_LoadedMoreUsers.empty());
			const uint32_t options	= (!willProceedLoadMore ? GridUpdaterOptions::UPDATE_GRID : 0)
									| (fullPurge ? GridUpdaterOptions::FULLPURGE : 0)
									| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
									| (!willProceedLoadMore ? GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS : 0)
									| GridUpdaterOptions::REFRESH_PROCESS_DATA
									| (!p_Updates.empty() ? GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG : 0)
									//| GridUpdaterOptions::PROCESS_LOADMORE
									;

			GridUpdater updater(*this, GridUpdaterOptions(options), p_Updates);
			ASSERT(!updater.GetOptions().IsPartialPurge());

			if (willProceedLoadMore)
			{
				unprocessedPostUpdateError.emplace();
				updater.SetPostUpdateErrorBackupTarget(&*unprocessedPostUpdateError);
			}

			GridIgnoreModification ignoramus(*this);

			if (fullPurge && p_Users.empty())
				GetRowIndex().LoadRows(false);

			for (const auto& bu : p_Users)
			{
				if (bu.HasFlag(BusinessObject::Flag::CANCELED))
				{
					row_pk_t BUK;
					m_Template.GetBusinessUserPK(*this, bu, BUK);
					auto row = GetRowIndex().GetRow(BUK, false, updater.GetOptions().IsFullPurge());
					if (nullptr != row)
						AddRoleDelegationFlag(row, bu);
				}
				else
				{
					if (!bu.HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
					{
						wstring immutableId = bu.GetOnPremisesImmutableId() ? *bu.GetOnPremisesImmutableId() : PooledString();
						if (!immutableId.empty())
						{
							vector<GridBackendField> pk;
							m_Template.GetBusinessUserPK(*this, bu, pk);
							m_O365RowPk[immutableId] = pk;
						}

						auto newRow = m_Template.AddRow(*this, bu, {}, updater, false, false);
						ASSERT(nullptr != newRow);
						if (nullptr != newRow)
						{
							auto ittOnPremRow = m_OnPremRowPk.find(immutableId);
							if (ittOnPremRow != m_OnPremRowPk.end())
							{
								auto onPremRow = GetRowIndex().GetExistingRow(ittOnPremRow->second);
								ASSERT(nullptr != onPremRow);

								// If and only if the o365 row is not already marked as hybrid, mark it as such
								if (nullptr != onPremRow && onPremRow != newRow)
								{
									// New o365 user, already has on-prem row, so hybrid user
									// Copy on-prem part from on-prem row
									newRow->AddFields(m_Template.m_OnPremCols.GetFields(*onPremRow));

									m_Template.SetHybrid(*this, *newRow, bu);

									RemoveRow(onPremRow);

									row_pk_t newPk;
									GetRowPK(newRow, newPk);
									m_OnPremRowPk[immutableId] = newPk;
								}
							}
							else
							{
								// New o365 user, but not on-prem
							}

							updater.GetOptions().AddRowWithRefreshedValues(newRow);
							AddRoleDelegationFlag(newRow, bu);

							// Hybrid environment can't be created by a graph users load
							ASSERT(	!(m_Template.IsOnPremMetatype(*newRow) || m_Template.IsHybridMetatype(*newRow))
									|| nullptr != m_Template.m_ColumnMetaType);
						}
					}
					else
					{
						// We are removing the user because it was explicitly deleted by delta
						bool handled = false;

						GridBackendRow* rowBeingRemoved = FindRow(bu.GetID());
						if (rowBeingRemoved != nullptr)
						{
							wstring immutableId = rowBeingRemoved->GetField(m_Template.m_ColumnOPImmutableId).GetValueStr();
							auto ittO365 = m_O365RowPk.find(immutableId);
							auto ittOnPrem = m_OnPremRowPk.find(immutableId);
							if (ittO365 != m_O365RowPk.end() && ittOnPrem != m_OnPremRowPk.end())
							{
								// The O365 part of an hybrid user is being removed
								GridBackendRow* o365Row = GetRowIndex().GetExistingRow(ittO365->second);
								ASSERT(nullptr != o365Row);
								if (nullptr != o365Row)
								{
									auto objGUIDCol = m_Template.m_OnPremCols.m_ColObjectGUID;
									row_pk_t newPk;
									newPk.emplace_back(o365Row->GetField(objGUIDCol).GetValueStr(), m_Template.m_ColumnID->GetID());
									//newPk.emplace_back(_YTEXT(""), m_Template.m_ColumnUserType->GetID());

									auto newRow = GetRowIndex().GetRow(newPk, true, false);
									ASSERT(nullptr != newRow);
									if (nullptr != newRow)
									{
										auto existingRow = GetRowIndex().GetExistingRow(newPk);
										m_OnPremRowPk[immutableId] = newPk;

										m_Template.SetOnPrem(*this, *newRow);
										newRow->AddFields(m_Template.m_OnPremCols.GetFields(*o365Row));
										existingRow = GetRowIndex().GetExistingRow(newPk);
										RemoveRow(o365Row);
										existingRow = GetRowIndex().GetExistingRow(newPk);

										handled = true;
									}
								}
							}

							if (ittO365 != m_O365RowPk.end())
								m_O365RowPk.erase(ittO365);
						}

						if (!handled && nullptr != rowBeingRemoved)
							RemoveRow(rowBeingRemoved);
					}
				}
			}

			updateRBACStatusBarText();
		}

		if (!p_LoadedMoreUsers.empty())
			updateUsersLoadMore(p_LoadedMoreUsers, p_Refresh, !p_Updates.empty(), unprocessedPostUpdateError);
	}
}

void GridUsers::AddCreatedBusinessObjects(const vector<BusinessUser>& p_Users, bool p_ScrollToNew)
{
	GridBackendRow* rowToFocus = nullptr;

	{
		GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
		for (const auto& user : p_Users)
		{
			ASSERT(user.HasFlag(BusinessObject::CREATED) && IsTemporaryCreatedObjectID(user.GetID()));
			auto row = m_Template.AddRow(*this, user, vector<GridBackendField>(), updater, true, false);
			ASSERT(!row->IsModified() && !row->IsDeleted());
			if (row->IsCreated() && (row->IsError() || row->IsDeleted()))
			{
				row->SetEditError(false);
				row->SetEditDeleted(false);
			}
			AddRoleDelegationFlag(row, user);

			if (p_ScrollToNew)
			{
				// Shouldn't have several users in this case
				ASSERT(nullptr == rowToFocus);
				rowToFocus = row;
			}
			else
			{
				row->SetSelected(true);
			}
		}
	}

	if (p_ScrollToNew && nullptr != rowToFocus)
	{
		OnUnSelectAll();
		rowToFocus->SetSelected(true);
		ScrollToRow(rowToFocus);
	}
}

void GridUsers::RemoveBusinessObjects(const vector<BusinessUser>& p_Users)
{
	bool removed = false;
	for (const auto& user : p_Users)
		removed = m_Template.RemoveRow(*this, user) || removed;

	if (removed)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
}

void GridUsers::UpdateBusinessObjects(const vector<BusinessUser>& p_Users, bool p_SetModifiedStatus)
{
	GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
	for (const auto& user : p_Users)
	{
		ASSERT(!user.HasFlag(BusinessObject::CREATED) && !IsTemporaryCreatedObjectID(user.GetID()));
		auto row = m_Template.UpdateRow(*this, user, p_SetModifiedStatus && !user.HasFlag(BusinessObject::CREATED), updater, false);
		ASSERT(!row->IsCreated() && !row->IsDeleted());
		if (row->IsModified() && (row->IsError() || row->IsDeleted()))
		{
			row->SetEditError(false);
			row->SetEditDeleted(false);
		}
		AddRoleDelegationFlag(row, user);
	}
}

void GridUsers::UpdateUsersLoadMore(const vector<BusinessUser>& p_Users, bool p_FromRefresh, bool p_DoNotShowLoadingErrors)
{
	updateUsersLoadMore(p_Users, p_FromRefresh, p_DoNotShowLoadingErrors, boost::none);
}

void GridUsers::UpdateUsersLoadMailbox(const vector<BusinessUser>& p_Users, bool p_FromRefresh, bool p_DoNotShowLoadingErrors)
{
	updateUsersLoadMailbox(p_Users, p_FromRefresh, p_DoNotShowLoadingErrors, boost::none);
}

void GridUsers::updateUsersLoadMore(const vector<BusinessUser>& p_Users, bool p_FromRefresh, bool p_DoNotShowLoadingErrors, const boost::YOpt<GridUpdater::PostUpdateError>& p_UnprocessedPostUpdateError)
{
	GridIgnoreModification ignoramus(*this);
	GridUpdaterOptions updateOptions(GridUpdaterOptions::UPDATE_GRID
		//| GridUpdaterOptions::FULLPURGE
		| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		//| GridUpdaterOptions::REFRESH_PROCESS_DATA
		| GridUpdaterOptions::PROCESS_LOADMORE
		| (p_DoNotShowLoadingErrors ? GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG : 0));
	GridUpdater updater(*this, updateOptions);

	if (p_UnprocessedPostUpdateError)
		updater.InitPostUpdateError(*p_UnprocessedPostUpdateError);

	for (const auto& user : p_Users)
	{
		if (!p_FromRefresh)
		{
			vector<GridBackendField> BUpk;
			m_Template.GetBusinessUserPK(*this, user, BUpk);
			GridBackendRow* rowToUpdate = GetRowIndex().GetExistingRow(BUpk);
			ASSERT(nullptr != rowToUpdate);
			if (nullptr != rowToUpdate)
				rowToUpdate->ClearStatus();
		}

		auto row = m_Template.UpdateRow(*this, user, false, updater, true);
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			if (!user.HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
			{
				updater.GetOptions().AddRowWithRefreshedValues(row);
				AddRoleDelegationFlag(row, user);
			}
			else
			{
				m_Template.RemoveRow(*this, user);
			}
		}
	}
}

void GridUsers::updateUsersLoadMailbox(const vector<BusinessUser>& p_Users, bool p_FromRefresh, bool p_DoNotShowLoadingErrors, const boost::YOpt<GridUpdater::PostUpdateError>& p_UnprocessedPostUpdateError)
{
	{
		GridIgnoreModification ignoramus(*this);
		GridUpdaterOptions updateOptions(GridUpdaterOptions::UPDATE_GRID
			//| GridUpdaterOptions::FULLPURGE
			| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			//| GridUpdaterOptions::REFRESH_PROCESS_DATA
			//| GridUpdaterOptions::PROCESS_LOADMORE
			| (p_DoNotShowLoadingErrors ? GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG : 0));
		GridUpdater updater(*this, updateOptions);

		if (p_UnprocessedPostUpdateError)
			updater.InitPostUpdateError(*p_UnprocessedPostUpdateError);

		for (const auto& user : p_Users)
		{
			ASSERT(user.GetUserType() && *user.GetUserType() != BusinessUser::g_UserTypeGuest);
			if (user.GetUserType() && *user.GetUserType() != BusinessUser::g_UserTypeGuest)
			{
				if (!p_FromRefresh)
				{
					vector<GridBackendField> BUpk;
					m_Template.GetBusinessUserPK(*this, user, BUpk);
					GridBackendRow* rowToUpdate = GetRowIndex().GetExistingRow(BUpk);
					ASSERT(nullptr != rowToUpdate);
					if (nullptr != rowToUpdate)
						rowToUpdate->ClearStatus();
				}

				auto row = m_Template.UpdateRowMailbox(*this, user, updater);
				ASSERT(nullptr != row);
				if (nullptr != row)
				{
					updater.GetOptions().AddRowWithRefreshedValues(row);
					AddRoleDelegationFlag(row, user);
				}
			}
		}
	}

	if (!m_PowerShellCols.empty())
	{
		SetColumnsVisible(m_PowerShellCols);
		GotoColumn(m_PowerShellCols[m_PowerShellCols.size() - 1]);
	}
}

O365Grid::SnapshotCaps GridUsers::GetSnapshotCapabilities() const
{
	return { true };
}

GridFieldModificationsOnPrem& GridUsers::GetOnPremModifications()
{
	ASSERT(m_OnPremModifications);
	return *m_OnPremModifications;
}

const GridFieldModificationsOnPrem& GridUsers::GetOnPremModifications() const
{
	ASSERT(m_OnPremModifications);
	return *m_OnPremModifications;
}

void GridUsers::RevertOnPrem(bool p_SelectedRowsOnly /*= false*/, bool p_UpdateGridIfNeeded/* = true*/)
{
	if (HasOnPremiseChanges())
	{
		CWaitCursor c;
		bool shouldUpdate = false;

		if (p_SelectedRowsOnly)
		{
			TemporarilyImplodeNonGroupRowsAndExec(*this
				, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS
				, [this, &shouldUpdate, p_UpdateGridIfNeeded](const bool p_HasRowsToReExplode)
			{
					shouldUpdate = GetOnPremModifications().RevertSelectedRows() && p_UpdateGridIfNeeded && !p_HasRowsToReExplode;
			})();
		}
		else
		{
			ScopedImplodeRows scopedImplodeRows(*this, GetOnPremModifications().GetModifiedRows(true, true), p_UpdateGridIfNeeded);
			shouldUpdate = GetOnPremModifications().RevertAll() && p_UpdateGridIfNeeded && !scopedImplodeRows.HasExplosionsToRestore();
		}

		if (shouldUpdate)
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}
}

bool GridUsers::HasRevertableModificationsPending(bool inSelectionOnly /*= false*/)
{
	return HasModificationsPending(inSelectionOnly) || HasOnPremiseChanges(inSelectionOnly);
}

void GridUsers::EmptyGrid()
{
	ASSERT(!HasModificationsPending());
	RemoveAllRows();
	m_O365RowPk.clear();
	m_OnPremRowPk.clear();
}

RoleDelegationUtil::RBAC_Privilege GridUsers::GetPrivilegeEdit() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_USER_EDIT;
}

RoleDelegationUtil::RBAC_Privilege GridUsers::GetPrivilegeCreate() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_USER_CREATE;
}

RoleDelegationUtil::RBAC_Privilege GridUsers::GetPrivilegeDelete() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_USER_DELETE;
}

bool GridUsers::IsRowForLoadMore(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return CacheGrid::IsRowForLoadMore(p_Row, p_Col)
		//&& !p_Row->IsGroupRow() // checked in base class above
		&& !p_Row->IsCreated()
		&& !p_Row->IsDeleted()
		&& GridUtil::IsBusinessUser(p_Row)
		&& !IsTemporaryCreatedObjectID(p_Row->GetField(GetColId()).GetValueStr());
}

bool GridUsers::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	if (m_PowerShellCols.end() != std::find(m_PowerShellCols.begin(), m_PowerShellCols.end(), p_Col))
		return IsRowForLoadMore(p_Row, nullptr) && GridUtil::IsBusinessUser(p_Row, false) && !p_Row->GetField(m_ColMailboxInfoLoaded).GetValueBool(); // FIXME: col is not bool anymore

	return GridUsersBaseClass::IsRowValidForNoFieldText(p_Row, p_Col);
}

void GridUsers::OnCustomDoubleClick()
{
	YTestCmdUI testAvailability(ID_MODULEGRID_EDIT);
	testAvailability.Enable(FALSE);
	OnCmdMsg(ID_MODULEGRID_EDIT, CN_UPDATE_COMMAND_UI, &testAvailability, nullptr);
	if (testAvailability.m_bEnabled)
		OnCmdMsg(ID_MODULEGRID_EDIT, CN_COMMAND, nullptr, nullptr);
}

void GridUsers::onPostExplodeMultiValueSisterhood(GridBackendRow* p_Row, const std::set<GridBackendColumn*>& p_Columns)
{
	if (nullptr != GetSecondaryStatusColumn())
	{
		const auto src = GetMultivalueManager().GetTopSourceRow(p_Row);
		if (nullptr != src && src != p_Row)
		{
			// Replicate on prem status
			if (src->HasField(GetSecondaryStatusColumn()))
				p_Row->AddField(src->GetField(GetSecondaryStatusColumn()));
		}
	}
}
