#include "FrameUsersRecycleBin.h"
#include "UpdatedObjectsGenerator.h"

IMPLEMENT_DYNAMIC(FrameUsersRecycleBin, GridFrameBase)

BEGIN_MESSAGE_MAP(FrameUsersRecycleBin, GridFrameBase)
END_MESSAGE_MAP()

void FrameUsersRecycleBin::ShowUsersRecycleBin(const vector<BusinessUser>& p_Users)
{
	if (CompliesWithDataLoadPolicy())
		m_GridUsersRC.BuildView(p_Users, {});
}

void FrameUsersRecycleBin::ShowUsersRecycleBin(const vector<BusinessUser>& p_Users, const vector<O365UpdateOperation>& p_UpdateOperations)
{
	if (CompliesWithDataLoadPolicy())
		m_GridUsersRC.BuildView(p_Users, p_UpdateOperations);
}

void FrameUsersRecycleBin::createGrid()
{
	m_AddRBACHideOutOfScope = true;
	BOOL gridCreated = m_GridUsersRC.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
	ASSERT(gridCreated);
}

bool FrameUsersRecycleBin::customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(p_Tab);
	CreateRefreshGroup(p_Tab);
	CreateApplyGroup(p_Tab);
	CreateRevertGroup(p_Tab);

	auto editGroup = p_Tab.AddGroup(YtriaTranslate::Do(FrameUsersRecycleBin_customizeActionsRibbonTab_1, _YLOC("Manage Deleted Users")).c_str());
	setGroupImage(*editGroup, 0);

	{
		auto ctrl = editGroup->Add(xtpControlButton, ID_USERSRECYCLEBIN_RESTORE);
		GridFrameBase::setImage({ ID_USERSRECYCLEBIN_RESTORE }, IDB_RESTORE, xtpImageNormal);
		GridFrameBase::setImage({ ID_USERSRECYCLEBIN_RESTORE }, IDB_RESTORE_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	{
		auto ctrl = editGroup->Add(xtpControlButton, ID_USERSRECYCLEBIN_HARDDELETE);
		GridFrameBase::setImage({ ID_USERSRECYCLEBIN_HARDDELETE }, IDB_HARDDELETE, xtpImageNormal);
		GridFrameBase::setImage({ ID_USERSRECYCLEBIN_HARDDELETE }, IDB_HARDDELETE_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	CreateUserInfoGroup(p_Tab);

	automationAddCommandIDToGreenLight(ID_USERSRECYCLEBIN_RESTORE);
	automationAddCommandIDToGreenLight(ID_USERSRECYCLEBIN_HARDDELETE);

	return true;
}

O365Grid& FrameUsersRecycleBin::GetGrid()
{
	return m_GridUsersRC;
}

void FrameUsersRecycleBin::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameUsersRecycleBin::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameUsersRecycleBin::ApplySpecific(bool p_Selected)
{
    UpdatedObjectsGenerator<BusinessUser> updatedUsersGenerator;
    updatedUsersGenerator.BuildUpdatedObjectsRecycleBin(GetGrid(), p_Selected);

    CommandInfo info;
    info.Data() = updatedUsersGenerator;
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::User, Command::ModuleTask::UpdateRecycleBin, info, { &GetGrid(), p_Selected ? AutomationConstant::val().m_ActionNameSaveSelected : AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
}

bool FrameUsersRecycleBin::HasApplyButton()
{
	return true;
}

void FrameUsersRecycleBin::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.SetOrigin(GetModuleCriteria().m_Origin);
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	if (HasLastUpdateOperations() && !ShouldForceFullRefreshAfterApply())
    {
		auto& ids = info.GetIds();
		ids.clear();
        for (const auto& updateOp : GetLastUpdateOperations())
            ids.insert(updateOp.GetPkFieldStr(m_GridUsersRC.GetColId()));
    }

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::User, Command::ModuleTask::ListRecycleBin, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

AutomatedApp::AUTOMATIONSTATUS FrameUsersRecycleBin::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionSelectedDelete(p_Action))
	{
		p_CommandID = ID_USERSRECYCLEBIN_HARDDELETE;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedRestore(p_Action))
	{
		p_CommandID = ID_USERSRECYCLEBIN_RESTORE;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}

	return statusRV;
}