#pragma once

#include "BaseO365Grid.h"
#include "BusinessUser.h"
#include "GridTemplateUsers.h"
#include "GridUpdater.h"
#include "O365UpdateOperation.h"

class FrameUsers;
using GridUsersBaseClass = ModuleO365Grid<BusinessUser>;
class GridUsers : public GridUsersBaseClass
{
public:
    GridUsers();
	virtual ~GridUsers();

	virtual void	AddCreatedBusinessObjects(const vector<BusinessUser>& p_Users, bool p_ScrollToNew) override;
	virtual void	UpdateBusinessObjects(const vector<BusinessUser>& p_Users, bool p_SetModifiedStatus) override;
	virtual void	RemoveBusinessObjects(const vector<BusinessUser>& p_Users) override;
	void UpdateUsersLoadMore(const vector<BusinessUser>& p_Users, bool p_FromRefresh, bool p_DoNotShowLoadingErrors);
	void UpdateUsersLoadMailbox(const vector<BusinessUser>& p_Users, bool p_FromRefresh, bool p_DoNotShowLoadingErrors);

	void BuildView(const vector<BusinessUser>& p_Users, const vector<BusinessUser>& p_LoadedMoreUsers, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_AllowFullPurge);
    
	bool CanRefreshOnPrem() const;

	std::vector<BusinessUser> GetLoadMoreRequestInfo(const std::vector<GridBackendRow *>& p_Rows);
	std::vector<BusinessUser> GetMailboxRequestInfo(const std::vector<GridBackendRow*>& p_Rows);

	bool HasPasswordModifications(bool p_InSelection) const;

	virtual row_pk_t UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody) override;

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeEdit() const override;
	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeCreate() const override;
	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeDelete() const override;

	virtual bool IsRowForLoadMore(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;
	
	void SetForceSorting(bool p_ForceSorting);

	// FIXME: bad design. ONLY for UpdatedObjectsGenerator
	GridBackendColumn* GetOPAttributeColumn(int p_Index) const;

	void ShowOnPremiseUsers(const vector<OnPremiseUser>& p_Users);
	void SetOnPremiseUsersUpdateResult(const vector<O365UpdateOperation>& p_UpdateResults);

	struct OnPremiseChanges
	{
		std::vector<std::pair<row_pk_t, OnPremiseUser>> m_UsersToUpdate;
	};

	OnPremiseChanges GetOnPremiseChanges(bool p_SelectedRowsOnly = false);
	bool HasOnPremiseChanges(bool p_SelectedRowsOnly = false) const;

	GridFieldModificationsOnPrem& GetOnPremModifications();
	const GridFieldModificationsOnPrem& GetOnPremModifications() const;

	void RevertOnPrem(bool p_SelectedRowsOnly = false, bool p_UpdateGridIfNeeded = true);

	bool HasRevertableModificationsPending(bool inSelectionOnly = false) override;

	void EmptyGrid() override;

protected:
	virtual BusinessUser getBusinessObject(GridBackendRow* row) const;
	virtual OnPremiseUser getOnPremiseUser(GridBackendRow* row) const;

	virtual void customizeGrid() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
    virtual void InitializeCommands() override;

	afx_msg void OnEdit();
	afx_msg void OnUpdateEdit(CCmdUI* pCmdUI);

	afx_msg void OnRefreshOnPrem();
	afx_msg void OnUpdateRefreshOnPrem(CCmdUI* pCmdUI);
	afx_msg void OnUserLoadMore();
	afx_msg void OnUpdateUserLoadMore(CCmdUI* pCmdUI);
	afx_msg void OnUserLoadMailboxInfo();
	afx_msg void OnUpdateUserLoadMailboxInfo(CCmdUI* pCmdUI);
	afx_msg void OnShowSentValues();
	afx_msg void OnUpdateShowSentValues(CCmdUI* pCmdUI);
	afx_msg void OnShowRecycleBin();
	afx_msg void OnUpdateShowRecycleBin(CCmdUI* pCmdUI);
	afx_msg void OnResetPassword();
	afx_msg void OnUpdateResetPassword(CCmdUI* pCmdUI);
	afx_msg void OnSetManager();
	afx_msg void OnUpdateSetManager(CCmdUI* pCmdUI);
	afx_msg void OnRemoveManager();
	afx_msg void OnUpdateRemoveManager(CCmdUI* pCmdUI);
	afx_msg void OnDelete();
	afx_msg void OnRevokeSessions();
	afx_msg void OnUpdateRevokeSessions(CCmdUI* pCmdUI);

	afx_msg void OnBatchImport();
	afx_msg void OnUpdateBatchImport(CCmdUI* pCmdUI);

	afx_msg void OnBatchUpdate();
	afx_msg void OnUpdateBatchUpdate(CCmdUI* pCmdUI);

	afx_msg void OnChangePreFilter();
	afx_msg void OnUpdateChangePreFilter(CCmdUI* pCmdUI);

	afx_msg void OnEditOnPrem();
	afx_msg void OnUpdateEditOnPrem(CCmdUI* pCmdUI);
	afx_msg void OnEditO365();
	afx_msg void OnUpdateEditO365(CCmdUI* pCmdUI);

	DECLARE_MESSAGE_MAP()

	virtual BOOL PreTranslateMessage(MSG* pMsg) override;

	virtual INT_PTR showDialog(vector<BusinessUser>& p_Objects, const std::set<wstring>& p_ListFieldErrors, DlgFormsHTML::Action p_Action, CWnd* p_Parent) override;

	virtual void customizeGridPostProcess() override;

	void updateUsersLoadMore(const vector<BusinessUser>& p_Users, bool p_FromRefresh, bool p_DoNotShowLoadingErrors, const boost::YOpt<GridUpdater::PostUpdateError>& p_UnprocessedPostUpdateError);
	void updateUsersLoadMailbox(const vector<BusinessUser>& p_Users, bool p_FromRefresh, bool p_DoNotShowLoadingErrors, const boost::YOpt<GridUpdater::PostUpdateError>& p_UnprocessedPostUpdateError);

	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

protected:
	HACCEL m_hAccelSpecific = nullptr;

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

	void OnCustomDoubleClick() override;
	void onPostExplodeMultiValueSisterhood(GridBackendRow* p_Row, const std::set<GridBackendColumn*>& p_Columns) override;

	void InitNewRow(GridBackendRow* p_Row) override;// flags row as "more" not loaded

	GridBackendRow* FindRow(const wstring& p_UserId);

private:
    void LoadMoreImpl(const std::vector<GridBackendRow*>& p_Rows);

	bool AskApplyPending(const std::vector<GridBackendRow*>& p_Rows);

	void LoadMailboxInfo(const std::vector<GridBackendRow*>& p_Rows);
	void batchProcess(bool p_UpdateExsiting);

	GridTemplateUsers	m_Template;
	GridBackendColumn*	m_ColMailboxInfoLoaded;
	FrameUsers*			m_Frame = nullptr;
	bool				m_ForceSorting;

	vector<GridBackendColumn*> m_PowerShellCols;
	map<wstring, row_pk_t> m_O365RowPk; // Everything with an immutable id that is at least O365
	map<wstring, row_pk_t> m_OnPremRowPk; // Everything with an immutable id that is at least OnPrem
	std::unique_ptr<GridFieldModificationsOnPrem> m_OnPremModifications;

	template<class T> friend class OnPremModificationsApplyer;
	template<class T> friend class OnPremModificationsReverter;
};
