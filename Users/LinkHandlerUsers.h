#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerUsers : public IBrowserLinkHandler
{
public:
    virtual void Handle(YBrowserLink& p_Link) const override;
};

// =================================================================================================

class LinkHandlerPrefilteredUsers : public IBrowserLinkHandler
{
public:
	virtual void Handle(YBrowserLink& p_Link) const override;
};

// =================================================================================================

class LinkHandlerUsersRecycleBin : public IBrowserLinkHandler
{
public:
	virtual void Handle(YBrowserLink& p_Link) const override;
};

// =================================================================================================

class LinkHandlerGroupMemberships : public IBrowserLinkHandler
{
public:
	virtual void Handle(YBrowserLink& p_Link) const override;
	virtual bool IsAllowedWithAJLLicense() const override;
};
