#include "ModuleUser.h"

#include "AutomationDataStructure.h"
#include "BasicPageRequestLogger.h"
#include "BusinessGroup.h"
#include "BusinessObjectManager.h"
#include "BusinessUser.h"
#include "DlgEmailConsent.h"
#include "DlgUserCacheDataHTML.h"
#include "DlgUsersModuleOptions.h"
#include "FrameUsers.h"
#include "FrameUsersRecycleBin.h"
#include "FrameUserGroups.h"
#include "FrameUserManagerHierarchy.h"
#include "GraphCache.h"
#include "InvokeResultWrapper.h"
#include "MailboxRequester.h"
#include "MailboxCollectionDeserializer.h"
#include "MsGraphFieldNames.h"
#include "MSGraphSession.h"
#include "O365AdminUtil.h"
#include "OnPremiseUserUpdateRequester.h"
#include "RecipientPermissionCollectionDeserializer.h"
#include "RecipientPermissionRequester.h"
#include "RevokeUserAccessRequester.h"
#include "RunOnScopeEnd.h"
#include "SingleRequestResult.h"
#include "TaskDataManager.h"
#include "UpdatedObjectsGenerator.h"
#include "UserListFullPropertySet.h"
#include "UserListNoLicensePlansInfoPropertySet.h"
#include "YCallbackMessage.h"
#include "YDataCallbackMessage.h"
#include "YSafeCreateTask.h"
#include "GridUsers.h"
#include "UserListRequester.h"
#include "ShowOnPremUsersCommand.h"
#include "CommandDispatcher.h"
#include "ErrorRecordCollectionDeserializer.h"

using namespace Util;

void ModuleUser::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
    {
    case Command::ModuleTask::List:
	case Command::ModuleTask::ListDetails:
	case Command::ModuleTask::ListWithPrefilter:
        show(p_Command);
        break;
	case Command::ModuleTask::ListMyOwners:
	{
		Command newCmd = p_Command;
		newCmd.GetCommandInfo().GetIds() = { PooledString(GetConnectedSession().GetConnectedUserId()) };
		showParentGroups(newCmd);
		break;
	}
    case Command::ModuleTask::ListOwners:
        showParentGroups(p_Command);
        break;
	case Command::ModuleTask::UpdateModified:
		update(p_Command, YtriaTranslate::Do(ModuleUser_executeImpl_1, _YLOC("Update Users")).c_str());
		break;
	case Command::ModuleTask::UpdateMoreInfo:
		more(p_Command);
		break;
	case Command::ModuleTask::UpdateMailbox:
		moreMailbox(p_Command);
		break;
    //case Command::ModuleTask::SendEmailAdvancedSession:
    //    sendEmail(false);
    //    break;
    //case Command::ModuleTask::SendEmailUltraAdminSession:
    //    sendEmail(true);
    //    break;
#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
	case Command::ModuleTask::ListManagerHierarchy:
		showManagerHierarchy(p_Command);
		break;
#endif
	case Command::ModuleTask::ListRecycleBin:
		showRecycleBin(p_Command);
		break;
	case Command::ModuleTask::UpdateRecycleBin:
		update(p_Command, YtriaTranslate::Do(ModuleUser_executeImpl_2, _YLOC("Update Users Recycle Bin")).c_str());
		break;
	case Command::ModuleTask::RevokeAccess:
		revokeAccess(p_Command);
		break;
	case Command::ModuleTask::LoadSnapshot:
		loadSnapshot(p_Command);
		break;
	case Command::ModuleTask::ApplyOnPremChanges:
		saveOnPrem(p_Command);
		break;
    default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
        break;
    }
}

void ModuleUser::show(Command p_Command)
{
    auto p_SourceWindow = p_Command.GetSourceWindow();
    auto p_Action		= p_Command.GetAutomationAction();

    CommandInfo& info	= p_Command.GetCommandInfo();
    FrameUsers* frame	= dynamic_cast<FrameUsers*>(info.GetFrame());

	const bool isRefresh = nullptr != frame;
	bool askAutoLoadOnPrem = false;
    if (nullptr == frame)
    {
        if (!FrameUsers::CanCreateNewFrame(true, p_SourceWindow.GetCWnd(), p_Action))
            return;

        ModuleCriteria moduleCriteria;
		moduleCriteria.m_Origin			= Command::ModuleTask::ListDetails == p_Command.GetTask() ? Origin::User : Origin::Tenant;
        moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::IDS;
		moduleCriteria.m_Privilege		= info.GetRBACPrivilege();
		if (Command::ModuleTask::ListDetails == p_Command.GetTask())
			moduleCriteria.m_IDs = info.GetIds();

		ASSERT(moduleCriteria.m_IDs.end() == std::find_if(moduleCriteria.m_IDs.begin(), moduleCriteria.m_IDs.end(), [](const PooledString& id) {return O365Grid::IsTemporaryCreatedObjectID(id); }));

		if (p_Command.GetTask() == Command::ModuleTask::ListWithPrefilter)
		{
			DlgUsersModuleOptions opt(p_SourceWindow.GetCWnd(), Sapio365Session::Find(p_Command.GetSessionIdentifier()));
			if (IDCANCEL == opt.DoModal())
			{
				if (nullptr != p_Action)
					SetActionCanceledByUser(p_Action);
				return;
			}
			ASSERT(!p_Command.GetCommandInfo().Data2().is_valid());
			p_Command.GetCommandInfo().Data2() = opt.GetOptions();
		}
		else
			askAutoLoadOnPrem = true;

		const bool shouldCreate = p_Command.GetCommandInfo().Data2()
			? ShouldCreateFrame<FrameUsers>(moduleCriteria, p_SourceWindow, p_Action, p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>())
			: ShouldCreateFrame<FrameUsers>(moduleCriteria, p_SourceWindow, p_Action);

        if (shouldCreate && p_SourceWindow.UserConfirmation(p_Action))
        {
			CWaitCursor _;
            frame = new FrameUsers(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleUser_show_1, _YLOC("Users")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
			if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
				frame->SetOptions(p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>());
            frame->InitModuleCriteria(moduleCriteria);
            AddGridFrame(frame);
            frame->Erect();
        }
    }

	// No assert: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
	//ASSERT(nullptr != frame);
    if (nullptr != frame)
    {
		if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
			frame->SetOptions(p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>());

		const bool syncNow = frame->GetAndResetSyncNow();

		// FIXME: For user details, we should probably have a separate method that prevent from loading a whole cache.
		// It should at least use the existing cache (delta-synced) but do separate requests if cache is not init...
		// At best, it should init a 'local' delta with id selection and keep it within the frame.
		loadUsers(p_Command, info, frame, isRefresh, syncNow);

        /*if (frame->GetRefreshMode() == RefreshMode::NotSet)
        {
            if (p_Command.GetTask() == Command::ModuleTask::ListDetails)
                showUserDetails(p_Command, info, frame, isRefresh);
            else
				loadUsers(p_Command, info, frame, isRefresh, syncNow);
        }
        else
        {
			if ((info.GetIds().empty() || ShouldDoListRequests(frame->GetBusinessObjectManager()->GetGraphCache().GetUserCount(), info.GetIds().size(), MSGraphSession::g_DefaultNbElementsPerPage)))
				loadUsers(p_Command, info, frame, isRefresh, syncNow);
			else
				getUsersWithIndividualRequests(p_Command, info, frame, isRefresh);
        }*/
	}
	else
	{
		// Automation doesn't allow to re-use an existing window.
		// It we make it possible, we might encounter some issue, as the refresh has been triggered by ShouldCreateFrame<>() without passing the current action
		// It then can't be greenlighted at the right moment (doing it here is wrong).
		ASSERT(nullptr == p_Action);

		SetAutomationGreenLight(p_Action);
	}
}

void ModuleUser::showParentGroups(const Command& p_Command)
{
	auto		p_SourceWindow	= p_Command.GetSourceWindow();
	auto		p_Action		= p_Command.GetAutomationAction();
	auto		sourceCWnd		= p_SourceWindow.GetCWnd();

    CommandInfo info = p_Command.GetCommandInfo();
	auto frame = dynamic_cast<FrameUserGroups*>(info.GetFrame());

	const bool isRefresh = nullptr != frame;
	if (nullptr == frame)
	{
		if (FrameUserGroups::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin				= Origin::User;
			moduleCriteria.m_UsedContainer		= ModuleCriteria::UsedContainer::IDS;
			moduleCriteria.m_IDs				= info.GetIds();
			moduleCriteria.m_Privilege			= info.GetRBACPrivilege();
			moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

			if (ShouldCreateFrame<FrameUserGroups>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;
				const wstring title = p_Command.GetTask() == Command::ModuleTask::ListMyOwners
					? _T("My Group Memberships")
					: YtriaTranslate::Do(ModuleUser_showParentGroups_1, _YLOC("Group Memberships")).c_str();
				frame = new FrameUserGroups(p_Command.GetTask() == Command::ModuleTask::ListMyOwners, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), title, *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
		}
	}

	// No assert: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
	//ASSERT(nullptr != frame);
	if (nullptr != frame)
	{
		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleUser_showParentGroups_2, _YLOC("Show Group Memberships...")).c_str(), frame, p_Command, !isRefresh);

        RefreshSpecificData refreshSpecificData;
        if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
            refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

        const bool partialRefresh = !refreshSpecificData.m_Criteria.m_IDs.empty();
        const auto& ids = partialRefresh  ? refreshSpecificData.m_Criteria.m_IDs : info.GetIds();

		// We added some columns about group licensing in this module, so always use cache.
		const bool wantsAdditionalMetadata = true;// (bool)frame->GetMetaDataColumnInfo();
        TaskWrapper<vector<BusinessUser>> parentGroupsTask;
        if (!ids.empty())
            parentGroupsTask = frame->GetBusinessObjectManager()->GetBusinessUsersParentGroups(ids, wantsAdditionalMetadata, taskData);
        else
            parentGroupsTask = pplx::task_from_result(vector<BusinessUser>());

        parentGroupsTask.ThenByTask([partialRefresh, hwnd = frame->GetSafeHwnd(), refreshSpecificData, taskData, p_Action, isRefresh](pplx::task<vector<BusinessUser>> p_PreviousTask)
		{
			auto businessUsers = p_PreviousTask.get();
			YDataCallbackMessage<vector<BusinessUser>>::DoPost(businessUsers, [hwnd, taskData, p_Action, partialRefresh, isRefresh, isCanceled = taskData.IsCanceled()](const vector<BusinessUser>& p_Users)
			{
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameUserGroups*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(p_Users)))
				{
					ASSERT(nullptr != frame);
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting groups for %s users...", Str::getStringFromNumber(p_Users.size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());
					//frame->GetGrid().ClearStatus();

					{
						CWaitCursor _;
						if (frame->HasLastUpdateOperations())
						{
							frame->ShowUserWithParentGroups(p_Users, frame->GetLastUpdateOperations(), false); //ShowHierarchies(p_Groups, frame->GetLastUpdateOperations());
							frame->ForgetLastUpdateOperations();
						}
						else
						{
							frame->ShowUserWithParentGroups(p_Users, {}, !partialRefresh);
						}
					}

					if (!isRefresh)
					{
						frame->UpdateContext(taskData, hwnd);
						shouldFinishTask = false;
					}
				}

				if (shouldFinishTask)
				{
					// Because we didn't (and can't) call UpdateContext here.
					ModuleBase::TaskFinished(frame, taskData, p_Action);
				}
			});
		});
	}
	else
	{
		SetAutomationGreenLight(p_Action);
	}
}

void ModuleUser::showManagerHierarchy(const Command& p_Command)
{
#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
	auto				p_SourceWindow	= p_Command.GetSourceWindow();
	AutomationAction*	p_Action		= p_Command.GetAutomationAction();

    CommandInfo					info		= p_Command.GetCommandInfo();
	FrameUserManagerHierarchy*	frame		= dynamic_cast<FrameUserManagerHierarchy*>(info.GetFrame());
	const auto&					p_UserIDs	= info.GetIds();

	const bool isRefresh = nullptr != frame;
	if (nullptr == frame)
	{
		auto sourceCWnd = p_SourceWindow.GetCWnd();
		if (!FrameUserManagerHierarchy::CanCreateNewFrame(true, p_SourceWindow.GetCWnd(), p_Action))
			return;

		ModuleCriteria moduleCriteria;
		moduleCriteria.m_Origin			= Origin::User;
		moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::IDS;
		moduleCriteria.m_IDs			= p_UserIDs;
		moduleCriteria.m_Privilege		= info.GetRBACPrivilege();
		if (ShouldCreateFrame<FrameUserManagerHierarchy>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
		{
			CWaitCursor _;
			frame = new FrameUserManagerHierarchy(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleUser_showManagerHierarchy_1, _YLOC("Management Hierarchy")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
			frame->InitModuleCriteria(moduleCriteria);
			AddGridFrame(frame);
			frame->Erect();
		}
	}

	// No assert: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
	//ASSERT(nullptr != frame);
	if (nullptr != frame)
	{
		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleUser_showManagerHierarchy_2, _YLOC("Show Management Hierarchy...")).c_str(), frame, p_Command, !isRefresh);

        auto userIds = p_UserIDs;

        RefreshSpecificData refreshSpecificData;
        if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
        {
            refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();
            userIds = refreshSpecificData.m_Criteria.m_IDs;
        }

		frame->GetBusinessObjectManager()->GetBusinessUsersManagerHierarchy(userIds, taskData).ThenByTask(
			[hwnd = frame->GetSafeHwnd(), refreshSpecificData, taskData, p_Action, isRefresh](pplx::task<vector<BusinessUser>> p_PreviousTask)
		{
			auto businessUsers = p_PreviousTask.get();

			YDataCallbackMessage<vector<BusinessUser>>::DoPost(businessUsers, [hwnd, refreshSpecificData, taskData, p_Action, isRefresh, isCanceled = taskData.IsCanceled()](vector<BusinessUser>& p_Users)
			{
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameUserManagerHierarchy*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, isCanceled, taskData, false))
				{
                    const bool partialRefresh = !refreshSpecificData.m_Criteria.m_IDs.empty();

					ASSERT(nullptr != frame);
					frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
					frame->GetGrid().ClearLog(taskData.GetId());

					{
						CWaitCursor _;
						frame->ShowUserManagers(p_Users, !partialRefresh);
					}
					
					if (!isRefresh)
					{
						frame->UpdateContext(taskData);
						shouldFinishTask = false;
					}
				}

				if (shouldFinishTask)
				{
					// Because we didn't (and can't) call UpdateContext here.
					ModuleBase::TaskFinished(frame, taskData, p_Action);
				}
			});
		});
	}
	else
	{
		// Automation doesn't allow to re-use an existing window.
		// It we make it possible, we might encounter some issue, as the refresh has been triggered by ShouldCreateFrame<>() without passing the current action
		// It then can't be greenlighted at the right moment (doing it here is wrong).
		ASSERT(nullptr == p_Action);

		SetAutomationGreenLight(p_Action);
	}
#endif
}

void ModuleUser::showRecycleBin(const Command& p_Command)
{
	auto				p_SourceWindow = p_Command.GetSourceWindow();
	AutomationAction*	p_Action = p_Command.GetAutomationAction();

	CommandInfo					info = p_Command.GetCommandInfo();
	FrameUsersRecycleBin*	frame = dynamic_cast<FrameUsersRecycleBin*>(info.GetFrame());
	const auto&					p_UserIDs = info.GetIds();

	const bool isRefresh = nullptr != frame;
	if (nullptr == frame)
	{
		auto sourceCWnd = p_SourceWindow.GetCWnd();
		if (!FrameUsersRecycleBin::CanCreateNewFrame(true, p_SourceWindow.GetCWnd(), p_Action))
			return;

		ModuleCriteria moduleCriteria;
		moduleCriteria.m_Origin			= Origin::User;
		moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::IDS;
		moduleCriteria.m_IDs			= p_UserIDs;
		moduleCriteria.m_Privilege		= info.GetRBACPrivilege();
		if (ShouldCreateFrame<FrameUsersRecycleBin>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
		{
			CWaitCursor _;
			frame = new FrameUsersRecycleBin(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleUser_showRecycleBin_1, _YLOC("Users: Recycle Bin")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
			frame->InitModuleCriteria(moduleCriteria);
			AddGridFrame(frame);
			frame->Erect();
		}
	}

	if (nullptr != frame)
	{
		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleUser_showRecycleBin_2, _YLOC("Show Users Recycle Bin...")).c_str(), frame, p_Command, !isRefresh);

		YSafeCreateTask([this, hwnd = frame->GetSafeHwnd(), bom = frame->GetBusinessObjectManager(), taskData, p_Action, isRefresh]()
		{
			auto businessUsers = bom->GetBusinessUsersRecycleBin({}, DeletedUserListNoLicensePlansInfoPropertySet(), taskData).GetTask().get();

			ensureAssignedByGroupsAreInCache(businessUsers, bom, taskData);
			//ensureManagersAreInCache(businessUsers, bom, taskData);

			YDataCallbackMessage<vector<BusinessUser>>::DoPost(businessUsers, [hwnd, taskData, p_Action, isRefresh, isCanceled = taskData.IsCanceled()](vector<BusinessUser>& p_Users)
			{
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameUsersRecycleBin*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, isCanceled, taskData, false))
				{
					ASSERT(nullptr != frame);
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s deleted users...", Str::getStringFromNumber(p_Users.size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());

					{
						CWaitCursor _;
						if (frame->HasLastUpdateOperations())
						{
							frame->ShowUsersRecycleBin(p_Users, frame->GetLastUpdateOperations());
							frame->ForgetLastUpdateOperations();
						}
						else
						{
							frame->ShowUsersRecycleBin(p_Users);
						}
					}
					if (!isRefresh)
					{
						frame->UpdateContext(taskData, hwnd);
						shouldFinishTask = false;
					}
				}

				if (shouldFinishTask)
				{
					// Because we didn't (and can't) call UpdateContext here.
					ModuleBase::TaskFinished(frame, taskData, p_Action);
				}
			});
		});
	}
	else
		SetAutomationGreenLight(p_Action);
}

void ModuleUser::update(const Command& p_Command, const wstring& p_TaskText)
{
	AutomationAction* action = p_Command.GetAutomationAction();
	
    ASSERT(p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessUser>>());
    if (p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessUser>>())
    {
        auto updatedUsersGenerator = p_Command.GetCommandInfo().Data().get_value<UpdatedObjectsGenerator<BusinessUser>>();
        if (!updatedUsersGenerator.GetObjects().empty())
        {
            // The frame to update once the data has been sent.
            GridFrameBase* existingFrame = dynamic_cast<GridFrameBase*>(p_Command.GetCommandInfo().GetFrame());
            ASSERT(nullptr != existingFrame);
            if (nullptr != existingFrame)
            {
				auto taskData = addFrameTask(p_TaskText, existingFrame, p_Command, false);
				YSafeCreateTask([taskData, existingFrame, objs = updatedUsersGenerator, action]()
				{
                    auto updateOperations = existingFrame->GetBusinessObjectManager()->WriteSingleRequest(objs.GetObjects(), objs.GetPrimaryKeys(), taskData);

					YCallbackMessage::DoPost([existingFrame, action, taskData, updateOperations{ std::move(updateOperations) }]()
					{
						if (::IsWindow(existingFrame->GetSafeHwnd()))
						{
							existingFrame->GetGrid().ClearLog(taskData.GetId());
							existingFrame->RefreshAfterUpdate(vector<O365UpdateOperation>{ updateOperations }, action);
						}
						else
						{
							SetAutomationGreenLight(action);
						}
						TaskDataManager::Get().RemoveTaskData(taskData.GetId(), existingFrame->GetSafeHwnd());
					});
                });
            }
            else
                SetAutomationGreenLight(action, _YTEXT(""));
        }
        else
            SetAutomationGreenLight(action);
    }
    else
        SetAutomationGreenLight(action);
}

void ModuleUser::more(const Command& p_Command)
{
	auto p_Action = p_Command.GetAutomationAction();

    // TODO: Don't use "UpdatedObjectsGenerator" for something that is not an update (duh)
    ASSERT(p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessUser>>());
    if (p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessUser>>())
    {
        auto updatedUsersGenerator = p_Command.GetCommandInfo().Data().get_value<UpdatedObjectsGenerator<BusinessUser>>();
        if (!updatedUsersGenerator.GetObjects().empty())
        {
            // The frame to update once the data has been sent.
            FrameUsers* existingFrame = dynamic_cast<FrameUsers*>(p_Command.GetCommandInfo().GetFrame());
            ASSERT(nullptr != existingFrame);
            if (nullptr != existingFrame)
            {
                auto taskData = addFrameTask(YtriaTranslate::Do(ModuleUser_more_1, _YLOC("More Info")).c_str(), existingFrame, p_Command, false);
				YSafeCreateTask([this, hwnd = existingFrame->GetSafeHwnd(), bom = existingFrame->GetBusinessObjectManager(), taskData, p_Action, updatedUsersGenerator, gridLogger = existingFrame->GetGridLogger()]()
				{
					vector<BusinessUser> businessUsers;
					{
						gridLogger->SetIgnoreHttp404Errors(true);
						RunOnScopeEnd _([&gridLogger]()
							{
								gridLogger->SetIgnoreHttp404Errors(false);
							});
						businessUsers = bom->MoreInfoBusinessUsers(updatedUsersGenerator.GetObjects(), false, taskData).GetTask().get();
					}

					ensureAssignedByGroupsAreInCache(businessUsers, bom, taskData);
					ensureManagersAreInCache(businessUsers, bom, taskData);

					// FIXME: which blocks of data to save here?
					// MoreInfoBusinessUsers() is now not configurable, always loads: sync props + UserAdditionalRequest::RequestMailboxSettingsForNonGuest | UserAdditionalRequest::RequestDriveForNonGuest | UserAdditionalRequest::RequestManager
					// Update as soon as we make it a user choice!
					bom->GetGraphCache().AddUsers(businessUsers, true, { UBI::MIN, UBI::LIST, UBI::SYNCV1, UBI::SYNCBETA, UBI::MAILBOXSETTINGS, UBI::ARCHIVEFOLDERNAME, UBI::DRIVE, UBI::MANAGER, UBI::ASSIGNEDLICENSES }, boost::none, false);

					YDataCallbackMessage<vector<BusinessUser>>::DoPost(businessUsers, [hwnd, taskData, p_Action, isCanceled = taskData.IsCanceled()](const vector<BusinessUser>& p_Users)
					{
						auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameUsers*>(CWnd::FromHandle(hwnd)) : nullptr;
						if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(p_Users)))
						{
							ASSERT(nullptr != frame);
							frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
							frame->GetGrid().ClearLog(taskData.GetId());

							{
								CWaitCursor _;
								frame->UpdateUsersLoadMore(p_Users, false);
							}
						}

						// Because we didn't (and can't) call UpdateContext here.
						ModuleBase::TaskFinished(frame, taskData, p_Action);
					});
				});
            }
            else
                SetAutomationGreenLight(p_Action, _YTEXT(""));
        }
        else
            SetAutomationGreenLight(p_Action);
    }
    else
        SetAutomationGreenLight(p_Action);
}

void ModuleUser::moreMailbox(const Command& p_Command)
{
	auto p_Action = p_Command.GetAutomationAction();

	ASSERT(p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessUser>>());
	if (p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessUser>>())
	{
		auto updatedUsersGenerator = p_Command.GetCommandInfo().Data().get_value<UpdatedObjectsGenerator<BusinessUser>>();
		if (!updatedUsersGenerator.GetObjects().empty())
		{
			// The frame to update once the data has been sent.
			FrameUsers* existingFrame = dynamic_cast<FrameUsers*>(p_Command.GetCommandInfo().GetFrame());
			ASSERT(nullptr != existingFrame);
			if (nullptr != existingFrame)
			{
				if (!ModuleUtil::AskUserPowerShellAuth(existingFrame->GetSapio365Session(), existingFrame))
					return;
				
				auto taskData = addFrameTask(_T("Get Mailbox Info"), existingFrame, p_Command, false);
				YSafeCreateTaskMutable([this, hwnd = existingFrame->GetSafeHwnd(), bom = existingFrame->GetBusinessObjectManager(), taskData, p_Action, users = updatedUsersGenerator.GetObjects(), gridLogger = existingFrame->GetGridLogger()] () mutable
				{
					wstring errors;
					wstring mailboxErrors;
					wstring recipientErrors;
					
					auto initResult = bom->GetSapio365Session()->InitExchangePowerShell(taskData.GetOriginator());
					errors += GetPSErrorString(initResult);

					if ((initResult && initResult->Get().m_Success) || !initResult)
					{
						auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("mailbox"), users.size());

						//FIXME: Handle cancel here?
						for (auto& user : users)
						{
							errors.clear();

							{
								auto deserializer = std::make_shared<MailboxCollectionDeserializer>();
								logger->IncrementObjCount();
								logger->SetContextualInfo(user.GetDisplayName() ? PooledString(*user.GetDisplayName()) : PooledString(_YTEXT("")));
								logger->SetLogMessage(_T("mailbox"));

								MailboxRequester requester(deserializer, user.GetUserPrincipalName() == boost::none ? _YTEXT("") : *user.GetUserPrincipalName(), logger);
								requester.Send(bom->GetSapio365Session(), taskData).get();

								auto mailboxResult = requester.GetResult();
								if (!mailboxResult->Get().m_Success)
								{
									wstring mailboxErrors;

									auto mailboxErrorsStream = mailboxResult->Get().m_ErrorStream;
									auto errStreamDeserializer = std::make_shared<ErrorRecordCollectionDeserializer>();
									mailboxErrorsStream->Deserialize(*errStreamDeserializer);

									for (int i = 0; i < mailboxErrorsStream->GetSize(); ++i)
										mailboxErrors += wstring(mailboxErrorsStream->GetAsString(i)) + _YTEXT("\n");

									SapioError error(mailboxErrors, 0);
									if (!errStreamDeserializer->GetData().empty())
										error.SetReason(errStreamDeserializer->GetData()[0].m_CategoryInfoReason);

									user.SetMailboxInfoError(error);
									
									errors += mailboxErrors;
								}
								else if (!deserializer->GetData().empty())
								{
									user.SetMailboxType(PooledString(deserializer->GetData()[0].m_RecipientTypeDetails));
									user.SetMailboxDeliverToMailboxAndForward(deserializer->GetData()[0].m_DeliverToMailboxAndForward);
									user.SetMailboxForwardingSmtpAddress(PooledString(deserializer->GetData()[0].m_ForwardingSmtpAddress));
									user.SetMailboxForwardingAddress(PooledString(deserializer->GetData()[0].m_ForwardingAddress));
									vector<PooledString> canSendOnBehalf;
									for (const auto& s : deserializer->GetData()[0].m_GrantSendOnBehalfTo)
										canSendOnBehalf.push_back(s);
									user.SetMailboxCanSendOnBehalf(canSendOnBehalf);
								}

								user.SetDataDate(UBI::MAILBOX, taskData.GetLastRequestOn());
							}

							{
								auto recDeserializer = std::make_shared<RecipientPermissionCollectionDeserializer>();
								logger->SetLogMessage(_T("recipient permissions"));

								RecipientPermissionRequester recRequester(recDeserializer, user.GetUserPrincipalName() == boost::none ? _YTEXT("") : *user.GetUserPrincipalName(), logger);
								recRequester.Send(bom->GetSapio365Session(), taskData).get();

								if (!recRequester.GetResult()->Get().m_Success)
								{
									wstring recipientErrors;

									auto recipientErrorsStream = recRequester.GetResult()->Get().m_ErrorStream;
									for (int i = 0; i < recipientErrorsStream->GetSize(); ++i)
										recipientErrors += wstring(recipientErrorsStream->GetAsString(i)) + _YTEXT("\n");

									SapioError error(recipientErrors, 0);
									user.SetRecipientPermissionsError(error);
									errors += recipientErrors;
								}
								else if (!recDeserializer->GetData().empty())
								{
									vector<PooledString> canSendAsUser;
									for (const auto& s : recDeserializer->GetData())
										canSendAsUser.push_back(s.m_Trustee);
									user.SetMailboxCanSendAsUser(canSendAsUser);
								}

								if (!errors.empty())
								{
									SapioError error(errors, 0);
									user.SetError(error);
								}

								user.SetDataDate(UBI::RECIPIENTPERMISSIONS, taskData.GetLastRequestOn());
							}
						}
					}
					else
					{
						ASSERT(!errors.empty());
						// Init failed? We put an error on all users
						if (!errors.empty())
						{
							taskData.SetLastRequestOn(YTimeDate::GetCurrentTimeDate());

							SapioError error(errors, 0);
							for (auto& user : users)
							{
								user.SetMailboxInfoError(error);
								user.SetRecipientPermissionsError(error);

								user.SetDataDate(UBI::MAILBOX, taskData.GetLastRequestOn());
								user.SetDataDate(UBI::RECIPIENTPERMISSIONS, taskData.GetLastRequestOn());
							}
						}
					}

					LoggerService::User(_T("Writing cache to disk."), taskData.GetOriginator());
					bom->GetGraphCache().SaveUsersInSqlCache(users, { UBI::MAILBOX, UBI::RECIPIENTPERMISSIONS }, boost::none, false);

					YCallbackMessage::DoPost([hwnd, taskData, p_Action, isCanceled = taskData.IsCanceled(), users]()
					{
						auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameUsers*>(CWnd::FromHandle(hwnd)) : nullptr;
						if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(users)))
						{
							ASSERT(nullptr != frame);
							frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
							frame->GetGrid().ClearLog(taskData.GetId());

							CWaitCursor _;
							frame->UpdateUsersLoadMailbox(users, false);
						}

						// Because we didn't (and can't) call UpdateContext here.
						ModuleBase::TaskFinished(frame, taskData, p_Action);
					});
				});
			}
			else
				SetAutomationGreenLight(p_Action, _YTEXT(""));
		}
		else
			SetAutomationGreenLight(p_Action);
	}
	else
		SetAutomationGreenLight(p_Action);
}

void ModuleUser::revokeAccess(const Command& p_Command)
{
	ASSERT(false); // DEPRECATED

	AutomationAction* p_Action = p_Command.GetAutomationAction();

	ASSERT(p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessUser>>());
	if (p_Command.GetCommandInfo().Data().is_type<UpdatedObjectsGenerator<BusinessUser>>())
	{
		// The frame to update once the data has been sent.
		auto existingFrame = dynamic_cast<FrameUsers*>(p_Command.GetCommandInfo().GetFrame());
		ASSERT(nullptr != existingFrame);
		if (nullptr != existingFrame)
		{
			auto updatedUsersGenerator = p_Command.GetCommandInfo().Data().get_value<UpdatedObjectsGenerator<BusinessUser>>();

			const vector<BusinessUser> Temp = updatedUsersGenerator.GetObjects();
			auto taskData = addFrameTask(_T("Revoking access to Office 365 applications"), existingFrame, p_Command, false);

			YSafeCreateTask([taskData, hwnd = existingFrame->GetSafeHwnd(), bom = existingFrame->GetBusinessObjectManager(), updatedUsersGenerator, p_Action, p_Command]()
			{
				vector<O365UpdateOperation> updateOperations;
				ASSERT(updatedUsersGenerator.GetObjects().size() == updatedUsersGenerator.GetPrimaryKeys().size());
				if (updatedUsersGenerator.GetObjects().size() == updatedUsersGenerator.GetPrimaryKeys().size())
				{
					size_t pkIndex = 0;
					for (const auto& businessObj : updatedUsersGenerator.GetObjects())
					{
						RevokeUserAccessRequester requester(businessObj.GetID(), Util::GetDisplayNameOrId(businessObj));
						requester.Send(bom->GetSapio365Session(), taskData).GetTask().wait();
						updateOperations.emplace_back(updatedUsersGenerator.GetPrimaryKeys()[pkIndex++]);
						updateOperations.back().StoreResults({ HttpResultWithError(requester.GetResult().GetResult()) });
					}
				}

				// Refresh grid
				YCallbackMessage::DoPost([hwnd, p_Action, taskData, updateOperations{ std::move(updateOperations) }, p_Command]()
				{
					if (::IsWindow(hwnd))
					{
						auto frame = dynamic_cast<FrameUsers*>(CWnd::FromHandle(hwnd));
						frame->GetGrid().ClearLog(taskData.GetId());
						// Trigger refresh
						frame->RefreshAfterUpdate(vector<O365UpdateOperation>{ updateOperations }, p_Action);
					}
					SetAutomationGreenLight(p_Action);
					TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
				});
			});
		}
		else
			SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
	else
		SetAutomationGreenLight(p_Action);
}

void ModuleUser::loadSnapshot(const Command& p_Command)
{
	using LoaderType = std::shared_ptr<GridSnapshot::Loader>;
	if (p_Command.GetCommandInfo().Data().is_type<LoaderType>())
	{
		auto loader = p_Command.GetCommandInfo().Data().get_value<LoaderType>();
		const auto& metadata = loader->GetMetadata();

		if (GridUtil::g_AutoNameUsers == *metadata.m_GridAutomationName)
			LoadSnapshot<FrameUsers>(p_Command, YtriaTranslate::Do(ModuleUser_show_1, _YLOC("Users")).c_str());
		else if (GridUtil::g_AutoNameUserGroups == *metadata.m_GridAutomationName)
			LoadSnapshot<FrameUserGroups>(p_Command, [&p_Command](const GridSnapshot::Metadata& p_SnapshotMeta, const ModuleCriteria& p_ModuleCriteria, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
				{
					const bool isMyData = p_SnapshotMeta.m_Flags && GridSnapshot::Flags::MyData == (GridSnapshot::Flags::MyData & *p_SnapshotMeta.m_Flags);
					const wstring title = isMyData
						? _T("My Group Memberships")
						: YtriaTranslate::Do(ModuleUser_showParentGroups_1, _YLOC("Group Memberships")).c_str();
					return new FrameUserGroups(isMyData, p_LicenseContext, p_SessionIdentifier, title, p_Module, p_HistoryMode, p_PreviousFrame);
				});
	}
}

void ModuleUser::saveOnPrem(const Command& p_Command)
{
	auto action = p_Command.GetAutomationAction();
	FrameUsers* existingFrame = dynamic_cast<FrameUsers*>(p_Command.GetCommandInfo().GetFrame());

	ASSERT(nullptr != existingFrame && p_Command.GetCommandInfo().Data().is_type<GridUsers::OnPremiseChanges>());
	if (nullptr != existingFrame && p_Command.GetCommandInfo().Data().is_type<GridUsers::OnPremiseChanges>())
	{
		auto& changes = p_Command.GetCommandInfo().Data().get_value<GridUsers::OnPremiseChanges>();
		auto taskData = addFrameTask(_T("Update On-Premises Users"), existingFrame, p_Command, false);
		YSafeCreateTask([taskData, existingFrame, changes, action, sapio365Session = existingFrame->GetSapio365Session()]()
			{
				vector<O365UpdateOperation> upOp;
				upOp.reserve(changes.m_UsersToUpdate.size());
				for (const auto& c : changes.m_UsersToUpdate)
				{
					upOp.emplace_back(c.first);

					OnPremiseUserUpdateRequester req(c.second);
					req.Send(sapio365Session, taskData).get();
					auto result = req.GetResult();
					ASSERT(result);
					if (result)
					{
						if (result->Get().m_Success)
						{
							// Handy to use a fake http_response with 200 code so that Grid code can see it's a success.
							// Consider updating code that uses this status code if you want not to use the http_response in that case.
							RestResultInfo successResult(true);
							successResult.Response.set_status_code(200);
							upOp.back().StoreResult({ successResult, boost::none });
						}
						else
						{
							SapioError err(GetPSErrorString(result), 0);
							upOp.back().StoreResult({ boost::none, err });
						}
					}
				}

				YCallbackMessage::DoPost([existingFrame, action, taskData, upOp{ std::move(upOp) }]()
				{
					if (::IsWindow(existingFrame->GetSafeHwnd()))
					{
						existingFrame->GetGrid().ClearLog(taskData.GetId());
						existingFrame->SetOnPremiseUsersUpdateResult(upOp);
					}
					
					SetAutomationGreenLight(action);
					TaskDataManager::Get().RemoveTaskData(taskData.GetId(), existingFrame->GetSafeHwnd());
				});
			});
	}
	else
		SetAutomationGreenLight(action, _YTEXT(""));
}

//void ModuleUser::sendEmail(const bool p_UltraAdmin)
//{
//	DlgEmailConsent dlg(p_UltraAdmin, nullptr);
//    if (dlg.DoModal() == IDOK)
//    {
//		const auto emailInfo = p_UltraAdmin ? dlg.GetEmailInfoForUltraAdminSession() : dlg.GetEmailInfoForAdvancedUserSession();
//
//        MSGraphSession::Get().SendMail(emailInfo.m_Subject, _YTEXT("text"), emailInfo.m_Content, emailInfo.m_Recipients).ThenByTask([](pplx::task<RestResultInfo> p_ResultInfo)
//        {
//            try
//            {
//                auto result = p_ResultInfo.get();
//
//                YCallbackMessage::DoPost([]()
//                {
//                    YCodeJockMessageBox message(nullptr,
//                        DlgMessageBox::eIcon_Information,
//                        _TLOC("Consent Email"),
//                        _TLOC("Email successfully sent!"),
//                        _TLOC(""),
//                        { { IDOK, _YTEXT("OK") } }
//                    );
//                    message.DoModal();
//                });
//            }
//            catch (const RestException& e)
//            {
//                YCallbackMessage::DoPost([e]()
//                {
//                    YCodeJockMessageBox message(nullptr,
//                        DlgMessageBox::eIcon_Error,
//                        _TLOC("Consent Email"),
//                        wstring(_TLOC("An error occurred while sending consent email: ")) + e.WhatUnicode(),
//                        _TLOC(""),
//                        { { IDOK, _YTEXT("OK") } }
//                    );
//                    message.DoModal();
//                });
//            }
//            catch (const std::exception& e)
//            {
//                YCallbackMessage::DoPost([e]()
//                {
//                    YCodeJockMessageBox message(nullptr,
//                        DlgMessageBox::eIcon_Error,
//                        _TLOC("Consent Email"),
//                        wstring(_TLOC("An error occurred while sending consent email: ")) + MFCUtil::convertUTF8_to_UNICODE(e.what()),
//                        _TLOC(""),
//                        { { IDOK, _YTEXT("OK") } }
//                    );
//                    message.DoModal();
//                });
//            }
//        });
//    }
//}

void ModuleUser::loadUsers(const Command& p_Command, const CommandInfo& p_CommandInfo, FrameUsers* p_Frame, bool p_IsRefresh, bool p_Sync)
{
	ASSERT(!p_Sync || p_IsRefresh); // Don't be a fool, can't sync a first load
	ASSERT(!p_Sync || !p_CommandInfo.Data2().is_valid()); // Cannot sync with pre-filtering

	const bool gridIsEmpty = p_Frame->GetGrid().GetBackendRows().empty();

	auto taskData = addFrameTask(YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_1, _YLOC("Show Users")).c_str(), p_Frame, p_Command, !p_IsRefresh);

	// Load Mores
	std::vector<BusinessUser> usersLoadMoreRequestInfo;

	if (p_IsRefresh
		&& p_Frame->GetAndResetDoLoadMoreOnNextRefresh()
		&& !gridIsEmpty)
	{
		GridUsers* gridUsers = dynamic_cast<GridUsers*>(&p_Frame->GetGrid());
		ASSERT(nullptr != gridUsers);
		if (nullptr != gridUsers)
		{
			std::vector<GridBackendRow*> rowsWithMoreLoaded;
			gridUsers->GetRowsWithMoreLoaded(rowsWithMoreLoaded);

			if (!rowsWithMoreLoaded.empty())
				usersLoadMoreRequestInfo = gridUsers->GetLoadMoreRequestInfo(rowsWithMoreLoaded);

			if (p_Frame->HasLastUpdateOperations())
			{
				usersLoadMoreRequestInfo.erase(std::remove_if(usersLoadMoreRequestInfo.begin(), usersLoadMoreRequestInfo.end(), [p_Frame](const BusinessUser& p_User) {
					GridUsers* gridUsers = dynamic_cast<GridUsers*>(&p_Frame->GetGrid());
					ASSERT(nullptr != gridUsers);
					if (nullptr != gridUsers)
					{
						for (const auto& update : p_Frame->GetLastUpdateOperations())
						{
							if (update.GetPkFieldStr(gridUsers->GetColId()) == p_User.GetID())
								return false; // That user has just been updated, keep it for load more
						}
					}
					return true;
					})
					, usersLoadMoreRequestInfo.end());
			}
		}
	}

	bool shouldUseSql = !p_IsRefresh; // not refreshing existing grid
						//&& (!moduleOptions || moduleOptions->m_CustomFilter.IsEmpty()); // not using pre-filtering

	// FIXME: should we show the following message with filters as we use SQL cache?

	if (shouldUseSql)
	{
		const auto useCacheSetting = UseCacheSetting().Get();

		if (useCacheSetting)
		{
			shouldUseSql = *useCacheSetting;
		}
		else
		{
			DlgUserCacheDataHTML dlg(p_Frame);
			dlg.SetTitleWindow(YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_6, _YLOC("Cached data available:")).c_str());
			dlg.SetIntroWindow(YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_8, _YLOC("You have data available in the cache. How do you want sapio365 to handle this?")).c_str());
			dlg.SetCacheTxtBold(YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_11, _YLOC("Always use cached data")).c_str());
			dlg.SetCacheTxtReg(YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_7, _YLOC("- faster but data may not be up to date")).c_str());
			dlg.SetCacheTxtLine(YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_15, _YLOC("(recommended for large tenants)")).c_str());
			dlg.SetServerBold(YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_10, _YLOC("Always load from Server")).c_str());
			dlg.SetServerReg(YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_16, _YLOC("- may be slower")).c_str());
			dlg.SetServerLine(_T("(recommended for smaller tenants)"));
			dlg.SetCommentTextReg(YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_19, _YLOC("You can change this in Settings")).c_str());

			if (IDOK == dlg.DoModal())
				shouldUseSql = false;
			else
				shouldUseSql = true;

			UseCacheSetting().Set(shouldUseSql);
		}
	}

	auto bom = p_Frame->GetBusinessObjectManager();
	ASSERT(bom);

	bom->GetGraphCache().UpdateUsersDeltaLinkFromSql();

	struct State
	{
		const enum Mode { LOAD, SYNC, REFRESH } mode;
		const bool sqlCacheIsExpired;
		const bool sqlCacheIsFull;
		const bool hasSyncLink;
		const bool refreshMore;
		const bool hasDeltaCapabilities; // Doesn't exist for Germany https://docs.microsoft.com/en-us/graph/deployments, but this cloud is said not to be used anymore (by clients)...
		const std::shared_ptr<wstring> localDeltaLink;
		bool userAgreedToUpdateCache = true;
		bool sqlCacheHasMissingInfo = false;
	}
	state{ p_Sync ? State::SYNC : p_IsRefresh ? State::REFRESH : State::LOAD
		, bom->GetGraphCache().IsSqlUsersCacheExpired()
		, bom->GetGraphCache().IsSqlUsersCacheFull()
		, bom->GetGraphCache().IsUsersDeltaSyncAvailable()
		, p_IsRefresh && !usersLoadMoreRequestInfo.empty()
		, bom->GetGraphCache().HasUsersDeltaCapabilities()
		, p_Frame->GetUsersDeltaLink() ? std::make_shared<wstring>(*p_Frame->GetUsersDeltaLink()) : std::make_shared<wstring>() };

	if (shouldUseSql
		&& p_CommandInfo.GetIds().empty() // load all
		&& (!p_CommandInfo.Data2().is_type<ModuleOptions>() || p_CommandInfo.Data2().get_value<ModuleOptions>().m_CustomFilter.IsEmpty()) // no pre-filter
		)
	{
		ASSERT(state.mode == State::LOAD);
		const auto fullListSize = bom->GetGraphCache().GetSqlUsersCacheFullListSize();
		ASSERT(state.sqlCacheIsFull == (fullListSize >= 0));

		if (state.sqlCacheIsFull)
		{
			auto count = bom->GetGraphCache().CountSqlUsersMissingRequirements({ UBI::MIN, UBI::LIST, UBI::ASSIGNEDLICENSES });

			state.sqlCacheHasMissingInfo = count > 0;

			wstring title, message, details;
			if (0 < count && state.sqlCacheIsExpired)
			{
				title = YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_11, _YLOC("Cached data is out of date")).c_str();
				message = YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_12, _YLOC("This cache is more than 24 hours old.")).c_str();
				details = YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_16, _YLOC("Also, %1 of the %2 users found in the current cache have incomplete data. If you don't load from Server, they will not appear in your grid."), Str::getStringFromNumber(count).c_str(), Str::getStringFromNumber(fullListSize).c_str());
			}
			else if (0 < count)
			{
				title = YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_2, _YLOC("Incomplete data")).c_str();
				message = YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_3, _YLOC("Would you like to load the whole list from the server?")).c_str();
				details = YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_17, _YLOC("%1 of the %2 users found in the current cache have incomplete data. If you don't load from Server, they will not appear in your grid."), Str::getStringFromNumber(count).c_str(), Str::getStringFromNumber(fullListSize).c_str());
			}
			else if (state.sqlCacheIsExpired)
			{
				title = YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_11, _YLOC("Cached data is out of date")).c_str();
				message = YtriaTranslate::Do(ModuleGroup_getGroupsWithListRequest_12, _YLOC("This cache is more than 24 hours old.")).c_str();
				details.clear();
			}

			if (!title.empty())
			{
				YCodeJockMessageBox dlg(
					p_Frame,
					DlgMessageBox::eIcon_Question,
					title,
					message,
					details,
					{ { IDOK, YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_4, _YLOC("Load from Server")).c_str() },{ IDCANCEL, YtriaTranslate::Do(ModuleUser_getUsersWithListRequest_5, _YLOC("Continue with cache")).c_str() } });

				if (IDCANCEL == dlg.DoModal())
				{
					state.userAgreedToUpdateCache = false;
				}
			}
		}
	}

	YSafeCreateTaskMutable([this, p_Command, gridIsEmpty, shouldUseSql, state, taskData, p_CommandInfo, p_Action = p_Command.GetAutomationAction(), hwnd = p_Frame->GetSafeHwnd(), usersLoadMoreRequestInfo, bom, isUpdateAfterApply = p_Frame->HasLastUpdateOperations(), isUserDetailsFrame = !p_Frame->GetModuleCriteria().m_IDs.empty(), p_IsRefresh, gridLogger = p_Frame->GetGridLogger()]() mutable
	{
		const auto ids = p_CommandInfo.GetIds();
		const auto& moduleOptions = p_CommandInfo.Data2().is_type<ModuleOptions>()
			? p_CommandInfo.Data2().get_value<ModuleOptions>()
			: boost::YOpt<ModuleOptions>();

		auto usersPtr = std::make_shared<vector<BusinessUser>>();
		auto usersWithMoreLoadedPtr = std::make_shared<vector<BusinessUser>>();

		boost::YOpt<YTimeDate> refreshDate;
		auto& users = *usersPtr;
		auto& usersWithMoreLoaded = *usersWithMoreLoadedPtr;
		vector<BusinessUser> usersToDoLoadMore/* = usersLoadMoreRequestInfo*/;
		bool loadMoreOnlyReceivedUsers = true;

		const bool wantsAll = ids.empty();
		auto userIds = ids;

		bool genericProcess = true;
		std::shared_ptr<wstring> lastDeltaLink;
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("users"));

		// If true, user details uses a partial delta (on specific ids) if available, else user details use synced cache and/or individual requests
		// Delta cannot work if id selection can be changed in module (currently not the case).
		static constexpr bool usePartialDeltaForUserDetails = true;
		if (moduleOptions && !moduleOptions->m_CustomFilter.IsEmpty())
		{
			if (shouldUseSql)
			{
				// Request only min info? If we do, we must ensure cache is up-to-date (see above dialog and userAgreedToUpdateCache value).
				users = bom->GetGraphCache().GetPrefilteredUsers(taskData, moduleOptions->m_CustomFilter, logger).get();

				if (users.size() > 0 && !users[0].GetError())
				{
					O365IdsContainer ids;
					for (const auto& u : users)
						ids.insert(u.GetID());

					if (!ids.empty())
						users = bom->GetGraphCache().GetSqlCachedUsers(hwnd, ids, true, taskData);
				}
			}
			else
			{
				users = bom->GetGraphCache().GetPrefilteredUsers(taskData, moduleOptions->m_CustomFilter, logger).get();
			}

			genericProcess = false;
		}
		else if (!userIds.empty() && (usePartialDeltaForUserDetails ? (isUpdateAfterApply && (!isUserDetailsFrame || !state.hasDeltaCapabilities)) : (isUpdateAfterApply || isUserDetailsFrame)))
		{
			ASSERT(state.mode != State::SYNC || isUserDetailsFrame); // Specific case refresh after apply.

			// Load more requests will be done anyway, don't take them into account to decide
			const auto numElementsToRefresh = userIds.size() - usersLoadMoreRequestInfo.size();
			if (numElementsToRefresh <= 0 || !ShouldDoListRequests(bom->GetGraphCache().GetUserCount(), numElementsToRefresh, MSGraphSession::g_DefaultNbElementsPerPage))
			{
				// As it seems for efficient to do separate requests, do it:

				loadMoreOnlyReceivedUsers = false;

				ASSERT(users.empty());
				for (const auto& id : userIds)
				{
					auto it = std::find_if(usersLoadMoreRequestInfo.begin(), usersLoadMoreRequestInfo.end(), [&id](auto& u)
						{
							return id == u.GetID();
						});

					// Load more users will be requested later at the end of this method, because loadMoreOnlyReceivedUsers is false.

					if (usersLoadMoreRequestInfo.end() == it)
					{
						users.emplace_back();
						users.back().m_Id = id;
					}
				}

				if (!users.empty())
				{
					users = bom->GetBusinessUsersIndividualRequests(users, taskData).GetTask().get();
					bom->GetGraphCache().SaveUsersInSqlCache(users, { UBI::MIN, UBI::LIST, UBI::ASSIGNEDLICENSES }, refreshDate, false);
				}
			}
			else
			{
				// Maybe change this eventually: we currently force a load of the whole list (updated with sync) to avoid issue in grid modifications.
				if (state.hasDeltaCapabilities)
					users = bom->GetGraphCache().GetUpToDateUsers<BusinessUser>(userIds, logger, GraphCache::PreferredMode::DELTASYNC, taskData).get();
				else
					users = bom->GetGraphCache().GetUpToDateUsers<BusinessUser>(userIds, logger, GraphCache::PreferredMode::LIST, taskData).get();
				refreshDate = bom->GetGraphCache().GetSqlUsersCacheLastFullRefreshDate();
			}
			
			genericProcess = false;
		}
		else if (usePartialDeltaForUserDetails && !userIds.empty() && isUserDetailsFrame)
		{
			ASSERT(state.hasDeltaCapabilities);
			if (state.mode == State::LOAD || (state.mode == State::REFRESH && !isUpdateAfterApply) || !state.localDeltaLink)
			{
				ASSERT(!lastDeltaLink);
				lastDeltaLink = std::make_shared<wstring>(_YTEXT(""));
				users = bom->GetGraphCache().InitPartialUsersDelta(taskData, userIds, lastDeltaLink, logger).get();

				if (!users.empty())
				{
					O365IdsContainer ids;
					for (const auto& u : users)
						ids.insert(u.GetID());
					users = bom->GetGraphCache().GetSqlCachedUsers(hwnd, userIds, true, taskData);
				}
			}
			else
			{
				ASSERT(state.mode == State::SYNC || state.mode == State::REFRESH);
				lastDeltaLink = state.localDeltaLink ? std::make_shared<wstring>(*state.localDeltaLink) : std::make_shared<wstring>();
				users = bom->GetGraphCache().SyncUsersFromDeltaLink(taskData, logger, lastDeltaLink, false).get();

				if (state.mode == State::REFRESH)
				{
					ASSERT(isUpdateAfterApply);

					if (!users.empty())
					{
						O365IdsContainer ids;
						for (const auto& u : users)
							ids.insert(u.GetID());

						users = bom->GetGraphCache().GetSqlCachedUsers(hwnd, userIds, true, taskData);
					}
				}
			}
			genericProcess = false;
		}

		if (genericProcess)
		{
			ASSERT(state.mode != State::SYNC || wantsAll);
			ASSERT(state.mode != State::SYNC || state.hasDeltaCapabilities);

			if (state.mode == State::SYNC && state.hasSyncLink && state.hasDeltaCapabilities)
			{
				// Sync (+ update sql)
				users = bom->GetGraphCache().SyncUsersFromDeltaLink(taskData, logger, state.localDeltaLink).get();

				// Don't: Sync + load sql
				//users = bom->GetGraphCache().GetUpToDateUsers<BusinessUser>(userIds, logger, GraphCache::PreferredMode::DELTASYNC, taskData, state.localDeltaLink).get();

				refreshDate = bom->GetGraphCache().GetSqlUsersCacheLastFullRefreshDate();
			}
			else if (shouldUseSql && state.sqlCacheIsFull && (!state.sqlCacheIsExpired && !state.sqlCacheHasMissingInfo || !state.userAgreedToUpdateCache))
			{
				ASSERT(state.mode == State::LOAD);

				// load sql
				users = bom->GetGraphCache().GetSqlCachedUsers(hwnd, userIds, false, taskData);
				refreshDate = bom->GetGraphCache().GetSqlUsersCacheLastFullRefreshDate();
			}
			else if (shouldUseSql && (!state.sqlCacheIsFull || state.sqlCacheIsExpired || state.sqlCacheHasMissingInfo) && state.userAgreedToUpdateCache && state.hasDeltaCapabilities && state.hasSyncLink)
			{
				ASSERT(state.mode == State::LOAD);

				// Delta sync sql (sync + load sql)
				users = bom->GetGraphCache().GetUpToDateUsers<BusinessUser>(userIds, logger, GraphCache::PreferredMode::DELTASYNC, taskData, state.localDeltaLink).get();
				refreshDate = bom->GetGraphCache().GetSqlUsersCacheLastFullRefreshDate();
			}
			else if (state.mode == State::LOAD && (!state.sqlCacheIsFull || state.sqlCacheIsExpired || state.sqlCacheHasMissingInfo) && state.userAgreedToUpdateCache && state.hasDeltaCapabilities && !state.hasSyncLink)
			{
				// Init sql (delta)
				users = bom->GetGraphCache().GetUpToDateUsers<BusinessUser>(userIds, logger, GraphCache::PreferredMode::INITDELTA, taskData).get();
				refreshDate = bom->GetGraphCache().GetSqlUsersCacheLastFullRefreshDate();
			}
			else if (state.mode == State::LOAD && (!state.sqlCacheIsFull || state.sqlCacheIsExpired || state.sqlCacheHasMissingInfo) && state.userAgreedToUpdateCache && !state.hasDeltaCapabilities)
			{
				// Init sql (list)
				users = bom->GetGraphCache().GetUpToDateUsers<BusinessUser>(userIds, logger, GraphCache::PreferredMode::LIST, taskData).get();
				refreshDate = bom->GetGraphCache().GetSqlUsersCacheLastFullRefreshDate();
			}
			else if (state.hasDeltaCapabilities)
			{
				ASSERT(state.mode == State::REFRESH || state.mode == State::SYNC);
				// Init sql (delta)
				users = bom->GetGraphCache().GetUpToDateUsers<BusinessUser>(userIds, logger, GraphCache::PreferredMode::INITDELTA, taskData).get();
				refreshDate = bom->GetGraphCache().GetSqlUsersCacheLastFullRefreshDate();
			}
			else
			{
				ASSERT(state.mode == State::REFRESH);
				// Init sql (list)
				users = bom->GetGraphCache().GetUpToDateUsers<BusinessUser>(userIds, logger, GraphCache::PreferredMode::LIST, taskData).get();
				refreshDate = bom->GetGraphCache().GetSqlUsersCacheLastFullRefreshDate();
			}
		}

		if (!lastDeltaLink)
			lastDeltaLink = bom->GetGraphCache().GetUsersDeltaLink();

		// Load mores
		if (state.refreshMore && !usersLoadMoreRequestInfo.empty())
		{
			ASSERT(state.mode == State::REFRESH || state.mode == State::SYNC);

			// Move load more users to another vector
			{
				// This doesn't work for obscure reasons... bug in stl?
				// instead of moving the desired element to the end, it kinda duplicates another element.... WTF
				/*auto it = std::remove_if(users.begin(), users.end(), [&usersLoadMoreRequestInfo](auto& u)
					{
						return usersLoadMoreRequestInfo.end() != std::find_if(usersLoadMoreRequestInfo.begin(), usersLoadMoreRequestInfo.end(), [&u](const auto& uMore) {return uMore.GetID() == u.GetID(); });
					});

				if (users.end() != it)
				{
					std::move(it, users.end(), std::back_inserter(usersToDoLoadMore));
					users.erase(it, users.end());
				}*/

				for (const auto& lmu : usersLoadMoreRequestInfo)
				{
					auto it = std::find_if(users.begin(), users.end(), [&lmu](auto& u)
						{
							return lmu.GetID() == u.GetID();
						});

					if (users.end() != it)
					{
						if (!it->HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
						{
							std::move(it, it + 1, std::back_inserter(usersToDoLoadMore));
							users.erase(it);
						}
					}
					else if (!loadMoreOnlyReceivedUsers)
					{
						usersToDoLoadMore.push_back(lmu);
					}
				}
			}

			gridLogger->SetIgnoreHttp404Errors(true);
			RunOnScopeEnd _([&gridLogger]()
				{
					gridLogger->SetIgnoreHttp404Errors(false);
				});

			usersWithMoreLoaded = bom->MoreInfoBusinessUsers(usersToDoLoadMore, false, taskData).GetTask().get();

			std::copy(usersWithMoreLoaded.begin(), usersWithMoreLoaded.end(), std::back_inserter(users));
		}
		else
		{
			ASSERT(usersWithMoreLoaded.empty());
			// Copy more loaded users to another vector
			std::copy_if(users.begin(), users.end(), std::back_inserter(usersWithMoreLoaded), [](const auto& u)
				{
					return u.IsMoreLoaded();
				});
		}

		ensureAssignedByGroupsAreInCache(users, bom, taskData);
		ensureManagersAreInCache(users, bom, taskData);

		if (state.refreshMore && !usersLoadMoreRequestInfo.empty() && !usersWithMoreLoaded.empty())
		{
			LoggerService::User(_T("Writing cache to disk."), taskData.GetOriginator());

			// Useless, done in GetUpToDateUsers
			//bom->GetGraphCache().SaveUsersInSqlCache(users, { UBI::MIN, UBI::LIST, UBI::ASSIGNEDLICENSES }, refreshDate, clearCache);

			// Save load more users.
			// FIXME: which blocks of data to save here?
			// MoreInfoBusinessUsers() is now not configurable, always loads: sync props + UserAdditionalRequest::RequestMailboxSettingsForNonGuest | UserAdditionalRequest::RequestDriveForNonGuest | UserAdditionalRequest::RequestManager
			// Update as soon as we make it a user choice!
			bom->GetGraphCache().SaveUsersInSqlCache(usersWithMoreLoaded, { UBI::MIN, UBI::LIST, UBI::SYNCV1, UBI::SYNCBETA, UBI::MAILBOXSETTINGS, UBI::ARCHIVEFOLDERNAME, UBI::DRIVE, UBI::MANAGER, UBI::ASSIGNEDLICENSES }, boost::none, false);
		}

		if (!ids.empty())
		{
			// Ensure requested order is preserved (probably not very efficient when many ids)
			LoggerService::User(_T("Sorting users..."), taskData.GetOriginator());
			std::sort(users.begin(), users.end(), [&ids](const auto& lhUser, const auto& rhUser)
				{
					return ids.find(lhUser.GetID()) < ids.find(rhUser.GetID());
				});
		}

		YDataCallbackMessage<std::shared_ptr<vector<BusinessUser>>>::DoPost(usersPtr, [p_Command, gridIsEmpty, taskData, sync = state.mode == State::SYNC, p_Action, hwnd, usersWithMoreLoaded = usersWithMoreLoadedPtr, p_IsRefresh, isCanceled = taskData.IsCanceled(), isLoadAll = wantsAll, refreshDate, lastDeltaLink](std::shared_ptr<vector<BusinessUser>> p_Users)
		{
			ASSERT(p_Users);
			ASSERT(usersWithMoreLoaded);
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameUsers*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(*p_Users)))
			{
				ASSERT(nullptr != frame);
				frame->SetRefreshMode(RefreshMode::ListRequest);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s users...", Str::getStringFromNumber(p_Users->size()).c_str()));
				frame->GetGrid().ClearLog(taskData.GetId());
				//frame->GetGrid().ClearStatus();

				LoggerService::User(_T("Loading data into grid."), taskData.GetOriginator());
				{
					CWaitCursor _;
					bool allowFullPurge = true;
					if (frame->HasLastUpdateOperations())
					{
						allowFullPurge = !sync && !isCanceled && p_IsRefresh;
						frame->ShowUsers(*p_Users, *usersWithMoreLoaded, frame->GetLastUpdateOperations(), true, allowFullPurge);
						frame->ForgetLastUpdateOperations();
					}
					else
					{
						allowFullPurge = !sync && !isCanceled;
						frame->ShowUsers(*p_Users, *usersWithMoreLoaded, {}, true, allowFullPurge);
					}
				}

				if (refreshDate)
					frame->SetLastRefreshDate(*refreshDate);

				if (!p_IsRefresh)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}

				if (lastDeltaLink)
					frame->SetUsersDeltaLink(*lastDeltaLink);
				else
					frame->SetUsersDeltaLink(boost::none);
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.

				frame->ProcessLicenseContext(); // Let's consume license tokens if needed
				ModuleBase::TaskFinished(frame, taskData, p_Action);
			}

			if (frame)
			{
				auto& grid = dynamic_cast<GridUsers&>(frame->GetGrid());
				if (!AutomatedApp::IsAutomationRunning() && frame->GetBusinessObjectManager()->GetGraphCache().GetAutoLoadOnPrem() && grid.CanRefreshOnPrem() && gridIsEmpty)
				{
					auto actionCmd = std::make_shared<ShowOnPremUsersCommand>(*frame);
					CommandDispatcher::GetInstance().Execute(Command::CreateCommand(actionCmd, *frame, Command::ModuleTarget::User));
				}
			}
		});
	});
}

void ModuleUser::getUsersWithIndividualRequests(const Command& p_Command, const CommandInfo& p_CommandInfo, FrameUsers* p_Frame, bool p_IsRefresh)
{
    auto taskData = addFrameTask(YtriaTranslate::Do(ModuleUser_getUsersWithIndividualRequests_1, _YLOC("Show Users (subset)")).c_str(), p_Frame, p_Command, !p_IsRefresh);

	// Load Mores
	std::vector<BusinessUser> usersLoadMoreRequestInfo;
	GridUsers* gridUsers = dynamic_cast<GridUsers*>(&p_Frame->GetGrid());
	ASSERT(nullptr != gridUsers);
	if (nullptr != gridUsers)
	{
		std::vector<GridBackendRow*> rowsWithMoreLoaded;
		gridUsers->GetRowsWithMoreLoaded(rowsWithMoreLoaded);

		if (!rowsWithMoreLoaded.empty())
		{
			usersLoadMoreRequestInfo = gridUsers->GetLoadMoreRequestInfo(rowsWithMoreLoaded);
			usersLoadMoreRequestInfo.erase(std::remove_if(usersLoadMoreRequestInfo.begin(), usersLoadMoreRequestInfo.end(), [&ids = p_CommandInfo.GetIds()](const BusinessUser& user) { return ids.end() == ids.find(user.GetID()); }), usersLoadMoreRequestInfo.end());
		}
	}

	// FIXME: Ensure this is right
	const bool refreshFromSql = !p_IsRefresh;

	YSafeCreateTask([this, refreshFromSql, taskData, p_Action = p_Command.GetAutomationAction(), hwnd = p_Frame->GetSafeHwnd(), usersLoadMoreRequestInfo, bom = p_Frame->GetBusinessObjectManager(), ids = p_CommandInfo.GetIds(), p_IsRefresh, gridLogger = p_Frame->GetGridLogger()]()
	{
		/*Results*/
		auto businessUsersPtr = std::make_shared<vector<BusinessUser>>();
		auto usersWithMoreLoadedPtr = std::make_shared<vector<BusinessUser>>();
		auto& usersWithMoreLoaded = *usersWithMoreLoadedPtr;
		auto& businessUsers = *businessUsersPtr;


		vector<BusinessUser> usersToDoLoadMore = usersLoadMoreRequestInfo;
		vector<BusinessUser> usersToDoNoLoadMore;
		bool allRefreshedFromSql = refreshFromSql;
		auto userIds = ids;

		for (auto& id : userIds)
		{
			if (usersLoadMoreRequestInfo.end() == std::find_if(usersLoadMoreRequestInfo.begin(), usersLoadMoreRequestInfo.end(), [id](const BusinessUser& user) { return user.GetID() == id; }))
			{
				usersToDoNoLoadMore.emplace_back();
				usersToDoNoLoadMore.back().m_Id = id;
			}
		}

		if (refreshFromSql)
		{
			const auto sqlCachedUsers = bom->GetGraphCache().GetSqlCachedUsers(hwnd, userIds, { UBI::MIN, UBI::LIST, UBI::ASSIGNEDLICENSES }, true, taskData);
			businessUsers.reserve(userIds.size());
			for (auto& user : sqlCachedUsers)
			{
				const auto userIt = userIds.find(user.GetID());
				ASSERT(userIds.end() != userIt);
				if (userIds.end() != userIt)
				{
					userIds.erase(userIt);
					businessUsers.emplace_back(user);

					{
						auto it = std::find_if(usersToDoNoLoadMore.cbegin(), usersToDoNoLoadMore.cend(), [&user](const auto& p_User)
						{
							return user.GetID() == p_User.GetID();
						});

						if (usersToDoNoLoadMore.end() != it)
							usersToDoNoLoadMore.erase(it);
					}

					if (user.IsMoreLoaded())
					{
						usersWithMoreLoaded.emplace_back(user);
						auto it = std::find_if(usersToDoLoadMore.cbegin(), usersToDoLoadMore.cend(), [&user](const auto& p_User)
						{
							return p_User.GetID() == user.GetID();
						});

						if (usersToDoLoadMore.end() != it)
							usersToDoLoadMore.erase(it);
					}

					if (userIds.empty())
						break;					
				}
			}

			allRefreshedFromSql = usersToDoLoadMore.empty() && usersToDoNoLoadMore.empty();
		}

		if (!usersToDoNoLoadMore.empty())
		{
			auto bUsers = bom->GetBusinessUsersIndividualRequests(usersToDoNoLoadMore, taskData).GetTask().get();
			std::copy(std::make_move_iterator(std::begin(bUsers)), std::make_move_iterator(std::end(bUsers)), std::back_inserter(businessUsers));
		}

		// Load mores
		if (!usersLoadMoreRequestInfo.empty())
		{
			if (taskData.IsCanceled())
			{
				for (auto& u : usersToDoLoadMore)
				{
					usersWithMoreLoaded.emplace_back(u);
					usersWithMoreLoaded.back().SetFlags(BusinessObject::CANCELED);
					businessUsers.emplace_back(usersWithMoreLoaded.back());
				}
			}
			else if (!usersToDoLoadMore.empty())
			{
				ASSERT(!allRefreshedFromSql);
				gridLogger->SetIgnoreHttp404Errors(true);
				RunOnScopeEnd _([&gridLogger]()
					{
						gridLogger->SetIgnoreHttp404Errors(false);
					});

				auto moreLoadedUsers = bom->MoreInfoBusinessUsers(usersToDoLoadMore, false, taskData).GetTask().get();
				std::copy(std::begin(moreLoadedUsers), std::end(moreLoadedUsers), std::back_inserter(businessUsers));
				std::copy(std::make_move_iterator(std::begin(moreLoadedUsers)), std::make_move_iterator(std::end(moreLoadedUsers)), std::back_inserter(usersWithMoreLoaded));
			}

			ensureAssignedByGroupsAreInCache(businessUsers, bom, taskData);
			ensureManagersAreInCache(businessUsers, bom, taskData);

			if (!allRefreshedFromSql)
			{
				bom->GetGraphCache().SaveUsersInSqlCache(businessUsers, { UBI::MIN, UBI::LIST, UBI::ASSIGNEDLICENSES }, boost::none, false);
				// FIXME: which blocks of data to save here?
				// MoreInfoBusinessUsers() is now not configurable, always loads: sync props + UserAdditionalRequest::RequestMailboxSettingsForNonGuest | UserAdditionalRequest::RequestDriveForNonGuest | UserAdditionalRequest::RequestManager
				// Update as soon as we make it a user choice!
				bom->GetGraphCache().SaveUsersInSqlCache(usersWithMoreLoaded, { UBI::MIN, UBI::LIST, UBI::SYNCV1, UBI::SYNCBETA, UBI::MAILBOXSETTINGS, UBI::ARCHIVEFOLDERNAME, UBI::DRIVE, UBI::MANAGER, UBI::ASSIGNEDLICENSES }, boost::none, false);
			}

			ASSERT(usersWithMoreLoaded.size() == usersLoadMoreRequestInfo.size());
		}

		// Ensure requested order is preserved (probably not very efficient when many ids)
		LoggerService::User(_T("Sorting users..."), taskData.GetOriginator());
		std::sort(businessUsers.begin(), businessUsers.end(), [&ids](const BusinessUser& lhUser, const BusinessUser& rhUser)
		{
			return ids.find(lhUser.GetID()) < ids.find(rhUser.GetID());
		});

		YDataCallbackMessage<std::shared_ptr<vector<BusinessUser>>>::DoPost(businessUsersPtr, [taskData, p_Action, hwnd, usersWithMoreLoaded = usersWithMoreLoadedPtr, p_IsRefresh, isCanceled = taskData.IsCanceled()](const std::shared_ptr<vector<BusinessUser>>& p_Users)
		{
			ASSERT(p_Users);
			ASSERT(usersWithMoreLoaded);
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameUsers*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, isCanceled && !everythingCanceled(*p_Users)))
			{
				ASSERT(nullptr != frame);
				frame->SetRefreshMode(RefreshMode::IndividualRequest);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s users...", Str::getStringFromNumber(p_Users->size()).c_str()));
				frame->GetGrid().ClearLog(taskData.GetId());
				//frame->GetGrid().ClearStatus();

				{
					CWaitCursor _;
					if (frame->HasLastUpdateOperations())
					{
						frame->ShowUsers(*p_Users, *usersWithMoreLoaded, frame->GetLastUpdateOperations(), true, !isCanceled);
						frame->ForgetLastUpdateOperations();
					}
					else
					{
						frame->ShowUsers(*p_Users, *usersWithMoreLoaded, {}, true, !isCanceled);
					}
				}

				if (!p_IsRefresh)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, taskData, p_Action);
			}
		});
	});
}

void ModuleUser::ensureAssignedByGroupsAreInCache(vector<BusinessUser>& p_BusinessUsers, const std::shared_ptr<BusinessObjectManager>& p_BOM, YtriaTaskData p_TaskData)
{
	// Be sure all 'assigned by' groups are in memory cache.
	O365IdsContainer groupIds;
	for (auto& user : p_BusinessUsers)
	{
		if (user.GetLicenseAssignmentStates())
		{
			for (auto& las : *user.GetLicenseAssignmentStates())
			{
				// If not in cache
				if (las.m_AssignedByGroup && *las.m_AssignedByGroup != p_BOM->GetGraphCache().GetGroupInCache(*las.m_AssignedByGroup).GetID())
					groupIds.insert(*las.m_AssignedByGroup);
			}
		}
	}

	if (!groupIds.empty())
	{
		p_BOM->GetGraphCache().GetSqlCachedGroups(p_TaskData.GetOriginator(), groupIds, false);

		// FIXME: This logger creates unwanted display
		//auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("'assigned by' group"), groupIds.size());
		auto logger = std::make_shared<BasicRequestLogger>(_T("'assigned by' group"));

		// FIXME: Should we use delta sync here?
		// Ensure no group is missing...
		for (auto& id : groupIds)
		{
			BusinessGroup bg;
			bg.SetID(id);
			p_BOM->GetGraphCache().SyncUncachedGroup(bg, logger, p_TaskData, false);
		}
		ASSERT(std::all_of(groupIds.begin(), groupIds.end(), [&p_BOM](const auto& id) { return id == p_BOM->GetGraphCache().GetGroupInCache(id).GetID(); }));
	}
}

void ModuleUser::ensureManagersAreInCache(vector<BusinessUser>& p_BusinessUsers, const std::shared_ptr<BusinessObjectManager>& p_BOM, YtriaTaskData p_TaskData)
{
	// Should we delta-sync?
	//p_BOM->GetGraphCache().GetUpToDateUsers<CachedUser>()

	O365IdsContainer managerIds;
	for (auto& user : p_BusinessUsers)
	{
		if (user.GetManagerID())
		{
			// If not in cache
			if (*user.GetManagerID() != p_BOM->GetGraphCache().GetUserInCache(*user.GetManagerID(), false).GetID())
				managerIds.insert(*user.GetManagerID());
		}
	}

	if (!managerIds.empty())
	{
		// Do not pass p_TaskData if we don't want to allow cancellation of this step.
		p_BOM->GetGraphCache().GetSqlCachedUsers(p_TaskData.GetOriginator(), managerIds, false, p_TaskData);

		// FIXME: This logger creates unwanted display
		//auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("manager"), groupIds.size());
		auto logger = std::make_shared<BasicRequestLogger>(_T("manager"));

		// FIXME: Should we use delta sync here?
		// Ensure no group is missing...
		for (auto& id : managerIds)
		{
			BusinessUser bu;
			bu.SetID(id);
			p_BOM->GetGraphCache().SyncUncachedUser(bu, logger, p_TaskData, false);
		}
		ASSERT(std::all_of(managerIds.begin(), managerIds.end(), [&p_BOM](const auto& id) { return id == p_BOM->GetGraphCache().GetUserInCache(id, false).GetID(); }));
	}
}
