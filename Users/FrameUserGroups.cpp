#include "FrameUserGroups.h"

#include "CommandDispatcher.h"
#include "GridUpdater.h"

IMPLEMENT_DYNAMIC(FrameUserGroups, GridFrameBase)

void FrameUserGroups::createGrid()
{
	BOOL gridCreated = m_GridUserGroups.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
	ASSERT(gridCreated);
}

GridFrameBase::O365ControlConfig FrameUserGroups::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigUsers();
}

GridFrameBase::O365ControlConfig FrameUserGroups::GetApplyPartialControlConfig()
{
	return GetApplyPartialControlConfigUsers();
}

bool FrameUserGroups::customizeActionsRibbonTab(CXTPRibbonTab& p_Tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(p_Tab);
	CreateRefreshGroup(p_Tab);

	if (!m_GridUserGroups.IsMyData())
	{
		CreateApplyGroup(p_Tab);
		CreateRevertGroup(p_Tab);

		auto groupCtrl1 = p_Tab.AddGroup(YtriaTranslate::Do(FrameUserGroups_customizeActionsRibbonTab_2, _YLOC("Member")).c_str());
		setGroupImage(*groupCtrl1, 0);

		{
			auto ctrl = groupCtrl1->Add(xtpControlButton, ID_USERGROUPSGRID_ADDTOGROUP);
			GridFrameBase::setImage({ ID_USERGROUPSGRID_ADDTOGROUP }, IDB_USERS_ADDTOGROUP, xtpImageNormal);
			GridFrameBase::setImage({ ID_USERGROUPSGRID_ADDTOGROUP }, IDB_USERS_ADDTOGROUP_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		auto groupCtrl = p_Tab.AddGroup(YtriaTranslate::Do(FrameUserGroups_customizeActionsRibbonTab_1, _YLOC("Memberships")).c_str());
		setGroupImage(*groupCtrl, 0);

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_USERGROUPSGRID_REMOVEFROMGROUP);
			GridFrameBase::setImage({ ID_USERGROUPSGRID_REMOVEFROMGROUP }, IDB_USERS_REMOVEFROMGROUP, xtpImageNormal);
			GridFrameBase::setImage({ ID_USERGROUPSGRID_REMOVEFROMGROUP }, IDB_USERS_REMOVEFROMGROUP_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	
		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_USERGROUPSGRID_COPYTO);
			GridFrameBase::setImage({ ID_USERGROUPSGRID_COPYTO }, IDB_USERS_COPYMEMBERSHIP, xtpImageNormal);
			GridFrameBase::setImage({ ID_USERGROUPSGRID_COPYTO }, IDB_USERS_COPYMEMBERSHIP_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_USERGROUPSGRID_TRANSFERTO);
			GridFrameBase::setImage({ ID_USERGROUPSGRID_TRANSFERTO }, IDB_USERS_TRANSFERMEMBERSHIP, xtpImageNormal);
			GridFrameBase::setImage({ ID_USERGROUPSGRID_TRANSFERTO }, IDB_USERS_TRANSFERMEMBERSHIP_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	CreateLinkGroups(p_Tab, !m_GridUserGroups.IsMyData(), true/*!m_GridUserGroups.IsMyData()*/);

	automationAddCommandIDToGreenLight(ID_USERGROUPSGRID_ADDTOGROUP);
	automationAddCommandIDToGreenLight(ID_USERGROUPSGRID_REMOVEFROMGROUP);
	automationAddCommandIDToGreenLight(ID_USERGROUPSGRID_COPYTO);
	automationAddCommandIDToGreenLight(ID_USERGROUPSGRID_TRANSFERTO);

	return true;
}

FrameUserGroups::FrameUserGroups(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
	, m_GridUserGroups(p_IsMyData)
{
    m_CreateAddRemoveToSelection = !p_IsMyData;
    m_CreateRefreshPartial = !p_IsMyData;
	m_CreateShowContext = !p_IsMyData;
	m_CreateApplyPartial = !p_IsMyData;
}

void FrameUserGroups::ShowUserWithParentGroups(const vector<BusinessUser>& p_Users, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge)
{
	AddUserWithParentGroups(p_Users, true, p_UpdateOperations, p_FullPurge);
}

void FrameUserGroups::AddUserWithParentGroups(const vector<BusinessUser>& p_Users, const bool p_IsRefresh, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge)
{
	m_GridUserGroups.AddUserWithParentGroups(p_Users, p_IsRefresh, p_UpdateOperations, p_FullPurge);
}

O365Grid& FrameUserGroups::GetGrid()
{
	return m_GridUserGroups;
}

void FrameUserGroups::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameUserGroups::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameUserGroups::ApplySpecific(bool p_Selected)
{
    auto pGrid = dynamic_cast<GridUserGroups*>(&GetGrid());
    ASSERT(nullptr != pGrid);

    CommandInfo info;
    info.Data() = pGrid->GetGroupChanges(p_Selected, true);
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Group, Command::ModuleTask::UpdateGroupItems, info, { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
}

void FrameUserGroups::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	ASSERT(Origin::User == GetModuleCriteria().m_Origin);
	ASSERT(ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer);

	CommandInfo info;
	info.SetOrigin(GetModuleCriteria().m_Origin);
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

    if (HasLastUpdateOperations())
		GridUpdater::HandleCreatedModifications(m_GridUserGroups, AccessLastUpdateOperations());

	if (HasLastUpdateOperations() && !ShouldForceFullRefreshAfterApply())
	{
		auto& ids = info.GetIds();
		ids.clear();
		for (const auto& updateOp : GetLastUpdateOperations())
			ids.insert(updateOp.GetPkFieldStr(m_GridUserGroups.GetColUserId()));
    }
    else
    {
    	info.GetIds() = GetModuleCriteria().m_IDs;
    }

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::User, Command::ModuleTask::ListOwners, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameUserGroups::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::User, Command::ModuleTask::ListOwners, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

AutomatedApp::AUTOMATIONSTATUS FrameUserGroups::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;
	
	if (IsActionGroupMembershipAdd(p_Action))
		p_CommandID = ID_USERGROUPSGRID_ADDTOGROUP;
	else if (IsActionGroupMembershipRemove(p_Action))
		p_CommandID = ID_USERGROUPSGRID_REMOVEFROMGROUP;
	else if (IsActionGroupMembershipCopy(p_Action))
		p_CommandID = ID_USERGROUPSGRID_COPYTO;
	else if (IsActionGroupMembershipTransfer(p_Action))
		p_CommandID = ID_USERGROUPSGRID_TRANSFERTO;	
	
	return statusRV;
}