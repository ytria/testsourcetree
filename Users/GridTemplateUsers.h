#pragma once

#include "BaseO365Grid.h"
#include "GridTemplate.h"
#include "OnPremiseUsersColumns.h"
#include "SqlCacheConfig.h"
#include <boost/bimap.hpp>
#include <array>

#define USER_DISPLAY_NAME_COL_UID "userDisplayName"

class BusinessUser;
class GridUpdater;

class GridTemplateUsers : public GridTemplate
{
public:
    GridTemplateUsers();
    GridTemplateUsers(bool p_ShowCachedUsersOnly);
	GridBackendRow* AddRow(O365Grid& p_Grid, const BusinessUser& p_BU, const vector<GridBackendField>& p_ExtraRowPKValues, GridUpdater& p_Updater, bool p_SetCreatedStatus, bool p_KeepExistingIfUserCanceled);// p_ExtraPKValuesByColumnID contains row pk values not contained in BusinessUser (e.g. parent row Group ID)
	bool			RemoveRow(O365Grid& p_Grid, const BusinessUser& p_BU);
	GridBackendRow* UpdateRow(O365Grid& p_Grid, const BusinessUser& p_BU, bool p_SetModifiedStatus, GridUpdater& p_Updater, bool p_FromLoadMore);
	GridBackendRow* UpdateRow(O365Grid& p_Grid, const OnPremiseUser& p_OnPremUser, const map<wstring, row_pk_t>& p_O365RowPk, map<wstring, row_pk_t>& p_OnPremRowPk, UpdateFieldOption p_UpdateFieldOption = UpdateFieldOption::NONE);
	GridBackendRow* UpdateRow(O365Grid& p_Grid, const OnPremiseUser& p_OnPremUser, const map<wstring, row_pk_t>& p_O365RowPk, map<wstring, row_pk_t>& p_OnPremRowPk, std::set<PooledString>& p_NewImmutableIds, UpdateFieldOption p_UpdateFieldOption = UpdateFieldOption::NONE);
	GridBackendRow* UpdateRowMailbox(O365Grid& p_Grid, const BusinessUser& p_BU, GridUpdater& p_Updater);

	void			GetBusinessUser(const O365Grid& p_Grid, GridBackendRow* p_Row, BusinessUser& p_BU) const;
	row_pk_t		CreateRowPk(const OnPremiseUser& p_OnPremUser);

	bool GetOnPremiseUser(const O365Grid& p_Grid, GridBackendRow* p_Row, OnPremiseUser& p_OnPremUser) const;

    bool IsShowCachedUsersOnly() const;

	static COLORREF g_ColorRowUser;

	bool GetBusinessUserPK(O365Grid& p_Grid, const BusinessUser& p_BU, vector<GridBackendField>& BUpk) const;

	GridBackendColumn*& GetColumnForProperty(const wstring& p_PropertyName);
	GridBackendColumn* AddDefaultColumnFor(O365Grid& p_Grid, const wstring& p_PropertyName, const wstring& p_FamilyName, const set<GridBackendUtil::COLUMNPRESETFLAGS>& p_Flags);
	GridBackendColumn* AddDefaultColumnFor(O365Grid& p_Grid, const wstring& p_PropertyName, const wstring& p_FamilyName, const set<GridBackendUtil::COLUMNPRESETFLAGS>& p_Flags, const wstring& p_CustomName, const wstring& p_CustomUniqueID);

	virtual void CustomizeGrid(O365Grid& p_Grid) override;

	struct DirectRowUpdater
	{
	public:
		DirectRowUpdater(GridTemplateUsers& p_Template)
			: m_Template(p_Template)
		{}

		void operator()(O365Grid& p_Grid, const BusinessUser& p_BU, GridBackendRow* p_Row, UpdateFieldOption p_UpdateFieldOption)
		{
			m_Template.updateRow(p_Grid, p_BU, p_Row, p_UpdateFieldOption);
		}

	private:
		GridTemplateUsers& m_Template;
	};

	void MarkUserRowWithRevokePending(O365Grid& p_Grid, GridBackendRow* p_Row, GridUpdater& p_Updater);

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column);
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column);

	void SetO365(O365Grid& p_Grid, GridBackendRow& p_Row, bool p_Guest);
	void SetO365(O365Grid& p_Grid, GridBackendRow& p_Row, const BusinessUser& p_User);
	void SetOnPrem(O365Grid& p_Grid, GridBackendRow& p_Row);
	void SetHybrid(O365Grid& p_Grid, GridBackendRow& p_Row, const BusinessUser& p_User);
	void SetHybrid(O365Grid& p_Grid, GridBackendRow& p_Row);

	void UpdateCommonInfo(GridBackendRow* p_Row);

public:
	GridBackendColumn*& GetRequestDateColumn(UBI p_BlockId);

private:
	void updateRow(O365Grid& p_Grid, const BusinessUser& p_BU, GridBackendRow* p_Row, UpdateFieldOption p_UpdateFieldOption);

	template<typename T>
	GridBackendField* updateField(const T& p_Value, GridBackendRow* p_Row, GridBackendColumn* p_Col, UpdateFieldOption p_UpdateFieldOption, const PooledString& p_AlternateStringValue = PooledString());

	template<typename T>
	GridBackendField* updateFieldOnPrem(const T& p_Value, GridBackendRow* p_Row, GridBackendColumn* p_Col, UpdateFieldOption p_UpdateFieldOption);

	void updateRowMailboxSettings(const BusinessUser& p_BU, GridBackendRow* p_Row, UpdateFieldOption p_UpdateFieldOption);
	void updateRowMailbox(const BusinessUser& p_BU, GridBackendRow* p_Row, UpdateFieldOption p_UpdateFieldOption);

	void setLastRequestDateTime(O365Grid& p_Grid, const BusinessUser& p_BU, GridBackendRow* p_Row);

	static GridBackendColumn*& getColumnForProperty(GridTemplateUsers& p_That, const wstring& p_PropertyName);

	static void setValue(OnPremiseUser& p_OnPremUser, std::function<void(OnPremiseUser&, const boost::YOpt<PooledString>&)> p_Setter, const GridBackendField& p_Field);
	static void setValue(OnPremiseUser& p_OnPremUser, std::function<void(OnPremiseUser&, const boost::YOpt<YTimeDate>&)> p_Setter, const GridBackendField& p_Field);
	static void setValue(OnPremiseUser& p_OnPremUser, std::function<void(OnPremiseUser&, const boost::YOpt<int32_t>&)> p_Setter, const GridBackendField& p_Field);
	static void setValue(OnPremiseUser& p_OnPremUser, std::function<void(OnPremiseUser&, const boost::YOpt<bool>&)> p_Setter, const GridBackendField& p_Field);

public:
	GridBackendColumn* m_ColumnAboutMe;
	GridBackendColumn* m_ColumnAccountEnabled;
	GridBackendColumn* m_ColumnBirthday;
	GridBackendColumn* m_ColumnBusinessPhones;
	GridBackendColumn* m_ColumnCity;
    GridBackendColumn* m_ColumnCompanyName;
	GridBackendColumn* m_ColumnCountry;
	GridBackendColumn* m_ColumnDepartment;
	GridBackendColumn* m_ColumnGivenName;
	GridBackendColumn* m_ColumnHireDate;
    GridBackendColumn* m_ColumnImAddresses;
	GridBackendColumn* m_ColumnInterests;
	GridBackendColumn* m_ColumnJobTitle;
	GridBackendColumn* m_ColumnMail;
	GridBackendColumn* m_ColumnMailNickname;
    GridBackendColumn* m_ColumnMailboxSettingsArchiveFolderId;
	GridBackendColumn* m_ColumnMailboxSettingsArchiveFolderName;
    GridBackendColumn* m_ColumnMailboxSettingsTimeZone;
    GridBackendColumn* m_ColumnMailboxSettingsLanguageLocale;
    GridBackendColumn* m_ColumnMailboxSettingsLanguageDisplayName;
    GridBackendColumn* m_ColumnMailboxSettingsAutomaticRepliesSettingsStatus;
    GridBackendColumn* m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeDateTime;
    GridBackendColumn* m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeTimeZone;
    GridBackendColumn* m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeDateTime;
    GridBackendColumn* m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeTimeZone;
    GridBackendColumn* m_ColumnMailboxSettingsAutomaticRepliesSettingsInternalReplyMessage;
    GridBackendColumn* m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalReplyMessage;
    GridBackendColumn* m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalAudience;
	GridBackendColumn* m_ColumnMailboxSettingsTimeFormat;
	GridBackendColumn* m_ColumnMailboxSettingsDateFormat;
	GridBackendColumn* m_ColumnMailboxSettingsDelegateMeetingMessageDeliveryOptions;
	GridBackendColumn* m_ColumnMailboxSettingsWorkingHoursDaysOfWeek;
	GridBackendColumn* m_ColumnMailboxSettingsWorkingHoursStartTime;
	GridBackendColumn* m_ColumnMailboxSettingsWorkingHoursEndTime;
	GridBackendColumn* m_ColumnMailboxSettingsWorkingHoursTimeZoneName;
	GridBackendColumn* m_ColumnManagerDisplayName;
	GridBackendColumn* m_ColumnManagerUserPrincipalName;
	GridBackendColumn* m_ColumnManagerID;
    GridBackendColumn* m_ColumnMobilePhone;
	GridBackendColumn* m_ColumnMySite;
	GridBackendColumn* m_ColumnOfficeLocation;
	GridBackendColumn* m_ColumnOPImmutableId;
	GridBackendColumn* m_ColumnOPLastSync;
	GridBackendColumn* m_ColumnOPSecurityID;
	GridBackendColumn* m_ColumnOPSyncEnabled;
	GridBackendColumn* m_ColumnPasswordPolicies;
	// GridBackendColumn* m_ColumnPasswordProfile;
	GridBackendColumn* m_ColumnPassword;
	GridBackendColumn* m_ColumnForceChangePassword;
	GridBackendColumn* m_ColumnForceChangePasswordWithMfa;
	GridBackendColumn* m_ColumnPastProjects;
	GridBackendColumn* m_ColumnPostalCode;
	GridBackendColumn* m_ColumnPreferredLanguage;
	GridBackendColumn* m_ColumnPreferredName;
	GridBackendColumn* m_ColumnProxyAddresses;
	GridBackendColumn* m_ColumnResponsibilities;
	GridBackendColumn* m_ColumnSchools;
	GridBackendColumn* m_ColumnSkills;
	GridBackendColumn* m_ColumnState;
	GridBackendColumn* m_ColumnStreetAddress;
	GridBackendColumn* m_ColumnSurname;
	GridBackendColumn* m_ColumnUsageLocation;
	GridBackendColumn* m_ColumnUserPrincipalName;
	GridBackendColumn* m_ColumnUserType;
	GridBackendColumn* m_ColumnDeletedDateTime;
	GridBackendColumn* m_ColumnSkuIds;
	GridBackendColumn* m_ColumnSkuPartNumbers;
	GridBackendColumn* m_ColumnSkuNames;

	// Recently added (2019/01/31)
	GridBackendColumn* m_ColumnAgeGroup;
	GridBackendColumn* m_ColumnCreatedDateTime;
	GridBackendColumn* m_ColumnConsentProvidedForMinor;
	GridBackendColumn* m_ColumnLegalAgeGroupClassification;
	GridBackendColumn* m_ColumnSignInSessionsValidFromDateTime;
	GridBackendColumn* m_ColumnOPDomainName;
	GridBackendColumn* m_ColumnOPSamAccountName;
	GridBackendColumn* m_ColumnOPUserPrincipalName;
	GridBackendColumn* m_ColumnOPProvisioningErrorsCategory;
	GridBackendColumn* m_ColumnOPProvisioningErrorsOccuredDateTime;
	GridBackendColumn* m_ColumnOPProvisioningErrorsPropertyCausingError;
	GridBackendColumn* m_ColumnOPProvisioningErrorsValue;

	GridBackendColumn* m_ColumnOtherMails;
	GridBackendColumn* m_ColumnPreferredDataLocation;
	GridBackendColumn* m_ColumnShowInAddressList;
	GridBackendColumn* m_ColumnExternalUserStatus;
	GridBackendColumn* m_ColumnExternalUserStatusChangedOn;

	#define NUM_OP_EXTENSION_ATTRIBUTES 15
	static const int g_NumOPExtensionAttributes = NUM_OP_EXTENSION_ATTRIBUTES;
	GridBackendColumn* m_ColumnsOPExtensionAttributes1;
	GridBackendColumn* m_ColumnsOPExtensionAttributes2;
	GridBackendColumn* m_ColumnsOPExtensionAttributes3;
	GridBackendColumn* m_ColumnsOPExtensionAttributes4;
	GridBackendColumn* m_ColumnsOPExtensionAttributes5;
	GridBackendColumn* m_ColumnsOPExtensionAttributes6;
	GridBackendColumn* m_ColumnsOPExtensionAttributes7;
	GridBackendColumn* m_ColumnsOPExtensionAttributes8;
	GridBackendColumn* m_ColumnsOPExtensionAttributes9;
	GridBackendColumn* m_ColumnsOPExtensionAttributes10;
	GridBackendColumn* m_ColumnsOPExtensionAttributes11;
	GridBackendColumn* m_ColumnsOPExtensionAttributes12;
	GridBackendColumn* m_ColumnsOPExtensionAttributes13;
	GridBackendColumn* m_ColumnsOPExtensionAttributes14;
	GridBackendColumn* m_ColumnsOPExtensionAttributes15;
	static const std::array<GridBackendColumn* GridTemplateUsers::*, NUM_OP_EXTENSION_ATTRIBUTES> m_ColumnsOPExtensionAttributes;

	constexpr GridBackendColumn*& GetOPAttributeColumn(int p_Index);
	// Inline cause called by GridUsers
	constexpr GridBackendColumn* const & GetOPAttributeColumn(int p_Index) const
	{
		return this->*(m_ColumnsOPExtensionAttributes[p_Index]);
	}

	GridBackendColumn* m_ColumnEmployeeID;
	GridBackendColumn* m_ColumnFaxNumber;
	GridBackendColumn* m_ColumnIsResourceAccount;
	GridBackendColumn* m_ColumnOPDistinguishedName;

	// Recently added (2020/05/04)
	GridBackendColumn* m_ColumnCreationType;
	GridBackendColumn* m_ColumnIdentitiesSignInType;
	GridBackendColumn* m_ColumnIdentitiesIssuer;
	GridBackendColumn* m_ColumnIdentitiesIssuerAssignedId;
	GridBackendColumn* m_ColumnLastPasswordChangeDateTime;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesAssignedByGroupId;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesAssignedByGroupDisplayName;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesAssignedByGroupMail;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesState;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesError;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesSkuId;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesSkuPartNumber;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesSkuName;
	GridBackendColumn* m_ColumnRefresTokenValidFromDateTime;

	GridBackendColumn* m_ColumnIsAnyLicenseAssignedByGroup;

	GridBackendColumn* m_ColumnMailboxType;
	GridBackendColumn* m_ColumnMailboxDeliverToMailboxAndForward;
	GridBackendColumn* m_ColumnMailboxForwardingSmtpAddress;
	GridBackendColumn* m_ColumnMailboxForwardingAddress;
	GridBackendColumn* m_ColumnMailboxCanSendOnBehalf;
	GridBackendColumn* m_ColumnRecipientPermissionsCanSendAsUser;

	GridBackendColumn* m_ColumnLastRequestDateTime;
	std::map<UBI, GridBackendColumn*> m_ColumnsRequestDateTime;
		
	// Drive columns
	GridBackendColumn* m_ColumnDriveName;
	GridBackendColumn* m_ColumnDriveId;
	GridBackendColumn* m_ColumnDriveDescription;
	GridBackendColumn* m_ColumnDriveQuotaState;
	GridBackendColumn* m_ColumnDriveQuotaUsed;
	GridBackendColumn* m_ColumnDriveQuotaRemaining;
	GridBackendColumn* m_ColumnDriveQuotaDeleted;
	GridBackendColumn* m_ColumnDriveQuotaTotal;
	GridBackendColumn* m_ColumnDriveCreationTime;
	GridBackendColumn* m_ColumnDriveLastModifiedTime;
	GridBackendColumn* m_ColumnDriveType;
	GridBackendColumn* m_ColumnDriveWebUrl;
	GridBackendColumn* m_ColumnDriveCreatedByUserEmail;
	GridBackendColumn* m_ColumnDriveCreatedByUserId;
	GridBackendColumn* m_ColumnDriveCreatedByAppName;
	GridBackendColumn* m_ColumnDriveCreatedByAppId;
	GridBackendColumn* m_ColumnDriveCreatedByDeviceName;
	GridBackendColumn* m_ColumnDriveCreatedByDeviceId;
	GridBackendColumn* m_ColumnDriveOwnerUserName;
	GridBackendColumn* m_ColumnDriveOwnerUserId;
	GridBackendColumn* m_ColumnDriveOwnerUserEmail;
	GridBackendColumn* m_ColumnDriveOwnerAppName;
	GridBackendColumn* m_ColumnDriveOwnerAppId;
	GridBackendColumn* m_ColumnDriveOwnerDeviceName;
	GridBackendColumn* m_ColumnDriveOwnerDeviceId;

	OnPremiseUsersColumns m_OnPremCols;

	int m_AllowedIconId;
	int m_BlockedIconId;
	int m_GroupLicensingIconId;
	int m_MailboxLoadedIconId;
	int m_MailboxNotLoadedIconId;

	static boost::bimap<PooledString, PooledString> g_CreationTypes;

	static wstring g_GroupLicensing;

private:
    bool m_ShowCachedUsersOnly;
};
