#pragma once

#include "BaseO365Grid.h"
#include "GridTemplateUsers.h"

class GridUsersRecycleBin : public O365Grid
{
public:
	GridUsersRecycleBin();
	virtual ~GridUsersRecycleBin();

	void BuildView(vector<BusinessUser> p_Users, const vector<O365UpdateOperation>& p_UpdateOperations);

	afx_msg void OnRestore();
	afx_msg void OnUpdateRestore(CCmdUI* pCmdUI);

	afx_msg void OnHardDelete();
	afx_msg void OnUpdateHardDelete(CCmdUI* pCmdUI);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
	virtual void customizeGrid() override;
	virtual void InitializeCommands() override;

	DECLARE_MESSAGE_MAP()

private:
    void RemoveUsersNotModified(vector<BusinessUser>& p_Users, const vector<O365UpdateOperation>& p_UpdateOperations);
	
	virtual const wstring&	annotationGetModuleName() const override;
	virtual void			annotationModuleSetDefault(GridAnnotation& p_NewAnnotation) const;

	GridTemplateUsers m_TemplateUsers;
};

