#include "FrameUsers.h"

#include "AutomationNames.h"
#include "BusinessObjectManager.h"
#include "CommandDispatcher.h"
#include "DlgOnPremServerConfig.h"
#include "FlatObjectListFrameRefresher.h"
#include "ForceSyncOnPremCommand.h"
#include "GridUpdater.h"
#include "OnPremRevertSelectedButtonUpdater.h"
#include "OnPremRevertAllButtonUpdater.h"
#include "OnPremSaveSelectedButtonUpdater.h"
#include "OnPremSaveAllButtonUpdater.h"
#include "PersistentCacheUtil.h"
#include "RevokeAccessFieldUpdate.h"
#include "UpdatedObjectsGenerator.h"
#include "YDataCallbackMessage.h"
#include "DlgOnPremAADConnectServer.h"

IMPLEMENT_DYNAMIC(FrameUsers, GridFrameBase)

BEGIN_MESSAGE_MAP(FrameUsers, GridFrameBase)
	/*ON_COMMAND(ID_APPLYALLONPREM, OnApplyAllOnPrem)
	ON_UPDATE_COMMAND_UI(ID_APPLYALLONPREM, OnUpdateApplyAllOnPrem)
	ON_COMMAND(ID_APPLYSELECTEDONPREM, OnApplySelectedOnPrem)
	ON_UPDATE_COMMAND_UI(ID_APPLYSELECTEDONPREM, OnUpdateApplySelectedOnPrem)
	ON_COMMAND(ID_REVERTALLONPREM, OnRevertAllOnPrem)
	ON_UPDATE_COMMAND_UI(ID_REVERTALLONPREM, OnUpdateRevertAllOnPrem)
	ON_COMMAND(ID_REVERTSELECTEDONPREM, OnRevertSelectedOnPrem)
	ON_UPDATE_COMMAND_UI(ID_REVERTSELECTEDONPREM, OnUpdateRevertSelectedOnPrem)*/
	ON_COMMAND(ID_FORCESYNCONPREM, OnForceSyncOnPrem)
	ON_UPDATE_COMMAND_UI(ID_FORCESYNCONPREM, OnUpdateForceSyncOnPrem)
END_MESSAGE_MAP()

FrameUsers::FrameUsers(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame, true)
{
	m_ModBtnsUpdater.SetRevertAllEnableUpdater(std::make_unique<OnPremRevertAllButtonUpdater<FrameUsers>>(*this));
	m_ModBtnsUpdater.SetRevertSelectedEnableUpdater(std::make_unique<OnPremRevertSelectedButtonUpdater<FrameUsers>>(*this));
	m_ModBtnsUpdater.SetSaveAllEnableUpdater(std::make_unique<OnPremSaveAllButtonUpdater<FrameUsers>>(*this));
	m_ModBtnsUpdater.SetSaveSelectedEnableUpdater(std::make_unique<OnPremSaveSelectedButtonUpdater<FrameUsers>>(*this));
}

void FrameUsers::applyOnPremSpecific(bool p_SelectednOnly)
{
	GetGrid().StoreSelection();

	{
		CommandInfo info;
		info.Data() = m_GridUsers.GetOnPremiseChanges(p_SelectednOnly);
		info.SetFrame(this);
		info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

		CommandDispatcher::GetInstance().Execute(
			Command(GetLicenseContext(),
				GetSafeHwnd(),
				Command::ModuleTarget::User,
				Command::ModuleTask::ApplyOnPremChanges,
				info,
				{ &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
	}

	GetGrid().RestoreSelection();
}

//void FrameUsers::OnApplyAllOnPrem()
//{
//	YCodeJockMessageBox confirmation(this,
//		DlgMessageBox::eIcon_Question,
//		_YTEXT("Save All On-Premises Changes"),
//		_YTEXT("Are you sure?"),
//		_YTEXT(""),
//		{ { IDOK, _YTEXT("Yes") },{ IDCANCEL, _YTEXT("No") } });
//	if (confirmation.DoModal() == IDOK)
//		applyOnPremSpecific(false);
//}
//
//void FrameUsers::OnUpdateApplyAllOnPrem(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable(IsConnected() && HasNoTaskRunning() && hasOnPremiseChanges(false));
//}
//
//void FrameUsers::OnApplySelectedOnPrem()
//{
//	YCodeJockMessageBox confirmation(this,
//		DlgMessageBox::eIcon_Question,
//		_YTEXT("Save Selected On-Premises Changes"),
//		_YTEXT("Are you sure?"),
//		_YTEXT(""),
//		{ { IDOK, _YTEXT("Yes") },{ IDCANCEL, _YTEXT("No") } });
//	if (confirmation.DoModal() == IDOK)
//		applyOnPremSpecific(true);
//}
//
//void FrameUsers::OnUpdateApplySelectedOnPrem(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable(IsConnected() && HasNoTaskRunning() && hasOnPremiseChanges(true));
//}
//
//void FrameUsers::OnRevertAllOnPrem()
//{
//	YCodeJockMessageBox confirmation(
//		this,
//		DlgMessageBox::eIcon_ExclamationWarning,
//		_T("Undo all On-Premises changes"),
//		_YTEXT("Are you sure?"),
//		_YTEXT("All the On-Premises pending changes will be lost."),
//		{ { IDOK, _YTEXT("Yes") },{ IDCANCEL, _YTEXT("No") } }
//	);
//
//	if (IDOK == confirmation.DoModal())
//		m_GridUsers.RevertOnPrem();
//}
//
//void FrameUsers::OnUpdateRevertAllOnPrem(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable(hasOnPremiseChanges() && HasNoTaskRunning());
//}
//
//void FrameUsers::OnRevertSelectedOnPrem()
//{
//	YCodeJockMessageBox confirmation(
//		this,
//		DlgMessageBox::eIcon_ExclamationWarning,
//		_T("Undo selected On-Premises changes"),
//		_YTEXT("Are you sure?"),
//		_YTEXT("Pending On-Premises changes in selection will be lost."),
//		{ { IDOK, _YTEXT("Yes") },{ IDCANCEL, _YTEXT("No") } }
//	);
//
//	if (IDOK == confirmation.DoModal())
//		m_GridUsers.RevertOnPrem(true);
//}
//
//void FrameUsers::OnUpdateRevertSelectedOnPrem(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable(hasOnPremiseChanges(true) && HasNoTaskRunning());
//}

void FrameUsers::OnUpdateForceSyncOnPrem(CCmdUI* pCmdUI)
{
	bool hasComputerName = !GetBusinessObjectManager()->GetGraphCache().GetAADComputerName().empty();
	pCmdUI->Enable(HasNoTaskRunning() && hasComputerName);
}

void FrameUsers::OnSetAADComputerName()
{
	const bool showWarningOnAnchorChange = true; //FIXME: It could be true only if onprem already loaded.
	DlgOnPremAADConnectServer dlg(showWarningOnAnchorChange, this);
	dlg.DoModal();
}

void FrameUsers::OnUpdateSetAADComputerName(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(HasNoTaskRunning());
}

void FrameUsers::OnForceSyncOnPrem()
{


	auto actionCmd = std::make_shared<ForceSyncOnPremCommand>(*this);
	CommandDispatcher::GetInstance().Execute(Command::CreateCommand(actionCmd, *this, Command::ModuleTarget::User));
}

void FrameUsers::createGrid()
{
	m_AddRBACHideOutOfScope = true;

	if (GetModuleCriteria().m_IDs.empty())
		m_GridUsers.SetForceSorting(true);
	BOOL gridCreated = m_GridUsers.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
	ASSERT(gridCreated);
}

bool FrameUsers::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	GridFrameBase::customizeActionsRibbonTab(tab, true, false);

	/*{
		CXTPRibbonGroup* applyGroup = findGroup(tab, g_ActionsApplyGroup.c_str());
		ASSERT(nullptr != applyGroup);
		if (nullptr != applyGroup)
		{
			addControlTo(
				{
					ID_APPLYALLONPREM,
					IDB_APPLY,
					IDB_APPLY_16X16,
					_YTEXT("Save All On-Premises"),
					_YTEXT("Save All On-Premises"),
					_YTEXT("Save your On-Premises changes.\nAny pending changes such as edited or deleted items will be indicated in the status column.\nTip: Grouping by the status column is a quick way to check for all pending changes.\nNote: The Undo button will NOT undo changes that have already been saved.")
				}, *applyGroup, ID_APPLYALLONPREM);

			addControlTo(
				{
					ID_APPLYSELECTEDONPREM,
					IDB_APPLY_SELECTED,
					IDB_APPLY_SELECTED_16X16,
					_YTEXT("Save Selected On-Premises"),
					_YTEXT("Save Selected On-Premises"),
					_YTEXT("Save selected On-Premises changes.")
				}, *applyGroup, ID_APPLYSELECTEDONPREM);
		}
	}

	{
		CXTPRibbonGroup* revertGroup = findGroup(tab, g_ActionsRevertGroup.c_str());
		ASSERT(nullptr != revertGroup);
		if (nullptr != revertGroup)
		{
			auto ctrl = addControlTo(
				{
					ID_REVERTALLONPREM,
					IDB_REVERT_ALL,
					IDB_REVERT_ALL_16X16,
					_YTEXT("Undo All On-Premises"),
					_YTEXT("Undo All On-Premises"),
					_YTEXT("Undo all On-Premises changes currently pending in the grid.ip: Grouping by the status column is a quick way to check for all pending changes.\nNote: This button will NOT undo changes that have already been saved to the server.")
				}, *revertGroup, ID_REVERTALLONPREM);

			ctrl->SetBeginGroup(TRUE);

			addControlTo(
				{
					ID_REVERTSELECTEDONPREM,
					IDB_REVERT_SELECTED,
					IDB_REVERT_SELECTED_16X16,
					_YTEXT("Selected On-Premises"),
					_YTEXT("Selected On-Premises"),
					_YTEXT("Undo any selected On-Premises changes currently pending in the grid.\nTip: Grouping by the status column is a quick way to check for all pending changes.\nNote: This button will NOT undo changes that have already been saved to the server.")
				}, *revertGroup, ID_REVERTSELECTEDONPREM);
		}
	}*/

	{
		CXTPRibbonGroup* dataGroup = findGroup(tab, g_ActionsDataGroup.c_str());
		ASSERT(nullptr != dataGroup);
		if (nullptr != dataGroup)
		{
			if (GetOptions() && !GetOptions()->m_CustomFilter.IsEmpty())
			{
				auto ctrl = dataGroup->Add(xtpControlButton, ID_USERGRID_CHANGEPREFILTER);
				setImage({ ID_USERGRID_CHANGEPREFILTER }, IDB_CHANGEMODULEOPTIONS, xtpImageNormal);
				setImage({ ID_USERGRID_CHANGEPREFILTER }, IDB_CHANGEMODULEOPTIONS_16x16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			if (!GetOptions() || GetOptions()->m_CustomFilter.IsEmpty())
			{
				auto ctrl = dataGroup->Add(xtpControlSplitButtonPopup, ID_USERGRID_REFRESH_ONPREM);
				GridFrameBase::setImage({ ID_USERGRID_REFRESH_ONPREM }, IDB_USERGRID_REFRESH_ONPREM, xtpImageNormal);
				GridFrameBase::setImage({ ID_USERGRID_REFRESH_ONPREM }, IDB_USERGRID_REFRESH_ONPREM_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);

				auto popup = dynamic_cast<CXTPControlPopup*>(ctrl);
				ASSERT(nullptr != popup);
				if (nullptr != popup)
				{
					{
						auto ctrl = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_USERGRID_REFRESH_ONPREM);
					}

					{
						auto forceSynCtrl = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_FORCESYNCONPREM);

						setControlTexts(
							forceSynCtrl,
							_T("Sync AD DS/Azure AD"),
							_T("Sync AD DS/Azure AD"),
							_T("Sync AD DS/Azure AD"));
					}
				}
			}
			{
				auto ctrl = dataGroup->Add(xtpControlButton, ID_USERGRID_LOADMORE);
				GridFrameBase::setImage({ ID_USERGRID_LOADMORE }, IDB_LOADMORE, xtpImageNormal);
				GridFrameBase::setImage({ ID_USERGRID_LOADMORE }, IDB_LOADMORE_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			{
				auto ctrl = dataGroup->Add(xtpControlButton, ID_USERGRID_GETMAILBOXINFO);
				GridFrameBase::setImage({ ID_USERGRID_GETMAILBOXINFO }, IDB_LOADMAILBOXINFO, xtpImageNormal);
				GridFrameBase::setImage({ ID_USERGRID_GETMAILBOXINFO }, IDB_LOADMAILBOXINFO_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}
		}
	}

	CXTPRibbonGroup* editGroup = findGroup(tab, g_ActionsEditGroup.c_str());
	ASSERT(nullptr != editGroup);
	if (nullptr != editGroup)
	{
		{
			auto editCtrl = editGroup->FindControl(ID_MODULEGRID_EDIT);
			const auto pos = editGroup->IndexOf(editCtrl);
			editGroup->Remove(editCtrl);
			editCtrl = editGroup->Add(xtpControlSplitButtonPopup, ID_MODULEGRID_EDIT, nullptr, pos);
			setGridControlTooltip(editCtrl);

			auto popup = dynamic_cast<CXTPControlPopup*>(editCtrl);
			ASSERT(nullptr != popup);
			if (nullptr != popup)
			{
				{
					auto editOCtrl = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_USERGRID_EDITO365);

					setControlTexts(
						editOCtrl,
						_T("Edit O365"),
						_T("Edit O365"),
						_T("Edit O365"));
				}
				{
					auto editOPCtrl = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_USERGRID_EDITONPREM);

					setControlTexts(
						editOPCtrl,
						_T("Edit On-Premises"),
						_T("Edit On-Premises"),
						_T("Edit On-Premises"));
				}
			}
		}

		/*{
			auto ctrl = editGroup->Add(xtpControlButton, ID_USERGRID_EDITONPREM, nullptr, GetGrid().HasView() ? 2 : 1);
			GridFrameBase::setImage({ ID_USERGRID_EDITONPREM }, IDB_USERS_EDITONPREM, xtpImageNormal);
			GridFrameBase::setImage({ ID_USERGRID_EDITONPREM }, IDB_USERS_EDITONPREM_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}*/

		{
			auto ctrl = editGroup->Add(xtpControlButton, ID_USERGRID_SETMANAGER);
			GridFrameBase::setImage({ ID_USERGRID_SETMANAGER }, IDB_USERS_SETMANAGER, xtpImageNormal);
			GridFrameBase::setImage({ ID_USERGRID_SETMANAGER }, IDB_USERS_SETMANAGER_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = editGroup->Add(xtpControlButton, ID_USERGRID_REMOVEMANAGER);
			GridFrameBase::setImage({ ID_USERGRID_REMOVEMANAGER }, IDB_USERS_REMOVEMANAGER, xtpImageNormal);
			GridFrameBase::setImage({ ID_USERGRID_REMOVEMANAGER }, IDB_USERS_REMOVEMANAGER_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = editGroup->Add(xtpControlButton, ID_USERGRID_RESETPASSWORD);
			GridFrameBase::setImage({ ID_USERGRID_RESETPASSWORD }, IDB_USERS_RESET_PASSWORD, xtpImageNormal);
			GridFrameBase::setImage({ ID_USERGRID_RESETPASSWORD }, IDB_USERS_RESET_PASSWORD_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = editGroup->Add(xtpControlButton, ID_USERGRID_REVOKESESSIONS);
			GridFrameBase::setImage({ ID_USERGRID_REVOKESESSIONS }, IDB_USERS_REVOKEACCESS, xtpImageNormal);
			GridFrameBase::setImage({ ID_USERGRID_REVOKESESSIONS }, IDB_USERS_REVOKEACCESS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = editGroup->Add(xtpControlButton, ID_USERGRID_BATCHIMPORT);
			GridFrameBase::setImage({ ID_USERGRID_BATCHIMPORT }, IDB_BATCHIMPORT, xtpImageNormal);
			GridFrameBase::setImage({ ID_USERGRID_BATCHIMPORT }, IDB_BATCHIMPORT_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = editGroup->Add(xtpControlButton, ID_USERGRID_BATCHUPDATE);
			GridFrameBase::setImage({ ID_USERGRID_BATCHUPDATE }, IDB_BATCHUPDATE, xtpImageNormal);
			GridFrameBase::setImage({ ID_USERGRID_BATCHUPDATE }, IDB_BATCHUPDATE_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	{
		CXTPRibbonGroup* linkGroup = findGroup(tab, g_ActionsLinkUser.c_str());
		ASSERT(nullptr != linkGroup);
		if (nullptr != linkGroup)
			addNewModuleCommandControl(*linkGroup, ID_USERGRID_SHOW_RECYCLEBIN, { ID_USERGRID_SHOW_RECYCLEBIN_FRAME, ID_USERGRID_SHOW_RECYCLEBIN_NEWFRAME }, { IDB_RECYCLEBIN , IDB_RECYCLEBIN_16X16 });
	}

	automationAddCommandIDToGreenLight(ID_USERGRID_RESETPASSWORD);
	automationAddCommandIDToGreenLight(ID_USERGRID_SETMANAGER);
	automationAddCommandIDToGreenLight(ID_USERGRID_REMOVEMANAGER);
	automationAddCommandIDToGreenLight(ID_USERGRID_REVOKESESSIONS);
	automationAddCommandIDToGreenLight(ID_USERGRID_CHANGEPREFILTER);

	return true;
}

void FrameUsers::ShowUsers(const vector<BusinessUser>& p_Users, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Update, bool p_AllowFullPurge)
{
    m_GridUsers.BuildView(p_Users, {}, p_UpdateOperations, p_Update, p_AllowFullPurge);
}

void FrameUsers::ShowUsers(const vector<BusinessUser>& p_Users, const vector<BusinessUser>& p_LoadedMoreUsers, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Update, bool p_AllowFullPurge)
{
	m_GridUsers.BuildView(p_Users, p_LoadedMoreUsers, p_UpdateOperations, p_Update, p_AllowFullPurge);
}

void FrameUsers::ShowOnPremiseUsers(const vector<OnPremiseUser>& p_Users)
{
	m_GridUsers.ShowOnPremiseUsers(p_Users);
}

void FrameUsers::UpdateUsersLoadMore(const vector<BusinessUser>& p_Users, bool p_FromRefresh)
{
    m_GridUsers.UpdateUsersLoadMore(p_Users, p_FromRefresh, false);
}

void FrameUsers::UpdateUsersLoadMailbox(const vector<BusinessUser>& p_Users, bool p_FromRefresh)
{
	m_GridUsers.UpdateUsersLoadMailbox(p_Users, p_FromRefresh, false);
}

void FrameUsers::SetOnPremiseUsersUpdateResult(const vector<O365UpdateOperation>& p_UpdateResults)
{
	m_GridUsers.SetOnPremiseUsersUpdateResult(p_UpdateResults);
}

O365Grid& FrameUsers::GetGrid()
{
	return m_GridUsers;
}

void FrameUsers::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameUsers::ApplySpecific(bool p_Selected)
{
    UpdatedObjectsGenerator<BusinessUser> updatedUsersGenerator;
    updatedUsersGenerator.BuildUpdatedObjects(GetGrid(), p_Selected);

	// FIXME: Really really bad code...
	std::vector<RevokeAccessFieldUpdate*> revokeAccessFieldUpdates;
	for (auto& mod : GetGrid().GetModifications().GetRowFieldModifications())
	{
		for (auto& fu : mod->GetFieldUpdates())
		{
			auto rafu = dynamic_cast<RevokeAccessFieldUpdate*>(fu.get());
			if (nullptr != rafu)
				revokeAccessFieldUpdates.push_back(rafu);
		}
	}

	auto getUserId = [colId = GetGrid().GetColId()->GetID()](auto& rafu)
	{
		PooledString id;

		for (auto& f : rafu->GetRowKey())
		{
			if (f.GetColID() == colId)
			{
				id = f.GetValueStr();
				break;
			}
		}

		return id;
	};

	if (!revokeAccessFieldUpdates.empty())
	{
		for (auto& user : updatedUsersGenerator.GetObjectsNoConst())
		{
			auto it = std::find_if(revokeAccessFieldUpdates.begin(), revokeAccessFieldUpdates.end(), [&user, getUserId](const auto& rafu)
				{
					return user.GetID() == getUserId(rafu);
				});
			if (revokeAccessFieldUpdates.end() != it)
			{
				user.SetShouldRevokeAccess(true);
			}
		}
	}

	/////

    CommandInfo info;
    info.Data() = updatedUsersGenerator;
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::User, Command::ModuleTask::UpdateModified, info, { &GetGrid(), p_Selected ? AutomationConstant::val().m_ActionNameSaveSelected : AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
}

void FrameUsers::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameUsers::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	FlatObjectListFrameRefresher()(*this, Command::ModuleTarget::User, Origin::User, p_CurrentAction, [this](const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Update, bool p_ForceSortingIfEmptyGrid)
	{
		// Only reached when update operations contain only successful deletion (== no request necessary)
		this->ShowUsers({}, p_UpdateOperations, p_Update, false);
	});
}

AutomatedApp::AUTOMATIONSTATUS FrameUsers::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionSelectedUserLoadMore(p_Action))
	{
		p_CommandID = ID_USERGRID_LOADMORE;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedUserLoadMailboxInfo(p_Action))
	{
		p_CommandID = ID_USERGRID_GETMAILBOXINFO;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedEditUser(p_Action))
		p_CommandID = ID_MODULEGRID_EDIT;
	else if (IsActionSelectedResetPassword(p_Action))
		p_CommandID = ID_USERGRID_RESETPASSWORD;
	else if (IsActionCreateUser(p_Action))
		p_CommandID = ID_MODULEGRID_CREATE;
	else if (IsActionSelectedRevokeUserAccess(p_Action))
		p_CommandID = ID_USERGRID_REVOKESESSIONS;

	return statusRV;
}

GridFrameBase::ApplyConfirmationConfig FrameUsers::GetApplyConfirmationConfig(bool p_ApplySelected) const
{
	if (m_GridUsers.HasPasswordModifications(p_ApplySelected))
	{
		ApplyConfirmationConfig applyConfirmationConfig;
		applyConfirmationConfig.m_AdditionalWarning = YtriaTranslate::Do(FrameUsers_getApplyConfirmationConfig_1, _YLOC("There are temporary passwords in the grid, be sure to copy-paste them before applying as you won't be able to see them again later.")).c_str();
		applyConfirmationConfig.m_Icon = DlgMessageBox::eIcon_ExclamationWarning;
		return applyConfirmationConfig;
	}

	return GridFrameBase::GetApplyConfirmationConfig(p_ApplySelected);
}

bool FrameUsers::hasLoseableModificationsPending(bool inSelectionOnly /*= false*/)
{
	return hasModificationsPending(inSelectionOnly) || hasOnPremiseChanges(inSelectionOnly);
}

bool FrameUsers::hasOnPremiseChanges(bool inSelectionOnly /*= false*/)
{
	return m_GridUsers.HasOnPremiseChanges(inSelectionOnly);
}

void FrameUsers::ShowRecycleBin()
{
	CommandInfo info;
	info.SetOrigin(GetModuleCriteria().m_Origin);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(Util::ForSubModule(GetLicenseContext()), GetSafeHwnd(), Command::ModuleTarget::User, Command::ModuleTask::ListRecycleBin, info, { &GetGrid(), _YTEXT("RecycleBin"), GetAutomationActionRecording(), GetAutomationAction() }));
}

bool FrameUsers::HasDeltaFeature() const
{
	return GetSessionInfo().UseDeltaUsers()/* && GetModuleCriteria().m_IDs.empty()*/;
}
