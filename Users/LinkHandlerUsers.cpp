#include "LinkHandlerUsers.h"

#include "Command.h"
#include "CommandDispatcher.h"

void LinkHandlerUsers::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);
    
	CommandInfo info;
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::MainModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::User, Command::ModuleTask::List, info, { nullptr, g_ActionNameShowUsers, nullptr, p_Link.GetAutomationAction() }));
}

// =================================================================================================

void LinkHandlerPrefilteredUsers::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	// New automation name?
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::MainModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::User, Command::ModuleTask::ListWithPrefilter, info, { nullptr, g_ActionNameShowUsers, nullptr, p_Link.GetAutomationAction() }));
}

// =================================================================================================

void LinkHandlerUsersRecycleBin::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	info.SetOrigin(Origin::Tenant);
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::User, Command::ModuleTask::ListRecycleBin, info, { nullptr, g_ActionNameShowUsersRecycleBin, nullptr, p_Link.GetAutomationAction()}));
}

// =================================================================================================

void LinkHandlerGroupMemberships::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	info.SetOrigin(Origin::User);
	p_Link.ENABLE_FREE_ACCESS();// free access
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::User, Command::ModuleTask::ListMyOwners, info, { nullptr, g_ActionNameMyDataContacts, nullptr, p_Link.GetAutomationAction() }));
}

bool LinkHandlerGroupMemberships::IsAllowedWithAJLLicense() const
{
	return true; // only MyData is allowed
}