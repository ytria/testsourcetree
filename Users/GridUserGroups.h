#pragma once

#include "BusinessUser.h"
#include "BaseO365Grid.h"
#include "GridGroupsCommon.h"
#include "GridTemplateGroups.h"
#include "GridTemplateUsers.h"
#include "MyDataMeRowHandler.h"

class FrameUserGroups;
class GridUpdater;

class GridUserGroups : public O365Grid
{
public:
	GridUserGroups(bool p_IsMyData = false);
	virtual ~GridUserGroups() = default;

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;
	void AddUserWithParentGroups(vector<BusinessUser> p_Users, const bool p_IsRefresh, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge);

	GridGroupsCommon::GroupChanges GetGroupChanges(bool p_Selected, bool p_TopLevel);
    GridBackendColumn* GetColUserId();

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	virtual bool HasModificationsPending(bool inSelectionOnly = false) override;
	virtual bool HasRevertableModificationsPending(bool inSelectionOnly = false) override;

	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

	bool IsMyData() const override;

	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

protected:
	virtual void customizeGrid() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
	virtual void InitializeCommands() override;

	afx_msg void OnAddUsersToGroups();
	afx_msg void OnUpdateAddUsersToGroups(CCmdUI* pCmdUI);
	afx_msg void OnDeleteUsersFromGroups();
	afx_msg void OnUpdateDeleteUsersFromGroups(CCmdUI* pCmdUI);

	afx_msg void OnCopySelectionMembershipTo();
	afx_msg void OnTransferSelectionMembership();
	afx_msg void OnUpdateCopyTransferSelectionMembership(CCmdUI* pCmdUI);

	DECLARE_MESSAGE_MAP()

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

private:
    void RemoveNonModifiedUsers(vector<BusinessUser>& p_Users, const vector<O365UpdateOperation>& p_Updates);
	void getGroups(vector<BusinessGroup>& p_Groups) const;
	void getUsers(vector<BusinessUser>& p_Users) const;

	void deleteUsersFromGroups(const set<GridBackendRow*>& p_GroupRows, bool p_Update, bool p_DoNotUnSelectAll);

	void copySelectedMembershipTo(bool removeSourceMembership);
	void addMembersToGroupsImpl(const vector<SelectedItem>& p_NewMembers, const std::set<GridBackendRow*>& p_TargetGroupRows, bool removeFromSourceGroup, bool shouldScrollToFirstAddedRow);

	GridBackendColumn* m_ColumnDirectMember;
	GridBackendColumn* m_ColumnHasAssignedLicences;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesSkuPartNumber;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesSkuName;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesSkuId;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesState;
	GridBackendColumn* m_ColumnLicenseAssignmentStatesError;
	GridTemplateUsers	m_TemplateUsers;
	GridTemplateGroups	m_TemplateGroups;

	vector<GridBackendColumn*>	m_MetaColumns;

	bool m_HasLoadMoreAdditionalInfo;
	O365IdsContainer m_MetaIDsMoreLoaded;

	FrameUserGroups*	m_Frame;

	bool m_IsMyData;

	int m_DirectMemberIconId;
	int m_IndirectMemberIconId;
	static wstring g_DirectMemberText;
	static wstring g_IndirectMemberText;

	struct PendingAddGroups
	{
		vector<SelectedItem> m_Users;
		std::set<GridBackendRow*> m_GroupsToAddTo;
		bool m_ShouldScroll = false;
		bool m_ShouldRemoveFromSource = false;
	};
	boost::YOpt<PendingAddGroups> m_PendingAddGroups;

	boost::YOpt<MyDataMeRowHandler> m_MyDataMeHandler;
};