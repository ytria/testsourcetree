#include "GridUsersRecycleBin.h"

#include "AutomationWizardUserRecycleBin.h"
#include "BasicGridSetup.h"
#include "BusinessUserConfiguration.h"
#include "FrameUsersRecycleBin.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"
#include "RestoreObjectModification.h"

BEGIN_MESSAGE_MAP(GridUsersRecycleBin, O365Grid)
	ON_COMMAND(ID_USERSRECYCLEBIN_RESTORE,					OnRestore)
	ON_UPDATE_COMMAND_UI(ID_USERSRECYCLEBIN_RESTORE,		OnUpdateRestore)
	ON_COMMAND(ID_USERSRECYCLEBIN_HARDDELETE,				OnHardDelete)
	ON_UPDATE_COMMAND_UI(ID_USERSRECYCLEBIN_HARDDELETE,		OnUpdateHardDelete)
END_MESSAGE_MAP()

GridUsersRecycleBin::GridUsersRecycleBin()
	: m_TemplateUsers(false)
{
	m_MayContainUnscopedUserGroupOrSite = true;

	EnableGridModifications();

	initWizard<AutomationWizardUserRecycleBin>(_YTEXT("Automation\\UserRecycleBin"));
}

GridUsersRecycleBin::~GridUsersRecycleBin()
{}

void GridUsersRecycleBin::BuildView(vector<BusinessUser> p_Users, const vector<O365UpdateOperation>& p_UpdateOperations)
{
	if (p_Users.size() == 1 && p_Users[0].GetError())
	{
		HandlePostUpdateError(p_Users[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_Users[0].GetError()->GetFullErrorMessage());
	}
	else
	{
		const uint32_t options	= GridUpdaterOptions::UPDATE_GRID
								| (p_UpdateOperations.empty() ? GridUpdaterOptions::FULLPURGE : 0)
								| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
								| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
								| GridUpdaterOptions::REFRESH_PROCESS_DATA
								/*| GridUpdaterOptions::PROCESS_LOADMORE*/;
		GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateOperations);
		GridIgnoreModification ignoramus(*this);

		// Update only groups on which we applied modifications (in case we did apply some)
        if (!p_UpdateOperations.empty())
            RemoveUsersNotModified(p_Users, p_UpdateOperations);

        for (const auto& bu : p_Users)
        {
            auto row = m_TemplateUsers.AddRow(*this, bu, vector<GridBackendField>(), updater, false, /*FIXME!!!*/false);
			ASSERT(nullptr != row);
			if (nullptr != row)
			{
				AddRoleDelegationFlag(row, bu);
				updater.GetOptions().AddRowWithRefreshedValues(row);
			}
        }
	}
}

void GridUsersRecycleBin::OnRestore()
{
	if (showUserRestrictedAccessDialog(USER_ADMIN_REQUIREMENT))
	{
		TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool p_HasRowsToReExplode)
			{
				bool update = false;
				for (auto selectedRow : GetSelectedRows())
				{
					ASSERT(nullptr != selectedRow);
					if (nullptr != selectedRow && !selectedRow->IsGroupRow() && IsAuthorizedByRoleDelegation(selectedRow, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_RECYCLEBIN_MANAGE))
					{
						row_pk_t rowPk;
						GetRowPK(selectedRow, rowPk);

						ASSERT(!selectedRow->IsCreated());
						GetModifications().Add(std::make_unique<RestoreObjectModification>(*this, rowPk));
						update = true;
					}
				}

				if (update & !p_HasRowsToReExplode)
					UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
			})();
	}
}

void GridUsersRecycleBin::OnUpdateRestore(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && HasOneNonGroupRowSelectedAtLeast() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_RECYCLEBIN_MANAGE));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsersRecycleBin::OnHardDelete()
{
	if (showUserRestrictedAccessDialog(USER_ADMIN_REQUIREMENT))
	{
		TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool p_HasRowsToReExplode)
			{
				bool update = false;
				for (auto selectedRow : GetSelectedRows())
				{
					ASSERT(nullptr != selectedRow);
					if (nullptr != selectedRow && !selectedRow->IsGroupRow() && IsAuthorizedByRoleDelegation(selectedRow, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_RECYCLEBIN_MANAGE))
					{
						row_pk_t rowPk;
						GetRowPK(selectedRow, rowPk);
						ASSERT(!selectedRow->IsCreated());
						GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, rowPk));
						update = true;
					}
				}

				if (update && !p_HasRowsToReExplode)
					UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
			})();
	}
}

void GridUsersRecycleBin::OnUpdateHardDelete(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && HasOneNonGroupRowSelectedAtLeast() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_USER_RECYCLEBIN_MANAGE));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridUsersRecycleBin::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart /*= 0*/)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		if (GridUtil::IsSelectionAtLeastOneOfType<BusinessUser>(*this))
		{
			pPopup->ItemInsert(ID_USERSRECYCLEBIN_RESTORE);
			pPopup->ItemInsert(ID_USERSRECYCLEBIN_HARDDELETE);
			pPopup->ItemInsert(); // Sep
		}

		/*// Bypass O365Grid as we don't want default user actions.
		CacheGrid::OnCustomPopupMenu(pPopup, nStart);*/
		O365Grid::OnCustomPopupMenu(pPopup, nStart);
	}
}

void GridUsersRecycleBin::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameUsersRecycleBin, LicenseUtil::g_codeSapio365users);
		setup.Setup(*this, false);
	}

	const auto& ucfg = BusinessUserConfiguration::GetInstance();

	m_TemplateUsers.m_ColumnDisplayName				= AddColumn(_YUID(O365_USER_DISPLAYNAME), ucfg.GetTitle(_YUID(O365_USER_DISPLAYNAME)),											g_FamilyInfo,		g_ColumnSize22, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnUserPrincipalName		= AddColumn(_YUID(O365_USER_USERPRINCIPALNAME), ucfg.GetTitle(_YUID(O365_USER_USERPRINCIPALNAME)),								g_FamilyInfo,		g_ColumnSize32, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnUserType				= AddColumn(_YUID(O365_USER_USERTYPE), ucfg.GetTitle(_YUID(O365_USER_USERTYPE)),												g_FamilyInfo,		g_ColumnSize12, { g_ColumnsPresetDefault });

	m_TemplateUsers.m_ColumnSkuPartNumbers			= AddColumn(_YUID(O365_USER_ASSIGNEDLICENSES), ucfg.GetTitle(_YUID(O365_USER_ASSIGNEDLICENSES)),								g_FamilyInfo,		g_ColumnSize32, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnSkuIds					= AddColumnCaseSensitive(_YUID("assignedLicenses.skuId"), YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_2, _YLOC("Assigned Licenses Sku ID")).c_str(),																	g_FamilyInfo,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_TemplateUsers.m_ColumnSkuPartNumbers->SetMultivalueFamily(YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_2, _YLOC("Assigned Licenses")).c_str());
	m_TemplateUsers.m_ColumnSkuPartNumbers->AddMultiValueExplosionSister(m_TemplateUsers.m_ColumnSkuIds);

	m_TemplateUsers.m_ColumnDeletedDateTime			= AddColumnDate(_YUID(O365_USER_DELETEDDATETIME), ucfg.GetTitle(_YUID(O365_USER_DELETEDDATETIME)),								g_FamilyInfo,		g_ColumnSize16, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnAccountEnabled			= AddColumnIcon(_YUID(O365_USER_ACCOUNTENABLED), ucfg.GetTitle(_YUID(O365_USER_ACCOUNTENABLED)), g_FamilyInfo);
	m_TemplateUsers.m_ColumnAccountEnabled->SetPresetFlags({ g_ColumnsPresetDefault });
	
	static const wstring g_FamilyMail = YtriaTranslate::Do(GridUsers_customizeGrid_6, _YLOC("Mail")).c_str();
	m_TemplateUsers.m_ColumnMail					= AddColumn(_YUID(O365_USER_MAIL), ucfg.GetTitle(_YUID(O365_USER_MAIL)),														g_FamilyMail,		g_ColumnSize32, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnMailNickname			= AddColumn(_YUID(O365_USER_MAILNICKNAME), ucfg.GetTitle(_YUID(O365_USER_MAILNICKNAME)),										g_FamilyMail,		g_ColumnSize22);
	m_TemplateUsers.m_ColumnProxyAddresses			= AddColumn(_YUID(O365_USER_PROXYADDRESSES), ucfg.GetTitle(_YUID(O365_USER_PROXYADDRESSES)),									g_FamilyMail,		g_ColumnSize22);
	m_TemplateUsers.m_ColumnImAddresses				= AddColumn(_YUID(O365_USER_IMADDRESSES), ucfg.GetTitle(_YUID(O365_USER_IMADDRESSES)),											g_FamilyMail,		g_ColumnSize22);
	m_TemplateUsers.m_ColumnGivenName				= AddColumn(_YUID(O365_USER_GIVENNAME), ucfg.GetTitle(_YUID(O365_USER_GIVENNAME)),												g_FamilyInfo,		g_ColumnSize22);
	m_TemplateUsers.m_ColumnSurname					= AddColumn(_YUID(O365_USER_SURNAME), ucfg.GetTitle(_YUID(O365_USER_SURNAME)),													g_FamilyInfo,		g_ColumnSize22);
	
	static const wstring g_FamilyJobInfo = YtriaTranslate::Do(GridUsers_customizeGrid_13, _YLOC("Job Info")).c_str();
	m_TemplateUsers.m_ColumnJobTitle				= AddColumn(_YUID(O365_USER_JOBTITLE), ucfg.GetTitle(_YUID(O365_USER_JOBTITLE)),												g_FamilyJobInfo,	g_ColumnSize12, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnCompanyName				= AddColumn(_YUID(O365_USER_COMPANYNAME), ucfg.GetTitle(_YUID(O365_USER_COMPANYNAME)),											g_FamilyJobInfo,	g_ColumnSize22);
	m_TemplateUsers.m_ColumnDepartment				= AddColumn(_YUID(O365_USER_DEPARTMENT), ucfg.GetTitle(_YUID(O365_USER_DEPARTMENT)),											g_FamilyJobInfo,	g_ColumnSize12, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnBusinessPhones			= AddColumn(_YUID(O365_USER_BUSINESSPHONES), ucfg.GetTitle(_YUID(O365_USER_BUSINESSPHONES)),									g_FamilyJobInfo,	g_ColumnSize12, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnMobilePhone				= AddColumn(_YUID(O365_USER_MOBILEPHONE), ucfg.GetTitle(_YUID(O365_USER_MOBILEPHONE)),											g_FamilyJobInfo,	g_ColumnSize12, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnOfficeLocation			= AddColumn(_YUID(O365_USER_OFFICELOCATION), ucfg.GetTitle(_YUID(O365_USER_OFFICELOCATION)),									g_FamilyJobInfo,	g_ColumnSize12, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnStreetAddress			= AddColumn(_YUID(O365_USER_STREETADDRESS), ucfg.GetTitle(_YUID(O365_USER_STREETADDRESS)),										g_FamilyJobInfo,	g_ColumnSize22);
	m_TemplateUsers.m_ColumnCity					= AddColumn(_YUID(O365_USER_CITY), ucfg.GetTitle(_YUID(O365_USER_CITY)),														g_FamilyJobInfo,	g_ColumnSize12);
	m_TemplateUsers.m_ColumnState					= AddColumn(_YUID(O365_USER_STATE), ucfg.GetTitle(_YUID(O365_USER_STATE)),														g_FamilyJobInfo,	g_ColumnSize12);
	m_TemplateUsers.m_ColumnPostalCode				= AddColumn(_YUID(O365_USER_POSTALCODE), ucfg.GetTitle(_YUID(O365_USER_POSTALCODE)),											g_FamilyJobInfo,	g_ColumnSize12);
	m_TemplateUsers.m_ColumnCountry					= AddColumn(_YUID(O365_USER_COUNTRY), ucfg.GetTitle(_YUID(O365_USER_COUNTRY)),													g_FamilyJobInfo,	g_ColumnSize12);
	m_TemplateUsers.m_ColumnPreferredLanguage		= AddColumn(_YUID(O365_USER_PREFERREDLANGUAGE), ucfg.GetTitle(_YUID(O365_USER_PREFERREDLANGUAGE)),								g_FamilyInfo,		g_ColumnSize12);
	m_TemplateUsers.m_ColumnUsageLocation			= AddColumn(_YUID(O365_USER_USAGELOCATION), ucfg.GetTitle(_YUID(O365_USER_USAGELOCATION)),										g_FamilyInfo,		g_ColumnSize12);
	
	static const wstring g_FamilyPassword = YtriaTranslate::Do(GridUsers_customizeGrid_27, _YLOC("Password Info")).c_str();
	m_TemplateUsers.m_ColumnPasswordPolicies			= AddColumn(_YUID(O365_USER_PASSWORDPOLICIES), ucfg.GetTitle(_YUID(O365_USER_PASSWORDPOLICIES)),								g_FamilyPassword,	g_ColumnSize12);
	m_TemplateUsers.m_ColumnPassword					= AddColumn(_YUID(O365_USER_PASSWORDPROFILE_PASSWORD), ucfg.GetTitle(_YUID(O365_USER_PASSWORDPROFILE_PASSWORD)),				g_FamilyPassword,	g_ColumnSize12);
	m_TemplateUsers.m_ColumnForceChangePassword			= AddColumnCheckBox(_YUID(O365_USER_PASSWORDPROFILE_FORCECHANGE), ucfg.GetTitle(_YUID(O365_USER_PASSWORDPROFILE_FORCECHANGE)),	g_FamilyPassword,	g_ColumnSize6);
	m_TemplateUsers.m_ColumnForceChangePasswordWithMfa	= AddColumnCheckBox(_YUID(O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA), ucfg.GetTitle(_YUID(O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA)), g_FamilyPassword, g_ColumnSize6);
	
	static const wstring g_FamilyOnPremises = YtriaTranslate::Do(GridUsers_customizeGrid_42, _YLOC("On-Premises")).c_str();
	m_TemplateUsers.m_ColumnOPSyncEnabled			= AddColumnCheckBox(_YUID(O365_USER_ONPREMISESSYNCENABLED), ucfg.GetTitle(_YUID(O365_USER_ONPREMISESSYNCENABLED)),				g_FamilyOnPremises, g_ColumnSize6);
	m_TemplateUsers.m_ColumnOPImmutableId			= AddColumn(_YUID(O365_USER_ONPREMISESIMMUTABLEID), ucfg.GetTitle(_YUID(O365_USER_ONPREMISESIMMUTABLEID)),						g_FamilyOnPremises, g_ColumnSize12, { g_ColumnsPresetTech });
	m_TemplateUsers.m_ColumnOPLastSync				= AddColumnDate(_YUID(O365_USER_ONPREMISESLASTSYNCDATETIME), ucfg.GetTitle(_YUID(O365_USER_ONPREMISESLASTSYNCDATETIME)),		g_FamilyOnPremises, g_ColumnSize16);
	m_TemplateUsers.m_ColumnOPSecurityID			= AddColumn(_YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER), ucfg.GetTitle(_YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER)),		g_FamilyOnPremises, g_ColumnSize12, { g_ColumnsPresetTech });
	addColumnGraphID();
	m_TemplateUsers.m_ColumnID = GetColId();
	m_TemplateUsers.CustomizeGrid(*this);

	AddColumnForRowPK(GetColId());
	AddColumnForRowPK(m_TemplateUsers.m_ColumnUserType);
	AddSorting(m_TemplateUsers.m_ColumnDisplayName, GridBackendUtil::ASC);

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameUsersRecycleBin*>(GetParentFrame()));
}

wstring GridUsersRecycleBin::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, m_TemplateUsers.m_ColumnDisplayName);
}

void GridUsersRecycleBin::InitializeCommands()
{
	O365Grid::InitializeCommands();

	const auto profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_USERSRECYCLEBIN_RESTORE;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridUsersRecycleBin_InitializeCommands_1, _YLOC("Restore Selected")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUsersRecycleBin_InitializeCommands_2, _YLOC("Restore Selected")).c_str();
		//_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_RESTORE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_USERSRECYCLEBIN_RESTORE, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridUsersRecycleBin_InitializeCommands_3, _YLOC("Restore Selected Users")).c_str(),
			YtriaTranslate::Do(GridUsersRecycleBin_InitializeCommands_4, _YLOC("Restore Selected Users.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_USERSRECYCLEBIN_HARDDELETE;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridUsersRecycleBin_InitializeCommands_5, _YLOC("Permanently Delete")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUsersRecycleBin_InitializeCommands_6, _YLOC("Permanently Delete")).c_str();
		//_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_HARDDELETE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_USERSRECYCLEBIN_HARDDELETE, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridUsersRecycleBin_InitializeCommands_7, _YLOC("Permanently Delete Users")).c_str(),
			YtriaTranslate::Do(GridUsersRecycleBin_InitializeCommands_8, _YLOC("Permanently Delete Users.")).c_str(),
			_cmd.m_sAccelText);
	}
}

void GridUsersRecycleBin::RemoveUsersNotModified(vector<BusinessUser>& p_Users, const vector<O365UpdateOperation>& p_UpdateOperations)
{
    p_Users.erase(std::remove_if(p_Users.begin(), p_Users.end(), [this, &p_UpdateOperations](const BusinessUser& p_User) {
        vector<GridBackendField> pk;
		m_TemplateUsers.GetBusinessUserPK(*this, p_User, pk);

        for (const auto& update : p_UpdateOperations)
        {
            if (update.GetPrimaryKey() == pk)
                return false;
        }
        return true;
    }), p_Users.end());
}

const wstring& GridUsersRecycleBin::annotationGetModuleName() const
{
	return GridUtil::g_AutoNameUsers;
}

void GridUsersRecycleBin::annotationModuleSetDefault(GridAnnotation& p_NewAnnotation) const
{
	p_NewAnnotation.SetRefDeleted(true);
}