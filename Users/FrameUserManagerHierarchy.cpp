#include "FrameUserManagerHierarchy.h"

#include "CommandDispatcher.h"

#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
IMPLEMENT_DYNAMIC(FrameUserManagerHierarchy, GridFrameBase)

void FrameUserManagerHierarchy::createGrid()
{
	BOOL gridCreated = m_GridUserManagerHierarchy.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
	ASSERT(gridCreated);
}

bool FrameUserManagerHierarchy::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	// Remove "Group Info" ribbon tab: p_ShowLinkTabGroup = false.
	return GridFrameBase::customizeActionsRibbonTab(tab, true, false);
}

FrameUserManagerHierarchy::FrameUserManagerHierarchy(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
	//m_CreateRefreshPartial = true;
	//m_CreateAddRemoveToSelection = true;
	m_CreateShowContext = true;
}

void FrameUserManagerHierarchy::ShowUserManagers(vector<BusinessUser>& p_Users, bool p_FullPurge)
{
	if (CompliesWithDataLoadPolicy())
		m_GridUserManagerHierarchy.AddUserWithManager(p_Users, true, p_FullPurge);
}

O365Grid& FrameUserManagerHierarchy::GetGrid()
{
	return m_GridUserManagerHierarchy;
}

void FrameUserManagerHierarchy::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.SetOrigin(GetModuleCriteria().m_Origin);
	ASSERT(ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer);
	if (ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer)
		info.GetIds() = GetModuleCriteria().m_IDs;
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::User, Command::ModuleTask::ListManagerHierarchy, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameUserManagerHierarchy::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::User, Command::ModuleTask::ListManagerHierarchy, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

AutomatedApp::AUTOMATIONSTATUS FrameUserManagerHierarchy::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	return statusRV;
}
#endif