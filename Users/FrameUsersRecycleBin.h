#pragma once

#include "GridFrameBase.h"
#include "GridUsersRecycleBin.h"

class FrameUsersRecycleBin : public GridFrameBase
{
public:
	DECLARE_DYNAMIC(FrameUsersRecycleBin)
	using GridFrameBase::GridFrameBase;
	virtual ~FrameUsersRecycleBin() = default;

	void ShowUsersRecycleBin(const vector<BusinessUser>& p_Users);
    void ShowUsersRecycleBin(const vector<BusinessUser>& p_Users, const vector<O365UpdateOperation>& p_UpdateOperations);

	virtual O365Grid& GetGrid() override;
	virtual void ApplyAllSpecific();
    virtual void ApplySelectedSpecific();

    void ApplySpecific(bool p_Selected);

    virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;

	virtual bool HasApplyButton() override;

protected:
	DECLARE_MESSAGE_MAP()

	virtual void createGrid() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	GridUsersRecycleBin m_GridUsersRC;
};
