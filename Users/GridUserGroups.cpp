#include "GridUserGroups.h"

#include "AutomationWizardUserGroups.h"
#include "BasicGridSetup.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessUser.h"
#include "BusinessUserGuest.h"
#include "DlgAddItemsToGroups.h"
#include "FrameUserGroups.h"
#include "GraphCache.h"
#include "GridUpdater.h"
#include "../Resource.h"
#include "SQL + Cloud/Role Delegation/RoleDelegationUtil.h"
#include "SkuAndPlanReference.h"

void GridUserGroups::RemoveNonModifiedUsers(vector<BusinessUser>& p_Users, const vector<O365UpdateOperation>& p_Updates)
{
    p_Users.erase(std::remove_if(p_Users.begin(), p_Users.end(), [this, &p_Updates](const BusinessUser& p_User) {
        vector<GridBackendField> pk;
		m_TemplateUsers.GetBusinessUserPK(*this, p_User, pk);

        for (const auto& update : p_Updates)
        {
            if (update.GetPrimaryKey() == pk)
                return false;
        }
        return true;
    }), p_Users.end());
}

wstring GridUserGroups::g_DirectMemberText;
wstring GridUserGroups::g_IndirectMemberText;

BEGIN_MESSAGE_MAP(GridUserGroups, O365Grid)
	ON_COMMAND(ID_USERGROUPSGRID_ADDTOGROUP,				OnAddUsersToGroups)
	ON_UPDATE_COMMAND_UI(ID_USERGROUPSGRID_ADDTOGROUP,		OnUpdateAddUsersToGroups)
	ON_COMMAND(ID_USERGROUPSGRID_REMOVEFROMGROUP,			OnDeleteUsersFromGroups)
	ON_UPDATE_COMMAND_UI(ID_USERGROUPSGRID_REMOVEFROMGROUP,	OnUpdateDeleteUsersFromGroups)
	ON_COMMAND(ID_USERGROUPSGRID_COPYTO,					OnCopySelectionMembershipTo)
	ON_UPDATE_COMMAND_UI(ID_USERGROUPSGRID_COPYTO,			OnUpdateCopyTransferSelectionMembership)
	ON_COMMAND(ID_USERGROUPSGRID_TRANSFERTO,				OnTransferSelectionMembership)
	ON_UPDATE_COMMAND_UI(ID_USERGROUPSGRID_TRANSFERTO,		OnUpdateCopyTransferSelectionMembership)
END_MESSAGE_MAP()

GridUserGroups::GridUserGroups(bool p_IsMyData/* = false*/)
	: m_Frame(nullptr)
	, m_ColumnDirectMember(nullptr)
	, m_HasLoadMoreAdditionalInfo(false)
	, m_ColumnHasAssignedLicences(nullptr)
	, m_ColumnLicenseAssignmentStatesSkuPartNumber(nullptr)
	, m_ColumnLicenseAssignmentStatesSkuName(nullptr)
	, m_ColumnLicenseAssignmentStatesSkuId(nullptr)
	, m_ColumnLicenseAssignmentStatesState(nullptr)
	, m_ColumnLicenseAssignmentStatesError(nullptr)
	, m_IsMyData(p_IsMyData)
{
	m_MayContainUnscopedUserGroupOrSite = true;

	EnableGridModifications();

	initWizard<AutomationWizardUserGroups>(_YTEXT("Automation\\UserGroups"));

	if (IsMyData())
		m_MyDataMeHandler.emplace();
}

void GridUserGroups::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameUserGroups, LicenseUtil::g_codeSapio365userGroups,
		{
			{ nullptr, _YTEXT(""), _YTEXT("") },
			{ nullptr, _YTEXT(""), _YTEXT("") },
			{ &m_TemplateGroups.m_ColumnMetaID, YtriaTranslate::Do(GridUserGroups_customizeGrid_1, _YLOC("User ID")).c_str(), GridTemplate::g_TypeUser }
		});
		setup.Setup(*this, true);
		m_TemplateUsers.m_ColumnLastRequestDateTime = setup.GetColumnLastRequestDateTime();
	}

	m_TemplateUsers.m_ColumnDisplayName			= AddColumn(_YUID(O365_USER_DISPLAYNAME),			YtriaTranslate::Do(GridUserGroups_customizeGrid_2, _YLOC("User Display Name")).c_str(),	GridTemplate::g_TypeUser,	g_ColumnSize22, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnUserPrincipalName	= AddColumn(_YUID(O365_USER_USERPRINCIPALNAME),		YtriaTranslate::Do(GridUserGroups_customizeGrid_3, _YLOC("Username")).c_str(),			GridTemplate::g_TypeUser,	g_ColumnSize32);
	setBasicGridSetupHierarchy({ { { m_TemplateUsers.m_ColumnDisplayName, 0 } }
								,{ rttr::type::get<BusinessGroup>().get_id() }
								,{ rttr::type::get<BusinessGroup>().get_id(), rttr::type::get<BusinessUser>().get_id() } });

	if (IsMyData())
		m_TemplateUsers.m_ColumnDisplayName->RemovePresetFlags(g_ColumnsPresetDefault);

	static const wstring g_FamilyLicenses = _T("Licenses");
	m_ColumnHasAssignedLicences = AddColumnCheckBox(_YUID("HASASSIGNEDLICENSES"), _T("Has licenses assigned"), g_FamilyLicenses);
	m_ColumnHasAssignedLicences->SetPresetFlags({ g_ColumnsPresetDefault });
	m_ColumnLicenseAssignmentStatesSkuName = m_TemplateUsers.AddDefaultColumnFor(*this, _YUID("ASSIGNEDBYGROUPLICENSENAME"), g_FamilyLicenses, { g_ColumnsPresetDefault });
	m_ColumnLicenseAssignmentStatesSkuPartNumber = m_TemplateUsers.AddDefaultColumnFor(*this, _YUID("ASSIGNEDSKUPARTNUMBER"), g_FamilyLicenses, { });
	m_ColumnLicenseAssignmentStatesSkuId = m_TemplateUsers.AddDefaultColumnFor(*this, _YUID("licenseAssignmentStates.skuId"), g_FamilyLicenses, { g_ColumnsPresetTech });
	m_ColumnLicenseAssignmentStatesState = m_TemplateUsers.AddDefaultColumnFor(*this, _YUID("licenseAssignmentStates.state"), g_FamilyLicenses, { });
	m_ColumnLicenseAssignmentStatesError = m_TemplateUsers.AddDefaultColumnFor(*this, _YUID("licenseAssignmentStates.error"), g_FamilyLicenses, { });

	// We just used the template to create columns, but don't want them to be filled by template.
	m_TemplateUsers.m_ColumnLicenseAssignmentStatesSkuName = nullptr;
	m_TemplateUsers.m_ColumnLicenseAssignmentStatesSkuPartNumber = nullptr;
	m_TemplateUsers.m_ColumnLicenseAssignmentStatesSkuId = nullptr;
	m_TemplateUsers.m_ColumnLicenseAssignmentStatesState = nullptr;
	m_TemplateUsers.m_ColumnLicenseAssignmentStatesError = nullptr;

	m_MetaColumns.insert(m_MetaColumns.end(),
		{
			m_TemplateUsers.m_ColumnDisplayName,
			m_TemplateUsers.m_ColumnUserPrincipalName,
			m_TemplateUsers.m_ColumnLastRequestDateTime,
		});
	/*Add additional meta columns*/
	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		for (const auto& c : *metaDataColumnInfos)
		{
			ASSERT(nullptr == m_TemplateUsers.GetColumnForProperty(c.GetPropertyName()));
			if (nullptr == m_TemplateUsers.GetColumnForProperty(c.GetPropertyName()))
			{
				auto col = c.AddTo(*this, GridTemplate::g_TypeUser);
				m_TemplateUsers.GetColumnForProperty(c.GetPropertyName()) = col;
				m_MetaColumns.push_back(col);
				m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
			}
		}
	}

	m_ColumnDirectMember						= AddColumn(_YUID("directMember"),					YtriaTranslate::Do(GridUserGroups_customizeGrid_4, _YLOC("Direct Member")).c_str(),		GridTemplate::g_TypeUser,	g_ColumnSize12, { g_ColumnsPresetDefault });
	m_TemplateGroups.m_ColumnDisplayName		= AddColumn(_YUID(GROUP_DISPLAY_NAME_COL_UID),		YtriaTranslate::Do(GridUserGroups_customizeGrid_5, _YLOC("Group Display Name")).c_str(),	GridTemplate::g_TypeGroup,	g_ColumnSize22, { g_ColumnsPresetDefault });
	m_TemplateGroups.m_ColumnIsTeam				= AddColumnIcon(_YUID(O365_GROUP_ISTEAM),				YtriaTranslate::Do(GridGroups_customizeGrid_3, _YLOC("Is a Team")).c_str(), GridTemplate::g_TypeGroup);
	SetColumnPresetFlags(m_TemplateGroups.m_ColumnIsTeam, { g_ColumnsPresetDefault });
	m_TemplateGroups.m_ColumnCombinedGroupType	= AddColumn(_YUID("groupType"),						YtriaTranslate::Do(GridUserGroups_customizeGrid_6, _YLOC("Group Type")).c_str(),			GridTemplate::g_TypeGroup,	g_ColumnSize12, { g_ColumnsPresetDefault });
	addColumnGraphID(GridTemplate::g_TypeGroup);
	ASSERT(nullptr == m_TemplateGroups.m_ColumnID);
	m_TemplateGroups.m_ColumnID = GetColId();
	ASSERT(nullptr == m_TemplateUsers.m_ColumnID);
	m_TemplateUsers.m_ColumnID	= GetColId();

	AddColumnForRowPK(GetColId());
	AddColumnForRowPK(m_TemplateGroups.m_ColumnMetaID);

	m_TemplateUsers.CustomizeGrid(*this);
	m_TemplateGroups.CustomizeGrid(*this);

	m_DirectMemberIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_DIRECT_MEMBER));
	m_IndirectMemberIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_INDIRECT_MEMBER));

	if (g_DirectMemberText.empty())
		g_DirectMemberText = YtriaTranslate::Do(GridUserGroups_customizeGrid_7, _YLOC("Direct")).c_str();
	if (g_IndirectMemberText.empty())
		g_IndirectMemberText = YtriaTranslate::Do(GridUserGroups_customizeGrid_8, _YLOC("Inherited")).c_str();

	m_Frame = dynamic_cast<FrameUserGroups*>(GetParentFrame());

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_Frame);
}

wstring GridUserGroups::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Member"), m_TemplateUsers.m_ColumnDisplayName } }, m_TemplateGroups.m_ColumnDisplayName);
}

bool GridUserGroups::HasModificationsPending(bool inSelectionOnly /*= false*/)
{
	if (inSelectionOnly)
		return HasModificationsPendingInParentOfSelected();
	return O365Grid::HasModificationsPending(false);
}

bool GridUserGroups::HasRevertableModificationsPending(bool inSelectionOnly /*= false*/)
{
	// No distinction like in GridUserGroups::HasModificationsPending as revert only concerns really selected rows.
	return O365Grid::HasModificationsPending(inSelectionOnly);
}

bool GridUserGroups::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return O365Grid::IsRowValidForNoFieldText(p_Row, p_Col)
		&& (!p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore)
			|| m_MetaIDsMoreLoaded.end() == m_MetaIDsMoreLoaded.find(p_Row->GetField(m_TemplateGroups.m_ColumnMetaID).GetValueStr()));
}

bool GridUserGroups::IsMyData() const
{
	return m_IsMyData;
}

O365Grid::SnapshotCaps GridUserGroups::GetSnapshotCapabilities() const
{
	return { true };
}

void GridUserGroups::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_USERGROUPSGRID_ADDTOGROUP);
		pPopup->ItemInsert(); // Sep
		pPopup->ItemInsert(ID_USERGROUPSGRID_REMOVEFROMGROUP);
		pPopup->ItemInsert(ID_USERGROUPSGRID_COPYTO);
		pPopup->ItemInsert(ID_USERGROUPSGRID_TRANSFERTO);
		pPopup->ItemInsert(); // Sep
	}

	O365Grid::OnCustomPopupMenu(pPopup, nStart);
}
void GridUserGroups::InitializeCommands()
{
	O365Grid::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_USERGROUPSGRID_ADDTOGROUP;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridUserGroups_InitializeCommands_1, _YLOC("Add to Groups...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUserGroups_InitializeCommands_2, _YLOC("Add to Groups...")).c_str();
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		// For the right click menu icon.
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_ADDTOGROUP_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGROUPSGRID_ADDTOGROUP, hIcon, false);

		setRibbonTooltip(
			_cmd.m_nCmdID,
			YtriaTranslate::Do(GridUserGroups_InitializeCommands_3, _YLOC("Add Users to Groups")).c_str(),
			YtriaTranslate::Do(GridUserGroups_InitializeCommands_4, _YLOC("Add all selected users to groups. You will be able to select groups from a directory list.\nNote: This function is only available in Hierarchy view (Ctrl+F11).")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_USERGROUPSGRID_REMOVEFROMGROUP;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridUserGroups_InitializeCommands_5, _YLOC("Remove")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUserGroups_InitializeCommands_6, _YLOC("Remove")).c_str();
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		// For the right click menu icon.
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_REMOVEFROMGROUP_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGROUPSGRID_REMOVEFROMGROUP, hIcon, false);

		setRibbonTooltip(
			_cmd.m_nCmdID, 
			YtriaTranslate::Do(GridUserGroups_InitializeCommands_7, _YLOC("Remove Selected Group Memberships")).c_str(), 
			YtriaTranslate::Do(GridUserGroups_InitializeCommands_8, _YLOC("Remove all selected groups memberships.")).c_str(), 
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_USERGROUPSGRID_COPYTO;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridUserGroups_InitializeCommands_9, _YLOC("Copy")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUserGroups_InitializeCommands_10, _YLOC("Copy...")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);

		// For the right click menu icon.
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_COPYMEMBERSHIP_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGROUPSGRID_COPYTO, hIcon, false);

		setRibbonTooltip(
			_cmd.m_nCmdID, 
			YtriaTranslate::Do(GridUserGroups_InitializeCommands_11, _YLOC("Copy Memberships")).c_str(),
			YtriaTranslate::Do(GridUserGroups_InitializeCommands_12, _YLOC("Copy the currently selected group memberships to additional users.\nYou will be able to select additional users from a directory li")).c_str(), 
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_USERGROUPSGRID_TRANSFERTO;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridUserGroups_InitializeCommands_13, _YLOC("Move")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridUserGroups_InitializeCommands_14, _YLOC("Move...")).c_str();
		g_CmdManager->CmdSetup(profileName, _cmd);

		// For the right click menu icon.
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_TRANSFERMEMBERSHIP_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGROUPSGRID_TRANSFERTO, hIcon, false);

		setRibbonTooltip(
			_cmd.m_nCmdID, 
			YtriaTranslate::Do(GridUserGroups_InitializeCommands_15, _YLOC("Move Memberships")).c_str(), 
			YtriaTranslate::Do(GridUserGroups_InitializeCommands_16, _YLOC("Transfer the currently selected group memberships to other users.\nYou will be able to select users from a directory list.\nNote: This will also unassign the memberships from those members currently selected in the grid.")).c_str(), 
			_cmd.m_sAccelText);
	}
}

ModuleCriteria GridUserGroups::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_Frame->GetModuleCriteria();

    criteria.m_IDs.clear();
    for (auto row : p_Rows)
        criteria.m_IDs.insert(row->GetField(m_TemplateGroups.m_ColumnMetaID).GetValueStr());

    return criteria;
}

void GridUserGroups::AddUserWithParentGroups(vector<BusinessUser> p_Users, const bool p_IsRefresh, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge)
{
	ASSERT(!IsMyData() || p_Users.size() == 1);

	{
		const uint32_t options = GridUpdaterOptions::UPDATE_GRID
			| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
			| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			/*| GridUpdaterOptions::REFRESH_PROCESS_DATA*/
			/*| GridUpdaterOptions::PROCESS_LOADMORE*/;

		GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateOperations);

		bool oneCanceled = false;
		bool oneOK = false;
		for (const auto& user : p_Users)
		{
			if (m_HasLoadMoreAdditionalInfo)
			{
				if (user.IsMoreLoaded())
					m_MetaIDsMoreLoaded.insert(user.GetID());
				else
					m_MetaIDsMoreLoaded.erase(user.GetID());
			}

			GridBackendRow* rowUser = m_TemplateUsers.AddRow(*this, user, { m_TemplateGroups.GetMetaIDField(user.GetID()) }, updater, false, true);// no value - for pk consistency
			ASSERT(nullptr != rowUser);
			if (nullptr != rowUser)
			{
				if (!IsMyData()) // Row will be deleted below
					updater.GetOptions().AddRowWithRefreshedValues(rowUser);
				rowUser->SetHierarchyParentToHide(true);

				if (user.HasFlag(BusinessObject::CANCELED))
				{
					oneCanceled = true;

					// Could be uncommented if we notice missing explosion refresh on canceled rows
					//auto children = GetChildRows(rowUser);
					//for (auto child : children)
					//	updater.GetOptions().AddRowWithRefreshedValues(child);
				}
				else
				{
					oneOK = true;
					if (!IsMyData()) // Row will be deleted below
						updater.GetOptions().AddPartialPurgeParentRow(rowUser);

					vector<GridBackendField> BUK = { m_TemplateGroups.GetMetaIDField(user.GetID()) };
					for (const auto& groupId : user.GetParentGroups())
					{
						auto group = BusinessGroup(GetGraphCache().GetGroupInCache(groupId));
						//ASSERT(group.GetID() == groupId);
						group.SetID(groupId);
						GridBackendRow* rowGroup = m_TemplateGroups.AddRow(*this, group, BUK, updater, false, rowUser, {}, false);
						ASSERT(nullptr != rowGroup);
						if (nullptr != rowGroup)
						{
							updater.GetOptions().AddRowWithRefreshedValues(rowGroup);

							for (auto c : m_MetaColumns)
							{
								if (rowUser->HasField(c))
									rowGroup->AddField(rowUser->GetField(c));
								else
									rowGroup->RemoveField(c);
							}

							if (user.IsDirectMemberOf(group))
								rowGroup->AddField(g_DirectMemberText, m_ColumnDirectMember, m_DirectMemberIconId);
							else
								rowGroup->AddField(g_IndirectMemberText, m_ColumnDirectMember, m_IndirectMemberIconId);

							AddRoleDelegationFlag(rowGroup, group);

							{
								boost::YOpt<vector<PooledString>> skuIds;
								boost::YOpt<vector<PooledString>> skuPartNumbers;
								boost::YOpt<vector<PooledString>> skuNames;
								boost::YOpt<vector<PooledString>> states;
								boost::YOpt<vector<PooledString>> errors;
								if (user.GetLicenseAssignmentStates())
								{
									static const wstring errorUnknown = _YERROR("-- Unknown SKU --"); // GridBackendUtil::g_NoValueString ??
									for (const auto& las : *user.GetLicenseAssignmentStates())
									{
										if (las.m_AssignedByGroup == group.GetID())
										{
											if (!skuIds)
											{
												skuIds.emplace();
												skuPartNumbers.emplace();
												skuNames.emplace();
												states.emplace();
												errors.emplace();
											}

											if (las.m_SkuId)
											{
												const auto& skuPartNumber = SkuAndPlanReference::GetInstance().GetSkuPartNumber(*las.m_SkuId);
												const auto& label = SkuAndPlanReference::GetInstance().GetSkuLabel(*las.m_SkuId);

												skuIds->emplace_back(*las.m_SkuId);

												if (skuPartNumber.IsEmpty())
													skuPartNumbers->emplace_back(errorUnknown);
												else
													skuPartNumbers->emplace_back(skuPartNumber);

												if (!label.IsEmpty())
													skuNames->emplace_back(label);
												else if (!skuPartNumber.IsEmpty())
													skuNames->emplace_back(skuPartNumber);
												else
													skuNames->emplace_back(errorUnknown);
											}
											else
											{
												skuIds->emplace_back(GridBackendUtil::g_NoValueString);
												skuPartNumbers->emplace_back(GridBackendUtil::g_NoValueString);
												skuNames->emplace_back(GridBackendUtil::g_NoValueString);
											}

											if (las.m_State)
												states->emplace_back(*las.m_State);
											else
												states->emplace_back(GridBackendUtil::g_NoValueString);

											if (las.m_Error)
												errors->emplace_back(*las.m_Error);
											else
												errors->emplace_back(GridBackendUtil::g_NoValueString);
										}
									}

									rowGroup->AddField(skuIds.is_initialized(), m_ColumnHasAssignedLicences);
									rowGroup->AddField(skuIds, m_ColumnLicenseAssignmentStatesSkuId);
									rowGroup->AddField(skuPartNumbers, m_ColumnLicenseAssignmentStatesSkuPartNumber);
									rowGroup->AddField(skuNames, m_ColumnLicenseAssignmentStatesSkuName);
									rowGroup->AddField(states, m_ColumnLicenseAssignmentStatesState);
									rowGroup->AddField(errors, m_ColumnLicenseAssignmentStatesError);
								}
							}
						}
					}
				}

				AddRoleDelegationFlag(rowUser, user);

				if (IsMyData())
				{
					ASSERT(m_MyDataMeHandler);
					ASSERT(rowUser->HasChildrenRows() || user.GetParentGroups().empty());
					m_MyDataMeHandler->GetRidOfMeRow(*this, rowUser);
				}
			}
		}

		if (oneCanceled)
		{
			if (oneOK)
				updater.GetOptions().SetPurgeFlag(GridUpdaterOptions::PARTIALPURGE);
			else
				updater.GetOptions().SetPurgeFlag(0); // Everything canceled, don't purge anything.
		}
	}

	if (m_PendingAddGroups)
	{
		ASSERT(p_Users.size() == m_PendingAddGroups->m_Users.size());

		auto pendingAdd = *m_PendingAddGroups;
		m_PendingAddGroups.reset();
		addMembersToGroupsImpl(pendingAdd.m_Users, pendingAdd.m_GroupsToAddTo, pendingAdd.m_ShouldRemoveFromSource, pendingAdd.m_ShouldScroll);
		ASSERT(!m_PendingAddGroups);
	}
}

GridGroupsCommon::GroupChanges GridUserGroups::GetGroupChanges(bool p_Selected, bool p_TopLevel)
{
	GridGroupsCommon::GroupChanges groupChanges;

    vector<GridBackendRow*> rows;
	if (p_Selected && p_TopLevel)
	{
		std::set<GridBackendRow*> topLevelRows;
		if (!IsMyData())
		{
			for (auto row : GetSelectedRows())
			{
				auto topLevelRow = row->GetTopAncestorOrThis();
				topLevelRows.insert(topLevelRow);
			}
		}

		OnUnSelectAll();
		auto modRows = GetModifications().GetModifiedRows(true, false);
		for (auto row : modRows)
		{
			if (IsMyData() || topLevelRows.end() != topLevelRows.find(row->GetTopAncestorOrThis()))
			{
				row->SetSelected(true);
				rows.push_back(row);
			}
		}
	}
	else if (p_Selected)
	{
		GetSelectedNonGroupRows(rows);
	}

	for (auto row : (p_Selected ? rows : GetBackendRows()))
	{
		if (!row->IsGroupRow())
		{
			if (row->IsCreated() || row->IsDeleted())
			{
				ASSERT(GridUtil::IsBusinessGroup(row));
				if (GridUtil::IsBusinessGroup(row))
				{
					auto parentRow = row->GetParentRow();
					ASSERT(nullptr == parentRow && IsMyData() || nullptr != parentRow && GridUtil::IsBusinessUser(parentRow));
					if (nullptr == parentRow && IsMyData() || nullptr != parentRow && GridUtil::IsBusinessUser(parentRow))
					{
						ASSERT(row->HasField(GetColId()));
						PooledString groupID = row->GetField(GetColId()).GetValueStr();
						ASSERT(IsMyData() ? m_MyDataMeHandler->HasField(GetColId()->GetID()) : parentRow->HasField(GetColId()));
						PooledString userID = IsMyData() ? m_MyDataMeHandler->GetField(GetColId()->GetID()).GetValueStr() : parentRow->GetField(GetColId()).GetValueStr();

						auto it = std::find_if(groupChanges.begin(), groupChanges.end(), [&groupID, &userID](GridGroupsCommon::GroupChange& groupChange) {return groupChange.m_GroupID == groupID && groupChange.m_ItemID == userID; });
						ASSERT(it == groupChanges.end());
						if (it == groupChanges.end())
						{
							groupChanges.emplace_back();
							GridGroupsCommon::GroupChange& groupChange = groupChanges.back();

							groupChange.m_ItemType = GridGroupsCommon::GroupChange::UserItem;

							groupChange.m_GroupID = groupID;
							groupChange.m_ItemID = userID;

							groupChange.m_RowPrimaryKey.emplace();
							GetRowPK(row, *groupChange.m_RowPrimaryKey);
							groupChange.m_UpdateType = row->IsCreated() ? BusinessObject::UPDATE_TYPE_ADDMEMBERS : BusinessObject::UPDATE_TYPE_DELETEMEMBERS;
						}
					}
				}
			}
		}
	}

	//std::sort(groupChanges.begin(), groupChanges.end(), [](const GroupChange& gc1, const GroupChange& gc2) {return gc1.m_GroupID < gc2.m_GroupID; });
	return groupChanges;
}

GridBackendColumn* GridUserGroups::GetColUserId()
{
    return m_TemplateGroups.m_ColumnMetaID;
}

void GridUserGroups::OnAddUsersToGroups()
{
	if (IsFrameConnected())
	{
		// <User Id, <User name, Group ids>>
		std::map<PooledString, std::pair<PooledString, std::set<PooledString>>> parentsForSelection;
		set<GridBackendRow*> businessUserRows;

		std::function<void(GridBackendRow*)> addTargetUser;
		if (IsMyData())
			addTargetUser = [this, &businessUserRows, &parentsForSelection](GridBackendRow* p_Row)
		{
			auto businessUserRow = GridUtil::IsBusinessUser(p_Row) ? p_Row : p_Row->GetParentRow();
			ASSERT(GridUtil::IsBusinessUser(businessUserRow));
			if (GridUtil::IsBusinessUser(businessUserRow) && businessUserRows.insert(businessUserRow).second)
				parentsForSelection[businessUserRow->GetField(GetColId()).GetValueStr()] = { businessUserRow->GetField(m_TemplateUsers.m_ColumnDisplayName).GetValueStr(), GetIdsOfChildRowsThatAllMatch({businessUserRow}) };
		};
		else
			addTargetUser = [this, &businessUserRows, &parentsForSelection](GridBackendRow* p_Row)
		{
			auto businessUserRow = GridUtil::IsBusinessUser(p_Row) ? p_Row : p_Row->GetParentRow();
			ASSERT(GridUtil::IsBusinessUser(businessUserRow));
			if (GridUtil::IsBusinessUser(businessUserRow) && businessUserRows.insert(businessUserRow).second)
				parentsForSelection[businessUserRow->GetField(GetColId()).GetValueStr()] = { businessUserRow->GetField(m_TemplateUsers.m_ColumnDisplayName).GetValueStr(), GetIdsOfChildRowsThatAllMatch({businessUserRow}) };
		};

		if (IsMyData())
		{
			parentsForSelection[m_MyDataMeHandler->GetField(GetColId()->GetID()).GetValueStr()] = { m_MyDataMeHandler->GetField(m_TemplateUsers.m_ColumnDisplayName->GetID()).GetValueStr(), GetIdsOfChildRowsThatAllMatch({nullptr}) };
			businessUserRows.insert(nullptr);
		}
		else
		{
			for (auto row : GetSelectedRows())
			{
				ASSERT(nullptr != row);
				if (nullptr != row && !row->IsGroupRow())
					addTargetUser(row);
			}
		}

		ASSERT(!businessUserRows.empty());
		if (!businessUserRows.empty())
		{
			vector<BusinessGroup> groups;
			getGroups(groups);

			ASSERT(!IsUsingRoleDelegation() || m_Frame->GetModuleCriteria().m_Privilege == RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT);

			DlgAddItemsToGroups dlg(
				GetSession(),
				parentsForSelection,
				DlgAddItemsToGroups::SELECT_GROUPS_FOR_USERS,
				{},
				groups,
				{},
				YtriaTranslate::Do(GridUserGroups_OnAddUsersToGroups_1, _YLOC("Add %1 selected user(s) to groups"), Str::getStringFromNumber(businessUserRows.size()).c_str()),
				YtriaTranslate::Do(GridUserGroups_OnAddUsersToGroups_2, _YLOC("The following %1 user(s) will be added to the groups you select below.\nTo see the complete list, use Load full directory."), Str::getStringFromNumber(businessUserRows.size()).c_str()),
				RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT,
				this);
			if (IDOK == dlg.DoModal())
			{
				if (nullptr != GetColId())
				{
					bool update = false;
					GridBackendRow* rowToScrollTo = nullptr;
					// List of all new users to add to selected groups.
					auto& newItems = dlg.GetSelectedItems();

					GridUpdater updater(*this, GridUpdaterOptions(/*GridUpdaterOptions::UPDATE_GRID | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES | */GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));

					// Add new users.
					for (auto userRow : businessUserRows)
					{
						ASSERT(nullptr != userRow || IsMyData());
						if (nullptr != userRow || IsMyData())
						{
							auto userId = IsMyData() ? m_MyDataMeHandler->GetField(GetColId()->GetID()).GetValueStr() : userRow->GetField(GetColId()).GetValueStr();
							vector<GridBackendRow*> addedRows;
							for (const auto& item : newItems)
							{
								// Recycle the row if already in.
								GridBackendRow* groupRow = GetChildRow(userRow, item.GetID(), GetColId()->GetID());

								if (nullptr == groupRow)
								{
									GridBackendField fieldId(item.GetID(), GetColId());

									// use cache so that we can have the real "group types" value.
									const auto group = BusinessGroup(GetGraphCache().GetGroupInCache(item.GetID()));
									ASSERT(group.GetID() == item.GetID()); // The DlgAddItemsToGroups loads the cache, we should always find our group in there...

									vector<GridBackendField> BUK;
									m_TemplateGroups.GetBusinessGroupPK(*this, group, BUK, { m_TemplateGroups.GetMetaIDField(userId) });

									GridBackendRow* groupRow = GetRowIndex().GetRow(BUK, true, false);
									ASSERT(nullptr != groupRow);
									if (nullptr != groupRow)
									{
										update = true;

										SetRowObjectType(groupRow, item.GetObjectType());
										m_TemplateGroups.updateRow(*this, group, groupRow, GetGraphCache(), false);

										groupRow->SetParentRow(userRow);

										ASSERT(GridUtil::IsBusinessGroup(groupRow));
										if (GridUtil::IsBusinessGroup(groupRow))
										{
											if (IsMyData())
											{
												for (auto c : m_MetaColumns)
												{
													if (m_MyDataMeHandler->HasField(c->GetID()))
														groupRow->AddField(m_MyDataMeHandler->GetField(c->GetID()));
													else
														groupRow->RemoveField(c);
												}
											}
											else
											{
												for (auto c : m_MetaColumns)
												{
													if (userRow->HasField(c))
														groupRow->AddField(userRow->GetField(c));
													else
														groupRow->RemoveField(c);
												}
											}
										}

										addedRows.push_back(groupRow);
									}
								}
								else
								{
									if (groupRow->IsDeleted())
									{
										row_pk_t rowPk;
										GetRowPK(groupRow, rowPk);
										update = GetModifications().Revert(rowPk, false);
									}
								}
							}

							if (nullptr == rowToScrollTo && addedRows.size() > 0)
							{
								OnUnSelectAll();
								rowToScrollTo = addedRows[0];
							}

							for (auto row : addedRows)
							{
								row_pk_t rowPk;
								GetRowPK(row, rowPk);
								GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPk));
								SelectRowAndExpandParentGroups(row, false);
							}
						}
					}

					if (update)
						UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

					if (nullptr != rowToScrollTo)
						ScrollToRow(rowToScrollTo);
				}
			}
		}
	}
}

void GridUserGroups::OnUpdateAddUsersToGroups(CCmdUI* pCmdUI)
{
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE);

	BOOL enable = FALSE;
	if (!IsMyData()
		&& IsFrameConnected()
		&& hasNoTaskRunning()
		&& IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT)
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, hasOutOfScopePrivilege](auto& p_Row)
		{
			if (nullptr != p_Row && !p_Row->IsGroupRow())
			{
				auto userRow = IsMyData() ? nullptr : p_Row->GetTopAncestorOrThis();
				ASSERT(nullptr == userRow && IsMyData() || GridUtil::IsBusinessUser(userRow));
				return hasOutOfScopePrivilege || IsUserAuthorizedByRoleDelegation(userRow, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE);
			}
			return false;
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridUserGroups::OnDeleteUsersFromGroups()
{
	deleteUsersFromGroups(GetSelectedRows(), true, false);
}

void GridUserGroups::OnUpdateDeleteUsersFromGroups(CCmdUI* pCmdUI)
{
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE);

	BOOL enable = FALSE;
	if (!IsMyData()
		&& hasNoTaskRunning()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, hasOutOfScopePrivilege](auto& p_Row)
		{
			return nullptr != p_Row
				&& !p_Row->IsGroupRow()
				&& (IsMyData() || nullptr != p_Row->GetParentRow())
				&& p_Row->GetField(m_ColumnDirectMember).GetIcon() == m_DirectMemberIconId
				&& (hasOutOfScopePrivilege || IsUserAuthorizedByRoleDelegation(p_Row->GetParentRow(), RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE))
				&& IsGroupAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT);
		}))
		enable = TRUE;
	
	pCmdUI->Enable(enable);

	setTextFromProfUISCommand(*pCmdUI);
}

void GridUserGroups::OnCopySelectionMembershipTo()
{
	copySelectedMembershipTo(false);
}

void GridUserGroups::OnTransferSelectionMembership()
{
	copySelectedMembershipTo(true);
}

void GridUserGroups::OnUpdateCopyTransferSelectionMembership(CCmdUI* pCmdUI)
{
	const bool isTransferMembership = ID_USERGROUPSGRID_TRANSFERTO == pCmdUI->m_nID;
	const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE);

	BOOL enable = FALSE;
	if (!IsMyData()
		&& hasNoTaskRunning()
		&& IsFrameConnected()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, isTransferMembership, hasOutOfScopePrivilege](auto& p_Row)
		{
			if (nullptr != p_Row && !p_Row->IsGroupRow())
			{
				if (GridUtil::IsBusinessUser(p_Row))
				{
					if (!isTransferMembership || hasOutOfScopePrivilege || IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE))
					{
						vector<GridBackendRow*> children;
						return GetBackend().GetHierarchyChildren(p_Row, children, GridBackend::GHCflags::ONLY_DIRECT, [this](GridBackendRow* theGroupRow)
						{
							return IsGroupAuthorizedByRoleDelegation(theGroupRow, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT);
						});
					}
				}
				else
				{
					ASSERT(GridUtil::IsBusinessGroup(p_Row));
					if (p_Row->GetField(m_ColumnDirectMember).GetIcon() == m_DirectMemberIconId
						&& IsGroupAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT)
						&& (!isTransferMembership || hasOutOfScopePrivilege || IsUserAuthorizedByRoleDelegation(p_Row->GetParentRow(), RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)))
						return true;
				}
			}
			return false;
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);

	setTextFromProfUISCommand(*pCmdUI);
}

bool GridUserGroups::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	if (m_TemplateUsers.GetSnapshotValue(p_Value, p_Field, p_Column))
		return true;

	if (m_TemplateGroups.GetSnapshotValue(p_Value, p_Field, p_Column))
		return true;

	if (p_Field.HasValue())
	{
		if (&p_Column == m_ColumnDirectMember)
		{
			if (p_Field.GetIcon() == m_DirectMemberIconId)
			{
				p_Value = _YTEXT("1");
			}
			else
			{
				ASSERT(p_Field.GetIcon() == m_IndirectMemberIconId);
				p_Value = _YTEXT("0");
			}
			return true;
		}
	}

	return O365Grid::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridUserGroups::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (m_TemplateUsers.LoadSnapshotValue(p_Value, p_Row, p_Column))
		return true;

	if (m_TemplateGroups.LoadSnapshotValue(p_Value, p_Row, p_Column))
		return true;

	if (m_ColumnDirectMember == &p_Column)
	{
		if (_YTEXT("1") == p_Value)
		{
			p_Row.AddField(g_DirectMemberText, m_ColumnDirectMember, m_DirectMemberIconId);
		}
		else
		{
			ASSERT(_YTEXT("0") == p_Value);
			p_Row.AddField(g_IndirectMemberText, m_ColumnDirectMember, m_IndirectMemberIconId);
		}

		return true;
	}

	return O365Grid::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

void GridUserGroups::getGroups(vector<BusinessGroup>& p_Groups) const
{
	CWaitCursor _;
	for (auto& row : GetBackendRows())
	{
		if (!row->IsExplosionAdded() && IsGroupAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT))
		{
			BusinessGroup group;
			m_TemplateGroups.GetBusinessGroup(*this, row, group);

			if (p_Groups.end() == std::find_if(p_Groups.begin(), p_Groups.end(), [&group](const BusinessGroup& bg) {return bg.GetID() == group.GetID(); }))
				p_Groups.emplace_back(group);
		}
	}
}

void GridUserGroups::getUsers(vector<BusinessUser>& p_Users) const
{
	CWaitCursor _;
	for (auto& row : GetBackendRows())
	{
		if (!row->IsExplosionAdded() && GridUtil::IsBusinessUser(row))
		{
			BusinessUser user;
			m_TemplateUsers.GetBusinessUser(*this, row, user);

			if (p_Users.end() == std::find_if(p_Users.begin(), p_Users.end(), [&user](const BusinessUser& bu) {return bu.GetID() == user.GetID(); }))
			{
				if (GridUtil::isBusinessType<BusinessUserGuest>(row))
					user.SetUserType(PooledString(BusinessUser::g_UserTypeGuest));
				else if (GridUtil::isBusinessType<BusinessUser>(row))
					user.SetUserType(PooledString(BusinessUser::g_UserTypeMember));
				p_Users.emplace_back(user);
			}
		}
	}
}

void GridUserGroups::deleteUsersFromGroups(const set<GridBackendRow*>& p_GroupRows, bool p_Update, bool p_DoNotUnSelectAll)
{
	bool update = false;

	map<GridBackendRow*, row_pk_t> rowsToRevert;
	std::set<GridBackendRow*> processedRows;
	for (auto row : p_GroupRows)
	{
		ASSERT(nullptr != row);
		if (nullptr != row && !row->IsGroupRow() && (IsMyData() || nullptr != row->GetParentRow()) && row->GetField(m_ColumnDirectMember).GetIcon() == m_DirectMemberIconId)
		{
			if (processedRows.end() == processedRows.find(row))
			{
				ASSERT(GridUtil::IsBusinessGroup(row));

				if (IsGroupAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT))
				{
					row_pk_t rowPk;
					GetRowPK(row, rowPk);

					if (row->IsCreated())
					{
						rowsToRevert[row] = rowPk;
					}
					else
					{
						GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, rowPk));
						update = true;
					}

					processedRows.insert(row);
				}
			}
		}
	}

	if (!processedRows.empty())
	{
		if (!p_DoNotUnSelectAll)
			OnUnSelectAll();
		for (auto row : processedRows)
			row->SetSelected(true);
	}

	for (auto item : rowsToRevert)
	{
		if (GetModifications().Revert(item.second, false))
		{
			update = true;
		}
		else
		{
			ASSERT(false);
		}
	}

	if (update && p_Update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

	//AutomationGreenlight();
}

void GridUserGroups::copySelectedMembershipTo(bool removeSourceMembership)
{
	if (IsFrameConnected() && IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT))
	{
		// <Group Id, <Group name, User ids>>
		std::map<PooledString, std::pair<PooledString, std::set<PooledString>>> parentsForSelection;
		set<GridBackendRow*> targetGroupRows;
		set<GridBackendRow*> userSelectedRows;

		auto addTargetGroup = [this, &targetGroupRows, &parentsForSelection](GridBackendRow* p_Row)
		{
			ASSERT(GridUtil::IsBusinessGroup(p_Row));
			if (GridUtil::IsBusinessGroup(p_Row))
			{
				const PooledString id = p_Row->GetField(GetColId()).GetValueStr();
				auto it = std::find_if(targetGroupRows.begin(), targetGroupRows.end(), [&id, this](GridBackendRow* row) { return id == row->GetField(GetColId()).GetValueStr(); });
				if (GridUtil::IsBusinessGroup(p_Row) && targetGroupRows.end() == it && targetGroupRows.insert(p_Row).second)
				{
					std::set<PooledString> userIds;
					vector< GridBackendRow*> siblings;
					GetRowsByCriteria(siblings, [&id, this](GridBackendRow* row) { return id == row->GetField(GetColId()).GetValueStr(); });
					ASSERT(!siblings.empty()); // At least 'p_Row'

					for (auto& sibling : siblings)
					{
						ASSERT(GridUtil::IsBusinessGroup(sibling));
						if (GridUtil::IsBusinessGroup(sibling))
						{
							ASSERT(nullptr != sibling->GetParentRow());
							userIds.insert(sibling->GetParentRow()->GetField(GetColId()).GetValueStr());
						}
					}

					parentsForSelection[id] = { p_Row->GetField(m_TemplateGroups.m_ColumnDisplayName).GetValueStr(), userIds };
				}
			}
		};

		auto addTargetGroupsFromUser = [this, &addTargetGroup](GridBackendRow* p_Row)
		{
			ASSERT(GridUtil::IsBusinessUser(p_Row));
			if (GridUtil::IsBusinessUser(p_Row))
			{
				auto children = GetChildRows(p_Row);
				for (auto child : children)
					addTargetGroup(child);
			}
		};

		for (auto row : GetSelectedRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row && !row->IsGroupRow())
			{
				if (GridUtil::IsBusinessUser(row))
					addTargetGroupsFromUser(row);
				else if (GridUtil::IsBusinessGroup(row))
					addTargetGroup(row);
			}
		}

		ASSERT(!targetGroupRows.empty());
		if (!targetGroupRows.empty())
		{
			vector<BusinessUser> users;
			getUsers(users);

			const wstring title = removeSourceMembership
				?	YtriaTranslate::Do(GridUserGroups_copySelectedMembershipTo_1, _YLOC("Move %1 selected memberships"), Str::getStringFromNumber(targetGroupRows.size()).c_str())
				:	YtriaTranslate::Do(GridUserGroups_copySelectedMembershipTo_2, _YLOC("Copy %1 selected memberships"), Str::getStringFromNumber(targetGroupRows.size()).c_str());

			const wstring explanation = removeSourceMembership
				?	YtriaTranslate::Do(GridUserGroups_copySelectedMembershipTo_3, _YLOC("These %1 memberships will be moved to the users you select below.\nThis will also unassign these memberships from the members currently selected in the grid.\nTo see the complete list, use Load full directory."), Str::getStringFromNumber(targetGroupRows.size()).c_str())
				:	YtriaTranslate::Do(GridUserGroups_copySelectedMembershipTo_4, _YLOC("These %1 memberships will be copied and assigned to the users you select below.\nTo see the complete list, use Load full directory."), Str::getStringFromNumber(targetGroupRows.size()).c_str());

			// if removeSourceMembership , should we restrict dialog to single row selection?
			DlgAddItemsToGroups dlg(
				GetSession(),
				parentsForSelection,
				DlgAddItemsToGroups::SELECT_USERS_FOR_GROUPS,
				users,
				{},
				{},
				title,
				explanation,
				!IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE)
					? RoleDelegationUtil::RBAC_Privilege::RBAC_NONE
					: RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE,
				this);
			if (IDOK == dlg.DoModal())
			{
				GridBackendRow* rowToScrollTo = nullptr;
				bool update = false;

				if (nullptr != GetColId())
				{
					// List of all new user to add to selected groups to.
					auto& newUsers = dlg.GetSelectedItems();
					OnUnSelectAll();
					addMembersToGroupsImpl(newUsers, targetGroupRows, removeSourceMembership, true);
				}
			}
		}

		if (m_PendingAddGroups)
		{
			ASSERT(nullptr != m_Frame);
			if (nullptr != m_Frame)
				m_Frame->AddToSelection(m_PendingAddGroups->m_Users);
			else
				m_PendingAddGroups.reset();
		}
	}
}

void GridUserGroups::addMembersToGroupsImpl(const vector<SelectedItem>& p_NewMembers, const std::set<GridBackendRow*>& p_TargetGroupRows, bool removeFromSourceGroup, bool shouldScrollToFirstAddedRow)
{
	ASSERT(!m_PendingAddGroups);

	GridBackendRow* rowToScrollTo = nullptr;
	bool update = false;
	set<GridBackendRow*> groupRowsToRemove;

	GridUpdater updater(*this, GridUpdaterOptions(/*GridUpdaterOptions::UPDATE_GRID | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES | */GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));

	for (auto targetGroupRow : p_TargetGroupRows)
	{
		ASSERT(nullptr != targetGroupRow);
		if (nullptr != targetGroupRow && GridUtil::IsBusinessGroup(targetGroupRow))
		{
			bool hasBeenAdded = false;
			const GridBackendField& groupFieldID = targetGroupRow->GetField(GetColId());

			vector<GridBackendRow*> addedRows;
			for (const auto& memberBeingAdded : p_NewMembers)
			{
				if (m_PendingAddGroups && m_PendingAddGroups->m_Users.end() != std::find(m_PendingAddGroups->m_Users.begin(), m_PendingAddGroups->m_Users.end(), memberBeingAdded))
				{
					m_PendingAddGroups->m_GroupsToAddTo.insert(targetGroupRow);
					continue;
				}

				const auto memberRows = GetRows(memberBeingAdded.GetID(), GetColId()->GetID());
				ASSERT(memberRows.size() == 0 || (memberRows.size() == 1 && 0 == memberRows.front()->GetHierarchyLevel())); //Either the user is once in the grid or is not.
				if (memberRows.size() == 0)
				{
					// We need to add it to selection before proceeding.
					if (!m_PendingAddGroups)
						m_PendingAddGroups.emplace();

					m_PendingAddGroups->m_Users.push_back(memberBeingAdded);
					m_PendingAddGroups->m_GroupsToAddTo.insert(targetGroupRow);
					continue;
				}

				auto memberRow = memberRows.front();
				// Recycle the row if already in.
				GridBackendRow* existingGroupRow = GetChildRow(memberRow, groupFieldID.GetValueStr(), GetColId()->GetID());
				if (nullptr == existingGroupRow)
				{
					const PooledString metaID = memberRow->GetField(GetColId()).GetValueStr();
					ASSERT(!metaID.IsEmpty());

					vector<GridBackendField> groupPK{ groupFieldID, m_TemplateGroups.GetMetaIDField(metaID) };
					rttr::type::type_id typeId;

					// use cache so that we can have the real "group types" value.
					auto bg = BusinessGroup(GetGraphCache().GetGroupInCache(groupFieldID.GetValueStr()));
					ASSERT(bg.GetID() == groupFieldID.GetValueStr());
					typeId = bg.get_type().get_id();

					const vector<GridBackendField> newRowFields
					{
						{ memberRow->GetField(m_TemplateUsers.m_ColumnDisplayName).GetValueStr(), m_TemplateUsers.m_ColumnDisplayName },
						{ memberRow->GetField(m_TemplateUsers.m_ColumnUserPrincipalName).GetValueStr(), m_TemplateUsers.m_ColumnUserPrincipalName },
						{ g_DirectMemberText, m_ColumnDirectMember, m_DirectMemberIconId }
					};

					GridBackendRow* newGroupRow = m_TemplateGroups.AddRow(*this, bg, groupPK, updater, true, memberRow, newRowFields, false);
					ASSERT(nullptr != newGroupRow);
					if (nullptr != newGroupRow)
					{
						update = true;
						addedRows.push_back(newGroupRow);
						hasBeenAdded = true;
						AddRoleDelegationFlag(newGroupRow, bg);
					}
				}
				else
				{
					if (existingGroupRow->IsDeleted())
					{
						row_pk_t rowPk;
						GetRowPK(existingGroupRow, rowPk);
						update = GetModifications().Revert(rowPk, false) || update;
					}
					hasBeenAdded = false;
				}
			}

			if (nullptr == rowToScrollTo && addedRows.size() > 0)
				rowToScrollTo = addedRows[0];

			for (auto row : addedRows)
				SelectRowAndExpandParentGroups(row, false);

			if (hasBeenAdded && removeFromSourceGroup && targetGroupRow->GetField(m_ColumnDirectMember).GetIcon() == m_DirectMemberIconId)
				groupRowsToRemove.insert(targetGroupRow);
		}
	}

	if (removeFromSourceGroup && !groupRowsToRemove.empty())
		deleteUsersFromGroups(groupRowsToRemove, !update && !m_PendingAddGroups, true);

	if (update && !m_PendingAddGroups)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

	if (nullptr != rowToScrollTo)
	{
		if (shouldScrollToFirstAddedRow)
			ScrollToRow(rowToScrollTo);
	}
	else
	{
		if (m_PendingAddGroups)
		{
			m_PendingAddGroups->m_ShouldScroll = true;
			m_PendingAddGroups->m_ShouldRemoveFromSource = removeFromSourceGroup;
		}
	}
}
