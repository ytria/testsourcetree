#include "GridTemplateUsers.h"

#include "BasicGridSetup.h"
#include "BusinessUser.h"
#include "BusinessUserGuest.h"
#include "BusinessUserConfiguration.h"
#include "GridUpdater.h"
#include "GridUsers.h"
#include "GridUtil.h"
#include "MFCUtil.h"
#include "MsGraphFieldNames.h"
#include "RevokeAccessFieldUpdate.h"
#include "SisterhoodFieldUpdate.h"
#include "SkuAndPlanReference.h"
// without the "../" it takes the wrong resource.h...
#include "../Resource.h"

COLORREF GridTemplateUsers::g_ColorRowUser = RGB(255, 250, 205);// lemonchiffon
boost::bimap<PooledString, PooledString> GridTemplateUsers::g_CreationTypes;
wstring GridTemplateUsers::g_GroupLicensing;

row_pk_t GridTemplateUsers::CreateRowPk(const OnPremiseUser& p_OnPremUser)
{
	row_pk_t pk;

	pk.reserve(2);
	pk.emplace_back(p_OnPremUser.GetObjectGUID() ? *p_OnPremUser.GetObjectGUID() : PooledString::g_EmptyString, m_ColumnID->GetID());
	//pk.emplace_back(PooledString::g_EmptyString, m_ColumnUserType->GetID());

	return pk;
}

const std::array<GridBackendColumn* GridTemplateUsers::*, NUM_OP_EXTENSION_ATTRIBUTES> GridTemplateUsers::m_ColumnsOPExtensionAttributes
{
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes1,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes2,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes3,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes4,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes5,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes6,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes7,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes8,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes9,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes10,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes11,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes12,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes13,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes14,
	&GridTemplateUsers::m_ColumnsOPExtensionAttributes15,
};

GridTemplateUsers::GridTemplateUsers()
	: GridTemplateUsers(true)
{
}

GridTemplateUsers::GridTemplateUsers(bool p_ShowCachedUsersOnly)
	: m_ShowCachedUsersOnly(p_ShowCachedUsersOnly)
	, m_ColumnAboutMe(nullptr)
	, m_ColumnAccountEnabled(nullptr)
	, m_ColumnBirthday(nullptr)
	, m_ColumnBusinessPhones(nullptr)
	, m_ColumnCity(nullptr)
	, m_ColumnCompanyName(nullptr)
	, m_ColumnCountry(nullptr)
	, m_ColumnDepartment(nullptr)
	, m_ColumnGivenName(nullptr)
	, m_ColumnHireDate(nullptr)
	, m_ColumnImAddresses(nullptr)
	, m_ColumnInterests(nullptr)
	, m_ColumnJobTitle(nullptr)
	, m_ColumnMail(nullptr)
	, m_ColumnMailNickname(nullptr)
	, m_ColumnMailboxSettingsArchiveFolderId(nullptr)
	, m_ColumnMailboxSettingsArchiveFolderName(nullptr)
	, m_ColumnMailboxSettingsTimeZone(nullptr)
	, m_ColumnMailboxSettingsLanguageLocale(nullptr)
	, m_ColumnMailboxSettingsLanguageDisplayName(nullptr)
	, m_ColumnMailboxSettingsAutomaticRepliesSettingsStatus(nullptr)
	, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeDateTime(nullptr)
	, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeTimeZone(nullptr)
	, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeDateTime(nullptr)
	, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeTimeZone(nullptr)
	, m_ColumnMailboxSettingsAutomaticRepliesSettingsInternalReplyMessage(nullptr)
	, m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalReplyMessage(nullptr)
	, m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalAudience(nullptr)
	, m_ColumnMailboxSettingsTimeFormat(nullptr)
	, m_ColumnMailboxSettingsDateFormat(nullptr)
	, m_ColumnMailboxSettingsDelegateMeetingMessageDeliveryOptions(nullptr)
	, m_ColumnMailboxSettingsWorkingHoursDaysOfWeek(nullptr)
	, m_ColumnMailboxSettingsWorkingHoursStartTime(nullptr)
	, m_ColumnMailboxSettingsWorkingHoursEndTime(nullptr)
	, m_ColumnMailboxSettingsWorkingHoursTimeZoneName(nullptr)
	, m_ColumnManagerDisplayName(nullptr)
	, m_ColumnManagerUserPrincipalName(nullptr)
	, m_ColumnManagerID(nullptr)
	, m_ColumnMobilePhone(nullptr)
	, m_ColumnMySite(nullptr)
	, m_ColumnOfficeLocation(nullptr)
	, m_ColumnOPImmutableId(nullptr)
	, m_ColumnOPLastSync(nullptr)
	, m_ColumnOPSecurityID(nullptr)
	, m_ColumnOPSyncEnabled(nullptr)
	, m_ColumnPasswordPolicies(nullptr)
	//, m_ColumnPasswordProfile(nullptr)
	, m_ColumnPassword(nullptr)
	, m_ColumnForceChangePassword(nullptr)
	, m_ColumnForceChangePasswordWithMfa(nullptr)
	, m_ColumnPastProjects(nullptr)
	, m_ColumnPostalCode(nullptr)
	, m_ColumnPreferredLanguage(nullptr)
	, m_ColumnPreferredName(nullptr)
	, m_ColumnProxyAddresses(nullptr)
	, m_ColumnResponsibilities(nullptr)
	, m_ColumnSchools(nullptr)
	, m_ColumnSkills(nullptr)
	, m_ColumnState(nullptr)
	, m_ColumnStreetAddress(nullptr)
	, m_ColumnSurname(nullptr)
	, m_ColumnUsageLocation(nullptr)
	, m_ColumnUserPrincipalName(nullptr)
	, m_ColumnUserType(nullptr)
	, m_ColumnDeletedDateTime(nullptr)
	, m_ColumnSkuIds(nullptr)
	, m_ColumnSkuPartNumbers(nullptr)
	, m_ColumnSkuNames(nullptr)
	, m_ColumnLastRequestDateTime(nullptr)
	, m_ColumnAgeGroup(nullptr)
	, m_ColumnCreatedDateTime(nullptr)
	, m_ColumnConsentProvidedForMinor(nullptr)
	, m_ColumnLegalAgeGroupClassification(nullptr)
	, m_ColumnSignInSessionsValidFromDateTime(nullptr)
	, m_ColumnOPDomainName(nullptr)
	, m_ColumnOPSamAccountName(nullptr)
	, m_ColumnOPUserPrincipalName(nullptr)
	, m_ColumnOPProvisioningErrorsCategory(nullptr)
	, m_ColumnOPProvisioningErrorsOccuredDateTime(nullptr)
	, m_ColumnOPProvisioningErrorsPropertyCausingError(nullptr)
	, m_ColumnOPProvisioningErrorsValue(nullptr)
	, m_ColumnOtherMails(nullptr)
	, m_ColumnPreferredDataLocation(nullptr)
	, m_ColumnShowInAddressList(nullptr)
	, m_ColumnExternalUserStatus(nullptr)
	, m_ColumnExternalUserStatusChangedOn(nullptr)
	, m_AllowedIconId(NO_ICON)
	, m_BlockedIconId(NO_ICON)
	, m_GroupLicensingIconId(NO_ICON)
	, m_MailboxLoadedIconId(NO_ICON)
	, m_MailboxNotLoadedIconId(NO_ICON)
	, m_ColumnEmployeeID(nullptr)
	, m_ColumnFaxNumber(nullptr)
	, m_ColumnIsResourceAccount(nullptr)
	, m_ColumnOPDistinguishedName(nullptr)
	/* Drive columns */
	, m_ColumnDriveName(nullptr)
	, m_ColumnDriveId(nullptr)
	, m_ColumnDriveDescription(nullptr)
	, m_ColumnDriveQuotaState(nullptr)
	, m_ColumnDriveQuotaUsed(nullptr)
	, m_ColumnDriveQuotaRemaining(nullptr)
	, m_ColumnDriveQuotaDeleted(nullptr)
	, m_ColumnDriveQuotaTotal(nullptr)
	, m_ColumnDriveCreationTime(nullptr)
	, m_ColumnDriveLastModifiedTime(nullptr)
	, m_ColumnDriveType(nullptr)
	, m_ColumnDriveWebUrl(nullptr)
	, m_ColumnDriveCreatedByUserEmail(nullptr)
	, m_ColumnDriveCreatedByUserId(nullptr)
	, m_ColumnDriveCreatedByAppName(nullptr)
	, m_ColumnDriveCreatedByAppId(nullptr)
	, m_ColumnDriveCreatedByDeviceName(nullptr)
	, m_ColumnDriveCreatedByDeviceId(nullptr)
	, m_ColumnDriveOwnerUserName(nullptr)
	, m_ColumnDriveOwnerUserId(nullptr)
	, m_ColumnDriveOwnerUserEmail(nullptr)
	, m_ColumnDriveOwnerAppName(nullptr)
	, m_ColumnDriveOwnerAppId(nullptr)
	, m_ColumnDriveOwnerDeviceName(nullptr)
	, m_ColumnDriveOwnerDeviceId(nullptr)
	, m_ColumnsOPExtensionAttributes1(nullptr)
	, m_ColumnsOPExtensionAttributes2(nullptr)
	, m_ColumnsOPExtensionAttributes3(nullptr)
	, m_ColumnsOPExtensionAttributes4(nullptr)
	, m_ColumnsOPExtensionAttributes5(nullptr)
	, m_ColumnsOPExtensionAttributes6(nullptr)
	, m_ColumnsOPExtensionAttributes7(nullptr)
	, m_ColumnsOPExtensionAttributes8(nullptr)
	, m_ColumnsOPExtensionAttributes9(nullptr)
	, m_ColumnsOPExtensionAttributes10(nullptr)
	, m_ColumnsOPExtensionAttributes11(nullptr)
	, m_ColumnsOPExtensionAttributes12(nullptr)
	, m_ColumnsOPExtensionAttributes13(nullptr)
	, m_ColumnsOPExtensionAttributes14(nullptr)
	, m_ColumnsOPExtensionAttributes15(nullptr)
	, m_ColumnCreationType(nullptr)
	, m_ColumnIdentitiesSignInType(nullptr)
	, m_ColumnIdentitiesIssuer(nullptr)
	, m_ColumnIdentitiesIssuerAssignedId(nullptr)
	, m_ColumnLastPasswordChangeDateTime(nullptr)
	, m_ColumnLicenseAssignmentStatesAssignedByGroupId(nullptr)
	, m_ColumnLicenseAssignmentStatesAssignedByGroupDisplayName(nullptr)
	, m_ColumnLicenseAssignmentStatesAssignedByGroupMail(nullptr)
	, m_ColumnLicenseAssignmentStatesState(nullptr)
	, m_ColumnLicenseAssignmentStatesError(nullptr)
	, m_ColumnLicenseAssignmentStatesSkuId(nullptr)
	, m_ColumnLicenseAssignmentStatesSkuPartNumber(nullptr)
	, m_ColumnLicenseAssignmentStatesSkuName(nullptr)
	, m_ColumnRefresTokenValidFromDateTime(nullptr)
	, m_ColumnIsAnyLicenseAssignedByGroup(nullptr)
	, m_ColumnMailboxType(nullptr)
	, m_ColumnMailboxDeliverToMailboxAndForward(nullptr)
	, m_ColumnMailboxForwardingSmtpAddress(nullptr)
	, m_ColumnMailboxForwardingAddress(nullptr)
	, m_ColumnMailboxCanSendOnBehalf(nullptr)
	, m_ColumnRecipientPermissionsCanSendAsUser(nullptr)
{
	if (g_CreationTypes.empty())
	{
		g_CreationTypes.insert({ PooledString(), _T("Regular school or work account") });
		g_CreationTypes.insert({ _YTEXT("Invitation"), _T("External account") });
		g_CreationTypes.insert({ _YTEXT("LocalAccount"), _T("Local account for an Azure Active Directory B2C tenant") });
		g_CreationTypes.insert({ _YTEXT("EmailVerified"), _T("Self-service sign-up using email verification") });
	}

	if (g_GroupLicensing.empty())
		g_GroupLicensing = _T("Member - Group licensing");
}

GridBackendRow* GridTemplateUsers::AddRow(O365Grid& p_Grid, const BusinessUser& p_BU, const vector<GridBackendField>& p_ExtraRowPKValues, GridUpdater& p_Updater, bool p_SetCreatedStatus, bool p_KeepExistingIfUserCanceled)
{
	vector<GridBackendField> rowPk;
	GetBusinessUserPK(p_Grid, p_BU, rowPk);
	rowPk.insert(rowPk.end(), p_ExtraRowPKValues.begin(), p_ExtraRowPKValues.end());

	if (p_SetCreatedStatus)
	{
		bool wasError = false;
		{
			auto mod = p_Grid.GetModifications().GetRowModification<CreatedObjectModification>(rowPk);
			if (nullptr != mod)
			{
				wasError = mod->GetState() == Modification::State::RemoteError;
				p_Grid.GetModifications().Revert(rowPk, false);
			}
		}

		{
			auto row = p_Grid.GetRowIndex().GetExistingRow(rowPk);
			ASSERT(nullptr == row || wasError);
			if (nullptr != row)
				p_Grid.RemoveRow(row);
		}
	}

	ASSERT(!p_SetCreatedStatus || nullptr == p_Grid.GetRowIndex().GetExistingRow(rowPk));

	const bool rowExists = nullptr != p_Grid.GetRowIndex().GetExistingRow(rowPk);
	GridBackendRow* row = nullptr;
	if (p_KeepExistingIfUserCanceled && p_BU.HasFlag(BusinessObject::CANCELED)
		&& rowExists)
	{
		row = p_Grid.GetRowIndex().GetRow(rowPk, false, !p_SetCreatedStatus && (p_Updater.GetOptions().IsFullPurge() || p_Updater.GetOptions().IsPartialPurge()));
		ASSERT(nullptr != row);
		setLastRequestDateTime(p_Grid, p_BU, row);
	}
	else
	{
		row = p_Grid.GetRowIndex().GetRow(rowPk, true, !p_SetCreatedStatus && (p_Updater.GetOptions().IsFullPurge() || p_Updater.GetOptions().IsPartialPurge()));
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			SetO365(p_Grid, *row, p_BU);

			if (!rowExists) 
			{
				// newly created row, init stuff
				auto col = GetRequestDateColumn(UBI::MAILBOX);
				if (nullptr != col && p_Grid.IsRowForLoadMore(row, nullptr) && GridUtil::IsBusinessUser(row, false))
					row->AddField(p_Grid.GetLoadMoreText(false), col, m_MailboxNotLoadedIconId).RemoveModified();
			}

			auto deletedMod = p_Grid.IsGridModificationsEnabled()	? p_Grid.GetModifications().GetRowModification<DeletedObjectModification>(rowPk)
																	: nullptr;

			// Don't display the error message if the object has been successfully deleted (404 status code)
			if (p_BU.GetError() && !(p_BU.GetError()->GetStatusCode() == 404 && nullptr != deletedMod))
			{
				ASSERT(!p_SetCreatedStatus);

				// Even with a 404 error, we can have the display name when it comes from a previous request (see Authors Hierarchy grid)
				ASSERT(nullptr != m_ColumnDisplayName);
				updateField(p_BU.GetDisplayName(), row, m_ColumnDisplayName, UpdateFieldOption::NONE);

				p_Updater.OnLoadingError(row, *p_BU.GetError());
			}
			else
			{
				// Clear previous error
				if (row->IsError())
					row->ClearStatus();

				updateRow(p_Grid, p_BU, row, p_SetCreatedStatus ? UpdateFieldOption::IS_OBJECT_CREATION : UpdateFieldOption::NONE);
			}

			if (p_SetCreatedStatus)
			{
				auto mod = std::make_unique<CreatedObjectModification>(p_Grid, rowPk);
				mod->SetShouldRemoveRowOnRevertError(true); // Because refresh is using delta Sync!
				p_Grid.GetModifications().Add(std::move(mod));
			}

			if (p_Grid.IsGridModificationsEnabled())
				p_Updater.AddUpdatedRowPk(rowPk);
		}
	}

	return row;
}

void GridTemplateUsers::GetBusinessUser(const O365Grid& p_Grid, GridBackendRow* p_Row, BusinessUser& p_BU) const
{
	{
		const auto& field = p_Row->GetField(m_ColumnID);
		if (hasValidErrorlessValue(field))
		{
			p_BU.SetID(field.GetValueStr());
			p_BU.SetRBACDelegationID(p_Grid.GetGraphCache().GetUserInCache(p_BU.GetID(), false).GetRBACDelegationID());
		}
	}

	{
		const auto& field = p_Row->GetField(m_ColumnDisplayName);
		if (hasValidErrorlessValue(field))
			p_BU.SetDisplayName(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnAboutMe)
	{
		const auto& field = p_Row->GetField(m_ColumnAboutMe);
		if (hasValidErrorlessValue(field))
			p_BU.SetAboutMe(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnAccountEnabled)
	{
		const auto& field = p_Row->GetField(m_ColumnAccountEnabled);
		if (hasValidErrorlessValue(field))
			p_BU.SetAccountEnabled(field.GetValueBool());
	}

	if (nullptr != m_ColumnBirthday)
	{
		const auto& field = p_Row->GetField(m_ColumnBirthday);
		if (hasValidErrorlessValue(field))
			p_BU.SetBirthday(YTimeDate(field.GetValueTimeDate()));
	}

	if (nullptr != m_ColumnBusinessPhones)
	{
		const auto& field = p_Row->GetField(m_ColumnBusinessPhones);
		if (hasValidErrorlessValue(field))
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				p_BU.SetBusinessPhones(*field.GetValuesMulti<PooledString>());
			else
				p_BU.SetBusinessPhones(vector<PooledString>{ field.GetValueStr() });
		}
	}

	if (nullptr != m_ColumnCity)
	{
		const auto& field = p_Row->GetField(m_ColumnCity);
		if (hasValidErrorlessValue(field))
			p_BU.SetCity(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnCompanyName)
    {
        const auto& field = p_Row->GetField(m_ColumnCompanyName);
		if (hasValidErrorlessValue(field))
            p_BU.SetCompanyName(PooledString(field.GetValueStr()));
    }

	if (nullptr != m_ColumnCountry)
	{
		const auto& field = p_Row->GetField(m_ColumnCountry);
		if (hasValidErrorlessValue(field))
			p_BU.SetCountry(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnDepartment)
	{
		const auto& field = p_Row->GetField(m_ColumnDepartment);
		if (hasValidErrorlessValue(field))
			p_BU.SetDepartment(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnGivenName)
	{
		const auto& field = p_Row->GetField(m_ColumnGivenName);
		if (hasValidErrorlessValue(field))
			p_BU.SetGivenName(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnHireDate)
	{
		const auto& field = p_Row->GetField(m_ColumnHireDate);
		if (hasValidErrorlessValue(field))
			p_BU.SetHireDate(YTimeDate(field.GetValueTimeDate()));
	}

	if (nullptr != m_ColumnImAddresses)
    {
        const auto& field = p_Row->GetField(m_ColumnImAddresses);
		if (hasValidErrorlessValue(field))
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				p_BU.SetImAddresses(*field.GetValuesMulti<PooledString>());
			else
				p_BU.SetImAddresses(vector<PooledString>{ field.GetValueStr() });
		}
    }

	if (nullptr != m_ColumnInterests)
	{
		const auto& field = p_Row->GetField(m_ColumnInterests);
		if (hasValidErrorlessValue(field))
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				p_BU.SetInterests(*field.GetValuesMulti<PooledString>());
			else
				p_BU.SetInterests(vector<PooledString>{ field.GetValueStr() });
		}
	}

	if (nullptr != m_ColumnJobTitle)
	{
		const auto& field = p_Row->GetField(m_ColumnJobTitle);
		if (hasValidErrorlessValue(field))
			p_BU.SetJobTitle(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnMail)
	{
		const auto& field = p_Row->GetField(m_ColumnMail);
		if (hasValidErrorlessValue(field))
			p_BU.SetMail(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnMailNickname)
	{
		const auto& field = p_Row->GetField(m_ColumnMailNickname);
		if (hasValidErrorlessValue(field))
			p_BU.SetMailNickname(PooledString(field.GetValueStr()));
	}

	// FIXME: TODO MailboxSettings
	//m_ColumnMailboxSettingsArchiveFolderId;
	//m_ColumnMailboxSettingsArchiveFolderName;
	//m_ColumnMailboxSettingsTimeZone;
	//m_ColumnMailboxSettingsLanguageLocale;
	//m_ColumnMailboxSettingsLanguageDisplayName;
	//m_ColumnMailboxSettingsAutomaticRepliesSettingsStatus;
	//m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeDateTime;
	//m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeTimeZone;
	//m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeDateTime;
	//m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeTimeZone;
	//m_ColumnMailboxSettingsAutomaticRepliesSettingsInternalReplyMessage;
	//m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalReplyMessage;
	//m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalAudience;
	//m_ColumnMailboxSettingsTimeFormat;
	//m_ColumnMailboxSettingsDateFormat;
	//m_ColumnMailboxSettingsDelegateMeetingMessageDeliveryOptions;
	//m_ColumnMailboxSettingsWorkingHoursDaysOfWeek;
	//m_ColumnMailboxSettingsWorkingHoursStartTime;
	//m_ColumnMailboxSettingsWorkingHoursEndTime;
	//m_ColumnMailboxSettingsWorkingHoursTimeZoneName;

	ASSERT(nullptr != m_ColumnManagerID || nullptr == m_ColumnManagerDisplayName && nullptr == m_ColumnManagerUserPrincipalName);
	if (nullptr != m_ColumnManagerID)
	{
		const auto& field = p_Row->GetField(m_ColumnManagerID);
		if (hasValidErrorlessValue(field))
			p_BU.SetManagerID(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnMobilePhone)
	{
		const auto& field = p_Row->GetField(m_ColumnMobilePhone);
		if (hasValidErrorlessValue(field))
			p_BU.SetMobilePhone(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnMySite)
	{
		const auto& field = p_Row->GetField(m_ColumnMySite);
		if (hasValidErrorlessValue(field))
			p_BU.SetMySite(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnOfficeLocation)
	{
		const auto& field = p_Row->GetField(m_ColumnOfficeLocation);
		if (hasValidErrorlessValue(field))
			p_BU.SetOfficeLocation(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnOPImmutableId)
	{
		const auto& field = p_Row->GetField(m_ColumnOPImmutableId);
		if (hasValidErrorlessValue(field))
			p_BU.SetOnPremisesImmutableId(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnOPLastSync)
	{
		const auto& field = p_Row->GetField(m_ColumnOPLastSync);
		if (hasValidErrorlessValue(field))
			p_BU.SetOnPremisesLastSyncDateTime(field.GetValueTimeDate());
	}

	if (nullptr != m_ColumnOPSecurityID)
	{
		const auto& field = p_Row->GetField(m_ColumnOPSecurityID);
		if (hasValidErrorlessValue(field))
			p_BU.SetOnPremisesSecurityIdentifier(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnOPSyncEnabled)
	{
		const auto& field = p_Row->GetField(m_ColumnOPSyncEnabled);
		if (hasValidErrorlessValue(field))
			p_BU.SetOnPremisesSyncEnabled(field.GetValueBool());
	}

	if (nullptr != m_ColumnPasswordPolicies)
	{
		const auto& field = p_Row->GetField(m_ColumnPasswordPolicies);
		if (hasValidErrorlessValue(field))
			p_BU.SetPasswordPolicies(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnPassword)
	{
		const auto& field = p_Row->GetField(m_ColumnPassword);
		if (hasValidErrorlessValue(field))
			p_BU.SetPassword(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnForceChangePassword)
	{
	    const auto& field = p_Row->GetField(m_ColumnForceChangePassword);
		if (hasValidErrorlessValue(field))
            p_BU.SetForceChangePassword(field.GetValueBool());
	}

	if (nullptr != m_ColumnForceChangePasswordWithMfa)
	{
		const auto& field = p_Row->GetField(m_ColumnForceChangePasswordWithMfa);
		if (hasValidErrorlessValue(field))
			p_BU.SetForceChangePasswordWithMfa(field.GetValueBool());
	}

	if (nullptr != m_ColumnPastProjects)
	{
		const auto& field = p_Row->GetField(m_ColumnPastProjects);
		if (hasValidErrorlessValue(field))
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				p_BU.SetPastProjects(*field.GetValuesMulti<PooledString>());
			else
				p_BU.SetPastProjects(vector<PooledString>{ field.GetValueStr() });
		}
	}

	if (nullptr != m_ColumnPostalCode)
	{
		const auto& field = p_Row->GetField(m_ColumnPostalCode);
		if (hasValidErrorlessValue(field))
			p_BU.SetPostalCode(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnPreferredLanguage)
	{
		const auto& field = p_Row->GetField(m_ColumnPreferredLanguage);
		if (hasValidErrorlessValue(field))
			p_BU.SetPreferredLanguage(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnPreferredName)
	{
		const auto& field = p_Row->GetField(m_ColumnPreferredName);
		if (hasValidErrorlessValue(field))
			p_BU.SetPreferredName(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnProxyAddresses)
	{
		const auto& field = p_Row->GetField(m_ColumnProxyAddresses);
		if (hasValidErrorlessValue(field))
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				p_BU.SetProxyAddresses(*field.GetValuesMulti<PooledString>());
			else
				p_BU.SetProxyAddresses(vector<PooledString>{ field.GetValueStr() });
		}
	}

	if (nullptr != m_ColumnResponsibilities)
	{
		const auto& field = p_Row->GetField(m_ColumnResponsibilities);
		if (hasValidErrorlessValue(field))
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				p_BU.SetResponsibilities(*field.GetValuesMulti<PooledString>());
			else
				p_BU.SetResponsibilities(vector<PooledString>{ field.GetValueStr() });
		}
	}

	if (nullptr != m_ColumnSchools)
	{
		const auto& field = p_Row->GetField(m_ColumnSchools);
		if (hasValidErrorlessValue(field))
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				p_BU.SetSchools(*field.GetValuesMulti<PooledString>());
			else
				p_BU.SetSchools(vector<PooledString>{ field.GetValueStr() });
		}
	}

	if (nullptr != m_ColumnSkills)
	{
		const auto& field = p_Row->GetField(m_ColumnSkills);
		if (hasValidErrorlessValue(field))
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				p_BU.SetSkills(*field.GetValuesMulti<PooledString>());
			else
				p_BU.SetSkills(vector<PooledString>{ field.GetValueStr() });
		}
	}

	if (nullptr != m_ColumnState)
	{
		const auto& field = p_Row->GetField(m_ColumnState);
		if (hasValidErrorlessValue(field))
			p_BU.SetState(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnStreetAddress)
	{
		const auto& field = p_Row->GetField(m_ColumnStreetAddress);
		if (hasValidErrorlessValue(field))
			p_BU.SetStreetAddress(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnSurname)
	{
		const auto& field = p_Row->GetField(m_ColumnSurname);
		if (hasValidErrorlessValue(field))
			p_BU.SetSurname(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnUsageLocation)
	{
		const auto& field = p_Row->GetField(m_ColumnUsageLocation);
		if (hasValidErrorlessValue(field))
			p_BU.SetUsageLocation(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnUserPrincipalName)
	{
		const auto& field = p_Row->GetField(m_ColumnUserPrincipalName);
		if (hasValidErrorlessValue(field))
			p_BU.SetUserPrincipalName(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnUserType)
	{
		const auto& field = p_Row->GetField(m_ColumnUserType);
		if (hasValidErrorlessValue(field))
			p_BU.SetUserType(PooledString(field.GetValueStr()));
	}
	else
	{
		if (GridUtil::IsBusinessUser(p_Row, false))
		{
			p_BU.SetUserType(PooledString(BusinessUser::g_UserTypeMember));
		}
		else if (GridUtil::IsBusinessUser(p_Row, true))
		{
			p_BU.SetUserType(PooledString(BusinessUser::g_UserTypeGuest));
		}
		else
		{
			ASSERT(false);
		}
	}

	if (nullptr != m_ColumnDeletedDateTime)
	{
		const auto& field = p_Row->GetField(m_ColumnDeletedDateTime);
		if (hasValidErrorlessValue(field))
			p_BU.SetDeletedDateTime(field.GetValueTimeDate());
	}

	if (nullptr != m_ColumnSkuIds && nullptr != m_ColumnSkuPartNumbers)
	{
		const auto& fieldIds = p_Row->GetField(m_ColumnSkuIds);
		const auto& fieldPNs = p_Row->GetField(m_ColumnSkuPartNumbers);
		if (hasValidErrorlessValue(fieldPNs))
		{
			if (fieldPNs.IsMulti() && nullptr != fieldPNs.GetValuesMulti<PooledString>())
			{
				ASSERT(nullptr != fieldIds.GetValuesMulti<PooledString>() && fieldIds.GetValuesMulti<PooledString>()->size() == fieldPNs.GetValuesMulti<PooledString>()->size());
				vector<BusinessAssignedLicense> assignedLicences;
				for (size_t i = 0; i < fieldPNs.GetValuesMulti<PooledString>()->size(); ++i)
				{
					assignedLicences.emplace_back();
					assignedLicences.back().SetSkuPartNumber((*fieldPNs.GetValuesMulti<PooledString>())[i]);
					assignedLicences.back().SetID((*fieldIds.GetValuesMulti<PooledString>())[i]);
				}
				p_BU.SetAssignedLicenses(assignedLicences);
			}
			else
			{
				if (BusinessUserConfiguration::GetInstance().GetValueStringLicenses_Unlicensed() != fieldPNs.GetValueStr())
				{
					BusinessAssignedLicense assignedLicense;
					assignedLicense.SetSkuPartNumber(PooledString(fieldPNs.GetValueStr()));
					assignedLicense.SetID(fieldIds.GetValueStr());
					p_BU.SetAssignedLicenses(vector<BusinessAssignedLicense>{ assignedLicense });
				}
				else
				{
					p_BU.SetAssignedLicenses(vector<BusinessAssignedLicense>{ });
				}
			}
		}
	}

	if (nullptr != m_ColumnAgeGroup)
	{
		const auto& field = p_Row->GetField(m_ColumnAgeGroup);
		if (hasValidErrorlessValue(field))
			p_BU.SetAgeGroup(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnCreatedDateTime)
	{
		const auto& field = p_Row->GetField(m_ColumnCreatedDateTime);
		if (hasValidErrorlessValue(field))
			p_BU.SetCreatedDateTime(field.GetValueTimeDate());
	}

	if (nullptr != m_ColumnConsentProvidedForMinor)
	{
		const auto& field = p_Row->GetField(m_ColumnConsentProvidedForMinor);
		if (hasValidErrorlessValue(field))
			p_BU.SetConsentProvidedForMinor(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnLegalAgeGroupClassification)
	{
		const auto& field = p_Row->GetField(m_ColumnLegalAgeGroupClassification);
		if (hasValidErrorlessValue(field))
			p_BU.SetLegalAgeGroupClassification(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnSignInSessionsValidFromDateTime)
	{
		const auto& field = p_Row->GetField(m_ColumnSignInSessionsValidFromDateTime);
		if (hasValidErrorlessValue(field))
			p_BU.SetSignInSessionsValidFromDateTime(field.GetValueTimeDate());
	}

	if (nullptr != m_ColumnOPDomainName)
	{
		const auto& field = p_Row->GetField(m_ColumnOPDomainName);
		if (hasValidErrorlessValue(field))
			p_BU.SetOnPremisesDomainName(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnOPSamAccountName)
	{
		const auto& field = p_Row->GetField(m_ColumnOPSamAccountName);
		if (hasValidErrorlessValue(field))
			p_BU.SetOnPremisesSamAccountName(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnOPUserPrincipalName)
	{
		const auto& field = p_Row->GetField(m_ColumnOPUserPrincipalName);
		if (hasValidErrorlessValue(field))
			p_BU.SetOnPremisesUserPrincipalName(PooledString(field.GetValueStr()));
	}

	// handle this?
	/*
	GridBackendColumn* m_ColumnOPProvisioningErrorsCategory;
	GridBackendColumn* m_ColumnOPProvisioningErrorsOccuredDateTime;
	GridBackendColumn* m_ColumnOPProvisioningErrorsPropertyCausingError;
	GridBackendColumn* m_ColumnOPProvisioningErrorsValue;
	*/

	if (nullptr != m_ColumnOtherMails)
	{
		const auto& field = p_Row->GetField(m_ColumnOtherMails);
		if (hasValidErrorlessValue(field))
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				p_BU.SetOtherMails(*field.GetValuesMulti<PooledString>());
			else
				p_BU.SetOtherMails(vector<PooledString>{ field.GetValueStr() });
		}
	}

	if (nullptr != m_ColumnPreferredDataLocation)
	{
		const auto& field = p_Row->GetField(m_ColumnPreferredDataLocation);
		if (hasValidErrorlessValue(field))
			p_BU.SetPreferredDataLocation(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnShowInAddressList)
	{
		const auto& field = p_Row->GetField(m_ColumnShowInAddressList);
		if (hasValidErrorlessValue(field))
			p_BU.SetShowInAddressList(field.GetValueBool());
	}

	if (nullptr != m_ColumnExternalUserStatus)
	{
		const auto& field = p_Row->GetField(m_ColumnExternalUserStatus);
		if (hasValidErrorlessValue(field))
			p_BU.SetExternalUserStatus(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnExternalUserStatusChangedOn)
	{
		const auto& field = p_Row->GetField(m_ColumnExternalUserStatusChangedOn);
		if (hasValidErrorlessValue(field))
			p_BU.SetExternalUserStatusChangedOn(field.GetValueTimeDate());
	}

	{
		OnPremisesExtensionAttributes onPremisesExtensionAttributes;
		std::vector<boost::YOpt<PooledString> OnPremisesExtensionAttributes::*> attributes
		{
				&OnPremisesExtensionAttributes::m_ExtensionAttribute1
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute2
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute3
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute4
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute5
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute6
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute7
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute8
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute9
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute10
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute11
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute12
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute13
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute14
			,	&OnPremisesExtensionAttributes::m_ExtensionAttribute15
		};
		for (int i = 0; i < g_NumOPExtensionAttributes; ++i)
		{
			auto col = GetOPAttributeColumn(i);
			if (nullptr != col)
			{
				const auto& field = p_Row->GetField(col);
				if (hasValidErrorlessValue(field))
					onPremisesExtensionAttributes.*(attributes[i]) = PooledString(field.GetValueStr());
			}
		}

		p_BU.SetOnPremisesExtensionAttributes(onPremisesExtensionAttributes);
	}

	if (nullptr != m_ColumnEmployeeID)
	{
		const auto& field = p_Row->GetField(m_ColumnEmployeeID);
		if (hasValidErrorlessValue(field))
			p_BU.SetEmployeeID(PooledString(field.GetValueStr()));
	}
	
	if (nullptr != m_ColumnFaxNumber)
	{
		const auto& field = p_Row->GetField(m_ColumnFaxNumber);
		if (hasValidErrorlessValue(field))
			p_BU.SetFaxNumber(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnIsResourceAccount)
	{
		const auto& field = p_Row->GetField(m_ColumnIsResourceAccount);
		if (hasValidErrorlessValue(field))
			p_BU.SetIsResourceAccount(field.GetValueBool());
	}

	if (nullptr != m_ColumnOPDistinguishedName)
	{
		const auto& field = p_Row->GetField(m_ColumnOPDistinguishedName);
		if (hasValidErrorlessValue(field))
			p_BU.SetOnPremisesDistinguishedName(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnCreationType)
	{
		const auto& field = p_Row->GetField(m_ColumnCreationType);
		if (hasValidErrorlessValue(field))
			p_BU.SetCreationType(PooledString(field.GetValueStr()));
	}

	// FIXME: Handle Identities
	//if (nullptr != m_ColumnIdentitiesSignInType)
	//if (nullptr != m_ColumnIdentitiesIssuer)
	//if (nullptr != m_ColumnIdentitiesIssuerAssignedId)

	if (nullptr != m_ColumnLastPasswordChangeDateTime)
	{
		const auto& field = p_Row->GetField(m_ColumnLastPasswordChangeDateTime);
		if (hasValidErrorlessValue(field))
			p_BU.SetLastPasswordChangeDateTime(field.GetValueTimeDate());
	}

	// FIXME: Handle LicenseAssignmentStates
	//if (nullptr != m_ColumnLicenseAssignmentStatesAssignedByGroupId)
	//if (nullptr != m_ColumnLicenseAssignmentStatesAssignedByGroupDisplayName)
	//if (nullptr != m_ColumnLicenseAssignmentStatesAssignedByGroupMail)
	//if (nullptr != m_ColumnLicenseAssignmentStatesState)
	//if (nullptr != m_ColumnLicenseAssignmentStatesError)
	//if (nullptr != m_ColumnLicenseAssignmentStatesSkuId)
	//if (nullptr != m_ColumnLicenseAssignmentStatesSkuPartNumber)
	//if (nullptr != m_ColumnLicenseAssignmentStatesSkuLabel)
	//if (nullptr != m_ColumnIsAnyLicenseAssignedByGroup)

	if (nullptr != m_ColumnRefresTokenValidFromDateTime)
	{
		const auto& field = p_Row->GetField(m_ColumnRefresTokenValidFromDateTime);
		if (hasValidErrorlessValue(field))
			p_BU.SetRefresTokenValidFromDateTime(field.GetValueTimeDate());
	}

	if (nullptr != m_ColumnMailboxType)
	{
		const auto& field = p_Row->GetField(m_ColumnMailboxType);
		if (hasValidErrorlessValue(field))
			p_BU.SetMailboxType(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnMailboxDeliverToMailboxAndForward)
	{
		const auto& field = p_Row->GetField(m_ColumnMailboxDeliverToMailboxAndForward);
		if (hasValidErrorlessValue(field))
			p_BU.SetMailboxDeliverToMailboxAndForward(field.GetValueBool());
	}

	if (nullptr != m_ColumnMailboxForwardingSmtpAddress)
	{
		const auto& field = p_Row->GetField(m_ColumnMailboxForwardingSmtpAddress);
		if (hasValidErrorlessValue(field))
			p_BU.SetMailboxForwardingSmtpAddress(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnMailboxForwardingAddress)
	{
		const auto& field = p_Row->GetField(m_ColumnMailboxForwardingAddress);
		if (hasValidErrorlessValue(field))
			p_BU.SetMailboxForwardingAddress(PooledString(field.GetValueStr()));
	}

	if (nullptr != m_ColumnMailboxCanSendOnBehalf)
	{
		const auto& field = p_Row->GetField(m_ColumnMailboxCanSendOnBehalf);
		if (hasValidErrorlessValue(field))
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				p_BU.SetMailboxCanSendOnBehalf(*field.GetValuesMulti<PooledString>());
			else
				p_BU.SetMailboxCanSendOnBehalf(vector<PooledString>{ field.GetValueStr() });
		}
	}

	if (nullptr != m_ColumnRecipientPermissionsCanSendAsUser)
	{
		const auto& field = p_Row->GetField(m_ColumnRecipientPermissionsCanSendAsUser);
		if (hasValidErrorlessValue(field))
		{
			if (field.IsMulti() && nullptr != field.GetValuesMulti<PooledString>())
				p_BU.SetMailboxCanSendAsUser(*field.GetValuesMulti<PooledString>());
			else
				p_BU.SetMailboxCanSendAsUser(vector<PooledString>{ field.GetValueStr() });
		}
	}

	for (const auto& item : m_ColumnsRequestDateTime)
	{
		if (nullptr != item.second)
		{
			const auto& field = p_Row->GetField(item.second);
			if (hasValidErrorlessValue(field) && field.IsDate())
			{
				p_BU.SetDataDate(item.first, field.GetValueTimeDate());
			}
		}
	}

	if (p_BU.GetOnPremiseUser())
		GetOnPremiseUser(p_Grid, p_Row, *p_BU.GetOnPremiseUser());
}

bool GridTemplateUsers::GetOnPremiseUser(const O365Grid& p_Grid, GridBackendRow* p_Row, OnPremiseUser& p_OnPremUser) const
{
	if (nullptr != p_Row
		&& (IsOnPremMetatype(*p_Row) || IsHybridMetatype(*p_Row)))
	{
		for (auto c : m_OnPremCols.GetColumns())
			m_OnPremCols.SetValue(p_OnPremUser, c, p_Row->GetField(c));
		return true;
	}

	return false;
}

bool GridTemplateUsers::IsShowCachedUsersOnly() const
{
	return m_ShowCachedUsersOnly;
}

bool GridTemplateUsers::RemoveRow(O365Grid& p_Grid, const BusinessUser& p_BU)
{
	bool removed = false;

	vector< GridBackendRow* > ListRows;
	p_Grid.GetAllNonGroupRows(ListRows);
	for (const auto& Row : ListRows)
	{
		const GridBackendField& IDField = Row->GetField(m_ColumnID);
		if (IDField.GetValueStr() == (wstring)p_BU.GetID())
		{
			removed = p_Grid.RemoveRow(Row);
			break;
		}
	}

	return removed;
}

bool GridTemplateUsers::GetBusinessUserPK(O365Grid& p_Grid, const BusinessUser& p_BU, vector<GridBackendField>& BUpk) const
{
	BUpk.clear();

	vector<GridBackendColumn*> pkColumns;
	p_Grid.GetPKColumns(pkColumns, false);
	ASSERT(!pkColumns.empty());

	for (auto c : pkColumns)
	{
		ASSERT(nullptr != c);
		if (nullptr != c)
		{
			const wstring& UID = c->GetUniqueID();
			if (nullptr != m_ColumnID && UID == m_ColumnID->GetUniqueID() || UID == _YUID(O365_ID))
				BUpk.emplace_back(p_BU.GetID(), c->GetID());
			else if ((nullptr != m_ColumnUserType && UID == m_ColumnUserType->GetUniqueID() || UID == _YUID(O365_USER_USERTYPE)) && p_BU.GetUserType().is_initialized())
			{
				ASSERT(false); // Has been removed from PK
				BUpk.emplace_back(p_BU.GetUserType().get(), c->GetID());
			}
			// add other pk fields here (those that are set in columns added as grid row pk via AddColumnForRowPK) - this is grid-dependent, cannot use business object metadata
		}
	}
	ASSERT(!BUpk.empty());
	return !BUpk.empty();
}

constexpr GridBackendColumn*& GridTemplateUsers::GetOPAttributeColumn(int p_Index)
{
	return this->*(m_ColumnsOPExtensionAttributes[p_Index]);
}

GridBackendColumn*& GridTemplateUsers::GetColumnForProperty(const wstring& p_PropertyName)
{
	return getColumnForProperty(*this, p_PropertyName);
}

GridBackendColumn* GridTemplateUsers::AddDefaultColumnFor(O365Grid& p_Grid, const wstring& p_PropertyName, const wstring& p_FamilyName, const set<GridBackendUtil::COLUMNPRESETFLAGS>& p_Flags)
{
	return AddDefaultColumnFor(p_Grid, p_PropertyName, p_FamilyName, p_Flags, {}, {});
}

GridBackendColumn* GridTemplateUsers::AddDefaultColumnFor(O365Grid& p_Grid, const wstring& p_PropertyName, const wstring& p_FamilyName, const set<GridBackendUtil::COLUMNPRESETFLAGS>& p_Flags, const wstring& p_CustomName, const wstring& p_CustomUniqueID)
{
	auto& col = GetColumnForProperty(p_PropertyName);
	const auto& ucfg = BusinessUserConfiguration::GetInstance();

	wstring title;
	if (p_CustomName.empty())
		title = ucfg.GetTitle(p_PropertyName);
	else if (Str::contains(p_CustomName, _YTEXT("%s")))
		title = _YTEXTFORMAT(p_CustomName.c_str(), ucfg.GetTitle(p_PropertyName).c_str());
	else
		title = p_CustomName;

	wstring uniqueID;
	if (p_CustomUniqueID.empty())
		uniqueID = p_PropertyName;
	else if (Str::contains(p_CustomUniqueID, _YTEXT("%s")))
		uniqueID = _YTEXTFORMAT(p_CustomUniqueID.c_str(), p_PropertyName.c_str());
	else
		uniqueID = p_CustomUniqueID;

	if (p_PropertyName == _YUID(O365_USER_DISPLAYNAME)
		|| p_PropertyName == _YUID(O365_USER_MAILNICKNAME)
		|| p_PropertyName == _YUID(O365_USER_PROXYADDRESSES)
		|| p_PropertyName == _YUID(O365_USER_IMADDRESSES)
		|| p_PropertyName == _YUID(O365_USER_GIVENNAME)
		|| p_PropertyName == _YUID(O365_USER_SURNAME)
		|| p_PropertyName == _YUID("manager.displayName")
		|| p_PropertyName == _YUID(O365_USER_COMPANYNAME)
		|| p_PropertyName == _YUID(O365_USER_STREETADDRESS)
		|| p_PropertyName == _YUID(O365_USER_SKILLS)
		|| p_PropertyName == _YUID(O365_USER_SCHOOLS)
		|| p_PropertyName == _YUID(O365_USER_INTERESTS)
		|| p_PropertyName == _YUID(O365_USER_PREFERREDNAME)
		|| p_PropertyName == _YUID(O365_USER_ONPREMISESDISTINGUISHEDNAME)
		|| p_PropertyName == _YUID(O365_USER_ONPREMISESDOMAINNAME)
		|| p_PropertyName == _YUID(O365_USER_ONPREMISESSAMACCOUNTNAME)
		|| p_PropertyName == _YUID(O365_USER_ONPREMISESUSERPRINCIPALNAME)
		|| p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_TIMEZONENAME))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize22, p_Flags);
	else if (p_PropertyName == _YUID("metaType"))
	{
		col = p_Grid.AddColumnIcon(uniqueID, title, p_FamilyName);
		col->SetPresetFlags(p_Flags);
	}
	else if (	p_PropertyName == _YUID(O365_USER_USERPRINCIPALNAME)
			||	p_PropertyName == _YUID(O365_USER_MAIL)
			||	p_PropertyName == _YUID(O365_USER_OTHERMAILS)
			||	p_PropertyName == _YUID("manager.userPrincipalName")
			||	p_PropertyName == _YUID(O365_USER_PASTPROJECTS)
			||	p_PropertyName == _YUID(O365_USER_CREATIONTYPE)
			||	p_PropertyName == _YUID(O365_USER_MAILBOX_FORWARDINGSMTPADDRESS))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize32, p_Flags);
	else if (	p_PropertyName == _YUID(O365_USER_PASSWORDPOLICIES)
			||	p_PropertyName == _YUID(O365_USER_PASSWORDPROFILE_PASSWORD)
			||	p_PropertyName == _YUID(O365_USER_EMPLOYEEID)
			||	p_PropertyName == _YUID(O365_USER_JOBTITLE)
			||	p_PropertyName == _YUID(O365_USER_DEPARTMENT)
			||	p_PropertyName == _YUID(O365_USER_BUSINESSPHONES)
			||	p_PropertyName == _YUID(O365_USER_MOBILEPHONE)
			||	p_PropertyName == _YUID(O365_USER_FAXNUMBER)
			||	p_PropertyName == _YUID(O365_USER_OFFICELOCATION)
			||	p_PropertyName == _YUID(O365_USER_CITY)
			||	p_PropertyName == _YUID(O365_USER_STATE)
			||	p_PropertyName == _YUID(O365_USER_POSTALCODE)
			||	p_PropertyName == _YUID(O365_USER_COUNTRY)
			||	p_PropertyName == _YUID(O365_USER_PREFERREDLANGUAGE)
			||	p_PropertyName == _YUID(O365_USER_USAGELOCATION)
			||	p_PropertyName == _YUID(O365_USER_ABOUTME)
			||	p_PropertyName == _YUID(O365_USER_RESPONSIBILITIES)
			||	p_PropertyName == _YUID(O365_USER_PREFERREDDATALOCATION)
			||	p_PropertyName == _YUID(O365_USER_AGEGROUP)
			||	p_PropertyName == _YUID(O365_USER_CONSENTPROVIDEDFORMINOR)
			||	p_PropertyName == _YUID(O365_USER_LEGALAGEGROUPCLASSIFICATION)
			||	p_PropertyName == _YUID(O365_USER_ONPREMISESIMMUTABLEID)
			||	p_PropertyName == _YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER)
			||	p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY)
			||	p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR)
			||	p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE)
			||	p_PropertyName == _YUID(O365_COLDRIVENAME)
			||	p_PropertyName == _YUID(O365_COLDRIVEID)
			||	p_PropertyName == _YUID(O365_COLDRIVEDESCRIPTION)
			||	p_PropertyName == _YUID(O365_COLDRIVEQUOTASTATE)
			||	p_PropertyName == _YUID(O365_COLDRIVETYPE)
			||	p_PropertyName == _YUID(O365_COLDRIVECREATEDBYUSEREMAIL)
			||	p_PropertyName == _YUID(O365_COLDRIVECREATEDBYUSERID)
			||	p_PropertyName == _YUID(O365_COLDRIVECREATEDBYAPPNAME)
			||	p_PropertyName == _YUID(O365_COLDRIVECREATEDBYAPPID)
			||	p_PropertyName == _YUID(O365_COLDRIVECREATEDBYDEVICENAME)
			||	p_PropertyName == _YUID(O365_COLDRIVECREATEDBYDEVICEID)
			||	p_PropertyName == _YUID(O365_COLDRIVEOWNERUSERNAME)
			||	p_PropertyName == _YUID(O365_COLDRIVEOWNERUSERID)
			||	p_PropertyName == _YUID(O365_COLDRIVEOWNERUSEREMAIL)
			||	p_PropertyName == _YUID(O365_COLDRIVEOWNERAPPNAME)
			||	p_PropertyName == _YUID(O365_COLDRIVEOWNERAPPID)
			||	p_PropertyName == _YUID(O365_COLDRIVEOWNERDEVICENAME)
			||	p_PropertyName == _YUID(O365_COLDRIVEOWNERDEVICEID)
			||	Str::beginsWith(p_PropertyName, _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute"))
			||	p_PropertyName == _YUID(O365_USER_EXTERNALUSERSTATUS)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_ARCHIVEFOLDERNAME)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_ARCHIVEFOLDER)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_LANGUAGE_DISPLAYNAME)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_STATUS)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_EXTERNALAUDIENCE)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDSTARTDATETIME_TIMEZONE)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDENDDATETIME_TIMEZONE)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_TIMEFORMAT)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_DATEFORMAT)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_DELEGATEMEETINGMESSAGEDELIVERYOPTIONS)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_DAYSOFWEEK)
			||	p_PropertyName == _YUID(O365_USER_MAILBOX_TYPE)
			||  p_PropertyName == _YUID(O365_USER_MAILBOX_FORWARDINGADDRESS)
			|| p_PropertyName == _YUID(O365_USER_MAILBOX_CAN_SEND_ON_BEHALF)
			|| p_PropertyName == _YUID(O365_USER_MAILBOX_CAN_SEND_AS_USER)
		)
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (	p_PropertyName == _YUID(O365_USER_PASSWORDPROFILE_FORCECHANGE)
			||	p_PropertyName == _YUID(O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA)
			||	p_PropertyName == _YUID(O365_USER_ISRESOURCEACCOUNT)
			||	p_PropertyName == _YUID(O365_USER_SHOWINADDRESSLIST)
			||	p_PropertyName == _YUID(O365_USER_ONPREMISESSYNCENABLED)
			|| p_PropertyName == _YUID(O365_USER_MAILBOX_DELIVERTOMAILBOXANDFORWARD))
	{
		col = p_Grid.AddColumnCheckBox(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize6, p_Flags);
		col->SetPresetFlags(p_Flags);
	}
	else if (	p_PropertyName == _YUID(O365_USER_USERTYPE)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_TIMEZONE)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_LANGUAGE_LOCALE))
		col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize8, p_Flags);
	else if (p_PropertyName == _YUID(O365_USER_ACCOUNTENABLED))
	{
		col = p_Grid.AddColumnIcon(uniqueID, title, p_FamilyName);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID(O365_USER_ASSIGNEDLICENSES) || p_PropertyName == _YUID("assignedLicenses.skuId") || p_PropertyName == _YUID("ASSIGNEDLICENSENAMES"))
	{
		if (p_PropertyName == _YUID(O365_USER_ASSIGNEDLICENSES))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize32, p_Flags);
		else if (p_PropertyName == _YUID("ASSIGNEDLICENSENAMES"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize32, p_Flags);
		else if (p_PropertyName == _YUID("assignedLicenses.skuId"))
			col = p_Grid.AddColumnCaseSensitive(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);

		std::vector<GridBackendColumn*> cols{ m_ColumnSkuPartNumbers, m_ColumnSkuNames, m_ColumnSkuIds };
		cols.erase(std::remove_if(cols.begin(), cols.end(), [](auto& c) {return nullptr == c; }), cols.end());
		if (cols.size() > 1)
		{
			for (auto c : cols)
				c->ClearExplosionSisters();

			cols[0]->SetMultivalueFamily(YtriaTranslate::Do(GridUsers_customizeGrid_61, _YLOC("Assigned Licenses")).c_str());
			for (size_t i = 1; i < cols.size(); ++i)
				cols[0]->AddMultiValueExplosionSister(cols[i]);
		}
	}
	else if (p_PropertyName == _YUID(O365_USER_MANAGER_ID))
		col = p_Grid.AddColumnCaseSensitive(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_USER_MYSITE))
		col = p_Grid.AddColumnHyperlink(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize8, p_Flags);
	else if (	p_PropertyName == _YUID(O365_USER_CREATEDDATETIME)
			||	p_PropertyName == _YUID(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME)
			||	p_PropertyName == _YUID(O365_USER_ONPREMISESLASTSYNCDATETIME)
			||	p_PropertyName == _YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME)
			||	p_PropertyName == _YUID(O365_USER_EXTERNALUSERSTATUSCHANGEDON)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDSTARTDATETIME_DATETIME)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDENDDATETIME_DATETIME)
			||	p_PropertyName == _YUID(O365_USER_LASTPASSWORDCHANGEDATETIME)
			||	p_PropertyName == _YUID(O365_USER_REFRESTOKENVALIDFROMDATETIME))
		col = p_Grid.AddColumnDate(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize16, p_Flags);
	else if (	p_PropertyName == _YUID(O365_USER_BIRTHDAY)
			||	p_PropertyName == _YUID(O365_USER_HIREDATE))
	{
		col = p_Grid.AddColumnDate(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize16, p_Flags);
		p_Grid.SetColumnDateFormatNoTime(col);
	}
	else if (	p_PropertyName == _YUID(O365_COLDRIVEQUOTAUSED)
			||	p_PropertyName == _YUID(O365_COLDRIVEQUOTAREMAINING)
			||	p_PropertyName == _YUID(O365_COLDRIVEQUOTADELETED)
			||	p_PropertyName == _YUID(O365_COLDRIVEQUOTATOTAL))
	{
		col = p_Grid.AddColumnNumber(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		col->SetXBytesColumnFormat();
	}
	else if	(	p_PropertyName == _YUID(O365_COLDRIVECREATIONTIME)
			||	p_PropertyName == _YUID(O365_COLDRIVELASTMODIFIEDDATETIME))
		col = p_Grid.AddColumnDate(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (p_PropertyName == _YUID(O365_COLDRIVEWEBURL))
		col = p_Grid.AddColumnHyperlink(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
	else if (	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_EXTERNALREPLYMESSAGE)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_INTERNALREPLYMESSAGE))
		col = p_Grid.AddColumnRichText(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize22, p_Flags);
	else if (	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_STARTTIME)
			||	p_PropertyName == _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_ENDTIME))
	{
		col = p_Grid.AddColumnDate(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize22, p_Flags);
		p_Grid.SetColumnDateFormatNoDate(col);
	}
	else if (p_PropertyName == _YUID("identities.signInType") 
			|| p_PropertyName == _YUID("identities.issuer") 
			|| p_PropertyName == _YUID("identities.issuerAssignedId"))
	{
		if (p_PropertyName == _YUID("identities.signInType"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		else if (p_PropertyName == _YUID("identities.issuer"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		else if (p_PropertyName == _YUID("identities.issuerAssignedId"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);

		std::vector<GridBackendColumn*> cols{ m_ColumnIdentitiesSignInType, m_ColumnIdentitiesIssuer, m_ColumnIdentitiesIssuerAssignedId };
		cols.erase(std::remove_if(cols.begin(), cols.end(), [](auto& c) {return nullptr == c; }), cols.end());
		if (cols.size() > 1)
		{
			for (auto c : cols)
				c->ClearExplosionSisters();

			cols[0]->SetMultivalueFamily(_T("Identities"));
			for (size_t i = 1; i < cols.size(); ++i)
				cols[0]->AddMultiValueExplosionSister(cols[i]);
		}
	}
	else if (p_PropertyName == _YUID("GROUPLICENSINGMEMBER"))
	{
		col = p_Grid.AddColumnIcon(uniqueID, title, p_FamilyName);
		col->SetPresetFlags(p_Flags);
	}
	else if (p_PropertyName == _YUID("licenseAssignmentStates.skuId")
		|| p_PropertyName == _YUID("licenseAssignmentStates.assignedByGroup")
		|| p_PropertyName == _YUID("ASSIGNEDBYGROUPNAME")
		|| p_PropertyName == _YUID("ASSIGNEDBYGROUPEMAIL")
		|| p_PropertyName == _YUID("licenseAssignmentStates.state")
		|| p_PropertyName == _YUID("licenseAssignmentStates.error")
		|| p_PropertyName == _YUID("licenseAssignmentStates.skuId")
		|| p_PropertyName == _YUID("ASSIGNEDSKUPARTNUMBER")
		|| p_PropertyName == _YUID("ASSIGNEDBYGROUPLICENSENAME"))
	{
		if (p_PropertyName == _YUID("licenseAssignmentStates.skuId"))
			col = p_Grid.AddColumnCaseSensitive(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		else if (p_PropertyName == _YUID("licenseAssignmentStates.assignedByGroup"))
			col = p_Grid.AddColumnCaseSensitive(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		else if (p_PropertyName == _YUID("ASSIGNEDBYGROUPNAME"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize22, p_Flags);
		else if (p_PropertyName == _YUID("ASSIGNEDBYGROUPEMAIL"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		else if (p_PropertyName == _YUID("licenseAssignmentStates.state"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		else if (p_PropertyName == _YUID("licenseAssignmentStates.error"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		else if (p_PropertyName == _YUID("licenseAssignmentStates.skuId"))
			col = p_Grid.AddColumnCaseSensitive(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize12, p_Flags);
		else if (p_PropertyName == _YUID("ASSIGNEDSKUPARTNUMBER"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize32, p_Flags);
		else if (p_PropertyName == _YUID("ASSIGNEDBYGROUPLICENSENAME"))
			col = p_Grid.AddColumn(uniqueID, title, p_FamilyName, CacheGrid::g_ColumnSize32, p_Flags);

		std::vector<GridBackendColumn*> cols
			{ m_ColumnLicenseAssignmentStatesSkuId
			, m_ColumnLicenseAssignmentStatesSkuPartNumber
			, m_ColumnLicenseAssignmentStatesSkuName
			, m_ColumnLicenseAssignmentStatesAssignedByGroupId
			, m_ColumnLicenseAssignmentStatesAssignedByGroupDisplayName
			, m_ColumnLicenseAssignmentStatesAssignedByGroupMail
			, m_ColumnLicenseAssignmentStatesState
			, m_ColumnLicenseAssignmentStatesError };
		cols.erase(std::remove_if(cols.begin(), cols.end(), [](auto& c) {return nullptr == c; }), cols.end());
		if (cols.size() > 1)
		{
			for (auto c : cols)
				c->ClearExplosionSisters();

			cols[0]->SetMultivalueFamily(_T("Assigned Licences States"));
			for (size_t i = 1; i < cols.size(); ++i)
				cols[0]->AddMultiValueExplosionSister(cols[i]);
		}
	}
	else
	{
		ASSERT(false);
		ASSERT(nullptr == col);
	}

	return col;
}

void GridTemplateUsers::CustomizeGrid(O365Grid& p_Grid)
{
	GridTemplate::CustomizeGrid(p_Grid);

	m_AllowedIconId = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ALLOWED_SIGNIN));
	m_BlockedIconId = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_BLOCKED_SIGNIN));
	m_GroupLicensingIconId = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_GROUP));
	m_MailboxLoadedIconId = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_LOADED_MAILBOX_INFO));
	m_MailboxNotLoadedIconId = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_NOTLOADED_MAILBOX_INFO));
}

void GridTemplateUsers::MarkUserRowWithRevokePending(O365Grid& p_Grid, GridBackendRow* p_Row, GridUpdater& p_Updater)
{
	ASSERT(nullptr != p_Row);
	updateField(RevokeAccessFieldUpdate::GetPlaceHolder(), p_Row, m_ColumnSignInSessionsValidFromDateTime, UpdateFieldOption::IS_MODIFICATION);
}

bool GridTemplateUsers::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	if (p_Field.HasValue())
	{
		if (&p_Column == m_ColumnAccountEnabled)
		{
			if (p_Field.GetIcon() == m_AllowedIconId)
				p_Value = _YTEXT("1");
			else if (p_Field.GetIcon() == m_BlockedIconId)
				p_Value = _YTEXT("0");
			else
				ASSERT(false);
			return true;
		}
		else if (&p_Column == m_ColumnSkuPartNumbers || &p_Column == m_ColumnSkuNames)
		{
			if (p_Field.GetValueStr() == BusinessUserConfiguration::GetInstance().GetValueStringLicenses_Unlicensed())
			{
				p_Value = _YTEXT("0");
				return true;
			}
		}
	}

	return false;
}

bool GridTemplateUsers::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (&p_Column == m_ColumnAccountEnabled)
	{
		// First set the bool(used by UpdatedObjectsGenerator)
		// then the string (which won't change the bool value) for the display.
		// If we set the string first, setting the bool would override it.

		ASSERT(!p_Value.empty());
		if (p_Value == _YTEXT("1"))
		{
			auto& field = p_Row.AddField(true, m_ColumnAccountEnabled);
			field.SetValue(BusinessUserConfiguration::GetInstance().GetValueStringSignInStatus_Allowed());
			field.SetIcon(m_AllowedIconId);
			field.RemoveModified();
		}
		else
		{
			ASSERT(p_Value == _YTEXT("0"));
			auto& field = p_Row.AddField(false, m_ColumnAccountEnabled, m_BlockedIconId);
			field.SetValue(BusinessUserConfiguration::GetInstance().GetValueStringSignInStatus_Blocked());
			field.SetIcon(m_BlockedIconId);
			field.RemoveModified();
		}

		return true;
	}
	else if (&p_Column == m_ColumnSkuPartNumbers || &p_Column == m_ColumnSkuNames)
	{
		if (p_Value == _YTEXT("0"))
		{
			p_Row.AddField(BusinessUserConfiguration::GetInstance().GetValueStringLicenses_Unlicensed(), &p_Column);
			return true;
		}
	}

	return false;
}

void GridTemplateUsers::SetO365(O365Grid& p_Grid, GridBackendRow& p_Row, bool p_Guest)
{
	GridUtil::SetUserRowObjectType(p_Grid, &p_Row, BusinessUserGuest());
	SetO365Metatype(p_Row);
}

void GridTemplateUsers::SetO365(O365Grid& p_Grid, GridBackendRow& p_Row, const BusinessUser& p_User)
{
	GridUtil::SetUserRowObjectType(p_Grid, &p_Row, p_User);
	SetO365Metatype(p_Row);
}

void GridTemplateUsers::SetOnPrem(O365Grid& p_Grid, GridBackendRow& p_Row)
{
	GridUtil::SetRowObjectType(p_Grid, &p_Row, OnPremiseUser());
	SetOnPremMetatype(p_Row);
}

void GridTemplateUsers::SetHybrid(O365Grid& p_Grid, GridBackendRow& p_Row, const BusinessUser& p_User)
{
	GridUtil::SetRowObjectType(p_Grid, &p_Row, p_User);
	ASSERT(GridUtil::IsBusinessUser(&p_Row, false));
	SetHybridMetatype(p_Row);
}

void GridTemplateUsers::SetHybrid(O365Grid& p_Grid, GridBackendRow& p_Row)
{
	ASSERT(GridUtil::IsBusinessUser(&p_Row, false));
	SetHybridMetatype(p_Row);
}

GridBackendColumn*& GridTemplateUsers::GetRequestDateColumn(UBI p_BlockId)
{
	return m_ColumnsRequestDateTime[p_BlockId];
}

GridBackendColumn*& GridTemplateUsers::getColumnForProperty(GridTemplateUsers& p_That, const wstring& p_PropertyName)
{
	using ColumnsByProperty = std::map<wstring, GridBackendColumn* GridTemplateUsers::*>;
	static const ColumnsByProperty m_RuleStringProps
	{
		{ _YUID("metaType"), &GridTemplateUsers::m_ColumnMetaType },
		{ _YUID(O365_ID), &GridTemplateUsers::m_ColumnID },
		{ _YUID(O365_USER_DISPLAYNAME), &GridTemplateUsers::m_ColumnDisplayName },

		{ _YUID(O365_USER_ABOUTME), &GridTemplateUsers::m_ColumnAboutMe },
		{ _YUID(O365_USER_ACCOUNTENABLED), &GridTemplateUsers::m_ColumnAccountEnabled },
		{ _YUID(O365_USER_BIRTHDAY), &GridTemplateUsers::m_ColumnBirthday },
		{ _YUID(O365_USER_BUSINESSPHONES), &GridTemplateUsers::m_ColumnBusinessPhones },
		{ _YUID(O365_USER_CITY), &GridTemplateUsers::m_ColumnCity },
		{ _YUID(O365_USER_COMPANYNAME), &GridTemplateUsers::m_ColumnCompanyName },
		{ _YUID(O365_USER_COUNTRY), &GridTemplateUsers::m_ColumnCountry },
		{ _YUID(O365_USER_DEPARTMENT), &GridTemplateUsers::m_ColumnDepartment },
		{ _YUID(O365_USER_GIVENNAME), &GridTemplateUsers::m_ColumnGivenName },
		{ _YUID(O365_USER_HIREDATE), &GridTemplateUsers::m_ColumnHireDate },
		{ _YUID(O365_USER_IMADDRESSES), &GridTemplateUsers::m_ColumnImAddresses },
		{ _YUID(O365_USER_INTERESTS), &GridTemplateUsers::m_ColumnInterests },
		{ _YUID(O365_USER_JOBTITLE), &GridTemplateUsers::m_ColumnJobTitle },
		{ _YUID(O365_USER_MAIL), &GridTemplateUsers::m_ColumnMail },
		{ _YUID(O365_USER_MAILNICKNAME), &GridTemplateUsers::m_ColumnMailNickname },
		{ _YUID(O365_USER_MAILBOXSETTINGS_ARCHIVEFOLDER), &GridTemplateUsers::m_ColumnMailboxSettingsArchiveFolderId },
		{ _YUID(O365_USER_MAILBOXSETTINGS_ARCHIVEFOLDERNAME), &GridTemplateUsers::m_ColumnMailboxSettingsArchiveFolderName },
		{ _YUID(O365_USER_MAILBOXSETTINGS_TIMEZONE), &GridTemplateUsers::m_ColumnMailboxSettingsTimeZone },
		{ _YUID(O365_USER_MAILBOXSETTINGS_LANGUAGE_LOCALE), &GridTemplateUsers::m_ColumnMailboxSettingsLanguageLocale },
		{ _YUID(O365_USER_MAILBOXSETTINGS_LANGUAGE_DISPLAYNAME), &GridTemplateUsers::m_ColumnMailboxSettingsLanguageDisplayName },
		{ _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_STATUS), &GridTemplateUsers::m_ColumnMailboxSettingsAutomaticRepliesSettingsStatus },
		{ _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDSTARTDATETIME_DATETIME), &GridTemplateUsers::m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeDateTime },
		{ _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDSTARTDATETIME_TIMEZONE), &GridTemplateUsers::m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeTimeZone },
		{ _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDENDDATETIME_DATETIME), &GridTemplateUsers::m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeDateTime },
		{ _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDENDDATETIME_TIMEZONE), &GridTemplateUsers::m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeTimeZone },
		{ _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_INTERNALREPLYMESSAGE), &GridTemplateUsers::m_ColumnMailboxSettingsAutomaticRepliesSettingsInternalReplyMessage },
		{ _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_EXTERNALREPLYMESSAGE), &GridTemplateUsers::m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalReplyMessage },
		{ _YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_EXTERNALAUDIENCE), &GridTemplateUsers::m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalAudience },
		{ _YUID(O365_USER_MAILBOXSETTINGS_TIMEFORMAT), &GridTemplateUsers::m_ColumnMailboxSettingsTimeFormat },
		{ _YUID(O365_USER_MAILBOXSETTINGS_DATEFORMAT), &GridTemplateUsers::m_ColumnMailboxSettingsDateFormat },
		{ _YUID(O365_USER_MAILBOXSETTINGS_DELEGATEMEETINGMESSAGEDELIVERYOPTIONS), &GridTemplateUsers::m_ColumnMailboxSettingsDelegateMeetingMessageDeliveryOptions },
		{ _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_DAYSOFWEEK), &GridTemplateUsers::m_ColumnMailboxSettingsWorkingHoursDaysOfWeek },
		{ _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_STARTTIME), &GridTemplateUsers::m_ColumnMailboxSettingsWorkingHoursStartTime },
		{ _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_ENDTIME), &GridTemplateUsers::m_ColumnMailboxSettingsWorkingHoursEndTime },
		{ _YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_TIMEZONENAME), &GridTemplateUsers::m_ColumnMailboxSettingsWorkingHoursTimeZoneName },
		{ _YUID("manager.displayName"), &GridTemplateUsers::m_ColumnManagerDisplayName },
		{ _YUID("manager.userPrincipalName"), &GridTemplateUsers::m_ColumnManagerUserPrincipalName },
		{ _YUID(O365_USER_MANAGER_ID), &GridTemplateUsers::m_ColumnManagerID },
		{ _YUID(O365_USER_MOBILEPHONE), &GridTemplateUsers::m_ColumnMobilePhone },
		{ _YUID(O365_USER_MYSITE), &GridTemplateUsers::m_ColumnMySite },
		{ _YUID(O365_USER_OFFICELOCATION), &GridTemplateUsers::m_ColumnOfficeLocation },
		{ _YUID(O365_USER_ONPREMISESIMMUTABLEID), &GridTemplateUsers::m_ColumnOPImmutableId },
		{ _YUID(O365_USER_ONPREMISESLASTSYNCDATETIME), &GridTemplateUsers::m_ColumnOPLastSync },
		{ _YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER), &GridTemplateUsers::m_ColumnOPSecurityID },
		{ _YUID(O365_USER_ONPREMISESSYNCENABLED), &GridTemplateUsers::m_ColumnOPSyncEnabled },
		{ _YUID(O365_USER_PASSWORDPOLICIES), &GridTemplateUsers::m_ColumnPasswordPolicies },

		{ _YUID(O365_USER_PASSWORDPROFILE_PASSWORD), &GridTemplateUsers::m_ColumnPassword },
		{ _YUID(O365_USER_PASSWORDPROFILE_FORCECHANGE), &GridTemplateUsers::m_ColumnForceChangePassword },
		{ _YUID(O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA), &GridTemplateUsers::m_ColumnForceChangePasswordWithMfa },
		{ _YUID(O365_USER_PASTPROJECTS), &GridTemplateUsers::m_ColumnPastProjects },
		{ _YUID(O365_USER_POSTALCODE), &GridTemplateUsers::m_ColumnPostalCode },
		{ _YUID(O365_USER_PREFERREDLANGUAGE), &GridTemplateUsers::m_ColumnPreferredLanguage },
		{ _YUID(O365_USER_PREFERREDNAME), &GridTemplateUsers::m_ColumnPreferredName },
		{ _YUID(O365_USER_PROXYADDRESSES), &GridTemplateUsers::m_ColumnProxyAddresses },
		{ _YUID(O365_USER_RESPONSIBILITIES), &GridTemplateUsers::m_ColumnResponsibilities },
		{ _YUID(O365_USER_SCHOOLS), &GridTemplateUsers::m_ColumnSchools },
		{ _YUID(O365_USER_SKILLS), &GridTemplateUsers::m_ColumnSkills },
		{ _YUID(O365_USER_STATE), &GridTemplateUsers::m_ColumnState },
		{ _YUID(O365_USER_STREETADDRESS), &GridTemplateUsers::m_ColumnStreetAddress },
		{ _YUID(O365_USER_SURNAME), &GridTemplateUsers::m_ColumnSurname },
		{ _YUID(O365_USER_USAGELOCATION), &GridTemplateUsers::m_ColumnUsageLocation },
		{ _YUID(O365_USER_USERPRINCIPALNAME), &GridTemplateUsers::m_ColumnUserPrincipalName },
		{ _YUID(O365_USER_USERTYPE), &GridTemplateUsers::m_ColumnUserType },
		{ _YUID(O365_USER_DELETEDDATETIME), &GridTemplateUsers::m_ColumnDeletedDateTime },
		{ _YUID("assignedLicenses.skuId"), &GridTemplateUsers::m_ColumnSkuIds },
		{ _YUID(O365_USER_ASSIGNEDLICENSES), &GridTemplateUsers::m_ColumnSkuPartNumbers },
		{ _YUID("ASSIGNEDLICENSENAMES"), &GridTemplateUsers::m_ColumnSkuNames },

		{ _YUID(O365_USER_AGEGROUP), &GridTemplateUsers::m_ColumnAgeGroup },
		{ _YUID(O365_USER_CREATEDDATETIME), &GridTemplateUsers::m_ColumnCreatedDateTime },
		{ _YUID(O365_USER_CONSENTPROVIDEDFORMINOR), &GridTemplateUsers::m_ColumnConsentProvidedForMinor },
		{ _YUID(O365_USER_LEGALAGEGROUPCLASSIFICATION), &GridTemplateUsers::m_ColumnLegalAgeGroupClassification },
		{ _YUID(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME), &GridTemplateUsers::m_ColumnSignInSessionsValidFromDateTime },
		{ _YUID(O365_USER_ONPREMISESDOMAINNAME), &GridTemplateUsers::m_ColumnOPDomainName },
		{ _YUID(O365_USER_ONPREMISESSAMACCOUNTNAME), &GridTemplateUsers::m_ColumnOPSamAccountName },
		{ _YUID(O365_USER_ONPREMISESUSERPRINCIPALNAME), &GridTemplateUsers::m_ColumnOPUserPrincipalName },
		{ _YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY), &GridTemplateUsers::m_ColumnOPProvisioningErrorsCategory },
		{ _YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME), &GridTemplateUsers::m_ColumnOPProvisioningErrorsOccuredDateTime },
		{ _YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR), &GridTemplateUsers::m_ColumnOPProvisioningErrorsPropertyCausingError },
		{ _YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE), &GridTemplateUsers::m_ColumnOPProvisioningErrorsValue },

		{ _YUID(O365_USER_OTHERMAILS), &GridTemplateUsers::m_ColumnOtherMails },
		{ _YUID(O365_USER_PREFERREDDATALOCATION), &GridTemplateUsers::m_ColumnPreferredDataLocation },
		{ _YUID(O365_USER_SHOWINADDRESSLIST), &GridTemplateUsers::m_ColumnShowInAddressList },
		{ _YUID(O365_USER_EXTERNALUSERSTATUS), &GridTemplateUsers::m_ColumnExternalUserStatus },
		{ _YUID(O365_USER_EXTERNALUSERSTATUSCHANGEDON), &GridTemplateUsers::m_ColumnExternalUserStatusChangedOn },

		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute1"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes1 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute2"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes2 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute3"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes3 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute4"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes4 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute5"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes5 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute6"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes6 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute7"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes7 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute8"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes8 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute9"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes9 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute10"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes10 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute11"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes11 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute12"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes12 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute13"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes13 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute14"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes14 },
		{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YUID(".extensionAttribute15"), &GridTemplateUsers::m_ColumnsOPExtensionAttributes15 },

		{ _YUID(O365_USER_EMPLOYEEID), &GridTemplateUsers::m_ColumnEmployeeID },
		{ _YUID(O365_USER_FAXNUMBER), &GridTemplateUsers::m_ColumnFaxNumber },
		{ _YUID(O365_USER_ISRESOURCEACCOUNT), &GridTemplateUsers::m_ColumnIsResourceAccount },
		{ _YUID(O365_USER_ONPREMISESDISTINGUISHEDNAME), &GridTemplateUsers::m_ColumnOPDistinguishedName },

		{ _YUID(O365_USER_CREATIONTYPE), &GridTemplateUsers::m_ColumnCreationType },
		{ _YUID("identities.signInType"), &GridTemplateUsers::m_ColumnIdentitiesSignInType },
		{ _YUID("identities.issuer"), &GridTemplateUsers::m_ColumnIdentitiesIssuer },
		{ _YUID("identities.issuerAssignedId"), &GridTemplateUsers::m_ColumnIdentitiesIssuerAssignedId },
		{ _YUID(O365_USER_LASTPASSWORDCHANGEDATETIME), &GridTemplateUsers::m_ColumnLastPasswordChangeDateTime },
		{ _YUID("licenseAssignmentStates.skuId"), &GridTemplateUsers::m_ColumnLicenseAssignmentStatesSkuId },
		{ _YUID("ASSIGNEDSKUPARTNUMBER"), &GridTemplateUsers::m_ColumnLicenseAssignmentStatesSkuPartNumber },
		{ _YUID("ASSIGNEDBYGROUPLICENSENAME"), &GridTemplateUsers::m_ColumnLicenseAssignmentStatesSkuName },
		{ _YUID("licenseAssignmentStates.assignedByGroup"), &GridTemplateUsers::m_ColumnLicenseAssignmentStatesAssignedByGroupId },
		{ _YUID("ASSIGNEDBYGROUPNAME"), &GridTemplateUsers::m_ColumnLicenseAssignmentStatesAssignedByGroupDisplayName },
		{ _YUID("ASSIGNEDBYGROUPEMAIL"), &GridTemplateUsers::m_ColumnLicenseAssignmentStatesAssignedByGroupMail },
		{ _YUID("licenseAssignmentStates.state"), &GridTemplateUsers::m_ColumnLicenseAssignmentStatesState },
		{ _YUID("licenseAssignmentStates.error"), &GridTemplateUsers::m_ColumnLicenseAssignmentStatesError },
		{ _YUID("GROUPLICENSINGMEMBER"), &GridTemplateUsers::m_ColumnIsAnyLicenseAssignedByGroup },
		{ _YUID(O365_USER_REFRESTOKENVALIDFROMDATETIME), &GridTemplateUsers::m_ColumnRefresTokenValidFromDateTime },
		{ _YUID(O365_USER_MAILBOX_TYPE), &GridTemplateUsers::m_ColumnMailboxType },
		{ _YUID(O365_USER_MAILBOX_DELIVERTOMAILBOXANDFORWARD), &GridTemplateUsers::m_ColumnMailboxDeliverToMailboxAndForward },
		{ _YUID(O365_USER_MAILBOX_FORWARDINGSMTPADDRESS), &GridTemplateUsers::m_ColumnMailboxForwardingSmtpAddress },
		{ _YUID(O365_USER_MAILBOX_FORWARDINGADDRESS), &GridTemplateUsers::m_ColumnMailboxForwardingAddress },
		{ _YUID(O365_USER_MAILBOX_CAN_SEND_ON_BEHALF), &GridTemplateUsers::m_ColumnMailboxCanSendOnBehalf },
		{ _YUID(O365_USER_MAILBOX_CAN_SEND_AS_USER), &GridTemplateUsers::m_ColumnRecipientPermissionsCanSendAsUser },

		{ BasicGridSetup::g_LastQueryColumnUID, &GridTemplateUsers::m_ColumnLastRequestDateTime },

		{ _YUID(O365_COLDRIVENAME), &GridTemplateUsers::m_ColumnDriveName },
		{ _YUID(O365_COLDRIVEID), &GridTemplateUsers::m_ColumnDriveId },
		{ _YUID(O365_COLDRIVEDESCRIPTION), &GridTemplateUsers::m_ColumnDriveDescription },
		{ _YUID(O365_COLDRIVEQUOTASTATE), &GridTemplateUsers::m_ColumnDriveQuotaState },
		{ _YUID(O365_COLDRIVEQUOTAUSED), &GridTemplateUsers::m_ColumnDriveQuotaUsed },
		{ _YUID(O365_COLDRIVEQUOTAREMAINING), &GridTemplateUsers::m_ColumnDriveQuotaRemaining },
		{ _YUID(O365_COLDRIVEQUOTADELETED), &GridTemplateUsers::m_ColumnDriveQuotaDeleted },
		{ _YUID(O365_COLDRIVEQUOTATOTAL), &GridTemplateUsers::m_ColumnDriveQuotaTotal },
		{ _YUID(O365_COLDRIVECREATIONTIME), &GridTemplateUsers::m_ColumnDriveCreationTime },
		{ _YUID(O365_COLDRIVELASTMODIFIEDDATETIME), &GridTemplateUsers::m_ColumnDriveLastModifiedTime },
		{ _YUID(O365_COLDRIVETYPE), &GridTemplateUsers::m_ColumnDriveType },
		{ _YUID(O365_COLDRIVEWEBURL), &GridTemplateUsers::m_ColumnDriveWebUrl },
		{ _YUID(O365_COLDRIVECREATEDBYUSEREMAIL), &GridTemplateUsers::m_ColumnDriveCreatedByUserEmail },
		{ _YUID(O365_COLDRIVECREATEDBYUSERID), &GridTemplateUsers::m_ColumnDriveCreatedByUserId },
		{ _YUID(O365_COLDRIVECREATEDBYAPPNAME), &GridTemplateUsers::m_ColumnDriveCreatedByAppName },
		{ _YUID(O365_COLDRIVECREATEDBYAPPID), &GridTemplateUsers::m_ColumnDriveCreatedByAppId },
		{ _YUID(O365_COLDRIVECREATEDBYDEVICENAME), &GridTemplateUsers::m_ColumnDriveCreatedByDeviceName },
		{ _YUID(O365_COLDRIVECREATEDBYDEVICEID), &GridTemplateUsers::m_ColumnDriveCreatedByDeviceId },
		{ _YUID(O365_COLDRIVEOWNERUSERNAME), &GridTemplateUsers::m_ColumnDriveOwnerUserName },
		{ _YUID(O365_COLDRIVEOWNERUSERID), &GridTemplateUsers::m_ColumnDriveOwnerUserId },
		{ _YUID(O365_COLDRIVEOWNERUSEREMAIL), &GridTemplateUsers::m_ColumnDriveOwnerUserEmail },
		{ _YUID(O365_COLDRIVEOWNERAPPNAME), &GridTemplateUsers::m_ColumnDriveOwnerAppName },
		{ _YUID(O365_COLDRIVEOWNERAPPID), &GridTemplateUsers::m_ColumnDriveOwnerAppId },
		{ _YUID(O365_COLDRIVEOWNERDEVICENAME), &GridTemplateUsers::m_ColumnDriveOwnerDeviceName },
		{ _YUID(O365_COLDRIVEOWNERDEVICEID), &GridTemplateUsers::m_ColumnDriveOwnerDeviceId },
	};

	const auto it = m_RuleStringProps.find(p_PropertyName);
	if (m_RuleStringProps.end() != it)
		return p_That.*(it->second);

	ASSERT(false); // Either you forgot to add your new column in above map, or you're requesting an inexistent one!
	static GridBackendColumn* fakePtr;
	fakePtr = nullptr;
	return fakePtr;
}

GridBackendRow* GridTemplateUsers::UpdateRow(O365Grid& p_Grid, const BusinessUser& p_BU, bool p_SetModifiedStatus, GridUpdater& p_Updater, bool p_FromLoadMore)
{
	vector<GridBackendField> BUpk;
	GetBusinessUserPK(p_Grid, p_BU, BUpk);

	if (p_SetModifiedStatus)
	{
		auto mod = p_Grid.GetModifications().GetRowModification<DeletedObjectModification>(BUpk);
		if (nullptr != mod)
		{
			ASSERT(nullptr == p_Grid.GetModifications().GetRowModification<CreatedObjectModification>(BUpk)
				&& nullptr == p_Grid.GetModifications().GetRowFieldModification(BUpk));
			p_Grid.GetModifications().Revert(BUpk, false);
		}
	}

	GridBackendRow* rowToUpdate = p_Grid.GetRowIndex().GetExistingRow(BUpk);
	if (nullptr != rowToUpdate)
	{
		if (p_BU.GetError())
		{
			ASSERT(!p_SetModifiedStatus);
			if (p_FromLoadMore)
			{
				//p_Grid.ClearFieldsByPreset(rowToUpdate, O365Grid::g_ColumnsPresetMore);
				for (const auto& col : p_Grid.GetColumnsByPresets({ O365Grid::g_ColumnsPresetMore }))
					rowToUpdate->AddField(p_BU.GetError()->GetFullErrorMessage(), col, GridBackendUtil::ICON_ERROR);
			}

			// Update fields that don't correspond to main load more request, they should contain a "Skipped" error.
			updateRowMailboxSettings(p_BU, rowToUpdate, p_SetModifiedStatus ? UpdateFieldOption::IS_MODIFICATION : UpdateFieldOption::NONE);

			// FIXME: Update other fields?

			p_Updater.OnLoadingError(rowToUpdate, *p_BU.GetError());
		}
		else
		{
			// Clear previous error
			if (!p_SetModifiedStatus && !p_FromLoadMore && rowToUpdate->IsError())
				rowToUpdate->ClearStatus();

			updateRow(p_Grid, p_BU, rowToUpdate, p_SetModifiedStatus ? UpdateFieldOption::IS_MODIFICATION : UpdateFieldOption::NONE);

			if (rowToUpdate->HasModifiedField(p_Grid.GetBackend().GetStatusColumn()->GetID()) && p_SetModifiedStatus)
			{
				p_Grid.OnRowModified(rowToUpdate);
			}
			else
			{
				GridUtil::HandleMultiError(rowToUpdate
					,	{ { p_BU.GetMailboxSettingsError(), YtriaTranslate::Do(GridTemplateUsers_UpdateRow_2, _YLOC("Mailbox settings: %1"), _YTEXT("%s")) }
						, { p_BU.GetArchiveFolderNameError(), YtriaTranslate::Do(GridTemplateUsers_UpdateRow_3, _YLOC("Archive Folder name: %1"), _YTEXT("%s")) }
						, { p_BU.GetDriveError(), YtriaTranslate::Do(GridTemplateGroups_UpdateRow_6, _YLOC("OneDrive: %1"), _YTEXT("%s")) }
						, { p_BU.GetManagerError(), YtriaTranslate::Do(GridTemplateUsers_UpdateRow_5, _YLOC("Manager: %1"), _YTEXT("%s")) }
						, { p_BU.GetMailboxInfoError(), _YFORMAT(L"Mailbox Info: %s", _YTEXT("%s")) }
						, { p_BU.GetRecipientPermissionsError(), _YFORMAT(L"Recipient Permissions: %s", _YTEXT("%s")) }
					}
				, p_Updater);
			}
		}

		if (p_Grid.IsGridModificationsEnabled())
			p_Updater.AddUpdatedRowPk(BUpk);
	}

	return rowToUpdate;
}

GridBackendRow* GridTemplateUsers::UpdateRow(O365Grid& p_Grid, const OnPremiseUser& p_OnPremUser, const map<wstring, row_pk_t>& p_O365RowPk, map<wstring, row_pk_t>& p_OnPremRowPk, UpdateFieldOption p_UpdateFieldOption/* = UpdateFieldOption::NONE*/)
{
	std::set<PooledString> dummy;
	return UpdateRow(p_Grid, p_OnPremUser, p_O365RowPk, p_OnPremRowPk, dummy, p_UpdateFieldOption);
}

GridBackendRow* GridTemplateUsers::UpdateRow(O365Grid& p_Grid, const OnPremiseUser& p_OnPremUser, const map<wstring, row_pk_t>& p_O365RowPk, map<wstring, row_pk_t>& p_OnPremRowPk, std::set<PooledString>& p_NewImmutableIds, UpdateFieldOption p_UpdateFieldOption)
{
	GridBackendRow* row = nullptr;

	GridFrameBase* frame = dynamic_cast<GridFrameBase*>(p_Grid.GetParentFrame());
	bool useConsistencyGuid = false;
	ASSERT(nullptr != frame);
	if (nullptr != frame)
		useConsistencyGuid = frame->GetBusinessObjectManager()->GetGraphCache().GetUseConsistencyGuid();

	const auto immutableId = OnPremiseUser::ToImmutableID(p_OnPremUser, useConsistencyGuid);
	ASSERT(!immutableId.empty());
	p_NewImmutableIds.insert(immutableId);

	auto itt = p_O365RowPk.find(immutableId);
	if (itt != p_O365RowPk.end())
	{
		// Refreshing on-prem user which has an o365 equivalent
		// In that case, fill the existing o365 row
		row = p_Grid.GetRowIndex().GetExistingRow(itt->second);
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			SetHybrid(p_Grid, *row);
			p_OnPremRowPk[immutableId] = itt->second;
		}
	}

	if (nullptr == row && p_OnPremUser.GetObjectGUID())
	{
		// Refreshing on-prem user which has no o365 equivalent
		// In that case, create a new row
		row_pk_t pk = CreateRowPk(p_OnPremUser);
		static const bool willPurge = false; // We never purge when refreshing OnPrem
		row = p_Grid.GetRowIndex().GetRow(pk, true, willPurge);
		SetOnPrem(p_Grid , *row);
		p_OnPremRowPk[immutableId] = pk;
	}

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		if (nullptr != p_Grid.GetSecondaryStatusColumn() && !row->HasField(p_Grid.GetSecondaryStatusColumn()))
			row->AddField(PooledString::g_EmptyString, p_Grid.GetSecondaryStatusColumn()).RemoveModified();

		updateFieldOnPrem(p_OnPremUser.GetAccountExpires(), row, m_OnPremCols.m_ColAccountExpires, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetAccountLockoutTime(), row, m_OnPremCols.m_ColAccountLockoutTime, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetAccountNotDelegated(), row, m_OnPremCols.m_ColAccountNotDelegated, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetAdminCount(), row, m_OnPremCols.m_ColAdminCount, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetAllowReversiblePasswordEncryption(), row, m_OnPremCols.m_ColAllowReversiblePasswordEncryption, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetAuthenticationPolicy(), row, m_OnPremCols.m_ColAuthenticationPolicy, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetAuthenticationPolicySilo(), row, m_OnPremCols.m_ColAuthenticationPolicySilo, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetBadLogonCount(), row, m_OnPremCols.m_ColBadLogonCount, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetBadPasswordTime(), row, m_OnPremCols.m_ColBadPasswordTime, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetBadPwdCount(),  row, m_OnPremCols.m_ColBadPwdCount, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetCannotChangePassword(),  row, m_OnPremCols.m_ColCannotChangePassword, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetCanonicalName(),  row, m_OnPremCols.m_ColCanonicalName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetCertificates(),  row, m_OnPremCols.m_ColCertificates, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetCity(),  row, m_OnPremCols.m_ColCity, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetCN(),  row, m_OnPremCols.m_ColCN, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetCodePage(),  row, m_OnPremCols.m_ColCodePage, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetCompany(),  row, m_OnPremCols.m_ColCompany, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetCompoundIdentitySupported(),  row, m_OnPremCols.m_ColCompoundIdentitySupported, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetCountry(),  row, m_OnPremCols.m_ColCountry, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetCountryCode(),  row, m_OnPremCols.m_ColCountryCode, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetCreated(), row, m_OnPremCols.m_ColCreated, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetDeleted(), row, m_OnPremCols.m_ColDeleted, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetDepartment(), row, m_OnPremCols.m_ColDepartment, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetDescription(), row, m_OnPremCols.m_ColDescription, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetDisplayName(), row, m_OnPremCols.m_ColDisplayName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetDistinguishedName(), row, m_OnPremCols.m_ColDistinguishedName, p_UpdateFieldOption);
		updateFieldOnPrem(OnPremiseUsersColumns::GetOrganizationalUnit(p_OnPremUser.GetDistinguishedName()), row, m_OnPremCols.m_ColOrganizationalUnit, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetDivision(), row, m_OnPremCols.m_ColDivision, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetDoesNotRequirePreAuth(), row, m_OnPremCols.m_ColDoesNotRequirePreAuth, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetdSCorePropagationData(), row, m_OnPremCols.m_ColDSCorePropagationData, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetEmailAddress(), row, m_OnPremCols.m_ColEmailAddress, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetEmployeeID(), row, m_OnPremCols.m_ColEmployeeID, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetEmployeeNumber(), row, m_OnPremCols.m_ColEmployeeNumber, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetEnabled(), row, m_OnPremCols.m_ColEnabled, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetFax(), row, m_OnPremCols.m_ColFax, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetGivenName(), row, m_OnPremCols.m_ColGivenName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetHomeDirectory(), row, m_OnPremCols.m_ColHomeDirectory, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetHomedirRequired(), row, m_OnPremCols.m_ColHomedirRequired, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetHomeDrive(), row, m_OnPremCols.m_ColHomeDrive, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetHomePage(), row, m_OnPremCols.m_ColHomePage, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetHomePhone(), row, m_OnPremCols.m_ColHomePhone, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetInitials(), row, m_OnPremCols.m_ColInitials, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetInstanceType(), row, m_OnPremCols.m_ColInstanceType, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetIsCriticalSystemObject(), row, m_OnPremCols.m_ColIsCriticalSystemObject, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetIsDeleted(), row, m_OnPremCols.m_ColIsDeleted, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetKerberosEncryptionType(), row, m_OnPremCols.m_ColKerberosEncryptionType, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetLastBadPasswordAttempt(), row, m_OnPremCols.m_ColLastBadPasswordAttempt, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetLastKnownParent(), row, m_OnPremCols.m_ColLastKnownParent, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetLastLogoff(), row, m_OnPremCols.m_ColLastLogoff, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetLastLogon(), row, m_OnPremCols.m_ColLastLogon, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetLastLogonDate(), row, m_OnPremCols.m_ColLastLogonDate, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetLockedOut(), row, m_OnPremCols.m_ColLockedOut, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetLogonCount(), row, m_OnPremCols.m_ColLogonCount, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetLogonHours(), row, m_OnPremCols.m_ColLogonHours, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetLogonWorkstations(), row, m_OnPremCols.m_ColLogonWorkstations, p_UpdateFieldOption);
		// Be careful if ever Manager becomes editable, 2 columns!
		updateFieldOnPrem(OnPremiseUsersColumns::ToFriendyName(p_OnPremUser.GetManager()), row, m_OnPremCols.m_ColManagerFriendly, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetManager(), row, m_OnPremCols.m_ColManager, p_UpdateFieldOption);
		// Be careful if ever MemberOf becomes editable, 2 columns!
		updateFieldOnPrem(OnPremiseUsersColumns::ToFriendyNames(p_OnPremUser.GetMemberOf()), row, m_OnPremCols.m_ColMemberOfFriendly, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetMemberOf(), row, m_OnPremCols.m_ColMemberOf, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetMNSLogonAccount(), row, m_OnPremCols.m_ColMNSLogonAccount, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetMobilePhone(), row, m_OnPremCols.m_ColMobilePhone, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetModified(), row, m_OnPremCols.m_ColModified, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetMsDsConsistencyGuidAsString(), row, m_OnPremCols.m_ColMSDSConsistencyGuid, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetMSDSUserAccountControlComputed(), row, m_OnPremCols.m_ColMSDSUserAccountControlComputed, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetName(), row, m_OnPremCols.m_ColName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetTSecurityDescriptor(), row, m_OnPremCols.m_ColNTSecurityDescriptor, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetObjectCategory(), row, m_OnPremCols.m_ColObjectCategory, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetObjectClass(), row, m_OnPremCols.m_ColObjectClass, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetObjectGUID(), row, m_OnPremCols.m_ColObjectGUID, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetObjectSid(), row, m_OnPremCols.m_ColObjectSid, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetOffice(), row, m_OnPremCols.m_ColOffice, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetOfficePhone(), row, m_OnPremCols.m_ColOfficePhone, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetOrganization(), row, m_OnPremCols.m_ColOrganization, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetOtherName(), row, m_OnPremCols.m_ColOtherName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetPasswordExpired(), row, m_OnPremCols.m_ColPasswordExpired, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetPasswordLastSet(), row, m_OnPremCols.m_ColPasswordLastSet, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetPasswordNeverExpires(), row, m_OnPremCols.m_ColPasswordNeverExpires, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetPasswordNotRequired(), row, m_OnPremCols.m_ColPasswordNotRequired, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetPOBox(), row, m_OnPremCols.m_ColPOBox, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetPostalCode(), row, m_OnPremCols.m_ColPostalCode, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetPrimaryGroup(), row, m_OnPremCols.m_ColPrimaryGroup, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetPrimaryGroupId(), row, m_OnPremCols.m_ColPrimaryGroupId, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetPrincipalsAllowedToDelegateToAccount(), row, m_OnPremCols.m_ColPrincipalsAllowedToDelegateToAccount, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetProfilePath(), row, m_OnPremCols.m_ColProfilePath, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetProtectedFromAccidentalDeletion(), row, m_OnPremCols.m_ColProtectedFromAccidentalDeletion, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetProxyAddresses(), row, m_OnPremCols.m_ColProxyAddresses, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetSamAccountName(), row, m_OnPremCols.m_ColSamAccountName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetSAMAccountType(), row, m_OnPremCols.m_ColSAMAccountType, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetScriptPath(), row, m_OnPremCols.m_ColScriptPath, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetSDRightsEffective(), row, m_OnPremCols.m_ColSDRightsEffective, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetServicePrincipalNames(), row, m_OnPremCols.m_ColServicePrincipalNames, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetSID(), row, m_OnPremCols.m_ColSID, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetSIDHistory(), row, m_OnPremCols.m_ColSIDHistory, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetSmartcardLogonRequired(), row, m_OnPremCols.m_ColSmartcardLogonRequired, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetState(), row, m_OnPremCols.m_ColState, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetStreetAddress(), row, m_OnPremCols.m_ColStreetAddress, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetSurname(), row, m_OnPremCols.m_ColSurname, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetTitle(), row, m_OnPremCols.m_ColTitle, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetTrustedForDelegation(), row, m_OnPremCols.m_ColTrustedForDelegation, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetTrustedToAuthForDelegation(), row, m_OnPremCols.m_ColTrustedToAuthForDelegation, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetUseDESKeyOnly(), row, m_OnPremCols.m_ColUseDESKeyOnly, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetUserAccountControl(), row, m_OnPremCols.m_ColUserAccountControl, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetUserCertificate(), row, m_OnPremCols.m_ColUserCertificate, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetUserPrincipalName(), row, m_OnPremCols.m_ColUserPrincipalName, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GttUSNChanged(), row, m_OnPremCols.m_ColUSNChanged, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetUSNCreated(), row, m_OnPremCols.m_ColUSNCreated, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetWhenChanged(), row, m_OnPremCols.m_ColWhenChanged, p_UpdateFieldOption);
		updateFieldOnPrem(p_OnPremUser.GetWhenCreated(), row, m_OnPremCols.m_ColWhenCreated, p_UpdateFieldOption);

		GridFrameBase* frame = dynamic_cast<GridFrameBase*>(p_Grid.GetParentFrame());
		bool useConsistencyGuid = false;
		ASSERT(nullptr != frame);
		if (nullptr != frame)
			useConsistencyGuid = frame->GetBusinessObjectManager()->GetGraphCache().GetUseConsistencyGuid();
		updateFieldOnPrem(OnPremiseUser::ToImmutableID(p_OnPremUser, useConsistencyGuid), row, m_OnPremCols.m_ColImmutableId, p_UpdateFieldOption);

		if (p_UpdateFieldOption == UpdateFieldOption::NONE) // Only load
			UpdateCommonInfo(row);
	}

	return row;
}

GridBackendRow* GridTemplateUsers::UpdateRowMailbox(O365Grid& p_Grid, const BusinessUser& p_BU, GridUpdater& p_Updater)
{
	vector<GridBackendField> BUpk;
	GetBusinessUserPK(p_Grid, p_BU, BUpk);

	GridBackendRow* rowToUpdate = p_Grid.GetRowIndex().GetExistingRow(BUpk);
	if (nullptr != rowToUpdate)
	{
		updateRowMailbox(p_BU, rowToUpdate, UpdateFieldOption::NONE);
		setLastRequestDateTime(p_Grid, p_BU, rowToUpdate);

		if (p_Grid.IsGridModificationsEnabled())
			p_Updater.AddUpdatedRowPk(BUpk);

		GridUtil::HandleMultiError(rowToUpdate
			, { { p_BU.GetMailboxInfoError(), _YFORMAT(L"Mailbox Info: %s", _YTEXT("%s")) }
			  , { p_BU.GetRecipientPermissionsError(), _YFORMAT(L"Recipient Permissions: %s", _YTEXT("%s")) }
			}
		, p_Updater, _YTEXT("ManagementObjectNotFoundException"));
	}

	return rowToUpdate;
}

namespace
{
	auto setErrorField = [](GridBackendField* p_Field)
	{
		if (nullptr != p_Field && !p_Field->IsGeneric())
			p_Field->SetIcon(GridBackendUtil::ICON_ERROR);
	};
}

void GridTemplateUsers::updateRow(O365Grid& p_Grid, const BusinessUser& p_BU, GridBackendRow* p_Row, UpdateFieldOption p_UpdateFieldOption)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		// For all users
		{
			if (!p_BU.GetID().IsEmpty())
			{
				auto& idField = p_Row->GetField(m_ColumnID);
				ASSERT(!idField.HasValue() || p_BU.GetID() == idField.GetValueStr());
				if (!idField.HasValue())
					p_Row->AddField(p_BU.GetID(), m_ColumnID);
			}

			if (UpdateFieldOption::NONE == p_UpdateFieldOption)
				setLastRequestDateTime(p_Grid, p_BU, p_Row);

			updateField(p_BU.GetDisplayName(), p_Row, m_ColumnDisplayName, p_UpdateFieldOption);
			updateField(p_BU.GetUserPrincipalName(), p_Row, m_ColumnUserPrincipalName, p_UpdateFieldOption);
			updateField(p_BU.GetUserType(), p_Row, m_ColumnUserType, p_UpdateFieldOption);

			if (nullptr != m_ColumnAccountEnabled)
			{
				boost::YOpt<PooledString> accountEnabled;
				int icon = NO_ICON;
				if (p_BU.GetAccountEnabled())
				{
					if (*p_BU.GetAccountEnabled())
					{
						accountEnabled = BusinessUserConfiguration::GetInstance().GetValueStringSignInStatus_Allowed();
						icon = m_AllowedIconId;
					}
					else
					{
						accountEnabled = BusinessUserConfiguration::GetInstance().GetValueStringSignInStatus_Blocked();
						icon = m_BlockedIconId;
					}
				}
				auto field = updateField(p_BU.GetAccountEnabled(), p_Row, m_ColumnAccountEnabled, p_UpdateFieldOption, accountEnabled ? *accountEnabled : PooledString());
				if (field && !field->IsGeneric())
					field->SetIcon(icon);
			}
			updateField(p_BU.GetBusinessPhones(), p_Row, m_ColumnBusinessPhones, p_UpdateFieldOption);
			updateField(p_BU.GetCity(), p_Row, m_ColumnCity, p_UpdateFieldOption);
			updateField(p_BU.GetCountry(), p_Row, m_ColumnCountry, p_UpdateFieldOption);
			updateField(p_BU.GetDepartment(), p_Row, m_ColumnDepartment, p_UpdateFieldOption);
			updateField(p_BU.GetGivenName(), p_Row, m_ColumnGivenName, p_UpdateFieldOption);
			updateField(p_BU.GetJobTitle(), p_Row, m_ColumnJobTitle, p_UpdateFieldOption);
			updateField(p_BU.GetCompanyName(), p_Row, m_ColumnCompanyName, p_UpdateFieldOption);
			updateField(p_BU.GetMailNickname(), p_Row, m_ColumnMailNickname, p_UpdateFieldOption);
			updateField(p_BU.GetFaxNumber(), p_Row, m_ColumnFaxNumber, p_UpdateFieldOption);
			updateField(p_BU.GetMobilePhone(), p_Row, m_ColumnMobilePhone, p_UpdateFieldOption);
			updateField(p_BU.GetOfficeLocation(), p_Row, m_ColumnOfficeLocation, p_UpdateFieldOption);
			updateField(p_BU.GetOnPremisesImmutableId(), p_Row, m_ColumnOPImmutableId, p_UpdateFieldOption);
			updateField(p_BU.GetPasswordPolicies(), p_Row, m_ColumnPasswordPolicies, p_UpdateFieldOption);
			updateField(p_BU.GetPassword(), p_Row, m_ColumnPassword, p_UpdateFieldOption);
			updateField(p_BU.GetForceChangePassword(), p_Row, m_ColumnForceChangePassword, p_UpdateFieldOption);
			updateField(p_BU.GetForceChangePasswordWithMfa(), p_Row, m_ColumnForceChangePasswordWithMfa, p_UpdateFieldOption);
			updateField(p_BU.GetPostalCode(), p_Row, m_ColumnPostalCode, p_UpdateFieldOption);
			updateField(p_BU.GetPreferredLanguage(), p_Row, m_ColumnPreferredLanguage, p_UpdateFieldOption);
			updateField(p_BU.GetState(), p_Row, m_ColumnState, p_UpdateFieldOption);
			updateField(p_BU.GetStreetAddress(), p_Row, m_ColumnStreetAddress, p_UpdateFieldOption);
			updateField(p_BU.GetSurname(), p_Row, m_ColumnSurname, p_UpdateFieldOption);
			updateField(p_BU.GetUsageLocation(), p_Row, m_ColumnUsageLocation, p_UpdateFieldOption);
			updateField(p_BU.GetDeletedDateTime(), p_Row, m_ColumnDeletedDateTime, p_UpdateFieldOption);

			// Drive fields
			if (p_BU.GetDrive())
			{
				if (p_BU.GetDriveError())
				{
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveName, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveId, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveDescription, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreationTime, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveLastModifiedTime, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveType, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveWebUrl, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveQuotaState, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveQuotaUsed, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveQuotaRemaining, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveQuotaDeleted, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveQuotaTotal, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByUserEmail, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByUserId, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByAppName, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByAppId, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByDeviceName, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveCreatedByDeviceId, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerUserName, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerUserId, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerAppName, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerAppId, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerDeviceName, p_UpdateFieldOption));
					setErrorField(updateField(p_BU.GetDriveError()->GetFullErrorMessage(), p_Row, m_ColumnDriveOwnerDeviceId, p_UpdateFieldOption));
				}
				else
				{
					updateField(p_BU.GetDrive()->GetName(), p_Row, m_ColumnDriveName, p_UpdateFieldOption);
					updateField(p_BU.GetDrive()->GetID(), p_Row, m_ColumnDriveId, p_UpdateFieldOption);
					updateField(p_BU.GetDrive()->GetDescription(), p_Row, m_ColumnDriveDescription, p_UpdateFieldOption);
					updateField(p_BU.GetDrive()->GetCreatedDateTime(), p_Row, m_ColumnDriveCreationTime, p_UpdateFieldOption);
					updateField(p_BU.GetDrive()->GetLastModifiedDateTime(), p_Row, m_ColumnDriveLastModifiedTime, p_UpdateFieldOption);
					updateField(p_BU.GetDrive()->GetDriveType(), p_Row, m_ColumnDriveType, p_UpdateFieldOption);
					updateField(p_BU.GetDrive()->GetWebUrl(), p_Row, m_ColumnDriveWebUrl, p_UpdateFieldOption);

					if (p_BU.GetDrive()->GetQuota())
					{
						updateField(p_BU.GetDrive()->GetQuota()->State, p_Row, m_ColumnDriveQuotaState, p_UpdateFieldOption);
						updateField(p_BU.GetDrive()->GetQuota()->Used, p_Row, m_ColumnDriveQuotaUsed, p_UpdateFieldOption);
						updateField(p_BU.GetDrive()->GetQuota()->Remaining, p_Row, m_ColumnDriveQuotaRemaining, p_UpdateFieldOption);
						updateField(p_BU.GetDrive()->GetQuota()->Deleted, p_Row, m_ColumnDriveQuotaDeleted, p_UpdateFieldOption);
						updateField(p_BU.GetDrive()->GetQuota()->Total, p_Row, m_ColumnDriveQuotaTotal, p_UpdateFieldOption);
					}
					if (p_BU.GetDrive()->GetCreatedBy())
					{
						if (p_BU.GetDrive()->GetCreatedBy()->User)
						{
							updateField(p_BU.GetDrive()->GetCreatedBy()->User->DisplayName, p_Row, m_ColumnDriveCreatedByUserEmail, p_UpdateFieldOption);
							updateField(p_BU.GetDrive()->GetCreatedBy()->User->Id, p_Row, m_ColumnDriveCreatedByUserId, p_UpdateFieldOption);
						}
						if (p_BU.GetDrive()->GetCreatedBy()->Application)
						{
							updateField(p_BU.GetDrive()->GetCreatedBy()->Application->DisplayName, p_Row, m_ColumnDriveCreatedByAppName, p_UpdateFieldOption);
							updateField(p_BU.GetDrive()->GetCreatedBy()->Application->Id, p_Row, m_ColumnDriveCreatedByAppId, p_UpdateFieldOption);
						}
						if (p_BU.GetDrive()->GetCreatedBy()->Device)
						{
							updateField(p_BU.GetDrive()->GetCreatedBy()->Device->DisplayName, p_Row, m_ColumnDriveCreatedByDeviceName, p_UpdateFieldOption);
							updateField(p_BU.GetDrive()->GetCreatedBy()->Device->Id, p_Row, m_ColumnDriveCreatedByDeviceId, p_UpdateFieldOption);
						}
					}
					if (p_BU.GetDrive()->GetOwner())
					{
						if (p_BU.GetDrive()->GetOwner()->User)
						{
							updateField(p_BU.GetDrive()->GetOwner()->User->DisplayName, p_Row, m_ColumnDriveOwnerUserName, p_UpdateFieldOption);
							updateField(p_BU.GetDrive()->GetOwner()->User->Id, p_Row, m_ColumnDriveOwnerUserId, p_UpdateFieldOption);
						}
						if (p_BU.GetDrive()->GetOwner()->Application)
						{
							updateField(p_BU.GetDrive()->GetOwner()->Application->DisplayName, p_Row, m_ColumnDriveOwnerAppName, p_UpdateFieldOption);
							updateField(p_BU.GetDrive()->GetOwner()->Application->Id, p_Row, m_ColumnDriveOwnerAppId, p_UpdateFieldOption);
						}
						if (p_BU.GetDrive()->GetOwner()->Device)
						{
							updateField(p_BU.GetDrive()->GetOwner()->Device->DisplayName, p_Row, m_ColumnDriveOwnerDeviceName, p_UpdateFieldOption);
							updateField(p_BU.GetDrive()->GetOwner()->Device->Id, p_Row, m_ColumnDriveOwnerDeviceId, p_UpdateFieldOption);
						}
					}
				}
			}
			else
			{
				// FIXME: Should we clear fields?
				// Only if UpdateFieldOption::NONE == p_UpdateFieldOption, as not handler in GetBusinessUser

			}

			// Manager
			if (nullptr != m_ColumnManagerID || nullptr != m_ColumnManagerDisplayName || nullptr != m_ColumnManagerUserPrincipalName)
			{
				if (p_BU.GetManagerError())
				{
					ASSERT(p_UpdateFieldOption == GridTemplate::UpdateFieldOption::NONE);
					setErrorField(updateField(p_BU.GetManagerError()->GetFullErrorMessage(), p_Row, m_ColumnManagerID, GridTemplate::UpdateFieldOption::NONE));
					setErrorField(updateField(p_BU.GetManagerError()->GetFullErrorMessage(), p_Row, m_ColumnManagerDisplayName, GridTemplate::UpdateFieldOption::NONE));
					setErrorField(updateField(p_BU.GetManagerError()->GetFullErrorMessage(), p_Row, m_ColumnManagerUserPrincipalName, GridTemplate::UpdateFieldOption::NONE));
				}
				else 
				{
					GridBackendField* newField{ nullptr };

					auto processManager = [this, &p_Grid, &p_BU, &newField, &p_Row]()
					{
						if (!p_BU.GetManagerID())
						{
							if (nullptr != m_ColumnManagerID)
								newField = &GridUtil::AddFieldImpl(p_Row, boost::YOpt<PooledString>(), m_ColumnManagerID);
							if (nullptr != m_ColumnManagerDisplayName)
								GridUtil::AddFieldImpl(p_Row, boost::YOpt<PooledString>(), m_ColumnManagerDisplayName);
							if (nullptr != m_ColumnManagerUserPrincipalName)
								GridUtil::AddFieldImpl(p_Row, boost::YOpt<PooledString>(), m_ColumnManagerUserPrincipalName);
						}
						else if (p_BU.GetManagerID() && p_BU.GetManagerID()->IsEmpty())
						{
							if (nullptr != m_ColumnManagerID)
								newField = &GridUtil::AddFieldImpl(p_Row, wstring(), m_ColumnManagerID);
							if (nullptr != m_ColumnManagerDisplayName)
								GridUtil::AddFieldImpl(p_Row, wstring(), m_ColumnManagerDisplayName);
							if (nullptr != m_ColumnManagerUserPrincipalName)
								GridUtil::AddFieldImpl(p_Row, wstring(), m_ColumnManagerUserPrincipalName);
						}
						else
						{
							if (nullptr != m_ColumnManagerID)
								newField = &GridUtil::AddFieldImpl(p_Row, p_BU.GetManagerID(), m_ColumnManagerID);

							if (nullptr != m_ColumnManagerDisplayName || nullptr != m_ColumnManagerUserPrincipalName)
							{
								auto user = p_Grid.GetGraphCache().GetUserInCache(*p_BU.GetManagerID(), false);
								ASSERT(user.GetID() == *p_BU.GetManagerID());
								if (user.GetID() == *p_BU.GetManagerID())
								{
									if (nullptr != m_ColumnManagerDisplayName)
										GridUtil::AddFieldImpl(p_Row, user.GetDisplayName(), m_ColumnManagerDisplayName);
									if (nullptr != m_ColumnManagerUserPrincipalName)
										GridUtil::AddFieldImpl(p_Row, user.GetUserPrincipalName(), m_ColumnManagerUserPrincipalName);
								}
								else
								{
									const wstring errorMessage = YtriaTranslate::DoError(GridTemplateUsers_updateRow_1, _YLOC("Manager not found in cache."),_YR("Y2420")).c_str();
									if (nullptr != m_ColumnManagerDisplayName)
										setErrorField(&GridUtil::AddFieldImpl(p_Row, errorMessage, m_ColumnManagerDisplayName));
									if (nullptr != m_ColumnManagerUserPrincipalName)
										setErrorField(&GridUtil::AddFieldImpl(p_Row, errorMessage, m_ColumnManagerUserPrincipalName));
								}
							}
						}
					};

					if (nullptr != m_ColumnManagerID && p_UpdateFieldOption == GridTemplate::UpdateFieldOption::IS_MODIFICATION)
					{
						O365Grid* grid = nullptr != p_Row ? dynamic_cast<O365Grid*>(p_Row->GetBackend()->GetCacheGrid()) : nullptr;
						ASSERT(nullptr != grid);
						if (nullptr != grid)
						{
							vector<GridBackendField> rowPK;
							if (UpdateFieldOption::NONE != p_UpdateFieldOption)
								grid->GetRowPK(p_Row, rowPK);

							const auto colID = m_ColumnManagerID->GetID();
							const bool hadField = p_Row->HasField(colID);
							const GridBackendField oldField = hadField ? p_Row->GetField(colID) : GridBackendField();

							const GridBackendField oldNameField = (hadField && nullptr != m_ColumnManagerDisplayName) ? p_Row->GetField(m_ColumnManagerDisplayName->GetID()) : GridBackendField();
							const GridBackendField oldPrincNameField = (hadField && nullptr != m_ColumnManagerUserPrincipalName) ? p_Row->GetField(m_ColumnManagerUserPrincipalName->GetID()) : GridBackendField();

							std::map<UINT, const GridBackendField*> sisterFields;
							if (nullptr != m_ColumnManagerDisplayName)
								sisterFields[m_ColumnManagerDisplayName->GetID()] = &oldNameField;
							if (nullptr != m_ColumnManagerUserPrincipalName)
								sisterFields[m_ColumnManagerUserPrincipalName->GetID()] = &oldPrincNameField;

							processManager();

							ASSERT(nullptr != newField);
							if (nullptr != newField
								&& (UpdateFieldOption::IS_OBJECT_CREATION == p_UpdateFieldOption || newField->IsModified())
								&& (hadField || !hadField && !newField->IsGeneric()))
							{
								if (UpdateFieldOption::IS_MODIFICATION == p_UpdateFieldOption)
									grid->GetModifications().Add(std::make_unique<SisterhoodFieldUpdate>(*grid, rowPK, colID, hadField ? oldField : GridBackendField::g_GenericConstField, *newField, sisterFields));

								// Show column containing modification if hidden (only if not technical)
								for (auto column : { m_ColumnManagerID , m_ColumnManagerDisplayName , m_ColumnManagerUserPrincipalName })
								{
									ASSERT(nullptr != column);
									if (nullptr != column && !column->IsVisible() && !column->HasPresetFlag(CacheGrid::g_ColumnsPresetTech))
										grid->GetBackend().SetColumnVisible(column, true, false);
								}
							}
						}
					}
					else
					{
						ASSERT(!p_BU.GetManagerID() || p_UpdateFieldOption == GridTemplate::UpdateFieldOption::NONE);
						processManager();
					}
				}
			}
			//////////////////////

			if (nullptr != m_ColumnSkuIds || nullptr != m_ColumnSkuPartNumbers || nullptr != m_ColumnSkuNames)
			{
				if (!p_BU.GetAssignedLicenses() || (p_BU.GetAssignedLicenses() && p_BU.GetAssignedLicenses()->empty()))
				{
					updateField(boost::YOpt<PooledString>(), p_Row, m_ColumnSkuIds, p_UpdateFieldOption);
					updateField(BusinessUserConfiguration::GetInstance().GetValueStringLicenses_Unlicensed(), p_Row, m_ColumnSkuNames, p_UpdateFieldOption);
					updateField(BusinessUserConfiguration::GetInstance().GetValueStringLicenses_Unlicensed(), p_Row, m_ColumnSkuPartNumbers, p_UpdateFieldOption);
				}
				else if (p_BU.GetAssignedLicenses())
				{
					vector<PooledString> skuIds;
					vector<PooledString> skuPartNumbers;
					vector<PooledString> skuNames;
					skuIds.reserve(p_BU.GetAssignedLicenses()->size());
					skuPartNumbers.reserve(p_BU.GetAssignedLicenses()->size());
					skuNames.reserve(p_BU.GetAssignedLicenses()->size());

					static const wstring errorUnknown = _YERROR("-- Unknown SKU --"); // GridBackendUtil::g_NoValueString ??
					bool error = false;
					for (const auto& assignedLicense : *p_BU.GetAssignedLicenses())
					{
						if (nullptr != m_ColumnSkuIds)
							skuIds.emplace_back(assignedLicense.GetID());

						if (nullptr != m_ColumnSkuPartNumbers || nullptr != m_ColumnSkuNames)
						{
							const auto& skuPartNumber = SkuAndPlanReference::GetInstance().GetSkuPartNumber(assignedLicense.GetID());
							const auto& label = SkuAndPlanReference::GetInstance().GetSkuLabel(assignedLicense.GetID());

							if (skuPartNumber.IsEmpty())
							{
								error = true;
								skuPartNumbers.emplace_back(errorUnknown);
							}
							else
							{
								skuPartNumbers.emplace_back(skuPartNumber);
							}

							if (label.IsEmpty())
							{
								if (skuPartNumber.IsEmpty())
									skuNames.emplace_back(errorUnknown);
								else
									skuNames.emplace_back(skuPartNumber);
							}
							else
							{
								skuNames.emplace_back(label);
							}
						}
					}

					if (nullptr != m_ColumnSkuIds)
						updateField(skuIds, p_Row, m_ColumnSkuIds, p_UpdateFieldOption);
					if (nullptr != m_ColumnSkuPartNumbers)
					{
						auto field = updateField(skuPartNumbers, p_Row, m_ColumnSkuPartNumbers, p_UpdateFieldOption);
						ASSERT(nullptr != field && !field->IsGeneric());
						if (error && nullptr != field && !field->IsGeneric())
							field->SetIcon(GridBackendUtil::ICON_ERROR);
					}
					if (nullptr != m_ColumnSkuNames)
						updateField(skuNames, p_Row, m_ColumnSkuNames, p_UpdateFieldOption);
				}
			}
			else
			{
				updateField(boost::YOpt<PooledString>(), p_Row, m_ColumnSkuIds, p_UpdateFieldOption);
				updateField(boost::YOpt<PooledString>(), p_Row, m_ColumnSkuPartNumbers, p_UpdateFieldOption);
				updateField(boost::YOpt<PooledString>(), p_Row, m_ColumnSkuNames, p_UpdateFieldOption);
			}

			if (p_BU.GetDataDate(UBI::MAILBOX) || p_BU.GetDataDate(UBI::RECIPIENTPERMISSIONS))
				updateRowMailbox(p_BU, p_Row, p_UpdateFieldOption);
		}

		// Only for edited users (not created)
		if (!p_BU.HasFlag(BusinessObject::CREATED))
		{
			updateField(p_BU.GetAboutMe(), p_Row, m_ColumnAboutMe, p_UpdateFieldOption);
			updateField(p_BU.GetBirthday(), p_Row, m_ColumnBirthday, p_UpdateFieldOption);
			updateField(p_BU.GetHireDate(), p_Row, m_ColumnHireDate, p_UpdateFieldOption);
			updateField(p_BU.GetInterests(), p_Row, m_ColumnInterests, p_UpdateFieldOption);
			updateField(p_BU.GetImAddresses(), p_Row, m_ColumnImAddresses, p_UpdateFieldOption);
			updateField(p_BU.GetMail(), p_Row, m_ColumnMail, p_UpdateFieldOption);

			updateRowMailboxSettings(p_BU, p_Row, p_UpdateFieldOption);

			updateField(p_BU.GetMySite(), p_Row, m_ColumnMySite, p_UpdateFieldOption);
			updateField(p_BU.GetOnPremisesLastSyncDateTime(), p_Row, m_ColumnOPLastSync, p_UpdateFieldOption);
			updateField(p_BU.GetOnPremisesSecurityIdentifier(), p_Row, m_ColumnOPSecurityID, p_UpdateFieldOption);
			updateField(p_BU.GetOnPremisesSyncEnabled(), p_Row, m_ColumnOPSyncEnabled, p_UpdateFieldOption);
			updateField(p_BU.GetPastProjects(), p_Row, m_ColumnPastProjects, p_UpdateFieldOption);
			updateField(p_BU.GetPreferredName(), p_Row, m_ColumnPreferredName, p_UpdateFieldOption);
			updateField(p_BU.GetProxyAddresses(), p_Row, m_ColumnProxyAddresses, p_UpdateFieldOption);
			updateField(p_BU.GetResponsibilities(), p_Row, m_ColumnResponsibilities, p_UpdateFieldOption);
			updateField(p_BU.GetSchools(), p_Row, m_ColumnSchools, p_UpdateFieldOption);
			updateField(p_BU.GetSkills(), p_Row, m_ColumnSkills, p_UpdateFieldOption);
			updateField(p_BU.GetAgeGroup(), p_Row, m_ColumnAgeGroup, p_UpdateFieldOption);
			updateField(p_BU.GetCreatedDateTime(), p_Row, m_ColumnCreatedDateTime, p_UpdateFieldOption);
			updateField(p_BU.GetConsentProvidedForMinor(), p_Row, m_ColumnConsentProvidedForMinor, p_UpdateFieldOption);
			updateField(p_BU.GetLegalAgeGroupClassification(), p_Row, m_ColumnLegalAgeGroupClassification, p_UpdateFieldOption);
			updateField(p_BU.GetSignInSessionsValidFromDateTime(), p_Row, m_ColumnSignInSessionsValidFromDateTime, p_UpdateFieldOption);
			updateField(p_BU.GetOnPremisesDomainName(), p_Row, m_ColumnOPDomainName, p_UpdateFieldOption);
			updateField(p_BU.GetOnPremisesSamAccountName(), p_Row, m_ColumnOPSamAccountName, p_UpdateFieldOption);
			updateField(p_BU.GetOnPremisesUserPrincipalName(), p_Row, m_ColumnOPUserPrincipalName, p_UpdateFieldOption);

			if (nullptr != m_ColumnOPProvisioningErrorsCategory
				|| nullptr != m_ColumnOPProvisioningErrorsOccuredDateTime
				|| nullptr != m_ColumnOPProvisioningErrorsPropertyCausingError
				|| nullptr != m_ColumnOPProvisioningErrorsValue)
			{
				boost::YOpt<vector<PooledString>> categories;
				boost::YOpt<vector<YTimeDate>> occurredDataTimes;
				boost::YOpt<vector<PooledString>> properties;
				boost::YOpt<vector<PooledString>> values;

				auto& oppe = p_BU.GetOnPremisesProvisioningErrors();
				if (oppe)
				{
					categories.emplace();
					occurredDataTimes.emplace();
					properties.emplace();
					values.emplace();

					for (auto& e : *oppe)
					{
						if (e.m_Category)
							categories->push_back(*e.m_Category);
						else
							categories->push_back(GridBackendUtil::g_NoValueString);

						if (e.m_OccurredDateTime)
							occurredDataTimes->push_back(*e.m_OccurredDateTime);
						else
							occurredDataTimes->push_back(GridBackendUtil::g_NoValueDate);

						if (e.m_PropertyCausingError)
							properties->push_back(*e.m_PropertyCausingError);
						else
							properties->push_back(GridBackendUtil::g_NoValueString);

						if (e.m_Value)
							values->push_back(*e.m_Value);
						else
							values->push_back(GridBackendUtil::g_NoValueString);
					}

					ASSERT(categories->size() == occurredDataTimes->size()
						&& categories->size() == properties->size()
						&& categories->size() == values->size()
						&& categories->size() == oppe->size());
				}

				// Do not clear fields if coming from Edit: it's not editable but empty because not handled in GetBusinessUser()
				if (categories || UpdateFieldOption::NONE == p_UpdateFieldOption)
				{
					updateField(categories, p_Row, m_ColumnOPProvisioningErrorsCategory, p_UpdateFieldOption);
					updateField(occurredDataTimes, p_Row, m_ColumnOPProvisioningErrorsOccuredDateTime, p_UpdateFieldOption);
					updateField(properties, p_Row, m_ColumnOPProvisioningErrorsPropertyCausingError, p_UpdateFieldOption);
					updateField(values, p_Row, m_ColumnOPProvisioningErrorsValue, p_UpdateFieldOption);
				}
			}

			updateField(p_BU.GetOtherMails(), p_Row, m_ColumnOtherMails, p_UpdateFieldOption);
			updateField(p_BU.GetPreferredDataLocation(), p_Row, m_ColumnPreferredDataLocation, p_UpdateFieldOption);
			updateField(p_BU.GetShowInAddressList(), p_Row, m_ColumnShowInAddressList, p_UpdateFieldOption);
			updateField(p_BU.GetExternalUserStatus(), p_Row, m_ColumnExternalUserStatus, p_UpdateFieldOption);
			updateField(p_BU.GetExternalUserStatusChangedOn(), p_Row, m_ColumnExternalUserStatusChangedOn, p_UpdateFieldOption);

			// Store this result?
			const auto hasAttributeColumn = [this]()
			{
				for (int i = 0; i < g_NumOPExtensionAttributes; ++i)
				{
					if (nullptr != GetOPAttributeColumn(i))
						return true;
				}
				return false;
			};

			if (hasAttributeColumn() && p_BU.GetOnPremisesExtensionAttributes())
			{
				std::vector<boost::YOpt<PooledString> OnPremisesExtensionAttributes::*> attributes
				{
						&OnPremisesExtensionAttributes::m_ExtensionAttribute1
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute2
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute3
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute4
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute5
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute6
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute7
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute8
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute9
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute10
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute11
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute12
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute13
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute14
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute15
				};
				for (int i = 0; i < g_NumOPExtensionAttributes; ++i)
					updateField((*p_BU.GetOnPremisesExtensionAttributes()).*(attributes[i]), p_Row, GetOPAttributeColumn(i), p_UpdateFieldOption);
			}

			updateField(p_BU.GetEmployeeID(), p_Row, m_ColumnEmployeeID, p_UpdateFieldOption);
			updateField(p_BU.GetIsResourceAccount(), p_Row, m_ColumnIsResourceAccount, p_UpdateFieldOption);
			updateField(p_BU.GetOnPremisesDistinguishedName(), p_Row, m_ColumnOPDistinguishedName, p_UpdateFieldOption);

			{
				PooledString creationType;
				if (p_BU.GetCreationType())
					creationType = *p_BU.GetCreationType();

				auto it = g_CreationTypes.left.find(creationType);
				if (g_CreationTypes.left.end() != it)
					creationType = it->second;

				updateField(creationType, p_Row, m_ColumnCreationType, p_UpdateFieldOption);
			}

			if (nullptr != m_ColumnIdentitiesSignInType
				|| nullptr != m_ColumnIdentitiesIssuer
				|| nullptr != m_ColumnIdentitiesIssuerAssignedId)
			{
				boost::YOpt<vector<PooledString>> signInTypes;
				boost::YOpt<vector<PooledString>> issuers;
				boost::YOpt<vector<PooledString>> issuerAssignedIds;
				if (p_BU.GetIdentities())
				{
					signInTypes.emplace();
					issuers.emplace();
					issuerAssignedIds.emplace();
					for (const auto& identity : *p_BU.GetIdentities())
					{
						if (identity.m_SignInType)
							signInTypes->emplace_back(*identity.m_SignInType);
						else
							signInTypes->emplace_back(GridBackendUtil::g_NoValueString);

						if (identity.m_Issuer)
							issuers->emplace_back(*identity.m_Issuer);
						else
							issuers->emplace_back(GridBackendUtil::g_NoValueString);

						if (identity.m_IssuerAssignedId)
							issuerAssignedIds->emplace_back(*identity.m_IssuerAssignedId);
						else
							issuerAssignedIds->emplace_back(GridBackendUtil::g_NoValueString);
					}
				}

				// FIXME: Do not clear fields if coming from Edit: it's not editable but empty because not handled in GetBusinessUser()
				if (signInTypes || UpdateFieldOption::NONE == p_UpdateFieldOption)
				{
					updateField(signInTypes, p_Row, m_ColumnIdentitiesSignInType, p_UpdateFieldOption);
					updateField(issuers, p_Row, m_ColumnIdentitiesIssuer, p_UpdateFieldOption);
					updateField(issuerAssignedIds, p_Row, m_ColumnIdentitiesIssuerAssignedId, p_UpdateFieldOption);
				}
			}
			
			updateField(p_BU.GetLastPasswordChangeDateTime(), p_Row, m_ColumnLastPasswordChangeDateTime, p_UpdateFieldOption);

			if (nullptr != m_ColumnLicenseAssignmentStatesSkuId
				|| nullptr != m_ColumnLicenseAssignmentStatesSkuPartNumber
				|| nullptr != m_ColumnLicenseAssignmentStatesSkuName
				|| nullptr != m_ColumnLicenseAssignmentStatesAssignedByGroupId
				|| nullptr != m_ColumnLicenseAssignmentStatesAssignedByGroupDisplayName
				|| nullptr != m_ColumnLicenseAssignmentStatesAssignedByGroupMail
				|| nullptr != m_ColumnLicenseAssignmentStatesState
				|| nullptr != m_ColumnLicenseAssignmentStatesError
				|| nullptr != m_ColumnIsAnyLicenseAssignedByGroup)
			{
				boost::YOpt<vector<PooledString>> skuIds;
				boost::YOpt<vector<PooledString>> skuPartNumbers;
				boost::YOpt<vector<PooledString>> skuNames;
				boost::YOpt<vector<PooledString>> assignedByGroupIds;
				boost::YOpt<vector<PooledString>> assignedByGroupDisplayNames;
				boost::YOpt<vector<PooledString>> assignedByGroupMails;
				boost::YOpt<vector<PooledString>> states;
				boost::YOpt<vector<PooledString>> errors;
				boost::YOpt<bool> groupLicensing;
				bool error = false;
				if (p_BU.GetLicenseAssignmentStates())
				{
					skuIds.emplace();
					skuPartNumbers.emplace();
					skuNames.emplace();
					assignedByGroupIds.emplace();
					assignedByGroupDisplayNames.emplace();
					assignedByGroupMails.emplace();
					states.emplace();
					errors.emplace();
					groupLicensing = false;
					static const wstring errorUnknown = _YERROR("-- Unknown SKU --"); // GridBackendUtil::g_NoValueString ??
					for (const auto& las : *p_BU.GetLicenseAssignmentStates())
					{
						if (las.m_SkuId)
						{
							if (nullptr != m_ColumnLicenseAssignmentStatesSkuId)
								skuIds->emplace_back(*las.m_SkuId);

							if (nullptr != m_ColumnLicenseAssignmentStatesSkuPartNumber || nullptr != m_ColumnLicenseAssignmentStatesSkuName)
							{
								const auto& skuPartNumber = SkuAndPlanReference::GetInstance().GetSkuPartNumber(*las.m_SkuId);
								const auto& label = SkuAndPlanReference::GetInstance().GetSkuLabel(*las.m_SkuId);

								if (nullptr != m_ColumnLicenseAssignmentStatesSkuPartNumber)
								{
									if (skuPartNumber.IsEmpty())
									{
										error = true;
										skuPartNumbers->emplace_back(errorUnknown);
									}
									else
									{
										skuPartNumbers->emplace_back(skuPartNumber);
									}
								}

								if (nullptr != m_ColumnLicenseAssignmentStatesSkuName)
								{
									if (!label.IsEmpty())
										skuNames->emplace_back(label);
									else if (!skuPartNumber.IsEmpty())
										skuNames->emplace_back(skuPartNumber);
									else
										skuNames->emplace_back(errorUnknown);
								}
							}
						}
						else
						{
							skuIds->emplace_back(GridBackendUtil::g_NoValueString);
							skuNames->emplace_back(GridBackendUtil::g_NoValueString);
							skuPartNumbers->emplace_back(GridBackendUtil::g_NoValueString);
						}

						if (nullptr != m_ColumnLicenseAssignmentStatesAssignedByGroupId
							|| nullptr != m_ColumnLicenseAssignmentStatesAssignedByGroupDisplayName
							|| nullptr != m_ColumnLicenseAssignmentStatesAssignedByGroupMail)
						{
							if (las.m_AssignedByGroup)
							{
								assignedByGroupIds->emplace_back(*las.m_AssignedByGroup);
								groupLicensing = true;
								auto g = p_Grid.GetGraphCache().GetGroupInCache(*las.m_AssignedByGroup);
								ASSERT(g.GetID() == *las.m_AssignedByGroup);
								assignedByGroupDisplayNames->emplace_back(g.GetDisplayName());
								if (g.GetMail())
									assignedByGroupMails->emplace_back(*g.GetMail());
								else
									assignedByGroupMails->emplace_back(GridBackendUtil::g_NoValueString);
							}
							else
							{
								assignedByGroupIds->emplace_back(GridBackendUtil::g_NoValueString);
								assignedByGroupDisplayNames->emplace_back(GridBackendUtil::g_NoValueString);
								assignedByGroupMails->emplace_back(GridBackendUtil::g_NoValueString);
							}
						}

						if (nullptr != m_ColumnLicenseAssignmentStatesState)
						{
							if (las.m_State)
								states->emplace_back(*las.m_State);
							else
								states->emplace_back(GridBackendUtil::g_NoValueString);
						}

						if (nullptr != m_ColumnLicenseAssignmentStatesError)
						{
							if (las.m_Error)
								errors->emplace_back(*las.m_Error);
							else
								errors->emplace_back(GridBackendUtil::g_NoValueString);
						}
					}
				}
				// FIXME: Do not clear fields if coming from Edit: it's not editable but empty because not handled in GetBusinessUser()
				if (skuIds || UpdateFieldOption::NONE == p_UpdateFieldOption)
				{
					updateField(skuIds, p_Row, m_ColumnLicenseAssignmentStatesSkuId, p_UpdateFieldOption);
					auto field = updateField(skuPartNumbers, p_Row, m_ColumnLicenseAssignmentStatesSkuPartNumber, p_UpdateFieldOption);
					ASSERT(!error || nullptr != field && !field->IsGeneric());
					if (error && nullptr != field && !field->IsGeneric())
						field->SetIcon(GridBackendUtil::ICON_ERROR);
					updateField(skuNames, p_Row, m_ColumnLicenseAssignmentStatesSkuName, p_UpdateFieldOption);
					updateField(assignedByGroupIds, p_Row, m_ColumnLicenseAssignmentStatesAssignedByGroupId, p_UpdateFieldOption);
					updateField(assignedByGroupDisplayNames, p_Row, m_ColumnLicenseAssignmentStatesAssignedByGroupDisplayName, p_UpdateFieldOption);
					updateField(assignedByGroupMails, p_Row, m_ColumnLicenseAssignmentStatesAssignedByGroupMail, p_UpdateFieldOption);
					updateField(states, p_Row, m_ColumnLicenseAssignmentStatesState, p_UpdateFieldOption);
					updateField(errors, p_Row, m_ColumnLicenseAssignmentStatesError, p_UpdateFieldOption);
					updateField(groupLicensing, p_Row, m_ColumnIsAnyLicenseAssignedByGroup, p_UpdateFieldOption);
				}
			}

			updateField(p_BU.GetRefresTokenValidFromDateTime(), p_Row, m_ColumnRefresTokenValidFromDateTime, p_UpdateFieldOption);
		}

		if (p_UpdateFieldOption == UpdateFieldOption::NONE) // Only load
			UpdateCommonInfo(p_Row);
	}
}

template<typename T>
GridBackendField* GridTemplateUsers::updateField(const T& p_Value, GridBackendRow* p_Row, GridBackendColumn* p_Col, UpdateFieldOption p_UpdateFieldOption, const PooledString& p_AlternateStringValue/* = PooledString()*/)
{
	ASSERT(p_AlternateStringValue.IsEmpty() || p_Col == m_ColumnAccountEnabled);

	GridBackendField* newField{ nullptr };
	if (nullptr != p_Col)
	{
		ASSERT(nullptr != p_Row);
        O365Grid* grid = nullptr != p_Row ? dynamic_cast<O365Grid*>(p_Row->GetBackend()->GetCacheGrid()) : nullptr;
		ASSERT(nullptr != grid);
		if (nullptr != grid)
		{
			const auto colID = p_Col->GetID();
			const bool hadField = p_Row->HasField(colID);
			const GridBackendField oldField = UpdateFieldOption::NONE != p_UpdateFieldOption ? p_Row->GetField(colID) : GridBackendField();

			// Do not update a field which had an error
			if (UpdateFieldOption::NONE != p_UpdateFieldOption && oldField.GetIcon() == GridBackendUtil::ICON_ERROR)
			{
				newField = &p_Row->GetField(colID);
			}
			else
			{
				vector<GridBackendField> rowPK;
				if (UpdateFieldOption::NONE != p_UpdateFieldOption)
					grid->GetRowPK(p_Row, rowPK);
				if (p_Col == m_ColumnMailboxSettingsAutomaticRepliesSettingsInternalReplyMessage || p_Col == m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalReplyMessage)
				{
					rttr::variant variantValue = p_Value; // Todo: Make updateField a non-template function as it's not generic...
					if (variantValue.is_type<boost::YOpt<PooledString>>())
						newField = &p_Row->AddFieldForRichText(variantValue.get_value<boost::YOpt<PooledString>>(), p_Col);
					else
						newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col); // In case it's an error
				}
				else if (p_Col == m_ColumnMySite || p_Col == m_ColumnDriveWebUrl)
				{
					rttr::variant variantValue = p_Value; // Todo: Make updateField a non-template function as it's not generic...
					if (variantValue.is_type<boost::YOpt<PooledString>>())
						newField = &p_Row->AddFieldForHyperlink(variantValue.get_value<boost::YOpt<PooledString>>(), p_Col);
					else
						newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col); // In case it's an error
				}
				else if (p_Col == m_ColumnAccountEnabled)
				{
					// First set the bool (used by UpdatedObjectsGenerator)
					// then the string (which won't change the bool value) for the display.
					// If we set the string first, setting the bool would override it.

					// As we set a string after a bool, the AddField can't correctly set the "modified" flag on value update (changing from String Type to Bool Type).
					// So we handle it manually.
					const GridBackendField& oldAccountEnabledField = UpdateFieldOption::NONE != p_UpdateFieldOption ? oldField : p_Row->GetField(colID);
					rttr::variant variantValue = p_Value;
					ASSERT(variantValue.is_type<boost::YOpt<bool>>());
					const auto& optBool = variantValue.get_value<boost::YOpt<bool>>();
					const bool modified = (optBool.is_initialized() != oldAccountEnabledField.HasValue()) || (optBool && *optBool != oldAccountEnabledField.GetValueBool());

					newField = &p_Row->AddField(p_Value, m_ColumnAccountEnabled);

					if (newField->HasValue())
						newField->SetValue(p_AlternateStringValue);

					if (modified && p_Row->HasFlag(GridBackendUtil::ISFINALROW) && !p_Row->GetBackend()->IsIgnoreFieldModifications())
						newField->SetModified();
					else if (!newField->IsGeneric())
						newField->RemoveModified();
				}
				else if (p_Col == m_ColumnIsAnyLicenseAssignedByGroup)
				{
					rttr::variant variantVal = p_Value;
					if (rttr::type::get<boost::YOpt<bool>>() == variantVal.get_type())
					{
						auto val = variantVal.get_value<boost::YOpt<bool>>();
						if (val && *val)
						{
							newField = &GridUtil::AddFieldImpl(p_Row, g_GroupLicensing, p_Col);
							if (!newField->IsGeneric())
								newField->SetIcon(m_GroupLicensingIconId);
						}
						else
						{
							if (p_UpdateFieldOption != UpdateFieldOption::NONE)
							{
								newField = &GridUtil::AddFieldImpl(p_Row, Str::g_EmptyString, p_Col);
								newField->SetIcon(NO_ICON);
							}
							else
								p_Row->RemoveField(p_Col);
						}
					}
					else  // In case it's an error
					{
						newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col);
					}
				}
				else
				{
					newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col);
				}

				if (UpdateFieldOption::NONE != p_UpdateFieldOption)
				{
					ASSERT(p_Row->HasField(m_ColumnID));
					ASSERT(nullptr != newField);
					if (nullptr != newField
						&& (UpdateFieldOption::IS_OBJECT_CREATION == p_UpdateFieldOption || newField->IsModified())
						&& (hadField || !hadField && !newField->IsGeneric()))
					{
						if (UpdateFieldOption::IS_MODIFICATION == p_UpdateFieldOption)
						{
							if (p_Col == m_ColumnSignInSessionsValidFromDateTime)
								grid->GetModifications().Add(std::make_unique<RevokeAccessFieldUpdate>(*grid, rowPK, p_Col->GetID(), hadField ? oldField : GridBackendField::g_GenericConstField, *newField));
							else
								grid->GetModifications().Add(std::make_unique<FieldUpdateO365>(*grid, rowPK, p_Col->GetID(), hadField ? oldField : GridBackendField::g_GenericConstField, *newField, p_Col == m_ColumnPassword ? FieldUpdateFlags::IgnoreRemoteHasOldValueError : FieldUpdateFlags::None));
						}

						// Show column containing modification if hidden (only if not technical)
						{
							auto column = grid->GetColumnByID(newField->GetColID());
							ASSERT(nullptr != column);
							if (nullptr != column && !column->IsVisible() && !column->HasPresetFlag(CacheGrid::g_ColumnsPresetTech))
								grid->GetBackend().SetColumnVisible(column, true, false);
						}
					}
				}
			}
		}
	}

	return newField;
}

template<typename T>
GridBackendField* GridTemplateUsers::updateFieldOnPrem(const T& p_Value, GridBackendRow* p_Row, GridBackendColumn* p_Col, UpdateFieldOption p_UpdateFieldOption)
{
	ASSERT(nullptr == p_Col || p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetOnPrem));

	GridBackendField* newField{ nullptr };
	if (nullptr != p_Col)
	{
		ASSERT(nullptr != p_Row);
		GridUsers* grid = nullptr != p_Row ? dynamic_cast<GridUsers*>(p_Row->GetBackend()->GetCacheGrid()) : nullptr;
		ASSERT(nullptr != grid);
		if (nullptr != grid)
		{
			const auto colID = p_Col->GetID();
			const bool hadField = p_Row->HasField(colID);
			const GridBackendField oldField = UpdateFieldOption::NONE != p_UpdateFieldOption ? p_Row->GetField(colID) : GridBackendField();

			// Do not update a field which had an error
			if (UpdateFieldOption::NONE != p_UpdateFieldOption && oldField.GetIcon() == GridBackendUtil::ICON_ERROR)
			{
				newField = &p_Row->GetField(colID);
			}
			else
			{
				vector<GridBackendField> rowPK;
				if (UpdateFieldOption::NONE != p_UpdateFieldOption)
					grid->GetRowPK(p_Row, rowPK);
				
				if (p_Col->IsDate())
				{
					rttr::variant variantVal = p_Value;
					if (rttr::type::get<boost::YOpt<YTimeDate>>() == variantVal.get_type())
					{
						auto val = variantVal.get_value<boost::YOpt<YTimeDate>>();
						newField = &AddDateOrNeverField(val, *p_Row, *p_Col);
					}
					else  // In case it's an error
					{
						newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col);
					}
				}
				else
				{
					newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col);
				}

				if (UpdateFieldOption::NONE != p_UpdateFieldOption)
				{
					ASSERT(p_Row->HasField(m_ColumnID));
					ASSERT(nullptr != newField);
					if (nullptr != newField
						&& (UpdateFieldOption::IS_OBJECT_CREATION == p_UpdateFieldOption || newField->IsModified())
						&& (hadField || !hadField && !newField->IsGeneric()))
					{
						if (UpdateFieldOption::IS_MODIFICATION == p_UpdateFieldOption)
						{
							grid->GetOnPremModifications().Add(std::make_unique<FieldUpdateOnPrem>(*grid, rowPK, p_Col->GetID(), hadField ? oldField : GridBackendField::g_GenericConstField, *newField, FieldUpdateFlags::IgnoreRemoteValue));
						}

						// Show column containing modification if hidden (only if not technical)
						{
							auto column = grid->GetColumnByID(newField->GetColID());
							ASSERT(nullptr != column);
							if (nullptr != column && !column->IsVisible() && !column->HasPresetFlag(CacheGrid::g_ColumnsPresetTech))
								grid->GetBackend().SetColumnVisible(column, true, false);
						}
					}
				}
			}
		}
	}

	return newField;
}

void GridTemplateUsers::updateRowMailboxSettings(const BusinessUser& p_BU, GridBackendRow* p_Row, UpdateFieldOption p_UpdateFieldOption)
{
	if (p_BU.GetMailboxSettingsError())
	{
		vector<GridBackendField*> errorMailboxSettingsFields;

		auto pushBackIfNotNull = [&errorMailboxSettingsFields](auto* fieldPtr)
		{
			if (nullptr != fieldPtr)
				errorMailboxSettingsFields.push_back(fieldPtr);
		};

		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsArchiveFolderId, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsTimeZone, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsLanguageLocale, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsLanguageDisplayName, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsStatus, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeDateTime, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeTimeZone, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeDateTime, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeTimeZone, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsInternalReplyMessage, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalReplyMessage, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalAudience, p_UpdateFieldOption));

		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsTimeFormat, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsDateFormat, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsDelegateMeetingMessageDeliveryOptions, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsWorkingHoursDaysOfWeek, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsWorkingHoursStartTime, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsWorkingHoursEndTime, p_UpdateFieldOption));
		pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsWorkingHoursTimeZoneName, p_UpdateFieldOption));

		// Should contains "skipped" in case of Mailbox settings error.
		ASSERT(p_BU.GetArchiveFolderNameError());
		if (p_BU.GetArchiveFolderNameError())
			pushBackIfNotNull(updateField(p_BU.GetArchiveFolderNameError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsArchiveFolderName, p_UpdateFieldOption));
		else
			pushBackIfNotNull(updateField(p_BU.GetMailboxSettingsError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsArchiveFolderName, p_UpdateFieldOption));

		for (auto& field : errorMailboxSettingsFields)
		{
			ASSERT(nullptr != field && !field->IsGeneric());
			if (nullptr != field && !field->IsGeneric())
				field->SetIcon(GridBackendUtil::ICON_ERROR);
		}
	}
	else if (p_BU.GetMailboxSettings())
	{
		// Mailbox settings can't be edited right now. 
		ASSERT(UpdateFieldOption::NONE == p_UpdateFieldOption);
		auto updateFieldOption = UpdateFieldOption::NONE/*p_UpdateFieldOption*/;

		const auto& mailboxSettings = p_BU.GetMailboxSettings();

		updateField(mailboxSettings->ArchiveFolder, p_Row, m_ColumnMailboxSettingsArchiveFolderId, updateFieldOption);

		if (p_BU.GetArchiveFolderNameError())
		{
			auto field = updateField(p_BU.GetArchiveFolderNameError()->GetFullErrorMessage(), p_Row, m_ColumnMailboxSettingsArchiveFolderName, updateFieldOption);
			ASSERT(nullptr == field || !field->IsGeneric());
			if (nullptr != field && !field->IsGeneric())
				field->SetIcon(GridBackendUtil::ICON_ERROR);
		}
		else
		{
			updateField(p_BU.GetArchiveFolderName(), p_Row, m_ColumnMailboxSettingsArchiveFolderName, updateFieldOption);
		}

		updateField(mailboxSettings->TimeZone, p_Row, m_ColumnMailboxSettingsTimeZone, updateFieldOption);
		if (mailboxSettings->Language)
		{
			updateField(mailboxSettings->Language->Locale, p_Row, m_ColumnMailboxSettingsLanguageLocale, updateFieldOption);
			updateField(mailboxSettings->Language->DisplayName, p_Row, m_ColumnMailboxSettingsLanguageDisplayName, updateFieldOption);
		}
		else
		{
			if (nullptr != m_ColumnMailboxSettingsLanguageLocale)
				p_Row->RemoveField(m_ColumnMailboxSettingsLanguageLocale);
			if (nullptr != m_ColumnMailboxSettingsLanguageDisplayName)
				p_Row->RemoveField(m_ColumnMailboxSettingsLanguageDisplayName);
		}

		if (mailboxSettings->AutomaticRepliesSetting)
		{
			const auto& automaticRepliesSetting = mailboxSettings->AutomaticRepliesSetting;
			updateField(automaticRepliesSetting->Status, p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsStatus, updateFieldOption);
			if (automaticRepliesSetting->ScheduledStartDateTime)
			{
				updateField(automaticRepliesSetting->ScheduledStartDateTime->DateTime, p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeDateTime, updateFieldOption);
				updateField(automaticRepliesSetting->ScheduledStartDateTime->TimeZone, p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeTimeZone, updateFieldOption);
			}
			else
			{
				if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeDateTime)
					p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeDateTime);
				if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeTimeZone)
					p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeTimeZone);
			}
			if (automaticRepliesSetting->ScheduledEndDateTime)
			{
				updateField(automaticRepliesSetting->ScheduledEndDateTime->DateTime, p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeDateTime, updateFieldOption);
				updateField(automaticRepliesSetting->ScheduledEndDateTime->TimeZone, p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeTimeZone, updateFieldOption);
			}
			else
			{
				if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeDateTime)
					p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeDateTime);
				if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeTimeZone)
					p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeTimeZone);
			}
			updateField(automaticRepliesSetting->InternalReplyMessage, p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsInternalReplyMessage, updateFieldOption);
			updateField(automaticRepliesSetting->ExternalReplyMessage, p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalReplyMessage, updateFieldOption);
			updateField(automaticRepliesSetting->ExternalAudience, p_Row, m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalAudience, updateFieldOption);
		}

		updateField(mailboxSettings->DateFormat, p_Row, m_ColumnMailboxSettingsDateFormat, updateFieldOption);
		updateField(mailboxSettings->DelegateMeetingMessageDeliveryOptions, p_Row, m_ColumnMailboxSettingsDelegateMeetingMessageDeliveryOptions, updateFieldOption);
		updateField(mailboxSettings->TimeFormat, p_Row, m_ColumnMailboxSettingsTimeFormat, updateFieldOption);

		if (mailboxSettings->WorkingHours)
		{
			const auto& workingHours = mailboxSettings->WorkingHours;
			updateField(workingHours->DaysOfWeek, p_Row, m_ColumnMailboxSettingsWorkingHoursDaysOfWeek, updateFieldOption);
			updateField(workingHours->StartTime, p_Row, m_ColumnMailboxSettingsWorkingHoursStartTime, updateFieldOption);
			updateField(workingHours->EndTime, p_Row, m_ColumnMailboxSettingsWorkingHoursEndTime, updateFieldOption);

			if (workingHours->TimeZone)
				updateField(workingHours->TimeZone->Name, p_Row, m_ColumnMailboxSettingsWorkingHoursTimeZoneName, updateFieldOption);
		}
	}
	else if (UpdateFieldOption::NONE == p_UpdateFieldOption)
	{
		if (nullptr != m_ColumnMailboxSettingsArchiveFolderId)
			p_Row->RemoveField(m_ColumnMailboxSettingsArchiveFolderId);
		if (nullptr != m_ColumnMailboxSettingsArchiveFolderName)
			p_Row->RemoveField(m_ColumnMailboxSettingsArchiveFolderName);
		if (nullptr != m_ColumnMailboxSettingsTimeZone)
			p_Row->RemoveField(m_ColumnMailboxSettingsTimeZone);

		if (nullptr != m_ColumnMailboxSettingsLanguageLocale)
			p_Row->RemoveField(m_ColumnMailboxSettingsLanguageLocale);
		if (nullptr != m_ColumnMailboxSettingsLanguageDisplayName)
			p_Row->RemoveField(m_ColumnMailboxSettingsLanguageDisplayName);

		if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsStatus)
			p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsStatus);
		if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeDateTime)
			p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeDateTime);
		if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeTimeZone)
			p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledStartDateTimeTimeZone);
		if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeDateTime)
			p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeDateTime);
		if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeTimeZone)
			p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsScheduledEndDateTimeTimeZone);
		if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsInternalReplyMessage)
			p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsInternalReplyMessage);
		if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalReplyMessage)
			p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalReplyMessage);
		if (nullptr != m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalAudience)
			p_Row->RemoveField(m_ColumnMailboxSettingsAutomaticRepliesSettingsExternalAudience);

		if (nullptr != m_ColumnMailboxSettingsTimeFormat)
			p_Row->RemoveField(m_ColumnMailboxSettingsTimeFormat);
		if (nullptr != m_ColumnMailboxSettingsDateFormat)
			p_Row->RemoveField(m_ColumnMailboxSettingsDateFormat);
		if (nullptr != m_ColumnMailboxSettingsDelegateMeetingMessageDeliveryOptions)
			p_Row->RemoveField(m_ColumnMailboxSettingsDelegateMeetingMessageDeliveryOptions);
		if (nullptr != m_ColumnMailboxSettingsWorkingHoursDaysOfWeek)
			p_Row->RemoveField(m_ColumnMailboxSettingsWorkingHoursDaysOfWeek);
		if (nullptr != m_ColumnMailboxSettingsWorkingHoursStartTime)
			p_Row->RemoveField(m_ColumnMailboxSettingsWorkingHoursStartTime);
		if (nullptr != m_ColumnMailboxSettingsWorkingHoursEndTime)
			p_Row->RemoveField(m_ColumnMailboxSettingsWorkingHoursEndTime);
		if (nullptr != m_ColumnMailboxSettingsWorkingHoursTimeZoneName)
			p_Row->RemoveField(m_ColumnMailboxSettingsWorkingHoursTimeZoneName);
	}
}

void GridTemplateUsers::updateRowMailbox(const BusinessUser& p_BU, GridBackendRow* p_Row, UpdateFieldOption p_UpdateFieldOption)
{
	if (p_BU.GetMailboxInfoError())
	{
		if (nullptr != m_ColumnMailboxType)
			p_Row->AddField(p_BU.GetMailboxInfoError()->GetFullErrorMessage(), m_ColumnMailboxType, GridBackendUtil::ICON_ERROR);
		if (nullptr != m_ColumnMailboxForwardingAddress)
			p_Row->AddField(p_BU.GetMailboxInfoError()->GetFullErrorMessage(), m_ColumnMailboxForwardingAddress, GridBackendUtil::ICON_ERROR);
		if (nullptr != m_ColumnMailboxForwardingSmtpAddress)
			p_Row->AddField(p_BU.GetMailboxInfoError()->GetFullErrorMessage(), m_ColumnMailboxForwardingSmtpAddress, GridBackendUtil::ICON_ERROR);
		if (nullptr != m_ColumnMailboxDeliverToMailboxAndForward)
			p_Row->AddField(p_BU.GetMailboxInfoError()->GetFullErrorMessage(), m_ColumnMailboxDeliverToMailboxAndForward, GridBackendUtil::ICON_ERROR);
		if (nullptr != m_ColumnMailboxCanSendOnBehalf)
			p_Row->AddField(p_BU.GetMailboxInfoError()->GetFullErrorMessage(), m_ColumnMailboxCanSendOnBehalf, GridBackendUtil::ICON_ERROR);
	}
	else
	{
		updateField(p_BU.GetMailboxType(), p_Row, m_ColumnMailboxType, p_UpdateFieldOption);
		updateField(p_BU.GetMailboxDeliverToMailboxAndForward(), p_Row, m_ColumnMailboxDeliverToMailboxAndForward, p_UpdateFieldOption);
		updateField(p_BU.GetMailboxForwardingSmtpAddress(), p_Row, m_ColumnMailboxForwardingSmtpAddress, p_UpdateFieldOption);
		updateField(p_BU.GetMailboxForwardingAddress(), p_Row, m_ColumnMailboxForwardingAddress, p_UpdateFieldOption);
		if (!p_BU.GetMailboxCanSendOnBehalf() || p_BU.GetMailboxCanSendOnBehalf()->empty())
			updateField(PooledString(), p_Row, m_ColumnMailboxCanSendOnBehalf, p_UpdateFieldOption);
		else
			updateField(p_BU.GetMailboxCanSendOnBehalf(), p_Row, m_ColumnMailboxCanSendOnBehalf, p_UpdateFieldOption);
	}

	if (p_BU.GetRecipientPermissionsError())
	{
		if (nullptr != m_ColumnRecipientPermissionsCanSendAsUser)
			p_Row->AddField(p_BU.GetRecipientPermissionsError()->GetFullErrorMessage(), m_ColumnRecipientPermissionsCanSendAsUser, GridBackendUtil::ICON_ERROR);
	}
	else
	{
		if (!p_BU.GetMailboxCanSendAsUser() || p_BU.GetMailboxCanSendAsUser()->empty())
			updateField(PooledString(), p_Row, m_ColumnRecipientPermissionsCanSendAsUser, p_UpdateFieldOption);
		else
			updateField(p_BU.GetMailboxCanSendAsUser(), p_Row, m_ColumnRecipientPermissionsCanSendAsUser, p_UpdateFieldOption);
	}
}

void GridTemplateUsers::setLastRequestDateTime(O365Grid& p_Grid, const BusinessUser& p_BU, GridBackendRow* p_Row)
{
	if (nullptr != p_Row && /*nullptr != m_ColumnLastRequestDateTime*/!m_ColumnsRequestDateTime.empty())
	{
		if (!p_BU.HasFlag(BusinessObject::CANCELED))
		{
			for (const auto& item : m_ColumnsRequestDateTime)
			{
				if (nullptr != item.second)
				{
					const auto& date = p_BU.GetDataDate(item.first);
					if (date)
					{
						if (item.second == m_ColumnLastRequestDateTime)
							p_Row->AddFieldTimeDate(date, item.second);
						else if (UBI::MAILBOX == item.first)
							p_Row->AddFieldTimeDate(date, item.second).SetIcon(m_MailboxLoadedIconId);
						else
							p_Row->AddFieldTimeDate(date, item.second).SetIcon(p_Grid.GetLoadMoreIcon(true)); // FIXME: We might want different icons for each column (only SYNCV1 for now)
					}
				}
			}
			//updateField(p_BU.GetLastRequestTime(), p_Row, m_ColumnLastRequestDateTime, UpdateFieldOption::NONE);
		}
	}
}

void GridTemplateUsers::setValue(OnPremiseUser& p_OnPremUser, std::function<void(OnPremiseUser&, const boost::YOpt<PooledString>&)> p_Setter, const GridBackendField& p_Field)
{
	if (hasValidErrorlessValue(p_Field) && p_Field.IsString())
		p_Setter(p_OnPremUser, PooledString(p_Field.GetValueStr()));
}

void GridTemplateUsers::setValue(OnPremiseUser& p_OnPremUser, std::function<void(OnPremiseUser&, const boost::YOpt<YTimeDate>&)> p_Setter, const GridBackendField& p_Field)
{
	if (hasValidErrorlessValue(p_Field) && p_Field.IsDate())
		p_Setter(p_OnPremUser, p_Field.GetValueTimeDate());
}

void GridTemplateUsers::setValue(OnPremiseUser& p_OnPremUser, std::function<void(OnPremiseUser&, const boost::YOpt<int32_t>&)> p_Setter, const GridBackendField& p_Field)
{
	if (hasValidErrorlessValue(p_Field) && p_Field.IsNumber())
		p_Setter(p_OnPremUser, static_cast<int32_t>(p_Field.GetValueDouble()));
}

void GridTemplateUsers::setValue(OnPremiseUser& p_OnPremUser, std::function<void(OnPremiseUser&, const boost::YOpt<bool>&)> p_Setter, const GridBackendField& p_Field)
{
	if (hasValidErrorlessValue(p_Field) && p_Field.IsBool())
		p_Setter(p_OnPremUser, p_Field.GetValueBool());
}

void GridTemplateUsers::UpdateCommonInfo(GridBackendRow* p_Row)
{
	if (nullptr != m_OnPremCols.m_ColCommonDisplayName)
	{
		if (p_Row->HasField(m_ColumnDisplayName))
			p_Row->AddField(p_Row->GetField(m_ColumnDisplayName), m_OnPremCols.m_ColCommonDisplayName->GetID());
		else if (p_Row->HasField(m_OnPremCols.m_ColCN))
			p_Row->AddField(p_Row->GetField(m_OnPremCols.m_ColCN), m_OnPremCols.m_ColCommonDisplayName->GetID());
	}

	if (nullptr != m_OnPremCols.m_ColCommonUserName)
	{
		if (p_Row->HasField(m_ColumnUserPrincipalName))
			p_Row->AddField(p_Row->GetField(m_ColumnUserPrincipalName), m_OnPremCols.m_ColCommonUserName->GetID());
		else if (p_Row->HasField(m_OnPremCols.m_ColUserPrincipalName))
			p_Row->AddField(p_Row->GetField(m_OnPremCols.m_ColUserPrincipalName), m_OnPremCols.m_ColCommonUserName->GetID());
	}
}
