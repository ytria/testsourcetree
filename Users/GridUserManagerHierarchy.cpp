#include "GridUserManagerHierarchy.h"

#include "AutomationWizardUserManagerHierarchy.h"
#include "BasicGridSetup.h"
#include "BusinessObjectManager.h"
#include "FrameUserManagerHierarchy.h"
#include "GridUpdater.h"
#include "../Resource.h"

#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
GridUserManagerHierarchy::GridUserManagerHierarchy()
	: m_ColumnSelectedUser(nullptr)
	, m_QueriedUserId(NO_ICON)
{
	m_MayContainUnscopedUserGroupOrSite = true;

	initWizard<AutomationWizardUserManagerHierarchy>(_YTEXT("Automation\\UserManagerHierarchy"));
}

void GridUserManagerHierarchy::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameManagerHierarchy, LicenseUtil::g_codeSapio365userMgrHierarchy,
		{ { nullptr, _YTEXT(""), _YTEXT("") }, { nullptr, _YTEXT(""), _YTEXT("") }, { nullptr, _YTEXT(""), _YTEXT("") } }
		);
		setup.Setup(*this, false);
	}

	m_ColumnSelectedUser						= AddColumn(_YUID("selecteduser"),				YtriaTranslate::Do(GridUserManagerHierarchy_customizeGrid_1, _YLOC("Queried User")).c_str(),		GridTemplate::g_TypeUser,	g_ColumnSize2,	{ g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnDisplayName			= AddColumn(_YUID(O365_USER_DISPLAYNAME),		YtriaTranslate::Do(GridUserManagerHierarchy_customizeGrid_2, _YLOC("User Display Name")).c_str(),	GridTemplate::g_TypeUser,	g_ColumnSize22, { g_ColumnsPresetDefault });
	m_TemplateUsers.m_ColumnUserPrincipalName	= AddColumn(_YUID(O365_USER_USERPRINCIPALNAME), YtriaTranslate::Do(GridUserManagerHierarchy_customizeGrid_3, _YLOC("Username")).c_str(),			GridTemplate::g_TypeUser,	g_ColumnSize32, { g_ColumnsPresetDefault });
	//m_TemplateUsers.m_ColumnMail				= AddColumn(_YUID(O365_USER_MAIL),				_TLOC("Email"),				g_FamilyMail,				g_ColumnSize32,	{ g_ColumnsPresetDefault });
	setBasicGridSetupHierarchy({ { { m_TemplateUsers.m_ColumnDisplayName, 0 } },{},{} });
	addColumnGraphID(GridTemplate::g_TypeUser);
	AddColumnForRowPK(GetColId());
	m_TemplateUsers.m_ColumnID = GetColId();
	m_TemplateUsers.CustomizeGrid(*this);

	m_QueriedUserId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_MANAGERQUERIEDUSER));

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameUserManagerHierarchy*>(GetParentFrame()));
}

wstring GridUserManagerHierarchy::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Selected user"), m_ColumnSelectedUser } }, m_TemplateUsers.m_ColumnDisplayName);
}

ModuleCriteria GridUserManagerHierarchy::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria;

    GridFrameBase* frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
    ASSERT(nullptr != frame);
    if (nullptr != frame)
    {
        criteria = frame->GetModuleCriteria();
        criteria.m_IDs.clear();

        std::set<GridBackendRow*> parentRows(p_Rows.begin(), p_Rows.end());

        // Take all ids which have anything in p_Rows as parent
        for (auto row : GetBackendRows())
        {
            // User had a query done on him?
            if (row->GetField(m_ColumnSelectedUser).HasValue())
            {
                // That row has an ancestor we need to refresh?
                if (parentRows.find(row->GetTopAncestorOrThis()) != parentRows.end())
                    criteria.m_IDs.insert(row->GetField(GetColId()).GetValueStr());
            }
        }


        /*for (auto row : p_Rows)
            criteria.m_IDs.insert(row->GetField(GetColId()).GetValueStr());*/
    }

    return criteria;
}

void GridUserManagerHierarchy::AddUserWithManager(vector<BusinessUser>& p_Users, const bool p_IsRefresh, bool p_FullPurge)
{
	ASSERT(p_IsRefresh);

	const uint32_t options	= GridUpdaterOptions::UPDATE_GRID
							| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
							| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
							| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
							| GridUpdaterOptions::REFRESH_PROCESS_DATA
							/*| GridUpdaterOptions::PROCESS_LOADMORE*/;

	GridUpdater updater(*this, GridUpdaterOptions(options));

	// cook managerial hierarchy:
	set<PooledString> selectedUserIDs;
	vector<BusinessUser> managers;
	for (BusinessUser& user : p_Users)
	{
		selectedUserIDs.insert(user.GetID());

		if (user.HasManager())
			user.GetManager().get()->AddDirectReport(user);

		BusinessUser& userToIntegrate = *user.GetManagerTop();

		set<PooledString> obsoleteTopManagerUserIDs;
		bool integratedIntoExistingHierarchy = false;
		for (auto& peopleager : managers)
		{
			bool integratedThisPeopleagerHierarchy = false;
			if (!integratedIntoExistingHierarchy)
			{
				integratedIntoExistingHierarchy		= peopleager.IntegrateIntoManagerHierarchy(userToIntegrate);
				integratedThisPeopleagerHierarchy	= integratedIntoExistingHierarchy;
			}
			if (!integratedThisPeopleagerHierarchy)
			{
				if (userToIntegrate.IntegrateIntoManagerHierarchy(peopleager))
					obsoleteTopManagerUserIDs.insert(peopleager.GetID());// peopleager belongs to userToIntegrate's hierarchy and is not a top manager after all, just another loser
			}
		}// achtung: do not break out of this loop, the whole set must always submit to tentative integration to remove fake news about top managers

		managers.erase(std::remove_if(managers.begin(), managers.end(), [=](BusinessUser& p_User) { return obsoleteTopManagerUserIDs.find(p_User.GetID()) != obsoleteTopManagerUserIDs.end(); } ), managers.end());
		
		if (!integratedIntoExistingHierarchy)
			managers.push_back(userToIntegrate);
	}

	// pouf
	for (const auto& manager : managers)
		addUserRow(nullptr, manager, selectedUserIDs, updater);
}

bool GridUserManagerHierarchy::IsQueryReferenceRow(GridBackendRow* p_Row) const
{
	return nullptr != p_Row && p_Row->GetField(m_ColumnSelectedUser).GetIcon() == m_QueriedUserId;
}

void GridUserManagerHierarchy::addUserRow(GridBackendRow* p_Row, const BusinessUser& p_User, const set<PooledString>& p_selectedUserIDs, GridUpdater& p_Updater)
{
	GridBackendRow* row = m_TemplateUsers.AddRow(*this, p_User, vector<GridBackendField>(), p_Updater, false, /*FIXME!!!*/false);
	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		p_Updater.GetOptions().AddRowWithRefreshedValues(row);
        p_Updater.GetOptions().AddPartialPurgeParentRow(row);

		row->SetParentRow(p_Row);
		if (p_selectedUserIDs.find(p_User.GetID()) != p_selectedUserIDs.end())
			row->AddField(YtriaTranslate::Do(GridUserManagerHierarchy_addUserRow_1, _YLOC("Query executed")).c_str(), m_ColumnSelectedUser, m_QueriedUserId);
		else
			row->RemoveField(m_ColumnSelectedUser);

		for (const auto& meat : p_User.GetDirectReports())
			addUserRow(row, *meat, p_selectedUserIDs, p_Updater);

		AddRoleDelegationFlag(row, p_User);
	}
}
#endif
