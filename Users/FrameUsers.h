#pragma once

#include "GridFrameBase.h"
#include "GridUsers.h"

class FrameUsers : public GridFrameBase
{
public:
	DECLARE_DYNAMIC(FrameUsers)
	FrameUsers(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
	virtual ~FrameUsers() = default;

	void ShowUsers(const vector<BusinessUser>& p_Users, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Update, bool p_AllowFullPurge);
	void ShowUsers(const vector<BusinessUser>& p_Users, const vector<BusinessUser>& p_LoadedMoreUsers, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Update, bool p_AllowFullPurge);
	void ShowOnPremiseUsers(const vector<OnPremiseUser>& p_Users);
	void UpdateUsersLoadMore(const vector<BusinessUser>& p_Users, bool p_FromRefresh);
	void UpdateUsersLoadMailbox(const vector<BusinessUser>& p_Users, bool p_FromRefresh);
	void SetOnPremiseUsersUpdateResult(const vector<O365UpdateOperation>& p_UpdateResults);

	virtual O365Grid& GetGrid() override;
	virtual void ApplyAllSpecific() override;

    void ApplySpecific(bool p_Selected);

    virtual void ApplySelectedSpecific() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction);

	void ShowRecycleBin();

	bool HasDeltaFeature() const override;

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

	/*afx_msg void OnApplyAllOnPrem();
	afx_msg void OnUpdateApplyAllOnPrem(CCmdUI* pCmdUI);

	afx_msg void OnApplySelectedOnPrem();
	afx_msg void OnUpdateApplySelectedOnPrem(CCmdUI* pCmdUI);

	afx_msg void OnRevertAllOnPrem();
	afx_msg void OnUpdateRevertAllOnPrem(CCmdUI* pCmdUI);

	afx_msg void OnRevertSelectedOnPrem();
	afx_msg void OnUpdateRevertSelectedOnPrem(CCmdUI* pCmdUI);*/

	afx_msg void OnForceSyncOnPrem();
	afx_msg void OnUpdateForceSyncOnPrem(CCmdUI* pCmdUI);

	afx_msg void OnSetAADComputerName();
	afx_msg void OnUpdateSetAADComputerName(CCmdUI* pCmdUI);

	void applyOnPremSpecific(bool p_SelectednOnly);

	virtual void createGrid() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

	virtual ApplyConfirmationConfig GetApplyConfirmationConfig(bool p_ApplySelected) const override;

	bool hasLoseableModificationsPending(bool inSelectionOnly = false) override; // for refresh, close, logout
	bool hasOnPremiseChanges(bool inSelectionOnly = false);

// Members
private:
	GridUsers m_GridUsers;

	template<class T> friend class OnPremModificationsApplyer;
	template<class T> friend class OnPremSaveSelectedButtonUpdater;
	template<class T> friend class OnPremSaveAllButtonUpdater;
	template<class T> friend class OnPremRevertSelectedButtonUpdater;
	template<class T> friend class OnPremRevertAllButtonUpdater;
};
