#include "GridFrameLogger.h"

#include "GridFrameBase.h"
#include "BaseO365Grid.h"
#include "YMessageGrid.h"

namespace
{
	class YMessageModuleGrid : public YMessageGrid
	{
	public:
		YMessageModuleGrid(O365Grid& p_Grid, const int32_t taskID, const wstring& text, bool p_IsError)
			: YMessageGrid(p_Grid)
			, m_TaskID(taskID)
			, m_Text(text)
			, m_IsError(p_IsError)
		{
		}

		virtual void Execute() final
		{
			getGrid().AddLog(m_TaskID, m_Text.c_str(), m_IsError, true);
		}

	private:
		wstring m_Text;
		int32_t	m_TaskID;
		bool m_IsError;
	};
}

GridFrameLogger::GridFrameLogger(CFrameWnd& p_Frame)
	: FrameLogger(p_Frame)
	, m_Ignore404(false)
{}

void GridFrameLogger::LogSpecific(const LogEntry& p_Entry) const
{
	if (p_Entry.m_LogLevel == LogLevel::User
		// A bit dirty, but we have no choice since we only have a text...
		|| p_Entry.m_LogLevel == LogLevel::Error && (!m_Ignore404 || !Str::beginsWith(p_Entry.m_Message, _YTEXT("HTTP 404:"))))
	{
		GridFrameBase* frame = dynamic_cast<GridFrameBase*>(&getFrame());
		ASSERT(nullptr != frame);
		if (nullptr != frame)
		{
			YMessageModuleGrid* message = new YMessageModuleGrid(frame->GetGrid(), frame->GetTaskID(), p_Entry.m_Message, p_Entry.m_LogLevel == LogLevel::Error);
			message->Post();
		}
	}
	// message will be deleted when executed. Do not delete here.
}

void GridFrameLogger::SetIgnoreHttp404Errors(bool p_Ignore)
{
	m_Ignore404 = p_Ignore;
}
