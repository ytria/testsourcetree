#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "SpecialFolder.h"

class SpecialFolderDeserializer : public JsonObjectDeserializer, public Encapsulate<SpecialFolder>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

