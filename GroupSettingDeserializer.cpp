#include "GroupSettingDeserializer.h"

#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "SettingValueDeserializer.h"

void GroupSettingDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeString(_YTEXT("templateId"), m_Data.m_TemplateId, p_Object);

    {
        ListDeserializer<SettingValue, SettingValueDeserializer> svDeserializer;
        if (JsonSerializeUtil::DeserializeAny(svDeserializer, _YTEXT("values"), p_Object))
            m_Data.m_Values = std::move(svDeserializer.GetData());
    }
}
