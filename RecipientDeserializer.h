#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Recipient.h"

class RecipientDeserializer : public JsonObjectDeserializer, public Encapsulate<Recipient>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

