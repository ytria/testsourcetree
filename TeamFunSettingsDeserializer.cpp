#include "TeamFunSettingsDeserializer.h"

#include "JsonSerializeUtil.h"

void TeamFunSettingsDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowGiphy"), m_Data.AllowGiphy, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("giphyContentRating"), m_Data.GiphyContentRating, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowStickersAndMemes"), m_Data.AllowStickersAndMemes, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowCustomMemes"), m_Data.AllowCustomMemes, p_Object);
}
