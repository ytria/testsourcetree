#pragma once

#include "ISubItemDeletion.h"
#include "AttachmentInfo.h"

class GridMessages;

class MessageAttachmentDeletion : public ISubItemDeletion
{
public:
    MessageAttachmentDeletion(GridMessages& p_Grid, const AttachmentInfo& p_Info);

    virtual bool IsCorrespondingRow(GridBackendRow* p_Row) const override;

	virtual vector<ModificationLog> GetModificationLogs() const override;

	GridBackendRow* GetMainRowIfCollapsed() const override;

private:
    GridMessages& m_GridMessages;
    AttachmentInfo m_AttachmentInfo;
};