#pragma once

#include "BusinessConfigurationBase.h"

class BusinessUserConfiguration : public BusinessConfigurationBase
{
public:
	const wstring& GetValueStringSignInStatus_Allowed() const;
	const wstring& GetValueStringSignInStatus_Blocked() const;
	const wstring& GetValueStringLicenses_Unlicensed() const;

	DECLARE_GET_INSTANCE(BusinessUserConfiguration)

private:
	static wstring g_AllowedStatus;
	static wstring g_BlockedStatus;

	static wstring g_Unlicensed;

	BusinessUserConfiguration();
};