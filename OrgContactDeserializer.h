#pragma once

#include "BusinessOrgContact.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class OrgContactDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessOrgContact>
{
public:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};
