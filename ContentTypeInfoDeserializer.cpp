#include "ContentTypeInfoDeserializer.h"

#include "JsonSerializeUtil.h"

void ContentTypeInfoDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.Id, p_Object, true);
}
