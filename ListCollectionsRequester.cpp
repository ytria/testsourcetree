#include "ListCollectionsRequester.h"
#include "CosmosDBSqlSession.h"
#include "DumpDeserializer.h"
#include "LoggerService.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "BasicHttpRequestLogger.h"

Cosmos::ListCollectionsRequester::ListCollectionsRequester(const wstring& p_DbName)
    : m_DbName(p_DbName)
{
}

TaskWrapper<void> Cosmos::ListCollectionsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<Cosmos::Collection, Cosmos::CollectionDeserializer>>(_YTEXT("DocumentCollections"));
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("dbs"));
    uri.append_path(m_DbName);
    uri.append_path(_YTEXT("colls"));

    wstring uris = uri.to_string();

    LoggerService::User(YtriaTranslate::Do(Cosmos__ListCollectionsRequester_Send_1, _YLOC("Requesting collections list for database \"%1\" in CosmosDB"), m_DbName.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetCosmosDBSqlSession()->Get(uri.to_uri(),
        httpLogger,
        _YTEXT("colls"),
        wstring(_YTEXT("dbs/")) + m_DbName,
        p_Session->GetCosmosDbMasterKey(),
        m_Result,
        m_Deserializer,
        p_TaskData);
}

const vector<Cosmos::Collection>& Cosmos::ListCollectionsRequester::GetData() const
{
    return m_Deserializer->GetData();
}

const SingleRequestResult& Cosmos::ListCollectionsRequester::GetResult() const
{
    return *m_Result;
}
