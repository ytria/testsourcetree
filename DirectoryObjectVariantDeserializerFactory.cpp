#include "DirectoryObjectVariantDeserializerFactory.h"

DirectoryObjectVariantDeserializerFactory::DirectoryObjectVariantDeserializerFactory(std::shared_ptr<Sapio365Session> p_Session)
	: m_Session(p_Session)
{

}

#ifdef _DEBUG
DirectoryObjectVariantDeserializerFactory::DirectoryObjectVariantDeserializerFactory(std::shared_ptr<Sapio365Session> p_Session, bool p_NoSelectUsed)
	: DirectoryObjectVariantDeserializerFactory(p_Session)
{
	m_NoSelectUsed = p_NoSelectUsed;
}
#endif

DirectoryObjectVariantDeserializer DirectoryObjectVariantDeserializerFactory::Create()
{
#ifdef _DEBUG
    return DirectoryObjectVariantDeserializer(m_Session, m_NoSelectUsed);
#else
	return DirectoryObjectVariantDeserializer(m_Session);
#endif
}
