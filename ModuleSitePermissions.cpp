#include "ModuleSitePermissions.h"

#include "SharepointOnlineSession.h"
#include "SingleRequestResult.h"
#include "FrameSitePermissions.h"
#include "O365AdminUtil.h"
#include "RefreshSpecificData.h"
#include "RemoveRoleAssignmentRequester.h"
#include "RoleDefinitionsRequester.h"
#include "RoleDefinitionBindingsRequester.h"
#include "RoleDefinition.h"
#include "safeTaskCall.h"
#include "SitePermissionsChanges.h"
#include "SpGroupMembersRequester.h"
#include "SpGroupsRequester.h"
#include "SpUser.h"
#include "SpUsersRequester.h"
#include "YCallbackMessage.h"
#include "MultiObjectsRequestLogger.h"

void ModuleSitePermissions::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
    {
    case Command::ModuleTask::List:
    {
        Command newCmd = p_Command;
        show(p_Command);
        break;
    }
    case Command::ModuleTask::ApplyChanges:
        applyChanges(p_Command);
        break;
    }
}

void ModuleSitePermissions::applyChanges(const Command& p_Command)
{
    CommandInfo info = p_Command.GetCommandInfo();
    AutomationAction* action = p_Command.GetAutomationAction();
    FrameSitePermissions* frame = dynamic_cast<FrameSitePermissions*>(info.GetFrame());
    ASSERT(nullptr != frame);

    ASSERT(info.Data().is_type<SitePermissionsChanges>());
    if (info.Data().is_type<SitePermissionsChanges>())
    {
        const SitePermissionsChanges& changes = info.Data().get_value<SitePermissionsChanges>();
        auto bom = frame->GetBusinessObjectManager();

        GridSitePermissions* gridSitePerms = dynamic_cast<GridSitePermissions*>(&frame->GetGrid());
        
        auto taskData = addFrameTask(YtriaTranslate::Do(ModuleDrive_applyChanges_1, _YLOC("Applying Changes")).c_str(), frame, p_Command, false);
        YSafeCreateTask([this, changes, p_Command, criteria = frame->GetModuleCriteria(), hwnd = frame->m_hWnd, taskData, action, bom, moduleCriteria = frame->GetModuleCriteria()]()
        {
            RefreshSpecificData refreshData;
            refreshData.m_Criteria = criteria;
            ASSERT(refreshData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::IDS);
            refreshData.m_Criteria.m_IDs.clear();

            // Delete permissions
            std::map<SubItemKey, HttpResultWithError> deleteResults;
            for (auto& key : changes.m_UserPermissionsToDelete)
            {
                if (taskData.IsCanceled())
                    break;

                auto roleAssignmentRemover = std::make_shared<Sp::RemoveRoleAssignmentRequester>(key.m_Id1, key.m_Id2, key.m_Id4);
                auto result = safeTaskCall(roleAssignmentRemover->Send(bom->GetSapio365Session(), taskData).Then([roleAssignmentRemover]() {
                    return HttpResultWithError(roleAssignmentRemover->GetResult().GetResult(), boost::none);
                }), bom->GetSharepointSession(), Util::WriteErrorHandler, taskData).get();

                deleteResults[key] = result;
                refreshData.m_Criteria.m_IDs.insert(key.m_Id0);
            }
            for (auto& key : changes.m_GroupPermissionsToDelete)
            {
                if (taskData.IsCanceled())
                    break;

                auto roleAssignmentRemover = std::make_shared<Sp::RemoveRoleAssignmentRequester>(key.m_Id1, key.m_Id3, key.m_Id4);
                auto result = safeTaskCall(roleAssignmentRemover->Send(bom->GetSapio365Session(), taskData).Then([roleAssignmentRemover]() {
                    return HttpResultWithError(roleAssignmentRemover->GetResult().GetResult(), boost::none);
                }), bom->GetSharepointSession(), Util::WriteErrorHandler, taskData).get();

                deleteResults[key] = result;
                refreshData.m_Criteria.m_IDs.insert(key.m_Id0);
            }

            YCallbackMessage::DoPost([hwnd, refreshData, action, taskData, deleteResults]()
            {
                if (::IsWindow(hwnd))
                {
                    auto frame = dynamic_cast<FrameSitePermissions*>(CWnd::FromHandle(hwnd));
                    ASSERT(nullptr != frame);

                    frame->GetGrid().GetModifications().SetSubItemDeletionsErrors(deleteResults);
                    frame->GetGrid().ClearLog(taskData.GetId());
                    frame->RefreshAfterUpdate({ }, refreshData, action);
                }
                else
                {
                    SetAutomationGreenLight(action);
                }
                TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
            });
        });
    }
    else
    {
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2481")).c_str());
    }
}

void ModuleSitePermissions::doRefresh(FrameSitePermissions* p_pFrame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command)
{
    GridSitePermissions* gridSitePermissions = dynamic_cast<GridSitePermissions*>(&p_pFrame->GetGrid());

    RefreshSpecificData refreshSpecificData;
    if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
    {
        ASSERT(p_IsUpdate);
        refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();
    }

	ASSERT(false); // TODO: check if logging is correct
	auto logger = std::make_shared<BasicRequestLogger>(_T("users"));

    YSafeCreateTask([p_TaskData, hwnd = p_pFrame->GetSafeHwnd(), currentModuleCriteria = p_pFrame->GetModuleCriteria(), p_Action, p_IsUpdate, gridSitePermissions, refreshSpecificData, bom = p_pFrame->GetBusinessObjectManager()]()
    {
        const bool partialRefresh = !refreshSpecificData.m_Criteria.m_IDs.empty();
        const auto& criteria = partialRefresh ? refreshSpecificData.m_Criteria : currentModuleCriteria;

		O365DataMap<BusinessSite, SitePermissionsInfo> sitePermissionInfos;

        ASSERT(criteria.m_Origin == Origin::Site);
        if (criteria.m_Origin == Origin::Site)
        {
			auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("site permissions"), criteria.m_IDs.size());
            for (const auto& id : criteria.m_IDs)
            {
                BusinessSite site;
                site.SetID(id);
				logger->SetContextualInfo(bom->GetGraphCache().GetSiteContextualInfo(id));
                bom->GetGraphCache().SyncUncachedSite(site, logger, p_TaskData);

                sitePermissionInfos[site] = SitePermissionsInfo();
                SitePermissionsInfo& permInfo = sitePermissionInfos[site];

                if (p_TaskData.IsCanceled())
                {
                    site.SetFlags(site.GetFlags() | BusinessObject::Flag::CANCELED);
                    continue;
                }
                else
                {
                    ASSERT(site.GetWebUrl() && !site.GetWebUrl()->IsEmpty());
                    if (site.GetWebUrl() && !site.GetWebUrl()->IsEmpty())
                    {
                        try
                        {
                            web::uri webUrl = web::uri(site.GetWebUrl() ? *site.GetWebUrl() : _YTEXT(""));
                            auto siteUrl =  Sp::GetSiteUrlFromWebUrl(webUrl);
                            // Get all users
                            auto usersRequester = std::make_shared<Sp::SpUsersRequester>(siteUrl);
                            auto users = safeTaskCall(usersRequester->Send(bom->GetSapio365Session(), p_TaskData).Then([usersRequester]() {
                                return usersRequester->GetData();
                            }), bom->GetSharepointSession(), Util::ListErrorHandler<Sp::User>, p_TaskData).get();

                            if (users.size() == 1 && users[0].GetError())
                            {
                                auto& usersListError = *users[0].GetError();
                                usersListError.AddErrorMessagePrefix(YtriaTranslate::Do(ModuleSitePermissions_doRefresh_1, _YLOC("Error retrieving users list: ")).c_str());
                                permInfo.m_UsersListError = usersListError;
                                continue;
                            }
                            // For each user, get specific permissions
                            for (auto& user : users)
                            {
                                ASSERT(user.Id);
                                if (user.Id)
                                {
                                    PooledString userId = std::to_wstring(*user.Id);

                                    auto userPermsRequester = std::make_shared<Sp::RoleDefinitionBindingsRequester>(siteUrl, userId);
                                    auto userPerms = safeTaskCall(userPermsRequester->Send(bom->GetSapio365Session(), p_TaskData).Then([userPermsRequester]() {
                                        return userPermsRequester->GetData();
                                    }), bom->GetSharepointSession(), Util::ListErrorHandler<Sp::RoleDefinition>, p_TaskData).get();

                                    if (userPerms.size() == 1 && userPerms[0].GetError())
                                    {
                                        if (userPerms[0].GetError()->GetStatusCode() != 404)
                                        {
                                            auto& userPermsError = *userPerms[0].GetError();
                                            userPermsError.AddErrorMessagePrefix(YtriaTranslate::Do(ModuleSitePermissions_doRefresh_2, _YLOC("Error retrieving user permissions: ")).c_str());
                                            user.SetError(userPermsError);
                                        }
                                    }

                                    for (const auto& perm : userPerms)
                                    {
                                        if (perm.Id)
                                            permInfo.m_UserRoles[userId].insert(*perm.Id);
                                    }
                                    permInfo.m_Users[userId] = user;
                                }
                            }

                            // Get all role definitions
                            auto rolesRequester = std::make_shared<Sp::RoleDefinitionsRequester>(siteUrl);
                            auto roleDefs = safeTaskCall(rolesRequester->Send(bom->GetSapio365Session(), p_TaskData).Then([rolesRequester]() {
                                return rolesRequester->GetData();
                            }), bom->GetSharepointSession(), Util::ListErrorHandler<Sp::RoleDefinition>, p_TaskData).get();
                            if (roleDefs.size() == 1 && roleDefs[0].GetError())
                            {
                                auto& roleDefinitionsError = *roleDefs[0].GetError();
                                roleDefinitionsError.AddErrorMessagePrefix(YtriaTranslate::Do(ModuleSitePermissions_doRefresh_3, _YLOC("Error retrieving permissions list: ")).c_str());
                                permInfo.m_RoleDefinitionsError = roleDefinitionsError;
                                continue;
                            }
                            permInfo.m_RoleDefinitions = std::set<Sp::RoleDefinition>(roleDefs.begin(), roleDefs.end());

                            // Get all groups
                            auto groupsRequester = std::make_shared<Sp::SpGroupsRequester>(siteUrl);
                            auto groups = safeTaskCall(groupsRequester->Send(bom->GetSapio365Session(), p_TaskData).Then([groupsRequester]() {
                                return groupsRequester->GetData();
                            }), bom->GetSharepointSession(), Util::ListErrorHandler<Sp::Group>, p_TaskData).get();
                            if (groups.size() == 1 && groups[0].GetError())
                            {
                                permInfo.m_GroupsListError = groups[0].GetError();
                                continue;
                            }
                            permInfo.m_Groups = groups;

                            // Get members for each group
                            for (auto& group : permInfo.m_Groups)
                            {
                                ASSERT(group.Id != boost::none);
                                if (group.Id != boost::none)
                                {
                                    PooledString groupId = std::to_wstring(*group.Id);

                                    auto membersRequester = std::make_shared<Sp::SpGroupMembersRequester>(siteUrl, groupId);
                                    auto groupMembers = safeTaskCall(membersRequester->Send(bom->GetSapio365Session(), p_TaskData).Then([membersRequester]() {
                                        return membersRequester->GetData();
                                    }), bom->GetSharepointSession(), Util::ListErrorHandler<Sp::User>, p_TaskData).get();

                                    if (groupMembers.size() == 1 && groupMembers[0].GetError())
                                    {
                                        auto& membersError = *groupMembers[0].GetError();
                                        membersError.AddErrorMessagePrefix(YtriaTranslate::Do(ModuleSitePermissions_doRefresh_4, _YLOC("Error retrieving group members list: ")).c_str());
                                        group.SetError(membersError);
                                    }
                                    for (const auto& user : groupMembers)
                                    {
                                        PooledString userId = user.Id ? std::to_wstring(*user.Id) : _YTEXT("");
                                        permInfo.m_GroupMembers[PooledString(groupId)].insert(userId);
                                    }

                                    auto groupPermsRequester = std::make_shared<Sp::RoleDefinitionBindingsRequester>(siteUrl, groupId);
                                    auto groupPerms = safeTaskCall(groupPermsRequester->Send(bom->GetSapio365Session(), p_TaskData).Then([groupPermsRequester]() {
                                        return groupPermsRequester->GetData();
                                    }), bom->GetSharepointSession(), Util::ListErrorHandler<Sp::RoleDefinition>, p_TaskData).get();

                                    if (groupPerms.size() == 1 && groupPerms[0].GetError())
                                    {
                                        wstring prefix = group.GetError() ? group.GetError()->GetFullErrorMessage() : _YTEXT("");
                                        prefix.append(YtriaTranslate::Do(ModuleSitePermissions_doRefresh_5, _YLOC("\nError retrieving group permissions: ")).c_str());
                                        auto& groupPermsError = *groupPerms[0].GetError();
                                        groupPermsError.AddErrorMessagePrefix(prefix);
                                        group.SetError(groupPermsError);
                                    }
                                    
                                    for (const auto& perm : groupPerms)
                                    {
                                        if (perm.Id)
                                            permInfo.m_GroupRoles[groupId].insert(*perm.Id);
                                    }
                                }
                            }
                        }
                        catch (const pplx::task_canceled&)
                        {
                            site.SetFlags(site.GetFlags() | BusinessObject::Flag::CANCELED);
                        }
                     
                    }

                    if (p_TaskData.IsCanceled())
                        site.SetFlags(site.GetFlags() | BusinessObject::Flag::CANCELED);
                }
            }
        }

        YCallbackMessage::DoPost([hwnd, p_TaskData, partialRefresh, p_Action, sitePermissionInfos, p_IsUpdate, isCanceled = p_TaskData.IsCanceled()]
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameSitePermissions*>(CWnd::FromHandle(hwnd)) : nullptr;
            if (ShouldBuildView(hwnd, isCanceled, p_TaskData, false))
            {
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting permissions for %s sites...", Str::getStringFromNumber(sitePermissionInfos.size()).c_str()));
                frame->GetGrid().ClearLog(p_TaskData.GetId());

				{
					CWaitCursor _;
					frame->BuildView(sitePermissionInfos, !partialRefresh);
				}
                
				if (!p_IsUpdate)
				{
					frame->UpdateContext(p_TaskData, hwnd);
					shouldFinishTask = false;
				}
            }

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, p_TaskData, p_Action);
			}
        });
    });
}

void ModuleSitePermissions::show(const Command& p_Command)
{
    AutomationAction*	p_Action	= p_Command.GetAutomationAction();
    const CommandInfo&	info		= p_Command.GetCommandInfo();
    const auto			p_Origin	= info.GetOrigin();

    ASSERT(p_Origin != Origin::NotSet);
    if (p_Origin != Origin::NotSet)
    {
        RefreshSpecificData refreshSpecificData;
        bool isUpdate = false;

        const auto&	p_UserIDs	= info.GetIds();
        auto p_SourceWindow		= p_Command.GetSourceWindow();
        auto sourceCWnd			= p_SourceWindow.GetCWnd();

        FrameSitePermissions* frame = dynamic_cast<FrameSitePermissions*>(info.GetFrame());
        if (nullptr == frame)
        {
            ASSERT(!p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>());

            if (FrameSitePermissions::CanCreateNewFrame(true, sourceCWnd, p_Action))
            {
                ModuleCriteria moduleCriteria;
                moduleCriteria.m_Origin			= p_Origin;
                moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::IDS;
                moduleCriteria.m_IDs			= p_UserIDs;
				moduleCriteria.m_Privilege		= info.GetRBACPrivilege();

                if (moduleCriteria.m_IDs.empty())
                {
                    auto& id = GetConnectedSession().GetConnectedUserId();
                    ASSERT(!id.empty());
                    if (!id.empty())
                        moduleCriteria.m_IDs.insert(id);
                }

                if (ShouldCreateFrame<FrameSitePermissions>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
                {
                    CWaitCursor _;

                    frame = new FrameSitePermissions(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleSitePermissions_showSitePermissions_1, _YLOC("Site Permissions")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
                    frame->InitModuleCriteria(moduleCriteria);
                    AddGridFrame(frame);
                    frame->Erect();
                }
            }
        }
        else
        {
            isUpdate = true;
        }

        if (nullptr != frame)
        {
            auto taskData = addFrameTask(YtriaTranslate::Do(ModuleSitePermissions_showSitePermissions_2, _YLOC("Show Site Permissions")).c_str(), frame, p_Command, !isUpdate);
            doRefresh(frame, p_Action, taskData, isUpdate, p_Command);
        }
        else
        {
            SetAutomationGreenLight(p_Action, _YTEXT(""));
        }
    }
    else
    {
        SetAutomationGreenLight(p_Action, p_Command.ToString() + YtriaTranslate::DoError(ModuleContact_showContacts_3, _YLOC(" - origin not set"),_YR("Y2482")).c_str());
    }
}

