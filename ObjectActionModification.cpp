#include "ObjectActionModification.h"

#include "BaseO365Grid.h"

ObjectActionModification::ObjectActionModification(O365Grid& p_Grid, const row_pk_t& p_RowKey, bool p_RowStateIsDeletedInsteadOfModified, const wstring& statusText)
    : RowModification(p_Grid, p_RowKey)
	, m_RowStateIsDeletedInsteadOfModified(p_RowStateIsDeletedInsteadOfModified)
{
	GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		if (m_RowStateIsDeletedInsteadOfModified)
		{
			getGrid().OnRowDeleted(row, false, statusText); // Display the "Row Deleted" icon.
			ASSERT(!row->IsModified() && !row->IsCreated());
			if (row->IsError())
				row->SetEditError(false);
		}
		else
		{
			row->SetEditModified(true);
			getGrid().OnRowModified(row, 0, false, statusText);
			ASSERT(!row->IsDeleted() && !row->IsCreated());
			if (row->IsError())
				row->SetEditError(false);
		}
	}
}

ObjectActionModification::~ObjectActionModification()
{
	if (shouldRevert())
	{
		//if (GetState() == Modification::State::AppliedLocally)
		{
			GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

			if (nullptr != row)
			{
				if (row->IsExplosionFragment())
				{
					ASSERT(!row->IsExplosionAdded());
					const auto siblings = getGrid().GetMultivalueManager().GetExplosionSiblings(row);
					for (auto sibling : siblings)
					{
						ASSERT(m_RowStateIsDeletedInsteadOfModified && sibling->IsDeleted() || !m_RowStateIsDeletedInsteadOfModified && sibling->IsModified());
						if (m_RowStateIsDeletedInsteadOfModified && sibling->IsDeleted() || !m_RowStateIsDeletedInsteadOfModified && sibling->IsModified())
							getGrid().OnRowNothingToSave(sibling, false); // Remove all status flags the row.
					}
				}
				else
				{
					ASSERT(GetState() != Modification::State::AppliedLocally || m_RowStateIsDeletedInsteadOfModified && row->IsDeleted() || !m_RowStateIsDeletedInsteadOfModified && row->IsModified());
					// Remove all status flags from the row.
					getGrid().OnRowNothingToSave(row, false);
				}
			}
		}
	}
}
