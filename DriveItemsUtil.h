#pragma once

#include "GridDriveItems.h"

class DriveItemsUtil
{
public:
    static bool IsCorrespondingRow(GridDriveItems& p_Grid, GridBackendRow* p_Row, const GridDriveItems::DriveItemPermissionInfo& p_PermissionInfo);

    static const wstring ROLE_VALUE_READ;
    static const wstring ROLE_VALUE_WRITE;
    static const wstring ROLE_VALUE_OWNER;
    static const wstring TARGET_ANONYMOUS;
	static const wstring TARGET_ORGANIZATION;
	// static const wstring TARGET_USERS; not used at the moment.
	static const wstring TARGET_ANONYMOUS_NUDE;
	static const wstring TARGET_ORGANIZATION_NUDE;
	static const wstring TARGET_USERS_NUDE;
};

