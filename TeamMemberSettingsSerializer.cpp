#include "TeamMemberSettingsSerializer.h"

#include "JsonSerializeUtil.h"

TeamMemberSettingsSerializer::TeamMemberSettingsSerializer(const TeamMemberSettings& p_TeamMemberSettings)
    : m_TeamMemberSettings(p_TeamMemberSettings)
{
}

void TeamMemberSettingsSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeBool(_YTEXT("allowCreateUpdateChannels"), m_TeamMemberSettings.GetAllowCreateUpdateChannels(), obj);
	JsonSerializeUtil::SerializeBool(_YTEXT("allowCreatePrivateChannels"), m_TeamMemberSettings.GetAllowCreatePrivateChannels(), obj);
	JsonSerializeUtil::SerializeBool(_YTEXT("allowDeleteChannels"), m_TeamMemberSettings.GetAllowDeleteChannels(), obj);
    JsonSerializeUtil::SerializeBool(_YTEXT("allowAddRemoveApps"), m_TeamMemberSettings.GetAllowAddRemoveApps(), obj);
    JsonSerializeUtil::SerializeBool(_YTEXT("allowCreateUpdateRemoveTabs"), m_TeamMemberSettings.GetAllowCreateUpdateRemoveTabs(), obj);
    JsonSerializeUtil::SerializeBool(_YTEXT("allowCreateUpdateRemoveConnectors"), m_TeamMemberSettings.GetAllowCreateUpdateRemoveConnectors(), obj);
}
