#include "TimeZoneBaseSerializer.h"

#include "JsonSerializeUtil.h"

TimeZoneBaseSerializer::TimeZoneBaseSerializer(const TimeZoneBase& p_TimeZoneBase)
	: m_TimeZoneBase(p_TimeZoneBase)
{
}

void TimeZoneBaseSerializer::Serialize()
{
	m_Json = web::json::value::object();
	auto& obj = m_Json.as_object();

	JsonSerializeUtil::SerializeString(_YTEXT("name"), m_TimeZoneBase.Name, obj);
}
