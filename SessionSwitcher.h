#pragma once

#include "ISessionLoader.h"

class Sapio365Session;
struct SessionIdentifier;

class SessionSwitcher
{
public:
	SessionSwitcher(std::unique_ptr<ISessionLoader> p_Loader);
	SessionSwitcher(nullptr_t) = delete;

	TaskWrapper<std::shared_ptr<Sapio365Session>> Switch();
	void SetForceReloadExisting(bool p_ReloadIfExists);

private:
	void SwitchToAlreadyLoaded(const std::shared_ptr<Sapio365Session>& p_Session);

	std::unique_ptr<ISessionLoader> m_Loader;
	bool m_ForceReloadExisting;
};

