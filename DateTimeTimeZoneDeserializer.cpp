#include "DateTimeTimeZoneDeserializer.h"

#include "JsonSerializeUtil.h"

void DateTimeTimeZoneDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("dateTime"), m_Data.DateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("timeZone"), m_Data.TimeZone, p_Object);

	// What should we do if it's different from UTC?
	// Convert m_Data.DateTime to correct timezone?
	// How to map this string to a time zone offset?
	ASSERT(!m_Data.TimeZone || *m_Data.TimeZone == _YTEXT("UTC"));
}
