#include "IdentitySerializer.h"

IdentitySerializer::IdentitySerializer(const Identity& p_Identity)
	:m_Identity(p_Identity)
{
}

void IdentitySerializer::Serialize()
{
	m_Json = web::json::value::object();
	auto& obj = m_Json.as_object();

	JsonSerializeUtil::SerializeString(_YUID("displayName"), m_Identity.DisplayName, obj);
	JsonSerializeUtil::SerializeString(_YUID("id"), m_Identity.Id, obj);
	JsonSerializeUtil::SerializeString(_YUID("email"), m_Identity.Email, obj);
	JsonSerializeUtil::SerializeString(_YUID("identityProvider"), m_Identity.IdentityProvider, obj);
}
