#include "SubItemFieldUpdate.h"

#include "ActivityLoggerUtil.h"
#include "BaseO365Grid.h"
#include "CompositeSubElementFormatter.h"

SubItemFieldUpdate::SubItemFieldUpdate(const SubItemFieldValue& p_OldValue, const SubItemFieldValue& p_NewValue, UINT p_ColumnKey,
										O365Grid& p_Grid, const SubItemKey& p_Key, GridBackendColumn* p_ColSubItemElder, GridBackendColumn* p_ColSubItemPrimaryKey, const wstring& p_ObjectName) :
    ISubItemModification(p_Grid, p_Key, p_ColSubItemElder, p_ColSubItemPrimaryKey, p_ObjectName),
    m_OldValue(p_OldValue),
    m_NewValue(p_NewValue),
    m_ColumnKey(p_ColumnKey)
{
    m_FormatterId = g_FormatterId;
}

void SubItemFieldUpdate::RevertSpecific(const SubItemInfo& p_Info)
{
    ASSERT(nullptr != p_Info.m_Row);
    if (nullptr != p_Info.m_Row)
    {
        if (!p_Info.m_Collapsed)
        {
            // Put back old value in cell
            switch (m_OldValue.GetFieldDataType())
            {
            case GridBackendUtil::STRING:
            case GridBackendUtil::STRING_LIST:
                p_Info.m_Row->AddField(m_OldValue.GetValueStr(), m_ColumnKey);
                break;
            case GridBackendUtil::DATE:
            case GridBackendUtil::DATE_LIST:
                p_Info.m_Row->AddField(m_OldValue.GetValueTimeDate(), m_ColumnKey);
                break;
            case GridBackendUtil::BOOL:
            case GridBackendUtil::BOOL_LIST:
                p_Info.m_Row->AddField(m_OldValue.GetValueBool(), m_ColumnKey);
                break;
            default:
                ASSERT(false);
                break;
            }
        }
        else
        {
            // Put back old value in vector
            auto& field = p_Info.m_Row->GetField(m_ColumnKey);
            switch (m_OldValue.GetFieldDataType())
            {
            case GridBackendUtil::STRING:
            case GridBackendUtil::STRING_LIST:
                field.GetValuesMulti<PooledString>()->operator[](p_Info.m_Index) = m_OldValue.GetValueStr();
                break;
            case GridBackendUtil::DATE:
            case GridBackendUtil::DATE_LIST:
                field.GetValuesMulti<YTimeDate>()->operator[](p_Info.m_Index) = m_OldValue.GetValueTimeDate();
                break;
            case GridBackendUtil::BOOL:
            case GridBackendUtil::BOOL_LIST:
                field.GetValuesMulti<bool>()->operator[](p_Info.m_Index) = m_OldValue.GetValueBool();
                break;
            default:
                ASSERT(false);
                break;
            }
        }
    }
}

UINT SubItemFieldUpdate::GetColumnKey() const
{
    return m_ColumnKey;
}

std::unique_ptr<SubItemFieldUpdate> SubItemFieldUpdate::CreateReplacement(SubItemFieldUpdate& p_ToBeReplaced, std::unique_ptr<SubItemFieldUpdate> p_Replacement)
{
    ASSERT(p_Replacement->m_OldValue == p_ToBeReplaced.m_NewValue);
    p_Replacement->m_OldValue = p_ToBeReplaced.m_OldValue;
    return p_Replacement;
}

bool SubItemFieldUpdate::Refresh(GridBackendRow* p_Row, const std::map<SubItemKey, HttpResultWithError>& p_Errors)
{
    bool changed = false;
    auto oldState = m_State;

	if (IsCollapsedRow(p_Row))
		m_ActualValue = SubItemFieldValue(p_Row->GetField(m_ColumnKey), GetMultiValueIndex(p_Row));
	else
		m_ActualValue = SubItemFieldValue(p_Row->GetField(m_ColumnKey));

	auto ittErr = p_Errors.find(GetKey());
	if (ittErr != p_Errors.end() && ittErr->second.m_Error != boost::none)
	{
		m_State = Modification::State::RemoteError;
	}
	else
	{
		if (m_ActualValue == m_OldValue)
			m_State = State::RemoteHasOldValue;
		else if (m_ActualValue == m_NewValue)
			m_State = State::RemoteHasNewValue;
		else
			m_State = State::RemoteHasOtherValue;
	}

    if (m_State != oldState)
        changed = true;

    return changed;
}

void SubItemFieldUpdate::ShowAppliedLocally(GridBackendRow* p_Row) const
{
	if (!IsCollapsedRow(p_Row))
		addFormatterForExpandedRow(p_Row);
	else
		addFormatterForCollapsedRow(p_Row);
}

void SubItemFieldUpdate::addFormatterForCollapsedRow(GridBackendRow* p_Row) const
{
    std::vector<PooledString>* pkIds = p_Row->GetField(m_ColSubItemPrimaryKey).GetValuesMulti<PooledString>();

    const wstring pkVal = getMostSpecificSubItemPk();

    if (nullptr != pkIds)
    {
        for (size_t i = 0; i < pkIds->size(); ++i)
        {
            if (pkIds->operator[](i) == pkVal && ShouldCreateFormatter(p_Row, i))
            {
                createOrUpdateFormatter(p_Row, i);
            }
        }
    }
}

void SubItemFieldUpdate::addFormatterForExpandedRow(GridBackendRow* p_Row) const
{
    ASSERT(nullptr != m_ColSubItemPrimaryKey);
    if (nullptr != m_ColSubItemPrimaryKey)
    {
        const MultivalueRowManager& mvMgr = GetGrid().GetMultivalueManager();
        vector<pair<GridBackendRow*, map<GridBackendColumn*, vector<GridBackendRow*>>>>	implosionSetup;// source row vs. col + exploded rows
        mvMgr.GetImplosionSetup({ p_Row }, implosionSetup);
        ASSERT(implosionSetup.size() <= 1);
        if (implosionSetup.size() == 1)
        {
            const auto sourceRow = implosionSetup.front().first;
            auto& rows = implosionSetup.front().second[m_ColSubItemPrimaryKey];
            if (sourceRow == p_Row)
                createOrUpdateFormatter(sourceRow, 0);
            else
            {
                ptrdiff_t pos = std::find(rows.begin(), rows.end(), p_Row) - rows.begin();
                if (pos < static_cast<ptrdiff_t>(rows.size()))
                    createOrUpdateFormatter(sourceRow, pos + 1);// rows does not contain the source row
                else
                    ASSERT(false);// not found!
            }
        }
    }
}

void SubItemFieldUpdate::createOrUpdateFormatter(GridBackendRow* p_Row, size_t p_Index) const
{
    UINT columnKey = GetColumnKey();

    GridBackendColumn* column = GetGrid().GetColumnByID(columnKey);
    ASSERT(nullptr != column);
    if (nullptr != column)
    {
        GridBackendField& field = p_Row->GetField(column);
        const std::shared_ptr<ICellFormatter>& formatter = column->GetCellFormatter();
        if (nullptr == formatter)
        {
            auto compositeFormatter = std::make_shared<CompositeSubElementFormatter>();
            auto prefixFormatter = createPrefixFormatter();

            prefixFormatter->AddElement(&field, p_Index);
            compositeFormatter->AddFormatter(std::move(prefixFormatter), g_FormatterId);
            column->SetCellFormatter(compositeFormatter);
        }
        else
        {
            auto compositeFormatter = dynamic_cast<CompositeSubElementFormatter*>(formatter.get());
            ASSERT(nullptr != compositeFormatter);
            if (nullptr != compositeFormatter)
            {
                // Make sure a formatter for sub item updates exists
                if (!compositeFormatter->HasFormatter(g_FormatterId))
                    compositeFormatter->AddFormatter(std::move(createPrefixFormatter()), g_FormatterId);

                // Add the new element to that formatter
                compositeFormatter->AddElement(&p_Row->GetField(column), p_Index, g_FormatterId);
            }
        }
    }
}

std::unique_ptr<SubElementPrefixFormatter> SubItemFieldUpdate::createPrefixFormatter() const
{
    return std::make_unique<SubElementPrefixFormatter>(_YTEXT("[UPDATED]"));
}

const SubItemFieldValue& SubItemFieldUpdate::GetActualValue() const
{
	return m_ActualValue;
}

const SubItemFieldValue& SubItemFieldUpdate::GetOldValue() const
{
	return m_OldValue;
}

const SubItemFieldValue& SubItemFieldUpdate::GetNewValue() const
{
	return m_NewValue;
}

vector<Modification::ModificationLog> SubItemFieldUpdate::GetModificationLogs() const
{
	auto column = GetGrid().GetColumnByID(GetColumnKey());
	ASSERT(nullptr != column);

	ModificationLog mlog(getPk(), GetObjectName(), ActivityLoggerUtil::g_ActionUpdate, nullptr != column ? column->GetTitleDisplayed() : ModificationLog::g_NotSet, GetState());

	const auto nv = GetNewValue().ToString();
	if (GridBackendUtil::g_NoValueString != nv)
		mlog.m_NewValue = nv;

	const auto ov = GetOldValue().ToString();
	if (GridBackendUtil::g_NoValueString != ov)
		mlog.m_OldValue = ov;

	return { mlog };
}