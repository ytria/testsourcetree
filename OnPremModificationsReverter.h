#pragma once

#include "IModificationReverter.h"

template<class GridClass>
class OnPremModificationsReverter : public IModificationReverter
{
public:
	OnPremModificationsReverter(GridClass& p_Grid);

	void RevertAll() override;
	void RevertSelected() override;

	enum : INT_PTR
	{
		CANCEL = IDCANCEL,
		UNDOBOTH = IDOK,
		UNDOREGULAR = IDYES,
		UNDOONPREM = IDNO,
	};

	static INT_PTR RevertAllConfirm(CWnd* p_Parent, bool p_RegularMods, bool p_OnPremMods);
	static INT_PTR RevertSelectedConfirm(CWnd* p_Parent, bool p_RegularMods, bool p_OnPremMods);

private:
	GridClass& m_Grid;
};

template<class GridClass>
OnPremModificationsReverter<GridClass>::OnPremModificationsReverter(GridClass& p_Grid)
	: m_Grid(p_Grid)
{
}

template<class GridClass>
void OnPremModificationsReverter<GridClass>::RevertAll()
{
	ASSERT(m_Grid.GetParentGridFrameBase()->hasRevertableModificationsPending(false));

	const auto res = RevertAllConfirm(&m_Grid, m_Grid.HasModificationsPending(false), m_Grid.HasOnPremiseChanges(false));
	if (UNDOBOTH == res || UNDOREGULAR == res)
		m_Grid.RevertAllModifications(UNDOREGULAR == res);
	if (UNDOBOTH == res || UNDOONPREM == res)
		m_Grid.RevertOnPrem(false, UNDOONPREM == res);
	if (UNDOBOTH == res)
		m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

template<class GridClass>
void OnPremModificationsReverter<GridClass>::RevertSelected()
{
	ASSERT(m_Grid.GetParentGridFrameBase()->hasRevertableModificationsPending(true));

	const auto res = RevertSelectedConfirm(&m_Grid, m_Grid.HasModificationsPending(true), m_Grid.HasOnPremiseChanges(true));
	if (UNDOBOTH == res || UNDOREGULAR == res)
		m_Grid.RevertSelectedRows(UNDOREGULAR == res ? O365Grid::RevertFlags::UPDATE_GRID_IF_NEEDED : 0);
	if (UNDOBOTH == res || UNDOONPREM == res)
		m_Grid.RevertOnPrem(true, UNDOONPREM == res);
	if (UNDOBOTH == res)
		m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

template<class GridClass>
INT_PTR OnPremModificationsReverter<GridClass>::RevertAllConfirm(CWnd* p_Parent, bool p_RegularMods, bool p_OnPremMods)
{
	YCodeJockMessageBox confirmation(
		p_Parent,
		DlgMessageBox::eIcon_ExclamationWarning,
		YtriaTranslate::Do(GridFrameBase_On365RevertAll_1, _YLOC("Undo all changes")).c_str(),
		YtriaTranslate::Do(GridFrameBase_On365RevertAll_2, _YLOC("Are you sure?")).c_str(),
		YtriaTranslate::Do(GridFrameBase_On365RevertAll_3, _YLOC("All the pending changes will be lost.")).c_str(),
		p_RegularMods && p_OnPremMods
		? std::unordered_map<UINT, wstring>{ { UNDOBOTH, _T("Undo both") } , { UNDOREGULAR, _T("Undo O365") }, { UNDOONPREM, _T("Undo AS DS") },{ CANCEL, _T("Cancel") } }
		: std::unordered_map<UINT, wstring>{ { p_RegularMods ? UNDOREGULAR : UNDOONPREM, YtriaTranslate::Do(GridFrameBase_On365RevertAll_4, _YLOC("Yes")).c_str() }, { CANCEL, YtriaTranslate::Do(GridFrameBase_On365RevertAll_5, _YLOC("No")).c_str() } }
	);

	return confirmation.DoModal();
}

template<class GridClass>
INT_PTR OnPremModificationsReverter<GridClass>::RevertSelectedConfirm(CWnd* p_Parent, bool p_RegularMods, bool p_OnPremMods)
{
	YCodeJockMessageBox confirmation(
		p_Parent,
		DlgMessageBox::eIcon_ExclamationWarning,
		YtriaTranslate::Do(GridFrameBase_On365RevertSelected_1, _YLOC("Undo selected changes")).c_str(),
		YtriaTranslate::Do(GridFrameBase_On365RevertSelected_2, _YLOC("Are you sure?")).c_str(),
		YtriaTranslate::Do(GridFrameBase_On365RevertSelected_3, _YLOC("Pending changes in selection will be lost.")).c_str(),
		p_RegularMods && p_OnPremMods
		? std::unordered_map<UINT, wstring>{ { UNDOBOTH, _T("Undo both") }, { UNDOREGULAR, _T("Undo O365") }, { UNDOONPREM, _T("Undo AS DS") }, { CANCEL, _T("Cancel") } }
		: std::unordered_map<UINT, wstring>{ { p_RegularMods ? UNDOREGULAR : UNDOONPREM, YtriaTranslate::Do(GridFrameBase_On365RevertSelected_4, _YLOC("Yes")).c_str() },{ CANCEL, YtriaTranslate::Do(GridFrameBase_On365RevertSelected_5, _YLOC("No")).c_str() } }
	);

	return confirmation.DoModal();
}
