#include "DlgUserCacheDataHTML.h"

#include "AutomatedApp.h"
#include "CacheGrid.h"
#include "CodeJockFrameBase.h"
#include "Localization/YtriaTranslate.h"

namespace
{
	static const wstring g_OkButtonID			= _YTEXT("ok");
	static const wstring g_TitleTxtID			= _YTEXT("title-txt");
	static const wstring g_IntroTxtID			= _YTEXT("intro-txt");
	static const wstring g_CachedTxtBoldID		= _YTEXT("cached-txt-bold");
	static const wstring g_CachedTxtRegID		= _YTEXT("cached-txt-reg");
	static const wstring g_CacheTxtSecondID		= _YTEXT("cached-txt-second");
	static const wstring g_ServerTxtBoldID		= _YTEXT("server-txt-bold");
	static const wstring g_ServerTxtRegID		= _YTEXT("server-txt-reg");
	static const wstring g_ServerTxtSecondID	= _YTEXT("server-txt-second");
	static const wstring g_CommentTxtBoldID		= _YTEXT("comment-txt-bold");
	static const wstring g_CommentTxtRegID		= _YTEXT("comment-txt-reg");
}

DlgUserCacheDataHTML::DlgUserCacheDataHTML( CWnd* p_Parent)
	: ResizableDialog(IDD, p_Parent)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_OkButton(_T("OK"))
{
// 	if (g_Sizes.empty())
// 	{
// 		const bool success = Str::LoadSizeFile(IDR_USERCACHE_SIZE, g_Sizes);
// 		ASSERT(success);
// 	}

	m_HtmlView->SetLinkOpeningPolicy(true, true);
}


DlgUserCacheDataHTML::~DlgUserCacheDataHTML()
{}

void DlgUserCacheDataHTML::SetIntroWindow(const wstring& p_IntroWindow)
{
	m_IntroWindow = p_IntroWindow;
	const bool success = ChangeText(g_IntroTxtID, MFCUtil::encodeToHtmlEntitiesExceptTags(p_IntroWindow));
	ASSERT(success || !::IsWindow(m_HtmlView->m_hWnd));
}

void DlgUserCacheDataHTML::SetOkButton(const wstring& p_OkButton)
{
	m_OkButton = p_OkButton;
	const bool success = ChangeText(g_OkButtonID, MFCUtil::encodeToHtmlEntitiesExceptTags(p_OkButton));
	ASSERT(success || !::IsWindow(m_HtmlView->m_hWnd));
}

void DlgUserCacheDataHTML::SetTitleWindow(const wstring& p_TitleWindow)
{
	m_TitleWindow = p_TitleWindow;
	const bool success = ChangeText(g_TitleTxtID, MFCUtil::encodeToHtmlEntitiesExceptTags(p_TitleWindow));
	ASSERT(success || !::IsWindow(m_HtmlView->m_hWnd));
}

void DlgUserCacheDataHTML::SetCacheTxtBold(const wstring& p_CacheTxtBold)
{
	m_CacheTxtBold = p_CacheTxtBold;
	const bool success = ChangeText(g_CachedTxtBoldID, MFCUtil::encodeToHtmlEntitiesExceptTags(p_CacheTxtBold));
	ASSERT(success || !::IsWindow(m_HtmlView->m_hWnd));
}

void DlgUserCacheDataHTML::SetCacheTxtLine(const wstring& p_CacheTxtLine)
{
	m_CacheTxtLine = p_CacheTxtLine;
	const bool success = ChangeText(g_CacheTxtSecondID, MFCUtil::encodeToHtmlEntitiesExceptTags(p_CacheTxtLine));
	ASSERT(success || !::IsWindow(m_HtmlView->m_hWnd));
}

void DlgUserCacheDataHTML::SetCacheTxtReg(const wstring& p_CacheTxtReg)
{
	m_CacheTxtReg = p_CacheTxtReg;
	const bool success = ChangeText(g_CachedTxtRegID, MFCUtil::encodeToHtmlEntitiesExceptTags(p_CacheTxtReg));
	ASSERT(success || !::IsWindow(m_HtmlView->m_hWnd));
}

void DlgUserCacheDataHTML::SetServerBold(const wstring& p_ServerTxteBold)
{
	m_ServerBold = p_ServerTxteBold;
	const bool success = ChangeText(g_ServerTxtBoldID, MFCUtil::encodeToHtmlEntitiesExceptTags(p_ServerTxteBold));
	ASSERT(success || !::IsWindow(m_HtmlView->m_hWnd));
}

void DlgUserCacheDataHTML::SetServerLine(const wstring& p_ServerTxtLine)
{
	m_ServerLine = p_ServerTxtLine;
	const bool success = ChangeText(g_ServerTxtSecondID, MFCUtil::encodeToHtmlEntitiesExceptTags(p_ServerTxtLine));
	ASSERT(success || !::IsWindow(m_HtmlView->m_hWnd));
}

void DlgUserCacheDataHTML::SetServerReg(const wstring& p_ServerTxtReg)
{
	m_ServerReg = p_ServerTxtReg;
	const bool success = ChangeText(g_ServerTxtRegID, MFCUtil::encodeToHtmlEntitiesExceptTags(p_ServerTxtReg));
	ASSERT(success || !::IsWindow(m_HtmlView->m_hWnd));
}

void DlgUserCacheDataHTML::SetCommentTextBold(const wstring& p_CommentTextBold)
{
	m_CommentTextBold = p_CommentTextBold;
	const bool success = ChangeText(g_CommentTxtBoldID, MFCUtil::encodeToHtmlEntitiesExceptTags(p_CommentTextBold));
	ASSERT(success || !::IsWindow(m_HtmlView->m_hWnd));
}

void DlgUserCacheDataHTML::SetCommentTextReg(const wstring& p_CommentTextReg)
{
	m_CommentTextReg = p_CommentTextReg;
	const bool success = ChangeText(g_CommentTxtRegID, MFCUtil::encodeToHtmlEntitiesExceptTags(p_CommentTextReg));
	ASSERT(success || !::IsWindow(m_HtmlView->m_hWnd));
}

BOOL DlgUserCacheDataHTML::OnInitDialogSpecificResizable()
{
	CWnd* pCtrl = GetDlgItem(IDC_DLG_HTMLCACHEUSER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_DLG_HTMLPLACEHOLDER, NULL);
	m_HtmlView->SetPostedDataTarget(this);
	m_HtmlView->SetDocumentCompleteCallback(this);
	m_HtmlView->YLoadFromResource(_YTEXT("CacheOrLoadData.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	const int height = [=]()
	{
		return (HIDPI_XW(300));
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			return HIDPI_XW(650);
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowText(CString(AutomatedApp::GetApplicationName().c_str()));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
	//BlockVResize(true);

	return TRUE;
}

bool DlgUserCacheDataHTML::ChangeText(const wstring& p_Id, const wstring& p_newText)
{
	if (m_HtmlView && ::IsWindow(m_HtmlView->m_hWnd))
	{
		m_HtmlView->ExecuteScript(_YTEXTFORMAT(L"changeTxt(\"%s\",\"%s\");", p_Id.c_str(), p_newText.c_str()));
		return true;
	}
	return false;
}


bool DlgUserCacheDataHTML::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	bool RetVal = true;
	PostedData::const_iterator itData = data.find(_YTEXT("OPTION"));
	if (itData->second == _YTEXT("SERVER"))
		endDialogFixed(IDOK);
	else if (itData->second == _YTEXT("CACHE"))
		endDialogFixed(IDCANCEL);
	ASSERT(data.find(g_OkButtonID) != data.end());

	return false;
}

void DlgUserCacheDataHTML::OnDocumentComplete()
{
	SetCacheTxtReg(m_CacheTxtReg);
	SetIntroWindow(m_IntroWindow);
	SetTitleWindow(m_TitleWindow);
	SetCacheTxtBold(m_CacheTxtBold);
	SetCacheTxtLine(m_CacheTxtLine);
	SetServerBold(m_ServerBold);
	SetServerLine(m_ServerLine);
	SetServerReg(m_ServerReg);
	SetCommentTextBold(m_CommentTextBold);
	SetCommentTextReg(m_CommentTextReg);
	SetOkButton(m_OkButton);
}
