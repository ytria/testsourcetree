#include "GenericGridRefresher.h"
#include "GridFrameBase.h"
#include "BaseO365Grid.h"

GenericGridRefresher::GenericGridRefresher(O365Grid& p_Grid)
	: m_Grid(p_Grid)
{
}

void GenericGridRefresher::RefreshAll()
{
	auto frame = m_Grid.GetParentGridFrameBase();

	auto doRefresh = checkPendingModifications(frame, true);
	if (doRefresh)
	{
		if (frame->GetGrid().WithLoadMore() && frame->GetGrid().HasRowWithMoreLoaded())
		{
			// Do not ask the question if Automation is running
			// If this refresh is manually triggered during an automation, the refresh will be blocked later, so clearing the grid will result in a empty grid :-o
			// If refresh is triggered by an automation action, refresh should occur whatever what, without dropping.
			// We might consider adding an option to RefreshAll action, or a specific DropAndRefresh action (calling On365RefreshAsFirstLoad())
			if (!AutomatedApp::IsAutomationRunning())
			{
				YCodeJockMessageBox confirmation(frame,
					DlgMessageBox::eIcon_Question,
					YtriaTranslate::Do(GridFrameBase_On365Refresh_6, _YLOC("Reload Info?")).c_str(),
					YtriaTranslate::Do(GridFrameBase_On365Refresh_7, _YLOC("Additional information have previously been loaded, would you like to drop them or refresh them as well?")).c_str(),
					YtriaTranslate::Do(GridFrameBase_On365Refresh_8, _YLOC("Refreshing additional information can take some time, whereas dropping them will get the grid back to its initial state.")).c_str(),
					{ { IDOK, YtriaTranslate::Do(DlgBorder_OnInitDialog_3, _YLOC("Drop")).c_str() }
					, { IDYES, YtriaTranslate::Do(Command_ToString_26, _YLOC("Refresh")).c_str() }
					, { IDCANCEL, YtriaTranslate::Do(GridFrameBase_On365Refresh_5, _YLOC("Cancel")).c_str() } });

				const auto res = confirmation.DoModal();
				if (IDOK == res)
				{
					doRefresh = false;
					frame->SetDoLoadMoreOnNextRefresh(false);
					frame->SetSyncNow(false);
					frame->On365RefreshAsFirstLoadImpl(false);
				}
				else if (IDYES == res)
				{
					doRefresh = true;
					frame->SetDoLoadMoreOnNextRefresh(true);
				}
				else if (IDCANCEL == res)
				{
					doRefresh = false;
				}
			}
		}

		if (doRefresh)
		{
			frame->SetSyncNow(false);
			frame->On365RefreshImpl(vector<O365UpdateOperation>{}, frame->GetAutomationAction());
		}
	}
}

void GenericGridRefresher::RefreshSelection()
{
	auto frame = m_Grid.GetParentGridFrameBase();

	ASSERT(frame->m_CreateRefresh && frame->m_CreateRefreshPartial);

	if (checkPendingModifications(frame, true))
	{
		frame->SetSyncNow(false);
		frame->SetDoLoadMoreOnNextRefresh(true);
		frame->On365RefreshSelectionImpl(frame->GetRefreshSelectedData(), {}, false, frame->GetAutomationAction());
	}
}

void GenericGridRefresher::Sync()
{
	auto frame = m_Grid.GetParentGridFrameBase();

	ASSERT(frame->HasDeltaFeature());
	if (frame->HasDeltaFeature())
	{
		auto doRefresh = checkPendingModifications(frame, false);
		if (doRefresh)
		{
			if (frame->GetGrid().WithLoadMore() && frame->GetGrid().HasRowWithMoreLoaded())
			{
				// Do not ask the question if Automation is running
				// If this refresh is manually triggered during an automation, the refresh will be blocked later, so clearing the grid will result in a empty grid :-o
				// If refresh is triggered by an automation action, refresh should occur whatever what, without dropping.
				// We might consider adding an option to RefreshAll action, or a specific DropAndRefresh action (calling On365RefreshAsFirstLoad())
				if (!AutomatedApp::IsAutomationRunning())
				{
					YCodeJockMessageBox confirmation(frame,
						DlgMessageBox::eIcon_Question,
						YtriaTranslate::Do(GridFrameBase_On365Refresh_6, _YLOC("Reload Info?")).c_str(),
						_T("Additional information have previously been loaded, would you like to refresh them as well?"),
						_T("Chosing Yes will refreshing additional information (can take some time), whereas chosing No will let them untouched."),
						{ { IDYES, _T("Yes") }
						, { IDNO, _T("No") }
						, { IDCANCEL, YtriaTranslate::Do(GridFrameBase_On365Refresh_5, _YLOC("Cancel")).c_str() } });

					const auto res = confirmation.DoModal();
					if (IDYES == res)
					{
						doRefresh = true;
						frame->SetDoLoadMoreOnNextRefresh(true);
					}
					else if (IDNO == res)
					{
						doRefresh = true;
						frame->SetDoLoadMoreOnNextRefresh(false);
					}
					else if (IDCANCEL == res)
					{
						doRefresh = false;
					}
				}
			}

			if (doRefresh)
			{
				frame->SetSyncNow(true);
				frame->On365RefreshImpl(vector<O365UpdateOperation>{}, frame->GetAutomationAction());
			}
		}
	}
}

bool GenericGridRefresher::checkPendingModifications(GridFrameBase* p_Frame, bool p_SelectionOnly) const
{
	bool doRefresh = true;

	p_Frame->m_ForceFullRefreshAfterApply = false;
	p_Frame->m_NextRefreshSelectedData.reset();

	if (!p_SelectionOnly && p_Frame->/*hasModificationsPending*/hasLoseableModificationsPending()
		|| p_SelectionOnly && p_Frame->/*hasModificationsPendingInParentOfSelected*/hasLoseableModificationsPendingInParentOfSelected())
	{
		auto applyConfirmationConfig = p_Frame->GetApplyConfirmationConfig(p_SelectionOnly);
		YCodeJockMessageBox confirmation(p_Frame,
			applyConfirmationConfig.m_Icon,
			YtriaTranslate::Do(GridFrameBase_On365Refresh_1, _YLOC("Changes pending")).c_str(),
			YtriaTranslate::Do(GridFrameBase_On365Refresh_2, _YLOC("Some changes have not been saved and will be lost if you refresh. Do you want to continue?")).c_str()/**/,
			applyConfirmationConfig.m_AdditionalWarning,
			{ { IDOK, YtriaTranslate::Do(GridFrameBase_On365Refresh_3, _YLOC("Continue")).c_str() }/**/,
			  { IDCANCEL, YtriaTranslate::Do(GridFrameBase_On365Refresh_5, _YLOC("Cancel")).c_str() } });

		// The user has to explicitly call Save or Revert in his script if he wants to get rid of this dialog.
		confirmation.SetBlockAutomation(true);

		switch (confirmation.DoModal())
		{
		case IDCANCEL:
			doRefresh = false;
			break;
		case IDOK: // Don't apply
			doRefresh = true;
			break;
		}
	}

	return doRefresh;
}
