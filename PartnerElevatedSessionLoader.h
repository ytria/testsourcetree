#pragma once

#include "ElevatedSessionLoader.h"

class PartnerElevatedSessionLoader : public ElevatedSessionLoader
{
public:
	PartnerElevatedSessionLoader(const SessionIdentifier& p_Id);
};

