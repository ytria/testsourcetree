#pragma once

class Sapio365Session;

struct MainFrameSapio365Session
{
	operator shared_ptr<Sapio365Session>();
	operator shared_ptr<const Sapio365Session>();
};
