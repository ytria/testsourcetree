#include "CachedUserDeserializer.h"

#include "JsonSerializeUtil.h"
#include "YtriaFieldsConstants.h"

void CachedUserDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YUID(YTRIA_CACHEDUSER_DISPLAYNAME), m_Data.m_DisplayName, p_Object);
    JsonSerializeUtil::DeserializeString(_YUID(YTRIA_CACHEDUSER_USERPRINCIPALNAME), m_Data.m_UserPrincipalName, p_Object);
    JsonSerializeUtil::DeserializeString(_YUID(YTRIA_CACHEDUSER_USERTYPE), m_Data.m_UserType, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YUID(YTRIA_CACHEDUSER_DELETEDDATETIME), m_Data.m_DeletedDateTime, p_Object);
}
