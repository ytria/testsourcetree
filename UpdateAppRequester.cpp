#include "UpdateAppRequester.h"
#include "ApplicationDeserializer.h"
#include "JsonSerializeUtil.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "MsGraphHttpRequestLogger.h"

UpdateAppRequester::UpdateAppRequester(const Application& p_App)
	:m_App(p_App)
{
}

TaskWrapper<void> UpdateAppRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri;
	uri.append_path(_YTEXT("applications"));
	uri.append_path(m_App.m_Id ? *m_App.m_Id : _YTEXT(""));

	// TODO: Use appSerializer
	auto payloadJson = web::json::value::object();
	payloadJson.as_object()[_YTEXT("passwordCredentials")] = web::json::value::array();
	
	auto& passwordCredArr = payloadJson.as_object()[_YTEXT("passwordCredentials")].as_array();
	if (m_App.m_PasswordCredentials)
	{
		const auto& creds = *m_App.m_PasswordCredentials;
		for (size_t i = 0; i < m_App.m_PasswordCredentials->size(); ++i)
		{
			passwordCredArr[i] = web::json::value::object();
			JsonSerializeUtil::SerializeTimeDate(_YTEXT("endDateTime"), creds[i].m_EndDateTime, passwordCredArr[i].as_object());
			JsonSerializeUtil::SerializeString(_YTEXT("secretText"), creds[i].m_SecretText, passwordCredArr[i].as_object());
		}
	}

	WebPayloadJSON payload(payloadJson);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Patch(uri.to_uri(), httpLogger, p_TaskData, payload, _YTEXT("beta")).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResInfo) {
		try
		{
			result->SetResult(p_ResInfo.get());
		}
		catch (const RestException& e)
		{
			RestResultInfo info(e.GetRequestResult());
			info.m_RestException = std::make_shared<RestException>(e);
			result->SetResult(info);
		}
		catch (const std::exception& e)
		{
			RestResultInfo info;
			info.m_Exception = e;
			result->SetResult(info);
		}
		catch (...)
		{
			result->SetResult(RestResultInfo());
		}
	});
}

const SingleRequestResult& UpdateAppRequester::GetResult() const
{
	return *m_Result;
}
