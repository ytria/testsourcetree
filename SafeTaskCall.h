#pragma once

#include "RestSession.h"

template <typename T>
TaskWrapper<T> safeTaskCall(TaskWrapper<T> p_Task, std::shared_ptr<RestSession> p_Session, YtriaTaskData p_TaskData)
{
    return p_Session->GetYSafeRestTaskCaller()->DoSafeRestCall(p_Task, p_TaskData);
}

template <typename T>
TaskWrapper<T> safeTaskCall(TaskWrapper<T> p_Task, std::shared_ptr<RestSession> p_Session, pplx::cancellation_token p_CancellationToken, YtriaTaskData p_TaskData)
{
    return p_Session->GetYSafeRestTaskCaller()->DoSafeRestCall(p_Task, p_CancellationToken, p_TaskData);
}

template <typename T, typename AnyErrorOp>
TaskWrapper<T> safeTaskCall(TaskWrapper<T> p_Task, std::shared_ptr<RestSession> p_Session, AnyErrorOp p_ErrorHandler, YtriaTaskData p_TaskData)
{
    return p_Session->GetYSafeRestTaskCaller()->DoSafeRestCall(p_Task, p_ErrorHandler, p_TaskData);
}

template <typename T, typename AnyErrorHandlerOp, typename RestErrorOp, typename NetworkErrorOp, typename OAuth2ErrorOp, typename StdExceptionErrorOp, typename OtherErrorsOp>
TaskWrapper<T> safeTaskCall(
    TaskWrapper<T> p_Task,
    std::shared_ptr<RestSession> p_Session,
    AnyErrorHandlerOp p_AnyErrorHandler,
    RestErrorOp p_RestErrorHandler,
    NetworkErrorOp p_NetworkErrorHandler,
    OAuth2ErrorOp p_OAuth2ErrorOp,
    StdExceptionErrorOp p_StdExceptionErrorOp,
    OtherErrorsOp p_OtherErrorsOp,
    YtriaTaskData p_TaskData)
{
    return p_Session->GetYSafeRestTaskCaller()->DoSafeRestCall(p_Task, p_AnyErrorHandler, p_RestErrorHandler, p_NetworkErrorHandler, p_OAuth2ErrorOp, p_StdExceptionErrorOp, p_OtherErrorsOp, p_TaskData);
}