#include "DlgOnPremServerConfig.h"

#include "Sapio365Settings.h"

namespace
{
	static const wstring& getPwdPlaceHolder()
	{
		static const wstring str = _T("Password Not Shown When Saved");
		return str;
	}
}

DlgOnPremServerConfig::DlgOnPremServerConfig(CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
{
	m_Server = OnPremServerSetting().Get();
	m_Username = OnPremUsernameSetting().Get();
	m_Password = m_Username ? OnPremEncryptedPasswordSetting(*m_Username).Get() : boost::YOpt<wstring>();
	if (!m_Password) // Backward compat -- remove
	{
		m_Password = OnPremPasswordSetting().Get();
		if (m_Password && m_Username)
		{
			OnPremEncryptedPasswordSetting(*m_Username).Set(m_Password);
			OnPremPasswordSetting().Set(boost::none);
		}
	}
}

wstring DlgOnPremServerConfig::getDialogTitle() const
{
	return _T("Powershell AD DS credentials");
}

void DlgOnPremServerConfig::generateJSONScriptData()
{
	initMain(_YTEXT("DlgPowershellConfig"));

	ASSERT(isEditDialog());

	getGenerator().addIconTextField(_YTEXT("intro"), _T("Those parameters will be securely encrypted and saved."), _YTEXT("fas fa-info-circle"));

	getGenerator().Add(BoolToggleEditor({ { _YTEXT("default"), _T("Erase settings and use default."), EditorFlags::DIRECT_EDIT, !m_Server && !m_Username && !m_Password } }));
	getGenerator().Add(StringEditor(_YTEXT("server"), _T("Server"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, m_Server ? *m_Server : _YTEXT("")));
	getGenerator().Add(StringEditor(_YTEXT("username"), _T("Username"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, m_Username ? *m_Username : _YTEXT("")));
	getGenerator().Add(PasswordEditor(_YTEXT("pwd"), _T("Password"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, m_Password ? getPwdPlaceHolder() : _YTEXT("")));

	getGenerator().addEvent(_YTEXT("default"), { _YTEXT("server"), EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueTrue });
	getGenerator().addEvent(_YTEXT("default"), { _YTEXT("username"), EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueTrue });
	getGenerator().addEvent(_YTEXT("default"), { _YTEXT("pwd"), EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueTrue });

	getGenerator().addApplyButton(_T("OK"));
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
}

bool DlgOnPremServerConfig::processPostedData(const IPostedDataTarget::PostedData& data)
{
	ASSERT(data.size() == 4 || data.size() == 1);

	if (data.size() == 1)
	{
		ASSERT(_YTEXT("default") == data.begin()->first && UsefulStrings::g_ToggleValueTrue == data.begin()->second);

		OnPremServerSetting().Set(boost::none);
		OnPremUsernameSetting().Set(boost::none);
		OnPremEncryptedPasswordSetting(Str::g_EmptyString).Set(boost::none);
	}
	else if (data.size() == 4)
	{
		for (const auto& d : data)
		{
			if (_YTEXT("default") == d.first)
				ASSERT(UsefulStrings::g_ToggleValueFalse == d.second);
			else if (_YTEXT("server") == d.first)
				m_Server = d.second;
			else if (_YTEXT("username") == d.first)
				m_Username = d.second;
			else if (_YTEXT("pwd") == d.first && getPwdPlaceHolder() != d.second)
				m_Password = d.second;
		}

		OnPremServerSetting().Set(m_Server);
		OnPremUsernameSetting().Set(m_Username);
		if (m_Username)
			OnPremEncryptedPasswordSetting(*m_Username).Set(m_Password);
	}

	return true;
}
