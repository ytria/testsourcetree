#include "FrameOnPremiseGroups.h"
#include "UpdatedObjectsGenerator.h"

IMPLEMENT_DYNAMIC(FrameOnPremiseGroups, GridFrameBase)

FrameOnPremiseGroups::FrameOnPremiseGroups(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, p_Module, p_HistoryMode, p_PreviousFrame)
{
	m_CreateAddRemoveToSelection = !p_IsMyData;
	m_CreateRefreshPartial = false;
	m_CreateShowContext = !p_IsMyData;
}

void FrameOnPremiseGroups::ShowOnPremiseGroups(const vector<OnPremiseGroup>& p_OnPremiseGroups, bool p_FullPurge)
{
	m_OnPremiseGroupsGrid.BuildView(p_OnPremiseGroups, p_FullPurge);
}

void FrameOnPremiseGroups::ApplyAllSpecific()
{

}

void FrameOnPremiseGroups::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	auto updatedObjectsGen = UpdatedObjectsGenerator<OnPremiseGroup>();

	CommandInfo info;
	info.Data() = updatedObjectsGen;
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::OnPremiseGroups, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameOnPremiseGroups::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.Data() = p_RefreshData;
	info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::OnPremiseGroups, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

O365Grid& FrameOnPremiseGroups::GetGrid()
{
	return m_OnPremiseGroupsGrid;
}

void FrameOnPremiseGroups::createGrid()
{
	m_OnPremiseGroupsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameOnPremiseGroups::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigGroups();
}


AutomatedApp::AUTOMATIONSTATUS FrameOnPremiseGroups::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;
	return statusRV;
}
