#include "RemoveRoleAssignmentRequester.h"
#include "O365AdminUtil.h"
#include "Sapio365Session.h"
#include "SharepointOnlineSession.h"
#include "SingleRequestResult.h"
#include "BasicHttpRequestLogger.h"

Sp::RemoveRoleAssignmentRequester::RemoveRoleAssignmentRequester(PooledString p_SiteName, PooledString p_PrincipalId, PooledString p_RoleDefId)
    :m_SiteName(p_SiteName),
    m_PrincipalId(p_PrincipalId),
    m_RoleDefId(p_RoleDefId)
{
}

TaskWrapper<void> Sp::RemoveRoleAssignmentRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("sites"));
    uri.append_path(m_SiteName);
    uri.append_path(_YTEXT("_api"));
    uri.append_path(_YTEXT("web"));
    uri.append_path(_YTEXT("roleAssignments"));
    uri.append_path(wstring(_YTEXT("removeRoleAssignment(principalid=")) +
        wstring(m_PrincipalId) + wstring(_YTEXT(",roledefid=") + wstring(m_RoleDefId) + wstring(_YTEXT(")"))));

    LoggerService::User(YtriaTranslate::Do(Sp__RemoveRoleAssignmentRequester_Send_1, _YLOC("Removing role assignment for user \"%1\" in site \"%2\""), m_PrincipalId.c_str(), m_SiteName.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetSharepointSession()->Post(uri.to_uri(), httpLogger, m_Result, nullptr, p_TaskData);
}

const SingleRequestResult& Sp::RemoveRoleAssignmentRequester::GetResult() const
{
    return *m_Result;
}
