#include "BasicPageRequestLogger.h"

#include "LoggerService.h"
#include "Str.h"

BasicPageRequestLogger::BasicPageRequestLogger(const wstring& p_LogMessage)
{
	m_LogMessage = p_LogMessage;
}

void BasicPageRequestLogger::Log(const web::uri& p_Uri, size_t p_ObjCountToDate, HWND p_Originator)
{
	wstring log;

	if (!m_LogMessage.empty())
	{
		if (m_Page > 1)
		{
			wstring pageNum = Str::getStringFromNumber(m_Page);
			if (!m_ContextualInfo.empty())
			{
				if (p_ObjCountToDate > 0)
					log = _YFORMAT(L"Loading %s - batch #%s ( related to \"%s\" ) - %s received so far...", m_LogMessage.c_str(), pageNum.c_str(), m_ContextualInfo.c_str(), Str::getStringFromNumber(p_ObjCountToDate).c_str());
				else
					log = _YFORMAT(L"Loading %s - batch #%s ( related to \"%s\" )", m_LogMessage.c_str(), pageNum.c_str(), m_ContextualInfo.c_str());
			}
			else
			{
				if (p_ObjCountToDate > 0)
					log = _YFORMAT(L"Loading %s - batch #%s - %s received so far...", m_LogMessage.c_str(), pageNum.c_str(), Str::getStringFromNumber(p_ObjCountToDate).c_str());
				else
					log = _YFORMAT(L"Loading %s - batch #%s", m_LogMessage.c_str(), pageNum.c_str());
			}
		}
		else
		{
			if (!m_ContextualInfo.empty())
				log = _YFORMAT(L"Loading %s ( related to \"%s\" )", m_LogMessage.c_str(), m_ContextualInfo.c_str());
			else
				log = _YFORMAT(L"Loading %s", m_LogMessage.c_str());
		}

		LoggerService::User(log, p_Originator);
	}
}

std::shared_ptr<IPageRequestLogger> BasicPageRequestLogger::Clone() const
{
	auto clone = std::make_shared<BasicPageRequestLogger>(*this);
	clone->m_Page = 1;
	return clone;
}
