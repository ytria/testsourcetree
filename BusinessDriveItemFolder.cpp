#include "BusinessDriveItemFolder.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessDriveItemFolder>("Folder") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Folder"))))
		.constructor()(policy::ctor::as_object)
		.property("DisplayName", &BusinessDriveItemFolder::m_DisplayName)
		;

	registration::class_<BusinessDriveItemNotebook>("Notebook") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Notebook"))))
		.constructor()(policy::ctor::as_object)
		;
}

BusinessDriveItemFolder::BusinessDriveItemFolder() : BusinessDriveItemFolder(false)
{
}

BusinessDriveItemFolder::BusinessDriveItemFolder(const bool p_IsRoot) : BusinessDriveItem(),
	m_IsRoot(p_IsRoot),
	m_RecursiveCountFiles(0),
	m_RecursiveCountFolders(0)
{
	SetIsFolder(true);
}

BusinessDriveItemFolder::BusinessDriveItemFolder(const BusinessDriveItem& p_DriveItem, bool p_IsNotebook)
	: BusinessDriveItem(p_DriveItem)
	, m_IsRoot(false)
	, m_RecursiveCountFiles(0)
	, m_RecursiveCountFolders(0)
{
	ASSERT(!p_IsNotebook && IsFolder() || p_IsNotebook && IsNotebook());
	for (const auto& child : p_DriveItem.GetDriveItemChildren())
	{
		if (child.IsFolder())
			AddChildFolder(child);
		else if (child.IsNotebook())
			AddChildNotebook(child);
		else
			AddChild(child);
	}
}

BusinessDriveItemFolder::BusinessDriveItemFolder(const BusinessDriveItem& p_DriveItem)
	: BusinessDriveItemFolder(p_DriveItem, false)
{

}

bool BusinessDriveItemFolder::IsRoot() const
{
	return m_IsRoot;
}

const boost::YOpt<PooledString>& BusinessDriveItemFolder::GetDisplayName() const
{
	return m_DisplayName;
}

void BusinessDriveItemFolder::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}

wstring BusinessDriveItemFolder::GetObjectName() const
{
	return BusinessObject::GetObjectName();
}

int64_t BusinessDriveItemFolder::GetFolderSize() const
{
	int64_t size = 0;

	for (const auto& driveItem : m_Children)
	{
		ASSERT(!driveItem.IsFolder() && !driveItem.IsNotebook());
		if (!driveItem.IsFolder() && !driveItem.IsNotebook())
			size += driveItem.GetTotalSize();
	}

	return size;
}

void BusinessDriveItemFolder::SetDriveId(const PooledString& p_Val)
{
	BusinessDriveItem::SetDriveId(p_Val);

	for (auto& child : m_Children)
		child.SetDriveId(p_Val);
	for (auto& child : m_ChildrenFolder)
		child.SetDriveId(p_Val);
	for (auto& child : m_ChildrenNotebook)
		child.SetDriveId(p_Val);
}

const vector<BusinessDriveItem>& BusinessDriveItemFolder::GetChildren() const
{
	return m_Children;
}

const vector<BusinessDriveItemFolder>& BusinessDriveItemFolder::GetChildrenFolder() const
{
	return m_ChildrenFolder;
}

const vector<BusinessDriveItemNotebook>& BusinessDriveItemFolder::GetChildrenNotebook() const
{
	return m_ChildrenNotebook;
}

void BusinessDriveItemFolder::AddChild(const BusinessDriveItem& p_Child)
{
	ASSERT(!p_Child.IsFolder() && !p_Child.IsNotebook());
	m_Children.push_back(p_Child);
	++m_RecursiveCountFiles;
	if (IsRoot())
		addToTotalSize(p_Child);
}

void BusinessDriveItemFolder::AddChildFolder(const BusinessDriveItem& p_Child)
{
	ASSERT(p_Child.IsFolder());
	if (p_Child.IsFolder())
	{
		BusinessDriveItemFolder bdif(p_Child);
		m_ChildrenFolder.push_back(bdif);
		if (IsRoot())
			addToTotalSize(p_Child);
		updateFileAndFolderCounts(bdif);
	}
}

void BusinessDriveItemFolder::AddChildNotebook(const BusinessDriveItem& p_Child)
{
	ASSERT(p_Child.IsNotebook());
	if (p_Child.IsNotebook())
	{
		BusinessDriveItemNotebook bdin(p_Child);
		m_ChildrenNotebook.push_back(bdin);
		updateFileAndFolderCounts(bdin);
	}
}

void BusinessDriveItemFolder::addToTotalSize(const BusinessDriveItem& p_Child)
{
	ASSERT(IsRoot());
	if (IsRoot())
		SetTotalSize(GetTotalSize() + p_Child.GetTotalSize());
}

void BusinessDriveItemFolder::updateFileAndFolderCounts(const BusinessDriveItemFolder& p_Child)
{
	++m_RecursiveCountFolders;
	m_RecursiveCountFiles += p_Child.GetChildren().size();
	for (const auto& subFolder : p_Child.GetChildrenFolder())
		updateFileAndFolderCounts(subFolder);
}

void BusinessDriveItemFolder::updateFileAndFolderCounts(const BusinessDriveItemNotebook& p_Child)
{
	++m_RecursiveCountFolders;
	m_RecursiveCountFiles += p_Child.GetChildren().size();
}

size_t BusinessDriveItemFolder::GetRecursiveCountFiles() const
{
	return m_RecursiveCountFiles;
}

size_t BusinessDriveItemFolder::GetRecursiveCountFolders() const
{
	return m_RecursiveCountFolders;
}

size_t BusinessDriveItemFolder::GetChildrenCount() const
{
	return m_ChildrenFolder.size() + m_Children.size() + m_ChildrenNotebook.size();
}

bool BusinessDriveItemFolder::Isinvalid() const
{
	bool rvError = false;
	
	if (GetChildrenCount() == 1)
	{
		if (m_ChildrenFolder.size() == 1 && m_ChildrenFolder[0].GetError())
			rvError = true;
		else if (m_Children.size() == 1 && m_Children[0].GetError())
			rvError = true;
		else if (m_ChildrenNotebook.size() == 1 && m_ChildrenNotebook[0].GetError())
			rvError = true;
	}

	return rvError;
}

const boost::YOpt<SapioError>& BusinessDriveItemFolder::GetMainError() const
{
	static const boost::YOpt<SapioError> g_DummyError;
	if (!m_ChildrenFolder.empty())
		return m_ChildrenFolder[0].GetError();
	else if (!m_Children.empty())
		return m_Children[0].GetError();
	else if (!m_ChildrenNotebook.empty())
		return m_ChildrenNotebook[0].GetError();
	return g_DummyError;
}

// ========================================================================================

BusinessDriveItemNotebook::BusinessDriveItemNotebook()
	: BusinessDriveItemFolder(false)
{
	SetIsFolder(false);
	SetIsNotebook(true);
}

BusinessDriveItemNotebook::BusinessDriveItemNotebook(const BusinessDriveItem& p_DriveItem)
	: BusinessDriveItemFolder(p_DriveItem, true)
{
	
}

int64_t BusinessDriveItemNotebook::GetNotebookSize() const
{
	return GetFolderSize();
}