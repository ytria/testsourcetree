#include "SignInLocationDeserializer.h"
#include "GeoCoordinatesDeserializer.h"

void SignInLocationDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("city"), m_Data.m_City, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("countryOrRegion"), m_Data.m_CountryOrRegion, p_Object);
	{
		GeoCoordinatesDeserializer d;
		JsonSerializeUtil::DeserializeAny(d, _YTEXT("geoCoordinates"), p_Object);
		m_Data.m_GeoCoordinates = d.GetData();
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("state"), m_Data.m_State, p_Object);
}
