#pragma once

#include "ModuleBase.h"

class FrameDirectoryAudits;

class ModuleDirectoryAudits : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	virtual void executeImpl(const Command& p_Command) override;
	void showAuditLogs(Command p_Command);
	void loadSnapshot(const Command& p_Command);
};
