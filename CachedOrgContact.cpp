#include "CachedOrgContact.h"

#include "BusinessGroup.h"
#include "BusinessOrgContact.h"
#include "MsGraphFieldNames.h"

RTTR_REGISTRATION
{
    using namespace rttr;

registration::class_<CachedOrgContact>("CachedOrgContact") (metadata(MetadataKeys::TypeDisplayName, _YTEXT("Cached Organizational Contact")))
.constructor()(policy::ctor::as_object)
.property(O365_ID, &CachedOrgContact::m_Id)
.property(O365_USER_DISPLAYNAME, &CachedOrgContact::m_DisplayName)
.property(O365_USER_MAIL, &CachedOrgContact::m_Mail);

}

CachedOrgContact::CachedOrgContact(const BusinessOrgContact& p_OrgContact)
    : m_Id(p_OrgContact.GetID())
    , m_DisplayName(p_OrgContact.GetDisplayName())
    , m_Mail(p_OrgContact.GetMail())
{
}

BusinessOrgContact CachedOrgContact::ToBusinessOrgContact() const
{
	BusinessOrgContact orgContact;
	orgContact.SetID(m_Id);
	orgContact.SetDisplayName(m_DisplayName);
	orgContact.SetMail(m_Mail);
	return orgContact;
}

PooledString CachedOrgContact::GetId() const
{
	return m_Id;
}

PooledString CachedOrgContact::GetDisplayName() const
{
	return m_DisplayName ? m_DisplayName.get() : _YTEXT("");
}

PooledString CachedOrgContact::GetMail() const
{
    return m_Mail ? m_Mail.get() : _YTEXT("");
}
