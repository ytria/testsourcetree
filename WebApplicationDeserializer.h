#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "WebApplication.h"

class WebApplicationDeserializer : public JsonObjectDeserializer, public Encapsulate<WebApplication>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

