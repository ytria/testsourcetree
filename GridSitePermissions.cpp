#include "GridSitePermissions.h"
#include "AutomationWizardSitePermissions.h"
#include "BaseO365Grid.h"
#include "BasicGridSetup.h"
#include "FrameSitePermissions.h"
#include "GridUtil.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"
#include "SpUser.h"
#include "RoleDefinition.h"
#include "GridBackendUtil.h"
#include "RoleAssignmentPermission.h"
#include "RoleAssignmentPermissionDeletion.h"
#include "SubItemsFlagGetter.h"
#include "SpUtils.h"

BEGIN_MESSAGE_MAP(GridSitePermissions, ModuleO365Grid<BusinessSite>)
    ON_COMMAND(ID_SITEPERMISSIONSGRID_DELETEPERMISSIONS, OnDeletePermissions)
    ON_UPDATE_COMMAND_UI(ID_SITEPERMISSIONSGRID_DELETEPERMISSIONS, OnUpdateDeletePermissions)
END_MESSAGE_MAP()

GridSitePermissions::GridSitePermissions()
    : m_Frame(nullptr)
{
    // FIXME: Add desired actions when the required methods are implemented below!
	if (g_FamilyCommonInfo.empty() ) 
		g_FamilyCommonInfo = YtriaTranslate::Do(YtriaGridCluster_LocalizeStaticMembers_8, _YLOC("Common Info")).c_str();
	if(g_FamilyUserInfo.empty())
		g_FamilyUserInfo = YtriaTranslate::Do(DlgIDInfoContainer_OnInitDialog_4, _YLOC("User Info")).c_str();
	if(g_FamilyGroupInfo.empty())
		g_FamilyGroupInfo = YtriaTranslate::Do(DlgSearchAndReplaceGroupNavigatorResults__SARGrid_customizeGrid_6, _YLOC("Group Info")).c_str();
	if (g_FamilyPermissionInfo.empty())
		g_FamilyPermissionInfo = YtriaTranslate::Do(GridSitePermissions_g_FamilyPermissionInfo_1, _YLOC("Permission info")).c_str();
    UseDefaultActions(/*O365Grid::ACTION_CREATE | *//*O365Grid::ACTION_DELETE *//*| O365Grid::ACTION_EDIT | O365Grid::ACTION_VIEW*/0);
    EnableGridModifications();

    initWizard<AutomationWizardSitePermissions>(_YTEXT("Automation\\SitePermissions"));
}

GridSitePermissions::~GridSitePermissions()
{
    GetBackend().Clear();
}

ModuleCriteria GridSitePermissions::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_Frame->GetModuleCriteria();

    criteria.m_IDs.clear();
    for (auto row : p_Rows)
        criteria.m_IDs.insert(row->GetField(m_ColMetaId).GetValueStr());

    return criteria;
}

void GridSitePermissions::BuildView(const O365DataMap<BusinessSite, ModuleSitePermissions::SitePermissionsInfo>& p_SitePermissions, bool p_FullPurge)
{
    GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
        | (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
        | GridUpdaterOptions::REFRESH_MODIFICATION_STATES
        | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
        | GridUpdaterOptions::REFRESH_PROCESS_DATA
	);

    GridUpdater updater(*this, options);
    
    GridIgnoreModification ignoramus(*this);
    for (const auto& keyVal : p_SitePermissions)
    {
        const auto& site = keyVal.first;
        const auto& permInfo = keyVal.second;
    
        GridBackendField fieldSiteId(site.GetID(), m_ColMetaId);
        GridBackendRow* siteRow = m_RowIndex.GetRow({ fieldSiteId }, true, true);
        ASSERT(nullptr != siteRow);
        if (nullptr != siteRow)
        {
            updater.GetOptions().AddRowWithRefreshedValues(siteRow);
            updater.GetOptions().AddPartialPurgeParentRow(siteRow);

            siteRow->SetHierarchyParentToHide(true);
            siteRow->AddField(site.GetDisplayName(), m_ColMetaDisplayName);
            siteRow->AddField(site.GetName(), m_ColSiteName);
            siteRow->AddField(site.GetWebUrl(), m_ColWebUrl);
            siteRow->AddField(Sp::GetSiteUrlFromWebUrl(web::uri(site.GetWebUrl() ? *site.GetWebUrl() : _YTEXT(""))), m_ColSiteUrl);
    
            SetRowObjectType(siteRow, site);
            if (!site.HasFlag(BusinessObject::CANCELED))
            {
                if (permInfo.m_UsersListError)
                    updater.OnLoadingError(siteRow, *permInfo.m_UsersListError);
                else if (permInfo.m_RoleDefinitionsError)
                    updater.OnLoadingError(siteRow, *permInfo.m_RoleDefinitionsError);
                else
                {
					// Clear previous error
					if (siteRow->IsError())
						siteRow->ClearStatus();

                    // Display all direct users
                    for (const auto& keyVal : permInfo.m_Users)
                    {
                        const auto& userId = keyVal.first;
                        const auto& user = keyVal.second;
                        // Does the user has specific roles assigned?
                        auto itt = permInfo.m_UserRoles.find(userId);
                        if (itt != permInfo.m_UserRoles.end())
                        {
                            GridBackendField fieldGroupId(_YTEXT(""), m_ColGroupId);
                            GridBackendField fieldUserId(userId, m_ColUserId);
                            auto directUserRow = m_RowIndex.GetRow({ fieldSiteId, fieldGroupId, fieldUserId }, true, true);

							if (user.GetError())
							{
								updater.OnLoadingError(directUserRow, *user.GetError());
							}
							else
							{
								// Clear previous error
								if (directUserRow->IsError())
									directUserRow->ClearStatus();
							}

                            updater.GetOptions().AddRowWithRefreshedValues(directUserRow);
                            directUserRow->SetParentRow(siteRow);
                            SetRowObjectType(directUserRow, Sp::User(), user.HasFlag(BusinessObject::CANCELED));

                            directUserRow->AddField(site.GetDisplayName(), m_ColMetaDisplayName);
                            directUserRow->AddField(site.GetName(), m_ColSiteName);
                            directUserRow->AddField(site.GetWebUrl(), m_ColWebUrl);
                            directUserRow->AddField(Sp::GetSiteUrlFromWebUrl(web::uri(site.GetWebUrl() ? *site.GetWebUrl() : _YTEXT(""))), m_ColSiteUrl);

                            FillUserFields(site, user, directUserRow, updater);

                            vector<Sp::RoleDefinition> roleDefs;
                            for (const auto& roleDefId : itt->second)
                            {
                                Sp::RoleDefinition roleDef(roleDefId);
                                auto roleDefItt = permInfo.m_RoleDefinitions.find(roleDef);
                                if (roleDefItt != permInfo.m_RoleDefinitions.end())
                                {
                                    roleDefs.push_back(*roleDefItt);
                                }
                            }
                            FillPermissionFields(roleDefs, directUserRow, updater);
                        }
                    }
                }

                for (const auto& group : permInfo.m_Groups)
                {
                    PooledString groupId = group.Id ? std::to_wstring(*group.Id) : _YTEXT("");
                    GridBackendField fieldGroupId(groupId, m_ColGroupId);

                    auto groupRow = m_RowIndex.GetRow({ fieldGroupId, fieldSiteId }, true, true);
                    ASSERT(nullptr != groupRow);
                    if (nullptr != groupRow)
                    {
                        updater.GetOptions().AddRowWithRefreshedValues(groupRow);
                        groupRow->SetParentRow(siteRow);

                        SetRowObjectType(groupRow, group, group.HasFlag(BusinessObject::CANCELED));
                        groupRow->AddField(site.GetDisplayName(), m_ColMetaDisplayName);
                        groupRow->AddField(site.GetName(), m_ColSiteName);
                        groupRow->AddField(site.GetWebUrl(), m_ColWebUrl);
                        groupRow->AddField(Sp::GetSiteUrlFromWebUrl(web::uri(site.GetWebUrl() ? *site.GetWebUrl() : _YTEXT(""))), m_ColSiteUrl);
						if (group.GetError())
						{
							updater.OnLoadingError(groupRow, *group.GetError());
						}
						else
						{
							// Clear previous error
							if (groupRow->IsError())
								groupRow->ClearStatus();
						}

                        FillGroupFields(site, group, groupRow, updater);

                        auto ittGroupRole = permInfo.m_GroupRoles.find(groupId);
                        if (ittGroupRole != permInfo.m_GroupRoles.end())
                        {
                            vector<Sp::RoleDefinition> roleDefs;
                            for (const auto& roleDefId : ittGroupRole->second)
                            {
                                Sp::RoleDefinition roleDef(roleDefId);
                                auto roleDefItt = permInfo.m_RoleDefinitions.find(roleDef);
                                if (roleDefItt != permInfo.m_RoleDefinitions.end())
                                    roleDefs.push_back(*roleDefItt);
                            }
                            FillPermissionFields(roleDefs, groupRow, updater);

                            auto itt = permInfo.m_GroupMembers.find(groupId);
                            if (itt != permInfo.m_GroupMembers.end())
                            {
                                for (const auto& memberUserId : itt->second)
                                {
                                    GridBackendField fieldUserId(memberUserId, m_ColUserId);
                                    auto userRow = m_RowIndex.GetRow({ fieldUserId, fieldGroupId, fieldSiteId }, true, true);
                                    ASSERT(nullptr != userRow);
                                    if (nullptr != userRow)
                                    {
                                        SetRowObjectType(userRow, Sp::User());
                                        userRow->AddField(site.GetDisplayName(), m_ColMetaDisplayName);
                                        userRow->AddField(site.GetName(), m_ColSiteName);
                                        userRow->AddField(site.GetWebUrl(), m_ColWebUrl);
                                        userRow->AddField(Sp::GetSiteUrlFromWebUrl(web::uri(site.GetWebUrl() ? *site.GetWebUrl() : _YTEXT(""))), m_ColSiteUrl);
                                        updater.GetOptions().AddRowWithRefreshedValues(userRow);
                                        userRow->SetParentRow(groupRow);

                                        auto userItt = permInfo.m_Users.find(memberUserId);
                                        if (userItt != permInfo.m_Users.end())
                                        {
                                            const auto& user = userItt->second;

											if (user.GetError())
											{
												updater.OnLoadingError(userRow, *user.GetError());
											}
											else
											{
												// Clear previous error
												if (userRow->IsError())
													userRow->ClearStatus();
											}

                                            userRow->AddField(group.Title, m_ColGroupName);
                                            FillUserFields(site, user, userRow, updater);
                                            FillPermissionFields(roleDefs, userRow, updater);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

ModuleSitePermissions::SitePermissionsChanges GridSitePermissions::GetSitePermissionsChanges(bool p_Selected)
{
    ModuleSitePermissions::SitePermissionsChanges permChanges;

    // Permission deletions
    for (const auto& deletedPermission : GetModifications().GetSubItemDeletions())
    {
        if (!p_Selected || (p_Selected && GetModifications().IsModInSelectedRows(deletedPermission.get())))
        {
            auto roleAssignmentPermDel = dynamic_cast<RoleAssignmentPermissionDeletion*>(deletedPermission.get());
            ASSERT(nullptr != roleAssignmentPermDel);
            if (nullptr != roleAssignmentPermDel)
            {
                if (roleAssignmentPermDel->IsUserPermissionDeletion())
                    permChanges.m_UserPermissionsToDelete.push_back(roleAssignmentPermDel->GetKey());
                else if (roleAssignmentPermDel->IsGroupPermissionDeletion())
                    permChanges.m_GroupPermissionsToDelete.push_back(roleAssignmentPermDel->GetKey());
            }
        }
    }

    return permChanges;
}

void GridSitePermissions::OnDeletePermissions()
{
    if (!GetSelectedRows().empty())
    {
        CommandInfo info;

        ModuleSitePermissions::SitePermissionsChanges permInfo;

        for (auto row : GetSelectedRows())
        {
            PooledString groupId = row->GetField(m_ColGroupId).GetValueStr();
            PooledString userId = row->GetField(m_ColUserId).GetValueStr();
            PooledString siteUrl = Sp::GetSiteUrlFromWebUrl(row->GetField(m_ColWebUrl).GetValueStr());
            
            RoleAssignmentPermission rap;
            rap.SiteUrl = siteUrl;
            rap.SiteId	= row->GetField(m_ColMetaId).GetValueStr();
            rap.UserId	= userId;
            rap.GroupId = groupId;
			rap.m_Row	= row;

            const auto& fieldRoleDefId = row->GetField(m_ColPermissionsId);
            if (GetMultivalueManager().IsRowExploded(row, m_ColPermissionsId))
            {
                PooledString roleDefId = fieldRoleDefId.GetValueStr();
                rap.RoleDefinitionId = roleDefId;
                CreateDeletion(row, rap);
            }
            else
            {
                std::vector<PooledString>* roleDefIds = fieldRoleDefId.GetValuesMulti<PooledString>();
                if (nullptr != roleDefIds)
                {
                    for (const auto& roleDefId : *roleDefIds)
                    {
                        rap.RoleDefinitionId = roleDefId;
                        CreateDeletion(row, rap);
                    }
                }
                else
                {
                    // Single value
                    PooledString roleDefId = fieldRoleDefId.GetValueStr();
                    rap.RoleDefinitionId = roleDefId;

                    CreateDeletion(row, rap);
                }
            }
        }
        UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
    }
}

void GridSitePermissions::OnUpdateDeletePermissions(CCmdUI* pCmdUI)
{
    bool enable = false;
    if (IsFrameConnected() && hasNoTaskRunning())
    {
		for (auto row : GetSelectedRows())
        {
            if ((GridUtil::isBusinessType<Sp::User>(row) || GridUtil::isBusinessType<Sp::Group>(row)) && !row->IsMoreLoaded())
            {
                enable = true;
                break;
            }
        }
    }

    pCmdUI->Enable(enable ? TRUE : FALSE);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridSitePermissions::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameSitePermissions, LicenseUtil::g_codeSapio365sitePermissions,
		{
			{ &m_ColMetaDisplayName, YtriaTranslate::Do(GridLists_customizeGrid_1, _YLOC("Site Display Name")).c_str(), g_FamilyOwner },
			{ &m_ColMetaUserDisplayName, g_TitleOwnerUserName, g_FamilyOwner },
			{ &m_ColMetaId, g_TitleOwnerID,	g_FamilyOwner }
		}, { Sp::User() });
		setup.Setup(*this, false);
	}

    setBasicGridSetupHierarchy({ { { m_ColMetaDisplayName, 0 } }
								,{ rttr::type::get<Sp::User>().get_id() }
								,{ rttr::type::get<Sp::User>().get_id(), rttr::type::get<Sp::Group>().get_id(), rttr::type::get<BusinessSite>().get_id() } });

    //addColumnIsLoadMore(_YUID("loadMore"), _TLOC("Permissions loaded"));
    addColumnGraphID();

    m_ColSiteName	= AddColumn(_YUID("name"), YtriaTranslate::Do(GridSites_customizeGrid_6, _YLOC("Site Name")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColGroupId	= AddColumn(_YUID("groupId"), YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_2, _YLOC("Group ID")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColUserId		= AddColumn(_YUID("userId"), YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_6, _YLOC("User ID")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColWebUrl		= AddColumn(_YUID("webUrl"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_33, _YLOC("Web Url")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetTech });

    // Common columns
    m_ColIsHiddenInUI	= AddColumnCheckBox(_YUID("isHiddenInUI"), YtriaTranslate::Do(GridSpGroups_customizeGrid_15, _YLOC("Is Hidden in UI")).c_str(), g_FamilyCommonInfo, g_ColumnSize12);
    m_ColLoginName0		= AddColumn(_YUID("claimLoginName0"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_34, _YLOC("Claim Login Name (0)")).c_str(), g_FamilyCommonInfo, g_ColumnSize12);
    m_ColLoginName1     = AddColumn(_YUID("claimLoginName1"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_35, _YLOC("Claim Login Name (1)")).c_str(), g_FamilyCommonInfo, g_ColumnSize12);
    m_ColLoginName2     = AddColumn(_YUID("claimLoginName2"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_36, _YLOC("Claim Login Name (2)")).c_str(), g_FamilyCommonInfo, g_ColumnSize12);
    m_ColPrincipalType	= AddColumn(_YUID("principalType"), YtriaTranslate::Do(GridSpGroups_customizeGrid_4, _YLOC("Principal Type")).c_str(), g_FamilyCommonInfo, g_ColumnSize12);
    m_ColGroupName		= AddColumn(_YUID("groupName"), YtriaTranslate::Do(DlgSearchAndReplaceGroupNavigatorResults__SARGrid_customizeGrid_4, _YLOC("Group Name")).c_str(), g_FamilyCommonInfo, g_ColumnSize12);
    m_ColTitle			= AddColumn(_YUID("title"), YtriaTranslate::Do(GridSpGroups_customizeGrid_3, _YLOC("Title")).c_str(), g_FamilyCommonInfo, g_ColumnSize12, { g_ColumnsPresetDefault });

    // Group columns
    m_ColAllowMembersEditMembership		= AddColumn(_YUID("AllowMembersEditMembership"),		YtriaTranslate::Do(GridSpGroups_customizeGrid_6, _YLOC("Allow Members Edit Membership")).c_str(), g_FamilyGroupInfo,  g_ColumnSize12);
    m_ColAllowRequestToJoinLeave		= AddColumn(_YUID("AllowRequestToJoinLeave"),			YtriaTranslate::Do(GridSpGroups_customizeGrid_7, _YLOC("Allow Request To Join Leave")).c_str(), g_FamilyGroupInfo, g_ColumnSize12);
    m_ColAutoAcceptRequestToJoinLeave	= AddColumn(_YUID("AutoAcceptRequestToJoinLeave"),		YtriaTranslate::Do(GridSpGroups_customizeGrid_8, _YLOC("Auto Accept Request To Join Leave")).c_str(), g_FamilyGroupInfo, g_ColumnSize12);
    m_ColCanCurrentUserEditMembership	= AddColumn(_YUID("CanCurrentUserEditMembership"),		YtriaTranslate::Do(GridSpGroups_customizeGrid_9, _YLOC("Can Current User Edit Membership")).c_str(), g_FamilyGroupInfo, g_ColumnSize12);
    m_ColCanCurrentUserManageGroup		= AddColumn(_YUID("CanCurrentUserManageGroup"),			YtriaTranslate::Do(GridSpGroups_customizeGrid_10, _YLOC("Can Current User Manage Group")).c_str(), g_FamilyGroupInfo, g_ColumnSize12);
    m_ColCanCurrentUserViewMembership	= AddColumn(_YUID("CanCurrentUserViewMembership"),		YtriaTranslate::Do(GridSpGroups_customizeGrid_11, _YLOC("Can Current User View Membership")).c_str(), g_FamilyGroupInfo, g_ColumnSize12);
    m_ColDescription					= AddColumn(_YUID("Description"),						YtriaTranslate::Do(AutomatedApp_ProcessWndProcException_18, _YLOC("Description")).c_str(), g_FamilyGroupInfo, g_ColumnSize12);
    m_ColOnlyAllowMembersViewMembership = AddColumn(_YUID("OnlyAllowMembersViewMembership"),	YtriaTranslate::Do(GridSpGroups_customizeGrid_12, _YLOC("Only Allow Members View Membership")).c_str(), g_FamilyGroupInfo, g_ColumnSize12);
    m_ColOwnerTitle						= AddColumn(_YUID("OwnerTitle"),						YtriaTranslate::Do(GridSpGroups_customizeGrid_13, _YLOC("Owner Title")).c_str(), g_FamilyGroupInfo, g_ColumnSize12);
    m_ColRequestToJoinLeaveEmailSetting = AddColumn(_YUID("RequestToJoinLeaveEmailSetting"),	YtriaTranslate::Do(GridSpGroups_customizeGrid_14, _YLOC("Request To Join Leave Email Setting")).c_str(), g_FamilyGroupInfo, g_ColumnSize12);

    // User columns
    m_ColEmail			= AddColumn(_YUID("email"), YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_11, _YLOC("Email")).c_str(), g_FamilyUserInfo, g_ColumnSize12);
    m_ColIsSiteAdmin	= AddColumn(_YUID("isSiteAdmin"), YtriaTranslate::Do(GridSpUsers_customizeGrid_6, _YLOC("Is Site Admin")).c_str(), g_FamilyUserInfo, g_ColumnSize12);
    m_ColSiteUrl = AddColumn(_YUID("siteUrl"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_50, _YLOC("Site Url")).c_str(), g_FamilyUserInfo, g_ColumnSize12, { g_ColumnsPresetTech });

    // Permission columns
    m_ColPermissionsBasePermissionsHigh = AddColumnNumber(_YUID("permissionsBasePermissionsHigh"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_21, _YLOC("Permissions - Base Permissions - High")).c_str(), g_FamilyPermissionInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColPermissionsBasePermissionsLow	= AddColumnNumber(_YUID("permissionsBasePermissionsLow"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_22, _YLOC("Permissions - Base Permissions - Low")).c_str(), g_FamilyPermissionInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColPermissionsDescription			= AddColumn(_YUID("permissionsDescription"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_23, _YLOC("Permissions - Description")).c_str(), g_FamilyPermissionInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColPermissionsId					= AddColumn(_YUID("permissionsId"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_24, _YLOC("Permissions - ID")).c_str(), g_FamilyPermissionInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColPermissionsHidden				= AddColumnCheckBox(_YUID("permissionsHidden"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_25, _YLOC("Permissions - Hidden")).c_str(), g_FamilyPermissionInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColPermissionsName				= AddColumn(_YUID("permissionsName"),  YtriaTranslate::Do(GridSitePermissions_customizeGrid_26, _YLOC("Permissions - Name")).c_str(), g_FamilyPermissionInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColPermissionsOrder				= AddColumn(_YUID("permissionsOrder"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_27, _YLOC("Permissions - Order")).c_str(), g_FamilyPermissionInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColPermissionsRoleTypeKind		= AddColumn(_YUID("permissionsRoleTypeKind"), YtriaTranslate::Do(GridSitePermissions_customizeGrid_28, _YLOC("Permissions - Role Type Kind")).c_str(), g_FamilyPermissionInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColPermissionsBasePermissionsHigh->SetMultivalueFamily(YtriaTranslate::Do(GridSitePermissions_customizeGrid_29, _YLOC("Permissions")).c_str());
    m_ColPermissionsBasePermissionsHigh->AddMultiValueExplosionSister(m_ColPermissionsBasePermissionsLow);
    m_ColPermissionsBasePermissionsHigh->AddMultiValueExplosionSister(m_ColPermissionsDescription);
    m_ColPermissionsBasePermissionsHigh->AddMultiValueExplosionSister(m_ColPermissionsId);
    m_ColPermissionsBasePermissionsHigh->AddMultiValueExplosionSister(m_ColPermissionsHidden);
    m_ColPermissionsBasePermissionsHigh->AddMultiValueExplosionSister(m_ColPermissionsName);
    m_ColPermissionsBasePermissionsHigh->AddMultiValueExplosionSister(m_ColPermissionsOrder);
    m_ColPermissionsBasePermissionsHigh->AddMultiValueExplosionSister(m_ColPermissionsRoleTypeKind);

    AddColumnForRowPK(m_ColMetaId);
    AddColumnForRowPK(m_ColGroupId);
    AddColumnForRowPK(m_ColUserId);

    m_Frame = dynamic_cast<FrameSitePermissions*>(GetParentFrame());

    SetRowFlagger(std::make_unique<SubItemsFlagGetter>(GetModifications(), m_ColPermissionsId->GetMultiValueElderColumn()));

    if (nullptr != GetAutomationWizard())
        GetAutomationWizard()->SetFrame(m_Frame);
}

wstring GridSitePermissions::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Site"), m_ColMetaDisplayName }, { _T("Group"), m_ColGroupName } }, m_ColEmail);
}

void GridSitePermissions::FillGroupFields(const BusinessSite& p_Site, const Sp::Group& p_SpGroup, GridBackendRow* p_Row, GridUpdater& p_Updater)
{
    PooledString groupId = p_SpGroup.Id ? std::to_wstring(*p_SpGroup.Id) : _YTEXT("");
    p_Row->AddField(p_SpGroup.Title, m_ColTitle);
    p_Row->AddField(groupId, m_ColGroupId);
    p_Row->AddField(groupId, GetColId());

    // Common columns
    p_Row->AddField(p_SpGroup.IsHiddenInUI, m_ColIsHiddenInUI);
    FillLoginName(p_Row, p_SpGroup.LoginName);
    p_Row->AddField(p_SpGroup.PrincipalType, m_ColPrincipalType);
    p_Row->AddField(p_SpGroup.Title, m_ColGroupName);
    p_Row->AddField(p_SpGroup.Title, m_ColTitle);

    // Group columns
    p_Row->AddField(p_SpGroup.AllowMembersEditMembership, m_ColAllowMembersEditMembership);
    p_Row->AddField(p_SpGroup.AllowRequestToJoinLeave, m_ColAllowRequestToJoinLeave);
    p_Row->AddField(p_SpGroup.AutoAcceptRequestToJoinLeave, m_ColAutoAcceptRequestToJoinLeave);
    p_Row->AddField(p_SpGroup.CanCurrentUserEditMembership, m_ColCanCurrentUserEditMembership);
    p_Row->AddField(p_SpGroup.CanCurrentUserManageGroup, m_ColCanCurrentUserManageGroup);
    p_Row->AddField(p_SpGroup.CanCurrentUserViewMembership, m_ColCanCurrentUserViewMembership);
    p_Row->AddField(p_SpGroup.Description, m_ColDescription);
    p_Row->AddField(p_SpGroup.OnlyAllowMembersViewMembership, m_ColOnlyAllowMembersViewMembership);
    p_Row->AddField(p_SpGroup.OwnerTitle, m_ColOwnerTitle);
    p_Row->AddField(p_SpGroup.RequestToJoinLeaveEmailSetting, m_ColRequestToJoinLeaveEmailSetting);
}

void GridSitePermissions::FillUserFields(const BusinessSite& p_Site, const Sp::User& p_SpUser, GridBackendRow* p_Row, GridUpdater& p_Updater)
{
    PooledString userId = p_SpUser.Id ? std::to_wstring(*p_SpUser.Id) : _YTEXT("");
    p_Row->AddField(p_SpUser.Title, m_ColTitle);
    p_Row->AddField(userId, m_ColUserId);
    p_Row->AddField(userId, GetColId());

    // Common columns
    p_Row->AddField(p_SpUser.IsHiddenInUI, m_ColIsHiddenInUI);
    FillLoginName(p_Row, p_SpUser.LoginName);
    p_Row->AddField(p_SpUser.PrincipalType, m_ColPrincipalType);
    p_Row->AddField(p_SpUser.Title, m_ColTitle);

    // User columns
    p_Row->AddField(p_SpUser.Email, m_ColEmail);
    p_Row->AddField(p_SpUser.IsSiteAdmin, m_ColIsSiteAdmin);
}

void GridSitePermissions::FillPermissionFields(const vector<Sp::RoleDefinition>& p_RoleDefinitions, GridBackendRow* p_Row, GridUpdater& p_Updater)
{
    vector<PooledString> basePermissionsHigh;
    vector<PooledString> basePermissionsLow;
    vector<PooledString> description;
    vector<BOOLItem> hidden;
    vector<PooledString> id;
    vector<PooledString> name;
    vector<__int3264> order;
    vector<__int3264> roleTypeKind;
    for (const auto& roleDef : p_RoleDefinitions)
    {
        if (roleDef.BasePermissions)
        {
            basePermissionsHigh.push_back(roleDef.BasePermissions->High ? *roleDef.BasePermissions->High : GridBackendUtil::g_NoValueString);
            basePermissionsLow.push_back(roleDef.BasePermissions->Low ? *roleDef.BasePermissions->Low : GridBackendUtil::g_NoValueString);
        }
        else
        {
            basePermissionsHigh.push_back(GridBackendUtil::g_NoValueString);
            basePermissionsLow.push_back(GridBackendUtil::g_NoValueString);
        }

        description.push_back(roleDef.Description ? *roleDef.Description : GridBackendUtil::g_NoValueString);
        hidden.push_back(roleDef.Hidden ? *roleDef.Hidden : GridBackendUtil::g_NoValueBoolItem);
        id.push_back(roleDef.Id ? std::to_wstring(*roleDef.Id) : GridBackendUtil::g_NoValueString);
        name.push_back(roleDef.Name ? *roleDef.Name : GridBackendUtil::g_NoValueString);
        order.push_back(roleDef.Order ? *roleDef.Order : GridBackendUtil::g_NoValueNumberInt);
        roleTypeKind.push_back(roleDef.RoleTypeKind ? *roleDef.RoleTypeKind : GridBackendUtil::g_NoValueNumberInt);
    }

    if (!basePermissionsHigh.empty())
    {
        p_Row->AddField(basePermissionsHigh, m_ColPermissionsBasePermissionsHigh);
        p_Row->AddField(basePermissionsLow, m_ColPermissionsBasePermissionsLow);
        p_Row->AddField(description, m_ColPermissionsDescription);
        p_Row->AddField(hidden, m_ColPermissionsHidden);
        p_Row->AddField(id, m_ColPermissionsId);
        p_Row->AddField(name, m_ColPermissionsName);
        p_Row->AddField(order, m_ColPermissionsOrder);
        p_Row->AddField(roleTypeKind, m_ColPermissionsRoleTypeKind);
    }
    else
    {
        p_Row->RemoveField(m_ColPermissionsBasePermissionsHigh);
        p_Row->RemoveField(m_ColPermissionsBasePermissionsLow);
        p_Row->RemoveField(m_ColPermissionsDescription);
        p_Row->RemoveField(m_ColPermissionsHidden);
        p_Row->RemoveField(m_ColPermissionsId);
        p_Row->RemoveField(m_ColPermissionsName);
        p_Row->RemoveField(m_ColPermissionsOrder);
        p_Row->RemoveField(m_ColPermissionsRoleTypeKind);
    }
}

GridBackendColumn* GridSitePermissions::GetColPermissionsElder() const
{
    return m_ColPermissionsBasePermissionsHigh;
}

GridBackendColumn* GridSitePermissions::GetColPermissionsPk() const
{
    return m_ColPermissionsId;
}

GridBackendColumn* GridSitePermissions::GetColSiteUrl() const
{
	return m_ColSiteUrl;
}

GridBackendColumn* GridSitePermissions::GetColGroupId() const
{
	return m_ColGroupId;
}

GridBackendColumn* GridSitePermissions::GetColUserId() const
{
	return m_ColUserId;
}

bool GridSitePermissions::IsCorrespondingRow(GridBackendRow* p_Row, const RoleAssignmentPermission& p_AssigmentPermission)
{
    if (GetMultivalueManager().IsRowExploded(p_Row, m_ColPermissionsId))
    {
        PooledString siteUrl = p_Row->GetField(m_ColSiteUrl).GetValueStr();
        PooledString groupId = p_Row->GetField(m_ColGroupId).GetValueStr();
        PooledString userId = p_Row->GetField(m_ColUserId).GetValueStr();
        PooledString permissionsId = p_Row->GetField(m_ColPermissionsId).GetValueStr();

        // Exploded row
        if (siteUrl == p_AssigmentPermission.SiteUrl && groupId == p_AssigmentPermission.GroupId &&
            userId == p_AssigmentPermission.UserId && permissionsId == p_AssigmentPermission.RoleDefinitionId)
        {
            return true;
        }
    }
    else
    {
        // Collapsed row
        GridBackendField& fieldSiteUrl = p_Row->GetField(m_ColSiteUrl);
        GridBackendField& fieldGroupId = p_Row->GetField(m_ColGroupId);
        GridBackendField& fieldUserId = p_Row->GetField(m_ColUserId);
        GridBackendField& fieldRoleDefId = p_Row->GetField(m_ColPermissionsId);
        if (fieldSiteUrl.HasValue() && fieldGroupId.HasValue() && fieldUserId.HasValue())
        {
            if (PooledString(fieldSiteUrl.GetValueStr()) == p_AssigmentPermission.SiteUrl &&
                PooledString(fieldGroupId.GetValueStr()) == p_AssigmentPermission.GroupId &&
                PooledString(fieldUserId.GetValueStr()) == p_AssigmentPermission.UserId)
            {
                // Role Definition Id corresponding?
                std::vector<PooledString>* roleDefIds = fieldRoleDefId.GetValuesMulti<PooledString>();
                if (nullptr != roleDefIds)
                {
                    for (const auto& permId : *roleDefIds)
                    {
                        if (permId == p_AssigmentPermission.RoleDefinitionId)
                            return true;
                    }
                }
                else if (PooledString(fieldRoleDefId.GetValueStr()) == p_AssigmentPermission.RoleDefinitionId) // Row with only one permission
                {
                    return true;
                }
            }
        }
    }
    return false;
}

BusinessSite GridSitePermissions::getBusinessObject(GridBackendRow* p_Row) const
{
    ASSERT(GridUtil::isBusinessType<BusinessSite>(p_Row));
    return BusinessSite();
}

void GridSitePermissions::UpdateBusinessObjects(const vector<BusinessSite>& p_Sites, bool p_SetModifiedStatus)
{
    ASSERT(false);
}

void GridSitePermissions::RemoveBusinessObjects(const vector<BusinessSite>& p_Sites)
{
    ASSERT(false);
}

void GridSitePermissions::CreateDeletion(GridBackendRow* p_Row, const RoleAssignmentPermission& p_Info)
{
    auto deletion = std::make_unique<RoleAssignmentPermissionDeletion>(*this, p_Info);
    deletion->ShowAppliedLocally(p_Row);
    if (GetModifications().Add(std::move(deletion)))
        p_Row->SetEditError(false);
}

void GridSitePermissions::InitializeCommands()
{
    ModuleO365Grid<BusinessSite>::InitializeCommands();

    const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

    // Delete permissions
    {
        CExtCmdItem _cmd;
        _cmd.m_nCmdID = ID_SITEPERMISSIONSGRID_DELETEPERMISSIONS;
        _cmd.m_sMenuText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_13, _YLOC("Delete Permissions")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_13, _YLOC("Delete Permissions")).c_str();
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
        HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_SITEPERMISSIONSGRID_DELETEPERMISSIONS_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, IDB_SITEPERMISSIONSGRID_DELETEPERMISSIONS, hIcon, false);
        setRibbonTooltip(_cmd.m_nCmdID,
            YtriaTranslate::Do(GridDriveItems_InitializeCommands_13, _YLOC("Delete Permissions")).c_str(),
            YtriaTranslate::Do(GridDriveItems_InitializeCommands_13, _YLOC("Delete Permissions")).c_str(),
            _cmd.m_sAccelText);
    }
}

void GridSitePermissions::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {
        if (CRMpipe().IsFeatureSandboxed())
            pPopup->ItemInsert(ID_SITEPERMISSIONSGRID_DELETEPERMISSIONS);
    }
    ModuleO365Grid<BusinessSite>::OnCustomPopupMenu(pPopup, nStart);
}

void GridSitePermissions::FillLoginName(GridBackendRow* p_Row, const boost::YOpt<PooledString>& p_LoginName)
{
    wstring loginName = p_LoginName ? *p_LoginName : _YTEXT("");

    wstring login0;
    wstring login1;
    wstring login2;

    if (loginName.find(L'|') == wstring::npos)
    {
        login0 = loginName;
        p_Row->AddField(login0, m_ColLoginName0);
    }
    else
    {
        auto tokens = Str::explodeIntoVector(loginName, wstring(_YTEXT("|")));
        if (tokens.size() == 3)
        {
            login0 = tokens[0];
            login1 = tokens[1];
            login2 = tokens[2];

            p_Row->AddField(login1, m_ColLoginName1);
            p_Row->AddField(login2, m_ColLoginName2);
        }
        else
            login0 = loginName;
        p_Row->AddField(login0, m_ColLoginName0);
    }
}

wstring GridSitePermissions::g_FamilyCommonInfo;
wstring GridSitePermissions::g_FamilyUserInfo;
wstring GridSitePermissions::g_FamilyGroupInfo;
wstring GridSitePermissions::g_FamilyPermissionInfo;
