#pragma once

#include "ISessionsMigrationVersion.h"

class SessionsMigrationAddLoadAutoOnPremVersion4To5 : public ISessionsMigrationVersion
{
public:
	void Build() override;
	VersionSourceTarget GetVersionInfo() const override;

private:
	bool CreateNewFromOld();
	bool AddLoadAutoFlag();
};

