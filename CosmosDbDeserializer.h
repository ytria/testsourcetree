#pragma once

#include "CosmosDb.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

namespace Azure
{
	class CosmosDbDeserializer : public JsonObjectDeserializer, public Encapsulate<Azure::CosmosDb>
	{
	public:
		void DeserializeObject(const web::json::object& p_Object) override;
	};
}

