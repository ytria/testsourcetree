#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ContentType.h"

class ContentTypeDeserializer : public JsonObjectDeserializer, public Encapsulate<ContentType>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

