#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ContentTypeOrder.h"

class ContentTypeOrderDeserializer : public JsonObjectDeserializer, public Encapsulate<ContentTypeOrder>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

