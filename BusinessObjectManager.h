#pragma once

#include "BusinessAttachment.h"
#include "BusinessChannel.h"
#include "BusinessChatMessage.h"
#include "BusinessContactFolder.h"
#include "BusinessConversation.h"
#include "BusinessDirectoryRole.h"
#include "BusinessDirectoryRoleTemplate.h"
#include "BusinessDrive.h"
#include "BusinessDriveItemFolder.h"
#include "BusinessEvent.h"
#include "BusinessFileAttachment.h"
#include "BusinessGroup.h"
#include "BusinessGroupSetting.h"
#include "BusinessGroupSettingTemplate.h"
#include "BusinessItemAttachment.h"
#include "BusinessLicenseDetail.h"
#include "BusinessList.h"
#include "BusinessMessage.h"
#include "BusinessMailFolder.h"
#include "BusinessPermission.h"
#include "BusinessPost.h"
#include "BusinessSite.h"
#include "BusinessSubscribedSku.h"
#include "BusinessUser.h"
#include "BusinessLifeCyclePolicies.h"
#include "CachedGroup.h"
#include "Group.h"
#include "EventListRequester.h"
#include "LifeCyclePolicies.h"
#include "MessageListRequester.h"
#include "ModuleOptions.h"
#include "O365AdminUtil.h"
#include "O365UpdateOperation.h"
#include "ObjectDelta.h"
#include "MultiObjectsPageRequestLogger.h"
#include "Sapio365Session.h"
#include "User.h"
#include "YtriaTaskData.h"
#include "IRequestLogger.h"
#include "IPageRequestLogger.h"
#include "MultiObjectsRequestLogger.h"

using namespace Util;

class IPropertySetBuilder;
class MSGraphSession;
class ExchangeOnlineSession;
class GraphCache;

class BusinessObjectManager
	: public std::enable_shared_from_this<BusinessObjectManager>
{
public:
    BusinessObjectManager(std::shared_ptr<Sapio365Session> p_O365graphSession);

    // dtor declaration required for std::unique_ptr<GroupPropertyLists> to compile below. 
    // https://www.fluentcpp.com/2017/09/22/make-pimpl-using-unique_ptr/
    ~BusinessObjectManager();

	GraphCache& GetGraphCache() const;

    using row_primary_key_t = vector<GridBackendField>;

    // Every vector element contains the request made by a single object
	template<class T>
	vector<O365UpdateOperation> WriteSingleRequest(const vector<T>& p_BusinessObjects, const vector<row_primary_key_t>& p_Keys, YtriaTaskData p_TaskData);
    template<class T>
    vector<O365UpdateOperation> WriteMultiRequests(const vector<T>& p_BusinessObjects, const vector<row_primary_key_t>& p_Keys, YtriaTaskData p_TaskData);

    TaskWrapper<vector<BusinessAttachment>> GetUserEventAttachments(const PooledString& p_UserId, const PooledString& p_EventId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<vector<BusinessAttachment>> GetGroupEventAttachments(const PooledString& p_GroupId, const PooledString& p_EventId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<vector<BusinessAttachment>> GetMessageAttachments(const PooledString& p_UserId, const PooledString& p_MessageId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task);
	TaskWrapper<vector<BusinessAttachment>> GetPostAttachments(const PooledString& p_GroupId, const PooledString& p_ConversationId, const PooledString& p_ThreadId, const PooledString& p_PostId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task);

    TaskWrapper<BusinessItemAttachment>     GetMessageItemAttachment(const PooledString& p_UserId, const PooledString& p_MessageId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<BusinessItemAttachment>     GetUserEventItemAttachment(const PooledString& p_UserId, const PooledString& p_EventId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<BusinessItemAttachment>     GetGroupEventItemAttachment(const PooledString& p_GroupId, const PooledString& p_EventId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<BusinessItemAttachment>     GetGroupPostItemAttachment(const PooledString& p_GroupId, const PooledString& p_ConversationId, const PooledString& p_ThreadId, const PooledString& p_PostId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task);

    // Deletion methods
    TaskWrapper<HttpResultWithError>      DeleteUserEventAttachment(const PooledString& p_UserId, const PooledString& p_EventId, const PooledString& p_AttachmentId, YtriaTaskData p_Task);
    TaskWrapper<HttpResultWithError>      DeleteGroupEventAttachment(const PooledString& p_GroupId, const PooledString& p_EventId, const PooledString& p_AttachmentId, YtriaTaskData p_Task);
    TaskWrapper<HttpResultWithError>      DeleteUserMessageAttachment(const PooledString& p_UserId, const PooledString& p_MessageId, const PooledString& p_AttachmentId, YtriaTaskData p_Task);
	TaskWrapper<HttpResultWithError>      DeleteGroupPostAttachment(const PooledString& p_GroupId, const PooledString& p_ConversationId, const PooledString& p_ThreadId, const PooledString& p_PostId, const PooledString& p_AttachmentId, YtriaTaskData p_Task);

    // Directory Roles
    TaskWrapper<vector<BusinessDirectoryRole>>			GetDirectoryRoles(YtriaTaskData p_Task);
    TaskWrapper<vector<BusinessDirectoryRoleTemplate>>	GetDirectoryRoleTemplates(YtriaTaskData p_Task);
    TaskWrapper<vector<BusinessUser>>					GetDirectoryRoleMembers(const PooledString& p_DirectoryRoleId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData task);
	TaskWrapper<HttpResultWithError>						DeleteDirectoryRoleMember(const PooledString& p_UserId, const PooledString& p_DirectoryRoleId, YtriaTaskData p_TaskData);
	TaskWrapper<HttpResultWithError>						CreateDirectoryRoleFromTemplate(const PooledString& p_DirectoryRoleTemplateId, YtriaTaskData p_TaskData);
	TaskWrapper<HttpResultWithError>						DeleteDirectoryRole(const PooledString& p_DirectoryRoleId, YtriaTaskData p_TaskData);
	TaskWrapper<HttpResultWithError>						AddDirectoryRoleMember(const PooledString& p_UserId, const PooledString& p_DirectoryRoleId, YtriaTaskData p_TaskData);

	// Users
	TaskWrapper<vector<BusinessUser>>			GetBusinessUsersRecycleBin(const O365IdsContainer& p_IDs, const IPropertySetBuilder& p_PropertySet, YtriaTaskData p_TaskData);// empty = all
    TaskWrapper<vector<BusinessUser>>			GetBusinessUsersParentGroups(const O365IdsContainer& p_IDs, bool p_UseSQLCacheForUsers, YtriaTaskData p_TaskData);// all groups users are members of (if U is in G1 and G1 is in G2, returns G1 and G2)
#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
	TaskWrapper<vector<BusinessUser>>			GetBusinessUsersManagerHierarchy(const O365IdsContainer& p_IDs, YtriaTaskData p_TaskData);
#endif

	TaskWrapper<vector<BusinessEvent>>			GetUserBusinessEvents(const PooledString& p_UserId, const ModuleOptions& p_Options, map<BusinessUser, std::shared_ptr<EventListRequester>>& p_Requesters, const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger, YtriaTaskData p_Task);

    TaskWrapper<vector<BusinessUser>>			MoreInfoBusinessUsers(const vector<BusinessUser>& p_BusinessUsers, bool p_ReturnFullListOnCancel, YtriaTaskData p_Task);
	TaskWrapper<vector<BusinessUser>>			GetBusinessUsersIndividualRequests(const vector<BusinessUser>& p_BusinessUsers, YtriaTaskData p_Task);

    TaskWrapper<vector<BusinessLicenseDetail>>	GetUserBusinessLicenseDetails(const PooledString& p_UserId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData);

	// Contacts
	TaskWrapper<vector<BusinessContact>>			GetBusinessContacts(const PooledString& p_Id, const std::shared_ptr<IPageRequestLogger>& p_PageLogger, YtriaTaskData p_TaskData);
    TaskWrapper<vector<BusinessContact>>			GetBusinessContacts(const PooledString& p_Id, const PooledString& p_ContactFolderId, const std::shared_ptr<IPageRequestLogger>& p_PageLogger, YtriaTaskData p_TaskData);
    TaskWrapper<vector<BusinessContactFolder>>	GetBusinessContactFoldersHierarchy(const PooledString& p_UserId, const std::shared_ptr<IPageRequestLogger>& p_PageLogger, YtriaTaskData p_TaskData);

	// Messages
	TaskWrapper<vector<BusinessMessage>>		GetUserBusinessMessages(const PooledString& p_UserId, const ModuleOptions& p_Options, std::map<BusinessUser, std::shared_ptr<MessageListRequester>>& p_Requesters, const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
	TaskWrapper<vector<BusinessMessage>>		GetUserBusinessMessages(const PooledString& p_UserId, const O365IdsContainer& p_MessageIDs, const ModuleOptions& p_Options, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
    TaskWrapper<BusinessFileAttachment>		GetMessageFileAttachment(const PooledString& p_UserId, const PooledString& p_MessageId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
	TaskWrapper<wstring>						GetMessageMIMEContent(const wstring& p_UserId, const wstring& p_MessageId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData);

    // Events
    TaskWrapper<BusinessFileAttachment>     GetUserEventFileAttachment(const PooledString& p_UserId, const PooledString& p_EventId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
    TaskWrapper<BusinessFileAttachment>     GetGroupEventFileAttachment(const PooledString& p_UserId, const PooledString& p_EventId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
	
	// MailFolders
    TaskWrapper<BusinessMailFolder>         GetUserBusinessMailFolder(const PooledString& p_UserId, const PooledString& p_MailFolderId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task);
	
    // Groups
    TaskWrapper<vector<BusinessConversation>>	GetBusinessConversations(const PooledString& p_GroupId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
	TaskWrapper<vector<BusinessPost>>			GetBusinessPosts(const PooledString& p_GroupId, const PooledString& p_ConversationId, const PooledString& p_ThreadId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
	TaskWrapper<BusinessFileAttachment>         GetPostFileAttachment(const PooledString& p_GroupId, const PooledString& p_ConversationId, const PooledString& p_ThreadId, const PooledString& p_PostId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
	TaskWrapper<vector<BusinessGroup>>			GetBusinessGroupsRecycleBin(const IPropertySetBuilder& p_PropertySet, YtriaTaskData p_TaskData);
	TaskWrapper<vector<BusinessGroup>>			GetBusinessGroupMembersHierarchies(const O365IdsContainer& p_IDs, bool p_UseSQLCacheForTopLevel, YtriaTaskData p_TaskData, bool p_AlsoRequestOwners);// empty = all
	TaskWrapper<int32_t>						CountGroupMembers(const PooledString& p_ID, std::shared_ptr<IPageRequestLogger> p_Logger, YtriaTaskData p_Task);
	TaskWrapper<vector<BusinessGroup>>			GetBusinessGroupOwnersHierarchies(const O365IdsContainer& p_IDs, bool p_UseSQLCacheForTopLevel, YtriaTaskData p_TaskData, bool p_OnlyFirstLevel);// empty = all
	//void										GetBusinessGroupOwnersHierarchies(vector<BusinessGroup>& p_Groups, YtriaTaskData p_TaskData, bool p_OnlyFirstLevel);// empty = all
	void										GetBusinessGroupOwnersHierarchy(BusinessGroup& p_Group, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData, bool p_OnlyFirstLevel);
    TaskWrapper<vector<BusinessGroup>>			GetBusinessGroupAuthorsHierarchies(const O365IdsContainer& p_IDs, bool p_UseSQLCacheForTopLevel, YtriaTaskData p_TaskData);// empty = all
    TaskWrapper<vector<BusinessEvent>>			GetGroupBusinessEvents(const PooledString& p_GroupId, const ModuleOptions& p_Options, map<BusinessGroup, std::shared_ptr<EventListRequester>>& p_Requesters, const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger, YtriaTaskData p_Task);
	TaskWrapper<BusinessLifeCyclePolicies>		GetBusinessLCP(const PooledString& p_GroupId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<vector<BusinessChannel>>         GetBusinessChannels(const PooledString& p_GroupId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
    TaskWrapper<vector<BusinessGroupSettingTemplate>> GetBusinessGroupSettingTemplates(YtriaTaskData p_TaskData);
    TaskWrapper<vector<BusinessGroupSetting>>         GetBusinessGroupSettings(YtriaTaskData p_TaskData);
    TaskWrapper<HttpResultWithError>             UpdateGroupSetting(const BusinessGroupSetting& p_GroupSetting, YtriaTaskData p_TaskData);
    TaskWrapper<HttpResultWithError>             UpdateGroupSetting(const PooledString& p_GroupId, const BusinessGroupSetting& p_GroupSetting, YtriaTaskData p_TaskData);
    TaskWrapper<HttpResultWithError>             CreateGroupSetting(const BusinessGroupSetting& p_GroupSetting, YtriaTaskData p_TaskData);
    TaskWrapper<HttpResultWithError>             CreateGroupSetting(const PooledString& p_GroupId, const BusinessGroupSetting& p_GroupSetting, YtriaTaskData p_TaskData);
    TaskWrapper<HttpResultWithError>             DeleteGroupSetting(const PooledString& p_GroupSettingId, YtriaTaskData p_TaskData);
    TaskWrapper<HttpResultWithError>             DeleteGroupSetting(const PooledString& p_GroupSettingId, const PooledString& p_GroupId, YtriaTaskData p_TaskData);

	TaskWrapper<vector<BusinessGroup>>			MoreInfoBusinessGroups(const vector<BusinessGroup>& p_BusinessGroups, bool p_ReturnFullListOnCancel, YtriaTaskData p_Task, boost::YOpt<BusinessLifeCyclePolicies> p_LCP);
	TaskWrapper<vector<BusinessGroup>>			MoreInfoDeletedBusinessGroups(const vector<BusinessGroup>& p_DeletedBusinessGroups, bool p_ReturnFullListOnCancel, YtriaTaskData p_Task);
	TaskWrapper<vector<BusinessGroup>>			GetBusinessGroupsIndividualRequests(const vector<BusinessGroup>& p_BusinessGroups, YtriaTaskData p_Task);


    // Sharepoint
    TaskWrapper<vector<BusinessSite>>       GetAllBusinessSites(bool p_FromCache, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<BusinessSite>               GetRootBusinessSite(const PooledString& p_GroupId, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<vector<BusinessList>>       GetBusinessLists(const PooledString& p_SiteId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<BusinessList>               GetBusinessList(const PooledString& p_SiteId, const PooledString& p_ListId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<BusinessList>               GetBusinessList(const PooledString& p_SiteId, const PooledString& p_ListId, const vector<wstring>& p_FieldsToRetrieve, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<vector<BusinessListItem>>   GetBusinessListItems(const PooledString& p_SiteId, const PooledString& p_ListId, const vector<wstring>& p_FieldsToRetrieve, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<BusinessDrive>              GetDefaultSiteDrive(const PooledString& p_SiteId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<vector<BusinessSite>>       GetSubSites(const PooledString& p_SiteId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task);

    // Drive Items
    TaskWrapper<BusinessDrive>					GetBusinessUserDrive(const PooledString& p_UserId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<BusinessDrive>					GetBusinessGroupDrive(const PooledString& p_GroupId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<vector<BusinessPermission>>		GetBusinessPermissions(const PooledString& p_DriveId, const PooledString& p_DriveItemId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task);
	TaskWrapper<BusinessDriveItemFolder>		GetBusinessDriveItems(const PooledString& driveId, const boost::YOpt<wstring>& p_SearchCriteria, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task);
    TaskWrapper<HttpResultWithError>            UpdatePermission(const PooledString& p_DriveId, const PooledString& p_ItemId, const Permission& p_Permission, const vector<rttr::property>& p_Properties, YtriaTaskData p_Task);
    TaskWrapper<HttpResultWithError>            DeletePermission(const PooledString& p_DriveId, const PooledString& p_ItemId, const PooledString& p_PermissionId, YtriaTaskData p_Task);
	TaskWrapper<HttpResultWithError>			GetDriveItemContent(const wstring& p_DriveId, const wstring& p_DriveItem, YtriaTaskData p_TaskData);

    // Others	
	TaskWrapper<vector<BusinessSubscribedSku>>	GetSubscribedSkus(YtriaTaskData p_Task);

	
    std::shared_ptr<MSGraphSession> GetMSGraphSession(Sapio365Session::SessionType p_RbacSessionMode) const;
    std::shared_ptr<ExchangeOnlineSession> GetExchangeSession() const;
    std::shared_ptr<SharepointOnlineSession> GetSharepointSession() const;
    std::shared_ptr<Sapio365Session> GetSapio365Session() const;

private:
    void getBusinessGroupMembersHierarchy(BusinessGroup& p_Group, std::map<PooledString, BusinessGroup>& p_GroupsCache, const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
    void getBusinessGroupAuthorsAcceptedHierarchy(BusinessGroup& p_Group, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
    void getBusinessGroupAuthorsRejectedHierarchy(BusinessGroup& p_Group, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData);

public: // FIXME: Only public for MacroRequest
	enum UserAdditionalRequest
	{
		RequestMailboxSettingsForNonGuest = 0x1,
		RequestDriveForNonGuest = 0x2,
		RequestManager = 0x4,
	};

private:
	TaskWrapper<vector<BusinessUser>> getBusinessUsersIndividualRequests_impl(const vector<BusinessUser>& p_BusinessUsers, uint32_t p_UserAdditionnalInfoFlags, bool p_ReturnFullListOnCancel, YtriaTaskData p_Task);

public: // FIXME: Only public for MacroRequest
	enum GroupAdditionalRequest
	{
		RequestOnBehalf = 0x1,
		RequestTeam = 0x2,
		RequestGroupSettings = 0x4,
		RequestGroupLCP = 0x8,
		RequestOwners = 0x10,
		RequestMembersCount = 0x20,
		RequestDrive = 0x40,
        RequestIsYammer = 0x80,
	};

private:
	TaskWrapper<vector<BusinessGroup>> getBusinessGroupsIndividualRequests_impl(const vector<BusinessGroup>& p_BusinessGroups, bool p_UseSyncProperties, boost::YOpt<BusinessLifeCyclePolicies> p_LCP, uint32_t p_GroupAdditionnalInfoFlags, bool p_ReturnFullListOnCancel, YtriaTaskData p_Task);

    vector<Group> toJsonGroups(const vector<CachedGroup>& p_CachedGroups) const;

    vector<BusinessGroup> removeDuplicates(const vector<BusinessGroup> groups) const;

private:
	std::shared_ptr<Sapio365Session> m_Sapio365Session;

    struct GroupPropertyLists;
    std::unique_ptr<GroupPropertyLists> m_PropertyLists;
};

#include "BusinessObjectManager.hpp"