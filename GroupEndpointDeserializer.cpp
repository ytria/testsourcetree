#include "GroupEndpointDeserializer.h"

#include "JsonSerializeUtil.h"
#include "MsGraphFieldNames.h"

void GroupEndpointDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	JsonSerializeUtil::DeserializeString(_YTEXT("deletedDateTime"), m_Data.m_DeletedDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("capability"), m_Data.m_Capability, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("providerId"), m_Data.m_ProviderId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("providerName"), m_Data.m_ProviderName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("uri"), m_Data.m_Uri, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("providerResourceId"), m_Data.m_ProviderResourceId, p_Object);
}
