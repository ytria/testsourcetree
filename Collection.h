#pragma once

#include "BaseObj.h"
#include "IndexingPolicy.h"

namespace Cosmos
{
    class Collection : public Cosmos::BaseObj
    {
    public:
        boost::YOpt<PooledString> _doc;
        boost::YOpt<PooledString> _sprocs;
        boost::YOpt<PooledString> _triggers;
        boost::YOpt<PooledString> _udfs;
        boost::YOpt<PooledString> _conflicts;
        boost::YOpt<Cosmos::IndexingPolicy> IndexingPolicy;
        boost::YOpt<Cosmos::PartitionKey> PartitionKey;
    };
}

