#include "ProvisionedPlanSerializer.h"

#include "JsonSerializeUtil.h"

ProvisionedPlanSerializer::ProvisionedPlanSerializer(const ProvisionedPlan& p_ProvisionedPlan)
    : m_ProvisionedPlan(p_ProvisionedPlan)
{
}

void ProvisionedPlanSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("capabilityStatus"), m_ProvisionedPlan.m_CapabilityStatus, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("provisioningStatus"), m_ProvisionedPlan.m_ProvisioningStatus, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("service"), m_ProvisionedPlan.m_Service, obj);
}
