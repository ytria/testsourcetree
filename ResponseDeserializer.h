#pragma once

#include "ExchangeObjectDeserializer.h"

namespace Ex
{
class ResponseDeserializer : public ExchangeObjectDeserializer
{
public:
    ResponseDeserializer(const wstring& p_ResponseNode);
    void DeserializeObject(const XERCES_CPP_NAMESPACE::DOMElement& p_Envelope) override;

    void GetErrorInfo(XERCES_CPP_NAMESPACE::DOMElement* responseElement);
    bool HasError() const;

private:
    const wstring m_ResponseNode;

    wstring m_ErrorCode;
    wstring m_ErrorMessage;
    bool m_HasError;
};
}

