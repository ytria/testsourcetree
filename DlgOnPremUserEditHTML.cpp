#include "DlgOnPremUserEditHTML.h"

#include "OnPremiseUsersColumns.h"

DlgOnPremUserEditHTML::DlgOnPremUserEditHTML(vector<OnPremiseUser>& p_Users, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
	: DlgFormsHTML(p_Action, p_Parent, _YTEXT(""))
	, m_Users(p_Users)
{
}

bool DlgOnPremUserEditHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	std::map<wstring, wstring> alreadyProcessedProperties;

	for (const auto& item : data)
	{
		const auto& propName = item.first;
		ASSERT(!propName.empty());
		const auto& propValue = item.second;

		{
			auto itt = m_StringSetters.find(propName);
			if (m_StringSetters.end() != itt)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties[propName] = propValue;

				for (auto& businessUser : m_Users)
					itt->second(businessUser, boost::YOpt<PooledString>(propValue));

				continue;
			}
		}

		{
			auto it = m_BoolSetters.find(propName);
			if (m_BoolSetters.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties[propName] = propValue;

				for (auto& businessUser : m_Users)
					it->second(businessUser, boost::YOpt<bool>(Str::getBoolFromString(propValue)));

				continue;
			}
		}

		{
			auto it = m_DateSetters.find(propName);
			if (m_DateSetters.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties[propName] = propValue;

				auto date = DateTimeFromText(propValue);
				if (date)
				{
					for (auto& businessUser : m_Users)
						it->second(businessUser, *date);
				}

				continue;
			}
		}

		if (alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName))
		{
			auto it = m_VectorStringsSetters.find(propName);
			if (m_VectorStringsSetters.end() != it)
			{
				vector<PooledString> value;
				auto itts = data.equal_range(propName);
				for (auto itt = itts.first; itt != itts.second; ++itt)
				{
					if (!itt->second.empty())
						value.push_back(itt->second);
				}

				alreadyProcessedProperties[propName] = propValue;

				if (value.empty())
					value.push_back(GridBackendUtil::g_NoValueString);

				for (auto& businessUser : m_Users)
					it->second(businessUser, value);

				continue;
			}
		}
	}

	return true;
}

void DlgOnPremUserEditHTML::addEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, const vector<wstring>& p_ApplicableUserTypes)
{
	addObjectStringEditor<OnPremiseUser>(m_Users
		, p_Getter
		, p_PropName
		, OnPremiseUsersColumns::GetTitle(p_PropName)
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, [p_Flags, &p_ApplicableUserTypes](const OnPremiseUser& bu)
		{
			return 0;
		}
	);
	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

void DlgOnPremUserEditHTML::addEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides /*= BoolEditor::CheckUncheckedLabelOverrides()*/)
{
	addObjectBoolToggleEditor<OnPremiseUser>(m_Users
		, p_Getter
		, p_PropName
		, OnPremiseUsersColumns::GetTitle(p_PropName)
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, [p_Flags, &p_ApplicableUserTypes](const OnPremiseUser& bu)
		{
			return 0;
		}
		, p_CheckUncheckedLabelOverrides
			);
	ASSERT(!hasProperty(p_PropName));
	m_BoolSetters[p_PropName] = p_Setter;
}

void DlgOnPremUserEditHTML::addEditor(DateGetter p_Getter, DateSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, bool p_WithTime, const boost::YOpt<wstring>& p_MaxDate/* = boost::none*/, const boost::YOpt<std::pair<wstring, wstring>>& p_MinMaxDate/* = boost::none*/)
{
	addObjectDateEditor<OnPremiseUser>(m_Users
		, p_Getter
		, p_PropName
		// FIXME: Hack
		, p_PropName == _YUID("onPremAccountExpires") ? _YFORMAT(L"%s (UTC)", OnPremiseUsersColumns::GetTitle(p_PropName).c_str()) : OnPremiseUsersColumns::GetTitle(p_PropName)
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, [p_Flags, &p_ApplicableUserTypes](const OnPremiseUser& bu)
		{
			return 0;
		}
		, p_WithTime
		, p_MaxDate
		, p_MinMaxDate
			);
	ASSERT(!hasProperty(p_PropName));
	m_DateSetters[p_PropName] = p_Setter;
}

void DlgOnPremUserEditHTML::addEditor(VectorStringGetter p_Getter, VectorStringSetter p_Setter, const wstring& p_PropName, uint32_t p_Flags, const vector<wstring>& p_ApplicableUserTypes)
{
	addObjectMultiValueEditor<OnPremiseUser>(m_Users,
		p_Getter,
		p_PropName,
		OnPremiseUsersColumns::GetTitle(p_PropName),
		p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE),
		[](const OnPremiseUser& bu) {
			return 0;
		}
	);
	ASSERT(!hasProperty(p_PropName));
	m_VectorStringsSetters[p_PropName] = p_Setter;
}

void DlgOnPremUserEditHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgOnPremUserEditHTML"), false, true, true);

	getGenerator().addCategory(_T("Info"), CategoryFlags::EXPAND_BY_DEFAULT);
	{
		addEditor(&OnPremiseUser::GetAccountExpires						, &OnPremiseUser::SetAccountExpires						, _YUID("onPremAccountExpires"), 0, {}, true);
		addEditor(&OnPremiseUser::GetAccountNotDelegated				, &OnPremiseUser::SetAccountNotDelegated				, _YUID("onPremAccountNotDelegated"), 0, {});
		addEditor(&OnPremiseUser::GetAllowReversiblePasswordEncryption	, &OnPremiseUser::SetAllowReversiblePasswordEncryption	, _YUID("onPremAllowReversiblePasswordEncryption"), 0, {});
		addEditor(&OnPremiseUser::GetCannotChangePassword				, &OnPremiseUser::SetCannotChangePassword				, _YUID("onPremCannotChangePassword"), 0, {});
		addEditor(&OnPremiseUser::GetCity								, &OnPremiseUser::SetCity								, _YUID("onPremCity"), 0, {});
		addEditor(&OnPremiseUser::GetCompany							, &OnPremiseUser::SetCompany							, _YUID("onPremCompany"), 0, {});
		//addEditor(&OnPremiseUser::GetCompoundIdentitySupported		, &OnPremiseUser::SetCompoundIdentitySupported			, _YUID("onPremCompoundIdentitySupported"), 0, {});
		addEditor(&OnPremiseUser::GetCountry							, &OnPremiseUser::SetCountry							, _YUID("onPremCountry"), 0, {});
		addEditor(&OnPremiseUser::GetDepartment							, &OnPremiseUser::SetDepartment							, _YUID("onPremDepartment"), 0, {});
		addEditor(&OnPremiseUser::GetDescription						, &OnPremiseUser::SetDescription						, _YUID("onPremDescription"), 0, {});
		addEditor(&OnPremiseUser::GetDisplayName						, &OnPremiseUser::SetDisplayName						, _YUID("onPremDisplayName"), 0, {});
		addEditor(&OnPremiseUser::GetDivision							, &OnPremiseUser::SetDivision							, _YUID("onPremDivision"), 0, {});
		addEditor(&OnPremiseUser::GetEmailAddress						, &OnPremiseUser::SetEmailAddress						, _YUID("onPremEmailAddress"), 0, {});
		addEditor(&OnPremiseUser::GetEmployeeID							, &OnPremiseUser::SetEmployeeID							, _YUID("onPremEmployeeID"), 0, {});
		addEditor(&OnPremiseUser::GetEmployeeNumber						, &OnPremiseUser::SetEmployeeNumber						, _YUID("onPremEmployeeNumber"), 0, {});
		addEditor(&OnPremiseUser::GetEnabled							, &OnPremiseUser::SetEnabled							, _YUID("onPremEnabled"), 0, {});
		addEditor(&OnPremiseUser::GetFax								, &OnPremiseUser::SetFax								, _YUID("onPremFax"), 0, {});
		addEditor(&OnPremiseUser::GetGivenName							, &OnPremiseUser::SetGivenName							, _YUID("onPremGivenName"), 0, {});
		addEditor(&OnPremiseUser::GetHomeDirectory						, &OnPremiseUser::SetHomeDirectory						, _YUID("onPremHomeDirectory"), 0, {});
		addEditor(&OnPremiseUser::GetHomeDrive							, &OnPremiseUser::SetHomeDrive							, _YUID("onPremHomeDrive"), 0, {});
		addEditor(&OnPremiseUser::GetHomePage							, &OnPremiseUser::SetHomePage							, _YUID("onPremHomePage"), 0, {});
		addEditor(&OnPremiseUser::GetHomePhone							, &OnPremiseUser::SetHomePhone							, _YUID("onPremHomePhone"), 0, {});
		addEditor(&OnPremiseUser::GetInitials							, &OnPremiseUser::SetInitials							, _YUID("onPremInitials"), 0, {});
		//addEditor(&OnPremiseUser::GetKerberosEncryptionType			, &OnPremiseUser::SetKerberosEncryptionType				, _YUID("onPremKerberosEncryptionType"), 0, {});
		addEditor(&OnPremiseUser::GetLogonWorkstations					, &OnPremiseUser::SetLogonWorkstations					, _YUID("onPremLogonWorkstations"), 0, {});
		addEditor(&OnPremiseUser::GetManager							, &OnPremiseUser::SetManager							, _YUID("onPremManager"), 0, {});
		addEditor(&OnPremiseUser::GetMobilePhone						, &OnPremiseUser::SetMobilePhone						, _YUID("onPremMobilePhone"), 0, {});
		addEditor(&OnPremiseUser::GetOffice								, &OnPremiseUser::SetOffice								, _YUID("onPremOffice"), 0, {});
		addEditor(&OnPremiseUser::GetOfficePhone						, &OnPremiseUser::SetOfficePhone						, _YUID("onPremOfficePhone"), 0, {});
		addEditor(&OnPremiseUser::GetOrganization						, &OnPremiseUser::SetOrganization						, _YUID("onPremOrganization"), 0, {});
		addEditor(&OnPremiseUser::GetOtherName							, &OnPremiseUser::SetOtherName							, _YUID("onPremOtherName"), 0, {});
		addEditor(&OnPremiseUser::GetPasswordNeverExpires				, &OnPremiseUser::SetPasswordNeverExpires				, _YUID("onPremPasswordNeverExpires"), 0, {});
		addEditor(&OnPremiseUser::GetPasswordNotRequired				, &OnPremiseUser::SetPasswordNotRequired				, _YUID("onPremPasswordNotRequired"), 0, {});
		addEditor(&OnPremiseUser::GetPOBox								, &OnPremiseUser::SetPOBox								, _YUID("onPremPOBox"), 0, {});
		addEditor(&OnPremiseUser::GetPostalCode							, &OnPremiseUser::SetPostalCode							, _YUID("onPremPostalCode"), 0, {});
		addEditor(&OnPremiseUser::GetProfilePath						, &OnPremiseUser::SetProfilePath						, _YUID("onPremProfilePath"), 0, {});
		addEditor(&OnPremiseUser::GetProxyAddresses						, &OnPremiseUser::SetProxyAddresses						, _YUID("onPremProxyAddresses"), 0, {});
		addEditor(&OnPremiseUser::GetScriptPath							, &OnPremiseUser::SetScriptPath							, _YUID("onPremScriptPath"), 0, {});
		addEditor(&OnPremiseUser::GetSmartcardLogonRequired				, &OnPremiseUser::SetSmartcardLogonRequired				, _YUID("onPremSmartcardLogonRequired"), 0, {});
		addEditor(&OnPremiseUser::GetState								, &OnPremiseUser::SetState								, _YUID("onPremState"), 0, {});
		addEditor(&OnPremiseUser::GetStreetAddress						, &OnPremiseUser::SetStreetAddress						, _YUID("onPremStreetAddress"), 0, {});
		addEditor(&OnPremiseUser::GetSurname							, &OnPremiseUser::SetSurname							, _YUID("onPremSurname"), 0, {});
		addEditor(&OnPremiseUser::GetTitle								, &OnPremiseUser::SetTitle								, _YUID("onPremTitle"), 0, {});
		addEditor(&OnPremiseUser::GetTrustedForDelegation				, &OnPremiseUser::SetTrustedForDelegation				, _YUID("onPremTrustedForDelegation"), 0, {});
		addEditor(&OnPremiseUser::GetUserPrincipalName					, &OnPremiseUser::SetUserPrincipalName					, _YUID("onPremUserPrincipalName"), 0, {});
	}

	getGenerator().addApplyButton(isEditDialog() ? LocalizedStrings::g_ApplyButtonText : LocalizedStrings::g_CreateButtonText);
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
	getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);
}

bool DlgOnPremUserEditHTML::hasProperty(const wstring& p_PropertyName) const
{
	return m_StringSetters.end() != m_StringSetters.find(p_PropertyName);
}
