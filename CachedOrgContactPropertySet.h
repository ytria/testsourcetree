#pragma once

#include "IPropertySetBuilder.h"

class CachedOrgContactPropertySet : public IPropertySetBuilder
{
public:
	virtual vector<rttr::property> GetPropertySet() const override;
};
