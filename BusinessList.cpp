#include "BusinessList.h"
#include "BusinessColumnDefinition.h"

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessList>("List") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("List"))))
		.constructor()(policy::ctor::as_object)
		.property("Id", &BusinessList::m_Id)
		.property("DisplayName", &BusinessList::m_DisplayName)
		;
}

BusinessList::BusinessList(const List& p_JsonList)
{
    m_Id = p_JsonList.Id;
    wstring displayName = p_JsonList.DisplayName ? *p_JsonList.DisplayName : wstring(_YTEXT(""));
    SetName(p_JsonList.Name);
    SetCreatedBy(p_JsonList.CreatedBy);
    SetCreatedDateTime(p_JsonList.CreatedDateTime);
    SetDescription(p_JsonList.Description);
    SetLastModifiedBy(p_JsonList.LastModifiedBy);
    SetLastModifiedDateTime(p_JsonList.LastModifiedDateTime);
    SetParentReference(p_JsonList.ParentReference);
    SetWebUrl(p_JsonList.WebUrl);
    SetContentTypes(p_JsonList.ContentTypes);
    SetDisplayName(p_JsonList.DisplayName);
    SetListInfo(p_JsonList.ListInfo);
    SetEtag(p_JsonList.Etag);
    SetSystem(p_JsonList.System);

    vector<BusinessListItem> businessListItems;
    for (const auto& item : p_JsonList.Items)
        businessListItems.emplace_back(item);
    SetListItems(businessListItems);

    vector<BusinessColumnDefinition> businessColumns;
    for (const auto& column : p_JsonList.Columns)
        businessColumns.emplace_back(column);
    SetColumns(businessColumns);
}

boost::YOpt<PooledString> BusinessList::GetName() const
{
    return m_Name;
}

void BusinessList::SetName(const boost::YOpt<PooledString>& p_Val)
{
    m_Name = p_Val;
}

const boost::YOpt<PooledString>& BusinessList::GetDisplayName() const
{
	return m_DisplayName;
}

void BusinessList::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}

const boost::YOpt<IdentitySet>& BusinessList::GetCreatedBy() const
{
    return m_CreatedBy;
}

void BusinessList::SetCreatedBy(const boost::YOpt<IdentitySet>& p_Val)
{
    m_CreatedBy = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessList::GetCreatedDateTime() const
{
    return m_CreatedDateTime;
}

void BusinessList::SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_CreatedDateTime = p_Val;
}

const boost::YOpt<PooledString>& BusinessList::GetDescription() const
{
    return m_Description;
}

void BusinessList::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
    m_Description = p_Val;
}

const boost::YOpt<IdentitySet>& BusinessList::GetLastModifiedBy() const
{
    return m_LastModifiedBy;
}

void BusinessList::SetLastModifiedBy(const boost::YOpt<IdentitySet>& p_Val)
{
    m_LastModifiedBy = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessList::GetLastModifiedDateTime() const
{
    return m_LastModifiedDateTime;
}

void BusinessList::SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_LastModifiedDateTime = p_Val;
}

const boost::YOpt<SystemFacet>& BusinessList::GetSystem() const
{
    return m_System;
}

void BusinessList::SetSystem(const boost::YOpt<SystemFacet>& p_Val)
{
    m_System = p_Val;
}

const vector<BusinessListItem>& BusinessList::GetListItems() const
{
    return m_ListItems;
}

void BusinessList::SetListItems(const vector<BusinessListItem>& p_Val)
{
    m_ListItems = p_Val;
}

const boost::YOpt<PooledString>& BusinessList::GetEtag() const
{
    return m_ETag;
}

void BusinessList::SetEtag(const boost::YOpt<PooledString>& p_Val)
{
    m_ETag = p_Val;
}

const boost::YOpt<ItemReference>& BusinessList::GetParentReference() const
{
    return m_ParentReference;
}

void BusinessList::SetParentReference(const boost::YOpt<ItemReference>& p_Val)
{
    m_ParentReference = p_Val;
}

const boost::YOpt<ListInfo>& BusinessList::GetListInfo() const
{
    return m_ListInfo;
}

void BusinessList::SetListInfo(const boost::YOpt<ListInfo>& p_Val)
{
    m_ListInfo = p_Val;
}

const boost::YOpt<PooledString>& BusinessList::GetWebUrl() const
{
    return m_WebUrl;
}

void BusinessList::SetWebUrl(const boost::YOpt<PooledString>& p_Val)
{
    m_WebUrl = p_Val;
}

const vector<BusinessColumnDefinition>& BusinessList::GetColumns() const
{
    return m_Columns;
}

void BusinessList::SetColumns(const vector<BusinessColumnDefinition>& p_Val)
{
    m_Columns = p_Val;
}

const vector<ContentType>& BusinessList::GetContentTypes() const
{
    return m_ContentTypes;
}

void BusinessList::SetContentTypes(const vector<ContentType>& p_Val)
{
    m_ContentTypes = p_Val;
}
