#pragma once

#include "ModuleBase.h"

class FrameSignIns;

class ModuleSignIns : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	void executeImpl(const Command& p_Command) override;
	void doRefresh(FrameSignIns* p_pFrame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command);
	void showSignIns(Command p_Command);
	void loadSnapshot(const Command& p_Command);
};

