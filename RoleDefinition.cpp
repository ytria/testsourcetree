#include "RoleDefinition.h"

RTTR_REGISTRATION
{
    using namespace rttr;
registration::class_<Sp::RoleDefinition>("RoleDefinition") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Role Definition"))))
.constructor()(policy::ctor::as_object);
}

Sp::RoleDefinition::RoleDefinition(int32_t p_Id)
    :Id(p_Id)
{
}
