#pragma once

#include "ISubItemModification.h"
#include "SubElementPrefixFormatter.h"
#include "SubItemFieldValue.h"

class SubItemFieldUpdate : public ISubItemModification
{
public:
    SubItemFieldUpdate(const SubItemFieldValue& p_OldValue, const SubItemFieldValue& p_NewValue, UINT p_ColumnKey,
						O365Grid& p_Grid, const SubItemKey& p_Key, GridBackendColumn* p_ColSubItemElder, GridBackendColumn* p_ColSubItemPrimaryKey, const wstring& p_ObjectName);

    virtual void RevertSpecific(const SubItemInfo& p_Info) override;
    virtual bool Refresh(GridBackendRow* p_Row, const std::map<SubItemKey, HttpResultWithError>& p_Errors) override;

    void ShowAppliedLocally(GridBackendRow* p_Row) const override;

    UINT GetColumnKey() const;

	const SubItemFieldValue& GetNewValue() const;
	const SubItemFieldValue& GetActualValue() const;
	const SubItemFieldValue& GetOldValue() const;

	virtual vector<ModificationLog> GetModificationLogs() const override;

    static std::unique_ptr<SubItemFieldUpdate> CreateReplacement(SubItemFieldUpdate& p_ToBeReplaced, std::unique_ptr<SubItemFieldUpdate> p_Replacement);

private:
    void addFormatterForCollapsedRow(GridBackendRow* p_Row) const;
    void addFormatterForExpandedRow(GridBackendRow* p_Row) const;
    void createOrUpdateFormatter(GridBackendRow* p_Row, size_t p_Index) const;

    std::unique_ptr<SubElementPrefixFormatter> createPrefixFormatter() const;

    SubItemFieldValue m_ActualValue;
    SubItemFieldValue m_OldValue;
    SubItemFieldValue m_NewValue;

    UINT m_ColumnKey;

    static const size_t g_FormatterId = 0;

    friend class RowSubItemFieldUpdates; // Access to Refresh()
};

