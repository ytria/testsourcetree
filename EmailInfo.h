#pragma once

struct EmailInfo
{
    wstring m_Subject;
    wstring m_Content;
    wstring m_TenantName;
    vector<PooledString> m_Recipients;
};
