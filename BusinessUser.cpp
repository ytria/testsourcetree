#include "BusinessUser.h"

#include "BusinessGroup.h"
#include "BusinessUserConfiguration.h"
#include "CachedUser.h"
#include "Languages.h"
#include "MsGraphFieldNames.h"
#include "MSGraphSession.h"
#include "O365AdminUtil.h"
#include "RevokeUserAccessRequester.h"
#include "safeTaskCall.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "TimeUtil.h"
#include "YtriaFieldsConstants.h"
#include "MsGraphHttpRequestLogger.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessUser>("User") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("User"))))
		.constructor()(policy::ctor::as_object)
// *** Main Info
		.property(O365_USER_DISPLAYNAME, &BusinessUser::m_DisplayName)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_USERPRINCIPALNAME, &BusinessUser::m_UserPrincipalName)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_USERTYPE, &BusinessUser::m_UserType)(
			metadata(MetadataKeys::ValidForCreation, true),
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_ACCOUNTENABLED, &BusinessUser::m_AccountEnabled)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_GIVENNAME, &BusinessUser::m_GivenName)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_SURNAME, &BusinessUser::m_Surname)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_PREFERREDLANGUAGE, &BusinessUser::m_PreferredLanguage)( // LIST OF VALUES - Should follow ISO 639-1 Code: for example "en-US"
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_USAGELOCATION, &BusinessUser::m_UsageLocation)( // LIST OF VALUES - A two letter country code (ISO standard 3166), not nullable
			metadata(MetadataKeys::ValidForCreation, true)
			)

// **** Mail Settings
		.property(O365_USER_MAIL, &BusinessUser::m_Mail)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_MAILNICKNAME, &BusinessUser::m_MailNickname)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_PROXYADDRESSES, &BusinessUser::m_ProxyAddresses)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_IMADDRESSES, &BusinessUser::m_ImAddresses)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_MAILBOXSETTINGS, &BusinessUser::m_MailboxSettings)(
			metadata(MetadataKeys::ReadOnly, true)
			)

// **** Job Info
		.property(O365_USER_JOBTITLE, &BusinessUser::m_JobTitle)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_COMPANYNAME, &BusinessUser::m_CompanyName)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_DEPARTMENT, &BusinessUser::m_Department)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_BUSINESSPHONES, &BusinessUser::m_BusinessPhones)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_MOBILEPHONE, &BusinessUser::m_MobilePhone)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_OFFICELOCATION, &BusinessUser::m_OfficeLocation)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_STREETADDRESS, &BusinessUser::m_StreetAddress)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_CITY, &BusinessUser::m_City)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_STATE, &BusinessUser::m_State)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_POSTALCODE, &BusinessUser::m_PostalCode)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_COUNTRY, &BusinessUser::m_Country)( // 2 Letters code ????
			metadata(MetadataKeys::ValidForCreation, true)
			)

// **** More Info
		.property(O365_USER_MYSITE, &BusinessUser::m_MySite)
		.property(O365_USER_PASSWORDPOLICIES, &BusinessUser::m_PasswordPolicies)( // 4 enum: None, DisablePasswordExpiration, DisableStrongPassword, or both DisablePasswordExpiration & DisableStrongPassword (do not remove the space after the comma!) 
			metadata(MetadataKeys::ValidForCreation, true)
			)
		// This property is split below.
		//.property(O365_USER_PASSWORDPROFILE, &BusinessUser::m_PasswordProfile)
		.property(O365_USER_PASSWORDPROFILE_PASSWORD, &BusinessUser::m_Password)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_PASSWORDPROFILE_FORCECHANGE, &BusinessUser::m_ForceChangePassword)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA, &BusinessUser::m_ForceChangePasswordWithMfa)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_ABOUTME, &BusinessUser::m_AboutMe)
		.property(O365_USER_BIRTHDAY, &BusinessUser::m_Birthday)
		.property(O365_USER_PASTPROJECTS, &BusinessUser::m_PastProjects)
		.property(O365_USER_RESPONSIBILITIES, &BusinessUser::m_Responsibilities)
		.property(O365_USER_SKILLS, &BusinessUser::m_Skills)
		.property(O365_USER_SCHOOLS, &BusinessUser::m_Schools)
		.property(O365_USER_INTERESTS, &BusinessUser::m_Interests)
		.property(O365_USER_PREFERREDNAME, &BusinessUser::m_PreferredName)(
			metadata(MetadataKeys::ReadOnly, true) // always return error 400 despite nothing says it's read-only...
			)
		.property(O365_USER_HIREDATE, &BusinessUser::m_HireDate)

// **** On Premises info
		.property(O365_USER_ONPREMISESSYNCENABLED, &BusinessUser::m_OnPremisesSyncEnabled)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_ONPREMISESLASTSYNCDATETIME, &BusinessUser::m_OnPremisesLastSyncDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_ONPREMISESSECURITYIDENTIFIER, &BusinessUser::m_OnPremisesSecurityIdentifier)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_ONPREMISESIMMUTABLEID, &BusinessUser::m_OnPremisesImmutableId)(
			metadata(MetadataKeys::ValidForCreation, true)
			)

// **** HIDDEN - No sense to show these in edit
		.property(O365_USER_ASSIGNEDLICENSES, &BusinessUser::m_AssignedLicenses)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_ASSIGNEDPLANS, &BusinessUser::m_AssignedPlans)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_PROVISIONEDPLANS, &BusinessUser::m_ProvisionedPlans)(
			metadata(MetadataKeys::ReadOnly, true)
			)

// *** ONLY FOR DELETED USER
		.property(O365_USER_DELETEDDATETIME, &BusinessUser::m_DeletedDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)

// Recently added (2019/01/31)
		.property(O365_USER_AGEGROUP, &BusinessUser::m_AgeGroup)(
			metadata(MetadataKeys::ReadOnly, true) // Enum: null, minor, notAdult, Adult
			)
		.property(O365_USER_CREATEDDATETIME, &BusinessUser::m_CreatedDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_CONSENTPROVIDEDFORMINOR, &BusinessUser::m_ConsentProvidedForMinor)(
			metadata(MetadataKeys::ReadOnly, true) // Enum: null, granted, denied, notRequired
			)
		.property(O365_USER_LEGALAGEGROUPCLASSIFICATION, &BusinessUser::m_LegalAgeGroupClassification)(
			metadata(MetadataKeys::ReadOnly, true) // Enum: null, minorWithOutParentalConsent, minorWithParentalConsent, minorNoParentalConsentRequired, notAdult, adult
			)
		.property(O365_USER_ONPREMISESDOMAINNAME, &BusinessUser::m_OnPremisesDomainName)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_ONPREMISESSAMACCOUNTNAME, &BusinessUser::m_OnPremisesSamAccountName)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_ONPREMISESUSERPRINCIPALNAME, &BusinessUser::m_OnPremisesUserPrincipalName)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME, &BusinessUser::m_SignInSessionsValidFromDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_ONPREMISESPROVISIONINGERRORS, &BusinessUser::m_OnPremisesProvisioningErrors)(
			metadata(MetadataKeys::ReadOnly, true)
			)

		// Recently added (2019/03/20)
		.property(O365_USER_EMPLOYEEID, &BusinessUser::m_EmployeeID)
		.property(O365_USER_FAXNUMBER, &BusinessUser::m_FaxNumber)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_USER_ISRESOURCEACCOUNT, &BusinessUser::m_IsResourceAccount) // null == false
		.property(O365_USER_ONPREMISESDISTINGUISHEDNAME, &BusinessUser::m_OnPremisesDistinguishedName)(
			metadata(MetadataKeys::ReadOnly, true)
			)

		// Recently moved from beta to v1.0 (2019/03/22)
		.property(O365_USER_OTHERMAILS, &BusinessUser::m_OtherMails)
		.property(O365_USER_SHOWINADDRESSLIST, &BusinessUser::m_ShowInAddressList)
		.property(O365_USER_ONPREMISESEXTENSIONATTRIBUTES, &BusinessUser::m_OnPremisesExtensionAttributes)

		// Recently added (2020/05/04)
		.property(O365_USER_CREATIONTYPE, &BusinessUser::m_CreationType)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_IDENTITIES, &BusinessUser::m_Identities)(
			metadata(MetadataKeys::SyncOnly, true)
			)
		.property(O365_USER_LASTPASSWORDCHANGEDATETIME, &BusinessUser::m_LastPasswordChangeDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_LICENSEASSIGNMENTSTATES, &BusinessUser::m_LicenseAssignmentStates)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_LICENSEASSIGNMENTSTATES, &BusinessUser::m_RefresTokenValidFromDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)

		/* Beta API */
		.property(O365_USER_PREFERREDDATALOCATION, &BusinessUser::m_PreferredDataLocation)(
			metadata(MetadataKeys::BetaAPI, true) // Duplicated from User, kinda bad design..
			)
		.property(O365_USER_EXTERNALUSERSTATUS, &BusinessUser::m_ExternalUserStatus)(
			metadata(MetadataKeys::BetaAPI, true), // Duplicated from User, kinda bad design..
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_USER_EXTERNALUSERSTATUSCHANGEDON, &BusinessUser::m_ExternalUserStatus)(
			metadata(MetadataKeys::BetaAPI, true), // Duplicated from User, kinda bad design..
			metadata(MetadataKeys::ReadOnly, true)
			)

		// Manager
		.property(O365_USER_MANAGER_ID, &BusinessUser::m_ManagerID)
		;
}


// =================================================================================================
// Kind of a hack, specialization for User.

template<>
vector<rttr::property> BusinessObject::GetUpdatableProperties<User, BusinessUser>(User& p_JsonObject, const BusinessObjectBoolMetadataFilters& p_BusinessObjectMetadataFilter, const JsonObjectRequestWhenFilter& p_JsonObjectRequestWhenFilter) const
{
	// Use these types so that we can just copy paste from generic impl.
	// Only a few lines added for PasswordProfile
	using BusinessType = BusinessUser;
	using JsonType = User;

	const BusinessType* businessObj = dynamic_cast<const BusinessType*>(this);
	ASSERT(nullptr != businessObj);
	if (nullptr == businessObj)
		return{};

	p_JsonObject.m_Id = GetID();

	vector<rttr::property> updatableProps;

	const rttr::type businessType = rttr::type::get<BusinessType>();
	const rttr::type jsonType = rttr::type::get<JsonType>();

	for (const auto& businessProp : businessType.get_properties())
	{
		// ignore some properties.
		if (!businessProp.is_valid() || !p_BusinessObjectMetadataFilter(businessProp))
			continue;

		auto jsonPropName = businessProp.get_name();
		/* ++ user specialization ++ */
		if (businessProp.get_name() == O365_USER_PASSWORDPROFILE_PASSWORD
			|| businessProp.get_name() == O365_USER_PASSWORDPROFILE_FORCECHANGE
			|| businessProp.get_name() == O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA)
			jsonPropName = O365_USER_PASSWORDPROFILE;
		/* -- user specialization -- */
		const rttr::property jsonTypeProp = jsonType.get_property(jsonPropName);

		if (!jsonTypeProp.is_valid() || !p_JsonObjectRequestWhenFilter(jsonTypeProp))
			continue;

		if (businessProp.get_type() == rttr::type::get<boost::YOpt<PooledString>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<PooledString>>();
				if (value != boost::none)
				{
					/* ++ user specialization ++ */
					if (businessProp.get_name() == O365_USER_PASSWORDPROFILE_PASSWORD)
					{
						if (jsonTypeProp.is_valid())
						{
							ASSERT(jsonTypeProp.get_name() == O365_USER_PASSWORDPROFILE);
							rttr::variant propVal = jsonTypeProp.get_value(p_JsonObject);
							boost::YOpt<PasswordProfile> ppValue = propVal.get_value<boost::YOpt<PasswordProfile>>();
							if (!ppValue)
								ppValue.emplace();
							ppValue->Password = value;

							const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, ppValue);
							ASSERT(setSuccess);

							auto it = std::find_if(updatableProps.begin(), updatableProps.end(), [](rttr::property& prop) {return prop.get_name() == O365_USER_PASSWORDPROFILE; });
							if (updatableProps.end() != it)
								*it = jsonTypeProp;
							else
								updatableProps.push_back(jsonTypeProp);
						}
					}
					else
					/* -- user specialization -- */
					{
						const rttr::property jsonTypeProp = jsonType.get_property(businessProp.get_name());
						if (jsonTypeProp.is_valid())
						{
							const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, value);
							ASSERT(setSuccess);
							updatableProps.push_back(jsonTypeProp);
						}
					}
				}
			}
		}
		else if (businessProp.get_type() == rttr::type::get<boost::YOpt<bool>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<bool>>();
				if (value != boost::none)
				{
					/* ++ user specialization ++ */
					if (businessProp.get_name() == O365_USER_PASSWORDPROFILE_FORCECHANGE
						|| businessProp.get_name() == O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA)
					{
						if (jsonTypeProp.is_valid())
						{
							ASSERT(jsonTypeProp.get_name() == O365_USER_PASSWORDPROFILE);
							rttr::variant propVal = jsonTypeProp.get_value(p_JsonObject);
							boost::YOpt<PasswordProfile> ppValue = propVal.get_value<boost::YOpt<PasswordProfile>>();
							if (!ppValue)
								ppValue.emplace();
							if (businessProp.get_name() == O365_USER_PASSWORDPROFILE_FORCECHANGE)
								ppValue->ForceChangePasswordNextSignIn = value;
							else //if (businessProp.get_name() == O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA)
								ppValue->ForceChangePasswordNextSignInWithMfa = value;

							const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, ppValue);
							ASSERT(setSuccess);

							auto it = std::find_if(updatableProps.begin(), updatableProps.end(), [](rttr::property& prop) {return prop.get_name() == O365_USER_PASSWORDPROFILE; });
							if (updatableProps.end() != it)
								*it = jsonTypeProp;
							else
								updatableProps.push_back(jsonTypeProp);
						}
					}
					else
					/* -- user specialization -- */
					{
						const rttr::property jsonTypeProp = jsonType.get_property(businessProp.get_name());
						if (jsonTypeProp.is_valid())
						{
							const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, value);
							ASSERT(setSuccess);
							updatableProps.push_back(jsonTypeProp);
						}
					}
				}
			}
		}
		else if (businessProp.get_type() == rttr::type::get<boost::YOpt<YTimeDate>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<YTimeDate>>();
				if (value != boost::none)
				{
					const rttr::property jsonTypeProp = jsonType.get_property(businessProp.get_name());
					if (jsonTypeProp.is_valid())
					{
						boost::YOpt<PooledString> dateString = TimeUtil::GetInstance().GetISO8601String(*value, TimeUtil::ISO8601_HAS_DATE_AND_TIME);
						// Dates can't be null or empty in Office 365 graph API.
						// "0001-01-01T00:00:00Z" is the date we get when it has never been set, unfortunately settings it gives HTTP 500.
						// Then do not send empty string if ever it's null or empty
						//if (dateString && dateString->IsEmpty()) 
						//	dateString = _YTEXT("0001-01-01T00:00:00Z"); 
						if (dateString && !dateString->IsEmpty())
						{
							const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, dateString);
							ASSERT(setSuccess);
							updatableProps.push_back(jsonTypeProp);
						}
					}
				}
			}
		}
		else if (businessProp.get_type() == rttr::type::get<boost::YOpt<vector<PooledString>>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<vector<PooledString>>>();
				if (value != boost::none)
				{
					const rttr::property jsonTypeProp = jsonType.get_property(businessProp.get_name());
					if (jsonTypeProp.is_valid())
					{
						if (jsonTypeProp.get_type() == rttr::type::get<boost::YOpt<vector<PooledString>>>())
						{
							const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, value);
							ASSERT(setSuccess);
							updatableProps.push_back(jsonTypeProp);
						}
						else if (jsonTypeProp.get_type() == rttr::type::get<vector<PooledString>>())
						{
							const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, *value);
							ASSERT(setSuccess);
							updatableProps.push_back(jsonTypeProp);
						}
						else
						{
							ASSERT(false);
						}
					}
				}
			}
		}
		else if (businessProp.get_type() == rttr::type::get<boost::YOpt<int32_t>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<int32_t>>();
				if (value != boost::none)
				{
					const rttr::property jsonTypeProp = jsonType.get_property(businessProp.get_name());
					if (jsonTypeProp.is_valid())
					{
						const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, value);
						ASSERT(setSuccess);
						updatableProps.push_back(jsonTypeProp);
					}
				}
			}
		}
		/* ++ user specialization ++ */
		else if (businessProp.get_type() == rttr::type::get<boost::YOpt<OnPremisesExtensionAttributes>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<OnPremisesExtensionAttributes>>();
				if (value != boost::none)
				{
					const rttr::property jsonTypeProp = jsonType.get_property(businessProp.get_name());
					if (jsonTypeProp.is_valid())
					{
						const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, value);
						ASSERT(setSuccess);
						updatableProps.push_back(jsonTypeProp);
					}
				}
			}
		}
		/* -- user specialization -- */
		else if (businessProp.get_type() == rttr::type::get<boost::YOpt<vector<ObjectIdentity>>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<vector<ObjectIdentity>>>();
				if (value != boost::none)
				{
					const rttr::property jsonTypeProp = jsonType.get_property(businessProp.get_name());
					if (jsonTypeProp.is_valid())
					{
						const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, value);
						ASSERT(setSuccess);
						updatableProps.push_back(jsonTypeProp);
					}
				}
			}
		}
		else
		{
			ASSERT(false);
		}
	}

	return updatableProps;
}

// =================================================================================================

namespace
{
	User CachedToUser(const CachedUser& p_CachedUser)
	{
		User user;
		user.m_Id = p_CachedUser.GetID();
		user.DisplayName = p_CachedUser.GetDisplayName();
		user.UserPrincipalName = p_CachedUser.GetUserPrincipalName();
		user.UserType = p_CachedUser.GetUserType();
		if (p_CachedUser.GetDeletedDateTime().is_initialized())
			user.DeletedDateTime = TimeUtil::GetInstance().GetISO8601String(p_CachedUser.GetDeletedDateTime().get());
		user.Mail = p_CachedUser.GetMail();
		return user;
	}
}

const wstring BusinessUser::g_UserTypeMember = _YTEXT("Member");
const wstring BusinessUser::g_UserTypeGuest = _YTEXT("Guest");

boost::YOpt<bool> BusinessUser::GetMailboxDeliverToMailboxAndForward() const
{
	return m_DeliverToMailboxAndForward;
}

void BusinessUser::SetMailboxDeliverToMailboxAndForward(boost::YOpt<bool> val)
{
	m_DeliverToMailboxAndForward = val;
}

boost::YOpt<PooledString> BusinessUser::GetMailboxForwardingSmtpAddress() const
{
	return m_ForwardingSmtpAddress;
}

void BusinessUser::SetMailboxForwardingSmtpAddress(boost::YOpt<PooledString> val)
{
	m_ForwardingSmtpAddress = val;
}

boost::YOpt<SapioError> BusinessUser::GetMailboxInfoError() const
{
	return m_MailboxInfoError;
}

void BusinessUser::SetMailboxInfoError(boost::YOpt<SapioError> val)
{
	m_MailboxInfoError = val;
}

boost::YOpt<SapioError> BusinessUser::GetRecipientPermissionsError() const
{
	return m_RecipientPermissionsError;
}

void BusinessUser::SetRecipientPermissionsError(boost::YOpt<SapioError> val)
{
	m_RecipientPermissionsError = val;
}

boost::YOpt<PooledString> BusinessUser::GetMailboxForwardingAddress() const
{
	return m_ForwardingAddress;
}

void BusinessUser::SetMailboxForwardingAddress(const boost::YOpt<PooledString>& val)
{
	m_ForwardingAddress = val;
}

boost::YOpt<PooledString> BusinessUser::GetMailboxType() const
{
	return m_MailboxType;
}

void BusinessUser::SetMailboxType(const boost::YOpt<PooledString>& val)
{
	m_MailboxType = val;
}

BusinessUser::BusinessUser(const User& p_User)
	: BusinessUser()
{
	SetValuesFrom(p_User);
}

BusinessUser::BusinessUser(const CachedUser& p_User)
	: BusinessUser(CachedToUser(p_User))
{
	SetRBACDelegationID(p_User.GetRBACDelegationID());
}

// Used to be in header, but led to crash ... Bug #201105.RO.00D136: Silent crash after clicking on "Create" in Users module
BusinessUser::BusinessUser() = default;

BusinessUser& BusinessUser::MergeWith(const BusinessUser& p_Other)
{
	/** BusinessObject **/
	if (!p_Other.m_Id.IsEmpty())
		m_Id = p_Other.m_Id;
	if (p_Other.m_ErrorMessage)
		m_ErrorMessage = p_Other.m_ErrorMessage;
	m_Flags |= p_Other.m_Flags;
	ASSERT(m_RBAC_DelegationID == p_Other.m_RBAC_DelegationID
		|| (NO_RBAC != m_RBAC_DelegationID || NO_RBAC != p_Other.m_RBAC_DelegationID));
	if (NO_RBAC == m_RBAC_DelegationID && NO_RBAC != p_Other.m_RBAC_DelegationID)
		m_RBAC_DelegationID = p_Other.m_RBAC_DelegationID;

	for (const auto& item : p_Other.m_DataBlockDates)
	{
		if (item.second)
			m_DataBlockDates[item.first] = item.second;
	}

	/*for (const auto& item : p_Other.m_DataBlockErrors)
	{
		if (item.second)
			m_DataBlockErrors[item.first] = item.second;
	}*/

	/********************/

	if (p_Other.m_AboutMe)
		m_AboutMe = p_Other.m_AboutMe;
	if (p_Other.m_AccountEnabled)
		m_AccountEnabled = p_Other.m_AccountEnabled;
	if (p_Other.m_AgeGroup)
		m_AgeGroup = p_Other.m_AgeGroup;
	if (p_Other.m_AssignedLicenses)
		m_AssignedLicenses = p_Other.m_AssignedLicenses;
	if (p_Other.m_AssignedPlans)
		m_AssignedPlans = p_Other.m_AssignedPlans;
	if (p_Other.m_Birthday)
		m_Birthday = p_Other.m_Birthday;
	if (p_Other.m_BusinessPhones)
		m_BusinessPhones = p_Other.m_BusinessPhones;
	if (p_Other.m_City)
		m_City = p_Other.m_City;
	if (p_Other.m_CompanyName)
		m_CompanyName = p_Other.m_CompanyName;
	if (p_Other.m_ConsentProvidedForMinor)
		m_ConsentProvidedForMinor = p_Other.m_ConsentProvidedForMinor;
	if (p_Other.m_Country)
		m_Country = p_Other.m_Country;
	if (p_Other.m_CreatedDateTime)
		m_CreatedDateTime = p_Other.m_CreatedDateTime;
	if (p_Other.m_Department)
		m_Department = p_Other.m_Department;
	if (p_Other.m_DisplayName)
		m_DisplayName = p_Other.m_DisplayName;
	if (p_Other.m_EmployeeID)
		m_EmployeeID = p_Other.m_EmployeeID;
	if (p_Other.m_FaxNumber)
		m_FaxNumber = p_Other.m_FaxNumber;
	if (p_Other.m_ForceChangePassword)
		m_ForceChangePassword = p_Other.m_ForceChangePassword;
	if (p_Other.m_ForceChangePasswordWithMfa)
		m_ForceChangePasswordWithMfa = p_Other.m_ForceChangePasswordWithMfa;
	if (p_Other.m_GivenName)
		m_GivenName = p_Other.m_GivenName;
	if (p_Other.m_HireDate)
		m_HireDate = p_Other.m_HireDate;
	if (p_Other.m_ImAddresses)
		m_ImAddresses = p_Other.m_ImAddresses;
	if (p_Other.m_Interests)
		m_Interests = p_Other.m_Interests;
	if (p_Other.m_IsResourceAccount)
		m_IsResourceAccount = p_Other.m_IsResourceAccount;
	if (p_Other.m_JobTitle)
		m_JobTitle = p_Other.m_JobTitle;
	if (p_Other.m_LegalAgeGroupClassification)
		m_LegalAgeGroupClassification = p_Other.m_LegalAgeGroupClassification;
	if (p_Other.m_Mail)
		m_Mail = p_Other.m_Mail;
	if (p_Other.m_MailNickname)
		m_MailNickname = p_Other.m_MailNickname;
	if (p_Other.m_MailboxSettings)
		m_MailboxSettings = p_Other.m_MailboxSettings;
	if (p_Other.m_MobilePhone)
		m_MobilePhone = p_Other.m_MobilePhone;
	if (p_Other.m_MySite)
		m_MySite = p_Other.m_MySite;
	if (p_Other.m_OfficeLocation)
		m_OfficeLocation = p_Other.m_OfficeLocation;
	if (p_Other.m_OnPremisesDistinguishedName)
		m_OnPremisesDistinguishedName = p_Other.m_OnPremisesDistinguishedName;
	if (p_Other.m_OnPremisesDomainName)
		m_OnPremisesDomainName = p_Other.m_OnPremisesDomainName;
	if (p_Other.m_OnPremisesImmutableId)
		m_OnPremisesImmutableId = p_Other.m_OnPremisesImmutableId;
	if (p_Other.m_OnPremisesLastSyncDateTime)
		m_OnPremisesLastSyncDateTime = p_Other.m_OnPremisesLastSyncDateTime;
	if (p_Other.m_OnPremisesProvisioningErrors)
		m_OnPremisesProvisioningErrors = p_Other.m_OnPremisesProvisioningErrors;
	if (p_Other.m_OnPremisesSamAccountName)
		m_OnPremisesSamAccountName = p_Other.m_OnPremisesSamAccountName;
	if (p_Other.m_OnPremisesSecurityIdentifier)
		m_OnPremisesSecurityIdentifier = p_Other.m_OnPremisesSecurityIdentifier;
	if (p_Other.m_OnPremisesSyncEnabled)
		m_OnPremisesSyncEnabled = p_Other.m_OnPremisesSyncEnabled;
	if (p_Other.m_OnPremisesUserPrincipalName)
		m_OnPremisesUserPrincipalName = p_Other.m_OnPremisesUserPrincipalName;
	if (p_Other.m_PasswordPolicies)
		m_PasswordPolicies = p_Other.m_PasswordPolicies;
	if (p_Other.m_Password)
		m_Password = p_Other.m_Password;
	if (p_Other.m_PastProjects)
		m_PastProjects = p_Other.m_PastProjects;
	if (p_Other.m_PostalCode)
		m_PostalCode = p_Other.m_PostalCode;
	if (p_Other.m_PreferredLanguage)
		m_PreferredLanguage = p_Other.m_PreferredLanguage;
	if (p_Other.m_PreferredName)
		m_PreferredName = p_Other.m_PreferredName;
	if (p_Other.m_ProvisionedPlans)
		m_ProvisionedPlans = p_Other.m_ProvisionedPlans;
	if (p_Other.m_ProxyAddresses)
		m_ProxyAddresses = p_Other.m_ProxyAddresses;
	if (p_Other.m_SignInSessionsValidFromDateTime)
		m_SignInSessionsValidFromDateTime = p_Other.m_SignInSessionsValidFromDateTime;
	if (p_Other.m_Responsibilities)
		m_Responsibilities = p_Other.m_Responsibilities;
	if (p_Other.m_Schools)
		m_Schools = p_Other.m_Schools;
	if (p_Other.m_Skills)
		m_Skills = p_Other.m_Skills;
	if (p_Other.m_State)
		m_State = p_Other.m_State;
	if (p_Other.m_StreetAddress)
		m_StreetAddress = p_Other.m_StreetAddress;
	if (p_Other.m_Surname)
		m_Surname = p_Other.m_Surname;
	if (p_Other.m_UsageLocation)
		m_UsageLocation = p_Other.m_UsageLocation;
	if (p_Other.m_UserPrincipalName)
		m_UserPrincipalName = p_Other.m_UserPrincipalName;
	if (p_Other.m_UserType)
		m_UserType = p_Other.m_UserType;

	if (p_Other.m_ArchiveFolderName)
		m_ArchiveFolderName = p_Other.m_ArchiveFolderName;
	if (p_Other.m_DeletedDateTime)
		m_DeletedDateTime = p_Other.m_DeletedDateTime;

	if (!p_Other.m_ParentGroups.empty())
		m_ParentGroups = p_Other.m_ParentGroups;
	if (!p_Other.m_ParentGroupIDsFirstLevel.empty())
		m_ParentGroupIDsFirstLevel = p_Other.m_ParentGroupIDsFirstLevel;

	// manager hierarchy
#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
	if (p_Other.m_Manager)
		m_Manager = p_Other.m_Manager;
	if (!p_Other.m_DirectReports.empty())
		MFCUtil::DuplicateUniquePtrVector(m_DirectReports, p_Other.m_DirectReports);
#endif
	if (p_Other.m_ManagerID)
		m_ManagerID = p_Other.m_ManagerID;

	if (p_Other.m_OtherMails)
		m_OtherMails = p_Other.m_OtherMails;
	if (p_Other.m_PreferredDataLocation)
		m_PreferredDataLocation = p_Other.m_PreferredDataLocation;
	if (p_Other.m_ShowInAddressList)
		m_ShowInAddressList = p_Other.m_ShowInAddressList;
	if (p_Other.m_ExternalUserStatus)
		m_ExternalUserStatus = p_Other.m_ExternalUserStatus;
	if (p_Other.m_ExternalUserStatusChangedOn)
		m_ExternalUserStatusChangedOn = p_Other.m_ExternalUserStatusChangedOn;
	if (p_Other.m_OnPremisesExtensionAttributes)
		m_OnPremisesExtensionAttributes = p_Other.m_OnPremisesExtensionAttributes;

	if (p_Other.m_CreationType)
		m_CreationType = p_Other.m_CreationType;
	if (p_Other.m_Identities)
		m_Identities = p_Other.m_Identities;
	if (p_Other.m_LastPasswordChangeDateTime)
		m_LastPasswordChangeDateTime = p_Other.m_LastPasswordChangeDateTime;
	if (p_Other.m_LicenseAssignmentStates)
		m_LicenseAssignmentStates = p_Other.m_LicenseAssignmentStates;
	if (p_Other.m_RefresTokenValidFromDateTime)
		m_RefresTokenValidFromDateTime = p_Other.m_RefresTokenValidFromDateTime;

	if (p_Other.m_EmployeeID)
		m_EmployeeID = p_Other.m_EmployeeID;
	if (p_Other.m_FaxNumber)
		m_FaxNumber = p_Other.m_FaxNumber;
	if (p_Other.m_IsResourceAccount)
		m_IsResourceAccount = p_Other.m_IsResourceAccount;
	if (p_Other.m_OnPremisesDistinguishedName)
		m_OnPremisesDistinguishedName = p_Other.m_OnPremisesDistinguishedName;

	if (p_Other.m_Drive)
		m_Drive = p_Other.m_Drive;

	// Powershell fields
	if (p_Other.m_MailboxType)
		m_MailboxType = p_Other.m_MailboxType;
	if (p_Other.m_DeliverToMailboxAndForward)
		m_DeliverToMailboxAndForward  = p_Other.m_DeliverToMailboxAndForward;
	if (p_Other.m_ForwardingSmtpAddress)
		m_ForwardingSmtpAddress = p_Other.m_ForwardingSmtpAddress;
	if (p_Other.m_ForwardingAddress)
		m_ForwardingAddress = p_Other.m_ForwardingAddress;
	if (p_Other.m_MailboxCanSendOnBehalf)
		m_MailboxCanSendOnBehalf = p_Other.m_MailboxCanSendOnBehalf;
	if (p_Other.m_MailboxCanSendAsUser)
		m_MailboxCanSendAsUser = p_Other.m_MailboxCanSendAsUser;

	// Errors
	if (p_Other.m_ArchiveFolderNameError)
		m_ArchiveFolderNameError = p_Other.m_ArchiveFolderNameError;
	if (p_Other.m_MailboxSettingsError)
		m_MailboxSettingsError = p_Other.m_MailboxSettingsError;
	if (p_Other.m_DriveError)
		m_DriveError = p_Other.m_DriveError;
	if (p_Other.m_ManagerError)
		m_ManagerError = p_Other.m_ManagerError;
	if (p_Other.m_MailboxInfoError)
		m_MailboxInfoError = p_Other.m_MailboxInfoError;
	if (p_Other.m_RecipientPermissionsError)
		m_RecipientPermissionsError = p_Other.m_RecipientPermissionsError;

	return *this;
}

void BusinessUser::SetValuesFrom(const User& p_User)
{
	SetID(p_User.m_Id);
	SetDisplayName(p_User.DisplayName);
	SetAboutMe(p_User.AboutMe);
	SetAccountEnabled(p_User.AccountEnabled);
	SetAgeGroup(p_User.AgeGroup);
	SetAssignedLicenses(p_User.AssignedLicenses);
	SetAssignedPlans(p_User.AssignedPlans);

	if (p_User.Birthday.is_initialized())
	{
		if (p_User.Birthday->IsEmpty())
		{
			SetBirthday(boost::none);
		}
		else
		{
			YTimeDate td;
			if (TimeUtil::GetInstance().ConvertTextToDate(p_User.Birthday.get(), td))
				SetBirthday(td);
		}
	}

	SetBusinessPhones(p_User.BusinessPhones);
	SetCity(p_User.City);
	SetConsentProvidedForMinor(p_User.ConsentProvidedForMinor);
	SetCountry(p_User.Country);

	if (p_User.CreatedDateTime.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_User.CreatedDateTime.get(), td))
			SetCreatedDateTime(td);
	}
	else
	{
		SetCreatedDateTime(boost::none);
	}

	SetDepartment(p_User.Department);
	SetGivenName(p_User.GivenName);

	if (p_User.HireDate.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_User.HireDate.get(), td))
			SetHireDate(td);
	}
	else
	{
		SetHireDate(boost::none);
	}

	SetImAddresses(p_User.ImAddresses);
	SetInterests(p_User.Interests);
	SetJobTitle(p_User.JobTitle);
	SetLegalAgeGroupClassification(p_User.LegalAgeGroupClassification);
	SetMail(p_User.Mail);
	SetMailNickname(p_User.MailNickname);
	SetMailboxSettings(p_User.MailboxSettings);
	SetMobilePhone(p_User.MobilePhone);
	SetMySite(p_User.MySite);
	SetOfficeLocation(p_User.OfficeLocation);
	SetOnPremisesDomainName(p_User.OnPremisesDomainName);
	SetOnPremisesImmutableId(p_User.OnPremisesImmutableId);

	if (p_User.OnPremisesLastSyncDateTime.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_User.OnPremisesLastSyncDateTime.get(), td))
			SetOnPremisesLastSyncDateTime(td);
	}
	else
	{
		SetSignInSessionsValidFromDateTime(boost::none);
	}

	SetOnPremisesProvisioningErrors(p_User.OnPremisesProvisioningErrors);
	SetOnPremisesSamAccountName(p_User.OnPremisesSamAccountName);
	SetOnPremisesSecurityIdentifier(p_User.OnPremisesSecurityIdentifier);
	SetOnPremisesSyncEnabled(p_User.OnPremisesSyncEnabled);
	SetOnPremisesUserPrincipalName(p_User.OnPremisesUserPrincipalName);
	SetPasswordPolicies(p_User.PasswordPolicies);
	SetPasswordProfile(p_User.PasswordProfile);
	SetPastProjects(p_User.PastProjects);
	SetPostalCode(p_User.PostalCode);
	SetPreferredLanguage(p_User.PreferredLanguage);
	SetPreferredName(p_User.PreferredName);
	SetProvisionedPlans(p_User.ProvisionedPlans);
	SetProxyAddresses(p_User.ProxyAddresses);

	if (p_User.SignInSessionsValidFromDateTime.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_User.SignInSessionsValidFromDateTime.get(), td))
			SetSignInSessionsValidFromDateTime(td);
	}
	else
	{
		SetSignInSessionsValidFromDateTime(boost::none);
	}

	SetResponsibilities(p_User.Responsibilities);
	SetSchools(p_User.Schools);
	SetSkills(p_User.Skills);
	SetState(p_User.State);
	SetStreetAddress(p_User.StreetAddress);
	SetSurname(p_User.Surname);
	SetUsageLocation(p_User.UsageLocation);
	SetUserPrincipalName(p_User.UserPrincipalName);
	SetUserType(p_User.UserType);
	SetDeletedDateTime(p_User.DeletedDateTime);

	SetOtherMails(p_User.OtherMails);
	SetPreferredDataLocation(p_User.PreferredDataLocation);
	SetShowInAddressList(p_User.ShowInAddressList);
	SetExternalUserStatus(p_User.ExternalUserStatus);

	if (p_User.ExternalUserStatusChangedOn.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_User.ExternalUserStatusChangedOn.get(), td))
			SetExternalUserStatusChangedOn(td);
	}
	else
	{
		SetExternalUserStatusChangedOn(boost::none);
	}

	SetOnPremisesExtensionAttributes(p_User.OnPremisesExtensionAttributes);

	SetCreationType(p_User.CreationType);
	SetIdentities(p_User.Identities);
	if (p_User.LastPasswordChangeDateTime.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_User.LastPasswordChangeDateTime.get(), td))
			SetLastPasswordChangeDateTime(td);
	}
	else
	{
		SetLastPasswordChangeDateTime(boost::none);
	}

	SetLicenseAssignmentStates(p_User.LicenseAssignmentStates);

	if (p_User.RefresTokenValidFromDateTime.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_User.RefresTokenValidFromDateTime.get(), td))
			SetRefresTokenValidFromDateTime(td);
	}
	else
	{
		SetRefresTokenValidFromDateTime(boost::none);
	}


	SetEmployeeID(p_User.EmployeeID);
	SetFaxNumber(p_User.FaxNumber);
	SetIsResourceAccount(p_User.IsResourceAccount);
	SetOnPremisesDistinguishedName(p_User.OnPremisesDistinguishedName);
}

void BusinessUser::SetValuesFrom(const CachedUser& p_User)
{
	SetValuesFrom(CachedToUser(p_User));
	SetRBACDelegationID(p_User.GetRBACDelegationID());
}

bool BusinessUser::IsMoreLoaded() const
{
	// FIXME: LoadMore is still made of several request all at once.
	// If we have this block we should have all.
	return GetDataDate(UBI::SYNCV1).is_initialized();
}

User BusinessUser::ToUser(const BusinessUser & p_BusinessUser)
{
    User user;

    user.m_Id = p_BusinessUser.GetID();
	user.DisplayName = p_BusinessUser.GetDisplayName();
    user.AboutMe = p_BusinessUser.GetAboutMe();
    user.AccountEnabled = p_BusinessUser.GetAccountEnabled();
	user.AgeGroup = p_BusinessUser.GetAgeGroup();
	p_BusinessUser.CopyAssignedLicensesTo(user.AssignedLicenses);
	if (p_BusinessUser.GetAssignedPlans())
		user.AssignedPlans = *p_BusinessUser.GetAssignedPlans();
    if (p_BusinessUser.GetBirthday())
        user.Birthday = TimeUtil::GetInstance().GetISO8601String(p_BusinessUser.GetBirthday().get());
	if (p_BusinessUser.GetBusinessPhones())
		user.BusinessPhones = *p_BusinessUser.GetBusinessPhones();
    user.City = p_BusinessUser.GetCity();
	user.ConsentProvidedForMinor = p_BusinessUser.GetConsentProvidedForMinor();
	if (p_BusinessUser.GetCreatedDateTime())
		user.CreatedDateTime = TimeUtil::GetInstance().GetISO8601String(p_BusinessUser.GetCreatedDateTime().get());
    user.Country = p_BusinessUser.GetCountry();
    user.Department = p_BusinessUser.GetDepartment();
    user.GivenName = p_BusinessUser.GetGivenName();
    if (p_BusinessUser.GetHireDate().is_initialized())
        user.HireDate = TimeUtil::GetInstance().GetISO8601String(p_BusinessUser.GetHireDate().get());
    user.Interests = p_BusinessUser.GetInterests();
    user.JobTitle = p_BusinessUser.GetJobTitle();
	user.LegalAgeGroupClassification = p_BusinessUser.GetLegalAgeGroupClassification();
    user.Mail = p_BusinessUser.GetMail();
    user.MailNickname = p_BusinessUser.GetMailNickname();
    user.MailboxSettings = p_BusinessUser.GetMailboxSettings();
    user.MobilePhone = p_BusinessUser.GetMobilePhone();
    user.MySite = p_BusinessUser.GetMySite();
    user.OfficeLocation = p_BusinessUser.GetOfficeLocation();
	user.OnPremisesDomainName = p_BusinessUser.GetOnPremisesDomainName();
    user.OnPremisesImmutableId = p_BusinessUser.GetOnPremisesImmutableId();
	if (p_BusinessUser.GetOnPremisesLastSyncDateTime())
		user.OnPremisesLastSyncDateTime = TimeUtil::GetInstance().GetISO8601String(p_BusinessUser.GetOnPremisesLastSyncDateTime().get());
	user.OnPremisesProvisioningErrors = p_BusinessUser.GetOnPremisesProvisioningErrors();
	user.OnPremisesSamAccountName = p_BusinessUser.GetOnPremisesSamAccountName();
    user.OnPremisesSecurityIdentifier = p_BusinessUser.GetOnPremisesSecurityIdentifier();
    user.OnPremisesSyncEnabled = p_BusinessUser.GetOnPremisesSyncEnabled();
	user.OnPremisesUserPrincipalName = p_BusinessUser.GetOnPremisesUserPrincipalName();
    user.PasswordPolicies = p_BusinessUser.GetPasswordPolicies();

	PasswordProfile passwordProfile;
	passwordProfile.Password = p_BusinessUser.GetPassword() ? *p_BusinessUser.GetPassword() : _YTEXT("");
    passwordProfile.ForceChangePasswordNextSignIn = p_BusinessUser.GetForceChangePassword() ? *p_BusinessUser.GetForceChangePassword() : false;
	passwordProfile.ForceChangePasswordNextSignInWithMfa = p_BusinessUser.GetForceChangePasswordWithMfa() ? *p_BusinessUser.GetForceChangePasswordWithMfa() : false;
	user.PasswordProfile = passwordProfile;
    
    user.PastProjects = p_BusinessUser.GetPastProjects();
    user.PostalCode = p_BusinessUser.GetPostalCode();
    user.PreferredLanguage = p_BusinessUser.GetPreferredLanguage();
    user.PreferredName = p_BusinessUser.GetPreferredName();
	if (p_BusinessUser.GetProvisionedPlans())
		user.ProvisionedPlans = *p_BusinessUser.GetProvisionedPlans();
	if (p_BusinessUser.GetProxyAddresses())
		user.ProxyAddresses = *p_BusinessUser.GetProxyAddresses();
	if (p_BusinessUser.GetSignInSessionsValidFromDateTime())
		user.SignInSessionsValidFromDateTime = TimeUtil::GetInstance().GetISO8601String(p_BusinessUser.GetSignInSessionsValidFromDateTime().get());
    user.Responsibilities = p_BusinessUser.GetResponsibilities();
    user.Schools = p_BusinessUser.GetSchools();
    user.Skills = p_BusinessUser.GetSkills();
    user.State = p_BusinessUser.GetState();
    user.StreetAddress = p_BusinessUser.GetStreetAddress();
    user.Surname = p_BusinessUser.GetSurname();
    user.UsageLocation = p_BusinessUser.GetUsageLocation();
    user.UserPrincipalName = p_BusinessUser.GetUserPrincipalName();
	user.UserType = p_BusinessUser.GetUserType();
	if (p_BusinessUser.GetDeletedDateTime().is_initialized())
		user.DeletedDateTime = TimeUtil::GetInstance().GetISO8601String(p_BusinessUser.GetDeletedDateTime().get());

	user.OtherMails = p_BusinessUser.GetOtherMails();
	user.PreferredDataLocation = p_BusinessUser.GetPreferredDataLocation();
	user.ShowInAddressList = p_BusinessUser.GetShowInAddressList();
	user.ExternalUserStatus = p_BusinessUser.GetExternalUserStatus();
	if (p_BusinessUser.GetExternalUserStatusChangedOn().is_initialized())
		user.ExternalUserStatusChangedOn = TimeUtil::GetInstance().GetISO8601String(p_BusinessUser.GetExternalUserStatusChangedOn().get());
	user.OnPremisesExtensionAttributes = p_BusinessUser.GetOnPremisesExtensionAttributes();

	user.EmployeeID = p_BusinessUser.GetEmployeeID();
	user.FaxNumber = p_BusinessUser.GetFaxNumber();
	user.IsResourceAccount = p_BusinessUser.GetIsResourceAccount();
	user.OnPremisesDistinguishedName = p_BusinessUser.GetOnPremisesDistinguishedName();

	user.CreationType = p_BusinessUser.GetCreationType();
	user.Identities = p_BusinessUser.GetIdentities();
	if (p_BusinessUser.GetLastPasswordChangeDateTime().is_initialized())
		user.LastPasswordChangeDateTime = TimeUtil::GetInstance().GetISO8601String(p_BusinessUser.GetLastPasswordChangeDateTime().get());
	user.LicenseAssignmentStates = p_BusinessUser.GetLicenseAssignmentStates();
	if (p_BusinessUser.GetRefresTokenValidFromDateTime().is_initialized())
		user.RefresTokenValidFromDateTime = TimeUtil::GetInstance().GetISO8601String(p_BusinessUser.GetRefresTokenValidFromDateTime().get());

	return user;
}

bool BusinessUser::operator<(const BusinessUser& p_Other) const
{
	return GetID() < p_Other.GetID();
}

wstring BusinessUser::GetValue(const wstring& p_PropName) const
{
	if (Str::beginsWith(p_PropName, _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES)))
	{
		if (GetOnPremisesExtensionAttributes())
		{
			static const wstring prefix = wstring(_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES)) + _YUID(".extensionAttribute");
			const auto index = Str::getIntFromString(p_PropName.substr(prefix.size()));
			ASSERT(1 <= index && index <= 15);
			if (1 <= index && index <= 15)
			{
				std::vector<boost::YOpt<PooledString> OnPremisesExtensionAttributes::*> attributes
				{
						&OnPremisesExtensionAttributes::m_ExtensionAttribute1
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute2
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute3
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute4
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute5
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute6
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute7
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute8
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute9
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute10
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute11
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute12
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute13
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute14
					,	&OnPremisesExtensionAttributes::m_ExtensionAttribute15
				};

				const auto& val = (*GetOnPremisesExtensionAttributes()).*(attributes[index - 1]);
				return val ? *val : wstring();
			}
		}

		return wstring();
	}
	else if (p_PropName == _YUID(O365_USER_ACCOUNTENABLED))
		return GetAccountEnabled() ? *GetAccountEnabled() ? BusinessUserConfiguration::GetInstance().GetValueStringSignInStatus_Allowed() : BusinessUserConfiguration::GetInstance().GetValueStringSignInStatus_Blocked() : wstring();

	return BusinessObject::GetValue(p_PropName);
}

vector<PooledString> BusinessUser::GetProperties(const wstring& p_PropName) const
{
	if (Str::beginsWith(p_PropName, _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES)))
		return { _YTEXT(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) };

	ASSERT(p_PropName == MFCUtil::convertASCII_to_UNICODE(rttr::type::get<BusinessUser>().get_property(Str::convertToASCII(p_PropName.c_str())).get_name().to_string()));
	return { p_PropName };
}

void BusinessUser::ClearLicenseData()
{
	m_AssignedLicenses.reset();
	m_AssignedPlans.reset(); 
	m_ProvisionedPlans.reset();
}

void BusinessUser::SetShouldRevokeAccess(bool p_Revoke)
{
	ASSERT(p_Revoke); // Makes no sense otherwise
	m_ShouldRevokeAccess = p_Revoke;
}

bool BusinessUser::GetShouldRevokeAccess() const
{
	return m_ShouldRevokeAccess;
}

const vector<PooledString>& BusinessUser::GetParentGroups() const
{
	return m_ParentGroups;
}

const vector<PooledString>& BusinessUser::GetParentGroupsIDsFirstLevel() const
{
	return m_ParentGroupIDsFirstLevel;
}

void BusinessUser::SetParentGroups(const vector<PooledString>& p_GroupIds)
{
	m_ParentGroups = p_GroupIds;
}

void BusinessUser::SetFirstLevelParentGroups(const vector<PooledString>& p_GroupIds)
{
	for (const auto& gp : p_GroupIds)
	{
		ASSERT(gp != GetID());// what ho?
		if (gp != GetID())
		{
			m_ParentGroupIDsFirstLevel.push_back(gp);
			if (std::none_of(m_ParentGroups.begin(), m_ParentGroups.end(), [&gp](const PooledString& id) { return gp == id; }))
				m_ParentGroups.push_back(gp);
		}
	}
}

bool BusinessUser::IsDirectMemberOf(const BusinessGroup& p_Group) const
{
	return std::find(m_ParentGroupIDsFirstLevel.begin(), m_ParentGroupIDsFirstLevel.end(), p_Group.GetID()) != m_ParentGroupIDsFirstLevel.end();
}

#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
void BusinessUser::SetManager(const BusinessUser& p_Manager)
{
	m_Manager = std::make_shared<BusinessUser>(p_Manager);
}

std::shared_ptr<BusinessUser>& BusinessUser::GetManager()
{
	return m_Manager;
}

BusinessUser* BusinessUser::GetManagerTop()
{
	BusinessUser* topManagerRV = this;
	
	while (nullptr != topManagerRV && topManagerRV->HasManager())
		topManagerRV = topManagerRV->GetManager().get();

	ASSERT(nullptr != topManagerRV);
	return topManagerRV;
}

bool BusinessUser::HasManager() const
{
	return nullptr != m_Manager && !m_Manager->m_Id.IsEmpty();
}

void BusinessUser::AddDirectReport(const BusinessUser& p_DR)
{
	BusinessUser* matchingDR = nullptr;
	for (auto findIt = m_DirectReports.begin(); matchingDR == nullptr && findIt != m_DirectReports.end(); findIt++)
		if ((*findIt)->GetID() == p_DR.GetID())
			matchingDR = (*findIt).get();

	if (nullptr != matchingDR)
	{
		// integrate DRs from p_DR
		for (const auto& DR : p_DR.GetDirectReports())
			matchingDR->AddDirectReport(*DR);
	}
	else
	{
		m_DirectReports.emplace_back(std::make_unique<BusinessUser>(p_DR));
	}
}

bool BusinessUser::IntegrateIntoManagerHierarchy(BusinessUser& p_User)
{
	bool integratedRV = false;
	
	BusinessUser& userToIntegrate = *p_User.GetManagerTop();

	if (userToIntegrate.GetID() == GetID())
	{
		for (auto& dr : userToIntegrate.GetDirectReports())
			AddDirectReport(*dr);
		integratedRV = true;
	}
	else
	{
		for (auto findIt = m_DirectReports.begin(); !integratedRV && findIt != m_DirectReports.end(); findIt++)
			integratedRV = (*findIt)->IntegrateIntoManagerHierarchy(userToIntegrate);
	}

	return integratedRV;
}

const vector<std::unique_ptr<BusinessUser>>& BusinessUser::GetDirectReports() const
{
	return m_DirectReports;
}

bool BusinessUser::HasDirecReports() const
{
	return !GetDirectReports().empty();
}
#endif

void BusinessUser::SetManagerID(const boost::YOpt<PooledString>& val)
{
	m_ManagerID = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetManagerID() const
{
	return m_ManagerID;
}

void BusinessUser::SetManagerError(const boost::YOpt<SapioError>& p_Val)
{
	m_ManagerError = p_Val;
}

const boost::YOpt<SapioError>& BusinessUser::GetManagerError() const
{
	return m_ManagerError;
}

const boost::YOpt<SapioError>& BusinessUser::GetDriveError() const
{
	return m_DriveError;
}

void BusinessUser::SetDriveError(const boost::YOpt<SapioError>& p_Val)
{
	m_DriveError = p_Val;
}

const boost::YOpt<PooledString>& BusinessUser::GetAboutMe() const 
{ 
	return m_AboutMe; 
}

void BusinessUser::SetAboutMe(const boost::YOpt<PooledString>& val) 
{ 
	m_AboutMe = val; 
}

const boost::YOpt<bool>& BusinessUser::GetAccountEnabled() const 
{ 
	return m_AccountEnabled; 
}

void BusinessUser::SetAccountEnabled(const boost::YOpt<bool>& val) 
{ 
	m_AccountEnabled = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetAgeGroup() const
{
	return m_AgeGroup;
}

void BusinessUser::SetAgeGroup(const boost::YOpt<PooledString>& val)
{
	m_AgeGroup = val;
}

const boost::YOpt<vector<BusinessAssignedLicense>>& BusinessUser::GetAssignedLicenses() const
{ 
	return m_AssignedLicenses;
}

boost::YOpt<vector<BusinessAssignedLicense>>& BusinessUser::GetAssignedLicenses()
{
	return m_AssignedLicenses;
}

void BusinessUser::SetAssignedLicenses(const vector<BusinessAssignedLicense>& val)
{ 
	m_AssignedLicenses = val; 
}

void BusinessUser::SetAssignedLicenses(const vector<AssignedLicense>& val)
{
	if (m_AssignedLicenses)
		m_AssignedLicenses->clear();
	else
		m_AssignedLicenses.emplace();
	m_AssignedLicenses->reserve(val.size());
	for (const auto& lic : val)
		m_AssignedLicenses->push_back(lic);
}

void BusinessUser::CopyAssignedLicensesTo(vector<AssignedLicense>& val) const
{
	val.clear();
	if (m_AssignedLicenses)
	{
		for (auto& lic : *m_AssignedLicenses)
			val.emplace_back(lic.ToAssignedLicense());
	}
}

const boost::YOpt<vector<AssignedPlan>>& BusinessUser::GetAssignedPlans() const
{ 
	return m_AssignedPlans; 
}

void BusinessUser::SetAssignedPlans(const vector<AssignedPlan>& val) 
{ 
	m_AssignedPlans = val; 
}

const boost::YOpt<YTimeDate>& BusinessUser::GetBirthday() const 
{ 
	return m_Birthday; 
}

void BusinessUser::SetBirthday(const boost::YOpt<YTimeDate>& val)
{
	m_Birthday = val;
	if (m_Birthday)
		m_Birthday->SetHasTime(false);
}

const boost::YOpt<PooledString>& BusinessUser::GetBusinessPhone() const
{
	if (m_BusinessPhones && !m_BusinessPhones->empty())
		m_LastGetBusinessPhone = (*m_BusinessPhones)[0];
	else
		m_LastGetBusinessPhone.reset();

	return m_LastGetBusinessPhone;
}

void BusinessUser::SetBusinessPhone(const boost::YOpt<PooledString>& val)
{
	if (val)
	{
		if (!m_BusinessPhones)
			m_BusinessPhones.emplace();

		if (m_BusinessPhones->empty())
			m_BusinessPhones->push_back(*val);
		else
			(*m_BusinessPhones)[0] = *val;
	}
	else if (m_BusinessPhones)
	{
		m_BusinessPhones->clear();
	}
}

const boost::YOpt<vector<PooledString>>& BusinessUser::GetBusinessPhones() const
{ 
	return m_BusinessPhones; 
}

void BusinessUser::SetBusinessPhones(const vector<PooledString>& val) 
{ 
	m_BusinessPhones = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetCity() const 
{ 
	return m_City; 
}

void BusinessUser::SetCity(const boost::YOpt<PooledString>& val) 
{ 
	m_City = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetConsentProvidedForMinor() const
{
	return m_ConsentProvidedForMinor;
}

void BusinessUser::SetConsentProvidedForMinor(const boost::YOpt<PooledString>& val)
{
	m_ConsentProvidedForMinor = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetCountry() const 
{ 
	return m_Country; 
}

void BusinessUser::SetCountry(const boost::YOpt<PooledString>& val) 
{ 
	m_Country = val; 
}

const boost::YOpt<YTimeDate>& BusinessUser::GetCreatedDateTime() const
{
	return m_CreatedDateTime;
}

void BusinessUser::SetCreatedDateTime(const boost::YOpt<YTimeDate>& val)
{
	m_CreatedDateTime = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetDepartment() const 
{ 
	return m_Department; 
}

void BusinessUser::SetDepartment(const boost::YOpt<PooledString>& val) 
{ 
	m_Department = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetDisplayName() const
{
	return m_DisplayName;
}

void BusinessUser::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}

const boost::YOpt<PooledString>& BusinessUser::GetGivenName() const 
{ 
	return m_GivenName; 
}

void BusinessUser::SetGivenName(const boost::YOpt<PooledString>& val) 
{ 
	m_GivenName = val; 
}

const boost::YOpt<YTimeDate>& BusinessUser::GetHireDate() const 
{ 
	return m_HireDate; 
}

void BusinessUser::SetHireDate(const boost::YOpt<YTimeDate>& val)
{
	m_HireDate = val;
	if (m_HireDate)
		m_HireDate->SetHasTime(false);
}

const boost::YOpt<vector<PooledString>>& BusinessUser::GetInterests() const 
{ 
	return m_Interests; 
}

void BusinessUser::SetInterests(const boost::YOpt<vector<PooledString>>& val) 
{ 
	m_Interests = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetJobTitle() const 
{ 
	return m_JobTitle; 
}

void BusinessUser::SetJobTitle(const boost::YOpt<PooledString>& val) 
{ 
	m_JobTitle = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetLegalAgeGroupClassification() const
{
	return m_LegalAgeGroupClassification;
}

void BusinessUser::SetLegalAgeGroupClassification(const boost::YOpt<PooledString>& val)
{
	m_LegalAgeGroupClassification = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetMail() const 
{ 
	return m_Mail; 
}

void BusinessUser::SetMail(const boost::YOpt<PooledString>& val) 
{ 
	m_Mail = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetMailNickname() const 
{ 
	return m_MailNickname; 
}

void BusinessUser::SetMailNickname(const boost::YOpt<PooledString>& val) 
{ 
	m_MailNickname = val; 
}

const boost::YOpt<vector<PooledString>>& BusinessUser::GetMailboxCanSendOnBehalf() const
{
	return m_MailboxCanSendOnBehalf;
}

void BusinessUser::SetMailboxCanSendOnBehalf(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_MailboxCanSendOnBehalf = p_Val;
}

const boost::YOpt<vector<PooledString>>& BusinessUser::GetMailboxCanSendAsUser() const
{
	return m_MailboxCanSendAsUser;
}

void BusinessUser::SetMailboxCanSendAsUser(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_MailboxCanSendAsUser = p_Val;
}

const boost::YOpt<BusinessDrive>& BusinessUser::GetDrive() const
{
	return m_Drive;
}

void BusinessUser::SetDrive(const boost::YOpt<BusinessDrive>& p_Val)
{
	m_Drive = p_Val;
}

const boost::YOpt<MailboxSettings>& BusinessUser::GetMailboxSettings() const 
{ 
	return m_MailboxSettings; 
}

void BusinessUser::SetMailboxSettings(const boost::YOpt<MailboxSettings>& val) 
{ 
	ASSERT(!m_MailboxSettingsError || !val);
	m_MailboxSettings = val; 
}

const boost::YOpt<SapioError>& BusinessUser::GetMailboxSettingsError() const
{
	return m_MailboxSettingsError;
}

void BusinessUser::SetMailboxSettingsError(const boost::YOpt<SapioError>& p_Val)
{
	ASSERT(!m_MailboxSettings);
	m_MailboxSettingsError = p_Val;
}

const boost::YOpt<PooledString>& BusinessUser::GetMobilePhone() const 
{ 
	return m_MobilePhone; 
}

void BusinessUser::SetMobilePhone(const boost::YOpt<PooledString>& val) 
{ 
	m_MobilePhone = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetMySite() const 
{ 
	return m_MySite; 
}

void BusinessUser::SetMySite(const boost::YOpt<PooledString>& val) 
{ 
	m_MySite = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetOfficeLocation() const 
{ 
	return m_OfficeLocation; 
}

void BusinessUser::SetOfficeLocation(const boost::YOpt<PooledString>& val) 
{
	m_OfficeLocation = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetOnPremisesDomainName() const
{
	return m_OnPremisesDomainName;
}

void BusinessUser::SetOnPremisesDomainName(const boost::YOpt<PooledString>& val)
{
	m_OnPremisesDomainName = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetOnPremisesImmutableId() const 
{ 
	return m_OnPremisesImmutableId; 
}

void BusinessUser::SetOnPremisesImmutableId(const boost::YOpt<PooledString>& val) 
{ 
	m_OnPremisesImmutableId = val; 
}

const boost::YOpt<YTimeDate>& BusinessUser::GetOnPremisesLastSyncDateTime() const
{ 
	return m_OnPremisesLastSyncDateTime; 
}

void BusinessUser::SetOnPremisesLastSyncDateTime(const boost::YOpt<YTimeDate>& val)
{ 
	m_OnPremisesLastSyncDateTime = val; 
}

const boost::YOpt<vector<OnPremisesProvisioningError>>& BusinessUser::GetOnPremisesProvisioningErrors() const
{
	return m_OnPremisesProvisioningErrors;
}

void BusinessUser::SetOnPremisesProvisioningErrors(const boost::YOpt<vector<OnPremisesProvisioningError>>& val)
{
	m_OnPremisesProvisioningErrors = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetOnPremisesSamAccountName() const
{
	return m_OnPremisesSamAccountName;
}

void BusinessUser::SetOnPremisesSamAccountName(const boost::YOpt<PooledString>& val)
{
	m_OnPremisesSamAccountName = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetOnPremisesSecurityIdentifier() const 
{ 
	return m_OnPremisesSecurityIdentifier; 
}

void BusinessUser::SetOnPremisesSecurityIdentifier(const boost::YOpt<PooledString>& val) 
{ 
	m_OnPremisesSecurityIdentifier = val; 
}

const boost::YOpt<bool>& BusinessUser::GetOnPremisesSyncEnabled() const 
{ 
	return m_OnPremisesSyncEnabled; 
}

void BusinessUser::SetOnPremisesSyncEnabled(const boost::YOpt<bool>& val) 
{ 
	m_OnPremisesSyncEnabled = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetOnPremisesUserPrincipalName() const
{
	return m_OnPremisesUserPrincipalName;
}

void BusinessUser::SetOnPremisesUserPrincipalName(const boost::YOpt<PooledString>& val)
{
	m_OnPremisesUserPrincipalName = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetPasswordPolicies() const 
{ 
	return m_PasswordPolicies; 
}

void BusinessUser::SetPasswordPolicies(const boost::YOpt<PooledString>& val) 
{ 
	m_PasswordPolicies = val; 
}

void BusinessUser::SetPasswordProfile(const boost::YOpt<PasswordProfile>& val)
{ 
	if (boost::none != val)
	{
		SetPassword(val.get().Password);
		SetForceChangePassword(val.get().ForceChangePasswordNextSignIn);
		SetForceChangePasswordWithMfa(val.get().ForceChangePasswordNextSignInWithMfa);
	}
	else
	{
		SetPassword(boost::none);
		SetForceChangePassword(boost::none);
		SetForceChangePasswordWithMfa(boost::none);
	}
}

const boost::YOpt<PooledString>& BusinessUser::GetPassword() const
{
	return m_Password;
}

void BusinessUser::SetPassword(const boost::YOpt<PooledString>& val)
{
	m_Password = val;
}

const boost::YOpt<bool>& BusinessUser::GetForceChangePassword() const
{
	return m_ForceChangePassword;
}

void BusinessUser::SetForceChangePassword(const boost::YOpt<bool>& p_Val)
{
	m_ForceChangePassword = p_Val;
}

const boost::YOpt<bool>& BusinessUser::GetForceChangePasswordWithMfa() const
{
	return m_ForceChangePasswordWithMfa;
}

void BusinessUser::SetForceChangePasswordWithMfa(const boost::YOpt<bool>& p_Val)
{
	m_ForceChangePasswordWithMfa = p_Val;
}

const boost::YOpt<vector<PooledString>>& BusinessUser::GetPastProjects() const 
{ 
	return m_PastProjects; 
}

void BusinessUser::SetPastProjects(const boost::YOpt<vector<PooledString>>& val) 
{ 
	m_PastProjects = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetPostalCode() const 
{ 
	return m_PostalCode; 
}

void BusinessUser::SetPostalCode(const boost::YOpt<PooledString>& val) 
{ 
	m_PostalCode = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetPreferredLanguage() const 
{ 
	return m_PreferredLanguage;
}

void BusinessUser::SetPreferredLanguage(const boost::YOpt<PooledString>& val) 
{
	m_PreferredLanguage = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetPreferredName() const 
{ 
	return m_PreferredName; 
}

void BusinessUser::SetPreferredName(const boost::YOpt<PooledString>& val) 
{ 
	m_PreferredName = val; 
}

const boost::YOpt<vector<ProvisionedPlan>>& BusinessUser::GetProvisionedPlans() const
{ 
	return m_ProvisionedPlans; 
}

void BusinessUser::SetProvisionedPlans(const vector<ProvisionedPlan>& val) 
{ 
	m_ProvisionedPlans = val; 
}

const boost::YOpt<vector<PooledString>>& BusinessUser::GetProxyAddresses() const
{ 
	return m_ProxyAddresses; 
}

void BusinessUser::SetProxyAddresses(const vector<PooledString>& val) 
{ 
	m_ProxyAddresses = val; 
}

const boost::YOpt<YTimeDate>& BusinessUser::GetSignInSessionsValidFromDateTime() const
{
	return m_SignInSessionsValidFromDateTime;
}

void BusinessUser::SetSignInSessionsValidFromDateTime(const boost::YOpt<YTimeDate>& val)
{
	m_SignInSessionsValidFromDateTime = val;
}

const boost::YOpt<vector<PooledString>>& BusinessUser::GetResponsibilities() const 
{ 
	return m_Responsibilities; 
}

void BusinessUser::SetResponsibilities(const boost::YOpt<vector<PooledString>>& val) 
{ 
	m_Responsibilities = val; 
}

const boost::YOpt<vector<PooledString>>& BusinessUser::GetSchools() const 
{ 
	return m_Schools; 
}

void BusinessUser::SetSchools(const boost::YOpt<vector<PooledString>>& val) 
{ 
	m_Schools = val; 
}

const boost::YOpt<vector<PooledString>>& BusinessUser::GetSkills() const 
{ 
	return m_Skills; 
}

void BusinessUser::SetSkills(const boost::YOpt<vector<PooledString>>& val) 
{ 
	m_Skills = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetState() const 
{ 
	return m_State; 
}

void BusinessUser::SetState(const boost::YOpt<PooledString>& val) 
{ 
	m_State = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetStreetAddress() const 
{ 
	return m_StreetAddress; 
}

void BusinessUser::SetStreetAddress(const boost::YOpt<PooledString>& val) 
{ 
	m_StreetAddress = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetSurname() const 
{ 
	return m_Surname; 
}

void BusinessUser::SetSurname(const boost::YOpt<PooledString>& val) 
{ 
	m_Surname = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetUsageLocation() const 
{ 
	return m_UsageLocation; 
}

void BusinessUser::SetUsageLocation(const boost::YOpt<PooledString>& val) 
{
	m_UsageLocation = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetUserPrincipalName() const
{ 
	return m_UserPrincipalName; 
}

void BusinessUser::SetUserPrincipalName(const boost::YOpt<PooledString>& val)
{ 
	m_UserPrincipalName = val; 
}

const boost::YOpt<PooledString>& BusinessUser::GetUserType() const 
{ 
	return m_UserType; 
}

void BusinessUser::SetUserType(const boost::YOpt<PooledString>& val) 
{ 
	m_UserType = val; 
}

const boost::YOpt<vector<PooledString>>& BusinessUser::GetImAddresses() const
{
    return m_ImAddresses;
}

void BusinessUser::SetImAddresses(const boost::YOpt<vector<PooledString>>& p_Val)
{
    m_ImAddresses = p_Val;
}

const boost::YOpt<PooledString>& BusinessUser::GetCompanyName() const
{
    return m_CompanyName;
}

void BusinessUser::SetCompanyName(const boost::YOpt<PooledString>& p_Val)
{
    m_CompanyName = p_Val;
}

void BusinessUser::SetUserPrincipalNameWithoutDomain(const boost::YOpt<PooledString>& val)
{
	if (m_UserPrincipalName)
	{
		auto strings = Str::explodeIntoVector(*m_UserPrincipalName, PooledString(_YTEXT("@")), true, false);
		m_UserPrincipalName = *val + _YTEXT("@") + strings[1];
	}
	else
	{
		m_UserPrincipalName = *val + _YTEXT("@");
	}
}

void BusinessUser::SetUserPrincipalNameDomain(const boost::YOpt<PooledString>& val)
{
	if (m_UserPrincipalName)
	{
		auto strings = Str::explodeIntoVector(*m_UserPrincipalName, PooledString(_YTEXT("@")), true, false);
		m_UserPrincipalName = strings[0] + wstring(_YTEXT("@")) + wstring(*val);
	}
	else
	{
		m_UserPrincipalName = _YTEXT("@") + (wstring)*val;
	}
}

const boost::YOpt<vector<PooledString>>& BusinessUser::GetOtherMails() const
{
	return m_OtherMails;
}

void BusinessUser::SetOtherMails(const boost::YOpt<vector<PooledString>>& val)
{
	m_OtherMails = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetPreferredDataLocation() const
{
	return m_PreferredDataLocation;
}

void BusinessUser::SetPreferredDataLocation(const boost::YOpt<PooledString>& val)
{
	m_PreferredDataLocation = val;
}

const boost::YOpt<bool>& BusinessUser::GetShowInAddressList() const
{
	return m_ShowInAddressList;
}

void BusinessUser::SetShowInAddressList(const boost::YOpt<bool>& val)
{
	m_ShowInAddressList = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetExternalUserStatus() const
{
	return m_ExternalUserStatus;
}

void BusinessUser::SetExternalUserStatus(const boost::YOpt<PooledString>& val)
{
	m_ExternalUserStatus = val;
}

const boost::YOpt<YTimeDate>& BusinessUser::GetExternalUserStatusChangedOn() const
{
	return m_ExternalUserStatusChangedOn;
}

void BusinessUser::SetExternalUserStatusChangedOn(const boost::YOpt<YTimeDate>& val)
{
	m_ExternalUserStatusChangedOn = val;
}

const boost::YOpt<OnPremisesExtensionAttributes>& BusinessUser::GetOnPremisesExtensionAttributes() const
{
	return m_OnPremisesExtensionAttributes;
}

void BusinessUser::SetOnPremisesExtensionAttributes(const boost::YOpt<OnPremisesExtensionAttributes>& val)
{
	m_OnPremisesExtensionAttributes = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetEmployeeID() const
{
	return m_EmployeeID;
}

void BusinessUser::SetEmployeeID(const boost::YOpt<PooledString>& val)
{
	m_EmployeeID = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetFaxNumber() const
{
	return m_FaxNumber;
}

void BusinessUser::SetFaxNumber(const boost::YOpt<PooledString>& val)
{
	m_FaxNumber = val;
}

const boost::YOpt<bool>& BusinessUser::GetIsResourceAccount() const
{
	return m_IsResourceAccount;
}

void BusinessUser::SetIsResourceAccount(const boost::YOpt<bool>& val)
{
	m_IsResourceAccount = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetOnPremisesDistinguishedName() const
{
	return m_OnPremisesDistinguishedName;
}

void BusinessUser::SetOnPremisesDistinguishedName(const boost::YOpt<PooledString>& val)
{
	m_OnPremisesDistinguishedName = val;
}

const boost::YOpt<PooledString>& BusinessUser::GetCreationType() const
{
	return m_CreationType;
}

void BusinessUser::SetCreationType(const boost::YOpt<PooledString>& val)
{
	m_CreationType = val;
}

const boost::YOpt<vector<ObjectIdentity>>& BusinessUser::GetIdentities() const
{
	return m_Identities;
}

void BusinessUser::SetIdentities(const boost::YOpt<vector<ObjectIdentity>>& val)
{
	m_Identities = val;
}

const boost::YOpt<YTimeDate>& BusinessUser::GetLastPasswordChangeDateTime() const
{
	return m_LastPasswordChangeDateTime;
}

void BusinessUser::SetLastPasswordChangeDateTime(const boost::YOpt<YTimeDate>& val)
{
	m_LastPasswordChangeDateTime = val;
}

const boost::YOpt<vector<LicenseAssignmentState>>& BusinessUser::GetLicenseAssignmentStates() const
{
	return m_LicenseAssignmentStates;
}

boost::YOpt<vector<LicenseAssignmentState>>& BusinessUser::GetLicenseAssignmentStates()
{
	return m_LicenseAssignmentStates;
}

void BusinessUser::SetLicenseAssignmentStates(const boost::YOpt<vector<LicenseAssignmentState>>& val)
{
	m_LicenseAssignmentStates = val;
}

const boost::YOpt<YTimeDate>& BusinessUser::GetRefresTokenValidFromDateTime() const
{
	return m_RefresTokenValidFromDateTime;
}

void BusinessUser::SetRefresTokenValidFromDateTime(const boost::YOpt<YTimeDate>& val)
{
	m_RefresTokenValidFromDateTime = val;
}

const boost::YOpt<OnPremiseUser>& BusinessUser::GetOnPremiseUser() const
{
	return m_OnPremUser;
}

boost::YOpt<OnPremiseUser>& BusinessUser::GetOnPremiseUser()
{
	return m_OnPremUser;
}

void BusinessUser::SetDeletedDateTime(const boost::YOpt<YTimeDate>& p_DelDate)
{
	m_DeletedDateTime = p_DelDate;
}

void BusinessUser::SetDeletedDateTime(const boost::YOpt<PooledString>& p_DelDate)
{
	if (p_DelDate.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_DelDate.get(), td))
			SetDeletedDateTime(td);
	}
}

const boost::YOpt<YTimeDate>& BusinessUser::GetDeletedDateTime() const
{
	return m_DeletedDateTime;
}

bool BusinessUser::IsFromRecycleBin() const
{
	return m_DeletedDateTime.is_initialized();
}

const boost::YOpt<PooledString>& BusinessUser::GetArchiveFolderName() const
{
    return m_ArchiveFolderName;
}

void BusinessUser::SetArchiveFolderName(const boost::YOpt<PooledString>& p_Val)
{
	ASSERT(!m_ArchiveFolderNameError);
    m_ArchiveFolderName = p_Val;
}

void BusinessUser::SetArchiveFolderNameError(const boost::YOpt<SapioError>& p_Error)
{
	ASSERT(!m_ArchiveFolderName);
	m_ArchiveFolderNameError = p_Error;
}

const boost::YOpt<SapioError>& BusinessUser::GetArchiveFolderNameError() const
{
	return m_ArchiveFolderNameError;
}

pplx::task<vector<HttpResultWithError>> BusinessUser::SendEditRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	return YSafeCreateTaskMutable([that = *this, p_Sapio365Session, p_TaskData]() mutable
	{
		vector<HttpResultWithError> results;

		if (that.GetShouldRevokeAccess())
		{
			auto revokeUserAccessRequester = std::make_shared<RevokeUserAccessRequester>(that.GetID(), Util::GetDisplayNameOrId(that));
			auto task = safeTaskCall(revokeUserAccessRequester->Send(p_Sapio365Session, p_TaskData).Then([revokeUserAccessRequester]()
				{
					return HttpResultWithError(revokeUserAccessRequester->GetResult().GetResult());
				}), p_Sapio365Session->GetMSGraphSession(Sapio365Session::APP_SESSION), Util::WriteErrorHandler, p_TaskData);

			results.push_back(task.get());
		}

		{
			User user;
			auto updatableProperties = that.GetUpdatableProperties<User, BusinessUser>(user, { { { MetadataKeys::ReadOnly, false }, { MetadataKeys::BetaAPI, false } } }, { OnList, 0 });

			boost::YOpt<bool> forceMFA;
			if (user.PasswordProfile && user.PasswordProfile->Password && user.PasswordProfile->ForceChangePasswordNextSignInWithMfa)
			{
				if (user.PasswordProfile)
				{
					forceMFA = user.PasswordProfile->ForceChangePasswordNextSignInWithMfa;
					user.PasswordProfile->ForceChangePasswordNextSignInWithMfa.reset();
				}
			}

			if (!updatableProperties.empty())
			{
				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
				auto task = safeTaskCall(user.Update(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, updatableProperties).Then([](const RestResultInfo& p_Result) {
					return HttpResultWithError(p_Result, boost::none);
					}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
				results.push_back(task.get());
			}

			if (forceMFA)
			{
				// Do separate request for "force MFA", otherwise it's not taken into account...

				auto prop = rttr::instance(user).get_derived_type().get_property(O365_USER_PASSWORDPROFILE);

				User user2;
				user2.m_Id = user.m_Id;
				user2.PasswordProfile.emplace();
				user2.PasswordProfile->ForceChangePasswordNextSignInWithMfa = forceMFA;

				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
				auto task = safeTaskCall(user2.Update(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, { prop }).Then([](const RestResultInfo& p_Result) {
					return HttpResultWithError(p_Result, boost::none);
				}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
				results.push_back(task.get());
			}
		}

		// Do separate request for load more properties
		{
			User user;
			const auto updatableProperties = that.GetUpdatableProperties<User, BusinessUser>(user, { { { MetadataKeys::ReadOnly, false }, { MetadataKeys::BetaAPI, false } } }, { OnSync, OnList });
			if (!updatableProperties.empty())
			{
				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
				auto task = safeTaskCall(user.Update(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, updatableProperties).Then([](const RestResultInfo& p_Result) {
					return HttpResultWithError(p_Result, boost::none);
				}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
				results.push_back(task.get());
			}
		}

		// Do separate request for beta properties
		{
			User user;
			const auto updatableProperties = that.GetUpdatableProperties<User, BusinessUser>(user, { { { MetadataKeys::ReadOnly, false }, { MetadataKeys::BetaAPI, true } } }, { OnSync, OnList });
			if (!updatableProperties.empty())
			{
				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
				auto task = safeTaskCall(user.Update(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, updatableProperties).Then([](const RestResultInfo& p_Result) {
					return HttpResultWithError(p_Result, boost::none);
				}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
				results.push_back(task.get());
			}
		}

		// Manager
		if (that.GetManagerID())
		{
			if (that.GetManagerID()->IsEmpty())
			{
				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
				auto task = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->RemoveUserManager(that.GetID(), httpLogger, p_TaskData).Then([](const RestResultInfo& p_Result) {
					return HttpResultWithError(p_Result, boost::none);
				}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
				results.push_back(task.get());
			}
			else
			{
				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
				auto task = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->SetUserManager(that.GetID(), *that.GetManagerID(), httpLogger, p_TaskData).Then([](const RestResultInfo& p_Result) {
					return HttpResultWithError(p_Result, boost::none);
					}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
				results.push_back(task.get());
			}
		}

		return results;
	});
}

TaskWrapper<HttpResultWithError> BusinessUser::SendCreateRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	User user;
	const auto creatableProperties = GetUpdatableProperties<User, BusinessUser>(user, { { { MetadataKeys::ValidForCreation, true } } }, {});
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(user.Create(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, creatableProperties).Then([](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessUser::SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	User user = BusinessUser::ToUser(*this);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
    return safeTaskCall(user.Delete(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData).Then([user, p_Sapio365Session](const RestResultInfo& p_Result) {
		if (204 == p_Result.Response.status_code())
			p_Sapio365Session->GetGraphCache().Remove(user);
        return HttpResultWithError(p_Result, boost::none);
    }), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessUser::SendRestoreRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	User user = BusinessUser::ToUser(*this);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(user.Restore(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData).Then([user, p_Sapio365Session](const RestResultInfo& p_Result) {
		if (200 == p_Result.Response.status_code())
			p_Sapio365Session->GetGraphCache().RemoveDeletedUser(user);
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessUser::SendHardDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	User user = BusinessUser::ToUser(*this);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(user.HardDelete(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData).Then([user, p_Sapio365Session](const RestResultInfo& p_Result) {
		if (204 == p_Result.Response.status_code())
			p_Sapio365Session->GetGraphCache().RemoveDeletedUser(user);
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

namespace
{
	static boost::YOpt<PooledString> g_emptyOptPooledString;
}

#define DefAccessOnPremisesExtensionAttribute(_index) \
const boost::YOpt<PooledString>& BusinessUser::GetOnPremisesExtensionAttribute##_index() const \
{ \
	if (m_OnPremisesExtensionAttributes) \
		return m_OnPremisesExtensionAttributes->m_ExtensionAttribute##_index; \
	return g_emptyOptPooledString; \
} \
void BusinessUser::SetOnPremisesExtensionAttribute##_index(const boost::YOpt<PooledString>& val) \
{ \
	ASSERT(m_OnPremisesExtensionAttributes); /*Is this useful?*/ \
	if (!m_OnPremisesExtensionAttributes) \
		m_OnPremisesExtensionAttributes.emplace(); \
	m_OnPremisesExtensionAttributes->m_ExtensionAttribute##_index = val; \
} \

DefAccessOnPremisesExtensionAttribute(1)
DefAccessOnPremisesExtensionAttribute(2)
DefAccessOnPremisesExtensionAttribute(3)
DefAccessOnPremisesExtensionAttribute(4)
DefAccessOnPremisesExtensionAttribute(5)
DefAccessOnPremisesExtensionAttribute(6)
DefAccessOnPremisesExtensionAttribute(7)
DefAccessOnPremisesExtensionAttribute(8)
DefAccessOnPremisesExtensionAttribute(9)
DefAccessOnPremisesExtensionAttribute(10)
DefAccessOnPremisesExtensionAttribute(11)
DefAccessOnPremisesExtensionAttribute(12)
DefAccessOnPremisesExtensionAttribute(13)
DefAccessOnPremisesExtensionAttribute(14)
DefAccessOnPremisesExtensionAttribute(15)

#undef DefAccessOnPremisesExtensionAttribute