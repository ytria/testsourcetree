#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessGroupSetting.h"

class GroupSettingDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessGroupSetting>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

