#pragma once

#include "PooledString.h"
#include "RoleDelegationUtil.h"

class Sapio365Session;
class CachedObject
{
public:
	CachedObject();
	CachedObject(const wstring& p_ID);

	const PooledString& GetID() const;

	void	SetRBACDelegationID(const int64_t p_RoleDelegationID);
	int64_t GetRBACDelegationID() const;
	bool	HasPrivilege(const RoleDelegationUtil::RBAC_Privilege p_Right, std::shared_ptr<Sapio365Session> p_Session) const;
	bool	IsRBACAuthorized() const;

protected:
	PooledString m_Id;

private:
	int64_t m_RBAC_DelegationID;// ID of delegation whose scope is valid for this object: -1 means no active Role Delegation, 0 means doesn't match active Role Delegation scope
	
	RTTR_ENABLE()
	RTTR_REGISTRATION_FRIEND
};