#include "MSGraphUtil.h"

#include "includes_WIN.h"
#include "includes_STL.h"

#include "MFCUtil.h"
#include "GraphPaginatorRequester.h"

string_t Rest::GetSelectQuery(const vector<rttr::property>& wantedProperties)
{
    string_t selectQuery;
    bool isFirst = true;

    for (const auto& property : wantedProperties)
    {
        if (isFirst)
            isFirst = false;
        else
            selectQuery += L",";
        selectQuery += MFCUtil::convertASCII_to_UNICODE(property.get_name().to_string());
    }
    return selectQuery;
}

string_t Rest::GetSelectQuery(const vector<PooledString>& wantedProperties)
{
	string_t selectQuery;
	bool isFirst = true;

	for (const auto& property : wantedProperties)
	{
		if (isFirst)
			isFirst = false;
		else
			selectQuery += L",";
		selectQuery += property;
	}
	return selectQuery;
}

boost::YOpt<rttr::type> Rest::GetElementType(const web::json::value& data)
{
    string_t typeNode = _YTEXT("@odata.type");

    if (data.has_field(typeNode))
    {
        static const size_t partToRemove = std::string("#").size();
        std::string finalType = std::string(MFCUtil::convertUNICODE_to_ASCII(data.as_object().at(typeNode).as_string())).replace(0, partToRemove, "");
        if (!finalType.empty())
            return rttr::type::get_by_name(finalType);
    }

    return boost::none;
}

MsGraphPaginator Rest::CreateDefaultPaginator(const std::shared_ptr<MSGraphSession>& p_GraphSession, const std::shared_ptr<IDatedJsonDeserializer>& p_Deserializer, const YtriaTaskData& p_TaskData)
{
	auto defaultRequester = std::make_shared<GraphPaginatorRequester>(p_GraphSession, p_Deserializer, nullptr, p_TaskData);
	return MsGraphPaginator(uri.to_uri(), defaultRequester, p_TaskData);
}


