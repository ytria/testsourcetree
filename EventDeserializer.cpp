#include "EventDeserializer.h"

#include "Attendee.h"
#include "AttendeeDeserializer.h"
#include "DateTimeTimeZoneDeserializer.h"
#include "ItemBodyDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "LocationDeserializer.h"
#include "MsGraphFieldNames.h"
#include "PatternedRecurrenceDeserializer.h"
#include "RecipientDeserializer.h"
#include "ResponseStatusDeserializer.h"

void EventDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        ListDeserializer<Attendee, AttendeeDeserializer> attDeserializer;
        if (JsonSerializeUtil::DeserializeAny(attDeserializer, _YTEXT("attendees"), p_Object))
            m_Data.SetAttendees(attDeserializer.GetData());
    }

    {
        ItemBodyDeserializer ibd;
        if (JsonSerializeUtil::DeserializeAny(ibd, _YTEXT("body"), p_Object))
            m_Data.SetBody(ibd.GetData());
    }
    
	JsonSerializeUtil::DeserializeString(_YTEXT("bodyPreview"), m_Data.m_BodyPreview, p_Object);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("categories"), p_Object))
            m_Data.SetCategories(lsd.GetData());
    }
    JsonSerializeUtil::DeserializeString(_YTEXT("changeKey"), m_Data.m_ChangeKey, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.m_CreatedDateTime, p_Object);

    {
        DateTimeTimeZoneDeserializer dttzd;
        if (JsonSerializeUtil::DeserializeAny(dttzd, _YTEXT("end"), p_Object))
            m_Data.SetEnd(dttzd.GetData());
    }

    JsonSerializeUtil::DeserializeBool(_YTEXT("hasAttachments"), m_Data.m_HasAttachments, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("iCalUId"), m_Data.m_ICalUId, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT(O365_ID), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeString(_YTEXT("importance"), m_Data.m_Importance, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isAllDay"), m_Data.m_IsAllDay, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isCancelled"), m_Data.m_IsCancelled, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isOrganizer"), m_Data.m_IsOrganizer, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isReminderOn"), m_Data.m_IsReminderOn, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.m_LastModifiedDateTime, p_Object);

    {
        LocationDeserializer ld;
        if (JsonSerializeUtil::DeserializeAny(ld, _YTEXT("location"), p_Object))
            m_Data.SetLocation(ld.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("onlineMeetingUrl"), m_Data.m_OnlineMeetingUrl, p_Object);

    {
        RecipientDeserializer rd;
        if (JsonSerializeUtil::DeserializeAny(rd, _YTEXT("organizer"), p_Object))
            m_Data.SetOrganizer(rd.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("originalEndTimeZone"), m_Data.m_OriginalEndTimeZone, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("originalStart"), m_Data.m_OriginalStart, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("originalStartTimeZone"), m_Data.m_OriginalStartTimeZone, p_Object);

    {
        PatternedRecurrenceDeserializer prd;
        if (JsonSerializeUtil::DeserializeAny(prd, _YTEXT("recurrence"), p_Object))
            m_Data.SetRecurrence(prd.GetData());
    }

    JsonSerializeUtil::DeserializeInt32(_YTEXT("reminderMinutesBeforeStart"), m_Data.m_ReminderMinutesBeforeStart, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("responseRequested"), m_Data.m_ResponseRequested, p_Object);

    {
        ResponseStatusDeserializer rsd;
        if (JsonSerializeUtil::DeserializeAny(rsd, _YTEXT("responseStatus"), p_Object))
            m_Data.SetResponseStatus(rsd.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("sensitivity"), m_Data.m_Sensitivity, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("seriesMasterId"), m_Data.m_SeriesMasterId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("showAs"), m_Data.m_ShowAs, p_Object);
    
    {
        DateTimeTimeZoneDeserializer dttzd;
        if (JsonSerializeUtil::DeserializeAny(dttzd, _YTEXT("start"), p_Object))
            m_Data.SetStart(dttzd.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("subject"), m_Data.m_Subject, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.m_Type, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("webLink"), m_Data.m_WebLink, p_Object);
}
