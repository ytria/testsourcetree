#include "MailboxPermissionsAccessRights.h"

const wstring MailboxPermissionsAccessRights::g_ChangeOwner = _YTEXT("ChangeOwner");
const wstring MailboxPermissionsAccessRights::g_ChangePermission = _YTEXT("ChangePermission");
const wstring MailboxPermissionsAccessRights::g_DeleteItem = _YTEXT("DeleteItem");
const wstring MailboxPermissionsAccessRights::g_ExternalAccount = _YTEXT("ExternalAccount");
const wstring MailboxPermissionsAccessRights::g_FullAccess = _YTEXT("FullAccess");
const wstring MailboxPermissionsAccessRights::g_ReadPermission = _YTEXT("ReadPermission");
