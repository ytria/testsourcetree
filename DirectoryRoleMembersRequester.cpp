#include "DirectoryRoleMembersRequester.h"

#include "BusinessGroup.h"
#include "BusinessUser.h"
#include "CachedUserPropertySet.h"
#include "MSGraphSession.h"
#include "MSGraphUtil.h"
#include "PaginatedRequestResults.h"
#include "Sapio365Session.h"
#include "UserDeserializer.h"
#include "UserDeserializerFactory.h"
#include "ValueListDeserializer.h"
#include "RbacRequiredPropertySet.h"
#include "MsGraphHttpRequestLogger.h"
#include "SessionIdentifier.h"

DirectoryRoleMembersRequester::DirectoryRoleMembersRequester(PooledString p_DirectoryRoleId, const std::shared_ptr<IRequestLogger>& p_Logger)
    :m_DirectoryRoleId(p_DirectoryRoleId),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> DirectoryRoleMembersRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	auto factory	= std::make_unique<UserDeserializerFactory>(p_Session);
    m_Deserializer	= std::make_shared<ValueListDeserializer<BusinessUser, UserDeserializer>>(std::move(factory));

	auto properties = CachedUserPropertySet().GetPropertySet();

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObjects(m_Deserializer
		, { _YTEXT("directoryRoles"), m_DirectoryRoleId, _YTEXT("members") }
		, { { _YTEXT("$select"), Rest::GetSelectQuery(properties) } }
		, false
		, m_Logger
		, httpLogger
		, p_TaskData);
}

TaskWrapper<void> DirectoryRoleMembersRequester::Send(std::shared_ptr<MSGraphSession> p_Session, const SessionIdentifier& p_Identifier, YtriaTaskData p_TaskData)
{
	auto factory = std::make_unique<UserDeserializerFactory>(nullptr);
	m_Deserializer = std::make_shared<ValueListDeserializer<BusinessUser, UserDeserializer>>(std::move(factory));
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Identifier);
	return p_Session->getObjects(m_Deserializer
		, { _YTEXT("directoryRoles"), m_DirectoryRoleId, _YTEXT("members") }
		, { { _YTEXT("$select"), Rest::GetSelectQuery(CachedUserPropertySet().GetPropertySet()) } }
		, false
		, m_Logger
		, httpLogger
		, p_TaskData);
}

const vector<BusinessUser>& DirectoryRoleMembersRequester::GetData() const
{
    return m_Deserializer->GetData();
}

vector<BusinessUser>& DirectoryRoleMembersRequester::GetData()
{
    return m_Deserializer->GetData();
}
