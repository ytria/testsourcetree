#pragma once

#include "GridFrameBase.h"
#include "GridApplications.h"
#include "Application.h"

class FrameApplications : public GridFrameBase
{
public:
	DECLARE_DYNAMIC(FrameApplications)
	FrameApplications(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
	virtual ~FrameApplications() = default;

	void BuildView(const vector<Application>& p_Applications, bool p_FullPurge, bool p_NoPurge);
	O365Grid& GetGrid() override;

	void RefreshSpecific(AutomationAction* p_CurrentAction) override;
	void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;
	bool HasApplyButton() override;

protected:
	virtual void createGrid() override;

	// returns false if no frame specific tab is needed.
	bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	AutomatedApp::AUTOMATIONSTATUS automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	GridApplications m_Grid;
};

