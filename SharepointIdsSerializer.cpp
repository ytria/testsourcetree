#include "SharepointIdsSerializer.h"

#include "JsonSerializeUtil.h"

SharepointIdsSerializer::SharepointIdsSerializer(const SharepointIds& p_SpIds)
    : m_SpIds(p_SpIds)
{
}

void SharepointIdsSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("listId"), m_SpIds.ListId, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("listItemId"), m_SpIds.ListItemId, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("listItemUniqueId"), m_SpIds.ListItemUniqueId, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("siteId"), m_SpIds.SiteId, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("siteUrl"), m_SpIds.SiteUrl, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("webId"), m_SpIds.WebId, obj);
}
