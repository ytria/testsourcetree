#pragma once

#include "IRequester.h"

#include "Team.h"
#include "TeamDeserializer.h"
#include "SingleRequestResult.h"
#include "IRequestLogger.h"

class TeamRequester : public IRequester
{
public:
	TeamRequester(PooledString p_TeamId, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const Team& GetData() const;

private:
	PooledString m_TeamId;

	shared_ptr<TeamDeserializer> m_Deserializer;
	shared_ptr<SingleRequestResult> m_Result;
	shared_ptr<IRequestLogger> m_Logger;
};

