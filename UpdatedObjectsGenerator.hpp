#include "UpdatedObjectsGenerator.h"

#include "BusinessGroup.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessUserGuest.h"
#include "BusinessUserUnknown.h"
#include "BusinessUser.h"
#include "CacheGrid.h"
#include "GridBackendRow.h"
#include "GridGroups.h"
#include "GridUsers.h"
#include "MsGraphFieldNames.h"
#include "YtriaFieldsConstants.h"

template<class BusinessType>
UpdatedObjectsGenerator<BusinessType>::UpdatedObjectsGenerator()
{
}

template<class BusinessType>
void UpdatedObjectsGenerator<BusinessType>::BuildUpdatedObjects(CacheGrid& p_Grid, bool p_FromSelection, uint32_t p_ActionFlags/* = UOGFlags::ACTION_MODIFY | UOGFlags::ACTION_CREATE | UOGFlags::ACTION_DELETE*/)
{
	TemporarilyImplodeNonGroupRowsAndExec(p_Grid
		, p_FromSelection ? TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS : TemporarilyImplodeNonGroupRowsAndExec::ALL_ROWS
		, [this, &p_Grid, p_FromSelection, p_ActionFlags](const bool /*p_HasRowsToReExplode*/)
		{
			ASSERT(m_ObjectsToUpdate.empty()); // Use either SetObjects or BuildUpdatedObjects, not both

			vector<GridBackendRow*> rows;
			if (p_FromSelection)
				p_Grid.GetSelectedNonGroupRows(rows);
			else
				p_Grid.GetAllNonGroupRows(rows);

			for (const auto& pRow : rows)
			{
				// FIXME: We should use GridModifications instead. Row status is not 100% guaranteed to be up-to-date...
				ASSERT(nullptr != pRow);
				if (nullptr != pRow)
				{
					if ((p_ActionFlags & UOGFlags::ACTION_MODIFY) == UOGFlags::ACTION_MODIFY && pRow->IsModified())
					{
						ASSERT(!pRow->IsError());
						BuildObjToModify(p_Grid, pRow);
					}
					else if ((p_ActionFlags & UOGFlags::ACTION_CREATE) == UOGFlags::ACTION_CREATE && pRow->IsCreated())
					{
						ASSERT(!pRow->IsError());
						BuildObjToCreate(p_Grid, pRow);
					}
					else if ((p_ActionFlags & UOGFlags::ACTION_DELETE) == UOGFlags::ACTION_DELETE && pRow->IsDeleted())
					{
						ASSERT(!pRow->IsError());
						BuildObjToDelete(p_Grid, pRow);
					}
				}
			}
		})();
}

template<class BusinessType>
void UpdatedObjectsGenerator<BusinessType>::BuildUpdatedObjectsRecycleBin(CacheGrid& p_Grid, bool p_FromSelection)
{
	TemporarilyImplodeNonGroupRowsAndExec(p_Grid
		, p_FromSelection ? TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS : TemporarilyImplodeNonGroupRowsAndExec::ALL_ROWS
		, [this, &p_Grid, p_FromSelection](const bool /*p_HasRowsToReExplode*/)
	{
		ASSERT(m_ObjectsToUpdate.empty()); // Use either SetObjects or BuildUpdatedObjects, not both

		vector<GridBackendRow*> rows;

		if (p_FromSelection)
			p_Grid.GetSelectedNonGroupRows(rows);
		else
			p_Grid.GetAllNonGroupRows(rows);

		for (const auto& pRow : rows)
		{
			ASSERT(nullptr != pRow);
			if (nullptr != pRow && (pRow->IsModified() || pRow->IsDeleted()))
			{
				GridBackendColumn* pColID = p_Grid.GetColumnByUniqueID(_T(O365_ID));
				ASSERT(nullptr != pColID);
				if (nullptr != pColID)
				{
					const GridBackendField& fID = pRow->GetField(pColID);
					ASSERT(fID.HasValue());
					if (fID.HasValue())
					{
						BusinessType businessObj;
						businessObj.SetID(fID.GetValueStr());

						if (pRow->IsModified())
							businessObj.SetUpdateStatus(BusinessObject::UPDATE_TYPE_RESTORE);
						else if (pRow->IsDeleted())
							businessObj.SetUpdateStatus(BusinessObject::UPDATE_TYPE_HARDDELETE);
						else
							ASSERT(FALSE);

						m_ObjectsToUpdate.push_back(businessObj);

						primary_key_t rowPk;
						p_Grid.GetRowPK(pRow, rowPk);
						m_PrimaryKeys.push_back(rowPk);
					}
				}
			}
		}
	})();
}

template<class BusinessType>
const vector<BusinessType>& UpdatedObjectsGenerator<BusinessType>::GetObjects() const
{
    return m_ObjectsToUpdate;
}


template<class BusinessType>
vector<BusinessType>& UpdatedObjectsGenerator<BusinessType>::GetObjectsNoConst()
{
	return m_ObjectsToUpdate;
}

template<class BusinessType>
void UpdatedObjectsGenerator<BusinessType>::SetObjects(const vector<BusinessType>& p_Objects)
{
    m_ObjectsToUpdate = p_Objects;
}


template<class BusinessType>
void UpdatedObjectsGenerator<BusinessType>::BuildObjToModify(const CacheGrid& p_Grid, GridBackendRow* p_pRow)
{
	ASSERT(nullptr != p_pRow);

    bool isTypeOk = false;

    if (rttr::type::get<BusinessType>() == rttr::type::get<BusinessUser>())
        isTypeOk = GridUtil::isBusinessType<BusinessUser>(p_pRow) ||
            GridUtil::isBusinessType<BusinessUserGuest>(p_pRow);
    else
        isTypeOk = GridUtil::isBusinessType<BusinessType>(p_pRow);

	if (nullptr != p_pRow && isTypeOk)
	{
		// FIXME: Use IGenericEditablePropertiesGrid::getBusinessObject if the grid inherits from IGenericEditablePropertiesGrid<BusinessType>

        BusinessType businessObj;
		businessObj.SetUpdateStatus(BusinessObject::UPDATE_TYPE_MODIFY);

		auto pColID = p_Grid.GetColumnByUniqueID(_T(O365_ID));
		ASSERT(nullptr != pColID);

		const auto& fID = p_pRow->GetField(pColID);
		if (fID.HasValue())
			businessObj.SetID(fID.GetValueStr());

		const auto& AllColumns = p_Grid.GetBackend().GetColumns();
		for (const auto& pCol : AllColumns)
		{
			ASSERT(nullptr != pCol);
			if (nullptr != pCol)
			{
				const auto& f = p_pRow->GetField(pCol);
				if (f.HasValue() && f.IsModified())
				{
					const auto fieldName = MFCUtil::convertUNICODE_to_ASCII(pCol->GetUniqueID());

					// FIXME: This is really dirty !! :-o
					if (rttr::type::get<BusinessType>() == rttr::type::get<BusinessUser>()
						&& Str::beginsWith(fieldName, O365_USER_ONPREMISESEXTENSIONATTRIBUTES)
						&& !((BusinessUser*)&businessObj)->GetOnPremisesExtensionAttributes())
					{
						auto gridUsers = dynamic_cast<const GridUsers*>(&p_Grid);
						ASSERT(nullptr != gridUsers);
						if (nullptr != gridUsers)
						{
							std::vector<boost::YOpt<PooledString> OnPremisesExtensionAttributes::*> attributes
							{
									&OnPremisesExtensionAttributes::m_ExtensionAttribute1
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute2
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute3
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute4
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute5
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute6
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute7
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute8
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute9
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute10
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute11
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute12
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute13
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute14
								,	&OnPremisesExtensionAttributes::m_ExtensionAttribute15
							};

							auto val = ((BusinessUser*)&businessObj)->GetOnPremisesExtensionAttributes();
							if (!val)
								val.emplace();
							for (int index = 1; index <= 15; ++index)
							{
								auto col = gridUsers->GetOPAttributeColumn(index - 1);
								(*val).*(attributes[index - 1]) = p_pRow->GetField(col).GetValueStr();
							}
								
							((BusinessUser*)&businessObj)->SetOnPremisesExtensionAttributes(val);
						}
					}
					else if (rttr::type::get<BusinessType>() == rttr::type::get<BusinessGroup>())
					{
						auto gridGroups = dynamic_cast<const GridGroups*>(&p_Grid);
						ASSERT(nullptr != gridGroups);
						if (nullptr != gridGroups)
						{
							if (gridGroups->IsTeamColumn(pCol))
							{
								auto propbu = rttr::type::get(businessObj).get_property(YTRIA_GROUP_TEAM);
								ASSERT(propbu.is_valid());
								if (propbu.is_valid())
								{
									auto& val = propbu.get_value(businessObj);
									if (val.is_type<boost::YOpt<Team>>() && !val.get_value<boost::YOpt<Team>>().is_initialized())
									{
										// FIXME: Really crap design
										if (p_pRow->GetField(gridGroups->GetIsTeamColumn()).IsModified())
											((BusinessGroup*)&businessObj)->SetEditHasTeam(true);

										auto team = gridGroups->GetTeam(p_pRow);
										propbu.set_value(businessObj, team);
									}
								}
								continue;
							}
							else if (gridGroups->IsArchiveTeamColumn(pCol))
							{
								// FIXME: Really crap design
								if (!((BusinessGroup*)&businessObj)->GetPendingTeamArchiving())
								{
									auto archivedCol = gridGroups->GetColumnByUniqueID(_YUID("isArchived"));
									auto setSpoCol = gridGroups->GetColumnByUniqueID(_YUID("setSpoSiteReadOnlyForMembers"));
									((BusinessGroup*)&businessObj)->SetTeamArchived(p_pRow->GetField(archivedCol).GetIcon() != NO_ICON);
									if (p_pRow->HasField(setSpoCol))
										((BusinessGroup*)&businessObj)->SetSpoSiteReadOnlyForMembers(p_pRow->GetField(setSpoCol).GetValueBool());
								}
								continue;
							}
						}
					}

					{
						auto propbu = rttr::type::get(businessObj).get_property(fieldName.c_str());
						if (propbu.is_valid())
						{
							if (propbu.get_type() == rttr::type::get<boost::YOpt<PooledString>>())
							{
								propbu.set_value(businessObj, boost::YOpt<PooledString>(f.GetValueStr()));
							}
							else if (propbu.get_type() == rttr::type::get<PooledString>())
							{
								propbu.set_value(businessObj, PooledString(f.GetValueStr()));
							}
							else if (propbu.get_type() == rttr::type::get<boost::YOpt<bool>>())
							{
								propbu.set_value(businessObj, boost::YOpt<bool>(f.GetValueBool()));
							}
							else if (propbu.get_type() == rttr::type::get<bool>())
							{
								propbu.set_value(businessObj, f.GetValueBool());
							}
							else if (propbu.get_type() == rttr::type::get<boost::YOpt<YTimeDate>>())
							{
								propbu.set_value(businessObj, boost::YOpt<YTimeDate>(f.GetValueTimeDate()));
							}
							else if (propbu.get_type() == rttr::type::get<YTimeDate>())
							{
								propbu.set_value(businessObj, f.GetValueTimeDate());
							}
							else if (propbu.get_type() == rttr::type::get<vector<PooledString>>())
							{
								if (f.IsMulti() && nullptr != f.GetValuesMulti<PooledString>())
									propbu.set_value(businessObj, *f.GetValuesMulti<PooledString>());
								else
									propbu.set_value(businessObj, (vector<PooledString>{ f.GetValueStr() }));
							}
							else if (propbu.get_type() == rttr::type::get<boost::YOpt<vector<PooledString>>>())
							{
								if (f.IsMulti() && nullptr != f.GetValuesMulti<PooledString>())
									propbu.set_value(businessObj, boost::YOpt<vector<PooledString>>(*f.GetValuesMulti<PooledString>()));
								else
									propbu.set_value(businessObj, boost::YOpt<vector<PooledString>>(vector<PooledString>{ f.GetValueStr() }));
							}
							else
							{
								// TODO: Other types!!!
								ASSERT(false);
							}
						}
					}
				}
			}
		}
        m_ObjectsToUpdate.push_back(businessObj);

        primary_key_t rowPk;
        p_Grid.GetRowPK(p_pRow, rowPk);
        m_PrimaryKeys.push_back(rowPk);
	}
}

template<class BusinessType>
void UpdatedObjectsGenerator<BusinessType>::BuildObjToCreate(const CacheGrid& p_Grid, GridBackendRow* p_pRow)
{
	// Is row pointer valid.
	ASSERT(nullptr != p_pRow);

	const bool isTypeOk = GridUtil::isBusinessType<BusinessType>(p_pRow);

	if (nullptr != p_pRow && isTypeOk)
	{
		// Is class type BusinessType valid within rttr.
		rttr::type classType = rttr::type::get<BusinessType>();
		if (classType.is_valid())
		{
			// Start filling in class type T.
            BusinessType businessObj;
			businessObj.SetUpdateStatus(BusinessObject::UPDATE_TYPE_CREATE);

			for (const auto& propbu : classType.get_properties())
			{
				auto propNeededForCreation = propbu.get_metadata(MetadataKeys::ValidForCreation);
				if (propNeededForCreation.is_valid())
				{
					// Get the column associated with this property.
					wstring PropName = MFCUtil::convertASCII_to_UNICODE(propbu.get_name().to_string());
					GridBackendColumn* pCol = p_Grid.GetColumnByUniqueID(PropName);
					ASSERT(nullptr != pCol);
					if (nullptr != pCol)
					{
						// Get the field new value.
						const GridBackendField& f = p_pRow->GetField(pCol);

						// bad hack, no other solution right now.
						// Wouldn't be needed if we used IGenericEditablePropertiesGrid::getBusinessObject when the grid inherits from IGenericEditablePropertiesGrid<BusinessType>
						if (classType == rttr::type::get<BusinessGroup>() && PropName == _YTEXT(O365_GROUP_MAILNICKNAME))
						{
							ASSERT(propbu.get_type() == rttr::type::get<boost::YOpt<PooledString>>());
							if (propbu.get_type() == rttr::type::get<boost::YOpt<PooledString>>())
							{
								if (f.HasValue())
									propbu.set_value(businessObj, boost::YOpt<PooledString>(f.GetValueStr()));
								else // FIXME: In this case we should ensure Group Type is SecurityGroup
									propbu.set_value(businessObj, boost::YOpt<PooledString>(_YTEXT("00000000-0000-0000-0000-000000000000")));
							}
						}
						else if (f.HasValue())
						{
							if (propbu.get_type() == rttr::type::get<boost::YOpt<PooledString>>())
							{
								propbu.set_value(businessObj, boost::YOpt<PooledString>(f.GetValueStr()));
							}
							else if (propbu.get_type() == rttr::type::get<PooledString>())
							{
								PooledString toto(f.GetValueStr());
								propbu.set_value(businessObj, toto);
							}
							else if (propbu.get_type() == rttr::type::get<boost::YOpt<bool>>())
							{
								propbu.set_value(businessObj, boost::YOpt<bool>(f.GetValueBool()));
							}
							else if (propbu.get_type() == rttr::type::get<bool>())
							{
								propbu.set_value(businessObj, f.GetValueBool());
							}
							else if (propbu.get_type() == rttr::type::get<vector<PooledString>>())
							{
								if (f.IsMulti() && nullptr != f.GetValuesMulti<PooledString>())
									propbu.set_value(businessObj, *f.GetValuesMulti<PooledString>());
								else
									propbu.set_value(businessObj, vector<PooledString>{ f.GetValueStr() });
							}
							else if (propbu.get_type() == rttr::type::get<boost::YOpt<vector<PooledString>>>())
							{
								if (f.IsMulti() && nullptr != f.GetValuesMulti<PooledString>())
									propbu.set_value(businessObj, boost::YOpt<vector<PooledString>>(*f.GetValuesMulti<PooledString>()));
								else
									propbu.set_value(businessObj, boost::YOpt<vector<PooledString>>(vector<PooledString>{ f.GetValueStr() }));
							}
							else if (propbu.get_type() == rttr::type::get<boost::YOpt<YTimeDate>>())
							{
								propbu.set_value(businessObj, boost::YOpt<YTimeDate>(f.GetValueTimeDate()));
							}
							else if (propbu.get_type() == rttr::type::get<YTimeDate>())
							{
								propbu.set_value(businessObj, f.GetValueTimeDate());
							}
							else
							{
								// TODO: Other types!!!
								ASSERT(false);
							}
						}
					}
				}
			}

			if (businessObj.GetID().IsEmpty())
			{
				auto colID = p_Grid.GetColumnByUniqueID(_YUID(O365_ID));
				ASSERT(nullptr != colID);
				if (nullptr != colID)
				{
					const GridBackendField& f = p_pRow->GetField(colID);
					businessObj.SetID(f.GetValueStr());
				}
			}

            m_ObjectsToUpdate.push_back(businessObj);
            
            primary_key_t rowPk;
            p_Grid.GetRowPK(p_pRow, rowPk);
            m_PrimaryKeys.push_back(rowPk);
		}
	}
}

template<class BusinessType>
void UpdatedObjectsGenerator<BusinessType>::BuildObjToDelete(const CacheGrid& p_Grid, GridBackendRow* p_pRow)
{
	ASSERT(nullptr != p_pRow);
    
    bool isGuestAmongUsers = false;
    if (rttr::type::get<BusinessType>() == rttr::type::get<BusinessUser>())
    {
        if (GridUtil::getBusinessTypeId(p_pRow) == rttr::type::get<BusinessUserGuest>().get_id())
            isGuestAmongUsers = true;
    }

	if (nullptr != p_pRow && (GridUtil::isBusinessType<BusinessType>(p_pRow) || isGuestAmongUsers))
	{
        BusinessType businessObj;

        auto businessGrid = dynamic_cast<const ModuleO365Grid<BusinessType>*>(&p_Grid);
        ASSERT(nullptr != businessGrid);
        if (nullptr != businessGrid)
        {
            businessObj = businessGrid->getBusinessObject(p_pRow);
            businessObj.SetUpdateStatus(BusinessObject::UPDATE_TYPE_DELETE);
            m_ObjectsToUpdate.push_back(businessObj);

            primary_key_t rowPk;
            p_Grid.GetRowPK(p_pRow, rowPk);
            m_PrimaryKeys.push_back(rowPk);
        }
	}
}


template<class BusinessType>
const vector<typename UpdatedObjectsGenerator<BusinessType>::primary_key_t>& UpdatedObjectsGenerator<BusinessType>::GetPrimaryKeys() const
{
    return m_PrimaryKeys;
}

template<class BusinessType>
void UpdatedObjectsGenerator<BusinessType>::SetPrimaryKeys(const vector<primary_key_t>& p_PrimaryKeys)
{
	m_PrimaryKeys = p_PrimaryKeys;
}
