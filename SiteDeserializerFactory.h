#pragma once

#include "IDeserializerFactory.h"
#include "Sapio365Session.h"
#include "SiteDeserializer.h"

class SiteDeserializerFactory : public IDeserializerFactory<SiteDeserializer>
{
public:
	SiteDeserializerFactory(std::shared_ptr<const Sapio365Session> p_Session);
	SiteDeserializer Create() override;

private:
	std::shared_ptr<const Sapio365Session> m_Session;
};
