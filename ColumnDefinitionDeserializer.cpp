#include "ColumnDefinitionDeserializer.h"

#include "BooleanColumnDeserializer.h"
#include "CalculatedColumnDeserializer.h"
#include "ChoiceColumnDeserializer.h"
#include "CurrencyColumnDeserializer.h"
#include "DateTimeColumnDeserializer.h"
#include "DefaultValueColumnDeserializer.h"
#include "JsonSerializeUtil.h"
#include "LookupColumnDeserializer.h"
#include "NumberColumnDeserializer.h"
#include "PersonOrGroupColumnDeserializer.h"
#include "TextColumnDeserializer.h"

void ColumnDefinitionDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("columnGroup"), m_Data.m_ColumnGroup, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.m_Description, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("enforceUniqueValues"), m_Data.m_EnforceUniqueValues, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("hidden"), m_Data.m_Hidden, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeBool(_YTEXT("indexed"), m_Data.m_Indexed, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("readOnly"), m_Data.m_ReadOnly, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("required"), m_Data.m_Required, p_Object);

    {
        BooleanColumnDeserializer bcd;
        if (JsonSerializeUtil::DeserializeAny(bcd, _YTEXT("boolean"), p_Object))
            m_Data.m_Boolean = bcd.GetData();
    }

    {
        CalculatedColumnDeserializer ccd;
        if (JsonSerializeUtil::DeserializeAny(ccd, _YTEXT("calculated"), p_Object))
            m_Data.m_Calculated = ccd.GetData();
    }


    {
        ChoiceColumnDeserializer ccd;
        if (JsonSerializeUtil::DeserializeAny(ccd, _YTEXT("choice"), p_Object))
            m_Data.m_Choice = ccd.GetData();
    }

    {
        CurrencyColumnDeserializer ccd;
        if (JsonSerializeUtil::DeserializeAny(ccd, _YTEXT("currency"), p_Object))
            m_Data.m_Currency = ccd.GetData();
    }

    {
        DateTimeColumnDeserializer bcd;
        if (JsonSerializeUtil::DeserializeAny(bcd, _YTEXT("dateTime"), p_Object))
            m_Data.m_DateTime = bcd.GetData();
    }

    {
        DefaultValueColumnDeserializer dvcd;
        if (JsonSerializeUtil::DeserializeAny(dvcd, _YTEXT("defaultValue"), p_Object))
            m_Data.m_DefaultValue = dvcd.GetData();
    }

    {
        LookupColumnDeserializer ld;
        if (JsonSerializeUtil::DeserializeAny(ld, _YTEXT("lookup"), p_Object))
            m_Data.m_Lookup = ld.GetData();
    }

    {
        NumberColumnDeserializer nd;
        if (JsonSerializeUtil::DeserializeAny(nd, _YTEXT("number"), p_Object))
            m_Data.m_Number = nd.GetData();
    }

    {
        PersonOrGroupColumnDeserializer pogd;
        if (JsonSerializeUtil::DeserializeAny(pogd, _YTEXT("personOrGroup"), p_Object))
            m_Data.m_PersonOrGroup = pogd.GetData();
    }

    {
        TextColumnDeserializer td;
        if (JsonSerializeUtil::DeserializeAny(td, _YTEXT("text"), p_Object))
            m_Data.m_Text = td.GetData();
    }
}