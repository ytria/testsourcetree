#pragma once

#include "ObjectActionModification.h"
#include "WithSiblings.h"

class O365Grid;

class DeletedObjectModification : public WithSiblings<ObjectActionModification, DeletedObjectModification>
{
public:
    DeletedObjectModification(O365Grid& p_Grid, const row_pk_t& p_RowKey);

    virtual void RefreshState() override;
	virtual vector<ModificationLog> GetModificationLogs() const override;
};

// =================================================================================================

class DeletedFolderModification : public DeletedObjectModification
{
public:
	DeletedFolderModification(O365Grid& p_Grid, const row_pk_t& p_RowKey);
	~DeletedFolderModification();

private:
	void recursiveSetParentDeletedState(GridBackendRow* p_ParentRow, bool deleted);
};
