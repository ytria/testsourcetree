#pragma once

#include "ExchangeObjectDeserializer.h"
#include "UserId.h"

class UserIdDeserializer : public Ex::ExchangeObjectDeserializer
{
public:
    void DeserializeObject(const XERCES_CPP_NAMESPACE::DOMElement& p_Element) override;
    const Ex::UserId& GetObject() const;

private:
    Ex::UserId m_UserId;
};

