#include "GenericModificationsReverter.h"

#include "BaseO365Grid.h"
#include "YCodeJockMessageBox.h"

GenericModificationsReverter::GenericModificationsReverter(O365Grid& p_Grid)
	: m_Grid(p_Grid)
{
}

void GenericModificationsReverter::RevertAll()
{
	ASSERT(m_Grid.GetParentGridFrameBase()->hasRevertableModificationsPending(false));

	if (RevertAllConfirm(&m_Grid))
		m_Grid.RevertAllModifications(true);
}

void GenericModificationsReverter::RevertSelected()
{
	ASSERT(m_Grid.GetParentGridFrameBase()->hasRevertableModificationsPending(true));

	if (RevertSelectedConfirm(&m_Grid))
		m_Grid.GetParentGridFrameBase()->revertSelectedImpl();
}

bool GenericModificationsReverter::RevertAllConfirm(CWnd* p_Parent)
{
	YCodeJockMessageBox confirmation(
		p_Parent,
		DlgMessageBox::eIcon_ExclamationWarning,
		YtriaTranslate::Do(GridFrameBase_On365RevertAll_1, _YLOC("Undo all changes")).c_str(),
		YtriaTranslate::Do(GridFrameBase_On365RevertAll_2, _YLOC("Are you sure?")).c_str(),
		YtriaTranslate::Do(GridFrameBase_On365RevertAll_3, _YLOC("All the pending changes will be lost.")).c_str(),
		{ { IDOK, YtriaTranslate::Do(GridFrameBase_On365RevertAll_4, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridFrameBase_On365RevertAll_5, _YLOC("No")).c_str() } }
	);

	return IDOK == confirmation.DoModal();
}

bool GenericModificationsReverter::RevertSelectedConfirm(CWnd* p_Parent)
{
	YCodeJockMessageBox confirmation(
		p_Parent,
		DlgMessageBox::eIcon_ExclamationWarning,
		YtriaTranslate::Do(GridFrameBase_On365RevertSelected_1, _YLOC("Undo selected changes")).c_str(),
		YtriaTranslate::Do(GridFrameBase_On365RevertSelected_2, _YLOC("Are you sure?")).c_str(),
		YtriaTranslate::Do(GridFrameBase_On365RevertSelected_3, _YLOC("Pending changes in selection will be lost.")).c_str(),
		{ { IDOK, YtriaTranslate::Do(GridFrameBase_On365RevertSelected_4, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridFrameBase_On365RevertSelected_5, _YLOC("No")).c_str() } }
	);

	return IDOK == confirmation.DoModal();
}
