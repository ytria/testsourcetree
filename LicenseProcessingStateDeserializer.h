#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "LicenseProcessingState.h"

class LicenseProcessingStateDeserializer : public JsonObjectDeserializer, public Encapsulate<LicenseProcessingState>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};
