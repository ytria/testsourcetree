#include "ChatMessageDeserializer.h"

#include "ChatAttachmentDeserializer.h"
#include "ChatMention.h"
#include "ChatReaction.h"
#include "IdentitySetDeserializer.h"
#include "ItemBodyDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "RecipientDeserializer.h"

class ChatMentionDeserializer : public JsonObjectDeserializer, public Encapsulate<ChatMention>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override
	{
		JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.m_Type, p_Object);
		JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
		JsonSerializeUtil::DeserializeString(_YTEXT("mentionText"), m_Data.m_MentionText, p_Object);
	}
};

class ParticipantInfoDeserializer : public JsonObjectDeserializer, public Encapsulate<ParticipantInfo>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override
	{
		{
			IdentitySetDeserializer isd;
			if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("identity"), p_Object))
				m_Data.m_Identity = isd.GetData();
		}

		JsonSerializeUtil::DeserializeString(_YTEXT("languageId"), m_Data.m_LanguageId, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("region"), m_Data.m_Region, p_Object);
	}
};

class ChatReactionDeserializer : public JsonObjectDeserializer, public Encapsulate<ChatReaction>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override
	{
		JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.m_Type, p_Object);

		{
			ParticipantInfoDeserializer isd;
			if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("user"), p_Object))
				m_Data.m_User = isd.GetData();
		}

		JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.m_CreatedDateTime, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("content"), m_Data.m_Content, p_Object);
	}
};


void ChatMessageDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
	JsonSerializeUtil::DeserializeString(_YTEXT("replyToId"), m_Data.m_ReplyToId, p_Object);

	{
		IdentitySetDeserializer isd;
		if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("from"), p_Object))
			m_Data.SetFrom(isd.GetData());
	}

	JsonSerializeUtil::DeserializeString(_YTEXT("etag"), m_Data.m_Etag, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("messageType"), m_Data.m_MessageType, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.m_CreatedDateTime, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.m_LastModifiedDateTime, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("deleted"), m_Data.m_Deleted, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("deletedDateTime"), m_Data.m_DeletedDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("subject"), m_Data.m_Subject, p_Object);

    {
        ItemBodyDeserializer ibd;
        if (JsonSerializeUtil::DeserializeAny(ibd, _YTEXT("body"), p_Object))
            m_Data.SetBody(ibd.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("summary"), m_Data.m_Summary, p_Object);

	// Attachments
	{
		ListDeserializer<ChatAttachment, ChatAttachmentDeserializer> lsd;
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("attachments"), p_Object))
			m_Data.SetAttachments(lsd.GetData());
	}

	// Mentions
	{
		ListDeserializer<ChatMention, ChatMentionDeserializer> lsd;
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("mentions"), p_Object))
			m_Data.SetMentions(lsd.GetData());
	}

	JsonSerializeUtil::DeserializeString(_YTEXT("importance"), m_Data.m_Importance, p_Object);

	// Reactions
	{
		ListDeserializer<ChatReaction, ChatReactionDeserializer> lsd;
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("reactions"), p_Object))
			m_Data.SetReactions(lsd.GetData());
	}

	JsonSerializeUtil::DeserializeString(_YTEXT("locale"), m_Data.m_Locale, p_Object);
}
