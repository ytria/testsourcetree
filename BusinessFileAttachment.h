#pragma once

#include "BusinessAttachment.h"
#include "FileExistsAction.h"
#include "FileAttachment.h"

class BusinessFileAttachment : public BusinessAttachment
{
public:
    BusinessFileAttachment();
    BusinessFileAttachment(const FileAttachment& p_FileAttachment);
    
    const boost::YOpt<PooledString>& GetContentBytes() const;
    void SetContentBytes(const boost::YOpt<PooledString>& p_Val);
    
    const boost::YOpt<PooledString>& GetContentId() const;
    void SetContentId(const boost::YOpt<PooledString>& p_Val);
    
    const boost::YOpt<PooledString>& GetContentLocation() const;
    void SetContentLocation(const boost::YOpt<PooledString>& p_Val);

	// Return written file's full path
    wstring ToFile(const wstring& p_Path, FileExistsAction p_FileExistsAction, const wstring& p_OptionalFileName = _YTEXT(""));

private:
    boost::YOpt<PooledString> m_ContentBytes;
    boost::YOpt<PooledString> m_ContentId;
    boost::YOpt<PooledString> m_ContentLocation;

    friend class FileAttachmentDeserializer;
};

