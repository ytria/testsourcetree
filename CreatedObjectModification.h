#pragma once

#include "RowModification.h"
#include "WithSiblings.h"

#include <map>

class O365Grid;

class CreatedObjectModification : public WithSiblings<RowModification, CreatedObjectModification>
{
public:
    CreatedObjectModification(O365Grid& p_Grid, const row_pk_t& p_RowKey);
	virtual ~CreatedObjectModification();

    virtual void RefreshState() override;

	GridBackendRow* RestoreRow(GridBackendRow* p_ExistingRowToRestore = nullptr);

	void SetNewPK(const row_pk_t& p_NewRowKey);

	CreatedObjectModification& operator=(const CreatedObjectModification& p_Other);

	virtual vector<ModificationLog> GetModificationLogs() const override;

	// FIXME: hack to change a behavior only in a few cases...
	void SetShouldRemoveRowOnRevertError(bool p_Val);

private:
	std::map<UINT, GridBackendField> m_FieldsBackup;
	row_pk_t m_ParentPKBackup;

	bool m_ShouldRemoveRowOnRevertError;

	// Clone ctor
	CreatedObjectModification(CreatedObjectModification& p_Other, const row_pk_t& p_NewRowKey);
};
