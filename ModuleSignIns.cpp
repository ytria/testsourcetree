#include "ModuleSignIns.h"

#include "FrameSignIns.h"
#include "RefreshSpecificData.h"
#include "SignInsRequester.h"
#include "safeTaskCall.h"
#include "DlgSigninsModuleOptions.h"
#include "Sapio365Session.h"
#include "BasicPageRequestLogger.h"

void ModuleSignIns::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
	case Command::ModuleTask::List:
		showSignIns(p_Command);
		break;
	case Command::ModuleTask::LoadSnapshot:
		loadSnapshot(p_Command);
		break;
	}
}

void ModuleSignIns::doRefresh(FrameSignIns* p_Frame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command)
{
	GridSignIns* grid = dynamic_cast<GridSignIns*>(&p_Frame->GetGrid());
	RefreshSpecificData refreshSpecificData;
	if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
	{
		ASSERT(p_IsUpdate);
		refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();
	}

	YSafeCreateTask([p_Command, p_Frame, p_TaskData, hwnd = p_Frame->GetSafeHwnd(), moduleCriteria = p_Frame->GetModuleCriteria(), p_Action, p_IsUpdate, grid, refreshSpecificData, bom = p_Frame->GetBusinessObjectManager()]() {
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("sign-ins"));
		auto requester = std::make_shared<SignInsRequester>(logger);

		ModuleOptions moduleOptions;
		if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
			moduleOptions = p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>();
		requester->SetCutOffDateTime(moduleOptions.m_CutOffDateTime);
		requester->SetCustomFilter(moduleOptions.m_CustomFilter);

		vector<SignIn> signIns = safeTaskCall(requester->Send(bom->GetSapio365Session(), p_TaskData).Then([requester]() {
			return requester->GetData();
		}), bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ListErrorHandler<SignIn>, p_TaskData).get();

		bool partialRefresh = !refreshSpecificData.m_Criteria.m_IDs.empty();
		const auto& criteria = partialRefresh ? refreshSpecificData.m_Criteria : moduleCriteria;
		YCallbackMessage::DoPost([=]() {
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameSignIns*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, p_TaskData.IsCanceled(), p_TaskData, false))
			{
				ASSERT(nullptr != frame);
				if (nullptr != frame)
				{
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s sign-ins...", Str::getStringFromNumber(signIns.size()).c_str()));
					frame->GetGrid().ClearLog(p_TaskData.GetId());

					{
						CWaitCursor _;
						frame->BuildView(signIns, !partialRefresh, false);
					}

					if (!p_IsUpdate)
					{
						frame->UpdateContext(p_TaskData, hwnd);
						shouldFinishTask = false;
					}
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, p_TaskData, p_Action);
			}
		});
	});
}

void ModuleSignIns::showSignIns(Command p_Command)
{
	AutomationAction* p_Action = p_Command.GetAutomationAction();
	const CommandInfo& info = p_Command.GetCommandInfo();
	const auto p_Origin = info.GetOrigin();

	RefreshSpecificData refreshSpecificData;
	bool isUpdate = false;

	auto p_SourceWindow = p_Command.GetSourceWindow();

	auto sourceCWnd = p_SourceWindow.GetCWnd();

	FrameSignIns* frame = dynamic_cast<FrameSignIns*>(info.GetFrame());
	if (nullptr == frame)
	{
		ASSERT(!p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>());

		if (FrameSignIns::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin = p_Origin;
			moduleCriteria.m_Privilege = info.GetRBACPrivilege();
				
			if (ShouldCreateFrame<FrameSignIns>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				DlgSigninsModuleOptions opt(p_SourceWindow.GetCWnd(), Sapio365Session::Find(p_Command.GetSessionIdentifier()));
				if (IDCANCEL == opt.DoModal())
				{
					if (nullptr != p_Action)
						SetActionCanceledByUser(p_Action);
					return;
				}
				p_Command.GetCommandInfo().Data2() = opt.GetOptions();

				CWaitCursor _;

				frame = new FrameSignIns(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(CommandDispatcher_getModuleName_28, _YLOC("Sign-ins")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
					frame->SetOptions(p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
		}
	}
	else
	{
		isUpdate = true;
	}

	if (nullptr != frame)
	{
		if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
			frame->SetOptions(p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>());
		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleSignIns_showSignIns_2, _YLOC("Show Sign-Ins")).c_str(), frame, p_Command, !isUpdate);
		doRefresh(frame, p_Action, taskData, isUpdate, p_Command);
	}
	else
	{
		SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
}

void ModuleSignIns::loadSnapshot(const Command& p_Command)
{
	LoadSnapshot<FrameSignIns>(p_Command, YtriaTranslate::Do(CommandDispatcher_getModuleName_28, _YLOC("Sign-ins")).c_str());
}
