#include "ChannelLoadMoreRequester.h"

#include "BusinessChannel.h"
#include "BusinessDriveItem.h"
#include "ChannelFilesFolderRequester.h"
#include "ChannelPrivateMembersRequester.h"
#include "DriveRequester.h"
#include "DriveItemDeserializer.h"
#include "IRequestLogger.h"
#include "O365AdminUtil.h"
#include "RESTUtils.h"
#include "RunOnScopeEnd.h"
#include "safeTaskCall.h"
#include "Sapio365Session.h"
#include <cpprest/http_msg.h>

ChannelLoadMoreRequester::ChannelLoadMoreRequester(const boost::YOpt<PooledString>& p_TeamId, const wstring& p_ChannelId, const boost::YOpt<PooledString>& p_MembershipType, int32_t p_Flags, const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_Logger(p_Logger)
	, m_Channel(std::make_shared<BusinessChannel>())
	, m_RequestMembers(REQUEST_MEMBERS == (REQUEST_MEMBERS & p_Flags))
	, m_RequestDrive(REQUEST_DRIVE == (REQUEST_DRIVE & p_Flags))
{
	ASSERT(m_RequestMembers || m_RequestDrive); // So what?
	m_Channel->SetID(p_ChannelId);
	m_Channel->SetGroupId(p_TeamId);
	m_Channel->SetMembershipType(p_MembershipType);
}

TaskWrapper<void> ChannelLoadMoreRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	return YSafeCreateTask(([logger = m_Logger, businessChannel = m_Channel, teamId = m_Channel->GetGroupId(), channelId = m_Channel->GetID(), requestMembers = m_RequestMembers, requestDrive = m_RequestDrive, p_Session, p_TaskData]() {
		// Whatever happens, put back old message value in logger
		RunOnScopeEnd([logger, oldMessage = logger->GetLogMessage()]() {
			logger->SetLogMessage(oldMessage);
		});

		// Private Members
		if (requestMembers && businessChannel->GetMembershipType() && *businessChannel->GetMembershipType() == _YTEXT("private"))
		{
			logger->SetLogMessage(_T("private members"));

			auto privateMembersRequester = std::make_shared<ChannelPrivateMembersRequester>(teamId, channelId, logger);

			auto privateMembers = safeTaskCall(privateMembersRequester->Send(p_Session, p_TaskData).Then([privateMembersRequester]()
				{
					return privateMembersRequester->GetData();
				}), p_Session->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ListErrorHandler<BusinessAADUserConversationMember>, p_TaskData).get();


			if (privateMembers.size() == 1 && privateMembers[0].GetError())
				businessChannel->SetPrivateChannelMembersError(privateMembers[0].GetError());
			else
				businessChannel->SetPrivateChannelMembers(privateMembers);
		}

		// Drive
		if (requestDrive)
		{
			{
				logger->SetLogMessage(_T("file folders"));

				auto filesFolderRequester = std::make_shared<ChannelFilesFolderRequester>(teamId ? *teamId : _YTEXT(""), channelId, logger);

				auto filesFolder = safeTaskCall(filesFolderRequester->Send(p_Session, p_TaskData).Then([filesFolderRequester]()
					{
						return filesFolderRequester->GetData();
					}), p_Session->GetMSGraphSession(Sapio365Session::APP_SESSION), Util::ObjectErrorHandler<BusinessDriveItem>, p_TaskData).get();


				if (!filesFolder.GetError() || filesFolder.GetError()->GetStatusCode() != web::http::status_codes::NotFound)
					businessChannel->SetFilesFolder(filesFolder);
			}

			if (businessChannel->GetFilesFolder())
			{
				if (businessChannel->GetFilesFolder()->GetError())
				{
					BusinessDrive errorDrive;
					errorDrive.SetError(businessChannel->GetFilesFolder()->GetError());
					businessChannel->SetDrive(errorDrive);
				}
				else
				{
					// Sync drive

					logger->SetLogMessage(_T("document library"));
					auto driveRequester = std::make_shared<DriveRequester>(DriveRequester::DriveSource::Drive, businessChannel->GetFilesFolder()->GetDriveId(), logger);
					auto drive = safeTaskCall(driveRequester->Send(p_Session, p_TaskData).Then([driveRequester]() {
						return driveRequester->GetData();
						}), p_Session->GetMSGraphSession(Sapio365Session::APP_SESSION), Util::ObjectErrorHandler<BusinessDrive>, p_TaskData).get();

					businessChannel->SetDrive(drive);
				}
			}
		}

		businessChannel->SetFlags(BusinessObject::MORE_LOADED);
	}));
}

const BusinessChannel& ChannelLoadMoreRequester::GetData() const
{
	return *m_Channel;
}
