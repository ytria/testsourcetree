#pragma once

#include "ModuleBase.h"

class FrameApplications;

class ModuleApplications : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	void executeImpl(const Command& p_Command) override;
	void doRefresh(FrameApplications* p_Frame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command);
	void showApplications(Command p_Command);
};

