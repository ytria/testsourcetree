#pragma once

#include "Contract.h"
#include "GraphCache.h"
#include "MSGraphSession.h"
#include "RequestDumper.h"
#include "O365Gateways.h"
#include "BasicPowerShellSession.h"
#include "ExchangePowerShellSession.h"
#include "LocalADInfo.h"
#include "SessionIdentifier.h"
#include <map>
#include <memory>
#include <ostream>

class AzureSession;
class ExchangeOnlineSession;
class SharepointOnlineSession;
class CosmosDBSqlSession;
class MainFrame;

class Sapio365Session : public std::enable_shared_from_this<Sapio365Session>
{
public:
	static std::shared_ptr<Sapio365Session> Find(const SessionIdentifier& p_Indentifier);
	static void SignOut(std::shared_ptr<Sapio365Session> p_Session, CWnd* p_ParentForDialog);
	static void SetSignedIn(std::shared_ptr<Sapio365Session> p_Session, CFrameWnd* p_Source);
	static bool CheckCompatibilityWithActiveRBAC(const wstring& p_TenantName, MainFrame* p_MainFrame, vector<shared_ptr<Sapio365Session>> p_SessionsToIgnore);
	static bool IsDumpGloballyEnabled();
	static void SetDumpGloballyEnabled();
	static vector<wstring> SetDumpGloballyDisabled(bool p_SaveToRegistry); // Returns dump file paths
	static wstring GetDumpPath();

	struct GraphError
	{
		wstring m_ErrorName;
		wstring m_ErrorDescription;
		wstring m_ErrorUri;
		wstring m_ErrorCode;
		/*add more?*/
	};
	static GraphError ParseGraphJsonError(const wstring& p_JsonError);
	static const wstring& GetUltraAdminID();// fake news - for SQL UpdateBy IDs
	static shared_ptr<MSGraphSession> CreateMSGraphSession();
	static bool IsUsedInMoreThanOneFrame(const std::shared_ptr<Sapio365Session>& p_Session);
	static void NotifyFrames(const std::shared_ptr<Sapio365Session>& p_Session);
	static void CancelAsyncUserCountTaskIfNeeded(const std::shared_ptr<Sapio365Session>& p_Session);

	bool GetForceDisableLoadOnPrem() const;
	void SetForceDisableLoadOnPrem(bool p_Value);

public:
	Sapio365Session();
	~Sapio365Session();

	// if p_ParentWindowForError is nullptr, no error dialog will be shown
	bool SetUseRoleDelegation(const int64_t p_ID, CWnd* p_ParentWindowForError);
	bool IsUseRoleDelegation() const;
    void SetIdentifier(const SessionIdentifier& p_Identifier);

    std::shared_ptr<IRestAuthenticator> GetRoleDelegationAuthenticator() const;

	void			SetSessionType(const wstring& p_SessionType);
	const wstring&	GetSessionType() const;

	bool IsUltraAdmin() const;
	bool IsAdvanced() const;
	bool IsElevated() const;
	bool IsRole() const;
	bool IsStandard() const;
	bool IsPartnerAdvanced() const;
	bool IsPartnerElevated() const;

	bool	AreRoleDelegationSessionsValid() const;
	int64_t GetRoleDelegationID() const;

	const SessionIdentifier& GetIdentifier() const;

	bool IsRBACHideUnscopedObjects() const; // Always true in automation
	void SetRBACHideUnscopedObjects(const bool p_Hide);

    const wstring&	GetCosmosDbSqlUri() const;
    void			SetCosmosDbSqlUri(const wstring& p_Val);
	void			SetCosmosSingleContainer(const bool p_CSC);

    const wstring&	GetCosmosDbMasterKey() const;
    void			SetCosmosDbMasterKey(const wstring& p_Val);
	bool			IsReadyForCosmos() const;
	bool			IsCosmosSingleContainer() const;
	wstring			GetCosmosAccount() const;

    // For technical reasons (shared_from_this() cannot be called from ctor), we can't set graph cache in the constructor 
    void SetGraphCache(std::unique_ptr<GraphCache>&& p_GraphCache);

    void SetAppId(const wstring& p_AppId);
    const wstring& GetAppId() const;

	void SetEmailOrAppId(const wstring& p_EmailOrAppId);
	const wstring& GetEmailOrAppId() const;

	void SetMSGraphSession(const shared_ptr<MSGraphSession>& p_Session);
	enum SessionType
	{
		USER_SESSION,
		APP_SESSION
	};
    std::shared_ptr<MSGraphSession> GetMainMSGraphSession() const;
	std::shared_ptr<MSGraphSession> GetMSGraphSession(SessionType p_SessionType) const;
    std::shared_ptr<SharepointOnlineSession> GetSharepointSession() const;
    std::shared_ptr<ExchangeOnlineSession> GetExchangeSession() const;
    std::shared_ptr<CosmosDBSqlSession> GetCosmosDBSqlSession() const;
	std::shared_ptr<AzureSession> GetAzureSession() const;

	std::shared_ptr<BasicPowerShellSession> GetBasicPowershellSession() const;
	void RemoveBasicPowershellSession();
	std::shared_ptr<ExchangePowerShellSession> GetExchangePowershellSession() const;
	bool HasBasicPowerShellSession() const;
	bool HasExchangePowerShellSession() const;
	bool HasPowerShellDll() const;
	std::shared_ptr<InvokeResultWrapper> InitExchangePowerShell(HWND p_Originator) const;
	std::shared_ptr<InvokeResultWrapper> InitBasicPowerShell(HWND p_Originator) const;
	void RemoveAzureSession();

	void SwapSessionsWith(std::shared_ptr<Sapio365Session>& p_Other);

    wstring GetTenantName() const;
	wstring GetTenantName(const bool p_WithFullDomain) const;
	wstring GetRBACTenantName(const bool p_WithFullDomain) const;
	wstring GetTenantNameDisplay() const;

	GraphCache& GetGraphCache();
	const GraphCache& GetGraphCache() const;

	bool IsConnected() const;

	void EnableRequestDumping();
	void DisableRequestDumping();

	const PooledString& GetConnectedUserID() const;
	const PooledString& GetConnectedUserPrincipalName() const;
	const PooledString& GetConnectedUserDisplayName() const;

	template<typename ScopeBusinessObjectType> // BusinessUser, BusinessGroup, BusinessSite
	vector<rttr::property> WithRBACRequiredProperties(const vector<rttr::property>& p_Props) const;

	template<typename ScopeBusinessObjectType> // BusinessUser, BusinessGroup, BusinessSite
	vector<PooledString> WithRBACRequiredProperties(const vector<PooledString>& p_Props) const;

	void SetAsyncUserCountTaskData(boost::YOpt<YtriaTaskData> p_TaskData);
	bool IsAsyncUserCountTaskCancel();

	const O365Gateways& GetGateways() const;
	O365Gateways& GetGateways();

	// sapio365 admin access level
	void SetAdminAccess(const set<RoleDelegationUtil::RBAC_AdminScope>& p_AdminAccess);
	bool IsAdminManager() const;
	bool IsAdminRBAC() const;
	bool IsAdminLogs() const;
	bool IsAdminAnnotations() const;
	bool IsCompanyAdmin() const;

	static bool IsCompanyAdmin(std::shared_ptr<MSGraphSession> p_Session, const PooledString& p_UserId);// creates a task - very bad perf in loops!
	static bool IsCompanyAdmin(std::shared_ptr<const Sapio365Session> p_Session);
	static bool IsAdminManager(std::shared_ptr<const Sapio365Session> p_Session);
	static bool IsAdminRBAC(std::shared_ptr<const Sapio365Session> p_Session);
	static bool IsAdminLogs(std::shared_ptr<const Sapio365Session> p_Session);
	static bool IsAdminAnnotations(std::shared_ptr<const Sapio365Session> p_Session);
	//

	void SetCommandLineAutomationInProgress(const bool p_AutomationInProgress);
	bool IsCommandLineAutomationInProgress() const;

	static bool AreSharingCredentials(const std::shared_ptr<const Sapio365Session>& p_Session1, const std::shared_ptr<const Sapio365Session>& p_Session2);

	const RoleDelegation& GetRoleDelegationFromCache() const;

	bool HasContracts(bool p_ForceRequest = true);
	const vector<Contract>& GetContracts(bool p_ForceRequest = true);

private:
	void setRoleDelegationCredentials(const wstring& p_Tenant, const wstring& p_Username, const wstring& p_Password, const wstring& p_ClientId, const wstring& p_ClientSecret);
	bool createRoleDelegationSessions(const wstring& p_AppRefName);
	void setRoleDelegationID(const int64_t p_ID);

	bool hasAdminAccess(RoleDelegationUtil::RBAC_AdminScope p_AA) const;

	void cancelAsyncUserCountTask(); // blocking

    SessionIdentifier m_Identifier;

    wstring m_AppId;

    wstring m_CosmosDbSqlUri;
    wstring m_CosmosDbMasterKey;
	bool	m_CosmosSingleContainer;

	// Don't change declaration order of those two
	std::shared_ptr<MSGraphSession> m_MsGraphSession;
    std::unique_ptr<GraphCache>		m_GraphCache;

    mutable std::shared_ptr<SharepointOnlineSession>	m_SharepointSession;
    mutable std::shared_ptr<ExchangeOnlineSession>		m_ExchangeSession;
    mutable std::shared_ptr<CosmosDBSqlSession>			m_CosmosSqlSession;
	mutable std::shared_ptr<AzureSession>				m_AzureSession;
	mutable std::shared_ptr<ExchangePowerShellSession>	m_ExchangePowerShellSession;
	mutable std::shared_ptr<BasicPowerShellSession>		m_BasicPowerShellSession;
	mutable bool m_ResetCosmosSession;
	bool m_RBACHideUnscopedObjects;
	bool m_CommandLineAutomationInProgress;

    O365Gateways m_Gateways;

	boost::YOpt<YtriaTaskData> m_AsyncUsersCountTaskData;

	set<RoleDelegationUtil::RBAC_AdminScope>	m_AdminAccess;// sapio365 role
	mutable boost::YOpt<bool>					m_IsCompanyAdmin;// office365 role

	boost::YOpt<vector<Contract>> m_Contracts;
	
	boost::YOpt<LocalADInfo> m_LocalADInfo;

	// ============DUMP===================

	static bool g_DumpEnable;
	static bool g_DumpRegistryRead;

	std::shared_ptr<RequestDumper> m_RequestDumper;
	bool m_ForceDisableLoadOnPrem;

	template <class T>
	friend class RbacRequiredPropertySet;
};

template<typename ScopeBusinessObjectType> // BusinessUser, BusinessGroup, BusinessSite
vector<rttr::property>
Sapio365Session::WithRBACRequiredProperties(const vector<rttr::property>& p_Props) const
{
	auto properties = p_Props;
	if (IsUseRoleDelegation())
	{
		ScopeBusinessObjectType dummy;
		auto& del = GetRoleDelegationFromCache();
		for (const auto& propName : del.GetRequiredProperties(MFCUtil::convertASCII_to_UNICODE(ScopeBusinessObjectType().get_type().get_name().to_string())))
		{
			const auto props = dummy.GetProperties(propName);
			for (const auto& prop : props)
			{
				const auto propName = Str::convertToASCII(prop);
				if (properties.end() == std::find_if(properties.begin(), properties.end(), [&propName](auto& p_Prop) {return p_Prop.get_name() == propName; }))
					properties.push_back(dummy.get_type().get_property(propName));
			}
		}
	}
	return properties;
}

template<typename ScopeBusinessObjectType> // BusinessUser, BusinessGroup, BusinessSite
vector<PooledString>
Sapio365Session::WithRBACRequiredProperties(const vector<PooledString>& p_Props) const
{
	auto properties = p_Props;
	if (IsUseRoleDelegation())
	{
		ScopeBusinessObjectType dummy;
		auto& del = GetRoleDelegationFromCache();
		for (const auto& propName : del.GetRequiredProperties(MFCUtil::convertASCII_to_UNICODE(ScopeBusinessObjectType().get_type().get_name().to_string())))
		{
			const auto props = dummy.GetProperties(propName);
			for (auto& prop : props)
			{
				if (properties.end() == std::find(properties.begin(), properties.end(), prop))
					properties.push_back(prop);
			}
		}
	}
	return properties;
}
