#pragma once

// FIXME: Add tenant for Partner sessions.
struct SessionIdentifier
{
    SessionIdentifier();
    SessionIdentifier(const wstring& p_EmailOrAppId, const wstring& p_SessionType, const int64_t p_RoleID);

	bool IsUltraAdmin() const;
	bool IsAdvanced() const;
	bool IsElevated() const;
	bool IsStandard() const;
	bool IsRole() const;
	bool IsPartnerAdvanced() const;
	bool IsPartnerElevated() const;

    bool operator==(const SessionIdentifier& p_Other) const;
    bool operator<(const SessionIdentifier& p_Other) const;

	wstring Serialize() const;
	bool Deserialize(const wstring& p_Serialized);

	static constexpr int64_t g_NoRole = 0;

    wstring m_EmailOrAppId;
    int64_t m_RoleID = g_NoRole;
    wstring m_SessionType;
};
