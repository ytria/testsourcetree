#pragma once

#include "IRequester.h"

#include "Database.h"
#include "DatabaseDeserializer.h"
#include "ValueListDeserializer.h"

class DumpDeserializer;
class SingleRequestResult;

namespace Cosmos
{
    class ListDbsRequester : public IRequester
    {
    public:
        ListDbsRequester() = default;
        virtual TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

        const vector<Cosmos::Database>& GetData() const;
        const SingleRequestResult& GetResult() const;

    private:
        std::shared_ptr<ValueListDeserializer<Cosmos::Database, Cosmos::DatabaseDeserializer>> m_Deserializer;
        std::shared_ptr<SingleRequestResult> m_Result;
    };
}

