#include "OnPremiseGroup.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<OnPremiseGroup>("OnPremiseGroup") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("On-Premises Group"))))
	.constructor()(policy::ctor::as_object);

	/*registration::class_<HybridGroup>("HybridGroup") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Hybrid Group"))))
		.constructor()(policy::ctor::as_object);*/
}

OnPremiseGroup::OnPremiseGroup()
{
}

const boost::YOpt<vector<PooledString>>& OnPremiseGroup::GetMember() const
{
	return m_Member;
}

void OnPremiseGroup::SetMember(const boost::YOpt<vector<PooledString>>& val)
{
	m_Member = val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseGroup::GetMemberOf() const
{
	return m_MemberOf;
}

void OnPremiseGroup::SetMemberOf(const boost::YOpt<vector<PooledString>>& val)
{
	m_MemberOf = val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseGroup::GetMembers() const
{
	return m_Members;
}

void OnPremiseGroup::SetMembers(const boost::YOpt<vector<PooledString>>& val)
{
	m_Members = val;
}

const boost::YOpt<vector<YTimeDate>>& OnPremiseGroup::GetDSCorePropagationData() const
{
	return m_DSCorePropagationData;
}

void OnPremiseGroup::SetDSCorePropagationData(const boost::YOpt<vector<YTimeDate>>& val)
{
	m_DSCorePropagationData = val;
}

const boost::YOpt<int32_t>& OnPremiseGroup::GetAdminCount() const
{
	return m_AdminCount;
}

void OnPremiseGroup::SetAdminCount(const boost::YOpt<int32_t>& val)
{
	m_AdminCount = val;
}

const boost::YOpt<int32_t>& OnPremiseGroup::GetGroupType() const
{
	return m_GroupType;
}

void OnPremiseGroup::SetGroupType(const boost::YOpt<int32_t>& val)
{
	m_GroupType = val;
}

const boost::YOpt<int32_t>& OnPremiseGroup::GetInstanceType() const
{
	return m_InstanceType;
}

void OnPremiseGroup::SetInstanceType(const boost::YOpt<int32_t>& val)
{
	m_InstanceType = val;
}

const boost::YOpt<int32_t>& OnPremiseGroup::GetSAMAccountType() const
{
	return m_SAMAccountType;
}

void OnPremiseGroup::SetSAMAccountType(const boost::YOpt<int32_t>& val)
{
	m_SAMAccountType = val;
}

const boost::YOpt<int32_t>& OnPremiseGroup::GetSDRightsEffective() const
{
	return m_SDRightsEffective;
}

void OnPremiseGroup::SetSDRightsEffective(const boost::YOpt<int32_t>& val)
{
	m_SDRightsEffective = val;
}

const boost::YOpt<int32_t>& OnPremiseGroup::GetSystemFlags() const
{
	return m_SystemFlags;
}

void OnPremiseGroup::SetSystemFlags(const boost::YOpt<int32_t>& val)
{
	m_SystemFlags = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetCanonicalName() const
{
	return m_CanonicalName;
}

void OnPremiseGroup::SetCanonicalName(const boost::YOpt<PooledString>& val)
{
	m_CanonicalName = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetCN() const
{
	return m_CN;
}

void OnPremiseGroup::SetCN(const boost::YOpt<PooledString>& val)
{
	m_CN = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetDescription() const
{
	return m_Description;
}

void OnPremiseGroup::SetDescription(const boost::YOpt<PooledString>& val)
{
	m_Description = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetDisplayName() const
{
	return m_DisplayName;
}

void OnPremiseGroup::SetDisplayName(const boost::YOpt<PooledString>& val)
{
	m_DisplayName = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetDistinguishedName() const
{
	return m_DistinguishedName;
}

void OnPremiseGroup::SetDistinguishedName(const boost::YOpt<PooledString>& val)
{
	m_DistinguishedName = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetGroupCategory() const
{
	return m_GroupCategory;
}

void OnPremiseGroup::SetGroupCategory(const boost::YOpt<PooledString>& val)
{
	m_GroupCategory = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetGroupScope() const
{
	return m_GroupScope;
}

void OnPremiseGroup::SetGroupScope(const boost::YOpt<PooledString>& val)
{
	m_GroupScope = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetHomePage() const
{
	return m_HomePage;
}

void OnPremiseGroup::SetHomePage(const boost::YOpt<PooledString>& val)
{
	m_HomePage = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetLastKnownParent() const
{
	return m_LastKnownParent;
}

void OnPremiseGroup::SetLastKnownParent(const boost::YOpt<PooledString>& val)
{
	m_LastKnownParent = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetManagedBy() const
{
	return m_ManagedBy;
}

void OnPremiseGroup::SetManagedBy(const boost::YOpt<PooledString>& val)
{
	m_ManagedBy = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetName() const
{
	return m_Name;
}

void OnPremiseGroup::SetName(const boost::YOpt<PooledString>& val)
{
	m_Name = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetObjectCategory() const
{
	return m_ObjectCategory;
}

void OnPremiseGroup::SetObjectCategory(const boost::YOpt<PooledString>& val)
{
	m_ObjectCategory = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetObjectClass() const
{
	return m_ObjectClass;
}

void OnPremiseGroup::SetObjectClass(const boost::YOpt<PooledString>& val)
{
	m_ObjectClass = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetObjectGUID() const
{
	return m_ObjectGUID;
}

void OnPremiseGroup::SetObjectGUID(const boost::YOpt<PooledString>& val)
{
	m_ObjectGUID = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetObjectSid() const
{
	return m_ObjectSid;
}

void OnPremiseGroup::SetObjectSid(const boost::YOpt<PooledString>& val)
{
	m_ObjectSid = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetSamAccountName() const
{
	return m_SamAccountName;
}

void OnPremiseGroup::SetSamAccountName(const boost::YOpt<PooledString>& val)
{
	m_SamAccountName = val;
}

const boost::YOpt<PooledString>& OnPremiseGroup::GetSID() const
{
	return m_SID;
}

void OnPremiseGroup::SetSID(const boost::YOpt<PooledString>& val)
{
	m_SID = val;
}

const boost::YOpt<YTimeDate>& OnPremiseGroup::GetCreated() const
{
	return m_Created;
}

void OnPremiseGroup::SetCreated(const boost::YOpt<YTimeDate>& val)
{
	m_Created = val;
}

const boost::YOpt<YTimeDate>& OnPremiseGroup::GetCreateTimeStamp() const
{
	return m_CreateTimeStamp;
}

void OnPremiseGroup::SetCreateTimeStamp(const boost::YOpt<YTimeDate>& val)
{
	m_CreateTimeStamp = val;
}

const boost::YOpt<YTimeDate>& OnPremiseGroup::GetModified() const
{
	return m_Modified;
}

void OnPremiseGroup::SetModified(const boost::YOpt<YTimeDate>& val)
{
	m_Modified = val;
}

const boost::YOpt<YTimeDate>& OnPremiseGroup::GetModifyTimeStamp() const
{
	return m_ModifyTimeStamp;
}

void OnPremiseGroup::SetModifyTimeStamp(const boost::YOpt<YTimeDate>& val)
{
	m_ModifyTimeStamp = val;
}

const boost::YOpt<YTimeDate>& OnPremiseGroup::GetUSNChanged() const
{
	return m_USNChanged;
}

void OnPremiseGroup::SetUSNChanged(const boost::YOpt<YTimeDate>& val)
{
	m_USNChanged = val;
}

const boost::YOpt<YTimeDate>& OnPremiseGroup::GetUSNCreated() const
{
	return m_USNCreated;
}

void OnPremiseGroup::SetUSNCreated(const boost::YOpt<YTimeDate>& val)
{
	m_USNCreated = val;
}

const boost::YOpt<YTimeDate>& OnPremiseGroup::GetWhenChanged() const
{
	return m_WhenChanged;
}

void OnPremiseGroup::SetWhenChanged(const boost::YOpt<YTimeDate>& val)
{
	m_WhenChanged = val;
}

const boost::YOpt<YTimeDate>& OnPremiseGroup::GetWhenCreated() const
{
	return m_WhenCreated;
}

void OnPremiseGroup::SetWhenCreated(const boost::YOpt<YTimeDate>& val)
{
	m_WhenCreated = val;
}

const boost::YOpt<bool>& OnPremiseGroup::GetDeleted() const
{
	return m_Deleted;
}

void OnPremiseGroup::SetDeleted(const boost::YOpt<bool>& val)
{
	m_Deleted = val;
}

const boost::YOpt<bool>& OnPremiseGroup::GetIsCriticalSystemObject() const
{
	return m_IsCriticalSystemObject;
}

void OnPremiseGroup::SetIsCriticalSystemObject(const boost::YOpt<bool>& val)
{
	m_IsCriticalSystemObject = val;
}

const boost::YOpt<bool>& OnPremiseGroup::GetIsDeleted() const
{
	return m_IsDeleted;
}

void OnPremiseGroup::SetIsDeleted(const boost::YOpt<bool>& val)
{
	m_IsDeleted = val;
}

const boost::YOpt<bool>& OnPremiseGroup::GetProtectedFromAccidentalDeletion() const
{
	return m_ProtectedFromAccidentalDeletion;
}

void OnPremiseGroup::SetProtectedFromAccidentalDeletion(const boost::YOpt<bool>& val)
{
	m_ProtectedFromAccidentalDeletion = val;
}
