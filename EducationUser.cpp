#include "EducationUser.h"

const boost::YOpt<IdentitySet>& EducationUser::GetCreatedBy() const
{
	return m_CreatedBy;
}

void EducationUser::SetCreatedBy(const boost::YOpt<IdentitySet>& p_Val)
{
	m_CreatedBy = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetExternalSource() const
{
	return m_ExternalSource;
}

void EducationUser::SetExternalSource(const boost::YOpt<PooledString>& p_Val)
{
	m_ExternalSource = p_Val;
}

const boost::YOpt<PhysicalAddress>& EducationUser::GetMailingAddress() const
{
	return m_MailingAddress;
}

void EducationUser::SetMailingAddress(const boost::YOpt<PhysicalAddress>& p_Val)
{
	m_MailingAddress = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetMailingAddressCity() const
{
	if (m_MailingAddress)
		return m_MailingAddress->City;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetMailingAddressCity(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_MailingAddress)
		m_MailingAddress = PhysicalAddress();
	m_MailingAddress->City = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetMailingAddressCountryOrRegion() const
{
	if (m_MailingAddress)
		return m_MailingAddress->CountryOrRegion;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetMailingAddressCountryOrRegion(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_MailingAddress)
		m_MailingAddress = PhysicalAddress();
	m_MailingAddress->CountryOrRegion = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetMailingAddressPostalCode() const
{
	if (m_MailingAddress)
		return m_MailingAddress->PostalCode;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetMailingAddressPostalCode(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_MailingAddress)
		m_MailingAddress = PhysicalAddress();
	m_MailingAddress->PostalCode = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetMailingAddressState() const
{
	if (m_MailingAddress)
		return m_MailingAddress->State;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetMailingAddressState(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_MailingAddress)
		m_MailingAddress = PhysicalAddress();
	m_MailingAddress->State = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetMailingAddressStreet() const
{
	if (m_MailingAddress)
		return m_MailingAddress->Street;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetMailingAddressStreet(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_MailingAddress)
		m_MailingAddress = PhysicalAddress();
	m_MailingAddress->Street = p_Val;
}

const boost::YOpt<PhysicalAddress>& EducationUser::GetResidenceAddress() const
{
	return m_ResidenceAddress;
}

void EducationUser::SetResidenceAddress(const boost::YOpt<PhysicalAddress>& p_Val)
{
	m_ResidenceAddress = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetResidenceAddressCity() const
{
	if (m_ResidenceAddress)
		return m_ResidenceAddress->City;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetResidenceAddressCity(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_ResidenceAddress)
		m_ResidenceAddress = PhysicalAddress();
	m_ResidenceAddress->City = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetResidenceAddressCountryOrRegion() const
{
	if (m_ResidenceAddress)
		return m_ResidenceAddress->CountryOrRegion;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetResidenceAddressCountryOrRegion(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_ResidenceAddress)
		m_ResidenceAddress = PhysicalAddress();
	m_ResidenceAddress->CountryOrRegion = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetResidenceAddressPostalCode() const
{
	if (m_ResidenceAddress)
		return m_ResidenceAddress->PostalCode;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetResidenceAddressPostalCode(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_ResidenceAddress)
		m_ResidenceAddress = PhysicalAddress();
	m_ResidenceAddress->PostalCode = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetResidenceAddressState() const
{
	if (m_ResidenceAddress)
		return m_ResidenceAddress->State;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetResidenceAddressState(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_ResidenceAddress)
		m_ResidenceAddress = PhysicalAddress();
	m_ResidenceAddress->State = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetResidenceAddressStreet() const
{
	if (m_ResidenceAddress)
		return m_ResidenceAddress->Street;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetResidenceAddressStreet(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_ResidenceAddress)
		m_ResidenceAddress = PhysicalAddress();
	m_ResidenceAddress->Street = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetMiddleName() const
{
	return m_MiddleName;
}

void EducationUser::SetMiddleName(const boost::YOpt<PooledString>& p_Val)
{
	m_MiddleName = p_Val;
}

const boost::YOpt<PooledString>& EducationUser::GetPrimaryRole() const
{
	return m_PrimaryRole;
}

void EducationUser::SetPrimaryRole(const boost::YOpt<PooledString>& p_Val)
{
	m_PrimaryRole = p_Val;
}

const boost::YOpt<vector<RelatedContact>>& EducationUser::GetRelatedContacts() const
{
	return m_RelatedContacts;
}

void EducationUser::SetRelatedContacts(const boost::YOpt<vector<RelatedContact>>& p_Val)
{
	m_RelatedContacts = p_Val;
}

const boost::YOpt<EducationStudent>& EducationUser::GetStudent() const
{
	return m_Student;
}

void EducationUser::SetStudent(const boost::YOpt<EducationStudent>& p_Val)
{
	m_Student = p_Val;
}

const boost::YOpt<EducationTeacher>& EducationUser::GetTeacher() const
{
	return m_Teacher;
}

void EducationUser::SetTeacher(const boost::YOpt<EducationTeacher>& p_Val)
{
	m_Teacher = p_Val;
}

void EducationUser::SetStudentBirthDate(const boost::YOpt<YTimeDate>& p_BirthDate)
{
	if (!m_Student)
		m_Student = EducationStudent();
	m_Student->m_BirthDate = p_BirthDate;
}

const boost::YOpt<YTimeDate>& EducationUser::GetStudentBirthDate() const
{
	if (m_Student)
		return m_Student->m_BirthDate;

	static boost::YOpt<YTimeDate> dummy;
	return dummy;
}

void EducationUser::SetStudentExternalId(const boost::YOpt<PooledString>& p_ExternalId)
{
	if (!m_Student)
		m_Student = EducationStudent();
	m_Student->m_ExternalId = p_ExternalId;
}

const boost::YOpt<PooledString>& EducationUser::GetStudentExternalId() const
{
	if (m_Student)
		return m_Student->m_ExternalId;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetStudentGender(const boost::YOpt<PooledString>& p_Gender)
{
	if (!m_Student)
		m_Student = EducationStudent();
	m_Student->m_Gender = p_Gender;
}

const boost::YOpt<PooledString>& EducationUser::GetStudentGender() const
{
	if (m_Student)
		return m_Student->m_Gender;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetStudentGrade(const boost::YOpt<PooledString>& p_Grade)
{
	if (!m_Student)
		m_Student = EducationStudent();
	m_Student->m_Grade = p_Grade;
}

const boost::YOpt<PooledString>& EducationUser::GetStudentGrade() const
{
	if (m_Student)
		return m_Student->m_Grade;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetStudentGraduationYear(const boost::YOpt<PooledString>& p_GradYear)
{
	if (!m_Student)
		m_Student = EducationStudent();
	m_Student->m_GraduationYear = p_GradYear;
}

const boost::YOpt<PooledString>& EducationUser::GetStudentGraduationYear() const
{
	if (m_Student)
		return m_Student->m_GraduationYear;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetStudentNumber(const boost::YOpt<PooledString>& p_StudentNumber)
{
	if (!m_Student)
		m_Student = EducationStudent();
	m_Student->m_StudentNumber = p_StudentNumber;
}

const boost::YOpt<PooledString>& EducationUser::GetStudentNumber() const
{
	if (m_Student)
		return m_Student->m_StudentNumber;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetTeacherExternalId(const boost::YOpt<PooledString>& p_TeacherExternalId)
{
	if (!m_Teacher)
		m_Teacher = EducationTeacher();
	m_Teacher->m_ExternalId = p_TeacherExternalId;
}

const boost::YOpt<PooledString>& EducationUser::GetTeacherExternalId() const
{
	if (m_Teacher)
		return m_Teacher->m_ExternalId;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationUser::SetTeacherNumber(const boost::YOpt<PooledString>& p_TeacherNumber)
{
	if (!m_Student)
		m_Teacher = EducationTeacher();
	m_Teacher->m_TeacherNumber = p_TeacherNumber;
}

const boost::YOpt<PooledString>& EducationUser::GetTeacherNumber() const
{
	if (m_Teacher)
		return m_Teacher->m_TeacherNumber;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}
