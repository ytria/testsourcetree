#pragma once

#include "BusinessObject.h"
#include <vector>

class BusinessAADUserConversationMember : public BusinessObject
{
public:
	BusinessAADUserConversationMember() = default;

	const boost::YOpt<PooledString>&			GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>&			GetUserID() const;
	void SetUserID(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>&			GetEmail() const;
	void SetEmail(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<vector<PooledString>>&	GetRoles() const;
	void SetRoles(const boost::YOpt<vector<PooledString>>& p_Val);

private:
	boost::YOpt<PooledString>			m_DisplayName;
	boost::YOpt<PooledString>			m_UserID;
	boost::YOpt<PooledString>			m_Email;
	boost::YOpt<vector<PooledString>>	m_Roles;

	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND

	friend class BusinessAADUserConversationMemberDeserializer;
};