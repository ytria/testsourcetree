#include "DelegatePermissionsSerializer.h"

#include "DelegatePermissions.h"
#include "XmlSerializeUtil.h"

Ex::DelegatePermissionsSerializer::DelegatePermissionsSerializer(XERCES_CPP_NAMESPACE::DOMDocument& p_Document, const Ex::DelegatePermissions& p_Obj)
    :IXmlSerializer(p_Document),
    m_Obj(p_Obj)
{
}

void Ex::DelegatePermissionsSerializer::SerializeImpl(DOMElement& p_Element)
{
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("CalendarFolderPermissionLevel"), m_Obj.m_CalendarFolder);
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("TasksFolderPermissionLevel"), m_Obj.m_TasksFolder);
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("InboxFolderPermissionLevel"), m_Obj.m_InboxFolder);
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("ContactsFolderPermissionLevel"), m_Obj.m_ContactsFolder);
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("NotesFolderPermissionLevel"), m_Obj.m_NotesFolder);
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("JournalFolderPermissionLevel"), m_Obj.m_JournalFolder);
}
