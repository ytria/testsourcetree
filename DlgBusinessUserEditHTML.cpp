#include "DlgBusinessUserEditHTML.h"

#include "AutomationNames.h"
#include "DlgRegAccountRegistration.h"
#include "Languages.h"

#include <boost/iterator/transform_iterator.hpp>


BaseDlgBusinessUserEditHTML::BaseDlgBusinessUserEditHTML(vector<BusinessUser>& p_BusinessUsers, DlgFormsHTML::Action p_Action, CWnd* p_Parent, const wstring& p_AutomationActionName)
	: DlgFormsHTML(p_Action, p_Parent, p_AutomationActionName)
	, m_BusinessUsers(p_BusinessUsers)
{
}

BaseDlgBusinessUserEditHTML::BaseDlgBusinessUserEditHTML(vector<BusinessUser>& p_BusinessUsers, DlgFormsHTML::Action p_Action, CWnd* p_Parent) : 
	BaseDlgBusinessUserEditHTML(p_BusinessUsers, p_Action, p_Parent, _YTEXT(""))
{
}

BaseDlgBusinessUserEditHTML::~BaseDlgBusinessUserEditHTML()
{

}

bool BaseDlgBusinessUserEditHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	std::map<wstring, wstring> alreadyProcessedProperties;
	for (const auto& item : data)
	{
		const auto& propName = item.first;
		ASSERT(!propName.empty());
		const auto& propValue = item.second;

		{
			auto it = m_StringSetters.find(propName);
			if (m_StringSetters.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties[propName] = propValue;

				for (auto& businessUser : m_BusinessUsers)
					it->second(businessUser, boost::YOpt<PooledString>(propValue));

				continue;
			}
		}

		{
			auto it = m_BoolSetters.find(propName);
			if (m_BoolSetters.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties[propName] = propValue;

				for (auto& businessUser : m_BusinessUsers)
					it->second(businessUser, boost::YOpt<bool>(Str::getBoolFromString(propValue)));

				continue;
			}
		}

		{
			auto it = m_DateSetters.find(propName);
			if (m_DateSetters.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties[propName] = propValue;

				for (auto& businessUser : m_BusinessUsers)
				{
					ASSERT(!propValue.empty()); // Having a date set should be mandatory (can't be null/empty in Office365)
					if (!propValue.empty())
					{
						auto date = DateTimeFromText(propValue);
						if (date)
							it->second(businessUser, *date);
					}
					//else
					//{
					//	it->second(businessUser, /*boost::none*/YTimeDate());
					//}
				}

				continue;
			}
		}

		{
			auto it = m_MultiCommaSeparatedStringSetters.find(propName);
			if (m_MultiCommaSeparatedStringSetters.end() != it)
			{
				auto& current = alreadyProcessedProperties[propName];

				if (!current.empty())
					current += _YTEXT(", ");
				current += propValue;

				for (auto& businessUser : m_BusinessUsers)
					it->second(businessUser, boost::YOpt<PooledString>(current));

				continue;
			}
		}

		const bool res = processPostedDataSpecific(item);
		ASSERT(res);
	}

	postProcessPostedData();

	return true;
}

void BaseDlgBusinessUserEditHTML::addStringEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes)
{
	/*if (isCreatePrefillDialog() && 0 == (p_Flags & EditorFlags::DIRECT_EDIT))
	{
		ASSERT(m_BusinessUsers.size() == 1);
		if (m_BusinessUsers.size() == 1 && p_Getter(m_BusinessUsers.back()))
		{
			p_Flags |= EditorFlags::DIRECT_EDIT;
		}
	}*/

	addObjectStringEditor<BusinessUser>(m_BusinessUsers
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, [p_Flags, &p_ApplicableUserTypes](const BusinessUser& bu)
		{
			return getRestrictionsImpl(bu, p_Flags, p_ApplicableUserTypes);
		}
	);
	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

void BaseDlgBusinessUserEditHTML::addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues, vector<wstring> p_ApplicableUserTypes)
{
	/*if (isCreatePrefillDialog() && 0 == (p_Flags & EditorFlags::DIRECT_EDIT))
	{
		ASSERT(m_BusinessUsers.size() == 1);
		if (m_BusinessUsers.size() == 1 && p_Getter(m_BusinessUsers.back()))
		{
			p_Flags |= EditorFlags::DIRECT_EDIT;
		}
	}*/

	addObjectListEditor<BusinessUser>(m_BusinessUsers
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, p_LabelsAndValues
		, [p_Flags, &p_ApplicableUserTypes](const BusinessUser& bu)
		{
			return getRestrictionsImpl(bu, p_Flags, p_ApplicableUserTypes);
		}
	);
	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

void BaseDlgBusinessUserEditHTML::addUserNameEditor(StringGetter p_Getter, StringSetter p_SetterLeft, StringSetter p_SetterRight, const wstring& p_PropNameLeft, const wstring& p_PropNameRight, const wstring& p_PropLabel, uint32_t p_Flags, const vector<std::tuple<wstring, wstring>>& p_LabelsAndValues, vector<wstring> p_ApplicableUserTypes)
{
	/*if (isCreatePrefillDialog() && 0 == (p_Flags & EditorFlags::DIRECT_EDIT))
	{
		ASSERT(m_BusinessUsers.size() == 1);
		if (m_BusinessUsers.size() == 1 && p_Getter(m_BusinessUsers.back()))
		{
			p_Flags |= EditorFlags::DIRECT_EDIT;
		}
	}*/

	addObjectUserNameEditor<BusinessUser>(m_BusinessUsers
		, p_Getter
		, p_PropNameLeft
		, p_PropNameRight
		, p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, p_LabelsAndValues
		, [p_Flags, &p_ApplicableUserTypes](const BusinessUser& bu)
		{
			return getRestrictionsImpl(bu, p_Flags, p_ApplicableUserTypes);
		}
	);

	ASSERT(!hasProperty(p_PropNameLeft));
	ASSERT(!hasProperty(p_PropNameRight));
	m_StringSetters[p_PropNameLeft] = p_SetterLeft;
	m_StringSetters[p_PropNameRight] = p_SetterRight;
}

void BaseDlgBusinessUserEditHTML::addMultiCommaSeparatedStringsCheckListEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const map<wstring, wstring>& p_LabelsAndValues, vector<wstring> p_ApplicableUserTypes)
{
	/*if (isCreatePrefillDialog() && 0 == (p_Flags & EditorFlags::DIRECT_EDIT))
	{
		ASSERT(m_BusinessUsers.size() == 1);
		if (m_BusinessUsers.size() == 1 && p_Getter(m_BusinessUsers.back()))
		{
			p_Flags |= EditorFlags::DIRECT_EDIT;
		}
	}*/

	addObjectMultiCommaSeparatedStringsCheckListEditor<BusinessUser>(m_BusinessUsers
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, p_LabelsAndValues
		, [p_Flags, &p_ApplicableUserTypes](const BusinessUser& bu)
		{
			return getRestrictionsImpl(bu, p_Flags, p_ApplicableUserTypes);
		}
	);
	ASSERT(!hasProperty(p_PropName));
	m_MultiCommaSeparatedStringSetters[p_PropName] = p_Setter;
}

void BaseDlgBusinessUserEditHTML::addBoolEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides/* = BoolEditor::CheckUncheckedLabelOverrides()*/)
{
	/*if (isCreatePrefillDialog() && 0 == (p_Flags & EditorFlags::DIRECT_EDIT))
	{
		ASSERT(m_BusinessUsers.size() == 1);
		if (m_BusinessUsers.size() == 1 && p_Getter(m_BusinessUsers.back()))
		{
			p_Flags |= EditorFlags::DIRECT_EDIT;
		}
	}*/

	addObjectBoolEditor<BusinessUser>(m_BusinessUsers
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, [p_Flags, &p_ApplicableUserTypes](const BusinessUser& bu)
		{
			return getRestrictionsImpl(bu, p_Flags, p_ApplicableUserTypes);
		}
		, p_CheckUncheckedLabelOverrides
	);
	ASSERT(!hasProperty(p_PropName));
	m_BoolSetters[p_PropName] = p_Setter;
}

void BaseDlgBusinessUserEditHTML::addBoolToggleEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides/* = BoolEditor::CheckUncheckedLabelOverrides()*/)
{
	/*if (isCreatePrefillDialog() && 0 == (p_Flags & EditorFlags::DIRECT_EDIT))
	{
		ASSERT(m_BusinessUsers.size() == 1);
		if (m_BusinessUsers.size() == 1 && p_Getter(m_BusinessUsers.back()))
		{
			p_Flags |= EditorFlags::DIRECT_EDIT;
		}
	}*/

	addObjectBoolToggleEditor<BusinessUser>(m_BusinessUsers
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, [p_Flags, &p_ApplicableUserTypes](const BusinessUser& bu)
		{
			return getRestrictionsImpl(bu, p_Flags, p_ApplicableUserTypes);
		}
		, p_CheckUncheckedLabelOverrides
	);
	ASSERT(!hasProperty(p_PropName));
	m_BoolSetters[p_PropName] = p_Setter;
}

void BaseDlgBusinessUserEditHTML::addDateEditor(DateGetter p_Getter, DateSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableUserTypes, const boost::YOpt<wstring>& p_MaxDate)
{
	/*if (isCreatePrefillDialog() && 0 == (p_Flags & EditorFlags::DIRECT_EDIT))
	{
		ASSERT(m_BusinessUsers.size() == 1);
		if (m_BusinessUsers.size() == 1 && p_Getter(m_BusinessUsers.back()))
		{
			p_Flags |= EditorFlags::DIRECT_EDIT;
		}
	}*/

	addObjectDateEditor<BusinessUser>(m_BusinessUsers
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE)
		, [p_Flags, &p_ApplicableUserTypes](const BusinessUser& bu)
		{
			return getRestrictionsImpl(bu, p_Flags, p_ApplicableUserTypes);
		}
		, false
		, p_MaxDate
	);
	ASSERT(!hasProperty(p_PropName));
	m_DateSetters[p_PropName] = p_Setter;
}

const ComboEditor::LabelsAndValues& BaseDlgBusinessUserEditHTML::getLocationList()
{
	static ComboEditor::LabelsAndValues locations;

	if (locations.empty())
	{
		auto transformerFunction = [](const boost::bimap<wstring, wstring>::left_value_type& elem)
		{
			return std::make_pair(elem.first, elem.second);
		};

		auto begin = boost::make_transform_iterator(DlgRegAccountRegistration::GetCountries().left.begin(), transformerFunction);
		auto end = boost::make_transform_iterator(DlgRegAccountRegistration::GetCountries().left.end(), transformerFunction);

		locations = ComboEditor::LabelsAndValues(begin, end);
	}

	return locations;
}

const ComboEditor::LabelsAndValues& BaseDlgBusinessUserEditHTML::getLanguageList()
{
	static ComboEditor::LabelsAndValues languages;

	if (languages.empty())
	{
		auto transformerFunction = [](const boost::bimap<wstring, wstring>::left_value_type& elem)
		{
			return std::make_pair(elem.first, elem.second);
		};

		auto begin = boost::make_transform_iterator(Languages::Get().left.begin(), transformerFunction);
		auto end = boost::make_transform_iterator(Languages::Get().left.end(), transformerFunction);

		languages = ComboEditor::LabelsAndValues(begin, end);
	}

	return languages;
}

uint32_t BaseDlgBusinessUserEditHTML::getRestrictionsImpl(const BusinessUser& bu, uint32_t p_Flags, const vector<wstring>& p_ApplicableUserTypes)
{
	uint32_t restrictFlags = 0;

	if (EditorFlags::RESTRICT_LOAD_MORE == (EditorFlags::RESTRICT_LOAD_MORE & p_Flags) && !bu.IsMoreLoaded())
		restrictFlags |= EditorFlags::RESTRICT_LOAD_MORE;

	if (EditorFlags::RESTRICT_NOT_CREATED == (EditorFlags::RESTRICT_NOT_CREATED & p_Flags) && BusinessGroup::Flag::CREATED == (bu.GetFlags() & BusinessGroup::Flag::CREATED))
		restrictFlags |= EditorFlags::RESTRICT_NOT_CREATED;

	if (!p_ApplicableUserTypes.empty() && bu.GetUserType() && p_ApplicableUserTypes.end() == std::find(p_ApplicableUserTypes.begin(), p_ApplicableUserTypes.end(), (wstring)*bu.GetUserType()))
		restrictFlags |= EditorFlags::RESTRICT_TYPE;

	return restrictFlags;
}

bool BaseDlgBusinessUserEditHTML::hasProperty(const wstring& p_PropertyName) const
{
	return m_StringSetters.end()	!= m_StringSetters.find(p_PropertyName)
		|| m_BoolSetters.end()		!= m_BoolSetters.find(p_PropertyName)
		|| m_DateSetters.end()		!= m_DateSetters.find(p_PropertyName)
		|| m_MultiCommaSeparatedStringSetters.end() != m_MultiCommaSeparatedStringSetters.find(p_PropertyName);
}

// ==================================================================================================

DlgBusinessUserEditHTML::DlgBusinessUserEditHTML(vector<BusinessUser>& p_BusinessUsers, boost::YOpt<Organization> p_Org, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
	: BaseDlgBusinessUserEditHTML(p_BusinessUsers, p_Action, p_Parent, p_Action == DlgFormsHTML::Action::CREATE ? g_ActionNameCreateUser : g_ActionNameSelectedEditUser)
	, m_AutoGeneratePassword(true)
	, m_Org(p_Org)
{
	ASSERT(!m_BusinessUsers.empty());
}

DlgBusinessUserEditHTML::~DlgBusinessUserEditHTML()
{
}

void DlgBusinessUserEditHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgBusinessUserEditHTML"), false, true, true);

	if (isCreateDialog() || isEditCreateDialog() || isPrefilledCreateDialog())
	{
		if (isPrefilledCreateDialog()) // FIXME: icon and color ?
			getGenerator().addIconTextField(_YTEXT("prefilledCreateInfo"), _T("Reminder: Some fields have been pre-filled with the properties of the currently selected users."), _YTEXT("fas fa-info-circle"), _YTEXT("#e4f0fb"), _YTEXT("#5882bc"));

		// If you add/remove an editable property here, don't forget to update GridTemplateUsers::updateRow as well,
		// otherwise new editable properties won't show up in the grid.

		const UINT directEditFlag = (isCreateDialog() || isPrefilledCreateDialog()) ? EditorFlags::DIRECT_EDIT : 0;

		getGenerator().addCategory(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_1, _YLOC("Mandatory Information")).c_str(), CategoryFlags::EXPAND_BY_DEFAULT);
		{
			//addStringEditor(&BusinessUser::GetUserType, &BusinessUser::SetUserType, _YTEXT("userType"), _TLoc("User Type"), EditorFlags::READONLY, {});

			addStringEditor(&BusinessUser::GetDisplayName, &BusinessUser::SetDisplayName, _YTEXT("userDisplayName"), wstring(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_2, _YLOC("User Display Name")).c_str()) + _YTEXT(" *"), EditorFlags::REQUIRED | directEditFlag, {});

			// Principal name must remain unique, don't edit it in multi mode
			if (!(isEditCreateDialog() && m_BusinessUsers.size() > 1))
			{
				vector<std::tuple<wstring, wstring>> domains;
				ASSERT(m_Org);
				if (m_Org)
				{
					//m_DefaultDomain = m_Org->GetDefaultDomain();
					for (const auto& dom : m_Org->GetDomains())
						domains.emplace_back(dom, dom);
				}

				addUserNameEditor(&BusinessUser::GetUserPrincipalName
					, &BusinessUser::SetUserPrincipalNameWithoutDomain
					, &BusinessUser::SetUserPrincipalNameDomain
					, _YTEXT("username-name")
					, _YTEXT("username-domain")
					, wstring(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_3, _YLOC("Username")).c_str()) + _YTEXT(" *")
					, EditorFlags::REQUIRED | directEditFlag
					, domains
					, { BusinessUser::g_UserTypeMember });
			}

			addBoolToggleEditor(&BusinessUser::GetAccountEnabled
							, &BusinessUser::SetAccountEnabled
							, _YTEXT("signinStatus")
							, YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_4, _YLOC("Sign-in status")).c_str()
							, EditorFlags::REQUIRED | directEditFlag
							, {}
							, {YtriaTranslate::Do(GridTemplateUsers_GridTemplateUsers_1, _YLOC("Allowed")).c_str(), YtriaTranslate::Do(GridTemplateUsers_GridTemplateUsers_2, _YLOC("Blocked")).c_str()});

			addStringEditor(&BusinessUser::GetMailNickname
				, &BusinessUser::SetMailNickname
				, _YTEXT("emailNickname")
				, wstring(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_5, _YLOC("Email Nickname")).c_str()) + _YTEXT(" *")
				, EditorFlags::REQUIRED | directEditFlag
				, { BusinessUser::g_UserTypeMember });

			bool allTheSamePwd = true, allTheSameFC = true, allTheSameFCMFA = true;
			const auto valPwd = !m_BusinessUsers.front().GetPassword() || m_BusinessUsers.front().GetPassword()->IsEmpty();
			const auto valFC = m_BusinessUsers.front().GetForceChangePassword();
			const auto valFCMFA = m_BusinessUsers.front().GetForceChangePasswordWithMfa();
			for (const auto& object : m_BusinessUsers)
			{
				{
					const auto newVal = !object.GetPassword() || object.GetPassword()->IsEmpty();
					if (valPwd != newVal)
						allTheSamePwd = false;
				}
				{
					const auto newValFC = object.GetForceChangePassword();
					if (valFC != newValFC)
						allTheSameFC = false;
				}
				{
					const auto newValFCMFA = object.GetForceChangePasswordWithMfa();
					if (valFCMFA != newValFCMFA)
						allTheSameFCMFA = false;
				}
			}
			{
				uint32_t flags = EditorFlags::DIRECT_EDIT;

				bool value = true;
				if (allTheSamePwd)
					value = valPwd;
				else
					flags = EditorFlags::NOCHANGE;

				getGenerator().Add(BoolToggleEditor({ { g_ParamNameAutoGenerate, YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_6, _YLOC("Auto-generate Password")).c_str(), flags, value } }));
			}
			addStringEditor(&BusinessUser::GetPassword, &BusinessUser::SetPassword, g_ParamNamePassword, wstring(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_7, _YLOC("Password")).c_str()) + _YTEXT(" *"), EditorFlags::REQUIRED | directEditFlag, {});

			{
				uint32_t flags = !isEditCreateDialog() || valFC ? EditorFlags::DIRECT_EDIT : 0;

				bool value = true;
				if (isEditCreateDialog())
				{
					if (allTheSameFC)
						value = valFC && *valFC;
					else
						flags = EditorFlags::NOCHANGE;
				}

				getGenerator().Add(BoolToggleEditor({ { g_ParamNameForceChange, YtriaTranslate::Do(DlgUserPasswordHTML_generateJSONScriptData_3, _YLOC("Force change on next login")).c_str(), flags, value } }));
			}

			{
				uint32_t flags = !isEditCreateDialog() || valFCMFA ? EditorFlags::DIRECT_EDIT : 0;

				bool value = true;
				if (isEditCreateDialog())
				{
					if (allTheSameFCMFA)
						value = valFCMFA && *valFCMFA;
					else
						flags = EditorFlags::NOCHANGE;
				}

				getGenerator().Add(BoolToggleEditor({ { g_ParamNameForceChangeMFA, _T("Force change with MFA on next login"), flags, value } }));
			}
			/*addBoolToggleEditor(&BusinessUser::GetForceChangePassword, &BusinessUser::SetForceChangePassword, _YTEXT("forceChangePassword"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_8, _YLOC("Force change on next login")).c_str(), EditorFlags::REQUIRED | directEditFlag, {});
			addBoolToggleEditor(&BusinessUser::GetForceChangePasswordWithMfa, &BusinessUser::SetForceChangePasswordWithMfa, _YTEXT("forceChangePasswordWithMFA"), _T("Force change with MFA on next login"), EditorFlags::REQUIRED | directEditFlag, {});*/
			// Disable password field if autogenerate
			getGenerator().addEvent(g_ParamNameAutoGenerate, EventTarget({ g_ParamNamePassword, EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueTrue }));
			// "Force change with MFA" only works if "Force change is set"
			getGenerator().addEvent(g_ParamNameForceChange, EventTarget({ g_ParamNameForceChangeMFA, EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueFalse }));
		}

		getGenerator().addCategory(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_9, _YLOC("Basic Information")).c_str(), CategoryFlags::NOFLAG);
		{
			addStringEditor(&BusinessUser::GetGivenName, &BusinessUser::SetGivenName, _YTEXT("firstName"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_10, _YLOC("First Name")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetSurname, &BusinessUser::SetSurname, _YTEXT("lastName"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_11, _YLOC("Last Name")).c_str(), 0, {});

			// Should follow ISO 639-1 Code: for example "en-US"
			// This value is nullable, but right now it's not handled (should add a selectable empty choice in combo)
			// So, set Required flag to force value selection as soon as edit is enabled (no direct-edit) to remain consistent.
			addComboEditor(&BusinessUser::GetPreferredLanguage, &BusinessUser::SetPreferredLanguage, _YTEXT("preferredLanguage"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_12, _YLOC("Preferred Language")).c_str(), EditorFlags::REQUIRED, getLanguageList(), {});

			// A two letter country code (ISO standard 3166), not nullable - Set Required flag to force value selection as soon as edit is enabled (no direct-edit)
			addComboEditor(&BusinessUser::GetUsageLocation, &BusinessUser::SetUsageLocation, _YTEXT("usageLocation"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_13, _YLOC("Location for License Usage")).c_str(), EditorFlags::REQUIRED, getLocationList(), {});
		}

		/*addCategory(_TLoc("Mail Config"), false, false);
		{
			//addStringEditor(&BusinessUser::GetMail, &BusinessUser::SetMail, _YTEXT("mail"), _TLoc("Email"), EditorFlags::READONLY, { BusinessUser::g_UserTypeMember });
			
			// FIXME: Need those missing fields? (read only)
			// proxy addresses
			// Im addresses
			// Mailbox settings ?
		}*/

		getGenerator().addCategory(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_14, _YLOC("Job Information")).c_str(), CategoryFlags::NOFLAG);
		{
			addStringEditor(&BusinessUser::GetJobTitle, &BusinessUser::SetJobTitle, _YTEXT("jobTitle"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_15, _YLOC("Job Title")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetCompanyName, &BusinessUser::SetCompanyName, _YTEXT("companyName"), _T("Company Name"), 0, {});
			addStringEditor(&BusinessUser::GetDepartment, &BusinessUser::SetDepartment, _YTEXT("department"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_16, _YLOC("Department")).c_str(), 0, {});

			// Is string collection in Graph, but handle it as single string
			addStringEditor(&BusinessUser::GetBusinessPhone, &BusinessUser::SetBusinessPhone, _YTEXT("businessPhones"), YtriaTranslate::Do(GridUsers_customizeGrid_17, _YLOC("Office Phone")), 0, {});

			addStringEditor(&BusinessUser::GetMobilePhone, &BusinessUser::SetMobilePhone, _YTEXT("mobilePhone"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_17, _YLOC("Mobile Phone")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetFaxNumber, &BusinessUser::SetFaxNumber, _YTEXT("faxNumber"), YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_20, _YLOC("Fax Number")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetOfficeLocation, &BusinessUser::SetOfficeLocation, _YTEXT("officeLocation"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_18, _YLOC("Office Location")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetStreetAddress, &BusinessUser::SetStreetAddress, _YTEXT("streetAddress"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_19, _YLOC("Street Address")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetCity, &BusinessUser::SetCity, _YTEXT("city"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_20, _YLOC("City")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetState, &BusinessUser::SetState, _YTEXT("state"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_21, _YLOC("State")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetPostalCode, &BusinessUser::SetPostalCode, _YTEXT("postalCode"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_22, _YLOC("Postal Code")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetCountry, &BusinessUser::SetCountry, _YTEXT("country"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_23, _YLOC("Country")).c_str(), 0, {});
		}

		getGenerator().addCategory(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_24, _YLOC("More Information")).c_str(), CategoryFlags::NOFLAG);
		{
			//addStringEditor(&BusinessUser::GetMySite, &BusinessUser::SetMySite, _YTEXT("personnalSite"), _TLoc("Personnal Site"), EditorFlags::RESTRICT_LOAD_MORE, { BusinessUser::g_UserTypeMember });

			// 4 enum: None, DisablePasswordExpiration, DisableStrongPassword, or both DisablePasswordExpiration & DisableStrongPassword (do not remove the space after the comma!) 
			ComboEditor::LabelsAndValues passwordPolicies{
				{ YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_25, _YLOC("None")).c_str(), _YTEXT("None") },
				{ YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_26, _YLOC("DisablePasswordExpiration")).c_str(), _YTEXT("DisablePasswordExpiration") },
				{ YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_27, _YLOC("DisableStrongPassword")).c_str(), _YTEXT("DisableStrongPassword") },
				{ YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_28, _YLOC("DisablePasswordExpiration, DisableStrongPassword")).c_str(), _YTEXT("DisablePasswordExpiration, DisableStrongPassword") } };
			addComboEditor(&BusinessUser::GetPasswordPolicies, &BusinessUser::SetPasswordPolicies, _YTEXT("passwordPolicies"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_29, _YLOC("Password Policies")).c_str(), EditorFlags::REQUIRED, passwordPolicies, {});

			//addStringEditor(&BusinessUser::GetAboutMe, &BusinessUser::SetAboutMe, _YTEXT("about"), _TLoc("About"), EditorFlags::RESTRICT_LOAD_MORE, { BusinessUser::g_UserTypeMember });
			//addStringEditor(&BusinessUser::GetPreferredName, &BusinessUser::SetPreferredName, _YTEXT("preferredName"), _TLoc("Preferred Name"), EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::READONLY, {}); // always return error 400 despite nothing says it's read-only...
		}

		/*addCategory(_TLoc("On-Premises Information"), NOFLAG);
		{
			addBoolEditor(&BusinessUser::GetOnPremisesSyncEnabled, &BusinessUser::SetOnPremisesSyncEnabled, _YTEXT("syncEnabled"), _TLoc("Sync Enabled"), EditorFlags::READONLY, {});
			addStringEditor(&BusinessUser::GetOnPremisesLastSyncDateTime, &BusinessUser::SetOnPremisesLastSyncDateTime, _YTEXT("lastSyncDateTime"), _TLoc("Last Sync Date Time"), EditorFlags::READONLY, {});
			addStringEditor(&BusinessUser::GetOnPremisesSecurityIdentifier, &BusinessUser::SetOnPremisesSecurityIdentifier, _YTEXT("securityId"), _TLoc("security Identifier"), EditorFlags::READONLY, {});
			addStringEditor(&BusinessUser::GetOnPremisesImmutableId, &BusinessUser::SetOnPremisesImmutableId, _YTEXT("immutableId"), _TLoc("Immutable ID"), EditorFlags::READONLY, {});
		}*/
	}
	else
	{
		ASSERT(isEditDialog() || isReadDialog());

		if (std::any_of(m_BusinessUsers.begin(), m_BusinessUsers.end(), [](auto& bu) {return !bu.IsMoreLoaded(); }))
			getGenerator().addIconTextField(_YTEXT("bewareLoadInfo"), _T("Reminder: Some fields may only be editable once you click the 'Load Info' button in the grid."), _YTEXT("fas fa-exclamation-triangle"), _YTEXT("#ff1de"), _YTEXT("#de7e00"));

		getGenerator().addCategory(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_30, _YLOC("Basic Information")).c_str(), CategoryFlags::EXPAND_BY_DEFAULT);
		{
			addStringEditor(&BusinessUser::GetDisplayName, &BusinessUser::SetDisplayName, _YTEXT("userDisplayName"), wstring(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_31, _YLOC("User Display Name")).c_str()) + _YTEXT(" *"), EditorFlags::REQUIRED, {});

			// Principal name must remain unique, don't edit it in multi mode
			if (m_BusinessUsers.size() <= 1)
			{
				vector<std::tuple<wstring, wstring>> domains;
				ASSERT(m_Org);
				if (m_Org)
				{
					for (const auto& dom : m_Org->GetDomains())
						domains.emplace_back(dom, dom);
				}

				addUserNameEditor(&BusinessUser::GetUserPrincipalName
					, &BusinessUser::SetUserPrincipalNameWithoutDomain
					, &BusinessUser::SetUserPrincipalNameDomain
					, _YTEXT("pn-name"), _YTEXT("pn-domain")
					, wstring(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_32, _YLOC("Username")).c_str()) + _YTEXT(" *")
					, EditorFlags::REQUIRED
					, domains
					, { BusinessUser::g_UserTypeMember });
			}

			//addStringEditor(&BusinessUser::GetUserType, &BusinessUser::SetUserType, _YTEXT("userType"), _TLoc("User Type"), EditorFlags::READONLY, {});
			addBoolToggleEditor(&BusinessUser::GetAccountEnabled
							, &BusinessUser::SetAccountEnabled
							, _YTEXT("signinStatus")
							, YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_33, _YLOC("Sign-in status")).c_str()
							, EditorFlags::REQUIRED
							, {}
							, { YtriaTranslate::Do(GridTemplateUsers_GridTemplateUsers_1, _YLOC("Allowed")).c_str(), YtriaTranslate::Do(GridTemplateUsers_GridTemplateUsers_2, _YLOC("Blocked")).c_str() });

			addBoolToggleEditor(&BusinessUser::GetIsResourceAccount
							, &BusinessUser::SetIsResourceAccount
							, _YTEXT("isResourceAccount")
							, YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_18, _YLOC("Is Resource Account")).c_str()
							, 0
							, {}
						);


			addStringEditor(&BusinessUser::GetGivenName, &BusinessUser::SetGivenName, _YTEXT("firstName"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_34, _YLOC("First Name")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetSurname, &BusinessUser::SetSurname, _YTEXT("lastName"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_35, _YLOC("Last Name")).c_str(), 0, {});

			// Should follow ISO 639-1 Code: for example "en-US"
			// This value is nullable, but right now it's not handled (should add a selectable empty choice in combo)
			// So, set Required flag to force value selection as soon as edit is enabled (no direct-edit) to remain consistent.
			addComboEditor(&BusinessUser::GetPreferredLanguage, &BusinessUser::SetPreferredLanguage, _YTEXT("preferredLanguage"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_36, _YLOC("Preferred Language")).c_str(), EditorFlags::REQUIRED, getLanguageList(), {});

			// A two letter country code (ISO standard 3166), not nullable - Set Required flag to force value selection as soon as edit is enabled (no direct-edit)
			addComboEditor(&BusinessUser::GetUsageLocation, &BusinessUser::SetUsageLocation, _YTEXT("usageLocation"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_37, _YLOC("Location for License Usage")).c_str(), EditorFlags::REQUIRED, getLocationList(), {});

			addStringEditor(&BusinessUser::GetPreferredDataLocation, &BusinessUser::SetPreferredDataLocation, _YTEXT("preferredDataLocation"), YtriaTranslate::Do(GridGroups_customizeGrid_62, _YLOC("Preferred Data Location")).c_str(), EditorFlags::RESTRICT_LOAD_MORE, {});
			addBoolToggleEditor(&BusinessUser::GetShowInAddressList
				, &BusinessUser::SetShowInAddressList
				, _YTEXT("showInAddressList")
				, YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_16, _YLOC("Show in Address List")).c_str()
				, 0
				, {});
		}

		getGenerator().addCategory(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_38, _YLOC("Mail Config")).c_str(), CategoryFlags::NOFLAG);
		{
			//addStringEditor(&BusinessUser::GetMail, &BusinessUser::SetMail, _YTEXT("mail"), _TLoc("Email"), EditorFlags::READONLY, { BusinessUser::g_UserTypeMember });
			addStringEditor(&BusinessUser::GetMailNickname
				, &BusinessUser::SetMailNickname
				, _YTEXT("emailNickname")
				, wstring(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_39, _YLOC("Email Nickname")).c_str()) + +_YTEXT(" *")
				, EditorFlags::REQUIRED
				, { BusinessUser::g_UserTypeMember });

			// FIXME: Add OtherMails (multivalue) and EditorFlags::RESTRICT_LOAD_MORE
			////

			// FIXME: Need those missing fields? (read only)
			// proxy addresses
			// Im addresses
			// Mailbox settings ?
		}

		getGenerator().addCategory(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_40, _YLOC("Job Information")).c_str(), CategoryFlags::NOFLAG);
		{
			addStringEditor(&BusinessUser::GetEmployeeID, &BusinessUser::SetEmployeeID, _YTEXT("employeeID"), YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_19, _YLOC("Employee ID")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetJobTitle, &BusinessUser::SetJobTitle, _YTEXT("jobTitle"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_41, _YLOC("Job Title")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetCompanyName, &BusinessUser::SetCompanyName, _YTEXT("companyName"), _T("Company Name"), 0, {});
			addStringEditor(&BusinessUser::GetDepartment, &BusinessUser::SetDepartment, _YTEXT("department"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_42, _YLOC("Department")).c_str(), 0, {});

			// Is string collection in Graph, but handle it as single string
			addStringEditor(&BusinessUser::GetBusinessPhone, &BusinessUser::SetBusinessPhone, _YTEXT("businessPhones"), YtriaTranslate::Do(GridUsers_customizeGrid_17, _YLOC("Office Phone")), 0, {});

			addStringEditor(&BusinessUser::GetMobilePhone, &BusinessUser::SetMobilePhone, _YTEXT("mobilePhone"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_43, _YLOC("Mobile Phone")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetFaxNumber, &BusinessUser::SetFaxNumber, _YTEXT("faxNumber"), YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_20, _YLOC("Fax Number")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetOfficeLocation, &BusinessUser::SetOfficeLocation, _YTEXT("officeLocation"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_44, _YLOC("Office Location")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetStreetAddress, &BusinessUser::SetStreetAddress, _YTEXT("streetAddress"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_45, _YLOC("Street Address")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetCity, &BusinessUser::SetCity, _YTEXT("city"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_46, _YLOC("City")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetState, &BusinessUser::SetState, _YTEXT("state"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_47, _YLOC("State")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetPostalCode, &BusinessUser::SetPostalCode, _YTEXT("postalCode"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_48, _YLOC("Postal Code")).c_str(), 0, {});
			addStringEditor(&BusinessUser::GetCountry, &BusinessUser::SetCountry, _YTEXT("country"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_49, _YLOC("Country")).c_str(), 0, {});
		}

		getGenerator().addCategory(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_50, _YLOC("More Information")).c_str(), CategoryFlags::NOFLAG);
		{
			addStringEditor(&BusinessUser::GetMySite, &BusinessUser::SetMySite, _YTEXT("personnalSite"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_51, _YLOC("Personnal Site")).c_str(), EditorFlags::RESTRICT_LOAD_MORE, { BusinessUser::g_UserTypeMember });

			// 4 enum: None, DisablePasswordExpiration, DisableStrongPassword, or both DisablePasswordExpiration & DisableStrongPassword (do not remove the space after the comma!) 
			ComboEditor::LabelsAndValues passwordPolicies{
				{ YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_52, _YLOC("None")).c_str(), _YTEXT("None") },
				{ YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_53, _YLOC("DisablePasswordExpiration")).c_str(), _YTEXT("DisablePasswordExpiration") },
				{ YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_54, _YLOC("DisableStrongPassword")).c_str(), _YTEXT("DisableStrongPassword") },
				{ YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_55, _YLOC("DisablePasswordExpiration, DisableStrongPassword")).c_str(), _YTEXT("DisablePasswordExpiration, DisableStrongPassword") } };
			addComboEditor(&BusinessUser::GetPasswordPolicies, &BusinessUser::SetPasswordPolicies, _YTEXT("passwordPolicies"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_56, _YLOC("Password Policies")).c_str(), EditorFlags::REQUIRED, passwordPolicies, {});

			addStringEditor(&BusinessUser::GetAboutMe, &BusinessUser::SetAboutMe, _YTEXT("about"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_57, _YLOC("About")).c_str(), EditorFlags::RESTRICT_LOAD_MORE, { BusinessUser::g_UserTypeMember });
			//addStringEditor(&BusinessUser::GetPreferredName, &BusinessUser::SetPreferredName, _YTEXT("preferredName"), _TLoc("Preferred Name"), EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::READONLY, {}); // always return error 400 despite nothing says it's read-only...
			addDateEditor(&BusinessUser::GetBirthday, &BusinessUser::SetBirthday, _YTEXT("birthday"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_58, _YLOC("Birthday")).c_str(), EditorFlags::RESTRICT_LOAD_MORE, {}, Str::g_EmptyString);
			addDateEditor(&BusinessUser::GetHireDate, &BusinessUser::SetHireDate, _YTEXT("hireDate"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_59, _YLOC("Hire Date")).c_str(), EditorFlags::RESTRICT_LOAD_MORE, {}, boost::none);
		}

		getGenerator().addCategory(_T("On-Premises Information"), CategoryFlags::NOFLAG);
		{
			//addBoolEditor(&BusinessUser::GetOnPremisesSyncEnabled, &BusinessUser::SetOnPremisesSyncEnabled, _YTEXT("syncEnabled"), _TLoc("Sync Enabled"), EditorFlags::READONLY, {});
			//addStringEditor(&BusinessUser::GetOnPremisesLastSyncDateTime, &BusinessUser::SetOnPremisesLastSyncDateTime, _YTEXT("lastSyncDateTime"), _TLoc("Last Sync Date Time"), EditorFlags::READONLY, {});
			//addStringEditor(&BusinessUser::GetOnPremisesSecurityIdentifier, &BusinessUser::SetOnPremisesSecurityIdentifier, _YTEXT("securityId"), _TLoc("security Identifier"), EditorFlags::READONLY, {});
			addStringEditor(&BusinessUser::GetOnPremisesImmutableId, &BusinessUser::SetOnPremisesImmutableId, _YTEXT("immutableId"), _T("Immutable ID"), 0, {});

#define AddOnPremiseAttributeEditor(_index) \
			{ \
				const auto id = wstring(_YTEXT("onPremisesExtensionAttributes.extensionAttribute")) + Str::getStringFromNumber((_index)); \
				const auto title = _YFORMAT(L"Attribute %s", Str::getStringFromNumber((_index)).c_str()); \
				addStringEditor(&BusinessUser::GetOnPremisesExtensionAttribute##_index, &BusinessUser::SetOnPremisesExtensionAttribute##_index, id, title, 0, {}); \
			} \

			AddOnPremiseAttributeEditor(1);
			AddOnPremiseAttributeEditor(2);
			AddOnPremiseAttributeEditor(3);
			AddOnPremiseAttributeEditor(4);
			AddOnPremiseAttributeEditor(5);
			AddOnPremiseAttributeEditor(6);
			AddOnPremiseAttributeEditor(7);
			AddOnPremiseAttributeEditor(8);
			AddOnPremiseAttributeEditor(9);
			AddOnPremiseAttributeEditor(10);
			AddOnPremiseAttributeEditor(11);
			AddOnPremiseAttributeEditor(12);
			AddOnPremiseAttributeEditor(13);
			AddOnPremiseAttributeEditor(14);
			AddOnPremiseAttributeEditor(15);

#undef AddOnPremiseAttributeEditor
		}
	}

	if (isReadDialog())
	{
		getGenerator().addCloseButton(LocalizedStrings::g_CloseButtonText);
	}
	else
	{
		getGenerator().addApplyButton(isEditDialog() ? LocalizedStrings::g_ApplyButtonText : LocalizedStrings::g_CreateButtonText);
		getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
		getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);
	}
}

bool DlgBusinessUserEditHTML::processPostedDataSpecific(const IPostedDataTarget::PostedData::value_type& p_Value)
{
	if (p_Value.first == g_ParamNameAutoGenerate)
	{
		ASSERT(isCreateDialog() || isEditCreateDialog() || isPrefilledCreateDialog());
		m_AutoGeneratePassword = Str::getBoolFromString(p_Value.second);
		return true;
	}

	if (g_ParamNameForceChange == p_Value.first)
	{
		auto val = Str::getBoolFromString(p_Value.second);
		for (auto& businessUser : m_BusinessUsers)
		{
			businessUser.SetForceChangePassword(val);
			if (!val) // This only works because g_ParamNameForceChange is alphabetically before g_ParamNameForceChangeMFA
				businessUser.SetForceChangePasswordWithMfa(val);
		}
		return true;
	}

	if (g_ParamNameForceChangeMFA == p_Value.first)
	{
		auto val = Str::getBoolFromString(p_Value.second);
		for (auto& businessUser : m_BusinessUsers)
			businessUser.SetForceChangePasswordWithMfa(val);
		return true;
	}

	// TODO: Other types?
	ASSERT(false);
	return false;
}

void DlgBusinessUserEditHTML::postProcessPostedData()
{
}

bool DlgBusinessUserEditHTML::GetAutoGeneratePassword() const
{
	ASSERT(isCreateDialog() || isEditCreateDialog() || isPrefilledCreateDialog());
	return m_AutoGeneratePassword;
}

wstring DlgBusinessUserEditHTML::getDialogTitle() const
{
	wstring title;

	if (isReadDialog())
		title = YtriaTranslate::Do(DlgBusinessUserEditHTML_getDialogTitle_2, _YLOC("View %1 user(s)"), Str::getStringFromNumber(m_BusinessUsers.size()).c_str());
	else if (isEditDialog())
		title = YtriaTranslate::Do(DlgBusinessUserEditHTML_getDialogTitle_3, _YLOC("Edit %1 user(s)"), Str::getStringFromNumber(m_BusinessUsers.size()).c_str());
	else if (isEditCreateDialog())
		title = YtriaTranslate::Do(DlgBusinessUserEditHTML_getDialogTitle_4, _YLOC("Edit %1 new user(s)"), Str::getStringFromNumber(m_BusinessUsers.size()).c_str());
	else if (isCreateDialog())
		title = YtriaTranslate::Do(DlgBusinessUserEditHTML_getDialogTitle_1, _YLOC("Create a new user")).c_str();
	else if (isPrefilledCreateDialog())
		title = YtriaTranslate::Do(DlgBusinessUserEditHTML_getDialogTitle_1, _YLOC("Create a new user")).c_str();

	return title;
}
