#include "FrameSitePermissions.h"
#include "GridSitePermissions.h"
#include "Command.h"

IMPLEMENT_DYNAMIC(FrameSitePermissions, GridFrameBase)

void FrameSitePermissions::BuildView(const O365DataMap<BusinessSite, ModuleSitePermissions::SitePermissionsInfo>& p_SitePermissions, bool p_FullPurge)
{
    m_Grid.BuildView(p_SitePermissions, p_FullPurge);
}

O365Grid& FrameSitePermissions::GetGrid()
{
    return m_Grid;
}

void FrameSitePermissions::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameSitePermissions::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameSitePermissions::ApplySpecific(bool p_Selected)
{
    CommandInfo info;
    info.Data() = m_Grid.GetSitePermissionsChanges(p_Selected);
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(
        Command(GetLicenseContext(),
            GetSafeHwnd(),
            Command::ModuleTarget::SitePermissions,
            Command::ModuleTask::ApplyChanges,
            info,
            { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
}

void FrameSitePermissions::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::SitePermissions, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameSitePermissions::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::SitePermissions, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

bool FrameSitePermissions::HasApplyButton()
{
    return true;
}

void FrameSitePermissions::createGrid()
{
    m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameSitePermissions::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigSites();
}

bool FrameSitePermissions::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
    m_CreateRefreshPartial = true;
    CreateViewGroup(tab);
    CreateRefreshGroup(tab);

    if (CRMpipe().IsFeatureSandboxed())
    {
        CreateApplyGroup(tab);
		CreateRevertGroup(tab);

        auto permGroup = tab.AddGroup(YtriaTranslate::Do(FrameSitePermissions_customizeActionsRibbonTab_1, _YLOC("Permissions")).c_str());
        ASSERT(nullptr != permGroup);
        if (nullptr != permGroup)
        {
            setGroupImage(*permGroup, 0);
            auto ctrl = permGroup->Add(xtpControlButton, ID_SITEPERMISSIONSGRID_DELETEPERMISSIONS);
            GridFrameBase::setImage({ ID_SITEPERMISSIONSGRID_DELETEPERMISSIONS }, IDB_SITEPERMISSIONSGRID_DELETEPERMISSIONS, xtpImageNormal);
            GridFrameBase::setImage({ ID_SITEPERMISSIONSGRID_DELETEPERMISSIONS }, IDB_SITEPERMISSIONSGRID_DELETEPERMISSIONS_16X16, xtpImageNormal);
            setGridControlTooltip(ctrl);
        }
    }

    return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameSitePermissions::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
    AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

    return statusRV;
}
