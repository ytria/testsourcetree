#pragma once

#include "AttachmentsInfo.h"

class GridEvents;
class GridMessages;
class GridPosts;

class AttachmentInfo : public AttachmentsInfo::IDs
{
public:
    AttachmentInfo(GridEvents& p_Grid, const wstring& p_AttachmentId, GridBackendRow* p_Row);
    AttachmentInfo(GridMessages& p_Grid, const wstring& p_AttachmentId, GridBackendRow* p_Row);
    AttachmentInfo(GridPosts& p_Grid, const wstring& p_AttachmentId, GridBackendRow* p_Row);

	GridBackendRow* m_Row;

private:
    template <typename T>
    void Construct(T& p_Grid, const wstring& p_AttachmentId, GridBackendRow* p_Row);
};

#include "AttachmentInfo.hpp"
