#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "PreAuthorizedApplication.h"

class PreAuthorizedApplicationDeserializer : public JsonObjectDeserializer, public Encapsulate<PreAuthorizedApplication>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

