#include "BusinessAADUserConversationMemberDeserializer.h"
#include "ListDeserializer.h"

void BusinessAADUserConversationMemberDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("userId"), m_Data.m_UserID, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("email"), m_Data.m_Email, p_Object);

	{
		ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("roles"), p_Object))
			m_Data.m_Roles = std::move(lsd.GetData());
	}
}
