#pragma once

#include "IJsonSerializer.h"
#include "Team.h"

class TeamSerializer : public IJsonSerializer
{
public:
    TeamSerializer(const Team& p_Team);
    void Serialize() override;

private:
    const Team& m_Team;
};

