#include "ModuleSiteRoles.h"

#include "SharepointOnlineSession.h"
#include "FrameSiteRoles.h"
#include "GridSiteRoles.h"
#include "O365AdminUtil.h"
#include "RefreshSpecificData.h"
#include "RoleDefinitionsRequester.h"
#include "safeTaskCall.h"
//#include "SiteRolesRequester.h"
#include "SpUtils.h"
#include "BasicPageRequestLogger.h"
#include "YSafeCreateTask.h"

void ModuleSiteRoles::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
    {
    case Command::ModuleTask::List:
        Command newCmd = p_Command;
        showSiteRoles(p_Command);
        break;
    }
}

void ModuleSiteRoles::doRefresh(FrameSiteRoles* p_pFrame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command)
{
    GridSiteRoles* gridSiteRoles = dynamic_cast<GridSiteRoles*>(&p_pFrame->GetGrid());

    RefreshSpecificData refreshSpecificData;
    if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
    {
        ASSERT(p_IsUpdate);
        refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();
    }

    YSafeCreateTask([p_TaskData, hwnd = p_pFrame->GetSafeHwnd(), currentModuleCriteria = p_pFrame->GetModuleCriteria(), p_Action, p_IsUpdate, gridSiteRoles, refreshSpecificData, bom = p_pFrame->GetBusinessObjectManager()]()
    {
        const bool partialRefresh = !refreshSpecificData.m_Criteria.m_IDs.empty();
        const auto& criteria = partialRefresh ? refreshSpecificData.m_Criteria : currentModuleCriteria;

		O365DataMap<BusinessSite, vector<Sp::RoleDefinition>> siteRoles;

		ASSERT(false); // TODO: correct logging
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("roles"));
		auto nonPageLogger = std::make_shared<BasicRequestLogger>(_T("roles"));

        ASSERT(criteria.m_Origin == Origin::Site);
        if (criteria.m_Origin == Origin::Site)
        {
            for (const auto& id : criteria.m_IDs)
            {
                BusinessSite site;
                site.SetID(id);
				logger->SetContextualInfo(bom->GetGraphCache().GetSiteContextualInfo(id));
                bom->GetGraphCache().SyncUncachedSite(site, nonPageLogger, p_TaskData);

                if (p_TaskData.IsCanceled())
                {
                    site.SetFlags(site.GetFlags() | BusinessObject::Flag::CANCELED);
                    siteRoles[site] = {};
                    continue;
                }
                else
                {
                    auto siteName = site.GetName();
                    ASSERT(siteName != boost::none && !siteName->IsEmpty());

                    web::uri webUrl = web::uri(site.GetWebUrl() ? *site.GetWebUrl() : _YTEXT(""));
                    auto siteUrl = Sp::GetSiteUrlFromWebUrl(webUrl);

                    auto requester = std::make_shared<Sp::RoleDefinitionsRequester>(siteUrl);


                    std::vector<Sp::RoleDefinition> SiteRolesData = safeTaskCall(requester->Send(bom->GetSapio365Session(), p_TaskData).Then([requester]() {
                        return requester->GetData();
                    }), bom->GetSharepointSession(), Util::ListErrorHandler<Sp::RoleDefinition>, p_TaskData).get();

                    if (p_TaskData.IsCanceled())
                        site.SetFlags(site.GetFlags() | BusinessObject::Flag::CANCELED);
                    siteRoles[site] = SiteRolesData;

                }
            }
        }

        YDataCallbackMessage<O365DataMap<BusinessSite, vector<Sp::RoleDefinition>>>::DoPost(siteRoles, [hwnd, partialRefresh, p_TaskData, p_Action, p_IsUpdate, isCanceled = p_TaskData.IsCanceled()](const O365DataMap<BusinessSite, vector<Sp::RoleDefinition>>& p_SpRoles)
        {
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameSiteRoles*>(CWnd::FromHandle(hwnd)) : nullptr;
            if (ShouldBuildView(hwnd, isCanceled, p_TaskData, false))
            {
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting roles for %s sites...", Str::getStringFromNumber(p_SpRoles.size()).c_str()));
                frame->GetGrid().ClearLog(p_TaskData.GetId());

				{
					CWaitCursor _;
					frame->BuildView(p_SpRoles, !partialRefresh);
				}

				if (!p_IsUpdate)
				{
					frame->UpdateContext(p_TaskData, hwnd);
					shouldFinishTask = false;
				}
            }

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, p_TaskData, p_Action);
			}
        });
    });
}

void ModuleSiteRoles::showSiteRoles(const Command& p_Command)
{
    AutomationAction*	p_Action = p_Command.GetAutomationAction();
    const CommandInfo&	info = p_Command.GetCommandInfo();
    const auto			p_Origin = info.GetOrigin();

    ASSERT(p_Origin != Origin::NotSet);
    if (p_Origin != Origin::NotSet)
    {
        RefreshSpecificData refreshSpecificData;
        bool isUpdate = false;

        const auto&	p_UserIDs = info.GetIds();
        auto		p_SourceWindow = p_Command.GetSourceWindow();

        auto sourceCWnd = p_SourceWindow.GetCWnd();

        FrameSiteRoles* frame = dynamic_cast<FrameSiteRoles*>(info.GetFrame());
        if (nullptr == frame)
        {
            ASSERT(!p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>());

            if (FrameSiteRoles::CanCreateNewFrame(true, sourceCWnd, p_Action))
            {
                ModuleCriteria moduleCriteria;
                moduleCriteria.m_Origin			= p_Origin;
                moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::IDS;
                moduleCriteria.m_IDs			= p_UserIDs;
				moduleCriteria.m_Privilege		= info.GetRBACPrivilege();

                if (moduleCriteria.m_IDs.empty())
                {
                    auto& id = GetConnectedSession().GetConnectedUserId();
                    ASSERT(!id.empty());
                    if (!id.empty())
                        moduleCriteria.m_IDs.insert(id);
                }

                if (ShouldCreateFrame<FrameSiteRoles>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
                {
                    CWaitCursor _;

                    frame = new FrameSiteRoles(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(CommandDispatcher_getModuleName_26, _YLOC("Site Roles")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
                    frame->InitModuleCriteria(moduleCriteria);
                    AddGridFrame(frame);
                    frame->Erect();
                }
            }
        }
        else
        {
			isUpdate = true;
        }

        if (nullptr != frame)
        {
            auto taskData = addFrameTask(YtriaTranslate::Do(GridSites_InitializeCommands_31, _YLOC("Show Site Roles")).c_str(), frame, p_Command, !isUpdate);
            doRefresh(frame, p_Action, taskData, isUpdate, p_Command);
        }
        else
        {
            SetAutomationGreenLight(p_Action, _YTEXT(""));
        }
    }
    else
    {
        SetAutomationGreenLight(p_Action, p_Command.ToString() + YtriaTranslate::DoError(ModuleContact_showContacts_3, _YLOC(" - origin not set"),_YR("Y2483")).c_str());
    }
}
