#include "BusinessUserGuest.h"

#include "BusinessGroup.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessUserGuest>("Guest") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Guest"))))
	.constructor()(policy::ctor::as_object)
	;
}
