#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "FieldValueSet.h"

class FieldValueSetDeserializer : public JsonObjectDeserializer, public Encapsulate<FieldValueSet>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

