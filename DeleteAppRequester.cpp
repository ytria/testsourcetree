#include "DeleteAppRequester.h"
#include "MsGraphHttpRequestLogger.h"

DeleteAppRequester::DeleteAppRequester(const wstring& p_Id)
	:m_Id(p_Id)
{
}

TaskWrapper<void> DeleteAppRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri;
	uri.append_path(_YTEXT("applications"));
	uri.append_path(m_Id);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Delete(uri.to_uri(), httpLogger, p_TaskData).ThenByTask([result = m_Result] (pplx::task<RestResultInfo> p_ResInfo) {
		try
		{
			result->SetResult(p_ResInfo.get());
		}
		catch (const RestException& e)
		{
			RestResultInfo info(e.GetRequestResult());
			info.m_RestException = std::make_shared<RestException>(e);
			result->SetResult(info);
		}
		catch (const std::exception& e)
		{
			RestResultInfo info;
			info.m_Exception = e;
			result->SetResult(info);
		}
	});
}

const SingleRequestResult& DeleteAppRequester::GetResult() const
{
	return *m_Result;
}
