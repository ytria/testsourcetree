#include "QuotaDeserializer.h"

#include "JsonSerializeUtil.h"

void QuotaDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeInt64(_YTEXT("total"), m_Data.Total, p_Object);
    JsonSerializeUtil::DeserializeInt64(_YTEXT("used"), m_Data.Used, p_Object);
    JsonSerializeUtil::DeserializeInt64(_YTEXT("remaining"), m_Data.Remaining, p_Object);
    JsonSerializeUtil::DeserializeInt64(_YTEXT("deleted"), m_Data.Deleted, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("state"), m_Data.State, p_Object);
}
