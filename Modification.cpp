#include "Modification.h"

const wstring Modification::g_HiddenValueFromLog = _YTEXT("***");

Modification::Modification(State p_State, const wstring& p_ObjectName):
	m_State(p_State),
	m_ObjectName(p_ObjectName)
{
}

Modification::State Modification::GetState() const
{
    return m_State;
}

const wstring& Modification::GetObjectName() const
{
	return m_ObjectName;
}

void Modification::SetObjectName(const wstring& p_ObjectName)
{
	m_ObjectName = p_ObjectName;
}

const wstring Modification::ModificationLog::g_NotSet = _YTEXT("N/A");