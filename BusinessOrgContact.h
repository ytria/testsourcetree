#pragma once

#include "BusinessObject.h"
#include "PooledString.h"
#include "HttpResultWithError.h"

#include <vector>

class BusinessGroup;
class BusinessOrgContact	: public BusinessObject
							, public JsonSerializable
{
public:
    BusinessOrgContact();
	BusinessOrgContact(const BusinessOrgContact& p_Other);
	BusinessOrgContact& operator=(const BusinessOrgContact& p_Other);

	const vector<std::unique_ptr<BusinessGroup>>&	GetParentGroups() const;
	const vector<PooledString>&		GetParentGroupsIDsFirstLevel() const;
    void SetParentGroups(const vector<BusinessGroup>& p_Groups);
	void AddParent(const BusinessGroup& p_Group);
	void AddParentIDFirstLevel(const PooledString& p_GroupID);
	bool IsDirectMemberOf(const BusinessGroup& p_Group) const;

	void							SetManager(const BusinessOrgContact& p_Manager);
	std::shared_ptr<BusinessOrgContact>&	GetManager();
	BusinessOrgContact*					GetManagerTop();// ptr to highest personager in hierarchy - 'this' if none set - never nullptr
	bool							HasManager() const;

	//void							AddDirectReport(const BusinessOrgContact& p_ManagerID);
	//const vector<BusinessOrgContact>&		GetDirectReports() const;
	//bool							HasDirecReports() const;
	//bool							IntegrateIntoManagerHierarchy(BusinessOrgContact& p_User);// if p_User is a direct report to this user or to any of its direct reports, add it to the relevant user's DRs and retrun true

	const boost::YOpt<vector<PooledString>>& GetBusinessPhones() const;
	void SetBusinessPhones(const vector<PooledString>& val);
	const boost::YOpt<PooledString>& GetCity() const;
	void SetCity(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetCountry() const;
	void SetCountry(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetDepartment() const;
	void SetDepartment(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<PooledString>& GetGivenName() const;
	void SetGivenName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetJobTitle() const;
	void SetJobTitle(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetMail() const;
	void SetMail(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetMailNickname() const;
	void SetMailNickname(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetMobilePhone() const;
	void SetMobilePhone(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetOfficeLocation() const;
	void SetOfficeLocation(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetOnPremisesLastSyncDateTime() const;
	void SetOnPremisesLastSyncDateTime(const boost::YOpt<PooledString>& val);
	const boost::YOpt<bool>& GetOnPremisesSyncEnabled() const;
	void SetOnPremisesSyncEnabled(const boost::YOpt<bool>& val);
	const boost::YOpt<PooledString>& GetPostalCode() const;
	void SetPostalCode(const boost::YOpt<PooledString>& val);
	const boost::YOpt<vector<PooledString>>& GetProxyAddresses() const;
	void SetProxyAddresses(const vector<PooledString>& val);
	const boost::YOpt<PooledString>& GetState() const;
	void SetState(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetStreetAddress() const;
	void SetStreetAddress(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetSurname() const;
	void SetSurname(const boost::YOpt<PooledString>& val);
    const boost::YOpt<PooledString>& GetCompanyName() const;
    void SetCompanyName(const boost::YOpt<PooledString>& p_Val);

    /*bool GetExists() const;
    void SetExists(bool val);*/

	bool operator<(const BusinessOrgContact& p_Other) const;

	/*enum Flag
	{
		MORE_LOADED	= BusinessObject::Flag::CUSTOM_FLAG_BEGIN,
		CREATED		= MORE_LOADED << 1,
		//etc..
	};*/

protected:
	virtual pplx::task<vector<HttpResultWithError>> SendEditRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;
	//virtual TaskWrapper<HttpResultWithError> SendCreateRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;
	virtual TaskWrapper<HttpResultWithError> SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;
	//virtual TaskWrapper<HttpResultWithError> SendRestoreRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;
	//virtual TaskWrapper<HttpResultWithError> SendHardDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;

private:
	/*****************************************/
	/*** WARNING: operator=() is overriden ***/
	/*****************************************/

	boost::YOpt<vector<PooledString>>	m_BusinessPhones;
	boost::YOpt<PooledString>			m_City;
    boost::YOpt<PooledString>			m_CompanyName;
	boost::YOpt<PooledString>			m_Country;
	boost::YOpt<PooledString>			m_Department;
	boost::YOpt<PooledString>			m_DisplayName;	
	boost::YOpt<PooledString>			m_GivenName;
	boost::YOpt<PooledString>			m_JobTitle;
	boost::YOpt<PooledString>			m_Mail;
	boost::YOpt<PooledString>			m_MailNickname;
	boost::YOpt<PooledString>			m_MobilePhone;
	boost::YOpt<PooledString>			m_OfficeLocation;
	boost::YOpt<PooledString>			m_OnPremisesLastSyncDateTime;
	boost::YOpt<bool>					m_OnPremisesSyncEnabled;
	boost::YOpt<PooledString>			m_PostalCode;
	boost::YOpt<vector<PooledString>>	m_ProxyAddresses;
	boost::YOpt<PooledString>			m_State;
	boost::YOpt<PooledString>			m_StreetAddress;
	boost::YOpt<PooledString>			m_Surname;

    //bool								m_Exists;

	vector<std::unique_ptr<BusinessGroup>>	m_ParentGroups;
	vector<PooledString>	m_ParentGroupIDsFirstLevel;

	// manager hierarchy
	std::shared_ptr<BusinessOrgContact>	m_Manager;
	//vector<BusinessUser>			m_DirectReports;

	/*****************************************/
	/*** WARNING: operator=() is overriden ***/
	/*****************************************/

    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND

    friend class OrgContactDeserializer;
};
