#pragma once

#include "BusinessUser.h"
#include "RelatedContact.h"
#include "EducationStudent.h"
#include "EducationTeacher.h"

class EducationUser : public BusinessUser
{
public:
	const boost::YOpt<IdentitySet>& GetCreatedBy() const;
	void SetCreatedBy(const boost::YOpt<IdentitySet>& p_Val);

	const boost::YOpt<PooledString>& GetExternalSource() const;
	void SetExternalSource(const boost::YOpt<PooledString>& p_Val);
	
	// Mailing address
	const boost::YOpt<PhysicalAddress>& GetMailingAddress() const;
	void SetMailingAddress(const boost::YOpt<PhysicalAddress>& p_Val);

	const boost::YOpt<PooledString>& GetMailingAddressCity() const;
	void SetMailingAddressCity(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetMailingAddressCountryOrRegion() const;
	void SetMailingAddressCountryOrRegion(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetMailingAddressPostalCode() const;
	void SetMailingAddressPostalCode(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetMailingAddressState() const;
	void SetMailingAddressState(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetMailingAddressStreet() const;
	void SetMailingAddressStreet(const boost::YOpt<PooledString>& p_Val);

	// Residence address
	const boost::YOpt<PhysicalAddress>& GetResidenceAddress() const;
	void SetResidenceAddress(const boost::YOpt<PhysicalAddress>& p_Val);

	const boost::YOpt<PooledString>& GetResidenceAddressCity() const;
	void SetResidenceAddressCity(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetResidenceAddressCountryOrRegion() const;
	void SetResidenceAddressCountryOrRegion(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetResidenceAddressPostalCode() const;
	void SetResidenceAddressPostalCode(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetResidenceAddressState() const;
	void SetResidenceAddressState(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetResidenceAddressStreet() const;
	void SetResidenceAddressStreet(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetMiddleName() const;
	void SetMiddleName(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<PooledString>& GetPrimaryRole() const;
	void SetPrimaryRole(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<vector<RelatedContact>>& GetRelatedContacts() const;
	void SetRelatedContacts(const boost::YOpt<vector<RelatedContact>>& p_Val);
	const boost::YOpt<EducationStudent>& GetStudent() const;
	void SetStudent(const boost::YOpt<EducationStudent>& p_Val);
	const boost::YOpt<EducationTeacher>& GetTeacher() const;
	void SetTeacher(const boost::YOpt<EducationTeacher>& p_Val);

	// Student info
	void SetStudentBirthDate(const boost::YOpt<YTimeDate>& p_BirthDate);
	const boost::YOpt<YTimeDate>& GetStudentBirthDate() const;
	void SetStudentExternalId(const boost::YOpt<PooledString>& p_ExternalId);
	const boost::YOpt<PooledString>& GetStudentExternalId() const;
	void SetStudentGender(const boost::YOpt<PooledString>& p_Gender);
	const boost::YOpt<PooledString>& GetStudentGender() const;
	void SetStudentGrade(const boost::YOpt<PooledString>& p_Grade);
	const boost::YOpt<PooledString>& GetStudentGrade() const;
	void SetStudentGraduationYear(const boost::YOpt<PooledString>& p_GradYear);
	const boost::YOpt<PooledString>& GetStudentGraduationYear() const;
	void SetStudentNumber(const boost::YOpt<PooledString>& p_StudentNumber);
	const boost::YOpt<PooledString>& GetStudentNumber() const;

	// Teacher info
	void SetTeacherExternalId(const boost::YOpt<PooledString>& p_TeacherExternalId);
	const boost::YOpt<PooledString>& GetTeacherExternalId() const;
	void SetTeacherNumber(const boost::YOpt<PooledString>& p_TeacherNumber);
	const boost::YOpt<PooledString>& GetTeacherNumber() const;

private:
	boost::YOpt<IdentitySet> m_CreatedBy;
	boost::YOpt<PooledString> m_ExternalSource;
	boost::YOpt<PhysicalAddress> m_MailingAddress;
	boost::YOpt<PhysicalAddress> m_ResidenceAddress;
	boost::YOpt<PooledString> m_MiddleName;
	boost::YOpt<PooledString> m_PrimaryRole;
	boost::YOpt<vector<RelatedContact>> m_RelatedContacts;
	boost::YOpt<EducationStudent> m_Student;
	boost::YOpt<EducationTeacher> m_Teacher;

	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
	friend class EducationUserDeserializer;
};

