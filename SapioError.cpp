#include "SapioError.h"
#include "Str.h"

SapioError::SapioError(const std::exception& e)
	: SapioError(Str::convertFromUTF8(e.what()), 0)
{

}

SapioError::SapioError(const wstring& message, unsigned short code)
	: m_ErrorMessage(message)
	, m_StatusCode(code)
{

}

SapioError::SapioError()
	: m_StatusCode(0)
{

}

wstring SapioError::GetFullErrorMessage() const
{
	return m_ErrorMessage;
}

void SapioError::AddErrorMessagePrefix(const wstring& p_Prefix)
{
    m_ErrorMessage = p_Prefix + m_ErrorMessage;
}

void SapioError::SetReason(const wstring& p_Reason)
{
	m_Reason = p_Reason;
}

const uint32_t& SapioError::GetStatusCode() const
{
	return m_StatusCode;
}

const wstring& SapioError::GetReason() const
{
	return m_Reason;
}

// =================================================================================================

HttpError::HttpError(const RestException& e)
	: SapioError(generateFullErrorMessage(e), e.GetRequestResult().Response.status_code())
{

}

wstring HttpError::generateFullErrorMessage(const RestException& e)
{
	const wstring errorMessage = e.WhatUnicode();
	const auto httpStatusCode = e.GetRequestResult().Response.status_code();
	const wstring httpReasonPhrase = e.GetRequestResult().Response.reason_phrase();
	if (errorMessage.empty())
	{
		if (0 != httpStatusCode)
		{
			return wstring(_YTEXT("HTTP ")) +
				std::to_wstring(httpStatusCode) +
				wstring(_YTEXT(" - ")) +
				httpReasonPhrase;
		}
	}
	else
	{
		if (0 == httpStatusCode)
			return errorMessage;
		else
		{
			return wstring(_YTEXT("HTTP ")) +
				std::to_wstring(httpStatusCode) +
				wstring(_YTEXT(" - ")) +
				httpReasonPhrase +
				wstring(_YTEXT(" - ")) + errorMessage;
		}
	}

	return wstring(_YTEXT(""));
}

SkippedError::SkippedError()
	: SapioError(getErrorMessage(), 0)
{

}

bool SkippedError::IsSkippedError(const SapioError& p_SapioError)
{
	return getErrorMessage() == p_SapioError.GetFullErrorMessage();
}

const wstring& SkippedError::getErrorMessage()
{
	static wstring errorMsg = YtriaTranslate::Do(YtriaGridDiscoverReplicas_LocalizeStaticMembers_12, _YLOC("Skipped")).c_str();
	return errorMsg;
}
