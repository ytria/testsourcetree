#pragma once

class WindowsListObserver;
class WindowsListUpdater
{
public:

	static WindowsListUpdater& GetInstance();

	void Add(WindowsListObserver* p_pObserver);
	void Remove(WindowsListObserver* p_pObserver);
	void RemoveAll(WindowsListObserver* p_pObserver);

	void Update();

private:

	set< WindowsListObserver* > m_WindowsList;
};