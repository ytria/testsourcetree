#pragma once

#include "JsonObjectDeserializer.h"
#include "EducationSchool.h"
#include "Encapsulate.h"

class EducationSchoolDeserializer : public JsonObjectDeserializer, public Encapsulate<EducationSchool>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

