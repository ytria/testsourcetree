#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessDriveItem.h"

class DriveItemDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessDriveItem>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;

private:
    PooledString GetOneDrivePath() const;
};

