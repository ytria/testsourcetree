#include "GroupDeserializer.h"

#include "AssignedLicenseDeserializer.h"
#include "BusinessGroupConfiguration.h"
#include "JsonSerializeUtil.h"
#include "LicenseProcessingStateDeserializer.h"
#include "ListDeserializer.h"
#include "MsGraphFieldNames.h"
#include "OnPremisesProvisioningErrorDeserializer.h"

GroupDeserializer::GroupDeserializer()
{

}

GroupDeserializer::GroupDeserializer(std::shared_ptr<const Sapio365Session> p_Session)
	: RoleDelegationTagger(p_Session, &m_Data)
{

}

#ifdef _DEBUG
GroupDeserializer::GroupDeserializer(std::shared_ptr<const Sapio365Session> p_Session, bool p_NoSelectUsed)
	: GroupDeserializer(p_Session)
{
	m_NoSelectUsed = p_NoSelectUsed;
}
#endif

void GroupDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	ASSERT(m_NoSelectUsed || containsAllRequiredProperties<BusinessGroup>(p_Object));

	m_Data.SetLastRequestTime(GetDate());

	JsonSerializeUtil::DeserializeId(_YUID(O365_ID), m_Data.m_Id, p_Object, true);
	deserializeBoolAndTagObject(_YUID(O365_GROUP_ALLOWEXTERNALSENDERS), m_Data.m_IsAllowExternalSenders, p_Object);
	deserializeBoolAndTagObject(_YUID(O365_GROUP_AUTOSUBSCRIBENEWMEMBERS), m_Data.m_IsAutoSubscribeNewMembers, p_Object);
	deserializeStringAndTagObject(_YUID(O365_GROUP_CLASSIFICATION), m_Data.m_Classification, p_Object);
	deserializeTimeDateAndTagObject(_YUID(O365_GROUP_CREATEDDATETIME), m_Data.m_CreatedDateTime, p_Object);
	deserializeStringAndTagObject(_YUID(O365_GROUP_DISPLAYNAME), m_Data.m_DisplayName, p_Object);
	deserializeStringAndTagObject(_YUID(O365_GROUP_DESCRIPTION), m_Data.m_Description, p_Object);

    {
        ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_GROUP_GROUPTYPES), p_Object))
            m_Data.m_GroupTypes = std::move(lsd.GetData());
    }

    {
        ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_GROUP_PROXYADDRESSES), p_Object))
            m_Data.m_ProxyAddresses = std::move(lsd.GetData());
    }

	deserializeStringAndTagObject(_YUID(O365_GROUP_MAIL), m_Data.m_Mail, p_Object);
	deserializeBoolAndTagObject(_YUID(O365_GROUP_MAILENABLED), m_Data.m_IsMailEnabled, p_Object);
	deserializeStringAndTagObject(_YUID(O365_GROUP_MAILNICKNAME), m_Data.m_MailNickname, p_Object);
	deserializeTimeDateAndTagObject(_YUID(O365_GROUP_ONPREMISESLASTSYNCDATETIME), m_Data.m_OnPremisesLastSyncDateTime, p_Object);
	deserializeStringAndTagObject(_YUID(O365_GROUP_ONPREMISESSECURITYIDENTIFIER), m_Data.m_OnPremisesSecurityIdentifier, p_Object);

	{
		ListDeserializer<OnPremisesProvisioningError, OnPremisesProvisioningErrorDeserializer> oppeld;
		oppeld.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(oppeld, _YUID(O365_GROUP_ONPREMISESPROVISIONINGERRORS), p_Object))
			m_Data.m_OnPremisesProvisioningErrors = std::move(oppeld.GetData());

        if (m_Data.m_OnPremisesProvisioningErrors != boost::none)
        {
            for (auto& error : *m_Data.m_OnPremisesProvisioningErrors)
            {
                tagObject(_YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY), error.m_Category);
                tagObject(_YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME), error.m_OccurredDateTime);
                tagObject(_YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR), error.m_PropertyCausingError);
                tagObject(_YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE), error.m_Value);
            }
        }

        if (m_Data.m_OnPremisesProvisioningErrors == boost::none ||
            m_Data.m_OnPremisesProvisioningErrors != boost::none && m_Data.m_OnPremisesProvisioningErrors->empty())
        {
            markPropertyAsChecked(_YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY));
            markPropertyAsChecked(_YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME));
            markPropertyAsChecked(_YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR));
            markPropertyAsChecked(_YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE));
        }
	}

	deserializeBoolAndTagObject(_YUID(O365_GROUP_ONPREMISESSYNCENABLED), m_Data.m_IsOnPremisesSyncEnabled, p_Object);
	deserializeBoolAndTagObject(_YUID(O365_GROUP_SECURITYENABLED), m_Data.m_IsSecurityEnabled, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YUID(O365_GROUP_UNSEENCOUNT), m_Data.m_UnseenCount, p_Object);
	deserializeStringAndTagObject(_YUID(O365_GROUP_VISIBILITY), m_Data.m_Visibility, p_Object);
	deserializeTimeDateAndTagObject(_YUID(O365_GROUP_DELETEDDATETIME), m_Data.m_DeletedDateTime, p_Object);
	deserializeTimeDateAndTagObject(_YUID(O365_GROUP_RENEWEDDATETIME), m_Data.m_RenewedDateTime, p_Object);
	deserializeStringAndTagObject(_YUID(O365_GROUP_PREFERREDDATALOCATION), m_Data.m_PreferredDataLocation, p_Object);

	{
		ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_GROUP_RESOURCEBEHAVIOROPTIONS), p_Object))
			m_Data.m_ResourceBehaviorOptions = std::move(lsd.GetData());
	}

	{
		ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_GROUP_RESOURCEPROVISIONINGOPTIONS), p_Object))
			m_Data.m_ResourceProvisioningOptions = std::move(lsd.GetData());
	}

	deserializeBoolAndTagObject(_YUID(O365_GROUP_HIDEFROMOUTLOOKCLIENTS), m_Data.m_HideFromOutlookClients, p_Object);
	deserializeBoolAndTagObject(_YUID(O365_GROUP_HIDEFROMADDRESSLISTS), m_Data.m_HideFromAddressLists, p_Object);

	{
		LicenseProcessingStateDeserializer lpsd;
		lpsd.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(lpsd, _YUID(O365_GROUP_LICENSEPROCESSINGSTATE), p_Object))
			m_Data.m_LicenseProcessingState = std::move(lpsd.GetData());
	}

	deserializeStringAndTagObject(_YUID(O365_GROUP_SECURITYIDENTIFIER), m_Data.m_SecurityIdentifier, p_Object);

	{
		const JsonSerializeUtil::String propName = _YUID(O365_GROUP_ASSIGNEDLICENSES);
		ListDeserializer<BusinessAssignedLicense, AssignedLicenseDeserializer> assLicDeserializer;
		assLicDeserializer.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(assLicDeserializer, propName, p_Object))
			m_Data.m_AssignedLicenses = std::move(assLicDeserializer.GetData());

		if (m_Data.m_AssignedLicenses != boost::none)
		{
			auto subscribedSkus = GetSession().GetGraphCache().GetCachedSubscribedSkus();
			for (const auto& assignedLicense : *m_Data.m_AssignedLicenses)
			{
				auto itt = subscribedSkus.find(assignedLicense.GetID());
				if (itt != subscribedSkus.end())
					tagObject(propName, itt->second.m_SkuPartNumber);
			}
		}

		if (m_Data.m_AssignedLicenses == boost::none ||
			m_Data.m_AssignedLicenses != boost::none && m_Data.m_AssignedLicenses->empty())
			tagObject(propName, BusinessGroupConfiguration::GetInstance().GetValueStringLicenses_Unlicensed());
	}

	if (p_Object.find(L"@removed") != p_Object.end())
		m_Data.SetFlags(m_Data.GetFlags() | BusinessObject::REMOVEDBYDELTASYNC);

	/* BETA API */
	deserializeStringAndTagObject(_YUID(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE), m_Data.m_MembershipRuleProcessingState, p_Object);
	deserializeStringAndTagObject(_YUID(O365_GROUP_MEMBERSHIPRULE), m_Data.m_MembershipRule, p_Object);
	deserializeStringAndTagObject(_YUID(O365_GROUP_THEME), m_Data.m_Theme, p_Object);
	deserializeStringAndTagObject(_YUID(O365_GROUP_PREFERREDLANGUAGE), m_Data.m_PreferredLanguage, p_Object);

	tagObjectFromComputedProperties();
}
