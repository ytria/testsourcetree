#include "LicenseProcessingStateDeserializer.h"

#include "JsonSerializeUtil.h"

void LicenseProcessingStateDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("state"), m_Data.m_State, p_Object);
}
