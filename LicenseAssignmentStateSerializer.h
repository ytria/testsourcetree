#pragma once

#include "IJsonSerializer.h"
#include "LicenseAssignmentState.h"

class LicenseAssignmentStateSerializer : public IJsonSerializer
{
public:
	LicenseAssignmentStateSerializer(const LicenseAssignmentState& p_LicenseAssignmentState);
	void Serialize() override;

private:
	LicenseAssignmentState m_LicenseAssignmentState;
};

