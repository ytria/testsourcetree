#pragma once

class PreAuthorizedApplication
{
public:
	boost::YOpt<PooledString> m_AppId;
	boost::YOpt<vector<PooledString>> m_DelegatedPermissionIds;
};

