#include "MailboxPermissions.h"

RTTR_REGISTRATION
{
	using namespace rttr;
registration::class_<MailboxPermissions>("MailboxPermissions") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("MailboxPermissions"))))
.constructor()(policy::ctor::as_object);
}

MailboxPermissions::MailboxPermissions()
	:m_IsInherited(false),
	m_IsValid(false)
{

}
