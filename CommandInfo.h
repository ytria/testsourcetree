#pragma once

#include "IActionCommand.h"
#include "MetaDataColumnInfo.h"
#include "PooledString.h"
#include "ordered_set.h"
#include "ordered_map.h"
#include "RoleDelegationUtil.h"

enum class Origin
{
    NotSet,
    User,
    Group,
    Site,
    Message,
    Tenant,
    Conversations,
    Post,
    Lists,
	Channel,
	DeletedUser,
	Schools,
};

class GridFrameBase;

#define USE_ORDERED_SET 1
#if USE_ORDERED_SET
using O365IdsContainer = tsl::ordered_set<PooledString>;
template<class T1, class T2>
using O365DataMap = tsl::ordered_map<T1, T2>;
#else
using O365IdsContainer = std::set<PooledString>;
template<class T1, class T2>
using O365DataMap = std::map<T1, T2>;
#endif

using DriveSubInfoIds = std::set<std::tuple<PooledString, PooledString, PooledString, PooledString>>;

struct SubIdKey : public std::pair<PooledString, PooledString>
{
	SubIdKey(PooledString p_Id)
	{
		first = p_Id;
	}
	SubIdKey(PooledString p_Id, PooledString p_DisplayName)
	{
		first = p_Id;
		second = p_DisplayName;
	}
	PooledString GetId() const
	{
		return first;
	}
	PooledString GetDisplayName() const
	{
		return second;
	}
};

template<>
struct std::hash<SubIdKey>
{
	size_t operator()(const SubIdKey& _Keyval) const
	{
		return fnv1a_hash_bytes((const unsigned char*)_Keyval.first.c_str(), ((wstring)_Keyval.first).size() * sizeof(wchar_t)) +
			fnv1a_hash_bytes((const unsigned char*)_Keyval.second.c_str(), ((wstring)_Keyval.second).size() * sizeof(wchar_t));
	}
};

using O365SubIdsContainer = O365DataMap<SubIdKey, O365IdsContainer>;
using O365SubSubIdsContainer = O365DataMap<PooledString, O365SubIdsContainer>;

struct CommandInfo
{
public:
	O365IdsContainer& GetIds()
	{
		return m_Ids;
	}

	const O365IdsContainer& GetIds() const
	{
		return m_Ids;
	}

    O365SubIdsContainer& GetSubIds()
    {
        return m_SubIds;
    }

    const O365SubIdsContainer& GetSubIds() const
    {
        return m_SubIds;
    }

    O365SubSubIdsContainer& GetSubSubIds()
    {
        return m_SubSubIDs;
    }

    const O365SubSubIdsContainer& GetSubSubIds() const
    {
        return m_SubSubIDs;
    }

	GridFrameBase* GetFrame() const
	{
		return m_ExistingFrame;
	}

	void SetFrame(GridFrameBase* p_ExistingFrame)
	{
		m_ExistingFrame = p_ExistingFrame;
	}
	
	Origin GetOrigin() const
	{
		return m_Origin;
	}

	void SetOrigin(Origin p_Origin)
	{
		m_Origin = p_Origin;
	}

    const PooledString& GetString() const
    {
        return m_String;
    }

    void SetString(const PooledString& p_String)
    {
        m_String = p_String;
    }

    const rttr::variant& Data() const
    {
        return m_Data;
    }

    rttr::variant& Data()
    {
        return m_Data;
    }

	const std::shared_ptr<IActionCommand>& GetActionCommand() const
	{
		return m_ActionCmd;
	}

	std::shared_ptr<IActionCommand>& GetActionCommand()
	{
		return m_ActionCmd;
	}

	const rttr::variant& Data2() const
	{
		return m_Data2;
	}

	rttr::variant& Data2()
	{
		return m_Data2;
	}

	void SetRBACPrivilege(const RoleDelegationUtil::RBAC_Privilege p_Privilege)
	{
		m_Privilege = p_Privilege;
	}
	
	RoleDelegationUtil::RBAC_Privilege GetRBACPrivilege() const
	{
		return m_Privilege;
	}

	void SetMetaDataColumnInfo(const boost::YOpt<MetaDataColumnInfos>& p_MetaDataColumnInfo)
	{
		m_MetaDataColumnInfo = p_MetaDataColumnInfo;
	}

	const boost::YOpt<MetaDataColumnInfos>& GetMetaDataColumnInfo() const
	{
		return m_MetaDataColumnInfo;
	}

private:
	O365IdsContainer					m_Ids;
    O365SubIdsContainer					m_SubIds;
    O365SubSubIdsContainer				m_SubSubIDs;
    GridFrameBase*						m_ExistingFrame = nullptr;
    Origin								m_Origin		= Origin::NotSet;
    PooledString						m_String;
    rttr::variant						m_Data;
	rttr::variant						m_Data2;
	std::shared_ptr<IActionCommand>		m_ActionCmd;
	RoleDelegationUtil::RBAC_Privilege	m_Privilege		= RoleDelegationUtil::RBAC_NONE;
	boost::YOpt<MetaDataColumnInfos>	m_MetaDataColumnInfo;
};