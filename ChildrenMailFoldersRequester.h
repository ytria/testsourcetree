#pragma once

#include "IRequester.h"

#include "MessageListRequester.h"

class BusinessMailFolder;
class MailFolderDeserializer;
class PaginatedRequestResults;

template <class T, class U>
class ValueListDeserializer;

class ChildrenMailFoldersRequester : public IRequester
{
public:
    ChildrenMailFoldersRequester(PooledString p_UserId, PooledString p_MailFolderId, bool p_IncludeMessages, const std::shared_ptr<IPageRequestLogger>& p_Logger);
    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	void SetMessagesCutOffDateTime(const wstring& p_CutOff);
	void SetMessagesCustomFilter(const ODataFilter& p_CustomFilter);

	void SetMessagesListCallback(const std::function<void(const MessageListRequester&)>&);

    vector<BusinessMailFolder>& GetData();

private:
    PooledString m_UserId;
    PooledString m_MailFolderId;
    bool m_IncludeMessages;
	wstring m_CutOffDateTime;
	ODataFilter m_CustomFilter;

    std::shared_ptr<ValueListDeserializer<BusinessMailFolder, MailFolderDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;

	std::function<void(const MessageListRequester&)> m_MessagesListCallback;
};

