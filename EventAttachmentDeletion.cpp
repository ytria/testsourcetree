#include "EventAttachmentDeletion.h"

#include "ActivityLoggerUtil.h"
#include "GridEvents.h"

EventAttachmentDeletion::EventAttachmentDeletion(GridEvents& p_Grid, const AttachmentInfo& p_Info) :
    ISubItemDeletion(	p_Grid, SubItemKey(p_Info.UserOrGroupID(), p_Info.EventOrMessageOrConversationID(), p_Info.AttachmentID()),
						p_Grid.m_ColAttachmentId,
						p_Grid.m_ColAttachmentId,
						p_Grid.GetName(p_Info.m_Row)),
    m_GridEvents(p_Grid),
    m_AttachmentInfo(p_Info)
{
}

bool EventAttachmentDeletion::IsCorrespondingRow(GridBackendRow* p_Row) const
{
    if (!IsCollapsedRow(p_Row))
    {
        // Exploded row
        if (p_Row->GetField(m_GridEvents.GetColMetaID()).HasValue() &&
            p_Row->GetField(m_GridEvents.m_ColId).HasValue() &&
            p_Row->GetField(m_GridEvents.m_ColAttachmentId).HasValue())
        {
            if (PooledString(p_Row->GetField(m_GridEvents.GetColMetaID()).GetValueStr()) == m_AttachmentInfo.UserOrGroupID() &&
                PooledString(p_Row->GetField(m_GridEvents.m_ColId).GetValueStr()) == m_AttachmentInfo.EventOrMessageOrConversationID() &&
                PooledString(p_Row->GetField(m_GridEvents.m_ColAttachmentId).GetValueStr()) == m_AttachmentInfo.AttachmentID())
            {
                return true;
            }
        }
    }
    else
    {
        // Collapsed row
        if (p_Row->GetField(m_GridEvents.GetColMetaID()).HasValue() &&
            p_Row->GetField(m_GridEvents.m_ColId).HasValue() &&
            p_Row->GetField(m_GridEvents.m_ColAttachmentId).HasValue())
        {
            if (PooledString(p_Row->GetField(m_GridEvents.GetColMetaID()).GetValueStr()) == m_AttachmentInfo.UserOrGroupID() &&
                PooledString(p_Row->GetField(m_GridEvents.m_ColId).GetValueStr()) == m_AttachmentInfo.EventOrMessageOrConversationID())
            {
                std::vector<PooledString>* attachmentIds = p_Row->GetField(m_GridEvents.m_ColAttachmentId).GetValuesMulti<PooledString>();
                if (nullptr != attachmentIds)
                {
                    for (const auto& attachId : *attachmentIds)
                    {
                        if (attachId == m_AttachmentInfo.AttachmentID())
                            return true;
                    }
                }
                else  if (PooledString(p_Row->GetField(m_GridEvents.m_ColAttachmentId).GetValueStr()) == m_AttachmentInfo.AttachmentID()) // Row with only one attachment
                {
                    return true;
                }
            }
        }
    }
    return false;
}

vector<Modification::ModificationLog> EventAttachmentDeletion::GetModificationLogs() const
{
	return { ModificationLog(getPk(), GetObjectName(), ActivityLoggerUtil::g_ActionDelete, _YTEXT("Attachment"), GetState()) };
}

GridBackendRow* EventAttachmentDeletion::GetMainRowIfCollapsed() const
{
	vector<GridBackendRow*> rows;
	m_GridEvents.GetRowsByCriteria(rows, [this](GridBackendRow* p_Row)
		{
			const auto metaId = p_Row->GetField(m_GridEvents.GetColMetaID()).GetValueStr();
			const auto id = p_Row->GetField(m_GridEvents.GetColId()).GetValueStr();
			return nullptr != metaId && m_AttachmentInfo.UserOrGroupID() == metaId
				&& nullptr != id && m_AttachmentInfo.EventOrMessageOrConversationID() == id;
		});

	if (rows.size() == 1 && IsCollapsedRow(rows.front()))
		return rows.front();

	return nullptr;
}
