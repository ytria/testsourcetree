#pragma once

#include "IModificationReverter.h"

class O365Grid;

class GenericModificationsReverter : public IModificationReverter
{
public:
	GenericModificationsReverter(O365Grid& p_Grid);

	void RevertAll() override;
	void RevertSelected() override;

	static bool RevertAllConfirm(CWnd* p_Parent);
	static bool RevertSelectedConfirm(CWnd* p_Parent);

private:
	O365Grid& m_Grid;
};
