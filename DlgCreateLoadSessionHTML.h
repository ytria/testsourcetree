#pragma once

#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "SessionIdentifier.h"
#include "YAdvancedHtmlView.h"

#include <memory>
#include "PersistentSession.h"

class ModuleBase;

class DlgCreateLoadSessionHTML	: public ResizableDialog
								, public YAdvancedHtmlView::IPostedDataTarget
{
public:
	DlgCreateLoadSessionHTML(const vector<PersistentSession>& p_Sessions, bool noUltraAdminOrRole = false);

	enum class Mode
	{
		LOAD_SESSION,
		NEW_ADVANCED_USER,
		NEW_ULTRA_ADMIN_OR_SKIP,
		NEW_STANDARD_USER
	};

	void SetIsElevating(bool p_IsPromoting);
	void SetNoNewSession(bool p_NoNewSession);

	void SetIntro(const wstring& p_IntroTitle, const wstring& p_IntroDesc);

    bool ShouldCreateStandardSession();
    bool ShouldCreateAdvancedSession();
	bool ShouldCreateUltraAdminSession();
	DlgCreateLoadSessionHTML::Mode GetWantedAction() const;
    SessionIdentifier GetSelectedSession() const;

	enum { IDD = IDD_DLG_DEFAULT_HTML };

	/* YAdvancedHtmlView::IPostedDataTarget */
	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data);

protected:
    virtual BOOL OnInitDialogSpecificResizable() override;
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    DECLARE_MESSAGE_MAP()

	void NewAdvancedSession();
	void NewStandardSession();

    void NewUltraAdminOrSkip();
	void LoadSession(const wstring& sessionName);

    ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output, const std::function<bool(int)>& p_IsHeightLimited);
	web::json::value getJSONSessionEntry(const PersistentSession& p_Session, bool p_IsCurrentSession);
	static UINT getHbsHeight(const wstring& hbsName);

private:
	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	static std::map<wstring, int, Str::keyLessInsensitive> g_HbsHeights;
	int m_totalHbsHeight;
	int m_MaxWidth;
	bool m_NoUltraAdminOrRole;
	bool m_IsElevating;
	bool m_NoNewSession;
	wstring m_IntroTitle;
	wstring m_IntroDesc;

	Mode m_Mode;
    wstring m_SessionToLoad;

	vector<PersistentSession> m_Sessions;

	static wstring g_NewAdmin;
	static wstring g_NewUltraAdmin;
	static wstring g_NewStandard;
};

