#pragma once

#include "IRequester.h"
#include "InvokeResultWrapper.h"
#include "OnPremiseUser.h"

class IRequestLogger;

class OnPremiseUserUpdateRequester : public IRequester
{
public:
	OnPremiseUserUpdateRequester(const OnPremiseUser& p_Users);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	std::shared_ptr<InvokeResultWrapper> GetResult();

	void SetAuthType(const boost::YOpt<wstring>& p_AuthType);
	void SetChangePasswordAtLogon(boost::YOpt<bool>& p_Change);

private:
	std::shared_ptr<InvokeResultWrapper> m_Result;
	std::shared_ptr<IPSObjectCollectionDeserializer> m_Deserializer;

	OnPremiseUser m_User;
	boost::YOpt<wstring> m_AuthType;
	boost::YOpt<bool> m_ChangePasswordAtLogon;
};

