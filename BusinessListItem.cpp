#include "BusinessListItem.h"

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessListItem>("ListItem") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("ListItem"))))
		.constructor()(policy::ctor::as_object);
}

BusinessListItem::BusinessListItem(const ListItem& p_JsonListItem)
{
    m_Id = p_JsonListItem.Id;
    m_Name = p_JsonListItem.Name;
    m_CreatedBy = p_JsonListItem.CreatedBy;
    m_CreatedDateTime = p_JsonListItem.CreatedDateTime;
    m_Description = p_JsonListItem.Description;
    m_LastModifiedBy = p_JsonListItem.LastModifiedBy;
    m_LastModifiedDateTime = p_JsonListItem.LastModifiedDateTime;
    m_WebUrl = p_JsonListItem.WebUrl;
    m_Fields = p_JsonListItem.Fields;
    m_ContentType = p_JsonListItem.ContentType;
}

const boost::YOpt<PooledString>& BusinessListItem::GetName() const
{
    return m_Name;
}

void BusinessListItem::SetName(const boost::YOpt<PooledString>& p_Val)
{
    m_Name = p_Val;
}

const boost::YOpt<IdentitySet>& BusinessListItem::GetCreatedBy() const
{
    return m_CreatedBy;
}

void BusinessListItem::SetCreatedBy(const boost::YOpt<IdentitySet>& p_Val)
{
    m_CreatedBy = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessListItem::GetCreatedDateTime() const
{
    return m_CreatedDateTime;
}

void BusinessListItem::SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_CreatedDateTime = p_Val;
}

const boost::YOpt<PooledString>& BusinessListItem::GetDescription() const
{
    return m_Description;
}

void BusinessListItem::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
    m_Description = p_Val;
}

const boost::YOpt<IdentitySet>& BusinessListItem::GetLastModifiedBy() const
{
    return m_LastModifiedBy;
}

void BusinessListItem::SetLastModifiedBy(const boost::YOpt<IdentitySet>& p_Val)
{
    m_LastModifiedBy = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessListItem::GetLastModifiedDateTime() const
{
    return m_LastModifiedDateTime;
}

void BusinessListItem::SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_LastModifiedDateTime = p_Val;
}

const boost::YOpt<PooledString>& BusinessListItem::GetWebUrl() const
{
    return m_WebUrl;
}

void BusinessListItem::SetWebUrl(const boost::YOpt<PooledString>& p_Val)
{
    m_WebUrl = p_Val;
}

const boost::YOpt<ContentTypeInfo>& BusinessListItem::GetContentType() const
{
    return m_ContentType;
}

void BusinessListItem::SetContentType(const boost::YOpt<ContentTypeInfo>& p_Val)
{
    m_ContentType = p_Val;
}

const boost::YOpt<FieldValueSet>& BusinessListItem::GetFields() const
{
    return m_Fields;
}

void BusinessListItem::SetFields(const boost::YOpt<FieldValueSet>& p_Val)
{
    m_Fields = p_Val;
}
