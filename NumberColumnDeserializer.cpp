#include "NumberColumnDeserializer.h"

#include "JsonSerializeUtil.h"

void NumberColumnDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("decimalPlaces"), m_Data.DecimalPlaces, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("displayAs"), m_Data.DisplayAs, p_Object);
    JsonSerializeUtil::DeserializeDouble(_YTEXT("maximum"), m_Data.Maximum, p_Object);
    JsonSerializeUtil::DeserializeDouble(_YTEXT("minimum"), m_Data.Minimum, p_Object);
}
