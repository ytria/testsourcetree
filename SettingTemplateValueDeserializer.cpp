#include "SettingTemplateValueDeserializer.h"

#include "JsonSerializeUtil.h"

void SettingTemplateValueDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("defaultValue"), m_Data.DefaultValue, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.Description, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.Name, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.Type, p_Object);
}
