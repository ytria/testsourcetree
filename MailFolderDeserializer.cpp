#include "MailFolderDeserializer.h"

#include "JsonSerializeUtil.h"
#include "MsGraphFieldNames.h"

void MailFolderDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeInt32(_YTEXT(O365_MAILFOLDER_CHILDFOLDERCOUNT), m_Data.m_ChildFolderCount, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_MAILFOLDER_DISPLAYNAME), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT(O365_ID), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_MAILFOLDER_PARENTFOLDERID), m_Data.m_ParentFolderId, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT(O365_MAILFOLDER_TOTALITEMCOUNT), m_Data.m_TotalItemCount, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT(O365_MAILFOLDER_UNREADITEMCOUNT), m_Data.m_UnreadItemCount, p_Object);
}
