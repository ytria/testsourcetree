#include "LocationDeserializer.h"

#include "JsonSerializeUtil.h"
#include "PhysicalAddressDeserializer.h"

void LocationDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        PhysicalAddressDeserializer pad;
        if (JsonSerializeUtil::DeserializeAny(pad, _YTEXT("address"), p_Object))
            m_Data.Address = pad.GetData();
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.DisplayName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("locationEmailAddress"), m_Data.LocationEmailAddress, p_Object);
}
