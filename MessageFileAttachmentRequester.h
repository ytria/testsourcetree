#pragma once

#include "IRequester.h"
#include "IRequestLogger.h"

class BusinessFileAttachment;
class FileAttachmentDeserializer;

class MessageFileAttachmentRequester : public IRequester
{
public:
    MessageFileAttachmentRequester(PooledString p_UserId, PooledString p_MessageId, PooledString p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger);

    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
    const BusinessFileAttachment& GetData() const;

private:
    std::shared_ptr<FileAttachmentDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;

    PooledString m_UserId;
    PooledString m_MessageId;
    PooledString m_AttachmentId;
};

