#pragma once

#include "IActionCommand.h"

class FrameGroups;

class ShowOnPremGroupsCommand : public IActionCommand
{
public:
	ShowOnPremGroupsCommand(FrameGroups& p_Frame);

	void ExecuteImpl() const override;

private:
	FrameGroups& m_Frame;
};

