#include "ModuleCriteria.h"

#include "JsonObjectDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"

namespace
{
	void Serialize(const wstring& p_PropName, const O365IdsContainer& p_Value, web::json::object& p_Json)
	{
		auto list = vector<web::json::value>();
		list.reserve(p_Value.size());
		for (const auto& elem : p_Value)
			list.push_back(web::json::value::string(elem));
		p_Json[p_PropName] = web::json::value::array(list);
	}

	void Serialize(const wstring& p_PropName, const O365SubIdsContainer& p_Value, web::json::object& p_Json)
	{
		auto list = vector<web::json::value>();
		list.reserve(p_Value.size());
		for (const auto& elem : p_Value)
		{
			web::json::value json = web::json::value::object();
			auto& obj = json.as_object();
			JsonSerializeUtil::SerializeString(_YTEXT("id"), elem.first.GetId(), obj);
			JsonSerializeUtil::SerializeString(_YTEXT("displayName"), elem.first.GetDisplayName(), obj);
			Serialize(_YTEXT("sub"), elem.second, obj);
			list.push_back(json);
		}
		p_Json[p_PropName] = web::json::value::array(list);
	}

	void Serialize(const wstring& p_PropName, const O365SubSubIdsContainer& p_Value, web::json::object& p_Json)
	{
		auto list = vector<web::json::value>();
		list.reserve(p_Value.size());
		for (const auto& elem : p_Value)
		{
			web::json::value json = web::json::value::object();
			auto& obj = json.as_object();
			JsonSerializeUtil::SerializeString(_YTEXT("id"), elem.first, obj);
			Serialize(_YTEXT("subsub"), elem.second, obj);
			list.push_back(json);
		}
		p_Json[p_PropName] = web::json::value::array(list);
	}


	/*
	{
		ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
		if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_GROUP_GROUPTYPES), p_Object))
			m_Data.m_GroupTypes = std::move(lsd.GetData());
	}
	*/

	bool Deserialize(const wstring& p_PropName, O365IdsContainer& p_Value, const web::json::object& p_Json)
	{
		bool success = false;

		ListStringDeserializer lsd;
		success = JsonSerializeUtil::DeserializeAny(lsd, p_PropName, p_Json);
		if (success)
			p_Value.insert(lsd.GetData().begin(), lsd.GetData().end());

		return success;
	}

	class SubIdContainerValueDeserializer : public JsonObjectDeserializer
	{
	public:
		SubIdContainerValueDeserializer()
			: m_Data({_YTEXT(""), _YTEXT("")}, {})
		{

		}

		virtual void DeserializeObject(const web::json::object& p_Object) override
		{
			JsonSerializeUtil::DeserializeString(_YTEXT("id"), m_Data.first.first, p_Object);
			JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.first.second, p_Object);
			::Deserialize(_YTEXT("sub"), m_Data.second, p_Object);
		}

		const O365SubIdsContainer::value_type& GetData() const
		{
			return m_Data;
		}

		O365SubIdsContainer::value_type& GetData()
		{
			return m_Data;
		}

		O365SubIdsContainer::value_type m_Data;
	};

	bool Deserialize(const wstring& p_PropName, O365SubIdsContainer& p_Value, const web::json::object& p_Json)
	{
		bool success = false;
		ListDeserializer<O365SubIdsContainer::value_type, SubIdContainerValueDeserializer> ld;
		success = JsonSerializeUtil::DeserializeAny(ld, p_PropName, p_Json);
		if (success)
			p_Value.insert(ld.GetData().begin(), ld.GetData().end());
		return success;
	}

	class SubSubIdContainerValueDeserializer : public JsonObjectDeserializer
	{
	public:
		virtual void DeserializeObject(const web::json::object& p_Object) override
		{
			JsonSerializeUtil::DeserializeString(_YTEXT("id"), m_Data.first, p_Object);
			::Deserialize(_YTEXT("subsub"), m_Data.second, p_Object);
		}

		const O365SubSubIdsContainer::value_type& GetData() const
		{
			return m_Data;
		}

		O365SubSubIdsContainer::value_type& GetData()
		{
			return m_Data;
		}

		O365SubSubIdsContainer::value_type m_Data;
	};

	bool Deserialize(const wstring& p_PropName, O365SubSubIdsContainer& p_Value, const web::json::object& p_Json)
	{
		bool success = false;
		ListDeserializer<O365SubSubIdsContainer::value_type, SubSubIdContainerValueDeserializer> ld;
		success = JsonSerializeUtil::DeserializeAny(ld, p_PropName, p_Json);
		if (success)
			p_Value.insert(ld.GetData().begin(), ld.GetData().end());
		return success;
	}
}

bool ModuleCriteria::Matches(const ModuleCriteria& other) const
{
    if (m_Origin != other.m_Origin || m_UsedContainer != other.m_UsedContainer || m_Privilege != other.m_Privilege)
        return false;

    return	UsedContainer::O365REPORTCRITERIA	!= m_UsedContainer // reports are never the same.
		&&
			(UsedContainer::NOTSET				== m_UsedContainer ||
	        UsedContainer::IDS					== m_UsedContainer && m_IDs == other.m_IDs ||
			UsedContainer::SUBIDS				== m_UsedContainer && m_SubIDs == other.m_SubIDs ||
			UsedContainer::SUBSUBIDS			== m_UsedContainer && m_SubSubIDs == other.m_SubSubIDs)
		&&	m_MetaDataColumnInfo == other.m_MetaDataColumnInfo;
}

wstring ModuleCriteria::Serialize() const
{
	web::json::value json = web::json::value::object();
	auto& obj = json.as_object();

	JsonSerializeUtil::SerializeUint32(_YTEXT("origin"), static_cast<uint32_t>(m_Origin), obj);
	JsonSerializeUtil::SerializeUint32(_YTEXT("usedContainer"), static_cast<uint32_t>(m_UsedContainer), obj);

	switch (m_UsedContainer)
	{
	case UsedContainer::NOTSET:
		break;
	case UsedContainer::IDS:
		::Serialize(_YTEXT("ids"), m_IDs, obj);
		break;
	case UsedContainer::SUBIDS:
		::Serialize(_YTEXT("subids"), m_SubIDs, obj);
		break;
	case UsedContainer::SUBSUBIDS:
		::Serialize(_YTEXT("subsubids"), m_SubSubIDs, obj);
		break;
	case UsedContainer::O365REPORTCRITERIA:
		obj[_YTEXT("o365reportcriteria")] = m_ReportCriteria.Serialize();
		break;
	default:
		ASSERT(false);
		break;
	}

	if (m_MetaDataColumnInfo)
	{
		vector<web::json::value> values;
		for (const auto& c : *m_MetaDataColumnInfo)
			values.push_back(c.Serialize());
		obj[_YTEXT("metaDataColumnInfo")] = web::json::value::array(values);
	}

	// Don't save RBAC privilege.
	// This Serialize method has been made for Snapshot which don't save Out-of-scope rows.
	// Whatever the reader's privilege, snapshot always only shows what the creator's privilege allowed.
	//m_Privilege

	return json.to_string();
}

bool ModuleCriteria::Deserialize(const wstring& p_String)
{
	bool success = true;

	auto json = web::json::value::parse(p_String);

	ASSERT(json.is_object());
	if (json.is_object())
	{
		const auto& obj = json.as_object();
		if (!obj.empty())
		{
			{
				boost::YOpt<uint32_t> orig;
				JsonSerializeUtil::DeserializeUint32(_YTEXT("origin"), orig, obj);
				ASSERT(orig);
				if (orig)
				{
					ASSERT(static_cast<uint32_t>(Origin::NotSet) <= *orig && *orig <= static_cast<uint32_t>(Origin::Schools));
					if (static_cast<uint32_t>(Origin::NotSet) <= *orig && *orig <= static_cast<uint32_t>(Origin::Schools))
						m_Origin = static_cast<Origin>(*orig);
				}
			}
			{
				boost::YOpt<uint32_t> cont;
				JsonSerializeUtil::DeserializeUint32(_YTEXT("usedContainer"), cont, obj);
				ASSERT(cont);
				if (cont)
				{
					ASSERT(static_cast<uint32_t>(UsedContainer::NOTSET) <= *cont && *cont <= static_cast<uint32_t>(UsedContainer::O365REPORTCRITERIA));
					if (static_cast<uint32_t>(UsedContainer::NOTSET) <= *cont && *cont <= static_cast<uint32_t>(UsedContainer::O365REPORTCRITERIA))
						m_UsedContainer = static_cast<UsedContainer>(*cont);
				}
			}

			switch (m_UsedContainer)
			{
			case UsedContainer::NOTSET:
				break;
			case UsedContainer::IDS:
				{
					const auto b = ::Deserialize(_YTEXT("ids"), m_IDs, obj);
					ASSERT(b);
				}
				break;
			case UsedContainer::SUBIDS:
				{
					const auto b = ::Deserialize(_YTEXT("subids"), m_SubIDs, obj);
					ASSERT(b);
				}
				break;
			case UsedContainer::SUBSUBIDS:
				{
					const auto b = ::Deserialize(_YTEXT("subsubids"), m_SubSubIDs, obj);
					ASSERT(b);
				}
				break;
			case UsedContainer::O365REPORTCRITERIA:
				{
					auto it = obj.find(_YTEXT("o365reportcriteria"));
					if (it != obj.end() && !it->second.is_null())
						m_ReportCriteria.Deserialize(it->second);
					else
						ASSERT(false);
				}
				break;
			default:
				ASSERT(false);
				break;
			}

			{
				auto it = obj.find(_YTEXT("metaDataColumnInfo"));
				if (obj.end() != it)
				{
					auto& val = it->second;
					ASSERT(val.is_array());
					if (val.is_array())
					{
						auto& vals = val.as_array();
						if (vals.size() > 0)
						{
							m_MetaDataColumnInfo.emplace();
							for (auto& v : vals)
							{
								auto dv = MetaDataColumnInfo::Deserialize(v);
								if (dv)
									m_MetaDataColumnInfo->push_back(*dv);
							}
						}
					}
				}
			}
		}
		else
		{
			success = false;
		}
	}
	else
	{
		success = false;
	}

	return success;
}

