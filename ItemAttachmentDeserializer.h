#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessItemAttachment.h"

class ItemAttachmentDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessItemAttachment>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

