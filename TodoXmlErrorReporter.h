#include "xercesc\sax\ErrorHandler.hpp"
#include "xercesc\sax\SAXParseException.hpp"

class TodoXmlErrorReporter : public XERCES_CPP_NAMESPACE::ErrorHandler
{
public:
    TodoXmlErrorReporter();

    void warning(const XERCES_CPP_NAMESPACE::SAXParseException& p_Exception) override;
    void error(const XERCES_CPP_NAMESPACE::SAXParseException& p_Exception) override;
    void fatalError(const XERCES_CPP_NAMESPACE::SAXParseException& p_Exception) override;
    void resetErrors() override;

    bool HasError() const;

private:
    bool m_HasError;
};
