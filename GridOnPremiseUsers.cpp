#include "GridOnPremiseUsers.h"

#include "AutomationWizardOnPremiseUsers.h"
#include "BasicGridSetup.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"
#include "FrameOnPremiseUsers.h"
#include "OnPremiseUser.h"

BEGIN_MESSAGE_MAP(GridOnPremiseUsers, O365Grid)
	ON_COMMAND(ID_LICENSESGRID_SHOWUNASSIGNED, OnEditUsers)
	ON_UPDATE_COMMAND_UI(ID_LICENSESGRID_SHOWUNASSIGNED, OnUpdateEditUsers)
END_MESSAGE_MAP()

GridOnPremiseUsers::GridOnPremiseUsers()
	: m_Frame(nullptr),
	m_ColLastRequestDateTime(nullptr),
	m_ColAccountExpires(nullptr),
	m_ColAccountNotDelegated(nullptr),
	m_ColAccountExpirationDate(nullptr),
	m_ColAccountLockoutTime(nullptr),
	m_ColAdminCount(nullptr),
	m_ColAllowReversiblePasswordEncryption(nullptr),
	m_ColAuthenticationPolicy(nullptr),
	m_ColAuthenticationPolicySilo(nullptr),
	m_ColBadLogonCount(nullptr),
	m_ColBadPasswordTime(nullptr),
	m_ColBadPwdCount(nullptr),
	m_ColCannotChangePassword(nullptr),
	m_ColCanonicalName(nullptr),
	m_ColCertificates(nullptr),
	m_ColCity(nullptr),
	m_ColCN(nullptr),
	m_ColCodePage(nullptr),
	m_ColCompany(nullptr),
	m_ColCompoundIdentitySupported(nullptr),
	m_ColCountry(nullptr),
	m_ColCountryCode(nullptr),
	m_ColCreated(nullptr),
	m_ColCreateTimeStamp(nullptr),
	m_ColDeleted(nullptr),
	m_ColDepartment(nullptr),
	m_ColDescription(nullptr),
	m_ColDisplayName(nullptr),
	m_ColDistinguishedName(nullptr),
	m_ColDivision(nullptr),
	m_ColDoesNotRequirePreAuth(nullptr),
	m_ColDSCorePropagationData(nullptr),
	m_ColEmailAddress(nullptr),
	m_ColEmployeeID(nullptr),
	m_ColEmployeeNumber(nullptr),
	m_ColEnabled(nullptr),
	m_ColFax(nullptr),
	m_ColGivenName(nullptr),
	m_ColHomeDirectory(nullptr),
	m_ColHomedirRequired(nullptr),
	m_ColHomeDrive(nullptr),
	m_ColHomePage(nullptr),
	m_ColHomePhone(nullptr),
	m_ColInitials(nullptr),
	m_ColInstanceType(nullptr),
	m_ColIsCriticalSystemObject(nullptr),
	m_ColIsDeleted(nullptr),
	m_ColKerberosEncryptionType(nullptr),
	m_ColLastBadPasswordAttempt(nullptr),
	m_ColLastKnownParent(nullptr),
	m_ColLastLogoff(nullptr),
	m_ColLastLogon(nullptr),
	m_ColLogonHours(nullptr),
	m_ColLastLogonDate(nullptr),
	m_ColLastLogonTimestamp(nullptr),
	m_ColLockedOut(nullptr),
	m_ColLogonCount(nullptr),
	m_ColLogonWorkstations(nullptr),
	m_ColManager(nullptr),
	m_ColMemberOf(nullptr),
	m_ColModified(nullptr),
	m_ColModifyTimeStamp(nullptr),
	m_ColMNSLogonAccount(nullptr),
	m_ColMobilePhone(nullptr),
	m_ColMSDSUserAccountControlComputed(nullptr),
	m_ColName(nullptr),
	m_ColNTSecurityDescriptor(nullptr),
	m_ColObjectCategory(nullptr),
	m_ColObjectClass(nullptr),
	m_ColObjectGUID(nullptr),
	m_ColObjectSid(nullptr),
	m_ColOffice(nullptr),
	m_ColOfficePhone(nullptr),
	m_ColOrganization(nullptr),
	m_ColOtherName(nullptr),
	m_ColPasswordExpired(nullptr),
	m_ColPasswordLastSet(nullptr),
	m_ColPasswordNeverExpires(nullptr),
	m_ColPasswordNotRequired(nullptr),
	m_ColPOBox(nullptr),
	m_ColPostalCode(nullptr),
	m_ColPrimaryGroup(nullptr),
	m_ColPrimaryGroupId(nullptr),
	m_ColPrincipalsAllowedToDelegateToAccount(nullptr),
	m_ColProfilePath(nullptr),
	m_ColProtectedFromAccidentalDeletion(nullptr),
	m_ColPwdLastSet(nullptr),
	m_ColSamAccountName(nullptr),
	m_ColSAMAccountType(nullptr),
	m_ColScriptPath(nullptr),
	m_ColSDRightsEffective(nullptr),
	m_ColServicePrincipalNames(nullptr),
	m_ColSID(nullptr),
	m_ColSIDHistory(nullptr),
	m_ColSmartcardLogonRequired(nullptr),
	m_ColState(nullptr),
	m_ColStreetAddress(nullptr),
	m_ColSurname(nullptr),
	m_ColTitle(nullptr),
	m_ColTrustedForDelegation(nullptr),
	m_ColTrustedToAuthForDelegation(nullptr),
	m_ColUseDESKeyOnly(nullptr),
	m_ColUserAccountControl(nullptr),
	m_ColUserCertificate(nullptr),
	m_ColUserPrincipalName(nullptr),
	m_ColUSNChanged(nullptr),
	m_ColUSNCreated(nullptr),
	m_ColWhenChanged(nullptr),
	m_ColWhenCreated(nullptr)
{
	UseDefaultActions(O365Grid::ACTION_EDIT);
	EnableGridModifications();
	initWizard<AutomationWizardOnPremiseUsers>(_YTEXT("Automation\\OnPremiseUsers"));
}

void GridOnPremiseUsers::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameOnPremiseUsers, LicenseUtil::g_codeSapio365onPremiseUsers);
		setup.Setup(*this, true);
		m_ColLastRequestDateTime = setup.GetColumnLastRequestDateTime();
	}

	setBasicGridSetupHierarchy({ { { m_ColDistinguishedName, 0 } }
								, { rttr::type::get<OnPremiseUser>().get_id() }
								, { rttr::type::get<OnPremiseUser>().get_id() } });

	m_ColSamAccountName = AddColumn(_YUID("samAccountName"), _T("Sam Account Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAccountExpires = AddColumnDate(_YUID("accountExpires"), _T("Account Expires"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColAccountLockoutTime = AddColumnDate(_YUID("accountLockoutTime"), _T("Account Lockout Time"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColAccountNotDelegated = AddColumnCheckBox(_YUID("accountNotDelegated"), _T("Account not delegated"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColAdminCount = AddColumn(_YUID("adminCount"), _T("Admin Count"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColAllowReversiblePasswordEncryption = AddColumnCheckBox(_YUID("allowReversiblePasswordEncryption"), _T("Allow Reversible Password Encryption"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColAuthenticationPolicy = AddColumn(_YUID("authenticationPolicy"), _T("Authentication Policy"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAuthenticationPolicySilo = AddColumn(_YUID("authenticationPolicySilo"), _T("Authentication Policy Silo"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColBadLogonCount = AddColumn(_YUID("badLogonCount"), _T("Bad Logon Count"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColBadPasswordTime = AddColumnDate(_YUID("badPasswordTime"), _T("Bad Password Time"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColBadPwdCount = AddColumn(_YUID("badPwdCount"), _T("Bad-Pwd-Count"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColCannotChangePassword = AddColumnCheckBox(_YUID("cannotChangePassword"), _T("Cannot Change Password"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColCanonicalName = AddColumn(_YUID("canonicalName"), _T("Canonical Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColCertificates = AddColumn(_YUID("certificates"), _T("Certificates"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColCity = AddColumn(_YUID("city"), _T("City"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColCN = AddColumn(_YUID("CN"), _T("CN"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColCodePage = AddColumn(_YUID("codePage"), _T("Code Page"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColCompany = AddColumn(_YUID("company"), _T("Company"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColCompoundIdentitySupported = AddColumn(_YUID("compoundIdentitySupported"), _T("Compound Identity Supported"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColCountry = AddColumn(_YUID("country"), _T("Country"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColCountryCode = AddColumn(_YUID("countryCode"), _T("Country Code"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColCreated = AddColumnDate(_YUID("created"), _T("Created"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreateTimeStamp = AddColumnDate(_YUID("createTimeStamp"), _T("Create TimeStamp"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColDeleted = AddColumnCheckBox(_YUID("deleted"), _T("Deleted"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColDepartment = AddColumn(_YUID("department"), _T("Department"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColDescription = AddColumn(_YUID("description"), _T("Description"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColDisplayName = AddColumn(_YUID("displayName"), _T("Display Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColDistinguishedName = AddColumn(_YUID("distinguishedName"), _T("Distinguished Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColDivision = AddColumn(_YUID("division"), _T("Division"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColDoesNotRequirePreAuth = AddColumnCheckBox(_YUID("doesNotRequirePreAuth"), _T("Does not require PreAuth"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColDSCorePropagationData = AddColumnDate(_YUID("dsCorePropagationData"), _T("dsCorePropagationData"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColEmailAddress = AddColumn(_YUID("emailAddress"), _T("Email Address"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColEmployeeID = AddColumn(_YUID("employeeID"), _T("Employee ID"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColEmployeeNumber = AddColumn(_YUID("employeeNumber"), _T("Employee Number"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColEnabled = AddColumnCheckBox(_YUID("enabled"), _T("Enabled"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColFax = AddColumn(_YUID("fax"), _T("Fax"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColGivenName = AddColumn(_YUID("givenName"), _T("Given Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColHomeDirectory = AddColumn(_YUID("homeDirectory"), _T("Home Directory"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColHomedirRequired = AddColumnCheckBox(_YUID("homeDirRequired"), _T("Homedir Required"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColHomeDrive = AddColumn(_YUID("homeDrive"), _T("Home Drive"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColHomePage = AddColumn(_YUID("homePage"), _T("Home Page"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColHomePhone = AddColumn(_YUID("homePhone"), _T("Home Phone"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColInitials = AddColumn(_YUID("initials"), _T("Initials"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColInstanceType = AddColumn(_YUID("instanceType"), _T("Instance Type"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColIsCriticalSystemObject = AddColumnCheckBox(_YUID("isCriticalSystemObject"), _T("Is Critical System Object"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColIsDeleted = AddColumnCheckBox(_YUID("isDeleted"), _T("Is Deleted"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColKerberosEncryptionType = AddColumn(_YUID("kerberosEncryptionType"), _T("Kerberos Encryption Type"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColLastBadPasswordAttempt = AddColumnDate(_YUID("lastBadPasswordAttempt"), _T("Last Bad Password Attempt"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColLastKnownParent = AddColumn(_YUID("lastKnownParent"), _T("Last known parent"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColLastLogoff = AddColumnDate(_YUID("lastLogoff"), _T("Last Logoff"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColLastLogon = AddColumnDate(_YUID("lastLogon"), _T("Last Logon"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColLastLogonDate = AddColumnDate(_YUID("lastLogonDate"), _T("Last Logon Date"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColLastLogonTimestamp = AddColumnDate(_YUID("lastLogonTimeStamp"), _T("Last Logon Timestamp"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColLockedOut = AddColumnCheckBox(_YUID("lockedOut"), _T("Locked Out"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColLogonCount = AddColumn(_YUID("logonCount"), _T("Logon Count"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColLogonHours = AddColumn(_YUID("logonHours"), _T("Logon Hours"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColLogonWorkstations = AddColumn(_YUID("logonWorkstations"), _T("Logon Workstations"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColManager = AddColumn(_YUID("manager"), _T("Manager"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColMemberOf = AddColumn(_YUID("memberOf"), _T("Member Of"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColMNSLogonAccount = AddColumnCheckBox(_YUID("mnsLogonAccount"), _T("MNS Logon Account"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColMobilePhone = AddColumn(_YUID("phone"), _T("Phone"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColModified = AddColumnDate(_YUID("modified"), _T("Modified"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColModifyTimeStamp = AddColumnDate(_YUID("modifyTimeStamp"), _T("Modify Timestamp"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMSDSUserAccountControlComputed = AddColumn(_YUID("msdsUserAccountControlComputed"), _T("MsDS User Account Control Computed"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColName = AddColumn(_YUID("name"), _T("Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColNTSecurityDescriptor = AddColumn(_YUID("nTSecurityDescriptor"), _T("nTSecurityDescriptor"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColObjectCategory = AddColumn(_YUID("objectCategory"), _T("Object Category"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColObjectClass = AddColumn(_YUID("objectClass"), _T("Object Class"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColObjectGUID = AddColumn(_YUID("objectGUID"), _T("Object GUID"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColObjectSid = AddColumn(_YUID("objectSid"), _T("Object Sid"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOffice = AddColumn(_YUID("office"), _T("Office"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOfficePhone = AddColumn(_YUID("officePhone"), _T("Office Phone"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOrganization = AddColumn(_YUID("organization"), _T("Organization"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOtherName = AddColumn(_YUID("otherName"), _T("Other Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColPasswordExpired = AddColumnCheckBox(_YUID("passwordExpired"), _T("Password Expired"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColPasswordLastSet = AddColumnDate(_YUID("passwordLastSet"), _T("Password Last Set"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColPasswordNeverExpires = AddColumnCheckBox(_YUID("passwordNeverExpires"), _T("Password never expires"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColPasswordNotRequired = AddColumnCheckBox(_YUID("passwordNotRequired"), _T("Password not required"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColPOBox = AddColumn(_YUID("poBox"), _T("PO Box"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColPostalCode = AddColumn(_YUID("postalCode"), _T("Postal Code"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColPrimaryGroup = AddColumn(_YUID("primaryGroup"), _T("Primary Group"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColPrimaryGroupId = AddColumn(_YUID("primaryGroupId"), _T("Primary Group ID"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColPrincipalsAllowedToDelegateToAccount = AddColumn(_YUID("principalsAllowedToDelegateToAccount"), _T("Principals allowed to delegate to account"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColProfilePath = AddColumn(_YUID("profilePath"), _T("Profile Path"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColProtectedFromAccidentalDeletion = AddColumnCheckBox(_YUID("protectedFromAccidentalDeletion"), _T("Protected from accidental deletion"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColPwdLastSet = AddColumnDate(_YUID("pwdLastSet"), _T("Pwd Last Set"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColSAMAccountType = AddColumn(_YUID("sAMAccountType"), _T("sAM Account Type"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColServicePrincipalNames = AddColumn(_YUID("servicePrincipalNames"), _T("Service Principal Names"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColScriptPath = AddColumn(_YUID("scriptPath"), _T("Script Path"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColSDRightsEffective = AddColumn(_YUID("sDRightsEffective"), _T("SD Rights Effective"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColSID = AddColumn(_YUID("sid"), _T("SID"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColSIDHistory = AddColumn(_YUID("sidHistory"), _T("SID History"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColSmartcardLogonRequired = AddColumnCheckBox(_YUID("smartCardLogonRequired"), _T("SmartCard logon required"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColState = AddColumn(_YUID("state"), _T("State"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColStreetAddress = AddColumn(_YUID("streetAddress"), _T("Street Address"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColSurname = AddColumn(_YUID("surname"), _T("Surname"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColTitle = AddColumn(_YUID("title"), _T("Title"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColTrustedForDelegation = AddColumnCheckBox(_YUID("trustedForDelegation"), _T("Trusted for delegation"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColTrustedToAuthForDelegation = AddColumnCheckBox(_YUID("trustedToAuthForDelegation"), _T("Trusted to auth for delegation"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColUseDESKeyOnly = AddColumnCheckBox(_YUID("useDESKeyOnly"), _T("Use DES key only"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColUserAccountControl = AddColumn(_YUID("userAccountControl"), _T("User Account Control"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColUserCertificate = AddColumn(_YUID("user Certificate"), _T("User Certificate"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColUserPrincipalName = AddColumn(_YUID("userPrincipalName"), _T("User Principal Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColUSNChanged = AddColumn(_YUID("uSNChanged"), _T("uSNChanged"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColUSNCreated = AddColumn(_YUID("uSNCreated"), _T("uSNCreated"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColWhenChanged = AddColumnDate(_YUID("whenChanged"), _T("When Changed"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColWhenCreated = AddColumnDate(_YUID("whenCreated"), _T("When Created"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	addColumnGraphID();
	AddColumnForRowPK(GetColId());

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameOnPremiseUsers*>(GetParentFrame()));
}

void GridOnPremiseUsers::BuildView(const vector<OnPremiseUser>& p_Users, bool p_FullPurge)
{
	if (p_Users.size() == 1 && p_Users[0].GetError() != boost::none && p_Users[0].GetID().IsEmpty())
	{
		HandlePostUpdateError(p_Users[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_Users[0].GetError()->GetFullErrorMessage());
	}
	else
	{
		GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
			| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			| GridUpdaterOptions::REFRESH_PROCESS_DATA
		);
		GridUpdater updater(*this, options);

		for (const auto& onPremUser : p_Users)
		{
			GridBackendField fID(onPremUser.GetObjectGUID(), GetColId());
			GridBackendRow* row = m_RowIndex.GetRow({ fID }, true, true);

			SetRowObjectType(row, onPremUser);
			FillRow(row, onPremUser);
		}
	}
}

OnPremiseUser GridOnPremiseUsers::getBusinessObject(GridBackendRow*) const
{
	return OnPremiseUser();
}

void GridOnPremiseUsers::UpdateBusinessObjects(const vector<OnPremiseUser>& p_Permissions, bool p_SetModifiedStatus)
{

}

void GridOnPremiseUsers::RemoveBusinessObjects(const vector<OnPremiseUser>& p_Permissions)
{

}

wstring GridOnPremiseUsers::GetName(const GridBackendRow* p_Row) const
{
	return wstring();
}

RoleDelegationUtil::RBAC_Privilege GridOnPremiseUsers::GetPrivilegeEdit() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;
}

GridBackendRow* GridOnPremiseUsers::FillRow(GridBackendRow* p_Row, const OnPremiseUser& p_OnPremiseUser)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		AddDateOrNeverField(p_OnPremiseUser.GetAccountExpires() , *p_Row, *m_ColAccountExpires);
		AddDateOrNeverField(p_OnPremiseUser.GetAccountLockoutTime(), *p_Row, *m_ColAccountLockoutTime);
		p_Row->AddField(p_OnPremiseUser.GetAccountNotDelegated(), m_ColAccountNotDelegated);
		p_Row->AddField(p_OnPremiseUser.GetAdminCount(), m_ColAdminCount);
		p_Row->AddField(p_OnPremiseUser.GetAllowReversiblePasswordEncryption(), m_ColAllowReversiblePasswordEncryption);
		p_Row->AddField(p_OnPremiseUser.GetAuthenticationPolicy(), m_ColAuthenticationPolicy);
		p_Row->AddField(p_OnPremiseUser.GetAuthenticationPolicySilo(), m_ColAuthenticationPolicySilo);
		p_Row->AddField(p_OnPremiseUser.GetBadLogonCount(), m_ColBadLogonCount);
		AddDateOrNeverField(p_OnPremiseUser.GetBadPasswordTime(), *p_Row, *m_ColBadPasswordTime);
		p_Row->AddField(p_OnPremiseUser.GetBadPwdCount(), m_ColBadPwdCount);
		p_Row->AddField(p_OnPremiseUser.GetCannotChangePassword(), m_ColCannotChangePassword);
		p_Row->AddField(p_OnPremiseUser.GetCanonicalName(), m_ColCanonicalName);
		p_Row->AddField(p_OnPremiseUser.GetCertificates(), m_ColCertificates);
		p_Row->AddField(p_OnPremiseUser.GetCity(), m_ColCity);
		p_Row->AddField(p_OnPremiseUser.GetCN(), m_ColCN);
		p_Row->AddField(p_OnPremiseUser.GetCodePage(), m_ColCodePage);
		p_Row->AddField(p_OnPremiseUser.GetCompany(), m_ColCompany);
		p_Row->AddField(p_OnPremiseUser.GetCompoundIdentitySupported(), m_ColCompoundIdentitySupported);
		p_Row->AddField(p_OnPremiseUser.GetCountry(), m_ColCountry);
		p_Row->AddField(p_OnPremiseUser.GetCountryCode(), m_ColCountryCode);
		AddDateOrNeverField(p_OnPremiseUser.GetCreated(), *p_Row, *m_ColCreated);
		p_Row->AddField(p_OnPremiseUser.GetDeleted(), m_ColDeleted);
		p_Row->AddField(p_OnPremiseUser.GetDepartment(), m_ColDepartment);
		p_Row->AddField(p_OnPremiseUser.GetDescription(), m_ColDescription);
		p_Row->AddField(p_OnPremiseUser.GetDisplayName(), m_ColDisplayName);
		p_Row->AddField(p_OnPremiseUser.GetDistinguishedName(), m_ColDistinguishedName);
		p_Row->AddField(p_OnPremiseUser.GetDivision(), m_ColDivision);
		p_Row->AddField(p_OnPremiseUser.GetDoesNotRequirePreAuth(), m_ColDoesNotRequirePreAuth);
		p_Row->AddField(p_OnPremiseUser.GetdSCorePropagationData(), m_ColDSCorePropagationData);
		p_Row->AddField(p_OnPremiseUser.GetEmailAddress(), m_ColEmailAddress);
		p_Row->AddField(p_OnPremiseUser.GetEmployeeID(), m_ColEmployeeID);
		p_Row->AddField(p_OnPremiseUser.GetEmployeeNumber(), m_ColEmployeeNumber);
		p_Row->AddField(p_OnPremiseUser.GetEnabled(), m_ColEnabled);
		p_Row->AddField(p_OnPremiseUser.GetFax(), m_ColFax);
		p_Row->AddField(p_OnPremiseUser.GetGivenName(), m_ColGivenName);
		p_Row->AddField(p_OnPremiseUser.GetHomeDirectory(), m_ColHomeDirectory);
		p_Row->AddField(p_OnPremiseUser.GetHomedirRequired(), m_ColHomedirRequired);
		p_Row->AddField(p_OnPremiseUser.GetHomeDrive(), m_ColHomeDrive);
		p_Row->AddField(p_OnPremiseUser.GetHomePage(), m_ColHomePage);
		p_Row->AddField(p_OnPremiseUser.GetHomePhone(), m_ColHomePhone);
		p_Row->AddField(p_OnPremiseUser.GetInitials(), m_ColInitials);
		p_Row->AddField(p_OnPremiseUser.GetInstanceType(), m_ColInstanceType);
		p_Row->AddField(p_OnPremiseUser.GetIsCriticalSystemObject(), m_ColIsCriticalSystemObject);
		p_Row->AddField(p_OnPremiseUser.GetIsDeleted(), m_ColIsDeleted);
		p_Row->AddField(p_OnPremiseUser.GetKerberosEncryptionType(), m_ColKerberosEncryptionType);
		AddDateOrNeverField(p_OnPremiseUser.GetLastBadPasswordAttempt(), *p_Row, *m_ColLastBadPasswordAttempt);
		p_Row->AddField(p_OnPremiseUser.GetLastKnownParent(), m_ColLastKnownParent);
		AddDateOrNeverField(p_OnPremiseUser.GetLastLogoff(), *p_Row, *m_ColLastLogoff);
		AddDateOrNeverField(p_OnPremiseUser.GetLastLogon(), *p_Row, *m_ColLastLogon);
		AddDateOrNeverField(p_OnPremiseUser.GetLastLogonDate(), *p_Row, *m_ColLastLogonDate);
		p_Row->AddField(p_OnPremiseUser.GetLockedOut(), m_ColLockedOut);
		p_Row->AddField(p_OnPremiseUser.GetLogonCount(), m_ColLogonCount);
		p_Row->AddField(p_OnPremiseUser.GetLogonHours(), m_ColLogonHours);
		p_Row->AddField(p_OnPremiseUser.GetLogonWorkstations(), m_ColLogonWorkstations);
		p_Row->AddField(p_OnPremiseUser.GetManager(), m_ColManager);
		p_Row->AddField(p_OnPremiseUser.GetMemberOf(), m_ColMemberOf);
		p_Row->AddField(p_OnPremiseUser.GetMNSLogonAccount(), m_ColMNSLogonAccount);
		p_Row->AddField(p_OnPremiseUser.GetMobilePhone(), m_ColMobilePhone);
		AddDateOrNeverField(p_OnPremiseUser.GetModified(), *p_Row, *m_ColModified);
		p_Row->AddField(p_OnPremiseUser.GetMSDSUserAccountControlComputed(), m_ColMSDSUserAccountControlComputed);
		p_Row->AddField(p_OnPremiseUser.GetName(), m_ColName);
		p_Row->AddField(p_OnPremiseUser.GetTSecurityDescriptor(), m_ColNTSecurityDescriptor);
		p_Row->AddField(p_OnPremiseUser.GetObjectCategory(), m_ColObjectCategory);
		p_Row->AddField(p_OnPremiseUser.GetObjectClass(), m_ColObjectClass);
		p_Row->AddField(p_OnPremiseUser.GetObjectGUID(), m_ColObjectGUID);
		p_Row->AddField(p_OnPremiseUser.GetObjectSid(), m_ColObjectSid);
		p_Row->AddField(p_OnPremiseUser.GetOffice(), m_ColOffice);
		p_Row->AddField(p_OnPremiseUser.GetOfficePhone(), m_ColOfficePhone);
		p_Row->AddField(p_OnPremiseUser.GetOrganization(), m_ColOrganization);
		p_Row->AddField(p_OnPremiseUser.GetOtherName(), m_ColOtherName);
		p_Row->AddField(p_OnPremiseUser.GetPasswordExpired(), m_ColPasswordExpired);
		AddDateOrNeverField(p_OnPremiseUser.GetPasswordLastSet(), *p_Row, *m_ColPasswordLastSet);
		p_Row->AddField(p_OnPremiseUser.GetPasswordNeverExpires(), m_ColPasswordNeverExpires);
		p_Row->AddField(p_OnPremiseUser.GetPasswordNotRequired(), m_ColPasswordNotRequired);
		p_Row->AddField(p_OnPremiseUser.GetPOBox(), m_ColPOBox);
		p_Row->AddField(p_OnPremiseUser.GetPostalCode(), m_ColPostalCode);
		p_Row->AddField(p_OnPremiseUser.GetPrimaryGroup(), m_ColPrimaryGroup);
		p_Row->AddField(p_OnPremiseUser.GetPrimaryGroupId(), m_ColPrimaryGroupId);
		p_Row->AddField(p_OnPremiseUser.GetPrincipalsAllowedToDelegateToAccount(), m_ColPrincipalsAllowedToDelegateToAccount);
		p_Row->AddField(p_OnPremiseUser.GetProfilePath(), m_ColProfilePath);
		p_Row->AddField(p_OnPremiseUser.GetProtectedFromAccidentalDeletion(), m_ColProtectedFromAccidentalDeletion);
		p_Row->AddField(p_OnPremiseUser.GetSamAccountName(), m_ColSamAccountName);
		p_Row->AddField(p_OnPremiseUser.GetSAMAccountType(), m_ColSAMAccountType);
		p_Row->AddField(p_OnPremiseUser.GetScriptPath(), m_ColScriptPath);
		p_Row->AddField(p_OnPremiseUser.GetSDRightsEffective(), m_ColSDRightsEffective);
		p_Row->AddField(p_OnPremiseUser.GetServicePrincipalNames(), m_ColServicePrincipalNames);
		p_Row->AddField(p_OnPremiseUser.GetSID(), m_ColSID);
		p_Row->AddField(p_OnPremiseUser.GetSIDHistory(), m_ColSIDHistory);
		p_Row->AddField(p_OnPremiseUser.GetSmartcardLogonRequired(), m_ColSmartcardLogonRequired);
		p_Row->AddField(p_OnPremiseUser.GetState(), m_ColState);
		p_Row->AddField(p_OnPremiseUser.GetStreetAddress(), m_ColStreetAddress);
		p_Row->AddField(p_OnPremiseUser.GetSurname(), m_ColSurname);
		p_Row->AddField(p_OnPremiseUser.GetTitle(), m_ColTitle);
		p_Row->AddField(p_OnPremiseUser.GetTrustedForDelegation(), m_ColTrustedForDelegation);
		p_Row->AddField(p_OnPremiseUser.GetTrustedToAuthForDelegation(), m_ColTrustedToAuthForDelegation);
		p_Row->AddField(p_OnPremiseUser.GetUseDESKeyOnly(), m_ColUseDESKeyOnly);
		p_Row->AddField(p_OnPremiseUser.GetUserAccountControl(), m_ColUserAccountControl);
		p_Row->AddField(p_OnPremiseUser.GetUserCertificate(), m_ColUserCertificate);
		p_Row->AddField(p_OnPremiseUser.GetUserPrincipalName(), m_ColUserPrincipalName);
		p_Row->AddField(p_OnPremiseUser.GttUSNChanged(), m_ColUSNChanged);
		p_Row->AddField(p_OnPremiseUser.GetUSNCreated(), m_ColUSNCreated);
		AddDateOrNeverField(p_OnPremiseUser.GetWhenChanged(), *p_Row, *m_ColWhenChanged);
		AddDateOrNeverField(p_OnPremiseUser.GetWhenCreated(), *p_Row, *m_ColWhenCreated);
	}

	return p_Row;
}

void GridOnPremiseUsers::AddDateOrNeverField(const boost::YOpt<YTimeDate>& p_Date, GridBackendRow& p_Row, GridBackendColumn& p_Col)
{
	if (p_Date)
		p_Row.AddField(*p_Date, &p_Col);
	else
		p_Row.AddField(_T("Never"), &p_Col);
}

bool GridOnPremiseUsers::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return false;
}

void GridOnPremiseUsers::OnEditUsers()
{

}

void GridOnPremiseUsers::OnUpdateEditUsers(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && HasOneNonGroupRowSelectedAtLeast());
	setTextFromProfUISCommand(*pCmdUI);
}
