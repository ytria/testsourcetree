#include "AssignedLicenseNonPropDeserializer.h"

#include "JsonSerializeUtil.h"
#include "YtriaFieldsConstants.h"

using namespace JsonSerializeUtil;

void AssignedLicenseNonPropDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    DeserializeString(_YTEXT(YTRIA_BUSINESSASSIGNEDLICENSE_SKUPARTNUMBER), m_Data.m_SkuPartNumber, p_Object);
}
