#include "LinkHandlerLicenses.h"

#include "Command.h"
#include "CommandInfo.h"
#include "CommandDispatcher.h"

void LinkHandlerLicenses::Handle(YBrowserLink& p_Link) const
{
    HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	info.SetOrigin(Origin::Tenant);
    CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Licenses, Command::ModuleTask::List, info, { nullptr, g_ActionNameShowLicenses, nullptr, p_Link.GetAutomationAction() }));
}

// =================================================================================================

void LinkHandlerMyLicenses::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	info.SetOrigin(Origin::User);
	p_Link.ENABLE_FREE_ACCESS();// free access
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Licenses, Command::ModuleTask::ListMe, info, { nullptr, g_ActionNameShowLicenses, nullptr, p_Link.GetAutomationAction() }));
}

bool LinkHandlerMyLicenses::IsAllowedWithAJLLicense() const
{
	return true; // only MyData is allowed
}
