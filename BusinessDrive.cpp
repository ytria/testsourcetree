#include "BusinessDrive.h"

#include "Drive.h"
#include "LoggerService.h"
#include "MFCUtil.h"

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessDrive>("Drive") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Drive"))))
		.constructor()(policy::ctor::as_object)
		.property("Id", &BusinessDrive::m_Id)
		;
}

BusinessDrive::BusinessDrive(const Drive& p_Drive)
{
    m_Id = p_Drive.Id;
}

const boost::YOpt<IdentitySet>& BusinessDrive::GetOwner() const
{
    return m_Owner;
}

void BusinessDrive::SetOwner(const boost::YOpt<IdentitySet>& p_Val)
{
    m_Owner = p_Val;
}

const boost::YOpt<Quota>& BusinessDrive::GetQuota() const
{
    return m_Quota;
}

void BusinessDrive::SetQuota(const boost::YOpt<Quota>& p_Val)
{
    m_Quota = p_Val;
}

const boost::YOpt<PooledString>& BusinessDrive::GetDriveType() const
{
    return m_DriveType;
}

void BusinessDrive::SetDriveType(const boost::YOpt<PooledString>& p_Val)
{
    m_DriveType = p_Val;
}

const boost::YOpt<IdentitySet>& BusinessDrive::GetCreatedBy() const
{
	return m_CreatedBy;
}

void BusinessDrive::SetCreatedBy(const boost::YOpt<IdentitySet>& p_Val)
{
	m_CreatedBy = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessDrive::GetCreatedDateTime() const
{
	return m_CreatedDateTime;
}

void BusinessDrive::SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
	m_CreatedDateTime = p_Val;
}

const boost::YOpt<PooledString>& BusinessDrive::GetDescription() const
{
	return m_Description;
}

void BusinessDrive::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
	m_Description = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessDrive::GetLastModifiedDateTime() const
{
	return m_LastModifiedDateTime;
}

void BusinessDrive::SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
	m_LastModifiedDateTime = p_Val;
}

const boost::YOpt<PooledString>& BusinessDrive::GetName() const
{
	return m_Name;
}

void BusinessDrive::SetName(const boost::YOpt<PooledString>& p_Val)
{
	m_Name = p_Val;
}

const boost::YOpt<PooledString>& BusinessDrive::GetWebUrl() const
{
	return m_WebUrl;
}

void BusinessDrive::SetWebUrl(const boost::YOpt<PooledString>& p_Val)
{
	m_WebUrl = p_Val;
}

const boost::YOpt<SharepointIds>& BusinessDrive::GetSharepointIds() const
{
	return m_SharepointIds;
}

void BusinessDrive::SetSharepointIds(const boost::YOpt<SharepointIds>& p_Val)
{
	m_SharepointIds = p_Val;
}
