#pragma once

class AccessRightsFormatter
{
public:
	AccessRightsFormatter(const wstring& p_AccessRights);

	wstring Format();

private:
	wstring m_AccessRights;
};

