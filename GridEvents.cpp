#include "GridEvents.h"

#include "AttachmentInfo.h"
#include "AutomationWizardEventsGroup.h"
#include "AutomationWizardEventsUser.h"
#include "BasicGridSetup.h"
#include "BusinessGroup.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessUser.h"
#include "Command.h"
#include "CommandDispatcher.h"
#include "DlgContentViewer.h"
#include "DlgDownloadAttachments.h"
#include "EventAttachmentDeletion.h"
#include "FrameEvents.h"
#include "GridTemplateGroups.h"
#include "GridTemplateUsers.h"
#include "GridUpdater.h"
#include "GridUtil.h"
#include "MessageBodyData.h"
#include "MessageBodyViewer.h"
#include "O365AdminUtil.h"
#include "SubItemsFlagGetter.h"
#include "YCallbackMessage.h"
#include "DlgModuleOptions.h"
#include "DlgEventsModuleOptions.h"

IMPLEMENT_CacheGrid_DeleteRowLParam(GridEvents, MessageBodyData)

BEGIN_MESSAGE_MAP(GridEvents, ModuleO365Grid<BusinessEvent>)
    ON_COMMAND(ID_EVENTSGRID_SHOWEVENTMESSAGEBODY,	  OnShowEventMessageBody)
	ON_COMMAND(ID_EVENTSGRID_CHANGEOPTIONS,			  OnChangeOptions)
    ON_COMMAND(ID_EVENTSGRID_SHOWATTACHMENTS,		  OnShowEventAttachments)
    ON_COMMAND(ID_EVENTSGRID_DOWNLOADATTACHMENTS,	  OnDownloadEventAttachments)
    ON_COMMAND(ID_EVENTSGRID_VIEW_ITEM_ATTACHMENT,	  OnViewItemAttachment)
    ON_COMMAND(ID_EVENTSGRID_DELETEATTACHMENT,		  OnDeleteAttachment)
    ON_COMMAND(ID_EVENTSGRID_TOGGLEINLINEATTACHMENTS, OnToggleInlineAttachments)

    ON_UPDATE_COMMAND_UI(ID_EVENTSGRID_SHOWEVENTMESSAGEBODY,	OnUpdateShowEventMessageBody)
	ON_UPDATE_COMMAND_UI(ID_EVENTSGRID_CHANGEOPTIONS,			OnUpdateChangeOptions)
    ON_UPDATE_COMMAND_UI(ID_EVENTSGRID_SHOWATTACHMENTS,			OnUpdateShowEventAttachments)
    ON_UPDATE_COMMAND_UI(ID_EVENTSGRID_DOWNLOADATTACHMENTS,		OnUpdateDownloadEventAttachments)
    ON_UPDATE_COMMAND_UI(ID_EVENTSGRID_VIEW_ITEM_ATTACHMENT,	OnUpdateViewItemAttachment)
    ON_UPDATE_COMMAND_UI(ID_EVENTSGRID_DELETEATTACHMENT,		OnUpdateDeleteAttachment)
    ON_UPDATE_COMMAND_UI(ID_EVENTSGRID_TOGGLEINLINEATTACHMENTS, OnUpdateToggleInlineAttachments)
END_MESSAGE_MAP()

GridEvents::GridEvents(Origin p_Origin, bool p_IsMyData/* = false*/)
	: m_FrameOrigin(nullptr)
	, m_MessageViewer(nullptr)
	, m_HasLoadMoreAdditionalInfo(false)
	, m_ShowInlineAttachments(false)
	, m_IsMyData(p_IsMyData)
{
	m_WithOwnerAsTopAncestor = true;

	// FIXME: Add desired actions when the required methods are implemented below!
	UseDefaultActions(/*O365Grid::ACTION_CREATE | */O365Grid::ACTION_DELETE /*| O365Grid::ACTION_EDIT | O365Grid::ACTION_VIEW*/);
	EnableGridModifications();
    SetShowInlineAttachments(false);

	m_Origin = p_Origin;
	// FIXME: Do we want to separate both wizards?
	ASSERT(m_Origin == Origin::User || m_Origin == Origin::Group);
	if (m_Origin == Origin::User)
	{
		m_TemplateUsers = std::make_unique<GridTemplateUsers>();
		initWizard<AutomationWizardEventsUser>(_YTEXT("Automation\\EventsUser"));
	}
	else
	{
		ASSERT(!IsMyData());
		m_TemplateGroups = std::make_unique<GridTemplateGroups>();
		initWizard<AutomationWizardEventsGroup>(_YTEXT("Automation\\EventsGroup"));
	}

	if (IsMyData())
		m_MyDataMeHandler.emplace();
}

GridEvents::~GridEvents()
{
    GetBackend().Clear();
}

ModuleCriteria GridEvents::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_FrameOrigin->GetModuleCriteria();

    criteria.m_IDs.clear();
    for (auto row : p_Rows)
        criteria.m_IDs.insert(row->GetField(GetColMetaID()).GetValueStr());

    return criteria;
}

void GridEvents::customizeGrid()
{
	m_FrameEvents = dynamic_cast<FrameEvents*>(GetParentFrame());
	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_GRIDEVENTS_ACCELERATOR));

	{
		BasicGridSetup setup(GridUtil::g_AutoNameEvents, LicenseUtil::g_codeSapio365events,
		{
			{ &getColMetaDisplayName(), g_TitleOwnerDisplayName, g_FamilyOwner },
			{ m_TemplateUsers ? &m_TemplateUsers->m_ColumnUserPrincipalName : nullptr, g_TitleOwnerUserName, g_FamilyOwner },
			{ &getColMetaID(), g_TitleOwnerID, g_FamilyOwner }
		}, { BusinessEvent() });
		setup.Setup(*this, false);
	}

	if (IsMyData())
		getColMetaDisplayName()->RemovePresetFlags(g_ColumnsPresetDefault);

	if (m_TemplateUsers)
		m_MetaColumns.insert(m_MetaColumns.end(), { GetColMetaID(), getColMetaDisplayName(), m_TemplateUsers->m_ColumnUserPrincipalName });
	else
		m_MetaColumns.insert(m_MetaColumns.end(), { GetColMetaID(), getColMetaDisplayName() });

	m_FrameOrigin = dynamic_cast<GridFrameBase*>(GetParentFrame());
	ASSERT(nullptr != m_FrameOrigin);

	setBasicGridSetupHierarchy({ { { m_TemplateUsers ? m_TemplateUsers->m_ColumnUserPrincipalName : m_TemplateGroups->m_ColumnDisplayName, 0 } }
								,{ rttr::type::get<BusinessEvent>().get_id() }
								,{ m_Origin == Origin::User ? rttr::type::get<BusinessUser>().get_id() : rttr::type::get<BusinessGroup>().get_id(), rttr::type::get<BusinessEvent>().get_id() } });

	if (m_TemplateGroups)
	{
		m_TemplateGroups->m_ColumnIsTeam = AddColumnIcon(_YUID("meta.isTeam"), g_TitleOwnerIsTeam, g_FamilyOwner);
		SetColumnPresetFlags(m_TemplateGroups->m_ColumnIsTeam, { g_ColumnsPresetDefault });
		MoveColumnBefore(m_TemplateGroups->m_ColumnIsTeam, GetColMetaID());
		m_TemplateGroups->m_ColumnCombinedGroupType = AddColumn(_YUID("meta.groupType"), g_TitleOwnerGroupType, g_FamilyOwner, g_ColumnSize12, { g_ColumnsPresetDefault });
		MoveColumnBefore(m_TemplateGroups->m_ColumnCombinedGroupType, GetColMetaID());

		m_MetaColumns.insert(m_MetaColumns.end(), { m_TemplateGroups->m_ColumnIsTeam, m_TemplateGroups->m_ColumnCombinedGroupType });
	}

	/*Add additional meta columns*/
	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		auto processTemplate = [this, &metaDataColumnInfos](auto& p_Template)
		{
			for (const auto& c : *metaDataColumnInfos)
			{
				ASSERT(nullptr == p_Template->GetColumnForProperty(c.GetPropertyName()));
				if (nullptr == p_Template->GetColumnForProperty(c.GetPropertyName()))
				{
					auto col = c.AddTo(*this, g_FamilyOwner);
					p_Template->GetColumnForProperty(c.GetPropertyName()) = col;
					m_MetaColumns.push_back(col);
					m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
				}
			}
		};

		if (m_TemplateUsers)
			processTemplate(m_TemplateUsers);
		else
			processTemplate(m_TemplateGroups);
	}

	m_ColIsCancelled					= AddColumnCheckBox(_YUID("isCancelled"),						YtriaTranslate::Do(GridEvents_customizeGrid_1, _YLOC("Cancelled")).c_str(),							g_FamilyInfo,				g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColSubject						= AddColumn(_YUID("subject"),									YtriaTranslate::Do(GridEvents_customizeGrid_2, _YLOC("Subject")).c_str(),								g_FamilyInfo,				g_ColumnSize32,	{ g_ColumnsPresetDefault });
	static const wstring g_FamilyResponseSatatus = YtriaTranslate::Do(GridEvents_customizeGrid_3, _YLOC("Response Status")).c_str();
	m_ColResponseRequested				= AddColumnCheckBox(_YUID("responseRequested"),					YtriaTranslate::Do(GridEvents_customizeGrid_4, _YLOC("Response Requested")).c_str(),					g_FamilyResponseSatatus,	g_ColumnSize6);
	m_ColResponseStatusResponse			= AddColumn(_YUID("responseStatus.response"),					YtriaTranslate::Do(GridEvents_customizeGrid_5, _YLOC("Response")).c_str(),								g_FamilyResponseSatatus,	g_ColumnSize12);
	m_ColResponseStatusTime				= AddColumnDate(_YUID("responseStatus.time"),					YtriaTranslate::Do(GridEvents_customizeGrid_6, _YLOC("Response On")).c_str(),							g_FamilyResponseSatatus,	g_ColumnSize16);
	static const wstring g_FamilySchedule = YtriaTranslate::Do(GridEvents_customizeGrid_7, _YLOC("Schedule")).c_str();
	m_ColIsAllDay						= AddColumnCheckBox(_YUID("isAllDay"),							YtriaTranslate::Do(GridEvents_customizeGrid_8, _YLOC("All Day")).c_str(),							g_FamilySchedule,			g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColStartDateTime					= AddColumnDate(_YUID("start.dateTime"),						YtriaTranslate::Do(GridEvents_customizeGrid_9, _YLOC("Start On")).c_str(),								g_FamilySchedule,			g_ColumnSize16,	{ g_ColumnsPresetDefault });
	m_ColStartTimeZone					= AddColumn(_YUID("start.timeZone"),							YtriaTranslate::Do(GridEvents_customizeGrid_10, _YLOC("Start Time Zone")).c_str(),						g_FamilySchedule,			g_ColumnSize8);
	m_ColOriginalStart					= AddColumnDate(_YUID("originalStart"),							YtriaTranslate::Do(GridEvents_customizeGrid_11, _YLOC("Original Start On")).c_str(),					g_FamilySchedule,			g_ColumnSize16);
	m_ColOriginalStartTimeZone			= AddColumn(_YUID("originalStartTimeZone"),						YtriaTranslate::Do(GridEvents_customizeGrid_12, _YLOC("Original Start Time Zone")).c_str(),				g_FamilySchedule,			g_ColumnSize12);
	m_ColEndDateTime					= AddColumnDate(_YUID("end.dateTime"),							YtriaTranslate::Do(GridEvents_customizeGrid_13, _YLOC("End On")).c_str(),								g_FamilySchedule,			g_ColumnSize16,	{ g_ColumnsPresetDefault });
	m_ColEndTimeZone					= AddColumn(_YUID("end.timeZone"),								YtriaTranslate::Do(GridEvents_customizeGrid_14, _YLOC("End Time Zone")).c_str(),						g_FamilySchedule,			g_ColumnSize12);
	m_ColOriginalEndTimeZone			= AddColumn(_YUID("originalEndTimeZone"),						YtriaTranslate::Do(GridEvents_customizeGrid_15, _YLOC("Original End Time Zone")).c_str(),				g_FamilySchedule,			g_ColumnSize12);
	m_ColIsReminderOn					= AddColumnCheckBox(_YUID("isReminderOn"),						YtriaTranslate::Do(GridEvents_customizeGrid_16, _YLOC("Reminder On")).c_str(),						g_FamilyInfo,				g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColReminderMinutesBeforeStart		= AddColumnNumber(_YUID("reminderMinutesBeforeStart"),			YtriaTranslate::Do(GridEvents_customizeGrid_17, _YLOC("Reminder Minutes Before Start")).c_str(),		g_FamilyInfo,				g_ColumnSize12,	{ g_ColumnsPresetDefault });
	m_ColBodyPreview					= AddColumnRichText(_YUID("bodyPreview"),						YtriaTranslate::Do(GridEvents_customizeGrid_18, _YLOC("Preview")).c_str(),								g_FamilyInfo,				g_ColumnSize32,	{ g_ColumnsPresetDefault });
	m_ColBodyContentType				= AddColumn(_YUID("body.contentType"),							YtriaTranslate::Do(GridEvents_customizeGrid_19, _YLOC("Body ContentType")).c_str(),						g_FamilyInfo,				g_ColumnSize6);
	m_ColCategories						= AddColumn(_YUID("categories"),								YtriaTranslate::Do(GridEvents_customizeGrid_20, _YLOC("Categories")).c_str(),							g_FamilyInfo,				g_ColumnSize12);
    m_ColChangeKey						= AddColumn(_YUID("changeKey"),									YtriaTranslate::Do(GridEvents_customizeGrid_21, _YLOC("Change Key")).c_str(),							g_FamilyInfo,				g_ColumnSize12);
	static const wstring g_FamilyOrganizer = YtriaTranslate::Do(GridEvents_customizeGrid_22, _YLOC("Organizer")).c_str();
	m_ColIsOrganizer					= AddColumnCheckBox(_YUID("isOrganizer"),						YtriaTranslate::Do(GridEvents_customizeGrid_23, _YLOC("Organizer")).c_str(),							g_FamilyOrganizer,			g_ColumnSize6,	{ g_ColumnsPresetDefault });
	m_ColOrganizerEmailAddressName		= AddColumn(_YUID("organizer.emailAddress.Name"),				YtriaTranslate::Do(GridEvents_customizeGrid_24, _YLOC("Organizer - Name")).c_str(),						g_FamilyOrganizer,			g_ColumnSize22,	{ g_ColumnsPresetDefault });
	m_ColOrganizerEmailAddressAddress	= AddColumn(_YUID("organizer.emailAddress.Address"),			YtriaTranslate::Do(GridEvents_customizeGrid_25, _YLOC("Organizer - Email")).c_str(),					g_FamilyOrganizer,			g_ColumnSize32);
	static const wstring g_FamilyAttendees = YtriaTranslate::Do(GridEvents_customizeGrid_26, _YLOC("Attendees")).c_str();
	m_ColAttendeesType					= AddColumn(_YUID("attendees.type"),							YtriaTranslate::Do(GridEvents_customizeGrid_27, _YLOC("Attendees - Type")).c_str(),						g_FamilyAttendees,			g_ColumnSize12);
	m_ColAttendeesEmailAddressName		= AddColumn(_YUID("attendees.email.name"),						YtriaTranslate::Do(GridEvents_customizeGrid_28, _YLOC("Attendees - Name")).c_str(),						g_FamilyAttendees,			g_ColumnSize22,	{ g_ColumnsPresetDefault });
	m_ColAttendeesEmailAddressAddress	= AddColumn(_YUID("attendees.email.address"),					YtriaTranslate::Do(GridEvents_customizeGrid_29, _YLOC("Attendees - Email")).c_str(),					g_FamilyAttendees,			g_ColumnSize32);
	m_ColAttendeesStatusResponse		= AddColumn(_YUID("attendees.status.response"),					YtriaTranslate::Do(GridEvents_customizeGrid_30, _YLOC("Attendees - Response")).c_str(),					g_FamilyAttendees,			g_ColumnSize12,	{ g_ColumnsPresetDefault });
	m_ColAttendeesStatusTime			= AddColumnDate(_YUID("attendees.status.time"),					YtriaTranslate::Do(GridEvents_customizeGrid_31, _YLOC("Attendees - Response On")).c_str(),				g_FamilyAttendees,			g_ColumnSize16);
	m_ColType							= AddColumn(_YUID("type"),										YtriaTranslate::Do(GridEvents_customizeGrid_32, _YLOC("Event Type")).c_str(),							g_FamilyInfo,				g_ColumnSize12,	{ g_ColumnsPresetDefault });
	m_ColImportance						= AddColumn(_YUID("importance"),								YtriaTranslate::Do(GridEvents_customizeGrid_33, _YLOC("Importance")).c_str(),							g_FamilyInfo,				g_ColumnSize8);
	m_ColSensitivity					= AddColumn(_YUID("sensitivity"),								YtriaTranslate::Do(GridEvents_customizeGrid_34, _YLOC("Sensitivity")).c_str(),							g_FamilyInfo,				g_ColumnSize12);
	m_ColShowAs							= AddColumn(_YUID("showAs"),									YtriaTranslate::Do(GridEvents_customizeGrid_35, _YLOC("Show As")).c_str(),								g_FamilyInfo,				g_ColumnSize12);
	m_ColOnlineMeetingUrl				= AddColumnHyperlink(_YUID("onlineMeetingUrl"),					YtriaTranslate::Do(GridEvents_customizeGrid_36, _YLOC("Online Meeting URL")).c_str(),					g_FamilyInfo,				g_ColumnSize8,	{ g_ColumnsPresetDefault });
	m_ColCreatedDateTime				= AddColumnDate(_YUID("createdDateTime"),						YtriaTranslate::Do(GridEvents_customizeGrid_37, _YLOC("Created On")).c_str(),							g_FamilyInfo,				g_ColumnSize16,	{ g_ColumnsPresetDefault });
	m_ColLastModifiedDateTime			= AddColumnDate(_YUID("lastModifiedDateTime"),					YtriaTranslate::Do(GridEvents_customizeGrid_38, _YLOC("Last Modified")).c_str(),						g_FamilyInfo,				g_ColumnSize16,	{ g_ColumnsPresetDefault });
	static const wstring g_FamilyLocation = YtriaTranslate::Do(GridEvents_customizeGrid_39, _YLOC("Location")).c_str();
	m_ColLocationDisplayName			= AddColumn(_YUID("location.displayName"),						YtriaTranslate::Do(GridEvents_customizeGrid_40, _YLOC("Display Name - Location")).c_str(),				g_FamilyLocation,			g_ColumnSize22,	{ g_ColumnsPresetDefault });
	m_ColLocationLocationEmailAddress	= AddColumn(_YUID("location.locationEmailAddress"),				YtriaTranslate::Do(GridEvents_customizeGrid_41, _YLOC("Email - Location")).c_str(),						g_FamilyLocation,			g_ColumnSize32);
	m_ColLocationAddressStreet			= AddColumn(_YUID("location.address.street"),					YtriaTranslate::Do(GridEvents_customizeGrid_42, _YLOC("Street - Location")).c_str(),					g_FamilyLocation,			g_ColumnSize22);
	m_ColLocationAddressCity			= AddColumn(_YUID("location.address.city"),						YtriaTranslate::Do(GridEvents_customizeGrid_43, _YLOC("City - Location")).c_str(),						g_FamilyLocation,			g_ColumnSize12);
	m_ColLocationAddressPostalCode		= AddColumn(_YUID("location.address.postalCode"),				YtriaTranslate::Do(GridEvents_customizeGrid_44, _YLOC("Postal Code - Location")).c_str(),				g_FamilyLocation,			g_ColumnSize12);
	m_ColLocationAddressState			= AddColumn(_YUID("location.address.state"),					YtriaTranslate::Do(GridEvents_customizeGrid_45, _YLOC("State - State")).c_str(),						g_FamilyLocation,			g_ColumnSize12);
	m_ColLocationAddressCountryOrRegion = AddColumn(_YUID("location.address.country"),					YtriaTranslate::Do(GridEvents_customizeGrid_46, _YLOC("Country - Location")).c_str(),					g_FamilyLocation,			g_ColumnSize12);
	m_ColWebLink						= AddColumnHyperlink(_YUID("weblink"),							YtriaTranslate::Do(GridEvents_customizeGrid_47, _YLOC("URL")).c_str(),									g_FamilyInfo,				g_ColumnSize8,	{ g_ColumnsPresetDefault });
	static const wstring g_FamilyRecurrence = YtriaTranslate::Do(GridEvents_customizeGrid_48, _YLOC("Recurrence")).c_str();
    m_ColRecurrencePatternType			= AddColumn(_YUID("recurrence.pattern.type"),					YtriaTranslate::Do(GridEvents_customizeGrid_49, _YLOC("Type - Recurrence")).c_str(),					g_FamilyRecurrence,			g_ColumnSize8,	{ g_ColumnsPresetDefault });
    m_ColRecurrencePatternInterval		= AddColumnNumberInt(_YUID("recurrence.pattern.interval"),		YtriaTranslate::Do(GridEvents_customizeGrid_50, _YLOC("Interval - Recurrence")).c_str(),				g_FamilyRecurrence,			g_ColumnSize6);
	m_ColRecurrencePatternMonth			= AddColumnNumberInt(_YUID("recurrence.pattern.month"),			YtriaTranslate::Do(GridEvents_customizeGrid_51, _YLOC("Month - Recurrence")).c_str(),					g_FamilyRecurrence,			g_ColumnSize6);
	m_ColRecurrencePatternDayOfMonth	= AddColumnNumberInt(_YUID("recurrence.pattern.dayOfMonth"),	YtriaTranslate::Do(GridEvents_customizeGrid_52, _YLOC("Day Of Month - Recurrence")).c_str(),			g_FamilyRecurrence,			g_ColumnSize6);
	m_ColRecurrencePatternDaysOfWeek	= AddColumn(_YUID("recurrence.pattern.daysOfWeek"),				YtriaTranslate::Do(GridEvents_customizeGrid_53, _YLOC("Days Of Week - Recurrence")).c_str(),			g_FamilyRecurrence,			g_ColumnSize12);
	m_ColRecurrencePatternFirstDayWeek	= AddColumn(_YUID("recurrence.pattern.firstDayOfWeek"),			YtriaTranslate::Do(GridEvents_customizeGrid_54, _YLOC("First Day Of Week - Recurrence")).c_str(),		g_FamilyRecurrence,			g_ColumnSize8);
	m_ColRecurrencePatternIndex			= AddColumn(_YUID("recurrence.pattern.index"),					YtriaTranslate::Do(GridEvents_customizeGrid_55, _YLOC("Day Index - Recurrence")).c_str(),				g_FamilyRecurrence,			g_ColumnSize8);
    m_ColRecurrenceRangeType			= AddColumn(_YUID("recurrence.range.type"),						YtriaTranslate::Do(GridEvents_customizeGrid_56, _YLOC("Range Type - Recurrence")).c_str(),				g_FamilyRecurrence,			g_ColumnSize8);
	m_ColRecurrenceRangeStartDate		= AddColumnDate(_YUID("recurrence.range.startDate"),			YtriaTranslate::Do(GridEvents_customizeGrid_57, _YLOC("Start Date - Recurrence")).c_str(),				g_FamilyRecurrence,			g_ColumnSize16);
	SetColumnDateFormatNoTime(m_ColRecurrenceRangeStartDate);
	m_ColRecurrenceRangeEndDate			= AddColumnDate(_YUID("recurrence.range.endDate"),				YtriaTranslate::Do(GridEvents_customizeGrid_58, _YLOC("End Date - Recurrence")).c_str(),				g_FamilyRecurrence,			g_ColumnSize16);
	SetColumnDateFormatNoTime(m_ColRecurrenceRangeEndDate);
	m_ColRecurrenceRecurrenceTimeZone	= AddColumn(_YUID("recurrence.range.recurrenceTimeZone"),		YtriaTranslate::Do(GridEvents_customizeGrid_59, _YLOC("Time Zone - Recurrence")).c_str(),				g_FamilyRecurrence,			g_ColumnSize8);
	m_ColRecurrenceNumberOfOccurences	= AddColumnNumberInt(_YUID("recurrence.range.nOccurences"),		YtriaTranslate::Do(GridEvents_customizeGrid_60, _YLOC("Number Of Occurences - Recurrence")).c_str(),	g_FamilyRecurrence,			g_ColumnSize6);
 	addColumnGraphID();

	AddColumnForRowPK(GetColMetaID());
	AddColumnForRowPK(GetColId());

	m_ColSeriesMasterId					= AddColumn(_YUID("seriesMasterId"),							YtriaTranslate::Do(GridEvents_customizeGrid_61, _YLOC("Series Master ID")).c_str(),						g_FamilyInfo,				g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColICalUid						= AddColumn(_YUID("iCalUID"),									YtriaTranslate::Do(GridEvents_customizeGrid_62, _YLOC("Cross Calendar Unique ID")).c_str(),				g_FamilyInfo,				g_ColumnSize12, { g_ColumnsPresetTech });
	static const wstring g_FamilyAttachment = YtriaTranslate::Do(GridEvents_customizeGrid_63, _YLOC("Attachment")).c_str();
	m_ColHasGraphAttachment             = AddColumnCheckBox(_YUID("hasAttachments"),					YtriaTranslate::Do(GridEvents_customizeGrid_64, _YLOC("Has Attachment")).c_str(),						g_FamilyAttachment,         g_ColumnSize6,	{ g_ColumnsPresetDefault });
	addColumnIsLoadMore(_YUID("loaded"), YtriaTranslate::Do(GridEvents_customizeGrid_65, _YLOC("Status - Load Attachment Info")).c_str());
	m_ColHasAnyAttachment = AddColumnCheckBox(_YUID("containsAttachments"),								YtriaTranslate::Do(GridEvents_customizeGrid_66, _YLOC("Contains Attachments incl. inline")).c_str(),	g_FamilyAttachment,			g_ColumnSize6);
	m_ColAttachmentType					= AddColumn(_YUID("attachment.type"),							YtriaTranslate::Do(GridEvents_customizeGrid_67, _YLOC("Type")).c_str(),             					g_FamilyAttachment,			g_ColumnSize12, { g_ColumnsPresetDefault });
	SetColumnPresetFlags(AddColumnFileType(_T("Icon"), g_FamilyAttachment), { g_ColumnsPresetDefault });
	m_ColAttachmentName					= AddColumn(_YUID("attachment.name"),							YtriaTranslate::Do(GridEvents_customizeGrid_68, _YLOC("Name")).c_str(),				                	g_FamilyAttachment,			g_ColumnSize32,	{ g_ColumnsPresetDefault });
	if (nullptr != m_ColAttachmentName)
		m_ColAttachmentName->SetManageFileFormat(true);
	m_ColAttachmentNameExtOnly = AddColumn(_YUID("ATTACHMENTNAMEEXTONLY"), _T("Extension"), g_FamilyAttachment, g_ColumnSize6);
	m_ColAttachmentContentType			= AddColumn(_YUID("attachment.contentType"),					YtriaTranslate::Do(GridEvents_customizeGrid_69, _YLOC("Content Type")).c_str(),             			g_FamilyAttachment,			g_ColumnSize12);
	m_ColAttachmentSize					= AddColumnNumberInt(_YUID("attachment.size"),					YtriaTranslate::Do(GridEvents_customizeGrid_70, _YLOC("Size")).c_str(),					                g_FamilyAttachment,			g_ColumnSize8,	{ g_ColumnsPresetDefault });
	m_ColAttachmentSize->SetXBytesColumnFormat();
	m_ColAttachmentLastModifiedDateTime = AddColumnDate(_YUID("attachment.lastModifiedDateTime"),		YtriaTranslate::Do(GridEvents_customizeGrid_71, _YLOC("Last Modified Date")).c_str(),			            g_FamilyAttachment,			g_ColumnSize16);
	m_ColAttachmentIsInline				= AddColumnCheckBox(_YUID("attachment.isInline"),				YtriaTranslate::Do(GridEvents_customizeGrid_72, _YLOC("Inline")).c_str(),				                g_FamilyAttachment,			g_ColumnSize6);
	m_ColAttachmentId					= AddColumn(_YUID("attachment.id"),								YtriaTranslate::Do(GridEvents_customizeGrid_75, _YLOC("Attachment ID")).c_str(),						            g_FamilyAttachment,			g_ColumnSize12, { g_ColumnsPresetTech });
	
	m_ColAttendeesEmailAddressName->SetMultivalueFamily(YtriaTranslate::Do(GridEvents_customizeGrid_76, _YLOC("Attendees")).c_str());
	m_ColAttendeesEmailAddressName->AddMultiValueExplosionSister(m_ColAttendeesEmailAddressAddress);
	m_ColAttendeesEmailAddressName->AddMultiValueExplosionSister(m_ColAttendeesType);
	m_ColAttendeesEmailAddressName->AddMultiValueExplosionSister(m_ColAttendeesStatusResponse);
	m_ColAttendeesEmailAddressName->AddMultiValueExplosionSister(m_ColAttendeesStatusTime);    

	// This column will be the elder of the sisterhood. Keep it the same as the one used in row flagger to identify subitems. 
	m_ColAttachmentId->SetMultivalueFamily(YtriaTranslate::Do(GridEvents_customizeGrid_77, _YLOC("Attachments")).c_str());
    m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentName);
    m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentType);
    m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentContentType);
    m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentSize);
    m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentLastModifiedDateTime);
    m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentIsInline);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentNameExtOnly);
	m_ColAttachmentId->AddMultiValueExplosionSister(GetColumnFileType());

	SetColumnsPresetFlags({		m_ColHasAnyAttachment,
                                m_ColAttachmentType,
								m_ColAttachmentId,
								m_ColAttachmentName,
								m_ColAttachmentContentType,
								m_ColAttachmentSize,
								m_ColAttachmentLastModifiedDateTime,
								m_ColAttachmentIsInline,
								m_ColAttachmentNameExtOnly ,
								GetColumnFileType() },
						  { g_ColumnsPresetMore });

	addTopCategories(YtriaTranslate::Do(GridEvents_customizeGrid_78, _YLOC("Event Info")).c_str(), YtriaTranslate::Do(GridEvents_customizeGrid_79, _YLOC("Attachment Info")).c_str(), true);

	if (m_TemplateUsers)
		m_TemplateUsers->CustomizeGrid(*this);
	if (m_TemplateGroups)
		m_TemplateGroups->CustomizeGrid(*this);

    m_Icons			= GridUtil::LoadAttachmentIcons(*this);
    m_MessageViewer = std::make_unique<MessageBodyViewer>(*this, m_ColBodyContentType, m_ColSubject);
	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_FrameOrigin);

    SetRowFlagger(std::make_unique<SubItemsFlagGetter>(GetModifications(), m_ColAttachmentId->GetMultiValueElderColumn()));
}

wstring GridEvents::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Owner"), getColMetaDisplayName() }, { _T("Starts on"), m_ColStartDateTime } }, m_ColSubject);
}

RoleDelegationUtil::RBAC_Privilege GridEvents::getPermission(const int p_CommandID) const
{
	const bool userOrigin = Origin::User == m_Origin;
	ASSERT(userOrigin || Origin::Group == m_Origin);
	if (userOrigin || Origin::Group == m_Origin)
	{
		switch (p_CommandID)
		{
		case ID_EVENTSGRID_SHOWATTACHMENTS:
			return userOrigin ? RoleDelegationUtil::RBAC_USER_EVENTS_LOADMORE				: RoleDelegationUtil::RBAC_GROUP_EVENTS_LOADMORE;
		case ID_EVENTSGRID_DOWNLOADATTACHMENTS:
			return userOrigin ? RoleDelegationUtil::RBAC_USER_EVENTS_ATTACHMENTS_DOWNLOAD	: RoleDelegationUtil::RBAC_GROUP_EVENTS_ATTACHMENTS_DOWNLOAD;
		case ID_EVENTSGRID_DELETEATTACHMENT:
			return userOrigin ? RoleDelegationUtil::RBAC_USER_EVENTS_ATTACHMENTS_EDIT		: RoleDelegationUtil::RBAC_GROUP_EVENTS_ATTACHMENTS_EDIT;
		}
	}

	ASSERT(false);
	return RoleDelegationUtil::RBAC_NONE;
}

GridBackendColumn* GridEvents::GetColMetaID() const
{
	return getColMetaID();
}

bool GridEvents::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return ModuleO365Grid<BusinessEvent>::IsRowValidForNoFieldText(p_Row, p_Col)
		&& (!p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore)
			|| m_MetaIDsMoreLoaded.end() == m_MetaIDsMoreLoaded.find(p_Row->GetField(GetColMetaID()).GetValueStr()));
}

RoleDelegationUtil::RBAC_Privilege GridEvents::GetPrivilegeDelete() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_USER_EVENTS_DELETE;
}

bool GridEvents::IsMyData() const
{
	return m_IsMyData;
}

O365Grid::SnapshotCaps GridEvents::GetSnapshotCapabilities() const
{
	return { true };
}

void GridEvents::customizeGridPostProcess()
{
	O365Grid::customizeGridPostProcess();
	SetNoFieldTextAndIcon(GetColumnsByPresets({ g_ColumnsPresetMore }), IDB_ICON_LM_FILE_ATTACHMENT, YtriaTranslate::Do(GridEvents_customizeGridPostProcess_1, _YLOC("To see this information, use the 'Load Attachment Info' button")).c_str(), YtriaTranslate::Do(GridEvents_customizeGridPostProcess_2, _YLOC(" - Use the 'Load Attachment Info' button to display additional information - ")).c_str());
	SetLoadMoreStatus(true,		YtriaTranslate::Do(GridEvents_customizeGridPostProcess_3, _YLOC("Loaded")).c_str(),		IDB_ICON_LMSTATUS_LOADED_ATTACHMENTS);
	SetLoadMoreStatus(false,	YtriaTranslate::Do(GridEvents_customizeGridPostProcess_4, _YLOC("Not loaded")).c_str(),	IDB_ICON_LMSTATUS_NOTLOADED_ATTACHMENTS);
}

void GridEvents::BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_FullPurge, bool p_NoPurge)
{
	buildTreeViewImpl(p_Events, p_Updates, {}, p_Refresh, p_FullPurge, p_NoPurge);
}

void GridEvents::BuildTreeView(const O365DataMap<BusinessGroup, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_FullPurge, bool p_NoPurge)
{
	buildTreeViewImpl(p_Events, p_Updates, {}, p_Refresh, p_FullPurge, p_NoPurge);
}

void GridEvents::BuildTreeViewWithAttachments(const O365DataMap<BusinessUser, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_Updates, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge)
{
	buildTreeViewImpl(p_Events, p_Updates, p_BusinessAttachments, p_Refresh, p_FullPurge, false);
}

void GridEvents::BuildTreeViewWithAttachments(const O365DataMap<BusinessGroup, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_Updates, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge)
{
	buildTreeViewImpl(p_Events, p_Updates, p_BusinessAttachments, p_Refresh, p_FullPurge, false);
}

void GridEvents::OnChangeOptions()
{
	if (hasNoTaskRunning() && IsFrameConnected())
	{
		ASSERT(m_FrameEvents->GetOptions());
		DlgEventsModuleOptions dlgOptions(m_FrameEvents, GetSession(), m_FrameEvents->GetOptions() ? *m_FrameEvents->GetOptions() : ModuleOptions());
		if (dlgOptions.DoModal() == IDOK)
		{
			CommandInfo info;
			info.SetFrame(m_FrameEvents);
			info.SetOrigin(GetOrigin());
			info.SetRBACPrivilege(m_FrameEvents->GetModuleCriteria().m_Privilege);
			info.Data2() = dlgOptions.GetOptions();

			// FIXME: Automation action name !!!
			Command command(m_FrameEvents->GetLicenseContext(), m_FrameEvents->GetSafeHwnd(), Command::ModuleTarget::Event, Command::ModuleTask::List, info, { this, g_ActionNameRefreshAll, GetAutomationActionRecording(), nullptr });
			CommandDispatcher::GetInstance().Execute(command);
		}
	}
}

void GridEvents::OnShowEventMessageBody()
{
    if (nullptr != m_MessageViewer)
        m_MessageViewer->ToggleView();
}

void GridEvents::OnShowEventAttachments()
{
    std::vector<GridBackendRow*> selectedRows;
    GetSelectedNonGroupRows(selectedRows);
    loadMoreImpl(selectedRows);
}

void GridEvents::OnUpdateShowEventAttachments(CCmdUI* pCmdUI)
{
    BOOL enable = FALSE;
    
    if (hasNoTaskRunning() && IsFrameConnected())
    {
		const auto privilege = getPermission(ID_EVENTSGRID_SHOWATTACHMENTS);
        wstring strMetaId;
        for (auto& row : GetSelectedRows())
        {
            if (GridUtil::isBusinessType<BusinessEvent>(row) && O365Grid::IsAuthorizedByRoleDelegation(row, privilege, true))
            {
                strMetaId = row->GetField(GetColMetaID()).GetValueStr();
                if (!strMetaId.empty() && !row->IsMoreLoaded())
                {
                    enable = TRUE;
                    break;
                }
            }
        }
    }

    pCmdUI->Enable(enable);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridEvents::OnDownloadEventAttachments()
{
    DlgDownloadAttachments dlg(this, DlgDownloadAttachments::CONTEXT::EVENT);
    if (dlg.DoModal() != IDOK)
        return;

    AttachmentsInfo attachmentsMetaInfo;
    attachmentsMetaInfo.m_FileExistsAction		= dlg.GetFileExistsAction();
    attachmentsMetaInfo.m_DownloadFolderName	= dlg.GetFolderPath();
    attachmentsMetaInfo.m_OpenFolder            = dlg.GetOpenFolder();
	const auto useOwnerName						= dlg.GetUseOwnerName();

	const auto privilege = getPermission(ID_EVENTSGRID_DOWNLOADATTACHMENTS);
	std::set<AttachmentsInfo::IDs> alreadyProcessedAttachmentIDs;
    for (auto& row : GetSelectedRows())
    {
        if (GridUtil::isBusinessType<BusinessEvent>(row) && O365Grid::IsAuthorizedByRoleDelegation(row, privilege, true))
        {
            if (row->GetField(m_ColAttachmentId).HasValue())
            {
                wstring entityId = row->GetField(GetColMetaID()).GetValueStr();
                wstring eventId = row->GetField(GetColId()).GetValueStr();

				wstring columnsPath;
				if (useOwnerName)
					columnsPath = row->GetField(getColMetaDisplayName()).GetValueStr();
				if (!columnsPath.empty())
					columnsPath += _YTEXT("\\");
				columnsPath += GridUtil::GetColumnsPath(*this, row, dlg.GetSelectedColumnsTitleDisplayed());

				if (nullptr != row->GetField(m_ColAttachmentId).GetValuesMulti<PooledString>())
				{
					auto attachmentIds = row->GetField(m_ColAttachmentId).GetValuesMulti<PooledString>();
					auto attachmentTypes = row->GetField(m_ColAttachmentType).GetValuesMulti<PooledString>();
					ASSERT(nullptr != attachmentIds && nullptr != attachmentTypes);
					if (nullptr != attachmentIds && nullptr != attachmentTypes)
					{
						ASSERT(attachmentTypes->size() == attachmentIds->size());
						if (attachmentTypes->size() == attachmentIds->size())
						{
							size_t index = 0;
							for (const auto& attachmentId : *attachmentIds)
							{
								if ((*attachmentTypes)[index] == Util::GetFileAttachmentStr())
								{
									AttachmentsInfo::Infos infos;
									infos.m_IDs.UserOrGroupID() = entityId;
									infos.m_IDs.EventOrMessageOrConversationID() = eventId;
									infos.m_IDs.AttachmentID() = attachmentId;

									if (alreadyProcessedAttachmentIDs.insert(infos.m_IDs).second)
									{
										infos.m_ColumnsPath = columnsPath;
										attachmentsMetaInfo.m_AttachmentInfos.push_back(infos);
									}
								}
								++index;
							}
						}
					}
				}
				else
                {
					const wstring attachmentId = row->GetField(m_ColAttachmentId).GetValueStr();
					const wstring attachmentType = row->GetField(m_ColAttachmentType).GetValueStr();
					if (attachmentType == Util::GetFileAttachmentStr())
					{
						AttachmentsInfo::Infos infos;
						infos.m_IDs.UserOrGroupID() = entityId;
						infos.m_IDs.EventOrMessageOrConversationID() = eventId;
						infos.m_IDs.AttachmentID() = attachmentId;

						if (alreadyProcessedAttachmentIDs.insert(infos.m_IDs).second)
						{
							infos.m_ColumnsPath = columnsPath;
							attachmentsMetaInfo.m_AttachmentInfos.push_back(infos);
						}
					}
                }
            }
        }
    }

	CommandInfo info;
    info.Data() = attachmentsMetaInfo;
    info.SetFrame(m_FrameOrigin);
	info.SetRBACPrivilege(m_FrameOrigin->GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Event, Command::ModuleTask::DownloadAttachments, info, { this, g_ActionNameSelectedAttachmentDownload, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
}

void GridEvents::OnViewItemAttachment()
{
    CommandInfo info;

    ASSERT(GetSelectedRows().size() == 1);
    if (GetSelectedRows().size() == 1)
    {
        auto& row = *(GetSelectedRows().begin());

        GridBackendField& idField			= row->GetField(GetColMetaID());
        GridBackendField& eventIdField		= row->GetField(GetColId());
		GridBackendField& eventSubjectField = row->GetField(m_ColSubject);
        GridBackendField& attachmentIdField = row->GetField(m_ColAttachmentId);

        if (attachmentIdField.HasValue() && eventIdField.HasValue() && idField.HasValue())
        {
			info.Data() = AttachmentsInfo::IDs(idField.GetValueStr(), eventIdField.GetValueStr(), attachmentIdField.GetValueStr(), eventSubjectField.GetValueStr());
            info.SetFrame(m_FrameOrigin);
            info.SetOrigin(m_Origin);
			info.SetRBACPrivilege(m_FrameOrigin->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Event, Command::ModuleTask::ViewItemAttachment, info, { this, g_ActionNameSelectedPreviewItem, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
        }
    }
}

void GridEvents::OnDeleteAttachment()
{
	const auto deleteAttachment = [this](GridBackendRow* p_Row, const PooledString& p_attId)
	{
		AttachmentInfo info(*this, p_attId, p_Row);

		auto deletion = std::make_unique<EventAttachmentDeletion>(*this, info);
		deletion->ShowAppliedLocally(p_Row);
		if (GetModifications().Add(std::move(deletion)))
		{
			// Successfully added deletion? Remove error flag in case we just overwrote an old error with the new deletion
			p_Row->SetEditError(false);
		}
	};

	/*std::map<GridBackendRow*, bool> alreadyCheckedAncestors;
	boost::YOpt<bool> shouldRevertExistingModifications;
	
	const auto checkExistingMod = [this, &alreadyCheckedAncestors, &shouldRevertExistingModifications](GridBackendRow* p_Row)
	{
		if (alreadyCheckedAncestors.end() == alreadyCheckedAncestors.find(p_Row))
		{
			bool proceedWithDeletion = true;
			row_primary_key_t pk;
			GetRowPKNoDynamicColumns(p_Row, pk);

			auto mod = GetModifications().GetRowModification<RowModification>(pk);

			if (nullptr != mod)
			{
				if (!shouldRevertExistingModifications)
				{
					YCodeJockMessageBox errorMessage(this,
						DlgMessageBox::eIcon_Question,
						YtriaTranslate::Do(GridMessages_OnDeleteAttachment_1, _YLOC("Delete Attachments")).c_str(),
						YtriaTranslate::Do(GridMessages_OnDeleteAttachment_2, _YLOC("Some message are flagged for delete. Do you want to restore them and delete attachments instead?")).c_str(),
						_YTEXT(""),
						{ { IDOK, YtriaTranslate::Do(GridMessages_OnDeleteAttachment_3, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridMessages_OnDeleteAttachment_4, _YLOC("No")).c_str() } });

					shouldRevertExistingModifications = errorMessage.DoModal() == IDOK;
				}

				ASSERT(shouldRevertExistingModifications);
				if (shouldRevertExistingModifications && *shouldRevertExistingModifications)
				{
					GetModifications().RevertRowModifications(pk);
					proceedWithDeletion = true;
				}
				else
				{
					proceedWithDeletion = false;
				}
			}

			alreadyCheckedAncestors[p_Row] = proceedWithDeletion;
		}

		return alreadyCheckedAncestors[p_Row];
	};*/

	std::vector<GridBackendRow*> rows;
	GetSelectedNonGroupRows(rows);
	TemporarilyImplodeOutOfSisterhoodColumns(*this, m_ColAttachmentId, rows, [=](const bool p_HasRowsToReExplode)
	{
		const auto privilege = getPermission(ID_EVENTSGRID_DELETEATTACHMENT);
		for (auto row : GetSelectedRows())
		{
			if (GridUtil::IsBusinessEvent(row) && O365Grid::IsAuthorizedByRoleDelegation(row, privilege, true))
			{
				auto& field = row->GetField(m_ColAttachmentId);
				if (field.HasValue())
				{
					if (GetMultivalueManager().IsRowExploded(row, m_ColAttachmentId))
					{
						ASSERT(row->IsExplosionFragment());
						ASSERT(!field.IsMulti());
						if (field.HasValue())
						{
							auto topRow = GetMultivalueManager().GetTopSourceRow(row);
							//if (checkExistingMod(topRow))
								deleteAttachment(row, field.GetValueStr());
						}
					}
					else
					{
						if (field.IsMulti())
						{
							std::vector<PooledString>* attachmentIds = row->GetField(m_ColAttachmentId).GetValuesMulti<PooledString>();
							ASSERT(nullptr != attachmentIds);
							if (nullptr != attachmentIds)
							{
								//if (checkExistingMod(row))
								{
									for (const auto& attachmentId : *attachmentIds)
										deleteAttachment(row, attachmentId);
								}
							}
						}
						else
						{
							//if (checkExistingMod(row))
								deleteAttachment(row, field.GetValueStr());
						}
					}

					GetModifications().ShowSubItemModificationAppliedLocally(row);
				}
			}
		}

		if (!p_HasRowsToReExplode)
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	})();
}

void GridEvents::OnToggleInlineAttachments()
{
    if (!m_ShowInlineAttachments)
    {
		TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::ALL_ROWS, [this](const bool p_HasRowsToReExplode)
		{
			SetShowInlineAttachments(true);
			ModuleUtil::ClearModifiedRowsStatus(*this);
			ViewAttachments(m_AllAttachments, false, false, !p_HasRowsToReExplode);
		})();
    }
}

void GridEvents::OnUpdateChangeOptions(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridEvents::OnUpdateShowEventMessageBody(CCmdUI* pCmdUI)
{
	auto frame = dynamic_cast<FrameEvents*>(GetParentFrame());
	pCmdUI->Enable(hasNoTaskRunning() && GridUtil::IsSelectionAtLeastOneOfType<BusinessEvent>(*this) && frame && frame->GetOptions() && frame->GetOptions()->m_BodyContent);
    pCmdUI->SetCheck(nullptr != m_MessageViewer && m_MessageViewer->IsShowing());

	setTextFromProfUISCommand(*pCmdUI);
}

void GridEvents::OnUpdateDownloadEventAttachments(CCmdUI* pCmdUI)
{
    BOOL enable = FALSE;

	if (hasNoTaskRunning() && IsFrameConnected())
	{
		const auto privilege = getPermission(ID_EVENTSGRID_DOWNLOADATTACHMENTS);
		// Enable only if attachment(s) are loaded
		for (auto& row : GetSelectedRows())
		{
			if (GridUtil::isBusinessType<BusinessEvent>(row) && O365Grid::IsAuthorizedByRoleDelegation(row, privilege, true))
			{
				const auto& aType = row->GetField(m_ColAttachmentType);
				if (aType.HasValue())
				{
					if (aType.IsMulti())
					{
						if (aType.MultiValueContains<PooledString>(Util::GetFileAttachmentStr()))
						{
							enable = TRUE;
							break;
						}
					}
					else if (Util::GetFileAttachmentStr() == aType.GetValueStr())
					{
						enable = TRUE;
						break;
					}
				}
			}
		}
	}

    pCmdUI->Enable(enable);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridEvents::OnUpdateViewItemAttachment(CCmdUI* pCmdUI)
{
    // Enable only if attachment(s) are loaded
    BOOL enable = FALSE;
	if (hasNoTaskRunning() && GetSelectedRows().size() == 1)
    {
        auto row = *(GetSelectedRows().begin());
        if (GridUtil::isBusinessType<BusinessEvent>(row))
        {
            BusinessAttachment attachment;

            wstring userId			= row->GetField(GetColMetaID()).GetValueStr();
            wstring eventId			= row->GetField(GetColId()).GetValueStr();
            wstring attachmentId	= row->GetField(m_ColAttachmentId).GetValueStr();
            wstring attachmentType	= row->GetField(m_ColAttachmentType).GetValueStr();

            if (!userId.empty() && !eventId.empty() && !attachmentId.empty() && attachmentType == Util::GetItemAttachmentStr())
                enable = true;
        }
    }

    pCmdUI->Enable(enable);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridEvents::OnUpdateDeleteAttachment(CCmdUI* pCmdUI)
{
    BOOL enable = FALSE;
	if (hasNoTaskRunning())
	{
		const auto privilege = getPermission(ID_EVENTSGRID_DELETEATTACHMENT);
		// Enable only if all necessary info for deleting the attachment(s) is loaded
		for (auto& row : GetSelectedRows())
		{
			if (GridUtil::isBusinessType<BusinessEvent>(row) && O365Grid::IsAuthorizedByRoleDelegation(row, privilege, true))
			{
				if (row->GetField(m_ColAttachmentId).HasValue())
				{
					wstring entityId = row->GetField(GetColMetaID()).GetValueStr();
					wstring eventId = row->GetField(GetColId()).GetValueStr();
					if (!entityId.empty() && !eventId.empty())
					{
						enable = TRUE;
						break;
					}
				}
			}
		}
	}

    pCmdUI->Enable(enable);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridEvents::OnUpdateToggleInlineAttachments(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && !m_ShowInlineAttachments && !m_AllAttachments.empty());
    setTextFromProfUISCommand(*pCmdUI);
}

void GridEvents::loadMoreImpl(const std::vector<GridBackendRow*>& p_Rows)
{
    auto ids = GetAttachmentsRequestInfo(p_Rows, {}, true);
    if (!ids.empty())
    {
        CommandInfo info;
        info.Data() = ids;
        info.SetFrame(m_FrameOrigin);
	    info.SetRBACPrivilege(m_FrameOrigin->GetModuleCriteria().m_Privilege);
        CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Event, Command::ModuleTask::ListAttachments, info, { this, g_ActionNameSelectedAttachmentLoadInfo, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
    }
}

std::set<AttachmentsInfo::IDs> GridEvents::GetAttachmentsRequestInfo(const std::vector<GridBackendRow *>& p_Rows, const O365IdsContainer& p_EventIdsFilter, const bool p_ShowInlineWarningDialog)
{
    std::set<AttachmentsInfo::IDs> ids;

	const auto privilege = getPermission(ID_EVENTSGRID_SHOWATTACHMENTS);
    boost::YOpt<bool> ignoreNoAttachment;
    for (auto& row : p_Rows)
    {
        if (GridUtil::isBusinessType<BusinessEvent>(row) && O365Grid::IsAuthorizedByRoleDelegation(row, privilege, true) )
        {
            if (p_ShowInlineWarningDialog && !row->GetField(m_ColHasGraphAttachment).GetValueBool() )
            {
                if (!ignoreNoAttachment)
                {
                    YCodeJockMessageBox dlg(this,
                        DlgMessageBox::eIcon_Question,
                        YtriaTranslate::Do(GridEvents_GetAttachmentsRequestInfo_1, _YLOC("Load Attachment Info")).c_str(),
                        YtriaTranslate::Do(GridMessages_LoadMoreImpl_2, _YLOC("Some selected items only contain inline attachments. Do you want to load them too?")).c_str(),
                        _YTEXT(""),
                        { { IDYES, YtriaTranslate::Do(BackstagePanelSettings_OnReset_3, _YLOC("Yes")).c_str() },{ IDNO, YtriaTranslate::Do(BackstagePanelSettings_OnReset_4, _YLOC("No")).c_str() },{ IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });

                    const auto res = dlg.DoModal();
                    if (IDCANCEL == res)
                        return std::set<AttachmentsInfo::IDs>();

                    ignoreNoAttachment = IDNO == res;
                }
                if (*ignoreNoAttachment)
                    continue;
            }

            auto strMetaId = wstring(row->GetField(GetColMetaID()).GetValueStr());
            if (!strMetaId.empty())
            {
                auto strEventId = wstring(row->GetField(GetColId()).GetValueStr());
                ASSERT(!strEventId.empty());
                if (p_EventIdsFilter.empty() || p_EventIdsFilter.find(strEventId) != p_EventIdsFilter.end())
                    ids.emplace(strMetaId, strEventId, PooledString(), row->GetField(m_ColSubject).GetValueStr());
            }
        }
    }

    return ids;
}

void GridEvents::SetShowInlineAttachments(const bool& p_Val)
{
    m_ShowInlineAttachments = p_Val;
}

void GridEvents::HideMessageViewer()
{
	if (m_MessageViewer && m_MessageViewer->IsShowing())
		m_MessageViewer->ToggleView();
}

void GridEvents::onPostExplodeMultiValueSisterhood(GridBackendRow* p_Row, const std::set<GridBackendColumn*>& p_Columns)
{
	if (p_Columns.end() != p_Columns.find(m_ColAttachmentType))
	{
		GridUtil::SetAttachmentIcon(p_Row->GetField(m_ColAttachmentType), m_Icons);
		SetRowFileType(p_Row, p_Row->GetField(m_ColAttachmentName).GetValueStr(), false);
	}
}

void GridEvents::fillAttachmentFields(GridBackendRow* p_Row, const vector<BusinessAttachment>& p_Attachments)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		vector<PooledString>	attachmentType;
		vector<PooledString>	attachmentId;
		vector<PooledString>	attachmentName;
		vector<PooledString>	attachmentContentType;
		vector<__int3264>		attachmentSize;
		vector<YTimeDate>		attachmentLastModifiedDateTime;
		vector<BOOLItem>		attachmentIsInline;
		vector<PooledString>	attachmentExt;
		vector<PooledString>	attachmentNoDotExt;
		int32_t					fileTypeIcon = NO_ICON;

        for (const auto& attachment : p_Attachments)
        {
            ASSERT(attachment.GetIsInline().is_initialized());
            if (attachment.GetIsInline().is_initialized())
            {
                if (m_ShowInlineAttachments || (!m_ShowInlineAttachments && attachment.GetDataType() &&
                    !(*attachment.GetIsInline() == true &&
                        *attachment.GetDataType() == Util::GetFileAttachmentStr())))
                {
                    attachmentType.push_back(attachment.GetDataType() ? *attachment.GetDataType() : GridBackendUtil::g_NoValueString);
                    attachmentId.push_back(attachment.GetID());
                    attachmentName.push_back(attachment.GetName() ? *attachment.GetName() : GridBackendUtil::g_NoValueString);
                    attachmentContentType.push_back(attachment.GetContentType() ? *attachment.GetContentType() : GridBackendUtil::g_NoValueString);
                    attachmentSize.push_back(attachment.GetSize() ? static_cast<__int3264>(*attachment.GetSize()) : GridBackendUtil::g_NoValueNumberInt);
                    attachmentLastModifiedDateTime.push_back(attachment.GetLastModifiedDateTime() ? *attachment.GetLastModifiedDateTime() : GridBackendUtil::g_NoValueDate);
                    if (attachment.GetIsInline())
                        attachmentIsInline.push_back(*attachment.GetIsInline() ? TRUE : FALSE);
                    else
                        attachmentIsInline.push_back(GridBackendUtil::g_NoValueBoolItem);

					if (attachment.GetName())
					{
						const auto data = GetFileTypeAndIcon(*attachment.GetName(), false);
						attachmentNoDotExt.push_back(std::get<0>(data));
						attachmentExt.push_back(std::get<1>(data));
						if (p_Attachments.size() == 1)
							fileTypeIcon = std::get<2>(data);
					}
					else
					{
						attachmentNoDotExt.push_back(GridBackendUtil::g_NoValueString);
						attachmentExt.push_back(GridBackendUtil::g_NoValueString);
					}
                }
            }
        }

        p_Row->AddField(!p_Attachments.empty(), m_ColHasAnyAttachment);
		if (!p_Attachments.empty())
		{
			auto& attachmentTypeField = p_Row->AddField(attachmentType, m_ColAttachmentType);
			if (attachmentType.size() == 1)
                GridUtil::SetAttachmentIcon(attachmentTypeField, m_Icons);

			p_Row->AddField(attachmentId, m_ColAttachmentId);
			p_Row->AddField(attachmentName, m_ColAttachmentName);
			p_Row->AddField(attachmentContentType, m_ColAttachmentContentType);
			p_Row->AddField(attachmentSize, m_ColAttachmentSize);
			p_Row->AddField(attachmentLastModifiedDateTime, m_ColAttachmentLastModifiedDateTime);
			if (attachmentIsInline.size() == 1)
				p_Row->AddFieldForCheckBox(TRUE == attachmentIsInline.back().m_BOOL ? true : false, m_ColAttachmentIsInline);
			else
				p_Row->AddField(attachmentIsInline, m_ColAttachmentIsInline);

			p_Row->AddField(attachmentNoDotExt, GetColumnFileType(), fileTypeIcon);
			p_Row->AddField(attachmentExt, m_ColAttachmentNameExtOnly);
		}
		else
		{
			p_Row->RemoveField(m_ColAttachmentType);
			p_Row->RemoveField(m_ColAttachmentId);
			p_Row->RemoveField(m_ColAttachmentName);
			p_Row->RemoveField(m_ColAttachmentContentType);
			p_Row->RemoveField(m_ColAttachmentSize);
			p_Row->RemoveField(m_ColAttachmentLastModifiedDateTime);
			p_Row->RemoveField(m_ColAttachmentIsInline);
			p_Row->RemoveField(GetColumnFileType());
			p_Row->RemoveField(m_ColAttachmentNameExtOnly);
		}
	}
}

void GridEvents::ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_RefreshModificationStates, bool p_ShowPostUpdateError, bool p_UpdateGrid)
{
    ModuleUtil::MergeAttachments(m_AllAttachments, p_BusinessAttachments);

	GridIgnoreModification ignoramus(*this);
	const uint32_t options	= (p_UpdateGrid ? GridUpdaterOptions::UPDATE_GRID : 0)
							/*| GridUpdaterOptions::PURGE*/
							| (p_RefreshModificationStates ? GridUpdaterOptions::REFRESH_MODIFICATION_STATES : 0)
							| (p_ShowPostUpdateError ? GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS : 0)
							/*| GridUpdaterOptions::REFRESH_PROCESS_DATA*/
							| GridUpdaterOptions::PROCESS_LOADMORE;
	GridUpdaterOptions updateOptions(options, GetColumnsByPresets({g_ColumnsPresetMore}));
    GridUpdater updater(*this, updateOptions);

	loadAttachments(p_BusinessAttachments, updater);
}

void GridEvents::loadAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, GridUpdater& updater)
{
	for (const auto& byEntity : p_BusinessAttachments)
	{
		GridBackendField fMetaID(byEntity.first, GetColMetaID()); //user or group

		for (const auto& keyVal : byEntity.second)
		{
			const auto& attachments = keyVal.second;

			vector<GridBackendField> pk = { fMetaID, {keyVal.first, GetColId()} };

			GridBackendRow* row = m_RowIndex.GetExistingRow(pk, updater);

			if (nullptr != row)
			{
				updater.GetOptions().AddRowWithRefreshedValues(row);

				if (attachments.size() == 1 && attachments[0].GetError())
				{
					ClearFieldsByPreset(row, O365Grid::g_ColumnsPresetMore);
					updater.OnLoadingError(row, *attachments[0].GetError());
				}
				else
				{
					// Should we?
					// Clear previous error
					// NB: this is active in GridMessages

					//if (row->IsError())
					//	row->ClearStatus();

					fillAttachmentFields(row, attachments);
				}
			}
		}
	}
}

void GridEvents::ProcessDownloadedAttachments(const AttachmentsInfo& p_DownloadedAttachments)
{

}

GridBackendRow* GridEvents::fillRow(GridBackendRow* p_ParentRow, const BusinessEvent& p_Event)
{
	GridBackendField fMetaID(p_ParentRow->GetField(GetColMetaID()));
	GridBackendField fID(p_Event.GetID(), GetColId());
	GridBackendRow* row = m_RowIndex.GetRow({ fMetaID, fID }, true, true);

    ASSERT(nullptr != row);
    if (nullptr != row)
    {
		for (auto c : m_MetaColumns)
		{
			if (p_ParentRow->HasField(c))
				row->AddField(p_ParentRow->GetField(c));
			else
				row->RemoveField(c);
		}

        fillEventFields(row, p_Event);
    }
    return row;
}

void GridEvents::fillEventFields(GridBackendRow* p_Row, const BusinessEvent& p_Event)
{
    ASSERT(nullptr != p_Row);
    if (nullptr != p_Row)
    {
		SetRowObjectType(p_Row, p_Event, p_Event.HasFlag(BusinessObject::CANCELED));
        p_Row->AddFieldForRichText(p_Event.GetBodyPreview(), m_ColBodyPreview);
        p_Row->AddField(p_Event.GetCategories(), m_ColCategories);
        p_Row->AddField(p_Event.GetChangeKey(), m_ColChangeKey);
        p_Row->AddField(p_Event.GetCreatedDateTime(), m_ColCreatedDateTime);
        p_Row->AddField(p_Event.GetICalUId(), m_ColICalUid);
		p_Row->AddField(p_Event.GetImportance(), m_ColImportance);
        p_Row->AddField(p_Event.GetIsAllDay(), m_ColIsAllDay);
        p_Row->AddField(p_Event.GetIsCancelled(), m_ColIsCancelled);
        p_Row->AddField(p_Event.GetHasAttachments(), m_ColHasGraphAttachment);

        if (p_Event.GetOrganizer() && p_Event.GetOrganizer()->m_EmailAddress)
        {
            p_Row->AddField(p_Event.GetOrganizer()->m_EmailAddress->m_Name, m_ColOrganizerEmailAddressName);
            p_Row->AddField(p_Event.GetOrganizer()->m_EmailAddress->m_Address, m_ColOrganizerEmailAddressAddress);
        }
		else
		{
			p_Row->RemoveField(m_ColOrganizerEmailAddressName);
			p_Row->RemoveField(m_ColOrganizerEmailAddressAddress);
		}

        if (p_Event.GetStart())
        {
            p_Row->AddField(p_Event.GetStart()->DateTime, m_ColStartDateTime);
            p_Row->AddField(p_Event.GetStart()->TimeZone, m_ColStartTimeZone);
        }
		else
		{
			p_Row->RemoveField(m_ColStartDateTime);
			p_Row->RemoveField(m_ColStartTimeZone);
		}

        if (p_Event.GetEnd())
        {
            p_Row->AddField(p_Event.GetEnd()->DateTime, m_ColEndDateTime);
            p_Row->AddField(p_Event.GetEnd()->TimeZone, m_ColEndTimeZone);
        }
		else
		{
			p_Row->RemoveField(m_ColEndDateTime);
			p_Row->RemoveField(m_ColEndTimeZone);
		}

        if (p_Event.GetLocation())
        {
            const auto& location = *p_Event.GetLocation();
            p_Row->AddField(location.DisplayName, m_ColLocationDisplayName);
            p_Row->AddField(location.LocationEmailAddress, m_ColLocationLocationEmailAddress);
            if (location.Address)
            {
                p_Row->AddField(location.Address->Street, m_ColLocationAddressStreet);
                p_Row->AddField(location.Address->City, m_ColLocationAddressCity);
                p_Row->AddField(location.Address->State, m_ColLocationAddressState);
                p_Row->AddField(location.Address->CountryOrRegion, m_ColLocationAddressCountryOrRegion);
                p_Row->AddField(location.Address->PostalCode, m_ColLocationAddressPostalCode);
            }
			else
			{
				p_Row->RemoveField(m_ColLocationAddressStreet);
				p_Row->RemoveField(m_ColLocationAddressCity);
				p_Row->RemoveField(m_ColLocationAddressState);
				p_Row->RemoveField(m_ColLocationAddressCountryOrRegion);
				p_Row->RemoveField(m_ColLocationAddressPostalCode);
			}
        }
		else
		{
			p_Row->RemoveField(m_ColLocationDisplayName);
			p_Row->RemoveField(m_ColLocationLocationEmailAddress);
			p_Row->RemoveField(m_ColLocationAddressStreet);
			p_Row->RemoveField(m_ColLocationAddressCity);
			p_Row->RemoveField(m_ColLocationAddressState);
			p_Row->RemoveField(m_ColLocationAddressCountryOrRegion);
			p_Row->RemoveField(m_ColLocationAddressPostalCode);
		}

        if (p_Event.GetRecurrence())
        {
            const auto& recurrence = *p_Event.GetRecurrence();
            if (recurrence.Pattern)
            {
                const auto& pattern = *recurrence.Pattern;
				p_Row->AddField(pattern.Type, m_ColRecurrencePatternType);
                p_Row->AddField(pattern.Interval, m_ColRecurrencePatternInterval);
                p_Row->AddField(pattern.Month, m_ColRecurrencePatternMonth);
                p_Row->AddField(pattern.DayOfMonth, m_ColRecurrencePatternDayOfMonth);
                p_Row->AddField(pattern.DaysOfWeek, m_ColRecurrencePatternDaysOfWeek);
				p_Row->AddField(pattern.FirstDayOfWeek, m_ColRecurrencePatternFirstDayWeek);
				p_Row->AddField(pattern.Index, m_ColRecurrencePatternIndex);
            }
			else
			{
				p_Row->RemoveField(m_ColRecurrencePatternType);
				p_Row->RemoveField(m_ColRecurrencePatternInterval);
				p_Row->RemoveField(m_ColRecurrencePatternMonth);
				p_Row->RemoveField(m_ColRecurrencePatternDayOfMonth);
				p_Row->RemoveField(m_ColRecurrencePatternDaysOfWeek);
				p_Row->RemoveField(m_ColRecurrencePatternFirstDayWeek);
				p_Row->RemoveField(m_ColRecurrencePatternIndex);
			}

            if (recurrence.Range)
            {
                const auto& range = *recurrence.Range;
				p_Row->AddField(range.Type, m_ColRecurrenceRangeType);
                p_Row->AddField(range.StartDate, m_ColRecurrenceRangeStartDate);
                p_Row->AddField(range.EndDate, m_ColRecurrenceRangeEndDate);
                p_Row->AddField(range.RecurrenceTimeZone, m_ColRecurrenceRecurrenceTimeZone);
                p_Row->AddField(range.NumberOfOccurences, m_ColRecurrenceNumberOfOccurences);
            }
			else
			{
				p_Row->RemoveField(m_ColRecurrenceRangeType);
				p_Row->RemoveField(m_ColRecurrenceRangeStartDate);
				p_Row->RemoveField(m_ColRecurrenceRangeEndDate);
				p_Row->RemoveField(m_ColRecurrenceRecurrenceTimeZone);
				p_Row->RemoveField(m_ColRecurrenceNumberOfOccurences);
			}
        }

        p_Row->AddField(p_Event.GetIsOrganizer(), m_ColIsOrganizer);

        vector<PooledString> attendeesType;
        vector<PooledString> attendeesEmailAddressName;
        vector<PooledString> attendeesEmailAddressAddress;
        vector<PooledString> attendeesStatusResponse;
        vector<YTimeDate> attendeesStatusTime;
        for (const auto& attendee : p_Event.GetAttendees())
        {
            attendeesType.push_back(attendee.Type ? *attendee.Type : GridBackendUtil::g_NoValueString);
            attendeesEmailAddressName.push_back((attendee.EmailAddress && attendee.EmailAddress->m_Name) ? *attendee.EmailAddress->m_Name : GridBackendUtil::g_NoValueString);
            attendeesEmailAddressAddress.push_back((attendee.EmailAddress && attendee.EmailAddress->m_Address) ? *attendee.EmailAddress->m_Address : GridBackendUtil::g_NoValueString);
            attendeesStatusResponse.push_back((attendee.Status && attendee.Status->Response) ? *attendee.Status->Response : GridBackendUtil::g_NoValueString);
            attendeesStatusTime.push_back((attendee.Status && attendee.Status->Time) ? *attendee.Status->Time : GridBackendUtil::g_NoValueDate);
        }
        p_Row->AddField(attendeesType, m_ColAttendeesType);
        p_Row->AddField(attendeesEmailAddressName, m_ColAttendeesEmailAddressName);
        p_Row->AddField(attendeesEmailAddressAddress, m_ColAttendeesEmailAddressAddress);
        p_Row->AddField(attendeesStatusResponse, m_ColAttendeesStatusResponse);
        p_Row->AddField(attendeesStatusTime, m_ColAttendeesStatusTime);

        if (p_Event.GetResponseStatus())
        {
            const auto& responseStatus = p_Event.GetResponseStatus();
			p_Row->AddField(responseStatus->Response, m_ColResponseStatusResponse);
			p_Row->AddField(responseStatus->Time, m_ColResponseStatusTime);
        }
		else
		{
			p_Row->RemoveField(m_ColResponseStatusResponse);
			p_Row->RemoveField(m_ColResponseStatusTime);			
		}

        p_Row->AddField(p_Event.GetIsReminderOn(), m_ColIsReminderOn);
        p_Row->AddField(p_Event.GetLastModifiedDateTime(), m_ColLastModifiedDateTime);
        p_Row->AddFieldForHyperlink(p_Event.GetOnlineMeetingUrl(), m_ColOnlineMeetingUrl);
        p_Row->AddField(p_Event.GetOriginalEndTimeZone(), m_ColOriginalEndTimeZone);
        p_Row->AddField(p_Event.GetOriginalStart(), m_ColOriginalStart);
        p_Row->AddField(p_Event.GetOriginalStartTimeZone(), m_ColOriginalStartTimeZone);
        p_Row->AddField(p_Event.GetReminderMinutesBeforeStart(), m_ColReminderMinutesBeforeStart);
        p_Row->AddField(p_Event.GetResponseRequested(), m_ColResponseRequested);
		p_Row->AddField(p_Event.GetSensitivity(), m_ColSensitivity);
        p_Row->AddField(p_Event.GetSeriesMasterId(), m_ColSeriesMasterId);
		p_Row->AddField(p_Event.GetShowAs(), m_ColShowAs);

		if (p_Event.GetBody())
            p_Row->AddField(p_Event.GetBody()->ContentType, m_ColBodyContentType);
		else
			p_Row->RemoveField(m_ColBodyContentType);

        p_Row->AddField(p_Event.GetSubject(), m_ColSubject);
		p_Row->AddField(p_Event.GetType(), m_ColType);
        p_Row->AddFieldForHyperlink(p_Event.GetWebLink(), m_ColWebLink);
    }

    MessageBodyData* messageBodyData = reinterpret_cast<MessageBodyData*>(p_Row->GetLParam());
    if (nullptr != messageBodyData)
    {
        if (p_Event.GetBody() && p_Event.GetBody()->Content)
        {
            messageBodyData->m_BodyContent = p_Event.GetBody()->Content;
        }
        else
        {
            p_Row->SetLParam(NULL);
            delete messageBodyData;
        }
    }
    else
    {
        if (p_Event.GetBody() && p_Event.GetBody()->Content)
            p_Row->SetLParam((LPARAM) new MessageBodyData(*p_Event.GetBody()->Content));
    }
}

std::vector<SubItemKey> GridEvents::GetDeleteAttachmentsData(bool p_Selected)
{
    std::vector<SubItemKey> attachments;
    if (!p_Selected)
    {
        for (const auto& deletedAttachment : GetModifications().GetSubItemDeletions())
            attachments.push_back(deletedAttachment->GetKey());
    }
    else
    {
        for (auto row : GetSelectedRows())
        {
            for (const auto& deletedAttachment : GetModifications().GetSubItemDeletions())
            {
                if (deletedAttachment->IsCorrespondingRow(row))
                    attachments.push_back(deletedAttachment->GetKey());
            }
        }
    }

	return attachments;
}

BusinessEvent GridEvents::getBusinessObject(GridBackendRow* row) const
{
	ASSERT(GridUtil::IsBusinessEvent(row));

	BusinessEvent businessEvent;

	{
		const auto& field = row->GetField(GetColId());
		if (field.HasValue())
			businessEvent.SetID(field.GetValueStr());
	}

	{
		const auto& field = row->GetField(m_ColBodyPreview);
		if (field.HasValue())
			businessEvent.SetBodyPreview(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColCategories);
		if (field.HasValue())
		{
			if (field.IsMulti())
				businessEvent.SetCategories(boost::YOpt<vector<PooledString>>(*field.GetValuesMulti<PooledString>()));
			else
				businessEvent.SetCategories(boost::YOpt<vector<PooledString>>(vector<PooledString>({ field.GetValueStr() })));
		}
	}

	{
		const auto& field = row->GetField(m_ColChangeKey);
		if (field.HasValue())
			businessEvent.SetChangeKey(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColCreatedDateTime);
		if (field.HasValue())
			businessEvent.SetCreatedDateTime(field.GetValueTimeDate());
	}

	{
		const auto& field = row->GetField(m_ColHasAnyAttachment);
		if (field.HasValue())
			businessEvent.SetHasAttachments(field.GetValueBool());
	}

	{
		const auto& field = row->GetField(m_ColICalUid);
		if (field.HasValue())
			businessEvent.SetICalUId(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColImportance);
		if (field.HasValue())
			businessEvent.SetImportance(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColIsAllDay);
		if (field.HasValue())
			businessEvent.SetIsAllDay(field.GetValueBool());
	}

	{
		const auto& field = row->GetField(m_ColIsCancelled);
		if (field.HasValue())
			businessEvent.SetIsCancelled(field.GetValueBool());
	}

	{
		const auto& field = row->GetField(m_ColIsOrganizer);
		if (field.HasValue())
			businessEvent.SetIsOrganizer(field.GetValueBool());
	}

	{
		const auto& field = row->GetField(m_ColIsReminderOn);
		if (field.HasValue())
			businessEvent.SetIsReminderOn(field.GetValueBool());
	}

	{
		const auto& field = row->GetField(m_ColLastModifiedDateTime);
		if (field.HasValue())
			businessEvent.SetLastModifiedDateTime(field.GetValueTimeDate());
	}

	{
		const auto& field = row->GetField(m_ColOnlineMeetingUrl);
		if (field.HasValue())
			businessEvent.SetOnlineMeetingUrl(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColOriginalEndTimeZone);
		if (field.HasValue())
			businessEvent.SetOriginalEndTimeZone(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColOriginalStart);
		if (field.HasValue())
			businessEvent.SetOriginalStart(field.GetValueTimeDate());
	}

	{
		const auto& field = row->GetField(m_ColOriginalStartTimeZone);
		if (field.HasValue())
			businessEvent.SetOriginalStartTimeZone(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColReminderMinutesBeforeStart);
		if (field.HasValue())
			businessEvent.SetReminderMinutesBeforeStart(static_cast<int32_t>(field.GetValueLong()));
	}

	{
		const auto& field = row->GetField(m_ColResponseRequested);
		if (field.HasValue())
			businessEvent.SetResponseRequested(field.GetValueBool());
	}

	{
		const auto& field = row->GetField(m_ColSensitivity);
		if (field.HasValue())
			businessEvent.SetSensitivity(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColSeriesMasterId);
		if (field.HasValue())
			businessEvent.SetSeriesMasterId(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColShowAs);
		if (field.HasValue())
			businessEvent.SetShowAs(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColSubject);
		if (field.HasValue())
			businessEvent.SetSubject(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColType);
		if (field.HasValue())
			businessEvent.SetType(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = row->GetField(m_ColWebLink);
		if (field.HasValue())
			businessEvent.SetWebLink(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	auto ownerRow = row->GetParentRow();
	if (nullptr != ownerRow)
	{
		ASSERT(GridUtil::IsBusinessUser(ownerRow) || GridUtil::IsBusinessGroup(ownerRow));

		const auto& field = ownerRow->GetField(GetColId());
		if (field.HasValue())
			businessEvent.SetOwnerId(boost::YOpt<PooledString>(field.GetValueStr()), GridUtil::IsBusinessUser(ownerRow));
	}
	else
	{
		ASSERT(m_MyDataMeHandler);
		const auto& field = m_MyDataMeHandler->GetField(GetColId());
		if (field.HasValue())
			businessEvent.SetOwnerId(boost::YOpt<PooledString>(field.GetValueStr()), true);
	}

	return businessEvent;
}

void GridEvents::UpdateBusinessObjects(const vector<BusinessEvent>& p_Events, bool p_SetModifiedStatus)
{
	// Implement ME!
	ASSERT(false);
}

void GridEvents::RemoveBusinessObjects(const vector<BusinessEvent>& p_Events)
{
	// Implement ME!
	ASSERT(false);
}

void GridEvents::InitializeCommands()
{
    ModuleO365Grid<BusinessEvent>::InitializeCommands();

	std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

    const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_EVENTSGRID_CHANGEOPTIONS;
		_cmd.m_sMenuText = _T("Change Options");
		_cmd.m_sToolbarText = _T("Change Options");
		_cmd.m_sTipTool = _T("Adjust cut-off date, optional data and filter.");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANGEMODULEOPTIONS_16x16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_EVENTSGRID_CHANGEOPTIONS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Change Options"),
			_T("Adjust cut-off date, optional data and filter."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_EVENTSGRID_SHOWEVENTMESSAGEBODY;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridEvents_InitializeCommands_1, _YLOC("Preview Body...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridEvents_InitializeCommands_2, _YLOC("Preview Body...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGE_CONTENT_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_EVENTSGRID_SHOWEVENTMESSAGEBODY, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridEvents_InitializeCommands_3, _YLOC("Preview Event Body")).c_str(),
			YtriaTranslate::Do(GridEvents_InitializeCommands_4, _YLOC("Preview the event message body.\nOnly one event can be previewed at a time.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_EVENTSGRID_SHOWATTACHMENTS;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridEvents_InitializeCommands_5, _YLOC("Load Attachment Info")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridEvents_InitializeCommands_10, _YLOC("Load Attachment Info")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_EVENTS_SHOWATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_EVENTSGRID_SHOWATTACHMENTS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridEvents_InitializeCommands_15, _YLOC("Load Attachment Info")).c_str(),
			YtriaTranslate::Do(GridEvents_InitializeCommands_16, _YLOC("Load the additional attachment info for all selected rows.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_EVENTSGRID_DOWNLOADATTACHMENTS;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridEvents_InitializeCommands_6, _YLOC("Download Attachments...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridEvents_InitializeCommands_11, _YLOC("Download Attachments...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_EVENTS_DOWNLOADATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_EVENTSGRID_DOWNLOADATTACHMENTS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridEvents_InitializeCommands_17, _YLOC("Download Attachments")).c_str(),
			YtriaTranslate::Do(GridEvents_InitializeCommands_18, _YLOC("Download the attachment files of all selected items to your computer.\r\nYou can select a destination folder in the resulting dialog.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_EVENTSGRID_VIEW_ITEM_ATTACHMENT;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridEvents_InitializeCommands_7, _YLOC("Preview Item...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridEvents_InitializeCommands_12, _YLOC("Preview Item...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_EVENTS_VIEWITEMATTACHMENT_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_EVENTSGRID_VIEW_ITEM_ATTACHMENT, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridEvents_InitializeCommands_19, _YLOC("Preview Attached Item")).c_str(),
			YtriaTranslate::Do(GridEvents_InitializeCommands_20, _YLOC("Preview the body of an attached email, vCard, or calendar.\nYou must first load the attachment info and select the item in the grid for this function to be active.\nOnly one item can be previewed at a time.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
        _cmd.m_nCmdID		= ID_EVENTSGRID_DELETEATTACHMENT;
        _cmd.m_sMenuText	= YtriaTranslate::Do(GridEvents_InitializeCommands_8, _YLOC("Delete Attachments")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridEvents_InitializeCommands_13, _YLOC("Delete Attachments")).c_str();
        _cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_EVENTS_DELETEATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_EVENTSGRID_DELETEATTACHMENT, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridEvents_InitializeCommands_21, _YLOC("Delete Attachments")).c_str(),
			YtriaTranslate::Do(GridEvents_InitializeCommands_22, _YLOC("Delete all selected attachment files.")).c_str(),
			_cmd.m_sAccelText);
    }

    {
        CExtCmdItem _cmd;
        _cmd.m_nCmdID = ID_EVENTSGRID_TOGGLEINLINEATTACHMENTS;
        _cmd.m_sMenuText = YtriaTranslate::Do(GridEvents_InitializeCommands_9, _YLOC("Show Inline Attachments")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridEvents_InitializeCommands_14, _YLOC("Show Inline Attachments")).c_str();
        _cmd.m_sAccelText = _YTEXT("");
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

        HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_EVENTS_TOGGLEINLINEATTACHMENTS_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_EVENTSGRID_TOGGLEINLINEATTACHMENTS, hIcon, false);

        setRibbonTooltip(_cmd.m_nCmdID,
            YtriaTranslate::Do(GridEvents_InitializeCommands_23, _YLOC("Show Inline Attachments")).c_str(),
            YtriaTranslate::Do(GridEvents_InitializeCommands_24, _YLOC("Include inline attachments in the grid.\nBy default, inline attachments, like embedded images, are not listed.\nTo go back to default, use the Refresh All button.")).c_str(),
            _cmd.m_sAccelText);
    }
}

void GridEvents::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {
		pPopup->ItemInsert(ID_EVENTSGRID_CHANGEOPTIONS);
        pPopup->ItemInsert(ID_EVENTSGRID_SHOWEVENTMESSAGEBODY);
		pPopup->ItemInsert(); // Sep
        pPopup->ItemInsert(ID_EVENTSGRID_SHOWATTACHMENTS);
		pPopup->ItemInsert(ID_EVENTSGRID_TOGGLEINLINEATTACHMENTS);
        pPopup->ItemInsert(ID_EVENTSGRID_DOWNLOADATTACHMENTS);
        pPopup->ItemInsert(ID_EVENTSGRID_VIEW_ITEM_ATTACHMENT);
        pPopup->ItemInsert(ID_EVENTSGRID_DELETEATTACHMENT);
        pPopup->ItemInsert(); // Sep
    }

	ModuleO365Grid<BusinessEvent>::OnCustomPopupMenu(pPopup, nStart);
}

void GridEvents::OnGbwSelectionChangedSpecific()
{
    if (m_MessageViewer && m_MessageViewer->IsShowing())
    {
        // Try to less freeze on selection change by posting a message.
        YCallbackMessage::DoPost([this]()
			{
				if (::IsWindow(m_hWnd) && m_MessageViewer && m_MessageViewer->IsShowing())
					m_MessageViewer->ShowSelectedMessageBody();
			}
        );
    }
}

BOOL GridEvents::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return ModuleO365Grid<BusinessEvent>::PreTranslateMessage(pMsg);
}

bool GridEvents::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	if (m_TemplateUsers && m_TemplateUsers->GetSnapshotValue(p_Value, p_Field, p_Column)
		|| m_TemplateGroups && m_TemplateGroups->GetSnapshotValue(p_Value, p_Field, p_Column))
		return true;

	return ModuleO365Grid<BusinessEvent>::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridEvents::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (m_TemplateUsers && m_TemplateUsers->LoadSnapshotValue(p_Value, p_Row, p_Column)
		|| m_TemplateGroups && m_TemplateGroups->LoadSnapshotValue(p_Value, p_Row, p_Column))
		return true;

	return ModuleO365Grid<BusinessEvent>::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

bool GridEvents::GetSnapshotAdditionalValue(wstring& p_Value, GridBackendRow& p_Row)
{
	MessageBodyData* messageBodyData = reinterpret_cast<MessageBodyData*>(p_Row.GetLParam());
	if (nullptr != messageBodyData && messageBodyData->m_BodyContent)
	{
		p_Value = *messageBodyData->m_BodyContent;
		return true;
	}

	return false;
}

bool GridEvents::SetSnapshotAdditionalValue(const wstring& p_Value, GridBackendRow& p_Row)
{
	MessageBodyData* messageBodyData = reinterpret_cast<MessageBodyData*>(p_Row.GetLParam());
	if (nullptr != messageBodyData)
		messageBodyData->m_BodyContent = p_Value;
	else
		p_Row.SetLParam((LPARAM) new MessageBodyData(PooledString(p_Value)));

	return true;
}

GridBackendRow* GridEvents::addParentRow(const BusinessUser& p_User, GridUpdater& p_Updater)
{
	if (m_HasLoadMoreAdditionalInfo)
	{
		if (p_User.IsMoreLoaded())
			m_MetaIDsMoreLoaded.insert(p_User.GetID());
		else
			m_MetaIDsMoreLoaded.erase(p_User.GetID());
	}

	ASSERT(m_TemplateUsers);
	if (m_TemplateUsers)
		return m_TemplateUsers->AddRow(*this, p_User, {}, p_Updater, false, true);
	return nullptr;
}

GridBackendRow* GridEvents::addParentRow(const BusinessGroup& p_Group, GridUpdater& p_Updater)
{
	if (m_HasLoadMoreAdditionalInfo)
	{
		if (p_Group.IsMoreLoaded())
			m_MetaIDsMoreLoaded.insert(p_Group.GetID());
		else
			m_MetaIDsMoreLoaded.erase(p_Group.GetID());
	}

	ASSERT(m_TemplateGroups);
	if (m_TemplateGroups)
		return m_TemplateGroups->AddRow(*this, p_Group, {}, p_Updater, false, nullptr, {}, true);
	return nullptr;
}

template<class BusinessObjectType>
void GridEvents::buildTreeViewImpl(const O365DataMap<BusinessObjectType, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_Updates, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge, bool p_NoPurge)
{
	if (BusinessObjectType().get_type() == BusinessGroup().get_type())
	{
		ASSERT(m_Origin == Origin::Group);
		ASSERT(!m_TemplateUsers && m_TemplateGroups);
		ASSERT(!IsMyData());
	}
	else
	{
		ASSERT(BusinessObjectType().get_type() == BusinessUser().get_type());
		ASSERT(m_Origin == Origin::User);
		ASSERT(m_TemplateUsers && !m_TemplateGroups);
		ASSERT(!IsMyData() || p_Events.size() == 1);

	}

	const uint32_t options = (p_Refresh ? GridUpdaterOptions::UPDATE_GRID : 0)
		| (p_NoPurge ? 0 : (p_FullPurge || IsMyData() ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE))
		| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		| GridUpdaterOptions::REFRESH_PROCESS_DATA
		/*| GridUpdaterOptions::PROCESS_LOADMORE*/
		;

	GridUpdater updater(*this, GridUpdaterOptions(options), p_Updates);
	GridIgnoreModification ignoramus(*this);

	for (const auto& keyVal : p_Events)
	{
		auto parentRow = addParentRow(keyVal.first, updater);
		ASSERT(nullptr != parentRow);
		if (nullptr != parentRow)
		{
			if (!IsMyData()) // Row will be deleted below
			{
				updater.GetOptions().AddRowWithRefreshedValues(parentRow);
				updater.GetOptions().AddPartialPurgeParentRow(parentRow);
			}

			parentRow->SetHierarchyParentToHide(true);

			buildTreeItem(keyVal.first, keyVal.second, parentRow, updater);
			AddRoleDelegationFlag(parentRow, keyVal.first);

			if (IsMyData())
			{
				ASSERT(m_MyDataMeHandler);
				ASSERT(parentRow->HasChildrenRows() || keyVal.second.empty());
				m_MyDataMeHandler->GetRidOfMeRow(*this, parentRow);
			}
		}
	}

	loadAttachments(p_BusinessAttachments, updater);
}

void GridEvents::buildTreeItem(const BusinessUser& p_User, const vector<BusinessEvent>& p_Events, GridBackendRow* p_ParentRow, GridUpdater& p_GridUpdater)
{
	if (p_Events.size() == 1 && p_Events[0].GetError())
	{
		p_GridUpdater.OnLoadingError(p_ParentRow, *p_Events[0].GetError());
	}
	else
	{
		// Clear previous error
		if (p_ParentRow->IsError())
			p_ParentRow->ClearStatus();

		for (const auto& event : p_Events)
		{
			auto eventRow = fillRow(p_ParentRow, event);
			ASSERT(nullptr != eventRow);
			if (nullptr != eventRow)
			{
				p_GridUpdater.GetOptions().AddRowWithRefreshedValues(eventRow);
				eventRow->SetParentRow(p_ParentRow);
			}
		}
	}
}

void GridEvents::buildTreeItem(const BusinessGroup& p_Group, const vector<BusinessEvent>& p_Events, GridBackendRow* p_ParentRow, GridUpdater& p_GridUpdater)
{
	ASSERT(!IsMyData());
	if (p_Events.size() == 1 && p_Events[0].GetError())
	{
		p_GridUpdater.OnLoadingError(p_ParentRow, *p_Events[0].GetError());
	}
	else
	{
		// Clear previous error
		if (p_ParentRow->IsError())
			p_ParentRow->ClearStatus();

		for (const auto& event : p_Events)
		{
			auto eventRow = fillRow(p_ParentRow, event);
			ASSERT(nullptr != eventRow);
			if (nullptr != eventRow)
			{
				p_GridUpdater.GetOptions().AddRowWithRefreshedValues(eventRow);
				eventRow->SetParentRow(p_ParentRow);
			}
		}
	}
}

GridBackendColumn*& GridEvents::getColMetaDisplayName() const
{
	return m_TemplateUsers ? m_TemplateUsers->m_ColumnDisplayName : m_TemplateGroups->m_ColumnDisplayName;
}

GridBackendColumn*& GridEvents::getColMetaID() const
{
	return m_TemplateUsers ? m_TemplateUsers->m_ColumnID : m_TemplateGroups->m_ColumnID;
}
