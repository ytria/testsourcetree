#pragma once

#include "IJsonSerializer.h"
#include "TeamMessagingSettings.h"

class TeamMessagingSettingsSerializer : public IJsonSerializer
{
public:
    TeamMessagingSettingsSerializer(const TeamMessagingSettings& p_Settings);
    void Serialize() override;

private:
    const TeamMessagingSettings& m_Settings;
};

