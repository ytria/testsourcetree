#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "TargetResource.h"

class TargetResourceDeserializer : public JsonObjectDeserializer, public Encapsulate<TargetResource>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

