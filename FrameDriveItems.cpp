#include "FrameDriveItems.h"

#include "AutomationNames.h"
#include "Command.h"
#include "CommandDispatcher.h"

class FrameDriveItems::GridLogger
	: public GridFrameLogger
{
public:
	GridLogger(FrameDriveItems& p_Frame)
		: GridFrameLogger(p_Frame)
		, m_DriveFrame(p_Frame)
	{
	}

	virtual void LogSpecific(const LogEntry& p_Entry) const override final
	{
		if (!m_DriveFrame.isDownloadProcessRunning())
			GridFrameLogger::LogSpecific(p_Entry);
	}

private:
	FrameDriveItems& m_DriveFrame;
};

// ============================================================================

IMPLEMENT_DYNAMIC(FrameDriveItems, GridFrameBase)

BEGIN_MESSAGE_MAP(FrameDriveItems, GridFrameBase)
    ON_UPDATE_COMMAND_UI(ID_365REFRESH,					OnUpdate365Refresh)
	ON_UPDATE_COMMAND_UI(ID_365REFRESHSELECTED,			OnUpdate365RefreshSelection)
	ON_UPDATE_COMMAND_UI(ID_365REFRESHSELECTED_USERS,	OnUpdate365RefreshSelection)
	ON_UPDATE_COMMAND_UI(ID_365REFRESHSELECTED_GROUPS,	OnUpdate365RefreshSelection)
	ON_UPDATE_COMMAND_UI(ID_365REFRESHSELECTED_SITES,	OnUpdate365RefreshSelection)
END_MESSAGE_MAP()

FrameDriveItems::FrameDriveItems(Origin p_Origin, bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame, false, std::make_shared<FrameDriveItems::GridLogger>(*this))
	, m_driveItemsGrid(p_Origin, p_IsMyData)
	, m_DownloadRunning(false)
{
    m_CreateAddRemoveToSelection = !p_IsMyData && (p_Origin == Origin::User || p_Origin == Origin::Group);
    m_CreateRefreshPartial = !p_IsMyData;
	m_CreateShowContext = !p_IsMyData && p_Origin != Origin::Channel;
	m_CreateApplyPartial = !p_IsMyData;
}

void FrameDriveItems::ShowDriveItem(const O365DataMap<BusinessUser, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses)
{
	if (CompliesWithDataLoadPolicy())
		m_driveItemsGrid.BuildTreeView(p_DriveItems, p_UpdateOperations, p_UpdateGrid, p_FullPurge, p_Permissions, p_CheckoutStatuses);
}

void FrameDriveItems::ShowDriveItem(const O365DataMap<BusinessGroup, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses)
{
	if (CompliesWithDataLoadPolicy())
		m_driveItemsGrid.BuildTreeView(p_DriveItems, p_UpdateOperations, p_UpdateGrid, p_FullPurge, p_Permissions, p_CheckoutStatuses);
}

void FrameDriveItems::ShowDriveItem(const O365DataMap<BusinessSite, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses)
{
	if (CompliesWithDataLoadPolicy())
		m_driveItemsGrid.BuildTreeView(p_DriveItems, p_UpdateOperations, p_UpdateGrid, p_FullPurge, p_Permissions, p_CheckoutStatuses);
}

void FrameDriveItems::ShowDriveItem(const O365DataMap<BusinessChannel, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses)
{
	if (CompliesWithDataLoadPolicy())
		m_driveItemsGrid.BuildTreeView(p_DriveItems, p_UpdateOperations, p_UpdateGrid, p_FullPurge, p_Permissions, p_CheckoutStatuses);
}

void FrameDriveItems::OnUpdate365Refresh(CCmdUI* pCmdUI)
{
	YTestCmdUI cmdUi(pCmdUI->m_nID);
	GridFrameBase::OnUpdate365Refresh(&cmdUi);
	pCmdUI->Enable(cmdUi.m_bEnabled && m_driveItemsGrid.GetNbDownloadsOccuring() == 0);
}

void FrameDriveItems::OnUpdate365RefreshSelection(CCmdUI* pCmdUI)
{
	YTestCmdUI cmdUi(pCmdUI->m_nID);
	GridFrameBase::OnUpdate365RefreshSelection(&cmdUi);
	pCmdUI->Enable(cmdUi.m_bEnabled && m_driveItemsGrid.GetNbDownloadsOccuring() == 0);
}

void FrameDriveItems::createGrid()
{
	m_driveItemsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameDriveItems::GetRefreshPartialControlConfig()
{
	O365ControlConfig config;
	if (GetOrigin() == Origin::User || GetOrigin() == Origin::DeletedUser)
	{
		config = GetRefreshPartialControlConfigUsers();
	}
	else if (GetOrigin() == Origin::Group)
	{
		config = GetRefreshPartialControlConfigGroups();
	}
	else if (GetOrigin() == Origin::Site)
	{
		config = GetRefreshPartialControlConfigSites();
	}
	else if (GetOrigin() == Origin::Channel)
	{
		config = GetRefreshPartialControlConfigChannels();
	}
	else
		ASSERT(FALSE);

	return config;
}

GridFrameBase::O365ControlConfig FrameDriveItems::GetApplyPartialControlConfig()
{
	O365ControlConfig config;
	if (GetOrigin() == Origin::User || GetOrigin() == Origin::DeletedUser)
	{
		config = GetApplyPartialControlConfigUsers();
	}
	else if (GetOrigin() == Origin::Group)
	{
		config = GetApplyPartialControlConfigGroups();
	}
	else if (GetOrigin() == Origin::Site)
	{
		config = GetApplyPartialControlConfigSites();
	}
	else if (GetOrigin() == Origin::Channel)
	{
		config = GetApplyPartialControlConfigChannels();
	}
	else
		ASSERT(FALSE);

	return config;
}

// returns false if no frame specific tab is needed.
bool FrameDriveItems::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(tab);
	CreateRefreshGroup(tab);
	CreateApplyGroup(tab);
	CreateRevertGroup(tab);

	if (CRMpipe().IsFeatureSandboxed() && GetSearchCriteria())
	{
		CXTPRibbonGroup* dataGroup = findGroup(tab, g_ActionsDataGroup.c_str());
		ASSERT(nullptr != dataGroup);
		if (nullptr != dataGroup)
		{
			auto ctrl = dataGroup->Add(xtpControlButton, ID_DRIVEITEMSGRID_CHANGESEARCHCRITERIA);
			setImage({ ID_DRIVEITEMSGRID_CHANGESEARCHCRITERIA }, IDB_CHANGEMODULEOPTIONS, xtpImageNormal);
			setImage({ ID_DRIVEITEMSGRID_CHANGESEARCHCRITERIA }, IDB_CHANGEMODULEOPTIONS_16x16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	{
		auto groupCtrl = tab.AddGroup(_T("Manage Files"));

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_DRIVEITEMSGRID_DELETE);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_DELETE }, IDB_DELETE, xtpImageNormal);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_DELETE }, IDB_DELETE_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		if (!CRMpipe().IsFeatureSandboxed() || !GetSearchCriteria())
		{
			{
				auto ctrl = groupCtrl->Add(xtpControlButton, ID_DRIVEITEMSGRID_RENAME);
				GridFrameBase::setImage({ ID_DRIVEITEMSGRID_RENAME }, IDB_DRIVEITEMSGRID_RENAME, xtpImageNormal);
				GridFrameBase::setImage({ ID_DRIVEITEMSGRID_RENAME }, IDB_DRIVEITEMSGRID_RENAME_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			{
				auto ctrl = groupCtrl->Add(xtpControlButton, ID_DRIVEITEMSGRID_CREATEFOLDER);
				GridFrameBase::setImage({ ID_DRIVEITEMSGRID_CREATEFOLDER }, IDB_DRIVEITEMSGRID_CREATEFOLDER, xtpImageNormal);
				GridFrameBase::setImage({ ID_DRIVEITEMSGRID_CREATEFOLDER }, IDB_DRIVEITEMSGRID_CREATEFOLDER_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			{
				auto ctrl = groupCtrl->Add(xtpControlButton, ID_DRIVEITEMSGRID_IMPORTFOLDER);
				GridFrameBase::setImage({ ID_DRIVEITEMSGRID_IMPORTFOLDER }, IDB_DRIVEITEMSGRID_IMPORTFOLDER, xtpImageNormal);
				GridFrameBase::setImage({ ID_DRIVEITEMSGRID_IMPORTFOLDER }, IDB_DRIVEITEMSGRID_IMPORTFOLDER_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			{
				auto ctrl = groupCtrl->Add(xtpControlButton, ID_DRIVEITEMSGRID_ADDFILE);
				GridFrameBase::setImage({ ID_DRIVEITEMSGRID_ADDFILE }, IDB_DRIVEITEMSGRID_ADDFILE, xtpImageNormal);
				GridFrameBase::setImage({ ID_DRIVEITEMSGRID_ADDFILE }, IDB_DRIVEITEMSGRID_ADDFILE_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			{
				auto ctrl = groupCtrl->Add(xtpControlButton, ID_DRIVEITEMSGRID_SHOWFOLDERINFLAT);
				GridFrameBase::setImage({ ID_DRIVEITEMSGRID_SHOWFOLDERINFLAT }, IDB_DRIVEITEMSGRID_SHOWFOLDERINFLAT, xtpImageNormal);
				GridFrameBase::setImage({ ID_DRIVEITEMSGRID_SHOWFOLDERINFLAT }, IDB_DRIVEITEMSGRID_SHOWFOLDERINFLAT_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}
		}

		{
			auto ctrl = groupCtrl->Add(xtpControlButton, ID_DRIVEITEMSGRID_DOWNLOAD);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_DOWNLOAD }, IDB_DRIVEITEMSGRID_DOWNLOAD, xtpImageNormal);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_DOWNLOAD }, IDB_DRIVEITEMSGRID_DOWNLOAD_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	{
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
		auto checkoutGroupCtrl = tab.AddGroup(_T("Checkout and retention"));
		{
			auto ctrl = checkoutGroupCtrl->Add(xtpControlButton, ID_DRIVEITEMSGRID_LOADCHECKOUTSTATUS);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_LOADCHECKOUTSTATUS }, IDB_DRIVEITEMSGRID_LOADCHECKOUTSTATUS, xtpImageNormal);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_LOADCHECKOUTSTATUS }, IDB_DRIVEITEMSGRID_LOADCHECKOUTSTATUS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
#else
		auto checkoutGroupCtrl = tab.AddGroup(_T("Checkout and retention"));
#endif

		{
			auto ctrl = checkoutGroupCtrl->Add(xtpControlButton, ID_DRIVEITEMSGRID_CHECKIN);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_CHECKIN }, IDB_DRIVEITEMSGRID_CHECKIN, xtpImageNormal);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_CHECKIN }, IDB_DRIVEITEMSGRID_CHECKIN_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = checkoutGroupCtrl->Add(xtpControlButton, ID_DRIVEITEMSGRID_CHECKOUT);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_CHECKOUT }, IDB_DRIVEITEMSGRID_CHECKOUT, xtpImageNormal);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_CHECKOUT }, IDB_DRIVEITEMSGRID_CHECKOUT_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}	

	auto permGroup = tab.AddGroup(YtriaTranslate::Do(FrameDriveItems_customizeActionsRibbonTab_1, _YLOC("Permissions")).c_str());
	ASSERT(nullptr != permGroup);
	if (nullptr != permGroup)
	{
		setGroupImage(*permGroup, 0);

		{
			auto ctrl = permGroup->Add(xtpControlSplitButtonPopup, ID_DRIVEITEMSGRID_VIEWPERMISSIONS);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_VIEWPERMISSIONS }, IDB_DRIVEITEMSGRID_VIEWPERMISSIONS, xtpImageNormal);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_VIEWPERMISSIONS }, IDB_DRIVEITEMSGRID_VIEWPERMISSIONS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
	
			auto popup = dynamic_cast<CXTPControlPopup*>(ctrl);
			popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_DRIVEITEMSGRID_VIEWPERMISSIONS);
			popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODE);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODE }, IDB_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODE_16X16, xtpImageNormal);
			popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODEANDFLAT);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODEANDFLAT }, IDB_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODEANDFLAT_16X16, xtpImageNormal);
		}

		{
			auto ctrl = permGroup->Add(xtpControlButton, ID_DRIVEITEMSGRID_TOGGLEOWNERPERMISSIONS);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_TOGGLEOWNERPERMISSIONS }, IDB_DRIVEITEMSGRID_TOGGLEOWNERPERMISSIONS, xtpImageNormal);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_TOGGLEOWNERPERMISSIONS }, IDB_DRIVEITEMSGRID_TOGGLEOWNERPERMISSIONS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = permGroup->Add(xtpControlButton, ID_DRIVEITEMSGRID_DELETEPERMISSIONS);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_DELETEPERMISSIONS }, IDB_DRIVEITEMSGRID_DELETEPERMISSIONS, xtpImageNormal);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_DELETEPERMISSIONS }, IDB_DRIVEITEMSGRID_DELETEPERMISSIONS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = permGroup->Add(xtpControlButton, ID_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD }, IDB_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD, xtpImageNormal);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD }, IDB_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = permGroup->Add(xtpControlButton, ID_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE }, IDB_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE, xtpImageNormal);
			GridFrameBase::setImage({ ID_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE }, IDB_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

    CreateLinkGroups(tab, !m_driveItemsGrid.IsMyData() && (GetOrigin() == Origin::User || GetOrigin() == Origin::DeletedUser), GetOrigin() == Origin::Group);

	automationAddCommandIDToGreenLight(ID_DRIVEITEMSGRID_DELETEPERMISSIONS);
	
    return true;
}

O365Grid& FrameDriveItems::GetGrid()
{
	return m_driveItemsGrid;
}

void FrameDriveItems::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameDriveItems::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameDriveItems::ApplySpecific(bool p_Selected)
{
	CommandInfo info;
	info.Data() = m_driveItemsGrid.GetDriveChanges(p_Selected);
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	CommandDispatcher::GetInstance().Execute(
        Command(GetLicenseContext(),
            GetSafeHwnd(),
            Command::ModuleTarget::Drive,
            Command::ModuleTask::ApplyChanges,
            info,
            { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
}

void FrameDriveItems::RefreshSpecific(AutomationAction* p_CurrentAction)
{   
	Command::ModuleTask task = m_SearchCriteria ? Command::ModuleTask::SearchSubItems : Command::ModuleTask::ListSubItems;
    CommandInfo info;
    info.SetOrigin(GetModuleCriteria().m_Origin);
	if (ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer)
		info.GetIds() = GetModuleCriteria().m_IDs;
	else if (ModuleCriteria::UsedContainer::SUBIDS == GetModuleCriteria().m_UsedContainer)
		info.GetSubIds() = GetModuleCriteria().m_SubIDs;
	else
		ASSERT(false);
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	if (m_SearchCriteria)
		info.Data2() = m_SearchCriteria;

	if (HasLastUpdateOperations())
		GridUpdater::HandleCreatedModifications(GetGrid(), AccessLastUpdateOperations());

	try
	{
		auto& grid = dynamic_cast<GridDriveItems&>(GetGrid());
		grid.SetShowOwnerPermissions(false);
	}
	catch (std::bad_cast)
	{
		ASSERT(false);
	}

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Drive, task, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameDriveItems::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
	// FIXME: Bug #190610.MB.00A032: Bug when editing permissions
	// Issue on refreshing exploded data after update.
	// Then we implode before any refresh (grid update is not necessary though).
	if (p_RefreshData.m_SelectedRowsAncestors)
	{
		vector<GridBackendRow*> explodedRows;
		m_driveItemsGrid.GetMultivalueManager().GetTopSourceRowsAll(explodedRows);

		vector<GridBackendRow*> rowsToImplode;
		for (const auto& row : explodedRows)
		{
			if (p_RefreshData.m_SelectedRowsAncestors->end() != p_RefreshData.m_SelectedRowsAncestors->find(row->GetTopAncestorOrThis()))
				rowsToImplode.push_back(row);
		}

		if (!rowsToImplode.empty())
			m_driveItemsGrid.MultivalueImplode(rowsToImplode, false);
	}
	else
	{
		m_driveItemsGrid.MultiValuesImplodeAll(false);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////

	if (HasLastUpdateOperations())
		GridUpdater::HandleCreatedModifications(GetGrid(), AccessLastUpdateOperations());

    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	if (m_SearchCriteria)
		info.Data2() = m_SearchCriteria;
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Drive, m_SearchCriteria ? Command::ModuleTask::SearchSubItems : Command::ModuleTask::ListSubItems, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

AutomatedApp::AUTOMATIONSTATUS FrameDriveItems::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionSelectedLoadPermissions(p_Action))
	{
		p_CommandID = ID_DRIVEITEMSGRID_VIEWPERMISSIONS;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedDeletePermissions(p_Action))
	{
		p_CommandID = ID_DRIVEITEMSGRID_DELETEPERMISSIONS;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedDownload(p_Action))
	{
		p_CommandID = ID_DRIVEITEMSGRID_DOWNLOAD;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedLoadCheckoutAndRetention(p_Action))
	{
		p_CommandID = ID_DRIVEITEMSGRID_LOADCHECKOUTSTATUS;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}

	return statusRV;
}

void FrameDriveItems::revertSelectedImpl()
{
	// FIXME: With flag O365Grid::RevertFlags::ANY_LEVEL_REVERT_CHILDREN,
	// if a folder is selected, any modification on contained files/folders will be reverted too without notice.
	GetGrid().RevertSelectedRows(O365Grid::RevertFlags::UPDATE_GRID_IF_NEEDED | O365Grid::RevertFlags::ANY_LEVEL_REVERT_CHILDREN);
}

void FrameDriveItems::ViewPermissions(const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, bool p_RefreshUpdates)
{
    m_driveItemsGrid.ViewPermissions(p_Permissions, p_RefreshUpdates, false, false, true);
}

void FrameDriveItems::ViewPermissionsExplode(const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions)
{
	m_driveItemsGrid.ViewPermissions(p_Permissions, false, true, false, true);
}

void FrameDriveItems::ViewPermissionsExplodeFlat(const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions)
{
	m_driveItemsGrid.ViewPermissions(p_Permissions, false, true, true, true);
}

void FrameDriveItems::ViewCheckoutStatuses(const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses)
{
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
	m_driveItemsGrid.ViewCheckoutStatuses(p_CheckoutStatuses, true);
#else
	ASSERT(false);
#endif
}

bool FrameDriveItems::CloseFrame()
{
    if (downloadConfirmation())
		return GridFrameBase::CloseFrame();

    return false;
}

bool FrameDriveItems::CanBeClosedWithoutConfirmation()
{
	return GridFrameBase::CanBeClosedWithoutConfirmation() && m_driveItemsGrid.GetNbDownloadsOccuring() <= 0;
}

bool FrameDriveItems::HasApplyButton()
{
    return true;
}

bool FrameDriveItems::downloadConfirmation()
{
    bool close = true;
    if (m_driveItemsGrid.GetNbDownloadsOccuring() > 0)
    {
        YCodeJockMessageBox confirmation(
            this,
            DlgMessageBox::eIcon_Question,
            YtriaTranslate::Do(FrameDriveItems_downloadConfirmation_1, _YLOC("Downloads pending")).c_str(),
            YtriaTranslate::Do(FrameDriveItems_downloadConfirmation_2, _YLOC("One more more download(s) are not finished. Do you want to cancel them now?")).c_str(),
            L"",
            { { IDOK, YtriaTranslate::Do(FrameDriveItems_downloadConfirmation_3, _YLOC("Cancel Download(s)")).c_str() },{ IDCANCEL, YtriaTranslate::Do(FrameDriveItems_downloadConfirmation_4, _YLOC("Keep window open and wait for download(s) to finish")).c_str() } });

        switch (confirmation.DoModal())
        {
        case IDOK:
            m_TaskData.Cancel();
            // Close immediately. The download task will ensure the window is valid before attempting any operation on it
            break;
        case IDCANCEL:
            close = false;
            break;
        default:
            break;
        }
    }
    return close;
}

const std::map<PooledString, PooledString>& FrameDriveItems::GetChannelNames() const
{
	ASSERT(Origin::Channel == GetOrigin());
	return m_ChannelNames;
}

void FrameDriveItems::SetChannelNames(const std::map<PooledString, PooledString>& p_ChannelNames)
{
	ASSERT(Origin::Channel == GetOrigin());
	m_ChannelNames = p_ChannelNames;
}

void FrameDriveItems::SetDownloadProcessRunning(bool p_Running)
{
	m_DownloadRunning = p_Running;
}

void FrameDriveItems::SetSearchCriteria(const boost::YOpt<wstring>& p_SearchCriteria)
{
	m_SearchCriteria = p_SearchCriteria;
}

const boost::YOpt<wstring>& FrameDriveItems::GetSearchCriteria() const
{
	return m_SearchCriteria;
}

void FrameDriveItems::RefreshWithNewSearchCriteria(const wstring& p_SearchCriteria)
{
	// If module has not been initialized with Criteria this is forbidden.
	ASSERT(m_SearchCriteria);

	CommandInfo info;
	info.SetOrigin(GetModuleCriteria().m_Origin);
	if (ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer)
		info.GetIds() = GetModuleCriteria().m_IDs;
	else if (ModuleCriteria::UsedContainer::SUBIDS == GetModuleCriteria().m_UsedContainer)
		info.GetSubIds() = GetModuleCriteria().m_SubIDs;
	else
		ASSERT(false);
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	info.Data2() = boost::YOpt<wstring>(p_SearchCriteria);

	if (HasLastUpdateOperations())
		GridUpdater::HandleCreatedModifications(GetGrid(), AccessLastUpdateOperations());

	try
	{
		auto& grid = dynamic_cast<GridDriveItems&>(GetGrid());
		grid.SetShowOwnerPermissions(false);
	}
	catch (std::bad_cast)
	{
		ASSERT(false);
	}

	AutomationAction* action = nullptr; // FIXME!!!!!!
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Drive, Command::ModuleTask::SearchSubItems, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), action }));
}

bool FrameDriveItems::isDownloadProcessRunning() const
{
	return m_DownloadRunning;
}
