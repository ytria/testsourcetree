#pragma once

#include "IJsonSerializer.h"
#include "ObjectIdentity.h"

class ObjectIdentitySerializer : public IJsonSerializer
{
public:
	ObjectIdentitySerializer(const ObjectIdentity& p_ObjectIdentity);
	void Serialize() override;

private:
	ObjectIdentity m_ObjectIdentity;
};

