#include "UpdateApplicationRequester.h"
#include "Sapio365Session.h"
#include "ApplicationDeserializer.h"
#include "MsGraphHttpRequestLogger.h"

UpdateApplicationRequester::UpdateApplicationRequester(const wstring& p_AppId)
	:m_AppId(p_AppId)
{
}

TaskWrapper<void> UpdateApplicationRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<SingleRequestResult>();
	m_Deserializer = std::make_shared<ApplicationDeserializer>();

	web::uri_builder uri;
	uri.append_path(_YTEXT("applications"));
	uri.append_path(m_AppId);

	WebPayloadJSON payload(m_Values);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Patch(uri.to_uri(), httpLogger, p_TaskData, payload).ThenByTask([result = m_Result, deserializer = m_Deserializer](pplx::task<RestResultInfo> p_ResInfo) {
		try
		{
			result->SetResult(p_ResInfo.get());
			auto body = result->GetResult().GetBodyAsString();
			ASSERT(body.is_initialized());
			if (body.is_initialized())
				deserializer->Deserialize(web::json::value::parse(*body));
		}
		catch (const RestException& e)
		{
			RestResultInfo info(e.GetRequestResult());
			info.m_RestException = std::make_shared<RestException>(e);
			result->SetResult(info);
		}
		catch (const std::exception& e)
		{
			RestResultInfo info;
			info.m_Exception = e;
			result->SetResult(info);
		}
	});
}

void UpdateApplicationRequester::SetDisplayName(const wstring& p_DisplayName)
{
	m_Values[_YTEXT("displayName")] = json::value::string(p_DisplayName);
}
