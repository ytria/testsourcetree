#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Deleted.h"

class DeletedDeserializer : public JsonObjectDeserializer, public Encapsulate<Deleted>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

