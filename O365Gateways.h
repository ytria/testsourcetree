#pragma once
#include "UserGraphSessionCreator.h"
#include "AppGraphSessionCreator.h"

class MSGraphSession;
class GraphCache;

class O365Gateways
{
public:
    std::shared_ptr<MSGraphSession> GetUserGraphSession() const;
    std::shared_ptr<MSGraphSession> GetAppGraphSession() const;

    void SetUserGraphSession(const std::shared_ptr<MSGraphSession>& p_Session);
    void SetAppGraphSession(const std::shared_ptr<MSGraphSession>& p_Session);

    void SetTenant(const wstring& p_Tenant);
    void SetO365Credentials(const wstring& p_Username, const wstring& p_Password);
    void SetAppGraphSessionCredentials(const wstring& p_ClientId, const wstring& p_ClientSecret);

    const wstring& GetUltraAdminAppClientId() const;

    void CreateSessions(const wstring& p_AppRefName);
    void ClearSessions();

    bool AreGraphSessionsValid() const;

    const boost::YOpt<UserGraphSessionCreator>& GetLastUserSessionCreator() const;
    const boost::YOpt<AppGraphSessionCreator>& GetLastAppSessionCreator() const;

private:
    std::shared_ptr<MSGraphSession> m_MsGraphSessionUser;
    std::shared_ptr<MSGraphSession> m_MsGraphSessionApp;

    boost::YOpt<UserGraphSessionCreator> m_UserSessionCreator;
    boost::YOpt<AppGraphSessionCreator> m_AppSessionCreator;

    wstring m_O365Username;
    wstring m_O365Password;

    wstring m_Tenant;
    wstring m_UltraAdminSessionClientId;
    wstring m_UltraAdminSessionClientSecret;

    bool m_AreGraphSessionsValid = false;    
};

