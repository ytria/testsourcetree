#pragma once

#include "ISubItemDeletion.h"
#include "AttachmentInfo.h"

class GridEvents;

class EventAttachmentDeletion : public ISubItemDeletion
{
public:
    EventAttachmentDeletion(GridEvents& p_Grid, const AttachmentInfo& p_Info);

    virtual bool IsCorrespondingRow(GridBackendRow* p_Row) const override;

	virtual vector<ModificationLog> GetModificationLogs() const override;

	GridBackendRow* GetMainRowIfCollapsed() const override;

private:
    GridEvents& m_GridEvents;

    AttachmentInfo m_AttachmentInfo;
};