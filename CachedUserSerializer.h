#pragma once

#include "IJsonSerializer.h"
#include "CachedUser.h"

class CachedUserSerializer : public IJsonSerializer
{
public:
    CachedUserSerializer(const CachedUser& p_CachedUser);
    void Serialize() override;

private:
    const CachedUser& m_CachedUser;
};

