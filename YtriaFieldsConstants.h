#pragma once

#define YTRIA_LASTREQUESTEDDATETIME					"ytriaLastRequestedDateTime"
#define YTRIA_FLAGS									"ytriaFlags"
#define YTRIA_ERROR									"ytriaError"
#define YTRIA_DRIVE									"ytriaDrive"

#define YTRIA_BUSINESSASSIGNEDLICENSE_SKUPARTNUMBER	"ytriaBusinessAssignedLicenseSkuPartNumber"

#define YTRIA_CACHEDUSER_DISPLAYNAME				"ytriaCachedUserDisplayName"
#define YTRIA_CACHEDUSER_USERPRINCIPALNAME			"ytriaCachedUserPrincipalName"
#define YTRIA_CACHEDUSER_USERTYPE					"ytriaCachedUserType"
#define YTRIA_CACHEDUSER_DELETEDDATETIME			"ytriaCachedUserDeletedDateTime"

#define YTRIA_USER_ARCHIVEFOLDERNAME				"ytriaUserArchiveFolderName"
#define YTRIA_USER_ARCHIVEFOLDERNAMEERROR			"ytriaUserArchiveFolderNameError"
#define YTRIA_USER_MAILBOXSETTINGSERROR				"ytriaUserMailboxSettingsError"
#define YTRIA_USER_MANAGER							"ytriaUserManager"

#define YTRIA_GROUP_HASTEAMINFO						"ytriaGroupHasTeamInfo"
#define YTRIA_GROUP_LCPSTATUS						"ytriaGroupLcpStatus"
#define YTRIA_GROUP_MEMBERSCOUNT					"ytriaGroupMembersCount"
#define YTRIA_GROUP_CREATEDONBEHALFOF				"ytriaGroupCreatedOnBehalfOf"
#define YTRIA_GROUP_TEAM							"ytriaGroupTeam"
#define YTRIA_GROUP_OWNERS							"ytriaGroupOwners"
#define YTRIA_GROUP_GROUPUNIFIEDGUESTSETTINGACTIVATED "settingGroupUnifiedGuestActivated"
#define YTRIA_GROUP_SETTINGALLOWTOADDGUESTS         "settingAllowToAddGuests"
#define YTRIA_GROUP_SETTINGALLOWTOADDGUESTSID       "settingAllowToAddGuestsId"
#define YTRIA_GROUP_ISYAMMER						"isYammer"

#define YTRIA_USER_MAILBOX_DELIVERTOMAILBOXANDFORWARD "ytriaUserMailboxDeliverToMailboxAndForward"
#define YTRIA_USER_MAILBOX_FORWARDINGSMTPADDRESS	"ytriaUserMailboForwardingSmtpAddress"
#define YTRIA_USER_MAILBOX_TYPE						"ytriaUserMailboxType"
#define YTRIA_USER_MAILBOX_FORWARDINGADDRESS		"ytriaUserMailboxForwardingAddress"

#define YTRIA_USER_MAILBOX_CAN_SEND_ON_BEHALF		"ytriaUserMailboxCanSendOnBehalf"
#define YTRIA_USER_MAILBOX_CAN_SEND_AS_USER			"ytriaUserMailboxCanSendAsUser"

#define YTRIA_GROUP_TEAM_ERROR						"ytriaGroupTeamError"
#define YTRIA_GROUP_GROUPSETTINGS_ERROR				"ytriaGroupSettingsError"
#define YTRIA_GROUP_USERCREATEDONBEHALFOF_ERROR		"ytriaUserCreatedOnBehalfOfError"
#define YTRIA_GROUP_OWNERS_ERROR					"ytriaOwnersError"
#define YTRIA_GROUP_LCPSTATUS_ERROR					"ytriaGroupLcpStatusError"
#define YTRIA_GROUP_ISYAMMER_ERROR					"ytriaGroupIsYammerError"
#define YTRIA_USER_MANAGER_ERROR					"ytriaUserManagerError"
#define YTRIA_DRIVE_ERROR							"ytriaDriveError"
#define YTRIA_USER_MAILBOX_ERROR					"ytriaUserMailboxError"
#define YTRIA_USER_MAILBOX_RECIPIENTPERMISSIONS_ERROR	"ytriaUserMailboxRecipientPermissionsError"

#define YTRIA_SITE_SUBSITES							"ytriaSiteSubsites"
