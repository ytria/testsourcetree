#include "GenericSaveSelectedButtonUpdater.h"

#include "BaseO365Grid.h"
#include "GridFrameBase.h"

GenericSaveSelectedButtonUpdater::GenericSaveSelectedButtonUpdater(GridFrameBase& p_Frame)
	: m_Frame(p_Frame)
{
}

void GenericSaveSelectedButtonUpdater::Update(CCmdUI* p_CmdUi)
{
	p_CmdUi->Enable(m_Frame.IsConnected() && m_Frame.HasNoTaskRunning() && m_Frame.hasModificationsPending(true));
}
