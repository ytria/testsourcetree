#pragma once

#include "IGenericEditablePropertiesGrid.h"
#include "CacheGridTools.h"
#include "DlgGenPropModifHTML.h"

template<class CacheGridClass, class BusinessObjectClass>
void IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::CreateObject()
{
	vector<BusinessObjectClass> bos{ {} };

	const auto res = showDialog(bos, {}, DlgFormsHTML::Action::CREATE, this);
	if (IDOK == res)
		AddCreatedBusinessObjects(bos, true);
}

template<class CacheGridClass, class BusinessObjectClass>
void IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::EditObjects()
{
	TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool /*p_HasRowsToReExplode*/)
	{
		std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, GetPrivilegeEdit(), [this](GridBackendRow* p_Row)
		{
			auto editableRow = isRegisteredType(p_Row) ? p_Row : (GetEditParentWhenChildSelected() ? GridUtil::getParentOfBusinessType<BusinessObjectClass>(p_Row, true) : nullptr);
			return nullptr != editableRow && !editableRow->IsGroupRow() && isRegisteredType(editableRow) && canEdit(editableRow);
		});

		ASSERT(!selectedRows.empty());
		if (!selectedRows.empty())
		{
			bool isEditCreate = true;
			vector<BusinessObjectClass> businessObjects;
			std::set<wstring> fieldErrors;

			for (auto row : selectedRows)
			{
				auto objectRow = GetEditParentWhenChildSelected() ? GridUtil::getParentOfBusinessType<BusinessObjectClass>(row, true) : (/*GridUtil::isBusinessType<BusinessObjectClass>(row)*/isRegisteredType(row) ? row : nullptr);
                if (nullptr != objectRow && canEdit(objectRow))
				{
					businessObjects.emplace_back(getBusinessObject(objectRow));
					isEditCreate = isEditCreate && (objectRow->IsCreated() || businessObjects.back().HasFlag(BusinessObject::CREATED));

					getFieldsThatHaveErrors(objectRow, fieldErrors);
				}
			}

			const auto res = showDialog(businessObjects, fieldErrors, isEditCreate ? DlgFormsHTML::Action::EDIT_CREATE : DlgFormsHTML::Action::EDIT, this);
			if (IDOK == res)
			{
				if (isEditCreate)
					AddCreatedBusinessObjects(businessObjects, false);
				else
					UpdateBusinessObjects(businessObjects, true);
			}
		}
	})();
}

template<class CacheGridClass, class BusinessObjectClass>
bool IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::selectionHasRegisteredType(uint32_t rowFlagsToExclude)
{
    bool isRegistered = false;

    for (auto row : GetSelectedRows())
    {
        if (nullptr != row && !row->IsGroupRow() && isRegisteredType(row) && 0 == (rowFlagsToExclude & row->GetFlags()))
        {
            isRegistered = true;
            break;
        }
    }

    return isRegistered;
}

template<class CacheGridClass, class BusinessObjectClass>
bool IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::isRegisteredType(GridBackendRow* p_Row)
{
    bool isRegistered = false;

    ASSERT(nullptr != p_Row);
    if (nullptr != p_Row)
    {
        const bool rowValid = nullptr != p_Row && nullptr != p_Row->GetBackend() && nullptr != p_Row->GetBackend()->GetCacheGrid();
        ASSERT(rowValid);
        if (rowValid && m_RegisteredBusinessTypes.find(GridUtil::getBusinessTypeId(p_Row)) != m_RegisteredBusinessTypes.end())
            isRegistered = true;
    }

    return isRegistered;
}


template<class CacheGridClass, class BusinessObjectClass>
void IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::ViewObjects()
{
	vector<BusinessObjectClass> bos;
	for (auto row : GetSelectedRows())
	{
		if (!row->IsGroupRow())
			bos.emplace_back(getBusinessObject(row));
	}

	if (!bos.empty())
		const auto res = showDialog(bos, {}, DlgFormsHTML::Action::READ, this);
}


template<class CacheGridClass, class BusinessObjectClass>
void IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::ShowModificationDialog(DlgFormsHTML::Action p_Action)
{
	switch (p_Action)
	{
	case DlgFormsHTML::Action::CREATE:
		CreateObject();
		break;
	case DlgFormsHTML::Action::EDIT:
		EditObjects();
		break;
	case DlgFormsHTML::Action::READ:
		ViewObjects();
		break;
	default:
		break;
	}
}

template<class CacheGridClass, class BusinessObjectClass>
bool IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::canEdit(GridBackendRow*) const
{
	// Override this method if you want to add restrictions on some objects.
	return true;
}

template<class CacheGridClass, class BusinessObjectClass>
bool IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::canDelete(GridBackendRow*) const
{
	// Override this method if you want to add restrictions on some objects.
	return true;
}

template<class CacheGridClass, class BusinessObjectClass>
void IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::getFieldsThatHaveErrors(GridBackendRow*, std::set<wstring>&) const
{
	// Override this method if you want to fill the list with field that contain errors.
}

template<class CacheGridClass, class BusinessObjectClass>
void IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::AddCreatedBusinessObjects(const vector<BusinessObjectClass>&, bool p_ScrollToNew)
{
	// Override it if you need it!
	ASSERT(false);
}

template<class CacheGridClass, class BusinessObjectClass>
void IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::UpdateBusinessObjects(const vector<BusinessObjectClass>&, bool p_SetModifiedStatus)
{
	// Override it if you need it!
	ASSERT(false);
}

template<class CacheGridClass, class BusinessObjectClass>
void IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::RemoveBusinessObjects(const vector<BusinessObjectClass>&)
{
	// Override it if you need it!
	ASSERT(false);
}

template<class CacheGridClass, class BusinessObjectClass>
INT_PTR IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::showDialog(vector<BusinessObjectClass>& p_Objects, const std::set<wstring>& p_ListFieldErrors, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
{
	ASSERT(false); // The following dialog is not relevant anymore.
	return DlgGenPropModifHTML<BusinessObjectClass>(p_Objects, p_Action, true, p_Parent).DoModal();
}

template<class CacheGridClass, class BusinessObjectClass>
void IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::SetEditParentWhenChildSelected(bool p_Enable)
{
	// If we want both things, we need to fix EditObjects()
	ASSERT(m_RegisteredBusinessTypes.empty() || !p_Enable);

	m_EditParentWhenChildSelected = p_Enable;
}

template<class CacheGridClass, class BusinessObjectClass>
bool IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::GetEditParentWhenChildSelected() const
{
	return m_EditParentWhenChildSelected;
}

template<class CacheGridClass, class BusinessObjectClass>
RoleDelegationUtil::RBAC_Privilege IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::GetPrivilegeEdit() const
{
	ASSERT(false);
	return RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;// set it in your own grid
}

template<class CacheGridClass, class BusinessObjectClass>
RoleDelegationUtil::RBAC_Privilege IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::GetPrivilegeCreate() const
{
	ASSERT(false);
	return RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;// set it in your own grid
}

template<class CacheGridClass, class BusinessObjectClass>
RoleDelegationUtil::RBAC_Privilege IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::GetPrivilegeDelete() const
{
	ASSERT(false);
	return RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;// set it in your own grid
}