#include "FrameOnPremiseUsers.h"
#include "UpdatedObjectsGenerator.h"

IMPLEMENT_DYNAMIC(FrameOnPremiseUsers, GridFrameBase)

FrameOnPremiseUsers::FrameOnPremiseUsers(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, p_Module, p_HistoryMode, p_PreviousFrame)
{
	m_CreateAddRemoveToSelection = !p_IsMyData;
	m_CreateRefreshPartial = false;
	m_CreateShowContext = !p_IsMyData;
}

void FrameOnPremiseUsers::ShowOnPremiseUsers(const vector<OnPremiseUser>& p_OnPremiseUsers, bool p_FullPurge)
{
	m_OnPremiseUsersGrid.BuildView(p_OnPremiseUsers, p_FullPurge);
}

void FrameOnPremiseUsers::ApplyAllSpecific()
{

}

void FrameOnPremiseUsers::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	auto updatedObjectsGen = UpdatedObjectsGenerator<OnPremiseUser>();

	CommandInfo info;
	info.Data() = updatedObjectsGen;
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::OnPremiseUsers, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameOnPremiseUsers::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.Data() = p_RefreshData;
	info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::OnPremiseUsers, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

O365Grid& FrameOnPremiseUsers::GetGrid()
{
	return m_OnPremiseUsersGrid;
}

void FrameOnPremiseUsers::createGrid()
{
	m_OnPremiseUsersGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameOnPremiseUsers::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigUsers();
}


AutomatedApp::AUTOMATIONSTATUS FrameOnPremiseUsers::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;
	return statusRV;
}
