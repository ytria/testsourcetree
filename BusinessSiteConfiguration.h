#pragma once

#include "BusinessConfigurationBase.h"

class BusinessSiteConfiguration : public BusinessConfigurationBase
{
public:
	DECLARE_GET_INSTANCE(BusinessSiteConfiguration)

private:
	BusinessSiteConfiguration();
};
