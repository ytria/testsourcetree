#include "MessageInMailfolderPropertySet.h"

vector<rttr::property> MessageInMailfolderPropertySet::GetPropertySet() const
{
	ASSERT(false);
	return {};
}

vector<PooledString> MessageInMailfolderPropertySet::GetStringPropertySet() const
{
	return {
		_YTEXT("id"),
		_YTEXT("parentFolderId"),
		_YTEXT("subject"),
		_YTEXT("sentDateTime"),
		_YTEXT("receivedDateTime"),
		_YTEXT("from"),
		_YTEXT("sender"),
		_YTEXT("isRead"),
	};
}
