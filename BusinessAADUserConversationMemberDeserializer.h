#pragma once

#include "BusinessAADUserConversationMember.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class BusinessAADUserConversationMemberDeserializer : public JsonObjectDeserializer
													, public Encapsulate<BusinessAADUserConversationMember>
{
public:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};