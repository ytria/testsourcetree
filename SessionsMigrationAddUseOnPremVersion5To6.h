#pragma once

#include "ISessionsMigrationVersion.h"

class SessionsMigrationAddUseOnPremVersion5To6 : public ISessionsMigrationVersion
{
public:
	void Build() override;
	VersionSourceTarget GetVersionInfo() const override;

private:
	bool CreateNewFromOld();
	bool AddUseOnPremFlag();
};

