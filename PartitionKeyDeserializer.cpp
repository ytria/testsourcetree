#include "PartitionKeyDeserializer.h"

#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"

void Cosmos::PartitionKeyDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        ListStringDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("paths"), p_Object))
            m_Data.Paths = std::move(deserializer.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("kind"), m_Data.Kind, p_Object);
}

