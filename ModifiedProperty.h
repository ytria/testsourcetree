#pragma once

class ModifiedProperty
{
public:
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<PooledString> m_NewValue;
	boost::YOpt<PooledString> m_OldValue;
};

