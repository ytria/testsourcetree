#pragma once

#include "ISessionLoader.h"

class ElevatedSessionLoader : public ISessionLoader
{
public:
	ElevatedSessionLoader(const SessionIdentifier& p_Id);

protected:
	ElevatedSessionLoader(const SessionIdentifier& p_Id, bool p_Partner);

protected:
	TaskWrapper<std::shared_ptr<Sapio365Session>> Load(const std::shared_ptr<Sapio365Session>& p_SapioSession) override;

private:
	const bool m_Partner;
};

