#include "FlatObjectListFrameRefresher.h"
#include "GridUpdater.h"
#include "BaseO365Grid.h"

void FlatObjectListFrameRefresher::operator()(GridFrameBase& p_Frame, Command::ModuleTarget p_ModuleTarget, Origin p_DetailOrigin, AutomationAction* p_CurrentAction, std::function<void(const vector<O365UpdateOperation>&, bool, bool)> p_DirectGridUpdate)
{
	CommandInfo info;
	info.SetFrame(&p_Frame);
	info.SetRBACPrivilege(p_Frame.GetModuleCriteria().m_Privilege);
	if (p_Frame.GetOptions() && !p_Frame.GetOptions()->m_CustomFilter.IsEmpty())
		info.Data2() = *p_Frame.GetOptions();

	if (p_Frame.HasLastUpdateOperations())
		GridUpdater::HandleCreatedModifications(p_Frame.GetGrid(), p_Frame.AccessLastUpdateOperations());

	ASSERT(p_Frame.GetModuleCriteria().m_UsedContainer == ModuleCriteria::UsedContainer::IDS);
	const auto isUserOrGroupDetails = p_Frame.GetModuleCriteria().m_Origin == Origin::User || p_Frame.GetModuleCriteria().m_Origin == Origin::Group;
	if (p_Frame.HasLastUpdateOperations() && !p_Frame.ShouldForceFullRefreshAfterApply()) // Refresh after update
	{
		auto& ids = info.GetIds();
		ids.clear();

		for (const auto& updateOp : p_Frame.GetLastUpdateOperations())
		{
			auto id = updateOp.GetPkFieldStr(p_Frame.GetGrid().GetColId());
			if (!O365Grid::IsTemporaryCreatedObjectID(id))
			{
				bool successDeletion = false;
				for (const auto& result : updateOp.GetResults())
				{
					if (result.m_ResultInfo)
					{
						if (web::http::methods::DEL == result.m_ResultInfo->Request.method()
							&& 204 == result.m_ResultInfo->Response.status_code())
						{
							// Check a corresponding DeletedObjectModification object exists.
							// As in Users module, web::http::methods::DEL is also used for User's Manager removal.
							if (nullptr != p_Frame.GetGrid().GetModifications().GetRowModification<DeletedObjectModification>(updateOp.GetPrimaryKey()))
							{
								successDeletion = true;
								break;
							}
						}
					}
				}

				// FIXME: Should we better put it in the list, do the request, and interpret the 404 error as the deletion confirmation?
				if (!successDeletion && (!isUserOrGroupDetails || p_Frame.GetModuleCriteria().m_IDs.end() != p_Frame.GetModuleCriteria().m_IDs.find(id)))
					ids.insert(id);
			}
		}

		if (ids.empty())
		{
			// Update operations only contain failed creations or success deletions.
			// No request.
			// Directly trigger a grid update.
			p_DirectGridUpdate(p_Frame.GetLastUpdateOperations(), true, false);
			p_Frame.ForgetLastUpdateOperations();
			p_Frame.setAutomationGreenLight(p_CurrentAction);
			return;
		}
	}
	else if (isUserOrGroupDetails)
	{
		info.GetIds() = p_Frame.GetModuleCriteria().m_IDs;
	}

	CommandDispatcher::GetInstance().Execute(Command(p_Frame.GetLicenseContext(), p_Frame.GetSafeHwnd(), p_ModuleTarget, Command::ModuleTask::List, info, { &p_Frame.GetGrid(), g_ActionNameRefreshAll, p_Frame.GetAutomationActionRecording(), p_CurrentAction }));
}
