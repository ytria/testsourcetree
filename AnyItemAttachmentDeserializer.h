#pragma once

#include "JsonObjectDeserializer.h"
#include "AnyItemAttachment.h"
#include "Encapsulate.h"

class AnyItemAttachmentDeserializer : public JsonObjectDeserializer, public Encapsulate<AnyItemAttachment>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

