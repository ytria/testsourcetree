#include "DirectoryObjectVariantDeserializer.h"

#include "DirectoryRoleDeserializer.h"
#include "GroupDeserializer.h"
#include "JsonSerializeUtil.h"
#include "OrgContactDeserializer.h"
#include "SqlCacheConfig.h"
#include "UserDeserializer.h"

DirectoryObjectVariantDeserializer::DirectoryObjectVariantDeserializer(std::shared_ptr<Sapio365Session> p_Session)
	: m_Session(p_Session)
{

}

#ifdef _DEBUG
DirectoryObjectVariantDeserializer::DirectoryObjectVariantDeserializer(std::shared_ptr<Sapio365Session> p_Session, bool p_NoSelectUsed)
	: DirectoryObjectVariantDeserializer(p_Session)
{
	m_NoSelectUsed = p_NoSelectUsed;
}
#endif

void DirectoryObjectVariantDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	m_Data.SetLastRequestTime(YTimeDate::GetCurrentTimeDate());

	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);

	boost::YOpt<PooledString> dataType;
	JsonSerializeUtil::DeserializeString(_YTEXT("@odata.type"), dataType, p_Object);

	if (dataType)
	{
		if (*dataType == _YTEXT("#microsoft.graph.group"))
		{
#ifdef _DEBUG
			GroupDeserializer gd(m_Session, m_NoSelectUsed);
#else
			GroupDeserializer gd(m_Session);
#endif
			gd.SetDate(GetDate());
			gd.DeserializeObject(p_Object);
			m_Data.m_BusinessGroup.emplace(std::move(gd.GetData()));
		}
		else if (*dataType == _YTEXT("#microsoft.graph.orgContact"))
		{
			OrgContactDeserializer ocd;
			ocd.SetDate(GetDate());
			ocd.DeserializeObject(p_Object);
			m_Data.m_BusinessOrgContact.emplace(std::move(ocd.GetData()));
		}
		else if (*dataType == _YTEXT("#microsoft.graph.user"))
		{
#ifdef _DEBUG
			UserDeserializer ud(m_Session, m_NoSelectUsed);
#else
			UserDeserializer ud(m_Session);
#endif
			ud.SetDate(GetDate());
			ud.DeserializeObject(p_Object);
			ud.GetData().SetDataDate(UBI::MIN, m_Data.GetLastRequestTime());
			m_Data.m_BusinessUser.emplace(std::move(ud.GetData()));
		}
		else if (*dataType == _YTEXT("#microsoft.graph.directoryRole"))
		{
			DirectoryRoleDeserializer drd;
			drd.SetDate(GetDate());
			drd.DeserializeObject(p_Object);
			m_Data.m_BusinessDirectoryRole.emplace(std::move(drd.GetData()));
		}
		else
		{
			// Handle new type?
			ASSERT(false);
		}
	}
}
