#pragma once

#include "IRequester.h"
#include "HttpResultWithError.h"

class UploadMessageAttachmentRequester : public IRequester
{
public:
	UploadMessageAttachmentRequester(const wstring& p_FilePath, const wstring& p_MessageId);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const HttpResultWithError& GetResult() const;
	void UseMainMSGraphSession();

private:
	wstring m_FilePath;
	const wstring m_MessageId;

	std::shared_ptr<HttpResultWithError> m_Result;

	bool m_UseMainGraphSession;
};

