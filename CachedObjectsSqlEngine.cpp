#include "CachedObjectsSqlEngine.h"
#include "DataCollectionSqlQueryRowHandler.h"
#include "PersistentCacheUtil.h"
#include "Registry.h"
#include "SQLiteUtil.h"
#include "SqlQuerySelect.h"
#include "Str.h"
#include "TimeUtil.h"

const wstring CachedObjectsSqlEngine::g_TableMetadata = _YTEXT("Metadata");
const wstring CachedObjectsSqlEngine::g_TableUsers = _YTEXT("Users");
const wstring CachedObjectsSqlEngine::g_TableGroups = _YTEXT("Groups");

const wstring CachedObjectsSqlEngine::g_ColId = _YTEXT("id");
const wstring CachedObjectsSqlEngine::g_ColValue = _YTEXT("value");

const wstring CachedObjectsSqlEngine::g_ColMetadataName = _YTEXT("name");
const wstring CachedObjectsSqlEngine::g_ColMetadataFullListSize = _YTEXT("fullListSize");
const wstring CachedObjectsSqlEngine::g_ColMetadataDate = _YTEXT("fullRefreshDate");
const wstring CachedObjectsSqlEngine::g_ColVersion = _YTEXT("version");
const wstring CachedObjectsSqlEngine::g_ColLastDelta = _YTEXT("lastDelta");

CachedObjectsSqlEngine::CachedObjectsSqlEngine(const SessionIdentifier& p_SessionId)
    : O365SQLiteEngine(PersistentCacheUtil::GetFilenameFromSessionAndMigrateIfNecessary(p_SessionId, g_Version))
	, m_Identifier(p_SessionId)
	, m_SqlStatementGetMetadata(nullptr)
	, m_SqlStatementSetMetadata(nullptr)
	, m_SqlStatementDeleteMetadata(nullptr)
{
}

CachedObjectsSqlEngine::~CachedObjectsSqlEngine()
{
	sqlite3_finalize(m_SqlStatementGetMetadata);
	sqlite3_finalize(m_SqlStatementSetMetadata);
	sqlite3_finalize(m_SqlStatementDeleteMetadata);

	for (const auto& item : m_UpsertStatements)
	{
		for (const auto& stmt : item.second)
			sqlite3_finalize(stmt.second);
	}

	for (const auto& stmt : m_DeleteStatements)
		sqlite3_finalize(stmt.second);
}

void CachedObjectsSqlEngine::Init(const std::vector<wstring>& p_UserDataColumns, const std::vector<wstring>& p_GroupDataColumns)
{
	// Init tables
	{
		m_TableMetadata.m_Name = g_TableMetadata;
		{
			SQLiteUtil::Column colTableName;
			colTableName.m_Name = g_ColMetadataName;
			colTableName.m_Flags = SQLiteUtil::PK;
			colTableName.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;

			SQLiteUtil::Column colIsFull;
			colIsFull.m_Name = g_ColMetadataFullListSize;
			colIsFull.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;

			SQLiteUtil::Column colFullRefreshDate;
			colFullRefreshDate.m_Name = g_ColMetadataDate;
			colFullRefreshDate.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;

			SQLiteUtil::Column colVersion;
			colVersion.m_Name = g_ColVersion;
			colVersion.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;

			SQLiteUtil::Column colLastDelta;
			colLastDelta.m_Name = g_ColLastDelta;
			colLastDelta.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;

			m_TableMetadata.AddColumn(colTableName);
			m_TableMetadata.AddColumn(colIsFull);
			m_TableMetadata.AddColumn(colFullRefreshDate);
			m_TableMetadata.AddColumn(colVersion);
			m_TableMetadata.AddColumn(colLastDelta);
		}

		m_TableUsers.m_Name = g_TableUsers;
		addColsToTable(m_TableUsers, p_UserDataColumns);
		m_TableGroups.m_Name = g_TableGroups;
		addColsToTable(m_TableGroups, p_GroupDataColumns);

		createTables(GetTables());
	}

	// Prepare metadata statements.
	{
		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXTFORMAT(L"SELECT * FROM %s WHERE %s=@NAM", g_TableMetadata.c_str(), g_ColMetadataName.c_str()));
			const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementGetMetadata, nullptr);
			ASSERT(res == SQLITE_OK);
		}

		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXTFORMAT(L"INSERT OR REPLACE INTO %s(%s, %s, %s, %s, %s) VALUES(@NAM, @FUL, @DAT, @VER, @DEL)", g_TableMetadata.c_str(), g_ColMetadataName.c_str(), g_ColMetadataFullListSize.c_str(), g_ColMetadataDate.c_str(), g_ColVersion.c_str(), g_ColLastDelta.c_str()));
			const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementSetMetadata, nullptr);
			ASSERT(res == SQLITE_OK);
		}

		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXTFORMAT(L"DELETE FROM %s WHERE %s=@NAM", g_TableMetadata.c_str(), g_ColMetadataName.c_str()));
			const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementDeleteMetadata, nullptr);
			ASSERT(res == SQLITE_OK);
		}
	}
}

bool CachedObjectsSqlEngine::IsForSession(const SessionIdentifier& p_SessionId) const
{
	return m_Identifier == p_SessionId;
}

wstring CachedObjectsSqlEngine::getUpsertQuery(const wstring& p_TableName, const wstring& p_DataCol)
{
    return wstring(_YTEXT("INSERT INTO \"") + p_TableName + _YTEXT("\"(") + g_ColId + _YTEXT(",") + p_DataCol +
        _YTEXT(") VALUES(@ID,@VAL) ON CONFLICT(") + g_ColId + _YTEXT(") DO UPDATE SET ") + p_DataCol +
        _YTEXT("=excluded.") + p_DataCol);
}

wstring CachedObjectsSqlEngine::getDeleteQuery(const wstring& p_TableName)
{
	return _YTEXTFORMAT(L"DELETE FROM %s WHERE %s=@ID", p_TableName.c_str(), g_ColId.c_str());
}

sqlite3_stmt* CachedObjectsSqlEngine::getUpsertStatement(const wstring& p_TableName, const wstring& p_DataCol)
{
	auto& stmt = m_UpsertStatements[p_TableName][p_DataCol];
	if (nullptr == stmt)
	{
		const auto query = getUpsertQuery(p_TableName, p_DataCol);
		const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(query);
		const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &stmt, nullptr);
		ASSERT(res == SQLITE_OK);
	}
	ASSERT(nullptr != stmt);
	return stmt;
}

sqlite3_stmt* CachedObjectsSqlEngine::getDeleteStatement(const wstring& p_TableName)
{
	auto& stmt = m_DeleteStatements[p_TableName];
	if (nullptr == stmt)
	{
		const auto query = getDeleteQuery(p_TableName);
		const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(query);
		const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &stmt, nullptr);
		ASSERT(res == SQLITE_OK);
	}
	ASSERT(nullptr != stmt);
	return stmt;
}

void CachedObjectsSqlEngine::SaveUser(const PooledString& p_Id, const std::map<wstring, wstring>& p_ObjectBody)
{
	for (const auto& bod : p_ObjectBody)
		save(getUpsertStatement(m_TableUsers.m_Name, bod.first), p_Id, bod.second);
}

void CachedObjectsSqlEngine::SaveUser(const SQLCachedObject& p_Obj)
{
	SaveUser(p_Obj.m_Id, p_Obj.m_ObjectBody);
}

void CachedObjectsSqlEngine::SaveGroup(const PooledString& p_Id, const std::map<wstring, wstring>& p_ObjectBody)
{
	for (const auto& bod : p_ObjectBody)
		save(getUpsertStatement(m_TableGroups.m_Name, bod.first), p_Id, bod.second);
}

void CachedObjectsSqlEngine::SaveGroup(const SQLCachedObject& p_Obj)
{
	SaveGroup(p_Obj.m_Id, p_Obj.m_ObjectBody);
}

void CachedObjectsSqlEngine::DeleteUser(const PooledString& p_Id)
{
	deleteEntry(getDeleteStatement(m_TableUsers.m_Name), p_Id);
}

void CachedObjectsSqlEngine::DeleteGroup(const PooledString& p_Id)
{
	deleteEntry(getDeleteStatement(m_TableGroups.m_Name), p_Id);
}

int CachedObjectsSqlEngine::CountUsersMissingRequirements(const std::vector<wstring>& p_RequiredDataColumns) const
{
	return countObjectsMissingRequirements(g_TableUsers, p_RequiredDataColumns);
}

int CachedObjectsSqlEngine::CountGroupsMissingRequirements(const std::vector<wstring>& p_RequiredDataColumns) const
{
	return countObjectsMissingRequirements(g_TableGroups, p_RequiredDataColumns);
}

int CachedObjectsSqlEngine::GetRowCount(const wstring& p_TableName) const
{
	int count = 0;

	const auto countQuery = _YTEXTFORMAT(L"SELECT COUNT(*) FROM \"%s\"", p_TableName.c_str());
	const auto countQueryAscii = MFCUtil::convertUNICODE_to_ASCII(countQuery);
	char* errMsg = nullptr;
	const auto result = sqlite3_exec(countQueryAscii.c_str(), countCallback, &count, &errMsg);

	ProcessError(result, errMsg, countQuery);

	return count;
}

void CachedObjectsSqlEngine::SaveUsersMetadata(const Metadata& p_Metadata)
{
	saveMetadata(m_TableUsers, p_Metadata);
}

void CachedObjectsSqlEngine::SaveGroupsMetadata(const Metadata& p_Metadata)
{
	saveMetadata(m_TableGroups, p_Metadata);
}

CachedObjectsSqlEngine::Metadata CachedObjectsSqlEngine::LoadUsersMetadata() const
{
	return loadMetadata(m_TableUsers);
}

CachedObjectsSqlEngine::Metadata CachedObjectsSqlEngine::LoadGroupsMetadata() const
{
	return loadMetadata(m_TableGroups);
}

vector<CachedObjectsSqlEngine::SQLCachedObject> CachedObjectsSqlEngine::LoadUsers(const O365IdsContainer& p_IDs, const std::vector<wstring>& p_RequiredDataColumns/* = {}*/)
{
    return load(m_TableUsers, p_IDs, p_RequiredDataColumns);
}

vector<CachedObjectsSqlEngine::SQLCachedObject> CachedObjectsSqlEngine::LoadGroups(const O365IdsContainer& p_IDs, const std::vector<wstring>& p_RequiredDataColumns/* = {}*/)
{
    return load(m_TableGroups, p_IDs, p_RequiredDataColumns);
}

vector<SQLiteUtil::Table> CachedObjectsSqlEngine::GetTables() const
{
    return { m_TableMetadata, m_TableUsers, m_TableGroups };
}

wstring CachedObjectsSqlEngine::Clear()
{
	wstring error;

	StartTransaction();

	for (auto& table : GetTables())
	{
		if (!clearTable(table))
		{
			auto err = sqlite3_errmsg(GetDbHandle());
			if (nullptr != err)
				error = Str::convertFromUTF8(err);
			break;
		}
	}

	if (error.empty())
		EndTransaction();
	else
		RollbackTransaction();

	return error;
}

bool CachedObjectsSqlEngine::ClearUsers()
{
	return clearTableAndMetadata(m_TableUsers);
}

bool CachedObjectsSqlEngine::ClearGroups()
{
	return clearTableAndMetadata(m_TableGroups);
}

bool CachedObjectsSqlEngine::UserTableHasLicensePlans() const
{
	auto userMetadata = LoadUsersMetadata();
	return userMetadata.m_Version && *userMetadata.m_Version >= 1;
}

vector<CachedObjectsSqlEngine::SQLCachedObject> CachedObjectsSqlEngine::load(const SQLiteUtil::Table& p_Table, const O365IdsContainer& p_IDs, const std::vector<wstring>& p_RequiredDataColumns)
{
	wstring query = _YTEXTFORMAT(L"SELECT * FROM \"%s\"", p_Table.m_Name.c_str());

	if (!p_IDs.empty())
	{
		query += _YTEXTFORMAT(L" WHERE %s IN(", g_ColId.c_str());

		for (auto it = p_IDs.cbegin(); it != p_IDs.cend(); ++it)
		{
			if (it != p_IDs.begin())
				query += _YTEXTFORMAT(L", \"%s\"", it->c_str());
			else
				query += _YTEXTFORMAT(L"\"%s\"", it->c_str());
		}

		query += _YTEXT(")");

		for (const auto& c : p_RequiredDataColumns)
			query += _YTEXTFORMAT(L" AND %s IS NOT NULL", c.c_str());
	}
	else if (!p_RequiredDataColumns.empty())
	{
		query += _YTEXT(" WHERE");
		for (const auto& c : p_RequiredDataColumns)
			query += _YTEXTFORMAT(L" %s IS NOT NULL AND", c.c_str());
		query.erase(query.size() - 4); // Erase last " AND".
	}

	const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(query);

    vector<SQLCachedObject> objects;
	objects.reserve(GetRowCount(p_Table.m_Name));

    // Query elements
    sqlite3_stmt* stmt = nullptr;
	const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &stmt, nullptr);
    ASSERT(res == SQLITE_OK);
    if (res == SQLITE_OK)
    {
		while (this->sqlite3_step(stmt) == SQLITE_ROW)
		{
			objects.emplace_back();
			auto& obj = objects.back();
			const auto numCol = sqlite3_column_count(stmt);

			obj.m_Id = Str::convertFromUTF8(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0)));
			for (int c = 1; c < numCol; ++c)
			{
				auto name = Str::convertFromUTF8(reinterpret_cast<const char*>(sqlite3_column_name(stmt, c)));
				auto textPtr = reinterpret_cast<const char*>(sqlite3_column_text(stmt, c));
				if (nullptr != textPtr)
					obj.m_ObjectBody[name] = Str::convertFromUTF8(textPtr);
			}
		}
    }

	objects.shrink_to_fit();

	sqlite3_finalize(stmt);

	ProcessError(res, nullptr, query);

    return objects;
}

void CachedObjectsSqlEngine::save(sqlite3_stmt* p_Statement, const PooledString& p_Id, const wstring& p_ObjectBody)
{
    const auto id = Str::convertToUTF8(p_Id);
    const auto objBody = Str::convertToUTF8(p_ObjectBody);

	auto status = sqlite3_bind_text(p_Statement, 1, id.c_str(), -1, SQLITE_STATIC);
    status = sqlite3_bind_text(p_Statement, 2, objBody.c_str(), -1, SQLITE_STATIC);
    status = this->sqlite3_step(p_Statement);

	ProcessError(status, nullptr, _YTEXT(""));

	sqlite3_reset(p_Statement);
}

void CachedObjectsSqlEngine::deleteEntry(sqlite3_stmt* p_Statement, const PooledString& p_Id)
{
	const auto id = Str::convertToUTF8(p_Id);

	auto status = sqlite3_bind_text(p_Statement, 1, id.c_str(), -1, SQLITE_STATIC);
	status = this->sqlite3_step(p_Statement);

	ProcessError(status, nullptr, _YTEXT(""));

	sqlite3_reset(p_Statement);
}

void CachedObjectsSqlEngine::addColsToTable(SQLiteUtil::Table& p_Table, const std::vector<wstring>& p_DataColumns)
{
    SQLiteUtil::Column colId;
    colId.m_Name = g_ColId;
    colId.m_Flags = SQLiteUtil::PK;
    colId.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;
    p_Table.AddColumn(colId);

	for (const auto c : p_DataColumns)
	{
		SQLiteUtil::Column colValue;
		colValue.m_Name = c;
		colValue.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;
		p_Table.AddColumn(colValue);
	}
}

CachedObjectsSqlEngine::Metadata CachedObjectsSqlEngine::loadMetadata(const SQLiteUtil::Table& p_Table) const
{
	Metadata metadata;

	const auto name = Str::convertToUTF8(p_Table.m_Name);
	auto status = sqlite3_bind_text(m_SqlStatementGetMetadata, 1, name.c_str(), -1, SQLITE_STATIC);
	ASSERT(status == SQLITE_OK);
	if (status == SQLITE_OK)
	{
		status = this->sqlite3_step(m_SqlStatementGetMetadata);
		if (SQLITE_ROW == status)
		{
			if (p_Table.m_Name == Str::convertFromUTF8(reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, 0))))
			{
				auto countString = Str::convertFromUTF8(reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, 1)));
				if (!countString.empty())
					metadata.m_FullListSize = MFCUtil::getDWORDFromString(countString);

				auto dateString = Str::convertFromUTF8(reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, 2)));
				if (!dateString.empty())
				{
					metadata.m_FullRefreshDate.emplace();
					if (!TimeUtil::GetInstance().ConvertTextToDate(dateString, *metadata.m_FullRefreshDate))
					{
						ASSERT(false);
						metadata.m_FullRefreshDate.reset();
					}
				}

				auto versionString = Str::convertFromUTF8(reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, 3)));
				if (!versionString.empty())
					metadata.m_Version = MFCUtil::getDWORDFromString(versionString);

				auto lastDeltaString = Str::convertFromUTF8(reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, 4)));
				if (!lastDeltaString.empty())
					metadata.m_LastDelta = lastDeltaString;

				ASSERT(metadata.m_FullListSize && metadata.m_FullRefreshDate
					|| !metadata.m_FullListSize && !metadata.m_FullRefreshDate);
			}
			else
			{
				ASSERT(false);
			}
		}

		ProcessError(status, nullptr, _YTEXT(""));

		ASSERT(SQLITE_DONE == this->sqlite3_step(m_SqlStatementGetMetadata));
	}
	else
	{
		ProcessError(status, nullptr, _YTEXT(""));
	}

	sqlite3_reset(m_SqlStatementGetMetadata);

	return metadata;
}

void CachedObjectsSqlEngine::saveMetadata(const SQLiteUtil::Table& p_Table, const Metadata& p_Metadata)
{
	ASSERT(!p_Metadata.m_Version); // Current g_Version will be used anyway.

	const auto name = Str::convertToUTF8(p_Table.m_Name);
	const auto fullSize = p_Metadata.m_FullListSize ? Str::convertToUTF8(Str::getStringFromNumber(*p_Metadata.m_FullListSize)) : std::string();
	const auto date = p_Metadata.m_FullRefreshDate ? Str::convertToUTF8(TimeUtil::GetInstance().GetISO8601String(*p_Metadata.m_FullRefreshDate)) : std::string();
	const auto version = Str::convertToUTF8(Str::getStringFromNumber(g_Version));
	const auto lastDelta = p_Metadata.m_LastDelta ? Str::convertToUTF8(*p_Metadata.m_LastDelta) : std::string();

	sqlite3_bind_text(m_SqlStatementSetMetadata, 1, name.c_str(), -1, SQLITE_STATIC);
	if (p_Metadata.m_FullListSize)
		sqlite3_bind_text(m_SqlStatementSetMetadata, 2, fullSize.c_str(), -1, SQLITE_STATIC);
	else
		sqlite3_bind_null(m_SqlStatementSetMetadata, 2);

	if (p_Metadata.m_FullRefreshDate)
		sqlite3_bind_text(m_SqlStatementSetMetadata, 3, date.c_str(), -1, SQLITE_STATIC);
	else
		sqlite3_bind_null(m_SqlStatementSetMetadata, 3);

	sqlite3_bind_text(m_SqlStatementSetMetadata, 4, version.c_str(), -1, SQLITE_STATIC);
	sqlite3_bind_text(m_SqlStatementSetMetadata, 5, lastDelta.c_str(), -1, SQLITE_STATIC);

	const auto result = this->sqlite3_step(m_SqlStatementSetMetadata);
	ASSERT(result == SQLITE_DONE);
	ProcessError(result, nullptr, _YTEXT(""));
	sqlite3_reset(m_SqlStatementSetMetadata);
}

int CachedObjectsSqlEngine::countObjectsMissingRequirements(const wstring& p_TableName, const std::vector<wstring>& p_RequiredDataColumns) const
{
	ASSERT(!p_RequiredDataColumns.empty());

	int count = 0;

	wstring countQuery = _YTEXTFORMAT(L"SELECT COUNT(*) FROM \"%s\" O WHERE ", p_TableName.c_str());
	for (const auto& c : p_RequiredDataColumns)
		countQuery += _YTEXTFORMAT(L"O.%s IS NULL OR ", c.c_str());
	if (!p_RequiredDataColumns.empty())
		countQuery.erase(countQuery.size() - 4); // Erase last " OR ";

	const auto countQueryAscii = MFCUtil::convertUNICODE_to_ASCII(countQuery);

	char* errMsg = nullptr;
	const auto result = sqlite3_exec(countQueryAscii.c_str(), countCallback, &count, &errMsg);
	ASSERT(result == SQLITE_OK);
	ProcessError(result, errMsg, _YTEXT(""));

	return count;
}

bool CachedObjectsSqlEngine::clearTableAndMetadata(const SQLiteUtil::Table& p_Table)
{
	return clearTable(p_Table) && clearMetadataFor(p_Table);
}

bool CachedObjectsSqlEngine::clearTable(const SQLiteUtil::Table& p_Table)
{
	const auto query = _YTEXTFORMAT(L"DELETE FROM \"%s\"", p_Table.m_Name.c_str());
	const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(query);

	char* errMsg = nullptr;
	const auto result = sqlite3_exec(queryAscii.c_str(), nullptr, nullptr, &errMsg);
	ASSERT(result == SQLITE_OK);
	ProcessError(result, errMsg, _YTEXT(""));
	return result == SQLITE_OK;
}

bool CachedObjectsSqlEngine::clearMetadataFor(const SQLiteUtil::Table& p_Table)
{
	deleteEntry(m_SqlStatementDeleteMetadata, m_TableMetadata.m_Name);
	return true;
}

int countCallback(void* count, int argc, char** argv, char** colName)
{
	auto* c = reinterpret_cast<int*>(count);
	if (argc == 1 && argv != nullptr)
		*c = atoi(argv[0]);
	return 0;
}
