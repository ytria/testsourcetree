#pragma once

class MailboxPermissionsAccessRights
{
public:
	static const wstring g_ChangeOwner;
	static const wstring g_ChangePermission;
	static const wstring g_DeleteItem;
	static const wstring g_ExternalAccount;
	static const wstring g_FullAccess;
	static const wstring g_ReadPermission;
};

