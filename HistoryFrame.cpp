#include "HistoryFrame.h"

#include "GridFrameBase.h"
#include "MFCUtil.h"
#include "BaseO365Grid.h"

IHistoryFrame::IHistoryFrame(CFrameWnd* p_That, HistoryMode::Mode p_HistoryMode, IHistoryFrame* p_PreviousHistoryFrame)
	: m_That(p_That)
	, m_PreviousHistoryFrame(p_PreviousHistoryFrame)
	, m_NextHistoryFrame(nullptr)
	, m_HistoryMode(p_HistoryMode)
{
}

IHistoryFrame::~IHistoryFrame()
{
	ASSERT(dynamic_cast<IHistoryFrame*>(m_That) == this);

	// destroy whole history branch
	auto frame = this;
	while (nullptr != frame && nullptr != frame->m_NextHistoryFrame)
		frame = frame->m_NextHistoryFrame;

	while (nullptr != frame)
	{
		auto previousToClose = frame->m_PreviousHistoryFrame;
		frame->m_PreviousHistoryFrame = nullptr;
		if (this != frame)
		{
			frame->m_NextHistoryFrame = nullptr;
			frame->m_That->SendMessage(WM_CLOSE, GridFrameBase::g_NoWindowsListUpdate);
		}
		frame = previousToClose;
	}
}

void IHistoryFrame::Erect()
{
	ASSERT(dynamic_cast<IHistoryFrame*>(m_That) == this);

	ErectSpecific();

	{
		// Force ribbon update to avoid empty texts if an error message box is displayed during load.
		// Without this, the ribbon will only be updated on the next OnIdle() which often doesn't take splace
		// before the error message box (and blocked when the box is shown).
		// (See Bug #180316.RO.00801A)
		auto gfb = dynamic_cast<GridFrameBase*>(m_That);
		ASSERT(nullptr != gfb && nullptr != gfb->GetCommandBars());
		if (nullptr != gfb && nullptr != gfb->GetCommandBars())
			gfb->GetCommandBars()->UpdateCommandBars();
	}

	if (nullptr != m_PreviousHistoryFrame)
	{
		ASSERT(nullptr != m_PreviousHistoryFrame->m_That);
		if (nullptr != m_PreviousHistoryFrame->m_That)
		{
			applyFramePlacement(m_PreviousHistoryFrame->m_That, m_That);	
			CallShowWindow(m_PreviousHistoryFrame->m_That, SW_HIDE);
			m_PreviousHistoryFrame->HiddenViaHistory();
		}

		const bool shouldDetachExistingNextHistory = HistoryMode::Mode::DETACH_HISTORY == m_HistoryMode;
		if (nullptr != m_PreviousHistoryFrame->m_NextHistoryFrame && this != m_PreviousHistoryFrame->m_NextHistoryFrame)
		{
			ASSERT(nullptr != m_PreviousHistoryFrame->m_NextHistoryFrame->m_That
				&& !m_PreviousHistoryFrame->m_NextHistoryFrame->m_That->IsWindowVisible());

			if (shouldDetachExistingNextHistory)
			{
				// Detach existing 'next' history
				m_PreviousHistoryFrame->m_NextHistoryFrame->m_PreviousHistoryFrame = nullptr;
				m_PreviousHistoryFrame->m_NextHistoryFrame->m_That->ShowWindow(SW_SHOWNOACTIVATE);
				m_PreviousHistoryFrame->m_NextHistoryFrame->HistoryChanged();
			}
			else
			{
				// Clean existing next frames for our previous frame.
				auto frame = m_PreviousHistoryFrame->m_NextHistoryFrame;
				frame->m_PreviousHistoryFrame = nullptr;
				while (nullptr != frame->m_NextHistoryFrame)
					frame = frame->m_NextHistoryFrame;

				while (nullptr != frame)
				{
					frame->m_NextHistoryFrame = nullptr;
					auto previousToClose = frame->m_PreviousHistoryFrame;
					frame->m_PreviousHistoryFrame = nullptr;
					frame->m_That->SendMessage(WM_CLOSE, GridFrameBase::g_NoWindowsListUpdate);
					frame = previousToClose;
				}
			}
		}
		m_PreviousHistoryFrame->m_NextHistoryFrame = this;

		{
			auto frame = m_PreviousHistoryFrame;
			while (nullptr != frame)
			{
				frame->HistoryChanged();
				frame = frame->m_PreviousHistoryFrame;
			}
		}
	}

	// Bug #180904.SR.008A96: When loading a module in the same frame, the previous grid is shown below the log display.
	{
		auto gfb = dynamic_cast<GridFrameBase*>(m_That);
		ASSERT(nullptr != gfb);
		if (nullptr != gfb)
			gfb->GetGrid().UpdateWindow();
	}

	HistoryChanged();
}

bool IHistoryFrame::HasPrevious() const
{
	return nullptr != m_PreviousHistoryFrame;
}

bool IHistoryFrame::HasNext() const
{
	return nullptr != m_NextHistoryFrame;
}

IHistoryFrame* IHistoryFrame::GetNext() const
{
	return m_NextHistoryFrame;
}

bool IHistoryFrame::GoToPrevious()
{
	return GoTo(m_PreviousHistoryFrame);
}

bool IHistoryFrame::GoToNext(bool p_Refresh)
{
	const bool ret = GoTo(m_NextHistoryFrame);
	if (ret && p_Refresh)
	{
		ASSERT(nullptr != m_NextHistoryFrame->m_That);
		if (nullptr != m_NextHistoryFrame->m_That)
			m_NextHistoryFrame->m_That->SendMessage(WM_COMMAND, ID_365REFRESH);
	}
	return ret;
}

bool IHistoryFrame::GoTo(IHistoryFrame* pFrame)
{
	if (nullptr != pFrame)
	{
		ASSERT(nullptr != m_That && dynamic_cast<IHistoryFrame*>(m_That) == this);
		if (nullptr != m_That)
		{
			ASSERT(nullptr != pFrame->m_That);
			if (nullptr != pFrame->m_That)
				applyFramePlacement(m_That, pFrame->m_That);
			pFrame->ShownViaHistory();
			CallShowWindow(m_That, SW_HIDE);
			HiddenViaHistory();
		}
		return true;
	}

	return false;
}

bool IHistoryFrame::JumpTo(IHistoryFrame* pFrame)
{
	ASSERT(isInSameHistory(pFrame));
	return GoTo(pFrame);
}

vector<IHistoryFrame*> IHistoryFrame::GetPreviousFrames() const
{
	vector<IHistoryFrame*> frames;

	auto previousFrame = m_PreviousHistoryFrame;
	while (nullptr != previousFrame)
	{
		ASSERT(this != previousFrame);
		frames.push_back(previousFrame);
		previousFrame = previousFrame->m_PreviousHistoryFrame;
	}

	return frames;
}

vector<IHistoryFrame*> IHistoryFrame::GetNextFrames() const
{
	vector<IHistoryFrame*> frames;

	auto nextFrame = m_NextHistoryFrame;
	while (nullptr != nextFrame)
	{
		ASSERT(this != nextFrame);
		frames.push_back(nextFrame);
		nextFrame = nextFrame->m_NextHistoryFrame;
	}

	return frames;
}

void IHistoryFrame::applyFramePlacement(CFrameWnd* p_SrcFrame, CFrameWnd* p_Destframe)
{
	ASSERT(nullptr != p_SrcFrame && nullptr != p_Destframe);
	ASSERT(!p_SrcFrame->IsIconic() && p_SrcFrame->IsWindowVisible());
	if (FALSE == p_SrcFrame->IsIconic() && FALSE != p_SrcFrame->IsWindowVisible())
	{
		WINDOWPLACEMENT winPla;
		winPla.length = sizeof(WINDOWPLACEMENT);
		GetWindowPlacement(p_SrcFrame->m_hWnd, &winPla);
		p_Destframe->SetWindowPlacement(&winPla);
	}
	else
	{
		CallShowWindow(p_Destframe, SW_RESTORE);
	}
}

bool IHistoryFrame::isInSameHistory(IHistoryFrame* pFrame) const
{
	ASSERT(nullptr != pFrame && this != pFrame);
	if (nullptr != pFrame && this != pFrame)
	{
		auto previousFrame = m_PreviousHistoryFrame;
		while (nullptr != previousFrame)
		{
			if (previousFrame == pFrame)
				return true;
			previousFrame = previousFrame->m_PreviousHistoryFrame;
		}

		auto nextFrame = m_NextHistoryFrame;
		while (nullptr != nextFrame)
		{
			if (nextFrame == pFrame)
				return true;
			nextFrame = nextFrame->m_NextHistoryFrame;
		}
	}

	return nullptr != pFrame && this == pFrame;
}

void IHistoryFrame::HistoryChanged()
{
	// Nothing to do in base class
}

void IHistoryFrame::ShownViaHistory()
{
	// Nothing to do in base class
}

void IHistoryFrame::HiddenViaHistory()
{
	// Nothing to do in base class
}

void IHistoryFrame::CallShowWindow(CFrameWnd* p_Frame, int p_CmdShow)
{
	WINDOWPLACEMENT wp;
	wp.length = sizeof(WINDOWPLACEMENT);
	p_Frame->GetWindowPlacement(&wp);
	wp.showCmd = p_CmdShow;
	p_Frame->SetWindowPlacement(&wp);
}
