#include "ModuleDrive.h"

#include "AutomationDataStructure.h"
#include "BusinessObjectManager.h"
#include "CachedUserPropertySet.h"
#include "ChannelLoadMoreRequester.h"
#include "CheckoutStatusRequester.h"
#include "CommandInfo.h"
#include "DlgInput.h"
#include "DriveChanges.h"
#include "DriveItemDownloadInfo.h"
#include "FileUtil.h"
#include "FrameDriveItems.h"
#include "GraphCache.h"
#include "MSGraphSession.h"
#include "Office365Admin.h"
#include "O365AdminUtil.h"
#include "RestResultInfo.h"
#include "safeTaskCall.h"
#include "TaskDataManager.h"
#include "YDataCallbackMessage.h"
#include "YCallbackMessage.h"

#include <cpprest/containerstream.h>
#include <cpprest/filestream.h>
#include <cpprest/asyncrt_utils.h>
#include "MultiObjectsRequestLogger.h"
#include "MultiObjectsRequestLogger.h"
#include "MultiObjectsPageRequestLogger.h"
#include "LoggerService.h"
#include "MFCUtil.h"
#include "GridDriveItems.h"

using namespace Util;

void ModuleDrive::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
    case Command::ModuleTask::ListMe:
    {
        Command newCmd = p_Command;
        newCmd.GetCommandInfo().GetIds() = { PooledString(GetConnectedSession().GetConnectedUserId()) };
        showDriveItems(newCmd);
        break;
    }

    case Command::ModuleTask::ListSubItems:
		showDriveItems(p_Command);
		break;

	case Command::ModuleTask::ListPermissions:
	case Command::ModuleTask::ListPermissionsExplode:
	case Command::ModuleTask::ListPermissionsExplodeFlat:
		viewPermissions(p_Command);
		break;

    case Command::ModuleTask::DownloadDriveItems:
		downloadDriveItems(p_Command);
		break;

    case Command::ModuleTask::ApplyChanges:
        applyChanges(p_Command);
        break;

	case Command::ModuleTask::LoadCheckoutStatus:
		loadCheckoutStatus(p_Command);
		break;

	case Command::ModuleTask::LoadSnapshot:
		loadSnapshot(p_Command);
		break;

	case Command::ModuleTask::SearchMe:
	{
		Command newCmd = p_Command;
		newCmd.GetCommandInfo().GetIds() = { PooledString(GetConnectedSession().GetConnectedUserId()) };
		showDriveItems(newCmd);
		break;
	}

	case Command::ModuleTask::Search:
	case Command::ModuleTask::SearchSubItems:
		showDriveItems(p_Command);
		break;

	default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
		break;
    }
}

void ModuleDrive::applyChanges(const Command& p_Command)
{
    CommandInfo info = p_Command.GetCommandInfo();
    AutomationAction* action = p_Command.GetAutomationAction();
    auto frame = dynamic_cast<FrameDriveItems*>(info.GetFrame());
	ASSERT(nullptr != frame);

    ASSERT(info.Data().is_type<DriveChanges>());
    if (info.Data().is_type<DriveChanges>())
    {
        const auto& changes = info.Data().get_value<DriveChanges>();
		auto bom = frame->GetBusinessObjectManager();

		O365IdsContainer itemIds;
        for (const auto& driveItem : changes.m_Updates.m_ObjectsToUpdate)
            itemIds.insert(driveItem.GetID());
        for (const auto& subKey : changes.m_PermsToDelete)
            itemIds.insert(subKey.m_Id2);
        for (const auto& subKey : changes.m_PermsToUpdate)
            itemIds.insert(subKey.second.m_Id2);

        auto taskData = addFrameTask(YtriaTranslate::Do(ModuleDrive_applyChanges_1, _YLOC("Applying changes")).c_str(), frame, p_Command, false);

		auto gridDriveItems = dynamic_cast<GridDriveItems*>(&frame->GetGrid());
		ASSERT(nullptr != gridDriveItems && nullptr != gridDriveItems->GetColMetaID());

		YSafeCreateTask([this, changes, p_Command, metaIDColID = gridDriveItems->GetColMetaID()->GetID(), criteria = frame->GetModuleCriteria(), hwnd = frame->m_hWnd, taskData, action, bom, moduleCriteria = frame->GetModuleCriteria()]()
        {
			RefreshSpecificData refreshData;
			refreshData.m_Criteria = criteria;			

			const bool isForChannels = criteria.m_Origin == Origin::Channel;
			ASSERT(isForChannels && refreshData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::SUBIDS
				|| refreshData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::IDS);

			const auto oldSubIds = refreshData.m_Criteria.m_SubIDs;
			if (isForChannels)
				refreshData.m_Criteria.m_SubIDs.clear();
			else
				refreshData.m_Criteria.m_IDs.clear();

            // Drive item updates
            std::vector<O365UpdateOperation> updateOperations;
            if (!changes.m_Updates.m_ObjectsToUpdate.empty())
            {
				std::map<wstring, PooledString> newFolderIds;
                const auto& updates = changes.m_Updates;

				// Do not call generic impl, as we need to do more stuff in the loop
				// updateOperations = bom->WriteSingleRequest(updates.m_ObjectsToUpdate, updates.m_PrimaryKeys, taskData);
				{
					const auto& driveItems = updates.m_ObjectsToUpdate;
					const auto& pks = updates.m_PrimaryKeys;
					ASSERT(driveItems.size() == pks.size());
					if (driveItems.size() == pks.size())
					{
						size_t pkIndex = 0;

						auto processItem = [&updateOperations, &newFolderIds, &taskData, &pks, &pkIndex, &bom](const auto& driveItem)
						{
							BusinessDriveItem tempDriveItem;
							bool newItem = false;
							const BusinessDriveItem* driveItemPtr = &driveItem;
							if (O365Grid::IsTemporaryCreatedObjectID(driveItem.GetID()))
							{
								newItem = true;
								if (driveItem.GetName())
								{
									if (driveItem.IsFolder())
									{
										LoggerService::User(_YFORMAT(L"Creating folder \"%s\" - %s/%s", driveItem.GetName()->c_str(), std::to_wstring(pkIndex + 1).c_str(), std::to_wstring(pks.size()).c_str()), taskData.GetOriginator());
									}
									else
									{
										ASSERT(!driveItem.IsNotebook());
										LoggerService::User(_YFORMAT(L"Creating file \"%s\" - %s/%s", driveItem.GetName()->c_str(), std::to_wstring(pkIndex + 1).c_str(), std::to_wstring(pks.size()).c_str()), taskData.GetOriginator());
									}
								}
								else
								{
									ASSERT(false);
									if (driveItem.IsFolder())
									{
										LoggerService::User(_YFORMAT(L"Creating new folder - %s/%s", std::to_wstring(pkIndex + 1).c_str(), std::to_wstring(pks.size()).c_str()), taskData.GetOriginator());
									}
									else
									{
										ASSERT(!driveItem.IsNotebook());
										LoggerService::User(_YFORMAT(L"Creating new file - %s/%s", std::to_wstring(pkIndex + 1).c_str(), std::to_wstring(pks.size()).c_str()), taskData.GetOriginator());
									}
								}

								if (driveItem.GetParentFolderId() && O365Grid::IsTemporaryCreatedObjectID(*driveItem.GetParentFolderId()))
								{
									auto it = newFolderIds.find(*driveItem.GetParentFolderId());
									ASSERT(newFolderIds.end() != it);
									if (newFolderIds.end() != it)
									{
										tempDriveItem = driveItem;
										tempDriveItem.SetParentFolderId(it->second);
										driveItemPtr = &tempDriveItem;
									}
								}
							}
							else
							{
								newItem = false;

								boost::YOpt<PooledString> itemName;
								if (driveItem.GetName())
								{
									itemName = driveItem.GetName();
								}
								else if (!driveItem.GetOneDrivePath().IsEmpty())
								{
									itemName = [&path = driveItem.GetOneDrivePath()]()
									{
										const wstring p = path;
										const auto pos = p.rfind(L'/');
										if (wstring::npos == pos)
											return boost::YOpt<PooledString>(path);
										if (pos < p.size() - 1)
											return boost::YOpt<PooledString>(p.substr(pos + 1));
										return boost::YOpt<PooledString>();
									}();
								}

								if (itemName)
								{
									if (driveItem.IsFolder())
									{
										LoggerService::User(_YFORMAT(L"Updating folder \"%s\" - %s/%s", itemName->c_str(), std::to_wstring(pkIndex + 1).c_str(), std::to_wstring(pks.size()).c_str()), taskData.GetOriginator());
									}
									else
									{
										ASSERT(!driveItem.IsNotebook());
										LoggerService::User(_YFORMAT(L"Updating file \"%s\" - %s/%s", itemName->c_str(), std::to_wstring(pkIndex + 1).c_str(), std::to_wstring(pks.size()).c_str()), taskData.GetOriginator());
									}
								}
								else
								{
									ASSERT(false);
									if (driveItem.IsFolder())
									{
										LoggerService::User(_YFORMAT(L"Updating folder with id \"%s\" - %s/%s", driveItem.GetID().c_str(), std::to_wstring(pkIndex + 1).c_str(), std::to_wstring(pks.size()).c_str()), taskData.GetOriginator());
									}
									else
									{
										ASSERT(!driveItem.IsNotebook());
										LoggerService::User(_YFORMAT(L"Updating file with id \"%s\" - %s/%s", driveItem.GetID().c_str(), std::to_wstring(pkIndex + 1).c_str(), std::to_wstring(pks.size()).c_str()), taskData.GetOriginator());
									}
								}
							}

							updateOperations.emplace_back(pks[pkIndex++]);
							updateOperations.back().StoreResults(driveItemPtr->SendWriteSingleRequest(bom->GetSapio365Session(), taskData).GetTask().get());

							if (newItem && (driveItemPtr->IsFolder() || driveItemPtr->IsNotebook()))
							{
								auto& results = updateOperations.back().GetResults();
								ASSERT(results.size() == 1);
								if (!results.empty())
								{
									const auto statusCode = results.back().m_ResultInfo->Response.status_code();
									if (statusCode == http::status_codes::Created)
									{
										const auto value = json::value::parse(*results.back().m_ResultInfo->GetBodyAsString());
										ASSERT(value.is_object());
										const auto& obj = value.as_object();
										auto it = obj.find(_YTEXT(O365_ID));
										if (it != obj.end() && it->second.is_string())
										{
											newFolderIds[driveItem.GetID()] = it->second.as_string();
										}
									}
								}
							}
						};

						// Sort by hierarchy level so that folders are processed before their children.
						// (Sort pointers to avoid copying objects)

						vector<const BusinessDriveItem*> sortedItems;
						std::transform(driveItems.begin(), driveItems.end(), std::back_inserter(sortedItems), [](const auto& item) {return &item; });
						std::sort(sortedItems.begin(), sortedItems.end(), [](const auto& left, const auto& right) {return left->GetHierarchyLevel() < right->GetHierarchyLevel(); });
						for (const auto& driveItem : sortedItems)
						{
							processItem(*driveItem);
						}
					}
				}

				if (isForChannels)
				{
					ASSERT(!oldSubIds.empty());
					for (auto& updateOp : updateOperations)
					{
						const auto channelID = updateOp.GetPkFieldStr(metaIDColID);
						std::vector<const SubIdKey*> teamIds;
						for (auto& sub : oldSubIds)
						{
							if (sub.second.end() != sub.second.find(channelID))
								teamIds.push_back(&sub.first);
						}
						ASSERT(teamIds.size() == 1);
						for (auto teamId : teamIds) // Just in case same channel id in several teams
							refreshData.m_Criteria.m_SubIDs[*teamId].insert(channelID);
					}
				}
				else
				{
					for (auto& updateOp : updateOperations)
						refreshData.m_Criteria.m_IDs.insert(updateOp.GetPkFieldStr(metaIDColID));
				}
            }

            // Delete permissions
			std::map<SubItemKey, HttpResultWithError> deleteResults;
			for (auto& permToDelete : changes.m_PermsToDelete)
			{
				if (taskData.IsCanceled())
					break;

				ASSERT(!isForChannels);

				deleteResults[permToDelete] = bom->DeletePermission(/*permToDelete.m_Id0, */permToDelete.m_Id1, permToDelete.m_Id2, permToDelete.m_Id3, taskData).GetTask().get();

				if (isForChannels)
				{
					ASSERT(!oldSubIds.empty());
					const auto& channelID = permToDelete.m_Id0;
					std::vector<const SubIdKey*> teamIds;
					for (auto& sub : oldSubIds)
					{
						if (sub.second.end() != sub.second.find(channelID))
							teamIds.push_back(&sub.first);
					}
					ASSERT(teamIds.size() == 1);
					for (auto teamId : teamIds) // Just in case same channel id in several teams
						refreshData.m_Criteria.m_SubIDs[*teamId].insert(channelID);
				}
				else
				{
					refreshData.m_Criteria.m_IDs.insert(permToDelete.m_Id0);
				}
			}

            // Update permissions
			std::map<SubItemKey, HttpResultWithError> updateResults;
            for (auto& permToUpdate : changes.m_PermsToUpdate)
            {
				if (taskData.IsCanceled())
					break;

                const auto& businessPerm = permToUpdate.first;
                const auto& key = permToUpdate.second;
                updateResults[key] = bom->UpdatePermission(
					/*key.m_Id0,*/
                    key.m_Id1,
					key.m_Id2,
                    businessPerm.ToPermission(),
                    { rttr::type::get<Permission>().get_property("roles") },
                    taskData).GetTask().get();

				if (isForChannels)
				{
					ASSERT(!oldSubIds.empty());
					const auto& channelID = key.m_Id0;
					std::vector<const SubIdKey*> teamIds;
					for (auto& sub : oldSubIds)
					{
						if (sub.second.end() != sub.second.find(channelID))
							teamIds.push_back(&sub.first);
					}
					ASSERT(teamIds.size() == 1);
					for (auto teamId : teamIds) // Just in case same channel id in several teams
						refreshData.m_Criteria.m_SubIDs[*teamId].insert(channelID);
				}
				else
				{
					refreshData.m_Criteria.m_IDs.insert(key.m_Id0);
				}
            }

            YCallbackMessage::DoPost([hwnd, refreshData, action, taskData, deleteResults, updateResults, updateOperations]()
            {
                if (::IsWindow(hwnd))
                {
					auto frame = dynamic_cast<FrameDriveItems*>(CWnd::FromHandle(hwnd));
					ASSERT(nullptr != frame);

                    frame->GetGrid().GetModifications().SetSubItemDeletionsErrors(deleteResults);
                    frame->GetGrid().GetModifications().SetSubItemUpdateErrors(updateResults);
                    frame->GetGrid().ClearLog(taskData.GetId());
                    frame->RefreshAfterUpdate(vector<O365UpdateOperation>{ updateOperations }, refreshData, action);
                }
                else
                {
                    SetAutomationGreenLight(action);
                }
                TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
            });
        });
    }
    else
    {
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2468")).c_str());
    }
}

void ModuleDrive::showDriveItems(Command p_Command)
{
	auto& info		= p_Command.GetCommandInfo();
	const auto	p_Origin	= info.GetOrigin();

	ASSERT(p_Origin != Origin::NotSet);
    if (p_Origin != Origin::NotSet)
    {
        const auto&	p_IDs = info.GetIds();
		const auto& p_SubIDs = info.GetSubIds();
        auto	p_Frame = dynamic_cast<FrameDriveItems*>(info.GetFrame());
        auto	p_SourceWindow = p_Command.GetSourceWindow();
        auto	p_Action = p_Command.GetAutomationAction();
        auto	sourceCWnd = p_SourceWindow.GetCWnd();

        auto frame = p_Frame;
        if (nullptr == frame)
        {
            if (!FrameDriveItems::CanCreateNewFrame(true, sourceCWnd, p_Action))
                return;

            ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin = p_Origin;
			if (p_Origin == Origin::Channel)
			{
				ASSERT(p_IDs.empty());
				moduleCriteria.m_UsedContainer = ModuleCriteria::UsedContainer::SUBIDS;
				moduleCriteria.m_SubIDs = p_SubIDs;
			}
			else
			{
				ASSERT(p_SubIDs.empty());
				moduleCriteria.m_UsedContainer = ModuleCriteria::UsedContainer::IDS;
				moduleCriteria.m_IDs = p_IDs;
			}
			moduleCriteria.m_Privilege = info.GetRBACPrivilege();
			moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

            if (ShouldCreateFrame<FrameDriveItems>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
            {
				if (CRMpipe().IsFeatureSandboxed() && (Command::ModuleTask::Search == p_Command.GetTask() || Command::ModuleTask::SearchSubItems == p_Command.GetTask() || Command::ModuleTask::SearchMe == p_Command.GetTask()))
				{
					DlgInput dlg(_YTEXT("Optional search criteria"), _YTEXT(""), _YTEXT(""), p_SourceWindow.GetCWnd());
					if (IDOK == dlg.DoModal())
						info.Data2() = boost::YOpt<wstring>(dlg.GetText());
				}

                CWaitCursor _;

				const wstring title = p_Origin == Origin::User	
					? (p_Command.GetTask() == Command::ModuleTask::ListMe
						? _T("My OneDrive Files")
						: YtriaTranslate::Do(ModuleDrive_showDriveItems_1, _YLOC("OneDrive Files")).c_str())
					: YtriaTranslate::Do(ModuleDrive_showDriveItems_3, _YLOC("Files")).c_str();
				frame = new FrameDriveItems(p_Origin, p_Command.GetTask() == Command::ModuleTask::ListMe, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), title, *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				if (p_Command.GetCommandInfo().Data2().is_type<boost::YOpt<wstring>>())
					frame->SetSearchCriteria(p_Command.GetCommandInfo().Data2().get_value<boost::YOpt<wstring>>());
                frame->InitModuleCriteria(moduleCriteria);
                AddGridFrame(frame);
                frame->Erect();
            }
            else
                return;
        }

		boost::YOpt<wstring> searchCriteria;
		if (nullptr != frame && p_Command.GetCommandInfo().Data2().is_type<boost::YOpt<wstring>>())
		{
			frame->SetSearchCriteria(p_Command.GetCommandInfo().Data2().get_value<boost::YOpt<wstring>>());
			searchCriteria = p_Command.GetCommandInfo().Data2().get_value<boost::YOpt<wstring>>();
		}

        const bool isUpdate = nullptr != p_Frame;
		const wstring taskTitle = p_Origin == Origin::User	? YtriaTranslate::Do(ModuleDrive_showDriveItems_2, _YLOC("Show OneDrive Files")).c_str()
															: YtriaTranslate::Do(ModuleDrive_showDriveItems_4, _YLOC("Show Files")).c_str();
        auto taskData = addFrameTask(taskTitle, frame, p_Command, !isUpdate);

        auto gridDriveItems = dynamic_cast<GridDriveItems*>(&frame->GetGrid());
        std::vector<GridBackendRow*> rowsForLoadPermissions;
		std::vector<GridBackendRow*> rowsForLoadCheckoutStatus;

		std::map<PooledString, PooledString> channelNames;
        RefreshSpecificData refreshSpecificData;
		if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
		{
			refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();
		}
		else
		{
			if (p_Origin == Origin::Channel && info.Data().is_type<std::map<PooledString, PooledString>>()) // Only happens when the window is first opened
				channelNames = info.Data().get_value<std::map<PooledString, PooledString>>();
			gridDriveItems->GetRowsWithMoreLoaded(rowsForLoadPermissions);
			gridDriveItems->GetRowsWithCheckoutStatusLoaded(rowsForLoadCheckoutStatus);
		}

        if (refreshSpecificData.m_SelectedRowsAncestors && !refreshSpecificData.m_SelectedRowsAncestors->empty())
        {
			ASSERT(rowsForLoadPermissions.empty());
            gridDriveItems->GetRowsByCriteria(rowsForLoadPermissions, [&rowAncestors = *refreshSpecificData.m_SelectedRowsAncestors](GridBackendRow* row) {
                return row->IsMoreLoaded() && rowAncestors.find(row->GetTopAncestorOrThis()) != rowAncestors.end();
            });

			ASSERT(rowsForLoadCheckoutStatus.empty());
			gridDriveItems->GetRowsByCriteria(rowsForLoadCheckoutStatus, [gridDriveItems, &rowAncestors = *refreshSpecificData.m_SelectedRowsAncestors](GridBackendRow* row) {
				return gridDriveItems->HasRowCheckoutStatus(row) && rowAncestors.find(row->GetTopAncestorOrThis()) != rowAncestors.end();
			});
        }

		if (p_Origin == Origin::Channel && channelNames.empty())
		{
			ASSERT(nullptr != frame);
			if (nullptr != frame)
				channelNames = frame->GetChannelNames();
		}

		YSafeCreateTask([this, taskData, refreshSpecificData, hwnd = frame->GetSafeHwnd(), p_Origin, p_IDs, p_SubIDs, p_Action, isUpdate, gridDriveItems, rowsForLoadPermissions, bom = frame->GetBusinessObjectManager(), permissionsRequestInfo = gridDriveItems->GetSubRequestInfo(rowsForLoadPermissions, {}), checkoutStatusRequestInfo = gridDriveItems->GetSubRequestInfo(rowsForLoadCheckoutStatus, {}), wantsAdditionalMetadata = (bool)frame->GetMetaDataColumnInfo(), channelNames, searchCriteria]()
		{
            const bool partialRefresh = Origin::Channel == p_Origin ? !refreshSpecificData.m_Criteria.m_SubIDs.empty() : !refreshSpecificData.m_Criteria.m_IDs.empty();
			const auto& ids = partialRefresh ? refreshSpecificData.m_Criteria.m_IDs : p_IDs;
			const auto& subIds = partialRefresh ? refreshSpecificData.m_Criteria.m_SubIDs : p_SubIDs;

			if (Origin::User == p_Origin || Origin::DeletedUser == p_Origin)
				ModuleDrive::showDriveItemsIds<BusinessUser>(ids, searchCriteria, permissionsRequestInfo, checkoutStatusRequestInfo, partialRefresh, p_Origin, bom, hwnd, p_Action, taskData, isUpdate, wantsAdditionalMetadata);
			else if (Origin::Group == p_Origin)
				ModuleDrive::showDriveItemsIds<BusinessGroup>(ids, searchCriteria, permissionsRequestInfo, checkoutStatusRequestInfo, partialRefresh, p_Origin, bom, hwnd, p_Action, taskData, isUpdate, wantsAdditionalMetadata);
			else if (Origin::Site == p_Origin)
				ModuleDrive::showDriveItemsIds<BusinessSite>(ids, searchCriteria, permissionsRequestInfo, checkoutStatusRequestInfo, partialRefresh, p_Origin, bom, hwnd, p_Action, taskData, isUpdate, wantsAdditionalMetadata);
			else if (Origin::Channel == p_Origin)
				ModuleDrive::showDriveItemsSubIds(subIds, permissionsRequestInfo, checkoutStatusRequestInfo, partialRefresh, p_Origin, bom, hwnd, p_Action, taskData, isUpdate, channelNames);
		});
	}
	else
	{
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleContact_showContacts_3, _YLOC(" - origin not set"),_YR("Y2469")).c_str());
	}
}

void ModuleDrive::viewPermissions(const Command& p_Command)
{
	CommandInfo info = p_Command.GetCommandInfo();

	if (info.Data().is_type<DriveSubInfoIds>())
	{
		auto&	p_IDs		= info.Data().get_value<DriveSubInfoIds>();
		auto	p_Frame		= dynamic_cast<FrameDriveItems*>(info.GetFrame());
		auto	p_Action	= p_Command.GetAutomationAction();
		auto	p_TaskType	= p_Command.GetTask();

		ASSERT(nullptr != p_Frame);
		if (nullptr != p_Frame)
		{
			auto taskData = addFrameTask(YtriaTranslate::Do(ModuleDrive_viewPermissions_1, _YLOC("Show Permissions")).c_str(), p_Frame, p_Command, false);

			YSafeCreateTask([p_IDs, hwnd = p_Frame->GetSafeHwnd(), taskData, p_Action, p_TaskType, bom = p_Frame->GetBusinessObjectManager()]()
			{
				using AllPermissions = std::map<std::pair<PooledString, PooledString>, std::vector<BusinessPermission>>;
				AllPermissions allPermissions;

				auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("permissions"), p_IDs.size());

				// Do request for every id
				for (const auto& id : p_IDs)
				{
					const wstring& driveId = std::get<1>(id);
					const wstring& driveItemId = std::get<2>(id);
					const wstring& driveItemName = std::get<3>(id);

					logger->IncrementObjCount();
					logger->SetContextualInfo(driveItemName);

					auto taskResult = bom->GetBusinessPermissions(driveId, driveItemId, logger, taskData).Then([id, taskData](const vector<BusinessPermission>& p_Permissions)
					{
						return std::make_pair(std::make_pair(std::get<0>(id), std::get<2>(id)), p_Permissions);
					}).GetTask().get();
					allPermissions[taskResult.first] = taskResult.second;
				}

				YDataCallbackMessage<AllPermissions>::DoPost(allPermissions, [taskData, hwnd, p_Action, p_TaskType, isCanceled = taskData.IsCanceled()](const AllPermissions& p_BusinessPermissions)
				{
					if (ShouldBuildView(hwnd, isCanceled, taskData, false))
					{
						auto frame = dynamic_cast<FrameDriveItems*>(CWnd::FromHandle(hwnd));
						frame->GetGrid().ClearLog(taskData.GetId());

						if (p_TaskType == Command::ModuleTask::ListPermissionsExplode)
							frame->ViewPermissionsExplode(p_BusinessPermissions);
						else if (p_TaskType == Command::ModuleTask::ListPermissionsExplodeFlat)
							frame->ViewPermissionsExplodeFlat(p_BusinessPermissions);
						else
							frame->ViewPermissions(p_BusinessPermissions, false);
					}
					TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
					SetAutomationGreenLight(p_Action);
				});
			});			
		}
		else
		{
			ASSERT(false);
			SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleDrive_viewPermissions_2, _YLOC(" - no frame"),_YR("Y2497")).c_str());
		}
	}
	else
	{
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleDrive_viewPermissions_3, _YLOC(" - no ids"),_YR("Y2498")).c_str());
	}
}

void ModuleDrive::downloadDriveItems(const Command& p_Command)
{
	CommandInfo info = p_Command.GetCommandInfo();
	if (info.Data().is_type<vector<DriveItemDownloadInfo>>())
	{
		auto& p_DownloadInfos	= info.Data().get_value<vector<DriveItemDownloadInfo>>();
		auto p_Frame			= dynamic_cast<FrameDriveItems*>(info.GetFrame());
		auto p_Action			= p_Command.GetAutomationAction();
		ASSERT(nullptr != p_Frame);
		if (nullptr != p_Frame)
		{
			HWND hwnd = p_Frame->GetSafeHwnd();

            auto grid = dynamic_cast<GridDriveItems*>(&p_Frame->GetGrid());
            ASSERT(nullptr != grid);
            if (nullptr != grid)
            {
                HWND gridHwnd = grid->GetSafeHwnd();

				// Especially used by Grid logger (Bug #200511.SR.00C04E)
				p_Frame->SetDownloadProcessRunning(true);

				const wstring taskTitle = p_Frame->GetOrigin() == Origin::User	? YtriaTranslate::Do(ModuleDrive_downloadDriveItems_1, _YLOC("Download OneDrive Files")).c_str()
																				: YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_1, _YLOC("Download Files")).c_str();
				auto taskData = addFrameTask(taskTitle, p_Frame, p_Command, false);
				YSafeCreateTask([this, p_DownloadInfos, p_Action, p_Frame, taskData, grid, gridHwnd, hwnd]()
                {
                    YCallbackMessage::DoPost([grid, p_DownloadInfos]()
					{
                        for (const auto& downloadInfo : p_DownloadInfos)
                        {
                            GridBackendField& field = downloadInfo.m_Row->AddField(0, grid->GetColDownloadProgress());

                            // Needed if we restart the download
                            field.SetCellType(GridBackendUtil::CELL_TYPE_PROGRESSBAR);
                            field.SetDataType(GridBackendUtil::LONG);
                        }
                        grid->SetColumnsVisible({ grid->GetColDownloadProgress() });
                        grid->UpdateMegaShark();
                    });

                    std::shared_ptr<DownloadTaskPool> downloadTaskPool = grid->GetDownloadTaskPool(taskData, hwnd);
                    downloadTaskPool->m_TaskPool->SetThisSharedPtr(downloadTaskPool->m_TaskPool);

					auto downloadCounter = std::make_shared<size_t>(p_DownloadInfos.size());
                    for (const auto& downloadInfo : p_DownloadInfos)
                    {
                        downloadTaskPool->m_TaskPool->Add([this, downloadInfo, taskData, p_Frame, grid, gridHwnd, downloadCounter, p_Action]()
                        {
                            auto taskResult = p_Frame->GetBusinessObjectManager()->GetDriveItemContent(downloadInfo.m_DriveId, downloadInfo.m_DriveItemId, taskData)
								.ThenByTask([this, downloadInfo, p_Frame, grid, taskData, gridHwnd, downloadCounter, p_Action](pplx::task<HttpResultWithError> p_GetDriveItemTask) {
								try
								{
									auto httpResultWithError = p_GetDriveItemTask.get();
									if (!httpResultWithError.m_Error && httpResultWithError.m_ResultInfo)
									{
										YCallbackMessage::DoPost([downloadInfo, grid, gridHwnd]() {
											if (::IsWindow(gridHwnd))
												grid->AddOccuringDownload(downloadInfo.m_Row);
										});
										auto fileLocation = downloadStreamingFile(httpResultWithError.m_ResultInfo->Response, downloadInfo, taskData, [p_Frame, downloadInfo, grid, taskData, gridHwnd](int32_t p_ProgressPercent)
										{
											YCallbackMessage::DoPost([downloadInfo, p_ProgressPercent, p_Frame, grid, taskData, gridHwnd]() {
												GridBackendRow* row = downloadInfo.m_Row;
												if (::IsWindow(gridHwnd))
													row->AddField(p_ProgressPercent, grid->GetColDownloadProgress());
											});
										});
										YCallbackMessage::DoPost([downloadInfo, grid, gridHwnd, fileLocation, downloadCounter, p_Action, p_Frame]()
										{
											if (::IsWindow(gridHwnd))
												grid->RemoveOccuringDownload(downloadInfo, fileLocation);

											if (--(*downloadCounter) == 0) // It was the last file
											{
												SetAutomationGreenLight(p_Action);
												if (::IsWindow(gridHwnd))
													p_Frame->SetDownloadProcessRunning(false);
												//TaskDataManager::Get().RemoveTaskData(taskData.GetId()); // Done in DownloadTaskPool destructor
											}
										});
									}
									else
									{
										ASSERT(httpResultWithError.m_Error);
										if (httpResultWithError.m_Error)
										{
											YCallbackMessage::DoPost([this, downloadInfo, httpResultWithError, p_Frame, grid, taskData, gridHwnd, downloadCounter, p_Action]() {
												GridBackendRow* row = downloadInfo.m_Row;
												ASSERT(row);
												if (nullptr != row && ::IsWindow(gridHwnd))
													downloadFailed(downloadInfo, *grid, *row, SapioError(*httpResultWithError.m_Error));

												if (--(*downloadCounter) == 0) // It was the last file
												{
													SetAutomationGreenLight(p_Action);
													if (::IsWindow(gridHwnd))
														p_Frame->SetDownloadProcessRunning(false);
													//TaskDataManager::Get().RemoveTaskData(taskData.GetId()); // Done in DownloadTaskPool destructor
												}
											});
										}
									}
								}
								catch (const std::exception& e)
								{
									YCallbackMessage::DoSend([this, &e, downloadInfo, grid, taskData, gridHwnd, p_Frame, downloadCounter, p_Action]() {
										GridBackendRow* row = downloadInfo.m_Row;
										ASSERT(row);
										if (nullptr != row)
										{
											if (::IsWindow(gridHwnd))
												downloadFailed(downloadInfo, *grid, *row, SapioError(e));
										}

										if (--(*downloadCounter) == 0) // It was the last file
										{
											SetAutomationGreenLight(p_Action);
											if (::IsWindow(gridHwnd))
												p_Frame->SetDownloadProcessRunning(false);
											//TaskDataManager::Get().RemoveTaskData(taskData.GetId()); // Done in DownloadTaskPool destructor
										}
									});
								}
                            }).GetTask().wait();
                        });
                    }
                });
            }
		}
		else
		{
			SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleDrive_viewPermissions_2, _YLOC(" - no frame"),_YR("Y2499")).c_str());
		}
	}
	else
	{
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2470")).c_str());
	}
}

void ModuleDrive::downloadFailed(const DriveItemDownloadInfo& p_DownloadInfo, GridDriveItems& p_Grid, GridBackendRow& p_Row, const SapioError& p_Error)
{
	O365Grid::RowStatusHandler<Scope::O365>(p_Grid).SetRowError(&p_Row, p_Error, _YTEXT(""));
	p_Row.AddField(_T("Failed"), p_Grid.GetColDownloadProgress(), GridBackendUtil::ICON_ERROR);
	p_Grid.RemoveOccuringDownload(p_DownloadInfo, boost::none);
	p_Grid.UpdateMegaShark();
}

void ModuleDrive::loadCheckoutStatus(const Command& p_Command)
{
	CommandInfo info = p_Command.GetCommandInfo();

	if (info.Data().is_type<DriveSubInfoIds>())
	{
		auto&	p_IDs = info.Data().get_value<DriveSubInfoIds>();
		auto	p_Frame = dynamic_cast<FrameDriveItems*>(info.GetFrame());
		auto	p_Action = p_Command.GetAutomationAction();
		auto	p_TaskType = p_Command.GetTask();

		ASSERT(nullptr != p_Frame);
		if (nullptr != p_Frame)
		{
			auto taskData = addFrameTask(_T("Load Checkout status"), p_Frame, p_Command, false);

			YSafeCreateTask([p_IDs, hwnd = p_Frame->GetSafeHwnd(), taskData, p_Action, p_TaskType, bom = p_Frame->GetBusinessObjectManager()]()
			{
				auto checkoutStatuses = retrieveCheckoutStatus(bom, p_IDs, taskData);

				YDataCallbackMessage<CheckoutStatuses>::DoPost(checkoutStatuses, [taskData, hwnd, p_Action, p_TaskType, isCanceled = taskData.IsCanceled()](const CheckoutStatuses& p_CheckoutStatuses)
				{
					if (ShouldBuildView(hwnd, isCanceled, taskData, false))
					{
						auto frame = dynamic_cast<FrameDriveItems*>(CWnd::FromHandle(hwnd));
						frame->GetGrid().ClearLog(taskData.GetId());
						frame->ViewCheckoutStatuses(p_CheckoutStatuses);
					}
					TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
					SetAutomationGreenLight(p_Action);
				});
			});
		}
		else
		{
			ASSERT(false);
			SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleDrive_viewPermissions_2, _YLOC(" - no frame"), _YR("Y2497")).c_str());
		}
	}
	else
	{
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleDrive_viewPermissions_3, _YLOC(" - no ids"), _YR("Y2498")).c_str());
	}
}

void ModuleDrive::loadSnapshot(const Command& p_Command)
{
	LoadSnapshot<FrameDriveItems>(p_Command, [&p_Command](const GridSnapshot::Metadata& p_SnapshotMeta, const ModuleCriteria& p_ModuleCriteria, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
		{
			const bool isMyData = p_SnapshotMeta.m_Flags && GridSnapshot::Flags::MyData == (GridSnapshot::Flags::MyData & *p_SnapshotMeta.m_Flags);
			const wstring title = p_ModuleCriteria.m_Origin == Origin::User
				? (isMyData
					? _T("My OneDrive Files")
					: YtriaTranslate::Do(ModuleDrive_showDriveItems_1, _YLOC("OneDrive Files")).c_str())
				: YtriaTranslate::Do(ModuleDrive_showDriveItems_3, _YLOC("Files")).c_str();

			return new FrameDriveItems(p_ModuleCriteria.m_Origin, isMyData, p_LicenseContext, p_SessionIdentifier, title, p_Module, p_HistoryMode, p_PreviousFrame);
		});
}

boost::YOpt<wstring> ModuleDrive::downloadStreamingFile(web::http::http_response p_Response, const DriveItemDownloadInfo& p_DownloadInfo, YtriaTaskData p_TaskData, std::function<void(int32_t p_ProgressPercent)> p_ChunkDownloadedCallback)
{
    boost::YOpt<wstring> retVal;

	// If task was in queue but has been canceled, don't create the file
	if (p_TaskData.IsCanceled())
	{
		p_Response.body().streambuf().close(); // Stop the download
		return retVal;
	}

	wstring fullPath = p_DownloadInfo.m_Filepath;

	if (!p_DownloadInfo.m_OwnerName.IsEmpty())
	{
		if (!Str::endsWith(fullPath, _YTEXT("\\")) && fullPath.size() > 1)
			fullPath += _YTEXT("\\");

		fullPath += p_DownloadInfo.m_OwnerName;
		fullPath += _YTEXT("\\");
	}

	if (p_DownloadInfo.m_PreserveHierarchy)
	{
		if (Str::endsWith(fullPath, _YTEXT("\\")))
		{
			if (fullPath.size() > 1)
				fullPath = wstring(fullPath.begin(), fullPath.end() - 1);
		}
		fullPath += Str::Replace(p_DownloadInfo.m_DrivePath, _YTEXT("/"), _YTEXT("\\"));
	}

	if (!FileUtil::FileExists(fullPath))
	{
		bool pathCreated = FileUtil::CreateFolderHierarchy(fullPath);
		ASSERT(pathCreated);
		if (!pathCreated)
			return retVal;
	}

	fullPath += p_DownloadInfo.m_Filename;

	if (p_DownloadInfo.m_FileExistsAction == FileExistsAction::Skip && FileUtil::FileExists2(fullPath))
	{
		retVal = fullPath;
		return retVal;
	}

	auto contentLength = p_Response.headers().content_length();

	pplx::streams::istream bodyStream = p_Response.body();
	pplx::streams::container_buffer<std::string> fileBuffer;

	wstring tempPath;
	FileUtil::GetYtriaTempDirPath(tempPath);

	wchar_t tempFileName[MAX_PATH];

	if (::GetTempFileName(wstring(tempPath + wstring(_YTEXT("\\"))).c_str(), _YTEXT(""), 0, tempFileName) == 0)
		return retVal;

	std::ofstream diskFile(tempFileName, std::ofstream::binary);

	size_t bytesRead = 0;

	int32_t progressPercent = 0;

	while (bytesRead < contentLength && !p_TaskData.IsCanceled())
	{
		static const size_t CHUNK_SIZE = 32768;
		size_t bytesToRead = 0;
		if (contentLength - bytesRead >= CHUNK_SIZE)
			bytesToRead = CHUNK_SIZE;
		else
			bytesToRead = static_cast<size_t>(contentLength - bytesRead);

		size_t read = bodyStream.read(fileBuffer, bytesToRead).get();

		int32_t previousPercent = progressPercent;
		progressPercent = static_cast<int32_t>((static_cast<float>(bytesRead) / contentLength) * 100.0f);
		if (progressPercent != previousPercent || progressPercent == 100)
			p_ChunkDownloadedCallback(progressPercent);

		diskFile << fileBuffer.collection();
		diskFile.flush();
		fileBuffer = pplx::streams::container_buffer<std::string>(); // TODO: Improve this by not creating a container_buffer on every iteration

		bytesRead += read;
	}

	diskFile.close();
	if (p_TaskData.IsCanceled())
	{
		p_Response.body().streambuf().close();
		::DeleteFile(tempFileName); // Delete temp file if download is interrupted
		pplx::cancel_current_task();
	}
	else
	{
		if (p_DownloadInfo.m_FileExistsAction == FileExistsAction::Append)
		{
			fullPath = FileUtil::GetNoOverwriteFileName(fullPath);
		}
		else if (p_DownloadInfo.m_FileExistsAction == FileExistsAction::Overwrite)
		{
			if (FileUtil::FileExists2(fullPath))
				::DeleteFile(fullPath.c_str());
		}

		BOOL success = ::MoveFile(tempFileName, fullPath.c_str());
		ASSERT(success);
		if (success)
		{
			retVal = fullPath.c_str();
			SetCreatedAndModifiedTime(p_DownloadInfo, fullPath);
		}
	}

    return retVal;
}

void ModuleDrive::SetCreatedAndModifiedTime(const DriveItemDownloadInfo &p_DownloadInfo, const wstring & p_FinalFullPath)
{
    SYSTEMTIME createdSystemTime, modifiedSystemTime;
    ::GetSystemTime(&createdSystemTime);
    ::GetSystemTime(&modifiedSystemTime);
    createdSystemTime.wYear = p_DownloadInfo.m_CreatedDateTime.getYear();
    createdSystemTime.wMonth = p_DownloadInfo.m_CreatedDateTime.getMonth();
    createdSystemTime.wDay = p_DownloadInfo.m_CreatedDateTime.getDay();
    createdSystemTime.wHour = p_DownloadInfo.m_CreatedDateTime.getHour();
    createdSystemTime.wMinute = p_DownloadInfo.m_CreatedDateTime.getMin();
    createdSystemTime.wSecond = p_DownloadInfo.m_CreatedDateTime.getSec();

    modifiedSystemTime.wYear = p_DownloadInfo.m_ModifiedDateTime.getYear();
    modifiedSystemTime.wMonth = p_DownloadInfo.m_ModifiedDateTime.getMonth();
    modifiedSystemTime.wDay = p_DownloadInfo.m_ModifiedDateTime.getDay();
    modifiedSystemTime.wHour = p_DownloadInfo.m_ModifiedDateTime.getHour();
    modifiedSystemTime.wMinute = p_DownloadInfo.m_ModifiedDateTime.getMin();
    modifiedSystemTime.wSecond = p_DownloadInfo.m_ModifiedDateTime.getSec();

    HANDLE fileHandle = ::CreateFile(p_FinalFullPath.c_str(),
        FILE_WRITE_ATTRIBUTES,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL);

    FILETIME createdTime, modifiedTime;
    ::SystemTimeToFileTime(&createdSystemTime, &createdTime);
    ::SystemTimeToFileTime(&modifiedSystemTime, &modifiedTime);

    BOOL success = ::SetFileTime(fileHandle, &createdTime, &modifiedTime, &modifiedTime);
    ASSERT(success);
	::CloseHandle(fileHandle);
}

template<>
std::shared_ptr<O365DataMap<BusinessUser, BusinessDriveItemFolder>>
ModuleDrive::retrieveDriveItems<BusinessUser>(std::shared_ptr<BusinessObjectManager> p_BOM, const O365IdsContainer& p_IDs, const boost::YOpt<wstring>& p_SearchCriteria, Origin p_Origin, bool p_LoadFromSQlCache, YtriaTaskData p_TaskData)
{
	ASSERT(Origin::User == p_Origin || Origin::DeletedUser == p_Origin);
	ASSERT(!p_LoadFromSQlCache || Origin::User == p_Origin);

	auto driveItems = std::make_shared<O365DataMap<BusinessUser, BusinessDriveItemFolder>>();

	auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("drive"), p_IDs.size());

	auto processUser = [&p_BOM, &p_TaskData, &driveItems, &logger, &p_SearchCriteria](auto& user)
	{

		logger->SetLogMessage(_T("drive"));

		if (p_TaskData.IsCanceled())
		{
			user.SetFlags(user.GetFlags() | BusinessObject::Flag::CANCELED);
			(*driveItems)[user] = {};
			return;
		}

		auto result = p_BOM->GetBusinessUserDrive(user.GetID(), logger, p_TaskData).Then([p_TaskData, &user, p_BOM, &logger, &p_SearchCriteria](const BusinessDrive& p_BusinessDrive)
			{
				if (p_BusinessDrive.GetError())
				{
					BusinessDriveItemFolder businessDriveItem(true);
					businessDriveItem.SetError(p_BusinessDrive.GetError());
					return businessDriveItem;
				}

				logger->SetLogMessage(_T("drive items"));
				return p_BOM->GetBusinessDriveItems(p_BusinessDrive.GetID(), p_SearchCriteria, std::make_shared<MultiObjectsPageRequestLogger>(*logger), p_TaskData).GetTask().get();
			}).GetTask().get();

		(*driveItems)[user] = result;
	};

	Util::ProcessSubUserItems(processUser, p_IDs, p_Origin, p_BOM->GetGraphCache(), p_LoadFromSQlCache, logger, p_TaskData);

	return driveItems;
}

template<>
std::shared_ptr<O365DataMap<BusinessGroup, BusinessDriveItemFolder>>
ModuleDrive::retrieveDriveItems<BusinessGroup>(std::shared_ptr<BusinessObjectManager> p_BOM, const O365IdsContainer& p_IDs, const boost::YOpt<wstring>& p_SearchCriteria, Origin p_Origin, bool p_LoadFromSQlCache, YtriaTaskData p_TaskData)
{
	ASSERT(Origin::Group == p_Origin);

	auto driveItems = std::make_shared<O365DataMap<BusinessGroup, BusinessDriveItemFolder>>();

	vector<BusinessGroup> groups;
	if (p_LoadFromSQlCache)
		groups = p_BOM->GetGraphCache().GetSqlCachedGroups(p_TaskData.GetOriginator(), p_IDs, false, p_TaskData);

	auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("drive"), p_IDs.size());

	auto processGroup = [&p_BOM, &p_TaskData, &driveItems, &logger, &p_SearchCriteria](auto& group)
	{
		logger->IncrementObjCount();
		logger->SetContextualInfo(GetDisplayNameOrId(group));

		logger->SetLogMessage(_T("drive"));

		if (p_TaskData.IsCanceled())
		{
			group.SetFlags(group.GetFlags() | BusinessObject::Flag::CANCELED);
			(*driveItems)[group] = {};
			return;
		}

		auto result = p_BOM->GetBusinessGroupDrive(group.GetID(), logger, p_TaskData).Then([p_TaskData, &group, p_BOM, &logger, &p_SearchCriteria](const BusinessDrive& p_BusinessDrive)
			{
				if (p_BusinessDrive.GetError())
				{
					BusinessDriveItemFolder businessDriveItem(true);
					businessDriveItem.SetError(p_BusinessDrive.GetError());
					return businessDriveItem;
				}

				logger->SetLogMessage(_T("drive items"));
				return p_BOM->GetBusinessDriveItems(p_BusinessDrive.GetID(), p_SearchCriteria, std::make_shared<MultiObjectsPageRequestLogger>(*logger), p_TaskData).GetTask().get();
			}).GetTask().get();

		(*driveItems)[group] = result;
	};

	auto ids = p_IDs;
	for (auto& group : groups)
	{
		if (1 == ids.erase(group.GetID()))
			processGroup(group);
	}

	for (const auto& id : ids)
	{
		BusinessGroup group;
		group.SetID(id);
		logger->SetContextualInfo(p_BOM->GetGraphCache().GetGroupContextualInfo(id));
		p_BOM->GetGraphCache().SyncUncachedGroup(group, logger, p_TaskData);

		processGroup(group);
	}

	return driveItems;
}

template<>
std::shared_ptr<O365DataMap<BusinessSite, BusinessDriveItemFolder>>
ModuleDrive::retrieveDriveItems<BusinessSite>(std::shared_ptr<BusinessObjectManager> p_BOM, const O365IdsContainer& p_IDs, const boost::YOpt<wstring>& p_SearchCriteria, Origin p_Origin, bool p_LoadFromSQlCache, YtriaTaskData p_TaskData)
{
	ASSERT(!p_LoadFromSQlCache); // Only for users and groups for now.

	auto driveItems = std::make_shared<O365DataMap<BusinessSite, BusinessDriveItemFolder>>();

	auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("drive"), p_IDs.size());

	for (const auto& id : p_IDs)
	{
		ASSERT(Origin::Site == p_Origin);

		BusinessSite site;
		site.SetID(id);
		logger->SetContextualInfo(p_BOM->GetGraphCache().GetSiteContextualInfo(id));
		p_BOM->GetGraphCache().SyncUncachedSite(site, logger, p_TaskData);

		logger->IncrementObjCount();
		logger->SetContextualInfo(GetDisplayNameOrId(site));

		if (p_TaskData.IsCanceled())
		{
			site.SetFlags(site.GetFlags() | BusinessObject::Flag::CANCELED);
			(*driveItems)[site] = {};
		}
		else
		{
			logger->SetLogMessage(_T("drive"));
			auto result = p_BOM->GetDefaultSiteDrive(site.GetID(), logger, p_TaskData).Then([p_TaskData, &site, p_BOM, &logger, &p_SearchCriteria](const BusinessDrive& p_BusinessDrive)
			{
				logger->SetLogMessage(_T("drive items"));
				if (p_BusinessDrive.GetError())
				{
					BusinessDriveItemFolder businessDriveItem(true);
					businessDriveItem.SetError(p_BusinessDrive.GetError());
					return std::make_pair(site, businessDriveItem);
				}
				else
				{
					auto rootFolder = p_BOM->GetBusinessDriveItems(p_BusinessDrive.GetID(), p_SearchCriteria, std::make_shared<MultiObjectsPageRequestLogger>(*logger), p_TaskData).GetTask().get();
					return std::make_pair(site, rootFolder);
				}
			}).GetTask().get();

			(*driveItems)[result.first] = result.second;
		}
	}

	return driveItems;
}

template<class BusinessType>
void ModuleDrive::showDriveItemsIds(const O365IdsContainer& p_Ids, const boost::YOpt<wstring>& p_SearchCriteria, const DriveSubInfoIds& p_PermissionRequestInfo, const DriveSubInfoIds& p_CheckoutStatusRequestInfo, const bool p_PartialRefresh, const Origin p_Origin, const std::shared_ptr<BusinessObjectManager>& p_BOM, HWND p_Hwnd, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsRefresh, bool p_LoadFromSQlCache)
{
	const auto driveItems = retrieveDriveItems<BusinessType>(p_BOM, p_Ids, p_SearchCriteria, p_Origin, p_LoadFromSQlCache, p_TaskData);

	showDriveItemsCommonImpl(driveItems, p_PermissionRequestInfo, p_CheckoutStatusRequestInfo, p_PartialRefresh, p_Origin, p_BOM, p_Hwnd, p_Action, p_TaskData, p_IsRefresh, {});
}

void ModuleDrive::showDriveItemsSubIds(const O365SubIdsContainer& p_SubIds, const DriveSubInfoIds& p_PermissionRequestInfo, const DriveSubInfoIds& p_CheckoutStatusRequestInfo, const bool p_PartialRefresh, const Origin p_Origin, const std::shared_ptr<BusinessObjectManager>& p_BOM, HWND p_Hwnd, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsRefresh, const std::map<PooledString, PooledString>& p_ChannelNames)
{
	ASSERT(p_Origin == Origin::Channel);

	auto driveItems = std::make_shared<O365DataMap<BusinessChannel, BusinessDriveItemFolder>>();

	ASSERT(!p_SubIds.empty());

	auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("drive"), p_SubIds.size());
	for (auto& team : p_SubIds)
	{
		const auto& teamID = team.first.first;

		BusinessChannel channel;
		channel.SetGroupId(teamID);

		for (auto& channelID : team.second)
		{
			logger->IncrementObjCount();
			channel.SetID(channelID);
			auto it = p_ChannelNames.find(channelID);
			ASSERT(it != p_ChannelNames.end());
			if (it != p_ChannelNames.end())
				channel.SetDisplayName(it->second);
			else
				channel.SetDisplayName(boost::none);
			if (channel.GetDisplayName())
				logger->SetContextualInfo(*channel.GetDisplayName());
			else
				logger->SetContextualInfo(channelID);

			// Do we want to request the channel?
			/*auto channelRequester = std::make_shared<ChannelRequester>(teamID, channelID, logger);
			channel = safeTaskCall(channelRequester->Send(bom->GetSapio365Session(), taskData).Then([channelRequester]()
				{
					return channelRequester->GetData();
				}), bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::RBAC_USER), Util::ObjectErrorHandler<ChannelRequester>, taskData.GetOriginator()).get();*/

			if (!channel.GetError())
			{
				auto loadMoreRequester = std::make_shared<ChannelLoadMoreRequester>(channel.GetGroupId(), channel.GetID(), channel.GetMembershipType(), ChannelLoadMoreRequester::REQUEST_DRIVE, logger);
				auto result = loadMoreRequester->Send(p_BOM->GetSapio365Session(), p_TaskData).Then([&channel, p_TaskData, &loadMoreRequester, p_BOM, &logger]()
					{
						channel.SetDrive(loadMoreRequester->GetData().GetDrive());
						const auto& drive = channel.GetDrive();
						ASSERT(drive);
						if (!drive || drive->GetError())
						{
							BusinessDriveItemFolder businessDriveItem(true);
							if (drive)
								businessDriveItem.SetError(drive->GetError());
							return businessDriveItem;
						}

						logger->SetLogMessage(_T("drive items"));
						return p_BOM->GetBusinessDriveItems(channel.GetDrive()->GetID(), boost::none, std::make_shared<MultiObjectsPageRequestLogger>(*logger), p_TaskData).GetTask().get();
					}).GetTask().get();

				(*driveItems)[channel] = result;
			}
		}
	}

	showDriveItemsCommonImpl(driveItems, p_PermissionRequestInfo, p_CheckoutStatusRequestInfo, p_PartialRefresh, p_Origin, p_BOM, p_Hwnd, p_Action, p_TaskData, p_IsRefresh, p_ChannelNames);
}

template<class BusinessType>
void ModuleDrive::showDriveItemsCommonImpl(const std::shared_ptr<O365DataMap<BusinessType, BusinessDriveItemFolder>>& p_DriveItems, const DriveSubInfoIds& p_PermissionRequestInfo, const DriveSubInfoIds& p_CheckoutStatusRequestInfo, const bool p_PartialRefresh, const Origin p_Origin, const std::shared_ptr<BusinessObjectManager>& p_BOM, HWND p_Hwnd, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsRefresh, const std::map<PooledString, PooledString>& p_ChannelNames)
{
	auto permissions = retrievePermissions(p_BOM, p_PermissionRequestInfo, p_TaskData);
	auto checkoutStatuses = retrieveCheckoutStatus(p_BOM, p_CheckoutStatusRequestInfo, p_TaskData);

	YDataCallbackMessage<std::shared_ptr<O365DataMap<BusinessType, BusinessDriveItemFolder>>>::DoPost(p_DriveItems, [hwnd = p_Hwnd, partialRefresh = p_PartialRefresh, taskData = p_TaskData, p_Action, permissions, checkoutStatuses, p_IsRefresh, isCanceled = p_TaskData.IsCanceled(), p_Origin, p_ChannelNames](const std::shared_ptr<O365DataMap<BusinessType, BusinessDriveItemFolder>>& p_BusinessDriveItems)
	{
		bool shouldFinishTask = true;
		auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameDriveItems*>(CWnd::FromHandle(hwnd)) : nullptr;
		if (ShouldBuildView(hwnd, isCanceled, taskData, false))
		{
			ASSERT(nullptr != frame);
			if (p_Origin == Origin::User)
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting drive items for %s users...", Str::getStringFromNumber(p_BusinessDriveItems->size()).c_str()));
			else if (p_Origin == Origin::DeletedUser)
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting drive items for %s deleted users...", Str::getStringFromNumber(p_BusinessDriveItems->size()).c_str()));
			else if (p_Origin == Origin::Group)
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting drive items for %s groups...", Str::getStringFromNumber(p_BusinessDriveItems->size()).c_str()));
			else if (p_Origin == Origin::Site)
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting drive items for %s sites...", Str::getStringFromNumber(p_BusinessDriveItems->size()).c_str()));
			else if (p_Origin == Origin::Channel)
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting drive items for %s channels...", Str::getStringFromNumber(p_BusinessDriveItems->size()).c_str()));
			else
				ASSERT(false);
			frame->GetGrid().ClearLog(taskData.GetId());
			//frame->GetGrid().ClearStatus();

			{
				CWaitCursor _;
				if (frame->HasLastUpdateOperations())
				{
					frame->ShowDriveItem(*p_BusinessDriveItems, frame->GetLastUpdateOperations(), true, !partialRefresh, permissions, checkoutStatuses);
					frame->ForgetLastUpdateOperations();
				}
				else
				{
					frame->ShowDriveItem(*p_BusinessDriveItems, {}, true, !partialRefresh, permissions, checkoutStatuses);
				}

				if (p_Origin == Origin::Channel)
					frame->SetChannelNames(p_ChannelNames); // Store this for next refresh
			}

			if (!p_IsRefresh)
			{
				frame->UpdateContext(taskData, hwnd);
				shouldFinishTask = false;
			}
		}

		if (shouldFinishTask)
		{
			// Because we didn't (and can't) call UpdateContext here.
			ModuleBase::TaskFinished(frame, taskData, p_Action);
		}
	});
}

ModuleDrive::AllPermissions ModuleDrive::retrievePermissions(std::shared_ptr<BusinessObjectManager> p_BOM, const DriveSubInfoIds& p_RequestInfo, YtriaTaskData p_TaskData)
{
    AllPermissions allPermissions;

	if (!p_RequestInfo.empty())
	{
		auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("permissions"), p_RequestInfo.size());

		for (const auto& info : p_RequestInfo)
		{
			if (p_TaskData.IsCanceled())
				break;

			const auto& driveId = std::get<1>(info);
			const auto& driveItemId = std::get<2>(info);
			const auto& driveItemName = std::get<3>(info);

			logger->IncrementObjCount();
			logger->SetContextualInfo(driveItemName);

			auto taskResult = p_BOM->GetBusinessPermissions(driveId, driveItemId, logger, p_TaskData).Then([info](const vector<BusinessPermission>& p_Permissions)
				{
					return std::make_pair(std::make_pair(std::get<0>(info), std::get<2>(info)), p_Permissions);
				}).GetTask().get();
				allPermissions[taskResult.first] = taskResult.second;
		}
	}

    return allPermissions;
}

ModuleDrive::CheckoutStatuses ModuleDrive::retrieveCheckoutStatus(std::shared_ptr<BusinessObjectManager> p_BOM, const DriveSubInfoIds& p_RequestInfo, YtriaTaskData p_TaskData)
{
	CheckoutStatuses checkoutStatuses;

	if (!p_RequestInfo.empty())
	{
		auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("checkout status"), p_RequestInfo.size());

		// Do request for every id
		for (const auto& id : p_RequestInfo)
		{
			const wstring& metaId = std::get<0>(id);
			const wstring& driveId = std::get<1>(id);
			const wstring& driveItemId = std::get<2>(id);
			const wstring& driveItemName = std::get<3>(id);

			logger->IncrementObjCount();
			logger->SetContextualInfo(driveItemName);

			auto requester = std::make_shared<CheckoutStatusRequester>(driveId, driveItemId, logger);
			auto checkoutStatus = safeTaskCall(requester->Send(p_BOM->GetSapio365Session(), p_TaskData).Then([requester]()
				{
					return requester->GetCheckoutStatus();
				})
			, p_BOM->GetSapio365Session()->GetMSGraphSession(Sapio365Session::APP_SESSION)
			, Util::ObjectErrorHandler<CheckoutStatus>
			, p_TaskData).get();

			if (p_TaskData.IsCanceled())
				break;

			checkoutStatuses[{metaId, driveItemId}] = checkoutStatus;
		}
	}

	return checkoutStatuses;
}
