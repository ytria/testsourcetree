#include "DlgSigninsModuleOptions.h"

void DlgSigninsModuleOptions::generateJSONScriptData()
{
	initMain(_YTEXT("DlgModuleOptions"));

	getGenerator().addIconTextField(_YTEXT("iconText"), _T("Limit the number of sign-ins to load by selecting a date/time range. You can request more later."), _YTEXT("fas fa-calendar-alt"));
	addCutOffEditor(_T("Get sign-ins started within: "), _T("Started after (excluding)"));
	getGenerator().addIconTextField(_YTEXT("iconText2"), _T("Refine your selection"), _YTEXT("fas fa-search"));
	addFilterEditor();

	getGenerator().addApplyButton(YtriaTranslate::Do(CDlg_OnInitDialog_2, _YLOC("Ok")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str());
}

wstring DlgSigninsModuleOptions::getDialogTitle() const
{
	return _T("Load Sign-ins - Options");
}

std::map<wstring, DlgModuleOptions::FilterFieldProperties> DlgSigninsModuleOptions::getFilterFields() const
{
	// taken here: https://docs.microsoft.com/en-us/graph/api/signin-list?view=graph-rest-1.0&tabs=http#attributes-supported-by-filter-parameter
	return
	{
		{ _YTEXT("id"), { YtriaTranslate::Do(O365Grid_init_9, _YLOC("Graph ID")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } }, //	eq
		{ _YTEXT("userId"), { YtriaTranslate::Do(GridSignIns_customizeGrid_37, _YLOC("User ID")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } }, //	eq
		{ _YTEXT("appId"), { YtriaTranslate::Do(GridSignIns_customizeGrid_2, _YLOC("Application ID")).c_str(),  true, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } }, //	eq

		// FIXME: Our DateTime editor is not good, better with Date (with calendar) but that prevent from using EQ...
		//{ _YTEXT("createdDateTime"), { YtriaTranslate::Do(GridSignIns_customizeGrid_11, _YLOC("Date")).c_str(), false, FilterFieldProperties::DateTimeType(), g_FilterOperatorsEQGELE } }, //	eq, le, ge
		{ _YTEXT("createdDateTime"), { YtriaTranslate::Do(GridSignIns_customizeGrid_11, _YLOC("Date")).c_str(), false, FilterFieldProperties::DateType(), g_FilterOperatorsGELE } }, //	le, ge

		{ _YTEXT("userDisplayName"), { YtriaTranslate::Do(GridSignIns_customizeGrid_36, _YLOC("User display name")).c_str(),  true, FilterFieldProperties::StringType(), g_FilterOperatorsEQStartsWith } }, //	eq, startswith
		{ _YTEXT("userPrincipalName"), { YtriaTranslate::Do(GridSignIns_customizeGrid_38, _YLOC("Username")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQStartsWith } }, //	eq, startswith
		{ _YTEXT("appDisplayName"), { YtriaTranslate::Do(GridSignIns_customizeGrid_1, _YLOC("Application")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQStartsWith } }, //	eq, startswith
		{ _YTEXT("ipAddress"), { YtriaTranslate::Do(GridSignIns_customizeGrid_19, _YLOC("IP Address")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQStartsWith } }, //	eq, startswith
		{ _YTEXT("location/city"), { YtriaTranslate::Do(GridSignIns_customizeGrid_21, _YLOC("City")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQStartsWith } }, //	eq, startswith
		{ _YTEXT("location/state"), { YtriaTranslate::Do(GridSignIns_customizeGrid_26, _YLOC("State")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQStartsWith } }, //	eq, startswith
		{ _YTEXT("location/countryOrRegion"), { YtriaTranslate::Do(GridSignIns_customizeGrid_22, _YLOC("Country")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQStartsWith } }, //	eq, startswith
		{ _YTEXT("status/errorCode"), { YtriaTranslate::Do(GridSignIns_customizeGrid_42, _YLOC("Status error code")).c_str(), false, FilterFieldProperties::IntegerType(), g_FilterOperatorsEQ } }, //	eq
		// ???? //{ _YTEXT("initiatedBy/user/id"), true }, //	eq -- In doc, but doesn't work
		// ???? //{ _YTEXT("initiatedBy/user/displayName"), true }, //	eq -- In doc, but doesn't work
		// ???? //{ _YTEXT("initiatedBy/user/userPrincipalName"), true }, //	eq, startswith -- In doc, but doesn't work
		{ _YTEXT("clientAppUsed"), { YtriaTranslate::Do(GridSignIns_customizeGrid_8, _YLOC("Client app")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } }, //	eq
		{ _YTEXT("conditionalAccessStatus"), {  YtriaTranslate::Do(GridSignIns_customizeGrid_9, _YLOC("Conditional Access")).c_str(), true, FilterFieldProperties::EnumType({ { _YTEXT("success"), _T("Success (0)") }, { _YTEXT("failure"), _T("Failure (1)") }, { _YTEXT("notApplied"), _T("Not Applied (2)") }, { _YTEXT("unknownFutureValue"), _T("Unknown Future Value (3)") } }), g_FilterOperatorsEQ } },	// (enum)
		{ _YTEXT("deviceDetail/browser"), { YtriaTranslate::Do(GridSignIns_customizeGrid_12, _YLOC("Browser")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQStartsWith } }, //	eq, startswith
		{ _YTEXT("deviceDetail/operatingSystem"), { YtriaTranslate::Do(GridSignIns_customizeGrid_17, _YLOC("Operating system")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQStartsWith } }, //	eq, startswith
		{ _YTEXT("correlationId"), { YtriaTranslate::Do(GridSignIns_customizeGrid_10, _YLOC("Request ID sent")).c_str(), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } }, //	eq
		{ _YTEXT("isRisky"), { _T("Is Risky"), false, FilterFieldProperties::BoolType(), g_FilterOperatorsEQ } }, // bool, eq -- Not a sign-in property, but works :-o
	};
}
