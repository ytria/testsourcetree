#pragma once

#include "GridBackendColumn.h"
#include "GridBackendUtil.h"

class MetaDataColumnInfo
{
public:
	MetaDataColumnInfo(GridBackendColumn* p_SrcColumn); // Use Unique ID as property name.
	MetaDataColumnInfo(GridBackendColumn* p_SrcColumn, const PooledString& p_PropertyName);

	MetaDataColumnInfo(); // Only for deserialization.

	GridBackendColumn* AddTo(CacheGrid& p_Grid, const wstring& p_Family) const;
	GridBackendColumn* AddTo(CacheGrid& p_Grid, const wstring& p_Family, const wstring& p_TitleFormat, const wstring& p_UniqueIDPrefix) const;

	const PooledString& GetPropertyName() const;

	bool IsLoadMore() const;

	// Only checks m_PropertyName equality.
	bool operator==(const MetaDataColumnInfo& p_Other) const;

	web::json::value Serialize() const;
	static boost::YOpt<MetaDataColumnInfo> Deserialize(const web::json::value& p_Serialized);

private:
	PooledString m_PropertyName;
	PooledString m_Title;
	int m_DefaultWidth/* = GridBackendUtil::g_DefaultColWidth*/;
	int m_MinWidth/* = DEFAULTMINIMUMCOLUMNWIDTH*/;
	GridBackendUtil::DATATYPE m_DataType;
	GridBackendUtil::GRIDCELLTYPE m_CellType;
	GridBackendUtil::GRIDCELLTYPE m_GroupCellType;
	boost::YOpt<ColumnFormat> m_CellFormat;
	boost::YOpt<ColumnFormat> m_GroupFormat;
	bool m_IsCaseSensitive;
	bool m_IsLoadMore;
};

using MetaDataColumnInfos = vector<MetaDataColumnInfo>;
enum class Origin;
class MetaDataColumnInfosSetting
{
public:
	MetaDataColumnInfosSetting(const Origin& p_Origin);

	bool ShouldAskAgain() const;
	boost::YOpt<MetaDataColumnInfos> Get(const CacheGrid& p_SourceGrid, std::function<bool(GridBackendColumn*)> p_ColumnFilter) const;
	bool Set(const boost::YOpt<MetaDataColumnInfos>& p_MetaDataColumnInfos, const boost::YOpt<bool>& p_DontAskAgain);

	Origin GetOrigin() const;

private:
	bool exists() const;

	boost::YOpt<vector<wstring>> get() const;
	const wstring m_RegistryKey;
	const wstring m_RegistryValue;
};
