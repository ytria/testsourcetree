#pragma once

#include "IRequester.h"

#include "BusinessMailFolder.h"
#include "MailFolderDeserializer.h"
#include "ValueListDeserializer.h"
#include "IPageRequestLogger.h"

class MailFoldersTopLevelRequester : public IRequester
{
public:
	using DeserializerType = ValueListDeserializer<BusinessMailFolder, MailFolderDeserializer>;
	MailFoldersTopLevelRequester(const wstring& p_UserId, const std::shared_ptr<IPageRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	vector<BusinessMailFolder>& GetData();

private:
	shared_ptr<DeserializerType> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;
	wstring m_UserId;
};

