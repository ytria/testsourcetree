#include "CachedSite.h"

#include "MsGraphFieldNames.h"

RTTR_REGISTRATION
{
    using namespace rttr;

registration::class_<CachedSite>("CachedSite") (metadata(MetadataKeys::TypeDisplayName, _YTEXT("Cached Site")))
.constructor()(policy::ctor::as_object)
.property("displayName", &CachedSite::m_DisplayName)
.property("hierarchyDisplayName", &CachedSite::m_HierarchyDisplayName)
.property("webUrl", &CachedSite::m_HierarchyDisplayName);
}

CachedSite::CachedSite(const Site& site)
	: CachedObject(site.Id)
	, m_DisplayName(site.DisplayName)
    , m_HierarchyDisplayName(site.DisplayName ? *site.DisplayName : _YTEXT(""))
    , m_Name(site.Name)
    , m_WebUrl(site.WebUrl)
{
}

CachedSite::CachedSite(const BusinessSite& site)
	: CachedObject(site.m_Id)
    , m_DisplayName(site.GetDisplayName())
    , m_Name(site.GetName())
    , m_WebUrl(site.GetWebUrl())
    , m_HierarchyDisplayName(site.GetDisplayName() ? *site.GetDisplayName() : _YTEXT(""))
{
	SetRBACDelegationID(site.GetRBACDelegationID());
}

const PooledString& CachedSite::GetHierarchyDisplayName() const
{
    return m_HierarchyDisplayName;
}

void CachedSite::SetHierarchyDisplayName(const PooledString& p_Val)
{
    m_HierarchyDisplayName = p_Val;
}

const boost::YOpt<PooledString>& CachedSite::GetWebUrl() const
{
    return m_WebUrl;
}

PooledString CachedSite::GetDisplayName() const
{
	return m_DisplayName ? m_DisplayName.get() : _YTEXT("");
}

const boost::YOpt<PooledString>& CachedSite::GetName() const
{
	return m_Name;
}
