#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"
#include "AutomatedApp.h"
#include "IBrowserLinkHandler.h"
#include "Sapio365Session.h"
#include "PersistentSession.h"
#include "MSGraphConsentListener.h"

class Command;
class CodeJockFrameBase;
class GridFrameBase;
class MSGraphConsentListener;
class Sapio365Session;
class SessionsSqlEngine;
class PersistentSession;
class DlgLoading;
class O365LicenseValidator;

enum class LicenseTag
{
	Collaboration, // Cosmos
	CreateSnapshot,
	CreateRestorePoint,
	LoadRestorePoint,
	JobEditor,
	RBAC,
	OnPremise,
	AJL,
};

class Office365AdminApp : public AutomatedApp
{
public:
	Office365AdminApp();

	virtual wstring		GetGridConfigFolderPath() override;
	virtual CacheGrid*	getGrid(const wstring& i_GridName) override;
	virtual const wstring GetUserName() const override;// last used session user name
	virtual const wstring GetUserOrganisation() const override;
	virtual const wstring GetUserOrganisation(const wstring& i_UserName) override;
	virtual const wstring GetYtriaProductPath(const wstring& p_TargetAppName) override;
	virtual const wstring GetApplicationColor() const override;
	virtual const wstring GetApplicationSlogan() const override;

    bool GetConsent(const web::uri& p_BrowserUri, const wstring& p_SessionType, CWnd* p_Parent, const wstring& p_ConsentDlgTitle = _YTEXT(""));
	bool GetConsent(const web::uri& p_BrowserUri, const wstring& p_SessionType, CWnd* p_Parent, const std::function<void(ConsentResult)>& p_ConsentResultCallback, const wstring& p_ConsentDlgTitle);

    void CreateStandardSession(const boost::YOpt<Command>& p_Command, const wstring& p_PreferredLogin = Str::g_EmptyString);
	void CreateAdvancedSession(const boost::YOpt<Command>& p_Command, const wstring& p_PreferredLogin = Str::g_EmptyString);
	void CreatePartnerSession(const boost::YOpt<Command>& p_Command, const wstring& p_CustomerTenant, const wstring& p_PreferredLogin = Str::g_EmptyString);
    void CreateUltraAdminSession(const wstring& p_Tenant, const wstring& p_ClientId, const wstring& p_ClientSecret, const wstring& p_DisplayName, const boost::YOpt<Command>& p_Command, shared_ptr<Sapio365Session> p_Session);
	
	void ConnectPartnerToCustomer(std::shared_ptr<Sapio365Session> p_Session, const wstring& p_CustomerTenant);

	static std::pair<bool, wstring> RegenerateAppPassword(CWnd* p_Parent, const wstring& p_AppId, const std::shared_ptr<Sapio365Session>& p_Session, const shared_ptr<DlgLoading>& dlgLoading);
	std::pair<bool, wstring> TestElevatedSession(const std::shared_ptr<Sapio365Session>& p_Session, MainFrame& p_Mainframe, const PersistentSession& p_PersistentSession, bool p_TestWithDialog);

	/*** WARNING: This must only be used durring session login, this is the way to know of the session being loaded is FullAmin.***/
	/*** In other cases, use _MyFrame_->GetSessionInfo().m_SavedInfo.isFulladmin()***/
    bool IsSessionBeingLoadedUltraAdmin() const;

	int64_t GetSessionBeingLoadedRoleID() const;
	const wstring& GetSessionBeingLoadedSessionType() const;

    const wstring& GetSessionType() const;
	void SetSessionType(const wstring& p_SessionType);

	TaskWrapper<bool> LoadSessionFromAutomationOrCommandLine(const wstring& p_SessionNameX, const wstring& p_SessionType, const wstring& p_RoleNameX, AutomationAction* p_Action = nullptr);

	TaskWrapper<std::shared_ptr<Sapio365Session>> RelogElevatedSession(const std::shared_ptr<Sapio365Session>& p_SapioSession, const PersistentSession& p_PersistentSession, const SessionIdentifier& p_SessionIdentifier, bool p_FromModule, CFrameWnd* p_ParentFrame, bool p_RelogUltraAdmin = true);

	void ShowUltraAdminForElevatedSessionAuth(PersistentSession& persistentSession, std::shared_ptr<Sapio365Session> sapioSession, CFrameWnd* p_ParentFrame);

	void SetFavoriteSession(const SessionIdentifier& p_SessionIdentifier, bool isFavorite);
	bool IsFavoriteSession(const SessionIdentifier& p_SessionIdentifier, bool isFavorite);

    void InitMSGraph();

    wstring GetFullAdminTenant() const;
    void	SetFullAdminTenant(wstring val);
    wstring GetFullAdminAppId() const;
    void	SetFullAdminAppId(wstring val);
	void	SetFullAdminAppPassword(const wstring& p_Password);
	wstring GetFullAdminAppPassword() const;
	void	SetFullAdminDisplayName(const wstring& p_DisplayName);
	wstring GetFullAdminDisplayName() const;
	
    void SetShowGridErrors(bool p_ShowErrors);
    bool GetShowGridErrors();

	wstring SendFeedback(const wstring& p_Mood, const wstring& p_Message, const wstring& p_CurrentWindowTitle);
	void SuggestNewJob(CodeJockFrameBase* p_SourceFrame);

	static void SetAutomationLastLoadingError(const wstring& p_Error);

	// return true if caller should continue
	static bool ShowFeatureUnfinishedDialog(CWnd* p_Parent);

	std::shared_ptr<Sapio365Session> GetSapioSessionBeingLoaded();
	void SetSapioSessionBeingLoaded(const std::shared_ptr<Sapio365Session>& p_Session);
	void ResetSapioSessionBeingLoaded();

	bool GetIsCreatingSession() const;
	void SetIsCreatingSession(bool p_Creating);

	void AutomationPostExecuteActionProcess(bool p_AutomationCanceled) override;
	void RegisterForPostExecutionProcess(AutomationAction* p_MotherAction);

	void RunPendingCommandLineAutomation();

	static bool IsSubmoduleAction(AutomationAction* p_Action);

	static Office365AdminApp& Get();

	const boost::YOpt<SessionIdentifier>& GetSessionBeingLoadedIdentifier() const;
	void SetSessionBeingLoadedIdentifier(const boost::YOpt<SessionIdentifier>& p_Val);

	std::mutex& GetSessionTypeLock();

	template<LicenseTag licenseTag>
	static bool IsLicense();

	template<LicenseTag licenseTag>
	static wstring GetLicenseWarnMessage();

	template<LicenseTag licenseTag>
	static bool CheckAndWarn(CWnd* p_Parent);

	bool CanLicenseAJLremoveJobs() const;
	bool IsLicenseFullOrTrial(wstring& p_LicenseStatus) const;
	bool IsLicensePro() const;

	uint32_t GetAJLCapacity() const;

	struct LicenseTokenState
	{
		uint32_t available;
		uint32_t total;
	};
	// WARNING: with p_RequestAvailableCount == true, this requests available tokens (CRM)
	boost::YOpt<LicenseTokenState> GetLicenseTokenState(bool p_RequestAvailableCount) const;
	VendorLicense GetRefreshedLicense(bool p_Force = true) const;

	virtual void AutomationWizardScriptRunComplete() override;

	bool			UpdateJobSelectionIDs(const set<wstring>& p_JobIDs, CWnd* p_Parent);
	bool			UpdateJobSelectionIDs(const set<wstring>& p_JobIDs, const wstring& p_ValidationNumber);
	set<wstring>	GetAJLJobIDs() const;
	bool			IsJobAllowedByLicense(const wstring& p_JobID);

protected:
	DECLARE_MESSAGE_MAP()
	virtual void setAutomationRulesForPlatform() override;
	virtual void checkStopAutomationSpecific() override;
	virtual void getAdditionalCrashReporterFiles(map<wstring, wstring>& mandatoryFiles, map<wstring, wstring>& optionalFiles) override;

	virtual AUTOMATIONSTATUS automationProcessUserInputVar(	const wstring& p_DlgTitle, 
															const map<INT, wstring> p_BtnTexts, 
															vector<AutomationInputVariableDefinition>& p_VarDef, 
															const vector<AutomationInputVariableEvent>& p_VarEvents,
															const vector<AutomationInputVariableEvent>& p_VarEventsGlobal) override;
	virtual AUTOMATIONSTATUS automationPauseUntil(AutomationAction* i_Action) override; // platform-dependent
	virtual AUTOMATIONSTATUS automationUseDesignCollection(AutomationAction* i_Action) override; // platform-dependent - but doubtful it is used outside Notes

	virtual RecentAutomations*	createRecentAutomationManager() const override;

	virtual bool InitInstanceSpecific(const CommandLineInfo& i_cmdInfo) override; // app specific InitInstance
	void InitProductInstance() override;
	virtual void PostInitInstance();

	TaskWrapper<bool> LoadLastUsedSession(const SessionIdentifier& lastSessionUsed);

    virtual	bool ExitInstanceSpecific() override;
	virtual bool ValidateActionsSpecific(list< AutomationAction * >& i_ActionList) override; // app specific actions validation - must delete replaced actions
	virtual bool TranslateActionSpecific(AutomationAction* i_Action, list< AutomationAction* >& o_ActionList) override; // app specific script translation - must delete replaced actions
	virtual void TranslatePostProcessSpecific(list< AutomationAction * >& i_ActionList) override; // final processing after translate
	virtual void SetAutomationRulesSpecific() override;
	virtual AUTOMATIONSTATUS ExecuteAutomationAction(AutomationAction* i_Action) override;
	virtual void AutomationApplicationPreInit() override;
	virtual bool IsFilePathParamForFolderSpecific(AutomationAction * io_Action) override;
	virtual bool IsModalActionSpecific(AutomationAction* i_Action) override;
	virtual void downloadAutomationFileCallback(const wstring& p_Message) const override;

	std::pair<XTPPaintTheme, wstring> getDefaultCodejockTheme() const override;

private:
	void runCommandLineAutomation(const wstring& p_Path, bool p_IsJob);

    void createUserSession(const wstring& p_SessionType, const boost::YOpt<Command>& p_Command, const wstring& p_PreferredLogin = Str::g_EmptyString, const wstring& p_CustomTenant = Str::g_EmptyString);

	bool automationBlockInFullAdmin(AutomationAction* i_Action);	
	bool automationCheckSession(AutomationAction* p_Action);
	bool automationSetCurrentSessionToBeSetInMainFrameAfterScriptCompletion(AutomationAction* p_Action);

	virtual void	automationProcessContext() override;// set session365 from frame
	virtual void	automationInitVariablesSpecific(set<wstring, Str::keyLessInsensitive>& p_AppvariablesSystem) override;
	virtual wstring automationReplaceVarSystemSpecific(const wstring& io_Value) override;
	virtual void	createSystemLists(const wstring& p_ListName) override;
	virtual bool	isSystemList(const wstring& p_ListName) const override;
	bool			isSessionList(const wstring& p_ListName) const;
	virtual void	automationCleanup() override;
	void			cleanUpAfterExecuteAction(AutomationAction* i_Action, bool p_AutomationCanceled);
	void			addSessionLoopListAction(const PersistentSession& p_Session);
	virtual wstring automationListLabeCleanUp(const wstring& p_Label) override;
	virtual bool	automationIsNewModuleAction(const wstring& p_ActionName) const override;

	struct automationSessionVariables
	{
		wstring m_ValueForO365_List_Sessions;// e.g. g_O365_List_Sessions_Full
		wstring m_ValueForO365UserPrincipalName;
		wstring m_ValueForO365UserPrincipalEmail;
		wstring m_ValueForO365UserPrincipalType;
		wstring m_ValueForO365UserDisplayedName;
	};
	automationSessionVariables automationGetSessionVariables(const wstring& p_SessionType, const wstring& p_PrincipalName, const wstring& p_DisplayName, const wstring p_RoleName);
	automationSessionVariables automationGetSessionVariables(const shared_ptr<Sapio365Session>& p_Session);
	automationSessionVariables automationGetSessionVariables(const PersistentSession& p_Session);

	virtual std::shared_ptr<ILicenseValidator> makeLicenseValidator() override;
	
	virtual AUTOMATIONSTATUS automationSendMail(AutomationAction* i_Action) override;

	std::unique_ptr<IBrowserLinkHandler> getLinkHandler(AutomationAction* p_Action) const;

	O365LicenseValidator* getLicenseValidator() const;

    mutable std::mutex m_SessionTypeLock;
    wstring m_SessionType;

    wstring m_FullAdminTenant;
    wstring m_FullAdminAppId;
    wstring m_FullAdminAppPassword;
	wstring m_FullAdminDisplayName;

    bool m_ShowGridErrors;

	set<AutomationAction*> m_Coagulator;

	wstring m_RoleNameToFind;

	std::shared_ptr<Sapio365Session>	m_SapioSessionBeingLoaded;
	boost::YOpt<SessionIdentifier>		m_SessionBeingLoadedIdentifier;
	bool								m_SessionBeingCreated;

	std::shared_ptr<Sapio365Session>	m_automationSessionToRestoreInMainFrameAfterScriptCompletion;

	//wstring m_CommandLineAutomationJobCenterIDBackup;
	wstring m_CommandLineAutomationScriptFilePathBackup;
	wstring m_CommandLineAutomationJobKeyBackup;
	bool	m_PendingAutomationForPostInit;
	boost::YOpt<wstring> m_FileToOpen;

	static wstring g_LastLoadingError;
};


extern Office365AdminApp theApp;

#include "Office365Admin.hpp"
