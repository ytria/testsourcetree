#include "ModuleOptions.h"

#include "JsonSerializeUtil.h"

ModuleOptions::ModuleOptions()
	: m_RequestSoftDeletedFolders(false)
	, m_BodyPreview(true)
	, m_BodyContent(false)
	, m_MailHeaders(false)
	, m_LoadingScope(LoadingScope::LastWeek)
{
}

wstring ModuleOptions::Serialize() const
{
	web::json::value json = web::json::value::object();
	auto& obj = json.as_object();

	JsonSerializeUtil::SerializeBool(_YTEXT("requestSoftDeletedFolders"), m_RequestSoftDeletedFolders, obj);

	JsonSerializeUtil::SerializeBool(_YTEXT("bodyPreview"), m_BodyPreview, obj);
	JsonSerializeUtil::SerializeBool(_YTEXT("bodyContent"), m_BodyContent, obj);
	JsonSerializeUtil::SerializeBool(_YTEXT("mailHeaders"), m_MailHeaders, obj);
	JsonSerializeUtil::SerializeUint32(_YTEXT("loadingScope"), static_cast<uint32_t>(m_LoadingScope), obj);

	return json.to_string();
}

bool ModuleOptions::Deserialize(const wstring& p_String)
{
	bool success = true;

	auto json = web::json::value::parse(p_String);

	ASSERT(json.is_object());
	if (json.is_object())
	{
		const auto& obj = json.as_object();
		if (!obj.empty())
		{
			{
				boost::YOpt<bool> b;
				JsonSerializeUtil::DeserializeBool(_YTEXT("requestSoftDeletedFolders"), b, obj);
				if (b)
					m_RequestSoftDeletedFolders = *b;
			}
			{
				boost::YOpt<bool> b;
				JsonSerializeUtil::DeserializeBool(_YTEXT("bodyPreview"), b, obj);
				if (b)
					m_BodyPreview = *b;
			}
			{
				boost::YOpt<bool> b;
				JsonSerializeUtil::DeserializeBool(_YTEXT("bodyContent"), b, obj);
				if (b)
					m_BodyContent = *b;
			}
			{
				boost::YOpt<bool> b;
				JsonSerializeUtil::DeserializeBool(_YTEXT("mailHeaders"), b, obj);
				if (b)
					m_MailHeaders = *b;
			}
			{
				boost::YOpt<uint32_t> u;
				JsonSerializeUtil::DeserializeUint32(_YTEXT("loadingScope"), u, obj);
				ASSERT(u);
				if (u)
				{
					ASSERT(static_cast<uint32_t>(LoadingScope::All) <= *u && *u <= static_cast<uint32_t>(LoadingScope::Other));
					if (static_cast<uint32_t>(LoadingScope::All) <= *u && *u <= static_cast<uint32_t>(LoadingScope::Other))
						m_LoadingScope = static_cast<LoadingScope>(*u);
				}
			}
		}
		else
		{
			success = false;
		}
	}

	return success;
}

bool ModuleOptions::operator==(const ModuleOptions& p_Other) const
{
	return p_Other.m_ReferenceTimeDate == m_ReferenceTimeDate
		&& p_Other.m_CutOffDateTime == m_CutOffDateTime
		&& p_Other.m_RequestSoftDeletedFolders == m_RequestSoftDeletedFolders
		&& p_Other.m_BodyPreview == m_BodyPreview
		&& p_Other.m_BodyContent == m_BodyContent
		&& p_Other.m_MailHeaders == m_MailHeaders
		&& p_Other.m_LoadingScope == m_LoadingScope
		&& p_Other.m_CustomFilter.ToString() == m_CustomFilter.ToString();
}
