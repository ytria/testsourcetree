#include "DlgGroupsModuleOptions.h"

#include "BusinessGroupConfiguration.h"
#include "MsGraphFieldNames.h"
#include "RoleDelegationManager.h"

void DlgGroupsModuleOptions::generateJSONScriptData()
{
	initMain(_YTEXT("DlgGroupsModuleOptions"));

	getGenerator().addIconTextField(_YTEXT("iconText"), _T("Limit the number of groups to load by applying some filters. You can request more later."), _YTEXT("fas fa-users-cog"));

	addFilterEditor(true);

	getGenerator().addApplyButton(YtriaTranslate::Do(CDlg_OnInitDialog_2, _YLOC("Ok")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str());
}

wstring DlgGroupsModuleOptions::getDialogTitle() const
{
	return _T("Load Groups - Options");
}

std::map<wstring, DlgModuleOptions::FilterFieldProperties> DlgGroupsModuleOptions::getFilterFields() const
{
	return
	{
			{ _YTEXT(O365_GROUP_DISPLAYNAME), { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_DISPLAYNAME)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_GROUP_MAIL), { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_MAIL)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_GROUP_MAILNICKNAME), { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_MAILNICKNAME)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_GROUP_ONPREMISESLASTSYNCDATETIME), { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_ONPREMISESLASTSYNCDATETIME)), true, FilterFieldProperties::DateTimeType(), g_FilterOperatorsNoEQ } }		// (except eq) // NE is authorized but as we use DateType and data contains time, result is incorrect. FIXME when DateTimeType has a calendar.
		,	{ _YTEXT(O365_GROUP_ONPREMISESSYNCENABLED), { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_ONPREMISESSYNCENABLED)), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } } // bool
		,	{ _YTEXT(O365_GROUP_PROXYADDRESSES), { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_PROXYADDRESSES)), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_GROUP_SECURITYENABLED), { BusinessGroupConfiguration::GetInstance().GetTitle(_YTEXT(O365_GROUP_SECURITYENABLED)), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } } // bool
	};
}
