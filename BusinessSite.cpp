#include "BusinessSite.h"

#include "CachedSite.h"
#include "MsGraphFieldNames.h"

RTTR_REGISTRATION
{
	using namespace rttr;

registration::class_<BusinessSite>("Site") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Site"))))
.constructor()(policy::ctor::as_object)
.property(O365_ID, &BusinessSite::m_Id)
.property(O365_SITE_DISPLAYNAME, &BusinessSite::m_DisplayName)
.property(O365_SITE_CREATEDDATETIME, &BusinessSite::m_CreatedDateTime)
.property(O365_SITE_DESCRIPTION, &BusinessSite::m_Description)
.property(O365_SITE_LASTMODIFIEDDATETIME, &BusinessSite::m_LastModifiedDateTime)
.property(O365_SITE_NAME, &BusinessSite::m_Name)
.property(O365_SITE_ROOT, &BusinessSite::m_Root)
.property(O365_SITE_SHAREPOINTIDS, &BusinessSite::m_SharepointIds)
.property(O365_SITE_SITECOLLECTION, &BusinessSite::m_SiteCollection)
.property(O365_SITE_WEBURL, &BusinessSite::m_WebUrl);
}

namespace
{
	Site CachedToSite(const CachedSite& p_CachedSite)
	{
		Site site;
		site.Id = p_CachedSite.GetID();
		site.DisplayName = p_CachedSite.GetDisplayName();
		site.Name = p_CachedSite.GetName();
		site.WebUrl = p_CachedSite.GetWebUrl();
		return site;
	}
}

BusinessSite::BusinessSite(const Site& p_JsonSite)
{
	SetValuesFrom(p_JsonSite);
}

BusinessSite::BusinessSite(const CachedSite& p_CachedSite)
	: BusinessSite(CachedToSite(p_CachedSite))
{
	SetRBACDelegationID(p_CachedSite.GetRBACDelegationID());
}

void BusinessSite::SetValuesFrom(const Site& p_JsonSite)
{
	m_Id = p_JsonSite.Id;
	m_CreatedDateTime = p_JsonSite.CreatedDateTime;
	m_Description = p_JsonSite.Description;
	m_DisplayName = p_JsonSite.DisplayName;
	m_LastModifiedDateTime = p_JsonSite.LastModifiedDateTime;
	m_Name = p_JsonSite.Name;
	m_Root = p_JsonSite.Root;
	m_SharepointIds = p_JsonSite.SharepointIds;
	m_SiteCollection = p_JsonSite.SiteCollection;
	m_WebUrl = p_JsonSite.WebUrl;
}

void BusinessSite::SetValuesFrom(const CachedSite& p_CachedSite)
{
	SetValuesFrom(CachedToSite(p_CachedSite));
	SetRBACDelegationID(p_CachedSite.GetRBACDelegationID());
}

boost::YOpt<YTimeDate> BusinessSite::GetCreatedDateTime() const
{
    return m_CreatedDateTime;
}

void BusinessSite::SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_CreatedDateTime = p_Val;
}

boost::YOpt<PooledString> BusinessSite::GetDescription() const
{
    return m_Description;
}

void BusinessSite::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
    m_Description = p_Val;
}

boost::YOpt<PooledString> BusinessSite::GetDisplayName() const
{
    return m_DisplayName;
}

void BusinessSite::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
    m_DisplayName = p_Val;
}

boost::YOpt<YTimeDate> BusinessSite::GetLastModifiedDateTime() const
{
    return m_LastModifiedDateTime;
}

void BusinessSite::SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_LastModifiedDateTime = p_Val;
}

boost::YOpt<PooledString> BusinessSite::GetName() const
{
    return m_Name;
}

void BusinessSite::SetName(const boost::YOpt<PooledString>& p_Val)
{
    m_Name = p_Val;
}

boost::YOpt<Root> BusinessSite::GetRoot() const
{
    return m_Root;
}

void BusinessSite::SetRoot(const boost::YOpt<Root>& p_Val)
{
    m_Root = p_Val;
}

boost::YOpt<SharepointIds> BusinessSite::GetSharepointIds() const
{
    return m_SharepointIds;
}

void BusinessSite::SetSharepointIds(const boost::YOpt<SharepointIds>& p_Val)
{
    m_SharepointIds = p_Val;
}

boost::YOpt<SiteCollection> BusinessSite::GetSiteCollection() const
{
    return m_SiteCollection;
}

void BusinessSite::SetSiteCollection(const boost::YOpt<SiteCollection>& p_Val)
{
    m_SiteCollection = p_Val;
}

const boost::YOpt<PooledString>& BusinessSite::GetWebUrl() const
{
    return m_WebUrl;
}

void BusinessSite::SetWebUrl(const boost::YOpt<PooledString>& p_Val)
{
    m_WebUrl = p_Val;
}

const vector<BusinessSite>& BusinessSite::GetSubSites() const
{
    return m_SubSites;
}

void BusinessSite::SetSubSites(const vector<BusinessSite>& p_Val)
{
    m_SubSites = p_Val;
}

bool BusinessSite::operator<(const BusinessSite& p_Other) const
{
	return GetID() < p_Other.GetID();
}

vector<PooledString> BusinessSite::GetProperties(const wstring& p_PropName) const
{
    if (p_PropName == _YUID(O365_SITE_SHAREPOINTIDS))
    {
        return{
            _YTEXT(O365_SITE_SHAREPOINTIDSLISTID),
            _YTEXT(O365_SITE_SHAREPOINTIDSLISTITEMID),
            _YTEXT(O365_SITE_SHAREPOINTIDSLISTITEMUNIQUEID),
            _YTEXT(O365_SITE_SHAREPOINTIDSSITEID),
            _YTEXT(O365_SITE_SHAREPOINTIDSSITEURL),
            _YTEXT(O365_SITE_SHAREPOINTIDSWEBID)
        };
    }

	ASSERT(p_PropName == MFCUtil::convertASCII_to_UNICODE(rttr::type::get<BusinessSite>().get_property(Str::convertToASCII(p_PropName.c_str())).get_name().to_string()));
	return{ p_PropName };
}

const boost::YOpt<BusinessDrive>& BusinessSite::GetDrive() const
{
	return m_Drive;
}

void BusinessSite::SetDrive(const boost::YOpt<BusinessDrive>& p_Val)
{
	m_Drive = p_Val;
}
