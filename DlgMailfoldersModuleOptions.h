#pragma once

#include "DlgModuleOptions.h"

class DlgMailfoldersModuleOptions : public DlgModuleOptions
{
public:
	using DlgModuleOptions::DlgModuleOptions;

protected:
	void generateJSONScriptData() override;
	wstring getDialogTitle() const override;
};
