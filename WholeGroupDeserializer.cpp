#include "WholeGroupDeserializer.h"

#include "GroupDeserializer.h"
#include "GroupNonPropDeserializer.h"
#include "JsonSerializeUtil.h"

WholeGroupDeserializer::WholeGroupDeserializer(const std::shared_ptr<Sapio365Session>& p_Session)
    : m_Session(p_Session)
{
}

void WholeGroupDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
#ifdef _DEBUG
        GroupDeserializer d(m_Session, true);
#else
		GroupDeserializer d(m_Session);
#endif
		JsonSerializeUtil::DeserializeObject(d, p_Object);
        m_Data = std::move(d.GetData());

    }

    {
        GroupNonPropDeserializer d;
		JsonSerializeUtil::DeserializeObject(d, p_Object);
        m_Data.MergeWith(d.GetData());

		m_Data.SetHasTeamInfo(m_Data.GetTeam() && m_Data.GetTeam()->MemberSettings);
    }
}
