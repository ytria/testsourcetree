#pragma once

#include "BusinessObject.h"

namespace Sp
{
    class BasePermissions : public BusinessObject
    {
    public:
        boost::YOpt<PooledString> High;
        boost::YOpt<PooledString> Low;

    private:
        RTTR_ENABLE(BusinessObject)
            RTTR_REGISTRATION_FRIEND
    };
}

