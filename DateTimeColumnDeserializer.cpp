#include "DateTimeColumnDeserializer.h"

#include "JsonSerializeUtil.h"

void DateTimeColumnDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("displayAs"), m_Data.DisplayAs, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("format"), m_Data.Format, p_Object);
}
