#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "PatternedRecurrence.h"

class PatternedRecurrenceDeserializer : public JsonObjectDeserializer, public Encapsulate<PatternedRecurrence>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

