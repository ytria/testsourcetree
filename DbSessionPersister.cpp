#include "DbSessionPersister.h"
#include "InlineSqlQueryRowHandler.h"

#include "SessionsSqlEngine.h"
#include "SqlQuerySelect.h"
#include "QueryGetCredentialsId.h"
#include "SqlQueryPreparedStatement.h"
#include "Str.h"
#include "LoggerService.h"
#include "QuerySessionExists.h"
#include "SessionTypes.h"
#include "SessionIdsEmitter.h"

DbSessionPersister::DbSessionPersister(const PersistentSession& p_Session)
	: m_Session(p_Session)
{
	QuerySessionExists session(p_Session.GetIdentifier(), SessionsSqlEngine::Get());
	session.Run();
	m_SessionId = session.GetSessionId();
}

bool DbSessionPersister::Save(const oauth2_config& p_Config)
{
	bool success = true;

	QueryGetCredentialsId queryCredsId(SessionsSqlEngine::Get(), m_SessionId);
	queryCredsId.Run();
	int64_t credId = queryCredsId.GetCredentialsId();

	if (credId == 0)
	{
		wstring querySaveStr = _YTEXT(R"(INSERT INTO Credentials(AccessToken, RefreshToken, AppId, AppTenant, AppClientSecret) VALUES(?, ?, ?, ?, ?))");
		SqlQueryPreparedStatement querySave(SessionsSqlEngine::Get(), querySaveStr);
		querySave.BindString(1, p_Config.token().access_token());
		querySave.BindString(2, p_Config.token().refresh_token());
		querySave.BindString(3, m_Session.GetAppId());
		querySave.BindString(4, m_Session.GetTenantName());
		querySave.BindString(5, m_Session.GetAppClientSecret());
		querySave.Run();
		credId = sqlite3_last_insert_rowid(SessionsSqlEngine::Get().GetDbHandle());

		wstring queryUpdateStr = _YTEXT(R"(UPDATE Sessions SET CredentialsId=? WHERE Id=?)");
		SqlQueryPreparedStatement queryUpdateCred(SessionsSqlEngine::Get(), queryUpdateStr);
		queryUpdateCred.BindInt64(1, credId);
		queryUpdateCred.BindInt64(2, m_SessionId);
		queryUpdateCred.Run();

		if (m_Session.IsStandard() || m_Session.IsRole())
		{
			// For all other sessions with same email that are basic_user or role, set same credentials
			vector<int64_t> similarSessionIds;
			auto selectHandler = [&similarSessionIds](sqlite3_stmt* p_Stmt) {
				similarSessionIds.push_back(sqlite3_column_int64(p_Stmt, 0));
			};

			SqlQuerySelect querySimilarSessions(SessionsSqlEngine::Get(), InlineSqlQueryRowHandler<decltype(selectHandler)>(selectHandler), _YTEXT("SELECT Id FROM Sessions WHERE EmailOrAppId=? AND SessionType IN(?,?)"));
			querySimilarSessions.BindString(1, m_Session.GetEmailOrAppId());
			querySimilarSessions.BindString(2, SessionTypes::g_StandardSession);
			querySimilarSessions.BindString(3, SessionTypes::g_Role);
			querySimilarSessions.Run();

			for (const auto& sessionId : similarSessionIds)
			{
				SqlQueryPreparedStatement queryUpdateCredId(SessionsSqlEngine::Get(), _YTEXT("UPDATE Sessions SET CredentialsId=? WHERE Id=?"));
				queryUpdateCredId.BindInt64(1, credId);
				queryUpdateCredId.BindInt64(2, sessionId);
				queryUpdateCredId.Run();
			}
		}
	}
	else
	{
		wstring querySaveStr = _YTEXT(R"(UPDATE Credentials SET AccessToken=?,RefreshToken=? WHERE Id=?)");
		SqlQueryPreparedStatement querySave(SessionsSqlEngine::Get(), querySaveStr);
		querySave.BindString(1, p_Config.token().access_token());
		querySave.BindString(2, p_Config.token().refresh_token());
		querySave.BindInt64(3, credId);
		querySave.Run();
	}

	return success;
}

bool DbSessionPersister::Load(oauth2_config& p_Config)
{
	// Reload session, as it might have changed between Elevated/Advanced
	m_Session = SessionsSqlEngine::Get().Load(m_SessionId);

	const auto sessionId = SessionIdsEmitter(m_Session.GetIdentifier()).GetId();
	LoggerService::Debug(_YFORMAT(L"[[%s]] - Loading tokens for session with emailOrAppId=%s, sessionType=%s, roleId=%s", sessionId.c_str(), m_Session.GetEmailOrAppId().c_str(), m_Session.GetSessionType().c_str(), std::to_wstring(m_Session.GetRoleId()).c_str()));

	QueryGetCredentialsId queryCredsId(SessionsSqlEngine::Get(), m_SessionId);
	queryCredsId.Run();
	int64_t credId = queryCredsId.GetCredentialsId();

	auto loadHandler = [&p_Config](sqlite3_stmt* p_Stmt) {
		oauth2_token token;
		token.set_access_token(GetStringFromColumn(0, p_Stmt));
		token.set_refresh_token(GetStringFromColumn(1, p_Stmt));
		p_Config.set_token(token);
	};
	wstring queryLoadStr = _YTEXT(R"(SELECT AccessToken, RefreshToken FROM Credentials WHERE Id=?)");
	SqlQuerySelect queryLoad(SessionsSqlEngine::Get(), InlineSqlQueryRowHandler<decltype(loadHandler)>(loadHandler), queryLoadStr);
	queryLoad.BindInt64(1, credId);
	queryLoad.Run();

	bool success = !p_Config.token().access_token().empty() || !p_Config.token().refresh_token().empty();
	if (success)
		LoggerService::Debug(_YFORMAT(L"[[%s]] - Successfully loaded oauth2 tokens from database", sessionId.c_str()));
	else
		LoggerService::Debug(_YFORMAT(L"[[%s]] - One or more oauth2 tokens are missing", sessionId.c_str()));

	return success;
}
