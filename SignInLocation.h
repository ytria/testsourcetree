#pragma once

#include "GeoCoordinates.h"

class SignInLocation
{
public:
	boost::YOpt<PooledString> m_City;
	boost::YOpt<PooledString> m_CountryOrRegion;
	boost::YOpt<GeoCoordinates> m_GeoCoordinates;
	boost::YOpt<PooledString> m_State;
};

