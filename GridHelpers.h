#pragma once

#include "InternetMessageHeader.h"
#include "Recipient.h"
#include "SizeRange.h"

class GridBackendRow;
class GridBackendColumn;

class GridHelpers
{
public:
    GridHelpers() = delete;

	// Returns a pair of vectors <Addresses, Names>
    static std::pair<vector<wstring>, vector<wstring>> ExtractEmailAddresses(const vector<Recipient>& p_Recipients);
	static vector<wstring> ExtractEmailAddressesToString(const vector<Recipient>& p_Recipients);
	static vector<Recipient> ExtractRecipients(const vector<PooledString>& p_Addresses, const vector<PooledString>& p_Names);
	static Recipient ExtractRecipient(const PooledString& p_Address, const PooledString& p_Name);

	static vector<Recipient> ExtractRecipients(GridBackendRow* p_Row, GridBackendColumn* p_ColAddresses, GridBackendColumn* p_ColNames);

	/////
	// Returns a pair of vectors <Names, Values>
	static std::pair<vector<wstring>, vector<wstring>> ExtractHeaders(const vector<InternetMessageHeader>& p_Headers);
	static vector<InternetMessageHeader> ExtractInternetMessageHeaders(GridBackendRow* p_Row, GridBackendColumn* p_ColNames, GridBackendColumn* p_ColValues);
	static vector<InternetMessageHeader> ExtractInternetMessageHeaders(const vector<PooledString>& p_Names, const vector<PooledString>& p_Values);

	// SizeRange
	static SizeRange ExtractSizeRange(GridBackendRow* p_Row, GridBackendColumn* p_ColMin, GridBackendColumn* p_ColMax);
};

