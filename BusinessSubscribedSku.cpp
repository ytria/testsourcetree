#include "BusinessSubscribedSku.h"
#include "Sapio365Session.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessSubscribedSku>("Business Subscribed Sku") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Subscribed Sku"))))
		.constructor()(policy::ctor::as_object)
		.property("CapabilityStatus", &BusinessSubscribedSku::m_CapabilityStatus)
		.property("ConsumedUnits", &BusinessSubscribedSku::m_ConsumedUnits)
		.property("PrepaidUnits", &BusinessSubscribedSku::m_PrepaidUnits)
		.property("ServicePlans", &BusinessSubscribedSku::m_ServicePlans)
		.property("SkuId", &BusinessSubscribedSku::m_SkuId)
		.property("SkuPartNumber", &BusinessSubscribedSku::m_SkuPartNumber)
		.property("AppliesTo", &BusinessSubscribedSku::m_AppliesTo)
		;
}

const boost::YOpt<PooledString>& BusinessSubscribedSku::GetCapabilityStatus() const
{
	return m_CapabilityStatus;
}

void BusinessSubscribedSku::SetCapabilityStatus(const boost::YOpt<PooledString>& val)
{
	m_CapabilityStatus = val;
}

const boost::YOpt<int32_t>& BusinessSubscribedSku::GetConsumedUnits() const
{
	return m_ConsumedUnits;
}

void BusinessSubscribedSku::SetConsumedUnits(const boost::YOpt<int32_t>& val)
{
	m_ConsumedUnits = val;
}

const boost::YOpt<LicenseUnitsDetail>& BusinessSubscribedSku::GetPrepaidUnits() const
{
	return m_PrepaidUnits;
}

void BusinessSubscribedSku::SetPrepaidUnits(const boost::YOpt<LicenseUnitsDetail>& val)
{
	m_PrepaidUnits = val;
}

const vector<ServicePlanInfo>& BusinessSubscribedSku::GetServicePlans() const
{
	return m_ServicePlans;
}

void BusinessSubscribedSku::SetServicePlans(const vector<ServicePlanInfo>& val)
{
	m_ServicePlans = val;
}

const boost::YOpt<PooledString>& BusinessSubscribedSku::GetSkuId() const
{
	return m_SkuId;
}

void BusinessSubscribedSku::SetSkuId(const boost::YOpt<PooledString>& val)
{
	m_SkuId = val;
}

const boost::YOpt<PooledString>& BusinessSubscribedSku::GetSkuPartNumber() const
{
	return m_SkuPartNumber;
}

void BusinessSubscribedSku::SetSkuPartNumber(const boost::YOpt<PooledString>& val)
{
	m_SkuPartNumber = val;
}

const boost::YOpt<PooledString>& BusinessSubscribedSku::GetAppliesTo() const
{
	return m_AppliesTo;
}

void BusinessSubscribedSku::SetAppliesTo(const boost::YOpt<PooledString>& val)
{
	m_AppliesTo = val;
}

pplx::task<vector<HttpResultWithError>> BusinessSubscribedSku::SendEditRequest(std::shared_ptr<Sapio365Session> p_MSGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(false);
	return pplx::task_from_result(vector<HttpResultWithError>{ });
}

TaskWrapper<HttpResultWithError> BusinessSubscribedSku::SendCreateRequest(std::shared_ptr<Sapio365Session> p_MSGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(false);
    return pplx::task_from_result(HttpResultWithError());
}

TaskWrapper<HttpResultWithError> BusinessSubscribedSku::SendDeleteRequest(std::shared_ptr<Sapio365Session> p_MSGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(false);
    return pplx::task_from_result(HttpResultWithError());
}
