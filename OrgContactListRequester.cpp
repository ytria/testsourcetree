#include "OrgContactListRequester.h"

#include "BusinessGroup.h"
#include "IPropertySetBuilder.h"
#include "MSGraphCommonData.h"
#include "MSGraphUtil.h"
#include "MSGraphSession.h"
#include "MsGraphPaginator.h"
#include "OrgContactDeserializer.h"
#include "PaginatedRequestResults.h"
#include "Sapio365Session.h"
#include "ValueListDeserializer.h"
#include "PaginatorUtil.h"
#include "MsGraphHttpRequestLogger.h"

OrgContactListRequester::OrgContactListRequester(IPropertySetBuilder& p_PropertySet, const std::shared_ptr<IPageRequestLogger>& p_Logger)
    : m_Properties(std::move(p_PropertySet.GetPropertySet())),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> OrgContactListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<BusinessOrgContact, OrgContactDeserializer>>();

	web::uri_builder uri(_YTEXT("contacts"));
	uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(m_Properties));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	auto paginatorReq = Util::CreateDefaultGraphPageRequester(m_Deserializer, httpLogger, p_TaskData.GetOriginator());
	paginatorReq->SetUseBetaEndpoint(true);

	return MsGraphPaginator(uri.to_uri(), paginatorReq, m_Logger).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);

}

vector<BusinessOrgContact>& OrgContactListRequester::GetData()
{
    return m_Deserializer->GetData();
}
