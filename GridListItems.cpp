#include "GridListItems.h"

#include "AutomationWizardListItems.h"
#include "O365AdminUtil.h"
#include "FrameListItems.h"
#include "GridUpdater.h"
#include "BasicGridSetup.h"
#include "GraphCache.h"

GridListItems::GridListItems()
	: m_ColMetaId(nullptr)
{
    //UseDefaultActions(/*O365Grid::ACTION_CREATE | */O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT/* | O365Grid::ACTION_VIEW*/);

	initWizard<AutomationWizardListItems>(_YTEXT("Automation\\ListsItems"));
}

void GridListItems::customizeGrid()
{
	static const wstring g_FamilyList = YtriaTranslate::Do(GridListItems_customizeGrid_1, _YLOC("List")).c_str();
    
	{
		BasicGridSetup gridSetup(GridUtil::g_AutoNameListsContent, LicenseUtil::g_codeSapio365listItems);
		gridSetup.AddMetaColumn(GridSetupMetaColumn(&m_ColMetaDisplayName, YtriaTranslate::Do(GridListItems_customizeGrid_2, _YLOC("List Display Name")).c_str(), g_FamilyList, GridSetupMetaColumn::g_ColUIDMetaDisplayName));
		gridSetup.AddMetaColumn(GridSetupMetaColumn(&m_ColMetaName, YtriaTranslate::Do(GridListItems_customizeGrid_3, _YLOC("List Name")).c_str(), g_FamilyList, _YUID("meta.name")));
		gridSetup.AddMetaColumn(GridSetupMetaColumn(&m_ColMetaSiteDisplayName, YtriaTranslate::Do(GridListItems_customizeGrid_4, _YLOC("Site Display Name")).c_str(), g_FamilySite, GridSetupMetaColumn::g_ColUIDMetaUserName));
		gridSetup.AddMetaColumn(GridSetupMetaColumn(&m_ColMetaId, YtriaTranslate::Do(GridListItems_customizeGrid_5, _YLOC("List ID")).c_str(), g_FamilyList, GridSetupMetaColumn::g_ColUIDMetaId));
		gridSetup.Setup(*this, false);
	}

	m_ColMetaSiteDisplayName->SetUniqueID(_YUID("meta.siteDisplayName"));
	setBasicGridSetupHierarchy({ { { m_ColMetaDisplayName, 0 } }
								,{ rttr::type::get<BusinessListItem>().get_id() }
								,{ rttr::type::get<BusinessListItem>().get_id(), rttr::type::get<BusinessList>().get_id() } });

	m_ColMetaSiteId									= AddColumn(_YUID("meta.siteId"),								YtriaTranslate::Do(GridListItems_customizeGrid_6, _YLOC("Site ID")).c_str(),							g_FamilySite,				g_ColumnSize12, { g_ColumnsPresetTech });

	static const wstring g_FamilyItemInfo = YtriaTranslate::Do(GridListItems_customizeGrid_7, _YLOC("Item Info")).c_str();
	addColumnGraphID(g_FamilyItemInfo);
	ASSERT(nullptr != GetColId());
	if (nullptr != GetColId())
	{
		GetColId()->SetTitle(YtriaTranslate::Do(GridListItems_customizeGrid_8, _YLOC("Item ID")).c_str());
		GetColId()->SetPresetFlags({ g_ColumnsPresetDefault });
	}

	m_ColListItemName								= AddColumn(_YUID("listItem.name"),								YtriaTranslate::Do(GridListItems_customizeGrid_9, _YLOC("Item Name")).c_str(),							g_FamilyItemInfo,			g_ColumnSize22);
	m_ColListItemDescription						= AddColumn(_YUID("listItem.description"),						YtriaTranslate::Do(GridListItems_customizeGrid_10, _YLOC("Item Description")).c_str(),					g_FamilyItemInfo,			g_ColumnSize32);
	m_ColListItemCreatedDateTime					= AddColumnDate(_YUID("listItem.createdDateTime"),				YtriaTranslate::Do(GridListItems_customizeGrid_11, _YLOC("Created On")).c_str(),						g_FamilyItemInfo,			g_ColumnSize16, { g_ColumnsPresetDefault });
	static const wstring g_FamilyCreation = YtriaTranslate::Do(GridListItems_customizeGrid_12, _YLOC("Creation")).c_str();
	m_ColListItemCreatedByUserName					= AddColumn(_YUID("listItem.createdBy.user.name"),				YtriaTranslate::Do(GridListItems_customizeGrid_13, _YLOC("Created By - Username")).c_str(),				g_FamilyCreation,			g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColListItemCreatedByUserEmail					= AddColumn(_YUID("listItem.createdBy.user.email"),				YtriaTranslate::Do(GridListItems_customizeGrid_14, _YLOC("Created By - Email")).c_str(),				g_FamilyCreation,			g_ColumnSize32);
	m_ColListItemCreatedByUserId					= AddColumn(_YUID("listItem.createdBy.user.id"),				YtriaTranslate::Do(GridListItems_customizeGrid_15, _YLOC("Created By - User ID")).c_str(),				g_FamilyCreation,			g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColListItemCreatedByApplicationName			= AddColumn(_YUID("listItem.createdBy.application.name"),		YtriaTranslate::Do(GridListItems_customizeGrid_16, _YLOC("Created By - Application Name")).c_str(),		g_FamilyCreation,			g_ColumnSize22);
	m_ColListItemCreatedByApplicationId				= AddColumn(_YUID("listItem.createdBy.application.id"),			YtriaTranslate::Do(GridListItems_customizeGrid_17, _YLOC("Created By - Application ID")).c_str(),		g_FamilyCreation,			g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColListItemCreatedByDeviceName				= AddColumn(_YUID("listItem.createdBy.device.name"),			YtriaTranslate::Do(GridListItems_customizeGrid_18, _YLOC("Created By - Device Name")).c_str(),			g_FamilyCreation,			g_ColumnSize22);
	m_ColListItemCreatedByDeviceId					= AddColumn(_YUID("listItem.createdBy.device.id"),				YtriaTranslate::Do(GridListItems_customizeGrid_19, _YLOC("Created By - Device ID")).c_str(),			g_FamilyCreation,			g_ColumnSize12, { g_ColumnsPresetTech });
	static const wstring g_FamilyLastModification = YtriaTranslate::Do(GridListItems_customizeGrid_20, _YLOC("Last Modification")).c_str();
	m_ColListItemLastModifiedDateTime				= AddColumnDate(_YUID("listItem.lastModifiedDateTime"),			YtriaTranslate::Do(GridListItems_customizeGrid_21, _YLOC("Last Modified")).c_str(),						g_FamilyLastModification,	g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColListItemLastModifiedByUserName				= AddColumn(_YUID("listItem.lastModifiedBy.user.name"),			YtriaTranslate::Do(GridListItems_customizeGrid_22, _YLOC("Last Modified - Username")).c_str(),			g_FamilyLastModification,	g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColListItemLastModifiedByUserEmail			= AddColumn(_YUID("listItem.lastModifiedBy.user.email"),		YtriaTranslate::Do(GridListItems_customizeGrid_23, _YLOC("Last Modified - Email")).c_str(),				g_FamilyLastModification,	g_ColumnSize32);
	m_ColListItemLastModifiedByUserId				= AddColumn(_YUID("listItem.lastModifiedBy.user.id"),			YtriaTranslate::Do(GridListItems_customizeGrid_24, _YLOC("Last Modified - User ID")).c_str(),			g_FamilyLastModification,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColListItemLastModifiedByAppName				= AddColumn(_YUID("listItem.lastModifiedBy.application.name"),	YtriaTranslate::Do(GridListItems_customizeGrid_25, _YLOC("Last Modified - Application Name")).c_str(),	g_FamilyLastModification,	g_ColumnSize22);
	m_ColListItemLastModifiedByApplicationId		= AddColumn(_YUID("listItem.lastModifiedBy.application.id"),	YtriaTranslate::Do(GridListItems_customizeGrid_26, _YLOC("Last Modified - Application ID")).c_str(),	g_FamilyLastModification,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColListItemLastModifiedByDeviceName			= AddColumn(_YUID("listItem.lastModifiedBy.device.name"),		YtriaTranslate::Do(GridListItems_customizeGrid_27, _YLOC("Last Modified - Device Name")).c_str(),		g_FamilyLastModification,	g_ColumnSize22);
	m_ColListItemLastModifiedByDeviceId				= AddColumn(_YUID("listItem.lastModifiedBy.device.id"),			YtriaTranslate::Do(GridListItems_customizeGrid_28, _YLOC("Last Modified - Device ID")).c_str(),			g_FamilyLastModification,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColListItemWebUrl								= AddColumnHyperlink(_YUID("listItem.webUrl"),					YtriaTranslate::Do(GridListItems_customizeGrid_29, _YLOC("URL")).c_str(),								g_FamilyItemInfo,			g_ColumnSize6,	{ g_ColumnsPresetDefault });

	AddColumnForRowPK(m_ColMetaId);
	AddColumnForRowPK(m_ColMetaSiteId);
	AddColumnForRowPK(GetColId());

	addSystemTopCategory();
	AddCategoryTop(YtriaTranslate::Do(GridListItems_customizeGrid_30, _YLOC("List Items")).c_str(), vector<GridBackendColumn*> {m_ColMetaSiteId, m_ColMetaSiteDisplayName, m_ColMetaId, m_ColMetaDisplayName, m_ColMetaName, GetColId(), m_ColListItemName, m_ColListItemCreatedByUserName,
					m_ColListItemCreatedByUserId, m_ColListItemCreatedByUserEmail, m_ColListItemCreatedByApplicationName, m_ColListItemCreatedByApplicationId,
					m_ColListItemCreatedByDeviceName, m_ColListItemCreatedByDeviceId, m_ColListItemCreatedDateTime, m_ColListItemDescription, m_ColListItemLastModifiedByUserName,
					m_ColListItemLastModifiedByUserId, m_ColListItemLastModifiedByUserEmail, m_ColListItemLastModifiedByAppName, m_ColListItemLastModifiedByApplicationId,
				    m_ColListItemLastModifiedByDeviceName, m_ColListItemLastModifiedByDeviceId, m_ColListItemLastModifiedDateTime, m_ColListItemWebUrl});

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameListItems*>(GetParentFrame()));
}

wstring GridListItems::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Site"), m_ColMetaSiteDisplayName }, { _T("List"), m_ColMetaDisplayName } }, 
        _YFORMAT(L"Created on %s by %s", p_Row->GetField(m_ColListItemCreatedDateTime).ToString().c_str(), p_Row->GetField(m_ColListItemCreatedByUserName).ToString().c_str()));
}

void GridListItems::BuildView(const O365DataMap<wstring, vector<BusinessList>>& p_Lists, Origin p_Origin, bool p_FullPurge)
{
    clearGrid();

	GridUpdater updater(*this, GridUpdaterOptions(
        GridUpdaterOptions::UPDATE_GRID
        | (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
        | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
        | GridUpdaterOptions::REFRESH_PROCESS_DATA
    ));
    GridIgnoreModification ignoramus(*this);

    std::vector<ListColumn> listColumns;

    for (const auto& listsMap : p_Lists)
        for (const auto& list : listsMap.second)
            for (const auto& column : list.GetColumns())
				listColumns.emplace_back(&list, &column);

    std::sort(std::begin(listColumns), std::end(listColumns));

    createColumns(listColumns);

	// GridBackendField fieldItemIdForList(_YTEXT(""), GetColId());
    for (const auto& listKeyVal : p_Lists)
    {
        const wstring siteId = listKeyVal.first;
		GridBackendField fieldSiteId(siteId, m_ColMetaSiteId);

        for (const auto& list : listKeyVal.second)
        {
            GridBackendField fieldListId(list.GetID(), m_ColMetaId);
            auto parentRow = m_RowIndex.GetRow({ fieldSiteId, fieldListId/*, fieldItemIdForList */}, true, true);

            ASSERT(nullptr != parentRow);
            if (nullptr != parentRow)
            {
				updater.GetOptions().AddRowWithRefreshedValues(parentRow);
                updater.GetOptions().AddPartialPurgeParentRow(parentRow);

                const wstring hierarchyDisplayName = GetGraphCache().GetSiteHierarchyDisplayName(siteId);
                parentRow->SetHierarchyParentToHide(true);
                parentRow->AddField(hierarchyDisplayName, m_ColMetaSiteDisplayName);
                parentRow->AddField(list.GetDisplayName(), m_ColMetaDisplayName);
                parentRow->AddField(list.GetName(), m_ColMetaName);
                parentRow->AddField(list.GetID(), m_ColMetaId);

				SetRowObjectType(parentRow, list, list.HasFlag(BusinessObject::CANCELED));

				if (!list.HasFlag(BusinessObject::CANCELED))
				{
					if (list.GetListItems().size() == 1 && list.GetListItems()[0].GetError())
					{
						updater.OnLoadingError(parentRow, *list.GetListItems()[0].GetError());
					}
					else
					{
						// Clear previous error
						if (parentRow->IsError())
							parentRow->ClearStatus();

						for (const auto& listItem : list.GetListItems())
						{
							auto row = m_RowIndex.GetRow({ fieldSiteId, fieldListId, GridBackendField(listItem.GetID(), GetColId()) }, true, true);
							ASSERT(nullptr != row);
							if (nullptr != row)
							{
								updater.GetOptions().AddRowWithRefreshedValues(row);

								SetRowObjectType(row, listItem, listItem.HasFlag(BusinessObject::CANCELED));
								row->AddField(hierarchyDisplayName, m_ColMetaSiteDisplayName);
								row->SetParentRow(parentRow);
								fillRow(row, listItem, list);
							}
						}
					}
				}
            }
        }
    }

    vector<GridBackendColumn*> fieldColumns;
	for (const auto& keyVal : m_Columns)
		fieldColumns.push_back(keyVal.second);

    AddCategoryTop(YtriaTranslate::Do(GridListItems_BuildView_1, _YLOC("Fields")).c_str(), fieldColumns);
}

ModuleCriteria GridListItems::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    GridFrameBase* frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
    ModuleCriteria criteria = frame->GetModuleCriteria();

    ASSERT(nullptr != frame);
    if (nullptr != frame)
    {
        criteria.m_SubIDs.clear();
        for (auto row : p_Rows)
        {
            const PooledString& siteId = row->GetField(m_ColMetaSiteId).GetValueStr();
            const PooledString& listId = row->GetField(m_ColMetaId).GetValueStr();
            criteria.m_SubIDs[siteId].insert(listId);
        }
    }

    return criteria;
}

BusinessListItem GridListItems::getBusinessObject(GridBackendRow* p_Row) const
{
    return BusinessListItem();
}

void GridListItems::clearGrid()
{
    auto allColumns = GetBackend().GetColumns();
    vector<GridBackendColumn*> columnsToRemove;

    for (auto itt = std::begin(allColumns); itt != std::end(allColumns); ++itt)
    {
        GridBackendColumn* currentCol = *itt;
        if (currentCol != m_ColumnObjectType &&
            currentCol != m_ColMetaSiteId &&
            currentCol != m_ColMetaSiteDisplayName &&
            currentCol != m_ColMetaId &&
            currentCol != m_ColMetaDisplayName &&
            currentCol != m_ColMetaName &&
            currentCol != GetColId() &&
            currentCol != m_ColListItemName &&
            currentCol != m_ColListItemCreatedByUserName &&
            currentCol != m_ColListItemCreatedByUserId &&
            currentCol != m_ColListItemCreatedByUserEmail &&
            currentCol != m_ColListItemCreatedByApplicationName &&
            currentCol != m_ColListItemCreatedByApplicationId &&
            currentCol != m_ColListItemCreatedByDeviceName &&
            currentCol != m_ColListItemCreatedByDeviceId &&
            currentCol != m_ColListItemCreatedDateTime &&
            currentCol != m_ColListItemDescription &&
            currentCol != m_ColListItemLastModifiedByUserName &&
            currentCol != m_ColListItemLastModifiedByUserId &&
            currentCol != m_ColListItemLastModifiedByUserEmail &&
            currentCol != m_ColListItemLastModifiedByAppName &&
            currentCol != m_ColListItemLastModifiedByApplicationId &&
            currentCol != m_ColListItemLastModifiedByDeviceName &&
            currentCol != m_ColListItemLastModifiedByDeviceId &&
            currentCol != m_ColListItemLastModifiedDateTime &&
            currentCol != m_ColListItemWebUrl &&
            currentCol != GetBackend().GetStatusColumn() &&
            !GetBackend().IsHierarchyColumn(currentCol))
        {
            columnsToRemove.push_back(*itt);
        }
    }

    RemoveColumns(columnsToRemove);
    m_Columns.clear();
    m_ListColumns.clear();
    UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS | GridBackendUtil::GRIDUPDATE_MAIN_GCVD_RESET);
}

void GridListItems::createColumns(const vector<ListColumn>& p_ListColumns)
{
    for (const auto& listColumn : p_ListColumns)
    {
		if (nullptr != listColumn.m_Column)
		{
			const auto& column = *listColumn.m_Column;
			ASSERT(column.GetName());
			if (column.GetName())
			{
				auto colName = Str::toLower(*column.GetName());

				wstring displayName{ *column.GetName() };
				if (column.GetDisplayName())
					displayName = *column.GetDisplayName() + wstring(_YTEXT(" [")) + *column.GetName() + wstring(_YTEXT("]"));

				if (nullptr != listColumn.m_List)
					UpdateListColumnsCache(listColumn.m_List->GetID(), *column.GetName());

				// Column already exists?
				if (m_Columns.find(colName) != std::end(m_Columns))
					continue;

				if (column.GetTextFacet() || column.GetBooleanFacet() || column.GetCalculatedFacet() || column.GetChoiceFacet() ||
					column.GetCurrencyFacet() || column.GetDefaultValueFacet() || column.GetLookupFacet() || column.GetPersonOrGroupFacet())
				{
					m_Columns[colName] = AddColumn(colName, displayName, _YTEXT(""));
				}
				else if (column.GetDateTimeFacet())
				{
					m_Columns[colName] = AddColumnDate(colName, displayName, _YTEXT(""));
				}
				else if (column.GetNumberFacet())
				{
					m_Columns[colName] = AddColumnNumber(colName, displayName, _YTEXT(""));
				}
				else
				{
					// Unknown column type
					m_Columns[colName] = AddColumn(colName, displayName, _YTEXT(""));
				}
			}
		}
    }

}

void GridListItems::fillRow(GridBackendRow* p_Row, const BusinessListItem& p_ListItem, const BusinessList& p_List)
{
    p_Row->AddField(p_List.GetDisplayName(), m_ColMetaDisplayName);
    p_Row->AddField(p_List.GetName(), m_ColMetaName);
    p_Row->AddField(p_List.GetID(), m_ColMetaId);
    p_Row->AddField(p_ListItem.GetName(), m_ColListItemName);

    if (p_ListItem.GetCreatedBy())
    {
        const auto& createdBy = p_ListItem.GetCreatedBy();

        if (createdBy->User)
        {
            p_Row->AddField(createdBy->User->DisplayName, m_ColListItemCreatedByUserName);
            p_Row->AddField(createdBy->User->Id, m_ColListItemCreatedByUserId);
            p_Row->AddField(createdBy->User->Email, m_ColListItemCreatedByUserEmail);
        }
		else
		{
			p_Row->RemoveField(m_ColListItemCreatedByUserName);
			p_Row->RemoveField(m_ColListItemCreatedByUserId);
			p_Row->RemoveField(m_ColListItemCreatedByUserEmail);
		}

        if (createdBy->Application)
        {
            p_Row->AddField(createdBy->Application->DisplayName, m_ColListItemCreatedByApplicationName);
            p_Row->AddField(createdBy->Application->Id, m_ColListItemCreatedByApplicationId);
        }
		else
		{
			p_Row->RemoveField(m_ColListItemCreatedByApplicationName);
			p_Row->RemoveField(m_ColListItemCreatedByApplicationId);
		}

        if (createdBy->Device)
        {
            p_Row->AddField(createdBy->Device->DisplayName, m_ColListItemCreatedByDeviceName);
            p_Row->AddField(createdBy->Device->Id, m_ColListItemCreatedByDeviceId);
        }
		else
		{
			p_Row->RemoveField(m_ColListItemCreatedByDeviceName);
			p_Row->RemoveField(m_ColListItemCreatedByDeviceId);
		}
    }
	else
	{
		p_Row->RemoveField(m_ColListItemCreatedByUserName);
		p_Row->RemoveField(m_ColListItemCreatedByUserId);
		p_Row->RemoveField(m_ColListItemCreatedByUserEmail);
		p_Row->RemoveField(m_ColListItemCreatedByApplicationName);
		p_Row->RemoveField(m_ColListItemCreatedByApplicationId);
		p_Row->RemoveField(m_ColListItemCreatedByDeviceName);
		p_Row->RemoveField(m_ColListItemCreatedByDeviceId);
	}

    p_Row->AddField(p_ListItem.GetCreatedDateTime(), m_ColListItemCreatedDateTime);
    p_Row->AddField(p_ListItem.GetDescription(), m_ColListItemDescription);

    if (p_ListItem.GetLastModifiedBy())
    {
        const auto& lastModifiedBy = p_ListItem.GetLastModifiedBy();

        if (lastModifiedBy->User)
        {
            p_Row->AddField(lastModifiedBy->User->DisplayName, m_ColListItemLastModifiedByUserName);
            p_Row->AddField(lastModifiedBy->User->Id, m_ColListItemLastModifiedByUserId);
            p_Row->AddField(lastModifiedBy->User->Email, m_ColListItemLastModifiedByUserEmail);
        }
		else
		{
			p_Row->RemoveField(m_ColListItemLastModifiedByUserName);
			p_Row->RemoveField(m_ColListItemLastModifiedByUserId);
			p_Row->RemoveField(m_ColListItemLastModifiedByUserEmail);
		}

        if (lastModifiedBy->Application)
        {
            p_Row->AddField(lastModifiedBy->Application->DisplayName, m_ColListItemLastModifiedByAppName);
            p_Row->AddField(lastModifiedBy->Application->Id, m_ColListItemLastModifiedByApplicationId);
        }
		else
		{
			p_Row->RemoveField(m_ColListItemLastModifiedByAppName);
			p_Row->RemoveField(m_ColListItemLastModifiedByApplicationId);
		}

        if (lastModifiedBy->Device)
        {
            p_Row->AddField(lastModifiedBy->Device->DisplayName, m_ColListItemLastModifiedByDeviceName);
            p_Row->AddField(lastModifiedBy->Device->Id, m_ColListItemLastModifiedByDeviceId);
        }
		else
		{
			p_Row->RemoveField(m_ColListItemLastModifiedByDeviceName);
			p_Row->RemoveField(m_ColListItemLastModifiedByDeviceId);
		}
    }
	else
	{
		p_Row->RemoveField(m_ColListItemLastModifiedByUserName);
		p_Row->RemoveField(m_ColListItemLastModifiedByUserId);
		p_Row->RemoveField(m_ColListItemLastModifiedByUserEmail);
		p_Row->RemoveField(m_ColListItemLastModifiedByAppName);
		p_Row->RemoveField(m_ColListItemLastModifiedByApplicationId);
		p_Row->RemoveField(m_ColListItemLastModifiedByDeviceName);
		p_Row->RemoveField(m_ColListItemLastModifiedByDeviceId);
	}

    p_Row->AddField(p_ListItem.GetLastModifiedDateTime(), m_ColListItemLastModifiedDateTime);
    p_Row->AddFieldForHyperlink(p_ListItem.GetWebUrl(), m_ColListItemWebUrl);

    PopulateFields(p_List.GetID(), p_ListItem, p_Row);
}

void GridListItems::PopulateFields(const PooledString& p_ListId, const BusinessListItem &p_ListItem, GridBackendRow* p_Row)
{
    if (p_ListItem.GetFields())
    {
        size_t nbFields = p_ListItem.GetFields()->GetUnknownProperties().size();

        for (const auto& field : p_ListItem.GetFields()->GetUnknownProperties())
        {
            auto propertyName = Str::toLower(field.first);

            if (propertyName.find(_YTEXT("@")) != wstring::npos)
                continue;

            auto itt = m_Columns.find(propertyName);
            ASSERT(itt != std::end(m_Columns));
            if (itt != std::end(m_Columns))
            {
                auto col = itt->second;
                rttr::variant value = field.second;
                const bool isString = value.is_type<wstring>() || value.is_type<PooledString>();

                PooledString valStr;
                if (isString)
                {
                    if (value.is_type<wstring>())
                        valStr = PooledString(value.get_value<wstring>());
                    else if (value.is_type<PooledString>())
                        valStr = PooledString(value.get_value<PooledString>());
                    else
                        ASSERT(false);

                    if (col->GetType() == GridBackendUtil::DATE)
                    {
                        YTimeDate timeDate;
                        bool converted = TimeUtil::GetInstance().ConvertTextToDate(valStr, timeDate);
                        ASSERT(converted);
                        if (converted)
                            p_Row->AddField(timeDate, col);
						else
							p_Row->RemoveField(col);
                    }
                    else
                    {
                        p_Row->AddField(PooledString(value.get_value<wstring>()), col);
                    }
                }
                else if (value.is_type<bool>())
                    p_Row->AddField(value.get_value<bool>(), col);
                else if (value.is_type<int32_t>())
                    p_Row->AddField(value.get_value<int32_t>(), col);
                else if (value.is_type<double>())
                    p_Row->AddField(value.get_value<double>(), col);
				else
					p_Row->RemoveField(col);
            }
        }

        // For every field not in column cache for this list, put a grayed out cell
        auto ittCache = m_ListColumns.find(p_ListId);

        ASSERT(ittCache != m_ListColumns.end());
        if (ittCache != m_ListColumns.end())
        {
            for (const auto& keyVal : m_Columns)
            {
                const auto& colName = keyVal.first;
                const auto& column = keyVal.second;

                if (ittCache->second.find(colName) == ittCache->second.end())
                    p_Row->AddField(_YTEXT(""), column, GridBackendUtil::ICON_NOVALUE).SetColor(RGB(248, 248, 248));
            }
        }
    }
}

void GridListItems::UpdateListColumnsCache(const PooledString& p_ListId, const wstring& p_PropertyName)
{
    auto listColumnsItt = m_ListColumns.find(p_ListId);

    PooledString propertyName = Str::toLower(p_PropertyName);
    if (listColumnsItt != std::end(m_ListColumns))
    {
        auto& item = *listColumnsItt;
        item.second.insert(propertyName);
    }
    else
    {
        m_ListColumns[p_ListId] = std::set<PooledString>{ propertyName };
    }
}

void GridListItems::AddIdentityField(const boost::YOpt<IdentitySet>& identitySet, GridBackendRow* p_Row, GridBackendColumn* p_Column)
{
    if (identitySet)
    {
        const IdentitySet& idSet = *identitySet;
        if (idSet.Application)
            p_Row->AddField(idSet.Application->DisplayName, p_Column);
        else if (idSet.Device)
            p_Row->AddField(idSet.Device->DisplayName, p_Column);
        else if (idSet.User)
            p_Row->AddField(idSet.User->DisplayName, p_Column);
		else
			p_Row->RemoveField(p_Column);
    }
	else
	{
		p_Row->RemoveField(p_Column);
	}
}