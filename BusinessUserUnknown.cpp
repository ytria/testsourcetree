#include "BusinessUserUnknown.h"

#include "BusinessGroup.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessUserUnknown>("Unknown") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Unknown"))))
		.constructor()(policy::ctor::as_object)
		;
}
