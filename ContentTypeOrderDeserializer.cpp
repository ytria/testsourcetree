#include "ContentTypeOrderDeserializer.h"

#include "JsonSerializeUtil.h"

void ContentTypeOrderDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("default"), m_Data.Default, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("position"), m_Data.Position, p_Object);
}
