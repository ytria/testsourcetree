#include "TryLoadSnapshotCommand.h"

#include "CommandDispatcher.h"
#include "DlgCreateLoadSessionHTML.h"
#include "GridUtil.h"
#include "MainFrame.h"
#include "Office365Admin.h"
#include "Sapio365Session.h"
#include "SessionLoaderFactory.h"
#include "SessionSwitcher.h"
#include "SessionsSqlEngine.h"

TryLoadSnapshotCommand::TryLoadSnapshotCommand()
	:IActionCommand(_YTEXT(""))
{
}

void TryLoadSnapshotCommand::SetParentCommandForNextExecute(const Command* p_Command)
{
	m_ParentCommand = p_Command;
}

void TryLoadSnapshotCommand::SetSnapshotLoader(std::shared_ptr<GridSnapshot::Loader> p_Loader)
{
	m_Loader = p_Loader;
}

void TryLoadSnapshotCommand::ExecuteImpl() const
{
	ASSERT(nullptr != m_ParentCommand
		&& m_ParentCommand->GetCommandInfo().Data().is_type<std::shared_ptr<TryLoadSnapshotCommand>>()
		&& this == m_ParentCommand->GetCommandInfo().Data().get_value<std::shared_ptr<TryLoadSnapshotCommand>>().get());

	const auto& sessionIdentifier = m_ParentCommand->GetSessionIdentifier();

	ASSERT(m_Loader);
	if (m_Loader)
	{
		const auto& metadata = m_Loader->GetMetadata();

		// If mode is Restore Point, access must not be Public.
		ASSERT(!metadata.m_Mode || GridSnapshot::Options::Mode::Photo == *metadata.m_Mode
			|| !metadata.m_Access || GridSnapshot::Options::Access::Tenant == *metadata.m_Access);

		bool shouldAskForSession = false;
		if (!metadata.m_Access || *metadata.m_Access == GridSnapshot::Options::Access::Tenant)
		{
			shouldAskForSession = sessionIdentifier == SessionIdentifier();

			if (!shouldAskForSession)
			{
				auto session = Sapio365Session::Find(sessionIdentifier);
				shouldAskForSession = !session || !session->IsConnected() || *metadata.m_SessionRef != session->GetRBACTenantName(true);
			}
		}
		else
		{
			ASSERT(*metadata.m_Access == GridSnapshot::Options::Access::Public);
		}

		if (!shouldAskForSession)
		{
			ASSERT(metadata.m_Criteria && metadata.m_GridAutomationName);

			ModuleCriteria criteria;
			if (metadata.m_Criteria)
			{
				auto success = criteria.Deserialize(*metadata.m_Criteria);
				ASSERT(success);
			}
			else
			{
				LoggerService::Debug(_T("TryLoadSnapshotCommand::Execute: No Module criteria"));
			}

			if (metadata.m_GridAutomationName)
			{
				auto moduleTarget = GridUtil::ModuleTarget(*metadata.m_GridAutomationName);

				CommandInfo info;
				info.SetOrigin(criteria.m_Origin);
				info.Data() = m_Loader;
				HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);
				Command command(m_ParentCommand->GetLicenseContext(), m_ParentCommand->GetSourceWindow().GetHWND(), moduleTarget, Command::ModuleTask::LoadSnapshot, info, { nullptr, _YTEXT(""), nullptr, nullptr });
				command.SetSessionIdentifier(sessionIdentifier);
				CommandDispatcher::GetInstance().Execute(command);
			}
			else
			{
				LoggerService::Debug(_T("TryLoadSnapshotCommand::Execute: No target name. Cancel."));
			}
		}
		else
		{
			auto sessions = SessionsSqlEngine::Get().GetList();
			sessions.erase(std::remove_if(sessions.begin(), sessions.end(), [&tenantName = *metadata.m_SessionRef](const PersistentSession& p_Session)
			{
				// FIXME: Find a way to check role target tenant without loading it...
				// For now, role session are kept, the user has to try it to know of ot works for the snapshot.
				/*if (p_Session.IsRole())
				{
					const auto& rd = RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(p_ID, shared_from_this());
					ASSERT(rd.IsLoaded());
					if (rd.IsLoaded())
					{
						auto key = rd.GetKey();

						key.m_TargetTenant;
					}
				}
				else*/
					return !p_Session.IsRole() && p_Session.GetTenantName() != tenantName;
			}), sessions.end());

			const bool allowRoleOrUA = true; // Allow RBAC or Ultra admin sessions?
			DlgCreateLoadSessionHTML dlg(sessions, !allowRoleOrUA);
			dlg.SetIntro(_T("Select a session"), _YFORMAT(L"This snapshot requires that you connect to tenant (%s)", metadata.m_SessionRef ? metadata.m_SessionRef->c_str() : Str::g_EmptyString.c_str()));
			if (dlg.DoModal() == IDOK)
			{
				if (dlg.ShouldCreateStandardSession())
				{
					Office365AdminApp::Get().CreateStandardSession(*m_ParentCommand);
				}
				else if (dlg.ShouldCreateAdvancedSession())
				{
					Office365AdminApp::Get().CreateAdvancedSession(*m_ParentCommand);
				}
				else if (dlg.ShouldCreateUltraAdminSession())
				{
					Office365AdminApp::Get().CreateUltraAdminSession(_YTEXT(""), _YTEXT(""), _YTEXT(""), _YTEXT(""), *m_ParentCommand, nullptr);
				}
				else
				{
					ASSERT(!dlg.ShouldCreateStandardSession() && !dlg.ShouldCreateAdvancedSession() && !dlg.ShouldCreateUltraAdminSession());

					auto sessionId = dlg.GetSelectedSession();
					auto session = Sapio365Session::Find(sessionId);

					MainFrame* mainFrame = dynamic_cast<MainFrame*>(Office365AdminApp::Get().m_pMainWnd);

					if (session && session->IsConnected())
					{
						//retry with connected session.
						auto command = *m_ParentCommand;
						command.SetSessionIdentifier(session->GetIdentifier());
						CommandDispatcher::GetInstance().Execute(command);
					}
					else
					{
						// load session
						auto loader = SessionLoaderFactory(sessionId).CreateLoader();
						SessionSwitcher(std::move(loader)).Switch().Then([parentCommand = *m_ParentCommand, sessionId, mainFrame](const std::shared_ptr<Sapio365Session>& p_Session)
						{
							auto command = parentCommand;
							if (nullptr != p_Session)
							{
								// In case refresh token was expired or session logged-out, wait for authentication to complete
								RestSession::AuthenticationState state = RestSession::AuthenticationState::NotSet;
								ASSERT(nullptr != mainFrame);
								if (nullptr != mainFrame)
								{
									mainFrame->WaitSyncCacheEvent();
									command.SetLicenseContext(mainFrame->GetLicenseContext());
									auto session = mainFrame->GetSapio365Session();
									ASSERT(session);

									if (session)
									{
										state = session->GetMainMSGraphSession()->WaitAuthenticationState();
										// In case of Role Delegation, wait for all 3 sessions to be sure.
										// Not clear if there's any risk not doing this...
										// (If no role, three GetMSGraphSession() calls returns the same one and only session.
										{
											const auto state1 = session->GetMSGraphSession(Sapio365Session::APP_SESSION)->WaitAuthenticationState();
											const auto state2 = session->GetMSGraphSession(Sapio365Session::USER_SESSION)->WaitAuthenticationState();
											ASSERT(state == state1 && state1 == state2);
										}
									}
								}

								if (RestSession::AuthenticationState::Ready == state)
								{
									command.SetSessionIdentifier(sessionId);
									YCallbackMessage::DoPost([command]()
										{
											CommandDispatcher::GetInstance().Execute(command);//  not ExecuteImpl so that new license context can be validated
										});
								}
								else
									LoggerService::Debug(_YTEXTFORMAT(L"TryLoadSnapshotCommand::Execute: Unable to load session %s", sessionId.m_EmailOrAppId.c_str()).c_str());
							}
							else
								LoggerService::Debug(_YTEXTFORMAT(L"TryLoadSnapshotCommand::Execute: Unable to load session %s", sessionId.m_EmailOrAppId.c_str()).c_str());
						});
					}						
				}
			}
			else
			{
				// Canceled.
				LoggerService::Debug(_T("TryLoadSnapshotCommand::Execute: Select session canceled by user"));
			}
		}
	}
}
