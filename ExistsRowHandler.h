#pragma once

#include "ISqlQueryRowHandler.h"

class ExistsRowHandler : public ISqlQueryRowHandler
{
public:
	ExistsRowHandler();

	void HandleRow(sqlite3_stmt* p_Stmt) override;
	bool Exists() const;

private:
	bool m_Exists;
};

