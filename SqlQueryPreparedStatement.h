#pragma once

#include "ISqlQuery.h"

class SQLiteEngine;

class SqlQueryPreparedStatement : public ISqlQuery
{
public:
	SqlQueryPreparedStatement(SQLiteEngine& p_Engine, const wstring& p_Query, bool p_ThrowOnInitError = true);
	~SqlQueryPreparedStatement();

	void Run() override;
	int GetNbRowsChanged() const;

	void BindString(int p_ParamIndex, const wstring& p_Value);
	void BindInt(int p_ParamIndex, int p_Value);
	void BindNull(int p_ParamIndex);
	void BindInt64(int p_ParamIndex, int64_t p_Value);
	void BindBlob(int p_ParamIndex, const void* p_Data, int p_DataSize);

private:
	void HandleError() const;

	std::string m_Query;
	wstring m_QueryWstring;
	sqlite3_stmt* m_Stmt;
	SQLiteEngine& m_Engine;
};

