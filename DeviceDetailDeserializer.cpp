#include "DeviceDetailDeserializer.h"

#include "JsonSerializeUtil.h"

void DeviceDetailDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("browser"), m_Data.m_Browser, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("deviceId"), m_Data.m_DeviceId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isCompliant"), m_Data.m_IsCompliant, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isManaged"), m_Data.m_IsManaged, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("operatingSystem"), m_Data.m_OperatingSystem, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("trustType"), m_Data.m_TrustType, p_Object);
}
