#pragma once
#include "DlgFormsHTML.h"

class DlgOnPremAADConnectServer : public DlgFormsHTML
{
public:
	DlgOnPremAADConnectServer(bool p_ShowWarningLoaded, CWnd* p_Parent);

	//Returns true if processed
	bool processPostedData(const IPostedDataTarget::PostedData& data) override;

protected:
	wstring getDialogTitle() const override;
	int getMinWidthOverride() const override;

private:
	void generateJSONScriptData() override;

	boost::YOpt<wstring> m_AADConnectServer;
	boost::YOpt<bool> m_UseMsDsConsistencyGuid;
	const bool m_ShowWarningLoaded;
};

