#include "DbGroupSerializer.h"

#include "AssignedLicenseSerializer.h"
#include "BusinessDrive.h"
#include "CachedUserSerializer.h"
#include "DbDriveSerializer.h"
#include "JsonSerializeUtil.h"
#include "LicenseProcessingStateSerializer.h"
#include "ListSerializer.h"
#include "MsGraphFieldNames.h"
#include "NullableSerializer.h"
#include "OnPremisesProvisioningErrorSerializer.h"
#include "SapioErrorSerializer.h"
#include "TeamSerializer.h"
#include "YtriaFieldsConstants.h"

namespace
{
	std::map<PooledString, std::function<void(const BusinessGroup&, web::json::object&)>> g_Serializers
	{
			{ _YUID(O365_ID),										[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_ID), g.GetID(), o); } }
		,	{ _YUID(O365_GROUP_ALLOWEXTERNALSENDERS),				[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(O365_GROUP_ALLOWEXTERNALSENDERS), g.IsAllowExternalSenders(), o); } }
		,	{ _YUID(O365_GROUP_AUTOSUBSCRIBENEWMEMBERS),			[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(O365_GROUP_AUTOSUBSCRIBENEWMEMBERS), g.IsAutoSubscribeNewMembers(), o); } }
		,	{ _YUID(O365_GROUP_CLASSIFICATION),						[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_CLASSIFICATION), g.GetClassification(), o); } }
		,	{ _YUID(O365_GROUP_CREATEDDATETIME),					[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_GROUP_CREATEDDATETIME), g.GetCreatedDateTime(), o); } }
		,	{ _YUID(O365_GROUP_DISPLAYNAME),						[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_DISPLAYNAME), g.GetDisplayName(), o); } }
		,	{ _YUID(O365_GROUP_DESCRIPTION),						[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_DESCRIPTION), g.GetDescription(), o); } }
		,	{ _YUID(O365_GROUP_GROUPTYPES),							[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_GROUP_GROUPTYPES), g.GetGroupTypes(), o); } }
		,	{ _YUID(O365_GROUP_PROXYADDRESSES),						[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_GROUP_PROXYADDRESSES), g.GetProxyAddresses(), o); } }
		,	{ _YUID(O365_GROUP_MAIL),								[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_MAIL), g.GetMail(), o); } }
		,	{ _YUID(O365_GROUP_MAILENABLED),						[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(O365_GROUP_MAILENABLED), g.IsMailEnabled(), o); } }
		,	{ _YUID(O365_GROUP_MAILNICKNAME),						[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_MAILNICKNAME), g.GetMailNickname(), o); } }
		,	{ _YUID(O365_GROUP_ONPREMISESLASTSYNCDATETIME),			[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_GROUP_ONPREMISESLASTSYNCDATETIME), g.GetOnPremisesLastSyncDateTime(), o); } }
		,	{ _YUID(O365_GROUP_ONPREMISESSECURITYIDENTIFIER),		[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_ONPREMISESSECURITYIDENTIFIER), g.GetOnPremisesSecurityIdentifier(), o); } }
		,	{ _YUID(O365_GROUP_ONPREMISESPROVISIONINGERRORS),		[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(O365_GROUP_ONPREMISESPROVISIONINGERRORS), ListSerializer<OnPremisesProvisioningError, OnPremisesProvisioningErrorSerializer>(g.GetOnPremisesProvisioningErrors()), o); } }
		,	{ _YUID(O365_GROUP_ONPREMISESSYNCENABLED),				[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(O365_GROUP_ONPREMISESSYNCENABLED), g.IsOnPremisesSyncEnabled(), o); } }
		,	{ _YUID(O365_GROUP_SECURITYENABLED),					[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(O365_GROUP_SECURITYENABLED), g.IsSecurityEnabled(), o); } }
		,	{ _YUID(O365_GROUP_UNSEENCOUNT),						[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeInt32(_YUID(O365_GROUP_UNSEENCOUNT), g.GetUnseenCount(), o); } }
		,	{ _YUID(O365_GROUP_VISIBILITY),							[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_VISIBILITY), g.GetVisibility(), o); } }
		,	{ _YUID(O365_GROUP_DELETEDDATETIME),					[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_GROUP_DELETEDDATETIME), g.GetDeletedDateTime(), o); } }
		,	{ _YUID(O365_GROUP_RENEWEDDATETIME),					[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_GROUP_RENEWEDDATETIME), g.GetRenewedDateTime(), o); } }
		,	{ _YUID(O365_GROUP_PREFERREDDATALOCATION),				[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_PREFERREDDATALOCATION), g.GetPreferredDataLocation(), o); } }
		,	{ _YUID(O365_GROUP_RESOURCEBEHAVIOROPTIONS),			[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_GROUP_RESOURCEBEHAVIOROPTIONS), g.GetResourceBehaviorOptions(), o); } }
		,	{ _YUID(O365_GROUP_RESOURCEPROVISIONINGOPTIONS),		[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_GROUP_RESOURCEPROVISIONINGOPTIONS), g.GetResourceProvisioningOptions(), o); } }
		,	{ _YUID(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE),		[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE), g.GetMembershipRuleProcessingState(), o); } }
		,	{ _YUID(O365_GROUP_MEMBERSHIPRULE),						[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_MEMBERSHIPRULE), g.GetMembershipRule(), o); } }
		,	{ _YUID(O365_GROUP_THEME),								[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_THEME), g.GetTheme(), o); } }
		,	{ _YUID(O365_GROUP_PREFERREDLANGUAGE),					[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_PREFERREDLANGUAGE), g.GetPreferredLanguage(), o); } }
		,	{ _YUID(O365_GROUP_HIDEFROMOUTLOOKCLIENTS),				[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(O365_GROUP_HIDEFROMOUTLOOKCLIENTS), g.GetHideFromOutlookClients(), o); } }
		,	{ _YUID(O365_GROUP_HIDEFROMADDRESSLISTS),				[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(O365_GROUP_HIDEFROMADDRESSLISTS), g.GetHideFromAddressLists(), o); } }
		,	{ _YUID(O365_GROUP_LICENSEPROCESSINGSTATE),				[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(O365_GROUP_LICENSEPROCESSINGSTATE), NullableSerializer<LicenseProcessingState, LicenseProcessingStateSerializer>(g.GetLicenseProcessingState()), o); } }
		,	{ _YUID(O365_GROUP_SECURITYIDENTIFIER),					[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_GROUP_SECURITYIDENTIFIER), g.GetSecurityIdentifier(), o); } }
		,	{ _YUID(O365_GROUP_ASSIGNEDLICENSES),					[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(O365_GROUP_ASSIGNEDLICENSES), ListSerializer<BusinessAssignedLicense, AssignedLicenseSerializer>(g.GetAssignedLicenses()), o); } }
		,	{ _YUID(YTRIA_GROUP_GROUPUNIFIEDGUESTSETTINGACTIVATED),	[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(YTRIA_GROUP_GROUPUNIFIEDGUESTSETTINGACTIVATED), g.GetGroupUnifiedGuestSettingTemplateActivated(), o); } }
		,	{ _YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTS),			[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTS), g.GetSettingAllowToAddGuests(), o); } }
		,	{ _YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTSID),			[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTSID), g.GetSettingAllowToAddGuestsId(), o); } }
		//,	{ _YUID(YTRIA_LASTREQUESTEDDATETIME),					[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(YTRIA_LASTREQUESTEDDATETIME), g.GetLastRequestTime(), o); } }
		//,	{ _YUID(YTRIA_FLAGS),									[](const BusinessGroup& g, web::json::object& o)
		//	{
		//		ASSERT(!g.HasFlag(BusinessObject::CANCELED));  // Don't want to store canceled groups
		//		JsonSerializeUtil::SerializeUint32(_YUID(YTRIA_FLAGS), g.GetFlags(), o);
		//	} }
		,	{ _YUID(YTRIA_GROUP_TEAM),								[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_GROUP_TEAM), NullableSerializer<Team, TeamSerializer>(g.GetTeam()), o); } }
		,	{ _YUID(YTRIA_GROUP_CREATEDONBEHALFOF),					[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(YTRIA_GROUP_CREATEDONBEHALFOF), g.GetUserCreatedOnBehalfOf(), o); } }
		,	{ _YUID(YTRIA_GROUP_LCPSTATUS),							[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(YTRIA_GROUP_LCPSTATUS), g.GetLCPStatus(), o); } }
		,	{ _YUID(YTRIA_GROUP_MEMBERSCOUNT),						[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeUint32(_YUID(YTRIA_GROUP_MEMBERSCOUNT), g.GetMembersCount(), o); } }
		,	{ _YUID(YTRIA_GROUP_OWNERS),							[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(YTRIA_GROUP_OWNERS), g.GetOwnerUsers(), o); } }
		,	{ _YUID(YTRIA_DRIVE),									[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_DRIVE), NullableSerializer<BusinessDrive, DbDriveSerializer>(g.GetDrive()), o); } }
		,	{ _YUID(YTRIA_GROUP_ISYAMMER),							[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(YTRIA_GROUP_ISYAMMER), g.GetIsYammer(), o); } }
		,	{ _YUID(YTRIA_GROUP_TEAM_ERROR),						[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_GROUP_TEAM_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(g.GetTeamError()), o); } }
		,	{ _YUID(YTRIA_GROUP_GROUPSETTINGS_ERROR),				[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_GROUP_GROUPSETTINGS_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(g.GetGroupSettingsError()), o); } }
		,	{ _YUID(YTRIA_GROUP_USERCREATEDONBEHALFOF_ERROR),		[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_GROUP_USERCREATEDONBEHALFOF_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(g.GetUserCreatedOnBehalfOfError()), o); } }
		,	{ _YUID(YTRIA_GROUP_OWNERS_ERROR),						[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_GROUP_OWNERS_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(g.GetOwnersError()), o); } }
		,	{ _YUID(YTRIA_GROUP_LCPSTATUS_ERROR),					[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_GROUP_LCPSTATUS_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(g.GetLCPStatusError()), o); } }
		,	{ _YUID(YTRIA_GROUP_ISYAMMER_ERROR),					[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_GROUP_ISYAMMER_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(g.GetIsYammerError()), o); } }
		,	{ _YUID(YTRIA_DRIVE_ERROR),								[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_DRIVE_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(g.GetDriveError()), o); } }
		//,	{ _YUID(YTRIA_ERROR),									[](const BusinessGroup& g, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(g.GetError()), o); } }

		// request date per block
#define DATE_FIELD_FOR(_blockId_) \
		,	{ SqlCacheConfig<GroupCache>::DataBlockDateFieldById((_blockId_)).c_str(), [](const BusinessGroup& g, web::json::object& o) \
				{ \
					const auto& date = g.GetDataDate(_blockId_); \
					/* You must provide a date... */ \
					ASSERT(date.is_initialized()); \
					JsonSerializeUtil::SerializeTimeDate(SqlCacheConfig<GroupCache>::DataBlockDateFieldById((_blockId_)), date ? date : g.GetLastRequestTime(), o); \
				} \
			} \

		DATE_FIELD_FOR(GBI::MIN)
		DATE_FIELD_FOR(GBI::LIST)
		DATE_FIELD_FOR(GBI::SYNCV1)
		DATE_FIELD_FOR(GBI::SYNCBETA)
		DATE_FIELD_FOR(GBI::CREATEDONBEHALFOF)
		DATE_FIELD_FOR(GBI::TEAM)
		DATE_FIELD_FOR(GBI::GROUPSETTINGS)
		DATE_FIELD_FOR(GBI::LIFECYCLEPOLICIES)
		DATE_FIELD_FOR(GBI::OWNERS)
		DATE_FIELD_FOR(GBI::MEMBERSCOUNT)
		DATE_FIELD_FOR(GBI::DRIVE)	
		DATE_FIELD_FOR(GBI::ISYAMMER)

#undef DATE_FIELD_FOR
	};
}

const std::vector<PooledString>& DbGroupSerializer::MemoryCacheRequestFields()
{
	static const std::vector<PooledString> g_MemoryCacheRequestFields
	{
		_YUID(O365_ID),
		_YUID(O365_GROUP_DISPLAYNAME),
		_YUID(O365_GROUP_GROUPTYPES),
		_YUID(O365_GROUP_MAILENABLED),
		_YUID(O365_GROUP_SECURITYENABLED),
		_YUID(O365_GROUP_RESOURCEPROVISIONINGOPTIONS), // For isTeam
	};

	return g_MemoryCacheRequestFields;
}

const std::vector<PooledString>& DbGroupSerializer::NotInMemoryCacheListRequestFields()
{
	static const std::vector<PooledString> g_NotCachedListRequestFields
	{
		_YUID(O365_GROUP_CLASSIFICATION),
		_YUID(O365_GROUP_CREATEDDATETIME),
		_YUID(O365_GROUP_RENEWEDDATETIME),
		_YUID(O365_GROUP_DESCRIPTION),
		_YUID(O365_GROUP_PROXYADDRESSES),
		_YUID(O365_GROUP_MAIL),
		_YUID(O365_GROUP_MAILNICKNAME),
		_YUID(O365_GROUP_ONPREMISESLASTSYNCDATETIME),
		_YUID(O365_GROUP_ONPREMISESSECURITYIDENTIFIER),
		_YUID(O365_GROUP_ONPREMISESPROVISIONINGERRORS),
		_YUID(O365_GROUP_ONPREMISESSYNCENABLED),
		_YUID(O365_GROUP_VISIBILITY),
		_YUID(O365_GROUP_RESOURCEBEHAVIOROPTIONS),
		_YUID(O365_GROUP_PREFERREDDATALOCATION),
		_YUID(O365_GROUP_LICENSEPROCESSINGSTATE),
		_YUID(O365_GROUP_SECURITYIDENTIFIER),
		_YUID(O365_GROUP_ASSIGNEDLICENSES),
    };

	return g_NotCachedListRequestFields;
}

const std::vector<PooledString>& DbGroupSerializer::SyncOnlyRequestFields_v1()
{
	static const std::vector<PooledString> g_SyncOnlyRequestFields_v1
	{
		_YUID(O365_GROUP_ALLOWEXTERNALSENDERS),
		_YUID(O365_GROUP_AUTOSUBSCRIBENEWMEMBERS),
		_YUID(O365_GROUP_UNSEENCOUNT),
		_YUID(O365_GROUP_HIDEFROMOUTLOOKCLIENTS),
		_YUID(O365_GROUP_HIDEFROMADDRESSLISTS),
	};

	return g_SyncOnlyRequestFields_v1;
}

const std::vector<PooledString>& DbGroupSerializer::SyncOnlyRequestFields_beta()
{
	static const std::vector<PooledString> g_SyncOnlyRequestFields_beta
	{
		_YUID(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE),
		_YUID(O365_GROUP_MEMBERSHIPRULE),
		_YUID(O365_GROUP_THEME),
		_YUID(O365_GROUP_PREFERREDLANGUAGE),
	};

	return g_SyncOnlyRequestFields_beta;
}

const std::vector<PooledString>& DbGroupSerializer::CreatedOnBehalfOfRequestFields()
{
	static const std::vector<PooledString> g_CreatedOnBehalfOfRequestFields
	{
		_YUID(YTRIA_GROUP_CREATEDONBEHALFOF),
		_YUID(YTRIA_GROUP_USERCREATEDONBEHALFOF_ERROR),
	};

	return g_CreatedOnBehalfOfRequestFields;
}

const std::vector<PooledString>& DbGroupSerializer::TeamRequestFields()
{
	static const std::vector<PooledString> g_TeamRequestFields
	{
		_YUID(YTRIA_GROUP_TEAM),
		_YUID(YTRIA_GROUP_TEAM_ERROR),
	};

	return g_TeamRequestFields;
}

const std::vector<PooledString>& DbGroupSerializer::GroupSettingsRequestFields()
{
	static const std::vector<PooledString> g_GroupSettingsRequestFields
	{
		_YUID(YTRIA_GROUP_GROUPUNIFIEDGUESTSETTINGACTIVATED),
		_YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTS),
		_YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTSID),
		_YUID(YTRIA_GROUP_GROUPSETTINGS_ERROR),
	};

	return g_GroupSettingsRequestFields;
}

const std::vector<PooledString>& DbGroupSerializer::LifeCyclePoliciesRequestFields()
{
	static const std::vector<PooledString> g_LifeCyclePoliciesRequestFields
	{
		_YUID(YTRIA_GROUP_LCPSTATUS),
		_YUID(YTRIA_GROUP_LCPSTATUS_ERROR),
	};

	return g_LifeCyclePoliciesRequestFields;
}

const std::vector<PooledString>& DbGroupSerializer::OwnersRequestFields()
{
	static const std::vector<PooledString> g_OwnersRequestFields
	{
		_YUID(YTRIA_GROUP_OWNERS),
		_YUID(YTRIA_GROUP_OWNERS_ERROR),
	};

	return g_OwnersRequestFields;
}

const std::vector<PooledString>& DbGroupSerializer::MembersCountRequestFields()
{
	static const std::vector<PooledString> g_MembersCountRequestFields
	{
		_YUID(YTRIA_GROUP_MEMBERSCOUNT),
	};

	return g_MembersCountRequestFields;
}

const std::vector<PooledString>& DbGroupSerializer::DriveRequestFields()
{
	static const std::vector<PooledString> g_DriveRequestFields
	{
		_YUID(YTRIA_DRIVE),
		_YUID(YTRIA_DRIVE_ERROR), //FIXME: Not handled yet
	};

	return g_DriveRequestFields;
}

const std::vector<PooledString>& DbGroupSerializer::IsYammerRequestFields()
{
	static const std::vector<PooledString> g_IsYammerRequestFields
	{
		_YUID(YTRIA_GROUP_ISYAMMER),
		_YUID(YTRIA_GROUP_ISYAMMER_ERROR),
	};

	return g_IsYammerRequestFields;
}

DbGroupSerializer::DbGroupSerializer(const BusinessGroup& p_Group, const std::vector<PooledString>& p_Fields)
	: m_Group(p_Group)
	, m_Fields(p_Fields)
{
	// No duplicates allowed. Eventually consider automatically removing duplicates.
	ASSERT(std::set<PooledString>(m_Fields.begin(), m_Fields.end()).size() == m_Fields.size());
}

DbGroupSerializer::DbGroupSerializer(const BusinessGroup& p_Group, const GBI p_BlockId)
	: DbGroupSerializer(p_Group, SqlCacheConfig<GroupCache>::DataBlockFieldsById(p_BlockId))
{
	m_DataDateField = SqlCacheConfig<GroupCache>::DataBlockDateFieldById(p_BlockId);
}

void DbGroupSerializer::Serialize()
{
	m_Json = web::json::value::object();
	auto& obj = m_Json.as_object();

	for (const auto& field : m_Fields)
	{
		ASSERT(g_Serializers.end() != g_Serializers.find(field));
		g_Serializers[field](m_Group, obj);
	}

	if (!m_DataDateField.IsEmpty())
	{
		ASSERT(g_Serializers.end() != g_Serializers.find(m_DataDateField));
		g_Serializers[m_DataDateField](m_Group, obj);
	}
}
