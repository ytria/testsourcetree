#include "FrameChannels.h"

IMPLEMENT_DYNAMIC(FrameChannels, GridFrameBase)

O365Grid& FrameChannels::GetGrid()
{
    return m_GridChannels;
}

void FrameChannels::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.SetOrigin(GetModuleCriteria().m_Origin);
    ASSERT(ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer);
    if (ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer)
        info.GetIds() = GetModuleCriteria().m_IDs;
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Channel, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameChannels::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.SetOrigin(GetOrigin());
    info.Data() = p_RefreshData;
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Channel, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

FrameChannels::FrameChannels(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
    : GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
	m_CreateAddRemoveToSelection = true;
	m_CreateRefreshPartial = true;
	m_CreateShowContext = true;
}

void FrameChannels::ShowChannels(const O365DataMap<BusinessGroup, std::vector<BusinessChannel>>& p_Channels, const std::vector<BusinessChannel>& p_LoadedMoreChannels, bool p_FullPurge)
{
	m_GridChannels.BuildView(p_Channels, p_LoadedMoreChannels, p_FullPurge);
}

void FrameChannels::UpdateChannelsLoadMore(const vector<BusinessChannel>& p_Channels)
{
	m_GridChannels.UpdateChannelsLoadMore(p_Channels);
}

void FrameChannels::createGrid()
{
    m_GridChannels.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameChannels::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigGroups();
}

bool FrameChannels::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewDataEditGroups(tab);

	{
		auto dataGroup = findGroup(tab, g_ActionsDataGroup.c_str());
		ASSERT(nullptr != dataGroup);
		if (nullptr != dataGroup)
		{
			auto ctrl = dataGroup->Add(xtpControlButton, ID_CHANNELSGRID_LOADMORE);
			GridFrameBase::setImage({ ID_CHANNELSGRID_LOADMORE }, IDB_LOADMORE, xtpImageNormal);
			GridFrameBase::setImage({ ID_CHANNELSGRID_LOADMORE }, IDB_LOADMORE_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	{
		auto group = tab.AddGroup(g_ActionsLinkChannel.c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addNewModuleCommandControl(*group, ID_CHANNELSGRID_SHOWMESSAGES, { ID_CHANNELSGRID_SHOWMESSAGES_FRAME, ID_CHANNELSGRID_SHOWMESSAGES_NEWFRAME }, { IDB_CHANNELS_SHOW_MESSAGES, IDB_CHANNELS_SHOW_MESSAGES_16X16 });
			addNewModuleCommandControl(*group, ID_CHANNELSGRID_SHOWMEMBERS, { ID_CHANNELSGRID_SHOWMEMBERS_FRAME, ID_CHANNELSGRID_SHOWMEMBERS_NEWFRAME }, { IDB_CHANNELS_SHOW_MEMBERS, IDB_CHANNELS_SHOW_MEMBERS_16X16 });
			addNewModuleCommandControl(*group, ID_CHANNELSGRID_SHOWSITES, { ID_CHANNELSGRID_SHOWSITES_FRAME, ID_CHANNELSGRID_SHOWSITES_NEWFRAME }, { IDB_CHANNELS_SHOW_SITES, IDB_CHANNELS_SHOW_SITES_16X16 });
			addNewModuleCommandControl(*group, ID_CHANNELSGRID_SHOWDRIVEITEMS, { ID_CHANNELSGRID_SHOWDRIVEITEMS_FRAME, ID_CHANNELSGRID_SHOWDRIVEITEMS_NEWFRAME }, { IDB_CHANNELS_SHOW_DRIVEITEMS, IDB_CHANNELS_SHOW_DRIVEITEMS_16X16 });
		}
	}

	CreateLinkGroups(tab, false, true);

	return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameChannels::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
    AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	const bool newFrame = p_Action->IsParamTrue(g_NewFrame);

	if (IsActionSelectedChannelLoadMore(p_Action))
	{
		p_CommandID = ID_CHANNELSGRID_LOADMORE;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedShowChannelMessages(p_Action))
	{
		p_CommandID = newFrame ? ID_CHANNELSGRID_SHOWMESSAGES_NEWFRAME : ID_CHANNELSGRID_SHOWMESSAGES_FRAME;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedShowChannelMembers(p_Action))
	{
		p_CommandID = newFrame ? ID_CHANNELSGRID_SHOWMEMBERS_NEWFRAME : ID_CHANNELSGRID_SHOWMEMBERS_FRAME;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedShowChannelSites(p_Action))
	{
		p_CommandID = newFrame ? ID_CHANNELSGRID_SHOWSITES_NEWFRAME : ID_CHANNELSGRID_SHOWSITES_FRAME;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}

    return statusRV;
}
