#pragma once

#include "OnPremiseGroup.h"

class O365Grid;

class OnPremiseGroupsColumns
{
public:
	bool Create(O365Grid & p_Grid);

	static const wstring& GetTitle(const wstring& p_YUID);

	void SetValue(OnPremiseGroup& p_OnPremGroup, GridBackendColumn* p_Col, const GridBackendField& p_Field) const;

	vector<GridBackendField> GetFields(GridBackendRow& p_Row);
	void SetBlankFields(GridBackendRow& p_Row);

	const vector<GridBackendColumn*>& GetColumns() const;
	const vector<GridBackendColumn*>& GetCommonColumns() const;

	GridBackendColumn* m_ColCommonDisplayName = nullptr;
	GridBackendColumn* m_ColCommonMetaType = nullptr;

	GridBackendColumn* m_ColAdminCount = nullptr;
	GridBackendColumn* m_ColCanonicalName = nullptr;
	GridBackendColumn* m_ColCN = nullptr;
	GridBackendColumn* m_ColCreated = nullptr;
	GridBackendColumn* m_ColCreateTimeStamp = nullptr;
	GridBackendColumn* m_ColDeleted = nullptr;
	GridBackendColumn* m_ColDescription = nullptr;
	GridBackendColumn* m_ColDisplayName = nullptr;
	GridBackendColumn* m_ColDistinguishedName = nullptr;
	GridBackendColumn* m_ColDSCorePropagationData = nullptr;
	GridBackendColumn* m_ColGroupCategory = nullptr;
	GridBackendColumn* m_ColGroupScope = nullptr;
	GridBackendColumn* m_ColGroupType = nullptr;
	GridBackendColumn* m_ColHomePage = nullptr;
	GridBackendColumn* m_ColInstanceType = nullptr;
	GridBackendColumn* m_ColIsCriticalSystemObject = nullptr;
	GridBackendColumn* m_ColIsDeleted = nullptr;
	GridBackendColumn* m_ColLastKnownParent = nullptr;
	GridBackendColumn* m_ColManagedBy = nullptr;
	GridBackendColumn* m_ColMember = nullptr;
	GridBackendColumn* m_ColMemberOf = nullptr;
	GridBackendColumn* m_ColMembers = nullptr;
	GridBackendColumn* m_ColModified = nullptr;
	GridBackendColumn* m_ColModifyTimeStamp = nullptr;
	GridBackendColumn* m_ColName = nullptr;
	GridBackendColumn* m_ColNTSecurityDescriptor = nullptr;
	GridBackendColumn* m_ColObjectCategory = nullptr;
	GridBackendColumn* m_ColObjectClass = nullptr;
	GridBackendColumn* m_ColObjectGUID = nullptr;
	GridBackendColumn* m_ColObjectSid = nullptr;
	GridBackendColumn* m_ColProtectedFromAccidentalDeletion = nullptr;
	GridBackendColumn* m_ColSamAccountName = nullptr;
	GridBackendColumn* m_ColSAMAccountType = nullptr;
	GridBackendColumn* m_ColSDRightsEffective = nullptr;
	GridBackendColumn* m_ColSID = nullptr;
	GridBackendColumn* m_ColSIDHistory = nullptr;
	GridBackendColumn* m_ColSystemFlags = nullptr;
	GridBackendColumn* m_ColUSNChanged = nullptr;
	GridBackendColumn* m_ColUSNCreated = nullptr;
	GridBackendColumn* m_ColWhenChanged = nullptr;
	GridBackendColumn* m_ColWhenCreated = nullptr;

private:
	vector<GridBackendColumn*> m_Columns;
	vector<GridBackendColumn*> m_CommonColumns;
	bool m_Created = false;

	std::map<GridBackendColumn*, std::function<void(OnPremiseGroup&, const boost::YOpt<PooledString>&)>> m_StringSetters;
	std::map<GridBackendColumn*, std::function<void(OnPremiseGroup&, const boost::YOpt<bool>&)>> m_BoolSetters;
	std::map<GridBackendColumn*, std::function<void(OnPremiseGroup&, const boost::YOpt<YTimeDate>&)>> m_DateSetters;
	std::map<GridBackendColumn*, std::function<void(OnPremiseGroup&, const boost::YOpt<int32_t>&)>> m_IntSetters;
	std::map<GridBackendColumn*, std::function<void(OnPremiseGroup&, const boost::YOpt<vector<PooledString>>&)>> m_StringListSetters;
	std::map<GridBackendColumn*, std::function<void(OnPremiseGroup&, const boost::YOpt<vector<YTimeDate>>&)>> m_DateListSetters;
};

