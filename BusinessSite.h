#pragma once

#include "BusinessDrive.h"
#include "BusinessObject.h"
#include "Root.h"
#include "SharepointIds.h"
#include "Site.h"
#include "SiteCollection.h"

class CachedSite;
class BusinessSite : public BusinessObject
{
public:
    BusinessSite() = default;
    BusinessSite(const Site& p_JsonSite);
	BusinessSite(const CachedSite& p_CachedSite);

	void SetValuesFrom(const Site& p_JsonSite);
	void SetValuesFrom(const CachedSite& p_CachedSite);

    boost::YOpt<YTimeDate> GetCreatedDateTime() const;
    void SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_Val);
    
    boost::YOpt<PooledString> GetDescription() const;
    void SetDescription(const boost::YOpt<PooledString>& p_Val);
    
    boost::YOpt<PooledString> GetDisplayName() const;
    void SetDisplayName(const boost::YOpt<PooledString>& p_Val);
    
    boost::YOpt<YTimeDate> GetLastModifiedDateTime() const;
    void SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val);
    
    boost::YOpt<PooledString> GetName() const;
    void SetName(const boost::YOpt<PooledString>& p_Val);
    
    boost::YOpt<Root> GetRoot() const;
    void SetRoot(const boost::YOpt<Root>& p_Val);
    
    boost::YOpt<SharepointIds> GetSharepointIds() const;
    void SetSharepointIds(const boost::YOpt<SharepointIds>& p_Val);
    
    boost::YOpt<SiteCollection> GetSiteCollection() const;
    void SetSiteCollection(const boost::YOpt<SiteCollection>& p_Val);
    
    const boost::YOpt<PooledString>& GetWebUrl() const;
    void SetWebUrl(const boost::YOpt<PooledString>& p_Val);

    const vector<BusinessSite>& GetSubSites() const;
    void SetSubSites(const vector<BusinessSite>& p_Val);

	bool operator<(const BusinessSite& p_Other) const;

	virtual vector<PooledString> GetProperties(const wstring& p_PropName) const override;

	const boost::YOpt<BusinessDrive>& GetDrive() const;
	void SetDrive(const boost::YOpt<BusinessDrive>& p_Val);

private:
    boost::YOpt<YTimeDate> m_CreatedDateTime;
    boost::YOpt<PooledString> m_Description;
    boost::YOpt<YTimeDate> m_LastModifiedDateTime;
    boost::YOpt<PooledString> m_Name;
    boost::YOpt<Root> m_Root;
    boost::YOpt<SharepointIds> m_SharepointIds;
    boost::YOpt<SiteCollection> m_SiteCollection;
    boost::YOpt<PooledString> m_WebUrl;

    // Non properties
    vector<BusinessSite> m_SubSites;
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<BusinessDrive> m_Drive;

    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND
    friend class SiteDeserializer;
    friend class DbSiteSerializer;
    friend class SiteNonPropDeserializer;
};

DECLARE_BUSINESSOBJECT_EQUALTO_AND_HASH_SPECIALIZATION(BusinessSite)
