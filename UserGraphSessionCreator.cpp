#include "UserGraphSessionCreator.h"
#include "NetworkUtils.h"
#include "OAuth2Authenticators.h"
#include "SessionTypes.h"
#include "MSGraphCommonData.h"
#include "O365AdminErrorHandler.h"

UserGraphSessionCreator::UserGraphSessionCreator(const wstring& p_Tenant, const wstring& p_Username, const wstring& p_Password)
	:m_Tenant(p_Tenant),
	m_Username(p_Username),
	m_Password(p_Password)
{
	ASSERT(!(m_Username.empty() || m_Tenant.empty()));
	if (m_Username.empty() || m_Tenant.empty())
		throw std::invalid_argument("Username and tenant must be filled.");
}

std::shared_ptr<MSGraphSession> UserGraphSessionCreator::Create()
{
	auto session = std::make_shared<MSGraphSession>(NetworkUtils::GetHttpClientConfig(), nullptr);

	auto authenticator = std::make_shared<ResourceOwnerCredentialsGrantAuthenticator>(session,
		SessionTypes::GetAdvancedSessionAppID(),
		_YTEXT(""),
		m_Username,
		m_Password,
		_YTEXT("user.read offline_access"),
		Rest::GetAzureADEndpoint() + wstring(_YTEXT("/")) + m_Tenant + _YTEXT("/oauth2/v2.0/token"));

	session->SetErrorHandler(std::make_unique<O365AdminErrorHandler>(session));
	session->SetAuthenticator(authenticator);

	auto result = session->Start(nullptr).get();
	m_Result.m_Success = result.state == RestAuthenticationResult::State::Success;
	if (result.state == RestAuthenticationResult::State::Success)
	{
		LoggerService::Debug(_YFORMAT(L"Successfully created user session with username %s", m_Username.c_str()));
	}
	else
	{
		m_Result.m_GraphError = result.error_message;
		LoggerService::Debug(_YFORMAT(L"Unable to create user session with username %s", m_Username.c_str()));
	}

	return session;
}

const wstring& UserGraphSessionCreator::GetTenant() const
{
	return m_Tenant;
}

const wstring& UserGraphSessionCreator::GetUsername() const
{
	return m_Username;
}

const wstring& UserGraphSessionCreator::GetPassword() const
{
	return m_Password;
}
