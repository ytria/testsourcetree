#pragma once

#include "BusinessGroup.h"

// only for identification issues in grids
class BusinessGroupDeleted : public BusinessGroup
{
public:
	using BusinessGroup::BusinessGroup;

private:
	RTTR_ENABLE(BusinessGroup)
};
