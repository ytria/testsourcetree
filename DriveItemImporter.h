#pragma once

class GridDriveItems;
class GridBackendRow;
class DlgLoading;

class DriveItemImporter
{
public:
	DriveItemImporter(GridDriveItems& p_Grid);
	virtual ~DriveItemImporter() = default;

public:
	virtual void Run(const wstring& p_Path, const std::set<GridBackendRow*>& p_ParentRows, std::shared_ptr<DlgLoading>& p_Dlg = std::shared_ptr<DlgLoading>()) = 0;

protected:
	GridDriveItems& m_Grid;
	boost::YOpt<bool> m_Replacing;
};