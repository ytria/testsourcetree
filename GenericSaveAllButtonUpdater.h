#pragma once

#include "IButtonUpdater.h"

class GridFrameBase;

class GenericSaveAllButtonUpdater : public IButtonUpdater
{
public:
	GenericSaveAllButtonUpdater(GridFrameBase& p_Frame);
	void Update(CCmdUI* p_CmdUi) override;

private:
	GridFrameBase& m_Frame;
};

