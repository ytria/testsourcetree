#pragma once

class IPropertySetBuilder
{
public:
	// FIXME: We should get rid of this one eventually
    virtual vector<rttr::property> GetPropertySet() const = 0;

	virtual vector<PooledString> GetStringPropertySet() const;
    virtual ~IPropertySetBuilder() = default;
};

