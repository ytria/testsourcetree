#include "HttpResultWithError.h"

HttpResultWithError::HttpResultWithError()
{

}

HttpResultWithError::HttpResultWithError(const boost::YOpt<RestResultInfo>& p_ResultInfo, const boost::YOpt<SapioError>& p_Error)
    : m_ResultInfo(p_ResultInfo)
	, m_Error(p_Error)
{
	ASSERT(!m_Error || !m_ResultInfo || (m_ResultInfo->Response.status_code() < 200 || 300 <= m_ResultInfo->Response.status_code()));
}

HttpResultWithError::HttpResultWithError(const boost::YOpt<RestResultInfo>& p_ResultInfo)
	: m_ResultInfo(p_ResultInfo)
{
	if (m_ResultInfo && m_ResultInfo->m_RestException)
		m_Error = HttpError(*m_ResultInfo->m_RestException);
	else if (m_ResultInfo && m_ResultInfo->m_Exception)
		m_Error = SapioError(*m_ResultInfo->m_Exception);

	ASSERT(!m_Error || !m_ResultInfo || (m_ResultInfo->Response.status_code() < 200 || 300 <= m_ResultInfo->Response.status_code()));
}
