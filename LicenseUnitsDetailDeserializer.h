#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "LicenseUnitsDetail.h"

class LicenseUnitsDetailDeserializer : public JsonObjectDeserializer, public Encapsulate<LicenseUnitsDetail>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

