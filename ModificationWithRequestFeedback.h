#pragma once

#include "IMainItemModification.h"
#include "GridBackendField.h"

class ModificationWithRequestFeedback : public IMainItemModification
{
public:
	ModificationWithRequestFeedback(const wstring& p_ObjName);
	virtual ~ModificationWithRequestFeedback() = default;
	
	virtual void Apply() = 0;
	virtual void Revert() = 0;

	void RefreshState() override;

	void SetResultError(const wstring& p_Error);
	bool HasResultError() const;
	const wstring& GetResultError() const;

	const std::vector<GridBackendField>& GetRowKey() const;

protected:
	std::vector<GridBackendField> m_RowKey;

private:
	wstring m_Error;
};

