#pragma once

#include "DelegateUser.h"
#include "ExchangeObjectDeserializer.h"

class DelegateUserDeserializer : public Ex::ExchangeObjectDeserializer
{
public:
    void DeserializeObject(const XERCES_CPP_NAMESPACE::DOMElement& p_Envelope) override;

    const Ex::DelegateUser& GetObject() const;

private:
    Ex::DelegateUser m_DelegateUser;
};

