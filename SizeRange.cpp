#include "SizeRange.h"

#include "Str.h"

wstring SizeRange::ToString() const
{
	if (m_MinimumSize && m_MaximumSize)
		return _YTEXTFORMAT(L"[%s%s, %s%s]", Str::getStringFromNumber(*m_MinimumSize).c_str(), NumberFormat::GetByteSuffixes()[1].c_str(), Str::getStringFromNumber(*m_MaximumSize).c_str(), NumberFormat::GetByteSuffixes()[1].c_str());
	ASSERT(false);
	return _YTEXT("");
}
