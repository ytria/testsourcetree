#include "ChildrenMailFoldersRequester.h"

#include "ValueListDeserializer.h"
#include "MailFolderDeserializer.h"
#include "MSGraphCommonData.h"
#include "MessageDeserializer.h"
#include "BusinessMailFolder.h"
#include "MSGraphSession.h"
#include "MultiObjectsPageRequestLogger.h"
#include "PaginatedRequestResults.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "MessageInMailfolderPropertySet.h"
#include "RESTUtils.h"
#include "MSGraphUtil.h"
#include "MessageListRequester.h"
#include "MsGraphHttpRequestLogger.h"

using namespace Rest;

ChildrenMailFoldersRequester::ChildrenMailFoldersRequester(PooledString p_UserId, PooledString p_MailFolderId, bool p_IncludeMessages, const std::shared_ptr<IPageRequestLogger>& p_Logger)
    : m_UserId(p_UserId)
	, m_MailFolderId(p_MailFolderId)
	, m_IncludeMessages(p_IncludeMessages)
	, m_Logger(p_Logger)
{
}

TaskWrapper<void> ChildrenMailFoldersRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<BusinessMailFolder, MailFolderDeserializer>>();
    
	web::uri_builder uri;
	uri.append_path(USERS_PATH);
	uri.append_path(m_UserId);
	uri.append_path(MAILFOLDERS_PATH);
	uri.append_path(m_MailFolderId);
	uri.append_path(CHILDFOLDERS_PATH);
	
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Deserializer, 100, m_Logger, httpLogger, p_TaskData.GetOriginator()).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData)
		.Then([callback = m_MessagesListCallback
			, cutOff = m_CutOffDateTime
			, customFilter = m_CustomFilter
			, mailFolderDeserializer = m_Deserializer
			, includeMessages = m_IncludeMessages
			, userId = m_UserId
			, p_Session
			, p_TaskData
			, logger = m_Logger] {
        for (auto& mailFolder : mailFolderDeserializer->GetData())
        {
            if (p_TaskData.IsCanceled())
                break;

            if (includeMessages)
            {
				auto newLogger = logger->Clone();
				newLogger->SetLogMessage(_T("messages"));
				MessageListRequester messagesReq(MessageInMailfolderPropertySet(), userId, mailFolder.m_Id, newLogger);
				messagesReq.SetCutOffDateTime(cutOff);
				messagesReq.SetCustomFilter(customFilter);
				messagesReq.Send(p_Session, p_TaskData).GetTask().wait();

				if (callback)
					callback(messagesReq);
				
				mailFolder.m_Messages = messagesReq.GetData();
            }

            if (mailFolder.m_ChildFolderCount && mailFolder.m_ChildFolderCount > 0)
            {
				logger->SetContextualInfo(mailFolder.m_DisplayName ? *mailFolder.m_DisplayName : _YTEXT(""));

                ChildrenMailFoldersRequester childRequester(userId, mailFolder.GetID(), includeMessages, logger->Clone());
				childRequester.SetMessagesCutOffDateTime(cutOff);
				childRequester.SetMessagesCustomFilter(customFilter);
                childRequester.Send(p_Session, p_TaskData).GetTask().wait();
                mailFolder.m_ChildrenMailFolders = childRequester.GetData();
            }
        }
    }, p_TaskData.GetToken());
}

void ChildrenMailFoldersRequester::SetMessagesCutOffDateTime(const wstring& p_CutOff)
{
	m_CutOffDateTime = p_CutOff;
}

void ChildrenMailFoldersRequester::SetMessagesCustomFilter(const ODataFilter& p_CustomFilter)
{
	m_CustomFilter = p_CustomFilter;
}

void ChildrenMailFoldersRequester::SetMessagesListCallback(const std::function<void(const MessageListRequester&)>& p_Callback)
{
	m_MessagesListCallback = p_Callback;
}

vector<BusinessMailFolder>& ChildrenMailFoldersRequester::GetData()
{
    return m_Deserializer->GetData();
}
