#include "EducationSchoolUpdateRequester.h"
#include "LoggerService.h"
#include "Sapio365Session.h"
#include "O365AdminUtil.h"
#include "MsGraphHttpRequestLogger.h"

EducationSchoolUpdateRequester::EducationSchoolUpdateRequester(const wstring& p_SchoolId, const web::json::value& p_SchoolChanges)
	:m_SchoolId(p_SchoolId),
	m_SchoolChanges(p_SchoolChanges)
{
	ASSERT(p_SchoolChanges.is_object());
}

TaskWrapper<void> EducationSchoolUpdateRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("schools"));
	uri.append_path(m_SchoolId);

	LoggerService::User(_YFORMAT(L"Updating school with id %s", m_SchoolId.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Patch(uri.to_uri(), httpLogger, p_TaskData, WebPayloadJSON(m_SchoolChanges)).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResInfo) {
		*result = Util::GetResult(p_ResInfo);
	});
}

const HttpResultWithError& EducationSchoolUpdateRequester::GetResult() const
{
	return *m_Result;
}
