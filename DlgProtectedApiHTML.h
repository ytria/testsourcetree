#pragma once

#include "DlgFormsHTML.h"
#include "Sapio365Session.h"

class DlgProtectedApiHTML : public DlgFormsHTML
{
public:
	DlgProtectedApiHTML(const std::shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent);

protected:
	virtual void generateJSONScriptData() override;

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

	virtual wstring getDialogTitle() const override;

private:
	web::json::value& addQuestion(const wstring& p_Text);
	web::json::value& addAnswer(const wstring& p_Label, const wstring& p_Text);

	int currentQuestionIndex;
	const std::shared_ptr<Sapio365Session>& m_Session;
};