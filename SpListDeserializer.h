#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "SpList.h"

namespace Sp
{
class ListDeserializer : public JsonObjectDeserializer, public Encapsulate<Sp::List>
{
protected:
    void DeserializeObject(const web::json::object& p_Object) override;
};
}
