#include "VerifiedDomainDeserializer.h"

#include "JsonSerializeUtil.h"

void VerifiedDomainDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("capabilities"), m_Data.Capabilities, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isDefault"), m_Data.IsDefault, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isInitial"), m_Data.IsInitial, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.Name, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.Type, p_Object);
}
