#include "DlgBrowserOpener.h"

#include "DlgBrowser.h"
#include "DlgSpecialExtAdminAccess.h"
#include "LoggerService.h"
#include "Messages.h"
#include "YtriaTranslate.h"
#include "Office365Admin.h"
#include "MainFrame.h"

DlgBrowserOpener::DlgBrowserOpener()
{
}

int DlgBrowserOpener::OpenBrowser(CWnd* p_Parent)
{
	INT_PTR result = IDCANCEL;
	if (p_Parent == nullptr) 
	{
		auto mainframe = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
		ASSERT(nullptr != mainframe);
		p_Parent = mainframe;
	}

	if (nullptr != p_Parent)
	{
		// No oauth2 browser already opened?
		if (nullptr == GetOauth2Browser())
		{
			m_Uri.append_query(_YTEXT("prompt"), _YTEXT("login"));
			LoggerService::Debug(_YTEXTFORMAT(L"DlgBrowserOpener::OpenBrowser: Received WM_OPEN_OAUTH2_BROWSER. Opening browser with url %s", m_Uri.to_string().c_str()));

			auto browser = new DlgBrowser(m_Uri.to_string(), YtriaTranslate::Do(DlgBrowserOpener_OpenBrowser_1, _YLOC("Please sign in")).c_str(), p_Parent);
			SetOauth2Browser(browser);
			result = browser->DoModal();

			if (result == IDCANCEL)
			{
				Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
				ASSERT(nullptr != app);
				if (nullptr != app
					// FIXME: Bad design, bad fix: when browser is opened to change the password of a connected
					// Ultra Admin session, the 'session being loaded' must no be reset, otherwise it fails at MainFrame.cpp,805
					&& nullptr == dynamic_cast<DlgSpecialExtAdminAccess*>(p_Parent))
					app->ResetSapioSessionBeingLoaded();
				m_UserCanceled.set();
			}

			auto b = GetOauth2Browser();
			SetOauth2Browser(nullptr);
			delete b;
		}
	}

    return (int) result;
}

void DlgBrowserOpener::CloseBrowser()
{
	auto mainframe = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainframe);
	if(nullptr != mainframe)
		mainframe->PostMessage(WM_CLOSE_OAUTH2_BROWSER, 0, 0);
}

DlgBrowser* DlgBrowserOpener::GetOauth2Browser()
{
	// Kind of a hack
	auto mainframe = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	DlgBrowser* browser = nullptr;
	ASSERT(nullptr != mainframe);
	if (nullptr != mainframe)
	{
		browser = (DlgBrowser*)mainframe->SendMessage(WM_GET_OAUTH2_BROWSER);
		ASSERT(dynamic_cast<DlgBrowser*>(browser) == browser);
	}
	return browser;
}

void DlgBrowserOpener::SetOauth2Browser(DlgBrowser* p_DlgBrowser)
{
	// Kind of a hack
	auto mainframe = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainframe);
	if (nullptr != mainframe)
		mainframe->SendMessage(WM_SET_OAUTH2_BROWSER, (WPARAM)p_DlgBrowser);
}
