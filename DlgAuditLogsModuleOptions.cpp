#include "DlgAuditLogsModuleOptions.h"

void DlgAuditLogsModuleOptions::generateJSONScriptData()
{
	initMain(_YTEXT("DlgModuleOptions"));

	getGenerator().addIconTextField(_YTEXT("iconText"), _T("Limit the number of audit logs to load by selecting a date/time range. You can request more later."), _YTEXT("fas fa-calendar-alt"));
	addCutOffEditor(_T("Get audit logs started within: "), _T("Started after (excluding)"));

	getGenerator().addIconTextField(_YTEXT("iconText2"), _T("Refine your selection"), _YTEXT("fas fa-search"));
	addFilterEditor();

	getGenerator().addApplyButton(YtriaTranslate::Do(CDlg_OnInitDialog_2, _YLOC("Ok")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str());
}

wstring DlgAuditLogsModuleOptions::getDialogTitle() const
{
	return _T("Load audit logs - Options");
}

std::map<wstring, DlgModuleOptions::FilterFieldProperties> DlgAuditLogsModuleOptions::getFilterFields() const
{
	// https://docs.microsoft.com/en-us/graph/api/directoryaudit-list?view=graph-rest-1.0&tabs=http#attributes-supported-by-filter-parameter
	return
	{
			{ _YTEXT("activityDisplayName"), { _T("Activity"), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQStartsWith } } // eq, startswith

		// FIXME: Our DateTime editor is not good, better with Date (with calendar) but that prevent from using EQ...
		//,	{ _YTEXT("activityDateTime"), { _T("Date"), true, FilterFieldProperties::DateTimeType(), g_FilterOperatorsEQGELE } } // eq, ge, le
		,	{ _YTEXT("activityDateTime"), { _T("Date"), true, FilterFieldProperties::DateType(), g_FilterOperatorsGELE } } // ge, le

		,	{ _YTEXT("loggedByService"), { _T("Service"), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } } // 	eq
		,	{ _YTEXT("initiatedBy/user/id"), { _T("User ID - Initiated by"), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } } // 	eq
		,	{ _YTEXT("initiatedBy/user/displayName"), { _T("User Display Name - Initiated by"), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } } // 	eq
		,	{ _YTEXT("initiatedBy/user/userPrincipalName"), { _T("Username - Initiated by"), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQStartsWith } } // 	eq, startswith
		,	{ _YTEXT("initiatedBy/app/appId"), { _T("Application ID - Initiated by"), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } } // 	eq
		,	{ _YTEXT("initiatedBy/app/displayName"), { _T("Application name displayed in Azure Portal - Initiated by"), true, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } } // 	eq
		//,	{ _YTEXT("targetResources/any(t:t/id eq '{value}')"), { _T(""), false, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } } // 	eq
		//,	{ _YTEXT("targetResources/any(t:t/displayName eq '{value}')"), { _T(""), false, FilterFieldProperties::StringType(), g_FilterOperatorsEQ } } // 	eq
		//,	{ _YTEXT("targetResources/any(x:startswith(x/displayName, '{value}'))"), { _T(""), false, FilterFieldProperties::StringType(), {} } } // 	startswith
	};
}
