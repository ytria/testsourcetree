#pragma once

#include "BusinessAttachment.h"
#include "BaseO365Grid.h"
#include "Command.h"
#include "RoleDelegationUtil.h"

class BusinessUser;
class BusinessMessage;
class GridUpdater;
enum class Origin;

class GridUtil
{
public:
	// Call at shutdown required to prevent call of StringPool after it's deleted.
	static void ClearStatics();

	static void Add365ObjectTypes(CacheGrid& p_Grid);

	static bool IsBusinessContact(const GridBackendRow* p_Row);
	static bool IsBusinessConversation(const GridBackendRow* p_Row);
    static bool IsBusinessConversationThread(const GridBackendRow* p_Row);
	static bool IsBusinessDrive(const GridBackendRow* p_Row);
	static bool IsBusinessDriveItem(const GridBackendRow* p_Row);
	static bool IsBusinessDriveItemFolder(const GridBackendRow* p_Row);
	static bool IsBusinessDriveItemNotebook(const GridBackendRow* p_Row);
	static bool IsBusinessEvent(const GridBackendRow* p_Row);
	static bool IsBusinessGroup(const GridBackendRow* p_Row);
	static bool IsBusinessMailFolder(const GridBackendRow* p_Row);
	static bool IsBusinessMessage(const GridBackendRow* p_Row);
	static bool IsBusinessSite(const GridBackendRow* p_Row);
    static bool IsBusinessList(const GridBackendRow* p_Row);
    static bool IsBusinessUser(const GridBackendRow* p_Row, bool p_TrueForGuest = true);
    static bool IsBusinessListItem(const GridBackendRow* p_Row);
	static bool IsBusinessPost(const GridBackendRow* p_Row);
	static bool IsBusinessChannel(const GridBackendRow* p_Row);
	static bool IsBusinessChatMessageOrReply(const GridBackendRow* p_Row);
	static bool IsBusinessOrgContact(const GridBackendRow* p_Row);
	static bool IsBusinessAADUserConversationMember(const GridBackendRow* p_Row);

	static bool IsBusinessUserDeleted(const GridBackendRow* p_Row);
	static bool IsBusinessGroupDeleted(const GridBackendRow* p_Row);

	//static bool IsHybridUser(const GridBackendRow* p_Row);
	static bool IsOnPremUser(const GridBackendRow* p_Row);

	//static bool IsHybridGroup(const GridBackendRow* p_Row);
	static bool IsOnPremGroup(const GridBackendRow* p_Row);

    struct AttachmentIcons
    {
        int m_FileAttachment = 0;
        int m_ItemAttachment = 0;
        int m_ReferenceAttachment = 0;
    };

    static AttachmentIcons LoadAttachmentIcons(CacheGrid& p_Grid);
    static void SetAttachmentIcon(GridBackendField& p_Field, const AttachmentIcons& p_Icons);

    static bool IsAtLeastOneErrorlessSelected(CacheGrid& p_Grid);
    static int getBusinessTypeId(const GridBackendRow* p_Row);
    template<class RTTRRegisteredType> static bool isBusinessType(const GridBackendRow* p_Row);
	template<class RTTRRegisteredType> static bool hasParentOfBusinessType(const GridBackendRow* p_Row);
	template<class RTTRRegisteredType> static GridBackendRow* getParentOfBusinessType(GridBackendRow* p_Row, bool p_AllowThis);
	template<class RTTRRegisteredType> static bool IsSelectionAtLeastOneOfType(const CacheGrid& p_Grid);
	template<class RTTRRegisteredType> static bool IsSelectionAtLeastOneNotOfType(const CacheGrid& p_Grid);
	template<class RTTRRegisteredType> static bool IsSelectionOrParentAtLeastOfType(const CacheGrid& p_Grid);

	static bool IsSelectionAuthorizedByRoleDelegation(const O365Grid& p_Grid, const RoleDelegationUtil::RBAC_Privilege p_Right, const bool p_CheckAncestorPrivilegeIfPresent = false);
	static bool IsSelectionAuthorizedByRoleDelegation(const O365Grid& p_Grid, const RoleDelegationUtil::RBAC_Privilege p_Right, const std::function<bool(GridBackendRow*)> p_AdditionalCondition, const bool p_CheckAncestorPrivilegeIfPresent = false);
	static vector<GridBackendRow*> GetSelectionAuthorizedByRoleDelegation(const O365Grid& p_Grid, const RoleDelegationUtil::RBAC_Privilege p_Right, const std::function<bool(GridBackendRow*)> p_AdditionalCondition, const bool p_CheckAncestorPrivilegeIfPresent = false);

	static wstring GetColumnsPath(CacheGrid& p_Grid, GridBackendRow* p_Row, const vector<GridBackendColumn*>& p_Columns);

	static void SetUserRowObjectType(CacheGrid& p_Grid, GridBackendRow* p_Row, const BusinessUser& p_User);
	static void SetGroupRowObjectType(CacheGrid& p_Grid, GridBackendRow* p_Row, const BusinessGroup& p_Group);
	static void SetRowObjectType(CacheGrid& p_Grid, GridBackendRow* p_Row, const rttr::variant& p_RowObject);

	static boost::YOpt<PooledString> ExtractHtmlBodyPreview(const boost::YOpt<PooledString>& p_HtmlContent);
	static bool SaveEmailAsEmlFile(const BusinessMessage& p_Message, const vector<BusinessAttachment>& p_Attachments, const wstring& p_FolderPath, bool p_OverwriteExisting);

	static bool ContainsImgWithCID(const wstring& p_HtmlContent);

	static wstring ReplaceCIDsByPaths(const wstring& p_HtmlContent, const map<wstring, wstring>& p_CidsToPaths);

	static bool IsCommandAllowed(const CacheGrid& p_Grid, const int p_CommandID, const Origin p_Origin);

	// p_IsTeam is BusinessGroup::g_Team if a Team and empty if not a Team
	static void SetRowIsTeam(const wstring& p_IsTeam, GridBackendRow* p_RowToUpdate, GridBackendColumn* p_IsTeamColumn, int p_TeamIcon);
	static int AddIconTeam(CacheGrid& p_Grid);

	static void HandleMultiError(GridBackendRow* p_Row, std::vector<std::pair<boost::YOpt<SapioError>, wstring>>&& p_Errors, GridUpdater& p_GridUpdater, const wstring& p_NoPopupSpecificReason = _YTEXT(""));

	static const wstring g_AutoNameClassMembers;
	static const wstring g_AutoNameSchools;
	static const wstring g_AutoNameEduUsers;
    static const wstring g_AutoNameSiteRoles;
    static const wstring g_AutoNameSpUsers;
	static const wstring g_AutoNameDriveItems;
	static const wstring g_AutoNameMessages;
	static const wstring g_AutoNameManagerHierarchy;
    static const wstring g_AutoNameChannels;
	static const wstring g_AutoNameContacts;
    static const wstring g_AutoNameSpGroups;
    static const wstring g_AutoNameDirectoryRoles;
	static const wstring g_AutoNameMailFolders;
	static const wstring g_AutoNameMessageRules;
	static const wstring g_AutoNameEvents;
	static const wstring g_AutoNameUserGroups;
	static const wstring g_AutoNameGroups;
	static const wstring g_AutoNameGroupsRecycleBin;
	static const wstring g_AutoNameGroupsHierarchy;
	static const wstring g_AutoNameGroupsOwnersHierarchy;
	static const wstring g_AutoNameGroupsAuthorsHierarchy;
	static const wstring g_AutoNameConversations;
	static const wstring g_AutoNameSites;
	static const wstring g_AutoNameSitePermissions;
    static const wstring g_AutoNameLists;
    static const wstring g_AutoNameListsContent;
	static const wstring g_AutoNameLicenses;
	static const wstring g_AutoNameLicensesSummary;
	static const wstring g_AutoNameLicensesDeletedUsers;
    static const wstring g_AutoNamePosts;
    static const wstring g_AutoNameColumns;
	static const wstring g_AutoNameUsers;
	static const wstring g_AutoNameUsersRecycleBin;
	static const wstring g_AutoNameChannelMessages;
	static const wstring g_AutoNameSignIns;
	static const wstring g_AutoNameDirectoryAudits;
	static const wstring g_AutoNamePrivateChannelMembers;
	static const wstring g_AutoNameApplications;
	static const wstring g_AutoNameMailboxPermissions;
	static const wstring g_AutoNameOnPremiseUsers;
	static const wstring g_AutoNameOnPremiseGroups;
	// Not a real automation name, as real ones depend on report IDs, but a handy generic one.
	static const wstring g_AutoNameUsageReports;

	static wstring ModuleName(const wstring& p_GridAutomationName);
	static Command::ModuleTarget ModuleTarget(const wstring& p_GridAutomationName);

	template<typename T>
	static GridBackendField& AddFieldImpl(GridBackendRow* p_Row, const T& p_Value, GridBackendColumn* p_Col);

	template<>
	static GridBackendField& AddFieldImpl(GridBackendRow* p_Row, const bool& p_Value, GridBackendColumn* p_Col);

	template<>
	static  GridBackendField& AddFieldImpl(GridBackendRow* p_Row, const boost::YOpt<bool>& p_Value, GridBackendColumn* p_Col);

	template<>
	static  GridBackendField& AddFieldImpl(GridBackendRow* p_Row, const YTimeDate& p_Value, GridBackendColumn* p_Col);

	template<>
	static  GridBackendField& AddFieldImpl(GridBackendRow* p_Row, const boost::YOpt<YTimeDate>& p_Value, GridBackendColumn* p_Col);

private:

	static vector<IconTypeSetup> g_IconSetup;
	template<class RTTRRegisteredType> static void setObjectType(int32_t p_IconResID);
};

template<class RTTRRegisteredType>
bool GridUtil::IsSelectionAtLeastOneOfType(const CacheGrid& p_Grid)
{
	return std::any_of(p_Grid.GetSelectedRows().begin(), p_Grid.GetSelectedRows().end(), [](GridBackendRow* p_Row)
	{
		return isBusinessType<RTTRRegisteredType>(p_Row);
	});
}

template<class RTTRRegisteredType>
bool GridUtil::IsSelectionAtLeastOneNotOfType(const CacheGrid& p_Grid)
{
	return std::any_of(p_Grid.GetSelectedRows().begin(), p_Grid.GetSelectedRows().end(), [](GridBackendRow* p_Row)
	{
		return !isBusinessType<RTTRRegisteredType>(p_Row);
	});
}

template<class RTTRRegisteredType>
bool GridUtil::IsSelectionOrParentAtLeastOfType(const CacheGrid& p_Grid)
{
	return std::any_of(p_Grid.GetSelectedRows().begin(), p_Grid.GetSelectedRows().end(), [](GridBackendRow* p_Row)
	{
		return isBusinessType<RTTRRegisteredType>(p_Row) || hasParentOfBusinessType<RTTRRegisteredType>(p_Row);
	});
}

template<class RTTRRegisteredType>
bool GridUtil::isBusinessType(const GridBackendRow* p_Row)
{
	bool isMyTypeRV = false;

	if (nullptr != p_Row && !p_Row->IsGroupRow() && nullptr != p_Row->GetBackend() && nullptr != p_Row->GetBackend()->GetCacheGrid())
	{
		const auto& rowTypeField = p_Row->GetField(p_Row->GetBackend()->GetCacheGrid()->GetColumnObjectType());

		if (rowTypeField.HasAlternativeValue())
			isMyTypeRV = p_Row->GetBackend()->GetCacheGrid()->GetObjectTypeIndex(RTTRRegisteredType()) == rowTypeField.GetAlternativeValue();
	}

	return isMyTypeRV;
}

template<class RTTRRegisteredType>
bool GridUtil::hasParentOfBusinessType(const GridBackendRow* p_Row)
{
	if (nullptr != p_Row)
	{
		auto parent = p_Row->GetParentRow();
		while (nullptr != parent)
		{
			if (isBusinessType<RTTRRegisteredType>(parent))
				return true;

			parent = parent->GetParentRow();
		}
	}

	return false;
}

template<class RTTRRegisteredType>
GridBackendRow* GridUtil::getParentOfBusinessType(GridBackendRow* p_Row, bool p_AllowThis)
{
	GridBackendRow* parent = nullptr;

	if (nullptr != p_Row)
	{
		parent = p_AllowThis ? p_Row : p_Row->GetParentRow();
		while (nullptr != parent)
		{
			if (isBusinessType<RTTRRegisteredType>(parent))
				break;

			parent = parent->GetParentRow();
		}
	}

	return parent;
}

template<typename T>
GridBackendField& GridUtil::AddFieldImpl(GridBackendRow* p_Row, const T& p_Value, GridBackendColumn* p_Col)
{
	return p_Row->AddField(p_Value, p_Col);
}

template<>
GridBackendField& GridUtil::AddFieldImpl(GridBackendRow* p_Row, const bool& p_Value, GridBackendColumn* p_Col)
{
	return p_Row->AddFieldForCheckBox(p_Value, p_Col);
}

template<>
GridBackendField& GridUtil::AddFieldImpl(GridBackendRow* p_Row, const boost::YOpt<bool>& p_Value, GridBackendColumn* p_Col)
{
	return p_Row->AddFieldForCheckBox(p_Value, p_Col);
}

template<>
GridBackendField& GridUtil::AddFieldImpl(GridBackendRow* p_Row, const YTimeDate& p_Value, GridBackendColumn* p_Col)
{
	return p_Row->AddFieldTimeDate(p_Value, p_Col);
}

template<>
GridBackendField& GridUtil::AddFieldImpl(GridBackendRow* p_Row, const boost::YOpt<YTimeDate>& p_Value, GridBackendColumn* p_Col)
{
	return p_Row->AddFieldTimeDate(p_Value, p_Col);
}
