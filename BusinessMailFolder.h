#pragma once

#include "BusinessObject.h"
#include "BusinessMessage.h"
#include "MailFolder.h"
#include "Message.h"
#include <vector>

class BusinessMailFolder : public BusinessObject
{
public:
    BusinessMailFolder() = default;
    BusinessMailFolder(const MailFolder& p_MailFolder);

	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<int32_t> m_ChildFolderCount;
	boost::YOpt<PooledString> m_ParentFolderId;
	boost::YOpt<int32_t> m_TotalItemCount;
	boost::YOpt<int32_t> m_UnreadItemCount;

    vector<BusinessMailFolder> m_ChildrenMailFolders;
    vector<BusinessMessage> m_Messages;

    static MailFolder ToMailFolder(const BusinessMailFolder& p_BusinessMailFolder);

    RTTR_ENABLE(BusinessObject)
};
