#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessConversationThread.h"

class ConversationThreadDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessConversationThread>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

