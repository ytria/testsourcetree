#pragma once

#include "GridFrameBase.h"
#include "OnPremiseGroup.h"
#include "GridOnPremiseGroups.h"

class FrameOnPremiseGroups : public GridFrameBase
{
public:
	FrameOnPremiseGroups(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

	DECLARE_DYNAMIC(FrameOnPremiseGroups)
	virtual ~FrameOnPremiseGroups() = default;

	void ShowOnPremiseGroups(const vector<OnPremiseGroup>& p_OnPremiseGroups, bool p_FullPurge);

	virtual void ApplyAllSpecific();
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
	virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

	virtual O365Grid& GetGrid() override;

protected:
	virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

	//virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabGroup, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	GridOnPremiseGroups m_OnPremiseGroupsGrid;
};

