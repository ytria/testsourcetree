#include "ModuleSites.h"

#include "BasicPageRequestLogger.h"
#include "BasicRequestLogger.h"
#include "BusinessObjectManager.h"
#include "BusinessSite.h"
#include "ChannelSiteRequester.h"
#include "CommandDispatcher.h"
#include "FrameSites.h"
#include "GraphCache.h"
#include "MultiObjectsRequestLogger.h"
#include "RunOnScopeEnd.h"
#include "safeTaskCall.h"
#include "SiteRequester.h"
#include "SiteLoadMoreRequester.h"
#include "TaskDataManager.h"
#include "UpdatedObjectsGenerator.h"
#include "YDataCallbackMessage.h"
#include "YSafeCreateTask.h"

void ModuleSites::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
    {
    case Command::ModuleTask::List:
    {
        const auto& info = p_Command.GetCommandInfo();

        if (info.GetOrigin() == Origin::Group)
            showGroupRootSite(p_Command);
		else if (info.GetOrigin() == Origin::Channel)
			showChannelRootSite(p_Command);
        else if (info.GetOrigin() == Origin::Tenant)
            showRootSite(p_Command);
        else
            SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleSites_executeImpl_1, _YLOC(" - invalid origin"),_YR("Y2501")).c_str());
    }
	break;
	case Command::ModuleTask::UpdateMoreInfo:
		LoadMore(p_Command);
		break;
	case Command::ModuleTask::LoadSnapshot:
		loadSnapshot(p_Command);
		break;
    default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
        break;
    }
}

void ModuleSites::showRootSite(const Command& p_Command)
{
	const auto&	info		= p_Command.GetCommandInfo();
	auto p_ExistingFrame	= dynamic_cast<FrameSites*>(info.GetFrame());
	const auto& p_GroupIDs	= info.GetIds();
	auto p_SourceWindow		= p_Command.GetSourceWindow();
	auto p_Action			= p_Command.GetAutomationAction();

	auto sourceCWnd = p_SourceWindow.GetCWnd();
    auto frame = p_ExistingFrame;

    if (nullptr == p_ExistingFrame)
    {
        if (FrameSites::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin		= Origin::Tenant;
			moduleCriteria.m_Privilege	= info.GetRBACPrivilege();
			if (ShouldCreateFrame<FrameSites>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				frame = new FrameSites(Origin::Tenant, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleSites_showRootSite_1, _YLOC("SharePoint Sites")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
			else
			{
				// No error: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
				SetAutomationGreenLight(p_Action);
				return;
			}
		}
		else
		{
			SetAutomationGreenLight(p_Action, _YTEXT(""));
			return;
		}
    }

    HWND		hwnd		= frame->GetSafeHwnd();
	const bool	isUpdate	= nullptr != p_ExistingFrame;
    auto		taskData	= addFrameTask(YtriaTranslate::Do(ModuleSites_showRootSite_2, _YLOC("Show SharePoint Sites")).c_str(), frame, p_Command, !isUpdate);

	// Finish it if you need it
	bool refreshFromSql = false;

	std::vector<GridBackendRow*> rowsWithMoreLoaded;
	frame->GetGrid().GetRowsWithMoreLoaded(rowsWithMoreLoaded);
	
	std::vector<BusinessSite> sitesLoadMoreRequestInfo;
	if (!rowsWithMoreLoaded.empty())
	{
		auto gridSites = dynamic_cast<GridSites*>(&frame->GetGrid());
		ASSERT(nullptr != gridSites);
		if (nullptr != gridSites)
			sitesLoadMoreRequestInfo = gridSites->GetLoadMoreRequestInfo(rowsWithMoreLoaded);
	}

	frame->GetBusinessObjectManager()->GetAllBusinessSites(refreshFromSql, std::make_shared<BasicPageRequestLogger>(_T("sites")), taskData).ThenByTaskMutable([this, bom = frame->GetBusinessObjectManager(), hwnd, taskData, p_Action, isUpdate, rowsWithMoreLoaded, sitesLoadMoreRequestInfo, gridLogger = frame->GetGridLogger()](pplx::task<vector<BusinessSite>> p_PreviousTask) mutable
    {
		auto sites = p_PreviousTask.get();

		if (!sitesLoadMoreRequestInfo.empty())
		{
			auto logger = std::make_shared<BasicRequestLogger>(_YTEXT(""));

			{
				gridLogger->SetIgnoreHttp404Errors(true);
				RunOnScopeEnd _([&gridLogger]()
					{
						gridLogger->SetIgnoreHttp404Errors(false);
					});

				for (auto& site : sitesLoadMoreRequestInfo)
				{
					const auto& cachedSite = bom->GetGraphCache().GetSiteInCache(site.GetID());
					logger->SetContextualInfo(cachedSite.GetDisplayName());

					auto loadMoreRequester = std::make_shared<SiteLoadMoreRequester>(site.GetID(), logger);
					loadMoreRequester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();
					site = loadMoreRequester->GetData();
				}
			}
		}

		YDataCallbackMessage<vector<BusinessSite>>::DoPost(sites, [hwnd, taskData, p_Action, isUpdate, isCanceled = taskData.IsCanceled(), sitesLoadedMore = sitesLoadMoreRequestInfo](const vector<BusinessSite>& p_Sites)
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameSites*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, false))
			{
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s sites...", Str::getStringFromNumber(p_Sites.size()).c_str()));
				frame->GetGrid().ClearLog(taskData.GetId());

				{
					CWaitCursor _;
					frame->ShowSites(p_Sites, sitesLoadedMore, true);
				}

				if (!isUpdate)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				frame->ProcessLicenseContext(); // Let's consume license tokens if needed
				ModuleBase::TaskFinished(frame, taskData, p_Action);
			}
		});
    });
}

void ModuleSites::showGroupRootSite(const Command& p_Command)
{
	const auto& info		= p_Command.GetCommandInfo();
	auto p_ExistingFrame	= dynamic_cast<FrameSites*>(info.GetFrame());
	const auto& p_GroupIDs	= info.GetIds();
	auto p_SourceWindow		= p_Command.GetSourceWindow();
	auto p_Action			= p_Command.GetAutomationAction();

	auto sourceCWnd = p_SourceWindow.GetCWnd();

    auto frame = p_ExistingFrame;
    if (nullptr == p_ExistingFrame)
    {
        if (FrameSites::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin				= Origin::Group;
			moduleCriteria.m_UsedContainer		= ModuleCriteria::UsedContainer::IDS;
			moduleCriteria.m_IDs				= p_GroupIDs;
			moduleCriteria.m_Privilege			= info.GetRBACPrivilege();
			moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

			if (ShouldCreateFrame<FrameSites>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				frame = new FrameSites(Origin::Group, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleSites_showGroupRootSite_1, _YLOC("SharePoint Sites")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
			else
			{
				// No error: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
				SetAutomationGreenLight(p_Action);
				return;
			}
		}
		else
		{
			SetAutomationGreenLight(p_Action, _YTEXT(""));
			return;
		}
    }

	RefreshSpecificData refreshSpecificData;
	if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
		refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

	const bool isUpdate = nullptr != p_ExistingFrame;
    auto taskData	= addFrameTask(YtriaTranslate::Do(ModuleSites_showGroupRootSite_2, _YLOC("Show SharePoint Sites")).c_str(), frame, p_Command, !isUpdate);

	std::vector<GridBackendRow*> rowsWithMoreLoaded;
	frame->GetGrid().GetRowsWithMoreLoaded(rowsWithMoreLoaded);

	std::vector<BusinessSite> sitesLoadMoreRequestInfo;
	if (!rowsWithMoreLoaded.empty())
	{
		auto gridSites = dynamic_cast<GridSites*>(&frame->GetGrid());
		ASSERT(nullptr != gridSites);
		if (nullptr != gridSites)
			sitesLoadMoreRequestInfo = gridSites->GetLoadMoreRequestInfo(rowsWithMoreLoaded);
	}

	YSafeCreateTaskMutable([this, sitesLoadMoreRequestInfo, hwnd = frame->GetSafeHwnd(), taskData, p_Action, p_GroupIDs, isUpdate, bom = frame->GetBusinessObjectManager(), refreshSpecificData, wantsAdditionalMetadata = (bool)frame->GetMetaDataColumnInfo(), gridLogger = frame->GetGridLogger()]() mutable
    {
		O365DataMap<BusinessGroup, BusinessSite> allSites;

		vector<BusinessGroup> groups;
		if (wantsAdditionalMetadata)
		{
			// Do not pass taskData if we don't want to allow cancellation of this step.
			groups = bom->GetGraphCache().GetSqlCachedGroups(taskData.GetOriginator(), p_GroupIDs, true, taskData);
		}

		auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("root site"), p_GroupIDs.size());

		auto processGroup = [this, &sitesLoadMoreRequestInfo, &bom, &taskData, &allSites, &logger, &gridLogger](auto& group) mutable
		{
			logger->IncrementObjCount();
			logger->SetContextualInfo(GetDisplayNameOrId(group));

			BusinessSite site;
			if (!taskData.IsCanceled())
				site = bom->GetRootBusinessSite(group.GetID(), logger, taskData).GetTask().get();

			auto ittMoreSite = std::find_if(sitesLoadMoreRequestInfo.begin(), sitesLoadMoreRequestInfo.end(), [site](const BusinessSite& p_Site) { return p_Site.GetID() == site.GetID(); });
			if (ittMoreSite != sitesLoadMoreRequestInfo.end())
			{
				gridLogger->SetIgnoreHttp404Errors(true);
				RunOnScopeEnd _([&gridLogger]()
					{
						gridLogger->SetIgnoreHttp404Errors(false);
					});

				auto loadMoreRequester = std::make_shared<SiteLoadMoreRequester>(site.GetID(), logger);
				loadMoreRequester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();
				site = loadMoreRequester->GetData();

				*ittMoreSite = site;
				auto subsites = site.GetSubSites();
				for (auto& subsite : subsites)
				{
					auto ittMoreSubSite = std::find_if(sitesLoadMoreRequestInfo.begin(), sitesLoadMoreRequestInfo.end(), [subsite](const BusinessSite& p_Site) { return p_Site.GetID() == subsite.GetID(); });
					if (ittMoreSubSite != sitesLoadMoreRequestInfo.end())
					{
						auto loadMoreRequester = std::make_shared<SiteLoadMoreRequester>(site.GetID(), logger);
						loadMoreRequester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();
						subsite = loadMoreRequester->GetData();

						*ittMoreSubSite = subsite;
					}
				}
				site.SetSubSites(subsites);
			}

			if (taskData.IsCanceled())
			{
				group.SetFlags(BusinessObject::CANCELED);
				allSites[group] = BusinessSite();
			}
			else
			{
				allSites[group] = site;
			}
		};

		auto ids = p_GroupIDs;
		for (auto& group : groups)
		{
			if (1 == ids.erase(group.GetID()))
				processGroup(group);
		}

		ASSERT(!wantsAdditionalMetadata || ids.empty()); // Is it normal not all groups are in SQL cache?

        for (const auto& id : ids)
        {
			BusinessGroup group;
			group.SetID(id);
			logger->SetContextualInfo(bom->GetGraphCache().GetGroupContextualInfo(id));
			bom->GetGraphCache().SyncUncachedGroup(group, logger, taskData);

			processGroup(group);
        }

		YDataCallbackMessage<O365DataMap<BusinessGroup, BusinessSite>>::DoPost(allSites, [hwnd, sitesLoadMoreRequestInfo, taskData, p_Action, refreshSpecificData, isUpdate, isCanceled = taskData.IsCanceled()](const O365DataMap<BusinessGroup, BusinessSite>& p_Sites)
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameSites*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, false))
			{
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting sites for %s groups...", Str::getStringFromNumber(p_Sites.size()).c_str()));
				frame->GetGrid().ClearLog(taskData.GetId());

				{
					CWaitCursor _;
					frame->ShowSites(p_Sites, sitesLoadMoreRequestInfo, refreshSpecificData.m_Criteria.m_IDs.empty());
				}

				if (!isUpdate)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, taskData, p_Action);
			}
		});
    });
}

void ModuleSites::showChannelRootSite(const Command& p_Command)
{
	const auto& info = p_Command.GetCommandInfo();
	auto p_ExistingFrame = dynamic_cast<FrameSites*>(info.GetFrame());
	const auto& ids = info.GetSubIds();
	auto p_SourceWindow = p_Command.GetSourceWindow();
	auto p_Action = p_Command.GetAutomationAction();

	auto sourceCWnd = p_SourceWindow.GetCWnd();

	auto frame = p_ExistingFrame;
	if (nullptr == p_ExistingFrame)
	{
		if (FrameSites::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin = Origin::Channel;
			moduleCriteria.m_UsedContainer = ModuleCriteria::UsedContainer::SUBIDS;
			moduleCriteria.m_SubIDs = ids;
			moduleCriteria.m_Privilege = info.GetRBACPrivilege();
			moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

			if (ShouldCreateFrame<FrameSites>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				frame = new FrameSites(Origin::Channel, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleSites_showGroupRootSite_1, _YLOC("SharePoint Sites")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
			else
			{
				// No error: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
				SetAutomationGreenLight(p_Action);
				return;
			}
		}
		else
		{
			SetAutomationGreenLight(p_Action, _YTEXT(""));
			return;
		}
	}

	const bool isUpdate = nullptr != p_ExistingFrame;
	auto taskData = addFrameTask(YtriaTranslate::Do(ModuleSites_showGroupRootSite_2, _YLOC("Show SharePoint Sites")).c_str(), frame, p_Command, !isUpdate);

	using DataType = std::map<PooledString, PooledString>;
	DataType channelNames;

	RefreshSpecificData refreshSpecificData;
	if (info.Data().is_type<DataType>()) // Only happens when the window is first opened
		channelNames = info.Data().get_value<DataType>();
	else if (info.Data().is_type<RefreshSpecificData>()) // Only during a partial refresh
		refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

	if (channelNames.empty())
	{
		ASSERT(nullptr != frame);
		if (nullptr != frame)
			channelNames = frame->GetChannelNames();
	}

	std::vector<GridBackendRow*> rowsWithMoreLoaded;
	frame->GetGrid().GetRowsWithMoreLoaded(rowsWithMoreLoaded);

	std::vector<BusinessSite> sitesLoadMoreRequestInfo;
	if (!rowsWithMoreLoaded.empty())
	{
		auto gridSites = dynamic_cast<GridSites*>(&frame->GetGrid());
		ASSERT(nullptr != gridSites);
		if (nullptr != gridSites)
			sitesLoadMoreRequestInfo = gridSites->GetLoadMoreRequestInfo(rowsWithMoreLoaded);
	}

	YSafeCreateTaskMutable([this, sitesLoadMoreRequestInfo, hwnd = frame->GetSafeHwnd(), taskData, p_Action, ids, isUpdate, bom = frame->GetBusinessObjectManager(), refreshSpecificData, channelNames, gridLogger = frame->GetGridLogger()]() mutable
	{
		O365DataMap<BusinessChannel, BusinessSite> allSites;

		auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("root site"), ids.size());

		auto processChannel = [this, &sitesLoadMoreRequestInfo, &bom, &taskData, &allSites, &logger, &gridLogger](BusinessChannel& channel) mutable
		{
			logger->IncrementObjCount();
			logger->SetContextualInfo(GetDisplayNameOrId(channel));

			BusinessSite site;
			if (!taskData.IsCanceled())
			{
				{
					auto requester = std::make_shared<ChannelSiteRequester>(channel.GetGroupId() ? *channel.GetGroupId() : _YTEXT(""), channel.GetID(), logger);

					gridLogger->SetIgnoreHttp404Errors(true);
					RunOnScopeEnd _([&gridLogger]()
						{
							gridLogger->SetIgnoreHttp404Errors(false);
						});

					site = safeTaskCall(requester->Send(bom->GetSapio365Session(), taskData).Then([requester, taskData, bom, logger]
						{
							auto subLogger = std::make_shared<MultiObjectsPageRequestLogger>(*logger);
							subLogger->SetLogMessage(_T("subsites"));
							auto businessSubSites = bom->GetSubSites(requester->GetData().GetID(), subLogger, taskData).GetTask().get();
							auto site = requester->GetData();
							site.SetSubSites(businessSubSites);
							return site;
						}), bom->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ObjectErrorHandler<BusinessSite>, taskData).get();
				}
			}

			if (!taskData.IsCanceled())
			{
				auto ittMoreSite = std::find_if(sitesLoadMoreRequestInfo.begin(), sitesLoadMoreRequestInfo.end(), [site](const BusinessSite& p_Site) { return p_Site.GetID() == site.GetID(); });
				if (ittMoreSite != sitesLoadMoreRequestInfo.end())
				{
					gridLogger->SetIgnoreHttp404Errors(true);
					RunOnScopeEnd _([&gridLogger]()
						{
							gridLogger->SetIgnoreHttp404Errors(false);
						});

					auto loadMoreRequester = std::make_shared<SiteLoadMoreRequester>(site.GetID(), logger);
					loadMoreRequester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();
					site = loadMoreRequester->GetData();

					*ittMoreSite = site;
					auto subsites = site.GetSubSites();
					for (auto& subsite : subsites)
					{
						auto ittMoreSubSite = std::find_if(sitesLoadMoreRequestInfo.begin(), sitesLoadMoreRequestInfo.end(), [subsite](const BusinessSite& p_Site) { return p_Site.GetID() == subsite.GetID(); });
						if (ittMoreSubSite != sitesLoadMoreRequestInfo.end())
						{
							auto loadMoreRequester = std::make_shared<SiteLoadMoreRequester>(site.GetID(), logger);
							loadMoreRequester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();
							subsite = loadMoreRequester->GetData();

							*ittMoreSubSite = subsite;
						}
					}
					site.SetSubSites(subsites);
				}
			}

			if (taskData.IsCanceled())
			{
				channel.SetFlags(BusinessObject::CANCELED);
				allSites[channel] = BusinessSite();
			}
			else
			{
				allSites[channel] = site;
			}
		};

		BusinessChannel channel;
		for (const auto& byTeam : ids)
		{
			for (auto& channelId : byTeam.second)
			{
				channel.SetID(channelId);
				channel.SetGroupId(byTeam.first.GetId());
				auto it = channelNames.find(channelId);
				ASSERT(it != channelNames.end());
				if (it != channelNames.end())
					channel.SetDisplayName(it->second);
				else
					channel.SetDisplayName(boost::none);

				processChannel(channel);
			}
		}

		YDataCallbackMessage<O365DataMap<BusinessChannel, BusinessSite>>::DoPost(allSites, [hwnd, sitesLoadMoreRequestInfo, taskData, p_Action, refreshSpecificData, isUpdate, isCanceled = taskData.IsCanceled(), channelNames](const O365DataMap<BusinessChannel, BusinessSite>& p_Sites)
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameSites*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, false))
			{
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting sites for %s channels...", Str::getStringFromNumber(p_Sites.size()).c_str()));
				frame->GetGrid().ClearLog(taskData.GetId());

				{
					CWaitCursor _;
					frame->ShowSites(p_Sites, sitesLoadMoreRequestInfo, refreshSpecificData.m_Criteria.m_IDs.empty());
					frame->SetChannelNames(channelNames); // Store this for next refresh
				}

				if (!isUpdate)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, taskData, p_Action);
			}
		});
	});
}

void ModuleSites::LoadMore(const Command& p_Command)
{
	auto frame = dynamic_cast<FrameSites*>(p_Command.GetCommandInfo().GetFrame());

	using DataType = UpdatedObjectsGenerator<BusinessSite>;
	bool isDataValid = p_Command.GetCommandInfo().Data().is_type<DataType>();

	ASSERT(nullptr != frame && isDataValid);
	if (nullptr != frame && isDataValid)
	{
		auto taskData = addFrameTask(_T("More Info"), frame, p_Command, false);
		
		YSafeCreateTask([this, taskData, bom = frame->GetBusinessObjectManager(), p_Command, hwnd = frame->GetSafeHwnd(), gridLogger = frame->GetGridLogger()]() {
			auto logger = std::make_shared<BasicRequestLogger>(_T(""));
			auto sites = p_Command.GetCommandInfo().Data().get_value<DataType>().GetObjects();

			{
				gridLogger->SetIgnoreHttp404Errors(true);
				RunOnScopeEnd _([&gridLogger]()
					{
						gridLogger->SetIgnoreHttp404Errors(false);
					});

				for (auto& site : sites)
				{
					const auto& cachedSite = bom->GetGraphCache().GetSiteInCache(site.GetID());
					logger->SetContextualInfo(cachedSite.GetDisplayName());

					auto loadMoreRequester = std::make_shared<SiteLoadMoreRequester>(site.GetID(), logger);
					loadMoreRequester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();
					site = loadMoreRequester->GetData();
				}
			}

			YCallbackMessage::DoPost([canceled = taskData.IsCanceled(), hwnd, taskData, sites, command = p_Command]() {
				FrameSites* frame = ::IsWindow(hwnd) ? dynamic_cast<FrameSites*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, canceled, taskData, false))
				{
					ASSERT(nullptr != frame);

					frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
					frame->GetGrid().ClearLog(taskData.GetId());
					{
						CWaitCursor cursor;
						frame->UpdateSitesLoadMore(sites, false);
					}
				}
				ModuleBase::TaskFinished(frame, taskData, command.GetAutomationAction());
			});
			
		});
	}
}

void ModuleSites::loadSnapshot(const Command& p_Command)
{
	LoadSnapshot<FrameSites>(p_Command, [&p_Command](const GridSnapshot::Metadata& p_SnapshotMeta, const ModuleCriteria& p_ModuleCriteria, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
		{
			return new FrameSites(p_ModuleCriteria.m_Origin, p_LicenseContext, p_SessionIdentifier, YtriaTranslate::Do(ModuleSites_showRootSite_1, _YLOC("SharePoint Sites")).c_str(), p_Module, p_HistoryMode, p_PreviousFrame);
		});
}

//void ModuleSites::DoLoadMoreRequests(BusinessSite& p_Site, std::shared_ptr<IFriendlyRequestLogger> p_Logger, const std::shared_ptr<Sapio365Session>& p_Session, YtriaTaskData p_TaskData)
//{
//	BusinessSite siteBackup = p_Site;
//	wstring oldLogMessage = p_Logger->GetLogMessage();
//	// Site
//	auto syncRequester = std::make_shared<SiteRequester>(p_Site.GetID(), p_Logger);
//	p_Logger->SetLogMessage(_T("site"));
//	p_Site = safeTaskCall(syncRequester->Send(p_Session, p_TaskData).Then([syncRequester]() {
//		return syncRequester->GetData();
//	}), p_Session->GetMSGraphSession(Sapio365Session::RBAC_USER), Util::ObjectErrorHandler<BusinessSite>, p_TaskData.GetOriginator()).get();
//
//	if (p_Site.GetError())
//	{
//		boost::YOpt<SapioError> error = p_Site.GetError();
//		p_Site = siteBackup;
//		p_Site.SetError(error);
//	}
//
//	// Drive
//	p_Logger->SetLogMessage(_T("document library"));
//	auto requester = std::make_shared<DriveRequester>(DriveRequester::DriveSource::Site, p_Site.GetID(), p_Logger);
//	auto drive = safeTaskCall(requester->Send(p_Session, p_TaskData).Then([requester]() {
//		return requester->GetData();
//	}), p_Session->GetMSGraphSession(Sapio365Session::RBAC_USER), Util::ObjectErrorHandler<BusinessDrive>, p_TaskData.GetOriginator()).get();
//
//	// Site has no error, but drive has error?
//	if (!p_Site.GetError() && drive.GetError() && drive.GetError()->GetStatusCode() != web::http::status_codes::NotFound)
//		p_Site.SetError(drive.GetError());
//	p_Site.SetDrive(drive);
//	p_Site.SetFlags(BusinessObject::MORE_LOADED);
//
//	p_Logger->SetLogMessage(oldLogMessage);
//}
