#pragma once

#include "IRequester.h"
#include "Application.h"

class SingleRequestResult;
class ApplicationDeserializer;

class UpdateAppRequester : public IRequester
{
public:
	UpdateAppRequester(const Application& p_App);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const SingleRequestResult& GetResult() const;

private:
	std::shared_ptr<SingleRequestResult> m_Result;

	Application m_App;
};

