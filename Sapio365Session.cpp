#include "Sapio365Session.h"

#include "AzureADOAuth2BrowserAuthenticator.h"
#include "BasicPageRequestLogger.h"
#include "CommandDispatcher.h"
#include "ContractListRequester.h"
#include "DirectoryRolesRequester.h"
#include "DirectoryRoleMembersRequester.h"
#include "DlgBrowserOpener.h"
#include "FileUtil.h"
#include "GridFrameBase.h"
#include "IPowerShell.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "MSGraphCommonData.h"
#include "MSGraphSession.h"
#include "NetworkUtils.h"
#include "O365AdminErrorHandler.h"
#include "Product.h"
#include "Sapio365Settings.h"
#include "SharepointOnlineSession.h"
#include "AzureAdRefreshTokenAuthenticator.h"
#include "ExchangeOnlineSession.h"
#include "CosmosDBSqlSession.h"
#include "AzureSession.h"
#include "BasicRequestLogger.h"
#include "SessionsSqlEngine.h"
#include "SessionTypes.h"
#include "InvokeResultWrapper.h"
#include "ExchangeAuthenticationCollectionDeserializer.h"

bool Sapio365Session::g_DumpEnable			= false;
bool Sapio365Session::g_DumpRegistryRead	= false;

std::shared_ptr<Sapio365Session> Sapio365Session::Find(const SessionIdentifier& p_Identifier)
{
	if (p_Identifier.m_RoleID >= SessionIdentifier::g_NoRole)
	{
		{
			std::shared_ptr<Sapio365Session> mainFrameSession = MainFrameSapio365Session();
			if (mainFrameSession && p_Identifier == mainFrameSession->GetIdentifier())
				return mainFrameSession;
		}

		for (const auto& gridFrame : CommandDispatcher::GetInstance().GetAllGridFrames())
		{
			auto o365GraphSession = gridFrame->GetSapio365Session();
			if (o365GraphSession && p_Identifier == o365GraphSession->GetIdentifier())
				return o365GraphSession;
		}
	}

	return nullptr;
}

bool Sapio365Session::IsUsedInMoreThanOneFrame(const std::shared_ptr<Sapio365Session>& p_Session)
{
	ASSERT(p_Session);
	if (p_Session)
	{
		int count = 0;
		{
			std::shared_ptr<Sapio365Session> mainFrameSession = MainFrameSapio365Session();
			if (mainFrameSession == p_Session)
				++count;
		}

		for (const auto& gridFrame : CommandDispatcher::GetInstance().GetAllGridFrames())
		{
			if (p_Session == gridFrame->GetSapio365Session() && ++count > 1)
				return true;
		}
	}

	return false;
}

void Sapio365Session::NotifyFrames(const std::shared_ptr<Sapio365Session>& p_Session)
{
	ASSERT(p_Session);
	if (p_Session)
	{
		auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
		ASSERT(nullptr != mainFrame);
		if (nullptr != mainFrame && p_Session == mainFrame->GetSapio365Session())
			mainFrame->SetSapio365Session(p_Session);

		for (const auto& gridFrame : CommandDispatcher::GetInstance().GetAllGridFrames())
		{
			if (p_Session == gridFrame->GetSapio365Session())
				gridFrame->SetSapio365Session(p_Session);
		}
	}
}

void Sapio365Session::CancelAsyncUserCountTaskIfNeeded(const std::shared_ptr<Sapio365Session>& p_Session)
{
	if (p_Session && !IsUsedInMoreThanOneFrame(p_Session))
		p_Session->cancelAsyncUserCountTask();
}

bool Sapio365Session::GetForceDisableLoadOnPrem() const
{
	return m_ForceDisableLoadOnPrem;
}

void Sapio365Session::SetForceDisableLoadOnPrem(bool p_Value)
{
	m_ForceDisableLoadOnPrem = p_Value;
}

void Sapio365Session::SignOut(std::shared_ptr<Sapio365Session> p_Session, CWnd* p_ParentForDialog)
{
	ASSERT(p_Session);

	std::shared_ptr<Sapio365Session> nextMainSession;
	YTimeDate nextMainSessionDate;
	vector<std::shared_ptr<Sapio365Session>> sessionsToSignOut;
	GridFrameBase* oneSourceGridFrame = nullptr;

#define DISABLE_FRAMES_DURING_UPDATE 0 // Was made to prevent command update (that checks delegation) during session change, but doesn't seem to happen anymore...
#if DISABLE_FRAMES_DURING_UPDATE
	std::map<GridFrameBase*, BOOL> frameEnablement;
#endif

	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
	{
		if (AreSharingCredentials(mainFrame->GetSapio365Session(), p_Session))
		{
			sessionsToSignOut.push_back(mainFrame->GetSapio365Session());

			// We are signing-out main session, find the next main.
			for (const auto& gridFrame : CommandDispatcher::GetInstance().GetAllGridFrames())
			{
				if (AreSharingCredentials(gridFrame->GetSapio365Session(), p_Session))
				{
					if (nullptr == oneSourceGridFrame && gridFrame->GetSapio365Session() == p_Session)
						oneSourceGridFrame = gridFrame;

					if (sessionsToSignOut.end() == std::find(sessionsToSignOut.begin(), sessionsToSignOut.end(), gridFrame->GetSapio365Session()))
						sessionsToSignOut.push_back(gridFrame->GetSapio365Session());
				}
				else if (gridFrame->GetSapio365Session()
					&& gridFrame->GetSapio365Session()->IsConnected()
					&& gridFrame->GetSessionInfo().GetLastUsedOn() > nextMainSessionDate)
				{
					nextMainSession = gridFrame->GetSapio365Session();
					nextMainSessionDate = gridFrame->GetSessionInfo().GetLastUsedOn();
				}

#if DISABLE_FRAMES_DURING_UPDATE
				frameEnablement[gridFrame] = gridFrame->IsWindowEnabled();
				gridFrame->EnableWindow(FALSE);
#endif
			}

			if (nextMainSession && !Sapio365Session::CheckCompatibilityWithActiveRBAC(nextMainSession->GetTenantName(true), mainFrame, sessionsToSignOut))
			{
#if DISABLE_FRAMES_DURING_UPDATE
				for (const auto& item : frameEnablement)
					item.first->EnableWindow(item.second);
#endif
				return;
			}
		}
		else
		{
			for (const auto& gridFrame : CommandDispatcher::GetInstance().GetAllGridFrames())
			{
				if (AreSharingCredentials(gridFrame->GetSapio365Session(), p_Session))
				{
					if (nullptr == oneSourceGridFrame && gridFrame->GetSapio365Session() == p_Session)
						oneSourceGridFrame = gridFrame;

					if (sessionsToSignOut.end() == std::find(sessionsToSignOut.begin(), sessionsToSignOut.end(), gridFrame->GetSapio365Session()))
						sessionsToSignOut.push_back(gridFrame->GetSapio365Session());
				}

#if DISABLE_FRAMES_DURING_UPDATE
				for (const auto& item : frameEnablement)
					item.first->EnableWindow(item.second);
#endif
			}
		}
	}

	if (sessionsToSignOut.size() > 1)
	{
		YCodeJockMessageBox msgBox(p_ParentForDialog,
			DlgMessageBox::eIcon::eIcon_ExclamationWarning,
			YtriaTranslate::Do(Sapio365Session_SignOut_6, _YLOC("Several opened windows share the same credentials.")).c_str(),
			YtriaTranslate::Do(Sapio365Session_SignOut_7, _YLOC("As every Standard or Role session using %1 account will be signed-out, they will be disconnected"), sessionsToSignOut.front()->GetEmailOrAppId().c_str()),
			_YTEXT(""),
			{ { IDOK, YtriaTranslate::Do(consoleEZApp_InitInstanceSpecific_1, _YLOC("Continue")).c_str() },{ IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
		if (IDCANCEL == msgBox.DoModal())
		{
#if DISABLE_FRAMES_DURING_UPDATE
			for (const auto& item : frameEnablement)
				item.first->EnableWindow(item.second);
#endif
			return;
		}
	}
	else
	{
		const auto roleID = p_Session->GetRoleDelegationID();
		bool sharedCredentials = false;
		if (nullptr != oneSourceGridFrame)
			sharedCredentials = SessionsSqlEngine::Get().HasSharedCredentials(oneSourceGridFrame->GetSessionInfo().GetCredentialsId());
		else
			sharedCredentials = SessionsSqlEngine::Get().HasSharedCredentials(mainFrame->GetSessionInfo().GetCredentialsId());

		if (sharedCredentials)
		{
			YCodeJockMessageBox msgBox(p_ParentForDialog,
				DlgMessageBox::eIcon::eIcon_ExclamationWarning,
				YtriaTranslate::Do(BackstagePanelSessions_OnSignout_3, _YLOC("Several sessions share the same credentials.")).c_str(),
				YtriaTranslate::Do(BackstagePanelSessions_OnSignout_7, _YLOC("Every Standard or Role session using %1 account will be signed-out"), p_Session->GetEmailOrAppId().c_str()),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(consoleEZApp_InitInstanceSpecific_1, _YLOC("Continue")).c_str() },{ IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
			if (IDCANCEL == msgBox.DoModal())
			{
#if DISABLE_FRAMES_DURING_UPDATE
				for (const auto& item : frameEnablement)
					item.first->EnableWindow(item.second);
#endif
				return;
			}
		}
	}

	for (auto& session : sessionsToSignOut)
	{
		if (session->m_AsyncUsersCountTaskData)
		{
			if (!session->m_AsyncUsersCountTaskData->IsComplete() && !session->m_AsyncUsersCountTaskData->IsCanceled())
				session->m_AsyncUsersCountTaskData->Cancel();
			session->m_AsyncUsersCountTaskData.reset();
		}

		session->GetMainMSGraphSession()->ClearToken();
		// Do not call SetUseRoleDelegation(0) or setRoleDelegationID(0), we want to keep the current role ID and only reset delegation sessions.
		// So not remove delegation from cache, it's used by existing frames to update their commands.
		//RoleDelegationManager::GetInstance().RemoveFromCache(p_Session->GetRoleDelegationID());
		session->m_Gateways.ClearSessions();
	}

	for (auto& session : sessionsToSignOut)
	{
		SessionsSqlEngine::Get().Logout(session->GetIdentifier());
		if (session->GetIdentifier().m_SessionType == SessionTypes::g_ElevatedSession) // because Logout did a downgrade
			session->SetSessionType(SessionTypes::g_AdvancedSession);
	}

	for (const auto& gridFrame : CommandDispatcher::GetInstance().GetAllGridFrames())
	{
		if (std::find(sessionsToSignOut.begin(), sessionsToSignOut.end(), gridFrame->GetSapio365Session()) != sessionsToSignOut.end())
			gridFrame->SetSapio365Session(gridFrame->GetSapio365Session()); // Reset the same session to trigger UI update.
	}

	if (nullptr != mainFrame)
	{
		if (std::find(sessionsToSignOut.begin(), sessionsToSignOut.end(), mainFrame->GetSapio365Session()) != sessionsToSignOut.end())
			mainFrame->SetSapio365Session(nextMainSession);
		else
			mainFrame->SetSapio365Session(mainFrame->GetSapio365Session()); // Reset the same session to trigger UI update.
	}

#if DISABLE_FRAMES_DURING_UPDATE
	for (const auto& item : frameEnablement)
		item.first->EnableWindow(item.second);
#endif

	mainFrame->NotifyBackstagePanelSessions();
}

void Sapio365Session::SetSignedIn(std::shared_ptr<Sapio365Session> p_Session, CFrameWnd* p_Source)
{
	ASSERT(p_Session);
	if (p_Session)
	{
		p_Session->GetGraphCache().InitSqlCache();
		if (IsDumpGloballyEnabled())
			p_Session->EnableRequestDumping();
	}

	Util::InitOnPrem(p_Session);

	auto userGraphSession = p_Session->GetMSGraphSession(SessionType::USER_SESSION);
	auto appGraphSession = p_Session->GetMSGraphSession(SessionType::APP_SESSION);
	auto mainGraphSession = p_Session->GetMainMSGraphSession();
	
	userGraphSession->SetErrorHandler(std::make_unique<O365AdminErrorHandler>(userGraphSession, p_Session));
	appGraphSession->SetErrorHandler(std::make_unique<O365AdminErrorHandler>(appGraphSession, p_Session));
	mainGraphSession->SetErrorHandler(std::make_unique<O365AdminErrorHandler>(mainGraphSession, p_Session));

	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);

	std::shared_ptr<Sapio365Session> oldMainSession;
	if (nullptr != mainFrame)
	{
		oldMainSession = mainFrame->GetSapio365Session();
		if (p_Source == mainFrame
			|| oldMainSession == p_Session // Reset the same session to trigger UI update.
			|| !oldMainSession)
			mainFrame->SetSapio365Session(p_Session);
		else
			mainFrame->SetSapio365Session(oldMainSession); // Reset the same session to trigger UI update (list of all sessions).

		if (p_Source == mainFrame && oldMainSession != p_Session)
			mainFrame->UpdateSessionNSATracking();
	}

	bool asyncUsersCountTaskCancel =
			p_Source == mainFrame
		&&	oldMainSession
		&&	oldMainSession != p_Session
		&&	oldMainSession->m_AsyncUsersCountTaskData
		&&	!oldMainSession->m_AsyncUsersCountTaskData->IsComplete()
		&&	!oldMainSession->m_AsyncUsersCountTaskData->IsCanceled();

	for (const auto& gridFrame : CommandDispatcher::GetInstance().GetAllGridFrames())
	{
		auto frameSession = gridFrame->GetSapio365Session();

		asyncUsersCountTaskCancel = asyncUsersCountTaskCancel && frameSession != oldMainSession;

		if ((p_Source == gridFrame || frameSession == p_Session)) // Reset the same session to trigger UI update.
			gridFrame->SetSapio365Session(p_Session);
	}

	if (asyncUsersCountTaskCancel
		&& !oldMainSession->m_AsyncUsersCountTaskData->IsComplete()
		&& !oldMainSession->m_AsyncUsersCountTaskData->IsCanceled())
		oldMainSession->m_AsyncUsersCountTaskData->Cancel();

	if (nullptr != p_Session && 0 < p_Session->GetRoleDelegationID())
		RoleDelegationManager::GetInstance().RemoveFromCache(p_Session->GetRoleDelegationID());

	mainFrame->NotifyBackstagePanelSessions();
}

bool Sapio365Session::CheckCompatibilityWithActiveRBAC(const wstring& p_TenantName, MainFrame* p_MainFrame, vector<shared_ptr<Sapio365Session>> p_SessionsToIgnore)
{
	ASSERT(nullptr != p_MainFrame);

	bool ok = true;
	// Bug #190605.SR.00A002: Remove restriction that prevented from switching to another tenant when a frame was open on a tenant using RBAC and cosmos.
	/*if (nullptr != p_MainFrame)
	{
		for (const auto& gridFrame : CommandDispatcher::GetInstance().GetAllGridFrames())
		{
			auto frameSession = gridFrame->GetSapio365Session();
			if (frameSession && p_SessionsToIgnore.end() == std::find(p_SessionsToIgnore.begin(), p_SessionsToIgnore.end(), frameSession))
			{
				auto org = frameSession->GetGraphCache().GetOrganization(YtriaTaskData()).get();
				if (frameSession->IsUseRoleDelegation()
					&& frameSession->IsReadyForCosmos()
					&& p_TenantName != org.GetInitialDomain())
				{
					ok = false;
					break;
				}
			}
		}
	}

	if (!ok && nullptr != p_MainFrame)
	{
		YCodeJockMessageBox msgBox(p_MainFrame,
			DlgMessageBox::eIcon::eIcon_Information,
			YtriaTranslate::Do(GridFrameBase_OnRelog_6, _YLOC("RBAC Incompatibility")).c_str(),
			YtriaTranslate::Do(Sapio365Session_CheckCompatibilityWithActiveRBAC_3, _YLOC("Some modules using a Cosmos-RBAC session are currently opened. You must close them to be able to switch to tenant %1."), p_TenantName.c_str()),
			_YTEXT("The new session won't be loaded."),
			{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
		msgBox.DoModal();
	}*/

	return ok;
}

Sapio365Session::Sapio365Session()
	: m_Identifier(_YTEXT(""), _YTEXT(""), 0)
	, m_ResetCosmosSession(false)
	, m_RBACHideUnscopedObjects(true)
	, m_MsGraphSession(std::make_shared<MSGraphSession>(NetworkUtils::GetHttpClientConfig(), nullptr))
	, m_CommandLineAutomationInProgress(false)
	, m_CosmosSingleContainer(false)
	, m_ForceDisableLoadOnPrem(false)
{
    auto authenticator = std::make_shared<AzureADOAuth2BrowserAuthenticator>(m_MsGraphSession,
        Rest::GetMsGraphEndpoint(),
        _YTEXT(""),
        _YTEXT(""),
        SessionTypes::GetBasicUserRedirectUri(),
        std::make_shared<DlgBrowserOpener>());

	m_MsGraphSession->SetErrorHandler(std::make_unique<O365AdminErrorHandler>(m_MsGraphSession));
	m_MsGraphSession->SetAuthenticator(authenticator);
}

Sapio365Session::~Sapio365Session()
{
}

bool Sapio365Session::SetUseRoleDelegation(const int64_t p_ID, CWnd* p_ParentWindowForError)
{
	bool result = false;
	if (RoleDelegationManager::GetInstance().IsReadyToRun())
	{
		if (p_ID == GetRoleDelegationID() && (0 == p_ID || AreRoleDelegationSessionsValid()))
		{
			result = true;
		}
		else
		{
			const bool reenablingRole = p_ID == GetRoleDelegationID();
			if (!reenablingRole)
			{
				// Role changing - cache is obsolete.
				// But do not clear Org and connected user!! Only object lists
				//GetGraphCache().Clear();
				GetGraphCache().ClearObjectLists();
			}

			// Reset role
			{
				RoleDelegationManager::GetInstance().RemoveFromCache(GetRoleDelegationID()); // Remove previous role delegation from internal map
				setRoleDelegationID(0);
				m_Gateways.ClearSessions();
				GetGraphCache().SetRBACOrganization(boost::none);
			}

			// Set new role
			{
				if (p_ID > 0)
				{
					RoleDelegationManager::GetInstance().RemoveFromCache(p_ID); // Force reload on the next line
					const auto& rd = RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(p_ID, shared_from_this());
					ASSERT(rd.IsLoaded());
					if (rd.IsLoaded())
					{
						auto key = rd.GetKey();

                        if (key.m_TargetTenant.empty())
                            key.m_TargetTenant = GetTenantName(true);

						setRoleDelegationCredentials(key.m_TargetTenant, key.m_Login, key.m_PWD, key.m_ApplicationID, key.m_ApplicationPWD);
						if (createRoleDelegationSessions(key.m_Name))
						{
							setRoleDelegationID(p_ID);
							ASSERT(IsUseRoleDelegation());
							result = true;

							TraceIntoFile::trace(_YDUMP("Sapio365Session"), _YDUMP("SetUseRoleDelegation"), _YDUMPFORMAT(L"Successfully loaded %s session: %s from existing credentials", SessionTypes::g_Role.c_str(), key.m_ApplicationID.c_str()));
						}
						else
						{
							if (reenablingRole) // Is that case, even if role activation failed, we want to keep the role ID not to change the Session Identifier
								setRoleDelegationID(p_ID);

							RoleDelegationManager::GetInstance().RemoveFromCache(p_ID); // Clear cache if error
							result = false;
						}
					}
				}
			}
		}	
	}

	if (!result && nullptr != p_ParentWindowForError)
	{
		wstring userError;
		wstring appError;

		const auto& userSessionCreator = m_Gateways.GetLastUserSessionCreator();
		ASSERT(userSessionCreator);
		if (userSessionCreator)
		{
			const auto& result = userSessionCreator->GetResult();
			if (!result.m_Success)
				userError = result.m_GraphError;
		}

		const auto& appSessionCreator = m_Gateways.GetLastAppSessionCreator();
		ASSERT(appSessionCreator);
		if (appSessionCreator)
		{
			const auto& result = appSessionCreator->GetResult();
			if (!result.m_Success)
				appError = result.m_GraphError;
		}

		wstring userSessionResult;
		if (!userError.empty())
		{
			userSessionResult = _YFORMAT(L"An error occured while creating user session \"%s\" for tenant %s:\n", userSessionCreator->GetUsername().c_str(), userSessionCreator->GetTenant().c_str());
			userSessionResult += wstring(_T("Details:\n")) + ParseGraphJsonError(userSessionCreator->GetResult().m_GraphError).m_ErrorDescription;
		}

		wstring appSessionResult;
		if (!appError.empty())
		{
			appSessionResult = _YFORMAT(L"An error occured while creating application session \"%s\" for tenant %s:\n", appSessionCreator->GetApplicationName().c_str(), appSessionCreator->GetTenant().c_str());
			appSessionResult += wstring(_T("Details:\n")) + ParseGraphJsonError(appSessionCreator->GetResult().m_GraphError).m_ErrorDescription;
		}

		if (!userSessionResult.empty() && !appSessionResult.empty())
			userSessionResult += _YTEXT("\n\n");
		
		YCodeJockMessageBox dlg(p_ParentWindowForError,
			DlgMessageBox::eIcon_Error,
			YtriaTranslate::Do(Sapio365Session_SetUseRoleDelegation_1, _YLOC("Unable to activate the background sessions!")).c_str(),
			YtriaTranslate::Do(Sapio365Session_SetUseRoleDelegation_2, _YLOC("Please verify the credentials entered in the selected Role-Base Access Control configuration")).c_str(),
			userSessionResult + appSessionResult,
			{ { IDOK, YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str() } });
		dlg.DoModal();
	}

	return result;
}

bool Sapio365Session::IsUseRoleDelegation() const
{
	return GetRoleDelegationID() != 0;
}

void Sapio365Session::SetIdentifier(const SessionIdentifier& p_Identifier)
{
    m_Identifier = p_Identifier;
}

void Sapio365Session::setRoleDelegationCredentials(const wstring& p_Tenant, const wstring& p_Username, const wstring& p_Password, const wstring& p_ClientId, const wstring& p_ClientSecret)
{
    m_Gateways.SetO365Credentials(p_Username, p_Password);
    m_Gateways.SetAppGraphSessionCredentials(p_ClientId, p_ClientSecret);
    m_Gateways.SetTenant(p_Tenant);
}

bool Sapio365Session::createRoleDelegationSessions(const wstring& p_AppRefName)
{
	m_Gateways.CreateSessions(p_AppRefName);
	if (AreRoleDelegationSessionsValid())
	{
		if (m_RequestDumper) // If dumping enabled, ensure it is for role sessions too.
			EnableRequestDumping();
		return true;
	}
	return false;
}

bool Sapio365Session::AreRoleDelegationSessionsValid() const
{
	return m_Gateways.AreGraphSessionsValid();
}

void Sapio365Session::setRoleDelegationID(const int64_t p_ID)
{
	m_Identifier.m_RoleID = p_ID;
}

const RoleDelegation& Sapio365Session::GetRoleDelegationFromCache() const
{
	return RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(GetRoleDelegationID(), shared_from_this());
}

bool Sapio365Session::HasContracts(bool p_ForceRequest /*= true*/)
{
	return !GetContracts(p_ForceRequest).empty();
}

const vector<Contract>& Sapio365Session::GetContracts(bool p_ForceRequest /*= true*/)
{
	if (!m_Contracts || p_ForceRequest)
	{
		// FIXME: Should we check (IsAdvanced() || IsElevated()	|| IsPartnerAdvanced() || IsPartnerElevated()) ?
		CWaitCursor c;

		ContractListRequester req(std::make_shared<BasicPageRequestLogger>(_T("contracts")));
		YtriaTaskData taskData;
		safeTaskCall(req.Send(shared_from_this(), taskData), GetMSGraphSession(Sapio365Session::USER_SESSION), taskData).GetTask().wait();
		m_Contracts = req.GetData();
	}

	return *m_Contracts;
}

int64_t Sapio365Session::GetRoleDelegationID() const
{
	return m_Identifier.m_RoleID;
}

const SessionIdentifier& Sapio365Session::GetIdentifier() const
{
	return m_Identifier;
}

void Sapio365Session::SetGraphCache(std::unique_ptr<GraphCache>&& p_GraphCache)
{
	ASSERT(!m_GraphCache); // Don't set it twice!
    m_GraphCache = std::move(p_GraphCache);
}

void Sapio365Session::SetAppId(const wstring& p_AppId)
{
    m_AppId = p_AppId;
}

const wstring& Sapio365Session::GetAppId() const
{
    return m_AppId;
}

void Sapio365Session::SetEmailOrAppId(const wstring& p_EmailOrAppId)
{
	ASSERT(m_Identifier.m_EmailOrAppId.empty() || MFCUtil::StringMatchW(m_Identifier.m_EmailOrAppId, p_EmailOrAppId));
	m_Identifier.m_EmailOrAppId = p_EmailOrAppId;
}

const wstring& Sapio365Session::GetEmailOrAppId() const
{
	return m_Identifier.m_EmailOrAppId;
}

void Sapio365Session::SetMSGraphSession(const shared_ptr<MSGraphSession>& p_Session)
{
	m_MsGraphSession = p_Session;
}

std::shared_ptr<MSGraphSession> Sapio365Session::GetMainMSGraphSession() const
{
    return m_MsGraphSession;
}

std::shared_ptr<MSGraphSession> Sapio365Session::GetMSGraphSession(SessionType p_SessionType) const
{
	if (!IsUseRoleDelegation() && GetSessionType() != SessionTypes::g_ElevatedSession)
        return m_MsGraphSession;

	// If this fails, session has a role ID but role sessions have not been (re-)created yet.
	ASSERT(m_Gateways.GetUserGraphSession() && m_Gateways.GetAppGraphSession());
	if ((p_SessionType == USER_SESSION && m_Gateways.GetUserGraphSession() == nullptr) || (p_SessionType == APP_SESSION && m_Gateways.GetAppGraphSession() == nullptr))
	{
		wstring sessionStr = p_SessionType == SessionType::APP_SESSION ? _YTEXT("app") : _YTEXT("user");
		wstring strMessage = _YFORMAT(L"Sapio365Session::GetMSGraphSession: The requested RestSession \"%s\" session doesn't exist", sessionStr.c_str());
		throw std::exception(MFCUtil::convertUNICODE_to_UTF8(strMessage).c_str());
	}

    return APP_SESSION == p_SessionType ? m_Gateways.GetAppGraphSession() : m_Gateways.GetUserGraphSession();
}

std::shared_ptr<ExchangeOnlineSession> Sapio365Session::GetExchangeSession() const
{
    if (nullptr == m_ExchangeSession)
    {
        m_ExchangeSession = std::make_shared<ExchangeOnlineSession>(NetworkUtils::GetHttpClientConfig(), nullptr);
        auto authenticator = std::make_shared<AzureAdRefreshTokenAuthenticator>(GetMSGraphSession(Sapio365Session::USER_SESSION),
            m_ExchangeSession,
            GetTenantName(),
            ExchangeOnlineSession::GetResource(),
            GetMSGraphSession(Sapio365Session::USER_SESSION)->GetClientConfig().oauth2()->token().refresh_token(),
            GetAppId());
        m_ExchangeSession->SetAuthenticator(authenticator);
        m_ExchangeSession->SetErrorHandler(std::make_unique<O365AdminErrorHandler>(m_ExchangeSession));
    }

    return m_ExchangeSession;
}

std::shared_ptr<CosmosDBSqlSession> Sapio365Session::GetCosmosDBSqlSession() const
{
    if (nullptr == m_CosmosSqlSession || m_ResetCosmosSession)
    {
        m_CosmosSqlSession = std::make_shared<CosmosDBSqlSession>(GetCosmosDbSqlUri(), NetworkUtils::GetHttpClientConfig(), nullptr);
        m_CosmosSqlSession->SetErrorHandler(std::make_unique<O365AdminErrorHandler>(m_CosmosSqlSession));
		m_ResetCosmosSession = false;
    }

    return m_CosmosSqlSession;
}

std::shared_ptr<AzureSession> Sapio365Session::GetAzureSession() const
{
	if (nullptr == m_AzureSession)
	{
		// Application: Ytria sapio365 - Azure Management Service (1.0)
		m_AzureSession = std::make_unique<AzureSession>(NetworkUtils::GetHttpClientConfig(), nullptr);
		auto auth = std::make_shared<AzureADOAuth2BrowserAuthenticator>(m_AzureSession,
			_YTEXT("https://management.core.windows.net/"),
			_YTEXT(""),
			_YTEXT("86ad736e-d5eb-4254-ad4f-e5cb464e43fd"),
			_YTEXT("http://localhost:33366"),
			std::make_shared<DlgBrowserOpener>());
		m_AzureSession->SetAuthenticator(auth);
	}

	return m_AzureSession;
}

std::shared_ptr<BasicPowerShellSession> Sapio365Session::GetBasicPowershellSession() const
{
	if (nullptr == m_BasicPowerShellSession)
		InitBasicPowerShell(0);

	return m_BasicPowerShellSession;
}

void Sapio365Session::RemoveBasicPowershellSession()
{
	m_BasicPowerShellSession.reset();
}

std::shared_ptr<ExchangePowerShellSession> Sapio365Session::GetExchangePowershellSession() const
{
	if (nullptr == m_ExchangePowerShellSession)
		InitExchangePowerShell(0);
	
	return m_ExchangePowerShellSession;
}

bool Sapio365Session::HasBasicPowerShellSession() const
{
	return nullptr != m_BasicPowerShellSession;
}

bool Sapio365Session::HasExchangePowerShellSession() const
{
	return nullptr != m_ExchangePowerShellSession;
}

bool Sapio365Session::HasPowerShellDll() const
{
	// Not efficient, probably won't be a problem anyway
	return std::make_shared<ExchangePowerShellSession>(_YTEXT(""))->GetPowerShell() != nullptr;
}

std::shared_ptr<InvokeResultWrapper> Sapio365Session::InitExchangePowerShell(HWND p_Originator) const
{
	std::shared_ptr<InvokeResultWrapper> result;

	if (nullptr == m_ExchangePowerShellSession)
	{
		wstring defaultDomain = GetGraphCache().GetOrganization(YtriaTaskData()).get().GetDefaultDomain();
		auto powerShellSession = std::make_shared<ExchangePowerShellSession>(defaultDomain);
		if (powerShellSession->GetPowerShell() == nullptr)
			return result;

		// TODO: this code is gonna have to move soon (class ExchangePowerShell)
		LoggerService::User(_T("Checking and installing missing dependencies..."), p_Originator);
		powerShellSession->AddScriptStopOnError(_YTEXT(R"(
			if ((Get-Module -ListAvailable -Name MSAL.PS).count -eq 0) {
				[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12;
				Install-PackageProvider nuget -Force -Scope CurrentUser
				Install-Module -Name PowerShellGet -Force -AllowClobber -Scope CurrentUser
				PowerShell "[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12;Install-Module MSAL.PS -Force -AcceptLicense -RequiredVersion 4.10.0.2 -Scope CurrentUser;"
			}
		)"), p_Originator);

		result = std::make_shared<InvokeResultWrapper>(powerShellSession->Invoke(p_Originator));
		bool installedModules = result->Get().m_Success;
		if (installedModules)
		{
			powerShellSession = std::make_shared<ExchangePowerShellSession>(defaultDomain);
			result = powerShellSession->CreateExchangeToken(p_Originator);
			bool createdToken = result->Get().m_Success;
			if (installedModules && createdToken)
			{
				m_ExchangePowerShellSession = powerShellSession;
				m_ExchangePowerShellSession->SetHasLiveSession(true);
			}
		}
	}

	return result;
}

std::shared_ptr<InvokeResultWrapper> Sapio365Session::InitBasicPowerShell(HWND p_Originator) const
{
	std::shared_ptr<InvokeResultWrapper> result;

	if (nullptr == m_BasicPowerShellSession)
	{
		m_BasicPowerShellSession = std::make_shared<BasicPowerShellSession>(false);

		if (m_BasicPowerShellSession->GetPowerShell() == nullptr)
			return nullptr;

		wstring server = GetGraphCache().GetADDSServer();
		wstring username = GetGraphCache().GetADDSUsername();
		wstring password = GetGraphCache().GetADDSPassword();

		if (!server.empty() && !username.empty() && !password.empty())
		{
			LoggerService::User(_YFORMAT(L"Connecting to %s with username %s...", server.c_str(), username.c_str()), p_Originator);
			m_BasicPowerShellSession->AddScript(_YTEXTFORMAT(LR"(
					$Env:ADPS_LoadDefaultDrive = 0
					Import-Module ActiveDirectory
					$secStringPassword = ConvertTo-SecureString "%s" -AsPlainText -Force
					$credObject = New-Object System.Management.Automation.PSCredential ("%s", $secStringPassword)
					New-PSDrive -Name ADDS -PSProvider ActiveDirectory -Server "%s" -Scope Global -Root '' -Credential $credObject | out-null
					Set-Location ADDS:
				)", password.c_str(), username.c_str(), server.c_str()).c_str(), 0);
		}
		else
		{
			LoggerService::User(_T("Could not find server, username and password for OnPremise PowerShell, defaulting to current domain..."), p_Originator);
		}
		result = std::make_shared<InvokeResultWrapper>(m_BasicPowerShellSession->Invoke(p_Originator));
	}

	return result;
}

void Sapio365Session::RemoveAzureSession()
{
	m_AzureSession = nullptr;
}

void Sapio365Session::SwapSessionsWith(std::shared_ptr<Sapio365Session>& p_Other)
{
	m_MsGraphSession.swap(p_Other->m_MsGraphSession);
	m_SharepointSession.swap(p_Other->m_SharepointSession);
	m_ExchangeSession.swap(p_Other->m_ExchangeSession);
	m_CosmosSqlSession.swap(p_Other->m_CosmosSqlSession);
	m_AzureSession.swap(p_Other->m_AzureSession);
	std::swap(m_Gateways, p_Other->m_Gateways);
}

std::shared_ptr<SharepointOnlineSession> Sapio365Session::GetSharepointSession() const
{
    if (nullptr == m_SharepointSession)
    {
        // At this point, the cache should already be fully synced.
        const wstring tenantName = GetTenantName();
        const wstring resource = wstring(_YTEXT("https://")) + tenantName + _YTEXT(".sharepoint.com");

        m_SharepointSession = std::make_shared<SharepointOnlineSession>(resource, NetworkUtils::GetHttpClientConfig(), nullptr);
        auto authenticator = std::make_shared<AzureAdRefreshTokenAuthenticator>(GetMSGraphSession(Sapio365Session::USER_SESSION),
            m_SharepointSession,
            tenantName,
            resource,
            GetMSGraphSession(Sapio365Session::USER_SESSION)->GetClientConfig().oauth2()->token().refresh_token(),
			GetAppId());
        m_SharepointSession->SetAuthenticator(authenticator);
        m_SharepointSession->SetErrorHandler(std::make_unique<O365AdminErrorHandler>(m_SharepointSession));
    }

    return m_SharepointSession;
}

wstring Sapio365Session::GetTenantName() const
{
	return GetTenantName(false);
}

wstring Sapio365Session::GetTenantName(const bool p_WithFullDomain) const
{
	wstring tenantName;
    Organization org = GetGraphCache().GetOrganization(YtriaTaskData()).get();

	tenantName = org.GetInitialDomain();
	if (!p_WithFullDomain)
	{
		const auto& tokens = Str::explodeIntoVector(tenantName, wstring(_YTEXT(".")));
		ASSERT(!tokens.empty());
		if (!tokens.empty())
			tenantName = tokens[0];
	}

    return tenantName;
}

wstring Sapio365Session::GetRBACTenantName(const bool p_WithFullDomain) const
{
	wstring tenantName;
	const auto& org = GetGraphCache().GetRBACOrganization();

	ASSERT(org);
	if (org)
	{
		tenantName = org->GetInitialDomain();
		if (!p_WithFullDomain)
		{
			const auto& tokens = Str::explodeIntoVector(tenantName, wstring(_YTEXT(".")));
			ASSERT(!tokens.empty());
			if (!tokens.empty())
				tenantName = tokens[0];
		}
	}

	return tenantName;
}

wstring Sapio365Session::GetTenantNameDisplay() const
{
	Organization org = GetGraphCache().GetOrganization(YtriaTaskData()).get();
	return org.DisplayName.get().c_str();
}

GraphCache& Sapio365Session::GetGraphCache()
{
	ASSERT(m_GraphCache);
	if (m_GraphCache)
		return *m_GraphCache;

	static GraphCache empty(nullptr);
	return empty;
}

const GraphCache& Sapio365Session::GetGraphCache() const
{
	ASSERT(m_GraphCache);
	if (m_GraphCache)
		return *m_GraphCache;

	static GraphCache empty(nullptr);
	return empty;
}

bool Sapio365Session::IsConnected() const
{
    return m_MsGraphSession->HasToken();
}

void Sapio365Session::EnableRequestDumping()
{
	if (!m_RequestDumper)
		m_RequestDumper = std::make_shared<RequestDumper>(GetIdentifier());

	GetMainMSGraphSession()->SetRequestDumper(m_RequestDumper);
	if (AreRoleDelegationSessionsValid())
	{
		GetMSGraphSession(Sapio365Session::APP_SESSION)->SetRequestDumper(m_RequestDumper);
		GetMSGraphSession(Sapio365Session::USER_SESSION)->SetRequestDumper(m_RequestDumper);
	}
}

void Sapio365Session::DisableRequestDumping()
{
	GetMainMSGraphSession()->SetRequestDumper(nullptr);
	if (AreRoleDelegationSessionsValid())
	{
		GetMSGraphSession(Sapio365Session::APP_SESSION)->SetRequestDumper(nullptr);
		GetMSGraphSession(Sapio365Session::USER_SESSION)->SetRequestDumper(nullptr);
	}
	m_RequestDumper.reset();
}

bool Sapio365Session::IsDumpGloballyEnabled()
{
	if (!g_DumpRegistryRead)
	{
		wstring trash;
		g_DumpEnable = Registry::readString(HKEY_CURRENT_USER, _YTEXT("Software\\Ytria"), _YTEXT("YtriaEnableDump"), trash) && !trash.empty();
		g_DumpRegistryRead = true;
	}
	return g_DumpEnable;
}

void Sapio365Session::SetDumpGloballyEnabled()
{
	IsDumpGloballyEnabled(); // Call this to be sure we've read the registry

	g_DumpEnable = true;
	Registry::writeString(HKEY_CURRENT_USER, _YTEXT("Software\\Ytria"), _YTEXT("YtriaEnableDump"), _YTEXT("YES"));

	{
		{
			auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
			ASSERT(nullptr != mainFrame);

			if (nullptr != mainFrame)
			{
				auto o365GraphSession = mainFrame->GetSapio365Session();
				if (o365GraphSession)
				{
					o365GraphSession->EnableRequestDumping();
					mainFrame->SetSapio365Session(o365GraphSession); // Reset the same session to trigger UI update.
				}
			}
		}

		for (const auto& gridFrame : CommandDispatcher::GetInstance().GetAllGridFrames())
		{
			auto o365GraphSession = gridFrame->GetSapio365Session();
			if (o365GraphSession)
			{
				o365GraphSession->EnableRequestDumping();
				gridFrame->SetSapio365Session(o365GraphSession); // Reset the same session to trigger UI update.
			}
		}
	}
}

vector<wstring> Sapio365Session::SetDumpGloballyDisabled(bool p_SaveToRegistry)
{
	IsDumpGloballyEnabled(); // Call this to be sure we've read the registry

	g_DumpEnable = false;
	if (p_SaveToRegistry)
		Registry::erase(HKEY_CURRENT_USER, _YTEXT("Software\\Ytria"), _YTEXT("YtriaEnableDump"));

	{
		{
			auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
			ASSERT(nullptr != mainFrame);

			if (nullptr != mainFrame)
			{
				auto o365GraphSession = mainFrame->GetSapio365Session();
				if (o365GraphSession)
				{
					o365GraphSession->DisableRequestDumping();
					mainFrame->SetSapio365Session(o365GraphSession); // Reset the same session to trigger UI update.
				}
			}
		}

		for (const auto& gridFrame : CommandDispatcher::GetInstance().GetAllGridFrames())
		{
			auto o365GraphSession = gridFrame->GetSapio365Session();
			if (o365GraphSession)
			{
				o365GraphSession->DisableRequestDumping();
				gridFrame->SetSapio365Session(o365GraphSession); // Reset the same session to trigger UI update.
			}
		}
	}

	return RequestDumper::ClearPaths();
}

wstring Sapio365Session::GetDumpPath()
{
	return RequestDumper::GetDumpPath();
}

std::shared_ptr<IRestAuthenticator> Sapio365Session::GetRoleDelegationAuthenticator() const
{
    return m_Gateways.GetUserGraphSession()->GetAuthenticator();
}

const wstring& Sapio365Session::GetCosmosDbSqlUri() const
{
    return m_CosmosDbSqlUri;
}

void Sapio365Session::SetCosmosDbSqlUri(const wstring& p_Val)
{
	m_CosmosDbSqlUri.clear();
	if (CosmosManager().IsValid(p_Val))
		m_CosmosDbSqlUri = p_Val;
	m_ResetCosmosSession = true;
}

const wstring& Sapio365Session::GetCosmosDbMasterKey() const
{
    return m_CosmosDbMasterKey;
}

void Sapio365Session::SetCosmosSingleContainer(const bool p_CSC)
{
	m_CosmosSingleContainer = p_CSC;
}

bool Sapio365Session::IsCosmosSingleContainer() const
{
	return m_CosmosSingleContainer;
}

wstring	Sapio365Session::GetCosmosAccount() const
{
	wstring cosmosAccount = _T("~ Account not set ~");

	auto cURI = Str::explodeIntoTokenVector(GetCosmosDbSqlUri().c_str(), _YTEXT("https://"), false);
	if (!cURI.empty())
	{
		const auto sub = Str::explodeIntoTokenVector(cURI.front(), _YTEXT("."), false);
		if (!sub.empty())
			cosmosAccount = sub.front();
	}

	return cosmosAccount;
}

void Sapio365Session::SetCosmosDbMasterKey(const wstring& p_Val)
{
	m_CosmosDbMasterKey.clear();
	if (CosmosManager().IsValid(p_Val))
		m_CosmosDbMasterKey = p_Val;
}

bool Sapio365Session::IsReadyForCosmos() const
{
	CosmosManager cm;
	return cm.IsValid(m_CosmosDbMasterKey) && cm.IsValid(m_CosmosDbSqlUri);
}

const PooledString& Sapio365Session::GetConnectedUserID() const
{
	ASSERT(GetGraphCache().GetCachedConnectedUser().is_initialized());
	if (GetGraphCache().GetCachedConnectedUser().is_initialized())
		return GetGraphCache().GetCachedConnectedUser().get().m_Id;

	return PooledString::g_EmptyString;
}

const PooledString& Sapio365Session::GetConnectedUserPrincipalName() const
{
	ASSERT(GetGraphCache().GetCachedConnectedUser().is_initialized());
	if (GetGraphCache().GetCachedConnectedUser().is_initialized())
		return GetGraphCache().GetCachedConnectedUser().get().GetUserPrincipalName().get();

	return PooledString::g_EmptyString;
}

const PooledString& Sapio365Session::GetConnectedUserDisplayName() const
{
	ASSERT(GetGraphCache().GetCachedConnectedUser().is_initialized());
	if (GetGraphCache().GetCachedConnectedUser().is_initialized())
		return GetGraphCache().GetCachedConnectedUser().get().GetDisplayName().get();

	return PooledString::g_EmptyString;
}

void Sapio365Session::SetAsyncUserCountTaskData(boost::YOpt<YtriaTaskData> p_TaskData)
{
	m_AsyncUsersCountTaskData = p_TaskData;
}

bool Sapio365Session::IsAsyncUserCountTaskCancel()
{
	return m_AsyncUsersCountTaskData && m_AsyncUsersCountTaskData->IsCanceled();
}

void Sapio365Session::cancelAsyncUserCountTask()
{
	if (m_AsyncUsersCountTaskData)
	{
		if (!m_AsyncUsersCountTaskData->IsComplete() && !m_AsyncUsersCountTaskData->IsCanceled())
			m_AsyncUsersCountTaskData->Cancel();
		while (!m_AsyncUsersCountTaskData->IsComplete() && !m_AsyncUsersCountTaskData->IsCanceled())
			Sleep(50);

		m_AsyncUsersCountTaskData.reset();
	}
}

bool Sapio365Session::IsRBACHideUnscopedObjects() const
{
	return AutomatedApp::IsAutomationRunning() || m_RBACHideUnscopedObjects;
}

void Sapio365Session::SetRBACHideUnscopedObjects(const bool p_Hide)
{
	ASSERT(!AutomatedApp::IsAutomationRunning() || p_Hide);
	if (!AutomatedApp::IsAutomationRunning())
		m_RBACHideUnscopedObjects = p_Hide;
}

void Sapio365Session::SetSessionType(const wstring& p_SessionType)
{
	m_Identifier.m_SessionType = p_SessionType;
}

const wstring& Sapio365Session::GetSessionType() const
{
	return m_Identifier.m_SessionType;
}

bool Sapio365Session::IsUltraAdmin() const
{
	return m_Identifier.m_SessionType == SessionTypes::g_UltraAdmin;
}

bool Sapio365Session::IsAdvanced() const
{
	return m_Identifier.m_SessionType == SessionTypes::g_AdvancedSession;
}

bool Sapio365Session::IsElevated() const
{
	return m_Identifier.m_SessionType == SessionTypes::g_ElevatedSession;
}

bool Sapio365Session::IsRole() const
{
	return m_Identifier.m_SessionType == SessionTypes::g_Role;
}

bool Sapio365Session::IsStandard() const
{
	return m_Identifier.m_SessionType == SessionTypes::g_StandardSession;
}

bool Sapio365Session::IsPartnerAdvanced() const
{
	return m_Identifier.m_SessionType == SessionTypes::g_PartnerAdvancedSession;
}

bool Sapio365Session::IsPartnerElevated() const
{
	return m_Identifier.m_SessionType == SessionTypes::g_PartnerElevatedSession;
}

// static 
bool Sapio365Session::IsCompanyAdmin(std::shared_ptr<const Sapio365Session> p_Session)
{
	bool companyAdmin = false;

	if (p_Session && p_Session->IsAdvanced())
		companyAdmin = IsCompanyAdmin(p_Session->GetMainMSGraphSession(), p_Session->GetConnectedUserID());

	return companyAdmin;
}

bool Sapio365Session::IsCompanyAdmin(std::shared_ptr<MSGraphSession> p_Session, const PooledString& p_UserId)
{
	bool companyAdmin = false;

	if (p_Session)
	{
		CWaitCursor c;

		YtriaTaskData taskData;
		auto roles = [&p_Session, taskData]()
		{
			auto requester = std::make_shared<DirectoryRolesRequester>(std::make_shared<BasicRequestLogger>(_T("directory roles")));
			return safeTaskCall(requester->Send(p_Session, SessionIdentifier(), taskData).Then([requester]()
			{
				return requester->GetData();
			}), p_Session, taskData).get();
		}();

		auto roleIt = std::find_if(roles.begin(), roles.end(), [](auto& p_Role)
		{
			// Find "Company Administrator" role.
			return p_Role.GetRoleTemplateId() && *p_Role.GetRoleTemplateId() == _YTEXT("62e90394-69f5-4237-9190-012177145e10");
		});

		if (roles.end() != roleIt)
		{
			// Request "Company Administrator" role members.
			auto requester = std::make_shared<DirectoryRoleMembersRequester>(roleIt->GetID(), std::make_shared<BasicRequestLogger>(_T("directory role members")));

			auto roleMembers = safeTaskCall(requester->Send(p_Session, SessionIdentifier(), taskData).Then([requester, taskData]()
			{
				return requester->GetData();
			}), p_Session, taskData).get();

			companyAdmin = std::any_of(roleMembers.begin(), roleMembers.end(), [&p_UserId](auto& p_User)
			{
				return p_User.GetID() == p_UserId;
			});
		}
	}

	return companyAdmin;
}

Sapio365Session::GraphError Sapio365Session::ParseGraphJsonError(const wstring& p_JsonError)
{
	GraphError graphError;

	// Unfortunately cpprestsdk add some text before the json
	auto firstBracketPos = Str::find(p_JsonError, _YTEXT("{"));
	if (std::wstring::npos != firstBracketPos)
		graphError.m_ErrorDescription = p_JsonError.substr(firstBracketPos);

	web::json::value jsonVal;
	try
	{
		jsonVal = web::json::value::parse(graphError.m_ErrorDescription);
	}
	catch (const web::json::json_exception&)
	{
		graphError.m_ErrorDescription.clear();
	}

	if (jsonVal.is_object())
	{
		if (jsonVal.has_field(_YTEXT("error")))
			graphError.m_ErrorName = jsonVal.at(_YTEXT("error")).as_string();
		else
			graphError.m_ErrorName.clear();

		if (jsonVal.has_field(_YTEXT("error_description")))
			graphError.m_ErrorDescription = jsonVal.at(_YTEXT("error_description")).as_string();
		else
			graphError.m_ErrorDescription.clear();

		if (jsonVal.has_field(_YTEXT("error_uri")))
			graphError.m_ErrorUri = jsonVal.at(_YTEXT("error_uri")).as_string();
		else
			graphError.m_ErrorUri.clear();

		if (jsonVal.has_field(_YTEXT("error_codes")))
		{
			auto codes = jsonVal.at(_YTEXT("error_codes")).as_array();
			ASSERT(codes.size() == 1); // Does it really happen to be more than 1?
			if (codes.size() > 0)
				graphError.m_ErrorCode = codes[0].to_string();
		}
		else
		{
			graphError.m_ErrorCode.clear();
		}
	}
	else
	{
		// May be some error in the format HTTP - XXX - reason - description
		auto tokens = Str::explodeIntoVector(p_JsonError, wstring(_YTEXT("-")), false, true);
		if (tokens.size() == 3)
		{
			if (Str::beginsWith(tokens[0], _YTEXT("HTTP ")))
			{
				graphError.m_ErrorCode = tokens[0].substr(5);
			}

			auto nameAndDesc = Str::explodeIntoVector(tokens[2], wstring(_YTEXT(":")), false, true);
			if (nameAndDesc.size() == 2)
			{
				graphError.m_ErrorName = nameAndDesc[0];
				// Keep this empty so that the whole error is used (see before return below) 
				//graphError.m_ErrorDescription = nameAndDesc[1];
			}
		}
	}

	if (graphError.m_ErrorDescription.empty())
		graphError.m_ErrorDescription = p_JsonError;

	return graphError;
}

const wstring& Sapio365Session::GetUltraAdminID()
{
	static const wstring g_UAID = _YTEXT("~Ultra|Admin~");
	return g_UAID;
}

shared_ptr<MSGraphSession> Sapio365Session::CreateMSGraphSession()
{
	auto session = std::make_shared<MSGraphSession>(NetworkUtils::GetHttpClientConfig(), nullptr);
	auto authenticator = std::make_shared<AzureADOAuth2BrowserAuthenticator>(session,
		Rest::GetMsGraphEndpoint(),
		_YTEXT(""),
		SessionTypes::GetAdvancedSessionAppID(),
		SessionTypes::GetBasicUserRedirectUri(),
		std::make_shared<DlgBrowserOpener>());

	session->SetErrorHandler(std::make_unique<O365AdminErrorHandler>(session));
	session->SetAuthenticator(authenticator);

	return session;
}

const O365Gateways& Sapio365Session::GetGateways() const
{
	return m_Gateways;
}

O365Gateways& Sapio365Session::GetGateways()
{
	return m_Gateways;
}

void Sapio365Session::SetAdminAccess(const set<RoleDelegationUtil::RBAC_AdminScope>& p_AdminAccess)
{
	m_AdminAccess = p_AdminAccess;
}

bool Sapio365Session::hasAdminAccess(RoleDelegationUtil::RBAC_AdminScope p_AA) const
{
	return m_AdminAccess.find(p_AA) != m_AdminAccess.end();
}

bool Sapio365Session::IsAdminManager() const
{
	return hasAdminAccess(RoleDelegationUtil::RBAC_AdminScope::RBAC_ADMIN_MANAGER) || IsCompanyAdmin();
}

bool Sapio365Session::IsAdminRBAC() const
{
	return hasAdminAccess(RoleDelegationUtil::RBAC_AdminScope::RBAC_ADMIN_RBAC) || IsAdminManager();
}

bool Sapio365Session::IsAdminLogs() const
{
	return hasAdminAccess(RoleDelegationUtil::RBAC_AdminScope::RBAC_ADMIN_LOGS) || IsAdminManager();
}

bool Sapio365Session::IsAdminAnnotations() const
{
	return hasAdminAccess(RoleDelegationUtil::RBAC_AdminScope::RBAC_ADMIN_ANNOTATIONS) || IsAdminManager();
}

// static 
bool Sapio365Session::IsAdminManager(std::shared_ptr<const Sapio365Session> p_Session)
{
	return p_Session && p_Session->IsAdminManager();
}

bool Sapio365Session::IsAdminRBAC(std::shared_ptr<const Sapio365Session> p_Session)
{
	return p_Session && p_Session->IsAdminRBAC();
}

bool Sapio365Session::IsAdminLogs(std::shared_ptr<const Sapio365Session> p_Session)
{
	return p_Session && p_Session->IsAdminLogs();
}

bool Sapio365Session::IsAdminAnnotations(std::shared_ptr<const Sapio365Session> p_Session)
{
	return p_Session && p_Session->IsAdminAnnotations();
}

bool Sapio365Session::IsCompanyAdmin() const
{
	if (!m_IsCompanyAdmin.is_initialized())
		m_IsCompanyAdmin = !IsUltraAdmin() && IsCompanyAdmin(GetMainMSGraphSession(), GetConnectedUserID());
	return m_IsCompanyAdmin.get();
}

void Sapio365Session::SetCommandLineAutomationInProgress(const bool p_AutomationInProgress)
{
	m_CommandLineAutomationInProgress = p_AutomationInProgress;
}

bool Sapio365Session::IsCommandLineAutomationInProgress() const
{
	return m_CommandLineAutomationInProgress;
}

bool Sapio365Session::AreSharingCredentials(const std::shared_ptr<const Sapio365Session>& p_Session1, const std::shared_ptr<const Sapio365Session>& p_Session2)
{
	if (p_Session1 && p_Session2 && p_Session1 != p_Session2)
	{
		if ((p_Session1->GetSessionType() == SessionTypes::g_StandardSession || p_Session1->GetSessionType() == SessionTypes::g_Role)
			&& (p_Session2->GetSessionType() == SessionTypes::g_StandardSession || p_Session2->GetSessionType() == SessionTypes::g_Role))
		{
			return p_Session1->GetEmailOrAppId() == p_Session2->GetEmailOrAppId();
		}

		return p_Session1->GetIdentifier() == p_Session2->GetIdentifier();
	}

	return p_Session1 == p_Session2;
}
