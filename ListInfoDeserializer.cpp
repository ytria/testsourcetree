#include "ListInfoDeserializer.h"

#include "JsonSerializeUtil.h"

void ListInfoDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("contentTypesEnabled"), m_Data.ContentTypesEnabled, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("hidden"), m_Data.Hidden, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("template"), m_Data.Template, p_Object);
}
