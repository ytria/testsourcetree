#include "ModuleEduUsers.h"
#include "RefreshSpecificData.h"
#include "FrameEduUsers.h"
#include "CommandShowEduUsers.h"
#include "EducationUserUpdateRequester.h"

void ModuleEduUsers::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
	case Command::ModuleTask::List:
		showEduUsers(p_Command);
		break;
	case Command::ModuleTask::ApplyChanges:
		applyChanges(p_Command);
		break;
	default:
		ASSERT(false);
	}
}

void ModuleEduUsers::showEduUsers(const Command& p_Command)
{
	AutomationAction* p_Action = p_Command.GetAutomationAction();
	const CommandInfo& info = p_Command.GetCommandInfo();
	const auto p_Origin = info.GetOrigin();

	RefreshSpecificData refreshSpecificData;
	bool isUpdate = false;

	auto p_SourceWindow = p_Command.GetSourceWindow();
	auto sourceCWnd = p_SourceWindow.GetCWnd();

	FrameEduUsers* frame = dynamic_cast<FrameEduUsers*>(info.GetFrame());
	if (nullptr == frame)
	{
		ASSERT(!p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>());

		if (FrameEduUsers::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin = Origin::Tenant;
			moduleCriteria.m_Privilege = info.GetRBACPrivilege();

			if (ShouldCreateFrame<FrameEduUsers>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				frame = new FrameEduUsers(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), _T("Education Users"), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
		}
	}
	else
	{
		isUpdate = true;
	}

	if (nullptr != frame)
	{
		// Refresh after update? Refresh only specific users
		if (p_Command.GetCommandInfo().Data().is_type<vector<FrameEduUsers>>())
		{
			auto schools = p_Command.GetCommandInfo().Data().get_value<vector<EducationUser>>();
			CommandShowEduUsers command(schools, *frame, p_Command.GetAutomationSetup(), p_Command.GetAutomationAction(), p_Command.GetLicenseContext(), isUpdate);
			command.Execute();
		}
		else
		{
			CommandShowEduUsers command(*frame, p_Command.GetAutomationSetup(), p_Command.GetAutomationAction(), p_Command.GetLicenseContext(), isUpdate);
			command.Execute();
		}
	}
	else
	{
		SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
}

void ModuleEduUsers::applyChanges(const Command& p_Command)
{
	auto action = p_Command.GetAutomationAction();

	const auto& info = p_Command.GetCommandInfo();
	auto frame = dynamic_cast<FrameEduUsers*>(info.GetFrame());

	ASSERT(info.Data().is_type<EduUsersChanges>());
	if (info.Data().is_type<EduUsersChanges>())
	{
		const auto& changes = info.Data().get_value<EduUsersChanges>();

		YtriaTaskData taskData;
		if (!changes.m_Updates.empty())
			taskData = addFrameTask(_T("Applying changes"), frame, p_Command, false);

		YSafeCreateTask([bom = frame->GetBusinessObjectManager(), hwnd = frame->GetSafeHwnd(), taskData, changes, action]()
		{
			vector<O365UpdateOperation> updates;

			for (const auto& userUpdate : changes.m_Updates)
			{
				if (taskData.IsCanceled())
					break;

				EducationUserUpdateRequester requester(userUpdate.m_UserId, userUpdate.m_NewValues);
				requester.Send(bom->GetSapio365Session(), taskData).GetTask().get();

				updates.emplace_back(userUpdate.m_RowPk);
				updates.back().StoreResult(requester.GetResult());
			}

			YCallbackMessage::DoPost([action, hwnd, taskData, updates]() mutable
			{
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameEduUsers*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (nullptr != frame)
				{
					frame->GetGrid().ClearLog(taskData.GetId());
					frame->RefreshAfterUpdate(std::move(updates), action);
				}
				else
					SetAutomationGreenLight(action);
				TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
			});
		});
	}
}
