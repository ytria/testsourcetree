#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class TimeZoneBaseDeserializer : public JsonObjectDeserializer, public Encapsulate<TimeZoneBase>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

