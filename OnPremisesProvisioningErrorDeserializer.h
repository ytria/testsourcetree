#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "OnPremisesProvisioningError.h"

class OnPremisesProvisioningErrorDeserializer : public JsonObjectDeserializer, public Encapsulate<OnPremisesProvisioningError>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};
