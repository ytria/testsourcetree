#include "BusinessGroupSettingTemplate.h"

RTTR_REGISTRATION
{
    using namespace rttr;
registration::class_<BusinessGroupSettingTemplate>("GroupSettingTemplate") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Group Setting Template"))))
.constructor()(policy::ctor::as_object);
}

BusinessGroupSettingTemplate::BusinessGroupSettingTemplate()
{
}


BusinessGroupSettingTemplate::BusinessGroupSettingTemplate(const GroupSettingTemplate& p_GroupSettingTemplate)
{
    SetID(p_GroupSettingTemplate.Id ? *p_GroupSettingTemplate.Id : _YTEXT(""));
    SetDisplayName(p_GroupSettingTemplate.DisplayName);
    SetDescription(p_GroupSettingTemplate.Description);
    SetValues(p_GroupSettingTemplate.Values);
}

const boost::YOpt<PooledString>& BusinessGroupSettingTemplate::GetDisplayName() const
{
	return m_DisplayName;
}

void BusinessGroupSettingTemplate::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}

const boost::YOpt<PooledString>& BusinessGroupSettingTemplate::GetDescription() const
{
    return m_Description;
}

void BusinessGroupSettingTemplate::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
    m_Description = p_Val;
}

const std::vector<SettingTemplateValue>& BusinessGroupSettingTemplate::GetValues() const
{
    return m_Values;
}

void BusinessGroupSettingTemplate::SetValues(const std::vector<SettingTemplateValue>& p_Val)
{
    m_Values = p_Val;
}

const wstring BusinessGroupSettingTemplate::g_StringProperty = _YTEXT("System.String");
const wstring BusinessGroupSettingTemplate::g_BoolProperty = _YTEXT("System.Boolean");
const wstring BusinessGroupSettingTemplate::g_GuidProperty = _YTEXT("System.Guid");
const wstring BusinessGroupSettingTemplate::g_Int32Property = _YTEXT("System.Int32");
const wstring BusinessGroupSettingTemplate::g_GroupUnifiedGuestSettingTemplateId = _YTEXT("08d542b9-071f-4e16-94b0-74abb372e3d9");
const wstring BusinessGroupSettingTemplate::g_AllowToAddGuestsName = _YTEXT("AllowToAddGuests");
