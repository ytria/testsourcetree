#include "ModulePrivateChannelMembers.h"

#include "AddPrivateChannelMemberRequester.h"
#include "ChannelPrivateMembersRequester.h"
#include "ChannelRequester.h"
#include "DeletePrivateChannelMemberRequester.h"
#include "FramePrivateChannelMembers.h"
#include "UpdatePrivateChannelMemberRequester.h"

void ModulePrivateChannelMembers::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
	case Command::ModuleTask::List:
		show(p_Command);
		break;
	case Command::ModuleTask::ApplyChanges:
		apply(p_Command);
		break;
	default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
		break;
	}
}

void ModulePrivateChannelMembers::show(const Command& p_Command)
{
	const auto& info = p_Command.GetCommandInfo();
	const auto	p_Origin = info.GetOrigin();

	ASSERT(p_Origin == Origin::Channel);
	if (p_Origin == Origin::Channel)
	{
		auto&	p_IDs = info.GetSubIds();
		auto	p_Frame = dynamic_cast<FramePrivateChannelMembers*>(info.GetFrame());
		auto	p_SourceWindow = p_Command.GetSourceWindow();
		auto	p_Action = p_Command.GetAutomationAction();
		auto	sourceCWnd = p_SourceWindow.GetCWnd();

		auto frame = p_Frame;
		if (nullptr == frame)
		{
			ASSERT(!p_IDs.empty());
			ASSERT(!p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>());

			if (!FramePrivateChannelMembers::CanCreateNewFrame(true, sourceCWnd, p_Action))
				return;

			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin = p_Origin;
			moduleCriteria.m_UsedContainer = ModuleCriteria::UsedContainer::SUBIDS;
			moduleCriteria.m_SubIDs = p_IDs;
			moduleCriteria.m_Privilege = info.GetRBACPrivilege();
			moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

			if (ShouldCreateFrame<FramePrivateChannelMembers>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				frame = new FramePrivateChannelMembers(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), _T("Private Channel Members"), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
		}

		if (nullptr != frame)
		{
			HWND hwnd = frame->GetSafeHwnd();
			const bool isRefresh = nullptr != p_Frame;
			auto taskData = addFrameTask(_T("Show Private Channel Members"), frame, p_Command, !isRefresh);

			using DataType = std::map<PooledString, PooledString>;
			DataType channelNames;

			RefreshSpecificData refreshSpecificData;
			if (info.Data().is_type<DataType>()) // Only happens when the window is first opened
				channelNames = info.Data().get_value<DataType>();
			else if (info.Data().is_type<RefreshSpecificData>()) // Only during a partial refresh
				refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

			if (channelNames.empty())
			{
				ASSERT(nullptr != frame);
				if (nullptr != frame)
					channelNames = frame->GetChannelNames();
			}

			YSafeCreateTaskMutable([this, taskData, refreshSpecificData, hwnd, p_Origin, p_IDs, currentModuleCriteria = frame->GetModuleCriteria(), p_Action, bom = frame->GetBusinessObjectManager(), isRefresh = nullptr != p_Frame, channelNames]() mutable
			{
				auto logger = std::make_shared<BasicRequestLogger>(_T("private members"));

				const bool partialRefresh = !refreshSpecificData.m_Criteria.m_SubIDs.empty();
				const auto& ids = partialRefresh ? refreshSpecificData.m_Criteria.m_SubIDs : p_IDs;

				ASSERT(!ids.empty());

				vector<BusinessChannel> channels;

				for (auto& team : ids)
				{
					const auto& teamID = team.first.first;

					BusinessChannel channel;
					channel.SetGroupId(teamID);

					for (auto& channelID : team.second)
					{
						channel.SetID(channelID);
						auto it = channelNames.find(channelID);
						ASSERT(it != channelNames.end());
						if (it != channelNames.end())
							channel.SetDisplayName(it->second);
						else
							channel.SetDisplayName(boost::none);

						channels.emplace_back(channel);

						// Do we want to request the channel?
						/*auto channelRequester = std::make_shared<ChannelRequester>(teamID, channelID, logger);
						channels.back() = safeTaskCall(channelRequester->Send(bom->GetSapio365Session(), taskData).Then([channelRequester]()
							{
								return channelRequester->GetData();
							}), bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::RBAC_USER), Util::ObjectErrorHandler<ChannelRequester>, taskData.GetOriginator()).get();*/
						
						if (!channels.back().GetError())
						{
							auto privateMembersRequester = std::make_shared<ChannelPrivateMembersRequester>(teamID, channelID, logger);
							auto privateMembers = safeTaskCall(privateMembersRequester->Send(bom->GetSapio365Session(), taskData).Then([privateMembersRequester]()
								{
									return privateMembersRequester->GetData();
								}), bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ListErrorHandler<BusinessAADUserConversationMember>, taskData).get();

							if (privateMembers.size() == 1 && privateMembers[0].GetError())
								channels.back().SetPrivateChannelMembersError(privateMembers[0].GetError());
							else
								channels.back().SetPrivateChannelMembers(privateMembers);
						}
					}
				}

				YCallbackMessage::DoPost([hwnd, taskData, p_Action, channels, isRefresh, isCanceled = taskData.IsCanceled(), channelNames, partialRefresh]
					{
						bool shouldFinishTask = true;
						auto frame = ::IsWindow(hwnd) ? dynamic_cast<FramePrivateChannelMembers*>(CWnd::FromHandle(hwnd)) : nullptr;
						if (ShouldBuildView(hwnd, isCanceled, taskData, false))
						{
							ASSERT(nullptr != frame);

							frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting members for %s channels...", Str::getStringFromNumber(channels.size()).c_str()));
							frame->GetGrid().ClearLog(taskData.GetId());
							//frame->GetGrid().ClearStatus();

							{
								CWaitCursor _;
								if (frame->HasLastUpdateOperations())
								{
									frame->ShowPrivateChannelMembers(channels, frame->GetLastUpdateOperations(), !partialRefresh);
									frame->ForgetLastUpdateOperations();
								}
								else
								{
									frame->ShowPrivateChannelMembers(channels, {}, !partialRefresh);
								}
								frame->SetChannelNames(channelNames); // Store this for next refresh
							}

							if (!isRefresh)
							{
								frame->UpdateContext(taskData, hwnd);
								shouldFinishTask = false;
							}
						}

						if (shouldFinishTask)
						{
							// Because we didn't (and can't) call UpdateContext here.
							ModuleBase::TaskFinished(frame, taskData, p_Action);
						}
					});
			});
		}
		else
		{
			ASSERT(nullptr == p_Action);
			SetAutomationGreenLight(p_Action, _YTEXT(""));
		}
	}
	else
	{
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleContact_showContacts_3, _YLOC(" - origin not set"), _YR("Y2466")).c_str());
	}
}

void ModulePrivateChannelMembers::apply(const Command& p_Command)
{
	CommandInfo info = p_Command.GetCommandInfo();
	AutomationAction* action = p_Command.GetAutomationAction();
	auto frame = dynamic_cast<FramePrivateChannelMembers*>(info.GetFrame());
	ASSERT(nullptr != frame);

	auto bom = frame->GetBusinessObjectManager();
	ASSERT(info.Data().is_type<GridPrivateChannelMembers::Changes>());
	if (info.Data().is_type<GridPrivateChannelMembers::Changes>())
	{
		const auto& changes = info.Data().get_value<GridPrivateChannelMembers::Changes>();
		if (!changes.GetMemberRemovals().empty() || !changes.GetMemberAdditions().empty() || !changes.GetMemberRoleUpdates().empty())
		{
			auto taskData = addFrameTask(YtriaTranslate::Do(ModuleDrive_applyChanges_1, _YLOC("Applying changes")).c_str(), frame, p_Command, false);
			YSafeCreateTask([this, changes, p_Command, criteria = frame->GetModuleCriteria(), hwnd = frame->m_hWnd, taskData, action, bom, moduleCriteria = frame->GetModuleCriteria()]()
			{
				vector<O365UpdateOperation> updates;

				for (const auto& memberRemoval : changes.GetMemberRemovals())
				{
					if (taskData.IsCanceled())
						break;
					DeletePrivateChannelMemberRequester requester(memberRemoval.m_MemberId, memberRemoval.m_TeamId, memberRemoval.m_ChannelId);
					requester.Send(bom->GetSapio365Session(), taskData).GetTask().get();

					updates.emplace_back(memberRemoval.m_RowPk);
					updates.back().StoreResult(requester.GetResult());
				}

				for (const auto& memberAddition : changes.GetMemberAdditions())
				{
					if (taskData.IsCanceled())
						break;

					AddPrivateChannelMemberRequester requester(memberAddition.m_Member, memberAddition.m_TeamId, memberAddition.m_ChannelId);
					requester.Send(bom->GetSapio365Session(), taskData).GetTask().get();

					updates.emplace_back(memberAddition.m_RowPk);
					updates.back().StoreResult(requester.GetResult());
				}

				for (const auto& memberRoleUpdate : changes.GetMemberRoleUpdates())
				{
					if (taskData.IsCanceled())
						break;

					UpdatePrivateChannelMemberRequester requester(memberRoleUpdate.m_Member, memberRoleUpdate.m_TeamId, memberRoleUpdate.m_ChannelId);
					requester.Send(bom->GetSapio365Session(), taskData).GetTask().get();

					updates.emplace_back(memberRoleUpdate.m_RowPk);
					updates.back().StoreResult(requester.GetResult());
				}

				YCallbackMessage::DoPost([action, taskData, updates, hwnd]() mutable
				{
					auto frame = ::IsWindow(hwnd) ? dynamic_cast<FramePrivateChannelMembers*>(CWnd::FromHandle(hwnd)) : nullptr;
					if (nullptr != frame)
					{
						frame->GetGrid().ClearLog(taskData.GetId());
						frame->RefreshAfterUpdate(std::move(updates), action);
					}
					else
						SetAutomationGreenLight(action);
					TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
				});
			});
		}
	}
}
