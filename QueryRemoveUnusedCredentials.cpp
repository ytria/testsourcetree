#include "QueryRemoveUnusedCredentials.h"
#include "SessionsSqlEngine.h"
#include "SqlQueryPreparedStatement.h"

QueryRemoveUnusedCredentials::QueryRemoveUnusedCredentials(SessionsSqlEngine& p_Engine)
	:m_Engine(p_Engine)
{
}

void QueryRemoveUnusedCredentials::Run()
{
	const wstring deleteQueryStr = _YTEXT(
		R"(DELETE FROM Credentials WHERE Id IN (SELECT Credentials.Id FROM Credentials LEFT JOIN Sessions On Sessions.CredentialsId=Credentials.Id WHERE Sessions.CredentialsId IS NULL))"
	);
	SqlQueryPreparedStatement query(m_Engine, deleteQueryStr);
	query.Run();

	m_Status = query.GetStatus();
}
