#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "SearchResult.h"

class SearchResultDeserializer : public JsonObjectDeserializer, public Encapsulate<SearchResult>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

