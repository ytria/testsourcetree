#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "IncludedPath.h"

namespace Cosmos
{
    class IncludedPathDeserializer : public JsonObjectDeserializer, public Encapsulate<Cosmos::IncludedPath>
    {
    public:
        void DeserializeObject(const web::json::object& p_Object) override;
    };
}
