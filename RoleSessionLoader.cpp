#include "OAuth2BrowserSessionLoader.h"
#include "QueryLoadRole.h"
#include "QueryRoleExists.h"
#include "QuerySessionExists.h"
#include "RoleSessionLoader.h"
#include "SessionTypes.h"
#include "SessionsSqlEngine.h"

RoleSessionLoader::RoleSessionLoader(const SessionIdentifier& p_Identifier)
	:ISessionLoader(p_Identifier)
{
	ASSERT(p_Identifier.m_SessionType == SessionTypes::g_Role && p_Identifier.m_RoleID != SessionIdentifier::g_NoRole);
}

void RoleSessionLoader::SetHideRoleUnscopedObjectsOverride(bool p_HideRoleUnscopedObjects)
{
	m_HideRoleUnscopedObjectsOverride = p_HideRoleUnscopedObjects;
}


TaskWrapper<std::shared_ptr<Sapio365Session>> RoleSessionLoader::Load(const std::shared_ptr<Sapio365Session>& p_SapioSession)
{
	QuerySessionExists sessionExists(GetSessionId(), SessionsSqlEngine::Get());
	sessionExists.Run();

	PersistentSession persistentSession;

	// Are we opening a role which doesn't have a corresponding session in the database? If so, create potentially missing role and session
	if (GetSessionId().m_SessionType == SessionTypes::g_Role && !sessionExists.Exists())
	{
		QueryRoleExists roleExists(GetSessionId().m_RoleID, SessionsSqlEngine::Get());
		roleExists.Run();
		ASSERT(roleExists.Exists());
		if (roleExists.Exists())
		{
			QueryLoadRole queryLoad(GetSessionId().m_RoleID, SessionsSqlEngine::Get());
			queryLoad.Run();

			SessionIdentifier standardIdentifier(GetSessionId());
			standardIdentifier.m_SessionType = SessionTypes::g_StandardSession;
			standardIdentifier.m_RoleID = 0;

			// Let's get existing credentials
			PersistentSession roleSession = SessionsSqlEngine::Get().LoadWithAnyRole(standardIdentifier);

			roleSession.SetRoleId(GetSessionId().m_RoleID);
			roleSession.SetSessionType(GetSessionId().m_SessionType);
			roleSession.SetAccessCount(1);
			roleSession.SetCreatedOn(YTimeDate::GetCurrentTimeDate());
			roleSession.SetLastUsedOn(YTimeDate::GetCurrentTimeDate());
			roleSession.SetRoleNameInDb(queryLoad.GetRoleName());
			roleSession.SetRoleShowOwnScopeOnly(true);
			SessionsSqlEngine::Get().Save(roleSession);
			persistentSession = SessionsSqlEngine::Get().Load(roleSession.GetIdentifier());
		}
	}
	else
		persistentSession = GetPersistentSession(p_SapioSession);

	OAuth2BrowserSessionLoader loader(GetSessionId(), persistentSession);
	return loader.Load(p_SapioSession).Then([persistentSession, hideUnscopedOverride = m_HideRoleUnscopedObjectsOverride] (const std::shared_ptr<Sapio365Session> & p_SapioSession) {
		
		if (hideUnscopedOverride)
		{
			p_SapioSession->SetRBACHideUnscopedObjects(*hideUnscopedOverride);
			persistentSession.GetRoleShowOwnScopeOnly();
		}

		return p_SapioSession;
	});
}
