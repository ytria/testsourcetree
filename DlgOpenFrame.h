#pragma once

#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "YAdvancedHtmlView.h"

class DlgOpenFrame	: public ResizableDialog
					, public YAdvancedHtmlView::IPostedDataTarget
{
private:
	class IFrameItem
	{
	protected:
		IFrameItem(UINT p_ID, const wstring& p_HBSName, const wstring& p_Title, const wstring& p_Text);

	public:
		UINT GetID() const;

	protected:
		const wstring& getHBSName() const;
		const wstring& getTitle() const;
		const wstring& getText() const;

	private:
		const UINT m_ID;
		const wstring m_HBSName;
		const wstring m_Title;
		const wstring m_Text;

	protected:
		friend class DlgOpenFrame;
	};

public:
	class DetachNewWindow : public IFrameItem
	{
	public:
		DetachNewWindow();
		static constexpr UINT g_ID = 16;
	};

	class DetachHistory : public IFrameItem
	{
	public:
		DetachHistory();
		static constexpr UINT g_ID = 17;
	};

	class OverrideHistory : public IFrameItem
	{
	public:
		OverrideHistory();
		static constexpr UINT g_ID = 18;
	};

	class CreateNewWindow : public IFrameItem
	{
	public:
		CreateNewWindow();
		static constexpr UINT g_ID = 19;
	};

	class ReuseWindowFrame : public IFrameItem
	{
	public:
		ReuseWindowFrame();
		static constexpr UINT g_ID = 20;
	};

public:
	struct CheckboxText
	{
		wstring m_Main;
		wstring m_Sub;
	};
	DlgOpenFrame(const wstring& p_Title, const vector<IFrameItem>& p_Items, const CheckboxText& p_CheckBoxText, CWnd* p_Parent);

	enum { IDD = IDD_DLG_OPENFRAME_HTML };

	bool GetCheckboxState() const;

	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data);

protected:
    virtual BOOL OnInitDialogSpecificResizable() override;

    ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);
	bool checkNoDuplicates();

private:
	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	bool m_checkBoxChecked;
	wstring m_Title;
	CheckboxText m_CheckBoxText;
	vector<IFrameItem> m_Items;
	//bool m_CheckboxState;

	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;
	static const wstring g_CheckboxName;
	static const wstring g_CancelName;
};

class DlgOpenFrameForHistory : public DlgOpenFrame
{
public:
	DlgOpenFrameForHistory(CWnd* p_Parent);
};

class DlgOpenFrameForReuse : public DlgOpenFrame
{
public:
	DlgOpenFrameForReuse(CWnd* p_Parent);
};