#pragma once

#include "IPropertySetBuilder.h"

class CachedUserPropertySet : public IPropertySetBuilder
{
public:
    virtual vector<rttr::property> GetPropertySet() const override;
};

class CachedDeletedUserPropertySet : public CachedUserPropertySet
{
public:
	virtual vector<rttr::property> GetPropertySet() const override;
};

