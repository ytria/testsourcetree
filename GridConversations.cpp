#include "GridConversations.h"

#include "AutomationWizardConversations.h"
#include "BasicGridSetup.h"
#include "BusinessGroup.h"
#include "BusinessGroupConfiguration.h"
#include "FrameConversations.h"
#include "GridHelpers.h"
#include "GridUtil.h"
#include "GridUpdater.h"
#include "O365AdminUtil.h"

BEGIN_MESSAGE_MAP(GridConversations, ModuleO365Grid<BusinessConversation>)
	NEWMODULE_COMMAND(ID_CONVERSATIONSGRID_SHOWPOSTS, ID_CONVERSATIONSGRID_SHOWPOSTS_FRAME, ID_CONVERSATIONSGRID_SHOWPOSTS_NEWFRAME, OnShowConversationPosts, OnUpdateShowPosts)
END_MESSAGE_MAP()

GridConversations::GridConversations()
	: m_HasLoadMoreAdditionalInfo(false)
{
    //UseDefaultActions(/*O365Grid::ACTION_CREATE | */O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT/* | O365Grid::ACTION_VIEW*/);

	initWizard<AutomationWizardConversations>(_YTEXT("Automation\\Conversations"));
}

void GridConversations::BuildView(const O365DataMap<BusinessGroup, vector<BusinessConversation>>& p_Conversations, bool p_FullPurge)
{
	GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID
			| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			| GridUpdaterOptions::REFRESH_PROCESS_DATA
		)
	);

    for (const auto& keyVal : p_Conversations)
    {
		const auto& businessGroup = keyVal.first;

		if (m_HasLoadMoreAdditionalInfo)
		{
			if (businessGroup.IsMoreLoaded())
				m_MetaIDsMoreLoaded.insert(businessGroup.GetID());
			else
				m_MetaIDsMoreLoaded.erase(businessGroup.GetID());
		}

		auto groupRow = m_Template.AddRow(*this, businessGroup, {}, updater, false, nullptr, {}, true);
        ASSERT(nullptr != groupRow);
        if (nullptr != groupRow)
        {
			updater.GetOptions().AddRowWithRefreshedValues(groupRow);
            updater.GetOptions().AddPartialPurgeParentRow(groupRow);

			groupRow->SetHierarchyParentToHide(true);
			SetRowObjectType(groupRow, businessGroup, businessGroup.HasFlag(BusinessObject::CANCELED));

            if (keyVal.second.size() == 1 && keyVal.second[0].GetError())
            {
				updater.OnLoadingError(groupRow, *keyVal.second[0].GetError());
            }
            else
            {
				// Clear previous error
				if (groupRow->IsError())
					groupRow->ClearStatus();

                const auto& conversations = keyVal.second;
                for (const auto& conversation : conversations)
                {
                    GridBackendRow* conversationRow = fillRow(conversation, groupRow, updater);
					ASSERT(nullptr != conversationRow);
					if (nullptr != conversationRow)
					{
						updater.GetOptions().AddRowWithRefreshedValues(conversationRow);
						conversationRow->SetParentRow(groupRow);
					}
                }
            }

			AddRoleDelegationFlag(groupRow, businessGroup);
        }
    }
}

ModuleCriteria GridConversations::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria;

    GridFrameBase* frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
    ASSERT(nullptr != frame);
    if (nullptr != frame)
    {
        criteria = frame->GetModuleCriteria();
        criteria.m_IDs.clear();
        for (auto row : p_Rows)
            criteria.m_IDs.insert(row->GetField(GetColId()).GetValueStr());
    }

    return criteria;
}

void GridConversations::customizeGrid()
{
	static const wstring g_FamilyGroup = YtriaTranslate::Do(GridConversations_customizeGrid_1, _YLOC("Group")).c_str();
	{
		BasicGridSetup setup(GridUtil::g_AutoNameConversations, LicenseUtil::g_codeSapio365conversations,
		{
			{ &m_Template.m_ColumnDisplayName, YtriaTranslate::Do(GridConversations_customizeGrid_2, _YLOC("Group Display Name")).c_str(), g_FamilyGroup },
			{ nullptr, _YTEXT(""), _YTEXT("") },
			{ &m_Template.m_ColumnID, YtriaTranslate::Do(GridConversations_customizeGrid_3, _YLOC("Group ID")).c_str(),	g_FamilyGroup }
		});
		setup.Setup(*this, false);
	}

	setBasicGridSetupHierarchy({ { { m_Template.m_ColumnDisplayName, 0 } }
								, { rttr::type::get<BusinessConversationThread>().get_id() }
								, { rttr::type::get<BusinessConversationThread>().get_id(), rttr::type::get<BusinessConversation>().get_id(), rttr::type::get<BusinessGroup>().get_id() } });

	m_Template.m_ColumnIsTeam = AddColumnIcon(_YUID("meta.isTeam"), YtriaTranslate::Do(GridGroups_customizeGrid_3, _YLOC("Is a Team")).c_str(), g_FamilyGroup);
	SetColumnPresetFlags(m_Template.m_ColumnIsTeam, { g_ColumnsPresetDefault });
	MoveColumnBefore(m_Template.m_ColumnIsTeam, m_Template.m_ColumnID);
	m_Template.m_ColumnCombinedGroupType = AddColumn(_YUID("meta.groupType"), YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_5, _YLOC("Group Type")).c_str(), g_FamilyGroup, g_ColumnSize12, { g_ColumnsPresetDefault });
	MoveColumnBefore(m_Template.m_ColumnCombinedGroupType, m_Template.m_ColumnID);

	m_MetaColumns.insert(m_MetaColumns.end(), { /*m_Template.m_ColumnID, */m_Template.m_ColumnDisplayName, m_Template.m_ColumnIsTeam, m_Template.m_ColumnCombinedGroupType });

	/*Add additional meta columns*/
	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		for (const auto& c : *metaDataColumnInfos)
		{
			ASSERT(nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()));
			if (nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()))
			{
				auto col = c.AddTo(*this, g_FamilyGroup);
				m_Template.GetColumnForProperty(c.GetPropertyName()) = col;
				m_MetaColumns.emplace_back(col);
				m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
			}
		}
	}

    m_ColTopic					= AddColumn(		_YUID("topic"),				YtriaTranslate::Do(GridConversations_customizeGrid_4, _YLOC("Topic")).c_str(),				g_FamilyInfo,		g_ColumnSize32,	{ g_ColumnsPresetDefault });
    m_ColHasAttachments			= AddColumnCheckBox(_YUID("hasAttachments"),	YtriaTranslate::Do(GridConversations_customizeGrid_5, _YLOC("Has Attachments")).c_str(),		g_FamilyInfo,		g_ColumnSize6,	{ g_ColumnsPresetDefault });
    m_ColIsLocked               = AddColumnCheckBox(_YUID("isLocked"),			YtriaTranslate::Do(GridConversations_customizeGrid_6, _YLOC("Is Locked")).c_str(),			g_FamilyInfo,		g_ColumnSize6,	{ g_ColumnsPresetDefault });
	static const wstring g_FamilyPersons = YtriaTranslate::Do(GridConversations_customizeGrid_7, _YLOC("Persons")).c_str();
	m_ColUniqueSenders			= AddColumn(_YUID("uniqueSenders"),				YtriaTranslate::Do(GridConversations_customizeGrid_8, _YLOC("Unique Senders")).c_str(),		g_FamilyPersons,	g_ColumnSize32,	{ g_ColumnsPresetDefault });
	m_ColLastDeliveredDateTime	= AddColumnDate(_YUID("lastDeliveredDateTime"), YtriaTranslate::Do(GridConversations_customizeGrid_9, _YLOC("Last Delivered")).c_str(),		g_FamilyPersons,	g_ColumnSize16,	{ g_ColumnsPresetDefault });
	m_ColPreview				= AddColumnRichText(_YUID("preview"),			YtriaTranslate::Do(GridConversations_customizeGrid_10, _YLOC("Preview")).c_str(),				g_FamilyInfo,		g_ColumnSize32,	{ g_ColumnsPresetDefault });
    m_ColCcRecipientsName       = AddColumn(_YUID("ccRecipients.name"),			YtriaTranslate::Do(GridConversations_customizeGrid_11, _YLOC("Copy To - Name")).c_str(),		g_FamilyPersons,	g_ColumnSize22,	{ g_ColumnsPresetDefault });
    m_ColCcRecipientsAddress    = AddColumn(_YUID("ccRecipients.address"),		YtriaTranslate::Do(GridConversations_customizeGrid_12, _YLOC("Copy To - Email")).c_str(),		g_FamilyPersons,	g_ColumnSize32);
    m_ColToRecipientsName       = AddColumn(_YUID("toRecipients.name"),			YtriaTranslate::Do(GridConversations_customizeGrid_13, _YLOC("Recipients - Name")).c_str(),	g_FamilyPersons,	g_ColumnSize22,	{ g_ColumnsPresetDefault });
    m_ColToRecipientsAddress    = AddColumn(_YUID("toRecipients.address"),		YtriaTranslate::Do(GridConversations_customizeGrid_14, _YLOC("Recipients - Email")).c_str(),	g_FamilyPersons,	g_ColumnSize32);
	addColumnGraphID();

	m_ColCcRecipientsName->SetMultivalueFamily(YtriaTranslate::Do(GridConversations_customizeGrid_15, _YLOC("CopyTo")).c_str());
	m_ColCcRecipientsName->AddMultiValueExplosionSister(m_ColCcRecipientsAddress);
	m_ColToRecipientsName->SetMultivalueFamily(YtriaTranslate::Do(GridConversations_customizeGrid_16, _YLOC("Recipients")).c_str());
	m_ColToRecipientsName->AddMultiValueExplosionSister(m_ColToRecipientsAddress);

    AddColumnForRowPK(m_Template.m_ColumnID);
    AddColumnForRowPK(GetColId());
    SetColumnsVisible(vector<GridBackendColumn*> { m_Template.m_ColumnID }, false);

	m_Template.CustomizeGrid(*this);

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameConversations*>(GetParentFrame()));
}

wstring GridConversations::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Group"), m_Template.m_ColumnDisplayName } }, m_ColTopic);
}

void GridConversations::OnShowConversationPosts()
{
	if (openModuleConfirmation())
	{
		CommandInfo info;

		O365SubSubIdsContainer ids;
		map<PooledString, PooledString> threadsTopics;

		std::set<PooledString> threadIds;

		for (auto row : GetBackend().GetSelectedRowsInOrder())
		{
			if (!row->IsError()
				&& IsGroupAuthorizedByRoleDelegation(row->GetField(m_Template.m_ColumnID).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CONVERSATIONS_READ))
			{
				if (GridUtil::isBusinessType<BusinessConversationThread>(row))
				{
					addThreadToInfo(row, ids, threadsTopics, threadIds);
				}
				else if (GridUtil::isBusinessType<BusinessConversation>(row) || GridUtil::isBusinessType<BusinessGroup>(row))
				{
					vector<GridBackendRow*> childrenRows;
					GetBackend().GetHierarchyChildren(row, childrenRows);

					for (auto childRow : childrenRows)
					{
						if (GridUtil::isBusinessType<BusinessConversationThread>(childRow))
							addThreadToInfo(childRow, ids, threadsTopics, threadIds);
					}
				}
			}
		}

		const bool noID = ids.empty();
		if (!noID && showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT))
		{
			info.Data() = threadsTopics;
			info.GetSubSubIds() = ids;
			info.SetOrigin(Origin::Conversations);
			auto f = dynamic_cast<FrameConversations*>(GetParentFrame());
			ASSERT(nullptr != f);
			if (nullptr != f)
				info.SetRBACPrivilege(f->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Post, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowPost, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
			AutomationGreenlight(noID ? YtriaTranslate::DoError(GridConversations_OnShowConversationPosts_3, _YLOC("No Conversation ID"),_YR("Y2443")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"),_YR("Y2439")).c_str());
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2441")).c_str());
	}
}

void GridConversations::OnUpdateShowPosts(CCmdUI* pCmdUI)
{
    setTextFromProfUISCommand(*pCmdUI);

	const bool enable = std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& p_Row)
	{
		return !p_Row->IsError()
			&& (GridUtil::isBusinessType<BusinessConversationThread>(p_Row) || GridUtil::isBusinessType<BusinessConversation>(p_Row) || GridUtil::isBusinessType<BusinessGroup>(p_Row))
			&& IsGroupAuthorizedByRoleDelegation(p_Row->GetField(m_Template.m_ColumnID).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CONVERSATIONS_READ);

	});
    
    pCmdUI->Enable(enable);
}

GridBackendRow* GridConversations::fillRow(const BusinessConversation& p_Conversation, GridBackendRow* p_ParentRow, GridUpdater& p_Updater)
{
    GridBackendField fieldConvId(p_Conversation.GetID(), GetColId());

	auto conversationRow = m_RowIndex.GetRow({ p_ParentRow->GetField(m_Template.m_ColumnID), fieldConvId}, true, true);

	ASSERT(nullptr != conversationRow);
	if (nullptr != conversationRow)
	{
		// Meta fields
		for (auto c : m_MetaColumns)
		{
			if (p_ParentRow->HasField(c))
				conversationRow->AddField(p_ParentRow->GetField(c));
			else
				conversationRow->RemoveField(c);
		}

		updateRow(p_Conversation, conversationRow, &p_Updater);
	}

	return conversationRow;
}

void GridConversations::updateRow(const BusinessConversation& p_Conversation, GridBackendRow* p_ConversationRow, GridUpdater* p_Updater)
{
	ASSERT(nullptr != p_ConversationRow);
    if (nullptr != p_ConversationRow)
    {
		SetRowObjectType(p_ConversationRow, p_Conversation, p_Conversation.HasFlag(BusinessObject::CANCELED));
        p_ConversationRow->AddField(p_Conversation.GetHasAttachments(), m_ColHasAttachments);
        p_ConversationRow->AddField(p_Conversation.GetLastDeliveredDateTime(), m_ColLastDeliveredDateTime);
        p_ConversationRow->AddFieldForRichText(p_Conversation.GetPreview(), m_ColPreview);
        p_ConversationRow->AddField(p_Conversation.GetTopic(), m_ColTopic);
        p_ConversationRow->AddField(p_Conversation.GetUniqueSenders(), m_ColUniqueSenders);

		ASSERT(p_ConversationRow->HasField(m_Template.m_ColumnID));
        const auto& groupIdField = p_ConversationRow->GetField(m_Template.m_ColumnID);

        for (const auto& thread : p_Conversation.GetConversationThreads())
        {
            GridBackendField fieldThreadId(thread.GetID(), GetColId());

            auto threadRow = m_RowIndex.GetRow({ groupIdField, fieldThreadId}, true, true);
            ASSERT(nullptr != threadRow);
            if (nullptr != threadRow)
            {
				SetRowObjectType(threadRow, thread, thread.HasFlag(BusinessObject::CANCELED));

				// Meta fields
				for (auto c : m_MetaColumns)
				{
					if (p_ConversationRow->HasField(c))
						threadRow->AddField(p_ConversationRow->GetField(c));
					else
						threadRow->RemoveField(c);
				}
                threadRow->AddField(thread.GetTopic(), m_ColTopic);
                threadRow->AddField(thread.GetHasAttachments(), m_ColHasAttachments);
                threadRow->AddField(thread.GetLastDeliveredDateTime(), m_ColLastDeliveredDateTime);
                threadRow->AddField(thread.GetUniqueSenders(), m_ColUniqueSenders);
                threadRow->AddFieldForRichText(thread.GetPreview(), m_ColPreview);
                threadRow->AddField(thread.GetIsLocked(), m_ColIsLocked);

                auto ccRecipients = GridHelpers::ExtractEmailAddresses(thread.GetCcRecipients());
                threadRow->AddField(ccRecipients.first, m_ColCcRecipientsAddress);
                threadRow->AddField(ccRecipients.second, m_ColCcRecipientsName);

                auto toRecipients = GridHelpers::ExtractEmailAddresses(thread.GetToRecipients());
                threadRow->AddField(toRecipients.first, m_ColToRecipientsAddress);
                threadRow->AddField(toRecipients.second, m_ColToRecipientsName);

                threadRow->SetParentRow(p_ConversationRow);

				if (nullptr != p_Updater)
					p_Updater->GetOptions().AddRowWithRefreshedValues(threadRow);
            }
        }
	}
}

BusinessConversation GridConversations::getBusinessObject(GridBackendRow* p_ConversationRow) const
{
	BusinessConversation bc;

	ASSERT(nullptr != p_ConversationRow);
	if (nullptr != p_ConversationRow)
	{
		{
			const auto& field = p_ConversationRow->GetField(GetColId());
			if (field.HasValue())
				bc.SetID(field.GetValueStr());
		}

		{
			const auto& field = p_ConversationRow->GetField(m_ColHasAttachments);
			if (field.HasValue())
				bc.SetHasAttachments(field.GetValueBool());
		}

		{
			const auto& field = p_ConversationRow->GetField(m_ColLastDeliveredDateTime);
			if (field.HasValue())
				bc.SetLastDeliveredDateTime(boost::YOpt<YTimeDate>(field.GetValueTimeDate()));
		}

		{
			const auto& field = p_ConversationRow->GetField(m_ColPreview);
			if (field.HasValue())
				bc.SetPreview(boost::YOpt<PooledString>(field.GetValueStr()));
		}

		{
			const auto& field = p_ConversationRow->GetField(m_ColTopic);
			if (field.HasValue())
				bc.SetTopic(boost::YOpt<PooledString>(field.GetValueStr()));
		}

		{
			const auto& field = p_ConversationRow->GetField(m_ColUniqueSenders);
			if (field.HasValue())
			{
				if (field.IsMulti())
					bc.SetUniqueSenders(*field.GetValuesMulti<PooledString>());
				else
					bc.SetUniqueSenders(vector<PooledString>({ field.GetValueStr() }));
			}
		}
	}

	return bc;
}

void GridConversations::UpdateBusinessObjects(const vector<BusinessConversation>& p_Conversations, bool p_SetModifiedStatus)
{
	bool modified = false;
	for (const auto& conversation : p_Conversations)
		modified = updateRowForBusinessConversation(conversation, p_SetModifiedStatus) || modified;

	if (modified)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridConversations::RemoveBusinessObjects(const vector<BusinessConversation>& p_Conversations)
{
	bool removed = false;
	for (const auto& conversation : p_Conversations)
		removed = removeRowForBusinessConversation(conversation) || removed;

	if (removed)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
}

void GridConversations::InitializeCommands()
{
    const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

    {
        CExtCmdItem _cmd;

        _cmd.m_nCmdID		= ID_CONVERSATIONSGRID_SHOWPOSTS;
        _cmd.m_sMenuText	= YtriaTranslate::Do(GridConversations_InitializeCommands_1, _YLOC("Show Posts...")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridConversations_InitializeCommands_2, _YLOC("Show Posts...")).c_str();
        _cmd.m_sAccelText	= _YTEXT("");
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CONVERSATIONS_SHOWPOSTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CONVERSATIONSGRID_SHOWPOSTS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridConversations_InitializeCommands_3, _YLOC("Show Posts")).c_str(),
			YtriaTranslate::Do(GridConversations_InitializeCommands_4, _YLOC("Display all posts for the selected conversations.")).c_str(),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_CONVERSATIONSGRID_SHOWPOSTS_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridConversations_InitializeCommands_5, _YLOC("Show Posts...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_CONVERSATIONSGRID_SHOWPOSTS_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridConversations_InitializeCommands_6, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
    }

	ModuleO365Grid<BusinessConversation>::InitializeCommands();
}

void GridConversations::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {
        pPopup->ItemInsert(ID_CONVERSATIONSGRID_SHOWPOSTS);
        pPopup->ItemInsert(); // Sep
    }

	ModuleO365Grid<BusinessConversation>::OnCustomPopupMenu(pPopup, nStart);
}

bool GridConversations::updateRowForBusinessConversation(const BusinessConversation& p_Conversation, bool p_SetModifiedStatus)
{
	bool modified = false;
	GridBackendRow* rowToUpdate = nullptr;

	vector< GridBackendRow* > ListRows;
	GetAllNonGroupRows(ListRows);
	for (const auto& Row : ListRows)
	{
		const GridBackendField& IDField = Row->GetField(GetColId());
		if (IDField.GetValueStr() == (wstring)p_Conversation.GetID())
		{
			rowToUpdate = Row;
			break;
		}
	}

	if (nullptr != rowToUpdate)
	{
		updateRow(p_Conversation, rowToUpdate, nullptr);
		if (rowToUpdate->HasModifiedField() && p_SetModifiedStatus)
			OnRowModified(rowToUpdate);
		else
			rowToUpdate->SetEditModified(false);

		modified = rowToUpdate->HasModifiedField();
	}

	return modified;
}

bool GridConversations::removeRowForBusinessConversation(const BusinessConversation& p_Conversation)
{
	bool removed = false;

	vector< GridBackendRow* > ListRows;
	GetAllNonGroupRows(ListRows);
	for (const auto& Row : ListRows)
	{
		const GridBackendField& IDField = Row->GetField(GetColId());
		if (IDField.GetValueStr() == (wstring)p_Conversation.GetID())
		{
			removed = RemoveRow(Row);
			break;
		}
	}

	return removed;
}

void GridConversations::addThreadToInfo(GridBackendRow* threadRow, O365SubSubIdsContainer& p_Ids, std::map<PooledString, PooledString>& p_ThreadsTopic, std::set<PooledString>& p_UsedThreadIds)
{
    PooledString threadId = threadRow->GetField(GetColId()).GetValueStr();
    if (p_UsedThreadIds.find(threadId) == std::end(p_UsedThreadIds))
    {
		PooledString groupId = threadRow->GetField(m_Template.m_ColumnID).GetValueStr();
        PooledString conversationId, conversationTopic;
        ASSERT(nullptr != threadRow->GetParentRow());
        if (nullptr != threadRow->GetParentRow())
        {
            conversationId = threadRow->GetParentRow()->GetField(GetColId()).GetValueStr();
            conversationTopic = threadRow->GetParentRow()->GetField(m_ColTopic).GetValueStr();
            p_ThreadsTopic[threadId] = conversationTopic;
        }

        ASSERT(!groupId.IsEmpty() && !threadId.IsEmpty() && !conversationId.IsEmpty());

        auto& groupKey = p_Ids[groupId];
        groupKey[conversationId].insert(threadId);

        p_UsedThreadIds.insert(threadId);
    }
}

bool GridConversations::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return GridConversationsBaseClass::IsRowValidForNoFieldText(p_Row, p_Col)
		&& (!p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore)
			|| m_MetaIDsMoreLoaded.end() == m_MetaIDsMoreLoaded.find(p_Row->GetField(m_Template.m_ColumnID).GetValueStr()));
}
