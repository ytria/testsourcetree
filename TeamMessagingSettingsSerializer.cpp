#include "TeamMessagingSettingsSerializer.h"

#include "JsonSerializeUtil.h"

TeamMessagingSettingsSerializer::TeamMessagingSettingsSerializer(const TeamMessagingSettings& p_Settings)
    : m_Settings(p_Settings)
{
}

void TeamMessagingSettingsSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeBool(_YTEXT("allowUserEditMessages"), m_Settings.GetAllowUserEditMessages(), obj);
    JsonSerializeUtil::SerializeBool(_YTEXT("allowUserDeleteMessages"), m_Settings.GetAllowUserDeleteMessages(), obj);
    JsonSerializeUtil::SerializeBool(_YTEXT("allowOwnerDeleteMessages"), m_Settings.GetAllowOwnerDeleteMessages(), obj);
    JsonSerializeUtil::SerializeBool(_YTEXT("allowTeamMentions"), m_Settings.GetAllowTeamMentions(), obj);
    JsonSerializeUtil::SerializeBool(_YTEXT("allowChannelMentions"), m_Settings.GetAllowChannelMentions(), obj);
}
