#include "MessagesDeepHierarchyRequester.h"
#include "ChildrenMailFoldersRequester.h"
#include "O365AdminUtil.h"
#include "MailFoldersTopLevelRequester.h"
#include "PaginatorUtil.h"
#include "safeTaskCall.h"
#include "MessageInMailfolderPropertySet.h"

MessagesDeepHierarchyRequester::MessagesDeepHierarchyRequester(const wstring& p_UserId, const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_UserId(p_UserId)
	, m_Logger(p_Logger)
{
}

void MessagesDeepHierarchyRequester::SetMessagesCutOffDateTime(const wstring& p_CutOffDateTime)
{
	m_CutOffDateTime = p_CutOffDateTime;
}

void MessagesDeepHierarchyRequester::SetMessagesCustomFilter(const ODataFilter& p_CustomFilter)
{
	m_CustomFilter = p_CustomFilter;
}

TaskWrapper<void> MessagesDeepHierarchyRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	auto topLevelLogger = m_Logger->Clone();
	topLevelLogger->SetLogMessage(_T("mail folders"));
	auto topLevelFoldersRequester = std::make_shared<MailFoldersTopLevelRequester>(m_UserId, topLevelLogger);

	m_MailFolders = std::make_shared<vector<BusinessMailFolder>>();

	return safeTaskCall(topLevelFoldersRequester->Send(p_Session, p_TaskData).ThenByTask(
		[cutOff = m_CutOffDateTime
		, customFilter = m_CustomFilter
		, userId = m_UserId
		, p_Session
		, topLevelFoldersRequester
		, mailFolders = m_MailFolders
		, p_TaskData
		, logger = m_Logger](pplx::task<void> p_PreviousTask) {
		
		try
		{
			p_PreviousTask.get();
		}
		catch (const RestException& e)
		{
			BusinessMailFolder errObj;
			errObj.SetError(HttpError(e));
			mailFolders->push_back(errObj);
		}
		catch (const std::exception& e)
		{
			BusinessMailFolder errObj;
			errObj.SetError(SapioError(e));
			mailFolders->push_back(errObj);
		}

		for (auto& mailFolder : topLevelFoldersRequester->GetData())
		{
			if (p_TaskData.IsCanceled())
				break;

			try
			{
				// Get all subfolders of this top-level mailfolder and their respective messages
				auto childrenMailFoldersLogger = logger->Clone();
				childrenMailFoldersLogger->SetLogMessage(_T("mail subfolders"));
				childrenMailFoldersLogger->SetContextualInfo(mailFolder.m_DisplayName ? *mailFolder.m_DisplayName : Str::MakeMidEllipsis(mailFolder.GetID()));
				ChildrenMailFoldersRequester childrenRequester(userId, mailFolder.GetID(), true, childrenMailFoldersLogger);
				if (!cutOff.empty())
					childrenRequester.SetMessagesCutOffDateTime(cutOff);
				childrenRequester.SetMessagesCustomFilter(customFilter);

				childrenRequester.Send(p_Session, p_TaskData).GetTask().wait();

				vector<BusinessMailFolder> childrenBusinessMailFolders;
				for (const auto& mailFolder : childrenRequester.GetData())
					childrenBusinessMailFolders.emplace_back(mailFolder);
				mailFolder.m_ChildrenMailFolders = childrenBusinessMailFolders;

				// Get messages in this immediate top-level folder
				auto messageListLogger = logger->Clone();
				messageListLogger->SetLogMessage(_T("messages"));
				messageListLogger->SetContextualInfo(mailFolder.m_DisplayName ? *mailFolder.m_DisplayName : Str::MakeMidEllipsis(mailFolder.GetID()));
				MessageListRequester thisFolderReq(MessageInMailfolderPropertySet(), userId, wstring(mailFolder.m_Id), messageListLogger);
				if (!cutOff.empty())
					thisFolderReq.SetCutOffDateTime(cutOff);
				thisFolderReq.Send(p_Session, p_TaskData).GetTask().wait();

				mailFolder.m_Messages = thisFolderReq.GetData();

				mailFolders->emplace_back(mailFolder);
			}
			catch (RestException& e)
			{
				BusinessMailFolder mailFolderError;
				mailFolderError.SetError(HttpError(e));
				mailFolders->emplace_back(mailFolderError);
			}
			catch (const pplx::task_canceled&)
			{
			}
			catch (const std::exception& e)
			{
				BusinessMailFolder mailFolderError;
				mailFolderError.SetError(SapioError(e));
				mailFolders->emplace_back(mailFolderError);
			}
		}
	}), p_Session->GetMSGraphSession(Sapio365Session::APP_SESSION), p_TaskData);
}

vector<BusinessMailFolder>& MessagesDeepHierarchyRequester::GetData()
{
	return *m_MailFolders;
}
