#include "TargetResourceDeserializer.h"

#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "ModifiedPropertyDeserializer.h"

void TargetResourceDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.m_Type, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("userPrincipalName"), m_Data.m_UserPrincipalName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("groupType"), m_Data.m_GroupType, p_Object);
	{
		ListDeserializer<ModifiedProperty, ModifiedPropertyDeserializer> d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("modifiedProperties"), p_Object))
			m_Data.m_ModifiedProperties = d.GetData();
	}
}
