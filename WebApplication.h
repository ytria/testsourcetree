#pragma once

#include "ImplicitGrantSettings.h"

class WebApplication
{
public:
	boost::YOpt<PooledString> m_HomePageUrl;
	boost::YOpt<ImplicitGrantSettings> m_ImplicitGrantSettings;
	boost::YOpt<PooledString> m_LogoutUrl;
	boost::YOpt<vector<PooledString>> m_RedirectUris;
};

