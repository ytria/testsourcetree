#include "GridUpdaterOptions.h"
#include "GridApplications.h"
#include "GridUpdater.h"
#include "BasicGridSetup.h"
#include "FrameApplications.h"
#include "AutomationWizardApplications.h"

BEGIN_MESSAGE_MAP(GridApplications, ModuleO365Grid<Application>)
END_MESSAGE_MAP()

GridApplications::GridApplications()
	: m_Frame(nullptr),
	m_ColAddInsId(nullptr),
	m_ColAddInsPropertiesKey(nullptr),
	m_ColAddInsPropertiesValue(nullptr),
	m_ColAddInsType(nullptr),
	m_ColApiAcceptMappedClaims(nullptr),
	m_ColApiKnownClientApplications(nullptr),
	m_ColApiOauth2PermissionScopesAdminConsentDescription(nullptr),
	m_ColApiOauth2PermissionScopesAdminConsentDisplayName(nullptr),
	m_ColApiOauth2PermissionScopesId(nullptr),
	m_ColApiOauth2PermissionScopesIsEnabled(nullptr),
	m_ColApiOauth2PermissionScopesOrigin(nullptr),
	m_ColApiOauth2PermissionScopesType(nullptr),
	m_ColApiOauth2PermissionScopesUserConsentDescription(nullptr),
	m_ColApiOauth2PermissionScopesUserConsentDisplayName(nullptr),
	m_ColApiOauth2PermissionScopesValue(nullptr),
	m_ColApiPreAuthorizedApplicationsAppId(nullptr),
	m_ColApiPreAuthorizedApplicationsDelegatedPermissionIds(nullptr),
	m_ColApiRequestedAccessTokenVersion(nullptr),
	m_ColAppId(nullptr),
	m_ColAppRoleAllowedMemberTypes(nullptr),
	m_ColAppRoleDescription(nullptr),
	m_ColAppRoleDisplayName(nullptr),
	m_ColAppRoleId(nullptr),
	m_ColAppRoleIsEnabled(nullptr),
	m_ColAppRoleOrigin(nullptr),
	m_ColAppRoleValue(nullptr),
	m_ColCreatedDateTime(nullptr),
	m_ColDeletedDateTime(nullptr),
	m_ColDisplayName(nullptr),
	m_ColGroupMembershipClaims(nullptr),
	m_ColId(nullptr),
	m_ColIdentifierUris(nullptr),
	m_ColInfoLogoUrl(nullptr),
	m_ColInfoMarketingUrl(nullptr),
	m_ColInfoPrivacyStatementUrl(nullptr),
	m_ColInfoSupportUrl(nullptr),
	m_ColInfoTermsOfServiceUrl(nullptr),
	m_ColIsFallbackPublicClient(nullptr),
	m_ColKeyCredsCustomKeyIdentifier(nullptr),
	m_ColKeyCredsDisplayName(nullptr),
	m_ColKeyCredsEndDateTime(nullptr),
	m_ColKeyCredsKeyId(nullptr),
	m_ColKeyCredsStartDateTime(nullptr),
	m_ColKeyCredsType(nullptr),
	m_ColKeyCredsUsage(nullptr),
	m_ColKeyCredsKey(nullptr),
	m_ColLogo(nullptr),
	m_ColOptClaimsIdTokenAdditionalProperties(nullptr),
	m_ColOptClaimsIdTokenEssential(nullptr),
	m_ColOptClaimsIdTokenName(nullptr),
	m_ColOptClaimsIdTokenSource(nullptr),
	m_ColOptClaimsAccessTokenAdditionalProperties(nullptr),
	m_ColOptClaimsAccessTokenEssential(nullptr),
	m_ColOptClaimsAccessTokenName(nullptr),
	m_ColOptClaimsAccessTokenSource(nullptr),
	m_ColOptClaimsSaml2TokenAdditionalProperties(nullptr),
	m_ColOptClaimsSaml2TokenEssential(nullptr),
	m_ColOptClaimsSaml2TokenName(nullptr),
	m_ColOptClaimsSaml2TokenSource(nullptr),
	m_ColParentalControlSettingsCountriesBlockedForMinors(nullptr),
	m_ColParentalControlSettingsLegalAgeGroupRule(nullptr),
	m_ColPasswordCredentialsCustomKeyIdentifier(nullptr),
	m_ColPasswordCredentialsDisplayName(nullptr),
	m_ColPasswordCredentialsEndDateTime(nullptr),
	m_ColPasswordCredentialsHint(nullptr),
	m_ColPasswordCredentialsKeyId(nullptr),
	m_ColPasswordCredentialsSecretText(nullptr),
	m_ColPasswordCredentialsStartDateTime(nullptr),
	m_ColPublicClientRedirectUris(nullptr),
	m_ColPublisherDomain(nullptr),
	m_ColRequiredResourceAccessResourceAccessId(nullptr),
	m_ColRequiredResourceAccessResourceAccessType(nullptr),
	m_ColRequiredResourceAccessResourceAppId(nullptr),
	m_ColSignInAudience(nullptr),
	m_ColTags(nullptr),
	m_ColTokenEncryptionKeyId(nullptr),
	m_ColWebHomePageUrl(nullptr),
	m_ColWebImplicitGrantSettingsEnableIdTokenIssuance(nullptr),
	m_ColWebImplicitGrantSettingsEnableAccessTokenIssuance(nullptr),
	m_ColWebLogoutUrl(nullptr),
	m_ColWebRedirectUris(nullptr)
{
	UseDefaultActions(0);
	initWizard<AutomationWizardApplications>(_YTEXT("Automation\\Applications"));

}

GridApplications::~GridApplications()
{
	GetBackend().Clear();
}

void GridApplications::BuildView(const vector<Application>& p_Applications, bool p_FullPurge, bool p_NoPurge)
{
	if (p_Applications.size() == 1 && p_Applications[0].GetError() != boost::none && p_Applications[0].GetID().IsEmpty())
	{
		HandlePostUpdateError(p_Applications[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_Applications[0].GetError()->GetFullErrorMessage());
	}
	else
	{
		GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
			| (p_NoPurge ? 0 : (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE))
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			| GridUpdaterOptions::REFRESH_PROCESS_DATA
		);
		GridUpdater updater(*this, options);

		for (const auto& app : p_Applications)
		{
			GridBackendField fID(app.GetID(), GetColId());
			GridBackendRow* row = m_RowIndex.GetRow({ fID }, true, true);

			ASSERT(nullptr != row);
			if (nullptr != row)
			{
				SetRowObjectType(row, app);
				FillApplicationFields(*row, app);
			}
		}
	}
}

void GridApplications::FillApplicationFields(GridBackendRow& p_Row, const Application& p_Application)
{
	if (p_Application.m_AddIns)
	{
		vector<PooledString> addInIds;
		vector<PooledString> addInPropertyKeys;
		vector<PooledString> addInPropertyValues;
		vector<PooledString> addInType;
		for (const auto& addIn : *p_Application.m_AddIns)
		{
			addInIds.push_back(addIn.m_Id ? *addIn.m_Id : GridBackendUtil::g_NoValueString);
			if (addIn.m_Properties)
			{
				for (const auto& property : *addIn.m_Properties)
				{
					addInPropertyKeys.push_back(property.m_Key ? *property.m_Key : GridBackendUtil::g_NoValueString);
					addInPropertyValues.push_back(property.m_Value ? *property.m_Value : GridBackendUtil::g_NoValueString);
				}
			}
			addInIds.push_back(addIn.m_Type ? *addIn.m_Type : GridBackendUtil::g_NoValueString);
		}

	}

	if (p_Application.m_Api)
	{
		const auto& api = *p_Application.m_Api;
		p_Row.AddField(api.m_AcceptMappedClaims, m_ColApiAcceptMappedClaims);
		p_Row.AddField(api.m_KnownClientApplications, m_ColApiKnownClientApplications);
		
		if (api.m_OAuth2PermissionScopes)
		{
			vector<PooledString> adminConsentDescriptions;
			vector<PooledString> adminConsentDisplayNames;
			vector<PooledString> ids;
			vector<BOOLItem> isEnableds;
			vector<PooledString> origins;
			vector<PooledString> types;
			vector<PooledString> userConsentDescs;
			vector<PooledString> userConsentDispNames;
			vector<PooledString> values;
			for (const auto& scope : *api.m_OAuth2PermissionScopes)
			{
				adminConsentDescriptions.push_back(scope.m_AdminConsentDescription ? *scope.m_AdminConsentDescription : GridBackendUtil::g_NoValueString);
				adminConsentDisplayNames.push_back(scope.m_AdminConsentDisplayName ? *scope.m_AdminConsentDisplayName : GridBackendUtil::g_NoValueString);
				ids.push_back(scope.m_Id ? *scope.m_Id : GridBackendUtil::g_NoValueString);
				isEnableds.push_back(scope.m_IsEnabled ? *scope.m_IsEnabled : GridBackendUtil::g_NoValueBoolItem);
				origins.push_back(scope.m_Origin ? *scope.m_Origin : GridBackendUtil::g_NoValueString);
				types.push_back(scope.m_Type ? *scope.m_Type : GridBackendUtil::g_NoValueString);
				userConsentDescs.push_back(scope.m_UserConsentDescription ? *scope.m_UserConsentDescription : GridBackendUtil::g_NoValueString);
				userConsentDispNames.push_back(scope.m_UserConsentDisplayName ? *scope.m_UserConsentDisplayName : GridBackendUtil::g_NoValueString);
				values.push_back(scope.m_Value ? *scope.m_Value : GridBackendUtil::g_NoValueString);
			}
			p_Row.AddField(adminConsentDescriptions, m_ColApiOauth2PermissionScopesAdminConsentDescription);
			p_Row.AddField(adminConsentDisplayNames, m_ColApiOauth2PermissionScopesAdminConsentDisplayName);
			p_Row.AddField(ids, m_ColApiOauth2PermissionScopesId);
			p_Row.AddField(isEnableds, m_ColApiOauth2PermissionScopesIsEnabled);
			p_Row.AddField(origins, m_ColApiOauth2PermissionScopesOrigin);
			p_Row.AddField(types, m_ColApiOauth2PermissionScopesType);
			p_Row.AddField(userConsentDescs, m_ColApiOauth2PermissionScopesUserConsentDescription);
			p_Row.AddField(userConsentDispNames, m_ColApiOauth2PermissionScopesUserConsentDisplayName);
			p_Row.AddField(values, m_ColApiOauth2PermissionScopesValue);
		}

		if (api.m_PreAuthorizedApplications)
		{
			vector<PooledString> appIds;
			vector<PooledString> delegatedPermsIds;
			for (const auto& preAuthApps : *api.m_PreAuthorizedApplications)
			{
				appIds.push_back(preAuthApps.m_AppId ? *preAuthApps.m_AppId : GridBackendUtil::g_NoValueString);
				delegatedPermsIds.push_back(preAuthApps.m_DelegatedPermissionIds ? GridBackendUtil::MakeMultivalueString(*preAuthApps.m_DelegatedPermissionIds) : GridBackendUtil::g_NoValueString);
			}
			p_Row.AddField(appIds, m_ColApiPreAuthorizedApplicationsAppId);
			if (!delegatedPermsIds.empty())
				p_Row.AddField(delegatedPermsIds, m_ColApiPreAuthorizedApplicationsDelegatedPermissionIds).SetIsFromMultivalueNested(true);
			else
				p_Row.RemoveField(m_ColApiPreAuthorizedApplicationsDelegatedPermissionIds);

		}
		p_Row.AddField(api.m_RequestedAccessTokenVersion, m_ColApiRequestedAccessTokenVersion);
	}
	p_Row.AddField(p_Application.m_AppId, m_ColAppId);
	if (p_Application.m_AppRoles)
	{
		vector<PooledString> allowedMemberTypes;
		vector<PooledString> descriptions;
		vector<PooledString> displayNames;
		vector<PooledString> ids;
		vector<BOOLItem> isEnableds;
		vector<PooledString> origins;
		vector<PooledString> values;
		
		for (const auto& role : *p_Application.m_AppRoles)
		{
			allowedMemberTypes.push_back(role.m_AllowedMemberTypes ? GridBackendUtil::MakeMultivalueString(*role.m_AllowedMemberTypes) : GridBackendUtil::g_NoValueString);
			descriptions.push_back(role.m_Description ? *role.m_Description : GridBackendUtil::g_NoValueString);
			displayNames.push_back(role.m_DisplayName ? *role.m_DisplayName : GridBackendUtil::g_NoValueString);
			ids.push_back(role.m_Id ? *role.m_Id : GridBackendUtil::g_NoValueString);
			isEnableds.push_back(role.m_IsEnabled ? *role.m_IsEnabled : GridBackendUtil::g_NoValueBoolItem);
			origins.push_back(role.m_Origin ? *role.m_Origin : GridBackendUtil::g_NoValueString);
			values.push_back(role.m_Value ? *role.m_Value : GridBackendUtil::g_NoValueString);
		}
		if (!allowedMemberTypes.empty())
			p_Row.AddField(allowedMemberTypes, m_ColAppRoleAllowedMemberTypes).SetIsFromMultivalueNested(true);
		else
			p_Row.RemoveField(m_ColAppRoleAllowedMemberTypes);
		p_Row.AddField(descriptions, m_ColAppRoleDescription);
		p_Row.AddField(displayNames, m_ColAppRoleDisplayName);
		p_Row.AddField(ids, m_ColAppRoleId);
		p_Row.AddField(isEnableds, m_ColAppRoleIsEnabled);
		p_Row.AddField(origins, m_ColAppRoleOrigin);
		p_Row.AddField(values, m_ColAppRoleValue);

	}
	p_Row.AddField(p_Application.m_CreatedDateTime, m_ColCreatedDateTime);
	p_Row.AddField(p_Application.m_DeletedDateTime, m_ColDeletedDateTime);
	p_Row.AddField(p_Application.m_DisplayName, m_ColDisplayName);
	p_Row.AddField(p_Application.m_GroupMembershipClaims, m_ColGroupMembershipClaims);
	p_Row.AddField(p_Application.m_IdentifierUris, m_ColIdentifierUris);
	if (p_Application.m_Info)
	{
		const auto& info = *p_Application.m_Info;
		p_Row.AddField(info.m_LogoUrl, m_ColInfoLogoUrl);
		p_Row.AddField(info.m_MarketingUrl, m_ColInfoMarketingUrl);
		p_Row.AddField(info.m_PrivacyStatementUrl, m_ColInfoPrivacyStatementUrl);
		p_Row.AddField(info.m_SupportUrl, m_ColInfoSupportUrl);
		p_Row.AddField(info.m_TermsOfServiceUrl, m_ColInfoTermsOfServiceUrl);
	}

	p_Row.AddField(p_Application.m_IsFallbackPublicClient, m_ColIsFallbackPublicClient);
	if (p_Application.m_KeyCredentials)
	{
		vector<PooledString> customKeyIds;
		vector<PooledString> displayNames;
		vector<YTimeDate> endDateTimes;
		vector<PooledString> keyIds;
		vector<YTimeDate> startDateTimes;
		vector<PooledString> types;
		vector<PooledString> usages;
		vector<PooledString> keys;
		for (const auto& keyCred : *p_Application.m_KeyCredentials)
		{
			customKeyIds.push_back(keyCred.m_CustomKeyIdentifier ? *keyCred.m_CustomKeyIdentifier : GridBackendUtil::g_NoValueString);
			displayNames.push_back(keyCred.m_DisplayName ? *keyCred.m_DisplayName : GridBackendUtil::g_NoValueString);
			endDateTimes.push_back(keyCred.m_EndDateTime ? *keyCred.m_EndDateTime : GridBackendUtil::g_NoValueDate);
			keyIds.push_back(keyCred.m_KeyId ? *keyCred.m_KeyId : GridBackendUtil::g_NoValueString);
			startDateTimes.push_back(keyCred.m_StartDateTime ? *keyCred.m_StartDateTime : GridBackendUtil::g_NoValueDate);
			types.push_back(keyCred.m_Type ? *keyCred.m_Type : GridBackendUtil::g_NoValueString);
			usages.push_back(keyCred.m_Usage ? *keyCred.m_Usage : GridBackendUtil::g_NoValueString);
			keys.push_back(keyCred.m_Key ? *keyCred.m_Key : GridBackendUtil::g_NoValueString);
		}
		p_Row.AddField(customKeyIds, m_ColKeyCredsCustomKeyIdentifier);
		p_Row.AddField(displayNames, m_ColKeyCredsDisplayName);
		p_Row.AddField(endDateTimes, m_ColKeyCredsEndDateTime);
		p_Row.AddField(keyIds, m_ColKeyCredsEndDateTime);
		p_Row.AddField(startDateTimes, m_ColKeyCredsStartDateTime);
		p_Row.AddField(types, m_ColKeyCredsType);
		p_Row.AddField(usages, m_ColKeyCredsUsage);
		p_Row.AddField(keys, m_ColKeyCredsKey);

	}
	p_Row.AddField(p_Application.m_Logo, m_ColLogo);
	if (p_Application.m_OptionalClaims)
	{
		const auto& optClaims = *p_Application.m_OptionalClaims;
		if (optClaims.m_IdToken)
		{
			vector<PooledString> additionalProps;
			vector<BOOLItem> essentials;
			vector<PooledString> names;
			vector<PooledString> sources;
			for (const auto& claim : *optClaims.m_IdToken)
			{
				additionalProps.push_back(claim.m_AdditionalProperties ? GridBackendUtil::MakeMultivalueString(*claim.m_AdditionalProperties) : GridBackendUtil::g_NoValueString);
				essentials.push_back(claim.m_Essential ? *claim.m_Essential : GridBackendUtil::g_NoValueBoolItem);
				names.push_back(claim.m_Name ? *claim.m_Name : GridBackendUtil::g_NoValueString);
				sources.push_back(claim.m_Source ? *claim.m_Source : GridBackendUtil::g_NoValueString);
			}
			if (!additionalProps.empty())
				p_Row.AddField(additionalProps, m_ColOptClaimsIdTokenAdditionalProperties);
			else
				p_Row.RemoveField(m_ColOptClaimsIdTokenAdditionalProperties);
			p_Row.AddField(essentials, m_ColOptClaimsIdTokenEssential);
			p_Row.AddField(names, m_ColOptClaimsIdTokenName);
			p_Row.AddField(sources, m_ColOptClaimsIdTokenSource);
		}
		if (optClaims.m_AccessToken)
		{
			vector<PooledString> additionalProps;
			vector<BOOLItem> essentials;
			vector<PooledString> names;
			vector<PooledString> sources;
			for (const auto& claim : *optClaims.m_AccessToken)
			{
				additionalProps.push_back(claim.m_AdditionalProperties ? GridBackendUtil::MakeMultivalueString(*claim.m_AdditionalProperties) : GridBackendUtil::g_NoValueString);
				essentials.push_back(claim.m_Essential ? *claim.m_Essential : GridBackendUtil::g_NoValueBoolItem);
				names.push_back(claim.m_Name ? *claim.m_Name : GridBackendUtil::g_NoValueString);
				sources.push_back(claim.m_Source ? *claim.m_Source : GridBackendUtil::g_NoValueString);
			}
			if (!additionalProps.empty())
				p_Row.AddField(additionalProps, m_ColOptClaimsAccessTokenAdditionalProperties);
			else
				p_Row.RemoveField(m_ColOptClaimsAccessTokenAdditionalProperties);
			p_Row.AddField(essentials, m_ColOptClaimsIdTokenEssential);
			p_Row.AddField(names, m_ColOptClaimsIdTokenName);
			p_Row.AddField(sources, m_ColOptClaimsIdTokenSource);
		}
		if (optClaims.m_Saml2Token)
		{
			vector<PooledString> additionalProps;
			vector<BOOLItem> essentials;
			vector<PooledString> names;
			vector<PooledString> sources;
			for (const auto& claim : *optClaims.m_Saml2Token)
			{
				additionalProps.push_back(claim.m_AdditionalProperties ? GridBackendUtil::MakeMultivalueString(*claim.m_AdditionalProperties) : GridBackendUtil::g_NoValueString);
				essentials.push_back(claim.m_Essential ? *claim.m_Essential : GridBackendUtil::g_NoValueBoolItem);
				names.push_back(claim.m_Name ? *claim.m_Name : GridBackendUtil::g_NoValueString);
				sources.push_back(claim.m_Source ? *claim.m_Source : GridBackendUtil::g_NoValueString);
			}
			if (!additionalProps.empty())
				p_Row.AddField(additionalProps, m_ColOptClaimsSaml2TokenAdditionalProperties);
			else
				p_Row.RemoveField(m_ColOptClaimsSaml2TokenAdditionalProperties);
			p_Row.AddField(essentials, m_ColOptClaimsIdTokenEssential);
			p_Row.AddField(names, m_ColOptClaimsIdTokenName);
			p_Row.AddField(sources, m_ColOptClaimsIdTokenSource);
		}
	}
	if (p_Application.m_ParentalControlSettings)
	{
		const auto& parentalSettings = *p_Application.m_ParentalControlSettings;
		p_Row.AddField(parentalSettings.m_CountriesBlockedForMinors, m_ColParentalControlSettingsCountriesBlockedForMinors);
		p_Row.AddField(parentalSettings.m_LegalAgeGroupRule, m_ColParentalControlSettingsLegalAgeGroupRule);
	}
	if (p_Application.m_PasswordCredentials)
	{
		vector<PooledString> customKeyIds;
		vector<PooledString> dispNames;
		vector<YTimeDate> endDateTimes;
		vector<PooledString> hints;
		vector<PooledString> keyIds;
		vector<PooledString> secretTexts;
		vector<YTimeDate> startDateTimes;
		for (const auto& pwdCred : *p_Application.m_PasswordCredentials)
		{
			customKeyIds.push_back(pwdCred.m_CustomKeyIdentifier ? *pwdCred.m_CustomKeyIdentifier : GridBackendUtil::g_NoValueString);
			dispNames.push_back(pwdCred.m_DisplayName ? *pwdCred.m_DisplayName : GridBackendUtil::g_NoValueString);
			endDateTimes.push_back(pwdCred.m_EndDateTime ? *pwdCred.m_EndDateTime : GridBackendUtil::g_NoValueDate);
			hints.push_back(pwdCred.m_Hint ? *pwdCred.m_Hint : GridBackendUtil::g_NoValueString);
			keyIds.push_back(pwdCred.m_KeyId ? *pwdCred.m_KeyId : GridBackendUtil::g_NoValueString);
			secretTexts.push_back(pwdCred.m_SecretText ? *pwdCred.m_SecretText : GridBackendUtil::g_NoValueString);
			startDateTimes.push_back(pwdCred.m_StartDateTime ? *pwdCred.m_StartDateTime : GridBackendUtil::g_NoValueDate);
		}
	}
	if (p_Application.m_PublicClient)
		p_Row.AddField(p_Application.m_PublicClient->m_RedirectUris, m_ColPublicClientRedirectUris);

	p_Row.AddField(p_Application.m_PublisherDomain, m_ColPublisherDomain);
	if (p_Application.m_RequiredResourceAccess)
	{
		vector<PooledString> resAppIds;
		const auto& reqResAccess = *p_Application.m_RequiredResourceAccess;
		for (const auto& reqResAccess : *p_Application.m_RequiredResourceAccess)
		{
			if (reqResAccess.m_ResourceAccess)
			{
				vector<PooledString> ids;
				vector<PooledString> types;
				for (const auto& resAccess : *reqResAccess.m_ResourceAccess)
				{
					ids.push_back(resAccess.m_Id ? *resAccess.m_Id : GridBackendUtil::g_NoValueString);
					ids.push_back(resAccess.m_Type ? *resAccess.m_Type : GridBackendUtil::g_NoValueString);
				}
				p_Row.AddField(ids, m_ColRequiredResourceAccessResourceAccessId);
				p_Row.AddField(types, m_ColRequiredResourceAccessResourceAccessType);
			}
			resAppIds.push_back(reqResAccess.m_ResourceAppId ? *reqResAccess.m_ResourceAppId : GridBackendUtil::g_NoValueString);
		}
		p_Row.AddField(resAppIds, m_ColRequiredResourceAccessResourceAppId);

	}
	p_Row.AddField(p_Application.m_SignInAudience, m_ColSignInAudience);
	p_Row.AddField(p_Application.m_Tags, m_ColTags);
	p_Row.AddField(p_Application.m_TokenEncryptionKeyId, m_ColTokenEncryptionKeyId);
	if (p_Application.m_WebApplication)
	{
		const auto& web = *p_Application.m_WebApplication;
		p_Row.AddField(web.m_HomePageUrl, m_ColWebHomePageUrl);

		if (web.m_ImplicitGrantSettings)
		{
			const auto& grantSettings = *web.m_ImplicitGrantSettings;
			p_Row.AddField(grantSettings.m_EnableIdTokenIssuance, m_ColWebImplicitGrantSettingsEnableIdTokenIssuance);
			p_Row.AddField(grantSettings.m_EnableAccessTokenIssuance, m_ColWebImplicitGrantSettingsEnableAccessTokenIssuance);
		}

		p_Row.AddField(web.m_LogoutUrl, m_ColWebLogoutUrl);
		p_Row.AddField(web.m_RedirectUris, m_ColWebRedirectUris);
	}

}

wstring GridApplications::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, m_ColDisplayName);
}

void GridApplications::customizeGrid()
{
	m_Frame = dynamic_cast<FrameApplications*>(GetParentFrame());

	{
		BasicGridSetup setup(GridUtil::g_AutoNameApplications, LicenseUtil::g_codeSapio365applications/*,
			{
				{ &m_ColMetaDisplayName, g_TitleOwnerDisplayName, g_FamilyOwner },
				{ &m_ColMetaUserPrincipalName, g_TitleOwnerUserName, g_FamilyOwner },
				{ &m_ColMetaId, g_TitleOwnerID, g_FamilyOwner }
			}*/);
		setup.Setup(*this, false);
	}

	addColumnGraphID();
	m_ColDisplayName = AddColumn(_YUID("displayName"), _T("Display name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });

	static const wstring g_FamilyAddIn = _T("AddIns");
	m_ColAddInsId = AddColumn(_YUID("addInsId"), _T("AddIns - Id"), g_FamilyAddIn, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAddInsPropertiesKey = AddColumn(_YUID("addInsPropertiesKey"), _T("AddIns - Properties - Key"), g_FamilyAddIn, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAddInsPropertiesValue = AddColumn(_YUID("addInsPropertiesValue"), _T("AddIns - Properties - Value"), g_FamilyAddIn, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAddInsType = AddColumn(_YUID("addInsType"), _T("AddIns - Type"), g_FamilyAddIn, g_ColumnSize22, { g_ColumnsPresetDefault });

	static const wstring g_FamilyApi = _T("Api");
	m_ColApiAcceptMappedClaims = AddColumnCheckBox(_YUID("apiAcceptMappedClaims"), _T("Api - Accept mapped claims"), g_FamilyApi, g_ColumnSize2, { g_ColumnsPresetDefault });
	m_ColApiKnownClientApplications = AddColumn(_YUID("apiKnownClientApplications"), _T("Api - Known client applications"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColApiOauth2PermissionScopesAdminConsentDescription = AddColumn(_YUID("apiOAuth2PermissionScopesAdminConsentDescription"), _T("Api - OAuth2 permission scopes - Admin consent description"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColApiOauth2PermissionScopesAdminConsentDisplayName = AddColumn(_YUID("apiOAuth2PermissionScopesAdminConsentName"), _T("Api - OAuth2 permission scopes - Admin consent name"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColApiOauth2PermissionScopesId = AddColumn(_YUID("apiOAuth2PermissionScopesId"), _T("Api - OAuth2 permission scopes - Id"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColApiOauth2PermissionScopesIsEnabled = AddColumnCheckBox(_YUID("apiOAuth2PermissionScopesIsEnabled"), _T("Api - OAuth2 permission scopes - Is enabled"), g_FamilyApi, g_ColumnSize2, { g_ColumnsPresetDefault });
	m_ColApiOauth2PermissionScopesOrigin = AddColumn(_YUID("apiOAuth2PermissionScopesOrigin"), _T("Api - OAuth2 permission scopes - Origin"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColApiOauth2PermissionScopesType = AddColumn(_YUID("apiOAuth2PermissionScopesType"), _T("Api - OAuth2 permission scopes - Type"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColApiOauth2PermissionScopesUserConsentDescription = AddColumn(_YUID("apiOAuth2PermissionScopesUserConsentDescription"), _T("Api - OAuth2 permission scopes - User consent description"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColApiOauth2PermissionScopesUserConsentDisplayName = AddColumn(_YUID("apiOAuth2PermissionScopesDisplayName"), _T("Api - OAuth2 permission scopes - Display name"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColApiOauth2PermissionScopesValue = AddColumn(_YUID("apiOAuth2PermissionScopesValue"), _T("Api - OAuth2 permission scopes - Value"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColApiPreAuthorizedApplicationsAppId = AddColumn(_YUID("apiPreAuthorizedApplicationsAppId"), _T("Api - Pre authorized applications - App id"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColApiPreAuthorizedApplicationsDelegatedPermissionIds = AddColumn(_YUID("apiPreAuthorizedApplicationsDelegatedPermissionIds"), _T("Api - Pre authorized applications - Delegated permission ids"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColApiRequestedAccessTokenVersion = AddColumnNumber(_YUID("apiRequestAccessTokenVersion"), _T("Api - Request access token version"), g_FamilyApi, g_ColumnSize22, { g_ColumnsPresetDefault });

	m_ColAppId = AddColumn(_YUID("appId"), _T("App id"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	
	static const wstring g_FamilyAppRoles = _T("App roles");
	m_ColAppRoleAllowedMemberTypes = AddColumn(_YUID("appRoleAllowedMemberTypes"), _T("App role - Allowed member types"), g_FamilyAppRoles, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAppRoleDescription = AddColumn(_YUID("appRoleDescription"), _T("App role - Description"), g_FamilyAppRoles, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAppRoleDisplayName = AddColumn(_YUID("appRoleDisplayName"), _T("App role - Display name"), g_FamilyAppRoles, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAppRoleId = AddColumn(_YUID("appRoleId"), _T("App role - Id"), g_FamilyAppRoles, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAppRoleIsEnabled = AddColumnCheckBox(_YUID("appRoleIsEnabled"), _T("App role - Is enabled"), g_FamilyAppRoles, g_ColumnSize2, { g_ColumnsPresetDefault });
	m_ColAppRoleOrigin = AddColumn(_YUID("appRoleOrigin"), _T("App role - Origin"), g_FamilyAppRoles, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAppRoleValue = AddColumn(_YUID("appRoleValue"), _T("App role - Value"), g_FamilyAppRoles, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAppRoleAllowedMemberTypes->SetMultivalueFamily(_T("App roles"));
	m_ColAppRoleAllowedMemberTypes->AddMultiValueExplosionSister(m_ColAppRoleDescription);
	m_ColAppRoleAllowedMemberTypes->AddMultiValueExplosionSister(m_ColAppRoleDisplayName);
	m_ColAppRoleAllowedMemberTypes->AddMultiValueExplosionSister(m_ColAppRoleId);
	m_ColAppRoleAllowedMemberTypes->AddMultiValueExplosionSister(m_ColAppRoleIsEnabled);
	m_ColAppRoleAllowedMemberTypes->AddMultiValueExplosionSister(m_ColAppRoleOrigin);
	m_ColAppRoleAllowedMemberTypes->AddMultiValueExplosionSister(m_ColAppRoleValue);

	m_ColCreatedDateTime = AddColumnDate(_YUID("createdDateTime"), _T("Created date time"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColDeletedDateTime = AddColumnDate(_YUID("deletedDateTime"), _T("Deleted date time"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColGroupMembershipClaims = AddColumn(_YUID("groupMembershipClaims"), _T("Group membership claims"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColIdentifierUris = AddColumn(_YUID("identifierUris"), _T("Identifier uris"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	
	static const wstring g_FamilyInformationUrl = _T("Informational Url");
	m_ColInfoLogoUrl = AddColumn(_YUID("infoLogoUrl"), _T("Info - Logo url"), g_FamilyInformationUrl, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColInfoMarketingUrl = AddColumnHyperlink(_YUID("infoMarketingUrl"), _T("Info - Marketing url"), g_FamilyInformationUrl, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColInfoPrivacyStatementUrl = AddColumnHyperlink(_YUID("infoPrivacyStatementUrl"), _T("Info - Privacy statement url"), g_FamilyInformationUrl, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColInfoSupportUrl = AddColumnHyperlink(_YUID("infoSupportUrl"), _T("Info - Support url"), g_FamilyInformationUrl, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColInfoTermsOfServiceUrl = AddColumnHyperlink(_YUID("infoTermsOfServiceUrl"), _T("Info - Terms of service url"), g_FamilyInformationUrl, g_ColumnSize22, { g_ColumnsPresetDefault });

	m_ColIsFallbackPublicClient = AddColumnCheckBox(_YUID("isFallbackPublicClient"), _T("Is fallback public client"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });

	static const wstring g_FamilyKeyCredentials = _T("Key credentials");
	m_ColKeyCredsCustomKeyIdentifier = AddColumn(_YUID("keyCredsCustomKeyIdentifier"), _T("Key credentials - Custom key identifier"), g_FamilyKeyCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColKeyCredsDisplayName = AddColumn(_YUID("keyCredsDisplayName"), _T("Key credentials - Display name"), g_FamilyKeyCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColKeyCredsEndDateTime = AddColumnDate(_YUID("keyCredsEndDateTime"), _T("Key credentials - End date time"), g_FamilyKeyCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColKeyCredsKeyId = AddColumn(_YUID("keyCredsKeyId"), _T("Key credentials - Key id"), g_FamilyKeyCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColKeyCredsStartDateTime = AddColumnDate(_YUID("keyCredsStartDateTime"), _T("Key credentials - Start date time"), g_FamilyKeyCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColKeyCredsType = AddColumn(_YUID("keyCredsType"), _T("Key credentials - Type"), g_FamilyKeyCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColKeyCredsUsage = AddColumn(_YUID("keyCredsUsage"), _T("Key credentials - Usage"), g_FamilyKeyCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColKeyCredsKey = AddColumn(_YUID("keyCredsKey"), _T("Key credentials - Key"), g_FamilyKeyCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColKeyCredsCustomKeyIdentifier->SetMultivalueFamily(_T("Key credentials"));
	m_ColKeyCredsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColKeyCredsDisplayName);
	m_ColKeyCredsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColKeyCredsEndDateTime);
	m_ColKeyCredsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColKeyCredsKeyId);
	m_ColKeyCredsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColKeyCredsStartDateTime);
	m_ColKeyCredsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColKeyCredsType);
	m_ColKeyCredsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColKeyCredsUsage);
	m_ColKeyCredsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColKeyCredsKey);

	m_ColLogo = AddColumn(_YUID("logo"), _T("Logo"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });

	static const wstring g_FamilyOptionalClaims = _T("Optional Claims");
	m_ColOptClaimsIdTokenAdditionalProperties = AddColumn(_YUID("optClaimsIdTokenAdditionalProperties"), _T("Optional claims"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOptClaimsIdTokenEssential = AddColumn(_YUID("optClaimsIdTokenEssential"), _T("Optional claims - Id token essential"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOptClaimsIdTokenName = AddColumn(_YUID("optClaimsIdTokenName"), _T("Optional claims - Id token name"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOptClaimsIdTokenSource = AddColumn(_YUID("optClaimsIdTokenSource"), _T("Optional claims - Id token source"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOptClaimsAccessTokenAdditionalProperties = AddColumn(_YUID("optClaimsAccessTokenAdditionalProperties"), _T("Optional claims - Access token - Additional properties"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOptClaimsAccessTokenEssential = AddColumn(_YUID("optClaimsAccessTokenEssential"), _T("Optional claims - Access token - Essential"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOptClaimsAccessTokenName = AddColumn(_YUID("optClaimsAccessTokenName"), _T("Optional claims - Access token - Name"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOptClaimsAccessTokenSource = AddColumn(_YUID("optClaimsAccessTokenSource"), _T("Optional claims - Access token - Source"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOptClaimsSaml2TokenAdditionalProperties = AddColumn(_YUID("optClaimsSaml2TokenAdditionalProperties"), _T("Optional claims - Saml2 token - Additional properties"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOptClaimsSaml2TokenEssential = AddColumn(_YUID("optClaimsSaml2TokenEssential"), _T("Optional claims - Saml2 token - Essential"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOptClaimsSaml2TokenName = AddColumn(_YUID("optClaimsSaml2TokenName"), _T("Optional claims - Saml2 token - Token name"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColOptClaimsSaml2TokenSource = AddColumn(_YUID("optClaimsSaml2TokenSource"), _T("Optional claims - Saml2 token - Token source"), g_FamilyOptionalClaims, g_ColumnSize22, { g_ColumnsPresetDefault });

	static const wstring g_FamilyParentalControlSettings = _T("Parental control settings");
	m_ColParentalControlSettingsCountriesBlockedForMinors = AddColumn(_YUID("parentalControlSettingsCountriesBlockedForMinors"), _T("Parental control settings - Countries blocked for minors"), g_FamilyParentalControlSettings, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColParentalControlSettingsLegalAgeGroupRule = AddColumn(_YUID("parentalControlSettingsLegalAgeGroupRule"), _T("Parental control settings - Legal age group rule"), g_FamilyParentalControlSettings, g_ColumnSize22, { g_ColumnsPresetDefault });

	static const wstring g_FamilyPasswordCredentials = _T("Password credentials");
	m_ColPasswordCredentialsCustomKeyIdentifier = AddColumn(_YUID("passwordCredentialsCustomKeyIdentifier"), _T("Password credentials - Custom key identifier"), g_FamilyPasswordCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColPasswordCredentialsDisplayName = AddColumn(_YUID("passwordCredentialsDisplayName"), _T("Password credentials - Display Name"), g_FamilyPasswordCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColPasswordCredentialsEndDateTime = AddColumnDate(_YUID("passwordCredentialsEndDateTime"), _T("Password credentials - End date time"), g_FamilyPasswordCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColPasswordCredentialsHint = AddColumn(_YUID("passwordCredentialsHint"), _T("Password credentials - Hint"), g_FamilyPasswordCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColPasswordCredentialsKeyId = AddColumn(_YUID("passwordCredentialsKeyId"), _T("Password credentials - Key id"), g_FamilyPasswordCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColPasswordCredentialsSecretText = AddColumn(_YUID("passwordCredentialsSecretText"), _T("Password credentials - Secret text"), g_FamilyPasswordCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColPasswordCredentialsStartDateTime = AddColumnDate(_YUID("passwordCredentialsStartDateTime"), _T("Password credentials - Start date time"), g_FamilyPasswordCredentials, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColPasswordCredentialsCustomKeyIdentifier->SetMultivalueFamily(_T("Key credentials"));
	m_ColPasswordCredentialsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColPasswordCredentialsDisplayName);
	m_ColPasswordCredentialsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColPasswordCredentialsEndDateTime);
	m_ColPasswordCredentialsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColPasswordCredentialsHint);
	m_ColPasswordCredentialsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColPasswordCredentialsKeyId);
	m_ColPasswordCredentialsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColPasswordCredentialsSecretText);
	m_ColPasswordCredentialsCustomKeyIdentifier->AddMultiValueExplosionSister(m_ColPasswordCredentialsStartDateTime);

	static const wstring g_FamilyPublicClient = _T("Password credentials");
	m_ColPublicClientRedirectUris = AddColumn(_YUID("publicClientRedirectUris"), _T("Public client - Redirect uris"), g_FamilyPublicClient, g_ColumnSize22, { g_ColumnsPresetDefault });

	m_ColPublisherDomain = AddColumn(_YUID("publisherDomain"), _T("Publisher domain"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });

	static const wstring g_FamilyRequiredResourceAccess = _T("Required resource access");
	m_ColRequiredResourceAccessResourceAccessId = AddColumn(_YUID("requiredResourceAccessResourceAccessId"), _T("Required resource access - Resource access - Id"), g_FamilyRequiredResourceAccess, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColRequiredResourceAccessResourceAccessType = AddColumn(_YUID("requiredResourceAccessResourceAccessType"), _T("Required resource access - Resource access - Type"), g_FamilyRequiredResourceAccess, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColRequiredResourceAccessResourceAppId = AddColumn(_YUID("requiredResourceAccessResourceAppId"), _T("Required resource access - Resource app id"), g_FamilyRequiredResourceAccess, g_ColumnSize22, { g_ColumnsPresetDefault });

	m_ColSignInAudience = AddColumn(_YUID("signInAudience"), _T("Sign in audience"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColTags = AddColumn(_YUID("tags"), _T("Tags"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColTokenEncryptionKeyId = AddColumn(_YUID("tokenEncryptionKeyId"), _T("Token encryption key id"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });

	static const wstring g_FamilyWeb = _T("Web");
	m_ColWebHomePageUrl = AddColumn(_YUID("webHomePageUrl"), _T("Web - Homepage url"), g_FamilyWeb, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColWebImplicitGrantSettingsEnableIdTokenIssuance = AddColumn(_YUID("webImplicitGrantSettingsEnableIdTokenIssuance"), _T("Web - Implicit grant settings - Enable id token issuance"), g_FamilyWeb, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColWebImplicitGrantSettingsEnableAccessTokenIssuance = AddColumn(_YUID("webImplicitGrantSettingsEnableAccessTokenIssuance"), _T("Web - Implicit grant settings - Enable access token issuance"), g_FamilyWeb, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColWebLogoutUrl = AddColumn(_YUID("webLogoutUrl"), _T("Web - Logout url"), g_FamilyWeb, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColWebRedirectUris = AddColumn(_YUID("webRedirectUris"), _T("Web - Redirect uris"), g_FamilyWeb, g_ColumnSize22, { g_ColumnsPresetDefault });

	AddColumnForRowPK(GetColId());

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameApplications*>(GetParentFrame()));
}

Application GridApplications::getBusinessObject(GridBackendRow*) const
{
	return Application();
}

void GridApplications::UpdateBusinessObjects(const vector<Application>& p_Applications, bool p_SetModifiedStatus)
{

}

void GridApplications::RemoveBusinessObjects(const vector<Application>& p_Applications)
{

}

void GridApplications::InitializeCommands()
{
	ModuleO365Grid<Application>::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);
}

void GridApplications::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ModuleO365Grid<Application>::OnCustomPopupMenu(pPopup, nStart);

	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		
	}
}
