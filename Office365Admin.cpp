#include "Office365Admin.h"

#include "AutomationNames.h"
#include "AzureADOAuth2BrowserAuthenticator.h"
#include "BackstageLogger.h"
#include "BusinessObjectManager.h"
#include "CodeJockFrameBase.h"
#include "Command.h"
#include "CommandDispatcher.h"
#include "CompositeLogger.h"
#include "ConsentGiver.h"
#include "DlgAddItemsToGroups.h"
#include "DlgAutomationInputHTML.h"
#include "DlgBrowserOpener.h"
#include "DlgDownloadAttachments.h"
#include "DlgExportPref.h"
#include "DlgJobSelectionValidation.h"
#include "DlgModuleOptions.h"
#include "DlgSelectO365Report.h"
#include "DlgSpecialExtAdminAccess.h"
#include "DlgUnderConstruction.h"
#include "FileLogger.h"
#include "FileUtil.h"
#include "GraphCache.h"
#include "GridFrameBase.h"
#include "FrameStatusBarLogger.h"
#include "FrameUsers.h"
#include "LinkHandlerAuditLogs.h"
#include "LinkHandlerContacts.h"
#include "LinkHandlerDirectoryRoles.h"
#include "LinkHandlerDrive.h"
#include "LinkHandlerEvents.h"
#include "LinkHandlerGroups.h"
#include "LinkHandlerLicenses.h"
#include "LinkHandlerMessages.h"
#include "LinkHandlerMessageRules.h"
#include "LinkHandlerO365Reports.h"
#include "LinkHandlerSchools.h"
#include "LinkHandlerSignIns.h"
#include "LinkHandlerSites.h"
#include "LinkHandlerUsers.h"
#include "LoggerService.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "MultipleAppInstanceGuard.h"
#include "MSGraphCommonData.h"
#include "MSGraphConsentListener.h"
#include "MSGraphSession.h"
#include "NalpeironLicenseManager.h"
#include "NetworkUtils.h"
#include "O365AdminErrorHandler.h"
#include "O365LicenseValidator.h"
#include "OSUtil.h"
#include "PersistentCacheUtil.h"
#include "ProductBasicNoLicense.h" // #include "ProductBasic.h"
#include "ProductUpdate.h"
#include "QueryRoleIdByName.h"
#include "QuerySessionByName.h"
#include "RegexSFDBasicImpl.h"
#include "RecentOpeningManagerBasic.h"
#include "Sapio365Session.h"
#include "Sapio365Settings.h"
#include "SessionTypes.h"
#include "SkuAndPlanReference.h"
#include "SugarCRMSession.h"
#include "SystemUtil.h"
#include "TaskDataManager.h"
#include "VersionNo.h"
#include "VersionProduct.h"
#include "VsConsoleLogger.h"
#include "YCallbackMessage.h"

#include <memory>
#include "MigrationRunner.h"
#include "SessionsSqlEngine.h"
#include "UISessionListSorter.h"
#include "PersistentSession.h"
#include "DbSessionPersister.h"
#include "OAuth2AuthenticatorBase.h"
#include "SqlQuerySelect.h"
#include "ExistsRowHandler.h"
#include "QuerySessionExists.h"
#include "QueryRoleExists.h"
#include "QueryCreateRole.h"
#include "QueryLoadRole.h"
#include "DlgSpecialExtAdminUltraAdminJsonGenerator.h"
#include "SessionMigrationsProvisioner.h"
#include "MigrationRegistrations.h"
#include "ApplicationListRequester.h"
#include "Application.h"
#include "BasicPageRequestLogger.h"
#include "RemoveApplicationPasswordRequester.h"
#include "AddApplicationPasswordRequester.h"
#include "DlgLoading.h"
#include "SessionIdsEmitter.h"
#include "MsGraphHttpRequestLogger.h"
#include "UpdateApplicationRequester.h"
#include "QueryDeleteSession.h"
#include "QueryRemoveUnusedCredentials.h"
#include "DlgSpecialExtAdminManageElevatedJsonGenerator.h"
#include "SessionLoaderFactory.h"
#include "SessionSwitcher.h"
#include "AppGraphSessionCreator.h"
#include "CreateMessageRequester.h"
#include "UploadMessageAttachmentRequester.h"
#include "SendMessageRequester.h"

using namespace std::literals::string_literals;


static NumberUtil g_NumberUtilbasic;
NumberUtil* g_NumberUtilInstance = &g_NumberUtilbasic;
SystemUtil* g_SystemUtilInstance = nullptr;
static TimeUtil g_TimeUtilbasic;
TimeUtil* g_TimeUtilInstance = &g_TimeUtilbasic;

extern const wchar_t*	g_ProfUisVersionSetting = _YTEXT("GUI.0.1");
extern const int		g_FileVer[4] = { FILEVER };
extern const wchar_t*	g_FileVerString = _YTEXT(STRFILEVER);
extern const wstring	g_UpdateProductCodeName = SAPIO365_PRODUCT_NAME;
extern const bool		g_DlgExportPrefShowAdvancedBoolStuff = false;
extern const bool		g_TimeFormatHasAllWithCustomSeparator = false;

#define CL_OPT_SESSION	"s"
#define CL_OPT_SESSION_UNICODE	_YTEXT(CL_OPT_SESSION)

#define CL_OPT_ROLE	"r"
#define CL_OPT_ROLE_UNICODE _YTEXT(CL_OPT_ROLE)

#define CL_OPT_SESSIONTYPE	"t"
#define CL_OPT_SESSIONTYPE_UNICODE _YTEXT(CL_OPT_SESSIONTYPE)

wstring Office365AdminApp::g_LastLoadingError;

Office365AdminApp& Office365AdminApp::Get()
{
	return theApp;
}


BEGIN_MESSAGE_MAP(Office365AdminApp, AutomatedApp)
END_MESSAGE_MAP()

Office365AdminApp::Office365AdminApp() 
	: AutomatedApp(SAPIO365_PRODUCT_NAME, MultipleAppInstanceGuard::ASKUSER)
	, m_ShowGridErrors(true)
	, m_SessionBeingCreated(false)
	, m_PendingAutomationForPostInit(false)
{
	m_isAutomatedBeforeRun = false;
	setApplicationMayBeOffline(false);
}

Office365AdminApp theApp;

void Office365AdminApp::InitMSGraph()
{
	// https://docs.microsoft.com/en-us/sharepoint/dev/general-development/how-to-avoid-getting-throttled-or-blocked-in-sharepoint-online
	// Doesn't seem to have a big impact though... Still getting HTTP 429 errors (#180514.SR.00841A).
	RestSession::g_UserAgent = _YTEXTFORMAT(L"ISV|%s|%s/%s", Product::getInstance().getCompany().c_str(), Product::getInstance().getApplication().c_str(), Product::getInstance().getMajMinVersion().c_str());// Product::getInstance().getFullNameWithFullVersion();
}

void Office365AdminApp::SetSessionBeingLoadedIdentifier(const boost::YOpt<SessionIdentifier>& p_Val)
{
	m_SessionBeingLoadedIdentifier = p_Val;
}

std::mutex& Office365AdminApp::GetSessionTypeLock()
{
	return m_SessionTypeLock;
}

const boost::YOpt<SessionIdentifier>& Office365AdminApp::GetSessionBeingLoadedIdentifier() const
{
	return m_SessionBeingLoadedIdentifier;
}
void Office365AdminApp::SetFullAdminTenant(wstring val)
{
	m_FullAdminTenant = val;
}	

wstring Office365AdminApp::GetFullAdminTenant() const
{
    return m_FullAdminTenant;
}

void Office365AdminApp::SetFullAdminAppId(wstring val)
{
	m_FullAdminAppId = val;
}

wstring Office365AdminApp::GetFullAdminAppId() const
{
    return m_FullAdminAppId;
}

void Office365AdminApp::SetFullAdminAppPassword(const wstring& p_Password)
{
    m_FullAdminAppPassword = p_Password;
}

wstring Office365AdminApp::GetFullAdminAppPassword() const
{
    return m_FullAdminAppPassword;
}

void Office365AdminApp::SetFullAdminDisplayName(const wstring& p_DisplayName)
{
	m_FullAdminDisplayName = p_DisplayName;
}

wstring Office365AdminApp::GetFullAdminDisplayName() const
{
	return m_FullAdminDisplayName;
}

wstring Office365AdminApp::SendFeedback(const wstring& p_Mood, const wstring& p_Message, const wstring& p_CurrentWindowTitle)
{
	auto licenseManager = GetLicenseManager();
	ASSERT(nullptr != licenseManager);

	wstring error;

	wstring message;
	MFCUtil::convertEscapeXMLChar(p_Message, message);

	wstring currentWindowTitle;
	MFCUtil::convertEscapeXMLChar(p_CurrentWindowTitle, currentWindowTitle);

	web::uri_builder builder(RegistryYtria().getDominoBackendURLHTTPS(_YTEXT("FeedbackBeta.nsf/feedback?openAgent")));
	web::http::client::http_client client(RegistryYtria().getDominoBackendURLHTTPS(_YTEXT("")), NetworkUtils::GetHttpClientConfig());

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
	const auto& allFrames = CommandDispatcher::GetInstance().GetAllGridFrames();

	wostringstream body;

	//TODO use Xerces to generate the XML! cf serializer or command history

	body << _YTEXT("<ytria>")

		<< _YTEXT("<comment>")
		<< message
		<< _YTEXT("</comment>")

		<< _YTEXT("<mood>")
		<< p_Mood
		<< _YTEXT("</mood>")

		<< _YTEXT("<windows>")
		<< wstring(OSUtil::GetOSVersion())
		<< _YTEXT("</windows>")

		<< _YTEXT("<bit>")
#if _WIN64
		<< _YTEXT("64")
#else
		<< _YTEXT("32")
#endif
		<< _YTEXT("</bit>")

		<< _YTEXT("<version>")
		<< Product::getInstance().getVersion()
		<< _YTEXT("</version>")

		<< _YTEXT("<curmodule>")
		<< currentWindowTitle
		<< _YTEXT("</curmodule>")

		<< _YTEXT("<mainframe>")

		<< _YTEXT("<session>")
		<< (mainFrame->GetSessionInfo().IsUltraAdmin() ? _YTEXT("fulladmin") : _YTEXT("regular"))
		<< _YTEXT("</session>")

		<< _YTEXT("<sessionname>")
		<< mainFrame->GetSessionInfo().GetEmailOrAppId().c_str()
		<< _YTEXT("</sessionname>");

	if (mainFrame->GetSapio365Session())
	{
		auto session = mainFrame->GetSapio365Session();
		const auto& graphCache = session->GetGraphCache();
		body << _YTEXT("<cache>")
			<< _YTEXT("<users>")
			<< Str::getStringFromNumber(graphCache.GetUserCacheSize())
			<< _YTEXT("</users>")
			<< _YTEXT("<deletedusers>")
			<< Str::getStringFromNumber(graphCache.GetDeletedUserCacheSize())
			<< _YTEXT("</deletedusers>")
			<< _YTEXT("<groups>")
			<< Str::getStringFromNumber(graphCache.GetGroupCacheSize())
			<< _YTEXT("</groups>")
			<< _YTEXT("<sites>")
			<< Str::getStringFromNumber(graphCache.GetSiteCacheSize())
			<< _YTEXT("</sites>")
			<< _YTEXT("<orgcontacts>")
			<< Str::getStringFromNumber(graphCache.GetOrgContactCacheSize())
			<< _YTEXT("</orgcontacts>")
			<< _YTEXT("</cache>");
	}

	body	<< _YTEXT("</mainframe>")
			<< _YTEXT("<appname>")
			<< GetApplicationName()
			<< _YTEXT("</appname>")
			<< _YTEXT("<modules>");

	for (const auto& frame : allFrames)
	{
		size_t contextSize = 0;
		{
			const auto& moduleCriteria = frame->GetModuleCriteria();

			if (ModuleCriteria::UsedContainer::IDS == moduleCriteria.m_UsedContainer)
			{
				contextSize = moduleCriteria.m_IDs.size();
			}
			else if (ModuleCriteria::UsedContainer::SUBIDS == moduleCriteria.m_UsedContainer)
			{
				for (const auto& item : moduleCriteria.m_SubIDs)
				{
					contextSize += item.second.size();
				}
			}
		}

		body << _YTEXT("<module>")

			<< _YTEXT("<title>")
			<< frame->GetFrameTitle()
			<< _YTEXT("</title>")

			<< _YTEXT("<context>")
			<< Str::getStringFromNumber(contextSize)
			<< _YTEXT("</context>")

			<< _YTEXT("<grid>")
			<< frame->GetGrid().GetRowDataCount()
			<< _YTEXT("</grid>")

			<< _YTEXT("<session>")
			<< (frame->GetSessionInfo().IsUltraAdmin() ? _YTEXT("fulladmin") : _YTEXT("regular"))
			<< _YTEXT("</session>")

			<< _YTEXT("<sessionname>")
			<< frame->GetSessionInfo().GetEmailOrAppId().c_str()
			<< _YTEXT("</sessionname>");

			if (frame->GetSapio365Session())
			{
				auto session = frame->GetSapio365Session();
				const auto& graphCache = session->GetGraphCache();
				body << _YTEXT("<cache>")
					<< _YTEXT("<users>")
					<< Str::getStringFromNumber(graphCache.GetUserCacheSize())
					<< _YTEXT("</users>")
					<< _YTEXT("<groups>")
					<< Str::getStringFromNumber(graphCache.GetGroupCacheSize())
					<< _YTEXT("</groups>")
					<< _YTEXT("<sites>")
					<< Str::getStringFromNumber(graphCache.GetSiteCacheSize())
					<< _YTEXT("</sites>")
					<< _YTEXT("</cache>");
			}

			body	<< _YTEXT("</module>");
	}

	body << _YTEXT("</modules>");
	if (nullptr != licenseManager)
	{
		VendorLicense license = licenseManager->GetLicense(false);
		body
				<< _YTEXT("<compid>")
				<< license.m_ComputerID
				<< _YTEXT("</compid>")

				<< _YTEXT("<sugarref>")
				<< license.m_SugarREF
				<< _YTEXT("</sugarref>")

				<< _YTEXT("<license>")
				<< license.m_LicenseCode
				<< _YTEXT("</license>")

				<< _YTEXT("<replyTo>")
				<< licenseManager->GetUserData().m_Email
				<< _YTEXT("</replyTo>")

				;
	}
	body <<  _YTEXT("</ytria>");
	wstring tr = body.str();
	TRACE(_YDUMP("\n\nBODY\n\n%s\n\n"), tr.c_str());
	const web::http::http_response response = client.request(web::http::methods::POST, builder.to_string(), body.str(), _YTEXT("application/xml")).get(); // blocking
	const wstring responseBody = response.extract_string().get();

	if (Str::find(responseBody, _YTEXT("<ok>ok</ok>")) == std::wstring::npos) // not success
	{
		auto errorPos = Str::find(responseBody, _YTEXT("<error>"));
		ASSERT(std::wstring::npos != errorPos);
		if (std::wstring::npos != errorPos)
		{
			errorPos += wstring(_YTEXT("<error>")).size();
			auto endErrorPos = Str::find(responseBody, _YTEXT("</error>"));

			error = responseBody.substr(errorPos, endErrorPos - errorPos);
		}
		else
		{
			error = YtriaTranslate::DoError(CIAOException_stringLoad_1, _YLOC("Unknown error %1"),_YR("Y2525"), Str::getStringFromNumber(response.status_code()).c_str());
		}
	}

	return error;
}

void Office365AdminApp::SuggestNewJob(CodeJockFrameBase* p_SourceFrame)
{
	ASSERT(nullptr != p_SourceFrame);
	bool done = false;
	wstring userText;
	while (!done)
	{
		DlgFeedback dlg(userText, DlgFeedback::MOOD::SUGGEST_NEW_JOB, p_SourceFrame);
		const auto res = dlg.DoModal();

		if (IDCANCEL == res)
		{
			done = true;
		}
		else
		{
			CString windowTitle;
			if (nullptr != p_SourceFrame)
				p_SourceFrame->GetWindowText(windowTitle);
			userText = dlg.GetText();
			const auto error = SendFeedback(_YTEXT("JobRequest"), userText, (LPCTSTR)windowTitle);
			if (error.empty())
			{
				YCodeJockMessageBox msgBox(p_SourceFrame, DlgMessageBox::eIcon_Information, YtriaTranslate::Do(AutomationWizardO365_RunFooter_1, _YLOC("Thank you!")).c_str(), YtriaTranslate::Do(AutomationWizardO365_RunFooter_2, _YLOC("Your request was successfully sent to Ytria.")).c_str(), _YTEXT(""), { {IDOK, YtriaTranslate::Do(AutomationWizardO365_RunFooter_3, _YLOC("Close")).c_str()} });
				msgBox.DoModal();
				done = true;
			}
			else
			{
				YCodeJockMessageBox msgBox(p_SourceFrame, DlgMessageBox::eIcon_Error, YtriaTranslate::Do(AutomationWizardO365_RunFooter_4, _YLOC("Request was not sent.")).c_str(), YtriaTranslate::Do(AutomationWizardO365_RunFooter_5, _YLOC("An error occurred:")).c_str(), error, { { IDOK, YtriaTranslate::Do(AutomationWizardO365_RunFooter_6, _YLOC("Try Again")).c_str() },{ IDCANCEL, YtriaTranslate::Do(AutomationWizardO365_RunFooter_7, _YLOC("Cancel")).c_str() } });
				if (IDCANCEL == msgBox.DoModal())
					done = true;
			}
		}
	}
}

void Office365AdminApp::SetAutomationLastLoadingError(const wstring& p_Error)
{
	g_LastLoadingError = p_Error;
}

bool Office365AdminApp::ShowFeatureUnfinishedDialog(CWnd* p_Parent)
{
	DlgUnderConstruction dlg(p_Parent);
	dlg.SetTitle(_YTEXT("We're still working on this feature!"));
	dlg.SetText(_YTEXT("You can continue, but we can't guarantee it will work. Sorry for the inconvenience."));
	dlg.SetOkButtonText(_YTEXT("OK, I'll risk it"));
	dlg.SetCancelButtonText(_YTEXT("No thanks"));
	return dlg.DoModal() == IDOK;
}

std::shared_ptr<Sapio365Session> Office365AdminApp::GetSapioSessionBeingLoaded()
{
	return m_SapioSessionBeingLoaded;
}

void Office365AdminApp::SetSapioSessionBeingLoaded(const std::shared_ptr<Sapio365Session>& p_Session)
{
	m_SapioSessionBeingLoaded = p_Session;
}

void Office365AdminApp::ResetSapioSessionBeingLoaded()
{
	m_SapioSessionBeingLoaded		= nullptr;
	m_SessionBeingLoadedIdentifier = boost::none;
}

bool Office365AdminApp::GetIsCreatingSession() const
{
	return m_SessionBeingCreated;
}

void Office365AdminApp::SetIsCreatingSession(bool p_Creating)
{
	m_SessionBeingCreated = p_Creating;
}

void Office365AdminApp::CreateAdvancedSession(const boost::YOpt<Command>& p_Command, const wstring& p_PreferredLogin/* = Str::g_EmptyString*/)
{
    createUserSession(SessionTypes::g_AdvancedSession, p_Command, p_PreferredLogin);
}

void Office365AdminApp::CreatePartnerSession(const boost::YOpt<Command>& p_Command, const wstring& p_CustomerTenant, const wstring& p_PreferredLogin /*= Str::g_EmptyString*/)
{
	createUserSession(SessionTypes::g_PartnerAdvancedSession, p_Command, p_PreferredLogin, p_CustomerTenant);
}

void Office365AdminApp::CreateUltraAdminSession(const wstring& p_Tenant, const wstring& p_ClientId, const wstring& p_ClientSecret, const wstring& p_DisplayName, const boost::YOpt<Command>& p_Command, shared_ptr<Sapio365Session> p_Session)
{
	DlgSpecialExtAdminAccess dlg(p_Tenant, p_ClientId, p_ClientSecret, p_DisplayName, nullptr, p_Session, std::make_unique<DlgSpecialExtAdminUltraAdminJsonGenerator>(false), false);
    if (dlg.DoModal() == IDOK)
    {
        auto tenant			= dlg.GetTenant();
        auto clientId		= dlg.GetAppId();
        auto clientSecret	= dlg.GetClientSecret();
		auto displayName	= dlg.GetDisplayName();

        auto testRes = RoleDelegationUtil::TestUltraAdminSession(tenant, clientId, clientSecret);

        MainFrame* mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
        if (testRes.first)
        {
		    // New MSGraphSession
		    auto sapioSession = std::make_shared<Sapio365Session>();
            sapioSession->SetGraphCache(std::make_unique<GraphCache>(sapioSession));
		    m_SapioSessionBeingLoaded = sapioSession;
			SetIsCreatingSession(true);

            web::http::uri_builder tokenUri(Rest::GetAzureADEndpoint());
            tokenUri.append_path(tenant);
            tokenUri.append_path(_YTEXT("oauth2/v2.0/token"));

            auto credentialsAuthenticator = std::make_unique<ClientCredentialsAuthenticator>(sapioSession->GetMainMSGraphSession(),
                clientId,
                clientSecret,
                tokenUri.to_string(),
			    Rest::GetMsGraphEndpoint() + _YTEXT("/.default"));
            //auto sessionManager = std::make_unique<RestCredentialsRegistryManager_DEPRECATED>(PersistentSession::GetApiNameForRegistry(graphSession->GetMainMSGraphSession()->GetName()));
            //credentialsAuthenticator->SetCredentialsManager(std::move(sessionManager));

		    sapioSession->GetMainMSGraphSession()->SetAuthenticator(std::move(credentialsAuthenticator));

            Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
            ASSERT(nullptr != app);
            if (nullptr != app)
            {
                app->SetFullAdminTenant(tenant);
                app->SetFullAdminAppId(clientId);
                app->SetFullAdminAppPassword(dlg.GetSaveSecretKey() ? clientSecret : _YTEXT(""));
				app->SetFullAdminDisplayName(displayName);
            }

			ASSERT(!m_SessionBeingLoadedIdentifier);
			m_SessionBeingLoadedIdentifier = SessionIdentifier();
		    m_SessionBeingLoadedIdentifier->m_SessionType = SessionTypes::g_UltraAdmin;
			m_SessionBeingLoadedIdentifier->m_EmailOrAppId = clientId;
			m_SessionBeingLoadedIdentifier->m_RoleID = 0;

            const wstring oldSessionType = GetSessionType();
            m_SessionType = SessionTypes::g_UltraAdmin;

		    sapioSession->GetMainMSGraphSession()->Start(mainFrame).Then([this, p_Session, oldSessionType, p_Command, mainFrame, sapioSession, tenant, clientId, clientSecret, displayName](const RestAuthenticationResult& p_Result)
            {
                bool success = p_Result.state == RestAuthenticationResult::State::Success;

                if (!success)
                {
                    std::lock_guard<std::mutex> lock(m_SessionTypeLock);
                    m_SessionType = oldSessionType;
                    YCallbackMessage::DoPost([this, mainFrame, p_Session, error = p_Result.error_message, tenant, clientId, clientSecret, displayName, p_Command]()
                    {
						{
							TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("CreateFullAdminSession"), _YDUMPFORMAT(L"Error while authenticating %s (%s) : %s", tenant.c_str(), clientId.c_str(), error.c_str()));
							// FIXME: We should use the logger service.
							// We need to rework logger channels to allow trace-only logging.
							//LoggerService::Debug(YtriaTranslate::Do(Office365AdminApp_CreateFullAdminSession_4, _YLOC("CreateFullAdminSession: Error while authenticating %1 (%2) : %3"), tenant.c_str(), clientId.c_str(), error.c_str()));
						}

						if (AutomatedApp::IsAutomationRunning() && IsActionLoadSession(GetCurrentAction()))
						{
							GetCurrentAction()->AddError(_YFORMAT(L"An error occurred while authenticating: %s", Sapio365Session::ParseGraphJsonError(error).m_ErrorDescription.c_str()).c_str());
							SetAutomationGreenLight(GetCurrentAction());
						}
						else
                        {
							Util::ShowApplicationConnectionError(_T("Unable to sign in using the provided Ultra Admin credentials."), error, mainFrame);
							CreateUltraAdminSession(tenant, clientId, clientSecret, displayName, p_Command, p_Session);
						}
                    });
                }
                else
                {
                    {
                        std::lock_guard<std::mutex> lock(m_SessionTypeLock);
                        m_SessionType = SessionTypes::g_UltraAdmin;
                    }

                    if (p_Command)
                    {
                        if (nullptr != mainFrame)
					    {
                            mainFrame->WaitSyncCacheEvent();
						    auto& c = const_cast<Command&>(p_Command.get());
						    c.SetLicenseContext(mainFrame->GetLicenseContext());
					    }

                        YCallbackMessage::DoPost([this, p_Command]()
                        {
                            CommandDispatcher::GetInstance().Execute(*p_Command);
                        });
                    }
                }
		    });

        }
        else
        {
            YCallbackMessage::DoPost([this, mainFrame, p_Session, error = testRes.second, tenant, clientId, clientSecret, displayName, p_Command]()
            {
				{
					TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("CreateFullAdminSession"), _YDUMPFORMAT(L"Error while testing authentication %s (%s): %s", tenant.c_str(), clientId.c_str(), error.c_str()));
					// FIXME: We should use the logger service.
					// We need to rework logger channels to allow trace-only logging.
					//LoggerService::Debug(YtriaTranslate::Do(Office365AdminApp_CreateFullAdminSession_4, _YLOC("CreateFullAdminSession: Error while authenticating %1 (%2): %3"), tenant.c_str(), clientId.c_str(), error.c_str()));
				}

				if (AutomatedApp::IsAutomationRunning() && IsActionLoadSession(GetCurrentAction()))
				{
					GetCurrentAction()->AddError(_YFORMAT(L"An error occurred while authenticating: %s", Sapio365Session::ParseGraphJsonError(error).m_ErrorDescription.c_str()).c_str());
					SetAutomationGreenLight(GetCurrentAction());
				}
				else
				{
					Util::ShowApplicationConnectionError(_T("Unable to sign in using the provided Ultra Admin credentials."), error, mainFrame);
					CreateUltraAdminSession(tenant, clientId, clientSecret, displayName, p_Command, p_Session); // Re-ask for credentials
				}
            });
        }
    }
}

std::pair<bool, wstring> Office365AdminApp::RegenerateAppPassword(CWnd* p_Parent, const wstring& p_AppId, const std::shared_ptr<Sapio365Session>& p_Session, const shared_ptr<DlgLoading>& p_DlgLoading)
{
	bool success = false;
	wstring password;

	std::pair<bool, wstring> retVal;

	Application foundApp = ApplicationListRequester::GetAppFromAppId(p_Session, p_AppId);

	ASSERT(foundApp.m_Id);
	if (foundApp.m_Id)
	{
		bool proceed = true;
		if (foundApp.m_PasswordCredentials && foundApp.m_PasswordCredentials->size() > 1)
		{
			ASSERT(nullptr != p_Parent && GetCurrentThreadId() == GetWindowThreadProcessId(p_Parent->m_hWnd, NULL));
			YCodeJockMessageBox resultDlg(p_Parent,
				DlgMessageBox::eIcon_Information,
				_YTEXT("sapio365"),
				YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_30, _YLOC("This will overwrite existing passwords for this application. Continue?")).c_str(),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(ModuleGroup_updateSettings_4, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(DlgBusinessEditHTML_DlgBusinessEditHTML_2, _YLOC("Cancel")).c_str() } });
			proceed = resultDlg.DoModal() == IDOK;
		}

		if (proceed)
		{
			AddApplicationPasswordRequester requester(YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_5, _YLOC("sapio365 Ultra Admin")).c_str(), *foundApp.m_Id);
			{
				CWaitCursor cursor;
				for (const auto& cred : *foundApp.m_PasswordCredentials)
				{
					ASSERT(cred.m_KeyId != boost::none);
					if (cred.m_KeyId != boost::none)
					{
						RemoveApplicationPasswordRequester removeAppReq(*foundApp.m_Id, *cred.m_KeyId);
						removeAppReq.Send(p_Session, YtriaTaskData()).GetTask().wait();
					}
				}
				requester.Send(p_Session, YtriaTaskData()).GetTask().wait();
			}

			ASSERT(requester.GetData().m_SecretText);
			if (requester.GetData().m_SecretText)
				password = *requester.GetData().m_SecretText;

			auto statusCode = requester.GetResult().GetResult().Response.status_code();
			if (statusCode >= 200 && statusCode < 300)
			{
				success = true;

				YCallbackMessage::DoExecAsap(p_Parent, [p_Parent, p_DlgLoading]() {
					if (p_DlgLoading)
						p_DlgLoading->EndDialog(IDOK);
					YCodeJockMessageBox resultDlg(p_Parent,
						DlgMessageBox::eIcon_Information,
						YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_21, _YLOC("Success!")).c_str(),
						YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_34, _YLOC("The password has been generated successfully!")).c_str(),
						_YTEXT(""),
						{ { IDOK, YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_35, _YLOC("OK")).c_str() } });
					resultDlg.DoModal();
				});
			}
			else
			{
				YCallbackMessage::DoExecAsap(p_Parent, [p_Parent, p_DlgLoading]() {
					if (p_DlgLoading)
						p_DlgLoading->EndDialog(IDOK);
					YCodeJockMessageBox resultDlg(p_Parent,
						DlgMessageBox::eIcon_ExclamationWarning,
						_YTEXT("sapio365"), YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_40, _YLOC("Unable to update application password.")).c_str(),
						_YTEXT(""),
						{ { IDOK, YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_36, _YLOC("OK")).c_str() } });
					resultDlg.DoModal();
				});
			}
		}
	}
	else
	{
		YCallbackMessage::DoExecAsap(p_Parent, [p_Parent, p_AppId, p_DlgLoading]() {
			if (p_DlgLoading)
				p_DlgLoading->EndDialog(IDOK);
			YCodeJockMessageBox resultDlg(p_Parent,
				DlgMessageBox::eIcon_ExclamationWarning,
				_YTEXT("sapio365"),
				YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_43, _YLOC("Application with id \"%1\" was not found."), p_AppId.c_str()),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_37, _YLOC("OK")).c_str() } });
			resultDlg.DoModal();
		});
	}

	return std::make_pair(success, password);
}

std::pair<bool, wstring> Office365AdminApp::TestElevatedSession(const std::shared_ptr<Sapio365Session>& p_Session, MainFrame& p_Mainframe, const PersistentSession& p_PersistentSession, bool p_TestWithDialog)
{
	std::pair<bool, wstring> sessionResult;
	p_Session->GetGateways().SetUserGraphSession(p_Session->GetMainMSGraphSession());

	AppGraphSessionCreator creator(p_PersistentSession.GetTenantName(), p_PersistentSession.GetAppId(), p_PersistentSession.GetAppClientSecret(), p_PersistentSession.GetSessionName());
	auto appSession = creator.Create();
	if (p_TestWithDialog)
		sessionResult = RoleDelegationUtil::TestSessionWithDialog(appSession, p_PersistentSession.GetTenantName(), &p_Mainframe);
	else
		sessionResult = RoleDelegationUtil::TestSession(appSession, p_PersistentSession.GetTenantName());

	if (sessionResult.first)
	{
		p_Session->GetGateways().SetAppGraphSession(appSession);
		p_Session->GetGateways().SetAppGraphSessionCredentials(p_PersistentSession.GetAppId(), p_PersistentSession.GetAppClientSecret());
		p_Session->GetGateways().SetTenant(p_PersistentSession.GetTenantName());
	}

	return sessionResult;
}

bool Office365AdminApp::IsSessionBeingLoadedUltraAdmin() const
{
	ASSERT(m_SessionBeingLoadedIdentifier);
	return m_SessionBeingLoadedIdentifier && m_SessionBeingLoadedIdentifier->m_SessionType == SessionTypes::g_UltraAdmin;
}

int64_t Office365AdminApp::GetSessionBeingLoadedRoleID() const
{
	return m_SessionBeingLoadedIdentifier ? m_SessionBeingLoadedIdentifier->m_RoleID : SessionIdentifier::g_NoRole;
}

const wstring& Office365AdminApp::GetSessionBeingLoadedSessionType() const
{
	return m_SessionBeingLoadedIdentifier ? m_SessionBeingLoadedIdentifier->m_SessionType : Str::g_EmptyString;
}

const wstring& Office365AdminApp::GetSessionType() const
{
    std::lock_guard<std::mutex> lock(m_SessionTypeLock);
    return m_SessionType; // Useful only while the global session has not been built
}

void Office365AdminApp::SetSessionType(const wstring& p_SessionType)
{
	m_SessionType = p_SessionType;
}

TaskWrapper<bool> Office365AdminApp::LoadSessionFromAutomationOrCommandLine(const wstring& p_SessionNameX, const wstring& p_SessionType, const wstring& p_RoleNameX, AutomationAction* p_Action/* = nullptr*/)
{
	wstring error;

	// role can be inserted in session name from UI drop-down selection with list of role sessions in automation - split them
	auto	sessionName = p_SessionNameX;
	auto	role		= p_RoleNameX;
	auto	nameAndRole = Str::explodeIntoVector(p_SessionNameX, g_O365_ListItemSessionRoleSeparator);
	if (nameAndRole.size() == 2)
	{	
		sessionName = nameAndRole[0];
		role		= nameAndRole[1];
		ASSERT(MFCUtil::StringMatchW(role, p_RoleNameX) || p_RoleNameX.empty());
	}

	ASSERT(!sessionName.empty());
	if (sessionName.empty())
		error = _YERROR("Can't load session with empty name.");

	if (error.empty() && !role.empty() && !p_SessionType.empty() && p_SessionType != SessionTypes::g_Role)
		error = _YFORMATERROR(L"Can't load role session with type \"%s\".", p_SessionType.c_str());

	if (error.empty() && p_SessionType == SessionTypes::g_Role && role.empty())
		error = _YERROR("Can't load role session without specifying role name.");

	SessionIdentifier sessionIdentifier;

	if (error.empty())
	{
		sessionIdentifier.m_SessionType = p_SessionType;

		if (!role.empty())
			sessionIdentifier.m_SessionType = SessionTypes::g_Role;

		if (sessionIdentifier.m_SessionType == SessionTypes::g_UltraAdmin)
		{
			QuerySessionByName sessionByDisplayName(SessionsSqlEngine::Get(), sessionName, sessionIdentifier.m_SessionType, 0);
			sessionByDisplayName.Run();
			sessionIdentifier.m_EmailOrAppId = sessionByDisplayName.GetEmailOrAppId();
		}
		else if (sessionIdentifier.m_SessionType == SessionTypes::g_Role && !role.empty())
		{
			QueryRoleIdByName roleIdByName(SessionsSqlEngine::Get(), sessionName, role);
			roleIdByName.Run();
			if (0 != roleIdByName.GetRoleId())
			{
				sessionIdentifier.m_EmailOrAppId	= sessionName;
				sessionIdentifier.m_RoleID			= roleIdByName.GetRoleId();
			}
		}
		else if (sessionIdentifier.m_SessionType == SessionTypes::g_StandardSession
			|| sessionIdentifier.m_SessionType == SessionTypes::g_AdvancedSession
			|| sessionIdentifier.m_SessionType == SessionTypes::g_ElevatedSession
			|| sessionIdentifier.m_SessionType == SessionTypes::g_PartnerAdvancedSession)
		{
			sessionIdentifier.m_EmailOrAppId = sessionName;
		}

		if (!sessionIdentifier.m_EmailOrAppId.empty())
		{
			QuerySessionExists sessionExists(sessionIdentifier, SessionsSqlEngine::Get());
			sessionExists.Run();
			if (!sessionExists.Exists())
				sessionIdentifier.m_EmailOrAppId.clear();
		}
	}

	if (sessionIdentifier.m_EmailOrAppId.empty())
	{
		if (error.empty())
		{
			if (sessionIdentifier.m_SessionType == SessionTypes::g_UltraAdmin)
				error = _YFORMATERROR(L"Ultra Admin Session \"%s\" doesn't exist.", sessionName.c_str());
			else if (sessionIdentifier.m_SessionType == SessionTypes::g_ElevatedSession)
				error = _YFORMATERROR(L"Elevated Session \"%s\" doesn't exist.", sessionName.c_str());
			else if (sessionIdentifier.m_SessionType == SessionTypes::g_AdvancedSession)
				error = _YFORMATERROR(L"Advanced Session \"%s\" doesn't exist.", sessionName.c_str());
			else if (sessionIdentifier.m_SessionType == SessionTypes::g_StandardSession)
				error = _YFORMATERROR(L"Standard Session \"%s\" doesn't exist.", sessionName.c_str());
			else if (sessionIdentifier.m_SessionType == SessionTypes::g_Role)
				error = _YFORMATERROR(L"Role Session \"[%s] %s\" doesn't exist.", role.c_str(), sessionName.c_str());
			else if (sessionIdentifier.m_SessionType == SessionTypes::g_PartnerAdvancedSession)
				error = _YFORMATERROR(L"Partner Advanced Session \"%s\" doesn't exist.", sessionName.c_str());
		}

		if (nullptr != p_Action)
		{
			p_Action->AddError(error.c_str());
			SetAutomationGreenLight(p_Action);
		}
		else
		{
			ASSERT(!Office365AdminApp::IsAutomated());
			YCodeJockMessageBox(m_pMainWnd,
								DlgMessageBox::eIcon_Error,
								_YTEXT("Load Session"),
								error,
								_YTEXT(""),
								{ { IDOK, _YTEXT("Ok") } }
			).DoModal();
		}
		return pplx::task_from_result<bool>(false);
	}

	auto loader = SessionLoaderFactory(sessionIdentifier).CreateLoader();
	auto loadTask = SessionSwitcher(std::move(loader)).Switch();

	if (nullptr != p_Action)
	{
		// Non blocking call.
		// Just return true to please automation.
		return pplx::task_from_result<bool>(true);
	}

	return loadTask.Then([](const std::shared_ptr<Sapio365Session>& p_Session) {
		return nullptr != p_Session;
	});
}

TaskWrapper<std::shared_ptr<Sapio365Session>> Office365AdminApp::RelogElevatedSession(const std::shared_ptr<Sapio365Session>& p_SapioSession, const PersistentSession& p_PersistentSession, const SessionIdentifier& p_SessionIdentifier, bool p_FromModule, CFrameWnd* p_ParentFrame, bool p_RelogUltraAdmin)
{
	ASSERT(p_PersistentSession.IsElevated() || p_PersistentSession.IsPartnerElevated());
	auto authorizationAuthenticator = std::make_unique<AzureADOAuth2BrowserAuthenticator>(p_SapioSession->GetMainMSGraphSession(),
		Rest::GetMsGraphEndpoint(),
		_YTEXT(""),
		SessionTypes::GetAppId(p_PersistentSession.GetSessionType()),
		SessionTypes::GetUserRedirectUri(p_PersistentSession.GetSessionType()),
		std::make_unique<DlgBrowserOpener>()
		);
	authorizationAuthenticator->SetPreferredLogin(p_PersistentSession.GetEmailOrAppId());
	p_SapioSession->GetMainMSGraphSession()->SetAuthenticator(std::move(authorizationAuthenticator));
	p_SapioSession->SetSessionType(_YTEXT(""));

	LoggerService::Debug(_YTEXTFORMAT(L"LoadSession: session with name %s is logged-out. Prompting for login", p_SessionIdentifier.m_EmailOrAppId.c_str()));

	const wstring oldSessionType = GetSessionType();
	if (!p_FromModule)
		m_SessionType = SessionTypes::g_ElevatedSession;

	return p_SapioSession->GetMainMSGraphSession()->Start(p_ParentFrame, false).Then([this, p_FromModule, p_SessionIdentifier, p_ParentFrame, p_PersistentSession, oldSessionType, p_SapioSession, techName = p_SessionIdentifier.m_EmailOrAppId, p_RelogUltraAdmin](const RestAuthenticationResult& p_Result)
	{
		bool authSuccess = p_Result.state == RestAuthenticationResult::State::Success;
		if (authSuccess)
		{
			if (p_RelogUltraAdmin)
			{
				// First step of authentication passed?
				YCallbackMessage::DoSend([this, p_PersistentSession, p_SessionIdentifier, p_ParentFrame, p_SapioSession, p_FromModule]() mutable {
					// Ask if user just wants to regenerate password
					YCodeJockMessageBox msg(p_ParentFrame, DlgMessageBox::eIcon_Question, _T("Regenerate password?"), _T("Do you want to regenerate the password of the application used by this session?"), _T(""), { {IDOK, _T("Yes")}, {IDCANCEL, _T("No")} });
					auto msgRes = msg.DoModal();

					auto persistentSession = p_PersistentSession;
					if (msgRes == IDOK)
					{
						auto genPasswdResult = RegenerateAppPassword(p_ParentFrame, persistentSession.GetAppId(), p_SapioSession, nullptr);
						if (!genPasswdResult.first)
						{
							ShowUltraAdminForElevatedSessionAuth(persistentSession, p_SapioSession, p_ParentFrame);
						}
						else
						{
							bool retry = true;
							while (retry)
							{
								AppGraphSessionCreator creator(persistentSession.GetTenantName(), persistentSession.GetAppId(), genPasswdResult.second, persistentSession.GetSessionName());
								auto appSession = creator.Create();
								auto result = RoleDelegationUtil::TestSessionWithDialog(appSession, persistentSession.GetTenantName(), p_ParentFrame);
								if (result.first)
								{
									retry = false;
									persistentSession.SetAppClientSecret(genPasswdResult.second);
									SessionsSqlEngine::Get().Save(persistentSession);
									p_SapioSession->GetGateways().SetUserGraphSession(p_SapioSession->GetMainMSGraphSession());
									p_SapioSession->GetGateways().SetAppGraphSession(appSession);
									p_SapioSession->GetGateways().SetAppGraphSessionCredentials(persistentSession.GetAppId(), persistentSession.GetAppClientSecret());
									p_SapioSession->GetGateways().SetTenant(persistentSession.GetTenantName());

									if (p_FromModule)
									{
										m_SessionType = SessionTypes::g_ElevatedSession;
										m_SapioSessionBeingLoaded = p_SapioSession;
										m_SessionBeingLoadedIdentifier = p_SessionIdentifier;
									}
									p_ParentFrame->SendMessage(WM_AUTHENTICATION_SUCCESS, (WPARAM) new wstring(p_SapioSession->GetMainMSGraphSession()->GetName()), 0);
								}
								else
								{
									retry = IDRETRY == Util::ShowApplicationConnectionError(_T("Unable to sign back in with the current Advanced session with Elevated privileges."), result.second, p_ParentFrame, true);
								}
							}
						}
					}
					else
					{
						// User session is authenticated, let's show the ultra admin dialog for the ultra admin part.
						ShowUltraAdminForElevatedSessionAuth(persistentSession, p_SapioSession, p_ParentFrame);
					}
				});
			}
			else
				p_ParentFrame->SendMessage(WM_AUTHENTICATION_SUCCESS, (WPARAM) new wstring(p_SapioSession->GetMainMSGraphSession()->GetName()), 0);

			TraceIntoFile::trace(_YDUMP("[["s) + SessionIdsEmitter(p_SessionIdentifier).GetId() + _YDUMP("]]") + _YDUMP("Office365AdminApp"), _YDUMP("LoadSession"), _YDUMPFORMAT(L"Successfully loaded %s session: %s", m_SessionType.c_str(), techName.c_str()));
		}
		else if (!p_FromModule)
		{
			std::lock_guard<std::mutex> lock(m_SessionTypeLock);
			m_SessionType = oldSessionType;
		}

		return authSuccess ? p_SapioSession : nullptr;
	});
}

void Office365AdminApp::ShowUltraAdminForElevatedSessionAuth(PersistentSession& persistentSession, std::shared_ptr<Sapio365Session> sapioSession, CFrameWnd* p_ParentFrame)
{
	DlgSpecialExtAdminAccess dlg(persistentSession.GetTenantName(),
		persistentSession.GetAppId(),
		persistentSession.GetAppClientSecret(),
		persistentSession.GetSessionName(), p_ParentFrame, sapioSession, std::make_unique<DlgSpecialExtAdminManageElevatedJsonGenerator>(sapioSession, false), false);
	if (dlg.DoModal() == IDOK)
	{
		AppGraphSessionCreator creator(dlg.GetTenant(), dlg.GetAppId(), dlg.GetClientSecret(), dlg.GetDisplayName());
		auto appSession = creator.Create();

		bool sessionOk = RoleDelegationUtil::TestSessionWithDialog(appSession, dlg.GetTenant(), p_ParentFrame).first;
		if (sessionOk)
		{
			persistentSession.SetAppId(dlg.GetAppId());
			persistentSession.SetTenantName(dlg.GetTenant());
			persistentSession.SetAppClientSecret(dlg.GetClientSecret());
			SessionsSqlEngine::Get().Save(persistentSession);
			sapioSession->GetGateways().SetUserGraphSession(sapioSession->GetMainMSGraphSession());
			sapioSession->GetGateways().SetAppGraphSession(appSession);
			sapioSession->GetGateways().SetAppGraphSessionCredentials(dlg.GetAppId(), dlg.GetClientSecret());
			sapioSession->GetGateways().SetTenant(dlg.GetTenant());
			p_ParentFrame->SendMessage(WM_AUTHENTICATION_SUCCESS, (WPARAM) new wstring(sapioSession->GetMainMSGraphSession()->GetName()), 0);
		}
	}
}

void Office365AdminApp::SetFavoriteSession(const SessionIdentifier& p_SessionIdentifier, bool isFavorite)
{
	ASSERT(!p_SessionIdentifier.m_EmailOrAppId.empty() && p_SessionIdentifier.m_RoleID >= SessionIdentifier::g_NoRole);

	PersistentSession loadedSession = SessionsSqlEngine::Get().Load(p_SessionIdentifier);
	ASSERT(loadedSession.IsValid());
	if (loadedSession.IsValid())
	{
		auto session = loadedSession;
		session.SetFavorite(isFavorite);
		SessionsSqlEngine::Get().Save(session);

		// Update list after favorite change.
		MainFrame* mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
		ASSERT(nullptr != mainFrame);
		if (nullptr != mainFrame)
			mainFrame->PopulateRecentSessions();
	}

}

bool Office365AdminApp::IsFavoriteSession(const SessionIdentifier& p_SessionIdentifier, bool isFavorite)
{
	ASSERT(!p_SessionIdentifier.m_EmailOrAppId.empty() && p_SessionIdentifier.m_RoleID >= SessionIdentifier::g_NoRole);

	PersistentSession loadedSession = SessionsSqlEngine::Get().Load(p_SessionIdentifier);
	ASSERT(loadedSession.IsValid());
	if (loadedSession.IsValid())
		return loadedSession.IsFavorite();

	ASSERT(false);
	return false;
}

wstring Office365AdminApp::GetGridConfigFolderPath()
{
	wstring dirPath = GetConfigFolderPath();

	dirPath += L"\\" + Product::s_STRING_ALL_GRIDS_CONFIG_DIR;
	if (CreateDirectory(dirPath.c_str(), NULL) == 0)// https://i.imgflip.com/wqy3h.jpg
	{
		DWORD dirError = GetLastError();
		if (dirError == ERROR_PATH_NOT_FOUND)
			TraceIntoFile::trace(_YTEXT("Office365AdminApp"), _YTEXT("GetGridConfigFolderPath"), YtriaTranslate::DoError(Office365AdminApp_GetGridConfigFolderPath_1, _YLOC("Unable to create directory: %1"),_YR("Y2528"), dirPath.c_str()));
	}

	return dirPath;
}

CacheGrid* Office365AdminApp::getGrid(const wstring& i_GridName)
{
	CacheGrid* grid = GetCurrentContext()[i_GridName];
	if (nullptr == grid)
		AddError(YtriaTranslate::Do(Office365AdminApp_getGrid_1, _YLOC("Grid actions must be nested in a module action, e.g.:\n<%1>\n\t<%2>\n</%3>"), g_ActionNameShowUsers.c_str(), g_ActionNameShowUsers.c_str(), AutomationConstant::val().m_ActionNameUnGroupAll.c_str()));
	return grid;
}

const wstring Office365AdminApp::GetUserName() const
{
	wstring UN;

// CRASH at registration time: SessionSQLEngine not initialized yet
// not needed for sapio365 (nice-to-have - only needed for offline reg. / Notes)
// 	auto sessions = SessionsSqlEngine::Get().GetList();
// 	std::sort(sessions.begin(), sessions.end(), UISessionListSorter());
// 	ASSERT(!sessions.empty());
// 	if (!sessions.empty())
// 		UN = sessions.front().GetFullname();

// Temporary solution in reply to above issue
	auto lastSessionUsed = MainFrameSessionSetting().Get();
	if (lastSessionUsed)
	{
		UN = lastSessionUsed->m_EmailOrAppId;
	}
	else
	{
		// Maybe we didn't migrate yet?
		auto oldLastSessionUsed = MainFrameSessionOldSetting().Get();
		if (oldLastSessionUsed)
			UN = oldLastSessionUsed->m_EmailOrAppId;
	}

	return UN;
}

const wstring Office365AdminApp::GetUserOrganisation() const
{
	// TODO: 
	wstring Toto;
	return Toto;
}

const wstring Office365AdminApp::GetUserOrganisation(const wstring& i_UserName)
{
	// TODO: 
	wstring Toto;
	return Toto;
}

const wstring Office365AdminApp::GetYtriaProductPath(const wstring& p_TargetAppName)
{
	// TODO: 
	wstring Toto;
	return Toto;
}

void Office365AdminApp::setAutomationRulesForPlatform()
{
	// TODO: 
}

void Office365AdminApp::checkStopAutomationSpecific()
{
	// TODO: 
}

void Office365AdminApp::getAdditionalCrashReporterFiles(map<wstring, wstring>& mandatoryFiles, map<wstring, wstring>& optionalFiles)
{
	if (TraceIntoFile::IsEnabled())
	{
		auto& files = TraceIntoFile::GetPossibleTraceFiles();

		ASSERT(files.size() > 0);

		int counter = 0;
		for (auto& file : files)
		{
			if (0 == counter)
				optionalFiles[_YTEXT("Trace")] = file;
			else
				optionalFiles[_YTEXTFORMAT(L"Trace.%s", Str::getStringFromNumber(counter).c_str())] = file;
			++counter;
		}
	}	
}

AutomatedApp::AUTOMATIONSTATUS Office365AdminApp::automationPauseUntil(AutomationAction* i_Action)
{
	// TODO: 
	return AUTOMATIONSTATUS_OK;
}

AutomatedApp::AUTOMATIONSTATUS Office365AdminApp::automationUseDesignCollection(AutomationAction* i_Action)
{
	// TODO: 
	return AUTOMATIONSTATUS_OK;
}

RecentAutomations* Office365AdminApp::createRecentAutomationManager() const
{
	return nullptr;
}

bool Office365AdminApp::InitInstanceSpecific(const CommandLineInfo& i_cmdInfo)
{
	// Nothings works by default.
	bool RetVal = false;

	// FIXME: Dirty patch to prevent multiple row statuses in sapio365 while keeping EZsuite current behavior
	// (as it's not easy to know if changing this would break anything...)
	GridBackendRow::g_PreventMultipleEditStatuses = true;

	// TODO: Needed???
	//
	// 	// InitCommonControlsEx() is required on Windows XP if an application
	// 	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// 	// visual styles.  Otherwise, any window creation will fail.
	// 	INITCOMMONCONTROLSEX InitCtrls;
	// 	InitCtrls.dwSize = sizeof(InitCtrls);
	// 
	// 	// Set this to include all the common control classes you want to use
	// 	// in your application.
	// 	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	// 	InitCommonControlsEx(&InitCtrls);

	//g_PaintManager.InstallPaintManager(RUNTIME_CLASS(CExtPaintManagerWindows7));
	// CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows7));
	//CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);

	// TODO: Needed???
	// 	EnableTaskbarInteraction(FALSE);

	// Initialize OLE libraries
	_AFX_THREAD_STATE* pState = AfxGetThreadState();
	if (!pState->m_bNeedTerm)
		AfxOleInit();
	AfxEnableControlContainer();

	//  Disable those dialogs to avoid this issue: Bug #180420.EH.008272: [- CRITICAL! -] Server Busy message issue when using the CHTmlView
	AfxOleGetMessageFilter()->EnableNotRespondingDialog(FALSE);
	AfxOleGetMessageFilter()->EnableBusyDialog(FALSE);

	ASSERT(Product::isInit());

	ReinitCrashReporter();

	//GridBackendColumn::SetDefaultCaseSensitivity(true);

	RegexSFD::initInstance(new RegexSFDBasicImpl);

	LoggerService::AddLogger(std::make_shared<FileLogger>());
	LoggerService::AddLogger(std::make_shared<VsConsoleLogger>());
	LoggerService::AddLogger(std::make_unique<BackstageLogger>());

	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object
	MainFrame* pFrame = new MainFrame;
	if (nullptr != pFrame)
	{
		// We might consider passing true as soon as we correctly set LogEntry::m_originator when logging.
		//LoggerService::AddLogger(std::make_unique<FrameStatusBarLogger>(*pFrame, false, true));

		m_pMainWnd = pFrame;

		if (pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE | WS_CLIPCHILDREN))
		{
			// TODO: Needed???
			// 	HINSTANCE hInst = AfxGetResourceHandle();
			// 	m_hMDIMenu = ::LoadMenu(hInst, MAKEINTRESOURCE(IDR_MFCApplicationTYPE));
			// 	m_hMDIAccel = ::LoadAccelerators(hInst, MAKEINTRESOURCE(IDR_MFCApplicationTYPE));

			// The one and only window has been initialized, so show and update it
			pFrame->ShowWindow(SW_SHOW);
			pFrame->UpdateWindow();

			SetRecentAutomationMgrLocal(pFrame->GetRecentAutomationManager());// for app launch with -x

			// Now it works.
			RetVal = true;
		}
	}

    InitMSGraph();
	
	// Sessions credentials (registry) and json info location changes between v1.3 to v1.4
	PersistentSession::CheckUpdateTo1dot4();
	{
		MigrationRegistrations registrations;
		auto provisioner = std::make_unique<SessionMigrationsProvisioner>(registrations);
		MigrationRunner migrationRunner(std::move(provisioner));
		migrationRunner.Run();
	}

	// Just for those people who ran a version that migrated 2 to 3 without renaming the last used (probably only here at Ytria)
	{
		auto oldLastSessionUsed = MainFrameSessionOldSetting().Get();
		auto lastSessionUsed = MainFrameSessionSetting().Get();
		if (oldLastSessionUsed && !lastSessionUsed)
		{
			lastSessionUsed = oldLastSessionUsed;
			MainFrameSessionSetting().Set(lastSessionUsed);

			auto& sessionType = oldLastSessionUsed->m_SessionType;
			if (sessionType == SessionTypes::g_StandardSession)
				sessionType = _YTEXT("basic_user");
			else if (sessionType == SessionTypes::g_AdvancedSession)
				sessionType = _YTEXT("delegated_admin");
			else if (sessionType == SessionTypes::g_UltraAdmin)
				sessionType = _YTEXT("full_admin");
			else if (sessionType == SessionTypes::g_ElevatedSession)
				sessionType = _YTEXT("full_session");

			MainFrameSessionOldSetting().Set(oldLastSessionUsed);
		}
	}

	SessionsSqlEngine::Init(); // always init AFTER migration

	QueryRemoveUnusedCredentials queryCleanup(SessionsSqlEngine::Get());
	queryCleanup.Run();

	// Add some time-initialization traces. Do it only after the NotesSession and the
	// Product has been initialized. [2007/05/18]
	Product::getInstance<ProductBasic>().tracesAtProductInitialization();

	// Backup the command line in case YtriaUpdate will be use. (=>> NEVER IN SAPIO365 though)
	Product::getInstance<ProductBasic>().initYtriaUpdateContext(getCurrentAppFullPath(), theApp.m_lpCmdLine);

	// Init recent sessions list
	const auto lastPersistentSession = pFrame->PopulateRecentSessions();
	// Retrieve last session
	auto lastSessionUsed = MainFrameSessionSetting().Get();
	if (!lastSessionUsed && lastPersistentSession.IsValid()) // Backward compatibility
		lastSessionUsed = lastPersistentSession.GetIdentifier();

	// ProductUpdate info have updated in LicenseManager::checkUserRegistration
	ProductUpdate::GetInstance().ShowNotificationIfNeeded();

	if (i_cmdInfo.GetFlagsAndParams().size() == 1 && 0 == i_cmdInfo.CountFlags()) // Only one param, no flag. Mainly when a file is opened with sapio365 from explorer.
		m_FileToOpen = i_cmdInfo.GetParam(0);

	const bool isAutomated = !i_cmdInfo.GetJobCenterKey().empty() || !i_cmdInfo.GetScriptPath().empty();// Bug #191011.EH.00A52A: let Automation end in error if initial LoadSession failed

	const auto& commandLineSession = i_cmdInfo.GetParamForFlag(CL_OPT_SESSION_UNICODE);	
	if (!commandLineSession.empty())
	{
		m_CommandLineAutomationScriptFilePathBackup = GetCommandLineAutomationScript();
		m_CommandLineAutomationJobKeyBackup			= GetCommandLineAutomationJobCenterKey();
		ClearCommandLineAutomationJobCenterKey();
		ClearCommandLineAutomationScript();
		m_PendingAutomationForPostInit = false;

		GetCommandLineErrors().clear();

		LoadSessionFromAutomationOrCommandLine(commandLineSession, i_cmdInfo.GetParamForFlag(CL_OPT_SESSIONTYPE_UNICODE), i_cmdInfo.GetParamForFlag(CL_OPT_ROLE_UNICODE)).Then([this, isAutomated](bool success)
			{
				if (success)
				{
					ASSERT(m_SapioSessionBeingLoaded);
					if (m_SapioSessionBeingLoaded)
						m_SapioSessionBeingLoaded->SetCommandLineAutomationInProgress(isAutomated);
				}
				else if (!isAutomated)
				{
					const wstring pendingAuto	= !m_CommandLineAutomationScriptFilePathBackup.empty() ? m_CommandLineAutomationScriptFilePathBackup : GetCommandLineAutomationScript();
					const wstring pendingJob	= GetCommandLineAutomationJobCenterKey();
					m_CommandLineAutomationScriptFilePathBackup.clear();
					ClearCommandLineAutomationJobCenterKey();
					ClearCommandLineAutomationScript();
					if (!pendingAuto.empty() || !pendingJob.empty())
					{
						YCallbackMessage::DoPost([pendingAuto, pendingJob, this]()
							{
								if (!pendingAuto.empty() || !pendingJob.empty())
								{
									if (!pendingAuto.empty())
										TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("InitInstanceSpecific"), _YDUMP("No recent session loaded. '-x' option can't be honored."));
									else
										TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("InitInstanceSpecific"), _YDUMP("No recent session loaded. '-j' option can't be honored."));

									YCodeJockMessageBox msgBox(	m_pMainWnd,
																DlgMessageBox::eIcon::eIcon_Error,
																_T("Command Line error"),
																!pendingAuto.empty()	? _YFORMATERROR(L"No recent session loaded. Script '%s' can't be run.", pendingAuto.c_str())
																						: _YFORMATERROR(L"No recent session loaded. Job '%s' can't be run.", pendingJob.c_str()),
																_YTEXT(""),
																{ { IDOK, YtriaTranslate::Do(Office365AdminApp_GetConsent_3, _YLOC("OK")).c_str() } });
									msgBox.DoModal();
								}
							});
					}
				}
			});
	}
	else if (lastSessionUsed && !(*lastSessionUsed == SessionIdentifier()))
	{
		m_CommandLineAutomationScriptFilePathBackup = GetCommandLineAutomationScript();
		m_CommandLineAutomationJobKeyBackup			= GetCommandLineAutomationJobCenterKey();
		ClearCommandLineAutomationScript();
		ClearCommandLineAutomationJobCenterKey();
		m_PendingAutomationForPostInit = false;

		// Do not display warning dialog if launching automation via command line (it would block it).
		if (m_CommandLineAutomationScriptFilePathBackup.empty() && m_CommandLineAutomationJobKeyBackup.empty())
		{
			auto sessionsList = SessionsSqlEngine::Get().GetList();
			bool hasUltraAdmin = std::any_of(sessionsList.begin(), sessionsList.end(), [](const PersistentSession& p_Session) {
				return p_Session.GetSessionType() == SessionTypes::g_UltraAdmin;
			});
			if (hasUltraAdmin)
			{
				Util::ShowRestrictedAccessDialog(DlgRestrictedAccess::Image::ULTRAADMIN_DEPRECATED,
					{
						_T("Ultra Admin sessions are no longer employed."),
						_T("As of version 1.7 the Ultra Admin session type has been replaced."),
						_T("However, you will be offered the option to recycle the privileges from your previous Ultra Admin sessions, to promote your Advanced session to elevated status."),
						_T("Learn more..."),
						Product::getURLHelpWebSitePage(_YTEXT("Help/dialog-new-elevated-privileges"))
					},
					HideUltraAdminDeprecatedReminderSetting(), pFrame, true, 45, 90);
			}
		}

		LoadLastUsedSession(*lastSessionUsed).Then([this, isAutomated](bool success)
			{
				if (success)
				{
					ASSERT(m_SapioSessionBeingLoaded);
					if (m_SapioSessionBeingLoaded)
						m_SapioSessionBeingLoaded->SetCommandLineAutomationInProgress(isAutomated);
				}
				else if (!isAutomated)
				{
					const wstring pendingAuto	= !m_CommandLineAutomationScriptFilePathBackup.empty() ? m_CommandLineAutomationScriptFilePathBackup : GetCommandLineAutomationScript();
					const wstring pendingJob	= !m_CommandLineAutomationJobKeyBackup.empty() ? m_CommandLineAutomationJobKeyBackup : GetCommandLineAutomationJobCenterKey();
					m_CommandLineAutomationScriptFilePathBackup.clear();
					ClearCommandLineAutomationJobCenterKey();
					ClearCommandLineAutomationScript();
					if (!pendingAuto.empty() || !pendingJob.empty())
					{
						YCallbackMessage::DoPost([pendingAuto, pendingJob, this]()
							{
								if (!pendingAuto.empty() || !pendingJob.empty())
								{
									if (!pendingAuto.empty())
										TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("InitInstanceSpecific"), _YDUMP("No recent session loaded. '-x' option can't be honored."));
									else
										TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("InitInstanceSpecific"), _YDUMP("No recent session loaded. '-j' option can't be honored."));

									YCodeJockMessageBox msgBox(	m_pMainWnd,
																DlgMessageBox::eIcon::eIcon_Error,
																_T("Command Line error"),
																!pendingAuto.empty()	? _YFORMATERROR(L"No recent session loaded. Script '%s' can't be run.", pendingAuto.c_str())
																						: _YFORMATERROR(L"No recent session loaded. Job '%s' can't be run.", pendingJob.c_str()),
																!pendingAuto.empty()	? _T("In order to run a script using the command line option -x, you must ensure the last session is valid and automatically reloaded on launch (see Preferences).")
																						: _T("In order to run a job using the command line option -j, you must ensure the last session is valid and automatically reloaded on launch (see Preferences)."),
																{ { IDOK, YtriaTranslate::Do(Office365AdminApp_GetConsent_3, _YLOC("OK")).c_str() } });
									msgBox.DoModal();
								}
							});
					}
				}
			});
	}
	else
	{
		// As no recent session is loaded, MainFrame::sessionWasSet won't be called, and -j/-x options won't be executed.
		// Handle them (at least try to...)
		if (!GetCommandLineAutomationJobCenterKey().empty())
		{
			m_CommandLineAutomationJobKeyBackup = GetCommandLineAutomationJobCenterKey();
			ClearCommandLineAutomationJobCenterKey();
		}
		else if (!GetCommandLineAutomationScript().empty())
		{
			m_CommandLineAutomationScriptFilePathBackup = GetCommandLineAutomationScript();
			ClearCommandLineAutomationScript();
		}

		m_PendingAutomationForPostInit = true;
	}

	return RetVal;
}

void Office365AdminApp::InitProductInstance()
{
	if (!Product::isInit())
	{
		Product::initInstance(
			ProductBasicNoLicense::ProductNoLicenseFactory(), //ProductBasic::ProductBasicFactory(),
			getCurrentAppFullPath(),
			InterProductLaunch::g_sapio365.c_str(),
			CString(STRFILEVER),
			CString(STRLEGALCOPYRIGHT));
	}
}

void Office365AdminApp::PostInitInstance()
{
	// Handle -x when no last used session is reloaded

	if (m_PendingAutomationForPostInit)
	{
		// Should we?
		if (m_FileToOpen)
		{
			m_PendingAutomationForPostInit = false;
			ASSERT(m_CommandLineAutomationJobKeyBackup.empty() && m_CommandLineAutomationScriptFilePathBackup.empty());
			MainFrame* mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
			ASSERT(nullptr != mainFrame);
			if (nullptr != mainFrame)
				mainFrame->OpenFile(*m_FileToOpen);
		}
		////
		else if (!m_CommandLineAutomationJobKeyBackup.empty())
		{
			const auto jobKey = m_CommandLineAutomationJobKeyBackup;
			m_CommandLineAutomationJobKeyBackup.clear();
			m_PendingAutomationForPostInit = false;
			runCommandLineAutomation(jobKey, true);
		}
		else if (!m_CommandLineAutomationScriptFilePathBackup.empty())
		{
			const auto path = downloadFileIfNeeded(m_CommandLineAutomationScriptFilePathBackup);
			m_CommandLineAutomationScriptFilePathBackup.clear();
			m_PendingAutomationForPostInit = false;
			runCommandLineAutomation(path, false);
		}
	}
	m_PendingAutomationForPostInit = false;
}

TaskWrapper<bool> Office365AdminApp::LoadLastUsedSession(const SessionIdentifier& lastSessionUsed)
{
	auto autoLoadLastSession = AutoLoadLastSessionSetting().Get();
	if (autoLoadLastSession && *autoLoadLastSession)
	{
		auto loader = SessionLoaderFactory(lastSessionUsed).CreateLoader();
		return SessionSwitcher(std::move(loader)).Switch().Then([](const std::shared_ptr<Sapio365Session>& p_Session) {
			return nullptr != p_Session;
		});
	}

	return pplx::task_from_result<bool>(false);
}

bool Office365AdminApp::ExitInstanceSpecific()
{
	SkuAndPlanReference::DeleteInstance();

	RegexSFD::closeInstance();

	Product::releaseInstance();

	FactoryRecentOpening::DeleteInstance();

	GridUtil::ClearStatics();

	AfxOleTerm(FALSE);

	return true;
}

bool Office365AdminApp::ValidateActionsSpecific(list< AutomationAction * >& i_ActionList)
{
	bool rv = true;

	for (auto action : i_ActionList)
	{
		ASSERT(nullptr != action);
		if (nullptr != action)
		{
			if (IsActionSelectedDownload(action) || IsActionSelectedAttachmentDownload(action) || IsActionSelectedDownloadAsEML(action))
			{
				if (action->HasSetParam(g_IfFileExists))
				{
					const wstring& ife = action->GetSetParamValue(g_IfFileExists);
					if (!(MFCUtil::StringMatchW(ife, g_CreateNew) ||
						  MFCUtil::StringMatchW(ife, g_Overwrite) ||
						  MFCUtil::StringMatchW(ife, g_Skip)))
					{
						action->AddError(YtriaTranslate::DoError(Office365AdminApp_ValidateActionsSpecific_2, _YLOC("Invalid value '%1' set for parameter '%2'"),_YR("Y2529"), ife.c_str(), g_IfFileExists.c_str()).c_str());
						rv = false;
					}
				}
			}
			else if (IsActionSelectedEditUser(action) ||
					 IsActionSelectedEditGroup(action) ||
					 IsActionSelectedEditLicense(action))
			{
				if (action->GetChildrenCount(AutomationConstant::val().m_ActionNameSetParam) == 0)
				{
					action->AddError(YtriaTranslate::DoError(Office365AdminApp_ValidateActionsSpecific_1, _YLOC("No SetParam found: nothing can be edited"),_YR("Y2503")).c_str());
					rv = false;
				}
			}
			else if (IsActionCreateUser(action) ||
					 IsActionCreateGroup(action))
			{
				if (action->GetChildrenCount(AutomationConstant::val().m_ActionNameSetParam) == 0)
				{
					action->AddError(_YERROR("No SetParam found: nothing can be created"));
					rv = false;
				}
			}
			else if (IsActionGroupMembersCopy(action))
			{
				if (action->HasParam(g_ParamNameGroupAction))
				{
					const wstring& copyAction = action->GetParamValue(g_ParamNameGroupAction);
					if (!(MFCUtil::StringMatchW(copyAction, g_ValueGroupActionCopyGroups) || MFCUtil::StringMatchW(copyAction, g_ValueGroupActionCopyMembers)))
					{
						action->AddError(YtriaTranslate::DoError(Office365AdminApp_ValidateActionsSpecific_2, _YLOC("Invalid value '%1' set for parameter '%2'"),_YR("Y2530"), copyAction.c_str(), g_ParamNameGroupAction.c_str()).c_str());
						rv = false;
					}
				}
			}
			else if (IsActionGroupMembersMove(action))
			{
				if (action->HasParam(g_ParamNameGroupAction))
				{
					const wstring& copyAction = action->GetParamValue(g_ParamNameGroupAction);
					if (!(MFCUtil::StringMatchW(copyAction, g_ValueGroupActionMoveGroups) || MFCUtil::StringMatchW(copyAction, g_ValueGroupActionMoveMembers)))
					{
						action->AddError(YtriaTranslate::DoError(Office365AdminApp_ValidateActionsSpecific_2, _YLOC("Invalid value '%1' set for parameter '%2'"),_YR("Y2531"), copyAction.c_str(), g_ParamNameGroupAction.c_str()).c_str());
						rv = false;
					}
				}
			}
			else if (IsActionSelectedEditLicenseLocation(action))
			{
				const wstring& location = action->GetSetParamValue(g_ParamNameLocation);
				if (location.empty())
				{
					action->AddError(YtriaTranslate::DoError(Office365AdminApp_ValidateActionsSpecific_5, _YLOC("Missing parameter '%1'"),_YR("Y2532"), g_ParamNameLocation.c_str()).c_str());
					rv = false;
				}
			}
			else if (IsActionSelectedResetPassword(action))
			{
				const wstring& newPassword = action->GetSetParamValue(g_ParamNamePassword);
				if (newPassword.empty() && !action->IsSetParamValueTrue(g_ParamNameAutoGenerate))
				{
					action->AddError(YtriaTranslate::DoError(Office365AdminApp_ValidateActionsSpecific_5, _YLOC("Missing parameter '%1'"),_YR("Y2533"), g_ParamNamePassword.c_str()).c_str());
					rv = false;
				}
				else if (!newPassword.empty() && action->IsSetParamValueTrue(g_ParamNameAutoGenerate))
				{
					action->AddError(YtriaTranslate::DoError(Office365AdminApp_ValidateActionsSpecific_7, _YLOC("'%1' is set to true, but a value for '%2' was also supplied: '%3'"),_YR("Y2534"), g_ParamNameAutoGenerate.c_str(), g_ParamNamePassword.c_str(), newPassword.c_str()).c_str());
					rv = false;
				}

				if (action->IsSetParamValueTrue(g_ParamNameForceChangeMFA) && !action->IsSetParamValueTrue(g_ParamNameForceChange))
				{
					action->AddError(_YFORMAT(L"'%s' is set to true, but '%s' is not", g_ParamNameForceChangeMFA.c_str(), g_ParamNameForceChange.c_str()).c_str());
					rv = false;
				}
			}
			else if (IsActionCheckSession(action))
			{
				const auto& actionType = action->GetParamValue(AutomationConstant::val().m_ParamNameType);
				if (!actionType.empty() && !MFCUtil::StringMatchW(actionType, AutomationConstant::val().m_ValueTypeError))
				{
					action->AddError(_YFORMATERROR(L"Invalid %s: %s", AutomationConstant::val().m_ParamNameType.c_str(), actionType.c_str()).c_str());
					rv = false;
				}
				
				const auto& checkType	= action->GetParamValue(AutomationConstant::val().m_ParamNameValue);
				const auto requirements = Str::explodeIntoSet(checkType, g_RequirementsDelimitor);
				if (checkType.empty())
				{
					action->AddError(_YFORMATERROR(L"Missing %s", AutomationConstant::val().m_ParamNameValue.c_str()).c_str());
					rv = false;
				}

				for (auto& r : requirements)
					if (!MFCUtil::StringMatchW(Str::lrtrim(r), g_ParamValueRequiredLogin) &&
						!MFCUtil::StringMatchW(Str::lrtrim(r), g_ParamValueWarningIfAdmin) &&
						!MFCUtil::StringMatchW(Str::lrtrim(r), g_ParamValueWarningIfAdminTech) &&
						!MFCUtil::StringMatchW(Str::lrtrim(r), g_ParamValueWarningIfUltraAdminTech))
					{
						action->AddError(_YFORMATERROR(L"Invalid %s: %s", AutomationConstant::val().m_ParamNameValue.c_str(), checkType.c_str()).c_str());
						rv = false;
					}
			}
			else if (IsActionLoadSession(action))
			{
				const auto& sessionName = action->GetParamValue(AutomationConstant::val().m_ParamNameName);				
				if (!AutomationUtil::IsVariable(sessionName))// if variable, name may contain role: do not verify yet
				{
					if (sessionName.empty())
					{
						action->AddError(_YFORMATERROR(L"\"%s\" cannot be empty.", sessionName.c_str()).c_str());
						rv = false;
					}					
					
					const auto& sessionType = action->GetParamValue(AutomationConstant::val().m_ParamNameType);
					const auto& roleName	= action->GetParamValue(g_ParamNameRole);
					if (sessionType.empty() && roleName.empty())
					{
						action->AddError(_YFORMATERROR(L"\"%s\" and \"%s\" cannot be both empty.", AutomationConstant::val().m_ParamNameType.c_str(), g_ParamNameRole.c_str()).c_str());
						rv = false;
					}
					else if (roleName.empty() && (!AutomationUtil::IsVariable(sessionType) && !SessionTypes::IsValidType(sessionType)))
					{
						action->AddError(_YFORMATERROR(L"\"%s\" value is invalid: %s - valid values are: %s", AutomationConstant::val().m_ParamNameType.c_str(), sessionType.c_str(), Str::implode(SessionTypes::GetTypes(), _YTEXT(", ")).c_str()).c_str());
						rv = false;
					}
				}
			}
			else if (IsActionMainFrame(action))
			{
// 				if (action->GetMother(g_ActionNameMainframe) != nullptr)
// 				{
// 					action->AddError(_YFORMATERROR(L"Cannot have a '%s' parent action.", g_ActionNameMainframe.c_str()).c_str());
// 					rv = false;
// 				}
			}
			else if (IsSubmoduleAction(action))
			{
/* verification done at last execution time to allow the usage of variables in stead of true & false
				if (action->IsParamTrue(g_AutoCloseFrame) && !action->IsParamTrue(g_NewFrame))
				{
					action->AddError(YtriaTranslate::DoError(Office365AdminApp_ValidateActionsSpecific_8, _YLOC("%1=\"true\" is invalid without %2=\"true\""),_YR("Y2535"), g_AutoCloseFrame.c_str(), g_NewFrame.c_str()).c_str());
					rv = false;
				}
*/
			}
		}
	}

	return rv;
}

bool Office365AdminApp::IsSubmoduleAction(AutomationAction* p_Action)
{
	return
		// Users
			IsActionSelectedShowParentGroups(p_Action)
		||	IsActionSelectedShowDriveItemsUsers(p_Action)
		||	IsActionSelectedShowMessages(p_Action)
		||	IsActionSelectedShowContacts(p_Action)
		||	IsActionSelectedShowMailFolders(p_Action)
		||	IsActionSelectedShowEventsUsers(p_Action)
		||	IsActionSelectedShowManagerHierarchy(p_Action)
		||	IsActionSelectedShowLicenses(p_Action)
		||	IsActionSelectedShowMessageRules(p_Action)
		||	IsActionSelectedShowDetailsUser(p_Action)

		// Groups
		||	IsActionSelectedShowMembers(p_Action)
		||	IsActionSelectedShowOwners(p_Action)
		||	IsActionSelectedShowAuthors(p_Action)
		||	IsActionSelectedShowConversations(p_Action)
		||	IsActionSelectedShowSites(p_Action)
		||	IsActionSelectedShowDriveItemsGroups(p_Action)
		||	IsActionSelectedShowEventsGroups(p_Action)
		||	IsActionSelectedShowChannels(p_Action)
		||	IsActionSelectedShowDetailsGroup(p_Action)
		
		// Sites
		||	IsActionSelectedShowDriveItemsSites(p_Action)
		||	IsActionSelectedShowLists(p_Action)

		// Lists
		||	IsActionSelectedShowListItems(p_Action)
		||	IsActionSelectedShowListColumns(p_Action)

		// Messages
		||	IsActionShowMessageDetails(p_Action)

		// Channels
		|| IsActionSelectedShowChannelMembers(p_Action)
		|| IsActionSelectedShowChannelMessages(p_Action)
		|| IsActionSelectedShowChannelSites(p_Action)

		// Conversations
		||	IsActionSelectedShowPosts(p_Action)

		// Schools
		||	IsActionSelectedShowClassMembers(p_Action)
		;
}

bool Office365AdminApp::TranslateActionSpecific(AutomationAction* i_Action, list< AutomationAction* >& o_ActionList)
{
	bool rv = true;
	
	ASSERT(nullptr != g_automationEngine);
	ASSERT(nullptr != i_Action);
	if (nullptr != i_Action && nullptr != g_automationEngine)
	{
		bool translated = false;
		map < wstring, wstring > params;

		if (IsActionSelectedDownload(i_Action) || IsActionSelectedAttachmentDownload(i_Action) || IsActionSelectedDownloadAsEML(i_Action))
		{
			auto p = i_Action->GetParam(AutomationConstant::val().m_ParamNameFilePath);
			if (nullptr != p)
			{
				wstring folderPath = p->GetParamValue();
				if (folderPath.back() != BackSlash)
				{
					folderPath.push_back(BackSlash);
					p->SetParamValue(folderPath);
				}
			}
			generateSetParamAction(AutomationConstant::val().m_ParamNameKeepAlive,	i_Action->GetName(), i_Action, o_ActionList);// i_Action->GetName(): action name matches dlg automation name
			generateSetParamAction(AutomationConstant::val().m_ParamNameFilePath,	i_Action->GetName(), i_Action, o_ActionList);
			generateSetParamAction(g_OwnerName,										i_Action->GetName(), i_Action, o_ActionList);
			generateSetParamAction(g_Hierarchy,										i_Action->GetName(), i_Action, o_ActionList);
			generateSetParamAction(g_IfFileExists,									i_Action->GetName(), i_Action, o_ActionList);
			generateSetParamAction(g_OpenFolderAfterDownload,						i_Action->GetName(), i_Action, o_ActionList);
			//generateSetParamAction(AutomationConstant::val().m_Click, AutomationConstant::val().m_Click, i_Action->GetName(), i_Action, o_ActionList);// choose folder
			o_ActionList.push_back(i_Action);
			translated = true;
		}
		else if (IsActionSelectedEditUser(i_Action)
				|| IsActionSelectedEditGroup(i_Action)
				|| IsActionSelectedEditLicense(i_Action)
				|| IsActionCreateUser(i_Action)
				|| IsActionCreateGroup(i_Action))
		{
			for (auto c : i_Action->GetChildren())
			{
				if (IsActionSetParam(c) && !c->HasParam(AutomationConstant::val().m_ParamNameTarget))
					c->AddParam(AutomationConstant::val().m_ParamNameTarget, i_Action->GetName());// action names are dlg automation target names
			}
			generateSetParamAction(AutomationConstant::val().m_ParamNameKeepAlive, i_Action->GetName(), i_Action, o_ActionList);
		}
		else if (IsActionGroupMembersAdd(i_Action) || IsActionGroupMembersCopy(i_Action) || IsActionGroupMembersMove(i_Action) || 
				 IsActionGroupMembershipAdd(i_Action) || IsActionGroupMembershipCopy(i_Action) || IsActionGroupMembershipTransfer(i_Action) ||
				 IsActionGroupSendersAddAccepted(i_Action) || IsActionGroupSendersAddRejected(i_Action) ||
				 IsActionOwnerAdd(i_Action))
		{
			o_ActionList.push_back(i_Action);

			generateSetParamAction(AutomationConstant::val().m_ParamNameKeepAlive, DlgAddItemsToGroups::g_AutomationName, i_Action, o_ActionList);

			if (i_Action->IsParamTrue(g_ParamNameLoadDirectory))
				generateSetParamAction(g_ParamNameLoadDirectory, AutomationConstant::val().m_Click, DlgAddItemsToGroups::g_AutomationName, i_Action, o_ActionList);

			auto setParam	= generateSetParamAction(DlgAddItemsToGroups::g_AutomationNameSelectionGrid, _YTEXT(""), DlgAddItemsToGroups::g_AutomationName, i_Action, o_ActionList);
			auto select		= g_automationEngine->AddAction(AutomationConstant::val().m_ActionNameSelect, 
															{	{ AutomationConstant::val().m_ParamNameTarget,	GridAvailableUsers::g_AutomationName },
																{ AutomationConstant::val().m_ParamNameLines,	AutomationConstant::val().m_ValueByValue } }, 
															o_ActionList, setParam);

			for (auto c : i_Action->GetChildren())
			{
				if (IsActionSelect(c))
				{
					g_automationEngine->AddAction(AutomationConstant::val().m_ActionNameSetParam, 
													{	{ AutomationConstant::val().m_ParamNameTarget,		GridAvailableUsers::g_AutomationName },
														{ AutomationConstant::val().m_ParamNameColumnID,	c->GetParamValue(AutomationConstant::val().m_ParamNameColumnID) },
														{ AutomationConstant::val().m_ParamNameValue,		c->GetParamValue(AutomationConstant::val().m_ParamNameValue) } },
													o_ActionList, select);
					c->SetIsExecuted(true);
				}
				else if (!IsActionSetParam(c) && !IsGridAction(c))
				{
					i_Action->AddError(YtriaTranslate::DoError(Office365AdminApp_TranslateActionSpecific_2, _YLOC("Ignored invalid sub-action: %1"),_YR("Y2536"), c->toString().GetBuffer()).c_str(), TraceOutput::TRACE_WARNING);
				}
			}

			//  cf GridGroupsHierarchy::copySelectedMembersToGroups
			int CJbtnID = 0;
			const wstring& groupAction = i_Action->GetParamValue(g_ParamNameGroupAction);
			if (MFCUtil::StringMatchW(groupAction, g_ValueGroupActionMoveMembers) || MFCUtil::StringMatchW(groupAction, g_ValueGroupActionCopyMembers))
				CJbtnID = IDYES;
			else if (MFCUtil::StringMatchW(groupAction, g_ValueGroupActionMoveGroups) || MFCUtil::StringMatchW(groupAction, g_ValueGroupActionCopyGroups))
				CJbtnID = IDOK;
			if (0 != CJbtnID)
				i_Action->AddParam(AutomationConstant::val().m_ParamNameCJDialogChoice, Str::getStringFromNumber(CJbtnID));

			if (!select->HasChildren())
				i_Action->AddError(YtriaTranslate::DoError(Office365AdminApp_TranslateActionSpecific_1, _YLOC("No selection found"),_YR("Y2504")).c_str());
		
			translated = true;
		}
		else if (IsActionShowUnassignedLicenses(i_Action) || IsActionShowServicePlans(i_Action))
		{
			if (!i_Action->HasParam(AutomationConstant::val().m_ParamNameValue))
				i_Action->AddParam(AutomationConstant::val().m_ParamNameValue, AutomationConstant::val().m_ValueTrue);
		}
		else if (IsActionSelectedEditLicenseLocation(i_Action))
		{
			o_ActionList.push_back(i_Action);

			generateSetParamAction(AutomationConstant::val().m_ParamNameKeepAlive, i_Action->GetName(), i_Action, o_ActionList);// i_Action->GetName(): action name matches dlg automation name
			generateSetParamAction(g_ParamNameLocation, i_Action->GetName(), i_Action, o_ActionList);// i_Action->GetName(): action name matches dlg automation name

			translated = true;
		}
		else if (IsActionSelectedResetPassword(i_Action))
		{
			o_ActionList.push_back(i_Action);

			generateSetParamAction(AutomationConstant::val().m_ParamNameKeepAlive, i_Action->GetName(), i_Action, o_ActionList);
			generateSetParamAction(g_ParamNamePassword, i_Action->GetName(), i_Action, o_ActionList);
			generateSetParamAction(g_ParamNameAutoGenerate, i_Action->GetName(), i_Action, o_ActionList);
			generateSetParamAction(g_ParamNameForceChange, i_Action->GetName(), i_Action, o_ActionList);
			generateSetParamAction(g_ParamNameForceChangeMFA, i_Action->GetName(), i_Action, o_ActionList);

			if (i_Action->HasSetParam(g_ParamNamePassword) && !i_Action->HasSetParam(g_ParamNameAutoGenerate))
				generateSetParamAction(g_ParamNameAutoGenerate, AutomationConstant::val().m_ValueFalse, i_Action->GetName(), i_Action, o_ActionList);

			if (i_Action->HasSetParam(g_ParamNamePassword) || i_Action->HasSetParam(g_ParamNameAutoGenerate))
				generateSetParamAction(g_ParamNameCreatePassword, AutomationConstant::val().m_ValueTrue, i_Action->GetName(), i_Action, o_ActionList);
			else
				generateSetParamAction(g_ParamNameCreatePassword, AutomationConstant::val().m_ValueFalse, i_Action->GetName(), i_Action, o_ActionList);

			translated = true;
		}
		else if (IsActionShowUsageReport(i_Action))
		{
			generateSetParamAction(AutomationConstant::val().m_ParamNameKeepAlive, i_Action->GetName(), i_Action, o_ActionList);
			DlgSelectO365Report::TranslateAction(this, i_Action, o_ActionList);
			translated = true;
		}
		else if (IsActionShowMyDataMail(i_Action)
				|| IsActionShowMyDataMailFolder(i_Action)
				|| IsActionShowMyDataCalendar(i_Action)
				|| IsActionSelectedShowMessages(i_Action)
				|| IsActionSelectedShowMailFolders(i_Action)
				|| IsActionSelectedShowEventsUsers(i_Action)
				|| IsActionSelectedShowEventsGroups(i_Action)
				|| IsActionShowSignIns(i_Action)
				|| IsActionShowAuditLogs(i_Action)

				|| IsActionShowUsers(i_Action)
				|| IsActionShowGroups(i_Action)
				)
		{
			generateSetParamAction(AutomationConstant::val().m_ParamNameKeepAlive, i_Action->GetName(), i_Action, o_ActionList);
			DlgModuleOptions::TranslateAction(this, i_Action, o_ActionList);
			translated = true;
		}
		else if (IsActionLoadSession(i_Action))
		{
			const auto& sessionName = i_Action->GetParamValue(AutomationConstant::val().m_ParamNameName);
			if (!sessionName.empty() && i_Action->GetParamValue(g_ParamNameRole).empty())
			{
				auto nameAndRole = Str::explodeIntoVector(sessionName, g_O365_ListItemSessionRoleSeparator);
				if (nameAndRole.size() == 2)
				{
					i_Action->SetParam(AutomationConstant::val().m_ParamNameName, nameAndRole[0]);
					i_Action->SetParam(g_ParamNameRole, nameAndRole[1]);
					o_ActionList.push_back(i_Action);
					translated = true;
				}
			}
		}

		// else keep original action
		if (!translated)
			o_ActionList.push_back(i_Action);

		// error check
		for (list< AutomationAction * >::iterator itAction = o_ActionList.begin(); rv && itAction != o_ActionList.end(); itAction++)
			rv = !(*itAction)->HasErrors(TraceOutput::TRACE_ERROR);
	}
	else
		rv = false;

	return rv;
}

void Office365AdminApp::TranslatePostProcessSpecific(list< AutomationAction * >& i_ActionList)
{
	// TODO:
}

bool Office365AdminApp::IsFilePathParamForFolderSpecific(AutomationAction * io_Action)
{
	return 	IsActionSelectedDownload(io_Action) || IsActionSelectedAttachmentDownload(io_Action) || IsActionSelectedDownloadAsEML(io_Action);
}

bool Office365AdminApp::IsModalActionSpecific(AutomationAction* i_Action)
{
	return	IsActionSelectedAttachmentDownload(i_Action) ||
			IsActionSelectedDownload(i_Action) ||
			IsActionSelectedDownloadAsEML(i_Action) ||
			IsActionGroupMembersAdd(i_Action) ||
			IsActionGroupMembersCopy(i_Action) ||
			IsActionGroupMembersMove(i_Action) ||
			IsActionGroupMembershipAdd(i_Action) ||
			IsActionGroupMembershipCopy(i_Action) ||
			IsActionGroupMembershipTransfer(i_Action) ||
			IsActionGroupSendersAddAccepted(i_Action) ||
			IsActionGroupSendersAddRejected(i_Action) ||
			IsActionOwnerAdd(i_Action);
}

void Office365AdminApp::downloadAutomationFileCallback(const wstring& p_Message) const
{
	MainFrame* mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		mainFrame->DownloadAutomationFileCallback(p_Message);
}

std::pair<XTPPaintTheme, wstring> Office365AdminApp::getDefaultCodejockTheme() const
{
	return { xtpThemeOffice2013, _YTEXT("YTRIAPURPLE_INI") };
}

void Office365AdminApp::SetAutomationRulesSpecific()
{
	// Keep it sorted on action name for easy maintenance
	for (auto& showActionName : {	
									g_ActionNameMyDataCalendar,
									g_ActionNameMyDataContacts,
									g_ActionNameMyDataDrive,
									g_ActionNameMyDataMail,
									g_ActionNameMyDataMailFolder,
									g_ActionNameShowAuditLogs,
									g_ActionNameShowDirectoryRoles,
									g_ActionNameShowGroups,
									g_ActionNameShowGroupsRecycleBin,
									g_ActionNameShowLicenses,
									g_ActionNameShowSchools,
									g_ActionNameShowSignIns,
									g_ActionNameShowSites,
									g_ActionNameShowUsageReport,
									g_ActionNameShowUsers,
									g_ActionNameShowUsersRecycleBin,
								})
	{
		g_rules.AddParam(showActionName, AutomationConstant::val().m_ParamNameKeepAlive);
	}

	// Prefilter
	for (auto& actionName : {
								g_ActionNameShowGroups,
								g_ActionNameShowUsers,
							})
	{
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamFilter);
	}

	// Keep it sorted on action name for easy maintenance
	for (auto& selectedShowActionName : {
											g_ActionNameSelectedShowAuthors,
											g_ActionNameSelectedShowChannels,
											g_ActionNameSelectedShowContacts,
											g_ActionNameSelectedShowConversations,
											g_ActionNameSelectedShowDetailsGroups,
											g_ActionNameSelectedShowDetailsUsers,
											g_ActionNameSelectedShowDriveItemsGroups,
											g_ActionNameSelectedShowDriveItemsSites,
											g_ActionNameSelectedShowDriveItemsUsers,
											g_ActionNameSelectedShowEventsGroups,
											g_ActionNameSelectedShowEventsUsers,
											g_ActionNameSelectedShowLicenses,
											g_ActionNameSelectedShowListColumns,
											g_ActionNameSelectedShowListItems,
											g_ActionNameSelectedShowLists,
											g_ActionNameSelectedShowMailFolders,
											g_ActionNameSelectedShowMembers,
											g_ActionNameSelectedShowMessageRules,
											g_ActionNameSelectedShowMessages,
											g_ActionNameSelectedShowChannelMembers,
											g_ActionNameSelectedShowChannelsMessages,											
											g_ActionNameSelectedShowChannelSites,
											g_ActionNameSelectedShowMessagesDetails,
											g_ActionNameSelectedShowOwners,
											g_ActionNameSelectedShowParentGroups,
											g_ActionNameSelectedShowPost,
											g_ActionNameSelectedShowSites,
										})
	{
		g_rules.AddParam(selectedShowActionName, g_NewFrame);
		g_rules.AddParam(selectedShowActionName, AutomationConstant::val().m_ParamNameKeepAlive);
		g_rules.AddParam(selectedShowActionName, g_ColumnTransport);
	}

	// Messages Module options
	for (const auto& actionName :	{
										g_ActionNameMyDataMail,
										g_ActionNameSelectedShowMessages,
									})
	{
		g_rules.AddParam(actionName, g_ParamNameCutOff);
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamEmails);
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamChats);
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamSoftDeletedFolders);
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamBodyPreview);
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamFullBody);
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamMailHeaders);
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamFilter);
	}

	// Events Module options
	for (const auto& actionName :	{
										g_ActionNameMyDataCalendar,
										g_ActionNameSelectedShowEventsGroups,
										g_ActionNameSelectedShowEventsUsers,
									})
	{
		g_rules.AddParam(actionName, g_ParamNameCutOff);
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamBodyPreview);
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamFullBody);
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamFilter);
	}

	// Audit / Signins
	for (const auto& actionName :	{
										g_ActionNameShowSignIns,
										g_ActionNameShowAuditLogs,
									})
	{
		g_rules.AddParam(actionName, g_ParamNameCutOff);
		g_rules.AddParam(actionName, DlgModuleOptions::g_AutoParamFilter);
	}

	g_rules.AddParam(g_ActionNameMainframe, AutomationConstant::val().m_ParamNameEmpty);

	g_rules.AddParam(g_ActionNameShowUsageReport, AutomationConstant::val().m_ParamNameName);
	g_rules.AddParam(g_ActionNameShowUsageReport, g_ParamNamePeriodOrDate);
	g_rules.AddParam(g_ActionNameShowUsageReport, g_ParamNameReportFile);
	g_rules.AddParam(g_ActionNameShowUsageReport, g_ParamNameAdditionalInfo);

	g_rules.AddParam(g_ActionNameSelectedUserLoadMore,		AutomationConstant::val().m_ParamNameEmpty);
    g_rules.AddParam(g_ActionNameSelectedGroupLoadMore,		AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameSelectedSiteLoadMore,		AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameSelectedChannelLoadMore,	AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameSelectedViewPermissions,	AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameSelectedDeletePermissions, AutomationConstant::val().m_ParamNameEmpty);

	for (auto& action : { g_ActionNameSelectedDownload
						, g_ActionNameSelectedAttachmentDownload
						, g_ActionNameSelectedDownloadAsEML })
	{
		g_rules.AddParam(action,				AutomationConstant::val().m_ParamNameFilePath);
		g_rules.AddParam(action,				g_IfFileExists);
		g_rules.AddParam(action,				g_OwnerName);
		g_rules.AddParam(action,				g_Hierarchy);
		g_rules.AddParam(action,				g_OpenFolderAfterDownload);
		g_rules.AddParam(action,				AutomationConstant::val().m_ParamNameKeepAlive);
	}

	g_rules.AddParam(DlgDownloadAttachments::m_ActionNameGridDownloadAttachmentsDefault, AutomationConstant::val().m_ValueEmpty);

	g_rules.AddParam(g_ActionNameSelectedEditUser,				AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameSelectedEditGroup,				AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameSelectedEditLicense,			AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameSelectedEditLicenseLocation,	AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameSelectedEditLicenseLocation,	g_ParamNameLocation);

	g_rules.AddParam(g_ActionNameCreateUser,					AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameCreateGroup,					AutomationConstant::val().m_ParamNameKeepAlive);

	g_rules.AddParam(AutomationConstant::val().m_ActionNameSave,			AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(AutomationConstant::val().m_ActionNameSaveSelected,	AutomationConstant::val().m_ParamNameKeepAlive);

	g_rules.AddParam(g_ActionNameSelectedView,			AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameSelectedDelete,		AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameSelectedRestore,		AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameSelectedRemoveFrom,	AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameSelectedAddTo,			AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameSelectedMoveTo,		AutomationConstant::val().m_ParamNameEmpty);

	g_rules.AddParam(g_ActionNameSelectedPreviewBody,			AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameSelectedPreviewItem,			AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameSelectedAttachmentLoadInfo,	AutomationConstant::val().m_ParamNameEmpty);

	g_rules.AddParam(g_ActionNameSelectedAttachmentDelete,		AutomationConstant::val().m_ParamNameEmpty);

	g_rules.AddParam(g_ActionNameShowSystemLists,			AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameHideSystemLists,			AutomationConstant::val().m_ParamNameEmpty);

	g_rules.AddParam(g_ActionNameRefreshAll,		AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameRefreshSelected,	AutomationConstant::val().m_ParamNameEmpty);

	g_rules.AddParam(g_ActionNameLoadSession, AutomationConstant::val().m_ParamNameName);
	g_rules.AddParam(g_ActionNameLoadSession, AutomationConstant::val().m_ParamNameType);
	g_rules.AddParam(g_ActionNameLoadSession, g_ParamNameRole);

	g_rules.AddParam(g_ActionNameShowColumnsDefault,	AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameShowColumnsAll,		AutomationConstant::val().m_ParamNameEmpty);

	g_rules.AddParam(g_ActionNameGroupMemberRemove, AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameGroupMemberAdd,	AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameGroupMemberCopy,	AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameGroupMemberMove,	AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameGroupMemberAdd,	g_ParamNameLoadDirectory);
	g_rules.AddParam(g_ActionNameGroupMemberCopy,	g_ParamNameLoadDirectory);
	g_rules.AddParam(g_ActionNameGroupMemberMove,	g_ParamNameLoadDirectory);

	g_rules.AddParam(g_ActionNameGroupMemberCopy,	g_ParamNameGroupAction);
	g_rules.AddParam(g_ActionNameGroupMemberMove,	g_ParamNameGroupAction);
	g_rules.AddParam(g_ActionNameGroupMemberCopy,	AutomationConstant::val().m_ParamNameCJDialogChoice);
	g_rules.AddParam(g_ActionNameGroupMemberMove,	AutomationConstant::val().m_ParamNameCJDialogChoice);

	g_rules.AddParam(g_ActionNameGroupMembershipRemove,		AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameGroupMembershipAdd,		AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameGroupMembershipCopy,		AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameGroupMembershipTransfer,	AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameGroupMembershipAdd,		g_ParamNameLoadDirectory);
	g_rules.AddParam(g_ActionNameGroupMembershipCopy,		g_ParamNameLoadDirectory);
	g_rules.AddParam(g_ActionNameGroupMembershipTransfer,	g_ParamNameLoadDirectory);

	g_rules.AddParam(g_ActionNameGroupOwnerRemove,	AutomationConstant::val().m_ParamNameEmpty);
	g_rules.AddParam(g_ActionNameGroupOwnerAdd,		AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameGroupOwnerAdd,		g_ParamNameLoadDirectory);

	g_rules.AddParam(g_ActionNameGroupSenderAddAccepted,	AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameGroupSenderAddRejected,	AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameGroupSenderAddAccepted,	g_ParamNameLoadDirectory);
	g_rules.AddParam(g_ActionNameGroupSenderAddRejected,	g_ParamNameLoadDirectory);

	g_rules.AddParam(g_ActionNameShowUnassignedLicenses,	AutomationConstant::val().m_ParamNameValue);
	g_rules.AddParam(g_ActionNameShowServicePlans,			AutomationConstant::val().m_ParamNameValue);

	g_rules.AddParam(g_ActionNameSelectedResetPassword,		AutomationConstant::val().m_ParamNameKeepAlive);
	g_rules.AddParam(g_ActionNameSelectedResetPassword,		g_ParamNamePassword);
	g_rules.AddParam(g_ActionNameSelectedResetPassword,		g_ParamNameAutoGenerate);
	g_rules.AddParam(g_ActionNameSelectedResetPassword,		g_ParamNameForceChange);
	g_rules.AddParam(g_ActionNameSelectedResetPassword,		g_ParamNameForceChangeMFA);
	//g_rules.AddParam(g_ActionNameSelectedResetPassword,		g_ParamNameCreatePassword);

	g_rules.AddParam(g_SetCurrentSessionToBeSetInMainFrameAfterScriptCompletion, AutomationConstant::val().m_ParamNameValue);
	g_rules.AddActionAlias(g_SetCurrentSessionToBeRestoredAfterScriptCompletion_DEPRECATED, g_SetCurrentSessionToBeSetInMainFrameAfterScriptCompletion, true);

	g_rules.AddParam(g_ActionNameSelectedRevokeUserAccess, AutomationConstant::val().m_ValueEmpty);

	g_rules.AddParam(g_ActionNameSelectedLoadCheckoutAndRetention, AutomationConstant::val().m_ValueEmpty);

	g_rules.AddParam(g_ActionNameSetNestedGroup, AutomationConstant::val().m_ParamNameValue);
}

void Office365AdminApp::createUserSession(const wstring& p_SessionType, const boost::YOpt<Command>& p_Command, const wstring& p_PreferredLogin/* = Str::g_EmptyString*/, const wstring& p_CustomTenant/* = Str::g_EmptyString*/)
{
    MainFrame* mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
    ASSERT(nullptr != mainFrame);
    if (nullptr != mainFrame
		&& Util::ShowRestrictedAccessDialog(
			DlgRestrictedAccess::CONSENT_REMINDER
			, { YtriaTranslate::Do(Office365AdminApp_createUserSession_1, _YLOC("Reminder: If this is the first time, you will be asked to give 'app consent'.")).c_str()
				, YtriaTranslate::Do(Office365AdminApp_createUserSession_2, _YLOC("Rest assured that your information will never pass through any external servers, and will not be accessible to any other instances of sapio365.")).c_str()
				, YtriaTranslate::Do(Office365AdminApp_createUserSession_3, _YLOC("Note that this consent cannot overwrite your tenant rights. You can only authorise sapio365 to access information you have the rights to access. ")).c_str()
				, YtriaTranslate::Do(DlgRoleEditKey_OnNewPostedURL_4, _YLOC("(more...)")).c_str()
				, Product::getURLHelpWebSitePage(_YTEXT("Help/give-consent")) }
			, HideConsentReminderDialogSetting()
			, mainFrame))
    {
		auto sapioSession = std::make_shared<Sapio365Session>();
        sapioSession->SetGraphCache(std::make_unique<GraphCache>(sapioSession));
		m_SapioSessionBeingLoaded = sapioSession;

        wstring appId = SessionTypes::GetAppId(p_SessionType);
        auto newAuthenticator = std::make_unique<AzureADOAuth2BrowserAuthenticator>(sapioSession->GetMainMSGraphSession(),
			Rest::GetMsGraphEndpoint(),
            _YTEXT(""),
            appId,
            SessionTypes::GetUserRedirectUri(p_SessionType),
            std::make_shared<DlgBrowserOpener>(),
			!p_CustomTenant.empty() ? p_CustomTenant : _YTEXT("common"));

		newAuthenticator->SetPreferredLogin(p_PreferredLogin);

		sapioSession->GetMainMSGraphSession()->SetAuthenticator(std::move(newAuthenticator));
        sapioSession->SetAppId(appId);

		m_SessionBeingLoadedIdentifier = SessionIdentifier();
		m_SessionBeingLoadedIdentifier->m_SessionType = p_SessionType;
		m_SessionBeingLoadedIdentifier->m_RoleID = 0;
		SetIsCreatingSession(true);

        const wstring oldSessionType = p_SessionType;
        m_SessionType = p_SessionType;
        
        sapioSession->GetMainMSGraphSession()->CreateSession(mainFrame).ThenByTaskMutable([this, oldSessionType, mainFrame, cmd = p_Command, sapioSession](pplx::task<RestAuthenticationResult> createTask) mutable
        {
			bool success = false;
            try
            {
                auto result = createTask.get();
                if (result.state == RestAuthenticationResult::State::Failure)
                {
                    std::lock_guard<std::mutex> lock(m_SessionTypeLock);
                    m_SessionType = oldSessionType;
					success = false;
                    ResetSapioSessionBeingLoaded();
					SetIsCreatingSession(false);
                }
                else // Success
                {
                    if (cmd)
                    {
                        if (nullptr != mainFrame)
						{
							mainFrame->WaitSyncCacheEvent();
							cmd->SetLicenseContext(mainFrame->GetLicenseContext());
							cmd->SetSessionIdentifier(mainFrame->GetSessionInfo().GetIdentifier());
						}

                        YCallbackMessage::DoPost([this, cmd]()
                        {
                            CommandDispatcher::GetInstance().Execute(*cmd);
                        });
                    }
					success = true;
                }
            }
            catch (const std::exception& e)
            {
				success = false;
                LoggerService::Debug(YtriaTranslate::DoError(Office365AdminApp_createUserSession_5, _YLOC("CreateSession: An error occurred while creating new session: %1"),_YR("Y2537"),
					Str::convertFromUTF8(e.what()).c_str()));
            }
		});
    }
}

bool Office365AdminApp::automationBlockInFullAdmin(AutomationAction* i_Action)
{
	bool rvBlocked = false;

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	ASSERT(nullptr != i_Action);
	if (mainFrame->GetSessionInfo().IsUltraAdmin() && nullptr != i_Action)
	{
		if (MFCUtil::StringMatchW(i_Action->GetName().substr(0, g_ActionShowMyDataPrefix.size()), g_ActionShowMyDataPrefix))
		{
			i_Action->AddError(YtriaTranslate::DoError(Office365AdminApp_automationBlockInFullAdmin_1, _YLOC("Automation cannot run in an Ultra Admin session."),_YR("Y2505")).c_str());
			SetAutomationGreenLight(i_Action);
			rvBlocked = true;
		}
	}
	
	return rvBlocked;
}

void Office365AdminApp::CreateStandardSession(const boost::YOpt<Command>& p_Command, const wstring& p_PreferredLogin/* = Str::g_EmptyString*/)
{
    createUserSession(SessionTypes::g_StandardSession, p_Command, p_PreferredLogin);
}

void Office365AdminApp::SetShowGridErrors(bool p_ShowErrors)
{
    m_ShowGridErrors = p_ShowErrors;
}

bool Office365AdminApp::GetShowGridErrors()
{
    return m_ShowGridErrors;
}

bool Office365AdminApp::automationCheckSession(AutomationAction* p_Action)
{
	// similar to AutomationWizardO365::scriptAuthorized
	// modifications needed in this code should be copied to AutomationWizardO365::scriptAuthorized
	// TODO refactoring 

	bool rvAuthorized = true;

	ASSERT(IsActionCheckSession(p_Action));
	if (IsActionCheckSession(p_Action))
	{
		const auto setAsError	= MFCUtil::StringMatchW(p_Action->GetParamValue(AutomationConstant::val().m_ParamNameType), AutomationConstant::val().m_ValueTypeError);
		const auto requirements	= Str::explodeIntoSet(p_Action->GetParamValue(AutomationConstant::val().m_ParamNameValue), g_RequirementsDelimitor);
		rvAuthorized = AutomationWizardO365::CheckRequirements(requirements, !setAsError, AutomatedApp::GetCurrentContext(), GetAutomationScript(), false, p_Action);

		SetAutomationGreenLight(p_Action);
	}

	return rvAuthorized;
}

bool Office365AdminApp::automationSetCurrentSessionToBeSetInMainFrameAfterScriptCompletion(AutomationAction* p_Action)
{
	bool rv = false;

	ASSERT(IsSetCurrentSessionToBeSetInMainFrameAfterScriptCompletion(p_Action));
	if (IsSetCurrentSessionToBeSetInMainFrameAfterScriptCompletion(p_Action))
	{
		if (!p_Action->HasParam(AutomationConstant::val().m_ParamNameValue) || p_Action->IsParamTrue(AutomationConstant::val().m_ParamNameValue))
			m_automationSessionToRestoreInMainFrameAfterScriptCompletion = MainFrameSapio365Session();
		// Should we?
		/*else if (p_Action->IsParamValueEqual(AutomationConstant::val().m_ParamNameValue, AutomationConstant::val().m_ValueFalse))
			m_automationSessionToRestoreInMainFrameAfterScriptCompletion.reset();*/

		rv = true;
	}

	SetAutomationGreenLight(p_Action);

	return rv;
}

AutomatedApp::AUTOMATIONSTATUS Office365AdminApp::ExecuteAutomationAction(AutomationAction* i_Action)
{
	AutomatedApp::AUTOMATIONSTATUS rvStatus = AUTOMATIONSTATUS_ERROR;

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);

	ASSERT(nullptr != mainFrame);
	ASSERT(nullptr != i_Action);
	
	if (nullptr != mainFrame && !automationBlockInFullAdmin(i_Action))
	{
		std::unique_ptr<IBrowserLinkHandler> handler;
		auto licenseContext = mainFrame->GetLicenseContext();// copy
		if (IsActionLoadSession(i_Action))
		{
			if (LoadSessionFromAutomationOrCommandLine(i_Action->GetParamValue(AutomationConstant::val().m_ParamNameName), i_Action->GetParamValue(AutomationConstant::val().m_ParamNameType), i_Action->GetParamValue(g_ParamNameRole), i_Action).get())
				rvStatus = AUTOMATIONSTATUS_OK;
			else
				i_Action->AddError(_YFORMATERROR(L"Unable to load session %s", i_Action->GetParamValue(AutomationConstant::val().m_ParamNameName).c_str()).c_str());
		}
		else if (IsActionCheckSession(i_Action))
		{
			if (automationCheckSession(i_Action))
				rvStatus = AUTOMATIONSTATUS_OK;
		}
		else if (IsSetCurrentSessionToBeSetInMainFrameAfterScriptCompletion(i_Action))
		{
			if (automationSetCurrentSessionToBeSetInMainFrameAfterScriptCompletion(i_Action))
				rvStatus = AUTOMATIONSTATUS_OK;
		}
		else if (IsActionMainFrame(i_Action))
		{
			auto mfAction = mainFrame->GetAutomationActionMother();
			mainFrame->SetMotherAutomationAction(nullptr);
			mainFrame->SetMotherAutomationAction(i_Action);
			mainFrame->Automate();
			mainFrame->SetMotherAutomationAction(mfAction);
			if (!i_Action->HasErrors(TraceOutput::TRACE_ERROR))
				rvStatus = AUTOMATIONSTATUS_OK;
		}
		else if (handler = getLinkHandler(i_Action))
		{
			SetAutomationLastLoadingError(_YTEXT(""));
			i_Action->SetSourceWnd(AfxGetApp()->m_pMainWnd);
			handler->Handle(YBrowserLink(licenseContext, _YTEXT(""), i_Action));
			if (!i_Action->HasErrors(TraceOutput::TRACE_ERROR))
				rvStatus = AUTOMATIONSTATUS_OK;
		}
		// contextual actions
		else if (nullptr != i_Action) // action for most recent frame
		{
			AutomationContext currentContext	= AutomatedApp::GetCurrentContext();
			CodeJockFrameBase* frame			= currentContext.GetFrame();
			i_Action->SetSourceWnd(frame);
			ASSERT(nullptr != frame);
			if (nullptr != frame)
				rvStatus = frame->ExecuteAction(i_Action);
			if (AUTOMATIONSTATUS_ERROR == rvStatus)
				i_Action->AddError(_YERROR("The command cannot be executed in this context."));
		}
	}

	return rvStatus;
}

void Office365AdminApp::RegisterForPostExecutionProcess(AutomationAction* p_MotherAction)
{
	m_Coagulator.insert(p_MotherAction);
}

void Office365AdminApp::runCommandLineAutomation(const wstring& p_Path, bool p_IsJob)
{
	// cannot start command-line automation before sign-in
	MainFrame* mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
	{
		ASSERT(!(!p_Path.empty() && p_IsJob && !GetCommandLineAutomationJobCenterKey().empty() && p_Path != GetCommandLineAutomationJobCenterKey()));
		ASSERT(!(!p_Path.empty() && !p_IsJob && !GetCommandLineAutomationScript().empty() && p_Path != GetCommandLineAutomationScript()));
		const wstring& jobId		= !p_Path.empty() && p_IsJob	? p_Path : GetCommandLineAutomationJobCenterKey();
		const wstring& scriptPath	= !p_Path.empty() && !p_IsJob	? p_Path : GetCommandLineAutomationScript();
		ClearCommandLineAutomationJobCenterKey();
		ClearCommandLineAutomationScript();

		if (!jobId.empty())
		{
			ASSERT(!IsAutomationRunning());
			if (!IsAutomationRunning())
			{
				ASSERT(nullptr != mainFrame->GetAutomationWizard());
				if (nullptr != mainFrame->GetAutomationWizard())
				{
					mainFrame->GetAutomationWizard()->Run(jobId, GetAutomationPreRunJobCenterKey());
					JobComplete();
				}
			}
		}
		else if (!scriptPath.empty())
		{
			ASSERT(!IsAutomationRunning());
			if (!IsAutomationRunning())
			{
				SetAutomationContext(mainFrame->GetAutomationContext());
				Automate(scriptPath, true, true);
			}
		}

		GetCommandLineErrors().clear();
	}
}

void Office365AdminApp::RunPendingCommandLineAutomation()
{
	ASSERT(!m_PendingAutomationForPostInit);
	// Should we?
	if (m_FileToOpen)
	{
		ASSERT(m_CommandLineAutomationJobKeyBackup.empty() && m_CommandLineAutomationScriptFilePathBackup.empty());
		MainFrame* mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
		ASSERT(nullptr != mainFrame);
		if (nullptr != mainFrame)
			mainFrame->OpenFile(*m_FileToOpen);
	}
	////
	else if (!m_CommandLineAutomationScriptFilePathBackup.empty())
	{
		const auto path = m_CommandLineAutomationScriptFilePathBackup;
		m_CommandLineAutomationScriptFilePathBackup.clear();
		runCommandLineAutomation(path, false);		
	}
	else if (!m_CommandLineAutomationJobKeyBackup.empty())
	{
		const auto jobKey = m_CommandLineAutomationJobKeyBackup;
		m_CommandLineAutomationJobKeyBackup.clear();
		runCommandLineAutomation(jobKey, true);
	}
}

void Office365AdminApp::AutomationPostExecuteActionProcess(bool p_AutomationCanceled)
{
	for (auto a : m_Coagulator)
		cleanUpAfterExecuteAction(a, p_AutomationCanceled);
	m_Coagulator.clear();
}

void Office365AdminApp::cleanUpAfterExecuteAction(AutomationAction* i_Action, bool p_AutomationCanceled)
{
	ASSERT(nullptr != i_Action && i_Action->IsExecuted());
	if (nullptr != i_Action && i_Action->IsExecuted())
	{
		GridFrameBase* gfb = nullptr != i_Action->GetTargetWnd() && ::IsWindow(i_Action->GetTargetWnd()->m_hWnd)	? dynamic_cast<GridFrameBase*>(i_Action->GetTargetWnd())
																													: nullptr;
		if (nullptr != gfb)
		{	
			gfb->AutomationCleanup();

			if (!p_AutomationCanceled)
			{
				// !newframe case is handled in GridFrameBase::postAutomateSpecific
				const bool newFrame = !Office365AdminApp::IsSubmoduleAction(i_Action) || i_Action->IsParamTrue(g_NewFrame);
				if (newFrame)
				{
					AutomationEngine* engine = GetAutomationEngine();
					const bool keepAlive = i_Action->HasParam(AutomationConstant::val().m_ParamNameKeepAlive)
						? i_Action->IsParamTrue(AutomationConstant::val().m_ParamNameKeepAlive) || i_Action->IsParamThis(AutomationConstant::val().m_ParamNameKeepAlive)
						: true; // (nullptr != engine && engine->IsKeepAlive()) || !i_Action->HasParam(AutomationConstant::val().m_ParamNameKeepAlive) // For new frame, keepalive defaults to "this".
					if (!keepAlive)
						gfb->CloseFrame();
				}
			}
		}
	}
}

void Office365AdminApp::automationCleanup()
{
	for (const auto& byUser : CommandDispatcher::GetInstance().GetAllModules())
	{
		const auto& user = byUser.first;
		for (const auto& byTarget : byUser.second)
		{
			const std::unordered_set<GridFrameBase*>& moduleFrames = byTarget.second->GetGridFrames();
			for (auto gfb : moduleFrames)
			{
				ASSERT(nullptr != gfb);
				gfb->AutomationCleanup();
			}
		}
	}

	if (m_automationSessionToRestoreInMainFrameAfterScriptCompletion)
	{
		auto mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
		ASSERT(nullptr != mainFrame);
		if (nullptr != mainFrame)
			mainFrame->SetSapio365Session(m_automationSessionToRestoreInMainFrameAfterScriptCompletion);
		m_automationSessionToRestoreInMainFrameAfterScriptCompletion.reset();
	}

	std::shared_ptr<Sapio365Session> session = MainFrameSapio365Session();
	if (session)
		session->SetCommandLineAutomationInProgress(false);

	SetCurrentContext(AutomationContext(nullptr, nullptr));
}

AutomatedApp::AUTOMATIONSTATUS Office365AdminApp::automationProcessUserInputVar(const wstring& p_DlgTitle, 
																				const map<INT, wstring> p_BtnTexts, 
																				vector<AutomationInputVariableDefinition>& p_VarDef, 
																				const vector<AutomationInputVariableEvent>& p_VarEvents,
																				const vector<AutomationInputVariableEvent>& p_VarEventsGlobal)
{
	AUTOMATIONSTATUS rv = AUTOMATIONSTATUS_CANCEL;

	auto frame		= GetCurrentContext().GetFrame();
	CWnd* parent	= nullptr == frame && nullptr != GetWizard() ? GetWizard()->GetParentForPreset() : frame;

	DlgAutomationInputHTML dlg(p_DlgTitle, p_BtnTexts, p_VarDef, p_VarEvents, p_VarEventsGlobal, parent);
	if (dlg.DoModal() == IDOK)
	{
		p_VarDef = dlg.GetScriptVariables();
		rv = AUTOMATIONSTATUS_OK;
	}

	return rv;
}

wstring Office365AdminApp::automationListLabeCleanUp(const wstring& p_Label)
{
	return Str::StripAt(p_Label, g_O365_ListItemSessionRoleSeparator);// remove role incrustation in automation drop-down list labels
}

void Office365AdminApp::AutomationApplicationPreInit()
{
	// TODO:
}

bool Office365AdminApp::GetConsent(const web::uri& p_BrowserUri, const wstring& p_SessionType, CWnd* p_Parent, const wstring& p_ConsentDlgTitle)
{
	bool requestSuccessfullyStarted = false;

	auto callback = [this, p_Parent](const ConsentResult& p_Result) {
		if (!p_Result.HasError)
		{
			YCallbackMessage::DoPost(p_Parent, [this, p_Parent]()
			{
				// Will happen if dialog gets closed as the listener shared_ptr will get deleted, which will trigger cancellation of the task
				TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("GetConsent"), _YDUMP("Consent successfully granted."));
				// FIXME: We should use the logger service.
				// We need to rework logger channels to allow trace-only logging.
				//LoggerService::Debug(_YTEXT("GetConsent: Consent successfully granted."));

				YCodeJockMessageBox msgBox(p_Parent,
					DlgMessageBox::eIcon::eIcon_Information,
					YtriaTranslate::Do(Office365AdminApp_GetConsent_1, _YLOC("Consent")).c_str(),
					YtriaTranslate::Do(Office365AdminApp_GetConsent_2, _YLOC("Consent successfully granted.")).c_str(),
					_YTEXT(""),
					{ { IDOK, YtriaTranslate::Do(Office365AdminApp_GetConsent_3, _YLOC("OK")).c_str() } });
				msgBox.DoModal();
			});
		}
		else if (!p_Result.ErrorDescription.empty())
		{
			// If no error message, it means the user quit the consent process. No need to display the message box
			YCallbackMessage::DoPost(p_Parent, [this, p_Parent, error = p_Result.ErrorDescription]()
			{
				// Will happen if dialog gets closed as the listener shared_ptr will get deleted, which will trigger cancellation of the task
				TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("GetConsent"), _YDUMPFORMAT(L"error: %s", error.c_str()));
				// FIXME: We should use the logger service.
				// We need to rework logger channels to allow trace-only logging.
				//LoggerService::Debug(_YDUMPFORMAT(L"GetConsent error: %s", error.c_str()));

				YCodeJockMessageBox msgBox(p_Parent,
					DlgMessageBox::eIcon::eIcon_Error,
					YtriaTranslate::Do(Office365AdminApp_GetConsent_4, _YLOC("Consent")).c_str(),
					YtriaTranslate::Do(Office365AdminApp_GetConsent_5, _YLOC("Consent was not given. Please check information and resubmit.")).c_str(),
					YtriaTranslate::Do(Office365AdminApp_GetConsent_7, _YLOC("Error message: %1"), error.c_str()),
					{ { IDOK, YtriaTranslate::Do(Office365AdminApp_GetConsent_6, _YLOC("OK")).c_str() } });
				msgBox.DoModal();
			});
		}
	};

	return GetConsent(p_BrowserUri, p_SessionType, p_Parent, callback, p_ConsentDlgTitle);
}

bool Office365AdminApp::GetConsent(const web::uri& p_BrowserUri, const wstring& p_SessionType, CWnd* p_Parent, const std::function<void(ConsentResult)>& p_ConsentResultCallback, const wstring& p_ConsentDlgTitle)
{
	return ConsentGiver::GetInstance().GetConsent(p_BrowserUri, p_SessionType, p_Parent, p_ConsentResultCallback, p_ConsentDlgTitle);
}

const wstring Office365AdminApp::GetApplicationColor() const
{
	return _YTEXT("955BA3");
}

const wstring Office365AdminApp::GetApplicationSlogan() const
{
	return YtriaTranslate::Do(Office365AdminApp_GetApplicationSlogan_1, _YLOC("Manage all your Office 365 tenant data easily.")).c_str();
}

std::shared_ptr<ILicenseValidator> Office365AdminApp::makeLicenseValidator()
{
	VendorLicense::SetFeatureKeyToken(LicenseUtil::g_codeSapio365FeatureToken);
	VendorLicense::SetFeatureKeyCap(LicenseUtil::g_codeSapio365FeatureGlobalUserCount);

	return std::make_shared<O365LicenseValidator>();
}

void Office365AdminApp::automationInitVariablesSpecific(set<wstring, Str::keyLessInsensitive>& p_AppvariablesSystem)
{
	p_AppvariablesSystem.insert(g_O365Tenant_DefaultDomainName);
	p_AppvariablesSystem.insert(g_O365Tenant_InitialDomainName);
	p_AppvariablesSystem.insert(g_O365LastLoadingError);
	p_AppvariablesSystem.insert(g_O365UserPrincipalName);
	p_AppvariablesSystem.insert(g_O365UserPrincipalEmail);
	p_AppvariablesSystem.insert(g_O365UserPrincipalType);
	p_AppvariablesSystem.insert(g_O365UserDisplayedName);
}

wstring Office365AdminApp::automationReplaceVarSystemSpecific(const wstring& p_SysVarName)
{
	wstring varValue;

	if (MFCUtil::StringMatchW(p_SysVarName, g_O365LastLoadingError))
	{
		varValue = g_LastLoadingError;
	}
	else
	{
		const auto& ac = GetCurrentContext();

		auto gridFrame = dynamic_cast<GridFrameBase*>(ac.GetFrame());
		auto mainFrame = dynamic_cast<MainFrame*>(ac.GetFrame());

		auto session = ac.GetFrame() == nullptr ? MainFrameSapio365Session() : nullptr != gridFrame ? gridFrame->GetSapio365Session() : nullptr != mainFrame ? mainFrame->GetSapio365Session() : nullptr;
		if (session)
		{
			auto org = session->GetGraphCache().GetRBACOrganization();
			if (org.is_initialized())
			{
				if (MFCUtil::StringMatchW(p_SysVarName, g_O365Tenant_InitialDomainName))
					varValue = org.get().GetInitialDomain();
				else if (MFCUtil::StringMatchW(p_SysVarName, g_O365Tenant_DefaultDomainName))
					varValue = org.get().GetDefaultDomain();
			}

			auto av = automationGetSessionVariables(session);
			if (MFCUtil::StringMatchW(p_SysVarName, g_O365UserPrincipalName))
				varValue = av.m_ValueForO365UserPrincipalName;
			else if (MFCUtil::StringMatchW(p_SysVarName, g_O365UserPrincipalEmail))
				varValue = MFCUtil::ValidateEmail(av.m_ValueForO365UserPrincipalEmail).empty() ? av.m_ValueForO365UserPrincipalEmail : wstring();
			else if (MFCUtil::StringMatchW(p_SysVarName, g_O365UserPrincipalType))
				varValue = av.m_ValueForO365UserPrincipalType;
			else if (MFCUtil::StringMatchW(p_SysVarName, g_O365UserDisplayedName))
				varValue = av.m_ValueForO365UserDisplayedName;
		}
	}
	
	return varValue;
}

Office365AdminApp::automationSessionVariables Office365AdminApp::automationGetSessionVariables(const shared_ptr<Sapio365Session>& p_Session)
{
	if (p_Session->IsUltraAdmin())
		return 	automationGetSessionVariables(p_Session->GetSessionType(), GetFullAdminDisplayName(), GetFullAdminDisplayName(), p_Session->GetRoleDelegationFromCache().GetDelegation().m_Name);
	else
	{
		const auto& user = p_Session->GetGraphCache().GetCachedConnectedUser();
		ASSERT(user);
		if (user)
			return automationGetSessionVariables(	p_Session->GetSessionType(),
													user->GetUserPrincipalName() ? user->GetUserPrincipalName()->c_str() : wstring(),
													user->GetDisplayName() ? user->GetDisplayName()->c_str() : wstring(),
													p_Session->GetRoleDelegationFromCache().GetDelegation().m_Name);
	}

	return automationSessionVariables();
}

Office365AdminApp::automationSessionVariables Office365AdminApp::automationGetSessionVariables(const PersistentSession& p_Session)
{
	return automationGetSessionVariables(p_Session.GetSessionType(), p_Session.GetSessionName(), p_Session.GetFullname(), p_Session.GetRoleNameInDb());
}

Office365AdminApp::automationSessionVariables Office365AdminApp::automationGetSessionVariables(const wstring& p_SessionType, const wstring& p_PrincipalName, const wstring& p_DisplayName, const wstring p_RoleName)
{
	static const map<wstring, wstring> g_ListNameBySessionType =
	{
		{ SessionTypes::g_UltraAdmin,				g_O365_List_Sessions_UltraAdmin },
		{ SessionTypes::g_AdvancedSession,			g_O365_List_Sessions_Advanced },
		{ SessionTypes::g_ElevatedSession,			g_O365_List_Sessions_Elevated },
		{ SessionTypes::g_Role,						g_O365_List_Sessions_Role },
		{ SessionTypes::g_StandardSession,			g_O365_List_Sessions_Standard },
		{ SessionTypes::g_PartnerAdvancedSession,	g_O365_List_Sessions_PartnerAdvanced },
		{ SessionTypes::g_PartnerElevatedSession,	g_O365_List_Sessions_PartnerElevated },
	};

	automationSessionVariables av;

	av.m_ValueForO365_List_Sessions		= g_ListNameBySessionType.at(p_SessionType);
	av.m_ValueForO365UserPrincipalEmail = p_PrincipalName;
	av.m_ValueForO365UserPrincipalType	= p_SessionType;

	if (p_SessionType == SessionTypes::g_Role)
	{
		av.m_ValueForO365UserPrincipalName = p_PrincipalName + g_O365_ListItemSessionRoleSeparator + p_RoleName;
		av.m_ValueForO365UserDisplayedName = _YTEXTFORMAT(L"[%s] %s", p_RoleName.c_str(), p_DisplayName.c_str());
	}
	else
	{
		av.m_ValueForO365UserPrincipalName = p_PrincipalName;
		av.m_ValueForO365UserDisplayedName = p_DisplayName;
	}

	return av;
}

void Office365AdminApp::createSystemLists(const wstring& p_ListName) 
{
	ASSERT(nullptr != g_automationEngine);
	if (nullptr != g_automationEngine)
	{
		if (MFCUtil::StringMatchW(p_ListName, g_O365Tenant_AllDomains))
		{
			const auto& ac = GetCurrentContext();

			auto gridFrame = dynamic_cast<GridFrameBase*>(ac.GetFrame());
			auto mainFrame = dynamic_cast<MainFrame*>(ac.GetFrame());

			auto session = nullptr != gridFrame ? gridFrame->GetSapio365Session() : nullptr != mainFrame ? mainFrame->GetSapio365Session() : nullptr;
			if (session)
			{
				auto org = session->GetGraphCache().GetRBACOrganization();
				if (org.is_initialized())
				{
					g_automationEngine->ClearList(g_O365Tenant_AllDomains);
					for (const auto& domain : org.get().GetDomains())
					{
						AutomationAction* setVar = g_automationEngine->AddAction(AutomationConstant::val().m_ActionNameSetVar, { { g_O365Tenant_DomainName, domain } }, GetAutomationRules());
						g_automationEngine->AddActionsToLoopList(g_O365Tenant_AllDomains, { setVar });
					}
				}
			}
		}
		else if (isSessionList(p_ListName))
		{
			static const map<wstring, wstring> g_TypeByListName = {
																	{ g_O365_List_Sessions_UltraAdmin,		SessionTypes::g_UltraAdmin },
																	{ g_O365_List_Sessions_Advanced,		SessionTypes::g_AdvancedSession },
																	{ g_O365_List_Sessions_Elevated,		SessionTypes::g_ElevatedSession },
																	{ g_O365_List_Sessions_Role,			SessionTypes::g_Role },
																	{ g_O365_List_Sessions_Standard,		SessionTypes::g_StandardSession },
																	{ g_O365_List_Sessions_PartnerAdvanced,	SessionTypes::g_PartnerAdvancedSession },
																	{ g_O365_List_Sessions_PartnerElevated,	SessionTypes::g_PartnerElevatedSession },
																   };
			ASSERT(g_TypeByListName.find(p_ListName) != g_TypeByListName.end());
			if (g_TypeByListName.find(p_ListName) != g_TypeByListName.end())
			{
				auto sessions = SessionsSqlEngine::Get().GetList(g_TypeByListName.at(p_ListName));
				std::sort(sessions.begin(), sessions.end(), UISessionListSorter());
				for (const auto& session : sessions)
					addSessionLoopListAction(session);
			}
		}
	}
}

void Office365AdminApp::addSessionLoopListAction(const PersistentSession& p_Session)
{
	ASSERT(nullptr != g_automationEngine);
	if (nullptr != g_automationEngine)
	{
		auto av = automationGetSessionVariables(p_Session);
		AutomationAction* setVar = g_automationEngine->AddAction(AutomationConstant::val().m_ActionNameSetVar, { { g_O365_ListItemSessionName, av.m_ValueForO365UserPrincipalName }, { g_O365_ListItemSessionLabel, av.m_ValueForO365UserDisplayedName } }, GetAutomationRules());
		g_automationEngine->AddActionsToLoopList(av.m_ValueForO365_List_Sessions, { setVar });
	}
}

bool Office365AdminApp::isSessionList(const wstring& p_ListName) const
{
	return	MFCUtil::StringMatchW(p_ListName, g_O365_List_Sessions_UltraAdmin)
		||	MFCUtil::StringMatchW(p_ListName, g_O365_List_Sessions_Advanced)
		||	MFCUtil::StringMatchW(p_ListName, g_O365_List_Sessions_Elevated)
		||	MFCUtil::StringMatchW(p_ListName, g_O365_List_Sessions_Role)
		||	MFCUtil::StringMatchW(p_ListName, g_O365_List_Sessions_Standard)
		||	MFCUtil::StringMatchW(p_ListName, g_O365_List_Sessions_PartnerAdvanced)
		||	MFCUtil::StringMatchW(p_ListName, g_O365_List_Sessions_PartnerElevated);
}

bool Office365AdminApp::isSystemList(const wstring& p_ListName) const
{
	return	MFCUtil::StringMatchW(p_ListName, g_O365Tenant_AllDomains) || isSessionList(p_ListName);
}

void Office365AdminApp::automationProcessContext()
{
	const bool automationIsAboutToRun = !IsAutomationRunning();
	ASSERT(!automationIsAboutToRun || !m_automationSessionToRestoreInMainFrameAfterScriptCompletion);

	// set sapio365Session in mainframe Bug #190527.EH.009F72
	// we want to carry the session through the automation process
	const auto currentContext = GetCurrentContext();
	// Test IsWindow a frame can be destroyed (closed) at any time during a job - Bug #190614.SB.00A086
	auto frame = (nullptr != currentContext.GetFrame() && ::IsWindow(currentContext.GetFrame()->m_hWnd))
				? dynamic_cast<GridFrameBase*>(currentContext.GetFrame())
				: nullptr;
	auto mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != frame && nullptr != mainFrame) // if coming from main frame, no need to reset its own session
	{
		const auto sessionFrame		= frame->GetSapio365Session();
		const auto sessionMainFrame	= mainFrame->GetSapio365Session();
		if (sessionFrame && sessionFrame != sessionMainFrame && sessionFrame->IsConnected())
		{
			if (automationIsAboutToRun)
				m_automationSessionToRestoreInMainFrameAfterScriptCompletion = sessionMainFrame;
			mainFrame->SetSapio365Session(sessionFrame);
		}
	}
}

AutomatedApp::AUTOMATIONSTATUS Office365AdminApp::automationSendMail(AutomationAction* i_Action)
{
	auto rv = AUTOMATIONSTATUS::AUTOMATIONSTATUS_ERROR;

	ASSERT(IsActionSendMail(i_Action));
	if (IsActionSendMail(i_Action))
	{
		const auto currentContext = GetCurrentContext();

		auto frame =	nullptr != currentContext.GetFrame() && ::IsWindow(currentContext.GetFrame()->m_hWnd) ?
						dynamic_cast<GridFrameBase*>(currentContext.GetFrame()) :
						nullptr;

		auto mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
		ASSERT(nullptr != mainFrame);
		if (nullptr != mainFrame)
		{
			auto sapio365Session = nullptr != frame ? frame->GetSapio365Session() : mainFrame->GetSapio365Session();
			if (sapio365Session)
			{
				ASSERT(sapio365Session->GetMainMSGraphSession());// in RBAC we need to send emails via the user account
				if (sapio365Session->GetMainMSGraphSession())
				{
					vector<PooledString> recipients;
					for (const auto& r : Str::explodeIntoVector(i_Action->GetParamValue(AutomationConstant::val().m_ParamNameSendTo), wstring(_YTEXT(",")), false, true))
					{
						auto err = MFCUtil::ValidateEmail(r, AutomationConstant::val().m_ParamNameSendTo);
						if (err.empty())
							recipients.push_back(r);
						else
							i_Action->AddError(err.c_str());
					}

					if (!recipients.empty() && !i_Action->HasErrors(TraceOutput::TRACE_ALARM_LEVEL::TRACE_ERROR))
					{
						try
						{
							AutomationAction* bodyAction = nullptr;
							map<PooledString, bool> attachmentPaths;
							for (const auto child : i_Action->GetChildren())
							{
								ASSERT(nullptr != child);
								if (IsActionBody(child))
									bodyAction = child;
								else if (IsActionAttachment(child))
								{
									const auto& attachmentFilePath = child->GetParamValue(AutomationConstant::val().m_ParamNameFilePath);
									if (FileUtil::FileExists(attachmentFilePath))
										attachmentPaths[attachmentFilePath] = child->IsParamTrue(AutomationConstant::val().m_ParamNameZip);
									else
										i_Action->AddError(_YFORMATERROR(L"Attachment file not found: %s", attachmentFilePath.c_str()).c_str());
								}
								else
									i_Action->AddError(YtriaTranslate::Do(Office365AdminApp_automationSendMail_3, _YLOC("Irrelevant action: %1"), child->toString()).c_str(), TraceOutput::TRACE_WARNING);
								if (nullptr != child)
									child->SetIsExecuted(true);
								IncrementCount(child);
							}

							ASSERT(nullptr != bodyAction);
							if (nullptr != bodyAction && !i_Action->HasErrors(TraceOutput::TRACE_ERROR))
							{
								const PooledString contentType = bodyAction->GetValue().find(_YTEXT("<")) == wstring::npos ? _YTEXT("Text") : _YTEXT("HTML");// audacious!
								auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, sapio365Session->GetIdentifier());

								CreateMessageRequester msgReq(i_Action->GetParamValue(AutomationConstant::val().m_ParamNameSubject), contentType, bodyAction->GetValue());
								msgReq.UseMainGraphSession();
								for (const auto& recipient : recipients)
									msgReq.AddRecipient(recipient);
								msgReq.Send(sapio365Session, YtriaTaskData()).get();

								if (!msgReq.GetResult().m_Error)
								{
									bool hasAttachmentError = false;

									wstring zipFileName = i_Action->GetParamValue(AutomationConstant::val().m_ParamNameZipName);

									auto makeZipFilepath = [](const wstring& p_Path) {
										wstring yTempDir;
										FileUtil::GetYtriaTempDirPath(yTempDir);
										yTempDir.append(_YTEXT("\\"));

										auto dateTime = TimeUtil::GetInstance().GetISO8601String(YTimeDate::GetCurrentTimeDate());
										FileUtil::MakeFileNameValid(dateTime);

										return yTempDir + p_Path + _YTEXT("_") + dateTime + _YTEXT(".zip");
									};

									vector<wstring> filesInMainZip;
									for (const auto& attachment : attachmentPaths)
									{
										wstring attachmentPath = attachment.first;
										bool zipThisFile = attachment.second;

										if (zipThisFile)
										{
											wstring thisFileZip = makeZipFilepath(FileUtil::FileGetFileName(attachmentPath));
											wstring thisFileZipError = FileUtil::Zip({ attachmentPath }, thisFileZip);
											ASSERT(thisFileZipError.empty());
											if (thisFileZipError.empty())
												attachmentPath = thisFileZip;
											else
												TraceIntoFile::trace(_YDUMP("Office365Admin"), _YDUMP("automationSendMail"), thisFileZipError.c_str());
										}

										if (zipFileName.empty())
										{
											UploadMessageAttachmentRequester attReq(attachmentPath, msgReq.GetMessageId());
											attReq.UseMainMSGraphSession();
											attReq.Send(sapio365Session, YtriaTaskData()).get();

											if (attReq.GetResult().m_Error)
											{
												hasAttachmentError = true;
												TraceIntoFile::trace(_YDUMP("Office365Admin"), _YDUMP("automationSendMail"), attReq.GetResult().m_Error->GetFullErrorMessage().c_str());
											}
										}
										else
											filesInMainZip.push_back(attachmentPath);
									}

									if (!zipFileName.empty())
									{
										wstring mainZipName = zipFileName;
										FileUtil::MakeFileNameValid(mainZipName);
										mainZipName = makeZipFilepath(FileUtil::FileGetFileName(mainZipName));
										wstring mainFileZipError = FileUtil::Zip(filesInMainZip, mainZipName);

										ASSERT(mainFileZipError.empty());
										if (mainFileZipError.empty())
										{
											UploadMessageAttachmentRequester attReq(mainZipName, msgReq.GetMessageId());
											attReq.UseMainMSGraphSession();
											attReq.Send(sapio365Session, YtriaTaskData()).get();

											if (attReq.GetResult().m_Error)
											{
												hasAttachmentError = true;
												TraceIntoFile::trace(_YDUMP("Office365Admin"), _YDUMP("automationSendMail"), attReq.GetResult().m_Error->GetFullErrorMessage().c_str());
											}
										}
										else
											TraceIntoFile::trace(_YDUMP("Office365Admin"), _YDUMP("automationSendMail"), mainFileZipError.c_str());
									}

									if (!hasAttachmentError)
									{
										SendMessageRequester sendRequester(msgReq.GetMessageId());
										sendRequester.UseMainMSGraphSession();
										sendRequester.Send(sapio365Session, YtriaTaskData()).get();

										if (sendRequester.GetResult().m_Error)
											TraceIntoFile::trace(_YDUMP("Office365Admin"), _YDUMP("automationSendMail"), sendRequester.GetResult().m_Error->GetFullErrorMessage().c_str());
									}
									else
									{
										TraceIntoFile::trace(_YDUMP("Office365Admin"), _YDUMP("automationSendMail"), _YTEXT("One or more errors occured while uploading attachments."));

										CreateMessageRequester msgError(i_Action->GetParamValue(AutomationConstant::val().m_ParamNameSubject), _YTEXT("text"), _T("A problem occured while adding attachments to this message. Please make sure the attachments are not too big."));
										msgError.UseMainGraphSession();
										for (const auto& recipient : recipients)
											msgError.AddRecipient(recipient);
										msgError.Send(sapio365Session, YtriaTaskData()).get();

										SendMessageRequester sendRequester(msgError.GetMessageId());
										sendRequester.UseMainMSGraphSession();
										sendRequester.Send(sapio365Session, YtriaTaskData()).get();
									}
								}
								else
								{
									TraceIntoFile::trace(_YDUMP("Office365Admin"), _YDUMP("automationSendMail"), msgReq.GetResult().m_Error->GetFullErrorMessage().c_str());
								}
								rv = AUTOMATIONSTATUS::AUTOMATIONSTATUS_OK;
							}
						}
						catch (const RestException& re)
						{
							i_Action->AddError(re.WhatUnicode().c_str());
						}
						catch (const std::exception& e)
						{
							i_Action->AddError(Str::convertFromUTF8(e.what()).c_str());
						}
					}
					else
						i_Action->AddError(YtriaTranslate::Do(Office365AdminApp_automationSendMail_1, _YLOC("A valid session must be active")).c_str());
				}
			}
			else
				i_Action->AddError(YtriaTranslate::Do(Office365AdminApp_automationSendMail_1, _YLOC("A valid session must be active")).c_str());
		}

		SetAutomationGreenLight(i_Action);
	}

	return rv;
}

std::unique_ptr<IBrowserLinkHandler> Office365AdminApp::getLinkHandler(AutomationAction* p_Action) const
{
	std::unique_ptr<IBrowserLinkHandler> handler;
	if (IsActionShowUsers(p_Action))
	{
		if (p_Action->HasParam(DlgModuleOptions::g_AutoParamFilter))
			handler.reset(new LinkHandlerPrefilteredUsers);
		else
			handler.reset(new LinkHandlerUsers);
	}
	else if (IsActionShowGroups(p_Action))
	{
		if (p_Action->HasParam(DlgModuleOptions::g_AutoParamFilter))
			handler.reset(new LinkHandlerPrefilteredGroups);
		else
			handler.reset(new LinkHandlerGroups);
	}
	else if (IsActionShowSites(p_Action))
		handler.reset(new LinkHandlerSites);
	else if (IsActionShowDirectoryRoles(p_Action))
		handler.reset(new LinkHandlerDirectoryRoles);
	else if (IsActionShowLicences(p_Action))
		handler.reset(new LinkHandlerLicenses);
	else if (IsActionShowSignIns(p_Action))
		handler.reset(new LinkHandlerSignIns);
	else if (IsActionShowAuditLogs(p_Action))
		handler.reset(new LinkHandlerAuditLogs);
	else if (IsActionShowUsersRecycleBin(p_Action))
		handler.reset(new LinkHandlerUsersRecycleBin);
	else if (IsActionShowGroupsRecycleBin(p_Action))
		handler.reset(new LinkHandlerGroupsRecycleBin);
	else if (IsActionShowSchools(p_Action))
		handler.reset(new LinkHandlerSchools);
	else if (IsActionShowUsageReport(p_Action))
		handler.reset(new LinkHandlerO365Reports);
	/*else if (IsActionShowOrgContacts(p_Action))
		handler.reset(new LinkHandlerOrgContacts);*/
	else if (IsActionShowMyDataMail(p_Action))
		handler.reset(new LinkHandlerMessages);
	else if (IsActionShowMyDataCalendar(p_Action))
		handler.reset(new LinkHandlerEvents);
	else if (IsActionShowMyDataDrive(p_Action))
		handler.reset(new LinkHandlerDrive);
	else if (IsActionShowMyDataContact(p_Action))
		handler.reset(new LinkHandlerContacts);
	else if (IsActionShowMyDataMesssageRules(p_Action))
		handler.reset(new LinkHandlerMessageRules);

	return std::move(handler);
}

bool Office365AdminApp::automationIsNewModuleAction(const wstring& p_ActionName) const
{
	auto a = p_ActionName;
	return Str::beginsWith(Str::toUpper(a), _YTEXT("SHOW"));// lazy
}

void Office365AdminApp::ConnectPartnerToCustomer(std::shared_ptr<Sapio365Session> p_Session, const wstring& p_CustomerTenant)
{
	ASSERT(p_Session && (p_Session->IsAdvanced() || p_Session->IsElevated()) && !p_CustomerTenant.empty());

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame && p_Session && (p_Session->IsAdvanced() || p_Session->IsElevated()) && !p_CustomerTenant.empty())
	{
		const auto principalName = p_Session->GetConnectedUserPrincipalName();
		SessionIdentifier newSessId{ principalName, SessionTypes::g_PartnerAdvancedSession, 0 };

		auto loadSession = [newSessId]()
		{
			auto loader = SessionLoaderFactory(newSessId).CreateLoader();
			SessionSwitcher(std::move(loader)).Switch();
		};

		if (!SessionsSqlEngine::Get().SessionExists(newSessId))
		{
			auto oldPersistenSession = SessionsSqlEngine::Get().Load(p_Session->GetIdentifier());
			ASSERT(oldPersistenSession.IsValid());
			if (oldPersistenSession.IsValid())
			{
				auto proceed = [loadSession, oldPersistenSession, p_CustomerTenant, p_Session]()
				{
					YTimeDate now = YTimeDate::GetCurrentTimeDate();
					PersistentSession newPersistentSession;
					newPersistentSession.SetAccessCount(1);
					newPersistentSession.SetCreatedOn(now);

					newPersistentSession.SetSessionType(SessionTypes::g_PartnerAdvancedSession);
					newPersistentSession.SetTechnicalName(oldPersistenSession.GetEmailOrAppId());
					newPersistentSession.SetSessionName(_YTEXTFORMAT(L"Customer: %s", p_CustomerTenant.c_str()));
					newPersistentSession.SetLastUsedOn(now);
					newPersistentSession.SetFullname(oldPersistenSession.GetFullname());
					newPersistentSession.SetTenantName(p_CustomerTenant);

					SessionsSqlEngine::Get().Save(newPersistentSession);
					{
						DbSessionPersister persister(newPersistentSession);
						persister.Save(p_Session->GetMainMSGraphSession()->GetAuthenticator()->GetOAuth2Config());
					}

					loadSession();					
				};

				YCodeJockMessageBox dlg(mainFrame
					, DlgMessageBox::eIcon_ExclamationWarning
					, _T("Admin Consent Needed")
					, _YFORMAT(L"To continue, Admin consent must be given to sapio365 on tenant %s.", p_CustomerTenant.c_str())
					, _YFORMAT(L"If it's not already the case, you can either give it yourself using admin credentials on %s, or ask your customer to do it for you by sending him the following URL.", p_CustomerTenant.c_str())
					, { {IDOK, _YTEXT("Give consent now")} , {IDYES, _YTEXT("Copy consent URL")}, {IDCANCEL, _YTEXT("Continue")} }
				, IDCANCEL);

				auto res = dlg.DoModal();
				if (res == IDOK)
				{
					auto callback = [mainFrame, proceed](const ConsentResult& p_Result)
					{
						auto it = p_Result.Query.find(_YTEXT("admin_consent"));
						if (!p_Result.HasError && p_Result.Query.end() != it && it->second == _YTEXT("True"))
						{
							YCallbackMessage::DoPost(proceed);
						}
						else
						{
							auto itErr = p_Result.Query.find(_YTEXT("error"));
							auto itErrSub = p_Result.Query.find(_YTEXT("error_subcode"));
							wstring errorDetails = p_Result.ErrorDescription.empty() && p_Result.Query.end() != itErr
								? (p_Result.Query.end() != itErrSub ? _YFORMAT(L"Reason: %s (%s)", itErr->second.c_str(), itErrSub->second.c_str())
									: _YFORMAT(L"Reason: %s", itErr->second.c_str(), itErrSub->second.c_str()))
								: p_Result.ErrorDescription;
							YCallbackMessage::DoPost([mainFrame, errorDetails, proceed]()
								{
									if (IDOK == YCodeJockMessageBox(mainFrame
										, DlgMessageBox::eIcon_Error
										, _T("Error")
										, _YFORMATERROR(L"Consent has not been given. Continue?")
										, errorDetails
										, { {IDOK, _YTEXT("OK")}, {IDCANCEL, _YTEXT("Cancel")} }).DoModal())
									{
										proceed();
									}
								}
							);
						}
					};

					if (!ConsentGiver::GetInstance().GetConsent(SessionTypes::GetAdvancedSessionConsentUrl(true).to_uri()
						, SessionTypes::g_AdvancedSession
						, mainFrame
						, callback
						, _YTEXT("")))
					{
						if (IDOK == YCodeJockMessageBox(mainFrame
							, DlgMessageBox::eIcon_Error
							, _T("Error")
							, _YFORMATERROR(L"Consent has not been given. Continue?")
							, _YTEXT("")
							, { {IDOK, _YTEXT("OK")}, {IDCANCEL, _YTEXT("Cancel")} }).DoModal())
						{
							proceed();
						}
					}
				}
				else if (res == IDYES)
				{
					MFCUtil::copyUnicodeTextToClipboard(SessionTypes::GetAdvancedSessionConsentUrl(true, true).to_uri().to_string());

					YCodeJockMessageBox(mainFrame
						, DlgMessageBox::eIcon_Information
						, _T("Consent URL copied")
						, _T("Consent URL has been copied to clipboard. You may give it to your customer and try connecting again when he's done.")
						, _YTEXT("")
						, { {IDOK, _YTEXT("OK")} }).DoModal();
				}
				else
				{
					proceed();
				}
			}
			else
			{
				YCodeJockMessageBox(mainFrame
					, DlgMessageBox::eIcon_Error
					, _T("Error")
					, _YFORMATERROR(L"Advanced session %s could not be found. Unable to connect to customer.")
					, _YTEXT("")
					, { {IDOK, _YTEXT("OK")} }).DoModal();
				return;
			}
		}
		else
		{
			loadSession();
		}
	}
}

O365LicenseValidator* Office365AdminApp::getLicenseValidator() const
{
	ASSERT(nullptr != GetLicenseManager());
	O365LicenseValidator* licenseValidator = nullptr != GetLicenseManager() ? dynamic_cast<O365LicenseValidator*>(GetLicenseManager()->GetLicenseValidator().get()) : nullptr;
	ASSERT(nullptr != licenseValidator);
	return licenseValidator;
}

template<>
bool Office365AdminApp::IsLicense<LicenseTag::Collaboration>() // Cosmos
{
	auto licenseValidator = theApp.getLicenseValidator();
	return nullptr != licenseValidator && licenseValidator->IsLicenseCollaboration(theApp.GetLicenseManager()->GetLicense(false));
}

template<>
bool Office365AdminApp::IsLicense<LicenseTag::CreateSnapshot>()
{
	auto licenseValidator = theApp.getLicenseValidator();
	return nullptr != licenseValidator && licenseValidator->IsLicenseCreateReadOnly(theApp.GetLicenseManager()->GetLicense(false));
}

template<>
bool Office365AdminApp::IsLicense<LicenseTag::CreateRestorePoint>()
{
	auto licenseValidator = theApp.getLicenseValidator();
	return nullptr != licenseValidator && licenseValidator->IsLicenseCreateRestorePoint(theApp.GetLicenseManager()->GetLicense(false));
}

template<>
bool Office365AdminApp::IsLicense<LicenseTag::LoadRestorePoint>()
{
	auto licenseValidator = theApp.getLicenseValidator();
	return nullptr != licenseValidator && licenseValidator->IsLicenseLoadRestorePoint(theApp.GetLicenseManager()->GetLicense(false));
}

template<>
bool Office365AdminApp::IsLicense<LicenseTag::JobEditor>()
{
	auto licenseValidator = theApp.getLicenseValidator();
	return nullptr != licenseValidator && licenseValidator->IsLicenseJobEditor(theApp.GetLicenseManager()->GetLicense(false));
}

template<>
bool Office365AdminApp::IsLicense<LicenseTag::RBAC>()
{
	auto licenseValidator = theApp.getLicenseValidator();
	return nullptr != licenseValidator && licenseValidator->IsLicenseRBAC(theApp.GetLicenseManager()->GetLicense(false));
}

template<>
bool Office365AdminApp::IsLicense<LicenseTag::OnPremise>()
{
	auto licenseValidator = theApp.getLicenseValidator();
	return nullptr != licenseValidator && licenseValidator->IsLicenseOnPremise(theApp.GetLicenseManager()->GetLicense(false));
}

template<>
bool Office365AdminApp::IsLicense<LicenseTag::AJL>()
{
	auto licenseValidator = theApp.getLicenseValidator();
	return nullptr != licenseValidator && licenseValidator->IsLicenseAJL(theApp.GetLicenseManager()->GetLicense(false));
}

template<>
wstring Office365AdminApp::GetLicenseWarnMessage<LicenseTag::Collaboration>()
{
	return _T("This feature is only available with a Collaboration license.");
}

template<>
wstring Office365AdminApp::GetLicenseWarnMessage<LicenseTag::CreateSnapshot>()
{
	return _T("This feature is only available with a Create Snapshot license.");
}

template<>
wstring Office365AdminApp::GetLicenseWarnMessage<LicenseTag::CreateRestorePoint>()
{
	return _T("This feature is only available with a Create Restore Point license.");
}

template<>
wstring Office365AdminApp::GetLicenseWarnMessage<LicenseTag::LoadRestorePoint>()
{
	return _T("Only snapshots are authorized with your license.");
}

template<>
wstring Office365AdminApp::GetLicenseWarnMessage<LicenseTag::JobEditor>()
{
	return _T("This feature is only available with a Jobs Editor license.");
}

template<>
wstring Office365AdminApp::GetLicenseWarnMessage<LicenseTag::RBAC>()
{
	return _T("This feature is only available with an RBAC license.");
}
template<>
wstring Office365AdminApp::GetLicenseWarnMessage<LicenseTag::OnPremise>()
{
	return _T("This feature is only available with a On-Premises license.");
}

template<>
wstring Office365AdminApp::GetLicenseWarnMessage<LicenseTag::AJL>()
{
	// Shouldn't be useful.
	ASSERT(false);
	return _T("This feature is only available with a AJL license.");
}

bool Office365AdminApp::CanLicenseAJLremoveJobs() const
{
	auto licenseValidator = getLicenseValidator();
	return nullptr != licenseValidator && licenseValidator->CanLicenseAJLremoveJobs(GetLicenseManager()->GetLicense(false));
}

uint32_t Office365AdminApp::GetAJLCapacity() const
{
	auto licenseValidator = getLicenseValidator();
	return nullptr != licenseValidator ? licenseValidator->GetAJLCapacity(GetLicenseManager()->GetLicense(false)) : 0;
}

boost::YOpt<Office365AdminApp::LicenseTokenState> Office365AdminApp::GetLicenseTokenState(bool p_UpdateAvailableCount) const
{
	boost::YOpt<LicenseTokenState> licenseTokenState;
	if (nullptr != GetLicenseManager())
	{
		const auto& license = GetLicenseManager()->GetCachedLicense();
		const auto tokenFeature = license.GetFeatureToken();
		if (tokenFeature.IsAuthorized())
			licenseTokenState.emplace(LicenseTokenState{ p_UpdateAvailableCount ? GetLicenseManager()->CRMGetLicenseAvailableTokens() : LicenseUtil::g_NoValueUINT32, tokenFeature.GetPoolMax() });
	}
	return licenseTokenState;
}

VendorLicense Office365AdminApp::GetRefreshedLicense(bool p_Force) const
{
	VendorLicense license(NalpeironLicenseManager::Ytria_Undefined);
	{
		CWaitCursor waitCursor;
		license = p_Force ? GetLicenseManager()->RefreshLicense() : GetLicenseManager()->GetLicense(false);
	}

	// FIXME: We should probably update all the existing sessions.
	std::shared_ptr<Sapio365Session> session = MainFrameSapio365Session();
	if (session)
	{
		CosmosManager cosmosKeyManager;// Cosmos login storage (URL, Master Key)
		auto cnx = cosmosKeyManager.GetConnectorFromLicense(session);
		session->SetCosmosDbMasterKey(cnx.m_MasterKey);
		session->SetCosmosDbSqlUri(cnx.m_URL);
	}

	return license;
}

bool Office365AdminApp::IsLicensePro() const
{
	auto licenseValidator = theApp.getLicenseValidator();
	return nullptr != licenseValidator && licenseValidator->IsLicensePro(theApp.GetLicenseManager()->GetLicense(false));
}

bool Office365AdminApp::IsLicenseFullOrTrial(wstring& p_LicenseStatus) const
{
	bool rvFullOrTrial = false;

	LicenseManager* licenseMgr = GetLicenseManager();
	ASSERT(nullptr != licenseMgr);
	if (nullptr != licenseMgr)
	{
		auto license	= licenseMgr->GetLicense(false);
		rvFullOrTrial	= license.IsFullOrTrial();
		p_LicenseStatus = license.m_StatusStr;
	}

	return rvFullOrTrial;
}

bool Office365AdminApp::UpdateJobSelectionIDs(const set<wstring>& p_JobIDs, CWnd* p_Parent)
{
	DlgJobSelectionValidation dlgValidation(p_Parent);
	if (dlgValidation.DoModal() == IDOK)
		return UpdateJobSelectionIDs(p_JobIDs, dlgValidation.GetVN());

	return false;
}

bool Office365AdminApp::UpdateJobSelectionIDs(const set<wstring>& p_JobIDs, const wstring& p_ValidationNumber)
{
	LicenseManager* licenseMgr = GetLicenseManager();
	ASSERT(nullptr != licenseMgr);
	return nullptr != licenseMgr && licenseMgr->UpdateJobList(p_JobIDs, p_ValidationNumber);
}

set<wstring> Office365AdminApp::GetAJLJobIDs() const
{
	LicenseManager* licenseMgr = GetLicenseManager();
	ASSERT(nullptr != licenseMgr);
	return nullptr != licenseMgr ? licenseMgr->GetLicense(false).GetAJLJobSelectionIDs() : set<wstring>();
}

bool Office365AdminApp::IsJobAllowedByLicense(const wstring& p_JobID)
{
	auto licenseValidator = getLicenseValidator();
	return nullptr != licenseValidator && licenseValidator->IsJobAllowedByLicense(GetLicenseManager()->GetLicense(false), p_JobID);
}

void Office365AdminApp::AutomationWizardScriptRunComplete()
{
	if (IsLicense<LicenseTag::AJL>())
	{
		TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("AutomationWizardScriptRunComplete"), _YDUMPFORMAT(L"Job complete - License is AJL - disabling modules..."));

		// apply license restrictions
		for (const auto& bySession : CommandDispatcher::GetInstance().GetAllModules())
		{
			const auto& session = bySession.first;
			for (const auto& byTarget : bySession.second)
				for (const auto& frame : byTarget.second->GetGridFrames())
					frame->ApplyAJLLicenseAccess();
		}
	}
}
