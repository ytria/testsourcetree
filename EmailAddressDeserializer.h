#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "EmailAddress.h"

class EmailAddressDeserializer : public JsonObjectDeserializer, public Encapsulate<EmailAddress>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

