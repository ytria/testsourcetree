#include "BusinessPermission.h"


BusinessPermission::BusinessPermission()
{
}

BusinessPermission::BusinessPermission(const Permission& p_Permission)
{
    SetID(p_Permission.Id ? *p_Permission.Id : wstring(_YTEXT("")));
    SetGrantedTo(p_Permission.GrantedTo);
    SetInvitation(p_Permission.Invitation);
    SetInheritedFrom(p_Permission.InheritedFrom);
    SetLink(p_Permission.Link);
    SetRole(p_Permission.Role);
    SetShareId(p_Permission.ShareId);
}

const boost::YOpt<IdentitySet>& BusinessPermission::GetGrantedTo() const
{
    return m_GrantedTo;
}

void BusinessPermission::SetGrantedTo(const boost::YOpt<IdentitySet>& p_Val)
{
    m_GrantedTo = p_Val;
}

const boost::YOpt<SharingInvitation>& BusinessPermission::GetInvitation() const
{
    return m_Invitation;
}

void BusinessPermission::SetInvitation(const boost::YOpt<SharingInvitation>& p_Val)
{
    m_Invitation = p_Val;
}

const boost::YOpt<ItemReference>& BusinessPermission::GetInheritedFrom() const
{
    return m_InheritedFrom;
}

void BusinessPermission::SetInheritedFrom(const boost::YOpt<ItemReference>& p_Val)
{
    m_InheritedFrom = p_Val;
}

const boost::YOpt<SharingLink>& BusinessPermission::GetLink() const
{
    return m_Link;
}

void BusinessPermission::SetLink(const boost::YOpt<SharingLink>& p_Val)
{
    m_Link = p_Val;
}

const boost::YOpt<vector<PooledString>>& BusinessPermission::GetRole() const
{
    return m_Role;
}

void BusinessPermission::SetRole(const boost::YOpt<vector<PooledString>>& p_Val)
{
    m_Role = p_Val;
}

const boost::YOpt<PooledString>& BusinessPermission::GetShareId() const
{
    return m_ShareId;
}

void BusinessPermission::SetShareId(const boost::YOpt<PooledString>& p_Val)
{
    m_ShareId = p_Val;
}

Permission BusinessPermission::ToPermission() const
{
    Permission permission;
    permission.Id = m_Id;
    permission.GrantedTo = m_GrantedTo;
    permission.Invitation = m_Invitation;
    permission.InheritedFrom = m_InheritedFrom;
    permission.Link = m_Link;
    permission.Role = m_Role;
    permission.ShareId = m_ShareId;

    return permission;
}
