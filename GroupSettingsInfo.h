#pragma once

#include "BusinessGroupSetting.h"
#include "BusinessGroupSettingTemplate.h"

#include <vector>
#include <map>

struct GroupSettingsInfo
{
    std::vector<BusinessGroupSettingTemplate> m_AllGroupSettingTemplates;
    std::map<PooledString, vector<BusinessGroupSetting>> m_GroupSettingsPerGroup;
    vector<BusinessGroupSetting> m_GroupSettingsTenantWide;
};
