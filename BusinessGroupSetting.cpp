#include "BusinessGroupSetting.h"

RTTR_REGISTRATION
{
    using namespace rttr;
registration::class_<BusinessGroupSetting>("GroupSetting") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Group Setting"))))
.constructor()(policy::ctor::as_object);
}

BusinessGroupSetting::BusinessGroupSetting()
{
}

BusinessGroupSetting::BusinessGroupSetting(const GroupSetting& p_GroupSetting)
{
    SetDisplayName(p_GroupSetting.DisplayName);
    SetID(p_GroupSetting.Id ? *p_GroupSetting.Id : PooledString(_YTEXT("")));
    SetTemplateId(p_GroupSetting.TemplateId);
    SetValues(p_GroupSetting.Values);
}

GroupSetting BusinessGroupSetting::ToGroupSetting(const BusinessGroupSetting& p_GroupSetting)
{
    GroupSetting setting;

    setting.DisplayName = p_GroupSetting.GetDisplayName();
    setting.Id = p_GroupSetting.GetID();
    setting.TemplateId = p_GroupSetting.GetTemplateId();
    setting.Values = p_GroupSetting.GetValues();

    return setting;
}

const boost::YOpt<PooledString>& BusinessGroupSetting::GetDisplayName() const
{
	return m_DisplayName;
}

void BusinessGroupSetting::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}

const boost::YOpt<PooledString>& BusinessGroupSetting::GetTemplateId() const
{
    return m_TemplateId;
}

void BusinessGroupSetting::SetTemplateId(const boost::YOpt<PooledString>& p_Val)
{
    m_TemplateId = p_Val;
}

const vector<SettingValue>& BusinessGroupSetting::GetValues() const
{
    return m_Values;
}

vector<SettingValue>& BusinessGroupSetting::GetValues()
{
    return m_Values;
}

void BusinessGroupSetting::SetValues(const vector<SettingValue>& p_Val)
{
    m_Values = p_Val;
}

const boost::YOpt<bool>& BusinessGroupSetting::GetCreatedOrDeleted() const
{
    return m_CreatedOrDeleted;
}

void BusinessGroupSetting::SetCreatedOrDeleted(const boost::YOpt<bool>& p_Val)
{
    m_CreatedOrDeleted = p_Val;
}

bool BusinessGroupSetting::operator==(const BusinessGroupSetting& p_Other) const
{
	return m_Id				== p_Other.m_Id
		&& m_DisplayName	== p_Other.m_DisplayName
		&& m_TemplateId		== p_Other.m_TemplateId
		&& m_Values			== p_Other.m_Values;
}
