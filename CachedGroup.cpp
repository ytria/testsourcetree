#include "CachedGroup.h"

#include "MsGraphFieldNames.h"
#include "BusinessGroup.h"

RTTR_REGISTRATION
{
    using namespace rttr;
	registration::class_<CachedGroup>("CachedGroup") (metadata(MetadataKeys::TypeDisplayName, _YTEXT("Cached Group")))
	.constructor()(policy::ctor::as_object)
	.property(O365_GROUP_DISPLAYNAME, &CachedGroup::m_DisplayName)
	.property(O365_GROUP_GROUPTYPES, &CachedGroup::m_GroupTypes)
	.property(O365_GROUP_MAILENABLED, &CachedGroup::m_MailEnabled)
	.property(O365_GROUP_SECURITYENABLED, &CachedGroup::m_SecurityEnabled)
	.property(O365_GROUP_RESOURCEPROVISIONINGOPTIONS, &CachedGroup::m_ResourceProvisioningOptions)
	.property(O365_GROUP_MAIL, &CachedGroup::m_Mail)
		;
}

CachedGroup::CachedGroup(const Group& group)
	: CachedObject(group.m_Id)
	, m_DisplayName(group.m_DisplayName ? *group.m_DisplayName : PooledString(_YTEXT("")))
	, m_Mail(group.m_Mail)
	, m_GroupTypes(group.m_GroupTypes)
	, m_MailEnabled(group.m_MailEnabled)
	, m_SecurityEnabled(group.m_SecurityEnabled)
	, m_ResourceProvisioningOptions(group.m_ResourceProvisioningOptions)
{}

CachedGroup::CachedGroup(const BusinessGroup& p_Group)
	: CachedObject(p_Group.m_Id)
    , m_DisplayName(p_Group.GetDisplayName() ? *p_Group.GetDisplayName() : _YTEXT(""))
	, m_Mail(p_Group.GetMail())
    , m_GroupTypes(p_Group.GetGroupTypes())
    , m_MailEnabled(p_Group.IsMailEnabled())
    , m_SecurityEnabled(p_Group.IsSecurityEnabled())
	, m_ResourceProvisioningOptions(p_Group.GetResourceProvisioningOptions())
{
	SetRBACDelegationID(p_Group.GetRBACDelegationID());
}

const PooledString& CachedGroup::GetDisplayName() const
{
	return m_DisplayName;
}

const boost::YOpt<PooledString>& CachedGroup::GetMail() const
{
	return m_Mail;
}

const boost::YOpt<vector<PooledString>>& CachedGroup::GetGroupTypes() const
{
	return m_GroupTypes;
}

const boost::YOpt<bool>& CachedGroup::GetMailEnabled() const
{
	return m_MailEnabled;
}

const boost::YOpt<bool>& CachedGroup::GetSecurityEnabled() const
{
	return m_SecurityEnabled;
}

bool CachedGroup::GetIsTeam() const
{
	return m_ResourceProvisioningOptions && m_ResourceProvisioningOptions->end() != std::find(m_ResourceProvisioningOptions->begin(), m_ResourceProvisioningOptions->end(), _YTEXT("Team"));
}

const boost::YOpt<vector<PooledString>> CachedGroup::GetResourceProvisioningOptions() const
{
	return m_ResourceProvisioningOptions;
}
