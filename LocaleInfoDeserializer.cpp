#include "LocaleInfoDeserializer.h"

#include "JsonSerializeUtil.h"

void LocaleInfoDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("locale"), m_Data.Locale, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.DisplayName, p_Object);
}
