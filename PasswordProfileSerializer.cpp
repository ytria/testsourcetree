#include "PasswordProfileSerializer.h"

#include "JsonSerializeUtil.h"

PasswordProfileSerializer::PasswordProfileSerializer(const PasswordProfile& p_PasswordProfile)
    : m_PasswordProfile(p_PasswordProfile)
{
}

void PasswordProfileSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("password"), m_PasswordProfile.Password, obj);
    JsonSerializeUtil::SerializeBool(_YTEXT("forceChangePasswordNextSignIn"), m_PasswordProfile.ForceChangePasswordNextSignIn, obj);
	JsonSerializeUtil::SerializeBool(_YTEXT("forceChangePasswordNextSignInWithMfa"), m_PasswordProfile.ForceChangePasswordNextSignInWithMfa, obj);
}
