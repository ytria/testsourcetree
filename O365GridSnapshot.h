#pragma once

#include "GridSnapshotUtil.h"

class GridFrameBase;
class O365GridSnapshot
{
public:
	static const wstring g_FileExtensionPhoto;
	static const wstring g_FileExtensionRestore;

	// If filepath empty, opens file chooser
	// if p_ParentFrame is nullptr, main frame is used
	static void Load(wstring p_FilePath = _YTEXT(""), GridFrameBase* p_ParentFrame = nullptr);

	// If p_Mode is none, any mode allowed (in open file dialog)
	// if p_ParentFrame is nullptr, main frame is used
	static void Load(boost::YOpt<GridSnapshot::Options::Mode> p_Mode, GridFrameBase* p_ParentFrame = nullptr);

private:
	static void Load(wstring p_FilePath, boost::YOpt<GridSnapshot::Options::Mode> p_Mode, GridFrameBase* p_ParentFrame = nullptr);
};