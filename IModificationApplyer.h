#pragma once

class IModificationApplyer
{
public:
	virtual ~IModificationApplyer() = default;

	virtual void ApplyAll() = 0;
	virtual void ApplySelected() = 0;
};

