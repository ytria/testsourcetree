#pragma once

#include "IRequester.h"
#include "HttpResultWithError.h"

class CreateMessageRequester : public IRequester
{
public:
	CreateMessageRequester(const wstring& p_Subject, const wstring& p_ContentType, const wstring& p_Content);

	void AddRecipient(const wstring& p_Recipient);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	void UseMainGraphSession();

	const HttpResultWithError& GetResult() const;
	wstring GetMessageId() const;

private:
	wstring m_Subject;
	wstring m_ContentType;
	wstring m_Content;
	vector<PooledString> m_Recipients;

	bool m_UseMainGraphSession;

	std::shared_ptr<wstring> m_MessageId;
	std::shared_ptr<HttpResultWithError> m_Result;
};

