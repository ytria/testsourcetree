#include "CommandShowEduUsers.h"
#include "EducationUserListRequester.h"
#include "FrameEduUsers.h"
#include "GridEduUsers.h"
#include "BasicPageRequestLogger.h"
#include "YSafeCreateTask.h"

CommandShowEduUsers::CommandShowEduUsers(FrameEduUsers& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, bool p_IsUpdate)
	:IActionCommand(_YTEXT("")),
	m_Frame(p_Frame),
	m_AutoSetup(p_AutoSetup),
	m_LicenseCtx(p_LicenseCtx),
	m_AutoAction(p_AutoAction),
	m_IsUpdate(p_IsUpdate)
{
}

CommandShowEduUsers::CommandShowEduUsers(const vector<EducationUser>& p_Users, FrameEduUsers& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, bool p_IsUpdate)
	:IActionCommand(_YTEXT("")),
	m_Frame(p_Frame),
	m_AutoSetup(p_AutoSetup),
	m_LicenseCtx(p_LicenseCtx),
	m_AutoAction(p_AutoAction),
	m_IsUpdate(p_IsUpdate),
	m_Users(p_Users)
{
}

void CommandShowEduUsers::ExecuteImpl() const
{
	GridEduUsers* grid = dynamic_cast<GridEduUsers*>(&m_Frame.GetGrid());

	auto taskData = Util::AddFrameTask(_T("Show education users"), &m_Frame, m_AutoSetup, m_LicenseCtx, false);

	YSafeCreateTask([isUpdate = m_IsUpdate, taskData, hwnd = m_Frame.GetSafeHwnd(), moduleCriteria = m_Frame.GetModuleCriteria(), autoAction = m_AutoAction, grid, bom = m_Frame.GetBusinessObjectManager()]() {
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("education users"));
		auto eduUsersRequester = std::make_shared<EducationUserListRequester>(logger);

		std::shared_ptr<vector<EducationUser>> eduUsers = std::make_shared<vector<EducationUser>>();
		*eduUsers = safeTaskCall(eduUsersRequester->Send(bom->GetSapio365Session(), taskData).Then([&eduUsersRequester, &eduUsers]() {
			eduUsers = std::make_shared<vector<EducationUser>>(std::move(eduUsersRequester->GetData()));
			return eduUsersRequester->GetData(); // Empty because of the move above. Needed because of ListErrorHandler :-S
		}), bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ListErrorHandler<EducationUser>, taskData).get();

		YCallbackMessage::DoPost([=]() {
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameEduUsers*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ModuleBase::ShouldBuildView(hwnd, taskData.IsCanceled(), taskData, false))
			{
				ASSERT(nullptr != frame);
				if (nullptr != frame)
				{
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s education users...", Str::getStringFromNumber(eduUsers->size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());

					{
						CWaitCursor _;
						if (frame->HasLastUpdateOperations())
						{
							frame->BuildView(*eduUsers, frame->GetLastUpdateOperations(), false);
							frame->ForgetLastUpdateOperations();
						}
						else
						{
							frame->BuildView(*eduUsers, {}, true);
						}
					}

					if (!isUpdate)
					{
						frame->UpdateContext(taskData, hwnd);
						shouldFinishTask = false;
					}
				}

				if (shouldFinishTask)
					frame->TaskFinished(taskData, nullptr, hwnd);
			}
			});
	});
}
