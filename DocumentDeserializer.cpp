#include "DocumentDeserializer.h"

#include "BaseObjDeserializer.h"
#include "JsonSerializeUtil.h"

void Cosmos::DocumentDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    BaseObjDeserializer::Deserialize(p_Object, GetData());
	JsonSerializeUtil::DeserializeString(_YTEXT("_attachments"), m_Data._attachments, p_Object);

    auto content = web::json::value::object();
    for (const auto& kv : p_Object)
    {
        const auto& keyName = kv.first;
        const auto& keyValue = kv.second;
        if (!keyName.empty() && keyName[0] != L'_' && keyName != _YTEXT("id"))
            content[keyName] = keyValue;
    }

    m_Data.Content = content;
}
