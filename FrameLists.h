#pragma once

#include "GridFrameBase.h"

#include "BusinessList.h"
#include "GridLists.h"

class FrameLists : public GridFrameBase
{
public:
	FrameLists(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
    virtual ~FrameLists() = default;
    DECLARE_DYNAMIC(FrameLists)

    void ShowLists(const O365DataMap<BusinessSite, vector<BusinessList>>& p_Lists, Origin p_Origin, bool p_FullPurge);

    virtual O365Grid& GetGrid() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;
    virtual void createGrid() override;


	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

    virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

protected:
    //DECLARE_MESSAGE_MAP()

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
    GridLists m_ListsGrid;
};

