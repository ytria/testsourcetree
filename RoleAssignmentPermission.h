#pragma once

class GridBackendRow;

class RoleAssignmentPermission
{
public:
	RoleAssignmentPermission():m_Row(nullptr){}

    PooledString SiteId;
    PooledString SiteUrl;
    PooledString UserId;
    PooledString GroupId;
    PooledString RoleDefinitionId;

	GridBackendRow* m_Row;
};

