#include "DlgEmailConsent.h"

#include "DlgSelectRecipients.h"
#include "GridSelectRecipients.h"
#include "Office365Admin.h"
#include "SessionTypes.h"
#include "JSONUtil.h"
#include "cpprest\json.h"

std::map<wstring, int, Str::keyLessInsensitive> DlgEmailConsent::g_Sizes;
const wstring DlgEmailConsent::g_EmailIDFieldName = _YTEXT("emailid");
const wstring DlgEmailConsent::g_ForwardIDFieldName = _YTEXT("forwardid");
const wstring DlgEmailConsent::g_SubjectIDFieldName = _YTEXT("subjectid");
const wstring DlgEmailConsent::g_ContentIDFieldName = _YTEXT("contentid");
const wstring DlgEmailConsent::g_TenantFieldName = _YTEXT("tenantname");
const wstring DlgEmailConsent::g_AppIdFieldName = _YTEXT("appidname");
const wstring DlgEmailConsent::g_RedirectURLFieldName = _YTEXT("redirectURL");

DlgEmailConsent::DlgEmailConsent(bool p_UltraAdmin, CWnd* p_Parent)
	: ResizableDialog(IDD, p_Parent)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_UltraAdmin(p_UltraAdmin)
{
	// This class hass not been used for a long time.
	ASSERT(FALSE);

	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_SENDEMAILFORCONSENT_SIZE, g_Sizes);
		ASSERT(success);
	}
}

bool DlgEmailConsent::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	const wstring& BtnClick = data.begin()->second;
	ASSERT(data.begin()->first.empty());
	ASSERT(data.size() >= 1);

	bool RetVal = true;

	if (BtnClick == m_CancelButtonText)
	{
		endDialogFixed(IDCANCEL);
		RetVal = false;
	}
	else if (BtnClick == m_SendButtonText)
	{
		for (const auto& Chunk : data)
		{
            if (Chunk.first == g_EmailIDFieldName)
                m_Recipients = Chunk.second;
            else if (Chunk.first == g_ForwardIDFieldName)
                m_CCRecipients = Chunk.second;
            else if (Chunk.first == g_SubjectIDFieldName)
                m_Subject = Chunk.second;
            else if (Chunk.first == g_ContentIDFieldName)
                m_Content = Chunk.second;
            else if (Chunk.first == g_TenantFieldName)
                m_TenantName = Chunk.second;
            else if (Chunk.first == g_AppIdFieldName)
                m_AppId = Chunk.second;
			else if (Chunk.first == g_RedirectURLFieldName)
				m_RedirectURL = Chunk.second;
		}

		endDialogFixed(IDOK);
		RetVal = false;
	}

	return RetVal;
}

bool DlgEmailConsent::OnNewPostedURL(const wstring& url)
{
	return false; // Nothing to do.
}

const wstring& DlgEmailConsent::GetRecipients() const
{
    return m_Recipients;
}

const wstring& DlgEmailConsent::GetCCRecipients() const
{
	return m_CCRecipients;
}

const wstring& DlgEmailConsent::GetSubject() const
{
    return m_Subject;
}

const wstring& DlgEmailConsent::GetContent() const
{
    return m_Content;
}

const wstring& DlgEmailConsent::GetTenantName() const
{
    return m_TenantName;
}

const wstring& DlgEmailConsent::GetAppId() const
{
    return m_AppId;
}

const wstring& DlgEmailConsent::GetRedirectURL() const
{
	return m_RedirectURL;
}

EmailInfo DlgEmailConsent::GetEmailInfoForAdvancedUserSession() const
{
    EmailInfo emailInfo;

    Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());

    emailInfo.m_Content = FillConsentUrlPlaceholders(GetContent(), SessionTypes::GetAdvancedSessionConsentUrl(true).to_string());

    emailInfo.m_Subject = GetSubject();
    emailInfo.m_TenantName = GetTenantName();

    vector<PooledString> recipients;
    for (const auto& recipient : Str::explodeIntoVector(GetRecipients(), wstring(_YTEXT(";"))))
        recipients.emplace_back(recipient);

    emailInfo.m_Recipients = recipients;
    return emailInfo;
}

EmailInfo DlgEmailConsent::GetEmailInfoForUltraAdminSession() const
{
    EmailInfo emailInfo;

    Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());

    emailInfo.m_Content = FillConsentUrlPlaceholders(GetContent(), SessionTypes::GetUltraAdminConsentUrl(GetTenantName(), GetAppId(), GetRedirectURL()).to_string());

    emailInfo.m_Subject = GetSubject();
    emailInfo.m_TenantName = GetTenantName();

    vector<PooledString> recipients;
    for (const auto& recipient : Str::explodeIntoVector(GetRecipients(), wstring(_YTEXT(";"))))
        recipients.emplace_back(recipient);

    emailInfo.m_Recipients = recipients;
    return emailInfo;
}

BOOL DlgEmailConsent::OnInitDialogSpecificResizable()
{
    SetWindowText(_YTEXT("Send Admin Consent Email"));

	CRect htmlRect;
	GetClientRect(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, NULL);
	m_HtmlView->SetPostedDataTarget(this);
	m_HtmlView->SetPostedURLTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
		{
			 { _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
			,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-sendEmailForConsent.js"), _YTEXT("") } } // This file is in resources
			,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("sendEmailForConsent.js"), _YTEXT("") } }, // This file is in resources
		}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("sendEmailForConsent.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const wstring minHeightKey = m_UltraAdmin ? L"min-height" : L"min-height-no-tenant";
			const auto it = g_Sizes.find(minHeightKey);
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-width"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return it->second;
			return HIDPI_XW(htmlRect.Width());
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
	BlockVResize(true);

    return TRUE;
}

void DlgEmailConsent::OnSelectEmailRecipients()
{
	//     DlgSelectRecipients dlg(new GridSelectRecipients(), this);
	//     if (dlg.DoModal() == IDOK)
	//     {
	//         wstring txtRecipients;
	// 
	//         auto emails = dlg.GetSelectedRecipients();
	// 
	//         bool first = true;
	//         for (const auto& email : emails)
	//         {
	//             if (first)
	//             {
	//                 txtRecipients += wstring(email);
	//                 first = false;
	//             }
	//             else
	//                 txtRecipients += wstring(_YTEXT(";")) + wstring(email);
	//         }
	//         m_TxtRecipients.SetWindowText(txtRecipients.c_str());
	//         if (!txtRecipients.empty())
	//             m_BtnOk.EnableWindow(TRUE);
	//     }
}

void DlgEmailConsent::generateJSONScriptData(wstring& p_Output)
{
	m_CancelButtonText = YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str();
	m_SendButtonText = YtriaTranslate::Do(YtriaGridDatabase__ThreadConsoleCommand_1, _YLOC("Send")).c_str();

	wostringstream s;

    const wstring emailTemplate = getEmailTemplate();

    web::json::value root =
        JSON_BEGIN_OBJECT
            JSON_BEGIN_PAIR
                _YTEXT("main"),
                JSON_BEGIN_OBJECT
                    JSON_PAIR_KEY(_YTEXT("id"))                     JSON_PAIR_VALUE(JSON_STRING(_YTEXT("DlgEmailConsent"))),
                    JSON_PAIR_KEY(_YTEXT("method"))                 JSON_PAIR_VALUE(JSON_STRING(_YTEXT("POST"))),
                    JSON_PAIR_KEY(_YTEXT("action"))                 JSON_PAIR_VALUE(JSON_STRING(_YTEXT("#"))),
                JSON_END_OBJECT
            JSON_END_PAIR,
            JSON_BEGIN_PAIR
                _YTEXT("items"),
                JSON_BEGIN_ARRAY
                    JSON_BEGIN_OBJECT
                        JSON_PAIR_KEY(_YTEXT("hbs"))                    JSON_PAIR_VALUE(JSON_STRING(_YTEXT("sendEmailForConsent"))),
                        JSON_PAIR_KEY(_YTEXT("introText"))              JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_YTEXT("")))),
                        JSON_PAIR_KEY(_YTEXT("introTitle"))             JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_1, _YLOC("Please complete the following using the resulting registration information:")).c_str()))),
                        JSON_PAIR_KEY(_YTEXT("footerInf"))              JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_YTEXT("")))),
                        JSON_PAIR_KEY(_YTEXT("emailField"))             JSON_PAIR_VALUE(JSON_STRING(g_EmailIDFieldName)),
                        JSON_PAIR_KEY(_YTEXT("emailLabel"))             JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(GridContacts_customizeGrid_68, _YLOC("Email Address")).c_str()))),
                        JSON_PAIR_KEY(_YTEXT("emailPlaceholder"))       JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
                        JSON_PAIR_KEY(_YTEXT("emailPopup"))             JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(GridContacts_customizeGrid_68, _YLOC("Email Address")).c_str())),
                        JSON_PAIR_KEY(_YTEXT("emailValue"))             JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
                        JSON_PAIR_KEY(_YTEXT("forwardField"))           JSON_PAIR_VALUE(JSON_STRING(g_ForwardIDFieldName)),
                        JSON_PAIR_KEY(_YTEXT("forwardLabel"))           JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgEmailConsent_generateJSONScriptData_7, _YLOC("CC email")).c_str()))),
                        JSON_PAIR_KEY(_YTEXT("forwardPlaceholder"))     JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
                        JSON_PAIR_KEY(_YTEXT("forwardPopup"))           JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgEmailConsent_generateJSONScriptData_8, _YLOC("CC email")).c_str())),
                        JSON_PAIR_KEY(_YTEXT("forwardValue"))           JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
                        JSON_PAIR_KEY(_YTEXT("subjectField"))           JSON_PAIR_VALUE(JSON_STRING(g_SubjectIDFieldName)),
                        JSON_PAIR_KEY(_YTEXT("subjectLabel"))           JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(GridEvents_customizeGrid_2, _YLOC("Subject")).c_str()))),
                        JSON_PAIR_KEY(_YTEXT("subjectPlaceholder"))     JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
                        JSON_PAIR_KEY(_YTEXT("subjectPopup"))           JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
                        JSON_PAIR_KEY(_YTEXT("subjectValue"))           JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
                        JSON_PAIR_KEY(_YTEXT("contentField"))           JSON_PAIR_VALUE(JSON_STRING(g_ContentIDFieldName)),
                        JSON_PAIR_KEY(_YTEXT("contentLabel"))           JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(GridPosts_customizeGrid_42, _YLOC("Content")).c_str()))),
                        JSON_PAIR_KEY(_YTEXT("contentPlaceholder"))     JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
                        JSON_PAIR_KEY(_YTEXT("contentPopup"))           JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(GridPosts_customizeGrid_42, _YLOC("Content")).c_str())),
                        JSON_PAIR_KEY(_YTEXT("contentValue"))           JSON_PAIR_VALUE(JSON_STRING(emailTemplate)),
                        JSON_PAIR_KEY(_YTEXT("tenantField"))            JSON_PAIR_VALUE(JSON_STRING(g_TenantFieldName)),
                        JSON_PAIR_KEY(_YTEXT("tenantLabel"))            JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_2, _YLOC("Your Tenant name:")).c_str()))),
                        JSON_PAIR_KEY(_YTEXT("tenantPlaceholder"))      JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_3, _YLOC("mytenant.onmicrosoft.com")).c_str())),
                        JSON_PAIR_KEY(_YTEXT("tenantPopup"))            JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_4, _YLOC("Please provide valid name")).c_str())),
                        JSON_PAIR_KEY(_YTEXT("tenantValue"))            JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
                        JSON_PAIR_KEY(_YTEXT("showTenant"))             JSON_PAIR_VALUE(JSON_STRING(m_UltraAdmin ? _YTEXT("true") : _YTEXT("false"))),
                        JSON_PAIR_KEY(_YTEXT("appIdField"))             JSON_PAIR_VALUE(JSON_STRING(g_AppIdFieldName)),
                        JSON_PAIR_KEY(_YTEXT("appIdLabel"))             JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_18, _YLOC("Application Id:")).c_str()))),
                        JSON_PAIR_KEY(_YTEXT("appIdPlaceholder"))       JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_9, _YLOC("xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")).c_str())),
                        JSON_PAIR_KEY(_YTEXT("appIdPopup"))             JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgEmailConsent_generateJSONScriptData_16, _YLOC("Please provide an app id")).c_str())),
                        JSON_PAIR_KEY(_YTEXT("appIdValue"))             JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
						JSON_PAIR_KEY(_YTEXT("RedirectURLField"))		JSON_PAIR_VALUE(JSON_STRING(g_RedirectURLFieldName)),
						JSON_PAIR_KEY(_YTEXT("RedirectURLLabel"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgEmailConsent_generateJSONScriptData_17, _YLOC("Application redirect URL")).c_str()))),
						JSON_PAIR_KEY(_YTEXT("RedirectURLPlaceholder"))	JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_8, _YLOC("http://localhost:33366")).c_str())),
						JSON_PAIR_KEY(_YTEXT("RedirectURLPopup"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
						JSON_PAIR_KEY(_YTEXT("RedirectURLValue"))		JSON_PAIR_VALUE(JSON_STRING(m_RedirectURL)),
                        JSON_PAIR_KEY(_YTEXT("showAppId"))              JSON_PAIR_VALUE(JSON_STRING(m_UltraAdmin ? _YTEXT("true") : _YTEXT("false")))
                    JSON_END_OBJECT,
                    JSON_BEGIN_OBJECT
                        JSON_BEGIN_PAIR
                            _YTEXT("hbs"), JSON_STRING(_YTEXT("Button"))
                        JSON_END_PAIR,
                        JSON_BEGIN_PAIR
                            _YTEXT("choices"),
                            JSON_BEGIN_ARRAY
                                JSON_BEGIN_OBJECT
                                    JSON_PAIR_KEY(_YTEXT("type"))               JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
                                    JSON_PAIR_KEY(_YTEXT("label"))              JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(m_SendButtonText))),
                                    JSON_PAIR_KEY(_YTEXT("attribute"))          JSON_PAIR_VALUE(JSON_STRING(_YTEXT("post"))),
                                    JSON_PAIR_KEY(_YTEXT("class"))              JSON_PAIR_VALUE(JSON_STRING(_YTEXT("is-primary")))
                                JSON_END_OBJECT,
                                JSON_BEGIN_OBJECT
                                    JSON_PAIR_KEY(_YTEXT("type"))               JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
                                    JSON_PAIR_KEY(_YTEXT("label"))              JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(m_CancelButtonText))),
                                    JSON_PAIR_KEY(_YTEXT("attribute"))          JSON_PAIR_VALUE(JSON_STRING(_YTEXT("cancel"))),
                                JSON_END_OBJECT
                            JSON_END_ARRAY
                        JSON_END_PAIR
                    JSON_END_OBJECT
                JSON_END_ARRAY
            JSON_END_PAIR,
        JSON_END_OBJECT;

    p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}

wstring DlgEmailConsent::getEmailTemplate() const
{
    wstring value = _YTEXT("Hello admin, please click on that link to give consent to sapio365: %CONSENT_LINK%");
    return value;
}

wstring DlgEmailConsent::FillConsentUrlPlaceholders(wstring p_Content, const wstring& p_ConsentUrl) const
{
    static const wstring URL_PLACEHOLDER = _YTEXT("%CONSENT_LINK%");

    size_t searchIndex = 0;
    size_t placeholderPos = 0;

    do 
    {
        placeholderPos = p_Content.find(URL_PLACEHOLDER, searchIndex);

        if (placeholderPos != wstring::npos)
        {
            p_Content.replace(placeholderPos, URL_PLACEHOLDER.size(), p_ConsentUrl);
            searchIndex = placeholderPos + URL_PLACEHOLDER.size();
        }
    } while (placeholderPos != wstring::npos);

    return p_Content;
}
