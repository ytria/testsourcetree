#include "O365UpdateOperation.h"


O365UpdateOperation::O365UpdateOperation(const vector<GridBackendField>& p_PrimaryKey)
    : m_PrimaryKey(p_PrimaryKey)
{
}

O365UpdateOperation::O365UpdateOperation(const O365UpdateOperation& other, const vector<GridBackendField>& p_PrimaryKey)
	: m_PrimaryKey(p_PrimaryKey)
	, m_Results(other.m_Results)
{

}

const vector<GridBackendField>& O365UpdateOperation::GetPrimaryKey() const
{
    return m_PrimaryKey;
}

PooledString O365UpdateOperation::GetPkFieldStr(GridBackendColumn* p_Column) const
{
    PooledString result;

    ASSERT(nullptr != p_Column);
    if (nullptr != p_Column)
		return GetPkFieldStr(p_Column->GetID());
    
    ASSERT(false);
    return result;
}

PooledString O365UpdateOperation::GetPkFieldStr(UINT p_ColumnID) const
{
	PooledString result;

	for (const auto& field : m_PrimaryKey)
	{
		if (field.GetColID() == p_ColumnID)
			return field.GetValueStr();
	}

	ASSERT(false);
	return result;
}

const GridBackendField& O365UpdateOperation::GetPkField(GridBackendColumn* p_Column) const
{
	ASSERT(nullptr != p_Column);
	if (nullptr != p_Column)
		return GetPkField(p_Column->GetID());

	ASSERT(false);
	return GridBackendField::g_GenericConstField;
}

const GridBackendField& O365UpdateOperation::GetPkField(UINT p_ColumnID) const
{
	for (const auto& field : m_PrimaryKey)
	{
		if (field.GetColID() == p_ColumnID)
			return field;
	}

	ASSERT(false);
	return GridBackendField::g_GenericConstField;
}

void O365UpdateOperation::StoreResult(const HttpResultWithError& p_HttpResultWithError)
{
	m_Results.emplace_back(p_HttpResultWithError);
}

void O365UpdateOperation::StoreResults(const vector<HttpResultWithError>& p_HttpResultsWithError)
{
	m_Results.insert(m_Results.end(), p_HttpResultsWithError.begin(), p_HttpResultsWithError.end());
}

const std::vector<HttpResultWithError>& O365UpdateOperation::GetResults() const
{
	return m_Results;
}
