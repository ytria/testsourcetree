#pragma once

#include "BusinessUser.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class UserNonPropDeserializer : public JsonObjectDeserializer
                              , public Encapsulate<BusinessUser>
{
public:
    UserNonPropDeserializer() = default;
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

