#include "OrganizationRequester.h"

#include "GraphPageRequester.h"
#include "OrganizationDeserializer.h"
#include "MSGraphSession.h"
#include "PaginatedRequestResults.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "ValueListDeserializer.h"
#include "MsGraphHttpRequestLogger.h"

OrganizationRequester::OrganizationRequester(bool p_UseRoleSession, const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_UseRoleSession(p_UseRoleSession),
	m_Logger(p_Logger)
{

}

TaskWrapper<void> OrganizationRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	auto session = m_UseRoleSession ? p_Session->GetMSGraphSession(Sapio365Session::USER_SESSION) : p_Session->GetMainMSGraphSession();
    return Send(p_Session, session, p_TaskData);
}

TaskWrapper<void> OrganizationRequester::Send(const std::shared_ptr<const Sapio365Session>& p_Session, std::shared_ptr<MSGraphSession> p_GraphSession, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<Organization, OrganizationDeserializer>>();

	web::uri_builder uri(_YTEXT("organization"));

	SessionIdentifier identifier;
	if (p_Session)
		identifier = p_Session->GetIdentifier();

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, identifier);
	auto requester = std::make_shared<GraphPageRequester>(m_Deserializer, nullptr, httpLogger);
	return MsGraphPaginator(uri.to_uri(), requester, m_Logger, MsGraphPaginator::g_DefaultPageSize).Paginate(p_GraphSession, p_TaskData);
	// Default paginator used a retryer, not needed here as there's never more than one page of Organization (and moreover always only one)
	//return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Deserializer, m_Logger, httpLogger, p_TaskData.GetOriginator()).Paginate(p_GraphSession, p_TaskData);
}

const vector<Organization>& OrganizationRequester::GetData() const
{
    return m_Deserializer->GetData();
}
