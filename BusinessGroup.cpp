#include "BusinessGroup.h"

#include "BasicPageRequestLogger.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessGroupSetting.h"
#include "BusinessGroupSettingTemplate.h"
#include "BusinessUser.h"
#include "CachedGroup.h"
#include "GroupSettingDeserializer.h"
#include "GroupSettingsRequester.h"
#include "MsGraphFieldNames.h"
#include "MSGraphCommonData.h"
#include "MsGraphHttpRequestLogger.h"
#include "MSGraphSession.h"
#include "MSGraphUtil.h"
#include "O365AdminUtil.h"
#include "safeTaskCall.h"
#include "Sapio365Session.h"
#include "Team.h"
#include "TimeUtil.h"
#include "ValueListDeserializer.h"
#include "YSafeCreateTask.h"
#include "YtriaFieldsConstants.h"

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessGroup>("Group") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Group"))))
		.constructor()(policy::ctor::as_object)
// **** Basic Info
		.property(O365_GROUP_DISPLAYNAME, &BusinessGroup::m_DisplayName)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_GROUP_DESCRIPTION, &BusinessGroup::m_Description)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_GROUP_CLASSIFICATION, &BusinessGroup::m_Classification)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_VISIBILITY, &BusinessGroup::m_Visibility)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_GROUP_GROUPTYPES, &BusinessGroup::m_GroupTypes)(
			//metadata(MetadataKeys::ReadOnly, true),
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_GROUP_SECURITYENABLED, &BusinessGroup::m_IsSecurityEnabled)(
			metadata(MetadataKeys::ReadOnly, true),
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_GROUP_CREATEDDATETIME, &BusinessGroup::m_CreatedDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_RENEWEDDATETIME, &BusinessGroup::m_RenewedDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)

// **** Mail Info
		.property(O365_GROUP_MAILENABLED, &BusinessGroup::m_IsMailEnabled)(
			metadata(MetadataKeys::ReadOnly, true),
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_GROUP_MAIL, &BusinessGroup::m_Mail)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_PROXYADDRESSES, &BusinessGroup::m_ProxyAddresses)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_MAILNICKNAME, &BusinessGroup::m_MailNickname)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		/*.property(O365_GROUP_ISSUBSCRIBEDBYMAIL, &BusinessGroup::m_IsSubscribedByMail)(
			metadata(MetadataKeys::ReadOnly, true)
			)*/
		.property(O365_GROUP_ALLOWEXTERNALSENDERS, &BusinessGroup::m_IsAllowExternalSenders)
		.property(O365_GROUP_AUTOSUBSCRIBENEWMEMBERS, &BusinessGroup::m_IsAutoSubscribeNewMembers)
		.property(O365_GROUP_UNSEENCOUNT, &BusinessGroup::m_UnseenCount)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_HIDEFROMOUTLOOKCLIENTS, &BusinessGroup::m_HideFromOutlookClients)
		.property(O365_GROUP_HIDEFROMADDRESSLISTS, &BusinessGroup::m_HideFromAddressLists)
		.property(O365_GROUP_LICENSEPROCESSINGSTATE, &BusinessGroup::m_LicenseProcessingState)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_SECURITYIDENTIFIER, &BusinessGroup::m_SecurityIdentifier)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_ASSIGNEDLICENSES, &BusinessUser::m_AssignedLicenses)(
			metadata(MetadataKeys::ReadOnly, true)
			)

// **** On Premises
		.property(O365_GROUP_ONPREMISESSYNCENABLED, &BusinessGroup::m_IsOnPremisesSyncEnabled)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_ONPREMISESLASTSYNCDATETIME, &BusinessGroup::m_OnPremisesLastSyncDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_ONPREMISESSECURITYIDENTIFIER, &BusinessGroup::m_OnPremisesSecurityIdentifier)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_ONPREMISESPROVISIONINGERRORS, &BusinessGroup::m_OnPremisesProvisioningErrors)(
			metadata(MetadataKeys::ReadOnly, true)
			)
// **** Other
        .property(YTRIA_GROUP_GROUPUNIFIEDGUESTSETTINGACTIVATED, &BusinessGroup::m_SettingAllowToAddGuestsIsActivated)(
            metadata(MetadataKeys::ReadOnly, true)
            )
        .property(YTRIA_GROUP_SETTINGALLOWTOADDGUESTS, &BusinessGroup::m_SettingAllowToAddGuests)(
            metadata(MetadataKeys::ReadOnly, true)
            )
        .property(YTRIA_GROUP_SETTINGALLOWTOADDGUESTSID, &BusinessGroup::m_SettingAllowToAddGuestsId)(
            metadata(MetadataKeys::ReadOnly, true)
            )
		.property(O365_GROUP_RESOURCEBEHAVIOROPTIONS, &BusinessGroup::m_ResourceBehaviorOptions)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_RESOURCEPROVISIONINGOPTIONS, &BusinessGroup::m_ResourceProvisioningOptions)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_GROUP_PREFERREDDATALOCATION, &BusinessGroup::m_PreferredDataLocation)

		/* Beta API */
		.property(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE, &BusinessGroup::m_MembershipRuleProcessingState)(
			metadata(MetadataKeys::BetaAPI, true) // Duplicated from Group, kinda bad design..
			)
		.property(O365_GROUP_MEMBERSHIPRULE, &BusinessGroup::m_MembershipRule)(
			metadata(MetadataKeys::BetaAPI, true) // Duplicated from Group, kinda bad design..
			)
		.property(O365_GROUP_THEME, &BusinessGroup::m_Theme)(
			metadata(MetadataKeys::BetaAPI, true) // Duplicated from Group, kinda bad design..
			)
		.property(O365_GROUP_PREFERREDLANGUAGE, &BusinessGroup::m_PreferredLanguage)(
			metadata(MetadataKeys::ReadOnly, true),
			metadata(MetadataKeys::BetaAPI, true) // Duplicated from Group, kinda bad design..
			)

		// *** ONLY FOR DELETED GROUP
		.property(O365_GROUP_DELETEDDATETIME, &BusinessGroup::m_DeletedDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)

		// For Team editing only
		.property(YTRIA_GROUP_TEAM, &BusinessGroup::m_Team)(
			metadata(MetadataKeys::ReadOnly, true) // To avoid automatic editing stuff
			)

		// For Team archive/unarchive only
		.property("isArchived", &BusinessGroup::m_PendingArchivedTeam)(
			metadata(MetadataKeys::ReadOnly, true) // To avoid automatic editing stuff
			)
		.property("setSpoSiteReadOnlyForMembers", &BusinessGroup::m_PendingSpoSiteReadOnlyForMembers)(
			metadata(MetadataKeys::ReadOnly, true) // To avoid automatic editing stuff
			)

		.property(YTRIA_GROUP_ISYAMMER, &BusinessGroup::m_IsYammer)(
			metadata(MetadataKeys::ReadOnly, true),
			metadata(MetadataKeys::BetaAPI, true) // Duplicated from Group, kinda bad design..
			)
		;
}

namespace
{
	Group CachedToGroup(const CachedGroup& p_Cachedgroup)
	{
		Group group;
		group.m_Id = p_Cachedgroup.GetID();
		group.m_DisplayName = p_Cachedgroup.GetDisplayName();
		group.m_Mail = p_Cachedgroup.GetMail();
		group.m_GroupTypes = p_Cachedgroup.GetGroupTypes();
		group.m_MailEnabled = p_Cachedgroup.GetMailEnabled();
		group.m_SecurityEnabled = p_Cachedgroup.GetSecurityEnabled();
		group.m_ResourceProvisioningOptions = p_Cachedgroup.GetResourceProvisioningOptions();
		return group;
	}
}

wstring BusinessGroup::g_Office365Group;
//const wstring BusinessGroup::g_DynamicGroup = _TLOC("Dynamic group");
wstring BusinessGroup::g_DistributionList;
wstring BusinessGroup::g_SecurityGroup;
wstring BusinessGroup::g_MailEnabledSecurityGroup;
wstring BusinessGroup::g_Team;
wstring BusinessGroup::g_Yammer;
wstring BusinessGroup::g_Archived;

const wstring BusinessGroup::g_DynamicMembership = _YTEXT("DynamicMembership");
const wstring BusinessGroup::g_TeamProvisionningOption = _YTEXT("Team");

bool BusinessGroup::g_InitLocalization = false;

BusinessGroup::BusinessGroup()
	: m_HasTeamInfo(false)
{
    if (!g_InitLocalization)
    {
        g_Office365Group = YtriaTranslate::Do(BusinessGroup_g_Office365Group_1, _YLOC("Office 365 group"));
        g_DistributionList = YtriaTranslate::Do(BusinessGroup_g_DistributionList_1, _YLOC("Distribution list"));
        g_SecurityGroup = YtriaTranslate::Do(BusinessGroup_g_SecurityGroup_1, _YLOC("Security Group"));
        g_MailEnabledSecurityGroup = YtriaTranslate::Do(BusinessGroup_g_MailEnabledSecurityGroup_1, _YLOC("Mail-enabled security group"));
		g_Team = YtriaTranslate::Do(GridTemplateGroups_updateRowTeam_1, _YLOC("Team"));
		g_Yammer = _T("Yammer");
		g_Archived = _T("Archived");

        g_InitLocalization = true;
    }
}

BusinessGroup::BusinessGroup(const Group& p_JsonGroup)
    : BusinessGroup()
{
	SetValuesFrom(p_JsonGroup);
}


BusinessGroup::BusinessGroup(const BusinessGroup& p_Other)
	:  BusinessObject(dynamic_cast<const BusinessObject&>(p_Other))
{
	*this = p_Other;
}

BusinessGroup::BusinessGroup(const CachedGroup& p_CachedGroup)
	: BusinessGroup(CachedToGroup(p_CachedGroup))
{
	SetRBACDelegationID(p_CachedGroup.GetRBACDelegationID());
}

BusinessGroup& BusinessGroup::operator=(const BusinessGroup& p_Other)
{
	BusinessObject::operator=(p_Other);

	m_ProxyAddresses = p_Other.m_ProxyAddresses;

	m_DisplayName = p_Other.m_DisplayName;
	m_IsAllowExternalSenders = p_Other.m_IsAllowExternalSenders;
	m_IsAutoSubscribeNewMembers = p_Other.m_IsAutoSubscribeNewMembers;
	m_Classification = p_Other.m_Classification;
	m_CreatedDateTime = p_Other.m_CreatedDateTime;
	m_RenewedDateTime = p_Other.m_RenewedDateTime;
	m_Description = p_Other.m_Description;
	m_GroupTypes = p_Other.m_GroupTypes;
	//m_IsSubscribedByMail  = p_Other.m_IsSubscribedByMail;
	m_Mail = p_Other.m_Mail;
	m_IsMailEnabled = p_Other.m_IsMailEnabled;
	m_MailNickname = p_Other.m_MailNickname;
	m_OnPremisesLastSyncDateTime = p_Other.m_OnPremisesLastSyncDateTime;
	m_OnPremisesSecurityIdentifier = p_Other.m_OnPremisesSecurityIdentifier;
	m_OnPremisesProvisioningErrors = p_Other.m_OnPremisesProvisioningErrors;
	m_IsOnPremisesSyncEnabled = p_Other.m_IsOnPremisesSyncEnabled;
	m_IsSecurityEnabled = p_Other.m_IsSecurityEnabled;
	m_UnseenCount = p_Other.m_UnseenCount;
	m_Visibility = p_Other.m_Visibility;
	m_Team = p_Other.m_Team;
	m_PendingArchivedTeam = p_Other.m_PendingArchivedTeam;
	m_PendingSpoSiteReadOnlyForMembers = p_Other.m_PendingSpoSiteReadOnlyForMembers;
	m_UserCreatedOnBehalfOf = p_Other.m_UserCreatedOnBehalfOf;
	m_HasTeamInfo = p_Other.m_HasTeamInfo;
	m_SettingAllowToAddGuestsIsActivated = p_Other.m_SettingAllowToAddGuestsIsActivated;
	m_SettingAllowToAddGuests = p_Other.m_SettingAllowToAddGuests;
	m_SettingAllowToAddGuestsId = p_Other.m_SettingAllowToAddGuestsId;
	m_DeletedDateTime = p_Other.m_DeletedDateTime;

	m_LCPStatus = p_Other.m_LCPStatus;
	m_CombinedGroupType = p_Other.m_CombinedGroupType;
	m_DynamicMembership = p_Other.m_DynamicMembership;

	m_ChildrenUsers = p_Other.m_ChildrenUsers;
	m_ChildrenGroups = p_Other.m_ChildrenGroups;
	m_ChildrenOrgContacts = p_Other.m_ChildrenOrgContacts;
	m_Parents = p_Other.m_Parents;

	m_OwnersUsers = p_Other.m_OwnersUsers;
	//m_OwnersGroups = p_Other.m_OwnersGroups;

	m_AuthorAcceptedUsers = p_Other.m_AuthorAcceptedUsers;
	m_AuthorAcceptedGroups = p_Other.m_AuthorAcceptedGroups;
	m_AuthorAcceptedOrgContacts = p_Other.m_AuthorAcceptedOrgContacts;
	m_AuthorRejectedUsers = p_Other.m_AuthorRejectedUsers;
	m_AuthorRejectedGroups = p_Other.m_AuthorRejectedGroups;
	m_AuthorRejectedOrgContacts = p_Other.m_AuthorRejectedOrgContacts;

	m_LCPId = p_Other.m_LCPId;

	m_TeamError = p_Other.m_TeamError;
	m_GroupSettingsError = p_Other.m_GroupSettingsError;
	m_UserCreatedOnBehalfOfError = p_Other.m_UserCreatedOnBehalfOfError;
	m_OwnersError = p_Other.m_OwnersError;
	m_LCPStatusError = p_Other.m_LCPStatusError;
	m_DriveError = p_Other.m_DriveError;
	m_IsYammerError = p_Other.m_IsYammerError;
	m_ChildrenError = p_Other.m_ChildrenError;
	m_AuthorsAcceptedError = p_Other.m_AuthorsAcceptedError;
	m_AuthorsRejectedError = p_Other.m_AuthorsRejectedError;

	m_ResourceBehaviorOptions = p_Other.m_ResourceBehaviorOptions;
	m_ResourceProvisioningOptions = p_Other.m_ResourceProvisioningOptions;

	m_PreferredDataLocation = p_Other.m_PreferredDataLocation;

	m_MembersCount = p_Other.m_MembersCount;
	m_Drive = p_Other.m_Drive;

	m_HideFromOutlookClients = p_Other.m_HideFromOutlookClients;
	m_HideFromAddressLists = p_Other.m_HideFromAddressLists;
	m_LicenseProcessingState = p_Other.m_LicenseProcessingState;
	m_SecurityIdentifier = p_Other.m_SecurityIdentifier;

	/* Beta API */
	m_MembershipRuleProcessingState = p_Other.m_MembershipRuleProcessingState;
	m_MembershipRule = p_Other.m_MembershipRule;
	m_Theme = p_Other.m_Theme;
	m_PreferredLanguage = p_Other.m_PreferredLanguage;

	m_IsTeamForEdit = p_Other.m_IsTeamForEdit;

	m_IsYammer = p_Other.m_IsYammer;

	m_AssignedLicenses = p_Other.m_AssignedLicenses;

	return *this;
}

BusinessGroup& BusinessGroup::MergeWith(const BusinessGroup& p_Other)
{
	/** BusinessObject **/
	if (!p_Other.m_Id.IsEmpty())
		m_Id = p_Other.m_Id;
	if (p_Other.m_ErrorMessage)
		m_ErrorMessage = p_Other.m_ErrorMessage;
	m_Flags |= p_Other.m_Flags;
	ASSERT(m_RBAC_DelegationID == p_Other.m_RBAC_DelegationID || NO_RBAC == m_RBAC_DelegationID || p_Other.m_RBAC_DelegationID == NO_RBAC);
	if (NO_RBAC == m_RBAC_DelegationID && NO_RBAC != p_Other.m_RBAC_DelegationID)
		m_RBAC_DelegationID = p_Other.m_RBAC_DelegationID;

	for (const auto& item : p_Other.m_DataBlockDates)
	{
		if (item.second)
			m_DataBlockDates[item.first] = item.second;
	}
	/********************/

	if (p_Other.m_ProxyAddresses)
		m_ProxyAddresses = p_Other.m_ProxyAddresses;
	if (p_Other.m_DisplayName)
		m_DisplayName = p_Other.m_DisplayName;
	if (p_Other.m_IsAllowExternalSenders)
		m_IsAllowExternalSenders = p_Other.m_IsAllowExternalSenders;
	if (p_Other.m_IsAutoSubscribeNewMembers)
		m_IsAutoSubscribeNewMembers = p_Other.m_IsAutoSubscribeNewMembers;
	if (p_Other.m_Classification)
		m_Classification = p_Other.m_Classification;
	if (p_Other.m_CreatedDateTime)
		m_CreatedDateTime = p_Other.m_CreatedDateTime;
	if (p_Other.m_RenewedDateTime)
		m_RenewedDateTime = p_Other.m_RenewedDateTime;
	if (p_Other.m_Description)
		m_Description = p_Other.m_Description;
	if (p_Other.m_GroupTypes)
		m_GroupTypes = p_Other.m_GroupTypes;
	//if (p_Other.m_IsSubscribedByMail)
	//	m_IsSubscribedByMail  = p_Other.m_IsSubscribedByMail;
	if (p_Other.m_Mail)
		m_Mail = p_Other.m_Mail;
	if (p_Other.m_IsMailEnabled)
		m_IsMailEnabled = p_Other.m_IsMailEnabled;
	if (p_Other.m_MailNickname)
		m_MailNickname = p_Other.m_MailNickname;
	if (p_Other.m_OnPremisesLastSyncDateTime)
		m_OnPremisesLastSyncDateTime = p_Other.m_OnPremisesLastSyncDateTime;
	if (p_Other.m_OnPremisesSecurityIdentifier)
		m_OnPremisesSecurityIdentifier = p_Other.m_OnPremisesSecurityIdentifier;
	if (p_Other.m_OnPremisesProvisioningErrors)
		m_OnPremisesProvisioningErrors = p_Other.m_OnPremisesProvisioningErrors;
	if (p_Other.m_IsOnPremisesSyncEnabled)
		m_IsOnPremisesSyncEnabled = p_Other.m_IsOnPremisesSyncEnabled;
	if (p_Other.m_IsSecurityEnabled)
		m_IsSecurityEnabled = p_Other.m_IsSecurityEnabled;
	if (p_Other.m_UnseenCount)
		m_UnseenCount = p_Other.m_UnseenCount;
	if (p_Other.m_Visibility)
		m_Visibility = p_Other.m_Visibility;
	if (p_Other.m_Team)
		m_Team = p_Other.m_Team;
	if (p_Other.m_UserCreatedOnBehalfOf)
		m_UserCreatedOnBehalfOf = p_Other.m_UserCreatedOnBehalfOf;
	if (p_Other.m_HasTeamInfo)
		m_HasTeamInfo = p_Other.m_HasTeamInfo;
	if (p_Other.m_SettingAllowToAddGuestsIsActivated)
		m_SettingAllowToAddGuestsIsActivated = p_Other.m_SettingAllowToAddGuestsIsActivated;
	if (p_Other.m_SettingAllowToAddGuests)
		m_SettingAllowToAddGuests = p_Other.m_SettingAllowToAddGuests;
	if (p_Other.m_SettingAllowToAddGuestsId)
		m_SettingAllowToAddGuestsId = p_Other.m_SettingAllowToAddGuestsId;

	if (p_Other.m_DeletedDateTime)
		m_DeletedDateTime = p_Other.m_DeletedDateTime;

	if (p_Other.m_LCPStatus)
		m_LCPStatus = p_Other.m_LCPStatus;
	if (p_Other.m_CombinedGroupType)
		m_CombinedGroupType = p_Other.m_CombinedGroupType;
	if (p_Other.m_DynamicMembership)
		m_DynamicMembership = p_Other.m_DynamicMembership;

	if (!p_Other.m_ChildrenUsers.empty())
		m_ChildrenUsers = p_Other.m_ChildrenUsers;
	if (!p_Other.m_ChildrenGroups.empty())
		m_ChildrenGroups = p_Other.m_ChildrenGroups;
	if (!p_Other.m_ChildrenOrgContacts.empty())
		m_ChildrenOrgContacts = p_Other.m_ChildrenOrgContacts;
	if (!p_Other.m_Parents.empty())
		m_Parents = p_Other.m_Parents;

	if (!p_Other.m_OwnersUsers.empty())
		m_OwnersUsers = p_Other.m_OwnersUsers;
	/*if (!p_Other.m_OwnersGroups.empty())
		m_OwnersGroups = p_Other.m_OwnersGroups;*/

	if (!p_Other.m_AuthorAcceptedUsers.empty())
		m_AuthorAcceptedUsers = p_Other.m_AuthorAcceptedUsers;
	if (!p_Other.m_AuthorAcceptedGroups.empty())
		m_AuthorAcceptedGroups = p_Other.m_AuthorAcceptedGroups;
	if (!p_Other.m_AuthorAcceptedOrgContacts.empty())
		m_AuthorAcceptedOrgContacts = p_Other.m_AuthorAcceptedOrgContacts;
	if (!p_Other.m_AuthorRejectedUsers.empty())
		m_AuthorRejectedUsers = p_Other.m_AuthorRejectedUsers;
	if (!p_Other.m_AuthorRejectedGroups.empty())
		m_AuthorRejectedGroups = p_Other.m_AuthorRejectedGroups;
	if (!p_Other.m_AuthorRejectedOrgContacts.empty())
		m_AuthorRejectedOrgContacts = p_Other.m_AuthorRejectedOrgContacts;

	if (p_Other.m_LCPId)
		m_LCPId = p_Other.m_LCPId;

	if (p_Other.m_TeamError)
		m_TeamError = p_Other.m_TeamError;
	if (p_Other.m_GroupSettingsError)
		m_GroupSettingsError = p_Other.m_GroupSettingsError;
	if (p_Other.m_UserCreatedOnBehalfOfError)
		m_UserCreatedOnBehalfOfError = p_Other.m_UserCreatedOnBehalfOfError;
	if (p_Other.m_OwnersError)
		m_OwnersError = p_Other.m_OwnersError;
	if (p_Other.m_LCPStatusError)
		m_LCPStatusError = p_Other.m_LCPStatusError;

	if (p_Other.m_ResourceBehaviorOptions)
		m_ResourceBehaviorOptions = p_Other.m_ResourceBehaviorOptions;
	if (p_Other.m_ResourceProvisioningOptions)
		m_ResourceProvisioningOptions = p_Other.m_ResourceProvisioningOptions;

	if (p_Other.m_PreferredDataLocation)
		m_PreferredDataLocation = p_Other.m_PreferredDataLocation;

	if (p_Other.m_MembersCount)
		m_MembersCount = p_Other.m_MembersCount;
		
	if (p_Other.m_Drive)
		m_Drive = p_Other.m_Drive;

	if (p_Other.m_HideFromOutlookClients)
		m_HideFromOutlookClients = p_Other.m_HideFromOutlookClients;

	if (p_Other.m_HideFromAddressLists)
		m_HideFromAddressLists = p_Other.m_HideFromAddressLists;

	if (p_Other.m_LicenseProcessingState)
		m_LicenseProcessingState = p_Other.m_LicenseProcessingState;

	if (p_Other.m_SecurityIdentifier)
		m_SecurityIdentifier = p_Other.m_SecurityIdentifier;

	if (p_Other.m_DriveError)
		m_DriveError = p_Other.m_DriveError;

	if (p_Other.m_IsYammerError)
		m_IsYammerError = p_Other.m_IsYammerError;

	/* Beta API */
	if (p_Other.m_MembershipRuleProcessingState)
		m_MembershipRuleProcessingState = p_Other.m_MembershipRuleProcessingState;
	if (p_Other.m_MembershipRule)
		m_MembershipRule = p_Other.m_MembershipRule;
	if (p_Other.m_Theme)
		m_Theme = p_Other.m_Theme;
	if (p_Other.m_PreferredLanguage)
		m_PreferredLanguage = p_Other.m_PreferredLanguage;

	if (p_Other.m_IsYammer)
		m_IsYammer = p_Other.m_IsYammer;

	if (p_Other.m_AssignedLicenses)
		m_AssignedLicenses = p_Other.m_AssignedLicenses;

	return *this;
}

void BusinessGroup::SetValuesFrom(const Group& p_JsonGroup)
{
	SetID(p_JsonGroup.m_Id);
	SetDisplayName(p_JsonGroup.m_DisplayName);
	SetIsAllowExternalSenders(p_JsonGroup.m_IsAllowExternalSenders);
	SetIsAutoSubscribeNewMembers(p_JsonGroup.m_IsAutoSubscribeNewMembers);
	SetClassification(p_JsonGroup.m_Classification);
	SetCreatedDateTime(p_JsonGroup.m_CreatedDateTime);
	SetRenewedDateTime(p_JsonGroup.m_RenewedDateTime);
	SetDescription(p_JsonGroup.m_Description);
	SetGroupTypes(p_JsonGroup.m_GroupTypes);
	SetProxyAddresses(p_JsonGroup.m_ProxyAddresses);
	//SetIsSubscribedByMail(p_JsonGroup.m_IsSubscribedByMail);
	SetMail(p_JsonGroup.m_Mail);
	SetIsMailEnabled(p_JsonGroup.m_MailEnabled);
	SetMailNickname(p_JsonGroup.m_MailNickname);
	SetOnPremisesLastSyncDateTime(p_JsonGroup.m_OnPremisesLastSyncDateTime);
	SetOnPremisesSecurityIdentifier(p_JsonGroup.m_OnPremisesSecurityIdentifier);
	SetOnPremisesProvisioningErrors(p_JsonGroup.m_OnPremisesProvisioningErrors);
	SetOnPremisesSyncEnabled(p_JsonGroup.m_IsOnPremisesSyncEnabled);
	SetIsSecurityEnabled(p_JsonGroup.m_SecurityEnabled);
	SetUnseenCount(p_JsonGroup.m_UnseenCount);
	SetVisibility(p_JsonGroup.m_Visibility);
	SetDeletedDateTime(p_JsonGroup.m_DeletedDateTime);
	SetPreferredDataLocation(p_JsonGroup.m_PreferredDataLocation);
	SetHideFromOutlookClients(p_JsonGroup.m_HideFromOutlookClients);
	SetHideFromAddressLists(p_JsonGroup.m_HideFromAddressLists);
	SetLicenseProcessingState(p_JsonGroup.m_LicenseProcessingState);
	SetSecurityIdentifier(p_JsonGroup.m_SecurityIdentifier);

	m_ResourceProvisioningOptions = p_JsonGroup.m_ResourceProvisioningOptions;

	/* Beta API */
	SetMembershipRuleProcessingState(p_JsonGroup.m_MembershipRuleProcessingState);
	SetMembershipRule(p_JsonGroup.m_MembershipRule);
	SetTheme(p_JsonGroup.m_Theme);
	SetPreferredLanguage(p_JsonGroup.m_PreferredLanguage);
}

void BusinessGroup::SetValuesFrom(const CachedGroup& p_CachedGroup)
{
	SetValuesFrom(CachedToGroup(p_CachedGroup));
	SetRBACDelegationID(p_CachedGroup.GetRBACDelegationID());
}

bool BusinessGroup::IsMoreLoaded() const
{
	// FIXME: LoadMore is still made of several request all at once.
	// If we have this block we should have all.
	// Be careful, some blocks are not requested for every group type.
	return GetDataDate(GBI::OWNERS).is_initialized();
}

const boost::YOpt<PooledString>& BusinessGroup::GetDisplayName() const
{
	return m_DisplayName;
}

Group BusinessGroup::ToGroup(const BusinessGroup & p_BusinessGroup)
{
    Group group;

    group.m_Id = p_BusinessGroup.GetID();
    group.m_DisplayName = p_BusinessGroup.GetDisplayName();
    group.m_IsAllowExternalSenders = p_BusinessGroup.IsAllowExternalSenders();
    group.m_IsAutoSubscribeNewMembers = p_BusinessGroup.IsAutoSubscribeNewMembers();
	group.m_Classification = p_BusinessGroup.GetClassification();
	if (p_BusinessGroup.GetCreatedDateTime().is_initialized())
		group.m_CreatedDateTime = TimeUtil::GetInstance().GetISO8601String(p_BusinessGroup.GetCreatedDateTime().get());
	if (p_BusinessGroup.GetRenewedDateTime().is_initialized())
		group.m_RenewedDateTime = TimeUtil::GetInstance().GetISO8601String(p_BusinessGroup.GetRenewedDateTime().get());
    group.m_Description = p_BusinessGroup.GetDescription();
	if (p_BusinessGroup.GetGroupTypes())
		group.m_GroupTypes = *p_BusinessGroup.GetGroupTypes();
	if (p_BusinessGroup.GetProxyAddresses())
		group.m_ProxyAddresses = *p_BusinessGroup.GetProxyAddresses();
    //group.m_IsSubscribedByMail = p_BusinessGroup.IsSubscribedByMail();
    group.m_Mail = p_BusinessGroup.GetMail();
    group.m_MailEnabled = p_BusinessGroup.IsMailEnabled();
    group.m_MailNickname = p_BusinessGroup.GetMailNickname();
	if (p_BusinessGroup.GetOnPremisesLastSyncDateTime().is_initialized())
		group.m_OnPremisesLastSyncDateTime = TimeUtil::GetInstance().GetISO8601String(p_BusinessGroup.GetOnPremisesLastSyncDateTime().get());
    group.m_OnPremisesSecurityIdentifier = p_BusinessGroup.GetOnPremisesSecurityIdentifier();
	group.m_OnPremisesProvisioningErrors = p_BusinessGroup.GetOnPremisesProvisioningErrors();
    group.m_IsOnPremisesSyncEnabled = p_BusinessGroup.IsOnPremisesSyncEnabled();
	group.m_SecurityEnabled = p_BusinessGroup.IsSecurityEnabled();
	group.m_HideFromOutlookClients = p_BusinessGroup.GetHideFromOutlookClients();
	group.m_HideFromAddressLists = p_BusinessGroup.GetHideFromAddressLists();
	group.m_LicenseProcessingState = p_BusinessGroup.GetLicenseProcessingState();
	group.m_SecurityIdentifier = p_BusinessGroup.GetSecurityIdentifier();

    group.m_UnseenCount = p_BusinessGroup.GetUnseenCount();
	group.m_Visibility = p_BusinessGroup.GetVisibility();
	if (p_BusinessGroup.GetDeletedDateTime().is_initialized())
		group.m_DeletedDateTime = TimeUtil::GetInstance().GetISO8601String(p_BusinessGroup.GetDeletedDateTime().get());
	group.m_PreferredDataLocation = p_BusinessGroup.GetPreferredDataLocation();
	group.m_ResourceProvisioningOptions = p_BusinessGroup.GetResourceProvisioningOptions();

	/* Beta API */
	group.m_MembershipRuleProcessingState = p_BusinessGroup.GetMembershipRuleProcessingState();
	group.m_MembershipRule = p_BusinessGroup.GetMembershipRule();
	group.m_Theme = p_BusinessGroup.GetTheme();
	group.m_PreferredLanguage = p_BusinessGroup.GetPreferredLanguage();

    return group;
}

bool BusinessGroup::operator<(const BusinessGroup& p_Other) const
{
	return GetID() < p_Other.GetID();
}

void BusinessGroup::SetLCPId(const PooledString& p_LCPId)
{
	m_LCPId = p_LCPId;
}

const boost::YOpt<SapioError>& BusinessGroup::GetDriveError() const
{
	return m_DriveError;
}

void BusinessGroup::SetDriveError(const boost::YOpt<SapioError>& p_Val)
{
	m_DriveError = p_Val;
}

const boost::YOpt<bool>& BusinessGroup::GetIsYammer() const
{
	return m_IsYammer;
}

void BusinessGroup::SetIsYammer(const boost::YOpt<bool>& p_Val)
{
	m_IsYammer = p_Val;
}

const boost::YOpt<SapioError>& BusinessGroup::GetIsYammerError() const
{
	return m_IsYammerError;
}

void BusinessGroup::SetIsYammerError(const boost::YOpt<SapioError>& p_Val)
{
	m_IsYammerError = p_Val;
}

const boost::YOpt<bool>& BusinessGroup::GetSettingAllowToAddGuests() const
{
    return m_SettingAllowToAddGuests;
}

void BusinessGroup::SetSettingAllowToAddGuests(const boost::YOpt<bool>& p_Val)
{
    m_SettingAllowToAddGuests = p_Val;
}

void BusinessGroup::SetSettingAllowToAddGuestsId(const boost::YOpt<PooledString>& p_Val)
{
    m_SettingAllowToAddGuestsId = p_Val;
}

const boost::YOpt<SapioError>& BusinessGroup::GetGroupSettingsError() const
{
	return m_GroupSettingsError;
}

void BusinessGroup::SetGroupSettingsError(const boost::YOpt<SapioError>& p_Val)
{
	m_GroupSettingsError = p_Val;
}

const boost::YOpt<BusinessDrive>& BusinessGroup::GetDrive() const
{
	return m_Drive;
}

void BusinessGroup::SetDrive(const boost::YOpt<BusinessDrive>& p_Val)
{
	m_Drive = p_Val;
}

const boost::YOpt<vector<PooledString>> BusinessGroup::GetResourceBehaviorOptions() const
{
	return m_ResourceBehaviorOptions;
}

const boost::YOpt<vector<PooledString>> BusinessGroup::GetResourceProvisioningOptions() const
{
	return m_ResourceProvisioningOptions;
}

void BusinessGroup::SetResourceBehaviorOptions(const vector<PooledString>& p_ResourceBehaviorOptions)
{
	m_ResourceBehaviorOptions = p_ResourceBehaviorOptions;
}

void BusinessGroup::SetResourceProvisioningOptions(const vector<PooledString>& p_ResourceProvisioningOptions)
{
	m_ResourceProvisioningOptions = p_ResourceProvisioningOptions;
}

const boost::YOpt<PooledString>& BusinessGroup::GetPreferredDataLocation() const
{
	return m_PreferredDataLocation;
}

void BusinessGroup::SetPreferredDataLocation(const boost::YOpt<PooledString>& p_PreferredDataLocation)
{
	m_PreferredDataLocation = p_PreferredDataLocation;
}

const boost::YOpt<uint32_t> BusinessGroup::GetMembersCount() const
{
	return m_MembersCount;
}

void BusinessGroup::SetMembersCount(const boost::YOpt<uint32_t>& p_MembersCount)
{
	m_MembersCount = p_MembersCount;
}

const boost::YOpt<bool>& BusinessGroup::GetHideFromOutlookClients() const
{
	return m_HideFromOutlookClients;
}

void BusinessGroup::SetHideFromOutlookClients(const boost::YOpt<bool>& p_Val)
{
	m_HideFromOutlookClients = p_Val;
}

const boost::YOpt<bool>& BusinessGroup::GetHideFromAddressLists() const
{
	return m_HideFromAddressLists;
}

void BusinessGroup::SetHideFromAddressLists(const boost::YOpt<bool>& p_Val)
{
	m_HideFromAddressLists = p_Val;
}

const boost::YOpt<LicenseProcessingState>& BusinessGroup::GetLicenseProcessingState() const
{
	return m_LicenseProcessingState;
}

void BusinessGroup::SetLicenseProcessingState(const boost::YOpt<LicenseProcessingState>& p_Val)
{
	m_LicenseProcessingState = p_Val;
}

const boost::YOpt<PooledString>& BusinessGroup::GetSecurityIdentifier() const
{
	return m_SecurityIdentifier;
}

void BusinessGroup::SetSecurityIdentifier(const boost::YOpt<PooledString>& p_Val)
{
	m_SecurityIdentifier = p_Val;
}

const boost::YOpt<vector<BusinessAssignedLicense>>& BusinessGroup::GetAssignedLicenses() const
{
	return m_AssignedLicenses;
}

boost::YOpt<vector<BusinessAssignedLicense>>& BusinessGroup::GetAssignedLicenses()
{
	return m_AssignedLicenses;
}

void BusinessGroup::SetAssignedLicenses(const vector<BusinessAssignedLicense>& val)
{
	m_AssignedLicenses = val;
}

void BusinessGroup::SetAssignedLicenses(const vector<AssignedLicense>& val)
{
	if (m_AssignedLicenses)
		m_AssignedLicenses->clear();
	else
		m_AssignedLicenses.emplace();
	m_AssignedLicenses->reserve(val.size());
	for (const auto& lic : val)
		m_AssignedLicenses->push_back(lic);
}

void BusinessGroup::CopyAssignedLicensesTo(vector<AssignedLicense>& val) const
{
	val.clear();
	if (m_AssignedLicenses)
	{
		for (auto& lic : *m_AssignedLicenses)
			val.emplace_back(lic.ToAssignedLicense());
	}
}

const boost::YOpt<PooledString>& BusinessGroup::GetMembershipRuleProcessingState() const
{
	return m_MembershipRuleProcessingState;
}

void BusinessGroup::SetMembershipRuleProcessingState(const boost::YOpt<PooledString>& p_MembershipRuleProcessingState)
{
	m_MembershipRuleProcessingState = p_MembershipRuleProcessingState;
}

const boost::YOpt<PooledString>& BusinessGroup::GetMembershipRuleProcessingStateForEdit() const
{
	if (m_MembershipRuleProcessingState && !m_MembershipRuleProcessingState->IsEmpty())
		return m_MembershipRuleProcessingState;

	static boost::YOpt<PooledString> m_NoState = GridBackendUtil::g_NoValueString;
	return m_NoState;
}

const boost::YOpt<PooledString>& BusinessGroup::GetMembershipRule() const
{
	return m_MembershipRule;
}

void BusinessGroup::SetMembershipRule(const boost::YOpt<PooledString>& p_MembershipRule)
{
	m_MembershipRule = p_MembershipRule;
}

const boost::YOpt<PooledString>& BusinessGroup::GetTheme() const
{
	return m_Theme;
}

void BusinessGroup::SetTheme(const boost::YOpt<PooledString>& p_Theme)
{
	m_Theme = p_Theme;
}

const boost::YOpt<PooledString>& BusinessGroup::GetThemeForEdit() const
{
	if (m_Theme && !m_Theme->IsEmpty())
		return m_Theme;

	static boost::YOpt<PooledString> m_NoTheme = GridBackendUtil::g_NoValueString;
	return m_NoTheme;
}

const boost::YOpt<PooledString>& BusinessGroup::GetPreferredLanguage() const
{
	return m_PreferredLanguage;
}

void BusinessGroup::SetPreferredLanguage(const boost::YOpt<PooledString>& p_PreferredLanguage)
{
	m_PreferredLanguage = p_PreferredLanguage;
}

const boost::YOpt<OnPremiseGroup>& BusinessGroup::GetOnPremiseGroup() const
{
	return m_OnPremGroup;
}

boost::YOpt<OnPremiseGroup>& BusinessGroup::GetOnPremiseGroup()
{
	return m_OnPremGroup;
}

void BusinessGroup::SetDeletedDateTime(const boost::YOpt<YTimeDate>& p_DelDate)
{
	m_DeletedDateTime = p_DelDate;
}

void BusinessGroup::SetDeletedDateTime(const boost::YOpt<PooledString>& p_DelDate)
{
	if (p_DelDate.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_DelDate.get(), td))
			SetDeletedDateTime(td);
	}
}

const boost::YOpt<YTimeDate>& BusinessGroup::GetDeletedDateTime() const
{
	return m_DeletedDateTime;
}

bool BusinessGroup::IsFromRecycleBin() const
{
	return m_DeletedDateTime.is_initialized();
}

void BusinessGroup::SetGroupUnifiedGuestSettingTemplateActivated(const boost::YOpt<bool>& p_Val)
{
    m_SettingAllowToAddGuestsIsActivated = p_Val;
}

bool BusinessGroup::GetIsTeam() const
{
	return m_ResourceProvisioningOptions && m_ResourceProvisioningOptions->end() != std::find(m_ResourceProvisioningOptions->begin(), m_ResourceProvisioningOptions->end(), g_TeamProvisionningOption);
}

const boost::YOpt<bool>& BusinessGroup::GetEditIsTeam() const
{
	if (!m_IsTeamForEdit.is_initialized() && GetIsTeam())
		m_IsTeamForEdit = true;
	return m_IsTeamForEdit;
}

void BusinessGroup::SetEditHasTeam(const boost::YOpt<bool>& p_HasTeam)
{
	ASSERT(p_HasTeam.is_initialized());
	m_IsTeamForEdit = p_HasTeam;
}

bool BusinessGroup::GetHasTeamInfo() const
{
	return m_HasTeamInfo;
}

void BusinessGroup::SetHasTeamInfo(bool p_HasTeamInfo)
{
    m_HasTeamInfo = p_HasTeamInfo;
}

const boost::YOpt<bool>& BusinessGroup::IsAllowExternalSenders() const
{
	return m_IsAllowExternalSenders;
}

const boost::YOpt<bool>& BusinessGroup::IsAutoSubscribeNewMembers() const
{
	return m_IsAutoSubscribeNewMembers;
}

const boost::YOpt<PooledString>& BusinessGroup::GetClassification() const
{
	return m_Classification;
}

const boost::YOpt<YTimeDate>& BusinessGroup::GetCreatedDateTime() const
{
	return m_CreatedDateTime;
}

const boost::YOpt<YTimeDate>& BusinessGroup::GetRenewedDateTime() const
{
	return m_RenewedDateTime;
}

const boost::YOpt<PooledString>& BusinessGroup::GetDescription() const
{
	return m_Description;
}

const boost::YOpt<vector<PooledString>>&	BusinessGroup::GetGroupTypes() const
{
	return m_GroupTypes;
}

const boost::YOpt<vector<PooledString>>& BusinessGroup::GetProxyAddresses() const
{
	return m_ProxyAddresses;
}

//const boost::YOpt<bool>& BusinessGroup::IsSubscribedByMail() const
//{
//	return m_IsSubscribedByMail;
//}

const boost::YOpt<PooledString>& BusinessGroup::GetMail() const
{
	return m_Mail;
}

const boost::YOpt<bool>& BusinessGroup::IsMailEnabled() const
{
	return m_IsMailEnabled;
}

const boost::YOpt<PooledString>& BusinessGroup::GetMailNickname() const
{
	return m_MailNickname;
}

const boost::YOpt<YTimeDate>& BusinessGroup::GetOnPremisesLastSyncDateTime() const
{
	return m_OnPremisesLastSyncDateTime;
}

const boost::YOpt<PooledString>& BusinessGroup::GetOnPremisesSecurityIdentifier() const
{
	return m_OnPremisesSecurityIdentifier;
}

const boost::YOpt<vector<OnPremisesProvisioningError>>& BusinessGroup::GetOnPremisesProvisioningErrors() const
{
	return m_OnPremisesProvisioningErrors;
}

const boost::YOpt<bool>& BusinessGroup::IsOnPremisesSyncEnabled() const
{
	return m_IsOnPremisesSyncEnabled;
}

const boost::YOpt<bool>& BusinessGroup::IsSecurityEnabled() const
{
	return m_IsSecurityEnabled;
}

const boost::YOpt<int32_t>& BusinessGroup::GetUnseenCount() const
{
	return m_UnseenCount;
}

const boost::YOpt<PooledString>& BusinessGroup::GetVisibility() const
{
	return m_Visibility;
}

const boost::YOpt<Team>& BusinessGroup::GetTeam() const
{
    return m_Team;
}

const boost::YOpt<SapioError>& BusinessGroup::GetTeamError() const
{
	return m_TeamError;
}

const boost::YOpt<PooledString>& BusinessGroup::GetCombinedGroupType() const
{
	m_CombinedGroupType = boost::none;

	if (m_GroupTypes && m_GroupTypes->end() != std::find(m_GroupTypes->begin(), m_GroupTypes->end(), _YTEXT("Unified")))
	{
		m_CombinedGroupType = g_Office365Group;
	}
	//else if (m_GroupTypes && m_GroupTypes.end() != std::find(m_GroupTypes.begin(), m_GroupTypes.end(), g_DynamicMembership))
	//{
	//	m_CombinedGroupType = g_DynamicGroup;
	//}
	else if (m_IsMailEnabled.is_initialized() && m_IsSecurityEnabled.is_initialized())
	{
		if (*m_IsMailEnabled && !*m_IsSecurityEnabled)
			m_CombinedGroupType = g_DistributionList;
		else if (!*m_IsMailEnabled && *m_IsSecurityEnabled)
			m_CombinedGroupType = g_SecurityGroup;
		else if (*m_IsMailEnabled && *m_IsSecurityEnabled)
			m_CombinedGroupType = g_MailEnabledSecurityGroup;
	}

	return m_CombinedGroupType;
}

const boost::YOpt<PooledString>& BusinessGroup::GetLCPStatus() const
{
	return m_LCPStatus;
}

const boost::YOpt<SapioError>& BusinessGroup::GetLCPStatusError() const
{
	return m_LCPStatusError;
}

const boost::YOpt<bool>& BusinessGroup::IsDynamicMembership() const
{
	if (!m_DynamicMembership)
		m_DynamicMembership = m_GroupTypes && m_GroupTypes->end() != std::find(m_GroupTypes->begin(), m_GroupTypes->end(), g_DynamicMembership);

	return m_DynamicMembership;
}

void BusinessGroup::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}

const boost::YOpt<PooledString>& BusinessGroup::GetSettingAllowToAddGuestsId() const
{
    return m_SettingAllowToAddGuestsId;
}

const boost::YOpt<bool>& BusinessGroup::GetGroupUnifiedGuestSettingTemplateActivated() const
{
    return m_SettingAllowToAddGuestsIsActivated;
}

void BusinessGroup::SetIsAllowExternalSenders(const boost::YOpt<bool>& p_IsAllowExternalSenders)
{
	m_IsAllowExternalSenders = p_IsAllowExternalSenders;
}

void BusinessGroup::SetIsAutoSubscribeNewMembers(const boost::YOpt<bool>&  p_IsAutoSubscribeNewMembers)
{
	m_IsAutoSubscribeNewMembers = p_IsAutoSubscribeNewMembers;
}

void BusinessGroup::SetClassification(const boost::YOpt<PooledString>& p_Classification)
{
	m_Classification = p_Classification;
}

void BusinessGroup::SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_CreatedDateTime)
{
	m_CreatedDateTime = p_CreatedDateTime;
}

void BusinessGroup::SetCreatedDateTime(const boost::YOpt<PooledString>& p_CreatedDateTime)
{
	if (p_CreatedDateTime.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_CreatedDateTime.get(), td))
			SetCreatedDateTime(td);
	}
}

void BusinessGroup::SetRenewedDateTime(const boost::YOpt<YTimeDate>& p_RenewedDateTime)
{
	m_RenewedDateTime = p_RenewedDateTime;
}

void BusinessGroup::SetRenewedDateTime(const boost::YOpt<PooledString>& p_RenewedDateTime)
{
	if (p_RenewedDateTime.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_RenewedDateTime.get(), td))
			SetRenewedDateTime(td);
	}
}

void BusinessGroup::SetDescription(const boost::YOpt<PooledString>& p_Description)
{
	m_Description = p_Description;
}

void BusinessGroup::SetGroupTypes(const boost::YOpt<vector<PooledString>>& p_GroupTypes)
{
	m_GroupTypes = p_GroupTypes;
	m_DynamicMembership = boost::none;
}

void BusinessGroup::SetProxyAddresses(const boost::YOpt<vector<PooledString>>& p_ProxyAddresses)
{
	m_ProxyAddresses = p_ProxyAddresses;
}

//void BusinessGroup::SetIsSubscribedByMail(const boost::YOpt<bool>&  p_IsSubscribedByMail)
//{
//	m_IsSubscribedByMail = p_IsSubscribedByMail;
//}

void BusinessGroup::SetMail(const boost::YOpt<PooledString>& p_Mail)
{
	m_Mail = p_Mail;
}

void BusinessGroup::SetIsMailEnabled(const boost::YOpt<bool>&  p_IsMailEnabled)
{
	m_IsMailEnabled = p_IsMailEnabled;
}

void BusinessGroup::SetMailNickname(const boost::YOpt<PooledString>& p_MailNickname)
{
	m_MailNickname = p_MailNickname;
}

void BusinessGroup::SetOnPremisesLastSyncDateTime(const boost::YOpt<YTimeDate>& p_OnPremisesLastSyncDateTime)
{
	m_OnPremisesLastSyncDateTime = p_OnPremisesLastSyncDateTime;
}

void BusinessGroup::SetOnPremisesLastSyncDateTime(const boost::YOpt<PooledString>& p_OnPremisesLastSyncDateTime)
{
	if (p_OnPremisesLastSyncDateTime.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_OnPremisesLastSyncDateTime.get(), td))
			SetCreatedDateTime(td);
	}
}

void BusinessGroup::SetOnPremisesSecurityIdentifier(const boost::YOpt<PooledString>& p_OnPremisesSecurityIdentifier)
{
	m_OnPremisesSecurityIdentifier = p_OnPremisesSecurityIdentifier;
}

void BusinessGroup::SetOnPremisesProvisioningErrors(const boost::YOpt<vector<OnPremisesProvisioningError>>& p_OnPremisesProvisioningErrors)
{
	m_OnPremisesProvisioningErrors = p_OnPremisesProvisioningErrors;
}

void BusinessGroup::SetOnPremisesSyncEnabled(const boost::YOpt<bool>&  p_IsOnPremisesSyncEnabled)
{
	m_IsOnPremisesSyncEnabled = p_IsOnPremisesSyncEnabled;
}

void BusinessGroup::SetIsSecurityEnabled(const boost::YOpt<bool>&  p_IsSecurityEnabled)
{
	m_IsSecurityEnabled = p_IsSecurityEnabled;
}

void BusinessGroup::SetUnseenCount(const boost::YOpt<int32_t> p_UnseenCount)
{
	m_UnseenCount = p_UnseenCount;
}

void BusinessGroup::SetVisibility(const boost::YOpt<PooledString>& p_Visibility)
{
	ASSERT(!p_Visibility
		|| MFCUtil::StringMatchW(*p_Visibility, _YTEXT("Public"))
		|| MFCUtil::StringMatchW(*p_Visibility, _YTEXT("Private"))
		|| MFCUtil::StringMatchW(*p_Visibility, _YTEXT("HiddenMembership")));
	m_Visibility = p_Visibility;
}

void BusinessGroup::SetTeam(const boost::YOpt<Team>& p_Team)
{
	ASSERT(!m_TeamError || !p_Team);
    m_Team = p_Team;
}

void BusinessGroup::SetTeamError(const boost::YOpt<SapioError>& p_Val)
{
	ASSERT(!m_Team);
	m_TeamError = p_Val;
}

const boost::YOpt<bool>& BusinessGroup::IsTeamArchived() const
{
	if (m_PendingArchivedTeam)
		return m_PendingArchivedTeam;
	if (m_Team)
		return m_Team->IsArchived;
	static const boost::YOpt<bool> noTeamValue;
	return noTeamValue;
}

const boost::YOpt<bool>& BusinessGroup::GetPendingTeamArchiving() const
{
	return m_PendingArchivedTeam;
}

void BusinessGroup::SetTeamArchived(const boost::YOpt<bool>& p_Archived)
{
	// Also used when saving changes, where we don;t necessarily have the team...
	//ASSERT(m_Team);
	if (!m_Team || p_Archived != m_Team->IsArchived)
		m_PendingArchivedTeam = p_Archived;
	else
		m_PendingArchivedTeam.reset();
}

const boost::YOpt<bool>& BusinessGroup::GetSpoSiteReadOnlyForMembers() const
{
	return m_PendingSpoSiteReadOnlyForMembers;
}

void BusinessGroup::SetSpoSiteReadOnlyForMembers(const boost::YOpt<bool>& p_SpoSiteReadOnlyForMembers)
{
	// Can't assert due to multi-edit 
	//ASSERT(m_PendingArchivedTeam && *m_PendingArchivedTeam);
	if (m_PendingArchivedTeam && *m_PendingArchivedTeam)
		m_PendingSpoSiteReadOnlyForMembers = p_SpoSiteReadOnlyForMembers;
}

void BusinessGroup::SetLCPStatus(const boost::YOpt<PooledString>& p_LCPStatus)
{
	ASSERT(!m_LCPStatusError || !p_LCPStatus);
	m_LCPStatus = p_LCPStatus;
}

void BusinessGroup::SetLCPStatusError(const boost::YOpt<SapioError>& p_LCPStatusError)
{
	ASSERT(!m_LCPStatus || !p_LCPStatusError);
	m_LCPStatusError = p_LCPStatusError;
}

void BusinessGroup::SetDynamicMembership(const boost::YOpt<bool>& p_DynamicMembership)
{
	ASSERT(p_DynamicMembership);
	if (p_DynamicMembership)
	{
		ASSERT(m_GroupTypes);
		if (!m_GroupTypes)
			m_GroupTypes.emplace();

		if (p_DynamicMembership.get())
		{
			if (m_GroupTypes->end() == std::find(m_GroupTypes->begin(), m_GroupTypes->end(), g_DynamicMembership))
				m_GroupTypes->push_back(g_DynamicMembership);

			m_DynamicMembership = true;
		}
		else
		{
			auto it = std::find(m_GroupTypes->begin(), m_GroupTypes->end(), g_DynamicMembership);
			if (m_GroupTypes->end() != it)
				m_GroupTypes->erase(it);

			m_DynamicMembership = false;
		}
	}
}

void BusinessGroup::SetCombinedGroupType(const boost::YOpt<PooledString>& p_CombinedGroupType)
{
	if (p_CombinedGroupType)
	{
		if (!m_GroupTypes)
			m_GroupTypes.emplace();

		if (*p_CombinedGroupType == g_Office365Group)
		{
			//m_GroupTypes.clear();
			m_GroupTypes->erase(std::remove_if(m_GroupTypes->begin(), m_GroupTypes->end(), [](const PooledString& str) {return str != g_DynamicMembership; }), m_GroupTypes->end());
			m_GroupTypes->push_back(_YTEXT("Unified"));
			m_IsMailEnabled = true; // On group creation, this value is mandatory but even if we set this to false, it's true at the end.
			m_IsSecurityEnabled = false; // On group creation, this value is mandatory but it makes no sense for an Office 365 group... Consider setting true if false causes problems...
		}
		//else if (*p_CombinedGroupType == g_DynamicGroup)
		//{
		//	ASSERT(false); // Not handled correctly yet! These groups seem to require some charged license we don't have for now.
		//	m_GroupTypes.clear();
		//	m_GroupTypes.push_back(g_DynamicMembership);
		//	// FIXME: Add Dynamic Membership rules.
		//}
		else if (*p_CombinedGroupType == g_DistributionList)
		{
			//m_GroupTypes.clear();
			m_GroupTypes->erase(std::remove_if(m_GroupTypes->begin(), m_GroupTypes->end(), [](const PooledString& str) {return str != g_DynamicMembership; }), m_GroupTypes->end());
			m_IsMailEnabled = true;
			m_IsSecurityEnabled = false;
		}
		else if (*p_CombinedGroupType == g_SecurityGroup)
		{
			//m_GroupTypes.clear();
			m_GroupTypes->erase(std::remove_if(m_GroupTypes->begin(), m_GroupTypes->end(), [](const PooledString& str) {return str != g_DynamicMembership; }), m_GroupTypes->end());
			m_IsMailEnabled = false;
			m_IsSecurityEnabled = true;
		}
		else if (*p_CombinedGroupType == g_MailEnabledSecurityGroup)
		{
			//m_GroupTypes.clear();
			m_GroupTypes->erase(std::remove_if(m_GroupTypes->begin(), m_GroupTypes->end(), [](const PooledString& str) {return str != g_DynamicMembership; }), m_GroupTypes->end());
			m_IsMailEnabled = true;
			m_IsSecurityEnabled = true;
		}

		ASSERT(p_CombinedGroupType == GetCombinedGroupType());
	}
	else
	{
		m_GroupTypes.reset();
		m_IsMailEnabled.reset();
		m_IsSecurityEnabled.reset();
	}
}

const boost::YOpt<PooledString>& BusinessGroup::GetMailWithoutDomain() const
{
	static boost::YOpt<PooledString> withoutDomain;
	if (m_Mail)
	{
		auto strings = Str::explodeIntoVector(*m_Mail, PooledString(_YTEXT("@")), true, false);
		withoutDomain = strings[0];
	}
	return withoutDomain;
}

void BusinessGroup::SetMailWithoutDomain(const boost::YOpt<PooledString>& val)
{
	if (m_Mail)
	{
		auto strings = Str::explodeIntoVector(*m_Mail, PooledString(_YTEXT("@")), true, false);
		m_Mail = *val + _YTEXT("@") + strings[1];
	}
	else
	{
		m_Mail = *val + _YTEXT("@");
	}

	m_MailNickname = *val;
}

const boost::YOpt<PooledString>& BusinessGroup::GetMailDomain() const
{
	static boost::YOpt<PooledString> domain;
	if (m_Mail)
	{
		auto strings = Str::explodeIntoVector(*m_Mail, PooledString(_YTEXT("@")), true, false);
		domain = strings[1];
	}
	return domain;
}

void BusinessGroup::SetMailDomain(const boost::YOpt<PooledString>& val)
{
	if (m_Mail)
	{
		auto strings = Str::explodeIntoVector(*m_Mail, PooledString(_YTEXT("@")), true, false);
		m_Mail = strings[0] + _YTEXT("@") + (wstring)*val;
	}
	else
	{
		m_Mail = _YTEXT("@") + (wstring)*val;
	}
}

const boost::YOpt<PooledString>& BusinessGroup::GetUserCreatedOnBehalfOf() const
{
    return m_UserCreatedOnBehalfOf;
}

void BusinessGroup::SetUserCreatedOnBehalfOf(const boost::YOpt<PooledString>& p_Val)
{
	ASSERT(!m_UserCreatedOnBehalfOfError || !p_Val);
    m_UserCreatedOnBehalfOf = p_Val;
}

const boost::YOpt<SapioError>& BusinessGroup::GetUserCreatedOnBehalfOfError() const
{
	return m_UserCreatedOnBehalfOfError;
}

void BusinessGroup::SetUserCreatedOnBehalfOfError(const boost::YOpt<SapioError>& p_UserCreatedOnBehalfOfError)
{
	ASSERT(!m_UserCreatedOnBehalfOf);
	m_UserCreatedOnBehalfOfError = p_UserCreatedOnBehalfOfError;
}

void BusinessGroup::AddParent(const BusinessGroup& p_Parent)
{
	// No assert or test, recursivity is possible...
	//ASSERT(p_Parent.GetID() != GetID());
	//if (p_Parent.GetID() != GetID())
		m_Parents.push_back(p_Parent.GetID());
}

/*
void BusinessGroup::RemoveParent(const BusinessGroup& p_Parent)
{
	m_Parents.erase(p_Parent);
}
*/

void BusinessGroup::AddChild(const BusinessUser& p_Child)
{
	ASSERT(p_Child.GetID() != GetID());
	if (p_Child.GetID() != GetID())
		m_ChildrenUsers.push_back(p_Child.GetID());
}

void BusinessGroup::AddChild(const BusinessGroup& p_Child)
{
	// No assert or test, recursivity is possible...
	//ASSERT(p_Child.GetID() != GetID());
	//if (p_Child.GetID() != GetID())
		m_ChildrenGroups.push_back(p_Child.GetID());
}

void BusinessGroup::AddChild(const BusinessOrgContact& p_Child)
{
	ASSERT(p_Child.GetID() != GetID());
	if (p_Child.GetID() != GetID())
		m_ChildrenOrgContacts.push_back(p_Child.GetID());
}

/*
void BusinessGroup::RemoveChild(const BusinessObject& p_Child)
{
	m_Children.erase(p_Child);
}

void BusinessGroup::SetChildren(const vector<BusinessObject>& p_Children)
{
	m_Children = p_Children;
}
*/

void BusinessGroup::SetParents(const vector<BusinessGroup>& p_Parents)
{
	m_Parents.clear();
	for (const auto& p : p_Parents)
		AddParent(p);
}

const vector<PooledString>& BusinessGroup::GetChildrenUsers() const
{
	return m_ChildrenUsers;
}

const vector<PooledString>& BusinessGroup::GetChildrenGroups() const
{
	return m_ChildrenGroups;
}

const vector<PooledString>& BusinessGroup::GetChildrenOrgContacts() const
{
	return m_ChildrenOrgContacts;
}

void BusinessGroup::SetChildrenUsers(const vector<PooledString>& p_IDs)
{
	ASSERT(std::none_of(p_IDs.begin(), p_IDs.end(), [this](const auto& p_Id) {return GetID() == p_Id; }));
	m_ChildrenUsers = p_IDs;
}

void BusinessGroup::SetChildrenGroups(const vector<PooledString>& p_IDs)
{
	// No assert or test, recursivity is possible...
	//ASSERT(std::none_of(p_IDs.begin(), p_IDs.end(), [this](const auto& p_Id) {return GetID() == p_Id; }));
	m_ChildrenGroups = p_IDs;
}

void BusinessGroup::SetChildrenOrgContacts(const vector<PooledString>& p_IDs)
{
	ASSERT(std::none_of(p_IDs.begin(), p_IDs.end(), [this](const auto& p_Id) {return GetID() == p_Id; }));
	m_ChildrenOrgContacts = p_IDs;
}

const boost::YOpt<SapioError>& BusinessGroup::GetChildrenError() const
{
	return m_ChildrenError;
}

void BusinessGroup::SetChildrenError(const boost::YOpt<SapioError>& p_Val)
{
	m_ChildrenError = p_Val;
}

const vector<PooledString>& BusinessGroup::GetParents() const
{
	return m_Parents;
}

void BusinessGroup::AddOwner(const BusinessUser& p_Owner)
{
	ASSERT(!m_OwnersError);
	m_OwnersUsers.push_back(p_Owner.GetID());
}

//void BusinessGroup::AddOwner(const BusinessGroup& p_Owner)
//{
//	ASSERT(!m_OwnersError);
//	m_OwnersGroups.push_back(p_Owner.GetID());
//}

const vector<PooledString>& BusinessGroup::GetOwnerUsers() const
{
	return m_OwnersUsers;
}

//const vector<PooledString>& BusinessGroup::GetOwnerGroups() const
//{
//	return m_OwnersGroups;
//}

void BusinessGroup::ClearOwners()
{
	m_OwnersUsers.clear();
	//m_OwnersGroups.clear();
}

void BusinessGroup::SetOwnersError(const boost::YOpt<SapioError>& p_Val)
{
	ASSERT(m_OwnersUsers.empty()/* && m_OwnersGroups.empty()*/);
	m_OwnersError = p_Val;
}

const boost::YOpt<SapioError>& BusinessGroup::GetOwnersError() const
{
	return m_OwnersError;
}

void BusinessGroup::AddAuthorAccepted(const BusinessUser& p_Author)
{
	m_AuthorAcceptedUsers.push_back(p_Author.GetID());
}

void BusinessGroup::AddAuthorAccepted(const BusinessGroup& p_Author)
{
	m_AuthorAcceptedGroups.push_back(p_Author.GetID());
}

void BusinessGroup::AddAuthorAccepted(const BusinessOrgContact& p_Author)
{
	m_AuthorAcceptedOrgContacts.push_back(p_Author.GetID());
}

void BusinessGroup::AddAuthorRejected(const BusinessUser& p_Author)
{
	m_AuthorRejectedUsers.push_back(p_Author.GetID());
}

void BusinessGroup::AddAuthorRejected(const BusinessGroup& p_Author)
{
	m_AuthorRejectedGroups.push_back(p_Author.GetID());
}

void BusinessGroup::AddAuthorRejected(const BusinessOrgContact& p_Author)
{
	m_AuthorRejectedOrgContacts.push_back(p_Author.GetID());
}

const vector<PooledString>& BusinessGroup::GetAuthorAcceptedUsers() const
{
	return m_AuthorAcceptedUsers;
}

const vector<PooledString>& BusinessGroup::GetAuthorAcceptedGroups() const
{
	return m_AuthorAcceptedGroups;
}

const vector<PooledString>& BusinessGroup::GetAuthorAcceptedOrgContacts() const
{
	return m_AuthorAcceptedOrgContacts;
}

const vector<PooledString>&	BusinessGroup::GetAuthorRejectedUsers() const
{
	return m_AuthorRejectedUsers;
}

const vector<PooledString>& BusinessGroup::GetAuthorRejectedGroups() const
{
	return m_AuthorRejectedGroups;
}

const vector<PooledString>& BusinessGroup::GetAuthorRejectedOrgContacts() const
{
	return m_AuthorRejectedOrgContacts;
}

const boost::YOpt<SapioError>& BusinessGroup::GetAuthorsAcceptedError() const
{
	return m_AuthorsAcceptedError;
}

void BusinessGroup::SetAuthorsAcceptedError(const boost::YOpt<SapioError>& p_Val)
{
	m_AuthorsAcceptedError = p_Val;
}

const boost::YOpt<SapioError>& BusinessGroup::GetAuthorsRejectedError() const
{
	return m_AuthorsRejectedError;
}

void BusinessGroup::SetAuthorsRejectedError(const boost::YOpt<SapioError>& p_Val)
{
	m_AuthorsRejectedError = p_Val;
}

pplx::task<vector<HttpResultWithError>> BusinessGroup::SendEditRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	vector<pplx::task<HttpResultWithError>> tasks;

	{
		Group group;
		auto updatableProperties = GetUpdatableProperties<Group, BusinessGroup>(group, { { { MetadataKeys::ReadOnly, false }, { MetadataKeys::BetaAPI, false } } }, { OnList, 0 });
		if (!updatableProperties.empty())
		{
			auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
			auto task = safeTaskCall(group.Update(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, updatableProperties).Then([](const RestResultInfo& p_Result) {
				return HttpResultWithError(p_Result, boost::none);
			}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
			tasks.push_back(task);
		}
	}

	// Do separate request for load more properties
	{
		Group group;
		auto updatableProperties = GetUpdatableProperties<Group, BusinessGroup>(group, { { { MetadataKeys::ReadOnly, false }, { MetadataKeys::BetaAPI, false } } }, { OnSync, OnList });
		if (!updatableProperties.empty())
		{
			auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
			auto task = safeTaskCall(group.Update(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, updatableProperties).Then([](const RestResultInfo& p_Result) {
				return HttpResultWithError(p_Result, boost::none);
			}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
			tasks.push_back(task);
		}
	}

	// Do separate request for beta properties
	{
		Group group;
		auto updatableProperties = GetUpdatableProperties<Group, BusinessGroup>(group, { { { MetadataKeys::ReadOnly, false }, { MetadataKeys::BetaAPI, true } } }, { OnSync, OnList });
		if (!updatableProperties.empty())
		{
			auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
			auto task = safeTaskCall(group.Update(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, updatableProperties).Then([](const RestResultInfo& p_Result) {
				return HttpResultWithError(p_Result, boost::none);
			}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
			tasks.push_back(task);
		}
	}

	if (GetTeam())
	{
		const rttr::type jsonType = rttr::type::get<Team>();
		auto props = jsonType.get_properties();
		vector<rttr::property> properties(props.cbegin(), props.cend());
		auto serializedTeam = GetTeam()->Serialize(properties);

		web::uri_builder uri;
		uri.append_path(Rest::GROUPS_PATH);
		uri.append_path(GetID());
		uri.append_path(_YTEXT("team"));

		// FIXME: We have to use beta for now to handle "allowCreatePrivateChannels" in MemberSettings.
		const wstring overrideBaseUri = _YTEXT("beta");
		if (GetEditIsTeam()) // Create team
		{
			auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
			auto task = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->Put(uri.to_uri(), httpLogger, p_TaskData, *serializedTeam, overrideBaseUri).Then([](const RestResultInfo& p_Result) {
				return HttpResultWithError(p_Result, boost::none);
				}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
			tasks.push_back(task);
		}
		else // Update team
		{
			auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
			auto task = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->Patch(uri.to_uri(), httpLogger, p_TaskData, *serializedTeam, overrideBaseUri).Then([](const RestResultInfo& p_Result) {
				return HttpResultWithError(p_Result, boost::none);
				}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
			tasks.push_back(task);
		}
	}

	if (GetPendingTeamArchiving())
	{
		// Archive Team
		web::uri_builder uri;
		uri.append_path(Rest::TEAMS_PATH/*GROUPS_PATH*/);
		uri.append_path(GetID());
		//uri.append_path(_YTEXT("team"));

		auto body = web::json::value::object();
		if (*GetPendingTeamArchiving())
		{
			uri.append_path(_YTEXT("archive"));

			if (GetSpoSiteReadOnlyForMembers())
				body[_YTEXT("shouldSetSpoSiteReadOnlyForMembers")] = web::json::value::boolean(*GetSpoSiteReadOnlyForMembers());
		}
		else
		{
			uri.append_path(_YTEXT("unarchive"));
		}

		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
		auto task = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_uri(), httpLogger, p_TaskData, WebPayloadJSON(body)).Then([](const RestResultInfo& p_Result) {
			return HttpResultWithError(p_Result, boost::none);
			}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
		tasks.push_back(task);
	}

    if (GetGroupUnifiedGuestSettingTemplateActivated().is_initialized() || GetSettingAllowToAddGuests().is_initialized())
    {
        // Query settings for the group
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("group settings"));
        auto requester = std::make_shared<GroupSettingsRequester>(GetID(), logger);

        auto groupSettingsTask = safeTaskCall(requester->Send(p_Sapio365Session, p_TaskData).Then([this, requester, p_TaskData]()
        {
            return requester->GetData();
        }), p_Sapio365Session->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ListErrorHandler<BusinessGroupSetting>, p_TaskData);

        std::vector<BusinessGroupSetting> groupSettings = groupSettingsTask.get();
		if (groupSettings.size() == 1 && groupSettings[0].GetError().is_initialized())
		{
			tasks.push_back(pplx::task_from_result(HttpResultWithError(RestResultInfo(), groupSettings[0].GetError().get())));
		}
		else
		{
			bool groupUnifiedGuestWasActivated = false;
			bool allowToAddGuestsOldValue = false;
			PooledString groupSettingId;
			for (const auto& setting : groupSettings)
			{
				ASSERT(setting.GetTemplateId());
				if (setting.GetTemplateId() && *setting.GetTemplateId() == BusinessGroupSettingTemplate::g_GroupUnifiedGuestSettingTemplateId)
				{
					groupUnifiedGuestWasActivated = true;
					groupSettingId = setting.GetID();
					for (const auto& value : setting.GetValues())
					{
						if (value.Name && *value.Name == BusinessGroupSettingTemplate::g_AllowToAddGuestsName && value.Value)
							allowToAddGuestsOldValue = Str::toLower(*value.Value) == _YTEXT("true");
					}
				}
			}

			if (groupUnifiedGuestWasActivated &&
				GetGroupUnifiedGuestSettingTemplateActivated().is_initialized() &&
				!*GetGroupUnifiedGuestSettingTemplateActivated())
			{
				// Delete group setting
				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
				auto deleteTask = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->DeleteGroupSetting(groupSettingId, GetID(), httpLogger, p_TaskData).Then([](const RestResultInfo& p_Result) {
					return HttpResultWithError(p_Result, boost::none);
				}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
				tasks.push_back(deleteTask);
			}
			else if (!groupUnifiedGuestWasActivated &&
				GetGroupUnifiedGuestSettingTemplateActivated().is_initialized() &&
				*GetGroupUnifiedGuestSettingTemplateActivated())
			{
				// Create group setting
				BusinessGroupSetting setting;
				setting.SetTemplateId(PooledString(BusinessGroupSettingTemplate::g_GroupUnifiedGuestSettingTemplateId));
				SettingValue value;
				value.Name = PooledString(BusinessGroupSettingTemplate::g_AllowToAddGuestsName);

				if (GetSettingAllowToAddGuests().is_initialized()) // Value changed?
				{
					value.Value = *GetSettingAllowToAddGuests() ? _YTEXT("true") : _YTEXT("false");
				}
				else
				{
					value.Value = _YTEXT("false");
				}
				setting.GetValues().push_back(value);

				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
				auto createTask = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->CreateGroupSetting(GetID(), BusinessGroupSetting::ToGroupSetting(setting), httpLogger, p_TaskData).Then([](const RestResultInfo& p_Result) {
					return HttpResultWithError(p_Result, boost::none);
				}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);

				tasks.push_back(createTask);
			}
			else if (groupUnifiedGuestWasActivated && GetSettingAllowToAddGuests().is_initialized())
			{
				ASSERT(!groupSettingId.IsEmpty());
				if (!groupSettingId.IsEmpty())
				{
					BusinessGroupSetting setting;
					setting.SetTemplateId(PooledString(BusinessGroupSettingTemplate::g_GroupUnifiedGuestSettingTemplateId));
					setting.SetID(groupSettingId);

					SettingValue value;
					value.Name = BusinessGroupSettingTemplate::g_AllowToAddGuestsName;
					ASSERT(GetSettingAllowToAddGuests().is_initialized());
					if (GetSettingAllowToAddGuests().is_initialized())
					{
						if (*GetSettingAllowToAddGuests())
							value.Value = _YTEXT("true");
						else
							value.Value = _YTEXT("false");
					}
					setting.GetValues().push_back(value);

					auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
					auto updateTask = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->UpdateGroupSetting(GetID(), BusinessGroupSetting::ToGroupSetting(setting), httpLogger, p_TaskData).Then([](const RestResultInfo& p_Result) {
						return HttpResultWithError(p_Result, boost::none);
					}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);

					tasks.push_back(updateTask);
				}
			}
		}
    }

	return TaskWrapper<vector<HttpResultWithError>>(pplx::when_all(tasks.begin(), tasks.end()));
}

TaskWrapper<HttpResultWithError> BusinessGroup::SendCreateRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	Group group;
	const auto creatableProperties = GetUpdatableProperties<Group, BusinessGroup>(group, { { { MetadataKeys::ValidForCreation, true } } }, {});
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(group.Create(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, creatableProperties).Then([](const RestResultInfo& p_Result){
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessGroup::SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	auto group = BusinessGroup::ToGroup(*this);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(group.Delete(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData).Then([group, p_Sapio365Session](const RestResultInfo& p_Result) {
		if (204 == p_Result.Response.status_code())
			p_Sapio365Session->GetGraphCache().Remove(group);
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

vector<TaskWrapper<HttpResultWithError>> BusinessGroup::SendAddMembersRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	// WARNING: Don't change order (Users, Groups, OrgContacts), you'd break ModuleGroup::updateGroupItems

    vector<TaskWrapper<HttpResultWithError>> tasks;

	Group group;
	group.m_Id = GetID();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	for (const auto& userID : GetChildrenUsers())
	{
		tasks.push_back(safeTaskCall(group.AddMember(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, userID, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& groupID : GetChildrenGroups())
	{
		tasks.push_back(safeTaskCall(group.AddMember(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, groupID, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& orgContactID : GetChildrenOrgContacts())
	{
		tasks.push_back(safeTaskCall(group.AddMember(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, orgContactID, p_TaskData, true).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

    return tasks;
}

vector<TaskWrapper<HttpResultWithError>> BusinessGroup::SendDeleteMembersRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	// WARNING: Don't change order (Users, Groups, OrgContacts), you'd break ModuleGroup::updateGroupItems

    vector<TaskWrapper<HttpResultWithError>> tasks;

	Group group;
	group.m_Id = GetID();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	for (const auto& userID : GetChildrenUsers())
	{
		tasks.push_back(safeTaskCall(group.DeleteMember(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, userID, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& groupID : GetChildrenGroups())
	{
		tasks.push_back(safeTaskCall(group.DeleteMember(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, groupID, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& orgContactID : GetChildrenOrgContacts())
	{
		tasks.push_back(safeTaskCall(group.DeleteMember(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, orgContactID, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

    return tasks;
}

vector<TaskWrapper<HttpResultWithError>> BusinessGroup::SendAddOwnersRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
    vector<TaskWrapper<HttpResultWithError>> tasks;

	Group group;
	group.m_Id = GetID();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	for (const auto& userID : GetOwnerUsers())
	{
		tasks.push_back(safeTaskCall(group.AddOwner(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, userID, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}
    
    return tasks;
}

vector<TaskWrapper<HttpResultWithError>> BusinessGroup::SendDeleteOwnersRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
    vector<TaskWrapper<HttpResultWithError>> tasks;

	Group group;
	group.m_Id = GetID();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	for (const auto& userID : GetOwnerUsers())
	{
		tasks.push_back(safeTaskCall(group.DeleteOwners(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, userID, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

    return tasks;
}

vector<TaskWrapper<HttpResultWithError>> BusinessGroup::SendAddAcceptedRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	// WARNING: Don't change order (Users, Groups, OrgContacts), you'd break ModuleGroup::updateGroupItems

    vector<TaskWrapper<HttpResultWithError>> tasks;

	Group group;
	group.m_Id = GetID();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	for (const auto& userID : GetAuthorAcceptedUsers())
	{
		tasks.push_back(safeTaskCall(group.AddAccepted(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, userID, Rest::USERS_PATH, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& groupID : GetAuthorAcceptedGroups())
	{
		tasks.push_back(safeTaskCall(group.AddAccepted(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, groupID, Rest::GROUPS_PATH, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& orgContactID : GetAuthorAcceptedOrgContacts())
	{
		tasks.push_back(safeTaskCall(group.AddAccepted(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, orgContactID, Rest::CONTACTS_PATH, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

    return tasks;
}

vector<TaskWrapper<HttpResultWithError>> BusinessGroup::SendAddRejectedRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	// WARNING: Don't change order (Users, Groups, OrgContacts), you'd break ModuleGroup::updateGroupItems

    vector<TaskWrapper<HttpResultWithError>> tasks;

	Group group;
	group.m_Id = GetID();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	for (const auto& userID : GetAuthorRejectedUsers())
	{
		tasks.push_back(safeTaskCall(group.AddRejected(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, userID, Rest::USERS_PATH, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& groupID : GetAuthorRejectedGroups())
	{
		tasks.push_back(safeTaskCall(group.AddRejected(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, groupID, Rest::GROUPS_PATH, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& orgContactID : GetAuthorRejectedOrgContacts())
	{
		tasks.push_back(safeTaskCall(group.AddRejected(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, orgContactID, Rest::CONTACTS_PATH, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

    return tasks;
}

vector<TaskWrapper<HttpResultWithError>> BusinessGroup::SendDeleteAuthorizationRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	// WARNING: Don't change order (Users, Groups, OrgContacts), you'd break ModuleGroup::updateGroupItems

    vector<TaskWrapper<HttpResultWithError>> tasks;

	Group group;
	group.m_Id = GetID();

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	for (const auto& userID : GetAuthorAcceptedUsers())
	{
		tasks.push_back(safeTaskCall(group.DeleteAuthorization(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, userID, Rest::USERS_PATH, true, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& userID : GetAuthorRejectedUsers())
	{
		tasks.push_back(safeTaskCall(group.DeleteAuthorization(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, userID, Rest::USERS_PATH, false, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& groupID : GetAuthorAcceptedGroups())
	{
		tasks.push_back(safeTaskCall(group.DeleteAuthorization(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, groupID, Rest::GROUPS_PATH, true, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
			{
				return HttpResultWithError(p_Result, boost::none);
			}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& groupID : GetAuthorRejectedGroups())
	{
		tasks.push_back(safeTaskCall(group.DeleteAuthorization(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, groupID, Rest::GROUPS_PATH, false, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& orgContactID : GetAuthorAcceptedOrgContacts())
	{
		tasks.push_back(safeTaskCall(group.DeleteAuthorization(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, orgContactID, Rest::CONTACTS_PATH, true, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
			{
				return HttpResultWithError(p_Result, boost::none);
			}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

	for (const auto& orgContactID : GetAuthorRejectedOrgContacts())
	{
		tasks.push_back(safeTaskCall(group.DeleteAuthorization(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, orgContactID, Rest::CONTACTS_PATH, false, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
		{
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData));
	}

    return tasks;
}

vector<TaskWrapper<HttpResultWithError>> BusinessGroup::SendGroupSettingsModify(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
    vector<TaskWrapper<HttpResultWithError>> tasks;

    if (GetGroupUnifiedGuestSettingTemplateActivated().is_initialized() || GetSettingAllowToAddGuests().is_initialized())
    {
        // Query settings for the group
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("group settings"));
        auto requester = std::make_shared<GroupSettingsRequester>(GetID(), logger);
        auto groupSettingsTask = safeTaskCall(requester->Send(p_Sapio365Session, p_TaskData).Then([this, requester, p_TaskData]()
        {
            return requester->GetData();
        }), p_Sapio365Session->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ListErrorHandler<BusinessGroupSetting>, p_TaskData);

        std::vector<BusinessGroupSetting> groupSettings = groupSettingsTask.get();
        if (groupSettings.size() == 1 && groupSettings[0].GetError().is_initialized())
        {
            tasks.push_back(pplx::task_from_result(HttpResultWithError(RestResultInfo(), groupSettings[0].GetError().get())));
            return tasks;
        }

        bool groupUnifiedGuestWasActivated = false;
        bool allowToAddGuestsOldValue = false;
        PooledString groupSettingId;
        for (const auto& setting : groupSettings)
        {
            ASSERT(setting.GetTemplateId());
            if (setting.GetTemplateId() && *setting.GetTemplateId() == BusinessGroupSettingTemplate::g_GroupUnifiedGuestSettingTemplateId)
            {
                groupUnifiedGuestWasActivated = true;
                groupSettingId = setting.GetID();
                for (const auto& value : setting.GetValues())
                {
                    if (value.Name && *value.Name == BusinessGroupSettingTemplate::g_AllowToAddGuestsName && value.Value)
                        allowToAddGuestsOldValue = Str::toLower(*value.Value) == _YTEXT("true");
                }
            }
        }

        if (groupUnifiedGuestWasActivated &&
            GetGroupUnifiedGuestSettingTemplateActivated().is_initialized() &&
            !*GetGroupUnifiedGuestSettingTemplateActivated())
        {
            // Delete group setting
			auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
            auto deleteResult = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->DeleteGroupSetting(groupSettingId, GetID(), httpLogger, p_TaskData).Then([](const RestResultInfo& p_Result) {
                return HttpResultWithError(p_Result, boost::none);
            }), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData).get();
            tasks.push_back(pplx::task_from_result(deleteResult));
        }
        else if (!groupUnifiedGuestWasActivated &&
            GetGroupUnifiedGuestSettingTemplateActivated().is_initialized() &&
            *GetGroupUnifiedGuestSettingTemplateActivated())
        {
            // Create group setting
            BusinessGroupSetting setting;
            setting.SetTemplateId(PooledString(BusinessGroupSettingTemplate::g_GroupUnifiedGuestSettingTemplateId));
            SettingValue value;
            value.Name = PooledString(BusinessGroupSettingTemplate::g_AllowToAddGuestsName);

            if (GetSettingAllowToAddGuests().is_initialized()) // Value changed?
            {
                value.Value = *GetSettingAllowToAddGuests() ? _YTEXT("true") : _YTEXT("false");
            }
            else
            {
                value.Value = _YTEXT("false");
            }
            setting.GetValues().push_back(value);

			auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
            auto createResult = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->CreateGroupSetting(GetID(), BusinessGroupSetting::ToGroupSetting(setting), httpLogger, p_TaskData).Then([](const RestResultInfo& p_Result) {
                return HttpResultWithError(p_Result, boost::none);
            }), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData).get();

            tasks.push_back(pplx::task_from_result(createResult));
        }
        else if (groupUnifiedGuestWasActivated && GetSettingAllowToAddGuests().is_initialized())
        {
            ASSERT(!groupSettingId.IsEmpty());
            if (!groupSettingId.IsEmpty())
            {
                BusinessGroupSetting setting;
                setting.SetTemplateId(PooledString(BusinessGroupSettingTemplate::g_GroupUnifiedGuestSettingTemplateId));
                setting.SetID(groupSettingId);

                SettingValue value;
                value.Name = BusinessGroupSettingTemplate::g_AllowToAddGuestsName;
                ASSERT(GetSettingAllowToAddGuests().is_initialized());
                if (GetSettingAllowToAddGuests().is_initialized())
                {
                    if (*GetSettingAllowToAddGuests())
                        value.Value = _YTEXT("true");
                    else
                        value.Value = _YTEXT("false");
                }
                setting.GetValues().push_back(value);

				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
                auto updateResult = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->UpdateGroupSetting(GetID(), BusinessGroupSetting::ToGroupSetting(setting), httpLogger, p_TaskData).Then([](const RestResultInfo& p_Result) {
                    return HttpResultWithError(p_Result, boost::none);
                }), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData).get();

                tasks.push_back(pplx::task_from_result(updateResult));
            }
        }
    }

    return tasks;
}

TaskWrapper<HttpResultWithError> BusinessGroup::SendRestoreRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	Group group = BusinessGroup::ToGroup(*this);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(group.Restore(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessGroup::SendHardDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	Group group = BusinessGroup::ToGroup(*this);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(group.HardDelete(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessGroup::SendLCPRenewRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	Group group = BusinessGroup::ToGroup(*this);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(group.GetRenew(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessGroup::SendLCPAddGroupRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	ASSERT(m_LCPId);
	Group group = BusinessGroup::ToGroup(*this);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(group.GetAddGroup(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, *m_LCPId).Then([p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessGroup::SendLCPRemoveGroupRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	ASSERT(m_LCPId);
	Group group = BusinessGroup::ToGroup(*this);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(group.GetRemoveGroup(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData, *m_LCPId).Then([p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

wstring BusinessGroup::GetValue(const wstring& p_PropName) const
{
	if (p_PropName == _YUID(O365_GROUP_ISTEAM))
		return (GetIsTeam() || GetEditIsTeam() && *GetEditIsTeam()) ? g_Team : wstring();
	else if (p_PropName == _YUID(O365_GROUP_GROUPTYPE))
	{ 
		const auto& gt = GetCombinedGroupType();
		return gt ? gt->c_str() : wstring();
	}
	else if (p_PropName == _YUID(O365_GROUP_DYNAMICMEMBERSHIP))
	{
		ASSERT(IsDynamicMembership());
		return *IsDynamicMembership() ? RoleDelegationUtil::g_BoolValueTrue : RoleDelegationUtil::g_BoolValueFalse;
	}

	return BusinessObject::GetValue(p_PropName);
}

vector<PooledString> BusinessGroup::GetProperties(const wstring& p_PropName) const
{
	if (p_PropName == _YUID(O365_GROUP_ISTEAM))
		return { _YTEXT(O365_GROUP_RESOURCEPROVISIONINGOPTIONS) };

	if (p_PropName == _YUID(O365_GROUP_GROUPTYPE))
		return	{ _YTEXT(O365_GROUP_GROUPTYPES)
				, _YTEXT(O365_GROUP_SECURITYENABLED)
				, _YTEXT(O365_GROUP_MAILENABLED) };

	if (p_PropName == _YUID(O365_GROUP_DYNAMICMEMBERSHIP))
		return { _YTEXT(O365_GROUP_GROUPTYPES) };

	ASSERT(p_PropName == MFCUtil::convertASCII_to_UNICODE(rttr::type::get<BusinessGroup>().get_property(Str::convertToASCII(p_PropName.c_str())).get_name().to_string()));
	return{ p_PropName };
}
