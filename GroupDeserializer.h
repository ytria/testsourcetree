#pragma once

#include "BusinessGroup.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "RoleDelegationTagger.h"

class GroupDeserializer	: public JsonObjectDeserializer
						, public Encapsulate<BusinessGroup>
						, public RoleDelegationTagger
{
public:
	GroupDeserializer(std::shared_ptr<const Sapio365Session> p_Session);

#ifdef _DEBUG
	// ctor only used by DirectoryObjectVariantDeserializer and WholeSiteDeserializer
	// to avoid assert when some properties are missing.
	GroupDeserializer(std::shared_ptr<const Sapio365Session> p_Session, bool p_NoSelectUsed);
#endif

    virtual void DeserializeObject(const web::json::object& p_Object) override;

private:
#ifdef _DEBUG
	bool m_NoSelectUsed = false;
#endif

	friend class GroupDeserializerFactory;
	GroupDeserializer();
};

