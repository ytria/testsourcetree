#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "GroupEndpoint.h"

class GroupEndpointDeserializer : public JsonObjectDeserializer, public Encapsulate<GroupEndpoint>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};

