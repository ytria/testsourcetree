#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessMessageRule.h"

class MessageRuleDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessMessageRule>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

class MessageRuleActionsDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessMessageRuleActions>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};

class MessageRulePredicatesDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessMessageRulePredicates>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};

class SizeRangeDeserializer : public JsonObjectDeserializer, public Encapsulate<SizeRange>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};

