#include "CosmosPaginator.h"

Cosmos::Paginator::Paginator(const HeadersType& p_Headers, FunctionType p_RequestFunction)
	: m_AdditionalHeaders(p_Headers),
	m_RequestFunction(p_RequestFunction)
{
}

pplx::task<void> Cosmos::Paginator::Paginate()
{
	return YSafeCreateTaskMutable([requestFct = m_RequestFunction, additionalHeaders = m_AdditionalHeaders] () mutable {
		static const wstring g_HeaderContinuation = _YTEXT("x-ms-continuation");
		auto result = requestFct(additionalHeaders);

		auto headers = result->GetResult().Response.headers();
		auto ittContinuation = headers.find(g_HeaderContinuation);
		bool addedHeaders = false;
		while (ittContinuation != headers.end())
		{
			if (!addedHeaders)
			{
				// Add the header x-ms-continuation to the next request
				additionalHeaders.emplace_back(g_HeaderContinuation, ittContinuation->second);
				addedHeaders = true;
			}
			else
			{
				additionalHeaders.back() = { g_HeaderContinuation, ittContinuation->second };
			}
			result = requestFct(additionalHeaders);

			headers = result->GetResult().Response.headers();
			ittContinuation = headers.find(g_HeaderContinuation);
		}
	});
}
