#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "EducationStudent.h"

class EducationStudentDeserializer : public JsonObjectDeserializer, public Encapsulate<EducationStudent>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

