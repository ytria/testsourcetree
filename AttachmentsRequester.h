#pragma once

#include "IRequester.h"
#include "BusinessAttachment.h"
#include "AttachmentDeserializer.h"
#include "IPageRequestLogger.h"
#include "ValueListDeserializer.h"

class AttachmentsRequester : public IRequester
{
public:
	enum class AttachmentsSource { UserEvents, GroupEvents, Messages };

	AttachmentsRequester(AttachmentsSource p_Source, PooledString p_UserOrGroupId, PooledString p_EventOrMsgId, const std::shared_ptr<IPageRequestLogger>& p_Logger);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const vector<BusinessAttachment>& GetData() const;

private:
	std::shared_ptr<ValueListDeserializer<BusinessAttachment, AttachmentDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;

	AttachmentsSource m_Source;
	PooledString m_UserOrGroupID;
	PooledString m_MsgOrEventId;
};

