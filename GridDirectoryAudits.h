#pragma once

#include "BaseO365Grid.h"
#include "DirectoryAudit.h"

class FrameDirectoryAudits;

class GridDirectoryAudits : public ModuleO365Grid<DirectoryAudit>
{
public:
	GridDirectoryAudits();
	virtual ~GridDirectoryAudits() override;
	void BuildView(const vector<DirectoryAudit>& p_DirAudits, bool p_FullPurge, bool p_NoPurge);

	wstring GetName(const GridBackendRow* p_Row) const override;
	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

protected:
	void customizeGrid() override;

	DirectoryAudit	getBusinessObject(GridBackendRow*) const override;
	void			UpdateBusinessObjects(const vector<DirectoryAudit>& p_DirAudits, bool p_SetModifiedStatus) override;
	void			RemoveBusinessObjects(const vector<DirectoryAudit>& p_DirAudits) override;

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

private:
	DECLARE_MESSAGE_MAP()

	afx_msg void OnChangeOptions();
	afx_msg void OnUpdateChangeOptions(CCmdUI* pCmdUI);

	void InitializeCommands() override;
	void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart) override;
	void FillDirAuditFields(GridBackendRow* p_Row, const DirectoryAudit& p_DirAudit);

	GridBackendColumn* m_ColActivityDateTime;//
	GridBackendColumn* m_ColLoggedByService;
	GridBackendColumn* m_ColOperationType;
	GridBackendColumn* m_ColCategory;//
	GridBackendColumn* m_ColCorrelationId;//
	GridBackendColumn* m_ColActivityDisplayName;//
	GridBackendColumn* m_ColResult;//
	GridBackendColumn* m_ColResultReason;//
	GridBackendColumn* m_ColAdditionalDetailsKey;//
	GridBackendColumn* m_ColAdditionalDetailsValue;//

	GridBackendColumn* m_ColTargetResourcesDisplayName;
	GridBackendColumn* m_ColTargetResourcesType;
	GridBackendColumn* m_ColTargetResourcesUserPrincipalName;
	GridBackendColumn* m_ColTargetResourcesGroupType;
	GridBackendColumn* m_ColTargetResourcesModifiedPropertiesDisplayName;
	GridBackendColumn* m_ColTargetResourcesModifiedPropertiesOldValue;
	GridBackendColumn* m_ColTargetResourcesModifiedPropertiesNewValue;
	GridBackendColumn* m_ColTargetResourcesId;

	GridBackendColumn* m_ColInitiatedBy;
	GridBackendColumn* m_ColInitiatedByAppAppId;//
	GridBackendColumn* m_ColInitiatedByAppDisplayName;//
	GridBackendColumn* m_ColInitiatedByAppServicePrincipalId;//
	GridBackendColumn* m_ColInitiatedByAppServicePrincipalName;//
	GridBackendColumn* m_ColInitiatedByUserId;//
	GridBackendColumn* m_ColInitiatedByUserDisplayName;//
	GridBackendColumn* m_ColInitiatedByUserUserPrincipalName;//
	GridBackendColumn* m_ColInitiatedByUserIpAddress;//

	FrameDirectoryAudits* m_Frame;
};

