#pragma once

#include "ExchangeObjectDeserializer.h"

#include "DelegatePermissions.h"

class DelegatePermissionsDeserializer : public Ex::ExchangeObjectDeserializer
{
public:
    void DeserializeObject(const XERCES_CPP_NAMESPACE::DOMElement& p_Element) override;
    const Ex::DelegatePermissions& GetObject() const;

private:
    Ex::DelegatePermissions m_DelegatePermissions;
};

