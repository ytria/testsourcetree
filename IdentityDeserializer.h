#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Identity.h"

class IdentityDeserializer : public JsonObjectDeserializer, public Encapsulate<Identity>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

