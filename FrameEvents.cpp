#include "FrameEvents.h"

#include "AutomationNames.h"
#include "Command.h"
#include "CommandDispatcher.h"
#include "ModuleBase.h"
#include "UpdatedObjectsGenerator.h"

IMPLEMENT_DYNAMIC(FrameEvents, GridFrameBase)

//BEGIN_MESSAGE_MAP(FrameEvents, GridFrameBase)
//END_MESSAGE_MAP()

FrameEvents::FrameEvents(Origin p_Origin, bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, p_Module, p_HistoryMode, p_PreviousFrame)
	, m_Origin(p_Origin)
	, m_EventsGrid(p_Origin, p_IsMyData)
{
    m_CreateAddRemoveToSelection = !p_IsMyData;
    m_CreateRefreshPartial = !p_IsMyData;
	m_CreateShowContext = !p_IsMyData;
	m_CreateApplyPartial = !p_IsMyData;
}

void FrameEvents::ShowEvents(const O365DataMap<BusinessUser, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_FullPurge, bool p_NoPurge)
{
	if (CompliesWithDataLoadPolicy())
		m_EventsGrid.BuildTreeView(p_Events, p_UpdateOperations, p_Refresh, p_FullPurge, p_NoPurge);
}

void FrameEvents::ShowEvents(const O365DataMap<BusinessGroup, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_FullPurge, bool p_NoPurge)
{
	if (CompliesWithDataLoadPolicy())
		m_EventsGrid.BuildTreeView(p_Events, p_UpdateOperations, p_Refresh, p_FullPurge, p_NoPurge);
}

void FrameEvents::ShowEventsWithAttachments(const O365DataMap<BusinessUser, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_UpdateOperations, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge)
{
	if (CompliesWithDataLoadPolicy())
		m_EventsGrid.BuildTreeViewWithAttachments(p_Events, p_UpdateOperations, p_BusinessAttachments, p_Refresh, p_FullPurge);
}

void FrameEvents::ShowEventsWithAttachments(const O365DataMap<BusinessGroup, vector<BusinessEvent>>& p_Events, const vector<O365UpdateOperation>& p_UpdateOperations, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge)
{
	if (CompliesWithDataLoadPolicy())
		m_EventsGrid.BuildTreeViewWithAttachments(p_Events, p_UpdateOperations, p_BusinessAttachments, p_Refresh, p_FullPurge);
}

void FrameEvents::createGrid()
{
	m_EventsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameEvents::GetRefreshPartialControlConfig()
{
	O365ControlConfig config;
	if (GetOrigin() == Origin::User || GetOrigin() == Origin::DeletedUser)
	{
		config = GetRefreshPartialControlConfigUsers();
	}
	else if (GetOrigin() == Origin::Group)
	{
		config = GetRefreshPartialControlConfigGroups();
	}
	else
		ASSERT(FALSE);

	return config;
}

// returns false if no frame specific tab is needed.
bool FrameEvents::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(tab);
	CreateApplyGroup(tab);
	CreateRevertGroup(tab);
	CXTPRibbonGroup* refreshGroup = CreateRefreshGroup(tab);
	ASSERT(nullptr != refreshGroup);
	if (nullptr != refreshGroup)
	{
		CXTPControl* ctrl = refreshGroup->Add(xtpControlButton, ID_EVENTSGRID_CHANGEOPTIONS);
		setImage({ ID_EVENTSGRID_CHANGEOPTIONS }, IDB_CHANGEMODULEOPTIONS, xtpImageNormal);
		setImage({ ID_EVENTSGRID_CHANGEOPTIONS }, IDB_CHANGEMODULEOPTIONS_16x16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	CXTPRibbonGroup* dataGroup = tab.AddGroup(YtriaTranslate::Do(FrameEvents_customizeActionsRibbonTab_2, _YLOC("Preview")).c_str());
	ASSERT(nullptr != dataGroup);
	if (nullptr != dataGroup)
	{
		CXTPControl* ctrl = dataGroup->Add(xtpControlButton, ID_EVENTSGRID_SHOWEVENTMESSAGEBODY);
		setImage({ ID_EVENTSGRID_SHOWEVENTMESSAGEBODY }, IDB_MESSAGE_CONTENT, xtpImageNormal);
		setImage({ ID_EVENTSGRID_SHOWEVENTMESSAGEBODY }, IDB_MESSAGE_CONTENT_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);		
	}

	CreateEditGroup(tab, false);

	auto group = tab.AddGroup(YtriaTranslate::Do(FrameEvents_customizeActionsRibbonTab_1, _YLOC("Attachments")).c_str());
    ASSERT(nullptr != group);
    if (nullptr != group)
    {
		setGroupImage(*group, 0);

		{
			auto ctrl = group->Add(xtpControlButton, ID_EVENTSGRID_SHOWATTACHMENTS);
			setImage({ ID_EVENTSGRID_SHOWATTACHMENTS }, IDB_EVENTS_SHOWATTACHMENTS, xtpImageNormal);
			setImage({ ID_EVENTSGRID_SHOWATTACHMENTS }, IDB_EVENTS_SHOWATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_EVENTSGRID_TOGGLEINLINEATTACHMENTS);
			GridFrameBase::setImage({ ID_EVENTSGRID_TOGGLEINLINEATTACHMENTS }, IDB_EVENTS_TOGGLEINLINEATTACHMENTS, xtpImageNormal);
			GridFrameBase::setImage({ ID_EVENTSGRID_TOGGLEINLINEATTACHMENTS }, IDB_EVENTS_TOGGLEINLINEATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_EVENTSGRID_DOWNLOADATTACHMENTS);
			setImage({ ID_EVENTSGRID_DOWNLOADATTACHMENTS }, IDB_EVENTS_DOWNLOADATTACHMENTS, xtpImageNormal);
			setImage({ ID_EVENTSGRID_DOWNLOADATTACHMENTS }, IDB_EVENTS_DOWNLOADATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_EVENTSGRID_VIEW_ITEM_ATTACHMENT);
			setImage({ ID_EVENTSGRID_VIEW_ITEM_ATTACHMENT }, IDB_EVENTS_VIEWITEMATTACHMENT, xtpImageNormal);
			setImage({ ID_EVENTSGRID_VIEW_ITEM_ATTACHMENT }, IDB_EVENTS_VIEWITEMATTACHMENT_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_EVENTSGRID_DELETEATTACHMENT);
			setImage({ ID_EVENTSGRID_DELETEATTACHMENT }, IDB_EVENTS_DELETEATTACHMENTS, xtpImageNormal);
			setImage({ ID_EVENTSGRID_DELETEATTACHMENT }, IDB_EVENTS_DELETEATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
    }

    CreateLinkGroups(tab, !m_EventsGrid.IsMyData() && (GetOrigin() == Origin::User || GetOrigin() == Origin::DeletedUser), GetOrigin() == Origin::Group);

	automationAddCommandIDToGreenLight(ID_EVENTSGRID_SHOWEVENTMESSAGEBODY);

    return true;
}

O365Grid& FrameEvents::GetGrid()
{
	return m_EventsGrid;
}

void FrameEvents::ApplySpecific(bool p_Selected)
{
	// Main items (event deletion)
	UpdatedObjectsGenerator<BusinessEvent> updatedEventsGenerator;
	updatedEventsGenerator.BuildUpdatedObjects(GetGrid(), p_Selected, UOGFlags::ACTION_DELETE);

	// Sub items (attachment deletion)
	const auto data = m_EventsGrid.GetDeleteAttachmentsData(p_Selected);

	CommandInfo info;
	info.Data() = std::make_pair(updatedEventsGenerator, data);
	info.SetOrigin(m_Origin);
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Event, Command::ModuleTask::UpdateModified, info, { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
}

void FrameEvents::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameEvents::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameEvents::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.SetOrigin(GetModuleCriteria().m_Origin);
	ASSERT(ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer);
	if (ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer)
		info.GetIds() = GetModuleCriteria().m_IDs;
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	if (GetOptions())
		info.Data2() = *GetOptions();

    try
    {
        auto& grid = dynamic_cast<GridEvents&>(GetGrid());
        grid.SetShowInlineAttachments(false);
    }
    catch (std::bad_cast)
    {
        ASSERT(false);
    }

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Event, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameEvents::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	if (GetOptions())
		info.Data2() = *GetOptions();
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Event, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameEvents::ShowItemAttachment(const BusinessItemAttachment& p_Attachment)
{
    m_EventsGrid.ShowItemAttachment(p_Attachment);
}

void FrameEvents::HiddenViaHistory()
{
	m_EventsGrid.HideMessageViewer();
}

bool FrameEvents::HasApplyButton()
{
    return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameEvents::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionSelectedPreviewBody(p_Action))
		p_CommandID = ID_EVENTSGRID_SHOWEVENTMESSAGEBODY;
	else if (IsActionSelectedAttachmentLoadInfo(p_Action))
	{
		p_CommandID = ID_EVENTSGRID_SHOWATTACHMENTS;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedAttachmentDownload(p_Action))
	{
		p_CommandID = ID_EVENTSGRID_DOWNLOADATTACHMENTS;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedPreviewItem(p_Action))
		p_CommandID = ID_EVENTSGRID_VIEW_ITEM_ATTACHMENT;
	else if (IsActionSelectedAttachmentDelete(p_Action))
		p_CommandID = ID_EVENTSGRID_DELETEATTACHMENT;

	return statusRV;
}

void FrameEvents::ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments)
{
    m_EventsGrid.ViewAttachments(p_BusinessAttachments, true, true, true);
}
