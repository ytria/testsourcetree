#include "BusinessUserConfiguration.h"

#include "MsGraphFieldNames.h"
#include "Str.h"

wstring BusinessUserConfiguration::g_AllowedStatus;
wstring BusinessUserConfiguration::g_BlockedStatus;
wstring BusinessUserConfiguration::g_Unlicensed;

BusinessUserConfiguration::BusinessUserConfiguration()
{
	m_YUIDandTitles[_YUID(O365_USER_DISPLAYNAME)] 																= YtriaTranslate::Do(GridUsers_customizeGrid_1, _YLOC("User Display Name"));
	m_YUIDandTitles[_YUID(O365_USER_USERPRINCIPALNAME)] 														= YtriaTranslate::Do(GridUsers_customizeGrid_2, _YLOC("Username"));
	m_YUIDandTitles[_YUID(O365_USER_ASSIGNEDLICENSES)] 															= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_1, _YLOC("Assigned Licenses Sku Part Number")).c_str();
	m_YUIDandTitles[_YUID("ASSIGNEDLICENSENAMES")] 																= _T("Assigned Licenses");
	m_YUIDandTitles[_YUID("assignedLicenses.skuId")] 															= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_2, _YLOC("Assigned Licenses Sku ID")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_USERTYPE)] 																	= YtriaTranslate::Do(GridUsers_customizeGrid_3, _YLOC("User Type"));
	m_YUIDandTitles[_YUID(O365_USER_ACCOUNTENABLED)] 															= YtriaTranslate::Do(GridUsers_customizeGrid_5, _YLOC("Sign-in status"));
	m_YUIDandTitles[_YUID(O365_USER_MAIL)] 																		= YtriaTranslate::Do(GridUsers_customizeGrid_7, _YLOC("Email"));
	m_YUIDandTitles[_YUID(O365_USER_MAILNICKNAME)]																= YtriaTranslate::Do(GridUsers_customizeGrid_8, _YLOC("Email nickname"));
	m_YUIDandTitles[_YUID(O365_USER_PROXYADDRESSES)] 															= YtriaTranslate::Do(GridUsers_customizeGrid_9, _YLOC("Aliases"));
	m_YUIDandTitles[_YUID(O365_USER_IMADDRESSES)] 																= YtriaTranslate::Do(GridUsers_customizeGrid_10, _YLOC("IM Addresses"));
	m_YUIDandTitles[_YUID(O365_USER_GIVENNAME)] 																= YtriaTranslate::Do(GridUsers_customizeGrid_11, _YLOC("First name"));
	m_YUIDandTitles[_YUID(O365_USER_SURNAME)] 																	= YtriaTranslate::Do(GridUsers_customizeGrid_12, _YLOC("Last name"));
	m_YUIDandTitles[_YUID(O365_USER_JOBTITLE)] 																	= YtriaTranslate::Do(GridUsers_customizeGrid_14, _YLOC("Job title"));
	m_YUIDandTitles[_YUID(O365_USER_COMPANYNAME)] 																= YtriaTranslate::Do(GridUsers_customizeGrid_15, _YLOC("Company Name"));
	m_YUIDandTitles[_YUID(O365_USER_DEPARTMENT)] 																= YtriaTranslate::Do(GridUsers_customizeGrid_16, _YLOC("Department"));
	m_YUIDandTitles[_YUID(O365_USER_BUSINESSPHONES)] 															= YtriaTranslate::Do(GridUsers_customizeGrid_17, _YLOC("Office Phone"));
	m_YUIDandTitles[_YUID(O365_USER_MOBILEPHONE)] 																= YtriaTranslate::Do(GridUsers_customizeGrid_18, _YLOC("Mobile phone"));
	m_YUIDandTitles[_YUID(O365_USER_OFFICELOCATION)] 															= YtriaTranslate::Do(GridUsers_customizeGrid_19, _YLOC("Office location"));
	m_YUIDandTitles[_YUID(O365_USER_STREETADDRESS)] 															= YtriaTranslate::Do(GridUsers_customizeGrid_20, _YLOC("Street Address"));
	m_YUIDandTitles[_YUID(O365_USER_CITY)] 																		= YtriaTranslate::Do(GridUsers_customizeGrid_21, _YLOC("City"));
	m_YUIDandTitles[_YUID(O365_USER_STATE)] 																	= YtriaTranslate::Do(GridUsers_customizeGrid_22, _YLOC("State"));
	m_YUIDandTitles[_YUID(O365_USER_POSTALCODE)] 																= YtriaTranslate::Do(GridUsers_customizeGrid_23, _YLOC("Postal Code"));
	m_YUIDandTitles[_YUID(O365_USER_COUNTRY)] 																	= YtriaTranslate::Do(GridUsers_customizeGrid_24, _YLOC("Country"));
	m_YUIDandTitles[_YUID(O365_USER_PREFERREDLANGUAGE)] 														= YtriaTranslate::Do(GridUsers_customizeGrid_25, _YLOC("Preferred language"));
	m_YUIDandTitles[_YUID(O365_USER_USAGELOCATION)] 															= YtriaTranslate::Do(GridUsers_customizeGrid_26, _YLOC("Location for License Usage"));
	m_YUIDandTitles[_YUID(O365_USER_PASSWORDPOLICIES)] 															= YtriaTranslate::Do(GridUsers_customizeGrid_28, _YLOC("Password policies"));
	m_YUIDandTitles[_YUID(O365_USER_PASSWORDPROFILE_PASSWORD)]													= YtriaTranslate::Do(GridUsers_customizeGrid_29, _YLOC("Password"));
	m_YUIDandTitles[_YUID(O365_USER_PASSWORDPROFILE_FORCECHANGE)] 												= YtriaTranslate::Do(GridUsers_customizeGrid_30, _YLOC("Force Change Password"));
	m_YUIDandTitles[_YUID(O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA)]										= _T("Force Change Password With MFA");
	m_YUIDandTitles[_YUID(O365_USER_MYSITE)] 																	= YtriaTranslate::Do(GridUsers_customizeGrid_32, _YLOC("Personal Site"));
	m_YUIDandTitles[_YUID(O365_USER_ABOUTME)] 																	= YtriaTranslate::Do(GridUsers_customizeGrid_33, _YLOC("About"));
	m_YUIDandTitles[_YUID(O365_USER_BIRTHDAY)] 																	= YtriaTranslate::Do(GridUsers_customizeGrid_34, _YLOC("Birthday"));
	m_YUIDandTitles[_YUID(O365_USER_PASTPROJECTS)]																= YtriaTranslate::Do(GridUsers_customizeGrid_35, _YLOC("Past projects"));
	m_YUIDandTitles[_YUID(O365_USER_SKILLS)]																	= YtriaTranslate::Do(GridUsers_customizeGrid_36, _YLOC("Skills"));
	m_YUIDandTitles[_YUID(O365_USER_RESPONSIBILITIES)] 															= YtriaTranslate::Do(GridUsers_customizeGrid_37, _YLOC("Expertise"));
	m_YUIDandTitles[_YUID(O365_USER_SCHOOLS)] 																	= YtriaTranslate::Do(GridUsers_customizeGrid_38, _YLOC("Schools"));
	m_YUIDandTitles[_YUID(O365_USER_INTERESTS)] 																= YtriaTranslate::Do(GridUsers_customizeGrid_39, _YLOC("Interests"));
	m_YUIDandTitles[_YUID(O365_USER_PREFERREDNAME)] 															= YtriaTranslate::Do(GridUsers_customizeGrid_40, _YLOC("Preferred name"));
	m_YUIDandTitles[_YUID(O365_USER_HIREDATE)]																	= YtriaTranslate::Do(GridUsers_customizeGrid_41, _YLOC("Hire date"));
	m_YUIDandTitles[_YUID(O365_USER_ONPREMISESSYNCENABLED)]														= YtriaTranslate::Do(GridUsers_customizeGrid_43, _YLOC("Sync Enabled - On-Premises"));
	m_YUIDandTitles[_YUID(O365_USER_ONPREMISESIMMUTABLEID)]														= YtriaTranslate::Do(GridUsers_customizeGrid_44, _YLOC("Immutable ID - On-Premises"));
	m_YUIDandTitles[_YUID(O365_USER_ONPREMISESLASTSYNCDATETIME)]												= YtriaTranslate::Do(GridUsers_customizeGrid_45, _YLOC("Last Sync - On-Premises "));
	m_YUIDandTitles[_YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER)]												= YtriaTranslate::Do(GridUsers_customizeGrid_46, _YLOC("Security ID - On-Premises"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_ARCHIVEFOLDERNAME)]											= YtriaTranslate::Do(GridUsers_customizeGrid_48, _YLOC("Archive Folder Display Name"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_ARCHIVEFOLDER)]												= YtriaTranslate::Do(GridUsers_customizeGrid_49, _YLOC("Archive Folder ID"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_TIMEZONE)]													= YtriaTranslate::Do(GridUsers_customizeGrid_50, _YLOC("Time Zone  - Mailbox Settings"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_LANGUAGE_DISPLAYNAME)]										= YtriaTranslate::Do(GridUsers_customizeGrid_51, _YLOC("Language Name - Mailbox Settings"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_LANGUAGE_LOCALE)]											= YtriaTranslate::Do(GridUsers_customizeGrid_52, _YLOC("Language Code - Mailbox Settings"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_STATUS)]								= YtriaTranslate::Do(GridUsers_customizeGrid_53, _YLOC("Status - Automatic Replies"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_EXTERNALAUDIENCE)]					= YtriaTranslate::Do(GridUsers_customizeGrid_54, _YLOC("External Audience - Automatic Replies"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_EXTERNALREPLYMESSAGE)]				= YtriaTranslate::Do(GridUsers_customizeGrid_55, _YLOC("External Message - Automatic Replies"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_INTERNALREPLYMESSAGE)]				= YtriaTranslate::Do(GridUsers_customizeGrid_56, _YLOC("Internal Message - Automatic Replies"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDSTARTDATETIME_DATETIME)]	= YtriaTranslate::Do(GridUsers_customizeGrid_57, _YLOC("Start On - Automatic Replies"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDSTARTDATETIME_TIMEZONE)]	= YtriaTranslate::Do(GridUsers_customizeGrid_58, _YLOC("Start Time Zone - Automatic Replies "));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDENDDATETIME_DATETIME)]		= YtriaTranslate::Do(GridUsers_customizeGrid_59, _YLOC("End On  - Automatic Replies"));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_AUTOMATICREPLYSETTINGS_SCHEDULEDENDDATETIME_TIMEZONE)]		= YtriaTranslate::Do(GridUsers_customizeGrid_60, _YLOC("End Time Zone - Automatic Replies "));
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_TIMEFORMAT)]												= _T("Time format of mailbox - Mailbox Settings");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_DATEFORMAT)]												= _T("Date format of mailbox - Mailbox Settings");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_DELEGATEMEETINGMESSAGEDELIVERYOPTIONS)]						= _T("Delegate meeting message delivery options - Mailbox Settings");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_DAYSOFWEEK)]									= _T("Work days - Mailbox Settings");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_STARTTIME)]									= _T("Start time of meeting hours - Mailbox Settings");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_ENDTIME)]										= _T("End time of meeting hours - Mailbox Settings");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOXSETTINGS_WORKINGHOURS_TIMEZONENAME)]									= _T("Time zone of meeting hours - Mailbox Settings");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOX_TYPE)]																= _T("Mailbox type");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOX_DELIVERTOMAILBOXANDFORWARD)]										= _T("Keep copy of forwarded message (SMTP) - Mailbox Settings");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOX_FORWARDINGSMTPADDRESS)]												= _T("SMTP forwarding address - Mailbox Settings");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOX_FORWARDINGADDRESS)]													= _T("Forwarding addresses - Mailbox Settings");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOX_CAN_SEND_ON_BEHALF)]												= _T("'Send on behalf of' delegates - Mailbox settings");
	m_YUIDandTitles[_YUID(O365_USER_MAILBOX_CAN_SEND_AS_USER)]													= _T("'Send as' delegates - Mailbox Settings");
	m_YUIDandTitles[_YUID(O365_USER_AGEGROUP)]																	= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_3, _YLOC("Age Group")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_DELETEDDATETIME)]															= YtriaTranslate::Do(GridUsersRecycleBin_customizeGrid_4, _YLOC("Deleted On"));
	m_YUIDandTitles[_YUID(O365_USER_CREATEDDATETIME)]															= YtriaTranslate::Do(GridContacts_customizeGrid_61, _YLOC("Created On")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME)]											= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_8, _YLOC("Sign-in Sessions valid from")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_CONSENTPROVIDEDFORMINOR)]													= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_9, _YLOC("Consent Provided for Minor")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_LEGALAGEGROUPCLASSIFICATION)]												= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_10, _YLOC("Age group Classification")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_ONPREMISESDOMAINNAME)]														= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_11, _YLOC("Domain Name on premises")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_ONPREMISESSAMACCOUNTNAME)]													= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_12, _YLOC("Sam Account Name on premises")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_ONPREMISESUSERPRINCIPALNAME)]												= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_13, _YLOC("User Principal Name on premises")).c_str();
	m_YUIDandTitles[_YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY)]											= YtriaTranslate::Do(GridGroups_customizeGrid_67, _YLOC("Category of Prov. Error")).c_str();
	m_YUIDandTitles[_YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME)]									= YtriaTranslate::Do(GridGroups_customizeGrid_68, _YLOC("Date of Prov. Error")).c_str();
	m_YUIDandTitles[_YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR)]								= YtriaTranslate::Do(GridGroups_customizeGrid_69, _YLOC("Property Causing Prov. Error")).c_str();
	m_YUIDandTitles[_YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE)]												= YtriaTranslate::Do(GridGroups_customizeGrid_70, _YLOC("Value of property causing Prov. Error")).c_str();

	m_YUIDandTitles[_YUID(O365_USER_OTHERMAILS)]																= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_14, _YLOC("Other Mails")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_PREFERREDDATALOCATION)]														= YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_25, _YLOC("Preferred Data Location")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_SHOWINADDRESSLIST)]															= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_16, _YLOC("Show in Address List")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_ONPREMISESDISTINGUISHEDNAME)]												= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_17, _YLOC("Distinguished Name on premises")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_ISRESOURCEACCOUNT)]															= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_18, _YLOC("Is Resource Account")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_EMPLOYEEID)]																= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_19, _YLOC("Employee ID")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_FAXNUMBER)]																	= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_20, _YLOC("Fax Number")).c_str();

	m_YUIDandTitles[_YUID(O365_USER_EXTERNALUSERSTATUS)]														= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_21, _YLOC("External User Status")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_EXTERNALUSERSTATUSCHANGEDON)]												= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_22, _YLOC("External User Status changed on")).c_str();

	m_YUIDandTitles[_YUID(O365_USER_CREATIONTYPE)]																= _T("Account type on creation");
	m_YUIDandTitles[_YUID("identities.signInType")]																= _T("Sign in type - Identities");
	m_YUIDandTitles[_YUID("identities.issuer")]																	= _T("Issuer - Identities");
	m_YUIDandTitles[_YUID("identities.issuerAssignedId")]														= _T("ID assigned by issuer - Identities");
	m_YUIDandTitles[_YUID(O365_USER_LASTPASSWORDCHANGEDATETIME)]												= _T("Password last changed on");
	m_YUIDandTitles[_YUID("licenseAssignmentStates.skuId")]														= _T("Sku ID of assigned licenses - Group licensing");
	m_YUIDandTitles[_YUID("ASSIGNEDSKUPARTNUMBER")]																= _T("Sku Part Number of assigned licenses - Group licensing");
	m_YUIDandTitles[_YUID("ASSIGNEDBYGROUPLICENSENAME")]														= _T("Licenses - Group licensing");
	m_YUIDandTitles[_YUID("licenseAssignmentStates.assignedByGroup")]											= _T("Group ID of assigning groups - Group licensing");
	m_YUIDandTitles[_YUID("GROUPLICENSINGMEMBER")]																= _T("Member - Group licensing");
	m_YUIDandTitles[_YUID("ASSIGNEDBYGROUPNAME")]																= _T("Assigning groups - Group licensing");
	m_YUIDandTitles[_YUID("ASSIGNEDBYGROUPEMAIL")]																= _T("Email of assigning groups - Group licensing");
	m_YUIDandTitles[_YUID("licenseAssignmentStates.state")]														= _T("State of assigned licenses - Group licensing");
	m_YUIDandTitles[_YUID("licenseAssignmentStates.error")]														= _T("License assignment error - Group licensing");
	m_YUIDandTitles[_YUID(O365_USER_REFRESTOKENVALIDFROMDATETIME)]												= _T("Refresh tokens, session cookies valid from");

	m_YUIDandTitles[_YUID("manager.displayName")]																= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_31, _YLOC("Manager")).c_str();
	m_YUIDandTitles[_YUID("manager.userPrincipalName")]															= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_32, _YLOC("Manager username")).c_str();
	m_YUIDandTitles[_YUID(O365_USER_MANAGER_ID)]																= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_33, _YLOC("Manager Graph ID")).c_str();

	m_YUIDandTitles[_YUID(O365_COLDRIVENAME)]																	= _T("OneDrive Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVEID)]																		= _T("OneDrive Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVEDESCRIPTION)]															= _T("OneDrive Description");
	m_YUIDandTitles[_YUID(O365_COLDRIVEQUOTASTATE)]																= _T("OneDrive - Storage State");
	m_YUIDandTitles[_YUID(O365_COLDRIVEQUOTAUSED)]																= _T("OneDrive - Storage Used");
	m_YUIDandTitles[_YUID(O365_COLDRIVEQUOTAREMAINING)]															= _T("OneDrive - Storage Remaining");
	m_YUIDandTitles[_YUID(O365_COLDRIVEQUOTADELETED)]															= _T("OneDrive - Storage Deleted");
	m_YUIDandTitles[_YUID(O365_COLDRIVEQUOTATOTAL)]																= _T("OneDrive - Storage Total");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATIONTIME)]															= _T("OneDrive - Creation Time");
	m_YUIDandTitles[_YUID(O365_COLDRIVELASTMODIFIEDDATETIME)]													= _T("OneDrive - Last Modified Date Time");
	m_YUIDandTitles[_YUID(O365_COLDRIVETYPE)]																	= _T("OneDrive Type");
	m_YUIDandTitles[_YUID(O365_COLDRIVEWEBURL)]																	= _T("OneDrive Web Url");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYUSEREMAIL)]														= _T("OneDrive - Created By User Email");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYUSERID)]														= _T("OneDrive - Created By User Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYAPPNAME)]														= _T("OneDrive - Created By App Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYAPPID)]															= _T("OneDrive - Created By App Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYDEVICENAME)]													= _T("OneDrive - Created By Device Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYDEVICEID)]														= _T("OneDrive - Created By Device Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERUSERNAME)]															= _T("OneDrive - Created Owner User Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERUSERID)]															= _T("OneDrive - Owner - User Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERUSEREMAIL)]															= _T("OneDrive - Owner - User Email");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERAPPNAME)]															= _T("OneDrive - Owner - App Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERAPPID)]																= _T("OneDrive - Owner - App Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERDEVICENAME)]														= _T("OneDrive - Owner - Device Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERDEVICEID)]															= _T("OneDrive - Owner - Device Id");

	// List properties (commented ones are of unhandled types)
	m_Properties4RoleDelegation =
	{
		_YUID(O365_USER_USERTYPE),
		_YUID(O365_USER_JOBTITLE),
		_YUID(O365_USER_DEPARTMENT),
		_YUID(O365_USER_COMPANYNAME),
		_YUID(O365_USER_OFFICELOCATION),
		_YUID(O365_USER_USAGELOCATION),
		_YUID(O365_USER_AGEGROUP),
		_YUID(O365_USER_STREETADDRESS),
		_YUID(O365_USER_CITY),
		_YUID(O365_USER_POSTALCODE),
		_YUID(O365_USER_STATE),
		_YUID(O365_USER_COUNTRY),
		_YUID(O365_USER_PREFERREDLANGUAGE),
		_YUID(O365_USER_ACCOUNTENABLED),
		_YUID(O365_USER_PASSWORDPOLICIES),
		_YUID(O365_USER_CONSENTPROVIDEDFORMINOR),
		_YUID(O365_USER_LEGALAGEGROUPCLASSIFICATION),
		_YUID(O365_USER_SURNAME),
		_YUID(O365_USER_DISPLAYNAME),
		_YUID(O365_USER_GIVENNAME),
		_YUID(O365_USER_MAIL),
		_YUID(O365_USER_MAILNICKNAME),
		_YUID(O365_USER_BIRTHDAY),
		_YUID(O365_USER_HIREDATE),
		_YUID(O365_USER_IMADDRESSES),
		_YUID(O365_USER_BUSINESSPHONES),
		_YUID(O365_USER_MOBILEPHONE),
		_YUID(O365_USER_PASSWORDPROFILE_FORCECHANGE),
		_YUID(O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA),
		_YUID(O365_USER_ASSIGNEDLICENSES),
		//_YUID(O365_USER_ASSIGNEDPLANS),
		//_YUID(O365_USER_PROVISIONEDPLANS),
		_YUID(O365_USER_PROXYADDRESSES),
		_YUID(O365_USER_CREATEDDATETIME),
		_YUID(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME),
		_YUID(O365_USER_ONPREMISESSYNCENABLED),
		_YUID(O365_USER_ONPREMISESDOMAINNAME),
		_YUID(O365_USER_ONPREMISESSAMACCOUNTNAME),
		_YUID(O365_USER_ONPREMISESUSERPRINCIPALNAME),
		_YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER),
		_YUID(O365_USER_ONPREMISESIMMUTABLEID),
		_YUID(O365_USER_SHOWINADDRESSLIST),
		_YUID(O365_USER_EXTERNALUSERSTATUS),
		_YUID(O365_USER_EXTERNALUSERSTATUSCHANGEDON),
		_YUID(O365_USER_ONPREMISESLASTSYNCDATETIME),
		_YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY),
		_YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME),
		_YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR),
		_YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE),
		_YUID(O365_USER_OTHERMAILS),
		//_YUID(O365_USER_PREFERREDDATALOCATION), // Load More
		_YUID(O365_USER_SHOWINADDRESSLIST),
		//_YUID(O365_USER_EXTERNALUSERSTATUS), // Load More
		//_YUID(O365_USER_EXTERNALUSERSTATUSCHANGEDON), // Load More
		_YUID(O365_USER_ONPREMISESDISTINGUISHEDNAME),
		_YUID(O365_USER_ISRESOURCEACCOUNT),
		_YUID(O365_USER_EMPLOYEEID),
		_YUID(O365_USER_FAXNUMBER),
		//_YUID("manager.displayName"), // Load More
		//_YUID("manager.userPrincipalName"), // Load More
		//_YUID(O365_USER_MANAGER_ID), // Load More
		//_YUID(O365_COLDRIVENAME), // Load More
		//_YUID(O365_COLDRIVEID), // Load More
		//_YUID(O365_COLDRIVEDESCRIPTION), // Load More
		//_YUID(O365_COLDRIVEQUOTASTATE), // Load Mores
		//_YUID(O365_COLDRIVEQUOTAUSED), // Load More
		//_YUID(O365_COLDRIVEQUOTAREMAINING), // Load More
		//_YUID(O365_COLDRIVEQUOTADELETED), // Load More
		//_YUID(O365_COLDRIVEQUOTATOTAL), // Load More
		//_YUID(O365_COLDRIVECREATIONTIME), // Load More
		//_YUID(O365_COLDRIVELASTMODIFIEDDATETIME), // Load More
		//_YUID(O365_COLDRIVETYPE), // Load More
		//_YUID(O365_COLDRIVEWEBURL), // Load More
		//_YUID(O365_COLDRIVECREATEDBYUSEREMAIL), // Load More
		//_YUID(O365_COLDRIVECREATEDBYUSERID), // Load More
		//_YUID(O365_COLDRIVECREATEDBYAPPNAME), // Load More
		//_YUID(O365_COLDRIVECREATEDBYAPPID), // Load More
		//_YUID(O365_COLDRIVECREATEDBYDEVICENAME), // Load More
		//_YUID(O365_COLDRIVECREATEDBYDEVICEID), // Load More
		//_YUID(O365_COLDRIVEOWNERUSERNAME), // Load More
		//_YUID(O365_COLDRIVEOWNERUSERID), // Load More
		//_YUID(O365_COLDRIVEOWNERUSEREMAIL), // Load More
		//_YUID(O365_COLDRIVEOWNERAPPNAME), // Load More
		//_YUID(O365_COLDRIVEOWNERAPPID), // Load More
		//_YUID(O365_COLDRIVEOWNERDEVICENAME), // Load More
		//_YUID(O365_COLDRIVEOWNERDEVICEID), // Load More
		_YUID(O365_USER_CREATIONTYPE),
		//_YUID("identities.signInType"),
		//_YUID("identities.issuer"),
		//_YUID("identities.issuerAssignedId"),
		_YUID(O365_USER_LASTPASSWORDCHANGEDATETIME),
		//_YUID("licenseAssignmentStates.skuId"),
		//_YUID("licenseAssignmentStates.assignedByGroup"),
		//_YUID("licenseAssignmentStates.state"),
		//_YUID("licenseAssignmentStates.error"),
		_YUID(O365_USER_REFRESTOKENVALIDFROMDATETIME),
	};

	for (size_t i = 0; i < 15; ++i)
	{
		const auto uid = wstring(_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES)) + _YUID(".extensionAttribute") + Str::getStringFromNumber(i + 1);
		const auto title = YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_34, _YLOC("Attribute %1 on premises"), Str::getStringFromNumber(i + 1).c_str());
		m_YUIDandTitles[uid] = title;
		m_Properties4RoleDelegation.insert(uid);
	}

	m_ComputedProperties =
	{
		_YUID(O365_USER_ACCOUNTENABLED)
	};

	g_AllowedStatus = YtriaTranslate::Do(GridTemplateUsers_GridTemplateUsers_1, _YLOC("Allowed")).c_str();
	g_BlockedStatus = YtriaTranslate::Do(GridTemplateUsers_GridTemplateUsers_2, _YLOC("Blocked")).c_str();
	g_Unlicensed	= YtriaTranslate::Do(GridTemplateUsers_GridTemplateUsers_3, _YLOC("-- Unlicensed --")).c_str();
}

const wstring& BusinessUserConfiguration::GetValueStringSignInStatus_Allowed() const
{
	return g_AllowedStatus;
}

const wstring& BusinessUserConfiguration::GetValueStringSignInStatus_Blocked() const
{
	return g_BlockedStatus;
}

const wstring& BusinessUserConfiguration::GetValueStringLicenses_Unlicensed() const
{
	return g_Unlicensed;
}