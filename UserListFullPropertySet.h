#pragma once

#include "IPropertySetBuilder.h"

class UserListFullPropertySet : public IPropertySetBuilder
{
public:
    virtual vector<rttr::property> GetPropertySet() const override;
};

