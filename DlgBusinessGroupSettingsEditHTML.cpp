#include "DlgBusinessGroupSettingsEditHTML.h"

DlgBusinessGroupSettingsEditHTML::DlgBusinessGroupSettingsEditHTML(
        const vector<BusinessGroupSetting>& p_GroupSettings,
        const std::vector<BusinessGroupSettingTemplate>& p_GroupSettingsTemplates,
        DlgFormsHTML::Action p_Action,
        CWnd* p_Parent)
    :   DlgFormsHTML(p_Action, p_Parent)
{
    for (const auto& setting : p_GroupSettings)
    {
        m_TenantSettingsByTemplateId[setting.GetTemplateId() ? *setting.GetTemplateId() : PooledString(_YTEXT(""))] = setting;
        m_TenantSettingsBySettingsId[setting.GetID()] = setting;
    }
    for (const auto& settingTemplate : p_GroupSettingsTemplates)
        m_BusinessGroupSettingsTemplates[settingTemplate.GetID()] = settingTemplate;
}

const map<PooledString, BusinessGroupSetting>& DlgBusinessGroupSettingsEditHTML::GetUpdatedSettings() const
{
    return m_UpdatedSettingsBySettingsId;
}

void DlgBusinessGroupSettingsEditHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgBusinessGroupSettings"), false, false, true);

    for (const auto& templateKeyVal : m_BusinessGroupSettingsTemplates)
    {
        const PooledString& templateId = templateKeyVal.first;
        const auto& settingsTemplate = templateKeyVal.second;

        uint32_t flags = 0;

        // Don't display some categories
        if (templateId == g_TemplateIdGroupUnifiedGuest || templateId == g_TemplateIdProhibitedNamesRestrictedSettings)
            continue;
        
        BusinessGroupSetting settings;
        // Search for an existing setting implementing this template
        auto settingItt = m_TenantSettingsByTemplateId.find(templateId);
        if (settingItt != m_TenantSettingsByTemplateId.end())
        {
            settings = settingItt->second;
        }
        else
        {
            // No setting for this template, but we still want to display it with the default values
            settings.SetID(templateId);
            std::vector<SettingValue> properties;
            for (const auto& prop : settingsTemplate.GetValues())
            {
                SettingValue value;
                value.Name = prop.Name;
                value.Value = prop.DefaultValue;
                properties.push_back(value);
            }
            settings.SetValues(properties);
        }

        bool isTemplateImplemented = settings.GetID() != templateId;

		getGenerator().addCategory(settingsTemplate.GetDisplayName() ? *settingsTemplate.GetDisplayName() : PooledString(_YTEXT("")), isTemplateImplemented ? CategoryFlags::EXPAND_BY_DEFAULT : CategoryFlags::NOFLAG);

		getGenerator().Add(WithTooltip<BoolToggleEditor>(
            {
                {
                    {
                        settings.GetID()
                        , YtriaTranslate::Do(DlgBusinessGroupSettingsEditHTML_generateJSONScriptData_1, _YLOC("Activated")).c_str()
						, flags | EditorFlags::DIRECT_EDIT
            			, isTemplateImplemented
                    }
                }
            }, YtriaTranslate::Do(DlgBusinessGroupSettingsEditHTML_generateJSONScriptData_2, _YLOC("Deactivate to delete this setting")).c_str()));

        for (const auto& property : settingsTemplate.GetValues())
        {
            ASSERT(boost::none != property.Type);
            ASSERT(boost::none != property.Name);
            if (boost::none != property.Type && boost::none != property.Name)
            {
                const wstring propertyId = settings.GetID() + g_IdDelim + *property.Name;
                if (*property.Type == BusinessGroupSettingTemplate::g_StringProperty ||
                    *property.Type == BusinessGroupSettingTemplate::g_GuidProperty)
                {
                    const auto& actualValues = settings.GetValues();
                    for (const auto& actualValue : actualValues)
                    {
                        if (actualValue.Name == property.Name)
                        {
                            // Exceptional case (if more than one, we should put the mess somewhere else instead of adding more junk here)
                            if (actualValue.Name != boost::none &&
                                templateId == g_TemplateIdPasswordRuleSettings &&
                                *actualValue.Name == g_PropertyBannedPasswordCheckOnPremisesMode)
                            {
                                wstring name = *actualValue.Name;
								getGenerator().Add(ComboEditor(
                                    { propertyId, InsertSpaceBetweenWords(*property.Name), flags, actualValue.Value ? *actualValue.Value : _YTEXT("") },
									{
									    { YtriaTranslate::Do(DlgHelpMain_OnInitDialog_6, _YLOC("Audit")).c_str(), _YTEXT("Audit") },
									    { YtriaTranslate::Do(DlgMultipleModificationsAdminServer_OnInitDialog_5, _YLOC("Enforce")).c_str(), _YTEXT("Enforce") }
									}));
                            }
                            else
                            {
                                getGenerator().Add(WithTooltip<StringEditor>({ propertyId, InsertSpaceBetweenWords(*property.Name), flags, actualValue.Value ? *actualValue.Value : _YTEXT("") }, property.Description ? *property.Description : PooledString(_YTEXT(""))));
                            }
                        }
                    }
                }
                else if (*property.Type == BusinessGroupSettingTemplate::g_BoolProperty)
                {
                    const auto& actualValues = settings.GetValues();
                    for (const auto& actualValue : actualValues)
                    {
                        if (actualValue.Name == property.Name)
                        {
                            bool val = false;
                            ASSERT(actualValue.Value != boost::none);
                            if (actualValue.Value != boost::none)
                            {
                                wstring actualVal = *actualValue.Value;
                                val = Str::toLower(*actualValue.Value) == UsefulStrings::g_ToggleValueTrue;
                            }

                            getGenerator().Add(WithTooltip<BoolToggleEditor>(
								{
									{
										{
                                            propertyId
											, InsertSpaceBetweenWords(*property.Name)
											, flags | EditorFlags::DIRECT_EDIT
                                            , val
										}
									}
								}, property.Description ? *property.Description : PooledString(_YTEXT(""))));
                        }
                    }
                }
                else if (*property.Type == BusinessGroupSettingTemplate::g_Int32Property)
                {
                    const auto& actualValues = settings.GetValues();
                    for (const auto& actualValue : actualValues)
                    {
                        if (actualValue.Name == property.Name)
							getGenerator().Add(WithTooltip<StringEditor>({ propertyId, InsertSpaceBetweenWords(*property.Name), flags, actualValue.Value ? *actualValue.Value : _YTEXT("") }, property.Description ? *property.Description : PooledString(_YTEXT(""))));
                        
                    }
                }
                else
                {
                    ASSERT(false);
                    // Unknown property type
                }
                getGenerator().addEvent(settings.GetID(), EventTarget({propertyId, EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueFalse }));
            }
        }
    }

    getGenerator().addApplyButton(isEditDialog() ? LocalizedStrings::g_ApplyButtonText : LocalizedStrings::g_CreateButtonText);
    getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
    getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);
}

bool DlgBusinessGroupSettingsEditHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
    for (const auto& formEntry : data)
    {
        const auto& formKey = formEntry.first;
        const auto& formValue = formEntry.second;
        std::vector<wstring> idParts = Str::explodeIntoVector(formKey, g_IdDelim);
        
        ASSERT(idParts.size() == 1 || idParts.size() == 2);
        if (idParts.size() == 1)
        {
            // "Activated" field
            const PooledString& templateId = idParts[0];
            
            bool isTemplateImplemented = m_TenantSettingsBySettingsId.find(templateId) != m_TenantSettingsBySettingsId.end();

            if ( (!isTemplateImplemented && formValue == UsefulStrings::g_ToggleValueTrue) ||
                 (isTemplateImplemented && formValue == UsefulStrings::g_ToggleValueFalse))
            {
                BusinessGroupSetting updatedSetting = GetOrCreateSettingById(templateId);
                updatedSetting.SetCreatedOrDeleted(formValue == UsefulStrings::g_ToggleValueTrue);
                m_UpdatedSettingsBySettingsId[updatedSetting.GetID()] = updatedSetting;
            }
        }
        else if (idParts.size() == 2)
        {
            // "Real" field belonging to a template
            const PooledString& settingsId = idParts[0];
            const PooledString& propertyName = idParts[1];

            BusinessGroupSetting updatedSetting = GetOrCreateSettingById(settingsId);
            AssignProperty(updatedSetting, propertyName, formValue);
            m_UpdatedSettingsBySettingsId[settingsId] = updatedSetting;
        }
    }

    return true;
}

BusinessGroupSetting DlgBusinessGroupSettingsEditHTML::GetOrCreateSettingById(const PooledString& p_SettingsId)
{
    BusinessGroupSetting updatedSetting;
    // First, check if it is in an already updated setting
    auto ittUpdatedSetting = m_UpdatedSettingsBySettingsId.find(p_SettingsId);
    if (ittUpdatedSetting == m_UpdatedSettingsBySettingsId.end())
    {
        // Not already in an updated setting, get it from the initial setting
        auto ittOldSetting = m_TenantSettingsBySettingsId.find(p_SettingsId);
        if (ittOldSetting != m_TenantSettingsBySettingsId.end())
        {
            updatedSetting = ittOldSetting->second;
        }
        else
        {
            // Not in the initial settings?
            // In that case, we need to create it and the settings id is the same as the template id
            const PooledString& id = p_SettingsId;
            updatedSetting = CreateUpdatedSetting(id, id); // Same 
            m_UpdatedSettingsBySettingsId[id] = updatedSetting;
        }
    }
    else
    {
        updatedSetting = ittUpdatedSetting->second;
    }

    return updatedSetting;
}

void DlgBusinessGroupSettingsEditHTML::AssignProperty(BusinessGroupSetting& p_GroupSetting, const PooledString& p_PropertyName, const PooledString& p_PropertyValue)
{
    SettingTemplateValue propertyInfo = GetSettingPropertyInfo(p_GroupSetting.GetTemplateId() ? *p_GroupSetting.GetTemplateId() : PooledString(_YTEXT("")), p_PropertyName);

    ASSERT(propertyInfo.Type != boost::none);
    if (propertyInfo.Type != boost::none)
    {
        for (auto& value : p_GroupSetting.GetValues())
        {
            if (value.Name == p_PropertyName)
            {
                // TODO: Handle every type differently (might be needed soon but not for now)
                if (*propertyInfo.Type == BusinessGroupSettingTemplate::g_StringProperty ||
                    *propertyInfo.Type == BusinessGroupSettingTemplate::g_GuidProperty ||
                    *propertyInfo.Type == BusinessGroupSettingTemplate::g_Int32Property ||
                    *propertyInfo.Type == BusinessGroupSettingTemplate::g_BoolProperty)
                {
                    value.Value = PooledString(p_PropertyValue);
                }
                break;
            }
        }
    }

}

SettingTemplateValue DlgBusinessGroupSettingsEditHTML::GetSettingPropertyInfo(const PooledString& p_TemplateId, const PooledString& p_PropertyName) const
{
    SettingTemplateValue info;
    
    auto ittTemplate = m_BusinessGroupSettingsTemplates.find(p_TemplateId);
    ASSERT(ittTemplate != m_BusinessGroupSettingsTemplates.end());
    if (ittTemplate != m_BusinessGroupSettingsTemplates.end())
    {
        for (const auto& property : ittTemplate->second.GetValues())
        {
            if (property.Name == p_PropertyName)
            {
                info = property;
                break;
            }
        }
    }

    return info;
}

const wstring DlgBusinessGroupSettingsEditHTML::g_IdDelim = _YTEXT("_");

const wstring DlgBusinessGroupSettingsEditHTML::g_TemplateIdGroupUnifiedGuest = _YTEXT("08d542b9-071f-4e16-94b0-74abb372e3d9");
const wstring DlgBusinessGroupSettingsEditHTML::g_TemplateIdProhibitedNamesRestrictedSettings = _YTEXT("aad3907d-1d1a-448b-b3ef-7bf7f63db63b");
const wstring DlgBusinessGroupSettingsEditHTML::g_TemplateIdPasswordRuleSettings = _YTEXT("5cf42378-d67d-4f36-ba46-e8b86229381d");
const wstring DlgBusinessGroupSettingsEditHTML::g_PropertyBannedPasswordCheckOnPremisesMode = _YTEXT("BannedPasswordCheckOnPremisesMode");

PooledString DlgBusinessGroupSettingsEditHTML::InsertSpaceBetweenWords(const PooledString& p_InputStr)
{
    const wstring inputStr = p_InputStr;
    wstring resultStr;

    for (auto itt = inputStr.begin(); itt != inputStr.end(); ++itt)
    {
        if (itt != inputStr.begin() && ::isupper(*itt))
            resultStr += _YTEXT(' ');
        resultStr += *itt;
    }
    return resultStr;
}

BusinessGroupSetting DlgBusinessGroupSettingsEditHTML::CreateUpdatedSetting(const PooledString& p_SettingsId, const PooledString& p_TemplateId)
{
    BusinessGroupSetting setting;
    setting.SetID(p_SettingsId);
    setting.SetTemplateId(p_TemplateId);

    auto ittTemplate = m_BusinessGroupSettingsTemplates.find(p_TemplateId);
    ASSERT(ittTemplate != m_BusinessGroupSettingsTemplates.end());
    if (ittTemplate != m_BusinessGroupSettingsTemplates.end())
    {
        // Create the setting from the default values
        for (const auto& templateVal : ittTemplate->second.GetValues())
        {
            SettingValue newValue;
            newValue.Name = templateVal.Name;
            newValue.Value = templateVal.DefaultValue;
            setting.GetValues().push_back(newValue);
        }
    }
    return setting;
}

