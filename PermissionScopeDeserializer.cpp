#include "PermissionScopeDeserializer.h"

#include "JsonSerializeUtil.h"

void PermissionScopeDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("adminConsentDescription"), m_Data.m_AdminConsentDescription, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("adminConsentDisplayName"), m_Data.m_AdminConsentDisplayName, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isEnabled"), m_Data.m_IsEnabled, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("origin"), m_Data.m_Origin, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.m_Type, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("userConsentDescription"), m_Data.m_UserConsentDescription, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("userConsentDisplayName"), m_Data.m_UserConsentDisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("value"), m_Data.m_Value, p_Object);
}
