#pragma once

#include "BusinessDirectoryRoleTemplate.h"
#include "BusinessDirectoryRole.h"

#include <vector>

struct DirectoryRolesInfo
{
    std::vector<BusinessDirectoryRoleTemplate> m_Templates;
    std::vector<BusinessDirectoryRole> m_Roles;
};

