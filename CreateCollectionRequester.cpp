#include "CreateCollectionRequester.h"
#include "CosmosDBSqlSession.h"
#include "DumpDeserializer.h"
#include "LoggerService.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "AzureADCommonData.h"
#include "BasicHttpRequestLogger.h"

Cosmos::CreateCollectionRequester::CreateCollectionRequester(const wstring& p_DatabaseName, const wstring& p_CollectionName)
    : m_DbName(p_DatabaseName),
    m_CollectionName(p_CollectionName)
{
}

TaskWrapper<void> Cosmos::CreateCollectionRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<DumpDeserializer>();
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("dbs"));
    uri.append_path(m_DbName);
    uri.append_path(_YTEXT("colls"));

    web::json::value body = web::json::value::object();
    body[_YTEXT("id")] = web::json::value::string(m_CollectionName);

    const WebPayloadJSON payload(body);

    LoggerService::User(YtriaTranslate::Do(Cosmos__CreateCollectionRequester_Send_1, _YLOC("Creating collection with name \"%1\" in database %2 in CosmosDB"), m_CollectionName.c_str(), m_DbName.c_str()), p_TaskData.GetOriginator());

    vector<pair<wstring, wstring>> additionalHeaders {
        { _YTEXT("x-ms-offer-throughput"), _YTEXT("400") }
    };

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetCosmosDBSqlSession()->Post(uri.to_uri(),
        httpLogger,
        payload,
        _YTEXT("colls"),
        wstring(_YTEXT("dbs/")) + m_DbName,
        p_Session->GetCosmosDbMasterKey(),
        m_Result,
        m_Deserializer,
        p_TaskData,
        additionalHeaders);
}

const wstring& Cosmos::CreateCollectionRequester::GetData() const
{
    return m_Deserializer->GetData();
}

const SingleRequestResult& Cosmos::CreateCollectionRequester::GetResult() const
{
    return *m_Result;
}
