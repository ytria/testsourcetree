#include "LinkHandlerJobScheduleEdit.h"

#include "MainFrame.h"

void LinkHandlerJobScheduleEdit::Handle(YBrowserLink& p_Link) const
{
	// run job whose key is the uri path
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame && mainFrame->GetAutomationWizard() != nullptr);
	if (nullptr != mainFrame && mainFrame->GetAutomationWizard() != nullptr)
		mainFrame->GetAutomationWizard()->Run(p_Link.GetUrl().path().substr(1), wstring());// .substr(1): path starts with '/'
}