#include "EmailAddressDeserializer.h"

#include "JsonSerializeUtil.h"

void EmailAddressDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("address"), m_Data.m_Address, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);
}
