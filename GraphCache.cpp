#include "GraphCache.h"

#include "BasicPageRequestLogger.h"
#include "BasicRequestLogger.h"
#include "BusinessObjectManager.h"
#include "CachedGroupPropertySet.h"
#include "CachedOrgContactPropertySet.h"
#include "CachedUserPropertySet.h"
#include "ConcatVector.h"
#include "DbGroupSerializer.h"
#include "DbUserSerializer.h"
#include "DeletedGroupListRequester.h"
#include "DeletedUserListRequester.h"
#include "DriveDeserializer.h"
#include "GroupDeserializer.h"
#include "GroupListRequester.h"
#include "GroupRequester.h"
#include "LoggerService.h"
#include "MainFrame.h"
#include "MSGraphCommonData.h"
#include "MsGraphFieldNames.h"
#include "MSGraphSession.h"
#include "MultiObjectsPageRequestLogger.h"
#include "MultiObjectsRequestLogger.h"
#include "MsGraphHttpRequestLogger.h"
#include "OrganizationRequester.h"
#include "OrgContactListRequester.h"
#include "OrgContactRequester.h"
#include "RESTUtils.h"
#include "RunOnScopeEnd.h"
#include "safeTaskCall.h"
#include "SiteRequester.h"
#include "SqlCacheConfig.h"
#include "UserDeserializer.h"
#include "UserListFullPropertySet.h"
#include "UserListRequester.h"
#include "UserRequester.h"
#include "ValueListDeserializer.h"
#include "WholeUserDeserializer.h"
#include "WholeGroupDeserializer.h"
#include "WholeSiteDeserializer.h"
#include "UserListFullPropertySet.h"
#include "GroupListFullPropertySet.h"

using namespace Rest;

void GraphCache::Clear()
{
	ClearObjectLists();
	ClearOrgAndConnectedUser();
}

void GraphCache::ClearObjectLists()
{
	ClearUsers();
	ClearGroups();
	ClearOrgContacts();
	ClearSites();
	ClearDeletedUsers();
}

void GraphCache::ClearOrgAndConnectedUser()
{
	m_ConnectedUser.reset();
	m_ConnectedUserProfilePicture.reset();
	m_Organization.reset();
}

void GraphCache::ClearUsers()
{
	m_CachedUsers.clear();
	m_SkuCounters.reset();
	m_SyncedAllUsers = false;
	m_SqlUsersCacheLoaded = false;
}

void GraphCache::ClearGroups()
{
	m_CachedGroups.clear();
	m_SyncedAllGroups = false;
	m_SqlGroupsCacheLoaded = false;
}

void GraphCache::ClearOrgContacts()
{
	m_CachedOrgContacts.clear();
	m_SyncedAllOrgContacts = false;
}

void GraphCache::ClearSites()
{
	m_CachedSites.clear();
}

void GraphCache::ClearDeletedUsers()
{
	m_CachedDeletedUsers.clear();
	m_SyncedAllDeletedUsers = false;
}

void GraphCache::ClearDeletedGroups()
{
	m_CachedDeletedGroups.clear();
	m_SyncedAllDeletedGroups = false;
}

bool GraphCache::IsUsersDeltaSyncAvailable() const
{
	return m_UsersDeltaLink && !m_UsersDeltaLink->empty();
}

bool GraphCache::IsGroupsDeltaSyncAvailable() const
{
	ASSERT(m_PersistentSession.IsValid());
	return m_GroupsDeltaLink && !m_GroupsDeltaLink->empty();
}

bool GraphCache::GetAutoLoadOnPrem() const
{
	ASSERT(m_PersistentSession.IsValid());
	return m_PersistentSession.IsValid() && m_PersistentSession.GetAutoLoadOnPrem();
}

bool GraphCache::GetAskedAutoLoadOnPrem() const
{
	ASSERT(m_PersistentSession.IsValid());
	return m_PersistentSession.IsValid() && m_PersistentSession.GetAskedAutoLoadOnPrem();
}

bool GraphCache::GetUseOnPrem() const
{
	ASSERT(m_PersistentSession.IsValid());
	return m_PersistentSession.IsValid() && m_PersistentSession.GetUseOnPrem();
}

bool GraphCache::GetDontAskAgainUseOnPrem() const
{
	ASSERT(m_PersistentSession.IsValid());
	return m_PersistentSession.IsValid() && m_PersistentSession.GetDontAskAgainUseOnPrem();
}

void GraphCache::SetHybridCheckSuccess(bool p_Value)
{
	m_HybridCheckSuccess = p_Value;
}

bool GraphCache::GetHybridCheckSuccess() const
{
	return m_HybridCheckSuccess;
}

bool GraphCache::GetUseConsistencyGuid() const
{
	ASSERT(m_PersistentSession.IsValid());
	return m_PersistentSession.GetUseMsDsConsistencyGuid();
}

wstring GraphCache::GetAADComputerName() const
{
	ASSERT(m_PersistentSession.IsValid());
	return m_PersistentSession.GetAADComputerName();
}

wstring GraphCache::GetADDSServer() const
{
	ASSERT(m_PersistentSession.IsValid());
	return m_PersistentSession.GetADDSServer();
}

wstring GraphCache::GetADDSUsername() const
{
	ASSERT(m_PersistentSession.IsValid());
	return m_PersistentSession.GetADDSUsername();
}

wstring GraphCache::GetADDSPassword() const
{
	ASSERT(m_PersistentSession.IsValid());
	return m_PersistentSession.GetADDSPassword();
}

void GraphCache::Sync()
{
	LoggerService::Debug(_YTEXT("GraphCache::Sync(): Starting to sync cache"));

	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());

	auto syncedProfilePicture	= std::make_shared<bool>(false);
	auto syncedOrganization		= std::make_shared<bool>(false);

	ASSERT(nullptr != app);
	if (nullptr != app)
	{
		/*** WARNING!!!! This method should only be called at session login (in MainFrame) ***/
		/*** During login, the app knows if the session is Full Admin or not (the GraphSession doesn't...) ***/
		if (app->IsSessionBeingLoadedUltraAdmin())
		{
			// In full admin, MainFrame::OnReadyToTestAuthentication is never called then Organization is not initialized
			ASSERT(!m_Organization.is_initialized());
			if (!m_Organization.is_initialized())
			{
				// The caller wraps this whole method call in a safeMSGraphCall, do not use it here so that exception reaches the caller
				auto syncOrganization = /*safeMSGraphCall(*/GetOrganization(YtriaTaskData())/*, m_MSGraphSession)*/.Then([syncedOrganization](const Organization& p_Organization)
				{
					*syncedOrganization = true;
					LoggerService::Debug(_YTEXT("GraphCache::Sync(): Successfully synced organization"));
					return p_Organization;
				});

				auto org = syncOrganization.get();
				if (*syncedOrganization)
					m_Organization = org;
			}
			else
			{
				*syncedOrganization = true;
			}

			if (!*syncedOrganization)
			{
				m_Organization = boost::none;
				LoggerService::Debug(_YTEXT("GraphCache::Sync(): Unable to sync organization"));
			}
		}
		else
		{
			const bool isPartner = SessionTypes::g_PartnerAdvancedSession == app->GetSessionBeingLoadedSessionType()
								|| SessionTypes::g_PartnerElevatedSession == app->GetSessionBeingLoadedSessionType();
			if (!isPartner)
				m_ConnectedUser = getConnectedUser(YtriaTaskData()).GetTask().get();

			if (m_ConnectedUser && app->GetIsCreatingSession()
				&& MainFrame::IsOverwritingExistingElevated(*m_ConnectedUser->GetUserPrincipalName(), app->GetSessionBeingLoadedSessionType()).first)
					return; // Don't waste time syncing if we won't use the resulting data

			if (isPartner || (m_ConnectedUser != boost::none && !m_ConnectedUser->m_Id.IsEmpty()))
			{
				// in case of new session, MainFrame::OnReadyToTestAuthentication is not called, then Organization is not initialized.
				// Do not assert.
				//ASSERT(m_Organization.is_initialized());
				if (!m_Organization.is_initialized())
				{
					// The caller wraps this whole method call in a safeMSGraphCall, do not use it here so that exception reaches the caller
					auto syncOrganization = /*safeMSGraphCall(*/GetOrganization(YtriaTaskData())/*, m_MSGraphSession)*/.Then([syncedOrganization](const Organization& p_Organization)
					{
						*syncedOrganization = true;
						return p_Organization;
					});

					auto org = syncOrganization.get();
					if (*syncedOrganization)
						m_Organization = org;
				}
				else
				{
					*syncedOrganization = true;
				}

				if (!isPartner)
				{
					auto syncProfilePicture = safeTaskCall(getProfilePicture(YtriaTaskData()), getSapio365Session()->GetMainMSGraphSession(), YtriaTaskData()).Then([syncedProfilePicture](const std::vector<unsigned char>& p_Picture)
						{
							*syncedProfilePicture = true;
							return p_Picture;
						});

					syncProfilePicture.GetTask().wait();
					if (*syncedProfilePicture)
						m_ConnectedUserProfilePicture = syncProfilePicture.get();
					else
						m_ConnectedUserProfilePicture = boost::none;
				}

				if (!*syncedOrganization)
				{
					m_Organization = boost::none;
					LoggerService::Debug(_YTEXT("GraphCache::Sync(): Unable to sync organization"));
				}
			}
			else
				m_ConnectedUser = boost::none;
		}
	}
}

void GraphCache::SetSyncError(const wstring& p_Error)
{
	ASSERT(m_SyncError.empty() || p_Error.empty()); // Is this normal to have another error while there already had one?
	m_SyncError = p_Error;
}

const wstring& GraphCache::GetSyncError() const
{
	return m_SyncError;
}

vector<BusinessUser> GraphCache::getSqlCachedUsers(HWND p_Hwnd, const std::vector<UBI>& p_RequiredBlocks, const O365IdsContainer& p_IDs, bool p_StoreInGraphCache, bool p_EnsureOrder, YtriaTaskData p_TaskData/* = YtriaTaskData()*/)
{
	ASSERT(p_StoreInGraphCache); // Why wouldn't you want to update memory cache?

	vector<BusinessUser> users;

	if (p_IDs.empty())
		LoggerService::User(YtriaTranslate::Do(GraphCache_GetSqlCachedUsers_1, _YLOC("Loading users from cache...")).c_str(), p_Hwnd);
	else
		LoggerService::User(_YFORMAT(L"Loading %s users from cache...", std::to_wstring(p_IDs.size()).c_str()), p_Hwnd);

	ASSERT(m_SqlCache);
	if (m_SqlCache)
	{
		const auto requiredBlockNames = SqlCacheConfig<UserCache>::DataBlockNames(p_RequiredBlocks);
		const bool loadAll = p_IDs.empty();
		auto jsonUsers = m_SqlCache->LoadUsers(p_IDs, requiredBlockNames);

		if (p_EnsureOrder && !p_IDs.empty())
		{
			// Ensure requested order is preserved (probably not very efficient when many ids)
			//LoggerService::User(_T("Sorting users..."), p_Hwnd);
			std::sort(jsonUsers.begin(), jsonUsers.end(), [&p_IDs](const auto& lhUser, const auto& rhUser)
				{
					return p_IDs.find(lhUser.m_Id) < p_IDs.find(rhUser.m_Id);
				});
		}

		users.reserve(jsonUsers.size());
		size_t count = 0;
		bool canceled = false;
		for (const auto& jsonUser : jsonUsers)
		{
			if (p_TaskData.IsCanceled())
			{
				canceled = true;
				break;
			}
			WholeUserDeserializer d(getSapio365Session());
			web::json::value obj = mergeBodies(jsonUser.m_ObjectBody);
			d.Deserialize(obj);
			users.push_back(std::move(d.GetData()));
			if ((++count % 1000) == 0)
				LoggerService::User(_YFORMAT(L"Loading users from cache %s/%s", Str::getStringFromNumber(count).c_str(), Str::getStringFromNumber(jsonUsers.size()).c_str()), p_Hwnd);
		}

		LoggerService::User(_YFORMAT(L"Loaded %s users from cache", Str::getStringFromNumber(count).c_str()), p_Hwnd);

		if (p_StoreInGraphCache)
		{
			if (IsHandleSkuCounters() && loadAll && !canceled && IsSqlUsersCacheFull()
				&& (requiredBlockNames.empty()
					|| requiredBlockNames.end() != std::find(requiredBlockNames.begin(), requiredBlockNames.end(), SqlCacheObjectSpecific<UserCache>::DataBlockName(UBI::ASSIGNEDLICENSES))))
			{
				addUsersAndCountSkus(users, false);
			}
			else
			{
				AddUsers(users, false);
			}

			if (loadAll && !canceled)
			{
				if (IsSqlUsersCacheFull())
					setAllUsersSynced();
				m_SqlUsersCacheLoaded = true;
			}
		}
	}

	return users;
}

vector<BusinessGroup> GraphCache::getSqlCachedGroups(HWND p_Hwnd, const std::vector<GBI>& p_RequiredBlocks, const O365IdsContainer& p_IDs, bool p_StoreInGraphCache, bool p_EnsureOrder, YtriaTaskData p_TaskData/* = YtriaTaskData()*/)
{
	ASSERT(p_StoreInGraphCache); // Why wouldn't you want to update memory cache?
	
	vector<BusinessGroup> groups;

	if (p_IDs.empty())
		LoggerService::User(YtriaTranslate::Do(GraphCache_GetSqlCachedGroups_1, _YLOC("Loading groups from cache...")).c_str(), p_Hwnd);
	else
		LoggerService::User(_YFORMAT(L"Loading %s groups from cache...", std::to_wstring(p_IDs.size()).c_str()), p_Hwnd);

	ASSERT(m_SqlCache);
	if (m_SqlCache)
	{
		const auto requiredBlockNames = SqlCacheConfig<GroupCache>::DataBlockNames(p_RequiredBlocks);
		const bool loadAll = p_IDs.empty();
		auto jsonGroups = m_SqlCache->LoadGroups(p_IDs, requiredBlockNames);

		if (p_EnsureOrder && !p_IDs.empty())
		{
			// Ensure requested order is preserved (probably not very efficient when many ids)
			//LoggerService::User(_T("Sorting groups..."), p_Hwnd);
			std::sort(jsonGroups.begin(), jsonGroups.end(), [&p_IDs](const auto& lhGroup, const auto& rhGroup)
				{
					return p_IDs.find(lhGroup.m_Id) < p_IDs.find(rhGroup.m_Id);
				});
		}

		groups.reserve(jsonGroups.size());
		size_t count = 0;
		bool canceled = false;
		for (const auto& jsonGroup : jsonGroups)
		{
			if (p_TaskData.IsCanceled())
			{
				canceled = true;
				break;
			}
			WholeGroupDeserializer d(getSapio365Session());
			web::json::value obj = mergeBodies(jsonGroup.m_ObjectBody);
			d.Deserialize(obj);
			groups.push_back(d.GetData());
			if ((++count % 1000) == 0)
				LoggerService::User(_YFORMAT(L"Loading groups from cache %s/%s", Str::getStringFromNumber(count).c_str(), Str::getStringFromNumber(jsonGroups.size()).c_str()), p_Hwnd);
		}

		groups.shrink_to_fit();

		LoggerService::User(_YFORMAT(L"Loaded %s groups from cache", Str::getStringFromNumber(count).c_str()), p_Hwnd);

		if (p_StoreInGraphCache)
		{
			AddGroups(groups, false);

			if (loadAll && !canceled)
			{
				if (IsSqlGroupsCacheFull())
					setAllGroupsSynced();
				m_SqlGroupsCacheLoaded = true;
			}
		}
	}

	return groups;
}

vector<BusinessUser> GraphCache::GetSqlCachedUsers(HWND p_Hwnd, const std::vector<UBI>& p_RequiredBlocks, YtriaTaskData p_TaskData/* = YtriaTaskData()*/)
{
	return getSqlCachedUsers(p_Hwnd, p_RequiredBlocks, {}, true, false, p_TaskData);
}

vector<BusinessGroup> GraphCache::GetSqlCachedGroups(HWND p_Hwnd, const std::vector<GBI>& p_RequiredBlocks, YtriaTaskData p_TaskData/* = YtriaTaskData()*/)
{
	return getSqlCachedGroups(p_Hwnd, p_RequiredBlocks, {}, true, false, p_TaskData);
}

vector<BusinessUser> GraphCache::GetSqlCachedUsers(HWND p_Hwnd, const O365IdsContainer& p_IDs, bool p_EnsureOrder, YtriaTaskData p_TaskData/* = YtriaTaskData()*/)
{
	return getSqlCachedUsers(p_Hwnd, {}, p_IDs, true, p_EnsureOrder, p_TaskData);
}

vector<BusinessGroup> GraphCache::GetSqlCachedGroups(HWND p_Hwnd, const O365IdsContainer& p_IDs, bool p_EnsureOrder, YtriaTaskData p_TaskData/* = YtriaTaskData()*/)
{
	return getSqlCachedGroups(p_Hwnd, {}, p_IDs, true, p_EnsureOrder, p_TaskData);
}

vector<BusinessUser> GraphCache::GetSqlCachedUsers(HWND p_Hwnd, const O365IdsContainer& p_IDs, const std::vector<UBI>& p_RequiredBlocks, bool p_EnsureOrder, YtriaTaskData p_TaskData/* = YtriaTaskData()*/)
{
	return getSqlCachedUsers(p_Hwnd, p_RequiredBlocks, p_IDs, true, p_EnsureOrder, p_TaskData);
}

vector<BusinessGroup> GraphCache::GetSqlCachedGroups(HWND p_Hwnd, const O365IdsContainer& p_IDs, const std::vector<GBI>& p_RequiredBlocks, bool p_EnsureOrder, YtriaTaskData p_TaskData/* = YtriaTaskData()*/)
{
	return getSqlCachedGroups(p_Hwnd, p_RequiredBlocks, p_IDs, true, p_EnsureOrder, p_TaskData);
}

bool GraphCache::IsSqlUsersCacheFull()
{
	return -1 < GetSqlUsersCacheFullListSize();
}

bool GraphCache::IsSqlGroupsCacheFull()
{
	return -1 < GetSqlGroupsCacheFullListSize();
}

int GraphCache::GetSqlUsersCacheFullListSize()
{
	ASSERT(m_SqlCache);
	if (m_SqlCache)
	{
		const auto count = m_SqlCache->LoadUsersMetadata().m_FullListSize;
		return count ? *count : -1;
	}
	return -1;
}

int GraphCache::GetSqlGroupsCacheFullListSize()
{
	ASSERT(m_SqlCache);
	if (m_SqlCache)
	{
		const auto count = m_SqlCache->LoadGroupsMetadata().m_FullListSize;
		return count ? *count : -1;
	}
	return -1;
}

const boost::YOpt<YTimeDate> GraphCache::GetSqlUsersCacheLastFullRefreshDate()
{
	ASSERT(m_SqlCache);
	if (m_SqlCache)
		return m_SqlCache->LoadUsersMetadata().m_FullRefreshDate;

	return boost::none;
}

const boost::YOpt<YTimeDate> GraphCache::GetSqlGroupsCacheLastFullRefreshDate()
{
	ASSERT(m_SqlCache);
	if (m_SqlCache)
		return m_SqlCache->LoadGroupsMetadata().m_FullRefreshDate;

	return boost::none;
}

int GraphCache::CountSqlUsersMissingRequirements(const std::vector<UBI>& p_RequiredBlocks)
{
	ASSERT(m_SqlCache);
	if (m_SqlCache)
		return m_SqlCache->CountUsersMissingRequirements(SqlCacheConfig<UserCache>::DataBlockNames(p_RequiredBlocks));

	return 0;
}

int GraphCache::CountSqlGroupsMissingRequirements(const std::vector<GBI>& p_RequiredBlocks)
{
	ASSERT(m_SqlCache);
	if (m_SqlCache)
		return m_SqlCache->CountGroupsMissingRequirements(SqlCacheConfig<GroupCache>::DataBlockNames(p_RequiredBlocks));

	return 0;
}

const std::shared_ptr<CachedObjectsSqlEngine>& GraphCache::GetSqlCache() const
{
	return m_SqlCache;
}

bool GraphCache::IsSqlUsersCacheLoaded() const
{
	return m_SqlUsersCacheLoaded;
}

bool GraphCache::IsSqlGroupsCacheLoaded() const
{
	return m_SqlGroupsCacheLoaded;
}

void GraphCache::LoadFromSqlUsersCacheIfNotAlreadyInMemory(HWND p_Hwnd, const vector<PooledString>& p_Ids)
{
	if (!IsSqlUsersCacheLoaded()
		&& !std::all_of(p_Ids.begin(), p_Ids.end(), [this, p_Hwnd](auto& id){ return GetUserInCache(id, false).GetID() == id; }))
	{
		// FIXME: Load only required users?
		GetSqlCachedUsers(p_Hwnd, { UBI::MIN });
	}

	ASSERT(std::all_of(p_Ids.begin(), p_Ids.end(), [this](auto& id) { return GetUserInCache(id, false).GetID() == id; }));
}

void GraphCache::LoadFromSqlGroupsCacheIfNotAlreadyInMemory(HWND p_Hwnd, const vector<PooledString>& p_Ids)
{
	if (!IsSqlGroupsCacheLoaded()
		&& !std::all_of(p_Ids.begin(), p_Ids.end(), [this, p_Hwnd](auto& id) { return GetGroupInCache(id).GetID() == id; }))
	{
		// FIXME: Load only required groups?
		GetSqlCachedGroups(p_Hwnd, { GBI::MIN });
	}

	ASSERT(std::all_of(p_Ids.begin(), p_Ids.end(), [this](auto& id){ return GetGroupInCache(id).GetID() == id; }));
}

namespace
{
	static const double g_ExpirationInSeconds = 24 * 60 * 60; // 24h
}

bool GraphCache::IsSqlUsersCacheExpired() const
{
	bool result = false;

	ASSERT(m_SqlCache);
	if (m_SqlCache)
	{
		result = true;
		const auto metadata = m_SqlCache->LoadUsersMetadata();
		if (metadata.m_FullRefreshDate)
		{
			const auto diffInSeconds = YTimeDate::GetCurrentTimeDate() - *metadata.m_FullRefreshDate;
			return diffInSeconds > g_ExpirationInSeconds;
		}
	}

	return result;	
}

bool GraphCache::IsSqlGroupsCacheExpired() const
{
	bool result = false;

	ASSERT(m_SqlCache);
	if (m_SqlCache)
	{
		result = true;
		const auto metadata = m_SqlCache->LoadGroupsMetadata();
		if (metadata.m_FullRefreshDate)
		{
			const auto diffInSeconds = YTimeDate::GetCurrentTimeDate() - *metadata.m_FullRefreshDate;
			return diffInSeconds > g_ExpirationInSeconds;
		}
	}

	return result;
}

std::shared_ptr<Sapio365Session> GraphCache::getSapio365Session() const
{
	return m_Sapio365Session.lock();
}

TaskWrapper<BusinessUser> GraphCache::getConnectedUser(YtriaTaskData p_Task)
{
	if (boost::none != m_ConnectedUser)
		return YSafeCreateTask([this]() { return *m_ConnectedUser; });
	else
	{
		// Pass nullptr to avoid role tagging.
        auto deserializer = std::make_shared<UserDeserializer>(nullptr);
		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, getSapio365Session()->GetIdentifier());
		return getSapio365Session()->GetMainMSGraphSession()->GetConnectedUser(deserializer, httpLogger, p_Task).Then([deserializer, this]
		{
            m_ConnectedUser = deserializer->GetData();
			// FIXME: This might potentially create issues with RBAC as we're storing the user
			// without taking RBAC props into account (RBAC not enabled yet when this point is reached...)
			// FIXME2: Can't add to SQL (too early, session not ready!)
			AddUser(*m_ConnectedUser);
			return *m_ConnectedUser;
		});
	}
}

void GraphCache::SetOrganization(const Organization& p_Organization)
{
	m_Organization = p_Organization;
}

TaskWrapper<Organization> GraphCache::GetOrganization(YtriaTaskData p_Task) const
{
	if (boost::none != m_Organization)
		return pplx::task_from_result(*m_Organization);
    else
    {
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("organization"));
        auto requester = std::make_shared<OrganizationRequester>(false, logger);
        return requester->Send(getSapio365Session(), p_Task).Then([requester]() {
            if (!requester->GetData().empty())
                return requester->GetData()[0];
            ASSERT(false);
            return Organization();
        });
    }
}

const boost::YOpt<Organization>& GraphCache::GetCachedOrganization() const
{
	return m_Organization;
}

void GraphCache::SetRBACOrganization(const boost::YOpt<Organization>& p_RBACOrganization)
{
	m_RBACOrganization = p_RBACOrganization;
}

const boost::YOpt<Organization>& GraphCache::GetRBACOrganization() const
{
	if (!getSapio365Session()->IsUseRoleDelegation())
	{
		ASSERT(!m_RBACOrganization);
		return GetCachedOrganization();
	}

	if (!m_RBACOrganization)
	{
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("organization"));
		auto requester = std::make_shared<OrganizationRequester>(true, logger);
		m_RBACOrganization = requester->Send(getSapio365Session(), YtriaTaskData()).Then([requester, this]() {
			if (!requester->GetData().empty())
				return requester->GetData()[0];
			ASSERT(false);
			return Organization();
		}).GetTask().get();
	}

	ASSERT(m_RBACOrganization);
	return m_RBACOrganization;
}

void GraphCache::StoreGroupMembers(const BusinessGroup& p_Group)
{
	std::lock_guard<std::mutex> lock(m_GroupMembersLock);
	auto& groupMembers = m_GroupMembers[p_Group.GetID()];
	groupMembers.m_Groups = p_Group.GetChildrenGroups();
	groupMembers.m_OrgContacts = p_Group.GetChildrenOrgContacts();
	groupMembers.m_Users = p_Group.GetChildrenUsers();
	groupMembers.m_Error = p_Group.GetError();
}

bool GraphCache::GetGroupMembers(BusinessGroup& p_Group)
{
	std::lock_guard<std::mutex> lock(m_GroupMembersLock);
	if (m_GroupMembers.end() != m_GroupMembers.find(p_Group.GetID()))
	{
		const auto& groupMembers = m_GroupMembers[p_Group.GetID()];

		p_Group.SetChildrenGroups(groupMembers.m_Groups);
		p_Group.SetChildrenOrgContacts(groupMembers.m_OrgContacts);
		p_Group.SetChildrenUsers(groupMembers.m_Users);
		if (groupMembers.m_Error)
			p_Group.SetError(groupMembers.m_Error);

		return true;
	}

	return false;
}

std::map<PooledString, int32_t> GraphCache::GetSkuCounters() const
{
	std::map<PooledString, int32_t> counters;

	{
		std::lock_guard<std::mutex> lock(m_CachedUsersLock);
		if (m_SkuCounters)
			counters = *m_SkuCounters;
	}

	return counters;
}

bool GraphCache::IsHandleSkuCounters() const
{
	return getSapio365Session() && getSapio365Session()->IsUseRoleDelegation()
		&& RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(getSapio365Session()->GetRoleDelegationID(), getSapio365Session()).HasSkuAccessLimits()
		&& RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(getSapio365Session()->GetRoleDelegationID(), getSapio365Session()).HasPrivilege(RoleDelegationUtil::RBAC_USER_LICENSES_EDIT);
}

TaskWrapper<vector<uint8_t>> GraphCache::getProfilePicture(YtriaTaskData p_Task)
{
	if (boost::none != m_ConnectedUserProfilePicture)
		return YSafeCreateTask([this]() { return *m_ConnectedUserProfilePicture; });
	else
	{
		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, getSapio365Session()->GetIdentifier());
		return getSapio365Session()->GetMainMSGraphSession()->GetProfilePicture(httpLogger, p_Task);
	}
}

size_t GraphCache::GetUserCacheSize() const
{
    return m_CachedUsers.size();
}

size_t GraphCache::GetGroupCacheSize() const
{
    return m_CachedGroups.size();
}

size_t GraphCache::GetOrgContactCacheSize() const
{
	return m_CachedOrgContacts.size();
}

size_t GraphCache::GetSiteCacheSize() const
{
    return m_CachedSites.size();
}

size_t GraphCache::GetDeletedUserCacheSize() const
{
	return m_CachedDeletedUsers.size();
}

size_t GraphCache::GetDeletedGroupCacheSize() const
{
	return m_CachedDeletedGroups.size();
}

vector<CachedUser> GraphCache::GetCachedUsers() const
{
    vector<CachedUser> cachedUsers;

    std::lock_guard<std::mutex> lock(m_CachedUsersLock);
    for (const auto& keyVal : m_CachedUsers)
        cachedUsers.push_back(keyVal.second);

    return cachedUsers;
}

vector<CachedUser> GraphCache::GetCachedUsers(const O365IdsContainer& p_Ids) const
{
	if (p_Ids.empty())
		return GetCachedUsers();

	vector<CachedUser> cachedUsers;
	cachedUsers.resize(p_Ids.size());

	size_t count = 0;

	{
		std::lock_guard<std::mutex> lock(m_CachedUsersLock);
		for (const auto& keyVal : m_CachedUsers)
		{
			auto it = p_Ids.find(keyVal.first);
			if (p_Ids.end() != it)
			{
				cachedUsers[std::distance(p_Ids.begin(), it)] = keyVal.second;
				++count;
			}
		}
	}

	// If this fails, some groups in resulting vector are invalid.
	ASSERT(count == p_Ids.size());

	return cachedUsers;
}
	

vector<CachedGroup> GraphCache::GetCachedGroups() const
{
    vector<CachedGroup> cachedGroups;

    std::lock_guard<std::mutex> lock(m_CachedGroupsLock);
    for (const auto& keyVal : m_CachedGroups)
        cachedGroups.push_back(keyVal.second);

    return cachedGroups;
}

vector<CachedGroup> GraphCache::GetCachedGroups(const O365IdsContainer& p_Ids) const
{
	if (p_Ids.empty())
		return GetCachedGroups();

	vector<CachedGroup> cachedGroups;
	cachedGroups.resize(p_Ids.size());

	size_t count = 0;

	{
		std::lock_guard<std::mutex> lock(m_CachedGroupsLock);
		for (const auto& keyVal : m_CachedGroups)
		{
			auto it = p_Ids.find(keyVal.first);
			if (p_Ids.end() != it)
			{
				cachedGroups[std::distance(p_Ids.begin(), it)] = keyVal.second;
				++count;
			}
		}
	}

	// If this fails, some groups in resulting vector are invalid.
	ASSERT(count == p_Ids.size());

	return cachedGroups;
}

vector<CachedOrgContact> GraphCache::GetCachedOrgContacts() const
{
	vector<CachedOrgContact> cachedOrgContact;

	std::lock_guard<std::mutex> lock(m_CachedOrgContactsLock);
	for (const auto& keyVal : m_CachedOrgContacts)
		cachedOrgContact.push_back(keyVal.second);

	return cachedOrgContact;
}

map<PooledString, CachedSubscribedSku> GraphCache::GetCachedSubscribedSkus() const
{
    std::lock_guard<std::mutex> lock(m_CachedSubscribedSkusLock);
    return m_CachedSubscribedSkus;
}

vector<CachedSite> GraphCache::GetCachedSites() const
{
    vector<CachedSite> cachedSites;

    std::lock_guard<std::mutex> lock(m_CachedSitesLock);
    for (const auto& keyVal : m_CachedSites)
        cachedSites.push_back(keyVal.second);

    return cachedSites;
}

vector<CachedUser> GraphCache::GetCachedDeletedUsers() const
{
	vector<CachedUser> cachedDeletedUsers;

	std::lock_guard<std::mutex> lock(m_CachedDeletedUsersLock);
	for (const auto& keyVal : m_CachedDeletedUsers)
		cachedDeletedUsers.push_back(keyVal.second);

	return cachedDeletedUsers;
}

vector<CachedGroup> GraphCache::GetCachedDeletedGroups() const
{
	vector<CachedGroup> cachedDeletedGroups;

	std::lock_guard<std::mutex> lock(m_CachedDeletedGroupsLock);
	for (const auto& keyVal : m_CachedDeletedGroups)
		cachedDeletedGroups.push_back(keyVal.second);

	return cachedDeletedGroups;
}

namespace
{
	template <class T>
	wstring GetCachedObjDisplayNameOrId(const T& p_Obj)
	{
		return p_Obj.GetID().IsEmpty() ? p_Obj.GetID() : p_Obj.GetDisplayName();
	}
}

wstring GraphCache::GetUserContextualInfo(const wstring& p_UserId) const
{
	wstring ctxInfo = p_UserId;

	std::lock_guard<std::mutex> lock(m_CachedUsersLock);
	auto itt = m_CachedUsers.find(p_UserId);
	if (itt != m_CachedUsers.end())
		ctxInfo = GetCachedObjDisplayNameOrId(itt->second);
	
	return ctxInfo;
}

wstring GraphCache::GetGroupContextualInfo(const wstring& p_GroupId) const
{
	wstring ctxInfo = p_GroupId;

	std::lock_guard<std::mutex> lock(m_CachedGroupsLock);
	auto itt = m_CachedGroups.find(p_GroupId);
	if (itt != m_CachedGroups.end())
		ctxInfo = GetCachedObjDisplayNameOrId(itt->second);

	return ctxInfo;
}

wstring GraphCache::GetSiteContextualInfo(const wstring& p_SiteId) const
{
	wstring ctxInfo = p_SiteId;

	std::lock_guard<std::mutex> lock(m_CachedSitesLock);
	auto itt = m_CachedSites.find(p_SiteId);
	if (itt != m_CachedSites.end())
		ctxInfo = GetCachedObjDisplayNameOrId(itt->second);

	return ctxInfo;
}

CachedUser GraphCache::GetUserInCache(const PooledString& p_Id, bool p_AllowFallbackToDeletedUser) const
{
	CachedUser user;

	std::lock_guard<std::mutex> lock(m_CachedUsersLock);
	auto userItt = m_CachedUsers.find(p_Id);
	if (userItt != std::end(m_CachedUsers))
		user = userItt->second;
	else if (p_AllowFallbackToDeletedUser)
		user = GetDeletedUserInCache(p_Id);

	return user;
}

CachedUser GraphCache::GetUserInCacheByEmail(const PooledString& p_Email, bool p_AllowFallbackToDeletedUser) const
{
	ASSERT(!p_Email.IsEmpty());

	CachedUser user;

	if (!p_Email.IsEmpty())
	{
		std::lock_guard<std::mutex> lock(m_CachedUsersLock);
		auto userItt = std::find_if(m_CachedUsers.begin(), m_CachedUsers.end(), [&p_Email](const std::pair<PooledString, CachedUser>& p_Item) {return p_Email == p_Item.second.GetDisplayName(); });
		if (userItt != std::end(m_CachedUsers))
			user = userItt->second;
		else if (p_AllowFallbackToDeletedUser)
			user = GetDeletedUserInCacheByEmail(p_Email);
	}

	return user;
}

CachedUser GraphCache::GetDeletedUserInCache(const PooledString& p_Id) const
{
	CachedUser deletedUser;

	std::lock_guard<std::mutex> lock(m_CachedDeletedUsersLock);
	auto userItt = m_CachedDeletedUsers.find(p_Id);
	if (userItt != std::end(m_CachedDeletedUsers))
		deletedUser = userItt->second;

	return deletedUser;
}

CachedUser GraphCache::GetDeletedUserInCacheByEmail(const PooledString& p_Email) const
{
	CachedUser deletedUser;

	std::lock_guard<std::mutex> lock(m_CachedDeletedUsersLock);
	auto userItt = std::find_if(m_CachedDeletedUsers.begin(), m_CachedDeletedUsers.end(), [&p_Email](const std::pair<PooledString, CachedUser>& p_Item) {return p_Email == p_Item.second.GetDisplayName(); });
	if (userItt != std::end(m_CachedDeletedUsers))
		deletedUser = userItt->second;

	return deletedUser;
}

CachedGroup	GraphCache::GetGroupInCache(const PooledString& p_Id) const
{
	CachedGroup group;

	std::lock_guard<std::mutex> lock(m_CachedGroupsLock);
	auto groupItt = m_CachedGroups.find(p_Id);
	if (groupItt != std::end(m_CachedGroups))
		group = groupItt->second;

	return group;
}

CachedGroup GraphCache::GetDeletedGroupInCache(const PooledString& p_Id) const
{
	CachedGroup deletedGroup;

	std::lock_guard<std::mutex> lock(m_CachedDeletedGroupsLock);
	auto userItt = m_CachedDeletedGroups.find(p_Id);
	if (userItt != std::end(m_CachedDeletedGroups))
		deletedGroup = userItt->second;

	return deletedGroup;
}

CachedSite GraphCache::GetSiteInCache(const PooledString& p_Id) const
{
	CachedSite site;

    std::lock_guard<std::mutex> lock(m_CachedSitesLock);
    auto siteItt = m_CachedSites.find(p_Id);
    if (siteItt != std::end(m_CachedSites))
        site = siteItt->second;

    return site;
}

BusinessOrgContact GraphCache::GetCachedOrgContact(const PooledString& p_Id) const
{
	BusinessOrgContact orgContact;

	std::lock_guard<std::mutex> lock(m_CachedOrgContactsLock);
	auto orgContactItt = m_CachedOrgContacts.find(p_Id);
	if (orgContactItt != std::end(m_CachedOrgContacts))
		orgContact = orgContactItt->second.ToBusinessOrgContact();

	return orgContact;
}

void GraphCache::SetSiteHierarchyDisplayName(const PooledString& p_Id, const PooledString& p_HierarchyDisplayName)
{
    std::lock_guard<std::mutex> lock(m_CachedSitesLock);
    auto siteItt = m_CachedSites.find(p_Id);
    if (siteItt != std::end(m_CachedSites))
    {
        m_CachedSites[p_Id].SetHierarchyDisplayName(p_HierarchyDisplayName);
    }
    else
        ASSERT(false);
}

wstring GraphCache::GetSiteHierarchyDisplayName(const PooledString& p_Id) const
{
    std::lock_guard<std::mutex> lock(m_CachedSitesLock);
    auto siteItt = m_CachedSites.find(p_Id);
    if (siteItt != std::end(m_CachedSites))
        return m_CachedSites.at(p_Id).GetHierarchyDisplayName();
    ASSERT(false);
    return _YTEXT("");
}

void GraphCache::Remove(const User& p_User)
{
	{
		std::lock_guard<std::mutex> lock(m_CachedUsersLock);
		m_CachedUsers.erase(p_User.m_Id);
	}

	if (m_SqlCache)
		m_SqlCache->DeleteUser(p_User.m_Id);
}

void GraphCache::Remove(const Group& p_Group)
{
	{
		std::lock_guard<std::mutex> lock(m_CachedGroupsLock);
		m_CachedGroups.erase(p_Group.m_Id);
	}

	if (m_SqlCache)
		m_SqlCache->DeleteGroup(p_Group.m_Id);
}

void GraphCache::RemoveDeletedUser(const User& p_DeletedUser)
{
	std::lock_guard<std::mutex> lock(m_CachedDeletedUsersLock);
	m_CachedDeletedUsers.erase(p_DeletedUser.m_Id);
}

void GraphCache::RemoveDeletedGroup(const Group& p_DeletedGroup)
{
	std::lock_guard<std::mutex> lock(m_CachedDeletedGroupsLock);
	m_CachedDeletedGroups.erase(p_DeletedGroup.m_Id);
}

void GraphCache::AddUsers(vector<BusinessUser>& p_Users, bool p_SaveInSqlCache/* = false*/, const std::vector<UBI>& p_Blocks/* = {}*/, const boost::YOpt<YTimeDate>& p_LastFullRefreshDate/* = boost::none*/, bool p_ShouldClearSql/* = false*/)
{
    std::map<PooledString, CachedUser> cachedUsers;
    {
        std::lock_guard<std::mutex> lock(m_CachedUsersLock);
        cachedUsers = m_CachedUsers;
    }

	if (p_SaveInSqlCache)
		SaveUsersInSqlCache(p_Users, p_Blocks, p_LastFullRefreshDate, p_ShouldClearSql);

    for (const auto& user : p_Users)
	{
		ASSERT(!p_SaveInSqlCache || !user.HasFlag(BusinessObject::SHOULDBEIGNORED)); // List should have been cleaned by SaveUsersInSqlCache
		if (!user.HasFlag(BusinessObject::SHOULDBEIGNORED))
		{
			if (user.HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
				cachedUsers.erase(user.m_Id);
			else if (!user.HasFlag(BusinessObject::CANCELED)) // Don't store canceled users
				cachedUsers[user.m_Id] = CachedUser(user);
		}
	}

    std::lock_guard<std::mutex> lock(m_CachedUsersLock);
    m_CachedUsers = std::move(cachedUsers);
}

void GraphCache::addUsersAndCountSkus(vector<BusinessUser>& p_Users, bool p_SaveInSqlCache/* = false*/, const std::vector<UBI>& p_Blocks/* = {}*/, const boost::YOpt<YTimeDate>& p_LastFullRefreshDate/* = boost::none*/, bool p_ShouldClearSql/* = false*/)
{
	std::map<PooledString, CachedUser> cachedUsers;
	{
		std::lock_guard<std::mutex> lock(m_CachedUsersLock);
		cachedUsers = m_CachedUsers;
	}

	if (p_SaveInSqlCache)
		SaveUsersInSqlCache(p_Users, p_Blocks, p_LastFullRefreshDate, p_ShouldClearSql);

	std::map<PooledString, int32_t> skuCounters;
	for (const auto& user : p_Users)
	{
		ASSERT(!p_SaveInSqlCache || !user.HasFlag(BusinessObject::SHOULDBEIGNORED)); // List should have been cleaned by SaveUsersInSqlCache
		if (!user.HasFlag(BusinessObject::SHOULDBEIGNORED))
		{
			if (user.HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
			{
				cachedUsers.erase(user.m_Id);
			}
			else if (!user.HasFlag(BusinessObject::CANCELED)) // Don't store canceled users
			{
				// FIXME: if this fails, we're trying to count licenses without having them...
				ASSERT(user.GetDataDate(UBI::ASSIGNEDLICENSES));

				if (user.IsRBACAuthorized() && user.GetAssignedLicenses())
				{
					for (auto& assLic : *user.GetAssignedLicenses())
						skuCounters[assLic.GetID()]++;
				}
				cachedUsers[user.m_Id] = CachedUser(user);
			}
		}
	}

	std::lock_guard<std::mutex> lock(m_CachedUsersLock);
	m_CachedUsers = std::move(cachedUsers);
	m_SkuCounters = std::move(skuCounters);
}

void GraphCache::AddGroups(vector<BusinessGroup>& p_Groups, bool p_SaveInSqlCache/* = false*/, const std::vector<GBI>& p_Blocks/* = {}*/, const boost::YOpt<YTimeDate>& p_LastFullRefreshDate/* = boost::none*/, bool p_ShouldClearSql/* = false*/)
{
    std::map<PooledString, CachedGroup> cachedGroups;
    {
        std::lock_guard<std::mutex> lock(m_CachedGroupsLock);
        cachedGroups = m_CachedGroups;
    }

	if (p_SaveInSqlCache)
		SaveGroupsInSqlCache(p_Groups, p_Blocks, p_LastFullRefreshDate, p_ShouldClearSql);

	for (const auto& group : p_Groups)
	{
		if (group.HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
			cachedGroups.erase(group.m_Id);
		else if (!group.HasFlag(BusinessObject::CANCELED)) // Don't store canceled groups
			cachedGroups[group.m_Id] = CachedGroup(group);
	}

    std::lock_guard<std::mutex> lock(m_CachedGroupsLock);
    m_CachedGroups = std::move(cachedGroups);
}

void GraphCache::AddSites(const vector<BusinessSite>& p_Sites)
{
	std::map<PooledString, CachedSite> cachedSites;
	{
		std::lock_guard<std::mutex> lock(m_CachedSitesLock);
		cachedSites = m_CachedSites;
	}

	for (const auto& site : p_Sites)
	{
		if (!site.GetID().IsEmpty() && !site.HasFlag(BusinessObject::CANCELED)) // Don't store canceled sites
			cachedSites[site.GetID()] = CachedSite(site);
	}

	std::lock_guard<std::mutex> lock(m_CachedSitesLock);
	m_CachedSites = std::move(cachedSites);
}

void GraphCache::AddUser(const BusinessUser& p_User, bool p_SaveInSqlCache/* = false*/, const std::vector<UBI>& p_Blocks/* = {}*/)
{
	ASSERT(!p_User.IsFromRecycleBin());
	ASSERT(!p_User.HasFlag(BusinessObject::CANCELED));
	if (!p_User.HasFlag(BusinessObject::CANCELED)) // Don't store canceled users
	{
		if (p_SaveInSqlCache)
		{
			std::vector<BusinessUser> users{ p_User };
			SaveUsersInSqlCache(users, p_Blocks, boost::none, false);
		}
		std::lock_guard<std::mutex> lock(m_CachedUsersLock);
		if (p_User.HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
		{
			auto itt = m_CachedUsers.find(p_User.m_Id);
			if (itt != m_CachedUsers.end())
				m_CachedUsers.erase(itt);
		}
		else
		{
			m_CachedUsers[p_User.m_Id] = CachedUser(p_User);
		}
	}
}

void GraphCache::AddGroup(const BusinessGroup& p_Group, bool p_SaveInSqlCache/* = false*/, const std::vector<GBI>& p_Blocks/* = {}*/)
{
	ASSERT(!p_Group.IsFromRecycleBin());
	ASSERT(!p_Group.HasFlag(BusinessObject::CANCELED));
	if (!p_Group.HasFlag(BusinessObject::CANCELED)) // Don't store canceled groups
	{
		if (p_SaveInSqlCache)
		{
			std::vector<BusinessGroup> groups{ p_Group };
			SaveGroupsInSqlCache(groups, p_Blocks, boost::none, false);
		}
		std::lock_guard<std::mutex> lock(m_CachedGroupsLock);
		if (p_Group.HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
		{
			auto itt = m_CachedGroups.find(p_Group.m_Id);
			if (itt != m_CachedGroups.end())
				m_CachedGroups.erase(itt);
		}
		else
		{
			m_CachedGroups[p_Group.m_Id] = CachedGroup(p_Group);
		}
	}
}

void GraphCache::Add(const BusinessOrgContact& p_OrgContact)
{
	ASSERT(!p_OrgContact.HasFlag(BusinessObject::CANCELED));
	if (!p_OrgContact.HasFlag(BusinessObject::CANCELED)) // Don't store canceled orgContacts
	{
		std::lock_guard<std::mutex> lock(m_CachedOrgContactsLock);
		m_CachedOrgContacts[p_OrgContact.m_Id] = CachedOrgContact(p_OrgContact);
	}
}

void GraphCache::Add(const vector<BusinessOrgContact>& p_OrgContacts)
{
    std::map<PooledString, CachedOrgContact> cachedOrgContacts;
    {
        std::lock_guard<std::mutex> lock(m_CachedOrgContactsLock);
        cachedOrgContacts = m_CachedOrgContacts;
    }

    for (const auto& orgContact : p_OrgContacts)
    {
        if (!orgContact.GetID().IsEmpty() && !orgContact.HasFlag(BusinessObject::CANCELED)) // Don't store canceled orgContacts
            cachedOrgContacts[orgContact.GetID()] = CachedOrgContact(orgContact);
    }

    std::lock_guard<std::mutex> lock(m_CachedOrgContactsLock);
    m_CachedOrgContacts = std::move(cachedOrgContacts);
}

void GraphCache::Add(const vector<BusinessSubscribedSku>& p_SubscribedSkus)
{
    std::map<PooledString, CachedSubscribedSku> cachedSubscribedSkus;
    {
        std::lock_guard<std::mutex> lock(m_CachedSubscribedSkusLock);
        cachedSubscribedSkus = m_CachedSubscribedSkus;
    }

    for (const auto& subscribedSku : p_SubscribedSkus)
    {
        if (subscribedSku.GetSkuId() != boost::none && !subscribedSku.GetSkuId()->IsEmpty())
            cachedSubscribedSkus[*subscribedSku.GetSkuId()] = CachedSubscribedSku(subscribedSku);
    }

    std::lock_guard<std::mutex> lock(m_CachedSubscribedSkusLock);
    m_CachedSubscribedSkus = std::move(cachedSubscribedSkus);
}

void GraphCache::AddSite(const BusinessSite& p_Site)
{
	ASSERT(!p_Site.HasFlag(BusinessObject::CANCELED));
	if (!p_Site.HasFlag(BusinessObject::CANCELED)) // Don't store canceled sites
	{
		std::lock_guard<std::mutex> lock(m_CachedGroupsLock);
		m_CachedSites[p_Site.m_Id] = CachedSite(p_Site);
	}
}

web::json::value GraphCache::mergeBodies(const std::map<wstring, wstring>& p_Bodies)
{
	web::json::value obj;

	for (const auto& b : p_Bodies)
	{
		ASSERT(!b.second.empty());
		if (!b.second.empty())
		{
			const auto val = json::value::parse(b.second);
			ASSERT(val.is_object());
			if (val.is_object())
			{
				if (obj.is_null())
				{
					obj = val;
				}
				else
				{
					auto& existingObj = obj.as_object();
					auto& newObj = val.as_object();
					for (const auto& v : newObj)
					{
						ASSERT(existingObj.end() == existingObj.find(v.first));
						existingObj[v.first] = std::move(v.second);
					}
				}
			}
		}
	}

	return obj;
}

void GraphCache::AddDeletedUsers(const vector<BusinessUser>& p_DeletedUsers)
{
	std::map<PooledString, CachedUser> cachedDeletedUsers;
	{
		std::lock_guard<std::mutex> lock(m_CachedDeletedUsersLock);
		cachedDeletedUsers = m_CachedDeletedUsers;
	}

	for (const auto& delUser : p_DeletedUsers)
	{
		ASSERT(delUser.IsFromRecycleBin());
		if (!delUser.HasFlag(BusinessObject::CANCELED)) // Don't store canceled deleted users
			cachedDeletedUsers[delUser.m_Id] = CachedUser(delUser);
	}

	std::lock_guard<std::mutex> lock(m_CachedDeletedUsersLock);
	m_CachedDeletedUsers = std::move(cachedDeletedUsers);
}

void GraphCache::AddDeletedGroups(const vector<BusinessGroup>& p_DeletedGroups)
{
	std::map<PooledString, CachedGroup> cachedDeletedGroups;
	{
		std::lock_guard<std::mutex> lock(m_CachedDeletedGroupsLock);
		cachedDeletedGroups = m_CachedDeletedGroups;
	}

	for (const auto& delGroup : p_DeletedGroups)
	{
		ASSERT(delGroup.IsFromRecycleBin());
		if (!delGroup.HasFlag(BusinessObject::CANCELED)) // Don't store canceled deleted groups
			cachedDeletedGroups[delGroup.m_Id] = CachedGroup(delGroup);
	}

	std::lock_guard<std::mutex> lock(m_CachedDeletedGroupsLock);
	m_CachedDeletedGroups = std::move(cachedDeletedGroups);
}

BusinessUser& GraphCache::SyncUncachedUser(BusinessUser& p_User, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData, bool p_AllowFallbackToDeletedUser)
{
	const bool useEmail = p_User.m_Id.IsEmpty() && p_User.GetUserPrincipalName();
	ASSERT(!(useEmail ? *p_User.GetUserPrincipalName() : p_User.m_Id).IsEmpty());

	const auto cachedUser = useEmail
		? GetUserInCacheByEmail(*p_User.GetUserPrincipalName(), p_AllowFallbackToDeletedUser)
		: GetUserInCache(p_User.m_Id, p_AllowFallbackToDeletedUser);
    if (cachedUser.GetID().IsEmpty())
    {
		RunOnScopeEnd _([objName = p_Logger->GetLogMessage(), p_Logger]() { p_Logger->SetLogMessage(objName); });
		p_Logger->SetLogMessage(_T("user"));

		unique_ptr<UserRequester> userSync = useEmail
			? std::make_unique<UserRequesterByEmail>(*p_User.GetUserPrincipalName(), CachedUserPropertySet(), false, p_Logger)
			: std::make_unique<UserRequester>(p_User.m_Id, CachedUserPropertySet(), false, p_Logger);
		userSync->SetRbacSessionMode(Sapio365Session::USER_SESSION);
		safeTaskCall(userSync->Send(getSapio365Session(), p_TaskData)
			, getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION)
			, [&userSync](const std::exception_ptr& p_ExceptPtr)
		{
			ASSERT(p_ExceptPtr);
			if (p_ExceptPtr)
			{
				try
				{
					std::rethrow_exception(p_ExceptPtr);
				}
				catch (const RestException& e)
				{
					userSync->GetData().SetError(HttpError(e));
				}
				catch (const std::exception& e)
				{
					userSync->GetData().SetError(SapioError(e));
				}
			}
		}, YtriaTaskData()).GetTask().wait();

		if (!p_TaskData.IsCanceled())
		{
			BusinessUser& user = userSync->GetData();
			ASSERT(!user.m_Id.IsEmpty());
			if (!user.m_Id.IsEmpty())
			{
				ASSERT(user.GetUserType() || user.GetError());

				// To remove the @ and crap after inside the id
				if (useEmail)
				{
					const wstring id = user.m_Id;
					auto atPos = id.find(_YTEXT("@"));
					if (atPos != std::wstring::npos)
						user.m_Id = std::wstring(std::begin(id), std::begin(id) + atPos);
				}
				else
				{
					user.m_Id = p_User.m_Id; // Original id is clean
				}

				if (!user.GetError()) // Don't store user in cache if sync failed.
					updateUser(user);
				else if (p_AllowFallbackToDeletedUser)
				{
					SyncAllDeletedUsersOnce(p_TaskData).GetTask().wait();

					auto deletedUser = GetDeletedUserInCache(p_User.m_Id);
					if (!deletedUser.GetID().IsEmpty())
						user.SetValuesFrom(deletedUser);
				}
			}
			p_User = std::move(user);
		}
    }
	else
	{
		p_User.SetValuesFrom(cachedUser);
	}

	return p_User;
}

BusinessGroup& GraphCache::SyncUncachedGroup(BusinessGroup& p_Group, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData, bool p_AllowOverrideLoggerMessage/* = true*/)
{
	const auto cachedGroup = GetGroupInCache(p_Group.m_Id);
    if (cachedGroup.GetID().IsEmpty())
    {
		RunOnScopeEnd _([objName = p_Logger->GetLogMessage(), p_Logger]() { p_Logger->SetLogMessage(objName); });
		if (p_AllowOverrideLoggerMessage)
			p_Logger->SetLogMessage(_T("group"));

        GroupRequester groupSync(p_Group.m_Id, CachedGroupPropertySet(), p_Logger);
        safeTaskCall(groupSync.Send(getSapio365Session(), p_TaskData)
			, getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION)
			, [&groupSync](const std::exception_ptr& p_ExceptPtr)
		{
			ASSERT(p_ExceptPtr);
			if (p_ExceptPtr)
			{
				try
				{
					std::rethrow_exception(p_ExceptPtr);
				}
				catch (const RestException& e)
				{
					groupSync.GetData().SetError(HttpError(e));
				}
				catch (const std::exception& e)
				{
					groupSync.GetData().SetError(SapioError(e));
				}
			}
		}, YtriaTaskData()
		).GetTask().wait();

		if (!p_TaskData.IsCanceled())
		{
			BusinessGroup& group = groupSync.GetData();
			ASSERT(!group.m_Id.IsEmpty());
			if (!group.m_Id.IsEmpty())
			{
				group.m_Id = p_Group.m_Id; // To remove the @ and crap after inside the id
				if (!group.GetError()) // Don't store group in cache if sync failed
					updateGroup(group);
			}
			p_Group = std::move(group);
		}
    }
	else
	{
		p_Group.SetValuesFrom(cachedGroup);
	}

	return p_Group;
}

BusinessOrgContact& GraphCache::SyncUncachedOrgContact(BusinessOrgContact& p_OrgContact, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	const auto cachedOrgContact = GetCachedOrgContact(p_OrgContact.m_Id);
	if (cachedOrgContact.m_Id.IsEmpty())
	{
		RunOnScopeEnd _([objName = p_Logger->GetLogMessage(), p_Logger]() { p_Logger->SetLogMessage(objName); });
		p_Logger->SetLogMessage(_T("contact"));
		OrgContactRequester request(p_OrgContact.GetID(), CachedOrgContactPropertySet(), p_Logger);
		boost::YOpt<SapioError> error;
		safeTaskCall(request.Send(getSapio365Session(), p_TaskData)
			, getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION)
			, [&error](const std::exception_ptr& p_ExceptPtr)
		{
			ASSERT(p_ExceptPtr);
			if (p_ExceptPtr)
			{
				try
				{
					std::rethrow_exception(p_ExceptPtr);
				}
				catch (const RestException& e)
				{
					error = HttpError(e);
				}
				catch (const std::exception& e)
				{
					error = SapioError(e);
				}
			}
		}, YtriaTaskData()).GetTask().wait();


		if (!p_TaskData.IsCanceled())
		{
			auto orgContact = request.GetData();
			ASSERT(orgContact.m_Id != _YTEXT(""));
			if (orgContact.m_Id != _YTEXT(""))
			{
				orgContact.m_Id = p_OrgContact.m_Id; // To remove the @ and crap after inside the id
				if (!error) // Don't store org contact in cache if sync failed.
					updateOrgContact(orgContact);
				p_OrgContact.SetError(error);
				p_OrgContact = orgContact;
			}
		}
	}
	else
	{
		p_OrgContact = cachedOrgContact;
	}

	return p_OrgContact;
}

BusinessSite& GraphCache::SyncUncachedSite(BusinessSite& p_Site, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	const auto cachedSite = GetSiteInCache(p_Site.m_Id);
    if (cachedSite.GetID().IsEmpty())
    {
        Site site;
        site.Id = p_Site.m_Id;

        std::vector<rttr::property> cacheProperties;

        auto idProp = rttr::type::get<Site>().get_property("id");
        auto displayNameProp = rttr::type::get<Site>().get_property("displayName");
        auto nameProp = rttr::type::get<Site>().get_property("name");
        auto webUrlProp = rttr::type::get<Site>().get_property("webUrl");

        ASSERT(idProp.is_valid() && displayNameProp.is_valid() && nameProp.is_valid() && webUrlProp.is_valid());
        if (idProp.is_valid() && displayNameProp.is_valid() && nameProp.is_valid() && webUrlProp.is_valid())
        {
            cacheProperties.push_back(idProp);
            cacheProperties.push_back(displayNameProp);
            cacheProperties.push_back(nameProp);
            cacheProperties.push_back(webUrlProp);
        }

		RunOnScopeEnd _([objName = p_Logger->GetLogMessage(), p_Logger]() { p_Logger->SetLogMessage(objName); });
		p_Logger->SetLogMessage(_T("site"));

		boost::YOpt<SapioError> error;

		auto siteRequester = std::make_shared<SiteRequester>(site.Id, p_Logger);
        safeTaskCall(siteRequester->Send(getSapio365Session(), p_TaskData)
			, getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION)
			, [&error](const std::exception_ptr& p_ExceptPtr)
		{
			ASSERT(p_ExceptPtr);
			if (p_ExceptPtr)
			{
				try
				{
					std::rethrow_exception(p_ExceptPtr);
				}
				catch (const RestException& e)
				{
					error = HttpError(e);
				}
				catch (const std::exception& e)
				{
					error = SapioError(e);
				}
			}
		}, YtriaTaskData()).GetTask().wait();

		wstring goodSiteId = p_Site.GetID(); // Without the @ and crap after inside the id
		p_Site = siteRequester->GetData();

		if (!p_TaskData.IsCanceled())
		{
			ASSERT(!p_Site.GetID().IsEmpty());
			if (!p_Site.GetID().IsEmpty())
			{
				p_Site.SetID(goodSiteId); // To remove the @ and crap after inside the id
				if (!error) // Don't store site in cache if sync failed.
					updateSite(p_Site);
				p_Site.SetError(error);
			}
		}
    }
	else
	{
		p_Site.SetValuesFrom(cachedSite);
	}

	return p_Site;
}

void GraphCache::SyncUncachedUsers(vector<BusinessUser>& p_Users, YtriaTaskData p_TaskData, bool p_AllowFallbackToDeletedUser)
{
	auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("users"), p_Users.size());
	for (auto& currentUser : p_Users)
	{
		logger->IncrementObjCount();
        SyncUncachedUser(currentUser, logger, p_TaskData, p_AllowFallbackToDeletedUser);
	}
}

namespace
{
	class UserPropertySetWithLicenseIfNeeded : public IPropertySetBuilder
	{
	public:
		UserPropertySetWithLicenseIfNeeded(const IPropertySetBuilder& p_PropertySet, bool p_AddAssignedLicense)
			: m_PropertySet(p_PropertySet)
			, m_AddAssignedLicense(p_AddAssignedLicense)
		{

		}

		virtual vector<rttr::property> GetPropertySet() const override
		{
			auto propSet = m_PropertySet.GetPropertySet();

			if (m_AddAssignedLicense)
			{
				// just add O365_USER_ASSIGNEDLICENSES if necessary
				const auto prop = rttr::type::get<User>().get_property(O365_USER_ASSIGNEDLICENSES);
				if (propSet.end() == std::find(propSet.begin(), propSet.end(), prop))
					propSet.push_back(prop);
			}

			return propSet;
		}

	private:
		const IPropertySetBuilder& m_PropertySet;
		bool m_AddAssignedLicense;
	};
}

TaskWrapper<std::vector<BusinessUser>> GraphCache::SyncUsersFromDeltaLink(YtriaTaskData p_TaskData, const std::shared_ptr<IPageRequestLogger>& p_Logger, std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/, bool p_IsAllUsers/* = true*/)
{
	ASSERT(!p_OverrideDeltaLink || !p_OverrideDeltaLink->empty());
	if (p_IsAllUsers)
	{
		UpdateUsersDeltaLinkFromSql();
		ASSERT(m_UsersDeltaLink);
	}

	if (p_IsAllUsers && p_OverrideDeltaLink)
		m_UsersDeltaLink = p_OverrideDeltaLink;

	auto deltaRequester = std::make_unique<UserListRequester::DeltaLinkRequester>(p_OverrideDeltaLink ? *p_OverrideDeltaLink : *m_UsersDeltaLink);
	UserListRequester::DeltaLinkRequester& deltaReqRef = *deltaRequester.get();
	auto requester = std::make_shared<UserListRequester>(p_Logger, std::move(deltaRequester));
	return safeTaskCall(requester->Send(getSapio365Session(), p_TaskData).Then([this, &deltaReqRef, usersDeltaLink = m_UsersDeltaLink, p_OverrideDeltaLink, p_IsAllUsers, sqlCache2 = m_SqlCache, requester, p_TaskData]() {
		const bool canceled = p_TaskData.IsCanceled();
		auto& users = requester->GetData();

		boost::YOpt<YTimeDate> refreshDate;
		if (!canceled && p_IsAllUsers)
		{
			if (!users.empty())
				refreshDate = users.front().GetLastRequestTime();
			else
				refreshDate = p_TaskData.GetLastRequestOn();
		}

		// Sync is always list level.
		AddUsers(users, true, { UBI::MIN, UBI::LIST, UBI::ASSIGNEDLICENSES }, refreshDate, false);

		if (p_OverrideDeltaLink)
			*p_OverrideDeltaLink = deltaReqRef.GetNextDeltalink();
	
		if (!canceled && p_IsAllUsers)
		{
			setAllUsersSynced();

			if (sqlCache2)
			{
				*usersDeltaLink = deltaReqRef.GetNextDeltalink();
				saveUsersDeltaLink(*usersDeltaLink, sqlCache2);
			}
		}
		return users;
	}), getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessUser>, p_TaskData);
}

TaskWrapper<std::vector<BusinessUser>> GraphCache::GetPrefilteredUsers(YtriaTaskData p_TaskData, const ODataFilter& p_Filter, const std::shared_ptr<IPageRequestLogger>& p_Logger)
{
	ASSERT(!p_Filter.IsEmpty());
	auto subReq = std::make_unique<UserListRequester::RegularRequester>();
	subReq->SetProperties(UserListFullPropertySet().GetPropertySet());
	auto requester = std::make_shared<UserListRequester>(p_Logger, std::move(subReq));
	requester->SetCustomFilter(p_Filter);
	return safeTaskCall(requester->Send(getSapio365Session(), p_TaskData).Then([this, requester, p_TaskData]() {
		auto& users = requester->GetData();
		AddUsers(users, true, { UBI::MIN, UBI::LIST, UBI::ASSIGNEDLICENSES }, boost::none, false);
		return users;
		}), getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessUser>, p_TaskData);
}

TaskWrapper<std::vector<BusinessGroup>> GraphCache::SyncGroupsFromDeltaLink(YtriaTaskData p_TaskData, const std::shared_ptr<IPageRequestLogger>& p_Logger, std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/, bool p_IsAllGroups/* = true*/)
{
	ASSERT(!p_OverrideDeltaLink || !p_OverrideDeltaLink->empty());
	if (p_IsAllGroups)
	{
		UpdateGroupsDeltaLinkFromSql();
		ASSERT(m_GroupsDeltaLink);
	}

	if (p_IsAllGroups && p_OverrideDeltaLink)
		m_GroupsDeltaLink = p_OverrideDeltaLink;

	auto deltaRequester = std::make_unique<GroupListRequester::DeltaLinkRequester>(p_OverrideDeltaLink ? *p_OverrideDeltaLink : *m_GroupsDeltaLink);
	GroupListRequester::DeltaLinkRequester& deltaReqRef = *deltaRequester.get();
	auto requester = std::make_shared<GroupListRequester>(p_Logger, std::move(deltaRequester));
	return safeTaskCall(requester->Send(getSapio365Session(), p_TaskData).Then([this, &deltaReqRef, groupsDeltaLink = m_GroupsDeltaLink, p_OverrideDeltaLink, p_IsAllGroups, sqlCache2 = m_SqlCache, requester, p_TaskData]() {
		const bool canceled = p_TaskData.IsCanceled();
		auto& groups = requester->GetData();

		boost::YOpt<YTimeDate> refreshDate;
		if (!canceled && p_IsAllGroups)
		{
			if (!groups.empty())
				refreshDate = groups.front().GetLastRequestTime();
			else
				refreshDate = p_TaskData.GetLastRequestOn();
		}
		
		// Sync is always list level.
		AddGroups(groups, true, { GBI::MIN, GBI::LIST }, refreshDate, false);
		
		if (p_OverrideDeltaLink)
			*p_OverrideDeltaLink = deltaReqRef.GetNextDeltalink();

		if (!canceled && p_IsAllGroups)
		{
			setAllGroupsSynced();
			
			if (sqlCache2)
			{
				saveGroupsDeltaLink(deltaReqRef.GetNextDeltalink(), sqlCache2);
				*groupsDeltaLink = deltaReqRef.GetNextDeltalink();
			}
		}
		return groups;
		}), getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessGroup>, p_TaskData);
}

TaskWrapper<std::vector<BusinessGroup>> GraphCache::GetPrefilteredGroups(YtriaTaskData p_TaskData, const ODataFilter& p_Filter, const std::shared_ptr<IPageRequestLogger>& p_Logger)
{
	ASSERT(!p_Filter.IsEmpty());
	auto subReq = std::make_unique<GroupListRequester::RegularRequester>();
	subReq->SetProperties(GroupListFullPropertySet().GetPropertySet());
	auto requester = std::make_shared<GroupListRequester>(p_Logger, std::move(subReq));
	requester->SetCustomFilter(p_Filter);
	return safeTaskCall(requester->Send(getSapio365Session(), p_TaskData).Then([this, requester, p_TaskData]() {
		auto& groups = requester->GetData();
		AddGroups(groups, true, { GBI::MIN, GBI::LIST }, boost::none, false);
		return groups;
		}), getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessGroup>, p_TaskData);
}

TaskWrapper<std::vector<CachedOrgContact>> GraphCache::SyncAllOrgContacts(const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	auto requester = std::make_shared<OrgContactListRequester>(CachedOrgContactPropertySet(), p_Logger);
	return safeTaskCall(requester->Send(getSapio365Session(), p_TaskData), getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData).Then([this, requester]() {
		ClearOrgContacts();
		Add(requester->GetData());
		SetAllOrgContactsSynced();
		return GetCachedOrgContacts();
	}, p_TaskData.GetToken());
}

TaskWrapper<std::vector<CachedUser>> GraphCache::SyncAllDeletedUsers(YtriaTaskData p_TaskData)
{
	auto logger = std::make_shared<BasicPageRequestLogger>(_T("deleted users"));
	auto requester = std::make_shared<DeletedUserListRequester>(CachedDeletedUserPropertySet(), logger);
	return safeTaskCall(requester->Send(getSapio365Session(), p_TaskData), getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData).Then([this, requester]() {
		ClearDeletedUsers();
		AddDeletedUsers(requester->GetData());
		SetAllDeletedUsersSynced();
		return GetCachedDeletedUsers();
	}, p_TaskData.GetToken());
}

TaskWrapper<std::vector<CachedGroup>> GraphCache::SyncAllDeletedGroups(YtriaTaskData p_TaskData)
{
	auto logger = std::make_shared<BasicPageRequestLogger>(_T("deleted groups"));
	auto requester = std::make_shared<DeletedGroupListRequester>(CachedDeletedGroupPropertySet(), logger);
	return safeTaskCall(requester->Send(getSapio365Session(), p_TaskData), getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData).Then([this, requester]() {
		ClearDeletedGroups();
		AddDeletedGroups(requester->GetData());
		SetAllDeletedGroupsSynced();
		return GetCachedDeletedGroups();
	}, p_TaskData.GetToken());
}

TaskWrapper<std::vector<CachedOrgContact>> GraphCache::SyncAllOrgContactsOnce(const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	if (!m_SyncedAllOrgContacts)
		return SyncAllOrgContacts(p_Logger, p_TaskData);
	return pplx::task_from_result(GetCachedOrgContacts());
}

TaskWrapper<std::vector<CachedUser>> GraphCache::SyncAllDeletedUsersOnce(YtriaTaskData p_TaskData)
{
	if (!m_SyncedAllDeletedUsers)
		return SyncAllDeletedUsers(p_TaskData);
	return pplx::task_from_result(GetCachedDeletedUsers());
}

TaskWrapper<std::vector<CachedGroup>> GraphCache::SyncAllDeletedGroupsOnce(YtriaTaskData p_TaskData)
{
	if (!m_SyncedAllDeletedGroups)
		return SyncAllDeletedGroups(p_TaskData);
	return pplx::task_from_result(GetCachedDeletedGroups());
}

void GraphCache::UpdateUsersDeltaLinkFromSql()
{
	ASSERT(m_SqlCache && m_UsersDeltaLink);
	if (m_SqlCache && m_UsersDeltaLink)
	{
		auto metadata = m_SqlCache->LoadUsersMetadata();
		*m_UsersDeltaLink = metadata.m_LastDelta ? *metadata.m_LastDelta : wstring();
	}
}

void GraphCache::UpdateGroupsDeltaLinkFromSql()
{
	ASSERT(m_SqlCache && m_GroupsDeltaLink);
	if (m_SqlCache && m_GroupsDeltaLink)
	{
		auto metadata = m_SqlCache->LoadGroupsMetadata();
		*m_GroupsDeltaLink = metadata.m_LastDelta ? *metadata.m_LastDelta : wstring();
	}
}

std::shared_ptr<wstring> GraphCache::GetUsersDeltaLink() const
{
	return m_UsersDeltaLink;
}

std::shared_ptr<wstring> GraphCache::GetGroupsDeltaLink() const
{
	return m_GroupsDeltaLink;
}

void GraphCache::ClearUsersDeltaLink()
{
	ASSERT(m_SqlCache && m_UsersDeltaLink);
	if (m_SqlCache && m_UsersDeltaLink)
	{
		*m_UsersDeltaLink = _YTEXT("");
		saveUsersDeltaLink(*m_UsersDeltaLink, m_SqlCache);
	}
}

void GraphCache::ClearGroupsDeltaLink()
{
	ASSERT(m_SqlCache && m_GroupsDeltaLink);
	if (m_SqlCache && m_GroupsDeltaLink)
	{
		*m_GroupsDeltaLink = _YTEXT("");
		saveGroupsDeltaLink(*m_GroupsDeltaLink, m_SqlCache);
	}
}

void GraphCache::setAllGroupsSynced()
{
    m_SyncedAllGroups = true;
}

void GraphCache::SetAllOrgContactsSynced()
{
	m_SyncedAllOrgContacts = true;
}

void GraphCache::SetAllDeletedUsersSynced()
{
	m_SyncedAllDeletedUsers = true;
}

void GraphCache::SetAllDeletedGroupsSynced()
{
	m_SyncedAllDeletedGroups = true;
}

void GraphCache::SaveUsersInSqlCache(vector<BusinessUser>& p_Objs, const std::vector<UBI>& p_Blocks, const boost::YOpt<YTimeDate>& p_LastFullRefreshDate, bool p_ShouldClearSql)
{
	ASSERT(!p_Blocks.empty());
    if (!p_Blocks.empty())
    {
        m_SqlCache->StartTransaction();

		if (p_ShouldClearSql)
		{
			ASSERT(p_LastFullRefreshDate);
			m_SqlCache->ClearUsers();
		}

		for(auto it = p_Objs.begin(); p_Objs.end() != it;)
        {
			auto& user = *it;

			if (user.HasFlag(BusinessObject::SHOULDBEIGNORED))
			{
				it = p_Objs.erase(it);
				continue;
			}

			if (user.HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
			{
				m_SqlCache->DeleteUser(user.GetID());
			}
			else
			{
				// Can't be canceled if saving a full list
				ASSERT(!user.HasFlag(BusinessObject::CANCELED) || !p_LastFullRefreshDate);
				if (!user.HasFlag(BusinessObject::CANCELED)) // Don't store canceled users
				{
					std::map<wstring, wstring> body;
					for (const auto& b : p_Blocks)
					{
						// I couldn't find a place to set this date without looping on the whole vector.
						// This should be the only blocks that don't always have a date.
						if ((UBI::MIN == b || UBI::LIST == b || UBI::ASSIGNEDLICENSES == b) && !user.GetDataDate(b))
							user.SetDataDate(b, user.GetLastRequestTime());
						ASSERT(UBI::MIN != b && UBI::LIST != b && UBI::ASSIGNEDLICENSES != b || user.GetDataDate(b)); // We must have a date for any block that's gonna be saved.
						// if there's no date, it means no data for the block. Don't save.
						if (user.GetDataDate(b))
						{
							DbUserSerializer serializer(user, b);
							serializer.Serialize();
							body[SqlCacheObjectSpecific<UserCache>::DataBlockName(b)] = serializer.GetJson().serialize();
						}
					}
					m_SqlCache->SaveUser(user.GetID(), body);
				}
			}

			++it;
        }

		if (p_LastFullRefreshDate)
		{
			auto metadata = m_SqlCache->LoadUsersMetadata();
			metadata.m_Version = boost::none;
			metadata.m_FullListSize = static_cast<int>(p_Objs.size());
			metadata.m_FullRefreshDate = *p_LastFullRefreshDate;
			m_SqlCache->SaveUsersMetadata(metadata);
		}

        m_SqlCache->EndTransaction();
    }
}

void GraphCache::SaveGroupsInSqlCache(vector<BusinessGroup>& p_Objs, const std::vector<GBI>& p_Blocks, const boost::YOpt<YTimeDate>& p_LastFullRefreshDate, bool p_ShouldClearSql)
{
	ASSERT(!p_Blocks.empty());
	if (!p_Blocks.empty())
	{
        m_SqlCache->StartTransaction();

		if (p_ShouldClearSql)
		{
			ASSERT(p_LastFullRefreshDate);
			m_SqlCache->ClearGroups();
		}

        for (auto& group : p_Objs)
        {
			if (group.HasFlag(BusinessObject::REMOVEDBYDELTASYNC))
			{
				m_SqlCache->DeleteGroup(group.GetID());
			}
			else
			{
				// Can't be canceled if saving a full list
				ASSERT(!group.HasFlag(BusinessObject::CANCELED) || !p_LastFullRefreshDate);
				if (!group.HasFlag(BusinessObject::CANCELED)) // Don't store canceled groups
				{
					std::map<wstring, wstring> body;
					for (const auto& b : p_Blocks)
					{
						// I couldn't find a place to set this date without looping on the whole vector.
						// This should be the only blocks that don't always have a date.
						if ((GBI::MIN == b || GBI::LIST == b) && !group.GetDataDate(b))
							group.SetDataDate(b, group.GetLastRequestTime());
						ASSERT(GBI::MIN != b && GBI::LIST != b || group.GetDataDate(b)); // We must have a date for any block that's gonna be saved.
						// if there's no date, it means no data for the block. Don't save.
						if (group.GetDataDate(b))
						{
							DbGroupSerializer serializer(group, b);
							serializer.Serialize();
							body[SqlCacheObjectSpecific<GroupCache>::DataBlockName(b)] = serializer.GetJson().serialize();
						}
					}
					m_SqlCache->SaveGroup(group.GetID(), body);
				}
			}
        }

		if (p_LastFullRefreshDate)
		{
			auto metadata = m_SqlCache->LoadGroupsMetadata();
			metadata.m_Version = boost::none;
			metadata.m_FullListSize = static_cast<int>(p_Objs.size());
			metadata.m_FullRefreshDate = *p_LastFullRefreshDate;
			m_SqlCache->SaveGroupsMetadata(metadata);
		}

        m_SqlCache->EndTransaction();
    }
}

void GraphCache::setSkuCounters(std::map<PooledString, int32_t>&& p_SkuCunters)
{
	std::lock_guard<std::mutex> lock(m_CachedUsersLock);
	m_SkuCounters = std::move(p_SkuCunters);
}

void GraphCache::InitSqlCache()
{
	std::shared_ptr<Sapio365Session> session = getSapio365Session();
	ASSERT(session && !session->GetIdentifier().m_EmailOrAppId.empty());
	if (session)
	{
		if (nullptr == m_SqlCache || !m_SqlCache->IsForSession(session->GetIdentifier()))
		{
			m_SqlCache = std::make_unique<CachedObjectsSqlEngine>(session->GetIdentifier());
			m_SqlCache->Init(SqlCacheConfig<UserCache>::DataBlockNames(), SqlCacheConfig<GroupCache>::DataBlockNames());
		}
	}
}

void GraphCache::saveUsersDeltaLink(const wstring& p_DeltaLink, const std::shared_ptr<CachedObjectsSqlEngine>& p_SqlCache2)
{
	ASSERT(nullptr != p_SqlCache2);
	if (p_SqlCache2)
	{
		auto metadata = p_SqlCache2->LoadUsersMetadata();
		metadata.m_Version = boost::none;
		metadata.m_LastDelta = p_DeltaLink;
		p_SqlCache2->SaveUsersMetadata(metadata);
	}
}

void GraphCache::saveGroupsDeltaLink(const wstring& p_DeltaLink, const std::shared_ptr<CachedObjectsSqlEngine>& p_SqlCache2)
{
	ASSERT(nullptr != p_SqlCache2);
	if (p_SqlCache2)
	{
		auto metadata = p_SqlCache2->LoadGroupsMetadata();
		metadata.m_Version = boost::none;
		metadata.m_LastDelta = p_DeltaLink;
		p_SqlCache2->SaveGroupsMetadata(metadata);
	}
}

void GraphCache::setAllUsersSynced()
{
    m_SyncedAllUsers = true;
	m_UserCount = static_cast<int32_t>(GetUserCacheSize());
}

bool GraphCache::IsAllUsersSynced()
{
	return m_SyncedAllUsers;
}

bool GraphCache::IsAllGroupsSynced()
{
	return m_SyncedAllGroups;
}

bool GraphCache::IsAllOrgContactsSynced()
{
	return m_SyncedAllOrgContacts;
}

bool GraphCache::IsAllDeletedUsersSynced()
{
	return m_SyncedAllDeletedUsers;
}

bool GraphCache::IsAllDeletedGroupsSynced()
{
	return m_SyncedAllDeletedGroups;
}

const boost::YOpt<vector<uint8_t>>& GraphCache::GetCachedProfilePicture() const
{
	return m_ConnectedUserProfilePicture;
}

const boost::YOpt<BusinessUser>& GraphCache::GetCachedConnectedUser() const
{
	return m_ConnectedUser;
}

GraphCache::GraphCache(std::shared_ptr<Sapio365Session> p_Sapio365Session)
	: m_Sapio365Session(p_Sapio365Session)
	, m_UsersDeltaLink(std::make_shared<wstring>())
	, m_GroupsDeltaLink(std::make_shared<wstring>())
	, m_HybridCheckSuccess(false)
{
	Clear();
}

void GraphCache::SetPersistentSession(const PersistentSession& p_PersistentSession)
{
	ASSERT(p_PersistentSession.IsValid() && p_PersistentSession.GetIdentifier() == getSapio365Session()->GetIdentifier());
	m_PersistentSession = p_PersistentSession;
}

bool GraphCache::HasUsersDeltaCapabilities() const
{
	return m_PersistentSession.IsValid() && m_PersistentSession.UseDeltaUsers();
}

bool GraphCache::HasGroupsDeltaCapabilities() const
{
	return m_PersistentSession.IsValid() && m_PersistentSession.UseDeltaGroups();
}

void GraphCache::InitUserCount(int32_t p_Count)
{
	ASSERT(p_Count >= 0);
	m_UserCount = p_Count;
}

int32_t GraphCache::GetUserCount() const
{
	return m_UserCount > 0 ? m_UserCount : static_cast<int32_t>(GetUserCacheSize());
}

void GraphCache::updateUser(const User& p_User)
{
    std::lock_guard<std::mutex> lock(m_CachedUsersLock);
    m_CachedUsers[p_User.m_Id] = p_User;
}

void GraphCache::updateUser(const BusinessUser& p_User)
{
    std::lock_guard<std::mutex> lock(m_CachedUsersLock);
    m_CachedUsers[p_User.m_Id] = p_User;
}

void GraphCache::updateGroup(const Group& p_Group)
{
    std::lock_guard<std::mutex> lock(m_CachedGroupsLock);
    m_CachedGroups[p_Group.m_Id] = p_Group;
}

void GraphCache::updateGroup(const BusinessGroup& p_Group)
{
	std::lock_guard<std::mutex> lock(m_CachedGroupsLock);
	m_CachedGroups[p_Group.m_Id] = p_Group;
}

void GraphCache::updateOrgContact(const BusinessOrgContact& p_OrgContact)
{
	std::lock_guard<std::mutex> lock(m_CachedOrgContactsLock);
	m_CachedOrgContacts[p_OrgContact.m_Id] = p_OrgContact;
}

void GraphCache::updateSite(const BusinessSite& p_Site)
{
    std::lock_guard<std::mutex> lock(m_CachedSitesLock);
    m_CachedSites[p_Site.GetID()] = p_Site;
}

void GraphCache::updateDeletedUser(const BusinessUser& p_User)
{
	ASSERT(p_User.IsFromRecycleBin());
	std::lock_guard<std::mutex> lock(m_CachedDeletedUsersLock);
	m_CachedDeletedUsers[p_User.m_Id] = p_User;
}

void GraphCache::updateDeletedGroup(const BusinessGroup& p_Group)
{
	ASSERT(p_Group.IsFromRecycleBin());
	std::lock_guard<std::mutex> lock(m_CachedDeletedGroupsLock);
	m_CachedDeletedGroups[p_Group.m_Id] = p_Group;
}

namespace
{
	template<class UserObjectType>
	std::vector<UserObjectType> UserReturner(GraphCache& p_Cache, const O365IdsContainer& p_UserIds, const std::vector<BusinessUser>& p_Received, bool p_FullLoad, bool p_Canceled, bool p_HandleSkuCounters, HWND p_Hwnd);

	template<>
	std::vector<CachedUser> UserReturner<CachedUser>(GraphCache& p_Cache, const O365IdsContainer& p_UserIds, const std::vector<BusinessUser>& p_Received, bool p_FullLoad, bool p_Canceled, bool p_HandleSkuCounters, HWND p_Hwnd)
	{
		if (p_HandleSkuCounters
			&& !p_Canceled // If canceled, no count required.
			&& !p_FullLoad // If full load, already handled by caller.
			&& p_Cache.IsSqlUsersCacheFull()
			&& 0 == p_Cache.CountSqlUsersMissingRequirements({ UBI::ASSIGNEDLICENSES }))
		{
			auto sqlUsers = p_Cache.GetSqlCachedUsers(p_Hwnd, {}, false);

			std::map<PooledString, int32_t> skuCounters;
			for (const auto& user : sqlUsers)
			{
				if (user.IsRBACAuthorized() && user.GetAssignedLicenses())
				{
					for (auto& assLic : *user.GetAssignedLicenses())
						skuCounters[assLic.GetID()]++;
				}
			}

			// This is a hack :)
			class GraphCacheAccessor : public GraphCache
			{
			public:
				void SetSkuCounters(std::map<PooledString, int32_t>&& p_SkuCunters)
				{
					setSkuCounters(std::move(p_SkuCunters));
				}
			};
			reinterpret_cast<GraphCacheAccessor*>(&p_Cache)->SetSkuCounters(std::move(skuCounters));
		}

		return p_Cache.GetCachedUsers(p_UserIds);
	};

	template<>
	std::vector<BusinessUser> UserReturner<BusinessUser>(GraphCache& p_Cache, const O365IdsContainer& p_UserIds, const std::vector<BusinessUser>& p_Received, bool p_FullLoad, bool p_Canceled, bool p_HandleSkuCounters, HWND p_Hwnd)
	{
		// If canceled, no count required. If full load, already handled by caller.
		if (p_HandleSkuCounters && !(p_Canceled || p_FullLoad))
		{
			auto sqlUsers = p_Cache.GetSqlCachedUsers(p_Hwnd, {}, false);

			std::map<PooledString, int32_t> skuCounters;
			for (const auto& user : sqlUsers)
			{
				if (user.IsRBACAuthorized() && user.GetAssignedLicenses())
				{
					for (auto& assLic : *user.GetAssignedLicenses())
						skuCounters[assLic.GetID()]++;
				}
			}

			// This is a hack :)
			class GraphCacheAccessor : public GraphCache
			{
			public:
				void SetSkuCounters(std::map<PooledString, int32_t>&& p_SkuCunters)
				{
					setSkuCounters(std::move(p_SkuCunters));
				}
			};
			reinterpret_cast<GraphCacheAccessor*>(&p_Cache)->SetSkuCounters(std::move(skuCounters));

			if (p_UserIds.empty())
				return sqlUsers;
		}

		if (p_FullLoad && p_UserIds.empty() || p_Canceled && !(p_FullLoad && !p_UserIds.empty()))
			return p_Received;

		return p_Cache.GetSqlCachedUsers(p_Hwnd, p_UserIds, false);
	}

	template<class UserObjectType, GraphCache::PreferredMode>
	struct UserListPropertySetTyper
	{
		using Type = UserListFullPropertySet;
	};

	template<>
	struct UserListPropertySetTyper<CachedUser, GraphCache::PreferredMode::LIST>
	{
		using Type = CachedUserPropertySet;
	};

	template<class UserObjectType, GraphCache::PreferredMode preferredMode>
	using UserListPropertySet = typename UserListPropertySetTyper<UserObjectType, preferredMode>::Type;

	template<class UserObjectType>
	GraphCache::PreferredMode validateMode(GraphCache::PreferredMode p_Mode)
	{
		return p_Mode;
	}

	template<>
	GraphCache::PreferredMode validateMode<CachedUser>(GraphCache::PreferredMode p_Mode)
	{
		return GraphCache::PreferredMode::INITDELTA == p_Mode ? GraphCache::PreferredMode::LIST : p_Mode;
	}

	template<class PropertySetClass>
	std::vector<UBI> userBlocksForCache()
	{
		return { UBI::MIN, UBI::LIST, UBI::ASSIGNEDLICENSES };
	}

	template<>
	std::vector<UBI> userBlocksForCache<CachedUserPropertySet>()
	{
		return { UBI::MIN };
	}

	template<class UserObjectType>
	void clearUsersDeltaLinkIfNeeded(GraphCache& p_GraphCache, GraphCache::PreferredMode p_Mode)
	{
		// Nothing for CachedUser
	}

	template<>
	void clearUsersDeltaLinkIfNeeded<BusinessUser>(GraphCache& p_GraphCache, GraphCache::PreferredMode p_Mode)
	{
		if (GraphCache::PreferredMode::LIST == p_Mode)
			p_GraphCache.ClearUsersDeltaLink();
	}
}

namespace Util
{
	template<>
	std::vector<CachedUser> ListErrorHandler(const std::exception_ptr& p_ExceptPtr)
	{
		// Would we want to return the error in some CachedUser object (as for BusinessUser)?
		return {};
	};
}

template<class UserObjectType>
TaskWrapper<std::vector<UserObjectType>>
GraphCache::GetUpToDateUsers(const O365IdsContainer& p_UserIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/)
{
	if (p_OverrideDeltaLink && PreferredMode::DELTASYNC == p_PreferredMode)
		m_UsersDeltaLink = p_OverrideDeltaLink;
	clearUsersDeltaLinkIfNeeded<UserObjectType>(*this, p_PreferredMode);
	UpdateUsersDeltaLinkFromSql(); // be sure we have the last link.

	const auto handleSkuCounters = IsHandleSkuCounters();

	UserListRequester::InitialDeltaRequester* initialReqPtr = nullptr;
	UserListRequester::DeltaLinkRequester* deltaReqPtr = nullptr;
	std::vector<UBI> cacheBlocks;

	auto preferredMode = p_PreferredMode;
	if (!HasUsersDeltaCapabilities())
		preferredMode = PreferredMode::LIST;

	if (PreferredMode::DELTASYNC == preferredMode && !IsUsersDeltaSyncAvailable())
		preferredMode = PreferredMode::INITDELTA;

	//NOTE: For cacheduser specification, no initialDelta request, but regular list with minimum info
	preferredMode = validateMode<UserObjectType>(preferredMode);
	
	ASSERT(PreferredMode::LIST == preferredMode || HasUsersDeltaCapabilities());

	std::unique_ptr<UserListRequester::UserListSubRequester> subReq;
	if (PreferredMode::LIST == preferredMode)
	{
		ASSERT(m_UsersDeltaLink);
		*m_UsersDeltaLink = _YTEXT(""); // Get rid of existing delta link.

		auto subReqList = std::make_unique<UserListRequester::RegularRequester>();
		using PropertySetClass = UserListPropertySet<UserObjectType, PreferredMode::LIST>;
		subReqList->SetProperties(PropertySetClass().GetPropertySet());
		cacheBlocks = userBlocksForCache<PropertySetClass>();
		subReq = std::move(subReqList);
	}
	else if (PreferredMode::INITDELTA == preferredMode)
	{
		auto subReqInit = std::make_unique<UserListRequester::InitialDeltaRequester>();
		using PropertySetClass = UserListPropertySet<UserObjectType, PreferredMode::INITDELTA>;
		subReqInit->SetProperties(PropertySetClass().GetPropertySet());
		cacheBlocks = userBlocksForCache<PropertySetClass>();
		initialReqPtr = subReqInit.get();
		subReq = std::move(subReqInit);
	}
	else if (PreferredMode::DELTASYNC == preferredMode)
	{
		ASSERT(IsUsersDeltaSyncAvailable());
		auto subReqDelta = std::make_unique<UserListRequester::DeltaLinkRequester>(*m_UsersDeltaLink);
		deltaReqPtr = subReqDelta.get();
		subReq = std::move(subReqDelta);

		using PropertySetClass = UserListPropertySet<UserObjectType, PreferredMode::DELTASYNC>;
		cacheBlocks = userBlocksForCache<PropertySetClass>();
	}
	else
	{
		ASSERT(false);
	}

	auto requester = std::make_shared<UserListRequester>(p_Logger, std::move(subReq));
	return safeTaskCall(requester->Send(getSapio365Session(), p_TaskData)
		.Then([this, p_UserIds, usersDeltaLink = m_UsersDeltaLink, sqlCache2 = m_SqlCache, initialReqPtr, deltaReqPtr, requester, cacheBlocks, handleSkuCounters, p_TaskData]()
			{
				const bool isInitDelta = nullptr != initialReqPtr;
				const bool isList = nullptr == initialReqPtr && nullptr == deltaReqPtr;
				const bool isFullLoad = isList || isInitDelta;
				const bool canceled = p_TaskData.IsCanceled();

				auto& users = requester->GetData();

				const bool clear = isFullLoad && !canceled;
				if (clear)	
					ClearUsers();

				boost::YOpt<YTimeDate> refreshDate;
				if (!canceled)
				{
					if (!users.empty())
						refreshDate = users.front().GetLastRequestTime();
					else
						refreshDate = p_TaskData.GetLastRequestOn();
				}

				{
					LoggerService::User(_T("Writing cache to disk."), p_TaskData.GetOriginator());

					if (handleSkuCounters && isFullLoad && !canceled && cacheBlocks.end() != std::find(cacheBlocks.begin(), cacheBlocks.end(), UBI::ASSIGNEDLICENSES))
						addUsersAndCountSkus(users, true, cacheBlocks, refreshDate, clear);
					else
						AddUsers(users, true, cacheBlocks, refreshDate, clear);
				}

				if (!canceled)
				{
					setAllUsersSynced();

					ASSERT(usersDeltaLink);
					if (nullptr != initialReqPtr)
						*usersDeltaLink = initialReqPtr->GetNextDeltalink();
					else if (nullptr != deltaReqPtr)
						*usersDeltaLink = deltaReqPtr->GetNextDeltalink();
				}

				// Either save new link or save empty (list) or unchanged previous
				saveUsersDeltaLink(*usersDeltaLink, sqlCache2);

				return UserReturner<UserObjectType>(*this, p_UserIds, users, isFullLoad, canceled, handleSkuCounters, p_TaskData.GetOriginator());
			}), getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<UserObjectType>, p_TaskData);
}

// Force template instantiation as they've been declared extern in header.
template
TaskWrapper<std::vector<CachedUser>> GraphCache::GetUpToDateUsers<CachedUser>(const O365IdsContainer& p_UserIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/);
template
TaskWrapper<std::vector<BusinessUser>> GraphCache::GetUpToDateUsers<BusinessUser>(const O365IdsContainer& p_UserIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/);

// ================================================

namespace
{
	template<class GroupObjectType>
	std::vector<GroupObjectType> GroupReturner(GraphCache& p_Cache, const O365IdsContainer& p_UserIds, const std::vector<BusinessGroup>& p_Received, bool p_FullLoad, bool p_Canceled, HWND p_Hwnd);

	template<>
	std::vector<CachedGroup> GroupReturner<CachedGroup>(GraphCache& p_Cache, const O365IdsContainer& p_GroupIds, const std::vector<BusinessGroup>& p_Received, bool p_FullLoad, bool p_Canceled, HWND p_Hwnd)
	{
		ASSERT(p_Cache.IsSqlGroupsCacheLoaded() && p_Cache.IsSqlGroupsCacheFull());
		return p_Cache.GetCachedGroups(p_GroupIds);
	};

	template<>
	std::vector<BusinessGroup> GroupReturner<BusinessGroup>(GraphCache& p_Cache, const O365IdsContainer& p_GroupIds, const std::vector<BusinessGroup>& p_Received, bool p_FullLoad, bool p_Canceled, HWND p_Hwnd)
	{
		if (p_FullLoad && p_GroupIds.empty() || p_Canceled && !(p_FullLoad && !p_GroupIds.empty()))
			return p_Received;

		return p_Cache.GetSqlCachedGroups(p_Hwnd, p_GroupIds, false);
	}

	template<class PropertySetClass>
	std::vector<GBI> groupBlocksForCache()
	{
		return { GBI::MIN, GBI::LIST };
	}

	template<>
	std::vector<GBI> groupBlocksForCache<CachedGroupPropertySet>()
	{
		return { GBI::MIN };
	}

	template<class GroupObjectType, GraphCache::PreferredMode>
	struct GroupListPropertySetTyper
	{
		using Type = GroupListFullPropertySet;
	};

	template<>
	struct GroupListPropertySetTyper<CachedGroup, GraphCache::PreferredMode::LIST>
	{
		using Type = CachedGroupPropertySet;
	};

	template<class GroupObjectType, GraphCache::PreferredMode preferredMode>
	using GroupListPropertySet = typename GroupListPropertySetTyper<GroupObjectType, preferredMode>::Type;

	template<class GroupObjectType>
	void clearGroupsDeltaLinkIfNeeded(GraphCache& p_GraphCache, GraphCache::PreferredMode p_Mode)
	{
		// Nothing for CachedGroup
	}

	template<>
	void clearGroupsDeltaLinkIfNeeded<BusinessGroup>(GraphCache& p_GraphCache, GraphCache::PreferredMode p_Mode)
	{
		if (GraphCache::PreferredMode::LIST == p_Mode)
			p_GraphCache.ClearGroupsDeltaLink();
	}
}

namespace Util
{
	template<>
	std::vector<CachedGroup> ListErrorHandler(const std::exception_ptr& p_ExceptPtr)
	{
		// Would we want to return the error in some CachedGroup object (as for BusinessGroup)?
		return {};
	};
}

template<class GroupObjectType>
TaskWrapper<std::vector<GroupObjectType>>
GraphCache::GetUpToDateGroups(const O365IdsContainer& p_GroupIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/)
{
	if (p_OverrideDeltaLink && PreferredMode::DELTASYNC == p_PreferredMode)
		m_GroupsDeltaLink = p_OverrideDeltaLink;
	clearGroupsDeltaLinkIfNeeded<GroupObjectType>(*this, p_PreferredMode);
	UpdateGroupsDeltaLinkFromSql(); // be sure we have the last link.

	GroupListRequester::InitialDeltaRequester* initialReqPtr = nullptr;
	GroupListRequester::DeltaLinkRequester* deltaReqPtr = nullptr;
	std::vector<GBI> cacheBlocks;

	auto preferredMode = p_PreferredMode;
	if (!HasGroupsDeltaCapabilities())
		preferredMode = PreferredMode::LIST;

	if (PreferredMode::DELTASYNC == preferredMode && !IsGroupsDeltaSyncAvailable())
		preferredMode = PreferredMode::INITDELTA;

	//NOTE: For cachedgroup specification, no initialDelta request, but regular list with minimum info
	preferredMode = validateMode<GroupObjectType>(preferredMode);

	ASSERT(PreferredMode::LIST == preferredMode || HasGroupsDeltaCapabilities());

	std::unique_ptr<GroupListRequester::GroupListSubRequester> subReq;
	if (PreferredMode::LIST == preferredMode)
	{
		ASSERT(m_GroupsDeltaLink);
		*m_GroupsDeltaLink = _YTEXT(""); // Get rid of existing delta link.

		auto subReqList = std::make_unique<GroupListRequester::RegularRequester>();
		using PropertySetClass = GroupListPropertySet<GroupObjectType, PreferredMode::LIST>;
		subReqList->SetProperties(PropertySetClass().GetPropertySet());
		cacheBlocks = groupBlocksForCache<PropertySetClass>();
		subReq = std::move(subReqList);
	}
	else if (PreferredMode::INITDELTA == preferredMode)
	{
		auto subReqInit = std::make_unique<GroupListRequester::InitialDeltaRequester>();
		using PropertySetClass = GroupListPropertySet<GroupObjectType, PreferredMode::INITDELTA>;
		subReqInit->SetProperties(PropertySetClass().GetPropertySet());
		cacheBlocks = groupBlocksForCache<PropertySetClass>();
		initialReqPtr = subReqInit.get();
		subReq = std::move(subReqInit);
	}
	else if (PreferredMode::DELTASYNC == preferredMode)
	{
		ASSERT(IsGroupsDeltaSyncAvailable());
		auto subReqDelta = std::make_unique<GroupListRequester::DeltaLinkRequester>(*m_GroupsDeltaLink);
		deltaReqPtr = subReqDelta.get();
		subReq = std::move(subReqDelta);

		using PropertySetClass = GroupListPropertySet<GroupObjectType, PreferredMode::DELTASYNC>;
		cacheBlocks = groupBlocksForCache<PropertySetClass>();
	}
	else
	{
		ASSERT(false);
	}

	auto requester = std::make_shared<GroupListRequester>(p_Logger, std::move(subReq));
	return safeTaskCall(requester->Send(getSapio365Session(), p_TaskData)
		.Then([this, p_GroupIds, groupsDeltaLink = m_GroupsDeltaLink, sqlCache2 = m_SqlCache, initialReqPtr, deltaReqPtr, requester, cacheBlocks, p_TaskData]()
			{
				const bool isInitDelta = nullptr != initialReqPtr;
				const bool isList = nullptr == initialReqPtr && nullptr == deltaReqPtr;
				const bool isFullLoad = isList || isInitDelta;
				const bool canceled = p_TaskData.IsCanceled();

				auto& groups = requester->GetData();

				const bool clear = isFullLoad && !canceled;
				if (clear)
					ClearGroups();

				boost::YOpt<YTimeDate> refreshDate;
				if (!canceled)
				{
					if (!groups.empty())
						refreshDate = groups.front().GetLastRequestTime();
					else
						refreshDate = p_TaskData.GetLastRequestOn();
				}

				LoggerService::User(_T("Writing cache to disk."), p_TaskData.GetOriginator());
				AddGroups(groups, true, cacheBlocks, refreshDate, clear);

				if (!canceled)
				{
					setAllGroupsSynced();

					ASSERT(groupsDeltaLink);
					if (nullptr != initialReqPtr)
						*groupsDeltaLink = initialReqPtr->GetNextDeltalink();
					else if (nullptr != deltaReqPtr)
						*groupsDeltaLink = deltaReqPtr->GetNextDeltalink();
				}

				// Either save new link or save empty (list) or unchanged previous
				saveGroupsDeltaLink(*groupsDeltaLink, sqlCache2);

				return GroupReturner<GroupObjectType>(*this, p_GroupIds, groups, isFullLoad, canceled, p_TaskData.GetOriginator());
			}), getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<GroupObjectType>, p_TaskData);
}

// Force template instantiation as they've been declared extern in header.
template
TaskWrapper<std::vector<CachedGroup>> GraphCache::GetUpToDateGroups<CachedGroup>(const O365IdsContainer& p_GroupIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/);
template
TaskWrapper<std::vector<BusinessGroup>> GraphCache::GetUpToDateGroups<BusinessGroup>(const O365IdsContainer& p_GroupIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/);

TaskWrapper<std::vector<BusinessUser>> GraphCache::InitPartialUsersDelta(YtriaTaskData p_TaskData, const O365IdsContainer& p_UserIds, std::shared_ptr<wstring> p_OutputDeltaLink, const std::shared_ptr<IPageRequestLogger>& p_Logger)
{
	ASSERT(p_OutputDeltaLink);

	UserListRequester::InitialDeltaRequester* initialReqPtr = nullptr;
	std::unique_ptr<UserListRequester::UserListSubRequester> subReq;
	std::vector<UBI> cacheBlocks;
	auto subReqInit = std::make_unique<UserListRequester::InitialDeltaRequester>();
	using PropertySetClass = UserListPropertySet<BusinessUser, PreferredMode::INITDELTA>;
	subReqInit->SetProperties(PropertySetClass().GetPropertySet());
	cacheBlocks = userBlocksForCache<PropertySetClass>();
	initialReqPtr = subReqInit.get();
	subReq = std::move(subReqInit);

	auto requester = std::make_shared<UserListRequester>(p_Logger, std::move(subReq));

	ODataFilter idFilter;
	for (const auto& id : p_UserIds)
	{
		if (id == *p_UserIds.begin())
			idFilter.Equal(_YUID(O365_ID), id, true);
		else
			idFilter.Or().Equal(_YUID(O365_ID), id, true);
	}

	// Be careful, only works on 'id' with InitDelta requester !!
	requester->SetCustomFilter(idFilter);

	return safeTaskCall(requester->Send(getSapio365Session(), p_TaskData).Then([this, requester, initialReqPtr, p_OutputDeltaLink, cacheBlocks, p_TaskData]() {
		auto& users = requester->GetData();
		AddUsers(users, true, cacheBlocks, boost::none, false);

		if (nullptr != initialReqPtr)
			*p_OutputDeltaLink = initialReqPtr->GetNextDeltalink();
		return users;
		}), getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessUser>, p_TaskData);
}

TaskWrapper<std::vector<BusinessGroup>> GraphCache::InitPartialGroupsDelta(YtriaTaskData p_TaskData, const O365IdsContainer& p_GroupIds, std::shared_ptr<wstring> p_OutputDeltaLink, const std::shared_ptr<IPageRequestLogger>& p_Logger)
{
	ASSERT(p_OutputDeltaLink);

	GroupListRequester::InitialDeltaRequester* initialReqPtr = nullptr;
	std::unique_ptr<GroupListRequester::GroupListSubRequester> subReq;
	std::vector<GBI> cacheBlocks;
	auto subReqInit = std::make_unique<GroupListRequester::InitialDeltaRequester>();
	using PropertySetClass = GroupListPropertySet<BusinessGroup, PreferredMode::INITDELTA>;
	subReqInit->SetProperties(PropertySetClass().GetPropertySet());
	cacheBlocks = groupBlocksForCache<PropertySetClass>();
	initialReqPtr = subReqInit.get();
	subReq = std::move(subReqInit);

	auto requester = std::make_shared<GroupListRequester>(p_Logger, std::move(subReq));

	ODataFilter idFilter;
	for (const auto& id : p_GroupIds)
	{
		if (id == *p_GroupIds.begin())
			idFilter.Equal(_YUID(O365_ID), id, true);
		else
			idFilter.Or().Equal(_YUID(O365_ID), id, true);
	}

	// Be careful, only works on 'id' with InitDelta requester !!
	requester->SetCustomFilter(idFilter);

	return safeTaskCall(requester->Send(getSapio365Session(), p_TaskData).Then([this, requester, initialReqPtr, p_OutputDeltaLink, cacheBlocks, p_TaskData]() {
		auto& groups = requester->GetData();
		AddGroups(groups, true, cacheBlocks, boost::none, false);

		if (nullptr != initialReqPtr)
			*p_OutputDeltaLink = initialReqPtr->GetNextDeltalink();
		return groups;
		}), getSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessGroup>, p_TaskData);
}