#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "BusinessContact.h"

class ContactDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessContact>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

