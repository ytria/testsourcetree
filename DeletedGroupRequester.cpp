#include "DeletedGroupRequester.h"

#include "GroupDeserializer.h"
#include "MSGraphUtil.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "RbacRequiredPropertySet.h"
#include "MsGraphHttpRequestLogger.h"

TaskWrapper<void> DeletedGroupRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<GroupDeserializer>(p_Session);
    m_Result = std::make_shared<SingleRequestResult>();

	// Need to request RBAC props?
	// FIXME: New modular cache. Remove required stuff
	const auto properties = RbacRequiredPropertySet<BusinessGroup>(m_Properties, *p_Session).GetPropertySet();

	// Cannot request id, the api will send invalid json..
	web::uri_builder uri(_YTEXT("directory/deletedItems/microsoft.graph.group"));
    uri.append_path(m_GroupId);
    uri.append_query(U("$select"), Rest::GetSelectQuery(properties));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
    return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, m_Result, uri.to_uri(), m_UseBetaEndpoint, m_Logger, httpLogger, p_TaskData);
}
