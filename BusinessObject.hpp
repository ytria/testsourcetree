#pragma once

#include "TimeUtil.h"

template < class T >
map<PooledString, T*> BusinessObject::ToIDMap(vector<T>& p_BusObjects)
{
	map<PooledString, T*> boMap;

	for (const auto& bo : p_BusObjects)
	{
		ASSERT(!bo.GetID().IsEmpty());// either your object was not initialized, or you are using this on an irrelevant object
		if (!bo.GetID().IsEmpty())
			boMap[bo.GetID()] = &bo;
	}

	return boMap;
}

template < class T >
static vector<PooledString> BusinessObject::ToIDs(const vector<T>& p_BusObjects)
{
	vector<PooledString> IDs;

	for (const auto& bo : p_BusObjects)
	{
		ASSERT(!bo.GetID().IsEmpty());// either your object was not initialized, or you are using this on an irrelevant object
		if (!bo.GetID().IsEmpty())
			IDs.push_back(bo.GetID());
	}

	return IDs;
}

template <typename JsonType, typename BusinessType>
vector<rttr::property> BusinessObject::GetUpdatableProperties(JsonType& p_JsonObject, const BusinessObjectBoolMetadataFilters& p_BusinessObjectMetadataFilter, const JsonObjectRequestWhenFilter& p_JsonObjectRequestWhenFilter) const
{
	/******
	WARNING: If you change this method, don't forget to update BusinessUser specialization as well (See BusinessUser.cpp).
	******/

	const BusinessType* businessObj = dynamic_cast<const BusinessType*>(this);
	ASSERT(nullptr != businessObj);
	if (nullptr == businessObj)
		return{};

	p_JsonObject.m_Id = GetID();

	vector<rttr::property> updatableProps;

	const rttr::type businessType = rttr::type::get<BusinessType>();
	const rttr::type jsonType = rttr::type::get<JsonType>();

	for (const auto& businessProp : businessType.get_properties())
	{
		// ignore some properties.
		if (!businessProp.is_valid() || !p_BusinessObjectMetadataFilter(businessProp))
			continue;

		auto jsonPropName = businessProp.get_name();
		const rttr::property jsonTypeProp = jsonType.get_property(jsonPropName);

		if (!jsonTypeProp.is_valid() || !p_JsonObjectRequestWhenFilter(jsonTypeProp))
			continue;

		if (businessProp.get_type() == rttr::type::get<boost::YOpt<PooledString>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<PooledString>>();
				if (value != boost::none)
				{
					if (jsonTypeProp.is_valid())
					{
						const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, value);
						ASSERT(setSuccess);
						updatableProps.push_back(jsonTypeProp);
					}
				}
			}
		}
		else if (businessProp.get_type() == rttr::type::get<boost::YOpt<bool>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<bool>>();
				if (value != boost::none)
				{
					if (jsonTypeProp.is_valid())
					{
						const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, value);
						ASSERT(setSuccess);
						updatableProps.push_back(jsonTypeProp);
					}
				}
			}
		}
		else if (businessProp.get_type() == rttr::type::get<boost::YOpt<YTimeDate>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<YTimeDate>>();
				if (value != boost::none)
				{
					if (jsonTypeProp.is_valid())
					{
						boost::YOpt<PooledString> dateString = TimeUtil::GetInstance().GetISO8601String(*value, TimeUtil::ISO8601_HAS_DATE_AND_TIME);
						// Dates can't be null or empty in Office 365 graph API.
						// "0001-01-01T00:00:00Z" is the date we get when it has never been set, unfortunately settings it gives HTTP 500.
						// Then do not send empty string if ever it's null or empty
						//if (dateString && dateString->IsEmpty()) 
						//	dateString = _YTEXT("0001-01-01T00:00:00Z"); 
						if (dateString && !dateString->IsEmpty())
						{
							const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, dateString);
							ASSERT(setSuccess);
							updatableProps.push_back(jsonTypeProp);
						}
					}
				}
			}
		}
		else if (businessProp.get_type() == rttr::type::get<boost::YOpt<vector<PooledString>>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<vector<PooledString>>>();
				if (value != boost::none)
				{
					if (jsonTypeProp.is_valid())
					{
						if (jsonTypeProp.get_type() == rttr::type::get<boost::YOpt<vector<PooledString>>>())
						{
							const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, value);
							ASSERT(setSuccess);
							updatableProps.push_back(jsonTypeProp);
						}
						else if (jsonTypeProp.get_type() == rttr::type::get<vector<PooledString>>())
						{
							const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, *value);
							ASSERT(setSuccess);
							updatableProps.push_back(jsonTypeProp);
						}
						else
						{
							ASSERT(false);
						}
					}
				}
			}
		}
		else if (businessProp.get_type() == rttr::type::get<boost::YOpt<int32_t>>())
		{
			rttr::variant businessPropVal = businessProp.get_value(*businessObj);
			ASSERT(businessPropVal.is_valid());
			if (businessPropVal.is_valid())
			{
				const auto value = businessPropVal.get_value<boost::YOpt<int32_t>>();
				if (value != boost::none)
				{
					if (jsonTypeProp.is_valid())
					{
						const bool setSuccess = jsonTypeProp.set_value(p_JsonObject, value);
						ASSERT(setSuccess);
						updatableProps.push_back(jsonTypeProp);
					}
				}
			}
		}
		else
		{
			ASSERT(false);
		}
	}

	return updatableProps;
}
