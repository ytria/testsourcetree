#include "FrameApplications.h"

IMPLEMENT_DYNAMIC(FrameApplications, GridFrameBase)

FrameApplications::FrameApplications(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
	m_CreateRefreshPartial = false;
}

void FrameApplications::BuildView(const vector<Application>& p_Applications, bool p_FullPurge, bool p_NoPurge)
{
	m_Grid.BuildView(p_Applications, p_FullPurge, p_NoPurge);
}

O365Grid& FrameApplications::GetGrid()
{
	return m_Grid;
}

void FrameApplications::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Applications, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameApplications::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{

}

bool FrameApplications::HasApplyButton()
{
	return false;
}

void FrameApplications::createGrid()
{
	m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FrameApplications::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(tab);
	CXTPRibbonGroup* refreshGroup = CreateRefreshGroup(tab);

	return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameApplications::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;
	return statusRV;
}
