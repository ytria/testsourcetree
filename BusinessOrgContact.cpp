#include "BusinessOrgContact.h"

#include "BusinessGroup.h"
#include "Languages.h"
#include "MSGraphCommonData.h"
#include "MsGraphFieldNames.h"
#include "MSGraphSession.h"
#include "O365AdminUtil.h"
#include "Sapio365Session.h"
#include "TimeUtil.h"
#include "safeTaskCall.h"
#include "MsGraphHttpRequestLogger.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessOrgContact>("OrgContact") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Organizational Contact"))))
		.constructor()(policy::ctor::as_object)
		.property(O365_USER_DISPLAYNAME, &BusinessOrgContact::m_DisplayName)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_GIVENNAME, &BusinessOrgContact::m_GivenName)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_SURNAME, &BusinessOrgContact::m_Surname)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_MAIL, &BusinessOrgContact::m_Mail)/*(
			metadata(MetadataKeys::ReadOnly, true)
			)*/
		.property(O365_USER_MAILNICKNAME, &BusinessOrgContact::m_MailNickname)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_PROXYADDRESSES, &BusinessOrgContact::m_ProxyAddresses)/*(
			metadata(MetadataKeys::ReadOnly, true)
			)*/
		.property(O365_USER_JOBTITLE, &BusinessOrgContact::m_JobTitle)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_COMPANYNAME, &BusinessOrgContact::m_CompanyName)/*(
			metadata(MetadataKeys::ReadOnly, true)
			)*/
		.property(O365_USER_DEPARTMENT, &BusinessOrgContact::m_Department)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_BUSINESSPHONES, &BusinessOrgContact::m_BusinessPhones)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_MOBILEPHONE, &BusinessOrgContact::m_MobilePhone)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_OFFICELOCATION, &BusinessOrgContact::m_OfficeLocation)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_STREETADDRESS, &BusinessOrgContact::m_StreetAddress)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_CITY, &BusinessOrgContact::m_City)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_STATE, &BusinessOrgContact::m_State)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_POSTALCODE, &BusinessOrgContact::m_PostalCode)/*(
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_COUNTRY, &BusinessOrgContact::m_Country)/*( // 2 Letters code ????
			metadata(MetadataKeys::ValidForCreation, true)
			)*/
		.property(O365_USER_ONPREMISESSYNCENABLED, &BusinessOrgContact::m_OnPremisesSyncEnabled)/*(
			metadata(MetadataKeys::ReadOnly, true)
			)*/
		.property(O365_USER_ONPREMISESLASTSYNCDATETIME, &BusinessOrgContact::m_OnPremisesLastSyncDateTime)/*(
			metadata(MetadataKeys::ReadOnly, true)
			)*/
		;
}

BusinessOrgContact::BusinessOrgContact()
	//: m_Exists(true)
{

}

BusinessOrgContact::BusinessOrgContact(const BusinessOrgContact& p_Other)
	: BusinessObject(dynamic_cast<const BusinessObject&>(p_Other))
{
	*this = p_Other;
}

BusinessOrgContact& BusinessOrgContact::operator=(const BusinessOrgContact& p_Other)
{
	BusinessObject::operator=(p_Other);

	m_BusinessPhones = p_Other.m_BusinessPhones;
	m_City = p_Other.m_City;
	m_CompanyName = p_Other.m_CompanyName;
	m_Country = p_Other.m_Country;
	m_Department = p_Other.m_Department;
	m_DisplayName = p_Other.m_DisplayName;
	m_GivenName = p_Other.m_GivenName;
	m_JobTitle = p_Other.m_JobTitle;
	m_Mail = p_Other.m_Mail;
	m_MailNickname = p_Other.m_MailNickname;
	m_MobilePhone = p_Other.m_MobilePhone;
	m_OfficeLocation = p_Other.m_OfficeLocation;
	m_OnPremisesLastSyncDateTime = p_Other.m_OnPremisesLastSyncDateTime;
	m_OnPremisesSyncEnabled = p_Other.m_OnPremisesSyncEnabled;
	m_PostalCode = p_Other.m_PostalCode;
	m_ProxyAddresses = p_Other.m_ProxyAddresses;
	m_State = p_Other.m_State;
	m_StreetAddress = p_Other.m_StreetAddress;
	m_Surname = p_Other.m_Surname;

	//m_Exists = p_Other.m_Exists;

	MFCUtil::DuplicateUniquePtrVector(m_ParentGroups, p_Other.m_ParentGroups);
	m_ParentGroupIDsFirstLevel = p_Other.m_ParentGroupIDsFirstLevel;

	m_Manager = p_Other.m_Manager;
	//m_DirectReports = p_other.m_DirectReports;

	return *this;
}

//bool BusinessOrgContact::GetExists() const
//{
//    return m_Exists;
//}
//
//void BusinessOrgContact::SetExists(bool val)
//{
//    m_Exists = val;
//}

bool BusinessOrgContact::operator<(const BusinessOrgContact& p_Other) const
{
	return GetID() < p_Other.GetID();
}

const vector<std::unique_ptr<BusinessGroup>>& BusinessOrgContact::GetParentGroups() const
{
	return m_ParentGroups;
}

const vector<PooledString>& BusinessOrgContact::GetParentGroupsIDsFirstLevel() const
{
	return m_ParentGroupIDsFirstLevel;
}

void BusinessOrgContact::SetParentGroups(const vector<BusinessGroup>& p_Groups)
{
	m_ParentGroups.clear();
	for (auto& g : p_Groups)
		AddParent(g);
}

void BusinessOrgContact::AddParent(const BusinessGroup& p_Group)
{
	ASSERT(p_Group.GetID() != GetID());// what ho?
	if (p_Group.GetID() != GetID())
		m_ParentGroups.emplace_back(std::make_unique<BusinessGroup>(p_Group));
}

void BusinessOrgContact::AddParentIDFirstLevel(const PooledString& p_GroupID)
{
	ASSERT(p_GroupID != GetID());// what ho?
	if (p_GroupID != GetID())
		m_ParentGroupIDsFirstLevel.push_back(p_GroupID);
}

bool BusinessOrgContact::IsDirectMemberOf(const BusinessGroup& p_Group) const
{
	return std::find(m_ParentGroupIDsFirstLevel.begin(), m_ParentGroupIDsFirstLevel.end(), p_Group.GetID()) != m_ParentGroupIDsFirstLevel.end();
}

void BusinessOrgContact::SetManager(const BusinessOrgContact& p_Manager)
{
	m_Manager = std::make_shared<BusinessOrgContact>(p_Manager);
}

std::shared_ptr<BusinessOrgContact>& BusinessOrgContact::GetManager()
{
	return m_Manager;
}

BusinessOrgContact* BusinessOrgContact::GetManagerTop()
{
	BusinessOrgContact* topManagerRV = this;
	
	while (nullptr != topManagerRV && topManagerRV->HasManager())
		topManagerRV = topManagerRV->GetManager().get();

	ASSERT(nullptr != topManagerRV);
	return topManagerRV;
}

bool BusinessOrgContact::HasManager() const
{
	return nullptr != m_Manager && !m_Manager->m_Id.IsEmpty();
}

//void BusinessOrgContact::AddDirectReport(const BusinessOrgContact& p_DR)
//{
//	BusinessOrgContact* matchingDR = nullptr;
//	for (auto findIt = m_DirectReports.begin(); matchingDR == nullptr && findIt != m_DirectReports.end(); findIt++)
//		if (findIt->GetID() == p_DR.GetID())
//			matchingDR = &*findIt;
//	if (nullptr != matchingDR)
//	{
//		// integrate DRs from p_DR
//		for (const auto& DR : p_DR.GetDirectReports())
//			matchingDR->AddDirectReport(DR);
//	}
//	else
//		m_DirectReports.push_back(p_DR);
//}
//
//bool BusinessOrgContact::IntegrateIntoManagerHierarchy(BusinessOrgContact& p_User)
//{
//	bool integratedRV = false;
//	
//	BusinessOrgContact& userToIntegrate = *p_User.GetManagerTop();
//
//	if (userToIntegrate.GetID() == GetID())
//	{
//		for (auto& dr : userToIntegrate.GetDirectReports())
//			AddDirectReport(dr);
//		integratedRV = true;
//	}
//	else
//	{
//		for (auto findIt = m_DirectReports.begin(); !integratedRV && findIt != m_DirectReports.end(); findIt++)
//			integratedRV = findIt->IntegrateIntoManagerHierarchy(userToIntegrate);
//	}
//
//	return integratedRV;
//}
//
//const vector<BusinessOrgContact>& BusinessOrgContact::GetDirectReports() const
//{
//	return m_DirectReports;
//}
//
//bool BusinessOrgContact::HasDirecReports() const
//{
//	return !GetDirectReports().empty();
//}

const boost::YOpt<vector<PooledString>>& BusinessOrgContact::GetBusinessPhones() const
{ 
	return m_BusinessPhones; 
}

void BusinessOrgContact::SetBusinessPhones(const vector<PooledString>& val) 
{ 
	m_BusinessPhones = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetCity() const 
{ 
	return m_City; 
}

void BusinessOrgContact::SetCity(const boost::YOpt<PooledString>& val) 
{ 
	m_City = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetCountry() const 
{ 
	return m_Country; 
}

void BusinessOrgContact::SetCountry(const boost::YOpt<PooledString>& val) 
{ 
	m_Country = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetDepartment() const 
{ 
	return m_Department; 
}

void BusinessOrgContact::SetDepartment(const boost::YOpt<PooledString>& val) 
{ 
	m_Department = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetDisplayName() const
{
	return m_DisplayName;
}

void BusinessOrgContact::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetGivenName() const 
{ 
	return m_GivenName; 
}

void BusinessOrgContact::SetGivenName(const boost::YOpt<PooledString>& val) 
{ 
	m_GivenName = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetJobTitle() const 
{ 
	return m_JobTitle; 
}

void BusinessOrgContact::SetJobTitle(const boost::YOpt<PooledString>& val) 
{ 
	m_JobTitle = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetMail() const 
{ 
	return m_Mail; 
}

void BusinessOrgContact::SetMail(const boost::YOpt<PooledString>& val) 
{ 
	m_Mail = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetMailNickname() const 
{ 
	return m_MailNickname; 
}

void BusinessOrgContact::SetMailNickname(const boost::YOpt<PooledString>& val) 
{ 
	m_MailNickname = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetMobilePhone() const 
{ 
	return m_MobilePhone; 
}

void BusinessOrgContact::SetMobilePhone(const boost::YOpt<PooledString>& val) 
{ 
	m_MobilePhone = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetOfficeLocation() const 
{ 
	return m_OfficeLocation; 
}

void BusinessOrgContact::SetOfficeLocation(const boost::YOpt<PooledString>& val) 
{
	m_OfficeLocation = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetOnPremisesLastSyncDateTime() const 
{ 
	return m_OnPremisesLastSyncDateTime; 
}

void BusinessOrgContact::SetOnPremisesLastSyncDateTime(const boost::YOpt<PooledString>& val) 
{ 
	m_OnPremisesLastSyncDateTime = val; 
}

const boost::YOpt<bool>& BusinessOrgContact::GetOnPremisesSyncEnabled() const 
{ 
	return m_OnPremisesSyncEnabled; 
}

void BusinessOrgContact::SetOnPremisesSyncEnabled(const boost::YOpt<bool>& val) 
{ 
	m_OnPremisesSyncEnabled = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetPostalCode() const 
{ 
	return m_PostalCode; 
}

void BusinessOrgContact::SetPostalCode(const boost::YOpt<PooledString>& val) 
{ 
	m_PostalCode = val; 
}

const boost::YOpt<vector<PooledString>>& BusinessOrgContact::GetProxyAddresses() const
{ 
	return m_ProxyAddresses; 
}

void BusinessOrgContact::SetProxyAddresses(const vector<PooledString>& val) 
{ 
	m_ProxyAddresses = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetState() const 
{ 
	return m_State; 
}

void BusinessOrgContact::SetState(const boost::YOpt<PooledString>& val) 
{ 
	m_State = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetStreetAddress() const 
{ 
	return m_StreetAddress; 
}

void BusinessOrgContact::SetStreetAddress(const boost::YOpt<PooledString>& val) 
{ 
	m_StreetAddress = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetSurname() const 
{ 
	return m_Surname; 
}

void BusinessOrgContact::SetSurname(const boost::YOpt<PooledString>& val) 
{ 
	m_Surname = val; 
}

const boost::YOpt<PooledString>& BusinessOrgContact::GetCompanyName() const
{
    return m_CompanyName;
}

void BusinessOrgContact::SetCompanyName(const boost::YOpt<PooledString>& p_Val)
{
    m_CompanyName = p_Val;
}

pplx::task<vector<HttpResultWithError>> BusinessOrgContact::SendEditRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	// Not tested at all !
	ASSERT(false);

	vector<pplx::task<HttpResultWithError>> tasks;

	BusinessOrgContact target;
	const auto updatableProperties = GetUpdatableProperties<BusinessOrgContact, BusinessOrgContact>(target, { { { MetadataKeys::ReadOnly, false } } }, {});
	if (!updatableProperties.empty())
	{
		auto newData = Serialize(updatableProperties);

		web::uri_builder uri;
		uri.append_path(Rest::CONTACTS_PATH);
		uri.append_path(m_Id);

		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
		auto task = safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->Patch(uri.to_uri(), httpLogger, p_TaskData, *newData, _YTEXT("beta")).Then([](const RestResultInfo& p_Result) {
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
		tasks.push_back(task);
	}

	return pplx::when_all(tasks.begin(), tasks.end());
}

TaskWrapper<HttpResultWithError> BusinessOrgContact::SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	web::uri_builder uri;
	uri.append_path(Rest::CONTACTS_PATH);
	uri.append_path(m_Id);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Sapio365Session->GetIdentifier());
	return safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->Delete(uri.to_uri(), httpLogger, p_TaskData, _YTEXT("beta")).Then([p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

