#include "ModuleSchools.h"

#include "Command.h"
#include "CommandShowSchools.h"
#include "FrameSchools.h"
#include "RefreshSpecificData.h"
#include "EducationSchoolsListRequester.h"
#include "safeTaskCall.h"
#include "GridSchools.h"
#include "EducationSchoolDeleteRequester.h"
#include "EducationClassDeleteRequester.h"
#include "EducationClassAddToSchoolRequester.h"
#include "EducationClassUpdateRequester.h"
#include "EducationSchoolUpdateRequester.h"
#include "YSafeCreateTask.h"

void ModuleSchools::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
	case Command::ModuleTask::List:
		showSchools(p_Command);
		break;
	case Command::ModuleTask::ApplyChanges:
		applyChanges(p_Command);
		break;
	default:
		ASSERT(false);
	}
}

void ModuleSchools::showSchools(const Command& p_Command)
{
	AutomationAction* p_Action = p_Command.GetAutomationAction();
	const CommandInfo& info = p_Command.GetCommandInfo();
	const auto p_Origin = info.GetOrigin();

	RefreshSpecificData refreshSpecificData;
	bool isUpdate = false;

	auto p_SourceWindow = p_Command.GetSourceWindow();
	auto sourceCWnd = p_SourceWindow.GetCWnd();

	FrameSchools* frame = dynamic_cast<FrameSchools*>(info.GetFrame());
	if (nullptr == frame)
	{
		ASSERT(!p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>());

		if (FrameSchools::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin = Origin::Tenant;
			moduleCriteria.m_Privilege = info.GetRBACPrivilege();

			if (ShouldCreateFrame<FrameSchools>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				frame = new FrameSchools(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), _T("Schools"), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
		}
	}
	else
	{
		isUpdate = true;
	}

	if (nullptr != frame)
	{
		// Refresh after update? Refresh only specific schools
		if (p_Command.GetCommandInfo().Data().is_type<vector<EducationSchool>>())
		{
			auto schools = p_Command.GetCommandInfo().Data().get_value<vector<EducationSchool>>();
			CommandShowSchools command(schools, *frame, p_Command.GetAutomationSetup(), p_Command.GetAutomationAction(), p_Command.GetLicenseContext(), isUpdate);
			command.Execute();
		}
		else
		{
			CommandShowSchools command(*frame, p_Command.GetAutomationSetup(), p_Command.GetAutomationAction(), p_Command.GetLicenseContext(), isUpdate);
			command.Execute();
		}
	}
	else
	{
		SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
}

void ModuleSchools::applyChanges(const Command& p_Command)
{
	auto action = p_Command.GetAutomationAction();

	const auto& info = p_Command.GetCommandInfo();
	auto frame = dynamic_cast<FrameSchools*>(info.GetFrame());

	ASSERT(info.Data().is_type<SchoolsChanges>());
	if (info.Data().is_type<SchoolsChanges>())
	{
		const auto& changes = info.Data().get_value<SchoolsChanges>();

		YtriaTaskData taskData;
		if (!(changes.m_SchoolDeletions.empty() &&
			changes.m_SchoolUpdates.empty() &&
			changes.m_ClassRemovals.empty() &&
			changes.m_ClassAdditions.empty() &&
			changes.m_ClassUpdates.empty()))
			taskData = addFrameTask(_T("Applying changes"), frame, p_Command, false);

		YSafeCreateTask([bom = frame->GetBusinessObjectManager(), hwnd = frame->GetSafeHwnd(), taskData, changes, action]()
		{
			vector<O365UpdateOperation> updates;
			for (const auto& schoolDelete : changes.m_SchoolDeletions)
			{
				if (taskData.IsCanceled())
					break;

				EducationSchoolDeleteRequester requester(schoolDelete.m_SchoolId);
				requester.Send(bom->GetSapio365Session(), taskData).GetTask().get();

				updates.emplace_back(schoolDelete.m_RowPk);
				updates.back().StoreResult(requester.GetResult());
			}

			for (const auto& schoolUpdate : changes.m_SchoolUpdates)
			{
				if (taskData.IsCanceled())
					break;

				EducationSchoolUpdateRequester requester(schoolUpdate.m_SchoolId, schoolUpdate.m_NewValues);
				requester.Send(bom->GetSapio365Session(), taskData).GetTask().get();

				updates.emplace_back(schoolUpdate.m_RowPk);
				updates.back().StoreResult(requester.GetResult());
			}

			for (const auto& classDelete : changes.m_ClassRemovals)
			{
				if (taskData.IsCanceled())
					break;

				EducationClassDeleteRequester requester(classDelete.m_ClassId);
				requester.Send(bom->GetSapio365Session(), taskData).GetTask().get();

				updates.emplace_back(classDelete.m_RowPk);
				updates.back().StoreResult(requester.GetResult());
			}

			for (const auto& classAddition : changes.m_ClassAdditions)
			{
				if (taskData.IsCanceled())
					break;

				EducationClassAddToSchoolRequester requester(classAddition.m_SchoolId, classAddition.m_ClassId);
				requester.Send(bom->GetSapio365Session(), taskData).GetTask().get();

				updates.emplace_back(classAddition.m_RowPk);
				updates.back().StoreResult(requester.GetResult());
			}

			for (const auto& classUpdate : changes.m_ClassUpdates)
			{
				if (taskData.IsCanceled())
					break;

				EducationClassUpdateRequester requester(classUpdate.m_ClassId, classUpdate.m_NewValues);
				requester.Send(bom->GetSapio365Session(), taskData).GetTask().get();

				updates.emplace_back(classUpdate.m_RowPk);
				updates.back().StoreResult(requester.GetResult());
			}

			YCallbackMessage::DoPost([action, hwnd, taskData, updates]() mutable
			{
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameSchools*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (nullptr != frame)
				{
					frame->GetGrid().ClearLog(taskData.GetId());
					frame->RefreshAfterUpdate(std::move(updates), action);
				}
				else
					SetAutomationGreenLight(action);
				TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
			});
		});
	}
}
