#include "LicenseProcessingStateSerializer.h"

#include "JsonSerializeUtil.h"

LicenseProcessingStateSerializer::LicenseProcessingStateSerializer(const LicenseProcessingState& p_LicenseProcessingState)
    : m_LicenseProcessingState(p_LicenseProcessingState)
{
}

void LicenseProcessingStateSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("state"), m_LicenseProcessingState.m_State, obj);
}
