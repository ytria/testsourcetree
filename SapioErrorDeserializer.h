#pragma once

#include "Encapsulate.h"
#include "SapioError.h"
#include "JsonObjectDeserializer.h"

class SapioErrorDeserializer : public JsonObjectDeserializer, public Encapsulate<SapioError>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};
