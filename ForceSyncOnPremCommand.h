#pragma once

#include "IActionCommand.h"

class GridFrameBase;

class ForceSyncOnPremCommand : public IActionCommand
{
public:
	ForceSyncOnPremCommand(GridFrameBase& p_Frame);

	void ExecuteImpl() const override;

private:
	GridFrameBase& m_Frame;
};

