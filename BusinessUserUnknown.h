#pragma once

#include "BusinessUser.h"

// only for identification issues in grids
class BusinessUserUnknown : public BusinessUser
{
public:
	using BusinessUser::BusinessUser;

	RTTR_ENABLE(BusinessUser)
};
