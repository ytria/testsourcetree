#include "OAuth2BrowserSessionLoader.h"
#include "MainFrame.h"
#include "AzureADOAuth2BrowserAuthenticator.h"
#include "Office365Admin.h"
#include "MSGraphCommonData.h"
#include "DlgBrowserOpener.h"
#include "DbSessionPersister.h"
#include "SessionIdsEmitter.h"
#include "O365AdminUtil.h"

using namespace std::literals::string_literals;

OAuth2BrowserSessionLoader::OAuth2BrowserSessionLoader(const SessionIdentifier& p_Identifier, const PersistentSession& p_PersistentSession)
	: ISessionLoader(p_Identifier)
	, m_PersistentSession(p_PersistentSession)
{
	ASSERT(m_PersistentSession.GetIdentifier() == GetSessionId());
}

TaskWrapper<std::shared_ptr<Sapio365Session>> OAuth2BrowserSessionLoader::Load(const std::shared_ptr<Sapio365Session>& p_SapioSession)
{
	TaskWrapper<std::shared_ptr<Sapio365Session>> result = pplx::task_from_result(std::shared_ptr<Sapio365Session>());

	auto& app = Office365AdminApp::Get();
	MainFrame* mainFrame = dynamic_cast<MainFrame*>(app.m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr == mainFrame)
		return result;

	auto authorizationAuthenticator = std::make_unique<AzureADOAuth2BrowserAuthenticator>(p_SapioSession->GetMainMSGraphSession(),
		Rest::GetMsGraphEndpoint(),
		_YTEXT(""),
		SessionTypes::GetAppId(m_PersistentSession.GetSessionType()),
		SessionTypes::GetUserRedirectUri(m_PersistentSession.GetSessionType()),
		std::make_unique<DlgBrowserOpener>(),
		m_PersistentSession.IsPartnerAdvanced() || m_PersistentSession.IsPartnerElevated() ? m_PersistentSession.GetTenantName() : _YTEXT("common")
	);
	p_SapioSession->GetMainMSGraphSession()->SetAuthenticator(std::move(authorizationAuthenticator));

	const wstring oldSessionType = app.GetSessionType();
	app.SetSessionType(m_PersistentSession.GetSessionType());

	p_SapioSession->GetMainMSGraphSession()->SetSessionPersister(std::make_shared<DbSessionPersister>(m_PersistentSession));

	auto authenticator = dynamic_cast<OAuth2AuthenticatorBase*>(p_SapioSession->GetMainMSGraphSession()->GetAuthenticator().get());
	if (nullptr != authenticator)
		authenticator->SetPreferredLogin(GetSessionId().m_EmailOrAppId);

	if (!p_SapioSession->GetMainMSGraphSession()->LoadSession())
	{
		if (AutomatedApp::IsAutomationRunning() && IsActionLoadSession(app.GetCurrentAction()))
		{
			LoggerService::Debug(_YTEXTFORMAT(L"LoadSession: Unable to load session with name %s.", GetSessionId().m_EmailOrAppId.c_str()));
			app.GetCurrentAction()->AddError(YtriaTranslate::Do(Office365AdminApp_LoadSession_2, _YLOC("An error occurred while authenticating. Please make sure your credentials are correct.")).c_str());
			app.SetAutomationGreenLight(app.GetCurrentAction());
			result = pplx::task_from_result(shared_ptr<Sapio365Session>());
		}
		else
		{
			LoggerService::Debug(_YTEXTFORMAT(L"LoadSession: Unable to load session with name %s. Prompting for login", GetSessionId().m_EmailOrAppId.c_str()));

			result = p_SapioSession->GetMainMSGraphSession()->Start(mainFrame, false).Then([this, mainFrame, persistentSession = m_PersistentSession, p_SessionIdentifier = GetSessionId(), oldSessionType, p_SapioSession, techName = GetSessionId().m_EmailOrAppId](const RestAuthenticationResult& p_Result)
			{
				auto& app = Office365AdminApp::Get();
				const bool success = p_Result.state == RestAuthenticationResult::State::Success;
				if (success)
				{
					mainFrame->SendMessage(WM_AUTHENTICATION_SUCCESS, (WPARAM) new wstring(p_SapioSession->GetMainMSGraphSession()->GetName()), 0);
					TraceIntoFile::trace(_YDUMP("[["s) + SessionIdsEmitter(p_SessionIdentifier).GetId() + _YDUMP("]]") + _YDUMP("Office365AdminApp"), _YDUMP("LoadSession"), _YDUMPFORMAT(L"Successfully loaded %s session: %s", app.GetSessionType().c_str(), techName.c_str()));
				}
				else
				{
					std::lock_guard<std::mutex> lock(app.GetSessionTypeLock());
					app.SetSessionType(oldSessionType);
				}

				return success ? p_SapioSession : nullptr;
			});
		}
	}
	else
	{
		if (m_PersistentSession.IsRole())
			TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("LoadSession"), _YDUMPFORMAT(L"Successfully loaded %s session: %s from existing credentials", app.GetSessionType().c_str(), GetSessionId().m_EmailOrAppId.c_str()));
		result = pplx::task_from_result(p_SapioSession);
	}

	return result;
}
