#include "SpListDeserializer.h"

#include "JsonSerializeUtil.h"

namespace Sp
{
	void ListDeserializer::DeserializeObject(const web::json::object& p_Object)
	{
		JsonSerializeUtil::DeserializeBool(_YTEXT("AllowContentTypes"), m_Data.AllowContentTypes, p_Object);
		JsonSerializeUtil::DeserializeInt32(_YTEXT("BaseTemplate"), m_Data.BaseTemplate, p_Object);
		JsonSerializeUtil::DeserializeInt32(_YTEXT("BaseType"), m_Data.BaseType, p_Object);
		JsonSerializeUtil::DeserializeInt32(_YTEXT("BrowserFileHandling"), m_Data.BrowserFileHandling, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("ContentTypesEnabled"), m_Data.ContentTypesEnabled, p_Object);
		JsonSerializeUtil::DeserializeTimeDate(_YTEXT("Created"), m_Data.Created, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("DefaultContentApprovalWorkflowId"), m_Data.DefaultContentApprovalWorkflowId, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("DefaultDisplayFormUrl"), m_Data.DefaultDisplayFormUrl, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("DefaultEditFormUrl"), m_Data.DefaultEditFormUrl, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("DefaultNewFormUrl"), m_Data.DefaultNewFormUrl, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("DefaultViewUrl"), m_Data.DefaultViewUrl, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("Description"), m_Data.Description, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("Direction"), m_Data.Direction, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("DocumentTemplateUrl"), m_Data.DocumentTemplateUrl, p_Object);
		JsonSerializeUtil::DeserializeInt32(_YTEXT("DraftVersionVisibility"), m_Data.DraftVersionVisibility, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("EnableAttachments"), m_Data.EnableAttachments, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("EnableFolderCreation"), m_Data.EnableFolderCreation, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("EnableMinorVersions"), m_Data.EnableMinorVersions, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("EnableModeration"), m_Data.EnableModeration, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("EnableVersioning"), m_Data.EnableVersioning, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("EntityTypeName"), m_Data.EntityTypeName, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("ForceCheckout"), m_Data.ForceCheckout, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("HasExternalDataSource"), m_Data.HasExternalDataSource, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("HasUniqueRoleAssignments"), m_Data.HasUniqueRoleAssignments, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("Hidden"), m_Data.Hidden, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("Id"), m_Data.Id, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("ImageUrl"), m_Data.ImageUrl, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("IrmEnabled"), m_Data.IrmEnabled, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("IrmExpire"), m_Data.IrmExpire, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("IrmReject"), m_Data.IrmReject, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("IsApplicationList"), m_Data.IsApplicationList, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("IsCatalog"), m_Data.IsCatalog, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("IsPrivate"), m_Data.IsPrivate, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("IsSiteAssetsLibrary"), m_Data.IsSiteAssetsLibrary, p_Object);
		JsonSerializeUtil::DeserializeInt32(_YTEXT("ItemCount"), m_Data.ItemCount, p_Object);
		JsonSerializeUtil::DeserializeTimeDate(_YTEXT("LastItemDeletedDate"), m_Data.LastItemDeletedDate, p_Object);
		JsonSerializeUtil::DeserializeTimeDate(_YTEXT("LastItemModifiedDate"), m_Data.LastItemModifiedDate, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("ListItemEntityTypeFullName"), m_Data.ListItemEntityTypeFullName, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("MultipleDataList"), m_Data.MultipleDataList, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("NoCrawl"), m_Data.NoCrawl, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("OnQuickLaunch"), m_Data.OnQuickLaunch, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("ParentWebUrl"), m_Data.ParentWebUrl, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("SchemaXml"), m_Data.SchemaXml, p_Object);
		JsonSerializeUtil::DeserializeBool(_YTEXT("ServerTemplateCanCreateFolders"), m_Data.ServerTemplateCanCreateFolders, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("TemplateFeatureId"), m_Data.TemplateFeatureId, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("Title"), m_Data.Title, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("ValidationFormula"), m_Data.ValidationFormula, p_Object);
		JsonSerializeUtil::DeserializeString(_YTEXT("ValidationMessage"), m_Data.ValidationMessage, p_Object);
	}
}
