#pragma once

#include "ModuleBase.h"
#include "ModuleUtil.h"

class ModulePost : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

public:
    virtual void executeImpl(const Command& p_Command) override;

private:
	void showPosts(const Command& p_Command);

    void showAttachments(const Command& p_Command);
    void downloadAttachments(const Command& p_Command);
    void showItemAttachment(const Command& p_Command);
    void deleteAttachments(const Command& p_Command);

	O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>> retrievePosts(std::shared_ptr<BusinessObjectManager> p_BOM, const std::map<PooledString, PooledString>& p_ThreadTopics, const std::vector<ModuleUtil::PostMetaInfo>& p_ViewPostInfos, YtriaTaskData taskData);

    static vector<ModuleUtil::PostMetaInfo> SubSubIdsToPostMetaInfos(const O365SubSubIdsContainer& p_SubSubIds);
};

