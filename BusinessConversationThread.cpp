#include "BusinessConversationThread.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessConversationThread>("Conversation Thread")
	(metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Conversation Thread"))))
	.constructor()(policy::ctor::as_object);
}

BusinessConversationThread::BusinessConversationThread()
{
}


BusinessConversationThread::BusinessConversationThread(const ConversationThread& p_Thread)
{
    m_Id = p_Thread.Id;
    m_ToRecipients = p_Thread.ToRecipients;
    m_CcRecipients = p_Thread.CcRecipients;
    m_Topic = p_Thread.Topic;
    m_HasAttachments = p_Thread.HasAttachments;
    m_LastDeliveredDateTime = p_Thread.LastDeliveredDateTime;
    m_UniqueSenders = p_Thread.UniqueSenders;
    m_Preview = p_Thread.Preview;
    m_IsLocked = p_Thread.IsLocked;
}

const vector<Recipient>& BusinessConversationThread::GetToRecipients() const
{
    return m_ToRecipients;
}

void BusinessConversationThread::SetToRecipients(const vector<Recipient>& p_Val)
{
    m_ToRecipients = p_Val;
}

const vector<Recipient>& BusinessConversationThread::GetCcRecipients() const
{
    return m_CcRecipients;
}

void BusinessConversationThread::SetCcRecipients(const vector<Recipient>& p_Val)
{
    m_CcRecipients = p_Val;
}

const boost::YOpt<PooledString>& BusinessConversationThread::GetTopic() const
{
    return m_Topic;
}

void BusinessConversationThread::SetTopic(const boost::YOpt<PooledString>& p_Val)
{
    m_Topic = p_Val;
}

const boost::YOpt<bool>& BusinessConversationThread::GetHasAttachments() const
{
    return m_HasAttachments;
}

void BusinessConversationThread::SetHasAttachments(const boost::YOpt<bool>& p_Val)
{
    m_HasAttachments = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessConversationThread::GetLastDeliveredDateTime() const
{
    return m_LastDeliveredDateTime;
}

void BusinessConversationThread::SetLastDeliveredDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_LastDeliveredDateTime = p_Val;
}

const boost::YOpt<vector<PooledString>>& BusinessConversationThread::GetUniqueSenders() const
{
    return m_UniqueSenders;
}

void BusinessConversationThread::SetUniqueSenders(const boost::YOpt<vector<PooledString>>& p_Val)
{
    m_UniqueSenders = p_Val;
}

const boost::YOpt<PooledString>& BusinessConversationThread::GetPreview() const
{
    return m_Preview;
}

void BusinessConversationThread::SetPreview(const boost::YOpt<PooledString>& p_Val)
{
    m_Preview = p_Val;
}

const boost::YOpt<bool>& BusinessConversationThread::GetIsLocked() const
{
    return m_IsLocked;
}

void BusinessConversationThread::SetIsLocked(const boost::YOpt<bool>& p_Val)
{
    m_IsLocked = p_Val;
}
