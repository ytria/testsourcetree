#pragma once

#include "IRequester.h"

#include "MessageDeserializer.h"
#include "MsGraphPaginator.h"
#include "ODataFilter.h"
#include "PaginatedRequestResults.h"
#include "ValueListDeserializer.h"

class BusinessMessage;
class IPropertySetBuilder;

class MessageListRequester : public IRequester
{
public:
	using DeserializerType = ValueListDeserializer<BusinessMessage, MessageDeserializer>;

	MessageListRequester(IPropertySetBuilder& p_Properties, const wstring& p_UserId, const std::shared_ptr<IPageRequestLogger>& p_Logger);
	MessageListRequester(IPropertySetBuilder& p_Properties, const wstring& p_UserId, const wstring& p_FolderId, const std::shared_ptr<IPageRequestLogger>& p_Logger);

	void SetCutOffDateTime(const wstring& p_CutOff);
	void SetKeepEmails(bool p_Keep);
	void SetKeepChats(bool p_Keep);

	void SetCustomFilter(const ODataFilter& p_CustomFilter);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	vector<BusinessMessage>& GetData();

private:
	std::shared_ptr<DeserializerType> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;

	// Request infos
	wstring m_UserId;
	wstring m_FolderId;
	IPropertySetBuilder& m_Properties;

	ODataFilter m_CustomFilter;

	// Cut-off and filter infos
	wstring m_CutOffDateTime;
	bool m_KeepEmails;
	bool m_KeepChats;

	boost::YOpt<MsGraphPaginator> m_Paginator;

	friend class MailFolderHierarchyRequester;
};

