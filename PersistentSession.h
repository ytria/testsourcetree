#pragma once

#include "Group.h"
#include "Organization.h"
#include "YTimeDate.h"

#include "SessionStatus.h"
#include "SessionIdentifier.h"

#include <string>
#include <vector>

class RestSession;

class PersistentSession
{
public:
	PersistentSession();
	PersistentSession(const wstring& p_EmailAddress, const wstring& p_FullName, const vector<uint8_t>& p_ProfilePhoto);

	bool operator==(const PersistentSession& p_Other) const;

	SessionStatus GetStatus(int64_t p_RoleId) const;

	static SessionStatus GetStatus(const wstring& p_StatusString);
	static const wstring& GetStatusString(SessionStatus p_Status);

	bool GetRoleShowOwnScopeOnly() const;
	void SetRoleShowOwnScopeOnly(bool p_Val);
	bool IsValid() const;
	wstring GetConnectedUserId() const;
	void SetConnectedUserId(const wstring& p_Id);

	const wstring& GetSessionName() const; // E-mail for basic and delegated user sessions, custom display name for Ultra Admin.
	const wstring& GetEmailOrAppId() const; //E-mail for delegated, E-mail suffixed by @ for basic, app ID for Ultra Admin.

	const wstring& GetTenantDisplayName() const;
	const wstring& GetTenantName() const;
	const wstring& GetFullname() const;
	int32_t GetAccessCount() const;
	const YTimeDate& GetLastUsedOn() const;
	const YTimeDate& GetCreatedOn() const;
	const vector<uint8_t>& GetProfilePhoto() const;
	const wstring& GetSessionType() const;
	bool IsFavorite() const;
	int64_t GetRoleId() const;
	void SetRoleId(int64_t p_Val);
	void SetRoleNameInDb(const wstring& p_Val);
	wstring GetRoleName() const;
	wstring GetRoleNameInDb() const;
	int GetSessionTypeResourcePng() const;
	SessionIdentifier GetIdentifier() const;

	bool UseDeltaUsers() const;
	bool UseDeltaGroups() const;

	bool GetAutoLoadOnPrem() const;
	bool GetAskedAutoLoadOnPrem() const;

	bool GetUseOnPrem() const;
	bool GetDontAskAgainUseOnPrem() const;

	bool IsUltraAdmin() const;
	bool IsAdvanced() const;
	bool IsElevated() const;
	bool IsStandard() const;
	bool IsRole() const;
	bool IsPartnerAdvanced() const;
	bool IsPartnerElevated() const;

	void SetSessionName(const wstring& val); // E-mail for basic and delegated user sessions, custom display name for Ultra Admin.
	void SetFullname(const wstring& val);
	void SetAccessCount(int32_t val);
	void SetLastUsedOn(const YTimeDate& val);
	void SetCreatedOn(const YTimeDate& val);
	void SetProfilePhoto(const vector<uint8_t>& val);
	void SetTenant(const Organization& p_Organization);
	void SetTenantDisplayName(const wstring& p_TenantDisplayName);
	void SetTenantName(const wstring& p_TenantName);
	void SetSessionType(const wstring& p_SessionType);
	void SetFavorite(bool p_Favorite);
	void SetUseDeltaUsers(bool p_UseDeltaUsers);
	void SetUseDeltaGroups(bool p_UseDeltaGroups);
	void SetAutoLoadOnPrem(bool p_Value);
	void SetAskedAutoLoadOnPrem(bool p_Value);
	void SetUseOnPrem(bool p_Value);
	void SetDontAskAgainUseOnPrem(bool p_Value);

	// Credentials
	int64_t GetCredentialsId() const;
	void SetCredentialsId(const int64_t& p_Val);
	const wstring& GetAccessToken() const;
	void SetAccessToken(const wstring& p_Val);
	const wstring& GetRefreshToken() const;
	void SetRefreshToken(const wstring& p_Val);
	const wstring& GetAppClientSecret() const;
	void SetAppClientSecret(const wstring& p_Val);
	const wstring& GetAppId() const;
	void SetAppId(const wstring& p_Val);

	void SetTechnicalName(const wstring& p_TechnicalSessionName);

	static void CheckUpdateTo1dot4();

	wstring GetAADComputerName() const;
	void SetAADComputerName(const wstring& val);
	bool GetUseRemoteRSAT() const;
	void SetUseRemoteRSAT(bool val);
	bool GetUseMsDsConsistencyGuid() const;
	void SetUseMsDsConsistencyGuid(bool val);
	wstring GetADDSServer() const;
	void SetADDSServer(const wstring& val);
	wstring GetADDSUsername() const;
	void SetADDSUsername(const wstring& val);
	wstring GetADDSPassword() const;
	void SetADDSPassword(const wstring& val);

private:
	static PersistentSession initEmptySession();

	bool IsInactive() const;

public:
	static const PersistentSession g_EmptySession;

private:
	static wstring g_InvalidStatus;
	static wstring g_ActiveStatus;
	static wstring g_LockedStatus;
	static wstring g_StandbyStatus;
	static wstring g_CachedStatus;
	static wstring g_InactiveStatus;

	static const wstring g_Roles;
	static const wstring g_RoleID;
	static const wstring g_RoleName;
	static const wstring g_RoleLastUsedOn;
	static const wstring g_RoleCreatedOn;
	static const wstring g_RoleIsFavorite;
	static const wstring g_RoleAccessCount;
	static const wstring g_RoleShowOwnScopeOnly;

	static void initStatusStrings();

private:
	wstring m_EmailOrAppId;
	wstring m_DisplayName;
	wstring m_Fullname;
	wstring m_TenantName;
	wstring m_TenantDomain;
	wstring m_SessionType;
	wstring m_Id;
	wstring m_RoleNameInDb;

	int32_t m_AccessCount;
	YTimeDate m_LastUsedOn;
	YTimeDate m_CreatedOn;
	// TODO: Current License
	vector<uint8_t> m_ProfilePhoto;
	int64_t m_RoleId;
	int64_t m_CredentialsId;
	bool m_RoleShowOwnScopeOnly;

	bool m_IsFavorite;

	bool m_UseDeltaUsers;
	bool m_UseDeltaGroups;
	
	bool m_AutoLoadOnPrem;
	bool m_AskedAutoLoadOnPrem;

	bool m_UseOnPrem;
	bool m_DontAskAgainUseOnPrem;

	wstring m_AADComputerName;
	bool m_UseRemoteRSAT;
	bool m_UseMsDsConsistencyGuid;
	wstring m_ADDSServer;
	wstring m_ADDSUsername;
	wstring m_ADDSPassword;

	//OAuth2 credentials
	wstring m_AccessToken;
	wstring m_RefreshToken;
	wstring m_AppClientSecret;
	wstring m_AppId;

	static std::set<wstring> g_OpenedSessions;
};

