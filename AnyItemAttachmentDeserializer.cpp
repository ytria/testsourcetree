#include "AnyItemAttachmentDeserializer.h"

#include "AttendeeDeserializer.h"
#include "DateTimeTimeZoneDeserializer.h"
#include "EmailAddressDeserializer.h"
#include "ItemBodyDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "LocationDeserializer.h"
#include "PatternedRecurrenceDeserializer.h"
#include "PhysicalAddressDeserializer.h"
#include "RecipientDeserializer.h"
#include "ResponseStatusDeserializer.h"

void AnyItemAttachmentDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("@odata.type"), m_Data.DataType, p_Object);

    {
        ListDeserializer<Attendee, AttendeeDeserializer> attDeserializer;
        if (JsonSerializeUtil::DeserializeAny(attDeserializer, _YTEXT("attendees"), p_Object))
            m_Data.Attendees = std::move(attDeserializer.GetData());
    }

    {
        ItemBodyDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("body"), p_Object))
            m_Data.Body = std::move(deserializer.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("bodyPreview"), m_Data.BodyPreview, p_Object);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("categories"), p_Object))
            m_Data.Categories = std::move(lsd.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("changeKey"), m_Data.ChangeKey, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.CreatedDateTime, p_Object);

    {
        DateTimeTimeZoneDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("end"), p_Object))
            m_Data.End = std::move(deserializer.GetData());
    }

    JsonSerializeUtil::DeserializeBool(_YTEXT("hasAttachments"), m_Data.HasAttachments, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("iCalUId"), m_Data.ICalUId, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.Id, p_Object, true);
    JsonSerializeUtil::DeserializeString(_YTEXT("importance"), m_Data.Importance, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isAllDay"), m_Data.IsAllDay, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isCancelled"), m_Data.IsCancelled, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isOrganizer"), m_Data.IsOrganizer, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isReminderOn"), m_Data.IsReminderOn, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.LastModifiedDateTime, p_Object);

    {
        LocationDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("location"), p_Object))
            m_Data.Location = std::move(deserializer.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("onlineMeetingUrl"), m_Data.OnlineMeetingUrl, p_Object);

    {
        RecipientDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("organizer"), p_Object))
            m_Data.Organizer = std::move(deserializer.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("originalEndTimeZone"), m_Data.OriginalEndTimeZone, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("originalStart"), m_Data.OriginalStart, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("originalStartTimeZone"), m_Data.OriginalStartTimeZone, p_Object);

    {
        PatternedRecurrenceDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("recurrence"), p_Object))
            m_Data.Recurrence = std::move(deserializer.GetData());
    }

    JsonSerializeUtil::DeserializeInt32(_YTEXT("reminderMinutesBeforeStart"), m_Data.ReminderMinutesBeforeStart, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("responseRequested"), m_Data.ResponseRequested, p_Object);

    {
        ResponseStatusDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("responseStatus"), p_Object))
            m_Data.ResponseStatus = std::move(deserializer.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("sensitivity"), m_Data.Sensitivity, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("seriesMasterId"), m_Data.SeriesMasterId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("showAs"), m_Data.ShowAs, p_Object);

    {
        DateTimeTimeZoneDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("start"), p_Object))
            m_Data.Start = std::move(deserializer.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("subject"), m_Data.Subject, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.Type, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("webLink"), m_Data.WebLink, p_Object);

    {
        ListDeserializer<Recipient, RecipientDeserializer> deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("bccRecipients"), p_Object))
            m_Data.BccRecipients = std::move(deserializer.GetData());
    }

    {
        ListDeserializer<Recipient, RecipientDeserializer> deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("ccRecipients"), p_Object))
            m_Data.CcRecipients = std::move(deserializer.GetData());
    }

    {
        ListDeserializer<Recipient, RecipientDeserializer> deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("replyTo"), p_Object))
            m_Data.ReplyTo = std::move(deserializer.GetData());
    }

    {
        ListDeserializer<Recipient, RecipientDeserializer> deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("toRecipients"), p_Object))
            m_Data.ToRecipients = std::move(deserializer.GetData());
    }

    {
        RecipientDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("sender"), p_Object))
            m_Data.Sender = std::move(deserializer.GetData());
    }

    {
        RecipientDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("from"), p_Object))
            m_Data.From = std::move(deserializer.GetData());
    }

    {
        ItemBodyDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("uniqueBody"), p_Object))
            m_Data.UniqueBody = std::move(deserializer.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("conversationId"), m_Data.ConversationId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("inferenceClassification"), m_Data.InferenceClassification, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("internetMessageId"), m_Data.InternetMessageId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("parentFolderId"), m_Data.ParentFolderId, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("receivedDateTime"), m_Data.ReceivedDateTime, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("sentDateTime"), m_Data.SentDateTime, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isDeliveryReceiptRequested"), m_Data.IsDeliveryReceiptRequested, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isDraft"), m_Data.IsDraft, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isRead"), m_Data.IsRead, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isReadReceiptRequested"), m_Data.IsDeliveryReceiptRequested, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("assistantName"), m_Data.AssistantName, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("birthDay"), m_Data.Birthday, p_Object);

    {
        PhysicalAddressDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("businessAddress"), p_Object))
            m_Data.BusinessAddress = std::move(deserializer.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("businessHomePage"), m_Data.BusinessHomePage, p_Object);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("businessPhones"), p_Object))
            m_Data.BusinessPhones = std::move(lsd.GetData());
    }

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("children"), p_Object))
            m_Data.Children = std::move(lsd.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("companyName"), m_Data.CompanyName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("department"), m_Data.Department, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.DisplayName, p_Object);

    {
        ListDeserializer<EmailAddress, EmailAddressDeserializer> deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("emailAddresses"), p_Object))
            m_Data.EmailAddresses = std::move(deserializer.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("fileAs"), m_Data.FileAs, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("generation"), m_Data.Generation, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("givenName"), m_Data.GivenName, p_Object);

    {
        PhysicalAddressDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("homeAddress"), p_Object))
            m_Data.HomeAddress = std::move(deserializer.GetData());
    }

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("homePhones"), p_Object))
            m_Data.HomePhones = std::move(lsd.GetData());
    }

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("imAddresses"), p_Object))
            m_Data.ImAddresses = std::move(lsd.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("initials"), m_Data.Initials, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("jobTitle"), m_Data.JobTitle, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("manager"), m_Data.Manager, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("middleName"), m_Data.MiddleName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("mobilePhone"), m_Data.MobilePhone, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("nickName"), m_Data.NickName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("officeLocation"), m_Data.OfficeLocation, p_Object);

    {
        PhysicalAddressDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("otherAddress"), p_Object))
            m_Data.OtherAddress = std::move(deserializer.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("personalNotes"), m_Data.PersonalNotes, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("profession"), m_Data.Profession, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("spouseName"), m_Data.SpouseName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("surname"), m_Data.Surname, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("title"), m_Data.Title, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("yomiCompanyName"), m_Data.YomiCompanyName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("yomiGivenName"), m_Data.YomiGivenName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("yomiSurnameName"), m_Data.YomiSurnameName, p_Object);
}
