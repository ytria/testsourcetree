#include "Sapio365SessionSavedInfo_DEPRECATED.h"

#include "FileUtil.h"
#include "MainFrameSapio365Session.h"
#include "MFCUtil.h"
#include "MSGraphSession.h"
#include "Sapio365Session.h"
#include "Office365Admin.h"
#include "RestCredentialsRegistryManager_DEPRECATED.h"
#include "Resource.h"
#include "TimeUtil.h"

#include "LoggerService.h"
#include "YtriaTranslate.h"
#include "SessionInfo_DEPRECATED.h"
#include "RoleDelegationManager.h"

const wstring Sapio365SessionSavedInfo_DEPRECATED::SessionTypes_DEPRECATED::g_StandardSession{ _YTEXT("basic_user") };
const wstring Sapio365SessionSavedInfo_DEPRECATED::SessionTypes_DEPRECATED::g_AdvancedSession{ _YTEXT("delegated_admin") };
const wstring Sapio365SessionSavedInfo_DEPRECATED::SessionTypes_DEPRECATED::g_UltraAdmin{ _YTEXT("full_admin") };
const wstring Sapio365SessionSavedInfo_DEPRECATED::SessionTypes_DEPRECATED::g_Role{ _YTEXT("role") };

std::set<wstring> Sapio365SessionSavedInfo_DEPRECATED::g_OpenedSessions;

wstring Sapio365SessionSavedInfo_DEPRECATED::g_ActiveStatus;
wstring Sapio365SessionSavedInfo_DEPRECATED::g_LockedStatus;
wstring Sapio365SessionSavedInfo_DEPRECATED::g_StandbyStatus;
wstring Sapio365SessionSavedInfo_DEPRECATED::g_CachedStatus;
wstring Sapio365SessionSavedInfo_DEPRECATED::g_InactiveStatus;
wstring Sapio365SessionSavedInfo_DEPRECATED::g_InvalidStatus;

const wstring Sapio365SessionSavedInfo_DEPRECATED::g_Roles = _YTEXT("roles");
const wstring Sapio365SessionSavedInfo_DEPRECATED::g_RoleID = _YTEXT("id");
const wstring Sapio365SessionSavedInfo_DEPRECATED::g_RoleName = _YTEXT("name");
const wstring Sapio365SessionSavedInfo_DEPRECATED::g_RoleLastUsedOn = _YTEXT("lastUsedOn");
const wstring Sapio365SessionSavedInfo_DEPRECATED::g_RoleCreatedOn = _YTEXT("createdOn");
const wstring Sapio365SessionSavedInfo_DEPRECATED::g_RoleIsFavorite = _YTEXT("isFavorite");
const wstring Sapio365SessionSavedInfo_DEPRECATED::g_RoleAccessCount = _YTEXT("accessCount");
const wstring Sapio365SessionSavedInfo_DEPRECATED::g_RoleShowOwnScopeOnly = _YTEXT("showOwnScopeOnly");

const Sapio365SessionSavedInfo_DEPRECATED Sapio365SessionSavedInfo_DEPRECATED::g_EmptySession = Sapio365SessionSavedInfo_DEPRECATED::initEmptySession();

wstring Sapio365SessionSavedInfo_DEPRECATED::GetApiNameForRegistry(const wstring& p_ApiName)
{
	// Before v1.4.0
	//return p_ApiName;

	// from v1.4.0
	return p_ApiName + _YTEXT("_v2");
}

Sapio365SessionSavedInfo_DEPRECATED::Sapio365SessionSavedInfo_DEPRECATED()
    : Sapio365SessionSavedInfo_DEPRECATED(g_EmptySession)
{
}

Sapio365SessionSavedInfo_DEPRECATED::Sapio365SessionSavedInfo_DEPRECATED(const wstring& p_EmailAddress, const wstring& p_FullName, const vector<uint8_t>& p_ProfilePhoto)
    : m_DisplayName(p_EmailAddress)
    , m_EmailOrAppId(p_EmailAddress)
    , m_Fullname(p_FullName)
    , m_AccessCount(1)
    , m_LastUsedOn(YTimeDate::GetCurrentTimeDate())
    , m_CreatedOn(YTimeDate::GetCurrentTimeDate())
    , m_ProfilePhoto(p_ProfilePhoto)
    , m_IsFavorite(false)
    , m_ShowBaseSession(true)
{
}

Sapio365SessionSavedInfo_DEPRECATED::Sapio365SessionSavedInfo_DEPRECATED(const SessionInfo_DEPRECATED& p_ListElement)
{
    *this = p_ListElement.m_SavedInfo;
}

RestCredentialsRegistryManager_DEPRECATED& Sapio365SessionSavedInfo_DEPRECATED::GetCredentialsManager()
{
	static RestCredentialsRegistryManager_DEPRECATED instance(Sapio365SessionSavedInfo_DEPRECATED::GetApiNameForRegistry(MSGraphSession::API_NAME));
	return instance;
}

SessionStatus Sapio365SessionSavedInfo_DEPRECATED::GetStatus(int64_t p_RoleId) const
{
    SessionStatus status = SessionStatus::INVALID;

	if (GetTechnicalSessionName().empty())
		return status;
    
	auto graphSession = Sapio365Session::Find({ GetTechnicalSessionName(), GetSessionType(), p_RoleId });
	if (graphSession && graphSession->IsConnected())
	{
		status = SessionStatus::ACTIVE;
		graphSession.reset();
	}
	else
	{
		RestCredentials_DEPRECATED creds;
		creds.SetName(GetTechnicalSessionName());

		const bool loaded = GetCredentialsManager().Load(creds);

		if (IsFullAdmin() && loaded && creds.GetValues().find(_YTEXT("app_client_secret")) != creds.GetValues().end()
			&& creds.GetValues().find(_YTEXT("access_token")) != creds.GetValues().end()
			|| loaded && creds.GetValues().find(_YTEXT("access_token")) != creds.GetValues().end()
			&& creds.GetValues().find(_YTEXT("refresh_token")) != creds.GetValues().end()
			)
		{
			if (g_OpenedSessions.find(GetTechnicalSessionName()) != std::end(g_OpenedSessions))
			{
				status = SessionStatus::STANDBY;
			}
			else if (IsInactive())
			{
				status = SessionStatus::INACTIVE;
			}
			else
			{
				status = SessionStatus::CACHED;
			}
		}
		else
		{
			status = SessionStatus::LOCKED;
		}
	}

	return status;
}

SessionStatus Sapio365SessionSavedInfo_DEPRECATED::GetStatus(const wstring& p_StatusString)
{
	initStatusStrings();

	if (p_StatusString == g_ActiveStatus)
		return SessionStatus::ACTIVE;

	if (p_StatusString == g_StandbyStatus)
		return SessionStatus::STANDBY;

	if (p_StatusString == g_CachedStatus)
		return SessionStatus::CACHED;

	if (p_StatusString == g_InactiveStatus)
		return SessionStatus::INACTIVE;

	if (p_StatusString == g_LockedStatus)
		return SessionStatus::LOCKED;

	ASSERT(false);
	return SessionStatus::INVALID;
}

const wstring& Sapio365SessionSavedInfo_DEPRECATED::GetStatusString(SessionStatus p_Status)
{
	initStatusStrings();

	switch (p_Status)
	{
	case SessionStatus::INVALID:
		ASSERT(false);
		return g_InvalidStatus;
	case SessionStatus::ACTIVE:
		return g_ActiveStatus;
	case SessionStatus::STANDBY:
		return g_StandbyStatus;
	case SessionStatus::CACHED:
		return g_CachedStatus;
	case SessionStatus::INACTIVE:
		return g_InactiveStatus;
	case SessionStatus::LOCKED:
		return g_LockedStatus;
	default:
		break;
	}

	ASSERT(false);
	return g_InvalidStatus;
}

bool Sapio365SessionSavedInfo_DEPRECATED::IsInactive() const
{
    bool isInactive = false;
    
    const int64_t TWO_WEEKS = 60 * 60 * 24 * 14;

    const YTimeDate now = YTimeDate::GetCurrentTimeDate();
    if (now.DifferenceInSeconds(GetLastUsedOn()) > TWO_WEEKS)
        isInactive = true;

    return isInactive;
}

wstring Sapio365SessionSavedInfo_DEPRECATED::LoadStringKey(const web::json::value& p_Json, const wstring& p_Key)
{
    wstring readVal;
    if (p_Json.has_string_field(p_Key))
        readVal = p_Json.at(p_Key).as_string();
    else
        LoggerService::Debug(_YTEXTFORMAT(L"Sapio365SessionSavedInfo::Load: Unable to read field: %s", p_Key.c_str()));

    return readVal;
}

wstring Sapio365SessionSavedInfo_DEPRECATED::LoadStringKey(const web::json::value& p_Json, const wstring& p_Key, bool& p_Success)
{
    wstring readVal;
    p_Success = false;
    if (p_Json.has_string_field(p_Key))
    {
        readVal = p_Json.at(p_Key).as_string();
        p_Success = true;
    }
    else
        LoggerService::Debug(_YTEXTFORMAT(L"Sapio365SessionSavedInfo::Load: Unable to read field: %s", p_Key.c_str()));

    return readVal;
}

int32_t Sapio365SessionSavedInfo_DEPRECATED::LoadIntKey(const web::json::value& p_Json, const wstring& p_Key)
{
    int32_t readVal = 0;
    if (p_Json.has_integer_field(p_Key))
        readVal = p_Json.at(p_Key).as_integer();
    else
        LoggerService::Debug(_YTEXTFORMAT(L"Sapio365SessionSavedInfo::Load: Unable to read field: %s", p_Key.c_str()));

    return readVal;
}

bool Sapio365SessionSavedInfo_DEPRECATED::LoadBoolKey(const web::json::value& p_Json, const wstring& p_Key)
{
    bool readVal = false;
    if (p_Json.has_boolean_field(p_Key))
        readVal = p_Json.at(p_Key).as_bool();
    else
        LoggerService::Debug(_YTEXTFORMAT(L"Sapio365SessionSavedInfo::Load: Unable to read field: %s", p_Key.c_str()));

    return readVal;
}

YTimeDate Sapio365SessionSavedInfo_DEPRECATED::LoadTimeDateKey(const web::json::value& p_Json, const wstring& p_Key)
{
    YTimeDate readVal;
    if (p_Json.has_string_field(p_Key))
        TimeUtil::GetInstance().ConvertTextToDate(p_Json.at(p_Key).as_string(), readVal);
    else
        LoggerService::Debug(_YTEXTFORMAT(L"Sapio365SessionSavedInfo::Load: Unable to read field: %s", p_Key.c_str()));

    return readVal;
}

vector<uint8_t> Sapio365SessionSavedInfo_DEPRECATED::LoadBinaryKey(const web::json::value& p_Json, const wstring& p_Key)
{
    vector<uint8_t> readVal;
    if (p_Json.has_string_field(p_Key))
        readVal = utility::conversions::from_base64(p_Json.at(p_Key).as_string());
    else
        LoggerService::Debug(_YTEXTFORMAT(L"Sapio365SessionSavedInfo::Load: Unable to read field: %s", p_Key.c_str()));

    return readVal;
}

std::set<Sapio365SessionSavedInfo_DEPRECATED::Role> Sapio365SessionSavedInfo_DEPRECATED::LoadRoles(const web::json::value& p_Json)
{
    std::set<Sapio365SessionSavedInfo_DEPRECATED::Role> roleObjs;

    if (p_Json.has_array_field(g_Roles))
    {
        const auto& roles = p_Json.at(g_Roles).as_array();
        for (size_t i = 0; i < roles.size(); ++i)
        {
            Role role;
            const auto& jsonRole = roles.at(i);

            bool hasAllFields = jsonRole.has_number_field(g_RoleID) &&
                jsonRole.has_boolean_field(g_RoleIsFavorite) &&
                jsonRole.has_string_field(g_RoleLastUsedOn) &&
                jsonRole.has_string_field(g_RoleCreatedOn) &&
                jsonRole.has_number_field(g_RoleAccessCount);

            if (jsonRole.has_number_field(g_RoleID))
                role.m_Id = jsonRole.at(g_RoleID).as_number().to_int64();
            if (jsonRole.has_boolean_field(g_RoleIsFavorite))
                role.m_IsFavorite = jsonRole.at(g_RoleIsFavorite).as_bool();
            if (jsonRole.has_string_field(g_RoleLastUsedOn))
                TimeUtil::GetInstance().ConvertTextToDate(jsonRole.at(g_RoleLastUsedOn).as_string(), role.m_LastUsedOn);
            if (jsonRole.has_string_field(g_RoleCreatedOn))
                TimeUtil::GetInstance().ConvertTextToDate(jsonRole.at(g_RoleCreatedOn).as_string(), role.m_CreatedOn);
            if (jsonRole.has_number_field(g_RoleAccessCount))
                role.m_AccessCount = jsonRole.at(g_RoleAccessCount).as_number().to_int32();
            if (jsonRole.has_string_field(g_RoleName))
                role.m_Name = jsonRole.at(g_RoleName).as_string();
            else
				role.m_Name = RoleDelegationManager::GetInstance().GetDelegation(role.m_Id, MainFrameSapio365Session()).m_Name;
            if (jsonRole.has_boolean_field(g_RoleShowOwnScopeOnly))
                role.m_ShowOwnScopeOnly = jsonRole.at(g_RoleShowOwnScopeOnly).as_bool();
            else
                role.m_ShowOwnScopeOnly = true;

            ASSERT(hasAllFields);
            if (hasAllFields)
                roleObjs.insert(role);
        }
    }

    return roleObjs;
}

bool Sapio365SessionSavedInfo_DEPRECATED::IsValid() const
{
	// return !(*this == g_EmptySession);
	return m_Id != g_EmptySession.m_Id
		|| m_DisplayName != g_EmptySession.m_DisplayName
		|| m_EmailOrAppId != g_EmptySession.m_EmailOrAppId
		|| m_Fullname != g_EmptySession.m_Fullname
		|| m_TenantName != g_EmptySession.m_TenantName
		|| m_TenantDomain != g_EmptySession.m_TenantDomain
		|| m_AccessCount != g_EmptySession.m_AccessCount
		|| m_LastUsedOn != g_EmptySession.m_LastUsedOn
		|| m_CreatedOn != g_EmptySession.m_CreatedOn
		|| m_SessionType != g_EmptySession.m_SessionType;
}

wstring Sapio365SessionSavedInfo_DEPRECATED::GetConnectedUserId() const
{
    return m_Id;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetConnectedUserId(const wstring& p_Id)
{
    m_Id = p_Id;
}

const wstring& Sapio365SessionSavedInfo_DEPRECATED::GetTechnicalSessionName() const
{
    return m_EmailOrAppId;
}

const wstring& Sapio365SessionSavedInfo_DEPRECATED::GetSessionName() const
{
    return m_DisplayName;
}

const wstring& Sapio365SessionSavedInfo_DEPRECATED::GetTenantDisplayName() const
{
    return m_TenantName;
}

const wstring& Sapio365SessionSavedInfo_DEPRECATED::GetTenantName() const
{
    return m_TenantDomain;
}

const wstring& Sapio365SessionSavedInfo_DEPRECATED::GetFullname() const
{
    return m_Fullname;
}

int32_t Sapio365SessionSavedInfo_DEPRECATED::GetAccessCount() const
{
    return m_AccessCount;
}

const YTimeDate& Sapio365SessionSavedInfo_DEPRECATED::GetLastUsedOn() const
{
    return m_LastUsedOn;
}

const YTimeDate& Sapio365SessionSavedInfo_DEPRECATED::GetCreatedOn() const
{
    return m_CreatedOn;
}

const vector<uint8_t>& Sapio365SessionSavedInfo_DEPRECATED::GetProfilePhoto() const
{
    return m_ProfilePhoto;
}

const wstring& Sapio365SessionSavedInfo_DEPRECATED::GetSessionType() const
{
    return m_SessionType;
}

bool Sapio365SessionSavedInfo_DEPRECATED::IsFullAdmin() const
{
    return m_SessionType == SessionTypes_DEPRECATED::g_UltraAdmin;
}

bool Sapio365SessionSavedInfo_DEPRECATED::IsAdmin() const
{
	return m_SessionType == SessionTypes_DEPRECATED::g_AdvancedSession;
}

bool Sapio365SessionSavedInfo_DEPRECATED::IsFavorite() const
{
	return m_IsFavorite;
}

bool Sapio365SessionSavedInfo_DEPRECATED::GetShowBaseSession() const
{
    return m_ShowBaseSession;
}

int Sapio365SessionSavedInfo_DEPRECATED::GetSessionTypeResourcePng(int64_t p_RoleId) const
{
    int image = 0;
    if (m_SessionType == SessionTypes_DEPRECATED::g_StandardSession)
        image = IDB_ICON_STANDARD_SESSION;
    else if (m_SessionType == SessionTypes_DEPRECATED::g_AdvancedSession)
        image = IDB_ICON_ADVANCED_SESSION;
    else if (m_SessionType == SessionTypes_DEPRECATED::g_UltraAdmin)
        image = IDB_ICON_ULTRAADMIN_SESSION;

    if (p_RoleId != 0)
        image = IDB_ICON_ROLE_SESSION;

    ASSERT(image != 0);
    return image;
}

int Sapio365SessionSavedInfo_DEPRECATED::GetSessionTypeResourceBmp() const
{
    int image = 0;
    if (m_SessionType == SessionTypes_DEPRECATED::g_StandardSession)
        image = IDB_ICON_STANDARD_SESSION_BMP;
    else if (m_SessionType == SessionTypes_DEPRECATED::g_AdvancedSession)
        image = IDB_ICON_ADVANCED_SESSION_BMP;
    else if (m_SessionType == SessionTypes_DEPRECATED::g_UltraAdmin)
        image = IDB_ICON_ULTRAADMIN_SESSION_BMP;

    ASSERT(image != 0);
    return image;
}

wstring Sapio365SessionSavedInfo_DEPRECATED::GetDisplayedSessionType() const
{
    wstring shownSessionType;
    
    if (GetSessionType() == SessionTypes_DEPRECATED::g_StandardSession)
        shownSessionType = YtriaTranslate::Do(O365Session_GetDisplayedSessionType_1, _YLOC("Standard Session")).c_str();
    else if (GetSessionType() == SessionTypes_DEPRECATED::g_AdvancedSession)
        shownSessionType = YtriaTranslate::Do(O365Session_GetDisplayedSessionType_2, _YLOC("Advanced Session")).c_str();
    else if (GetSessionType() == SessionTypes_DEPRECATED::g_UltraAdmin)
        shownSessionType = YtriaTranslate::Do(O365Session_GetDisplayedSessionType_3, _YLOC("Ultra Admin Session")).c_str();

	/*
	else if(rbac)
		shownSessionType = YtriaTranslate::Do(Sapio365SessionSavedInfo_GetDisplayedSessionType_1, _YLOC("using role %1"), rbac.getRoleName());
	
	*/

    return shownSessionType;
}

const std::set<Sapio365SessionSavedInfo_DEPRECATED::Role>& Sapio365SessionSavedInfo_DEPRECATED::GetRoles() const
{
	return m_Roles;
}

std::set<Sapio365SessionSavedInfo_DEPRECATED::Role>& Sapio365SessionSavedInfo_DEPRECATED::GetRoles()
{
    return m_Roles;
}

void Sapio365SessionSavedInfo_DEPRECATED::Save() const
{
    web::json::value json = web::json::value::object();
    json[_YTEXT("emailAddress")] = web::json::value::string(GetSessionName());
    json[_YTEXT("tenantName")] = web::json::value::string(GetTenantDisplayName());
    json[_YTEXT("tenantTechName")] = web::json::value::string(GetTenantName());
    json[_YTEXT("fullName")] = web::json::value::string(GetFullname());
    json[_YTEXT("accessCount")] = web::json::value::number(GetAccessCount());
    json[_YTEXT("favorite")] = web::json::value::number(IsFavorite());
    json[_YTEXT("sessionType")] = web::json::value::string(GetSessionType());
    json[_YTEXT("showBaseSession")] = web::json::value::boolean(GetShowBaseSession());

    wstring lastUsedOn = TimeUtil::GetInstance().GetISO8601String(GetLastUsedOn());
    wstring createdOn = TimeUtil::GetInstance().GetISO8601String(GetCreatedOn());
    json[_YTEXT("lastUsedOn")] = web::json::value::string(lastUsedOn);
    json[_YTEXT("createdOn")] = web::json::value::string(createdOn);
    json[_YTEXT("profilePhoto")] = web::json::value::string(utility::conversions::to_base64(GetProfilePhoto()));

    size_t roleIndex = 0;
    for (const auto& role : m_Roles)
    {
        web::json::value jsonRole = web::json::value::object();
        jsonRole[g_RoleID] = web::json::value::number(role.m_Id);
        jsonRole[g_RoleName] = web::json::value::string(role.m_Name);

        jsonRole[g_RoleCreatedOn] = web::json::value::string(TimeUtil::GetInstance().GetISO8601String(role.m_CreatedOn));
        jsonRole[g_RoleLastUsedOn] = web::json::value::string(TimeUtil::GetInstance().GetISO8601String(role.m_LastUsedOn));

        jsonRole[g_RoleIsFavorite] = web::json::value::boolean(role.m_IsFavorite);
        jsonRole[g_RoleAccessCount] = web::json::value::number(role.m_AccessCount);
        jsonRole[g_RoleShowOwnScopeOnly] = web::json::value::boolean(role.m_ShowOwnScopeOnly);

        if (!json.has_array_field(g_Roles))
        {
            roleIndex = 0;
            json[g_Roles] = web::json::value::array();
            json[g_Roles].as_array()[roleIndex++] = jsonRole;
        }
        else
        {
            json[g_Roles].as_array()[roleIndex++] = jsonRole;
        }
    }

    std::ofstream file(GetFilePath(m_EmailOrAppId));
    file << MFCUtil::convertUNICODE_to_UTF8(json.serialize());
}

std::pair<bool, Sapio365SessionSavedInfo_DEPRECATED> Sapio365SessionSavedInfo_DEPRECATED::Load(const wstring& p_SessionTechnicalName)
{
	return LoadImpl(GetFilePath(p_SessionTechnicalName), true);
}

std::pair<bool, Sapio365SessionSavedInfo_DEPRECATED> Sapio365SessionSavedInfo_DEPRECATED::LoadImpl(const wstring& p_SessionFilePath, bool p_DeleteIfError)
{
	std::ifstream stream;
	stream.open(p_SessionFilePath, std::ios::in | std::ios::binary);

	std::pair<bool, Sapio365SessionSavedInfo_DEPRECATED> result;
	result.first = false;
	result.second.m_EmailOrAppId = FileUtil::FileRemoveExtension(FileUtil::FileGetFileName(p_SessionFilePath));

	Sapio365SessionSavedInfo_DEPRECATED& session = result.second;
	if (stream)
	{
		try
		{
			std::stringstream buffer;
			buffer << stream.rdbuf();
			web::json::value json = web::json::value::parse(MFCUtil::convertUTF8_to_UNICODE(buffer.str()));

			session.SetSessionName(LoadStringKey(json, _YTEXT("emailAddress")));
			session.SetTenantDisplayName(LoadStringKey(json, _YTEXT("tenantName")));
			session.SetTenantName(LoadStringKey(json, _YTEXT("tenantTechName")));
			session.SetFullname(LoadStringKey(json, _YTEXT("fullName")));
			session.SetAccessCount(LoadIntKey(json, _YTEXT("accessCount")));
			session.SetFavorite(LoadIntKey(json, _YTEXT("favorite")) == 1);

			if (json.has_boolean_field(_YTEXT("showBaseSession")))
				session.SetShowBaseSession(LoadBoolKey(json, _YTEXT("showBaseSession")));
			else
				session.SetShowBaseSession(true);

			if (json.has_string_field(_YTEXT("sessionType")))
			{
				session.SetSessionType(LoadStringKey(json, _YTEXT("sessionType")));
			}
			else
			{
				bool fullAdmin = LoadIntKey(json, _YTEXT("fullAdmin")) == 1;
				if (fullAdmin)
					session.SetSessionType(SessionTypes_DEPRECATED::g_UltraAdmin);
				else
					session.SetSessionType(SessionTypes_DEPRECATED::g_AdvancedSession);
				session.Save();
			}

			session.SetLastUsedOn(LoadTimeDateKey(json, _YTEXT("lastUsedOn")));
			session.SetCreatedOn(LoadTimeDateKey(json, _YTEXT("createdOn")));
			session.SetProfilePhoto(LoadBinaryKey(json, _YTEXT("profilePhoto")));
			session.SetRoles(LoadRoles(json));
			result.first = true;
		}
		catch (const std::exception& e)
		{
			stream.close();
			if (p_DeleteIfError)
			{
				LoggerService::Debug(_YDUMPFORMAT(L"Error while loading session %s: %s. Deleting session.", session.m_EmailOrAppId.c_str(), Str::convertFromUTF8(e.what()).c_str()));
				FileUtil::DeleteFiles({ p_SessionFilePath }, false, false);
				RestCredentialsRegistryManager_DEPRECATED reg(Sapio365SessionSavedInfo_DEPRECATED::GetApiNameForRegistry(MSGraphSession::API_NAME));
				RestCredentials_DEPRECATED cred;
				cred.SetName(session.m_EmailOrAppId);
				reg.Delete(cred);
			}
			else
			{
				LoggerService::Debug(_YDUMPFORMAT(L"Error while loading session %s: %s.", session.m_EmailOrAppId.c_str(), Str::convertFromUTF8(e.what()).c_str()));
			}
		}
	}

	return result;
}

void Sapio365SessionSavedInfo_DEPRECATED::Delete(const wstring& p_EmailOrAppId)
{
    ::DeleteFile(GetFilePath(p_EmailOrAppId).c_str());
}

bool Sapio365SessionSavedInfo_DEPRECATED::Exists(const wstring& p_EmailOrAppId)
{
	// TODO: Check if exists in database. Use global instance?
    std::ifstream file(GetFilePath(p_EmailOrAppId));
    return file.is_open();
}

void Sapio365SessionSavedInfo_DEPRECATED::SetTechnicalName(const wstring& p_TechnicalSessionName)
{
    m_EmailOrAppId = p_TechnicalSessionName;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetSessionName(const wstring & val)
{
    m_DisplayName = val;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetFullname(const wstring& val)
{
    m_Fullname = val;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetAccessCount(int32_t val)
{
    m_AccessCount = val;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetLastUsedOn(const YTimeDate& val)
{
    m_LastUsedOn = val;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetCreatedOn(const YTimeDate& val)
{
    m_CreatedOn = val;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetProfilePhoto(const vector<uint8_t>& val)
{
    m_ProfilePhoto = val;
}

wstring Sapio365SessionSavedInfo_DEPRECATED::GetFilePath(const wstring& p_Filename)
{
    return GetSessionFilesDirectoryPath() + p_Filename + _YTEXT(".json");
}

wstring Sapio365SessionSavedInfo_DEPRECATED::GetSessionFilesDirectoryPath()
{
	return MFCUtil::GetExpandedString(wstring(_YTEXT("%LOCALAPPDATA%\\Ytria\\sapio365\\sessions_v2\\")));
}

wstring Sapio365SessionSavedInfo_DEPRECATED::GetSessionFilesDirectoryPath_v1dot3()
{
	return MFCUtil::GetExpandedString(wstring(_YTEXT("%LOCALAPPDATA%\\Ytria\\sapio365\\sessions\\")));
}

wstring Sapio365SessionSavedInfo_DEPRECATED::GetOldSessionFilesDirectoryPath()
{
	return MFCUtil::GetExpandedString(wstring(_YTEXT("%LOCALAPPDATA%\\Ytria\\")));
}

void Sapio365SessionSavedInfo_DEPRECATED::checkOldSessionDirectory()
{
	static bool checkAlreadyDone = false;
	ASSERT(!checkAlreadyDone);
	if (!checkAlreadyDone)
	{
		const wstring newPath = GetSessionFilesDirectoryPath_v1dot3();
		if (!FileUtil::FileExists(newPath))
		{
			const wstring oldPath = GetOldSessionFilesDirectoryPath();
			if (FileUtil::FileExists(oldPath))
				FileUtil::ProcessFile({ oldPath + L"*.json" }, { newPath }, FO_MOVE, FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY);
		}
		checkAlreadyDone = true;
	}
}

namespace
{
	void ListJsonFiles(const wstring& p_Path, vector<wstring>& files)
	{
		CFileFind finder;
		if (finder.FindFile(p_Path.c_str()))
		{
			finder.FindNextFile();

			if (finder.IsDirectory())
			{
				const CString strWildcard = finder.GetFilePath() + _YTEXT("\\*.json");
				auto exploreMe = finder.FindFile(strWildcard);
				while (exploreMe)
				{
					exploreMe = finder.FindNextFile();
					if (!finder.IsDots())
						ListJsonFiles(finder.GetFilePath().GetBuffer(), files);
				}
			}
			else
			{
				files.push_back((LPCTSTR)finder.GetFilePath());
			}
		}
		finder.Close();
	}
}

void Sapio365SessionSavedInfo_DEPRECATED::CheckUpdateTo1dot4()
{
	static bool checkAlreadyDone = false;
	ASSERT(!checkAlreadyDone);
	if (!checkAlreadyDone)
	{
		std::map<wstring, wstring> m_UATenantToAppID;
		auto& newManager = GetCredentialsManager();
		if (newManager.List().empty())
		{
			RestCredentialsRegistryManager_DEPRECATED oldManager(MSGraphSession::API_NAME);
			for (auto s : oldManager.List())
			{
				if (oldManager.Load(s))
				{
					auto values = s.GetValues();
					if (values.end() != values.find(_YTEXT("app_id"))) // Is Ultra Admin
					{
						if (values[_YTEXT("app_tenant")].empty())
						{
							values[_YTEXT("app_tenant")] = s.GetName();

							//ASSERT(!values[_YTEXT("app_display_name")].empty());
							//if (!values[_YTEXT("app_display_name")].empty())
							//	values[_YTEXT("app_display_name")] = s.GetName();

							ASSERT(!values[_YTEXT("app_id")].empty());
							if (!values[_YTEXT("app_id")].empty())
							{
								m_UATenantToAppID[s.GetName()] = values[_YTEXT("app_id")];
								s.SetName(values[_YTEXT("app_id")]);
							}
						}
						s.SetValues(values);
					}
					newManager.Save(s);
				}
			}
		}

		const wstring newPath = GetSessionFilesDirectoryPath();
		if (!FileUtil::FileExists(newPath))
		{
			const wstring oldPath = GetSessionFilesDirectoryPath_v1dot3();
			if (FileUtil::FileExists(oldPath))
			{
				FileUtil::ProcessFile({ oldPath + L"*.json" }, { newPath }, FO_COPY, FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_FILESONLY);

				const auto path = Str::endsWith(newPath, _YTEXT("\\")) ? newPath.substr(0, newPath.size() - 1) : newPath;
				vector<wstring> files;
				ListJsonFiles(path, files);

				for (auto& file : files)
				{
					auto res = LoadImpl(file, false);
					if (res.first && res.second.IsFullAdmin())
					{
						auto& appID = m_UATenantToAppID[res.second.GetTechnicalSessionName()];
						res.second.SetSessionName(res.second.GetTechnicalSessionName());
						res.second.SetTechnicalName(appID);

						res.second.Save();
					}
				}
				
			}
		}
		checkAlreadyDone = true;
	}
}

void Sapio365SessionSavedInfo_DEPRECATED::SetTenant(const Organization& p_Organization)
{
	m_TenantDomain = p_Organization.GetInitialDomain();
	m_TenantName = p_Organization.DisplayName ? *p_Organization.DisplayName : _YTEXT("");
}

void Sapio365SessionSavedInfo_DEPRECATED::SetTenantDisplayName(const wstring& p_TenantDisplayName)
{
	m_TenantName = p_TenantDisplayName;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetTenantName(const wstring& p_TenantName)
{
	m_TenantDomain = p_TenantName;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetSessionType(const wstring& p_SessionType)
{
    m_SessionType = p_SessionType;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetFavorite(bool p_Favorite)
{
	m_IsFavorite = p_Favorite;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetShowBaseSession(bool p_ShowBaseSession)
{
    m_ShowBaseSession = p_ShowBaseSession;
}

void Sapio365SessionSavedInfo_DEPRECATED::SetRoles(const std::set<Role>& p_Roles)
{
    m_Roles = p_Roles;
}

void Sapio365SessionSavedInfo_DEPRECATED::initStatusStrings()
{
	static bool init = false;

	if (!init)
	{
		g_InvalidStatus		= YtriaTranslate::Do(O365Session_initStatusStrings_1, _YLOC("Error")).c_str();
		g_ActiveStatus		= YtriaTranslate::Do(O365Session_initStatusStrings_2, _YLOC("Active")).c_str();
		g_LockedStatus		= YtriaTranslate::Do(O365Session_initStatusStrings_3, _YLOC("Locked")).c_str();
		g_StandbyStatus		= YtriaTranslate::Do(O365Session_initStatusStrings_4, _YLOC("Standby")).c_str();
		g_CachedStatus		= YtriaTranslate::Do(O365Session_initStatusStrings_5, _YLOC("Cached")).c_str();
		g_InactiveStatus	= YtriaTranslate::Do(O365Session_initStatusStrings_6, _YLOC("Inactive")).c_str();
	}

	init = true;
}

Sapio365SessionSavedInfo_DEPRECATED Sapio365SessionSavedInfo_DEPRECATED::initEmptySession()
{
	checkOldSessionDirectory();
	return Sapio365SessionSavedInfo_DEPRECATED(_YTEXT(""), _YTEXT(""), {});
}

Sapio365SessionSavedInfo_DEPRECATED::Role::Role()
	: m_Id(0)
	, m_CreatedOn(YTimeDate::GetCurrentTimeDate())
	, m_LastUsedOn(m_CreatedOn)
	, m_AccessCount(1)
	, m_IsFavorite(false)
	, m_ShowOwnScopeOnly(false)
{
}

Sapio365SessionSavedInfo_DEPRECATED::Role::Role(int64_t Id)
	: Role()
{
	m_Id = Id;
}
