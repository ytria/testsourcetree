#pragma once

#include "ResourceAccess.h"

class RequiredResourceAccess
{
public:
	boost::YOpt<vector<ResourceAccess>> m_ResourceAccess;
	boost::YOpt<PooledString> m_ResourceAppId;
};

