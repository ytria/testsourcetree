#pragma once

#include "JsonObjectDeserializer.h"
#include "BusinessGroup.h"
#include "Encapsulate.h"

class Sapio365Session;

class WholeGroupDeserializer : public JsonObjectDeserializer
                             , public Encapsulate<BusinessGroup>
{
public:
    WholeGroupDeserializer(const std::shared_ptr<Sapio365Session>& p_Session);
    void DeserializeObject(const web::json::object& p_Object) override;

private:
    const std::shared_ptr<Sapio365Session>& m_Session;
};

