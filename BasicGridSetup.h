#pragma once

#include "GridSetupMetaColumn.h"

class CacheGrid;

class BasicGridSetup
{
public:
    BasicGridSetup(const wstring& p_AutomationName, const wstring& p_LicenseTrackingCode);
    BasicGridSetup(const wstring& p_AutomationName, const wstring& p_LicenseTrackingCode, const std::vector<GridSetupMetaColumn>& p_DefaultMetaColumns);
    BasicGridSetup(const wstring& p_AutomationName,
				   const wstring& p_LicenseTrackingCode,
				   const std::vector<GridSetupMetaColumn>& p_DefaultMetaColumns,
				   const std::vector<rttr::variant>& p_LoadMoreObjectTypes);

    void Setup(CacheGrid& p_Grid, bool p_AddColumnLastRequestDateTime); // FIXME: Remove this bool parameter as soon as every grid sets it to true.

    void AddMetaColumn(const GridSetupMetaColumn& p_MetaCol);

	GridBackendColumn* GetColumnLastRequestDateTime() const;

	static const wstring g_LastQueryColumnUID;

private:
    void setDefaultMetaColumns(const vector<GridSetupMetaColumn>& p_MetaCols);

private:
    const wstring m_AutomationName;
    const wstring m_LicenseTrackingCode;
    std::vector<GridSetupMetaColumn> m_MetaColumns;
    std::vector<rttr::variant> m_LoadMoreObjectTypes;
	GridBackendColumn* m_ColumnLastRequestDateTime;
};

