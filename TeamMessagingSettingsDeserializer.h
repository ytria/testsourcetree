#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "TeamMessagingSettings.h"

class TeamMessagingSettingsDeserializer : public JsonObjectDeserializer, public Encapsulate<TeamMessagingSettings>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

