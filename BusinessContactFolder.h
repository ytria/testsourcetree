#pragma once

#include "BusinessObject.h"

#include "BusinessContact.h"
#include "ContactFolder.h"

class BusinessContactFolder : public BusinessObject
{
public:
    BusinessContactFolder() = default;
    BusinessContactFolder(const ContactFolder& p_ContactFolder);

    const boost::YOpt<PooledString>& GetDisplayName() const;
    const boost::YOpt<PooledString>& GetParentFolderId() const;

    void SetDisplayName(const boost::YOpt<PooledString>& p_DisplayName);
    void SetParentFolderId(const boost::YOpt<PooledString>& p_ParentFolderId);

    const vector<BusinessContactFolder>& GetChildrenContactFolders() const;
    void SetChildrenContactFolders(const vector<BusinessContactFolder>& p_Val);

    const vector<BusinessContact>& GetContacts() const;
    void SetContacts(const vector<BusinessContact>& p_Val);
private:
    boost::YOpt<PooledString> m_DisplayName;
    boost::YOpt<PooledString> m_ParentFolderId;

    // Non-property members
    vector<BusinessContactFolder> m_ChildrenContactFolders;
    vector<BusinessContact> m_Contacts;

    friend class ContactFolderDeserializer;
};

