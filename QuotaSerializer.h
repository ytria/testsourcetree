#pragma once

#include "IJsonSerializer.h"
#include "Quota.h"

class QuotaSerializer : public IJsonSerializer
{
public:
	QuotaSerializer(const Quota& p_Quota);
	void Serialize() override;

private:
	Quota m_Quota;
};

