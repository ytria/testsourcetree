#pragma once

#include "IXmlSerializer.h"

namespace Ex
{
    class AddDelegate;
}

namespace Ex
{
class AddDelegateSerializer : public IXmlSerializer
{
public:
    AddDelegateSerializer(XERCES_CPP_NAMESPACE::DOMDocument& p_Document, const Ex::AddDelegate& p_AddDelegate);

protected:
    void SerializeImpl(DOMElement& p_Element) override;

private:
    const Ex::AddDelegate& m_AddDelegate;
};
}

