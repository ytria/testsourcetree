#include "IActionCommand.h"

IActionCommand::IActionCommand(const wstring& p_Name)
	:m_Name(p_Name)
{
}

void IActionCommand::Execute() const
{
	ExecuteImpl();
}

void IActionCommand::SetCallback(std::function<void()> p_Callback)
{
	m_Callback = p_Callback;
}

const wstring& IActionCommand::GetName() const
{
	return m_Name;
}

void IActionCommand::SetAutomationSetup(const AutomationSetup& p_Setup)
{
	m_Setup = p_Setup;
}
