#include "ConversationThreadPropertySet.h"

//#include "rttr/property.h"

vector<rttr::property> ConversationThreadPropertySet::GetPropertySet() const
{
	// Use GetStringPropertySet instead!!
	ASSERT(false);

	return {};
}

vector<PooledString> ConversationThreadPropertySet::GetStringPropertySet() const
{
	return
	{
			_YTEXT("ccRecipients")
		,	_YTEXT("hasAttachments")
		,	_YTEXT("id")
		,	_YTEXT("isLocked")
		,	_YTEXT("lastDeliveredDateTime")
		,	_YTEXT("preview")
		,	_YTEXT("toRecipients")
		,	_YTEXT("topic")		
		,	_YTEXT("uniqueSenders")
	};
}
