#pragma once

#include "IPSObjectCollectionDeserializer.h"
#include "Encapsulate.h"
#include "RecipientPermission.h"

class RecipientPermissionCollectionDeserializer : public IPSObjectCollectionDeserializer, public Encapsulate<std::vector<RecipientPermission>>
{
public:
	void Deserialize(IPSObjectPropertyReader& p_Reader) override;
	void SetCount(size_t p_Count) override;
};

