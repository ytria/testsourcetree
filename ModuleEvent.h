#pragma once

#include "ModuleBase.h"

#include "AttachmentsInfo.h"
#include "BusinessEvent.h"
#include "CommandInfo.h"
#include "ModuleOptions.h"
#include "EventListRequester.h"

class AutomationAction;
class FrameEvents;

class ModuleEvent : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

private:
    virtual void executeImpl(const Command& p_Command) override;

	void showEvents(Command p_Command);

	void update(const Command& p_Command);

	O365DataMap<BusinessUser, vector<BusinessEvent>> retrieveUserEvents(std::shared_ptr<BusinessObjectManager> p_BOM, const O365IdsContainer& p_IDs, Origin p_Origin, bool p_LoadUsersFromSQlCache, const ModuleOptions& p_Options, map<BusinessUser, std::shared_ptr<EventListRequester>>& p_Requesters, YtriaTaskData taskData);
	O365DataMap<BusinessGroup, vector<BusinessEvent>> retrieveGroupEvents(std::shared_ptr<BusinessObjectManager> p_BOM, const O365IdsContainer& p_IDs, bool p_LoadGroupsFromSQlCache, const ModuleOptions& p_Options, map<BusinessGroup, std::shared_ptr<EventListRequester>>& p_Requesters, YtriaTaskData taskData);

    void showAttachments(const Command& p_Command);
    void downloadAttachments(const Command& p_Command);
    void showItemAttachment(const Command& p_Command);
    void deleteAttachments(const Command& p_Command);

    void loadSnapshot(const Command& p_Command);
};

