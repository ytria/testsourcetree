#include "MessageFileAttachmentRequester.h"
#include "MSGraphSession.h"
#include "FileAttachmentDeserializer.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

MessageFileAttachmentRequester::MessageFileAttachmentRequester(PooledString p_UserId, PooledString p_MessageId, PooledString p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger)
    : m_UserId(p_UserId)
	, m_MessageId(p_MessageId)
	, m_AttachmentId(p_AttachmentId)
	, m_Logger(p_Logger)
{
}

TaskWrapper<void> MessageFileAttachmentRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<FileAttachmentDeserializer>();

    web::uri_builder uri(_YTEXT("users"));
    uri.append_path(m_UserId);
    uri.append_path(_YTEXT("messages"));
    uri.append_path(m_MessageId);
    uri.append_path(_YTEXT("attachments"));
    uri.append_path(m_AttachmentId);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
    return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, uri.to_uri(), false, m_Logger, httpLogger, p_TaskData);
}

const BusinessFileAttachment& MessageFileAttachmentRequester::GetData() const
{
    return m_Deserializer->GetData();
}

