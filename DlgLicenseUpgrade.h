#pragma once

#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "VendorLicense.h"
#include "YAdvancedHtmlView.h"

class DlgLicenseUpgrade;
class Simulator : public CCmdTarget
{
public:
	Simulator(DlgLicenseUpgrade* p_DlgUpgrade);
	BSTR Simulate(LPCWSTR p_NewQty, LPCWSTR p_NowOrNextBilling, LPCWSTR p_ValidationKey);
private:
	DECLARE_DISPATCH_MAP()

	DlgLicenseUpgrade* m_DlgUpgrade;
};

class DlgLicenseUpgrade : public ResizableDialog, public YAdvancedHtmlView::IPostedDataTarget, public YAdvancedHtmlView::IPostedURLTarget
{
public:
	DlgLicenseUpgrade(const VendorLicense& p_License, uint32_t p_Min, CWnd* p_Parent);

	enum { IDD = IDD_DLG_LICENSEUPGRADE};

	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data) override;
	virtual bool OnNewPostedURL(const wstring& url) override;

	const LicenseUpgradeConfig& GetUpgradeCfg() const;

protected:
	virtual BOOL OnInitDialogSpecificResizable() override;

	ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);

private:

	VendorLicense m_License;

	uint32_t m_Min;

	LicenseUpgradeConfig m_UpgradeConfig;

	static const wstring g_JSONBtnNameCancel;
	static const wstring g_JSONBtnNameOK;
	static const wstring g_JSONKeyValidation;
	static const wstring g_JSONNewUserCapacity;
	static const wstring g_JSONnext;
	static const wstring g_JSONnow;

	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;
};