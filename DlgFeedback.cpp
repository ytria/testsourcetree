#include "DlgFeedback.h"

#include "AutomatedApp.h"
#include "DlgRegAccountRegistration.h"
#include "JSONUtil.h"
#include "LicenseManager.h"
#include "Localization/YtriaTranslate.h"

std::map<wstring, int, Str::keyLessInsensitive> DlgFeedback::g_Sizes;

 const wstring DlgFeedback::g_CancelButtonName = _YTEXT("Cancel");
 const wstring DlgFeedback::g_SubmitButtonName = _YTEXT("Submit");
 const wstring DlgFeedback::g_TextAreaName = _YTEXT("textArea");

DlgFeedback::DlgFeedback(const wstring& previousText, MOOD mood, CWnd* p_Parent)
	: ResizableDialog(IDD_DLG_FEEDBACK, p_Parent)
	, m_Text(previousText)
	, m_Mood(mood)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_EndDialogFixedResult(IDCANCEL)
{
	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_FEEDBACK_SIZE, g_Sizes);
		ASSERT(success);
	}
}

DlgFeedback::~DlgFeedback()
{

}

bool DlgFeedback::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	{
		// Cancel?
		const auto it = data.find(g_CancelButtonName);
		if (data.end() != it)
		{
			endDialogFixed(IDCANCEL);
			return false;
		}
	}

	for (const auto& item : data)
	{
		const auto& propName = item.first;

		ASSERT(g_CancelButtonName != propName); // Handled above.

		if (g_SubmitButtonName == propName)
			continue;

		ASSERT(propName == g_TextAreaName);
		if (propName == g_TextAreaName)
			m_Text = item.second;
	}

	endDialogFixed(IDOK);

	return false;
}

const wstring& DlgFeedback::GetText() const
{
	return m_Text;
}

BOOL DlgFeedback::OnInitDialogSpecificResizable()
{
	wstring title;
	if (m_Mood == MOOD::QUESTION)
		title = YtriaTranslate::Do(DlgFeedback_OnInitDialogSpecificResizable_1, _YLOC("Your question about sapio365")).c_str();
	else if (m_Mood == MOOD::SUGGEST_NEW_JOB)
		title = YtriaTranslate::Do(AutomationWizardO365_generateJSON_3, _YLOC("Suggest a new job")).c_str();
	SetWindowText(CString(title.c_str()));

	CWnd* pCtrl = GetDlgItem(IDC_DLG_FEEDBACK_HTMLPLACEHOLDER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_DLG_FEEDBACK_HTMLPLACEHOLDER, NULL);

	m_HtmlView->SetPostedDataTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
		{
				{ L"JSON"		,{ L"", L"", jsonScriptData.c_str() } }
			,	{ L"HBS-JS"		,{ L"text/javascript", L"hbs-feedback.js", L"" } } // This file is in resources
			,	{ L"OTHER-JS"	,{ L"text/javascript", L"feedback.js", L"" } }, // This file is in resources
		}
	);

	m_HtmlView->SetLinksToReplace({ { L"OTHER-CSS",{ L"stylesheet", L"feedback.css" } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-height"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-width"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
	//BlockVResize(true);

	return TRUE;
}

namespace
{
	static const wstring g_DoubleQuotes = _YTEXT("\"");
	static const wstring g_Semicolon = _YTEXT(":");
	static const wstring g_Comma = _YTEXT(",");
}

void DlgFeedback::generateJSONScriptData(wstring& p_Output)
{
	const wstring userEmail = [=]()
	{
		AutomatedApp* o365App = dynamic_cast<AutomatedApp*>(AfxGetApp());
		ASSERT(nullptr != o365App);
		if (nullptr != o365App)
		{
			LicenseManager* licenseMgr = o365App->GetLicenseManager();
			ASSERT(nullptr != licenseMgr);
			if (nullptr != licenseMgr)
				return licenseMgr->GetUserData().m_Email;
		}
		return wstring(_YTEXT(""));
	}();

	const wstring iconString = GetIconString();
	const wstring subTitle = GetSubtitle(userEmail);
	const wstring textPlaceHolder = GetPlaceHolder();
	const wstring sendBtnText = GetSendBtnText();

    json::value root =
     JSON_BEGIN_OBJECT
        JSON_BEGIN_PAIR
            _YTEXT("main"),
            JSON_BEGIN_OBJECT
                JSON_PAIR_KEY(_YTEXT("id"))                     JSON_PAIR_VALUE(JSON_STRING(_YTEXT("DlgFeedback"))),
                JSON_PAIR_KEY(_YTEXT("method"))                 JSON_PAIR_VALUE(JSON_STRING(_YTEXT("POST"))),
                JSON_PAIR_KEY(_YTEXT("action"))                 JSON_PAIR_VALUE(JSON_STRING(_YTEXT("#"))),

            JSON_END_OBJECT
        JSON_END_PAIR,
        JSON_BEGIN_PAIR
            _YTEXT("items"),
            JSON_BEGIN_ARRAY
                JSON_BEGIN_OBJECT
                    JSON_PAIR_KEY(_YTEXT("hbs"))                JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Feedback"))),
					//JSON_PAIR_KEY(_YTEXT("formTitle"))          JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesButKeepTags(title))),
                    JSON_PAIR_KEY(_YTEXT("subTitle"))           JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(subTitle))),
                    JSON_PAIR_KEY(_YTEXT("placeholder"))        JSON_PAIR_VALUE(JSON_STRING(textPlaceHolder)),
                    JSON_PAIR_KEY(_YTEXT("value"))              JSON_PAIR_VALUE(JSON_STRING(m_Text)),
                    JSON_PAIR_KEY(_YTEXT("textareaName"))       JSON_PAIR_VALUE(JSON_STRING(g_TextAreaName)),
					JSON_BEGIN_PAIR
						_YTEXT("subItems"),
						JSON_BEGIN_ARRAY
							JSON_BEGIN_OBJECT
								JSON_PAIR_KEY(_YTEXT("hbs"))                JSON_PAIR_VALUE(JSON_STRING(iconString)),
							JSON_END_OBJECT,
						JSON_END_ARRAY
					JSON_END_PAIR
                JSON_END_OBJECT,
                JSON_BEGIN_OBJECT
                    JSON_PAIR_KEY(_YTEXT("hbs"))                JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Button"))),
                    JSON_BEGIN_PAIR
                        _YTEXT("choices"),
                        JSON_BEGIN_ARRAY
                            JSON_BEGIN_OBJECT
                                JSON_PAIR_KEY(_YTEXT("type"))      JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
                                JSON_PAIR_KEY(_YTEXT("label"))     JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(sendBtnText))),
                                JSON_PAIR_KEY(_YTEXT("name"))      JSON_PAIR_VALUE(JSON_STRING(g_SubmitButtonName)),
                                JSON_PAIR_KEY(_YTEXT("attribute")) JSON_PAIR_VALUE(JSON_STRING(_YTEXT("post"))),
                                JSON_PAIR_KEY(_YTEXT("class"))     JSON_PAIR_VALUE(JSON_STRING(_YTEXT("is-primary"))),
                            JSON_END_OBJECT,
                            JSON_BEGIN_OBJECT
                                JSON_PAIR_KEY(_YTEXT("type"))      JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
                                JSON_PAIR_KEY(_YTEXT("label"))     JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgFeedback_generateJSONScriptData_6, _YLOC("Cancel")).c_str()))),
								JSON_PAIR_KEY(_YTEXT("attribute")) JSON_PAIR_VALUE(JSON_STRING(_YTEXT("cancel"))),
								JSON_PAIR_KEY(_YTEXT("name"))      JSON_PAIR_VALUE(JSON_STRING(g_CancelButtonName)),
                            JSON_END_OBJECT
                        JSON_END_ARRAY
                    JSON_END_PAIR
                JSON_END_OBJECT
            JSON_END_ARRAY
        JSON_END_PAIR
     JSON_END_OBJECT;


    p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}

wstring DlgFeedback::GetIconString() const
{
	wstring str;
	if (m_Mood == MOOD::HAPPY)
		str = _YTEXT("feedback-happy");
	else if (m_Mood == MOOD::ANGRY)
		str = _YTEXT("feedback-sad");
	else if (m_Mood == MOOD::QUESTION || m_Mood == MOOD::SUGGEST_NEW_JOB)
		str = _YTEXT("feedback-question");
	return str;
}

wstring DlgFeedback::GetSubtitle(const wstring& p_UserEmail) const
{
	wstring str;
	if (m_Mood == MOOD::HAPPY || m_Mood == MOOD::ANGRY)
		str = YtriaTranslate::Do(DlgFeedback_generateJSONScriptData_1, _YLOC("Let us know what you're thinking, anything at all, as often as you want. Your observations and opinions are important to us."));
	else if (m_Mood == MOOD::QUESTION)
		str = YtriaTranslate::Do(DlgFeedback_generateJSONScriptData_7, _YLOC("Ask us anything concerning sapio365, we'll reply to you (%1) as soon as possible."), p_UserEmail.c_str());
	else if (m_Mood == MOOD::SUGGEST_NEW_JOB)
		str = YtriaTranslate::Do(DlgFeedback_GetSubtitle_1, _YLOC("Send us a new job suggestion")).c_str();
	return str;
}

wstring DlgFeedback::GetPlaceHolder() const
{
	wstring str;
	if (m_Mood == MOOD::HAPPY || m_Mood == MOOD::ANGRY)
		str = YtriaTranslate::Do(DlgFeedback_generateJSONScriptData_3, _YLOC("e.g. I clicked a user entry, and I wondered how to see their drive items."));
	else if (m_Mood == MOOD::QUESTION)
		str = YtriaTranslate::Do(DlgFeedback_generateJSONScriptData_2, _YLOC("e.g. How can I see permissions?"));
	else if (m_Mood == MOOD::SUGGEST_NEW_JOB)
		str = _YTEXT("");
	return str;
}

wstring DlgFeedback::GetSendBtnText() const
{
	wstring str;
	if (m_Mood == MOOD::HAPPY || m_Mood == MOOD::ANGRY)
		str = YtriaTranslate::Do(DlgFeedback_generateJSONScriptData_5, _YLOC("Send your comments"));
	else if (m_Mood == MOOD::QUESTION)
		str = YtriaTranslate::Do(DlgFeedback_generateJSONScriptData_4, _YLOC("Send your question"));
	else if (m_Mood == MOOD::SUGGEST_NEW_JOB)
		str = YtriaTranslate::Do(DlgFeedback_GetSendBtnText_1, _YLOC("Send Suggestion")).c_str();
	return str;
}
