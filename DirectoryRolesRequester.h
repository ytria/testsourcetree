#pragma once

#include "IRequester.h"
#include "IRequestLogger.h"

template <class T, class U>
class ValueListDeserializer;

class BusinessDirectoryRole;
class DirectoryRoleDeserializer;
class PaginatedRequestResults;
class Sapio365Session;
struct SessionIdentifier;

class DirectoryRolesRequester : public IRequester
{
public:
	DirectoryRolesRequester(const std::shared_ptr<IRequestLogger>& p_Logger);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	TaskWrapper<void> Send(std::shared_ptr<MSGraphSession> p_Session, const SessionIdentifier& p_Identifier, YtriaTaskData p_TaskData);

    const vector<BusinessDirectoryRole>& GetData() const;

private:
    std::shared_ptr<ValueListDeserializer<BusinessDirectoryRole, DirectoryRoleDeserializer>> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
};

