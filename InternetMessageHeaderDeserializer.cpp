#include "InternetMessageHeaderDeserializer.h"

#include "JsonSerializeUtil.h"

void InternetMessageHeaderDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("value"), m_Data.m_Value, p_Object);
}
