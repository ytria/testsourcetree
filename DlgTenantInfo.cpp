#include "DlgTenantInfo.h"

#include "JSONUtil.h"
#include "Office365Admin.h"
#include "SessionTypes.h"

std::map<wstring, int, Str::keyLessInsensitive> DlgTenantInfo::g_Sizes;

const wstring DlgTenantInfo::g_OKButtonName = _YTEXT("ok");
const wstring DlgTenantInfo::g_CancelButtonName = _YTEXT("cancel");
const wstring DlgTenantInfo::g_AppIdFieldName = _YTEXT("applicationid");
const wstring DlgTenantInfo::g_TenantFieldName = _YTEXT("tenantname");
const wstring DlgTenantInfo::g_RedirectURLFieldName = _YTEXT("redirectURL");

DlgTenantInfo::DlgTenantInfo(const wstring& p_Tenant, const wstring& p_AppId, const wstring& p_RedirectURL, CWnd* p_Parent)
	: ResizableDialog(DlgTenantInfo::IDD, p_Parent)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_Tenant(p_Tenant)
	, m_AppId(p_AppId)
	, m_RedirectURL(p_RedirectURL)
{
	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_GIVEADMINCONSENT_SIZE, g_Sizes);
		ASSERT(success);
	}
}

bool DlgTenantInfo::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	int endDialogId = -1;
	for (const auto& item : data)
	{
		if (item.first == g_AppIdFieldName)
			m_AppId = item.second;
		else if (item.first == g_TenantFieldName)
			m_Tenant = item.second;
		else if (item.first == g_RedirectURLFieldName)
			m_RedirectURL = item.second;
		else if (item.first == g_CancelButtonName)
			endDialogId = IDCANCEL;
		else if (item.first == g_OKButtonName)
			endDialogId = IDOK;
	}

	ASSERT(IDOK == endDialogId || IDCANCEL == endDialogId);
	if (IDOK == endDialogId || IDCANCEL == endDialogId)
	{
		endDialogFixed(endDialogId);
		return false;
	}

	return true;
}

bool DlgTenantInfo::OnNewPostedURL(const wstring& url)
{
	return false; // Nothing to do.
}

const wstring& DlgTenantInfo::GetTenantName() const
{
    return m_Tenant;
}

const wstring& DlgTenantInfo::GetAppId() const
{
    return m_AppId;
}

const wstring& DlgTenantInfo::GetRedirectURL() const
{
	return m_RedirectURL;
}

BOOL DlgTenantInfo::OnInitDialogSpecificResizable()
{
	SetWindowText(YtriaTranslate::Do(DlgTenantInfo_OnInitDialogSpecificResizable_1, _YLOC("Tenant information")).c_str());

	CWnd* pCtrl = GetDlgItem(IDC_GRID_PLACEHOLDER);
	ASSERT(nullptr != pCtrl);
	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, NULL);
	m_HtmlView->SetPostedDataTarget(this);
	m_HtmlView->SetPostedURLTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
		{
			 { _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
			,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-giveAdminConsent.js"), _YTEXT("") } } // This file is in resources
			,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("giveAdminConsent.js"), _YTEXT("") } }, // This file is in resources
		}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("giveAdminConsent.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-height"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-width"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
	BlockVResize(true);

	return TRUE;
}

void DlgTenantInfo::generateJSONScriptData(wstring& p_Output)
{
    web::json::value root =
        JSON_BEGIN_OBJECT
            JSON_BEGIN_PAIR
                _YTEXT("main"),
                JSON_BEGIN_OBJECT
                    JSON_PAIR_KEY(_YTEXT("id"))             JSON_PAIR_VALUE(JSON_STRING(_YTEXT("DlgTenantInfo"))),
                    JSON_PAIR_KEY(_YTEXT("method"))         JSON_PAIR_VALUE(JSON_STRING(_YTEXT("POST"))),
                    JSON_PAIR_KEY(_YTEXT("action"))         JSON_PAIR_VALUE(JSON_STRING(_YTEXT("#")))
                JSON_END_OBJECT
            JSON_END_PAIR,
            JSON_BEGIN_PAIR
                _YTEXT("items"),
                JSON_BEGIN_ARRAY
                    JSON_BEGIN_OBJECT
                        JSON_PAIR_KEY(_YTEXT("hbs"))					JSON_PAIR_VALUE(JSON_STRING(_YTEXT("giveAdminConsent"))),
                        JSON_PAIR_KEY(_YTEXT("introTitle"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_1, _YLOC("Please complete the following using the resulting registration information:")).c_str()))),
                        JSON_PAIR_KEY(_YTEXT("tenantField"))			JSON_PAIR_VALUE(JSON_STRING(g_TenantFieldName)),
                        JSON_PAIR_KEY(_YTEXT("tenantLabel"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_2, _YLOC("Your Tenant name:")).c_str()))),
                        JSON_PAIR_KEY(_YTEXT("tenantPlaceholder"))		JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_3, _YLOC("mytenant.onmicrosoft.com")).c_str())),
                        JSON_PAIR_KEY(_YTEXT("tenantPopup"))			JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_4, _YLOC("Please provide valid name")).c_str())),
                        JSON_PAIR_KEY(_YTEXT("tenantValue"))			JSON_PAIR_VALUE(JSON_STRING(m_Tenant)),
						JSON_PAIR_KEY(_YTEXT("tenantReadOnly"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("X"))),
						JSON_PAIR_KEY(_YTEXT("appIDField"))				JSON_PAIR_VALUE(JSON_STRING(g_AppIdFieldName)),
                        JSON_PAIR_KEY(_YTEXT("appIDLabel"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_5, _YLOC("Your application ID")).c_str()))),
                        JSON_PAIR_KEY(_YTEXT("appIDPlaceholder"))		JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_6, _YLOC("xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")).c_str())),
                        JSON_PAIR_KEY(_YTEXT("appIDPopup"))				JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
                        JSON_PAIR_KEY(_YTEXT("appIDValue"))				JSON_PAIR_VALUE(JSON_STRING(m_AppId)),
						JSON_PAIR_KEY(_YTEXT("appIDReadOnly"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("X"))),
						JSON_PAIR_KEY(_YTEXT("RedirectURLField"))		JSON_PAIR_VALUE(JSON_STRING(g_RedirectURLFieldName)),
						JSON_PAIR_KEY(_YTEXT("RedirectURLLabel"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_7, _YLOC("Your application redirect URL")).c_str()))),
						JSON_PAIR_KEY(_YTEXT("RedirectURLPlaceholder"))	JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_8, _YLOC("http://localhost:33366")).c_str())),
						JSON_PAIR_KEY(_YTEXT("RedirectURLPopup"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT(""))),
						JSON_PAIR_KEY(_YTEXT("RedirectURLValue"))		JSON_PAIR_VALUE(JSON_STRING(m_RedirectURL))
                    JSON_END_OBJECT,
                    JSON_BEGIN_OBJECT
                        JSON_BEGIN_PAIR
                            _YTEXT("hbs"),
                            JSON_STRING(_YTEXT("Button"))
                        JSON_END_PAIR,
                        JSON_BEGIN_PAIR
                            _YTEXT("choices"),
                            JSON_BEGIN_ARRAY
                                JSON_BEGIN_OBJECT
                                    JSON_PAIR_KEY(_YTEXT("type"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
									JSON_PAIR_KEY(_YTEXT("name"))		JSON_PAIR_VALUE(JSON_STRING(g_OKButtonName)),
                                    JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_9, _YLOC("OK")).c_str()))),
                                    JSON_PAIR_KEY(_YTEXT("attribute"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("post"))),
                                    JSON_PAIR_KEY(_YTEXT("class"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("is-primary")))
                                JSON_END_OBJECT,
                                JSON_BEGIN_OBJECT
                                    JSON_PAIR_KEY(_YTEXT("type"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
									JSON_PAIR_KEY(_YTEXT("name"))		JSON_PAIR_VALUE(JSON_STRING(g_CancelButtonName)),
                                    JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_10, _YLOC("CANCEL")).c_str()))),
                                    JSON_PAIR_KEY(_YTEXT("attribute"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("cancel")))
                                JSON_END_OBJECT
                            JSON_END_ARRAY
                        JSON_END_PAIR
                    JSON_END_OBJECT
                JSON_END_ARRAY
            JSON_END_PAIR
        JSON_END_OBJECT;
            
    p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}
