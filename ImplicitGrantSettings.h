#pragma once

class ImplicitGrantSettings
{
public:
	boost::YOpt<bool> m_EnableIdTokenIssuance;
	boost::YOpt<bool> m_EnableAccessTokenIssuance;
};

