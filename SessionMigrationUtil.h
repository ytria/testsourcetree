#pragma once

#include "IMigrationVersion.h"

class SessionMigrationUtil
{
public:
	static bool CopyDatabase(const VersionSourceTarget& p_SourceTarget);
};