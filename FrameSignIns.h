#pragma once

#include "GridFrameBase.h"
#include "GridSignIns.h"
#include "SignIn.h"
#include "SignInsRequester.h"

class FrameSignIns : public GridFrameBase
{
public:
	DECLARE_DYNAMIC(FrameSignIns)
	FrameSignIns(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
	virtual ~FrameSignIns() = default;

	void BuildView(const vector<SignIn>& p_SignIns, bool p_FullPurge, bool p_NoPurge);

	O365Grid& GetGrid() override;

	void RefreshSpecific(AutomationAction* p_CurrentAction) override;
	void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;
	bool HasApplyButton() override;

protected:
	virtual void createGrid() override;

	/*O365ControlConfig GetRefreshPartialControlConfig() override;*/

	// returns false if no frame specific tab is needed.
	bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	GridSignIns m_Grid;
};

