#pragma once

#include "JsonObjectDeserializer.h"
#include "CosmosDbAccountKeys.h"
#include "Encapsulate.h"

namespace Azure
{
	class CosmosDbAccountKeyDeserializer : public JsonObjectDeserializer, public Encapsulate<Azure::CosmosDbAccountKeys>
	{
	public:
		void DeserializeObject(const web::json::object& p_Object) override;
	};
}

