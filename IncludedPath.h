#pragma once

namespace Cosmos
{
    class IncludedPath
    {
    public:
        boost::YOpt<PooledString> Path;
        boost::YOpt<PooledString> DataType;
        boost::YOpt<PooledString> Kind;
        boost::YOpt<PooledString> Precision;
    };
}
