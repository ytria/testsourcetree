#include "CreatedObjectModification.h"

#include "ActivityLoggerUtil.h"
#include "BaseO365Grid.h"

CreatedObjectModification::CreatedObjectModification(O365Grid& p_Grid, const row_pk_t& p_RowKey)
    : WithSiblings<RowModification, CreatedObjectModification>(p_Grid, p_RowKey)
	, m_ShouldRemoveRowOnRevertError(false)
{
	GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		getGrid().OnRowCreated(row, false); // Display the "Row Created" icon.
		m_FieldsBackup = row->GetFields();

		if (nullptr != row->GetParentRow())
			getGrid().GetRowPK(row->GetParentRow(), m_ParentPKBackup);
	}
}

CreatedObjectModification::CreatedObjectModification(CreatedObjectModification& p_Other, const row_pk_t& p_NewRowKey)
	: WithSiblings<RowModification, CreatedObjectModification>(p_Other.getGrid(), p_NewRowKey)
	, m_FieldsBackup(p_Other.m_FieldsBackup)
	, m_ParentPKBackup(p_Other.m_ParentPKBackup)
	, m_ShouldRemoveRowOnRevertError(p_Other.m_ShouldRemoveRowOnRevertError)
{
	SetObjectName(p_Other.GetObjectName());
	for (auto& f : p_NewRowKey)
	{
		auto it = m_FieldsBackup.find(f.GetColID());
		if (m_FieldsBackup.end() != it)
			it->second = f;
	}
}

CreatedObjectModification::~CreatedObjectModification()
{
	if (shouldRevert())
	{
		if (GetState() == Modification::State::AppliedLocally || m_ShouldRemoveRowOnRevertError && GetState() == Modification::State::RemoteError)
		{
			GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

			ASSERT(nullptr != row);
			if (nullptr != row)
				getGrid().RemoveRow(row);
		}
		else
		{
			GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

			ASSERT(nullptr != row);
			if (nullptr != row)
				getGrid().OnRowNothingToSave(row, false); // Remove all status flags from the row.
		}
	}
}

void CreatedObjectModification::RefreshState()
{
	GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

	if (nullptr != row)
	{
		if (row->IsError())
		{
			m_State = IMainItemModification::State::RemoteError;
		}
		else
		{
			// FIXME: Check fields?

			m_State = IMainItemModification::State::RemoteHasNewValue;
			getGrid().OnRowSaved(row, false, YtriaTranslate::Do(CreatedObjectModification_RefreshState_1, _YLOC("Creation has been commited successfully.")).c_str());
		}
	}
	else
	{
		m_State = IMainItemModification::State::RemoteHasOldValue;

		if (nullptr == row)
			row = RestoreRow();

		ASSERT(nullptr != row);
		getGrid().OnRowSavedNotReceived(row, false, YtriaTranslate::Do(CreatedObjectModification_RefreshState_2, _YLOC("Creation has not been applied yet.")).c_str());
	}

	for (auto& sib : m_SiblingModifications)
		sib->RefreshState();
}

GridBackendRow* CreatedObjectModification::RestoreRow(GridBackendRow* p_ExistingRowToRestore)
{
	ASSERT(nullptr != p_ExistingRowToRestore || nullptr == getGrid().GetRowIndex().GetExistingRow(GetRowKey()));
	GridBackendRow* row = getGrid().GetRowIndex().GetRow(GetRowKey(), true, false);

	if (nullptr != row)
	{
		ASSERT(nullptr == p_ExistingRowToRestore || p_ExistingRowToRestore == row);
		row->AddFields(m_FieldsBackup);

		// Do NOT clear status, we want to keep the Created flag so that next edit is still a creation.
		//getGrid().OnRowNothingToSave(row, false);

		if (nullptr == row->GetParentRow())
		{
			if (!m_ParentPKBackup.empty())
			{
				GridBackendRow* parentRow = getGrid().GetRowIndex().GetExistingRow(m_ParentPKBackup);
				ASSERT(nullptr != parentRow);
				if (nullptr != parentRow)
					row->SetParentRow(parentRow);
			}
		}
		else
		{
			ASSERT(!m_ParentPKBackup.empty() && row->GetParentRow() == getGrid().GetRowIndex().GetExistingRow(m_ParentPKBackup));
		}
	}

	for (auto& sib : m_SiblingModifications)
		sib->RestoreRow(nullptr);

	return row;
}

void CreatedObjectModification::SetNewPK(const row_pk_t& p_NewRowKey)
{
	// Case with sibling mods is not handled!
	ASSERT(m_SiblingModifications.empty());

	const bool shouldRev = shouldRevert();

	CreatedObjectModification newMod(*this, p_NewRowKey);

	*this = newMod;
	if (!shouldRev)
		DontRevertOnDestruction();

	newMod.DontRevertOnDestruction();
}

CreatedObjectModification& CreatedObjectModification::operator=(const CreatedObjectModification& p_Other)
{
	// Case with sibling mods is not handled!
	ASSERT(m_SiblingModifications.empty());

	this->RowModification::operator=(p_Other);

	m_FieldsBackup		= p_Other.m_FieldsBackup;
	m_ParentPKBackup	= p_Other.m_ParentPKBackup;

	return *this;
}

vector<Modification::ModificationLog> CreatedObjectModification::GetModificationLogs() const
{
	vector<wstring> values;
	for (const auto& item : m_FieldsBackup)
	{
		if (item.second.HasValue())
		{
			auto col = getGrid().GetColumnByID(item.first);
			ASSERT(nullptr != col);
			const auto& title = nullptr != col ? col->GetTitleHierarchy() : ModificationLog::g_NotSet;
			const auto v = item.second.ToString();
			if (!MFCUtil::StringMatch(GridBackendUtil::g_NoValueString.c_str(), v.c_str()))
				values.push_back(_YTEXTFORMAT(L"%s: %s", title.c_str(), v.c_str()));
		}
	}

	return { ModificationLog(GridBackendUtil::ToString(GetRowKey()), GetObjectName(), ActivityLoggerUtil::g_ActionCreate, getGrid().GetAutomationName(), values, GetState()) };
}

void CreatedObjectModification::SetShouldRemoveRowOnRevertError(bool p_Val)
{
	m_ShouldRemoveRowOnRevertError = p_Val;
}
