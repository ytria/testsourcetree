#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "ItemReference.h"

class ItemReferenceDeserializer : public JsonObjectDeserializer, public Encapsulate<ItemReference>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

