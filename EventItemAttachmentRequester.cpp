#include "EventItemAttachmentRequester.h"

#include "BusinessItemAttachment.h"
#include "ItemAttachmentDeserializer.h"
#include "MSGraphSession.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

EventItemAttachmentRequester::EventItemAttachmentRequester(EntityType p_Type, PooledString p_EntityId, PooledString p_EventId, PooledString p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger)
    : m_Type(p_Type),
    m_EntityId(p_EntityId),
    m_EventId(p_EventId),
    m_AttachmentId(p_AttachmentId),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> EventItemAttachmentRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ItemAttachmentDeserializer>();

    web::uri_builder uri;
    std::shared_ptr<MsGraphHttpRequestLogger> httpLogger;
    if (m_Type == EntityType::User)
    {
        uri.append_path(_YTEXT("users"));
	    httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
    }
    else if (m_Type == EntityType::Group)
    {
        uri.append_path(_YTEXT("groups"));
		httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
    }

    uri.append_path(m_EntityId);
    uri.append_path(_YTEXT("events"));
    uri.append_path(m_EventId);
    uri.append_path(_YTEXT("attachments"));
    uri.append_path(m_AttachmentId);
    uri.append_query(_YTEXT("$expand"), _YTEXT("microsoft.graph.ItemAttachment/Item"));

    return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, uri.to_uri(), false, m_Logger, httpLogger, p_TaskData);
}

const BusinessItemAttachment& EventItemAttachmentRequester::GetData() const
{
    return m_Deserializer->GetData();
}
