#include "DlgProtectedApiHTML.h"

#include "ApplicationListRequester.h"
#include "BasicRequestLogger.h"

DlgProtectedApiHTML::DlgProtectedApiHTML(const std::shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::READ, p_Parent)
	, m_Session(p_Session)
	, currentQuestionIndex(0)
{

}

void DlgProtectedApiHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgProtectedApiHTML"));

	getGenerator().addIconTextField(_YTEXT("text1"), MFCUtil::encodeToHtmlEntitiesExceptTags(_T("Request access to private channel chats that are otherwise not accessible to admins.")), _YTEXT("fas fa-exclamation"), _YTEXT(""), _YTEXT("#ff0000"));
	getGenerator().addIconTextField(_YTEXT("text2"), MFCUtil::encodeToHtmlEntitiesExceptTags(_T("The only way to read private channel chats is to be a member of that channel. Alternatively, you can use the current application to access them.\nAccessing chats in Team's private channel chats requires using Protected APIs from Microsoft.\nUse the link below to request access from Microsoft.")), _YTEXT(""), _YTEXT(""), _YTEXT(""));
	getGenerator().addIconTextField(_YTEXT("text2-1"), _T("<b><a href=\"https://aka.ms/teamsgraph/requestaccess\">Request access from Microsoft</a></b>"), _YTEXT("far fa-hand-point-right"), _YTEXT(""), _YTEXT(""));

	getGenerator().addIconTextField(_YTEXT("text3"), MFCUtil::encodeToHtmlEntitiesExceptTags(_T("To make things easier, we've prepared information you'll need and some sample text you can use to fill out the form.")), _YTEXT("fas fa-info"), _YTEXT(""), _YTEXT(""));

	const wstring appClientId = m_Session->GetGateways().GetUltraAdminAppClientId();
	wstring publisherDomain;
	wstring appName;
	wstring tenantId;

	{
		CWaitCursor _;
		Application foundApp = ApplicationListRequester::GetAppFromAppId(m_Session, appClientId);
		ASSERT(foundApp.m_Id);
		if (foundApp.m_Id)
		{
			if (foundApp.m_PublisherDomain)
				publisherDomain = *foundApp.m_PublisherDomain;
			if (foundApp.m_DisplayName)
				appName = *foundApp.m_DisplayName;
		}
		else
		{
			// FIXME: show error?
		}
		{
			const auto& org = m_Session->GetGraphCache().GetCachedOrganization();
			ASSERT(org);
			if (org)
				tenantId = org->m_Id;
		}
	}
	
	addQuestion(_T("Your email address and any others you want to list as an owner (semicolon separated)"));
	addAnswer(_T("Your email"), m_Session->GetEmailOrAppId());

	addQuestion(_T("May we contact you about your app's use of non-protected APIs?"));
	addAnswer(_T("Suggested answer"), _T("Yes"));

	addQuestion(_T("Publisher name"));
	addAnswer(_T("Your app's Publisher domain"), publisherDomain);

	addQuestion(_T("App name"));
	addAnswer(_T("Your app's name"), appName);

	addQuestion(_T("App id(s) to enable application permissions for"));
	addAnswer(_T("Your app's ID"), appClientId);

	addQuestion(_T("What does your app do?"));
	addAnswer(_T("Suggested answer"), _T("Locally installed app that gives admins access to all Office 365 raw data, much the same way PowerShell does, using a single interface that allows data manipulation, bulk actions and reporting. The app makes use of both delegated and app permissions when required."));

	addQuestion(_T("Why does your app need read access to all messages in the tenant?"));
	addAnswer(_T("Suggested answer"), _T("This app is an all access tool for global admins and company policy requires that all private channel chats are accessible without introducing changes to SharePoint permissions."));

	addQuestion(_T("Data retention - select one of these options:"));
	addAnswer(_T("Select"), _T("This app will not make a copy of Microsoft Teams messages.\n<i>The only data sapio365 retains is User and Group data cached on your local machine to improve loading times.</i>"));

	addQuestion(_T("What are the tenant ID's that this app needs to run in?"));
	addAnswer(_T("Your tenant ID"), tenantId);

	addQuestion(_T("Does your organization own all those tenants?"));
	addAnswer(_T("Select"), _T("Yes"));

	getGenerator().addIconTextField(_YTEXT("text4"), MFCUtil::encodeToHtmlEntitiesExceptTags(_T("<i>Questions 11 to 14 are optional.</i>")), _YTEXT("fas fa-info"), _YTEXT(""), _YTEXT(""));

	getGenerator().addCloseButton(_T("Close"));
}

bool DlgProtectedApiHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	return true;
}

wstring DlgProtectedApiHTML::getDialogTitle() const
{
	return _T("Request Access");
}

web::json::value& DlgProtectedApiHTML::addQuestion(const wstring& p_Text)
{
	++currentQuestionIndex;

	return getGenerator().addIconTextField(_YTEXTFORMAT(L"q%s", Str::getStringFromNumber(currentQuestionIndex).c_str()), MFCUtil::encodeToHtmlEntitiesExceptTags(_YTEXTFORMAT(L"<b>%s. %s</b>", Str::getStringFromNumber(currentQuestionIndex).c_str(), p_Text.c_str())), _YTEXT(""), _YTEXT(""), _YTEXT(""));
}

web::json::value& DlgProtectedApiHTML::addAnswer(const wstring& p_Label, const wstring& p_Text)
{
	return getGenerator().addIconTextField(_YTEXTFORMAT(L"q%s-a", Str::getStringFromNumber(currentQuestionIndex).c_str()), MFCUtil::encodeToHtmlEntitiesExceptTags(p_Text), p_Label, _YTEXT("far fa-chevron-square-right"), GridBackendUtil::g_NoColor);
}
