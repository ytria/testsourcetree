#pragma once

#include "IDlgSpecialExtAdminJsonGenerator.h"

class Sapio365Session;

class DlgSpecialExtAdminNewElevatedJsonGenerator : public IDlgSpecialExtAdminJsonGenerator
{
public:
	DlgSpecialExtAdminNewElevatedJsonGenerator(const std::shared_ptr<Sapio365Session>& p_Session);
	web::json::value Generate(const DlgSpecialExtAdminAccess& p_Dlg) override;

private:
	std::shared_ptr<Sapio365Session> m_Session;
};

