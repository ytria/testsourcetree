#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "InformationalUrl.h"

class InformationalUrlDeserializer : public JsonObjectDeserializer, public Encapsulate<InformationalUrl>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

