#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "KeyValue.h"

class KeyValueDeserializer : public JsonObjectDeserializer, public Encapsulate<KeyValue>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

