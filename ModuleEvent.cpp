#include "ModuleEvent.h"

#include "AutomationDataStructure.h"
#include "BusinessObjectManager.h"
#include "CommandInfo.h"
#include "FrameEvents.h"
#include "GraphCache.h"
#include "ModuleUtil.h"
#include "O365AdminUtil.h"
#include "TaskDataManager.h"
#include "YCallbackMessage.h"
#include "YDataCallbackMessage.h"
#include "ModuleOptions.h"
#include "DlgEventsModuleOptions.h"
#include "MultiObjectsRequestLogger.h"
#include "UpdatedObjectsGenerator.h"
#include "YSafeCreateTask.h"

using namespace Util;

void ModuleEvent::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
    case Command::ModuleTask::ListMe:
    {
        Command newCmd = p_Command;
        newCmd.GetCommandInfo().GetIds() = { PooledString(GetConnectedSession().GetConnectedUserId()) };
        showEvents(newCmd);
        break;
    }
	case Command::ModuleTask::List:
		showEvents(p_Command);
		break;
    case Command::ModuleTask::ListAttachments:
		showAttachments(p_Command);
		break;
    case Command::ModuleTask::DownloadAttachments:
		downloadAttachments(p_Command);
	    break;
    case Command::ModuleTask::ViewItemAttachment:
		showItemAttachment(p_Command);
	    break;
    case Command::ModuleTask::DeleteAttachment:
		deleteAttachments(p_Command);
	    break;
	case Command::ModuleTask::UpdateModified:
		update(p_Command);
		break;
	case Command::ModuleTask::LoadSnapshot:
		loadSnapshot(p_Command);
		break;
	default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
		break;
	}
}

void ModuleEvent::showEvents(Command p_Command)
{
    CommandInfo info = p_Command.GetCommandInfo();

	ASSERT(info.GetOrigin() != Origin::NotSet);
	if (info.GetOrigin() != Origin::NotSet)
	{
		const auto&	p_IDs			= info.GetIds();
		const auto	p_Origin		= info.GetOrigin();
		auto		p_ExistingFrame	= dynamic_cast<FrameEvents*>(info.GetFrame());
		auto		p_SourceWindow	= p_Command.GetSourceWindow();
		auto		p_Action		= p_Command.GetAutomationAction();

		auto sourceCWnd = p_SourceWindow.GetCWnd();

		FrameEvents* frame = p_ExistingFrame;
		if (nullptr == frame && FrameEvents::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin				= p_Origin;
			moduleCriteria.m_UsedContainer		= ModuleCriteria::UsedContainer::IDS;
			moduleCriteria.m_IDs				= p_IDs;
			moduleCriteria.m_Privilege			= info.GetRBACPrivilege();
			moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

			if (ShouldCreateFrame<FrameEvents>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				DlgEventsModuleOptions opt(p_SourceWindow.GetCWnd(), Sapio365Session::Find(p_Command.GetSessionIdentifier()));
				if (IDCANCEL == opt.DoModal())
				{
					if (nullptr != p_Action)
						SetActionCanceledByUser(p_Action);
					return;
				}
				p_Command.GetCommandInfo().Data2() = opt.GetOptions();

				CWaitCursor _;

				wstring title = p_Command.GetTask() == Command::ModuleTask::ListMe
					? _T("My Events")
					: YtriaTranslate::Do(ModuleEvent_showEvents_1, _YLOC("Events")).c_str();
				frame = new FrameEvents(p_Origin, p_Command.GetTask() == Command::ModuleTask::ListMe, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), title, *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
		}

		if (nullptr != frame)
		{
			if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
				frame->SetOptions(p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>());
			const bool isUpdate = nullptr != p_ExistingFrame;
			auto taskData = addFrameTask(YtriaTranslate::Do(ModuleEvent_showEvents_2, _YLOC("Show Events")).c_str(), frame, p_Command, !isUpdate);

            GridEvents* gridEvents = dynamic_cast<GridEvents*>(&frame->GetGrid());
            std::vector<GridBackendRow*> rowsForLoadMore;

            RefreshSpecificData refreshSpecificData;
			if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
				refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();
			else
				gridEvents->GetRowsWithMoreLoaded(rowsForLoadMore);
            
            if (refreshSpecificData.m_SelectedRowsAncestors && !refreshSpecificData.m_SelectedRowsAncestors->empty())
            {
				ASSERT(rowsForLoadMore.empty());
                gridEvents->GetRowsByCriteria(rowsForLoadMore, [&rowAncestors = *refreshSpecificData.m_SelectedRowsAncestors](GridBackendRow* row) {
                    return row->IsMoreLoaded() && rowAncestors.find(row->GetTopAncestorOrThis()) != rowAncestors.end();
                });
            }

			const auto& wantedIds = isUpdate && p_IDs.empty() ? frame->GetModuleCriteria().m_IDs : p_IDs;

			YSafeCreateTask([this, hwnd = frame->GetSafeHwnd(), currentModuleCriteria = frame->GetModuleCriteria(), isUpdate, refreshSpecificData, p_Action, p_Origin, wantedIds, taskData, bom = frame->GetBusinessObjectManager(), attachmentsRequestInfo = ModuleUtil::SetToVector(gridEvents->GetAttachmentsRequestInfo(rowsForLoadMore, {}, false)), wantsAdditionalMetadata = (bool)frame->GetMetaDataColumnInfo(), p_Command]()
			{
                // Get events
                const bool partialRefresh = !refreshSpecificData.m_Criteria.m_IDs.empty();
				const auto& ids = partialRefresh ? refreshSpecificData.m_Criteria.m_IDs : wantedIds;

				ModuleOptions moduleOptions;
				if (p_Command.GetCommandInfo().Data2().is_type<ModuleOptions>())
					moduleOptions = p_Command.GetCommandInfo().Data2().get_value<ModuleOptions>();

				if (p_Origin == Origin::User || p_Origin == Origin::DeletedUser)
				{
					map<BusinessUser, std::shared_ptr<EventListRequester>> requesters;
					const auto events = retrieveUserEvents(bom, ids, p_Origin, wantsAdditionalMetadata, moduleOptions, requesters, taskData);

					// Get attachments
					ModuleBase::AttachmentsContainer attachments;
					bool showAttachments = false;
					if (!attachmentsRequestInfo.empty() && !taskData.IsCanceled())
					{
						attachments = retrieveAttachments(bom, attachmentsRequestInfo, p_Origin, taskData);
						showAttachments = true;
					}

					YDataCallbackMessage<O365DataMap<BusinessUser, vector<BusinessEvent>>>::DoPost(events, [requesters, hwnd, partialRefresh, p_Origin, taskData, p_Action, attachments, showAttachments, isUpdate, isCanceled = taskData.IsCanceled()](const O365DataMap<BusinessUser, vector<BusinessEvent>>& p_Events)
					{
						bool shouldFinishTask = true;
						auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameEvents*>(CWnd::FromHandle(hwnd)) : nullptr;
						if (ShouldBuildView(hwnd, isCanceled, taskData, false))
						{
							ASSERT(nullptr != frame);

							frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting events for %s users...", Str::getStringFromNumber(p_Events.size()).c_str()));
							frame->GetGrid().ClearLog(taskData.GetId());
							//frame->GetGrid().ClearStatus();

							{
								CWaitCursor _;
								if (frame->HasLastUpdateOperations())
								{
									if (showAttachments)
										frame->ShowEventsWithAttachments(p_Events, frame->GetLastUpdateOperations(), attachments, true, !showAttachments && !partialRefresh);
									else
										frame->ShowEvents(p_Events, frame->GetLastUpdateOperations(), !showAttachments, !showAttachments && !partialRefresh, false);
									frame->ForgetLastUpdateOperations();
								}
								else
								{
									if (showAttachments)
										frame->ShowEventsWithAttachments(p_Events, {}, attachments, true, !showAttachments && !partialRefresh);
									else
										frame->ShowEvents(p_Events, {}, !showAttachments, !showAttachments && !partialRefresh, false);
								}
							}
							
							if (!isUpdate)
							{
								frame->UpdateContext(taskData, hwnd);
								shouldFinishTask = false;
							}
						}

						if (shouldFinishTask)
						{
							// Because we didn't (and can't) call UpdateContext here.
							ModuleBase::TaskFinished(frame, taskData, p_Action);
						}
					});
				}
				else if (p_Origin == Origin::Group)
				{
					map<BusinessGroup, std::shared_ptr<EventListRequester>> requesters;
					const auto events = retrieveGroupEvents(bom, ids, wantsAdditionalMetadata, moduleOptions, requesters, taskData);
					
					// Get attachments
					ModuleBase::AttachmentsContainer attachments;
					bool showAttachments = false;
					if (!attachmentsRequestInfo.empty())
					{
						attachments = retrieveAttachments(bom, attachmentsRequestInfo, p_Origin, taskData);
						showAttachments = true;
					}

					YDataCallbackMessage<O365DataMap<BusinessGroup, vector<BusinessEvent>>>::DoPost(events, [requesters, hwnd, partialRefresh, p_Origin, taskData, p_Action, attachments, showAttachments, isUpdate, isCanceled = taskData.IsCanceled()](const O365DataMap<BusinessGroup, vector<BusinessEvent>>& p_Events)
					{
						bool shouldFinishTask = true;
						auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameEvents*>(CWnd::FromHandle(hwnd)) : nullptr;
						if (ShouldBuildView(hwnd, isCanceled, taskData, false))
						{
							ASSERT(nullptr != frame);

							frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting events for %s groups...", Str::getStringFromNumber(p_Events.size()).c_str()));
							frame->GetGrid().ClearLog(taskData.GetId());
							//frame->GetGrid().ClearStatus();

							{
								CWaitCursor _;
								if (frame->HasLastUpdateOperations())
								{
									if (showAttachments)
										frame->ShowEventsWithAttachments(p_Events, frame->GetLastUpdateOperations(), attachments, true, !showAttachments && !partialRefresh);
									else
										frame->ShowEvents(p_Events, frame->GetLastUpdateOperations(), !showAttachments, !showAttachments && !partialRefresh, false);
									frame->ForgetLastUpdateOperations();
								}
								else
								{
									if (showAttachments)
										frame->ShowEventsWithAttachments(p_Events, {}, attachments, true, !showAttachments && !partialRefresh);
									else
										frame->ShowEvents(p_Events, {}, !showAttachments, !showAttachments && !partialRefresh, false);
								}
							}

							if (!isUpdate)
							{
								frame->UpdateContext(taskData, hwnd);
								shouldFinishTask = false;
							}
						}

						if (shouldFinishTask)
						{
							// Because we didn't (and can't) call UpdateContext here.
							ModuleBase::TaskFinished(frame, taskData, p_Action);
						}
					});
				}
				else
				{
					ASSERT(false);
				}			
			});
		}
	}
	else
	{
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleContact_showContacts_3, _YLOC(" - origin not set"),_YR("Y2471")).c_str());
	}
}

O365DataMap<BusinessUser, vector<BusinessEvent>> ModuleEvent::retrieveUserEvents(std::shared_ptr<BusinessObjectManager> p_BOM, const O365IdsContainer& p_IDs, Origin p_Origin, bool p_LoadUsersFromSQlCache, const ModuleOptions& p_Options, map<BusinessUser, std::shared_ptr<EventListRequester>>& p_Requesters, YtriaTaskData taskData)
{
	ASSERT(Origin::User == p_Origin || Origin::DeletedUser == p_Origin);
	ASSERT(!p_LoadUsersFromSQlCache || Origin::User == p_Origin);

	O365DataMap<BusinessUser, vector<BusinessEvent>> events;

	auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("user events"), p_IDs.size());

	auto processUser = [&taskData, &events, &p_BOM, &p_Options, &p_Requesters, &logger](auto& user)
	{
		vector<BusinessEvent> userEvents;
		if (!taskData.IsCanceled())
		{
			userEvents = p_BOM->GetUserBusinessEvents(user.GetID(), p_Options, p_Requesters, logger, taskData).GetTask().get();
		}
		if (taskData.IsCanceled())
		{
			user.SetFlags(user.GetFlags() | BusinessObject::Flag::CANCELED);
			events[user] = {};
		}
		else
		{
			events[user] = userEvents;
		}
	};

	Util::ProcessSubUserItems(processUser, p_IDs, p_Origin, p_BOM->GetGraphCache(), p_LoadUsersFromSQlCache, logger, taskData);
   
	return events;
}

O365DataMap<BusinessGroup, vector<BusinessEvent>> ModuleEvent::retrieveGroupEvents(std::shared_ptr<BusinessObjectManager> p_BOM, const O365IdsContainer& p_IDs, bool p_LoadGroupsFromSQlCache, const ModuleOptions& p_Options, map<BusinessGroup, std::shared_ptr<EventListRequester>>& p_Requesters, YtriaTaskData taskData)
{
	O365DataMap<BusinessGroup, vector<BusinessEvent>> events;

	vector<BusinessGroup> groups;
	if (p_LoadGroupsFromSQlCache)
	{
		// Do not pass taskData if we don't want to allow cancellation of this step.
		groups = p_BOM->GetGraphCache().GetSqlCachedGroups(taskData.GetOriginator(), p_IDs, false, taskData);
	}

	auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("group events"), p_IDs.size());
	auto processGroup = [&taskData, &events, &p_BOM, &p_Options, &p_Requesters, &logger](auto& group)
	{
		logger->IncrementObjCount();
		logger->SetContextualInfo(GetDisplayNameOrId(group));

		vector<BusinessEvent> groupEvents;
		if (!taskData.IsCanceled())
		{
			groupEvents = p_BOM->GetGroupBusinessEvents(group.GetID(), p_Options, p_Requesters, logger, taskData).GetTask().get();
		}
		if (taskData.IsCanceled())
		{
			group.SetFlags(group.GetFlags() | BusinessObject::Flag::CANCELED);
			events[group] = {};
		}
		else
		{
			events[group] = groupEvents;
		}
	};

	auto ids = p_IDs;
	for (auto& group : groups)
	{
		if (1 == ids.erase(group.GetID()))
			processGroup(group);
	}

	ASSERT(!p_LoadGroupsFromSQlCache || ids.empty()); // Is it normal not all groups are in SQL cache?

	for (const auto& id : ids)
	{
		BusinessGroup group;
		group.SetID(id);
		logger->SetContextualInfo(p_BOM->GetGraphCache().GetGroupContextualInfo(id));
		p_BOM->GetGraphCache().SyncUncachedGroup(group, std::make_shared<MultiObjectsRequestLogger>(*logger), taskData);

		processGroup(group);
	}
	return events;
}

void ModuleEvent::showAttachments(const Command& p_Command)
{
	CommandInfo info = p_Command.GetCommandInfo();

	if (info.Data().is_type<std::set<AttachmentsInfo::IDs>>())
	{
		auto&	p_IDs	= info.Data().get_value<std::set<AttachmentsInfo::IDs>>();
		auto	p_Frame	= dynamic_cast<FrameEvents*>(info.GetFrame());
		ModuleBase::showAttachments(p_Frame->GetBusinessObjectManager(), ModuleUtil::SetToVector(p_IDs), p_Frame, p_Frame->GetOrigin(), p_Command);
	}
	else
	{
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2472")).c_str());
	}
}

void ModuleEvent::downloadAttachments(const Command& p_Command)
{
    CommandInfo info = p_Command.GetCommandInfo();
    if (info.Data().is_type<AttachmentsInfo>())
    {
        auto&	p_AttachmentInfos = info.Data().get_value<AttachmentsInfo>();
        auto	existingFrame = dynamic_cast<FrameEvents*>(info.GetFrame());
        ASSERT(nullptr != existingFrame);
        if (nullptr != existingFrame)
            ModuleBase::downloadAttachments(existingFrame->GetBusinessObjectManager(), p_AttachmentInfos, existingFrame, existingFrame->GetOrigin(), p_Command);
        else
            SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleDrive_viewPermissions_2, _YLOC(" - no frame"),_YR("Y2500")).c_str());
    }
    else
    {
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2473")).c_str());
    }
}

void ModuleEvent::showItemAttachment(const Command& p_Command)
{
	CommandInfo info = p_Command.GetCommandInfo();
	ASSERT(info.Data().is_type<AttachmentsInfo::IDs>());
	if (info.Data().is_type<AttachmentsInfo::IDs>())
	{
		auto& p_ItemAttachment	= info.Data().get_value<AttachmentsInfo::IDs>();
		auto p_Origin			= info.GetOrigin();
		auto p_Frame			= dynamic_cast<FrameEvents*>(info.GetFrame());
		ModuleBase::showItemAttachment(p_Frame->GetBusinessObjectManager(), p_ItemAttachment, p_Origin, p_Frame, p_Command);
	}
	else
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2474")).c_str());
}

void ModuleEvent::deleteAttachments(const Command& p_Command)
{
    CommandInfo info = p_Command.GetCommandInfo();
    AutomationAction* action = p_Command.GetAutomationAction();
    FrameEvents* frame = dynamic_cast<FrameEvents*>(info.GetFrame());

    GridEvents* gridEvents = dynamic_cast<GridEvents*>(&frame->GetGrid());
    std::vector<GridBackendRow*> rowsWithMoreLoaded;
    gridEvents->GetRowsWithMoreLoaded(rowsWithMoreLoaded);

    ASSERT(info.Data().is_type<std::vector<SubItemKey>>());
    if (info.Data().is_type<std::vector<SubItemKey>>())
    {
        std::vector<SubItemKey> attachments = info.Data().get_value<std::vector<SubItemKey>>();

		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleEvent_deleteAttachments_1, _YLOC("Delete Attachments")).c_str(), frame, p_Command, false);
        RefreshSpecificData refreshData = frame->GetRefreshSelectedData();
		YSafeCreateTask([this, attachments, refreshData, frame, taskData, action, gridEvents, rowsWithMoreLoaded, info]()
		{
            std::map<SubItemKey, HttpResultWithError> results;

            // Delete attachments
            for (auto& attachment : attachments)
            {
                if (info.GetOrigin() == Origin::Group)
					results[attachment] = frame->GetBusinessObjectManager()->DeleteGroupEventAttachment(attachment.m_Id0, attachment.m_Id1, attachment.m_Id2, taskData).GetTask().get();
                else if (info.GetOrigin() == Origin::User)
                    results[attachment] = frame->GetBusinessObjectManager()->DeleteUserEventAttachment(attachment.m_Id0, attachment.m_Id1, attachment.m_Id2, taskData).GetTask().get();
            }

            YCallbackMessage::DoPost([frame, gridEvents, refreshData, action, taskData, results]()
            {
                if (::IsWindow(frame->GetSafeHwnd()))
                {
                    gridEvents->GetModifications().SetSubItemDeletionsErrors(results);
                    gridEvents->ClearLog(taskData.GetId());
                    frame->RefreshAfterSubItemUpdates(refreshData, action);
                }
                else
                {
                    SetAutomationGreenLight(action);
                }
                TaskDataManager::Get().RemoveTaskData(taskData.GetId(), frame->GetSafeHwnd());
            });
        });
    }
    else
    {
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2475")).c_str());
    }
}

void ModuleEvent::loadSnapshot(const Command& p_Command)
{
	LoadSnapshot<FrameEvents>(p_Command, [&p_Command](const GridSnapshot::Metadata& p_SnapshotMeta, const ModuleCriteria& p_ModuleCriteria, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
		{
			const bool isMyData = p_SnapshotMeta.m_Flags && GridSnapshot::Flags::MyData == (GridSnapshot::Flags::MyData & *p_SnapshotMeta.m_Flags);
			wstring title = isMyData
				? _T("My Events")
				: YtriaTranslate::Do(ModuleEvent_showEvents_1, _YLOC("Events")).c_str();
			return new FrameEvents(p_ModuleCriteria.m_Origin, isMyData, p_LicenseContext, p_SessionIdentifier, title, p_Module, p_HistoryMode, p_PreviousFrame);
		});
}

void ModuleEvent::update(const Command& p_Command)
{
	AutomationAction* action = p_Command.GetAutomationAction();

	using DataType = std::pair<UpdatedObjectsGenerator<BusinessEvent>, std::vector<SubItemKey>>;

	ASSERT(p_Command.GetCommandInfo().Data().is_type<DataType>());
	if (p_Command.GetCommandInfo().Data().is_type<DataType>())
	{
		auto updatedEventsGenerator = p_Command.GetCommandInfo().Data().get_value<DataType>().first;
		auto attachmentsToDelete = p_Command.GetCommandInfo().Data().get_value<DataType>().second;
		ASSERT(!updatedEventsGenerator.GetObjects().empty() || !attachmentsToDelete.empty());
		if (!updatedEventsGenerator.GetObjects().empty() || !attachmentsToDelete.empty())
		{
			// The frame to update once the data has been sent.
			GridFrameBase* existingFrame = dynamic_cast<GridFrameBase*>(p_Command.GetCommandInfo().GetFrame());
			ASSERT(nullptr != existingFrame);
			if (nullptr != existingFrame)
			{
				wstring taskTitle;
				if (updatedEventsGenerator.GetObjects().empty())
					taskTitle = YtriaTranslate::Do(ModuleEvent_deleteAttachments_1, _YLOC("Delete Attachments")).c_str();
				else if (attachmentsToDelete.empty())
					taskTitle = _T("Update Events");
				else
					taskTitle = _T("Update Events and Delete Attachments");

				auto gridEvents = dynamic_cast<GridEvents*>(&existingFrame->GetGrid());

				const bool isGroup = p_Command.GetCommandInfo().GetOrigin() == Origin::Group;
				ASSERT(isGroup || p_Command.GetCommandInfo().GetOrigin() == Origin::User);

				auto taskData = addFrameTask(taskTitle, existingFrame, p_Command, false);
				YSafeCreateTask([taskData, isGroup, hwnd = existingFrame->GetSafeHwnd(), metaIDColID = gridEvents->GetColMetaID()->GetID(), graphIDColID = gridEvents->GetColId()->GetID(), criteria = existingFrame->GetModuleCriteria(), bom = existingFrame->GetBusinessObjectManager(), updatedEventsGenerator, attachmentsToDelete, action]()
				{
					RefreshSpecificData refreshData;
					refreshData.m_Criteria = criteria;
					ASSERT(refreshData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::IDS);
					refreshData.m_Criteria.m_IDs.clear();

					std::vector<O365UpdateOperation> updateOperations;
					if (!updatedEventsGenerator.GetObjects().empty())
					{
						updateOperations = bom->WriteSingleRequest(updatedEventsGenerator.GetObjects(), updatedEventsGenerator.GetPrimaryKeys(), taskData);

						for (auto& updateOp : updateOperations)
						{
							auto userId = updateOp.GetPkFieldStr(metaIDColID);
							refreshData.m_Criteria.m_IDs.insert(userId);
						}
					}

					std::map<SubItemKey, HttpResultWithError> attachmentResults;
					// Delete attachments
					if (isGroup)
					{
						for (auto& attachment : attachmentsToDelete)
						{
							attachmentResults[attachment] = bom->DeleteGroupEventAttachment(attachment.m_Id0, attachment.m_Id1, attachment.m_Id2, taskData).GetTask().get();
							refreshData.m_Criteria.m_IDs.insert(attachment.m_Id0);
						}
					}
					else
					{
						for (auto& attachment : attachmentsToDelete)
						{
							attachmentResults[attachment] = bom->DeleteUserEventAttachment(attachment.m_Id0, attachment.m_Id1, attachment.m_Id2, taskData).GetTask().get();
							refreshData.m_Criteria.m_IDs.insert(attachment.m_Id0);
						}
					}

					YCallbackMessage::DoPost([hwnd, action, taskData, refreshData, updateOperations{ std::move(updateOperations) }, attachmentResults{ std::move(attachmentResults) }]()
					{
						if (::IsWindow(hwnd))
						{
							auto frame = dynamic_cast<FrameEvents*>(CWnd::FromHandle(hwnd));
							frame->GetGrid().ClearLog(taskData.GetId());
							frame->GetGrid().GetModifications().SetSubItemDeletionsErrors(attachmentResults);
							frame->RefreshAfterUpdate(vector<O365UpdateOperation>{ updateOperations }, refreshData, action);
						}
						else
						{
							SetAutomationGreenLight(action);
						}
						TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
					});
				});
			}
			else
				SetAutomationGreenLight(action, _YTEXT(""));
		}
		else
			SetAutomationGreenLight(action);
	}
	else
		SetAutomationGreenLight(action);
}