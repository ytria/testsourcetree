#pragma once

#include "IPSObjectCollectionDeserializer.h"

class ExchangeAuthenticationCollectionDeserializer : public IPSObjectCollectionDeserializer
{
public:
	ExchangeAuthenticationCollectionDeserializer();
	void Deserialize(IPSObjectPropertyReader& p_Reader) override;
	void SetCount(size_t p_Count) override;

	int64_t GetTokenExpiration() const;

private:
	int64_t m_TokenExpiration;
};

