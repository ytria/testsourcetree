#pragma once

#include "IPropertySetBuilder.h"

class UserFullPropertySet : public IPropertySetBuilder
{
public:
    virtual vector<rttr::property> GetPropertySet() const override;
};

