#pragma once

#include "IDeserializerFactory.h"
#include "EducationUserDeserializer.h"

class EducationUserDeserializerFactory : public IDeserializerFactory<EducationUserDeserializer>
{
public:
	EducationUserDeserializerFactory(std::shared_ptr<const Sapio365Session> p_Session);
	EducationUserDeserializer Create() override;

private:
	std::shared_ptr<const Sapio365Session>	m_Session;
};

