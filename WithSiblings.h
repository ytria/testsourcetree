#pragma once

#include "FieldUpdate.h" // for row_pk_t

template <class BaseModificationType, class ConcreteModificationType>
class WithSiblings : public BaseModificationType
{
public:
	using BaseModificationType::BaseModificationType;

	virtual bool IsCorrespondingRow(const row_pk_t& p_RowPk) const override final;
	void AddSiblingModification(std::unique_ptr<ConcreteModificationType> p_SiblingMod);

protected:
	std::deque<std::unique_ptr<ConcreteModificationType>> m_SiblingModifications;
};

#include "WithSiblings.hpp"
