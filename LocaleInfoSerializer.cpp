#include "LocaleInfoSerializer.h"

#include "JsonSerializeUtil.h"

LocaleInfoSerializer::LocaleInfoSerializer(const LocaleInfo& p_LocaleInfo)
    : m_LocaleInfo(p_LocaleInfo)
{
}

void LocaleInfoSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("locale"), m_LocaleInfo.Locale, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("displayName"), m_LocaleInfo.DisplayName, obj);
}
