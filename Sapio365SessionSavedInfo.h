#pragma once

#include "Group.h"
#include "Organization.h"
#include "RestCredentials.h"
#include "YTimeDate.h"

#include <string>
#include <vector>
#include "SessionStatus.h"
#include "RestCredentialsRegistryManager.h"

class IRestCredentialsManager;
class RestSession;
struct SessionInfo;

class Sapio365SessionSavedInfo
{
public:
    struct Role
    {
        Role();
		Role(int64_t Id);
        bool operator<(const Role& p_Other) const
        {
            return m_Id < p_Other.m_Id;
        }
        int64_t m_Id = 0;
		// Do not change both date order (see ctor)
        YTimeDate m_CreatedOn;
        YTimeDate m_LastUsedOn;
		///////////
        bool m_IsFavorite;
        bool m_ShowOwnScopeOnly;
        wstring m_Name;
        int32_t m_AccessCount;
    };

	static wstring GetApiNameForRegistry(const wstring& p_ApiName);
	static void CheckUpdateTo1dot4();

public:
    Sapio365SessionSavedInfo();
    Sapio365SessionSavedInfo(const SessionInfo& p_ListElement);
    Sapio365SessionSavedInfo(const wstring& p_EmailAddress, const wstring& p_FullName, const vector<uint8_t>& p_ProfilePhoto);

	static RestCredentialsRegistryManager& GetCredentialsManager();

	SessionStatus GetStatus(int64_t p_RoleId) const;

	static SessionStatus GetStatus(const wstring& p_StatusString);
	static const wstring& GetStatusString(SessionStatus p_Status);

	bool IsValid() const;
    wstring GetConnectedUserId() const;
    void SetConnectedUserId(const wstring& p_Id);
    
    void Save() const;

    const wstring& GetSessionName() const; // E-mail for basic and delegated user sessions, custom display name for Ultra Admin.
    const wstring& GetTechnicalSessionName() const; //E-mail for delegated, E-mail suffixed by @ for basic, app ID for Ultra Admin.

    const wstring& GetTenantDisplayName() const;
    const wstring& GetTenantName() const;
    const wstring& GetFullname() const;
    int32_t GetAccessCount() const;
    const YTimeDate& GetLastUsedOn() const;
    const YTimeDate& GetCreatedOn() const;
    const vector<uint8_t>& GetProfilePhoto() const;
    const wstring& GetSessionType() const;
    bool IsFullAdmin() const;
	bool IsAdmin() const;
	bool IsFavorite() const;
    bool GetShowBaseSession() const;
    int GetSessionTypeResourcePng(int64_t p_RoleId) const;
    int GetSessionTypeResourceBmp() const;
    wstring GetDisplayedSessionType() const;
	//const wstring& GetFullAdminRedirectURL() const;
	const std::set<Role>& GetRoles() const;
    std::set<Role>& GetRoles();

    void SetSessionName(const wstring& val); // E-mail for basic and delegated user sessions, custom display name for Ultra Admin.
    void SetFullname(const wstring& val);
    void SetAccessCount(int32_t val);
    void SetLastUsedOn(const YTimeDate& val);
    void SetCreatedOn(const YTimeDate& val);
    void SetProfilePhoto(const vector<uint8_t>& val);
    void SetTenant(const Organization& p_Organization);
    void SetTenantDisplayName(const wstring& p_TenantDisplayName);
    void SetTenantName(const wstring& p_TenantName);
    void SetSessionType(const wstring& p_SessionType);
	void SetFavorite(bool p_Favorite);
    void SetShowBaseSession(bool p_ShowBaseSession);
    void SetRoles(const std::set<Role>& p_Roles);

public:
    static std::pair<bool, Sapio365SessionSavedInfo> Load(const wstring& p_SessionTechnicalName);
    static void Delete(const wstring& p_SessionTechnicalName);
	static bool Exists(const wstring& p_SessionTechnicalName);
    
	static const vector<RestCredentials>& ListSessions(bool useLastRetrievedList = false);

    void SetTechnicalName(const wstring& p_TechnicalSessionName);

private:
    static wstring GetFilePath(const wstring& p_Filename);
	static wstring GetSessionFilesDirectoryPath();
	static wstring GetSessionFilesDirectoryPath_v1dot3();
	static wstring GetOldSessionFilesDirectoryPath();
	static void checkOldSessionDirectory();
	static Sapio365SessionSavedInfo initEmptySession();
	static wstring LoadStringKey(const web::json::value& p_Json, const wstring& p_Key);
    static wstring LoadStringKey(const web::json::value& p_Json, const wstring& p_Key, bool& p_Success);
    static int32_t LoadIntKey(const web::json::value& p_Json, const wstring& p_Key);
    static bool LoadBoolKey(const web::json::value& p_Json, const wstring& p_Key);
    static YTimeDate LoadTimeDateKey(const web::json::value& p_Json, const wstring& p_Key);
    static vector<uint8_t> LoadBinaryKey(const web::json::value& p_Json, const wstring& p_Key);
    static std::set<Sapio365SessionSavedInfo::Role> LoadRoles(const web::json::value& p_Json);
	static std::pair<bool, Sapio365SessionSavedInfo> LoadImpl(const wstring& p_SessionFilePath, bool p_DeleteIfError);

    bool IsInactive() const;

public:
	static const Sapio365SessionSavedInfo g_EmptySession;

private:
	static wstring g_InvalidStatus;
	static wstring g_ActiveStatus;
	static wstring g_LockedStatus;
	static wstring g_StandbyStatus;
	static wstring g_CachedStatus;
	static wstring g_InactiveStatus;

	static const wstring g_Roles;
	static const wstring g_RoleID;
    static const wstring g_RoleName;
	static const wstring g_RoleLastUsedOn;
	static const wstring g_RoleCreatedOn;
	static const wstring g_RoleIsFavorite;
	static const wstring g_RoleAccessCount;
    static const wstring g_RoleShowOwnScopeOnly;

	static void initStatusStrings();

private:
    wstring m_TechnicalSessionName;
    wstring m_SessionName;
    wstring m_Fullname;
    wstring m_TenantDisplayName;
    wstring m_TenantName;
    wstring m_SessionType;
	wstring m_Id;

    int32_t m_AccessCount;
    YTimeDate m_LastUsedOn;
    YTimeDate m_CreatedOn;
    // TODO: Current License
    vector<uint8_t> m_ProfilePhoto;
    std::set<Role> m_Roles;

	bool m_IsFavorite;
    bool m_ShowBaseSession;

    static std::set<wstring> g_OpenedSessions;
};

