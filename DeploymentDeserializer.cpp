#include "DeploymentDeserializer.h"

#include "JsonSerializeUtil.h"

void Azure::DeploymentDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);

	auto itt = p_Object.find(_YTEXT("properties"));
	ASSERT(itt != p_Object.end() && itt->second.is_object());
	if (itt != p_Object.end() && itt->second.is_object())
	{
		const auto& properties = itt->second.as_object();
		JsonSerializeUtil::DeserializeString(_YTEXT("provisioningState"), m_Data.m_ProvisioningState, properties);
		// outputs -> cosmosDBAccountName -> value
		auto ittOutputs = properties.find(_YTEXT("outputs"));
		if (ittOutputs != properties.end() && ittOutputs->second.is_object())
		{
			const auto& outputsObj = ittOutputs->second.as_object();
			auto ittAccountName = outputsObj.find(_YTEXT("cosmosDBAccountName"));
			ASSERT(ittAccountName != outputsObj.end());
			if (ittAccountName != outputsObj.end())
			{
				ASSERT(ittAccountName->second.is_object());
				if (ittAccountName->second.is_object())
					JsonSerializeUtil::DeserializeString(_YTEXT("value"), m_Data.m_CosmosDbAccountName, ittAccountName->second.as_object());
			}
		}

		auto ittError = properties.find(_YTEXT("error"));
		if (ittError != properties.end())
		{
			const auto& errorObj = ittError->second.as_object();
			JsonSerializeUtil::DeserializeString(_YTEXT("code"), m_Data.m_ErrorCode, errorObj);
			JsonSerializeUtil::DeserializeString(_YTEXT("message"), m_Data.m_ErrorMessage, errorObj);
		}
	}

}
