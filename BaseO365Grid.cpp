#include "BaseO365Grid.h"

#include "ActivityLogger.h"
#include "AnnotationManager.h"
#include "AutomationWizardO365.h"
#include "BasicGridSetup.h"
#include "BusinessUser.h"
#include "BusinessUserConfiguration.h"
#include "BusinessUserGuest.h"
#include "BusinessUserUnknown.h"
#include "BusinessGroup.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessOrgContact.h"
#include "CacheGridGroupArea.h"
#include "CacheGridTools.h"
#include "DlgLicenseMessageBoxHTML.h"
#include "DlgSnapshotOptions.h"
#include "DlgSelectColumns.h"
#include "FileUtil.h"
#include "GridFrameBase.h"
#include "GridSnapshot.h"
#include "GridUtil.h"
#include "GridViewManager.h"
#include "HistoryMode.h"
#include "JSONUtil.h"
#include "MsGraphFieldNames.h"
#include "O365GridSnapshot.h" 
#include "YCodeJockMessageBox.h"
#include "YFileDialog.h"

#define USE_FRAME_MESSAGE_BAR_FOR_ERROR 1

BEGIN_MESSAGE_MAP(O365Grid, CacheGrid)

	NEWMODULE_COMMAND(ID_USERGRID_SHOWDRIVEITEMS		, ID_USERGRID_SHOWDRIVEITEMS_FRAME		, ID_USERGRID_SHOWDRIVEITEMS_NEWFRAME		, OnShowUserDriveItems	, OnUpdateShowBusinessUser)
	NEWMODULE_COMMAND(ID_USERGRID_SHOWMESSAGES			, ID_USERGRID_SHOWMESSAGES_FRAME		, ID_USERGRID_SHOWMESSAGES_NEWFRAME			, OnShowMessages		, OnUpdateShowBusinessUser)
	NEWMODULE_COMMAND(ID_USERGRID_SHOWMAILBOXPERMISSIONS, ID_USERGRID_SHOWMAILBOXPERMISSIONS_FRAME, ID_USERGRID_SHOWMAILBOXPERMISSIONS_NEWFRAME, OnShowMailboxPermissions, OnUpdateShowBusinessUser)
	NEWMODULE_COMMAND(ID_USERGRID_SHOWCONTACTS			, ID_USERGRID_SHOWCONTACTS_FRAME		, ID_USERGRID_SHOWCONTACTS_NEWFRAME			, OnShowContacts		, OnUpdateShowBusinessUser)
	NEWMODULE_COMMAND(ID_USERGRID_SHOWEVENTS			, ID_USERGRID_SHOWEVENTS_FRAME			, ID_USERGRID_SHOWEVENTS_NEWFRAME			, OnShowEvents			, OnUpdateShowBusinessUser)
	NEWMODULE_COMMAND(ID_USERGRID_SHOWPARENTGROUPS		, ID_USERGRID_SHOWPARENTGROUPS_FRAME	, ID_USERGRID_SHOWPARENTGROUPS_NEWFRAME		, OnShowParentGroups	, OnUpdateShowBusinessUser)
	NEWMODULE_COMMAND(ID_USERGRID_SHOWLICENSES			, ID_USERGRID_SHOWLICENSES_FRAME		, ID_USERGRID_SHOWLICENSES_NEWFRAME			, OnShowLicenses		, OnUpdateShowBusinessUser)
	NEWMODULE_COMMAND(ID_USERGRID_SHOWMANAGERHIERARCHY	, ID_USERGRID_SHOWMANAGERHIERARCHY_FRAME, ID_USERGRID_SHOWMANAGERHIERARCHY_NEWFRAME	, OnShowManagerHierarchy, OnUpdateShowBusinessUser)
	NEWMODULE_COMMAND(ID_USERGRID_SHOWMESSAGERULES		, ID_USERGRID_SHOWMESSAGERULES_FRAME	, ID_USERGRID_SHOWMESSAGERULES_NEWFRAME		, OnShowMessageRules	, OnUpdateShowBusinessUser)

	NEWMODULE_COMMAND(ID_GRID_SHOWUSERDETAILS			, ID_GRID_SHOWUSERDETAILS_FRAME			, ID_GRID_SHOWUSERDETAILS_NEWFRAME			, OnShowUserDetails		, OnUpdateShowBusinessUser)

	NEWMODULE_COMMAND(ID_GROUPSGRID_SHOWMEMBERS			, ID_GROUPSGRID_SHOWMEMBERS_FRAME		, ID_GROUPSGRID_SHOWMEMBERS_NEWFRAME		, OnShowMembers			, OnUpdateShowBusinessGroup)
	NEWMODULE_COMMAND(ID_GROUPSGRID_SHOWOWNERS			, ID_GROUPSGRID_SHOWOWNERS_FRAME		, ID_GROUPSGRID_SHOWOWNERS_NEWFRAME			, OnShowOwners			, OnUpdateShowBusinessGroup)
	NEWMODULE_COMMAND(ID_GROUPSGRID_SHOWAUTHORS			, ID_GROUPSGRID_SHOWAUTHORS_FRAME		, ID_GROUPSGRID_SHOWAUTHORS_NEWFRAME		, OnShowAuthors			, OnUpdateShowBusinessGroup)
	NEWMODULE_COMMAND(ID_GROUPSGRID_SHOWDRIVEITEMS		, ID_GROUPSGRID_SHOWDRIVEITEMS_FRAME	, ID_GROUPSGRID_SHOWDRIVEITEMS_NEWFRAME		, OnShowGroupDriveItems	, OnUpdateShowBusinessGroup)
	NEWMODULE_COMMAND(ID_GROUPSGRID_SHOWCALENDARS		, ID_GROUPSGRID_SHOWCALENDARS_FRAME		, ID_GROUPSGRID_SHOWCALENDARS_NEWFRAME		, OnShowCalendars		, OnUpdateShowBusinessGroup)
	NEWMODULE_COMMAND(ID_GROUPSGRID_SHOWCONVERSATIONS	, ID_GROUPSGRID_SHOWCONVERSATIONS_FRAME	, ID_GROUPSGRID_SHOWCONVERSATIONS_NEWFRAME	, OnShowConversations	, OnUpdateShowBusinessGroup)
	NEWMODULE_COMMAND(ID_GROUPSGRID_SHOW_CHANNELS		, ID_GROUPSGRID_SHOW_CHANNELS_FRAME		, ID_GROUPSGRID_SHOW_CHANNELS_NEWFRAME		, OnShowChannels		, OnUpdateShowChannels)
	NEWMODULE_COMMAND(ID_GROUPSGRID_SHOWSITES			, ID_GROUPSGRID_SHOWSITES_FRAME			, ID_GROUPSGRID_SHOWSITES_NEWFRAME			, OnShowSites			, OnUpdateShowBusinessGroup)
	
	NEWMODULE_COMMAND(ID_GRID_SHOWGROUPDETAILS			, ID_GRID_SHOWGROUPDETAILS_FRAME		, ID_GRID_SHOWGROUPDETAILS_NEWFRAME			, OnShowGroupDetails	, OnUpdateShowBusinessGroup)

	NEWMODULE_COMMAND(ID_GRID_SHOWSITEDETAILS			, ID_GRID_SHOWSITEDETAILS_FRAME			, ID_GRID_SHOWSITEDETAILS_NEWFRAME			, OnShowSiteDetails		, OnUpdateShowBusinessSite)

	ON_COMMAND(ID_SHOWCOLUMNS_DEFAULT,				OnShowColumnsDefault)
	ON_UPDATE_COMMAND_UI(ID_SHOWCOLUMNS_DEFAULT,	OnUpdateShowDefaultColumns)
	ON_COMMAND(ID_SHOWCOLUMNS_ALL,					OnShowColumnsAll)
	ON_UPDATE_COMMAND_UI(ID_SHOWCOLUMNS_ALL,		OnUpdateShowColumnsAll)
	ON_COMMAND(ID_SHOWLASTQUERYCOLUMN,				OnShowLastQueryColumn)
	ON_UPDATE_COMMAND_UI(ID_SHOWLASTQUERYCOLUMN,	OnUpdateShowLastQueryColumn)

	ON_COMMAND(ID_ADDITIONAL_DATA_CONFIG,			OnEditAdditionalDataConfig)
	ON_UPDATE_COMMAND_UI(ID_ADDITIONAL_DATA_CONFIG,	OnUpdateEditAdditionalDataConfig)

	ON_COMMAND(ID_CREATE_GRID_PHOTO_SNAPSHOT, OnCreateSnapshotPhoto)
	ON_UPDATE_COMMAND_UI(ID_CREATE_GRID_PHOTO_SNAPSHOT, OnUpdateCreateSnapshotPhoto)

	ON_COMMAND(ID_CREATE_GRID_RESTORE_SNAPSHOT, OnCreateSnapshotRestorePoint)
	ON_UPDATE_COMMAND_UI(ID_CREATE_GRID_RESTORE_SNAPSHOT, OnUpdateCreateSnapshotRestorePoint)

END_MESSAGE_MAP()

bool O365Grid::g_InitStatics = false;

std::shared_ptr<Sapio365Session> O365Grid::GetSession() const
{
    auto frame = GetParentGridFrameBase();
    ASSERT(nullptr != frame);
    if (nullptr != frame)
        return frame->GetSapio365Session();

    return std::make_shared<Sapio365Session>();
}

wstring O365Grid::g_FamilyOwner;
wstring O365Grid::g_TitleOwnerDisplayName;
wstring O365Grid::g_TitleOwnerUserName;
wstring O365Grid::g_TitleOwnerUserType;
wstring O365Grid::g_TitleOwnerID;
wstring O365Grid::g_TitleGraphID;
wstring O365Grid::g_TitleOwnerGroupType;
wstring O365Grid::g_TitleOwnerIsTeam;
wstring O365Grid::g_FamilyName;
wstring O365Grid::g_FamilyInfo;
wstring O365Grid::g_FamilyMail;
wstring O365Grid::g_FamilySite;
wstring O365Grid::g_FamilyOnPrem;
wstring O365Grid::g_FamilyCommonInfo;

namespace
{
	const wstring g_TemporaryCreatedObjectIDPrefix = _YTEXT("_NEW_");
}

wstring O365Grid::GetNextTemporaryCreatedObjectID()
{
	static int index = 0;
	++index;
	return _YTEXTFORMAT(L"%s%s", g_TemporaryCreatedObjectIDPrefix.c_str(),  Str::getStringFromNumber(index).c_str());
}

bool O365Grid::IsTemporaryCreatedObjectID(const wstring& P_ID)
{
	return Str::beginsWith(P_ID, g_TemporaryCreatedObjectIDPrefix);
}

GridBackendColumn* O365Grid::GetColId() const
{
    return m_ColId;
}

GridBackendColumn* O365Grid::GetColRoleDelegationFlag() const
{
	return m_ColRoleDelegationFlag;
}

const GridBackendUtil::COLUMNPRESETFLAGS O365Grid::g_ColumnsPresetDefault	= GridBackendUtil::COLUMNPRESETFLAGS::PRESET_2;
const GridBackendUtil::COLUMNPRESETFLAGS O365Grid::g_ColumnsPresetMore		= GridBackendUtil::COLUMNPRESETFLAGS::PRESET_3;
const GridBackendUtil::COLUMNPRESETFLAGS O365Grid::g_ColumnsPresetMetaMore	= GridBackendUtil::COLUMNPRESETFLAGS::PRESET_4;
const GridBackendUtil::COLUMNPRESETFLAGS O365Grid::g_ColumnsPresetOnPremDefault = GridBackendUtil::COLUMNPRESETFLAGS::PRESET_5;
const GridBackendUtil::COLUMNPRESETFLAGS O365Grid::g_ColumnsPresetOnPrem	= GridBackendUtil::COLUMNPRESETFLAGS::PRESET_6;


O365Grid::O365Grid()
	: CacheGrid(	GridBackendUtil::FUNCTIONANNOTATIONS,
					GridBackendUtil::CREATETOOLBAR | 
					GridBackendUtil::CREATEGROUPAREA |
					GridBackendUtil::CREATEHTMLAREA |
					GridBackendUtil::CREATECOLORG | 
					GridBackendUtil::CREATESORTING | 
					GridBackendUtil::CREATEPIVOT | 
					GridBackendUtil::CREATECOMPARE | 
					//GridBackendUtil::CREATESAVEXML |
					GridBackendUtil::CREATEROWVIEWER |
					GridBackendUtil::CREATEVIEWMANAGER |
					GridBackendUtil::CREATESTATUSBAR,
					0,
					false)
	, m_ShowErrors(true)
	, m_HasAtLeastOneError(false)
	, m_HasAtLeastOne403Error(false)
	, m_PendingHasAtLeastOneError(false)
	, m_PendingHasAtLeastOne403Error(false)
	, m_PendingErrorOccuredDuringSave(false)
    , m_IdDeletionFormatter(0)
    , m_IdUpdateFormatter(0)
	, m_ColId(nullptr)
	, m_ColRoleDelegationFlag(nullptr)
	, m_ShowSentValue(false)
	, m_defaultActions(0)
	, m_IsUsingRoleDelegation(false)
	, m_IconRABCauthorized(-1)
	, m_IconRABCunauthorized(-1)
	, m_WithOwnerAsTopAncestor(false)
	, m_MayContainUnscopedUserGroupOrSite(false)
	, m_RBACHiddenCount(0)
#ifdef _DEBUG
	, m_HasModificationsPendingAssertCounter(0)
#endif
	, m_IconSavedNotReceived(NO_ICON)
	, m_HasSavedNotReceived(false)
	, m_IconSavedOverwitten(NO_ICON)
	, m_HasSavedOverwitten(false)
	, m_IconSaved(NO_ICON)
	, m_IconError(NO_ICON)
	, m_ModReverter(std::make_shared<GenericModificationsReverter>(*this))
	, m_ModApplyer(std::make_shared<GenericModificationsApplyer>(*this))
	, m_GridRefresher(std::make_shared<GenericGridRefresher>(*this))
	, m_SecondaryStatusColumn(nullptr)
{
	init();
}

BOOL O365Grid::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return CacheGrid::PreTranslateMessage(pMsg);
}

BOOL O365Grid::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	if (CN_COMMAND == nCode)
		HistoryMode::SetModeFromCommand(nID);

	return CacheGrid::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void O365Grid::init()
{
	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_O365GRID_ACCELERATOR));

	if (!g_InitStatics)
	{
		g_FamilyOwner				= YtriaTranslate::Do(O365Grid_init_1, _YLOC("Owner")).c_str();
		g_TitleOwnerDisplayName		= YtriaTranslate::Do(O365Grid_init_6, _YLOC("Owner Display Name")).c_str();
		g_TitleOwnerUserName		= YtriaTranslate::Do(O365Grid_init_7, _YLOC("Owner Username")).c_str();
		g_TitleOwnerUserType		= YtriaTranslate::Do(O365Grid_init_10, _YLOC("Owner User Type")).c_str();
		g_TitleOwnerID				= YtriaTranslate::Do(O365Grid_init_8, _YLOC("Owner ID")).c_str();
		g_TitleGraphID				= YtriaTranslate::Do(O365Grid_init_9, _YLOC("GUID")).c_str();
		g_TitleOwnerGroupType		= YtriaTranslate::Do(O365Grid_init_12, _YLOC("Owner Group Type")).c_str();
		g_TitleOwnerIsTeam			= YtriaTranslate::Do(O365Grid_init_11, _YLOC("Owner Is a Team")).c_str();
		
		g_FamilyName				= YtriaTranslate::Do(O365Grid_init_2, _YLOC("Name")).c_str();
		g_FamilyInfo				= YtriaTranslate::Do(O365Grid_init_3, _YLOC("Info")).c_str();
		g_FamilyMail				= YtriaTranslate::Do(O365Grid_init_4, _YLOC("Mail")).c_str();
		g_FamilySite				= YtriaTranslate::Do(O365Grid_init_5, _YLOC("Site")).c_str();
		g_FamilyOnPrem				= _T("On-Premises");
		g_FamilyCommonInfo			= _T("Common Info");

		g_InitStatics = true;
	}
}

bool O365Grid::isCompatibleWithDeletedUser(UINT cmdID)
{
	return ID_USERGRID_SHOWLICENSES == cmdID
		|| ID_USERGRID_SHOWLICENSES_FRAME == cmdID
		|| ID_USERGRID_SHOWLICENSES_NEWFRAME == cmdID

		|| ID_USERGRID_SHOWDRIVEITEMS == cmdID
		|| ID_USERGRID_SHOWDRIVEITEMS_FRAME == cmdID
		|| ID_USERGRID_SHOWDRIVEITEMS_NEWFRAME == cmdID

		|| ID_USERGRID_SHOWMESSAGES == cmdID
		|| ID_USERGRID_SHOWMESSAGES_FRAME == cmdID
		|| ID_USERGRID_SHOWMESSAGES_NEWFRAME == cmdID

		|| ID_USERGRID_SHOWEVENTS == cmdID
		|| ID_USERGRID_SHOWEVENTS_FRAME == cmdID
		|| ID_USERGRID_SHOWEVENTS_NEWFRAME == cmdID

		|| ID_USERGRID_SHOWCONTACTS == cmdID
		|| ID_USERGRID_SHOWCONTACTS_FRAME == cmdID
		|| ID_USERGRID_SHOWCONTACTS_NEWFRAME == cmdID

		|| ID_USERGRID_SHOWMESSAGERULES == cmdID
		|| ID_USERGRID_SHOWMESSAGERULES_FRAME == cmdID
		|| ID_USERGRID_SHOWMESSAGERULES_NEWFRAME == cmdID
		;
}

bool O365Grid::isCompatibleWithGuestUser(UINT cmdID)
{
	return ID_USERGRID_SHOWLICENSES != cmdID
		&& ID_USERGRID_SHOWLICENSES_FRAME != cmdID
		&& ID_USERGRID_SHOWLICENSES_NEWFRAME != cmdID
		
		&& ID_USERGRID_SHOWDRIVEITEMS != cmdID
		&& ID_USERGRID_SHOWDRIVEITEMS_FRAME != cmdID
		&& ID_USERGRID_SHOWDRIVEITEMS_NEWFRAME != cmdID

		&& ID_USERGRID_SHOWMESSAGES != cmdID
		&& ID_USERGRID_SHOWMESSAGES_FRAME != cmdID
		&& ID_USERGRID_SHOWMESSAGES_NEWFRAME != cmdID
		
		&& ID_USERGRID_SHOWEVENTS != cmdID
		&& ID_USERGRID_SHOWEVENTS_FRAME != cmdID
		&& ID_USERGRID_SHOWEVENTS_NEWFRAME != cmdID

		&& ID_USERGRID_SHOWCONTACTS != cmdID
		&& ID_USERGRID_SHOWCONTACTS_FRAME != cmdID
		&& ID_USERGRID_SHOWCONTACTS_NEWFRAME != cmdID

		&& ID_USERGRID_SHOWMESSAGERULES != cmdID
		&& ID_USERGRID_SHOWMESSAGERULES_FRAME != cmdID
		&& ID_USERGRID_SHOWMESSAGERULES_NEWFRAME != cmdID
		
		&& ID_USERGRID_SHOWMAILBOXPERMISSIONS != cmdID
		&& ID_USERGRID_SHOWMAILBOXPERMISSIONS_FRAME != cmdID
		&& ID_USERGRID_SHOWMAILBOXPERMISSIONS_NEWFRAME != cmdID;
}

bool O365Grid::isDebugMode() const
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	return nullptr != mainFrame && mainFrame->IsDebugMode();
}

IModificationReverter* O365Grid::GetModReverter()
{
	return m_ModReverter.get();
}

IModificationApplyer* O365Grid::GetModApplyer()
{
	return m_ModApplyer.get();
}

IGridRefresher* O365Grid::GetGridRefresher()
{
	return m_GridRefresher.get();
}

GridBackendColumn* O365Grid::addColumnIsLoadMore(const wstring& p_UniqueID, const wstring& p_ColumnTitle, bool p_DateColumn/* = false*/)
{
	CacheGrid::addColumnIsLoadMore(p_UniqueID, p_ColumnTitle, g_FamilyInfo, g_ColumnSize16, p_DateColumn);
	return GetIsMoreLoadedStatusColumn();
}

GridBackendColumn* O365Grid::addColumnDataBlockDate(const wstring& p_UniqueID, const wstring& p_ColumnTitle, const wstring& p_ColumnFamily)
{
	auto col = AddColumnDate(p_UniqueID, p_ColumnTitle, p_ColumnFamily, g_ColumnSize16);
	col->SetIgnoreModifications(true);
	col->SetFlag(GridBackendUtil::CANNOTBEGHOST);
	return col;
}

GridBackendColumn* O365Grid::AddSecondaryColumnStatus(const wstring& i_Title, const wstring& i_Family /*= _YTEXT("")*/)
{
	ASSERT(nullptr == m_SecondaryStatusColumn);
	if (nullptr == m_SecondaryStatusColumn)
	{
		m_SecondaryStatusColumn = AddColumn(_YUID("ONPREMSTATUS"), i_Title, i_Family, GridBackendUtil::STRING, GridBackendUtil::CELL_TYPE_STRING, HIDPI_XW(GridBackendUtil::g_MinColWidth));
		ASSERT(nullptr != m_SecondaryStatusColumn);
		if (nullptr != m_SecondaryStatusColumn)
		{
			ASSERT(nullptr != GetBackend().GetStatusColumn());

			const auto lockedColumns = GetBackend().GetLockedColumns();
			GetBackend().SetColumnsLock(false, lockedColumns);

			AddColumnToCategory(GetBackend().GetStatusColumn()->GetTopCategory(), m_SecondaryStatusColumn);
			GetBackend().MoveColumnAfter(m_SecondaryStatusColumn, GetBackend().GetStatusColumn()); // call from the backend to avoid UpdateMegaShark

			{
				auto defFro = GetDefaultFrozenColumn();
				if (nullptr == defFro || defFro->GetPosition() < m_SecondaryStatusColumn->GetPosition())
					setColumnDefaultFrozen(m_SecondaryStatusColumn);
			}

			m_SecondaryStatusColumn->SetStatusColumn(true);
			m_SecondaryStatusColumn->SetExplosionEnabled(false);
			m_SecondaryStatusColumn->SetLocked(true);
			m_SecondaryStatusColumn->SetFlag(GridBackendUtil::IGNOREINSNAPSHOT); //?

			GetBackend().SetColumnsLock(true, lockedColumns);
		}
	}
	return m_SecondaryStatusColumn;
}

bool O365Grid::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	if (p_Field.HasValue())
	{
		if (&p_Column == GetBackend().GetStatusColumn())
		{
			// Error fields have been handled before in GridSnapshot::ToSql
			ASSERT(p_Field.GetIcon() != GridBackendUtil::ICON_ERROR);
			p_Value = _YTEXT("");
			return true;
		}

		if (&p_Column == GetIsMoreLoadedStatusColumn())
		{
			if (p_Field.GetIcon() == GetLoadMoreIcon(true))
				p_Value = _YTEXT("1");
			else if (p_Field.GetIcon() == GetLoadMoreIcon(false))
				p_Value = _YTEXT("0");
			else
				ASSERT(false);
			return true;
		}

		/*if (&p_Column == GetColumnObjectType())
		{
		}*/
	}

	return false;
}

bool O365Grid::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (&p_Column == GetColumnObjectType())
	{
		const auto& objTypeSetup = GetObjectTypeSetup();
		for (size_t i = 0; i < objTypeSetup.size(); ++i)
		{
			const auto& obj = objTypeSetup[objTypeSetup.at(i)];
			if (obj.m_TypeDisplayName == p_Value)
			{
				SetRowObjectType(&p_Row, obj.m_TypeID);
				return true;
			}
		}

		ASSERT(false);
	}

	if (&p_Column == GetIsMoreLoadedStatusColumn())
	{
		ASSERT(!p_Value.empty());
		if (p_Value == _YTEXT("1"))
		{
			p_Row.SetIsMoreLoaded(true);
		}
		else
		{
			ASSERT(p_Value == _YTEXT("0"));
			p_Row.SetIsMoreLoaded(false);
		}
		p_Row.AddField(GetLoadMoreText(p_Row.IsMoreLoaded()), &p_Column, GetLoadMoreIcon(p_Row.IsMoreLoaded())).RemoveModified();

		return true;
	}

	return false;
}

bool O365Grid::GetSnapshotAdditionalValue(wstring& p_Value, GridBackendRow& p_Row)
{
	return false;
}

bool O365Grid::SetSnapshotAdditionalValue(const wstring& p_Value, GridBackendRow& p_Row)
{
	return false;
}

void O365Grid::setBasicGridSetupHierarchy(const BasicGridSetupHierarchy& p_Setup)
{
	SetHierarchyColumnsParentPKs(p_Setup.m_ColumnsParentPKs);
    for (const auto& typeId : p_Setup.m_LowestChildrenObjectTypes)
		AddHierarchyLowestChildrenObjectTypeId(typeId);

	if (!p_Setup.m_AllDefaultObjectTypes.empty())
	{
		auto fakeRow = AddRow();
		fakeRow->SetTechHidden(true);
		for (auto& type : p_Setup.m_AllDefaultObjectTypes)
			SetRowObjectType(fakeRow, type);
		RemoveRow(fakeRow);
	}
}

vector<GridBackendColumn*> O365Grid::addSystemTopCategory()
{
	auto columns	= GetBackend().GetHierarchyColumns();
	auto cot		= GetColumnObjectType();
	if (nullptr != cot)
		columns.push_back(cot);
	auto cst = GetBackend().GetStatusColumn();
	if (nullptr != cst)
		columns.push_back(cst);
	auto clqo = GetColumnByUniqueID(BasicGridSetup::g_LastQueryColumnUID);
	if (nullptr != clqo)
		columns.push_back(clqo);
	if (nullptr != m_ColRoleDelegationFlag)
		columns.push_back(m_ColRoleDelegationFlag);
	if (!columns.empty())
		AddCategoryTop(YtriaTranslate::Do(O365Grid_addSystemTopCategory_1, _YLOC("System")).c_str(), columns);

	return columns;
}

void O365Grid::addTopCategories(const wstring& p_MainTitle, const wstring& p_LoadMoreTitle, bool p_IsMoreLoadedInLoadMore)
{
	ASSERT(WithLoadMore());
	if (WithLoadMore())
	{
		auto systemColumns		= addSystemTopCategory();
		auto remainingColumns	= GetBackendColumns();
		auto loadMoreColumns	= GetColumnsByPresets({ g_ColumnsPresetMore });
        if (p_IsMoreLoadedInLoadMore)
		    loadMoreColumns.insert(loadMoreColumns.begin(), GetIsMoreLoadedStatusColumn());
		remainingColumns.erase(std::remove_if(remainingColumns.begin(), remainingColumns.end(), [=](GridBackendColumn* c)
		{
			return std::find(systemColumns.begin(), systemColumns.end(), c) != systemColumns.end() || std::find(loadMoreColumns.begin(), loadMoreColumns.end(), c) != loadMoreColumns.end();
		}), remainingColumns.end());

		AddCategoryTop(p_MainTitle,		remainingColumns);
		AddCategoryTop(p_LoadMoreTitle, loadMoreColumns);
	}
}

GridBackendColumn* O365Grid::addColumnGraphID(const wstring& p_Family)
{
	m_ColId = AddColumnCaseSensitive(_YUID(O365_ID), g_TitleGraphID, p_Family, g_ColumnSize12, { g_ColumnsPresetTech });
	return m_ColId;
}

GridBackendColumn* O365Grid::addColumnGraphID()
{
	return addColumnGraphID(g_FamilyInfo);
}

void O365Grid::addColumnRoleDelegationFlag()
{
	addColumnRoleDelegationFlag(g_FamilyInfo);
}

void O365Grid::addColumnRoleDelegationFlag(const wstring& p_Family)
{
	if (InitRoleDelegation())
	{
		m_ColRoleDelegationFlag = AddColumnIcon(_YUID("RBAC"), YtriaTranslate::Do(NotesACL_s_ItemName_12__29__3, _YLOC("Access")).c_str(), p_Family);
		ASSERT(nullptr != m_ColRoleDelegationFlag);
		if (nullptr != m_ColRoleDelegationFlag)
		{
			m_ColRoleDelegationFlag->SetLocked(true);
			m_ColRoleDelegationFlag->SetFlag(GridBackendUtil::IGNOREINSNAPSHOT);
		}
	}
}

void O365Grid::RevertAllModifications(bool p_UpdateGridIfNeeded)
{
	bool shouldUpdate = false;
	{
		ScopedImplodeRows scopedImplodeRows(*this, GetModifications().GetModifiedRows(true, true), p_UpdateGridIfNeeded);
		scopedImplodeRows.SetRowsNotToRestore(GetModifications().GetCreatedRows(true));
		shouldUpdate = GetModifications().RevertAll() && p_UpdateGridIfNeeded && !scopedImplodeRows.HasExplosionsToRestore();
	}
	if (shouldUpdate)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void O365Grid::RevertSelectedRows(uint32_t p_RevertFlags)
{
	// Can only have one of those flags at a time: ONLY_ASSOCIATED_TOP_LEVEL_ROWS, TOP_LEVEL_REVERT_CHILDREN, ANY_LEVEL_REVERT_CHILDREN
	ASSERT(
			!(ONLY_ASSOCIATED_TOP_LEVEL_ROWS == (ONLY_ASSOCIATED_TOP_LEVEL_ROWS & p_RevertFlags) && TOP_LEVEL_REVERT_CHILDREN == (TOP_LEVEL_REVERT_CHILDREN & p_RevertFlags))
		||	!(ONLY_ASSOCIATED_TOP_LEVEL_ROWS == (ONLY_ASSOCIATED_TOP_LEVEL_ROWS & p_RevertFlags) && ANY_LEVEL_REVERT_CHILDREN == (ANY_LEVEL_REVERT_CHILDREN & p_RevertFlags))
		||	!(TOP_LEVEL_REVERT_CHILDREN == (TOP_LEVEL_REVERT_CHILDREN & p_RevertFlags) && ANY_LEVEL_REVERT_CHILDREN == (ANY_LEVEL_REVERT_CHILDREN & p_RevertFlags))
	);

	if (GetModifications().HasModifications())
	{
		CWaitCursor c;
		bool shouldUpdate = false;

		if (GetModifications().HasSubItemModifications())
		{
			row_pk_t pk;

			// Voluntarily make a copy (RevertSubItem can remove a row form this set)
			const auto rows = GetSelectedRows();
			for (auto row : rows)
			{
				ASSERT(nullptr != row);

				// Should we handle this here?
				/*if (nullptr != row && p_OnlyAssociatedTopLevelRows)
					row = row->GetTopAncestorOrThis();*/

				// Test row state to avoid uselessly getting row pk and processing Revert (== linear search in 2 deques)
				if (nullptr != row && (row->IsModified() || row->IsCreated() || row->IsDeleted()))
				{
					GetRowPK(row, pk);
					if (GetModifications().RevertSubItem(pk))
						shouldUpdate = true;
					pk.clear();
				}
			}
		}

		TemporarilyImplodeNonGroupRowsAndExec(*this
			, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS
			, [this
				, &shouldUpdate
				, updateGridIfNeeded = UPDATE_GRID_IF_NEEDED == (UPDATE_GRID_IF_NEEDED & p_RevertFlags)
				, onlyAssociatedTopLevelRows = ONLY_ASSOCIATED_TOP_LEVEL_ROWS == (ONLY_ASSOCIATED_TOP_LEVEL_ROWS & p_RevertFlags)
				, topLevelRevertChildren = TOP_LEVEL_REVERT_CHILDREN == (TOP_LEVEL_REVERT_CHILDREN & p_RevertFlags)
				, anyLevelRevertChildren = ANY_LEVEL_REVERT_CHILDREN == (ANY_LEVEL_REVERT_CHILDREN & p_RevertFlags)](const bool p_HasRowsToReExplode)
		{
			auto revertWithDirectChildren = [this](auto p_Row, auto& p_SelectedRows, auto& p_ToRevertRows)
			{
				auto children = GetChildRows(p_Row);

				// Do not keep children that are in current grid selection, they'll be reverted individually.
				children.erase(std::remove_if(children.begin(), children.end(), [&p_SelectedRows](auto& p_Child) { return p_SelectedRows.end() != std::find(p_SelectedRows.begin(), p_SelectedRows.end(), p_Child); }), children.end());

				for (auto child : children)
				{
					if (child->IsModified() || child->IsCreated() || child->IsDeleted())
						p_ToRevertRows.insert(child);
				}

				return children;
			};
			auto recursivelyRevert = [this, revertWithDirectChildren](auto p_Row, auto& p_SelectedRows, auto& p_ToRevertRows)
			{
				auto recursivelyRevertImpl = [this, revertWithDirectChildren](auto p_Row, auto& p_SelectedRows, auto& p_ToRevertRows, const auto& self)->void
				{
					auto children = revertWithDirectChildren(p_Row, p_SelectedRows, p_ToRevertRows);

					for (auto child : children)
					{
						if (child->HasChildrenRows())
							self(child, p_SelectedRows, p_ToRevertRows, self);
					}
				};

				recursivelyRevertImpl(p_Row, p_SelectedRows, p_ToRevertRows, recursivelyRevertImpl);
			};

			row_pk_t pk;
			std::set<GridBackendRow*> toRevert;
			const auto& rows = GetSelectedRows();
			for (auto row : rows)
			{
				ASSERT(nullptr != row);

				if (onlyAssociatedTopLevelRows)
				{
					if (nullptr != row)
						row = row->GetTopAncestorOrThis();

					if (row->IsModified() || row->IsCreated() || row->IsDeleted())
						toRevert.insert(row);
				}
				else
				{
					if (row->IsModified() || row->IsCreated() || row->IsDeleted())
						toRevert.insert(row);
					if (nullptr != row && row->HasChildrenRows() && (anyLevelRevertChildren || topLevelRevertChildren && 0 == row->GetHierarchyLevel()))
						recursivelyRevert(row, rows, toRevert);					
				}
			}

			for (auto rowToRevert : toRevert)
			{
				// Test row state to avoid uselessly getting row pk and processing Revert (== linear search in 2 deques)
				if (nullptr != rowToRevert && (rowToRevert->IsModified() || rowToRevert->IsCreated() || rowToRevert->IsDeleted()))
				{
					GetRowPK(rowToRevert, pk);
					shouldUpdate = GetModifications().Revert(pk, false) || shouldUpdate;

					ASSERT(shouldUpdate); // Is this fails, it means row state is not synced to GridModifications.
					pk.clear();
				}
			}

			if (shouldUpdate && updateGridIfNeeded && !p_HasRowsToReExplode)
				UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		})();
	}
}

void O365Grid::EnableGridModifications()
{
	ASSERT(!m_Modifications);
	if (!m_Modifications)
		m_Modifications = make_unique<GridModificationsO365>(*this);
}

bool O365Grid::IsGridModificationsEnabled() const
{
	return nullptr != m_Modifications;
}

GridModificationsO365& O365Grid::GetModifications()
{
	// If it fails, you forgot to call EnableGridModifications
	ASSERT(m_Modifications);
    return *m_Modifications;
}

const GridModificationsO365& O365Grid::GetModifications() const
{
    ASSERT(m_Modifications);
    return *m_Modifications;
}

row_pk_t O365Grid::UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody)
{
	ASSERT(false); // Implement in your class if used.
	return rowPk;
}

void O365Grid::ModificationStateChanged(GridBackendRow* p_Row, RowFieldUpdates<Scope::O365>& p_Modification)
{
	// Nothing to do in base class
}

void O365Grid::ModificationStateChanged(GridBackendRow* p_Row, RowFieldUpdates<Scope::ONPREM>& p_Modification)
{
	// Nothing to do in base class
}

void O365Grid::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_CREATE_GRID_PHOTO_SNAPSHOT);
		pPopup->ItemInsert(ID_CREATE_GRID_RESTORE_SNAPSHOT);
		pPopup->ItemInsert(); // Sep

		if (hasUsersSelected())
		{
			if (GridUtil::IsCommandAllowed(*this, ID_GRID_SHOWUSERDETAILS, GetOrigin()))
				pPopup->ItemInsert(ID_GRID_SHOWUSERDETAILS);

			if (GridUtil::IsCommandAllowed(*this, ID_USERGRID_SHOWPARENTGROUPS, GetOrigin()))
				pPopup->ItemInsert(ID_USERGRID_SHOWPARENTGROUPS);

			if (GridUtil::IsCommandAllowed(*this, ID_USERGRID_SHOWLICENSES, GetOrigin()))
				pPopup->ItemInsert(ID_USERGRID_SHOWLICENSES);

#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
			if (GridUtil::IsCommandAllowed(*this, ID_USERGRID_SHOWMANAGERHIERARCHY, GetOrigin()))
				pPopup->ItemInsert(ID_USERGRID_SHOWMANAGERHIERARCHY);
#endif

			if (GridUtil::IsCommandAllowed(*this, ID_USERGRID_SHOWDRIVEITEMS, GetOrigin()))
				pPopup->ItemInsert(ID_USERGRID_SHOWDRIVEITEMS);

			if (GridUtil::IsCommandAllowed(*this, ID_USERGRID_SHOWMAILBOXPERMISSIONS, GetOrigin()))
				pPopup->ItemInsert(ID_USERGRID_SHOWMAILBOXPERMISSIONS);

			if (GridUtil::IsCommandAllowed(*this, ID_USERGRID_SHOWMESSAGES, GetOrigin()))
				pPopup->ItemInsert(ID_USERGRID_SHOWMESSAGES);

			if (GridUtil::IsCommandAllowed(*this, ID_USERGRID_SHOWEVENTS, GetOrigin()))
				pPopup->ItemInsert(ID_USERGRID_SHOWEVENTS);

			if (GridUtil::IsCommandAllowed(*this, ID_USERGRID_SHOWCONTACTS, GetOrigin()))
				pPopup->ItemInsert(ID_USERGRID_SHOWCONTACTS);

			if (GridUtil::IsCommandAllowed(*this, ID_USERGRID_SHOWMESSAGERULES, GetOrigin()))
				pPopup->ItemInsert(ID_USERGRID_SHOWMESSAGERULES);

			pPopup->ItemInsert(); // Sep
		}

		if (hasGroupsSelected())
		{
			if (GridUtil::IsCommandAllowed(*this, ID_GRID_SHOWGROUPDETAILS, GetOrigin()))
				pPopup->ItemInsert(ID_GRID_SHOWGROUPDETAILS);

			if (GridUtil::IsCommandAllowed(*this, ID_GROUPSGRID_SHOWMEMBERS, GetOrigin()))
				pPopup->ItemInsert(ID_GROUPSGRID_SHOWMEMBERS);

			if (GridUtil::IsCommandAllowed(*this, ID_GROUPSGRID_SHOWOWNERS, GetOrigin()))
				pPopup->ItemInsert(ID_GROUPSGRID_SHOWOWNERS);

			if (GridUtil::IsCommandAllowed(*this, ID_GROUPSGRID_SHOWAUTHORS, GetOrigin()))
				pPopup->ItemInsert(ID_GROUPSGRID_SHOWAUTHORS);

			if (GridUtil::IsCommandAllowed(*this, ID_GROUPSGRID_SHOWDRIVEITEMS, GetOrigin()))
				pPopup->ItemInsert(ID_GROUPSGRID_SHOWDRIVEITEMS);

			if (GridUtil::IsCommandAllowed(*this, ID_GROUPSGRID_SHOWCALENDARS, GetOrigin()))
				pPopup->ItemInsert(ID_GROUPSGRID_SHOWCALENDARS);

			if (GridUtil::IsCommandAllowed(*this, ID_GROUPSGRID_SHOWCONVERSATIONS, GetOrigin()))
				pPopup->ItemInsert(ID_GROUPSGRID_SHOWCONVERSATIONS);

			if (GridUtil::IsCommandAllowed(*this, ID_GROUPSGRID_SHOWSITES, GetOrigin()))
				pPopup->ItemInsert(ID_GROUPSGRID_SHOWSITES);

			pPopup->ItemInsert(); // Sep
		}

		if (hasSitesSelected())
		{
			if (GridUtil::IsCommandAllowed(*this, ID_GRID_SHOWSITEDETAILS, GetOrigin()))
				pPopup->ItemInsert(ID_GRID_SHOWSITEDETAILS);
		}
	}
}

void O365Grid::GetIDs(O365IdsContainer& p_IDs, bool p_Selected, Origin p_Origin, RoleDelegationUtil::RBAC_Privilege p_Privilege, bool p_IncludeGuestsWhenOriginIsUser/* = true*/) const
{
	GridBackendColumn* pColID = nullptr != m_ColId ? m_ColId : GetColumnByUniqueID(_YTEXT(O365_ID));
	ASSERT(nullptr != pColID);
	if (nullptr != pColID)
	{
		p_IDs.clear();

		const auto session = GetSession();
		const bool roleDelegation = session && session->IsUseRoleDelegation();

		const vector<GridBackendRow*>& rows = p_Selected ? GetBackend().GetSelectedRowsInOrder() : GetBackend().GetBackendRowsForDisplay();

		for (auto row : rows)
		{
			ASSERT(nullptr != row);

			bool isBizType = false;
			if (p_Origin == Origin::User)
				isBizType = GridUtil::IsBusinessUser(row, p_IncludeGuestsWhenOriginIsUser) && (!roleDelegation || IsUserAuthorizedByRoleDelegation(row, p_Privilege));
			else if (p_Origin == Origin::Group)
				isBizType = GridUtil::IsBusinessGroup(row) && (!roleDelegation || IsGroupAuthorizedByRoleDelegation(row, p_Privilege));
			else if (p_Origin == Origin::Site)
				isBizType = GridUtil::IsBusinessSite(row) && (!roleDelegation || IsSiteAuthorizedByRoleDelegation(row, p_Privilege));
			else
				ASSERT(FALSE);

			if (isBizType)
			{
				const GridBackendField& f = row->GetField(pColID);
				ASSERT(f.HasValue());
				if (f.HasValue())
				{
					wstring id(f.GetValueStr());
					if (!id.empty())
						p_IDs.insert(f.GetValueStr());
				}
			}
		}
	}
}

void O365Grid::GetTeamIDs(O365IdsContainer& p_IDs, bool p_Selected) const
{
	GridBackendColumn* pColID = nullptr != m_ColId ? m_ColId : GetColumnByUniqueID(_YTEXT(O365_ID));
	ASSERT(nullptr != pColID);
	if (nullptr != pColID)
	{
		p_IDs.clear();

		const vector<GridBackendRow*>& rows = p_Selected ? GetBackend().GetSelectedRowsInOrder() : GetBackend().GetBackendRowsForDisplay();
		for (auto row : rows)
		{
			ASSERT(nullptr != row);
			if (GridUtil::IsBusinessGroup(row) && isTeam(row) && IsGroupAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CHANNELS_READ))
			{
				auto id = row->GetField(pColID).GetValueStr();
				if (nullptr != id)
					p_IDs.insert(id);
			}
		}
	}
}

bool O365Grid::hasUsersSelected() const
{
	bool HasUsers = false;

	for (auto row : GetSelectedRows())
	{
		ASSERT(nullptr != row);
		if (nullptr != row && GridUtil::IsBusinessUser(row))
		{
			HasUsers = true;
			break;
		}
	}

	return HasUsers;
}

bool O365Grid::hasGroupsSelected() const
{
	bool HasGroups = false;

	for (auto row : GetSelectedRows())
	{
		ASSERT(nullptr != row);
		if (nullptr != row && GridUtil::IsBusinessGroup(row))
		{
			HasGroups = true;
			break;
		}
	}

	return HasGroups;
}

bool O365Grid::hasSitesSelected() const
{
	bool HasSites = false;

	for (auto row : GetSelectedRows())
	{
		ASSERT(nullptr != row);
		if (nullptr != row && GridUtil::IsBusinessSite(row))
		{
			HasSites = true;
			break;
		}
	}

	return HasSites;
}

bool O365Grid::IsFrameConnected() const
{
	bool Enable = false;

	auto pFrame = dynamic_cast<GridFrameBase*>(GetParent());
	ASSERT(nullptr != pFrame);
	if (nullptr != pFrame)
		Enable = pFrame->IsConnected();

	return Enable;
}

bool O365Grid::IsAuthorizedByRoleDelegation(const GridBackendRow* p_Row, const RoleDelegationUtil::RBAC_Privilege p_Privilege, const bool p_CheckAncestorPrivilegeIfPresent/*= false*/) const
{
	auto frame = GetParentGridFrameBase();
	if (nullptr != frame && !frame->GetSapio365Session())
	{
		ASSERT(frame->IsSnapshotMode());
		return false;
	}

	auto row = nullptr != p_Row && m_WithOwnerAsTopAncestor && p_CheckAncestorPrivilegeIfPresent ? p_Row->GetTopAncestor() : p_Row;

	if ((GridUtil::IsBusinessUser(row)))
		return IsUserAuthorizedByRoleDelegation(row, p_Privilege);

	if (GridUtil::IsBusinessGroup(row))
		return IsGroupAuthorizedByRoleDelegation(row, p_Privilege);

	if (GridUtil::IsBusinessSite(row))
		return IsSiteAuthorizedByRoleDelegation(row, p_Privilege);

	return true;
}

bool O365Grid::IsAuthorizedByRoleDelegation(const RoleDelegationUtil::RBAC_Privilege p_Privilege) const
{
	bool authorized = false;

	auto session = GetSession();
	if (session)
	{
		auto rdid = session->GetRoleDelegationID();
		authorized = 0 < rdid ? RoleDelegationManager::GetInstance().DelegationHasPrivilege(rdid, p_Privilege, GetSession()) : true;
	}

	return authorized;
}

bool O365Grid::hasNoTaskRunning() const
{
	bool noTaskRunning = true;

	auto pFrame = dynamic_cast<GridFrameBase*>(GetParent());
	ASSERT(nullptr != pFrame);
	if (nullptr != pFrame)
		noTaskRunning = pFrame->HasNoTaskRunning();

	return noTaskRunning;
}

bool O365Grid::GetShowSentValue() const
{
    return m_ShowSentValue;
}

void O365Grid::SetShowSentValue(bool p_Value)
{
    m_ShowSentValue = p_Value;
}

void O365Grid::OnShowUserDriveItems()
{
	const bool useSearch = MFCUtil::isALTDOWN() && CRMpipe().IsFeatureSandboxed();
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;

		if (GetAutomationName() == GridUtil::g_AutoNameUsersRecycleBin || Origin::DeletedUser == GetOrigin())
			commandInfo.SetOrigin(Origin::DeletedUser);
		else
			commandInfo.SetOrigin(Origin::User);

		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_USERGRID_SHOWDRIVEITEMS, Origin::DeletedUser == commandInfo.GetOrigin()));
		GetIDs(commandInfo.GetIds(), true, Origin::User, commandInfo.GetRBACPrivilege(), isCompatibleWithGuestUser(ID_USERGRID_SHOWDRIVEITEMS));

		const bool noID = commandInfo.GetIds().empty();
		if (!noID && showUserRestrictedAccessDialog(USER_PERMISSION_REQUIREMENT))
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Drive, useSearch ? Command::ModuleTask::SearchSubItems : Command::ModuleTask::ListSubItems, commandInfo, { this, g_ActionNameSelectedShowDriveItemsUsers, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(noID ? YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"),_YR("Y2351")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"),_YR("Y2352")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2353")).c_str());
	}
}

void O365Grid::OnShowMailboxPermissions()
{
	if (openModuleConfirmation())
	{
		if (!ModuleUtil::AskUserPowerShellAuth(GetSession(), this))
			return;

		CommandInfo commandInfo;

		if (GetAutomationName() == GridUtil::g_AutoNameUsersRecycleBin || Origin::DeletedUser == GetOrigin())
			commandInfo.SetOrigin(Origin::DeletedUser);
		else
			commandInfo.SetOrigin(Origin::User);

		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_USERGRID_SHOWMAILBOXPERMISSIONS, Origin::DeletedUser == commandInfo.GetOrigin()));
		GetIDs(commandInfo.GetIds(), true, Origin::User, commandInfo.GetRBACPrivilege(), isCompatibleWithGuestUser(ID_USERGRID_SHOWMAILBOXPERMISSIONS));

		const bool noID = commandInfo.GetIds().empty();
		if (!noID)
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::MailboxPermissions, Command::ModuleTask::List, commandInfo, { this, g_ActionNameSelectedShowMailboxPermissions, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(noID ? YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"), _YR("Y2354")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"), _YR("Y2355")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"), _YR("Y2356")).c_str());
	}
}

void O365Grid::OnShowMessages()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;

		if (GetAutomationName() == GridUtil::g_AutoNameUsersRecycleBin || Origin::DeletedUser == GetOrigin())
			commandInfo.SetOrigin(Origin::DeletedUser);
		else
			commandInfo.SetOrigin(Origin::User);

		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_USERGRID_SHOWMESSAGES, Origin::DeletedUser == commandInfo.GetOrigin()));
		GetIDs(commandInfo.GetIds(), true, Origin::User, commandInfo.GetRBACPrivilege(), isCompatibleWithGuestUser(ID_USERGRID_SHOWMESSAGES));

		const bool noID = commandInfo.GetIds().empty();
		if (!noID && showUserRestrictedAccessDialog(USER_PERMISSION_REQUIREMENT))
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Message, Command::ModuleTask::List, commandInfo, { this, g_ActionNameSelectedShowMessages, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(noID ? YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"),_YR("Y2354")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"),_YR("Y2355")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2356")).c_str());
	}
}

void O365Grid::OnShowContacts()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		if (GetAutomationName() == GridUtil::g_AutoNameUsersRecycleBin || Origin::DeletedUser == GetOrigin())
			commandInfo.SetOrigin(Origin::DeletedUser);
		else
			commandInfo.SetOrigin(Origin::User);

		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_USERGRID_SHOWCONTACTS, Origin::DeletedUser == commandInfo.GetOrigin()));
		GetIDs(commandInfo.GetIds(), true, Origin::User, commandInfo.GetRBACPrivilege(), isCompatibleWithGuestUser(ID_USERGRID_SHOWCONTACTS));

		const bool noID = commandInfo.GetIds().empty();
		if (!noID && showUserRestrictedAccessDialog(USER_PERMISSION_REQUIREMENT))
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Contact, Command::ModuleTask::List, commandInfo, { this, g_ActionNameSelectedShowContacts, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(noID ? YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"),_YR("Y2360")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"),_YR("Y2361")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2362")).c_str());
	}
}

void O365Grid::OnShowEvents()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		if (GetAutomationName() == GridUtil::g_AutoNameUsersRecycleBin || Origin::DeletedUser == GetOrigin())
			commandInfo.SetOrigin(Origin::DeletedUser);
		else
			commandInfo.SetOrigin(Origin::User);

		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_USERGRID_SHOWEVENTS, Origin::DeletedUser == commandInfo.GetOrigin()));
		GetIDs(commandInfo.GetIds(), true, Origin::User, commandInfo.GetRBACPrivilege(), isCompatibleWithGuestUser(ID_USERGRID_SHOWEVENTS));

		const bool noID = commandInfo.GetIds().empty();
		if (!noID && showUserRestrictedAccessDialog(USER_PERMISSION_REQUIREMENT))
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Event, Command::ModuleTask::List, commandInfo, { this, g_ActionNameSelectedShowEventsUsers, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(noID ? YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"),_YR("Y2366")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"),_YR("Y2367")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2368")).c_str());
	}
}

void O365Grid::OnShowParentGroups()
{
	ASSERT(GetAutomationName() != GridUtil::g_AutoNameUsersRecycleBin && Origin::DeletedUser != GetOrigin());

	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::User);
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_USERGRID_SHOWPARENTGROUPS));
		GetIDs(commandInfo.GetIds(), true, Origin::User, commandInfo.GetRBACPrivilege(), isCompatibleWithGuestUser(ID_USERGRID_SHOWPARENTGROUPS));

		if (!commandInfo.GetIds().empty())
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::User, Command::ModuleTask::ListOwners, commandInfo, { this, g_ActionNameSelectedShowParentGroups, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"),_YR("Y2369")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2370")).c_str());
	}
}

LicenseContext O365Grid::getLicenseContextForSubModule() const
{
	return Util::ForSubModule(GetLicenseContext());
}

void O365Grid::SetModReverter(std::shared_ptr<IModificationReverter> p_NewReverter)
{
	m_ModReverter = p_NewReverter;
}

void O365Grid::SetModApplyer(std::shared_ptr<IModificationApplyer> p_NewApplyer)
{
	m_ModApplyer = p_NewApplyer;
}

void O365Grid::SetGridRefresher(std::shared_ptr<IGridRefresher> p_NewRefresher)
{
	m_GridRefresher = p_NewRefresher;
}

GridBackendColumn* O365Grid::GetSecondaryStatusColumn() const
{
	return m_SecondaryStatusColumn;
}

void O365Grid::EmptyGrid()
{
	ASSERT(!HasModificationsPending());
	RemoveAllRows();
}

void O365Grid::ResetHasSavedWarningFlags()
{
	m_HasSavedNotReceived = false;
	m_HasSavedOverwitten = false;
}

bool O365Grid::IsRbacAuthorized(GridBackendRow* p_Row) const
{
	return nullptr != p_Row && (nullptr == m_ColRoleDelegationFlag || p_Row->GetField(m_ColRoleDelegationFlag).GetIcon() != m_IconRABCunauthorized);
}

void O365Grid::OnShowMembers()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::Group);
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_GROUPSGRID_SHOWMEMBERS));

		GetIDs(commandInfo.GetIds(), true, Origin::Group, commandInfo.GetRBACPrivilege());

		if (!commandInfo.GetIds().empty())
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Group, Command::ModuleTask::ListSubItems, commandInfo, { this, g_ActionNameSelectedShowMembers, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowMembers_1, _YLOC("No Group ID"), _YR("Y2371")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2372")).c_str());
	}
}

void O365Grid::OnShowOwners()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::Group);
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_GROUPSGRID_SHOWOWNERS));
		GetIDs(commandInfo.GetIds(), true, Origin::Group, commandInfo.GetRBACPrivilege());

		if (!commandInfo.GetIds().empty())
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Group, Command::ModuleTask::ListOwners, commandInfo, { this, g_ActionNameSelectedShowOwners, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowMembers_1, _YLOC("No Group ID"), _YR("Y2373")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2374")).c_str());
	}
}

void O365Grid::OnShowAuthors()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::Group);
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_GROUPSGRID_SHOWAUTHORS));
		GetIDs(commandInfo.GetIds(), true, Origin::Group, commandInfo.GetRBACPrivilege());

		const bool noID = commandInfo.GetIds().empty();
		if (!noID && showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT) && showGroupRestrictedAccessDialog(GROUP_LICENSE_REQUIREMENT)) // Only one dialog will be shown, they don't apply to same session type
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Group, Command::ModuleTask::ListAuthors, commandInfo, { this, g_ActionNameSelectedShowAuthors, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
			AutomationGreenlight(noID ? YtriaTranslate::DoError(O365Grid_OnShowMembers_1, _YLOC("No Group ID"),_YR("Y2375")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"),_YR("Y2376")).c_str());
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2377")).c_str());
	}
}

void O365Grid::OnShowGroupDriveItems()
{
	const bool useSearch = MFCUtil::isALTDOWN() && CRMpipe().IsFeatureSandboxed();
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::Group);
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_GROUPSGRID_SHOWDRIVEITEMS));
		GetIDs(commandInfo.GetIds(), true, Origin::Group, commandInfo.GetRBACPrivilege());

		if (!commandInfo.GetIds().empty())
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Drive, useSearch ? Command::ModuleTask::SearchSubItems : Command::ModuleTask::ListSubItems, commandInfo, { this, g_ActionNameSelectedShowDriveItemsGroups, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowMembers_1, _YLOC("No Group ID"),_YR("Y2378")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2379")).c_str());
	}
}

void O365Grid::OnShowCalendars()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::Group);
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_GROUPSGRID_SHOWCALENDARS));
		GetIDs(commandInfo.GetIds(), true, Origin::Group, commandInfo.GetRBACPrivilege());

		const bool noID = commandInfo.GetIds().empty();
		if (!noID && showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT))
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Event, Command::ModuleTask::List, commandInfo, { this, g_ActionNameSelectedShowEventsGroups, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(noID ? YtriaTranslate::DoError(O365Grid_OnShowMembers_1, _YLOC("No Group ID"),_YR("Y2380")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"),_YR("Y2381")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2382")).c_str());
	}
}

void O365Grid::OnShowConversations()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::Group);
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_GROUPSGRID_SHOWCONVERSATIONS));
		GetIDs(commandInfo.GetIds(), true, Origin::Group, commandInfo.GetRBACPrivilege());

		const bool noID = commandInfo.GetIds().empty();
		if (!noID && showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT))
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Conversation, Command::ModuleTask::List, commandInfo, { this, g_ActionNameSelectedShowConversations, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(noID ? YtriaTranslate::DoError(O365Grid_OnShowMembers_1, _YLOC("No Group ID"), _YR("Y2383")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"), _YR("Y2384")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2385")).c_str());
	}
}

void O365Grid::OnShowSites()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::Group);
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_GROUPSGRID_SHOWSITES));
		GetIDs(commandInfo.GetIds(), true, Origin::Group, commandInfo.GetRBACPrivilege());

		if (!commandInfo.GetIds().empty())
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Sites, Command::ModuleTask::List, commandInfo, { this, g_ActionNameSelectedShowSites, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowMembers_1, _YLOC("No Group ID"), _YR("Y2386")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2387")).c_str());
	}
}

void O365Grid::OnShowLicenses()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		if (GetAutomationName() == GridUtil::g_AutoNameUsersRecycleBin || Origin::DeletedUser == GetOrigin())
			commandInfo.SetOrigin(Origin::DeletedUser);
		else
			commandInfo.SetOrigin(Origin::User);

		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_USERGRID_SHOWLICENSES, Origin::DeletedUser == commandInfo.GetOrigin()));
		GetIDs(commandInfo.GetIds(), true, Origin::User, commandInfo.GetRBACPrivilege(), isCompatibleWithGuestUser(ID_USERGRID_SHOWLICENSES));

		if (!commandInfo.GetIds().empty())
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Licenses, Command::ModuleTask::List, commandInfo, { this, g_ActionNameSelectedShowLicenses, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"),_YR("Y2388")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2389")).c_str());
	}
}

void O365Grid::OnShowManagerHierarchy()
{
#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::User);
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_USERGRID_SHOWMANAGERHIERARCHY));
		GetIDs(commandInfo.GetIds(), true, Origin::User, commandInfo.GetRBACPrivilege(), isCompatibleWithGuestUser(ID_USERGRID_SHOWMANAGERHIERARCHY));

		if (!commandInfo.GetIds().empty())
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::User, Command::ModuleTask::ListManagerHierarchy, commandInfo, { this, g_ActionNameSelectedShowManagerHierarchy, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		else
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"),_YR("Y2390")).c_str());
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2391")).c_str());
	}
#endif
}

void O365Grid::OnShowMessageRules()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		if (GetAutomationName() == GridUtil::g_AutoNameUsersRecycleBin || Origin::DeletedUser == GetOrigin())
			commandInfo.SetOrigin(Origin::DeletedUser);
		else
			commandInfo.SetOrigin(Origin::User);

		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_USERGRID_SHOWMESSAGERULES, Origin::DeletedUser == commandInfo.GetOrigin()));
		GetIDs(commandInfo.GetIds(), true, Origin::User, commandInfo.GetRBACPrivilege(), isCompatibleWithGuestUser(ID_USERGRID_SHOWMESSAGERULES));

		const bool noID = commandInfo.GetIds().empty();
		if (!noID && showUserRestrictedAccessDialog(USER_ULTRA_ADMIN_REQUIREMENT))
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::MessageRules, Command::ModuleTask::List, commandInfo, { this, g_ActionNameSelectedShowMessageRules, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(noID ? YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"),_YR("Y2392")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"),_YR("Y2393")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2394")).c_str());
	}
}

void O365Grid::OnShowUserDetails()
{
	ASSERT(GetAutomationName() != GridUtil::g_AutoNameUsersRecycleBin && Origin::DeletedUser != GetOrigin());

	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_GRID_SHOWUSERDETAILS));
		GetIDs(commandInfo.GetIds(), true, Origin::User, commandInfo.GetRBACPrivilege(), isCompatibleWithGuestUser(ID_GRID_SHOWUSERDETAILS));

		if (!commandInfo.GetIds().empty())
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::User, Command::ModuleTask::ListDetails, commandInfo, { this, g_ActionNameSelectedShowDetailsUsers, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		else
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"),_YR("Y2395")).c_str());
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2396")).c_str());
	}
}

void O365Grid::OnShowGroupDetails()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_GRID_SHOWGROUPDETAILS));
		GetIDs(commandInfo.GetIds(), true, Origin::Group, commandInfo.GetRBACPrivilege());

		if (!commandInfo.GetIds().empty())
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Group, Command::ModuleTask::ListDetails, commandInfo, { this, g_ActionNameSelectedShowDetailsGroups, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		else
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowMembers_1, _YLOC("No Group ID"),_YR("Y2397")).c_str());
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2398")).c_str());
	}
}

void O365Grid::OnShowChannels()
{
	if (openModuleConfirmation())
	{
		CommandInfo info;
		info.SetOrigin(Origin::Group);
		GetTeamIDs(info.GetIds(), true);
		info.SetRBACPrivilege(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CHANNELS_READ);

		info.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(info.GetOrigin()));
		CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Channel, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowChannels, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
}

void O365Grid::OnShowSiteDetails()
{
	ASSERT(false); //FINISH THIS!

	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_GRID_SHOWSITEDETAILS));
		GetIDs(commandInfo.GetIds(), true, Origin::Site, commandInfo.GetRBACPrivilege());

		if (!commandInfo.GetIds().empty())
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Sites, Command::ModuleTask::ListDetails, commandInfo, { this, g_ActionNameSelectedShowDetailsSites, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		else
			AutomationGreenlight(_YERROR("No Site ID"));
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"), _YR("Y2398")).c_str());
	}
}

void O365Grid::OnUpdateShowChannels(CCmdUI* pCmdUI)
{
	auto enable = IsFrameConnected()
		&& GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_GROUP_CHANNELS_READ, [this](GridBackendRow* p_Row) {return GridUtil::IsBusinessGroup(p_Row) && isTeam(p_Row); });

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void O365Grid::InitializeCommands()
{
	CacheGrid::InitializeCommands();

	std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	// USERS
	{
		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_USERGRID_SHOWDRIVEITEMS;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_1, _YLOC("Show OneDrive Files...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_2, _YLOC("OneDrive Files...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_SHOW_DRIVEITEMS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_SHOWDRIVEITEMS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_3, _YLOC("Show OneDrive Files")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_4, _YLOC("Show a detailed hierarchy of all OneDrive files for all selected users.\nThis is like a file explorer for OneDrive.\nManage file permissions, preview files directly, and download files en-masse from this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_USERGRID_SHOWDRIVEITEMS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_31, _YLOC("Show OneDrive Files...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_USERGRID_SHOWDRIVEITEMS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_5, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_USERGRID_SHOWMESSAGES;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_6, _YLOC("Show Messages...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_7, _YLOC("Messages...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_SHOW_MESSAGES_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_SHOWMESSAGES, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_8, _YLOC("Show Messages")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_9, _YLOC("Show a detailed list of all messages for all selected users.\nPreview messages and manage attachments from this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_USERGRID_SHOWMESSAGES_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_10, _YLOC("Show Messages")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_USERGRID_SHOWMESSAGES_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_93, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID = ID_USERGRID_SHOWMAILBOXPERMISSIONS;
			_cmd.m_sMenuText = _T("Show Mailbox Permissions");
			_cmd.m_sToolbarText = _T("Mailbox Permissions...");
			_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_SHOW_MAILBOXPERMISSIONS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_SHOWMAILBOXPERMISSIONS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				_T("Show Mailbox Permissions"),
				_T("Show mailbox permissions for all selected users"),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_USERGRID_SHOWMAILBOXPERMISSIONS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = _T("Show Mailbox Permissions");
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_USERGRID_SHOWMAILBOXPERMISSIONS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = _T("Show in a new window");
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID = ID_USERGRID_SHOWMANAGERHIERARCHY;
			_cmd.m_sMenuText = YtriaTranslate::Do(O365Grid_InitializeCommands_11, _YLOC("Show Management Hierarchy...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_12, _YLOC("Management Hierarchy...")).c_str();
			_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_SHOW_MANAGERHIERARCHY_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_SHOWMANAGERHIERARCHY, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_13, _YLOC("Show Management Hierarchy")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_14, _YLOC("Display the entire organizational reporting structure for all selected users.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_USERGRID_SHOWMANAGERHIERARCHY_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_33, _YLOC("Show Management Hierarchy...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_USERGRID_SHOWMANAGERHIERARCHY_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_94, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}
#endif

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_USERGRID_SHOWCONTACTS;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_15, _YLOC("Show Contacts...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_16, _YLOC("Contacts...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_SHOW_CONTACTS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_SHOWCONTACTS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_17, _YLOC("Show Contacts")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_18, _YLOC("Show the detailed information for all contacts of selected users.\nMass-editing of contact information is possible from this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_USERGRID_SHOWCONTACTS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_32, _YLOC("Show Contacts...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_USERGRID_SHOWCONTACTS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_95, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_USERGRID_SHOWEVENTS;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_23, _YLOC("Show Events...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_24, _YLOC("Events...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_SHOW_EVENTS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_SHOWEVENTS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_25, _YLOC("Show Events")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_26, _YLOC("Show a detailed list of all events connected to all selected users.\nPreview events and manage attachments from this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_USERGRID_SHOWEVENTS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_35, _YLOC("Show Events...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_USERGRID_SHOWEVENTS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_97, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_USERGRID_SHOWLICENSES;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_27, _YLOC("Show Licenses...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_28, _YLOC("Licenses...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_SHOW_LICENSES_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_SHOWLICENSES, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_29, _YLOC("Show License and Service Plan Information")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_30, _YLOC("Show a detailed list of license and service plan information, including provisionning status, for all selected users.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_USERGRID_SHOWLICENSES_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_36, _YLOC("Show Licenses...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_USERGRID_SHOWLICENSES_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_98, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_USERGRID_SHOWPARENTGROUPS;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_37, _YLOC("Show Group Membership...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_38, _YLOC("Group Membership...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_SHOW_PARENTGROUPS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_SHOWPARENTGROUPS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_39, _YLOC("Show Group Membership...")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_40, _YLOC("Show all group memberships for all selected users.\nAdd or remove memberships for any number of users and groups in this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_USERGRID_SHOWPARENTGROUPS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_41, _YLOC("Show Group Membership...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_USERGRID_SHOWPARENTGROUPS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_99, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}
	}

	// GROUPS
	{
		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_GROUPSGRID_SHOWMEMBERS;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_42, _YLOC("Show Group Members...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_43, _YLOC("Members...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_SHOW_MEMBERS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_GROUPSGRID_SHOWMEMBERS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_44, _YLOC("Show Group Members")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_45, _YLOC("Show all members and the entire group hierarchy for all selected groups.\nAdd or remove memberships for any number of users and groups in this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWMEMBERS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_46, _YLOC("Show Group Members...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWMEMBERS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_100, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_GROUPSGRID_SHOWOWNERS;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_47, _YLOC("Show Group Owners...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_48, _YLOC("Owners...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_SHOW_OWNERS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_GROUPSGRID_SHOWOWNERS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_49, _YLOC("Show Group Owners")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_50, _YLOC("Show a detailed list of all owners for all selected groups.\nAdd or remove owners for any number of groups directly from this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWOWNERS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_51, _YLOC("Show Group Owners...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWOWNERS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_101, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_GROUPSGRID_SHOWAUTHORS;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_52, _YLOC("Show Delivery Management...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_53, _YLOC("Delivery Management...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_SHOW_AUTHORS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_GROUPSGRID_SHOWAUTHORS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_54, _YLOC("Show Authorized and Rejected Senders")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_55, _YLOC("Show all authorized and rejected senders for all selected groups.\nPerform delivery management directly from this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWAUTHORS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_56, _YLOC("Show Delivery Management...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWAUTHORS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_102, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_GROUPSGRID_SHOWDRIVEITEMS;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_57, _YLOC("Show Files...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_58, _YLOC("Files...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_SHOW_DRIVEITEMS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_GROUPSGRID_SHOWDRIVEITEMS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_59, _YLOC("Show Files")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_60, _YLOC("Show a detailed hierarchy of all files for all selected groups.\nManage file permissions, preview files directly, and download files en-masse from this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWDRIVEITEMS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_61, _YLOC("Show Files...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWDRIVEITEMS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_103, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_GROUPSGRID_SHOWCALENDARS;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_62, _YLOC("Show Group Events...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_63, _YLOC("Events...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_SHOW_CALENDARS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_GROUPSGRID_SHOWCALENDARS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_64, _YLOC("Show Group Events")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_65, _YLOC("Show a detailed list of all events connected to the selected groups.\nPreview events and manage attachments from this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWCALENDARS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_66, _YLOC("Show Group Events...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWCALENDARS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_104, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_GROUPSGRID_SHOWCONVERSATIONS;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_67, _YLOC("Show Group Conversations...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_68, _YLOC("Conversations...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_SHOW_CONVERSATIONS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_GROUPSGRID_SHOWCONVERSATIONS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_69, _YLOC("Show Group Conversations")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_70, _YLOC("Show a detailed list of all conversations for all selected groups.\nView and edit conversation properties, delete conversations, and drill down into conversation posts directly from this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWCONVERSATIONS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_71, _YLOC("Show Group Conversations...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWCONVERSATIONS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_105, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;

			_cmd.m_nCmdID = ID_GROUPSGRID_SHOW_CHANNELS;
			_cmd.m_sMenuText = YtriaTranslate::Do(GridGroups_InitializeCommands_25, _YLOC("Show Channels...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroups_InitializeCommands_26, _YLOC("Channels...")).c_str();
			_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANNELS_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_GROUPSGRID_SHOW_CHANNELS, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(GridGroups_InitializeCommands_27, _YLOC("Show Channels")).c_str(),
				YtriaTranslate::Do(GridGroups_InitializeCommands_13, _YLOC("Show a detailed list of all channels for the selected groups.\nView and edit channel properties, delete channels, and drill down to channel content directly from this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_GROUPSGRID_SHOW_CHANNELS_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroups_InitializeCommands_14, _YLOC("Show Channels...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_GROUPSGRID_SHOW_CHANNELS_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(GridGroups_InitializeCommands_28, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_GROUPSGRID_SHOWSITES;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_72, _YLOC("Show Group Sites...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_73, _YLOC("Sites...")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(profileName, _cmd);

			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_SHOW_SITES_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_GROUPSGRID_SHOWSITES, hIcon, false);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_74, _YLOC("Show Group Sites")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_75, _YLOC("Show a detailed list of all sites for all selected groups.\nDrill down into sites to see all files and sites directly from this view.")).c_str(),
				_cmd.m_sAccelText);

			// Only for drop down in ribbon
			{
				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWSITES_FRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_76, _YLOC("Show Group Sites...")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);

				_cmd.m_nCmdID = ID_GROUPSGRID_SHOWSITES_NEWFRAME;
				_cmd.m_sMenuText = _YTEXT("");
				_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_106, _YLOC("Show in a new window")).c_str();
				_cmd.m_sAccelText = _YTEXT("");
				g_CmdManager->CmdSetup(profileName, _cmd);
			}
		}
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_GRID_SHOWUSERDETAILS;
		_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_82, _YLOC("Show user details...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_83, _YLOC("User Details...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

        HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_SHOW_USER_DETAILS_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_GRID_SHOWUSERDETAILS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(O365Grid_InitializeCommands_84, _YLOC("Show user details")).c_str(),
			YtriaTranslate::Do(O365Grid_InitializeCommands_85, _YLOC("Show detailed info for the selected users.")).c_str(),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID		= ID_GRID_SHOWUSERDETAILS_FRAME;
			_cmd.m_sMenuText	= _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_86, _YLOC("Show User Details...")).c_str();
			_cmd.m_sAccelText	= _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_GRID_SHOWUSERDETAILS_NEWFRAME;
			_cmd.m_sMenuText	= _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_108, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText	= _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_GRID_SHOWGROUPDETAILS;
		_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_109, _YLOC("Show group details...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_110, _YLOC("Group Details...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_GROUPS_SHOW_GROUP_DETAILS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_GRID_SHOWGROUPDETAILS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(O365Grid_InitializeCommands_111, _YLOC("Show Group Details")).c_str(),
			YtriaTranslate::Do(O365Grid_InitializeCommands_112, _YLOC("Show detailed info for the selected groups.")).c_str(),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_GRID_SHOWGROUPDETAILS_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_114, _YLOC("Show Group Details...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_GRID_SHOWGROUPDETAILS_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_113, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_USERGRID_SHOWMESSAGERULES;
		_cmd.m_sMenuText = YtriaTranslate::Do(O365Grid_InitializeCommands_117, _YLOC("Show Message Rules...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_118, _YLOC("Message Rules...")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_SHOW_MESSAGERULES_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_USERGRID_SHOWMESSAGERULES, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(O365Grid_InitializeCommands_119, _YLOC("Show Message Rules for Selected Users")).c_str(),
			YtriaTranslate::Do(O365Grid_InitializeCommands_120, _YLOC("Show a detailed list of message rules set for the select users' mailboxes.")).c_str(),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_USERGRID_SHOWMESSAGERULES_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_121, _YLOC("Show Message Rules...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_USERGRID_SHOWMESSAGERULES_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_122, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_GRID_SHOWSITEDETAILS;
		_cmd.m_sMenuText = _T("Show site details...");
		_cmd.m_sToolbarText = _T("Site Details...");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_SITES_SHOW_SITE_DETAILS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_GRID_SHOWSITEDETAILS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Show Site Details"),
			_T("Show detailed info for the selected sites."),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_GRID_SHOWSITEDETAILS_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = _T("Show site details...");
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_GRID_SHOWSITEDETAILS_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_113, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	// GENERIC
	{
		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_SHOWCOLUMNS_DEFAULT;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_115, _YLOC("Show Default columns")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_87, _YLOC("Show Default Columns")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_88, _YLOC("Show Default Columns")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_89, _YLOC("Show the sapio365 default set of columns in the grid for the current module.")).c_str(),
				_cmd.m_sAccelText);
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID		= ID_SHOWCOLUMNS_ALL;
			_cmd.m_sMenuText	= YtriaTranslate::Do(O365Grid_InitializeCommands_116, _YLOC("Show All columns")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_90, _YLOC("Show All Columns")).c_str();
			_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(O365Grid_InitializeCommands_91, _YLOC("Show All Columns")).c_str(),
				YtriaTranslate::Do(O365Grid_InitializeCommands_92, _YLOC("Display all non-technical columns in the grid.\nNote: additional 'technical' columns can be made visible on an individual basis from the Grid Manager.")).c_str(),
				_cmd.m_sAccelText);
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID = ID_SHOWLASTQUERYCOLUMN;
			_cmd.m_sMenuText = YtriaTranslate::Do(O365Grid_InitializeCommands_129, _YLOC("'Last Queried On' Column")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_129, _YLOC("'Last Queried On' Column")).c_str();
			_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

			setRibbonTooltip(_cmd.m_nCmdID,
				_T("Show the 'Last Queried On' Column"),
				_T("Display a column containing the date and time on which the data was last retrieved from the server."),
				_cmd.m_sAccelText);
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID = ID_ADDITIONAL_DATA_CONFIG;
			_cmd.m_sMenuText = YtriaTranslate::Do(O365Grid_InitializeCommands_133, _YLOC("Configure Transferred Columns...")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_133, _YLOC("Configure Transferred Columns...")).c_str();
			_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
			g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

			setRibbonTooltip(_cmd.m_nCmdID,
				_T("Configure Transferred Columns"),
				_T("Configure Transferred Columns"),
				_cmd.m_sAccelText);
		}

		{
			{
				CExtCmdItem _cmd;
				_cmd.m_nCmdID = ID_CREATE_GRID_PHOTO_SNAPSHOT;
				_cmd.m_sMenuText = _T("Snapshot");
				_cmd.m_sToolbarText = _T("Snapshot");
				_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
				g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
				g_CmdManager->CmdSetIcon(profileName, ID_CREATE_GRID_PHOTO_SNAPSHOT, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_GRID_CREATE_PHOTO_SNAPSHOT_16X16));

				setRibbonTooltip(_cmd.m_nCmdID,
					_T("Create Snapshot (read-only)"),
					_T("Save your current data set as a file. It can be shared and read by others, but will not allow modifications to be saved to the server. You can also set a password and access restrictions for this file."),
					_cmd.m_sAccelText);
			}

			{
				CExtCmdItem _cmd;
				_cmd.m_nCmdID = ID_CREATE_GRID_RESTORE_SNAPSHOT;
				_cmd.m_sMenuText = _T("Restore Point");
				_cmd.m_sToolbarText = _T("Restore Point");
				_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
				g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
				g_CmdManager->CmdSetIcon(profileName, ID_CREATE_GRID_RESTORE_SNAPSHOT, MFCUtil::getCExtCmdIconFromImageResourceID(IDB_GRID_CREATE_RESTORE_SNAPSHOT_16X16));

				setRibbonTooltip(_cmd.m_nCmdID,
					_T("Create Restore Point (editable)"),
					_T("Pause and save your work so you can resume where you left off.\nThis saves a file of your current data set and all grid settings (filters, sort, etc...) that can be recovered and edited at a later time. You can also set a password for this file."),
					_cmd.m_sAccelText);
			}
		}
	}
}

void O365Grid::ShowPendingPostUpdateError()
{
	if (m_PendingHasAtLeastOneError)
	{
		ASSERT(IsWindowVisible());
		HandlePostUpdateError(m_PendingHasAtLeastOne403Error, m_PendingErrorOccuredDuringSave, m_PendingErrorDescription);
	}
}

void O365Grid::ShowErrorMessage(const wstring& p_Title, const wstring& p_Message, const wstring& p_Details, bool p_AddCheckbox /*= false*/)
{
	YCodeJockMessageBox errorMessage(this,
		DlgMessageBox::eIcon_ExclamationWarning,
		p_Title,
		p_Message + _YTEXT("\n\n") + p_Details,
		_YTEXT(""),// p_Details, // Extended message doesn't show well when using Verification checkbox
		{ { IDOK, YtriaTranslate::Do(O365Grid_HandlePostUpdateError_4, _YLOC("OK")).c_str() } });

	if (p_AddCheckbox)
		errorMessage.SetVerificationText(YtriaTranslate::Do(O365Grid_HandlePostUpdateError_3, _YLOC("Don't show again")).c_str());
	errorMessage.DoModal();

	if (p_AddCheckbox && errorMessage.IsVerificiationChecked())
	{
		auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
			app->SetShowGridErrors(false);
	}

	ensureOneErrorRowIsOnScreen();
}

void O365Grid::HandlePostUpdateError(bool p_HasForbiddenError, bool p_OccuredDuringSave, const wstring& p_ErrorDescription)
{
	auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
	ASSERT(app);
	if (app)
	{
		const auto gridIsEmpty = GetBackendRows().empty();
		wstring title;
		wstring message;
		wstring details;
		if (p_HasForbiddenError)
		{
			if (p_OccuredDuringSave)
			{
				title = YtriaTranslate::Do(O365Grid_HandlePostUpdateError_10, _YLOC("Error while saving")).c_str();
				message = YtriaTranslate::DoError(O365Grid_HandlePostUpdateError_22, _YLOC("At least one \"Access Denied/Forbidden\" error has occurred while saving the changes"),_YR("Y2399")).c_str();
			}
			else
			{
				title = YtriaTranslate::Do(O365Grid_HandlePostUpdateError_11, _YLOC("Error while loading")).c_str();
				message = YtriaTranslate::DoError(O365Grid_HandlePostUpdateError_23, _YLOC("At least one \"Access Denied/Forbidden\" error has occurred while loading the data."),_YR("Y2400")).c_str();
			}

			if (!gridIsEmpty) // Bug #200525.RL.00C11A: the error should show in the empty part of the grid instead of the No Data
				details = YtriaTranslate::Do(O365Grid_HandlePostUpdateError_12, _YLOC("Please see the status column of the grid for more details.")).c_str();
			else 
				details = p_ErrorDescription;
		}
		else
		{
			if (p_OccuredDuringSave)
			{
				title = YtriaTranslate::Do(O365Grid_HandlePostUpdateError_10, _YLOC("Error while saving")).c_str();
				message = YtriaTranslate::DoError(O365Grid_HandlePostUpdateError_24, _YLOC("An error has occurred while saving the changes."),_YR("Y2401")).c_str();
			}
			else
			{
				title = YtriaTranslate::Do(O365Grid_HandlePostUpdateError_11, _YLOC("Error while loading")).c_str();
				message = YtriaTranslate::DoError(O365Grid_HandlePostUpdateError_25, _YLOC("An error has occurred while loading the data."),_YR("Y2402")).c_str();
			}

			if (p_ErrorDescription.empty())
			{
				if (!gridIsEmpty) // Bug #200525.RL.00C11A: the error should show in the empty part of the grid instead of the No Data
					details = YtriaTranslate::Do(O365Grid_HandlePostUpdateError_12, _YLOC("Please see the status column of the grid for more details.")).c_str();
			}
			else
			{
				//if (!gridIsEmpty) // Bug #200525.RL.00C11A: the error should show in the empty part of the grid instead of the No Data
					details = p_ErrorDescription;
			}
		}

		if (gridIsEmpty) // Bug #200525.RL.00C11A: the error should show in the empty part of the grid instead of the No Data
		{
			if (p_ErrorDescription.empty())
				SetProcessText(_YFORMAT(L"%s: %s", title.c_str(), message.c_str()));
			else
				SetProcessText(_YFORMAT(L"%s: %s", title.c_str(), p_ErrorDescription.c_str()));
		}

		if (!m_PendingHasAtLeastOneError)
			Office365AdminApp::SetAutomationLastLoadingError(message);
		m_PendingHasAtLeastOneError = false;

#if USE_FRAME_MESSAGE_BAR_FOR_ERROR
		auto parent = GetParentGridFrameBase();
		if (nullptr != parent)
		{
			parent->ShowErrorMessageBar(title, message, details);
			ensureOneErrorRowIsOnScreen();
		}
#else
		if (app->GetShowGridErrors() && IsWindowVisible())
		{
			ShowErrorMessage(title, message, details, true);
		}
		else
		{
			m_PendingHasAtLeastOneError = true;
			m_PendingErrorOccuredDuringSave = p_OccuredDuringSave;
			m_PendingHasAtLeastOne403Error = p_HasForbiddenError;
			m_PendingErrorDescription = p_ErrorDescription;
		}
#endif
	}
}

void O365Grid::HandlePostUpdateNoError(bool p_OccuredDuringSave)
{
#if USE_FRAME_MESSAGE_BAR_FOR_ERROR
	RowStatusHandler<Scope::O365> o365Handler(*this);
	RowStatusHandler<Scope::ONPREM> onPremHandler(*this);

	std::function<bool(GridBackendRow*)> cond;
	
	if (nullptr != m_SecondaryStatusColumn)
		cond = [&o365Handler, &onPremHandler](GridBackendRow* row) {return o365Handler.IsError(row) || onPremHandler.IsError(row); };
	else
		cond = [&o365Handler](GridBackendRow* row) {return o365Handler.IsError(row); };

	if (!std::any_of(GetBackendRows().begin(), GetBackendRows().end(), cond))
	{
		auto parent = GetParentGridFrameBase();
		if (nullptr != parent)
		{
			if (p_OccuredDuringSave)
			{
				if (m_HasSavedNotReceived && m_HasSavedOverwitten)
					parent->ShowSuccessMessageBar(_T("Success"), _T("Changes successfully saved but some modified values have been overwritten or not received yet. You might want to use Refresh All to display the recent change."), IDB_ICON_SAVED_NOT_RECEIVED);
				else if (m_HasSavedNotReceived)
					parent->ShowSuccessMessageBar(_T("Success"), _T("Changes successfully saved but some modified values have not been received yet. You might want to use Refresh All to display the recent change."), IDB_ICON_SAVED_NOT_RECEIVED);
				else if (m_HasSavedOverwitten)
					parent->ShowSuccessMessageBar(_T("Success"), _T("Changes successfully saved but some modified values have been overwritten."), IDB_ICON_SAVED_OVERWRITTEN);
				else
					parent->ShowSuccessMessageBar(_T("Success"), _T("Changes successfully saved."), IDB_ICON_SAVED);
				ensureOneSavedRowIsOnScreen();
			}
			else
			{
				parent->RemoveMessageBar();
			}
		}
	}
#endif
}

ModuleCriteria O365Grid::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ASSERT(false);
    return ModuleCriteria();
}

Origin O365Grid::GetOrigin() const
{
	if (Origin::NotSet != m_Origin)
		return m_Origin;

	auto parentFrame = GetParentGridFrameBase();
	ASSERT(nullptr != parentFrame);
	m_Origin = nullptr != parentFrame ? parentFrame->GetOrigin() : Origin::NotSet;
	return m_Origin;
}

void O365Grid::OnUpdateShowBusinessUser(CCmdUI* pCmdUI)
{
	ASSERT(0 != pCmdUI->m_nID);
	auto enabled =	IsFrameConnected()
		&& (Origin::DeletedUser != GetOrigin() || isCompatibleWithDeletedUser(pCmdUI->m_nID))
		&& GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, getCommandRBACPrivilege(pCmdUI->m_nID, Origin::DeletedUser == GetOrigin()), [allowGuest = isCompatibleWithGuestUser(pCmdUI->m_nID)](GridBackendRow* p_Row){return GridUtil::IsBusinessUser(p_Row, allowGuest) || GridUtil::IsBusinessAADUserConversationMember(p_Row); });
		
	pCmdUI->Enable(enabled);
	setTextFromProfUISCommand(*pCmdUI);
	//setRibbonTooltipFromProfUISCommand(*pCmdUI);
}

void O365Grid::OnUpdateShowBusinessGroup(CCmdUI* pCmdUI)
{
	auto enabled = IsFrameConnected()
		&& GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, getCommandRBACPrivilege(pCmdUI->m_nID), [](GridBackendRow* p_Row){ return GridUtil::IsBusinessGroup(p_Row); });

	pCmdUI->Enable(enabled);
	setTextFromProfUISCommand(*pCmdUI);
}

void O365Grid::OnUpdateShowBusinessSite(CCmdUI* pCmdUI)
{
	auto enabled = IsFrameConnected()
		&& GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, getCommandRBACPrivilege(pCmdUI->m_nID), [](GridBackendRow* p_Row) {return GridUtil::IsBusinessSite(p_Row); });

	// FIXME: FInish this
	pCmdUI->Enable(FALSE/*enabled*/);
	setTextFromProfUISCommand(*pCmdUI);
}

void O365Grid::UseDefaultActions(UINT defaultActions)
{
	m_defaultActions = defaultActions;
}

UINT O365Grid::GetEditComamndID() const
{
	return ID_MODULEGRID_EDIT;
}

bool O365Grid::HasEditInVewer() const
{
	return HasEdit();
}

bool O365Grid::HasEdit() const
{
	return ACTION_EDIT == (m_defaultActions & ACTION_EDIT);
}

bool O365Grid::HasCreate() const
{
	return ACTION_CREATE == (m_defaultActions & ACTION_CREATE);
}

bool O365Grid::HasDelete() const
{
	return ACTION_DELETE == (m_defaultActions & ACTION_DELETE);
}

bool O365Grid::HasView() const
{
	return ACTION_VIEW == (m_defaultActions & ACTION_VIEW);
}

bool O365Grid::IsMyData() const
{
	return false;
}

O365Grid::SnapshotCaps O365Grid::GetSnapshotCapabilities() const
{
	return { false };
}

void O365Grid::OnCancel(CancelContext& p_Context)
{
	auto muhFrame = dynamic_cast<GridFrameBase*>(GetParent());
	ASSERT(nullptr != muhFrame);
	if (nullptr != muhFrame)
		muhFrame->OnCancel(p_Context);
}

void O365Grid::AutomationGreenlight(const wstring& p_ActionError)
{
	if (AutomatedApp::IsAutomationRunning())
	{
		auto app = dynamic_cast<AutomatedApp*>(AfxGetApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
		{
			auto currentAction	= app->GetCurrentAction();
			auto frameAction	= GetAutomationActionFromFrame();
			ASSERT(currentAction != nullptr);
			if (currentAction != nullptr && (currentAction == frameAction || currentAction->SourceWndMatch(GetParentFrame())))
			{
				if (!p_ActionError.empty())
					currentAction->AddError(p_ActionError.c_str());
				app->SetAutomationGreenLight(app->GetCurrentAction());
			}
		}
	}
}

/*
void O365Grid::AutomationGreenlight()
{
	AutomationGreenlight(wstring());
}
*/

void O365Grid::customizeGridPostProcess()
{
	setHasColumnPresets(true);// before GCVD creation
	ShowColumnsSubsetExclusive(O365Grid::g_ColumnsPresetDefault);

	for (auto c : GetBackend().GetColumns())
	{
		ASSERT(nullptr != c);
		if (nullptr != c)
		{
			if (c->IsCheckbox())
				c->SetReadOnly(true);
			if (c->IsCombo())
				ASSERT(false);
		}
	}

	// GetBackend().SetMakeDisplayRowProcess(std::bind(&O365Grid::processRowRBACstatus, this, std::placeholders::_1)); don't do it at every grid refresh
	m_IconRABCauthorized	= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ROLEDELEGATIONSTATUS_AUTHORIZED));
	m_IconRABCunauthorized	= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ROLEDELEGATIONSTATUS_UNAUTHORIZED));

	m_IconSavedNotReceived	= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SAVED_NOT_RECEIVED));
	m_IconSavedOverwitten	= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SAVED_OVERWRITTEN));
	m_IconSaved				= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SAVED));
	// Only used in message bar. Ensure IDB_ICON_ERROR and GridBackendUtil::ICON_ERROR are identical images.
	// Cannot use the local m_IconError in fields, as GridBackendUtil::ICON_ERROR is tested in GridBackend::createRowData.
	m_IconError				= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ERROR));

	SetFct_ColumnsShowDefault(std::bind(&O365Grid::OnShowColumnsDefault, this));
	SetFct_ColumnsShowAll(std::bind(&O365Grid::OnShowColumnsAll, this));
}

void O365Grid::onUpdateCommand(CCmdUI* pCmdUI)
{
	ASSERT(nullptr != pCmdUI);
	if (nullptr != pCmdUI)
	{
		pCmdUI->Enable();
		setTextFromProfUISCommand(*pCmdUI);
		//setRibbonTooltipFromProfUISCommand(*pCmdUI);
	}
}

void O365Grid::OnShowColumnsDefault()
{
	OnShowColumnsSubsetExclusive(g_ColumnsPresetDefault);
}

void O365Grid::OnUpdateShowDefaultColumns(CCmdUI* pCmdUI)
{
	onUpdateCommand(pCmdUI);
}

void O365Grid::OnShowColumnsAll()
{
	vector<GridBackendColumn*> columns;
	for (auto c : GetBackend().GetColumns())
		if (nullptr != c && !c->HasPresetFlag(g_ColumnsPresetTech) && !GetBackend().IsHierarchyColumn(c) && !c->IsVisible())
			columns.push_back(c);
	if (!columns.empty())
		SetColumnsVisible(columns, true, true);
}
 
void O365Grid::OnUpdateShowColumnsAll(CCmdUI* pCmdUI)
{
	onUpdateCommand(pCmdUI);
}

void O365Grid::OnShowLastQueryColumn()
{
	auto col = GetColumnByUniqueID(BasicGridSetup::g_LastQueryColumnUID);
	ASSERT(nullptr != col);
	if (nullptr != col)
		SetColumnVisible(col, !col->IsVisible(), col->IsVisible() ? (GridBackendUtil::SETVISIBLE_GOTO | GridBackendUtil::SETVISIBLE_DOUPDATE) : GridBackendUtil::SETVISIBLE_DOUPDATE);
}

void O365Grid::OnUpdateShowLastQueryColumn(CCmdUI* pCmdUI)
{
	auto col = GetColumnByUniqueID(BasicGridSetup::g_LastQueryColumnUID);
	pCmdUI->Enable(nullptr != col);
	pCmdUI->SetCheck(nullptr != col && col->IsVisible());
	setTextFromProfUISCommand(*pCmdUI);
}

void O365Grid::OnEditAdditionalDataConfig()
{
	const auto origin = GetAutomationName() == GridUtil::g_AutoNameUsers
		? Origin::User
		: (GetAutomationName() == GridUtil::g_AutoNameGroups
			? Origin::Group
			: Origin::NotSet);

	ASSERT(Origin::User == origin || Origin::Group == origin);
	if (Origin::User == origin || Origin::Group == origin)
	{
		auto& metaDataColumnInfosSetting = getMetaDataColumnInfosSetting(origin);
		if (metaDataColumnInfosSetting)
			editMetaDataColumnInfoSettings(*metaDataColumnInfosSetting, false);
	}
}

void O365Grid::OnUpdateEditAdditionalDataConfig(CCmdUI* pCmdUI)
{
	const auto origin = GetAutomationName() == GridUtil::g_AutoNameUsers
		? Origin::User
		: (GetAutomationName() == GridUtil::g_AutoNameGroups
			? Origin::Group
			: Origin::NotSet);

	ASSERT(Origin::User == origin || Origin::Group == origin);

	auto& metaDataColumnInfosSetting = getMetaDataColumnInfosSetting(origin);
	pCmdUI->Enable(metaDataColumnInfosSetting.is_initialized() ? TRUE : FALSE);
	setTextFromProfUISCommand(*pCmdUI);
}

void O365Grid::OnCreateSnapshotPhoto()
{
	createSnapshot(GridSnapshot::Options::Mode::Photo);
}

void O365Grid::OnCreateSnapshotRestorePoint()
{
	createSnapshot(GridSnapshot::Options::Mode::RestorePoint);
}

void O365Grid::createSnapshot(GridSnapshot::Options::Mode p_Mode)
{
	if ((GridSnapshot::Options::Mode::Photo == p_Mode && GetSnapshotCapabilities().HasPhoto()
			|| GridSnapshot::Options::Mode::RestorePoint == p_Mode && GetSnapshotCapabilities().HasRestore())
		&& GetSession())
	{
		if (GridSnapshot::Options::Mode::Photo == p_Mode && Office365AdminApp::CheckAndWarn<LicenseTag::CreateSnapshot>(this)
			|| GridSnapshot::Options::Mode::RestorePoint == p_Mode && Office365AdminApp::CheckAndWarn<LicenseTag::CreateRestorePoint>(this))
		{
			wstring defaultFileName;
			{
				CString str;
				GetParentGridFrameBase()->GetWindowText(str);
				defaultFileName = str;
				FileUtil::MakeFileNameValid(defaultFileName, false);
			}
			defaultFileName = Product::getInstance().getApplication() + _YTEXT(" - ") + defaultFileName;

			DlgSnapshotOptions dlgOptions(p_Mode, GetSession()->IsRole(), this);
			if (IDOK == dlgOptions.DoModal())
			{
				YFileDialog fileChooser(FALSE
					, GridSnapshot::Options::Mode::Photo == p_Mode ? O365GridSnapshot::g_FileExtensionPhoto.c_str() : O365GridSnapshot::g_FileExtensionRestore.c_str()
					, defaultFileName.c_str()
					, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_NOCHANGEDIR
					, GridSnapshot::Options::Mode::Photo == p_Mode
					? _YTEXTFORMAT(L"Sapio Snapshots (*.%s)|*.%s||", O365GridSnapshot::g_FileExtensionPhoto.c_str(), O365GridSnapshot::g_FileExtensionPhoto.c_str()).c_str()
					: _YTEXTFORMAT(L"Sapio Restore Points (*.%s)|*.%s||", O365GridSnapshot::g_FileExtensionRestore.c_str(), O365GridSnapshot::g_FileExtensionRestore.c_str()).c_str()
					, this);
				if (fileChooser.DoModal() == IDOK)
				{
					const wstring filePath = fileChooser.m_ofn.lpstrFile;

					auto password = dlgOptions.GetPassword();
					const auto& options = dlgOptions.GetOptions();

					bool success = false;

					{
						CWaitCursor _;

						auto frame = GetParentGridFrameBase();
						ASSERT(nullptr != frame);

						// Delete existing file, user has been prompted in CFileDialog
						if (FileUtil::FileExists(filePath))
						{
							auto res = FileUtil::DeleteFiles({ filePath }, false, false);
							ASSERT(0 == res);
							if (0 != res)
							{
								auto err = MFCUtil::GetLastErrorString();
							}
						}

						const auto session				= GetSession();
						const auto& conUser				= session ? session->GetGraphCache().GetCachedConnectedUser() : boost::none;
						const auto& name				= conUser ? conUser->GetDisplayName() : boost::none;
						const auto& techName			= conUser ? conUser->GetUserPrincipalName() : boost::none;
						const auto handleSkuCounters	= session ? boost::YOpt<bool>(session->GetGraphCache().IsHandleSkuCounters()) : boost::none;
						success = GridSnapshot::Create(*this
							, filePath
							, password
							, { session->GetTenantName(true)
								, name ? boost::YOpt<wstring>(*name) : boost::none
								, techName ? boost::YOpt<wstring>(*techName) : boost::none
								, frame ? frame->GetModuleCriteria().Serialize() : Str::g_EmptyString
								, IsMyData() ? boost::YOpt<uint32_t>(GridSnapshot::Flags::MyData) : boost::none
								, handleSkuCounters
								, frame && frame->GetOptions() ? boost::YOpt<wstring>(frame->GetOptions()->Serialize()) : boost::none }
							, std::make_unique<SnapshotDataCustomizer>(*this)
							, options
							, [this](GridBackendRow* p_Row) { return IsRbacAuthorized(p_Row); });
					}

					if (success)
					{
						auto res = YCodeJockMessageBox(this
							, DlgMessageBox::eIcon_Information
							, _T("Success")
							, GridSnapshot::Options::Mode::Photo == p_Mode
							? _YFORMAT(L"Snapshot successfully created:\n%s", filePath.c_str())
							: _YFORMAT(L"Restore Point successfully created:\n%s", filePath.c_str())
							, _YTEXT("")
							, { {IDYES, _T("Open Now")}, {IDOK, _T("Show in Explorer")}, {IDCANCEL, _T("Close")} }).DoModal();
						if (IDYES == res)
							O365GridSnapshot::Load(filePath, GetParentGridFrameBase());
						else if (IDOK == res)
							FileUtil::OpenFolderAndSelectFile(filePath);
					}
					else
					{
						//Delete incomplete file that may have been created.
						if (FileUtil::FileExists(filePath))
						{
							auto res = FileUtil::DeleteFiles({ filePath }, false, false);
							ASSERT(0 == res);
							if (0 != res)
							{
								auto err = MFCUtil::GetLastErrorString();
							}
						}

						YCodeJockMessageBox(this
							, DlgMessageBox::eIcon_Error
							, _T("Error")
							, GridSnapshot::Options::Mode::Photo == p_Mode
							? _YERROR("Unable to create snapshot.")
							: _YERROR("Unable to create restore point.")
							, _YTEXT("")
							, { {IDOK, _T("OK")} }).DoModal();
					}
				}
			}
		}
	}
}

void O365Grid::OnUpdateCreateSnapshotPhoto(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && GetSession() && GetSnapshotCapabilities().HasPhoto());
	setTextFromProfUISCommand(*pCmdUI);
}

void O365Grid::OnUpdateCreateSnapshotRestorePoint(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && GetSession() && GetSnapshotCapabilities().HasRestore());
	setTextFromProfUISCommand(*pCmdUI);
}

const vector<wstring> O365Grid::GetPresetNames(const UINT p_PresetFlags) const
{
	vector<wstring> rvPresetNames;

	if (GridBackendUtil::HasFlag(static_cast<int>(g_ColumnsPresetDefault), static_cast<int>(p_PresetFlags)))
		rvPresetNames.emplace_back(YtriaTranslate::Do(O365Grid_GetPresetNames_1, _YLOC("Default")).c_str());
	if (GridBackendUtil::HasFlag(static_cast<int>(g_ColumnsPresetTech), static_cast<int>(p_PresetFlags)))
		rvPresetNames.emplace_back(YtriaTranslate::Do(O365Grid_GetPresetNames_2, _YLOC("Technical")).c_str());

	return rvPresetNames;
}

void O365Grid::AddGenericColumns()
{
	CacheGrid::AddGenericColumns();
	if (m_MayContainUnscopedUserGroupOrSite)
		addColumnRoleDelegationFlag();
}

bool O365Grid::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore) || CacheGrid::IsRowValidForNoFieldText(p_Row, p_Col);
}

void O365Grid::LoadMorePostProcessRow(GridBackendRow* p_Row)
{
	if (WithLoadMore() && IsRowForLoadMore(p_Row, nullptr))
	{
		if (m_LoadMoreColumns.empty())
			m_LoadMoreColumns = GetColumnsByPresets({ O365Grid::g_ColumnsPresetMore });
		ASSERT(!m_LoadMoreColumns.empty());
		ASSERT(nullptr != p_Row);
		if (nullptr != p_Row)
		{
			const auto rows = GetMultivalueManager().GetExplosionSiblings(p_Row);

			for (auto row : rows)
			{
				ASSERT(nullptr != row);
				if (nullptr != row)
				{
					// Now made in GridUpdater::processExplosionSiblingUpdate() for sapio365, at least
					//if (row != p_Row)
					//{
					//	auto lmFields = row->SetFields(p_Row, m_LoadMoreColumns);// "load more" data must already be added to original row
					//	for (auto f : lmFields)
					//		if (nullptr != f && !f->IsExplosionFragment())
					//			f->BlendFontColor(GridBackendUtil::g_ColorDimmed);
					//}

					// if (!row->IsError()) if rows with errors are not marked as "load more", they are ignored in consecutive load more
					{
						if (GetIsMoreLoadedStatusColumn()->IsDate() && row->HasField(GetIsMoreLoadedStatusColumn()))
							row->GetField(GetIsMoreLoadedStatusColumn()).SetIcon(GetLoadMoreIcon(true));
						else
							row->AddField(GetLoadMoreText(true), GetIsMoreLoadedStatusColumn(), GetLoadMoreIcon(true)).RemoveModified();
						row->SetIsMoreLoaded(true);
					}
				}
			}
		}
	}
}

void O365Grid::InitNewRow(GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_Row);
	if (WithLoadMore() /*&& IsRowForLoadMore(p_Row) nein!*/)
	{
		p_Row->AddField(GetLoadMoreText(false), GetIsMoreLoadedStatusColumn(), GetLoadMoreIcon(false)).RemoveModified();
		p_Row->SetIsMoreLoaded(false);
	}

	if (nullptr != GetSecondaryStatusColumn())
		p_Row->AddField(PooledString::g_EmptyString, GetSecondaryStatusColumn()).RemoveModified();
}

bool O365Grid::IsColumnLoadMore(const GridBackendColumn* p_Column) const
{
	ASSERT(nullptr != p_Column);
	return nullptr != p_Column && p_Column->HasPresetFlag(g_ColumnsPresetMore);
}

bool O365Grid::IsColumnDefault(const GridBackendColumn* p_Column) const
{
	ASSERT(nullptr != p_Column);
	return nullptr != p_Column && p_Column->HasPresetFlag(g_ColumnsPresetDefault);
}

bool O365Grid::IsColumnTechnical(const GridBackendColumn* p_Column) const
{
	ASSERT(nullptr != p_Column);
	return nullptr != p_Column && p_Column->HasPresetFlag(g_ColumnsPresetTech);
}

bool O365Grid::IsColumnObjectID(const GridBackendColumn* p_Column) const
{
	ASSERT(nullptr != p_Column);
	return nullptr != p_Column && p_Column->GetUniqueID() == _YUID(O365_ID);
}

bool O365Grid::isConfiguredForHTMLArea()
{
	AutomationWizard* oz = GetAutomationWizard();
	return oz != nullptr && (!oz->IsEmpty());
}

bool O365Grid::showUserRestrictedAccessDialog(UserRestrictedAccessKind p_UserRestrictedAccessKind)
{
	switch (p_UserRestrictedAccessKind)
	{
	case USER_PERMISSION_REQUIREMENT:
		return showRestrictedAccessDialog(
			{ SessionTypes::g_AdvancedSession, SessionTypes::g_PartnerAdvancedSession },
			DlgRestrictedAccess::Image::PERMISSIONS_REMINDER,
			YtriaTranslate::Do(O365Grid_showUserRestrictedAccessDialog_3, _YLOC("Reminder: You will need the correct permissions.")).c_str(),
			YtriaTranslate::Do(O365Grid_showUserRestrictedAccessDialog_4, _YLOC("This is a friendly reminder that, if your Office365 credentials do not have the correct permissions, you may not have access to the items you select.")).c_str(),
			HideUserPermissionRequirementDialogSetting());
	case USER_ULTRA_ADMIN_REQUIREMENT:
		return showRestrictedAccessDialog(
			{ SessionTypes::g_AdvancedSession, SessionTypes::g_PartnerAdvancedSession },
			DlgRestrictedAccess::Image::GRAPHAPI_SUPPORT,
			YtriaTranslate::Do(O365Grid_showUserRestrictedAccessDialog_2, _YLOC("Reminder: The Microsoft Graph API may not fully support this yet in Advanced sessions.")).c_str(),
			YtriaTranslate::Do(O365Grid_showUserRestrictedAccessDialog_1, _YLOC("At the moment this query may require an Advanced session to work as intended.")).c_str(),
			HideUltraAdminRequirementDialogSetting());
	case USER_ADMIN_REQUIREMENT:
		//FIXME: Use another registry value, e.g. g_KeyHideDLGRAUsersAdmin ?
		// If so, we need to copy paste the call to showRestrictedAccessDialog here :)
		return showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT);
	default:
		ASSERT(false);
		break;
	}

	return true;
}

bool O365Grid::showGroupRestrictedAccessDialog(GroupRestrictedAccessKind p_GroupRestrictedAccessKind)
{
	switch (p_GroupRestrictedAccessKind)
	{
	case GROUP_ADMIN_REQUIREMENT:
		return showRestrictedAccessDialog(
			{ SessionTypes::g_UltraAdmin },
			DlgRestrictedAccess::Image::GRAPHAPI_SUPPORT,
			YtriaTranslate::Do(O365Grid_showGroupRestrictedAccessDialog_1, _YLOC("Reminder: The Microsoft Graph API may not fully support this yet in Ultra Admin mode.")).c_str(),
			YtriaTranslate::Do(O365Grid_showGroupRestrictedAccessDialog_2, _YLOC("At the moment this query may require an Advanced session to work as intended.")).c_str(),
			HideGroupAdminRestrictedAccessDialogSetting());
	case GROUP_LICENSE_REQUIREMENT: // Only known case is Delivery Management.
		// Only one dialog will be shown depending on current session type.
		return showRestrictedAccessDialog(
				{ SessionTypes::g_AdvancedSession, SessionTypes::g_PartnerAdvancedSession },
				DlgRestrictedAccess::Image::EXCLAMATION_MARK,
				YtriaTranslate::Do(O365Grid_showGroupLicenseRestrictedAccessDialog_5, _YLOC("Reminder: You may need a specific license.")).c_str(),
				YtriaTranslate::Do(O365Grid_showGroupLicenseRestrictedAccessDialog_2, _YLOC("This is a friendly reminder that, if your Office365 credentials do not have the correct license, then you may not have access to all the items you select.")).c_str(),
				HideGroupLicenseRestrictedAccessDialogSetting())
			&&
			showRestrictedAccessDialog(
				{ SessionTypes::g_Role },
				DlgRestrictedAccess::Image::EXCLAMATION_MARK,
				YtriaTranslate::Do(O365Grid_showGroupLicenseRestrictedAccessDialog_3, _YLOC("Reminder: Your current role may need a specific license.")).c_str(),
				YtriaTranslate::Do(O365Grid_showGroupLicenseRestrictedAccessDialog_4, _YLOC("This is a friendly reminder that, if your role's Office365 credentials do not have the correct licenses, you may not have access to all the items you select. Contact your administrator if needed.")).c_str(),
				HideGroupLicenseRestrictedAccessDialogSetting());
	default:
		ASSERT(false);
		break;
	}

	return true;
}

RoleDelegationUtil::RBAC_Privilege O365Grid::getCommandRBACPrivilege(UINT p_CmdId, bool p_ForDeletedUser/* = false*/)
{
	RoleDelegationUtil::RBAC_Privilege privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;

	switch (p_CmdId)
	{
	case ID_USERGRID_SHOWDRIVEITEMS:
	case ID_USERGRID_SHOWDRIVEITEMS_FRAME:
	case ID_USERGRID_SHOWDRIVEITEMS_NEWFRAME:
		if (p_ForDeletedUser)
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_RECYCLEBIN_MANAGE;
		else
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_ONEDRIVE_READ;
		break;

	case ID_USERGRID_SHOWMAILBOXPERMISSIONS:
	case ID_USERGRID_SHOWMAILBOXPERMISSIONS_FRAME:
	case ID_USERGRID_SHOWMAILBOXPERMISSIONS_NEWFRAME:
		if (p_ForDeletedUser)
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_RECYCLEBIN_MANAGE;
		else
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MAILBOXPERMISSIONS_READ;
		break;

	case ID_USERGRID_SHOWMESSAGES:
	case ID_USERGRID_SHOWMESSAGES_FRAME:
	case ID_USERGRID_SHOWMESSAGES_NEWFRAME:
		if (p_ForDeletedUser)
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_RECYCLEBIN_MANAGE;
		else
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MESSAGES_READ;
		break;
	case ID_MESSAGEGRID_SHOWBODY:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MESSAGES_SEE_MAIL_CONTENT;
		break;

	case ID_USERGRID_SHOWCONTACTS:
	case ID_USERGRID_SHOWCONTACTS_FRAME:
	case ID_USERGRID_SHOWCONTACTS_NEWFRAME:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_CONTACTS_READ;
		break;

	case ID_USERGRID_SHOWEVENTS:
	case ID_USERGRID_SHOWEVENTS_FRAME:
	case ID_USERGRID_SHOWEVENTS_NEWFRAME:
		if (p_ForDeletedUser)
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_RECYCLEBIN_MANAGE;
		else
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_EVENTS_READ;
		break;

	case ID_USERGRID_SHOWPARENTGROUPS:
	case ID_USERGRID_SHOWPARENTGROUPS_FRAME:
	case ID_USERGRID_SHOWPARENTGROUPS_NEWFRAME:
		// Can always read user groups.
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;
		break;

	case ID_USERGRID_SHOWLICENSES:
	case ID_USERGRID_SHOWLICENSES_FRAME:
	case ID_USERGRID_SHOWLICENSES_NEWFRAME:
		if (p_ForDeletedUser)
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_RECYCLEBIN_MANAGE;
		else
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_LICENSES_EDIT;
		break;

#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
	case ID_USERGRID_SHOWMANAGERHIERARCHY:
	case ID_USERGRID_SHOWMANAGERHIERARCHY_FRAME:
	case ID_USERGRID_SHOWMANAGERHIERARCHY_NEWFRAME:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MANAGEMENTHIERARCHY;
		break;
#endif

	case ID_USERGRID_SHOWMESSAGERULES:
	case ID_USERGRID_SHOWMESSAGERULES_FRAME:
	case ID_USERGRID_SHOWMESSAGERULES_NEWFRAME:
		if (p_ForDeletedUser)
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_RECYCLEBIN_MANAGE;
		else
			privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MESSAGERULES_READ;
		break;

	case ID_GRID_SHOWUSERDETAILS:
	case ID_GRID_SHOWUSERDETAILS_FRAME:
	case ID_GRID_SHOWUSERDETAILS_NEWFRAME:
		// Can always read users.
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;
		break;

	case ID_GROUPSGRID_SHOWMEMBERS:
	case ID_GROUPSGRID_SHOWMEMBERS_FRAME:
	case ID_GROUPSGRID_SHOWMEMBERS_NEWFRAME:
		// Can always read group members.
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;
		break;

	case ID_GROUPSGRID_SHOWOWNERS:
	case ID_GROUPSGRID_SHOWOWNERS_FRAME:
	case ID_GROUPSGRID_SHOWOWNERS_NEWFRAME:
		// Can always read group owners.
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;
		break;

	case ID_GROUPSGRID_SHOWAUTHORS:
	case ID_GROUPSGRID_SHOWAUTHORS_FRAME:
	case ID_GROUPSGRID_SHOWAUTHORS_NEWFRAME:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_DELIVERYMANAGEMENT_READ;
		break;

	case ID_GROUPSGRID_SHOWDRIVEITEMS:
	case ID_GROUPSGRID_SHOWDRIVEITEMS_FRAME:
	case ID_GROUPSGRID_SHOWDRIVEITEMS_NEWFRAME:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_ONEDRIVE_READ;
		break;

	case ID_GROUPSGRID_SHOWCALENDARS:
	case ID_GROUPSGRID_SHOWCALENDARS_FRAME:
	case ID_GROUPSGRID_SHOWCALENDARS_NEWFRAME:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_EVENTS_READ;
		break;

	case ID_GROUPSGRID_SHOWCONVERSATIONS:
	case ID_GROUPSGRID_SHOWCONVERSATIONS_FRAME:
	case ID_GROUPSGRID_SHOWCONVERSATIONS_NEWFRAME:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CONVERSATIONS_READ;
		break;

	case ID_GROUPSGRID_SHOWSITES:
	case ID_GROUPSGRID_SHOWSITES_FRAME:
	case ID_GROUPSGRID_SHOWSITES_NEWFRAME:
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_SITES_READ;
		break;

	case ID_GRID_SHOWGROUPDETAILS:
	case ID_GRID_SHOWGROUPDETAILS_FRAME:
	case ID_GRID_SHOWGROUPDETAILS_NEWFRAME:
		// Can always read groups.
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;
		break;

	default:
		ASSERT(false);
		privilege = RoleDelegationUtil::RBAC_Privilege::RBAC_NONE;
		break;
	}

	return privilege;
}

bool O365Grid::showRestrictedAccessDialog(const set<wstring>& p_RestrictedSessionTypes, DlgRestrictedAccess::Image p_Image, const wstring& p_Title, const wstring& p_Text, const ISetting<bool>& p_Setting)
{
	bool result = true;

	if (GetSession())
	{
		const wstring sessionType = GetSession()->GetSessionType();
		if (p_RestrictedSessionTypes.end() != p_RestrictedSessionTypes.find(sessionType))
			result = Util::ShowRestrictedAccessDialog(p_Image, { p_Title, p_Text }, p_Setting, this);
	}

	return result;
}

AutomationWizardO365* O365Grid::GetAutomationWizard()
{
	return m_AutomationWizard.get();
}

bool O365Grid::IsQueryReferenceRow(GridBackendRow* p_Row) const
{
	return nullptr != p_Row && (GetBackend().IsHierarchyReady() ? 0 == p_Row->GetHierarchyLevel() : true);
}

bool O365Grid::IsLessThanTopHierarchyRowsIndirectlySelected(size_t p_LessThanNumTopRows) const
{
	if (GetBackend().IsHierarchyReady())
	{
		std::set<GridBackendRow*> topLevelRows;
		for (auto row : GetSelectedRows())
		{
			topLevelRows.insert(row->GetTopAncestorOrThis());
			if (topLevelRows.size() >= p_LessThanNumTopRows)
				return false;
		}

		return true;
	}

	ASSERT(false);
	return true;
}

bool O365Grid::GetHTMLAreaJSON(wstring& p_JSON)
{
	bool hasWizardScripts = false;

	auto oz = GetAutomationWizard();
	ASSERT(nullptr != oz);
	if (nullptr != oz)
	{
		hasWizardScripts	= !oz->IsEmpty();
		p_JSON				= oz->GetJSON();
	}

	return hasWizardScripts;
}

bool O365Grid::MinimizeHTMLArea()
{
	bool rvMinimize = true;

	auto oz = dynamic_cast<AutomationWizardO365*>(GetAutomationWizard());
	ASSERT(nullptr != oz);
	if (nullptr != oz)
		rvMinimize = oz->IsEmpty();

	return rvMinimize;
}

void O365Grid::ProcessHTMLAreaData(const YAdvancedHtmlView::IPostedDataTarget::PostedData& p_Data)
{
	auto oz = GetAutomationWizard();
	ASSERT(nullptr != oz);
	if (nullptr != oz)
	{
		for (const auto& d : p_Data)
		{
			if (MFCUtil::StringMatchW(d.first, AutomationWizard::g_ScriptKey))
			{
				oz->Run(d.second, wstring());
				break;
			}
			else if (MFCUtil::StringMatchW(d.first, AutomationWizard::g_FooterNameBtn))
			{
				oz->RunFooter();
				break;
			}
			else if (MFCUtil::StringMatchW(d.first, AutomationWizardO365::g_GoBackstageBtn))
			{
				GetParentGridFrameBase()->showBackstageTab(ID_JOBCENTERCFG);
				break;
			}
		}
	}
}

UINT O365Grid::GetHTMLAreaMinizeImageID()
{
	return IDB_ICON_HTMLGA_ROBOT;
}

COLORREF O365Grid::GetHTMLAreaMinizeBackColour()
{
	return RGB(121, 81, 192);
}

UINT O365Grid::GetViewerMinizeImageID()
{
	return IDB_ICON_GRIDVIEWER;
}

AutomationAction* O365Grid::GetAutomationActionFromFrame() const
{
	AutomationAction* a = nullptr;

	auto frame = GetParentGridFrameBase();
	ASSERT(nullptr != frame);
	if (nullptr != frame)
		a = frame->GetAutomationAction();

	return a;
}

std::set<PooledString> O365Grid::GetIdsOfChildRowsThatAllMatch(const set<GridBackendRow*>& p_ParentRows, GridBackendColumn* p_CustomIDColumn/* = nullptr*/)
{
	return GetIdsOfChildRowsThatAllMatch(p_ParentRows, 0xffffffff, PooledString(), p_CustomIDColumn);
}

std::set<PooledString> O365Grid::GetIdsOfChildRowsThatAllMatch(const set<GridBackendRow*>& p_ParentRows, const UINT p_TestColumnID, const PooledString& p_TestValue, GridBackendColumn* p_CustomIDColumn/* = nullptr*/)
{
	std::set<PooledString> childIds;

	auto idColumn = nullptr == p_CustomIDColumn ? GetColId() : p_CustomIDColumn;

	map<PooledString, size_t> valueOccurences;
	for (const auto& pRow : GetBackendRows())
	{
		if (nullptr != pRow
			/*&& nullptr != pRow->GetParentRow()*/
			&& p_ParentRows.end() != std::find(p_ParentRows.begin(), p_ParentRows.end(), pRow->GetParentRow())
			&& (0xffffffff == p_TestColumnID || p_TestValue == pRow->GetField(p_TestColumnID).GetValueStr()))
		{
			valueOccurences[pRow->GetField(idColumn).GetValueStr()]++;
		}
	}

	for (const auto& valueOccurence : valueOccurences)
	{
		if (valueOccurence.second == p_ParentRows.size()) // Found in every parent
			childIds.insert(valueOccurence.first);
	}

	return childIds;
}

GraphCache& O365Grid::GetGraphCache()
{
    auto frame = GetParentGridFrameBase();
    ASSERT(nullptr != frame && frame->GetSapio365Session());
    if (nullptr != frame && frame->GetSapio365Session())
        return frame->GetSapio365Session()->GetGraphCache();

    static GraphCache dummy(nullptr);
    return dummy;
}

const GraphCache& O365Grid::GetGraphCache() const
{
    auto frame = GetParentGridFrameBase();
    ASSERT(nullptr != frame && frame->GetSapio365Session());
    if (nullptr != frame && frame->GetSapio365Session())
        return frame->GetSapio365Session()->GetGraphCache();

    static GraphCache dummy(nullptr);
    return dummy;
}

AutomatedApp::AUTOMATIONSTATUS O365Grid::processAutomationSetFilterTarget(AutomationAction* i_Action)
{
	AutomatedApp::AUTOMATIONSTATUS rv = AutomatedApp::AUTOMATIONSTATUS_ERROR;

	const bool reset = IsActionResetFilterTarget(i_Action);
	ASSERT(IsActionAddFilterTarget(i_Action) || IsActionRemoveFilterTarget(i_Action) || reset);
	if (IsActionAddFilterTarget(i_Action) || IsActionRemoveFilterTarget(i_Action) || reset)
	{
		auto f = GetParentGridFrameBase();
		if (nullptr != f)
		{
			auto gallery = f->GetGalleryHierchyFilter();
			if (nullptr != gallery)// pas de bras, pas de chocolat - just to make sure we address a relevant grid
			{
				if (reset)
				{
					resetHierarchyObjectTypeToFilterOn_noUpdate();
					rv = AutomatedApp::AUTOMATIONSTATUS_OK;
				}
				else
				{
					const bool	isAdd		= IsActionAddFilterTarget(i_Action);
					const auto& typeName	= i_Action->GetParamValue(AutomationConstant::val().m_ParamNameType);
					const auto& commands	= GetHierarchyFilterObjectTypeCommands();
					for (auto c : commands)
					{
						const auto&	setup = GetHierarchyFilterObjectTypeCommandSetup(c);
						if (MFCUtil::StringMatchW(setup.m_TypeDisplayName, typeName))
						{
							if (isAdd)
								AddHierarchyObjectTypeToFilterOn(setup.m_TypeIndex, false);
							else
								RemoveHierarchyObjectTypeToFilterOn(setup.m_TypeIndex, false);
							rv = AutomatedApp::AUTOMATIONSTATUS_OK;
							break;
						}
					}

					if (AutomatedApp::AUTOMATIONSTATUS_OK != rv)
						i_Action->AddError(YtriaTranslate::DoError(O365Grid_processAutomationSetFilterTarget_1, _YLOC("Type '%1' is unknown in grid %2"),_YR("Y2506"), typeName.c_str(), GetAutomationName().c_str()).c_str());
				}
			}
			else if (!reset)
				i_Action->AddError(YtriaTranslate::DoError(O365Grid_processAutomationSetFilterTarget_2, _YLOC("Grid %1 does not use filters by type"),_YR("Y2507"), GetAutomationName().c_str()).c_str());
		}
		else if (!reset)
			i_Action->AddError(YtriaTranslate::DoError(O365Grid_processAutomationSetFilterTarget_3, _YLOC("Grid %1 does not use filters by type"),_YR("Y2508"), GetAutomationName().c_str()).c_str());
	}
	
	return rv;
}

std::set<boost::int32_t> O365Grid::BasicGridSetupHierarchy::AllUserObjectTypes(bool p_IncludeOrgContact/* = false*/)
{
	if (p_IncludeOrgContact)
		return{ rttr::type::get<BusinessUser>().get_id(), rttr::type::get<BusinessUserGuest>().get_id(), rttr::type::get<BusinessUserUnknown>().get_id(), rttr::type::get<BusinessOrgContact>().get_id() };
	return{ rttr::type::get<BusinessUser>().get_id(), rttr::type::get<BusinessUserGuest>().get_id(), rttr::type::get<BusinessUserUnknown>().get_id() };
}

std::set<int32_t>& O365Grid::BasicGridSetupHierarchy::AppendAllUserObjectTypes(std::set<int32_t>& p_ObjectTypes, bool p_IncludeOrgContact /*= false*/)
{
	auto userTypes = AllUserObjectTypes(p_IncludeOrgContact);
	p_ObjectTypes.insert(std::make_move_iterator(userTypes.begin()), std::make_move_iterator(userTypes.end()));
	return p_ObjectTypes;
}

void O365Grid::LogModifications(const bool p_MainModifications)
{
	ActivityLogger::GetInstance().LogGridModifications(p_MainModifications, GetModifications());
}

bool O365Grid::HasLastQueryColumn() const
{
	return nullptr != GetColumnByUniqueID(BasicGridSetup::g_LastQueryColumnUID);
}

bool O365Grid::IsUserAuthorizedByRoleDelegation(const GridBackendRow* p_Row, const RoleDelegationUtil::RBAC_Privilege p_Privilege) const
{
	bool isAuthorized = false;

	if (GridUtil::IsBusinessUser(p_Row) // Also true for BusinessUserDeleted
		&& nullptr != m_ColId
		&& p_Row->HasField(m_ColId))
	{
		const PooledString userID(p_Row->GetField(m_ColId).GetValueStr());
		ASSERT(!userID.IsEmpty());
		if (!userID.IsEmpty())
		{
			auto user = GridUtil::IsBusinessUserDeleted(p_Row) ? GetGraphCache().GetDeletedUserInCache(userID) : GetGraphCache().GetUserInCache(userID, false);
			isAuthorized = user.HasPrivilege(p_Privilege, GetSession());
		}
	}
	else if (GridUtil::IsBusinessAADUserConversationMember(p_Row))
	{
		// Specific to GridPrivateChannelMembers
		auto userIDCol = GetColumnByUniqueID(_YUID("userId"));
		const PooledString userID(p_Row->GetField(userIDCol).GetValueStr());
		ASSERT(!userID.IsEmpty());
		if (!userID.IsEmpty())
		{
			auto user = GetGraphCache().GetUserInCache(userID, false);
			isAuthorized = user.HasPrivilege(p_Privilege, GetSession());
		}
	}

	return isAuthorized;
}

bool O365Grid::IsUserAuthorizedByRoleDelegation(const wstring& p_UserID, bool p_IsDeletedUser, const RoleDelegationUtil::RBAC_Privilege p_Privilege) const
{
	ASSERT(!p_UserID.empty());
	auto userInCache = p_IsDeletedUser ? GetGraphCache().GetDeletedUserInCache(p_UserID) : GetGraphCache().GetUserInCache(p_UserID, false);
	return !p_UserID.empty() && userInCache.HasPrivilege(p_Privilege, GetSession());
}

bool O365Grid::IsGroupAuthorizedByRoleDelegation(const GridBackendRow* p_Row, const RoleDelegationUtil::RBAC_Privilege p_Privilege) const
{
	bool isAuthorized = false;

	if (GridUtil::IsBusinessGroup(p_Row) // Also true for BusinessGroupDeleted
		&& nullptr != m_ColId
		&& p_Row->HasField(m_ColId))
		isAuthorized = IsGroupAuthorizedByRoleDelegation(p_Row->GetField(m_ColId).GetValueStr(), GridUtil::IsBusinessGroupDeleted(p_Row), p_Privilege);

	return isAuthorized;
}

bool O365Grid::IsGroupAuthorizedByRoleDelegation(const wstring& p_GroupID, bool p_IsDeletedGroup, const RoleDelegationUtil::RBAC_Privilege p_Privilege) const
{
	ASSERT(!p_GroupID.empty());
	auto groupInCache = p_IsDeletedGroup ? GetGraphCache().GetDeletedGroupInCache(p_GroupID) : GetGraphCache().GetGroupInCache(p_GroupID);
	return !p_GroupID.empty() && groupInCache.HasPrivilege(p_Privilege, GetSession());
}

bool O365Grid::IsSiteAuthorizedByRoleDelegation(const GridBackendRow* p_Row, const RoleDelegationUtil::RBAC_Privilege p_Privilege) const
{
	bool isAuthorized = false;

	if (GridUtil::IsBusinessSite(p_Row)
		&& nullptr != m_ColId
		&& p_Row->HasField(m_ColId))
	{
		const PooledString siteID(p_Row->GetField(m_ColId).GetValueStr());
		ASSERT(!siteID.IsEmpty());
		if (!siteID.IsEmpty())
			isAuthorized = GetGraphCache().GetSiteInCache(siteID).HasPrivilege(p_Privilege, GetSession());
	}

	return isAuthorized;
}

bool O365Grid::IsUsingRoleDelegation() const
{
	return m_IsUsingRoleDelegation;
}

bool O365Grid::InitRoleDelegation()
{
	auto session = GetSession();
	m_IsUsingRoleDelegation = session && session->IsUseRoleDelegation();
	return m_IsUsingRoleDelegation;
}

void O365Grid::AddRoleDelegationFlag(GridBackendRow* p_Row, const BusinessObject& p_Object)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row && IsUsingRoleDelegation() && m_MayContainUnscopedUserGroupOrSite)
	{
		ASSERT(nullptr != m_ColRoleDelegationFlag);
		auto colID = GetColId();
		if (nullptr != colID)
		{
			if (p_Object.IsRBACAuthorized())
			{
				p_Row->AddField(YtriaTranslate::Do(NalpeironLicenseManager_NalpeironLicenseManager_29, _YLOC("Authorized")).c_str(), m_ColRoleDelegationFlag, m_IconRABCauthorized);
				p_Row->SetTechHidden(false);// if the row was tech-hidden for reasons other than RBAC, too bad! alternative: restore filter on m_ColRoleDelegationFlag (OK as long as RBAC not enabled in hierarchical submodule grids)
			}
			else
			{				
				p_Row->AddField(YtriaTranslate::Do(O365Grid_AddRoleDelegationFlag_2, _YLOC("Unauthorized")).c_str(), m_ColRoleDelegationFlag, m_IconRABCunauthorized);
				p_Row->BlendTextColor(GridBackendUtil::g_ColorDimmed, false);

				auto frame = GetParentGridFrameBase();
				ASSERT(nullptr != frame);
				if (nullptr != frame)
				{
					const bool hide = frame->IsRBACHideUnscopedObjects();
					p_Row->SetTechHidden(hide);// if the row was tech-hidden for reasons other than RBAC, too bad! alternative: restore filter on m_ColRoleDelegationFlag
					if (hide)
						++m_RBACHiddenCount;
				}
			}
		}
	}
}

void O365Grid::AddRoleDelegationFlagFromParent(GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_Row && nullptr != p_Row->GetParentRow());

	if (nullptr != p_Row)
	{
		auto parentRow = p_Row->GetParentRow();
		if (nullptr != parentRow)
		{
			if (parentRow->HasField(m_ColRoleDelegationFlag))
			{
				auto& field = p_Row->AddField(parentRow->GetField(m_ColRoleDelegationFlag));
				if (field.GetIcon() == m_IconRABCunauthorized)
					p_Row->BlendTextColor(GridBackendUtil::g_ColorDimmed, false);
				if (parentRow->IsTechHidden())
				{
					p_Row->SetTechHidden(true);
					++m_RBACHiddenCount;
				}
			}
			else
			{
				p_Row->RemoveField(m_ColRoleDelegationFlag);
				p_Row->SetTechHidden(false);
			}
		}
	}
}

void O365Grid::UpdateRBACinfo(const bool p_HideOutOfScope)
{
	resetRBACHiddenCounter();
	if (p_HideOutOfScope)
	{
		for (auto row : GetBackendRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row)
			{
				const bool hide = row->GetField(m_ColRoleDelegationFlag).GetIcon() == m_IconRABCunauthorized;
				row->SetTechHidden(hide);
				if (hide)
					++m_RBACHiddenCount;
			}
		}
	}
	else
	{
		for (auto row : GetBackendRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row)
				row->SetTechHidden(false);// if the row was tech-hidden for reasons other than RBAC, too bad! alternative: restore filter on m_ColRoleDelegationFlag
		};
	}

	updateRBACStatusBarText();

	UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void O365Grid::updateRBACStatusBarText()
{
	if (m_RBACHiddenCount > 0)
		SetStatusBarOptionalText(YtriaTranslate::Do(O365Grid_updateRBACStatusBarText_1, _YLOC("RBAC Hidden Items: %1"), Str::getStringFromNumber(m_RBACHiddenCount).c_str()));
	else
		SetStatusBarOptionalText(_YTEXT(""));
}

void O365Grid::resetRBACHiddenCounter()
{
	m_RBACHiddenCount = 0;
}

void O365Grid::addMember(
	map<wstring, set<wstring>>& p_ItemsToMembers,
	GridBackendColumn* p_pColLeft,
	GridBackendColumn* p_pColRight,
	GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_pColLeft);
	ASSERT(nullptr != p_pColRight);
	ASSERT(nullptr != p_Row);
	if(nullptr != p_pColLeft && nullptr != p_pColRight && nullptr != p_Row)
	{
		wstring StrRight(p_Row->GetField(p_pColRight).GetValueStr());
		wstring StrLeft(p_Row->GetField(p_pColLeft).GetValueStr());
		if(!StrLeft.empty())
		{
			auto itTest = p_ItemsToMembers.find(StrLeft);
			if(itTest == p_ItemsToMembers.end())
				p_ItemsToMembers[StrLeft] = set<wstring>();

			if(!StrRight.empty() && StrLeft != StrRight)
				p_ItemsToMembers[StrLeft].insert(StrRight);
		}
	}
}

GridFrameBase* O365Grid::GetParentGridFrameBase() const
{
	auto frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
	ASSERT(nullptr != frame);
	return frame;
}

bool O365Grid::openModuleConfirmation()
{
	if (HasModificationsPending())
	{
		YCodeJockMessageBox confirmation(
			this,
			DlgMessageBox::eIcon_Question,
			YtriaTranslate::Do(GridFrameBase_closeConfirmation_1, _YLOC("Changes pending")).c_str(),
			YtriaTranslate::Do(O365Grid_openModuleConfirmation_4, _YLOC("Some changes have not been saved. If you continue, they will be lost.")).c_str(),
			_YTEXT(""),
			{ { IDOK, YtriaTranslate::Do(O365Grid_openModuleConfirmation_3, _YLOC("Continue")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridFrameBase_closeConfirmation_5, _YLOC("Cancel")).c_str() } });

		if (IDCANCEL == confirmation.DoModal())
			return false;

		RevertAllModifications(true);
	}

	return true;
}

bool O365Grid::HasModificationsPending(bool inSelectionOnly/* = false*/)
{
	bool result = false;
	if (IsGridModificationsEnabled())
	{
		if (inSelectionOnly)
		{
			for (auto row : GetSelectedRows())
			{
				if (GetModifications().IsModified(row))
				{
					result = true;
					break;
				}
			}

#if 0//def _DEBUG
			// Just check row status are synced to modifications.

			// there's a very short instant where both can be out-of-sync, so use kind of a counter to assert only if the situation lasts...
			if (result != std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [](GridBackendRow* p_Row) {return p_Row->IsModified() || p_Row->IsCreated() || p_Row->IsDeleted(); }))
				++m_HasModificationsPendingAssertCounter;
			else
				m_HasModificationsPendingAssertCounter = 0;

			// If this fails, row status ain't synced to modifications.
			ASSERT(m_HasModificationsPendingAssertCounter < 5);
#endif
		}
		else
		{
			result = GetModifications().HasModifications();
		}
	}

	return result;
}

bool O365Grid::HasModificationsPendingInParentOfSelected()
{
	if (IsGridModificationsEnabled())
	{
		if (GetBackend().IsHierarchyReady())
		{
			set<GridBackendRow*> selectedRowsParents;
			for (auto row : GetSelectedRows())
			{
				auto topRow = row->GetTopAncestorOrThis();
				if (selectedRowsParents.insert(topRow).second && GetModifications().HasAnyRowModificationOnOrUnder(topRow))
					return true;
			}
		}
		else
		{
			return HasModificationsPending(true);
		}
	}

	return false;
}

bool O365Grid::HasRevertableModificationsPending(bool inSelectionOnly /*= false*/)
{
	return HasModificationsPending(inSelectionOnly);
}

const O365Grid::MetadataColumnFilter& O365Grid::getMetadataColumnFilter(Origin p_Origin) const
{
	static std::map<Origin, MetadataColumnFilter> filters;

	auto& filter = filters[p_Origin];

	if (!filter.m_Filter)
	{
		filter.m_DefaultColumnIds =
			Origin::User == p_Origin	? std::set<PooledString>{ _YUID(O365_ID), _YUID(O365_USER_DISPLAYNAME), _YUID(O365_USER_USERPRINCIPALNAME) }
										: std::set<PooledString>{ _YUID(O365_ID), _YUID(O365_GROUP_DISPLAYNAME), _YUID(O365_GROUP_ISTEAM), _YUID(O365_GROUP_GROUPTYPE) };

		filter.m_IgnoredColumnIds =
			Origin::User == p_Origin	? std::set<PooledString>{ BasicGridSetup::g_LastQueryColumnUID }
										: std::set<PooledString>{ BasicGridSetup::g_LastQueryColumnUID, _YUID("statusAllowToAddGuests"), _YUID("expirationPolicyStatus"), _YUID("expireOn") };

		filter.m_Filter = [](const MetadataColumnFilter& p_Filter, GridBackendColumn* p_Col)
		{
			const bool ret = nullptr != p_Col
				&& !p_Col->GetGridBackend()->IsSystem(p_Col)
				&& !p_Col->IsDynamic()
				&& !p_Col->IsAnnotation()
				&& p_Col != p_Col->GetGridBackend()->GetCacheGrid()->GetIsMoreLoadedStatusColumn()
				&& p_Col != p_Col->GetGridBackend()->GetCacheGrid()->GetColumnObjectType()
				&& p_Filter.m_IgnoredColumnIds.end() == p_Filter.m_IgnoredColumnIds.find(p_Col->GetUniqueID())
				&& p_Filter.m_DefaultColumnIds.end() == p_Filter.m_DefaultColumnIds.find(p_Col->GetUniqueID());
			return ret;
		};
	}

	return filter;
}

boost::YOpt<vector<MetaDataColumnInfo>> O365Grid::getMetaDataColumnInfoFor(Origin p_Origin)
{
	boost::YOpt<vector<MetaDataColumnInfo>> metaDataColumnInfo;

	if (Origin::User == p_Origin || Origin::Group == p_Origin)
	{
		auto& savedSettings = getMetaDataColumnInfosSetting(p_Origin);
		if (savedSettings)
		{
			if (AutomatedApp::IsAutomationRunning())
			{
				if (GetAutomationName() == GridUtil::g_AutoNameUsers || GetAutomationName() == GridUtil::g_AutoNameGroups)
				{
					auto app = dynamic_cast<AutomatedApp*>(AfxGetApp());
					ASSERT(nullptr != app);
					if (nullptr != app)
					{
						auto currentAction = app->GetCurrentAction();
						ASSERT(nullptr != currentAction);
						if (nullptr != currentAction)
						{
							const auto& columnFilter = getMetadataColumnFilter(p_Origin);
							const auto actionColumnIDs = Str::explodeIntoVector(currentAction->GetParamValue(g_ColumnTransport), wstring(_YTEXT(";")), false, true);
							if (!actionColumnIDs.empty())
							{
								metaDataColumnInfo.emplace();
								for (const auto& colID : actionColumnIDs)
								{
									auto c = GetColumnByUniqueID(colID);
									if (columnFilter.Filter(c))
									{
										metaDataColumnInfo->emplace_back(c);
									}
									else
									{
										if (nullptr == c)
											currentAction->AddError(_YFORMAT(L"Unknown column '%s'.", colID.c_str()).c_str(), TraceOutput::TRACE_WARNING);
										else
											currentAction->AddError(_YFORMAT(L"Ignored column '%s'.", colID.c_str()).c_str(), TraceOutput::TRACE_WARNING);
									}
								}
							}

							if (!metaDataColumnInfo && !savedSettings->ShouldAskAgain())
								metaDataColumnInfo = savedSettings->Get(*this, columnFilter.ToFunc());
						}
					}
				}
			}
			else
			{
				if (savedSettings->ShouldAskAgain() && (GetAutomationName() == GridUtil::g_AutoNameUsers || GetAutomationName() == GridUtil::g_AutoNameGroups))
					metaDataColumnInfo = editMetaDataColumnInfoSettings(*savedSettings, true);
				else
					metaDataColumnInfo = savedSettings->Get(*this, getMetadataColumnFilter(p_Origin).ToFunc());
			}
		}
	}

	return metaDataColumnInfo;
}

boost::YOpt<MetaDataColumnInfosSetting>& O365Grid::getMetaDataColumnInfosSetting(Origin p_Origin)
{
	ASSERT(Origin::User == p_Origin || Origin::Group == p_Origin);
	auto& setting = Origin::User == p_Origin ? m_MetaDataColumnInfosSettingUser : m_MetaDataColumnInfosSettingGroup;
	if (!setting)
	{
		if (Origin::User == p_Origin && GetAutomationName() == GridUtil::g_AutoNameUsers)
			setting.emplace(Origin::User);
		else if (Origin::Group == p_Origin && GetAutomationName() == GridUtil::g_AutoNameGroups)
			setting.emplace(Origin::Group);
	}

	return setting;
}

boost::YOpt<vector<MetaDataColumnInfo>> O365Grid::editMetaDataColumnInfoSettings(MetaDataColumnInfosSetting& p_MetaDataColumnInfosSetting, bool p_ForModuleOpening)
{
	boost::YOpt<vector<MetaDataColumnInfo>> metaDataColumnInfo;
	const auto origin = p_MetaDataColumnInfosSetting.GetOrigin();
	ASSERT(Origin::User == origin || Origin::Group == origin);
	ASSERT(p_ForModuleOpening || GetAutomationName() == GridUtil::g_AutoNameUsers && Origin::User == origin || GetAutomationName() == GridUtil::g_AutoNameGroups && Origin::Group == origin);

	const auto& columnFilter = getMetadataColumnFilter(origin);
	metaDataColumnInfo = p_MetaDataColumnInfosSetting.Get(*this, columnFilter.ToFunc());

	vector<GridBackendColumn*> selectedColumns;
	if (metaDataColumnInfo)
	{
		selectedColumns.reserve(metaDataColumnInfo->size());
		for (const auto& c : *metaDataColumnInfo)
		{
			auto col = GetColumnByUniqueID(c.GetPropertyName());
			ASSERT(columnFilter.Filter(col));
			if (columnFilter.Filter(col))
				selectedColumns.push_back(col);
		}
	}

	wstring columnTitlesList;
	const auto& cfg = Origin::User == origin ? (BusinessConfigurationBase&)BusinessUserConfiguration::GetInstance() : (BusinessConfigurationBase&)BusinessGroupConfiguration::GetInstance();
	for (auto& uniqueId : columnFilter.m_DefaultColumnIds)
	{
		if (uniqueId == _YUID(O365_ID))
			columnTitlesList += g_TitleGraphID;
		else
			columnTitlesList += cfg.GetTitle(uniqueId);
		columnTitlesList += _YTEXT(", ");
	}

	if (!columnTitlesList.empty())
	{
		columnTitlesList.pop_back(); // Remove trailing space.
		columnTitlesList.pop_back(); // Remove trailing comma.
	}

	const wstring text = Origin::User == origin
		? YtriaTranslate::Do(O365Grid_editMetaDataColumnInfoSettings_9, _YLOC("Select the additional User columns you want to keep in your sub-views.\n[Included by default: %1]"), columnTitlesList.c_str())
		: YtriaTranslate::Do(O365Grid_editMetaDataColumnInfoSettings_10, _YLOC("Select the additional Group columns you want to keep in your sub-views.\n[Included by default: %1]"), columnTitlesList.c_str());

	DlgSelectColumns dlgSelect(selectedColumns, columnFilter.ToFunc(), this);
	dlgSelect.SetTitle(_T("Configure Transferred Columns"));
	dlgSelect.SetIntroText(text);
	if (p_ForModuleOpening)
	{
		dlgSelect.SetHideCancelButton(true);
		dlgSelect.SetOkButtonText(_T("Continue"));
		dlgSelect.SetVerificationText(YtriaTranslate::Do(O365Grid_editMetaDataColumnInfoSettings_7, _YLOC("Don't ask again - You can access this under the Column Map button")).c_str(), false, selectedColumns.empty());
		dlgSelect.SetEnableOkIfNoSelection(true);
	}
	else
	{
		dlgSelect.SetOkButtonText(_T("OK"));
		dlgSelect.SetCancelButtonText(YtriaTranslate::Do(O365Grid_editMetaDataColumnInfoSettings_8, _YLOC("Cancel")).c_str());
		dlgSelect.SetVerificationText(_T("Don't ask again - Turn off automatic display of this dialog"), !p_MetaDataColumnInfosSetting.ShouldAskAgain(), false);
		dlgSelect.SetEnableOkIfNoSelection(true);
	}

	bool save = false;
	bool dontAskAgain = false;
	if (IDOK == dlgSelect.DoModal())
	{
		if (!metaDataColumnInfo)
			metaDataColumnInfo.emplace();

		if (metaDataColumnInfo)
		{
			metaDataColumnInfo->clear();
			for (auto c : dlgSelect.GetSelectedColumns())
				metaDataColumnInfo->emplace_back(c);
				save = true;
			if (dlgSelect.IsVerificationChecked())
				dontAskAgain = true;
		}
	}

	if (save)
		p_MetaDataColumnInfosSetting.Set(metaDataColumnInfo, dontAskAgain);

	return metaDataColumnInfo;
}

bool O365Grid::annotationStoreInDB(GridAnnotation& p_Annotation)
{
	p_Annotation.m_Grid_VOL = this;
	return AnnotationManager::GetInstance().Write(p_Annotation, GetSession());
}

bool O365Grid::annotationsGetFromDB(const wstring& p_RowPK, const set<wstring>& p_ColumnIDs, vector<GridAnnotation>& p_Annotations)
{
	// FIXME: If snapshot, we could load annotations using the tenant saved in it.
	return AnnotationManager::GetInstance().LoadAnnotations(annotationGetModuleName(), p_RowPK, p_ColumnIDs, GetSession(), p_Annotations);
}

void O365Grid::annotationsRefreshFromCloud()
{
	DlgDoubleProgressCommon::RunModal(_T("Refreshing Comments..."), this, [session = GetSession()](std::shared_ptr<DlgDoubleProgressCommon> prog) mutable
	{
		prog->SetCounterTotal1(1);
		AnnotationManager::GetInstance().InitAfterSessionOrLicenseWasSet(session, *prog);
	});
}

bool O365Grid::annotationDeleteFromDB(GridAnnotation& p_Annotation)
{
	ASSERT(AnnotationUtil::AnnotationStatus::STATUS_REMOVED == p_Annotation.m_Status);
	return AnnotationManager::GetInstance().Write(p_Annotation, GetSession());
}

GridAnnotation O365Grid::AnnotationGetFromDB(const int64_t p_ID)
{
	return AnnotationManager::GetInstance().Read(p_ID, GetSession());
}

bool O365Grid::canRefreshEditOrDeleteAnnotations() const
{
	return (bool)GetSession();
}

bool O365Grid::annotationsLoadAllModule(vector<GridAnnotation>& p_Annotations)
{
	if (GetSession())
		return AnnotationManager::GetInstance().UpdateFromCloudAndLoadAnnotations(annotationGetModuleName(), GetSession(), p_Annotations, this);
	return false;
}

void O365Grid::annotationsWriteColumnSettings(const wstring& p_Module, const wstring& p_UID, const wstring& p_Settings)
{
	AnnotationManager::GetInstance().WriteColumnSettings(p_Module, p_UID, p_Settings, GetSession());
}

wstring O365Grid::annotationsGetColumnSettings(const wstring& p_Module, const wstring& p_UID)
{
	return AnnotationManager::GetInstance().GetColumnSettings(p_Module, p_UID, GetSession());
}

wstring O365Grid::GetUserID() const
{
	auto session = GetSession();
	if (session)
		return session->IsUltraAdmin() ? Sapio365Session::GetUltraAdminID() : session->GetConnectedUserID();

	ASSERT(false);
	return _YTEXT("");
}

bool O365Grid::isTeam(const GridBackendRow* p_Row) const
{
	GridBackendColumn* pColIsTeam = GetColumnByUniqueID(_YUID(O365_GROUP_ISTEAM));
	ASSERT(nullptr != pColIsTeam);
	// A team has an icon in the "Is A Team" column.
	// BEWARE: If ever we decide to put an icon even when it's not a team, this doesn't work anymore...
	return nullptr != p_Row && nullptr != pColIsTeam && !p_Row->IsGroupRow() && p_Row->GetField(pColIsTeam).GetIcon() != NO_ICON;
}

wstring O365Grid::makeRowName(const GridBackendRow* p_Row, const GridBackendColumn* p_Column) const
{
	ASSERT(nullptr != p_Row);
	return makeRowName(nullptr != p_Row ? p_Row->GetField(p_Column).ToString() : _YTEXT(""));
}

wstring O365Grid::makeRowName(const wstring& p_Name) const
{
	return makeRowName({}, p_Name);
}

wstring O365Grid::makeRowName(const GridBackendRow* p_Row, const vector<pair<wstring, GridBackendColumn*>> p_HierarchyTypeAndNames, GridBackendColumn* p_ColumnMainName) const
{
	return makeRowName(p_Row, p_HierarchyTypeAndNames, nullptr != p_Row ? p_Row->GetField(p_ColumnMainName).ToString() : _YTEXT(""));
}

wstring O365Grid::makeRowName(const GridBackendRow* p_Row, const vector<pair<wstring, GridBackendColumn*>> p_HierarchyTypeAndNames, const wstring& p_Name) const
{
	vector<pair<wstring, wstring>> htnStr;

	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
		for (const auto& p : p_HierarchyTypeAndNames)
			htnStr.push_back({ p.first, p_Row->GetField(p.second).ToString() });

	return makeRowName(htnStr, p_Name);
}

wstring O365Grid::makeRowName(const vector<std::pair<wstring, wstring>>& p_NameHierarchy, const wstring& p_Name) const
{
	wstring rvRowName;

	// from top to bottom (e.g. group - member)
	size_t k = 1, size = p_NameHierarchy.size();
	for (const auto& p : p_NameHierarchy)
	{
		if (1 == k)
			rvRowName = _YTEXT("[");
		rvRowName += _YFORMAT(L"%s: %s", p.first.c_str(), p.second.c_str());// object type: object name - mostly
		rvRowName += (k < size) ? _YTEXT(" - ") : _YTEXT("] ");
		k++;
	}

	rvRowName += p_Name;

	if (rvRowName.empty())
		rvRowName = _YTEXT("?");

	return rvRowName;
}

wstring O365Grid::annotationGetReferenceName(const GridBackendRow* p_Row) const
{
	return GetName(p_Row);
}

void O365Grid::GetGACustomText(CString& p_CustomText)
{
	ASSERT(nullptr != GetGroupArea());

	p_CustomText.Empty();

	auto frame = GetParentGridFrameBase();
	ASSERT(nullptr != frame);
	if (nullptr != frame)
	{
		// In GridFrameBase, the creation date is the date of creation of the frame.
		// This date is used to initialize the refresh date.
		// There are two condition we can get from this. First, we don't want to show anything
		// if the went the frame is initializing. Two, we don't want to show anything when the 
		// Refresh All has been clicked (In this case, the creation date will be earlier then the refresh date).
		if (frame->GetCreationDate() > frame->GetLastRefreshDate())
		{
			CString dateStr;
			DateTimeFormat format;
			format.m_TimeFormat.m_DisplayFlags = TimeFormat::HOURS | TimeFormat::MINUTES | TimeFormat::SECONDS;
			TimeUtil::GetInstance().ConvertDateToText(format, frame->GetLastRefreshDate(), dateStr);
			p_CustomText = YtriaTranslate::Do(O365Grid_GetGACustomText_1, _YLOC("Currently showing data from cache\nLast updated on %1"), dateStr).c_str();
		}
	}
}

const boost::YOpt<MetaDataColumnInfos>& O365Grid::getMetaDataColumnInfo() const
{
	return m_MetaDataColumnInfo;
}

void O365Grid::SetMetaDataColumnInfo(const boost::YOpt<MetaDataColumnInfos>& p_MetaDataColumnInfo)
{
	m_MetaDataColumnInfo = p_MetaDataColumnInfo;
}

std::function<wstring(const wstring&)> O365Grid::AnnotationsGetModuleNameTranslator() const
{
	return GridUtil::ModuleName;
}

void O365Grid::OnRowSavedNotReceived(GridBackendRow* p_Row, bool i_RefreshGrid, const wstring& i_StatusText)
{
	ASSERT(p_Row != nullptr);
	if (p_Row != nullptr)
	{
		p_Row->SetEditSaved(true);
		ASSERT(NO_ICON != m_IconSavedNotReceived);
		ASSERT(!i_StatusText.empty());
		GetBackend().OnRowChange(p_Row, i_StatusText, m_IconSavedNotReceived);
		m_HasSavedNotReceived = true;
	}
	if (i_RefreshGrid)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void O365Grid::OnRowSavedOverwritten(GridBackendRow* p_Row, bool i_RefreshGrid, const wstring& i_StatusText)
{
	ASSERT(p_Row != nullptr);
	if (p_Row != nullptr)
	{
		p_Row->SetEditSaved(true);
		ASSERT(NO_ICON != m_IconSavedOverwitten);
		ASSERT(!i_StatusText.empty());
		GetBackend().OnRowChange(p_Row, i_StatusText, m_IconSavedOverwitten);
		m_HasSavedOverwitten = true;
	}
	if (i_RefreshGrid)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void O365Grid::OnRowSaved(GridBackendRow* p_Row, const bool i_RefreshGrid, const wstring& i_StatusText/* = _YTEXT("")*/)
{
	ASSERT(p_Row != nullptr);
	if (p_Row != nullptr)
	{
		p_Row->SetEditSaved(true);
		ASSERT(NO_ICON != m_IconSaved);
		GetBackend().OnRowChange(p_Row, i_StatusText.empty() ? GridBackend::g_RowStatusSaved : i_StatusText, m_IconSaved);
	}
	if (i_RefreshGrid)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void O365Grid::ensureOneErrorRowIsOnScreen()
{
	GridBackendRow* row = nullptr;

	for (auto& r : GetSelectedRows())
	{
		if (r->IsError())
		{
			row = r;
			break;
		}
	}

	if (nullptr == row)
	{
		for (auto& r : GetBackendRows())
		{
			if (r->IsError())
			{
				row = r;
				break;
			}
		}
	}

	if (nullptr != row)
		EnsureVisibleRow(GetBackend().GetVisibleIndex(row), false);
}

void O365Grid::ensureOneSavedRowIsOnScreen()
{
	GridBackendRow* row = nullptr;

	for (auto& r : GetSelectedRows())
	{
		if (r->IsChangesSaved())
		{
			row = r;
			break;
		}
	}

	if (nullptr == row)
	{
		for (auto& r : GetBackendRows())
		{
			if (r->IsChangesSaved())
			{
				row = r;
				break;
			}
		}
	}

	if (nullptr != row)
		EnsureVisibleRow(GetBackend().GetVisibleIndex(row), false);
}

void O365Grid::removeRowPreprocess(GridBackendRow* p_Row, RowRemovalReason p_RowRemovalReason)
{
	if (IsGridModificationsEnabled() && (RR_FILTERED == p_RowRemovalReason || RR_HIDDEN == p_RowRemovalReason))
	{
		vector<GridBackendField> pk;
		GetRowPK(p_Row, pk);
		GetModifications().RemoveRowModifications(pk);
	}
}

void O365Grid::saveView(GridView& p_View)
{
	GridViewManager::GetInstance().Write(p_View, GetSession());
}

void O365Grid::LoadViews(vector<GridView>& p_Views)
{
	GridViewManager::GetInstance().LoadViews(GetViewModuleName(), p_Views);
	UpdateViewKreator(p_Views);
}

void O365Grid::ManageViews()
{
	auto frame = GetParentGridFrameBase();
	if (nullptr != frame)
		frame->showBackstageTab(ID_BACKSTAGE_GRIDVIEWS);
}

wstring O365Grid::GetViewModuleName() const
{
	return nullptr != m_AutomationWizard ? m_AutomationWizard->GetName() : GetAutomationName();
}

// ============================================================================

O365Grid::SnapshotDataCustomizer::SnapshotDataCustomizer(O365Grid& p_Grid)
	: m_Grid(p_Grid)
{

}

bool O365Grid::SnapshotDataCustomizer::GetValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	ASSERT(!p_Column.IsAnnotation());
	return m_Grid.GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool O365Grid::SnapshotDataCustomizer::SetValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	ASSERT(!p_Column.IsAnnotation());
	return m_Grid.LoadSnapshotValue(p_Value, p_Row, p_Column);
}

bool O365Grid::SnapshotDataCustomizer::GetAdditionalValue(wstring& p_Value, GridBackendRow& p_Row)
{
	return m_Grid.GetSnapshotAdditionalValue(p_Value, p_Row);
}

bool O365Grid::SnapshotDataCustomizer::SetAdditionalValue(const wstring& p_Value, GridBackendRow& p_Row)
{
	return m_Grid.SetSnapshotAdditionalValue(p_Value, p_Row);

}

// ============================================================================

template<>
bool O365Grid::RowStatusHandler<Scope::O365>::IsChangesSaved(GridBackendRow* p_Row) const
{
	ASSERT(nullptr != p_Row);
	return nullptr != p_Row && p_Row->IsChangesSaved();
}

template<>
bool O365Grid::RowStatusHandler<Scope::O365>::IsError(GridBackendRow* p_Row) const
{
	ASSERT(nullptr != p_Row);
	return nullptr != p_Row && p_Row->IsError();
}

template<>
bool O365Grid::RowStatusHandler<Scope::O365>::IsModified(GridBackendRow* p_Row) const
{
	ASSERT(nullptr != p_Row);
	return nullptr != p_Row && p_Row->IsModified();
}

template<>
void O365Grid::RowStatusHandler<Scope::O365>::ClearStatus(GridBackendRow* p_Row) const
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
		p_Row->ClearStatus();
}

template<>
bool O365Grid::RowStatusHandler<Scope::O365>::IsDeleted(GridBackendRow* p_Row) const
{
	ASSERT(nullptr != p_Row);
	return nullptr != p_Row && p_Row->IsDeleted();
}

template<>
void O365Grid::RowStatusHandler<Scope::O365>::OnRowModified(GridBackendRow* p_Row)
{
	m_Grid.OnRowModified(p_Row);
}

template<>
void O365Grid::RowStatusHandler<Scope::O365>::OnRowNothingToSave(GridBackendRow* p_Row, const bool i_RefreshGrid, const wstring& i_StatusText /*= _YTEXT("")*/)
{
	m_Grid.OnRowNothingToSave(p_Row, i_RefreshGrid, i_StatusText);
}

template<>
void O365Grid::RowStatusHandler<Scope::O365>::OnRowSavedNotReceived(GridBackendRow* p_Row, bool i_RefreshGrid, const wstring& i_StatusText)
{
	m_Grid.OnRowSavedNotReceived(p_Row, i_RefreshGrid, i_StatusText);
}

template<>
void O365Grid::RowStatusHandler<Scope::O365>::OnRowSavedOverwritten(GridBackendRow* p_Row, bool i_RefreshGrid, const wstring& i_StatusText)
{
	m_Grid.OnRowSavedOverwritten(p_Row, i_RefreshGrid, i_StatusText);
}

template<>
void O365Grid::RowStatusHandler<Scope::O365>::OnRowSaved(GridBackendRow* p_Row, const bool i_RefreshGrid, const wstring& i_StatusText /*= _YTEXT("")*/)
{
	m_Grid.OnRowSaved(p_Row, i_RefreshGrid, i_StatusText);
}

template<>
void O365Grid::RowStatusHandler<Scope::O365>::OnRowError(GridBackendRow* p_Row, const bool i_RefreshGrid, const wstring& i_StatusText /*= _YTEXT("")*/)
{
	m_Grid.OnRowError(p_Row, i_RefreshGrid, i_StatusText);
}

// ============================================================================

template<>
bool O365Grid::RowStatusHandler<Scope::ONPREM>::IsChangesSaved(GridBackendRow* p_Row) const
{
	ASSERT(nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn);
	return nullptr != p_Row
		&& nullptr != m_Grid.m_SecondaryStatusColumn
		&& p_Row->GetField(m_Grid.m_SecondaryStatusColumn).GetIcon() == m_Grid.m_IconSaved;
}

template<>
bool O365Grid::RowStatusHandler<Scope::ONPREM>::IsError(GridBackendRow* p_Row) const
{
	ASSERT(nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn);
	return nullptr != p_Row
		&& nullptr != m_Grid.m_SecondaryStatusColumn
		&& p_Row->GetField(m_Grid.m_SecondaryStatusColumn).GetIcon() == m_Grid.m_IconError;
}

template<>
bool O365Grid::RowStatusHandler<Scope::ONPREM>::IsModified(GridBackendRow* p_Row) const
{
	ASSERT(nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn);
	return nullptr != p_Row
		&& nullptr != m_Grid.m_SecondaryStatusColumn
		&& p_Row->GetField(m_Grid.m_SecondaryStatusColumn).GetIcon() == GridBackendUtil::ICON_MODIFIED;
}

template<>
bool O365Grid::RowStatusHandler<Scope::ONPREM>::IsDeleted(GridBackendRow* p_Row) const
{
	// Doesn't exist so far...
	return false;
	/*ASSERT(nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn);
	return nullptr != p_Row
		&& nullptr != m_Grid.m_SecondaryStatusColumn
		&& p_Row->GetField(m_Grid.m_SecondaryStatusColumn).GetIcon() == GridBackendUtil::ICON_DELETED;*/
}

template<>
void O365Grid::RowStatusHandler<Scope::ONPREM>::ClearStatus(GridBackendRow* p_Row) const
{
	ASSERT(nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn);
	if (nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn)
	{
		p_Row->AddField(PooledString::g_EmptyString, m_Grid.m_SecondaryStatusColumn);
		ASSERT(!p_Row->IsExplosionFragment());
		if (p_Row->IsExplosionSource())
		{
			std::unordered_set<GridBackendRow*> descendants;
			m_Grid.GetMultivalueManager().GetDescendants(p_Row, descendants);
			for (auto& d : descendants)
				d->AddField(p_Row->GetField(m_Grid.m_SecondaryStatusColumn));
		}
	}
}

template<>
void O365Grid::RowStatusHandler<Scope::ONPREM>::OnRowModified(GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn);
	if (nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn)
	{
		p_Row->AddField(GridBackend::g_RowStatusModified, m_Grid.m_SecondaryStatusColumn, GridBackendUtil::ICON_MODIFIED);
		ASSERT(!p_Row->IsExplosionFragment());
		if (p_Row->IsExplosionSource())
		{
			std::unordered_set<GridBackendRow*> descendants;
			m_Grid.GetMultivalueManager().GetDescendants(p_Row, descendants);
			for (auto& d : descendants)
				d->AddField(p_Row->GetField(m_Grid.m_SecondaryStatusColumn));
		}
	}
}

template<>
void O365Grid::RowStatusHandler<Scope::ONPREM>::OnRowNothingToSave(GridBackendRow* p_Row, const bool i_RefreshGrid, const wstring& i_StatusText /*= _YTEXT("")*/)
{
	ASSERT(nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn);
	if (nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn)
	{
		p_Row->AddField(PooledString::g_EmptyString, m_Grid.m_SecondaryStatusColumn);
		ASSERT(!p_Row->IsExplosionFragment());
		if (p_Row->IsExplosionSource())
		{
			std::unordered_set<GridBackendRow*> descendants;
			m_Grid.GetMultivalueManager().GetDescendants(p_Row, descendants);
			for (auto& d : descendants)
				d->AddField(p_Row->GetField(m_Grid.m_SecondaryStatusColumn));
		}
	}
}

template<>
void O365Grid::RowStatusHandler<Scope::ONPREM>::OnRowSavedNotReceived(GridBackendRow* p_Row, bool i_RefreshGrid, const wstring& i_StatusText)
{
	ASSERT(nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn);
	if (nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn)
	{
		ASSERT(NO_ICON != m_Grid.m_IconSavedNotReceived);
		ASSERT(!i_StatusText.empty());
		p_Row->AddField(i_StatusText, m_Grid.m_SecondaryStatusColumn, m_Grid.m_IconSavedNotReceived);
		ASSERT(!p_Row->IsExplosionFragment());
		if (p_Row->IsExplosionSource())
		{
			std::unordered_set<GridBackendRow*> descendants;
			m_Grid.GetMultivalueManager().GetDescendants(p_Row, descendants);
			for (auto& d : descendants)
				d->AddField(p_Row->GetField(m_Grid.m_SecondaryStatusColumn));
		}
		m_Grid.m_HasSavedNotReceived = true;
		if (i_RefreshGrid)
			m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}
}

template<>
void O365Grid::RowStatusHandler<Scope::ONPREM>::OnRowSavedOverwritten(GridBackendRow* p_Row, bool i_RefreshGrid, const wstring& i_StatusText)
{
	ASSERT(nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn);
	if (nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn)
	{
		ASSERT(NO_ICON != m_Grid.m_IconSavedOverwitten);
		ASSERT(!i_StatusText.empty());
		p_Row->AddField(i_StatusText, m_Grid.m_SecondaryStatusColumn, m_Grid.m_IconSavedOverwitten);
		ASSERT(!p_Row->IsExplosionFragment());
		if (p_Row->IsExplosionSource())
		{
			std::unordered_set<GridBackendRow*> descendants;
			m_Grid.GetMultivalueManager().GetDescendants(p_Row, descendants);
			for (auto& d : descendants)
				d->AddField(p_Row->GetField(m_Grid.m_SecondaryStatusColumn));
		}
		m_Grid.m_HasSavedOverwitten = true;
		if (i_RefreshGrid)
			m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}
}

template<>
void O365Grid::RowStatusHandler<Scope::ONPREM>::OnRowSaved(GridBackendRow* p_Row, const bool i_RefreshGrid, const wstring& i_StatusText /*= _YTEXT("")*/)
{
	ASSERT(nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn);
	if (nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn)
	{
		ASSERT(NO_ICON != m_Grid.m_IconSaved);
		p_Row->AddField(i_StatusText.empty() ? GridBackend::g_RowStatusSaved : i_StatusText, m_Grid.m_SecondaryStatusColumn, m_Grid.m_IconSaved);
		ASSERT(!p_Row->IsExplosionFragment() || p_Row->IsExplosionSource());
		if (p_Row->IsExplosionSource())
		{
			std::unordered_set<GridBackendRow*> descendants;
			m_Grid.GetMultivalueManager().GetDescendants(p_Row, descendants);
			for (auto& d : descendants)
				d->AddField(p_Row->GetField(m_Grid.m_SecondaryStatusColumn));
		}
		if (i_RefreshGrid)
			m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}
}

template<>
void O365Grid::RowStatusHandler<Scope::ONPREM>::OnRowError(GridBackendRow* p_Row, const bool i_RefreshGrid, const wstring& i_StatusText /*= _YTEXT("")*/)
{
	ASSERT(nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn);
	if (nullptr != p_Row && nullptr != m_Grid.m_SecondaryStatusColumn)
	{
		ASSERT(NO_ICON != m_Grid.m_IconSaved);
		p_Row->AddField(i_StatusText.empty() ? GridBackend::g_RowStatusError : i_StatusText, m_Grid.m_SecondaryStatusColumn, m_Grid.m_IconError);
		ASSERT(!p_Row->IsExplosionFragment());
		if (p_Row->IsExplosionSource())
		{
			std::unordered_set<GridBackendRow*> descendants;
			m_Grid.GetMultivalueManager().GetDescendants(p_Row, descendants);
			for (auto& d : descendants)
				d->AddField(p_Row->GetField(m_Grid.m_SecondaryStatusColumn));
		}
		if (i_RefreshGrid)
			m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}
}
