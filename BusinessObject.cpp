#include "BusinessObject.h"
#include "MsGraphFieldNames.h"
#include "TimeUtil.h"
#include "TypeMetadataKeys.h"
#include "RoleDelegationManager.h"
#include "Sapio365Session.h"

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessObject>("Business Object") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Business Object"))))
		.constructor()(policy::ctor::as_object)
		.property(O365_ID, &BusinessObject::m_Id)(
			metadata(MetadataKeys::Hidden, true),
			metadata(MetadataKeys::ReadOnly, true))
		;
}

BusinessObject::BusinessObject()
    : m_UpdateStatus(UPDATE_TYPE_NONE)
	, m_Flags(0)
	, m_RBAC_DelegationID(NO_RBAC)
{
}

const boost::YOpt<YTimeDate>& BusinessObject::GetLastRequestTime() const
{
	return m_LastRequestTime;
}

void BusinessObject::SetLastRequestTime(const boost::YOpt<YTimeDate>& p_Time)
{
	m_LastRequestTime = p_Time;
}

const boost::YOpt<YTimeDate>& BusinessObject::GetDataDate(const uint32_t p_DataBlockId) const
{
	const auto it = m_DataBlockDates.find(p_DataBlockId);
	if (m_DataBlockDates.end() != it)
		return it->second;
	static boost::YOpt<YTimeDate> emptyDate;
	return emptyDate;
}

void BusinessObject::SetDataDate(const uint32_t p_DataBlockId, const boost::YOpt<YTimeDate>& p_Date)
{
	m_DataBlockDates[p_DataBlockId] = p_Date;
}

// FIXME: Finish this!
//const boost::YOpt<SapioError>& BusinessObject::GetDataError(const uint32_t p_DataBlockId) const
//{
//	const auto it = m_DataBlockErrors.find(p_DataBlockId);
//	if (m_DataBlockErrors.end() != it)
//		return it->second;
//	static boost::YOpt<SapioError> emptyError;
//	return emptyError;
//}
//
//void BusinessObject::SetDataError(const uint32_t p_DataBlockId, const boost::YOpt<SapioError>& p_Error)
//{
//	m_DataBlockErrors[p_DataBlockId] = p_Error;
//}

void BusinessObject::SetID(const PooledString& p_ID)
{
	m_Id = p_ID;
}

const PooledString&	BusinessObject::GetID() const
{
	return m_Id;
}

BusinessObject::UPDATE_TYPE BusinessObject::GetUpdateStatus() const
{
	return m_UpdateStatus;
}

void BusinessObject::SetUpdateStatus(UPDATE_TYPE p_UpdateStatus)
{
	m_UpdateStatus = p_UpdateStatus;
}

TaskWrapper<vector<HttpResultWithError>> BusinessObject::SendWriteSingleRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
    switch (GetUpdateStatus())
    {
    case BusinessObject::UPDATE_TYPE_MODIFY:
        return SendEditRequest(p_Sapio365Session, p_TaskData);
    case BusinessObject::UPDATE_TYPE_CREATE:
		return pplx::task_from_result(vector<HttpResultWithError>{ SendCreateRequest(p_Sapio365Session, p_TaskData).GetTask().get() });
    case BusinessObject::UPDATE_TYPE_DELETE:
		return pplx::task_from_result(vector<HttpResultWithError>{ SendDeleteRequest(p_Sapio365Session, p_TaskData).GetTask().get() });
	case BusinessObject::UPDATE_TYPE_HARDDELETE:
		return pplx::task_from_result(vector<HttpResultWithError>{ SendHardDeleteRequest(p_Sapio365Session, p_TaskData).GetTask().get() });
	case BusinessObject::UPDATE_TYPE_RESTORE:
		return pplx::task_from_result(vector<HttpResultWithError>{ SendRestoreRequest(p_Sapio365Session, p_TaskData).GetTask().get() });
	case BusinessObject::UPDATE_TYPE_LCPRENEW:
		return pplx::task_from_result(vector<HttpResultWithError>{ SendLCPRenewRequest(p_Sapio365Session, p_TaskData).GetTask().get() });
	case BusinessObject::UPDATE_TYPE_LCPADDGROUP:
		return pplx::task_from_result(vector<HttpResultWithError>{ SendLCPAddGroupRequest(p_Sapio365Session, p_TaskData).GetTask().get() });
	case BusinessObject::UPDATE_TYPE_LCPREMOVEGROUP:
		return pplx::task_from_result(vector<HttpResultWithError>{ SendLCPRemoveGroupRequest(p_Sapio365Session, p_TaskData).GetTask().get() });

    default:
        ASSERT(false);
        break;
    }
	return pplx::task_from_result(vector<HttpResultWithError>{ });
}

vector<TaskWrapper<HttpResultWithError>> BusinessObject::SendWriteMultipleRequests(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
    switch (GetUpdateStatus())
    {
    case BusinessObject::UPDATE_TYPE_ADDMEMBERS:
        return SendAddMembersRequest(p_Sapio365Session, p_TaskData);
    case BusinessObject::UPDATE_TYPE_DELETEMEMBERS:
        return SendDeleteMembersRequest(p_Sapio365Session, p_TaskData);
    case BusinessObject::UPDATE_TYPE_ADDOWNERS:
        return SendAddOwnersRequest(p_Sapio365Session, p_TaskData);
    case BusinessObject::UPDATE_TYPE_DELETEOWNERS:
        return SendDeleteOwnersRequest(p_Sapio365Session, p_TaskData);
    case BusinessObject::UPDATE_TYPE_ADDAUTHORACCEPTED:
        return SendAddAcceptedRequest(p_Sapio365Session, p_TaskData);
    case BusinessObject::UPDATE_TYPE_ADDAUTHORREJECTED:
        return SendAddRejectedRequest(p_Sapio365Session, p_TaskData);
	case BusinessObject::UPDATE_TYPE_DELETEACCEPTEDAUTHORS:
	case BusinessObject::UPDATE_TYPE_DELETEREJECTEDAUTHORS:
        return SendDeleteAuthorizationRequest(p_Sapio365Session, p_TaskData);
    default:
        ASSERT(false);
        break;
    }
    return {};
}

void BusinessObject::SetError(const boost::YOpt<SapioError>& p_Error)
{
    m_ErrorMessage = p_Error;
}

boost::YOpt<SapioError>& BusinessObject::GetError()
{
    return m_ErrorMessage;
}

const boost::YOpt<SapioError>& BusinessObject::GetError() const
{
    return m_ErrorMessage;
}

void BusinessObject::SetFlags(uint32_t p_Flags)
{
	m_Flags = p_Flags;
}

uint32_t BusinessObject::GetFlags() const
{
	return m_Flags;
}

bool BusinessObject::HasFlag(uint32_t p_Flag) const
{
	return p_Flag == (GetFlags() & p_Flag);
}

bool BusinessObject::IsMoreLoaded() const
{
	return HasFlag(MORE_LOADED);
}

pplx::task<vector<HttpResultWithError>> BusinessObject::SendEditRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
	return pplx::task_from_result(vector<HttpResultWithError>{ });
}

vector<TaskWrapper<HttpResultWithError>> BusinessObject::SendAddMembersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
    return vector<TaskWrapper<HttpResultWithError>>();
}

vector<TaskWrapper<HttpResultWithError>> BusinessObject::SendDeleteMembersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
    return vector<TaskWrapper<HttpResultWithError>>();
}

vector<TaskWrapper<HttpResultWithError>> BusinessObject::SendAddOwnersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
    return vector<TaskWrapper<HttpResultWithError>>();
}

vector<TaskWrapper<HttpResultWithError>> BusinessObject::SendDeleteOwnersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
    return vector<TaskWrapper<HttpResultWithError>>();
}

vector<TaskWrapper<HttpResultWithError>> BusinessObject::SendAddAcceptedRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
    return vector<TaskWrapper<HttpResultWithError>>();
}

vector<TaskWrapper<HttpResultWithError>> BusinessObject::SendAddRejectedRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
    return vector<TaskWrapper<HttpResultWithError>>();
}

vector<TaskWrapper<HttpResultWithError>> BusinessObject::SendDeleteAuthorizationRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
    return vector<TaskWrapper<HttpResultWithError>>();
}

vector<TaskWrapper<HttpResultWithError>> BusinessObject::SendGroupSettingsModify(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
    ASSERT(FALSE);
    return vector<TaskWrapper<HttpResultWithError>>();
}

TaskWrapper<HttpResultWithError> BusinessObject::SendCreateRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
    return pplx::task_from_result(HttpResultWithError());
}

TaskWrapper<HttpResultWithError> BusinessObject::SendDeleteRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
    return pplx::task_from_result(HttpResultWithError());
}

TaskWrapper<HttpResultWithError> BusinessObject::SendRestoreRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
	return pplx::task_from_result(HttpResultWithError());
}

TaskWrapper<HttpResultWithError> BusinessObject::SendHardDeleteRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
	return pplx::task_from_result(HttpResultWithError());
}

TaskWrapper<HttpResultWithError> BusinessObject::SendLCPRenewRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
	return pplx::task_from_result(HttpResultWithError());
}

TaskWrapper<HttpResultWithError> BusinessObject::SendLCPAddGroupRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
	return pplx::task_from_result(HttpResultWithError());
}

TaskWrapper<HttpResultWithError> BusinessObject::SendLCPRemoveGroupRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(FALSE);
	return pplx::task_from_result(HttpResultWithError());
}

void BusinessObject::SetRBACDelegationID(const int64_t p_RBAC_DelegationID)
{
	m_RBAC_DelegationID = p_RBAC_DelegationID;
}

int64_t BusinessObject::GetRBACDelegationID() const
{
	return m_RBAC_DelegationID;
}

bool BusinessObject::IsRBACAuthorized() const
{
	return m_RBAC_DelegationID != OUT_OF_SCOPE;
}

wstring BusinessObject::ObjectGuidToImmutableID(const wstring& p_ObjectGuid)
{
	wstring immutableID;

	wstring objGuid;
	objGuid.reserve(p_ObjectGuid.size() + 2);
	objGuid.push_back(L'{');
	objGuid += p_ObjectGuid;
	objGuid.push_back(L'}');

	GUID guid;
	HRESULT hr = IIDFromString(objGuid.c_str(), &guid);
	ASSERT(hr == S_OK);
	if (hr == S_OK)
		immutableID = utility::conversions::to_base64({ reinterpret_cast<unsigned char*>(&guid.Data1), reinterpret_cast<unsigned char*>(&guid.Data1 + sizeof(guid.Data1)) });

	return immutableID;
}

bool BusinessObject::HasPrivilege(const RoleDelegationUtil::RBAC_Privilege p_Privilege, std::shared_ptr<Sapio365Session> p_Session) const
{
	return RoleDelegationManager::GetInstance().HasPrivilege(*this, p_Privilege, p_Session);
}

wstring BusinessObject::GetValue(const wstring& p_PropName) const
{
	wstring retVal;

	const auto propName = MFCUtil::convertUNICODE_to_ASCII(p_PropName);
	auto propbu = rttr::type::get(*this).get_property(propName.c_str());
	if (propbu.is_valid())
	{
		if (propbu.get_type() == rttr::type::get<boost::YOpt<PooledString>>())
		{
			const auto var = propbu.get_value(*this);
			if (var.is_valid())
			{
				const auto& val = var.get_value<boost::YOpt<PooledString>>();
				if (val && !val->IsEmpty())
					retVal = val->c_str();
			}
		}
		else if (propbu.get_type() == rttr::type::get<PooledString>())
		{
			const auto var = propbu.get_value(*this);
			if (var.is_valid())
			{
				const auto& val = var.get_value<PooledString>();
				if (!val.IsEmpty())
					retVal = val.c_str();
			}
		}
		else
		{
			ASSERT(false);
		}
	}
	return retVal;
}

vector<PooledString> BusinessObject::GetProperties(const wstring& p_PropName) const
{
	// Implement in your sub class if needed
	ASSERT(false);
	return { };
}

wstring BusinessObject::GetObjectName() const
{
	auto type = get_type();
	auto data = type.get_metadata(MetadataKeys::TypeDisplayName);
	if (data)
		return data.get_value<wstring>();
	ASSERT(false);
	return Str::g_EmptyString;
}
