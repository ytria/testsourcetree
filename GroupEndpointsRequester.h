#pragma once

#include "GroupEndpoint.h"
#include "IPageRequestLogger.h"
#include "IRequester.h"
#include "ValueListDeserializer.h"

class GroupEndpointDeserializer;
class GroupEndpointsRequester : public IRequester
{
public:
	GroupEndpointsRequester(PooledString p_GroupId, const std::shared_ptr<IPageRequestLogger>& p_Logger);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const std::vector<GroupEndpoint>& GetData() const;

private:
	std::shared_ptr<ValueListDeserializer<GroupEndpoint, GroupEndpointDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;

	PooledString m_GroupId;
};

