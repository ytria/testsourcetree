#pragma once

#include "IRequester.h"
#include "InvokeResultWrapper.h"

class IPSObjectCollectionDeserializer;
class IRequestLogger;

class RecipientPermissionRequester : public IRequester
{
public:
	RecipientPermissionRequester(const std::shared_ptr<IPSObjectCollectionDeserializer>& p_Deserializer, const wstring& p_UserPrincipalName, const std::shared_ptr<IRequestLogger>& p_RequestLogger);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const std::shared_ptr<InvokeResultWrapper>& GetResult() const;

private:
	std::shared_ptr<IPSObjectCollectionDeserializer> m_Deserializer;
	wstring m_UserPrincipalName;
	std::shared_ptr<IRequestLogger> m_Logger;
	std::shared_ptr<InvokeResultWrapper> m_Result;
};

