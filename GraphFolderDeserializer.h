#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "GraphFolder.h"

class GraphFolderDeserializer : public JsonObjectDeserializer, public Encapsulate<GraphFolder>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

