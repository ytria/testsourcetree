#include "O365HtmlMainView.h"

#include "ChilkatUtils.h"
#include "MainFrame.h"
#include "Office365Admin.h"
#include "YBrowserLinkHandler.h"
#include <cpprest/uri.h>

// Remove this soon: Just for testing
namespace
{
	class TestExternal : public CCmdTarget
	{
	public:
		TestExternal()
		{
			EnableAutomation();
		}

		BSTR ToUpper(LPCWSTR str)
		{
			return _bstr_t(const_cast<wchar_t*>(Str::toUpper(str).c_str()));
		}

	private:
		DECLARE_DISPATCH_MAP()
	};

	BEGIN_DISPATCH_MAP(TestExternal, CCmdTarget)
		DISP_FUNCTION(TestExternal, "toUpper", ToUpper, VT_BSTR, VTS_WBSTR)
	END_DISPATCH_MAP()
}

IMPLEMENT_DYNCREATE(O365HtmlMainView, YAdvancedHtmlView)

BEGIN_MESSAGE_MAP(O365HtmlMainView, YAdvancedHtmlView)
END_MESSAGE_MAP()

const wstring O365HtmlMainView::g_AJLtag						= _YTEXT("++A++J++L++");
const wstring O365HtmlMainView::g_PostedDataButtonFeedback		= _YTEXT("SendFeedback");
const wstring O365HtmlMainView::g_PostedDataButtonAJL			= _YTEXT("AJL_POST");
const wstring O365HtmlMainView::g_PostedDataAJLlist				= _YTEXT("AJL_LIST");

O365HtmlMainView::O365HtmlMainView() :
	m_HasNoJob(true),
	m_IsReadyForExecuteScript(false),
	m_IsSandbox(CRMpipe().IsFeatureSandboxed())
{
	SetPostedDataTarget(this);

    registerLinkHandlers();

// #ifdef _DEBUG
// 	// TESTING stuff. Modify before releasing :-)
// 	{
// 		SetDebugDisplayPostedData(true);
// 
// 		// External
// 		SetExternal(std::make_shared<TestExternal>());
// 	}
// #endif

	// HTML injection B=8
	registerScriptsAndLinksToReplace();
}

void O365HtmlMainView::SetReadyForExecuteScript()
{
	m_IsReadyForExecuteScript = true;
}

void O365HtmlMainView::RefreshHTMLBulldozer()
{
	registerScriptsAndLinksToReplace();
	// Refresh(); crash!
	YLoadFromResource(MainFrame::g_homePageResourceName.c_str());
}

void O365HtmlMainView::Navigate(const wstring& url)
{
	if (CString(url.c_str()).Find(_YTEXT("res://")) == 0)
		registerScriptsAndLinksToReplace();
	else
		clearScriptsAndLinksToReplace();
	
	YAdvancedHtmlView::Navigate(url);
}

void O365HtmlMainView::EnableUserMode()
{
	if (m_IsReadyForExecuteScript)
		ExecuteScript(L"externalFunction('enableMyDataMenu');");
}

void O365HtmlMainView::EnableAdminMode()
{
	if (m_IsReadyForExecuteScript)
		ExecuteScript(L"externalFunction('disableMyDataMenu');");
}

void O365HtmlMainView::EnableOnPremise()
{
	if (m_IsReadyForExecuteScript)
		ExecuteScript(L"externalFunction('specialShow', 'isOnPremise');");
}

void O365HtmlMainView::OnBeforeNavigate2(LPCTSTR lpszURL, DWORD nFlags, LPCTSTR lpszTargetFrameName, CByteArray& baPostedData, LPCTSTR lpszHeaders, BOOL* pbCancel)
{
	YAdvancedHtmlView::OnBeforeNavigate2(lpszURL, nFlags, lpszTargetFrameName, baPostedData, lpszHeaders, pbCancel);

	// Only if URL doesn't involve MFC resources (Below web::uri can't handle them)
	wstring linkURL = lpszURL;
	if (linkURL.find(_YTEXT("res://")) == wstring::npos)
	{
		const auto ajlTagPos = linkURL.find(g_AJLtag);
		if (wstring::npos != ajlTagPos)
			linkURL = linkURL.substr(0, ajlTagPos);
		web::uri uri(linkURL.c_str());

		auto handlerNodeItt = m_LinkHandlers.find(uri.scheme());
		if (handlerNodeItt != std::end(m_LinkHandlers))
		{
			MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
			ASSERT(nullptr != mainFrame);
			if (nullptr != mainFrame)
			{
				auto lc = mainFrame->GetLicenseContext();
				lc.SetIsAJLauthorized(wstring::npos != ajlTagPos);

				YBrowserLink link(lc, uri);
				handlerNodeItt->second->Handle(link);
				*pbCancel = TRUE;
			}
		}
	}
}

bool O365HtmlMainView::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	//const wstring& action = data.begin()->second;
	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != mainFrame && nullptr != app)
	{
		wstring AJLjobIDds;
		bool updateAJL = false;
		for (const auto& postedData : data)
		{
			const wstring& action	= postedData.first;
			const wstring& value	= postedData.second;
			if (action == g_PostedDataButtonFeedback)
				app->SuggestNewJob(mainFrame);
			else if (action == g_PostedDataAJLlist)
				AJLjobIDds = value;
			else if (action == g_PostedDataButtonAJL)
				updateAJL = true;
		}

		if (updateAJL && app->UpdateJobSelectionIDs(Str::explodeIntoSet(AJLjobIDds, JobCenterUtil::g_CatalogAJLSeparator, false, true), this))
			UpdateAJLInfo();
	}

	return false;
}

void O365HtmlMainView::registerLinkHandlers()
{
    m_LinkHandlers[_YTEXT("ytria")] = std::make_unique<YBrowserLinkHandler>();
}

void O365HtmlMainView::registerScriptsAndLinksToReplace()
{
//	until v 1.8.0.1819 (10 Nov 2020):
// 	const bool success = YAdvancedHtmlView::ReadJsonFileFromResourcesIntoVarString(ID_HOME_DEFAULT_JSON, jsonData);
// 	ASSERT(success);

	wstring jsonData;
	generateJSONScriptData(jsonData);

	// Job catalog
	wstring dataJS;
	generateJSONscriptDataJS(dataJS);

	SetScriptsToReplace(
		{
			{ L"JSON",		{ L"", L"", jsonData.c_str() } },
			{ L"DATA-JS",	{ L"", L"", dataJS.c_str() } },
			{ L"HBS-JS"	,	{ L"text/javascript", L"hbs-welcome.js", L"" } }, // This file is in resources
			{ L"OTHER-JS",	{ L"text/javascript", L"welcome.js", L"" } }, // This file is in resources
		}
	);

	// LINKS
	SetLinksToReplace({ { L"OTHER-CSS",{ L"stylesheet", L"welcome.css" } } }); // This file is in resources
}

void O365HtmlMainView::clearScriptsAndLinksToReplace()
{
	SetScriptsToReplace({});
	SetLinksToReplace({});
}

void O365HtmlMainView::generateJSONScriptData(wstring& p_Output)
{
	json::value main	= json::value::object();
	json::value items	= json::value::array();

	size_t itemIndex = 0;

	items[itemIndex] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("hbs"), JSON_STRING(_YTEXT("firstscreen"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("id"), JSON_STRING(_YTEXT("HomePage"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("method"), JSON_STRING(_YTEXT("POST"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("action"), JSON_STRING(_YTEXT("#"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("tiles"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("leftSection"),
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("title"), JSON_STRING(_T("Welcome!"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("intro"), JSON_STRING(_T("See and filter jobs using an easy to use Dashboard or a full catalog view."))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("action"), JSON_STRING(_T("Click To See Jobs"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("menuLabel"), JSON_STRING(_T("My Personal Data"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("menuActions"),
						JSON_BEGIN_ARRAY
						JSON_END_ARRAY
					JSON_END_PAIR
				JSON_END_OBJECT
			JSON_END_PAIR
		JSON_END_OBJECT;

	size_t k = 0, x = 0, m = 0;

	// TILES
	{
		auto& tiles = items[itemIndex].as_object()[_YTEXT("tiles")].as_array();

		// first row of cells
		{
			tiles[k] =
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("cells"),
						JSON_BEGIN_ARRAY
						JSON_END_ARRAY
					JSON_END_PAIR
				JSON_END_OBJECT;

			auto& cells = tiles[k].as_object()[_YTEXT("cells")].as_array();
			cells[x] = 
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("title"), JSON_STRING(_T("Users"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("id"), JSON_STRING(_T("users"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("icon"), JSON_STRING(_YTEXT("fal fa-user"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("color"), JSON_STRING(_YTEXT("#0CB06C"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("intro"), JSON_STRING(_T("Show the complete list of users"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("action"), JSON_STRING(_T("ytria://users_module"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("buttons"),
						JSON_BEGIN_ARRAY
						JSON_END_ARRAY
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpIntro"), JSON_STRING(_T("Once the user list is shown, drill down into user information by choosing users from the list and then selecting the appropriate command."))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpTitle"), JSON_STRING(_T("The following types of user data are available:"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpItemList"),
						JSON_BEGIN_ARRAY
						JSON_END_ARRAY
					JSON_END_PAIR
				JSON_END_OBJECT;

			{
				auto& buttons = cells[x].as_object()[_YTEXT("buttons")].as_array();
				buttons[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("buttonTitle"), JSON_STRING(_T("Show pre-filtered list"))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://prefiltered_users_module"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				buttons[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("buttonTitle"), JSON_STRING(_T("Show deleted users"))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://users_recyclebin_module"))
						JSON_END_PAIR
					JSON_END_OBJECT;

				m = 0;
				auto& helpItems = cells[x].as_object()[_YTEXT("helpItemList")].as_array();
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Group Membership"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Licenses"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("OneDrive Files"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Messages"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Events"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Contacts"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Recycle Bin"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Settings"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Message Rules"))
						JSON_END_PAIR
					JSON_END_OBJECT;
			}

			m = 0;
			x++;
			cells[x] = 
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("title"), JSON_STRING(_T("Groups & Teams"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("id"), JSON_STRING(_T("groups"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("icon"), JSON_STRING(_YTEXT("fal fa-users"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("color"), JSON_STRING(_YTEXT("#0099EF"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("intro"), JSON_STRING(_T("Show the complete list of groups & Teams"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("action"), JSON_STRING(_YTEXT("ytria://groups_module"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("buttons"),
						JSON_BEGIN_ARRAY
						JSON_END_ARRAY
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpIntro"), JSON_STRING(_T("Once the group list is shown, drill down into group information by choosing groups from the list and then selecting the appropriate command."))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpTitle"), JSON_STRING(_T("The following types of group data are available:"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpItemList"),
						JSON_BEGIN_ARRAY
						JSON_END_ARRAY
					JSON_END_PAIR
				JSON_END_OBJECT;

			{
				auto& buttons = cells[x].as_object()[_YTEXT("buttons")].as_array();
				buttons[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("buttonTitle"), JSON_STRING(_T("Show pre-filtered list"))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://prefiltered_groups_module"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				buttons[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("buttonTitle"), JSON_STRING(_T("Show deleted groups"))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://groups_recyclebin_module"))
						JSON_END_PAIR
				JSON_END_OBJECT;

				m = 0;
				auto& helpItems = cells[x].as_object()[_YTEXT("helpItemList")].as_array();
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Members"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Owners"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Delivery Management"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("File Library"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Events"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Conversations"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Recycle Bin"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Channels"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Settings"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Sites"))
						JSON_END_PAIR
					JSON_END_OBJECT;
			}
		}

		// second row of cells
		x = 0;
		m = 0;
		k++;
		{
			tiles[k] =
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("cells"),
					JSON_BEGIN_ARRAY
					JSON_END_ARRAY
				JSON_END_PAIR
			JSON_END_OBJECT;

			auto& cells = tiles[k].as_object()[_YTEXT("cells")].as_array();
			cells[x] = 
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("title"), JSON_STRING(_T("SharePoint Sites"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("id"), JSON_STRING(_T("sites"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("icon"), JSON_STRING(_YTEXT("fal fa-share-alt"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("color"), JSON_STRING(_YTEXT("#F45800"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("intro"), JSON_STRING(_T("Show the complete list of tenant sites"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("action"), JSON_STRING(_YTEXT("ytria://sites_module"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("buttons"),
						JSON_BEGIN_ARRAY
						JSON_END_ARRAY
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpIntro"), JSON_STRING(_T("Once the sites are shown, drill down into site information by choosing sites from the list and then selecting the appropriate command."))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpTitle"), JSON_STRING(_T("The following types of site data are available:"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpItemList"),
						JSON_BEGIN_ARRAY
						JSON_END_ARRAY
					JSON_END_PAIR
				JSON_END_OBJECT;

			{
				// no buttons for sharepoint?
				
				m = 0;
				auto& helpItems = cells[x].as_object()[_YTEXT("helpItemList")].as_array();
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Settings"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT						
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("File Library"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT						
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("Site Lists"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("List Items"))
						JSON_END_PAIR,
					JSON_END_OBJECT;
				helpItems[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("helpItem"), JSON_STRING(_T("List Columns"))
						JSON_END_PAIR
					JSON_END_OBJECT;
			}

			m = 0;
			x++;
			cells[x] = 
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("title"), JSON_STRING(_T("Tenant"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("id"), JSON_STRING(_T("tenant"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("icon"), JSON_STRING(_YTEXT("fal fa-building"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("color"), JSON_STRING(_YTEXT("#FFCE21"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("intro"), JSON_STRING(_T("Tenant level information"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("action"), JSON_STRING(_YTEXT(""))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("buttons"),
						JSON_BEGIN_ARRAY
						JSON_END_ARRAY
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpIntro"), JSON_STRING(_T("Access information relating to the tenant as a whole."))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpTitle"), JSON_STRING(_T("Manage Office 365 administrative roles by adding and removing users.<br><br>See a summary of your purchased licenses and their services plans.<br><br>Generate usage, sign-ins and audit log reports for a specified period."))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("helpItemList"),
						JSON_BEGIN_ARRAY
						JSON_END_ARRAY
					JSON_END_PAIR
				JSON_END_OBJECT;

			{
				auto& buttons = cells[x].as_object()[_YTEXT("buttons")].as_array();
				buttons[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("buttonTitle"), JSON_STRING(_T("Manage roles & administrators"))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://directory_roles_module"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				buttons[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("buttonTitle"), JSON_STRING(_T("Licenses & services"))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://licenses_module"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				buttons[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("buttonTitle"), JSON_STRING(_T("Usage reports"))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://reports_module"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				buttons[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("buttonTitle"), JSON_STRING(_T("Sign-ins report"))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://signins_module"))
						JSON_END_PAIR
					JSON_END_OBJECT;
				buttons[m++] =
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("buttonTitle"), JSON_STRING(_T("Audit logs report"))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://auditlogs_module"))
						JSON_END_PAIR
					JSON_END_OBJECT;
			}
		}
	}

	// LEFT SECTION - MENU ACTION - BUTTONS
	{
		m = 0;
		auto& buttons = items[itemIndex].as_object()[_YTEXT("leftSection")].as_object()[_YTEXT("menuActions")].as_array();
			buttons[m++] =
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("buttonTitle"), JSON_STRING(_T("Messages"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://curusermessages_module"))
					JSON_END_PAIR
				JSON_END_OBJECT;
			buttons[m++] =
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("buttonTitle"), JSON_STRING(_T("Events"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://curuserevents_module"))
					JSON_END_PAIR
				JSON_END_OBJECT;
			buttons[m++] =
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("buttonTitle"), JSON_STRING(_T("OneDrive Files"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://curuserdrives_module"))
					JSON_END_PAIR
				JSON_END_OBJECT;
			buttons[m++] =
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("buttonTitle"), JSON_STRING(_T("Contacts"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://curusercontacts_module"))
					JSON_END_PAIR
				JSON_END_OBJECT;
			buttons[m++] =
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("buttonTitle"), JSON_STRING(_T("Message Rules"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://curusermessagerules_module"))
					JSON_END_PAIR
				JSON_END_OBJECT;
			buttons[m++] =
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("buttonTitle"), JSON_STRING(_T("Group Memberships"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://curusergroupmemberships_module"))
					JSON_END_PAIR
				JSON_END_OBJECT;
			buttons[m++] =
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("buttonTitle"), JSON_STRING(_T("Licenses"))
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("buttonUrl"), JSON_STRING(_YTEXT("ytria://curuserlicenses_module"))
					JSON_END_PAIR
				JSON_END_OBJECT;
	}


	items[++itemIndex] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("hbs"), JSON_STRING(_YTEXT("catalog"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("id"), JSON_STRING(_YTEXT("catalog"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("method"), JSON_STRING(_YTEXT("POST"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("action"), JSON_STRING(_YTEXT("#"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("AJL_labelAdd"), JSON_STRING(_T("Add this Job to your selection of Jobs"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("AJL_labelRemowe"), JSON_STRING(_T("Remove this Job from your selection"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("AJL_currentState"), JSON_STRING(_T(" -- %C% on %T% %M%"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("AJL_newState"), JSON_STRING(_T(" (Added %N%)")/*" (+%N%/-%D%)")*/)
			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR --- done dynamically according to license feature cf UpdateCanRemoveJobs()
// 				_YTEXT("AJL_canRemoveJob"), JSON_STRING(_YTEXT("X")) 
// 			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("title_currentCount"), JSON_STRING(_YTEXT(" ( %C% )"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("tileAction"), JSON_STRING(_YTEXT("ytria://jobexec/[url]"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("colors"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("menuItems"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR,		
			JSON_BEGIN_PAIR
				_YTEXT("menuSheduledItems"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR,		
			JSON_BEGIN_PAIR
				_YTEXT("AJL"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR,		
			JSON_BEGIN_PAIR
				_YTEXT("actions"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR,		
			JSON_BEGIN_PAIR
				_YTEXT("topHeader"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("leftSide"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR,			
			JSON_BEGIN_PAIR
				_YTEXT("rightSide"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR
		JSON_END_OBJECT;

	k = 0;
	auto& colors = items[itemIndex].as_object()[_YTEXT("colors")].as_array();
	colors[k++] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("family"), JSON_STRING(_T("Groups"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("color"), JSON_STRING(_YTEXT("0,153,239"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("hex"), JSON_STRING(_YTEXT("#0099EF"))
			JSON_END_PAIR
		JSON_END_OBJECT;
	colors[k++] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("family"), JSON_STRING(_T("Sites"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("color"), JSON_STRING(_YTEXT("244,88,0"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("hex"), JSON_STRING(_YTEXT("#F45800"))
			JSON_END_PAIR
		JSON_END_OBJECT;
	colors[k++] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("family"), JSON_STRING(_T("Users"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("color"), JSON_STRING(_YTEXT("12,176,108"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("hex"), JSON_STRING(_YTEXT("#0CB06C"))
			JSON_END_PAIR
		JSON_END_OBJECT;
	colors[k++] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("family"), JSON_STRING(_T("Tenant"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("color"), JSON_STRING(_YTEXT("255,206,33"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("hex"), JSON_STRING(_YTEXT("#FFCE21"))
			JSON_END_PAIR
		JSON_END_OBJECT;

	k = 0;
	auto& menuItems = items[itemIndex].as_object()[_YTEXT("menuItems")].as_array();
	menuItems[k++] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("label"), JSON_STRING(_T("Execute this job"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("url"), JSON_STRING(_YTEXT("ytria://jobexec/[url]"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("tgt"), JSON_STRING(_YTEXT(""))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("val"), JSON_STRING(_YTEXT(""))
			JSON_END_PAIR
		JSON_END_OBJECT;
// 	menuItems[k++] =
// 		JSON_BEGIN_OBJECT
// 			JSON_BEGIN_PAIR
// 				_YTEXT("label"), JSON_STRING(_T("Create Custom from Job..."))
// 			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("url"), JSON_STRING(_YTEXT("ytria://create/[id]"))
// 			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("tgt"), JSON_STRING(_YTEXT(""))
// 			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("val"), JSON_STRING(_YTEXT(""))
// 			JSON_END_PAIR
// 		JSON_END_OBJECT;
	menuItems[k++] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("label"), JSON_STRING(_T("Schedule this Job"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("url"), JSON_STRING(_YTEXT("ytria://schedule/[id]"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("tgt"), JSON_STRING(_YTEXT(""))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("val"), JSON_STRING(_YTEXT(""))
			JSON_END_PAIR
		JSON_END_OBJECT;
	menuItems[k++] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("label"), JSON_STRING(_T("Delete this Job"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("url"), JSON_STRING(_YTEXT("ytria://delete/[id]"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("tgt"), JSON_STRING(_YTEXT("source"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("val"), JSON_STRING(_YTEXT("custom"))
			JSON_END_PAIR
		JSON_END_OBJECT;

	k = 0;
	auto& menuSheduledItems = items[itemIndex].as_object()[_YTEXT("menuSheduledItems")].as_array();
	menuSheduledItems[k++] = 
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("label"), JSON_STRING(_T("Execute Now"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("url"), JSON_STRING(_YTEXT("ytria://jobexec/[url]"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("tgt"), JSON_STRING(_YTEXT(""))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("val"), JSON_STRING(_YTEXT(""))
			JSON_END_PAIR
		JSON_END_OBJECT;
// 	menuSheduledItems[k++] = 
// 		JSON_BEGIN_OBJECT
// 			JSON_BEGIN_PAIR
// 				_YTEXT("label"), JSON_STRING(_T("Edit..."))
// 			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("url"), JSON_STRING(_YTEXT("ytria://editSched/[id]"))
// 			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("tgt"), JSON_STRING(_YTEXT(""))
// 			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("val"), JSON_STRING(_YTEXT(""))
// 			JSON_END_PAIR
// 		JSON_END_OBJECT;
	menuSheduledItems[k++] = 
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("label"), JSON_STRING(_T("Delete..."))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("url"), JSON_STRING(_YTEXT("ytria://delSched/[id]"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("tgt"), JSON_STRING(_YTEXT(""))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("val"), JSON_STRING(_YTEXT(""))
			JSON_END_PAIR
		JSON_END_OBJECT;

	k = 0;
	auto& AJL = items[itemIndex].as_object()[_YTEXT("AJL")].as_array();
	AJL[k++] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("labelAdd"), JSON_STRING(_T("Add Job"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("labelRemowe"), JSON_STRING(_T("Remove Job"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("labelSubmit"), JSON_STRING(_T("Apply Changes"))
			JSON_END_PAIR
		JSON_END_OBJECT;

// 	k = 0;
// 	auto& actions = items[itemIndex].as_object()[_YTEXT("actions")].as_array();
// 	actions[k++] =
// 		JSON_BEGIN_OBJECT
// 			JSON_BEGIN_PAIR
// 				_YTEXT("key"), JSON_STRING(_YTEXT("setfav"))
// 			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("label"), JSON_STRING(_T("Set as Favorite..."))
// 			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("url"), JSON_STRING(_YTEXT("ytria://setfav/[id]"))
// 			JSON_END_PAIR
// 		JSON_END_OBJECT;
// 	actions[k++] =
// 		JSON_BEGIN_OBJECT
// 			JSON_BEGIN_PAIR
// 				_YTEXT("key"), JSON_STRING(_YTEXT("unsetfav"))
// 			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("label"), JSON_STRING(_T("Unset as Favorite..."))
// 			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("url"), JSON_STRING(_YTEXT("ytria://unsetfav/[id]"))
// 			JSON_END_PAIR
// 		JSON_END_OBJECT;
	
	k = 0;
	auto& topHeader = items[itemIndex].as_object()[_YTEXT("topHeader")].as_array();
	topHeader[k++] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("AJL_labelSubmit"), JSON_STRING(_YTEXT("Apply your Changes"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("AJL_buttonValue"), JSON_STRING(g_PostedDataButtonAJL)
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("AJL_fieldName"), JSON_STRING(g_PostedDataAJLlist)
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("back-home-icon"), JSON_STRING(_YTEXT("fal fa-long-arrow-left"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("intro-catalog-icon"), JSON_STRING(_YTEXT("fal fa-book"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("intro-see-catalog"), JSON_STRING(_T("Browse full catalog"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("intro-dashboard-icon"), JSON_STRING(_YTEXT("fal fa-table"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("intro-see-dashboard"), JSON_STRING(_T("Browse dashboard"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("title-catalog"), JSON_STRING(_T("Full catalog"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("title-dashboard"), JSON_STRING(_T("Dashboard"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("search-placeholder"), JSON_STRING(_T("What do you want to do?"))
			JSON_END_PAIR
		JSON_END_OBJECT;

	k = 0;
	auto& leftSide = items[itemIndex].as_object()[_YTEXT("leftSide")].as_array();
	leftSide[k] = 
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("value-feedback"), JSON_STRING(g_PostedDataButtonFeedback)
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("label-feedback"), JSON_STRING(_T("Feedback"))
			JSON_END_PAIR
		JSON_END_OBJECT;

	k = 0;
	auto& rightSide = items[itemIndex].as_object()[_YTEXT("rightSide")].as_array();
	rightSide[k] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("intro-filters"), JSON_STRING(_T("Filters:"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("intro-scope"), JSON_STRING(_T("Scope:"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("intro-job"), JSON_STRING(_T("Job Type:"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("left-first-title"), JSON_STRING(_T("Have you tried our latest creations? %I%"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("left-first-data"), JSON_STRING(_YTEXT("newjobs"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("right-first-title"), JSON_STRING(_T("Scheduled Jobs %I%"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("right-first-data"), JSON_STRING(_YTEXT("scheduled"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("left-second-title"), JSON_STRING(_T("Selected AJL Jobs %I%"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("left-second-data"), JSON_STRING(_YTEXT("ajl"))
			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("__left-second-title"), JSON_STRING(_T("My last jobs"))
// 			JSON_END_PAIR,
// 			JSON_BEGIN_PAIR
// 				_YTEXT("__left-second-data"), JSON_STRING(_YTEXT("lastused"))
// 			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("right-second-title"), JSON_STRING(_T("My custom Jobs %I%"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("right-second-data"), JSON_STRING(_YTEXT("custom"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("isDebug"), JSON_STRING(m_IsSandbox ? _YTEXT("X") : _YTEXT(""))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("left-third-data"), JSON_STRING(m_IsSandbox ? _YTEXT("favorite") : _YTEXT(""))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("left-third-title"), JSON_STRING(m_IsSandbox ? _YTEXT("SANDBOX - Empty") : _YTEXT(""))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("right-third-data"), JSON_STRING(m_IsSandbox ? _YTEXT("muhBox") : _YTEXT(""))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("right-third-title"), JSON_STRING(m_IsSandbox ? _YTEXT("SANDBOX - third box on the right") : _YTEXT(""))
			JSON_END_PAIR
		JSON_END_OBJECT;


	const json::value root = json::value::object({	{ _YTEXT("main"), main },
													{ _YTEXT("items"), items }});
	auto rootSerialized = root.serialize();


	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(rootSerialized);

	if (CRMpipe().IsFeatureSandboxed())
	{
		const auto path = FileUtil::GetTempFolder() + _YTEXT("\\debugJsonMain.json");
		std::ofstream ostr(path, std::ios::out | std::ios::trunc);
		ostr << Str::convertToUTF8(ChilkatUtils::BeautifyJson(rootSerialized));
	}
}

vector<Script> O365HtmlMainView::getAllJobs()
{
	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != mainFrame && nullptr != app)
	{
		auto jobCenter = mainFrame->GetAutomationWizard();
		ASSERT(nullptr != jobCenter);
		if (nullptr != jobCenter)
		{
			auto jobs = jobCenter->GetScripts();

			// detect AJL IDs duplicates
			if (m_IsSandbox)
			{
				wstring ajlDebug;
				if (Registry::readString(HKEY_CURRENT_USER, _YTEXT("Software\\Ytria\\sapio365"), _YTEXT("AJLdebug"), ajlDebug) && !ajlDebug.empty())
				{
					wstring badScripts;
					map<wstring,vector<wstring>> ajlIDs;
					for (const auto& j : jobs)
						ajlIDs[j.m_LibraryID].push_back(j.m_Key);
					for(const auto& p : ajlIDs)
						if (p.second.size() > 1)
							badScripts += _YTEXTFORMAT(L"Jobs with duplicate AJL library ID %s:\t\n%s", p.first.c_str(), Str::implode(p.second, _YTEXT("\t\n")).c_str());
					if (!badScripts.empty())
						YCodeJockMessageBox(this, DlgMessageBox::eIcon_Error, _YTEXT("AJL error"), badScripts, _YTEXT(""), { {IDOK, YtriaTranslate::Do(BackstagePanelAbout_OnCheckForUpdate_3, _YLOC("OK")).c_str()} }).DoModal();
				}
			}

			jobs.erase(std::remove_if(jobs.begin(), jobs.end(), [](const auto& job)
				{
					ASSERT(job.HasID());
					return !job.IsJobCatalog();
				}), jobs.end());
			m_HasNoJob = jobs.empty();
			return jobs;
		}
	}

	return {};
}

void O365HtmlMainView::generateJSONscriptDataJS(wstring& p_Output)
{
// 	p_Output =
// 		_YTEXT("	var allJobs = [];\
// 					var allCategories = [];\
// 					var allScheduled = [];\
// 					var allSelectedAJL = [];\
// 					var maxSelectedAJL = 0;");

	json::value allJobs			= json::value::array();
	json::value allCategories	= json::value::array();
	json::value allScheduled	= json::value::array();
	json::value allSelectedAJL	= json::value::array();

	map<std::pair<wstring, wstring>, map<wstring, wstring>> categoryHierarchy;

	p_Output.clear();

	auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(app);

	size_t kjl = 0;
	for (const auto& AJLid : app->GetAJLJobIDs())
		allSelectedAJL[kjl++] = JSON_STRING(AJLid);

	size_t k = 0, kSchedule = 0;
	for (auto& j : getAllJobs())
	{
		j.setLastLevelCategoryIDs(categoryHierarchy);
		allJobs[k] = makeJSONjob(j);

		for (const auto& js : j.m_Schedules_Vol)
			allScheduled[kSchedule++] = makeJSONschedule(js, j, {});
		for (const auto& p : j.m_Presets_Vol)
			for (const auto& js : p.m_Schedules_Vol)
				allScheduled[kSchedule++] = makeJSONschedule(js, j, p.m_Key);
		k++;
	}

	size_t kParent = 0;
	for (const auto& ch : categoryHierarchy)
	{
		allCategories[kParent] = ch.second.empty() ?
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("id"), JSON_STRING(ch.first.first)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("label"), JSON_STRING(ch.first.second)
				JSON_END_PAIR
			JSON_END_OBJECT
			:
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("id"), JSON_STRING(ch.first.first)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("label"), JSON_STRING(ch.first.second)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("children"),
					JSON_BEGIN_ARRAY
					JSON_END_ARRAY
				JSON_END_PAIR
			JSON_END_OBJECT;


		if (!ch.second.empty())
		{
			size_t kChild = 0;
			auto& categories = allCategories[kParent].as_object()[_YTEXT("children")].as_array();
			for (const auto c : ch.second)
				categories[kChild++] =
				JSON_BEGIN_OBJECT
					JSON_BEGIN_PAIR
						_YTEXT("id"), JSON_STRING(c.first)
					JSON_END_PAIR,
					JSON_BEGIN_PAIR
						_YTEXT("label"), JSON_STRING(c.second)
					JSON_END_PAIR
				JSON_END_OBJECT;
		}
		kParent++;
	}


	auto allJobs_			= allJobs.serialize();
	auto allCategories_		= allCategories.serialize();
	auto allScheduled_		= allScheduled.serialize();
	auto allSelectedAJL_	= allSelectedAJL.serialize();

	if (CRMpipe().IsFeatureSandboxed())
	{
		const auto path = FileUtil::GetTempFolder() + _YTEXT("\\debugJsonMain_DATA-JS-alljobs.json");
		std::ofstream ostr(path, std::ios::out | std::ios::trunc);
		ostr << Str::convertToUTF8(ChilkatUtils::BeautifyJson(_YTEXTFORMAT(L"{\"value\":%s}", allJobs_.c_str())));
	}
	if (CRMpipe().IsFeatureSandboxed())
	{
		const auto path = FileUtil::GetTempFolder() + _YTEXT("\\debugJsonMain_DATA-JS-allCategories.json");
		std::ofstream ostr(path, std::ios::out | std::ios::trunc);
		ostr << Str::convertToUTF8(ChilkatUtils::BeautifyJson(_YTEXTFORMAT(L"{\"value\":%s}", allCategories_.c_str())));
	}
	if (CRMpipe().IsFeatureSandboxed())
	{
		const auto path = FileUtil::GetTempFolder() + _YTEXT("\\debugJsonMain_DATA-JS-allScheduled.json");
		std::ofstream ostr(path, std::ios::out | std::ios::trunc);
		ostr << Str::convertToUTF8(ChilkatUtils::BeautifyJson(_YTEXTFORMAT(L"{\"value\":%s}", allScheduled_.c_str())));
	}
	if (CRMpipe().IsFeatureSandboxed())
	{
		const auto path = FileUtil::GetTempFolder() + _YTEXT("\\debugJsonMain_DATA-JS-allSelectedAJL.json");
		std::ofstream ostr(path, std::ios::out | std::ios::trunc);
		ostr << Str::convertToUTF8(ChilkatUtils::BeautifyJson(_YTEXTFORMAT(L"{\"value\":%s}", allSelectedAJL_.c_str())));
	}

	p_Output += _YTEXT("var allJobs = ") + allJobs_ + _YTEXT(";\n");
	p_Output += _YTEXT("var allCategories = ") + allCategories_ + _YTEXT(";\n");
	p_Output += _YTEXT("var allScheduled = ") + allScheduled_ + _YTEXT(";\n");
	p_Output += _YTEXT("var allSelectedAJL = ") + allSelectedAJL_ + _YTEXT(";\n");
	p_Output += _YTEXT("var maxSelectedAJL = ") + Str::getStringFromNumber(app->GetAJLCapacity()) + _YTEXT(";\n");

	if (CRMpipe().IsFeatureSandboxed())
	{
		const auto path = FileUtil::GetTempFolder() + _YTEXT("\\debugJsonMain_DATA-JS.js");
		std::ofstream ostr(path, std::ios::out | std::ios::trunc);
		ostr << Str::convertToUTF8(ChilkatUtils::BeautifyJson(p_Output));
	}
}

bool O365HtmlMainView::HasNoJob() const
{
	return m_HasNoJob;
}

void O365HtmlMainView::UpdateAJLInfo()
{
	if (m_IsReadyForExecuteScript)
	{
		auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
			ExecuteScript(_YTEXTFORMAT(L"updateAJLInfo(\"%s\", \"%s\")", Str::implode(app->GetAJLJobIDs(), JobCenterUtil::g_CatalogAJLSeparator, false).c_str(), Str::getStringFromNumber(app->GetAJLCapacity()).c_str()));
	}
}

void O365HtmlMainView::RemoveOneJob(const Script& p_Job)
{
	if (m_IsReadyForExecuteScript)
		ExecuteScript(_YTEXTFORMAT(L"removeOneJob(\"%s\")", p_Job.GetHTMLid().c_str()));
}

void O365HtmlMainView::RemoveOneSchedule(const ScriptSchedule& p_JobSchedule)
{
	if (m_IsReadyForExecuteScript)
		ExecuteScript(_YTEXTFORMAT(L"removeOneSched(\"%s\")", p_JobSchedule.GetHTMLid().c_str()));
}

void O365HtmlMainView::UpdateCanRemoveJobs()
{		
	if (m_IsReadyForExecuteScript)
	{
		auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
		{
			const bool		canRemove		= app->CanLicenseAJLremoveJobs();
			const wstring	canRemoveFlag	= canRemove ? _YTEXT("X") : _YTEXT("");// empty: disable
			const wstring	countFormat		= canRemove ? _T(" ( Added %N% / Removed %D%)") : _T(" (Added %N%)");
			ExecuteScript(_YTEXTFORMAT(L"changeAJLCanRemove(\"%s\", \"%s\")", canRemoveFlag.c_str(), countFormat.c_str()));
		}
	}
}

void O365HtmlMainView::UpdateAll()
{
	if (m_IsReadyForExecuteScript)
	{
		ASSERT(HasNoJob());
		if (HasNoJob())
		{
			wstring newJSdata;
			generateJSONscriptDataJS(newJSdata);
			ExecuteScript(_YTEXTFORMAT(L"fullInitialization(\"%s\")", Str::EscapeJSONstring(newJSdata).c_str()));
		}
	}
}

void O365HtmlMainView::UpdateAllJobs()
{
	if (m_IsReadyForExecuteScript)
	{
		json::value allJobs = json::value::array();
		size_t k = 0;
		for (auto& j : getAllJobs())
			allJobs[k++] = makeJSONjob(j);

		auto allJobs_ = allJobs.serialize();

		if (CRMpipe().IsFeatureSandboxed())
		{
			const auto path = FileUtil::GetTempFolder() + _YTEXT("\\debugJsonMain_DATA-JS-updateAlljobs.json");
			std::ofstream ostr(path, std::ios::out | std::ios::trunc);
			ostr << Str::convertToUTF8(ChilkatUtils::BeautifyJson(_YTEXTFORMAT(L"{\"value\":%s}", allJobs_.c_str())));
		}

		ExecuteScript(_YTEXTFORMAT(L"updateAllJobs(\"%s\")", Str::EscapeJSONstring(allJobs_).c_str()));
	}
}

void O365HtmlMainView::UpdateAllSchedules()
{
	if (m_IsReadyForExecuteScript)
	{
		json::value allScheduled = json::value::array();

		size_t kSchedule = 0;
		for (auto& j : getAllJobs())
		{
			for (const auto& js : j.m_Schedules_Vol)
				allScheduled[kSchedule++] = makeJSONschedule(js, j, {});
			for (const auto& p : j.m_Presets_Vol)
				for (const auto& js : p.m_Schedules_Vol)
					allScheduled[kSchedule++] = makeJSONschedule(js, j, p.m_Key);
		}

		auto allScheduled_ = allScheduled.serialize();

		if (CRMpipe().IsFeatureSandboxed())
		{
			const auto path = FileUtil::GetTempFolder() + _YTEXT("\\debugJsonMain_DATA-JS-allScheduled.json");
			std::ofstream ostr(path, std::ios::out | std::ios::trunc);
			ostr << Str::convertToUTF8(ChilkatUtils::BeautifyJson(_YTEXTFORMAT(L"{\"value\":%s}", allScheduled_.c_str())));
		}

		ExecuteScript(_YTEXTFORMAT(L"updateAllScheds(\"%s\")", Str::EscapeJSONstring(allScheduled_).c_str()));
	}
}

json::value O365HtmlMainView::makeJSONjob(Script& p_Job)
{
	p_Job.setLastLevelCategoryIDs();
	auto jobJSON = 
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("id"), JSON_STRING(p_Job.GetHTMLid())
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("ajlid"), JSON_STRING(p_Job.m_LibraryID)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("link"), JSON_STRING(p_Job.m_IsAJL_vol ? p_Job.m_Key + g_AJLtag : p_Job.m_Key)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("source"), JSON_STRING(p_Job.IsSystem() ? _YTEXT("system") : _YTEXT("custom"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("family"), JSON_STRING(p_Job.m_Family)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("icon"), JSON_STRING(p_Job.m_IconFontAwesome)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("action"), JSON_STRING(p_Job.m_Action)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("isnew"), json::value::boolean(MFCUtil::StringMatchW(p_Job.m_IsNew, _YTEXT("true")))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("isSandbox"), JSON_STRING(p_Job.IsSandbox() ? _YTEXT("X") : _YTEXT(""))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("created"),
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("by"), JSON_STRING(p_Job.IsSystem() ? _YTEXT("Ytria") : p_Job.m_UpdateByName)
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("on"), JSON_STRING(p_Job.m_Created)
						JSON_END_PAIR
					JSON_END_OBJECT
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("lastedit"),
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("by"), JSON_STRING(p_Job.IsSystem() ? _YTEXT("Ytria") : p_Job.m_UpdateByName)
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("on"), JSON_STRING(p_Job.IsSystem() ? p_Job.m_LastEdit : TimeUtil::GetInstance().GetISO8601String(p_Job.m_UpdDate))
						JSON_END_PAIR
					JSON_END_OBJECT
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("categories"),
					JSON_BEGIN_ARRAY
					JSON_END_ARRAY
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("label"), JSON_STRING(p_Job.m_Title)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("keywords"), JSON_STRING(Str::replace(p_Job.m_Keywords, _YTEXT(";"), _YTEXT(",")))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("description"), JSON_STRING(p_Job.m_Description)
				JSON_END_PAIR
			JSON_END_OBJECT;

	auto& categories = jobJSON.as_object()[_YTEXT("categories")].as_array();
	size_t kTegories = 0;
	for (const auto& c : p_Job.m_LastLevelCategoryIDs_vol)
		categories[kTegories++] = JSON_STRING(c);
	ASSERT(categories.size() != 0 || !p_Job.IsSystem());

	return jobJSON;
}

json::value O365HtmlMainView::makeJSONschedule(const ScriptSchedule& p_JobSchedule, const Script& p_Job, const wstring& p_JobPresetKey)
{
	wstring schLink = p_JobPresetKey.empty() ? p_Job.m_Key : p_Job.m_Key + JobCenterUtil::g_URLTAG_Preset + p_JobPresetKey;
	if (p_Job.m_IsAJL_vol)
		schLink += g_AJLtag;

	// TODO store schedule data properly in DB columns
	static const wstring g_Starting = _T("starting");
	wstring frequency, recur_every, startsOn;
	const auto pFreq = p_JobSchedule.m_Description.find(_YTEXT(":"));
	if (pFreq != wstring::npos)
		frequency = p_JobSchedule.m_Description.substr(0, pFreq);
	const auto pStartsOn = p_JobSchedule.m_Description.find(g_Starting);
	if (pStartsOn != wstring::npos)
		startsOn = p_JobSchedule.m_Description.substr(pStartsOn + g_Starting.length());
	if (pFreq != wstring::npos && pStartsOn != wstring::npos)
		recur_every = p_JobSchedule.m_Description.substr(pFreq + 1, pStartsOn - 1);

	return 	JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("id"), JSON_STRING(p_JobSchedule.GetHTMLid())
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("link"), JSON_STRING(schLink)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("idJob"), JSON_STRING(p_Job.GetHTMLid())
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("lastedit"),
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("by"), JSON_STRING(p_JobSchedule.m_UpdateByName)
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("on"), JSON_STRING(TimeUtil::GetInstance().GetISO8601String(p_JobSchedule.m_UpdDate))
						JSON_END_PAIR
					JSON_END_OBJECT
				JSON_END_PAIR,
		// 									JSON_BEGIN_PAIR
		// 										_YTEXT("lastedit"),
		// 										JSON_BEGIN_OBJECT
		// 											JSON_BEGIN_PAIR
		// 												_YTEXT("by"), JSON_STRING(j.IsSystem() ? _YTEXT("Ytria") : j.m_UpdateByName)
		// 											JSON_END_PAIR,
		// 											JSON_BEGIN_PAIR
		// 												_YTEXT("on"), JSON_STRING(j.IsSystem() ? j.m_LastEdit : TimeUtil::GetInstance().GetISO8601String(j.m_UpdDate))
		// 											JSON_END_PAIR
		// 										JSON_END_OBJECT
		// 									JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("startson"), JSON_STRING(startsOn)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("frequency"), JSON_STRING(frequency)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("recur_every"), JSON_STRING(recur_every)
				JSON_END_PAIR
		JSON_END_OBJECT;
}