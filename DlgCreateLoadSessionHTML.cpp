#include "DlgCreateLoadSessionHTML.h"

#include "MainFrame.h"
#include "Str.h"
#include "TimeUtil.h"
#include "JSONUtil.h"
#include "RoleDelegationManager.h"
#include "UISessionListSorter.h"

BEGIN_MESSAGE_MAP(DlgCreateLoadSessionHTML, ResizableDialog)
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()

std::map<wstring, int, Str::keyLessInsensitive> DlgCreateLoadSessionHTML::g_HbsHeights;

wstring DlgCreateLoadSessionHTML::g_NewAdmin;
wstring DlgCreateLoadSessionHTML::g_NewUltraAdmin;
wstring DlgCreateLoadSessionHTML::g_NewStandard;

DlgCreateLoadSessionHTML::DlgCreateLoadSessionHTML(const vector<PersistentSession>& p_Sessions, bool noUltraAdminOrRole/* = false*/)
    : ResizableDialog(IDD)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_totalHbsHeight(0)
	, m_Mode(Mode::LOAD_SESSION)
	, m_MaxWidth(0)
	, m_NoUltraAdminOrRole(noUltraAdminOrRole)
	, m_Sessions(p_Sessions)
	, m_IsElevating(false)
	, m_NoNewSession(false)
{
	if (g_NewAdmin.empty())
		g_NewAdmin = YtriaTranslate::Do(DlgCreateLoadSessionHTML_DlgCreateLoadSessionHTML_1, _YLOC("New Advanced Session")).c_str();

	if (g_NewUltraAdmin.empty())
		g_NewUltraAdmin = YtriaTranslate::Do(DlgCreateLoadSessionHTML_DlgCreateLoadSessionHTML_2, _YLOC("New Ultra Admin Session")).c_str();

	if (g_NewStandard.empty())
		g_NewStandard = YtriaTranslate::Do(DlgCreateLoadSessionHTML_DlgCreateLoadSessionHTML_3, _YLOC("New Standard Session")).c_str();

	// Load hbs sizes
	if (g_HbsHeights.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_SESSIONLIST_SIZE, g_HbsHeights);
		ASSERT(success);
	}
}

void DlgCreateLoadSessionHTML::SetIsElevating(bool p_IsElevating)
{
	m_IsElevating = p_IsElevating;
}

void DlgCreateLoadSessionHTML::SetNoNewSession(bool p_NoNewSession)
{
	m_NoNewSession = p_NoNewSession;
}

void DlgCreateLoadSessionHTML::SetIntro(const wstring& p_IntroTitle, const wstring& p_IntroDesc)
{
	// No desc without title
	ASSERT(p_IntroDesc.empty() || !p_IntroTitle.empty());

	m_IntroTitle = p_IntroTitle;
	m_IntroDesc = p_IntroDesc;
}

bool DlgCreateLoadSessionHTML::ShouldCreateStandardSession()
{
    return Mode::NEW_STANDARD_USER == m_Mode;
}

bool DlgCreateLoadSessionHTML::ShouldCreateAdvancedSession()
{
	return Mode::NEW_ADVANCED_USER == m_Mode;
}

bool DlgCreateLoadSessionHTML::ShouldCreateUltraAdminSession()
{
    return Mode::NEW_ULTRA_ADMIN_OR_SKIP == m_Mode;
}

DlgCreateLoadSessionHTML::Mode DlgCreateLoadSessionHTML::GetWantedAction() const
{
	return m_Mode;
}

SessionIdentifier DlgCreateLoadSessionHTML::GetSelectedSession() const
{
    auto sessionInfos = Str::explodeIntoVector(m_SessionToLoad, wstring(_YTEXT("|")));

    SessionIdentifier sessionId;

    ASSERT(sessionInfos.size() == 3);
    if (sessionInfos.size() == 3)
    {
        sessionId.m_EmailOrAppId = sessionInfos[0];
        sessionId.m_SessionType = sessionInfos[1];
        try
        {
            sessionId.m_RoleID = stoll(sessionInfos[2]);
        }
        catch (const std::exception&)
        {
            sessionId.m_RoleID = SessionIdentifier::g_NoRole;
        }
    }

    return sessionId;
}

bool DlgCreateLoadSessionHTML::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	ASSERT(data.size() == 1);

	if (data.size() == 1)
	{
		const wstring& toLoad = data.begin()->first;
		if (toLoad == g_NewAdmin)
			NewAdvancedSession();
		else if (toLoad == g_NewUltraAdmin || toLoad == _YTEXT("skip"))
			NewUltraAdminOrSkip();
		else if (toLoad == g_NewStandard)
			NewStandardSession();
		else
			LoadSession(toLoad);

		return false;
	}

	return true;
}

BOOL DlgCreateLoadSessionHTML::OnInitDialogSpecificResizable()
{
	SetWindowText(YtriaTranslate::Do(DlgCreateLoadSessionHTML_OnInitDialogSpecificResizable_1, _YLOC("Select Session")).c_str());

	CWnd* pCtrl = GetDlgItem(IDC_GRID_PLACEHOLDER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(nullptr, nullptr, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, nullptr);

	// FIXME: Temporary - Remove when no more needed.
	//m_HtmlView->SetDebugDisplayPostedData(true);

	m_HtmlView->SetPostedDataTarget(this);

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));
	
	CRect dlgRect;
	GetWindowRect(dlgRect);

	const int maxHeight = [=]()
	{
		HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
		MONITORINFO info;
		info.cbSize = sizeof(MONITORINFO);
		GetMonitorInfo(monitor, &info);
		return 2 * (info.rcMonitor.bottom - info.rcMonitor.top) / 3;
	}();

	const auto addVMarging = [](int p_Height)
	{
		{
			const auto it = g_HbsHeights.find(_YTEXT("marginTop"));
			if (g_HbsHeights.end() != it)
				p_Height += it->second;
		}
		{
			const auto it = g_HbsHeights.find(_YTEXT("marginBottom"));
			if (g_HbsHeights.end() != it)
				p_Height += it->second;
		}

		return p_Height;
	};

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData, [&addVMarging, maxHeight, &htmlRect, &dlgRect](int p_Height)
		{
			const int deltaH = HIDPI_YH(addVMarging(p_Height)) - htmlRect.Height();
			return maxHeight < dlgRect.Height() + deltaH;
		});


	m_HtmlView->SetScriptsToReplace(
		{
			 { _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
			,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-sessionList.js"), _YTEXT("") } } // This file is in resources
			,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("sessionList.js"), _YTEXT("") } }, // This file is in resources
		}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("sessionList.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));


	const int height = [&addVMarging, maxHeight, &htmlRect, &dlgRect](int p_Height)
	{
		ASSERT(0 != p_Height);
		if (0 != p_Height)
		{
			const int deltaH = HIDPI_YH(addVMarging(p_Height)) - htmlRect.Height();
			return min(maxHeight, dlgRect.Height() + deltaH);
		}

		return maxHeight;
	}(m_totalHbsHeight);

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_HbsHeights.find(_YTEXT("min-width-Window"));
			ASSERT(g_HbsHeights.end() != it);
			if (g_HbsHeights.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();
		const int delta = targetHtmlWidth - htmlRect.Width();
		return dlgRect.Width() + delta;
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);

	// Limit width to avoid erratic behavior that appears beyond this width.
	m_MaxWidth = HIDPI_XW(1040);

	BlockVResize(true);

    return TRUE;
}

void DlgCreateLoadSessionHTML::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)
{
	ResizableDialog::OnGetMinMaxInfo(lpMMI);

	lpMMI->ptMaxSize.x = m_MaxWidth;
	lpMMI->ptMaxTrackSize.x = m_MaxWidth;
}

void DlgCreateLoadSessionHTML::NewAdvancedSession()
{
	m_Mode = Mode::NEW_ADVANCED_USER;
	endDialogFixed(IDOK);
}

void DlgCreateLoadSessionHTML::NewStandardSession()
{
	m_Mode = Mode::NEW_STANDARD_USER;
	endDialogFixed(IDOK);
}

void DlgCreateLoadSessionHTML::NewUltraAdminOrSkip()
{
	m_Mode = Mode::NEW_ULTRA_ADMIN_OR_SKIP;
	endDialogFixed(IDOK);
}

void DlgCreateLoadSessionHTML::LoadSession(const wstring& sessionName)
{
	m_Mode = Mode::LOAD_SESSION;
	m_SessionToLoad = sessionName;
	endDialogFixed(IDOK);
}

void DlgCreateLoadSessionHTML::generateJSONScriptData(wstring& p_Output, const std::function<bool(int)>& p_IsHeightLimited)
{    
	std::sort(m_Sessions.begin(), m_Sessions.end(), UISessionListSorter());

	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	const PersistentSession& currentSession = nullptr != mainFrame ? mainFrame->GetSessionInfo() : PersistentSession();
		
	m_totalHbsHeight = 0;

    auto main = web::json::value::object({
        { _YTEXT("label"), web::json::value::string(YtriaTranslate::Do(DlgCreateLoadSessionHTML_generateJSONScriptData_1, _YLOC("Last session")).c_str()) },
        { _YTEXT("method"), web::json::value::string(_YTEXT("POST")) },
        { _YTEXT("action"), web::json::value::string(_YTEXT("#")) },
    });

	if (m_IsElevating)
	{
		// Intro is automatically added, the one specified by the called will be ignored.
		ASSERT(m_IntroTitle.empty() && m_IntroDesc.empty());

		main[_YTEXT("isUltraSelect")] = json::value::string(_YTEXT("X"));
		main[_YTEXT("introTitle")] = json::value::string(_T("You have existing Ultra Admin sessions available"));
		main[_YTEXT("introDesc")] = json::value::string(_T("Do you want to use one of these to promote your Advanced session with elevated privileges?<br>Otherwise you can simply continue the setup."));
		main[_YTEXT("btnLabel")] = json::value::string(_T("Skip this step"));
		main[_YTEXT("btnDesc")] = json::value::string(_T("Continue the setup"));
		main[_YTEXT("btnValue")] = json::value::string(_YTEXT("skip"));

		m_totalHbsHeight += 100;
	}
	else
	{
		if (!m_IntroTitle.empty())
		{
			main[_YTEXT("introTitle")] = json::value::string(m_IntroTitle);
			m_totalHbsHeight += 80;
		
			if (!m_IntroDesc.empty())
			{
				// FIXME: Right now, if "isUltraSelect" is not set, "introDesc" is ignored.
				main[_YTEXT("introDesc")] = json::value::string(m_IntroDesc);
				m_totalHbsHeight += 20;
			}
		}
	}

	if (m_NoNewSession)
	{
		// FIXME: remove eventually
		main[_YTEXT("isCustomerSelect")] = json::value::string(_YTEXT("true")); // Remove footer
	}

    auto items = web::json::value::array();

    int itemIndex = 0;
    for (const auto& session : m_Sessions)
        items[itemIndex++] = getJSONSessionEntry(session, currentSession.IsValid() && (currentSession == session));
    
	if (m_IsElevating)
	{
		items[itemIndex++] =
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("hbs"), JSON_STRING(_YTEXT("SessionAdd-User-Or-Admin"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("choices"),
					JSON_BEGIN_ARRAY
						JSON_BEGIN_OBJECT
							JSON_PAIR_KEY(_YTEXT("buttonName"))         JSON_PAIR_VALUE(JSON_STRING(g_NewUltraAdmin)),
							JSON_PAIR_KEY(_YTEXT("type"))               JSON_PAIR_VALUE(JSON_STRING(_YTEXT("ultra_admin"))),
							JSON_PAIR_KEY(_YTEXT("buttonText"))         JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(g_NewUltraAdmin))),
						JSON_END_OBJECT
					JSON_END_ARRAY
				JSON_END_PAIR
			JSON_END_OBJECT;

		m_totalHbsHeight += getHbsHeight(_YTEXT("SessionAdd-User-Or-Admin"));
	}
	else if (!m_NoNewSession)
	{
		items[itemIndex++] = 
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("hbs"),  JSON_STRING(_YTEXT("SessionAdd-User-Or-Admin"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("choices"),
					JSON_BEGIN_ARRAY
						JSON_BEGIN_OBJECT
							JSON_PAIR_KEY(_YTEXT("buttonName"))         JSON_PAIR_VALUE(JSON_STRING(g_NewStandard)),
							JSON_PAIR_KEY(_YTEXT("type"))               JSON_PAIR_VALUE(JSON_STRING(_YTEXT("standard"))),
							JSON_PAIR_KEY(_YTEXT("buttonText"))         JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(g_NewStandard))),
						JSON_END_OBJECT,
						JSON_BEGIN_OBJECT
							JSON_PAIR_KEY(_YTEXT("buttonName"))         JSON_PAIR_VALUE(JSON_STRING(g_NewAdmin)),
							JSON_PAIR_KEY(_YTEXT("type"))               JSON_PAIR_VALUE(JSON_STRING(_YTEXT("advanced"))),
							JSON_PAIR_KEY(_YTEXT("buttonText"))         JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(g_NewAdmin))),
						JSON_END_OBJECT,
						/*JSON_BEGIN_OBJECT
							JSON_PAIR_KEY(_YTEXT("buttonName"))         JSON_PAIR_VALUE(JSON_STRING(g_NewUltraAdmin)),
							JSON_PAIR_KEY(_YTEXT("type"))               JSON_PAIR_VALUE(JSON_STRING(_YTEXT("ultra_admin"))),
							JSON_PAIR_KEY(_YTEXT("buttonText"))         JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(g_NewUltraAdmin))),
						JSON_END_OBJECT*/
					JSON_END_ARRAY
				JSON_END_PAIR
			JSON_END_OBJECT;

		m_totalHbsHeight += getHbsHeight(_YTEXT("SessionAdd-User-Or-Admin"));
	}

	// Add search is not all the cards can be displayed without scrollbar
	if (p_IsHeightLimited(m_totalHbsHeight))
	{
		main[_YTEXT("isSearch")] = json::value::string(_YTEXT("X"));
		m_totalHbsHeight += 80; // Search height?
	}

    const auto root = web::json::value::object({
        { _YTEXT("main"), main },
        { _YTEXT("items"), items}
    });

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}

web::json::value DlgCreateLoadSessionHTML::getJSONSessionEntry(const PersistentSession& p_Session, bool p_IsCurrentSession)
{
	const auto& type = p_Session.GetSessionType();
	const wstring date = [=] {
		CString dateAsText;
		TimeUtil::GetInstance().ConvertDateToText(DateTimeFormat(), p_Session.GetLastUsedOn(), dateAsText);
		return dateAsText;
	}();

	const auto& sessionTechName = p_Session.GetEmailOrAppId();
	const auto& sessionName = p_Session.GetSessionName();

	const auto roleid = p_Session.GetRoleId();
	ASSERT(roleid == 0 || (p_Session.IsElevated() || p_Session.IsPartnerElevated() || p_Session.IsRole()));
	const wstring buttonValue = _YTEXTFORMAT(L"%s|%s|%s", sessionTechName.c_str(), type.c_str(), Str::getStringFromNumber(roleid).c_str());

	wstring roleName = p_Session.GetRoleNameInDb();
	if (!roleName.empty())
		roleName = _YTEXTFORMAT(L"[ %s ]", roleName.c_str());
	
    auto entry = json::value::object();

    entry[_YTEXT("hbs")] = web::json::value::string(_YTEXT("SessionCard"));
	entry[_YTEXT("btnValue")] = web::json::value::string(buttonValue);
	if (roleid > 0)
	{
		// beware: the role and the session name in the UI are permuted here
		entry[_YTEXT("sessionName")] = web::json::value::string(MFCUtil::encodeToHtmlEntitiesExceptTags(roleName));
		entry[_YTEXT("type")] = web::json::value::string(_YTEXT("role_user"));
		entry[_YTEXT("isrole")] = web::json::value::string(_YTEXT("X"));
		entry[_YTEXT("realusername")] = web::json::value::string(YtriaTranslate::Do(DlgCreateLoadSessionHTML_getJSONSessionEntry_1, _YLOC("with: %1"), sessionName.c_str()));
		m_totalHbsHeight += getHbsHeight(_YTEXT("SessionCard-role"));
	}
	else
	{
		entry[_YTEXT("sessionName")] = web::json::value::string(sessionName);
		entry[_YTEXT("type")] = web::json::value::string(type);
		m_totalHbsHeight += getHbsHeight(_YTEXT("SessionCard"));
	}
    entry[_YTEXT("last-session")] = web::json::value::string(MFCUtil::encodeToHtmlEntitiesExceptTags(date));

    if (p_IsCurrentSession || (m_NoUltraAdminOrRole && (p_Session.IsUltraAdmin() || p_Session.IsRole())))
        entry[_YTEXT("disabled")] = web::json::value::string(_YTEXT("X"));

	return entry;
}

UINT DlgCreateLoadSessionHTML::getHbsHeight(const wstring& hbsName)
{
	auto it = g_HbsHeights.find(hbsName);
	ASSERT(g_HbsHeights.end() != it);
	if (g_HbsHeights.end() != it)
		return it->second;
	return 0;
}
