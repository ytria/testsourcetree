#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessSite.h"
#include "RoleDelegationTagger.h"

class SiteDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessSite>, public RoleDelegationTagger
{
public:
	SiteDeserializer(std::shared_ptr<const Sapio365Session> p_Session);

#ifdef _DEBUG
	// ctor only used in WholeSiteDeserializer to avoid assert when some properties are missing.
	SiteDeserializer(std::shared_ptr<const Sapio365Session> p_Session, bool p_NoSelectUsed);
#endif

    virtual void DeserializeObject(const web::json::object& p_Object) override;

private:
#ifdef _DEBUG
	bool m_NoSelectUsed = false;
#endif

	friend class SiteDeserializerFactory;
	SiteDeserializer();
};

