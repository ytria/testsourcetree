#include "GridDirectoryRoles.h"

#include "AutomationWizardDirectoryRoles.h"
#include "BasicGridSetup.h"
#include "BusinessUserGuest.h"
#include "CreatedObjectModification.h"
#include "DeletedObjectModification.h"
#include "DlgAddItemsToGroups.h"
#include "FrameDirectoryRoles.h"
#include "GridUpdater.h"

BEGIN_MESSAGE_MAP(GridDirectoryRoles, O365Grid)
#if HAS_ACTIVATE_ROLE_COMMAND
    ON_COMMAND(ID_DIRECTORYROLESGRID_ACTIVATEROLE, OnActivateRole)
    ON_UPDATE_COMMAND_UI(ID_DIRECTORYROLESGRID_ACTIVATEROLE, OnUpdateActivateRole)
#endif
#if HAS_DEACTIVATE_ROLE_COMMAND
    ON_COMMAND(ID_DIRECTORYROLESGRID_DEACTIVATEROLE, OnDeactivateRole)
    ON_UPDATE_COMMAND_UI(ID_DIRECTORYROLESGRID_DEACTIVATEROLE, OnUpdateDeactivateRole)
#endif
	ON_COMMAND(ID_DIRECTORYROLESGRID_ADDMEMBER, OnAddMember)
	ON_UPDATE_COMMAND_UI(ID_DIRECTORYROLESGRID_ADDMEMBER, OnUpdateAddMember)
	ON_COMMAND(ID_DIRECTORYROLESGRID_REMOVEMEMBER, OnRemoveMember)
	ON_UPDATE_COMMAND_UI(ID_DIRECTORYROLESGRID_REMOVEMEMBER, OnUpdateRemoveMember)
END_MESSAGE_MAP()

GridDirectoryRoles::GridDirectoryRoles()
{
	m_MayContainUnscopedUserGroupOrSite = true;

	EnableGridModifications();

	initWizard<AutomationWizardDirectoryRoles>(_YTEXT("Automation\\DirectoryRoles"));
}

ModuleCriteria GridDirectoryRoles::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria;

    GridFrameBase* frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
    ASSERT(nullptr != frame);
    if (nullptr != frame)
    {
        criteria = frame->GetModuleCriteria();
        criteria.m_IDs.clear();
        for (auto row : p_Rows)
        {
            PooledString templateId = row->GetTopAncestorOrThis()->GetField(GetColId()).GetValueStr();
            criteria.m_IDs.insert(templateId);
        }
    }

    return criteria;
}

void GridDirectoryRoles::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameDirectoryRoles, LicenseUtil::g_codeSapio365directoryRoles);
		setup.Setup(*this, true);
		m_ColumnLastRequestDateTime = setup.GetColumnLastRequestDateTime();
	}

    setBasicGridSetupHierarchy({ { { m_ColRoleDisplayName, 0 } }
								, O365Grid::BasicGridSetupHierarchy::AllUserObjectTypes()
								, { rttr::type::get<BusinessUser>().get_id(), rttr::type::get<BusinessDirectoryRoleTemplate>().get_id() } });

    m_ColRoleDisplayName		= AddColumn(_YUID(O365_DIRECTORYROLETEMPLATE_DISPLAYNAME), YtriaTranslate::Do(GridDirectoryRoles_customizeGrid_1, _YLOC("Display Name")).c_str(), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
    m_ColDescription			= AddColumn(_YUID(O365_DIRECTORYROLETEMPLATE_DESCRIPTION), YtriaTranslate::Do(GridDirectoryRoles_customizeGrid_2, _YLOC("Description")).c_str(), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
    m_ColActivated				= AddColumn(_YUID("activated"), YtriaTranslate::Do(GridDirectoryRoles_customizeGrid_3, _YLOC("Role Activated")).c_str(), g_FamilyInfo, HIDPI_XW(22), { g_ColumnsPresetDefault });
	// FIXME: UID is duplicated (O365_DIRECTORYROLETEMPLATE_DISPLAYNAME == O365_USER_DISPLAYNAME), find another decent one.
    m_ColUserDisplayName		= AddColumn(_YUID("userDisplayName"/*O365_USER_DISPLAYNAME*/), YtriaTranslate::Do(GridDirectoryRoles_customizeGrid_4, _YLOC("User Display Name")).c_str(), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
    m_ColUserPrincipalName		= AddColumn(_YUID(O365_USER_USERPRINCIPALNAME), YtriaTranslate::Do(GridDirectoryRoles_customizeGrid_5, _YLOC("User Principal Name")).c_str(), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColUserType				= AddColumn(_YUID(O365_USER_USERTYPE), YtriaTranslate::Do(GridUsers_customizeGrid_3, _YLOC("User Type")).c_str(), g_FamilyInfo, g_ColumnSize12);
	m_ColRoleId					= AddColumn(_YUID("roleId"), YtriaTranslate::Do(GridDirectoryRoles_customizeGrid_6, _YLOC("Role ID")).c_str(), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetTech });
	m_ColRoleTemplateId			= AddColumn(_YUID("roleTemplateId"), YtriaTranslate::Do(GridDirectoryRoles_customizeGrid_7, _YLOC("Role Template ID")).c_str(), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetTech });

    addColumnGraphID();
    AddColumnForRowPK(GetColId());
	AddColumnForRowPK(m_ColRoleDisplayName);
    SetHierarchyColumnsParentPKs({ { m_ColRoleDisplayName, GridBackendUtil::g_AllHierarchyLevels} });

	m_RoleActivated = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ACTIVATED_ROLE));
	m_RoleDeactivated = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_DEACTIVATED_ROLE));

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameDirectoryRoles*>(GetParentFrame()));
}

wstring GridDirectoryRoles::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Role"), m_ColRoleDisplayName } }, m_ColUserDisplayName);
}

O365Grid::SnapshotCaps GridDirectoryRoles::GetSnapshotCapabilities() const
{
	return { true };
}

void GridDirectoryRoles::InitializeCommands()
{
	O365Grid::InitializeCommands();

    const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);
    {

        // Activate role
#if HAS_ACTIVATE_ROLE_COMMAND
		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID = ID_DIRECTORYROLESGRID_ACTIVATEROLE;
			_cmd.m_sMenuText = YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_1, _YLOC("Activate Role")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_5, _YLOC("Activate Role")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DIRECTORYROLESGRID_ACTIVATEROLE_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_DIRECTORYROLESGRID_ACTIVATEROLE, hIcon, false);
			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_9, _YLOC("Activate Role")).c_str(),
				YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_10, _YLOC("Activates the selected Role")).c_str(),
				_cmd.m_sAccelText);
		}
#endif

        // Deactivate role
#if HAS_DEACTIVATE_ROLE_COMMAND
		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID = ID_DIRECTORYROLESGRID_DEACTIVATEROLE;
			_cmd.m_sMenuText = YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_2, _YLOC("Deactivate Role")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_6, _YLOC("Deactivate Role")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DIRECTORYROLESGRID_DEACTIVATEROLE_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_DIRECTORYROLESGRID_DEACTIVATEROLE, hIcon, false);
			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_11, _YLOC("Deactivate Role")).c_str(),
				YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_12, _YLOC("Deactivates the selected Role")).c_str(),
				_cmd.m_sAccelText);
		}
#endif

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID = ID_DIRECTORYROLESGRID_ADDMEMBER;
			_cmd.m_sMenuText = YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_3, _YLOC("Assign Roles")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_7, _YLOC("Assign Roles")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DIRECTORYROLESGRID_ADDMEMBER_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_DIRECTORYROLESGRID_ADDMEMBER, hIcon, false);
			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_13, _YLOC("Assign Roles to Users")).c_str(),
				YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_14, _YLOC("Assign the selected role type to users.")).c_str(),
				_cmd.m_sAccelText);
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID = ID_DIRECTORYROLESGRID_REMOVEMEMBER;
			_cmd.m_sMenuText = YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_4, _YLOC("Remove Roles")).c_str();
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_8, _YLOC("Remove Roles")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
			HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DIRECTORYROLESGRID_REMOVEMEMBER_16X16);
			ASSERT(hIcon);
			g_CmdManager->CmdSetIcon(profileName, ID_DIRECTORYROLESGRID_REMOVEMEMBER, hIcon, false);
			setRibbonTooltip(_cmd.m_nCmdID,
				YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_15, _YLOC("Remove Roles from Users")).c_str(),
				YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_16, _YLOC("Remove the selected role assignments from users.")).c_str(),
				_cmd.m_sAccelText);
		}
    }
}

void GridDirectoryRoles::BuildView(DirectoryRolesInfo p_RolesInfo, const vector<O365UpdateOperation>& p_UpdateOperations, const RefreshSpecificData& p_RefreshSpecificData, bool p_FullPurge)
{
	const uint32_t options	= GridUpdaterOptions::UPDATE_GRID
							| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
							| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
							| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
							| GridUpdaterOptions::REFRESH_PROCESS_DATA
							;
	GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateOperations);

    std::map<PooledString, BusinessDirectoryRole> templateIdRole = GetRolesMap(p_RolesInfo);

	bool oneCanceled = false;
	bool oneOK = false;
    for (const auto& roleTemplate : p_RolesInfo.m_Templates)
    {
        GridBackendField fieldId(roleTemplate.GetID(), GetColId());
		ASSERT(roleTemplate.GetDisplayName());
		GridBackendField fieldDisplayName(roleTemplate.GetDisplayName() ? *roleTemplate.GetDisplayName() : _YTEXT(""), m_ColRoleDisplayName);

		vector<GridBackendField> templateRowPk{ fieldId, fieldDisplayName };
		const bool wasExisting = nullptr != m_RowIndex.GetExistingRow(templateRowPk);
        auto rowTemplate = m_RowIndex.GetRow(templateRowPk, true, updater.GetOptions().IsFullPurge());

        ASSERT(nullptr != rowTemplate);
        if (nullptr != rowTemplate)
        {
			updater.GetOptions().AddRowWithRefreshedValues(rowTemplate);
			rowTemplate->SetHierarchyParentToHide(true);

			if (roleTemplate.HasFlag(BusinessObject::CANCELED))
			{
				oneCanceled = true;

				// Could be uncommented if we notice missing explosion refresh on canceled rows
				//auto children = GetChildRows(rowTemplate);
				//for (auto child : children)
				//	updater.GetOptions().AddRowWithRefreshedValues(child);
			}
			else
			{
				oneOK = true;
				rowTemplate->AddField(roleTemplate.GetLastRequestTime(), m_ColumnLastRequestDateTime);

				// Do we have a role instantiated corresponding to template?
				auto ittTemplate = templateIdRole.find(roleTemplate.GetID());
				if (ittTemplate != templateIdRole.end())
				{
					const auto& roles = ittTemplate->second;
					rowTemplate->AddField(YtriaTranslate::Do(GridDirectoryRoles_AddDirectoryRoleData_2, _YLOC("Activated")).c_str(), m_ColActivated, m_RoleActivated);
					AddDirectoryRoleData(rowTemplate, roleTemplate, roles, updater);
					updater.GetOptions().AddPartialPurgeParentRow(rowTemplate);
				}
				else
				{
					rowTemplate->RemoveField(m_ColRoleId);
					rowTemplate->AddField(roleTemplate.GetID(), m_ColRoleTemplateId);
					rowTemplate->AddField(YtriaTranslate::Do(GridDirectoryRoles_AddDirectoryRoleData_3, _YLOC("Deactivated")).c_str(), m_ColActivated, m_RoleDeactivated);
					updater.GetOptions().AddPartialPurgeParentRow(rowTemplate);
				}
			}

			if (!wasExisting || !roleTemplate.HasFlag(BusinessObject::CANCELED))
			{
				SetRowObjectType(rowTemplate, roleTemplate, roleTemplate.HasFlag(BusinessObject::CANCELED));
				rowTemplate->AddField(roleTemplate.GetID(), GetColId());
				rowTemplate->AddField(roleTemplate.GetDescription(), m_ColDescription);
			}
        }
    }

	if (oneCanceled)
	{
		if (oneOK)
			updater.GetOptions().SetPurgeFlag(GridUpdaterOptions::PARTIALPURGE); // Only purge not-canceled top rows
		else
			updater.GetOptions().SetPurgeFlag(0); // Everything canceled, don't purge anything.
	}
}

std::map<PooledString, BusinessDirectoryRole> GridDirectoryRoles::GetRolesMap(DirectoryRolesInfo &p_RolesInfo)
{
    std::map<PooledString, BusinessDirectoryRole> idRoles;
    for (const auto& role : p_RolesInfo.m_Roles)
    {
        const PooledString id = role.GetRoleTemplateId() ? *role.GetRoleTemplateId() : _YTEXT("");
        ASSERT(!id.IsEmpty());
        if (!id.IsEmpty())
            idRoles[id] = role;
    }

    return idRoles;
}

bool GridDirectoryRoles::activateRole(GridBackendRow* p_RoleRow)
{
	ASSERT(nullptr != p_RoleRow);
	if (nullptr != p_RoleRow && p_RoleRow->GetField(m_ColActivated).GetIcon() != m_RoleActivated)
	{
		const auto oldField = p_RoleRow->GetField(m_ColActivated);
		const auto& newField = p_RoleRow->AddField(YtriaTranslate::Do(GridDirectoryRoles_activateRole_1, _YLOC("Activated")).c_str(), m_ColActivated, m_RoleActivated);
		OnRowModified(p_RoleRow);

		row_pk_t rowPk;
		GetRowPK(p_RoleRow, rowPk);
		GetModifications().Add(std::make_unique<FieldUpdateO365>(*this, rowPk, m_ColActivated->GetID(), oldField, newField));
		return true;
	}
	return false;
}

void GridDirectoryRoles::AddDirectoryRoleData(GridBackendRow* p_RowTemplate, const BusinessDirectoryRoleTemplate& p_Template, const BusinessDirectoryRole& p_Role, GridUpdater& p_Updater)
{
	p_RowTemplate->AddField(p_Role.GetID(), m_ColRoleId);
	p_RowTemplate->AddField(p_Role.GetRoleTemplateId(), m_ColRoleTemplateId);
    for (const auto& user : p_Role.GetMembers())
    {
        GridBackendField fieldId(user.GetID(), GetColId());
		ASSERT(p_Template.GetDisplayName());
		GridBackendField fieldDisplayName(p_Template.GetDisplayName() ? *p_Template.GetDisplayName() : _YTEXT(""), m_ColRoleDisplayName);

        GridBackendRow* rowUser = m_RowIndex.GetRow({ fieldId, fieldDisplayName }, true, true);
		ASSERT(nullptr != rowUser);
		if (nullptr != rowUser)
		{
			p_Updater.GetOptions().AddRowWithRefreshedValues(rowUser);

			rowUser->AddField(user.GetDisplayName(), m_ColUserDisplayName);
			rowUser->AddField(user.GetUserPrincipalName(), m_ColUserPrincipalName);
			rowUser->AddField(user.GetUserType(), m_ColUserType);

			rowUser->AddField(p_Template.GetDescription(), m_ColDescription);

			rowUser->AddField(YtriaTranslate::Do(GridDirectoryRoles_AddDirectoryRoleData_1, _YLOC("Activated")).c_str(), m_ColActivated, m_RoleActivated);
			rowUser->AddField(p_Template.GetDisplayName(), m_ColRoleDisplayName);

			ASSERT(user.GetUserType().is_initialized());
			GridUtil::SetUserRowObjectType(*this, rowUser, user);

			rowUser->SetParentRow(p_RowTemplate);
			rowUser->AddField(p_RowTemplate->GetField(m_ColumnLastRequestDateTime).GetValueTimeDate(), m_ColumnLastRequestDateTime);

			AddRoleDelegationFlag(rowUser, user);

			p_Updater.AddUpdatedRowPk({ fieldId, fieldDisplayName });
		}
    }

    AddHierarchyObjectTypeToFilterOn(GetObjectTypeIndex(BusinessUser()));
    RemoveHierarchyObjectTypeToFilterOn(GetObjectTypeIndex(BusinessDirectoryRoleTemplate()));
}

GridBackendColumn* GridDirectoryRoles::GetColRoleId()
{
    return m_ColRoleId;
}

void GridDirectoryRoles::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {
#if HAS_ACTIVATE_ROLE_COMMAND
		pPopup->ItemInsert(ID_DIRECTORYROLESGRID_ACTIVATEROLE);
#endif
#if HAS_DEACTIVATE_ROLE_COMMAND
		pPopup->ItemInsert(ID_DIRECTORYROLESGRID_DEACTIVATEROLE);
#endif
		pPopup->ItemInsert(ID_DIRECTORYROLESGRID_ADDMEMBER);
		pPopup->ItemInsert(ID_DIRECTORYROLESGRID_REMOVEMEMBER);
        pPopup->ItemInsert(); // Sep
    }

	O365Grid::OnCustomPopupMenu(pPopup, nStart);
}

GridDirectoryRoles::RoleChange GridDirectoryRoles::initChange(GridBackendRow* p_RowTemplate) const
{
	RoleChange change;
	ASSERT(p_RowTemplate->HasField(GetColId()));
	if (p_RowTemplate->HasField(GetColId()))
		change.m_TemplateID = p_RowTemplate->GetField(GetColId()).GetValueStr();
	if (p_RowTemplate->HasField(m_ColRoleId))
		change.m_RoleID = p_RowTemplate->GetField(m_ColRoleId).GetValueStr();
	change.m_RowPrimaryKey.emplace();
	GetRowPK(p_RowTemplate, *change.m_RowPrimaryKey);
	return change;
}

GridDirectoryRoles::RoleChanges GridDirectoryRoles::GetChanges(const vector<GridBackendRow*>& p_Rows) const
{
	return getChangesImpl(p_Rows);
}


GridDirectoryRoles::RoleChanges GridDirectoryRoles::GetChanges(const set<GridBackendRow*>& p_Rows) const
{
	return getChangesImpl(p_Rows);
}

bool GridDirectoryRoles::HasModificationsPending(bool inSelectionOnly /*= false*/)
{
	if (inSelectionOnly)
		return HasModificationsPendingInParentOfSelected();
	return O365Grid::HasModificationsPending(false);
}

template<class Container>
GridDirectoryRoles::RoleChanges GridDirectoryRoles::getChangesImpl(const Container& p_Rows) const
{
	RoleChanges changes;

	std::set<GridBackendRow*> userRows;
	for (auto row : p_Rows)
	{
		if (!row->IsGroupRow())
			userRows.insert(row->GetTopAncestorOrThis());
	}

	for (auto userRow : userRows)
	{
		ASSERT(GridUtil::isBusinessType<BusinessDirectoryRoleTemplate>(userRow));
		if (userRow->IsModified())
		{
			const PooledString templateId(userRow->GetField(GetColId()).GetValueStr());
			auto it = std::find_if(changes.begin(), changes.end(), [templateId](const RoleChange& p_RoleChange) { return templateId == p_RoleChange.m_TemplateID; });
			ASSERT(changes.end() == it);
			changes.emplace_back();

			changes.back() = initChange(userRow);

			ASSERT(userRow->GetField(m_ColActivated).IsModified());
			if (userRow->GetField(m_ColActivated).GetIcon() == m_RoleActivated)
			{
				changes.back().m_Activate = true;
			}
			else
			{
				ASSERT(userRow->GetField(m_ColActivated).GetIcon() == m_RoleDeactivated);
				changes.back().m_Activate = false;
			}
		}

		auto childRows = GetChildRows(userRow);
		for (auto row : childRows)
		{
			ASSERT(GridUtil::IsBusinessUser(row));
			if (GridUtil::IsBusinessUser(row) && (row->IsCreated() || row->IsDeleted()))
			{
				auto parentRow = row->GetParentRow();
				ASSERT(nullptr != parentRow && GridUtil::isBusinessType<BusinessDirectoryRoleTemplate>(parentRow));
				if (nullptr != parentRow && GridUtil::isBusinessType<BusinessDirectoryRoleTemplate>(parentRow))
				{
					const PooledString templateId(parentRow->GetField(GetColId()).GetValueStr());
					auto it = std::find_if(changes.begin(), changes.end(), [row, templateId](const RoleChange& p_RoleChange) { return templateId == p_RoleChange.m_TemplateID; });
					// Force lambda return type to reference (otherwise it's by value).
					auto& change = (changes.end() == it) ? ([this, &changes, parentRow]() ->auto& {changes.emplace_back(); changes.back() = initChange(parentRow); return changes.back(); }())
						: *it;

					auto& userContainer = row->IsDeleted() ? change.m_UsersToRemove : change.m_UsersToAdd;
					ASSERT(row->HasField(GetColId()));
					if (row->HasField(GetColId()))
					{
						if (!userContainer.is_initialized())
							userContainer.emplace();

						row_pk_t pk;
						GetRowPK(row, pk);
						userContainer->emplace_back(row->GetField(GetColId()).GetValueStr(), pk);
					}
				}
			}
		}
	}

	return changes;
}

#if HAS_ACTIVATE_ROLE_COMMAND
void GridDirectoryRoles::OnActivateRole()
{
	ASSERT(IsFrameConnected());

	bool update = false;
	for (auto row : GetSelectedRows())
	{
		ASSERT(nullptr != row);
		if (nullptr != row && !row->IsGroupRow() && nullptr == row->GetParentRow())
		{
			ASSERT(GridUtil::isBusinessType<BusinessDirectoryRoleTemplate>(row));
			if (activateRole(row))
				update = true;
		}
	}

	if (update)
		UpdateMegaSharkOptimized(m_ColActivated->GetID());
}

void GridDirectoryRoles::OnUpdateActivateRole(CCmdUI* pCmdUI)
{
	bool enable = false;

	if (IsFrameConnected())
	{
		for (auto row : GetSelectedRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row && !row->IsGroupRow() && nullptr == row->GetParentRow() && row->GetField(m_ColActivated).GetIcon() == m_RoleDeactivated)
			{
				ASSERT(GridUtil::isBusinessType<BusinessDirectoryRoleTemplate>(row));
				enable = true;
				break;
			}
		}
	}

    pCmdUI->Enable(enable);
    setTextFromProfUISCommand(*pCmdUI);
}
#endif

void GridDirectoryRoles::OnAddMember()
{
	if (IsFrameConnected()
		&& IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_TENANT_ROLES_EDIT)
		&& showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT))
	{
		// < Role Template ID, <Role Name, User Ids>>
		std::map<PooledString, std::pair<PooledString, std::set<PooledString>>> parentsForSelection;
		set<GridBackendRow*> roleRows;
		for (auto row : GetSelectedRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row && !row->IsGroupRow())
			{
				auto parentRow = row->GetParentRow();
				if (nullptr == parentRow)
					parentRow = row;

				ASSERT(nullptr != parentRow);
				if (nullptr != parentRow && roleRows.end() == roleRows.find(parentRow))
				{
					ASSERT(GridUtil::isBusinessType<BusinessDirectoryRoleTemplate>(parentRow));
					roleRows.insert(parentRow);

					ASSERT(parentRow->HasField(m_ColRoleTemplateId));
					ASSERT(parentRow->HasField(m_ColRoleDisplayName));
					parentsForSelection[parentRow->GetField(m_ColRoleTemplateId).GetValueStr()] = { parentRow->GetField(m_ColRoleDisplayName).GetValueStr(), GetIdsOfChildRowsThatAllMatch({parentRow}) };
				}
			}
		}

		ASSERT(!roleRows.empty());
		if (!roleRows.empty())
		{
			vector<BusinessUser> users;
			getUsers(users);
			DlgAddItemsToGroups dlg( // DONE
				GetSession(),
				parentsForSelection,
				DlgAddItemsToGroups::LOADTYPE::SELECT_USERS_FOR_ROLES,
				users,
				{},
				{},
				YtriaTranslate::Do(GridDirectoryRoles_OnAddMember_2, _YLOC("Assign %1 directory roles to users"), Str::getStringFromNumber(roleRows.size()).c_str()),
				YtriaTranslate::Do(GridDirectoryRoles_OnAddMember_3, _YLOC("These %1 directory roles will be assigned to the users you select below.\r\nTo see the complete list, use Load full directory."), Str::getStringFromNumber(roleRows.size()).c_str()),
				IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_TENANT_ROLES_EDIT_OUT_OF_SCOPE)
					? RoleDelegationUtil::RBAC_Privilege::RBAC_NONE
					: RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE,
				this);
			if (IDOK == dlg.DoModal())
			{
				GridBackendRow* rowToScrollTo = nullptr;
				bool update = false;

				// List of all users to add to selected roles.
				auto& newItemList = dlg.GetSelectedItems();

				for (auto roleRow : roleRows)
				{
					ASSERT(nullptr != roleRow);
					if (nullptr != roleRow)
					{
						ASSERT(roleRow->HasField(m_ColRoleDisplayName));

						if (!newItemList.empty() && activateRole(roleRow))
							update = true;

						vector<GridBackendRow*> addedRows;
						for (auto& item : newItemList)
						{
							// Recycle the row if already in.
							GridBackendRow* rowUser = GetChildRow(roleRow, item.GetID(), GetColId()->GetID());

							if (nullptr == rowUser)
							{
								ASSERT(roleRow->HasField(m_ColRoleDisplayName));
								if (roleRow->HasField(m_ColRoleDisplayName))
								{
									GridBackendField fieldId(item.GetID(), GetColId());
									rowUser = m_RowIndex.GetRow({ fieldId, roleRow->GetField(m_ColRoleDisplayName) }, true, false);

									update = true;

									ASSERT(nullptr != rowUser);
									if (nullptr != rowUser)
									{
										rowUser->SetParentRow(roleRow);

										rowUser->AddField(roleRow->GetField(m_ColRoleDisplayName));
										rowUser->AddField(roleRow->GetField(m_ColDescription));

										rowUser->AddField(YtriaTranslate::Do(GridDirectoryRoles_OnAddMember_1, _YLOC("Activated")).c_str(), m_ColActivated, m_RoleActivated);

										rowUser->AddField(item.GetID(), GetColId());
										rowUser->AddField(item.GetPrincipalName(), m_ColUserPrincipalName);
										rowUser->AddField(item.GetDisplayName(), m_ColUserDisplayName);
										rowUser->AddField(item.GetUserGroupType(), m_ColUserType);

										SetRowObjectType(rowUser, item.GetObjectType());

										// FIXME: Do we want to update this now or only after apply?
										const auto user = BusinessUser(GetGraphCache().GetUserInCache(item.GetID(), false));
										AddRoleDelegationFlag(rowUser, user);

										addedRows.push_back(rowUser);
									}
								}
							}
							else
							{
								if (rowUser->IsDeleted())
								{
									row_pk_t rowPk;
									GetRowPK(rowUser, rowPk);
									update = GetModifications().Revert(rowPk, false);
								}
							}
						}

						if (nullptr == rowToScrollTo && addedRows.size() > 0)
						{
							OnUnSelectAll();
							rowToScrollTo = addedRows[0];
						}

						for (auto row : addedRows)
						{
							row_pk_t rowPk;
							GetRowPK(row, rowPk);
							GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPk));
							row->SetSelected(true);
						}
					}
				}

				if (update)
					UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

				if (nullptr != rowToScrollTo)
					ScrollToRow(rowToScrollTo);
			}
		}
	}
}

void GridDirectoryRoles::OnUpdateAddMember(CCmdUI* pCmdUI)
{
	bool enable = false;

	if (IsFrameConnected() && IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_TENANT_ROLES_EDIT))
	{
		enable = HasOneNonGroupRowSelectedAtLeast();
#if 0 // Full check, useless now.
		for (auto row : GetSelectedRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row && !row->IsGroupRow())
			{
				auto parentRow = row->GetParentRow();
				if (nullptr == parentRow /*&& row->GetField(m_ColActivated).GetIcon() == m_RoleActivated*/)
				{
					ASSERT(GridUtil::isBusinessType<BusinessDirectoryRoleTemplate>(row));
					enable = true;
				}
				else
				{
					ASSERT(GridUtil::isBusinessType<BusinessDirectoryRoleTemplate>(parentRow));
					enable = true;
				}

				if (enable)
					break;
			}
		}
#endif
	}

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridDirectoryRoles::OnRemoveMember()
{
	bool update = false;

	if (IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_TENANT_ROLES_EDIT)
		&& showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT))
	{
		const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_TENANT_ROLES_EDIT_OUT_OF_SCOPE);
		vector<GridBackendRow*> rows;
		GetSelectedNonGroupRows(rows);
		for (auto row : rows) // Do not use GetSelectedRows() as some rows can be deleted in the loop below (revert)
		{
			ASSERT(nullptr != row);
			if (nullptr != row && nullptr != row->GetParentRow() && row->GetField(m_ColActivated).GetIcon() == m_RoleActivated)
			{
				ASSERT(GridUtil::IsBusinessUser(row));

				if (hasOutOfScopePrivilege || IsUserAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE))
				{
					row_pk_t rowPk;
					GetRowPK(row, rowPk);

					if (row->IsCreated())
					{
						if (GetModifications().Revert(rowPk, false))
							update = true;
					}
					else
					{
						GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, rowPk));
						update = true;
					}
				}
			}
		}
	}

	if (update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridDirectoryRoles::OnUpdateRemoveMember(CCmdUI* pCmdUI)
{
	bool enable = false;

	if (IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_TENANT_ROLES_EDIT))
	{
		const bool hasOutOfScopePrivilege = IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_TENANT_ROLES_EDIT_OUT_OF_SCOPE);
		for (auto row : GetSelectedRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row && !row->IsGroupRow() && nullptr != row->GetParentRow() && row->GetField(m_ColActivated).GetIcon() == m_RoleActivated)
			{
				ASSERT(GridUtil::IsBusinessUser(row));
				if (!row->IsDeleted()
					&& (hasOutOfScopePrivilege || IsUserAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE))
					)
				{
					enable = true;
					break;
				}
			}
		}
	}

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridDirectoryRoles::getUsers(vector<BusinessUser>& p_Users) const
{
	CWaitCursor _;
	for (auto& row : GetBackendRows())
	{
		if (!row->IsExplosionAdded() && GridUtil::IsBusinessUser(row))
		{
			BusinessUser bu;
			{
				const auto& field = row->GetField(GetColId());
				ASSERT(field.HasValue());
				if (field.HasValue())
					bu.SetID(field.GetValueStr());
			}

			if (p_Users.end() == std::find_if(p_Users.begin(), p_Users.end(), [&bu](const BusinessUser& p_Bu) { return p_Bu.GetID() == bu.GetID(); }))
			{
				p_Users.emplace_back();
				auto& user = p_Users.back();

				user.SetID(bu.GetID());
				user.SetRBACDelegationID(GetGraphCache().GetUserInCache(user.GetID(), false).GetRBACDelegationID());

				{
					const auto& field = row->GetField(m_ColUserDisplayName);
					if (field.HasValue())
						user.SetDisplayName(PooledString(field.GetValueStr()));
				}

				{
					const auto& field = row->GetField(m_ColUserPrincipalName);
					if (field.HasValue())
						user.SetUserPrincipalName(PooledString(field.GetValueStr()));
				}

				{
					const auto& field = row->GetField(m_ColUserType);
					if (field.HasValue())
						user.SetUserType(PooledString(field.GetValueStr()));
				}
			}
		}
	}
}

bool GridDirectoryRoles::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	if (p_Field.HasValue())
	{
		if (&p_Column == m_ColActivated)
		{
			if (p_Field.GetIcon() == m_RoleActivated)
				p_Value = _YTEXT("1");
			else if (p_Field.GetIcon() == m_RoleDeactivated)
				p_Value = _YTEXT("0");
			else
				ASSERT(false);
			return true;
		}
	}

	return O365Grid::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridDirectoryRoles::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (&p_Column == m_ColActivated)
	{
		ASSERT(!p_Value.empty());
		if (p_Value == _YTEXT("1"))
		{
			p_Row.AddField(YtriaTranslate::Do(GridDirectoryRoles_OnAddMember_1, _YLOC("Activated")).c_str(), m_ColActivated, m_RoleActivated);
		}
		else
		{
			ASSERT(p_Value == _YTEXT("0"));
			p_Row.AddField(YtriaTranslate::Do(GridDirectoryRoles_AddDirectoryRoleData_3, _YLOC("Deactivated")).c_str(), m_ColActivated, m_RoleDeactivated);
		}

		return true;
	}

	return O365Grid::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

#if HAS_DEACTIVATE_ROLE_COMMAND
void GridDirectoryRoles::OnDeactivateRole()
{
	ASSERT(IsFrameConnected());

	bool update = false;
	for (auto row : GetSelectedRows())
	{
		ASSERT(nullptr != row);
		if (nullptr != row && !row->IsGroupRow() && nullptr == row->GetParentRow() && !row->HasChildrenRows() && row->GetField(m_ColActivated).GetIcon() == m_RoleActivated)
		{
			ASSERT(GridUtil::isBusinessType<BusinessDirectoryRoleTemplate>(row));
			row->AddField(YtriaTranslate::Do(GridDirectoryRoles_OnDeactivateRole_1, _YLOC("Deactivated")).c_str(), m_ColActivated, m_RoleDeactivated);
			OnRowModified(row);
			update = true;
		}
	}

	if (update)
		UpdateMegaSharkOptimized(m_ColActivated->GetID());
}

void GridDirectoryRoles::OnUpdateDeactivateRole(CCmdUI* pCmdUI)
{
	bool enable = false;

	if (IsFrameConnected())
	{
		for (auto row : GetSelectedRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row && !row->IsGroupRow() && nullptr == row->GetParentRow() && !row->HasChildrenRows() && row->GetField(m_ColActivated).GetIcon() == m_RoleActivated)
			{
				ASSERT(GridUtil::isBusinessType<BusinessDirectoryRoleTemplate>(row));
				enable = true;
				break;
			}
		}
	}

	// FIXME: This doesn't work yet.
    //pCmdUI->Enable(enable);
	pCmdUI->Enable(false);
    setTextFromProfUISCommand(*pCmdUI);
}
#endif