#pragma once

#include "BusinessMessageRule.h"
#include "DlgFormsHTML.h"

class DlgBusinessMessageRuleEditHTML : public DlgFormsHTML
{
public:
	DlgBusinessMessageRuleEditHTML(vector<BusinessMessageRule>& p_BusinessMessageRules, DlgFormsHTML::Action p_Action, const wstring& p_ComponentType, CWnd* p_Parent);
	virtual ~DlgBusinessMessageRuleEditHTML();

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override final;

	virtual void generateJSONScriptData() override final;

private:
	enum class PredicateType
	{
		CONDITION = 0,
		EXCEPTION,
	};

	void addStringEditor(boost::YOpt<PooledString> BusinessMessageRule::*p_String, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags);
	void addBoolToggleEditor(boost::YOpt<bool> BusinessMessageRule::*p_Bool, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides = BoolEditor::CheckUncheckedLabelOverrides());

	void addStringEditor(boost::YOpt<PooledString> BusinessMessageRuleActions::*p_String, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags);
	void addComboEditor(boost::YOpt<PooledString> BusinessMessageRuleActions::*p_String, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues);
	void addBoolToggleEditor(boost::YOpt<bool> BusinessMessageRuleActions::*p_Bool, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides = BoolEditor::CheckUncheckedLabelOverrides());

	void addStringEditor(PredicateType p_PredicateType, boost::YOpt<PooledString> BusinessMessageRulePredicates::*p_String, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags);
	void addComboEditor(PredicateType p_PredicateType, boost::YOpt<PooledString> BusinessMessageRulePredicates::*p_String, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues);
	void addBoolToggleEditor(PredicateType p_PredicateType, boost::YOpt<bool> BusinessMessageRulePredicates::*p_Bool, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides = BoolEditor::CheckUncheckedLabelOverrides());
	void addMultiValueEditor(PredicateType p_PredicateType, boost::YOpt<vector<PooledString>> BusinessMessageRulePredicates::*p_StringVector, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags);

	bool hasProperty(const wstring& p_PropertyName) const;

private:
	const wstring m_ComponentType;
	vector<BusinessMessageRule>& m_BusinessMessageRules;

	using RuleStringProps = std::map<wstring, boost::YOpt<PooledString> BusinessMessageRule::*>;
	RuleStringProps m_RuleStringProps;
	using RuleBoolProps = std::map<wstring, boost::YOpt<bool> BusinessMessageRule::*>;
	RuleBoolProps m_RuleBoolProps;

	using ActionStringProps = std::map<wstring, boost::YOpt<PooledString> BusinessMessageRuleActions::*>;
	ActionStringProps m_ActionStringProps;
	using ActionBoolProps = std::map<wstring, boost::YOpt<bool> BusinessMessageRuleActions::*>;
	ActionBoolProps m_ActionBoolProps;

	using PredicateStringProps = std::map<wstring, std::pair<PredicateType, boost::YOpt<PooledString> BusinessMessageRulePredicates::*>>;
	PredicateStringProps m_PredicateStringProps;
	using PredicateBoolProps = std::map<wstring, std::pair<PredicateType, boost::YOpt<bool> BusinessMessageRulePredicates::*>>;
	PredicateBoolProps m_PredicateBoolProps;
	using PredicateStringVectorProps = std::map<wstring, std::pair<PredicateType, boost::YOpt<vector<PooledString>> BusinessMessageRulePredicates::*>>;
	PredicateStringVectorProps m_PredicateStringVectorProps;

	static const wstring g_ListSeparator;
};