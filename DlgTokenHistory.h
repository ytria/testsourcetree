#pragma once

#include "ResizableDialog.h"

class LicenseManager;
class DlgTokenHistory : public ResizableDialog
{
public:
	DlgTokenHistory(LicenseManager* p_licenseManager, CWnd* p_Parent = nullptr);
	~DlgTokenHistory();

protected:
	void DoDataExchange(CDataExchange* pDX) override;
	BOOL OnInitDialogSpecificResizable() override;

	DECLARE_MESSAGE_MAP()

private:
	void fillGrid();

	CExtLabel	m_LabelGridHeader;
	CExtLabel	m_LabelGridArea;
	CExtButton	m_BtnOk;

	LicenseManager*	m_LicenseManager;

	class TokenHistoryGrid;
	std::unique_ptr<TokenHistoryGrid> m_Grid;
};
