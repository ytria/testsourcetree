#pragma once

#include "AttachmentsInfo.h"
#include "Command.h"
#include "ModuleBase.h"
#include "ResizableDialog.h"
#include "resource.h"
#include "StringHtmlView.h"

class DlgContentViewer	: public ResizableDialog
						, public DownloadInlineAttachmentsCallback
{
public:
	class NavigationCallback
	{
    public:
        virtual ~NavigationCallback() = default;
		virtual void Previous() = 0;
		virtual void Next() = 0;

		friend class DlgContentViewer;
	};

	class AttachmentIDGetter
	{
	public:
		virtual ~AttachmentIDGetter() = default;
		virtual AttachmentsInfo::IDs GetIds(GridBackendRow* p_Row) = 0;
	};

	DlgContentViewer(O365Grid* p_Grid, NavigationCallback* p_NavigationCallback, AttachmentIDGetter* p_AttachmentIDGetter, const wstring& p_Title, CWnd* p_Parent, bool m_AddBoilerplateHtml = false);

	enum
	{
		IDD = IDD_DLGHTMLVIEWER,
	};

	struct HeaderConfig
	{
		std::vector<std::pair<wstring, wstring>> m_FirstLine;
		std::vector<std::pair<wstring, wstring>> m_SecondLine;
		wstring m_Subject;
	};
    void SetLoadImagesCommandTarget(Command::ModuleTarget p_Target);
    void SetLoadImagesCommandTask(Command::ModuleTask p_Task);
	void SetContent(const HeaderConfig& p_HeaderConfig, const wstring& p_Content, bool p_LoadAsHtml, bool p_ShowLoadImagesButton);
    StringHtmlView* GetHtmlView() const;

public: // DownloadInlineAttachmentsCallback
	virtual void processInlineAttachments(const std::map<AttachmentsInfo::IDs, std::map<wstring, wstring>>& p_IdsToCidsToPaths) override;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialogSpecificResizable() override;
	virtual void OnCancel() override;
	virtual void OnOK() override;
	afx_msg void OnPrevious();
	afx_msg void OnNext();
    afx_msg void OnLoadImages();
	afx_msg void OnViewSource();

	DECLARE_MESSAGE_MAP();

private:
	void updateHeader();

	class MyTextEdit : public CExtEdit
	{
	public:
		using CExtEdit::CExtEdit;

	protected:
		virtual COLORREF OnQueryBackColor() const;
	};

	bool m_LoadAsHtml;
	bool m_ShowLoadImagesButton;
	wstring m_Content;
	wstring m_Title;
	HeaderConfig m_HeaderConfig;
	NavigationCallback* m_NavigationCallback;
	AttachmentIDGetter* m_AttachmentIDGetter;

	std::unique_ptr<StringHtmlView> m_HtmlView;
	MyTextEdit m_TextView;
	CXTPMarkupStatic m_Header;
	CXTPRibbonBackstageButton m_PreviousButton;
	CXTPRibbonBackstageButton m_NextButton;
    CXTPRibbonBackstageButton m_LoadImagesButton;
	CXTPRibbonBackstageButton m_ViewSourceButton;
	O365Grid* m_Grid;

    Command::ModuleTarget m_ShowImagesCmdTarget;
    Command::ModuleTask m_ShowImagesCmdTask;

	bool m_AddBoilerplateHtml;

	static TCHAR s_savedPositionKey[];
};
