#include "ExistsRowHandler.h"

ExistsRowHandler::ExistsRowHandler()
	:m_Exists(false)
{
}

void ExistsRowHandler::HandleRow(sqlite3_stmt* p_Stmt)
{
	m_Exists = true;
}

bool ExistsRowHandler::Exists() const
{
	return m_Exists;
}
