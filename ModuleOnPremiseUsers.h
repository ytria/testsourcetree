#pragma once

#include "CommandInfo.h"
#include "ModuleBase.h"

class FrameOnPremiseUsers;
struct RefreshSpecificData;

class ModuleOnPremiseUsers : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	virtual void executeImpl(const Command& p_Command);
	void showOnPremiseUsers(const Command& p_Command);
	void refresh(const Command& p_Command);
};

