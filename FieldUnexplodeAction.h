#pragma once

#include "FieldActionBase.h"

class FieldUnexplodeAction : public FieldActionBase
{
public:
    using FieldActionBase::FieldActionBase;

    virtual void Restore() override;
};

