#include "AutomaticRepliesSettingSerializer.h"

#include "DateTimeTimeZoneSerializer.h"
#include "JsonSerializeUtil.h"
#include "NullableSerializer.h"

AutomaticRepliesSettingSerializer::AutomaticRepliesSettingSerializer(const AutomaticRepliesSetting& p_AutoRepSet)
    : m_AutoRepSet(p_AutoRepSet)
{
}

void AutomaticRepliesSettingSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("externalAudience"), m_AutoRepSet.ExternalAudience, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("externalReplyMessage"), m_AutoRepSet.ExternalReplyMessage, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("internalReplyMessage"), m_AutoRepSet.InternalReplyMessage, obj);
    JsonSerializeUtil::SerializeAny(_YTEXT("scheduledEndDateTime"), NullableSerializer<DateTimeTimeZone, DateTimeTimeZoneSerializer>(m_AutoRepSet.ScheduledEndDateTime), obj);
    JsonSerializeUtil::SerializeAny(_YTEXT("scheduledStartDateTime"), NullableSerializer<DateTimeTimeZone, DateTimeTimeZoneSerializer>(m_AutoRepSet.ScheduledStartDateTime), obj);
    JsonSerializeUtil::SerializeString(_YTEXT("status"), m_AutoRepSet.Status, obj);
}
