#pragma once

#include "MsGraphPaginator.h"
#include "IMsGraphPageRequester.h"

class MSGraphSession;
class IValueListDeserializer;
class YtriaTaskData;

namespace Util
{
	MsGraphPaginator CreateDefaultGraphPaginator(const web::uri& p_Uri, const shared_ptr<IValueListDeserializer>& p_Deserializer, const std::shared_ptr<IPageRequestLogger>& p_Logger, const std::shared_ptr<IHttpRequestLogger>& p_HttpLogger, HWND p_Hwnd);
	MsGraphPaginator CreateDefaultGraphPaginator(const web::uri& p_Uri, const shared_ptr<IValueListDeserializer>& p_Deserializer, size_t p_ElemsPerPage, const std::shared_ptr<IPageRequestLogger>& p_Logger, const std::shared_ptr<IHttpRequestLogger>& p_HttpLogger, HWND p_Hwnd);
	std::shared_ptr<IMsGraphPageRequester> CreateDefaultGraphPageRequester(const shared_ptr<IValueListDeserializer>& p_Deserializer, const std::shared_ptr<IHttpRequestLogger>& p_HttpLogger, HWND p_Hwnd);
}