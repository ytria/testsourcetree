#pragma once

#include "Sapio365Session.h"
#include "VendorLicense.h"

class LicenseManager;
class CosmosManager
{
public:

	struct CosmosConnector
	{
		wstring m_URL;// COSMOS account
		wstring m_MasterKey;// COSMOS account
		wstring m_KeyValidation;// Ytria invoice number for storage in CRM / license agility
		bool	m_CosmosSingleContainer;
	};

	CosmosManager();
	
	CosmosConnector	GetConnectorFromLicense(std::shared_ptr<Sapio365Session>& p_Session) const;
	bool SendCnxToCRM(const CosmosConnector& p_cnx, std::shared_ptr<Sapio365Session>& p_Session, const bool p_Clear) const;// stores cnx into NLP license agility / to server - refreshes license from server

	bool IsValid(const CosmosConnector& p_cnx) const;
	bool IsValid(const wstring& p_cnxCredentialPart) const;

private:
	LicenseManager*	getLicenseManager() const;

	std::string		getFirstKrypt(std::shared_ptr<Sapio365Session>& p_Session) const;
	std::string		getSecondKrypt(std::shared_ptr<Sapio365Session>& p_Session) const;

	static const wstring g_Separator;
	static const wstring g_EMPTY;
};