#include "ManagerSQLandCloud.h"

ManagerSQLandCloud::ManagerSQLandCloud() :
	m_ErrorCode(CFG_NOERROR)
{
}

bool ManagerSQLandCloud::IsReadyToRun() const
{
	return CFG_NOERROR == m_ErrorCode;
}

void ManagerSQLandCloud::SetError(const ErrorCode p_Error)
{
	m_ErrorCode = p_Error;
}

ManagerSQLandCloud::ErrorCode ManagerSQLandCloud::GetError() const
{
	return m_ErrorCode;
}

wstring ManagerSQLandCloud::GetLastError() const
{
	wstring lastError = YtriaTranslate::Do(FileUtil_GetExceptionCause_1, _YLOC("No error")).c_str();

	switch (m_ErrorCode)
	{
		case CFG_NOERROR:
			// Why did you call GetLastError() again?
			ASSERT(false);
			break;
		case SQLCFG_ERROR:
			lastError = YtriaTranslate::DoError(ManagerSQLandCloud_GetLastError_2, _YLOC("Data repository configuration error"),_YR("Y2424")).c_str();
			break;
		case COSMOSCFG_ERROR:
			lastError = YtriaTranslate::DoError(ManagerSQLandCloud_GetLastError_4, _YLOC("Cosmos cloud configuration error"),_YR("Y2425")).c_str();
			break;
		case VERSION_ERROR:
			lastError = YtriaTranslate::DoError(ManagerSQLandCloud_GetLastError_5, _YLOC("This sapio365 version is too old compared to the data on the Cosmos cloud"),_YR("Y2426")).c_str();
			break;
		default:
			ASSERT(false);
			break;
	}

	return lastError;
}