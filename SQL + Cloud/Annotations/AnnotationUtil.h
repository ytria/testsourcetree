#pragma once

#include "GridAnnotation.h"
#include "Modification.h"
#include "YTimeDate.h"

namespace AnnotationUtil
{
	using CosmosUpdateData = map<SQLiteUtil::Table, map<wstring, SQLiteUtil::SQLRecord*>>;
};

class AnnotationModification : public Modification
{
public:
	AnnotationModification(const AnnotationRecord& p_Record);
	virtual ~AnnotationModification();

	virtual vector<ModificationLog> GetModificationLogs() const override;

	const AnnotationRecord& GetRecord() const;

private:
	AnnotationRecord m_Record;
};