#include "AnnotationManager.h"

#include "FileUtil.h"
#include "SQLCosmosSyncManager.h"
#include "YCallbackMessage.h"
#include "YCodeJockMessageBox.h"

AnnotationManager::AnnotationManager() : ManagerSQLandCloud()
{
	if (!m_AnnotationSQLite.Configure())
		SetError(SQLCFG_ERROR);
}

// static 
AnnotationManager& AnnotationManager::GetInstance()
{
	static AnnotationManager g_Instance;
	return g_Instance;
}

void AnnotationManager::InitAfterSessionOrLicenseWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress)
{
	CWaitCursor _;

	ASSERT(p_Session);
	if (IsReadyToRun() && p_Session)
	{
		if (m_AnnotationCosmos.Configure(m_AnnotationSQLite, p_Session))
		{
			CosmosUtil::SQLupdateData cosmosDocuments;
			p_Progress.SetCounterTotal2(0);
			p_Progress.SetText2(_T("Syncing Comments"));
			if (m_AnnotationCosmos.SelectAll(cosmosDocuments, p_Session, true))// fetch from Cosmos
			{
				updateSQLiteFromCloud(p_Session, cosmosDocuments, p_Progress);
				pushSQLiteToCloud(p_Session, cosmosDocuments, p_Progress);
			}
		}

		if (m_AnnotationCosmos.GetLastError() != CosmosSQLiteBridge::COSMOS_NOERROR &&
			m_AnnotationCosmos.GetLastError() != CosmosSQLiteBridge::SESSION_NOTREADYFORCOSMOS) // allowed to run on local machine
			SetError(COSMOSCFG_ERROR);
	}
}

bool AnnotationManager::updateSQLiteFromCloud(std::shared_ptr<const Sapio365Session> p_Session, const CosmosUtil::SQLupdateData& p_CosmosDocuments, DlgDoubleProgressCommon& p_Progress)
{
	return IsReadyToRun() && m_AnnotationSQLite.UpdateFromCosmos(p_CosmosDocuments, p_Session, p_Progress);
}

bool AnnotationManager::pushSQLiteToCloud(std::shared_ptr<const Sapio365Session> p_Session, const CosmosUtil::SQLupdateData& p_CosmosDocuments, DlgDoubleProgressCommon& p_Progress)
{
	bool rvUpdate = false;

	if (IsReadyToRun())
	{
		// fetch from SQLite - push to COSMOS
		AnnotationUtil::CosmosUpdateData SQLdata;
		if (m_AnnotationSQLite.SelectAllForUpdateToCloud(SQLdata, p_Session))
			rvUpdate = m_AnnotationCosmos.UpdateFromSQLite(SQLdata, p_CosmosDocuments, p_Session, p_Progress);
		else
		{
			TraceIntoFile::trace(_YDUMP("AnnotationManager"), _YDUMP("pushSQLiteToCloud"), _YTEXTFORMAT(L"Unable to load Comments data to push to %s (tenant %s)", p_Session->GetCosmosDbSqlUri().c_str(), p_Session->GetTenantName().c_str()));
			YCallbackMessage::DoSend([]()
			{
				YCodeJockMessageBox dlg(AfxGetApp()->m_pMainWnd,
										DlgMessageBox::eIcon_Error,
										YtriaTranslate::Do(Application_error_2, _YLOC("Error")).c_str(),
										YtriaTranslate::DoError(AnnotationManager_pushSQLiteToCloud_3, _YLOC("Unable to update Comments to cloud"),_YR("Y2429")).c_str(),
										_YTEXT(""),
										{ { IDOK, YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str() } });
				dlg.DoModal();
			});
		}

		m_AnnotationSQLite.CompleteUpdateToCloud(p_Session);
	}

	return rvUpdate;
}

bool AnnotationManager::Write(GridAnnotation& p_Annotation, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	p_Annotation.m_WriteError_VOL.clear();

	ASSERT(p_Session && p_Annotation.IsValid());
	if (p_Session && p_Annotation.IsValid())
	{
		if (p_Annotation.m_UpdateByID.empty()/*new annotation*/
			|| p_Session->IsAdminAnnotations()
			|| (p_Session->IsUltraAdmin() ? Sapio365Session::GetUltraAdminID() : p_Session->GetConnectedUserID()) == p_Annotation.m_UpdateByID)
		{
			auto annotationRecord = m_AnnotationSQLite.Write(p_Annotation, p_Session);
			rvSuccess = annotationRecord.IsLastWriteSuccess();
			if (/*!m_AnnotationSQLite.IsTransactionStarted() && */rvSuccess)
			{
				p_Annotation = annotationRecord.m_Annotation;
				if (!p_Annotation.IsLocal() && p_Session->IsReadyForCosmos())
				{
					m_AnnotationCosmos.WriteAsync<AnnotationRecord>(&annotationRecord, p_Session);
					rvSuccess = true;
				}
			}
			else
			{
				YCallbackMessage::DoSend([p_Annotation]()
				{
					YCodeJockMessageBox dlg(AfxGetApp()->m_pMainWnd,
											DlgMessageBox::eIcon_Error,
											YtriaTranslate::Do(Application_error_2, _YLOC("Error")).c_str(),
											YtriaTranslate::Do(AnnotationManager_Write_8, _YLOC("A problem occurred while trying to store the comment for column '%1', row '%2'"), p_Annotation.m_ColumnTitle.c_str(), p_Annotation.m_RowPK.c_str()).c_str(),
											_YTEXT(":-("),
											{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
					dlg.DoModal();
				});
			}
		}
		else
		{
			ASSERT(p_Annotation.m_ID);
			wstring action	= p_Annotation.IsRemoved() ? YtriaTranslate::Do(ColumnSelectionGrid_OnCustomPopupMenu_5, _YLOC("remove")).c_str() : YtriaTranslate::Do(DlgBusinessEditHTML_getDialogTitle_2, _YLOC("edit")).c_str();
			wstring annID	= p_Annotation.m_ID ? Str::getStringFromNumber<int64_t>(p_Annotation.m_ID.get()) : YtriaTranslate::DoError(AnnotationManager_Write_7, _YLOC("Not found!"),_YR("Y2428")).c_str();
			p_Annotation.m_WriteError_VOL = YtriaTranslate::Do(AnnotationManager_Write_9, _YLOC("You do not have permission to %1 comment '%2'\nIt belongs to %3"), action.c_str(), p_Annotation.m_Text.c_str(), p_Annotation.m_UpdateByName.c_str());
			TraceIntoFile::trace(_YTEXT("AnnotationManager"), _YTEXT("Write"), p_Annotation.m_WriteError_VOL);
			LoggerService::User(p_Annotation.m_WriteError_VOL, nullptr);
		}
	}
	else
	{
		YCallbackMessage::DoSend([p_Annotation]()
		{
			YCodeJockMessageBox dlg(AfxGetApp()->m_pMainWnd,
									DlgMessageBox::eIcon_Error,
									YtriaTranslate::Do(Application_error_2, _YLOC("Error")).c_str(),
									YtriaTranslate::Do(AnnotationManager_Write_10, _YLOC("A problem occurred while trying to store the comment, status is not valid for column '%1', row '%2'"), p_Annotation.m_ColumnTitle.c_str(), p_Annotation.m_RowPK.c_str()).c_str(),
									_YTEXT(":-("),
									{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
			dlg.DoModal();
		});
	}

	return rvSuccess;
}

bool AnnotationManager::LoadAnnotations(const wstring& p_Module, const wstring& p_RowPK, const wstring& p_ColumnID, std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations)
{
	return m_AnnotationSQLite.LoadAnnotations(false, p_Module, p_RowPK, {p_ColumnID}, p_Session, p_Annotations);
}

bool AnnotationManager::LoadAnnotations(const wstring& p_Module, const wstring& p_RowPK, const set<wstring>& p_ColumnIDs, std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations)
{
	return m_AnnotationSQLite.LoadAnnotations(false, p_Module, p_RowPK, p_ColumnIDs, p_Session, p_Annotations);
}

GridAnnotation AnnotationManager::Read(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_AnnotationSQLite.Read(p_ID, p_Session).m_Annotation;
}

bool AnnotationManager::UpdateFromCloudAndLoadAnnotations(const wstring& p_Module, std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations, CWnd* p_Parent)
{
	bool rv = false;

	if (p_Session)
	{
		const auto& table = m_AnnotationSQLite.GetTable();

		SQLiteUtil::WhereClause wcModule;
		wcModule.m_Column	= table.m_ColumnModule;
		wcModule.m_Values.push_back(p_Module);
		wcModule.m_Operator = Operator::Equal;

		SQLiteUtil::WhereClause wcTenant;
		wcTenant.m_Column	= table.m_ColumnTenant;
		wcTenant.m_Values.push_back(m_AnnotationSQLite.GetTenantName(p_Session));
		wcTenant.m_Operator = Operator::Equal;

		LinearMap<SQLiteUtil::Column, bool> orderBy;
		orderBy.push_back(table.m_ColumnColumnUID, true);
		//	orderBy.push_back(table.m_ColumnRowPK, true);
		// 	orderBy.push_back(table.m_ColumnColumnUID, true); multiple order-by not suported by Cosmos DB ("The order by query does not have a corresponding composite index that it can be served from.")
		// 	orderBy.push_back(table.m_ColumnDate, false);

		CosmosUtil::SQLupdateData cosmosDocuments;
		if (m_AnnotationCosmos.SelectAll(cosmosDocuments, p_Session, table, { wcTenant, wcModule }, orderBy, false, true))
		{
			DlgDoubleProgressCommon::RunModal(_YFORMAT(L"Syncing Comments for %s", p_Module.c_str()), p_Parent, [this, p_Session, &cosmosDocuments](auto prog)
				{
					updateSQLiteFromCloud(p_Session, cosmosDocuments, *prog);
				});
		}

		rv = m_AnnotationSQLite.LoadAnnotations(p_Module, p_Session, p_Annotations);
	}

	return rv;
}

bool AnnotationManager::UpdateFromCloudAndLoadAnnotationsAll(std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations, CWnd* p_Parent)
{
	const auto& table = m_AnnotationSQLite.GetTable();

	LinearMap<SQLiteUtil::Column, bool> orderBy;
//	orderBy.push_back(table.m_ColumnTenant, true);
	orderBy.push_back(table.m_ColumnColumnUID, true);
//	orderBy.push_back(table.m_ColumnRowPK, true);
// 	orderBy.push_back(table.m_ColumnColumnUID, true); multiple order-by not suported by Cosmos DB ("The order by query does not have a corresponding composite index that it can be served from.")
// 	orderBy.push_back(table.m_ColumnDate, false);

	CosmosUtil::SQLupdateData cosmosDocuments;
	if (m_AnnotationCosmos.SelectAll(cosmosDocuments, p_Session, table, {}, orderBy, true, true))
	{
		DlgDoubleProgressCommon::RunModal(_T("Syncing all Comments"), p_Parent, [this, p_Session, &cosmosDocuments](auto prog)
			{
				updateSQLiteFromCloud(p_Session, cosmosDocuments, *prog);
			});
	}
	return m_AnnotationSQLite.LoadAnnotationsAll(p_Session, p_Annotations);
}

void AnnotationManager::StartTransaction()
{
	m_AnnotationSQLite.StartTransaction();
}

void AnnotationManager::EndTransaction()
{
	m_AnnotationSQLite.EndTransaction();
}

bool AnnotationManager::WriteColumnSettings(const wstring& p_Module, const wstring& p_UID, const wstring& p_Settings, std::shared_ptr<const Sapio365Session> p_Session)
{
	auto SQLrecord = m_AnnotationSQLite.WriteColumnSettings(p_Module, p_UID, p_Settings, p_Session);
	if (p_Session->IsReadyForCosmos())
		m_AnnotationCosmos.WriteAsync<AnnotationColumnSettingsRecord>(&SQLrecord, p_Session);
	return SQLrecord.GetID() != 0;
}

wstring	AnnotationManager::GetColumnSettings(const wstring& p_Module, const wstring& p_UID, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_AnnotationSQLite.GetColumnSettings(p_Module, p_UID, p_Session);
}