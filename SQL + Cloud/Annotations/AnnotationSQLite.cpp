#include "AnnotationSQLite.h"

#include "ActivityLogger.h"
#include "SQLCosmosSyncManager.h"

AnnotationSQLite::AnnotationSQLite()
	: O365SQLiteEngine(_YTEXTFORMAT(L"_fgc%s_.ytr"
						, Str::getStringFromNumber<uint32_t>(g_CurrentVersion).c_str())
// 						, "@dump4ytria+666"
// 						, "c62f005d-9fa6-46be-a579-bc5b2fedfb47"
	)
{
	// Update this assert when version change.
	// For old reasons, g_CurrentVersion must at least be incremented each time SQLiteEngine::GetCurrentVersion() is.
	static_assert(g_CurrentVersion == 6 && SQLiteEngine::GetCurrentVersion() == 6, "Should you increment version or test?");
}

vector<SQLiteUtil::Table> AnnotationSQLite::GetTables() const
{
	return { m_Annotations, m_ColumnSettings };
}

const TableAnnotation& AnnotationSQLite::GetTable() const
{
	return m_Annotations;
}

wstring AnnotationSQLite::GetTenantName(const std::shared_ptr<const Sapio365Session>& p_Session) const
{
	ASSERT(p_Session);
	return p_Session ? p_Session->GetTenantName(true) : _YTEXT("");
}

bool AnnotationSQLite::LoadAnnotations(const bool p_AllTenantsAndModules, const wstring& p_Module, const wstring& p_RowPK, const set<wstring>& p_ColumnIDs, std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations)
{
	p_Annotations.clear();

	ASSERT(p_Session);
	if (p_Session)
	{
		LinearMap<wstring, bool> orderByDateDesc;
		orderByDateDesc.push_back(g_ColumnNameDate, false);

		map<wstring, set<wstring>>	WhereIN;
		map<wstring, wstring>		Where;
		if (!p_AllTenantsAndModules)
		{
			Where[g_ColumnNameTenant]	= GetTenantName(p_Session);
			Where[g_ColumnNameGridName] = p_Module;
		}
		if (!p_RowPK.empty())
			Where[g_ColumnNameGridRowPK] = p_RowPK;
		if (p_ColumnIDs.size() == 1)
			Where[g_ColumnNameColumnID] = *p_ColumnIDs.begin();
		else if (!p_ColumnIDs.empty())
			WhereIN[g_ColumnNameColumnID] = p_ColumnIDs;

		SQLiteUtil::QueryResultDataCollection SQLdata;
		if (SelectAll(O365Krypter(p_Session), m_Annotations, Where, WhereIN, {{ g_ColumnNameStatus, Str::getStringFromNumber<uint32_t>(AnnotationStatus::STATUS_REMOVED) }}, orderByDateDesc, SQLdata))
		{
			size_t k = 0;
			for (const auto& sqlResult : SQLdata)
			{
				AnnotationRecord SQLann(m_Annotations);
				if (SQLann.FromValuesToObject(sqlResult))
				{
					SQLann.m_Annotation.ReadPostProcess(SQLann);
					SQLann.m_Annotation.SetReadOnly(!(p_Session->IsAdminAnnotations() || (p_Session->IsUltraAdmin() ? Sapio365Session::GetUltraAdminID() : p_Session->GetConnectedUserID()) == SQLann.m_Annotation.m_UpdateByID));					
					if (!(SQLann.m_Annotation.m_RowPK.empty() || SQLann.m_Annotation.m_ColumnID.empty()))
					{
						SQLann.m_Annotation.m_IsTop_VOL = k == 0;
						k++;
						p_Annotations.push_back(SQLann.m_Annotation);
					}
				}
			}

			return true;
		}
	}

	return false;
}

bool AnnotationSQLite::LoadAnnotations(const wstring& p_Module, std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations)
{
	return LoadAnnotations(false, p_Module, {}, {}, p_Session, p_Annotations);
}

bool AnnotationSQLite::LoadAnnotationsAll(std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations)
{
	return LoadAnnotations(true, _YTEXT(""), {}, {}, p_Session, p_Annotations);
}

AnnotationRecord AnnotationSQLite::Read(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	AnnotationRecord annotation(m_Annotations);
	if (Select(O365Krypter(p_Session), &annotation, p_ID, g_ColumnNameStatus, AnnotationUtil::STATUS_REMOVED))
		annotation.m_Annotation.ReadPostProcess(annotation);// sad but true
	return annotation;
}

AnnotationRecord AnnotationSQLite::Write(const GridAnnotation& p_Annotation, std::shared_ptr<const Sapio365Session> p_Session)
{
	AnnotationRecord annotation = p_Annotation.HasID() ? Read(*p_Annotation.m_ID, p_Session) : AnnotationRecord(m_Annotations);

	annotation.m_Annotation = p_Annotation;
	annotation.SetID(p_Annotation.m_ID);// sad but true
	WriteAnnotationRecord(&annotation, p_Session);
	annotation.m_Annotation.m_ID = annotation.GetID();// sad but true again

	return annotation;
}

bool AnnotationSQLite::WriteAnnotationRecord(AnnotationRecord* p_Arecord, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	ASSERT(nullptr != p_Arecord);
	if (nullptr != p_Arecord)
	{
		ASSERT(!p_Arecord->m_Annotation.m_RowPK.empty());
		ASSERT(!p_Arecord->m_Annotation.m_ColumnID.empty());
		if (!(p_Arecord->m_Annotation.m_RowPK.empty() || p_Arecord->m_Annotation.m_ColumnID.empty()))
		{
			if (p_Arecord->IsForCloudStorageOnly() || prepareUpdateInfo(p_Arecord->m_Annotation, p_Session))
			{
				rvSuccess = WriteRecord(O365Krypter(p_Session), p_Arecord);
				if (!p_Arecord->IsForCloudStorageOnly() && rvSuccess /*&& !p_Arecord->m_Annotation.IsLocal()*/)
					ActivityLogger::GetInstance().LogAnnotationModifications(AnnotationModification(*p_Arecord), p_Session);
			}
		}
		else
		{
			auto err = _YFORMATERROR(L"Invalid row pk [%s] or column UID [%s]", p_Arecord->m_Annotation.m_RowPK.c_str(), p_Arecord->m_Annotation.m_ColumnID.c_str());
			TraceIntoFile::trace(_YTEXT("AnnotationSQLite"), _YTEXT("WriteAnnotationRecord"), err);
			p_Arecord->ResetID();
			p_Arecord->m_Annotation.m_WriteError_VOL = err;
		}
	}

	return rvSuccess;
}

bool AnnotationSQLite::prepareUpdateInfo(GridAnnotation& p_Annotation, std::shared_ptr<const Sapio365Session> p_Session) const
{
	bool rvSuccess = false;

	ASSERT(nullptr != p_Session);
	if (nullptr != p_Session)
	{
		const auto tenantName = GetTenantName(p_Session);
		bool tenantMatch = true;
		if (p_Annotation.m_TenantName.empty())
			p_Annotation.m_TenantName = tenantName;
		else if (tenantName != p_Annotation.m_TenantName)
		{
			ASSERT(false);
			TraceIntoFile::trace(_YDUMP("AnnotationSQLlite"), _YDUMP("prepareUpdateInfo"), _YTEXTFORMAT(L"Trying to store data from tenant: %s - while active tenant is: %s", p_Annotation.m_TenantName.c_str(), tenantName.c_str()));
			tenantMatch = false;
		}

		if (tenantMatch)
		{
			// Sapio365SessionSavedInfo might do the trick too
			const auto& user	= p_Session->GetGraphCache().GetCachedConnectedUser();
			const auto isUA		= p_Session->IsUltraAdmin();
			ASSERT(isUA || user.is_initialized());

			ASSERT(!p_Annotation.m_UpdDate.IsEmpty());// must be set in advance: in case of bulk annotation edit, we need to make sure the date/time is exactly the same for all to enable exact history edition (on bulks, two annotations match if same date, user ID and text)
			if (p_Annotation.m_UpdDate.IsEmpty())
			{
				TraceIntoFile::trace(_YDUMP("AnnotationSQLlite"), _YDUMP("prepareUpdateInfo"), _YTEXTFORMAT(L"Comment set for update without a date/time: %s", p_Annotation.m_Text.c_str()));
				p_Annotation.m_UpdDate = YTimeDate::GetCurrentTimeDate();
			}

			p_Annotation.m_UpdateByID				= isUA ? Sapio365Session::GetUltraAdminID() : user.is_initialized() ? user->GetID() : _YTEXT("N/A");
			p_Annotation.m_UpdateByName				= isUA ? YtriaTranslate::Do(GridSessionsBackstage_BuildView_3, _YLOC("Ultra Admin")).c_str() : user.is_initialized() && user->GetDisplayName().is_initialized() ? user->GetDisplayName().get() : _YTEXT("N/A");
			p_Annotation.m_UpdateByPrincipalName	= isUA ? YtriaTranslate::Do(GridSessionsBackstage_BuildView_3, _YLOC("Ultra Admin")).c_str() : user.is_initialized() && user->GetUserPrincipalName().is_initialized() ? user->GetUserPrincipalName().get() : _YTEXT("N/A");

			rvSuccess = true;
		}
	}

	return rvSuccess;
}

bool AnnotationSQLite::SelectAllForUpdateToCloud(CosmosUpdateData& p_AllData, std::shared_ptr<const Sapio365Session> p_Session)
{
	SelectAll<AnnotationRecord>(O365Krypter(p_Session), m_Annotations, SQLCosmosSyncManager::GetInstance().GetLastSyncDate(GetDBfilename(), m_Annotations.GetSQLcompliantName(), p_Session), m_ForCloudAnnotations);
	for (auto& a : m_ForCloudAnnotations)
	{
		a.m_Annotation.ReadPostProcess(a);// sad but true
		a.SetForCloudStorageOnly();
		WriteAnnotationRecord(&a, p_Session);// will only prepare values for Cosmos because of SetForCloudStorageOnly
		p_AllData[m_Annotations][std::to_wstring(a.GetID())] = &a;
	}

	SelectAll<AnnotationColumnSettingsRecord>(O365Krypter(p_Session), m_ColumnSettings, SQLCosmosSyncManager::GetInstance().GetLastSyncDate(GetDBfilename(), m_ColumnSettings.GetSQLcompliantName(), p_Session), m_ForCloudColumnSettings);
	for (auto& cs : m_ForCloudColumnSettings)
	{
		cs.m_AnnotationColumnSettings.ReadPostProcess(cs);// sad but true
		cs.SetForCloudStorageOnly();
		write(&cs, p_Session);// will only prepare values for Cosmos because of SetForCloudStorageOnly
		p_AllData[m_ColumnSettings][std::to_wstring(cs.GetID())] = &cs;
	}

	return true;// TODO check SelectAll errors
}

void AnnotationSQLite::CompleteUpdateToCloud(std::shared_ptr<const Sapio365Session> p_Session)
{
	m_ForCloudAnnotations.clear();
	m_ForCloudColumnSettings.clear();
	SetCloudSyncDate(p_Session);
}

bool AnnotationSQLite::UpdateFromCosmos(const CosmosUtil::SQLupdateData& p_AllData, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Prog)
{
	bool rvSuccess = true;

	TraceIntoFile::trace(_YDUMP("AnnotationSQLlite"), _YDUMP("UpdateFromCosmos"), _YTEXT("start"));

	size_t allSize = 0;
	for (const auto& p : p_AllData)
		allSize += p.second.size();
	ASSERT(allSize < (std::numeric_limits<DWORD>::max)());
	p_Prog.SetCounterTotal2(static_cast<DWORD>(allSize));

	auto findIt = p_AllData.find(m_Annotations);
	if (p_AllData.end() != findIt)
		updateSQLfromCosmosDocuments<AnnotationRecord>(m_Annotations, findIt->second, p_Session, p_Prog);

	findIt = p_AllData.find(m_ColumnSettings);
	if (p_AllData.end() != findIt)
		updateSQLfromCosmosDocuments<AnnotationColumnSettingsRecord>(m_ColumnSettings, findIt->second, p_Session, p_Prog);

	TraceIntoFile::trace(_YDUMP("AnnotationSQLlite"), _YDUMP("UpdateFromCosmos"), _YTEXT("end"));

	return rvSuccess;
}

template <class SQLRecordtype>
bool AnnotationSQLite::updateSQLfromCosmosDocuments(const SQLiteUtil::Table& p_Table, const vector<Cosmos::Document>& p_CosmosDocuments, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Prog)
{
	bool rvSuccess = true;

	const SQLiteUtil::Column& columnROWID	= p_Table.GetColumnROWID();
	const SQLiteUtil::Column& columnDATE	= p_Table.GetColumn(AnnotationUtil::g_ColumnNameDate);

	int64_t		rowId = 0;
	YTimeDate	lastUpdate;
	bool		validDate = false;

	const O365Krypter krypter(p_Session);

	for (const auto& cd : p_CosmosDocuments)
	{
		p_Prog.IncrementCounter2(_YFORMAT(L"Syncing Comments from Cosmos [%s]", p_Table.m_Name.c_str()));

		SQLRecordtype sqlObjectNew(p_Table);// the thing to write to the SQLite db
		sqlObjectNew.SetDoNotGenerateNewID();

		const auto& object = cd.Content.as_object();
		for (const auto& c : p_Table.m_Columns)
		{
			auto findIt = object.find(c.GetSQLcompliantName());
			if (findIt != object.end())
			{
				const auto& jsonValue = findIt->second;
				// everything must be stored as string in COSMOS! it is pointless to get numbers from the json text and reconvert these numbers to strings to create upate queries for SQLite
				if (findIt->second.is_string())
					sqlObjectNew.AddValue(c, jsonValue.as_string());
				else
					ASSERT(false);

				if (columnROWID == c)
					rowId = Str::getINT64FromString(jsonValue.as_string()); // jsonValue.as_number().to_int64();// abomination of desolation: stored as double in jsonValue = last digits are lost - EVERYTHING AS STRING!
				else if (columnDATE == c)
					validDate = TimeUtil::GetInstance().ConvertTextToDate(jsonValue.as_string(), lastUpdate);
			}
		}

		if (rowId != 0)
		{
			sqlObjectNew.SetID(rowId);

			bool update = false;

			YTimeDate date = getLastUpdate(p_Table, rowId, columnDATE);
			if (!date.IsEmpty())
			{
				sqlObjectNew.SetUpdateRecordFromCloud();
				if (validDate && lastUpdate > date)
					update = true;
			}
			else
			{
				sqlObjectNew.SetNewRecordFromCloud();
				update = true;
			}

			if (update)
				rvSuccess = WriteRecordFromValues(krypter, &sqlObjectNew) && rvSuccess;
		}
		else // ROWID not found, corrupted data
		{
			rvSuccess = false;
			auto findIt = object.find(_YTEXT("id"));// Cosmos standard key for their id entry - we set the same value as in "rowid"
			if (findIt != object.end())
			{
				const auto& jsonValue = findIt->second;
				ASSERT(findIt->second.is_string());
				TraceIntoFile::trace(_YDUMP("AnnotationSQLlite"), _YDUMP("updateSQLfromCosmosDocuments"), _YTEXTFORMAT(L"Corrupted data from Cosmos with id: %s (%s)", jsonValue.as_string().c_str(), m_Annotations.GetSQLcompliantName().c_str()));
			}
			else
				TraceIntoFile::trace(_YDUMP("AnnotationSQLlite"), _YDUMP("updateSQLfromCosmosDocuments"), _YTEXTFORMAT(L"Corrupted data from Cosmos with no id (%s)", m_Annotations.GetSQLcompliantName().c_str()));
		}
	}

	return rvSuccess;
}

wstring	AnnotationSQLite::GetColumnSettings(const wstring& p_Module, const wstring& p_ColumnUID, std::shared_ptr<const Sapio365Session> p_Session)
{
	return ReadColumnSettings(p_Module, p_ColumnUID, p_Session).m_AnnotationColumnSettings.m_Settings;
}

AnnotationColumnSettingsRecord AnnotationSQLite::ReadColumnSettings(const wstring& p_Module, const wstring& p_ColumnUID, std::shared_ptr<const Sapio365Session> p_Session)
{
	AnnotationColumnSettingsRecord settings(m_ColumnSettings);
	Select(O365Krypter(p_Session), &settings, { {m_ColumnSettings.m_Module.GetSQLcompliantName(), p_Module}, {m_ColumnSettings.m_UID.GetSQLcompliantName(), p_ColumnUID} });
	return settings;
}

AnnotationColumnSettingsRecord AnnotationSQLite::WriteColumnSettings(const wstring& p_Module, const wstring& p_UID, const wstring& p_Settings, std::shared_ptr<const Sapio365Session> p_Session)
{
	return WriteColumnSettings(AnnotationColumnSettings(p_Module, p_UID, p_Settings), p_Session);
}

AnnotationColumnSettingsRecord AnnotationSQLite::WriteColumnSettings(const AnnotationColumnSettings& p_ColumnSettings, std::shared_ptr<const Sapio365Session> p_Session)
{
	AnnotationColumnSettingsRecord acsr = ReadColumnSettings(p_ColumnSettings.m_Module, p_ColumnSettings.m_UID, p_Session);// load ID
	acsr.m_AnnotationColumnSettings.m_Module	= p_ColumnSettings.m_Module;
	acsr.m_AnnotationColumnSettings.m_UID		= p_ColumnSettings.m_UID;
	acsr.m_AnnotationColumnSettings.m_Settings	= p_ColumnSettings.m_Settings;
	if (!write(&acsr, p_Session))
		acsr.ResetID();
	acsr.m_AnnotationColumnSettings.m_ID = acsr.GetID();
	return acsr;
}

bool AnnotationSQLite::write(AnnotationColumnSettingsRecord* p_ColumnSettingsRecord, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	ASSERT(nullptr != p_ColumnSettingsRecord);
	if (nullptr != p_ColumnSettingsRecord)
	{
		ASSERT(!p_ColumnSettingsRecord->m_AnnotationColumnSettings.m_Module.empty());
		ASSERT(!p_ColumnSettingsRecord->m_AnnotationColumnSettings.m_UID.empty());
		if (!(p_ColumnSettingsRecord->m_AnnotationColumnSettings.m_Module.empty() || p_ColumnSettingsRecord->m_AnnotationColumnSettings.m_UID.empty()))
		{
			if (!p_ColumnSettingsRecord->IsForCloudStorageOnly())
				p_ColumnSettingsRecord->m_AnnotationColumnSettings.m_UpdDate = YTimeDate::GetCurrentTimeDate();
			rvSuccess = WriteRecord(O365Krypter(p_Session), p_ColumnSettingsRecord);
			// do not log modifications
		}
		else
		{
			auto err = _YFORMATERROR(L"Column setting: invalid module [%s] or column UID [%s]", p_ColumnSettingsRecord->m_AnnotationColumnSettings.m_Module.c_str(), p_ColumnSettingsRecord->m_AnnotationColumnSettings.m_UID.c_str());
			TraceIntoFile::trace(_YTEXT("AnnotationSQLite"), _YTEXT("write column setting"), err);
		}
	}

	return rvSuccess;
}