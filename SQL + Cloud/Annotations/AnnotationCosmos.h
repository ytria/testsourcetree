#pragma once

#include "AnnotationSQLite.h"
#include "CosmosSQLiteBridge.h"
#include "CosmosUtil.h"
#include "DlgDoubleProgressCommon.h"

class AnnotationCosmos : public CosmosSQLiteBridge
{
public:
	AnnotationCosmos();
	~AnnotationCosmos() = default;

	bool Write(const SQLiteUtil::SQLRecord* p_Document, std::shared_ptr<const Sapio365Session> p_Session);// an SQL record is a Cosmos document
	bool UpdateFromSQLite(const AnnotationUtil::CosmosUpdateData& p_SQLdata, const CosmosUtil::SQLupdateData& p_CosmosDocuments, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Prog);

	template<class SQLRecordType>
	void WriteAsync(const SQLRecordType* p_Document, std::shared_ptr<const Sapio365Session> p_Session)// an SQL record is a Cosmos document
	{
		if (nullptr != p_Document)
		{
			YSafeCreateTask([this, cosmosDoc = *p_Document, p_Session]()
			{
				Write(&cosmosDoc, p_Session);
			});
		}
	}
};