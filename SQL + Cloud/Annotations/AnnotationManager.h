#pragma once

#include "AnnotationCosmos.h"
#include "AnnotationSQLite.h"
#include "AnnotationUtil.h"
#include "DlgDoubleProgressCommon.h"
#include "ManagerSQLandCloud.h"

using namespace AnnotationUtil;

class AnnotationManager : public ManagerSQLandCloud
{
public:
	~AnnotationManager() = default;

	static AnnotationManager& GetInstance();

	virtual void InitAfterSessionOrLicenseWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress) override;// p_Session used for Cosmos

	bool LoadAnnotations(const wstring& p_Module, const wstring& p_RowPK, const set<wstring>& p_ColumnIDs, std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations);
	bool LoadAnnotations(const wstring& p_Module, const wstring& p_RowPK, const wstring& p_ColumnID, std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations);
	bool UpdateFromCloudAndLoadAnnotations(const wstring& p_Module, std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations, CWnd* p_Parent);
	bool UpdateFromCloudAndLoadAnnotationsAll(std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations, CWnd* p_Parent);
	
	bool			Write(GridAnnotation& p_Annotation, std::shared_ptr<const Sapio365Session> p_Session);// p_Session used for Cosmos - TODO process errors (returns false), only traced as of 04.2019	
	GridAnnotation	Read(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);

	void StartTransaction();
	void EndTransaction();

	bool	WriteColumnSettings(const wstring& p_Module, const wstring& p_UID, const wstring& p_Settings, std::shared_ptr<const Sapio365Session> p_Session);
	wstring	GetColumnSettings(const wstring& p_Module, const wstring& p_UID, std::shared_ptr<const Sapio365Session> p_Session);

private:
	AnnotationManager();

	bool updateSQLiteFromCloud(std::shared_ptr<const Sapio365Session> p_Session, const CosmosUtil::SQLupdateData& p_CosmosDocuments, DlgDoubleProgressCommon& p_Progress);// data updated from Cosmos to SQLite
	bool pushSQLiteToCloud(std::shared_ptr<const Sapio365Session> p_Session, const CosmosUtil::SQLupdateData& p_CosmosDocuments, DlgDoubleProgressCommon& p_Progress);// data updated from SQLite to Cosmos

	AnnotationSQLite	m_AnnotationSQLite;// Jesus
	AnnotationCosmos	m_AnnotationCosmos;// Holy Spirit
};