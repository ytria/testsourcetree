#include "AnnotationUtil.h"

#include "Str.h"

using namespace AnnotationUtil;

AnnotationModification::~AnnotationModification()
{
}

vector<Modification::ModificationLog> AnnotationModification::GetModificationLogs() const
{
	vector<Modification::ModificationLog> modificationLogs;

	vector<wstring> ownerObjectPK;// pk is not the annotation ID, rather the referred object's grid pk to enable meaningful analysis in the user activity logs
	if (nullptr != m_Record.m_Annotation.m_Grid_VOL)
	{
		LinearMap<GridBackendColumn*, wstring> PKfieldAsStrings;
		m_Record.m_Annotation.ExtractParentPK(*m_Record.m_Annotation.m_Grid_VOL, PKfieldAsStrings);
		for (size_t k = 0; k < PKfieldAsStrings.size(); k++)
		{
			const auto& pkColumn	= PKfieldAsStrings.at(k);
			const auto& pkStr		= PKfieldAsStrings[pkColumn];
			ownerObjectPK.push_back(pkStr);
		}
	}

	const auto& oldValues		= m_Record.GetOldValues();
	const bool	isCreateAction	= oldValues.empty();
	wstring		action			= m_Record.IsRemoved()	?	YtriaTranslate::Do(ApplicationShortcuts_ApplicationShortcuts_166, _YLOC("Delete")) :
									isCreateAction		?	YtriaTranslate::Do(DlgBusinessEditHTML_getDialogTitle_3, _YLOC("Create")) :
															YtriaTranslate::Do(DlgHelpMain_OnInitDialog_5, _YLOC("Update"));
	const auto& newValues = m_Record.GetValues();
	// optimization: log minimum info for creation/destruction
	if (isCreateAction || m_Record.IsRemoved())
	{
		auto findText = newValues.find(m_Record.GetTable().GetColumn(g_ColumnNameText));
		ASSERT(newValues.end() != findText);
		if (newValues.end() != findText)
			modificationLogs.emplace_back(ownerObjectPK, GetObjectName(), action, g_ColumnNameText, isCreateAction ? GridBackendUtil::g_NoValueString : findText->second, isCreateAction ? findText->second : GridBackendUtil::g_NoValueString, State::AppliedLocally);
	}
	else
		for (const auto newValue : newValues)
		{
			const auto& c = newValue.first;
			if (m_Record.IsValue4ModificationLog(c.m_Name))
			{
				auto findIt = oldValues.find(c);
				ASSERT(oldValues.end() != findIt);
				auto ov	= oldValues.end() == findIt ? GridBackendUtil::g_NoValueString : findIt->second;
				auto nv	= newValue.second;
				if (ov != nv)
				{
					if (AnnotationUtil::g_ColumnNameFlags == c.m_Name)
					{
						ov = Str::implode(GridAnnotation::GetFlagLabels(Str::getUINT64FromString(ov)), _YTEXT(", "));
						nv = Str::implode(GridAnnotation::GetFlagLabels(Str::getUINT64FromString(nv)), _YTEXT(", "));
					}

					modificationLogs.emplace_back(ownerObjectPK, GetObjectName(), action, c.m_NameForDisplay.empty() ? c.m_Name : c.m_NameForDisplay, ov, nv, State::AppliedLocally);
				}
			}
		}

	return modificationLogs;
}

AnnotationModification::AnnotationModification(const AnnotationRecord& p_Record) :
	Modification(State::AppliedLocally, _YFORMAT(L"Comment about: %s", p_Record.m_Annotation.m_ReferenceName.c_str())),
	m_Record(p_Record)
{
}

const AnnotationRecord& AnnotationModification::GetRecord() const
{
	return m_Record;
}