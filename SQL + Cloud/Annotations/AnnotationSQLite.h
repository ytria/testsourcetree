#pragma once

#include "AnnotationUtil.h"
#include "CosmosUtil.h"
#include "DlgDoubleProgressCommon.h"
#include "Document.h"
#include "O365SQLiteEngine.h"
#include "Sapio365Session.h"

using namespace SQLiteUtil;
using namespace AnnotationUtil;

// not using AnnotationManagerDefault (is without user/tenant management)

class AnnotationSQLite : public O365SQLiteEngine
{
public:
	AnnotationSQLite();
	virtual ~AnnotationSQLite() = default;

	virtual vector<SQLiteUtil::Table> GetTables() const override;

	bool LoadAnnotations(const bool p_AllTenantsAndModules, const wstring& p_Module, const wstring& p_RowPK, const set<wstring>& p_ColumnIDs, std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations);//all: all tenants, all modules
	bool LoadAnnotations(const wstring& p_Module, std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations);
	bool LoadAnnotationsAll(std::shared_ptr<const Sapio365Session> p_Session, vector<GridAnnotation>& p_Annotations);

	AnnotationRecord	Read(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);
	AnnotationRecord	Write(const GridAnnotation& p_Annotation, std::shared_ptr<const Sapio365Session> p_Session);
	bool				WriteAnnotationRecord(AnnotationRecord* p_Arecord, std::shared_ptr<const Sapio365Session> p_Session);

	bool SelectAllForUpdateToCloud(CosmosUpdateData& p_AllData, std::shared_ptr<const Sapio365Session> p_Session);// for Cosmos update
	void CompleteUpdateToCloud(std::shared_ptr<const Sapio365Session> p_Session);
	bool UpdateFromCosmos(const CosmosUtil::SQLupdateData& p_AllData, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Prog);

	const TableAnnotation& GetTable() const;

	wstring GetTenantName(const std::shared_ptr<const Sapio365Session>& p_Session) const;

	wstring							GetColumnSettings(const wstring& p_Module, const wstring& p_ColumnUID, std::shared_ptr<const Sapio365Session> p_Session);
	AnnotationColumnSettingsRecord	ReadColumnSettings(const wstring& p_Module, const wstring& p_ColumnUID, std::shared_ptr<const Sapio365Session> p_Session);
	AnnotationColumnSettingsRecord	WriteColumnSettings(const wstring& p_Module, const wstring& p_UID, const wstring& p_Settings, std::shared_ptr<const Sapio365Session> p_Session);
	AnnotationColumnSettingsRecord	WriteColumnSettings(const AnnotationColumnSettings& p_ColumnSettings, std::shared_ptr<const Sapio365Session> p_Session);

	uint32_t GetVersion() const override
	{
		return g_CurrentVersion;
	}

private:
	bool prepareUpdateInfo(GridAnnotation& p_Annotation, std::shared_ptr<const Sapio365Session> p_Session) const;

	template <class SQLRecordtype>
	bool updateSQLfromCosmosDocuments(const SQLiteUtil::Table& p_Table, const vector<Cosmos::Document>& p_CosmosDocuments, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Prog);

	bool write(AnnotationColumnSettingsRecord* p_ColumnSettings, std::shared_ptr<const Sapio365Session> p_Session);

	TableAnnotation					m_Annotations;
	TableAnnotationColumnSettings	m_ColumnSettings;

	vector<AnnotationRecord>				m_ForCloudAnnotations;
	vector<AnnotationColumnSettingsRecord>	m_ForCloudColumnSettings;

	static constexpr uint32_t g_CurrentVersion = 6;
};