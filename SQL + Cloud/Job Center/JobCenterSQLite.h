#pragma once

#include "AutomationWizard.h"
#include "O365SQLiteEngine.h"

class JobCenterSQLite : public O365SQLiteEngine
{
public:
	JobCenterSQLite();
	virtual ~JobCenterSQLite() = default;

	virtual vector<SQLiteUtil::Table>	GetTables() const override;	

	bool LoadAllScripts(std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts);
	bool LoadScripts(AutomationWizard* p_Wizard, std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts, const bool p_ForceSandboxScripts);
	bool LoadScripts(AutomationWizard* p_Wizard, std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts);
	bool GetScript(const wstring& p_ScriptKey, std::shared_ptr<const Sapio365Session> p_Session, Script& p_Script);
	bool GetScriptPreset(const wstring& p_ScriptPresetKey, std::shared_ptr<const Sapio365Session> p_Session, ScriptPreset& p_ScriptPreset);
	bool GetScriptSchedule(const wstring& p_ScriptScheduleKey, std::shared_ptr<const Sapio365Session> p_Session, ScriptSchedule& p_ScriptSchedule);
	bool HasScript(const wstring& p_ScriptKey, std::shared_ptr<const Sapio365Session> p_Session);
	bool HasScriptPreset(const wstring& p_ScriptPresetKey, std::shared_ptr<const Sapio365Session> p_Session);

	uint32_t GetScriptCount(const wstring& p_Module, std::shared_ptr<const Sapio365Session> p_Session);
	uint32_t GetScriptPresetCount(const int64_t& p_ScriptID, const bool p_WithRemoved, std::shared_ptr<const Sapio365Session> p_Session);

	ScriptRecord			Read(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);
	ScriptRecord			Write(const Script& p_Script, std::shared_ptr<const Sapio365Session> p_Session);
	ScriptPresetRecord		ReadPreset(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);
	ScriptPresetRecord		Write(const ScriptPreset& p_ScriptPreset, std::shared_ptr<const Sapio365Session> p_Session);
	ScriptScheduleRecord	ReadSchedule(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);
	ScriptScheduleRecord	Write(const ScriptSchedule& p_ScriptSchedule, std::shared_ptr<const Sapio365Session> p_Session);
	
	bool LoadSchedules(ScriptPreset& p_ScriptPreset, std::shared_ptr<const Sapio365Session> p_Session);
	bool Load(Script& p_Script, std::shared_ptr<const Sapio365Session> p_Session);

	bool WriteScriptRecord(ScriptRecord* p_Arecord, std::shared_ptr<const Sapio365Session> p_Session);
	bool WriteScriptPresetRecord(ScriptPresetRecord* p_Arecord, std::shared_ptr<const Sapio365Session> p_Session);
	bool WriteScriptScheduleRecord(ScriptScheduleRecord* p_Arecord, std::shared_ptr<const Sapio365Session> p_Session);

	bool IsEmpty(std::shared_ptr<const Sapio365Session> p_Session) const;

	uint32_t GetVersion() const override
	{
		return g_CurrentVersion;
	}

private:
	JobCenterSQLite(const wstring& p_DBfile); // For migration

	static constexpr uint32_t g_CurrentVersion = 8;

	template <class T>
	bool prepareUpdateInfo(T& p_ScriptItem, std::shared_ptr<const Sapio365Session> p_Session) const;
	
	SQLiteUtil::WhereClause getWC(const JobSelectionFilter* p_Filter);

	bool loadScripts(bool p_ParseMetadata, AutomationWizard* p_Wizard, std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts, const bool p_ForceSandboxScripts);

	static wstring getVersionedFilename(int p_Version);
	static wstring getFilenameAndMigrateIfNecessary();
	// Specify template for custom migration -- Default is just file copy.
	template<int vFrom, int vTo>
	struct MigrateDatabase
	{
	public:
		void operator()();
	};

	template<int vFrom>
	struct MigrateDatabase<vFrom, 8>
	{
	public:
		void operator()();
	};

	ScriptTable			m_Scripts;
	SQLiteUtil::Table	m_ScriptPresets;
	SQLiteUtil::Table	m_ScriptSchedules;
};
