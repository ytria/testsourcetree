#include "DlgJobPresetEdit.h"

wstring DlgJobPresetEdit::g_VarNameTitle;
wstring DlgJobPresetEdit::g_VarNameDescription;

DlgJobPresetEdit::DlgJobPresetEdit(const Script& p_Script, CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
	, m_Script(p_Script)
{
	if (g_VarNameTitle.empty())
	{
		g_VarNameTitle			= _YTEXT("PresetTitle");
		g_VarNameDescription	= _YTEXT("PresetDesc");
	}
}

DlgJobPresetEdit::DlgJobPresetEdit(const Script& p_Script, const ScriptPreset& p_Preset, CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
	, m_Script(p_Script)
	, m_Preset(p_Preset)
{
	if (g_VarNameTitle.empty())
	{
		g_VarNameTitle			= _YTEXT("PresetTitle");
		g_VarNameDescription	= _YTEXT("PresetDesc");
	}
}

void DlgJobPresetEdit::generateJSONScriptData()
{
	initMain(_YTEXT("DlgJobPresetEdit"));

	getGenerator().Add(StringEditor(g_VarNameTitle,		_T("Saved settings Reference"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, m_Preset.m_Title));
	getGenerator().Add(StringEditor(g_VarNameDescription,	_T("Description"), 0, m_Preset.m_Description));

	getGenerator().addApplyButton(YtriaTranslate::Do(DlgAutomationInputHTML_generateJSONScriptData_1, _YLOC("OK")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgAutomationInputHTML_generateJSONScriptData_2, _YLOC("Cancel")).c_str());
}

bool DlgJobPresetEdit::processPostedData(const IPostedDataTarget::PostedData& data)
{
	for (const auto& item : data)
		if (MFCUtil::StringMatchW(item.first, g_VarNameTitle))
			m_Preset.m_Title = item.second;
		else if (MFCUtil::StringMatchW(item.first, g_VarNameDescription))
			m_Preset.m_Description = item.second;

	return true;
}

ScriptPreset& DlgJobPresetEdit::GetPreset()
{
	return m_Preset;
}

wstring DlgJobPresetEdit::getDialogTitle() const
{
	return _YFORMAT(L"Edit saved settings for job: %s", m_Script.m_Title.c_str());
}