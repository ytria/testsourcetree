#pragma once

#include "DlgDoubleProgressCommon.h"
#include "JobCenterSQLite.h"
#include "ManagerSQLandCloud.h"

using namespace JobCenterUtil;

class JobCenterManager : public ManagerSQLandCloud
{
public:
	~JobCenterManager() = default;

	static JobCenterManager& GetInstance();

	bool			LoadScripts(AutomationWizard* p_Wizard, std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts);
	bool			LoadScripts(AutomationWizard* p_Wizard, std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts, const bool p_ForceSandboxScripts);
	bool			LoadAllScripts(std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts);
	bool			Write(Script& p_Script, std::shared_ptr<const Sapio365Session> p_Session);// p_Session used for Cosmos - TODO process errors (returns false)
	Script			Read(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);
	bool			GetScript(const wstring& p_ScriptKey, std::shared_ptr<const Sapio365Session> p_Session, Script& p_Script);
	bool			Write(ScriptPreset& p_ScriptPreset, std::shared_ptr<const Sapio365Session> p_Session);// p_Session used for Cosmos - TODO process errors (returns false)
	ScriptPreset	ReadPreset(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);
	bool			GetScriptPreset(const wstring& p_ScriptPresetKey, std::shared_ptr<const Sapio365Session> p_Session, ScriptPreset& p_ScriptPreset);
	bool			Write(ScriptSchedule& p_ScriptSchedule, std::shared_ptr<const Sapio365Session> p_Session);// p_Session used for Cosmos - TODO process errors (returns false)
	ScriptSchedule	ReadSchedule(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);
	bool			GetScriptSchedule(const wstring& p_ScriptScheduleKey, std::shared_ptr<const Sapio365Session> p_Session, ScriptSchedule& p_ScriptSchedule);
	uint32_t		GetScriptCount(const wstring& p_Module, std::shared_ptr<const Sapio365Session> p_Session);
	uint32_t		GetScriptPresetCount(const int64_t& p_ScriptID, const bool p_WithRemoved, std::shared_ptr<const Sapio365Session> p_Session);
	bool			HasScript(const wstring& p_ScriptKey, std::shared_ptr<const Sapio365Session> p_Session);
	bool			HasScriptPreset(const wstring& p_ScriptPresetKey, std::shared_ptr<const Sapio365Session> p_Session);
	bool			LoadSchedules(ScriptPreset& p_ScriptPreset, std::shared_ptr<const Sapio365Session> p_Session);
	bool			Load(Script& p_Script, std::shared_ptr<const Sapio365Session> p_Session);
	bool			IsEmpty(std::shared_ptr<const Sapio365Session> p_Session) const;

	virtual void InitAfterSessionOrLicenseWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress) override;// p_Session used for Cosmos

private:
	JobCenterManager();

	JobCenterSQLite m_JobCenterSQLite;
};