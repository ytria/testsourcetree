#include "JobCenterManager.h"

#include "AutomationWizardO365.h"

JobCenterManager::JobCenterManager() : ManagerSQLandCloud()
{
	if (!m_JobCenterSQLite.Configure())
		SetError(SQLCFG_ERROR);
}

JobCenterManager& JobCenterManager::GetInstance()
{
	static JobCenterManager g_Instance;
	return g_Instance;
}

bool JobCenterManager::LoadScripts(AutomationWizard* p_Wizard, std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts)
{
	return LoadScripts(p_Wizard, p_Session, p_Scripts, false);
}

bool JobCenterManager::LoadScripts(AutomationWizard* p_Wizard, std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts, const bool p_ForceSandboxScripts)
{
	return m_JobCenterSQLite.LoadScripts(p_Wizard, p_Session, p_Scripts, p_ForceSandboxScripts);
}

bool JobCenterManager::LoadAllScripts(std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts)
{
	return m_JobCenterSQLite.LoadAllScripts(p_Session, p_Scripts);
}

bool JobCenterManager::Write(Script& p_Script, std::shared_ptr<const Sapio365Session> p_Session)
{
	auto scriptRecord = m_JobCenterSQLite.Write(p_Script, p_Session);
	p_Script = scriptRecord.m_Script;
	// TODO Cosmos update
	return scriptRecord.HasValidID();
}

Script JobCenterManager::Read(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_JobCenterSQLite.Read(p_ID, p_Session).m_Script;
}

bool JobCenterManager::GetScript(const wstring& p_ScriptKey, std::shared_ptr<const Sapio365Session> p_Session, Script& p_Script)
{
	return m_JobCenterSQLite.GetScript(p_ScriptKey, p_Session, p_Script);
}

bool JobCenterManager::Write(ScriptPreset& p_ScriptPreset, std::shared_ptr<const Sapio365Session> p_Session)
{
	auto scriptPresetRecord = m_JobCenterSQLite.Write(p_ScriptPreset, p_Session);
	p_ScriptPreset = scriptPresetRecord.m_ScriptPreset;
	// TONOTDO Cosmos update: it is not advised to cloud-share presets, which more often than not contain data that is local to the machine or individual settings (e.g. email, export path etc.)
	return scriptPresetRecord.HasValidID();
}

ScriptPreset JobCenterManager::ReadPreset(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_JobCenterSQLite.ReadPreset(p_ID, p_Session).m_ScriptPreset;
}

bool JobCenterManager::GetScriptPreset(const wstring& p_ScriptPresetKey, std::shared_ptr<const Sapio365Session> p_Session, ScriptPreset& p_ScriptPreset)
{
	return m_JobCenterSQLite.GetScriptPreset(p_ScriptPresetKey, p_Session, p_ScriptPreset);
}

bool JobCenterManager::Write(ScriptSchedule& p_ScriptSchedule, std::shared_ptr<const Sapio365Session> p_Session)
{
	auto scriptScheduleRecord = m_JobCenterSQLite.Write(p_ScriptSchedule, p_Session);
	p_ScriptSchedule = scriptScheduleRecord.m_ScriptSchedule;
	// TODO Cosmos update
	return scriptScheduleRecord.HasValidID();
}

ScriptSchedule JobCenterManager::ReadSchedule(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_JobCenterSQLite.ReadSchedule(p_ID, p_Session).m_ScriptSchedule;
}

bool JobCenterManager::GetScriptSchedule(const wstring& p_ScriptScheduleKey, std::shared_ptr<const Sapio365Session> p_Session, ScriptSchedule& p_ScriptSchedule)
{
	return m_JobCenterSQLite.GetScriptSchedule(p_ScriptScheduleKey, p_Session, p_ScriptSchedule);
}

void JobCenterManager::InitAfterSessionOrLicenseWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress)
{
	CWaitCursor _;

	ASSERT(p_Session);
	if (IsReadyToRun() && p_Session)
	{
		AutomationWizardO365::InitAfterSessionWasSet(p_Session, p_Progress);

		// TODO Cosmos sync
	}
}

uint32_t JobCenterManager::GetScriptCount(const wstring& p_Module, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_JobCenterSQLite.GetScriptCount(p_Module, p_Session);
}

uint32_t JobCenterManager::GetScriptPresetCount(const int64_t& p_ScriptID, const bool p_WithRemoved, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_JobCenterSQLite.GetScriptPresetCount(p_ScriptID, p_WithRemoved, p_Session);
}

bool JobCenterManager::HasScript(const wstring& p_ScriptKey, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_JobCenterSQLite.HasScript(p_ScriptKey, p_Session);
}

bool JobCenterManager::HasScriptPreset(const wstring& p_ScriptPresetKey, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_JobCenterSQLite.HasScriptPreset(p_ScriptPresetKey, p_Session);
}

bool JobCenterManager::LoadSchedules(ScriptPreset& p_ScriptPreset, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_JobCenterSQLite.LoadSchedules(p_ScriptPreset, p_Session);
}

bool JobCenterManager::Load(Script& p_Script, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_JobCenterSQLite.Load(p_Script, p_Session);
}

bool JobCenterManager::IsEmpty(std::shared_ptr<const Sapio365Session> p_Session) const
{
	return m_JobCenterSQLite.IsEmpty(p_Session);
}
