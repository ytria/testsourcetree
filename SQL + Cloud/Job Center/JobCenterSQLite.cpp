#include "JobCenterSQLite.h"

#include "Office365Admin.h"
#include "SqlQueryMultiPreparedStatement.h"
#include "YCodeJockMessageBox.h"

JobCenterSQLite::JobCenterSQLite()
	: JobCenterSQLite(getFilenameAndMigrateIfNecessary())
{
}

JobCenterSQLite::JobCenterSQLite(const wstring& p_DBfile)
	: O365SQLiteEngine(p_DBfile)
{
	// Update this assert when version change.
	// For old reasons, g_CurrentVersion must at least be incremented each time SQLiteEngine::GetCurrentVersion() is.
	static_assert(g_CurrentVersion == 8 && SQLiteEngine::GetCurrentVersion() == 6, "Should you increment version or test?");

	{
		m_ScriptPresets.m_Name				= _YTEXT("ScriptPresets");
		m_ScriptPresets.m_NameForDisplay	= _T("Presets");

		m_ScriptPresets.AddColumnRowID();

		SQLiteUtil::Column cScriptID;
		cScriptID.m_Name				= JobCenterUtil::g_ColumnNameScriptID;
		cScriptID.m_DataType			= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cScriptID.m_ForeignKeyTable		= m_Scripts.GetSQLcompliantName();
		cScriptID.m_ForeignKeyColumn	= m_Scripts.GetColumnROWID().GetSQLcompliantName();
		m_ScriptPresets.AddColumn(cScriptID);

		SQLiteUtil::Column cStatus;
		cStatus.m_Name		= JobCenterUtil::g_ColumnNameStatus;
		cStatus.m_DataType	= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cStatus.m_Flags		= SQLiteUtil::NOTINGRID;
		m_ScriptPresets.AddColumn(cStatus);

		SQLiteUtil::Column cKey;
		cKey.m_Name				= JobCenterUtil::g_ColumnNameKey;
		cKey.m_NameForDisplay	= _T("Key");
		cKey.m_Flags			= SQLiteUtil::CASEINSENSITIVE;
		cKey.m_Flags			= SQLiteUtil::NOTNULL;
		m_ScriptPresets.AddColumn(cKey);

		SQLiteUtil::Column cTitle;
		cTitle.m_Name			= JobCenterUtil::g_ColumnNameTitle;
		cTitle.m_NameForDisplay = _T("Title");
		cTitle.m_Flags			= SQLiteUtil::NOTNULL;
		m_ScriptPresets.AddColumn(cTitle);

		SQLiteUtil::Column cDescription;
		cDescription.m_Name				= JobCenterUtil::g_ColumnNameDescription;
		cDescription.m_NameForDisplay	= _T("Description");
		m_ScriptPresets.AddColumn(cDescription);

		SQLiteUtil::Column cContent;
		cContent.m_Name				= JobCenterUtil::g_ColumnNameContent;
		cContent.m_NameForDisplay	= _T("XML content");
		cContent.m_Flags			= SQLiteUtil::NOTINGRID;
		m_ScriptPresets.AddColumn(cContent);

		SQLiteUtil::Column cSourcePath;
		cSourcePath.m_Name				= JobCenterUtil::g_ColumnNameSourceXMLFilePath;
		cSourcePath.m_NameForDisplay	= _T("Source File Path");
		m_ScriptPresets.AddColumn(cSourcePath);

		SQLiteUtil::Column cDate;
		cDate.m_Name			= JobCenterUtil::g_ColumnNameDate;
		cDate.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_3, _YLOC("Last Updated On")).c_str();
		cDate.m_Flags			= SQLiteUtil::ColumnFlags::ISDATE;
		m_ScriptPresets.AddColumn(cDate);

		SQLiteUtil::Column cUserUPDID;
		cUserUPDID.m_Name			= JobCenterUtil::g_ColumnNameUpdID;
		cUserUPDID.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_4, _YLOC("Last Updated By (ID)")).c_str();
		m_ScriptPresets.AddColumn(cUserUPDID);

		SQLiteUtil::Column cUserUPDName;
		cUserUPDName.m_Name				= JobCenterUtil::g_ColumnNameUpdName;
		cUserUPDName.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_5, _YLOC("Last Updated By (Name)")).c_str();
		m_ScriptPresets.AddColumn(cUserUPDName);

		SQLiteUtil::Column cUserUPDMail;
		cUserUPDMail.m_Name				= JobCenterUtil::g_ColumnNameUpdEmail;
		cUserUPDMail.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_6, _YLOC("Last Updated By (email)")).c_str();
		m_ScriptPresets.AddColumn(cUserUPDMail);
	}

	{
		m_ScriptSchedules.m_Name			= _YTEXT("ScriptSchedules");
		m_ScriptSchedules.m_NameForDisplay	= _T("Scheduled Tasks");

		m_ScriptSchedules.AddColumnRowID();

		SQLiteUtil::Column cScriptID;
		cScriptID.m_Name				= JobCenterUtil::g_ColumnNameScriptID;
		cScriptID.m_DataType			= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cScriptID.m_ForeignKeyTable		= m_Scripts.GetSQLcompliantName();
		cScriptID.m_ForeignKeyColumn	= m_Scripts.GetColumnROWID().GetSQLcompliantName();
		m_ScriptSchedules.AddColumn(cScriptID);

		SQLiteUtil::Column cScriptPresetID;
		cScriptPresetID.m_Name				= JobCenterUtil::g_ColumnNameScriptPresetID;
		cScriptPresetID.m_DataType			= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cScriptPresetID.m_ForeignKeyTable	= m_ScriptPresets.GetSQLcompliantName();
		cScriptPresetID.m_ForeignKeyColumn	= m_ScriptPresets.GetColumnROWID().GetSQLcompliantName();
		m_ScriptSchedules.AddColumn(cScriptPresetID);

		SQLiteUtil::Column cStatus;
		cStatus.m_Name		= JobCenterUtil::g_ColumnNameStatus;
		cStatus.m_DataType	= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cStatus.m_Flags		= SQLiteUtil::NOTINGRID;
		m_ScriptSchedules.AddColumn(cStatus);

		SQLiteUtil::Column cKey;
		cKey.m_Name				= JobCenterUtil::g_ColumnNameKey;
		cKey.m_NameForDisplay	= _T("Key");
		cKey.m_Flags			= SQLiteUtil::CASEINSENSITIVE;
		cKey.m_Flags			= SQLiteUtil::NOTNULL;
		m_ScriptSchedules.AddColumn(cKey);

		SQLiteUtil::Column cTitle;
		cTitle.m_Name			= JobCenterUtil::g_ColumnNameTitle;
		cTitle.m_NameForDisplay = _T("Title");
		cTitle.m_Flags			= SQLiteUtil::NOTNULL;
		m_ScriptSchedules.AddColumn(cTitle);

		SQLiteUtil::Column cDescription;
		cDescription.m_Name				= JobCenterUtil::g_ColumnNameDescription;
		cDescription.m_NameForDisplay	= _T("Description");
		m_ScriptSchedules.AddColumn(cDescription);

		SQLiteUtil::Column cDate;
		cDate.m_Name			= JobCenterUtil::g_ColumnNameDate;
		cDate.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_3, _YLOC("Last Updated On")).c_str();
		cDate.m_Flags			= SQLiteUtil::ColumnFlags::ISDATE;
		m_ScriptSchedules.AddColumn(cDate);

		SQLiteUtil::Column cUserUPDID;
		cUserUPDID.m_Name			= JobCenterUtil::g_ColumnNameUpdID;
		cUserUPDID.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_4, _YLOC("Last Updated By (ID)")).c_str();
		m_ScriptSchedules.AddColumn(cUserUPDID);

		SQLiteUtil::Column cUserUPDName;
		cUserUPDName.m_Name				= JobCenterUtil::g_ColumnNameUpdName;
		cUserUPDName.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_5, _YLOC("Last Updated By (Name)")).c_str();
		m_ScriptSchedules.AddColumn(cUserUPDName);

		SQLiteUtil::Column cUserUPDMail;
		cUserUPDMail.m_Name				= JobCenterUtil::g_ColumnNameUpdEmail;
		cUserUPDMail.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_6, _YLOC("Last Updated By (email)")).c_str();
		m_ScriptSchedules.AddColumn(cUserUPDMail);
	}
}

vector<SQLiteUtil::Table> JobCenterSQLite::GetTables() const
{
	return { m_Scripts, m_ScriptPresets, m_ScriptSchedules };
}

bool JobCenterSQLite::LoadSchedules(ScriptPreset& p_ScriptPreset, std::shared_ptr<const Sapio365Session> /*p_Session*/)
{
	auto rv = false;

	p_ScriptPreset.m_Schedules_Vol.clear();

	ASSERT(p_ScriptPreset.HasID());
	if (p_ScriptPreset.HasID())
	{
		SQLiteUtil::SelectQuery qSchedules;
		qSchedules.m_Table = m_ScriptSchedules;

		SQLiteUtil::WhereClause whereNotRemovedSchedule;
		whereNotRemovedSchedule.m_Column	= m_ScriptSchedules.GetColumnForQuery(JobCenterUtil::g_ColumnNameStatus);
		whereNotRemovedSchedule.m_Operator	= SQLiteUtil::Operator::NotEqual;
		whereNotRemovedSchedule.m_Values.push_back(Str::getStringFromNumber<uint32_t>(JobCenterStatus::STATUS_REMOVED));
		qSchedules.m_Where.push_back(whereNotRemovedSchedule);

		SQLiteUtil::WhereClause whereScriptPresetID;
		whereScriptPresetID.m_Column	= m_ScriptSchedules.GetColumnForQuery(JobCenterUtil::g_ColumnNameScriptPresetID);
		whereScriptPresetID.m_Operator	= SQLiteUtil::Operator::Equal;
		whereScriptPresetID.m_Values.push_back(Str::getStringFromNumber<int64_t>(p_ScriptPreset.m_ID.get()));
		ASSERT(!whereScriptPresetID.m_Values.front().empty());
		qSchedules.m_Where.push_back(whereScriptPresetID);

		vector<ScriptScheduleRecord> scheduleRecords;
		// Nothing encrypted, no session needed finally
		rv = SelectAll<ScriptScheduleRecord>(VoidKrypter()/*O365Krypter(p_Session)*/, qSchedules, scheduleRecords);
		if (rv)
			for (const auto& schr : scheduleRecords)
				p_ScriptPreset.m_Schedules_Vol.push_back(schr.m_ScriptSchedule);
	}

	return rv;
}

bool JobCenterSQLite::Load(Script& p_Script, std::shared_ptr<const Sapio365Session> /*p_Session*/)
{
	auto rv = false;

	p_Script.m_Presets_Vol.clear();
	p_Script.m_Schedules_Vol.clear();

	ASSERT(p_Script.HasID());
	if (p_Script.HasID())
	{
		// Nothing encrypted, no session needed finally
		auto krypt = VoidKrypter();// O365Krypter(p_Session);

		SQLiteUtil::SelectQuery qSchedules;
		qSchedules.m_Table = m_ScriptSchedules;

		SQLiteUtil::WhereClause whereNotRemovedSchedule;
		whereNotRemovedSchedule.m_Column	= m_ScriptSchedules.GetColumnForQuery(JobCenterUtil::g_ColumnNameStatus);
		whereNotRemovedSchedule.m_Operator	= SQLiteUtil::Operator::NotEqual;
		whereNotRemovedSchedule.m_Values.push_back(Str::getStringFromNumber<uint32_t>(JobCenterStatus::STATUS_REMOVED));
		qSchedules.m_Where.push_back(whereNotRemovedSchedule);

		SQLiteUtil::WhereClause whereScriptIDSchedule;
		whereScriptIDSchedule.m_Column		= m_ScriptSchedules.GetColumnForQuery(JobCenterUtil::g_ColumnNameScriptID);
		whereScriptIDSchedule.m_Operator	= SQLiteUtil::Operator::Equal;
		whereScriptIDSchedule.m_Values.push_back(Str::getStringFromNumber<int64_t>(p_Script.m_ID.get()));
		ASSERT(!whereScriptIDSchedule.m_Values.front().empty());
		qSchedules.m_Where.push_back(whereScriptIDSchedule);

		vector<ScriptScheduleRecord> schedules;
		SelectAll<ScriptScheduleRecord>(krypt, qSchedules, schedules);


		SQLiteUtil::SelectQuery qPresets;
		qPresets.m_Table = m_ScriptPresets;

		SQLiteUtil::WhereClause whereNotRemovedPresets;
		whereNotRemovedPresets.m_Column		= m_ScriptPresets.GetColumnForQuery(JobCenterUtil::g_ColumnNameStatus);
		whereNotRemovedPresets.m_Operator	= SQLiteUtil::Operator::NotEqual;
		whereNotRemovedPresets.m_Values.push_back(Str::getStringFromNumber<uint32_t>(JobCenterStatus::STATUS_REMOVED));
		qPresets.m_Where.push_back(whereNotRemovedPresets);

		SQLiteUtil::WhereClause whereScriptIDPreset;
		whereScriptIDPreset.m_Column	= m_ScriptPresets.GetColumnForQuery(JobCenterUtil::g_ColumnNameScriptID);
		whereScriptIDPreset.m_Operator	= SQLiteUtil::Operator::Equal;
		whereScriptIDPreset.m_Values.push_back(Str::getStringFromNumber<int64_t>(p_Script.m_ID.get()));
		ASSERT(!whereScriptIDPreset.m_Values.front().empty());
		qPresets.m_Where.push_back(whereScriptIDPreset);

		vector<ScriptPresetRecord> presets;
		SelectAll<ScriptPresetRecord>(krypt, qPresets, presets);

		for (auto& preset : presets)
		{
			preset.m_ScriptPreset.ReadPostProcess(preset);// sad but true
			p_Script.m_Presets_Vol.push_back(preset.m_ScriptPreset);
			auto& scriptPreset = p_Script.m_Presets_Vol.back();
			for (auto it = schedules.begin(); it != schedules.end();)
			{
				auto& schedRec = *it;
				schedRec.m_ScriptSchedule.ReadPostProcess(schedRec);// sad but true
				if (scriptPreset.m_ID && schedRec.m_ScriptSchedule.m_ScriptPresetID == scriptPreset.m_ID)
				{
					scriptPreset.m_Schedules_Vol.push_back(schedRec.m_ScriptSchedule);
					it = schedules.erase(it);
				}
				else
					++it;
			}
		}

		for (auto it = schedules.begin(); it != schedules.end();)
		{
			auto& schedRec = *it;
			schedRec.m_ScriptSchedule.ReadPostProcess(schedRec);// sad but true
			if (schedRec.m_ScriptSchedule.m_ScriptPresetID == 0)
			{
				p_Script.m_Schedules_Vol.push_back(schedRec.m_ScriptSchedule);
				it = schedules.erase(it);
			}
			else
				++it;
		}
	}

	return rv;
}

bool JobCenterSQLite::LoadAllScripts(std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts)
{
	return LoadScripts(nullptr, p_Session, p_Scripts);
}

bool JobCenterSQLite::LoadScripts(AutomationWizard* p_Wizard, std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts)
{
	return LoadScripts(p_Wizard, p_Session, p_Scripts, false);
}

bool JobCenterSQLite::LoadScripts(AutomationWizard* p_Wizard, std::shared_ptr<const Sapio365Session> p_Session, vector<Script>& p_Scripts, const bool p_ForceSandboxScripts)
{
	return loadScripts(false, p_Wizard, p_Session, p_Scripts, p_ForceSandboxScripts);
}

bool JobCenterSQLite::loadScripts(bool p_ParseMetadata, AutomationWizard* p_Wizard, std::shared_ptr<const Sapio365Session> /*p_Session*/, vector<Script>& p_Scripts, const bool p_ForceSandboxScripts)
{
	bool rvSuccess = false;

	p_Scripts.clear();

	// Nothing encrypted, no session needed finally
	//ASSERT(p_Session);
	//if (p_Session)
	auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != app)
	{
		SQLiteUtil::SelectQuery q;
		q.m_Table = m_Scripts;

		SQLiteUtil::WhereClause whereNotRemoved;
		whereNotRemoved.m_Column	= m_Scripts.GetColumnForQuery(JobCenterUtil::g_ColumnNameStatus);
		whereNotRemoved.m_Operator	= SQLiteUtil::Operator::NotEqual;
		whereNotRemoved.m_Values.push_back(Str::getStringFromNumber<uint32_t>(JobCenterStatus::STATUS_REMOVED));
		q.m_Where.push_back(whereNotRemoved);

		if (nullptr != p_Wizard)
		{
			SQLiteUtil::WhereClause whereModule;
			whereModule.m_Column	= m_Scripts.GetColumnForQuery(JobCenterUtil::g_ColumnNameModule);
			whereModule.m_Operator	= SQLiteUtil::Operator::Equal;
			whereModule.m_Values.push_back(p_Wizard->GetName());
			ASSERT(!whereModule.m_Values.front().empty());
			q.m_Where.push_back(whereModule);

			const auto& wFilters = p_Wizard->GetJobSelectionFilters();
			for (size_t kFilter = 0; kFilter < wFilters.size() ; kFilter++)
			{
				const auto& filter = wFilters[kFilter];
				ASSERT(filter);
				if (filter)
				{
					auto wcFilter = getWC(filter.get());
					
					if (0 == kFilter)
						wcFilter.m_ParenthOpen = true;
					if (wFilters.size() == kFilter + 1)
						wcFilter.m_ParenthClose = true;
					if (kFilter > 0)
						wcFilter.m_And = false;// Achtung: filters are OR'ed
					
					q.m_Where.push_back(wcFilter);
				}
			}
		}

		q.m_OrderBy.push_back(m_Scripts.GetColumnForQuery(JobCenterUtil::g_ColumnNamePosition), true);

		// Nothing encrypted, no session needed finally
		auto krypt = VoidKrypter();//O365Krypter(p_Session);
		vector<ScriptRecord> sr;
		rvSuccess = SelectAll(krypt, q, sr);

		SQLiteUtil::SelectQuery qPresets;
		qPresets.m_Table = m_ScriptPresets;
		
		{
			SQLiteUtil::WhereClause whereNotRemovedPreset;
			whereNotRemovedPreset.m_Column		= m_ScriptPresets.GetColumnForQuery(JobCenterUtil::g_ColumnNameStatus);
			whereNotRemovedPreset.m_Operator	= SQLiteUtil::Operator::NotEqual;
			whereNotRemovedPreset.m_Values.push_back(Str::getStringFromNumber<uint32_t>(JobCenterStatus::STATUS_REMOVED));
			qPresets.m_Where.push_back(whereNotRemovedPreset);

			qPresets.m_OrderBy.push_back(m_ScriptPresets.GetColumnForQuery(JobCenterUtil::g_ColumnNameDate), false);// newest on top
		}		

		SQLiteUtil::SelectQuery qSchedules;
		qSchedules.m_Table = m_ScriptSchedules;

		{
			SQLiteUtil::WhereClause whereNotRemovedSchedule;
			whereNotRemovedSchedule.m_Column	= m_ScriptSchedules.GetColumnForQuery(JobCenterUtil::g_ColumnNameStatus);
			whereNotRemovedSchedule.m_Operator	= SQLiteUtil::Operator::NotEqual;
			whereNotRemovedSchedule.m_Values.push_back(Str::getStringFromNumber<uint32_t>(JobCenterStatus::STATUS_REMOVED));
			qSchedules.m_Where.push_back(whereNotRemovedSchedule);

			qSchedules.m_OrderBy.push_back(m_ScriptSchedules.GetColumnForQuery(JobCenterUtil::g_ColumnNameDate), false);// newest on top
		}

		const bool isSandbox = CRMpipe().IsFeatureSandboxed();

		vector<ScriptPresetRecord>		presets;
		vector<ScriptScheduleRecord>	schedules;
		for (auto& record : sr)
		{
			record.m_Script.ReadPostProcess(record);
			if (!record.m_Script.IsSandbox() || p_ForceSandboxScripts || isSandbox)// me not know do in SQL
			{
				// TODO optim with global query
				presets.clear();
				{
					SQLiteUtil::WhereClause whereScriptID;
					whereScriptID.m_Column		= m_ScriptPresets.GetColumnForQuery(JobCenterUtil::g_ColumnNameScriptID);
					whereScriptID.m_Operator	= SQLiteUtil::Operator::Equal;
					whereScriptID.m_Values.push_back(Str::getStringFromNumber<int64_t>(record.GetID()));
					ASSERT(!whereScriptID.m_Values.front().empty());
					qPresets.m_Where.push_back(whereScriptID);
					SelectAll<ScriptPresetRecord>(krypt, qPresets, presets);
					qPresets.m_Where.pop_back();
				}

				schedules.clear();
				{
					SQLiteUtil::WhereClause whereScriptID;
					whereScriptID.m_Column		= m_ScriptSchedules.GetColumnForQuery(JobCenterUtil::g_ColumnNameScriptID);
					whereScriptID.m_Operator	= SQLiteUtil::Operator::Equal;
					whereScriptID.m_Values.push_back(Str::getStringFromNumber<int64_t>(record.GetID()));
					ASSERT(!whereScriptID.m_Values.front().empty());
					qSchedules.m_Where.push_back(whereScriptID);
					SelectAll<ScriptScheduleRecord>(krypt, qSchedules, schedules);
					qSchedules.m_Where.pop_back();
				}

				for (auto& preset : presets)
				{
					preset.m_ScriptPreset.ReadPostProcess(preset);// sad but true
					record.m_Script.m_Presets_Vol.push_back(preset.m_ScriptPreset);
					auto& scriptPreset = record.m_Script.m_Presets_Vol.back();
					for (auto it = schedules.begin(); it != schedules.end();)
					{
						auto& schedRec = *it;
						schedRec.m_ScriptSchedule.ReadPostProcess(schedRec);// sad but true
						if (scriptPreset.m_ID && schedRec.m_ScriptSchedule.m_ScriptPresetID == scriptPreset.m_ID)
						{
							scriptPreset.m_Schedules_Vol.push_back(schedRec.m_ScriptSchedule);
							it = schedules.erase(it);
						}
						else
						{
							++it;
						}
					}
				}

				p_Scripts.push_back(record.m_Script);
				auto& script = p_Scripts.back();
				for (auto it = schedules.begin(); it != schedules.end();)
				{
					auto& schedRec = *it;
					schedRec.m_ScriptSchedule.ReadPostProcess(schedRec);// sad but true
					if (schedRec.m_ScriptSchedule.m_ScriptPresetID == 0)
					{
						script.m_Schedules_Vol.push_back(schedRec.m_ScriptSchedule);
						it = schedules.erase(it);
					}
					else
					{
						++it;
					}
				}
			}
		}

		if (p_ParseMetadata) // For migration
		{
			ScriptParser().ParseScripts(p_Scripts, false, app->GetAJLJobIDs());
		}
		else
		{
#if _DEBUG
			static bool g_Check = false;
			if (g_Check)
			{
				auto scriptsCopy = p_Scripts;
				ScriptParser().ParseScripts(scriptsCopy, false, app->GetAJLJobIDs());
				for (size_t i = 0; i < scriptsCopy.size(); ++i)
				{
					ASSERT(p_Scripts[i].m_Title == scriptsCopy[i].m_Title);
					ASSERT(p_Scripts[i].m_Description == scriptsCopy[i].m_Description);
					ASSERT(p_Scripts[i].m_Tooltip == scriptsCopy[i].m_Tooltip);
					ASSERT(p_Scripts[i].m_Requirements == scriptsCopy[i].m_Requirements);
					//ASSERT(p_Scripts[i].m_IconType_vol == scriptsCopy[i].m_IconType_vol); // Useless
					ASSERT(p_Scripts[i].m_Version == scriptsCopy[i].m_Version);
					ASSERT(p_Scripts[i].m_CommonRestrictions == scriptsCopy[i].m_CommonRestrictions);
					ASSERT(p_Scripts[i].m_LibraryID == scriptsCopy[i].m_LibraryID);
					ASSERT(p_Scripts[i].m_Keywords == scriptsCopy[i].m_Keywords);
					ASSERT(p_Scripts[i].m_Categories == scriptsCopy[i].m_Categories);
					ASSERT(p_Scripts[i].m_Family == scriptsCopy[i].m_Family);
					ASSERT(p_Scripts[i].m_Action == scriptsCopy[i].m_Action);
					ASSERT(p_Scripts[i].m_IsNew == scriptsCopy[i].m_IsNew);
					ASSERT(p_Scripts[i].m_Created == scriptsCopy[i].m_Created);
					ASSERT(p_Scripts[i].m_LastEdit == scriptsCopy[i].m_LastEdit);
					ASSERT(p_Scripts[i].IsLib() == scriptsCopy[i].IsLib());
					ASSERT(p_Scripts[i].IsSandbox() == scriptsCopy[i].IsSandbox());
					ASSERT(p_Scripts[i].IsPositionLocked() == scriptsCopy[i].IsPositionLocked());
				}
			}
#endif
		}
	}

	return rvSuccess;
}

SQLiteUtil::WhereClause JobCenterSQLite::getWC(const JobSelectionFilter* p_Filter)
{
	SQLiteUtil::WhereClause wc;

	ASSERT(nullptr != p_Filter);
	if (nullptr != p_Filter)
	{
		switch (p_Filter->GetType())
		{
		case JobSelectionType::Common:
			wc.m_Column		= m_Scripts.GetColumnForQuery(JobCenterUtil::g_ColumnNameCommon);
			wc.m_Operator	= SQLiteUtil::Operator::Equal;
			wc.m_Values.push_back(p_Filter->GetValue());
			break;
		case JobSelectionType::KeyContains:
			wc.m_Column		= m_Scripts.GetColumnForQuery(JobCenterUtil::g_ColumnNameKey);
			wc.m_Operator	= SQLiteUtil::Operator::Like;
			wc.m_Values.push_back(_YTEXT("%") + p_Filter->GetValue() + _YTEXT("%"));
			break;
		case JobSelectionType::KeyEquals:
			wc.m_Column		= m_Scripts.GetColumnForQuery(JobCenterUtil::g_ColumnNameKey);
			wc.m_Operator	= SQLiteUtil::Operator::Equal;
			wc.m_Values.push_back(p_Filter->GetValue());
			break;
		default:
			ASSERT(false);
		}
	}

	return wc;
}

bool JobCenterSQLite::GetScript(const wstring& p_ScriptKey, std::shared_ptr<const Sapio365Session> /*p_Session*/, Script& p_Script)
{
	ScriptRecord script(m_Scripts);
	// Nothing encrypted, no session needed finally
	if (Select(VoidKrypter()/*O365Krypter(p_Session)*/, &script, JobCenterUtil::g_ColumnNameKey, p_ScriptKey, JobCenterUtil::g_ColumnNameStatus, static_cast<int64_t>(JobCenterStatus::STATUS_REMOVED)))
	{
		script.m_Script.ReadPostProcess(script);// sad but true
		p_Script = script.m_Script;
		auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
		{
#if _DEBUG
			static bool g_Check = false;
			if (g_Check)
			{
				vector<Script> scriptsCopy = { script.m_Script };
				ScriptParser().ParseScripts(scriptsCopy, false, app->GetAJLJobIDs());
				ASSERT(p_Script.m_Title == scriptsCopy[0].m_Title);
				ASSERT(p_Script.m_Description == scriptsCopy[0].m_Description);
				ASSERT(p_Script.m_Tooltip == scriptsCopy[0].m_Tooltip);
				ASSERT(p_Script.m_Requirements == scriptsCopy[0].m_Requirements);
				//ASSERT(p_Script.m_IconType_vol == scriptsCopy[0].m_IconType_vol); // Useless
				ASSERT(p_Script.m_Version == scriptsCopy[0].m_Version);
				ASSERT(p_Script.m_CommonRestrictions == scriptsCopy[0].m_CommonRestrictions);
				ASSERT(p_Script.m_LibraryID == scriptsCopy[0].m_LibraryID);
				ASSERT(p_Script.m_Keywords == scriptsCopy[0].m_Keywords);
				ASSERT(p_Script.m_Categories == scriptsCopy[0].m_Categories);
				ASSERT(p_Script.m_Family == scriptsCopy[0].m_Family);
				ASSERT(p_Script.m_Action == scriptsCopy[0].m_Action);
				ASSERT(p_Script.m_IsNew == scriptsCopy[0].m_IsNew);
				ASSERT(p_Script.m_Created == scriptsCopy[0].m_Created);
				ASSERT(p_Script.m_LastEdit == scriptsCopy[0].m_LastEdit);
				ASSERT(p_Script.IsLib() == scriptsCopy[0].IsLib());
				ASSERT(p_Script.IsSandbox() == scriptsCopy[0].IsSandbox());
				ASSERT(p_Script.IsPositionLocked() == scriptsCopy[0].IsPositionLocked());
			}
#endif
			return true;
		}
	}
	return false;
}

bool JobCenterSQLite::GetScriptPreset(const wstring& p_ScriptPresetKey, std::shared_ptr<const Sapio365Session> /*p_Session*/, ScriptPreset& p_ScriptPreset)
{
	ScriptPresetRecord scriptPreset(m_ScriptPresets);
	// Nothing encrypted, no session needed finally
	if (Select(VoidKrypter()/*O365Krypter(p_Session)*/, &scriptPreset, JobCenterUtil::g_ColumnNameKey, p_ScriptPresetKey, JobCenterUtil::g_ColumnNameStatus, static_cast<int64_t>(JobCenterStatus::STATUS_REMOVED)))
	{
		scriptPreset.m_ScriptPreset.ReadPostProcess(scriptPreset);// sad but true
		p_ScriptPreset = scriptPreset.m_ScriptPreset;
		return true;
	}
	return false;
}

bool JobCenterSQLite::GetScriptSchedule(const wstring& p_ScriptScheduleKey, std::shared_ptr<const Sapio365Session> /*p_Session*/, ScriptSchedule& p_ScriptSchedule)
{
	ScriptScheduleRecord scriptSchedule(m_ScriptSchedules);
	// Nothing encrypted, no session needed finally
	if (Select(VoidKrypter()/*O365Krypter(p_Session)*/, &scriptSchedule, JobCenterUtil::g_ColumnNameKey, p_ScriptScheduleKey, JobCenterUtil::g_ColumnNameStatus, static_cast<int64_t>(JobCenterStatus::STATUS_REMOVED)))
	{
		scriptSchedule.m_ScriptSchedule.ReadPostProcess(scriptSchedule);// sad but true
		p_ScriptSchedule = scriptSchedule.m_ScriptSchedule;
		return true;
	}
	return false;
}

uint32_t JobCenterSQLite::GetScriptCount(const wstring& p_Module, std::shared_ptr<const Sapio365Session> /*p_Session*/)
{
	// Nothing encrypted, no session needed finally
	auto count = SelectCount(VoidKrypter()/*O365Krypter(p_Session)*/, m_Scripts, JobCenterUtil::g_ColumnNameModule, p_Module, JobCenterUtil::g_ColumnNameStatus, JobCenterStatus::STATUS_REMOVED);
	if (count)
		return count.get();
	return 0;
}

uint32_t JobCenterSQLite::GetScriptPresetCount(const int64_t& p_ScriptID, const bool p_WithRemoved, std::shared_ptr<const Sapio365Session> /*p_Session*/)
{
	// Nothing encrypted, no session needed finally
	auto count = p_WithRemoved ?	SelectCount(VoidKrypter()/*O365Krypter(p_Session)*/, m_ScriptPresets, JobCenterUtil::g_ColumnNameScriptID, Str::getStringFromNumber<int64_t>(p_ScriptID)):
									SelectCount(VoidKrypter()/*O365Krypter(p_Session)*/, m_ScriptPresets, JobCenterUtil::g_ColumnNameScriptID, Str::getStringFromNumber<int64_t>(p_ScriptID), JobCenterUtil::g_ColumnNameStatus, JobCenterStatus::STATUS_REMOVED);
	if (count)
		return count.get();
	return 0;
}

bool JobCenterSQLite::HasScript(const wstring& p_ScriptKey, std::shared_ptr<const Sapio365Session> /*p_Session*/)
{
	// Nothing encrypted, no session needed finally
	auto count = SelectCount(VoidKrypter()/*O365Krypter(p_Session)*/, m_Scripts, JobCenterUtil::g_ColumnNameKey, p_ScriptKey, JobCenterUtil::g_ColumnNameStatus, JobCenterStatus::STATUS_REMOVED);
	return count && count.get() > 0;
}

bool JobCenterSQLite::HasScriptPreset(const wstring& p_ScriptKey, std::shared_ptr<const Sapio365Session> /*p_Session*/)
{
	// Nothing encrypted, no session needed finally
	auto count = SelectCount(VoidKrypter()/*O365Krypter(p_Session)*/, m_ScriptPresets, JobCenterUtil::g_ColumnNameKey, p_ScriptKey, JobCenterUtil::g_ColumnNameStatus, JobCenterStatus::STATUS_REMOVED);
	return count && count.get() > 0;
}

ScriptRecord JobCenterSQLite::Read(const int64_t p_ID, std::shared_ptr<const Sapio365Session> /*p_Session*/)
{
	ScriptRecord script(m_Scripts);
	if (Select(VoidKrypter()/*O365Krypter(p_Session)*/, &script, p_ID, JobCenterUtil::g_ColumnNameStatus, JobCenterStatus::STATUS_REMOVED))
		script.m_Script.ReadPostProcess(script);// sad but true
	return script;
}

ScriptRecord JobCenterSQLite::Write(const Script& p_Script, std::shared_ptr<const Sapio365Session> p_Session)
{
	ASSERT(!p_Script.m_Key.empty());

	Script s;
	GetScript(p_Script.m_Key, p_Session, s);

	ScriptRecord scriptRecord = s.HasID() ? Read(*s.m_ID, p_Session) : ScriptRecord(m_Scripts);
	scriptRecord.m_Script = p_Script;
	scriptRecord.SetID(s.m_ID);// sad but true
	WriteScriptRecord(&scriptRecord, p_Session);
	scriptRecord.m_Script.m_ID = scriptRecord.GetID();// sad but true again
	return scriptRecord;
}

ScriptPresetRecord JobCenterSQLite::ReadPreset(const int64_t p_ID, std::shared_ptr<const Sapio365Session> /*p_Session*/)
{
	ScriptPresetRecord scriptPreset(m_ScriptPresets);
	// Nothing encrypted, no session needed finally
	if (Select(VoidKrypter()/*O365Krypter(p_Session)*/, &scriptPreset, p_ID, JobCenterUtil::g_ColumnNameStatus, JobCenterStatus::STATUS_REMOVED))
		scriptPreset.m_ScriptPreset.ReadPostProcess(scriptPreset);// sad but true
	return scriptPreset;
}

ScriptPresetRecord JobCenterSQLite::Write(const ScriptPreset& p_ScriptPreset, std::shared_ptr<const Sapio365Session> p_Session)
{
	ASSERT(!p_ScriptPreset.m_Key.empty());

	ScriptPreset s;
	GetScriptPreset(p_ScriptPreset.m_Key, p_Session, s);

	ScriptPresetRecord scriptPresetRecord = s.HasID() ? ReadPreset(*s.m_ID, p_Session) : ScriptPresetRecord(m_ScriptPresets);
	scriptPresetRecord.m_ScriptPreset = p_ScriptPreset;
	scriptPresetRecord.SetID(s.m_ID);// sad but true
	WriteScriptPresetRecord(&scriptPresetRecord, p_Session);
	scriptPresetRecord.m_ScriptPreset.m_ID = scriptPresetRecord.GetID();// sad but true again
	return scriptPresetRecord;
}

ScriptScheduleRecord JobCenterSQLite::ReadSchedule(const int64_t p_ID, std::shared_ptr<const Sapio365Session> /*p_Session*/)
{
	ScriptScheduleRecord scriptSchedule(m_ScriptSchedules);
	// Nothing encrypted, no session needed finally
	if (Select(VoidKrypter()/*O365Krypter(p_Session)*/, &scriptSchedule, p_ID, JobCenterUtil::g_ColumnNameStatus, JobCenterStatus::STATUS_REMOVED))
		scriptSchedule.m_ScriptSchedule.ReadPostProcess(scriptSchedule);// sad but true
	return scriptSchedule;
}

ScriptScheduleRecord JobCenterSQLite::Write(const ScriptSchedule& p_ScriptSchedule, std::shared_ptr<const Sapio365Session> p_Session)
{
	ASSERT(!p_ScriptSchedule.m_Key.empty());

	ScriptSchedule s;
	GetScriptSchedule(p_ScriptSchedule.m_Key, p_Session, s);

	ScriptScheduleRecord scriptScheduleRecord = s.HasID() ? ReadSchedule(*s.m_ID, p_Session) : ScriptScheduleRecord(m_ScriptSchedules);
	scriptScheduleRecord.m_ScriptSchedule = p_ScriptSchedule;
	scriptScheduleRecord.SetID(s.m_ID);// sad but true
	WriteScriptScheduleRecord(&scriptScheduleRecord, p_Session);
	scriptScheduleRecord.m_ScriptSchedule.m_ID = scriptScheduleRecord.GetID();// sad but true again
	return scriptScheduleRecord;
}

bool JobCenterSQLite::WriteScriptRecord(ScriptRecord* p_Arecord, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	ASSERT(nullptr != p_Arecord);
	if (nullptr != p_Arecord)
		if (/*p_Arecord->IsForCloudStorageOnly() || */!p_Arecord->m_Script.m_LogUpdate_vol || prepareUpdateInfo<Script>(p_Arecord->m_Script, p_Session))
		{
			// Nothing encrypted, no session needed finally
			rvSuccess = WriteRecord(VoidKrypter()/*O365Krypter(p_Session)*/, p_Arecord);
		}

	return rvSuccess;
}

bool JobCenterSQLite::WriteScriptPresetRecord(ScriptPresetRecord* p_Arecord, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	ASSERT(nullptr != p_Arecord);
	if (nullptr != p_Arecord)
		if (/*p_Arecord->IsForCloudStorageOnly() || */prepareUpdateInfo<ScriptPreset>(p_Arecord->m_ScriptPreset, p_Session))
		{
			// Nothing encrypted, no session needed finally
			rvSuccess = WriteRecord(VoidKrypter()/*O365Krypter(p_Session)*/, p_Arecord);
		}

	return rvSuccess;
}

bool JobCenterSQLite::WriteScriptScheduleRecord(ScriptScheduleRecord* p_Arecord, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	ASSERT(nullptr != p_Arecord);
	if (nullptr != p_Arecord)
		if (/*p_Arecord->IsForCloudStorageOnly() || */prepareUpdateInfo<ScriptSchedule>(p_Arecord->m_ScriptSchedule, p_Session))
		{
			// Nothing encrypted, no session needed finally
			rvSuccess = WriteRecord(VoidKrypter()/*O365Krypter(p_Session)*/, p_Arecord);
		}

	return rvSuccess;
}

template <class T>
bool JobCenterSQLite::prepareUpdateInfo(T& p_ScriptItem, std::shared_ptr<const Sapio365Session> p_Session) const
{
	bool rvSuccess = false;

	ASSERT(nullptr != p_Session);
	if (nullptr != p_Session)
	{
		// Sapio365SessionSavedInfo might do the trick too
		const auto& user	= p_Session->GetGraphCache().GetCachedConnectedUser();
		const auto isUA		= p_Session->IsUltraAdmin();
		ASSERT(isUA || user.is_initialized());

		p_ScriptItem.m_UpdDate					= YTimeDate::GetCurrentTimeDate();
		p_ScriptItem.m_UpdateByID				= isUA ? Sapio365Session::GetUltraAdminID() : user.is_initialized() ? user->GetID() : _YTEXT("N/A");
		p_ScriptItem.m_UpdateByName				= isUA ? YtriaTranslate::Do(GridSessionsBackstage_BuildView_3, _YLOC("Ultra Admin")).c_str() : user.is_initialized() && user->GetDisplayName().is_initialized() ? user->GetDisplayName().get() : _YTEXT("N/A");
		p_ScriptItem.m_UpdateByPrincipalName	= isUA ? YtriaTranslate::Do(GridSessionsBackstage_BuildView_3, _YLOC("Ultra Admin")).c_str() : user.is_initialized() && user->GetUserPrincipalName().is_initialized() ? user->GetUserPrincipalName().get() : _YTEXT("N/A");

		rvSuccess = true;
	}

	return rvSuccess;
}

namespace
{
	int emptyCallback(void* count, int argc, char** argv, char** colName)
	{
		auto* c = reinterpret_cast<int*>(count);
		if (argc == 1 && argv != nullptr)
			*c = atoi(argv[0]);
		return 0;
	}
}

bool JobCenterSQLite::IsEmpty(std::shared_ptr<const Sapio365Session> p_Session) const
{
	int count = 0;

	const auto query = _YTEXTFORMAT(L"SELECT 1 FROM \"%s\" limit 1", m_Scripts.m_Name.c_str());
	const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(query);
	char* errMsg = nullptr;
	const auto result = sqlite3_exec(queryAscii.c_str(), emptyCallback, &count, &errMsg);

	ProcessError(result, errMsg, query);

	return count < 1;
}

wstring JobCenterSQLite::getVersionedFilename(int p_Version)
{
	static const wstring filenameTemplate = _YTEXT("_jcvd%s_.ytr");
	CString filename;
	filename.Format(filenameTemplate.c_str(), Str::getStringFromNumber<uint32_t>(p_Version).c_str());
	return wstring((LPCTSTR)filename);
}

wstring JobCenterSQLite::getFilenameAndMigrateIfNecessary()
{
	const auto fileName = getVersionedFilename(g_CurrentVersion);
	const auto folder = SQLiteEngine::getLocalAppDataDBFileFolder();

	if (!FileUtil::FileExists(folder + fileName))
	{
		int versionToMigrateFrom = g_CurrentVersion;
		for (int v = versionToMigrateFrom; v >= 0; --v)
		{
			if (FileUtil::FileExists(folder + getVersionedFilename(v)))
			{
				versionToMigrateFrom = v;
				break;
			}
		}

		for (int v = versionToMigrateFrom; v < g_CurrentVersion; ++v)
		{
			switch (v)
			{
			case 0:
				MigrateDatabase<0, 8>()(); // Migration didn't exist before v7.
				v = 7; // We already migrated everything up to 8 (like we were coming from 7)
				break;
			case 1:
				MigrateDatabase<1, 8>()(); // Migration didn't exist before v7.
				v = 7; // We already migrated everything up to 8 (like we were coming from 7)
				break;
			case 2:
				MigrateDatabase<2, 8>()(); // Migration didn't exist before v7.
				v = 7; // We already migrated everything up to 8 (like we were coming from 7)
				break;
			case 3:
				MigrateDatabase<3, 8>()(); // Migration didn't exist before v7.
				v = 7; // We already migrated everything up to 8 (like we were coming from 7)
				break;
			case 4:
				MigrateDatabase<4, 8>()(); // Migration didn't exist before v7.
				v = 7; // We already migrated everything up to 8 (like we were coming from 7)
				break;
			case 5:
				MigrateDatabase<5, 8>()(); // Migration didn't exist before v7.
				v = 7; // We already migrated everything up to 8 (like we were coming from 7)
				break;
			case 6:
				MigrateDatabase<6, 8>()(); // Migration didn't exist before v7.
				v = 7; // We already migrated everything up to 8 (like we were coming from 7)
				break;
			case 7:
				MigrateDatabase<7, 8>()();
				break;
			default:
				ASSERT(false);
			}
		}
	}

	return fileName;
}

template<int vFrom, int vTo>
void JobCenterSQLite::MigrateDatabase<vFrom, vTo>::operator()()
{
	const auto folder = SQLiteEngine::getLocalAppDataDBFileFolder();
	ASSERT(FileUtil::FileExists(folder + getVersionedFilename(vFrom)));

	const auto fromFilePath = folder + getVersionedFilename(vFrom);
	const auto toFilename = getVersionedFilename(vTo);
	const auto toFilePath = folder + toFilename;
	const auto err = FileUtil::ProcessFile({ fromFilePath }, { toFilePath }, FO_COPY, FOF_NO_UI);

	if (0 != err)
	{
		TCHAR buff[256] = _YTEXT("\0");
		::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, // It's a system error
			nullptr,
			err,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Do it in the standard language
			buff,
			sizeof(buff) / sizeof(TCHAR) - 1,
			nullptr);

		LoggerService::Debug(_YDUMPFORMAT(L"Unable to move Job Center DB file from %s to %s.", fromFilePath.c_str(), toFilePath.c_str()));
		LoggerService::Debug(buff);

		YCodeJockMessageBox dlg(nullptr
			, DlgMessageBox::eIcon_Error
			, _T("Error")
			, _YFORMATERROR(L"Unable to move Job Center DB file from %s to %s.", fromFilePath.c_str(), toFilePath.c_str())
			, buff
			, { {IDOK, _T("OK")} }
		);
		dlg.DoModal();
	}
}

template<int vFrom>
void JobCenterSQLite::MigrateDatabase<vFrom, 8>::operator()()
{
	const int vTo = 8;

	const auto folder = SQLiteEngine::getLocalAppDataDBFileFolder();
	ASSERT(FileUtil::FileExists(folder + getVersionedFilename(vFrom)));

	const auto fromFilePath = folder + getVersionedFilename(vFrom);
	const auto toFilename = getVersionedFilename(vTo);
	const auto toFilePath = folder + toFilename;
	const auto err = FileUtil::ProcessFile({ fromFilePath }, { toFilePath }, FO_COPY, FOF_NO_UI);

	if (0 != err)
	{
		TCHAR buff[256] = _YTEXT("\0");
		::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, // It's a system error
			nullptr,
			err,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Do it in the standard language
			buff,
			sizeof(buff) / sizeof(TCHAR) - 1,
			nullptr);

		LoggerService::Debug(_YDUMPFORMAT(L"Unable to copy Job Center DB file from %s to %s.", fromFilePath.c_str(), toFilePath.c_str()));
		LoggerService::Debug(buff);

		YCodeJockMessageBox dlg(nullptr
			, DlgMessageBox::eIcon_Error
			, _T("Error")
			, _YFORMATERROR(L"Unable to copy Job Center DB file from %s to %s.", fromFilePath.c_str(), toFilePath.c_str())
			, buff
			, { {IDOK, _T("OK")} }
		);
		dlg.DoModal();
	}
	else
	{
		JobCenterSQLite engine(toFilename);

		// Specific case, v7 has only existed one day internally.
		if (7 != vFrom)
		{
			// Add new columns
			{
				vector<wstring> newColumns
				{
					JobCenterUtil::g_ColumnNameScriptLibraryID,
					JobCenterUtil::g_ColumnNameKeywords,
					JobCenterUtil::g_ColumnNameCategories,
					JobCenterUtil::g_ColumnNameFamily,
					JobCenterUtil::g_ColumnNameAction,
					JobCenterUtil::g_ColumnNameIsNew,
					JobCenterUtil::g_ColumnNameCreated,
					JobCenterUtil::g_ColumnNameLastEdit,
					JobCenterUtil::g_ColumnNameScriptFlags,
				};

				std::vector<wstring> statements;
				statements.reserve(newColumns.size());
				for (const auto& nc : newColumns)
					statements.push_back(_YTEXTFORMAT(L"ALTER TABLE Scripts ADD COLUMN %s TEXT;", nc.c_str()));

				SqlQueryMultiPreparedStatement query(engine, statements);
				query.Run();

				auto success = query.IsSuccess();
				ASSERT(success);
			}
		}

		// Populate new columns
		{
			vector<Script> scripts;
			engine.loadScripts(7 != vFrom, nullptr, nullptr, scripts, true);
			engine.StartTransaction();
			for (auto& s : scripts)
			{
				s.m_LogUpdate_vol = false;
				// MainFrame module case has been messed up recently, let's ensure it's not.
				if (MFCUtil::StringMatchW(s.m_Module, JobCenterUtil::g_WizardNameMainFrame))
				{
					ASSERT(Str::beginsWith(s.m_Key, s.m_Module));
					if (Str::beginsWith(s.m_Key, s.m_Module))
						s.m_Key = JobCenterUtil::g_WizardNameMainFrame + s.m_Key.substr(s.m_Module.size());
					s.m_Module = JobCenterUtil::g_WizardNameMainFrame;
				}				
				engine.Write(s, nullptr);
			}
			engine.EndTransaction();
		}
	}
}
