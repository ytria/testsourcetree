#pragma once

#include "DlgFormsHTML.h"
#include "JobCenterUtil.h"

class DlgJobPresetEdit : public DlgFormsHTML
{
public:
	DlgJobPresetEdit(const Script& p_Script, CWnd* p_Parent);
	DlgJobPresetEdit(const Script& p_Script, const ScriptPreset& p_Preset, CWnd* p_Parent);

	ScriptPreset& GetPreset();

protected:
	virtual void generateJSONScriptData() override;

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;
	virtual wstring getDialogTitle() const override;

private:
	const Script& m_Script;

	static wstring g_VarNameTitle;
	static wstring g_VarNameDescription;

	ScriptPreset m_Preset;
};
