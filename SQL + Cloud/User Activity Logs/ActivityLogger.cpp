#include "ActivityLogger.h"

#include "AnnotationUtil.h"
#include "BaseO365Grid.h"
#include "FileUtil.h"
#include "GridFrameBase.h"
#include "Sapio365Session.h"
#include "TraceIntoFile.h"

ActivityLogger::ActivityLogger(): ManagerSQLandCloud(),
	m_MustClearGrids(true)
{
}

// static
ActivityLogger& ActivityLogger::GetInstance()
{
	static ActivityLogger g_Instance;
	return g_Instance;
}

void ActivityLogger::InitAfterSessionOrLicenseWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress)
{
	ClearGrids();

	ASSERT(p_Session);
	if (IsReadyToRun() && p_Session)
	{
		// Bug #190417.RO.009D4A: Using a task here leads to crash because NalpeironLicenseManager::LoadLicense is not thread-safe, whereas
		// AnnotationManager::GetInstance().InitAfterSessionOrLicenseWasSet is calling it concurrently... 

		if (m_LoggerCosmos.Configure(m_LoggerSQLite, p_Session))
		{
			CosmosUtil::SQLupdateData cosmosDocuments;
			if (m_LoggerCosmos.SelectAll(cosmosDocuments, p_Session, true))// fetch from Cosmos
			{
				// TODO pull in backup DB for admin
				pushSQLiteToCloud(p_Session, cosmosDocuments, p_Progress);
			}
		}
			
		if (m_LoggerCosmos.GetLastError() == CosmosSQLiteBridge::COSMOS_NOERROR ||
			m_LoggerCosmos.GetLastError() == CosmosSQLiteBridge::SESSION_NOTREADYFORCOSMOS) // allowed to run on local machine
			SetError(CFG_NOERROR);
	}
}

bool ActivityLogger::pushSQLiteToCloud(std::shared_ptr<const Sapio365Session> p_Session, const CosmosUtil::SQLupdateData& p_CosmosDocuments, DlgDoubleProgressCommon& p_Progress)
{
	bool rvUpdate = false;

	if (IsReadyToRun())
	{
		// fetch from SQLite - push to COSMOS
		ActivityLogUtil::CosmosUpdateData SQLdata;
		if (m_LoggerSQLite.SelectAllForUpdateToCloud(SQLdata, p_Session))
			rvUpdate = m_LoggerCosmos.UpdateFromSQLite(SQLdata, p_CosmosDocuments, p_Session, p_Progress);
		else
		{
			TraceIntoFile::trace(_YDUMP("RoleDelegationManager"), _YDUMP("pushSQLiteToCloud"),
								 _YTEXTFORMAT(L"Unable to load Logs to push to %s (tenant %s)", p_Session->GetCosmosDbSqlUri().c_str(), p_Session->GetTenantName().c_str()));
			YCallbackMessage::DoSend([]()
			{
				YCodeJockMessageBox dlg(AfxGetApp()->m_pMainWnd,
										DlgMessageBox::eIcon_Error,
										YtriaTranslate::DoError(DlgError_DlgError_1, _YLOC("Error"),_YR("Y2421")).c_str(),
										YtriaTranslate::Do(ActivityLogger_pushSQLiteToCloud_2, _YLOC("Unable to update Logs to cloud")).c_str(),
										_YTEXT(""),
										{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("Ok")).c_str() } });
				dlg.DoModal();
			});
		}

		m_LoggerSQLite.CompleteUpdateToCloud(p_Session);
	}

	return rvUpdate;
}

void ActivityLogger::ClearGrids()
{
	m_MustClearGrids = true;
}

bool ActivityLogger::MustClearGrids() const
{
	return m_MustClearGrids;
}

void ActivityLogger::GridsCleared()
{
	m_MustClearGrids = false;
}

void ActivityLogger::LogGridModifications(const bool p_MainModifications, const GridModificationsO365& p_Modifications)
{
	const O365Grid& grid		= p_Modifications.GetGrid();
	GridFrameBase* parentFrame	= dynamic_cast<GridFrameBase*>(grid.GetParent());

	std::shared_ptr<Sapio365Session> session365 = [parentFrame]
	{
		if (nullptr != parentFrame)
			return parentFrame->GetSapio365Session();
		else
		{
			const MainFrame* mf = dynamic_cast<const MainFrame*>(AfxGetApp()->m_pMainWnd);
			ASSERT(nullptr != mf);
			return mf->GetSapio365Session();
		}
	}();
	
	if (session365)
	{
		// local log
		SQLiteUtil::InsertDataCollection valuesForCloudStorage;
		m_LoggerSQLite.LogGridModifications(session365, p_MainModifications, p_Modifications, valuesForCloudStorage);

		// cloud log
		m_LoggerCosmos.CosmosSQLiteBridge::Write(valuesForCloudStorage, m_LoggerSQLite.GetActivityTable(), session365);
	}
}

void ActivityLogger::LogGridModifications(const bool p_MainModifications, const GridFieldModificationsOnPrem& p_Modifications)
{
	const O365Grid& grid = p_Modifications.GetGrid();
	GridFrameBase* parentFrame = dynamic_cast<GridFrameBase*>(grid.GetParent());

	std::shared_ptr<Sapio365Session> session365 = [parentFrame]
	{
		if (nullptr != parentFrame)
			return parentFrame->GetSapio365Session();
		else
		{
			const MainFrame* mf = dynamic_cast<const MainFrame*>(AfxGetApp()->m_pMainWnd);
			ASSERT(nullptr != mf);
			return mf->GetSapio365Session();
		}
	}();

	if (session365)
	{
		// local log
		SQLiteUtil::InsertDataCollection valuesForCloudStorage;
		m_LoggerSQLite.LogGridModifications(session365, p_MainModifications, p_Modifications, valuesForCloudStorage);

		// cloud log
		m_LoggerCosmos.CosmosSQLiteBridge::Write(valuesForCloudStorage, m_LoggerSQLite.GetActivityTable(), session365);
	}
}

void ActivityLogger::LogRBACModifications(const RoleDelegationModification& p_Modification)
{
	std::shared_ptr<Sapio365Session> session365 = []
	{
		const MainFrame* mf = dynamic_cast<const MainFrame*>(AfxGetApp()->m_pMainWnd);
		ASSERT(nullptr != mf);
		return mf->GetSapio365Session();
	}();
	
	// local log
	SQLiteUtil::InsertDataCollection valuesForCloudStorage;
	m_LoggerSQLite.LogRBACModifications(session365, p_Modification, valuesForCloudStorage);
	
	// cloud log
	m_LoggerCosmos.CosmosSQLiteBridge::Write(valuesForCloudStorage, m_LoggerSQLite.GetActivityTable(), session365);
}

void ActivityLogger::LogAnnotationModifications(const AnnotationModification& p_Modification, std::shared_ptr<const Sapio365Session> p_Session)
{
	YSafeCreateTask([this, p_Modification, p_Session]() // perf
	{
		// local log
		SQLiteUtil::InsertDataCollection valuesForCloudStorage;
		m_LoggerSQLite.LogAnnotationModifications(p_Session, p_Modification, valuesForCloudStorage);

		// cloud log
		m_LoggerCosmos.CosmosSQLiteBridge::Write(valuesForCloudStorage, m_LoggerSQLite.GetActivityTable(), p_Session);
	});
}

void ActivityLogger::LogBasicAction(const wstring& p_Action, const wstring& p_ActionStatus, const wstring& p_ModuleName, std::shared_ptr<const Sapio365Session> p_Session)
{
	YSafeCreateTask([this, p_Action, p_ActionStatus, p_ModuleName, p_Session]() // perf
	{
		// local log
		SQLiteUtil::InsertDataCollection valuesForCloudStorage;
		m_LoggerSQLite.LogBasicAction(p_Session, p_Action, p_ActionStatus, p_ModuleName, valuesForCloudStorage);

		// cloud log
		m_LoggerCosmos.CosmosSQLiteBridge::Write(valuesForCloudStorage, m_LoggerSQLite.GetActivityTable(), p_Session);
	});
}