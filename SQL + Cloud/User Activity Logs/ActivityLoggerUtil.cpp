#include "ActivityLoggerUtil.h"

const wstring ActivityLoggerUtil::g_ActionCreate						= _YTEXT("Create");
const wstring ActivityLoggerUtil::g_ActionUpdate						= _YTEXT("Update");
const wstring ActivityLoggerUtil::g_ActionDelete						= _YTEXT("Delete");
const wstring ActivityLoggerUtil::g_ActionRestore						= _YTEXT("Restore");// maybe not