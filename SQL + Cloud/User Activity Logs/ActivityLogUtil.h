#pragma once

#include "SQLiteUtil.h"
#include "YTimeDate.h"

namespace ActivityLogUtil
{
	const wstring g_ColumnNameDate				= _YTEXT("Date");
	const wstring g_ColumnNameTenant			= _YTEXT("Tenant");
	const wstring g_ColumnNameTenantDisplayName	= _YTEXT("Tenant Display Name");
	const wstring g_ColumnNameUserName			= _YTEXT("User Name");
	const wstring g_ColumnNameEmail				= _YTEXT("Email");
	const wstring g_ColumnNameLicenseCode		= _YTEXT("License Code");
	const wstring g_ColumnNameDeviceID			= _YTEXT("Device ID");
	const wstring g_ColumnNameSugarREF			= _YTEXT("REF");
	const wstring g_ColumnNameFullAdmin			= _YTEXT("Full Admin");
	const wstring g_ColumnNameModuleName		= _YTEXT("Module");
	const wstring g_ColumnNameAction			= _YTEXT("Action");
	const wstring g_ColumnNameActionStatus		= _YTEXT("Action Status");
	const wstring g_ColumnNameObjectKey			= _YTEXT("Key");
	const wstring g_ColumnNameObjectName		= _YTEXT("Name");
	const wstring g_ColumnNameProperty			= _YTEXT("Property");
	//const wstring g_ColumnNameGraphID			= _YTEXT("Graph ID");
	const wstring g_ColumnNameOldValue			= _YTEXT("Old Value");
	const wstring g_ColumnNameNewValue			= _YTEXT("New Value");
	// TODO object type
}

class ActivityLog
{
public:
	boost::YOpt<int64_t> m_ID;// sad but true

	YTimeDate	m_Date;
	wstring		m_Tenant;
	wstring		m_TenantDisplay;
	wstring		m_UserName;
	wstring		m_UserEmail;
	wstring		m_LicenseCode;
	wstring		m_DeviceID;
	wstring		m_SugarREF;
	uint32_t	m_FullAdmin;
	wstring		m_Action;
	wstring		m_Module;
	wstring		m_ActionStatus;
	wstring		m_ObjectPK;
	wstring		m_ObjectName;
	wstring		m_Property;
	wstring		m_ObjectOldValue;
	wstring		m_ObjectNewValue;

	bool HasID() const { return m_ID.is_initialized() && m_ID.get() > 0; }

	void ReadPostProcess(const SQLiteUtil::SQLRecord& p_SQL) 
	{
		m_ID = p_SQL.GetIDOpt();
	}
};

class ActivityLogRecord : public SQLiteUtil::SQLRecord
{
public:
	ActivityLogRecord(const SQLiteUtil::Table& p_Table) : SQLiteUtil::SQLRecord(p_Table)
	{
		Configure();
	}

	void Configure() override
	{
		configureMember(SQLiteUtil::g_ColumnNameRowID,					&m_ID,						SQLRecord::MemberType::OPTINT64);
		configureMember(ActivityLogUtil::g_ColumnNameDate,				&m_Log.m_Date,				SQLRecord::MemberType::DATE);
		configureMember(ActivityLogUtil::g_ColumnNameTenant,			&m_Log.m_Tenant,			SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameTenantDisplayName,	&m_Log.m_TenantDisplay,		SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameUserName,			&m_Log.m_UserName,			SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameEmail,				&m_Log.m_UserEmail,			SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameLicenseCode,		&m_Log.m_LicenseCode,		SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameDeviceID,			&m_Log.m_DeviceID,			SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameSugarREF,			&m_Log.m_SugarREF,			SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameFullAdmin,			&m_Log.m_FullAdmin,			SQLRecord::MemberType::UINT32);
		configureMember(ActivityLogUtil::g_ColumnNameModuleName,		&m_Log.m_Module,			SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameAction,			&m_Log.m_Action,			SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameActionStatus,		&m_Log.m_ActionStatus,		SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameObjectKey,			&m_Log.m_ObjectPK,			SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameObjectName,		&m_Log.m_ObjectName,		SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameProperty,			&m_Log.m_Property,			SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameOldValue,			&m_Log.m_ObjectNewValue,	SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameNewValue,			&m_Log.m_ActionStatus,		SQLRecord::MemberType::WSTRING);
		configureMember(ActivityLogUtil::g_ColumnNameActionStatus,		&m_Log.m_ObjectNewValue,	SQLRecord::MemberType::WSTRING);
	}

	ActivityLog m_Log;
};

namespace ActivityLogUtil
{
	using CosmosUpdateData = map<SQLiteUtil::Table, map<wstring, ActivityLogRecord*>>;
}