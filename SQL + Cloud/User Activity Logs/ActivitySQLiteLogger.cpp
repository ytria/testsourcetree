#include "ActivitySQLiteLogger.h"

#include "AnnotationUtil.h"
#include "BaseO365Grid.h"
#include "CreatedObjectModification.h"
#include "DeletedObjectModification.h"
#include "DriveItemPermissionDeletion.h"
#include "DriveItemPermissionFieldUpdate.h"
#include "EventAttachmentDeletion.h"
#include "GridModifications.h"
#include "MessageAttachmentDeletion.h"
#include "PostAttachmentDeletion.h"
#include "RestoreObjectModification.h"
#include "RowSubItemFieldUpdates.h"
#include "SQLCosmosSyncManager.h"
#include "SubItemFieldUpdate.h"
#include "UserLicenseModification.h"

const wstring ActivitySQLiteLogger::g_Delimiter = _YTEXT("+|+");

ActivitySQLiteLogger::ActivitySQLiteLogger()
	: O365SQLiteEngine(_YTEXTFORMAT(L"_ual%s_.ytr", Str::getStringFromNumber<uint32_t>(g_CurrentVersion).c_str()))
{
	// Update this assert when version change.
	// For old reasons, g_CurrentVersion must at least be incremented each time SQLiteEngine::GetCurrentVersion() is.
	static_assert(g_CurrentVersion == 6 && SQLiteEngine::GetCurrentVersion() == 6, "Should you increment version or test?");
	
	m_Activity.m_Name = _YTEXT("UserActivity");
	m_Activity.AddColumnRowID();

	SQLiteUtil::Column cDate;
	cDate.m_Name			= ActivityLogUtil::g_ColumnNameDate;
	cDate.m_NameForDisplay	= YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_2, _YLOC("Action On")).c_str();
	cDate.m_Flags			= SQLiteUtil::ColumnFlags::NOTNULL | SQLiteUtil::ColumnFlags::ISDATESYNC | SQLiteUtil::ColumnFlags::INDEX;
	m_Activity.AddColumn(cDate);

	SQLiteUtil::Column cTenant;
	cTenant.m_Name			 = ActivityLogUtil::g_ColumnNameTenant;
	cTenant.m_NameForDisplay = YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_3, _YLOC("Tenant")).c_str();
	cTenant.m_Flags			 = SQLiteUtil::ColumnFlags::NOTNULL | SQLiteUtil::ISORGANIZATION;
	m_Activity.AddColumn(cTenant);

	SQLiteUtil::Column cTenantDisplay;
	cTenantDisplay.m_Name			= ActivityLogUtil::g_ColumnNameTenantDisplayName;
	cTenantDisplay.m_NameForDisplay = YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_6, _YLOC("Tenant Display Name")).c_str();
	cTenantDisplay.m_Flags			= SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cTenantDisplay);

	SQLiteUtil::Column cUserName;
	cUserName.m_Name		   = ActivityLogUtil::g_ColumnNameUserName;
	cUserName.m_NameForDisplay = YtriaTranslate::Do(DlgUserPasswordList_OnInitDialog_3, _YLOC("User Name")).c_str();
	cUserName.m_Flags		   = SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cUserName);

	SQLiteUtil::Column cUserEmail;
	cUserEmail.m_Name		    = ActivityLogUtil::g_ColumnNameEmail;
	cUserEmail.m_NameForDisplay = YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_8, _YLOC("User Email")).c_str();
	cUserEmail.m_Flags		    = SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cUserEmail);

	SQLiteUtil::Column cLicenseCode;
	cLicenseCode.m_Name			  = ActivityLogUtil::g_ColumnNameLicenseCode;
	cLicenseCode.m_NameForDisplay = YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_17, _YLOC("License Code")).c_str();
	cLicenseCode.m_Flags		  = SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cLicenseCode);

	SQLiteUtil::Column cDeviceID;
	cDeviceID.m_Name		   = ActivityLogUtil::g_ColumnNameDeviceID;
	cDeviceID.m_NameForDisplay = YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_10, _YLOC("Device ID")).c_str();
	cDeviceID.m_Flags		   = SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cDeviceID);

	SQLiteUtil::Column cSugarREF;
	cSugarREF.m_Name		   = ActivityLogUtil::g_ColumnNameSugarREF;
	cSugarREF.m_NameForDisplay = YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_11, _YLOC("Ref")).c_str();
	cSugarREF.m_Flags		   = SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cSugarREF);

	SQLiteUtil::Column cFullAdmin;
	cFullAdmin.m_Name			= ActivityLogUtil::g_ColumnNameFullAdmin;
	cFullAdmin.m_NameForDisplay	= YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_12, _YLOC("Is Full Admin?")).c_str();
	cFullAdmin.m_Flags			= SQLiteUtil::ColumnFlags::NOTNULL | SQLiteUtil::ColumnFlags::ISBOOL;
	cFullAdmin.m_DataType		= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
	m_Activity.AddColumn(cFullAdmin);

	SQLiteUtil::Column cAction;
	cAction.m_Name			 = ActivityLogUtil::g_ColumnNameAction;
	cAction.m_NameForDisplay = YtriaTranslate::Do(DlgActionProp_OnInitDialog_1, _YLOC("Action")).c_str();
	cAction.m_Flags			 = SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cAction);

	SQLiteUtil::Column cModule;
	cModule.m_Name			 = ActivityLogUtil::g_ColumnNameModuleName;
	cModule.m_NameForDisplay = YtriaTranslate::Do(WindowsGrid_customizeGrid_4, _YLOC("Module")).c_str();
	cModule.m_Flags			 = SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cModule);

	SQLiteUtil::Column cActionStatus;
	cActionStatus.m_Name		   = ActivityLogUtil::g_ColumnNameActionStatus;
	cActionStatus.m_NameForDisplay = YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_15, _YLOC("Action Status")).c_str();
	cActionStatus.m_Flags		   = SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cActionStatus);

	SQLiteUtil::Column cObjectPK;
	cObjectPK.m_Name				= ActivityLogUtil::g_ColumnNameObjectKey;
	cObjectPK.m_NameForDisplay		= YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_16, _YLOC("Object Key")).c_str();
	cObjectPK.m_MultivalueDelimiter	= g_Delimiter;
	cObjectPK.m_Flags				= SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cObjectPK);

	SQLiteUtil::Column cObjectName;
	cObjectName.m_Name			 = ActivityLogUtil::g_ColumnNameObjectName;
	cObjectName.m_NameForDisplay = YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_17, _YLOC("Object Name")).c_str();
	cObjectName.m_Flags			 = SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cObjectName);

	SQLiteUtil::Column cProperty;
	cProperty.m_Name		   = ActivityLogUtil::g_ColumnNameProperty;
	cProperty.m_NameForDisplay = YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_18, _YLOC("Property")).c_str();
	cProperty.m_Flags		   = SQLiteUtil::ColumnFlags::NOTNULL;
	m_Activity.AddColumn(cProperty);

/* for when Modification stores Gaph ID
	Column cObjectGraphID;
	cObjectGraphID.m_Name = g_ColumnNameGraphID;
	cObjectGraphID.m_NameForDisplay = YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_19, _YLOC("Graph ID")).c_str();
	cObjectGraphID.m_Flags = ColumnFlags::NOTNULL;
	m_ActivityTable.AddColumn(cObjectGraphID);
*/

	SQLiteUtil::Column cObjectOldValue;
	cObjectOldValue.m_Name				= ActivityLogUtil::g_ColumnNameOldValue;
	cObjectOldValue.m_NameForDisplay	= YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_20, _YLOC("Previous Value")).c_str();
	cObjectOldValue.m_Flags				= SQLiteUtil::ENCRYPT;

	m_Activity.AddColumn(cObjectOldValue);

	SQLiteUtil::Column cObjectNewValue;
	cObjectNewValue.m_Name					= ActivityLogUtil::g_ColumnNameNewValue;
	cObjectNewValue.m_NameForDisplay		= YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_21, _YLOC("New Value")).c_str();
	cObjectNewValue.m_MultivalueDelimiter	= g_Delimiter; // For object creation, we have a multivalue
	cObjectNewValue.m_Flags					= SQLiteUtil::ENCRYPT;
	m_Activity.AddColumn(cObjectNewValue);

	Configure();
}

vector<SQLiteUtil::Table> ActivitySQLiteLogger::GetTables() const
{
	return { m_Activity };
}

void ActivitySQLiteLogger::prepareLogSessionInfo(std::shared_ptr<const Sapio365Session> p_Session, map<SQLiteUtil::Column, wstring>& p_LogSessionInfo)
{
	auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != app)
	{
		auto licenseManager = app->GetLicenseManager();
		ASSERT(nullptr != licenseManager);
		if (nullptr != licenseManager)
		{
			p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameLicenseCode)]	= licenseManager->GetLicense(false).GetCodeForDisplay();
			p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameDeviceID)]		= licenseManager->GetNSADeviceID();
			p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameSugarREF)]		= licenseManager->GetSugarRef();
			p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameFullAdmin)]		= app->GetSessionType() == SessionTypes::g_UltraAdmin ? _YTEXT("1") : _YTEXT("0");
		}
	}

	if (p_Session)
	{		
		p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameDate)]				= TimeUtil::GetInstance().GetISO8601String(YTimeDate::GetCurrentTimeDate());// store GMT seconds since 1970 instead?
		p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameTenant)]				= p_Session->GetTenantName(true);
		p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameTenantDisplayName)]	= p_Session->GetTenantNameDisplay();

		ASSERT(!p_Session->GetGraphCache().GetCachedConnectedUser() || p_Session->GetGraphCache().GetCachedConnectedUser()->GetDisplayName());
		if (p_Session->GetGraphCache().GetCachedConnectedUser()
			&& p_Session->GetGraphCache().GetCachedConnectedUser()->GetDisplayName())
		{
			wstring namePrefix = [p_Session]()
			{
				wstring prefix;

				if (p_Session->IsUseRoleDelegation())
				{
					auto rd = RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(p_Session->GetRoleDelegationID(), p_Session);
					prefix = YtriaTranslate::Do(CIAOSession_initInstance_4, _YLOC("[%1] "), rd.GetDelegation().m_Name.c_str());
				}

				return prefix;
			}();

			p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameUserName)]	= namePrefix + p_Session->GetGraphCache().GetCachedConnectedUser()->GetDisplayName().get().c_str();
			p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameEmail)]		= p_Session->GetGraphCache().GetCachedConnectedUser()->GetUserPrincipalName().get();
		}
		else
		{
			ASSERT(p_Session->IsUltraAdmin() || p_Session->IsPartnerAdvanced() || p_Session->IsPartnerElevated());
			if (p_Session->IsUltraAdmin())
			{
				p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameUserName)]	= YtriaTranslate::Do(GridSessionsBackstage_BuildView_3, _YLOC("Ultra Admin")).c_str();
				p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameEmail)]		= YtriaTranslate::Do(GridSessionsBackstage_BuildView_3, _YLOC("Ultra Admin")).c_str();
			}
			else if (p_Session->IsPartnerAdvanced() || p_Session->IsPartnerElevated())
			{
				// FIXME: Log more info
				p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameUserName)] = _T("PARTNER");
				p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameEmail)] = _T("PARTNER");
			}
			else
			{
				p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameUserName)]	= _YTEXT("???");
				p_LogSessionInfo[m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameEmail)]		= _YTEXT("???");
			}
		}
	}
}

template<class ModificationClass>
void ActivitySQLiteLogger::logModification(const wstring& p_ModuleName, std::shared_ptr<const Sapio365Session> p_Session, const ModificationClass& p_Modification, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage)
{
	SQLiteUtil::InsertDataCollection allLogs;

	SQLiteUtil::InsertData logSessionInfo = { {m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameModuleName), p_ModuleName} };
	prepareLogSessionInfo(p_Session, logSessionInfo);

	prepare(logSessionInfo, &p_Modification, allLogs);

	insert(O365Krypter(p_Session), m_Activity, allLogs, true, p_ValuesForCloudStorage, false);
}

void ActivitySQLiteLogger::LogRBACModifications(std::shared_ptr<const Sapio365Session> p_Session, const RoleDelegationModification& p_Modification, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage)
{
	logModification<RoleDelegationModification>(YtriaTranslate::Do(ActivitySQLiteLogger_LogRBACModifications_1, _YLOC("RBAC")).c_str(), p_Session, p_Modification, p_ValuesForCloudStorage);
}

void ActivitySQLiteLogger::LogAnnotationModifications(std::shared_ptr<const Sapio365Session> p_Session, const AnnotationModification& p_Modification, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage)
{
	logModification<AnnotationModification>(p_Modification.GetRecord().m_Annotation.m_Module, p_Session, p_Modification, p_ValuesForCloudStorage);
}

void ActivitySQLiteLogger::LogGridModifications(std::shared_ptr<const Sapio365Session> p_Session, const bool p_MainModifications, const GridModificationsO365& p_Modifications, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage)
{
	SQLiteUtil::InsertDataCollection allLogs;
	
	map<SQLiteUtil::Column, wstring> logSessionInfo = {{m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameModuleName), p_Modifications.GetGrid().GetAutomationName()}};	
	prepareLogSessionInfo(p_Session, logSessionInfo);

	if (p_MainModifications)
	{
		for (const auto& m : p_Modifications.GetRowModifications())
		{
			ASSERT(m);
			if (m)
				prepare(logSessionInfo, m.get(), allLogs);
		}

		for (const auto& m : p_Modifications.GetRowFieldModifications())
		{
			ASSERT(m);
			if (m)
				prepare(logSessionInfo, m.get(), allLogs);
		}

		for (auto m : p_Modifications.GetModsRequestFeedback())
		{
			ASSERT(m);
			if (m)
				prepare(logSessionInfo, m, allLogs);
		}
	}
	else
	{
		for (const auto& m : p_Modifications.GetSubItemUpdates())
		{
			ASSERT(m);
			if (m)
				prepare(logSessionInfo, m.get(), allLogs);
		}

		for (const auto& m : p_Modifications.GetSubItemDeletions())
		{
			ASSERT(m);
			if (m)
				prepare(logSessionInfo, m.get(), allLogs);
		}
	}

	insert(O365Krypter(p_Session), m_Activity, allLogs, true, p_ValuesForCloudStorage, false);
}

void ActivitySQLiteLogger::LogGridModifications(std::shared_ptr<const Sapio365Session> p_Session, const bool p_MainModifications, const GridFieldModificationsOnPrem& p_Modifications, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage)
{
	SQLiteUtil::InsertDataCollection allLogs;

	map<SQLiteUtil::Column, wstring> logSessionInfo = { {m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameModuleName), p_Modifications.GetGrid().GetAutomationName()} };
	prepareLogSessionInfo(p_Session, logSessionInfo);

	for (const auto& m : p_Modifications.GetRowFieldModifications())
	{
		ASSERT(m);
		if (m)
			prepare(logSessionInfo, m.get(), allLogs);
	}

	insert(O365Krypter(p_Session), m_Activity, allLogs, true, p_ValuesForCloudStorage, false);
}

const wstring& ActivitySQLiteLogger::getStateStr(const Modification::State p_State) const
{
	static map<Modification::State, wstring> g_States =
	{
		{ Modification::State::RemoteHasNewValue,	_YTEXT("Server has new value") },
		{ Modification::State::RemoteHasOldValue,	_YTEXT("Server has old value") },
		{ Modification::State::RemoteHasOtherValue,	_YTEXT("Server has other value") },
		{ Modification::State::RemoteError,			_YTEXT("Server error") },
		{ Modification::State::AppliedLocally,		_YTEXT("Applied locally") }
	};
	static wstring g_Unknown = _YTEXT("Unknown");

	auto findIt = g_States.find(p_State);
	if (g_States.end() == findIt)
	{
		ASSERT(false);
		return g_Unknown;
	}
	else
		return findIt->second;
}

void ActivitySQLiteLogger::prepare(const map<SQLiteUtil::Column, wstring>& p_LogSessionInfo, const Modification* p_Modification, vector<map<SQLiteUtil::Column, wstring>>& p_AllLogs)
{
	ASSERT(!p_LogSessionInfo.empty());
	ASSERT(nullptr != p_Modification);
	if (nullptr != p_Modification)
	{
		const SQLiteUtil::Column& columnActionStatus	= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameActionStatus);
		const SQLiteUtil::Column& columnObjectKey		= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameObjectKey);
		const SQLiteUtil::Column& columnObjectName		= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameObjectName);
		const SQLiteUtil::Column& columnAction			= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameAction);
		const SQLiteUtil::Column& columnOldValue		= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameOldValue);
		const SQLiteUtil::Column& columnNewValue		= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameNewValue);
		const SQLiteUtil::Column& columnProperty		= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameProperty);
		// static const Column& columnGraphID			= m_ActivityTable.GetColumn(g_ColumnNameGraphID);

		const auto modificationlogs = p_Modification->GetModificationLogs();
		for (const auto& mlog : modificationlogs)
		{
			SQLiteUtil::InsertData logInfoForSQL;
			logInfoForSQL.insert(p_LogSessionInfo.begin(), p_LogSessionInfo.end());

			logInfoForSQL[columnAction]			= mlog.m_Action;
			logInfoForSQL[columnActionStatus]	= getStateStr(mlog.m_State);
			logInfoForSQL[columnObjectKey]		= Str::implode(mlog.m_ObjectPK, g_Delimiter, true);
			logInfoForSQL[columnObjectName]		= mlog.m_ObjectName;
			logInfoForSQL[columnProperty]		= mlog.m_Property;
			if (mlog.m_OldValue.is_initialized())
				logInfoForSQL[columnOldValue] = mlog.m_OldValue.get();
			ASSERT(!(mlog.m_NewValue.is_initialized() && mlog.m_NewMultiValue.is_initialized()));
			if (mlog.m_NewValue.is_initialized())
				logInfoForSQL[columnNewValue] = mlog.m_NewValue.get();
			else if (mlog.m_NewMultiValue.is_initialized())
				logInfoForSQL[columnNewValue] = Str::implode(mlog.m_NewMultiValue.get(), g_Delimiter, true);

			p_AllLogs.push_back(logInfoForSQL);
		}
	}
}

const SQLiteUtil::Table& ActivitySQLiteLogger::GetActivityTable() const
{
	return m_Activity;
}

SQLiteUtil::Table& ActivitySQLiteLogger::GetActivityTable()
{
	return m_Activity;
}

bool ActivitySQLiteLogger::SelectAllForUpdateToCloud(ActivityLogUtil::CosmosUpdateData& p_AllData, std::shared_ptr<const Sapio365Session> p_Session)
{
	const O365Krypter krypter(p_Session);
	SelectAll<ActivityLogRecord>(krypter, m_Activity, SQLCosmosSyncManager::GetInstance().GetLastSyncDate(GetDBfilename(), m_Activity.GetSQLcompliantName(), p_Session), m_ForCloud);
	for (auto& logRecord : m_ForCloud)
	{
		logRecord.m_Log.ReadPostProcess(logRecord);// sad but true
		logRecord.SetForCloudStorageOnly();
		WriteRecord(krypter, &logRecord);// will only prepare values for Cosmos because of SetForCloudStorageOnly
		p_AllData[m_Activity][std::to_wstring(logRecord.GetID())] = &logRecord;
	}

	return true;// TODO check SelectAll errors
}

void ActivitySQLiteLogger::CompleteUpdateToCloud(std::shared_ptr<const Sapio365Session> p_Session)
{
	m_ForCloud.clear();

	SetCloudSyncDate(p_Session);
}

void ActivitySQLiteLogger::LogBasicAction(std::shared_ptr<const Sapio365Session> p_Session, const wstring& p_Action, const wstring& p_ActionStatus, const wstring& p_ModuleName, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage)
{
	// if you need more than action && module, you are probably not in the relevant place and need a full logModification through a Modification object, cf above.

	SQLiteUtil::InsertData logSessionInfo = { {m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameModuleName), p_ModuleName} };
	prepareLogSessionInfo(p_Session, logSessionInfo);

	const SQLiteUtil::Column& columnActionStatus	= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameActionStatus);
	const SQLiteUtil::Column& columnAction			= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameAction);

	const SQLiteUtil::Column& columnObjectKey	= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameObjectKey);
	const SQLiteUtil::Column& columnObjectName	= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameObjectName);
	const SQLiteUtil::Column& columnProperty	= m_Activity.GetColumn(ActivityLogUtil::g_ColumnNameProperty);

	SQLiteUtil::InsertData logInfoForSQL;
	logInfoForSQL.insert(logSessionInfo.begin(), logSessionInfo.end());
	logInfoForSQL[columnActionStatus]	= p_ActionStatus;
	logInfoForSQL[columnAction]			= p_Action;
	logInfoForSQL[columnObjectKey];
	logInfoForSQL[columnObjectName];
	logInfoForSQL[columnProperty];

	SQLiteUtil::InsertDataCollection allLogs;
	allLogs.push_back(logInfoForSQL);
	insert(O365Krypter(p_Session), m_Activity, allLogs, true, p_ValuesForCloudStorage, false);
}