#pragma once

#include "ActivityLogUtil.h"
#include "CosmosSQLiteBridge.h"
#include "CosmosUtil.h"
#include "DlgDoubleProgressCommon.h"

class ActivityCosmosLogger : public CosmosSQLiteBridge
{
public:
	ActivityCosmosLogger();
	~ActivityCosmosLogger() = default;

	bool Write(ActivityLogRecord* p_Document, std::shared_ptr<const Sapio365Session> p_Session);// an SQL record is a Cosmos document
	bool UpdateFromSQLite(const ActivityLogUtil::CosmosUpdateData& p_SQLdata, const CosmosUtil::SQLupdateData& p_CosmosDocuments, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress);
};