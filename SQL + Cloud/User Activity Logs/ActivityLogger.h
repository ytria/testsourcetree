#pragma once

#include "CosmosSQLiteBridge.h"
#include "ActivitySQLiteLogger.h"
#include "ActivityCosmosLogger.h"
#include "DlgDoubleProgressCommon.h"
#include "ManagerSQLandCloud.h"

// log user activity (edit, create, delete...) in local SQL or on the cloud

class Sapio365Session;
class ActivityLogger : public ManagerSQLandCloud
{
public:
	virtual ~ActivityLogger() = default;

	static ActivityLogger& GetInstance();

	void LogGridModifications(const bool p_MainModifications, const GridModificationsO365& p_Modifications);
	void LogGridModifications(const bool p_MainModifications, const GridFieldModificationsOnPrem& p_Modifications);
	void LogRBACModifications(const RoleDelegationModification& p_Modification);
	void LogAnnotationModifications(const AnnotationModification& p_Modification, std::shared_ptr<const Sapio365Session> p_Session);
	void LogBasicAction(const wstring& p_Action, const wstring& p_ActionStatus, const wstring& p_ModuleName, std::shared_ptr<const Sapio365Session> p_Session);
	
	virtual void InitAfterSessionOrLicenseWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress) override;// p_Session used for Cosmos

	bool MustClearGrids() const;
	void GridsCleared();
	void ClearGrids();

private:
	ActivityLogger();

	bool pushSQLiteToCloud(std::shared_ptr<const Sapio365Session> p_Session, const CosmosUtil::SQLupdateData& p_CosmosDocuments, DlgDoubleProgressCommon& p_Progress);// data updated from SQLite to Cosmos

	ActivitySQLiteLogger m_LoggerSQLite;
	ActivityCosmosLogger m_LoggerCosmos;

	bool m_MustClearGrids;
};