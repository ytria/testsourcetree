#pragma once

class ActivityLoggerUtil
{
public:
	static const wstring g_ActionCreate;
	static const wstring g_ActionUpdate;
	static const wstring g_ActionDelete;
	static const wstring g_ActionRestore;
};
