#pragma once

#include "ActivityLogUtil.h"
#include "AnnotationUtil.h"
#include "GridModifications.h"
#include "Modification.h"
#include "O365SQLiteEngine.h"
#include "Sapio365Session.h"

class ActivitySQLiteLogger : public O365SQLiteEngine
{
public:
	ActivitySQLiteLogger();
	virtual ~ActivitySQLiteLogger() = default;

	virtual vector<SQLiteUtil::Table> GetTables() const override;

	void LogGridModifications(std::shared_ptr<const Sapio365Session> p_Session, const bool p_MainModifications, const GridModificationsO365& p_Modifications, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage);
	void LogGridModifications(std::shared_ptr<const Sapio365Session> p_Session, const bool p_MainModifications, const GridFieldModificationsOnPrem& p_Modifications, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage);
	void LogRBACModifications(std::shared_ptr<const Sapio365Session> p_Session, const RoleDelegationModification& p_Modification, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage);
	void LogAnnotationModifications(std::shared_ptr<const Sapio365Session> p_Session, const AnnotationModification& p_Modification, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage);
	void LogBasicAction(std::shared_ptr<const Sapio365Session> p_Session, const wstring& p_Action, const wstring& p_ActionStatus, const wstring& p_ModuleName, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage);

	const SQLiteUtil::Table&	GetActivityTable() const;
	SQLiteUtil::Table&			GetActivityTable();

	bool SelectAllForUpdateToCloud(ActivityLogUtil::CosmosUpdateData& p_AllData, std::shared_ptr<const Sapio365Session> p_Session);
	void CompleteUpdateToCloud(std::shared_ptr<const Sapio365Session> p_Session);

	static const wstring g_Delimiter;

	uint32_t GetVersion() const override
	{
		return g_CurrentVersion;
	}

private:
	template<class ModificationClass>
	void logModification(const wstring& p_ModuleName, std::shared_ptr<const Sapio365Session> p_Session, const ModificationClass& p_Modification, SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage);

	const wstring& getStateStr(const Modification::State p_State) const;// string description - TODO user-friendly

	void prepareLogSessionInfo(std::shared_ptr<const Sapio365Session> p_Session, map<SQLiteUtil::Column, wstring>& p_LogSessionInfo);
	void prepare(const map<SQLiteUtil::Column, wstring>& p_LogSessionInfo, const Modification* p_Modification, vector<map<SQLiteUtil::Column, wstring>>& p_Logs);

	SQLiteUtil::Table m_Activity;

	vector<ActivityLogRecord> m_ForCloud;

	static constexpr uint32_t g_CurrentVersion = 6;
};