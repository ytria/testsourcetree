#include "CosmosConnectorSQLite.h"

const wstring CosmosConnectorSQLite::g_ColumnURL		= _YTEXT("URL");
const wstring CosmosConnectorSQLite::g_ColumnMasterKey	= _YTEXT("Master Key");

CosmosConnectorSQLite::CosmosConnectorSQLite() : O365SQLiteEngine(_YTEXT("c9sm6s.cnx"))
{
	m_CosmosKeys.m_Name = _YTEXT("CosmosKeys");

	{
		SQLiteUtil::Column cURL;
		cURL.m_Name = g_ColumnURL;
		m_CosmosKeys.AddColumn(cURL);

		SQLiteUtil::Column cMasterKey;
		cMasterKey.m_Name = g_ColumnMasterKey;
		m_CosmosKeys.AddColumn(cMasterKey);
	}
}

CosmosConnectorSQLite::BusinessCosmos CosmosConnectorSQLite::GetConnector()
{
	BusinessCosmos cnx;

	vector<CosmosConnectorSQLite::CosmosRecord> c = SelectAll<CosmosConnectorSQLite::CosmosRecord>(m_CosmosKeys);
	ASSERT(c.size() < 2);// unless we decide to allow multi-Cosmos configurations
	if (!c.empty())
		cnx = c.back().m_Cosmos;

	return cnx;
}

vector<SQLiteUtil::Table> CosmosConnectorSQLite::GetTables() const
{
	return { m_CosmosKeys };
}

void CosmosConnectorSQLite::AddTestData() // TODO REMOVE
{
	ASSERT(false);
	auto keySize = SelectCount(m_CosmosKeys);
	if (!keySize.is_initialized() || keySize.get() == 0)
	{
		CosmosRecord cnx(m_CosmosKeys);
		cnx.m_Cosmos.m_URL			= _YTEXT("https://azuresql.documents.azure.com:443/");// TODO fetch info from Nalpeiron agility data
		cnx.m_Cosmos.m_MasterKey	= _YTEXT("Cxzs4l6uur6RfBcF6bdZvwTGqjDNYJe9RvxJer9waDv3JkqDivvfdtEJXbj316qSVL6sWCoEsa2WqNCCwxabNg==");// TODO fetch info from Nalpeiron agility data
		WriteRecord(&cnx);
	}
}