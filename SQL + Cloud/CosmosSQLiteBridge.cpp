#include "CosmosSQLiteBridge.h"

#include "CosmosManager.h"
#include "CosmosDBSqlSession.h"
#include "CreateCollectionRequester.h"
#include "CreateDbRequester.h"
#include "Document.h"
#include "FileUtil.h"
#include "ListCollectionsRequester.h"
#include "ListDbsRequester.h"
#include "Office365Admin.h"
#include "QueryDocsRequester.h"
#include "safeTaskCall.h"
#include "Sapio365Session.h"
#include "SQLCosmosSyncManager.h"
#include "TraceIntoFile.h"
#include "WriteDocRequester.h"
#include "YCallbackMessage.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTaskData.h"

// https://docs.microsoft.com/en-ca/rest/api/cosmos-db/ 

using namespace Cosmos;

SQLiteUtil::Table CosmosSQLiteBridge::g_SingleContainerTable;
SQLiteUtil::Column CosmosSQLiteBridge::g_SingleContainerColumnModule;
SQLiteUtil::Column CosmosSQLiteBridge::g_SingleContainerColumnTable;

CosmosSQLiteBridge::CosmosSQLiteBridge():
	m_CurrentVersion(0),
	m_ErrorCode(CosmosSQLiteBridge::COSMOS_NOERROR)
{
}

void CosmosSQLiteBridge::SetSource(const SQLiteEngine& p_Engine, const vector<SQLiteUtil::Table>& p_SQLiteTables, const wstring& p_DBname, const bool p_UseSingleDB)
{
	m_SingleContainerModuleName.clear();

	m_SQLiteTables = p_SQLiteTables;

	ASSERT(!p_DBname.empty());
	if (p_UseSingleDB)
	{
		// to save money (Microsoft charges by the table), all data can be stored in a single table with extra context info (module, table); the switch is done with an option in the product license, not set by default
		if (g_SingleContainerTable.m_Name.empty())
			g_SingleContainerTable.m_Name			= _YTEXT("sapio365");
		if (g_SingleContainerColumnModule.m_Name.empty())
		{
			g_SingleContainerColumnModule.m_Name		= _YTEXT("MODULE");
			g_SingleContainerColumnModule.m_TableName	= g_SingleContainerTable.GetSQLcompliantName();
		}
		if (g_SingleContainerColumnTable.m_Name.empty())
		{
			g_SingleContainerColumnTable.m_Name			= _YTEXT("MODULETABLE");
			g_SingleContainerColumnTable.m_TableName	= g_SingleContainerTable.GetSQLcompliantName();
		}

		// FIXME: We use the base class version for name whereas each concrete now has its own version (p_Engine.GetVersion() >= SQLiteEngine::GetCurrentVersion())
		// Is it valid as is ???? (Below assert is here to remind us to check behavior when we sync some database with a different version.
		ASSERT(p_Engine.GetVersion() == SQLiteEngine::GetCurrentVersion());
		m_DBName					= _YTEXTFORMAT(L"_sc_%s.ytr", Str::getStringFromNumber<uint32_t>(SQLiteEngine::GetCurrentVersion()).c_str());
		m_CosomosTables				= { g_SingleContainerTable };
		m_SingleContainerModuleName = p_DBname;
		TraceIntoFile::trace(_YDUMP("CosmosSQLiteBridge"), _YDUMP("SetSource"), _YTEXTFORMAT(L"Configuring single container table in: %s/%s (for module: %s)", m_DBName.c_str(), g_SingleContainerTable.GetSQLcompliantName().c_str(), m_SingleContainerModuleName.c_str()));
	}
	else
	{
		m_DBName		= p_DBname;
		m_CosomosTables = p_SQLiteTables;
		TraceIntoFile::trace(_YDUMP("CosmosSQLiteBridge"), _YDUMP("SetSource"), _YTEXTFORMAT(L"Configuring regular tables in: %s", m_DBName.c_str()));
	}

	m_Engine = &p_Engine;
}

bool CosmosSQLiteBridge::Configure(const SQLiteEngine& p_Engine, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	m_ErrorCode = COSMOS_NOERROR;

	const auto	moduleDBname	= FileUtil::FileGetFileName(p_Engine.GetDBfilename());
	const auto	SQLiteversion	= p_Engine.GetVersion();

	// check | configure cosmos db & tables (collections)
	try
	{
		ASSERT(p_Session);
		if (p_Session && p_Session->IsReadyForCosmos())
		{
			SetSource(p_Engine, p_Engine.GetTablesForCloudSync(), moduleDBname, p_Session->IsCosmosSingleContainer());

			TraceIntoFile::trace(_YDUMP("CosmosSQLiteBridge"), _YDUMP("Configure"), _YTEXTFORMAT(L"Configuring: %s", m_DBName.c_str()));

			YtriaTaskData task;

			bool dbFound = false;
			Cosmos::ListDbsRequester dbRequester;
			dbRequester.Send(p_Session, task).GetTask().wait();
			const auto& DBlist = dbRequester.GetData();

			bool hn;
			for (const auto& db : DBlist)
			{
				if (MFCUtil::StringMatchW(db.m_Id, m_DBName))
					dbFound = true;

				m_CurrentVersion = max(static_cast<uint32_t>(Str::GetFirstNumber(db.m_Id, hn)), m_CurrentVersion);
			}

			// FIXME: SQLiteversion is the concrete SqlEngine version. We used to test the base class SQLiteEngine::GetCurrentVersion().
			// Has this change any impact? (Below assert is here to remind us to check behavior when we sync some database with a different version.
			ASSERT(SQLiteversion == SQLiteEngine::GetCurrentVersion());
			if (m_CurrentVersion > SQLiteversion)
			{
				// this is a rather lousy test: there is no migration/cleanup process yet (11/2019)
				m_ErrorCode = SAPIO365_TOOOLD_TOOCOLD;
				wstring error = YtriaTranslate::Do(CosmosSQLiteBridge_Configure_1, _YLOC("This version of sapio365 is out-of-date with the current Role-Based Access Control settings last updated by the admin.\nAll roles will be inaccessible to this client.\n\nPlease update sapio365 and try again.")).c_str();
				TraceIntoFile::trace(_YDUMP("CosmosSQLiteBridge"), _YDUMP("Configure"), _YTEXTFORMAT(L"COSMOS DB ERROR: %s", error.c_str()));
				YCallbackMessage::DoSend([error]()
				{
					YCodeJockMessageBox dlg(AfxGetApp()->m_pMainWnd,
											DlgMessageBox::eIcon_ExclamationWarning,
											YtriaTranslate::Do(Application_error_1, _YLOC("Warning")).c_str(),
											YtriaTranslate::Do(CosmosSQLiteBridge_Configure_3, _YLOC("Please update sapio365")).c_str(),
											error.c_str(), 
											{ { IDOK, YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str() } });
					dlg.DoModal();
					// TODO BTN update sapio365
				});
			}
			else
			{
				if (!dbFound)
				{
					Cosmos::CreateDbRequester requester(m_DBName);
					requester.Send(p_Session, task).GetTask().wait();
				}

				// check if tables (collections) are set
				Cosmos::ListCollectionsRequester tableRequester(m_DBName);
				tableRequester.Send(p_Session, task).GetTask().wait();
				const auto& COSMOStables = tableRequester.GetData();
				auto tablesCopy = m_CosomosTables;
				for (const auto& COSMOStable : COSMOStables)
				{
					for (const auto& SQLitetable : tablesCopy)
					{
						if (MFCUtil::StringMatchW(SQLitetable.GetSQLcompliantName(), COSMOStable.m_Id))
						{
							tablesCopy.erase(std::remove(tablesCopy.begin(), tablesCopy.end(), SQLitetable), tablesCopy.end());
							break;
						}
					}
				}

				// p_SQLiteTables now contains the missing tables to add to Cosmos
				for (const auto& SQLitetable : tablesCopy)
				{
					Cosmos::CreateCollectionRequester tableKreator(m_DBName, SQLitetable.GetSQLcompliantName());
					tableKreator.Send(p_Session, task).GetTask().wait();
				}

				rvSuccess = true;
			}
		}
		else
		{
			TraceIntoFile::trace(_YDUMP("CosmosSQLiteBridge"), _YDUMP("Configure"), _YTEXT("License not set for Cosmos."));
			m_ErrorCode = SESSION_NOTREADYFORCOSMOS;
		}
	}
	catch (const RestException& e)
	{
		processRESTexception(_YFORMAT(L"HTTP Code: %s - DB Configuration: %s", Str::getStringFromNumber<http::status_code>(e.GetRequestResult().Response.status_code()).c_str(), e.GetRequestResult().GetErrorMessage().c_str()), e.WhatUnicode(), true, p_Session);
	}
	catch (const std::exception& e)
	{
		processRESTexception(_T("DB Configuration"), Str::convertFromUTF8(e.what()), true, p_Session);
	}

	return rvSuccess;
}

void CosmosSQLiteBridge::processRESTexception(const wstring& p_Context, const wstring& p_Error, const bool p_MsgBox, const shared_ptr<const Sapio365Session>& p_Session)
{
	m_ErrorCode = RESTEXCEPTION;

	ASSERT(p_Session);
	const wstring cosmosAccount = p_Session ? p_Session->GetCosmosAccount() : _YTEXT("~ Session not set! ~");// no term for this - should never happen

	LoggerService::User(_YDUMPFORMAT(L"Cosmos REST error (Account: %s - DB: %s): %s", cosmosAccount.c_str(), m_DBName.c_str(), p_Error.c_str()), nullptr);
	TraceIntoFile::trace(_YDUMP("CosmosSQLiteBridge"), _YDUMP("processError"), _YTEXTFORMAT(L"COSMOS DB ERROR (ACCOUNT: %s - DB: %s): %s", cosmosAccount.c_str(), m_DBName.c_str(), p_Error.c_str()));
	if (p_MsgBox)
	{
		auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
		ASSERT(nullptr != app);
		if (AutomatedApp::IsAutomationRunning())
			app->AddErrorToCurrentAction(YtriaTranslate::Do(CosmosSQLiteBridge_processRESTexception_3, _YLOC("An error occurred accessing the Cosmos account %1"), cosmosAccount.c_str()).c_str());
		else if (p_Session && p_Session->IsCommandLineAutomationInProgress())
			app->AddErrorCommandLine(YtriaTranslate::Do(CosmosSQLiteBridge_processRESTexception_3, _YLOC("An error occurred accessing the Cosmos account %1"), cosmosAccount.c_str()));
		else
			YCallbackMessage::DoSend([p_Error, cosmosAccount, p_Context, this]()
			{
				YCodeJockMessageBox dlg(AfxGetApp()->m_pMainWnd,
										DlgMessageBox::eIcon_Error,
										YtriaTranslate::Do(CosmosSQLiteBridge_processRESTexception_3, _YLOC("An error occurred accessing the Cosmos account %1"), cosmosAccount.c_str()).c_str(),
										YtriaTranslate::Do(CosmosSQLiteBridge_processRESTexception_1, _YLOC("Please check your Cosmos credentials in About sapio365")).c_str(),
										_YTEXTFORMAT(L"%s\n%s\nDB: %s", p_Context.c_str(), p_Error.c_str(), m_DBName.c_str()),
										{ { IDOK, YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str() } });
				dlg.DoModal();
			});
	}
}

uint32_t CosmosSQLiteBridge::GetCurrentVersion()
{
	return m_CurrentVersion;
}

CosmosSQLiteBridge::ErrorCode CosmosSQLiteBridge::GetLastError() const
{
	return m_ErrorCode;
}

bool CosmosSQLiteBridge::Write(const SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage, const SQLiteUtil::Table& p_Table, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = true;

	for (const auto& vfcs : p_ValuesForCloudStorage)
		rvSuccess = Write(vfcs, p_Table, p_Session) && rvSuccess;

	return rvSuccess;
}

bool CosmosSQLiteBridge::Write(const SQLiteUtil::InsertData& p_ValuesForCloudStorage, const SQLiteUtil::Table& p_Table, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	const auto& writeToTable = m_SingleContainerModuleName.empty() ? p_Table : g_SingleContainerTable;
	TraceIntoFile::trace(_YDUMP("CosmosSQLiteBridge"), _YDUMP("Write"), _YTEXTFORMAT(L"Writing to: %s", writeToTable.GetSQLcompliantName().c_str()));

	// we must provide the Cosmos document ID
	auto findID = p_ValuesForCloudStorage.find(p_Table.GetColumnROWID());
	ASSERT(p_ValuesForCloudStorage.end() != findID);
	ASSERT(p_Session);
	if (p_Session && p_Session->IsReadyForCosmos() && p_ValuesForCloudStorage.end() != findID)
	{
		web::json::value cosmosDocument = web::json::value::object();

		// "id" is the standard Cosmos PK: insert new document if the value is not found in the collection, update document otherwise - it must be a string
		cosmosDocument[_YTEXT("id")] = web::json::value::string(findID->second);

		// add single container context
		if (!m_SingleContainerModuleName.empty())
		{
			cosmosDocument[g_SingleContainerColumnModule.GetSQLcompliantName()]	= web::json::value::string(m_SingleContainerModuleName);		
			cosmosDocument[g_SingleContainerColumnTable.GetSQLcompliantName()]	= web::json::value::string(p_Table.GetSQLcompliantName());
		}

		// add actual data
		for (const auto& v : p_ValuesForCloudStorage)
			cosmosDocument[v.first.GetSQLcompliantName()] = web::json::value::string(v.second);

		try
		{
			Cosmos::WriteDocRequester writer(m_DBName, writeToTable.GetSQLcompliantName(), cosmosDocument, true);
			writer.Send(p_Session, YtriaTaskData()).GetTask().wait();
			rvSuccess = true;
		}
		catch (const RestException& e)
		{
			processRESTexception(_YFORMAT(L"Writing document: %s", e.GetRequestResult().GetErrorMessage().c_str()), e.WhatUnicode(), false, p_Session);
		}
		catch (const std::exception& e)
		{
			processRESTexception(_T("Writing document"), Str::convertFromUTF8(e.what()), false, p_Session);
		}
	}
	else
	{
		if (!p_Session)
			TraceIntoFile::trace(_YDUMP("CosmosSQLiteBridge"), _YDUMP("Write to Cosmos DB"), _YTEXT("- ERROR - sapio365 Session not set"));
		else if (!p_Session->IsReadyForCosmos())
			TraceIntoFile::trace(_YDUMP("CosmosSQLiteBridge"), _YDUMP("Write to Cosmos DB"), _YTEXT("sapio365 Session not ready for Cosmos"));
		if (p_ValuesForCloudStorage.end() == findID)
			TraceIntoFile::trace(_YDUMP("CosmosSQLiteBridge"), _YDUMP("Write to Cosmos DB"), _YTEXT("no id was provided: unable to write"));
	}

	return rvSuccess;
}

bool CosmosSQLiteBridge::SelectAll(CosmosUtil::SQLupdateData& p_Documents, std::shared_ptr<const Sapio365Session> p_Session, const bool p_SyncDate)
{
	bool rvSuccess = true;
	
	p_Documents.clear();
	for (const auto& table : m_SQLiteTables)
		rvSuccess = rvSuccess && SelectAll(p_Documents, p_Session, table, {}, {}, false, p_SyncDate);

	return rvSuccess;
}

bool CosmosSQLiteBridge::SelectAll(CosmosUtil::SQLupdateData& p_Documents, std::shared_ptr<const Sapio365Session> p_Session, const SQLiteUtil::Table& p_Table, const vector<SQLiteUtil::WhereClause>& p_WhereClauses, const LinearMap<SQLiteUtil::Column, bool>& p_OrderBy, const bool p_PurgeBeforeSelect, const bool p_SyncDate)
{
	bool rvSuccess = false;

	if (p_PurgeBeforeSelect)
		p_Documents.clear();

	ASSERT(p_Session);
	if (p_Session && p_Session->IsReadyForCosmos())
	{
		const auto& selectFromTable = m_SingleContainerModuleName.empty() ? p_Table : g_SingleContainerTable;

		wstring selectStatement = _YTEXTFORMAT(L"select * from %s", selectFromTable.GetSQLcompliantName().c_str());
		TraceIntoFile::trace(_YDUMP("CosmosSQLiteBridge"), _YDUMP("SelectAll"), _YTEXTFORMAT(L"Select from: %s", selectFromTable.GetSQLcompliantName().c_str()));

		LinearMap<SQLiteUtil::Column, bool> ob;

		auto wc = p_WhereClauses;
		if (p_SyncDate)
		{
			SQLiteUtil::WhereClause wcSync;
			const auto& dateSyncColumn = p_Table.GetColumnDateSyncColumn();
			if (!dateSyncColumn.IsDummy())
			{
				auto syncDate = SQLCosmosSyncManager::GetInstance().GetLastSyncDate(m_SingleContainerModuleName.empty() ? m_DBName : m_SingleContainerModuleName, p_Table.GetSQLcompliantName(), p_Session);
				if (!syncDate.IsEmpty())
				{
					wcSync.m_Column		= dateSyncColumn;
					wcSync.m_Operator	= SQLiteUtil::Operator::Greater;
					wcSync.m_Values.push_back(TimeUtil::GetInstance().GetISO8601String(syncDate));

					wc.push_back(wcSync);
				}
			}
		}

		ASSERT(nullptr != m_Engine);
		if (nullptr != m_Engine)
		{
			auto defaultWC = m_Engine->GetSelectDefaultWhereClause(O365Krypter(p_Session), p_Table);
			if (defaultWC.IsValid())
				wc.push_back(defaultWC);
		}

		if (!m_SingleContainerModuleName.empty())
		{
			for (size_t k = 0; k < p_OrderBy.size(); k++)
			{
				auto column		= p_OrderBy.at(k);
				auto orderBy	= p_OrderBy[column];

				column.m_TableName = g_SingleContainerTable.GetSQLcompliantName();
				ob.push_back(column, orderBy);
			}

			for (auto& w : wc)
				w.m_Column.m_TableName = g_SingleContainerTable.GetSQLcompliantName();

			SQLiteUtil::WhereClause wcModule;
			wcModule.m_Column	= g_SingleContainerColumnModule;
			wcModule.m_Operator = SQLiteUtil::Operator::Equal;
			wcModule.m_Values.push_back(m_SingleContainerModuleName);

			SQLiteUtil::WhereClause wcTable;
			wcTable.m_Column	= g_SingleContainerColumnTable;
			wcTable.m_Operator	= SQLiteUtil::Operator::Equal;
			wcTable.m_Values.push_back(p_Table.GetSQLcompliantName());

			wc.push_back(wcModule);
			wc.push_back(wcTable);
		}
		else
			ob = p_OrderBy;

		SQLiteUtil::SelectQuery::ProcessWhere(wc, selectStatement);
		SQLiteUtil::SelectQuery::ProcessOrderBy(ob, selectStatement);

		try
		{
			Cosmos::QueryDocsRequester requester(m_DBName, selectFromTable.GetSQLcompliantName(), selectStatement, {});
			requester.Send(p_Session, YtriaTaskData()).GetTask().wait();
			p_Documents[p_Table] = requester.GetData();

			rvSuccess = true;
		}
		catch (const RestException& e)
		{
			processRESTexception(_YFORMAT(L"Reading documents: %s", e.GetRequestResult().GetErrorMessage().c_str()), e.WhatUnicode(), false, p_Session);
		}
		catch (const std::exception& e)
		{
			processRESTexception(_T("Reading documents"), Str::convertFromUTF8(e.what()), false, p_Session);
		}
	}
	else
	{
		if (!p_Session)
			TraceIntoFile::trace(_YDUMP("RoleDelegationCosmos"), _YDUMP("SelectAll"), _YTEXT("- ERROR - sapio365 Session not set"));
		else if (!p_Session->IsReadyForCosmos())
			TraceIntoFile::trace(_YDUMP("RoleDelegationCosmos"), _YDUMP("SelectAll"), _YTEXT("sapio365 Session not ready for Cosmos"));
	}

	return rvSuccess;
}