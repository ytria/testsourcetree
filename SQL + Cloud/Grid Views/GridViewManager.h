#pragma once

#include "DlgDoubleProgressCommon.h"
#include "GridView.h"
#include "GridViewManagerCosmos.h"
#include "JobCenterUtil.h"
#include "ManagerSQLandCloud.h"

class ScriptParser;
class GridViewManager : public ManagerSQLandCloud
{
public:
	~GridViewManager() = default;

	static GridViewManager& GetInstance();

	virtual void InitAfterSessionOrLicenseWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress) override;// p_Session used for Cosmos

	GridView				LoadView(const int64_t p_ViewID);
	boost::YOpt<GridView>	GetDefaultView(const wstring& p_Module);

	bool Write(GridView& p_View, std::shared_ptr<const Sapio365Session> p_Session);
	bool LoadViews(const wstring& p_Module, vector<GridView>& p_Views);
	bool LoadViews(const vector<int64_t>& p_IDs, vector<GridView>& p_Views);
	bool DeleteViews(const vector<int64_t>& p_ViewIDs);
	void UpdateSystemViews(const wstring& p_ModuleName, vector<Script>& p_ScriptsForViews, std::shared_ptr<const Sapio365Session> p_Session, ScriptParser& p_XMLparser);

private:
	GridViewManager();
};