#include "GridViewManager.h"

#include "GridViewManagerSQLite.h"
#include "Sapio365Session.h"
#include "TraceIntoFile.h"

// static 
GridViewManager& GridViewManager::GetInstance()
{
	static GridViewManager g_Instance;
	return g_Instance;
}

GridViewManager::GridViewManager()
{}

void GridViewManager::InitAfterSessionOrLicenseWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress)
{
	// TODO cosmos sync
}

GridView GridViewManager::LoadView(const int64_t p_ViewID)
{
	return GridViewManagerSQLite::GetInstance().LoadView(p_ViewID);
}

boost::YOpt<GridView> GridViewManager::GetDefaultView(const wstring& p_Module)
{
	return GridViewManagerSQLite::GetInstance().GetDefaultView(p_Module);
}

bool GridViewManager::DeleteViews(const vector<int64_t>& p_ViewIDs)
{
	return  GridViewManagerSQLite::GetInstance().DeleteViews(p_ViewIDs);
}

bool GridViewManager::LoadViews(const wstring& p_Module, vector<GridView>& p_Views)
{
	return GridViewManagerSQLite::GetInstance().LoadViews(p_Module, p_Views);
}

bool GridViewManager::LoadViews(const vector<int64_t>& p_IDs, vector<GridView>& p_Views)
{
	return GridViewManagerSQLite::GetInstance().LoadViews(p_IDs, p_Views);
}

bool GridViewManager::Write(GridView& p_View, std::shared_ptr<const Sapio365Session> p_Session)
{
	ASSERT(nullptr != p_Session);
	if (nullptr != p_Session)
	{
		// Sapio365SessionSavedInfo might do the trick too
		const auto& user	= p_Session->GetGraphCache().GetCachedConnectedUser();
		const auto isUA		= p_Session->IsUltraAdmin();
		ASSERT(isUA || user.is_initialized());

		p_View.m_UpdateByID				= isUA ? Sapio365Session::GetUltraAdminID() : user.is_initialized() ? user->GetID() : _YTEXT("N/A");
		p_View.m_UpdateByName			= isUA ? YtriaTranslate::Do(GridSessionsBackstage_BuildView_3, _YLOC("Ultra Admin")).c_str() : user.is_initialized() && user->GetDisplayName().is_initialized() ? user->GetDisplayName().get() : _YTEXT("N/A");
		p_View.m_UpdateByPrincipalName	= isUA ? YtriaTranslate::Do(GridSessionsBackstage_BuildView_3, _YLOC("Ultra Admin")).c_str() : user.is_initialized() && user->GetUserPrincipalName().is_initialized() ? user->GetUserPrincipalName().get() : _YTEXT("N/A");

		GridViewManagerSQLite::GetInstance().Write(p_View);
		return p_View.m_WriteError_VOL.empty();
	}

	return false;
}

void GridViewManager::UpdateSystemViews(const wstring& p_ModuleName, vector<Script>& p_ScriptsForViews, std::shared_ptr<const Sapio365Session> p_Session, ScriptParser& p_XMLparser)
{
	TraceIntoFile::trace(_YTEXT("GridViewManager"), _YTEXT("UpdateSystemViews"), _YTEXTFORMAT(L"Updating system views in %s", p_ModuleName.c_str()));

	vector<GridView> viewsInModule;
	LoadViews(p_ModuleName, viewsInModule);

	bool moduleHasDefaultView = false;
	map<wstring, GridView, CaseInsensitiveComparator> systemViewsInModuleByName;
	for (const auto& vim : viewsInModule)
	{
		if (vim.IsSystem())
			systemViewsInModuleByName[vim.m_Name] = vim;
		if (vim.IsDefault())
			moduleHasDefaultView = true;
	}

	for (auto& yScript : p_ScriptsForViews)
	{
		// it is a system grid view
		GridView gv;
		gv.m_Name			= yScript.m_GridViewSettings_Vol.get()[GridViewUtil::g_SystemHeaderTitle];
		gv.m_Version		= yScript.m_GridViewSettings_Vol.get()[GridViewUtil::g_SystemHeaderVersion];
		gv.m_Module			= p_ModuleName;
		gv.m_Description	= yScript.m_GridViewSettings_Vol.get()[GridViewUtil::g_SystemHeaderDescription];
		gv.SetDefault(MFCUtil::StringMatchW(yScript.m_GridViewSettings_Vol.get()[GridViewUtil::g_SystemHeaderDefault], AutomationConstant::val().m_ValueTrue));
		gv.SetSandbox(MFCUtil::StringMatchW(yScript.m_GridViewSettings_Vol.get()[GridViewUtil::g_SystemHeaderSandbox], AutomationConstant::val().m_ValueTrue));
		gv.SetSystem(true);
		gv.m_FileName_VOL = yScript.m_Key;
		ASSERT(!gv.m_Name.empty());
		if (gv.m_Name.empty())
			TraceIntoFile::trace(_YTEXT("GridViewManager"), _YTEXT("UpdateSystemViews"), _YTEXTFORMAT(L"Empty view name in %s", yScript.m_Key.c_str()));
		ASSERT(!gv.m_Version.empty());
		if (gv.m_Version.empty())
			TraceIntoFile::trace(_YTEXT("GridViewManager"), _YTEXT("UpdateSystemViews"), _YTEXTFORMAT(L"Empty view version in %s (%s)", gv.m_Name.c_str(), yScript.m_Key.c_str()));

		bool validView = !gv.m_Name.empty();
		if (validView)
		{
			const auto findIt	= systemViewsInModuleByName.find(gv.m_Name);
			const bool newView	= findIt == systemViewsInModuleByName.end();

			if (newView || gv.m_Version.compare(findIt->second.m_Version) > 0)
			{
				validView = p_XMLparser.ParseAndSerialize(yScript);// Bug #201215.RL.00D442: the issue about views when imported
				if (validView)
				{
					gv.m_XML = yScript.m_ScriptContent;
					ASSERT(!gv.m_XML.empty());
					if (!gv.m_XML.empty())
					{
						if (!newView)
							gv.m_ID = findIt->second.GetID();
						if (moduleHasDefaultView)
							gv.SetDefault(false);// once it is set, never change the default view from system updates
						Write(gv, p_Session);
					}
					else
						TraceIntoFile::trace(_YTEXT("GridViewManager"), _YTEXT("UpdateSystemViews"), _YTEXTFORMAT(L"Empty view XML in %s (%s)", gv.m_Name.c_str(), yScript.m_Key.c_str()));
				}
				else
					TraceIntoFile::trace(_YTEXT("GridViewManager"), _YTEXT("UpdateSystemViews"), _YTEXTFORMAT(L"Invalid view XML in %s (%s)", gv.m_Name.c_str(), yScript.m_Key.c_str()));
			}

			systemViewsInModuleByName.erase(gv.m_Name);
		}
	}

	// purge obsolete views
	vector<int64_t> purgeList;
	for (const auto ov : systemViewsInModuleByName)
		purgeList.push_back(ov.second.GetID());
	if (!purgeList.empty())
		DeleteViews(purgeList);
}