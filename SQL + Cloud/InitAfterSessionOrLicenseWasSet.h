#pragma once

#include "AnnotationManager.h"
#include "ActivityLogger.h"
#include "DlgDoubleProgressCommon.h"
#include "JobCenterManager.h"
#include "RoleDelegationManager.h"
#include "SQLCosmosSyncManager.h"

class InitAfterSessionOrLicenseWasSet
{
public:
	static void Do(std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent) // updates SQLite <=> Cosmos
	{
		DlgDoubleProgressCommon::RunModal(_T("Initializing session..."), p_Parent,
			[p_Session](std::shared_ptr<DlgDoubleProgressCommon> prog) mutable
			{
				ASSERT(p_Session);
				if (p_Session)
				{
					CWaitCursor _;
					CosmosManager cosmosKeyManager;// Cosmos login storage (URL, Master Key)
					auto cnx = cosmosKeyManager.GetConnectorFromLicense(p_Session);
					p_Session->SetCosmosDbMasterKey(cnx.m_MasterKey);
					p_Session->SetCosmosDbSqlUri(cnx.m_URL);
					p_Session->SetCosmosSingleContainer(cnx.m_CosmosSingleContainer);
 				}

				const bool cosmosReady = p_Session && p_Session->IsReadyForCosmos();
				prog->SetCounterTotal1(cosmosReady ? 5 : 2);

				if (cosmosReady)
				{
					SQLCosmosSyncManager::GetInstance().ManageCosmosSyncDates(p_Session);

					prog->SetCounterTotal2(1);
					prog->SetText1(_T("Syncing with cloud"));
					prog->IncrementCounter2(_T("Cosmos connection established"));

					prog->IncrementCounter1(_T("Updating RBAC"));
					prog->SetCounterTotal2(0);
					prog->SetText2(_YTEXT("Initializing"));
					RoleDelegationManager::GetInstance().InitAfterSessionOrLicenseWasSet(p_Session, *prog);// updates SQLite <=> Cosmos

					prog->IncrementCounter1(_T("Updating Activity Logs"));
					prog->SetCounterTotal2(0);
					prog->SetText2(_YTEXT("Initializing"));
					ActivityLogger::GetInstance().InitAfterSessionOrLicenseWasSet(p_Session, *prog);// update SQLite => Cosmos + refresh grids

					prog->IncrementCounter1(_T("Updating Comments"));
					prog->SetCounterTotal2(0);
					prog->SetText2(_YTEXT("Initializing"));
					AnnotationManager::GetInstance().InitAfterSessionOrLicenseWasSet(p_Session, *prog);// update SQLite => Cosmos + refresh grids
				}
				else
					ActivityLogger::GetInstance().ClearGrids();

				prog->IncrementCounter1(_T("Updating Job Center"));
				prog->SetCounterTotal2(0);
				prog->SetText2(_YTEXT("Initializing"));
				JobCenterManager::GetInstance().InitAfterSessionOrLicenseWasSet(p_Session, *prog);

				// set session management permissions
				if (p_Session)
				{
					set<RoleDelegationUtil::RBAC_AdminScope> AA;
					RoleDelegationManager::GetInstance().GetAdminAccessForSession(p_Session, AA);
					p_Session->SetAdminAccess(AA);
				}

				prog->IncrementCounter1(_T("Session initialized"));
			});
	}
};