#pragma once

#include "CosmosManager.h"
#include "CosmosUtil.h"
#include "SQLiteUtil.h"

// CosmosDB is used as cloud repository
// database configuration is performed according to SQLite configuration
// data pulled from CosmosDB is stored and used locally in SQLite
// SQLite updates are pushed to CosmosDB

class Sapio365Session;

class CosmosSQLiteBridge
{
public:
	CosmosSQLiteBridge();

	uint32_t GetCurrentVersion();// database version - e.g. 5 for _rd_5.ytr

	bool Configure(const SQLiteEngine& p_Engine, std::shared_ptr<const Sapio365Session> p_Session);
	void SetSource(const SQLiteEngine& p_Engine, const vector<SQLiteUtil::Table>& p_SQLiteTables, const wstring& p_DBname, const bool p_UseSingleDB);
	bool Write(const SQLiteUtil::InsertData& p_ValuesForCloudStorage, const SQLiteUtil::Table& p_Table, std::shared_ptr<const Sapio365Session> p_Session);
	bool Write(const SQLiteUtil::InsertDataCollection& p_ValuesForCloudStorage, const SQLiteUtil::Table& p_Table, std::shared_ptr<const Sapio365Session> p_Session);
	bool SelectAll(CosmosUtil::SQLupdateData& p_Documents, std::shared_ptr<const Sapio365Session> p_Session, const bool p_SyncDate);
	bool SelectAll(CosmosUtil::SQLupdateData& p_Documents, std::shared_ptr<const Sapio365Session> p_Session, const SQLiteUtil::Table& p_Table, const vector<SQLiteUtil::WhereClause>& p_WhereClauses, const LinearMap<SQLiteUtil::Column, bool>& p_OrderBy, const bool p_PurgeBeforeSelect, const bool p_SyncDate);

	enum ErrorCode : uint32_t
	{
		COSMOS_NOERROR = 0,
		SESSION_NOTREADYFORCOSMOS,
		SAPIO365_TOOOLD_TOOCOLD, // https://www.youtube.com/watch?v=-tVKRs4kfqc
		RESTEXCEPTION
	};

	ErrorCode GetLastError() const;

protected:
	void processRESTexception(const wstring& p_Context, const wstring& p_Error, const bool p_MsgBox, const shared_ptr<const Sapio365Session>& p_Session);

	wstring						m_DBName;
	vector<SQLiteUtil::Table>	m_SQLiteTables;
	vector<SQLiteUtil::Table>	m_CosomosTables;
	const SQLiteEngine*			m_Engine;

	uint32_t	m_CurrentVersion;
	ErrorCode	m_ErrorCode;

private:
	wstring m_SingleContainerModuleName;// all bits into the same hole
	
	static SQLiteUtil::Table	g_SingleContainerTable;
	static SQLiteUtil::Column	g_SingleContainerColumnModule;
	static SQLiteUtil::Column	g_SingleContainerColumnTable;
};