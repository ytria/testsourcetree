#include "SQLCosmosSyncManager.h"

#include "FileUtil.h"
#include "Sapio365Session.h"
#include "Str.h"

SQLCosmosSyncManager::SQLCosmosSyncManager()
	: O365SQLiteEngine(_YTEXTFORMAT(L"_sync%s_.ytr", Str::getStringFromNumber<uint32_t>(g_CurrentVersion).c_str()))
{
	// Update this assert when version change.
	// For old reasons, g_CurrentVersion must at least be incremented each time SQLiteEngine::GetCurrentVersion() is.
	static_assert(g_CurrentVersion == 6 && SQLiteEngine::GetCurrentVersion() == 6, "Should you increment version or test?");

	m_Sync.m_Name = _YTEXT("Sync");
	m_Sync.AddColumnRowID();
	m_Sync.SetDefaultColumnFlags(SQLiteUtil::ColumnFlags::NOTNULL);

	SQLiteUtil::Column cTenant;
	cTenant.m_Name = SqlCosmosSync::g_SyncColumnNameTenant;
	m_Sync.AddColumn(cTenant);

	SQLiteUtil::Column cDatabase;
	cDatabase.m_Name = SqlCosmosSync::g_SyncColumnNameDatabase;
	m_Sync.AddColumn(cDatabase);

	SQLiteUtil::Column cTable;
	cTable.m_Name = SqlCosmosSync::g_SyncColumnNameTable;
	m_Sync.AddColumn(cTable);

	SQLiteUtil::Column cLastSync;
	cLastSync.m_Name	= SqlCosmosSync::g_SyncColumnNameLastSync;
	cLastSync.m_Flags	= SQLiteUtil::ColumnFlags::ISDATE;
	m_Sync.AddColumn(cLastSync);


	m_Parameters.m_Name = _YTEXT("Parameters");
	m_Parameters.AddColumnRowID();
	m_Parameters.SetDefaultColumnFlags(SQLiteUtil::ColumnFlags::NOTNULL);

	m_Parameters.AddColumn(cTenant);

	SQLiteUtil::Column cName;
	cName.m_Name = SqlCosmosSync::g_ParametersColumnNameName;
	m_Parameters.AddColumn(cName);
	
	SQLiteUtil::Column cValue;
	cValue.m_Name = SqlCosmosSync::g_ParametersColumnNameValue;
	m_Parameters.AddColumn(cValue);
		
	Configure();
}

vector<SQLiteUtil::Table> SQLCosmosSyncManager::GetTables() const
{
	return { m_Sync, m_Parameters };
}

// static
SQLCosmosSyncManager& SQLCosmosSyncManager::GetInstance()
{
	static SQLCosmosSyncManager g_Instance;
	return g_Instance;
}

bool SQLCosmosSyncManager::ManageCosmosSyncDates(std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	ASSERT(p_Session);
	if (p_Session)
	{
		auto paramCosmosSingleContainer			= GetParameterValue(SqlCosmosSync::g_ParameterNameCosmosSingleContainer, p_Session);
		auto paramCosmosAccount					= GetParameterValue(SqlCosmosSync::g_ParameterNameCosmosAccount, p_Session);
		const bool isParamCosmosSingleContainer	= paramCosmosSingleContainer && paramCosmosSingleContainer.get() == SqlCosmosSync::g_ParametersValueTrue;
		if (isParamCosmosSingleContainer != p_Session->IsCosmosSingleContainer() || (paramCosmosAccount && paramCosmosAccount.get() != p_Session->GetCosmosAccount()))// no cosmos account in table: do nothing (first run)
			ClearSyncDates(p_Session);

		auto setSingle	= SetParameterValue(SqlCosmosSync::g_ParameterNameCosmosSingleContainer, p_Session->IsCosmosSingleContainer() ? SqlCosmosSync::g_ParametersValueTrue : SqlCosmosSync::g_ParametersValueFalse, p_Session);
		auto setAccount = SetParameterValue(SqlCosmosSync::g_ParameterNameCosmosAccount, p_Session->GetCosmosAccount(), p_Session);
		rvSuccess = setSingle && setAccount;
	}

	return rvSuccess;
}

boost::YOpt<wstring> SQLCosmosSyncManager::GetParameterValue(const wstring& p_ParameterName, std::shared_ptr<const Sapio365Session> p_Session)
{
	boost::YOpt<wstring> rvValue;

	ASSERT(p_Session);
	if (p_Session)
	{
		const O365Krypter krypter(p_Session);
		vector<SQLCosmosParametersRecord> parameterRecord;
		if (SelectAll<SQLCosmosParametersRecord>(krypter, m_Parameters,
												{	{ SqlCosmosSync::g_SyncColumnNameTenant, p_Session->GetTenantName(true)},
													{ SqlCosmosSync::g_ParametersColumnNameName, p_ParameterName } },
												{}, LinearMap<wstring, bool>(), parameterRecord))
			if (!parameterRecord.empty())
			{
				ASSERT(parameterRecord.size() == 1);
				rvValue = parameterRecord.front().m_Value;
			}
	}

	return rvValue;
}

bool SQLCosmosSyncManager::SetParameterValue(const wstring& p_ParameterName, const wstring& p_ParmaeterValue, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rv = false;

	ASSERT(p_Session);
	if (p_Session)
	{
		const O365Krypter krypter(p_Session);
		vector<SQLCosmosParametersRecord> parameterRecord;
		auto tenant = p_Session->GetTenantName(true);
		if (SelectAll<SQLCosmosParametersRecord>(krypter, m_Parameters,
												{	{ SqlCosmosSync::g_SyncColumnNameTenant, tenant },
													{ SqlCosmosSync::g_ParametersColumnNameName, p_ParameterName } },
												{}, LinearMap<wstring, bool>(), parameterRecord))
		{
			SQLCosmosParametersRecord newParameterRecord(m_Parameters);
			newParameterRecord.m_Tenant = tenant;
			newParameterRecord.m_Name	= p_ParameterName;
			newParameterRecord.m_Value	= p_ParmaeterValue;
			if (!parameterRecord.empty())
			{
				ASSERT(parameterRecord.size() == 1);
				newParameterRecord.SetID(parameterRecord.front().GetID());
			}

			rv = WriteRecord(krypter, &newParameterRecord);
		}
	}

	return rv;
}

YTimeDate SQLCosmosSyncManager::GetLastSyncDate(const wstring& p_Database, const wstring& p_Table, std::shared_ptr<const Sapio365Session> p_Session)
{
	YTimeDate syncDate;

	ASSERT(p_Session);
	ASSERT(!p_Database.empty());
	ASSERT(!p_Table.empty());
	
	if (p_Session)
	{
		vector<SQLCosmosSyncRecord> lastSyncRecord;
		const O365Krypter krypter(p_Session);
		if (SelectAll<SQLCosmosSyncRecord>(krypter, m_Sync,
											{	{ SqlCosmosSync::g_SyncColumnNameTenant, p_Session->GetTenantName(true)},
												{ SqlCosmosSync::g_SyncColumnNameDatabase, FileUtil::FileGetFileName(p_Database) },
												{ SqlCosmosSync::g_SyncColumnNameTable, p_Table } },
											{}, LinearMap<wstring, bool>(), lastSyncRecord))
			if (!lastSyncRecord.empty())
			{
				ASSERT(lastSyncRecord.size() == 1);
				syncDate = lastSyncRecord.front().m_LastSyncDate;
			}
	}

	return syncDate;
}

bool SQLCosmosSyncManager::SetCloudSyncDate(const wstring& p_Database, const wstring& p_Table, std::shared_ptr<const Sapio365Session> p_Session, const YTimeDate& p_NewSyncDate)
{
	bool rv = false;

	ASSERT(p_Session);
	if (p_Session)
	{
		const O365Krypter krypter(p_Session);
		vector<SQLCosmosSyncRecord> lastSyncRecord;
		auto tenant		= p_Session->GetTenantName(true);
		auto database	= FileUtil::FileGetFileName(p_Database);
		if (SelectAll<SQLCosmosSyncRecord>(krypter, m_Sync,
											{	{ SqlCosmosSync::g_SyncColumnNameTenant, tenant },
												{ SqlCosmosSync::g_SyncColumnNameDatabase, database },
												{ SqlCosmosSync::g_SyncColumnNameTable, p_Table } },
											{}, LinearMap<wstring, bool>(), lastSyncRecord))
		{
			SQLCosmosSyncRecord newSyncRecord(m_Sync);
			newSyncRecord.m_Tenant			= tenant;
			newSyncRecord.m_Database		= database;
			newSyncRecord.m_Table			= p_Table;
			newSyncRecord.m_LastSyncDate	= p_NewSyncDate;
			if (!lastSyncRecord.empty())
			{
				ASSERT(lastSyncRecord.size() == 1);
				newSyncRecord.SetID(lastSyncRecord.front().GetID());
			}

			rv = WriteRecord(krypter, &newSyncRecord);
		}
	}

	return rv;
}

bool SQLCosmosSyncManager::ClearSyncDates(std::shared_ptr<const Sapio365Session> p_Session)
{
	ASSERT(p_Session);

	return p_Session ?	executeAtomicStatement(_YFORMAT(L"DELETE FROM %s WHERE %s = '%s'", 
														m_Sync.GetSQLcompliantName().c_str(), SqlCosmosSync::g_SyncColumnNameTenant.c_str(), p_Session->GetTenantName(true).c_str())) :
						false;
}
