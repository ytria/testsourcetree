#pragma once

#include "CosmosUtil.h"
#include "DlgDoubleProgressCommon.h"
#include "Document.h"
#include "O365SQLiteEngine.h"
#include "RoleDelegationUtil.h"
#include "Sapio365Session.h"

class RoleDelegationSQLite : public O365SQLiteEngine
{
public:
	RoleDelegationSQLite();
	virtual ~RoleDelegationSQLite() = default;

	virtual vector<SQLiteUtil::Table> GetTables() const override;
	
	// achtung: all these public getters do not return objects with status RoleDelegationUtil::STATUS_REMOVED

	bool			LoadRoleDelegations(const PooledString& p_UserID, vector<RoleDelegation>& p_Delegations, std::shared_ptr<const Sapio365Session> p_Session);// false: something went wrong
	RoleDelegation	LoadRoleDelegation(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);

	bool IsSapio365AdminScopeSetForUser(std::shared_ptr<const Sapio365Session> p_Session, const PooledString& p_UserID, const RoleDelegationUtil::RBAC_AdminScope p_Scope);

	RoleDelegationUtil::Key							ReadKey(const int64_t p_KeyID, std::shared_ptr<const Sapio365Session> p_Session);
	RoleDelegationUtil::Delegation					ReadDelegation(const int64_t p_DelegationID, std::shared_ptr<const Sapio365Session> p_Session);
	RoleDelegationUtil::Filter						ReadFilter(const int64_t p_FilterID, std::shared_ptr<const Sapio365Session> p_Session);
	
	vector<RoleDelegationUtil::Delegation>			ReadDelegationsFeaturingThisKey(const int64_t p_KeyID, std::shared_ptr<const Sapio365Session> p_Session);// Achtung: returned objects are not loaded (with filters etc.)
	vector<RoleDelegationUtil::Delegation>			ReadDelegationsFeaturingThisFilter(const int64_t p_FilterID, std::shared_ptr<const Sapio365Session> p_Session);// loads core objects

	vector<RoleDelegationUtil::DelegationPrivilege>	ReadDelegationPrivileges(const int64_t p_DelegationID, std::shared_ptr<const Sapio365Session> p_Session);
	vector<RoleDelegationUtil::DelegationFilter>	ReadDelegationFilters(const int64_t p_DelegationID, std::shared_ptr<const Sapio365Session> p_Session);
	vector<RoleDelegationUtil::DelegationMember>	ReadDelegationMembers(const int64_t p_DelegationID, std::shared_ptr<const Sapio365Session> p_Session);

	int64_t CountKeys(std::shared_ptr<const Sapio365Session> p_Session);

	vector<RoleDelegationUtil::Key>		ReadKeys(std::shared_ptr<const Sapio365Session> p_Session);
	vector<RoleDelegationUtil::Filter>	ReadFilters(std::shared_ptr<const Sapio365Session> p_Session);

	RoleDelegationUtil::Key					Write(const RoleDelegationUtil::BusinessKey& p_Key, std::shared_ptr<const Sapio365Session> p_Session);
	RoleDelegationUtil::Delegation			Write(const RoleDelegationUtil::BusinessDelegation& p_Role, std::shared_ptr<const Sapio365Session> p_Session);
	RoleDelegationUtil::Filter				Write(const RoleDelegationUtil::BusinessFilter& p_Filter, std::shared_ptr<const Sapio365Session> p_Session);
	RoleDelegationUtil::DelegationFilter	Write(const RoleDelegationUtil::BusinessDelegationFilter& p_DelegationFilter, std::shared_ptr<const Sapio365Session> p_Session);
	RoleDelegationUtil::DelegationMember	Write(const RoleDelegationUtil::BusinessDelegationMember& p_DelegationMember, std::shared_ptr<const Sapio365Session> p_Session);
	RoleDelegationUtil::DelegationPrivilege	Write(const RoleDelegationUtil::BusinessDelegationPrivilege& p_DelegationPrivilege, std::shared_ptr<const Sapio365Session> p_Session);
	RoleDelegationUtil::sapio365Admin		Write(const RoleDelegationUtil::BusinessSapio365Admin& p_sapio365Admin, std::shared_ptr<const Sapio365Session> p_Session);

	bool WriteRoleDelegationRecord(RoleDelegationUtil::RoleDelegationRecord* p_RDrecord, std::shared_ptr<const Sapio365Session> p_Session);

	SQLiteUtil::Table& GetTableKeys();
	SQLiteUtil::Table& GetTableFilters();
	SQLiteUtil::Table& GetTableDelegations();
	SQLiteUtil::Table& GetTableAdmin();

	bool UpdateFromCosmos(const CosmosUtil::SQLupdateData& p_AllData, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Prog);
	
	wstring	GetSQLSelectStatementDelegationsForThisUser(const PooledString& p_UserID);

	void GetAdminAccessForSession(std::shared_ptr<const Sapio365Session> p_Session, set<RoleDelegationUtil::RBAC_AdminScope>& p_AA);

	bool SelectAllForUpdateToCloud(RoleDelegationUtil::CosmosUpdateData& p_AllData, std::shared_ptr<const Sapio365Session> p_Session);// for Cosmos update
	void CompleteUpdateToCloud(std::shared_ptr<const Sapio365Session> p_Session);

	uint32_t GetVersion() const override
	{
		return g_CurrentVersion;
	}

private:
	void	addDefaultColumns(SQLiteUtil::Table& p_Table) const;
	bool	prepareUpdateInfo(RoleDelegationUtil::BusinessData* p_Generic) const;

	RoleDelegationUtil::DelegationPrivilege	readDelegationPrivilege(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);
	RoleDelegationUtil::DelegationFilter	readDelegationFilter(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);
	RoleDelegationUtil::DelegationMember	readDelegationMember(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);

	RoleDelegationUtil::sapio365Admin		readsapio365Admin(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);

	template <class SQLRecordtype>
	bool updateSQLfromCosmosDocuments(const SQLiteUtil::Table& p_Table, const vector<Cosmos::Document>& p_CosmosDocuments, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Prog);

	// load all data, regardless of status - for push to COSMOS - TODO check SelectAll errors
	void readForUpdateToCloudKeys(std::shared_ptr<const Sapio365Session> p_Session);
	void readForUpdateToCloudFilters(std::shared_ptr<const Sapio365Session> p_Session);
	void readForUpdateToCloudDelegationFilters(std::shared_ptr<const Sapio365Session> p_Session);
	void readForUpdateToCloudDelegationMembers(std::shared_ptr<const Sapio365Session> p_Session);
	void readForUpdateToCloudDelegationPrivileges(std::shared_ptr<const Sapio365Session> p_Session);
	void readForUpdateToCloudDelegations(std::shared_ptr<const Sapio365Session> p_Session);
	void readForUpdateToCloudsapio365Admin(std::shared_ptr<const Sapio365Session> p_Session);

	SQLiteUtil::Table m_Keys;
	SQLiteUtil::Table m_Filters;
	SQLiteUtil::Table m_DelegationMembers;
	SQLiteUtil::Table m_DelegationFilters;
	SQLiteUtil::Table m_DelegationPrivileges;
	SQLiteUtil::Table m_Delegations;
	SQLiteUtil::Table m_sapio365Admin;

	vector<RoleDelegationUtil::Key>					m_ForCloudKeys;
	vector<RoleDelegationUtil::Filter>				m_ForCloudFitlers;
	vector<RoleDelegationUtil::DelegationFilter>	m_ForCloudDelegationFilters;
	vector<RoleDelegationUtil::DelegationMember>	m_ForCloudDelegationMembers;
	vector<RoleDelegationUtil::DelegationPrivilege>	m_ForCloudDelegationPrivileges;
	vector<RoleDelegationUtil::Delegation>			m_ForCloudDelegations;
	vector<RoleDelegationUtil::sapio365Admin>		m_ForCloudsapio365Admin;

	static constexpr uint32_t g_CurrentVersion = 6;
};