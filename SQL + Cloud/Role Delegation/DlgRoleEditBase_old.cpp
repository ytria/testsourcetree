#include "DlgRoleEditBase_old.h"

const wstring DlgRoleEditBase_old::g_VarName	= _YTEXT("NAME");
const wstring DlgRoleEditBase_old::g_VarInfo	= _YTEXT("INFO");

void DlgRoleEditBase_old::generateJSONScriptData()
{
	initMain(getInitMainTitle());

	addCategory(YtriaTranslate::Do(aclEZApp_OnSwitchID_1, _YLOC("Information")).c_str());

	auto businessData = getBusinessData();
	ASSERT(nullptr != businessData);
	if (nullptr != businessData)
	{
		addStringEditor(g_VarName, YtriaTranslate::Do(AutomatedApp_ProcessWndProcException_17, _YLOC("Name")).c_str(), _YTEXT(""), businessData->m_Name);
		addStringEditor(g_VarInfo, YtriaTranslate::Do(ColumnInfoGrid_AddColumnData_3, _YLOC("Info")).c_str(), YtriaTranslate::Do(DlgRoleEditBase_old_generateJSONScriptData_4, _YLOC("Enter information or description")).c_str(), businessData->m_Info);
	}

	generateJSONScriptDataSpecific();

	getGenerator().addApplyButton(YtriaTranslate::Do(DlgAutomationInputHTML_generateJSONScriptData_1, _YLOC("OK")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgAutomationInputHTML_generateJSONScriptData_2, _YLOC("Cancel")).c_str());
}

bool DlgRoleEditBase_old::processPostedData(const IPostedDataTarget::PostedData& data)
{
	auto businessData = getBusinessData();
	ASSERT(nullptr != businessData);
	if (nullptr != businessData)
		for (const auto& item : data)
			if (MFCUtil::StringMatchW(item.first, g_VarName))
				businessData->m_Name = item.second;
			else if (MFCUtil::StringMatchW(item.first, g_VarInfo))
				businessData->m_Info = item.second;
	
	return processPostedDataSpecific(data);
}

void DlgRoleEditBase_old::addStringEditor(const wstring& p_PropName, const wstring& p_PropLabel, const wstring& p_PropTooltip, const wstring& p_Value)
{
	getGenerator().Add(WithTooltip<StringEditor>({ p_PropName, p_PropLabel, EditorFlags::REQUIRED | EditorFlags::DIRECT_EDIT, p_Value }, p_PropTooltip));
}

void DlgRoleEditBase_old::addComboEditor(const wstring& p_PropName, const wstring& p_PropLabel, const wstring& p_PropTooltip, const wstring& p_Value, const ComboEditor::LabelsAndValues& p_LabelsAndValues)
{
	getGenerator().Add(WithTooltip<ComboEditor>({ { p_PropName, p_PropLabel, EditorFlags::REQUIRED | EditorFlags::DIRECT_EDIT, p_Value }, p_LabelsAndValues }, p_PropTooltip));
}

void DlgRoleEditBase_old::addCategory(const wstring& p_Title)
{
	getGenerator().addCategory(p_Title, CategoryFlags::EXPAND_BY_DEFAULT);
}

void DlgRoleEditBase_old::addNumberEditor(const wstring& p_PropName, const wstring& p_PropLabel, const wstring& p_PropTooltip, const double& p_Value)
{
	getGenerator().Add(WithTooltip<NumberEditor>({ p_PropName, p_PropLabel, EditorFlags::REQUIRED | EditorFlags::DIRECT_EDIT, p_Value }, p_PropTooltip));
}