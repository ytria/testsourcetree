#pragma once

#include "DlgRoleEditBase_old.h"

class DlgRoleEditAddMember : public DlgRoleEditBase_old
{
public:
	DlgRoleEditAddMember(CWnd* p_Parent);
	virtual ~DlgRoleEditAddMember();

	const RoleDelegationUtil::BusinessDelegationMember&	GetUser() const;
	void												SetUser(const RoleDelegationUtil::BusinessDelegationMember& p_User);

protected:
	virtual void								generateJSONScriptDataSpecific() override;
	virtual bool								processPostedDataSpecific(const IPostedDataTarget::PostedData& data) override;
	virtual wstring								getDialogTitle() const override;
	virtual RoleDelegationUtil::BusinessData*	getBusinessData() override;
	virtual wstring								getInitMainTitle() const override;

private:
	RoleDelegationUtil::BusinessDelegationMember m_User;

	static const wstring g_VarUserPrincipalName;
};