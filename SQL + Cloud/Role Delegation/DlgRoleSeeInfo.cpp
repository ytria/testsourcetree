#include "DlgRoleSeeInfo.h"

#include "BusinessGroupConfiguration.h"
#include "BusinessSiteConfiguration.h"
#include "BusinessUserConfiguration.h"
#include "DlgRoleEditKey.h"
#include "JSONUtil.h"
#include "RoleDelegationManager.h"
#include "Str.h"

std::map<wstring, int, Str::keyLessInsensitive> DlgRoleSeeInfo::g_Sizes;

namespace
{
	class ExternalFctsHandler : public CCmdTarget
	{
	public:
		ExternalFctsHandler(std::shared_ptr<Sapio365Session> p_Session, RoleDelegation& p_Delegation, CWnd* p_Parent)
			: m_Session(p_Session)
			, m_Parent(p_Parent),
			m_Delegation(p_Delegation)
		{
			EnableAutomation();
		}

		BSTR GetLicensePool(LPCWSTR p_CredName)
		{
			json::value jsonLicenses = json::value::array();

			size_t idx = 0;
			for (const auto& skuLimit : m_Delegation.GetDelegation().GetSkuAccessLimits())
			{
				wstring name = MFCUtil::encodeToHtmlEntitiesExceptTags(skuLimit.first);
				jsonLicenses[idx++] =
					JSON_BEGIN_OBJECT
						JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(name)),
						JSON_PAIR_KEY(_YTEXT("field"))		JSON_PAIR_VALUE(JSON_STRING(name)),
						JSON_PAIR_KEY(_YTEXT("value"))		JSON_PAIR_VALUE(JSON_STRING(std::to_wstring(skuLimit.second))),
					JSON_END_OBJECT;
			}
			return _bstr_t(jsonLicenses.serialize().c_str());
		}

	private:
		DECLARE_DISPATCH_MAP()

		std::shared_ptr<Sapio365Session> m_Session;
		std::map<wstring, wstring> m_CredRequests;
		RoleDelegation& m_Delegation;
		CWnd* m_Parent;
	};

	BEGIN_DISPATCH_MAP(ExternalFctsHandler, CCmdTarget)
		DISP_FUNCTION(ExternalFctsHandler, "getLicensePool", GetLicensePool, VT_BSTR, VTS_WBSTR)
	END_DISPATCH_MAP()
}

DlgRoleSeeInfo::DlgRoleSeeInfo(int64_t p_RoleId, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent)
	: DlgRoleEditBase(p_Parent)
	, m_Delegation(RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(p_RoleId, p_Session))
{
	m_HtmlView->SetExternal(std::make_shared<ExternalFctsHandler>(p_Session, m_Delegation, this));
	// Load hbs sizes
	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_RBACCONFIGSEEINFO_SIZE, g_Sizes);
		ASSERT(success);
	}
}

RoleDelegationUtil::BusinessData* DlgRoleSeeInfo::getBusinessData()
{
    return &m_Delegation.GetDelegation();
}

bool DlgRoleSeeInfo::isSpecificReadOnly() const
{
	return true;
}

int DlgRoleSeeInfo::getMinimumHeight() const
{
	const auto it = g_Sizes.find(_YTEXT("min-height"));
	ASSERT(g_Sizes.end() != it);
	if (g_Sizes.end() != it)
		return it->second;
	return DlgRoleEditBase::getMinimumHeight();
}

void DlgRoleSeeInfo::generateJSONScriptDataSpecific(web::json::value& p_JsonValue)
{
	auto& items		= p_JsonValue.as_array();
	auto itemIndex	= items.size();

	const wstring checked	= _YTEXT("x");
	const wstring unchecked = _YTEXT("");

	// flags / options
	items[itemIndex] =
		JSON_BEGIN_OBJECT
            JSON_BEGIN_PAIR
                _YTEXT("hbs"),  JSON_STRING(_YTEXT("rbacProperty"))
            JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("introName"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_T("Options")))
            JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("introTextOne"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_T("Role logging and enforcement parameters.")))
            JSON_END_PAIR,

			JSON_BEGIN_PAIR
                _YTEXT("propertyLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_T("Flags")))
            JSON_END_PAIR,

			JSON_BEGIN_PAIR
                _YTEXT("isReadOnly"),  JSON_STRING(checked)
            JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("propertyChoice"),
				JSON_BEGIN_ARRAY
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("value"),	JSON_STRING(_YTEXT("LogSessionLogin")),// value seems useless
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("label"),	JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(RoleDelegationUtil::BusinessDelegation::g_LabelLogSessionLogin))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("default"),	JSON_STRING(m_Delegation.GetDelegation().IsLogSessionLogin() ? checked : unchecked)
						JSON_END_PAIR,
					JSON_END_OBJECT,
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("value"),	JSON_STRING(_YTEXT("LogModuleOpening")),
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("label"),	JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(RoleDelegationUtil::BusinessDelegation::g_LabelLogModuleOpening))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("default"),	JSON_STRING(m_Delegation.GetDelegation().IsLogModuleOpening() ? checked : unchecked)
						JSON_END_PAIR,
					JSON_END_OBJECT,
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("value"),	JSON_STRING(_YTEXT("EnforceRole")),
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("label"),	JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(RoleDelegationUtil::BusinessDelegation::g_LabelEnforceRole))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("default"),	JSON_STRING(m_Delegation.GetDelegation().IsEnforceRole() ? checked : unchecked)
						JSON_END_PAIR,
					JSON_END_OBJECT,
                JSON_END_ARRAY
            JSON_END_PAIR,
		JSON_END_OBJECT;

	itemIndex++;

	// *********** List of Permissions (a.k.a. privileges)
	items[itemIndex] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("hbs"), JSON_STRING(_YTEXT("rbacPrivilegesStatus"))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("introName"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(FrameDriveItems_customizeActionsRibbonTab_1, _YLOC("Permissions")).c_str()))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("introTextOne"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleSeeInfo_generateJSONScriptDataSpecific_2, _YLOC("List of all the permissions provided to the current Role.")).c_str()))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("accessLabel"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(FrameDriveItems_customizeActionsRibbonTab_1, _YLOC("Permissions:")).c_str()))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("accessCount"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleSeeInfo_generateJSONScriptDataSpecific_6, _YLOC("( % selected)")).c_str()))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("accessChoice"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR,
		JSON_END_OBJECT;


    {
        auto& choices = items[itemIndex].as_object()[_YTEXT("accessChoice")].as_array();
        for (uint32_t scope = RoleDelegationUtil::RBAC_Scope::RBAC_SCOPE_USER; scope < RoleDelegationUtil::RBAC_Scope::RBAC_SCOPE_MAX; scope++)
        {
            const auto& scopeText = RoleDelegationUtil::GetScopeJSONKeyAndLabel(scope);
            choices[scope] =
                JSON_BEGIN_OBJECT
					JSON_PAIR_KEY(_YTEXT("value"))         JSON_PAIR_VALUE(JSON_STRING(scopeText.first)),
					JSON_PAIR_KEY(_YTEXT("label"))         JSON_PAIR_VALUE(JSON_STRING(scopeText.second)),
					JSON_BEGIN_PAIR
						_YTEXT("subChoice"),
						JSON_BEGIN_ARRAY
						JSON_END_ARRAY
					JSON_END_PAIR,
                JSON_END_OBJECT;

            auto& subChoices = choices[scope].as_object()[_YTEXT("subChoice")].as_array();
            const auto& scopePrivileges = RoleDelegationUtil::GetPrivileges(scope);
            size_t subChoiceIndex = 0;
            for (size_t k = 0; k < scopePrivileges.size(); k++)
            {
                const auto& thisPrivilege = scopePrivileges[k];

                if (m_Delegation.HasPrivilege(thisPrivilege.m_Privilege))
                {
                    subChoices[subChoiceIndex++] =
                        JSON_BEGIN_OBJECT
							JSON_PAIR_KEY(_YTEXT("value"))         JSON_PAIR_VALUE(JSON_STRING(Str::getStringFromNumber<uint32_t>(thisPrivilege.m_Privilege))),
							JSON_PAIR_KEY(_YTEXT("label"))         JSON_PAIR_VALUE(JSON_STRING(thisPrivilege.m_Label)),
                        JSON_END_OBJECT;
                }
            }
        }
    }

	// *********** List of Scopes (a.k.a. filters)
	++itemIndex;
	items[itemIndex] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("hbs"), JSON_STRING(_YTEXT("rbacFiltersStatus"))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("introName"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleSeeInfo_generateJSONScriptDataSpecific_4, _YLOC("Scopes")).c_str()))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("introTextOne"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleSeeInfo_generateJSONScriptDataSpecific_5, _YLOC("List of Scopes set for the current Role.")).c_str()))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("titleProperty"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(ActivitySQLiteLogger_ActivitySQLiteLogger_18, _YLOC("Property")).c_str()))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("titleCondition"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(GLOBAL_BEGIN_MESSAGE_MAP_1, _YLOC("Condition")).c_str()))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("titleValue"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(ColumnInfoGrid_customizeGrid_3, _YLOC("Value")).c_str()))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("allscopes"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR,
		JSON_END_OBJECT;

    {
        auto& choices = items[itemIndex].as_object()[_YTEXT("allscopes")].as_array();

        size_t choiceIndex = 0;
        for (const auto& filter : m_Delegation.GetFilters())
        {
            const auto& objType = filter.first;

            choices[choiceIndex] =
                JSON_BEGIN_OBJECT
					JSON_PAIR_KEY(_YTEXT("target")) JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgRoleSeeInfo_generateJSONScriptDataSpecific_10, _YLOC("Target: %1"),objType.c_str()))),
                JSON_BEGIN_PAIR
					_YTEXT("scopes"),
					JSON_BEGIN_ARRAY
					JSON_END_ARRAY
                JSON_END_PAIR,
                JSON_END_OBJECT;

			const BusinessConfigurationBase* bcb = nullptr;
			if (MFCUtil::StringMatchW(objType, _YTEXT("User")))
				bcb = &BusinessUserConfiguration::GetInstance();
			else if (MFCUtil::StringMatchW(objType, _YTEXT("Group")))
				bcb = &BusinessGroupConfiguration::GetInstance();
			else if (MFCUtil::StringMatchW(objType, _YTEXT("Site")))
				bcb = &BusinessSiteConfiguration::GetInstance();
			else
				ASSERT(false);

            auto& subChoices = choices[choiceIndex].as_object()[_YTEXT("scopes")].as_array();
            size_t subchoiceIndex = 0;
            for (const auto& propRule : filter.second)
            {
				const auto& propName = nullptr != bcb ? bcb->GetTitle(propRule.m_Property) : propRule.m_Property;
                subChoices[subchoiceIndex++] =
                    JSON_BEGIN_OBJECT
						JSON_PAIR_KEY(_YTEXT("property"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(propName))),
						JSON_PAIR_KEY(_YTEXT("filterType"))		JSON_PAIR_VALUE(JSON_STRING(RoleDelegationUtil::GetTaggerMatchMethod(propRule.m_TaggerMatchMethod))),
						JSON_PAIR_KEY(_YTEXT("value"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(propRule.m_Value))),
                    JSON_END_OBJECT;
            }
            ++choiceIndex;
        }
    }

	// *********** List of License Delegations (a.k.a. LicensePools)
	// Ideally, we should NOT show the following section if NO LicensePool has been set 

    if (!m_Delegation.GetDelegation().GetSkuAccessLimits().empty())
    {
	    ++itemIndex;
	    items[itemIndex] =
		    JSON_BEGIN_OBJECT
                JSON_BEGIN_PAIR
                    _YTEXT("hbs"),  JSON_STRING(_YTEXT("rbacLicensePoolsStatus"))
                JSON_END_PAIR,

		        JSON_BEGIN_PAIR
                    _YTEXT("introName"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_YTEXT("Delegation Privileges for Licenses")))
                JSON_END_PAIR,

		        JSON_BEGIN_PAIR
                    _YTEXT("introTextOne"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_YTEXT("List of purchased licenses available to this role.")))
                JSON_END_PAIR,

			    JSON_BEGIN_PAIR
                    _YTEXT("licensePools"),
				    JSON_BEGIN_ARRAY
                    JSON_END_ARRAY
                JSON_END_PAIR,
		    JSON_END_OBJECT;

        auto& choices = items[itemIndex].as_object()[_YTEXT("licensePools")].as_array();
    }
}

bool DlgRoleSeeInfo::processPostedDataSpecific(const IPostedDataTarget::PostedData& data)
{
    return true;
}

wstring DlgRoleSeeInfo::getDialogTitle() const
{
    return YtriaTranslate::Do(DlgRoleSeeInfo_getDialogTitle_1, _YLOC("Role Info")).c_str();
}
