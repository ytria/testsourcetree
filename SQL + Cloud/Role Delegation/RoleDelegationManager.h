#pragma once

#include "BusinessUser.h"
#include "ManagerSQLandCloud.h"
#include "RoleDelegationCosmos.h"
#include "RoleDelegationSQLite.h"
#include "RoleDelegationUtil.h"

class CachedObject;
class BusinessObject;

class RoleDelegationManager : public ManagerSQLandCloud
{
public:
	~RoleDelegationManager() = default;

	static RoleDelegationManager& GetInstance();

	template <class BusinessOrCachedObject>
	bool HasPrivilege(const BusinessOrCachedObject& p_Obj, const RoleDelegationUtil::RBAC_Privilege p_Privilege, std::shared_ptr<const Sapio365Session> p_Session);
	bool DelegationHasPrivilege(const int64_t p_DelegationID, const RoleDelegationUtil::RBAC_Privilege p_Privilege, std::shared_ptr<const Sapio365Session> p_Session);

	const RoleDelegation&	GetRoleDelegationFromCache(const int64_t p_DelegationID, std::shared_ptr<const Sapio365Session> p_Session);// 1st time: load from SQLite, then: get from buffer
	void					RemoveFromCache(const int64_t p_DelegationID);
	void					ClearCache();

	RoleDelegation							LoadRoleDelegationFromSQLite(const int64_t p_IDdelegation, std::shared_ptr<const Sapio365Session> p_Session);// loads full object
	vector<RoleDelegationUtil::Delegation>	ReadDelegationsFeaturingThisKey(const int64_t p_KeyID, std::shared_ptr<const Sapio365Session> p_Session);// loads core objects
	vector<RoleDelegationUtil::Delegation>	ReadDelegationsFeaturingThisFilter(const int64_t p_FilterID, std::shared_ptr<const Sapio365Session> p_Session);// loads core objects

	bool GetRoleDelegationsForThisUser(const PooledString& p_UserID, vector<RoleDelegation>& p_Delegations, std::shared_ptr<const Sapio365Session> p_Session);
	bool IsSapio365AdminScopeSetForUser(const PooledString& p_UserID, const RoleDelegationUtil::RBAC_AdminScope p_Scope, std::shared_ptr<const Sapio365Session> p_Session);

	RoleDelegationSQLite& GetRoleDelegationSQLite();
	RoleDelegationCosmos& GetRoleDelegationCosmos();

	// Role delegation building blocks
	enum class ErrorSource
	{
		ERROR_NONE,
		ERROR_SQLITE,
		ERROR_COSMOS
	};
	template<class T>
	ErrorSource Write(T& p_BusinessThing, std::shared_ptr<const Sapio365Session> p_Session);// p_Session used for Cosmos - TODO process errors (returns false), only traced as of 03.2019

	virtual void InitAfterSessionOrLicenseWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress) override;// p_Session used for Cosmos

	int64_t CountKeys(std::shared_ptr<const Sapio365Session> p_Session);

	RoleDelegationUtil::BusinessDelegation	GetDelegation(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);// loads core object (no filter, member etc)
	RoleDelegationUtil::BusinessKey			GetKey(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);
	RoleDelegationUtil::BusinessFilter		GetFilter(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session);

	vector<RoleDelegationUtil::Key>		GetKeys(std::shared_ptr<const Sapio365Session> p_Session);
	vector<RoleDelegationUtil::Filter>	GetFilters(std::shared_ptr<const Sapio365Session> p_Session);

	// p_Session used for Cosmos
	bool HasSessionAccessToConfig(std::shared_ptr<const Sapio365Session> p_Session) const;

	bool UpdateSQLiteFromCloud(std::shared_ptr<const Sapio365Session> p_Session, CWnd* p_Parent);// data updated from Cosmos to SQLite
	bool UpdateSQLiteFromCloudIfLeaseExpiredAndInvalidateDelegation(std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent);// data updated from Cosmos to SQLite if the session is intolerably too old - returns true if session 

	void GetAdminAccessForSession(std::shared_ptr<const Sapio365Session> p_Session, set<RoleDelegationUtil::RBAC_AdminScope>& p_AA);

	bool IsRoleEnforced(const int64_t p_RoleID, std::shared_ptr<const Sapio365Session> p_Session);

private:
	RoleDelegationManager();

	bool updateSQLiteFromCloud(std::shared_ptr<const Sapio365Session> p_Session, const CosmosUtil::SQLupdateData& p_CosmosDocuments, DlgDoubleProgressCommon& p_Progress);// data updated from Cosmos to SQLite
	bool pushSQLiteToCloud(std::shared_ptr<const Sapio365Session> p_Session, const CosmosUtil::SQLupdateData& p_CosmosDocuments, DlgDoubleProgressCommon& p_Progress);// data updated from SQLite to Cosmos

	RoleDelegationSQLite m_RoleDelegationSQLite;// Jesus
	RoleDelegationCosmos m_RoleDelegationCosmos;// Holy Spirit

	map<int64_t, RoleDelegation> m_DelegationsCache;// ID - delegation

	YTimeDate m_SessionStart;
};

#include "RoleDelegationManager.hpp"