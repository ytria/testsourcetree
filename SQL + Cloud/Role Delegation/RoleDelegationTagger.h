#pragma once

#include "Sapio365Session.h"
#include "JsonSerializeUtil.h"

class RoleDelegationTagger
{
public:
	RoleDelegationTagger(std::shared_ptr<const Sapio365Session> p_Session, BusinessObject* p_BusinessObject);
	RoleDelegationTagger();

protected:
	void deserializeStringAndTagObject(const JsonSerializeUtil::String& p_PropName, PooledString& p_Value, const web::json::object& p_Json);
	void deserializeStringAndTagObject(const JsonSerializeUtil::String& p_PropName, boost::YOpt<PooledString>& p_Value, const web::json::object& p_Json);
	void deserializeTimeDateAndTagObject(const JsonSerializeUtil::String& p_PropName, boost::YOpt<YTimeDate>& p_Value, const web::json::object& p_Json);
	void deserializeDateOnlyAndTagObject(const JsonSerializeUtil::String& p_PropName, boost::YOpt<YTimeDate>& p_Value, const web::json::object& p_Json);
	void deserializeBoolAndTagObject(const JsonSerializeUtil::String& p_PropName, boost::YOpt<bool>& p_Value, const web::json::object& p_Json);
    bool deserializeVectorStringAndTagObject(IDatedJsonDeserializer& p_Deserializer, const vector<PooledString>& p_Vector, PooledString p_PropName, const web::json::object& p_Json);

    void markPropertyAsChecked(const JsonSerializeUtil::String& p_PropName);

	void tagObject(const JsonSerializeUtil::String& p_PropName, const PooledString& p_Value);
	void tagObject(const JsonSerializeUtil::String& p_PropName, const boost::YOpt<PooledString>& p_Value);
	void tagObject(const JsonSerializeUtil::String& p_PropName, const boost::YOpt<YTimeDate>& p_Value);
	void tagObject(const JsonSerializeUtil::String& p_PropName, const boost::YOpt<bool>& p_Value);
	void tagObjectFromComputedProperties();// call this at the end of the deserialization process - all core properties need to be available

	template<typename ScopeBusinessObjectType> // BusinessUser, BusinessGroup, BusinessSite
	bool containsAllRequiredProperties(const web::json::object& p_Object) const;

    const Sapio365Session& GetSession() const;

private:
	void tagObjectFromValueAsString(const JsonSerializeUtil::String& p_PropName, const wchar_t* p_Value);
	bool propertyMatch(const RoleDelegationUtil::BusinessFilter& p_Filter, const wchar_t* p_DeserializedValue) const;
	bool isComputedProperty(const wstring& p_PropName) const;

	std::shared_ptr<const Sapio365Session> m_Session;
	// to optimize perf, store object to tag & its type
	BusinessObject*												m_BusinessObject;
	wstring														m_ObjectType;
	map<wstring, vector<RoleDelegationUtil::BusinessFilter>>	m_DelegationFilters;// property name - filters
	bool														m_BusinessObjectFailedDelegationFilters;
};

template<typename ScopeBusinessObjectType> // BusinessUser, BusinessGroup, BusinessSite
bool RoleDelegationTagger::containsAllRequiredProperties(const web::json::object& p_Object) const
{
	ScopeBusinessObjectType dummy;
	return std::none_of(m_DelegationFilters.begin(), m_DelegationFilters.end(), [&p_Object, &dummy](const auto& f)
	{
		auto props = dummy.GetProperties(f.first);
		return std::any_of(props.begin(), props.end(), [&p_Object](const auto& p)
		{
			return p_Object.end() == p_Object.find(p);
		});
	});
}
