#include "RoleDelegationManager.h"

#include "BusinessObject.h"
#include "BusinessDirectoryRole.h"
#include "CachedObject.h"
#include "CosmosManager.h"
#include "CosmosUtil.h"
#include "DlgDoubleProgressCommon.h"
#include "Document.h" //Cosmos document - TODO rename this file
#include "FileUtil.h"
#include "safeTaskCall.h"
#include "Sapio365Session.h"
#include "TraceIntoFile.h"

RoleDelegationManager::RoleDelegationManager(): ManagerSQLandCloud(),
	m_SessionStart(YTimeDate::GetCurrentTimeDate())
{
	if (!m_RoleDelegationSQLite.Configure())
		SetError(SQLCFG_ERROR);
}

void RoleDelegationManager::InitAfterSessionOrLicenseWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress)
{
	CWaitCursor _;

	ASSERT(p_Session);
	if (IsReadyToRun() && p_Session)
	{
		if (m_RoleDelegationCosmos.Configure(m_RoleDelegationSQLite, p_Session))
		{
			CosmosUtil::SQLupdateData cosmosDocuments;
			p_Progress.SetCounterTotal2(0);
			p_Progress.SetText2(_T("Loading RBAC..."));
			if (m_RoleDelegationCosmos.SelectAll(cosmosDocuments, p_Session, true))// fetch from Cosmos
			{
				updateSQLiteFromCloud(p_Session, cosmosDocuments, p_Progress);
				pushSQLiteToCloud(p_Session, cosmosDocuments, p_Progress);
			}
		}
		
		if (m_RoleDelegationCosmos.GetLastError() != CosmosSQLiteBridge::COSMOS_NOERROR &&
			m_RoleDelegationCosmos.GetLastError() != CosmosSQLiteBridge::SESSION_NOTREADYFORCOSMOS) // allowed to run on local machine
			SetError(COSMOSCFG_ERROR);
	}
}

// static
RoleDelegationManager& RoleDelegationManager::GetInstance()
{
	static RoleDelegationManager g_Instance;
	return g_Instance;
}

bool RoleDelegationManager::HasSessionAccessToConfig(std::shared_ptr<const Sapio365Session> p_Session) const
{
	return Sapio365Session::IsAdminRBAC(p_Session);
}

bool RoleDelegationManager::UpdateSQLiteFromCloud(std::shared_ptr<const Sapio365Session> p_Session, CWnd* p_Parent)
{
	ASSERT(p_Session);
	CosmosUtil::SQLupdateData cosmosDocuments;
	if (p_Session && m_RoleDelegationCosmos.SelectAll(cosmosDocuments, p_Session, true))// fetch from Cosmos
	{
		bool result = false;
		DlgDoubleProgressCommon::RunModal(_T("Updating data from cloud"), p_Parent, [this, p_Session, &cosmosDocuments , &result](auto prog)
			{
				result = updateSQLiteFromCloud(p_Session, cosmosDocuments, *prog);
			});
		return result;
	}
	return false;
}

bool RoleDelegationManager::UpdateSQLiteFromCloudIfLeaseExpiredAndInvalidateDelegation(std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent)
{
	bool rvInvalidateRBAC = false;

	ASSERT(p_Session);
	if (p_Session && p_Session->IsUseRoleDelegation() && p_Session->IsReadyForCosmos() && YTimeDate::GetCurrentTimeDate().DifferenceInSeconds(m_SessionStart) > 30 * 60)// force refresh from cloud every 30 minutes
	{
		m_SessionStart = YTimeDate::GetCurrentTimeDate();
		TraceIntoFile::trace(_YDUMP("RoleDelegationManager"), _YDUMP("UpdateSQLiteFromCloudIfLeaseExpiredAndInvalidateDelegation"), _YTEXT("Session RBAC expired, refresh from Cosmos"));

		auto rdBeforeUpdate = GetRoleDelegationFromCache(p_Session->GetRoleDelegationID(), p_Session);
		bool updated		= UpdateSQLiteFromCloud(p_Session, p_Parent);// clears cache
		auto rdAfterUpdate	= GetRoleDelegationFromCache(p_Session->GetRoleDelegationID(), p_Session);
		if (rdAfterUpdate.HasChangedComparedTo(rdBeforeUpdate))
		{
			TraceIntoFile::trace(_YDUMP("RoleDelegationManager"), _YDUMP("UpdateSQLiteFromCloudIfLeaseExpiredAndInvalidateDelegation"), _YTEXTFORMAT(L"%s Role Delegation has changed - disconnect RBAC", rdBeforeUpdate.GetDelegation().m_Name.c_str()));
			p_Session->SetUseRoleDelegation(0, nullptr);// 0 = no delegation; clears RBAC sessions
			rvInvalidateRBAC = true;
		}
	}

	return rvInvalidateRBAC;
}

bool RoleDelegationManager::updateSQLiteFromCloud(std::shared_ptr<const Sapio365Session> p_Session, const CosmosUtil::SQLupdateData& p_CosmosDocuments, DlgDoubleProgressCommon& p_Progress)
{
	bool rvUpdate = false;

	ClearCache();

	if (IsReadyToRun())
	{
		// push to SQLite
		rvUpdate = m_RoleDelegationSQLite.UpdateFromCosmos(p_CosmosDocuments, p_Session, p_Progress);

		if (!rvUpdate)
		{
			TraceIntoFile::trace(_YDUMP("RoleDelegationManager"), _YDUMP("UpdateSQLiteFromCloud"), 
								 _YTEXTFORMAT(L"Unable to properly update all data from Cosmos account: %s (tenant %s)", p_Session->GetCosmosDbSqlUri().c_str(), p_Session->GetTenantName().c_str()));
/*
			YCallbackMessage::DoSend([]()
			{
				YCodeJockMessageBox dlg(AfxGetApp()->m_pMainWnd,
										DlgMessageBox::eIcon_Error,
										YtriaTranslate::Do(Application_error_2, _YLOC("Error")).c_str(),
										YtriaTranslate::Do(RoleDelegationManager_updateSQLiteFromCloud_2, _YLOC("Unable to update RBAC from Cloud")).c_str(),
										_YTEXT(""),
										{ { IDOK, YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str() } });
				dlg.DoModal();
			});
*/
		}
	}

	return rvUpdate;
}

bool RoleDelegationManager::pushSQLiteToCloud(std::shared_ptr<const Sapio365Session> p_Session, const CosmosUtil::SQLupdateData& p_CosmosDocuments, DlgDoubleProgressCommon& p_Progress)
{
	bool rvUpdate = false;

	if (IsReadyToRun())
	{
		if (HasSessionAccessToConfig(p_Session))
		{
			// fetch from SQLite - push to COSMOS
			RoleDelegationUtil::CosmosUpdateData SQLdata;
			if (m_RoleDelegationSQLite.SelectAllForUpdateToCloud(SQLdata, p_Session))
				rvUpdate = m_RoleDelegationCosmos.UpdateFromSQLite(SQLdata, p_CosmosDocuments, p_Session, p_Progress);
			else
			{
				TraceIntoFile::trace(_YDUMP("RoleDelegationManager"), _YDUMP("pushSQLiteToCloud"),
					_YTEXTFORMAT(L"Unable to load RBAC data to push to %s (tenant %s)", p_Session->GetCosmosDbSqlUri().c_str(), p_Session->GetTenantName().c_str()));
				YCallbackMessage::DoSend([]()
					{
						YCodeJockMessageBox dlg(AfxGetApp()->m_pMainWnd,
												DlgMessageBox::eIcon_Error,
												YtriaTranslate::Do(Application_error_2, _YLOC("Error")).c_str(),
												YtriaTranslate::Do(RoleDelegationManager_pushSQLiteToCloud_2, _YLOC("Unable to update RBAC to cloud")).c_str(),
												_YTEXT(""),
												{ { IDOK, YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str() } });
						dlg.DoModal();
					});
			}
		}

		m_RoleDelegationSQLite.CompleteUpdateToCloud(p_Session);
	}

	return rvUpdate;
}

const RoleDelegation& RoleDelegationManager::GetRoleDelegationFromCache(const int64_t p_DelegationID, std::shared_ptr<const Sapio365Session> p_Session)
{
	if (p_DelegationID > 0 && IsReadyToRun())
	{
		auto findIt = m_DelegationsCache.find(p_DelegationID);
		if (m_DelegationsCache.end() == findIt)
		{
			auto res = m_DelegationsCache.insert({ p_DelegationID , m_RoleDelegationSQLite.LoadRoleDelegation(p_DelegationID, p_Session) });
			findIt = res.first;
			ASSERT(m_DelegationsCache.end() != findIt);
		}
		return findIt->second;
	}

	static RoleDelegation g_Dummy;
	return g_Dummy;
}

void RoleDelegationManager::RemoveFromCache(const int64_t p_DelegationID)
{
	m_DelegationsCache.erase(p_DelegationID);
}

void RoleDelegationManager::ClearCache()
{
	m_DelegationsCache.clear();
}

RoleDelegationUtil::BusinessDelegation RoleDelegationManager::GetDelegation(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_RoleDelegationSQLite.ReadDelegation(p_ID, p_Session).m_Delegation;
}

RoleDelegationUtil::BusinessKey RoleDelegationManager::GetKey(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	auto k = m_RoleDelegationSQLite.ReadKey(p_ID, p_Session);
	k.m_Key.m_ID = k.GetID();
	return k.m_Key;
}

RoleDelegationUtil::BusinessFilter RoleDelegationManager::GetFilter(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	auto f = m_RoleDelegationSQLite.ReadFilter(p_ID, p_Session);
	f.m_Filter.m_ID = f.GetID();
	return f.m_Filter;
}

vector<RoleDelegationUtil::Key> RoleDelegationManager::GetKeys(std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_RoleDelegationSQLite.ReadKeys(p_Session);
}

vector<RoleDelegationUtil::Filter> RoleDelegationManager::GetFilters(std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_RoleDelegationSQLite.ReadFilters(p_Session);
}

RoleDelegationSQLite& RoleDelegationManager::GetRoleDelegationSQLite()
{
	return m_RoleDelegationSQLite;
}

RoleDelegationCosmos& RoleDelegationManager::GetRoleDelegationCosmos()
{
	return m_RoleDelegationCosmos;
}

RoleDelegation RoleDelegationManager::LoadRoleDelegationFromSQLite(const int64_t p_IDdelegation, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_RoleDelegationSQLite.LoadRoleDelegation(p_IDdelegation, p_Session);
}

int64_t RoleDelegationManager::CountKeys(std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_RoleDelegationSQLite.CountKeys(p_Session);
}

vector<RoleDelegationUtil::Delegation> RoleDelegationManager::ReadDelegationsFeaturingThisKey(const int64_t p_KeyID, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_RoleDelegationSQLite.ReadDelegationsFeaturingThisKey(p_KeyID, p_Session);
}

vector<RoleDelegationUtil::Delegation> RoleDelegationManager::ReadDelegationsFeaturingThisFilter(const int64_t p_FilterID, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_RoleDelegationSQLite.ReadDelegationsFeaturingThisFilter(p_FilterID, p_Session);
}

bool RoleDelegationManager::DelegationHasPrivilege(const int64_t p_DelegationID, const RoleDelegationUtil::RBAC_Privilege p_Privilege, std::shared_ptr<const Sapio365Session> p_Session)
{
	return GetRoleDelegationFromCache(p_DelegationID, p_Session).HasPrivilege(p_Privilege);
}

bool RoleDelegationManager::GetRoleDelegationsForThisUser(const PooledString& p_UserID, vector<RoleDelegation>& p_Delegations, std::shared_ptr<const Sapio365Session> p_Session)
{
	return IsReadyToRun() && m_RoleDelegationSQLite.LoadRoleDelegations(p_UserID, p_Delegations, p_Session);
}

bool RoleDelegationManager::IsSapio365AdminScopeSetForUser(const PooledString& p_UserID, const RoleDelegationUtil::RBAC_AdminScope p_Scope, std::shared_ptr<const Sapio365Session> p_Session)
{
	return m_RoleDelegationSQLite.IsSapio365AdminScopeSetForUser(p_Session, p_UserID, p_Scope);
}

void RoleDelegationManager::GetAdminAccessForSession(std::shared_ptr<const Sapio365Session> p_Session, set<RoleDelegationUtil::RBAC_AdminScope>& p_AA)
{
	m_RoleDelegationSQLite.GetAdminAccessForSession(p_Session, p_AA);
}

bool RoleDelegationManager::IsRoleEnforced(const int64_t p_RoleID, std::shared_ptr<const Sapio365Session> p_Session)
{
	if (p_Session)
	{
		auto role = GetDelegation(p_RoleID, p_Session);
		return role.IsEnforceRole();
	}

	return false;
}