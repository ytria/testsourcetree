#pragma once

#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "..\Resource.h"
#include "RoleDelegationUtil.h"
#include "YAdvancedHtmlView.h"

class DlgRoleEditBase	: public ResizableDialog
						, public YAdvancedHtmlView::IPostedDataTarget
{
public:
	enum { IDD = IDD_DLG_ROLE_EDIT_BASE };

protected:
	DlgRoleEditBase(CWnd* p_Parent);

	virtual BOOL OnInitDialogSpecificResizable() override final;

	ENDDIALOG_FIXED(ResizableDialog)

	/* YAdvancedHtmlView::IPostedDataTarget */
	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data) override final;

	virtual bool								isSpecificReadOnly() const = 0;
	virtual void								generateJSONScriptDataSpecific(web::json::value& p_JsonValue) = 0;
	virtual bool								shouldAddCredentialsCreationButton(bool p_ButtonTop) const;
	virtual bool								processPostedDataSpecific(const IPostedDataTarget::PostedData& data) = 0;
	virtual RoleDelegationUtil::BusinessData*	getBusinessData() = 0;
	virtual wstring								getDialogTitle() const = 0;
	virtual int									getMinimumHeight() const;

	const std::unique_ptr<YAdvancedHtmlView>&	GetHtmlView();

protected:
	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;

private:
	void generateJSONScriptData(wstring& p_Output);

	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;

	static const wstring g_VarName;
	static const wstring g_VarInfo;
	static const wstring g_VarOKButton;
	static const wstring g_VarCancelButton;
};
