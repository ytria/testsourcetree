#include "DlgRoleEditAddFilter.h"

#include "DlgRoleEditFilter.h"
#include "SQL + Cloud/Role Delegation/RoleDelegationManager.h"

BEGIN_MESSAGE_MAP(DlgRoleEditAddFilter, ResizableDialog)
	ON_BN_CLICKED(IDC_BTN_ADDFILTER, OnBtnAdd)
END_MESSAGE_MAP()

DlgRoleEditAddFilter::DlgRoleEditAddFilter(const vector<RoleDelegation>& p_RoleDelegations, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent)
	: ResizableDialog(DlgRoleEditAddFilter::IDD, p_Parent)
	, m_RoleDelegations(p_RoleDelegations)
	, m_FIlterAlreadyPresentInDelegations(nullptr)
	, m_Session(p_Session)
{
}

DlgRoleEditAddFilter::~DlgRoleEditAddFilter()
{
}

void DlgRoleEditAddFilter::DoDataExchange(CDataExchange* pDX)
{
	ResizableDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDCANCEL,					m_btnCancel);
	DDX_Control(pDX, IDOK,						m_btnOK);
	DDX_Control(pDX, IDC_STATIC_INFOMERCIAL,	m_staticInfo);
	DDX_Control(pDX, IDC_BTN_ADDFILTER,			m_btnAdd);
}

BOOL DlgRoleEditAddFilter::OnInitDialogSpecificResizable()
{
	SetWindowText(YtriaTranslate::Do(DlgRoleEditAddFilter_OnInitDialogSpecificResizable_1, _YLOC("Scopes Selection")).c_str());
	m_btnOK.SetWindowText(YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str());
	m_btnCancel.SetWindowText(YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str());
	m_btnAdd.SetWindowText(YtriaTranslate::Do(DlgRoleEditAddFilter_OnInitDialogSpecificResizable_4, _YLOC("Create New Scope...")).c_str());

	vector<wstring> names;
	map<int64_t, set<wstring>> existingFilters;
	for (const auto& rd : m_RoleDelegations)
	{
		names.push_back(rd.GetDelegation().m_Name);

		for (const auto& filtersByObjectType : rd.GetFilters())
			for (const auto& f : filtersByObjectType.second)
				existingFilters[f.m_ID.get()].insert(rd.GetDelegation().m_Name);
	}

	m_staticInfo.SetWindowText(YtriaTranslate::Do(DlgRoleEditAddFilter_OnInitDialogSpecificResizable_6, _YLOC("Select scopes to assign to selected roles: %1"), Str::implode(names, _YTEXT(", ")).c_str()).c_str());

	CWnd* pDummy = GetDlgItem(IDC_STATIC_GRIDRECTAL);
	ASSERT(nullptr != pDummy);
	if (nullptr != pDummy)
	{
		CRect rcGrid;
		pDummy->GetWindowRect(&rcGrid);
		ScreenToClient(&rcGrid);
		auto gridOK = m_Grid.Create(this, rcGrid);
		ASSERT(gridOK);
		if (gridOK)
		{
			m_Grid.SetOnDoubleClickRowFunction(std::bind(&DlgRoleEditAddFilter::OnOK, this));
			m_Grid.SetIsActive(true);// this shit sucks - TODO remove
			m_Grid.LoadSQLintoGrid(O365Krypter(m_Session));
			m_Grid.ShowWindow(SW_SHOW);

			// mark already associated filters

			if (!existingFilters.empty())
			{
				m_FIlterAlreadyPresentInDelegations = m_Grid.AddColumn(_YUID("EXISTS"), YtriaTranslate::Do(DlgRoleEditAddFilter_OnInitDialogSpecificResizable_5, _YLOC("Already assigned to role(s)")).c_str(), _YTEXT(""));
				ASSERT(nullptr != m_FIlterAlreadyPresentInDelegations);
				if (nullptr != m_FIlterAlreadyPresentInDelegations)
				{
					wstring thisIDstr;
					int64_t thisID;
					for (const auto r : m_Grid.GetBackendRows())
					{
						ASSERT(nullptr != r);
						if (nullptr != r)
						{
							thisIDstr = r->GetField(SQLiteUtil::g_ColumnNameRowID).GetValueStr();
							thisID = Str::getINT64FromString(thisIDstr);
							auto findIt = existingFilters.find(thisID);
							if (findIt != existingFilters.end())
								r->AddField(vector<wstring>(findIt->second.begin(), findIt->second.end()), m_FIlterAlreadyPresentInDelegations->GetID());
						}
					}
					m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
				}
			}
		}
	}

	AddAnchor(m_Grid, CSize(0, 0), CSize(100, 100));
	AddAnchor(m_staticInfo, CSize(0, 0), CSize(100, 0));

	AddAnchor(m_btnAdd, CSize(0, 100), CSize(40, 100));
	AddAnchor(m_btnOK, CSize(40, 100), CSize(70, 100));
	AddAnchor(m_btnCancel, CSize(70, 100), CSize(100, 100));

	return TRUE;
}

void DlgRoleEditAddFilter::OnBtnAdd()
{
	DlgRoleEditFilter dlg(this);
	if (IDOK == dlg.DoModal())
	{
		auto roleFilter		= dlg.GetFilter();
		roleFilter.m_Status = RoleDelegationUtil::RoleStatus::STATUS_OK;
		RoleDelegationManager::GetInstance().Write(roleFilter, m_Session);
		m_Grid.LoadSQLintoGrid(O365Krypter(m_Session), {roleFilter.m_ID.get()});
	}
}

void DlgRoleEditAddFilter::OnOK()
{
	m_SelectedFiltersIDNames.clear();

	auto columnName = m_Grid.GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameName);
	wstring thisIDstr;
	for (auto r : m_Grid.GetSelectedRows())
	{
		ASSERT(nullptr != r);
		if (nullptr != r && !r->IsGroupRow())
		{
			thisIDstr = r->GetField(SQLiteUtil::g_ColumnNameRowID).GetValueStr();
			m_SelectedFiltersIDNames[Str::getINT64FromString(thisIDstr)] = r->GetField(columnName).GetValueStr();
		}
	}

	ResizableDialog::OnOK();
}

const map<int64_t, wstring>& DlgRoleEditAddFilter::GetSelectedFiltersIDNames() const
{
	return m_SelectedFiltersIDNames;
}