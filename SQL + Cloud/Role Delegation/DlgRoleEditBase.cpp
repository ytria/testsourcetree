#include "DlgRoleEditBase.h"
#include "JSONUtil.h"
#include "Sapio365Session.h"
#include "CRMpipe.h"

std::map<wstring, int, Str::keyLessInsensitive> DlgRoleEditBase::g_Sizes;
const wstring DlgRoleEditBase::g_VarName			= _YTEXT("NAME");
const wstring DlgRoleEditBase::g_VarInfo			= _YTEXT("INFO");
const wstring DlgRoleEditBase::g_VarOKButton		= _YTEXT("ok");
const wstring DlgRoleEditBase::g_VarCancelButton	= _YTEXT("cancel");


DlgRoleEditBase::DlgRoleEditBase(CWnd* p_Parent)
	: ResizableDialog(IDD, p_Parent)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
{
	// Load hbs sizes
	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_RBACCONFIG_SIZE, g_Sizes);
		ASSERT(success);
	}
}

BOOL DlgRoleEditBase::OnInitDialogSpecificResizable()
{
	SetWindowText(getDialogTitle().c_str());

	CWnd* pCtrl = GetDlgItem(IDC_GRID_PLACEHOLDER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, NULL);

	m_HtmlView->SetLinkOpeningPolicy(true, true);
	m_HtmlView->SetPostedDataTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
	{
		{ _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
		,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-rbacConfig.js"), _YTEXT("") } } // This file is in resources
		,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("rbacConfig.js"), _YTEXT("") } }, // This file is in resources
	}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("rbacConfig.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	const auto targetHtmlHeight = getMinimumHeight();
	const auto targetHtmlWidth = [=]()
	{
		const auto it = g_Sizes.find(_YTEXT("min-width"));
		ASSERT(g_Sizes.end() != it);
		if (g_Sizes.end() != it)
			return it->second;
		return 0;
	}();

	MFCUtil::SetHtmlViewContainerSizeAndConstraints({ HIDPI_XW(targetHtmlWidth), HIDPI_YH(targetHtmlHeight) }, htmlRect, false, this);

	return TRUE;
}

void DlgRoleEditBase::generateJSONScriptData(wstring& p_Output)
{
	json::value main = json::value::object(
	{
		{ _YTEXT("id")				, json::value::string(_YTEXT("DlgRoleEditBase")) },
		{ _YTEXT("method")			, json::value::string(_YTEXT("POST")) },
		{ _YTEXT("action")			, json::value::string(_YTEXT("#")) },
		{ _YTEXT("collapsAllBtn")	, json::value::string(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(CacheGrid_InitializeCommands_96, _YLOC("Collapse All")).c_str())) },
		{ _YTEXT("collapsAllIcon")	, json::value::string(_YTEXT("fas fa-minus-square")) },
		{ _YTEXT("ExpandAllBtn")	, json::value::string(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(CacheGrid_InitializeCommands_100, _YLOC("Expand All")).c_str())) },
		{ _YTEXT("expandAllIcon")	, json::value::string(_YTEXT("fas fa-plus-square")) },
	});

	// This will define if we have ANY new Cred button in the form
	if (shouldAddCredentialsCreationButton(false))
		main.as_object()[_YTEXT("withCreateNewPairButton")] = json::value::string(_YTEXT("X"));
	// This will define if we want the new Cred Button on the top of the dialog
	if (shouldAddCredentialsCreationButton(true))
		main.as_object()[_YTEXT("createNewPairButton")] = json::value::string(YtriaTranslate::Do(DlgRoleEditBase_generateJSONScriptData_9, _YLOC("Create New Application & Admin...")).c_str());

	wstring name;
	wstring info;
	ASSERT(nullptr != getBusinessData());
	if (nullptr != getBusinessData())
	{
		name = getBusinessData()->m_Name;
		info = getBusinessData()->m_Info;
	}

	json::value items = json::value::array();

	items[0] =
		JSON_BEGIN_OBJECT
            JSON_BEGIN_PAIR
                _YTEXT("hbs"),  JSON_STRING(_YTEXT("rbacIntro"))
            JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("isReadOnly"), JSON_STRING(isSpecificReadOnly() ? _YTEXT("X") : _YTEXT(""))
			JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("introName"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(CacheGrid_compareFields_4, _YLOC("Reference")).c_str()))
            JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("nameField"),  JSON_STRING(g_VarName)
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
                _YTEXT("nameLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_1, _YLOC("Reference Name")).c_str()))
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
                _YTEXT("namePlaceholder"),  JSON_STRING(YtriaTranslate::Do(DlgRoleEditBase_generateJSONScriptData_5, _YLOC("Each reference name must be UNIQUE.")).c_str())
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("nameValue"), JSON_STRING(name)
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("namePopup"), JSON_STRING(_YTEXT(""))
			JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("infoField"),  JSON_STRING(g_VarInfo)
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
                _YTEXT("infoLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(AutomatedApp_ProcessWndProcException_18, _YLOC("Description")).c_str()))
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
                _YTEXT("infoPlaceholder"),  JSON_STRING(YtriaTranslate::Do(DlgRoleEditBase_generateJSONScriptData_7, _YLOC("Enter a short description for this reference (optional)")).c_str())
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("infoValue"), JSON_STRING(info)
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("infoPopup"), JSON_STRING(_YTEXT(""))
			JSON_END_PAIR,

			/*For credentials only*/
			/*JSON_BEGIN_PAIR
				_YTEXT("warningInfoIcon"), JSON_STRING(_YTEXT("fas fa-info-circle"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
// 				_YTEXT("warningInfoOne"), JSON_STRING(MFCUtil::convertEscapeHTMLChar( _YCOM("All information entered will be encrypted and placed in your Cosmos DB instance.")))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
// 				_YTEXT("warningInfoTwo"), JSON_STRING(MFCUtil::convertEscapeHTMLChar( _YCOM("The encryption used some information unique to your tenant. Ytria does not have access to these information.")))
			JSON_END_PAIR,*/
		JSON_END_OBJECT;

	generateJSONScriptDataSpecific(items);

	if (isSpecificReadOnly())
	{
		items[items.size()] =
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("hbs"), JSON_STRING(_YTEXT("Button"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("choices"),
					JSON_BEGIN_ARRAY
						JSON_BEGIN_OBJECT
							JSON_PAIR_KEY(_YTEXT("type"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
							JSON_PAIR_KEY(_YTEXT("label"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(AutomatedApp_ProcessWndProcException_21, _YLOC("Close")).c_str()))),
							JSON_PAIR_KEY(_YTEXT("attribute"))      JSON_PAIR_VALUE(JSON_STRING(_YTEXT("cancel"))),
							JSON_PAIR_KEY(_YTEXT("name"))			JSON_PAIR_VALUE(JSON_STRING(g_VarCancelButton)),
							JSON_PAIR_KEY(_YTEXT("class"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("is-primary"))),
						JSON_END_OBJECT,
					JSON_END_ARRAY
				JSON_END_PAIR
			JSON_END_OBJECT;
	}
	else
	{
		items[items.size()] =
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("hbs"), JSON_STRING(_YTEXT("Button"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("choices"),
					JSON_BEGIN_ARRAY
						JSON_BEGIN_OBJECT
							JSON_PAIR_KEY(_YTEXT("type"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
							JSON_PAIR_KEY(_YTEXT("label"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str()))),
							JSON_PAIR_KEY(_YTEXT("attribute"))      JSON_PAIR_VALUE(JSON_STRING(_YTEXT("post"))),
							JSON_PAIR_KEY(_YTEXT("name"))			JSON_PAIR_VALUE(JSON_STRING(g_VarOKButton)),
							JSON_PAIR_KEY(_YTEXT("class"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("is-primary"))),
						JSON_END_OBJECT,
						JSON_BEGIN_OBJECT
							JSON_PAIR_KEY(_YTEXT("type"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
							JSON_PAIR_KEY(_YTEXT("label"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str()))),
							JSON_PAIR_KEY(_YTEXT("attribute"))      JSON_PAIR_VALUE(JSON_STRING(_YTEXT("cancel"))),
							JSON_PAIR_KEY(_YTEXT("name"))			JSON_PAIR_VALUE(JSON_STRING(g_VarCancelButton)),
						JSON_END_OBJECT,
					JSON_END_ARRAY
				JSON_END_PAIR
			JSON_END_OBJECT;
	}

	const json::value root = json::value::object({
		{ _YTEXT("main"), main },
		{ _YTEXT("items"), items }
	});

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}

bool DlgRoleEditBase::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	UINT clickedButtonId = 0xffffffff;

	// Has Cancel been pressed?
	if (data.end() != data.find(g_VarCancelButton))
	{
		clickedButtonId = IDCANCEL;
	}
	else
	{
		auto businessData = getBusinessData();
		ASSERT(nullptr != businessData);
		if (nullptr != businessData)
		{
			for (const auto& item : data)
			{
				if (item.first == g_VarOKButton)
					clickedButtonId = IDOK;
				else if (item.first == g_VarCancelButton)
					clickedButtonId = IDCANCEL;
				else if (item.first == g_VarName)
					businessData->m_Name = item.second;
				else if (item.first == g_VarInfo)
					businessData->m_Info = item.second;
			}
		}

        // Keep the dialog open if that method returns false
        if (!processPostedDataSpecific(data))
            return true;
	}

	if (0xffffffff != clickedButtonId)
	{
		endDialogFixed(clickedButtonId);
		return false;
	}

	ASSERT(false);
	return true;
}

bool DlgRoleEditBase::shouldAddCredentialsCreationButton(bool p_ButtonTop) const
{
	return false;
}

int DlgRoleEditBase::getMinimumHeight() const
{
	const auto it = g_Sizes.find(_YTEXT("min-height"));
	ASSERT(g_Sizes.end() != it);
	if (g_Sizes.end() != it)
		return it->second;
	return 0;
}

const std::unique_ptr<YAdvancedHtmlView>& DlgRoleEditBase::GetHtmlView()
{
	return m_HtmlView;
}
