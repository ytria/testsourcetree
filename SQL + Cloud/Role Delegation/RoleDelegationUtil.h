#pragma once


#include "Modification.h"
#include "ordered_map.h"
#include "SQLiteUtil.h"
#include "Str.h"
#include "YTimeDate.h"

#include <memory>

class MSGraphSession;
class PersistentSession;
class Sapio365Session;

namespace RoleDelegationUtil
{
	static const wstring g_ColumnNameStatus				= _YTEXT("Status");
	static const wstring g_ColumnNameDate				= _YTEXT("Date");
	static const wstring g_ColumnNameName				= _YTEXT("Name");
	static const wstring g_ColumnNameTenant				= _YTEXT("Tenant");// where the config was set && stored (sapio365 license tenant)
	static const wstring g_ColumnNameTargetTenant		= _YTEXT("TenantTarget");// where the config (key/credential) is to be used
	static const wstring g_ColumnNameInfo				= _YTEXT("Info");
	static const wstring g_ColumnNameAppID				= _YTEXT("Application ID");
	static const wstring g_ColumnNameAppPWD				= _YTEXT("Application PWD");
	static const wstring g_ColumnNameSecret				= _YTEXT("Secret");
	static const wstring g_ColumnNameURL				= _YTEXT("URL");
	static const wstring g_ColumnNameLogin				= _YTEXT("Login");
	static const wstring g_ColumnNamePWD				= _YTEXT("PWD");
	static const wstring g_ColumnNameLease				= _YTEXT("Lease");
	static const wstring g_ColumnNameIDKey				= _YTEXT("KeyID");// rowid in m_Keys
	static const wstring g_ColumnNameProperty			= _YTEXT("Property");
	static const wstring g_ColumnNameValue				= _YTEXT("Value");
	static const wstring g_ColumnNameObjectType			= _YTEXT("Object Type");
	static const wstring g_ColumnNameUserID				= _YTEXT("User ID");// graph ID
	static const wstring g_ColumnNameUserName			= _YTEXT("UserName");
	static const wstring g_ColumnNameUserPrincipalName	= _YTEXT("User Principal Name");
	static const wstring g_ColumnNameIDFilter			= _YTEXT("Filter ID");// rowid in m_Filters
	static const wstring g_ColumnNameUpdName			= _YTEXT("Udpated By");
	static const wstring g_ColumnNameUpdEmail			= _YTEXT("Udpated By Email");
	static const wstring g_ColumnNameUpdID				= _YTEXT("Updated By ID");// graph ID
	static const wstring g_ColumnNameSkuLimits			= _YTEXT("Sku Limits");
	static const wstring g_ColumnNamePrivilege			= _YTEXT("Privilege");
	static const wstring g_ColumnNameTaggerMatchMethod	= _YTEXT("Tagger Match Method");
	static const wstring g_ColumnNameAdminScope			= _YTEXT("AdminScope");
	static const wstring g_ColumnNameFlags				= _YTEXT("Flags");

	static const wstring g_ColumnNameIDDelegation		= _YTEXT("Delegation ID");

	static const wstring g_BoolValueTrue	= _YTEXT("true");
	static const wstring g_BoolValueFalse	= _YTEXT("false");

	static const wstring g_BoolValueUnset	= _YTEXT("[UNSET]");

	enum RBAC_RoleFlags : uint32_t
	{
		RBAC_ROLEFLAG_LOGSESSIONLOGIN	= 0x001,
		RBAC_ROLEFLAG_LOGMODULEOPENING	= 0x002,
		RBAC_ROLEFLAG_ENFORCEROLE		= 0x004
	};

	enum RBAC_Scope : uint32_t
	{
		// A C H T U N G
		// DO NOT CHANGE ORDER
		// APPEND NEW SCOPES AT THE END
		// or you will break the UI

		RBAC_SCOPE_USER = 0,
		RBAC_SCOPE_GROUP,
		RBAC_SCOPE_SITE,
		RBAC_SCOPE_OTHER,

		RBAC_SCOPE_MAX
	};
	
	enum RBAC_AdminScope : uint32_t
	{
		RBAC_ADMIN_NONE = 0,
		RBAC_ADMIN_MANAGER,
		RBAC_ADMIN_RBAC,
		RBAC_ADMIN_LOGS,
		RBAC_ADMIN_ANNOTATIONS
	};

	enum RBAC_Privilege : uint32_t
	{
		// A C H T U N G
		// THESE VALUES ARE STORED IN SQLITE - THEY MUST NEVER CHANGE!
		// A L W A Y S  explicitly define the enum value (See RBAC_next_available_privilege_ID)
		// when adding a new privilege, update RoleDelegation::GetAllPrivileges()
		// when removing a privilege, just comment the line (for keeping trace) and NEVER re-use the same value except if re-creating the exact same privilege

		RBAC_NONE								= 0,

		RBAC_USER_LOADMORE						= 1,
		RBAC_USER_EDIT							= 2,
		RBAC_USER_CREATE						= 3,
		RBAC_USER_DELETE						= 4,
		RBAC_USER_PWD_EDIT						= 5,
		RBAC_USER_LICENSES_EDIT					= 6,
		RBAC_USER_MANAGEMENTHIERARCHY			= 7,
		RBAC_USER_ONEDRIVE_READ					= 8,
		RBAC_USER_ONEDRIVE_LOADMORE				= 9,
		RBAC_USER_ONEDRIVE_PERMISSIONS_EDIT		= 10,
		RBAC_USER_ONEDRIVE_RENAME				= 11,
		RBAC_USER_ONEDRIVE_DELETE				= 12,
		RBAC_USER_ONEDRIVE_DOWNLOAD				= 71,
		RBAC_USER_ONEDRIVE_CREATEFOLDER			= 89,
		RBAC_USER_ONEDRIVE_ADDFILE				= 92,
		RBAC_USER_MESSAGES_READ					= 13,
		RBAC_USER_MESSAGES_LOADMORE				= 14,
		RBAC_USER_MESSAGES_ATTACHMENTS_EDIT		= 15,
		RBAC_USER_MESSAGES_ATTACHMENTS_DOWNLOAD	= 72,
		//RBAC_USER_MESSAGES_EDIT					= 16,
		RBAC_USER_MESSAGES_DELETE				= 17,
		//RBAC_USER_MAILFOLDERS_READ				= 18,
		//RBAC_USER_MAILFOLDERS_LOADMORE			= 19,
		//RBAC_USER_MAILFOLDERS_EDITATTACHMENTS	= 20,
		//RBAC_USER_MAILFOLDERS_EDIT				= 21,
		//RBAC_USER_MAILFOLDERS_DELETE			= 22,
		RBAC_USER_EVENTS_READ					= 23,
		RBAC_USER_EVENTS_LOADMORE				= 24,
		RBAC_USER_EVENTS_ATTACHMENTS_EDIT		= 25,
		RBAC_USER_EVENTS_ATTACHMENTS_DOWNLOAD	= 73,
		//RBAC_USER_EVENTS_EDIT					= 26,
		RBAC_USER_EVENTS_DELETE					= 27,
		RBAC_USER_CONTACTS_READ					= 28,
		RBAC_USER_MESSAGERULES_READ				= 29,
		RBAC_USER_MESSAGERULES_EDIT				= 30,
		RBAC_USER_MESSAGERULES_CREATE			= 31,
		RBAC_USER_MESSAGERULES_DELETE			= 32,
		RBAC_USER_RECYCLEBIN_MANAGE				= 33,
		RBAC_USER_MANAGER_UPDATE				= 95,

		RBAC_GROUP_LOADMORE						= 34,
		RBAC_GROUP_EDIT							= 35,
		RBAC_GROUP_CREATE						= 36,
		RBAC_GROUP_DELETE						= 37,
		RBAC_GROUP_ONEDRIVE_READ				= 38,
		RBAC_GROUP_ONEDRIVE_LOADMORE			= 39,
		RBAC_GROUP_ONEDRIVE_PERMISSIONS_EDIT	= 40,
		RBAC_GROUP_ONEDRIVE_RENAME				= 41,
		RBAC_GROUP_ONEDRIVE_DELETE				= 42,
		RBAC_GROUP_ONEDRIVE_DOWNLOAD			= 74,
		RBAC_GROUP_ONEDRIVE_CREATEFOLDER		= 90,
		RBAC_GROUP_ONEDRIVE_ADDFILE				= 93,
		RBAC_GROUP_EVENTS_READ					= 43,
		RBAC_GROUP_EVENTS_LOADMORE				= 44,
		RBAC_GROUP_EVENTS_ATTACHMENTS_EDIT		= 45,
		RBAC_GROUP_EVENTS_ATTACHMENTS_DOWNLOAD	= 75,
		//RBAC_GROUP_EVENTS_EDIT					= 46,
		//RBAC_GROUP_EVENTS_DELETE				= 47,
		RBAC_GROUP_MEMBERS_EDIT					= 48,
		RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE	= 84,
		RBAC_GROUP_OWNERS_EDIT					= 49,
		RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE		= 85,
		RBAC_GROUP_DELIVERYMANAGEMENT_READ		= 50,
		RBAC_GROUP_DELIVERYMANAGEMENT_EDIT		= 51,
		RBAC_GROUP_DELIVERYMANAGEMENT_EDIT_OUT_OF_SCOPE	= 86,
		RBAC_GROUP_CONVERSATIONS_READ			= 52,
		//RBAC_GROUP_CONVERSATIONS_EDIT			= 53,
		RBAC_GROUP_CONVERSATIONS_LOADMORE		= 76,
		RBAC_GROUP_CONVERSATIONS_ATTACHMENTS_EDIT		= 77,
		RBAC_GROUP_CONVERSATIONS_ATTACHMENTS_DOWNLOAD	= 78,
		RBAC_GROUP_CHANNELS_READ				= 54,
		//RBAC_GROUP_CHANNELS_EDIT				= 55,
		RBAC_GROUP_CHANNELS_LOADMORE			= 79,
		RBAC_GROUP_CHANNELS_ATTACHMENTS_EDIT	= 80,
		RBAC_GROUP_CHANNELS_ATTACHMENTS_DOWNLOAD	= 81,
		RBAC_GROUP_SITES_READ					= 56,
		//RBAC_GROUP_SITES_EDIT					= 57,
		RBAC_GROUP_RECYCLEBIN_MANAGE			= 58,
		//RBAC_GROUP_SETTINGS						= 59,
		//RBAC_GROUP_POLICY						= 60,

		RBAC_SITE_READ							= 61,
		RBAC_SITE_EDIT							= 62,
		RBAC_SITE_ONEDRIVE_READ					= 63,
		RBAC_SITE_ONEDRIVE_LOADMORE				= 64,
		RBAC_SITE_ONEDRIVE_PERMISSIONS_EDIT		= 65,
		RBAC_SITE_ONEDRIVE_RENAME				= 66,
		RBAC_SITE_ONEDRIVE_DELETE				= 67,
		RBAC_SITE_ONEDRIVE_DOWNLOAD				= 82,
		RBAC_SITE_ONEDRIVE_CREATEFOLDER			= 91,
		RBAC_SITE_ONEDRIVE_ADDFILE				= 94,
		RBAC_SITE_PERMISSIONS_READ				= 68,
		RBAC_SITE_PERMISSIONS_EDIT				= 69,

		RBAC_TENANT_ROLES_EDIT					= 70,
		RBAC_TENANT_ROLES_EDIT_OUT_OF_SCOPE		= 87,
		RBAC_TENANT_GROUP_EDITSETTINGS			= 83,
		RBAC_USER_MESSAGES_SEE_MAIL_CONTENT		= 88,

		RBAC_CHANNEL_ONEDRIVE_LOADMORE			= 96,
		RBAC_CHANNEL_ONEDRIVE_PERMISSIONS_EDIT	= 97,
		RBAC_CHANNEL_ONEDRIVE_DOWNLOAD			= 98,
		RBAC_CHANNEL_ONEDRIVE_DELETE			= 99,
		RBAC_CHANNEL_ONEDRIVE_RENAME			= 100,
		RBAC_CHANNEL_ONEDRIVE_CREATEFOLDER		= 101,
		RBAC_CHANNEL_ONEDRIVE_ADDFILE			= 102,

		RBAC_USER_MAILBOXPERMISSIONS_READ		= 103,
		RBAC_USER_LOADMAILBOX					= 104,

		// Update this one each time you add a new privilege
		RBAC_next_available_privilege_ID		= 105,

		// when adding a new privilege, update RoleDelegation::GetAllPrivileges()

		RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE = UINT_MAX, // For internal purpose only, use to check if an item is in the current scope or not.
	};

	enum RBAC_PrivilegeFlags : uint32_t
	{
		RBAC_FLAG_READ			= 0x001,
		RBAC_FLAG_EDIT			= 0x002,
		RBAC_FLAG_DELETE		= 0x004,
		RBAC_FLAG_CREATE		= 0x008,
		RBAC_FLAG_OUTOFSCOPE	= 0x010
	};

	struct RBAC_PrivilegeUI
	{
/*
		RBAC_PrivilegeUI(const RBAC_Privilege p_Priv, const RBAC_Scope p_Scope, const wstring& p_Name, const uint32_t p_Flags):
			m_Privilege(p_Priv), 
			m_Scope(p_Scope),
			m_Name(p_Name),
			m_Flags(p_Flags)
		{}
*/

		RBAC_Privilege			m_Privilege;
		RBAC_Scope				m_Scope;
		wstring					m_Label;
		uint32_t				m_Flags;// for DlgRoleEditDelegation - grouped selection
		vector<RBAC_Privilege>	m_Required;// for DlgRoleEditDelegation - privileges needed for the current privilege (e.g. LOADMORE required to edit attachments)

		bool HasFlag(const RBAC_PrivilegeFlags p_Flag) const
		{
			return (m_Flags & p_Flag) == m_Flags;
		}
	};

	enum RoleStatus : uint32_t
	{
		STATUS_OK = 0,
		STATUS_REMOVED,
		STATUS_LOCKED
	};

	enum TaggerMatchMethod : uint32_t
	{
		COMPARISON_EQUAL_CASEINSENSITIVE = 0,
		COMPARISON_EQUAL_CASESENSITIVE,
		COMPARISON_NOT_EQUAL_CASEINSENSITIVE,
		COMPARISON_NOT_EQUAL_CASESENSITIVE,

		COMPARISON_EQUAL_WILDCARD_CASEINSENSITIVE,
		COMPARISON_EQUAL_WILDCARD_CASESENSITIVE,
		COMPARISON_NOT_EQUAL_WILDCARD_CASEINSENSITIVE,
		COMPARISON_NOT_EQUAL_WILDCARD_CASESENSITIVE,

		COMPARISON_REGEX_CASEINSENSITIVE,
		COMPARISON_REGEX_CASESENSITIVE,
		COMPARISON_NOT_REGEX_CASEINSENSITIVE,
		COMPARISON_NOT_REGEX_CASESENSITIVE,
	};

	const wstring&					GetStatus(const uint32_t p_Status);
	const map<RoleStatus, wstring>&	GetValueAndLabel_Status();

	const wstring& GetPrivilegeLabel(const RBAC_Privilege& p_Privilege);
	const wstring& GetPrivilegeLabel(const uint32_t p_Privilege);

	const std::pair<wstring, wstring>& GetScopeJSONKeyAndLabel(const RBAC_Scope p_Scope);
	const std::pair<wstring, wstring>& GetScopeJSONKeyAndLabel(const uint32_t p_Scope);

	static const tsl::ordered_map<RBAC_Privilege, RBAC_PrivilegeUI>&	GetAllPrivileges();
	const vector<RBAC_PrivilegeUI>&					GetPrivileges(const RBAC_Scope p_Scope);
	const vector<RBAC_PrivilegeUI>&					GetPrivileges(const uint32_t p_Scope);

	bool IsUserPrivilege(const RBAC_Privilege p_Privilege);
	bool IsGroupPrivilege(const RBAC_Privilege p_Privilege);
	bool IsSitePrivilege(const RBAC_Privilege p_Privilege);
	bool IsScopePrivilege(const RBAC_Scope p_Scope, const RBAC_Privilege p_Privilege);

	const wstring&							GetTaggerMatchMethod(const uint32_t p_TMM);
	const map<TaggerMatchMethod, wstring>&	GetValueAndLabel_TaggerMatchMethod();
	const map<TaggerMatchMethod, wstring>&	GetValueAndLabel_TaggerMatchMethodMain();
	bool		IsTaggerMatchMethodNOT(const uint32_t p_TMM);
	bool		IsTaggerMatchMethodCASEINSENSITIVE(const uint32_t p_TMM);
	uint32_t	GetTaggerMatchMethod(const uint32_t p_MainMethod, const bool p_SubNOT, const bool p_SubCASESENSITIVE);
	uint32_t	GetTaggerMatchMethodMain(const uint32_t p_TMM);

	bool CreateApplication(const std::shared_ptr<Sapio365Session>& p_Session, const wstring& p_TargetTenant, const PersistentSession& p_PersistentSession, const wstring& p_NewAppDisplayName, CFrameWnd* p_FrameSource);
	bool QueryApp(const std::shared_ptr<Sapio365Session>& p_Session, const wstring& p_Id);

	pair<bool, wstring> TestSession(const std::shared_ptr<MSGraphSession>& p_Session, const wstring& p_TargetTenant);
	pair<bool, wstring> TestSessionWithDialog(const std::shared_ptr<MSGraphSession>& p_Session, const wstring& p_TargetTenant, CWnd* p_Parent);
	bool WaitTillAppReadyForConsent(const std::shared_ptr<MSGraphSession>& p_Session, const wstring& p_TargetTenant, CWnd* p_Parent);
    pair<bool, wstring> TestAdminSession(const wstring& p_Tenant, const wstring& p_Username, const wstring& p_Password);
    pair<bool, wstring> TestUltraAdminSession(const wstring& p_Tenant, const wstring& p_Username, const wstring& p_Password);

	const tsl::ordered_map<RoleDelegationUtil::RBAC_AdminScope, std::pair<wstring, wstring>>& GetAdminScopes();

	class FilterTypes
	{
	public:
		static FilterTypes& GetInstance();

		const wstring& GetTypeGroup()	const;
		const wstring& GetTypeSite()	const;
		const wstring& GetTypeUser()	const;

		const wstring& GetNameGroup()	const;
		const wstring& GetNameSite()	const;
		const wstring& GetNameUser()	const;

	private:
		FilterTypes();

		static wstring g_TypeGroup;
		static wstring g_TypeSite;
		static wstring g_TypeUser;
		static wstring g_NameGroup;
		static wstring g_NameSite;
		static wstring g_NameUser;
	};

	// I/O for tables in RoleAccessSQLite:

	struct BusinessData
	{
		virtual ~BusinessData() = default;

		wstring		m_Name;
		wstring		m_Info;
		YTimeDate	m_UpdDate;
		wstring		m_UpdateByID;// graph ID
		wstring		m_UpdateByName;
		wstring		m_UpdateByPrincipalName;
		wstring		m_TenantName;

		boost::YOpt<RoleStatus> m_Status;
		boost::YOpt<int64_t>	m_ID;// sad but true

		bool IsValid() const { return m_Status.is_initialized(); }

		wstring GetStatus() const;
		void	ReadPostProcess(const SQLiteUtil::SQLRecord& p_SQL);
	};
	
	// TODO: CLASS INTROSPECTION with SQL column / class members match by name

	class RoleDelegationRecord : public SQLiteUtil::SQLRecord 
	{
	public:
		RoleDelegationRecord(const SQLiteUtil::Table& p_Table) : SQLRecord(p_Table)
		{}

		virtual BusinessData* GetGeneric() = 0;

		const YTimeDate& GetLastUpdateDate()
		{
			const static YTimeDate g_Dummy;
			ASSERT(GetGeneric() != nullptr);
			return GetGeneric() != nullptr ? GetGeneric()->m_UpdDate : g_Dummy;
		}

		bool IsRemoved()
		{
			bool rvRemoved = false;

			auto g = GetGeneric();
			ASSERT(nullptr != g);
			if (nullptr != g)
				rvRemoved = STATUS_REMOVED == g->m_Status;

			return rvRemoved;
		}

	protected:
		void configureGeneric()
		{
			auto g = GetGeneric();
			ASSERT(nullptr != g);
			if (nullptr != g)
			{
				configureMember(SQLiteUtil::g_ColumnNameRowID,	&m_ID,							SQLiteUtil::SQLRecord::MemberType::OPTINT64);
				configureMember(g_ColumnNameStatus,				&g->m_Status,					SQLiteUtil::SQLRecord::MemberType::OPTUINT32);
				configureMember(g_ColumnNameName,				&g->m_Name,						SQLiteUtil::SQLRecord::MemberType::WSTRING);
				configureMember(g_ColumnNameInfo,				&g->m_Info,						SQLiteUtil::SQLRecord::MemberType::WSTRING);
				configureMember(g_ColumnNameDate,				&g->m_UpdDate,					SQLiteUtil::SQLRecord::MemberType::DATE);
				configureMember(g_ColumnNameUpdID,				&g->m_UpdateByID,				SQLiteUtil::SQLRecord::MemberType::WSTRING);
				configureMember(g_ColumnNameUpdName,			&g->m_UpdateByName,				SQLiteUtil::SQLRecord::MemberType::WSTRING);
				configureMember(g_ColumnNameUpdEmail,			&g->m_UpdateByPrincipalName,	SQLiteUtil::SQLRecord::MemberType::WSTRING);
				configureMember(g_ColumnNameTenant,				&g->m_TenantName,				SQLiteUtil::SQLRecord::MemberType::WSTRING);
			}
		}
	};

	//
	struct BusinessKey : public BusinessData
	{
		wstring m_ApplicationID;// ultra admin URL
		wstring m_ApplicationPWD;// ultra admin pwd
		wstring m_Login;// actual account
		wstring m_PWD;// actual account pwd
		wstring m_TargetTenant;
	};
	
	class Key : public RoleDelegationRecord
	{
	public:
		Key(const SQLiteUtil::Table& p_Table) : RoleDelegationRecord(p_Table) 
		{
			Configure();
		}

		virtual BusinessData*	GetGeneric() override { return &m_Key; }
		void					Configure()	override
		{
			configureGeneric();
			addColumnForModificationLog(g_ColumnNameTenant);
			configureMember(g_ColumnNameAppID,			&m_Key.m_ApplicationID,		SQLiteUtil::SQLRecord::MemberType::WSTRING, true);
			configureMember(g_ColumnNameAppPWD,			&m_Key.m_ApplicationPWD,	SQLiteUtil::SQLRecord::MemberType::WSTRING, true);
			configureMember(g_ColumnNameLogin,			&m_Key.m_Login,				SQLiteUtil::SQLRecord::MemberType::WSTRING, true);
			configureMember(g_ColumnNamePWD,			&m_Key.m_PWD,				SQLiteUtil::SQLRecord::MemberType::WSTRING, true);
			configureMember(g_ColumnNameTargetTenant,	&m_Key.m_TargetTenant,		SQLiteUtil::SQLRecord::MemberType::WSTRING, true);
		}

		BusinessKey m_Key;
	};

	//
	struct BusinessFilter : public BusinessData
	{
		wstring		m_Property;
		wstring		m_Value;
		wstring		m_ObjectType;
		uint32_t	m_TaggerMatchMethod = TaggerMatchMethod::COMPARISON_EQUAL_CASEINSENSITIVE;
	};
	
	class Filter : public RoleDelegationRecord
	{
	public:
		Filter(const SQLiteUtil::Table& p_Table) : RoleDelegationRecord(p_Table) 
		{
			Configure();
		}

		virtual BusinessData*	GetGeneric() override { return &m_Filter; }
		void					Configure()	override
		{
			configureGeneric();
			addColumnForModificationLog(g_ColumnNameTenant);
			configureMember(g_ColumnNameProperty,			&m_Filter.m_Property,			SQLiteUtil::SQLRecord::MemberType::WSTRING, true);
			configureMember(g_ColumnNameValue,				&m_Filter.m_Value,				SQLiteUtil::SQLRecord::MemberType::WSTRING, true);
			configureMember(g_ColumnNameObjectType,			&m_Filter.m_ObjectType,			SQLiteUtil::SQLRecord::MemberType::WSTRING, true);
			configureMember(g_ColumnNameTaggerMatchMethod,	&m_Filter.m_TaggerMatchMethod,	SQLiteUtil::SQLRecord::MemberType::UINT32, true);
		}

		BusinessFilter m_Filter;
	};

	//
	struct BusinessDelegationPrivilege : public BusinessData
	{
		BusinessDelegationPrivilege():m_IDDelegation(0), m_Privilege(RBAC_NONE) {}

		int64_t		m_IDDelegation;
		uint32_t	m_Privilege;// value of RBAC_Privilege

		bool IsDummy() const { return 0 == m_IDDelegation; }
	};

	class DelegationPrivilege : public RoleDelegationRecord
	{
	public:
		DelegationPrivilege(const SQLiteUtil::Table& p_Table) : RoleDelegationRecord(p_Table) 
		{
			Configure();
		}

		virtual BusinessData*	GetGeneric() override { return &m_DelegationPrivilege; }
		void					Configure()	override
		{
			configureGeneric();
			configureMember(g_ColumnNameIDDelegation,	&m_DelegationPrivilege.m_IDDelegation,	SQLiteUtil::SQLRecord::MemberType::INT64);
			configureMember(g_ColumnNamePrivilege,		&m_DelegationPrivilege.m_Privilege,		SQLiteUtil::SQLRecord::MemberType::UINT32, true);
		}

		virtual vector<wstring> GetPKForModificationLog() const override
		{
			return { Str::getStringFromNumber<int64_t>(m_DelegationPrivilege.m_IDDelegation) };
		}

		BusinessDelegationPrivilege m_DelegationPrivilege;
	};

	//
	struct BusinessDelegationFilter : public BusinessData
	{
		BusinessDelegationFilter() :m_IDDelegation(0), m_IDFilter(0) {}

		int64_t m_IDDelegation;
		int64_t m_IDFilter;
	};

	class DelegationFilter : public RoleDelegationRecord
	{
	public:
		DelegationFilter(const SQLiteUtil::Table& p_Table) : RoleDelegationRecord(p_Table)
		{
			Configure();
		}

		virtual BusinessData*	GetGeneric() override { return &m_DelegationFilter; }
		void					Configure()	override
		{
			configureGeneric();
			configureMember(g_ColumnNameIDDelegation,	&m_DelegationFilter.m_IDDelegation, SQLiteUtil::SQLRecord::MemberType::INT64);
			configureMember(g_ColumnNameIDFilter,		&m_DelegationFilter.m_IDFilter,		SQLiteUtil::SQLRecord::MemberType::INT64, true);
			logSwap(g_ColumnNameIDFilter, g_ColumnNameInfo);
		}

		virtual vector<wstring> GetPKForModificationLog() const override
		{
			return { Str::getStringFromNumber<int64_t>(m_DelegationFilter.m_IDDelegation) };
		}

		BusinessDelegationFilter m_DelegationFilter;
	};

	//
	struct BusinessDelegationMember : public BusinessData
	{
		BusinessDelegationMember():m_IDDelegation(0){}

		int64_t	m_IDDelegation;
		wstring	m_UserID;
		wstring	m_UserName;
		wstring	m_UserPrincipalName;
	};

	class DelegationMember : public RoleDelegationRecord
	{
	public:
		DelegationMember(const SQLiteUtil::Table& p_Table) : RoleDelegationRecord(p_Table) 
		{
			Configure();
		}

		virtual BusinessData*	GetGeneric() override { return &m_DelegationMember; }
		void					Configure()	override
		{
			configureGeneric();
			configureMember(g_ColumnNameIDDelegation,		&m_DelegationMember.m_IDDelegation,			SQLiteUtil::SQLRecord::MemberType::INT64);
			configureMember(g_ColumnNameUserID,				&m_DelegationMember.m_UserID,				SQLiteUtil::SQLRecord::MemberType::WSTRING);
			configureMember(g_ColumnNameUserName,			&m_DelegationMember.m_UserName,				SQLiteUtil::SQLRecord::MemberType::WSTRING);
			configureMember(g_ColumnNameUserPrincipalName,	&m_DelegationMember.m_UserPrincipalName,	SQLiteUtil::SQLRecord::MemberType::WSTRING, true);
		}

		virtual vector<wstring> GetPKForModificationLog() const override
		{
			return { Str::getStringFromNumber<int64_t>(m_DelegationMember.m_IDDelegation) };
		}

		BusinessDelegationMember m_DelegationMember;
	};

	//
	struct BusinessDelegation  : public BusinessData
	{
	public:
		static wstring g_LabelLogSessionLogin;
		static wstring g_LabelLogModuleOpening;
		static wstring g_LabelEnforceRole;

		BusinessDelegation(): 
			m_IDKey(0),
			m_Lease(30 * 60), // 30 minutes
			m_Flags(0)
		{
			if (g_LabelLogSessionLogin.empty())
			{
				g_LabelLogSessionLogin	= _T("Log role usage in User Activity Logs");
				g_LabelLogModuleOpening	= _T("Log access of modules with this role in User Activity Logs");
				g_LabelEnforceRole		= _T("Enforce user-assigned roles on standard session sign-ins");
			}
		}

		static vector<wstring> GetFlagLabels(const uint32_t p_Flags)
		{
			vector<wstring> labels;

			if ((p_Flags & RBAC_RoleFlags::RBAC_ROLEFLAG_LOGSESSIONLOGIN) == RBAC_RoleFlags::RBAC_ROLEFLAG_LOGSESSIONLOGIN)
				labels.push_back(g_LabelLogSessionLogin);
			if ((p_Flags & RBAC_RoleFlags::RBAC_ROLEFLAG_LOGMODULEOPENING) == RBAC_RoleFlags::RBAC_ROLEFLAG_LOGMODULEOPENING)
				labels.push_back(g_LabelLogModuleOpening);
			if ((p_Flags & RBAC_RoleFlags::RBAC_ROLEFLAG_ENFORCEROLE) == RBAC_RoleFlags::RBAC_ROLEFLAG_ENFORCEROLE)
				labels.push_back(g_LabelEnforceRole);

			return labels;
		}

		int64_t& GetIDKey()
		{
			return m_IDKey;
		}

		const int64_t& GetIDKey() const
		{
			return m_IDKey;
		}

		uint32_t GetLease() const
		{
			return m_Lease;
		}

		uint32_t GetFlags() const
		{
			return m_Flags;
		}

		bool IsLogSessionLogin() const
		{
			return GridBackendUtil::HasFlag(RBAC_RoleFlags::RBAC_ROLEFLAG_LOGSESSIONLOGIN, m_Flags);
		}
		bool IsLogModuleOpening() const
		{
			return GridBackendUtil::HasFlag(RBAC_RoleFlags::RBAC_ROLEFLAG_LOGMODULEOPENING, m_Flags);
		}
		bool IsEnforceRole() const
		{
			return GridBackendUtil::HasFlag(RBAC_RoleFlags::RBAC_ROLEFLAG_ENFORCEROLE, m_Flags);
		}

		void ResetFlags()
		{
			m_Flags = 0;
		}
		void SetLogSessionLogin()
		{
			m_Flags |= RBAC_RoleFlags::RBAC_ROLEFLAG_LOGSESSIONLOGIN;
		}
		void SetLogModuleOpening()
		{
			m_Flags |= RBAC_RoleFlags::RBAC_ROLEFLAG_LOGMODULEOPENING;
		}
		void SetEnforceRole()
		{
			m_Flags |= RBAC_RoleFlags::RBAC_ROLEFLAG_ENFORCEROLE;
		}

		int32_t GetSkuAccessLimit(const wstring& p_SkuPartNumber) const
		{
			if (!m_SkuAccessLimits.IsLoaded())
				m_SkuAccessLimits.Deserialize(m_SkuLimits);
			return m_SkuAccessLimits.GetLimit(p_SkuPartNumber);
		}

		const auto& GetSkuAccessLimits() const
		{
			if (!m_SkuAccessLimits.IsLoaded())
				m_SkuAccessLimits.Deserialize(m_SkuLimits);
			return m_SkuAccessLimits.GetLimits();
		}

		void SetSkuAccessLimit(const wstring& p_SkuPartNumber, int32_t p_Limit)
		{
			if (!m_SkuAccessLimits.IsLoaded())
				m_SkuAccessLimits.Deserialize(m_SkuLimits);
			m_SkuAccessLimits.SetLimit(p_SkuPartNumber, p_Limit);
			SerializeSkuAccessLimits();// sync db string and discrete data
		}

		void SerializeSkuAccessLimits()
		{
			if (!m_SkuAccessLimits.IsLoaded())
				m_SkuAccessLimits.Deserialize(m_SkuLimits);
			m_SkuLimits = m_SkuAccessLimits.Serialize();
		}

		bool HasSkuAccessLimits() const
		{
			if (!m_SkuAccessLimits.IsLoaded())
				m_SkuAccessLimits.Deserialize(m_SkuLimits);
			return m_SkuAccessLimits.IsLoaded();
		}

	private:
		int64_t		m_IDKey;
		uint32_t	m_Lease;// seconds until cloud refresh
		wstring		m_SkuLimits;
		uint32_t	m_Flags;

		struct SkuAccessLimits
		{
		public:
			bool IsLoaded() const
			{
				return !m_LimitsBySku.empty();
			}

			const std::map<wstring, int32_t>& GetLimits() const
			{
				return m_LimitsBySku;
			}

			int32_t GetLimit(const wstring& p_SkuPartNumber) const
			{
				auto it = m_LimitsBySku.find(p_SkuPartNumber);
				if (m_LimitsBySku.end() != it)
					return it->second;
				return -1;
			}

			void SetLimit(const wstring& p_SkuPartNumber, int32_t p_Limit)
			{
				if (p_Limit < 0)
					m_LimitsBySku.erase(p_SkuPartNumber);
				else
					m_LimitsBySku[p_SkuPartNumber] = p_Limit;				
			}

			wstring Serialize() const
			{
				wstring str;

				for (auto& item : m_LimitsBySku)
				{
					str += item.first;
					str += _YTEXT(":");
					str += Str::getStringFromNumber(item.second);
					str += _YTEXT(",");
				}
				return str;
			}

			bool Deserialize(const wstring& p_Serialized)
			{
				auto values = Str::explodeIntoVector(p_Serialized, wstring(_YTEXT(",")));
				for (auto& val : values)
				{
					auto pair = Str::explodeIntoVector(val, wstring(_YTEXT(":")));
					if (pair.size() == 2)
						m_LimitsBySku[pair[0]] = Str::getIntFromString(pair[1]);
				}

				// return false if malformed?
				return true;
			}

		private:
			std::map<wstring, int32_t> m_LimitsBySku;
		};

		mutable SkuAccessLimits m_SkuAccessLimits;

		friend class Delegation;
	};

	class Delegation : public RoleDelegationRecord
	{
	public:
		Delegation(const SQLiteUtil::Table& p_Table) : RoleDelegationRecord(p_Table) 
		{
			Configure();
		}

		virtual BusinessData*	GetGeneric() override { return &m_Delegation; }
		void					Configure()	override
		{
			configureGeneric();
			addColumnForModificationLog(g_ColumnNameTenant);
			logSwap(g_ColumnNameIDKey, g_ColumnNameInfo);
			configureMember(g_ColumnNameIDKey,		&m_Delegation.m_IDKey,		SQLiteUtil::SQLRecord::MemberType::INT64, true);
			configureMember(g_ColumnNameLease,		&m_Delegation.m_Lease,		SQLiteUtil::SQLRecord::MemberType::UINT32);
			configureMember(g_ColumnNameSkuLimits,	&m_Delegation.m_SkuLimits,	SQLiteUtil::SQLRecord::MemberType::WSTRING, true);
			configureMember(g_ColumnNameFlags,		&m_Delegation.m_Flags,		SQLiteUtil::SQLRecord::MemberType::UINT32, true);
		}

		BusinessDelegation m_Delegation;
	};

	struct BusinessSapio365Admin : public BusinessData
	{
		wstring		m_UserID;
		wstring		m_UserPrincipalName;
		wstring		m_UserName;
		uint64_t	m_AdminScope;
	};

	class sapio365Admin : public RoleDelegationRecord
	{
	public:
		sapio365Admin(const SQLiteUtil::Table& p_Table) : RoleDelegationRecord(p_Table)
		{
			Configure();
		}

		virtual BusinessData*	GetGeneric() override { return &m_sapio365Admin; }
		void					Configure()	override
		{
			configureGeneric();
			configureMember(g_ColumnNameUserID,				&m_sapio365Admin.m_UserID,				SQLiteUtil::SQLRecord::MemberType::WSTRING);
			configureMember(g_ColumnNameUserPrincipalName,	&m_sapio365Admin.m_UserPrincipalName,	SQLiteUtil::SQLRecord::MemberType::WSTRING, true);
			configureMember(g_ColumnNameUserName,			&m_sapio365Admin.m_UserName,			SQLiteUtil::SQLRecord::MemberType::WSTRING);
			configureMember(g_ColumnNameAdminScope,			&m_sapio365Admin.m_AdminScope,			SQLiteUtil::SQLRecord::MemberType::UINT64, true);
		}

		BusinessSapio365Admin m_sapio365Admin;
	};

	using CosmosUpdateData = map<SQLiteUtil::Table, map<wstring, RoleDelegationRecord*>>;
};

// current session delegations - loaded from SQLite
class RoleDelegation
{
public:
	RoleDelegation();
	RoleDelegation(const YTimeDate& p_LoadTime);

	void SetKey(const RoleDelegationUtil::Key& p_Key);
	void SetDelegation(const RoleDelegationUtil::Delegation& p_Delegation);
	void AddPrivilege(const RoleDelegationUtil::DelegationPrivilege& p_Privilege);
	void AddPrivilege(const RoleDelegationUtil::BusinessDelegationPrivilege& p_Privilege);
	void MarkPrivilegesAsRemoved();
	void AddFilter(const RoleDelegationUtil::Filter& p_Filter);
	void AddMember(const RoleDelegationUtil::DelegationMember& p_User);
	void RemoveFilter(const RoleDelegationUtil::Filter& p_Filter);
	void RemoveMember(const RoleDelegationUtil::DelegationMember& p_User);
	bool IsLeaseExpired() const;

	const RoleDelegationUtil::BusinessKey&							GetKey() const;
	const RoleDelegationUtil::BusinessDelegation&					GetDelegation() const;
	RoleDelegationUtil::BusinessDelegation&							GetDelegation();
	const vector<RoleDelegationUtil::BusinessDelegationPrivilege>&	GetPrivileges() const;
	vector<RoleDelegationUtil::BusinessDelegationPrivilege>&		GetPrivileges();
	const map<wstring, vector<RoleDelegationUtil::BusinessFilter>>&	GetFilters() const;
	const vector<RoleDelegationUtil::BusinessFilter>&				GetFilters(const wstring& p_ObjectType) const;
	vector<PooledString>											GetRequiredProperties(const wstring& p_ObjectType) const;
	const vector<RoleDelegationUtil::BusinessDelegationMember>&		GetMembers() const;
	int32_t															GetSkuAccessLimit(const wstring& p_SkuPartNumber) const;
	bool															HasSkuAccessLimits() const;
	bool															HasPrivilege(const RoleDelegationUtil::RBAC_Privilege& p_Privilege) const;
	RoleDelegationUtil::BusinessDelegationPrivilege&				GetPrivilege(const RoleDelegationUtil::RBAC_Privilege& p_Privilege);

	bool	HasFilter(const int64_t p_FilterID) const;
	bool	HasMember(const wstring& p_UserGraphID) const;
	bool	IsLoaded() const;
	int64_t	GetID() const;
	
	bool HasChangedComparedTo(const RoleDelegation& p_Other) const;// compares last update, key, filters, members, privileges; returns false if p_Other is olde ror has a different setup

private:
	RoleDelegationUtil::BusinessDelegation						m_Delegation;
	RoleDelegationUtil::BusinessKey								m_Key;
	vector<RoleDelegationUtil::BusinessDelegationPrivilege>		m_Privileges;
	map<wstring, vector<RoleDelegationUtil::BusinessFilter>>	m_Filters;// by object type
	vector<RoleDelegationUtil::BusinessDelegationMember>		m_Members;

	YTimeDate m_LoadTime;
};

class RoleDelegationModification : public Modification
{
public:
	RoleDelegationModification(RoleDelegationUtil::RoleDelegationRecord* p_Record);
	virtual ~RoleDelegationModification();

	virtual vector<ModificationLog> GetModificationLogs() const override;

private:
	RoleDelegationUtil::RoleDelegationRecord* m_Record;
};