#pragma once

#include "DlgFormsHTML.h"
#include "RoleDelegationUtil.h"

/**********************************************************/
/* REMOVE THIS WHEN DlgRoleEditBase is used everywhere. */
/**********************************************************/

class DlgRoleEditBase_old : public DlgFormsHTML
{
public:
	using DlgFormsHTML::DlgFormsHTML;

protected:
	virtual void								generateJSONScriptData() override;
	virtual void								generateJSONScriptDataSpecific() = 0;
	virtual bool								processPostedData(const IPostedDataTarget::PostedData& data) override;
	virtual bool								processPostedDataSpecific(const IPostedDataTarget::PostedData& data) = 0;
	virtual RoleDelegationUtil::BusinessData*	getBusinessData() = 0;
	virtual wstring								getInitMainTitle() const = 0;

	void addStringEditor(const wstring& p_PropName, const wstring& p_PropLabel, const wstring& p_PropTooltip, const wstring& p_Value);
	void addNumberEditor(const wstring& p_PropName, const wstring& p_PropLabel, const wstring& p_PropTooltip, const double& p_Value);
	void addComboEditor(const wstring& p_PropName, const wstring& p_PropLabel, const wstring& p_PropTooltip, const wstring& p_Value, const ComboEditor::LabelsAndValues& p_LabelsAndValues);
	void addCategory(const wstring& p_Title);

	static const wstring g_VarName;
	static const wstring g_VarInfo;

private:
	
};