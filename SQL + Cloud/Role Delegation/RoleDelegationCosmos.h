#pragma once

#include "CosmosSQLiteBridge.h"
#include "CosmosUtil.h"
#include "DlgDoubleProgressCommon.h"
#include "RoleDelegationSQLite.h"

class RoleDelegationCosmos : public CosmosSQLiteBridge
{
public:
	RoleDelegationCosmos();
	~RoleDelegationCosmos() = default;

	bool Write(RoleDelegationUtil::RoleDelegationRecord* p_Document, std::shared_ptr<const Sapio365Session> p_Session);// an SQL record is a Cosmos document
	bool UpdateFromSQLite(const RoleDelegationUtil::CosmosUpdateData& p_SQLdata, const CosmosUtil::SQLupdateData& p_CosmosDocuments, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Prog);
};