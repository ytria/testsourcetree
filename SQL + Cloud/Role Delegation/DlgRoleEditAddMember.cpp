#include "DlgRoleEditAddMember.h"

const wstring DlgRoleEditAddMember::g_VarUserPrincipalName = _YTEXT("USERRPRINCIPALNAME");

DlgRoleEditAddMember::DlgRoleEditAddMember(CWnd* p_Parent)
	: DlgRoleEditBase_old(DlgFormsHTML::Action::EDIT, p_Parent)
{
}

DlgRoleEditAddMember::~DlgRoleEditAddMember()
{
}

const RoleDelegationUtil::BusinessDelegationMember& DlgRoleEditAddMember::GetUser() const
{
	return m_User;
}

void DlgRoleEditAddMember::SetUser(const RoleDelegationUtil::BusinessDelegationMember& p_User)
{
	m_User = p_User;
}

void DlgRoleEditAddMember::generateJSONScriptDataSpecific()
{
	addCategory(YtriaTranslate::Do(DlgRoleEditAddMember_generateJSONScriptDataSpecific_1, _YLOC("User Member")).c_str());

	addStringEditor(g_VarUserPrincipalName, YtriaTranslate::Do(DlgUserPasswordList_OnInitDialog_3, _YLOC("User Name")).c_str(), YtriaTranslate::Do(DlgRoleEditAddMember_generateJSONScriptDataSpecific_2, _YLOC("!?NOT USED!? Enter the Office365 user name of the new member of this Role Delegation")).c_str(), m_User.m_UserPrincipalName);
}

bool DlgRoleEditAddMember::processPostedDataSpecific(const IPostedDataTarget::PostedData& data)
{
	for (const auto& item : data)
		if (MFCUtil::StringMatchW(item.first, g_VarUserPrincipalName))
			m_User.m_UserPrincipalName = item.second;

	return true;
}

wstring	DlgRoleEditAddMember::getDialogTitle() const
{
	return YtriaTranslate::Do(DlgRoleEditAddMember_getDialogTitle_1, _YLOC("!?NOT USED!? Assign New User")).c_str();
}

RoleDelegationUtil::BusinessData* DlgRoleEditAddMember::getBusinessData()
{
	return &m_User;
}

wstring	DlgRoleEditAddMember::getInitMainTitle() const
{
	return _YTEXT("DlgRoleEditUser");
}