#include "RoleDelegationTagger.h"

#include "BusinessUserConfiguration.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessSiteConfiguration.h"
#include "JsonSerializeUtil.h"
#include "RegexManager.h"
#include "RoleDelegationManager.h"
#include "RoleDelegationUtil.h"
#include "TimeUtil.h"

RoleDelegationTagger::RoleDelegationTagger()
{
}

RoleDelegationTagger::RoleDelegationTagger(std::shared_ptr<const Sapio365Session> p_Session, BusinessObject* p_BusinessObject)
	: m_Session(p_Session)
	, m_BusinessObject(nullptr)
	, m_BusinessObjectFailedDelegationFilters(false)
{
	// No assert because of GraphCache::getConnectedUser
	//ASSERT(m_Session);
	if (m_Session && m_Session->IsUseRoleDelegation() && RoleDelegationManager::GetInstance().IsReadyToRun())
	{
		m_BusinessObject = p_BusinessObject;
		ASSERT(nullptr != m_BusinessObject);
		if (nullptr != m_BusinessObject)
		{
			m_BusinessObject->SetRBACDelegationID(0);

			const auto& delegation	= RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(m_Session->GetRoleDelegationID(), m_Session);
			m_ObjectType			= MFCUtil::convertASCII_to_UNICODE(m_BusinessObject->get_type().get_name().to_string());
			const auto filters		= delegation.GetFilters(m_ObjectType);
			for (const auto& f : filters)
				m_DelegationFilters[f.m_Property].push_back(f);
		}
	}
}

void RoleDelegationTagger::deserializeStringAndTagObject(const JsonSerializeUtil::String& p_PropName, PooledString& p_Value, const web::json::object& p_Json)
{
	JsonSerializeUtil::DeserializeString(p_PropName, p_Value, p_Json);
	tagObject(p_PropName, p_Value);
}

void RoleDelegationTagger::deserializeStringAndTagObject(const JsonSerializeUtil::String& p_PropName, boost::YOpt<PooledString>& p_Value, const web::json::object& p_Json)
{
	JsonSerializeUtil::DeserializeString(p_PropName, p_Value, p_Json);
	tagObject(p_PropName, p_Value);
}

void RoleDelegationTagger::deserializeTimeDateAndTagObject(const JsonSerializeUtil::String& p_PropName, boost::YOpt<YTimeDate>& p_Value, const web::json::object& p_Json)
{
	JsonSerializeUtil::DeserializeTimeDate(p_PropName, p_Value, p_Json);
	tagObject(p_PropName, p_Value);
}

void RoleDelegationTagger::deserializeDateOnlyAndTagObject(const JsonSerializeUtil::String& p_PropName, boost::YOpt<YTimeDate>& p_Value, const web::json::object& p_Json)
{
	JsonSerializeUtil::DeserializeDateOnly(p_PropName, p_Value, p_Json);
	tagObject(p_PropName, p_Value);
}

void RoleDelegationTagger::deserializeBoolAndTagObject(const JsonSerializeUtil::String& p_PropName, boost::YOpt<bool>& p_Value, const web::json::object& p_Json)
{
	JsonSerializeUtil::DeserializeBool(p_PropName, p_Value, p_Json);
	tagObject(p_PropName, p_Value);
}

bool RoleDelegationTagger::deserializeVectorStringAndTagObject(IDatedJsonDeserializer& p_Deserializer, const vector<PooledString>& p_Vector, PooledString p_PropName, const web::json::object& p_Json)
{
    const bool exists = JsonSerializeUtil::DeserializeAny(p_Deserializer, p_PropName, p_Json);

	const auto str = Str::implode(p_Vector, _YTEXT(";"), true);
	tagObject(p_PropName, str);

    return exists;
}

void RoleDelegationTagger::markPropertyAsChecked(const JsonSerializeUtil::String& p_PropName)
{
    m_DelegationFilters.erase(p_PropName);
}

bool RoleDelegationTagger::isComputedProperty(const wstring& p_PropName) const
{
	static const auto& ucfg = BusinessUserConfiguration::GetInstance();
	static const auto& gcfg = BusinessGroupConfiguration::GetInstance();
	static const auto& scfg = BusinessSiteConfiguration::GetInstance();
	static const auto& type = RoleDelegationUtil::FilterTypes::GetInstance();

	ASSERT(type.GetTypeGroup() == m_ObjectType || type.GetTypeSite() == m_ObjectType || type.GetTypeUser() == m_ObjectType);

	return
		type.GetTypeGroup() == m_ObjectType ? gcfg.IsComputedProperty(p_PropName) :
		type.GetTypeSite()	== m_ObjectType	? scfg.IsComputedProperty(p_PropName) :
		type.GetTypeUser()	== m_ObjectType	? ucfg.IsComputedProperty(p_PropName) :
		false;
}

void RoleDelegationTagger::tagObject(const JsonSerializeUtil::String& p_PropName, const PooledString& p_Value)
{
	tagObjectFromValueAsString(p_PropName, p_Value.c_str());
}

void RoleDelegationTagger::tagObject(const JsonSerializeUtil::String& p_PropName, const boost::YOpt<PooledString>& p_Value)
{
	tagObjectFromValueAsString(p_PropName, p_Value.is_initialized() ? p_Value.get().c_str() : RoleDelegationUtil::g_BoolValueUnset.c_str());
}

void RoleDelegationTagger::tagObject(const JsonSerializeUtil::String& p_PropName, const boost::YOpt<YTimeDate>& p_Value)
{
	tagObjectFromValueAsString(p_PropName, p_Value.is_initialized() ? TimeUtil::GetInstance().GetISO8601String(p_Value.get()).c_str() : RoleDelegationUtil::g_BoolValueUnset.c_str());
}

void RoleDelegationTagger::tagObject(const JsonSerializeUtil::String& p_PropName, const boost::YOpt<bool>& p_Value)
{
	tagObjectFromValueAsString(p_PropName, p_Value.is_initialized() ? (p_Value.get() ? RoleDelegationUtil::g_BoolValueTrue.c_str() : RoleDelegationUtil::g_BoolValueFalse.c_str()) : RoleDelegationUtil::g_BoolValueUnset.c_str());
}

void RoleDelegationTagger::tagObjectFromValueAsString(const JsonSerializeUtil::String& p_PropName, const wchar_t* p_Value)
{
	if (nullptr != m_BusinessObject && m_Session && 
		!m_BusinessObject->IsRBACAuthorized() && // do not waste time tagging an accepted object
		!m_DelegationFilters.empty() && // do not waste time tagging an accepted object
		!m_BusinessObjectFailedDelegationFilters && // do not waste time tagging a discriminated object
		!isComputedProperty(p_PropName))
	{
		bool filterOut = false;

		// if property is filtered, check against all accepted values (nat�rlich filters on same property are OR'ed)
		auto findIt = m_DelegationFilters.find(p_PropName);
		if (m_DelegationFilters.end() != findIt)
		{
			filterOut = true;

			for (const auto& f : findIt->second)
			{
				if (propertyMatch(f, p_Value))
				{
					filterOut = false;
					break;
				}
			}

			m_DelegationFilters.erase(p_PropName);
		}

		m_BusinessObjectFailedDelegationFilters = filterOut;
		if (!m_BusinessObjectFailedDelegationFilters && m_DelegationFilters.empty())// if all filters were matched:
			m_BusinessObject->SetRBACDelegationID(m_Session->GetRoleDelegationID());// tag object
	}
}

void RoleDelegationTagger::tagObjectFromComputedProperties()
{
	if (nullptr != m_BusinessObject && m_Session &&
		!m_BusinessObject->IsRBACAuthorized() &&
		!m_DelegationFilters.empty() &&
		!m_BusinessObjectFailedDelegationFilters)// do not waste time tagging a discriminated object
	{
		bool filterOut = true;

		wstring value;

		for (auto it = m_DelegationFilters.begin(); it != m_DelegationFilters.end(); )
		{
			for (const auto& f : it->second)
			{
				if (!isComputedProperty(f.m_Property))
				{
					ASSERT(false);// if non-computed-property filters remain, m_BusinessObjectFailedDelegationFilters should be true
					break;
				}

				value = m_BusinessObject->GetValue(f.m_Property);
				if (propertyMatch(f, value.c_str()))
				{
					filterOut = false;
					break;
				}
			}

			it = m_DelegationFilters.erase(it);

			if (filterOut)
				break;
		}

		m_BusinessObjectFailedDelegationFilters = filterOut;
		if (!m_BusinessObjectFailedDelegationFilters && m_DelegationFilters.empty())// if all filters were matched:
			m_BusinessObject->SetRBACDelegationID(m_Session->GetRoleDelegationID());// tag object
	}
}

const Sapio365Session& RoleDelegationTagger::GetSession() const
{
    return *m_Session;
}

bool RoleDelegationTagger::propertyMatch(const RoleDelegationUtil::BusinessFilter& p_Filter, const wchar_t* p_DeserializedValue) const
{
	bool match = false;

	const bool not = RoleDelegationUtil::IsTaggerMatchMethodNOT(p_Filter.m_TaggerMatchMethod);

	const auto matchMethod = _wcsicmp(RoleDelegationUtil::g_BoolValueUnset.c_str(), p_Filter.m_Value.c_str()) == 0
								? (not ? RoleDelegationUtil::TaggerMatchMethod::COMPARISON_NOT_EQUAL_CASEINSENSITIVE : RoleDelegationUtil::TaggerMatchMethod::COMPARISON_EQUAL_CASEINSENSITIVE)
								: p_Filter.m_TaggerMatchMethod;

	switch (matchMethod)
	{
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_NOT_EQUAL_CASEINSENSITIVE:
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_EQUAL_CASEINSENSITIVE:
			match = _wcsicmp(p_DeserializedValue, p_Filter.m_Value.c_str()) == 0;
			break;
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_NOT_EQUAL_CASESENSITIVE:
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_EQUAL_CASESENSITIVE:
			match = wcscmp(p_DeserializedValue, p_Filter.m_Value.c_str()) == 0;
			break;
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_NOT_EQUAL_WILDCARD_CASEINSENSITIVE:
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_EQUAL_WILDCARD_CASEINSENSITIVE:
			match = MFCUtil::WildcardMatch(p_DeserializedValue, p_Filter.m_Value.c_str(), false);
			break;
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_NOT_EQUAL_WILDCARD_CASESENSITIVE:
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_EQUAL_WILDCARD_CASESENSITIVE:
			match = MFCUtil::WildcardMatch(p_DeserializedValue, p_Filter.m_Value.c_str(), true);
			break;
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_NOT_REGEX_CASEINSENSITIVE:
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_REGEX_CASEINSENSITIVE:
			{
				// Used to be match, but we want it to work as in CacheGrid
				//match = RegexManager::matchRegex(p_Filter.m_Value, p_DeserializedValue, false);
				list<list<RegexManager::RegexSubResult>> results;
				match = RegexManager::searchRegex(false, p_Filter.m_Value, p_DeserializedValue, results, false);
			}
			break;
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_NOT_REGEX_CASESENSITIVE:
		case RoleDelegationUtil::TaggerMatchMethod::COMPARISON_REGEX_CASESENSITIVE:
			{
				// Used to be match, but we want it to work as in CacheGrid
				//match = RegexManager::matchRegex(p_Filter.m_Value, p_DeserializedValue, true);
				list<list<RegexManager::RegexSubResult>> results;
				match = RegexManager::searchRegex(false, p_Filter.m_Value, p_DeserializedValue, results, true);
			}
			break;
		default:
			ASSERT(false);
	}

	if (not)
		match = !match;

	return match;
}