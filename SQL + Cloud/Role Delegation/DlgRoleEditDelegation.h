#pragma once

#include "BusinessSubscribedSku.h"
#include "DlgRoleEditBase.h"

// FIXME: Move this two classes to their own file.
class ICredentialAdder
{
public:
	virtual ~ICredentialAdder() = default;
	virtual RoleDelegationUtil::BusinessKey AddKey(CWnd* p_Parent) = 0;
};

class LambdaCredentialAdder : public ICredentialAdder
{
public:
	LambdaCredentialAdder(std::function<RoleDelegationUtil::BusinessKey(CWnd*)> p_AddKeyImpl)
		: m_AddKeyImpl(p_AddKeyImpl)
	{}

	virtual RoleDelegationUtil::BusinessKey AddKey(CWnd* p_Parent)
	{
		return m_AddKeyImpl(p_Parent);
	}

private:
	std::function<RoleDelegationUtil::BusinessKey(CWnd*)> m_AddKeyImpl;
};
////////////////////////////////////////////////////

class DlgRoleEditDelegation : public DlgRoleEditBase
{
public:
	DlgRoleEditDelegation(const vector<RoleDelegationUtil::Key>& p_Keys, const std::map<wstring, int32_t>& p_AssignedSkus, const std::shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent);
	virtual ~DlgRoleEditDelegation();

	void SetCredentialAdder(std::shared_ptr<ICredentialAdder> p_CredentialAdder);

	const RoleDelegation&	GetDelegation() const;
	void					SetDelegation(const RoleDelegation& p_Delegation);
	web::json::value		PopulateArrayLicenses(const vector<BusinessSubscribedSku>& p_Skus);

protected:
	virtual bool								isSpecificReadOnly() const override;
	virtual void								generateJSONScriptDataSpecific(web::json::value& p_JsonValue) override;
	virtual bool								processPostedDataSpecific(const IPostedDataTarget::PostedData& data) override;
	virtual wstring								getDialogTitle() const override;
	virtual RoleDelegationUtil::BusinessData*	getBusinessData() override;
	virtual int									getMinimumHeight() const override;

private:

	RoleDelegation m_Delegation;
	
	vector<RoleDelegationUtil::Key> m_Keys;
	const vector<BusinessSubscribedSku> m_Skus;
	std::map<wstring, int32_t> m_AssignedSkus;
	std::shared_ptr<Sapio365Session> m_Session;

	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;

	static const wstring g_VarRoleAccess;
	static const wstring g_VarRoleKey;
	static const wstring g_VarFlagsSectionFlags;
	static const wstring g_VarFlagsLogSessionLogin;
	static const wstring g_VarFlagsLogModuleOpening;
	static const wstring g_VarFlagsEnforceRole;

	static const wstring g_SkuCheckboxNamePrefix;
	static const wstring g_SkuValueNamePrefix;

	class CredentialAdderCaller;
};