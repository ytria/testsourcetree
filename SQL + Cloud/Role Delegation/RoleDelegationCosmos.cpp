#include "RoleDelegationCosmos.h"

#include "QueryDocsRequester.h"
#include "WriteDocRequester.h"

RoleDelegationCosmos::RoleDelegationCosmos() : CosmosSQLiteBridge()
{
}

bool RoleDelegationCosmos::Write(RoleDelegationUtil::RoleDelegationRecord* p_Document, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	if (nullptr != p_Document)
		TraceIntoFile::trace(_YDUMP("RoleDelegationCosmos"), _YDUMP("Write to Cosmos DB"), _YTEXTFORMAT(L"Uploading: %s", p_Document->GetGeneric()->m_Name.c_str()));

	ASSERT(nullptr != p_Document && p_Document->HasID());
	if (nullptr != p_Document && p_Document->HasID())
		rvSuccess = CosmosSQLiteBridge::Write(p_Document->GetValuesForCloudStorage(), p_Document->GetTable(), p_Session);
	else
		TraceIntoFile::trace(_YDUMP("RoleDelegationCosmos"), _YDUMP("Write to Cosmos DB"), YtriaTranslate::DoError(ActivityCosmosLogger_Write_1, _YLOC("- ERROR - NO ID / data not stored locally - cannot store into Cosmos"),_YR("Y2423")).c_str());

	return rvSuccess;
}

bool RoleDelegationCosmos::UpdateFromSQLite(const RoleDelegationUtil::CosmosUpdateData& p_SQLdata, const CosmosUtil::SQLupdateData& p_CosmosDocuments, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Prog)
{
	bool rvUpdate = true;

	wstring		cosmosROWID;
	YTimeDate	cosmosDate;

	for (const auto& p : p_SQLdata)
	{
		const auto& table	= p.first;
		const auto& sqlData = p.second;

		TraceIntoFile::trace(_YDUMP("RoleDelegationCosmos"), _YDUMP("UpdateFromSQLite"), _YTEXTFORMAT(L"Send data to Cosmos: %s", table.m_Name.c_str()));

		set<wstring> updatedIDs;

		const auto findTable = p_CosmosDocuments.find(table);
		// compare object last update date on Cosmos; if older than on SQLite, push to Cosmos
		if (p_CosmosDocuments.end() != findTable)
		{
			const auto& cosmosData = findTable->second;
			ASSERT(cosmosData.size() < (std::numeric_limits<DWORD>::max)());
			p_Prog.SetCounterTotal2(static_cast<DWORD>(cosmosData.size()));
			for (const auto& cd : cosmosData)
			{
				p_Prog.IncrementCounter2(_YFORMAT(L"Syncing existing RBAC [%s]", table.m_Name.c_str()));

				const auto& object = cd.Content.as_object();

				auto findIDjson		= object.find(SQLiteUtil::g_ColumnNameRowID);
				auto findDATEjson	= object.find(RoleDelegationUtil::g_ColumnNameDate);

				cosmosROWID.clear();
				cosmosDate.Clear();
				if (findIDjson != object.end() && findDATEjson != object.end())
				{
					if (findIDjson->second.is_string() && findDATEjson->second.is_string())
					{
						cosmosROWID = findIDjson->second.as_string();

						auto findIDsql = sqlData.find(cosmosROWID);
						if (sqlData.end() != findIDsql)
						{
							auto sqlRecord = findIDsql->second;
							ASSERT(nullptr != sqlRecord);
							if (nullptr != sqlRecord)
							{
								TimeUtil::GetInstance().ConvertTextToDate(findDATEjson->second.as_string(), cosmosDate);// ignore conversion error
								if (sqlRecord->GetLastUpdateDate() > cosmosDate)
									rvUpdate = Write(sqlRecord, p_Session) && rvUpdate;// push
								updatedIDs.insert(cosmosROWID);
							}
						}
						else
						{
							// WTF
							TraceIntoFile::trace(_YDUMP("RoleDelegationCosmos"), _YDUMP("UpdateFromSQLite"), _YTEXTFORMAT(L"ERROR Found in Cosmos: unknown item of %s: ID = %s", table.GetSQLcompliantName().c_str(), cosmosROWID.c_str()));
							rvUpdate = false;
							// ASSERT(false);
						}
					}
					else
					{
						// WTF everything must be stored as string in COSMOS! it is pointless to get numbers from the json text and reconvert these numbers to strings to create upate queries for SQLite
						TraceIntoFile::trace(_YDUMP("RoleDelegationCosmos"), _YDUMP("UpdateFromSQLite"), _YTEXTFORMAT(L"ERROR Data not stored as string in %s: ID = %s", table.GetSQLcompliantName().c_str(), cosmosROWID.c_str()));
						rvUpdate = false;
						// ASSERT(false);
					}
				}
				else
				{
					// WTF
					TraceIntoFile::trace(_YDUMP("RoleDelegationCosmos"), _YDUMP("UpdateFromSQLite"), _YTEXTFORMAT(L"ERROR Corrupted data (no Date or ID) in %s: ID = %s", table.GetSQLcompliantName().c_str(), cosmosROWID.c_str()));
					rvUpdate = false;
					// ASSERT(false);
				}
			}
		}

		// beneath the remains (new objects from SQLite or failures to compare corrupted Cosmos data) https://www.youtube.com/watch?v=FSv3-1taUTg
		ASSERT(sqlData.size() < (std::numeric_limits<DWORD>::max)());
		p_Prog.SetCounterTotal2(static_cast<DWORD>(sqlData.size()));
		for (const auto& pRemains : sqlData)
		{
			p_Prog.IncrementCounter2(_YFORMAT(L"Syncing new RBAC [%s]", table.m_Name.c_str()));
			if (updatedIDs.find(pRemains.first) == updatedIDs.end())
				rvUpdate = Write(pRemains.second, p_Session) && rvUpdate;
		}
	}

	return rvUpdate;
}