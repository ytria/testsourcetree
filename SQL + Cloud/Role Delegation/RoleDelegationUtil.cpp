#include "RoleDelegationUtil.h"

#include "AddApplicationPasswordRequester.h"
#include "ApplicationRequester.h"
#include "AppResourceAccess.h"
#include "AppGraphSessionCreator.h"
#include "BasicPageRequestLogger.h"
#include "BasicRequestLogger.h"
#include "BusinessGroup.h"
#include "BusinessSite.h"
#include "BusinessUser.h"
#include "CreateAppRequester.h"
#include "CRMpipe.h"
#include "DlgLoading.h"
#include "DlgTenantInfo.h"
#include "MainFrame.h"
#include "MSGraphCommonData.h"
#include "MsGraphFieldNames.h"
#include "MsGraphHttpRequestLogger.h"
#include "MSGraphSession.h"
#include "MSGraphUtil.h"
#include "O365AdminErrorHandler.h"
#include "O365AdminUtil.h"
#include "Office365Admin.h"
#include "OAuth2Authenticators.h"
#include "Organization.h"
#include "OrganizationRequester.h"
#include "RESTUtils.h"
#include "SafeTaskCall.h"
#include "Sapio365Session.h"
#include "SessionTypes.h"
#include "UserDeserializer.h"
#include "UserGraphSessionCreator.h"

using namespace RoleDelegationUtil;

wstring BusinessData::GetStatus() const
{
	return RoleDelegationUtil::GetStatus(IsValid() ? m_Status.get() : 666);
}

void BusinessData::ReadPostProcess(const SQLiteUtil::SQLRecord& p_SQL)
{
	m_ID = p_SQL.GetIDOpt();
}

RoleDelegation::RoleDelegation()
{
}

RoleDelegation::RoleDelegation(const YTimeDate& p_LoadTime) : m_LoadTime(p_LoadTime) 
{}

void RoleDelegation::SetKey(const Key& p_Key) 
{ 
	ASSERT(p_Key.HasID());
	if (p_Key.HasID())
		m_Key = p_Key.m_Key; 
}

void RoleDelegation::SetDelegation(const Delegation& p_Delegation) 
{ 
	ASSERT(p_Delegation.HasID());
	if (p_Delegation.HasID())
		m_Delegation = p_Delegation.m_Delegation; 
}

bool RoleDelegation::IsLoaded() const
{
	return m_Delegation.m_Status.is_initialized() && m_Delegation.m_ID.is_initialized();// TODO remove optional - keep 0 as uninit mark
}

int64_t RoleDelegation::GetID() const
{
	return m_Delegation.m_ID.is_initialized() ? m_Delegation.m_ID.get() : 0;
}

void RoleDelegation::AddPrivilege(const DelegationPrivilege& p_Privilege)
{
	ASSERT(p_Privilege.HasID());
	if (p_Privilege.HasID())
		AddPrivilege(p_Privilege.m_DelegationPrivilege);
}

void RoleDelegation::AddPrivilege(const BusinessDelegationPrivilege& p_Privilege)
{
	ASSERT(p_Privilege.IsValid());
	auto privilege = static_cast<RBAC_Privilege>(p_Privilege.m_Privilege);
	if (p_Privilege.IsValid() && privilege != RBAC_NONE && !HasPrivilege(privilege)) // check if delegation ID matches?
		m_Privileges.push_back(p_Privilege);
}

void RoleDelegation::MarkPrivilegesAsRemoved()
{
	for (auto& p : m_Privileges)
		p.m_Status = RoleStatus::STATUS_REMOVED;
}

void RoleDelegation::AddFilter(const Filter& p_Filter) 
{ 
	ASSERT(p_Filter.HasID());
	if (p_Filter.HasID()) 
		m_Filters[p_Filter.m_Filter.m_ObjectType].push_back(p_Filter.m_Filter); 
}

void RoleDelegation::AddMember(const DelegationMember& p_User)
{
	ASSERT(p_User.HasID());
	if (p_User.HasID())
		m_Members.push_back(p_User.m_DelegationMember);
}

void RoleDelegation::RemoveFilter(const Filter& p_Filter)
{
	// TODO
	ASSERT(false);
}

void RoleDelegation::RemoveMember(const DelegationMember& p_User)
{
	// TODO
	ASSERT(false);
}

bool RoleDelegation::IsLeaseExpired() const 
{ 
	return m_Delegation.GetLease() > m_LoadTime.DifferenceInSeconds(YTimeDate::GetCurrentTimeDateGMT()); 
}

const BusinessKey& RoleDelegation::GetKey() const
{
	return m_Key;
}

const BusinessDelegation& RoleDelegation::GetDelegation() const
{ 
	return m_Delegation; 
}

BusinessDelegation& RoleDelegation::GetDelegation()
{
	return m_Delegation;
}

const vector<BusinessFilter>& RoleDelegation::GetFilters(const wstring& p_ObjectType) const
{
	static vector<BusinessFilter> g_Dummy;

	auto findIt = m_Filters.find(p_ObjectType);
	if (m_Filters.end() != findIt)
		return findIt->second;

	return g_Dummy;
}

vector<PooledString> RoleDelegation::GetRequiredProperties(const wstring& p_ObjectType) const
{
	vector<PooledString> props;

	auto findIt = m_Filters.find(p_ObjectType);
	if (m_Filters.end() != findIt)
	{
		for (const auto& f : findIt->second)
			props.push_back(f.m_Property);
	}

	return props;
}

const vector<BusinessDelegationPrivilege>& RoleDelegation::GetPrivileges() const
{
	return m_Privileges;
}

vector<BusinessDelegationPrivilege>& RoleDelegation::GetPrivileges()
{
	return m_Privileges;
}

const map<wstring, vector<BusinessFilter>>& RoleDelegation::GetFilters() const
{
	return m_Filters;
}

const vector<BusinessDelegationMember>& RoleDelegation::GetMembers() const
{
	return m_Members;
}

int32_t RoleDelegation::GetSkuAccessLimit(const wstring& p_SkuPartNumber) const
{
	return m_Delegation.GetSkuAccessLimit(p_SkuPartNumber);
}

bool RoleDelegation::HasSkuAccessLimits() const
{
	return m_Delegation.HasSkuAccessLimits();
}

bool RoleDelegation::HasFilter(const int64_t p_FilterID) const
{
	for (const auto& f : m_Filters)
		for (const auto& fid : f.second)
			if (fid.m_ID == p_FilterID)
				return true;

	return false;
}

bool RoleDelegation::HasMember(const wstring& p_UserGraphID) const
{
	for (const auto& m : m_Members)
		if (m.m_UserID == p_UserGraphID)
			return true;

	return false;
}

bool RoleDelegation::HasPrivilege(const RBAC_Privilege& p_Privilege) const
{
	ASSERT(p_Privilege != RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE);
	if (p_Privilege == RBAC_NONE)
		return true;

	return std::any_of(m_Privileges.begin(), m_Privileges.end(), [&p_Privilege](const auto& r) { return r.m_Privilege == p_Privilege; });
}

BusinessDelegationPrivilege& RoleDelegation::GetPrivilege(const RoleDelegationUtil::RBAC_Privilege& p_Privilege)
{
	auto it = std::find_if(m_Privileges.begin(), m_Privileges.end(), [&p_Privilege](const auto& r) { return r.m_Privilege == p_Privilege; });
	if (m_Privileges.end() != it)
		return *it;

	static BusinessDelegationPrivilege g_Dummy;
	return g_Dummy;
}

bool RoleDelegation::HasChangedComparedTo(const RoleDelegation& p_Other) const// compares last update, key, filters, members, privileges; returns false if p_Other is olde ror has a different setup
{
	bool rvHasChanged = GetDelegation().m_UpdDate > p_Other.GetDelegation().m_UpdDate;
	if (!rvHasChanged)
	{
		rvHasChanged = GetKey().m_UpdDate > p_Other.GetKey().m_UpdDate;
		const auto& privThis	= GetPrivileges();
		const auto& privOther	= p_Other.GetPrivileges();
		rvHasChanged = privThis.size() != privOther.size();
		if (!rvHasChanged)
		{
			map<boost::YOpt<int64_t>, YTimeDate> privThisIDs;
			for (const auto& p : privThis)
				privThisIDs[p.m_ID] = p.m_UpdDate;
			rvHasChanged = privThisIDs.size() != privOther.size();
			if (!rvHasChanged)
			{
				for (const auto& p : privOther)
				{
					auto findIt = privThisIDs.find(p.m_ID);
					if (privThisIDs.end() == findIt)
					{
						rvHasChanged = true;
						break;
					}
					else if (findIt->second < p.m_UpdDate)
					{
						rvHasChanged = true;
						break;
					}
				}
			}
		}
	}

	if (!rvHasChanged)
	{
		const auto& filtersThis		= GetFilters();
		const auto& filtersOther	= p_Other.GetFilters();
		rvHasChanged = filtersThis.size() != filtersOther.size();
		if (!rvHasChanged)
		{
			for (const auto& fp : filtersThis)
			{
				auto findIt = filtersOther.find(fp.first);
				rvHasChanged = filtersOther.end() == findIt || findIt->second.size() != fp.second.size();
				if (!rvHasChanged)
				{
					map<boost::YOpt<int64_t>, YTimeDate> filterThisIDs;
					for (const auto& f : fp.second)
						filterThisIDs[f.m_ID] = f.m_UpdDate;
					rvHasChanged = filterThisIDs.size() != findIt->second.size();
					if (!rvHasChanged)
					{
						for (const auto& f : findIt->second)
						{
							auto findIt = filterThisIDs.find(f.m_ID);
							if (filterThisIDs.end() == findIt)
							{
								rvHasChanged = true;
								break;
							}
							else if (findIt->second < f.m_UpdDate)
							{
								rvHasChanged = true;
								break;
							}
						}
					}
				}
				else
					break;
			}
		}
	}

	if (!rvHasChanged)
	{
		const auto& membersThis		= GetMembers();
		const auto& membersOther	= p_Other.GetMembers();
		rvHasChanged = membersThis.size() != membersOther.size();
		if (!rvHasChanged)
		{
			map<boost::YOpt<int64_t>, YTimeDate> membersThisIDs;
			for (const auto& m : membersThis)
				membersThisIDs[m.m_ID] = m.m_UpdDate;
			rvHasChanged = membersThisIDs.size() != membersOther.size();
			if (!rvHasChanged)
			{
				for (const auto& m : membersOther)
				{
					auto findIt = membersThisIDs.find(m.m_ID);
					if (membersThisIDs.end() == findIt)
					{
						rvHasChanged = true;
						break;
					}
					else if (findIt->second < m.m_UpdDate)
					{
						rvHasChanged = true;
						break;
					}
				}
			}
		}
	}

	return rvHasChanged;
}

//////////////////////////////////
// FilterTypes

wstring FilterTypes::g_TypeGroup;
wstring FilterTypes::g_TypeSite;
wstring FilterTypes::g_TypeUser;
wstring FilterTypes::g_NameGroup;
wstring FilterTypes::g_NameSite;
wstring FilterTypes::g_NameUser;

FilterTypes& FilterTypes::GetInstance()
{
	static FilterTypes g_Instance;

	if (g_TypeGroup.empty())
	{
		g_TypeGroup = MFCUtil::convertASCII_to_UNICODE(BusinessGroup().get_type().get_name().to_string());
		g_TypeSite	= MFCUtil::convertASCII_to_UNICODE(BusinessSite().get_type().get_name().to_string());
		g_TypeUser	= MFCUtil::convertASCII_to_UNICODE(BusinessUser().get_type().get_name().to_string());
		g_NameGroup	= BusinessGroup().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>();
		g_NameSite	= BusinessSite().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>();
		g_NameUser	= BusinessUser().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>();
	}

	return g_Instance;
}

FilterTypes::FilterTypes()
{
}

const wstring& FilterTypes::GetTypeGroup()	const 
{
	return g_TypeGroup;
}

const wstring& FilterTypes::GetTypeSite()	const 
{
	return g_TypeSite;
}

const wstring& FilterTypes::GetTypeUser()	const 
{
	return g_TypeUser;
}

const wstring& FilterTypes::GetNameGroup()	const 
{
	return g_NameGroup;
}

const wstring& FilterTypes::GetNameSite()	const 
{
	return g_NameSite;
}

const wstring& FilterTypes::GetNameUser()	const 
{
	return g_NameUser;
}

namespace
{
	void AddPrivilege(tsl::ordered_map<RBAC_Privilege, RBAC_PrivilegeUI>& p_RolePrivileges, const RBAC_PrivilegeUI p_PrivilegeUI)
	{
		// If it fails, you forgot to increment RBAC_next_available_privilege_ID !!
		ASSERT(RBAC_Privilege::RBAC_next_available_privilege_ID != p_PrivilegeUI.m_Privilege);
		// If it fails, some privileges IDs are duplicated in RBAC_Privilege enum !!
		ASSERT(p_RolePrivileges.end() == p_RolePrivileges.find(p_PrivilegeUI.m_Privilege));
		p_RolePrivileges[p_PrivilegeUI.m_Privilege] = p_PrivilegeUI;
	}

	void AddSandboxPrivilege(tsl::ordered_map<RBAC_Privilege, RBAC_PrivilegeUI>& p_RolePrivileges, const RBAC_PrivilegeUI p_PrivilegeUI)
	{
		if (CRMpipe().IsFeatureSandboxed())
			AddPrivilege(p_RolePrivileges, p_PrivilegeUI);
	}
}

const tsl::ordered_map<RBAC_Privilege, RBAC_PrivilegeUI>& RoleDelegationUtil::GetAllPrivileges()
{
	static tsl::ordered_map<RBAC_Privilege, RBAC_PrivilegeUI> g_RolePrivileges;

	if (g_RolePrivileges.empty())
	{
		AddPrivilege(g_RolePrivileges, { RBAC_USER_LOADMORE, RBAC_SCOPE_USER,								YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_1, _YLOC("User - Load More")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_EDIT, RBAC_SCOPE_USER,									YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_2, _YLOC("User - Edit Properties")).c_str(), RBAC_FLAG_EDIT });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_CREATE, RBAC_SCOPE_USER,									YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_3, _YLOC("User - Create")).c_str(), RBAC_FLAG_CREATE });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_DELETE, RBAC_SCOPE_USER,									YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_4, _YLOC("User - Delete")).c_str(), RBAC_FLAG_DELETE });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_PWD_EDIT, RBAC_SCOPE_USER,								YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_5, _YLOC("User - Edit Password")).c_str(), RBAC_FLAG_EDIT });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_MANAGER_UPDATE, RBAC_SCOPE_USER,							_T("User - Update Manager"), RBAC_FLAG_EDIT, { RBAC_USER_LOADMORE } });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_LICENSES_EDIT, RBAC_SCOPE_USER,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_6, _YLOC("User - Edit Licenses")).c_str(), RBAC_FLAG_EDIT });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_LOADMAILBOX, RBAC_SCOPE_USER,							_T("User - Get Mailbox Info"), RBAC_FLAG_READ });
		//AddPrivilege(g_RolePrivileges, { RBAC_USER_MANAGEMENTHIERARCHY, RBAC_SCOPE_USER,					YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_7, _YLOC("User - View Management Hierarchy")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_ONEDRIVE_READ, RBAC_SCOPE_USER,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_8, _YLOC("User - View OneDrive Files")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_ONEDRIVE_LOADMORE, RBAC_SCOPE_USER,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_9, _YLOC("User - Load OneDrive Permissions")).c_str(), RBAC_FLAG_READ, {RBAC_USER_ONEDRIVE_READ}});
		AddPrivilege(g_RolePrivileges, { RBAC_USER_ONEDRIVE_PERMISSIONS_EDIT, RBAC_SCOPE_USER,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_10, _YLOC("User - Edit OneDrive Permissions")).c_str(), RBAC_FLAG_EDIT, {RBAC_USER_ONEDRIVE_READ, RBAC_USER_ONEDRIVE_LOADMORE} });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_ONEDRIVE_RENAME, RBAC_SCOPE_USER,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_11, _YLOC("User - Rename OneDrive Files")).c_str(), RBAC_FLAG_EDIT, {RBAC_USER_ONEDRIVE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_ONEDRIVE_DELETE, RBAC_SCOPE_USER,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_12, _YLOC("User - Delete OneDrive Files")).c_str(), RBAC_FLAG_DELETE, {RBAC_USER_ONEDRIVE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_ONEDRIVE_DOWNLOAD, RBAC_SCOPE_USER,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_13, _YLOC("User - Download OneDrive Files")).c_str(), RBAC_FLAG_READ,{ RBAC_USER_ONEDRIVE_READ } });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_ONEDRIVE_CREATEFOLDER, RBAC_SCOPE_USER,					_T("User - Create OneDrive Folders"), RBAC_FLAG_CREATE,{ RBAC_USER_ONEDRIVE_READ } });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_ONEDRIVE_ADDFILE, RBAC_SCOPE_USER,						_T("User - Add Files"), RBAC_FLAG_CREATE,{ RBAC_USER_ONEDRIVE_READ } });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_MESSAGES_READ, RBAC_SCOPE_USER,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_14, _YLOC("User - View Messages")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_MESSAGES_SEE_MAIL_CONTENT, RBAC_SCOPE_USER,				_T("User - See mail content"), RBAC_FLAG_READ, { RBAC_USER_MESSAGES_READ } });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_MESSAGES_LOADMORE, RBAC_SCOPE_USER,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_15, _YLOC("User - Load Message Attachments")).c_str(), RBAC_FLAG_READ, {RBAC_USER_MESSAGES_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_MESSAGES_ATTACHMENTS_EDIT, RBAC_SCOPE_USER,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_16, _YLOC("User - Edit Message Attachments")).c_str(), RBAC_FLAG_EDIT, {RBAC_USER_MESSAGES_READ, RBAC_USER_MESSAGES_LOADMORE} });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_MESSAGES_ATTACHMENTS_DOWNLOAD, RBAC_SCOPE_USER,			YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_17, _YLOC("User - Download Message Attachments")).c_str(), RBAC_FLAG_READ,{ RBAC_USER_MESSAGES_READ, RBAC_USER_MESSAGES_LOADMORE } });
		//AddPrivilege(g_RolePrivileges, { RBAC_USER_MESSAGES_EDIT, RBAC_SCOPE_USER,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_18, _YLOC("User - Edit Messages")).c_str(), RBAC_FLAG_READ, {RBAC_USER_MESSAGES_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_MESSAGES_DELETE, RBAC_SCOPE_USER,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_19, _YLOC("User - Delete Messages")).c_str(), RBAC_FLAG_DELETE,{ RBAC_USER_MESSAGES_READ } });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_EVENTS_READ, RBAC_SCOPE_USER,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_20, _YLOC("User - View Events")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_EVENTS_LOADMORE, RBAC_SCOPE_USER,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_21, _YLOC("User - Load Events Attachments")).c_str(), RBAC_FLAG_READ, {RBAC_USER_EVENTS_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_EVENTS_ATTACHMENTS_EDIT, RBAC_SCOPE_USER,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_22, _YLOC("User - Edit Events Attachments")).c_str(), RBAC_FLAG_EDIT, {RBAC_USER_EVENTS_READ, RBAC_USER_EVENTS_LOADMORE} });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_EVENTS_ATTACHMENTS_DOWNLOAD, RBAC_SCOPE_USER,			YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_23, _YLOC("User - Download Events Attachments")).c_str(), RBAC_FLAG_READ,{ RBAC_USER_EVENTS_READ, RBAC_USER_EVENTS_LOADMORE } });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_EVENTS_DELETE, RBAC_SCOPE_USER,							_T("User - Delete Events"), RBAC_FLAG_DELETE,{ RBAC_USER_EVENTS_READ } });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_CONTACTS_READ, RBAC_SCOPE_USER,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_24, _YLOC("User - View Contacts")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_MESSAGERULES_READ, RBAC_SCOPE_USER,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_25, _YLOC("User - View Message Rules")).c_str(), RBAC_FLAG_READ });
		AddSandboxPrivilege(g_RolePrivileges, { RBAC_USER_MESSAGERULES_EDIT, RBAC_SCOPE_USER,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_26, _YLOC("User - Edit Message Rules")).c_str(), RBAC_FLAG_EDIT, {RBAC_USER_MESSAGERULES_READ} });
		AddSandboxPrivilege(g_RolePrivileges, { RBAC_USER_MESSAGERULES_CREATE, RBAC_SCOPE_USER,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_27, _YLOC("User - Create Message Rules")).c_str(), RBAC_FLAG_CREATE, {RBAC_USER_MESSAGERULES_READ} });
		AddSandboxPrivilege(g_RolePrivileges, { RBAC_USER_MESSAGERULES_DELETE, RBAC_SCOPE_USER,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_28, _YLOC("User - Delete Message Rules")).c_str(), RBAC_FLAG_DELETE, {RBAC_USER_MESSAGERULES_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_RECYCLEBIN_MANAGE, RBAC_SCOPE_USER,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_29, _YLOC("User - Manage Recycle Bin")).c_str(), RBAC_FLAG_DELETE });
		AddPrivilege(g_RolePrivileges, { RBAC_USER_MAILBOXPERMISSIONS_READ, RBAC_SCOPE_USER,				_T("User - View Mailbox Permissions"), RBAC_FLAG_READ });

		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_LOADMORE, RBAC_SCOPE_GROUP,								YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_30, _YLOC("Group - Load More")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_EDIT, RBAC_SCOPE_GROUP,									YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_31, _YLOC("Group - Edit Properties")).c_str(), RBAC_FLAG_EDIT });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_CREATE, RBAC_SCOPE_GROUP,								YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_33, _YLOC("Group - Create")).c_str(), RBAC_FLAG_CREATE });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_DELETE, RBAC_SCOPE_GROUP,								YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_32, _YLOC("Group - Delete")).c_str(), RBAC_FLAG_DELETE });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_MEMBERS_EDIT, RBAC_SCOPE_GROUP,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_34, _YLOC("Group - Edit Members")).c_str(), RBAC_FLAG_EDIT });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_MEMBERS_EDIT_OUT_OF_SCOPE, RBAC_SCOPE_GROUP,			YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_35, _YLOC("Group - Edit Members (including out-of-scope items)")).c_str(), RBAC_FLAG_OUTOFSCOPE, {RBAC_GROUP_MEMBERS_EDIT} });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_OWNERS_EDIT, RBAC_SCOPE_GROUP,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_36, _YLOC("Group - Edit Owners")).c_str(), RBAC_FLAG_EDIT });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE, RBAC_SCOPE_GROUP,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_37, _YLOC("Group - Edit Owners (including out-of-scope items)")).c_str(), RBAC_FLAG_OUTOFSCOPE,{ RBAC_GROUP_OWNERS_EDIT } });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_DELIVERYMANAGEMENT_READ, RBAC_SCOPE_GROUP,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_38, _YLOC("Group - View Delivery Management")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_DELIVERYMANAGEMENT_EDIT, RBAC_SCOPE_GROUP,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_39, _YLOC("Group - Edit Delivery Management")).c_str(), RBAC_FLAG_EDIT,{ RBAC_GROUP_DELIVERYMANAGEMENT_READ } });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_DELIVERYMANAGEMENT_EDIT_OUT_OF_SCOPE, RBAC_SCOPE_GROUP,	YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_40, _YLOC("Group - Edit Delivery Management (including out-of-scope items)")).c_str(), RBAC_FLAG_OUTOFSCOPE,{ RBAC_GROUP_DELIVERYMANAGEMENT_EDIT } });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_ONEDRIVE_READ, RBAC_SCOPE_GROUP,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_41, _YLOC("Group - View Files")).c_str(), RBAC_FLAG_READ, {RBAC_GROUP_ONEDRIVE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_ONEDRIVE_LOADMORE, RBAC_SCOPE_GROUP,					YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_42, _YLOC("Group - Load File Permissions")).c_str(), RBAC_FLAG_READ, {RBAC_GROUP_ONEDRIVE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_ONEDRIVE_PERMISSIONS_EDIT, RBAC_SCOPE_GROUP,			YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_43, _YLOC("Group - Edit File Permissions")).c_str(), RBAC_FLAG_EDIT, {RBAC_GROUP_ONEDRIVE_READ, RBAC_GROUP_ONEDRIVE_LOADMORE} });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_ONEDRIVE_RENAME, RBAC_SCOPE_GROUP,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_44, _YLOC("Group - Rename Files")).c_str(), RBAC_FLAG_EDIT, {RBAC_GROUP_ONEDRIVE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_ONEDRIVE_DELETE, RBAC_SCOPE_GROUP,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_45, _YLOC("Group - Delete Files")).c_str(), RBAC_FLAG_DELETE, {RBAC_GROUP_ONEDRIVE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_ONEDRIVE_DOWNLOAD, RBAC_SCOPE_GROUP,					YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_46, _YLOC("Group - Download Files")).c_str(), RBAC_FLAG_READ,{ RBAC_GROUP_ONEDRIVE_READ } });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_ONEDRIVE_CREATEFOLDER, RBAC_SCOPE_GROUP,				_T("Group - Create Folders"), RBAC_FLAG_CREATE,{ RBAC_GROUP_ONEDRIVE_READ } });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_ONEDRIVE_ADDFILE, RBAC_SCOPE_GROUP,						_T("Group - Add Files"), RBAC_FLAG_CREATE,{ RBAC_GROUP_ONEDRIVE_READ } });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_EVENTS_READ, RBAC_SCOPE_GROUP,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_47, _YLOC("Group - View Events")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_EVENTS_LOADMORE, RBAC_SCOPE_GROUP,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_48, _YLOC("Group - Load Events Attachments")).c_str(), RBAC_FLAG_READ, {RBAC_GROUP_EVENTS_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_EVENTS_ATTACHMENTS_EDIT, RBAC_SCOPE_GROUP,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_49, _YLOC("Group - Edit Events Attachments")).c_str(), RBAC_FLAG_EDIT, {RBAC_GROUP_EVENTS_READ, RBAC_GROUP_EVENTS_LOADMORE} });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_EVENTS_ATTACHMENTS_DOWNLOAD, RBAC_SCOPE_GROUP,			YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_50, _YLOC("Group - Download Events Attachments")).c_str(), RBAC_FLAG_READ,{ RBAC_GROUP_EVENTS_READ, RBAC_GROUP_EVENTS_LOADMORE } });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_CONVERSATIONS_READ, RBAC_SCOPE_GROUP,					YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_51, _YLOC("Group - View Conversations")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_CONVERSATIONS_LOADMORE, RBAC_SCOPE_GROUP,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_52, _YLOC("Group - Load Conversations Attachments")).c_str(), RBAC_FLAG_READ, {RBAC_GROUP_CONVERSATIONS_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_CONVERSATIONS_ATTACHMENTS_EDIT, RBAC_SCOPE_GROUP,		YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_53, _YLOC("Group - Edit Conversations Attachments")).c_str(), RBAC_FLAG_EDIT,{ RBAC_GROUP_CONVERSATIONS_READ, RBAC_GROUP_CONVERSATIONS_LOADMORE } });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_CONVERSATIONS_ATTACHMENTS_DOWNLOAD, RBAC_SCOPE_GROUP,	YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_54, _YLOC("Group - Download Conversations Attachments")).c_str(), RBAC_FLAG_READ,{ RBAC_GROUP_CONVERSATIONS_READ, RBAC_GROUP_CONVERSATIONS_LOADMORE } });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_SITES_READ, RBAC_SCOPE_GROUP,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_55, _YLOC("Group - View Sites")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_RECYCLEBIN_MANAGE, RBAC_SCOPE_GROUP,					YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_56, _YLOC("Group - Manage Recycle Bin")).c_str(), RBAC_FLAG_DELETE });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_CHANNELS_READ, RBAC_SCOPE_GROUP,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_57, _YLOC("Group - View Channels")).c_str(), RBAC_FLAG_READ });
		AddPrivilege(g_RolePrivileges, { RBAC_GROUP_CHANNELS_LOADMORE, RBAC_SCOPE_GROUP,					YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_58, _YLOC("Group - Load Channels Attachments")).c_str(), RBAC_FLAG_READ,{ RBAC_GROUP_CHANNELS_READ } });
		AddSandboxPrivilege(g_RolePrivileges, { RBAC_GROUP_CHANNELS_ATTACHMENTS_EDIT, RBAC_SCOPE_GROUP,			YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_59, _YLOC("Group - Edit Channels Attachments")).c_str(),	RBAC_FLAG_EDIT,{ RBAC_GROUP_CHANNELS_READ, RBAC_GROUP_CHANNELS_LOADMORE } });
		AddSandboxPrivilege(g_RolePrivileges, { RBAC_GROUP_CHANNELS_ATTACHMENTS_DOWNLOAD, RBAC_SCOPE_GROUP,		YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_60, _YLOC("Group - Download Channels Attachments")).c_str(),	RBAC_FLAG_READ,{ RBAC_GROUP_CHANNELS_READ, RBAC_GROUP_CHANNELS_LOADMORE } });

		AddPrivilege(g_RolePrivileges, { RBAC_SITE_READ, RBAC_SCOPE_SITE,									YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_61, _YLOC("Site - View")).c_str(), RBAC_FLAG_READ });
		AddSandboxPrivilege(g_RolePrivileges, { RBAC_SITE_EDIT, RBAC_SCOPE_SITE,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_62, _YLOC("Site - Edit")).c_str(), RBAC_FLAG_EDIT, {RBAC_SITE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_SITE_ONEDRIVE_READ, RBAC_SCOPE_SITE,							YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_63, _YLOC("Site - View Files")).c_str(), RBAC_FLAG_READ, {RBAC_SITE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_SITE_ONEDRIVE_LOADMORE, RBAC_SCOPE_SITE,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_64, _YLOC("Site - Load File Permissions")).c_str(), RBAC_FLAG_READ, {RBAC_SITE_READ, RBAC_SITE_ONEDRIVE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_SITE_ONEDRIVE_PERMISSIONS_EDIT, RBAC_SCOPE_SITE,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_65, _YLOC("Site - Edit File Permissions")).c_str(), RBAC_FLAG_EDIT, {RBAC_SITE_READ, RBAC_SITE_ONEDRIVE_READ, RBAC_SITE_ONEDRIVE_LOADMORE} });
		AddPrivilege(g_RolePrivileges, { RBAC_SITE_ONEDRIVE_RENAME, RBAC_SCOPE_SITE,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_66, _YLOC("Site - Rename Files")).c_str(), RBAC_FLAG_EDIT, {RBAC_SITE_READ, RBAC_SITE_ONEDRIVE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_SITE_ONEDRIVE_DELETE, RBAC_SCOPE_SITE,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_67, _YLOC("Site - Delete Files")).c_str(), RBAC_FLAG_DELETE, {RBAC_SITE_READ, RBAC_SITE_ONEDRIVE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_SITE_ONEDRIVE_DOWNLOAD, RBAC_SCOPE_SITE,						YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_68, _YLOC("Site - Download Files")).c_str(),	RBAC_FLAG_READ,	{RBAC_SITE_READ, RBAC_SITE_ONEDRIVE_READ} });
		AddPrivilege(g_RolePrivileges, { RBAC_SITE_ONEDRIVE_CREATEFOLDER, RBAC_SCOPE_SITE,					_T("Site - Create Folders"), RBAC_FLAG_CREATE,{ RBAC_SITE_READ, RBAC_SITE_ONEDRIVE_READ } });
		AddPrivilege(g_RolePrivileges, { RBAC_SITE_ONEDRIVE_ADDFILE, RBAC_SCOPE_SITE,						_T("Site - Add Files"), RBAC_FLAG_CREATE,{ RBAC_SITE_READ, RBAC_SITE_ONEDRIVE_READ } });
		AddSandboxPrivilege(g_RolePrivileges, { RBAC_SITE_PERMISSIONS_READ, RBAC_SCOPE_SITE,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_69, _YLOC("Site - View Permissions")).c_str(), RBAC_FLAG_READ, {RBAC_SITE_READ} });
		AddSandboxPrivilege(g_RolePrivileges, { RBAC_SITE_PERMISSIONS_EDIT, RBAC_SCOPE_SITE,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_70, _YLOC("Site - Edit Permissions")).c_str(), RBAC_FLAG_EDIT, {RBAC_SITE_READ, RBAC_SITE_PERMISSIONS_READ} });

		AddPrivilege(g_RolePrivileges, { RBAC_TENANT_ROLES_EDIT,			RBAC_SCOPE_OTHER,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_71, _YLOC("Edit Tenant Roles")).c_str(), RBAC_FLAG_EDIT });
		AddPrivilege(g_RolePrivileges, { RBAC_TENANT_ROLES_EDIT_OUT_OF_SCOPE, RBAC_SCOPE_OTHER,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_72, _YLOC("Edit Tenant Roles (including out-of-scope items)")).c_str(), RBAC_FLAG_OUTOFSCOPE,{ RBAC_TENANT_ROLES_EDIT } });
		AddPrivilege(g_RolePrivileges, { RBAC_TENANT_GROUP_EDITSETTINGS,	RBAC_SCOPE_OTHER,				YtriaTranslate::Do(RoleDelegationUtil_GetAllPrivileges_73, _YLOC("Edit Tenant Group Settings")).c_str(), RBAC_FLAG_EDIT });

		AddPrivilege(g_RolePrivileges, { RBAC_CHANNEL_ONEDRIVE_LOADMORE, RBAC_SCOPE_SITE,					_T("Channel - Load File Permissions"), RBAC_FLAG_READ, {RBAC_GROUP_CHANNELS_READ/*, RBAC_CHANNEL_ONEDRIVE_READ*/} });
		AddPrivilege(g_RolePrivileges, { RBAC_CHANNEL_ONEDRIVE_PERMISSIONS_EDIT, RBAC_SCOPE_SITE,			_T("Channel - Edit File Permissions"), RBAC_FLAG_EDIT, {RBAC_GROUP_CHANNELS_READ/*, RBAC_CHANNEL_ONEDRIVE_READ*/, RBAC_CHANNEL_ONEDRIVE_LOADMORE} });
		AddPrivilege(g_RolePrivileges, { RBAC_CHANNEL_ONEDRIVE_RENAME, RBAC_SCOPE_SITE,						_T("Channel - Rename Files"), RBAC_FLAG_EDIT, {RBAC_GROUP_CHANNELS_READ/*, RBAC_CHANNEL_ONEDRIVE_READ*/} });
		AddPrivilege(g_RolePrivileges, { RBAC_CHANNEL_ONEDRIVE_DELETE, RBAC_SCOPE_SITE,						_T("Channel - Delete Files"), RBAC_FLAG_DELETE, {RBAC_GROUP_CHANNELS_READ/*, RBAC_CHANNEL_ONEDRIVE_READ*/} });
		AddPrivilege(g_RolePrivileges, { RBAC_CHANNEL_ONEDRIVE_DOWNLOAD, RBAC_SCOPE_SITE,					_T("Channel - Download Files"),	RBAC_FLAG_READ,	{RBAC_GROUP_CHANNELS_READ/*, RBAC_CHANNEL_ONEDRIVE_READ*/} });
		AddPrivilege(g_RolePrivileges, { RBAC_CHANNEL_ONEDRIVE_CREATEFOLDER, RBAC_SCOPE_SITE,				_T("Channel - Create Folders"), RBAC_FLAG_CREATE,{ RBAC_GROUP_CHANNELS_READ/*, RBAC_CHANNEL_ONEDRIVE_READ*/ } });
		AddPrivilege(g_RolePrivileges, { RBAC_CHANNEL_ONEDRIVE_ADDFILE, RBAC_SCOPE_SITE,					_T("Channel - Add Files"), RBAC_FLAG_CREATE,{ RBAC_GROUP_CHANNELS_READ/*, RBAC_CHANNEL_ONEDRIVE_READ*/ } });
	}

	return g_RolePrivileges;
}

const vector<RBAC_PrivilegeUI>& RoleDelegationUtil::GetPrivileges(const RBAC_Scope p_Scope)
{
	static map<RBAC_Scope, vector<RBAC_PrivilegeUI>> g_RolePrivilegesByScope;

	if (g_RolePrivilegesByScope.empty())
	{
		for (const auto& p : GetAllPrivileges())
			g_RolePrivilegesByScope[p.second.m_Scope].push_back(p.second);
	}

	return g_RolePrivilegesByScope[p_Scope];
}

const vector<RBAC_PrivilegeUI>&	RoleDelegationUtil::GetPrivileges(const uint32_t p_Scope)
{
	return GetPrivileges(static_cast<RBAC_Scope>(p_Scope));
}

const wstring& RoleDelegationUtil::GetPrivilegeLabel(const RBAC_Privilege& p_Privilege)
{
	const auto& allPrivileges = GetAllPrivileges();

	auto findIt = allPrivileges.find(p_Privilege);
	ASSERT(findIt != allPrivileges.end());
	if (findIt != allPrivileges.end())
		return findIt->second.m_Label;

	static const wstring& g_Invalid = YtriaTranslate::Do(RoleDelegationUtil_GetPrivilegeLabel_1, _YLOC("Invalid Permission")).c_str();
	return g_Invalid;
}

const wstring& RoleDelegationUtil::GetPrivilegeLabel(const uint32_t p_Privilege)
{
	return GetPrivilegeLabel(static_cast<RBAC_Privilege>(p_Privilege));
}

bool RoleDelegationUtil::IsUserPrivilege(const RBAC_Privilege p_Privilege)
{
	return RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE == p_Privilege || IsScopePrivilege(RBAC_SCOPE_USER, p_Privilege);
}

bool RoleDelegationUtil::IsGroupPrivilege(const RBAC_Privilege p_Privilege)
{
	return RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE == p_Privilege || IsScopePrivilege(RBAC_SCOPE_GROUP, p_Privilege);
}

bool RoleDelegationUtil::IsSitePrivilege(const RBAC_Privilege p_Privilege)
{
	return RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE == p_Privilege || IsScopePrivilege(RBAC_SCOPE_SITE, p_Privilege);
}

bool RoleDelegationUtil::IsScopePrivilege(const RBAC_Scope p_Scope, const RBAC_Privilege p_Privilege)
{
	const auto& allPrivileges = GetAllPrivileges();

	bool isInScope = false;

	const auto findIt = allPrivileges.find(p_Privilege);
	if (allPrivileges.end() != findIt)
		isInScope = findIt->second.m_Scope == p_Scope;

	return isInScope;
}

const std::pair<wstring, wstring>& RoleDelegationUtil::GetScopeJSONKeyAndLabel(const RBAC_Scope p_Scope)
{
	static map<RBAC_Scope, std::pair<wstring, wstring>> g_ScopeJSONkeys;

	if (g_ScopeJSONkeys.empty())
	{
		// A C H T U N G !
		// DO NOT LOCALIZE JSON KEY
		// must always match JSON keys for DlgRoleEditDelegation

		g_ScopeJSONkeys[RBAC_SCOPE_USER]	= std::make_pair(_YTEXT("USERS"),	YtriaTranslate::Do(RoleDelegationUtil_GetScopeJSONKeyAndLabel_1, _YLOC("All Users")).c_str());
		g_ScopeJSONkeys[RBAC_SCOPE_GROUP]	= std::make_pair(_YTEXT("GROUPS"),	YtriaTranslate::Do(RoleDelegationUtil_GetScopeJSONKeyAndLabel_2, _YLOC("All Groups")).c_str());
		g_ScopeJSONkeys[RBAC_SCOPE_SITE]	= std::make_pair(_YTEXT("SITES"),	_T("All SharePoint Sites"));
		g_ScopeJSONkeys[RBAC_SCOPE_OTHER]	= std::make_pair(_YTEXT("OTHER"),	YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_21, _YLOC("Other")).c_str());
	}

	auto findIt = g_ScopeJSONkeys.find(p_Scope);
	ASSERT(findIt != g_ScopeJSONkeys.end());
	if (findIt != g_ScopeJSONkeys.end())
		return findIt->second;

	static const std::pair<wstring, wstring> g_Invalid = std::make_pair(_YTEXT("INVALID_SCOPE"), YtriaTranslate::Do(RoleDelegationUtil_GetScopeJSONKeyAndLabel_5, _YLOC("INVALID SCOPE")).c_str());
	return g_Invalid;
}

const std::pair<wstring, wstring>& RoleDelegationUtil::GetScopeJSONKeyAndLabel(const uint32_t p_Scope)
{
	return GetScopeJSONKeyAndLabel(static_cast<RBAC_Scope>(p_Scope));
}

const map<RoleStatus, wstring>& RoleDelegationUtil::GetValueAndLabel_Status()
{
	static map<RoleStatus, wstring> g_RoleStatus;

	if (g_RoleStatus.empty())
	{
		g_RoleStatus[RoleStatus::STATUS_OK]			= YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("OK")).c_str();
		g_RoleStatus[RoleStatus::STATUS_LOCKED]		= YtriaTranslate::Do(O365Session_initStatusStrings_3, _YLOC("Locked")).c_str();
		g_RoleStatus[RoleStatus::STATUS_REMOVED]	= YtriaTranslate::Do(DlgMultiServer_getListStateString_1, _YLOC("Removed")).c_str();
	}

	return g_RoleStatus;
}

const wstring& RoleDelegationUtil::GetStatus(const uint32_t p_Status)
{
	static const wstring	g_Invalid	= YtriaTranslate::Do(DlgActions_doValidate_1, _YLOC("INVALID")).c_str();
	static const auto&		statusMap	= GetValueAndLabel_Status();

	const auto findIt = statusMap.find(static_cast<RoleStatus>(p_Status));
	if (statusMap.end() != findIt)
		return findIt->second;

	return g_Invalid;
}

const map<TaggerMatchMethod, wstring>& RoleDelegationUtil::GetValueAndLabel_TaggerMatchMethodMain()
{
	static map<TaggerMatchMethod, wstring> g_TaggerMatchMethodMain;

	if (g_TaggerMatchMethodMain.empty())
	{
		const auto& allmethods = GetValueAndLabel_TaggerMatchMethod();

		g_TaggerMatchMethodMain[TaggerMatchMethod::COMPARISON_EQUAL_CASEINSENSITIVE]			= allmethods.at(COMPARISON_EQUAL_CASEINSENSITIVE);
		g_TaggerMatchMethodMain[TaggerMatchMethod::COMPARISON_EQUAL_WILDCARD_CASEINSENSITIVE]	= allmethods.at(COMPARISON_EQUAL_WILDCARD_CASEINSENSITIVE);
		g_TaggerMatchMethodMain[TaggerMatchMethod::COMPARISON_REGEX_CASEINSENSITIVE]			= allmethods.at(COMPARISON_REGEX_CASEINSENSITIVE);
	}

	return g_TaggerMatchMethodMain;
}

const map<TaggerMatchMethod, wstring>& RoleDelegationUtil::GetValueAndLabel_TaggerMatchMethod()
{
	static map<TaggerMatchMethod, wstring> g_TaggerMatchMethod;

	if (g_TaggerMatchMethod.empty())
	{
		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_EQUAL_CASEINSENSITIVE]		= YtriaTranslate::Do(DlgGridComparatorSetup_OnInitDialog_15, _YLOC("Equal")).c_str(); // "Equal (case insensitive)"
		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_EQUAL_CASESENSITIVE]			= YtriaTranslate::Do(RoleDelegationUtil_GetValueAndLabel_TaggerMatchMethod_4, _YLOC("Equal (case sensitive)")).c_str();
		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_NOT_EQUAL_CASEINSENSITIVE]	= YtriaTranslate::Do(RoleDelegationUtil_GetValueAndLabel_TaggerMatchMethod_5, _YLOC("Not Equal")).c_str(); // "Not Equal (case insensitive)"
		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_NOT_EQUAL_CASESENSITIVE]		= YtriaTranslate::Do(RoleDelegationUtil_GetValueAndLabel_TaggerMatchMethod_6, _YLOC("Not Equal (case sensitive)")).c_str();

		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_EQUAL_WILDCARD_CASEINSENSITIVE]		= YtriaTranslate::Do(RoleDelegationUtil_GetValueAndLabel_TaggerMatchMethod_7, _YLOC("Match Wildcard Expression")).c_str(); // "Match Wildcard Expression (case insensitive)"
		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_EQUAL_WILDCARD_CASESENSITIVE]			= YtriaTranslate::Do(RoleDelegationUtil_GetValueAndLabel_TaggerMatchMethod_8, _YLOC("Match Wildcard Expression (case sensitive)")).c_str();
		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_NOT_EQUAL_WILDCARD_CASEINSENSITIVE]	= YtriaTranslate::Do(RoleDelegationUtil_GetValueAndLabel_TaggerMatchMethod_9, _YLOC("Not Match Wildcard Expression")).c_str(); // "Not Match Wildcard Expression (case insensitive)"
		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_NOT_EQUAL_WILDCARD_CASESENSITIVE]		= YtriaTranslate::Do(RoleDelegationUtil_GetValueAndLabel_TaggerMatchMethod_10, _YLOC("Not Match Wildcard Expression (case sensitive)")).c_str();

		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_REGEX_CASEINSENSITIVE]		= YtriaTranslate::Do(RoleDelegationUtil_GetValueAndLabel_TaggerMatchMethod_11, _YLOC("Match Regex")).c_str(); // "Match Regex (case insensitive)"
		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_REGEX_CASESENSITIVE]			= YtriaTranslate::Do(RoleDelegationUtil_GetValueAndLabel_TaggerMatchMethod_12, _YLOC("Match Regex (case sensitive)")).c_str();
		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_NOT_REGEX_CASEINSENSITIVE]	= YtriaTranslate::Do(RoleDelegationUtil_GetValueAndLabel_TaggerMatchMethod_13, _YLOC("Not Match Regex")).c_str(); // "Not Match Regex (case insensitive)"
		g_TaggerMatchMethod[TaggerMatchMethod::COMPARISON_NOT_REGEX_CASESENSITIVE]		= YtriaTranslate::Do(RoleDelegationUtil_GetValueAndLabel_TaggerMatchMethod_14, _YLOC("Not Match Regex (case sensitive)")).c_str();
	}

	return g_TaggerMatchMethod;
}

bool RoleDelegationUtil::IsTaggerMatchMethodNOT(const uint32_t p_TMM)
{
	auto delta = p_TMM - GetTaggerMatchMethodMain(p_TMM);
	return	delta == 2 || delta == 3;
}

bool RoleDelegationUtil::IsTaggerMatchMethodCASEINSENSITIVE(const uint32_t p_TMM)
{
	auto delta = p_TMM - GetTaggerMatchMethodMain(p_TMM);
	return	delta == 1 || delta == 3;
}

uint32_t RoleDelegationUtil::GetTaggerMatchMethod(const uint32_t p_MainMethod, const bool p_SubNOT, const bool p_SubCASESENSITIVE)
{
	uint32_t matchMethod = p_MainMethod;

	if (p_SubCASESENSITIVE && p_SubNOT)
		matchMethod += 3;
	else if (p_SubNOT)
		matchMethod += 2;
	else if (p_SubCASESENSITIVE)
		matchMethod += 1;

	return matchMethod;
}

uint32_t RoleDelegationUtil::GetTaggerMatchMethodMain(const uint32_t p_TMM)
{
	return p_TMM - (p_TMM % 4);
}

bool RoleDelegationUtil::CreateApplication(const std::shared_ptr<Sapio365Session>& p_Session, const wstring& p_TargetTenant, const PersistentSession& p_PersistentSession, const wstring& p_NewAppDisplayName, CFrameWnd* p_FrameSource)
{
	ASSERT(!p_NewAppDisplayName.empty());
	DlgLoading::RunModal(YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_17, _YLOC("Creating new application...")).c_str()
		, p_FrameSource
		, [p_Session, appDisplayName = p_NewAppDisplayName, sessionCpy = p_PersistentSession, p_FrameSource, p_TargetTenant](const std::shared_ptr<DlgLoading>& p_DlgLoading) mutable
	{
		const auto& graphSession = p_Session->GetMSGraphSession(Sapio365Session::USER_SESSION);

		CreateApplicationRequester createAppRequester(appDisplayName, AppResourceAccess::GetUltraAdminResourceAccess());
		createAppRequester.Send(p_Session, YtriaTaskData()).GetTask().wait();

		if (createAppRequester.GetResult().GetResult().Response.status_code() >= 200 && createAppRequester.GetResult().GetResult().Response.status_code() < 300)
		{
			const wstring id = createAppRequester.GetData().m_Id ? createAppRequester.GetData().m_Id->c_str() : _YTEXT("");
			const wstring appId = createAppRequester.GetData().m_AppId ? createAppRequester.GetData().m_AppId->c_str() : _YTEXT("");

			if (QueryApp(p_Session, id))
			{
				sessionCpy.SetAppId(appId);
				SessionsSqlEngine::Get().Save(sessionCpy);
				AddApplicationPasswordRequester appPwdRequester(appDisplayName, id);
				appPwdRequester.Send(p_Session, YtriaTaskData()).GetTask().wait();

				wstring password;
				bool addPwdSuccess = appPwdRequester.GetData().m_SecretText.is_initialized();
				if (addPwdSuccess)
				{
					password = *appPwdRequester.GetData().m_SecretText;
					sessionCpy.SetAppClientSecret(password);
					SessionsSqlEngine::Get().Save(sessionCpy);

					Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
					ASSERT(app);
					if (app)
					{
						YCallbackMessage::DoPost(p_FrameSource, [=] {
							auto consentCallback = [=](const ConsentResult& p_Result) {
								if (!p_Result.HasError)
								{
									AppGraphSessionCreator creator(p_TargetTenant, appId, password, appDisplayName);
									auto appSession = creator.Create();

									YCallbackMessage::DoPost(p_FrameSource, [=] () mutable {
										auto testResult = TestSessionWithDialog(appSession, p_TargetTenant, p_FrameSource);
										if (testResult.first) // Success?
											MainFrame::SetupElevatedSessionFromAdvancedSession(p_Session, sessionCpy, appId, password, appSession, p_FrameSource);
									});
								}
								else
								{
									YCodeJockMessageBox msgBox(p_FrameSource,
										DlgMessageBox::eIcon::eIcon_Error,
										YtriaTranslate::Do(Office365AdminApp_GetConsent_4, _YLOC("Consent")).c_str(),
										YtriaTranslate::Do(Office365AdminApp_GetConsent_5, _YLOC("Consent was not given. Please check information and resubmit.")).c_str(),
										YtriaTranslate::Do(Office365AdminApp_GetConsent_7, _YLOC("Error message: %1"), p_Result.ErrorDescription.c_str()),
										{ { IDOK, YtriaTranslate::Do(Office365AdminApp_GetConsent_6, _YLOC("OK")).c_str() } });
									msgBox.DoModal();
								}
							};

							// Before asking for consent, verify that we can get a token
							AppGraphSessionCreator creator(p_TargetTenant, appId, password, appDisplayName);
							auto appSession = creator.Create();

							WaitTillAppReadyForConsent(appSession, p_TargetTenant, p_FrameSource);

							wstring partnerDetails;
							if (p_Session->IsPartnerAdvanced() || p_Session->IsPartnerElevated())
								partnerDetails = _T("As a Partner, you will need Admin credentials on your customer tenant to proceed. Otherwise, you might want to copy the consent URL and send it to your customer so that they can do it for you.");
							YCodeJockMessageBox dlg(nullptr,
								DlgMessageBox::eIcon_Information,
								_T("Application Consent"),
								_YFORMAT(L"You will be asked to sign in and provide consent to the new application (%s).\r\n\r\nNote that the availability of the application can take a moment while Microsoft updates its components.", appDisplayName.c_str()),
								partnerDetails,
								partnerDetails.empty()
								? std::unordered_map<UINT, wstring>{
										{ IDOK, _T("Proceed") },
										{ IDCANCEL, _T("Skip") },
									}
								: std::unordered_map<UINT, wstring>{
										{ IDOK, _T("Proceed") },
										{ IDYES, _T("Copy URL") },
										{ IDCANCEL, _T("Skip") },
									});
							auto wantConsent = dlg.DoModal();
							if (wantConsent == IDYES)
							{
								ASSERT(!partnerDetails.empty());
								MFCUtil::copyUnicodeTextToClipboard(SessionTypes::GetUltraAdminConsentUrl(p_TargetTenant, appId, SessionTypes::GetPartnerAlternativeUltraAdminConsentRedirectUri()).to_uri().to_string());

								YCodeJockMessageBox(nullptr
									, DlgMessageBox::eIcon_Information
									, _T("Consent URL copied")
									, _T("Consent URL has been copied to clipboard. You may give it to your customer and try connecting again when he's done.")
									, _YTEXT("")
									, { {IDOK, _YTEXT("OK")} }).DoModal();
							}
							else if (wantConsent == IDOK)
							{
								wstring consentDlgTitle = _YFORMAT(L"App administrator consent - '%s'", appDisplayName.c_str());
								app->GetConsent(SessionTypes::GetUltraAdminConsentUrl(p_TargetTenant, appId, SessionTypes::GetDefaultUltraAdminConsentRedirectUri()).to_uri(), SessionTypes::g_UltraAdmin, p_FrameSource, consentCallback, consentDlgTitle);
							}
						});
					}
				}
			}
		}
		else
		{
			YCodeJockMessageBox msgBox(p_FrameSource,
				DlgMessageBox::eIcon::eIcon_Error,
				YtriaTranslate::Do(Office365AdminApp_CreateFullAdminSession_1, _YLOC("Error")).c_str(),
				_T("An error occurred while creating application."),
				Sapio365Session::ParseGraphJsonError(createAppRequester.GetResult().GetResult().GetErrorMessage()).m_ErrorDescription,
				{ { IDOK, YtriaTranslate::Do(Office365AdminApp_CreateFullAdminSession_3, _YLOC("OK")).c_str() } });
			msgBox.DoModal();
		}
	});

	return true;
}

bool RoleDelegationUtil::QueryApp(const std::shared_ptr<Sapio365Session>& p_Session, const wstring& p_Id)
{
	bool success = false;

	auto logger = std::make_shared<BasicRequestLogger>(_T("application"));

	ApplicationRequester appReq(p_Id, logger);
	appReq.Send(p_Session, YtriaTaskData()).GetTask().wait();
	while (appReq.GetResult().GetResult().Response.status_code() != 200)
	{
		appReq.Send(p_Session, YtriaTaskData()).GetTask().wait();

		if (appReq.GetResult().GetResult().Response.status_code() != 404 &&
			appReq.GetResult().GetResult().Response.status_code() != 200)
		{
			wstring exceptMessage;
			if (appReq.GetResult().GetResult().m_Exception)
				exceptMessage = MFCUtil::convertUTF8_to_UNICODE(appReq.GetResult().GetResult().m_Exception->what());
			else if (appReq.GetResult().GetResult().m_RestException)
				exceptMessage = appReq.GetResult().GetResult().m_RestException->WhatUnicode();
			LoggerService::Debug(wstring(_YTEXT("Querying created application failed: ")) + exceptMessage);
			break;
		}
		pplx::wait(1000);
	}

	success = appReq.GetResult().GetResult().Response.status_code() == 200;

	return success;
}

pair<bool, wstring> RoleDelegationUtil::TestSession(const std::shared_ptr<MSGraphSession>& p_GraphSession, const wstring& p_TargetTenant)
{
	CWaitCursor _;
	pair<bool, wstring> resAndErrMsg;

	// Try to get a token : validate credentials
	auto getTokenRes = p_GraphSession->Start(nullptr).get();
	resAndErrMsg.first = getTokenRes.state == RestAuthenticationResult::State::Success;

	if (resAndErrMsg.first)
	{
		// Try to get the Organization: validate consent

		YtriaTaskData task;
		auto logger = std::make_shared<BasicPageRequestLogger>(_T("organization"));
		auto requester = std::make_shared<OrganizationRequester>(false, logger);
		resAndErrMsg = safeTaskCall(requester->Send(nullptr, p_GraphSession, task).Then([requester, &p_TargetTenant]()
			{
				pair<bool, wstring> result{ true, _YTEXT("") };
				if (requester->GetData().empty())
				{
					result = { false, _YTEXT("No Organization.") };
				}
				else
				{
					// Probably useless... above Start will fail if there's a tenant mismatch.
					const auto& org = requester->GetData()[0];
					if (!MFCUtil::StringMatchW(org.GetInitialDomain(), p_TargetTenant))
						result = { false, _YTEXT("Tenant mismatch.") };
				}

				return result;
			}), p_GraphSession, [task](const std::exception_ptr& p_ExceptPtr)
			{
				pair<bool, wstring> result{ false, YtriaTranslate::Do(DlgAutomationConsole_OnStopAutomation_1, _YLOC("Canceled")).c_str() };
				if (!task.IsCanceled())
				{
					ASSERT(p_ExceptPtr);
					if (p_ExceptPtr)
					{
						try
						{
							std::rethrow_exception(p_ExceptPtr);
						}
						catch (const RestException & e)
						{
							result.second = HttpError(e).GetFullErrorMessage();
						}
						catch (const std::exception & e)
						{
							result.second = SapioError(e).GetFullErrorMessage();
						}
					}
				}
				return result;
			}, task).get();
	}
	else
	{
		resAndErrMsg.second = getTokenRes.error_message;
	}

	return resAndErrMsg;
}

bool RoleDelegationUtil::WaitTillAppReadyForConsent(const std::shared_ptr<MSGraphSession>& p_Session, const wstring& p_TargetTenant, CWnd* p_Parent)
{
	CWaitCursor _;
	auto resAndErrMsg = std::make_shared<pair<bool, wstring>>();
	resAndErrMsg->first = false;

	bool appExists = false;
	DlgLoading::RunModal(_T("Waiting for application. This may take a few minutes..."), p_Parent, [p_Session, resAndErrMsg, p_TargetTenant, &appExists](const std::shared_ptr<DlgLoading>&) {
		const int maxTries = 40;
		int tries = 0;
		::Sleep(30000);
		while (tries < maxTries && !appExists)
		{
			++tries;

			auto getTokenRes = p_Session->Start(nullptr).get();
			resAndErrMsg->first = getTokenRes.state == RestAuthenticationResult::State::Success;
			appExists = resAndErrMsg->first;
			::Sleep(1500);
		}
	});

	return appExists;
}

pair<bool, wstring> RoleDelegationUtil::TestSessionWithDialog(const std::shared_ptr<MSGraphSession>& p_Session, const wstring& p_TargetTenant, CWnd* p_Parent)
{
	CWaitCursor _;
	auto resAndErrMsg = std::make_shared<pair<bool, wstring>>();
	resAndErrMsg->first = false;

	DlgLoading::RunModal(_T("Testing application. This may take a few minutes..."), p_Parent, [p_Session, resAndErrMsg, p_TargetTenant](const std::shared_ptr<DlgLoading>&) {
		const int maxTries = 25;
		int tries = 0;
		while (tries < maxTries && !resAndErrMsg->first)
		{
			++tries;
			::Sleep(1500);
			
			// Try to get a token : validate credentials
			auto getTokenRes = p_Session->Start(nullptr).get();
			resAndErrMsg->first = getTokenRes.state == RestAuthenticationResult::State::Success;

			if (resAndErrMsg->first)
			{
				// Try to get the Organization: validate consent
				YtriaTaskData task;
				auto logger = std::make_shared<BasicPageRequestLogger>(_T("organization"));
				auto requester = std::make_shared<OrganizationRequester>(false, logger);
				*resAndErrMsg = safeTaskCall(requester->Send(nullptr, p_Session, task).Then([requester, p_TargetTenant]()
					{
						pair<bool, wstring> result{ true, _YTEXT("") };
						if (requester->GetData().empty())
						{
							result = { false, _YTEXT("No Organization.") };
						}
						else
						{
							// Probably useless... above Start will fail if there's a tenant mismatch.
							const auto& org = requester->GetData()[0];
							if (!MFCUtil::StringMatchW(org.GetInitialDomain(), p_TargetTenant))
								result = { false, _YTEXT("Tenant mismatch.") };
						}

						return result;
					}), p_Session, [task](const std::exception_ptr& p_ExceptPtr)
					{
						pair<bool, wstring> result{ false, YtriaTranslate::Do(DlgAutomationConsole_OnStopAutomation_1, _YLOC("Canceled")).c_str() };
						if (!task.IsCanceled())
						{
							ASSERT(p_ExceptPtr);
							if (p_ExceptPtr)
							{
								try
								{
									std::rethrow_exception(p_ExceptPtr);
								}
								catch (const RestException & e)
								{
									result.second = HttpError(e).GetFullErrorMessage();
								}
								catch (const std::exception & e)
								{
									result.second = SapioError(e).GetFullErrorMessage();
								}
							}
						}
						return result;
					}, task).get();
			}
			else
			{
				resAndErrMsg->second = getTokenRes.error_message;
			}
		}
	});

    return *resAndErrMsg;
}

pair<bool, wstring> RoleDelegationUtil::TestAdminSession(const wstring& p_Tenant, const wstring& p_Username, const wstring& p_Password)
{
	UserGraphSessionCreator userSessionCreator(p_Tenant, p_Username, p_Password);
	auto session = userSessionCreator.Create();
	auto res		= TestSession(session, p_Tenant);

	if (res.first)
	{
		// Check if company admin

		// Pass nullptr to avoid role tagging.
		YtriaTaskData task;
		auto deserializer = std::make_shared<UserDeserializer>(nullptr);
		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, SessionIdentifier());
		const auto connectedUserIdRes = safeTaskCall(session->GetConnectedUser(deserializer, httpLogger, task).Then([deserializer]
		{
			return std::pair<PooledString, wstring>{deserializer->GetData().GetID(), _YTEXT("")};
		}), session, [task](const std::exception_ptr& p_ExceptPtr)
		{
			pair<PooledString, wstring> result{ _YTEXT(""), YtriaTranslate::Do(DlgAutomationConsole_OnStopAutomation_1, _YLOC("Canceled")).c_str() };
			if (!task.IsCanceled())
			{
				ASSERT(p_ExceptPtr);
				if (p_ExceptPtr)
				{
					try
					{
						std::rethrow_exception(p_ExceptPtr);
					}
					catch (const RestException& e)
					{
						result.second = HttpError(e).GetFullErrorMessage();
					}
					catch (const std::exception& e)
					{
						result.second = SapioError(e).GetFullErrorMessage();
					}
				}
			}
			return result;
		}, task).get();

		if (connectedUserIdRes.first.IsEmpty())
		{
			res = { false, connectedUserIdRes.second };
		}
		else
		{
			if (!Sapio365Session::IsCompanyAdmin(session, connectedUserIdRes.first))
				res = { false, YtriaTranslate::Do(RoleDelegationUtil_TestAdminSession_2, _YLOC("You need a user with the admin role: Global Administrator.")).c_str() };
		}
	}

	return res;
}

pair<bool, wstring> RoleDelegationUtil::TestUltraAdminSession(const wstring& p_Tenant, const wstring& p_Username, const wstring& p_Password)
{
	AppGraphSessionCreator creator(p_Tenant, p_Username, p_Password, _YTEXT(""));
	auto appSession = creator.Create();

    return TestSession(appSession, p_Tenant);
}

const wstring& RoleDelegationUtil::GetTaggerMatchMethod(const uint32_t p_TMM)
{
	static const wstring g_Invalid	= YtriaTranslate::Do(DlgActions_doValidate_1, _YLOC("INVALID")).c_str();
	static const auto& tmmMap		= GetValueAndLabel_TaggerMatchMethod();

	const auto findIt = tmmMap.find(static_cast<TaggerMatchMethod>(p_TMM));
	if (tmmMap.end() != findIt)
		return findIt->second;

	return g_Invalid;
}

RoleDelegationModification::RoleDelegationModification(RoleDelegationUtil::RoleDelegationRecord* p_Record) : 
	Modification(State::AppliedLocally, p_Record ?  YtriaTranslate::Do(GridFrameBase_GetFrameTitle_1, _YLOC("%1 :: %2"), p_Record->GetTable().m_NameForDisplay.c_str(), p_Record->GetGeneric()->m_Name.c_str()) : _YTEXT("???")),
	m_Record(p_Record)
{
}

RoleDelegationModification::~RoleDelegationModification()
{
}

vector<Modification::ModificationLog> RoleDelegationModification::GetModificationLogs() const
{
	vector<Modification::ModificationLog> modificationLogs;

	ASSERT(m_Record != nullptr);
	if (m_Record != nullptr)
	{
		static const set<wstring> hideValues = {g_ColumnNamePWD, g_ColumnNameAppPWD};

		const auto		oldValues	= m_Record->GetOldValuesForLog();
		vector<wstring> pk			= m_Record->GetPKForModificationLog();

		const bool	isCreateAction	=	oldValues.empty();
		wstring		action			=	m_Record->IsRemoved()	?	YtriaTranslate::Do(ApplicationShortcuts_ApplicationShortcuts_166, _YLOC("Delete")) :
										isCreateAction			?	YtriaTranslate::Do(DlgBusinessEditHTML_getDialogTitle_3, _YLOC("Create")) :
																	YtriaTranslate::Do(DlgHelpMain_OnInitDialog_5, _YLOC("Update"));
		if (m_Record->IsRemoved())
			modificationLogs.emplace_back(pk, GetObjectName(), action, GridBackendUtil::g_NoValueString, GridBackendUtil::g_NoValueString, GridBackendUtil::g_NoValueString, State::AppliedLocally);
		else
			for (const auto newValues : m_Record->GetValuesForLog())
			{
				const auto& c = newValues.first;
				if (m_Record->IsValue4ModificationLog(c.m_Name))
				{
					auto findIt = oldValues.find(c);
					ASSERT(isCreateAction || oldValues.end() != findIt);
					auto ov = isCreateAction || oldValues.end() == findIt ? GridBackendUtil::g_NoValueString : findIt->second;
					auto nv = newValues.second;
					if (ov != nv)
					{
						if (c.m_Name == RoleDelegationUtil::g_ColumnNamePrivilege)
						{
							if (!isCreateAction)
								ov = GetPrivilegeLabel(Str::getUINT32FromString(ov));
							nv = GetPrivilegeLabel(Str::getUINT32FromString(nv));
						}
						else if (c.m_Name == RoleDelegationUtil::g_ColumnNameFlags)
						{
							if (!isCreateAction)
								ov = Str::implode(BusinessDelegation::GetFlagLabels(Str::getUINT32FromString(ov)), _YTEXT(", "));
							nv = Str::implode(BusinessDelegation::GetFlagLabels(Str::getUINT32FromString(nv)), _YTEXT(", "));
						}

						modificationLogs.emplace_back(pk, GetObjectName(), action, c.m_NameForDisplay.empty() ? c.m_Name : c.m_NameForDisplay,
														isCreateAction ? GridBackendUtil::g_NoValueString : (hideValues.find(c.m_Name) != hideValues.end() ? g_HiddenValueFromLog : ov),
														isCreateAction ? (hideValues.find(c.m_Name) != hideValues.end() ? g_HiddenValueFromLog : nv) : GridBackendUtil::g_NoValueString,
														State::AppliedLocally);
					}
				}
			}
	}

	return modificationLogs;
}

const tsl::ordered_map<RBAC_AdminScope, pair<wstring, wstring>>& RoleDelegationUtil::GetAdminScopes()
{
	static tsl::ordered_map<RBAC_AdminScope, pair<wstring, wstring>> g_AdminScopes;

	if (g_AdminScopes.empty())
	{
		g_AdminScopes[RBAC_ADMIN_MANAGER]		= std::make_pair<wstring, wstring>(_T("General Manager (sapio365)"),	_T("Able to use sapio365 like a Global Administrator regarding RBAC, activity logs, and comment management."));
		g_AdminScopes[RBAC_ADMIN_RBAC]			= std::make_pair<wstring, wstring>(_T("RBAC Configuration Manager"),	_T("Able to set/edit RBAC configuration."));
		g_AdminScopes[RBAC_ADMIN_LOGS]			= std::make_pair<wstring, wstring>(_T("Activity Logs Manager"),			_T("Able to access all user activity logs."));
		g_AdminScopes[RBAC_ADMIN_ANNOTATIONS]	= std::make_pair<wstring, wstring>(_T("Comments Manager"),				_T("Able to edit all comments."));
	}

	return g_AdminScopes;
}

wstring RoleDelegationUtil::BusinessDelegation::g_LabelLogSessionLogin;
wstring RoleDelegationUtil::BusinessDelegation::g_LabelLogModuleOpening;
wstring RoleDelegationUtil::BusinessDelegation::g_LabelEnforceRole;