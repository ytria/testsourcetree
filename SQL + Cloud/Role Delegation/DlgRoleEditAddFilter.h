#pragma once

#include "ResizableDialog.h"
#include "../../Resource.h"
#include "RoleDelegationCfgFiltersGrid.h"
#include "RoleDelegationUtil.h"

class Sapio365Session;

class DlgRoleEditAddFilter : public ResizableDialog
{
public:
	DlgRoleEditAddFilter(const vector<RoleDelegation>& p_RoleDelegations, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent);
	virtual ~DlgRoleEditAddFilter();

	enum { IDD = IDD_DLG_ROLEDELEGATION_SELECTFILTERS };
	
	virtual void OnOK() override;

	const map<int64_t, wstring>& GetSelectedFiltersIDNames() const;

protected:
	DECLARE_MESSAGE_MAP()

	virtual void DoDataExchange(CDataExchange* pDX) override;
	virtual BOOL OnInitDialogSpecificResizable() override;
	afx_msg void OnBtnAdd();

private:
	const vector<RoleDelegation>&	m_RoleDelegations;
	map<int64_t, wstring>			m_SelectedFiltersIDNames;

	RoleDelegationCfgFiltersGrid	m_Grid;
	GridBackendColumn*				m_FIlterAlreadyPresentInDelegations;

	std::shared_ptr<Sapio365Session> m_Session;

	CXTPButton	m_btnOK;
	CXTPButton	m_btnCancel;
	CExtLabel	m_staticInfo;
	CXTPButton	m_btnAdd;
};
