#include "DlgRoleEditDelegation.h"

#include "DlgRoleEditKey.h"
#include "JSONUtil.h"
#include "RoleDelegationManager.h"
#include "Str.h"
#include "CRMpipe.h"
#include "SubscribedSkusRequester.h"
#include "BasicRequestLogger.h"
#include "UserGraphSessionCreator.h"

class DlgRoleEditDelegation::CredentialAdderCaller : public CCmdTarget
{
public:
	static wstring g_VarNoCredentialKeySet;

	CredentialAdderCaller(std::shared_ptr<ICredentialAdder> p_CredentialAdder, DlgRoleEditDelegation& p_Dlg, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent)
		: m_CredentialAdder(p_CredentialAdder)
		, m_Dlg(p_Dlg)
		, m_Session(p_Session)
		, m_Parent(p_Parent)
	{
		EnableAutomation();

		if (g_VarNoCredentialKeySet.empty())
			g_VarNoCredentialKeySet = _YTEXT("-NOKEYSET-");

	}

	BSTR CreateNewCredential()
	{
		// Should return "id�label"
		wstring result;

		ASSERT(m_CredentialAdder);
		if (m_CredentialAdder)
		{
			auto newKey = m_CredentialAdder->AddKey(m_Parent);
			if (newKey.IsValid() && newKey.m_ID)
				result = Str::getStringFromNumber(*newKey.m_ID) + _YTEXT("�") + newKey.m_Name;
		}

		return _bstr_t(CString(result.c_str()));
	}

	BSTR GetLicensePool(LPCWSTR p_CredName)
	{
		wstring jsonLicenses = _YTEXT("[]");

		auto wCred = wstring(p_CredName);
		if (!wCred.empty() && !MFCUtil::StringMatchW(wCred, g_VarNoCredentialKeySet))
		{
			// Are doing this request for the first time?
			if (m_CredRequests.find(p_CredName) == m_CredRequests.end())
			{
				auto credentials	= RoleDelegationManager::GetInstance().GetKey(Str::getINT64FromString(p_CredName), m_Session);

				UserGraphSessionCreator userSessionCreator(credentials.m_TargetTenant, credentials.m_Login, credentials.m_PWD);
				auto tempSession	= userSessionCreator.Create();
				auto testResult		= RoleDelegationUtil::TestSession(tempSession, credentials.m_TargetTenant);

				// ASSERT(testResult.first); no ASSERT: credentials might have died
				if (testResult.first)
				{
					auto requester = std::make_shared<SubscribedSkusRequester>(std::make_shared<BasicRequestLogger>(_YTEXT("subscribed skus")));

					auto tempSapioSession = std::make_shared<Sapio365Session>();
					tempSapioSession->SetMSGraphSession(tempSession);

					bool error = false;
					try
					{
						requester->Send(tempSapioSession, YtriaTaskData()).GetTask().wait();
					}
					catch (const RestException & p_Except)
					{
						YCodeJockMessageBox errorMessage(nullptr, DlgMessageBox::eIcon_ExclamationWarning,
															_T("Error while requesting subscribed skus"),
															p_Except.WhatUnicode(),
															_YTEXT(""),
															{ { IDOK, _T("Ok") } });
						errorMessage.DoModal();
						error = true;
					}
					catch (const std::exception & p_Except)
					{
						YCodeJockMessageBox errorMessage(nullptr, DlgMessageBox::eIcon_ExclamationWarning,
															_T("Error while requesting subscribed skus"),
															Str::convertFromUTF8(p_Except.what()),
															_YTEXT(""),
															{ { IDOK, _T("Ok") } });
						errorMessage.DoModal();
						error = true;
					}

					if (!error)
					{
						m_Sskus = requester->GetData();
						jsonLicenses = m_Dlg.PopulateArrayLicenses(m_Sskus).serialize();
						m_CredRequests[p_CredName] = jsonLicenses;
					}
				}
				else
				{
					YCodeJockMessageBox errorMessage(nullptr, DlgMessageBox::eIcon_ExclamationWarning,
														_T("Error"),
														wstring(_T("An error occurred while querying licenses using the selected credentials.")),
														Sapio365Session::ParseGraphJsonError(testResult.second).m_ErrorDescription,
														{ { IDOK, _T("Ok") } });
					errorMessage.DoModal();
				}
			}
			else // Request already done?
			{
				jsonLicenses = m_CredRequests.at(p_CredName);
			}
		}

		return _bstr_t(jsonLicenses.c_str());
	}

	const auto& GetSskus() const
	{
		return m_Sskus;
	}

private:
	DECLARE_DISPATCH_MAP()

	std::shared_ptr<ICredentialAdder> m_CredentialAdder;
	std::shared_ptr<Sapio365Session> m_Session;
	std::map<wstring, wstring> m_CredRequests;
	vector<BusinessSubscribedSku> m_Sskus;
	DlgRoleEditDelegation& m_Dlg;
	CWnd* m_Parent;
};

wstring DlgRoleEditDelegation::CredentialAdderCaller::g_VarNoCredentialKeySet;

BEGIN_DISPATCH_MAP(DlgRoleEditDelegation::CredentialAdderCaller, CCmdTarget)
	DISP_FUNCTION(DlgRoleEditDelegation::CredentialAdderCaller, "createNewCredential", CreateNewCredential, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(DlgRoleEditDelegation::CredentialAdderCaller, "getLicensePool", GetLicensePool, VT_BSTR, VTS_WBSTR)
END_DISPATCH_MAP()

const wstring DlgRoleEditDelegation::g_VarRoleAccess	= _YTEXT("ROLEACCESS");
const wstring DlgRoleEditDelegation::g_VarRoleKey		= _YTEXT("ROLEKEY");

const wstring DlgRoleEditDelegation::g_VarFlagsSectionFlags			= _YTEXT("FLAGS");
const wstring DlgRoleEditDelegation::g_VarFlagsLogSessionLogin		= _YTEXT("FLAG_LOGSESSIONLOGIN");
const wstring DlgRoleEditDelegation::g_VarFlagsLogModuleOpening		= _YTEXT("FLAG_LOGMODULEOPENING");
const wstring DlgRoleEditDelegation::g_VarFlagsEnforceRole			= _YTEXT("FLAG_ENFORCE");

const wstring DlgRoleEditDelegation::g_SkuCheckboxNamePrefix	= _YTEXT("Checkbox_");
const wstring DlgRoleEditDelegation::g_SkuValueNamePrefix		= _YTEXT("Value_");

std::map<wstring, int, Str::keyLessInsensitive> DlgRoleEditDelegation::g_Sizes;

DlgRoleEditDelegation::DlgRoleEditDelegation(const vector<RoleDelegationUtil::Key>& p_Keys
											, const std::map<wstring, int32_t>& p_AssignedSkus
											, const std::shared_ptr<Sapio365Session>& p_Session
											, CWnd* p_Parent)
	: DlgRoleEditBase(p_Parent)
	, m_Keys(p_Keys)
	, m_AssignedSkus(p_AssignedSkus)
	, m_Session(p_Session)
{
	// Load hbs sizes
	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_RBACCONFIGROLEDEF_SIZE, g_Sizes);
		ASSERT(success);
	}
}

DlgRoleEditDelegation::~DlgRoleEditDelegation()
{
}

void DlgRoleEditDelegation::SetCredentialAdder(std::shared_ptr<ICredentialAdder> p_CredentialAdder)
{
	GetHtmlView()->SetExternal(std::make_shared<CredentialAdderCaller>(p_CredentialAdder, *this, m_Session, this));
}

const RoleDelegation& DlgRoleEditDelegation::GetDelegation() const
{
	return m_Delegation;
}

void DlgRoleEditDelegation::SetDelegation(const RoleDelegation& p_Delegation)
{
	m_Delegation = p_Delegation;
}

web::json::value DlgRoleEditDelegation::PopulateArrayLicenses(const vector<BusinessSubscribedSku>& p_Skus)
{
	json::value licenses = json::value::array();

	const wstring checked = _YTEXT("x");
	const wstring unchecked = _YTEXT("");

	size_t choiceIndex = 0;
	for (auto& sku : p_Skus)
	{
		const wstring checkboxName = g_SkuCheckboxNamePrefix + wstring(*sku.GetSkuPartNumber());
		const wstring checkboxLabel = *sku.GetSkuPartNumber();
		const wstring valueName = g_SkuValueNamePrefix + wstring(*sku.GetSkuPartNumber());
		const auto limit = m_Delegation.GetSkuAccessLimit(*sku.GetSkuPartNumber());
		const auto alreadyAssigned = limit >= 0 ? m_AssignedSkus[*sku.GetSkuPartNumber()] - limit : 0;
		const wstring valueLabel = sku.GetPrepaidUnits() && sku.GetPrepaidUnits()->m_Enabled
			? YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_25, _YLOC("%1 purchased ( %2 assigned to other roles )"), Str::getStringFromNumber(*sku.GetPrepaidUnits()->m_Enabled).c_str(), Str::getStringFromNumber(alreadyAssigned).c_str())
			: YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_26, _YLOC("0 purchased ( %1 assigned to other roles )"), Str::getStringFromNumber(alreadyAssigned).c_str());

		licenses[choiceIndex++] =
			JSON_BEGIN_OBJECT
			JSON_PAIR_KEY(_YTEXT("select"))		JSON_PAIR_VALUE(JSON_STRING(checkboxName)),
			JSON_PAIR_KEY(_YTEXT("default"))	JSON_PAIR_VALUE(JSON_STRING((limit >= 0 ? checked : unchecked))),
			JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(checkboxLabel))),
			JSON_PAIR_KEY(_YTEXT("field"))		JSON_PAIR_VALUE(JSON_STRING(valueName)),
			JSON_PAIR_KEY(_YTEXT("value"))		JSON_PAIR_VALUE(JSON_STRING(limit >= 0 ? Str::getStringFromNumber(limit) : _YTEXT("0"))),
			JSON_PAIR_KEY(_YTEXT("info"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(valueLabel)))
			JSON_END_OBJECT;
	}

	return licenses;
}

RoleDelegationUtil::BusinessData* DlgRoleEditDelegation::getBusinessData()
{
	return &m_Delegation.GetDelegation();
}

bool DlgRoleEditDelegation::isSpecificReadOnly() const
{
	return false;
}

int DlgRoleEditDelegation::getMinimumHeight() const
{
	const auto it = g_Sizes.find(_YTEXT("min-height"));
	ASSERT(g_Sizes.end() != it);
	if (g_Sizes.end() != it)
		return it->second;
	return DlgRoleEditBase::getMinimumHeight();
}

void DlgRoleEditDelegation::generateJSONScriptDataSpecific(web::json::value& p_JsonValue)
{
	const auto hasAddCredentials = GetHtmlView()->HasExternal();
	ASSERT(hasAddCredentials); // Did you forget to call SetCredentialAdder? 

	auto& items = p_JsonValue.as_array();

	auto itemIndex = items.size();
	items[itemIndex] =
		JSON_BEGIN_OBJECT
            JSON_BEGIN_PAIR
                _YTEXT("hbs"),  JSON_STRING(_YTEXT("rbacRoleDef"))
            JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("introName"),  JSON_STRING(_T("Credentials"))
            JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("introTextOne"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_2, _YLOC("Select the Credentials to be used by the Role.")).c_str()))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
                _YTEXT("newcredButton"),  JSON_STRING((hasAddCredentials ? MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_3, _YLOC("New Credentials...")).c_str()) : _YTEXT("")))
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
                _YTEXT("errorNewCredLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_4, _YLOC("Error! The new Credentials provided is already in the list...")).c_str()))
            JSON_END_PAIR,

			JSON_BEGIN_PAIR
                _YTEXT("keysField"),  JSON_STRING(g_VarRoleKey)
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
                _YTEXT("keysLabel"),  JSON_STRING(_T("Credentials:"))
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
                _YTEXT("keysUnsetLabel"),  JSON_STRING(_T("-- no credential available --"))
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("keysUnsetValue"), JSON_STRING(CredentialAdderCaller::g_VarNoCredentialKeySet)
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
                _YTEXT("keysChoice"),
				JSON_BEGIN_ARRAY
                JSON_END_ARRAY
            JSON_END_PAIR,	
		JSON_END_OBJECT;

	const wstring checked	= _YTEXT("x");
	const wstring unchecked = _YTEXT("");
	
	{
		auto& choices = items[itemIndex].as_object()[_YTEXT("keysChoice")].as_array();

		if (m_Delegation.GetDelegation().GetIDKey() == 0 && !m_Keys.empty())
			m_Delegation.GetDelegation().GetIDKey() = m_Keys.back().GetID();

		size_t choiceIndex = 0;
		for (auto& key : m_Keys)
		{
			choices[choiceIndex++] =
				JSON_BEGIN_OBJECT
					JSON_PAIR_KEY(_YTEXT("value"))         JSON_PAIR_VALUE(JSON_STRING(Str::getStringFromNumber(key.GetID()))),
					JSON_PAIR_KEY(_YTEXT("label"))         JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(key.m_Key.m_Name))),
					JSON_PAIR_KEY(_YTEXT("default"))       JSON_PAIR_VALUE(JSON_STRING(m_Delegation.GetDelegation().GetIDKey() == key.GetID() ? checked : unchecked)),
				JSON_END_OBJECT;
		}
	}

	++itemIndex;

	items[itemIndex] =
		JSON_BEGIN_OBJECT
            JSON_BEGIN_PAIR
                _YTEXT("hbs"),  JSON_STRING(_YTEXT("rbacProperty"))
            JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("introName"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_T("Options")))
            JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("introTextOne"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_T("Set role logging and enforcement parameters.")))
            JSON_END_PAIR,

			JSON_BEGIN_PAIR
                _YTEXT("propertyField"),  JSON_STRING(g_VarFlagsSectionFlags)
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
                _YTEXT("propertyLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_T("Flags")))
            JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("propertyChoice"),
				JSON_BEGIN_ARRAY
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("value"),	JSON_STRING(g_VarFlagsLogSessionLogin),
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("label"),	JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(RoleDelegationUtil::BusinessDelegation::g_LabelLogSessionLogin))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("default"),	JSON_STRING(m_Delegation.GetDelegation().IsLogSessionLogin() ? checked : unchecked)
						JSON_END_PAIR,
					JSON_END_OBJECT,
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("value"),	JSON_STRING(g_VarFlagsLogModuleOpening),
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("label"),	JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(RoleDelegationUtil::BusinessDelegation::g_LabelLogModuleOpening))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("default"),	JSON_STRING(m_Delegation.GetDelegation().IsLogModuleOpening() ? checked : unchecked)
						JSON_END_PAIR,
					JSON_END_OBJECT,
					JSON_BEGIN_OBJECT
						JSON_BEGIN_PAIR
							_YTEXT("value"),	JSON_STRING(g_VarFlagsEnforceRole),
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("label"),	JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(RoleDelegationUtil::BusinessDelegation::g_LabelEnforceRole))
						JSON_END_PAIR,
						JSON_BEGIN_PAIR
							_YTEXT("default"),	JSON_STRING(m_Delegation.GetDelegation().IsEnforceRole() ? checked : unchecked)
						JSON_END_PAIR,
					JSON_END_OBJECT,
                JSON_END_ARRAY
            JSON_END_PAIR,
		JSON_END_OBJECT;

	++itemIndex;

	items[itemIndex] =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("hbs"), JSON_STRING(_YTEXT("rbacPrivileges"))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("introName"), JSON_STRING(_T("Permissions"))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("introTextOne"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_8, _YLOC("Define the permissions provided to the current Role.")).c_str()))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("labelAllUnset"), JSON_STRING(_T("Unset All"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("labelAllRead"), JSON_STRING(_T("All Read"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("labelAllEdit"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_11, _YLOC("All Read & Edit")).c_str()))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("labelAllPriv"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_12, _YLOC("Full Privileges")).c_str()))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("accessDebugListBtn"), JSON_STRING( CRMpipe().IsFeatureSandboxed() ? _YTEXT("X"): _YTEXT("") )
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("accessLabel"), JSON_STRING(_T("Permissions:"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("accessField"), JSON_STRING(g_VarRoleAccess)
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("accessCount"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_14, _YLOC("( % selected )")).c_str()))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("accessChoice"),
				JSON_BEGIN_ARRAY
				JSON_END_ARRAY
			JSON_END_PAIR,
		JSON_END_OBJECT;

	static const int numberOfDigits = 3;

	{
		auto& choices = items[itemIndex].as_object()[_YTEXT("accessChoice")].as_array();
		wstring required;
		for (uint32_t scope = RoleDelegationUtil::RBAC_Scope::RBAC_SCOPE_USER; scope < RoleDelegationUtil::RBAC_Scope::RBAC_SCOPE_MAX; scope++)
		{
			const auto& scopeText = RoleDelegationUtil::GetScopeJSONKeyAndLabel(scope);
			choices[scope] =
					JSON_BEGIN_OBJECT
						JSON_PAIR_KEY(_YTEXT("value"))         JSON_PAIR_VALUE(JSON_STRING(scopeText.first)),
						JSON_PAIR_KEY(_YTEXT("label"))         JSON_PAIR_VALUE(JSON_STRING(scopeText.second)),
						//JSON_PAIR_KEY(_YTEXT("default"))       JSON_PAIR_VALUE(JSON_STRING(?)),
						JSON_BEGIN_PAIR
							_YTEXT("subChoice"),
							JSON_BEGIN_ARRAY								
							JSON_END_ARRAY
						JSON_END_PAIR,
					JSON_END_OBJECT;
			
			auto& subChoices			= choices[scope].as_object()[_YTEXT("subChoice")].as_array();
			const auto& scopePrivileges	= RoleDelegationUtil::GetPrivileges(scope);

			for (size_t k = 0; k < scopePrivileges.size(); k++)
			{
				const auto& thisPrivilege = scopePrivileges[k];
				
				required.clear();
				for (const auto& req : thisPrivilege.m_Required)
				{
					ASSERT(req <= 999); // If it fails, increase numberOfDigits
					required += Str::getStringFromNumber<uint32_t>(req, numberOfDigits) + _YTEXT(",");
				}
				if (!required.empty())
					required.pop_back();

				ASSERT(thisPrivilege.m_Privilege <= 999); // If it fails, increase numberOfDigits
				subChoices[k] =
					JSON_BEGIN_OBJECT
						JSON_PAIR_KEY(_YTEXT("value"))         JSON_PAIR_VALUE(JSON_STRING(Str::getStringFromNumber<uint32_t>(thisPrivilege.m_Privilege, numberOfDigits))),
						JSON_PAIR_KEY(_YTEXT("label"))         JSON_PAIR_VALUE(JSON_STRING(thisPrivilege.m_Label)),
						JSON_PAIR_KEY(_YTEXT("isread"))        JSON_PAIR_VALUE(JSON_STRING(thisPrivilege.HasFlag(RoleDelegationUtil::RBAC_FLAG_READ) ? checked : unchecked)),
						JSON_PAIR_KEY(_YTEXT("isedit"))        JSON_PAIR_VALUE(JSON_STRING(thisPrivilege.HasFlag(RoleDelegationUtil::RBAC_FLAG_EDIT) ? checked : unchecked)),
						JSON_PAIR_KEY(_YTEXT("isdelete"))      JSON_PAIR_VALUE(JSON_STRING(thisPrivilege.HasFlag(RoleDelegationUtil::RBAC_FLAG_DELETE) ? checked : unchecked)),
						JSON_PAIR_KEY(_YTEXT("iscreate"))      JSON_PAIR_VALUE(JSON_STRING(thisPrivilege.HasFlag(RoleDelegationUtil::RBAC_FLAG_CREATE) ? checked : unchecked)),
						JSON_PAIR_KEY(_YTEXT("default"))       JSON_PAIR_VALUE(JSON_STRING(m_Delegation.HasPrivilege(thisPrivilege.m_Privilege) ? checked : unchecked)),
						JSON_PAIR_KEY(_YTEXT("required"))      JSON_PAIR_VALUE(JSON_STRING(required)),
					JSON_END_OBJECT;
			}
		}
	}

	++itemIndex;

	// Str::getStringFromNumber<uint32_t>(RoleDelegationUtil::RBAC_USER_LICENSES_EDIT)   --- need to flag this here

	ASSERT(RoleDelegationUtil::RBAC_USER_LICENSES_EDIT <= 999); // If it fails, increase numberOfDigits

	items[itemIndex] =
		JSON_BEGIN_OBJECT
            JSON_BEGIN_PAIR
                _YTEXT("hbs"),  JSON_STRING(_YTEXT("rbacLicensePools"))
            JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("introName"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_15, _YLOC("Delegation Privileges for Licenses")).c_str()))
            JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("introTextOne"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_16, _YLOC("Set restrictions on delegation privileges available to this role for all Tenant purchased licenses.")).c_str()))
            JSON_END_PAIR,

		    JSON_BEGIN_PAIR
                _YTEXT("labelEnforceAll"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_18, _YLOC("Enforce All")).c_str()))
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
                _YTEXT("labelFreeAll"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_17, _YLOC("No Restriction")).c_str()))
            JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("colTitleSku"), JSON_STRING(_T("SKU"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("colTitleNb"), JSON_STRING(_T("Limit"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("colTitleTotal"), JSON_STRING(_T("Totals"))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("accessDebugListBtn"), JSON_STRING(CRMpipe().IsFeatureSandboxed() ? _YTEXT("X") : _YTEXT(""))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
				_YTEXT("editLicAccessVal"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(Str::getStringFromNumber<uint32_t>(RoleDelegationUtil::RBAC_USER_LICENSES_EDIT, numberOfDigits)))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("noEditLicAccessSetLabel"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_22, _YLOC("To allow License Delegation, you will need to enable the permission 'User - Edit Licenses'.")).c_str()))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("noEditLicAccessSetButton"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditDelegation_generateJSONScriptDataSpecific_23, _YLOC("Enable 'User - Edit Licenses' permission")).c_str()))
			JSON_END_PAIR,

			JSON_BEGIN_PAIR
                _YTEXT("licensePools"),
				JSON_BEGIN_ARRAY
                JSON_END_ARRAY
            JSON_END_PAIR,
		JSON_END_OBJECT;
}

bool DlgRoleEditDelegation::processPostedDataSpecific(const IPostedDataTarget::PostedData& data)
{
	std::map<wstring, std::pair<bool, int32_t>> skuLimits;

	m_Delegation.GetDelegation().ResetFlags();
	m_Delegation.MarkPrivilegesAsRemoved();
	for (const auto& item : data)
	{
		if (item.first == g_VarRoleAccess)
		{
			//m_Delegation.GetRoleAccess() = static_cast<RoleDelegationUtil::RoleAccess>(Str::getLongFromString(item.second));

			auto privID		= static_cast<RoleDelegationUtil::RBAC_Privilege>(Str::getLongFromString(item.second));
			auto& privilege = m_Delegation.GetPrivilege(privID);
			if (privilege.IsDummy())
			{
				RoleDelegationUtil::BusinessDelegationPrivilege newPrivilege;
				newPrivilege.m_Name			= _YFORMAT(L"For role: %s :: %s", m_Delegation.GetDelegation().m_Name.c_str(), RoleDelegationUtil::GetPrivilegeLabel(privID).c_str());
				newPrivilege.m_Info			= YtriaTranslate::Do(DlgRoleEditDelegation_processPostedDataSpecific_1, _YLOC("Role Delegation: %1"), m_Delegation.GetDelegation().m_Name.c_str());
				newPrivilege.m_Status		= RoleDelegationUtil::STATUS_OK;
				newPrivilege.m_IDDelegation = m_Delegation.GetID();
				newPrivilege.m_Privilege	= privID;
				m_Delegation.AddPrivilege(newPrivilege);
			}
			else
				privilege.m_Status = RoleDelegationUtil::STATUS_OK;
		}
		else if (item.first == g_VarRoleKey)
		{
			m_Delegation.GetDelegation().GetIDKey() = Str::getINT64FromString(item.second);
			m_Delegation.GetDelegation().m_Info		= RoleDelegationManager::GetInstance().GetKey(m_Delegation.GetDelegation().GetIDKey(), m_Session).m_Name;// for user activity logs
		}
		else if (item.first == g_VarFlagsSectionFlags)
		{
			if (item.second == g_VarFlagsLogSessionLogin)
				m_Delegation.GetDelegation().SetLogSessionLogin();
			else if (item.second == g_VarFlagsLogModuleOpening)
				m_Delegation.GetDelegation().SetLogModuleOpening();
			else if (item.second == g_VarFlagsEnforceRole)
				m_Delegation.GetDelegation().SetEnforceRole();
		}
		else
		{
			auto pos = item.first.find(g_SkuCheckboxNamePrefix, 0);
			if (std::wstring::npos != pos)
			{
				auto skuPartNumber = item.first.substr(pos + g_SkuCheckboxNamePrefix.size());
				skuLimits[skuPartNumber].first = !item.second.empty();
			}
			else
			{
				auto pos = item.first.find(g_SkuValueNamePrefix, 0);
				if (std::wstring::npos != pos)
				{
					auto skuPartNumber = item.first.substr(pos + g_SkuValueNamePrefix.size());
					skuLimits[skuPartNumber].second = Str::getIntFromString(item.second);
				}
			}
		}
	}

	const auto& ext = GetHtmlView()->GetExternal();
	ASSERT(ext);
	if (ext)
	{
		auto credExt = dynamic_cast<CredentialAdderCaller*>(ext.get());
		ASSERT(nullptr != credExt);
		if (nullptr != credExt)
		{
			for (auto& sku : credExt->GetSskus())
			{
				auto isinlist = skuLimits.find(wstring(*sku.GetSkuPartNumber()));
				// if sku has not been provided anymore: it has to be removed
				if (skuLimits.end() == isinlist)
					skuLimits[wstring(*sku.GetSkuPartNumber())].first = false;
			}
		}
	}

	for (const auto& val : skuLimits)
		m_Delegation.GetDelegation().SetSkuAccessLimit(val.first, val.second.first ? val.second.second : -1);

    return true;
}

wstring DlgRoleEditDelegation::getDialogTitle() const
{
	return YtriaTranslate::Do(DlgRoleEditDelegation_getDialogTitle_1, _YLOC("Role-Based Access Setup")).c_str();
}
