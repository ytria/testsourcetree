#include "RoleDelegationSQLite.h"

#include "ActivityLogger.h"
#include "DataCollectionSqlQueryRowHandler.h"
#include "Mainframe.h"
#include "MainFrameSapio365Session.h"
#include "MsGraphFieldNames.h"
#include "RoleDelegationUtil.h"
#include "SQLCosmosSyncManager.h"
#include "SqlQuerySelect.h"
#include "Str.h"
#include "TimeUtil.h"
#include "TraceIntoFile.h"

RoleDelegationSQLite::RoleDelegationSQLite()
	: O365SQLiteEngine(_YTEXTFORMAT(L"_rd%s_.ytr"
					, Str::getStringFromNumber<uint32_t>(g_CurrentVersion).c_str() 
					// DEBUG!
					//, SQLiteUtil::g_DumpEncryptionKey
					//,"c62f005d-9fa6-46be-a579-bc5b2fedfb47"
	)
)
{
	// Update this assert when version change.
	// For old reasons, g_CurrentVersion must at least be incremented each time SQLiteEngine::GetCurrentVersion() is.
	static_assert(g_CurrentVersion == 6 && SQLiteEngine::GetCurrentVersion() == 6, "Should you increment version or test?");

	m_Keys.m_Name					= _YTEXT("Keys");
	m_Filters.m_Name				= _YTEXT("Filters");
	m_DelegationFilters.m_Name		= _YTEXT("DelegationFilters");
	m_DelegationMembers.m_Name		= _YTEXT("DelegationMembers");
	m_DelegationPrivileges.m_Name	= _YTEXT("DelegationPrivileges");
	m_Delegations.m_Name			= _YTEXT("Delegation");
	m_sapio365Admin.m_Name			= _YTEXT("sapio365Admin");

	// m_NameForDisplay appears in user activity logs
	m_Keys.m_NameForDisplay					= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_1, _YLOC("Credential")).c_str();
	m_Filters.m_NameForDisplay				= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_2, _YLOC("Scope")).c_str();
	m_DelegationFilters.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_3, _YLOC("Role Scope")).c_str();
	m_DelegationMembers.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_4, _YLOC("Role User")).c_str();
	m_DelegationPrivileges.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_5, _YLOC("Role Permission")).c_str();
	m_Delegations.m_NameForDisplay			= YtriaTranslate::Do(GridDriveItems_customizeGrid_58, _YLOC("Role")).c_str();
	m_sapio365Admin.m_NameForDisplay		= _T("sapio365 Admin Roles");

	m_Keys.SetDefaultColumnFlags(SQLiteUtil::ColumnFlags::NOTNULL);
	m_Filters.SetDefaultColumnFlags(SQLiteUtil::ColumnFlags::NOTNULL);
	m_DelegationFilters.SetDefaultColumnFlags(SQLiteUtil::ColumnFlags::NOTNULL);
	m_DelegationMembers.SetDefaultColumnFlags(SQLiteUtil::ColumnFlags::NOTNULL);
	m_DelegationPrivileges.SetDefaultColumnFlags(SQLiteUtil::ColumnFlags::NOTNULL);
	m_Delegations.SetDefaultColumnFlags(SQLiteUtil::ColumnFlags::NOTNULL);
	m_sapio365Admin.SetDefaultColumnFlags(SQLiteUtil::ColumnFlags::NOTNULL);

	// Keys
	{
		addDefaultColumns(m_Keys);

		SQLiteUtil::Column cAppID;// ultra admin application ID
		cAppID.m_Name			= RoleDelegationUtil::g_ColumnNameAppID;
		cAppID.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_7, _YLOC("Utra Admin Application ID")).c_str();
		cAppID.m_Flags			= SQLiteUtil::ENCRYPT;
		m_Keys.AddColumn(cAppID);

		SQLiteUtil::Column cAppPWD;// ultra admin pwd
		cAppPWD.m_Name			 = RoleDelegationUtil::g_ColumnNameAppPWD;
		cAppPWD.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_8, _YLOC("UltraAdminApplicationSecretKey")).c_str(); // Not shown
		cAppPWD.m_Flags			 = SQLiteUtil::ENCRYPT | SQLiteUtil::NOTINGRID;
		m_Keys.AddColumn(cAppPWD);

		SQLiteUtil::Column cLogin;// admin account
		cLogin.m_Name			= RoleDelegationUtil::g_ColumnNameLogin;
		cLogin.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_9, _YLOC("Global Admin Username")).c_str();
		cLogin.m_Flags			= SQLiteUtil::ENCRYPT;
		m_Keys.AddColumn(cLogin);

		SQLiteUtil::Column cPWD;// admin pwd
		cPWD.m_Name				= RoleDelegationUtil::g_ColumnNamePWD;
		cPWD.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_10, _YLOC("GlobalAdminPassword")).c_str(); // Not shown
		cPWD.m_Flags			= SQLiteUtil::ENCRYPT | SQLiteUtil::NOTINGRID;
		m_Keys.AddColumn(cPWD);

		SQLiteUtil::Column cTargetTenantName;// tenant name for authentification of Ultra Admin session
		cTargetTenantName.m_Name			= RoleDelegationUtil::g_ColumnNameTargetTenant;
		cTargetTenantName.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_11, _YLOC("Target Tenant")).c_str();
		cTargetTenantName.m_Flags			= SQLiteUtil::ENCRYPT;
		m_Keys.AddColumn(cTargetTenantName);
	}

	// Filters
	{
		addDefaultColumns(m_Filters);

		SQLiteUtil::Column cProperty;
		cProperty.m_Name			= RoleDelegationUtil::g_ColumnNameProperty;
		cProperty.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_12, _YLOC("For Property")).c_str();
		m_Filters.AddColumn(cProperty);

		SQLiteUtil::Column cValue;
		cValue.m_Name			= RoleDelegationUtil::g_ColumnNameValue;
		cValue.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_13, _YLOC("With Value")).c_str();
		cValue.m_Flags			= SQLiteUtil::ENCRYPT;
		m_Filters.AddColumn(cValue);

		SQLiteUtil::Column cObjectType;
		cObjectType.m_Name			 = RoleDelegationUtil::g_ColumnNameObjectType;
		cObjectType.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_14, _YLOC("Scope Target")).c_str();
		m_Filters.AddColumn(cObjectType);

		SQLiteUtil::Column cTaggerMatchMethod;
		cTaggerMatchMethod.m_Name			= RoleDelegationUtil::g_ColumnNameTaggerMatchMethod;
		cTaggerMatchMethod.m_DataType		= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cTaggerMatchMethod.m_NameForDisplay	= YtriaTranslate::Do(GLOBAL_BEGIN_MESSAGE_MAP_1, _YLOC("Condition")).c_str();
		m_Filters.AddColumn(cTaggerMatchMethod);
	}

	// Delegation
	{
		addDefaultColumns(m_Delegations);

		SQLiteUtil::Column cIDKey;
		cIDKey.m_Name			= RoleDelegationUtil::g_ColumnNameIDKey;
		cIDKey.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_16, _YLOC("CredentialsID")).c_str(); // Not shown
		cIDKey.m_DataType		= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cIDKey.m_Flags			= SQLiteUtil::NOTINGRID;
		m_Delegations.AddColumn(cIDKey);

		SQLiteUtil::Column cLease;
		cLease.m_Name			= RoleDelegationUtil::g_ColumnNameLease;// minutes until local cache (this) is refreshed from cloud repository
		cLease.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_17, _YLOC("Lease")).c_str(); // Not shown
		cLease.m_DataType		= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cLease.m_Flags			= SQLiteUtil::NOTINGRID;
		m_Delegations.AddColumn(cLease);

		SQLiteUtil::Column cSkuLimits;
		cSkuLimits.m_Name			= RoleDelegationUtil::g_ColumnNameSkuLimits;
		cSkuLimits.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_18, _YLOC("SKULimits")).c_str();
		cSkuLimits.m_Flags			= SQLiteUtil::NOTINGRID;
		m_Delegations.AddColumn(cSkuLimits);

		SQLiteUtil::Column cFlags;
		cFlags.m_Name			= RoleDelegationUtil::g_ColumnNameFlags;
		cFlags.m_NameForDisplay	= _T("Flags");
		cFlags.m_DataType		= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cFlags.m_Flags			= SQLiteUtil::NOTINGRID;
		cFlags.m_DefaultValue	= _YTEXT("0");
		m_Delegations.AddColumn(cFlags);
	}

	// DelegationRights
	{
		addDefaultColumns(m_DelegationPrivileges);

		SQLiteUtil::Column cDelegationID;
		cDelegationID.m_Name				= RoleDelegationUtil::g_ColumnNameIDDelegation;
		cDelegationID.m_NameForDisplay		= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_19, _YLOC("RoleID")).c_str(); // Not shown
		cDelegationID.m_DataType			= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cDelegationID.m_ForeignKeyTable		= m_Delegations.GetSQLcompliantName();
		cDelegationID.m_ForeignKeyColumn	= m_Delegations.GetColumnROWID().GetSQLcompliantName();
		m_DelegationPrivileges.AddColumn(cDelegationID);

		SQLiteUtil::Column cPrivilege;
		cPrivilege.m_Name			= RoleDelegationUtil::g_ColumnNamePrivilege;// cf enum RBAC_Right
		cPrivilege.m_NameForDisplay = YtriaTranslate::Do(FrameDriveItems_customizeActionsRibbonTab_1, _YLOC("Permissions")).c_str(); // Not shown
		cPrivilege.m_DataType		= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		m_DelegationPrivileges.AddColumn(cPrivilege);
	}
	
	// DelegationFilters
	{
		addDefaultColumns(m_DelegationFilters);

		SQLiteUtil::Column cDelegationID;
		cDelegationID.m_Name				= RoleDelegationUtil::g_ColumnNameIDDelegation;
		cDelegationID.m_NameForDisplay		= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_19, _YLOC("RoleID")).c_str(); // Not shown
		cDelegationID.m_DataType			= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cDelegationID.m_ForeignKeyTable		= m_Delegations.GetSQLcompliantName();
		cDelegationID.m_ForeignKeyColumn	= m_Delegations.GetColumnROWID().GetSQLcompliantName();
		m_DelegationFilters.AddColumn(cDelegationID);

		SQLiteUtil::Column cFilterID;
		cFilterID.m_Name				= RoleDelegationUtil::g_ColumnNameIDFilter;
		cFilterID.m_NameForDisplay		= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_22, _YLOC("RoleFilterID")).c_str(); // Not shown
		cFilterID.m_DataType			= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cFilterID.m_ForeignKeyTable		= m_Filters.GetSQLcompliantName();
		cFilterID.m_ForeignKeyColumn	= m_Filters.GetColumnROWID().GetSQLcompliantName();
		m_DelegationFilters.AddColumn(cFilterID);
	}

	// DelegationMembers
	{
		addDefaultColumns(m_DelegationMembers);

		SQLiteUtil::Column cDelegationID;
		cDelegationID.m_Name				= RoleDelegationUtil::g_ColumnNameIDDelegation;
		cDelegationID.m_NameForDisplay		= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_23, _YLOC("RoleUserID")).c_str(); // Not shown
		cDelegationID.m_DataType			= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
		cDelegationID.m_ForeignKeyTable		= m_Delegations.GetSQLcompliantName();
		cDelegationID.m_ForeignKeyColumn	= m_Delegations.GetColumnROWID().GetSQLcompliantName();
		m_DelegationMembers.AddColumn(cDelegationID);

		SQLiteUtil::Column cUserID;
		cUserID.m_Name			 = RoleDelegationUtil::g_ColumnNameUserID;// graph ID
		cUserID.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_24, _YLOC("Assigned User ID")).c_str();
		m_DelegationMembers.AddColumn(cUserID);

		SQLiteUtil::Column cUserPrincipaleName;
		cUserPrincipaleName.m_Name			 = RoleDelegationUtil::g_ColumnNameUserPrincipalName;
		cUserPrincipaleName.m_Flags			 = SQLiteUtil::ENCRYPT;
		cUserPrincipaleName.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_25, _YLOC("Assigned User Principal Name")).c_str();
		m_DelegationMembers.AddColumn(cUserPrincipaleName);

		SQLiteUtil::Column cUserName;
		cUserName.m_Name		   = RoleDelegationUtil::g_ColumnNameUserName;
		cUserName.m_Flags		   = SQLiteUtil::ENCRYPT;
		cUserName.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_26, _YLOC("Assigned User Name")).c_str();
		m_DelegationMembers.AddColumn(cUserName);
	}

	// Admin
	{
		addDefaultColumns(m_sapio365Admin);

		SQLiteUtil::Column cUserID;
		cUserID.m_Name			 = RoleDelegationUtil::g_ColumnNameUserID;// graph ID
		cUserID.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_24, _YLOC("Assigned User ID")).c_str();
		m_sapio365Admin.AddColumn(cUserID);

		SQLiteUtil::Column cUserPrincipaleName;
		cUserPrincipaleName.m_Name			 = RoleDelegationUtil::g_ColumnNameUserPrincipalName;
		cUserPrincipaleName.m_Flags			 = SQLiteUtil::ENCRYPT;
		cUserPrincipaleName.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_25, _YLOC("Assigned User Principal Name")).c_str();
		m_sapio365Admin.AddColumn(cUserPrincipaleName);

		SQLiteUtil::Column cUserName;
		cUserName.m_Name		   = RoleDelegationUtil::g_ColumnNameUserName;
		cUserName.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_26, _YLOC("Assigned User Name")).c_str();
		m_sapio365Admin.AddColumn(cUserName);

		SQLiteUtil::Column cAdminScope;
		cAdminScope.m_Name				= RoleDelegationUtil::g_ColumnNameAdminScope;
		cAdminScope.m_DataType			= SQLiteUtil::DataType::SQL_TYPE_INTEGER;// RBAC_Admin
		cAdminScope.m_NameForDisplay	= _T("Admin Scope");
		m_sapio365Admin.AddColumn(cAdminScope);
	}
}

void RoleDelegationSQLite::addDefaultColumns(SQLiteUtil::Table& p_Table) const
{
	p_Table.AddColumnRowID();

	SQLiteUtil::Column cName;
	cName.m_Name		   = RoleDelegationUtil::g_ColumnNameName;
	cName.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_1, _YLOC("Reference Name")).c_str();
	p_Table.AddColumn(cName);

	SQLiteUtil::Column cStatus;
	cStatus.m_Name		= RoleDelegationUtil::g_ColumnNameStatus;
	cStatus.m_DataType	= SQLiteUtil::DataType::SQL_TYPE_INTEGER;
	cStatus.m_Flags		= SQLiteUtil::NOTINGRID;
	p_Table.AddColumn(cStatus);

	SQLiteUtil::Column cInfo;
	cInfo.m_Name			= RoleDelegationUtil::g_ColumnNameInfo;
	cInfo.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_2, _YLOC("Short Description")).c_str();
	p_Table.AddColumn(cInfo);

	SQLiteUtil::Column cDate;
	cDate.m_Name			= RoleDelegationUtil::g_ColumnNameDate;
	cDate.m_NameForDisplay	= YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_3, _YLOC("Last Updated On")).c_str();
	cDate.m_Flags			= SQLiteUtil::ColumnFlags::ISDATESYNC;
	p_Table.AddColumn(cDate);

	SQLiteUtil::Column cUserUPDID;
	cUserUPDID.m_Name			= RoleDelegationUtil::g_ColumnNameUpdID;
	cUserUPDID.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_4, _YLOC("Last Updated By (ID)")).c_str();
	cUserUPDID.m_Flags			= SQLiteUtil::ENCRYPT;// having at least one encrypted column per table makes all data tenant-dependent (data that cannot be decrypted with the tenant ID is not loaded into the application)
	p_Table.AddColumn(cUserUPDID);

	SQLiteUtil::Column cUserUPDName;
	cUserUPDName.m_Name			  = RoleDelegationUtil::g_ColumnNameUpdName;
	cUserUPDName.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_5, _YLOC("Last Updated By (Name)")).c_str();
	p_Table.AddColumn(cUserUPDName);

	SQLiteUtil::Column cUserUPDMail;
	cUserUPDMail.m_Name			  = RoleDelegationUtil::g_ColumnNameUpdEmail;
	cUserUPDMail.m_NameForDisplay = YtriaTranslate::Do(RoleDelegationSQLite_addDefaultColumns_6, _YLOC("Last Updated By (email)")).c_str();
	p_Table.AddColumn(cUserUPDMail);

	SQLiteUtil::Column cTenant;
	cTenant.m_Name	= RoleDelegationUtil::g_ColumnNameTenant;
	cTenant.m_Flags	= SQLiteUtil::NOTINGRID | SQLiteUtil::ISORGANIZATION;
	p_Table.AddColumn(cTenant);
}

vector<SQLiteUtil::Table> RoleDelegationSQLite::GetTables() const
{
	return { m_Keys, m_Filters, m_Delegations , m_DelegationFilters, m_DelegationMembers, m_DelegationPrivileges, m_sapio365Admin };
}

RoleDelegation RoleDelegationSQLite::LoadRoleDelegation(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	const YTimeDate now = YTimeDate::GetCurrentTimeDateGMT();

	RoleDelegation rd(now);

	ASSERT(p_ID != 0);

	auto delegation = ReadDelegation(p_ID, p_Session);
	if (delegation.HasID())
	{
		rd.SetDelegation(delegation);
	
		auto key = ReadKey(rd.GetDelegation().GetIDKey(), p_Session);
		if (key.HasID())
			rd.SetKey(key);

		// rights
		const auto delegationRights = ReadDelegationPrivileges(rd.GetID(), p_Session);
		for (const auto& dr : delegationRights)
			rd.AddPrivilege(dr);

		// filters
		const auto delegationFilters = ReadDelegationFilters(rd.GetID(), p_Session);
		for (const auto& df : delegationFilters)
			rd.AddFilter(ReadFilter(df.m_DelegationFilter.m_IDFilter, p_Session));

		// members
		const auto delegationMembers = ReadDelegationMembers(rd.GetID(), p_Session);
		for (const auto& du : delegationMembers)
			rd.AddMember(du);
	}

	return rd;
}

wstring RoleDelegationSQLite::GetSQLSelectStatementDelegationsForThisUser(const PooledString& p_UserID)
{	
	const auto cDID			= m_Delegations.GetColumnROWID().GetSQLcompliantName();
	const auto cDMDID		= m_DelegationMembers.GetColumn(RoleDelegationUtil::g_ColumnNameIDDelegation).GetSQLcompliantName();
	const auto cDMUID		= m_DelegationMembers.GetColumn(RoleDelegationUtil::g_ColumnNameUserID).GetSQLcompliantName();
	const auto cDStatus		= m_Delegations.GetColumn(RoleDelegationUtil::g_ColumnNameStatus).GetSQLcompliantName();
	const auto cDMStatus	= m_DelegationMembers.GetColumn(RoleDelegationUtil::g_ColumnNameStatus).GetSQLcompliantName();

	const auto tableDelegations			= m_Delegations.GetSQLcompliantName();
	const auto tableDelegationMembers	= m_DelegationMembers.GetSQLcompliantName();

	const auto statusRemoved = Str::getStringFromNumber<uint32_t>(RoleDelegationUtil::STATUS_REMOVED);

	CString sql;
	// select * from Delegation D join DelegationMembers DM ON DM.DelegationID = D.SAPIO365ROWID where DM.UserID = 'xyz' and D.Status <> 1 and DM.status <> 1
	sql.Format(_YTEXT("select * from %s D join %s DM on DM.%s = D.%s where DM.%s = '%s' and D.%s <> %s and DM.%s <> %s"), 
												tableDelegations.c_str(), 
												tableDelegationMembers.c_str(),
												cDMDID.c_str(),
												cDID.c_str(),
												cDMUID.c_str(),
												p_UserID.c_str(),
												cDStatus.c_str(),
												statusRemoved.c_str(),
												cDMStatus.c_str(),
												statusRemoved.c_str());
	return sql.GetBuffer(); 
}

bool RoleDelegationSQLite::LoadRoleDelegations(const PooledString& p_UserID, vector<RoleDelegation>& p_Delegations, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	p_Delegations.clear();

	if (!p_UserID.IsEmpty())
	{
		rvSuccess = true;

		const auto sqlStatement = GetSQLSelectStatementDelegationsForThisUser(p_UserID);

		SQLiteUtil::QueryResultDataCollection SQLdata;
		DataCollectionSqlQueryRowHandler rowHandler(*this, O365Krypter(p_Session), m_Delegations, SQLdata);
		SqlQuerySelect query(*this, rowHandler, sqlStatement);
		query.Run();

		if (query.GetStatus() == SQLITE_OK)
		{
			for (const auto& sqlResult : SQLdata)
			{
				RoleDelegationUtil::Delegation SQLrd(m_Delegations);
				if (SQLrd.FromValuesToObject(sqlResult))
				{
					RoleDelegation rd = LoadRoleDelegation(SQLrd.GetID(), p_Session);
					if (rd.IsLoaded())
						p_Delegations.push_back(rd);
				}
			}
		}
	}

	return rvSuccess;
}

RoleDelegationUtil::Key RoleDelegationSQLite::ReadKey(const int64_t p_KeyID, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::Key theKey(m_Keys);// https://www.youtube.com/watch?v=uGg11znHPA8
	if (Select(O365Krypter(p_Session), &theKey, p_KeyID, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED))
		theKey.m_Key.ReadPostProcess(theKey);
	return theKey;
}

int64_t RoleDelegationSQLite::CountKeys(std::shared_ptr<const Sapio365Session> p_Session)
{
	int64_t ckeys = 0;

	auto ck = SelectCount(O365Krypter(p_Session), m_Keys, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED);
	if (ck)
		ckeys = ck.get();

	return ckeys;
}

vector<RoleDelegationUtil::Key>	RoleDelegationSQLite::ReadKeys(std::shared_ptr<const Sapio365Session> p_Session)
{
	vector<RoleDelegationUtil::Key> keys;
	SelectAll<RoleDelegationUtil::Key>(O365Krypter(p_Session), m_Keys, wstring(), 0, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED, keys);
	for (auto& k : keys)
		k.m_Key.ReadPostProcess(k);// sad but true
	return keys;
}

RoleDelegationUtil::Delegation RoleDelegationSQLite::ReadDelegation(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::Delegation delegation(m_Delegations);
	if (Select(O365Krypter(p_Session), &delegation, p_ID, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED))
		delegation.m_Delegation.ReadPostProcess(delegation);// sad but true
	return delegation;
}

vector<RoleDelegationUtil::Delegation> RoleDelegationSQLite::ReadDelegationsFeaturingThisKey(const int64_t p_KeyID, std::shared_ptr<const Sapio365Session> p_Session)
{
	vector<RoleDelegationUtil::Delegation> delegations;
	SelectAll<RoleDelegationUtil::Delegation>(O365Krypter(p_Session), m_Delegations, RoleDelegationUtil::g_ColumnNameIDKey, p_KeyID, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED, delegations);
	for (auto& d : delegations)
		d.m_Delegation.ReadPostProcess(d);// sad but true
	return delegations;
}

vector<RoleDelegationUtil::Delegation> RoleDelegationSQLite::ReadDelegationsFeaturingThisFilter(const int64_t p_FilterID, std::shared_ptr<const Sapio365Session> p_Session)
{
	vector<RoleDelegationUtil::Delegation> delegations;

	{
		const auto cDID			= m_Delegations.GetColumnROWID().GetSQLcompliantName();
		const auto cDFDID		= m_DelegationFilters.GetColumn(RoleDelegationUtil::g_ColumnNameIDDelegation).GetSQLcompliantName();
		const auto cDFFID		= m_DelegationFilters.GetColumn(RoleDelegationUtil::g_ColumnNameIDFilter).GetSQLcompliantName();
		const auto cDStatus		= m_Delegations.GetColumn(RoleDelegationUtil::g_ColumnNameStatus).GetSQLcompliantName();
		const auto cDFStatus	= m_DelegationFilters.GetColumn(RoleDelegationUtil::g_ColumnNameStatus).GetSQLcompliantName();

		const auto tableDelegations			= m_Delegations.GetSQLcompliantName();
		const auto tableDelegationFilters	= m_DelegationFilters.GetSQLcompliantName();

		const auto statusRemoved	= Str::getStringFromNumber<uint32_t>(RoleDelegationUtil::STATUS_REMOVED);
		const auto filterId			= Str::getStringFromNumber(p_FilterID);

		CString sql;
		// select * from Delegation D join DelegationFilters DM ON DM.DelegationID = D.SAPIO365ROWID where DM.FilterID = 'xyz' and D.Status <> 1 and DM.status <> 1
		sql.Format(_YTEXT("select * from %s D join %s DM on DM.%s = D.%s where DM.%s = '%s' and D.%s <> %s and DM.%s <> %s"),
			tableDelegations.c_str(),
			tableDelegationFilters.c_str(),
			cDFDID.c_str(),
			cDID.c_str(),
			cDFFID.c_str(),
			filterId.c_str(),
			cDStatus.c_str(),
			statusRemoved.c_str(),
			cDFStatus.c_str(),
			statusRemoved.c_str());

		SQLiteUtil::QueryResultDataCollection SQLdata;
		DataCollectionSqlQueryRowHandler rowHandler(*this, O365Krypter(p_Session), m_Delegations, SQLdata);
		SqlQuerySelect query(*this, rowHandler, wstring(sql));
		query.Run();

		if (query.GetStatus() == SQLITE_OK)
		{
			for (const auto& sqlResult : SQLdata)
			{
				RoleDelegationUtil::Delegation obj(m_Delegations);
				if (obj.FromValuesToObject(sqlResult))
					delegations.emplace_back(obj);
			}
		}

		for (auto& d : delegations)
			d.m_Delegation.ReadPostProcess(d);// sad but true
	}

	return delegations;
}

vector<RoleDelegationUtil::DelegationPrivilege>	RoleDelegationSQLite::ReadDelegationPrivileges(const int64_t p_DelegationID, std::shared_ptr<const Sapio365Session> p_Session)
{
	vector<RoleDelegationUtil::DelegationPrivilege> rolePrivileges;
	SelectAll<RoleDelegationUtil::DelegationPrivilege>(O365Krypter(p_Session), m_DelegationPrivileges, RoleDelegationUtil::g_ColumnNameIDDelegation, p_DelegationID, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED, rolePrivileges);
	for (auto& rp : rolePrivileges)
		rp.m_DelegationPrivilege.ReadPostProcess(rp);// sad but true
	return rolePrivileges;
}

vector<RoleDelegationUtil::DelegationFilter> RoleDelegationSQLite::ReadDelegationFilters(const int64_t p_DelegationID, std::shared_ptr<const Sapio365Session> p_Session)
{
	vector<RoleDelegationUtil::DelegationFilter> roleFilters;
	SelectAll<RoleDelegationUtil::DelegationFilter>(O365Krypter(p_Session), m_DelegationFilters, RoleDelegationUtil::g_ColumnNameIDDelegation, p_DelegationID, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED, roleFilters);
	for (auto& rf : roleFilters)
		rf.m_DelegationFilter.ReadPostProcess(rf);// sad but true
	return roleFilters;
}

vector<RoleDelegationUtil::DelegationMember> RoleDelegationSQLite::ReadDelegationMembers(const int64_t p_DelegationID, std::shared_ptr<const Sapio365Session> p_Session)
{
	vector<RoleDelegationUtil::DelegationMember> roleUsers;
	SelectAll<RoleDelegationUtil::DelegationMember>(O365Krypter(p_Session), m_DelegationMembers, RoleDelegationUtil::g_ColumnNameIDDelegation, p_DelegationID, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED, roleUsers);
	for (auto& ru : roleUsers)
		ru.m_DelegationMember.ReadPostProcess(ru);// sad but true
	return roleUsers;
}

vector<RoleDelegationUtil::Filter> RoleDelegationSQLite::ReadFilters(std::shared_ptr<const Sapio365Session> p_Session)
{
	vector<RoleDelegationUtil::Filter> roleFilters;
	SelectAll<RoleDelegationUtil::Filter>(O365Krypter(p_Session), m_Filters, wstring(), 0, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED, roleFilters);
	for (auto& rf : roleFilters)
		rf.m_Filter.ReadPostProcess(rf);// sad but true
	return roleFilters;
}

RoleDelegationUtil::Filter RoleDelegationSQLite::ReadFilter(const int64_t p_FilterID, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::Filter filter(m_Filters);
	if (Select(O365Krypter(p_Session), &filter, p_FilterID, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED))
		filter.m_Filter.ReadPostProcess(filter);// sad but true
	return filter;
}

void RoleDelegationSQLite::readForUpdateToCloudKeys(std::shared_ptr<const Sapio365Session> p_Session)
{
	SelectAll<RoleDelegationUtil::Key>(O365Krypter(p_Session), m_Keys, SQLCosmosSyncManager::GetInstance().GetLastSyncDate(GetDBfilename(), m_Keys.GetSQLcompliantName(), p_Session), m_ForCloudKeys);
	for (auto& k : m_ForCloudKeys)
	{
		k.m_Key.ReadPostProcess(k);// sad but true
		k.SetForCloudStorageOnly();
		WriteRoleDelegationRecord(&k, p_Session);// will only prepare values for Cosmos because of SetForCloudStorageOnly
	}
}

void RoleDelegationSQLite::readForUpdateToCloudFilters(std::shared_ptr<const Sapio365Session> p_Session)
{
	SelectAll<RoleDelegationUtil::Filter>(O365Krypter(p_Session), m_Filters, SQLCosmosSyncManager::GetInstance().GetLastSyncDate(GetDBfilename(), m_Filters.GetSQLcompliantName(), p_Session), m_ForCloudFitlers);
	for (auto& rf : m_ForCloudFitlers)
	{
		rf.m_Filter.ReadPostProcess(rf);// sad but true
		rf.SetForCloudStorageOnly();
		WriteRoleDelegationRecord(&rf, p_Session);// will only prepare values for Cosmos because of SetForCloudStorageOnly
	}
}

void RoleDelegationSQLite::readForUpdateToCloudDelegationFilters(std::shared_ptr<const Sapio365Session> p_Session)
{
	SelectAll<RoleDelegationUtil::DelegationFilter>(O365Krypter(p_Session), m_DelegationFilters, SQLCosmosSyncManager::GetInstance().GetLastSyncDate(GetDBfilename(), m_DelegationFilters.GetSQLcompliantName(), p_Session), m_ForCloudDelegationFilters);
	for (auto& f : m_ForCloudDelegationFilters)
	{
		f.m_DelegationFilter.ReadPostProcess(f);// sad but true
		f.SetForCloudStorageOnly();
		WriteRoleDelegationRecord(&f, p_Session);// will only prepare values for Cosmos because of SetForCloudStorageOnly
	}
}

void RoleDelegationSQLite::readForUpdateToCloudDelegationMembers(std::shared_ptr<const Sapio365Session> p_Session)
{
	SelectAll<RoleDelegationUtil::DelegationMember>(O365Krypter(p_Session), m_DelegationMembers,  SQLCosmosSyncManager::GetInstance().GetLastSyncDate(GetDBfilename(), m_DelegationMembers.GetSQLcompliantName(), p_Session), m_ForCloudDelegationMembers);
	for (auto& m : m_ForCloudDelegationMembers)
	{
		m.m_DelegationMember.ReadPostProcess(m);// sad but true
		m.SetForCloudStorageOnly();
		WriteRoleDelegationRecord(&m, p_Session);// will only prepare values for Cosmos because of SetForCloudStorageOnly
	}
}

void RoleDelegationSQLite::readForUpdateToCloudDelegationPrivileges(std::shared_ptr<const Sapio365Session> p_Session)
{
	SelectAll<RoleDelegationUtil::DelegationPrivilege>(O365Krypter(p_Session), m_DelegationPrivileges, SQLCosmosSyncManager::GetInstance().GetLastSyncDate(GetDBfilename(), m_DelegationPrivileges.GetSQLcompliantName(), p_Session),  m_ForCloudDelegationPrivileges);
	for (auto& p : m_ForCloudDelegationPrivileges)
	{
		p.m_DelegationPrivilege.ReadPostProcess(p);// sad but true
		p.SetForCloudStorageOnly();
		WriteRoleDelegationRecord(&p, p_Session);// will only prepare values for Cosmos because of SetForCloudStorageOnly
	}
}

void RoleDelegationSQLite::readForUpdateToCloudDelegations(std::shared_ptr<const Sapio365Session> p_Session)
{
	SelectAll<RoleDelegationUtil::Delegation>(O365Krypter(p_Session), m_Delegations, SQLCosmosSyncManager::GetInstance().GetLastSyncDate(GetDBfilename(), m_Delegations.GetSQLcompliantName(), p_Session), m_ForCloudDelegations);
	for (auto& d : m_ForCloudDelegations)
	{
		d.m_Delegation.ReadPostProcess(d);// sad but true
		d.SetForCloudStorageOnly();
		WriteRoleDelegationRecord(&d, p_Session);// will only prepare values for Cosmos because of SetForCloudStorageOnly
	}
}

void RoleDelegationSQLite::readForUpdateToCloudsapio365Admin(std::shared_ptr<const Sapio365Session> p_Session)
{
	SelectAll<RoleDelegationUtil::sapio365Admin>(O365Krypter(p_Session), m_sapio365Admin, SQLCosmosSyncManager::GetInstance().GetLastSyncDate(GetDBfilename(), m_sapio365Admin.GetSQLcompliantName(), p_Session), m_ForCloudsapio365Admin);
	for (auto& a : m_ForCloudsapio365Admin)
	{
		a.m_sapio365Admin.ReadPostProcess(a);// sad but true
		a.SetForCloudStorageOnly();
		WriteRoleDelegationRecord(&a, p_Session);// will only prepare values for Cosmos because of SetForCloudStorageOnly
	}
}

RoleDelegationUtil::Key RoleDelegationSQLite::Write(const RoleDelegationUtil::BusinessKey& p_Key, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::Key theKey = p_Key.m_ID.is_initialized() ? ReadKey(p_Key.m_ID.get(), p_Session) : RoleDelegationUtil::Key(m_Keys);
	theKey.m_Key	= p_Key;
	theKey.SetID(p_Key.m_ID);// sad but true
	WriteRoleDelegationRecord(&theKey, p_Session);
	theKey.m_Key.m_ID = theKey.GetID();// sad but true again
	return theKey;
}

RoleDelegationUtil::Delegation RoleDelegationSQLite::Write(const RoleDelegationUtil::BusinessDelegation& p_Delegation, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::Delegation delegation = p_Delegation.m_ID.is_initialized() ? ReadDelegation(*p_Delegation.m_ID, p_Session) : RoleDelegationUtil::Delegation(m_Delegations);
	delegation.m_Delegation = p_Delegation;
	delegation.m_Delegation.SerializeSkuAccessLimits();
	delegation.SetID(p_Delegation.m_ID);// sad but true
	WriteRoleDelegationRecord(&delegation, p_Session);
	delegation.m_Delegation.m_ID = delegation.GetID();// sad but true again
	return delegation;
}

RoleDelegationUtil::Filter RoleDelegationSQLite::Write(const RoleDelegationUtil::BusinessFilter& p_Filter, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::Filter filter = p_Filter.m_ID.is_initialized() ? ReadFilter(*p_Filter.m_ID, p_Session) : RoleDelegationUtil::Filter(m_Filters);
	filter.m_Filter = p_Filter;
	filter.SetID(p_Filter.m_ID);// sad but true
	WriteRoleDelegationRecord(&filter, p_Session);
	filter.m_Filter.m_ID = filter.GetID();// sad but true again
	return filter;
}


RoleDelegationUtil::DelegationFilter RoleDelegationSQLite::Write(const RoleDelegationUtil::BusinessDelegationFilter& p_DelegationFilter, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::DelegationFilter df = p_DelegationFilter.m_ID.is_initialized() ? readDelegationFilter(*p_DelegationFilter.m_ID, p_Session) : RoleDelegationUtil::DelegationFilter(m_DelegationFilters);
	df.m_DelegationFilter = p_DelegationFilter;
	df.SetID(p_DelegationFilter.m_ID);// sad but true
	WriteRoleDelegationRecord(&df, p_Session);
	df.m_DelegationFilter.m_ID = df.GetID();// sad but true again
	return df;
}

RoleDelegationUtil::DelegationMember RoleDelegationSQLite::Write(const RoleDelegationUtil::BusinessDelegationMember& p_DelegationMember, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::DelegationMember dm = p_DelegationMember.m_ID.is_initialized() ? readDelegationMember(*p_DelegationMember.m_ID, p_Session) : RoleDelegationUtil::DelegationMember(m_DelegationMembers);
	dm.m_DelegationMember = p_DelegationMember;
	dm.SetID(p_DelegationMember.m_ID);// sad but true
	WriteRoleDelegationRecord(&dm, p_Session);
	dm.m_DelegationMember.m_ID = dm.GetID();// sad but true again
	return dm;
}

RoleDelegationUtil::DelegationPrivilege RoleDelegationSQLite::Write(const RoleDelegationUtil::BusinessDelegationPrivilege& p_DelegationPrivilege, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::DelegationPrivilege dp = p_DelegationPrivilege.m_ID.is_initialized() ? readDelegationPrivilege(*p_DelegationPrivilege.m_ID, p_Session) : RoleDelegationUtil::DelegationPrivilege(m_DelegationPrivileges);
	dp.m_DelegationPrivilege = p_DelegationPrivilege;
	dp.SetID(p_DelegationPrivilege.m_ID);// sad but true
	WriteRoleDelegationRecord(&dp, p_Session);
	dp.m_DelegationPrivilege.m_ID = dp.GetID();// sad but true again
	return dp;
}

RoleDelegationUtil::sapio365Admin RoleDelegationSQLite::Write(const RoleDelegationUtil::BusinessSapio365Admin& p_sapio365Admin, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::sapio365Admin a = p_sapio365Admin.m_ID.is_initialized() ? readsapio365Admin(*p_sapio365Admin.m_ID, p_Session) : RoleDelegationUtil::sapio365Admin(m_sapio365Admin);
	a.m_sapio365Admin = p_sapio365Admin;
	a.SetID(p_sapio365Admin.m_ID);// sad but true
	WriteRoleDelegationRecord(&a, p_Session);
	a.m_sapio365Admin.m_ID = a.GetID();// sad but true again
	return a;
}

RoleDelegationUtil::sapio365Admin RoleDelegationSQLite::readsapio365Admin(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::sapio365Admin a(m_sapio365Admin);
	if (Select(O365Krypter(p_Session), &a, p_ID, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED))
		a.m_sapio365Admin.ReadPostProcess(a);// sad but true
	return a;
}

RoleDelegationUtil::DelegationPrivilege	RoleDelegationSQLite::readDelegationPrivilege(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::DelegationPrivilege	dp(m_DelegationPrivileges);
	if (Select(O365Krypter(p_Session), &dp, p_ID, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED))
		dp.m_DelegationPrivilege.ReadPostProcess(dp);// sad but true
	return dp;
}

RoleDelegationUtil::DelegationFilter RoleDelegationSQLite::readDelegationFilter(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::DelegationFilter df(m_DelegationFilters);
	if (Select(O365Krypter(p_Session), &df, p_ID, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED))
		df.m_DelegationFilter.ReadPostProcess(df);// sad but true
	return df;
}

RoleDelegationUtil::DelegationMember RoleDelegationSQLite::readDelegationMember(const int64_t p_ID, std::shared_ptr<const Sapio365Session> p_Session)
{
	RoleDelegationUtil::DelegationMember dm(m_DelegationMembers);
	if (Select(O365Krypter(p_Session), &dm, p_ID, RoleDelegationUtil::g_ColumnNameStatus, RoleDelegationUtil::STATUS_REMOVED))
		dm.m_DelegationMember.ReadPostProcess(dm);// sad but true
	return dm;
}

bool RoleDelegationSQLite::prepareUpdateInfo(RoleDelegationUtil::BusinessData* p_Generic) const
{
	bool rvSuccess = false;

	ASSERT(nullptr != p_Generic);

	// update info: who, when
	// MainFrame connected user: only the MainFrame backstage should offer this functionality, otherwise pass a Sapio365Session
	std::shared_ptr<const Sapio365Session> session = MainFrameSapio365Session();
	ASSERT(session);
	if (session)
	{
		ASSERT(!session->IsUltraAdmin());
		if (!session->IsUltraAdmin())
		{
			const auto tenantName = session->GetTenantName(true);
			bool tenantMatch = true;
			if (p_Generic->m_TenantName.empty())
				p_Generic->m_TenantName = tenantName;
			else if (tenantName != p_Generic->m_TenantName)
			{
				ASSERT(false);
				TraceIntoFile::trace(_YDUMP("RoleDelegationSQLite"), _YDUMP("prepareUpdateInfo"), _YTEXTFORMAT(L"Trying to store data from tenant: %s - while active tenant is: %s", p_Generic->m_TenantName.c_str(), tenantName.c_str()));				
				tenantMatch = false;
			}
			
			if (tenantMatch)
			{
				// Sapio365SessionSavedInfo might do the trick too
				ASSERT(session->GetGraphCache().GetCachedConnectedUser().is_initialized());
				if (session->GetGraphCache().GetCachedConnectedUser().is_initialized())
				{
					const auto& user = session->GetGraphCache().GetCachedConnectedUser().get();
					ASSERT(user.GetDisplayName().is_initialized());
					ASSERT(user.GetUserPrincipalName().is_initialized());

					p_Generic->m_UpdDate				= YTimeDate::GetCurrentTimeDate();
					p_Generic->m_UpdateByID				= user.GetID().c_str();
					p_Generic->m_UpdateByName			= user.GetDisplayName().is_initialized() ? user.GetDisplayName().get() : _YTEXT("N/A");
					p_Generic->m_UpdateByPrincipalName	= user.GetUserPrincipalName().is_initialized() ? user.GetUserPrincipalName().get() : _YTEXT("N/A");

					rvSuccess = true;
				}
			}
		}
	}

	return rvSuccess;
}

bool RoleDelegationSQLite::WriteRoleDelegationRecord(RoleDelegationUtil::RoleDelegationRecord* p_RDrecord, std::shared_ptr<const Sapio365Session> p_Session)
{
	bool rvSuccess = false;

	ASSERT(nullptr != p_RDrecord);
	if (nullptr != p_RDrecord && (p_RDrecord->IsForCloudStorageOnly() || prepareUpdateInfo(p_RDrecord->GetGeneric())))
	{
		rvSuccess = WriteRecord(O365Krypter(p_Session), p_RDrecord);

		if (!p_RDrecord->IsForCloudStorageOnly())
			ActivityLogger::GetInstance().LogRBACModifications(RoleDelegationModification(p_RDrecord));
	}

	return rvSuccess;
}

SQLiteUtil::Table& RoleDelegationSQLite::GetTableKeys()
{
	return m_Keys;
}

SQLiteUtil::Table& RoleDelegationSQLite::GetTableFilters()
{
	return m_Filters;
}

SQLiteUtil::Table& RoleDelegationSQLite::GetTableDelegations()
{
	return m_Delegations;
}

SQLiteUtil::Table& RoleDelegationSQLite::GetTableAdmin()
{
	return m_sapio365Admin;
}

bool RoleDelegationSQLite::UpdateFromCosmos(const CosmosUtil::SQLupdateData& p_AllData, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Prog)
{
	bool rvSuccess = false;

	TraceIntoFile::trace(_YDUMP("RoleDelegationSQLite"), _YDUMP("UpdateFromCosmos"), _YTEXT("start"));

	size_t allSize = 0;
	for (const auto& p : p_AllData)
		allSize += p.second.size();
	ASSERT(allSize < (std::numeric_limits<DWORD>::max)());
	p_Prog.SetCounterTotal2(static_cast<DWORD>(allSize));

	// Achtung: process order is important because of foreign keys

	// delegations
	{
		auto findIt = p_AllData.find(m_Delegations);
		if (p_AllData.end() != findIt)
			rvSuccess = updateSQLfromCosmosDocuments<RoleDelegationUtil::Delegation>(m_Delegations, findIt->second, p_Session, p_Prog);
	}

	// keys
	{
		auto findIt = p_AllData.find(m_Keys);
		if (p_AllData.end() != findIt)
			rvSuccess = updateSQLfromCosmosDocuments<RoleDelegationUtil::Key>(m_Keys, findIt->second, p_Session, p_Prog);
	}

	// filters
	{
		auto findIt = p_AllData.find(m_Filters);
		if (p_AllData.end() != findIt)
			rvSuccess = updateSQLfromCosmosDocuments<RoleDelegationUtil::Filter>(m_Filters, findIt->second, p_Session, p_Prog);
	}

	// filter associations
	{
		auto findIt = p_AllData.find(m_DelegationFilters);
		if (p_AllData.end() != findIt)
			rvSuccess = updateSQLfromCosmosDocuments<RoleDelegationUtil::DelegationFilter>(m_DelegationFilters, findIt->second, p_Session, p_Prog);
	}

	// member associations
	{
		auto findIt = p_AllData.find(m_DelegationMembers);
		if (p_AllData.end() != findIt)
			rvSuccess = updateSQLfromCosmosDocuments<RoleDelegationUtil::DelegationMember>(m_DelegationMembers, findIt->second, p_Session, p_Prog);
	}

	// privileges associations
	{
		auto findIt = p_AllData.find(m_DelegationPrivileges);
		if (p_AllData.end() != findIt)
			rvSuccess = updateSQLfromCosmosDocuments<RoleDelegationUtil::DelegationPrivilege>(m_DelegationPrivileges, findIt->second, p_Session, p_Prog);
	}

	// sapio365Admin
	{
		auto findIt = p_AllData.find(m_sapio365Admin);
		if (p_AllData.end() != findIt)
			rvSuccess = updateSQLfromCosmosDocuments<RoleDelegationUtil::sapio365Admin>(m_sapio365Admin, findIt->second, p_Session, p_Prog);
	}

	TraceIntoFile::trace(_YDUMP("RoleDelegationSQLite"), _YDUMP("UpdateFromCosmos"), _YTEXT("end"));

	return rvSuccess;
}

bool RoleDelegationSQLite::SelectAllForUpdateToCloud(RoleDelegationUtil::CosmosUpdateData& p_AllData, std::shared_ptr<const Sapio365Session> p_Session)
{
	p_AllData.clear();

	// filter associations
	readForUpdateToCloudDelegationFilters(p_Session);
	for (auto& k : m_ForCloudDelegationFilters)
		p_AllData[k.GetTable()][std::to_wstring(k.GetID())] = &k;

	// member associations
	readForUpdateToCloudDelegationMembers(p_Session);
	for (auto& k : m_ForCloudDelegationMembers)
		p_AllData[k.GetTable()][std::to_wstring(k.GetID())] = &k;

	// privilege associations
	readForUpdateToCloudDelegationPrivileges(p_Session);
	for (auto& k : m_ForCloudDelegationPrivileges)
		p_AllData[k.GetTable()][std::to_wstring(k.GetID())] = &k;

	// filters
	readForUpdateToCloudFilters(p_Session);
	for (auto& k : m_ForCloudFitlers)
		p_AllData[k.GetTable()][std::to_wstring(k.GetID())] = &k;

	// delegations
	readForUpdateToCloudDelegations(p_Session);
	for (auto& k : m_ForCloudDelegations)
		p_AllData[k.GetTable()][std::to_wstring(k.GetID())] = &k;

	// keys
	readForUpdateToCloudKeys(p_Session);
	for (auto& k : m_ForCloudKeys)
		p_AllData[k.GetTable()][std::to_wstring(k.GetID())] = &k;

	// sapio365Admin
	readForUpdateToCloudsapio365Admin(p_Session);
	for (auto& k : m_ForCloudsapio365Admin)
		p_AllData[k.GetTable()][std::to_wstring(k.GetID())] = &k;

	return true;// TODO check SelectAll errors
}

void RoleDelegationSQLite::CompleteUpdateToCloud(std::shared_ptr<const Sapio365Session> p_Session)
{
	m_ForCloudKeys.clear();
	m_ForCloudFitlers.clear();
	m_ForCloudDelegationFilters.clear();
	m_ForCloudDelegationMembers.clear();
	m_ForCloudDelegationPrivileges.clear();
	m_ForCloudDelegations.clear();
	m_ForCloudsapio365Admin.clear();

	SetCloudSyncDate(p_Session);
}

template <class SQLRecordtype>
bool RoleDelegationSQLite::updateSQLfromCosmosDocuments(const SQLiteUtil::Table& p_Table, const vector<Cosmos::Document>& p_CosmosDocuments, std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Prog)
{
	bool rvSuccess = true;

	TraceIntoFile::trace(_YDUMP("RoleDelegationSQLite"), _YDUMP("updateSQLfromCosmosDocuments"), _YTEXTFORMAT(L"\tupdating table: %s", p_Table.m_Name.c_str()));

	const SQLiteUtil::Column& columnROWID	= p_Table.GetColumnROWID();
	const SQLiteUtil::Column& columnDATE	= p_Table.GetColumn(RoleDelegationUtil::g_ColumnNameDate);

	int64_t		rowId = 0;
	YTimeDate	lastUpdate;
	bool		validDate = false;

	const O365Krypter krypter(p_Session);

	for (const auto& cd : p_CosmosDocuments)
	{
		p_Prog.IncrementCounter2(_YFORMAT(L"Syncing RBAC from Cosmos [%s]", p_Table.m_Name.c_str()));

		SQLRecordtype sqlObjectNew(p_Table);// the thing to write to the SQLite db
		sqlObjectNew.SetDoNotGenerateNewID();

		const auto& object = cd.Content.as_object();
		for (const auto& c : p_Table.m_Columns)
		{
			auto findIt = object.find(c.GetSQLcompliantName());
			if (findIt != object.end())
			{
				const auto& jsonValue = findIt->second;
				// everything must be stored as string in COSMOS! it is pointless to get numbers from the json text and reconvert these numbers to strings to create upate queries for SQLite
				if (findIt->second.is_string())
					sqlObjectNew.AddValue(c, jsonValue.as_string());
				else
					ASSERT(false);

				if (columnROWID == c)
					rowId = Str::getINT64FromString(jsonValue.as_string()); // jsonValue.as_number().to_int64();// abomination of desolation: stored as double in jsonValue = last digits are lost - EVERYTHING AS STRING!
				else if (columnDATE == c)
					validDate = TimeUtil::GetInstance().ConvertTextToDate(jsonValue.as_string(), lastUpdate);
			}
		}
		
		if (rowId != 0)
		{
			sqlObjectNew.SetID(rowId);

			bool update = false;

			YTimeDate date = getLastUpdate(p_Table, rowId, columnDATE);
			if (!date.IsEmpty())
			{
				sqlObjectNew.SetUpdateRecordFromCloud();
				if (validDate && lastUpdate > date)
					update = true;
			}
			else
			{
				sqlObjectNew.SetNewRecordFromCloud();
				update = true;
			}

			if (update)
				rvSuccess = WriteRecordFromValues(krypter, &sqlObjectNew) && rvSuccess;
		}
		else // ROWID not found, corrupted data
		{
			rvSuccess = false;
			auto findIt = object.find(_YTEXT("id"));// Cosmos standard key for their id entry - we set the same value as in "rowid"
			if (findIt != object.end())
			{
				const auto& jsonValue = findIt->second;
				ASSERT(findIt->second.is_string());
				TraceIntoFile::trace(_YDUMP("RoleDelegationSQLite"), _YDUMP("updateSQLfromCosmosDocuments"), _YTEXTFORMAT(L"Corrupted data from Cosmos with id: %s (%s)", jsonValue.as_string().c_str(), p_Table.GetSQLcompliantName().c_str()));
			}
			else
				TraceIntoFile::trace(_YDUMP("RoleDelegationSQLite"), _YDUMP("updateSQLfromCosmosDocuments"), _YTEXTFORMAT(L"Corrupted data from Cosmos with no id (%s)", p_Table.GetSQLcompliantName().c_str()));
		}
	}

	return rvSuccess;
}

bool RoleDelegationSQLite::IsSapio365AdminScopeSetForUser(std::shared_ptr<const Sapio365Session> p_Session, const PooledString& p_UserID, const RoleDelegationUtil::RBAC_AdminScope p_Scope)
{
	SQLiteUtil::SelectQuery query;
	query.m_Table = m_sapio365Admin;
	
	SQLiteUtil::WhereClause wcUser;
	wcUser.m_Column		= m_sapio365Admin.GetColumnForQuery(RoleDelegationUtil::g_ColumnNameUserID);
	wcUser.m_Operator	= SQLiteUtil::Operator::Equal;
	wcUser.m_Values.push_back(p_UserID);

	SQLiteUtil::WhereClause wcScope;
	wcScope.m_Column	= m_sapio365Admin.GetColumnForQuery(RoleDelegationUtil::g_ColumnNameAdminScope);
	wcScope.m_Operator	= SQLiteUtil::Operator::Equal;
	wcScope.m_Values.push_back(Str::getStringFromNumber<uint32_t>(p_Scope));

	SQLiteUtil::WhereClause whereNotRemoved;
	whereNotRemoved.m_Column	= m_sapio365Admin.GetColumnForQuery(RoleDelegationUtil::g_ColumnNameStatus);
	whereNotRemoved.m_Operator	= SQLiteUtil::Operator::NotEqual;
	whereNotRemoved.m_Values.push_back(Str::getStringFromNumber<uint32_t>(RoleDelegationUtil::RoleStatus::STATUS_REMOVED));

	query.m_Where.push_back(wcUser);
	query.m_Where.push_back(wcScope);
	query.m_Where.push_back(whereNotRemoved);

	auto count = SelectCount(O365Krypter(p_Session), query);
	return count && count.get() > 0;
}

void RoleDelegationSQLite::GetAdminAccessForSession(std::shared_ptr<const Sapio365Session> p_Session, set<RoleDelegationUtil::RBAC_AdminScope>& p_AA)
{
	p_AA.clear();

	ASSERT(p_Session);
	if (p_Session && !p_Session->IsUltraAdmin() && !p_Session->IsPartnerAdvanced() && !p_Session->IsPartnerElevated())
	{
		vector<RoleDelegationUtil::sapio365Admin> accessAdmin;
		SelectAllStr<RoleDelegationUtil::sapio365Admin>(O365Krypter(p_Session), m_sapio365Admin, RoleDelegationUtil::g_ColumnNameUserID, wstring(p_Session->GetConnectedUserID().c_str()), RoleDelegationUtil::g_ColumnNameStatus, Str::getStringFromNumber(RoleDelegationUtil::STATUS_REMOVED), accessAdmin);
		for (const auto& aa : accessAdmin)
			p_AA.insert(static_cast<RoleDelegationUtil::RBAC_AdminScope>(aa.m_sapio365Admin.m_AdminScope));
	}
}