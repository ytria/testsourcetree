#pragma once

#include "DlgRoleEditBase.h"

class DlgRoleEditFilter : public DlgRoleEditBase
{
public:
	DlgRoleEditFilter(CWnd* p_Parent);
	virtual ~DlgRoleEditFilter();

	const RoleDelegationUtil::BusinessFilter&	GetFilter() const;
	void										SetFilter(const RoleDelegationUtil::BusinessFilter& p_Filter);

protected:
	virtual bool								isSpecificReadOnly() const override;
	virtual void								generateJSONScriptDataSpecific(web::json::value& p_JsonValue) override;
	virtual bool								processPostedDataSpecific(const IPostedDataTarget::PostedData& data) override;
	virtual	RoleDelegationUtil::BusinessData*	getBusinessData() override;
	virtual wstring								getDialogTitle() const override;
	virtual int									getMinimumHeight() const override;

private:
	RoleDelegationUtil::BusinessFilter m_Filter;

	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;

	static const wstring g_VarObjectType;
	static const wstring g_VarPropertyValue;
	static const wstring g_VarTaggerMatchMethod;
	static const wstring g_VarTaggerMatchMethodSub;
	static const wstring g_VarTaggerMatchMethodSubNOT;
	static const wstring g_VarTaggerMatchMethodSubCASESENSITIVE;

	static wstring g_VarPropertyNameGroup;
	static wstring g_VarPropertyNameSite;
	static wstring g_VarPropertyNameUser;	
};