#include "YCallbackMessage.h"
#include "YCodeJockMessageBox.h"

template<class T>
RoleDelegationManager::ErrorSource RoleDelegationManager::Write(T& p_BusinessThing, std::shared_ptr<const Sapio365Session> p_Session)
{
	ErrorSource rvError = ErrorSource::ERROR_NONE;

	if (p_BusinessThing.IsValid())
	{
		auto SQLthing		= m_RoleDelegationSQLite.Write(p_BusinessThing, p_Session);
		bool writeSuccess	= SQLthing.IsLastWriteSuccess();
		if (writeSuccess)
		{
			p_BusinessThing = *dynamic_cast<T*>(SQLthing.GetGeneric());
			if (p_Session->IsReadyForCosmos())
				writeSuccess = m_RoleDelegationCosmos.Write(&SQLthing, p_Session);
			if (!writeSuccess)
				rvError = ErrorSource::ERROR_COSMOS;
		}
		else
		{
			rvError = ErrorSource::ERROR_SQLITE;
			YCallbackMessage::DoSend([p_BusinessThing]()
			{
				YCodeJockMessageBox dlg(AfxGetApp()->m_pMainWnd,
										DlgMessageBox::eIcon_Error,
										YtriaTranslate::Do(LogGrid_getLogLevelText_4, _YLOC("Error")).c_str(),
										YtriaTranslate::Do(RoleDelegationManager_Write_6, _YLOC("A problem occurred while trying to store the data: %1"), p_BusinessThing.m_Name.c_str()).c_str(),
										_YTEXT(":-("),
										{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
				dlg.DoModal();
			});
		}
	}
	else
	{
		YCallbackMessage::DoSend([p_BusinessThing]()
		{
			YCodeJockMessageBox dlg(AfxGetApp()->m_pMainWnd,
									DlgMessageBox::eIcon_Error,
									YtriaTranslate::Do(Application_error_2, _YLOC("Error")).c_str(),
									YtriaTranslate::Do(RoleDelegationManager_Write_5, _YLOC("A problem occurred while trying to store the data, status is not valid for: %1"), p_BusinessThing.m_Name.c_str()).c_str(),
									_YTEXT(":-("),
									{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
			dlg.DoModal();
		});
	}
	
	return rvError;
}

template <class BusinessOrCachedObject>
bool RoleDelegationManager::HasPrivilege(const BusinessOrCachedObject& p_Obj, const RoleDelegationUtil::RBAC_Privilege p_Privilege, std::shared_ptr<const Sapio365Session> p_Session)
{
	// RoleDelegationID == -1 means no RoleDelegation
	// RoleDelegationID == 0 means not authorized by RoleDelegation
	return RoleDelegationUtil::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE == p_Privilege
		? 0 != p_Obj.GetRBACDelegationID()
		: -1 < p_Obj.GetRBACDelegationID() ? GetRoleDelegationFromCache(p_Obj.GetRBACDelegationID(), p_Session).HasPrivilege(p_Privilege) : true;
}