#pragma once

#include "BusinessSubscribedSku.h"
#include "DlgRoleEditBase.h"

class DlgRoleSeeInfo : public DlgRoleEditBase
{
public:
	DlgRoleSeeInfo(int64_t p_RoleId, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent);

protected:
	virtual bool								isSpecificReadOnly() const override;
	virtual void								generateJSONScriptDataSpecific(web::json::value& p_JsonValue) override;
	virtual bool								processPostedDataSpecific(const IPostedDataTarget::PostedData& data) override;
	virtual wstring								getDialogTitle() const override;
	virtual RoleDelegationUtil::BusinessData*	getBusinessData() override;
	virtual int									getMinimumHeight() const override;

private:
	RoleDelegation m_Delegation;
	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;

};
