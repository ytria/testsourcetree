#include "DlgRoleEditKey.h"

#include "AddApplicationPasswordRequester.h"
#include "ApplicationRequester.h"
#include "AppResourceAccess.h"
#include "BasicPageRequestLogger.h"
#include "BasicRequestLogger.h"
#include "CachedUserPropertySet.h"
#include "CreateAppRequester.h"
#include "DlgLoading.h"
#include "JSONUtil.h"
#include "MainFrame.h"
#include "OrganizationRequester.h"
#include "O365AdminUtil.h"
#include "Product.h"
#include "RequestsDlgUtil.h"
#include "RunOnScopeEnd.h"
#include "UserRequester.h"

class DlgRoleEditKey::ExternalFctsHandler : public CCmdTarget
{
public:
	static wstring g_DefaultPasswordValue;
	static wstring g_DefaultSecretKeyValue;

	ExternalFctsHandler(DlgRoleEditKey* p_Parent, std::shared_ptr<const Sapio365Session> p_Session, RoleDelegationUtil::BusinessKey *p_Key, YAdvancedHtmlView* p_HtmlView)
		: m_Parent(p_Parent)
		, m_Key(p_Key)
		, m_HtmlView(p_HtmlView)
		, m_Session(p_Session)
	{
		EnableAutomation();

		if (g_DefaultPasswordValue.empty())
			g_DefaultPasswordValue = _T("(Password not shown when saved)");

		if (g_DefaultSecretKeyValue.empty())
			g_DefaultSecretKeyValue = _T("(Application secret password not shown when saved)");

	}

	BSTR RunAdmin(LPCWSTR p_Tenant, LPCWSTR p_Login, LPCWSTR p_PWD)
	{
		_bstr_t rv = "BAD";

        wstring login = p_Login ? wstring(p_Login) : wstring(_YTEXT(""));
        wstring password = p_PWD ? wstring(p_PWD) : wstring(_YTEXT(""));
        wstring tenant = p_Tenant ? wstring(p_Tenant) : wstring(_YTEXT(""));

		wstring pwd = (g_DefaultPasswordValue == password) ? m_Key->m_PWD : password;

		auto adminSessionRes = RoleDelegationUtil::TestAdminSession(tenant, login, pwd);
		bool success = adminSessionRes.first;
		if (!success)
		{
			YCodeJockMessageBox dlg(m_Parent,
				DlgMessageBox::eIcon_Error,
				_T("Error when validating the Global Admin User session."),
				_T("Please verify the credentials entered for the Global Admin User."),
				Sapio365Session::ParseGraphJsonError(adminSessionRes.second).m_ErrorDescription,
				{ { IDOK, _T("OK") } });
			dlg.DoModal();
		}
		else
		{
			rv = "OK";
		}
		return rv;
	}

	BSTR RunUltra(LPCWSTR p_Tenant, LPCWSTR p_AppId, LPCWSTR p_AppSecret)
	{
		_bstr_t rv = "BAD";

        wstring appId = p_AppId ? wstring(p_AppId) : wstring(_YTEXT(""));
        wstring appSecret = p_AppSecret ? wstring(p_AppSecret) : wstring(_YTEXT(""));
        wstring tenant = p_Tenant ? wstring(p_Tenant) : wstring(_YTEXT(""));

		wstring appKey = (g_DefaultSecretKeyValue == appSecret) ? m_Key->m_ApplicationPWD : appSecret;

		bool success = false;
		auto ultraAdminSessionRes = RoleDelegationUtil::TestUltraAdminSession(tenant, appId, appKey);
		success = ultraAdminSessionRes.first;
		if (!success)
		{
			Util::ShowApplicationConnectionError(_T("Error when validating the Elevated Privileges application."), ultraAdminSessionRes.second, m_Parent);
		}
		else
		{
			rv = "OK";
		}
		return rv;
	}

	void CreateNewPair(LPCWSTR p_Tenant)
	{
		YCodeJockMessageBox conf(m_Parent,
			DlgMessageBox::eIcon_ExclamationWarning,
			_T("Continue?"),
			_T("This will create a new user with global admin rights and a new application on the related Azure AD."),
			_T(""),
			{ { IDOK, _T("OK") }, { IDCANCEL, _T("Cancel") } });
		auto res = conf.DoModal();
		if (res == IDOK)
		{
			auto tempGraphSession = Sapio365Session::CreateMSGraphSession();
			
			tempGraphSession->Start(m_Parent).Then([sapioSession = std::make_shared<Sapio365Session>(), parent = m_Parent, htmlView = m_HtmlView, tempGraphSession](RestAuthenticationResult p_Result) {
				if (p_Result.state == RestAuthenticationResult::State::Success)
				{
					bool success = true;

					YCallbackMessage::DoPost([parent]() { parent->EnableWindow(FALSE); });

					auto dlgLoading = std::make_shared<DlgLoading>(_T("Creating new application & admin..."));
					auto taskFinisher = std::make_shared<RunOnScopeEnd>([dlgLoading, parent]() {
						YCallbackMessage::DoPost([dlgLoading, parent]() {
							parent->EnableWindow(TRUE);
							dlgLoading->EndDialog(IDOK);
						});
					});

					sapioSession->SetMSGraphSession(tempGraphSession);

					OrganizationRequester orgReq(false, std::make_shared<BasicPageRequestLogger>(_T("organization")));
					orgReq.Send(sapioSession, YtriaTaskData()).GetTask().wait();


					wstring tenant;
					ASSERT(orgReq.GetData().size() == 1);
					if (orgReq.GetData().size() == 1)
						tenant = orgReq.GetData()[0].GetTenantName(true);

					YCallbackMessage::DoPost([dlgLoading]() {
						dlgLoading->DoModal();
					});
					// Create ultra admin app
					wstring password;
					CreateApplicationRequester createAppRequester(_T("sapio365 RBAC Ultra Admin"), AppResourceAccess::GetUltraAdminResourceAccess());
					{
						CWaitCursor waitCur;
						createAppRequester.Send(sapioSession, YtriaTaskData()).GetTask().wait();
					}

					const auto& res = createAppRequester.GetResult();
					ASSERT(res.GetResult().Response.status_code() == 201);
					if (res.GetResult().Response.status_code() != 201)
					{
						YCallbackMessage::DoPost([parent, res]() {
							YCodeJockMessageBox dlg(parent, DlgMessageBox::eIcon::eIcon_Error, _T("Can't create new application"), res.GetResult().GetErrorMessage(), _YTEXT(""), { { IDOK, _YTEXT("OK") } });
							dlg.DoModal();
						});
						
						success = false;
					}
					else
					{
						auto logger = std::make_shared<BasicRequestLogger>(_T("application"));
						LoggerService::Debug(wstring(_YTEXT("Created application with appId ")) + wstring(createAppRequester.GetData().m_AppId ? *createAppRequester.GetData().m_AppId : _YTEXT("")) +
							wstring(_YTEXT(" and id ")) + wstring(createAppRequester.GetData().m_Id ? *createAppRequester.GetData().m_Id : _YTEXT("")));
						ApplicationRequester appReq(createAppRequester.GetData().m_Id ? *createAppRequester.GetData().m_Id : _YTEXT(""), logger);
						appReq.Send(sapioSession, YtriaTaskData()).GetTask().wait();

						while (appReq.GetResult().GetResult().Response.status_code() != 200)
						{
							appReq.Send(sapioSession, YtriaTaskData()).GetTask().wait();

							if (appReq.GetResult().GetResult().Response.status_code() != 404 &&
								appReq.GetResult().GetResult().Response.status_code() != 200)
							{
								success = false;
								break;
							}

							pplx::wait(1000);
						}

						if (success)
						{
							wstring appId = appReq.GetData().m_Id ? *appReq.GetData().m_Id : _YTEXT("");
							AddApplicationPasswordRequester addAppPwdRequester(_T("sapio365 RBAC Ultra Admin"), appId);
							addAppPwdRequester.Send(sapioSession, YtriaTaskData()).GetTask().wait();

							success = addAppPwdRequester.GetData().m_SecretText.is_initialized();
							if (success)
								password = *addAppPwdRequester.GetData().m_SecretText;
						}
					}

					if (success)
					{
						// Create user and password
						auto usrnameAndPasswd = Util::CreateGlobalAdmin(tenant, sapioSession);
						success = !usrnameAndPasswd.first.empty();
						if (success)
						{
							auto logger = std::make_shared<BasicRequestLogger>(_T("user"));
							UserRequester usrReq(usrnameAndPasswd.first, CachedUserPropertySet(), true, logger);
							usrReq.SetRbacSessionMode(Sapio365Session::USER_SESSION);
							usrReq.Send(sapioSession, YtriaTaskData()).GetTask().wait();

							while (usrReq.GetResult().GetResult().Response.status_code() != 200)
							{
								usrReq.Send(sapioSession, YtriaTaskData()).GetTask().wait();
								if (usrReq.GetResult().GetResult().Response.status_code() != 404 &&
									usrReq.GetResult().GetResult().Response.status_code() != 200)
								{
									success = false;
									break;
								}

								pplx::wait(1000);
							}
							if (success)
							{
								YCallbackMessage::DoPost([usrnameAndPasswd, htmlView, createAppRequester, password, tenant]() {
									htmlView->ExecuteScript(_YTEXTFORMAT(LR"~(setInfo("%s", "%s", "%s", "%s", "%s");)~", usrnameAndPasswd.first.c_str(), usrnameAndPasswd.second.c_str(), createAppRequester.GetData().m_AppId ? createAppRequester.GetData().m_AppId->c_str() : _YTEXT(""), password.c_str(), tenant.c_str()));
									});
							}
						}
					}

					if (success)
					{
						// Can't enable this for now, as new Application needs time before being ready for consent...
#define PROVIDE_CONSENT_NOW 0
						YCallbackMessage::DoPost([parent]() {
#if PROVIDE_CONSENT_NOW
							YCodeJockMessageBox resultDlg(parent,
								DlgMessageBox::eIcon_Information,
								_YCOM("Success!"),
								_YCOM("The following has been successfully created in your Azure AD:\n-A new user with admin rights\n-A new application"),
								_YCOM("Due to API restriction, Admin Consent has not been given to the new application. You should do it now."),
								{ { IDOK, _YCOM("Provide Admin Consent") },{ IDCANCEL, _YCOM("Skip") } });

							if (IDOK == resultDlg.DoModal())
								m_Parent->getAdminConsent(p_Tenant, createAppRequester.GetData().m_AppId.c_str());
#else
							YCodeJockMessageBox resultDlg(parent,
								DlgMessageBox::eIcon_Information,
								_T("Success!"),
								_T("The following has been successfully created in your Azure AD:\n-A new user with global admin rights\n-A new application"),
								_T("Don�t forget to click the Provide Admin Consent button."),
								{ { IDOK, _T("OK") } });

							resultDlg.DoModal();
#endif
						});
					}
				}
			});
			
		}
	}

private:
	DECLARE_DISPATCH_MAP()

	std::shared_ptr<const Sapio365Session> m_Session;
	RoleDelegationUtil::BusinessKey* m_Key;
	YAdvancedHtmlView* m_HtmlView;
	DlgRoleEditKey* m_Parent;
};

wstring DlgRoleEditKey::ExternalFctsHandler::g_DefaultPasswordValue;
wstring DlgRoleEditKey::ExternalFctsHandler::g_DefaultSecretKeyValue;

BEGIN_DISPATCH_MAP(DlgRoleEditKey::ExternalFctsHandler, CCmdTarget)
	DISP_FUNCTION(DlgRoleEditKey::ExternalFctsHandler, "testAdminSession", RunAdmin, VT_BSTR, VTS_WBSTR VTS_WBSTR VTS_WBSTR)
	DISP_FUNCTION(DlgRoleEditKey::ExternalFctsHandler, "testUltraAdmin", RunUltra, VT_BSTR, VTS_WBSTR VTS_WBSTR VTS_WBSTR)
	DISP_FUNCTION(DlgRoleEditKey::ExternalFctsHandler, "createNewPair", CreateNewPair, VT_EMPTY, VTS_WBSTR)
END_DISPATCH_MAP()

std::map<wstring, int, Str::keyLessInsensitive> DlgRoleEditKey::g_Sizes;

const wstring DlgRoleEditKey::g_VarTenantName		= _YTEXT("TENANT");
const wstring DlgRoleEditKey::g_VarUltraAdminID		= _YTEXT("UAID");
const wstring DlgRoleEditKey::g_VarUltraAdminSecret	= _YTEXT("UASECRET");
const wstring DlgRoleEditKey::g_VarAdminLogin		= _YTEXT("ALOGIN");
const wstring DlgRoleEditKey::g_VarAdminPWD			= _YTEXT("APWD");

const wstring DlgRoleEditKey::g_UAConsentURL = _YTEXT("ytriaurl://getconsentforultra/");
const wstring DlgRoleEditKey::g_AdminConsentURL = _YTEXT("ytriaurl://getconsentforadmin/");

DlgRoleEditKey::DlgRoleEditKey(std::shared_ptr<const Sapio365Session> p_SessionForAdminConsent, CWnd* p_Parent)
	: DlgRoleEditBase(p_Parent)
	, m_Session(p_SessionForAdminConsent)
{
	
	ASSERT(m_Session);

	GetHtmlView()->SetExternal(std::make_shared<ExternalFctsHandler>(this, p_SessionForAdminConsent, &m_Key, GetHtmlView().get()));

	// Load hbs sizes
	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_RBACCONFIGCREDENTIALS_SIZE, g_Sizes);
		ASSERT(success);
	}

	GetHtmlView()->SetPostedURLTarget(this);
	GetHtmlView()->AddURL(g_UAConsentURL);
	GetHtmlView()->AddURL(g_AdminConsentURL);
}

DlgRoleEditKey::~DlgRoleEditKey()
{
}

const RoleDelegationUtil::BusinessKey&	DlgRoleEditKey::GetKey() const
{
	return m_Key;
}

void DlgRoleEditKey::SetKey(const RoleDelegationUtil::BusinessKey& p_Key)
{
	m_Key = p_Key;
}

bool DlgRoleEditKey::OnNewPostedURL(const wstring& url)
{
	ASSERT(!url.empty());
	if (!url.empty())
	{
		if (url.find(g_UAConsentURL) == 0)
		{
			const wstring params(url.substr(g_UAConsentURL.size()));

			auto tenantAndAppId = Str::explodeIntoVector(params, wstring(_YTEXT("/")));
			ASSERT(tenantAndAppId.size() == 2);
			if (tenantAndAppId.size() == 2)
				getAdminConsent(tenantAndAppId[1], tenantAndAppId[0]);
		}
		else if (url.find(g_AdminConsentURL) == 0)
		{
			auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
			ASSERT(nullptr != app);
			if (nullptr != app) {
				if (Util::ShowRestrictedAccessDialog(
					DlgRestrictedAccess::CONSENT_REMINDER
					, { YtriaTranslate::Do(DlgRoleEditKey_OnNewPostedURL_1, _YLOC("You will be asked to log-in and give 'app consent'.")).c_str()
					, YtriaTranslate::Do(Office365AdminApp_createUserSession_2, _YLOC("Rest assured that your information will never pass through any external servers, and will not be accessible to any other instances of sapio365.")).c_str()
					, YtriaTranslate::Do(DlgRoleEditKey_OnNewPostedURL_3, _YLOC("Note that this consent cannot overwrite users' tenant rights. You can only authorise sapio365 to access information user has the rights to access.")).c_str()
					, YtriaTranslate::Do(DlgRoleEditKey_OnNewPostedURL_4, _YLOC("(more...)")).c_str()
					, Product::getURLHelpWebSitePage(_YTEXT("Help/Consent")) }
					, NoSetting<bool>()  /// FIXME -- To be set
					, this
				))
				{
					app->GetConsent(SessionTypes::GetAdvancedSessionConsentUrl(true).to_uri(), SessionTypes::g_AdvancedSession, this);
				}
			}
		}
	}

	return true; // Maybe should be void.
}

RoleDelegationUtil::BusinessData* DlgRoleEditKey::getBusinessData()
{
	return &m_Key;
}

bool DlgRoleEditKey::isSpecificReadOnly() const
{
	return false;
}

void DlgRoleEditKey::generateJSONScriptDataSpecific(web::json::value& p_JsonValue)
{
	auto& items = p_JsonValue.as_array();
	auto nextItemIndex = items.size();
	ASSERT(1 == nextItemIndex);

	{
		auto& rbacItem = items[0];
		// If a Tenant Name is already set, this means the Credentials is set
		// So no button to create new Create Credentials
		if (m_Key.m_TargetTenant.empty())
		{
			rbacItem[_YTEXT("createNewPairButton")] = JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_24, _YLOC("Create New Admin & Application...")).c_str()));
			rbacItem[_YTEXT("introTextOne")] = JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_25, _YLOC("Clicking the 'Create New Admin & Application...'�button will conveniently create�a new user with admin rights and a new application�for�you in the related Azure AD tenant, and then populate the relevant fields below.\n\nOnce completed, don't forget to�click the�'Provide Admin Consent' button below.")).c_str()));
		}
		rbacItem[_YTEXT("warningInfoIcon")]	= JSON_STRING(_YTEXT("fas fa-info-circle"));
		rbacItem[_YTEXT("warningInfoOne")]	= JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_1, _YLOC("All information entered here will be encrypted.")).c_str()));
		rbacItem[_YTEXT("warningInfoTwo")]	= JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_2, _YLOC("This encryption uses information unique to your tenant, Ytria does not have access to this information.")).c_str()));
	}

	{
		const auto readOnlyTenant = !m_Key.m_TargetTenant.empty();
		wstring targetTenant = m_Key.m_TargetTenant;
        if (targetTenant.empty())
			targetTenant = m_Session->GetTenantName(true);

		items[nextItemIndex++] =
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("hbs"), JSON_STRING(_YTEXT("rbacTenant"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("introName"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_3, _YLOC("Target Tenant Name")).c_str()))
				JSON_END_PAIR,

				JSON_BEGIN_PAIR
					_YTEXT("introTextOne"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_4, _YLOC("By default, the target tenant is set to your current tenant. But you can set a different tenant for which the credentials will apply.")).c_str()))
				JSON_END_PAIR,

				JSON_BEGIN_PAIR
					_YTEXT("tenantField"), JSON_STRING(g_VarTenantName)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("tenantLabel"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(BackstagePanelSessions_SelectionChanged_1, _YLOC("Tenant Name")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("tenantPlaceHolder"), JSON_STRING(_YTEXT("yourtenant.onmicrosoft.com"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("tenantValue"), JSON_STRING(targetTenant)
				JSON_END_PAIR,
//				JSON_BEGIN_PAIR
// // //					_YTEXT("tenantPopup"),  JSON_STRING(MFCUtil::convertEscapeHTMLChar( _YCOM("Enter the Target Tenant Name.")))
//				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("tenantReadOnly"), JSON_STRING(readOnlyTenant ? _YTEXT("x") : _YTEXT(""))
				JSON_END_PAIR,
			JSON_END_OBJECT;
	}
	{
		const auto readOnlyLogin = !m_Key.m_Login.empty();
		const auto hasPassword = !m_Key.m_PWD.empty();
		items[nextItemIndex++] =
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("hbs"),  JSON_STRING(_YTEXT("rbacAdmin"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("introName"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_6, _YLOC("Credentials - Global Admin User")).c_str()))
				JSON_END_PAIR,
		
				JSON_BEGIN_PAIR
					_YTEXT("introTextOne"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_7, _YLOC("You must have a user in your tenant with the Global Administrator role. However, they are not required to be an active user, nor do they require an active license.")).c_str()))
				JSON_END_PAIR,
				/*JSON_BEGIN_PAIR
					_YTEXT("introTextTwo"),  JSON_STRING(MFCUtil::convertEscapeHTMLChar(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_8, _YLOC("If you haven't created one yet, please go to the ")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("linkTextTwo"), JSON_STRING(MFCUtil::convertEscapeHTMLChar(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_9, _YLOC("Office 365 Admin Portal")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("linkTwo"), JSON_STRING(_YTEXT("https://portal.office.com/adminportal/home"))
				JSON_END_PAIR,*/
		
				JSON_BEGIN_PAIR
					_YTEXT("nameField"),  JSON_STRING(g_VarAdminLogin)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("nameLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_3, _YLOC("Username")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("namePlaceHolder"),  JSON_STRING(_YTEXT("admin@yourtenant.onmicrosoft.com"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("nameValue"), JSON_STRING(m_Key.m_Login)
				JSON_END_PAIR,
//				JSON_BEGIN_PAIR
// //					_YTEXT("namePopup"),  JSON_STRING(MFCUtil::convertEscapeHTMLChar( _YCOM("Enter the username of the global administrator account.")))
//				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("nameReadOnly"), JSON_STRING(readOnlyLogin ? _YTEXT("x") : _YTEXT(""))
				JSON_END_PAIR,
		
				JSON_BEGIN_PAIR
					_YTEXT("pwdField"),  JSON_STRING(g_VarAdminPWD)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("pwdLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_7, _YLOC("Password")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("pwdPlaceHolder"),  JSON_STRING(_YTEXT("Password"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("pwdValue"),  JSON_STRING(hasPassword ? ExternalFctsHandler::g_DefaultPasswordValue : _YTEXT(""))
				JSON_END_PAIR,
//				JSON_BEGIN_PAIR
// //					_YTEXT("pwdPopup"),  JSON_STRING(MFCUtil::convertEscapeHTMLChar( _YCOM("Enter the password of the global administrator account.")))
//				JSON_END_PAIR,

				JSON_BEGIN_PAIR
					_YTEXT("adminConsentIcon"), JSON_STRING(_YTEXT("fas fa-exclamation-triangle"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("adminConsentInfoOne"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_26, _YLOC("To allow the use of this global admin user, you may have to provide admin consent to sapio365 on the related Tenant.")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("adminConsentButton"), JSON_STRING(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_19, _YLOC("Provide Admin Consent to sapio365")).c_str())
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("adminConsentAction"), JSON_STRING(g_AdminConsentURL)
				JSON_END_PAIR,

			JSON_END_OBJECT;
	}

	{
		const auto readOnlyAppID = !m_Key.m_ApplicationID.empty();
		const auto hasPassword = !m_Key.m_ApplicationPWD.empty();
		items[nextItemIndex++] =
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("hbs"),  JSON_STRING(_YTEXT("rbacUltra"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("introName"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_12, _YLOC("Credentials - App for Elevated Privileges")).c_str()))
				JSON_END_PAIR,
		
				JSON_BEGIN_PAIR
					_YTEXT("introTextOne"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_27, _YLOC("To use sapio365 RBAC, elevated privileges are required. These are handled by an application with a full set of permissions on your Azure AD.")).c_str()))
				JSON_END_PAIR,
				/*JSON_BEGIN_PAIR
					_YTEXT("introTextTwo"),  JSON_STRING(MFCUtil::convertEscapeHTMLChar(_YCOM("If you haven�t already done so, go to the ")))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("linkTextTwo"), JSON_STRING(MFCUtil::convertEscapeHTMLChar(_YCOM("Application Registration Portal")))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("linkTwo"), JSON_STRING(_YTEXT("https://apps.dev.microsoft.com/"))
				JSON_END_PAIR,*/
				JSON_BEGIN_PAIR
					_YTEXT("introTextThree"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_4, _YLOC("Need help? See ")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("linkTextThree"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_17, _YLOC("How to Register Your Application")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("linkThree"), JSON_STRING(Product::getURLHelpWebSitePage(_YTEXT("Help/dialog-how-to-create-application-ID")))
				JSON_END_PAIR,
		
				JSON_BEGIN_PAIR
					_YTEXT("appIDField"),  JSON_STRING(g_VarUltraAdminID)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("appIDLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_18, _YLOC("Application ID")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("appIDPlaceHolder"),  JSON_STRING(_YTEXT("xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("appIDValue"), JSON_STRING(m_Key.m_ApplicationID)
				JSON_END_PAIR,
//				JSON_BEGIN_PAIR
//					_YTEXT("appIDPopup"),  JSON_STRING(MFCUtil::convertEscapeHTMLChar(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_19, _YLOC("Please provide the application ID.")).c_str()))
//				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("appIDReadOnly"), JSON_STRING(readOnlyAppID ? _YTEXT("x") : _YTEXT(""))
				JSON_END_PAIR,
		
				JSON_BEGIN_PAIR
					_YTEXT("secretKeyField"),  JSON_STRING(g_VarUltraAdminSecret)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("secretKeyLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_14, _YLOC("Application Password")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("secretKeyPlaceHolder"),  JSON_STRING(_YTEXT("Application password"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("secretKeyValue"),  JSON_STRING(hasPassword ? ExternalFctsHandler::g_DefaultSecretKeyValue : _YTEXT(""))
				JSON_END_PAIR,
//				JSON_BEGIN_PAIR
//					_YTEXT("secretKeyPopup"),  JSON_STRING(MFCUtil::convertEscapeHTMLChar(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_15, _YLOC("Please provide the application password.")).c_str()))
//				JSON_END_PAIR,

				JSON_BEGIN_PAIR
					_YTEXT("adminConsentIcon"),  JSON_STRING(_YTEXT("fas fa-exclamation-triangle"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("adminConsentInfoOne"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_28, _YLOC("To use this application, admin consent is required on the associated tenant.")).c_str()))
				JSON_END_PAIR,
//				JSON_BEGIN_PAIR
// //					_YTEXT("adminConsentInfoTwo"),  JSON_STRING(MFCUtil::convertEscapeHTMLChar(_YCOM("The encryption used some information unique to your tenant. Ytria does not have access to these information.")))
//				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("adminConsentButton"),  JSON_STRING(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_29, _YLOC("Provide Admin Consent")).c_str())
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("adminConsentAction"),  JSON_STRING(g_UAConsentURL)
				JSON_END_PAIR,
			JSON_END_OBJECT;
	}
}

bool DlgRoleEditKey::processPostedDataSpecific(const IPostedDataTarget::PostedData& data)
{
	for (const auto& item : data)
	{
		if (g_VarTenantName == item.first)
		{
			m_Key.m_TargetTenant= item.second;
		}
		else  if (g_VarUltraAdminID == item.first)
		{
			m_Key.m_ApplicationID = item.second;
		}
		else if (g_VarUltraAdminSecret == item.first)
		{
			if (ExternalFctsHandler::g_DefaultSecretKeyValue != item.second)
				m_Key.m_ApplicationPWD = item.second;
		}
		else if (g_VarAdminLogin == item.first)
		{
			m_Key.m_Login = item.second;
		}
		else if (g_VarAdminPWD == item.first)
		{
			if (ExternalFctsHandler::g_DefaultPasswordValue != item.second)
				m_Key.m_PWD = item.second;
		}
	}

	return true;
}

wstring DlgRoleEditKey::getDialogTitle() const
{
	return YtriaTranslate::Do(DlgRoleEditKey_getDialogTitle_1, _YLOC("Credentials setup")).c_str();
}

int DlgRoleEditKey::getMinimumHeight() const
{
	const auto it = g_Sizes.find(_YTEXT("min-height"));
	ASSERT(g_Sizes.end() != it);
	if (g_Sizes.end() != it)
		return it->second;
	return DlgRoleEditBase::getMinimumHeight();
}

bool DlgRoleEditKey::shouldAddCredentialsCreationButton(bool p_ButtonTop) const
{
	// if true, this will add the create new on TOP. Disabled for now: button is in the rbacIntro...
	if (p_ButtonTop)
		return false;

	// If a Tenant Name is already set, this means the Credentials is set
	// So no button in the Form to create new Create Credentials
	return m_Key.m_TargetTenant.empty();
}

void DlgRoleEditKey::getAdminConsent(const wstring& p_Tenant, const wstring& p_AppID)
{
	MainFrame::GiveConsentForApp(p_Tenant, p_AppID, this);
}