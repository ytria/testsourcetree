#include "DlgRoleEditFilter.h"

#include "BusinessGroupConfiguration.h"
#include "BusinessSiteConfiguration.h"
#include "BusinessUserConfiguration.h"
#include "GroupListFullPropertySet.h"
#include "JSONUtil.h"
#include "MFCUtil.h"
#include "UserListNoLicensePlansInfoPropertySet.h"

std::map<wstring, int, Str::keyLessInsensitive> DlgRoleEditFilter::g_Sizes;

const wstring DlgRoleEditFilter::g_VarObjectType							= _YTEXT("OBJECTTYPE");
const wstring DlgRoleEditFilter::g_VarPropertyValue							= _YTEXT("PROPERTYVALUE");
const wstring DlgRoleEditFilter::g_VarTaggerMatchMethod						= _YTEXT("TAGGERMATCHMETHOD");
const wstring DlgRoleEditFilter::g_VarTaggerMatchMethodSub					= _YTEXT("TAGGERMATCHMETHODSUB");
const wstring DlgRoleEditFilter::g_VarTaggerMatchMethodSubNOT				= _YTEXT("NOT");
const wstring DlgRoleEditFilter::g_VarTaggerMatchMethodSubCASESENSITIVE		= _YTEXT("CASESENSITIVE");


wstring DlgRoleEditFilter::g_VarPropertyNameGroup;
wstring DlgRoleEditFilter::g_VarPropertyNameSite;
wstring DlgRoleEditFilter::g_VarPropertyNameUser;

DlgRoleEditFilter::DlgRoleEditFilter(CWnd* p_Parent)
	: DlgRoleEditBase(p_Parent)
{
	// Load hbs sizes
	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_RBACCONFIGFILTERS_SIZE, g_Sizes);
		ASSERT(success);
	}
}

DlgRoleEditFilter::~DlgRoleEditFilter()
{
}

const RoleDelegationUtil::BusinessFilter& DlgRoleEditFilter::GetFilter() const
{
	return m_Filter;
}

void DlgRoleEditFilter::SetFilter(const RoleDelegationUtil::BusinessFilter& p_Filter)
{
	m_Filter = p_Filter;
}

RoleDelegationUtil::BusinessData* DlgRoleEditFilter::getBusinessData()
{
	return &m_Filter;
}

bool DlgRoleEditFilter::isSpecificReadOnly() const
{
	return false;
}

void DlgRoleEditFilter::generateJSONScriptDataSpecific(web::json::value& p_JsonValue)
{
	auto& items = p_JsonValue.as_array();
	auto nextItemIndex = items.size();
	ASSERT(1 == nextItemIndex);

	if (g_VarPropertyNameGroup.empty())
	{
		ASSERT(g_VarPropertyNameSite.empty() && g_VarPropertyNameUser.empty());

		g_VarPropertyNameGroup	= RoleDelegationUtil::FilterTypes::GetInstance().GetTypeGroup();
		g_VarPropertyNameSite	= RoleDelegationUtil::FilterTypes::GetInstance().GetTypeSite();
		g_VarPropertyNameUser	= RoleDelegationUtil::FilterTypes::GetInstance().GetTypeUser();
	}

	{
		const bool hasUserFilter	= !BusinessUserConfiguration::GetInstance().GetProperties4RoleDelegation().empty();
		const bool hasGroupFilter	= !BusinessGroupConfiguration::GetInstance().GetProperties4RoleDelegation().empty();
		const bool hasSiteFilter	= !BusinessSiteConfiguration::GetInstance().GetProperties4RoleDelegation().empty();

		auto& item = items[nextItemIndex++];
		item =
			JSON_BEGIN_OBJECT
				JSON_BEGIN_PAIR
					_YTEXT("hbs"),  JSON_STRING(_YTEXT("rbacFilter"))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("introName"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditFilter_getDialogTitle_1, _YLOC("Scope Definition")).c_str()))
				JSON_END_PAIR,
		
				JSON_BEGIN_PAIR
					_YTEXT("introTextOne"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditFilter_generateJSONScriptDataSpecific_2, _YLOC("Note: currently only one value can be defined in this scope. To assign multiple scopes to a role, you will need to create multiple unique scopes. Scopes on different properties will be AND'd. Scopes on the same property will be OR'd.")).c_str()))
				JSON_END_PAIR,

				JSON_BEGIN_PAIR
					_YTEXT("objectField"),  JSON_STRING(g_VarObjectType)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("objectLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditFilter_generateJSONScriptDataSpecific_3, _YLOC("This scope will target:")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("objectChoice"),
					JSON_BEGIN_ARRAY
					JSON_END_ARRAY
				JSON_END_PAIR,

				JSON_BEGIN_PAIR
					_YTEXT("filterField"),  JSON_STRING(g_VarTaggerMatchMethod)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("filterLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(GLOBAL_BEGIN_MESSAGE_MAP_1, _YLOC("Condition:")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("filterChoice"),
					JSON_BEGIN_ARRAY
					JSON_END_ARRAY
				JSON_END_PAIR,

				JSON_BEGIN_PAIR
					_YTEXT("filterAltField"), JSON_STRING(g_VarTaggerMatchMethodSub)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("filterNotLabel"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditFilter_generateJSONScriptDataSpecific_5, _YLOC("Does NOT Match")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("filterNotValue"), JSON_STRING(g_VarTaggerMatchMethodSubNOT)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("filterNotDefault"), JSON_STRING(RoleDelegationUtil::IsTaggerMatchMethodNOT(m_Filter.m_TaggerMatchMethod) ? _YTEXT("x") : _YTEXT("")) // X if NOT option is SET
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("filterInsensitiveLabel"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSearchAndReplace_OnInitDialog_8, _YLOC("Case Sensitive")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("filterInsensitiveValue"), JSON_STRING(g_VarTaggerMatchMethodSubCASESENSITIVE)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("filterInsensitiveDefault"), JSON_STRING(RoleDelegationUtil::IsTaggerMatchMethodCASEINSENSITIVE(m_Filter.m_TaggerMatchMethod) ? _YTEXT("x") : _YTEXT(""))
				JSON_END_PAIR,

				JSON_BEGIN_PAIR
					_YTEXT("colUserField"),  JSON_STRING(hasUserFilter ? g_VarPropertyNameUser : _YTEXT(""))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("colUserLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditFilter_generateJSONScriptDataSpecific_7, _YLOC("For the property:")).c_str()))
				JSON_END_PAIR,
				/*JSON_BEGIN_PAIR
					_YTEXT("colUSerProp"),  JSON_STRING(_YTEXT("user"))
				JSON_END_PAIR,*/
				JSON_BEGIN_PAIR
					_YTEXT("colUserChoice"),
					JSON_BEGIN_ARRAY
					JSON_END_ARRAY
				JSON_END_PAIR,
			
				JSON_BEGIN_PAIR
					_YTEXT("colGroupField"),  JSON_STRING(hasGroupFilter ? g_VarPropertyNameGroup : _YTEXT(""))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("colGroupLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditFilter_generateJSONScriptDataSpecific_7, _YLOC("For the property:")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("colGroupChoice"),
					JSON_BEGIN_ARRAY
					JSON_END_ARRAY
				JSON_END_PAIR,

				JSON_BEGIN_PAIR
					_YTEXT("colSiteField"),  JSON_STRING(hasSiteFilter ? g_VarPropertyNameSite : _YTEXT(""))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("colSiteLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditFilter_generateJSONScriptDataSpecific_7, _YLOC("For the property:")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("colSiteChoice"),
					JSON_BEGIN_ARRAY
					JSON_END_ARRAY
				JSON_END_PAIR,

				JSON_BEGIN_PAIR
					_YTEXT("propertyField"),  JSON_STRING(g_VarPropertyValue)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("propertyLabel"),  JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditFilter_generateJSONScriptDataSpecific_10, _YLOC("With the value:")).c_str()))
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("propertyPlaceholder"),  JSON_STRING(YtriaTranslate::Do(DlgRoleEditFilter_generateJSONScriptDataSpecific_11, _YLOC("Enter the value to be used for the property above.")).c_str())
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("propertyValue"),  JSON_STRING(m_Filter.m_Value)
				JSON_END_PAIR,
				JSON_BEGIN_PAIR
					_YTEXT("propertyPopup"),  JSON_STRING(_YTEXT(""))
				JSON_END_PAIR,
			
			JSON_END_OBJECT;

		{
			auto& choices = item.as_object()[_YTEXT("objectChoice")].as_array();

			static const auto& filterTypes = RoleDelegationUtil::FilterTypes::GetInstance();

			const map<wstring, wstring> objectTypes{
				{ g_VarPropertyNameGroup , filterTypes.GetNameGroup() },
				{ g_VarPropertyNameSite , filterTypes.GetNameSite() },
				{ g_VarPropertyNameUser , filterTypes.GetNameUser() },
			};

			const auto defaultObjectType = m_Filter.m_ObjectType.empty() ? g_VarPropertyNameUser : m_Filter.m_ObjectType;

			size_t choiceIndex = 0;
			for (const auto& item : objectTypes)
			{
				choices[choiceIndex++] =
					JSON_BEGIN_OBJECT
						JSON_PAIR_KEY(_YTEXT("value"))         JSON_PAIR_VALUE(JSON_STRING(item.first)),
						JSON_PAIR_KEY(_YTEXT("label"))         JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(item.second))),
						JSON_PAIR_KEY(_YTEXT("default"))       JSON_PAIR_VALUE(JSON_STRING((defaultObjectType == item.first ? _YTEXT("x") : _YTEXT("")))),
					JSON_END_OBJECT;
			}
		}

		{
			static const auto& matchMethods = RoleDelegationUtil::GetValueAndLabel_TaggerMatchMethodMain();

			auto& choices = item.as_object()[_YTEXT("filterChoice")].as_array();
			size_t choiceIndex = 0;
			for (const auto& item : matchMethods)
			{
				choices[choiceIndex++] =
					JSON_BEGIN_OBJECT
						JSON_PAIR_KEY(_YTEXT("value"))		JSON_PAIR_VALUE(JSON_STRING(Str::getStringFromNumber(item.first))),
						JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(item.second))),
						JSON_PAIR_KEY(_YTEXT("default"))	JSON_PAIR_VALUE(JSON_STRING(RoleDelegationUtil::GetTaggerMatchMethodMain(m_Filter.m_TaggerMatchMethod) == item.first ? _YTEXT("x") : _YTEXT(""))),
					JSON_END_OBJECT;
			}
		}

		{
			auto& choices = item.as_object()[_YTEXT("colUserChoice")].as_array();
			const auto& ucfg = BusinessUserConfiguration::GetInstance();

			if (!ucfg.GetProperties4RoleDelegation().empty())
			{
				// Ensure at least one value is selected otherwise OK button is blocked.
				const auto selectedProperty = (ucfg.GetProperties4RoleDelegation().end() != ucfg.GetProperties4RoleDelegation().find(m_Filter.m_Property))
					? m_Filter.m_Property
					: *ucfg.GetProperties4RoleDelegation().begin();
				size_t choiceIndex = 0;
				for (const auto& propName : ucfg.GetProperties4RoleDelegation())
				{
					choices[choiceIndex++] =
						JSON_BEGIN_OBJECT
						JSON_PAIR_KEY(_YTEXT("value"))         JSON_PAIR_VALUE(JSON_STRING(propName)),
						JSON_PAIR_KEY(_YTEXT("label"))         JSON_PAIR_VALUE(JSON_STRING(ucfg.GetTitle(propName))),
						JSON_PAIR_KEY(_YTEXT("default"))       JSON_PAIR_VALUE(JSON_STRING((selectedProperty == propName ? _YTEXT("x") : _YTEXT("")))),
						JSON_END_OBJECT;
				}
			}
		}

		{
			auto& choices = item.as_object()[_YTEXT("colGroupChoice")].as_array();
			const auto& gcfg = BusinessGroupConfiguration::GetInstance();

			if (!gcfg.GetProperties4RoleDelegation().empty())
			{
				// Ensure at least one value is selected otherwise OK button is blocked.
				const auto selectedProperty = (gcfg.GetProperties4RoleDelegation().end() != gcfg.GetProperties4RoleDelegation().find(m_Filter.m_Property))
					? m_Filter.m_Property
					: *gcfg.GetProperties4RoleDelegation().begin();

				size_t choiceIndex = 0;
				for (const auto& propName : gcfg.GetProperties4RoleDelegation())
				{
					choices[choiceIndex++] =
						JSON_BEGIN_OBJECT
						JSON_PAIR_KEY(_YTEXT("value"))         JSON_PAIR_VALUE(JSON_STRING(propName)),
						JSON_PAIR_KEY(_YTEXT("label"))         JSON_PAIR_VALUE(JSON_STRING(gcfg.GetTitle(propName))),
						JSON_PAIR_KEY(_YTEXT("default"))       JSON_PAIR_VALUE(JSON_STRING((selectedProperty == propName ? _YTEXT("x") : _YTEXT("")))),
						JSON_END_OBJECT;
				}
			}
		}

		{
			auto& choices = item.as_object()[_YTEXT("colSiteChoice")].as_array();
			const auto& scfg = BusinessSiteConfiguration::GetInstance();

			// Ensure at least one value is selected otherwise OK button is blocked.
			const auto selectedProperty = (scfg.GetProperties4RoleDelegation().end() != scfg.GetProperties4RoleDelegation().find(m_Filter.m_Property))
				? m_Filter.m_Property
				: *scfg.GetProperties4RoleDelegation().begin();
			size_t choiceIndex = 0;
			for (const auto& propName : scfg.GetProperties4RoleDelegation())
			{
				choices[choiceIndex++] =
					JSON_BEGIN_OBJECT
					JSON_PAIR_KEY(_YTEXT("value"))         JSON_PAIR_VALUE(JSON_STRING(propName)),
					JSON_PAIR_KEY(_YTEXT("label"))         JSON_PAIR_VALUE(JSON_STRING(scfg.GetTitle(propName))),
					JSON_PAIR_KEY(_YTEXT("default"))       JSON_PAIR_VALUE(JSON_STRING((selectedProperty == propName ? _YTEXT("x") : _YTEXT("")))),
					JSON_END_OBJECT;
			}
		}
	}
}

bool DlgRoleEditFilter::processPostedDataSpecific(const IPostedDataTarget::PostedData& data)
{
	wstring objectType;
	wstring userProperty;
	wstring groupProperty;
	wstring siteProperty;
	wstring value;
	
	wstring			taggerMatchMethod;
	set<wstring>	taggerMatchMethodSub;

	for (const auto& item : data)
	{
		if (item.first == g_VarObjectType)
		{
			ASSERT(objectType.empty());
			objectType = item.second;
		}
		else if (item.first == g_VarPropertyValue)
		{
			ASSERT(value.empty());
			value = item.second;
		}
		else if (item.first == g_VarPropertyNameGroup)
		{
			ASSERT(groupProperty.empty());
			groupProperty = item.second;
		}
		else if (item.first == g_VarPropertyNameSite)
		{
			ASSERT(siteProperty.empty());
			siteProperty = item.second;
		}
		else if (item.first == g_VarPropertyNameUser)
		{
			ASSERT(userProperty.empty());
			userProperty = item.second;
		}
		else if (item.first == g_VarTaggerMatchMethod)
		{
			ASSERT(taggerMatchMethod.empty());
			taggerMatchMethod = item.second;
		}
		else if (item.first == g_VarTaggerMatchMethodSub)
		{
			taggerMatchMethodSub.insert(item.second);
		}
	}

	m_Filter.m_ObjectType = objectType;

	if (g_VarPropertyNameGroup == objectType)
		m_Filter.m_Property = groupProperty;
	else if (g_VarPropertyNameSite == objectType)
		m_Filter.m_Property = siteProperty;
	else if (g_VarPropertyNameUser == objectType)
		m_Filter.m_Property = userProperty;
	else
		ASSERT(false);

	m_Filter.m_Value				= value;
	m_Filter.m_TaggerMatchMethod	=  RoleDelegationUtil::GetTaggerMatchMethod(Str::getLongFromString(taggerMatchMethod), 
																				taggerMatchMethodSub.find(g_VarTaggerMatchMethodSubNOT) != taggerMatchMethodSub.end(),
																				taggerMatchMethodSub.find(g_VarTaggerMatchMethodSubCASESENSITIVE) != taggerMatchMethodSub.end());

    return true;
}

wstring DlgRoleEditFilter::getDialogTitle() const
{
	return YtriaTranslate::Do(DlgRoleEditFilter_getDialogTitle_1, _YLOC("Scope Definition")).c_str();
}

int DlgRoleEditFilter::getMinimumHeight() const
{
	const auto it = g_Sizes.find(_YTEXT("min-height"));
	ASSERT(g_Sizes.end() != it);
	if (g_Sizes.end() != it)
		return it->second;
	return DlgRoleEditBase::getMinimumHeight();
}
