#pragma once

#include "DlgRoleEditBase.h"

#include "Sapio365Session.h"

class DlgRoleEditKey	: public DlgRoleEditBase
						, public YAdvancedHtmlView::IPostedURLTarget
{
public:
	DlgRoleEditKey(std::shared_ptr<const Sapio365Session> p_SessionForAdminConsent, CWnd* p_Parent);
	virtual ~DlgRoleEditKey();

	const RoleDelegationUtil::BusinessKey&	GetKey() const;
	void									SetKey(const RoleDelegationUtil::BusinessKey& p_Key);

	virtual bool OnNewPostedURL(const wstring& url) override;

protected:
	virtual bool								isSpecificReadOnly() const override;
	virtual void								generateJSONScriptDataSpecific(web::json::value& p_JsonValue) override;
	virtual bool								processPostedDataSpecific(const IPostedDataTarget::PostedData& data) override;
	virtual RoleDelegationUtil::BusinessData*	getBusinessData() override;
	virtual wstring								getDialogTitle() const override;
	virtual int									getMinimumHeight() const override;
	virtual bool								shouldAddCredentialsCreationButton(bool p_ButtonTop) const override;

private:
	void getAdminConsent(const wstring& p_Tenant, const wstring& p_AppID);

private:
	RoleDelegationUtil::BusinessKey m_Key;
	std::shared_ptr<const Sapio365Session> m_Session;

	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;

	static const wstring g_VarTenantName;
	static const wstring g_VarUltraAdminID;
	static const wstring g_VarUltraAdminSecret;
	static const wstring g_VarAdminLogin;
	static const wstring g_VarAdminPWD;

	static const wstring g_UAConsentURL;
	static const wstring g_AdminConsentURL;

	class ExternalFctsHandler;
};