#pragma once

class Sapio365Session;
class DlgDoubleProgressCommon;

class ManagerSQLandCloud
{
public:
	ManagerSQLandCloud();
	~ManagerSQLandCloud() = default;

	virtual void InitAfterSessionOrLicenseWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress) = 0;// p_Session used for Cosmos

	enum ErrorCode : uint32_t
	{
		CFG_NOERROR = 0,
		SQLCFG_ERROR,
		COSMOSCFG_ERROR,
		VERSION_ERROR,
	};

	bool		IsReadyToRun() const;
	wstring		GetLastError() const;
	void		SetError(const ErrorCode p_Error);
	ErrorCode	GetError() const;

private:
	ErrorCode m_ErrorCode;
};