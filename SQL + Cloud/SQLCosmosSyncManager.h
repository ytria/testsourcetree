#pragma once

#include "O365SQLiteEngine.h"
#include "YTimeDate.h"

namespace SqlCosmosSync
{
	const wstring g_SyncColumnNameTenant	= _YTEXT("Tenant");
	const wstring g_SyncColumnNameDatabase	= _YTEXT("Database_");
	const wstring g_SyncColumnNameTable		= _YTEXT("Table_");
	const wstring g_SyncColumnNameLastSync	= _YTEXT("LastSync");

	const wstring g_ParametersColumnNameName	= _YTEXT("Name");
	const wstring g_ParametersColumnNameValue	= _YTEXT("Value");

	const wstring g_ParameterNameCosmosAccount			= _YTEXT("Cosmos Account");
	const wstring g_ParameterNameCosmosSingleContainer	= _YTEXT("Cosmos Single Container");

	const wstring g_ParametersValueTrue		= _YTEXT("True");
	const wstring g_ParametersValueFalse	= _YTEXT("False");
}

class SQLCosmosSyncRecord : public SQLiteUtil::SQLRecord
{
public:
	SQLCosmosSyncRecord(const SQLiteUtil::Table& p_Table) : SQLiteUtil::SQLRecord(p_Table)
	{
		Configure();
	}

	void Configure() override
	{
		configureMember(SQLiteUtil::g_ColumnNameRowID,				&m_ID,				SQLRecord::MemberType::OPTINT64);
		configureMember(SqlCosmosSync::g_SyncColumnNameTenant,		&m_Tenant,			SQLRecord::MemberType::WSTRING);
		configureMember(SqlCosmosSync::g_SyncColumnNameDatabase,	&m_Database,		SQLRecord::MemberType::WSTRING);
		configureMember(SqlCosmosSync::g_SyncColumnNameTable,		&m_Table,			SQLRecord::MemberType::WSTRING);
		configureMember(SqlCosmosSync::g_SyncColumnNameLastSync,	&m_LastSyncDate,	SQLRecord::MemberType::DATE);
	}

	wstring		m_Tenant;
	wstring		m_Database;
	wstring		m_Table;
	YTimeDate	m_LastSyncDate;
};

class SQLCosmosParametersRecord : public SQLiteUtil::SQLRecord
{
public:
	SQLCosmosParametersRecord(const SQLiteUtil::Table& p_Table) : SQLiteUtil::SQLRecord(p_Table)
	{
		Configure();
	}

	void Configure() override
	{
		configureMember(SQLiteUtil::g_ColumnNameRowID,					&m_ID,		SQLRecord::MemberType::OPTINT64);
		configureMember(SqlCosmosSync::g_SyncColumnNameTenant,			&m_Tenant,	SQLRecord::MemberType::WSTRING);
		configureMember(SqlCosmosSync::g_ParametersColumnNameName,		&m_Name,	SQLRecord::MemberType::WSTRING);
		configureMember(SqlCosmosSync::g_ParametersColumnNameValue,		&m_Value,	SQLRecord::MemberType::WSTRING);
	}

	wstring	m_Tenant;
	wstring	m_Name;
	wstring	m_Value;
};

class SQLCosmosSyncManager : public O365SQLiteEngine
{
public:
	static SQLCosmosSyncManager& GetInstance();
	
	virtual ~SQLCosmosSyncManager() = default;

	virtual vector<SQLiteUtil::Table> GetTables() const override;
	
	YTimeDate	GetLastSyncDate(const wstring& p_Database, const wstring& p_Table, std::shared_ptr<const Sapio365Session> p_Session);
	bool		SetCloudSyncDate(const wstring& p_Database, const wstring& p_Table, std::shared_ptr<const Sapio365Session> p_Session, const YTimeDate& p_NewSyncDate);
	bool		ClearSyncDates(std::shared_ptr<const Sapio365Session> p_Session);

	boost::YOpt<wstring>	GetParameterValue(const wstring& p_ParameterName, std::shared_ptr<const Sapio365Session> p_Session);
	bool					SetParameterValue(const wstring& p_ParameterName, const wstring& p_ParmaeterValue, std::shared_ptr<const Sapio365Session> p_Session);
	bool					ManageCosmosSyncDates(std::shared_ptr<const Sapio365Session> p_Session);

	uint32_t GetVersion() const override
	{
		return g_CurrentVersion;
	}

private:
	SQLCosmosSyncManager();

	SQLiteUtil::Table m_Sync;
	SQLiteUtil::Table m_Parameters;

	static constexpr uint32_t g_CurrentVersion = 6;
};