#include "CosmosManager.h"

#include "AutomatedApp.h"

#include "MainFrame.h"
#include "Sapio365Session.h"
#include "YCallbackMessage.h"
#include "YCodeJockMessageBox.h"
#include "YEncryption.h"

const wstring CosmosManager::g_Separator	= _YTEXT("i*[YTRIA]~!");// never change this!!
const wstring CosmosManager::g_EMPTY		= _YTEXT("##EMPTY##");

CosmosManager::CosmosManager()
{
}

LicenseManager* CosmosManager::getLicenseManager() const
{
	auto app = dynamic_cast<AutomatedApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	return nullptr != app ? app->GetLicenseManager() : nullptr;
}

bool CosmosManager::IsValid(const CosmosConnector& p_cnx) const
{
	return IsValid(p_cnx.m_URL) && IsValid(p_cnx.m_MasterKey);
}

bool CosmosManager::IsValid(const wstring& p_cnxCredentialPart) const
{
	return !p_cnxCredentialPart.empty() && p_cnxCredentialPart != g_EMPTY;
}

CosmosManager::CosmosConnector CosmosManager::GetConnectorFromLicense(std::shared_ptr<Sapio365Session>& p_Session) const
{
	CosmosConnector cnx;
	cnx.m_CosmosSingleContainer = false;

	auto licensMgr = getLicenseManager();
	ASSERT(nullptr != licensMgr);
	if (nullptr != licensMgr && p_Session)
	{
		auto license = licensMgr->GetLicense(false);// do not use RefreshLicense to avoid unnecessary network calls; the cosmos info is not supposed to change frequently

		const auto kkrypt   = license.GetAgilityValue(LicenseUtil::g_AgilityC00);
		const auto kcnx		= license.GetAgilityValue(LicenseUtil::g_AgilityC99);
		if (IsValid(kcnx) && IsValid(kkrypt))
		{
			std::string dkrypt;
			auto dkryptOK = YEncryption::decode(MFCUtil::convertUNICODE_to_ASCII(kkrypt), dkrypt, getFirstKrypt(p_Session)) == 0;
			if (dkryptOK)
			{
				std::string dkryptcnx;
				dkryptOK = YEncryption::decode(MFCUtil::convertUNICODE_to_ASCII(kcnx), dkryptcnx, dkrypt + getSecondKrypt(p_Session)) == 0;
				ASSERT(dkryptOK);
				if (dkryptOK)
				{
					wstring cnxStr = MFCUtil::convertASCII_to_UNICODE(dkryptcnx);
					auto v = Str::explodeIntoTokenVector(cnxStr, g_Separator);
					ASSERT(v.size() == 2);
					if (v.size() == 2)
					{
						cnx.m_MasterKey				= v[0];
						cnx.m_URL					= v[1];
						cnx.m_CosmosSingleContainer	= license.IsFeatureSet(LicenseUtil::g_codeSapio365CosmosSingleContainer);
					}
				}
			}
			else
			{
				// TODO
				// most probable cause: user logged into a different organisation (e.g. multi-tenant license)
			}
		}
	}
	
	return cnx;
}

bool CosmosManager::SendCnxToCRM(const CosmosConnector& p_cnx, std::shared_ptr<Sapio365Session>& p_Session, const bool p_Clear) const
{
	bool rvSuccess = false;

	wstring krypt;
	wstring cnx;

	if (p_Clear)
	{
		cnx		= g_EMPTY;
		krypt	= g_EMPTY;
	}
	else
	{
		std::string skrypt;
		auto srandom = Str::MakeRandomString(48);
		auto kryptOK = YEncryption::encode(srandom, skrypt, getFirstKrypt(p_Session)) == 0;
		ASSERT(kryptOK);
		if (kryptOK)
		{
			krypt = MFCUtil::convertASCII_to_UNICODE(skrypt);

			std::string scnx;
			kryptOK = YEncryption::encode(MFCUtil::convertUNICODE_to_ASCII(p_cnx.m_MasterKey + g_Separator + p_cnx.m_URL), scnx, srandom + getSecondKrypt(p_Session)) == 0;
			ASSERT(kryptOK);
			if (kryptOK)
				cnx = MFCUtil::convertASCII_to_UNICODE(scnx);
		}
	}
	
	ASSERT(!krypt.empty());
	ASSERT(!cnx.empty());

	auto licensMgr = getLicenseManager();
	ASSERT(nullptr != licensMgr);
	if (nullptr != licensMgr && !krypt.empty() && !cnx.empty())
	{
		map<wstring, wstring> CRMdata;
		CRMdata[_YTEXT("KeyValidation")]	= p_cnx.m_KeyValidation;
		CRMdata[_YTEXT("K1")]				= cnx;
		CRMdata[_YTEXT("K2")]				= krypt;
		CRMdata[_YTEXT("K3")]				= p_Session->GetTenantName(true);

		const auto& rr = licensMgr->CRMStoreCosmosCnx(CRMdata);
		if (rr.IsError())
		{
			YCallbackMessage::DoSend([rr]()
			{
				YCodeJockMessageBox dlg (AfxGetApp()->m_pMainWnd,
										 DlgMessageBox::eIcon_Error, 
										 YtriaTranslate::Do(Application_error_2, _YLOC("Error")).c_str(), 
										 YtriaTranslate::Do(CosmosManager_SendCnxToCRM_3, _YLOC("A problem occurred while trying to store the data:\n%1"), rr.GetCRMError().c_str()).c_str(), 
										 YtriaTranslate::Do(AutomationWizardO365_RunFooter_6, _YLOC("Try Again")), 
										 { { IDOK, YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str() }} );
				dlg.DoModal();
			});
		}
		else
		{
			licensMgr->RefreshLicense();
			rvSuccess = true;
		}
	}

	return rvSuccess;
}

std::string CosmosManager::getFirstKrypt(std::shared_ptr<Sapio365Session>& p_Session) const
{
	std::string krypt;

	auto& session = p_Session;
	if (session)
	{
		auto org = session.get()->GetGraphCache().GetCachedOrganization();
		ASSERT(org.is_initialized());
		if (org.is_initialized())
		{
			// tossin' salad
			auto k2 = MFCUtil::convertUNICODE_to_ASCII((org.get().m_Id + org.get().m_Id).c_str());
			krypt = k2;
			size_t k = k2.size();
			for (auto it = k2.rbegin(); it != k2.rend(); it++)
			{
				auto c = *it;
				auto z = ((c + k) % 2 == 0 ? c + 1 : c + 6) % 128;
				krypt[(666*z) % krypt.size()] = z;
				k += c;
			}
		}
	}

	return krypt;
}

std::string CosmosManager::getSecondKrypt(std::shared_ptr<Sapio365Session>& p_Session) const
{
	std::string krypt;
	
	auto& session = p_Session;
	if (session)
	{
		auto org = session.get()->GetGraphCache().GetCachedOrganization();
		ASSERT(org.is_initialized());
		if (org.is_initialized())
		{
			// tossin' salad
			auto k2 = MFCUtil::convertUNICODE_to_ASCII((org.get().m_Id).c_str());
			krypt = k2;
			for (size_t k = 0; k < k2.size(); k++)
			{
				auto c = k2[k] + 69;
				auto z = ((c + k) % 3 == 0 ? c + 6 : c + 66) % 128;
				krypt[(77 + z) % krypt.size()] = z;
			}
		}
	}

	return krypt;
}

