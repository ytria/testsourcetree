#pragma once

#include "O365SQLiteEngine.h"
#include "SQLiteUtil.h"

class CosmosConnectorSQLite : public O365SQLiteEngine
{
public:

	static const wstring g_ColumnURL;
	static const wstring g_ColumnMasterKey;

	struct BusinessCosmos
	{
		wstring m_URL;
		wstring m_MasterKey;
	};
	
	class CosmosRecord : public SQLiteUtil::SQLRecord
	{
	public:
		CosmosRecord(const SQLiteUtil::Table& p_Table) : SQLRecord(p_Table)
		{
			configureMember(SQLiteUtil::g_ColumnNameRowID,	&m_ID,					SQLiteUtil::SQLRecord::MemberType::OPTINT64);
			configureMember(g_ColumnURL,					&m_Cosmos.m_URL,		SQLiteUtil::SQLRecord::MemberType::WSTRING);
			configureMember(g_ColumnMasterKey,				&m_Cosmos.m_MasterKey,	SQLiteUtil::SQLRecord::MemberType::WSTRING);
		}

		BusinessCosmos m_Cosmos;
	};

	CosmosConnectorSQLite();

	virtual vector<SQLiteUtil::Table> GetTables() const override;
	
	BusinessCosmos GetConnector();

	void AddTestData();// TODO REMOVE

private:
	SQLiteUtil::Table m_CosmosKeys;
};