#pragma once

#include "Document.h"
#include "RESTUtils.h"
#include "SQLiteUtil.h"
#include "CosmosDbAccount.h"

class YAdvancedHtmlView;

namespace CosmosUtil
{
	using SQLupdateData = map<SQLiteUtil::Table, vector<Cosmos::Document>>;

	void CreateCosmosDbAccount(const shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent, YAdvancedHtmlView* p_HtmlView);
	TaskWrapper<void> DeleteCosmosDbAccount(const shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent, YAdvancedHtmlView* p_HtmlView);
	TaskWrapper<void> SelectCosmosDbAccount(const shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent, YAdvancedHtmlView* p_HtmlView);

	namespace detail
	{
		wstring GetDateTimeNow();

		// All this methods are blocking.
		void DeleteCosmosDbAccountImpl(const shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent, YAdvancedHtmlView* p_HtmlView);
		void SelectCosmosDbAccountImpl(const shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent, YAdvancedHtmlView* p_HtmlView);
		bool ShowCosmosDbDeleteDialog(const wstring& p_SubscriptionId,
			const wstring& p_ResGroup,
			const shared_ptr<Sapio365Session>& p_Session,
			const vector<Azure::CosmosDbAccount>& p_Accounts,
			CWnd* p_Parent,
			YAdvancedHtmlView* p_HtmlView);
		bool ShowCosmosDbSelectDialog(const wstring& p_SubscriptionId,
			const wstring& p_ResGroup,
			const shared_ptr<Sapio365Session>& p_Session,
			const vector<Azure::CosmosDbAccount>& p_Accounts,
			CWnd* p_Parent,
			YAdvancedHtmlView* p_HtmlView);
	}
}
