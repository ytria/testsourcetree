#pragma once

#include "CosmosManager.h"
#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "..\Resource.h"
#include "YAdvancedHtmlView.h"

class DlgEnterCosmosCNX : public ResizableDialog
						, public YAdvancedHtmlView::IPostedDataTarget
{
public:
	DlgEnterCosmosCNX(std::shared_ptr<Sapio365Session> p_SessionForConnectionTesting, bool p_IsCosmosSetForAnotherTenant, CWnd* p_Parent);
	virtual ~DlgEnterCosmosCNX();

	enum { IDD = IDD_DLG_ENTER_COSMOS_CNX };

	const CosmosManager::CosmosConnector& GetConnector() const;

	/* YAdvancedHtmlView::IPostedDataTarget */
	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data) override;

	bool NewConnectorEntered() const;
	bool IsClear() const;

	using OnOkValidation = std::function<bool(DlgEnterCosmosCNX&)>;
	void SetOnOkValidation(OnOkValidation p_OnOkValid);

protected:
	virtual BOOL OnInitDialogSpecificResizable() override;

	ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);

	CosmosManager::CosmosConnector m_Connector;
	bool m_Clear;
	bool m_IsCosmosSetForAnotherTenant;

	static const wstring g_VarCosmosURI;
	static const wstring g_VarCosmosKey;
	static const wstring g_VarYValidationKey;
	static const wstring g_VarOKButton;
	static const wstring g_VarCancelButton;
	static const wstring g_VarClearButton;

	OnOkValidation m_OnOkValidation;

	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;
};
