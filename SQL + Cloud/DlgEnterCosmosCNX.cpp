#include "DlgEnterCosmosCNX.h"

#include "CosmosUtil.h"
#include "CRMpipe.h"
#include "JSONUtil.h"
#include "ListDbsRequester.h"
#include "Product.h"
#include "YCodeJockMessageBox.h"

namespace
{
	class ExternalFctsHandler : public CCmdTarget
	{
	public:
		static wstring g_DefaultKeyValue;

		ExternalFctsHandler(CWnd* p_Parent, std::shared_ptr<Sapio365Session> p_Session, bool p_IsCosmosSetForOtherTenant, YAdvancedHtmlView* p_HtmlView)
			: m_Parent(p_Parent)
			, m_Session(p_Session)
			, m_HtmlView(p_HtmlView)
			, m_IsCosmosSetForOtherTenant(p_IsCosmosSetForOtherTenant)
		{
			EnableAutomation();

			if (g_DefaultKeyValue.empty())
				g_DefaultKeyValue = _T("(Application secret password not shown when saved)");
		}

		BSTR testConnection(LPCWSTR p_Uri, LPCWSTR p_Key, LPCWSTR p_OkVerbose)
		{
			_bstr_t rv = "BAD";

			if (!m_IsCosmosSetForOtherTenant)
			{
				auto backupKey = m_Session->GetCosmosDbMasterKey();
				auto backupURI = m_Session->GetCosmosDbSqlUri();
				if (p_Key != g_DefaultKeyValue)
					m_Session->SetCosmosDbMasterKey(p_Key);
				m_Session->SetCosmosDbSqlUri(p_Uri);

				wstring error;
				ASSERT(m_Session->IsReadyForCosmos());
				if (m_Session->IsReadyForCosmos())
				{
					Cosmos::ListDbsRequester dbRequester;
					try
					{
						dbRequester.Send(m_Session, YtriaTaskData()).GetTask().wait();
					}
					catch (const RestException & e)
					{
						error = e.WhatUnicode();
					}
					catch (const std::exception & e)
					{
						error = Str::convertFromUTF8(e.what());
					}
				}

				m_Session->SetCosmosDbMasterKey(backupKey);
				m_Session->SetCosmosDbSqlUri(backupURI);

				const auto success = error.empty();

				if (success)
				{
					// This is because the function is called in 2 places
					// and we want a dialog on OK only if when called from the button
					wstring isOkVerbose(p_OkVerbose);
					if (!isOkVerbose.empty()) {
						YCodeJockMessageBox dlg(m_Parent, DlgMessageBox::eIcon::eIcon_Information, _T("Test passed"), _T("Successfully connected to Cosmos DB."), _YTEXT(""), { { IDOK, _YTEXT("OK") } });
						dlg.DoModal();
					}
					rv = "OK";
				}
				else
				{
					YCodeJockMessageBox dlg(m_Parent, DlgMessageBox::eIcon::eIcon_Error, _T("Test failed"), _T("Unable to connect to Cosmos DB."), error, { { IDOK, _YTEXT("OK") } });
					dlg.DoModal();
				}
			}
			else
				rv = "OK";


			// A value is expected
			return rv;
		}

		BSTR createDbAccount()
		{
			_bstr_t rv = "";
			CosmosUtil::CreateCosmosDbAccount(m_Session, m_Parent, m_HtmlView);
			return rv;
		}

		BSTR deleteOldAccount()
		{
			_bstr_t rv = "";
			m_Parent->EnableWindow(FALSE);
			CosmosUtil::DeleteCosmosDbAccount(m_Session, m_Parent, m_HtmlView).Then([parent = m_Parent]()
				{
					parent->EnableWindow(TRUE);
				});
			return rv;
		}

		BSTR selectExistingAccount()
		{
			_bstr_t rv = "";
			m_Parent->EnableWindow(FALSE);
			CosmosUtil::SelectCosmosDbAccount(m_Session, m_Parent, m_HtmlView).Then([parent = m_Parent]()
				{
					parent->EnableWindow(TRUE);
				});
			return rv;
		}

	private:
		DECLARE_DISPATCH_MAP()

		std::shared_ptr<Sapio365Session> m_Session;
		CWnd* m_Parent;
		YAdvancedHtmlView* m_HtmlView;
		bool m_IsCosmosSetForOtherTenant;
	};

	wstring ExternalFctsHandler::g_DefaultKeyValue;

	BEGIN_DISPATCH_MAP(ExternalFctsHandler, CCmdTarget)
		DISP_FUNCTION(ExternalFctsHandler, "testConnect", testConnection, VT_BSTR, VTS_WBSTR VTS_WBSTR VTS_WBSTR)
		DISP_FUNCTION(ExternalFctsHandler, "createNewAccount", createDbAccount, VT_BSTR, VTS_NONE)
		DISP_FUNCTION(ExternalFctsHandler, "deleteOldAccount", deleteOldAccount, VT_BSTR, VTS_NONE)
		DISP_FUNCTION(ExternalFctsHandler, "selectExistingAccount", selectExistingAccount, VT_BSTR, VTS_NONE)
	END_DISPATCH_MAP()
}

const wstring DlgEnterCosmosCNX::g_VarCosmosURI = _YTEXT("COSMOS_URI");
const wstring DlgEnterCosmosCNX::g_VarCosmosKey = _YTEXT("COSMOS_KEY");
const wstring DlgEnterCosmosCNX::g_VarYValidationKey = _YTEXT("YVALIDATION_KEY");
const wstring DlgEnterCosmosCNX::g_VarOKButton = _YTEXT("ok");
const wstring DlgEnterCosmosCNX::g_VarCancelButton = _YTEXT("cancel");
const wstring DlgEnterCosmosCNX::g_VarClearButton = _YTEXT("clear");

std::map<wstring, int, Str::keyLessInsensitive> DlgEnterCosmosCNX::g_Sizes;

DlgEnterCosmosCNX::DlgEnterCosmosCNX(std::shared_ptr<Sapio365Session> p_SessionForConnectionTesting, bool p_IsCosmosSetForAnotherTenant, CWnd* p_Parent)
	: ResizableDialog(IDD, p_Parent)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_Clear(false)
	, m_IsCosmosSetForAnotherTenant(p_IsCosmosSetForAnotherTenant)
{
	m_HtmlView->SetExternal(std::make_shared<ExternalFctsHandler>(this, p_SessionForConnectionTesting, m_IsCosmosSetForAnotherTenant, m_HtmlView.get()));

	// Load hbs sizes
	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_COSMOSDBINFO_SIZE, g_Sizes);
		ASSERT(success);
	}

	if (p_SessionForConnectionTesting && !p_SessionForConnectionTesting->GetCosmosDbSqlUri().empty())
	{
		m_Connector.m_URL = p_SessionForConnectionTesting->GetCosmosDbSqlUri();
		m_Connector.m_MasterKey = p_SessionForConnectionTesting->GetCosmosDbMasterKey();
	}
}

DlgEnterCosmosCNX::~DlgEnterCosmosCNX()
{
}

void DlgEnterCosmosCNX::generateJSONScriptData(wstring& p_Output)
{
	json::value main = json::value::object({
		{ _YTEXT("id"), json::value::string(_YTEXT("DlgEnterCosmosCNX")) },
		{ _YTEXT("method"), json::value::string(_YTEXT("POST")) },
		{ _YTEXT("action"), json::value::string(_YTEXT("#")) },
		});

	json::value items = json::value::array();

	bool alreadySet = !m_Connector.m_URL.empty() || m_IsCosmosSetForAnotherTenant;
	bool hasKey = !m_Connector.m_MasterKey.empty();

	wstring cosmosUri = _YTEXT("https://youraccount.documents.azure.com:443/");
	wstring cosmosPrimaryKey = YtriaTranslate::Do(DlgEnterCosmosCNX_generateJSONScriptData_12, _YLOC("Read-Write Primary Key"));

	if (m_IsCosmosSetForAnotherTenant)
		cosmosUri = cosmosPrimaryKey = _T("[ - Set for another Office365 tenant - ]");

	items[0] =
		JSON_BEGIN_OBJECT
		JSON_BEGIN_PAIR
			_YTEXT("hbs"), JSON_STRING(_YTEXT("cosmosDbInfo"))
		JSON_END_PAIR,

		JSON_BEGIN_PAIR
			_YTEXT("introTextOne"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgDeleteCosmosDbAccount_generateJSONScriptData_1, _YLOC("This requires an active Azure account. The account must be set in your Microsoft Azure environment.\n\nYou can select an existing CosmosDB account or create a new one:\n-Select an existing account from a list by clicking Select existing Cosmos DB account...\n-Create a new account by clicking Create New Cosmos DB account...\n\nNote: Before you begin you must have an Azure account and an active subscription with at least one resource group.")).c_str()))
		JSON_END_PAIR,
		/*JSON_BEGIN_PAIR
			_YTEXT("introTextTwo"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgEnterCosmosCNX_generateJSONScriptData_2, _YLOC("If you haven't created on yet, please go to the ")).c_str()))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("linkRegisterText"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgDeleteCosmosDbAccount_generateJSONScriptData_3, _YLOC("Azure Cosmos DB portal")).c_str()))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("linkRegister"), JSON_STRING(_YTEXT("https://portal.azure.com/#blade/HubsExtension/Resources/resourceType/Microsoft.DocumentDb%2FdatabaseAccounts"))
		JSON_END_PAIR,*/

		JSON_BEGIN_PAIR
			_YTEXT("introTextThree"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_4, _YLOC("Need help? See ")).c_str()))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("linkTextThree"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSelectSubscriptionAndResourceGroup_generateJSONScriptData_5, _YLOC("How to create an Azure Cosmos DB Account")).c_str()))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("linkThree"), JSON_STRING(Product::getURLHelpWebSitePage(_YTEXT("Help/cosmosdb-setup")))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("createNewButton"), JSON_STRING(YtriaTranslate::Do(DlgEnterCosmosCNX_generateJSONScriptData_6, _YLOC("Create New Cosmos DB Account...")).c_str())
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("selectExistButton"), JSON_STRING(_T("Select existing Cosmos DB account"))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("deleteOldAccount"), JSON_STRING(YtriaTranslate::Do(DlgSelectSubscriptionAndResourceGroup_generateJSONScriptData_8, _YLOC("Delete accounts created by sapio365 ")).c_str())
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("deleteOldAccountLink"), JSON_STRING(_T("click here"))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("uriField"), JSON_STRING(g_VarCosmosURI)
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("uriLabel"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgEnterCosmosCNX_generateJSONScriptData_9, _YLOC("URI:")).c_str()))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("uriPlaceholder"), JSON_STRING(_YTEXT("https://youraccount.documents.azure.com:443/"))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("uriValue"), JSON_STRING(m_IsCosmosSetForAnotherTenant ? cosmosUri : m_Connector.m_URL)
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("uriPopup"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgEnterCosmosCNX_generateJSONScriptData_10, _YLOC("Enter the Read-Write URI of your Cosmos DB account")).c_str()))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("uriReadOnly"), JSON_STRING(_YTEXT("X"))
		JSON_END_PAIR,
		/* You can edit the Cosmos DB URI ONLY when in Sandbox */
		JSON_BEGIN_PAIR
			_YTEXT("uriNoUnLock"), JSON_STRING(CRMpipe().IsFeatureSandboxed() ? _YTEXT("") : _YTEXT("X"))
		JSON_END_PAIR,

		JSON_BEGIN_PAIR
			_YTEXT("keyField"), JSON_STRING(g_VarCosmosKey)
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("keyLabel"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgEnterCosmosCNX_generateJSONScriptData_11, _YLOC("Primary Key:")).c_str()))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("keyPlaceholder"), JSON_STRING(YtriaTranslate::Do(DlgEnterCosmosCNX_generateJSONScriptData_12, _YLOC("Read-Write Primary Key")))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("keyValue"), JSON_STRING(hasKey ? ExternalFctsHandler::g_DefaultKeyValue : cosmosPrimaryKey)
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("keyPopup"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgEnterCosmosCNX_generateJSONScriptData_13, _YLOC("Enter the Read-Write Primary Key of your Cosmos DB account.")).c_str()))
		JSON_END_PAIR,

		JSON_BEGIN_PAIR
			_YTEXT("warningInfoIcon"), JSON_STRING(_YTEXT("fas fa-exclamation-square"))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("warningInfoOne"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgEnterCosmosCNX_generateJSONScriptData_14, _YLOC("The URI and the Primary Key will be encrypted and placed in the current License Key.")).c_str()))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("warningInfoTwo"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_2, _YLOC("This encryption uses information unique to your tenant, Ytria does not have access to this information.")).c_str()))
		JSON_END_PAIR,

		JSON_BEGIN_PAIR
			_YTEXT("testConnectButton"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgEnterCosmosCNX_generateJSONScriptData_16, _YLOC("Test Connection")).c_str()))
		JSON_END_PAIR,

		JSON_BEGIN_PAIR
			_YTEXT("keyValidationField"), JSON_STRING(g_VarYValidationKey)
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("keyValidationIcon"), JSON_STRING(_YTEXT("fas fa-sign-in"))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("keyValidationMsg"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_9, _YLOC("To verify you are the authorised user, please enter the subscription code (Sxxxxxxxx) for this license.")).c_str()))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("keyValidationSubMsg"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_20, _YLOC("Your subscription code is located on your invoice below the cleverbridge reference number.")).c_str()))
		JSON_END_PAIR,
		JSON_BEGIN_PAIR
			_YTEXT("keyValidationPopup"), JSON_STRING(_YTEXT("Sxxxxxxxx"))
		JSON_END_PAIR,

		JSON_BEGIN_PAIR
			_YTEXT("labelFooter"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_YTEXT("")))
		JSON_END_PAIR,
		JSON_END_OBJECT;

	if (alreadySet)
	{
		// There is a Cosmos DB set, so we add the Clear button 
		items[1] =
			JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
			_YTEXT("hbs"), JSON_STRING(_YTEXT("Button"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
			_YTEXT("class"), JSON_STRING(_YTEXT("hasPosLeft")) // This is required in conjunction with class=isPosLeft hereafter
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
			_YTEXT("choices"),
			JSON_BEGIN_ARRAY
			JSON_BEGIN_OBJECT
			JSON_PAIR_KEY(_YTEXT("type"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
			JSON_PAIR_KEY(_YTEXT("label"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(CacheGrid_InitializeCommands_381, _YLOC("Clear")).c_str()))),
			JSON_PAIR_KEY(_YTEXT("attribute"))      JSON_PAIR_VALUE(JSON_STRING(_YTEXT("clear"))),
			JSON_PAIR_KEY(_YTEXT("name"))			JSON_PAIR_VALUE(JSON_STRING(g_VarClearButton)),
			JSON_PAIR_KEY(_YTEXT("class"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("isPosLeft"))), // needs class=hasPosLeft to work
			JSON_END_OBJECT,
			JSON_BEGIN_OBJECT
			JSON_PAIR_KEY(_YTEXT("type"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
			JSON_PAIR_KEY(_YTEXT("label"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str()))),
			JSON_PAIR_KEY(_YTEXT("attribute"))      JSON_PAIR_VALUE(JSON_STRING(_YTEXT("post"))),
			JSON_PAIR_KEY(_YTEXT("name"))			JSON_PAIR_VALUE(JSON_STRING(g_VarOKButton)),
			JSON_PAIR_KEY(_YTEXT("class"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("is-primary"))),
			JSON_END_OBJECT,
			JSON_BEGIN_OBJECT
			JSON_PAIR_KEY(_YTEXT("type"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
			JSON_PAIR_KEY(_YTEXT("label"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str()))),
			JSON_PAIR_KEY(_YTEXT("attribute"))      JSON_PAIR_VALUE(JSON_STRING(_YTEXT("cancel"))),
			JSON_PAIR_KEY(_YTEXT("name"))			JSON_PAIR_VALUE(JSON_STRING(g_VarCancelButton)),
			JSON_END_OBJECT,
			JSON_END_ARRAY
			JSON_END_PAIR
			JSON_END_OBJECT;

	}
	else
	{
		// There is no cosmos DB set yet, so No Clear button
		items[1] =
			JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
			_YTEXT("hbs"), JSON_STRING(_YTEXT("Button"))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
			_YTEXT("choices"),
			JSON_BEGIN_ARRAY
			JSON_BEGIN_OBJECT
			JSON_PAIR_KEY(_YTEXT("type"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
			JSON_PAIR_KEY(_YTEXT("label"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str()))),
			JSON_PAIR_KEY(_YTEXT("attribute"))      JSON_PAIR_VALUE(JSON_STRING(_YTEXT("post"))),
			JSON_PAIR_KEY(_YTEXT("name"))			JSON_PAIR_VALUE(JSON_STRING(g_VarOKButton)),
			JSON_PAIR_KEY(_YTEXT("class"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("is-primary"))),
			JSON_END_OBJECT,
			JSON_BEGIN_OBJECT
			JSON_PAIR_KEY(_YTEXT("type"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
			JSON_PAIR_KEY(_YTEXT("label"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str()))),
			JSON_PAIR_KEY(_YTEXT("attribute"))      JSON_PAIR_VALUE(JSON_STRING(_YTEXT("cancel"))),
			JSON_PAIR_KEY(_YTEXT("name"))			JSON_PAIR_VALUE(JSON_STRING(g_VarCancelButton)),
			JSON_END_OBJECT,
			JSON_END_ARRAY
			JSON_END_PAIR
			JSON_END_OBJECT;
	}


	const json::value root = json::value::object({
		{ _YTEXT("main"), main },
		{ _YTEXT("items"), items }
	});

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}

bool DlgEnterCosmosCNX::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	static const UINT invalidOk{ 0x666 };
	static const UINT uninitializedButtonId{ 0xffffffff };

	UINT clickedButtonId = uninitializedButtonId;

	m_Clear = false;
	for (const auto& item : data)
	{
		if (item.first == g_VarCosmosURI)
			m_Connector.m_URL = item.second;
		else if (item.first == g_VarCosmosKey)
			m_Connector.m_MasterKey = item.second;
		else if (item.first == g_VarYValidationKey)
			m_Connector.m_KeyValidation = item.second;
		else if (item.first == g_VarOKButton)
			clickedButtonId = IDOK;
		else if (item.first == g_VarCancelButton)
			clickedButtonId = IDCANCEL;
		else if (item.first == g_VarClearButton)
		{
			YCodeJockMessageBox dlg(this,
				DlgMessageBox::eIcon_ExclamationWarning,
				YtriaTranslate::Do(DlgEnterCosmosCNX_OnNewPostedData_1, _YLOC("Remove Cosmos credentials?")).c_str(),
				YtriaTranslate::Do(DlgEnterCosmosCNX_OnNewPostedData_2, _YLOC("Are you sure you want to disconnect Azure Cosmos DB for all sapio365 users?")).c_str(),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
			if (IDOK == dlg.DoModal())
			{
				clickedButtonId = IDOK;
				m_Clear = true;
			}
		}
	}

	if (clickedButtonId == IDOK && m_OnOkValidation && !m_OnOkValidation(*this))
		clickedButtonId = invalidOk;

	if (uninitializedButtonId != clickedButtonId)
	{
		if (invalidOk != clickedButtonId)
			endDialogFixed(clickedButtonId);
		return false;
	}

	ASSERT(false);
	return true;
}

bool DlgEnterCosmosCNX::IsClear() const
{
	return m_Clear;
}

void DlgEnterCosmosCNX::SetOnOkValidation(DlgEnterCosmosCNX::OnOkValidation p_OnOkValid)
{
	m_OnOkValidation = p_OnOkValid;
}

bool DlgEnterCosmosCNX::NewConnectorEntered() const
{
	return ExternalFctsHandler::g_DefaultKeyValue != m_Connector.m_MasterKey;// to make an explicit change (e.g. after typo in URI), retype the master key - to remove connector from license, clear URI
}

BOOL DlgEnterCosmosCNX::OnInitDialogSpecificResizable()
{
	SetWindowText(YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_13, _YLOC("Set Cosmos DB Connection Info")).c_str());

	CWnd* pCtrl = GetDlgItem(IDC_GRID_PLACEHOLDER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, NULL);

	m_HtmlView->SetLinkOpeningPolicy(true, true);
	m_HtmlView->SetPostedDataTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
	{
		{ _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
		,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-cosmosDbInfo.js"), _YTEXT("") } } // This file is in resources
		,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("cosmosDbInfo.js"), _YTEXT("") } }, // This file is in resources
	}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("cosmosDbInfo.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-height"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-width"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);

	BlockVResize(true);

	return TRUE;
}

const CosmosManager::CosmosConnector& DlgEnterCosmosCNX::GetConnector() const
{
	return m_Connector;
}
