#pragma once

#include "IDlgSpecialExtAdminJsonGenerator.h"

class Sapio365Session;

class DlgSpecialExtAdminManageElevatedJsonGenerator : public IDlgSpecialExtAdminJsonGenerator
{
public:
	DlgSpecialExtAdminManageElevatedJsonGenerator(const std::shared_ptr<Sapio365Session>& p_Session, bool p_HasDowngradeButton = true);
	web::json::value Generate(const DlgSpecialExtAdminAccess& p_Dlg) override;

private:
	bool m_HasDowngradeButton;
	std::shared_ptr<Sapio365Session> m_Session;
};

