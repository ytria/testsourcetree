#include "MessageAttachmentDeletion.h"

#include "ActivityLoggerUtil.h"
#include "GridMessages.h"

MessageAttachmentDeletion::MessageAttachmentDeletion(GridMessages& p_Grid, const AttachmentInfo& p_Info)  :
    ISubItemDeletion(	p_Grid,
						SubItemKey(p_Info.UserOrGroupID(), p_Info.EventOrMessageOrConversationID(), p_Info.AttachmentID()),
						p_Grid.m_ColAttachmentId,
						p_Grid.m_ColAttachmentId,
						p_Grid.GetName(p_Info.m_Row)),
    m_GridMessages(p_Grid),
    m_AttachmentInfo(p_Info)
{
}

bool MessageAttachmentDeletion::IsCorrespondingRow(GridBackendRow* p_Row) const
{
    bool result = false;
    if (!IsCollapsedRow(p_Row))
    {
        // Exploded row
        if (p_Row->GetField(m_GridMessages.GetColMetaID()).HasValue() &&
			p_Row->GetField(m_GridMessages.GetColId()).HasValue() &&
            p_Row->GetField(m_GridMessages.m_ColAttachmentId).HasValue())
        {
            if (PooledString(p_Row->GetField(m_GridMessages.GetColMetaID()).GetValueStr()) == m_AttachmentInfo.UserOrGroupID() &&
                PooledString(p_Row->GetField(m_GridMessages.GetColId()).GetValueStr()) == m_AttachmentInfo.EventOrMessageOrConversationID() &&
                PooledString(p_Row->GetField(m_GridMessages.m_ColAttachmentId).GetValueStr()) == m_AttachmentInfo.AttachmentID())
            {
                result = true;
            }
        }
    }
    else
    {
        // Collapsed row
        if (p_Row->GetField(m_GridMessages.GetColMetaID()).HasValue() &&
            p_Row->GetField(m_GridMessages.GetColId()).HasValue())
        {
            if (PooledString(p_Row->GetField(m_GridMessages.GetColMetaID()).GetValueStr()) == m_AttachmentInfo.UserOrGroupID() &&
                PooledString(p_Row->GetField(m_GridMessages.GetColId()).GetValueStr()) == m_AttachmentInfo.EventOrMessageOrConversationID())
            {
                // As it's about a deletion, if the row is collapsed, even though we can't find the attachment id,
                // it's the still one (with UserOrGroupID() and EventOrMessageOrConversationID()).
                // No need to check in multivalue.
                result = true;

                //bool foundValue = false;
                //if (p_Row->GetField(m_GridMessages.m_ColAttachmentId).HasValue())
                //{
                //    std::vector<PooledString>* attachmentIds = p_Row->GetField(m_GridMessages.m_ColAttachmentId).GetValuesMulti<PooledString>();
                //    if (nullptr != attachmentIds)
                //    {
                //        for (const auto& attachId : *attachmentIds)
                //        {
                //            if (attachId == m_AttachmentInfo.AttachmentID())
                //            {
                //                foundValue = true;
                //                break;
                //             }
                //        }
                //    }
                //    else if (PooledString(p_Row->GetField(m_GridMessages.m_ColAttachmentId).GetValueStr()) == m_AttachmentInfo.AttachmentID()) // Row with only one attachment
                //    {
                //         foundValue = true;
                //    }

                //    // The only case where we can fail to find the attachment ID is when we deleted the last attachment, i.e. no value in the field.
                //    ASSERT(foundValue);
                //}

                //// In case of attachment deletion, if the last attachment has been deleted, we can't find the attachment id but the row is the still one.
                //result = foundValue;
            }
        }
    }

    return result;
}

vector<Modification::ModificationLog> MessageAttachmentDeletion::GetModificationLogs() const
{
	return { ModificationLog(getPk(), GetObjectName(), ActivityLoggerUtil::g_ActionDelete, _YTEXT("Attachment"), GetState()) };
}

GridBackendRow* MessageAttachmentDeletion::GetMainRowIfCollapsed() const
{
	vector<GridBackendRow*> rows;
	m_GridMessages.GetRowsByCriteria(rows, [this](GridBackendRow* p_Row)
		{
			const auto metaId = p_Row->GetField(m_GridMessages.GetColMetaID()).GetValueStr();
			const auto id = p_Row->GetField(m_GridMessages.GetColId()).GetValueStr();
			return nullptr != metaId && m_AttachmentInfo.UserOrGroupID() == metaId
				&& nullptr != id && m_AttachmentInfo.EventOrMessageOrConversationID() == id;
		});

	if (rows.size() == 1 && IsCollapsedRow(rows.front()))
		return rows.front();

	return nullptr;
}
