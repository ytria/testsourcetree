#pragma once

#include "IActionCommand.h"

#include "EducationUser.h"

struct AutomationSetup;
class AutomationAction;
class BusinessObjectManager;
class FrameEduUsers;
class LicenseContext;

class CommandShowEduUsers : public IActionCommand
{
public:
	CommandShowEduUsers(FrameEduUsers& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, bool p_IsUpdate);
	CommandShowEduUsers(const vector<EducationUser>& p_Users, FrameEduUsers& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, bool p_IsUpdate);

protected:
	void ExecuteImpl() const override;

	AutomationSetup& m_AutoSetup;
	FrameEduUsers& m_Frame;
	const LicenseContext& m_LicenseCtx;
	AutomationAction* m_AutoAction;
	bool m_IsUpdate;
	vector<EducationUser> m_Users;
};

