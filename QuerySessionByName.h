#pragma once

#include "ISqlQuery.h"
#include "SessionIdentifier.h"

class O365SQLiteEngine;

// Get the first session matching the criteria, p_SessionEmailOrAppIdOrDisplayName being compared to EmailOrAppId and DisplayName
class QuerySessionByName : public ISqlQuery
{
public:
	QuerySessionByName(O365SQLiteEngine& p_Engine, const wstring& p_SessionEmailOrAppIdOrDisplayName, const wstring& p_SessionType, const int64_t& p_RoleId);
	void Run() override;

	const wstring& GetEmailOrAppId() const;

private:
	O365SQLiteEngine& m_Engine;
	const wstring m_SessionEmailOrAppIdOrDisplayName;
	const wstring m_SessionType;
	const int64_t m_RoleId;

	wstring m_EmailOrAppId;
};

