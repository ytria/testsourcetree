#include "MailboxPermissionsModification.h"
#include "GridMailboxPermissions.h"
#include "AccessRightsFormatter.h"

const wchar_t* MailboxPermissionsModification::g_Sep = _YTEXT(",");

MailboxPermissionsModification::MailboxPermissionsModification(GridMailboxPermissions& p_Grid, const row_pk_t& p_Row, const wstring& p_RightsToAdd, const wstring& p_RightsToRemove)
	: RowModification(p_Grid, p_Row),
	m_Grid(p_Grid),
	m_RightsToAdd(p_RightsToAdd),
	m_RightsToRemove(p_RightsToRemove)
{
	auto& grid = m_Grid.get();
	auto row = grid.GetRowIndex().GetExistingRow(p_Row);
	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		m_OldAccessRights = grid.GetAccessRights(*row);
		grid.SetAccessRights(*row, GetCombinedAccessRightsString(m_OldAccessRights));
		grid.OnRowModified(row);
	}
}

void MailboxPermissionsModification::RefreshState()
{
	auto permissionsRow = m_Grid.get().GetRowIndex().GetExistingRow(GetRowKey());
	ASSERT(nullptr != permissionsRow);
	if (nullptr == permissionsRow)
		return;

}

vector<Modification::ModificationLog> MailboxPermissionsModification::GetModificationLogs() const
{
	ASSERT(false); // TODO
	return {};
}

wstring MailboxPermissionsModification::GetCombinedAccessRightsString(const wstring& p_Current) const
{
	wstring result;

	vector<wstring> current = Str::explodeIntoVector(p_Current, g_Sep, false, true);

	current.erase(std::remove_if(begin(current), end(current), [toRemove = m_RightsToRemove](const wstring& p_Str) {
		return toRemove.find(p_Str) != wstring::npos;
	}), end(current));

	vector<wstring> toAdd = Str::explodeIntoVector(m_RightsToAdd, g_Sep, false, true);
	for (const auto& item : toAdd)
	{
		if (p_Current.find(item) == wstring::npos)
			current.push_back(item);
	}
	
	AccessRightsFormatter sorter(Str::implode(current, g_Sep));
	return sorter.Format();
}

void MailboxPermissionsModification::AddToList(wstring& p_List, const wstring& p_ToAdd)
{
	vector<wstring> list = Str::explodeIntoVector(p_List, g_Sep, false, true);
	list.push_back(p_ToAdd);

	p_List = AccessRightsFormatter(Str::implode(list, g_Sep)).Format();
}

void MailboxPermissionsModification::RemoveFromList(wstring& p_List, const wstring& p_ToRemove)
{
	vector<wstring> list = Str::explodeIntoVector(p_List, g_Sep, false, true);
	list.erase(std::remove(list.begin(), list.end(), p_ToRemove), list.end());

	p_List = AccessRightsFormatter(Str::implode(list, g_Sep)).Format();
}

void MailboxPermissionsModification::Merge(const MailboxPermissionsModification& p_Mod)
{
	const row_pk_t& rowKey = p_Mod.GetRowKey();

	vector<wstring> incomingToAdds = Str::explodeIntoVector(p_Mod.m_RightsToAdd, g_Sep, false, true);
	vector<wstring> incomingToRemove = Str::explodeIntoVector(p_Mod.m_RightsToRemove, g_Sep, false, true);

	for (const auto& incomingAdd : incomingToAdds)
	{
		auto ittInOwnAddList = m_RightsToAdd.find(incomingAdd);
		auto ittInOwnRemoveList = m_RightsToRemove.find(incomingAdd);
		if (ittInOwnAddList == wstring::npos && ittInOwnRemoveList == wstring::npos)
			AddToList(m_RightsToAdd, incomingAdd);
		else if (ittInOwnAddList == wstring::npos && ittInOwnRemoveList != wstring::npos)
			RemoveFromList(m_RightsToRemove, incomingAdd);
	}

	for (const auto& incomingRemove : incomingToRemove)
	{
		auto ittInOwnAddList = m_RightsToAdd.find(incomingRemove);
		auto ittInOwnRemoveList = m_RightsToRemove.find(incomingRemove);
		if (ittInOwnRemoveList == wstring::npos && ittInOwnAddList == wstring::npos)
			AddToList(m_RightsToRemove, incomingRemove);
		else if (ittInOwnRemoveList == wstring::npos && ittInOwnAddList != wstring::npos)
			RemoveFromList(m_RightsToAdd, incomingRemove);
	}
}

void MailboxPermissionsModification::Revert()
{
	auto& grid = m_Grid.get();
	auto row = grid.GetRowIndex().GetExistingRow(GetRowKey());
	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		grid.SetAccessRights(*row, m_OldAccessRights);
		grid.OnRowNothingToSave(row, false);
	}
}
