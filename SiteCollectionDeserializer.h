#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "SiteCollection.h"

class SiteCollectionDeserializer : public JsonObjectDeserializer, public Encapsulate<SiteCollection>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

