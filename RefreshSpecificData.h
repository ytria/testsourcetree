#pragma once

#include "ModuleCriteria.h"

struct RefreshSpecificData
{
    ModuleCriteria m_Criteria;
    boost::YOpt<set<GridBackendRow*>> m_SelectedRowsAncestors; // boost::none means all
};
