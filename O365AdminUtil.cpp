#include "O365AdminUtil.h"

#include "BasicPageRequestLogger.h"
#include "GridFrameBase.h"
#include "MainFrameSapio365Session.h"
#include "ModuleBase.h"
#include "MsGraphFieldNames.h"
#include "BaseO365Grid.h"
#include "Registry.h"
#include "Sapio365Session.h"
#include "TimeUtil.h"
#include "OnPremiseExecScriptRequester.h"
#include "Sapio365Settings.h"
#include "ADDomainControllerCollectionDeserializer.h"
#include "SimpleMessageRequestLogger.h"
#include "ADSyncGlobalSettingsParameterDeserializer.h"
#include "DlgOnPremisesOptions.h"

namespace Util
{
	HybridCheckResult DoHybridCheck(const std::shared_ptr<Sapio365Session>& p_Session)
	{
		HybridCheckResult result;

		ASSERT(nullptr != p_Session);
		if (nullptr != p_Session)
		{
			PersistentSession loadedSession = SessionsSqlEngine::Get().Load(p_Session->GetIdentifier());
			p_Session->GetGraphCache().SetPersistentSession(loadedSession);

			auto dcServer = p_Session->GetGraphCache().GetADDSServer();
			auto dcUsername = p_Session->GetGraphCache().GetADDSUsername();
			auto dcPassword = p_Session->GetGraphCache().GetADDSPassword();

			if (!dcServer.empty() && !dcUsername.empty() && !dcPassword.empty())
				LoggerService::Debug(_YTEXT("HybridCheck: Using a remote connection to domain controller"));

			auto& cache = p_Session->GetGraphCache();
			const auto& org = cache.GetOrganization(YtriaTaskData()).get();
			bool syncEnabled = org.OnPremisesSyncEnabled ? *org.OnPremisesSyncEnabled : false;
			LoggerService::Debug(_YTEXTFORMAT(L"HybridCheck: onPremisesSyncEnabled: %s", syncEnabled ? _YTEXT("true") : _YTEXT("false")));

			if (!syncEnabled)
			{
				result.m_CanDoHybrid = false;
				p_Session->SetForceDisableLoadOnPrem(true);
				LoggerService::Debug(_YTEXT("HybridCheck: Disabling onPremises features because onPremisesSyncEnabled=false"));
				return result;
			}

			wstring script = _YTEXT(R"(Get-ADDomainController)");

			auto logger = std::make_shared<SimpleMessageRequestLogger>(_T("HybridCheck: Verifying AD DS domain..."));
			auto requester = std::make_shared<OnPremiseExecScriptRequester>(script, logger);

			auto deserializer = std::make_shared<ADDomainControllerCollectionDeserializer>();
			requester->SetDeserializer(deserializer);
			requester->Send(p_Session, YtriaTaskData()).GetTask().wait();

			if (requester->GetResult()->Get().m_Success)
			{
				result.m_ADDSDomain = deserializer->GetDomain();
				vector<wstring> graphDomains;

				wstring allVerifiedDomains;
				for (const auto& graphDomain : org.VerifiedDomains)
				{
					wstring domain = graphDomain.Name ? *graphDomain.Name : wstring();
					graphDomains.push_back(domain);
					allVerifiedDomains += wstring(domain + _YTEXT("; "));
				}

				result.m_VerifiedDomains = graphDomains;
				result.m_CanDoHybrid = true;
				LoggerService::Debug(_YTEXTFORMAT(L"HybridCheck: Found verified domains from MS Graph: %s", allVerifiedDomains.c_str()));
				LoggerService::Debug(_YTEXTFORMAT(L"HybridCheck: Found domain on ADDS: %s", result.m_ADDSDomain.c_str()));
			}
			else
			{
				result.m_CanDoHybrid = false;
				LoggerService::Debug(_YTEXT("HybridCheck: Disabling onPremises features because either AAD Connect is not installed or sapio365 was unable to verify domain controller info using Get-ADDomainController"));
				p_Session->SetForceDisableLoadOnPrem(true);
			}
		}

		return result;
	}

	void InitOnPrem(const std::shared_ptr<Sapio365Session>& p_Session, bool p_ForceDlgEdit)
	{
		ASSERT(nullptr != p_Session);
		if (nullptr != p_Session)
		{
			auto& cache = p_Session->GetGraphCache();

			auto hybridCheckResult = DoHybridCheck(p_Session);
			cache.SetHybridCheckSuccess(hybridCheckResult.m_CanDoHybrid);
			if (hybridCheckResult.m_CanDoHybrid)
			{
				PersistentSession persistentSession = SessionsSqlEngine::Get().Load(p_Session->GetIdentifier());
				cache.SetPersistentSession(persistentSession);
				if (p_ForceDlgEdit || (!cache.GetUseOnPrem() && !cache.GetDontAskAgainUseOnPrem()))
				{
					DlgOnPremisesOptions dlgOpt(nullptr,
						persistentSession.GetUseOnPrem(),
						persistentSession.GetDontAskAgainUseOnPrem(),
						persistentSession.GetAADComputerName(),
						persistentSession.GetAutoLoadOnPrem(),
						persistentSession.GetUseMsDsConsistencyGuid(),
						persistentSession.GetUseRemoteRSAT(),
						persistentSession.GetADDSServer(),
						persistentSession.GetADDSUsername());

					dlgOpt.SetVerifiedDomains(hybridCheckResult.m_VerifiedDomains);
					dlgOpt.SetADDSDomain(hybridCheckResult.m_ADDSDomain);

					auto dlgResult = dlgOpt.DoModal();
					if (dlgResult == IDOK)
						DlgOnPremisesOptions::SaveOnPremisesOptions(dlgOpt, persistentSession, &cache);
				}
			}
			else
			{
				LoggerService::Debug(_YTEXT("HybridCheck: Disabling onPremises features because hybridCheck failed"));
				p_Session->SetForceDisableLoadOnPrem(true);
			}
		}
	}

	wstring PromptForNewApplicationName(const wstring& p_DefaultName, CWnd* p_Parent)
	{
		static const wstring g_DlgAutomationName = _YTEXT("SetApplicationName");
		class DlgAppNameHTML : public DlgFormsHTML
		{
		public:
			DlgAppNameHTML(const wstring& p_DefaultName, CWnd* p_Parent)
				: DlgFormsHTML(DlgFormsHTML::Action::CREATE, p_Parent, g_DlgAutomationName)
				, m_NewName(p_DefaultName)
			{

			}

			wstring getDialogTitle() const override
			{
				return _T("Create new application");
			}

			void generateJSONScriptData() override
			{
				initMain(_YTEXT("DlgAppNameHTML"));
				
				getGenerator().addCategory(_T("Enter an app name"), CategoryFlags::NO_COLLAPSE | CategoryFlags::EXPAND_BY_DEFAULT);
				getGenerator().addIconTextField(_YTEXT("intro"), _T("The newly created application will be listed in your Azure AD. Set a custom app name below."), _YTEXT("fas fa-info-circle"), _YTEXT("#e4f0fb"), _YTEXT("#5882bc"));
				getGenerator().Add(StringEditor(_YTEXT("name"), _T("Application name"), EditorFlags::REQUIRED | EditorFlags::DIRECT_EDIT, m_NewName));

				getGenerator().addApplyButton(LocalizedStrings::g_CreateButtonText);
				getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
			}

			bool processPostedData(const IPostedDataTarget::PostedData& data) override
			{
				for (auto& d : data)
				{
					if (d.first == _YTEXT("name"))
						m_NewName = d.second;
				}
				return true;
			}

			const wstring& GetNewName() const
			{
				return m_NewName;
			}

		private:
			wstring m_NewName;
		};

		DlgAppNameHTML dlg(p_DefaultName, p_Parent);
		if (IDCANCEL == dlg.DoModal())
			return _YTEXT("");
		return dlg.GetNewName();
	}

	wstring GetElevatedSessionAppName(const PersistentSession& p_Session)
	{
		ASSERT(p_Session.IsValid());
		wstring appDisplayName = _T("sapio365 application");
		if (p_Session.IsPartnerAdvanced() || p_Session.IsPartnerElevated())
			appDisplayName = _YFORMAT(L"%s for partner %s", appDisplayName.c_str(), p_Session.GetEmailOrAppId().c_str());
		else
			appDisplayName = _YFORMAT(L"%s for %s", appDisplayName.c_str(), p_Session.GetFullname().c_str());

		return appDisplayName;
	}

	YtriaTaskData AddFrameTask(const wstring& p_LogMessage, GridFrameBase* p_Frame, AutomationSetup& p_AutoSetup, const LicenseContext& p_LicenseContext, bool p_IsMotherTask)
	{
		ASSERT(nullptr != p_Frame);
		if (nullptr != p_Frame)
		{
			HWND hwnd = p_Frame->GetSafeHwnd();
			auto taskData = detail::AddTaskData(p_LogMessage, p_Frame);
			p_Frame->SetTaskData(taskData);

			p_Frame->GetGrid().SetProcessText(YtriaTranslate::Do(ModuleBase_addFrameTask_1, _YLOC("Preparing requests...")).c_str());
			
			if (p_IsMotherTask)
				p_Frame->SetMotherAutomationAction(p_AutoSetup.m_ActionToExecute);
			p_Frame->AutomationRecord(p_AutoSetup);
			p_Frame->GetGrid().SetAutomationActionRecording(p_AutoSetup.m_ActionToRecord);

			p_Frame->SetLicenseContext(p_LicenseContext);

			return taskData;
		}
		return YtriaTaskData();
	}

	YtriaTaskData detail::AddTaskData(const wstring& p_Name, CFrameWnd* p_Originator)
	{
		ASSERT(nullptr != p_Originator);

		wstring sessionName;

		if (p_Originator == ::AfxGetApp()->m_pMainWnd)
		{
			std::shared_ptr<Sapio365Session> session = MainFrameSapio365Session();
			ASSERT(session);
			if (session)
				sessionName = session->GetEmailOrAppId();
		}
		else
		{
			auto frame = dynamic_cast<GridFrameBase*>(p_Originator);
			ASSERT(nullptr != frame);
			if (nullptr != frame)
				sessionName = frame->GetSessionInfo().GetEmailOrAppId();
		}

		return TaskDataManager::Get().AddTaskData(p_Name, sessionName, p_Originator->m_hWnd);
	}

	wstring GetFileAttachmentStr()
    {
        static const wstring str{ _YTEXT("#microsoft.graph.fileAttachment") };
        return str;
    }

    wstring GetItemAttachmentStr()
    {
        static const wstring str{ _YTEXT("#microsoft.graph.itemAttachment") };
        return str;
    }

    wstring GetReferenceAttachmentStr()
    {
        static const wstring str{ _YTEXT("#microsoft.graph.referenceAttachment") };
        return str;
    }

	HttpResultWithError GetResult(pplx::task<RestResultInfo> p_Task)
	{
		HttpResultWithError result;

		try
		{
			result = HttpResultWithError(p_Task.get());
		}
		catch (const RestException& e)
		{
			RestResultInfo info(e.GetRequestResult());
			info.m_RestException = std::make_shared<RestException>(e);
			SapioError sapioError(info.GetErrorMessage(), info.Response.status_code());

			result = HttpResultWithError(info, sapioError);
		}
		catch (const std::exception& e)
		{
			RestResultInfo info;
			info.m_Exception = e;
			SapioError sapioError(e);

			result = HttpResultWithError(info, sapioError);
		}
		catch (...)
		{
			RestResultInfo info;
			SapioError sapioError(_T("Unknown error"), 0);

			result = HttpResultWithError(info, sapioError);
		}

		return result;
	}

	wstring RemoveDeltaFromDate(const wstring p_OrigDateIso8601, LoadingScope p_Delta)
	{
		YTimeDate cutOff;
		TimeUtil::GetInstance().GetTimedateFromISO8601String(p_OrigDateIso8601, cutOff);
		cutOff.SetFlags(YTimeDate::DATE | YTimeDate::TIME);

		if (p_Delta == LastHour)
			cutOff.Adjust(0, 0, 0, -1, 0, 0);
		else if (p_Delta == Last24h)
			cutOff.Adjust(0, 0, 0, -24, 0, 0);
		else if (p_Delta == LastWeek)
			cutOff.Adjust(0, 0, -7, 0, 0, 0);
		else if (p_Delta == LastMonth)
			cutOff.Adjust(0, -1, 0, 0, 0, 0);

		return TimeUtil::GetInstance().GetISO8601String(cutOff);
	}

	void RemoveLicensePlansProperties(vector<rttr::property>& p_Properties)
    {
        p_Properties.erase(std::remove_if(p_Properties.begin(), p_Properties.end(), [](const rttr::property& p_Property) {
            return /*p_Property.get_name() == O365_USER_ASSIGNEDLICENSES
				|| */p_Property.get_name() == O365_USER_ASSIGNEDPLANS
				|| p_Property.get_name() == O365_USER_PROVISIONEDPLANS;
        }), p_Properties.end());
    }

    RestResultInfo RestResultInfoErrorHandler(const std::exception_ptr& p_ExceptPtr)
	{
		ASSERT(p_ExceptPtr);
		if (p_ExceptPtr)
		{
			try
			{
				std::rethrow_exception(p_ExceptPtr);
			}
			catch (const RestException& e)
			{
				auto rri = e.GetRequestResult();
				rri.m_RestException = std::make_shared<RestException>(e);
				return rri;
			}
			catch (const std::exception& e)
			{
				RestResultInfo rri;
				rri.m_Exception = e;
				return rri;
			}
		}

		ASSERT(false);
		return RestResultInfo();
	}

	map<PooledString, int32_t> GetConsumedMap(const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus)
	{
		map<PooledString, int32_t> consumedSkus;

		// If no error
		if (p_BusinessSubscribedSkus.size() != 1 || !p_BusinessSubscribedSkus[0].GetError())
		{
			for (auto& ssku : p_BusinessSubscribedSkus)
			{
				if (ssku.GetSkuId())
					consumedSkus[*ssku.GetSkuId()] = ssku.GetConsumedUnits() ? *ssku.GetConsumedUnits() : 0;
			}
		}

		return consumedSkus;
	}

	HttpResultWithError WriteErrorHandler(const std::exception_ptr& p_ExceptPtr) {
        ASSERT(p_ExceptPtr);
        if (p_ExceptPtr)
        {
            try
            {
                std::rethrow_exception(p_ExceptPtr);
            }
            catch (const RestException& e)
            {
                return HttpResultWithError(e.GetRequestResult(), HttpError(e));
            }
            catch (const std::exception& e)
            {
                return HttpResultWithError(boost::none, SapioError(e));
            }
        }

        ASSERT(false);
        return HttpResultWithError();
    };

	bool ShowRestrictedAccessDialog(DlgRestrictedAccess::Image p_Image, RestrictedAccessDialogConfig p_Config, const ISetting<bool>& p_Setting, CWnd* p_Parent, bool p_ShowOnlyOK/* = false*/, int p_AdditionalHeight, int p_AdditionalWidth)
	{
		bool result = true;

		if (!p_Setting || !p_Setting.Get()) // If setting not set, dialog is shown (we don't care about setting's value)
		{
			const wstring okText = p_Config.m_OkButtonTextOverride.empty()
				? YtriaTranslate::Do(O365Grid_showRestrictedAccessDialog_1, _YLOC("Continue")).c_str()
				: p_Config.m_OkButtonTextOverride;

			const wstring cancelText = p_Config.m_CancelButtonTextOverride.empty()
				? (p_ShowOnlyOK ? _T("OK") : YtriaTranslate::Do(O365Grid_showRestrictedAccessDialog_2, _YLOC("Cancel")).c_str())
				: p_Config.m_CancelButtonTextOverride;

			DlgRestrictedAccess dlg(p_Image, p_Parent, p_AdditionalHeight, p_AdditionalWidth);
			if (p_ShowOnlyOK)
			{
				// Button on the right is cancel.
				// As we want to keep only one, keep the cancel button with 'OK' text.
				dlg.SetCancelButtonText(cancelText);
				dlg.HideOkButton();
			}
			else
			{
				dlg.SetOkButtonText(okText);
				dlg.SetCancelButtonText(cancelText);
			}
			if (p_Setting)
				dlg.SetNoShowCheckboxText(YtriaTranslate::Do(O365Grid_showRestrictedAccessDialog_3, _YLOC("Do not show again")).c_str());
			else
				dlg.HideNoShowCheckbox();
			dlg.SetText(p_Config.m_Text);
			dlg.SetTitle(p_Config.m_Title);
			if (!p_Config.m_ImportantText.empty())
				dlg.SetImportantText(p_Config.m_ImportantText);
			if (!p_Config.m_LearnMoreText.empty() && !p_Config.m_LearnMoreURL.empty())
				dlg.SetLearnMoreLink(p_Config.m_LearnMoreText, p_Config.m_LearnMoreURL);
			result = IDOK == dlg.DoModal();
			if (p_Setting && dlg.IsNoShow() && ((result && !p_ShowOnlyOK) || (!result && p_ShowOnlyOK)))
				p_Setting.Set(true);
		}

		return result;
	}

	ElevatedPrivilegesDlgResult ShowExplainElevatedPrivilegesDialog(CWnd* p_Parent, bool p_JustCreatedFullSession)
	{
		DlgRestrictedAccess dlg(DlgRestrictedAccess::Image::ADVANCED_TO_ELEVATED, p_Parent, 120, 135);
		
		dlg.SetOkButtonText(_T("Continue"));
		if (p_JustCreatedFullSession)
			dlg.SetCancelButtonText(_T("Skip"));
		else
			dlg.SetCancelButtonText(_T("Cancel"));
		dlg.HideNoShowCheckbox();
		if (p_JustCreatedFullSession)
		{
			dlg.SetTitle(_T("Provide elevated privileges?"));
			dlg.SetText(_T("Do you wish to enhance this Advanced session with elevated privileges?\r\n\r\nThis will extend the level of access of this Advanced session by automatically creating an application for maximum access. You will be prompted to sign in and provide admin consent for this application.\r\n\r\nNote that this will grant access to all personal data for calendars, mailboxes, drive items, etc..."));
		}
		else
		{
			dlg.SetTitle(_T("Promote with elevated privileges"));
			dlg.SetText(_T("Do you wish to enhance this Advanced session with elevated privileges?\r\n\r\nThis will extend the level of access of this Advanced session by automatically creating an application for maximum access. You will be prompted to sign in and provide admin consent for this application.\r\n\r\nNote that this will grant access to all personal data for calendars, mailboxes, drive items, etc..."));
		}
		dlg.SetImportantText(_YTEXT("<br>"));
		dlg.SetLearnMoreLink(_T("Learn more about elevated privileges..."), Product::getURLHelpWebSitePage(_YTEXT("Help/elevated-privileges")));
		dlg.SetActionButton(_T("Let me set this up "), _T("manually..."));

		auto dlgRes = dlg.DoModal();

		ElevatedPrivilegesDlgResult result;
		if (dlgRes == IDOK)
		{
			if (dlg.IsActionRequested())
				result = ElevatedPrivilegesDlgResult::ElevateManually;
			else
				result = ElevatedPrivilegesDlgResult::Elevate;
		}
		else if (dlgRes == IDCANCEL)
		{
			result = ElevatedPrivilegesDlgResult::Cancel;
		}

		return result;
	}

	INT_PTR ShowApplicationConnectionError(const wstring& p_Title, const wstring& p_JsonError, CWnd* p_Parent, bool p_WithRetryButton/* = false*/)
	{
		auto error = Sapio365Session::ParseGraphJsonError(p_JsonError);

		wstring errorMessage;
		if (error.m_ErrorCode == _YTEXT("401")
			&& error.m_ErrorName == _YTEXT("Authorization_IdentityNotFound"))
		{
			errorMessage = _T("An error occurred while authenticating. Please ensure the Application ID is correct and admin consent has been given.");
		}
		else if (error.m_ErrorCode == _YTEXT("700016")
			&& error.m_ErrorName == _YTEXT("unauthorized_client"))
		{
			errorMessage = _T("An error occurred while authenticating. Please verify the Application ID is correct.");
		}
		else if (error.m_ErrorCode == _YTEXT("7000215")
			&& error.m_ErrorName == _YTEXT("invalid_client"))
		{
			errorMessage = _T("An error occurred while authenticating. Please verify the Application password is correct.");
		}
		else
		{
			errorMessage = YtriaTranslate::Do(GridFrameBase_OnRelog_2, _YLOC("An error occurred while authenticating. Please ensure your credentials are correct and admin consent has been given.")).c_str();
		}

		YCodeJockMessageBox msgBox(p_Parent,
			DlgMessageBox::eIcon::eIcon_Error,
			p_Title,
			errorMessage,
			error.m_ErrorDescription,
			p_WithRetryButton
			? std::unordered_map<UINT, wstring>{ { IDRETRY, _T("Retry") }, { IDCANCEL, _T("Cancel") } }
			: std::unordered_map<UINT, wstring>{ { IDOK, YtriaTranslate::Do(GridFrameBase_OnRelog_3, _YLOC("OK")).c_str() } }
		);

		return msgBox.DoModal();
	}

	web::json::value GetFileSystemInfo(const wstring& p_FilePath)
	{
		auto hFile = CreateFile(p_FilePath.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

		if (hFile == INVALID_HANDLE_VALUE)
			return web::json::value::null();

		FILETIME ftCreate, ftAccess, ftWrite;
		if (FALSE == GetFileTime(hFile, &ftCreate, &ftAccess, &ftWrite))
			return web::json::value::null();

		SYSTEMTIME utcCreateTime;
		FileTimeToSystemTime(&ftWrite, &utcCreateTime);
		SYSTEMTIME utcAccessTime;
		FileTimeToSystemTime(&ftWrite, &utcAccessTime);
		SYSTEMTIME utcWriteTime;
		FileTimeToSystemTime(&ftWrite, &utcWriteTime);

		return web::json::value::object(
			{
				std::make_pair(_YTEXT("@odata.type"), web::json::value(_YTEXT("microsoft.graph.fileSystemInfo"))),
				std::make_pair(_YTEXT("createdDateTime"), web::json::value(TimeUtil::GetInstance().GetISO8601String(YTimeDate(utcCreateTime, 0)))),
				std::make_pair(_YTEXT("lastAccessedDateTime"), web::json::value(TimeUtil::GetInstance().GetISO8601String(YTimeDate(utcAccessTime, 0)))),
				std::make_pair(_YTEXT("lastModifiedDateTime"), web::json::value(TimeUtil::GetInstance().GetISO8601String(YTimeDate(utcWriteTime, 0)))),
			});
	}

	namespace
	{
		template<class LoggerClass>
		class LoggerDecorator
		{
		public:
			LoggerDecorator(std::shared_ptr<LoggerClass> p_Logger)
				: m_Logger(p_Logger)
			{
				ASSERT(m_Logger);
			}

			void operator()(const BusinessUser& p_User)
			{
				m_Logger->IncrementObjCount();
				m_Logger->SetContextualInfo(GetDisplayNameOrId(p_User));
			}

			void operator()(const PooledString& p_UserId)
			{
				m_Logger->IncrementObjCount();
				m_Logger->SetContextualInfo(p_UserId);
			}

			std::shared_ptr<LoggerClass>& operator*()
			{
				return m_Logger;
			}

		private:
			std::shared_ptr<LoggerClass> m_Logger;
		};

		//////////////////////////

		const std::shared_ptr<MultiObjectsRequestLogger>& toMORL(const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger)
		{
			return p_Logger;
		}

		std::shared_ptr<MultiObjectsRequestLogger> toMORL(const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger)
		{
			ASSERT(p_Logger);
			return std::make_shared<MultiObjectsRequestLogger>(*p_Logger);
		}

		///////////////////////////

		template<class LoggerClass>
		void ProcessSubUserItemsImpl(std::function<void(BusinessUser&)> p_ProcessUser, const O365IdsContainer& p_IDs, Origin p_Origin, GraphCache& p_GraphCache, bool p_UseSQLCache, LoggerDecorator<LoggerClass> p_Logger, YtriaTaskData p_TaskData)
		{
			ASSERT(!p_IDs.empty());
			if (p_UseSQLCache) // means more info than just cachedUser needed
			{
				ASSERT(p_Origin == Origin::User);

				// We want list properties
				auto users = p_GraphCache.GetSqlCachedUsers(p_TaskData.GetOriginator(), p_IDs, { UBI::MIN, UBI::LIST, UBI::ASSIGNEDLICENSES }, false);

				// Every time a user is loaded, it should be added to cache. As to get there, one must have selected some users in a grid,
				// it should not happen that the cache contains less users than requested. It would be he result of a bug elsewhere.
				// !!! Not if My Data or indirect path (like Memberships through Groups then Members), hence the following:
				if (users.size() != p_IDs.size())
				{
					auto logger = std::make_shared<BasicPageRequestLogger>(_T("users"));
					users = p_GraphCache.GetUpToDateUsers<BusinessUser>(p_IDs, logger, GraphCache::PreferredMode::DELTASYNC, p_TaskData).get();
				}

				ASSERT(users.size() == p_IDs.size());
				for (auto& user : users)
				{
					p_Logger(user);
					p_ProcessUser(user);
				}
			}
			else
			{
				for (const auto& id : p_IDs)
				{
					BusinessUser user;
					user.SetID(id);

					auto cachedUser = p_GraphCache.GetUserInCache(user.m_Id, Origin::DeletedUser == p_Origin);
					ASSERT(cachedUser.GetID() == user.m_Id);
					if (cachedUser.GetID() == user.m_Id)
					{
						user.SetValuesFrom(cachedUser);
					}
					else
					{
						// Should we load sql if it fails?
						ASSERT(Origin::DeletedUser == p_Origin);
						if (!p_TaskData.IsCanceled())
						{
							(*p_Logger)->SetContextualInfo(user.m_Id);
							
							p_GraphCache.SyncUncachedUser(user, toMORL(*p_Logger), p_TaskData, Origin::DeletedUser == p_Origin);
							ASSERT(Origin::DeletedUser != p_Origin || user.IsFromRecycleBin());
						}
					}

					p_Logger(user);
					p_ProcessUser(user);
				}
			}
		}
	}

	void ProcessSubUserItems(std::function<void(BusinessUser&)> p_ProcessUser, const O365IdsContainer& p_IDs, Origin p_Origin, GraphCache& p_GraphCache, bool p_UseSQLCache, std::shared_ptr<MultiObjectsRequestLogger> p_Logger, YtriaTaskData p_TaskData)
	{
		ProcessSubUserItemsImpl(p_ProcessUser, p_IDs, p_Origin, p_GraphCache, p_UseSQLCache, LoggerDecorator<MultiObjectsRequestLogger>(p_Logger), p_TaskData);
	}

	void ProcessSubUserItems(std::function<void(BusinessUser&)> p_ProcessUser, const O365IdsContainer& p_IDs, Origin p_Origin, GraphCache& p_GraphCache, bool p_UseSQLCache, std::shared_ptr<MultiObjectsPageRequestLogger> p_Logger, YtriaTaskData p_TaskData)
	{
		ProcessSubUserItemsImpl(p_ProcessUser, p_IDs, p_Origin, p_GraphCache, p_UseSQLCache, LoggerDecorator<MultiObjectsPageRequestLogger>(p_Logger), p_TaskData);
	}

	LicenseContext ForSubModule(const LicenseContext& p_LC)
	{
		auto lc = p_LC;
		lc.SetTokenRateType(LicenseContext::TokenRateType::SubModule);
		lc.BLOCK_FREE_ACCESS();// block free access
		return lc;
	}

}