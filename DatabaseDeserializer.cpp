#include "DatabaseDeserializer.h"

#include "JsonSerializeUtil.h"

void Cosmos::DatabaseDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    BaseObjDeserializer::Deserialize(p_Object, GetData());
    JsonSerializeUtil::DeserializeString(_YTEXT("_colls"), m_Data._colls, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("_users"), m_Data._users, p_Object);
}
