#pragma once

#include "IRequester.h"
#include "DeploymentDeserializer.h"

class SingleRequestResult;

namespace Azure
{
	class CreateDeploymentRequester : public IRequester
	{
	public:
		CreateDeploymentRequester(PooledString p_SubscriptionId, PooledString p_ResourceGroup, PooledString p_DeploymentName);

		TaskWrapper<void> Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	private:
		std::shared_ptr<Azure::DeploymentDeserializer> m_Deserializer;
		std::shared_ptr<SingleRequestResult> m_Result;

		PooledString m_SubscriptionId;
		PooledString m_ResourceGroup;
		PooledString m_DeploymentName;
	};
}
