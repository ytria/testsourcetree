#pragma once

class PersistentSession;
class SessionsSqlEngine;

class QuerySetToElevatedSession
{
public:
	QuerySetToElevatedSession(const PersistentSession& p_Session, SessionsSqlEngine& p_Engine);
	void Run();

private:
	const PersistentSession& m_Session;
	SessionsSqlEngine& m_Engine;
};

