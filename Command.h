#pragma once

#include "AutomationUtil.h"
#include "BusinessObject.h"
#include "CommandInfo.h"
#include "HistoryMode.h"
#include "Sapio365Session.h"

class AutomationAction;
class GridFrameBase;
class IActionCommand;

class Command
{
public:
	enum class ModuleTarget : uint16_t
	{
		None = 0,
		Applications,
		Contact,
		Conversation,
        Channel,
		ChannelMembers,
		ChannelMessages,
		Columns,
		ClassMembers,
		DirectoryRoles,
		DirectoryAudit,
		Drive,
		EduUsers,
		Event,
		Group,
		Licenses,
		Lists,
		ListsItem,
		Mail,
		Message,
		MessageRules,
		Organization,
		OrgContact,
		Post,
		Reports,
		SignIns,
		Sites,
        SiteRoles,
		User,
		Schools,
        SpUser,
        SpGroup,
        SitePermissions,
		MailboxPermissions,
		OnPremiseUsers,
		OnPremiseGroups,
		// add other modules here
		numModuleTargets,
	};

	enum class ModuleTask : uint16_t
	{
		ApplyChanges,
		ApplyOnPremChanges,
		List,
		ListWithPrefilter,
        ListDetails,
		ListByID,
        ListMe,
		ListMeByID,
		ListSubItems,
		ListOwners,
		ListMyOwners, // For "My Group Memberships"
		ListAuthors,
		ListManagerHierarchy,
		ListRecycleBin,
		UpdateModified,
		UpdateMoreInfo,
		UpdateRefresh,
		UpdateGroupItems,
		UpdateRecycleBin,
		UpdateLCP, // Lifecycle Policy for all groups.
		UpdateLCPGroup, // Per group Lifecycle actions.
		ListLCPGroup, // Per group status with respect to Lifecycle policy.
		ListPermissions,
		ListPermissionsExplode,
		ListPermissionsExplodeFlat,
        ListAttachments,
        DownloadInlineAttachments,
        DownloadAttachments,
		DownloadAndSaveAsEml,
        ViewItemAttachment,
		DeleteAttachment,
        //SendEmailAdvancedSession,
        //SendEmailUltraAdminSession,
		DownloadDriveItems,
        UpdateSettings,
		RevokeAccess, // For users
		LoadCheckoutStatus, // For drive items
		// FIXME: Finish this eventually
		//RetrieveFoldersIDs // For message rules
		LoadSnapshot,
		TryLoadSnapshot, // handy command used when snapshot requires load of a session.s
		ActionCommand,
		UpdateMailbox,
		Search,
		SearchSubItems,
		SearchMe,
    };

	enum class Status
	{
		Init,
		Sent,
		SendingFailure,
		ExecutionFailure
	};

	struct HistorySourceWindow
	{
	public:
		HistorySourceWindow(HWND p_Hwnd);

		HWND	GetHWND() const;
		CWnd*	GetCWnd() const;

		CFrameWnd* GetCFrameWndForHistory() const;

		bool UserConfirmation(AutomationAction* p_Action);
		HistoryMode::Mode GetHistoryMode() const;

	private:
		bool ShouldKeepInHistoryIfNewWindow() const;

	private:
		HWND m_Hwnd;
		HistoryMode::Mode m_HistoryMode;
	};
	
    Command(const LicenseContext& p_LicenseContext, HistorySourceWindow sourceHwnd, const ModuleTarget p_Target, const ModuleTask p_Task, const CommandInfo& p_CommandInfo, const AutomationSetup& p_AutomationSetup);
	virtual ~Command() = default;

	const SessionIdentifier& GetSessionIdentifier() const;
	void SetSessionIdentifier(const boost::YOpt<SessionIdentifier>& p_SessionIdentifier) const;
    ModuleTarget		GetTarget() const;
    ModuleTask			GetTask() const;
    const CommandInfo&  GetCommandInfo() const;
    CommandInfo&        GetCommandInfo();


    HistorySourceWindow		GetSourceWindow() const;
	const LicenseContext&	GetLicenseContext() const;
	void					SetLicenseContext(const LicenseContext& p_LicenseContext);
    CWnd*					GetSourceCWnd() const;
    AutomationAction*		GetAutomationAction() const;
    AutomationAction*		GetAutomationActionRecording() const;
    const wstring&			GetAutomationRecordingActionName() const;
    void					SetAutomationAction(AutomationAction* p_Action) const;
    void					SetAutomationActionRecording(AutomationAction* p_ActionRecording);
    wstring					ToString() const;

    AutomationSetup&	GetAutomationSetup() const;
    void				SetAutomationSetup(const AutomationSetup& p_AutomationSetup);
    AutomationAction*	AutomationRecord() const;

	static Command CreateCommand(const std::shared_ptr<IActionCommand>& p_ActionCmd, GridFrameBase& p_Frame, Command::ModuleTarget p_Target);

private:
	mutable boost::YOpt<SessionIdentifier> m_SessionIdentifier;
    CommandInfo                             m_CommandInfo;
	ModuleTarget							m_Target;
	ModuleTask								m_Task;
	HistorySourceWindow						m_SourceHwnd;
	LicenseContext							m_LicenseContext;

	mutable AutomationSetup					m_AutomationSetup;

	static const wstring					g_KeyHistoryMode;
};
