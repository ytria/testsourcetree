#include "DlgAutomationInputHTML.h"

#include "AutomationDataStructure.h"

DlgAutomationInputHTML::DlgAutomationInputHTML(	const wstring& p_Title, 
												const map<INT, wstring> p_BtnTexts, 
												const vector<AutomationInputVariableDefinition>& p_VariableDefinitions, 
												const vector<AutomationInputVariableEvent>& p_VarEvents, 
												const vector<AutomationInputVariableEvent>& p_VarEventsGlobal,
												CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
	, m_VarDef(p_VariableDefinitions)
	, m_VarEvents(p_VarEvents)
	, m_VarEventsGlobal(p_VarEventsGlobal)
	, m_Title(p_Title)
	, m_BtnTexts(p_BtnTexts)
{
	setIgnoreBlockVertResizeForHTMLContainer(true);
}

DlgAutomationInputHTML::~DlgAutomationInputHTML()
{
}

const vector<AutomationInputVariableDefinition>& DlgAutomationInputHTML::GetScriptVariables() const
{
	return m_VarDef;
}

void DlgAutomationInputHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgAutomationInputHTML"));

	for (const auto& v : m_VarDef)
	{
		uint32_t editFlags = 0;
		if (v.m_ReadOnly)
			editFlags |= EditorFlags::READONLY;
		else
			editFlags |= EditorFlags::DIRECT_EDIT;

		if (v.m_Mandatory)
			editFlags |= EditorFlags::REQUIRED;

		if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeString))
			getGenerator().Add(WithTooltip<StringEditor>({ v.m_VarName, v.m_DlgLabel, editFlags, v.m_DefaultValue }, v.m_DlgTooltip));
		else if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeNumber))
			getGenerator().Add(WithTooltip<NumberEditor>({ v.m_VarName, v.m_DlgLabel, editFlags, Str::getDoubleFromString(v.m_DefaultValue) }, v.m_DlgTooltip));
		else if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeCheckList))
		{
			CheckListEditor::LabelsAndValues labelsAndValues;
			std::vector<wstring> defaultValues;
			for (const auto& v : v.m_ListLabelsAndValues)
			{
				labelsAndValues.emplace_back(std::get<0>(v), std::get<1>(v));
				if (std::get<2>(v))
					defaultValues.emplace_back(std::get<1>(v));
			}
			getGenerator().Add(WithTooltip<CheckListEditor>({ { v.m_VarName, v.m_DlgLabel, editFlags, defaultValues }, labelsAndValues }, v.m_DlgTooltip));
		}
		else if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeRadioList))
		{
			CheckListEditor::LabelsAndValues labelsAndValues;
			std::vector<wstring> defaultValues;
			for (const auto& v : v.m_ListLabelsAndValues)
			{
				labelsAndValues.emplace_back(std::get<0>(v), std::get<1>(v));
				if (std::get<2>(v))
					defaultValues.emplace_back(std::get<1>(v));
			}
			getGenerator().Add(WithTooltip<AsRadio<CheckListEditor>>({ { v.m_VarName, v.m_DlgLabel, editFlags, defaultValues }, labelsAndValues }, v.m_DlgTooltip));
		}
		else if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeMultiList))
			getGenerator().addMultiListEditor(v.m_VarName, v.m_DlgLabel, v.m_DlgTooltip, editFlags, v.m_ListLabelsAndValues);
		else if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeBool))
			getGenerator().Add(WithTooltip<BoolEditor>({ { v.m_VarName, v.m_DlgLabel, editFlags, MFCUtil::StringMatchW(v.m_DefaultValue, AutomationConstant::val().m_ValueTrue) } }, v.m_DlgTooltip));
		else if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeBoolToggle))
			getGenerator().Add(WithTooltip<BoolToggleEditor>({ { { v.m_VarName, v.m_DlgLabel, editFlags, MFCUtil::StringMatchW(v.m_DefaultValue, AutomationConstant::val().m_ValueTrue) } } }, v.m_DlgTooltip));
		else if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeCategory))
			getGenerator().addCategory(v.m_DlgLabel, v.m_ExpandCategory ? CategoryFlags::EXPAND_BY_DEFAULT : CategoryFlags::NOFLAG);
		else if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeDate))
			getGenerator().Add(WithTooltip<DateEditor>({ { v.m_VarName, v.m_DlgLabel, editFlags, v.m_DefaultValue }, boost::none }, v.m_DlgTooltip));
		else if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeDateInPast))
			getGenerator().Add(WithTooltip<DateEditor>({ { v.m_VarName, v.m_DlgLabel, editFlags, v.m_DefaultValue }, Str::g_EmptyString }, v.m_DlgTooltip));
		else if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeCombo))
		{
			ASSERT(std::count_if(v.m_ListLabelsAndValues.begin(), v.m_ListLabelsAndValues.end(), [](auto& v) {return std::get<2>(v); }) <= 1);
			ComboEditor::LabelsAndValues labelsAndValues;
			wstring defaultValue;
			for (const auto& v : v.m_ListLabelsAndValues)
			{
				labelsAndValues.emplace_back(std::get<0>(v), std::get<1>(v));
				if (std::get<2>(v))
					defaultValue = std::get<1>(v);
			}
			getGenerator().Add(WithTooltip<ComboEditor>({ { v.m_VarName, v.m_DlgLabel, editFlags, defaultValue }, labelsAndValues }, v.m_DlgTooltip));
		}
		else if (MFCUtil::StringMatchW(v.GetEditorType(), AutomationInputVariableDefinition::EditorTypeLabel))
			getGenerator().addIconTextField(v.m_VarName, v.m_DlgLabel, v.m_Icon, v.m_Color, Str::g_EmptyString);
		else
			ASSERT(FALSE);
	}

	for (const auto& e : m_VarEvents)
		getGenerator().addEvent(e.m_Source, EventTarget({ e.m_Target, e.m_Mode, e.m_Value }));

	for (const auto& e : m_VarEventsGlobal)
		getGenerator().addGlobalEventTrigger(e.m_Source);

	auto findOK		= m_BtnTexts.find(IDOK);
	auto findCancel = m_BtnTexts.find(IDCANCEL);
	getGenerator().addApplyButton(findOK == m_BtnTexts.end() ? YtriaTranslate::Do(DlgAutomationInputHTML_generateJSONScriptData_1, _YLOC("OK")).c_str() : findOK->second.c_str());
	getGenerator().addCancelButton(findCancel == m_BtnTexts.end() ? YtriaTranslate::Do(DlgAutomationInputHTML_generateJSONScriptData_2, _YLOC("Cancel")).c_str() : findCancel->second.c_str());
}

bool DlgAutomationInputHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	for (const auto& item : data)
		for (auto& v : m_VarDef)
			if (MFCUtil::StringMatchW(item.first, v.m_VarName))
				v.AddValue(item.second);

	return true;
}

wstring DlgAutomationInputHTML::getDialogTitle() const
{
	return m_Title;
}