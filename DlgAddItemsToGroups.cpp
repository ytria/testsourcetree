#include "DlgAddItemsToGroups.h"

#include "DlgGenericListCtrl.h"
#include "BaseO365Grid.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessObjectManager.h"
#include "BusinessUser.h"
#include "BusinessUserGuest.h"
#include "BusinessUserUnknown.h"
#include "GridTemplate.h"
#include "GridUtil.h"
#include "Resources\resource.h"
#include "GraphCache.h"
#include "safeTaskCall.h"
#include "Resource.h"
#include "YCallbackMessage.h"
#include "EducationClassListRequester.h"
#include "BasicPageRequestLogger.h"

namespace
{
	const COLORREF g_AlreadyInColor = RGB(192, 192, 192);
}

//////////////////////////////////////////////////////////////////////////////////////////////////

GridAvailableUsers::~GridAvailableUsers()
{}

void GridAvailableUsers::SetParentDlg(DlgAddItemsToGroups* p_Dlg)
{
	m_Dlg = p_Dlg;
}

const wstring GridAvailableUsers::g_AutomationName = _YTEXT("GridMemberSelection");

void GridAvailableUsers::customizeGrid()
{
	SetAutomationName(g_AutomationName);
	GridUtil::Add365ObjectTypes(*this);
	m_TeamIcon = GridUtil::AddIconTeam(*this);
	m_EmailIcon = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_MESSAGE));
	AddColumnObjectType();
}

void GridAvailableUsers::OnCustomDoubleClick()
{
	if (nullptr != m_Dlg && m_Dlg->GetDlgItem(IDOK)->IsWindowEnabled())
		m_Dlg->SendMessage(WM_COMMAND, IDOK);
}

//////////////////////////////////////////////////////////////////////////////////////////////////

bool Commodore64CheckComboBox::_OnCbLbPreWndProc(LRESULT& lResult, UINT message, WPARAM wParam, LPARAM lParam)
{
	// Code copied directly from ExtComboBox.cpp
	// Has problems in 64 bit.
	switch (message)
	{
		case WM_CHAR:
		{
			if (wParam == VK_SPACE)
			{
				CExtComboBoxPopupListBox* pLB = OnComboBoxPopupListBoxQuery();
				ASSERT_VALID(pLB);
				const auto nIndex = INT(pLB->SendMessage(LB_GETCURSEL));
				CRect rcItem;
				pLB->SendMessage(LB_GETITEMRECT, nIndex, (LPARAM)&rcItem);
				pLB->InvalidateRect(rcItem, FALSE);
				CheckSet(nIndex, !CheckGet(nIndex));
				TriggerSelChange();
				lResult = 0L;
				return true;
			}
		}
		break;

		case WM_LBUTTONDOWN:
		case WM_LBUTTONDBLCLK:
		{
			CExtComboBoxPopupListBox * pLB = OnComboBoxPopupListBoxQuery();
			ASSERT_VALID(pLB);
			CRect rcClient;
			pLB->GetClientRect(&rcClient);
			CPoint pt(LOWORD(lParam), HIWORD(lParam));
			if (PtInRect(rcClient, pt))
			{
				const auto nItemHeight = INT(pLB->SendMessage(LB_GETITEMHEIGHT, 0, 0));
				const auto nTopIndex = INT(pLB->SendMessage(LB_GETTOPINDEX, 0, 0));
				const INT nIndex = nTopIndex + pt.y / nItemHeight;
				CRect rcItem;
				pLB->SendMessage(LB_GETITEMRECT, nIndex, (LPARAM)&rcItem);
				if (PtInRect(rcItem, pt))
				{
					pLB->InvalidateRect(rcItem, FALSE);
					CheckSet(nIndex, !CheckGet(nIndex));
					TriggerSelChange();
				}
			}
		}
		break;

		case WM_LBUTTONUP:
			lResult = 0L;
			return true;
	}

	return false;
}

void Commodore64CheckComboBox::TriggerSelChange()
{
	GetParent()->SendMessage(
		WM_COMMAND,
		MAKELONG(::__EXT_MFC_GetWindowLong(m_hWnd, GWL_ID), CBN_SELCHANGE),
		(LPARAM)m_hWnd);
}

//////////////////////////////////////////////////////////////////////////////////////////////////

const wstring DlgAddItemsToGroups::g_AutomationName					= _YTEXT("DlgAddItemsToGroups");
const wstring DlgAddItemsToGroups::g_AutomationNameSelectionGrid	= _YTEXT("ItemSelectionGrid");

BEGIN_MESSAGE_MAP(DlgAddItemsToGroups, ResizableDialog)
	ON_BN_CLICKED(IDC_BTN_ADDITEMSTOGROUPS_CLEAR,			OnClear)
	ON_BN_CLICKED(IDC_BTN_ADDITEMSTOGROUPS_REFRESH,			OnRefresh)
	ON_BN_CLICKED(IDC_BTN_ADDITEMSTOGROUPS_LOADALL_RIGHT,	OnLoadAll)
	ON_EN_CHANGE(IDC_EDIT_ADDITEMSTOGROUPS_CONTAINS,		OnEditChange)
	ON_LBN_SELCHANGE(IDC_LIST_ADDITEMSTOGROUPS_LISTITEMS,	OnListItemsLBNChange)
	ON_WM_DESTROY()
END_MESSAGE_MAP()

DlgAddItemsToGroups::DlgAddItemsToGroups(
	std::shared_ptr<Sapio365Session>												p_Session,
	const std::map<PooledString, std::pair<PooledString, std::set<PooledString>>>&	p_SelectedParents,
	const LOADTYPE																	p_LoadType, 
	const vector<BusinessUser>&														p_InitUsers, 
	const vector<BusinessGroup>&													p_InitGroups,
	const vector<BusinessOrgContact>&												p_InitOrgContacts,
	const wstring&																	p_Title,
	const wstring&																	p_Explanation,
	const RoleDelegationUtil::RBAC_Privilege										p_Privilege,
	CWnd*																			p_Parent)
	: ResizableDialog(DlgAddItemsToGroups::IDD,										p_Parent, g_AutomationName)
	, m_pColID(nullptr)
	, m_pColPrincipalName(nullptr)
	, m_pColDisplayName(nullptr)
	, m_pColItemType(nullptr)
	, m_pColIsTeam(nullptr)
	, m_pColIsShowEmail(nullptr)
	, m_SelectedParents(p_SelectedParents)
	, m_LoadType(p_LoadType)
	, m_Grid(0, GridBackendUtil::CREATECOLORG | GridBackendUtil::CREATESORTING | GridBackendUtil::CREATEPIVOT | GridBackendUtil::CREATECOMPARE | GridBackendUtil::CREATESAVEXML)
	, m_Session(p_Session)
	, m_GraphCache(p_Session->GetGraphCache())
	, m_InitUsers(p_InitUsers)
	, m_InitGroups(p_InitGroups)
	, m_InitOrgContacts(p_InitOrgContacts)
	, m_FilterAutoTriggered(false)
	, m_Title(p_Title)
	, m_Explanation(p_Explanation)
	, m_Privilege(p_Privilege)
{}

void DlgAddItemsToGroups::OnOK()
{
	for (GridBackendRow* pRow : m_Grid.GetSelectedRows())
	{
		if (!pRow->IsGroupRow())
		{
			const GridBackendField& FieldID				= pRow->GetField(m_pColID);
			const GridBackendField& FieldUPN			= pRow->GetField(m_pColPrincipalName);
			const GridBackendField& FieldDisplayName	= pRow->GetField(nullptr != m_pColDisplayName ? m_pColDisplayName : m_pColPrincipalName);
			const GridBackendField& FieldIsTeam			= nullptr != m_pColIsTeam ? pRow->GetField(m_pColIsTeam) : GridBackendField::g_GenericConstField;

			const auto type = [pRow, colItemType = m_pColItemType]() -> PooledString
			{
				if (nullptr != colItemType && pRow->GetField(colItemType).HasValue())
					return pRow->GetField(colItemType).GetValueStr();
				if (GridUtil::IsBusinessUser(pRow, false))
					return BusinessUser::g_UserTypeMember;
				if (GridUtil::IsBusinessUser(pRow, true))
					return BusinessUser::g_UserTypeGuest;
				if (GridUtil::IsBusinessOrgContact(pRow))
					return BusinessOrgContact().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>().c_str();
				ASSERT(false);
				return PooledString();
			};

			m_SelectedItems.emplace_back(	FieldID.GetValueStr(),
											FieldUPN.GetValueStr(),
											FieldDisplayName.GetValueStr(),
											m_Grid.GetRowObjectType(pRow),
											type(),
											FieldIsTeam.HasValue() ? FieldIsTeam.GetValueStr() : PooledString()
			);
		}
	}

	ResizableDialog::OnOK();
}

void DlgAddItemsToGroups::OnCancel()
{
    m_TaskData.Cancel();
    ResizableDialog::OnCancel();
}

const DlgAddItemsToGroups::SelectedItems& DlgAddItemsToGroups::GetSelectedItems() const
{
	return m_SelectedItems;
}

void DlgAddItemsToGroups::SelectionChanged(const CacheGrid& grid)
{
	BOOL enableWindow = FALSE;
	if (isMultiSelection())
	{
		if (!m_Grid.GetSelectedRows().empty())
			enableWindow = TRUE;
	}
	else if (!m_Grid.GetSelectedRows().empty())
	{
		enableWindow = (*m_Grid.GetSelectedRows().begin())->GetColor() != g_AlreadyInColor ? TRUE : FALSE;
	}
	m_btnOK.EnableWindow(enableWindow);
}

void DlgAddItemsToGroups::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDCANCEL,									m_btnCancel);
	DDX_Control(pDX, IDOK,										m_btnOK);
	DDX_Control(pDX, IDC_COMBO_ADDITEMSTOGROUPS_TYPEFILTERS,	m_comboTypeFilters);
	DDX_Control(pDX, IDC_STATIC_ADDITEMSTOGROUPS_GROUPSORUSERS, m_staticGroupsOrUsers);
	DDX_Control(pDX, IDC_STATIC_ADDITEMSTOGROUPS_CONTAINS,		m_staticContains);
	DDX_Control(pDX, IDC_EDIT_ADDITEMSTOGROUPS_CONTAINS,		m_editContains);
	DDX_Control(pDX, IDC_BTN_ADDITEMSTOGROUPS_CLEAR,			m_btnClear);
	DDX_Control(pDX, IDC_BTN_ADDITEMSTOGROUPS_REFRESH,			m_BtnRefresh);
	DDX_Control(pDX, IDC_BTN_ADDITEMSTOGROUPS_LOADALL_LEFT,		m_BtnLoadAllLeft);
	DDX_Control(pDX, IDC_STATIC_ADDITEMSTOGROUPS_LOADALL_LEFT,	m_staticLoadAllLeft);
	DDX_Control(pDX, IDC_BTN_ADDITEMSTOGROUPS_LOADALL_RIGHT,	m_BtnLoadAllRight);
	DDX_Control(pDX, IDC_STATIC_ADDITEMSTOGROUPS_LOADALL_RIGHT, m_staticLoadAllRight);
	DDX_Control(pDX, IDC_STATIC_ADDITEMSTOGROUPS_LISTITEMS,		m_staticExplanationText);
	DDX_Control(pDX, IDC_LIST_ADDITEMSTOGROUPS_LISTITEMS,		m_listItems);
}

BOOL DlgAddItemsToGroups::OnInitDialogSpecificResizable()
{
	SetWindowText(m_Title.c_str());
	m_staticExplanationText.SetWindowText(m_Explanation.c_str());

	if (getLoadType() != SELECT_USERS_FOR_SELECTION && getLoadType() != SELECT_GROUPS_FOR_SELECTION)
	{
		wstring text;
		for (auto& parent : m_SelectedParents)
		{
			const auto& id			= parent.first;
			const auto& displayName = parent.second.first;
			const auto& children	= parent.second.second;

			text = displayName;
			if (!children.empty())
				text += _YTEXTFORMAT(L" (%s)", Str::getStringFromNumber(children.size()).c_str());

			// PooledString::c_str() returns string pointer stored in StringPool, at least valid as long as m_SelectedParents lives.
			m_listItems.SetItemDataPtr(m_listItems.AddString(text.c_str()), (void*)id.c_str());
		}
	}

	if (m_listItems.GetCount() > 0)
	{
		m_listItems.SetSel(0, TRUE);
		OnListItemsLBNChange();
	}

	m_staticContains.SetWindowText(YtriaTranslate::Do(DlgAddItemsToGroups_OnInitDialogSpecificResizable_2, _YLOC("Contains:")).c_str());
	m_btnClear.SetWindowText(YtriaTranslate::Do(DlgAddItemsToGroups_OnInitDialogSpecificResizable_3, _YLOC("Clear")).c_str());
	m_btnOK.SetWindowText(YtriaTranslate::Do(DlgAddItemsToGroups_OnInitDialogSpecificResizable_5, _YLOC("OK")).c_str());
	m_btnOK.EnableWindow(FALSE);
	m_btnCancel.SetWindowText(YtriaTranslate::Do(DlgAddItemsToGroups_OnInitDialogSpecificResizable_6, _YLOC("Cancel")).c_str());

	m_staticLoadAllLeft.SetWindowText(YtriaTranslate::Do(DlgAddItemsToGroups_OnInitDialogSpecificResizable_13, _YLOC("Your currently loaded list")).c_str());
	m_staticLoadAllRight.SetWindowText(YtriaTranslate::Do(DlgAddItemsToGroups_OnInitDialogSpecificResizable_14, _YLOC("Load full directory")).c_str());

	CBitmap bitmap;
	bitmap.LoadBitmap(IDB_ICON_SEESELECTEDINACTIVEITEMS);
	BITMAP bmpinfo;
	bitmap.GetBitmap(&bmpinfo);

	m_BtnLoadAllLeft.SetDpiImageScaling(TRUE);
	m_BtnLoadAllRight.SetDpiImageScaling(TRUE);

	if (m_InitUsers.empty() && m_InitGroups.empty() && m_InitOrgContacts.empty())
	{
		// Please note that a Codejockstrap button bitmap must have a 1 pixel band around it because
		// Codejockstrap use the pixel at position (0, 0) as it's transparent colour ... yes! It's true.
		// They say that in works 99% of the time!!!
		// I guess I'm the 1%. (XTPDrawHelpers.cpp line 124)
		m_BtnLoadAllLeft.SetBitmap({}, IDB_ICON_SEESELECTEDINACTIVEITEMS);
		m_BtnLoadAllLeft.SetWindowPos(nullptr, 0, 0, HIDPI_XW(bmpinfo.bmWidth) - 1, HIDPI_YH(bmpinfo.bmHeight) - 1, SWP_NOMOVE | SWP_NOZORDER);
		m_staticLoadAllLeft.EnableWindow(FALSE);
		m_BtnLoadAllRight.SetBitmap({}, IDB_ICON_SEEALLACTIVEITEMS);
		m_BtnLoadAllRight.SetWindowPos(nullptr, 0, 0, HIDPI_XW(bmpinfo.bmWidth) - 1, HIDPI_YH(bmpinfo.bmHeight) - 1, SWP_NOMOVE | SWP_NOZORDER);
		m_BtnRefresh.SetWindowText(YtriaTranslate::Do(DlgAddItemsToGroups_OnInitDialogSpecificResizable_4, _YLOC("Refresh")).c_str());
	}
	else
	{
		m_BtnLoadAllLeft.SetBitmap({}, IDB_ICON_SEESELECTEDACTIVEITEMS);
		m_BtnLoadAllLeft.SetWindowPos(nullptr, 0, 0, HIDPI_XW(bmpinfo.bmWidth) - 1, HIDPI_YH(bmpinfo.bmHeight) - 1, SWP_NOMOVE | SWP_NOZORDER);
		m_staticLoadAllLeft.EnableWindow(TRUE);
		m_BtnLoadAllRight.SetBitmap({}, IDB_ICON_SEEALLINACTIVEITEMS);
		m_BtnLoadAllRight.SetWindowPos(nullptr, 0, 0, HIDPI_XW(bmpinfo.bmWidth) - 1, HIDPI_YH(bmpinfo.bmHeight) - 1, SWP_NOMOVE | SWP_NOZORDER);
		m_BtnRefresh.ShowWindow(SW_HIDE);
	}

	CWnd* progressBarStatic = GetDlgItem(IDC_ADDITEMSTOGROUP_PROGRESS);
	ASSERT(nullptr != progressBarStatic);
	if (nullptr != progressBarStatic)
	{
		CRect rcProgress;
		progressBarStatic->GetWindowRect(&rcProgress);
		ScreenToClient(&rcProgress);
		progressBarStatic->DestroyWindow();
        m_ProgressBar.Create(WS_CHILD | WS_VISIBLE, rcProgress, this, IDC_ADDITEMSTOGROUP_PROGRESS);
        m_ProgressBar.ModifyStyle(0, PBS_MARQUEE);
		m_ProgressBar.ShowWindow(SW_HIDE);
    }

	initGrid();

	if (hasOnlyGroups())
		m_staticGroupsOrUsers.SetWindowText(YtriaTranslate::Do(DlgAddItemsToGroups_OnInitDialogSpecificResizable_11, _YLOC("Filter by Group Type:")).c_str());
	else if (m_LoadType != SELECT_CLASSES)
		m_staticGroupsOrUsers.SetWindowText(YtriaTranslate::Do(DlgAddItemsToGroups_OnInitDialogSpecificResizable_7, _YLOC("Filter by Object Type:")).c_str());
	else// if (m_LoadType == SELECT_CLASSES)
	{
		m_staticGroupsOrUsers.ShowWindow(SW_HIDE);
		m_comboTypeFilters.ShowWindow(SW_HIDE);
	}

	// Get the size of the explanation text so we can move around the components so they fit like Lego.
	int yDelta = 0;
	{
		CClientDC dc(this);
		CFont* pOldFont = dc.SelectObject(&PmBridge_GetPM()->m_FontNormal);
		CRect rcExplanationText;
		dc.DrawText(m_Explanation.c_str(), rcExplanationText, DT_CALCRECT); // | DT_WORDBREAK);
		dc.SelectObject(pOldFont);
		const int explanationTextHeight = rcExplanationText.Height();
		m_staticExplanationText.GetWindowRect(&rcExplanationText);
		yDelta = explanationTextHeight - rcExplanationText.Height();
		rcExplanationText.InflateRect(0, 0, 0, yDelta);
		ScreenToClient(&rcExplanationText);
		m_staticExplanationText.MoveWindow(rcExplanationText);
	}

	// Move the Lego blocks.
	int listItemVResize = 0;
	if (getLoadType() == SELECT_USERS_FOR_SELECTION || getLoadType() == SELECT_GROUPS_FOR_SELECTION)
	{
		CRect rect;
		m_listItems.GetWindowRect(&rect);
		m_listItems.ShowWindow(SW_HIDE);

		yDelta -= rect.Height();
		listItemVResize = 0;
	}
	else
	{
		CRect rect;
		m_listItems.GetWindowRect(&rect);
		rect.OffsetRect(0, yDelta);
		ScreenToClient(&rect);
		m_listItems.MoveWindow(rect);
		listItemVResize = 30;
	}


	for (auto item : {static_cast<CWnd*>(&m_staticGroupsOrUsers)
					, static_cast<CWnd*>(&m_comboTypeFilters)
					, static_cast<CWnd*>(&m_staticLoadAllLeft)
					, static_cast<CWnd*>(&m_staticLoadAllRight)
					, static_cast<CWnd*>(&m_BtnLoadAllLeft)
					, static_cast<CWnd*>(&m_BtnLoadAllRight)
					, static_cast<CWnd*>(&m_staticContains)
					, static_cast<CWnd*>(&m_editContains)
					, static_cast<CWnd*>(&m_btnClear) })
	{
		CRect rect;
		item->GetWindowRect(&rect);
		rect.OffsetRect(0, yDelta);
		ScreenToClient(&rect);
		item->MoveWindow(rect);
	}
	{
		CRect rect;
		m_Grid.GetWindowRect(&rect);
		rect.top += yDelta;
		ScreenToClient(&rect);
		m_Grid.MoveWindow(rect);
	}

	AddAnchor(m_staticExplanationText, CSize(0, 0), CSize(100, 0));
	AddAnchor(m_listItems, CSize(0, 0), CSize(100, listItemVResize));
	AddAnchor(m_staticGroupsOrUsers, CSize(0, listItemVResize), CSize(15, listItemVResize));
	AddAnchor(m_comboTypeFilters, CSize(15, listItemVResize), CSize(100, listItemVResize));
	AddAnchor(m_BtnLoadAllLeft, CSize(0, listItemVResize), CSize(0, listItemVResize));
	AddAnchor(m_staticLoadAllLeft, CSize(0, listItemVResize), CSize(50, listItemVResize));
	AddAnchor(m_BtnLoadAllRight, CSize(50, listItemVResize), CSize(50, listItemVResize));
	AddAnchor(m_staticLoadAllRight, CSize(50, listItemVResize), CSize(100, listItemVResize));
	AddAnchor(m_staticContains, CSize(0, listItemVResize), CSize(15, listItemVResize));
	AddAnchor(m_editContains, CSize(15, listItemVResize), CSize(85, listItemVResize));
	AddAnchor(m_btnClear, CSize(85, listItemVResize), CSize(100, listItemVResize));
	AddAnchor(m_Grid, CSize(0, listItemVResize), CSize(100, 100));
	AddAnchor(m_BtnRefresh, CSize(0, 100), CSize(20, 100));
	AddAnchor(m_btnOK, CSize(60, 100), CSize(80, 100));
	AddAnchor(m_btnCancel, CSize(80, 100), CSize(100, 100));
	AddAnchor(m_ProgressBar, CSize(50, 50), CSize(50, 50));

	m_editContains.SetFocus();

	SetBkColor(RGB(255, 255, 255));
	m_staticGroupsOrUsers.SetBkColor(RGB(255, 255, 255));
	m_staticContains.SetBkColor(RGB(255, 255, 255));
	m_staticLoadAllLeft.SetBkColor(RGB(255, 255, 255));
	m_staticLoadAllRight.SetBkColor(RGB(255, 255, 255));
	m_staticExplanationText.SetBkColor(RGB(255, 255, 255));

	CExtResizableDialog::ShowSizeGrip(TRUE);
	SetDefID(IDC_BTN_ADDITEMSTOGROUPS_CLEAR);// do not close on return

	return FALSE;
}

LRESULT DlgAddItemsToGroups::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_COMMAND)
	{
		const auto hwndCombo = (HWND)lParam;
		if (hwndCombo == m_comboTypeFilters.GetSafeHwnd())
		{
			const WORD code = HIWORD(wParam);
			if (code == CBN_SELCHANGE)
			{	
				const int curSel = m_comboTypeFilters.GetCurSel();

				// Get values to filter out.
				m_LastValueFilter.clear();
				for (int i = 0; i < m_comboTypeFilters.GetCount(); i++)
				{
					if (m_comboTypeFilters.CheckGet(i))
					{
						CString csText;
						m_comboTypeFilters.GetLBText(i, csText);
						m_LastValueFilter.emplace_back((LPCTSTR)csText);
					}
				}

				if (!m_LastValueFilter.empty())
				{
					if (hasOnlyGroups())
						m_Grid.AddValueFilter(m_pColItemType, m_LastValueFilter);
					else
						m_Grid.AddValueFilter(m_Grid.GetColumnObjectType(), m_LastValueFilter);

					m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

					if (!m_FilterAutoTriggered)
					{
						// Grid update closes the combo drop list.
						// So, bring it back.
						m_comboTypeFilters.ShowDropDown();
					}
					m_FilterAutoTriggered = false;
				}
				else
					// At least one selection in the combo list.
					m_comboTypeFilters.CheckSet(curSel);
			}
		}
	}

	return ResizableDialog::WindowProc(message, wParam, lParam);
}

void DlgAddItemsToGroups::initTypeFilters()
{
	if (hasUsersGroupsAndOrgContacts())
	{
		if (0 == m_comboTypeFilters.GetCount())
		{
			// Hard coded (see Bug #181011.RO.008CD2: Guest should be shown as an object for filtering by default)
			// Fill it once here.
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessUser().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>().c_str()));
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessUserGuest().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>().c_str()));
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessOrgContact().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>().c_str()));

			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessGroup().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>().c_str()), false);
		}
	}
	else if (hasUsersAndGroups())
	{
		if (0 == m_comboTypeFilters.GetCount())
		{
			// Hard coded (see Bug #181011.RO.008CD2: Guest should be shown as an object for filtering by default)
			// Fill it once here.
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessUser().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>().c_str()));
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessUserGuest().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>().c_str()));

			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessGroup().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>().c_str()), false);
		}
	}
	else  if (hasOnlyUsers()) //users
	{
		if (0 == m_comboTypeFilters.GetCount())
		{
			// Hard coded (see Bug #181011.RO.008CD2: Guest should be shown as an object for filtering by default)
			// Fill it once here.
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessUser().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>().c_str()));
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessUserGuest().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>().c_str()));
		}
	}
	else  if (hasOnlyNonGuestUsers()) //non-guest users
	{
		if (0 == m_comboTypeFilters.GetCount())
		{
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessUser().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>().c_str()));
		}
	}
	else if (hasOnlyGroups())
	{
		if (0 == m_comboTypeFilters.GetCount())
		{
			// Hard coded
			// Fill it once here.
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessGroup::g_Office365Group.c_str()));
			//m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessGroup::g_DynamicGroup.c_str()));
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessGroup::g_DistributionList.c_str()));
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessGroup::g_SecurityGroup.c_str()));
			m_comboTypeFilters.CheckSet(m_comboTypeFilters.AddString(BusinessGroup::g_MailEnabledSecurityGroup.c_str()));
		}
	}
	else if (m_LoadType == SELECT_CLASSES)
	{
		// TODO
		m_comboTypeFilters.ShowWindow(SW_HIDE);
	}
	else
	{
		ASSERT(false);
	}

	m_FilterAutoTriggered = true;
	m_comboTypeFilters.TriggerSelChange();
}

void DlgAddItemsToGroups::OnListItemsLBNChange() 
{
	const int nCount = m_listItems.GetSelCount();
	CArray<int, int> listBoxSel;
	listBoxSel.SetSize(nCount);
	m_listItems.GetSelItems(nCount, listBoxSel.GetData());
	m_GrayedIds.clear();
	for (int i = 0; i <= listBoxSel.GetUpperBound(); ++i)
	{
		const auto sel = listBoxSel.GetAt(i);
		if (sel != LB_ERR)
		{
			auto it = m_SelectedParents.find(static_cast<wchar_t*>(m_listItems.GetItemDataPtr(sel)));
			ASSERT(m_SelectedParents.end() != it);
			if (m_SelectedParents.end() != it)
				m_GrayedIds.insert(it->second.second.begin(), it->second.second.end());
		}
	}

	for (auto row : m_Grid.GetBackendRows())
		setRowColor(row);

	m_Grid.UpdateMegaShark();
}

void DlgAddItemsToGroups::OnClear()
{
	m_editContains.SetWindowText(_YTEXT(""));
	OnEditChange();
}

void DlgAddItemsToGroups::OnEditChange()
{
	ASSERT(nullptr != m_pColPrincipalName);
	m_Grid.ClearFilters(m_pColPrincipalName->GetID(), false);

	CString csFilter;
	m_editContains.GetWindowText(csFilter);
	if (!csFilter.IsEmpty())
		m_Grid.AddFilter(m_pColPrincipalName, ID_EXT_HEADER_FILTER_MENU_TF_CONTAINS, csFilter, false);

	m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	m_editContains.SetFocus();
}

void DlgAddItemsToGroups::OnRefresh()
{
    m_BtnRefresh.EnableWindow(FALSE);
    m_Grid.ShowWindow(SW_HIDE);
	m_ProgressBar.SetPos(0);
	m_ProgressBar.SetMarquee(TRUE, 1);
    m_ProgressBar.ShowWindow(SW_SHOW);

    m_Grid.RemoveAllRows();

    SyncGrid(true);
}

void DlgAddItemsToGroups::OnLoadAll()
{
	if (m_staticLoadAllLeft.IsWindowEnabled())
	{
		m_BtnRefresh.EnableWindow(FALSE);
		m_Grid.ShowWindow(SW_HIDE);
		m_ProgressBar.SetPos(0);
		m_ProgressBar.SetMarquee(TRUE, 1);
		m_ProgressBar.ShowWindow(SW_SHOW);

		m_Grid.RemoveAllRows();

		SyncGrid(false);
	}
}

void DlgAddItemsToGroups::AddColDisplayName()
{
	if (nullptr == m_pColDisplayName)
		m_pColDisplayName = m_Grid.AddColumn(_YTEXT("DISPLAYNAME"), YtriaTranslate::Do(DlgAddItemsToGroups_loadUsersToGrid_1, _YLOC("Display Name")).c_str(), O365Grid::g_FamilyInfo.c_str());
}

void DlgAddItemsToGroups::initGrid()
{
	CWnd* pDummy = GetDlgItem(IDC_ADDITEMSTOGROUPS_DUMMY);
	ASSERT(nullptr != pDummy);
	CRect rcGrid;
	pDummy->GetWindowRect(&rcGrid);
	ScreenToClient(&rcGrid);
	m_Grid.Create(this, rcGrid);

	m_Grid.SetParentDlg(this);
	m_Grid.SetMultiSelection(isMultiSelection());
	m_Grid.AddSelectionObserver(this);

	wstring typeIdName;
	if (hasUsersAndGroups())
		typeIdName = YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_1, _YLOC("Group/User ID")).c_str();
	else if (hasUsersGroupsAndOrgContacts())
		typeIdName = YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_8, _YLOC("Group/User/Contact ID")).c_str();
	else if (hasOnlyUsers() || hasOnlyNonGuestUsers())
		typeIdName = YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_6, _YLOC("User ID")).c_str();
	else if (hasOnlyGroups())
		typeIdName = YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_2, _YLOC("Group ID")).c_str();
	else if (m_LoadType == SELECT_CLASSES)
		typeIdName = _T("Class ID");
	else
		ASSERT(FALSE);

	m_pColID = m_Grid.AddColumn(_YTEXT("ID"), typeIdName, O365Grid::g_FamilyInfo.c_str());

	m_Grid.SetColumnVisible(m_pColID, false);
	m_pColPrincipalName = m_Grid.AddColumn(_YTEXT("PRINCIPALNAME"), YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_3, _YLOC("Principal Name")).c_str(), O365Grid::g_FamilyInfo.c_str());
	if (m_LoadType == SELECT_CLASSES)
		m_Grid.SetColumnVisible(m_pColPrincipalName, false);

	if (hasOnlyGroups() || hasUsersAndGroups() || hasUsersGroupsAndOrgContacts())
	{
		m_pColIsTeam = m_Grid.AddColumnIcon(_YUID("ISTEAM"), YtriaTranslate::Do(GridGroups_customizeGrid_3, _YLOC("Is a Team")).c_str(), O365Grid::g_FamilyInfo);
		m_pColItemType = m_Grid.AddColumn(_YTEXT("ITEMTYPE"), YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_5, _YLOC("Group Type")).c_str(), O365Grid::g_FamilyInfo.c_str());
	}

	if (nullptr != m_pColItemType && m_LoadType == SELECT_CLASSES)
		m_Grid.SetColumnVisible(m_pColItemType, false);

	if (isShowEmail())
		m_pColIsShowEmail = m_Grid.AddColumn(_YTEXT("ISHOWEMAIL"), YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_10, _YLOC("Has Email")).c_str(), O365Grid::g_FamilyInfo.c_str(), GridBackendUtil::g_MinColWidth);

	if (m_LoadType == SELECT_CLASSES)
		AddColDisplayName();

	if (!m_InitUsers.empty() || !m_InitGroups.empty() || !m_InitOrgContacts.empty())
	{
		if (!m_InitUsers.empty() || !m_InitOrgContacts.empty())
			AddColDisplayName();

		for (const auto& user : m_InitUsers)
		{
			if (!RoleDelegationUtil::IsUserPrivilege(m_Privilege) || user.HasPrivilege(m_Privilege, m_Session))
			{
				GridBackendRow* pRow = m_Grid.AddRow();
				ASSERT(nullptr != pRow);
				if (nullptr != pRow)
				{
					GridUtil::SetUserRowObjectType(m_Grid, pRow, user);
					pRow->AddField(user.GetID(), m_pColID);
					pRow->AddField(user.GetDisplayName(), m_pColDisplayName);
					pRow->AddField(user.GetUserPrincipalName(), m_pColPrincipalName);
					//pRow->AddField(user.GetUserType(), m_pColItemType);
					if (nullptr != m_pColIsShowEmail)
					{
						bool IsEmail = user.GetMail() ? user.GetMail().get().IsEmpty() : false;
						pRow->AddField(
							IsEmail ? YtriaTranslate::Do(DlgRegAccountRegistration_validateCreateAccountInputs_4, _YLOC("Email")).c_str() : _YTEXT(""), 
							m_pColIsShowEmail, 
							IsEmail ? m_Grid.m_EmailIcon : NO_ICON);
					}
					setRowColor(pRow);
				}
			}
		}

		for (const auto& group : m_InitGroups)
		{
			if (!RoleDelegationUtil::IsGroupPrivilege(m_Privilege) || group.HasPrivilege(m_Privilege, m_Session))
			{
				GridBackendRow* pRow = m_Grid.AddRow();
				ASSERT(nullptr != pRow);
				if (nullptr != pRow)
				{
					m_Grid.SetRowObjectType(pRow, group);
					pRow->AddField(group.GetID(), m_pColID);
					pRow->AddField(group.GetDisplayName(), m_pColPrincipalName);
					ASSERT(nullptr != m_pColItemType);
					if (nullptr != m_pColItemType)
						pRow->AddField(group.GetCombinedGroupType(), m_pColItemType);
					if (nullptr != m_pColIsTeam)
						GridUtil::SetRowIsTeam(group.GetValue(_YUID(O365_GROUP_ISTEAM)), pRow, m_pColIsTeam, m_Grid.m_TeamIcon);
					setRowColor(pRow);
				}
			}
		}

		for (const auto& orgContact : m_InitOrgContacts)
		{
			GridBackendRow* pRow = m_Grid.AddRow();
			ASSERT(nullptr != pRow);
			if (nullptr != pRow)
			{
				m_Grid.SetRowObjectType(pRow, BusinessOrgContact());
				pRow->AddField(orgContact.GetID(), m_pColID);
				pRow->AddField(orgContact.GetDisplayName(), m_pColDisplayName);
				pRow->AddField(orgContact.GetMail(), m_pColPrincipalName);
				//pRow->AddField(pRow->GetField(m_Grid.GetColumnObjectType()).GetValueStr(), m_pColItemType);
				setRowColor(pRow);
			}
		}

		initGridUpdate();
	}
	else
	{
		m_Grid.ShowWindow(SW_HIDE);
		m_BtnRefresh.EnableWindow(FALSE);
		m_ProgressBar.SetPos(0);
		m_ProgressBar.SetMarquee(TRUE, 1);
		m_ProgressBar.ShowWindow(SW_SHOW);
		SyncGrid(false);
	}
}

void DlgAddItemsToGroups::SyncGrid(bool p_ForceSync)
{
	HWND hwnd = GetSafeHwnd();

	if (AutomationMustClose())// if automation is running in this dialog, process cannot be async
	{
		// TODO remove this when YDialog automation is ready to digest async processes
		CWaitCursor c;
		HWND hwnd = GetSafeHwnd();
		if (hasUsersAndGroups())
		{
			loadUsersToGridSync(m_TaskData, p_ForceSync);
			loadGroupsToGridSync(m_TaskData, p_ForceSync);
			finishGridUpdateSync();
		}
		else if (hasUsersGroupsAndOrgContacts())
		{
			loadUsersToGridSync(m_TaskData, p_ForceSync);
			loadGroupsToGridSync(m_TaskData, p_ForceSync);
			loadOrgContactsToGridSync(m_TaskData, p_ForceSync);
			finishGridUpdateSync();
		}
		else if (hasOnlyUsers() || hasOnlyNonGuestUsers())
		{
			loadUsersToGridSync(m_TaskData, p_ForceSync);
			finishGridUpdateSync();
		}
		else if (hasOnlyGroups())
		{
			loadGroupsToGridSync(m_TaskData, p_ForceSync);
			finishGridUpdateSync();
		}
		else if (m_LoadType == SELECT_CLASSES)
		{
			loadClassesToGridSync(m_TaskData, p_ForceSync);
			finishGridUpdateSync();
		}
		else
			ASSERT(FALSE);
	}
	else // human ... C.H.U.D.
	{
		if (hasUsersAndGroups())
		{
			loadUsersToGrid(hwnd, m_TaskData, p_ForceSync).Then([=](){
				loadGroupsToGrid(hwnd, m_TaskData, p_ForceSync).GetTask().wait();
				YCallbackMessage::DoPost([this, hwnd]()
				{
					if (::IsWindow(hwnd))
						finishGridUpdateSync();
				});
			});
		}
		else if (hasUsersGroupsAndOrgContacts())
		{
			loadUsersToGrid(hwnd, m_TaskData, p_ForceSync).Then([=](){
				loadGroupsToGrid(hwnd, m_TaskData, p_ForceSync).GetTask().wait();
				loadOrgContactsToGrid(hwnd, m_TaskData, p_ForceSync).GetTask().wait();
				YCallbackMessage::DoPost([this, hwnd]()
				{
					if (::IsWindow(hwnd))
						finishGridUpdateSync();
				});
			});
		}
		else if (hasOnlyUsers() || hasOnlyNonGuestUsers())
		{
			loadUsersToGrid(hwnd, m_TaskData, p_ForceSync).Then([this, hwnd]() {
				YCallbackMessage::DoPost([this, hwnd]()
				{
					if (::IsWindow(hwnd))
						finishGridUpdateSync();
				});
			});
		}
		else if (hasOnlyGroups())
		{
			loadGroupsToGrid(hwnd, m_TaskData, p_ForceSync).Then([this, hwnd]() {
				YCallbackMessage::DoPost([this, hwnd]()
				{
					if (::IsWindow(hwnd))
						finishGridUpdateSync();
				});
			});
		}
		else if (m_LoadType == SELECT_CLASSES)
		{
			loadClassesToGrid(hwnd, m_TaskData, p_ForceSync).Then([this, hwnd]() {
				YCallbackMessage::DoPost([this, hwnd]()
				{
					if (::IsWindow(hwnd))
						finishGridUpdateSync();
				});
			});
		}
		else
			ASSERT(FALSE);	
	}
}

void DlgAddItemsToGroups::initGridUpdate()
{
	CString csFilter;
	m_editContains.GetWindowText(csFilter);

	if (hasOnlyGroups())
	{
		ASSERT(nullptr != m_pColItemType);
		if (nullptr != m_pColItemType)
			m_Grid.GetBackend().ClearFilters(m_pColItemType->GetID());
	}
	else
		m_Grid.GetBackend().ClearFilters(m_Grid.GetColumnObjectType()->GetID());

	m_Grid.GetBackend().ClearFilters(m_pColPrincipalName->GetID());
	
	m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

	map<GridBackendColumn*, LONG> resizeColumns
	{
		{ m_pColID, -1 },
		{ m_pColPrincipalName, -1 },
		{ m_pColItemType, -1 },
		{ m_pColDisplayName, -1 }
	};
	vector<GridBackendColumn*> autoResizeWithGridcolumns{ m_pColID, m_pColItemType, m_pColDisplayName };

	for (auto it = resizeColumns.begin(); it != resizeColumns.end();)
	{
		if (nullptr == it->first)
			it = resizeColumns.erase(it);
		else
			++it;
	}

	autoResizeWithGridcolumns.erase(std::remove_if(autoResizeWithGridcolumns.begin(), autoResizeWithGridcolumns.end(), [](auto& col) {return nullptr == col; }), autoResizeWithGridcolumns.end());

	m_Grid.AutoResizeColumns(resizeColumns, CacheGrid::AutoResizeRule(true, true));
	m_Grid.SetColumnsAutoSizeWithGridResize(autoResizeWithGridcolumns, true);

	if (!csFilter.IsEmpty())
		m_Grid.AddFilter(m_pColPrincipalName, ID_EXT_HEADER_FILTER_MENU_TF_CONTAINS, csFilter, false);

	initTypeFilters();
}

const DlgAddItemsToGroups::LOADTYPE DlgAddItemsToGroups::getLoadType() const
{
	return m_LoadType;
}

void DlgAddItemsToGroups::addUsersTogrid(const vector<CachedUser>& p_Users)
{
	for (const auto& user : p_Users)
	{
		if ((!hasOnlyNonGuestUsers() || user.GetUserType() != BusinessUser::g_UserTypeGuest) && (!RoleDelegationUtil::IsUserPrivilege(m_Privilege) || user.HasPrivilege(m_Privilege, m_Session)))
		{
			GridBackendRow* pRow = m_Grid.AddRow();
			ASSERT(nullptr != pRow);
			if (nullptr != pRow)
			{
				GridUtil::SetUserRowObjectType(m_Grid, pRow, BusinessUser(user));
				pRow->AddField(user.GetID(), m_pColID);
				pRow->AddField(user.GetDisplayName(), m_pColDisplayName);
				pRow->AddField(user.GetUserPrincipalName(), m_pColPrincipalName);
				// pRow->AddField(user.GetUserType(), m_pColItemType);
				if (nullptr != m_pColIsShowEmail)
					pRow->AddField(
						user.IsEmailPresent() ? YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_11, _YLOC("Email")).c_str() : _YTEXT(""), 
						m_pColIsShowEmail, 
						user.IsEmailPresent() ? m_Grid.m_EmailIcon : NO_ICON);
				setRowColor(pRow);
			}
		}
	}
}

void DlgAddItemsToGroups::addGroupsTogrid(const vector<CachedGroup>& p_Groups)
{
	for (const auto& group : p_Groups)
	{
		if (!RoleDelegationUtil::IsGroupPrivilege(m_Privilege) || group.HasPrivilege(m_Privilege, m_Session))
		{
			GridBackendRow* pRow = m_Grid.AddRow();
			ASSERT(nullptr != pRow);
			if (nullptr != pRow)
			{
				BusinessGroup bg(group);
				m_Grid.SetRowObjectType(pRow, bg);
				pRow->AddField(bg.GetID(), m_pColID);
				pRow->AddField(bg.GetDisplayName(), m_pColPrincipalName);
				ASSERT(nullptr != m_pColItemType);
				if (nullptr != m_pColItemType)
					pRow->AddField(bg.GetCombinedGroupType(), m_pColItemType);
				if (nullptr != m_pColIsTeam)
					GridUtil::SetRowIsTeam(bg.GetValue(_YUID(O365_GROUP_ISTEAM)), pRow, m_pColIsTeam, m_Grid.m_TeamIcon);
				setRowColor(pRow);
			}
		}
	}
}

void DlgAddItemsToGroups::addOrgContactsTogrid(const vector<CachedOrgContact>& p_OrgContacts)
{
	for (const auto& orgCon : p_OrgContacts)
	{
		GridBackendRow* pRow = m_Grid.AddRow();
		ASSERT(nullptr != pRow);
		if (nullptr != pRow)
		{
			m_Grid.SetRowObjectType(pRow, BusinessOrgContact());
			pRow->AddField(orgCon.GetId(), m_pColID);
			pRow->AddField(orgCon.GetDisplayName(), m_pColDisplayName);
			pRow->AddField(orgCon.GetMail(), m_pColPrincipalName);
			//pRow->AddField(pRow->GetField(m_Grid.GetColumnObjectType()).GetValueStr(), m_pColItemType);
			setRowColor(pRow);
		}
	}
}

void DlgAddItemsToGroups::addClassesTogrid(const vector<EducationClass>& p_Classes)
{
	for (const auto& curClass : p_Classes)
	{
		DisplayClass(curClass);
	}
}

TaskWrapper<void> DlgAddItemsToGroups::loadUsersToGrid(HWND p_Hwnd, YtriaTaskData p_TaskData, bool p_ForceSync)
{
	AddColDisplayName();

    auto postSyncCallback = [this, p_Hwnd](const vector<CachedUser>& p_Users) {
        if (::IsWindow(p_Hwnd))
            YDataCallbackMessage<vector<CachedUser>>::DoPost(p_Users, std::bind(&DlgAddItemsToGroups::addUsersTogrid, this, std::placeholders::_1));
    };

	auto logger = std::make_shared<BasicPageRequestLogger>(_T("users"));
    if (!p_ForceSync)
    {
		return YSafeCreateTask([this, p_TaskData, p_Hwnd, logger, postSyncCallback]()
		{
			if (!m_GraphCache.IsSqlUsersCacheLoaded() && m_GraphCache.IsSqlUsersCacheFull())
				m_GraphCache.GetSqlCachedUsers(p_Hwnd, { UBI::MIN }); // Request users for memory cache (minimum level)

			// Request done only if never requested
			if (m_GraphCache.IsAllUsersSynced())
			{
				postSyncCallback(m_GraphCache.GetCachedUsers());
			}
			else
			{
				m_GraphCache.GetUpToDateUsers<CachedUser>({}, logger, GraphCache::PreferredMode::DELTASYNC, p_TaskData).Then([postSyncCallback](const vector<CachedUser>& p_Users)
				{
					postSyncCallback(p_Users);
				}, p_TaskData.GetToken()).GetTask().get();
			}
		});
    }
    else
    {
		// Request users for memory cache (minimum level)
		return m_GraphCache.GetUpToDateUsers<CachedUser>({}, logger, GraphCache::PreferredMode::DELTASYNC, p_TaskData).Then([postSyncCallback](const vector<CachedUser>& p_Users)
			{
				postSyncCallback(p_Users);
			}, p_TaskData.GetToken());
    }
}

void DlgAddItemsToGroups::loadUsersToGridSync(YtriaTaskData p_TaskData, bool p_ForceSync)
{
	AddColDisplayName();

	vector<CachedUser> users;
	auto logger = std::make_shared<BasicPageRequestLogger>(_T("users"));
	if (!p_ForceSync && m_GraphCache.IsSqlUsersCacheFull())
	{
		if (!m_GraphCache.IsSqlUsersCacheLoaded())
			m_GraphCache.GetSqlCachedUsers(m_hWnd, { UBI::MIN }); // Load users from sql to for memory cache (minimum level)
		users = m_GraphCache.GetCachedUsers();
	}
	else
	{
		users = m_GraphCache.GetUpToDateUsers<CachedUser>({}, logger, GraphCache::PreferredMode::DELTASYNC, p_TaskData).get();
	}

	addUsersTogrid(users);
}

TaskWrapper<void> DlgAddItemsToGroups::loadGroupsToGrid(HWND p_Hwnd, YtriaTaskData p_TaskData, bool p_ForceSync)
{
    auto postSyncCallback = [this, p_Hwnd](const vector<CachedGroup>& p_Groups) {
        if (::IsWindow(p_Hwnd))
            YDataCallbackMessage<vector<CachedGroup>>::DoPost(p_Groups, std::bind(&DlgAddItemsToGroups::addGroupsTogrid, this, std::placeholders::_1));
    };

	auto logger = std::make_shared<BasicPageRequestLogger>(_T("groups"));
    if (!p_ForceSync)
    {
		return YSafeCreateTask([this, p_TaskData, p_Hwnd, postSyncCallback, logger]()
		{
			if (!m_GraphCache.IsSqlGroupsCacheLoaded() && m_GraphCache.IsSqlGroupsCacheFull())
				m_GraphCache.GetSqlCachedGroups(p_Hwnd, { GBI::MIN }); // Request groups for memory cache (minimum level)
			
			// Request done only if never requested
			if (m_GraphCache.IsAllGroupsSynced())
			{
				postSyncCallback(m_GraphCache.GetCachedGroups());
			}
			else
			{
				m_GraphCache.GetUpToDateGroups<CachedGroup>({}, logger, GraphCache::PreferredMode::DELTASYNC, p_TaskData).Then([postSyncCallback](const vector<CachedGroup>& p_Groups)
					{
						postSyncCallback(p_Groups);
					}, p_TaskData.GetToken()).GetTask().get();
			}
		});
    }
    else
    {
		// Request groups for memory cache (minimum level)
		return m_GraphCache.GetUpToDateGroups<CachedGroup>({}, logger, GraphCache::PreferredMode::DELTASYNC, p_TaskData).Then([postSyncCallback](const vector<CachedGroup>& p_Groups)
			{
				postSyncCallback(p_Groups);
			}, p_TaskData.GetToken());
    }
}

TaskWrapper<void> DlgAddItemsToGroups::loadOrgContactsToGrid(HWND p_Hwnd, YtriaTaskData p_TaskData, bool p_ForceSync)
{
	auto postSyncCallback = [this, p_Hwnd](const vector<CachedOrgContact>& p_OrgContacts) {
		if (::IsWindow(p_Hwnd))
			YDataCallbackMessage<vector<CachedOrgContact>>::DoPost(p_OrgContacts, std::bind(&DlgAddItemsToGroups::addOrgContactsTogrid, this, std::placeholders::_1));
	};
	
	auto logger = std::make_shared<BasicPageRequestLogger>(_T("organization contacts"));

	if (!p_ForceSync)
	{
		return m_GraphCache.SyncAllOrgContactsOnce(logger, p_TaskData).Then([this, p_Hwnd, postSyncCallback](const vector<CachedOrgContact>& p_OrgContacts) {
			postSyncCallback(p_OrgContacts);
		}, p_TaskData.GetToken());
	}
	else
	{
		return m_GraphCache.SyncAllOrgContacts(logger, p_TaskData).Then([this, p_Hwnd, postSyncCallback](const vector<CachedOrgContact>& p_OrgContacts) {
			postSyncCallback(p_OrgContacts);
		}, p_TaskData.GetToken());
	}
}

TaskWrapper<void> DlgAddItemsToGroups::loadClassesToGrid(HWND p_Hwnd, YtriaTaskData p_TaskData, bool p_ForceSync)
{
	auto postSyncCallback = [this, p_Hwnd](const vector<EducationClass>& p_Classes) {
		if (::IsWindow(p_Hwnd))
			YDataCallbackMessage<vector<EducationClass>>::DoPost(p_Classes, std::bind(&DlgAddItemsToGroups::addClassesTogrid, this, std::placeholders::_1));
	};

	//if (p_ForceSync || !p_ForceSync)
	//{
	//	ASSERT(nullptr != m_Session);
	//	if (nullptr != m_Session)
	//	{
	//		auto requester = std::make_shared<EducationClassListRequester>();
	//		
	//	}
	//}
	auto classMembersLogger = std::make_shared<BasicPageRequestLogger>(_T("school classes"));
	auto requester = std::make_shared<EducationClassListRequester>(classMembersLogger);
	return safeTaskCall(requester->Send(m_Session, p_TaskData).Then([requester, postSyncCallback]() {
		postSyncCallback(requester->GetData());
	}), m_Session->GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData.GetToken(), p_TaskData);
}

void DlgAddItemsToGroups::loadGroupsToGridSync(YtriaTaskData p_TaskData, bool p_ForceSync)
{
	vector<CachedGroup> groups;

	auto logger = std::make_shared<BasicPageRequestLogger>(_T("groups"));
	if (!p_ForceSync && m_GraphCache.IsSqlGroupsCacheFull())
	{
		if (!m_GraphCache.IsSqlGroupsCacheLoaded())
			m_GraphCache.GetSqlCachedGroups(m_hWnd, { GBI::MIN }); // Load users from sql to for memory cache (minimum level)
		groups = m_GraphCache.GetCachedGroups();
	}
	else
	{
		groups = m_GraphCache.GetUpToDateGroups<CachedGroup>({}, logger, GraphCache::PreferredMode::DELTASYNC, p_TaskData).get();
	}

	addGroupsTogrid(groups);
}

void DlgAddItemsToGroups::loadOrgContactsToGridSync(YtriaTaskData p_TaskData, bool p_ForceSync)
{
	vector<CachedOrgContact> orgContacts;

	auto logger = std::make_shared<BasicPageRequestLogger>(_T("organization contacts"));

	if (!p_ForceSync)
		orgContacts = m_GraphCache.SyncAllOrgContactsOnce(logger, p_TaskData).get();
	else
		orgContacts = m_GraphCache.SyncAllOrgContacts(logger, p_TaskData).get();

	addOrgContactsTogrid(orgContacts);
}

void DlgAddItemsToGroups::loadClassesToGridSync(YtriaTaskData p_TaskData, bool p_ForceSync)
{
	vector<EducationClass> classes;

	// TODO: Classes requester
	/*if (!p_ForceSync)
		classes = ;
	else
		classes = ;*/

	addClassesTogrid(classes);
}

void DlgAddItemsToGroups::finishGridUpdateSync()
{
	initGridUpdate();

	m_ProgressBar.SetMarquee(FALSE, 0);
	m_ProgressBar.ShowWindow(SW_HIDE);
	m_Grid.ShowWindow(SW_SHOW);
	m_BtnRefresh.ShowWindow(SW_SHOW);
	m_BtnRefresh.EnableWindow(TRUE);
	m_BtnRefresh.SetWindowText(YtriaTranslate::Do(DlgAddItemsToGroups_finishGridUpdate_1, _YLOC("Refresh")).c_str());
	m_BtnLoadAllLeft.SetBitmap({}, IDB_ICON_SEESELECTEDINACTIVEITEMS);
	m_staticLoadAllLeft.EnableWindow(FALSE);
	m_BtnLoadAllRight.SetBitmap({}, IDB_ICON_SEEALLACTIVEITEMS);
	m_staticLoadAllRight.SetWindowText(YtriaTranslate::Do(DlgAddItemsToGroups_finishGridUpdateSync_1, _YLOC("Directory is fully loaded")).c_str());
}

void DlgAddItemsToGroups::shiftCtrlVertically(CWnd* p_Wnd, int offset)
{
    ASSERT(nullptr != p_Wnd);
    if (nullptr != p_Wnd)
    {
        CRect rect;
        p_Wnd->GetWindowRect(&rect);
        ScreenToClient(&rect);
        rect.OffsetRect(0, HIDPI_YH(offset));
        p_Wnd->MoveWindow(rect);
    }
}

bool DlgAddItemsToGroups::hasOnlyNonGuestUsers() const
{
	return getLoadType() == SELECT_USERS_FOR_ROLEDELEGATION;
}

bool DlgAddItemsToGroups::hasOnlyUsers() const
{
	return getLoadType() == SELECT_USERS_FOR_GROUPS
		|| getLoadType() == SELECT_USERS_FOR_ROLES
		|| getLoadType() == SELECT_USERS_FOR_SELECTION
		|| getLoadType() == SELECT_USER_FOR_MANAGER;
}

bool DlgAddItemsToGroups::hasOnlyGroups() const
{
	return getLoadType() == SELECT_GROUPS_FOR_USERS
		|| getLoadType() == SELECT_GROUPS_FOR_MEMBERS
		|| getLoadType() == SELECT_GROUPS_FOR_SELECTION;
}

bool DlgAddItemsToGroups::hasUsersAndGroups() const
{
	return getLoadType() == SELECT_USERSANDGROUPS_FOR_GROUPS;
}

bool DlgAddItemsToGroups::hasUsersGroupsAndOrgContacts() const
{
	return getLoadType() == SELECT_USERSGROUPSANDORGCONTACTS_FOR_GROUPS
		|| getLoadType() == SELECT_USERSGROUPSANDORGCONTACTS_FOR_GROUPS_SHOWEMAIL;
}

bool DlgAddItemsToGroups::isShowEmail() const
{
	return getLoadType() == SELECT_USERSGROUPSANDORGCONTACTS_FOR_GROUPS_SHOWEMAIL;
}

bool DlgAddItemsToGroups::isMultiSelection() const
{
	return getLoadType() != SELECT_USER_FOR_MANAGER;
}

void DlgAddItemsToGroups::setRowColor(GridBackendRow* p_Row)
{
	if (getLoadType() == SELECT_USERS_FOR_SELECTION || getLoadType() == SELECT_GROUPS_FOR_SELECTION)
	{
		if (m_SelectedParents.end() != m_SelectedParents.find(p_Row->GetField(m_pColID).GetValueStr()))
			p_Row->SetColor(g_AlreadyInColor);
		else
			p_Row->SetColor(GridBackendField::g_NoColor);
	}
	else
	{
		if (m_GrayedIds.end() != m_GrayedIds.find(p_Row->GetField(m_pColID).GetValueStr()))
			p_Row->SetColor(g_AlreadyInColor);
		else
			p_Row->SetColor(GridBackendField::g_NoColor);
	}
}

void DlgAddItemsToGroups::DisplayClass(const EducationClass& p_Class)
{
	GridBackendRow* pRow = m_Grid.AddRow();
	ASSERT(nullptr != pRow);
	if (nullptr != pRow)
	{
		m_Grid.SetRowObjectType(pRow, EducationClass());
		pRow->AddField(p_Class.GetID(), m_pColID);
		pRow->AddField(p_Class.GetDisplayName(), m_pColDisplayName);
		//pRow->AddField(p_Class.GetMail(), m_pColPrincipalName);
		//pRow->AddField(pRow->GetField(m_Grid.GetColumnObjectType()).GetValueStr(), m_pColItemType);
		setRowColor(pRow);
	}
}

void DlgAddItemsToGroups::setAutomationFieldMap()
{
	addAutomationFieldMap(g_AutomationNameSelectionGrid,	&m_Grid);
	addAutomationFieldMap(g_ParamNameLoadDirectory,			&m_BtnLoadAllRight);
}

void DlgAddItemsToGroups::setAutomationRadioButtonGroups()
{
}

void DlgAddItemsToGroups::setAutomationListChoicesGroups()
{
}