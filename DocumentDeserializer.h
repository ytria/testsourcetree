#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Document.h"

namespace Cosmos
{
    class DocumentDeserializer : public JsonObjectDeserializer, public Encapsulate<Cosmos::Document>
    {
    public:
        void DeserializeObject(const web::json::object& p_Object) override;
    };
}

