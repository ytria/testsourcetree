#pragma once

#include "IRequester.h"
#include "InvokeResultWrapper.h"

class IPSObjectCollectionDeserializer;
class IRequestLogger;

class AdSyncConnectorRequester : public IRequester
{
public:
	AdSyncConnectorRequester(const std::shared_ptr<IPSObjectCollectionDeserializer>& p_Deserializer, const std::shared_ptr<IRequestLogger>& p_RequestLogger);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const std::shared_ptr<InvokeResultWrapper>& GetResult() const;

private:
	std::shared_ptr<IPSObjectCollectionDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
	std::shared_ptr<InvokeResultWrapper> m_Result;
};

