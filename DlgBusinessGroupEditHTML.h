#pragma once

#include "BusinessGroup.h"
#include "DlgFormsHTML.h"
#include "Organization.h"

class GraphCache;

class DlgBusinessGroupEditHTML : public DlgFormsHTML
{
public:
	DlgBusinessGroupEditHTML(
		vector<BusinessGroup>& p_BusinessGroups, 
		const std::set<wstring>& p_ListFieldErrors,
		boost::YOpt<Organization> p_Org, const GraphCache& p_GraphCache, DlgFormsHTML::Action p_Action, CWnd* p_Parent);
	virtual ~DlgBusinessGroupEditHTML();


protected:
	virtual void generateJSONScriptData();


	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data);
	virtual wstring getDialogTitle() const override;

private:
	using StringGetter = std::function<const boost::YOpt<PooledString>&(const BusinessGroup&)>;
	using StringSetter = std::function<void(BusinessGroup&, const boost::YOpt<PooledString>&)>;

	using BoolGetter = std::function<const boost::YOpt<bool>&(const BusinessGroup&)>;
	using BoolSetter = std::function<void(BusinessGroup&, const boost::YOpt<bool>&)>;

	void addIsTeamEditor();
	void addStringEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableGroupTypes);
	void addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues, vector<wstring> p_ApplicableGroupTypes);
	void addUserNameEditor(StringGetter p_Getter, StringSetter p_SetterLeft, StringSetter p_SetterRight, const wstring& p_PropNameLeft, const wstring& p_PropNameRight, const wstring& p_PropLabel, uint32_t p_Flags, const vector<std::tuple<wstring, wstring>>& p_LabelsAndValues, vector<wstring> p_ApplicableUserTypes);
	void addBoolEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableGroupTypes);
    void addBoolToggleEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableGroupTypes);

	void addCategory(const wstring& p_CategoryName, bool p_ExpandByDefault, vector<wstring> p_ApplicableGroupTypes);

	static uint32_t getRestrictionsImpl(const BusinessGroup& bg, uint32_t p_Flags, const vector<wstring>& p_ApplicableGroupTypes);

	bool hasProperty(const wstring& p_PropertyName) const;

	// For teams
#define TEAM_SETTINGS_BOOL(_Prefix) \
	using Team##_Prefix##SettingsBoolGetter = std::function<const boost::YOpt<bool>&(const Team##_Prefix##Settings&)>; \
	using Team##_Prefix##SettingsBoolSetter = std::function<void(Team##_Prefix##Settings&, const boost::YOpt<bool>&)>; \
	void addBoolToggleEditor(Team##_Prefix##SettingsBoolGetter p_Getter, Team##_Prefix##SettingsBoolSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, const uint32_t p_Flags); \
	std::map<wstring, Team##_Prefix##SettingsBoolSetter> m_Team##_Prefix##SettingsBoolSetters; \

	TEAM_SETTINGS_BOOL(Member)
	TEAM_SETTINGS_BOOL(Guest)
	TEAM_SETTINGS_BOOL(Messaging)
	TEAM_SETTINGS_BOOL(Fun)

#undef TEAM_SETTINGS_BOOL

#define TEAM_SETTINGS_STRING(_Prefix) \
	using Team##_Prefix##SettingsStringGetter = std::function<const boost::YOpt<PooledString>&(const Team##_Prefix##Settings&)>; \
	using Team##_Prefix##SettingsStringSetter = std::function<void(Team##_Prefix##Settings&, const boost::YOpt<PooledString>&)>; \
	void addComboEditor(Team##_Prefix##SettingsStringGetter p_Getter, Team##_Prefix##SettingsStringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues); \
	std::map<wstring, Team##_Prefix##SettingsStringSetter> m_Team##_Prefix##SettingsStringSetters; \

	TEAM_SETTINGS_STRING(Fun)

#undef TEAM_SETTINGS_STRING

	int addErrorFlagIfNecessary(int p_Flags, const wstring& p_PropName);

private:
	vector<BusinessGroup>& m_BusinessGroups;
	const std::set<wstring>& m_ListFieldErrors;

	std::map<wstring, StringSetter> m_StringSetters;
	std::map<wstring, BoolSetter> m_BoolSetters;

	boost::YOpt<Organization> m_Org;

	const GraphCache& m_GraphCache;
};
