#include "ContactDeserializer.h"

#include "EmailAddressDeserializer.h"
#include "EmailAddress.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "MsGraphFieldNames.h"
#include "PhysicalAddressDeserializer.h"

void ContactDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_ASSISTANTNAME), m_Data.m_AssistantName, p_Object);
    JsonSerializeUtil::DeserializeDateOnly(_YTEXT(O365_CONTACT_BIRTHDAY), m_Data.m_Birthday, p_Object);
    
    {
        PhysicalAddressDeserializer pad;
        if (JsonSerializeUtil::DeserializeAny(pad, _YTEXT(O365_CONTACT_BUSINESSADDRESS), p_Object))
        {
            m_Data.m_BusinessAddressCity = pad.GetData().City;
            m_Data.m_BusinessAddressCountryOrRegion = pad.GetData().CountryOrRegion;
            m_Data.m_BusinessAddressPostalCode = pad.GetData().PostalCode;
            m_Data.m_BusinessAddressState = pad.GetData().State;
            m_Data.m_BusinessAddressStreet = pad.GetData().Street;
        }
    }

	JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_BUSINESSHOMEPAGE), m_Data.m_BusinessHomePage, p_Object);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT(O365_CONTACT_BUSINESSPHONES), p_Object))
            m_Data.m_BusinessPhones = lsd.GetData();
    }

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT(O365_CONTACT_CATEGORIES), p_Object))
            m_Data.m_Categories = lsd.GetData();
    }

	JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_CHANGEKEY), m_Data.m_ChangeKey, p_Object);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT(O365_CONTACT_CHILDREN), p_Object))
            m_Data.m_Children = lsd.GetData();
    }

    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_COMPANYNAME), m_Data.m_CompanyName, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT(O365_CONTACT_CREATEDDATETIME), m_Data.m_CreatedDateTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_DEPARTMENT), m_Data.m_Department, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_DISPLAYNAME), m_Data.m_DisplayName, p_Object);

    {
        ListDeserializer<EmailAddress, EmailAddressDeserializer> listDes;
        if (JsonSerializeUtil::DeserializeAny(listDes, _YTEXT(O365_CONTACT_EMAILADDRESSES), p_Object))
        {
            const auto& addresses = listDes.GetData();
            m_Data.m_EmailAddressesAddress.reserve(addresses.size());
            m_Data.m_EmailAddressesName.reserve(addresses.size());
            m_Data.m_EmailAddressesAndName.reserve(addresses.size());
            for (const auto& emailAddress : addresses)
            {
                PooledString address = emailAddress.m_Address != boost::none ? *emailAddress.m_Address : _YTEXT("");
                PooledString name = emailAddress.m_Name != boost::none ? *emailAddress.m_Name : _YTEXT("");
                m_Data.m_EmailAddressesAddress.push_back(address);
                m_Data.m_EmailAddressesName.push_back(name);

                CString nameAndAddress;
                nameAndAddress.Format(_YTEXT("<%s> %s"), name, address);
                m_Data.m_EmailAddressesAndName.push_back((LPCTSTR)nameAndAddress);
            }
        }
    }

    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_FILEAS), m_Data.m_FileAs, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_GENERATION), m_Data.m_Generation, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_GIVENNAME), m_Data.m_GivenName, p_Object);

    {
        PhysicalAddressDeserializer pad;
        if (JsonSerializeUtil::DeserializeAny(pad, _YTEXT(O365_CONTACT_HOMEADDRESS), p_Object))
        {
            m_Data.m_HomeAddressCity = pad.GetData().City;
            m_Data.m_HomeAddressCountryOrRegion = pad.GetData().CountryOrRegion;
            m_Data.m_HomeAddressPostalCode = pad.GetData().PostalCode;
            m_Data.m_HomeAddressState = pad.GetData().State;
            m_Data.m_HomeAddressStreet = pad.GetData().Street;
        }
    }

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT(O365_CONTACT_HOMEPHONES), p_Object))
            m_Data.m_HomePhones = lsd.GetData();
    }

	JsonSerializeUtil::DeserializeId(_YTEXT(O365_ID), m_Data.m_Id, p_Object, true);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT(O365_CONTACT_IMADDRESSES), p_Object))
            m_Data.m_ImAddresses = lsd.GetData();
    }

    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_INITIALS), m_Data.m_Initials, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_JOBTITLE), m_Data.m_JobTitle, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT(O365_CONTACT_LASTMODIFIEDDATETIME), m_Data.m_LastModifiedDateTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_MANAGER), m_Data.m_Manager, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_MIDDLENAME), m_Data.m_MiddleName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_MOBILEPHONE), m_Data.m_MobilePhone, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_NICKNAME), m_Data.m_Nickname, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_OFFICELOCATION), m_Data.m_OfficeLocation, p_Object);

    {
        PhysicalAddressDeserializer pad;
        if (JsonSerializeUtil::DeserializeAny(pad, _YTEXT(O365_CONTACT_OTHERADDRESS), p_Object))
        {
            m_Data.m_OtherAddressCity = pad.GetData().City;
            m_Data.m_OtherAddressCountryOrRegion = pad.GetData().CountryOrRegion;
            m_Data.m_OtherAddressPostalCode = pad.GetData().PostalCode;
            m_Data.m_OtherAddressState = pad.GetData().State;
            m_Data.m_OtherAddressStreet = pad.GetData().Street;
        }
    }

    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_PARENTFOLDERID), m_Data.m_ParentFolderId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_PERSONALNOTES), m_Data.m_PersonalNotes, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_PROFESSION), m_Data.m_Profession, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_SPOUSENAME), m_Data.m_SpouseName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_SURNAME), m_Data.m_Surname, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_TITLE), m_Data.m_Title, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_YOMICOMPANYNAME), m_Data.m_YomiCompanyName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_YOMIGIVENNAME), m_Data.m_YomiGivenName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_CONTACT_YOMISURNAME), m_Data.m_YomiSurnameName, p_Object);
}
