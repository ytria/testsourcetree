#pragma once

enum class SessionStatus
{
    INVALID,
    ACTIVE,
    STANDBY,
    CACHED,
    INACTIVE,
    LOCKED
};
