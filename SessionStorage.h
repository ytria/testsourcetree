#pragma once

class Role2
{
public:
	int64_t m_Id;
	YTimeDate m_CreatedOn;
	YTimeDate m_LastUsedOn;

	bool m_IsFavorite;
};

class SavedSession
{
public:
	int32_t m_AccessCount;
	YTimeDate m_CreatedOn;
	wstring m_UserEmailAddress;
	bool m_IsFavorite;
	wstring m_UserFullName;
	YTimeDate m_LastUsedOn;
	wstring m_ProfilePicture;
	wstring m_Type;
	bool m_ShowBaseSession;
	wstring m_TenantName;
	wstring m_TenantDomain;
	wstring m_AccessToken;
	wstring m_RefreshToken;
	// Roles
};

class SessionStorage
{
public:

};

