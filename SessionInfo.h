#pragma once

#include "Sapio365SessionSavedInfo.h"
#include "SessionStatus.h"

struct SessionInfo
{
public:
    SessionInfo();
    bool operator==(const SessionInfo& p_Other) const;

    static vector<SessionInfo> GetSessions(bool p_Sort);

	// If m_RoleId != 0, load the role name from m_SavedInfo or RoleDelegationManager if necessary
	wstring GetRoleName() const;

    Sapio365SessionSavedInfo m_SavedInfo;
    int64_t m_RoleId;
};

