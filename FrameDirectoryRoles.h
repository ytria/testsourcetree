#pragma once

#include "GridFrameBase.h"
#include "GridDirectoryRoles.h"
#include "DirectoryRolesInfo.h"

class FrameDirectoryRoles : public GridFrameBase
{
public:
    DECLARE_DYNAMIC(FrameDirectoryRoles)
	FrameDirectoryRoles(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

    virtual O365Grid& GetGrid() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;
    virtual void ApplyAllSpecific() override;
    virtual void ApplySelectedSpecific() override;

    void BuildView(const DirectoryRolesInfo& p_RolesInfo, const vector<O365UpdateOperation>& p_UpdateOperations, const RefreshSpecificData& p_RefreshSpecificData, bool p_FullPurge);
    bool HasApplyButton() override;

protected:
    virtual void createGrid() override;

	virtual O365ControlConfig GetApplyPartialControlConfig() override;
	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

    virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

    virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

	virtual void revertSelectedImpl() override;

private:
    void ApplySpecific(bool p_Selected);

    GridDirectoryRoles m_GridDirectoryRoles;
};

