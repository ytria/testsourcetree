#include "IncludedPathDeserializer.h"

#include "JsonSerializeUtil.h"

void Cosmos::IncludedPathDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("path"), m_Data.Path, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("dataType"), m_Data.DataType, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("kind"), m_Data.Kind, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("precision"), m_Data.Precision, p_Object);
}
