#include "CachedObject.h"

#include "MsGraphFieldNames.h"
#include "RoleDelegationManager.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<CachedObject>("CachedObject") (metadata(MetadataKeys::TypeDisplayName, _YTEXT("Cached Object")))
		.constructor()(policy::ctor::as_object)
		.property(O365_ID, &CachedObject::m_Id);
}

CachedObject::CachedObject()
	: m_RBAC_DelegationID(-1)
{
}

CachedObject::CachedObject(const wstring& p_ID)
	: m_RBAC_DelegationID(-1)
	, m_Id(p_ID)
{
}

const PooledString& CachedObject::GetID() const
{
	return m_Id;
}

int64_t CachedObject::GetRBACDelegationID() const
{
	return m_RBAC_DelegationID;
}

void CachedObject::SetRBACDelegationID(const int64_t p_RoleDelegationID)
{
	m_RBAC_DelegationID = p_RoleDelegationID;
}

bool CachedObject::HasPrivilege(const RoleDelegationUtil::RBAC_Privilege p_Privilege, std::shared_ptr<Sapio365Session> p_Session) const
{
	return RoleDelegationManager::GetInstance().HasPrivilege(*this, p_Privilege, p_Session);
}

bool CachedObject::IsRBACAuthorized() const
{
	return m_RBAC_DelegationID != 0;
}
