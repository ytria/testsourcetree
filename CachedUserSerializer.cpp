#include "CachedUserSerializer.h"

#include "JsonSerializeUtil.h"
#include "YtriaFieldsConstants.h"

CachedUserSerializer::CachedUserSerializer(const CachedUser& p_CachedUser)
    : m_CachedUser(p_CachedUser)
{
}

void CachedUserSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YUID(YTRIA_CACHEDUSER_DISPLAYNAME), m_CachedUser.GetDisplayName(), obj);
    JsonSerializeUtil::SerializeString(_YUID(YTRIA_CACHEDUSER_USERPRINCIPALNAME), m_CachedUser.GetUserPrincipalName(), obj);
    JsonSerializeUtil::SerializeString(_YUID(YTRIA_CACHEDUSER_USERTYPE), m_CachedUser.GetUserType(), obj);
    JsonSerializeUtil::SerializeTimeDate(_YUID(YTRIA_CACHEDUSER_DELETEDDATETIME), m_CachedUser.GetDeletedDateTime(), obj);
}
