#pragma once

#include "IRequester.h"
#include "BusinessSite.h"
#include "IPageRequestLogger.h"
#include "ValueListDeserializer.h"

class IPropertySetBuilder;
class SiteDeserializer;

class SitesRequester : public IRequester
{
public:
	SitesRequester(IPropertySetBuilder& p_Properties, const std::shared_ptr<IPageRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const vector<BusinessSite>& GetData() const;

private:
	std::shared_ptr<ValueListDeserializer<BusinessSite, SiteDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;

	vector<rttr::property> m_Properties;
};

