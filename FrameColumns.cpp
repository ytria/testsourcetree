#include "FrameColumns.h"

#include "AutomationNames.h"

IMPLEMENT_DYNAMIC(FrameColumns, GridFrameBase)

//BEGIN_MESSAGE_MAP(FrameColumns, GridFrameBase)
//END_MESSAGE_MAP()

void FrameColumns::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.SetOrigin(GetModuleCriteria().m_Origin);
    ASSERT(ModuleCriteria::UsedContainer::SUBIDS == GetModuleCriteria().m_UsedContainer);
    if (ModuleCriteria::UsedContainer::SUBIDS == GetModuleCriteria().m_UsedContainer)
        info.GetSubIds() = GetModuleCriteria().m_SubIDs;

    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Columns, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameColumns::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Columns, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

FrameColumns::FrameColumns(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
    m_CreateRefreshPartial = true;
}

O365Grid& FrameColumns::GetGrid()
{
    return m_Grid;
}

void FrameColumns::createGrid()
{
    m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FrameColumns::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
    GridFrameBase::customizeActionsRibbonTab(tab, false, false);
    return true;
}

void FrameColumns::ShowColumns(const O365DataMap<BusinessSite, vector<BusinessList>>& p_Lists, bool p_FullPurge)
{
    m_Grid.BuildView(p_Lists, p_FullPurge);
}

GridFrameBase::O365ControlConfig FrameColumns::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigLists();
}

AutomatedApp::AUTOMATIONSTATUS FrameColumns::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
    AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

    return statusRV;
}