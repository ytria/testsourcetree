#pragma once
#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ServicePlanInfo.h"

class ServicePlanInfoDeserializer : public JsonObjectDeserializer, public Encapsulate<ServicePlanInfo>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

