#include "FrameDirectoryRoles.h"

#include "BusinessDirectoryRoleTemplate.h"
#include "GridUpdater.h"
#include "CommandDispatcher.h"

IMPLEMENT_DYNAMIC(FrameDirectoryRoles, GridFrameBase)

FrameDirectoryRoles::FrameDirectoryRoles(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
    m_CreateRefreshPartial = true;
}

O365Grid& FrameDirectoryRoles::GetGrid()
{
    return m_GridDirectoryRoles;
}

void FrameDirectoryRoles::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.SetOrigin(Origin::Tenant);
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	if (HasLastUpdateOperations() && !ShouldForceFullRefreshAfterApply())
    {
        for (const auto& updateOp : GetLastUpdateOperations())
        {
            GridBackendRow* row = m_GridDirectoryRoles.GetRowIndex().GetExistingRow(updateOp.GetPrimaryKey());
            ASSERT(nullptr != row);
            if (nullptr != row)
            {
				GridBackendColumn* colId = m_GridDirectoryRoles.GetColId();
				ASSERT(nullptr != colId);
				if (nullptr != colId)
				{
					GridBackendRow* templateRow = nullptr;
					if (GridUtil::IsBusinessUser(row))
						templateRow = row->GetParentRow();
					else
						templateRow = row;

					ASSERT(GridUtil::isBusinessType<BusinessDirectoryRoleTemplate>(templateRow));
					info.GetIds().insert(templateRow->GetField(colId).GetValueStr());
				}
            }
        }
    }

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::DirectoryRoles, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameDirectoryRoles::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::DirectoryRoles, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameDirectoryRoles::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameDirectoryRoles::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameDirectoryRoles::BuildView(const DirectoryRolesInfo& p_RolesInfo, const vector<O365UpdateOperation>& p_UpdateOperations, const RefreshSpecificData& p_RefreshSpecificData, bool p_FullPurge)
{
    m_GridDirectoryRoles.BuildView(p_RolesInfo, p_UpdateOperations, p_RefreshSpecificData, p_FullPurge);
}

bool FrameDirectoryRoles::HasApplyButton()
{
    return true;
}

void FrameDirectoryRoles::createGrid()
{
    m_GridDirectoryRoles.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameDirectoryRoles::GetApplyPartialControlConfig()
{
	return GetApplyPartialControlConfigRoles();
}

GridFrameBase::O365ControlConfig FrameDirectoryRoles::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigRoles();
}

bool FrameDirectoryRoles::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
    //return GridFrameBase::customizeActionsRibbonTab(tab, true, false);
    CreateViewDataEditGroups(tab);
    {
        auto editGroup = findGroup(tab, g_ActionsEditGroup.c_str());
        ASSERT(nullptr != editGroup);
        if (nullptr != editGroup)
        {
#if HAS_ACTIVATE_ROLE_COMMAND
			{
				auto ctrl = editGroup->Add(xtpControlButton, ID_DIRECTORYROLESGRID_ACTIVATEROLE);
				GridFrameBase::setImage({ ID_DIRECTORYROLESGRID_ACTIVATEROLE }, IDB_DIRECTORYROLESGRID_ACTIVATEROLE, xtpImageNormal);
				GridFrameBase::setImage({ ID_DIRECTORYROLESGRID_ACTIVATEROLE }, IDB_DIRECTORYROLESGRID_ACTIVATEROLE_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}
#endif

#if HAS_DEACTIVATE_ROLE_COMMAND
			{
				auto ctrl = editGroup->Add(xtpControlButton, ID_DIRECTORYROLESGRID_DEACTIVATEROLE);
				GridFrameBase::setImage({ ID_DIRECTORYROLESGRID_DEACTIVATEROLE }, IDB_DIRECTORYROLESGRID_DEACTIVATEROLE, xtpImageNormal);
				GridFrameBase::setImage({ ID_DIRECTORYROLESGRID_DEACTIVATEROLE }, IDB_DIRECTORYROLESGRID_DEACTIVATEROLE_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}
#endif
			{
				auto ctrl = editGroup->Add(xtpControlButton, ID_DIRECTORYROLESGRID_ADDMEMBER);
				GridFrameBase::setImage({ ID_DIRECTORYROLESGRID_ADDMEMBER }, IDB_DIRECTORYROLESGRID_ADDMEMBER, xtpImageNormal);
				GridFrameBase::setImage({ ID_DIRECTORYROLESGRID_ADDMEMBER }, IDB_DIRECTORYROLESGRID_ADDMEMBER_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			{
				auto ctrl = editGroup->Add(xtpControlButton, ID_DIRECTORYROLESGRID_REMOVEMEMBER);
				GridFrameBase::setImage({ ID_DIRECTORYROLESGRID_REMOVEMEMBER }, IDB_DIRECTORYROLESGRID_REMOVEMEMBER, xtpImageNormal);
				GridFrameBase::setImage({ ID_DIRECTORYROLESGRID_REMOVEMEMBER }, IDB_DIRECTORYROLESGRID_REMOVEMEMBER_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}
        }
    }

	CreateLinkGroups(tab, true, false);

    return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameDirectoryRoles::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
    AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

    return statusRV;
}

void FrameDirectoryRoles::revertSelectedImpl()
{
	GetGrid().RevertSelectedRows(O365Grid::RevertFlags::UPDATE_GRID_IF_NEEDED | O365Grid::RevertFlags::TOP_LEVEL_REVERT_CHILDREN);
}

void FrameDirectoryRoles::ApplySpecific(bool p_Selected)
{
    auto grid = dynamic_cast<GridDirectoryRoles*>(&GetGrid());
    ASSERT(nullptr != grid);

    if (nullptr != grid)
    {
        CommandInfo info;
        if (p_Selected)
			info.Data() = grid->GetChanges(grid->GetSelectedRows());
		else
			info.Data() = grid->GetChanges(grid->GetBackendRows());

        info.SetFrame(this);
		info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
        CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::DirectoryRoles, Command::ModuleTask::UpdateModified, info, { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
    }
}
