#pragma once

class AppliedConditionalAccessPolicy
{
public:
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<vector<PooledString>> m_EnforcedGrantControls;
	boost::YOpt<vector<PooledString>> m_EnforcedSessionControls;
	boost::YOpt<PooledString> m_Id;
	boost::YOpt<PooledString> m_Result;
};

