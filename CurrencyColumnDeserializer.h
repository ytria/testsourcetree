#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "CurrencyColumn.h"

class CurrencyColumnDeserializer : public JsonObjectDeserializer, public Encapsulate<CurrencyColumn>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

