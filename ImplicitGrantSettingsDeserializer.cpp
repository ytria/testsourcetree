#include "ImplicitGrantSettingsDeserializer.h"

void ImplicitGrantSettingsDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeBool(_YTEXT("enableIdTokenIssuance"), m_Data.m_EnableIdTokenIssuance, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("enableAccessTokenIssuance"), m_Data.m_EnableAccessTokenIssuance, p_Object);
}
