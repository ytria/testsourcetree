#pragma once

class Migration
{
public:
	using StepType = std::function<bool()>;
	void AddStep(const StepType& p_Operation);
	bool Apply();

private:
	vector<StepType> m_Steps;
};

