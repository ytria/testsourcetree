#pragma once

#include "DlgFormsHTML.h"

class BusinessDriveItem;

class DlgBusinessDriveItemRename : public DlgFormsHTML
{
public:
    DlgBusinessDriveItemRename(const BusinessDriveItem& p_DriveItem, DlgFormsHTML::Action p_Action, CWnd* p_Parent);

    void generateJSONScriptData() override;
    virtual bool processPostedData(const IPostedDataTarget::PostedData& data);

    const wstring& GetNewName() const;

private:
    const BusinessDriveItem& m_DriveItem;
    wstring m_NewName;
};
