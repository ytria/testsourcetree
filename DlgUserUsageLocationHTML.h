#pragma once

#include "DlgBusinessUserEditHTML.h"

class DlgUserUsageLocationHTML : public BaseDlgBusinessUserEditHTML
{
public:
	DlgUserUsageLocationHTML(vector<BusinessUser>& p_Users, CWnd* p_Parent, const wstring p_AutomationActionName);
	virtual ~DlgUserUsageLocationHTML();

	virtual bool processPostedDataSpecific(const IPostedDataTarget::PostedData::value_type& p_Value);

	virtual void postProcessPostedData();

protected:
	virtual wstring getDialogTitle() const override;

private:
	virtual void generateJSONScriptData() override;
};
