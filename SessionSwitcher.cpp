#include "SessionSwitcher.h"

#include "ISessionLoader.h"
#include "MainFrame.h"
#include "TraceIntoFile.h"
#include "Sapio365Session.h"
#include "SessionIdsEmitter.h"
#include "TaskDataManager.h"
#include "OAuth2AuthenticatorBase.h"
#include "QuerySessionExists.h"
#include "O365AdminErrorHandler.h"

using namespace std::literals::string_literals;

SessionSwitcher::SessionSwitcher(std::unique_ptr<ISessionLoader> p_Loader)
	: m_Loader(std::move(p_Loader)),
	m_ForceReloadExisting(false)
{
	if (nullptr == m_Loader)
		throw std::invalid_argument("p_Loader cannot be null");
}

TaskWrapper<std::shared_ptr<Sapio365Session>> SessionSwitcher::Switch()
{
	const SessionIdentifier& sessionId = m_Loader->GetSessionId();

	ASSERT(!sessionId.m_EmailOrAppId.empty() && !sessionId.m_SessionType.empty());
	TraceIntoFile::trace(_YDUMP("[["s) + SessionIdsEmitter(sessionId).GetId() + _YDUMP("]]") + _YDUMP("SessionSwitcher"), _YDUMP("Switch"), _YDUMPFORMAT(L"Entering for %s (type %s, role %s)", sessionId.m_EmailOrAppId.c_str(), sessionId.m_SessionType.c_str(), Str::getStringFromNumber(sessionId.m_RoleID).c_str()));

	std::shared_ptr<Sapio365Session> sapioSessionToLoad;
	pplx::task<std::shared_ptr<Sapio365Session>> resultValue = pplx::task_from_result(sapioSessionToLoad);

	// FIXME: Should we still cancel with multi-session??
	TaskDataManager::Get().CancelAll();

	try
	{
		auto existingSession = Sapio365Session::Find(sessionId);
		if (!m_ForceReloadExisting && nullptr != existingSession && existingSession->IsConnected())
		{
			SwitchToAlreadyLoaded(existingSession);
			resultValue = pplx::task_from_result(existingSession);
			sapioSessionToLoad = existingSession;
		}
		else
		{
			if (nullptr == existingSession)
			{
				sapioSessionToLoad = std::make_shared<Sapio365Session>();
				sapioSessionToLoad->GetMainMSGraphSession()->SetErrorHandler(std::make_unique<O365AdminErrorHandler>(sapioSessionToLoad->GetMainMSGraphSession()));
				sapioSessionToLoad->SetGraphCache(std::make_unique<GraphCache>(sapioSessionToLoad));
			}
			else
			{
				sapioSessionToLoad = existingSession;
			}

			auto& app = Office365AdminApp::Get();
			app.SetSapioSessionBeingLoaded(sapioSessionToLoad);
			app.GetSapioSessionBeingLoaded()->SetIdentifier(sessionId);
			app.SetSessionBeingLoadedIdentifier(sessionId);
			app.SetIsCreatingSession(false);

			resultValue = m_Loader->Load(sapioSessionToLoad);
		}

	}
	catch (const std::exception& e)
	{
		LoggerService::Debug(wstring(_YTEXT("SessionSwitcher: Switch: ")) + Str::convertFromUTF8(e.what()));
	}

	return resultValue;
}

void SessionSwitcher::SetForceReloadExisting(bool p_ReloadIfExists)
{
	m_ForceReloadExisting = p_ReloadIfExists;
}

void SessionSwitcher::SwitchToAlreadyLoaded(const std::shared_ptr<Sapio365Session>& p_Session)
{
	auto mainFrame = dynamic_cast<MainFrame*>(::AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		mainFrame->SetSapio365Session(p_Session);

	auto app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != app)
	{
		// Bug #180719.RO.0087DA
		// As of 07/25/2018, the following code is also executed in MainFrame::postSyncCache() when the session does not already exist.
		if (AutomatedApp::IsAutomationRunning())
		{
			if (IsActionLoadSession(app->GetCurrentAction()))
			{
				ASSERT(!app->GetCurrentAction()->IsExecuted());
				app->SetAutomationGreenLight(app->GetCurrentAction());
			}
		}
	}
}
