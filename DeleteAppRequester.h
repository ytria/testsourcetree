#pragma once

#include "IRequester.h"
#include "SingleRequestResult.h"

class DeleteAppRequester : public IRequester
{
public:
	DeleteAppRequester(const wstring& p_Id);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const SingleRequestResult& GetResult() const;

private:
	std::shared_ptr<SingleRequestResult> m_Result;
	wstring m_Id;
};

