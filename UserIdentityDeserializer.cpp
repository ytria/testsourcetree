#include "UserIdentityDeserializer.h"

void UserIdentityDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_DisplayName, p_Object, false);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("ipAddress"), m_Data.m_IpAddress, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("userPrincipalName"), m_Data.m_UserPrincipalName, p_Object);
}
