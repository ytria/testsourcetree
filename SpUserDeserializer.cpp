#include "SpUserDeserializer.h"

#include "JsonSerializeUtil.h"
#include "UserIdInfoDeserializer.h"

void Sp::UserDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("Email"), m_Data.Email, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YTEXT("Id"), m_Data.Id, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("IsHiddenInUI"), m_Data.IsHiddenInUI, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("IsSiteAdmin"), m_Data.IsSiteAdmin, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("LoginName"), m_Data.LoginName, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YTEXT("PrincipalType"), m_Data.PrincipalType, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("Title"), m_Data.Title, p_Object);

    {
        Sp::UserIdInfoDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("UserId"), p_Object))
            m_Data.UserId = std::move(deserializer.GetData());
    }
}
