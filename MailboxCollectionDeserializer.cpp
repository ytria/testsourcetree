#include "MailboxCollectionDeserializer.h"
#include "Mailbox.h"
#include "PSSerializeUtil.h"
#include "Str.h"

void MailboxCollectionDeserializer::Deserialize(IPSObjectPropertyReader& p_Reader)
{
	Mailbox object;
	object.m_RecipientTypeDetails = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("RecipientTypeDetails"));
	object.m_ForwardingAddress = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("ForwardingAddress"));
	object.m_ForwardingSmtpAddress = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("ForwardingSmtpAddress"));
	object.m_DeliverToMailboxAndForward = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("DeliverToMailboxAndForward"));
	if (p_Reader.HasProperty(L"GrantSendOnBehalfTo"))
		object.m_GrantSendOnBehalfTo = Str::explodeIntoVector(wstring(p_Reader.GetStringProperty(L"GrantSendOnBehalfTo")), wstring(_YTEXT(" ")));
	m_Data.push_back(object);
}

void MailboxCollectionDeserializer::SetCount(size_t p_Count)
{
	m_Data.reserve(p_Count);
}
