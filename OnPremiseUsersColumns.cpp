#include "OnPremiseUsersColumns.h"

#include "BaseO365Grid.h"
#include "GridTemplate.h"

bool OnPremiseUsersColumns::Create(O365Grid& p_Grid)
{
	if (!m_Created)
	{
		m_ColCommonDisplayName = p_Grid.AddColumn(_YUID("commonDisplayName"), GetTitle(_YUID("commonDisplayName")), O365Grid::g_FamilyCommonInfo, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCommonUserName = p_Grid.AddColumn(_YUID("commonUserPrincipalName"), GetTitle(_YUID("commonUserPrincipalName")), O365Grid::g_FamilyCommonInfo, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCommonMetaType = p_Grid.AddColumnIcon(_YUID("metaType"), GetTitle(_YUID("metaType")), O365Grid::g_FamilyCommonInfo);
		m_ColCommonMetaType->SetPresetFlags({ O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });

		m_ColSamAccountName = p_Grid.AddColumn(_YUID("onPremsamAccountName"), GetTitle(_YUID("onPremsamAccountName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColAccountExpires = p_Grid.AddColumnDate(_YUID("onPremAccountExpires"), GetTitle(_YUID("onPremAccountExpires")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColAccountLockoutTime = p_Grid.AddColumnDate(_YUID("onPremAccountLockoutTime"), GetTitle(_YUID("onPremAccountLockoutTime")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColAccountNotDelegated = p_Grid.AddColumnCheckBox(_YUID("onPremAccountNotDelegated"), GetTitle(_YUID("onPremAccountNotDelegated")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColAdminCount = p_Grid.AddColumn(_YUID("onPremAdminCount"), GetTitle(_YUID("onPremAdminCount")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColAllowReversiblePasswordEncryption = p_Grid.AddColumnCheckBox(_YUID("onPremAllowReversiblePasswordEncryption"), GetTitle(_YUID("onPremAllowReversiblePasswordEncryption")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColAuthenticationPolicy = p_Grid.AddColumn(_YUID("onPremAuthenticationPolicy"), GetTitle(_YUID("onPremAuthenticationPolicy")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColAuthenticationPolicySilo = p_Grid.AddColumn(_YUID("onPremAuthenticationPolicySilo"), GetTitle(_YUID("onPremAuthenticationPolicySilo")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColBadLogonCount = p_Grid.AddColumn(_YUID("onPremBadLogonCount"), GetTitle(_YUID("onPremBadLogonCount")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColBadPasswordTime = p_Grid.AddColumnDate(_YUID("onPremBadPasswordTime"), GetTitle(_YUID("onPremBadPasswordTime")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColBadPwdCount = p_Grid.AddColumn(_YUID("onPremBadPwdCount"), GetTitle(_YUID("onPremBadPwdCount")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCannotChangePassword = p_Grid.AddColumnCheckBox(_YUID("onPremCannotChangePassword"), GetTitle(_YUID("onPremCannotChangePassword")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCanonicalName = p_Grid.AddColumn(_YUID("onPremCanonicalName"), GetTitle(_YUID("onPremCanonicalName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColCertificates = p_Grid.AddColumn(_YUID("onPremCertificates"), GetTitle(_YUID("onPremCertificates")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCity = p_Grid.AddColumn(_YUID("onPremCity"), GetTitle(_YUID("onPremCity")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCN = p_Grid.AddColumn(_YUID("onPremCN"), GetTitle(_YUID("onPremCN")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCodePage = p_Grid.AddColumn(_YUID("onPremCodePage"), GetTitle(_YUID("onPremCodePage")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCompany = p_Grid.AddColumn(_YUID("onPremCompany"), GetTitle(_YUID("onPremCompany")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCompoundIdentitySupported = p_Grid.AddColumn(_YUID("onPremCompoundIdentitySupported"), GetTitle(_YUID("onPremCompoundIdentitySupported")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCountry = p_Grid.AddColumn(_YUID("onPremCountry"), GetTitle(_YUID("onPremCountry")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCountryCode = p_Grid.AddColumn(_YUID("onPremCountryCode"), GetTitle(_YUID("onPremCountryCode")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCreated = p_Grid.AddColumnDate(_YUID("onPremCreated"), GetTitle(_YUID("onPremCreated")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDeleted = p_Grid.AddColumnCheckBox(_YUID("onPremDeleted"), GetTitle(_YUID("onPremDeleted")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDepartment = p_Grid.AddColumn(_YUID("onPremDepartment"), GetTitle(_YUID("onPremDepartment")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDescription = p_Grid.AddColumn(_YUID("onPremDescription"), GetTitle(_YUID("onPremDescription")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDisplayName = p_Grid.AddColumn(_YUID("onPremDisplayName"), GetTitle(_YUID("onPremDisplayName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDistinguishedName = p_Grid.AddColumn(_YUID("onPremDistinguishedName"), GetTitle(_YUID("onPremDistinguishedName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColOrganizationalUnit = p_Grid.AddColumn(_YUID("onPremOrganizationalUnit"), GetTitle(_YUID("onPremOrganizationalUnit")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDivision = p_Grid.AddColumn(_YUID("onPremDivision"), GetTitle(_YUID("onPremDivision")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDoesNotRequirePreAuth = p_Grid.AddColumnCheckBox(_YUID("onPremDoesNotRequirePreAuth"), GetTitle(_YUID("onPremDoesNotRequirePreAuth")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDSCorePropagationData = p_Grid.AddColumnDate(_YUID("onPremDsCorePropagationData"), GetTitle(_YUID("onPremDsCorePropagationData")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColEmailAddress = p_Grid.AddColumn(_YUID("onPremEmailAddress"), GetTitle(_YUID("onPremEmailAddress")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColEmployeeID = p_Grid.AddColumn(_YUID("onPremEmployeeID"), GetTitle(_YUID("onPremEmployeeID")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColEmployeeNumber = p_Grid.AddColumn(_YUID("onPremEmployeeNumber"), GetTitle(_YUID("onPremEmployeeNumber")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColEnabled = p_Grid.AddColumnCheckBox(_YUID("onPremEnabled"), GetTitle(_YUID("onPremEnabled")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColFax = p_Grid.AddColumn(_YUID("onPremFax"), GetTitle(_YUID("onPremFax")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColGivenName = p_Grid.AddColumn(_YUID("onPremGivenName"), GetTitle(_YUID("onPremGivenName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColHomeDirectory = p_Grid.AddColumn(_YUID("onPremHomeDirectory"), GetTitle(_YUID("onPremHomeDirectory")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColHomedirRequired = p_Grid.AddColumnCheckBox(_YUID("onPremHomeDirRequired"), GetTitle(_YUID("onPremHomeDirRequired")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColHomeDrive = p_Grid.AddColumn(_YUID("onPremHomeDrive"), GetTitle(_YUID("onPremHomeDrive")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColHomePage = p_Grid.AddColumn(_YUID("onPremHomePage"), GetTitle(_YUID("onPremHomePage")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColHomePhone = p_Grid.AddColumn(_YUID("onPremHomePhone"), GetTitle(_YUID("onPremHomePhone")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColInitials = p_Grid.AddColumn(_YUID("onPremInitials"), GetTitle(_YUID("onPremInitials")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColInstanceType = p_Grid.AddColumn(_YUID("onPremInstanceType"), GetTitle(_YUID("onPremInstanceType")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColIsCriticalSystemObject = p_Grid.AddColumnCheckBox(_YUID("onPremIsCriticalSystemObject"), GetTitle(_YUID("onPremIsCriticalSystemObject")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColIsDeleted = p_Grid.AddColumnCheckBox(_YUID("onPremIsDeleted"), GetTitle(_YUID("onPremIsDeleted")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColKerberosEncryptionType = p_Grid.AddColumn(_YUID("onPremKerberosEncryptionType"), GetTitle(_YUID("onPremKerberosEncryptionType")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColLastBadPasswordAttempt = p_Grid.AddColumnDate(_YUID("onPremLastBadPasswordAttempt"), GetTitle(_YUID("onPremLastBadPasswordAttempt")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColLastKnownParent = p_Grid.AddColumn(_YUID("onPremLastKnownParent"), GetTitle(_YUID("onPremLastKnownParent")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColLastLogoff = p_Grid.AddColumnDate(_YUID("onPremLastLogoff"), GetTitle(_YUID("onPremLastLogoff")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColLastLogon = p_Grid.AddColumnDate(_YUID("onPremLastLogon"), GetTitle(_YUID("onPremLastLogon")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColLastLogonDate = p_Grid.AddColumnDate(_YUID("onPremLastLogonDate"), GetTitle(_YUID("onPremLastLogonDate")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColLockedOut = p_Grid.AddColumnCheckBox(_YUID("onPremLockedOut"), GetTitle(_YUID("onPremLockedOut")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColLogonCount = p_Grid.AddColumn(_YUID("onPremLogonCount"), GetTitle(_YUID("onPremLogonCount")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColLogonHours = p_Grid.AddColumn(_YUID("onPremLogonHours"), GetTitle(_YUID("onPremLogonHours")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColLogonWorkstations = p_Grid.AddColumn(_YUID("onPremLogonWorkstations"), GetTitle(_YUID("onPremLogonWorkstations")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColManagerFriendly = p_Grid.AddColumn(_YUID("onPremManagerFriendly"), GetTitle(_YUID("onPremManagerFriendly")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColManager = p_Grid.AddColumn(_YUID("onPremManager"), GetTitle(_YUID("onPremManager")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColMemberOfFriendly = p_Grid.AddColumn(_YUID("onPremMemberOfFriendly"), GetTitle(_YUID("onPremMemberOfFriendly")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColMemberOf = p_Grid.AddColumn(_YUID("onPremMemberOf"), GetTitle(_YUID("onPremMemberOf")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColMemberOf->SetMultivalueFamily(_T("Member Of"));
		m_ColMemberOf->AddMultiValueExplosionSister(m_ColMemberOfFriendly);
		m_ColMNSLogonAccount = p_Grid.AddColumnCheckBox(_YUID("onPremMnsLogonAccount"), GetTitle(_YUID("onPremMnsLogonAccount")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColMobilePhone = p_Grid.AddColumn(_YUID("onPremMobilePhone"), GetTitle(_YUID("onPremMobilePhone")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColModified = p_Grid.AddColumnDate(_YUID("onPremModified"), GetTitle(_YUID("onPremModified")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColMSDSConsistencyGuid = p_Grid.AddColumn(_YUID("onPremMSDSConsistencyGuid"), GetTitle(_YUID("onPremMSDSConsistencyGuid")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColMSDSUserAccountControlComputed = p_Grid.AddColumn(_YUID("onPremMsdsUserAccountControlComputed"), GetTitle(_YUID("onPremMsdsUserAccountControlComputed")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColName = p_Grid.AddColumn(_YUID("onPremName"), GetTitle(_YUID("onPremName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColNTSecurityDescriptor = p_Grid.AddColumn(_YUID("onPremNTSecurityDescriptor"), GetTitle(_YUID("onPremNTSecurityDescriptor")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColObjectCategory = p_Grid.AddColumn(_YUID("onPremObjectCategory"), GetTitle(_YUID("onPremObjectCategory")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColObjectClass = p_Grid.AddColumn(_YUID("onPremObjectClass"), GetTitle(_YUID("onPremObjectClass")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColObjectGUID = p_Grid.AddColumn(_YUID("onPremObjectGUID"), GetTitle(_YUID("onPremObjectGUID")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColObjectSid = p_Grid.AddColumn(_YUID("onPremObjectSid"), GetTitle(_YUID("onPremObjectSid")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColOffice = p_Grid.AddColumn(_YUID("onPremOffice"), GetTitle(_YUID("onPremOffice")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColOfficePhone = p_Grid.AddColumn(_YUID("onPremOfficePhone"), GetTitle(_YUID("onPremOfficePhone")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColOrganization = p_Grid.AddColumn(_YUID("onPremOrganization"), GetTitle(_YUID("onPremOrganization")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColOtherName = p_Grid.AddColumn(_YUID("onPremOtherName"), GetTitle(_YUID("onPremOtherName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColPasswordExpired = p_Grid.AddColumnCheckBox(_YUID("onPremPasswordExpired"), GetTitle(_YUID("onPremPasswordExpired")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColPasswordLastSet = p_Grid.AddColumnDate(_YUID("onPremPasswordLastSet"), GetTitle(_YUID("onPremPasswordLastSet")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColPasswordNeverExpires = p_Grid.AddColumnCheckBox(_YUID("onPremPasswordNeverExpires"), GetTitle(_YUID("onPremPasswordNeverExpires")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColPasswordNotRequired = p_Grid.AddColumnCheckBox(_YUID("onPremPasswordNotRequired"), GetTitle(_YUID("onPremPasswordNotRequired")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColPOBox = p_Grid.AddColumn(_YUID("onPremPOBox"), GetTitle(_YUID("onPremPOBox")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColPostalCode = p_Grid.AddColumn(_YUID("onPremPostalCode"), GetTitle(_YUID("onPremPostalCode")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColPrimaryGroup = p_Grid.AddColumn(_YUID("onPremPrimaryGroup"), GetTitle(_YUID("onPremPrimaryGroup")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColPrimaryGroupId = p_Grid.AddColumn(_YUID("onPremPrimaryGroupId"), GetTitle(_YUID("onPremPrimaryGroupId")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColPrincipalsAllowedToDelegateToAccount = p_Grid.AddColumn(_YUID("onPremPrincipalsAllowedToDelegateToAccount"), GetTitle(_YUID("onPremPrincipalsAllowedToDelegateToAccount")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColProfilePath = p_Grid.AddColumn(_YUID("onPremProfilePath"), GetTitle(_YUID("onPremProfilePath")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColProtectedFromAccidentalDeletion = p_Grid.AddColumnCheckBox(_YUID("onPremProtectedFromAccidentalDeletion"), GetTitle(_YUID("onPremProtectedFromAccidentalDeletion")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColProxyAddresses = p_Grid.AddColumn(_YUID("onPremProxyAddresses"), GetTitle(_YUID("onPremProxyAddresses")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColSAMAccountType = p_Grid.AddColumn(_YUID("onPremSAMAccountType"), GetTitle(_YUID("onPremSAMAccountType")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColServicePrincipalNames = p_Grid.AddColumn(_YUID("onPremServicePrincipalNames"), GetTitle(_YUID("onPremServicePrincipalNames")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColScriptPath = p_Grid.AddColumn(_YUID("onPremScriptPath"), GetTitle(_YUID("onPremScriptPath")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColSDRightsEffective = p_Grid.AddColumn(_YUID("onPremSDRightsEffective"), GetTitle(_YUID("onPremSDRightsEffective")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColSID = p_Grid.AddColumn(_YUID("onPremSid"), GetTitle(_YUID("onPremSid")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColSIDHistory = p_Grid.AddColumn(_YUID("onPremSidHistory"), GetTitle(_YUID("onPremSidHistory")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColSmartcardLogonRequired = p_Grid.AddColumnCheckBox(_YUID("onPremSmartcardLogonRequired"), GetTitle(_YUID("onPremSmartcardLogonRequired")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColState = p_Grid.AddColumn(_YUID("onPremState"), GetTitle(_YUID("onPremState")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColStreetAddress = p_Grid.AddColumn(_YUID("onPremStreetAddress"), GetTitle(_YUID("onPremStreetAddress")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColSurname = p_Grid.AddColumn(_YUID("onPremSurname"), GetTitle(_YUID("onPremSurname")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColTitle = p_Grid.AddColumn(_YUID("onPremTitle"), GetTitle(_YUID("onPremTitle")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColTrustedForDelegation = p_Grid.AddColumnCheckBox(_YUID("onPremTrustedForDelegation"), GetTitle(_YUID("onPremTrustedForDelegation")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColTrustedToAuthForDelegation = p_Grid.AddColumnCheckBox(_YUID("onPremTrustedToAuthForDelegation"), GetTitle(_YUID("onPremTrustedToAuthForDelegation")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColUseDESKeyOnly = p_Grid.AddColumnCheckBox(_YUID("onPremUseDESKeyOnly"), GetTitle(_YUID("onPremUseDESKeyOnly")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColUserAccountControl = p_Grid.AddColumn(_YUID("onPremUserAccountControl"), GetTitle(_YUID("onPremUserAccountControl")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColUserCertificate = p_Grid.AddColumn(_YUID("onPremUserCertificate"), GetTitle(_YUID("onPremUserCertificate")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColUserPrincipalName = p_Grid.AddColumn(_YUID("onPremUserPrincipalName"), GetTitle(_YUID("onPremUserPrincipalName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColUSNChanged = p_Grid.AddColumn(_YUID("onPremUSNChanged"), GetTitle(_YUID("onPremUSNChanged")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColUSNCreated = p_Grid.AddColumn(_YUID("onPremUSNCreated"), GetTitle(_YUID("onPremUSNCreated")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });
		m_ColWhenChanged = p_Grid.AddColumnDate(_YUID("onPremWhenChanged"), GetTitle(_YUID("onPremWhenChanged")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColWhenCreated = p_Grid.AddColumnDate(_YUID("onPremWhenCreated"), GetTitle(_YUID("onPremWhenCreated")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColImmutableId = p_Grid.AddColumn(_YUID("onPremImmutableId"), GetTitle(_YUID("onPremImmutableId")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize16, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetTech });

		m_Columns = {
			m_ColAccountExpires,
			m_ColAccountLockoutTime,
			m_ColAccountNotDelegated,
			m_ColAdminCount,
			m_ColAllowReversiblePasswordEncryption,
			m_ColAuthenticationPolicy,
			m_ColAuthenticationPolicySilo,
			m_ColBadLogonCount,
			m_ColBadPasswordTime,
			m_ColBadPwdCount,
			m_ColCannotChangePassword,
			m_ColCanonicalName,
			m_ColCertificates,
			m_ColCity,
			m_ColCompoundIdentitySupported,
			m_ColCN,
			m_ColCodePage,
			m_ColCompany,
			m_ColCountry,
			m_ColCountryCode,
			m_ColCreated,
			m_ColDeleted,
			m_ColDepartment,
			m_ColDescription,
			m_ColDisplayName,
			m_ColDistinguishedName,
			m_ColOrganizationalUnit,
			m_ColDivision,
			m_ColDoesNotRequirePreAuth,
			m_ColDSCorePropagationData,
			m_ColEmailAddress,
			m_ColEmployeeID,
			m_ColEmployeeNumber,
			m_ColEnabled,
			m_ColFax,
			m_ColGivenName,
			m_ColHomeDirectory,
			m_ColHomedirRequired,
			m_ColHomeDrive,
			m_ColHomePage,
			m_ColHomePhone,
			m_ColInitials,
			m_ColInstanceType,
			m_ColIsCriticalSystemObject,
			m_ColIsDeleted,
			m_ColKerberosEncryptionType,
			m_ColLastBadPasswordAttempt,
			m_ColLastKnownParent,
			m_ColLastLogoff,
			m_ColLastLogon,
			m_ColLastLogonDate,
			m_ColLockedOut,
			m_ColLogonCount,
			m_ColLogonHours,
			m_ColLogonWorkstations,
			m_ColManagerFriendly,
			m_ColManager,
			m_ColMemberOfFriendly,
			m_ColMemberOf,
			m_ColMNSLogonAccount,
			m_ColMobilePhone,
			m_ColModified,
			m_ColMSDSConsistencyGuid,
			m_ColMSDSUserAccountControlComputed,
			m_ColName,
			m_ColNTSecurityDescriptor,
			m_ColObjectCategory,
			m_ColObjectClass,
			m_ColObjectGUID,
			m_ColObjectSid,
			m_ColOffice,
			m_ColOfficePhone,
			m_ColOrganization,
			m_ColOtherName,
			m_ColPasswordExpired,
			m_ColPasswordLastSet,
			m_ColPasswordNeverExpires,
			m_ColPasswordNotRequired,
			m_ColPOBox,
			m_ColPostalCode,
			m_ColPrimaryGroup,
			m_ColPrimaryGroupId,
			m_ColPrincipalsAllowedToDelegateToAccount,
			m_ColProfilePath,
			m_ColProtectedFromAccidentalDeletion,
			m_ColProxyAddresses,
			m_ColSamAccountName,
			m_ColSAMAccountType,
			m_ColScriptPath,
			m_ColServicePrincipalNames,
			m_ColSDRightsEffective,
			m_ColSID,
			m_ColSIDHistory,
			m_ColSmartcardLogonRequired,
			m_ColState,
			m_ColStreetAddress,
			m_ColSurname,
			m_ColTitle,
			m_ColTrustedForDelegation,
			m_ColTrustedToAuthForDelegation,
			m_ColUseDESKeyOnly,
			m_ColUserAccountControl,
			m_ColUserCertificate,
			m_ColUserPrincipalName,
			m_ColUSNChanged,
			m_ColUSNCreated,
			m_ColWhenChanged,
			m_ColWhenCreated,
			m_ColImmutableId
		};

		m_CommonColumns =
		{ 
			m_ColCommonDisplayName,
			m_ColCommonUserName,
			m_ColCommonMetaType,
		};
		p_Grid.AddCategoryTop(_T("On-Premises fields"), m_Columns);
		p_Grid.AddCategoryTop(_T("Common Info"), m_CommonColumns);

		// Usually done in O365Grid::customizeGridPostProcess(), but we are called outside so do it manually
		{
			auto process = [&p_Grid](auto& c)
			{
				ASSERT(nullptr != c);
				if (nullptr != c)
				{
					if (c->IsCheckbox())
						c->SetReadOnly(true);
					if (c->IsCombo())
						ASSERT(false);
					if (c->HasPresetFlag(O365Grid::g_ColumnsPresetOnPremDefault) && !c->HasPresetFlag(O365Grid::g_ColumnsPresetTech))
						p_Grid.SetColumnVisible(c, false, GridBackendUtil::SETVISIBLE_NONE);
				}
			};

			for (auto c : m_Columns)
				process(c);

			for (auto c : m_CommonColumns)
				process(c);
		}

		// Init setters maps
		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseUsersColumns::*, std::function<void(OnPremiseUser&, const boost::YOpt<PooledString>&)>>> stringSetters
			{
				{ &OnPremiseUsersColumns::m_ColCanonicalName, &OnPremiseUser::SetCanonicalName },
				{ &OnPremiseUsersColumns::m_ColCity, &OnPremiseUser::SetCity },
				{ &OnPremiseUsersColumns::m_ColCN, &OnPremiseUser::SetCN },
				{ &OnPremiseUsersColumns::m_ColCompany, &OnPremiseUser::SetCompany },
				{ &OnPremiseUsersColumns::m_ColCountry, &OnPremiseUser::SetCountry },
				{ &OnPremiseUsersColumns::m_ColDepartment, &OnPremiseUser::SetDepartment },
				{ &OnPremiseUsersColumns::m_ColDescription, &OnPremiseUser::SetDescription },
				{ &OnPremiseUsersColumns::m_ColDisplayName, &OnPremiseUser::SetDisplayName },
				{ &OnPremiseUsersColumns::m_ColDistinguishedName, &OnPremiseUser::SetDistinguishedName },
				{ &OnPremiseUsersColumns::m_ColDivision, &OnPremiseUser::SetDivision },
				{ &OnPremiseUsersColumns::m_ColEmailAddress, &OnPremiseUser::SetEmailAddress },
				{ &OnPremiseUsersColumns::m_ColEmployeeID, &OnPremiseUser::SetEmployeeID },
				{ &OnPremiseUsersColumns::m_ColEmployeeNumber, &OnPremiseUser::SetEmployeeNumber },
				{ &OnPremiseUsersColumns::m_ColFax, &OnPremiseUser::SetFax },
				{ &OnPremiseUsersColumns::m_ColGivenName, &OnPremiseUser::SetGivenName },
				{ &OnPremiseUsersColumns::m_ColHomeDirectory, &OnPremiseUser::SetHomeDirectory },
				{ &OnPremiseUsersColumns::m_ColHomeDrive, &OnPremiseUser::SetHomeDrive },
				{ &OnPremiseUsersColumns::m_ColHomePage, &OnPremiseUser::SetHomePage },
				{ &OnPremiseUsersColumns::m_ColHomePhone, &OnPremiseUser::SetHomePhone },
				{ &OnPremiseUsersColumns::m_ColInitials, &OnPremiseUser::SetInitials },
				{ &OnPremiseUsersColumns::m_ColLastKnownParent, &OnPremiseUser::SetLastKnownParent },
				{ &OnPremiseUsersColumns::m_ColManager, &OnPremiseUser::SetManager },
				{ &OnPremiseUsersColumns::m_ColMobilePhone, &OnPremiseUser::SetMobilePhone },
				{ &OnPremiseUsersColumns::m_ColName, &OnPremiseUser::SetName },
				{ &OnPremiseUsersColumns::m_ColNTSecurityDescriptor, &OnPremiseUser::SetNTSecurityDescriptor },
				{ &OnPremiseUsersColumns::m_ColObjectGUID, &OnPremiseUser::SetObjectGUID },
				{ &OnPremiseUsersColumns::m_ColObjectCategory, &OnPremiseUser::SetObjectCategory },
				{ &OnPremiseUsersColumns::m_ColObjectClass, &OnPremiseUser::SetObjectClass },
				{ &OnPremiseUsersColumns::m_ColObjectSid, &OnPremiseUser::SetObjectSid },
				{ &OnPremiseUsersColumns::m_ColOffice, &OnPremiseUser::SetOffice },
				{ &OnPremiseUsersColumns::m_ColOfficePhone, &OnPremiseUser::SetOfficePhone },
				{ &OnPremiseUsersColumns::m_ColOtherName, &OnPremiseUser::SetOtherName },
				{ &OnPremiseUsersColumns::m_ColLogonWorkstations, &OnPremiseUser::SetLogonWorkstations },
				{ &OnPremiseUsersColumns::m_ColOrganization, &OnPremiseUser::SetOrganization },
				{ &OnPremiseUsersColumns::m_ColPOBox, &OnPremiseUser::SetPOBox },
				{ &OnPremiseUsersColumns::m_ColPostalCode, &OnPremiseUser::SetPostalCode },
				{ &OnPremiseUsersColumns::m_ColPrimaryGroup, &OnPremiseUser::SetPrimaryGroup },
				{ &OnPremiseUsersColumns::m_ColProfilePath, &OnPremiseUser::SetProfilePath },
				{ &OnPremiseUsersColumns::m_ColSamAccountName, &OnPremiseUser::SetSamAccountName },
				{ &OnPremiseUsersColumns::m_ColSID, &OnPremiseUser::SetSID },
				{ &OnPremiseUsersColumns::m_ColScriptPath, &OnPremiseUser::SetScriptPath },
				{ &OnPremiseUsersColumns::m_ColState, &OnPremiseUser::SetState },
				{ &OnPremiseUsersColumns::m_ColStreetAddress, &OnPremiseUser::SetStreetAddress },
				{ &OnPremiseUsersColumns::m_ColSurname, &OnPremiseUser::SetSurname },
				{ &OnPremiseUsersColumns::m_ColTitle, &OnPremiseUser::SetTitle },
				{ &OnPremiseUsersColumns::m_ColUserPrincipalName, &OnPremiseUser::SetUserPrincipalName },
			};

			for (const auto& s : stringSetters)
				m_StringSetters[this->*(s.first)] = s.second;
		}

		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseUsersColumns::*, std::function<void(OnPremiseUser&, const boost::YOpt<bool>&)>>> boolSetters
			{
				{ &OnPremiseUsersColumns::m_ColAccountNotDelegated, &OnPremiseUser::SetAccountNotDelegated },
				{ &OnPremiseUsersColumns::m_ColAllowReversiblePasswordEncryption, &OnPremiseUser::SetAllowReversiblePasswordEncryption },
				{ &OnPremiseUsersColumns::m_ColCannotChangePassword, &OnPremiseUser::SetCannotChangePassword },
				{ &OnPremiseUsersColumns::m_ColDeleted, &OnPremiseUser::SetDeleted },
				{ &OnPremiseUsersColumns::m_ColDoesNotRequirePreAuth, &OnPremiseUser::SetDoesNotRequirePreAuth },
				{ &OnPremiseUsersColumns::m_ColEnabled, &OnPremiseUser::SetEnabled },
				{ &OnPremiseUsersColumns::m_ColHomedirRequired, &OnPremiseUser::SetHomedirRequired },
				{ &OnPremiseUsersColumns::m_ColIsCriticalSystemObject, &OnPremiseUser::SetIsCriticalSystemObject },
				{ &OnPremiseUsersColumns::m_ColIsDeleted, &OnPremiseUser::SetIsDeleted },
				{ &OnPremiseUsersColumns::m_ColLockedOut, &OnPremiseUser::SetLockedOut },
				{ &OnPremiseUsersColumns::m_ColMNSLogonAccount, &OnPremiseUser::SetMNSLogonAccount },
				{ &OnPremiseUsersColumns::m_ColPasswordExpired, &OnPremiseUser::SetPasswordExpired },
				{ &OnPremiseUsersColumns::m_ColPasswordNeverExpires, &OnPremiseUser::SetPasswordNeverExpires },
				{ &OnPremiseUsersColumns::m_ColPasswordNotRequired, &OnPremiseUser::SetPasswordNotRequired },
				{ &OnPremiseUsersColumns::m_ColProtectedFromAccidentalDeletion, &OnPremiseUser::SetProtectedFromAccidentalDeletion },
				{ &OnPremiseUsersColumns::m_ColSmartcardLogonRequired, &OnPremiseUser::SetSmartcardLogonRequired },
				{ &OnPremiseUsersColumns::m_ColTrustedForDelegation, &OnPremiseUser::SetTrustedForDelegation },
				{ &OnPremiseUsersColumns::m_ColTrustedToAuthForDelegation, &OnPremiseUser::SetTrustedToAuthForDelegation },
				{ &OnPremiseUsersColumns::m_ColUseDESKeyOnly, &OnPremiseUser::SetUseDESKeyOnly },
			};

			for (const auto& b : boolSetters)
				m_BoolSetters[this->*(b.first)] = b.second;
		}

		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseUsersColumns::*, std::function<void(OnPremiseUser&, const boost::YOpt<YTimeDate>&)>>> dateSetters
			{
				{ &OnPremiseUsersColumns::m_ColAccountExpires, &OnPremiseUser::SetAccountExpires },
				{ &OnPremiseUsersColumns::m_ColBadPasswordTime, &OnPremiseUser::SetBadPasswordTime },
				{ &OnPremiseUsersColumns::m_ColLastLogoff, &OnPremiseUser::SetLastLogoff },
				{ &OnPremiseUsersColumns::m_ColLastLogon, &OnPremiseUser::SetLastLogon },
				{ &OnPremiseUsersColumns::m_ColAccountLockoutTime, &OnPremiseUser::SetAccountLockoutTime },
				{ &OnPremiseUsersColumns::m_ColCreated, &OnPremiseUser::SetCreated },
				{ &OnPremiseUsersColumns::m_ColLastBadPasswordAttempt, &OnPremiseUser::SetLastBadPasswordAttempt },
				{ &OnPremiseUsersColumns::m_ColLastLogonDate, &OnPremiseUser::SetLastLogonDate },
				{ &OnPremiseUsersColumns::m_ColModified, &OnPremiseUser::SetModified },
				{ &OnPremiseUsersColumns::m_ColPasswordLastSet, &OnPremiseUser::SetPasswordLastSet },
				{ &OnPremiseUsersColumns::m_ColWhenChanged, &OnPremiseUser::SetWhenChanged },
				{ &OnPremiseUsersColumns::m_ColWhenCreated, &OnPremiseUser::SetWhenCreated },
			};

			for (const auto& d : dateSetters)
				m_DateSetters[this->*(d.first)] = d.second;
		}

		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseUsersColumns::*, std::function<void(OnPremiseUser&, const boost::YOpt<int32_t>&)>>> intSetters
			{
				{ &OnPremiseUsersColumns::m_ColAdminCount, &OnPremiseUser::SetAdminCount },
				{ &OnPremiseUsersColumns::m_ColBadLogonCount, &OnPremiseUser::SetBadLogonCount },
				{ &OnPremiseUsersColumns::m_ColBadPwdCount, &OnPremiseUser::SetBadPwdCount },
				{ &OnPremiseUsersColumns::m_ColCodePage, &OnPremiseUser::SetCodePage },
				{ &OnPremiseUsersColumns::m_ColCountryCode, &OnPremiseUser::SetCountryCode },
				{ &OnPremiseUsersColumns::m_ColInstanceType, &OnPremiseUser::SetInstanceType },
				{ &OnPremiseUsersColumns::m_ColLogonCount, &OnPremiseUser::SetLogonCount },
				{ &OnPremiseUsersColumns::m_ColMSDSUserAccountControlComputed, &OnPremiseUser::SetMSDSUserAccountControlComputed },
				{ &OnPremiseUsersColumns::m_ColPrimaryGroupId, &OnPremiseUser::SetPrimaryGroupId },
				{ &OnPremiseUsersColumns::m_ColSAMAccountType, &OnPremiseUser::SetSAMAccountType },
				{ &OnPremiseUsersColumns::m_ColSDRightsEffective, &OnPremiseUser::SetSDRightsEffective },
				{ &OnPremiseUsersColumns::m_ColUserAccountControl, &OnPremiseUser::SetUserAccountControl },
			};

			for (const auto& i : intSetters)
				m_IntSetters[this->*(i.first)] = i.second;
		}

		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseUsersColumns::*, std::function<void(OnPremiseUser&, const boost::YOpt<int64_t>&)>>> int64Setters
			{
				{ &OnPremiseUsersColumns::m_ColUSNChanged, &OnPremiseUser::SetUSNChanged },
				{ &OnPremiseUsersColumns::m_ColUSNCreated, &OnPremiseUser::SetUSNCreated },
			};

			for (const auto& i : int64Setters)
				m_Int64Setters[this->*(i.first)] = i.second;
		}

		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseUsersColumns::*, std::function<void(OnPremiseUser&, const boost::YOpt<vector<PooledString>>&)>>> stringListSetters
			{
				{ &OnPremiseUsersColumns::m_ColAuthenticationPolicy, &OnPremiseUser::SetAuthenticationPolicy },
				{ &OnPremiseUsersColumns::m_ColAuthenticationPolicySilo, &OnPremiseUser::SetAuthenticationPolicySilo },
				{ &OnPremiseUsersColumns::m_ColCertificates, &OnPremiseUser::SetCertificates },
				{ &OnPremiseUsersColumns::m_ColCompoundIdentitySupported, &OnPremiseUser::SetCompoundIdentitySupported },
				{ &OnPremiseUsersColumns::m_ColKerberosEncryptionType, &OnPremiseUser::SetKerberosEncryptionType },
				{ &OnPremiseUsersColumns::m_ColLogonHours, &OnPremiseUser::SetLogonHours },
				{ &OnPremiseUsersColumns::m_ColMemberOf, &OnPremiseUser::SetMemberOf },
				{ &OnPremiseUsersColumns::m_ColPrincipalsAllowedToDelegateToAccount, &OnPremiseUser::SetPrincipalsAllowedToDelegateToAccount },
				{ &OnPremiseUsersColumns::m_ColProxyAddresses, &OnPremiseUser::SetProxyAddresses },
				{ &OnPremiseUsersColumns::m_ColServicePrincipalNames, &OnPremiseUser::SetServicePrincipalNames },
				{ &OnPremiseUsersColumns::m_ColSIDHistory, &OnPremiseUser::SetSIDHistory },
				{ &OnPremiseUsersColumns::m_ColUserCertificate, &OnPremiseUser::SetUserCertificate },
			};

			for (const auto& sl : stringListSetters)
				m_StringListSetters[this->*(sl.first)] = sl.second;
		}

		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseUsersColumns::*, std::function<void(OnPremiseUser&, const boost::YOpt<vector<YTimeDate>>&)>>> dateListSetters
			{
				{ &OnPremiseUsersColumns::m_ColDSCorePropagationData, &OnPremiseUser::SetdSCorePropagationData },
			};

			for (const auto& dl : dateListSetters)
				m_DateListSetters[this->*(dl.first)] = dl.second;
		}

		m_Created = true;
		return true;
	}

	return false;
}

vector<GridBackendField> OnPremiseUsersColumns::GetFields(GridBackendRow& p_Row)
{
	vector<GridBackendField> fields;
	fields.reserve(m_Columns.size());

	for (auto col : m_Columns)
		fields.emplace_back(p_Row.GetField(col));

	return fields;
}

void OnPremiseUsersColumns::SetBlankFields(GridBackendRow& p_Row)
{
	p_Row.RemoveFields(m_Columns);
}

const vector<GridBackendColumn*>& OnPremiseUsersColumns::GetColumns() const
{
	return m_Columns;
}

const vector<GridBackendColumn*>& OnPremiseUsersColumns::GetCommonColumns() const
{
	return m_CommonColumns;
}

const wstring& OnPremiseUsersColumns::GetTitle(const wstring& p_YUID)
{
	static std::map<wstring, wstring> columnTitles
	{
		{ _YUID("metaType"), _T("O365/On-Premises") },
		{ _YUID("commonDisplayName"), _T("Display Name") },
		{ _YUID("commonUserPrincipalName"), _T("Username") },
		{ _YUID("onPremsamAccountName"), _T("Sam Account Name") },
		{ _YUID("onPremAccountExpires"), _T("Account Expires") },
		{ _YUID("onPremAccountLockoutTime"), _T("Account Lockout Time") },
		{ _YUID("onPremAccountNotDelegated"), _T("Account not delegated") },
		{ _YUID("onPremAdminCount"), _T("Admin Count") },
		{ _YUID("onPremAllowReversiblePasswordEncryption"), _T("Allow Reversible Password Encryption") },
		{ _YUID("onPremAuthenticationPolicy"), _T("Authentication Policy") },
		{ _YUID("onPremAuthenticationPolicySilo"), _T("Authentication Policy Silo") },
		{ _YUID("onPremBadLogonCount"), _T("Bad Logon Count") },
		{ _YUID("onPremBadPasswordTime"), _T("Bad Password Time") },
		{ _YUID("onPremBadPwdCount"), _T("Bad-Pwd-Count") },
		{ _YUID("onPremCannotChangePassword"), _T("Cannot Change Password") },
		{ _YUID("onPremCanonicalName"), _T("Canonical Name") },
		{ _YUID("onPremCertificates"), _T("Certificates") },
		{ _YUID("onPremCity"), _T("City") },
		{ _YUID("onPremCN"), _T("CN") },
		{ _YUID("onPremCodePage"), _T("Code Page") },
		{ _YUID("onPremCompany"), _T("Company") },
		{ _YUID("onPremCompoundIdentitySupported"), _T("Compound Identity Supported") },
		{ _YUID("onPremCountry"), _T("Country") },
		{ _YUID("onPremCountryCode"), _T("Country Code") },
		{ _YUID("onPremCreated"), _T("Created") },
		{ _YUID("onPremDeleted"), _T("Deleted") },
		{ _YUID("onPremDepartment"), _T("Department") },
		{ _YUID("onPremDescription"), _T("Description") },
		{ _YUID("onPremDisplayName"), _T("Display Name") },
		{ _YUID("onPremDistinguishedName"), _T("Distinguished Name") },
		{ _YUID("onPremOrganizationalUnit"), _T("Organizational Unit") },
		{ _YUID("onPremDivision"), _T("Division") },
		{ _YUID("onPremDoesNotRequirePreAuth"), _T("Does not require PreAuth") },
		{ _YUID("onPremDsCorePropagationData"), _T("dsCorePropagationData") },
		{ _YUID("onPremEmailAddress"), _T("Email Address") },
		{ _YUID("onPremEmployeeID"), _T("Employee ID") },
		{ _YUID("onPremEmployeeNumber"), _T("Employee Number") },
		{ _YUID("onPremEnabled"), _T("Enabled") },
		{ _YUID("onPremFax"), _T("Fax") },
		{ _YUID("onPremGivenName"), _T("Given Name") },
		{ _YUID("onPremHomeDirectory"), _T("Home Directory") },
		{ _YUID("onPremHomeDirRequired"), _T("Homedir Required") },
		{ _YUID("onPremHomeDrive"), _T("Home Drive") },
		{ _YUID("onPremHomePage"), _T("Home Page") },
		{ _YUID("onPremHomePhone"), _T("Home Phone") },
		{ _YUID("onPremInitials"), _T("Initials") },
		{ _YUID("onPremInstanceType"), _T("Instance Type") },
		{ _YUID("onPremIsCriticalSystemObject"), _T("Is Critical System Object") },
		{ _YUID("onPremIsDeleted"), _T("Is Deleted") },
		{ _YUID("onPremKerberosEncryptionType"), _T("Kerberos Encryption Type") },
		{ _YUID("onPremLastBadPasswordAttempt"), _T("Last Bad Password Attempt") },
		{ _YUID("onPremLastKnownParent"), _T("Last known parent") },
		{ _YUID("onPremLastLogoff"), _T("Last Logoff") },
		{ _YUID("onPremLastLogon"), _T("Last Logon") },
		{ _YUID("onPremLastLogonDate"), _T("Last Logon Date") },
		{ _YUID("onPremLockedOut"), _T("Locked Out") },
		{ _YUID("onPremLogonCount"), _T("Logon Count") },
		{ _YUID("onPremLogonHours"), _T("Logon Hours") },
		{ _YUID("onPremLogonWorkstations"), _T("Logon Workstations") },
		{ _YUID("onPremManagerFriendly"), _T("Manager (Friendly)") },
		{ _YUID("onPremManager"), _T("Manager") },
		{ _YUID("onPremMemberOfFriendly"), _T("Member Of (Friendly)") },
		{ _YUID("onPremMemberOf"), _T("Member Of") },
		{ _YUID("onPremMnsLogonAccount"), _T("MNS Logon Account") },
		{ _YUID("onPremMobilePhone"), _T("Mobile Phone") },
		{ _YUID("onPremModified"), _T("Modified") },
		{ _YUID("onPremMSDSConsistencyGuid"), _T("MsDS Consistency Guid") },
		{ _YUID("onPremMsdsUserAccountControlComputed"), _T("MsDS User Account Control Computed") },
		{ _YUID("onPremName"), _T("Name") },
		{ _YUID("onPremNTSecurityDescriptor"), _T("nTSecurityDescriptor") },
		{ _YUID("onPremObjectCategory"), _T("Object Category") },
		{ _YUID("onPremObjectClass"), _T("Object Class") },
		{ _YUID("onPremObjectGUID"), _T("Object GUID") },
		{ _YUID("onPremObjectSid"), _T("Object Sid") },
		{ _YUID("onPremOffice"), _T("Office") },
		{ _YUID("onPremOfficePhone"), _T("Office Phone") },
		{ _YUID("onPremOrganization"), _T("Organization") },
		{ _YUID("onPremOtherName"), _T("Other Name") },
		{ _YUID("onPremPasswordExpired"), _T("Password Expired") },
		{ _YUID("onPremPasswordLastSet"), _T("Password Last Set") },
		{ _YUID("onPremPasswordNeverExpires"), _T("Password never expires") },
		{ _YUID("onPremPasswordNotRequired"), _T("Password not required") },
		{ _YUID("onPremPOBox"), _T("PO Box") },
		{ _YUID("onPremPostalCode"), _T("Postal Code") },
		{ _YUID("onPremPrimaryGroup"), _T("Primary Group") },
		{ _YUID("onPremPrimaryGroupId"), _T("Primary Group ID") },
		{ _YUID("onPremPrincipalsAllowedToDelegateToAccount"), _T("Principals allowed to delegate to account") },
		{ _YUID("onPremProfilePath"), _T("Profile Path") },
		{ _YUID("onPremProtectedFromAccidentalDeletion"), _T("Protected from accidental deletion") },
		{ _YUID("onPremProxyAddresses"), _T("Proxy addresses") },
		{ _YUID("onPremSAMAccountType"), _T("sAM Account Type") },
		{ _YUID("onPremServicePrincipalNames"), _T("Service Principal Names") },
		{ _YUID("onPremScriptPath"), _T("Script Path") },
		{ _YUID("onPremSDRightsEffective"), _T("SD Rights Effective") },
		{ _YUID("onPremSid"), _T("SID") },
		{ _YUID("onPremSidHistory"), _T("SID History") },
		{ _YUID("onPremSmartcardLogonRequired"), _T("Smartcard logon required") },
		{ _YUID("onPremState"), _T("State") },
		{ _YUID("onPremStreetAddress"), _T("Street Address") },
		{ _YUID("onPremSurname"), _T("Surname") },
		{ _YUID("onPremTitle"), _T("Title") },
		{ _YUID("onPremTrustedForDelegation"), _T("Trusted for delegation") },
		{ _YUID("onPremTrustedToAuthForDelegation"), _T("Trusted to auth for delegation") },
		{ _YUID("onPremUseDESKeyOnly"), _T("Use DES key only") },
		{ _YUID("onPremUserAccountControl"), _T("User Account Control") },
		{ _YUID("onPremUserCertificate"), _T("User Certificate") },
		{ _YUID("onPremUserPrincipalName"), _T("User Principal Name") },
		{ _YUID("onPremUSNChanged"), _T("uSNChanged") },
		{ _YUID("onPremUSNCreated"), _T("uSNCreated") },
		{ _YUID("onPremWhenChanged"), _T("When Changed") },
		{ _YUID("onPremWhenCreated"), _T("When Created") },
		{ _YUID("onPremImmutableId"), _T("Immutable Id") },
	};


	const auto findIt = columnTitles.find(p_YUID);
	if (findIt != columnTitles.end())
		return findIt->second;

	ASSERT(false);
	return Str::g_EmptyString;
}

boost::YOpt<std::vector<PooledString>> OnPremiseUsersColumns::ToFriendyNames(const boost::YOpt<std::vector<PooledString>>& p_DistinguishedNames)
{
	boost::YOpt<std::vector<PooledString>> friendlyNames;

	if (p_DistinguishedNames)
	{
		friendlyNames.emplace();
		for (const auto& str : *p_DistinguishedNames)
			friendlyNames->emplace_back(ToFriendyName(str));
	}

	return friendlyNames;
}

PooledString OnPremiseUsersColumns::ToFriendyName(const PooledString& p_DistinguishedName)
{
	PooledString str;
	if (!p_DistinguishedName.IsEmpty())
	{
		wstring name = p_DistinguishedName;
		auto p1 = Str::find(name, _YTEXT("CN="));
		ASSERT(0 == p1);
		if (wstring::npos != p1)
		{
			auto p2 = Str::find(name, _YTEXT(","), p1);

			if (wstring::npos != p1)
				str = name.substr(p1 + 3, p2 - (p1 + 3));
			else
				str = name.substr(p1 + 3);
		}
	}
	return str;
}

boost::YOpt<PooledString> OnPremiseUsersColumns::ToFriendyName(const boost::YOpt<PooledString>& p_DistinguishedName)
{
	boost::YOpt<PooledString> res;

	if (p_DistinguishedName)
		res = ToFriendyName(*p_DistinguishedName);

	return res;
}

PooledString OnPremiseUsersColumns::GetOrganizationalUnit(const PooledString& p_DistinguishedName)
{
	PooledString res;
	wstring name = p_DistinguishedName;
	auto parts = Str::explodeIntoVector(name, wstring(_YTEXT(",")));
	std::vector<PooledString> vec;
	bool has = false;
	for (auto& p : parts)
	{
		auto p1 = Str::find(p, _YTEXT("OU="));
		if (wstring::npos == p1)
		{
			if (has)
				break;
			continue;
		}
		has = true;
		ASSERT(0 == p1);
		vec.push_back(p.substr(p1 + 3));
	}

	std::reverse(vec.begin(), vec.end());
	res = Str::implode(vec, _YTEXT("/"), true);

	return res;
}

boost::YOpt<PooledString> OnPremiseUsersColumns::GetOrganizationalUnit(const boost::YOpt<PooledString>& p_DistinguishedName)
{
	boost::YOpt<PooledString> res;

	if (p_DistinguishedName)
		res = GetOrganizationalUnit(*p_DistinguishedName);

	return res;
}

void OnPremiseUsersColumns::SetValue(OnPremiseUser& p_OnPremUser, GridBackendColumn* p_Col, const GridBackendField& p_Field) const
{
	if ((nullptr == p_Col || !GridTemplate::hasValidErrorlessValue(p_Field)) && (p_Field.GetValueStr() != GridBackendUtil::g_NoValueString))
		return;

	// String properties
	{
		auto it = m_StringSetters.find(p_Col);
		if (m_StringSetters.end() != it)
		{
			if (p_Field.IsString())
				(it->second)(p_OnPremUser, PooledString(p_Field.GetValueStr()));

			return;
		}
	}

	// Bool properties
	{
		auto it = m_BoolSetters.find(p_Col);
		if (m_BoolSetters.end() != it)
		{
			if (p_Field.IsBool()
				|| p_Field.IsNumber()) // GridBackendRow::AddFieldForCheckBox sets an int.
				(it->second)(p_OnPremUser, p_Field.GetValueBool());

			return;
		}
	}

	// Int32 properties
	{
		auto it = m_IntSetters.find(p_Col);
		if (m_IntSetters.end() != it)
		{
			if (p_Field.IsNumber())
				(it->second)(p_OnPremUser, static_cast<int32_t>(p_Field.GetValueDouble()));

			return;
		}
	}

	// Int64 properties
	{
		auto it = m_Int64Setters.find(p_Col);
		if (m_Int64Setters.end() != it)
		{
			if (p_Field.IsNumber())
				(it->second)(p_OnPremUser, static_cast<int64_t>(p_Field.GetValueDouble()));

			return;
		}
	}

	// Datetime properties
	{
		auto it = m_DateSetters.find(p_Col);
		if (m_DateSetters.end() != it)
		{
			if (p_Field.IsDate())
				(it->second)(p_OnPremUser, p_Field.GetValueTimeDate());

			return;
		}
	}

	// String list properties
	{
		auto it = m_StringListSetters.find(p_Col);
		if (m_StringListSetters.end() != it)
		{
			if (p_Field.IsMulti() && nullptr != p_Field.GetValuesMulti<PooledString>())
				(it->second)(p_OnPremUser, *p_Field.GetValuesMulti<PooledString>());
			else if (p_Field.IsString())
				(it->second)(p_OnPremUser, { { PooledString(p_Field.GetValueStr()) } });

			return;
		}
	}

	// Datetime list properties
	{
		auto it = m_DateListSetters.find(p_Col);
		if (m_DateListSetters.end() != it)
		{
			if (p_Field.IsMulti() && nullptr != p_Field.GetValuesMulti<YTimeDate>())
				(it->second)(p_OnPremUser, *p_Field.GetValuesMulti<YTimeDate>());
			else if (p_Field.IsDate())
				(it->second)(p_OnPremUser, { { p_Field.GetValueTimeDate() } });

			return;
		}
	}

	// This is not OnPremiseUser data, perfectly normal to reach this point for those columns only.
	ASSERT(m_ColMSDSConsistencyGuid == p_Col
		|| m_ColImmutableId == p_Col
		|| m_ColMemberOfFriendly == p_Col
		|| m_ColManagerFriendly == p_Col
		|| m_ColOrganizationalUnit == p_Col
		|| m_ColCommonDisplayName == p_Col
		|| m_ColCommonUserName == p_Col
		|| m_ColCommonMetaType == p_Col);
}
