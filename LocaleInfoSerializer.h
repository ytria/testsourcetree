#pragma once

#include "IJsonSerializer.h"

class LocaleInfoSerializer : public IJsonSerializer
{
public:
    LocaleInfoSerializer(const LocaleInfo& p_LocaleInfo);
    void Serialize() override;

private:
    const LocaleInfo& m_LocaleInfo;
};

