#include "RecurrencePatternDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"

void RecurrencePatternDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeInt32(_YTEXT("dayOfMonth"), m_Data.DayOfMonth, p_Object);

    {
        ListStringDeserializer svd;
        if (JsonSerializeUtil::DeserializeAny(svd, _YTEXT("daysOfWeek"), p_Object))
            m_Data.DaysOfWeek = svd.GetData();
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("firstDayOfWeek"), m_Data.FirstDayOfWeek, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("index"), m_Data.Index, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("interval"), m_Data.Interval, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("month"), m_Data.Month, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.Type, p_Object);
}
