#include "MessageBodyViewer.h"

#include "AttachmentInfo.h"
#include "BusinessChatMessage.h"
#include "BusinessEvent.h"
#include "BusinessMessage.h"
#include "BusinessPost.h"
#include "CacheGrid.h"
#include "GridUtil.h"
#include "MessageBodyData.h"

MessageBodyViewer::MessageBodyViewer(GridMessages& p_Grid, GridBackendColumn* p_ColBodyContentType, GridBackendColumn* p_ColSubject, GridBackendColumn* p_ColIsDraft)
	: MessageBodyViewer(p_Grid, p_ColBodyContentType, p_ColSubject, p_ColIsDraft, std::make_unique<BusinessTypeSpecificUtil<BusinessMessage>>())
{
	m_BMGrid = &p_Grid;
}

MessageBodyViewer::MessageBodyViewer(GridEvents& p_Grid, GridBackendColumn* p_ColBodyContentType, GridBackendColumn* p_ColSubject)
	: MessageBodyViewer(p_Grid, p_ColBodyContentType, p_ColSubject, nullptr, std::make_unique<BusinessTypeSpecificUtil<BusinessEvent>>())
{
	m_BEGrid = &p_Grid;
}

MessageBodyViewer::MessageBodyViewer(GridPosts& p_Grid, GridBackendColumn* p_ColBodyContentType, GridBackendColumn* p_ColSubject)
    : MessageBodyViewer(p_Grid, p_ColBodyContentType, p_ColSubject, nullptr, std::make_unique<BusinessTypeSpecificUtil<BusinessPost>>())
{
	m_BPGrid = &p_Grid;
}

MessageBodyViewer::MessageBodyViewer(GridTeamChannelMessages& p_Grid, GridBackendColumn* p_ColBodyContentType, GridBackendColumn* p_ColSubject)
	: MessageBodyViewer(p_Grid, p_ColBodyContentType, p_ColSubject, nullptr, std::make_unique<BusinessTypeSpecificUtil<BusinessChatMessage>>())
{
	m_BTCMGrid = &p_Grid;
}

MessageBodyViewer::MessageBodyViewer(O365Grid& p_Grid, GridBackendColumn* p_ColBodyContentType, GridBackendColumn* p_ColSubject, GridBackendColumn* p_ColIsDraft, std::unique_ptr<TypeSpecificUtil> p_TypeSpecificUtil)
	: m_Grid(p_Grid)
	, m_ColBodyContentType(p_ColBodyContentType)
	, m_ColSubject(p_ColSubject)
	, m_ColIsDraft(p_ColIsDraft)
	, m_IsShowingMessageBody(false)
	, m_DlgContentViewer(nullptr)
	, m_TypeSpecificUtil(std::move(p_TypeSpecificUtil))
	, m_BMGrid(nullptr)
	, m_BEGrid(nullptr)
	, m_BPGrid(nullptr)
	, m_BTCMGrid(nullptr)
	, m_AddBoilerplateHtml(false)
{
}

MessageBodyViewer::~MessageBodyViewer()
{
	Close();
}

void MessageBodyViewer::ShowSelectedMessageBody()
{
	// If there are exploded rows, they will have the same message ID.
	// Only include one of these rows. That way a multiple selection will be interpreted as a single selection.
	// (See Bug #180815.RL.0089BA)
	vector<GridBackendRow*> rows;
	{
		set<wstring> messageIds;
		for (auto r : m_Grid.GetSelectedRows())
		{
			if (!r->IsGroupRow())
			{
				const wstring messageId = r->GetField(((O365Grid*)&m_Grid)->GetColId()).GetValueStr();
				if (messageIds.insert(messageId).second)
					rows.push_back(r);
			}
		}
	}

    int nbRowsSelected = 0;
	GridBackendRow* row = nullptr;
	for (auto r : rows)
    {
		if (m_TypeSpecificUtil->IsValidRow(r))
		{
			if (nullptr == row)
				row = r;
			++nbRowsSelected;
		}
    }

    if (nbRowsSelected > 0)
    {
		const bool multiSelection = nbRowsSelected > 1;
		if (multiSelection)
			row = nullptr;

        bool isDraft = false;
        if (nullptr != m_ColIsDraft && nullptr != row)
        {
            const auto& field = row->GetField(m_ColIsDraft);
            if (field.IsBool())
                isDraft = field.GetValueBool();
        }

        MessageBodyData* messageBodyData = nullptr != row ? (MessageBodyData*)row->GetLParam() : nullptr;
        const wchar_t* content = nullptr;
        if (isDraft)
            content = m_TypeSpecificUtil->GetIsDraftText().c_str();
        else if (nullptr != messageBodyData && messageBodyData->m_BodyContent.is_initialized())
            content = messageBodyData->m_BodyContent->c_str();
        else if (multiSelection)
            content = m_TypeSpecificUtil->GetMultiSelectionText().c_str();

		wstring title;
		DlgContentViewer::HeaderConfig hc;
		if (nullptr != row)
		{
			if (nullptr != m_BMGrid)
			{
				hc = ModuleUtil::GetDlgContentViewerHeaderConfig(m_BMGrid->getBusinessObject(row));
				title = YtriaTranslate::Do(MessageBodyViewer_ShowSelectedMessageBody_1, _YLOC("Message Viewer")).c_str();
			}
			else if (nullptr != m_BEGrid)
			{
				hc = ModuleUtil::GetDlgContentViewerHeaderConfig(m_BEGrid->getBusinessObject(row));
				title = YtriaTranslate::Do(MessageBodyViewer_ShowSelectedMessageBody_2, _YLOC("Event Viewer")).c_str();
			}
			else if (nullptr != m_BPGrid)
			{
				hc = ModuleUtil::GetDlgContentViewerHeaderConfig(m_BPGrid->getBusinessObject(row));
				title = YtriaTranslate::Do(MessageBodyViewer_ShowSelectedMessageBody_3, _YLOC("Post Viewer")).c_str();
			}
			else if (nullptr != m_BTCMGrid)
			{
				hc = ModuleUtil::GetDlgContentViewerHeaderConfig(m_BTCMGrid->getBusinessObject(row));
				title = YtriaTranslate::Do(MessageBodyViewer_ShowSelectedMessageBody_4, _YLOC("Message Viewer")).c_str();
			}
		}

		const wstring type = (nullptr != row && row->HasField(m_ColBodyContentType)) ? row->GetField(m_ColBodyContentType).GetValueStr() : L"text";
        const bool isHtml = multiSelection || type == _YTEXT("html");
		const bool showLoadImagesButton = isHtml
										&& !multiSelection
										&& nullptr != content
										&& m_Grid.IsFrameConnected()
										&& GridUtil::ContainsImgWithCID(content);
        if (nullptr == m_DlgContentViewer || !::IsWindow(m_DlgContentViewer->m_hWnd))
        {
            delete m_DlgContentViewer;

			m_DlgContentViewer = new DlgContentViewer(&m_Grid, this, this, title, &m_Grid, m_AddBoilerplateHtml);
			m_DlgContentViewer->SetLoadImagesCommandTarget(Command::ModuleTarget::Message);
            m_DlgContentViewer->SetLoadImagesCommandTask(Command::ModuleTask::DownloadInlineAttachments);
			m_DlgContentViewer->SetContent(hc, nullptr != content ? content : _YTEXT(""), isHtml, showLoadImagesButton);
            m_DlgContentViewer->DoModeless();
        }
        else
        {
            m_DlgContentViewer->SetContent(hc, nullptr != content ? content : _YTEXT(""), isHtml, showLoadImagesButton);
        }

        m_DlgContentViewer->ShowWindow(SW_SHOW);
    }
	else
	{
		if (nullptr != m_DlgContentViewer || ::IsWindow(m_DlgContentViewer->m_hWnd))
		{
			m_DlgContentViewer->SetContent(DlgContentViewer::HeaderConfig(), m_TypeSpecificUtil->GetNoSelectionText().c_str(), true, false);
		}
		else
		{
			ASSERT(false);
		}
	}
}

void MessageBodyViewer::ToggleView()
{
    if (IsShowing())
        Close();
    else
        ShowSelectedMessageBody();
}

bool MessageBodyViewer::IsShowing() const
{
    return nullptr != m_DlgContentViewer && ::IsWindow(m_DlgContentViewer->m_hWnd) && m_DlgContentViewer->IsWindowVisible();
}

void MessageBodyViewer::SetAddBoilerplateHtml(const bool& p_Val)
{
	m_AddBoilerplateHtml = p_Val;
}

void MessageBodyViewer::Previous()
{
	GridBackendRow* pLastFocusRow = m_Grid.GetBackend().GetLastFocusedRow();
	if (nullptr != pLastFocusRow)
	{
		const vector < GridBackendRow* >& RowsForDisplay = m_Grid.GetBackend().GetBackendRowsForDisplay();
		auto it = std::find(RowsForDisplay.begin(), RowsForDisplay.end(), pLastFocusRow);
		ASSERT(it != RowsForDisplay.end());

		// Go to the previous row which does not have the same ID.
		GridBackendRow* pPrevRow = nullptr;
		GridBackendRow* pCurRow = *it;
		ASSERT(nullptr != pCurRow);
		wstring CurId = pCurRow->GetField(((O365Grid*)&m_Grid)->GetColId()).GetValueStr();
		ASSERT(!CurId.empty());
		wstring PrevId = CurId;
		while (PrevId == CurId)
		{
			if (it == RowsForDisplay.begin())
				it = RowsForDisplay.end();
			it--;

			pPrevRow = *it;
			ASSERT(nullptr != pPrevRow);
			PrevId = pPrevRow->GetField(((O365Grid*)&m_Grid)->GetColId()).GetValueStr();
			ASSERT(!PrevId.empty());
		}

		ASSERT(nullptr != pPrevRow);
		m_Grid.SetRowFocusedAndSelected(pPrevRow);
		m_Grid.UpdateMegaShark();
	}

	// 	// Get current focus.
	// 	CPoint ptTemp(m_Grid.FocusGet());
	// 	if (ptTemp.y < 0)
	// 		// Set to first row.
	// 		ptTemp.y = 0;
	// 	else if (ptTemp.y > m_Grid.GetBackend().RsRecordsCount() - 1)
	// 		// Set to last row.
	// 		ptTemp.y = m_Grid.GetBackend().RsRecordsCount() - 1;
	// 	else
	// 		// Decrement the index by 1.
	// 		ptTemp.y--;
	// 
	// 	m_Grid.FocusSetRowByVisibleIndex(ptTemp.y, false);
}

void MessageBodyViewer::Next()
{
	GridBackendRow* pLastFocusRow = m_Grid.GetBackend().GetLastFocusedRow();
	if (nullptr != pLastFocusRow)
	{
		const vector < GridBackendRow* >& RowsForDisplay = m_Grid.GetBackend().GetBackendRowsForDisplay();
		auto it = std::find(RowsForDisplay.begin(), RowsForDisplay.end(), pLastFocusRow);
		ASSERT(it != RowsForDisplay.end());

		// Go to the next row which does not have the same ID.
		GridBackendRow* pNextRow = nullptr;
		GridBackendRow* pCurRow = *it;
		ASSERT(nullptr != pCurRow);
		wstring CurId = pCurRow->GetField(((O365Grid*)&m_Grid)->GetColId()).GetValueStr();
		ASSERT(!CurId.empty());
		wstring NextId = CurId;
		while (NextId == CurId)
		{
			it++;
			if (it == RowsForDisplay.end())
				it = RowsForDisplay.begin();

			pNextRow = *it;
			ASSERT(nullptr != pNextRow);
			NextId = pNextRow->GetField(((O365Grid*)&m_Grid)->GetColId()).GetValueStr();
			ASSERT(!NextId.empty());
		}

		ASSERT(nullptr != pNextRow);
		m_Grid.SetRowFocusedAndSelected(pNextRow);
		m_Grid.UpdateMegaShark();
	}

	// 	// Get current focus.
	// 	CPoint ptTemp(m_Grid.FocusGet());
	// 	if (ptTemp.y < 0)
	// 		// Set to first row.
	// 		ptTemp.y = 0;
	// 	else if (ptTemp.y > m_Grid.GetBackend().RsRecordsCount() - 1)
	// 		// Set to last row.
	// 		ptTemp.y = m_Grid.GetBackend().RsRecordsCount() - 1;
	// 	else
	// 		// Increment the index by 1.
	// 		ptTemp.y++;
	// 
	// 	m_Grid.FocusSetRowByVisibleIndex(ptTemp.y, false);
}

AttachmentsInfo::IDs MessageBodyViewer::GetIds(GridBackendRow* p_Row)
{
	if (nullptr != m_BMGrid)
		return AttachmentInfo(*m_BMGrid, _YTEXT(""), p_Row);

	if (nullptr != m_BEGrid)
		return AttachmentInfo(*m_BEGrid, _YTEXT(""), p_Row);
	
	if (nullptr != m_BPGrid)
		return AttachmentInfo(*m_BPGrid, _YTEXT(""), p_Row);

	ASSERT(false);
	return AttachmentsInfo::IDs();
}

void MessageBodyViewer::Close()
{
    if (nullptr != m_DlgContentViewer && ::IsWindow(m_DlgContentViewer->m_hWnd))
        m_DlgContentViewer->SendMessage(WM_CLOSE, 0, 0);
	delete m_DlgContentViewer;
	m_DlgContentViewer = nullptr;
}

// =================================================================================================

namespace
{
	template<class RTTRType> static const wstring& getNoSelectionText();
	template<class RTTRType> static const wstring& getMultiSelectionText();
    template<class RTTRType> static const wstring& getIsDraftText();
	template<class RTTRType> static bool isValidRow(const GridBackendRow* p_Row);

	template<class RTTRType>
    const wstring& getIsDraftText()
    {
        static const wstring isDraftMessage = _YTEXT("<html><center><b>") + YtriaTranslate::Do(MessageBodyViewer_Close_1, _YLOC("No preview available because this message is a draft.")) + _YTEXT("</b></center></html>");
        return isDraftMessage;
    }

	template<>
	static const wstring& getNoSelectionText<BusinessMessage>()
	{
		static const wstring noSelectionHTML = _YTEXT("<html><center><b>") + YtriaTranslate::Do(MessageBodyViewer_Close_2, _YLOC("No message selected.")) + _YTEXT("</b></center></html>");
		return noSelectionHTML;
	}

	template<>
	static const wstring& getNoSelectionText<BusinessEvent>()
	{
		static const wstring noSelectionHTML = _YTEXT("<html><center><b>") + YtriaTranslate::Do(MessageBodyViewer_Close_3, _YLOC("No event selected.")) + _YTEXT("</b></center></html>");
		return noSelectionHTML;
	}

    template<>
    static const wstring& getNoSelectionText<BusinessPost>()
    {
        static const wstring noSelectionHTML = _YTEXT("<html><center><b>") + YtriaTranslate::Do(MessageBodyViewer_Close_4, _YLOC("No post selected.")) + _YTEXT("</b></center></html>");
        return noSelectionHTML;
    }

	template<>
	static const wstring& getNoSelectionText<BusinessChatMessage>()
	{
		static const wstring noSelectionHTML = _YTEXT("<html><center><b>") + YtriaTranslate::Do(MessageBodyViewer_Close_8, _YLOC("No message selected.")) + _YTEXT("</b></center></html>");
		return noSelectionHTML;
	}

	template<>
	static const wstring& getMultiSelectionText<BusinessMessage>()
	{
		static const wstring multiSelectionHTML = _YTEXT("<html><center><b>") + YtriaTranslate::Do(MessageBodyViewer_Close_5, _YLOC("Multiple messages selected.")) + _YTEXT("</b></center></html>");
		return multiSelectionHTML;
	}

	template<>
	static const wstring& getMultiSelectionText<BusinessEvent>()
	{
		static const wstring multiSelectionHTML = _YTEXT("<html><center><b>") + YtriaTranslate::Do(MessageBodyViewer_Close_6, _YLOC("Mutliple events selected.")) + _YTEXT("</b></center></html>");
		return multiSelectionHTML;
	}

    template<>
    static const wstring& getMultiSelectionText<BusinessPost>()
    {
        static const wstring multiSelectionHTML = _YTEXT("<html><center><b>") + YtriaTranslate::Do(MessageBodyViewer_Close_7, _YLOC("Multiple posts selected.")) + _YTEXT("</b></center></html>");
        return multiSelectionHTML;
    }

	template<>
	static const wstring& getMultiSelectionText<BusinessChatMessage>()
	{
		static const wstring multiSelectionHTML = _YTEXT("<html><center><b>") + YtriaTranslate::Do(MessageBodyViewer_Close_9, _YLOC("Multiple messages selected.")) + _YTEXT("</b></center></html>");
		return multiSelectionHTML;
	}

	template<class RTTRType>
	bool isValidRow(const GridBackendRow* p_Row)
	{
		return GridUtil::isBusinessType<RTTRType>(p_Row);
	}

	template<>
	bool isValidRow<BusinessChatMessage>(const GridBackendRow* p_Row)
	{
		return GridUtil::IsBusinessChatMessageOrReply(p_Row);
	}
}

template<class RTTRRegisteredType>
const wstring& MessageBodyViewer::BusinessTypeSpecificUtil<RTTRRegisteredType>::GetNoSelectionText() const
{
	return getNoSelectionText<RTTRRegisteredType>();
}

template<class RTTRRegisteredType>
const wstring& MessageBodyViewer::BusinessTypeSpecificUtil<RTTRRegisteredType>::GetIsDraftText() const
{
    return getIsDraftText<RTTRRegisteredType>();
}

template<class RTTRRegisteredType>
const wstring& MessageBodyViewer::BusinessTypeSpecificUtil<RTTRRegisteredType>::GetMultiSelectionText() const
{
	return getMultiSelectionText<RTTRRegisteredType>();
}

template<class RTTRRegisteredType>
bool MessageBodyViewer::BusinessTypeSpecificUtil<RTTRRegisteredType>::IsValidRow(const GridBackendRow* p_Row) const
{
	return isValidRow<RTTRRegisteredType>(p_Row);
}
