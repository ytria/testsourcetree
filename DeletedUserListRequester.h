#pragma once

#include "IRequester.h"
#include "IPageRequestLogger.h"

template <class T, class U>
class ValueListDeserializer;

class BusinessUser;
class UserDeserializer;
class IPropertySetBuilder;
class PaginatedRequestResults;

class DeletedUserListRequester : public IRequester
{
public:
    DeletedUserListRequester(const IPropertySetBuilder& p_PropertySet, const std::shared_ptr<IPageRequestLogger>& p_Logger);

    virtual TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    vector<BusinessUser>& GetData();

private:
    vector<rttr::property> m_Properties;

    std::shared_ptr<ValueListDeserializer<BusinessUser, UserDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;
};

