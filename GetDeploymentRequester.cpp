#include "GetDeploymentRequester.h"

#include "AzureSession.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "BasicHttpRequestLogger.h"

Azure::GetDeploymentRequester::GetDeploymentRequester(PooledString p_SubscriptionId, PooledString p_ResourceGroup, PooledString p_DeploymentName)
	:m_SubscriptionId(p_SubscriptionId),
	m_ResourceGroup(p_ResourceGroup),
	m_DeploymentName(p_DeploymentName)
{
}

TaskWrapper<void> Azure::GetDeploymentRequester::Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<Azure::DeploymentDeserializer>();
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri(_YTEXT("subscriptions"));
	uri.append_path(m_SubscriptionId);
	uri.append_path(_YTEXT("resourceGroups"));
	uri.append_path(m_ResourceGroup);
	uri.append_path(_YTEXT("providers"));
	uri.append_path(_YTEXT("Microsoft.Resources"));
	uri.append_path(_YTEXT("deployments"));
	uri.append_path(web::uri::encode_data_string(m_DeploymentName));
	uri.append_query(AzureSession::g_ApiVersionKey, _YTEXT("2018-05-01"));

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
	return p_Session->GetAzureSession()->Get(uri.to_uri(), httpLogger, m_Result, m_Deserializer, p_TaskData);
}

const Azure::Deployment& Azure::GetDeploymentRequester::GetData() const
{
	return m_Deserializer->GetData();
}
