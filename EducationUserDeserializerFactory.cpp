#include "EducationUserDeserializerFactory.h"
#include "EducationUserDeserializer.h"

EducationUserDeserializerFactory::EducationUserDeserializerFactory(std::shared_ptr<const Sapio365Session> p_Session)
	:m_Session(p_Session)
{
}

EducationUserDeserializer EducationUserDeserializerFactory::Create()
{
	return EducationUserDeserializer(m_Session);
}
