#pragma once

#include "CommandInfo.h"
#include "ModuleBase.h"
#include "IRequestLogger.h"

class FrameSites;

class ModuleSites : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

private:
    virtual void executeImpl(const Command& p_Command) override;
	void showRootSite(const Command& p_Command);
    void showGroupRootSite(const Command& p_Command);
	void showChannelRootSite(const Command& p_Command);
	void LoadMore(const Command& p_Command);
	void loadSnapshot(const Command& p_Command);
};

