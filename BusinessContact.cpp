#include "BusinessContact.h"
#include "MsGraphFieldNames.h"
#include "O365AdminUtil.h"
#include "safeTaskCall.h"

using namespace Util;

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessContact>("Contact") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Contact"))))
		.constructor()(policy::ctor::as_object)

// *** Basic Information
		.property(O365_CONTACT_GIVENNAME, &BusinessContact::m_GivenName)(
			metadata(MetadataKeys::ValidForCreation, true)
			)
		.property(O365_CONTACT_MIDDLENAME, &BusinessContact::m_MiddleName)
		.property(O365_CONTACT_SURNAME, &BusinessContact::m_Surname)
		.property(O365_CONTACT_DISPLAYNAME, &BusinessContact::m_DisplayName)
		.property(O365_CONTACT_CREATEDDATETIME, &BusinessContact::m_CreatedDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_CONTACT_LASTMODIFIEDDATETIME, &BusinessContact::m_LastModifiedDateTime)(
			metadata(MetadataKeys::ReadOnly, true)
			)
		.property(O365_CONTACT_PARENTFOLDERID, &BusinessContact::m_ParentFolderId)
		.property(O365_CONTACT_CATEGORIES, &BusinessContact::m_Categories)
		.property(O365_CONTACT_FILEAS, &BusinessContact::m_FileAs)
		.property(O365_CONTACT_PERSONALNOTES, &BusinessContact::m_PersonalNotes)
		//.property(O365_CONTACT_EMAILADDRESSESADDRESS, &BusinessContact::m_EmailAddressesAddress)
		.property(O365_CONTACT_EMAILADDRESSES, &BusinessContact::m_EmailAddressesAndName)
		.property(O365_CONTACT_EMAILADDRESSESNAME, &BusinessContact::m_EmailAddressesName)
		.property(O365_CONTACT_MOBILEPHONE, &BusinessContact::m_MobilePhone)
		.property(O365_CONTACT_IMADDRESSES, &BusinessContact::m_ImAddresses)
		.property(O365_CONTACT_CHANGEKEY, &BusinessContact::m_ChangeKey)(
			metadata(MetadataKeys::ReadOnly, true)
			)

// ***** Business Information
		.property(O365_CONTACT_COMPANYNAME, &BusinessContact::m_CompanyName)
		.property(O365_CONTACT_OFFICELOCATION, &BusinessContact::m_OfficeLocation)
		.property(O365_CONTACT_DEPARTMENT, &BusinessContact::m_Department)
		.property(O365_CONTACT_JOBTITLE, &BusinessContact::m_JobTitle)
		.property(O365_CONTACT_PROFESSION, &BusinessContact::m_Profession)
		.property(O365_CONTACT_MANAGER, &BusinessContact::m_Manager)
		.property(O365_CONTACT_ASSISTANTNAME, &BusinessContact::m_AssistantName)
		.property(O365_CONTACT_BUSINESSPHONES, &BusinessContact::m_BusinessPhones)
		//.property(O365_CONTACT_BUSINESSADDRESSCITY, &BusinessContact::m_BusinessAddressCity)
		//.property(O365_CONTACT_BUSINESSADDRESSCOUNTRYORREGION, &BusinessContact::m_BusinessAddressCountryOrRegion)
		//.property(O365_CONTACT_BUSINESSADDRESSPOSTALCODE, &BusinessContact::m_BusinessAddressPostalCode)
		//.property(O365_CONTACT_BUSINESSADDRESSSTATE, &BusinessContact::m_BusinessAddressState)
		//.property(O365_CONTACT_BUSINESSADDRESSSTREET, &BusinessContact::m_BusinessAddressStreet)
		.property(O365_CONTACT_BUSINESSHOMEPAGE, &BusinessContact::m_BusinessHomePage)

// **** Personal Information
		.property(O365_CONTACT_TITLE, &BusinessContact::m_Title)
		.property(O365_CONTACT_NICKNAME, &BusinessContact::m_Nickname)
		.property(O365_CONTACT_INITIALS, &BusinessContact::m_Initials)
		.property(O365_CONTACT_BIRTHDAY, &BusinessContact::m_Birthday)
		.property(O365_CONTACT_SPOUSENAME, &BusinessContact::m_SpouseName)
		.property(O365_CONTACT_CHILDREN, &BusinessContact::m_Children)
		.property(O365_CONTACT_GENERATION, &BusinessContact::m_Generation)
		.property(O365_CONTACT_HOMEPHONES, &BusinessContact::m_HomePhones)
		//.property(O365_CONTACT_HOMEADDRESSCITY, &BusinessContact::m_HomeAddressCity)
		//.property(O365_CONTACT_HOMEADDRESSCOUNTRYORREGION, &BusinessContact::m_HomeAddressCountryOrRegion)
		//.property(O365_CONTACT_HOMEADDRESSPOSTALCODE, &BusinessContact::m_HomeAddressPostalCode)
		//.property(O365_CONTACT_HOMEADDRESSSTATE, &BusinessContact::m_HomeAddressState)
		//.property(O365_CONTACT_HOMEADDRESSSTREET, &BusinessContact::m_HomeAddressStreet)
		//.property(O365_CONTACT_OTHERADDRESSCITY, &BusinessContact::m_OtherAddressCity)
		//.property(O365_CONTACT_OTHERADDRESSCOUNTRYORREGION, &BusinessContact::m_OtherAddressCountryOrRegion)
		//.property(O365_CONTACT_OTHERADDRESSPOSTALCODE, &BusinessContact::m_OtherAddressPostalCode)
		//.property(O365_CONTACT_OTHERADDRESSSTATE, &BusinessContact::m_OtherAddressState)
		//.property(O365_CONTACT_OTHERADDRESSSTREET, &BusinessContact::m_OtherAddressStreet)

// ***** Yomi Information
		.property(O365_CONTACT_YOMICOMPANYNAME, &BusinessContact::m_YomiCompanyName)
		.property(O365_CONTACT_YOMIGIVENNAME, &BusinessContact::m_YomiGivenName)
		.property(O365_CONTACT_YOMISURNAME, &BusinessContact::m_YomiSurnameName)

		;
}

BusinessContact::BusinessContact(const Contact& p_Contact)
{
    SetID(p_Contact.m_Id);

    if (boost::none != p_Contact.m_DisplayName)
        m_DisplayName = *p_Contact.m_DisplayName;
    if (boost::none != p_Contact.m_Surname)
        m_Surname = *p_Contact.m_Surname;
    if (boost::none != p_Contact.m_GivenName)
        m_GivenName = *p_Contact.m_GivenName;
    if (boost::none != p_Contact.m_MiddleName)
        m_MiddleName = *p_Contact.m_MiddleName;
    if (boost::none != p_Contact.m_Manager)
        m_Manager = *p_Contact.m_Manager;
    if (boost::none != p_Contact.m_JobTitle)
        m_JobTitle = *p_Contact.m_JobTitle;
    if (boost::none != p_Contact.m_MobilePhone)
        m_MobilePhone = *p_Contact.m_MobilePhone;

    if (boost::none != p_Contact.m_AssistantName)
        m_AssistantName = *p_Contact.m_AssistantName;
    if (boost::none != p_Contact.m_Birthday)
        m_Birthday = *p_Contact.m_Birthday;
    if (boost::none != p_Contact.m_BusinessHomePage)
        m_BusinessHomePage = *p_Contact.m_BusinessHomePage;
    if (boost::none != p_Contact.m_ChangeKey)
        m_ChangeKey = *p_Contact.m_ChangeKey;
    if (boost::none != p_Contact.m_CompanyName)
        m_CompanyName = *p_Contact.m_CompanyName;
    if (boost::none != p_Contact.m_CreatedDateTime)
        m_CreatedDateTime = *p_Contact.m_CreatedDateTime;
    if (boost::none != p_Contact.m_Department)
        m_Department = *p_Contact.m_Department;
    if (boost::none != p_Contact.m_FileAs)
        m_FileAs = *p_Contact.m_FileAs;
    if (boost::none != p_Contact.m_Generation)
        m_Generation = *p_Contact.m_Generation;
    if (boost::none != p_Contact.m_Initials)
        m_Initials = *p_Contact.m_Initials;
    if (boost::none != p_Contact.m_LastModifiedDateTime)
        m_LastModifiedDateTime = *p_Contact.m_LastModifiedDateTime;
    if (boost::none != p_Contact.m_NickName)
        m_Nickname = *p_Contact.m_NickName;
    if (boost::none != p_Contact.m_OfficeLocation)
        m_OfficeLocation = *p_Contact.m_OfficeLocation;
    if (boost::none != p_Contact.m_ParentFolderId)
        m_ParentFolderId = *p_Contact.m_ParentFolderId;
    if (boost::none != p_Contact.m_PersonalNotes)
        m_PersonalNotes = *p_Contact.m_PersonalNotes;
    if (boost::none != p_Contact.m_Profession)
        m_Profession = *p_Contact.m_Profession;
    if (boost::none != p_Contact.m_SpouseName)
        m_SpouseName = *p_Contact.m_SpouseName;
    if (boost::none != p_Contact.m_Title)
        m_Title = *p_Contact.m_Title;
    if (boost::none != p_Contact.m_YomiCompanyName)
        m_YomiCompanyName = *p_Contact.m_YomiCompanyName;
    if (boost::none != p_Contact.m_YomiGivenName)
        m_YomiGivenName = *p_Contact.m_YomiGivenName;
    if (boost::none != p_Contact.m_YomiSurnameName)
        m_YomiSurnameName = *p_Contact.m_YomiSurnameName;

    for (const auto& val : p_Contact.m_EmailAddresses)
    {
		CString Address;
		if (boost::none != val.m_Address)
		{
			m_EmailAddressesAddress.push_back(*val.m_Address);
			Address = val.m_Address.get().c_str();
		}

		CString Name;
		if (boost::none != val.m_Name)
		{
			m_EmailAddressesName.push_back(*val.m_Name);
			Name = val.m_Name.get().c_str();
		}

		ASSERT(m_EmailAddressesAddress.size() == m_EmailAddressesName.size());

		CString NameAndAddress;
		NameAndAddress.Format(_YTEXT("<%s> %s"), Name, Address);
		m_EmailAddressesAndName.push_back((LPCTSTR)NameAndAddress);
    }

    if (boost::none != p_Contact.m_HomePhones)
    {
        for (const auto& val : p_Contact.m_HomePhones.get())
            m_HomePhones.push_back(val);
    }

    if (boost::none != p_Contact.m_ImAddresses)
    {
        for (const auto& val : p_Contact.m_ImAddresses.get())
            m_ImAddresses.push_back(val);
    }

    if (boost::none != p_Contact.m_Children)
    {
        for (const auto& val : p_Contact.m_Children.get())
            m_Children.push_back(val);
    }

    if (boost::none != p_Contact.m_BusinessPhones)
    {
        for (const auto& val : p_Contact.m_BusinessPhones.get())
            m_BusinessPhones.push_back(val);
    }

    if (boost::none != p_Contact.m_Categories)
    {
        for (const auto& val : p_Contact.m_Categories.get())
            m_Categories.push_back(val);
    }

    if (boost::none != p_Contact.m_BusinessAddress.get().City)
        m_BusinessAddressCity = *p_Contact.m_BusinessAddress.get().City;
    if (boost::none != p_Contact.m_BusinessAddress.get().CountryOrRegion)
        m_BusinessAddressCountryOrRegion = *p_Contact.m_BusinessAddress.get().CountryOrRegion;
    if (boost::none != p_Contact.m_BusinessAddress.get().PostalCode)
        m_BusinessAddressPostalCode = *p_Contact.m_BusinessAddress.get().PostalCode;
    if (boost::none != p_Contact.m_BusinessAddress.get().State)
        m_BusinessAddressState = *p_Contact.m_BusinessAddress.get().State;
    if (boost::none != p_Contact.m_BusinessAddress.get().Street)
        m_BusinessAddressStreet = *p_Contact.m_BusinessAddress.get().Street;
    if (boost::none != p_Contact.m_OtherAddress.get().City)
        m_OtherAddressCity = *p_Contact.m_OtherAddress.get().City;
    if (boost::none != p_Contact.m_OtherAddress.get().CountryOrRegion)
        m_OtherAddressCountryOrRegion = *p_Contact.m_OtherAddress.get().CountryOrRegion;
    if (boost::none != p_Contact.m_OtherAddress.get().PostalCode)
        m_OtherAddressPostalCode = *p_Contact.m_OtherAddress.get().PostalCode;
    if (boost::none != p_Contact.m_OtherAddress.get().State)
        m_OtherAddressState = *p_Contact.m_OtherAddress.get().State;
    if (boost::none != p_Contact.m_OtherAddress.get().Street)
        m_OtherAddressStreet = *p_Contact.m_OtherAddress.get().Street;
    if (boost::none != p_Contact.m_HomeAddress.get().City)
        m_HomeAddressCity = *p_Contact.m_HomeAddress.get().City;
    if (boost::none != p_Contact.m_HomeAddress.get().CountryOrRegion)
        m_HomeAddressCountryOrRegion = *p_Contact.m_HomeAddress.get().CountryOrRegion;
    if (boost::none != p_Contact.m_HomeAddress.get().PostalCode)
        m_HomeAddressPostalCode = *p_Contact.m_HomeAddress.get().PostalCode;
    if (boost::none != p_Contact.m_HomeAddress.get().State)
        m_HomeAddressState = *p_Contact.m_HomeAddress.get().State;
    if (boost::none != p_Contact.m_HomeAddress.get().Street)
        m_HomeAddressStreet = *p_Contact.m_HomeAddress.get().Street;

    m_ContactFolderName = p_Contact.m_ContactFolderName ? *p_Contact.m_ContactFolderName : YtriaTranslate::Do(BusinessContact_BusinessContact_1, _YLOC("Default Contact Folder")).c_str();
}

BusinessContact::BusinessContact()
{
    m_ContactFolderName = YtriaTranslate::Do(BusinessContact_BusinessContact_1, _YLOC("Default Contact Folder")).c_str();
}

Contact BusinessContact::ToContact(const BusinessContact & p_BusinessContact)
{
    Contact contact;
    contact.m_Id = p_BusinessContact.GetID();
    contact.m_DisplayName = p_BusinessContact.m_DisplayName;
    contact.m_Surname = p_BusinessContact.m_Surname;
    contact.m_GivenName = p_BusinessContact.m_GivenName;
    contact.m_MiddleName = p_BusinessContact.m_MiddleName;
    contact.m_Manager = p_BusinessContact.m_Manager;
    contact.m_JobTitle = p_BusinessContact.m_JobTitle;
    contact.m_MobilePhone = p_BusinessContact.m_MobilePhone;
    contact.m_AssistantName = p_BusinessContact.m_AssistantName;
    contact.m_Birthday = p_BusinessContact.m_Birthday;
    contact.m_BusinessHomePage = p_BusinessContact.m_BusinessHomePage;
    contact.m_ChangeKey = p_BusinessContact.m_ChangeKey;
    contact.m_CompanyName = p_BusinessContact.m_CompanyName;
    contact.m_CreatedDateTime = p_BusinessContact.m_CreatedDateTime;
    contact.m_Department = p_BusinessContact.m_Department;
    contact.m_FileAs = p_BusinessContact.m_FileAs;
    contact.m_Generation = p_BusinessContact.m_Generation;
    contact.m_Initials = p_BusinessContact.m_Initials;
    contact.m_LastModifiedDateTime = p_BusinessContact.m_LastModifiedDateTime;
    contact.m_NickName = p_BusinessContact.m_Nickname;
    contact.m_OfficeLocation = p_BusinessContact.m_OfficeLocation;
    contact.m_ParentFolderId = p_BusinessContact.m_ParentFolderId;
    contact.m_PersonalNotes = p_BusinessContact.m_PersonalNotes;
    contact.m_Profession = p_BusinessContact.m_Profession;
    contact.m_SpouseName = p_BusinessContact.m_SpouseName;
    contact.m_Title = p_BusinessContact.m_Title;
    contact.m_YomiCompanyName = p_BusinessContact.m_YomiCompanyName;
    contact.m_YomiGivenName = p_BusinessContact.m_YomiGivenName;
    contact.m_YomiSurnameName = p_BusinessContact.m_YomiSurnameName;

	ASSERT(p_BusinessContact.m_EmailAddressesAddress.size() == p_BusinessContact.m_EmailAddressesName.size());
    for (size_t i = 0; i < p_BusinessContact.m_EmailAddressesAddress.size(); i++)
	{
		EmailAddress Temp;
		Temp.m_Address = p_BusinessContact.m_EmailAddressesAddress.at(i);
		Temp.m_Name = p_BusinessContact.m_EmailAddressesName.at(i);
		contact.m_EmailAddresses.push_back(Temp);
    }

    for (const auto& val : p_BusinessContact.m_EmailAddressesName)
    {
        // TODO:
    }

    for (const auto& val : p_BusinessContact.m_HomePhones)
    {
        // TODO:
    }

    for (const auto& val : p_BusinessContact.m_ImAddresses)
    {
        // TODO:
    }

    for (const auto& val : p_BusinessContact.m_Children)
    {
        // TODO:
    }

    for (const auto& val : p_BusinessContact.m_BusinessPhones)
    {
        // TODO:
    }

    for (const auto& val : p_BusinessContact.m_Categories)
    {
        // TODO:
    }

	if(boost::none != contact.m_BusinessAddress)
	{
		contact.m_BusinessAddress.get().City = p_BusinessContact.m_BusinessAddressCity;
		contact.m_BusinessAddress.get().CountryOrRegion = p_BusinessContact.m_BusinessAddressCountryOrRegion;
		contact.m_BusinessAddress.get().PostalCode = p_BusinessContact.m_BusinessAddressPostalCode;
		contact.m_BusinessAddress.get().State = p_BusinessContact.m_BusinessAddressState;
		contact.m_BusinessAddress.get().Street = p_BusinessContact.m_BusinessAddressStreet;
	}

	if (boost::none != contact.m_OtherAddress)
	{
		contact.m_OtherAddress.get().City = p_BusinessContact.m_OtherAddressCity;
		contact.m_OtherAddress.get().CountryOrRegion = p_BusinessContact.m_OtherAddressCountryOrRegion;
		contact.m_OtherAddress.get().PostalCode = p_BusinessContact.m_OtherAddressPostalCode;
		contact.m_OtherAddress.get().State = p_BusinessContact.m_OtherAddressState;
		contact.m_OtherAddress.get().Street = p_BusinessContact.m_OtherAddressStreet;
	}

	if (boost::none != contact.m_HomeAddress)
	{
		contact.m_HomeAddress.get().City = p_BusinessContact.m_HomeAddressCity;
		contact.m_HomeAddress.get().CountryOrRegion = p_BusinessContact.m_HomeAddressCountryOrRegion;
		contact.m_HomeAddress.get().PostalCode = p_BusinessContact.m_HomeAddressPostalCode;
		contact.m_HomeAddress.get().State = p_BusinessContact.m_HomeAddressState;
		contact.m_HomeAddress.get().Street = p_BusinessContact.m_HomeAddressStreet;
	}

    return contact;
}
