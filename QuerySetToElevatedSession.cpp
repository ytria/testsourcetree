#include "QuerySetToElevatedSession.h"
#include "PersistentSession.h"
#include "SessionsSqlEngine.h"
#include "SqlQueryPreparedStatement.h"
#include "SessionTypes.h"

QuerySetToElevatedSession::QuerySetToElevatedSession(const PersistentSession& p_Session, SessionsSqlEngine& p_Engine)
	: m_Session(p_Session)
	, m_Engine(p_Engine)
{
	ASSERT(m_Session.IsElevated() || m_Session.IsPartnerElevated());
}

void QuerySetToElevatedSession::Run()
{
	try
	{
		m_Engine.StartTransaction();

		wstring queryCredStr = _YTEXT(R"(UPDATE Credentials SET AppId=?,AppTenant=?,AppClientSecret=? WHERE Id=?)");
		SqlQueryPreparedStatement queryCreds(m_Engine, queryCredStr);
		queryCreds.BindString(1, m_Session.GetAppId());
		queryCreds.BindString(2, m_Session.GetTenantName());
		queryCreds.BindString(3, m_Session.GetAppClientSecret());
		queryCreds.BindInt64(4, m_Session.GetCredentialsId());
		queryCreds.Run();

		wstring querySessionsStr = _YTEXT(R"(UPDATE Sessions SET SessionType=? WHERE EmailOrAppId=? AND SessionType=? AND RoleId=0)");
		SqlQueryPreparedStatement querySessions(m_Engine, querySessionsStr);
		querySessions.BindString(1, m_Session.GetSessionType());
		querySessions.BindString(2, m_Session.GetEmailOrAppId());
		querySessions.BindString(3, m_Session.IsElevated() ? SessionTypes::g_AdvancedSession : SessionTypes::g_PartnerAdvancedSession);
		querySessions.Run();

		if (queryCreds.IsSuccess() && querySessions.IsSuccess())
			m_Engine.EndTransaction();
		else
			m_Engine.RollbackTransaction();
	}
	catch (const std::exception&)
	{
		m_Engine.RollbackTransaction();
		throw;
	}
}
