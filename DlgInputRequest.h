#pragma once

#include "Sapio365Session.h"

class DlgInputRequest : public ResizableDialog
{
public:
	DlgInputRequest(std::shared_ptr<Sapio365Session> p_Session, const wstring& p_SessionDisplayName, CWnd* p_Parent);

protected:
	virtual BOOL OnInitDialogSpecificResizable() override;
	virtual void DoDataExchange(CDataExchange* pDX) override;
	virtual void OnOK() override;

	afx_msg void OnSelType();
	DECLARE_MESSAGE_MAP()

private:
	std::shared_ptr<Sapio365Session> m_Session;
	const wstring m_SessionDisplayName;

	CExtLabel m_SessionName;
	CExtComboBox m_TypeCombo;
	CExtComboBox m_APICombo;
	CExtComboBox m_SessionCombo;
	CExtEdit m_URLEdit;
	CExtButton m_RunButton;
	CExtEdit m_JSonBodyEdit;

	class MyTextEdit : public CExtEdit
	{
	public:
		using CExtEdit::CExtEdit;

	protected:
		virtual COLORREF OnQueryBackColor() const;
	};
	MyTextEdit m_ResultEdit;
	MyTextEdit m_ResponseEdit;
};