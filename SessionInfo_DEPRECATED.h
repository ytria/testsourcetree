#pragma once

#include "Sapio365SessionSavedInfo_DEPRECATED.h"
#include "SessionStatus.h"

struct SessionInfo_DEPRECATED
{
public:
    SessionInfo_DEPRECATED();
    bool operator==(const SessionInfo_DEPRECATED& p_Other) const;

    static vector<SessionInfo_DEPRECATED> GetSessions(bool p_Sort);

	// If m_RoleId != 0, load the role name from m_SavedInfo or RoleDelegationManager if necessary
	wstring GetRoleName() const;

    Sapio365SessionSavedInfo_DEPRECATED m_SavedInfo;
    int64_t m_RoleId;
};

