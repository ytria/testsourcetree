#pragma once

#include "IRequester.h"
#include "IRequestLogger.h"

template <class T, class U>
class ValueListDeserializer;

class BusinessGroupSettingTemplate;
class GroupSettingTemplateDeserializer;

class GroupSettingTemplatesRequester : public IRequester
{
public:
	GroupSettingTemplatesRequester(const std::shared_ptr<IRequestLogger>& p_Logger);
    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    const vector<BusinessGroupSettingTemplate>& GetData() const;

private:
    std::shared_ptr<ValueListDeserializer<BusinessGroupSettingTemplate, GroupSettingTemplateDeserializer>> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
};

