#include "PostFileAttachmentRequester.h"

#include "BusinessFileAttachment.h"
#include "FileAttachmentDeserializer.h"
#include "MSGraphSession.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

PostFileAttachmentRequester::PostFileAttachmentRequester(PooledString p_GroupId, PooledString p_ConversationId, PooledString p_ThreadId, PooledString p_PostId, PooledString p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger)
    : m_GroupId(p_GroupId)
	, m_ConversationId(p_ConversationId)
	, m_ThreadId(p_ThreadId)
	, m_PostId(p_PostId)
	, m_AttachmentId(p_AttachmentId)
	, m_Logger(p_Logger)
{
}

TaskWrapper<void> PostFileAttachmentRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<FileAttachmentDeserializer>();

    web::uri_builder uri(_YTEXT("groups"));
    uri.append_path(m_GroupId);
    uri.append_path(_YTEXT("conversations"));
    uri.append_path(m_ConversationId);
    uri.append_path(_YTEXT("threads"));
    uri.append_path(m_ThreadId);
    uri.append_path(_YTEXT("posts"));
    uri.append_path(m_PostId);
    uri.append_path(_YTEXT("attachments"));
    uri.append_path(m_AttachmentId);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
    return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, uri.to_uri(), false, m_Logger, httpLogger, p_TaskData);
}

const BusinessFileAttachment& PostFileAttachmentRequester::GetData() const
{
    return m_Deserializer->GetData();
}
