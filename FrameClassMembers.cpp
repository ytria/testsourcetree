#include "FrameClassMembers.h"
#include "Command.h"
#include "GridSchools.h"

IMPLEMENT_DYNAMIC(FrameClassMembers, GridFrameBase)

FrameClassMembers::FrameClassMembers(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
	m_CreateRefreshPartial = false;
}

void FrameClassMembers::BuildView(const vector<EducationSchool>& p_Schools, const vector<O365UpdateOperation>& p_Updates, bool p_FullPurge)
{
	m_Grid.BuildView(p_Schools, p_Updates, p_FullPurge);
}

O365Grid& FrameClassMembers::GetGrid()
{
	return m_Grid;
}

void FrameClassMembers::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	if (HasLastUpdateOperations())
		info.Data() = GetInfoForRefreshAfterUpdate();
	else
		info.Data() = GetInfoForRefreshAll();

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::ClassMembers, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameClassMembers::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{

}

bool FrameClassMembers::HasApplyButton()
{
	return true;
}

void FrameClassMembers::ApplySpecific(bool p_Selected)
{
	auto grid = dynamic_cast<GridClassMembers*>(&GetGrid());

	ASSERT(nullptr != grid);
	if (nullptr != grid)
	{
		CommandInfo info;
		if (p_Selected)
		{
			vector<GridBackendRow*> selectedRows(grid->GetSelectedRows().begin(), grid->GetSelectedRows().end());
			info.Data() = grid->GetChanges(selectedRows);
		}
		else
			info.Data() = grid->GetChanges();
		info.SetFrame(this);
		info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::ClassMembers, Command::ModuleTask::ApplyChanges, info, { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
	}
}

void FrameClassMembers::ApplyAllSpecific()
{
	ApplySpecific(false);
}

void FrameClassMembers::ApplySelectedSpecific()
{
	ApplySpecific(true);
}

void FrameClassMembers::createGrid()
{
	m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FrameClassMembers::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewDataEditGroups(tab);
	
	auto editGroup = findGroup(tab, g_ActionsEditGroup.c_str());
	ASSERT(nullptr != editGroup);
	if (nullptr != editGroup)
	{
		{
			auto ctrl = editGroup->Add(xtpControlButton, ID_CLASSMEMBERSGRID_ADDMEMBER);
			GridFrameBase::setImage({ ID_CLASSMEMBERSGRID_ADDMEMBER }, IDB_CLASSMEMBERSGRID_ADDMEMBER, xtpImageNormal);
			GridFrameBase::setImage({ ID_CLASSMEMBERSGRID_ADDMEMBER }, IDB_CLASSMEMBERSGRID_ADDMEMBER_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = editGroup->Add(xtpControlButton, ID_CLASSMEMBERSGRID_REMOVEMEMBER);
			GridFrameBase::setImage({ ID_CLASSMEMBERSGRID_REMOVEMEMBER }, IDB_CLASSMEMBERSGRID_REMOVEMEMBER, xtpImageNormal);
			GridFrameBase::setImage({ ID_CLASSMEMBERSGRID_REMOVEMEMBER }, IDB_CLASSMEMBERSGRID_REMOVEMEMBER_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameClassMembers::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;
	return statusRV;
}

vector<EducationSchool> FrameClassMembers::GetInfoForRefreshAll()
{
	vector<EducationSchool> schools;
	
	GridClassMembers* grid = dynamic_cast<GridClassMembers*>(&GetGrid());
	ASSERT(nullptr != grid);
	if (nullptr != grid)
	{
		for (auto row : grid->GetBackendRows())
		{
			if (GridUtil::isBusinessType<EducationClass>(row))
			{
				EducationSchool school;
				school.m_Id = row->GetField(grid->m_ColSchoolId).GetValueStr();
				school.SetDisplayName(PooledString(row->GetParentRow()->GetField(grid->m_ColDisplayName).GetValueStr()));

				auto ittSchool = std::find(schools.begin(), schools.end(), school);
				if (ittSchool == schools.end())
				{
					schools.push_back(school);
					ittSchool = schools.end() - 1;
				}

				EducationClass edClass;
				edClass.SetID(row->GetField(grid->m_ColClassId).GetValueStr());
				edClass.SetDisplayName(PooledString(row->GetField(grid->m_ColDisplayName).GetValueStr()));

				auto ittClass = std::find(ittSchool->m_Classes.begin(), ittSchool->m_Classes.end(), edClass);
				if (ittClass == ittSchool->m_Classes.end())
					ittSchool->m_Classes.push_back(edClass);
			}
		}
	}

	return schools;
}

vector<EducationSchool> FrameClassMembers::GetInfoForRefreshAfterUpdate()
{
	vector<EducationSchool> schools;

	GridClassMembers* grid = dynamic_cast<GridClassMembers*>(&GetGrid());
	if (nullptr != grid)
	{
		for (const auto& update : GetLastUpdateOperations())
		{
			const auto& pk = update.GetPrimaryKey();
			auto row = grid->GetRowIndex().GetRow(pk, false, false);
			ASSERT(nullptr != row);
			if (nullptr != row)
			{
				if (GridUtil::isBusinessType<EducationUser>(row))
				{
					EducationSchool school;
					school.m_Id = row->GetField(grid->m_ColSchoolId).GetValueStr();
					school.SetDisplayName(PooledString(row->GetParentRow()->GetParentRow()->GetField(grid->m_ColDisplayName).GetValueStr()));

					auto ittSchool = std::find(schools.begin(), schools.end(), school);
					if (ittSchool == schools.end())
					{
						schools.push_back(school);
						ittSchool = schools.end() - 1;
					}

					EducationClass edClass;
					edClass.SetID(row->GetField(grid->m_ColClassId).GetValueStr());
					edClass.SetDisplayName(PooledString(row->GetParentRow()->GetField(grid->m_ColDisplayName).GetValueStr()));

					auto ittClass = std::find(ittSchool->m_Classes.begin(), ittSchool->m_Classes.end(), edClass);
					if (ittClass == ittSchool->m_Classes.end())
						ittSchool->m_Classes.push_back(edClass);
				}
			}
		}
	}

	return schools;
}
