#include "FrameListItems.h"

#include "AutomationNames.h"

IMPLEMENT_DYNAMIC(FrameListItems, GridFrameBase)

//BEGIN_MESSAGE_MAP(FrameListItems, GridFrameBase)
//END_MESSAGE_MAP()

FrameListItems::FrameListItems(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
    m_CreateRefreshPartial = true;
}

void FrameListItems::ShowListItems(const O365DataMap<wstring, vector<BusinessList>>& p_Lists, Origin p_Origin, bool p_FullPurge)
{
    if (CompliesWithDataLoadPolicy())
    {
		ASSERT(GetOrigin() == p_Origin);
        m_ListItemsGrid.BuildView(p_Lists, p_Origin, p_FullPurge);
    }
}

O365Grid& FrameListItems::GetGrid()
{
    return m_ListItemsGrid;
}

void FrameListItems::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
	info.SetOrigin(GetModuleCriteria().m_Origin);
	ASSERT(ModuleCriteria::UsedContainer::SUBIDS == GetModuleCriteria().m_UsedContainer);
	if (ModuleCriteria::UsedContainer::SUBIDS == GetModuleCriteria().m_UsedContainer)
		info.GetSubIds() = GetModuleCriteria().m_SubIDs;

    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::ListsItem, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameListItems::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::ListsItem, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameListItems::createGrid()
{
    m_ListItemsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameListItems::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigLists();
}

AutomatedApp::AUTOMATIONSTATUS FrameListItems::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	return statusRV;
}