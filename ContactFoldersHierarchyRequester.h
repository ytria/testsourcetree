#pragma once

#include "IRequester.h"
#include "IPageRequestLogger.h"

class BusinessContactFolder;
class ContactFolderDeserializer;
class PaginatedRequestResults;
class Sapio365Session;

template <class T, class U>
class ValueListDeserializer;

class ContactFoldersHierarchyRequester : public IRequester
{
public:
    ContactFoldersHierarchyRequester(const PooledString& p_UserId, const PooledString& p_ContactFolderId, const std::shared_ptr<IPageRequestLogger>& p_Logger);
    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    vector<BusinessContactFolder>& GetData();

private:
    PooledString m_UserId;
    PooledString m_ContactFolderId;

    std::shared_ptr<ValueListDeserializer<BusinessContactFolder, ContactFolderDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;
};

