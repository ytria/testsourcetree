#pragma once

#include "BusinessLifeCyclePolicies.h"
#include "DlgFormsHTML.h"

class DlgBusinessLCPEditHTML : public DlgFormsHTML
{
public:
	DlgBusinessLCPEditHTML(BusinessLifeCyclePolicies& p_BusinessLCP, DlgFormsHTML::Action p_Action, CWnd* p_Parent);
	virtual ~DlgBusinessLCPEditHTML();

	virtual void generateJSONScriptData();

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data);

private:
	// Keep. Coming soon.
	// 	using NumberGetter = std::function<const boost::YOpt<int32_t>&(const BusinessLifeCyclePolicies&)>;
	// 	using NumberSetter = std::function<void(BusinessLifeCyclePolicies&, const boost::YOpt<int32_t>&)>;
	// 	void addNumberEditor(NumberGetter p_Getter, NumberSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags);

	using StringGetter = std::function<const boost::YOpt<PooledString>&(const BusinessLifeCyclePolicies&)>;
	using StringSetter = std::function<void(BusinessLifeCyclePolicies&, const boost::YOpt<PooledString>&)>;
	void addStringEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags);
	void addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_ValuesAndLabels);

private:
	BusinessLifeCyclePolicies& m_BusinessLCP;

	// Keep. Coming soon.
	// std::map<wstring, NumberSetter> m_NumberSetters;

	std::map<wstring, StringSetter> m_StringSetters;
};
