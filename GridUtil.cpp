#include "includes_WIN.h"
#include "includes_STL.h"

#include "GridUtil.h"

#include "Application.h"
#include "BusinessAADUserConversationMember.h"
#include "BusinessChannel.h"
#include "BusinessChatMessage.h"
#include "BusinessChatMessageReply.h"
#include "BusinessContact.h"
#include "BusinessConversation.h"
#include "BusinessDirectoryRoleTemplate.h"
#include "BusinessDrive.h"
#include "BusinessDriveItemFolder.h"
#include "BusinessEvent.h"
#include "BusinessFileAttachment.h"
#include "BusinessGroup.h"
#include "BusinessGroupDeleted.h"
#include "BusinessGroupSetting.h"
#include "BusinessGroupSettingTemplate.h"
#include "BusinessItemAttachment.h"
#include "BusinessList.h"
#include "BusinessListItem.h"
#include "BusinessMailFolder.h"
#include "BusinessMessage.h"
#include "BusinessMessageRule.h"
#include "BusinessOrgContact.h"
#include "BusinessPost.h"
#include "BusinessSite.h"
#include "BusinessSubscribedSku.h"
#include "BusinessUser.h"
#include "BusinessUserDeleted.h"
#include "BusinessUserGuest.h"
#include "BusinessUserGuestDeleted.h"
#include "BusinessUserUnknown.h"
#include "BusinessUserUnknownDeleted.h"
#include "ChilkatUtils.h"
#include "CommandInfo.h"
#include "DelegateUser.h"
#include "DirectoryAudit.h"
#include "EducationClass.h"
#include "EducationSchool.h"
#include "FileUtil.h"
#include "GridUpdater.h"
#include "LoggerService.h"
#include "MailboxPermissions.h"
#include "MFCUtil.h"
#include "O365AdminUtil.h"
#include "OnPremiseGroup.h"
#include "OnPremiseUser.h"
#include "RoleDefinition.h"
#include "SignIn.h"
#include "SpGroup.h"
#include "SpUser.h"

#include "resource.h"
#include <regex>

bool GridUtil::IsAtLeastOneErrorlessSelected(CacheGrid& p_Grid)
{
	return std::any_of(p_Grid.GetSelectedRows().begin(), p_Grid.GetSelectedRows().end(), [](GridBackendRow* p_Row)
	{
		return !p_Row->IsGroupRow() && !p_Row->IsError();
	});
}

int GridUtil::getBusinessTypeId(const GridBackendRow* p_Row)
{
    int type = -1;

    if (nullptr != p_Row && nullptr != p_Row->GetBackend() && nullptr != p_Row->GetBackend()->GetCacheGrid())
    {
        const auto& rowTypeField = p_Row->GetField(p_Row->GetBackend()->GetCacheGrid()->GetColumnObjectType());
		if (rowTypeField.HasAlternativeValue())
		{
			const auto& typeSetup = p_Row->GetBackend()->GetCacheGrid()->GetObjectTypeSetup();
			type = typeSetup.at(static_cast<int>(rowTypeField.GetAlternativeValue()));
		}
    }
    else
        ASSERT(false);

    return type;
}

bool GridUtil::IsSelectionAuthorizedByRoleDelegation(const O365Grid& p_Grid, const RoleDelegationUtil::RBAC_Privilege p_Right, const bool p_CheckAncestorPrivilegeIfPresent /*= false*/)
{
	return IsSelectionAuthorizedByRoleDelegation(p_Grid, p_Right, nullptr, p_CheckAncestorPrivilegeIfPresent);
}

bool GridUtil::IsSelectionAuthorizedByRoleDelegation(const O365Grid& p_Grid, const RoleDelegationUtil::RBAC_Privilege p_Right, const std::function<bool(GridBackendRow*)> p_AdditionalCondition, const bool p_CheckAncestorPrivilegeIfPresent /*= false*/)
{
	return std::any_of(p_Grid.GetSelectedRows().begin(), p_Grid.GetSelectedRows().end(), [&p_Grid, &p_AdditionalCondition, &p_Right, p_CheckAncestorPrivilegeIfPresent](auto p_Row)
	{
		return (!p_AdditionalCondition || p_AdditionalCondition(p_Row)) && p_Grid.IsAuthorizedByRoleDelegation(p_Row, p_Right, p_CheckAncestorPrivilegeIfPresent);
	});
}

vector<GridBackendRow*> GridUtil::GetSelectionAuthorizedByRoleDelegation(const O365Grid& p_Grid, const RoleDelegationUtil::RBAC_Privilege p_Right, const std::function<bool(GridBackendRow*)> p_AdditionalCondition, const bool p_CheckAncestorPrivilegeIfPresent /*= false*/)
{
	vector<GridBackendRow*> rows = p_Grid.GetBackend().GetSelectedRowsInOrder();

	rows.erase(std::remove_if(rows.begin(), rows.end(), [&p_Grid, &p_AdditionalCondition, &p_Right, p_CheckAncestorPrivilegeIfPresent](auto p_Row)
	{
		return p_AdditionalCondition && !p_AdditionalCondition(p_Row) || !p_Grid.IsAuthorizedByRoleDelegation(p_Row, p_Right, p_CheckAncestorPrivilegeIfPresent);
	}), rows.end());

	return rows;
}

wstring GridUtil::GetColumnsPath(CacheGrid& p_Grid, GridBackendRow* p_Row, const vector<GridBackendColumn*>& p_Columns)
{
    wstring columnsPath;

    bool first = true;
	for (const auto& col : p_Columns)
    {
		ASSERT(nullptr != col);
		if (nullptr != col && p_Row->HasField(col))
		{
			const auto& field = p_Row->GetField(col);
			ASSERT(field.HasValue());
			if (field.HasValue())
			{
				wstring currentPart;
				if (!first)
					currentPart += _YTEXT(" - ");
				else
					first = false;

				wstring fieldValue;
				col->ToWStringForSorting(&field, fieldValue, true, nullptr);
				currentPart += fieldValue;
				columnsPath += currentPart;
			}
		}
    }

    if (columnsPath.size() > 50)
        columnsPath = std::wstring(std::begin(columnsPath), std::begin(columnsPath) + 50);

    FileUtil::MakeFileNameValid(columnsPath);
    columnsPath = Str::rtrim(columnsPath);

    return columnsPath;
}

void GridUtil::SetUserRowObjectType(CacheGrid& p_Grid, GridBackendRow* p_Row, const BusinessUser& p_User)
{
	if (p_User.IsFromRecycleBin())
	{
		if (p_User.GetUserType().is_initialized() && p_User.GetUserType().get() == BusinessUser::g_UserTypeGuest)
			p_Grid.SetRowObjectType(p_Row, BusinessUserGuestDeleted(), p_User.HasFlag(BusinessObject::CANCELED), NO_ICON, _YTEXT(""));
		else if (!p_User.GetUserType().is_initialized() || p_User.GetUserType().get().IsEmpty() || p_User.GetUserType().get() == BusinessUserUnknown().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>())
			p_Grid.SetRowObjectType(p_Row, BusinessUserUnknownDeleted(), p_User.HasFlag(BusinessObject::CANCELED));
		else
			p_Grid.SetRowObjectType(p_Row, BusinessUserDeleted(), p_User.HasFlag(BusinessObject::CANCELED), NO_ICON, _YTEXT(""));
	}
	else
	{
		if (p_User.GetUserType().is_initialized() && p_User.GetUserType().get() == BusinessUser::g_UserTypeGuest)
			p_Grid.SetRowObjectType(p_Row, BusinessUserGuest(), p_User.HasFlag(BusinessObject::CANCELED), NO_ICON, _YTEXT(""));
		else if (!p_User.GetUserType().is_initialized() || p_User.GetUserType().get().IsEmpty() || p_User.GetUserType().get() == BusinessUserUnknown().get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>())
			p_Grid.SetRowObjectType(p_Row, BusinessUserUnknown(), p_User.HasFlag(BusinessObject::CANCELED));
		else
			p_Grid.SetRowObjectType(p_Row, p_User, p_User.HasFlag(BusinessObject::CANCELED), NO_ICON, _YTEXT(""));
	}
}

void GridUtil::SetGroupRowObjectType(CacheGrid& p_Grid, GridBackendRow* p_Row, const BusinessGroup& p_Group)
{
	if (p_Group.IsFromRecycleBin())
		p_Grid.SetRowObjectType(p_Row, BusinessGroupDeleted(), p_Group.HasFlag(BusinessObject::CANCELED), NO_ICON, _YTEXT(""));
	else
		p_Grid.SetRowObjectType(p_Row, p_Group, p_Group.HasFlag(BusinessObject::CANCELED), NO_ICON, _YTEXT(""));
}

void GridUtil::SetRowObjectType(CacheGrid& p_Grid, GridBackendRow* p_Row, const rttr::variant& p_RowObject)
{
	if (p_RowObject.get_type().get_id() == rttr::type::get<BusinessUser>().get_id())
	{
		SetUserRowObjectType(p_Grid, p_Row, p_RowObject.get_value<BusinessUser>());
	}
	else if (p_RowObject.get_type().get_id() == rttr::type::get<BusinessGroup>().get_id())
	{
		SetGroupRowObjectType(p_Grid, p_Row, p_RowObject.get_value<BusinessGroup>());
	}
	else
	{
		p_Grid.SetRowObjectType(p_Row, p_RowObject, p_RowObject.get_value<BusinessObject>().HasFlag(BusinessObject::CANCELED));
	}
}

boost::YOpt<PooledString> GridUtil::ExtractHtmlBodyPreview(const boost::YOpt<PooledString>& p_HtmlContent)
{
	if (p_HtmlContent)
		return ChilkatUtils::HtmlToText(p_HtmlContent->c_str());
	return boost::none;
}

bool GridUtil::SaveEmailAsEmlFile(const BusinessMessage& p_Message, const vector<BusinessAttachment>& p_Attachments, const wstring& p_FolderPath, bool p_OverwriteExisting)
{
	ChilkatUtils::EmailEmlExporter emailExporter;

	if (p_Message.m_BodyContent)
		emailExporter.SetBody(*p_Message.m_BodyContent, p_Message.m_BodyContentType && *p_Message.m_BodyContentType == _YTEXT("html"));
	//Bounce address
	// charset
	if (p_Message.m_ReceivedDateTime)
		emailExporter.SetDateTime(*p_Message.m_ReceivedDateTime);
	// from
	emailExporter.SetFrom(p_Message.m_FromName ? *p_Message.m_FromName : _YTEXT(""), p_Message.m_FromAddress ? *p_Message.m_FromAddress : _YTEXT(""));
	// local date
	//mailer
	// preferred charset

	if (!p_Message.m_ReplyTo.empty() && p_Message.m_ReplyTo[0].m_EmailAddress && p_Message.m_ReplyTo[0].m_EmailAddress->m_Address)
	{
		ASSERT(p_Message.m_ReplyTo.size() == 1);
		emailExporter.SetReplyTo(*p_Message.m_ReplyTo[0].m_EmailAddress->m_Address);
	}


	if (p_Message.m_SenderAddress)
		emailExporter.SetSender(*p_Message.m_SenderAddress); // p_Message.m_SenderName ??

	wstring filename;
	ASSERT(p_Message.m_Subject);
	if (p_Message.m_Subject)
	{
		emailExporter.SetSubject(*p_Message.m_Subject);
		filename = *p_Message.m_Subject + _YTEXT(".eml");
		FileUtil::MakeFileNameValid(filename, false);
	}

	for (const auto& cc : p_Message.m_CcRecipients)
	{
		if (cc.m_EmailAddress)
			emailExporter.AddCC(cc.m_EmailAddress->m_Name ? *cc.m_EmailAddress->m_Name : _YTEXT(""), cc.m_EmailAddress->m_Address ? *cc.m_EmailAddress->m_Address : _YTEXT(""));
	}

	for (const auto& cc : p_Message.m_BccRecipients)
	{
		if (cc.m_EmailAddress)
			emailExporter.AddBCC(cc.m_EmailAddress->m_Name ? *cc.m_EmailAddress->m_Name : _YTEXT(""), cc.m_EmailAddress->m_Address ? *cc.m_EmailAddress->m_Address : _YTEXT(""));
	}

	for (const auto& header : p_Message.m_InternetMessageHeaders)
		emailExporter.AddHeader(header.m_Name ? *header.m_Name : _YTEXT(""), header.m_Value ? *header.m_Value : _YTEXT(""));

	emailExporter.SetReceiptRequested(p_Message.m_IsDeliveryReceiptRequested && *p_Message.m_IsDeliveryReceiptRequested);

	for (const auto& att : p_Attachments)
	{
		if (att.GetDataType())
		{
			if (*att.GetDataType() == Util::GetFileAttachmentStr())
			{
				auto fielAtt = dynamic_cast<const BusinessFileAttachment*>(&att);
				ASSERT(nullptr != fielAtt);
				if (nullptr != fielAtt)
				{
					ASSERT(fielAtt->GetName() && fielAtt->GetContentBytes() && fielAtt->GetContentType());
					if (fielAtt->GetName() && fielAtt->GetContentBytes() && fielAtt->GetContentType())
						emailExporter.AddAttachment(fielAtt->GetName()->c_str(), utility::conversions::from_base64(*fielAtt->GetContentBytes()), fielAtt->GetContentType()->c_str());
				}
			}
			else
			{
				ASSERT(*att.GetDataType() == Util::GetItemAttachmentStr());
				auto itemAtt = dynamic_cast<const BusinessItemAttachment*>(&att);
				ASSERT(nullptr != itemAtt);
				if (nullptr != itemAtt)
				{
					const auto& item = itemAtt->GetItem();
					switch (item->GetKind())
					{
					case AnyItemAttachment::Kind::Message:
					{
						auto message = item->ToMessage();
						ChilkatUtils::EmailEmlExporter attachedEmailExporter;
						// TOFO: Fill attachedEmailExporter
						emailExporter.AttachEmail(attachedEmailExporter);
					}
					default:
						break;
					}
				}
			}
		}
	}

	ASSERT(!filename.empty());
	if (!filename.empty())
	{
		const auto filePath = p_FolderPath + _YTEXT("\\") + filename;
		FileUtil::CreateFolderHierarchy(filePath);
		return emailExporter.Export(filePath, true);
	}

	return false;
}

bool GridUtil::ContainsImgWithCID(const wstring& p_HtmlContent)
{
	static std::wregex myRegex(_YTEXT(R"(<img[^>]*src="cid:[^"]*"[^>]*>)"));
	try
	{
		return std::regex_search(p_HtmlContent.c_str(), myRegex, std::regex_constants::format_first_only);
	}
	catch (const std::exception& e)
	{
		LoggerService::Debug(_YDUMPFORMAT(L"Str::ContainsImgWithCID: %s", Str::convertFromUTF8(e.what()).c_str()));
		ASSERT(false);
		return false;
	}
}

wstring GridUtil::ReplaceCIDsByPaths(const wstring& p_HtmlContent, const map<wstring, wstring>& p_CidsToPaths)
{
	auto newContent = p_HtmlContent;
	static std::wregex myRegex(_YTEXT(R"((<img[^>]*src=")(cid:[^"]*)("[^>]*>))"));

	std::wsmatch matches;
	while (std::regex_search(newContent, matches, myRegex, std::regex_constants::format_first_only))
	{
		std::wstring str = matches[2];
		// Remove "cid:"
		str = str.substr(wcslen(_YTEXT("cid:")));

		auto it = p_CidsToPaths.find(str);
		if (p_CidsToPaths.end() != it)
		{
			// Firefox only handles URLs in img src, convert path to URL.
			std::wstring replacepattern = _YTEXT("$01") + FileUtil::FilePathToURL(it->second) + _YTEXT("$03");
			newContent = std::regex_replace(newContent, myRegex, replacepattern, std::regex_constants::format_first_only);
		}
		else
		{
			// error !
			ASSERT(false);
			return p_HtmlContent;
		}
	}

	return newContent;
}

bool GridUtil::IsCommandAllowed(const CacheGrid& p_Grid, const int p_CommandID, const Origin p_Origin)
{
	bool allowed = false;
	const auto& autoName = p_Grid.GetAutomationName();

	switch (p_CommandID)
	{
	/*Users*/
	case ID_GRID_SHOWUSERDETAILS:
		allowed =	autoName != g_AutoNameUsers
				&&	autoName != g_AutoNameUsersRecycleBin
				&&	autoName != g_AutoNameLicensesDeletedUsers
				&&	p_Origin != Origin::DeletedUser;
		break;
	case ID_USERGRID_SHOWPARENTGROUPS:
		allowed =	autoName != g_AutoNameUserGroups
				&&	autoName != g_AutoNameUsersRecycleBin 
				&&	autoName != g_AutoNameLicensesDeletedUsers 
				&&	p_Origin != Origin::DeletedUser;
		break;
	case ID_USERGRID_SHOWLICENSES:
		allowed =	autoName != g_AutoNameLicenses
				&&	autoName != g_AutoNameLicensesDeletedUsers;
		break;
	case ID_USERGRID_SHOWMANAGERHIERARCHY:
		allowed =	autoName != g_AutoNameUsersRecycleBin
				&&	autoName != g_AutoNameLicensesDeletedUsers
#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
				&& autoName != g_AutoNameManagerHierarchy
#endif
				&&	p_Origin != Origin::DeletedUser;
		break;
	case ID_USERGRID_SHOWDRIVEITEMS:
		//allowed = autoName != g_AutoNameDriveItems;
		allowed =	autoName == g_AutoNameUsers
				||	autoName == g_AutoNameUsersRecycleBin;
		break;
	case ID_USERGRID_SHOWMESSAGES:
		//allowed = autoName != g_AutoNameMessages;
		allowed =	autoName == g_AutoNameUsers
				||	autoName == g_AutoNameUsersRecycleBin;
		break;
	case ID_USERGRID_SHOWMAILBOXPERMISSIONS:
		allowed = autoName == g_AutoNameUsers;
		break;
	case ID_USERGRID_SHOWEVENTS:
		//allowed = autoName != g_AutoNameEvents;
		allowed =	autoName == g_AutoNameUsers
				||	autoName == g_AutoNameUsersRecycleBin;
		break;
	case ID_USERGRID_SHOWCONTACTS:
		//allowed = autoName != g_AutoNameContacts;
		allowed =	autoName == g_AutoNameUsers
				||	autoName == g_AutoNameUsersRecycleBin;
		break;
	case ID_USERGRID_SHOWMESSAGERULES:
		//allowed = autoName != g_AutoNameMessageRules;
		allowed =	autoName == g_AutoNameUsers
				||	autoName == g_AutoNameUsersRecycleBin;
		break;

	/*Groups*/
	case ID_GRID_SHOWGROUPDETAILS:
		allowed = autoName != g_AutoNameGroups;
		break;
	case ID_GROUPSGRID_SHOWMEMBERS:
		allowed = autoName != g_AutoNameGroupsHierarchy;
		break;
	case ID_GROUPSGRID_SHOWOWNERS:
		allowed = autoName != g_AutoNameGroupsOwnersHierarchy;
		break;
	case ID_GROUPSGRID_SHOWAUTHORS:
		//allowed = autoName != g_AutoNameGroupsAuthorsHierarchy;
		allowed = autoName == g_AutoNameGroups;
		break;
	case ID_GROUPSGRID_SHOWDRIVEITEMS:
		//allowed = autoName != g_AutoNameDriveItems
		allowed = autoName == g_AutoNameGroups;
		break;
	case ID_GROUPSGRID_SHOWCALENDARS:
		//allowed = autoName != g_AutoNameEvents;
		allowed = autoName == g_AutoNameGroups;
		break;
	case ID_GROUPSGRID_SHOW_CHANNELS:
		//allowed = autoName != g_AutoNameChannels;
		allowed = autoName == g_AutoNameGroups;
		break;
	case ID_GROUPSGRID_SHOWCONVERSATIONS:
		//allowed = autoName != g_AutoNameConversations;
		allowed = autoName == g_AutoNameGroups;
		break;
	case ID_GROUPSGRID_SHOWSITES:
		//allowed = autoName != g_AutoNameSites;
		allowed = autoName == g_AutoNameGroups;
		break;
	default:
		ASSERT(false);
		break;
	}

	return allowed;
}

void GridUtil::SetRowIsTeam(const wstring& p_IsTeam, GridBackendRow* p_RowToUpdate, GridBackendColumn* p_IsTeamColumn, int p_TeamIcon)
{
	ASSERT(p_IsTeam.empty() || p_IsTeam == BusinessGroup::g_Team);

	if (!p_IsTeam.empty())
	{
		p_RowToUpdate->AddField(p_IsTeam, p_IsTeamColumn, p_TeamIcon);
	}
	else if (nullptr != p_IsTeamColumn)
	{
		p_RowToUpdate->RemoveField(p_IsTeamColumn);
	}
}

int GridUtil::AddIconTeam(CacheGrid& p_Grid)
{
	return p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_TEAM));
}

void GridUtil::HandleMultiError(GridBackendRow* p_Row, std::vector<std::pair<boost::YOpt<SapioError>, wstring>>&& p_Errors, GridUpdater& p_GridUpdater, const wstring& p_NoPopupSpecificReason)
{
	bool has403 = false;
	bool has403Description = false;
	wstring error403;
	vector<PooledString> errors;

	bool dontPopError = false;
	for (const auto& err : p_Errors)
	{
		if (err.first)
		{
			if (err.first->GetStatusCode() == web::http::status_codes::Forbidden)
			{
				has403 = true;
				if (!has403Description && error403.empty())
					error403 = err.first->GetFullErrorMessage();
				else
					has403Description = false;
			}
			CString str;
			str.Format(err.second.c_str(), err.first->GetFullErrorMessage().c_str());
			errors.push_back((LPCTSTR)str);

			if (!p_NoPopupSpecificReason.empty() && err.first->GetReason() == p_NoPopupSpecificReason)
				dontPopError = true;
		}
	}

	if (!errors.empty())
	{
		if (!p_Row->IsChangesSaved() && !p_Row->IsError()) // Keep existing saved or error status that may be the result of apply.
		{
			SapioError sapioError(has403 ? (has403Description ? error403 : _YTEXT("")) : errors.front(), has403 ? web::http::status_codes::Forbidden : 0);
			if (!dontPopError)
				p_GridUpdater.OnLoadingError(p_Row, sapioError);
			p_Row->AddField(errors, p_Row->GetBackend()->GetStatusColumn(), p_Row->GetField(p_Row->GetBackend()->GetStatusColumn()).GetIcon());
		}
		else if (has403)
		{
			if (has403Description)
				p_GridUpdater.Set403LoadingError(error403);
			else
				p_GridUpdater.Set403LoadingError();
		}
	}
	else
	{
		p_Row->SetEditModified(false);
	}
}

const wstring GridUtil::g_AutoNameClassMembers				= _YTEXT("ClassMembers");
const wstring GridUtil::g_AutoNameSchools					= _YTEXT("Schools");
const wstring GridUtil::g_AutoNameEduUsers					= _YTEXT("EduUsers");

const wstring GridUtil::g_AutoNameSiteRoles                 = _YTEXT("SiteRoles");
const wstring GridUtil::g_AutoNameSpUsers                   = _YTEXT("SpUsers");
const wstring GridUtil::g_AutoNameDriveItems				= _YTEXT("DriveItems");
const wstring GridUtil::g_AutoNameMessages					= _YTEXT("Messages");
#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
const wstring GridUtil::g_AutoNameManagerHierarchy			= _YTEXT("ManagerHierarchy");
#else
const wstring GridUtil::g_AutoNameManagerHierarchy			= _YTEXT("");
#endif
const wstring GridUtil::g_AutoNameChannels                  = _YTEXT("Channels");
const wstring GridUtil::g_AutoNameContacts					= _YTEXT("Contacts");
const wstring GridUtil::g_AutoNameDirectoryRoles            = _YTEXT("DirectoryRoles");
const wstring GridUtil::g_AutoNameSpGroups                  = _YTEXT("SpGroups");
const wstring GridUtil::g_AutoNameMailFolders				= _YTEXT("MailFolders");
const wstring GridUtil::g_AutoNameMessageRules				= _YTEXT("MessageRules");
const wstring GridUtil::g_AutoNameEvents					= _YTEXT("Events");
const wstring GridUtil::g_AutoNameUserGroups				= _YTEXT("UserGroups");
const wstring GridUtil::g_AutoNameGroups					= _YTEXT("Groups");
const wstring GridUtil::g_AutoNameGroupsRecycleBin			= _YTEXT("GroupsRecycleBin");
const wstring GridUtil::g_AutoNameGroupsHierarchy			= _YTEXT("GroupsHierarchy");
const wstring GridUtil::g_AutoNameGroupsOwnersHierarchy		= _YTEXT("GroupsOwnersHierarchy");
const wstring GridUtil::g_AutoNameGroupsAuthorsHierarchy	= _YTEXT("GroupsAuthorsHierarchy");
const wstring GridUtil::g_AutoNameConversations				= _YTEXT("ConversationsGrid");
const wstring GridUtil::g_AutoNameSites						= _YTEXT("Sites");
const wstring GridUtil::g_AutoNameSitePermissions			= _YTEXT("SitePermissions");
const wstring GridUtil::g_AutoNameLists						= _YTEXT("Lists");
const wstring GridUtil::g_AutoNameListsContent				= _YTEXT("ListsContent");
const wstring GridUtil::g_AutoNameLicenses					= _YTEXT("Licenses");
const wstring GridUtil::g_AutoNameLicensesSummary			= _YTEXT("LicensesSummary");
const wstring GridUtil::g_AutoNameLicensesDeletedUsers		= _YTEXT("LicensesDeletedUsers");
const wstring GridUtil::g_AutoNamePosts						= _YTEXT("Posts");
const wstring GridUtil::g_AutoNameColumns					= _YTEXT("Columns");
const wstring GridUtil::g_AutoNameUsers						= _YTEXT("Users");
const wstring GridUtil::g_AutoNameUsersRecycleBin			= _YTEXT("UsersRecycleBin");
const wstring GridUtil::g_AutoNameChannelMessages			= _YTEXT("ChannelMessages");
const wstring GridUtil::g_AutoNameSignIns					= _YTEXT("SignIns");
const wstring GridUtil::g_AutoNameDirectoryAudits			= _YTEXT("DirectoryAudits");
const wstring GridUtil::g_AutoNamePrivateChannelMembers		= _YTEXT("PrivateChannelMembers");
const wstring GridUtil::g_AutoNameApplications				= _YTEXT("Applications");
const wstring GridUtil::g_AutoNameMailboxPermissions		= _YTEXT("MailboxPermissions");
const wstring GridUtil::g_AutoNameOnPremiseUsers			= _YTEXT("OnPremiseUsers");
const wstring GridUtil::g_AutoNameOnPremiseGroups			= _YTEXT("OnPremiseGroups");
const wstring GridUtil::g_AutoNameUsageReports				= _YTEXT("UsageReports");

#define PRE_COMPUTED_USAGE_REPORT_MODULE_NAMES 0 // If precomputed, it keeps a huge string map in memory (more than 16000 elements), but is faster when called (except the first call)
wstring GridUtil::ModuleName(const wstring& p_GridAutomationName)
{
	if (p_GridAutomationName.empty())
		return p_GridAutomationName;

	static std::map<wstring, wstring> moduleNames = []()
	{
		std::map<wstring, wstring> moduleNamesMap
		{
			{ GridUtil::g_AutoNameApplications			, _T("Applications") },
			{ GridUtil::g_AutoNameChannelMessages		, YtriaTranslate::Do(ModuleTeamChannelMessages_showMessages_1, _YLOC("Team Channel Messages")).c_str() },
			{ GridUtil::g_AutoNameChannels				, YtriaTranslate::Do(ModuleChannel_showChannels_1, _YLOC("Team Channels")).c_str() },
			{ GridUtil::g_AutoNameClassMembers			, _T("Class Members") },
			{ GridUtil::g_AutoNameColumns				, YtriaTranslate::Do(ModuleColumns_showColumns_1, _YLOC("SharePoint Site List Columns")).c_str() },
			{ GridUtil::g_AutoNameContacts				, YtriaTranslate::Do(ModuleContact_showContacts_1, _YLOC("Contacts")).c_str() },
			{ GridUtil::g_AutoNameConversations			, YtriaTranslate::Do(ModuleConversation_showConversations_1, _YLOC("Group Conversations")).c_str() },
			{ GridUtil::g_AutoNameDirectoryAudits		, _T("Audit Logs") },
			{ GridUtil::g_AutoNameDirectoryRoles		, YtriaTranslate::Do(ModuleDirectoryRoles_showDirectoryRoles_1, _YLOC("Directory Roles")).c_str() },
			{ GridUtil::g_AutoNameDriveItems			, YtriaTranslate::Do(ModuleDrive_showDriveItems_1, _YLOC("OneDrive Files")).c_str() }, // for groups: YtriaTranslate::Do(ModuleDrive_showDriveItems_3, _YLOC("Files")).c_str()
			{ GridUtil::g_AutoNameEduUsers				, _T("Education Users") },
			{ GridUtil::g_AutoNameEvents				, YtriaTranslate::Do(ModuleEvent_showEvents_1, _YLOC("Events")).c_str() },
			{ GridUtil::g_AutoNameGroups				, YtriaTranslate::Do(ModuleGroup_show_1, _YLOC("Groups")).c_str() },
			{ GridUtil::g_AutoNameGroupsAuthorsHierarchy, YtriaTranslate::Do(ModuleGroup_showItems_2, _YLOC("Group Delivery Management")).c_str() },
			{ GridUtil::g_AutoNameGroupsHierarchy		, YtriaTranslate::Do(ModuleGroup_showItems_1, _YLOC("Group Members")).c_str() },
			{ GridUtil::g_AutoNameGroupsOwnersHierarchy	, YtriaTranslate::Do(ModuleGroup_showItems_3, _YLOC("Group Owners")).c_str() },
			{ GridUtil::g_AutoNameGroupsRecycleBin		, YtriaTranslate::Do(ModuleGroup_showRecycleBin_1, _YLOC("Groups: Recycle Bin")).c_str() },
			{ GridUtil::g_AutoNameLicenses				, YtriaTranslate::Do(ModuleLicense_showLicenses_1, _YLOC("Licenses and Service Plans")).c_str() },
			{ GridUtil::g_AutoNameLicensesDeletedUsers	, YtriaTranslate::Do(ModuleLicense_showLicenses_3, _YLOC("Deleted Users Licenses")).c_str() },
			{ GridUtil::g_AutoNameLicensesSummary		, YtriaTranslate::Do(ModuleLicense_showLicenses_2, _YLOC("Tenant Licenses and Service Plans")).c_str() },
			{ GridUtil::g_AutoNameLists					, YtriaTranslate::Do(ModuleLists_showLists_1, _YLOC("SharePoint Site Lists")).c_str() },
			{ GridUtil::g_AutoNameListsContent			, YtriaTranslate::Do(ModuleListsItems_showListsItems_1, _YLOC("SharePoint Site List Items")).c_str() },
			{ GridUtil::g_AutoNameMailboxPermissions	, _T("Mailbox Permissions") },
#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
			{ GridUtil::g_AutoNameManagerHierarchy		, YtriaTranslate::Do(ModuleUser_showManagerHierarchy_1, _YLOC("Management Hierarchy")).c_str() },
#endif
			{ GridUtil::g_AutoNameMessageRules			, YtriaTranslate::Do(ModuleMessageRules_showMessageRules_1, _YLOC("Message Rules")).c_str() },
			{ GridUtil::g_AutoNameMessages				, YtriaTranslate::Do(ModuleMessage_showMessages_1, _YLOC("Messages")).c_str() },
			{ GridUtil::g_AutoNameOnPremiseGroups		, _T("On-Premises Groups") },
			{ GridUtil::g_AutoNameOnPremiseUsers		, _T("On-Premises Users") },
			{ GridUtil::g_AutoNamePosts					, YtriaTranslate::Do(ModulePost_showPosts_1, _YLOC("Group Conversation Posts")).c_str() },
			{ GridUtil::g_AutoNamePrivateChannelMembers	, _T("Private Channel Members") },
			{ GridUtil::g_AutoNameSchools				, _T("Schools") },
			{ GridUtil::g_AutoNameSignIns				, YtriaTranslate::Do(CommandDispatcher_getModuleName_28, _YLOC("Sign-ins")).c_str() },
			{ GridUtil::g_AutoNameSiteRoles				, YtriaTranslate::Do(CommandDispatcher_getModuleName_26, _YLOC("Site Roles")).c_str() },
			{ GridUtil::g_AutoNameSites					, YtriaTranslate::Do(ModuleSites_showRootSite_1, _YLOC("SharePoint Sites")).c_str() },
			{ GridUtil::g_AutoNameSitePermissions		, YtriaTranslate::Do(ModuleSitePermissions_showSitePermissions_1, _YLOC("Site Permissions")).c_str() },
			{ GridUtil::g_AutoNameSpGroups				, YtriaTranslate::Do(ModuleSpGroups_showSpGroups_1, _YLOC("Site Groups")).c_str() },
			{ GridUtil::g_AutoNameSpUsers				, YtriaTranslate::Do(ModuleSpUsers_showSpUsers_1, _YLOC("Site Users")).c_str() },
			{ GridUtil::g_AutoNameUsageReports			, _T("Usage Reports") },
			{ GridUtil::g_AutoNameUserGroups			, YtriaTranslate::Do(ModuleUser_showParentGroups_1, _YLOC("Group Memberships")).c_str() },
			{ GridUtil::g_AutoNameUsers					, YtriaTranslate::Do(ModuleUser_show_1, _YLOC("Users")).c_str() },
			{ GridUtil::g_AutoNameUsersRecycleBin		, YtriaTranslate::Do(ModuleUser_showRecycleBin_1, _YLOC("Users: Recycle Bin")).c_str() },
		};
#if PRE_COMPUTED_USAGE_REPORT_MODULE_NAMES
		auto processCombination = [&moduleNamesMap](const std::vector<O365ReportID>& p_Ids)
		{
			O365ReportCriteria criteria(p_Ids, Str::g_EmptyString);
			moduleNamesMap[criteria.GetCombinedShortName()] = criteria.GetCombinedDisplayName();
		};

		// User reports
		{
			const std::vector<O365ReportID> userReportIds
			{
				O365ReportID::OneDriveActivityUserDetail,
				O365ReportID::OneDriveUsageAccountDetail,
				O365ReportID::TeamsDeviceUsageUserDetail,
				O365ReportID::TeamsUserActivityUserDetail,
				O365ReportID::EmailActivityUserDetail,
				O365ReportID::EmailAppUsageUserDetail,
				O365ReportID::MailboxUsageDetail,
				//O365ReportID::Office365GroupsActivityDetail,
				O365ReportID::Office365ActiveUserDetail,
				O365ReportID::Office365ActivationsUserDetail,
				O365ReportID::SharePointActivityUserDetail,
				//O365ReportID::SharePointSiteUsageDetail,
				O365ReportID::SkypeForBusinessActivityUserDetail,
				O365ReportID::SkypeForBusinessDeviceUsageUserDetail,
				O365ReportID::YammerActivityUserDetail,
				O365ReportID::YammerDeviceUsageUserDetail,
				//O365ReportID::YammerGroupsActivityDetail,
			};

			// https://www.tutorialspoint.com/cplusplus-program-to-generate-all-possible-combinations-out-of-a-b-c-d-e
			auto computeReportCombinations = [&processCombination](const std::vector<O365ReportID>& a, size_t requestedLength, size_t s, size_t currentLength, std::vector<bool>& check, size_t maxLength)
			{
				auto computeReportCombinations_recursive = [&processCombination](const std::vector<O365ReportID>& a, size_t requestedLength, size_t s, size_t currentLength, std::vector<bool>& check, size_t maxLength, const auto& self)->void
				{
					if (currentLength > requestedLength)
						return;

					if (currentLength == requestedLength)
					{
						std::vector<O365ReportID> ids;
						for (size_t i = 0; i < maxLength; ++i)
						{
							if (check[i])
								ids.push_back(a[i]);
						}
						processCombination(ids);
						return;
					}

					if (s == maxLength)
						return;

					check[s] = true;
					self(a, requestedLength, s + 1, currentLength + 1, check, maxLength, self);
					check[s] = false;
					self(a, requestedLength, s + 1, currentLength, check, maxLength, self);
				};

				computeReportCombinations_recursive(a, requestedLength, s, currentLength, check, maxLength, computeReportCombinations_recursive);				
			};

			std::vector<bool> check;
			check.resize(userReportIds.size(), false);
			for (size_t length = 1; length <= userReportIds.size(); ++length)
				computeReportCombinations(userReportIds, length, 0, 0, check, userReportIds.size());
		}

		// Other reports
		{
			const std::vector<O365ReportID> otherReportIds
			{
				O365ReportID::Office365GroupsActivityDetail,
				O365ReportID::SharePointSiteUsageDetail,
				O365ReportID::YammerGroupsActivityDetail,
			};

			for (auto id : otherReportIds)
				processCombination({ id });
		}
#endif
		return std::move(moduleNamesMap);
	}();

	auto it = moduleNames.find(p_GridAutomationName);
#if PRE_COMPUTED_USAGE_REPORT_MODULE_NAMES
	ASSERT(moduleNames.end() != it);
#endif
	if (moduleNames.end() != it)
		return it->second;

#if PRE_COMPUTED_USAGE_REPORT_MODULE_NAMES
#else
	const auto reportShortNames = Str::explodeIntoVector(p_GridAutomationName, wstring(_YTEXT("+")), false, true);
	std::vector<O365ReportID> reportIds;
	reportIds.reserve(reportShortNames.size());
	for (const auto& shortName : reportShortNames)
		reportIds.push_back(O365ReportCriteria::GetO365ReportIDForShortName(shortName));
	// Do not assert, as Activity Logs use 'fake' module names (as 'Session' or 'RBAC')
	//ASSERT(std::all_of(reportIds.begin(), reportIds.end(), [](auto& v) {return O365ReportID::Undefined < v && v < O365ReportID::Count; }));
	if (std::all_of(reportIds.begin(), reportIds.end(), [](auto& v) {return O365ReportID::Undefined < v && v < O365ReportID::Count; }))
	{
		O365ReportCriteria criteria(reportIds, Str::g_EmptyString);
		return criteria.GetCombinedDisplayName();
	}
#endif

	return p_GridAutomationName;
}

Command::ModuleTarget GridUtil::ModuleTarget(const wstring& p_GridAutomationName)
{
	static std::map<wstring, Command::ModuleTarget> moduleTargetsMap
	{
		{ GridUtil::g_AutoNameApplications			, Command::ModuleTarget::Applications },
		{ GridUtil::g_AutoNameChannelMessages		, Command::ModuleTarget::ChannelMessages },
		{ GridUtil::g_AutoNameChannels				, Command::ModuleTarget::Channel },
		{ GridUtil::g_AutoNameClassMembers			, Command::ModuleTarget::ClassMembers },
		{ GridUtil::g_AutoNameColumns				, Command::ModuleTarget::Columns },
		{ GridUtil::g_AutoNameContacts				, Command::ModuleTarget::Contact },
		{ GridUtil::g_AutoNameConversations			, Command::ModuleTarget::Conversation },
		{ GridUtil::g_AutoNameDirectoryAudits		, Command::ModuleTarget::DirectoryAudit },
		{ GridUtil::g_AutoNameDirectoryRoles		, Command::ModuleTarget::DirectoryRoles },
		{ GridUtil::g_AutoNameDriveItems			, Command::ModuleTarget::Drive },
		{ GridUtil::g_AutoNameEduUsers				, Command::ModuleTarget::EduUsers },
		{ GridUtil::g_AutoNameEvents				, Command::ModuleTarget::Event },
		{ GridUtil::g_AutoNameGroups				, Command::ModuleTarget::Group },
		{ GridUtil::g_AutoNameGroupsAuthorsHierarchy, Command::ModuleTarget::Group },
		{ GridUtil::g_AutoNameGroupsHierarchy		, Command::ModuleTarget::Group },
		{ GridUtil::g_AutoNameGroupsOwnersHierarchy	, Command::ModuleTarget::Group },
		{ GridUtil::g_AutoNameGroupsRecycleBin		, Command::ModuleTarget::Group },
		{ GridUtil::g_AutoNameLicenses				, Command::ModuleTarget::Licenses },
		{ GridUtil::g_AutoNameLicensesDeletedUsers	, Command::ModuleTarget::Licenses },
		{ GridUtil::g_AutoNameLicensesSummary		, Command::ModuleTarget::Licenses },
		{ GridUtil::g_AutoNameLists					, Command::ModuleTarget::Lists },
		{ GridUtil::g_AutoNameListsContent			, Command::ModuleTarget::ListsItem },
		{ GridUtil::g_AutoNameMailboxPermissions	, Command::ModuleTarget::MailboxPermissions },
#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
		{ GridUtil::g_AutoNameManagerHierarchy		, Command::ModuleTarget::User },
#endif
		{ GridUtil::g_AutoNameMessageRules			, Command::ModuleTarget::MessageRules },
		{ GridUtil::g_AutoNameMessages				, Command::ModuleTarget::Message },
		{ GridUtil::g_AutoNameOnPremiseGroups		, Command::ModuleTarget::OnPremiseGroups },
		{ GridUtil::g_AutoNameOnPremiseUsers		, Command::ModuleTarget::OnPremiseUsers },
		{ GridUtil::g_AutoNamePosts					, Command::ModuleTarget::Post },
		{ GridUtil::g_AutoNamePrivateChannelMembers	, Command::ModuleTarget::ChannelMembers },
		{ GridUtil::g_AutoNameSchools				, Command::ModuleTarget::Schools },
		{ GridUtil::g_AutoNameSignIns				, Command::ModuleTarget::SignIns },
		{ GridUtil::g_AutoNameSiteRoles				, Command::ModuleTarget::SiteRoles },
		{ GridUtil::g_AutoNameSites					, Command::ModuleTarget::Sites },
		{ GridUtil::g_AutoNameSitePermissions		, Command::ModuleTarget::SitePermissions },
		{ GridUtil::g_AutoNameSpGroups				, Command::ModuleTarget::SpGroup },
		{ GridUtil::g_AutoNameSpUsers				, Command::ModuleTarget::SpUser },
		{ GridUtil::g_AutoNameUserGroups			, Command::ModuleTarget::User },
		{ GridUtil::g_AutoNameUsers					, Command::ModuleTarget::User },
		{ GridUtil::g_AutoNameUsersRecycleBin		, Command::ModuleTarget::User },
	};

	auto it = moduleTargetsMap.find(p_GridAutomationName);
	if (moduleTargetsMap.end() != it)
		return it->second;

	const auto reportShortNames = Str::explodeIntoVector(p_GridAutomationName, wstring(_YTEXT("+")), false, true);
	std::vector<O365ReportID> reportIds;
	reportIds.reserve(reportShortNames.size());
	for (const auto& shortName : reportShortNames)
		reportIds.push_back(O365ReportCriteria::GetO365ReportIDForShortName(shortName));
	if (std::all_of(reportIds.begin(), reportIds.end(), [](auto& v) {return O365ReportID::Undefined < v&& v < O365ReportID::Count; }))
	{
		O365ReportCriteria criteria(reportIds, Str::g_EmptyString);
		if (p_GridAutomationName == criteria.GetCombinedShortName())
		{
			return Command::ModuleTarget::Reports;
		}
		else
		{
			ASSERT(false);
		}
	}
	else
	{
		ASSERT(false);
	}

	return Command::ModuleTarget::numModuleTargets; // Invalid target
}

vector<IconTypeSetup> GridUtil::g_IconSetup;

template<class RTTRRegisteredType> void GridUtil::setObjectType(int32_t p_IconResID)
{
	RTTRRegisteredType	dummyObject;
	IconTypeSetup		theTypeIconSetup;
	
	theTypeIconSetup.m_TypeID				= dummyObject.get_type().get_id();
	theTypeIconSetup.m_IconResourceIndex	= p_IconResID;
	if (dummyObject.get_type().get_metadata(MetadataKeys::TypeDisplayName).is_valid() && dummyObject.get_type().get_metadata(MetadataKeys::TypeDisplayName).is_type<wstring>())
		theTypeIconSetup.m_TypeDisplayName = dummyObject.get_type().get_metadata(MetadataKeys::TypeDisplayName).get_value<wstring>();
	else
		theTypeIconSetup.m_TypeDisplayName = wstring(_YTEXT("?"));

	ASSERT(BusinessObject().get_type().get_id() != theTypeIconSetup.m_TypeID);// bad RTTR registration
	if (BusinessObject().get_type().get_id() != theTypeIconSetup.m_TypeID)
		g_IconSetup.push_back(theTypeIconSetup);
}

void GridUtil::ClearStatics()
{
	g_IconSetup.clear();
}

// static
void GridUtil::Add365ObjectTypes(CacheGrid& p_Grid)
{
	if (g_IconSetup.empty())
	{
		// This order is important (displayed in hierarchy filters list, grid for instance).

		setObjectType<BusinessUser>(IDB_ICON_USER);
		setObjectType<BusinessUserDeleted>(IDB_ICON_USER_DELETED);
		setObjectType<BusinessUserGuest>(IDB_ICON_USER_2);
		setObjectType<BusinessUserGuestDeleted>(IDB_ICON_USER_2_DELETED);
		//setObjectType<HybridUser>(IDB_ICON_HYBRIDUSER);
		setObjectType<OnPremiseUser>(IDB_ICON_ONPREMISEUSER);

		setObjectType<BusinessOrgContact>(IDB_ICON_ORG_CONTACT);
		setObjectType<BusinessUserUnknown>(IDB_ICON_USER_3);
		setObjectType<BusinessUserUnknownDeleted>(IDB_ICON_USER_3_DELETED);
		setObjectType<BusinessGroup>(IDB_ICON_GROUP);
		setObjectType<BusinessGroupDeleted>(IDB_ICON_GROUP_DELETED);
		//setObjectType<HybridGroup>(IDB_ICON_HYBRIDGROUP);
		setObjectType<OnPremiseGroup>(IDB_ICON_ONPREMISEGROUP);
        setObjectType<BusinessDirectoryRoleTemplate>(IDB_ICON_DIRECTORYROLETEMPLATE);
		
		setObjectType<BusinessSite>(IDB_ICON_SITE);

		setObjectType<BusinessDriveItem>(IDB_ICON_DRIVEITEM);
		setObjectType<BusinessDriveItemNotebook>(IDB_ICON_DRIVEITEM_NOTEBOOK);
		setObjectType<BusinessDriveItemFolder>(IDB_ICON_DRIVEITEM_FOLDER);
		setObjectType<BusinessDrive>(IDB_ICON_DRIVE);

		setObjectType<BusinessMessage>(IDB_ICON_MESSAGE);
		setObjectType<BusinessMailFolder>(IDB_ICON_MAILFOLDER);
		setObjectType<BusinessMessageRule>(IDB_ICON_MESSAGERULE);
		setObjectType<BusinessMessageRuleComponent>(IDB_ICON_MESSAGERULE_COMPONENT);

		setObjectType<BusinessConversation>(IDB_ICON_CONVERSATION);
        setObjectType<BusinessConversationThread>(IDB_ICON_CONVERSATIONTHREAD);

		setObjectType<BusinessList>(IDB_ICON_LIST);

		setObjectType<BusinessContact>(IDB_ICON_CONTACT);
		
		setObjectType<BusinessEvent>(IDB_ICON_EVENT);
		
		setObjectType<BusinessListItem>(IDB_ICON_LISTITEM);	
		
		setObjectType<BusinessSubscribedSku>(IDB_ICON_BUSINESSSUBSCRIBEDSKU);
		setObjectType<ServicePlanInfo>(IDB_ICON_SERVICEPLANINFO); // FIXME: Do we want a BusinessObject here?

        setObjectType<BusinessPost>(IDB_ICON_POST);
        setObjectType<BusinessColumnDefinition>(IDB_ICON_COLUMNDEFINITION);
        setObjectType<BusinessChannel>(IDB_ICON_CHANNEL);
        setObjectType<BusinessGroupSetting>(IDB_ICON_COLUMNDEFINITION);
        setObjectType<BusinessGroupSettingTemplate>(IDB_ICON_COLUMNDEFINITION);

		setObjectType<BusinessAADUserConversationMember>(IDB_ICON_PRIVATECHANMEMBER);

		setObjectType<BusinessChatMessage>(IDB_ICON_CHANNELMESSAGE);
		setObjectType<BusinessChatMessageReply>(IDB_ICON_CHANNELREPLY);
        setObjectType<Ex::DelegateUser>(IDB_ICON_DELEGATEUSER);
        setObjectType<Sp::User>(IDB_ICON_SPUSER);
        setObjectType<Sp::Group>(IDB_ICON_SPGROUP);
        setObjectType<Sp::RoleDefinition>(IDB_ICON_SPROLE);
		setObjectType<SignIn>(IDB_ICON_SIGNIN);
		setObjectType<DirectoryAudit>(IDB_ICON_DIRECTORYAUDIT);
		setObjectType<EducationSchool>(IDB_ICON_EDUCATIONSCHOOL);
		setObjectType<EducationClass>(IDB_ICON_EDUCATIONCLASS);
		setObjectType<EducationUser>(IDB_ICON_EDUCATIONUSER);
		setObjectType<EducationStudent>(IDB_ICON_EDUCATIONSTUDENT);
		setObjectType<EducationTeacher>(IDB_ICON_EDUCATIONTEACHER);
		setObjectType<Application>(IDB_ICON_APPLICATION);
		setObjectType<MailboxPermissions>(IDB_ICON_MAILBOXPERMISSIONS);		
	}

	for (auto& iconSetup : g_IconSetup)
	{
		ASSERT(iconSetup.m_IconResourceIndex > NO_ICON);
		if (iconSetup.m_IconResourceIndex > NO_ICON)
		{
			if (!p_Grid.HasObjectTypeIcon(iconSetup.m_TypeID))
			{
				{
					auto icon = MFCUtil::getCExtCmdIconFromImageResourceID(iconSetup.m_IconResourceIndex);

					ASSERT(icon.GetSize().cx == 16 && icon.GetSize().cy == 16
						|| icon.GetSize().cx == HIDPI_XW(16) && icon.GetSize().cy == HIDPI_YH(16));
					iconSetup.m_IconGridIndex = p_Grid.AddImage(icon);
				}

				// Add grayed-out icon for canceled objects.
				{
					auto icon = MFCUtil::getCExtCmdIconFromImageResourceID(iconSetup.m_IconResourceIndex);

					// Convert to gray scale and lighten a bit (+25%)
					MFCUtil::ConvertToGrayscale(icon.m_bmpNormal, 1.25);

					ASSERT(icon.GetSize().cx == 16 && icon.GetSize().cy == 16
						|| icon.GetSize().cx == HIDPI_XW(16) && icon.GetSize().cy == HIDPI_YH(16));
					iconSetup.m_DisabledIconGridIndex = p_Grid.AddImage(icon);
				}

				p_Grid.AddObjectTypeIcon(iconSetup);
			}
		}
	}
}

bool GridUtil::IsBusinessContact(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessContact>(p_Row);
}

bool GridUtil::IsBusinessConversation(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessConversation>(p_Row);
}

bool GridUtil::IsBusinessConversationThread(const GridBackendRow* p_Row)
{
    return isBusinessType<BusinessConversationThread>(p_Row);
}

bool GridUtil::IsBusinessDrive(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessDrive>(p_Row);
}

bool GridUtil::IsBusinessDriveItem(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessDriveItem>(p_Row);
}

bool GridUtil::IsBusinessDriveItemFolder(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessDriveItemFolder>(p_Row);
}

bool GridUtil::IsBusinessDriveItemNotebook(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessDriveItemNotebook>(p_Row);
}

bool GridUtil::IsBusinessEvent(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessEvent>(p_Row);
}

bool GridUtil::IsBusinessGroup(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessGroup>(p_Row) || isBusinessType<BusinessGroupDeleted>(p_Row);
}

bool GridUtil::IsBusinessMailFolder(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessMailFolder>(p_Row);
}

bool GridUtil::IsBusinessMessage(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessMessage>(p_Row);
}

bool GridUtil::IsBusinessSite(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessSite>(p_Row);
}

bool GridUtil::IsBusinessList(const GridBackendRow* p_Row)
{
    return isBusinessType<BusinessList>(p_Row);
}

bool GridUtil::IsBusinessUser(const GridBackendRow* p_Row, bool p_TrueForGuest/* = true*/)
{
	return isBusinessType<BusinessUser>(p_Row) || isBusinessType<BusinessUserUnknown>(p_Row)
		|| isBusinessType<BusinessUserDeleted>(p_Row) || isBusinessType<BusinessUserUnknownDeleted>(p_Row)
		|| (p_TrueForGuest && (isBusinessType<BusinessUserGuest>(p_Row) || isBusinessType<BusinessUserGuestDeleted>(p_Row)))
		;
}

bool GridUtil::IsBusinessListItem(const GridBackendRow* p_Row)
{
    return isBusinessType<BusinessListItem>(p_Row);
}

bool GridUtil::IsBusinessPost(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessPost>(p_Row);
}

bool GridUtil::IsBusinessChannel(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessChannel>(p_Row);
}

bool GridUtil::IsBusinessChatMessageOrReply(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessChatMessage>(p_Row) || isBusinessType<BusinessChatMessageReply>(p_Row);
}

bool GridUtil::IsBusinessOrgContact(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessOrgContact>(p_Row);
}

bool GridUtil::IsBusinessAADUserConversationMember(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessAADUserConversationMember>(p_Row);
}

bool GridUtil::IsBusinessUserDeleted(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessUserDeleted>(p_Row)
		|| isBusinessType<BusinessUserUnknownDeleted>(p_Row)
		|| isBusinessType<BusinessUserGuestDeleted>(p_Row);
}

bool GridUtil::IsBusinessGroupDeleted(const GridBackendRow* p_Row)
{
	return isBusinessType<BusinessGroupDeleted>(p_Row);
}

//bool GridUtil::IsHybridUser(const GridBackendRow* p_Row)
//{
//	return isBusinessType<HybridUser>(p_Row);
//}

bool GridUtil::IsOnPremUser(const GridBackendRow* p_Row)
{
	return isBusinessType<OnPremiseUser>(p_Row);
}

//bool GridUtil::IsHybridGroup(const GridBackendRow* p_Row)
//{
//	return isBusinessType<HybridGroup>(p_Row);
//}

bool GridUtil::IsOnPremGroup(const GridBackendRow* p_Row)
{
	return isBusinessType<OnPremiseGroup>(p_Row);
}

GridUtil::AttachmentIcons GridUtil::LoadAttachmentIcons(CacheGrid& p_Grid)
{
	AttachmentIcons icons;

	icons.m_ReferenceAttachment = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_REFERENCE_ATTACHMENT));
	icons.m_ItemAttachment = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ITEM_ATTACHMENT));
	icons.m_FileAttachment = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_FILE_ATTACHMENT));

	return icons;
}

void GridUtil::SetAttachmentIcon(GridBackendField& p_Field, const AttachmentIcons& p_Icons)
{
    wstring typeStr = p_Field.GetValueStr();
    if (typeStr == Util::GetFileAttachmentStr())
        p_Field.SetIcon(p_Icons.m_FileAttachment);
    else if (typeStr == Util::GetItemAttachmentStr())
        p_Field.SetIcon(p_Icons.m_ItemAttachment);
    else if (typeStr == Util::GetReferenceAttachmentStr())
        p_Field.SetIcon(p_Icons.m_ReferenceAttachment);
}