#include "MessageListRequester.h"

#include "BusinessMessage.h"
#include "DateFieldCutOffCondition.h"
#include "IPropertySetBuilder.h"
#include "MsGraphHttpRequestLogger.h"
#include "MsGraphPaginator.h"
#include "MSGraphUtil.h"
#include "MultiObjectsPageRequestLogger.h"
#include "PaginatorUtil.h"
#include "RESTUtils.h"
#include "Sapio365Session.h"

MessageListRequester::MessageListRequester(IPropertySetBuilder& m_Properties, const wstring& p_UserId, const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: MessageListRequester(m_Properties, p_UserId, wstring(), p_Logger)
{
}

MessageListRequester::MessageListRequester(IPropertySetBuilder& p_Properties, const wstring& p_UserId, const wstring& p_FolderId, const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_UserId(p_UserId)
	, m_FolderId(p_FolderId)
	, m_KeepEmails(true)
	, m_KeepChats(false)
	, m_Properties(p_Properties)
	, m_Logger(p_Logger)
{
}

void MessageListRequester::SetCutOffDateTime(const wstring& p_CutOff)
{
	m_CutOffDateTime = p_CutOff;
}

void MessageListRequester::SetKeepEmails(bool p_Keep)
{
	m_KeepEmails = p_Keep;
}

void MessageListRequester::SetKeepChats(bool p_Keep)
{
	m_KeepChats = p_Keep;
}

void MessageListRequester::SetCustomFilter(const ODataFilter& p_CustomFilter)
{
	m_CustomFilter = p_CustomFilter;
}

TaskWrapper<void> MessageListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<DeserializerType>();

	web::uri_builder uri;
	uri.append_path(_YTEXT("users"));
	uri.append_path(m_UserId);
	if (!m_FolderId.empty())
	{
		uri.append_path(_YTEXT("mailFolders"));
		uri.append_path(m_FolderId);
		uri.append_path(_YTEXT("messages"));
	}
	else
	{
		uri.append_path(_YTEXT("messages"));
	}
	uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(m_Properties.GetStringPropertySet()));

	Rest::OrderBy(uri, _YTEXT("receivedDateTime desc"));

	ODataFilter oDataFilter;
	if (!m_CutOffDateTime.empty())
	{
		oDataFilter.GreaterThan(_YTEXT("receivedDateTime"), m_CutOffDateTime, false);
	}
	else if (!m_CustomFilter.HasFilterOn(_YTEXT("receivedDateTime")))
	{
		// FIXME: Microsoft restriction requires the orderBy fields to be also in filter, in the same order
		oDataFilter.GreaterThan(_YTEXT("receivedDateTime"), _YTEXT("1970-01-01"), false);
	}

	ASSERT(m_KeepChats || m_KeepEmails); // Kept nothing...?
	// Chat ids are real numbers
	// Message ids start with '<'
	if (!m_KeepChats)
	{
		if (!oDataFilter.IsEmpty())
			oDataFilter.And();
		oDataFilter.StartsWith(_YTEXT("internetMessageId"), _YTEXT("<"));
	}
	if (!m_KeepEmails)
	{
		if (!oDataFilter.IsEmpty())
			oDataFilter.And();
		oDataFilter.GreaterThan(_YTEXT("internetMessageId"), _YTEXT("0"), true);
	}

	if (!m_CustomFilter.IsEmpty())
	{
		if (!oDataFilter.IsEmpty())
			oDataFilter.And(m_CustomFilter);
		else
			oDataFilter = m_CustomFilter;
	}

	oDataFilter.ApplyTo(uri);

	web::http::http_headers headers;
	headers.add(_YTEXT("Prefer"), _YTEXT("outlook.allow-unsafe-html"));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	auto pageRequester = Util::CreateDefaultGraphPageRequester(m_Deserializer, httpLogger, p_TaskData.GetOriginator());
	pageRequester->SetHeaders(headers);
	m_Paginator.emplace(uri.to_uri(), pageRequester, m_Logger, 100);

	return m_Paginator->Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

vector<BusinessMessage>& MessageListRequester::GetData()
{
	return m_Deserializer->GetData();
}
