#pragma once

#include "IRequester.h"
#include "BusinessSite.h"
#include "IRequestLogger.h"

class GroupRootSiteRequester : public IRequester
{
public:
	GroupRootSiteRequester(const wstring& p_GroupId, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const BusinessSite& GetData() const;
	BusinessSite& GetData();

private:
	wstring m_GroupId;
	std::shared_ptr<SiteDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
};

