#include "OrganizationDeserializer.h"

#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "AssignedPlanDeserializer.h"
#include "ProvisionedPlanDeserializer.h"
#include "VerifiedDomainDeserializer.h"

void OrganizationDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        ListDeserializer<AssignedPlan, AssignedPlanDeserializer> assPlanDeserializer;
        if (JsonSerializeUtil::DeserializeAny(assPlanDeserializer, _YTEXT("assignedPlans"), p_Object))
            m_Data.AssignedPlans = std::move(assPlanDeserializer.GetData());
    }

	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);

    JsonSerializeUtil::DeserializeString(_YTEXT("city"), m_Data.City, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("companyLastDirSyncTime"), m_Data.CompanyLastDirSyncTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("country"), m_Data.Country, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("countryLetterCode"), m_Data.CountryLetterCode, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("deletionTimeStamp"), m_Data.DeletionTimestamp, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("dirSyncEnabled"), m_Data.DirSyncEnabled, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.DisplayName, p_Object);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("marketingNotificationEmails"), p_Object))
            m_Data.MarketingNotificationEmails = std::move(lsd.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("objectType"), m_Data.ObjectType, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("onPremisesSyncEnabled"), m_Data.OnPremisesSyncEnabled, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("postalCode"), m_Data.PostalCode, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("preferredLanguage"), m_Data.PreferredLanguage, p_Object);

    {
        ListDeserializer<ProvisionedPlan, ProvisionedPlanDeserializer> provPlanDeserializer;
        if (JsonSerializeUtil::DeserializeAny(provPlanDeserializer, _YTEXT("provisionedPlans"), p_Object))
            m_Data.ProvisionedPlans = std::move(provPlanDeserializer.GetData());
    }

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("securityComplianceNotificationMails"), p_Object))
            m_Data.SecurityComplianceNotificationMails = std::move(lsd.GetData());
    }

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("securityComplianceNotificationPhones"), p_Object))
            m_Data.SecurityComplianceNotificationPhones = std::move(lsd.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("state"), m_Data.State, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("street"), m_Data.Street, p_Object);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("technicalNotificationMails"), p_Object))
            m_Data.TechnicalNotificationMails = std::move(lsd.GetData());
    }
    
	JsonSerializeUtil::DeserializeString(_YTEXT("telephoneNumber"), m_Data.TelephoneNumber, p_Object);

    {
        ListDeserializer<VerifiedDomain, VerifiedDomainDeserializer> verDomDeserializer;
        if (JsonSerializeUtil::DeserializeAny(verDomDeserializer, _YTEXT("verifiedDomains"), p_Object))
            m_Data.VerifiedDomains = std::move(verDomDeserializer.GetData());
    }
}
