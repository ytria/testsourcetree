#include "DriveItemImporterFolder.h"

#include "DlgLoading.h"
#include "DriveItemImporterFile.h"
#include "GridDriveItems.h"
#include "FileUtil.h"

DriveItemImporterFolder::DriveItemImporterFolder(GridDriveItems& p_Grid, Mode p_Mode)
	: DriveItemImporter(p_Grid)
	, m_Mode(p_Mode)
{

}

void DriveItemImporterFolder::Run(const wstring& p_FolderPath, const std::set<GridBackendRow*>& p_ParentRows, std::shared_ptr<DlgLoading>& p_Dlg/* = std::shared_ptr<DlgLoading>()*/)
{
	if (p_Dlg)
		p_Dlg->SetText(Str::Replace(p_FolderPath, _YTEXT("\\"), _YTEXT("\\\\")));

	auto folderName = FileUtil::FileGetFileName(p_FolderPath);
	if (Str::endsWith(folderName, _YTEXT("\\")))
		folderName.pop_back();

	std::set<GridBackendRow*> newFolderRows;
	const std::set<GridBackendRow*>* parentRowsForFolderContent = m_Mode == Mode::IMPORT_FOLDER_AND_CONTENT ? &newFolderRows : &p_ParentRows;

	if (m_Mode == Mode::IMPORT_FOLDER_AND_CONTENT)
	{
		std::map<GridBackendRow*, GridBackendRow*> existingFolderRows;
		for (auto& parentRow : p_ParentRows) // FIXME: Also check row type
			existingFolderRows[parentRow] = m_Grid.GetChildRow(parentRow, folderName, m_Grid.m_ColName->GetID());

		std::map<GridBackendRow*, wstring> newFolderNames;

		for (auto& item : existingFolderRows)
		{
			if (item.second)
			{
				if (m_Replacing.is_initialized())
				{
					if (!*m_Replacing)
					{
						newFolderNames[item.first] = generateUniqueFolderName(folderName, item.first);
						item.second = nullptr;
					}
				}
				else
				{
					ASSERT(nullptr != item.first || m_Grid.m_MyDataMeHandler);
					const bool addingToRoot = nullptr == item.first || item.first->GetHierarchyLevel() == 0;

					const wstring message = addingToRoot
						? _YFORMAT(L"The destination root folder already contains a folder named \"%s\". Would you like to merge content or rename the new one?", folderName.c_str())
						: _YFORMAT(L"The destination folder \"%s\" already contains a folder named \"%s\". Would you like to merge content or rename the new one?", item.first->GetField(m_Grid.m_ColName).GetValueStr(), folderName.c_str());

					YCodeJockMessageBox dlg(&m_Grid,
						DlgMessageBox::eIcon_Question,
						_T("Name Conflict"),
						message,
						_YTEXT(""),//_YTEXT("If you decide to 'Replace', the whole content (files and sub-folders) will be replaced."),
						{ { IDOK, _T("Merge") },{ IDCANCEL, _T("Rename") } },
						IDCANCEL);
					dlg.SetVerificationText(_T("Do this for all upcoming conflicts."));

					if (IDOK == dlg.DoModal())
					{
						if (dlg.IsVerificiationChecked())
							m_Replacing = true;
					}
					else
					{
						newFolderNames[item.first] = generateUniqueFolderName(folderName, item.first);
						item.second = nullptr;
						if (dlg.IsVerificiationChecked())
							m_Replacing = false;
					}
				}
			}
		}

		BusinessDriveItemFolder newFolder;
		for (auto& parentRow : p_ParentRows)
		{
			auto it = existingFolderRows.find(parentRow);
			auto existingRowItem = it != existingFolderRows.end() ? *it : std::map<GridBackendRow*, GridBackendRow*>::value_type{};
			if (existingRowItem.second)
			{
				existingRowItem.second->SetFlag(GridDriveItems::g_ReplaceContentFlag);
				existingRowItem.second->SetSelected(true);
			}
			else
			{
				auto& name = newFolderNames[existingRowItem.first];

				newFolder.SetID(m_Grid.GetNextTemporaryCreatedObjectID());
				newFolder.SetName(PooledString(name.empty() ? folderName : name));

				if (nullptr == parentRow)
				{
					ASSERT(m_Grid.m_MyDataMeHandler);
					if (m_Grid.m_MyDataMeHandler)
						newFolder.SetDriveId(m_Grid.m_MyDataMeHandler->GetField(m_Grid.m_ColMetaDriveId).GetValueStr());
				}
				else
				{
					newFolder.SetDriveId(parentRow->GetField(m_Grid.m_ColMetaDriveId).GetValueStr());
				}

				existingRowItem.second = m_Grid.fillRow(newFolder, parentRow, false, nullptr);
				existingRowItem.second->SetSelected(true);

				row_pk_t pk;
				m_Grid.GetRowPK(existingRowItem.second, pk);
				m_Grid.GetModifications().Add(std::make_unique<CreatedObjectModification>(m_Grid, pk));
			}

			newFolderRows.insert(existingRowItem.second);
		}
	}
	else if (m_Mode == Mode::SYNC)
	{
		m_Replacing = true;
	}

	auto files = FileUtil::ListFiles(p_FolderPath, _YTEXT(""));
	if (!files.empty())
	{
		DriveItemImporterFile importer(m_Grid);
		importer.m_Replacing = m_Replacing;
		for (auto filePath : files)
			importer.Run(filePath, *parentRowsForFolderContent, p_Dlg);
	}

	auto folders = FileUtil::ListSubFolders(p_FolderPath);
	if (!folders.empty())
	{
		DriveItemImporterFolder importer(m_Grid, (m_Mode == Mode::SYNC || m_Mode == Mode::SUB_FOLDER_SYNC) ? Mode::SUB_FOLDER_SYNC : Mode::IMPORT_FOLDER_AND_CONTENT);
		importer.m_Replacing = m_Replacing;
		for (auto folderPath : folders)
			importer.Run(folderPath, *parentRowsForFolderContent, p_Dlg);
	}

	// Remove files/folders that haven't been updated.
	if (m_Mode == Mode::SYNC || m_Mode == Mode::SUB_FOLDER_SYNC)
	{
		row_pk_t rowPk;
		for (auto& folderRow : *parentRowsForFolderContent)
		{
			auto children = m_Grid.GetChildRows(folderRow);
			for (auto child : children)
			{
				if (GridUtil::IsBusinessDriveItem(child) && !child->IsCreated() && !child->IsModified())
				{
					m_Grid.GetRowPK(child, rowPk);
					m_Grid.GetModifications().Add(std::make_unique<DeletedObjectModification>(m_Grid, rowPk));
					child->SetSelected(true);
				}
				else if ((GridUtil::IsBusinessDriveItemFolder(child) || GridUtil::IsBusinessDriveItemNotebook(child)) && !child->IsCreated() && !child->HasFlag(GridDriveItems::g_ReplaceContentFlag))
				{
					m_Grid.GetRowPK(child, rowPk);
					child->SetSelected(true); // DeletedFolderModification will select children if parentRow is selected
					m_Grid.GetModifications().Add(std::make_unique<DeletedFolderModification>(m_Grid, rowPk));
				}
			}
		}
	}
}

wstring DriveItemImporterFolder::generateUniqueFolderName(const wstring& desiredFolderName, GridBackendRow* p_ParentRow)
{
	wstring folderName = desiredFolderName;

	GridBackendRow* existingRow = nullptr;
	int renameIndex = 0;
	do
	{
		if (nullptr != existingRow)
			folderName = _YTEXTFORMAT(L"%s (%s)", desiredFolderName.c_str(), Str::getStringFromNumber(++renameIndex).c_str());

		existingRow = m_Grid.GetChildRow(p_ParentRow, folderName, m_Grid.m_ColName->GetID());
	} while (nullptr != existingRow);

	return folderName;
}
