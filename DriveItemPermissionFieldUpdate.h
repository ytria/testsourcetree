#pragma once

#include "SubItemFieldUpdate.h"

#include "GridDriveItems.h"

class DriveItemPermissionFieldUpdate : public SubItemFieldUpdate
{
public:
    DriveItemPermissionFieldUpdate(const SubItemFieldValue& p_OldValue, const SubItemFieldValue& p_NewValue, UINT p_ColumnKey, GridDriveItems& p_Grid, const GridDriveItems::DriveItemPermissionInfo& p_Info);

    virtual bool IsCorrespondingRow(GridBackendRow* p_Row) const override;
    virtual bool ShouldCreateFormatter(GridBackendRow* p_Row, size_t p_Index) const override;

private:
    GridDriveItems& m_GridDriveItems;
    GridDriveItems::DriveItemPermissionInfo m_PermissionInfo;
};

