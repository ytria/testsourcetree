#pragma once

#include "CacheGrid.h"

#include "SessionStatus.h"
#include "PersistentSession.h"

class BackstagePanelSessions;

class GridSessionsBackstage : public CacheGrid
{
public:
    GridSessionsBackstage(BackstagePanelSessions& p_PanelSessions);

    virtual void customizeGrid() override;
    virtual void OnCustomDoubleClick();

    void Load(bool p_KeepSelection);

    void BuildView(const vector<PersistentSession>& p_Sessions);

    wstring GetSessionTypeText(const PersistentSession& p_Session);
    int GetSessionTypeIcon(const PersistentSession& p_Session);
    int GetSessionStatusIcon(const PersistentSession& p_Session);

	bool HasNonActiveSelectedSession(bool p_OnlyOne);
    bool HasOneUltraAdminSelectedSession();
    bool HasAnyUltraAdminSelectedSession();
	bool AllSelectedRowsAreAlreadyFavorite();
	bool HasNonLoggedOutSelectedSession(bool p_OnlyOne) const;
    bool HasInactiveElevatedSessionSelected() const;
    bool IsMainFrameSessionSelected() const;

	const PersistentSession* GetSelectedSession() const;
    const PersistentSession* GetSelectedSession(GridBackendRow& p_Row) const;

	bool RowStatusIs(GridBackendRow* p_Row, SessionStatus p_Status) const;
    bool IsMainFrameSession(GridBackendRow* p_Row) const;

	GridBackendColumn* GetEmailOrAppIdCol() const;
	GridBackendColumn* GetSessionTypeTechnicalCol() const;
	GridBackendColumn* GetColumnRoleID() const;
    GridBackendColumn* GetColumnRoleName() const;

protected:
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0);
	virtual void InitializeCommands();

	DECLARE_MESSAGE_MAP()

	afx_msg void OnClearSQL();
	afx_msg void OnUpdateClearSQL(CCmdUI* pCmdUI);

	afx_msg void OnUseDeltaUsers();
	afx_msg void OnUpdateUseDeltaUsers(CCmdUI* pCmdUI);

	afx_msg void OnUseDeltaGroups();
	afx_msg void OnUpdateUseDeltaGroups(CCmdUI* pCmdUI);

	afx_msg void OnEditOnPremSettings();
	afx_msg void OnUpdateEditOnPremSettings(CCmdUI* pCmdUI);
    

    static SessionIdentifier GetSessionIdentifier(const PersistentSession& p_Session);

private:
    bool AreSupportFeaturesEnabled() const;

	GridBackendColumn* m_ColFavorite;
    GridBackendColumn* m_ColSessionDisplayName;
    GridBackendColumn* m_ColTenantNameDisplay;
	GridBackendColumn* m_ColTenantNamePrimary;
    GridBackendColumn* m_ColLastUsedOn;
    GridBackendColumn* m_ColCreatedOn;
    GridBackendColumn* m_ColAccessCount;
    GridBackendColumn* m_ColLoginStatus;
    GridBackendColumn* m_ColType;
    GridBackendColumn* m_ColEmailOrAppId;
    GridBackendColumn* m_ColTypeTechnical;
	GridBackendColumn* m_ColRoleIDTechnical;
	GridBackendColumn* m_ColRoleName;
	GridBackendColumn* m_ColUseDeltaUsers;
	GridBackendColumn* m_ColUseDeltaGroups;
	GridBackendColumn* m_ColSessionAppID; //Ultra Admin only
    GridBackendColumn* m_ColOnPremEnabled;

    BackstagePanelSessions& m_PanelSessions;

    int m_FavoriteIconIndex;
    int m_StandardSessionIconIndex;
    int m_AdvancedSessionIconIndex;
    int m_UltraAdminSessionIconIndex;
    int m_RoleSessionIconIndex;
	int m_RoleEnforcedSessionIconIndex;
	int m_ElevatedSessionIconIndex;
	int m_PartnerAdvancedSessionIconIndex;
	int m_PartnerElevatedSessionIconIndex;
    int m_SessionStatusCachedIndex;
    int m_SessionStatusCurrentIndex;
    int m_SessionStatusInactiveIndex;
    int m_SessionStatusLockedIndex;
    int m_SessionStatusStandbyIndex;

	vector<PersistentSession> m_Sessions;
    boost::YOpt<SessionIdentifier> m_MainFrameSession;

	static wstring g_Favorite;
	static void InitStatics();    
};
