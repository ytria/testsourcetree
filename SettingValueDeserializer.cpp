#include "SettingValueDeserializer.h"

#include "JsonSerializeUtil.h"

void SettingValueDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.Name, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("value"), m_Data.Value, p_Object);
}
