#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Usage.h"

namespace Azure
{
	class UsageDeserializer : public JsonObjectDeserializer, public Encapsulate<Azure::Usage>
	{
	public:
		void DeserializeObject(const web::json::object& p_Object) override;
	};
}

