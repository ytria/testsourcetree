#include "FrameTeamChannelMessages.h"

#include "AutomationNames.h"

IMPLEMENT_DYNAMIC(FrameTeamChannelMessages, GridFrameBase)

O365Grid& FrameTeamChannelMessages::GetGrid()
{
    return m_Grid;
}

void FrameTeamChannelMessages::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.GetSubIds() = m_Grid.GetModuleCriteriaFromTopRows(m_Grid.GetBackendRows()).m_SubIDs;
    info.SetFrame(this);
    info.SetOrigin(Origin::Channel);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::ChannelMessages, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameTeamChannelMessages::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::ChannelMessages, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

//void FrameTeamChannelMessages::ApplySpecific(bool p_Selected)
//{
//    m_Grid.ApplyDeleteAttachments(p_Selected);
//}
//
//void FrameTeamChannelMessages::ApplyAllSpecific()
//{
//    m_Grid.ApplyDeleteAttachments(false);
//}
//
//void FrameTeamChannelMessages::ApplySelectedSpecific()
//{
//    m_Grid.ApplyDeleteAttachments(true);
//}

void FrameTeamChannelMessages::ShowMessages(const O365DataMap<BusinessGroup, std::map<BusinessChannel, vector<BusinessChatMessage>>>& p_Messages, bool p_Refresh, bool p_FullPurge)
{
    if (CompliesWithDataLoadPolicy())
        m_Grid.BuildView(p_Messages, p_Refresh, p_FullPurge);
}

void FrameTeamChannelMessages::HiddenViaHistory()
{
	m_Grid.HideMessageViewer();
}

//void FrameTeamChannelMessages::ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments)
//{
//    m_Grid.ViewAttachments(p_BusinessAttachments, true, true);
//}
//
//void FrameTeamChannelMessages::ShowItemAttachment(const BusinessItemAttachment& p_Attachment)
//{
//    m_Grid.ShowItemAttachment(p_Attachment);
//}

bool FrameTeamChannelMessages::HasApplyButton()
{
    return true;
}

const map<PooledString, PooledString>& FrameTeamChannelMessages::GetChannelNames() const
{
	return m_ChannelNames;
}

void FrameTeamChannelMessages::SetChannelNames(const map<PooledString, PooledString>& p_ChannelNames)
{
	m_ChannelNames = p_ChannelNames;
}

void FrameTeamChannelMessages::createGrid()
{
    m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FrameTeamChannelMessages::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(tab);
	CreateRefreshGroup(tab);
	//CreateApplyGroup(tab);

	auto groupPreview = tab.AddGroup(YtriaTranslate::Do(FrameTeamChannelMessages_customizeActionsRibbonTab_1, _YLOC("Preview")).c_str());
	ASSERT(nullptr != groupPreview);
	if (nullptr != groupPreview)
	{
		auto ctrl = groupPreview->Add(xtpControlButton, ID_GRIDTEAMCHANNELMESSAGES_SHOWBODY);
		setImage({ ID_GRIDTEAMCHANNELMESSAGES_SHOWBODY }, IDB_MESSAGE_CONTENT, xtpImageNormal);
		setImage({ ID_GRIDTEAMCHANNELMESSAGES_SHOWBODY }, IDB_MESSAGE_CONTENT_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	/*auto group = tab.AddGroup(_TLOC("Attachments");
    ASSERT(nullptr != group);
    if (nullptr != group)
    {
		setGroupImage(*group, 0);

		{
			auto ctrl = group->Add(xtpControlButton, ID_POSTSGRID_SHOWATTACHMENTS);
			setImage({ ID_POSTSGRID_SHOWATTACHMENTS }, IDB_MESSAGES_SHOWATTACHMENTS, xtpImageNormal);
			setImage({ ID_POSTSGRID_SHOWATTACHMENTS }, IDB_MESSAGES_SHOWATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_POSTSGRID_TOGGLEINLINEATTACHMENTS);
			GridFrameBase::setImage({ ID_POSTSGRID_TOGGLEINLINEATTACHMENTS }, IDB_POSTS_TOGGLEINLINEATTACHMENTS, xtpImageNormal);
			GridFrameBase::setImage({ ID_POSTSGRID_TOGGLEINLINEATTACHMENTS }, IDB_POSTS_TOGGLEINLINEATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_POSTSGRID_DOWNLOADATTACHMENTS);
			setImage({ ID_POSTSGRID_DOWNLOADATTACHMENTS }, IDB_MESSAGES_DOWNLOADATTACHMENTS, xtpImageNormal);
			setImage({ ID_POSTSGRID_DOWNLOADATTACHMENTS }, IDB_MESSAGES_DOWNLOADATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_POSTSGRID_VIEWITEMATTACHMENT);
			setImage({ ID_POSTSGRID_VIEWITEMATTACHMENT }, IDB_MESSAGES_VIEWITEMATTACHMENT, xtpImageNormal);
			setImage({ ID_POSTSGRID_VIEWITEMATTACHMENT }, IDB_MESSAGES_VIEWITEMATTACHMENT_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_POSTSGRID_DELETEATTACHMENT);
			setImage({ ID_POSTSGRID_DELETEATTACHMENT }, IDB_MESSAGES_DELETEATTACHMENTS, xtpImageNormal);
			setImage({ ID_POSTSGRID_DELETEATTACHMENT }, IDB_MESSAGES_DELETEATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
    }*/

    CreateLinkGroups(tab, false, true);

	automationAddCommandIDToGreenLight(ID_GRIDTEAMCHANNELMESSAGES_SHOWBODY);

    return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameTeamChannelMessages::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
    AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionSelectedPreviewBody(p_Action))
		p_CommandID = ID_GRIDTEAMCHANNELMESSAGES_SHOWBODY;
	/*else if (IsActionSelectedAttachmentLoadInfo(p_Action))
	{
		p_CommandID = ID_POSTSGRID_SHOWATTACHMENTS;
		SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedAttachmentDownload(p_Action))
	{
		p_CommandID = ID_POSTSGRID_DOWNLOADATTACHMENTS;
		SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedPreviewItem(p_Action))
		p_CommandID = ID_POSTSGRID_VIEWITEMATTACHMENT;
	else if (IsActionSelectedAttachmentDelete(p_Action))
		p_CommandID = ID_POSTSGRID_DELETEATTACHMENT;*/

    return statusRV;
}