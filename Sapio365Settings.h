#pragma once

#include "RegistrySetting.h"
#include "SessionIdentifier.h"

template<typename T>
class NoSetting : public ISetting<T>
{
	inline virtual boost::YOpt<T> Get() const override
	{
		return boost::none;
	}

	inline virtual bool Set(boost::YOpt<T> /*p_Val*/) const override
	{
		return false;
	}

	inline virtual operator bool() const override
	{
		return false;
	}
};

class UseCacheSetting : public RegistrySetting<bool>
{
public:
	UseCacheSetting();
};

class AutoReloadSetting : public RegistrySetting<bool>
{
public:
	AutoReloadSetting();
};

class AutoLoadLastSessionSetting : public RegistrySetting<bool>
{
public:
	AutoLoadLastSessionSetting();
};

class HistoryModeSetting : public RegistrySetting<wstring>
{
public:
	HistoryModeSetting();
};

class CreateNewFrameSetting : public RegistrySetting<bool>
{
public:
	CreateNewFrameSetting(const wstring& p_FrameAutomationName);
};

class HideNewWindowDialogSetting : public RegistrySetting<bool>
{
public:
	HideNewWindowDialogSetting(const wstring& p_FrameAutomationName);
};

// Handy class to reset all settings in NewWindow registry key (delete the key basically)
class AllNewWindowKeySettings : public RegistrySettingNoInit<bool>
{
public:
	AllNewWindowKeySettings();
};

class HideGroupAdminRestrictedAccessDialogSetting : public RegistrySetting<bool>
{
public:
	HideGroupAdminRestrictedAccessDialogSetting();
};

class HideGroupLicenseRestrictedAccessDialogSetting : public RegistrySetting<bool>
{
public:
	HideGroupLicenseRestrictedAccessDialogSetting();
};

class HideUserPermissionRequirementDialogSetting : public RegistrySetting<bool>
{
public:
	HideUserPermissionRequirementDialogSetting();
};

class HideUltraAdminRequirementDialogSetting : public RegistrySetting<bool>
{
public:
	HideUltraAdminRequirementDialogSetting();
};

class HideConsentReminderDialogSetting : public RegistrySetting<bool>
{
public:
	HideConsentReminderDialogSetting();
};

class HideUltraAdminDeprecatedReminderSetting : public RegistrySetting<bool>
{
public:
	HideUltraAdminDeprecatedReminderSetting();
};

class HideReportAnonymousDataWarningDialogSetting : public RegistrySetting<bool>
{
public:
	HideReportAnonymousDataWarningDialogSetting();
};

class HideStandardToExistingAdvancedReminderSetting : public RegistrySetting<bool>
{
public:
	HideStandardToExistingAdvancedReminderSetting();
};

class HideStandardToNewAdvancedReminderSetting : public RegistrySetting<bool>
{
public:
	HideStandardToNewAdvancedReminderSetting();
};

class HideJobCenterLoginRequiredDialogSetting : public RegistrySetting<bool>
{
public:
	HideJobCenterLoginRequiredDialogSetting(const wstring& p_ScriptName);
};

class HideHideJobCenterAdminRequiredDialogSetting : public RegistrySetting<bool>
{
public:
	HideHideJobCenterAdminRequiredDialogSetting(const wstring& p_ScriptName);
};

class HideJobCenterUltraAdminRequiredDialogSetting : public RegistrySetting<bool>
{
public:
	HideJobCenterUltraAdminRequiredDialogSetting(const wstring& p_ScriptName);
};

class HideHideJobCenterNoUltraAdminRequiredDialogSetting : public RegistrySetting<bool>
{
public:
	HideHideJobCenterNoUltraAdminRequiredDialogSetting(const wstring& p_ScriptName);
};

class DownloadFolderSetting : public RegistrySetting<wstring>
{
public:
	DownloadFolderSetting();
};

class DownloadMessageDefaultColumnsSetting : public RegistrySetting<wstring>
{
public:
	DownloadMessageDefaultColumnsSetting();
};

class DownloadEventDefaultColumnsSetting : public RegistrySetting<wstring>
{
public:
	DownloadEventDefaultColumnsSetting();
};

class DownloadPostDefaultColumnsSetting : public RegistrySetting<wstring>
{
public:
	DownloadPostDefaultColumnsSetting();
};

class MainFrameSessionSetting : public RegistrySettingNoInit<SessionIdentifier>
{
public:
	MainFrameSessionSetting();
};

// For migration 2 to 3 of session sql db.
class MainFrameSessionOldSetting : public RegistrySettingNoInit<SessionIdentifier>
{
public:
	MainFrameSessionOldSetting();
};

class MainFrameSessionListSizeSetting : public RegistrySetting<int>
{
public:
	enum GallerySize
	{
		SMALL_GALLERY = 0,
		MEDIUM_GALLERY,
		LARGE_GALLERY,
	};
public:
	MainFrameSessionListSizeSetting();
};

class OnlineSkuSetting : public RegistrySetting<wstring>
{
public:
	OnlineSkuSetting();
};

class DeltaTemporisatorSetting : public RegistrySetting<int>
{
public:
	DeltaTemporisatorSetting();
};

class OnPremServerSetting : public RegistrySetting<wstring>
{
public:
	OnPremServerSetting();
};

class OnPremUsernameSetting : public RegistrySetting<wstring>
{
public:
	OnPremUsernameSetting();
};

class OnPremEncryptedPasswordSetting : public RegistrySetting<wstring>
{
public:
	OnPremEncryptedPasswordSetting(const wstring& p_OnPremUsername);

	boost::YOpt<wstring> Get() const override;
	bool Set(boost::YOpt<wstring> p_Val) const override;

private:
	const wstring m_OnPremUsername;
};

// Backward compat
class OnPremPasswordSetting : public RegistrySetting<wstring>
{
public:
	OnPremPasswordSetting();
};

class OnPremAADComputerNameSetting : public RegistrySetting<wstring>
{
public:
	OnPremAADComputerNameSetting();
};

class OnPremAskedForAADComputerName : public RegistrySetting<bool>
{
public:
	OnPremAskedForAADComputerName();
};

class OnPremUseMsDsConsistencyGuidSetting : public RegistrySetting<bool>
{
public:
	OnPremUseMsDsConsistencyGuidSetting();
};
