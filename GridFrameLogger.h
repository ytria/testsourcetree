#pragma once

#include "FrameLogger.h"

class CacheGrid;
class GridFrameLogger : public FrameLogger
{
public:
	GridFrameLogger(CFrameWnd& p_Frame);
	virtual void LogSpecific(const LogEntry& p_Entry) const;

	void SetIgnoreHttp404Errors(bool p_Ignore);

private:
	bool m_Ignore404;
};