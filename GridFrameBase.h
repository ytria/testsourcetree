#pragma once

#include "BusinessObjectManager.h"
#include "CacheGrid.h"
#include "CacheGridColumnOrg.h"
#include "CodeJockFrameBase.h"
#include "CommandDispatcher.h"
#include "CommandInfo.h"
#include "FrameBase.h"
#include "GridFrameLogger.h"
#include "GridSnapshot.h"
#include "HistoryFrame.h"
#include "LicenseManager.h"
#include "ModificationButtonsUpdater.h"
#include "ModuleOptions.h"
#include "O365UpdateOperation.h"
#include "RefreshSpecificData.h"
#include "SelectedItem.h"
#include "YObserver.h"
#include "YTimeDate.h"
#include "YtriaTaskData.h"
#include "WindowsListObserver.h"

enum class RefreshMode
{
	IndividualRequest,
	ListRequest,
	NotSet
};

class ModuleBase;
class O365Grid;
class CXTPRibbonBar;
class DlgBrowser;
class GridFrameBase
	: public FrameBase<CodeJockFrameBase>
	, public GridFocusObserver
	, public WindowsListObserver
	, public CacheGridColumnOrgStateObserver
	, public ITaskOriginator
	, public IHistoryFrame
	, public LicenseManagerObserver
{
public:
	DECLARE_DYNAMIC(GridFrameBase)
	GridFrameBase(const LicenseContext& p_LicenseContext
		, const SessionIdentifier& p_SessionIdentifier
		, const PooledString& p_Title
		, ModuleBase& p_Module
		, HistoryMode::Mode p_HistoryMode
		, CFrameWnd* p_PreviousFrame
		, bool p_CanRefreshFromSql = false
		, std::shared_ptr<GridFrameLogger> p_CustomLogger = nullptr
	);
	virtual ~GridFrameBase() override;

	virtual bool sessionWasSet(bool p_SessionHasChanged) override; // Must only be called by FrameBase::SetSapio365Session  (exception made for GridFrameBase::OnFrameLoaded())

	// Pure virtual in WindowsListObserver.
	virtual void UpdateWindowList() override;

	virtual void ErectSpecific() override;
	virtual void HistoryChanged() override;
	virtual void ShownViaHistory() override;

    virtual bool HasApplyButton();

	virtual O365Grid& GetGrid() = 0;
	virtual AutomationContext GetAutomationContext() override;

	virtual void ApplyAllSpecific();
    virtual void ApplySelectedSpecific();
	void RefreshSpecific();
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) = 0;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction);

    void AddToSelection(const vector<SelectedItem>& p_SelectedItems);
	const wstring& GetFrameTitle() const;

	wstring GetWindowsListQuickAccessTitle() const;

	const YTimeDate& GetCreationDate() const;
	const YTimeDate& GetLastRefreshDate() const;
	void SetLastRefreshDate(const YTimeDate& p_TimeDate);
    bool IsFirstLoad();

    void SetTaskData(const YtriaTaskData& p_TaskData);
    vector<SelectedItem> AskForItemsToAdd(CWnd* p_Parent);

	//virtual bool UpdateRibbonCommand(CWnd* commandTarget, UINT commandID, LPCTSTR tooltip, LPCTSTR description, BOOL checked, UINT updateCommandFlags);

	/*Only call this at the end of a module loading task (no refresh, no load more, etc..)*/
	void UpdateContext(const YtriaTaskData& p_TaskData, HWND p_Originator);
	/*Call this when UpdateContext doesn't apply*/
	void TaskFinished(const YtriaTaskData& p_TaskData, AutomationAction* p_ActionToGreenlight, HWND p_Originator);

	void RefreshDataContext();

	Origin GetOrigin() const;

	virtual void OnCancel(CancelContext& p_Context);

	int GetTaskID() const;

	void SetIsBeingAutomaticallyArrangedOrTitled(bool p_BeingArrangedOrTitled);

	virtual bool CompliesWithDataLoadPolicy();

	virtual bool CloseFrame();
	virtual bool CanBeClosedWithoutConfirmation();

	static DWORD g_NoWindowsListUpdate;
	
	virtual AutomatedApp::AUTOMATIONSTATUS ExecuteAction(AutomationAction* p_Action) override;

	// If you override this, absolutely call base class impl first
	virtual void InitModuleCriteria(const ModuleCriteria& p_ModuleCriteria);

	bool CheckModuleCriteria(const ModuleCriteria& wantedCriteria);

	const ModuleCriteria& GetModuleCriteria() const;
    ModuleCriteria& GetModuleCriteria();

	ModuleBase& GetModuleBase();
	std::shared_ptr<BusinessObjectManager> GetBusinessObjectManager();

	virtual void SetTaskComplete(const bool p_Complete) override;
    bool HasNoTaskRunning() const;

	void SetRefreshMode(RefreshMode p_RefreshMode);
	RefreshMode GetRefreshMode() const;

	bool ShouldForceFullRefreshAfterApply() const;
	bool HasNextRefreshSelectedData() const;
	const RefreshSpecificData& GetNextRefreshSelectedData() const;
	bool HasLastUpdateOperations() const;
	const vector<O365UpdateOperation>& GetLastUpdateOperations() const;
	void ForgetLastUpdateOperations();
	void SetRemoveRowsCallback(const std::function<void(const std::set<GridBackendRow*>&)>& p_Callback);

    void RefreshAfterSubItemUpdates(const RefreshSpecificData& p_RefreshSpecificData, AutomationAction* p_CurrentAction);
	void RefreshAfterUpdate(vector<O365UpdateOperation>&& p_O365UpdateOperations, AutomationAction* p_CurrentAction);
	void RefreshAfterUpdate(vector<O365UpdateOperation>&& p_O365UpdateOperations, const RefreshSpecificData& p_RefreshSpecificData, AutomationAction* p_CurrentAction);

    RefreshSpecificData GetRefreshSelectedData();

	bool GetAndResetSyncNow();
	void SetSyncNow(bool p_Val);
	void SetDoLoadMoreOnNextRefresh(bool p_Val);
	bool GetAndResetDoLoadMoreOnNextRefresh();

	virtual bool HasDeltaFeature() const;

	// For DlgBrowserOpener
	afx_msg LRESULT OnOAuth2BrowserClose(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetOAuth2Browser(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetOAuth2Browser(WPARAM wParam, LPARAM lParam);
	////////

	afx_msg LRESULT OnAuthenticationSuccess(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAuthenticationCanceled(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAuthenticationFailure(WPARAM wParam, LPARAM lParam);

	virtual void ProcessLicenseContext() override;

	AutomationAction*	GetAutomationAction() const;
	void				AutomationCleanup();

	CXTPControlGallery* GetGalleryHierchyFilter() const;

	bool IsRBACHideUnscopedObjects() const;

	const boost::YOpt<vector<MetaDataColumnInfo>>& GetMetaDataColumnInfo() const;

	const PooledString& GetCustomTitle() const;

	void ShowErrorMessageBar(const wstring& p_Title, const wstring& p_Message, const wstring& p_Details);
	void ShowSuccessMessageBar(const wstring& p_Title, const wstring& p_Message, int p_ResIcon = NO_ICON);
	void RemoveMessageBar();

	void LoadSnaphot(std::shared_ptr<GridSnapshot::Loader> p_Loader, const SessionIdentifier& p_SessionIdentifierForFutureConnnection);
	bool IsSnapshotMode() const;
	void SetSnapshotMode(bool p_SnapshotMode);

	std::shared_ptr<GridFrameLogger> GetGridLogger();

	struct ApplyConfirmationConfig
	{
		wstring m_AdditionalWarning;
		DlgMessageBox::eIcon m_Icon = DlgMessageBox::eIcon_Question;
	};
	virtual ApplyConfirmationConfig GetApplyConfirmationConfig(bool p_ApplySelected) const;

	virtual void SetOptions(const ModuleOptions& p_Options); // can be overridden for debugging purposes like validating options.
	const boost::YOpt<ModuleOptions>& GetOptions() const;

	virtual void ApplyAJLLicenseAccess();

	boost::YOpt<wstring> GetUsersDeltaLink() const;
	boost::YOpt<wstring> GetGroupsDeltaLink() const;

	void SetUsersDeltaLink(const boost::YOpt<wstring>& p_Link);
	void SetGroupsDeltaLink(const boost::YOpt<wstring>& p_Link);

protected:
	vector<O365UpdateOperation>& AccessLastUpdateOperations();
    bool closeConfirmation(bool updateGridIfNoSave);
	bool closeHistoryConfirmation();

	virtual BOOL OnFrameLoaded() override;
	virtual void OnThemeWasSet() override;

	virtual void logout() override;

	DECLARE_MESSAGE_MAP()
    
	afx_msg void OnApplyAll();
	afx_msg void OnUpdateApplyAll(CCmdUI* pCmdUI);
    afx_msg void OnApplySelected();

    void ApplySelectedImpl(bool p_RestoreSelection);

    afx_msg void OnUpdateApplySelected(CCmdUI* pCmdUI);
	afx_msg void On365Refresh();
	afx_msg void OnUpdate365Refresh(CCmdUI* pCmdUI);
	afx_msg void On365RefreshAsFirstLoad();
	afx_msg void OnUpdate365RefreshAsFirstLoad(CCmdUI* pCmdUI);
	afx_msg void On365RefreshResyncDelta();
	afx_msg void OnUpdate365RefreshResyncDelta(CCmdUI* pCmdUI);
    afx_msg void On365RefreshSelection();
    afx_msg void OnUpdate365RefreshSelection(CCmdUI* pCmdUI);
	afx_msg void On365RevertAll();
	afx_msg void OnUpdate365RevertAll(CCmdUI* pCmdUI);
	afx_msg void On365RevertSelected();
	afx_msg void OnUpdate365RevertSelected(CCmdUI* pCmdUI);
    afx_msg void OnAddToSelection();
    afx_msg void OnUpdateAddToSelection(CCmdUI* pCmdUI);
    afx_msg void OnRemoveFromSelection();
    afx_msg void OnUpdateRemoveFromSelection(CCmdUI* pCmdUI);
	afx_msg void OnRelog();
	afx_msg void OnUpdateRelog(CCmdUI* pCmdUI);
    afx_msg void OnClose();
	afx_msg void OnUpdateLogout(CCmdUI* pCmdUI);
	afx_msg void OnQuickAccess();
	afx_msg void OnUpdateQuickAccess(CCmdUI* pCmdUI);
	afx_msg void OnMainWindowQuickAccess();
	afx_msg void OnUpdateMainWindowQuickAccess(CCmdUI* pCmdUI);
	afx_msg void OnShowDataContext();
	afx_msg void OnUpdateShowDataContext(CCmdUI* pCmdUI);
	afx_msg void OnCloseGridFrame();
	afx_msg void OnUpdateCloseGridFrame(CCmdUI* pCmdUI);
	afx_msg void OnHierarchyFilters();
	afx_msg void OnUpdateHierarchyFilters(CCmdUI* pCmdUI);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg LRESULT OnXTPCommand(WPARAM wParam, LPARAM lParam);
	afx_msg void OnPrevious();
	afx_msg void OnUpdatePrevious(CCmdUI* pCmdUI);
	afx_msg void OnNext();
	afx_msg void OnNextAndRefresh();
	afx_msg void OnUpdateNext(CCmdUI* pCmdUI);
	afx_msg void OnUpdateHistoryItem(CCmdUI* pCmdUI);
	afx_msg void OnUpdateInPlaceFilter(CCmdUI* pCmdUI);
	afx_msg void OnInPlaceFilter();
	afx_msg void OnUpdateInPlaceFilterCombo(CCmdUI* pCmdUI);
	afx_msg void OnUpdateInPlaceFilterCheckbox(CCmdUI* pCmdUI);
	afx_msg void OnInPlaceFilterCheckbox();
	afx_msg void OnUpdateCacheGridExport(CCmdUI* pCmdUI);
	afx_msg void OnCacheGridExport();
	afx_msg void OnUpdateCacheGridComparator(CCmdUI* pCmdUI);
	afx_msg void OnCacheGridComparator();
	afx_msg void OnUpdateCacheGridFilter(CCmdUI* pCmdUI);
	afx_msg void OnCacheGridFilter();
	afx_msg void OnUpdateCacheGridDuplicates(CCmdUI* pCmdUI);
	afx_msg void OnCacheGridDuplicates();
	afx_msg void OnUpdateCacheGridAnnotation(CCmdUI* pCmdUI);
	afx_msg void OnCacheGridAnnotation();
	afx_msg void OnUpdateClearUsersGraphCache_debug(CCmdUI* pCmdUI);
	afx_msg void OnClearGraphUsersCache_debug();
	afx_msg void OnUpdateClearGroupsGraphCache_debug(CCmdUI* pCmdUI);
	afx_msg void OnClearGraphGroupsCache_debug();
	afx_msg void OnToggleRemoveRBACOutOfScope();
	afx_msg void OnUpdateToggleRBACRemoveOutOfScope(CCmdUI* pCmdUI);
	afx_msg void OnShowErrorMessageBox();
	afx_msg void OnEditElevated();
	afx_msg void OnUpdateEditElevated(CCmdUI* pCmdUI);
	afx_msg void OnTokenBuy();

	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) override;
	virtual BOOL PreTranslateMessage(MSG* pMsg) override;
			
	// In-ribbon search test (not really working yet)
	//int OnCreateCommandBar(LPCREATEBARSTRUCT lpCreateBar);

	void FocusChanged(const CacheGrid& grid, bool hasFocus) override;
	void StateChanged(const CacheGridColumnOrg& gcvd, bool pinned, bool minimized) override;
	void LicenseChanged() override;

	void fixPosition();
    void SelectModificationsInNextRefreshSelectedData();

	virtual wstring getInitFrameTitle() const;

	// The grid must be created in this callback (called from OnCreate)
	// as we need access to the grid GCVD right atfer.
	virtual void createGrid() = 0;

	virtual void customizeRibbonBar(CXTPRibbonBar& ribbonBar);

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup);

	virtual void createFrameSpecificTabs();

    void CreateDefaultGroups(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup);

    void CreateLinkGroups(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup, bool p_ShowLinkTabSite = false);

    void CreateViewDataEditGroups(CXTPRibbonTab& tab);

    CXTPRibbonGroup* CreateViewGroup(CXTPRibbonTab& p_Tab); // Users view: Show Default Columns, Show All Columns
    CXTPRibbonGroup* CreateRefreshGroup(CXTPRibbonTab& p_Tab); // Users view: Refresh All, Load More
	CXTPRibbonGroup* CreateApplyGroup(CXTPRibbonTab& p_Tab);
	CXTPRibbonGroup* CreateRevertGroup(CXTPRibbonTab& p_Tab);
	CXTPRibbonGroup* CreateEditGroup(CXTPRibbonTab& p_Tab, bool p_AddApplyAndRevertGroupsIfNecessary = true); // Users/Groups view: Apply Group, View Selected, Edit Selected, Create, Delete
	CXTPRibbonGroup* CreateUserInfoGroup(CXTPRibbonTab& p_Tab); // Any view with users: Show Drive items, Show messages etc..
    CXTPRibbonGroup* CreateGroupInfoGroup(CXTPRibbonTab& p_Tab); // Any view with groups: Show Members, Show Owners, etc..
	CXTPRibbonGroup* CreateSiteInfoGroup(CXTPRibbonTab& p_Tab);

	struct O365ControlConfig
	{
		UINT m_ControlID = 0;
		UINT m_Image16x16ResourceID = 0;
		UINT m_Image32x32ResourceID = 0;
		wstring m_ControlText;
		wstring m_ControlTooltipTitle;
		wstring m_ControlDescription;
	};
	virtual O365ControlConfig GetRefreshPartialControlConfig();
	virtual O365ControlConfig GetApplyPartialControlConfig();
	virtual O365ControlConfig GetRevertPartialControlConfig();

	// To be called by child classes in their GetRefreshPartialControlConfig() override.
	static O365ControlConfig GetRefreshPartialControlConfigGroups();
	static O365ControlConfig GetRefreshPartialControlConfigLists();
	static O365ControlConfig GetRefreshPartialControlConfigRoles();
	static O365ControlConfig GetRefreshPartialControlConfigSites();
	static O365ControlConfig GetRefreshPartialControlConfigUsers();
	static O365ControlConfig GetRefreshPartialControlConfigChannels();

	// To be called by child classes in their GetApplyPartialControlConfig() override.
	static O365ControlConfig GetApplyPartialControlConfigGroups();
	static O365ControlConfig GetApplyPartialControlConfigUsers();
	static O365ControlConfig GetApplyPartialControlConfigRoles();
	static O365ControlConfig GetApplyPartialControlConfigSites();
	static O365ControlConfig GetApplyPartialControlConfigChannels();

	CXTPControl*	AddO365ApplyAllControl(CXTPRibbonGroup& p_Group);
    void			AddO365ApplySelectedControl(CXTPRibbonGroup& p_Group);
	CXTPControl*	AddO365RevertAllControl(CXTPRibbonGroup& p_Group);
	void			AddO365RevertSelectedControl(CXTPRibbonGroup& p_Group);

	void addNavigationGroup(CXTPRibbonTab& tab);

	/** FrameBase **/
	virtual void updateConnectedControl(CXTPControl& connectedControl) override;

	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) = 0;
	AutomatedApp::AUTOMATIONSTATUS			automationExecuteCommand(const UINT p_CommandID, AutomationAction* p_Action);

	void setGridControlTooltip(CXTPControl* p_Control);

	void setAutomationGreenLight(AutomationAction* p_Action);
	void automationAddCommandIDToGreenLight(const UINT p_CommandID);

	CXTPControl* addNewModuleCommandControl(CXTPRibbonGroup& group, UINT commandID, const vector<UINT> menuCommandIDs, const vector<UINT> imageIDs);

	CString getAcceleratorString(UINT p_CmdID);

	bool isDebugMode() const;

	void updateTimeRefreshControlToNow();

	bool hasModificationsPending(bool inSelectionOnly = false);
	virtual bool hasLoseableModificationsPending(bool inSelectionOnly = false); // for refresh, close, logout
	bool hasRevertableModificationsPending(bool inSelectionOnly = false);
	// This doesn't handle subitems mods.
	bool hasModificationsPendingInParentOfSelected();
	virtual bool hasLoseableModificationsPendingInParentOfSelected(); // for refresh, close, logout

	CXTPControl* addControlTo(const O365ControlConfig& p_Config, CXTPRibbonGroup& p_Group, UINT p_IdForAccelerator);

	uint32_t getAlreadyChargedTokenAmount() const;
	void setAlreadyChargedTokenAmount(uint32_t p_Tokens);

private:
	void removeFromSelection(const std::set<PooledString>& p_IdsToRemove, bool p_AllowEmpty);

	void							automationAddGenericCommandIDsToGreenlight();
	AutomatedApp::AUTOMATIONSTATUS	automationGetCommandID(UINT& p_CommandID, AutomationAction* p_Action);
	virtual void					postAutomateSpecific(AutomationAction* p_MotherAction);
	bool							closingAllowedByAutomation() const;
	
	virtual CXTPRibbonBar* createRibbonBar() override final;
	void onGCVDFocusChanged(bool gcvdHasFocus);

	CXTPControl* addGridCommandControl(CXTPRibbonGroup& group, UINT commandID, XTPControlType controlType = xtpControlButton);
	CXTPControl* addGridSplitButtonControl(CXTPRibbonGroup& group, UINT commandID, const vector<UINT> dropDownCommandIDs);
	CXTPControl* addGridPopupButtonControl(CXTPRibbonGroup& group, UINT commandID, const vector<UINT> dropDownCommandIDs, bool isSplit = false);
	enum class CommandImageSize
	{
		PX16,
		PX32,
	};
	static UINT getGridCommandImageID(UINT command, XTPImageState xTPImageState, const CommandImageSize size);

	void UpdateWindowListGallery(CXTPControlGallery* p_pGallery);
	void UpdateMenuSortByTotal();
	void UpdateMenuSetColumnTotal();
	void UpdateHierarchyFilterGallery(CXTPControlGallery* p_pGallery);
	void UpdateHierarchyFilterGallery();

	void customizeRibbonBar();
	void customizeGridGlobalRibbonTab(CXTPRibbonTab& tab);
	void customizeGridColumnRibbonTab(CXTPRibbonTab& tab);
	void customizeGridFormatRibbonTab(CXTPRibbonTab& tab);
	void customizeGridGroupRibbonTab(CXTPRibbonTab& tab);
	void customizeGridHierarchyRibbonTab(CXTPRibbonTab& tab);
	void customizeGridCellsRibbonTab(CXTPRibbonTab& tab);
	void customizeGridOptionsRibbonTab(CXTPRibbonTab& tab);

	void customizeWindowsRibbonTab(CXTPRibbonTab& tab);
	void customizeGridGCVDRibbonTab(CXTPRibbonTab& tab);
	void customizeInfoRibbonTab(CXTPRibbonTab& tab);

	void setSessionTabTitle();

	/*void OnSearchEdit(NMHDR* notifyStruct, LRESULT* result);
	afx_msg void OnUpdateSearchEdit(CCmdUI* pCmdUI);*/

	afx_msg void OnUpdateFiltersList(CCmdUI* pCmdUI);

	//afx_msg void OnUpdateFreezeHeader(CCmdUI* pCmdUI);
	/*void OnFreezeUpTo(NMHDR* pNMHDR, LRESULT* pResult);
	bool ExecuteFreezeUpTo(CXTPControl& control);*/

	void updateTimeRefreshControl();

	virtual void revertSelectedImpl();

	static void initStaticStrings();

	UINT GetGrayedHierarchyFilterTypeIconID(UINT normalId);

	CXTPControl* addGridConfigControl(CXTPRibbonGroup& group);
	CXTPControl* addGridStatsControls(CXTPRibbonGroup& group);
	CXTPControl* addGridDuplicatesControls(CXTPRibbonGroup& group);

	CXTPRibbonGroup* getSessionGroup();

	virtual wstring getPositionAndSizeRegistrySectionName() const;

	void updateNavigationDropDown(set<CXTPControl*> p_Controls, UINT p_Id, vector<IHistoryFrame*> p_History);

	bool canNavigate();

	void applyInPlaceFilter();
	CString getInPlaceFilterBackendText();
	void updateInPlaceFilterApplyButton();
	void updateInPlaceFilterText();
	bool isInPlaceFilterApplyShown() const;

	void OnNextImpl(bool p_Refresh);

	void On365RefreshAsFirstLoadImpl(bool p_Warn);
	void On365RefreshImpl(vector<O365UpdateOperation>&& p_UpdateOperations, AutomationAction* p_CurrentAction);
	void On365RefreshSelectionImpl(const RefreshSpecificData& p_RefreshData, vector<O365UpdateOperation>&& p_UpdateOperations, bool p_AppliedSubItems, AutomationAction* p_CurrentAction);
	// For DlgBrowserOpener
	DlgBrowser* getOauth2Browser() const;
	void setOauth2Browser(DlgBrowser* val);

	void updateDumpModeDisplay();

	bool showMessageBar(const wstring& p_Title, const wstring& p_Message, const wstring& p_Details, const std::unordered_map<UINT, CString>& buttons = {}, int p_ResIcon = NO_ICON, COLORREF p_CustomBackgroundColor = COLORREF(-1));

	void UpdateRefreshControls();

	void prepareLicenseRestrictions();

	void setRefreshControlTexts(CXTPControl* p_Control);

protected:
    YtriaTaskData		m_TaskData;
    wstring				m_CommandBarsProfileName;
    bool				m_CreateAddRemoveToSelection;
    bool				m_CreateRefresh;
	bool				m_CreateRefreshPartial;
	bool				m_CreateApplyPartial; // Whether to add "Save Selected" to ribbon or not when "Save All" is added.
	bool				m_CreateShowContext;

	bool				m_AddRBACHideOutOfScope;
	bool				m_CanRefreshFromSql;
	bool				m_RefreshFromSql;
	std::shared_ptr<BusinessObjectManager> m_BusinessObjectManager;

	ModificationButtonsUpdater m_ModBtnsUpdater;

private:
	const PooledString	m_CustomTitle;
	const YTimeDate		m_CreationDate;
	const wstring		m_CreationDateString;
	YTimeDate			m_RefreshDate;
	mutable wstring		m_Title;
	ModuleBase&			m_module;

	CXTPRibbonTab*		m_actionsRibbonTab;
	CXTPRibbonTab*		m_infoRibbonTab;
	CXTPRibbonTab*		m_globalRibbonTab;
	CXTPRibbonTab*		m_sortFilterRibbonTab;
	CXTPRibbonTab*		m_columnFormatRibbonTab;
	CXTPRibbonTab*		m_groupRibbonTab;
	CXTPRibbonTab*		m_hierarchyRibbonTab;
	CXTPRibbonTab*		m_explodeCellsRibbonTab;
	CXTPRibbonTab*		m_optionsRibbonTab;
	CXTPRibbonTab*		m_gcvdRibbonTab;	
	CXTPRibbonTab*		m_windowsRibbonTab;
	CXTPRibbonTab*		m_betaRibbonTab;

	CXTPRibbonTab*		m_lastGridRelatedRibbonTab;

	CXTPRibbonGroup*	m_SnapshotReconnectGroup;
	CXTPRibbonGroup*	m_SnapshotInfoGroup;

	std::shared_ptr<GridFrameLogger> m_GridLogger;

	static bool g_staticInit;

	struct GridCommandImages
	{
		UINT m_Normal;
		UINT m_Checked;
	};
	static const std::map<UINT, GridCommandImages> g_GridCommandImages32x32;
	static const std::map<UINT, GridCommandImages> g_GridCommandImages16x16;

	static const std::map<UINT, UINT> gridCommandsDuplicatesForRibbon; // maps duplicates to original grid commands

	list<UINT> m_PreviousSummaryToobarItems;
	list<UINT> m_PreviousColumnSummaryToolbarItems;
	set<UINT> m_PreviousHierachyFilterItems;

	Origin m_Origin;

	bool m_DisplayApplyConfirm;
	bool m_BeingArrangedOrTiled;

	ModuleCriteria m_ModuleCriteria;

	set<CXTPControl*> m_SetColumnTotalControls;
	set<CXTPControl*> m_SortByTotalControls;
	set<CXTPControl*> m_NavigatePreviousControls;
	set<CXTPControl*> m_NavigateNextControls;
	CXTPControl* m_RefreshTimeControl;
	CXTPControl* m_DataContextLabelControl1;
	CXTPControl* m_DataContextLabelControl2;

	bool m_HasNoTaskRunning;

	std::map<UINT, CString> m_AcceleratorTexts;

	set<UINT> m_Automation_CommandIDToGreenlight;// IDs of UI commands that do not trigger a task in a frame (frame task completion automatically greenlights the corresponding automation action)
	AutomationAction* m_AutomationActionRequestingExecutionPermit;// because m_MotherAction in CodeJockFrameBase is only set after the action was accepted for execution (cf CommandDispatcher::ExecuteImpl)

	wstring m_InPlaceFilter;
	UINT m_InPlaceFilterID;
	bool m_InPlaceFilterAutoApply;

	CXTPControlEdit* m_InPlaceFilterEdit;

	RefreshMode m_RefreshMode = RefreshMode::NotSet;

	vector<O365UpdateOperation> m_LastUpdateOperations;
	bool m_ForceFullRefreshAfterApply;
	boost::YOpt<RefreshSpecificData> m_NextRefreshSelectedData;

	DlgBrowser* m_oauth2Browser; // For DlgBrowserOpener

	bool m_DoNotUpdateWindowsListOnClose;
	bool m_IgnoreSessionChanged;

	CXTPControl*	m_RBACHideOutOfScopeBTN;
	bool			m_RBACHideOutOfScope;

	wstring m_UltraAdminTenant;
	wstring m_UltraAdminAppID;
	wstring m_UltraAdminSecretKey;
	wstring m_UltraAdminDisplayName;

	bool	m_SyncNow;
	bool	m_NextRefreshLoadMore;

	struct LastErrorInfo
	{
		wstring m_Title;
		wstring m_Message;
		wstring m_Details;

		bool empty()
		{
			return m_Title.empty() && m_Message.empty() && m_Details.empty();
		}
	} m_LastErrorInfo;

	boost::YOpt<wstring> m_SnapshotFileName;
	std::shared_ptr<Sapio365Session> m_SnapshotSession;
	std::function<void(const std::set<GridBackendRow*>&)> m_RemoveRowsCallback;

	boost::YOpt<ModuleOptions> m_Options;

	boost::YOpt<wstring> m_UsersDeltaLink;
	boost::YOpt<wstring> m_GroupsDeltaLink;

	uint32_t m_AlreadyChargedTokenAmount;

protected:
	static const XTPRibbonTabContextColor g_GridContextColor;
	static const XTPRibbonTabContextColor g_GCVDContextColor;
	static wstring	g_GridContextName;
	static wstring	g_GCVDContextName;
	static wstring	g_ViewGroup;
	static wstring	g_ActionsDataGroup;
	static wstring	g_ActionsApplyGroup;
	static wstring	g_ActionsRevertGroup;
	static wstring	g_ActionsEditGroup;
	static wstring	g_ActionsLinkUser;
	//static wstring	g_ActionsQuickCollapse;
	static wstring	g_ActionsLinkGroup;
	static wstring	g_ActionsLinkSite;
	static wstring	g_ActionsLinkSchools;
    //static wstring	g_ActionsDriveGroup;
	static wstring	g_ActionsLinkConversation;
	static wstring	g_ActionsLinkList;
	static wstring	g_ActionsLinkChannel;
	static wstring	g_ActionsLinkMessage;
	static wstring	g_ActionsLinkSignIn;

private:
	friend class FlatObjectListFrameRefresher;
	friend class GenericModificationsReverter;
	friend class GenericModificationsApplyer;
	friend class GenericGridRefresher;
	friend class GenericSaveAllButtonUpdater;
	friend class GenericSaveSelectedButtonUpdater;
	friend class GenericRevertAllButtonUpdater;
	friend class GenericRevertSelectedButtonUpdater;
	template<class T> friend class OnPremModificationsApplyer;
	template<class T> friend class OnPremModificationsReverter;
};
