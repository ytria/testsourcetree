#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "TeamFunSettings.h"

class TeamFunSettingsDeserializer : public JsonObjectDeserializer, public Encapsulate<TeamFunSettings>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

