#pragma once

#include "IPropertySetBuilder.h"

class UserMailboxSettingsPropertySet : public IPropertySetBuilder
{
public:
    virtual vector<rttr::property> GetPropertySet() const override;
};

