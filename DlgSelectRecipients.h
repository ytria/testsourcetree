#pragma once

#include "ResizableDialog.h"
#include "resource.h"

class GridSelectRecipients;

class DlgSelectRecipients : public ResizableDialog
{
public:
    DlgSelectRecipients(GridSelectRecipients* p_Grid, CWnd* p_Parent);

    enum { IDD = IDD_DLG_SELECTRECIPIENTS };
    
    vector<PooledString> GetSelectedRecipients();

protected:
    virtual void DoDataExchange(CDataExchange* pDX) override;
    virtual BOOL OnInitDialogSpecificResizable() override;

private:
    void createGrid();

    GridSelectRecipients* m_Grid;
    CExtButton m_BtnOk;
};

