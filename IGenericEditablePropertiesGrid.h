#pragma once

#include "BusinessObject.h"
#include "CacheGrid.h"
#include "DlgFormsHTML.h"
#include "BusinessMessageRule.h" // for SapioGraphObject


template<class CacheGridClass, class BusinessObjectClass>
class IGenericEditablePropertiesGrid : public CacheGridClass
{
public:
	// SapioGraphObject is BusinessMessageRule base class, kind of stripped down BusinessObject
	static_assert(std::is_base_of<BusinessObject, BusinessObjectClass>::value || std::is_base_of<SapioGraphObject, BusinessObjectClass>::value, "BusinessObjectClass must inherit from BusinessObject or SapioGraphObject");
	static_assert(std::is_base_of<CacheGrid, CacheGridClass>::value, "CacheGridClass must inherit from CacheGrid");

public:
	using CacheGridClass::CacheGridClass;

	virtual void AddCreatedBusinessObjects(const vector<BusinessObjectClass>&, bool p_ScrollToNew);
	virtual void UpdateBusinessObjects(const vector<BusinessObjectClass>&, bool p_SetModifiedStatus);
	virtual void RemoveBusinessObjects(const vector<BusinessObjectClass>&);

	void EditObjects();
	void CreateObject();
	void ViewObjects();

	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeEdit() const;
	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeCreate() const;
	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeDelete() const;

	void ShowModificationDialog(DlgFormsHTML::Action p_Action);

	virtual BusinessObjectClass getBusinessObject(GridBackendRow*) const = 0;
	virtual void getFieldsThatHaveErrors(GridBackendRow*, std::set<wstring>&) const;
	virtual bool canEdit(GridBackendRow*) const;
	virtual bool canDelete(GridBackendRow*) const;

protected:
	virtual INT_PTR showDialog(vector<BusinessObjectClass>& p_Objects, const std::set<wstring>& p_ListFieldErrors, DlgFormsHTML::Action p_Action, CWnd* p_Parent);

	void SetEditParentWhenChildSelected(bool p_Enable);
	bool GetEditParentWhenChildSelected() const;

protected:
    std::set<int> m_RegisteredBusinessTypes;

protected:
    bool selectionHasRegisteredType(uint32_t rowFlagsToExclude);
    bool isRegisteredType(GridBackendRow* p_Row);

private:
	bool m_EditParentWhenChildSelected = false;
};

#include "IGenericEditablePropertiesGrid.hpp"
