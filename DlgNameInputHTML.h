#pragma once

#include "DlgFormsHTML.h"

class DlgNameInputHTML : public DlgFormsHTML
{
public:
	DlgNameInputHTML(const wstring& p_AutomationName, const wstring& p_InputLabel, CWnd* p_Parent);

	virtual void generateJSONScriptData() override;
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

	const wstring& GetNewName() const;
	void SetDefaultName(const wstring& p_Name);

private:
	wstring m_NewName;
	wstring m_Label;
};

class DlgCreateFolderHTML : public DlgNameInputHTML
{
public:
	DlgCreateFolderHTML(CWnd* p_Parent);

	virtual void generateJSONScriptData() override;
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

	//bool ShouldRenameOnConflict() const;

private:
	//bool m_LetGraphRenameOnConflict;
};
