#pragma once

#include "BaseObj.h"

namespace Cosmos
{
    class Database : public BaseObj
    {
    public:
        boost::YOpt<PooledString> _colls;
        boost::YOpt<PooledString> _users;
    };
}

