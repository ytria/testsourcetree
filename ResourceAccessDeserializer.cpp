#include "ResourceAccessDeserializer.h"
#include "JsonSerializeUtil.h"

void ResourceAccessDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.m_Type, p_Object);
}
