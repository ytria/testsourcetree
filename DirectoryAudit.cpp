#include "DirectoryAudit.h"

RTTR_REGISTRATION
{
	using namespace rttr;
registration::class_<DirectoryAudit>("DirectoryAudit") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Audit log entry"))))
.constructor()(policy::ctor::as_object);
}
