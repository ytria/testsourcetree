#include "AzurePaginator.h"
#include "YSafeCreateTask.h"

Azure::Paginator::Paginator(const web::uri& p_Uri, FunctionType p_RequestFunction)
	: m_InitialUri(p_Uri)
	, m_RequestFunction(p_RequestFunction)
{
}

pplx::task<void> Azure::Paginator::Paginate()
{
	static const wstring g_NextLink = _YTEXT("nextLink");

	return YSafeCreateTask([requestFct = m_RequestFunction, initialUri = m_InitialUri]() {
		auto json = requestFct(initialUri);
		while (json.has_field(g_NextLink))
		{
			const auto& nextLink = json.at(g_NextLink);
			ASSERT(nextLink.is_string());
			if (nextLink.is_string())
			{
				web::uri_builder nextPageUri(nextLink.as_string());
				if (nextPageUri.is_valid())
				{
					nextPageUri.set_scheme(_YTEXT(""));
					nextPageUri.set_host(_YTEXT(""));

					json = requestFct(nextPageUri.to_uri());
				}
				else
					json = web::json::value();
			}
			else
				json = web::json::value();
		}
	});
}
