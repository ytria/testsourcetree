#include "AssignedLicenseSerializer.h"
#include "JsonSerializeUtil.h"
#include "YtriaFieldsConstants.h"

AssignedLicenseSerializer::AssignedLicenseSerializer(const BusinessAssignedLicense& p_AssignedLicense)
    : m_AssignedLicense(p_AssignedLicense)
{
}

void AssignedLicenseSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("skuId"), m_AssignedLicense.GetSkuId(), obj);
    JsonSerializeUtil::SerializeListString(_YTEXT("disabledPlans"), m_AssignedLicense.GetDisabledPlans(), obj);
    JsonSerializeUtil::SerializeString(_YTEXT(YTRIA_BUSINESSASSIGNEDLICENSE_SKUPARTNUMBER), m_AssignedLicense.GetSkuPartNumber(), obj);
}
