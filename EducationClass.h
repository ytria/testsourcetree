#pragma once

#include "BusinessObject.h"

#include "EducationTerm.h"
#include "IdentitySet.h"
#include "EducationUser.h"

class EducationClass : public BusinessObject
{
public:
	const boost::YOpt<PooledString>& GetDescription() const;
	void SetDescription(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<PooledString>& GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<PooledString>& GetMailNickname() const;
	void SetMailNickname(const boost::YOpt<PooledString>& p_Val);
	boost::YOpt<IdentitySet> m_CreatedBy;
	const boost::YOpt<PooledString>& GetClassCode() const;
	void SetClassCode(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<PooledString>& GetExternalId() const;
	void SetExternalId(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<PooledString>& GetExternalName() const;
	void SetExternalName(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<PooledString>& GetExternalSource() const;
	void SetExternalSource(const boost::YOpt<PooledString>& p_Val);
	boost::YOpt<EducationTerm> m_Term;

	// Non-properties
	vector<EducationUser> m_Users;
	PooledString m_SchoolId;

	bool operator<(const EducationClass& p_Other) const;
	bool operator==(const EducationClass& p_Other) const;


private:
	boost::YOpt<PooledString> m_Description;
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<PooledString> m_MailNickname;
	boost::YOpt<PooledString> m_ClassCode;
	boost::YOpt<PooledString> m_ExternalId;
	boost::YOpt<PooledString> m_ExternalName;
	boost::YOpt<PooledString> m_ExternalSource;

	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND

	friend class EducationClassDeserializer;
};

