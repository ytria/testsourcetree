#include "ClassMembersListRequester.h"

#include "EducationUser.h"
#include "EducationUserDeserializer.h"
#include "LoggerService.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "EducationUserDeserializerFactory.h"
#include "BasicPageRequestLogger.h"
#include "MsGraphHttpRequestLogger.h"

ClassMembersListRequester::ClassMembersListRequester(const wstring& p_ClassId, const std::shared_ptr<IPageRequestLogger>& p_Logger)
: m_ClassId(p_ClassId)
, m_Logger(p_Logger)
{
}

TaskWrapper<void> ClassMembersListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	auto factory = std::make_unique<EducationUserDeserializerFactory>(p_Session);
	m_Deserializer = std::make_shared<ValueListDeserializer<EducationUser, EducationUserDeserializer>>(std::move(factory));

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("classes"));
	uri.append_path(m_ClassId);
	uri.append_path(_YTEXT("members"));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	MsGraphPaginator paginator(uri.to_uri(), Util::CreateDefaultGraphPageRequester(m_Deserializer, httpLogger, p_TaskData.GetOriginator()), m_Logger);
	return paginator.Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

const vector<EducationUser>& ClassMembersListRequester::GetData() const
{
	return m_Deserializer->GetData();
}
