#include "DlgBusinessLCPEditHTML.h"

DlgBusinessLCPEditHTML::DlgBusinessLCPEditHTML(BusinessLifeCyclePolicies& p_BusinessLCP, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
	: DlgFormsHTML(p_Action, p_Parent)
	, m_BusinessLCP(p_BusinessLCP)
{}

DlgBusinessLCPEditHTML::~DlgBusinessLCPEditHTML()
{}

void DlgBusinessLCPEditHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgBusinessLCPEditHTML"));

	const uint32_t directEditFlag = isCreateDialog() ? EditorFlags::DIRECT_EDIT : 0;

	// Keep. Coming soon.
	// 	addNumberEditor(
	// 		&BusinessLifeCyclePolicies::GetNumberOfDays,
	// 		&BusinessLifeCyclePolicies::SetNumberOfDays,
	// 		_YTEXT("days"),
	// 		_TLoc("Days"),
	// 		EditorFlags::REQUIRED);

	addStringEditor(
		&BusinessLifeCyclePolicies::GetNumberOfDaysAsString,
		&BusinessLifeCyclePolicies::SetNumberOfDaysAsString,
		_YTEXT("Days"),
		YtriaTranslate::Do(DlgBusinessLCPEditHTML_generateJSONScriptData_1, _YLOC("Days")) + _YTEXT(" *"),
		EditorFlags::REQUIRED | directEditFlag);

	addStringEditor(
		&BusinessLifeCyclePolicies::GetAlternativeNotificationEmails, 
		&BusinessLifeCyclePolicies::SetAlternativeNotificationEmails, 
		_YTEXT("Email"),
		YtriaTranslate::Do(DlgBusinessLCPEditHTML_generateJSONScriptData_2, _YLOC("Email")) + _YTEXT(" *"),
		EditorFlags::REQUIRED | directEditFlag);

	addComboEditor(
		&BusinessLifeCyclePolicies::GetManagedGroupTypes, 
		&BusinessLifeCyclePolicies::SetManagedGroupTypes, 
		_YTEXT("Types"),
		YtriaTranslate::Do(DlgBusinessLCPEditHTML_generateJSONScriptData_3, _YLOC("Group Types")).c_str(), 
		EditorFlags::REQUIRED | directEditFlag,
			{
				{ YtriaTranslate::Do(DlgBusinessLCPEditHTML_generateJSONScriptData_4, _YLOC("All")).c_str(), _YTEXT("All") },
				{ YtriaTranslate::Do(DlgBusinessLCPEditHTML_generateJSONScriptData_5, _YLOC("Selected")).c_str(), _YTEXT("Selected") },
				{ YtriaTranslate::Do(DlgBusinessLCPEditHTML_generateJSONScriptData_6, _YLOC("None")).c_str(), _YTEXT("None") }
			}
		);

	getGenerator().addApplyButton(isEditDialog() ? LocalizedStrings::g_ApplyButtonText : LocalizedStrings::g_CreateButtonText);
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
	getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);
}

bool DlgBusinessLCPEditHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	for (const auto& item : data)
	{
		const auto& propName = item.first;
		const auto& propValue = item.second;

		auto itStr = m_StringSetters.find(propName);
		if (m_StringSetters.end() != itStr)
		{
			itStr->second(m_BusinessLCP, boost::YOpt<PooledString>(propValue));
		}
		else
		{
			// TODO: Other types?
			ASSERT(FALSE);
		}
	}

	return true;
}

// Keep. Coming soon.
// 	void DlgBusinessLCPEditHTML::addNumberEditor(NumberGetter p_Getter, NumberSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags)
// 	{
// 		addObjectNumberEditor<BusinessLifeCyclePolicies>(vector<BusinessLifeCyclePolicies>{m_BusinessLCP}, p_Getter, p_PropName, p_PropLabel, p_Flags, [](const BusinessLifeCyclePolicies&) { return 0; });
// 		ASSERT(m_NumberSetters.end() == m_NumberSetters.find(p_PropName));
// 		m_NumberSetters[p_PropName] = p_Setter;
// 	}

void DlgBusinessLCPEditHTML::addStringEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags)
{
	addObjectStringEditor<BusinessLifeCyclePolicies>(vector<BusinessLifeCyclePolicies>{m_BusinessLCP}, p_Getter, p_PropName, p_PropLabel, p_Flags, [](const BusinessLifeCyclePolicies&) { return 0; });
	ASSERT(m_StringSetters.end() == m_StringSetters.find(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

void DlgBusinessLCPEditHTML::addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_ValuesAndLabels)
{
	addObjectListEditor<BusinessLifeCyclePolicies>(vector<BusinessLifeCyclePolicies>{m_BusinessLCP}, p_Getter, p_PropName, p_PropLabel, p_Flags, p_ValuesAndLabels, [](const BusinessLifeCyclePolicies&) { return 0; });
	ASSERT(m_StringSetters.end() == m_StringSetters.find(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}
