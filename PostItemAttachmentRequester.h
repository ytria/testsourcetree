#pragma once

#include "IRequester.h"
#include "IRequestLogger.h"

class ItemAttachmentDeserializer;
class BusinessItemAttachment;

class PostItemAttachmentRequester : public IRequester
{
public:
    PostItemAttachmentRequester(PooledString p_GroupId, PooledString p_ConversationId, PooledString p_ThreadId, PooledString p_PostId, PooledString p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger);

    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
    const BusinessItemAttachment& GetData() const;

private:
    std::shared_ptr<ItemAttachmentDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;

    PooledString m_GroupId;
    PooledString m_ConversationId;
    PooledString m_ThreadId;
    PooledString m_PostId;
    PooledString m_AttachmentId;
};

