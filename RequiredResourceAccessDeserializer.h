#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "RequiredResourceAccess.h"

class RequiredResourceAccessDeserializer : public JsonObjectDeserializer, public Encapsulate<RequiredResourceAccess>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

