#pragma once

struct HistoryMode
{
public:
	enum Mode
	{
		NOT_SET,
		SAME_HISTORY,
		NEW_HISTORY,
		DETACH_HISTORY,
	};

	static void SetModeFromCommand(UINT command);
	static void SetCurrentMode(Mode mode);
	static Mode GetAndResetMode();

	static const wstring g_RegNewHistoryMode;
	static const wstring g_RegDetachHistoryMode;
	static const wstring g_RegRetainHistoryMode;

private:
	static Mode g_Current;
};
