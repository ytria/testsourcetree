#include "RoleAssignmentPermissionDeletion.h"
#include "ActivityLoggerUtil.h"


RoleAssignmentPermissionDeletion::RoleAssignmentPermissionDeletion(GridSitePermissions& p_Grid, const RoleAssignmentPermission& p_Info):
    ISubItemDeletion(p_Grid, SubItemKey(p_Info.SiteId, p_Info.SiteUrl, p_Info.UserId, p_Info.GroupId, p_Info.RoleDefinitionId), p_Grid.GetColPermissionsElder(), p_Grid.GetColPermissionsPk(), p_Grid.GetName(p_Info.m_Row)),
    m_GridSitePermissions(p_Grid),
    m_Info(p_Info)
{
}

bool RoleAssignmentPermissionDeletion::ShouldCreateFormatter(GridBackendRow* p_Row, size_t p_Index) const
{
    return true;
}

bool RoleAssignmentPermissionDeletion::IsCorrespondingRow(GridBackendRow* p_Row) const
{
    return m_GridSitePermissions.IsCorrespondingRow(p_Row, m_Info);
}

bool RoleAssignmentPermissionDeletion::IsUserPermissionDeletion() const
{
	return !m_Info.UserId.IsEmpty();
}

bool RoleAssignmentPermissionDeletion::IsGroupPermissionDeletion() const
{
	return !m_Info.GroupId.IsEmpty();
}

GridBackendRow* RoleAssignmentPermissionDeletion::GetMainRowIfCollapsed() const
{
	vector<GridBackendRow*> rows;
	m_GridSitePermissions.GetRowsByCriteria(rows, [this](GridBackendRow* p_Row)
		{
			const auto url = p_Row->GetField(m_GridSitePermissions.GetColSiteUrl()).GetValueStr();
			const auto groupId = p_Row->GetField(m_GridSitePermissions.GetColGroupId()).GetValueStr();
			const auto userId = p_Row->GetField(m_GridSitePermissions.GetColUserId()).GetValueStr();
			return nullptr != url && m_Info.SiteUrl == url
				&& nullptr != groupId && m_Info.GroupId == groupId
				&& nullptr != userId && m_Info.UserId == userId;
		});

	if (rows.size() == 1 && IsCollapsedRow(rows.front()))
		return rows.front();

	return nullptr;
}

vector<Modification::ModificationLog> RoleAssignmentPermissionDeletion::GetModificationLogs() const
{
	return{ ModificationLog(getPk(), GetObjectName(), ActivityLoggerUtil::g_ActionDelete, _YTEXT("Role Assignemnt Permission"), GetState()) };
}