#include "InvokeResultWrapper.h"

#include "YtriaPowerShellDll.h"

InvokeResultWrapper::InvokeResultWrapper(const InvokeResult& p_Result)
	:m_InvokeResult(p_Result)
{
}

InvokeResultWrapper::~InvokeResultWrapper()
{
	YtriaPowerShellDll::Get().ReleasePSObjectCollection(m_InvokeResult.m_Collection);
	YtriaPowerShellDll::Get().ReleasePSStream(m_InvokeResult.m_ErrorStream);
}

const InvokeResult& InvokeResultWrapper::Get() const
{
	return m_InvokeResult;
}

InvokeResult& InvokeResultWrapper::Get()
{
	return m_InvokeResult;
}

void InvokeResultWrapper::SetResult(const InvokeResult& p_Result)
{
	m_InvokeResult = p_Result;
}

wstring GetPSErrorString(const std::shared_ptr<InvokeResultWrapper>& p_Result)
{
	wstring errors;
	if (p_Result)
	{
		for (int i = 0; i < p_Result->Get().m_ErrorStream->GetSize(); ++i)
		{
			errors += p_Result->Get().m_ErrorStream->GetAsString(i);
			errors += _YTEXT("\r\n");
		}
	}

	return errors;
}
