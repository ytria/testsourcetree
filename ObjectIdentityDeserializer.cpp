#include "ObjectIdentityDeserializer.h"

#include "JsonSerializeUtil.h"

void ObjectIdentityDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("signInType"), m_Data.m_SignInType, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("issuer"), m_Data.m_Issuer, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("issuerAssignedId"), m_Data.m_IssuerAssignedId, p_Object);
}
