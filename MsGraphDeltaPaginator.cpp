#include "MsGraphDeltaPaginator.h"
#include "IMsGraphPageRequester.h"
#include "IPageRequestLogger.h"
#include "Sapio365Settings.h"

MsGraphDeltaPaginator::MsGraphDeltaPaginator(const web::uri& p_Uri, const shared_ptr<IMsGraphPageRequester>& p_Requester, const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_OriginalUri(p_Uri)
	, m_Requester(p_Requester)
	, m_RequestLogger(p_Logger)
	, m_Temporisator(0)
{
	ValidateObj();
	auto val = DeltaTemporisatorSetting().Get();
	if (val && *val >= 0)
		m_Temporisator = *val;
}

TaskWrapper<void> MsGraphDeltaPaginator::Paginate(const shared_ptr<MSGraphSession>& p_Session, YtriaTaskData p_TaskData)
{
	return YSafeCreateTask([that = *this, p_Session, p_TaskData, requestLogger = m_RequestLogger, deltaLinkPtr = m_DeltaLinkPtr]() {
		web::uri initialUri = that.m_OriginalUri;
		
		IMsGraphPageRequester& requester = *that.m_Requester;

		ASSERT(nullptr != requestLogger);
		if (nullptr != requestLogger)
		{
			requestLogger->ResetPageCount();
			requestLogger->Log(initialUri, requester.GetDeserializedCount(), p_TaskData.GetOriginator());
		}
		bool lastResultEmpty = false;
		auto json = requester.Request(initialUri, p_Session, p_TaskData);
		while (HasNextPage(json) && !requester.GetIsCutOff() && !p_TaskData.IsCanceled())
		{
			web::uri_builder nextPageUri(json.at(g_NextLinkKey).as_string());
			if (nextPageUri.is_valid())
			{
				Minimize(nextPageUri);
				if (nullptr != requestLogger)
				{
					requestLogger->IncrementPageCount();
					requestLogger->Log(nextPageUri.to_uri(), requester.GetDeserializedCount(), p_TaskData.GetOriginator());
				}
				if (lastResultEmpty && 0 < that.m_Temporisator)
				{
					auto millisecondsToWait = that.m_Temporisator;
					wstring errorText;
					if (millisecondsToWait >= 1000)
						errorText = _YFORMAT(L"Delta not ready - Retrying in %s seconds", Str::getStringFromNumber(millisecondsToWait / 1000).c_str());
					else if (millisecondsToWait > 0)
						errorText = _YFORMAT(L"Delta not ready - Retrying in %s milliseconds", Str::getStringFromNumber(millisecondsToWait).c_str());
					LoggerService::Error(errorText, p_TaskData.GetOriginator());

					while (millisecondsToWait > 0 && !p_TaskData.IsCanceled())
					{
						pplx::wait((std::min)(millisecondsToWait, 1000u));
						if (millisecondsToWait > 1000)
							millisecondsToWait -= 1000;
						else
							millisecondsToWait = 0;
					}
				}
				const auto previousCount = requester.GetDeserializedCount();
				json = requester.Request(nextPageUri.to_uri(), p_Session, p_TaskData);
				lastResultEmpty = requester.GetDeserializedCount() == previousCount;
			}
			else
				json = web::json::value();
		}

		if (deltaLinkPtr && json.has_string_field(g_DeltaLinkKey))
			*deltaLinkPtr = json.at(g_DeltaLinkKey).as_string();
	});
}

void MsGraphDeltaPaginator::SetDeltaLinkPtr(const std::shared_ptr<wstring>& p_DeltaLink)
{
	m_DeltaLinkPtr = p_DeltaLink;
}

void MsGraphDeltaPaginator::ValidateObj()
{
	if (nullptr == m_Requester || nullptr == m_RequestLogger)
	{
		ASSERT(false);
		throw std::invalid_argument("MsGraphDeltaPaginator: requester and requestLogger cannot be null");
	}
}

bool MsGraphDeltaPaginator::HasNextPage(const web::json::value& p_Json)
{
	return p_Json.has_string_field(g_NextLinkKey);
}

void MsGraphDeltaPaginator::Minimize(web::uri_builder& p_Uri)
{
	// Remove parts already included in base url
	p_Uri.set_scheme(_YTEXT(""));
	p_Uri.set_host(_YTEXT(""));

	vector<wstring> pathParts = web::uri::split_path(p_Uri.path());
	if (!pathParts.empty())
	{
		pathParts.erase(pathParts.begin());
		p_Uri.set_path(_YTEXT(""));
		for (const auto& part : pathParts)
			p_Uri.append_path(part);
	}
}

const wstring MsGraphDeltaPaginator::g_NextLinkKey = _YTEXT("@odata.nextLink");
const wstring MsGraphDeltaPaginator::g_DeltaLinkKey = _YTEXT("@odata.deltaLink");
