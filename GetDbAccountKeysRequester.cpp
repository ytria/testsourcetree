#include "GetDbAccountKeysRequester.h"
#include "AzureSession.h"
#include "CosmosDbAccountDeserializer.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "BasicHttpRequestLogger.h"

Azure::GetDbAccountKeysRequester::GetDbAccountKeysRequester(PooledString p_SubscriptionId, PooledString p_ResourceGroup, PooledString p_DbAccountName)
	: m_SubscriptionId(p_SubscriptionId),
	m_ResourceGroup(p_ResourceGroup),
	m_DbAccountName(p_DbAccountName)
{
}

TaskWrapper<void> Azure::GetDbAccountKeysRequester::Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<Azure::CosmosDbAccountKeyDeserializer>();
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri(_YTEXT("subscriptions"));
	uri.append_path(m_SubscriptionId);
	uri.append_path(_YTEXT("resourceGroups"));
	uri.append_path(m_ResourceGroup);
	uri.append_path(_YTEXT("providers"));
	uri.append_path(_YTEXT("Microsoft.DocumentDB"));
	uri.append_path(_YTEXT("databaseAccounts"));
	uri.append_path(m_DbAccountName);
	uri.append_path(_YTEXT("listKeys"));
	uri.append_query(AzureSession::g_ApiVersionKey, _YTEXT("2015-04-08"));

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
	return p_Session->GetAzureSession()->Post(uri.to_uri(), httpLogger, WebPayload(), m_Result, m_Deserializer, p_TaskData);
}

const Azure::CosmosDbAccountKeys& Azure::GetDbAccountKeysRequester::GetData() const
{
	return m_Deserializer->GetData();
}
