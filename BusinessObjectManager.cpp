#include "BusinessObjectManager.h"

#include "AttachmentDeserializer.h"
#include "BaseO365Grid.h"
#include "BusinessDirectoryObjectVariant.h"
#include "BusinessDrive.h"
#include "BusinessDriveItem.h"
#include "BusinessDriveItemFolder.h"
#include "BusinessItemAttachment.h"
#include "BusinessSite.h"
#include "CachedGroupPropertySet.h"
#include "CachedUserPropertySet.h"
#include "ChannelDeserializer.h"
#include "ChatMessageDeserializer.h"
#include "ChatMessagePropertySet.h"
#include "ContactDeserializer.h"
#include "ContactFolderDeserializer.h"
#include "ContactFoldersHierarchyRequester.h"
#include "ConversationDeserializer.h"
#include "ConversationThreadPropertySet.h"
#include "DeletedGroupListRequester.h"
#include "DeletedUserListRequester.h"
#include "DirectoryObjectVariantDeserializer.h"
#include "DirectoryObjectVariantDeserializerFactory.h"
#include "DirectoryObjectPartialInfo.h"
#include "DirectoryRoleDeserializer.h"
#include "DirectoryRoleMembersRequester.h"
#include "DirectoryRolesRequester.h"
#include "DirectoryRoleTemplateDeserializer.h"
#include "DirectoryRoleTemplatesRequester.h"
#include "DriveItemDeserializer.h"
#include "DriveItemsRequester.h"
#include "DriveDeserializer.h"
#include "EventDeserializer.h"
#include "EventFileAttachmentRequester.h"
#include "EventItemAttachmentRequester.h"
#include "GroupDeserializer.h"
#include "GroupDeserializerFactory.h"
#include "GraphCache.h"
#include "GraphListDeserializer.h"
#include "GroupListFullPropertySet.h"
#include "GroupListRequester.h"
#include "GroupSettingsRequester.h"
#include "GroupSettingTemplatesRequester.h"
#include "ItemAttachmentDeserializer.h"
#include "IPropertySetBuilder.h"
#include "LicenseDetailDeserializer.h"
#include "LifeCyclePoliciesDeserializer.h"
#include "ListDeserializer.h"
#include "ListItemDeserializer.h"
#include "LoggerService.h"
#include "MacroRequest.h"
#include "MailFolderDeserializer.h"
#include "MessageDeserializer.h"
#include "MessageItemAttachmentRequester.h"
#include "MSGraphCommonData.h"
#include "MsGraphFieldNames.h"
#include "MSGraphSession.h"
#include "MSGraphUtil.h"
#include "MessageFileAttachmentRequester.h"
#include "Office365Admin.h"
#include "PaginatorUtil.h"
#include "PermissionDeserializer.h"
#include "PostDeserializer.h"
#include "PostFileattachmentRequester.h"
#include "PostItemAttachmentRequester.h"
#include "RESTException.h"
#include "RESTUtils.h"
#include "RunOnScopeEnd.h"
#include "safeTaskCall.h"
#include "SapioError.h"
#include "SiteDeserializer.h"
#include "SiteDeserializerFactory.h"
#include "SqlCacheConfig.h"
#include "SubscribedSkuDeserializer.h"
#include "SubscribedSkuDeserializerFactory.h"
#include "TaskDataManager.h"
#include "TimeUtil.h"
#include "UserDeserializer.h"
#include "UserDeserializerFactory.h"
#include "UserFullPropertySet.h"
#include "UserListFullPropertySet.h"
#include "ValueListDeserializer.h"
#include "YSafeTaskWrapper.h"
#include "YtriaTaskData.h"
#include "YtriaTranslate.h"
#include "UserMacroRequest.h"
#include "GroupMacroRequest.h"
#include "DriveRequester.h"
#include "ChannelsRequester.h"
#include "MessagePropertySet.h"
#include "EventListRequester.h"
#include "EventPropertySet.h"
#include "MessageInMailfolderPropertySet.h"
#include "MailFoldersTopLevelRequester.h"
#include "BasicPageRequestLogger.h"
#include "BasicRequestLogger.h"
#include "MultiObjectsRequestLogger.h"
#include "MultiObjectsPageRequestLogger.h"
#include "SubscribedSkusRequester.h"
#include "SitesRequester.h"
#include "RttrPropertySet.h"
#include "RbacRequiredPropertySet.h"
#include "GroupRootSiteRequester.h"
#include "MsGraphHttpRequestLogger.h"

using namespace Util;

struct BusinessObjectManager::GroupPropertyLists
{
public:
	GroupPropertyLists(const std::shared_ptr<Sapio365Session>& p_O365graphSession)
		// Bug #191115.SB.00A75A: [-EASY-] In Groups, remove "Unseen count" from request selection in Ultra Admin mode
		: m_RemoveUnseenCount(p_O365graphSession && (p_O365graphSession->IsUltraAdmin() || p_O365graphSession->IsUseRoleDelegation()))
	{
	}

	enum ID
	{
		LIST = 0,
		DELETEDGROUP_LIST,
		DYNAMIC_O365,
		O365,
		DYNAMIC_SECURITY,
	};

	const std::unordered_map<std::vector<GBI>, std::vector<rttr::property>>& GetPropertySets(const BusinessGroup& p_BG, bool p_ForSync)
	{
		const bool isDeletedGroup					= p_BG.IsFromRecycleBin();
		const bool isUnknownType					= !p_BG.GetCombinedGroupType();
		const bool isO365OrUnknownType				= isUnknownType || *p_BG.GetCombinedGroupType() == BusinessGroup::g_Office365Group;
		const bool isSecurityOrUnknownType			= isUnknownType || *p_BG.GetCombinedGroupType() == BusinessGroup::g_SecurityGroup;
		const bool isDynamicMembershipOrUnknownType	= isUnknownType || p_BG.IsDynamicMembership() && *p_BG.IsDynamicMembership();

		auto id = LIST;
		if (isDeletedGroup)
		{
			id = DELETEDGROUP_LIST;
		}
		else if (p_ForSync)
		{
			if (isO365OrUnknownType)
			{
				if (isDynamicMembershipOrUnknownType)
					id = DYNAMIC_O365;
				else
					id = O365;
			}
			else if (isDynamicMembershipOrUnknownType)
			{
				ASSERT(isSecurityOrUnknownType);
				id = DYNAMIC_SECURITY;
			}
		}

		auto& lists = m_Lists[id];

		if (lists.empty())
		{
			switch (id)
			{
			case LIST:
				FillPropertyList<LIST>(lists);
				break;
			case DELETEDGROUP_LIST:
				FillPropertyList<DELETEDGROUP_LIST>(lists);
				break;
			case DYNAMIC_O365:
				FillPropertyList<DYNAMIC_O365>(lists);
				break;
			case O365:
				FillPropertyList<O365>(lists);
				break;
			case DYNAMIC_SECURITY:
				FillPropertyList<DYNAMIC_SECURITY>(lists);
				break;
			default:
				ASSERT(false);
				break;
			};
		}

		return lists;
	};

private:
	template<ID id>
	void FillPropertyList(std::unordered_map<std::vector<GBI>, vector<rttr::property>>& lists);

	template<>
	void FillPropertyList<LIST>(std::unordered_map<std::vector<GBI>, vector<rttr::property>>& lists)
	{
		ASSERT(lists.empty());
		lists.insert({ { GBI::LIST }, GroupListFullPropertySet().GetPropertySet() });
	}

	template<>
	void FillPropertyList<DELETEDGROUP_LIST>(std::unordered_map<std::vector<GBI>, vector<rttr::property>>& lists)
	{
		ASSERT(lists.empty());
		// FIXME: Is it ok to use GBI for a deleted group?
		lists.insert({ { GBI::LIST }, DeletedGroupListFullPropertySet().GetPropertySet() });
	}

	template<>
	void FillPropertyList<DYNAMIC_O365>(std::unordered_map<std::vector<GBI>, vector<rttr::property>>& lists)
	{
		ASSERT(lists.empty());

		Group dummy;
		lists.insert({ { GBI::LIST }, GroupListFullPropertySet().GetPropertySet() });
		lists.insert({ { GBI::SYNCV1 }, dummy.GetSyncOnlyProperties(dummy) });
		lists.insert({ { GBI::SYNCBETA }, dummy.GetBetaSyncOnlyProperties(dummy) });

		if (m_RemoveUnseenCount)
		{
			ASSERT(lists.size() == 3);
			auto& syncProps = lists[{ GBI::SYNCV1}];
			syncProps.erase(std::remove_if(syncProps.begin(), syncProps.end(), [](const rttr::property& p_Property) {
				return p_Property.get_name() == O365_GROUP_UNSEENCOUNT;
				}), syncProps.end());
		}
	}

	template<>
	void FillPropertyList<O365>(std::unordered_map<std::vector<GBI>, vector<rttr::property>>& lists)
	{
		ASSERT(lists.empty());

		FillPropertyList<DYNAMIC_O365>(lists);

		ASSERT(lists.size() == 3);
		auto& betaProps = lists[{ GBI::SYNCBETA }];
		betaProps.erase(std::remove_if(betaProps.begin(), betaProps.end(), [](const rttr::property& p_Property) {
			return p_Property.get_name() == O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE
				&& p_Property.get_name() == O365_GROUP_MEMBERSHIPRULE;
			}), betaProps.end());
		betaProps.shrink_to_fit();
	}

	template<>
	void FillPropertyList<DYNAMIC_SECURITY>(std::unordered_map<std::vector<GBI>, vector<rttr::property>>& lists)
	{
		ASSERT(lists.empty());

		Group dummy;
		lists.insert({ { GBI::LIST }, GroupListFullPropertySet().GetPropertySet() });
		lists.insert({ { GBI::SYNCBETA }, dummy.GetBetaSyncOnlyProperties(dummy) });

		ASSERT(lists.size() == 2);
		auto& betaProps = lists[{ GBI::SYNCBETA }];
		betaProps.erase(std::remove_if(betaProps.begin(), betaProps.end(), [](const rttr::property& p_Property) {
			return p_Property.get_name() != O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE
				&& p_Property.get_name() != O365_GROUP_MEMBERSHIPRULE;
			}), betaProps.end());
		betaProps.shrink_to_fit();
	}

private:
	std::map<ID, std::unordered_map<std::vector<GBI>, vector<rttr::property>>> m_Lists;
	bool m_RemoveUnseenCount;
};

BusinessObjectManager::BusinessObjectManager(std::shared_ptr<Sapio365Session> p_O365graphSession)
	: m_Sapio365Session(p_O365graphSession)
	, m_PropertyLists(std::make_unique<GroupPropertyLists>(p_O365graphSession))
{
	ASSERT(m_Sapio365Session);
}

BusinessObjectManager::~BusinessObjectManager() = default;

GraphCache& BusinessObjectManager::GetGraphCache() const
{
	ASSERT(m_Sapio365Session);
	return m_Sapio365Session->GetGraphCache();
}

TaskWrapper<vector<BusinessAttachment>> BusinessObjectManager::GetUserEventAttachments(const PooledString& p_UserId, const PooledString& p_EventId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessAttachment, AttachmentDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->ListUserEventAttachments(deserializer, p_UserId, p_EventId, p_Logger, httpLogger, p_Task).Then([p_Task, deserializer, that]()
    {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessAttachment>, p_Task);
}

TaskWrapper<vector<BusinessAttachment>> BusinessObjectManager::GetGroupEventAttachments(const PooledString& p_GroupId, const PooledString& p_EventId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessAttachment, AttachmentDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->ListGroupEventAttachments(deserializer, p_GroupId, p_EventId, p_Logger, httpLogger, p_Task).Then([p_Task, deserializer, that]()
    {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessAttachment>, p_Task);
}

TaskWrapper<vector<BusinessAttachment>> BusinessObjectManager::GetMessageAttachments(const PooledString& p_UserId, const PooledString& p_MessageId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessAttachment, AttachmentDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->ListMessageAttachments(deserializer, p_UserId, p_MessageId, p_Logger, httpLogger, p_Task).Then([deserializer, p_Task, that]()
    {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessAttachment>, p_Task);
}

TaskWrapper<vector<BusinessAttachment>> BusinessObjectManager::GetPostAttachments(const PooledString& p_GroupId, const PooledString& p_ConversationId, const PooledString& p_ThreadId, const PooledString& p_PostId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessAttachment, AttachmentDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->ListPostAttachments(deserializer, p_GroupId, p_ConversationId, p_ThreadId, p_PostId, p_Logger, httpLogger, p_Task).Then([deserializer, p_Task, that]()
    {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessAttachment>, p_Task);
}

TaskWrapper<BusinessItemAttachment> BusinessObjectManager::GetMessageItemAttachment(const PooledString& p_UserId, const PooledString& p_MessageId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

    auto requester = std::make_shared<MessageItemAttachmentRequester>(p_UserId, p_MessageId, p_AttachmentId, p_Logger);
	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, p_Task, that]()
    {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::APP_SESSION), p_Task);
}

TaskWrapper<BusinessItemAttachment> BusinessObjectManager::GetUserEventItemAttachment(const PooledString& p_UserId, const PooledString& p_EventId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

    auto requester = std::make_shared<EventItemAttachmentRequester>(EventItemAttachmentRequester::EntityType::User, p_UserId, p_EventId, p_AttachmentId, p_Logger);
    return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, p_Task, that]()
    {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::APP_SESSION), p_Task);
}

TaskWrapper<BusinessItemAttachment> BusinessObjectManager::GetGroupEventItemAttachment(const PooledString& p_GroupId, const PooledString& p_EventId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

    auto requester = std::make_shared<EventItemAttachmentRequester>(EventItemAttachmentRequester::EntityType::Group, p_GroupId, p_EventId, p_AttachmentId, p_Logger);
    return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, p_Task, that]()
    {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), p_Task);
}

TaskWrapper<BusinessItemAttachment> BusinessObjectManager::GetGroupPostItemAttachment(const PooledString& p_GroupId, const PooledString& p_ConversationId, const PooledString& p_ThreadId, const PooledString& p_PostId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
    std::shared_ptr<BusinessObjectManager> that(shared_from_this());

    auto requester = std::make_shared<PostItemAttachmentRequester>(p_GroupId, p_ConversationId, p_ThreadId, p_PostId, p_AttachmentId, p_Logger);
    return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, p_Task, that]()
    {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), p_Task);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::DeleteUserEventAttachment(const PooledString& p_UserId, const PooledString& p_EventId, const PooledString& p_AttachmentId, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->DeleteUserEventAttachment(p_UserId, p_UserId, p_AttachmentId, httpLogger, p_Task).Then([that](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_Task);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::DeleteGroupEventAttachment(const PooledString& p_GroupId, const PooledString& p_EventId, const PooledString& p_AttachmentId, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->DeleteGroupEventAttachment(p_GroupId, p_EventId, p_AttachmentId, httpLogger, p_Task).Then([p_Task, that](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_Task);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::DeleteUserMessageAttachment(const PooledString& p_UserId, const PooledString& p_MessageId, const PooledString& p_AttachmentId, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->DeleteUserMessageAttachment(p_UserId, p_MessageId, p_AttachmentId, httpLogger, p_Task).Then([that, p_Task](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_Task);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::DeleteGroupPostAttachment(const PooledString& p_GroupId, const PooledString& p_ConversationId, const PooledString& p_ThreadId, const PooledString& p_PostId, const PooledString& p_AttachmentId, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->DeleteGroupPostAttachment(p_GroupId, p_ConversationId, p_ThreadId, p_PostId, p_AttachmentId, httpLogger, p_Task).Then([that, p_Task](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_Task);
}

TaskWrapper<vector<BusinessDirectoryRole>> BusinessObjectManager::GetDirectoryRoles(YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

    auto requester = std::make_shared<DirectoryRolesRequester>(std::make_shared<BasicRequestLogger>(_T("directory roles")));
	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, that, p_Task]() {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), p_Task);
}

TaskWrapper<vector<BusinessDirectoryRoleTemplate>> BusinessObjectManager::GetDirectoryRoleTemplates(YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

    auto requester = std::make_shared<DirectoryRoleTemplatesRequester>(std::make_shared<BasicRequestLogger>(_T("directory role templates")));
	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, that, p_Task]() {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), p_Task);
}

TaskWrapper<vector<BusinessUser>> BusinessObjectManager::GetDirectoryRoleMembers(const PooledString& p_DirectoryRoleId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

    auto requester = std::make_shared<DirectoryRoleMembersRequester>(p_DirectoryRoleId, p_Logger);
	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, p_Task, that]() {
		that->GetGraphCache().AddUsers(requester->GetData(), true, { UBI::MIN });
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), p_Task);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::DeleteDirectoryRoleMember(const PooledString& p_UserId, const PooledString& p_DirectoryRoleId, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->DeleteDirectoryRoleMember(p_UserId, p_DirectoryRoleId, httpLogger, p_TaskData).Then([that, p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::CreateDirectoryRoleFromTemplate(const PooledString& p_DirectoryRoleTemplateId, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->CreateDirectoryRoleFromTemplate(p_DirectoryRoleTemplateId, httpLogger, p_TaskData).Then([that, p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::DeleteDirectoryRole(const PooledString& p_DirectoryRoleId, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->DeleteDirectoryRole(p_DirectoryRoleId, httpLogger, p_TaskData).Then([that, p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::AddDirectoryRoleMember(const PooledString& p_UserId, const PooledString& p_DirectoryRoleId, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->AddDirectoryRoleMember(p_UserId, p_DirectoryRoleId, httpLogger, p_TaskData).Then([that, p_TaskData](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

vector<BusinessGroup> BusinessObjectManager::removeDuplicates(const vector<BusinessGroup> p_Groups) const
{
    vector<BusinessGroup> businessGroups;

	std::set<PooledString> alreadyIn;
    for (const auto& group : p_Groups)
    {
		if (alreadyIn.insert(group.GetID()).second)
            businessGroups.push_back(group);
    }
    return businessGroups;
}

std::shared_ptr<MSGraphSession> BusinessObjectManager::GetMSGraphSession(Sapio365Session::SessionType p_RbacSessionMode) const
{
    return m_Sapio365Session->GetMSGraphSession(p_RbacSessionMode);
}

std::shared_ptr<ExchangeOnlineSession> BusinessObjectManager::GetExchangeSession() const
{
    return m_Sapio365Session->GetExchangeSession();
}

std::shared_ptr<SharepointOnlineSession> BusinessObjectManager::GetSharepointSession() const
{
    return m_Sapio365Session->GetSharepointSession();
}

std::shared_ptr<Sapio365Session> BusinessObjectManager::GetSapio365Session() const
{
    return m_Sapio365Session;
}

///////////////////////////////////////////////////////////////////////////////////
// GROUP https://www.youtube.com/watch?v=nHHAq_--rqA
///////////////////////////////////////////////////////////////////////////////////

TaskWrapper<vector<BusinessConversation>> BusinessObjectManager::GetBusinessConversations(const PooledString& p_GroupId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessConversation, ConversationDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetConversations(deserializer, p_GroupId, {}, ConversationThreadPropertySet().GetStringPropertySet(), p_Logger, httpLogger, p_TaskData).Then([deserializer, that, p_TaskData]()
    {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessConversation>, p_TaskData);
}

TaskWrapper<vector<BusinessPost>> BusinessObjectManager::GetBusinessPosts(const PooledString& p_GroupId, const PooledString& p_ConversationId, const PooledString& p_ThreadId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessPost, PostDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetPosts(deserializer, p_GroupId, p_ConversationId, p_ThreadId, p_Logger, httpLogger, p_TaskData).Then([deserializer, that, p_TaskData]()
    {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessPost>, p_TaskData);
}

TaskWrapper<vector<BusinessGroup>> BusinessObjectManager::GetBusinessGroupsRecycleBin(const IPropertySetBuilder& p_PropertySet, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

	auto logger = std::make_shared<BasicPageRequestLogger>(_T("deleted groups"));
	auto requester = std::make_shared<DeletedGroupListRequester>(p_PropertySet, logger);
	return safeTaskCall(requester->Send(GetSapio365Session(), p_TaskData).Then([requester, that, p_TaskData]() {
		auto& deletedGroups = requester->GetData();
		that->GetGraphCache().ClearDeletedGroups();
		that->GetGraphCache().AddDeletedGroups(deletedGroups);
		that->GetGraphCache().SetAllDeletedGroupsSynced();
		return deletedGroups;
	}), GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessGroup>, p_TaskData);
}

TaskWrapper<vector<BusinessGroup>> BusinessObjectManager::GetBusinessGroupMembersHierarchies(const O365IdsContainer& p_IDs, bool p_UseSQLCacheForTopLevel, YtriaTaskData p_TaskData, bool p_AlsoRequestOwners)
{
	ASSERT(!p_IDs.empty());
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	return safeTaskCall(YSafeCreateTask([p_IDs, p_TaskData, p_UseSQLCacheForTopLevel, that, p_AlsoRequestOwners]()
	{
		vector<BusinessGroup> businessGroups;
		map<PooledString, BusinessGroup> groupsCache;
		std::shared_ptr<MultiObjectsPageRequestLogger> ownersLogger;

		auto membersLogger = std::make_shared<MultiObjectsPageRequestLogger>(_T("group members"), p_IDs.size());
		if (p_AlsoRequestOwners)
			ownersLogger = std::make_shared<MultiObjectsPageRequestLogger>(_T("group owners"), p_IDs.size());

		if (p_UseSQLCacheForTopLevel)
		{
			businessGroups = that->GetGraphCache().GetSqlCachedGroups(p_TaskData.GetOriginator(), p_IDs, true, p_TaskData);

			for (auto& group : businessGroups)
			{
				if (p_TaskData.IsCanceled())
					break;

				membersLogger->IncrementObjCount();
				membersLogger->SetContextualInfo(GetDisplayNameOrId(group));
				if (groupsCache.find(group.GetID()) == groupsCache.end())
				{
					that->getBusinessGroupMembersHierarchy(group, groupsCache, membersLogger, p_TaskData);
				}
				else
				{
					auto success = that->GetGraphCache().GetGroupMembers(group);
					ASSERT(success);
				}

				if (!p_TaskData.IsCanceled() && p_AlsoRequestOwners)
				{
					ASSERT(ownersLogger);

					group.ClearOwners(); // Owners are stored in SQL cache, clear list as we're requesting it.

					ownersLogger->SetContextualInfo(GetDisplayNameOrId(group));
					ownersLogger->IncrementObjCount();

					that->GetBusinessGroupOwnersHierarchy(group, ownersLogger, p_TaskData, true);
				}				
			}
		}
		else
		{
			for (const auto& id : p_IDs)
			{
				businessGroups.emplace_back();
				auto& group = businessGroups.back();

				membersLogger->IncrementObjCount();
				if (groupsCache.find(id) == groupsCache.end())
				{
					group.SetID(id);
					membersLogger->SetContextualInfo(that->GetGraphCache().GetGroupContextualInfo(group.GetID()));
					that->GetGraphCache().SyncUncachedGroup(group, std::make_shared<MultiObjectsRequestLogger>(*membersLogger), p_TaskData);

					membersLogger->SetContextualInfo(GetDisplayNameOrId(group));

					that->getBusinessGroupMembersHierarchy(group, groupsCache, membersLogger, p_TaskData);
				}
				else
				{
					LoggerService::Debug(_YTEXTFORMAT(L"Group already requested (%s)", id.c_str()));
					group = groupsCache[id];
				}

				if (!p_TaskData.IsCanceled() && p_AlsoRequestOwners)
				{
					ASSERT(ownersLogger);

					group.ClearOwners(); // Owners are stored in SQL cache, clear list as we're requesting it.

					ownersLogger->SetContextualInfo(GetDisplayNameOrId(group));
					ownersLogger->IncrementObjCount();

					that->GetBusinessGroupOwnersHierarchy(group, ownersLogger, p_TaskData, true);
				}

				if (p_TaskData.IsCanceled())
				{
					businessGroups.pop_back();
					break;
				}
			}
		}

        return businessGroups;
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData);
}

TaskWrapper<int32_t> BusinessObjectManager::CountGroupMembers(const PooledString& p_GroupID, std::shared_ptr<IPageRequestLogger> p_Logger, YtriaTaskData p_Task)
{
	return safeTaskCall(YSafeCreateTask([=]()
		{
			int32_t userCount = 0;

			web::uri_builder firstPageUri{ Rest::GROUPS_PATH };
			firstPageUri.append_path(p_GroupID);
			firstPageUri.append_path(Rest::MEMBERS_PATH);

			firstPageUri.append_query(U("$select"), Rest::GetSelectQuery({ User().get_type().get_property(O365_ID) }));

			bool hasMoreItems = true;

			web::uri_builder nextPageUri(firstPageUri);
			nextPageUri.append_query(U("$top"), std::to_wstring(MSGraphSession::g_DefaultNbElementsPerPage));

			int32_t pageCount = 1;
			std::shared_ptr<BusinessObjectManager> that(shared_from_this());
			auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
			while (hasMoreItems)
			{
				if (p_Task.IsCanceled())
					break;

				p_Logger->Log(nextPageUri.to_uri(), userCount, p_Task.GetOriginator());
				auto count = GetMSGraphSession(httpLogger->GetSessionType())->Read(nextPageUri.to_uri(), httpLogger, p_Task).ThenMutable([&hasMoreItems, &nextPageUri, &pageCount, p_Task](RestResultInfo result) mutable
					{
						if (!p_Task.GetLastRequestOn())
							p_Task.SetLastRequestOn(YTimeDate::GetCurrentTimeDate());

						if (result.Response.status_code() != web::http::status_codes::OK)
							hasMoreItems = false;

						int32_t count = 0;
						auto body = result.GetBodyAsString();
						if (body)
						{
							auto json = json::value::parse(*body);
							assert(json[Rest::JSONFIELD_VALUE].is_array());
							if (json[Rest::JSONFIELD_VALUE].is_array())
							{
								auto array = json[Rest::JSONFIELD_VALUE].as_array();
								count = static_cast<int32_t>(array.size());
							}

							static const wstring g_NextLinkKey = _YTEXT("@odata.nextLink");
							hasMoreItems = json.has_field(g_NextLinkKey);
							if (hasMoreItems)
							{
								++pageCount;
								web::uri_builder newUri(json[g_NextLinkKey].as_string());
								newUri.set_scheme(U(""));
								newUri.set_host(U(""));

								// Split path to easily remove the beginning of the path ("v1.0")
								auto splitPath = web::uri::split_path(newUri.to_string());
								if (splitPath.size() > 1)
									splitPath = vector<wstring>(std::begin(splitPath) + 1, std::end(splitPath));

								if (!splitPath.empty())
								{
									// Remove query from last uri part if needed
									wstring lastPathElem = splitPath[splitPath.size() - 1];
									size_t queryPos = lastPathElem.find(_YTEXT("?"));
									if (queryPos != wstring::npos)
										lastPathElem = wstring(std::begin(lastPathElem), std::begin(lastPathElem) + queryPos);
									splitPath[splitPath.size() - 1] = lastPathElem;
								}

								newUri.set_path(_YTEXT(""));

								for (const auto& pathPart : splitPath)
									newUri.append_path(pathPart);

								nextPageUri = newUri.to_uri();
							}
						}
						else
							hasMoreItems = false;

						return count;
					}).GetTask().get();

					userCount += count;
			}

			return userCount;
		}), GetMSGraphSession(Sapio365Session::USER_SESSION), p_Task);
}

void BusinessObjectManager::getBusinessGroupMembersHierarchy(BusinessGroup& p_Group, std::map<PooledString, BusinessGroup>& p_GroupsCache, const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	const auto members = [this, &p_Group, &p_TaskData, &p_Logger]
	{
		std::shared_ptr<BusinessObjectManager> that(shared_from_this());
		auto factory = std::make_unique<DirectoryObjectVariantDeserializerFactory>(that->GetSapio365Session());
		auto deserializer = std::make_shared<ValueListDeserializer<BusinessDirectoryObjectVariant, DirectoryObjectVariantDeserializer>>(std::move(factory));
		auto properties = MSGraphSession::GetUsersGroupsAndOrgContactsSelectQueryForCache();
		// FIXME: new modular cache: remove required props
		properties = RbacRequiredPropertySet<BusinessGroup>(properties, *that->GetSapio365Session()).GetStringPropertySet();
		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
		return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetGroupMembers(deserializer, p_Group.GetID(), properties, p_Logger, httpLogger, p_TaskData).Then([deserializer, that]()
		{
			return std::move(deserializer->GetData());
		}), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessDirectoryObjectVariant>, p_TaskData).get();
	}();

	p_Logger->SetLogMessage(_T("group members"));
	p_Group.SetLastRequestTime(p_TaskData.GetLastRequestOn());

    if (members.size() == 1 && members[0].GetError())
    {
        p_Group.SetChildrenError(members[0].GetError());
		GetGraphCache().StoreGroupMembers(p_Group);
    }
    else
    {
	    for (const auto& member : members)
	    {
			if (member.IsBusinessUser())
			{
				const auto& bu = member.AsBusinessUser();
				GetGraphCache().AddUser(bu, true, { UBI::MIN });
				p_Group.AddChild(bu);
			}
			else if (member.IsBusinessOrgContact())
			{
				const auto& boc = member.AsBusinessOrgContact();
				GetGraphCache().Add(boc);
				p_Group.AddChild(boc);
			}
			else if (member.IsBusinessGroup())
			{
				const auto& bg = member.AsBusinessGroup();
				GetGraphCache().AddGroup(bg, true, { GBI::MIN });
				p_Group.AddChild(bg);
			}
	    }

		GetGraphCache().StoreGroupMembers(p_Group);
		if (p_GroupsCache.find(p_Group.GetID()) == p_GroupsCache.end())
			p_GroupsCache[p_Group.GetID()] = p_Group;

		BusinessGroup tempGroup;
		const BusinessGroup emptyGroup;

		auto multiObjLogger = std::make_shared<MultiObjectsRequestLogger>(*p_Logger);
		for (auto& childGroupId : p_Group.GetChildrenGroups())
		{
			if (p_TaskData.IsCanceled())
				break;

			if (p_GroupsCache.find(childGroupId) == p_GroupsCache.end())
			{
				tempGroup = emptyGroup;
				tempGroup.SetID(childGroupId);

				// We just added this group to the cache above, so no request will be made here.
				GetGraphCache().SyncUncachedGroup(tempGroup, multiObjLogger, p_TaskData);

				getBusinessGroupMembersHierarchy(tempGroup, p_GroupsCache, p_Logger, p_TaskData);
			}
			else
			{
				LoggerService::Debug(_YTEXTFORMAT(L"Child group already requested (%s)", childGroupId.c_str()));
			}
		}
    }
}

TaskWrapper<vector<BusinessGroup>> BusinessObjectManager::GetBusinessGroupOwnersHierarchies(const O365IdsContainer& p_IDs, bool p_UseSQLCacheForTopLevel, YtriaTaskData p_TaskData, bool p_OnlyFirstLevel)
{
	ASSERT(!p_IDs.empty());
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	return safeTaskCall(YSafeCreateTask([p_IDs, p_TaskData, p_OnlyFirstLevel, p_UseSQLCacheForTopLevel, that]()
	{
		vector<BusinessGroup> businessGroups;

		if (p_UseSQLCacheForTopLevel)
		{
			auto sqlGroups = that->GetGraphCache().GetSqlCachedGroups(p_TaskData.GetOriginator(), p_IDs, true, p_TaskData);

			auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("group owners"), sqlGroups.size());
			for (auto&& group : sqlGroups)
			{
				if (p_TaskData.IsCanceled())
					break;

				group.ClearOwners(); // Owners are stored in SQL cache, clear list as we're requesting it.
				
				logger->SetContextualInfo(GetDisplayNameOrId(group));
				logger->IncrementObjCount();

				that->GetBusinessGroupOwnersHierarchy(group, logger, p_TaskData, p_OnlyFirstLevel);
				if (!p_TaskData.IsCanceled())
					businessGroups.push_back(group);
			}
		}
		else	
		{
			auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("group owners"), p_IDs.size());

			for (const auto& id : p_IDs)
			{
				businessGroups.emplace_back();
				auto& group = businessGroups.back();
				group.SetID(id);

				logger->SetContextualInfo(that->GetGraphCache().GetGroupContextualInfo(group.GetID()));
				that->GetGraphCache().SyncUncachedGroup(group, std::make_shared<MultiObjectsRequestLogger>(*logger), p_TaskData);

				logger->SetContextualInfo(GetDisplayNameOrId(group));
				logger->IncrementObjCount();
				that->GetBusinessGroupOwnersHierarchy(group, logger, p_TaskData, p_OnlyFirstLevel);
				if (p_TaskData.IsCanceled())
				{
					businessGroups.pop_back();
					break;
				}
			}
		}

        return businessGroups;
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData);
}

//void BusinessObjectManager::GetBusinessGroupOwnersHierarchies(vector<BusinessGroup>& p_Groups, YtriaTaskData p_TaskData, bool p_OnlyFirstLevel)
//{
//	OwnerCounter groupCounter(0, p_Groups.size());
//	for (auto& group : p_Groups)
//	{
//		++groupCounter;
//		GetBusinessGroupOwnersHierarchy(group, groupCounter, p_Logger, p_TaskData, p_OnlyFirstLevel);
//	}
//}

void BusinessObjectManager::GetBusinessGroupOwnersHierarchy(BusinessGroup& p_Group, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData, bool p_OnlyFirstLevel)
{
	const auto owners = [this, &p_Group, &p_TaskData, &p_Logger]()
	{
		std::shared_ptr<BusinessObjectManager> that(shared_from_this());
		auto factory = std::make_unique<DirectoryObjectVariantDeserializerFactory>(that->GetSapio365Session());
		auto deserializer = std::make_shared<ValueListDeserializer<BusinessDirectoryObjectVariant, DirectoryObjectVariantDeserializer>>(std::move(factory));
		// Need to request RBAC props?
		auto properties = MSGraphSession::GetUsersGroupsAndOrgContactsSelectQueryForCache();

		// FIXME: new modular cache, remove required props.
		properties = RbacRequiredPropertySet<BusinessGroup>(properties, *that->GetSapio365Session()).GetStringPropertySet();

		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
		return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetGroupOwners(deserializer, p_Group.GetID(), properties, p_Logger, httpLogger, p_TaskData).Then([deserializer, that]()
		{
			return std::move(deserializer->GetData());
		}), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessDirectoryObjectVariant>, p_TaskData).get();
	}();

	p_Group.SetLastRequestTime(p_TaskData.GetLastRequestOn());

    if (owners.size() == 1 && owners[0].GetError())
    {
        p_Group.SetOwnersError(owners[0].GetError());
    }
    else
    {
        for (const auto& owner : owners)
        {
			if (p_TaskData.IsCanceled())
				break;

			ASSERT(owner.IsBusinessUser());
			if (owner.IsBusinessUser())
			{
				const auto& bu = owner.AsBusinessUser();
				GetGraphCache().AddUser(bu, true, { UBI::MIN });
				p_Group.AddOwner(bu);
			}
			else
			{
				ASSERT(false);
			}
        }
    }
}

TaskWrapper<vector<BusinessGroup>> BusinessObjectManager::GetBusinessGroupAuthorsHierarchies(const O365IdsContainer& p_IDs, bool p_UseSQLCacheForTopLevel, YtriaTaskData p_TaskData)
{
	ASSERT(!p_IDs.empty());

	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	return safeTaskCall(YSafeCreateTask([p_IDs, p_TaskData, p_UseSQLCacheForTopLevel, that]()
	{
		vector<BusinessGroup> businessGroups;
		map<PooledString, BusinessGroup> groupsCache;

		auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("group author hierarchies"), p_IDs.size());

		if (p_UseSQLCacheForTopLevel)
		{
			auto groups = that->GetGraphCache().GetSqlCachedGroups(p_TaskData.GetOriginator(), p_IDs, true, p_TaskData);
			for (auto& group : groups)
			{
				if (p_TaskData.IsCanceled())
					break;

				logger->IncrementObjCount();
				logger->SetContextualInfo(GetDisplayNameOrId(group));

				businessGroups.push_back(group);
				auto& bg = businessGroups.back();

				that->getBusinessGroupAuthorsAcceptedHierarchy(bg, logger, p_TaskData);
				that->getBusinessGroupAuthorsRejectedHierarchy(bg, logger, p_TaskData);

				auto pageLogger = std::make_shared<MultiObjectsPageRequestLogger>(*logger);
				for (const auto& groupAcceptedId : bg.GetAuthorAcceptedGroups())
				{
					if (p_TaskData.IsCanceled())
						break;

					if (groupsCache.end() == groupsCache.find(groupAcceptedId))
					{
						groupsCache[groupAcceptedId] = that->GetGraphCache().GetGroupInCache(groupAcceptedId);
						that->getBusinessGroupMembersHierarchy(groupsCache[groupAcceptedId], groupsCache, pageLogger, p_TaskData);
					}
					else
					{
						LoggerService::Debug(_YTEXTFORMAT(L"Group already requested (%s)", groupAcceptedId.c_str()));
					}
				}

				for (const auto& groupRejectedId : bg.GetAuthorRejectedGroups())
				{
					if (p_TaskData.IsCanceled())
						break;

					if (groupsCache.end() == groupsCache.find(groupRejectedId))
					{
						groupsCache[groupRejectedId] = that->GetGraphCache().GetGroupInCache(groupRejectedId);
						that->getBusinessGroupMembersHierarchy(groupsCache[groupRejectedId], groupsCache, pageLogger, p_TaskData);
					}
					else
					{
						LoggerService::Debug(_YTEXTFORMAT(L"Group already requested (%s)", groupRejectedId.c_str()));
					}
				}

				if (p_TaskData.IsCanceled())
				{
					businessGroups.pop_back();
					break;
				}
			}
		}
		else
		{
			for (const auto& id : p_IDs)
			{
				businessGroups.emplace_back();
				auto& group = businessGroups.back();
				group.SetID(id);
				logger->SetContextualInfo(that->GetGraphCache().GetGroupContextualInfo(group.GetID()));
				that->GetGraphCache().SyncUncachedGroup(group, std::make_shared<MultiObjectsRequestLogger>(*logger), p_TaskData);

				that->getBusinessGroupAuthorsAcceptedHierarchy(group, logger, p_TaskData);
				that->getBusinessGroupAuthorsRejectedHierarchy(group, logger, p_TaskData);

				auto pageLogger = std::make_shared<MultiObjectsPageRequestLogger>(*logger);

				for (const auto& groupAcceptedId : group.GetAuthorAcceptedGroups())
				{
					if (p_TaskData.IsCanceled())
						break;

					if (groupsCache.end() == groupsCache.find(groupAcceptedId))
					{
						groupsCache[groupAcceptedId] = that->GetGraphCache().GetGroupInCache(groupAcceptedId);
						that->getBusinessGroupMembersHierarchy(groupsCache[groupAcceptedId], groupsCache, pageLogger, p_TaskData);
					}
					else
					{
						LoggerService::Debug(_YTEXTFORMAT(L"Group already requested (%s)", groupAcceptedId.c_str()));
					}
				}

				for (const auto& groupRejectedId : group.GetAuthorRejectedGroups())
				{
					if (p_TaskData.IsCanceled())
						break;

					if (groupsCache.end() == groupsCache.find(groupRejectedId))
					{
						groupsCache[groupRejectedId] = that->GetGraphCache().GetGroupInCache(groupRejectedId);
						that->getBusinessGroupMembersHierarchy(groupsCache[groupRejectedId], groupsCache, pageLogger, p_TaskData);
					}
					else
					{
						LoggerService::Debug(_YTEXTFORMAT(L"Group already requested (%s)", groupRejectedId.c_str()));
					}
				}

				if (p_TaskData.IsCanceled())
				{
					businessGroups.pop_back();
					break;
				}
			}
		}

		return businessGroups;
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData);
}

TaskWrapper<vector<BusinessEvent>> BusinessObjectManager::GetGroupBusinessEvents(const PooledString & p_GroupId, const ModuleOptions& p_Options, map<BusinessGroup, std::shared_ptr<EventListRequester>>& p_Requesters, const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

	EventPropertySet properties;
	properties.SetBodyPreview(p_Options.m_BodyPreview);
	properties.SetBodyContent(p_Options.m_BodyContent);

	BusinessGroup group;
	group.m_Id = p_GroupId;
	p_Requesters[group] = std::make_shared<EventListRequester>(properties, p_GroupId, EventListRequester::Context::Groups, p_Logger);

	auto& requester = p_Requesters.at(group);
	requester->SetCutOffDateTime(p_Options.m_CutOffDateTime);
	requester->SetCustomFilter(p_Options.m_CustomFilter);

	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, that]() {
		return requester->GetData();
	}), GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessEvent>, p_Task);
}

TaskWrapper<BusinessLifeCyclePolicies> BusinessObjectManager::GetBusinessLCP(const PooledString& p_GroupId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	web::uri_builder uri;
	if (p_GroupId.IsEmpty())
	{
		uri.append_path(Rest::LIFECYCLEPOLICIES_PATH);
	}
	else
	{
		uri.append_path(Rest::GROUPS_PATH);
		uri.append_path(p_GroupId);
		uri.append_path(Rest::LIFECYCLEPOLICIES_PATH);
	}

	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto deserializer = std::make_shared<ValueListDeserializer<BusinessLifeCyclePolicies, LifeCyclePoliciesDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(Util::CreateDefaultGraphPaginator(uri.to_uri(), deserializer, MsGraphPaginator::g_DefaultPageSize, p_Logger, httpLogger, p_Task.GetOriginator())
		.Paginate(GetMSGraphSession(httpLogger->GetSessionType()), p_Task)
		.Then([deserializer, that]()
		{
			ASSERT(deserializer->GetData().size() <= 1);
			if (!deserializer->GetData().empty())
				return *deserializer->GetData().begin();
			else
				return BusinessLifeCyclePolicies(LifeCyclePolicies());
		}
	), GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ObjectErrorHandler<BusinessLifeCyclePolicies>, p_Task);
}

TaskWrapper<vector<BusinessChannel>> BusinessObjectManager::GetBusinessChannels(const PooledString& p_GroupId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

	auto requester = std::make_shared<ChannelsRequester>(p_GroupId, p_Logger);
	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_TaskData).Then([requester, p_TaskData, that]()
    {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessChannel>, p_TaskData);
}

TaskWrapper<vector<BusinessGroupSettingTemplate>> BusinessObjectManager::GetBusinessGroupSettingTemplates(YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

    auto requester = std::make_shared<GroupSettingTemplatesRequester>(std::make_shared<BasicRequestLogger>(_T("group setting templates")));
	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_TaskData).Then([requester, p_TaskData, that] () {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessGroupSettingTemplate>, p_TaskData);
}

TaskWrapper<vector<BusinessGroupSetting>> BusinessObjectManager::GetBusinessGroupSettings(YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

	auto logger = std::make_shared<BasicPageRequestLogger>(_T("group settings"));
    auto requester = std::make_shared<GroupSettingsRequester>(logger);
	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_TaskData).Then([requester, p_TaskData, that] () {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessGroupSetting>, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::UpdateGroupSetting(const BusinessGroupSetting& p_GroupSetting, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->UpdateGroupSetting(BusinessGroupSetting::ToGroupSetting(p_GroupSetting), httpLogger, p_TaskData).Then([that](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::UpdateGroupSetting(const PooledString& p_GroupId, const BusinessGroupSetting& p_GroupSetting, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->UpdateGroupSetting(p_GroupId, BusinessGroupSetting::ToGroupSetting(p_GroupSetting), httpLogger, p_TaskData).Then([that](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::CreateGroupSetting(const BusinessGroupSetting& p_GroupSetting, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->CreateGroupSetting(BusinessGroupSetting::ToGroupSetting(p_GroupSetting), httpLogger, p_TaskData).Then([that](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::CreateGroupSetting(const PooledString& p_GroupId, const BusinessGroupSetting& p_GroupSetting, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->CreateGroupSetting(p_GroupId, BusinessGroupSetting::ToGroupSetting(p_GroupSetting), httpLogger, p_TaskData).Then([that](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::DeleteGroupSetting(const PooledString& p_GroupSettingId, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->DeleteGroupSetting(p_GroupSettingId, httpLogger, p_TaskData).Then([that](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::DeleteGroupSetting(const PooledString& p_GroupSettingId, const PooledString& p_GroupId, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->DeleteGroupSetting(p_GroupSettingId, p_GroupId, httpLogger, p_TaskData).Then([that](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<vector<BusinessSite>> BusinessObjectManager::GetAllBusinessSites(bool p_FromCache, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

	ASSERT(!p_FromCache); // Finish it if you wanna use it
	//if (!p_FromCache)
	{
		auto rbacProps = RbacRequiredPropertySet<BusinessSite>(RttrPropertySet<Site>(PropertySetType::List).GetPropertySet(), *that->GetSapio365Session());
		auto requester = std::make_shared<SitesRequester>(rbacProps, p_Logger);

		return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, p_Task, that]() {
			that->GetGraphCache().AddSites(requester->GetData());
			return requester->GetData();
		}), GetMSGraphSession(Sapio365Session::APP_SESSION), ListErrorHandler<BusinessSite>, p_Task);
	}
	/*else
	{
		auto cachedSites = GetGraphCache().GetSqlCachedSites(p_Task, CachedObjectsSqlEngine::ObjLevel::Full);
		that->GetGraphCache().Add(cachedSites, CachedObjectsSqlEngine::Full, false);
		return pplx::task_from_result(cachedSites);
	}*/
}

TaskWrapper<BusinessSite> BusinessObjectManager::GetRootBusinessSite(const PooledString & p_GroupId, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto requester = std::make_shared<GroupRootSiteRequester>(p_GroupId, p_Logger);

	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, p_Task, that, logger = p_Logger] {
		that->GetGraphCache().AddSite(requester->GetData());
		auto subLogger = std::make_shared<MultiObjectsPageRequestLogger>(*logger);
		subLogger->SetLogMessage(_T("subsites"));
		auto businessSubSites = that->GetSubSites(requester->GetData().GetID(), subLogger, p_Task).GetTask().get();
		requester->GetData().SetSubSites(businessSubSites);
		return requester->GetData();
	}), GetMSGraphSession(Sapio365Session::APP_SESSION), Util::ObjectErrorHandler<BusinessSite>, p_Task);
}

TaskWrapper<vector<BusinessList>> BusinessObjectManager::GetBusinessLists(const PooledString& p_SiteId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessList, GraphListDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetLists(deserializer, p_SiteId, p_Logger, httpLogger, p_Task).Then([deserializer, p_Task, that]()
    {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessList>, p_Task);
}

TaskWrapper<BusinessList> BusinessObjectManager::GetBusinessList(const PooledString& p_SiteId, const PooledString& p_ListId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<GraphListDeserializer>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetList(deserializer, p_SiteId, p_ListId, p_Logger, httpLogger, p_Task).Then([deserializer, p_Task, that]
    {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::ObjectErrorHandler<BusinessList>, p_Task);
}

TaskWrapper<BusinessList> BusinessObjectManager::GetBusinessList(const PooledString& p_SiteId, const PooledString& p_ListId, const vector<wstring>& p_FieldsToRetrieve, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<GraphListDeserializer>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetList(deserializer, p_SiteId, p_ListId, p_FieldsToRetrieve, p_Logger, httpLogger, p_Task).Then([deserializer, p_Task, that]
    {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::ObjectErrorHandler<BusinessList>, p_Task);
}

TaskWrapper<vector<BusinessListItem>> BusinessObjectManager::GetBusinessListItems(const PooledString& p_SiteId, const PooledString& p_ListId, const vector<wstring>& p_FieldsToRetrieve, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessListItem, ListItemDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetListItems(deserializer, p_SiteId, p_ListId, p_FieldsToRetrieve, p_Logger, httpLogger, p_Task).Then([deserializer, p_Task, that]() {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessListItem>, p_Task);
}

TaskWrapper<BusinessDrive> BusinessObjectManager::GetDefaultSiteDrive(const PooledString& p_SiteId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<DriveDeserializer>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetDefaultSiteDrive(deserializer, p_SiteId, p_Logger, httpLogger, p_Task).Then([deserializer, that]()
    {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::ObjectErrorHandler<BusinessDrive>, p_Task);
}

TaskWrapper<vector<BusinessSite>> BusinessObjectManager::GetSubSites(const PooledString& p_SiteId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto factory		= std::make_unique<SiteDeserializerFactory>(GetSapio365Session());
	auto deserializer	= std::make_shared<ValueListDeserializer<BusinessSite, SiteDeserializer>>(std::move(factory));
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetSubSites(deserializer, p_SiteId, p_Logger, httpLogger, p_Task).Then([deserializer, p_Task, that, p_Logger]()
    {
        vector<BusinessSite> businessSites;
        businessSites.reserve(deserializer->GetData().size());
        for (auto& subsite : deserializer->GetData())
        {
			auto subSites = that->GetSubSites(subsite.GetID(), p_Logger, p_Task).GetTask().get();
            
            businessSites.push_back(std::move(subsite));
            businessSites.back().SetSubSites(subSites);
        }

        return businessSites;
    }), GetMSGraphSession(httpLogger->GetSessionType()), p_Task);
}

// Enable this flag (and delete obsolete code) as soon as acceptedSenders/rejectedSenders requests do accept $select query
// (see MSGraphSession::GetGroupAuthorsAccepted / MSGraphSession::GetGroupAuthorsRejected)
// Right now, they don't and we don't have enough information in the retrieved objects.
// Until Microsoft fix it, we must manually synchronize the objects (see below)
#define ACCEPTEDSENDERS_REQUEST_ACCEPTS_SELECT 0

void BusinessObjectManager::getBusinessGroupAuthorsAcceptedHierarchy(BusinessGroup& p_Group, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	p_Logger->SetLogMessage(_T("accepted authors"));
	const auto authorsAccepted = [this, &p_Group, &p_TaskData, &p_Logger]
	{
		std::shared_ptr<BusinessObjectManager> that(shared_from_this());


#if ACCEPTEDSENDERS_REQUEST_ACCEPTS_SELECT
		auto factory = std::make_unique<DirectoryObjectVariantDeserializerFactory>(that->GetSapio365Session());
		auto deserializer = std::make_shared<ValueListDeserializer<BusinessDirectoryObjectVariant, DirectoryObjectVariantDeserializer>>(std::move(factory));
		// Need to request RBAC props?
		auto properties = MSGraphSession::GetUsersGroupsAndOrgContactsSelectQueryForCache();
		properties = GetSapio365Session()->WithRBACRequiredProperties<BusinessUser>(properties);
		properties = GetSapio365Session()->WithRBACRequiredProperties<BusinessGroup>(properties);
		return safeTaskCall(GetMSGraphSession(Sapio365Session::USER_SESSION)->GetGroupAuthorsAccepted(deserializer, p_Group.GetID(), properties, p_Logger, p_TaskData).Then([deserializer, that]()
#else
#ifdef _DEBUG
		auto factory = std::make_unique<DirectoryObjectVariantDeserializerFactory>(that->GetSapio365Session(), true);
#else
		auto factory = std::make_unique<DirectoryObjectVariantDeserializerFactory>(that->GetSapio365Session());
#endif
		auto deserializer = std::make_shared<ValueListDeserializer<BusinessDirectoryObjectVariant, DirectoryObjectVariantDeserializer>>(std::move(factory));
		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
		return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetGroupAuthorsAccepted(deserializer, p_Group.GetID(), {}, p_Logger, httpLogger, p_TaskData).Then([deserializer, that]()
#endif
		{
			return std::move(deserializer->GetData());
		}), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessDirectoryObjectVariant>, p_TaskData).get();
	}();

	p_Group.SetLastRequestTime(p_TaskData.GetLastRequestOn());

    if (authorsAccepted.size() == 1 && authorsAccepted[0].GetError())
    {
        p_Group.SetAuthorsAcceptedError(authorsAccepted[0].GetError());
    }
    else
    {
        for (const auto& authorAccepted : authorsAccepted)
        {
			if (p_TaskData.IsCanceled())
				break;

            if (authorAccepted.IsBusinessUser())
            {
				bool addUser = true;

#if ACCEPTEDSENDERS_REQUEST_ACCEPTS_SELECT
				// See comment below, we cannot use this object directly.
				/*const auto& bu = authorAccepted.AsBusinessUser();
				GetGraphCache().Add(bu);*/
#endif
				// There's a bug in graph, the acceptedSenders/rejectedSenders requests gives a user type for org contacts.
				// This means, for now, we cannot rely of this information.
				// To find out if it's a user or an org contact, we have no other solution than manually syncing it.

				auto user = authorAccepted.AsBusinessUser();
				p_Logger->SetContextualInfo(GetGraphCache().GetUserContextualInfo(user.GetID()));
				GetGraphCache().SyncUncachedUser(user, p_Logger, p_TaskData, false); // Manually sync the user
				if (user.GetError() || user.GetID().IsEmpty())
				{
					// try Org Contact if error
					BusinessOrgContact orgContact;
					orgContact.SetID(user.GetID());
					GetGraphCache().SyncUncachedOrgContact(orgContact, p_Logger, p_TaskData);

					if (orgContact.GetError() || orgContact.GetID().IsEmpty())
					{
						ASSERT(false);
						// It's not a contact either, keep the user with error.
						user.SetID(authorAccepted.AsBusinessUser().GetID());
						user.SetDisplayName(authorAccepted.AsBusinessUser().GetDisplayName());
					}
					else
					{
						p_Group.AddAuthorAccepted(orgContact);
						addUser = false;
					}
				}

				if (addUser)
					p_Group.AddAuthorAccepted(user);
            }
			else if (authorAccepted.IsBusinessOrgContact())
			{
#if ACCEPTEDSENDERS_REQUEST_ACCEPTS_SELECT
				const auto& boc = authorAccepted.AsBusinessOrgContact();
				GetGraphCache().Add(boc);
#else
				auto boc = authorAccepted.AsBusinessOrgContact();
				GetGraphCache().SyncUncachedOrgContact(boc, p_Logger, p_TaskData); // Manually sync the orgContact
#endif
				p_Group.AddAuthorAccepted(boc);
			}
			else if (authorAccepted.IsBusinessGroup())
            {
#if ACCEPTEDSENDERS_REQUEST_ACCEPTS_SELECT
				const auto& group = authorAccepted.AsBusinessGroup();
				GetGraphCache().AddGroup(group);
#else
				auto group = authorAccepted.AsBusinessGroup();
				p_Logger->SetContextualInfo(GetGraphCache().GetGroupContextualInfo(group.GetID()));
				GetGraphCache().SyncUncachedGroup(group, p_Logger, p_TaskData); // Manually sync the group
#endif
				p_Group.AddAuthorAccepted(group);
            }
            else
            {
                ASSERT(false);
            }
        }
    }
}

void BusinessObjectManager::getBusinessGroupAuthorsRejectedHierarchy(BusinessGroup& p_Group, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	p_Logger->SetLogMessage(_T("rejected authors"));
	const auto authorsRejected = [this, &p_Group, &p_TaskData, &p_Logger]
	{

		std::shared_ptr<BusinessObjectManager> that(shared_from_this());
#if ACCEPTEDSENDERS_REQUEST_ACCEPTS_SELECT
		auto factory = std::make_unique<DirectoryObjectVariantDeserializerFactory>(that->GetSapio365Session());
		auto deserializer = std::make_shared<ValueListDeserializer<BusinessDirectoryObjectVariant, DirectoryObjectVariantDeserializer>>(std::move(factory));
		// Need to request RBAC props?
		auto properties = MSGraphSession::GetUsersGroupsAndOrgContactsSelectQueryForCache();
		properties = GetSapio365Session()->WithRBACRequiredProperties<BusinessUser>(properties);
		properties = GetSapio365Session()->WithRBACRequiredProperties<BusinessGroup>(properties);
		return safeTaskCall(GetMSGraphSession(Sapio365Session::USER_SESSION)->GetGroupAuthorsRejected(deserializer, p_Group.GetID(), properties, p_Logger, p_TaskData).Then([deserializer, that]()
#else
#ifdef _DEBUG
		auto factory = std::make_unique<DirectoryObjectVariantDeserializerFactory>(that->GetSapio365Session(), true);
#else
		auto factory = std::make_unique<DirectoryObjectVariantDeserializerFactory>(that->GetSapio365Session());
#endif
		auto deserializer = std::make_shared<ValueListDeserializer<BusinessDirectoryObjectVariant, DirectoryObjectVariantDeserializer>>(std::move(factory));

		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
		return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetGroupAuthorsRejected(deserializer, p_Group.GetID(), {}, p_Logger, httpLogger, p_TaskData).Then([deserializer, that]()
#endif
		{
			return std::move(deserializer->GetData());
		}), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessDirectoryObjectVariant>, p_TaskData).get();
	}();

    if (authorsRejected.size() == 1 && authorsRejected[0].GetError())
    {
        p_Group.SetAuthorsRejectedError(authorsRejected[0].GetError());
    }
    else
    {
        for (const auto& authorRejected : authorsRejected)
        {
			if (p_TaskData.IsCanceled())
				break;

			if (authorRejected.IsBusinessUser())
			{
				bool addUser = true;

#if ACCEPTEDSENDERS_REQUEST_ACCEPTS_SELECT
				// See comment below, we cannot use this object directly.
				/*const auto& bu = authorRejected.AsBusinessUser();
				GetGraphCache().Add(bu);*/
#endif
				// There's a bug in graph, the acceptedSenders/rejectedSenders requests gives a user type for org contacts.
				// This means, for now, we cannot rely of this information.
				// To find out if it's a user or an org contact, we have no other solution than manually syncing it.

				auto user = authorRejected.AsBusinessUser();
				p_Logger->SetContextualInfo(GetGraphCache().GetUserContextualInfo(user.GetID()));
				GetGraphCache().SyncUncachedUser(user, p_Logger, p_TaskData, false); // Manually sync the user
				if (user.GetError() || user.GetID().IsEmpty())
				{
					// try Org Contact if error
					BusinessOrgContact orgContact;
					orgContact.SetID(user.GetID());
					GetGraphCache().SyncUncachedOrgContact(orgContact, p_Logger, p_TaskData);

					if (orgContact.GetError() || orgContact.GetID().IsEmpty())
					{
						ASSERT(false);
						// It's not a contact either, keep the user with error.
						user.SetID(authorRejected.AsBusinessUser().GetID());
						user.SetDisplayName(authorRejected.AsBusinessUser().GetDisplayName());
					}
					else
					{
						p_Group.AddAuthorRejected(orgContact);
						addUser = false;
					}
				}

				if (addUser)
					p_Group.AddAuthorRejected(user);
			}
			else if (authorRejected.IsBusinessOrgContact())
			{
#if ACCEPTEDSENDERS_REQUEST_ACCEPTS_SELECT
				const auto& orgContact = authorRejected.AsBusinessOrgContact();
				GetGraphCache().Add(orgContact);
#else
				auto orgContact = authorRejected.AsBusinessOrgContact();
				GetGraphCache().SyncUncachedOrgContact(orgContact, p_Logger, p_TaskData); // Manually sync the orgContact
#endif
				p_Group.AddAuthorRejected(orgContact);
			}
			else if (authorRejected.IsBusinessGroup())
			{
#if ACCEPTEDSENDERS_REQUEST_ACCEPTS_SELECT
				const auto& group = authorRejected.AsBusinessGroup();
				GetGraphCache().AddGroup(group);
#else
				auto group = authorRejected.AsBusinessGroup();
				p_Logger->SetContextualInfo(GetGraphCache().GetGroupContextualInfo(group.GetID()));
				GetGraphCache().SyncUncachedGroup(group, p_Logger, p_TaskData); // Manually sync the group
#endif
				p_Group.AddAuthorRejected(group);
			}
			else
			{
				ASSERT(false);
			}
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////
// USER
///////////////////////////////////////////////////////////////////////////////////

TaskWrapper<vector<BusinessUser>> BusinessObjectManager::MoreInfoBusinessUsers(const vector<BusinessUser>& p_BusinessUsers, bool p_ReturnFullListOnCancel, YtriaTaskData p_Task)
{
	return getBusinessUsersIndividualRequests_impl(p_BusinessUsers, UserAdditionalRequest::RequestMailboxSettingsForNonGuest | UserAdditionalRequest::RequestDriveForNonGuest | UserAdditionalRequest::RequestManager, p_ReturnFullListOnCancel, p_Task);
}

TaskWrapper<vector<BusinessUser>> BusinessObjectManager::GetBusinessUsersIndividualRequests(const vector<BusinessUser>& p_BusinessUsers, YtriaTaskData p_TaskData)
{
	return getBusinessUsersIndividualRequests_impl(p_BusinessUsers, 0, true, p_TaskData);
}

TaskWrapper<vector<BusinessUser>> BusinessObjectManager::getBusinessUsersIndividualRequests_impl(const vector<BusinessUser>& p_BusinessUsers, uint32_t p_UserAdditionnalInfoFlags, bool p_ReturnFullListOnCancel, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	return safeTaskCall(
		YSafeCreateTask([p_BusinessUsers, p_Task, p_UserAdditionnalInfoFlags, p_ReturnFullListOnCancel, that]()
	{
		vector<BusinessUser> businessUsers;

		auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("users"), p_BusinessUsers.size());
		for (const auto& bu : p_BusinessUsers)
		{
			logger->IncrementObjCount();
			logger->SetContextualInfo(GetDisplayNameOrId(bu));

			if (p_Task.IsCanceled())
			{
				if (p_ReturnFullListOnCancel)
				{
					businessUsers.emplace_back(bu);
					businessUsers.back().SetFlags(BusinessObject::CANCELED);
					continue;
				}
				else
				{
					break;
				}
			}

			if (O365Grid::IsTemporaryCreatedObjectID(bu.GetID()))
			{
				businessUsers.emplace_back(bu);
				continue;
			}

			// FIXME: Kindof bad design
			const bool isMoreRequest = p_UserAdditionnalInfoFlags != 0;

			/* Beta API */
			static vector<rttr::property> betaSyncProperties;
			if (isMoreRequest && betaSyncProperties.empty())
			{
				User dummy;
				betaSyncProperties = dummy.GetBetaSyncOnlyProperties(dummy);
			}

			const auto properties = isMoreRequest
				? std::unordered_map<std::vector<UBI>, std::vector<rttr::property>>{ { { UBI::MIN, UBI::LIST, UBI::SYNCV1 }, UserFullPropertySet().GetPropertySet() }, { { UBI::SYNCBETA }, betaSyncProperties } }
				: std::unordered_map<std::vector<UBI>, std::vector<rttr::property>>{ { { UBI::MIN, UBI::LIST }, UserListFullPropertySet().GetPropertySet() } };
			BusinessUser businessUser(bu);
			UserMacroRequest umr(businessUser, properties, p_UserAdditionnalInfoFlags, that, logger, p_Task);

			if (umr.Run()) // Not Canceled
			{
				ASSERT(!p_Task.IsCanceled());
				if (isMoreRequest)
				{
					// FIXME: new modular cache
					businessUser.SetFlags(businessUser.GetFlags() | BusinessObject::MORE_LOADED);
				}
				businessUsers.emplace_back(businessUser);
			}
			else
			{
				ASSERT(p_Task.IsCanceled());
				if (p_ReturnFullListOnCancel)
				{
					businessUsers.emplace_back(businessUser);
					businessUsers.back().SetFlags(BusinessObject::CANCELED);
				}
			}
		}

		return businessUsers;
	}), GetMSGraphSession(Sapio365Session::USER_SESSION), p_Task);
}

TaskWrapper<vector<BusinessGroup>> BusinessObjectManager::getBusinessGroupsIndividualRequests_impl(const vector<BusinessGroup>& p_BusinessGroups, bool p_UseSyncProperties, boost::YOpt<BusinessLifeCyclePolicies> p_LCP, uint32_t p_GroupAdditionnalInfoFlags, bool p_ReturnFullListOnCancel, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	return safeTaskCall(
		YSafeCreateTask([p_BusinessGroups, p_UseSyncProperties, p_GroupAdditionnalInfoFlags, p_LCP, p_Task, p_ReturnFullListOnCancel, that]()
		{
			vector<BusinessGroup> businessGroups;
			auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("groups"), p_BusinessGroups.size());
			for (const auto& bg : p_BusinessGroups)
			{
				logger->IncrementObjCount();
				logger->SetContextualInfo(GetDisplayNameOrId(bg));

				if (p_Task.IsCanceled())
				{
					if (p_ReturnFullListOnCancel)
					{
						businessGroups.emplace_back(bg);
						businessGroups.back().SetFlags(BusinessObject::CANCELED);
						continue;
					}
					else
					{
						break;
					}
				}

				if (O365Grid::IsTemporaryCreatedObjectID(bg.GetID()))
				{
					businessGroups.emplace_back(bg);
					continue;
				}

				BusinessGroup businessGroup(bg);

				const auto props = that->m_PropertyLists->GetPropertySets(bg, p_UseSyncProperties);
				GroupMacroRequest gmr(businessGroup, props, p_LCP, p_GroupAdditionnalInfoFlags, that, logger, p_Task);
				if (gmr.Run()) // Not Canceled
				{
					ASSERT(!p_Task.IsCanceled());

					if (props.size() > 1 // we requested some sync props
						|| businessGroup.IsFromRecycleBin())
					{
						// FIXME: New modular cache
						businessGroup.SetFlags(businessGroup.GetFlags() | BusinessObject::MORE_LOADED);
					}

					businessGroups.emplace_back(businessGroup);
				}
				else
				{
					ASSERT(p_Task.IsCanceled());
				}
			}

			return businessGroups;
		}), GetMSGraphSession(Sapio365Session::USER_SESSION), p_Task);
}

TaskWrapper<vector<BusinessGroup>> BusinessObjectManager::MoreInfoBusinessGroups(const vector<BusinessGroup>& p_BusinessGroups, bool p_ReturnFullListOnCancel, YtriaTaskData p_Task, boost::YOpt<BusinessLifeCyclePolicies> p_LCP)
{
	ASSERT(std::none_of(p_BusinessGroups.begin(), p_BusinessGroups.end(), [](const BusinessGroup& bg) { return O365Grid::IsTemporaryCreatedObjectID(bg.GetID()); }));
	const auto flags = GroupAdditionalRequest::RequestOnBehalf
					| GroupAdditionalRequest::RequestTeam
					| GroupAdditionalRequest::RequestGroupSettings
					| GroupAdditionalRequest::RequestGroupLCP
					| GroupAdditionalRequest::RequestOwners
					| GroupAdditionalRequest::RequestMembersCount
					| GroupAdditionalRequest::RequestDrive
					| GroupAdditionalRequest::RequestIsYammer;
	return getBusinessGroupsIndividualRequests_impl(p_BusinessGroups, true, p_LCP, flags, p_ReturnFullListOnCancel, p_Task);
}

TaskWrapper<vector<BusinessGroup>> BusinessObjectManager::MoreInfoDeletedBusinessGroups(const vector<BusinessGroup>& p_DeletedBusinessGroups, bool p_ReturnFullListOnCancel, YtriaTaskData p_Task)
{
	ASSERT(std::none_of(p_DeletedBusinessGroups.begin(), p_DeletedBusinessGroups.end(), [](const BusinessGroup& bg) { return O365Grid::IsTemporaryCreatedObjectID(bg.GetID()); }));
	return getBusinessGroupsIndividualRequests_impl(p_DeletedBusinessGroups, false, boost::none, /*GroupAdditionalRequest::RequestOnBehalf | */GroupAdditionalRequest::RequestTeam | /*GroupAdditionalRequest::RequestGroupSettings |*/ GroupAdditionalRequest::RequestGroupLCP/* | GroupAdditionalRequest::RequestOwners | GroupAdditionalRequest::RequestMembersCount | BusinessObjectManager::GroupAdditionalRequest::RequestDrive*/, p_ReturnFullListOnCancel, p_Task);
}

TaskWrapper<vector<BusinessGroup>> BusinessObjectManager::GetBusinessGroupsIndividualRequests(const vector<BusinessGroup>& p_BusinessGroups, YtriaTaskData p_Task)
{
	return getBusinessGroupsIndividualRequests_impl(p_BusinessGroups, false, boost::YOpt<BusinessLifeCyclePolicies>(), 0, true, p_Task);
}

TaskWrapper<vector<BusinessUser>> BusinessObjectManager::GetBusinessUsersParentGroups(const O365IdsContainer& p_IDs, bool p_UseSQLCacheForUsers, YtriaTaskData p_TaskData)
{
	ASSERT(!p_IDs.empty());

	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	return safeTaskCall(YSafeCreateTask([p_TaskData, p_IDs, p_UseSQLCacheForUsers, that]()
    {
		// Should we sync like this? Probably overkill for only a few groups.
		//{
		//	auto logger = std::make_shared<BasicPageRequestLogger>(_T("users"));
		//	that->GetGraphCache().GetUpToDateGroups<CachedGroup>({}, logger, GraphCache::PreferredMode::DELTASYNC, p_TaskData).get();
		//}

        vector<BusinessUser> businessUsers;

		auto logger = std::make_shared<MultiObjectsRequestLogger>(_YTEXT("parent groups"), p_IDs.size());

		O365IdsContainer allParentGroupIds;
		auto processUserInLoop = [&p_TaskData, &that, &logger, &businessUsers, &allParentGroupIds](BusinessUser& p_User) mutable
		{
			businessUsers.emplace_back(p_User);
			auto& user = businessUsers.back();

			{
				logger->SetLogMessage(_T("parent groups"));

				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());

				try
				{
					auto parentGroupIds = that->GetMSGraphSession(httpLogger->GetSessionType())->GetMemberGroups(p_User.GetID(), logger, httpLogger, p_TaskData, false).get();
					user.SetParentGroups(parentGroupIds);
					allParentGroupIds.insert(parentGroupIds.begin(), parentGroupIds.end());
				}
				catch (const RestException& e)
				{
					user.SetError(HttpError(e));
				}
				catch (const std::exception& e)
				{
					user.SetError(SapioError(e));
				}
			}

			{
				logger->SetLogMessage(_T("direct parent groups"));

				// Using Variant as memberOf also returns Directory Roles
				auto factory = std::make_unique<DirectoryObjectVariantDeserializerFactory>(that->GetSapio365Session());
				auto deserializer = std::make_shared<ValueListDeserializer<BusinessDirectoryObjectVariant, DirectoryObjectVariantDeserializer>>(std::move(factory));

				// FIXME: new modular cache, remove required props
				const auto props = RbacRequiredPropertySet<BusinessGroup>(CachedGroupPropertySet().GetPropertySet(), *that->GetSapio365Session()).GetPropertySet();
				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
				
				try
				{
					that->GetMSGraphSession(httpLogger->GetSessionType())->GetMemberOf(deserializer, p_User.GetID(), props, logger, httpLogger, p_TaskData).get();

					vector<PooledString> parentGroupIds;
					for (auto& object : deserializer->GetData())
					{
						if (object.IsBusinessGroup())
							parentGroupIds.push_back(object.GetID());
					}

					user.SetFirstLevelParentGroups(parentGroupIds);
					allParentGroupIds.insert(parentGroupIds.begin(), parentGroupIds.end());
				}
				catch (const RestException& e)
				{
					user.SetError(HttpError(e));
				}
				catch (const std::exception& e)
				{
					user.SetError(SapioError(e));
				}
			}

			user.SetLastRequestTime(p_TaskData.GetLastRequestOn());
		};

		Util::ProcessSubUserItems(processUserInLoop, p_IDs, Origin::User, that->GetGraphCache(), p_UseSQLCacheForUsers, logger, p_TaskData);

		// Ensure all parent groups in cache
		if (!allParentGroupIds.empty())
		{
			that->GetGraphCache().GetSqlCachedGroups(p_TaskData.GetOriginator(), allParentGroupIds, false, p_TaskData);

			if (!p_TaskData.IsCanceled())
			{
				// FIXME: This logger creates unwanted display
							//auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("parent group"), parentGroupIds.size());
				auto logger = std::make_shared<BasicRequestLogger>(_T("parent group"));

				// FIXME: Should we use delta sync here?
				// Ensure no group is missing...
				for (auto& id : allParentGroupIds)
				{
					BusinessGroup bg;
					bg.SetID(id);
					that->GetGraphCache().SyncUncachedGroup(bg, logger, p_TaskData, false);
				}
				ASSERT(std::all_of(allParentGroupIds.begin(), allParentGroupIds.end(), [&that](const auto& id) { return id == that->GetGraphCache().GetGroupInCache(id).GetID(); }));
			}
		}

        return businessUsers;
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData);
}

#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
TaskWrapper<vector<BusinessUser>> BusinessObjectManager::GetBusinessUsersManagerHierarchy(const O365IdsContainer& p_IDs, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	return safeTaskCall
	(
		YSafeCreateTask()
		(
			[p_TaskData, p_IDs, that]()
			{
				vector<BusinessUser> usersWithHierarchy;

				// 1. Get business users from cache
				auto cachedUsers = that->GetGraphCache().SyncAllUsersOnceAndGetMap(p_TaskData).get();

				if (p_IDs.empty()) // Want all users
				{
					for (const auto& cachedUser : cachedUsers)
						usersWithHierarchy.emplace_back(cachedUser.second);
				}
				else
				{
					for (const auto& id : p_IDs)
					{
						auto userItt = cachedUsers.find(id);
						if (userItt != cachedUsers.end())
						{
							if (!userItt->second.GetId().IsEmpty())
								usersWithHierarchy.emplace_back(userItt->second);
						}
					}
				}

				// 2. Get business users managers and direct reports
				for (auto& thisUser : usersWithHierarchy)
				{
					if (p_TaskData.IsCanceled())
						return usersWithHierarchy;

					// FIXME: handle logging
					/*ObjectListFriendlyPageRequestLogger logger(_T("user manager"), counter);
					logger.Log(web::uri(), 0, taskData.GetOriginator());*/

					/////////////////////////////////////////////////
					// Get manager
                    auto deserializer = std::make_shared<UserDeserializer>(that->GetSapio365Session());
					const auto properties = that->GetSapio365Session()->WithRBACRequiredProperties<BusinessUser>(CachedUserPropertySet().GetPropertySet());
					BusinessUser manager = safeTaskCall
					(
						that->GetMSGraphSession(Sapio365Session::USER_SESSION)->GetUserManager(deserializer, thisUser.GetID(), properties, p_TaskData).then
						(
							[deserializer, that]
							{
								that->GetGraphCache().Add(deserializer->GetData(), CachedObjectsSqlEngine::Minimal, true);
								return deserializer->GetData();
							}
						),
						that->GetMSGraphSession(Sapio365Session::USER_SESSION),
						[that](const std::exception_ptr& p_ExceptPtr)
						{
							ASSERT(p_ExceptPtr);
							if (p_ExceptPtr)
							{
								try
								{
									std::rethrow_exception(p_ExceptPtr);
								}
								catch (const RestException)
								{
									return BusinessUser();
								}
								catch (const std::exception)
								{
									return BusinessUser();
								}
							}

							ASSERT(false);
							return BusinessUser();
						}
					).get();

					// Error????
					if (!manager.m_Id.IsEmpty())
						thisUser.SetManager(manager);

					/////////////////////////////////////////////////
					// Get direct reports
					auto factory					= std::make_unique<UserDeserializerFactory>(that->GetSapio365Session());
                    auto directReportsDeserializer	= std::make_shared<ValueListDeserializer<BusinessUser, UserDeserializer>>(std::move(factory));
					vector<BusinessUser> directReports = safeTaskCall
					(
						that->GetMSGraphSession(Sapio365Session::USER_SESSION)->GetUserDirectReports(directReportsDeserializer, thisUser.GetID(), properties, p_TaskData).then
						(
							[directReportsDeserializer, that]()
							{
								that->GetGraphCache().Add(directReportsDeserializer->GetData(), CachedObjectsSqlEngine::Minimal, true, boost::none);
								return directReportsDeserializer->GetData();
							}
						),
						that->GetMSGraphSession(Sapio365Session::USER_SESSION),
						[that](const std::exception_ptr& p_ExceptPtr)
						{
							ASSERT(p_ExceptPtr);
							if (p_ExceptPtr)
							{
								try
								{
									std::rethrow_exception(p_ExceptPtr);
								}
								catch (const RestException)
								{
									return vector<BusinessUser>();
								}
								catch (const std::exception)
								{
									return vector<BusinessUser>();
								}
							}

							ASSERT(false);
							return vector<BusinessUser>();
						}
					).get();

					// Error????
					for(const BusinessUser& user : directReports)
						if (!user.m_Id.IsEmpty())
							thisUser.AddDirectReport(user);
				}

				return usersWithHierarchy;
			}
		),
		GetMSGraphSession(Sapio365Session::USER_SESSION)
	);
}
#endif

TaskWrapper<vector<BusinessUser>> BusinessObjectManager::GetBusinessUsersRecycleBin(const O365IdsContainer& p_IDs, const IPropertySetBuilder& p_PropertySet, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

	auto logger = std::make_shared<BasicPageRequestLogger>(_T("deleted users"));
	auto requester = std::make_shared<DeletedUserListRequester>(p_PropertySet, logger);
	return safeTaskCall(requester->Send(GetSapio365Session(), p_TaskData).Then([p_IDs, requester, that]() {
		auto& deletedUsers = requester->GetData();
		that->GetGraphCache().ClearDeletedUsers();
		that->GetGraphCache().AddDeletedUsers(deletedUsers);
		that->GetGraphCache().SetAllDeletedUsersSynced();

		if (p_IDs.empty())
			return deletedUsers;

		vector<BusinessUser> deletedBusinessUsers;
		deletedBusinessUsers.reserve(p_IDs.size());
		for (auto& id : p_IDs)
		{
			const auto it = std::find_if(deletedUsers.begin(), deletedUsers.end(), [&id](const BusinessUser& p_User) {
				return p_User.m_Id == id;
			});
			if (deletedUsers.end() != it)
			{
				deletedBusinessUsers.emplace_back(*it);
			}
			else
			{
				ASSERT(false);

				// The list request result doesn't contain the requested user (whereas it contains all the users).
				// So we add the user to the list and put an error.
				deletedBusinessUsers.emplace_back();
				deletedBusinessUsers.back().SetID(id);
				deletedBusinessUsers.back().SetError(SapioError(YtriaTranslate::DoError(BusinessObjectManager_GetBusinessUsersRecycleBin_1, _YLOC("Requested deleted user doesn't exist."),_YR("Y2405")).c_str(), 0));
			}
		}
		return deletedBusinessUsers;
	}), GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessUser>, p_TaskData);
}

vector<Group> BusinessObjectManager::toJsonGroups(const vector<CachedGroup>& p_CachedGroups) const
{
    vector<Group> groups;
    for (const auto& cachedGroup : p_CachedGroups)
        groups.push_back(BusinessGroup::ToGroup(cachedGroup));
    return groups;
}

///////////////////////////////////////////////////////////////////////////////////
// CONTACT
///////////////////////////////////////////////////////////////////////////////////

TaskWrapper<vector<BusinessContact>> BusinessObjectManager::GetBusinessContacts(const PooledString& p_Id, const std::shared_ptr<IPageRequestLogger>& p_PageLogger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessContact, ContactDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetContacts(deserializer, p_Id, p_PageLogger, httpLogger, p_TaskData).Then([that, deserializer]() {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessContact>, p_TaskData);
}

TaskWrapper<vector<BusinessContact>> BusinessObjectManager::GetBusinessContacts(const PooledString& p_Id, const PooledString& p_ContactFolderId, const std::shared_ptr<IPageRequestLogger>& p_PageLogger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessContact, ContactDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetContacts(deserializer, p_Id, p_ContactFolderId, p_PageLogger, httpLogger, p_TaskData).Then([that, deserializer] () {
		return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

TaskWrapper<vector<BusinessContactFolder>> BusinessObjectManager::GetBusinessContactFoldersHierarchy(const PooledString& p_UserId, const std::shared_ptr<IPageRequestLogger>& p_PageLogger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

    auto deserializer = std::make_shared<ValueListDeserializer<BusinessContactFolder, ContactFolderDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetContactFolders(deserializer, p_UserId, p_PageLogger, httpLogger, p_TaskData).Then([httpLogger, deserializer, p_UserId, p_TaskData, that, p_PageLogger]() {
		vector<pplx::task<void>> contactFoldersTask;
		vector<pplx::task<void>> contactsTask;
        std::mutex mutex;

        for (auto& contactFolder : deserializer->GetData())
        {
            auto contactFolderId = contactFolder.GetID();

            auto hierarchyRequest = std::make_shared<ContactFoldersHierarchyRequester>(p_UserId, contactFolder.GetID(), p_PageLogger);
            contactFoldersTask.push_back(hierarchyRequest->Send(that->GetSapio365Session(), p_TaskData).Then([hierarchyRequest, &mutex, &contactFolder]() {
                std::lock_guard<std::mutex> lock(mutex);
                contactFolder.SetChildrenContactFolders(hierarchyRequest->GetData());
            }));

            auto contactsDeserializer = std::make_shared<ValueListDeserializer<BusinessContact, ContactDeserializer>>();
			contactsTask.push_back(that->GetMSGraphSession(httpLogger->GetSessionType())->GetContacts(contactsDeserializer, p_UserId, contactFolderId, p_PageLogger, httpLogger, p_TaskData).Then([&contactFolder, &mutex, that, contactsDeserializer]()
            {
                std::lock_guard<std::mutex> lock(mutex);
                contactFolder.SetContacts(contactsDeserializer->GetData());
            }));
        }

        pplx::when_all(std::begin(contactFoldersTask), std::end(contactFoldersTask)).wait();
        pplx::when_all(std::begin(contactsTask), std::end(contactsTask)).wait();

        return deserializer->GetData();
    }), GetMSGraphSession(Sapio365Session::APP_SESSION), p_TaskData);
}

///////////////////////////////////////////////////////////////////////////////////
// MESSAGE
///////////////////////////////////////////////////////////////////////////////////

TaskWrapper<vector<BusinessMessage>> BusinessObjectManager::GetUserBusinessMessages(const PooledString& p_UserId, const ModuleOptions& p_Options, std::map<BusinessUser, std::shared_ptr<MessageListRequester>>& p_Requesters, const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

	MessagePropertySet properties;
	properties.SetMailPreview(p_Options.m_BodyPreview);
	properties.SetBodyContent(p_Options.m_BodyContent);
	properties.SetMailHeaders(p_Options.m_MailHeaders);

	BusinessUser user;
	user.m_Id = p_UserId;

	p_Requesters[user] = std::make_shared<MessageListRequester>(properties, p_UserId, p_Logger);
	auto& requester = p_Requesters.at(user);
	if (p_Options.m_CutOffDateTime)
		requester->SetCutOffDateTime(*p_Options.m_CutOffDateTime);
	requester->SetCustomFilter(p_Options.m_CustomFilter);

	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_TaskData).Then([requester, p_UserId, p_TaskData, that, logger = p_Logger]()
	{
        std::map<PooledString, boost::YOpt<PooledString>> cachedIds;
        for (auto& message : requester->GetData())
        {
			if (p_TaskData.IsCanceled())
				break;

            PooledString folderId = message.m_ParentFolderId ? *message.m_ParentFolderId : _YTEXT("");

            if (cachedIds.find(folderId) == cachedIds.end())
            {
				auto nonPageLogger = std::make_shared<MultiObjectsRequestLogger>(*logger);
				nonPageLogger->SetLogMessage(_T("message folder"));
				nonPageLogger->SetContextualInfo(GetMessageFolderContext(message));

                BusinessMailFolder businessMailFolder = safeTaskCall(that->GetUserBusinessMailFolder(p_UserId, folderId, nonPageLogger, p_TaskData), that->GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData).get();
                message.m_ParentFolderName = businessMailFolder.m_DisplayName;
                cachedIds[folderId] = message.m_ParentFolderName;
            }
            else
            {
                message.m_ParentFolderName = cachedIds[folderId];
            }
        }

        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::APP_SESSION), ListErrorHandler<BusinessMessage>, p_TaskData);
}

TaskWrapper<vector<BusinessMessage>> BusinessObjectManager::GetUserBusinessMessages(const PooledString& p_UserId, const O365IdsContainer& p_MessageIDs, const ModuleOptions& p_Options, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	return YSafeCreateTask([p_UserId, p_MessageIDs, p_TaskData, logger = p_Logger, that]()
	{
		vector<BusinessMessage> listMessages;

		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, that->GetSapio365Session()->GetIdentifier());
		for (const auto& msgId : p_MessageIDs)
		{
			if (p_TaskData.IsCanceled())
				break;

            auto deserializer = std::make_shared<MessageDeserializer>();

			RunOnScopeEnd _([objName = logger->GetLogMessage(), logger]() { logger->SetLogMessage(objName); });
			logger->SetLogMessage(_YFORMAT(L"message with id %s", Str::MakeMidEllipsis(msgId).c_str()));

			listMessages.push_back(safeTaskCall(that->GetMSGraphSession(httpLogger->GetSessionType())->GetMessage(deserializer, p_UserId, msgId, logger, httpLogger, p_TaskData).Then([msgId, p_UserId, deserializer, p_TaskData, logger, that]() {
                BusinessMessage businessMessage = deserializer->GetData();

				RunOnScopeEnd _([objName = logger->GetLogMessage(), logger]() { logger->SetLogMessage(objName); });
				logger->SetLogMessage(_YFORMAT(L"mail folder for message id %s", Str::MakeMidEllipsis(msgId).c_str()));

				BusinessMailFolder businessMailFolder = safeTaskCall(that->GetUserBusinessMailFolder(p_UserId, businessMessage.m_ParentFolderId ? *businessMessage.m_ParentFolderId : _YTEXT(""), logger, p_TaskData), that->GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData).get();
                businessMessage.m_ParentFolderName = businessMailFolder.m_DisplayName;
                return businessMessage;
			}), that->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData).get());
 		}

		return listMessages;
	});
}

TaskWrapper<BusinessFileAttachment> BusinessObjectManager::GetMessageFileAttachment(const PooledString& p_UserId, const PooledString& p_MessageId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

    auto requester = std::make_shared<MessageFileAttachmentRequester>(p_UserId, p_MessageId, p_AttachmentId, p_Logger);
	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_TaskData).Then([requester, that]()
    {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::APP_SESSION), p_TaskData);
}

TaskWrapper<wstring> BusinessObjectManager::GetMessageMIMEContent(const wstring& p_UserId, const wstring& p_MessageId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

	web::uri_builder uri(_YTEXT("users"));
	uri.append_path(p_UserId);
	uri.append_path(_YTEXT("messages"));
	uri.append_path(p_MessageId);
	uri.append_path(_YTEXT("$value"));

	p_Logger->Log(p_TaskData.GetOriginator());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->Read(uri.to_uri(), httpLogger, p_TaskData, WebPayload(), _YTEXT(""), {}).Then([](RestResultInfo& result)
	{
		return *result.GetBodyAsString();
	}), GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

TaskWrapper<BusinessFileAttachment> BusinessObjectManager::GetUserEventFileAttachment(const PooledString& p_UserId, const PooledString& p_EventId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto requester = std::make_shared<EventFileAttachmentRequester>(EventFileAttachmentRequester::EntityType::User, p_UserId, p_EventId, p_AttachmentId, p_Logger);

	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_TaskData).Then([requester, that]()
    {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::APP_SESSION), p_TaskData);
}

TaskWrapper<BusinessFileAttachment> BusinessObjectManager::GetGroupEventFileAttachment(const PooledString& p_GroupId, const PooledString& p_EventId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
    std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto requester = std::make_shared<EventFileAttachmentRequester>(EventFileAttachmentRequester::EntityType::Group, p_GroupId, p_EventId, p_AttachmentId, p_Logger);

    return safeTaskCall(requester->Send(that->GetSapio365Session(), p_TaskData).Then([requester, that]()
    {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData);
}

TaskWrapper<BusinessMailFolder> BusinessObjectManager::GetUserBusinessMailFolder(const PooledString& p_UserId, const PooledString& p_MailFolderId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<MailFolderDeserializer>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetMailFolder(deserializer, p_UserId, p_MailFolderId, p_Logger, httpLogger, p_Task).Then([deserializer, that]() {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::ObjectErrorHandler<BusinessMailFolder>, p_Task);
}

///////////////////////////////////////////////////////////////////////////////////

TaskWrapper<vector<BusinessEvent>> BusinessObjectManager::GetUserBusinessEvents(const PooledString & p_UserId, const ModuleOptions& p_Options, map<BusinessUser, std::shared_ptr<EventListRequester>>& p_Requesters, const std::shared_ptr<MultiObjectsPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

	EventPropertySet properties;
	properties.SetBodyPreview(p_Options.m_BodyPreview);
	properties.SetBodyContent(p_Options.m_BodyContent);

	BusinessUser user;
	user.m_Id = p_UserId;
	p_Requesters[user] = std::make_shared<EventListRequester>(properties, p_UserId, EventListRequester::Context::Users, p_Logger);

	auto& requester = p_Requesters.at(user);
	requester->SetCutOffDateTime(p_Options.m_CutOffDateTime);
	requester->SetCustomFilter(p_Options.m_CustomFilter);
    
	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, that]() {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::APP_SESSION), ListErrorHandler<BusinessEvent>, p_Task);
}

///////////////////////////////////////////////////////////////////////////////////
// DRIVE
///////////////////////////////////////////////////////////////////////////////////

TaskWrapper<BusinessDrive> BusinessObjectManager::GetBusinessUserDrive(const PooledString & p_UserId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

	auto requester = std::make_shared<DriveRequester>(DriveRequester::DriveSource::User, p_UserId, p_Logger);
	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_TaskData).Then([that, requester]() {
		return requester->GetData();
	}), GetMSGraphSession(Sapio365Session::APP_SESSION), Util::ObjectErrorHandler<BusinessDrive>, p_TaskData);
}

TaskWrapper<BusinessDrive> BusinessObjectManager::GetBusinessGroupDrive(const PooledString & p_GroupId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

	auto requester = std::make_shared<DriveRequester>(DriveRequester::DriveSource::Group, p_GroupId, p_Logger);
	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_TaskData).Then([that, requester]() {
		return requester->GetData();
	}), GetMSGraphSession(Sapio365Session::APP_SESSION), Util::ObjectErrorHandler<BusinessDrive>, p_TaskData);
}

TaskWrapper<BusinessDriveItemFolder> BusinessObjectManager::GetBusinessDriveItems(const PooledString& driveId, const boost::YOpt<wstring>& p_SearchCriteria, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());

    auto requester = std::make_shared<DriveItemsRequester>(driveId, !p_SearchCriteria, false, p_Logger);
	requester->SetSearchCriteria(p_SearchCriteria);
    return safeTaskCall(requester->Send(that->GetSapio365Session(), p_TaskData).Then([requester, driveId, p_TaskData]() {
        BusinessDriveItemFolder	root(true);
		root.SetDriveId(driveId);
        for (auto& driveItem : requester->GetData())
        {
            if (p_TaskData.IsCanceled())
                break;

			driveItem.SetDriveId(driveId);
			if (driveItem.IsFolder())
				root.AddChildFolder(driveItem);
			else if (driveItem.IsNotebook())
				root.AddChildNotebook(driveItem);
			else
                root.AddChild(driveItem);
        }
        return root;
    }), GetMSGraphSession(Sapio365Session::APP_SESSION), Util::ObjectErrorHandler<BusinessDriveItemFolder>, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::UpdatePermission(const PooledString& p_DriveId, const PooledString& p_ItemId, const Permission& p_Permission, const vector<rttr::property>& p_Properties, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->UpdatePermission(p_DriveId, p_ItemId, p_Permission, p_Properties, httpLogger, p_Task).Then([that](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_Task);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::DeletePermission(const PooledString& p_DriveId, const PooledString& p_ItemId, const PooledString& p_PermissionId, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->DeletePermission(p_DriveId, p_ItemId, p_PermissionId, httpLogger, p_Task).Then([that](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_Task);
}

TaskWrapper<HttpResultWithError> BusinessObjectManager::GetDriveItemContent(const wstring& p_DriveId, const wstring& p_DriveItem, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetDriveItemContent(p_DriveId, p_DriveItem, httpLogger, p_TaskData).Then([that](const RestResultInfo& p_Result) {
		return HttpResultWithError(p_Result, boost::none);
	}), GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<vector<BusinessPermission>> BusinessObjectManager::GetBusinessPermissions(const PooledString& p_DriveId, const PooledString& p_DriveItemId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessPermission, PermissionDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetPermissions(deserializer, p_DriveId, p_DriveItemId, p_Logger, httpLogger, p_Task).Then([that, deserializer]() {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), Util::ListErrorHandler<BusinessPermission>, p_Task);
}

TaskWrapper<vector<BusinessSubscribedSku>> BusinessObjectManager::GetSubscribedSkus(YtriaTaskData p_Task)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
	auto logger = std::make_shared<BasicRequestLogger>(_T("subscribed skus"));
	auto requester = std::make_shared<SubscribedSkusRequester>(logger);

	return safeTaskCall(requester->Send(that->GetSapio365Session(), p_Task).Then([requester, that]() {
        that->GetGraphCache().Add(requester->GetData());
		return requester->GetData();
	}), GetMSGraphSession(Sapio365Session::USER_SESSION), ListErrorHandler<BusinessSubscribedSku>, p_Task);
}

TaskWrapper<vector<BusinessLicenseDetail>> BusinessObjectManager::GetUserBusinessLicenseDetails(const PooledString& p_UserId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto deserializer = std::make_shared<ValueListDeserializer<BusinessLicenseDetail, LicenseDetailDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, that->GetSapio365Session()->GetIdentifier());
	return safeTaskCall(GetMSGraphSession(httpLogger->GetSessionType())->GetLicenseDetails(deserializer, p_UserId, p_Logger, httpLogger, p_TaskData).Then([deserializer, that]() {
        return deserializer->GetData();
    }), GetMSGraphSession(httpLogger->GetSessionType()), ListErrorHandler<BusinessLicenseDetail>, p_TaskData);
}

TaskWrapper<BusinessFileAttachment> BusinessObjectManager::GetPostFileAttachment(const PooledString& p_GroupId, const PooledString& p_ConversationId, const PooledString& p_ThreadId, const PooledString& p_PostId, const PooledString& p_AttachmentId, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
{
	std::shared_ptr<BusinessObjectManager> that(shared_from_this());
    auto requester = std::make_shared<PostFileAttachmentRequester>(p_GroupId, p_ConversationId, p_ThreadId, p_PostId, p_AttachmentId, p_Logger);
    return safeTaskCall(requester->Send(that->GetSapio365Session(), p_TaskData).Then([requester, that]()
    {
        return requester->GetData();
    }), GetMSGraphSession(Sapio365Session::USER_SESSION), p_TaskData);
}
