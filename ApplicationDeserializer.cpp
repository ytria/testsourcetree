#include "KeyCredentialDeserializer.h"
#include "ApplicationDeserializer.h"
#include "ApiApplicationDeserializer.h"
#include "AppRoleDeserializer.h"
#include "ListDeserializer.h"
#include "PasswordCredentialDeserializer.h"
#include "PasswordCredential.h"
#include "AddInDeserializer.h"
#include "JsonSerializeUtil.h"
#include "InformationalUrlDeserializer.h"
#include "KeyCredentialDeserializer.h"
#include "KeyCredential.h"
#include "OptionalClaimsDeserializer.h"
#include "ParentalControlSettingsDeserializer.h"
#include "PublicClientApplicationDeserializer.h"
#include "RequiredResourceAccess.h"
#include "RequiredResourceAccessDeserializer.h"
#include "WebApplicationDeserializer.h"
#include "AddIn.h"

void ApplicationDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		ListDeserializer<AddIn, AddInDeserializer> addInDeserializer;
		if (JsonSerializeUtil::DeserializeAny(addInDeserializer, _YTEXT("addIns"), p_Object))
			m_Data.m_AddIns = std::move(addInDeserializer.GetData());
	}

	{
		ApiApplicationDeserializer apiAppDeserializer;
		if (JsonSerializeUtil::DeserializeAny(apiAppDeserializer, _YTEXT("api"), p_Object))
			m_Data.m_Api = std::move(apiAppDeserializer.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("appId"), m_Data.m_AppId, p_Object);
	{
		ListDeserializer<AppRole, AppRoleDeserializer> appRolesDeserializer;
		if (JsonSerializeUtil::DeserializeAny(appRolesDeserializer, _YTEXT("appRoles"), p_Object))
			m_Data.m_AppRoles = std::move(appRolesDeserializer.GetData());
	}
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.m_CreatedDateTime, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("deletedDateTime"), m_Data.m_DeletedDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("groupMembershipClaims"), m_Data.m_GroupMembershipClaims, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	{
		ListStringDeserializer identifierUrisDeserializer;
		if (JsonSerializeUtil::DeserializeAny(identifierUrisDeserializer, _YTEXT("identifierUris"), p_Object))
			m_Data.m_IdentifierUris = std::move(identifierUrisDeserializer.GetData());
	}
	{
		InformationalUrlDeserializer urlDeserializer;
		if (JsonSerializeUtil::DeserializeAny(urlDeserializer, _YTEXT("info"), p_Object))
			m_Data.m_Info = std::move(urlDeserializer.GetData());
	}
	JsonSerializeUtil::DeserializeBool(_YTEXT("isFallbackPublicClient"), m_Data.m_IsFallbackPublicClient, p_Object);
	{
		ListDeserializer<KeyCredential, KeyCredentialDeserializer> keyCredDeserializer;
		if (JsonSerializeUtil::DeserializeAny(keyCredDeserializer, _YTEXT("keyCredentials"), p_Object))
			m_Data.m_KeyCredentials = std::move(keyCredDeserializer.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("logo"), m_Data.m_Logo, p_Object);
	{
		OptionalClaimsDeserializer optClaimsDeserializer;
		if (JsonSerializeUtil::DeserializeAny(optClaimsDeserializer, _YTEXT("optionalClaims"), p_Object))
			m_Data.m_OptionalClaims = std::move(optClaimsDeserializer.GetData());
	}
	{
		ParentalControlSettingsDeserializer parentalCtrlSettingsDeserializer;
		if (JsonSerializeUtil::DeserializeAny(parentalCtrlSettingsDeserializer, _YTEXT("parentalControlSettings"), p_Object))
			m_Data.m_ParentalControlSettings = std::move(parentalCtrlSettingsDeserializer.GetData());
	}
	{
		ListDeserializer<PasswordCredential, PasswordCredentialDeserializer> d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("passwordCredentials"), p_Object))
			m_Data.m_PasswordCredentials = std::move(d.GetData());
	}
	{
		PublicClientApplicationDeserializer pubCxAppDeserializer;
		if (JsonSerializeUtil::DeserializeAny(pubCxAppDeserializer, _YTEXT("publicClient"), p_Object))
			m_Data.m_PublicClient = std::move(pubCxAppDeserializer.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("publisherDomain"), m_Data.m_PublisherDomain, p_Object);
	{
		ListDeserializer<RequiredResourceAccess, RequiredResourceAccessDeserializer> listDeserializer;
		if (JsonSerializeUtil::DeserializeAny(listDeserializer, _YTEXT("requiredResourceAccess"), p_Object))
			m_Data.m_RequiredResourceAccess = std::move(listDeserializer.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("signInAudience"), m_Data.m_SignInAudience, p_Object);
	{
		ListStringDeserializer tagsDeserializer;
		if (JsonSerializeUtil::DeserializeAny(tagsDeserializer, _YTEXT("tags"), p_Object))
			m_Data.m_Tags = std::move(tagsDeserializer.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("tokenEncryptionKeyId"), m_Data.m_TokenEncryptionKeyId, p_Object);
	{
		WebApplicationDeserializer webAppDeserializer;
		if (JsonSerializeUtil::DeserializeAny(webAppDeserializer, _YTEXT("web"), p_Object))
			m_Data.m_WebApplication = std::move(webAppDeserializer.GetData());
	}
}
