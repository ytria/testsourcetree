#include "GridSiteRoles.h"
#include "AutomationWizardSiteRoles.h"
#include "BaseO365Grid.h"
#include "BasicGridSetup.h"
#include "FrameSiteRoles.h"
#include "GridUtil.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"

BEGIN_MESSAGE_MAP(GridSiteRoles, ModuleO365Grid<Sp::RoleDefinition>)
END_MESSAGE_MAP()

GridSiteRoles::GridSiteRoles()
    : m_Frame(nullptr)
{
    // FIXME: Add desired actions when the required methods are implemented below!
    UseDefaultActions(/*O365Grid::ACTION_CREATE | *//*O365Grid::ACTION_DELETE *//*| O365Grid::ACTION_EDIT | O365Grid::ACTION_VIEW*/0);
    EnableGridModifications();

    initWizard<AutomationWizardSiteRoles>(_YTEXT("Automation\\SpUser"));
}

GridSiteRoles::~GridSiteRoles()
{
    GetBackend().Clear();
}

ModuleCriteria GridSiteRoles::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_Frame->GetModuleCriteria();

    criteria.m_IDs.clear();
    for (auto row : p_Rows)
        criteria.m_IDs.insert(row->GetField(m_ColMetaId).GetValueStr());

    return criteria;
}

void GridSiteRoles::BuildView(const O365DataMap<BusinessSite, vector<Sp::RoleDefinition>>& p_SiteRoles, bool p_FullPurge)
{
    GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
        | (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
        | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
        | GridUpdaterOptions::REFRESH_PROCESS_DATA
    );
    GridUpdater updater(*this, options);

    GridIgnoreModification ignoramus(*this);
    for (const auto& keyVal : p_SiteRoles)
    {
        const auto& site = keyVal.first;
        const auto& siteRoles = keyVal.second;

        GridBackendField fID(site.GetID(), m_ColMetaId);
        GridBackendRow* siteRow = m_RowIndex.GetRow({ fID }, true, true);
        ASSERT(nullptr != siteRow);
        if (nullptr != siteRow)
        {
            updater.GetOptions().AddRowWithRefreshedValues(siteRow);
            updater.GetOptions().AddPartialPurgeParentRow(siteRow);

            siteRow->SetHierarchyParentToHide(true);
            siteRow->AddField(site.GetDisplayName(), m_ColMetaDisplayName);

            SetRowObjectType(siteRow, site);
            if (!site.HasFlag(BusinessObject::CANCELED))
            {
                if (keyVal.second.size() == 1 && keyVal.second[0].GetError())
                {
                    updater.OnLoadingError(siteRow, *keyVal.second[0].GetError());
                }
                else
                {
                    // Clear previous error
                    if (siteRow->IsError())
                        siteRow->ClearStatus();

                    for (const auto& spRole : siteRoles)
                    {
                        GridBackendField fieldRoleId(spRole.Id ? std::to_wstring(*spRole.Id) : _YTEXT(""), GetColId());
                        GridBackendField fieldSiteId(site.GetID(), m_ColMetaId);
                        GridBackendRow* roleRow = m_RowIndex.GetRow({ fieldRoleId, fieldSiteId }, true, true);
                        ASSERT(nullptr != roleRow);
                        if (nullptr != roleRow)
                        {
                            updater.GetOptions().AddRowWithRefreshedValues(roleRow);
                            roleRow->SetParentRow(siteRow);

                            SetRowObjectType(roleRow, spRole, spRole.HasFlag(BusinessObject::CANCELED));
                            if (spRole.GetError())
                            {
                                updater.OnLoadingError(roleRow, *spRole.GetError());
                            }
                            else
                            {
                                // Clear previous error
                                if (roleRow->IsError())
                                    roleRow->ClearStatus();

                                siteRow->AddField(site.GetDisplayName(), m_ColMetaDisplayName);
                                FillSiteRolefields(site, spRole, roleRow, updater);
                            }
                        }
                    }
                }
            }
        }
    }
}

void GridSiteRoles::customizeGrid()
{
    {
        BasicGridSetup setup(GridUtil::g_AutoNameSiteRoles, LicenseUtil::g_codeSapio365siteRoles,
        {
            { &m_ColMetaDisplayName, YtriaTranslate::Do(GridLists_customizeGrid_1, _YLOC("Site Display Name")).c_str(), g_FamilyOwner },
            { &m_ColMetaUserDisplayName, g_TitleOwnerUserName, g_FamilyOwner },
            { &m_ColMetaId, g_TitleOwnerID,	g_FamilyOwner }
        }, { Sp::RoleDefinition() });
        setup.Setup(*this, false);
    }

    setBasicGridSetupHierarchy({ { { m_ColMetaDisplayName, 0 } }
								,{ rttr::type::get<Sp::RoleDefinition>().get_id() }
								,{ rttr::type::get<Sp::RoleDefinition>().get_id(), rttr::type::get<BusinessSite>().get_id() } });

    addColumnGraphID(); // Site Role id

    m_ColDescription = AddColumn(_YUID("description"), YtriaTranslate::Do(AutomatedApp_ProcessWndProcException_18, _YLOC("Description")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColHidden = AddColumnCheckBox(_YUID("hidden"), YtriaTranslate::Do(DlgFilterTree_OnHidenClk_1, _YLOC("Hidden")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColName = AddColumn(_YUID("name"), YtriaTranslate::Do(AutomatedApp_ProcessWndProcException_17, _YLOC("Name")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColOrder = AddColumnNumber(_YUID("order"), YtriaTranslate::Do(DlgPivotSetup_OnInitDialog_3, _YLOC("Order")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColRoleTypeKind = AddColumnNumber(_YUID("roleTypeKind"), YtriaTranslate::Do(GridSiteRoles_customizeGrid_5, _YLOC("Role Type Kind")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColBasePermissionsHigh = AddColumn(_YUID("basePermissionsHigh"), YtriaTranslate::Do(GridSiteRoles_customizeGrid_6, _YLOC("Base Permissions - High")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColBasePermissionsLow = AddColumn(_YUID("basePermissionsLow"), YtriaTranslate::Do(GridSiteRoles_customizeGrid_7, _YLOC("Base Permissions - Low")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    
    AddColumnForRowPK(m_ColMetaId); // Site id

    m_Frame = dynamic_cast<FrameSiteRoles*>(GetParentFrame());

    if (nullptr != GetAutomationWizard())
        GetAutomationWizard()->SetFrame(m_Frame);
}

wstring GridSiteRoles::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Site"), m_ColMetaDisplayName }, { _T("Owner"), m_ColMetaUserDisplayName } }, m_ColName);
}

Sp::RoleDefinition GridSiteRoles::getBusinessObject(GridBackendRow* p_Row) const
{
    ASSERT(GridUtil::isBusinessType<Sp::RoleDefinition>(p_Row));
    return Sp::RoleDefinition();
}

void GridSiteRoles::UpdateBusinessObjects(const vector<Sp::RoleDefinition>& p_Roles, bool p_SetModifiedStatus)
{
    ASSERT(false);
}

void GridSiteRoles::RemoveBusinessObjects(const vector<Sp::RoleDefinition>& p_Roles)
{
    ASSERT(false);
}

void GridSiteRoles::InitializeCommands()
{
    ModuleO365Grid<Sp::RoleDefinition>::InitializeCommands();

    const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);
}

void GridSiteRoles::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {

    }

    ModuleO365Grid<Sp::RoleDefinition>::OnCustomPopupMenu(pPopup, nStart);
}

void GridSiteRoles::FillSiteRolefields(const BusinessSite& p_Site, const Sp::RoleDefinition& p_SpRole, GridBackendRow* p_Row, GridUpdater& p_Updater)
{
    p_Row->AddField(p_Site.GetDisplayName(), m_ColMetaDisplayName);
    p_Row->AddField(p_SpRole.Description, m_ColDescription);
    p_Row->AddField(p_SpRole.Hidden, m_ColHidden);
    p_Row->AddField(p_SpRole.Id, GetColId());
    p_Row->AddField(p_SpRole.Name, m_ColName);
    p_Row->AddField(p_SpRole.Order, m_ColOrder);
    p_Row->AddField(p_SpRole.RoleTypeKind, m_ColRoleTypeKind);
    
    if (p_SpRole.BasePermissions != boost::none)
    {
        p_Row->AddField(p_SpRole.BasePermissions->High, m_ColBasePermissionsHigh);
        p_Row->AddField(p_SpRole.BasePermissions->Low, m_ColBasePermissionsLow);
    }
}
