#include "SisterhoodFieldUpdate.h"

#include "BaseO365Grid.h"

SisterhoodFieldUpdate::SisterhoodFieldUpdate(O365Grid& p_Grid, const row_pk_t& p_RowKey, const UINT& p_ColumnId, const GridBackendField& p_OldValue, const GridBackendField& p_NewValue, const std::map<UINT, const GridBackendField*>& p_SisterOldValues, uint32_t p_Flags/* = None*/)
	: FieldUpdateO365(p_Grid, p_RowKey, p_ColumnId, p_OldValue, p_NewValue, p_Flags)
{
	for (const auto& item : p_SisterOldValues)
	{
		auto& f = item.second;
		ASSERT(nullptr != f);
		if (nullptr != f)
			m_SisterOldValues[item.first] = !f->HasValue() ? boost::none : boost::YOpt<GridBackendField>(*f);
	}
}

SisterhoodFieldUpdate::~SisterhoodFieldUpdate()
{
	if (shoulRevertOnDestruction())
	{
		Revert();
		dontRevertOnDestruction(); // Base class already reverted in the above call.
	}
}

void SisterhoodFieldUpdate::Revert()
{
	if (m_State == State::AppliedLocally)
	{
		GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			for (auto& item : m_SisterOldValues)
			{
				// Put back old value
				if (item.second)
					row->AddField(*item.second, item.first).RemoveModified();
				else
					row->RemoveField(item.first);
			}
		}
	}

	FieldUpdate::Revert();
}

std::unique_ptr<FieldUpdateO365> SisterhoodFieldUpdate::CreateReplacement(std::unique_ptr<FieldUpdateO365> p_Replacement)
{
	auto newFU = FieldUpdate::CreateReplacement(std::move(p_Replacement));
	auto newSisFU = dynamic_cast<SisterhoodFieldUpdate*>(newFU.get());
	if (nullptr != newSisFU)
		newSisFU->m_SisterOldValues = m_SisterOldValues;
	return newFU;
}
