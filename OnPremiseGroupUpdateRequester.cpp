#include "OnPremiseGroupUpdateRequester.h"
#include "PSUtil.h"
#include "Sapio365Session.h"

OnPremiseGroupUpdateRequester::OnPremiseGroupUpdateRequester(const boost::YOpt<OnPremiseGroup>& p_Group/* = boost::none*/)
	: m_Group(p_Group)
{
}

TaskWrapper<void> OnPremiseGroupUpdateRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	ASSERT(m_Group);

	wstring psCmd = _YTEXT("Set-ADGroup ");

	if (m_Group)
	{
		ASSERT(m_Group->GetObjectGUID()/* || m_Group->GetSamAccountName()*/);
		if (m_Group->GetObjectGUID())
			psCmd += wstring(_YTEXT("-Identity ")) + m_Group->GetObjectGUID()->c_str();
		/*else if (m_Group->GetSamAccountName())
			psCmd += wstring(_YTEXT("-Identity ")) + m_Group->GetSamAccountName()->c_str();*/

		PSUtil::AddParamToCmd(psCmd, _YTEXT("AuthType"), m_AuthType);
		PSUtil::AddParamToCmd(psCmd, _YTEXT("Description"), m_Group->GetDescription());
		PSUtil::AddParamToCmd(psCmd, _YTEXT("DisplayName"), m_Group->GetDisplayName());
		PSUtil::AddParamToCmd(psCmd, _YTEXT("GroupCategory"), m_Group->GetGroupCategory());
		PSUtil::AddParamToCmd(psCmd, _YTEXT("GroupScope"), m_Group->GetGroupScope());
		PSUtil::AddParamToCmd(psCmd, _YTEXT("HomePage"), m_Group->GetHomePage());
		PSUtil::AddParamToCmd(psCmd, _YTEXT("ManagedBy"), m_Group->GetManagedBy());
		PSUtil::AddParamToCmd(psCmd, _YTEXT("Partition"), m_Partition);
		PSUtil::AddParamToCmd(psCmd, _YTEXT("SamAccountName"), m_Group->GetSamAccountName());
	}

	ASSERT(!m_Result);
	m_Result = std::make_shared<InvokeResultWrapper>();
	return YSafeCreateTaskMutable([psCmd, result = m_Result, deserializer = m_Deserializer, session = p_Session->GetBasicPowershellSession()/*, logger = m_Logger*/, taskData = p_TaskData]() {
		if (nullptr != session)
		{
			//logger->Log(taskData.GetOriginator());

			session->AddScript(psCmd.c_str(), taskData.GetOriginator());

			result->SetResult(session->Invoke(taskData.GetOriginator()));
			//if (result->Get().m_Success && nullptr != result->Get().m_Collection)
			//	result->Get().m_Collection->Deserialize(*deserializer);
		}
	});
}

std::shared_ptr<InvokeResultWrapper> OnPremiseGroupUpdateRequester::GetResult()
{
	return m_Result;
}

void OnPremiseGroupUpdateRequester::SetAuthType(const wstring& p_AuthType)
{
	m_AuthType = p_AuthType;
}

void OnPremiseGroupUpdateRequester::SetPartition(const wstring& p_Partition)
{
	m_Partition = p_Partition;
}
