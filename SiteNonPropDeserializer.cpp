#include "SiteNonPropDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "SiteDeserializer.h"
#include "SiteDeserializerFactory.h"
#include "YtriaFieldsConstants.h"

SiteNonPropDeserializer::SiteNonPropDeserializer(const std::shared_ptr<Sapio365Session>& p_Session)
    : m_Session(p_Session)
{
}

void SiteNonPropDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeTimeDate(_YUID(YTRIA_LASTREQUESTEDDATETIME), m_Data.m_LastRequestTime, p_Object);
    JsonSerializeUtil::DeserializeUint32(_YUID(YTRIA_FLAGS), m_Data.m_Flags, p_Object);

    {
        SiteDeserializerFactory factory(m_Session);
        ListMultiPassDeserializer<BusinessSite, SiteDeserializer> d(factory, m_Data.m_SubSites);
		JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_SITE_SUBSITES), p_Object);
    }
}
