#pragma once

#include "JsonObjectDeserializer.h"

#include "EducationTerm.h"
#include "Encapsulate.h"

class EducationTermDeserializer : public JsonObjectDeserializer, public Encapsulate<EducationTerm>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

