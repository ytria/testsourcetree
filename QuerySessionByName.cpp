#include "QuerySessionByName.h"

#include "InlineSqlQueryRowHandler.h"
#include "O365SQLiteEngine.h"
#include "SqlQuerySelect.h"

QuerySessionByName::QuerySessionByName(O365SQLiteEngine& p_Engine, const wstring& p_SessionEmailOrAppIdOrDisplayName, const wstring& p_SessionType, const int64_t& p_RoleId)
	: m_Engine(p_Engine)
	, m_SessionEmailOrAppIdOrDisplayName(p_SessionEmailOrAppIdOrDisplayName)
	, m_SessionType(p_SessionType)
	, m_RoleId(p_RoleId)
{
}

void QuerySessionByName::Run()
{
	auto handler = [this](sqlite3_stmt* p_Stmt) {
		if (m_EmailOrAppId.empty())
			m_EmailOrAppId = GetStringFromColumn(0, p_Stmt);
	};

	SqlQuerySelect querySession(m_Engine, InlineSqlQueryRowHandler<decltype(handler)>(handler), _YTEXT(R"(SELECT EmailOrAppId FROM Sessions WHERE (EmailorAppId=? OR DisplayName=?) AND SessionType=? AND RoleId=? ORDER BY IsFavorite DESC, LastUsed DESC, RoleId DESC)"));
	querySession.BindString(1, m_SessionEmailOrAppIdOrDisplayName);
	querySession.BindString(2, m_SessionEmailOrAppIdOrDisplayName);
	querySession.BindString(3, m_SessionType);
	querySession.BindInt64(4, m_RoleId);
	querySession.Run();

	m_Status = querySession.GetStatus();
}

const wstring& QuerySessionByName::GetEmailOrAppId() const
{
	return m_EmailOrAppId;
}
