#include "SessionInfo.h"

#include "IRestCredentialsManager.h"
#include "MainFrameSapio365Session.h"
#include "SQL + Cloud/Role Delegation/RoleDelegationManager.h"
#include "SessionTypes.h"

SessionInfo::SessionInfo()
{
    m_RoleId = 0;
}

vector<SessionInfo> SessionInfo::GetSessions(bool p_Sort)
{
    vector<SessionInfo> sessions;

    for (auto& topLevelSession : Sapio365SessionSavedInfo::GetCredentialsManager().List())
    {
		Sapio365SessionSavedInfo savedSession;
		auto loadRes = Sapio365SessionSavedInfo::Load(topLevelSession.GetName());
		if (loadRes.first)
		{
			savedSession = loadRes.second;

			SessionInfo listElem;
			listElem.m_SavedInfo = savedSession;

			if (savedSession.GetShowBaseSession())
				sessions.push_back(listElem);

			for (const auto& role : savedSession.GetRoles())
			{
				SessionInfo roleListElem(listElem);

				roleListElem.m_RoleId = role.m_Id;
				roleListElem.m_SavedInfo.SetSessionName(roleListElem.m_SavedInfo.GetSessionName());

				sessions.push_back(roleListElem);
			}
		}

    }

    if (p_Sort)
    {
        // Sort sessions by active, favorite and descending last used date.
        std::sort(sessions.begin(), sessions.end(), [](const SessionInfo& p_Elem1, const SessionInfo& p_Elem2) {
            const auto status1 = p_Elem1.m_SavedInfo.GetStatus(p_Elem1.m_RoleId);
            const auto status2 = p_Elem2.m_SavedInfo.GetStatus(p_Elem2.m_RoleId);

            if (status1 != status2 && (status1 == SessionStatus::ACTIVE || status2 == SessionStatus::ACTIVE))
                return status1 == SessionStatus::ACTIVE;

			auto retrieveFavAndlastUsed = [](const SessionInfo& p_SessionInfo, bool& p_Favorite, YTimeDate& p_LastUsed)
			{
				if (0 != p_SessionInfo.m_RoleId)
				{
					auto it = p_SessionInfo.m_SavedInfo.GetRoles().find(p_SessionInfo.m_RoleId);
					if (p_SessionInfo.m_SavedInfo.GetRoles().end() != it)
					{
						p_Favorite = it->m_IsFavorite;
						p_LastUsed = it->m_LastUsedOn;
						return;
					}
				}

				ASSERT(0 == p_SessionInfo.m_RoleId);
				p_Favorite = p_SessionInfo.m_SavedInfo.IsFavorite();
				p_LastUsed = p_SessionInfo.m_SavedInfo.GetLastUsedOn();
			};

			bool favorite1 = false;
			bool favorite2 = false;
			YTimeDate lastUsed1;
			YTimeDate lastUsed2;
			retrieveFavAndlastUsed(p_Elem1, favorite1, lastUsed1);
			retrieveFavAndlastUsed(p_Elem2, favorite2, lastUsed2);

			if (favorite1 != favorite2)
				return favorite1 && !favorite2;

			// Prioritize role if same lastused
			if (lastUsed1 == lastUsed2 && 0 != p_Elem1.m_RoleId && 0 == p_Elem2.m_RoleId)
				return true;

			return lastUsed1 > lastUsed2;
        });
    }

    return sessions;
}

wstring SessionInfo::GetRoleName() const
{
	wstring roleName;
	if (m_RoleId != 0)
	{
		{
			auto it = m_SavedInfo.GetRoles().find(m_RoleId);
			if (m_SavedInfo.GetRoles().end() != it)
				roleName = it->m_Name;
		}

		if (roleName.empty())
		{
			ASSERT(RoleDelegationManager::GetInstance().IsReadyToRun());
			if (RoleDelegationManager::GetInstance().IsReadyToRun())
				roleName = RoleDelegationManager::GetInstance().GetDelegation(m_RoleId, MainFrameSapio365Session()).m_Name;
			ASSERT(!roleName.empty());
		}
	}
	return roleName;
}

bool SessionInfo::operator==(const SessionInfo& p_Other) const
{
    return m_SavedInfo.GetTechnicalSessionName() == p_Other.m_SavedInfo.GetTechnicalSessionName() &&
        m_SavedInfo.GetFullname() == p_Other.m_SavedInfo.GetFullname() &&
        m_SavedInfo.GetSessionName() == m_SavedInfo.GetSessionName() &&
        m_SavedInfo.GetTenantDisplayName() == p_Other.m_SavedInfo.GetTenantDisplayName() &&
        m_SavedInfo.GetSessionType() == p_Other.m_SavedInfo.GetSessionType() &&
        m_RoleId == p_Other.m_RoleId;
}


