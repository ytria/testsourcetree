#include "RestoreObjectModification.h"

#include "ActivityLoggerUtil.h"
#include "BaseO365Grid.h"

RestoreObjectModification::RestoreObjectModification(O365Grid& p_Grid, const row_pk_t& p_RowKey)
    : ObjectActionModification(p_Grid, p_RowKey, false,YtriaTranslate::Do(RestoreObjectModification_RestoreObjectModification_3, _YLOC("Restored")).c_str())
{
}

void RestoreObjectModification::RefreshState()
{
	GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

	if (nullptr == row)
	{
		m_State = IMainItemModification::State::RemoteHasNewValue;
	}
	else if (row->IsError())
	{
		m_State = IMainItemModification::State::RemoteError;
	}
	else
	{
		m_State = IMainItemModification::State::RemoteHasOldValue;
		getGrid().OnRowSavedNotReceived(row, false, YtriaTranslate::Do(RestoreObjectModification_RefreshState_1, _YLOC("Restoration has not been applied yet.")).c_str());
	}
}

vector<Modification::ModificationLog> RestoreObjectModification::GetModificationLogs() const
{
	return{ ModificationLog(GridBackendUtil::ToString(GetRowKey()), GetObjectName(), ActivityLoggerUtil::g_ActionRestore, getGrid().GetAutomationName(), GetState()) };
}