#pragma once

#include "DlgFormsHTML.h"

class AutomationInputVariableDefinition;

class DlgAutomationInputHTML : public DlgFormsHTML
{
public:
	DlgAutomationInputHTML(	const wstring& p_Title, 
							const map<INT, wstring> p_BtnTexts, 
							const vector<AutomationInputVariableDefinition>& p_VariableDefinitions, 
							const vector<AutomationInputVariableEvent>& p_VarEvents, 
							const vector<AutomationInputVariableEvent>& p_VarEventsGlobal,
							CWnd* p_Parent);
	virtual ~DlgAutomationInputHTML();

	const vector<AutomationInputVariableDefinition>& GetScriptVariables() const;

protected:
	virtual void generateJSONScriptData() override;

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

	virtual wstring getDialogTitle() const override;

private:
	void addComboEditor(const wstring& p_PropName, const wstring& p_PropLabel, const wstring& p_PropTooltip, uint32_t p_Flags, const vector<std::tuple<wstring, wstring, bool>>& p_LabelsAndValues);

	vector<AutomationInputVariableDefinition>	m_VarDef;
	vector<AutomationInputVariableEvent>		m_VarEvents;
	vector<AutomationInputVariableEvent>		m_VarEventsGlobal;
	wstring										m_Title;
	const map<INT, wstring>						m_BtnTexts;
};