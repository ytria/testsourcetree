#pragma once

#include "IRequester.h"
#include "EducationUser.h"
#include "ValueListDeserializer.h"
#include "IPageRequestLogger.h"

class EducationUserDeserializer;

class ClassMembersListRequester : public IRequester
{
public:
	ClassMembersListRequester(const wstring& p_ClassId, const std::shared_ptr<IPageRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const vector<EducationUser>& GetData() const;

private:
	std::shared_ptr<ValueListDeserializer<EducationUser, EducationUserDeserializer>> m_Deserializer;
	wstring m_ClassId;
	std::shared_ptr<IPageRequestLogger> m_Logger;
};

