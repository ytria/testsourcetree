#include "EducationSchoolDeserializer.h"

#include "IdentitySetDeserializer.h"
#include "PhysicalAddressDeserializer.h"

void EducationSchoolDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.m_Description, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	JsonSerializeUtil::DeserializeString(_YTEXT("status"), m_Data.m_Status, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("externalSource"), m_Data.m_ExternalSource, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("principalEmail"), m_Data.m_PrincipalEmail, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("principalName"), m_Data.m_PrincipalName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("externalPrincipalId"), m_Data.m_ExternalPrincipalId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("highestGrade"), m_Data.m_HighestGrade, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("lowestGrade"), m_Data.m_LowestGrade, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("schoolNumber"), m_Data.m_SchoolNumber, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("externalId"), m_Data.m_ExternalId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("phone"), m_Data.m_Phone, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("fax"), m_Data.m_Fax, p_Object);

	{
		PhysicalAddressDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("address"), p_Object))
			m_Data.m_Address = std::move(d.GetData());
	}

	{
		IdentitySetDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("createdBy"), p_Object))
			m_Data.m_CreatedBy = std::move(d.GetData());
	}

}
