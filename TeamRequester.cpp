#include "TeamRequester.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

TeamRequester::TeamRequester(PooledString p_TeamId, const std::shared_ptr<IRequestLogger>& p_Logger)
	:m_TeamId(p_TeamId),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> TeamRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<TeamDeserializer>();
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri(_YTEXT("teams"));
	uri.append_path(m_TeamId);

	static const bool useBeta = true; // FIXME: We have to use beta for now to get "allowCreatePrivateChannels" in MemberSettings.
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, uri, useBeta, m_Logger, httpLogger, p_TaskData);
}

const Team& TeamRequester::GetData() const
{
	return m_Deserializer->GetData();
}
