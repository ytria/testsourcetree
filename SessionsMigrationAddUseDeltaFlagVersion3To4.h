#pragma once

#include "ISessionsMigrationVersion.h"

class SessionsMigrationAddUseDeltaFlagVersion3To4 : public ISessionsMigrationVersion
{
public:
	void Build() override;
	VersionSourceTarget GetVersionInfo() const override;

private:
	bool CreateNewFromOld();
	bool AddDeltaFlag();
};

