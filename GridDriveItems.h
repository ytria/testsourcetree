#pragma once

#include "BaseO365Grid.h"

#include "BusinessDriveItemFolder.h"
#include "BusinessPermission.h"
#include "CheckoutStatus.h"
#include "CommandInfo.h"
#include "DriveChanges.h"
#include "DriveItemDownloadInfo.h"
#include "GridTemplateGroups.h"
#include "GridTemplateUsers.h"
#include "GridUpdater.h"
#include "MyDataMeRowHandler.h"
#include "YCallbackMessage.h"
#include "YTaskPool.h"

#define USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS 1

struct DownloadTaskPool
{
    DownloadTaskPool(std::shared_ptr<YTaskPool> p_TaskPool, int p_TaskDataId, HWND p_Originator)
     : m_TaskPool(p_TaskPool),
        m_TaskDataId(p_TaskDataId),
        m_Originator(p_Originator)
    {}
    ~DownloadTaskPool()
    {
        YCallbackMessage::DoPost([taskId = m_TaskDataId, originator = m_Originator]()
        {
            TaskDataManager::Get().RemoveTaskData(taskId, originator);
        });
    }

    std::shared_ptr<YTaskPool> m_TaskPool;
private:
    int m_TaskDataId;
    HWND m_Originator;
};

class FrameDriveItems;

using GridDriveItemsBaseClass = ModuleO365Grid<BusinessDriveItem>;
class GridDriveItems : public GridDriveItemsBaseClass
{
public:
	GridDriveItems(Origin p_Origin, bool p_IsMyData = false);

	void BuildTreeView(const O365DataMap<BusinessUser, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_Updates, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses);
	void BuildTreeView(const O365DataMap<BusinessGroup, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_Updates, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses);
	void BuildTreeView(const O365DataMap<BusinessSite, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_Updates, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses);
	void BuildTreeView(const O365DataMap<BusinessChannel, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_Updates, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses);

	GridBackendColumn* GetColMetaID() const;
	GridBackendColumn* GetColMetaDriveID() const;
    GridBackendColumn* GetColDownloadProgress() const;

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

	void ViewPermissions(const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const bool p_RefreshUpdates, const bool p_Explode, const bool p_Flat, bool p_UpdateGrid);

#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
    void ViewCheckoutStatuses(const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses, bool p_UpdateGrid);
    bool HasRowCheckoutStatus(const GridBackendRow* p_Row) const;
#endif

	DriveChanges GetDriveChanges(bool p_Selected);

    void AddOccuringDownload(GridBackendRow* p_Row);
    void RemoveOccuringDownload(const DriveItemDownloadInfo& p_DownloadInfo, const boost::YOpt<wstring>& p_FileLocation);

    std::shared_ptr<DownloadTaskPool> GetDownloadTaskPool(YtriaTaskData p_TaskData, HWND p_Originator);

    int GetNbDownloadsOccuring() const;
    DriveSubInfoIds GetSubRequestInfo(const std::vector<GridBackendRow *>& p_Rows, const O365IdsContainer& p_ItemIds);

    GridBackendColumn* GetColumnPermissionId() const;
    GridBackendColumn* GetColumnRole() const;

	void SetShowOwnerPermissions(bool p_Val);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

    class DriveItemPermissionInfo
    {
    public:
        DriveItemPermissionInfo(GridDriveItems& p_Grid, const wstring& p_PermissionId, GridBackendRow* p_Row);

    public:
		PooledString m_OriginId;
        PooledString m_DriveId;
        PooledString m_DriveItemId;
        PooledString m_PermissionId;

		GridBackendRow* m_Row;
    };

	virtual row_pk_t UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody) override;

    bool IsMyData() const override;

    bool GetRowsWithCheckoutStatusLoaded(vector<GridBackendRow*>& p_RowsWithCheckoutStatus); // returns true if found at least one

	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

/*
	virtual RBAC_Privilege GetPrivilegeEdit() override;
	virtual RBAC_Privilege GetPrivilegeDelete() override;
*/

protected:
	virtual void customizeGrid() override;
	virtual void customizeGridPostProcess() override;
    virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
    virtual void InitializeCommands() override;

    virtual bool HasModificationsPending(bool inSelectionOnly = false) override;

	void SetPermissionRole(const wstring& p_RoleValue, const int p_CommandID);
	bool EnableSetPermissionRole(const wstring& p_PermissionRole, const int p_CommandID);

	afx_msg void OnViewPermissions();
	afx_msg void OnViewPermissionsExplode();
	afx_msg void OnViewPermissionsExplodeFlat();
    afx_msg void OnDeletePermissions();
    afx_msg void OnSetPermissionRoleToRead();
    afx_msg void OnSetPermissionRoleToWrite();
	afx_msg void OnDownload();
	afx_msg void OnDeleteItem();
    afx_msg void OnRenameItem();
	afx_msg void OnToggleOwnerPermissions();
	afx_msg void OnCreateFolder();
	afx_msg void OnAddFile();
	afx_msg void OnImportFolder();
	afx_msg void OnCheckIn();
	afx_msg void OnCheckOut();
	afx_msg void OnShowFoldersInFlatToggle();
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
    afx_msg void OnLoadCkeckoutStatus();
#endif

    afx_msg void OnUpdateShowPermissions(CCmdUI* pCmdUI);
    afx_msg void OnUpdateDeletePermissions(CCmdUI* pCmdUI);
    afx_msg void OnUpdateSetPermissionRoleToRead(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSetPermissionRoleToWrite(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDownload(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteItem(CCmdUI* pCmdUI);
    afx_msg void OnUpdateRenameItem(CCmdUI* pCmdUI);
	afx_msg void OnUpdateToggleOwnerPermissions(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCreateFolder(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAddFile(CCmdUI* pCmdUI);
	afx_msg void OnUpdateImportFolder(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCheckIn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCheckOut(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowFoldersInFlatToggle(CCmdUI* pCmdUI);
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
    afx_msg void OnUpdateLoadCkeckoutStatus(CCmdUI* pCmdUI);
#endif

	afx_msg void OnChangeSearchCriteria();
	afx_msg void OnUpdateChangeSearchCriteria(CCmdUI* pCmdUI);

    afx_msg void OnTimer(UINT_PTR nIDEvent);

    DECLARE_MESSAGE_MAP()

	virtual BOOL PreTranslateMessage(MSG* pMsg) override;

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

private:
	void checkInOut(bool p_CheckIn);
	void loadCkeckoutStatuses(const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses, GridUpdater& updater);
	void loadPermissions(const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, GridUpdater& updater);

    void loadMoreImpl(const std::vector<GridBackendRow*>& p_Rows);
    bool isModInSelectedRows(ISubItemModification* p_Mod);

	GridBackendRow* fillRow(const BusinessDriveItem& p_BusinessDriveItem, GridBackendRow* p_ParentRow, bool p_LoadOrRefresh, GridUpdater* p_Updater);

	virtual BusinessDriveItem getBusinessObject(GridBackendRow*) const override;

    void fillPermissionFields(GridBackendRow* p_Row, const vector<BusinessPermission>& p_Permissions);

    void fillGrantedTo(const BusinessPermission& p_Permission, vector<PooledString>& p_GrantedToApplicationIdentityID, vector<PooledString>& p_GrantedToApplicationIdentityName, vector<PooledString>& p_GrantedToDeviceIdentityID, vector<PooledString>& p_GrantedToDeviceIdentityName, vector<PooledString>& p_GrantedToUserIdentityID, vector<PooledString>& p_GrantedToUserIdentityName, vector<PooledString>& p_GrantedToUserIdentityEmail);
    void fillInvitation(const BusinessPermission& p_Permission, vector<PooledString>& p_InvitationEmail, vector<PooledString>& p_InvitationInvitedByApplicationIdentityId, vector<PooledString>& p_InvitationInvitedByApplicationIdentityName, vector<PooledString>& p_InvitationInvitedByDeviceIdentityId, vector<PooledString>& p_InvitationInvitedByDeviceIdentityName, vector<PooledString>& p_InvitationInvitedByUserIdentityId, vector<PooledString>& p_InvitationInvitedByUserIdentityName, vector<PooledString>& p_InvitationInvitedByUserIdentityEmail, vector<BOOLItem>& p_InvitationSignInRequired);
    void fillInheritedFrom(const BusinessPermission& p_Permission, vector<PooledString>& p_InheritedFromDriveId, vector<PooledString>& p_InheritedFromDriveType, vector<PooledString>& p_InheritedFromId, vector<PooledString>& p_InheritedFromName, vector<PooledString>& p_InheritedFromPath, vector<PooledString>& p_InheritedFromShareID, vector<PooledString>& p_InheritedFromSharepointIdsListID, vector<PooledString>& p_InheritedFromSharepointIdsListItemID, vector<PooledString>& p_InheritedFromSharepointIdsListItemUniqueID, vector<PooledString>& p_InheritedFromSharepointIdsSiteID, vector<HyperlinkItem>& p_InheritedFromSharepointIdsSiteURL, vector<PooledString>& p_InheritedFromSharepointIdsWebID);
    void fillLink(const BusinessPermission& p_Permission, vector<PooledString>& p_LinkApplicationIdentityId, vector<PooledString>& p_LinkApplicationIdentityName, vector<PooledString>& p_LinkType, vector<PooledString>& p_LinkScope, vector<HyperlinkItem>& p_LinkWebHtml, vector<HyperlinkItem>& p_LinkWebUrl);

	void requestViewPermissions(const std::vector<GridBackendRow*>& p_Rows, const Command::ModuleTask p_TaskType);

	virtual const wstring&	GetNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;
	virtual wstring	GetNoFieldTooltip(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;
	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

    virtual void postSetHierarchyViewProcess() override;

#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
    bool isRowWithUnloadedCheckout(const GridBackendRow* p_Row) const;
#endif

	void buildTreeViewGenericImpl(const BusinessDriveItem& p_DriveItem, GridBackendRow* p_ParentRow, GridUpdater& p_Updater);

	template <class BusinessType>
	void buildTreeViewImpl(const O365DataMap<BusinessType, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_Updates, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses);

	bool authorizedByRBAC(GridBackendRow* p_Row, const int p_CommandID) const;
	RoleDelegationUtil::RBAC_Privilege getPermission(const int p_CommandID) const;

	void fillCountsAndSizes(const BusinessDriveItemFolder* p_FolderItem, GridBackendRow* p_Row);
	
	GridBackendColumn*& getColMetaDisplayName() const;
	GridBackendColumn*& getColMetaID() const;

	GridBackendRow* addParentRow(const BusinessUser& p_User, GridUpdater& p_Updater);
	GridBackendRow* addParentRow(const BusinessGroup& p_Group, GridUpdater& p_Updater);
	GridBackendRow* addParentRow(const BusinessSite& p_Site, GridUpdater& p_Updater);
    GridBackendRow* addParentRow(const BusinessChannel& p_Channel, GridUpdater& p_Updater);

	void setFoldersVisibilityInFlatView(const bool p_ShowFoldersInFlatView);

    vector<GridBackendColumn*> getCheckoutStatusFamilyColumns() const;
    bool isCheckoutStatusDataColumn(const GridBackendColumn* p_Col) const;

    void fillCheckoutStatus(GridBackendRow* p_Row, const CheckoutStatus& p_CheckoutStatus, GridUpdater* p_Updater);
    void clearCheckoutStatus(GridBackendRow* p_Row);

private:
	std::unique_ptr<GridTemplateUsers> m_TemplateUsers;
	std::unique_ptr<GridTemplateGroups> m_TemplateGroups;

	struct SiteMetaColumns
	{
		GridBackendColumn* m_ColumnID = nullptr;
		GridBackendColumn* m_ColumnDisplayName = nullptr;
	};
	std::unique_ptr<SiteMetaColumns> m_SiteMetaColumns;

	struct ChannelMetaColumns
	{
		GridBackendColumn* m_ColumnTeamID = nullptr;
		GridBackendColumn* m_ColumnTeamDisplayName = nullptr;
		GridBackendColumn* m_ColumnChannelID = nullptr;
		GridBackendColumn* m_ColumnChannelDisplayName = nullptr;
	};
	std::unique_ptr<ChannelMetaColumns> m_ChannelMetaColumns;

    GridBackendColumn* m_ColMetaDriveId;

    GridBackendColumn* m_ColDirectory;
    GridBackendColumn* m_ColOneDrivePath;
    GridBackendColumn* m_ColName;
    GridBackendColumn* m_ColDownloadProgress;
    GridBackendColumn* m_ColNameExtOnly;
    GridBackendColumn* m_ColCreatedDateTime;
    GridBackendColumn* m_ColLastModifiedDateTime;

	GridBackendColumn* m_ColFileToBeUploaded;

    GridBackendColumn* m_ColFileSystemInfoCreatedDateTime;
    GridBackendColumn* m_ColFileSystemInfoLastModifiedTime;
    GridBackendColumn* m_ColCreatedByUserName;
    GridBackendColumn* m_ColCreatedByUserId;
    GridBackendColumn* m_ColCreatedByUserEmail;
    GridBackendColumn* m_ColCreatedByAppName;
    GridBackendColumn* m_ColCreatedByAppId;
    GridBackendColumn* m_ColCreatedByDeviceName;
    GridBackendColumn* m_ColCreatedByDeviceId;
    GridBackendColumn* m_ColLastModifiedByUserName;
    GridBackendColumn* m_ColLastModifiedByUserId;
    GridBackendColumn* m_ColLastModifiedByUserEmail;
    GridBackendColumn* m_ColLastModifiedByApplicationName;
    GridBackendColumn* m_ColLastModifiedByApplicationId;
    GridBackendColumn* m_ColLastModifiedByDeviceName;
    GridBackendColumn* m_ColLastModifiedByDeviceId;
    GridBackendColumn* m_ColFileHashes;
    GridBackendColumn* m_ColFileMimeType;
    GridBackendColumn* m_ColSharedOwnerUserName;
    GridBackendColumn* m_ColSharedOwnerUserId;
    GridBackendColumn* m_ColSharedOwnerUserEmail;
    GridBackendColumn* m_ColSharedOwnerApplicationName;
    GridBackendColumn* m_ColSharedOwnerApplicationId;
    GridBackendColumn* m_ColSharedOwnerDeviceName;
    GridBackendColumn* m_ColSharedOwnerDeviceId;
    GridBackendColumn* m_ColShared;
    GridBackendColumn* m_ColSharedScope;
    GridBackendColumn* m_ColSharedSharedByUserName;
    GridBackendColumn* m_ColSharedSharedByUserId;
    GridBackendColumn* m_ColSharedSharedByUserEmail;
    GridBackendColumn* m_ColSharedSharedByApplicationName;
    GridBackendColumn* m_ColSharedSharedByApplicationId;
    GridBackendColumn* m_ColSharedSharedByDeviceName;
    GridBackendColumn* m_ColSharedSharedByDeviceId;
    GridBackendColumn* m_ColSharedSharedDateTime;
    GridBackendColumn* m_ColPackageType;
    GridBackendColumn* m_ColETag;
    GridBackendColumn* m_ColCTag;

	//GridBackendColumn* m_ColDeleted; Useless for now, as Recycle bin items are not displayed in the grid
    GridBackendColumn* m_ColWebUrl;
	GridBackendColumn* m_ColFileCount;
	GridBackendColumn* m_ColFolderCount;
	GridBackendColumn* m_ColFileSize;
    GridBackendColumn* m_ColFolderSize;
    GridBackendColumn* m_ColTotalSize;    

    // Permissions columns
    GridBackendColumn* m_ColPermissionId;

	// Granted To
	GridBackendColumn* m_ColTarget;
    GridBackendColumn* m_ColGrantedToApplicationIdentityID;
    GridBackendColumn* m_ColGrantedToApplicationIdentityName;
    GridBackendColumn* m_ColGrantedToDeviceIdentityID;
    GridBackendColumn* m_ColGrantedToDeviceIdentityName;
    GridBackendColumn* m_ColGrantedToUserIdentityID;
    GridBackendColumn* m_ColGrantedToUserIdentityName;
    GridBackendColumn* m_ColGrantedToUserIdentityEmail;

    // Invitation
    GridBackendColumn* m_ColInvitationEmail;
    GridBackendColumn* m_ColInvitationInvitedByApplicationIdentityId;
    GridBackendColumn* m_ColInvitationInvitedByApplicationIdentityName;
    GridBackendColumn* m_ColInvitationInvitedByDeviceIdentityId;
    GridBackendColumn* m_ColInvitationInvitedByDeviceIdentityName;
    GridBackendColumn* m_ColInvitationInvitedByUserIdentityId;
    GridBackendColumn* m_ColInvitationInvitedByUserIdentityName;
    GridBackendColumn* m_ColInvitationInvitedByUserIdentityEmail;
    GridBackendColumn* m_ColInvitationSignInRequired;

    // Inherited From
    GridBackendColumn* m_ColInheritedFromDriveID;
    GridBackendColumn* m_ColInheritedFromDriveType;
    GridBackendColumn* m_ColInheritedFromId;
    GridBackendColumn* m_ColInheritedFromName;
    GridBackendColumn* m_ColInheritedFromPath;
    GridBackendColumn* m_ColInheritedFromShareID;
    GridBackendColumn* m_ColInheritedFromSharepointIDsListID;
    GridBackendColumn* m_ColInheritedFromSharepointIDsListItemID;
    GridBackendColumn* m_ColInheritedFromSharepointIDsListItemUniqueID;
    GridBackendColumn* m_ColInheritedFromSharepointIDsSiteID;
    GridBackendColumn* m_ColInheritedFromSharepointIDsSiteURL;
    GridBackendColumn* m_ColInheritedFromSharepointIDsWebID;
    GridBackendColumn* m_ColLinkApplicationIdentityId;
    GridBackendColumn* m_ColLinkApplicationIdentityName;
    GridBackendColumn* m_ColLinkType;
    GridBackendColumn* m_ColLinkScope;
    GridBackendColumn* m_ColLinkWebHtml;
    GridBackendColumn* m_ColLinkWebURL;
    
    GridBackendColumn* m_ColRole;
    GridBackendColumn* m_ColShareId;

	GridBackendColumn* m_ColDescription;
	GridBackendColumn* m_ColPublicationLevel;
	GridBackendColumn* m_ColPublicationVersionId;
	GridBackendColumn* m_ColSharepointIDsListID;
	GridBackendColumn* m_ColSharepointIDsListItemID;
	GridBackendColumn* m_ColSharepointIDsListItemUniqueID;
	GridBackendColumn* m_ColSharepointIDsSiteID;
	GridBackendColumn* m_ColSharepointIDsSiteURL;
	GridBackendColumn* m_ColSharepointIDsWebID;
	GridBackendColumn* m_ColLinkWebDavURL;

	// Checkout Status columns
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
    GridBackendColumn* m_ColCheckoutLoaded;
#endif
    GridBackendColumn* m_ColCheckoutUser;
    GridBackendColumn* m_ColCheckinComment;
    GridBackendColumn* m_ColComplianceTag;
	GridBackendColumn* m_ColComplianceTimeWrittenTime;

	vector<GridBackendColumn*>	m_MetaColumns;

    FrameDriveItems*			m_FrameDriveItems;

    wstring m_NameForCheckout;
    bool m_IsMyData;
	bool m_HasLoadMoreAdditionalInfo;
	bool m_ShowFoldersInFlatView;

	O365IdsContainer m_MetaIDsMoreLoaded;

	static wstring g_SharedPrivate;
	static wstring g_SharedShared;
	static wstring g_OwnerOnlyText;
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
    static wstring g_CheckoutNoFieldText;
    static wstring g_CheckoutNoFieldTooltip;
#endif

	bool m_ShowOwnerPermissions;
	map<std::pair<PooledString, PooledString>, vector<BusinessPermission>> m_AllPermissions;
	static const int g_HiddenOwnerLparam;
	//static const GridBackendUtil::ROWFLAGS g_RenameOnConflictFlag;
	static const GridBackendUtil::ROWFLAGS g_ReplaceContentFlag;
    
	int m_NbDownloadsOccuring;
    std::shared_ptr<YTaskPool> m_DownloadTaskPool;
    static const int g_DownloadTimerId = 1000; // Arbitrary

	std::map<PooledString, int> m_PublicationLevelIcons;

	HACCEL m_hAccelSpecific = nullptr;

	boost::YOpt<MyDataMeRowHandler> m_MyDataMeHandler;

    friend class DriveItemsUtil;

	friend class DriveItemImporterFile;
	friend class DriveItemImporterFolder;
};
