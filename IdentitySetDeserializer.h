#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "IdentitySet.h"

class IdentitySetDeserializer : public JsonObjectDeserializer, public Encapsulate<IdentitySet>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

