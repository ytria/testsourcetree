#include "MailboxSettingsDeserializer.h"

#include "AutomaticRepliesSettingDeserializer.h"
#include "JsonSerializeUtil.h"
#include "LocaleInfoDeserializer.h"
#include "WorkingHoursDeserializer.h"

void MailboxSettingsDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("archiveFolder"), m_Data.ArchiveFolder, p_Object);
    {
        AutomaticRepliesSettingDeserializer arsd;
        if (JsonSerializeUtil::DeserializeAny(arsd, _YTEXT("automaticRepliesSetting"), p_Object))
            m_Data.AutomaticRepliesSetting = arsd.GetData();
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("dateFormat"), m_Data.DateFormat, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("delegateMeetingMessageDeliveryOptions"), m_Data.DelegateMeetingMessageDeliveryOptions, p_Object);

    {
        LocaleInfoDeserializer lid;
        if (JsonSerializeUtil::DeserializeAny(lid, _YTEXT("language"), p_Object))
            m_Data.Language = lid.GetData();
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("timeFormat"), m_Data.TimeFormat, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("timeZone"), m_Data.TimeZone, p_Object);

	{
		WorkingHoursDeserializer whd;
		if (JsonSerializeUtil::DeserializeAny(whd, _YTEXT("workingHours"), p_Object))
			m_Data.WorkingHours = whd.GetData();
	}
}
