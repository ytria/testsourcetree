#include "GroupEndpointsRequester.h"
#include "GroupEndpointDeserializer.h"
#include "MsGraphHttpRequestLogger.h"
#include "MSGraphSession.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"

GroupEndpointsRequester::GroupEndpointsRequester(PooledString p_GroupId, const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_GroupId(p_GroupId)
	, m_Logger(p_Logger)
{
}

TaskWrapper<void> GroupEndpointsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<GroupEndpoint, GroupEndpointDeserializer>>();

	web::uri_builder uri;
	uri.append_path(_YTEXT("groups"));
	uri.append_path(m_GroupId);
	uri.append_path(_YTEXT("endpoints"));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());

	auto requester = Util::CreateDefaultGraphPageRequester(m_Deserializer, httpLogger, p_TaskData.GetOriginator());
	requester->SetUseBetaEndpoint(true);

	MsGraphPaginator paginator(uri.to_uri(), requester, m_Logger);
	return paginator.Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

const std::vector<GroupEndpoint>& GroupEndpointsRequester::GetData() const
{
	return m_Deserializer->GetData();
}
