#pragma once

#include "IHttpRequestLogger.h"
#include "Sapio365Session.h"

class MsGraphHttpRequestLogger : public IHttpRequestLogger
{
public:
	MsGraphHttpRequestLogger(Sapio365Session::SessionType p_SessionType, const SessionIdentifier& p_Identifier);

	void LogRequest(const wstring& p_Uri, const wstring& p_Method, const WebPayload& p_Payload, HWND p_Originator) override;
	void LogResponse(const wstring& p_Uri, web::http::status_code p_StatusCode, const wstring& p_Method, HWND p_Originator) override;
	shared_ptr<IHttpRequestLogger> Clone() const override;

	Sapio365Session::SessionType GetSessionType() const;

private:
	Sapio365Session::SessionType m_SessionType;
};

