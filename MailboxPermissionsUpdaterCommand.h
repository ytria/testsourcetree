#pragma once

#include "IActionCommand.h"

#include "MailboxPermissionsAddModification.h"
#include "MailboxPermissionsEditModification.h"
#include "MailboxPermissionsRemoveModification.h"


struct AutomationSetup;
class BusinessObjectManager;
class FrameMailboxPermissions;
class MultiObjectsRequestLogger;

class MailboxPermissionsUpdaterCommand : public IActionCommand
{
public:
	MailboxPermissionsUpdaterCommand(FrameMailboxPermissions& p_Frame);

	void AddAddition(const MailboxPermissionsAddModification& p_Mod);
	void AddEdition(const MailboxPermissionsEditModification& p_Mod);
	void AddRemoval(const MailboxPermissionsRemoveModification& p_Mod);

	void ExecuteImpl() const override;

private:
	static void ApplyAdditions(vector<MailboxPermissionsAddModification>& p_Additions, const std::shared_ptr<BusinessObjectManager>& p_Bom, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData taskData);
	static void ApplyEditions(vector<MailboxPermissionsEditModification>& p_Editions, const std::shared_ptr<BusinessObjectManager>& p_Bom, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData taskData);
	static void ApplyRemovals(vector<MailboxPermissionsRemoveModification>& p_Removals, const std::shared_ptr<BusinessObjectManager>& p_Bom, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData taskData);

	vector<MailboxPermissionsAddModification> m_Additions;
	vector<MailboxPermissionsEditModification> m_Editions;
	vector<MailboxPermissionsRemoveModification> m_Removals;

	FrameMailboxPermissions& m_Frame;
};

