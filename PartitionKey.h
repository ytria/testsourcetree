#pragma once

namespace Cosmos
{
    class PartitionKey
    {
    public:
        boost::YOpt<vector<PooledString>> Paths;
        boost::YOpt<PooledString> Kind;
    };
}
