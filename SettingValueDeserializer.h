#pragma once

#include "JsonObjectDeserializer.h"
#include "SettingValue.h"
#include "Encapsulate.h"

class SettingValueDeserializer : public JsonObjectDeserializer, public Encapsulate<SettingValue>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

