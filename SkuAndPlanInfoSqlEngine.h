#pragma once

#include "O365SQLiteEngine.h"

class SkuAndPlanInfoSQLEngine : public O365SQLiteEngine
{
public:
	SkuAndPlanInfoSQLEngine(const wstring& p_DBfile);

	~SkuAndPlanInfoSQLEngine();

	struct SkuInfo
	{
		PooledString m_Id;
		PooledString m_Name;
		PooledString m_Label;
		std::set<PooledString> m_PlanIds;
	};

	struct PlanInfo
	{
		PooledString m_Id;
		PooledString m_Name;
		PooledString m_Label;
		std::shared_ptr<std::set<PooledString>> m_Exclusions;
	};

	std::pair<std::map<PooledString, SkuInfo>, std::map<PooledString, PlanInfo>> Load() const;
	
	std::map<PooledString, SkuInfo> LoadSkus() const;
	std::map<PooledString, PlanInfo> LoadPlans() const;
	void Save(const std::map<PooledString, SkuInfo>& p_Skus);
	void Save(const std::map<PooledString, PlanInfo>& p_Plans);

	boost::YOpt<SkuInfo> LoadSku(const PooledString& p_Id) const;
	boost::YOpt<PlanInfo> LoadPlan(const PooledString& p_Id) const;
	void SaveSku(const SkuInfo& p_SkuInfo);
	void SavePlan(const PlanInfo& p_PlanInfo);

	uint32_t GetVersion() const override
	{
		//FIXME: We should probably handle a real version as for the other db..

		// See SQLSkuAndPlanInfoProvider ctor.
		return 1;
	}

private:
	SQLiteUtil::Table m_TableSku;
	SQLiteUtil::Table m_TablePlan;

	sqlite3_stmt* m_SqlStatementGetAllSkus;
	sqlite3_stmt* m_SqlStatementGetSku;
	sqlite3_stmt* m_SqlStatementSetSku;
	sqlite3_stmt* m_SqlStatementDeleteSku;

	sqlite3_stmt* m_SqlStatementGetAllPlans;
	sqlite3_stmt* m_SqlStatementGetPlan;
	sqlite3_stmt* m_SqlStatementSetPlan;
	sqlite3_stmt* m_SqlStatementDeletePlan;

private:
	void init();
	static SkuInfo getSkuInfo(sqlite3_stmt*);
	static PlanInfo getPlanInfo(sqlite3_stmt*);


	vector<SQLiteUtil::Table> GetTables() const override;
};