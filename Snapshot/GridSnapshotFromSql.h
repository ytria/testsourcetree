#pragma once

#include "GridSnapshot.h"

namespace GridSnapshot
{
	class FromSql
	{
	public:
		FromSql(GridBackendRow& p_Row, GridBackendColumn& p_Col);

		GridBackendField& operator()(const wstring& sqlValue, const std::unique_ptr<GridSnapshot::IDataCustomizer>& p_DataCustomizer);

	private:
		template <typename DataType>
		vector<DataType> DeserializeList(const wstring& p_Value, std::function<DataType(const wstring&)> p_SingleValueDeserializer);

	private:
		GridBackendRow& m_Row;
		GridBackendColumn& m_Col;
	};
}