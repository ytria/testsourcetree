#include "GridSnapshotConstants.h"

namespace GridSnapshot
{
	const int Constants::g_Version = 1;
	const std::string Constants::g_EncryptionKeyBase = "sapio365";
	
	const wstring Constants::g_TableMetadata = _YTEXT("Metadata");
	const wstring Constants::g_TableData = _YTEXT("Data");

	const wstring Constants::g_True = _YTEXT("1");
	const wstring Constants::g_False = _YTEXT("0");
	const wstring Constants::g_BoolUnset = _YTEXT("3");

	const wstring Constants::g_AdditionalDataId = _YTEXT("|||add|||"); // Ensure this doesn't collide with any column unique id.
}
