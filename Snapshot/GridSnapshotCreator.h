#pragma once

#include "GridSnapshotUtil.h"
#include "SQLiteEngine.h"

class DlgProgress;
class GridBackendRow;
namespace GridSnapshot
{
	// Only to be used by GridSnapshot::Create
	class Annotations;
	class Creator
	{
	public:
		Creator(std::unique_ptr<SQLiteEngine>&& p_Engine, CacheGrid& p_Grid, const Options& p_Options, std::function<bool(GridBackendRow*)> p_CustomRowFilter = nullptr);
		virtual ~Creator();

		bool CreateFromGrid(const wstring& p_Product, CustomMetadata&& p_CustomMetadata, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer, std::shared_ptr<DlgProgress> p_Prog);

	private:
		void init();
		void saveRow(GridBackendRow& p_Row, const std::vector<GridBackendColumn*>& p_Columns, int p_SqlId, std::map<GridBackendRow*, int>& p_RowSqlIds, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer, Annotations& p_SnapshotAnnotation);
		void saveMetadata(const Metadata& p_Metadata);

	private:
		std::unique_ptr<SQLiteEngine> m_Engine;
		CacheGrid& m_Grid;
		const Options& m_Options;

		std::vector<GridBackendColumn*> m_GridColumns;

		sqlite3_stmt* m_SqlStatementSetMetadata;
		sqlite3_stmt* m_SqlStatementSetRowData;

		std::function<bool(GridBackendRow*)> m_CustomRowFilter;
	};
}