#include "GridSnapshotCreator.h"

#include "CacheGrid.h"
#include "CacheGridTools.h"
#include "DlgProgress.h"
#include "GridSnapshotAnnotations.h"
#include "GridSnapshotConstants.h"
#include "GridSnapshotRowHandler.h"
#include "SqlQueryPreparedStatement.h"

namespace GridSnapshot
{
	Creator::Creator(std::unique_ptr<SQLiteEngine>&& p_Engine, CacheGrid& p_Grid, const Options& p_Options, std::function<bool(GridBackendRow*)> p_CustomRowFilter/* = nullptr*/)
		: m_Engine(std::move(p_Engine))
		, m_Grid(p_Grid)
		, m_GridColumns(p_Grid.GetBackendColumns())
		, m_Options(p_Options)
		, m_CustomRowFilter(p_CustomRowFilter)
	{
		// Keep only desired or required columns
		m_GridColumns.erase(std::remove_if(m_GridColumns.begin(), m_GridColumns.end(), [this, &p_Grid](const auto col)
			{
				return nullptr == col
					|| (!col->HasFlag(GridBackendUtil::ALWAYSINSNAPSHOT)																// Keep flagged columns. For instance, GridDriveItems::m_ColOneDrivePath
						&&
						(col->HasFlag(GridBackendUtil::IGNOREINSNAPSHOT)																// Remove flagged columns. For instance, MV explosion ref columns (don't remove that flag as this column can be temporary part of pk)
						|| col->GetUniqueID() != GridBackendUtil::g_OBJECTTYPEuniqueID													// Always keep object type column (part of system columns)
							&& !p_Grid.IsColumnPK(col)																					// Always keep PK columns except MV ones (see above)
							&& !p_Grid.GetBackend().IsColumnGrouped(col)																// Always keep grouped by columns
							&& p_Grid.GetBackend().GetStatusColumn() != col																// Always keep status column (for errors only)
							&& !(col->GetMultiValueExplosionSisters().size() > 1 && col->GetMultiValueElderColumn() == col)				// Always keep MV elder columns, otherwise MV doesn't work. 
							&& (m_Grid.GetBackend().IsSystem(col)																		// Remove system columns
								|| !m_Options.m_IncludeHiddenColumns && !col->IsVisible()												// Remove hidden columns if required
								|| !m_Options.m_IncludePermanentCommentColumns && (col->IsAnnotation() && !col->IsAnnotationVolatile()) // Remove permanent comments if required
								)
							)
						)
					;
			}), m_GridColumns.end());

		init();
	}

	Creator::~Creator()
	{
		sqlite3_finalize(m_SqlStatementSetMetadata);
		sqlite3_finalize(m_SqlStatementSetRowData);
	}

	void Creator::init()
	{
		const auto queryStringCreateMetadataTable = _YTEXT(R"(
			CREATE TABLE "Metadata" (
				"Id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, 
				"version" TEXT,
				"product" TEXT,
				"sessionRef" TEXT,
				"access" INTEGER,
				"mode" INTEGER,
				"autoName" TEXT,
				"criteria" TEXT,
				"xmlConfig" TEXT,
				"processesSetup" TEXT,
				"creationDate" TEXT,
				"creatorName" TEXT,
				"creatorTechName" TEXT,
				"flags" INTEGER,
				"handleSkuCount" INTEGER,
				"moduleOptions" TEXT); 
		)");

		wostringstream queryStringCreateDataTable;
		queryStringCreateDataTable
			<< _YTEXT(R"(CREATE TABLE "Data" ()")
			<< _YTEXT(R"("_has_children_" INTEGER, "_parent_id_" INTEGER, "_sql_id_" INTEGER NOT NULL PRIMARY KEY UNIQUE)");

		wstring sqlStatementStringSetData = _YTEXT(R"(INSERT OR REPLACE INTO "Data")");

		wstring columns = _YTEXT("('_has_children_', '_parent_id_', '_sql_id_',");
		wstring values = wstring(_YTEXT("VALUES(@HSC, @PID, @SID,"));

		const auto numberPadding = m_GridColumns.size() > 0 ? static_cast<int>(std::floor(std::log10(m_GridColumns.size()))) : 1;

		size_t i = 0;
		for (const auto& col : m_GridColumns)
		{
			if (!col->IsAnnotation()) // Handled separately
			{
				queryStringCreateDataTable
					<< _YTEXT(", \"")
					<< col->GetUniqueID()
					<< _YTEXT(R"(" TEXT)");

				columns += _YTEXT("'") + col->GetUniqueID() + _YTEXT("',");
				values += _YTEXT("@") + Str::getStringFromNumber(i++, numberPadding) + _YTEXT(",");
			}
		}

		{
			queryStringCreateDataTable
				<< _YTEXT(", \"")
				<< Constants::g_AdditionalDataId
				<< _YTEXT(R"(" TEXT)");
			columns += _YTEXT("'") + Constants::g_AdditionalDataId + _YTEXT("',");
			values += _YTEXT("@") + Str::getStringFromNumber(i++, numberPadding) + _YTEXT(",");
		}

		queryStringCreateDataTable << _YTEXT(R"(, FOREIGN KEY("_parent_id_") REFERENCES "Data"("_sql_id_"));)");

		// Replace trailing commas by parenthesis
		columns.back() = _YTEXT(')');
		values.back() = _YTEXT(')');
		sqlStatementStringSetData += columns + _YTEXT(" ") + values;


		// Create tables
		{
			{
				SqlQueryPreparedStatement queryCreateMetadataTable(*m_Engine, queryStringCreateMetadataTable, false);
				if (!m_Engine->GetError().empty())
					return;
				queryCreateMetadataTable.Run();
			}

			{
				SqlQueryPreparedStatement queryCreateDataTable(*m_Engine, queryStringCreateDataTable.str(), false);
				if (!m_Engine->GetError().empty())
					return;
				queryCreateDataTable.Run();
			}
		}

		// Prepare statements
		{
			CString query;
			query.Format(L"INSERT OR REPLACE INTO %s(version, product, sessionRef, mode, access, autoName, criteria, xmlConfig, processesSetup, creationDate, creatorName, creatorTechName, flags, handleSkuCount, moduleOptions) VALUES(@VER, @PRO, @SRE, @MOD, @ACC, @AUT, @CRT, @CFG, @PST, @CDT, @NAM, @TNM, @FLG, @HSC, @MOP)", Constants::g_TableMetadata.c_str());
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII((LPCTSTR)query);
			const auto res = sqlite3_prepare_v2(m_Engine->GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementSetMetadata, nullptr);
			ASSERT(res == SQLITE_OK);
		}
		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(sqlStatementStringSetData);
			const auto res = sqlite3_prepare_v2(m_Engine->GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementSetRowData, nullptr);
			ASSERT(res == SQLITE_OK);
		}
	}

	void Creator::saveRow(GridBackendRow& p_Row, const std::vector<GridBackendColumn*>& p_Columns, int p_SqlId, std::map<GridBackendRow*, int>& p_RowSqlIds, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer, Annotations& p_SnapshotAnnotation)
	{
		auto rowValues = RowHandler::GetValues(p_Row, p_Columns, p_DataCustomizer);
		ASSERT(!rowValues.empty());
		if (!rowValues.empty())
		{
			int index = 1;

			{
				if (p_Row.HasChildrenRows())
					sqlite3_bind_int(m_SqlStatementSetRowData, index++, 1);
				else
					sqlite3_bind_null(m_SqlStatementSetRowData, index++);
			}

			{
				auto parent = p_Row.GetParentRow();
				if (nullptr != parent)
				{
					auto it = p_RowSqlIds.find(parent);
					ASSERT(p_RowSqlIds.end() != it);
					if (p_RowSqlIds.end() != it)
						sqlite3_bind_int(m_SqlStatementSetRowData, index++, it->second);
					else
						sqlite3_bind_null(m_SqlStatementSetRowData, index++);
				}
				else
				{
					sqlite3_bind_null(m_SqlStatementSetRowData, index++);
				}
			}

			sqlite3_bind_int(m_SqlStatementSetRowData, index++, p_SqlId);
			for (const auto& col : p_Columns)
			{
				if (!col->IsAnnotation())
				{
					auto it = rowValues.find(col->GetUniqueID());
					if (rowValues.end() != it)
						sqlite3_bind_text(m_SqlStatementSetRowData, index++, Str::convertToUTF8(it->second).c_str(), -1, SQLITE_TRANSIENT);
					else
						sqlite3_bind_null(m_SqlStatementSetRowData, index++);
				}
			}

			{
				auto it = rowValues.find(Constants::g_AdditionalDataId);
				if (rowValues.end() != it)
					sqlite3_bind_text(m_SqlStatementSetRowData, index++, Str::convertToUTF8(it->second).c_str(), -1, SQLITE_TRANSIENT);
				else
					sqlite3_bind_null(m_SqlStatementSetRowData, index++);
			}

			const auto result = m_Engine->sqlite3_step(m_SqlStatementSetRowData);
			ASSERT(result == SQLITE_DONE);
			m_Engine->ProcessError(result, nullptr, _YTEXT(""));
			sqlite3_reset(m_SqlStatementSetRowData);

			// Handle annotations
			vector<GridAnnotation> annot;
			m_Grid.AnnotationsGet(&p_Row, p_Columns, annot);
			for (const auto& a : annot)
			{
				// FIXME: Optimize this?
				// we need to filter as AnnotationsGet returns all annotations whose source column is in p_Columns (even if annotation column is not in it)
				if (!a.IsRemoved() && p_Columns.end() != std::find_if(p_Columns.begin(), p_Columns.end(), [&a](auto& p_Col)
					{
						return p_Col->IsAnnotation()
							&& a.IsVolatile() == p_Col->IsAnnotationVolatile()
							&& nullptr != p_Col->GetAnnotationColumnSource()
							&& p_Col->GetAnnotationColumnSource()->GetUniqueID() == a.m_ColumnID;
					}))
					p_SnapshotAnnotation.Save(p_SqlId, m_Grid, &p_Row, a, Options::Mode::Photo == m_Options.m_Mode);
			}
		}
	}

	void Creator::saveMetadata(const Metadata& p_Metadata)
	{
		int index = 1;

		ASSERT(!p_Metadata.m_Version); // Current Constants::g_Version will be used anyway.
		const auto version = Str::convertToUTF8(Str::getStringFromNumber(Constants::g_Version));
		sqlite3_bind_text(m_SqlStatementSetMetadata, index++, version.c_str(), -1, SQLITE_STATIC);

		const auto product = p_Metadata.m_Product ? Str::convertToUTF8(*p_Metadata.m_Product) : std::string();
		if (p_Metadata.m_Product)
			sqlite3_bind_text(m_SqlStatementSetMetadata, index++, product.c_str(), -1, SQLITE_STATIC);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		const auto sessionRef = p_Metadata.m_SessionRef ? Str::convertToUTF8(*p_Metadata.m_SessionRef) : std::string();
		if (p_Metadata.m_SessionRef)
			sqlite3_bind_text(m_SqlStatementSetMetadata, index++, sessionRef.c_str(), -1, SQLITE_STATIC);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		if (p_Metadata.m_Mode)
			sqlite3_bind_int(m_SqlStatementSetMetadata, index++, (int)*p_Metadata.m_Mode);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		if (p_Metadata.m_Access)
			sqlite3_bind_int(m_SqlStatementSetMetadata, index++, (int)*p_Metadata.m_Access);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		const auto autoName = p_Metadata.m_GridAutomationName ? Str::convertToUTF8(*p_Metadata.m_GridAutomationName) : std::string();
		if (p_Metadata.m_GridAutomationName)
			sqlite3_bind_text(m_SqlStatementSetMetadata, index++, autoName.c_str(), -1, SQLITE_STATIC);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		const auto criteria = p_Metadata.m_Criteria ? Str::convertToUTF8(*p_Metadata.m_Criteria) : std::string();
		if (p_Metadata.m_Criteria)
			sqlite3_bind_text(m_SqlStatementSetMetadata, index++, criteria.c_str(), -1, SQLITE_STATIC);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		if (p_Metadata.m_XmlConfig)
			sqlite3_bind_text(m_SqlStatementSetMetadata, index++, p_Metadata.m_XmlConfig->c_str(), -1, SQLITE_STATIC);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		const auto processesSetup = p_Metadata.m_ProcessesSetup ? Str::convertToUTF8(*p_Metadata.m_ProcessesSetup) : std::string();
		if (p_Metadata.m_ProcessesSetup)
			sqlite3_bind_text(m_SqlStatementSetMetadata, index++, processesSetup.c_str(), -1, SQLITE_STATIC);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		const auto date = p_Metadata.m_CreationDate ? Str::convertToUTF8(TimeUtil::GetInstance().GetISO8601String(*p_Metadata.m_CreationDate, TimeUtil::ISO8601_HAS_DATE_AND_TIME, false)) : std::string();
		if (p_Metadata.m_CreationDate)
			sqlite3_bind_text(m_SqlStatementSetMetadata, index++, date.c_str(), -1, SQLITE_STATIC);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		const auto name = p_Metadata.m_CreatorName ? Str::convertToUTF8(*p_Metadata.m_CreatorName) : std::string();
		if (p_Metadata.m_CreatorName)
			sqlite3_bind_text(m_SqlStatementSetMetadata, index++, name.c_str(), -1, SQLITE_STATIC);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		const auto techName = p_Metadata.m_CreatorTechnicalName ? Str::convertToUTF8(*p_Metadata.m_CreatorTechnicalName) : std::string();
		if (p_Metadata.m_CreatorTechnicalName)
			sqlite3_bind_text(m_SqlStatementSetMetadata, index++, techName.c_str(), -1, SQLITE_STATIC);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		if (p_Metadata.m_Flags)
			sqlite3_bind_int(m_SqlStatementSetMetadata, index++, (int)*p_Metadata.m_Flags);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		if (p_Metadata.m_IsHandleSkuCounters)
			sqlite3_bind_int(m_SqlStatementSetMetadata, index++, *p_Metadata.m_IsHandleSkuCounters ? 1 : 0);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		const auto moduleOptions = p_Metadata.m_ModuleOptions ? Str::convertToUTF8(*p_Metadata.m_ModuleOptions) : std::string();
		if (p_Metadata.m_ModuleOptions)
			sqlite3_bind_text(m_SqlStatementSetMetadata, index++, moduleOptions.c_str(), -1, SQLITE_STATIC);
		else
			sqlite3_bind_null(m_SqlStatementSetMetadata, index++);

		const auto result = m_Engine->sqlite3_step(m_SqlStatementSetMetadata);
		ASSERT(result == SQLITE_DONE);
		m_Engine->ProcessError(result, nullptr, _YTEXT(""));
		sqlite3_reset(m_SqlStatementSetMetadata);
	}

	bool Creator::CreateFromGrid(const wstring& p_Product, CustomMetadata&& p_CustomMetadata, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer, std::shared_ptr<DlgProgress> p_Prog)
	{
		if (p_Prog)
		{
			p_Prog->setRange(0, 3);
			p_Prog->setText(_T("Metadata..."), false);
		}

		Metadata metadata;
		metadata.m_Product = p_Product;
		metadata.m_SessionRef = std::move(p_CustomMetadata.m_SessionRef);
		metadata.m_Mode = m_Options.m_Mode;
		metadata.m_Access = m_Options.m_Access;
		metadata.m_GridAutomationName = m_Grid.GetAutomationName();
		metadata.m_Criteria = std::move(p_CustomMetadata.m_CriteriaJson);
		metadata.m_CreationDate = YTimeDate::GetCurrentTimeDateGMT();
		metadata.m_CreatorName = std::move(p_CustomMetadata.m_CreatorName);
		metadata.m_CreatorTechnicalName = std::move(p_CustomMetadata.m_CreatorTechnicalName);
		metadata.m_Flags = std::move(p_CustomMetadata.m_Flags);
		metadata.m_IsHandleSkuCounters = std::move(p_CustomMetadata.m_IsHandleSkuCounters);
		metadata.m_ModuleOptions = std::move(p_CustomMetadata.m_ModuleOptionsJson);
		metadata.m_XmlConfig.emplace();

		if (p_Prog)
			p_Prog->IncrementGaugePosition(_T("Grid configuration..."), 1, false);
		m_Grid.GetGridConfigXMLString(*metadata.m_XmlConfig);

		if (p_Prog)
			p_Prog->IncrementGaugePosition(_T("Processes..."), 1, false);

		const bool restorePoint = Options::Mode::RestorePoint == m_Options.m_Mode;
		//if (restorePoint)
		{
			metadata.m_ProcessesSetup = m_Grid.SerializeProcessesSetup();
			if (metadata.m_ProcessesSetup->empty())
				metadata.m_ProcessesSetup.reset();
		}

		auto& columns = m_GridColumns;

		{
			if (p_Prog)
				p_Prog->IncrementGaugePosition(_T("Preparing rows..."), 1, false);
			Annotations snapshotAnnotation(*m_Engine, false);

			m_Engine->StartTransaction();

			saveMetadata(metadata);

			auto process = [this, &columns, &p_DataCustomizer, &snapshotAnnotation, &p_Prog, restorePoint]()
			{
				const auto& rows = /*restorePoint ? */m_Grid.GetBackendRows()/* : m_Grid.GetBackend().GetBackendRowsForDisplay()*/;
				if (p_Prog)
				{
					p_Prog->setRange(0, static_cast<int>(rows.size()));
					p_Prog->setGaugePosition(0);
				}
				std::map<GridBackendRow*, int> rowSqlIds;
				int sqlId = 1;
				for (auto row : rows)
				{
					if (p_Prog)
						p_Prog->IncrementGaugePosition(_YFORMAT(L"Saving row %s/%s", std::to_wstring(sqlId).c_str(), std::to_wstring(rows.size()).c_str()).c_str(), 1, false);

					ASSERT(nullptr != row);
					if (nullptr != row
						&& (!m_CustomRowFilter || m_CustomRowFilter(row))
						&& !row->IsComparatorAdded()
						&& (m_Options.m_IncludeHiddenRows || row->IsInGrid())
						)
					{
						ASSERT(!row->IsGroupRow());
						saveRow(*row, columns, sqlId, rowSqlIds, p_DataCustomizer, snapshotAnnotation);
						if (row->HasChildrenRows())
							rowSqlIds[row] = sqlId;
						++sqlId;
					}
				}
			};

			//if (restorePoint)
			{
				TemporarilyImplodeNonGroupRowsAndExec(m_Grid
					, TemporarilyImplodeNonGroupRowsAndExec::ALL_ROWS
					, [process](const bool p_HasRowsToReExplode)
					{
						process();
					})();
			}
			/*else
			{
				process();
			}*/


			m_Engine->EndTransaction();
		}

		return m_Engine->GetError().empty();
	}
}