#include "GridSnapshotRowHandler.h"

#include "CacheGrid.h"
#include "GridSnapshotConstants.h"
#include "GridSnapshotFromSql.h"
#include "GridSnapshotToSql.h"

namespace GridSnapshot
{
	void RowHandler::Parse(GridBackendRow& p_Row, CacheGrid& p_Grid, sqlite3_stmt* p_Stmt, std::function<void(const std::string&, const boost::YOpt<wstring>&)> p_UnknownHandler, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer)
	{
		const auto numCols = sqlite3_column_count(p_Stmt);
		for (auto c = 0; c < numCols; ++c)
		{
			const auto propertyName = sqlite3_column_name(p_Stmt, c);
			const auto propertyNameW = Str::convertFromUTF8(propertyName);
			const auto valuePtr = sqlite3_column_text(p_Stmt, c);
			const auto value = nullptr == valuePtr ? boost::none : boost::YOpt<wstring>(Str::convertFromUTF8(reinterpret_cast<const char*>(valuePtr)));

			if (Constants::g_AdditionalDataId == propertyNameW)
			{
				if (value)
					p_DataCustomizer->SetAdditionalValue(*value, p_Row);
			}
			else
			{
				auto col = p_Grid.GetColumnByUniqueID(propertyNameW);
				if (nullptr != col)
				{
					if (value)
						FromSql(p_Row, *col)(*value, p_DataCustomizer);
				}
				else if (p_UnknownHandler)
					p_UnknownHandler(propertyName, value);
			}
		}
	}

	std::map<wstring, wstring> RowHandler::GetValues(GridBackendRow& p_Row, std::vector<GridBackendColumn*> p_Columns, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer)
	{
		std::map<wstring, wstring> values;
		for (auto col : p_Columns)
		{
			ASSERT(nullptr != col);
			if (nullptr != col && !col->IsAnnotation()) // Annotations handled separately
				values[col->GetUniqueID()] = ToSql(p_Row.GetField(col), *col)(p_DataCustomizer);
		}

		wstring val;
		if (p_DataCustomizer->GetAdditionalValue(val, p_Row))
		{
			ASSERT(values[Constants::g_AdditionalDataId].empty());
			values[Constants::g_AdditionalDataId] = val;
		}

		return values;
	}
}