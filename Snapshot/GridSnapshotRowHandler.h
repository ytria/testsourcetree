#pragma once

#include "GridSnapshot.h"

namespace GridSnapshot
{
	class RowHandler
	{
	public:
		static void Parse(GridBackendRow& p_Row, CacheGrid& p_Grid, sqlite3_stmt* p_Stmt, std::function<void(const std::string&, const boost::YOpt<wstring>&)> p_UnknownHandler, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer);
		static std::map<wstring, wstring> GetValues(GridBackendRow& p_Row, std::vector<GridBackendColumn*> p_Columns, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer);
	};
}