#pragma once

#include "GridBackendColumn.h"

namespace GridSnapshot
{
	class IDataCustomizer
	{
	public:
		virtual ~IDataCustomizer() = default;
		virtual bool GetValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) = 0;
		virtual bool SetValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) = 0;

		// Any info about the row which is not stored in a field (like LParam).
		virtual bool GetAdditionalValue(wstring& p_Value, GridBackendRow& p_Row) = 0;
		virtual bool SetAdditionalValue(const wstring& p_Value, GridBackendRow& p_Row) = 0;
	};

	struct Options
	{
		enum class Mode : uint32_t { Photo = 0, RestorePoint = 1 } m_Mode = Mode::Photo;
		enum class Access : uint32_t { Tenant = 0, Public = 1 } m_Access = Access::Tenant;
		bool m_IncludeHiddenRows = true;
		bool m_IncludeHiddenColumns = true;
		bool m_IncludePermanentCommentColumns = true;
	};

	enum Flags : uint32_t
	{
		MyData = 0x1,
		// ...
	};

	struct CustomMetadata // For snapshot creation only
	{
		boost::YOpt<wstring> m_SessionRef;
		boost::YOpt<wstring> m_CreatorName;
		boost::YOpt<wstring> m_CreatorTechnicalName;
		boost::YOpt<wstring> m_CriteriaJson;
		boost::YOpt<uint32_t> m_Flags;
		boost::YOpt<bool> m_IsHandleSkuCounters; // For Licenses modules
		boost::YOpt<wstring> m_ModuleOptionsJson; //For modules with options (like messages, events, etc.)
	};

	struct Metadata
	{
		boost::YOpt<int> m_Version;
		boost::YOpt<wstring> m_Product;
		boost::YOpt<wstring> m_SessionRef; // Tenant
		boost::YOpt<Options::Mode> m_Mode;
		boost::YOpt<Options::Access> m_Access;
		boost::YOpt<wstring> m_GridAutomationName;
		boost::YOpt<wstring> m_Criteria; // Json serialized
		boost::YOpt<std::string> m_XmlConfig;
		boost::YOpt<wstring> m_ProcessesSetup;

		boost::YOpt<YTimeDate> m_CreationDate;
		boost::YOpt<wstring> m_CreatorName; // DisplayName
		boost::YOpt<wstring> m_CreatorTechnicalName; // Username

		boost::YOpt<uint32_t> m_Flags;

		boost::YOpt<bool> m_IsHandleSkuCounters; // For Licenses modules
		boost::YOpt<wstring> m_ModuleOptions; // Json serialized - For modules with options (like messages, events, etc.)
	};
}