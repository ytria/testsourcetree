#pragma once

namespace GridSnapshot
{
	struct Constants
	{
		static const int g_Version;
		static const std::string g_EncryptionKeyBase;

		static const wstring g_TableMetadata;
		static const wstring g_TableData;

		static const wstring g_True;
		static const wstring g_False;
		static const wstring g_BoolUnset;

		static const wstring g_AdditionalDataId;

		// No more than 2 digits + must be greater that bigger GridBackendUtil::DATATYPE
		static constexpr int g_ErrorFieldDataType = 99;
		static constexpr int g_MultiErrorFieldDataType = 98;
	};
}