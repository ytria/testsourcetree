#include "GridSnapshotToSql.h"

#include "GridBackendField.h"
#include "GridSnapshotConstants.h"

namespace GridSnapshot
{
	ToSql::ToSql(GridBackendField& p_Field, GridBackendColumn& p_Col)
		: m_Field(p_Field)
		, m_Col(p_Col)
	{

	}

	wstring ToSql::operator()(const std::unique_ptr<IDataCustomizer>& p_DataCustomizer)
	{
		ASSERT(!m_Col.HasFlag(GridBackendUtil::IGNOREINSNAPSHOT));

		wstring serialized;

		const bool isErrorField = m_Field.GetIcon() == GridBackendUtil::ICON_ERROR;
		// Max 15 types + 2 constants -> 2 digits
		if (isErrorField && GridBackendUtil::STRING_LIST == m_Field.GetDataType())
			serialized += Str::getStringFromNumber(Constants::g_MultiErrorFieldDataType, 2);
		else if (isErrorField)
		{
			ASSERT(GridBackendUtil::STRING == m_Field.GetDataType()); // Handle other types?
			serialized += Str::getStringFromNumber(Constants::g_ErrorFieldDataType, 2);
		}
		else
			serialized += Str::getStringFromNumber(m_Field.GetDataType(), 2);

		wstring valueStr;
		if (isErrorField // Error field are only generically handled (no call to customizer)
			|| !p_DataCustomizer || !p_DataCustomizer->GetValue(valueStr, m_Field, m_Col))
		{
			switch (m_Field.GetDataType())
			{
			case GridBackendUtil::UNKNOWN:
				//ASSERT(false); //?
				break;
			case GridBackendUtil::STRING:
				valueStr = m_Field.GetValueStr();
				break;
			case GridBackendUtil::STRING_LIST:
				valueStr = SerializeList<PooledString>(m_Field.GetValuesMulti<PooledString>(), [](const auto& str) { return (wstring)str; });
				break;
			case GridBackendUtil::DOUBLE:
				valueStr = Str::getStringFromDoubleRaw(m_Field.GetValueDouble());
				break;
			case GridBackendUtil::LONG:
				valueStr = Str::getStringFromNumber(static_cast<int64_t>(m_Field.GetValueDouble()));// FIXME: Can be 64-bits. Will be clarified when grid correctly handles 64bits integers.
				break;
			case GridBackendUtil::ULONG:
				valueStr = Str::getStringFromNumber(static_cast<uint64_t>(m_Field.GetValueDouble()));// FIXME: Can be 64-bits. Will be clarified when grid correctly handles 64bits integers.
				break;
			case GridBackendUtil::NUMBER_LIST_DOUBLE:
				valueStr = SerializeList<double>(m_Field.GetValuesMulti<double>(), [](const auto& num) { return Str::getStringFromDoubleRaw(num); });
				break;
			case GridBackendUtil::NUMBER_LIST_LONG:
				valueStr = SerializeList<__int3264>(m_Field.GetValuesMulti<__int3264>(), [](const auto& num) { return Str::getStringFromNumber(num); });
				break;
			case GridBackendUtil::DATE:
			{
				auto mode = TimeUtil::ISO8601_HAS_DATE_AND_TIME;
				if (m_Field.GetCellType() == GridBackendUtil::CELL_TYPE_TIME)
					mode = TimeUtil::ISO8601_HAS_TIME;
				ASSERT(DefaultValue<YTimeDate>::val() != m_Field.GetValueTimeDate()); // Why is this field's datatype DATE?
				valueStr = TimeUtil::GetInstance().GetISO8601String(m_Field.GetValueTimeDate(), mode, false).c_str();
				break;
			}
			case GridBackendUtil::DATE_LIST:
			{
				auto mode = TimeUtil::ISO8601_HAS_DATE_AND_TIME;
				if (m_Field.GetCellType() == GridBackendUtil::CELL_TYPE_TIME)
					mode = TimeUtil::ISO8601_HAS_TIME;
				valueStr = SerializeList<YTimeDate>(m_Field.GetValuesMulti<YTimeDate>(), [mode](const auto& timedate)
					{
						if (GridBackendUtil::g_NoValueDate == timedate)
							return GridBackendUtil::g_NoValueString;
						return TimeUtil::GetInstance().GetISO8601String(timedate, mode, false);
					});
				break;
			}
			case GridBackendUtil::BOOL:
				if (m_Field.GetValueBool())
					valueStr = Constants::g_True;
				else
					valueStr = Constants::g_False;
				break;
			case GridBackendUtil::VARIANT:
				ASSERT(false); // ??
				break;
			case GridBackendUtil::HYPERLINK:
				valueStr = m_Field.GetValueStr();
				break;
			case GridBackendUtil::HYPERLINK_LIST:
				valueStr = SerializeList<HyperlinkItem>(m_Field.GetValuesMulti<HyperlinkItem>(), [](const auto& hyperlink) { return hyperlink.m_URL; });
				break;
			case GridBackendUtil::BOOL_LIST:
				valueStr = SerializeList<BOOLItem>(m_Field.GetValuesMulti<BOOLItem>(), [](const auto& boolItem)
					{
						return TRUE == boolItem.m_BOOL
							? Constants::g_True
							: FALSE == boolItem.m_BOOL ? Constants::g_False
							: Constants::g_BoolUnset;
					});
				break;
			case GridBackendUtil::RICHTEXT:
				valueStr = m_Field.GetValueStr();
				break;
			default:
				break;
			}
		}

		serialized += valueStr;
		return serialized;
	}

	template <typename DataType>
	wstring ToSql::SerializeList(const vector<DataType>* p_Value, std::function<wstring(const DataType&)> p_SingleValueSerializer)
	{
		ASSERT(p_SingleValueSerializer);
		auto list = vector<web::json::value>();
		if (nullptr != p_Value)
		{
			list.reserve(p_Value->size());
			for (const auto& elem : *p_Value)
				list.push_back(web::json::value::string(p_SingleValueSerializer(elem)));
		}
		return web::json::value::array(list).serialize();
	}
}
