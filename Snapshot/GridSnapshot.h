#pragma once

#include "GridSnapshotLoader.h"
#include "GridSnapshotUtil.h"
#include "SQLiteEngine.h"

namespace GridSnapshot
{
	bool Create(CacheGrid& p_Grid
		, const wstring& p_FilePath
		, const std::string& p_Password
		, CustomMetadata&& p_CustomMetadata
		, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer
		, const Options& p_Options
		, std::function<bool(GridBackendRow*)> p_CustomRowFilter = nullptr);

	std::shared_ptr<Loader> GetLoader(const wstring& p_FilePath, const std::string& p_Password);
}
