#pragma once

#include "GridSnapshot.h"
#include "Str.h"

namespace GridSnapshot
{
	class ToSql
	{
	public:
		ToSql(GridBackendField& p_Field, GridBackendColumn& p_Col);
		wstring operator()(const std::unique_ptr<GridSnapshot::IDataCustomizer>& p_DataCustomizer);

	private:
		template <typename DataType>
		wstring SerializeList(const vector<DataType>* p_Value, std::function<wstring(const DataType&)> p_SingleValueSerializer);

	private:
		GridBackendField& m_Field;
		GridBackendColumn& m_Col;
	};
}
