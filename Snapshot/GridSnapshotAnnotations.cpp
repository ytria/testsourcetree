#include "GridSnapshotAnnotations.h"

#include "CacheGrid.h"
#include "CacheGridColumnOrg.h"
#include "CacheGridColumnOrgCtrl.h"
#include "GridSnapshotFromSql.h"
#include "GridSnapshotToSql.h"
#include "SqlQueryPreparedStatement.h"
#include "TimeUtil.h"

namespace GridSnapshot
{
	Annotations::Annotations(SQLiteEngine& p_Engine, bool p_ForLoad)
		: m_Engine(p_Engine)
		, m_SaveStmt(nullptr)
		, m_LoadStmt(nullptr)
		, m_ForLoad(p_ForLoad)
	{
		init();
		if (m_ForLoad)
			m_RandomNumber.emplace();
	}

	Annotations::~Annotations()
	{
		if (nullptr != m_SaveStmt)
			sqlite3_finalize(m_SaveStmt);
		if (nullptr != m_LoadStmt)
			sqlite3_finalize(m_LoadStmt);
	}

	bool Annotations::init()
	{
		if (m_ForLoad)
			return prepareLoadStatement(m_Engine.GetDbHandle(), m_LoadStmt);

		return createTable(m_Engine)
			&& prepareSaveStatement(m_Engine.GetDbHandle(), m_SaveStmt);
	}

	bool Annotations::Save(int64_t p_RowId, CacheGrid& p_Grid, GridBackendRow* p_Row, const GridAnnotation& p_GridAnnotation, bool p_PhotoMode)
	{
		ASSERT(!m_ForLoad);
		ASSERT(nullptr != m_SaveStmt);
		if (nullptr == m_SaveStmt)
			return false;

		int index = 1;

		ASSERT(p_GridAnnotation.m_ID);
		if (p_GridAnnotation.m_ID)
			sqlite3_bind_int64(m_SaveStmt, index++, *p_GridAnnotation.m_ID);
		else
			sqlite3_bind_null(m_SaveStmt, index++);

		if (p_GridAnnotation.m_Status)
			sqlite3_bind_int(m_SaveStmt, index++, *p_GridAnnotation.m_Status);
		else
			sqlite3_bind_null(m_SaveStmt, index++);

		const auto date = Str::convertToUTF8(TimeUtil::GetInstance().GetISO8601String(p_GridAnnotation.m_UpdDate, TimeUtil::ISO8601_HAS_DATE_AND_TIME, false));
		sqlite3_bind_text(m_SaveStmt, index++, date.c_str(), -1, SQLITE_STATIC);

		auto upById = Str::convertToUTF8(p_GridAnnotation.m_UpdateByID);
		sqlite3_bind_text(m_SaveStmt, index++, upById.c_str(), -1, SQLITE_STATIC);

		auto upByName = Str::convertToUTF8(p_GridAnnotation.m_UpdateByName);
		sqlite3_bind_text(m_SaveStmt, index++, upByName.c_str(), -1, SQLITE_STATIC);

		auto upByEmail = Str::convertToUTF8(p_GridAnnotation.m_UpdateByPrincipalName);
		sqlite3_bind_text(m_SaveStmt, index++, upByEmail.c_str(), -1, SQLITE_STATIC);

		auto tenant = Str::convertToUTF8(p_GridAnnotation.m_TenantName);
		sqlite3_bind_text(m_SaveStmt, index++, tenant.c_str(), -1, SQLITE_STATIC);

		auto ref = Str::convertToUTF8(p_GridAnnotation.m_ReferenceName);
		sqlite3_bind_text(m_SaveStmt, index++, ref.c_str(), -1, SQLITE_STATIC);

		auto rowPk = Str::convertToUTF8(p_GridAnnotation.m_RowPK);
		sqlite3_bind_text(m_SaveStmt, index++, rowPk.c_str(), -1, SQLITE_STATIC);

		auto colId = Str::convertToUTF8(p_GridAnnotation.m_ColumnID);
		sqlite3_bind_text(m_SaveStmt, index++, colId.c_str(), -1, SQLITE_STATIC);

		auto col = p_Grid.GetColumnByUniqueID(p_GridAnnotation.m_ColumnID);
		ASSERT(nullptr != col);
		auto annotCol = nullptr != col
			? (p_GridAnnotation.IsVolatile() ? col->GetAnnotationColumnVolatile()
											: col->GetAnnotationColumn())
			: nullptr;
		ASSERT(nullptr != annotCol);
		auto text = Str::convertToUTF8(p_PhotoMode && nullptr != annotCol ? ToSql(p_Row->GetField(annotCol), *annotCol)(nullptr) : p_GridAnnotation.m_Text);
		sqlite3_bind_text(m_SaveStmt, index++, text.c_str(), -1, SQLITE_STATIC);

		sqlite3_bind_int(m_SaveStmt, index++, p_GridAnnotation.m_Color);

		ASSERT(p_GridAnnotation.m_Flags <= (uint64_t)(std::numeric_limits<int64_t>::max)());
		sqlite3_bind_int64(m_SaveStmt, index++, (int64_t)p_GridAnnotation.m_Flags);

		if (p_GridAnnotation.IsVolatile())
			sqlite3_bind_int64(m_SaveStmt, index++, 1);
		else
			sqlite3_bind_int64(m_SaveStmt, index++, 0);

		sqlite3_bind_int64(m_SaveStmt, index++, p_RowId);

		const auto result = m_Engine.sqlite3_step(m_SaveStmt);
		ASSERT(result == SQLITE_DONE);
		m_Engine.ProcessError(result, nullptr, _YTEXT(""));
		sqlite3_reset(m_SaveStmt);
		return result == SQLITE_DONE;
	}

	bool Annotations::Load(int64_t p_RowId, CacheGrid& p_Grid, GridBackendRow* p_Row, bool p_PhotoMode)
	{
		ASSERT(m_ForLoad);

		auto res = sqlite3_bind_int64(m_LoadStmt, 1, p_RowId);

		auto result = m_Engine.sqlite3_step(m_LoadStmt);
		while (result == SQLITE_ROW)
		{
			GridAnnotation a;

			int index = 0;

			a.m_ID = sqlite3_column_int64(m_LoadStmt, index++);

			{
				const auto status = sqlite3_column_int(m_LoadStmt, index++);
				ASSERT(AnnotationUtil::AnnotationStatus::STATUS_OK <= status && status <= AnnotationUtil::AnnotationStatus::STATUS_LOCKED);
				if (AnnotationUtil::AnnotationStatus::STATUS_OK <= status && status <= AnnotationUtil::AnnotationStatus::STATUS_LOCKED)
					a.m_Status = (AnnotationUtil::AnnotationStatus)status;
			}

			{
				auto date = reinterpret_cast<const char*>(sqlite3_column_text(m_LoadStmt, index++));
				if (nullptr != date)
					TimeUtil::GetInstance().GetTimedateFromISO8601String(Str::convertFromUTF8(date), a.m_UpdDate);
			}

			{
				auto upById = reinterpret_cast<const char*>(sqlite3_column_text(m_LoadStmt, index++));
				if (nullptr != upById)
					a.m_UpdateByID = Str::convertFromUTF8(upById);
			}

			{
				auto upByName = reinterpret_cast<const char*>(sqlite3_column_text(m_LoadStmt, index++));
				if (nullptr != upByName)
					a.m_UpdateByName = Str::convertFromUTF8(upByName);
			}

			{
				auto upByPrincName = reinterpret_cast<const char*>(sqlite3_column_text(m_LoadStmt, index++));
				if (nullptr != upByPrincName)
					a.m_UpdateByPrincipalName = Str::convertFromUTF8(upByPrincName);
			}

			{
				auto tenant = reinterpret_cast<const char*>(sqlite3_column_text(m_LoadStmt, index++));
				if (nullptr != tenant)
					a.m_TenantName = Str::convertFromUTF8(tenant);
			}

			{
				auto ref = reinterpret_cast<const char*>(sqlite3_column_text(m_LoadStmt, index++));
				if (nullptr != ref)
					a.m_ReferenceName = Str::convertFromUTF8(ref);
			}

			{
				auto rowPk = reinterpret_cast<const char*>(sqlite3_column_text(m_LoadStmt, index++));
				if (nullptr != rowPk)
					a.m_RowPK = Str::convertFromUTF8(rowPk);
			}

			{
				auto colId = reinterpret_cast<const char*>(sqlite3_column_text(m_LoadStmt, index++));
				if (nullptr != colId)
					a.m_ColumnID = Str::convertFromUTF8(colId);
			}

			{
				auto text = reinterpret_cast<const char*>(sqlite3_column_text(m_LoadStmt, index++));
				if (nullptr != text)
					a.m_Text = Str::convertFromUTF8(text);
			}

			a.m_Color = sqlite3_column_int(m_LoadStmt, index++);
			a.m_Flags = static_cast<uint64_t>(sqlite3_column_int64(m_LoadStmt, index++));
			
			a.SetVolatile(1 == sqlite3_column_int64(m_LoadStmt, index++));
			
			const auto sqlRowId = sqlite3_column_int64(m_LoadStmt, index++);

			ASSERT(p_RowId == sqlRowId);

			ASSERT(m_RandomNumber);
			add(p_Grid, p_Row, a, p_PhotoMode, *m_RandomNumber);

			result = m_Engine.sqlite3_step(m_LoadStmt);
		}

		ASSERT(result == SQLITE_DONE);
		m_Engine.ProcessError(result, nullptr, _YTEXT(""));
		sqlite3_reset(m_LoadStmt);
		return result == SQLITE_DONE;
	}

	bool Annotations::createTable(SQLiteEngine& p_Engine)
	{
		const auto query = _YTEXT(R"(
		CREATE TABLE "Annotations" (
			"SAPIO365ROWID"	INTEGER NOT NULL PRIMARY KEY, 
			"Status" INTEGER,
			"Date" TEXT,
			"Updated_By_ID" TEXT,
			"Udpated_By" TEXT,
			"Udpated_By_Email" TEXT,
			"Tenant" TEXT,
			"Reference" TEXT,
			"Grid_Row_PK" TEXT,
			"Column_ID" TEXT,
			"Text" TEXT,
			"Color" INTEGER,
			"Flags" INTEGER,
			"Volatile" INTEGER,
			"_data_sql_id_" INTEGER,
			FOREIGN KEY("_data_sql_id_") REFERENCES "Data"("_sql_id_")); 
		)");

		SqlQueryPreparedStatement queryCreateMetadataTable(p_Engine, query, false);
		if (!p_Engine.GetError().empty())
			return false;

		queryCreateMetadataTable.Run();
		return true;
	}

	bool Annotations::prepareSaveStatement(sqlite3* p_Handle, sqlite3_stmt*& p_SaveStmt)
	{
		//const std::string queryAscii = R"(INSERT OR REPLACE INTO Annotations(SAPIO365ROWID, Status, Date, Updated_By_ID, Udpated_By, Udpated_By_Email, Tenant, Grid_Name, Reference, Grid_Row_PK, Column_ID, Column_Title, Text, Color, Flags) VALUES(@RID, @STA, @DAT, @UID, @UBY, @UEM, @TEN, @GDN, @REF, @RPK, @CID, @CTI, @TXT, @COL, @FLG))";
		const std::string queryAscii = R"(INSERT OR REPLACE INTO Annotations(SAPIO365ROWID, Status, Date, Updated_By_ID, Udpated_By, Udpated_By_Email, Tenant, Reference, Grid_Row_PK, Column_ID, Text, Color, Flags, Volatile, _data_sql_id_) VALUES(@RID, @STA, @DAT, @UID, @UBY, @UEM, @TEN, @REF, @RPK, @CID, @TXT, @CLR, @FLG, @VOL, @SQI))";
		const auto res = sqlite3_prepare_v2(p_Handle, queryAscii.c_str(), static_cast<int>(queryAscii.size()), &p_SaveStmt, nullptr);
		ASSERT(res == SQLITE_OK);
		return res == SQLITE_OK;
	}

	bool Annotations::prepareLoadStatement(sqlite3* p_Handle, sqlite3_stmt*& p_LoadStmt)
	{
		//const std::string queryAscii = R"(SELECT SAPIO365ROWID, Status, Date, Updated_By_ID, Udpated_By, Udpated_By_Email, Tenant, Grid_Name, Reference, Grid_Row_PK, Column_ID, Column_Title, Text, Color, Flags FROM Annotations)";
		const std::string queryAscii = R"(SELECT SAPIO365ROWID, Status, Date, Updated_By_ID, Udpated_By, Udpated_By_Email, Tenant, Reference, Grid_Row_PK, Column_ID, Text, Color, Flags, Volatile, _data_sql_id_ FROM Annotations WHERE Annotations._data_sql_id_=@SQI)";
		const auto res = sqlite3_prepare_v2(p_Handle, queryAscii.c_str(), static_cast<int>(queryAscii.size()), &p_LoadStmt, nullptr);
		ASSERT(res == SQLITE_OK);
		return res == SQLITE_OK;
	}

	bool Annotations::add(CacheGrid& p_Grid, GridBackendRow* p_Row, GridAnnotation& p_GridAnnotation, bool p_PhotoMode, RandomNumber<int64_t>& p_RandomNumber)
	{
		// p_PhotoMode + volatile : regular col
		// p_PhotoMode + permanent : regular col
		// !p_PhotoMode + volatile : real annot
		// !p_PhotoMode + permanent : read only new annotation kind

		if (p_PhotoMode)
		{
			ASSERT(!p_GridAnnotation.IsToEvaluate());
			auto sourceCol = p_Grid.GetColumnByUniqueID(p_GridAnnotation.m_ColumnID);
			const auto columnTitle = p_GridAnnotation.IsVolatile()
				? _YFORMAT(L"Snapshot - Temporary comments for: %s", sourceCol->GetTitleHierarchy().c_str())
				: _YFORMAT(L"Snapshot - Comments for: %s", sourceCol->GetTitleHierarchy().c_str());
			const auto uniqueID = p_GridAnnotation.m_ColumnID + (p_GridAnnotation.IsVolatile() ? AnnotationUtil::g_ColumnIDExtFakeVolSnapshot : AnnotationUtil::g_ColumnIDExtFakeSnapshot);
			auto col = p_Grid.GetColumnByUniqueID(uniqueID);

			if (nullptr == col)
			{
				// Inspired by CacheGrid::annotationColumnAdd

				col = p_Grid.AddColumnVariant(	uniqueID,
												columnTitle,
												sourceCol->GetFamily(),
												HIDPI_XW(8) * 22);

				col->SetPresetFlags(sourceCol->GetColumnPresetFlags());
				//col->SetDynamic(true);
				col->SetIgnoreModifications(true);
				col->SetNumberFormatDouble();
				if (sourceCol->GetTopCategory() != nullptr)
					p_Grid.AddColumnToCategory(sourceCol->GetTopCategory(), col);
				p_Grid.MoveColumnAfter(col, sourceCol);

				col->SetDynamic(true); // So that header color is green too.

				//Save new annotation column in the ytria default setup. 
				{
					// We must compute the annotation column position in the default setup (must be next to p_SourceColumn).
					//Backup Positions : 
					const int backupPos = col->GetPosition();
					const int backupVPos = col->GetPositionVirtual();
					// Get source Column in default Settings Column : 
					const auto& backendColumns = p_Grid.GetColumnOrg()->GetGCVDGrid()->GetGridSetupManager().m_YtriaDefaultSetup.m_Columns;
					//Compute annotation column default Position/ Virtual Position : 
					col->SetPosition(sourceCol->GetPosition() + 1);
					col->SetPositionVirtual(sourceCol->GetPositionVirtual() + 1);

					// Update default Setup :
					p_Grid.GetColumnOrg()->GetGCVDGrid()->UpdateYtriaDefaultColumnSetup({ col });
					//Reset annotation Column : 
					col->SetPosition(backupPos);
					col->SetPositionVirtual(backupVPos);
				}
			}

			ASSERT(nullptr != col);

			FromSql(*p_Row, *col)(p_GridAnnotation.m_Text, nullptr).SetColor(p_GridAnnotation.m_Color);
		}
		else
		{
			if (!p_GridAnnotation.IsVolatile())
			{
				p_GridAnnotation.SetFromSnapshot(true);
				p_GridAnnotation.SetReadOnly(true);
				p_GridAnnotation.m_ID = p_RandomNumber.Next();

				p_Grid.AddAnnotationFromSnapshot(p_GridAnnotation);
				return true;
			}
			else
			{
				p_GridAnnotation.m_ID.reset();
				p_GridAnnotation.SetEdited(true);

				std::map<GridBackendRow*, int64_t> updatedAnnotationIDs;
				auto sourceCol = p_Grid.GetColumnByUniqueID(p_GridAnnotation.m_ColumnID);
				if (p_Grid.AnnotationStore(p_GridAnnotation, p_GridAnnotation.m_UpdDate, sourceCol, { p_Row }, &p_Grid, updatedAnnotationIDs))
				{
					// Successfully added
					return true;
				}
			}
		}

		return false;
	}
}
