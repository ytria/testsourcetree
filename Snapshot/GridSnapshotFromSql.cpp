#include "GridSnapshotFromSql.h"

#include "GridBackendRow.h"
#include "GridSnapshotConstants.h"
#include "ListDeserializer.h"

namespace GridSnapshot
{
	FromSql::FromSql(GridBackendRow& p_Row, GridBackendColumn& p_Col)
		: m_Row(p_Row)
		, m_Col(p_Col)
	{

	}

	GridBackendField& FromSql::operator()(const wstring& sqlValue, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer)
	{
		ASSERT(!m_Col.HasFlag(GridBackendUtil::IGNOREINSNAPSHOT));

		auto typeString = sqlValue.substr(0, 2);

		boost::YOpt<wstring> valueString;

		if (typeString != _YTEXT("00")) //!= GridBackendUtil::UNKNOWN
			valueString = sqlValue.substr(2);

		auto dataType = stoi(typeString);

		const bool isErrorField	= Constants::g_ErrorFieldDataType == dataType
								|| Constants::g_MultiErrorFieldDataType == dataType;

		if (isErrorField // Error field are only generically handled (not call to customizer)
			|| !p_DataCustomizer || (valueString && !p_DataCustomizer->SetValue(*valueString, m_Row, m_Col)))
		{
			ASSERT(valueString);

			// FIXME: Add a fake type for annotation? That would allow to store a serialized version of an annotation with all its params.
			ASSERT(isErrorField || GridBackendUtil::UNKNOWN <= dataType && dataType <= GridBackendUtil::RICHTEXT);
			switch (dataType)
			{
			case GridBackendUtil::UNKNOWN:
				ASSERT(sqlValue == typeString); // No field
				break;
			case GridBackendUtil::STRING:
				return m_Row.AddField(valueString, &m_Col);
				break;
			case GridBackendUtil::STRING_LIST:
				return m_Row.AddField(DeserializeList<PooledString>(*valueString, [](const auto& str) {return str; }), &m_Col);
				break;
			case GridBackendUtil::DOUBLE:
				return m_Row.AddField(stod(*valueString), &m_Col);
				break;
			case GridBackendUtil::LONG:
				return m_Row.AddField(/*stol*/stoll(*valueString), &m_Col); // FIXME: Can be 64-bits. Will be clarified when grid correctly handles 64bits integers.
				break;
			case GridBackendUtil::ULONG:
				return m_Row.AddField(/*stoul*/stoull(*valueString), &m_Col); // FIXME: Can be 64-bits. Will be clarified when grid correctly handles 64bits integers.
				break;
			case GridBackendUtil::NUMBER_LIST_DOUBLE:
				return m_Row.AddField(DeserializeList<double>(*valueString, [](const auto& str) {return stod(str); }), &m_Col);
				break;
			case GridBackendUtil::NUMBER_LIST_LONG:
				return m_Row.AddField(DeserializeList<int64_t>(*valueString, [](const auto& str) {return /*stol*/stoll(str); }), &m_Col); // FIXME: Can be 64-bits. Will be clarified when grid correctly handles 64bits integers.
				break;
			case GridBackendUtil::DATE:
			{
				YTimeDate dt;
				auto mode = TimeUtil::GetInstance().GetTimedateFromISO8601String(*valueString, dt);
				ASSERT(TimeUtil::ISO8601_EMPTY != mode && TimeUtil::ISO8601_ERROR != mode);
				return m_Row.AddField(dt, &m_Col);
				break;
			}
			case GridBackendUtil::DATE_LIST:
			{
				YTimeDate dt;
				return m_Row.AddField(DeserializeList<YTimeDate>(*valueString, [&dt](const auto& str)
					{
						if (GridBackendUtil::g_NoValueString == str)
							dt = GridBackendUtil::g_NoValueDate;
						else
						{
							auto mode = TimeUtil::GetInstance().GetTimedateFromISO8601String(str, dt);
							ASSERT(TimeUtil::ISO8601_EMPTY != mode && TimeUtil::ISO8601_ERROR != mode);
						}
						return dt;
					}), &m_Col);
				break;
			}
			case GridBackendUtil::BOOL:
				// ?? //m_Row.AddField(Str::getBoolFromString(valueString), m_Col);
				return m_Row.AddFieldForCheckBox(Str::getBoolFromString(*valueString), &m_Col);
				break;
			case GridBackendUtil::VARIANT:
				ASSERT(false);
				break;
			case GridBackendUtil::HYPERLINK:
				return m_Row.AddFieldForHyperlink(PooledString(*valueString), &m_Col);
				break;
			case GridBackendUtil::HYPERLINK_LIST:
				return m_Row.AddField(DeserializeList<HyperlinkItem>(*valueString, [](const auto& str)
					{
						return HyperlinkItem(str);
					}), &m_Col);
				break;
			case GridBackendUtil::BOOL_LIST:
				return m_Row.AddField(DeserializeList<BOOLItem>(*valueString, [](const auto& str)
					{
						if (Constants::g_True == str)
							return BOOLItem(TRUE);
						if (Constants::g_False == str)
							return BOOLItem(FALSE);
						return BOOLItem();
					}), &m_Col);
				break;
			case GridBackendUtil::RICHTEXT:
				return m_Row.AddFieldForRichText(PooledString(*valueString), &m_Col);
				break;
			case Constants::g_ErrorFieldDataType:
				return m_Row.AddField(valueString, &m_Col, GridBackendUtil::ICON_ERROR);
				break;
			case Constants::g_MultiErrorFieldDataType:
				return m_Row.AddField(DeserializeList<PooledString>(*valueString, [](const auto& str) {return str; }), &m_Col, GridBackendUtil::ICON_ERROR);
				break;
			default:
				ASSERT(false);
				break;
			}
		}

		return m_Row.GetField(&m_Col);
	}

	template <typename DataType>
	vector<DataType>
	FromSql::DeserializeList(const wstring& p_Value, std::function<DataType(const wstring&)> p_SingleValueDeserializer)
	{
		vector<DataType> result;

		ASSERT(p_SingleValueDeserializer);

		std::error_code err;
		auto json = web::json::value::parse(p_Value, err);
		if (json != web::json::value::null())
		{
			ListStringDeserializer lsd;
			lsd.Deserialize(json);
			result.reserve(lsd.GetData().size());
			for (const auto& str : lsd.GetData())
				result.push_back(p_SingleValueDeserializer(str));
		}

		return result;
	}
}
