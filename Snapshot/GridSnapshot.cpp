#include "GridSnapshot.h"

#include "CacheGrid.h"
#include "DlgProgress.h"
#include "GridSnapshotConstants.h"
#include "GridSnapshotCreator.h"
#include "GridSnapshotLoader.h"
#include "Product.h"

namespace GridSnapshot
{
	class Engine : public SQLiteEngine
	{
	public:
		Engine(const wstring& p_FilePath, const std::string& p_EncryptionKey)
			: SQLiteEngine(p_FilePath, p_EncryptionKey, "")
		{}

		vector<SQLiteUtil::Table> GetTables() const override
		{
			// Not used, but base class is pure virtual...
			return { };
		}

		uint32_t GetVersion() const override
		{
			return Constants::g_Version;
		}
	};

	bool Create(CacheGrid& p_Grid
		, const wstring& p_FilePath
		, const std::string& p_Password
		, CustomMetadata&& p_CustomMetadata
		, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer
		, const Options& p_Options
		, std::function<bool(GridBackendRow*)> p_CustomRowFilter/* = nullptr*/)
	{
		auto engine = std::make_unique<Engine>(p_FilePath, Constants::g_EncryptionKeyBase + p_Password);
		const auto& error = engine->GetError();
		if (error.empty())
		{
			bool success = false;
			const wstring title = p_Options.m_Mode == Options::Mode::Photo ? _T("Creating Snapshot...") : _T("Creating Restore Point...");
			DlgProgress::RunModal(title, _T("Preparing..."), 1, &p_Grid, [&success, &engine, &p_Grid, &p_Options, &p_CustomMetadata, &p_DataCustomizer, &p_CustomRowFilter](std::shared_ptr<DlgProgress> p_Prog)
				{
					Creator creator(std::move(engine), p_Grid, p_Options, p_CustomRowFilter);
					// We can't do this in worker thread, because of grid interactions (i.e. implosion), hence the DoSend
					YCallbackMessage::DoSend(p_Prog.get(), [&success, &creator, &p_Grid, &p_CustomMetadata, &p_DataCustomizer, p_Prog]()
						{
							success = creator.CreateFromGrid(Product::getInstance().getApplication(), std::move(p_CustomMetadata), p_DataCustomizer, p_Prog);
						});
				});
			return success;
		}
		return false;
	}

	std::shared_ptr<Loader> GetLoader(const wstring& p_FilePath, const std::string& p_Password)
	{
		std::shared_ptr<Loader> loader;
		auto engine = std::make_unique<Engine>(p_FilePath, Constants::g_EncryptionKeyBase + p_Password);
		const auto& error = engine->GetError();
		if (error.empty())
			loader = std::make_shared<Loader>(std::move(engine));
		return loader;
	}
}
