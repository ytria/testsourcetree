#pragma once

#include "GridAnnotation.h"
#include "RandomNumber.h"
#include "SQLiteEngine.h"

namespace GridSnapshot
{
	class Annotations
	{
	public:
		Annotations(SQLiteEngine& p_Engine, bool p_ForLoad);
		~Annotations();
		bool Save(int64_t p_RowId, CacheGrid& p_Grid, GridBackendRow* p_Row, const GridAnnotation& p_GridAnnotation, bool p_PhotoMode);
		bool Load(int64_t p_RowId, CacheGrid& p_Grid, GridBackendRow* p_Row, bool p_PhotoMode);

	private:
		bool init();
		static bool createTable(SQLiteEngine& p_Engine);
		static bool prepareSaveStatement(sqlite3* p_Handle, sqlite3_stmt*& p_SaveStmt);
		static bool prepareLoadStatement(sqlite3* p_Handle, sqlite3_stmt*& p_LoadStmt);
		static bool add(CacheGrid& p_Grid, GridBackendRow* p_Row, GridAnnotation& p_GridAnnotation, bool p_PhotoMode, RandomNumber<int64_t>& p_RandomNumber);

	private:
		SQLiteEngine& m_Engine;
		sqlite3_stmt* m_SaveStmt;
		sqlite3_stmt* m_LoadStmt;
		bool m_ForLoad;
		boost::YOpt<RandomNumber<int64_t>> m_RandomNumber;
	};
}

