#include "GridSnapshotLoader.h"

#include "BaseO365Grid.h"
#include "CacheGrid.h"
#include "GridSnapshotAnnotations.h"
#include "GridSnapshotConstants.h"
#include "GridSnapshotFromSql.h"
#include "GridSnapshotRowHandler.h"
#include "TimeUtil.h"

namespace GridSnapshot
{
	Loader::Loader(std::unique_ptr<SQLiteEngine>&& p_Engine)
		: m_Engine(std::move(p_Engine))
	{
		init();
	}

	Loader::~Loader()
	{
		sqlite3_finalize(m_SqlStatementGetMetadata);
		sqlite3_finalize(m_SqlStatementGetRowData);
	}

	const Metadata& Loader::GetMetadata()
	{
		if (!m_Metadata)
		{
			m_Metadata.emplace();
			loadMetadata(*m_Metadata);
		}

		return *m_Metadata;
	}

	bool Loader::Load(CacheGrid& p_Grid, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer)
	{
		ASSERT(m_Metadata);
		ASSERT(m_NumRows);

		if (m_NumRows && *m_NumRows > 0)
		{
			// FIXME: For now, bypass "Progress Trigger" and force progress display.
			// Consider removing this.
			p_Grid.CreateProgressBar(*m_NumRows * 2
				, /*1*/ceil(p_Grid.GetGridOptions().m_ProgressTrigger / static_cast<double>(*m_NumRows * 2))
				, Options::Mode::Photo == m_Metadata->m_Mode
					? _T("Loading snapshot (rows)...")
					: _T("Loading restore point (rows)..."));
		}

		std::map<int, GridBackendRow*> sqlIdToRows;
		int lastRowId = 0;
		GridBackendRow* currentRow = nullptr;
		bool hasChildren = false;
		auto unknownPropHandler = [&currentRow, &sqlIdToRows, &hasChildren, &p_Grid, &lastRowId](const std::string& p_PropName, const boost::YOpt<wstring>& p_Value)
		{
			if (p_PropName == "_parent_id_")
			{
				if (p_Value)
				{
					auto parentSqlId = Str::getIntFromString(*p_Value);
					auto it = sqlIdToRows.find(parentSqlId);
					ASSERT(sqlIdToRows.end() != it);
					if (sqlIdToRows.end() != it)
						currentRow->SetParentRow(it->second);
				}
			}
			else if (p_PropName == "_has_children_")
			{
				hasChildren = p_Value.is_initialized();
			}
			else if (p_PropName == "_sql_id_")
			{
				ASSERT(p_Value);
				if (p_Value)
				{
					lastRowId = Str::getIntFromString(*p_Value);
					if (hasChildren)
						sqlIdToRows[lastRowId] = currentRow;
				}
			}
			else
			{
				ASSERT(false);
			}
		};

		Annotations snapshotAnnotations(*m_Engine, true);

		int res = 0;
		std::vector<std::pair<int, GridBackendRow*>> rows;
		while ((res = m_Engine->sqlite3_step(m_SqlStatementGetRowData)) == SQLITE_ROW)
		{
			currentRow = p_Grid.AddRow();
			ASSERT(nullptr != currentRow);
			if (nullptr != currentRow)
			{
				RowHandler::Parse(*currentRow, p_Grid, m_SqlStatementGetRowData, unknownPropHandler, p_DataCustomizer);
				rows.emplace_back(lastRowId, currentRow);

				if (nullptr != p_Grid.GetBackend().GetStatusColumn()
					&& GridBackendUtil::ICON_ERROR == currentRow->GetField(p_Grid.GetBackend().GetStatusColumn()).GetIcon())
				{
					currentRow->SetEditError(true);
					// FIXME: We should stop using generic CacheGrid type...
					auto grid = dynamic_cast<O365Grid*>(&p_Grid);
					ASSERT(nullptr != grid);
					if (nullptr != grid)
						grid->HandlePostUpdateError(false, false, Str::g_EmptyString);
				}
			}

			p_Grid.StepItProgressBar();
		}

		p_Grid.SetTextProgressBar(Options::Mode::Photo == m_Metadata->m_Mode
			? _T("Loading snapshot (comments)...")
			: _T("Loading restore point (comments)..."));

		for (auto& r : rows)
		{
			snapshotAnnotations.Load(r.first, p_Grid, r.second, Options::Mode::Photo == m_Metadata->m_Mode);
			p_Grid.StepItProgressBar();
		}

		p_Grid.DestroyProgressBar();

		m_Engine->ProcessError(res, nullptr, _YTEXT(""));

		sqlite3_reset(m_SqlStatementGetRowData);
		return m_Engine->GetError().empty();
	}

	const wstring& Loader::GetFilePath() const
	{
		return m_Engine->GetDBfilename();
	}

	void Loader::init()
	{
		// Prepare statements
		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXTFORMAT(L"SELECT version, product, sessionRef, mode, access, autoName, criteria, xmlConfig, processesSetup, creationDate, creatorName, creatorTechName, flags, handleSkuCount, moduleOptions FROM %s", Constants::g_TableMetadata.c_str()));
			const auto res = sqlite3_prepare_v2(m_Engine->GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementGetMetadata, nullptr);
			ASSERT(res == SQLITE_OK);
		}
		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXTFORMAT(L"SELECT * FROM %s ORDER BY '_parent_id_'", Constants::g_TableData.c_str()));
			const auto res = sqlite3_prepare_v2(m_Engine->GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementGetRowData, nullptr);
			ASSERT(res == SQLITE_OK);
		}

		{
			// As _sql_id_ is auto-incremented and we never delete any row, getting the max _sql_id_ is getting the number of rows.
			const auto maxQueryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXTFORMAT(L"SELECT MAX(_sql_id_) FROM \"%s\"", Constants::g_TableData.c_str()));
			sqlite3_stmt* m_Stmt;
			const auto res = sqlite3_prepare_v2(m_Engine->GetDbHandle(), maxQueryAscii.c_str(), static_cast<int>(maxQueryAscii.size()), &m_Stmt, nullptr);
			ASSERT(res == SQLITE_OK);
			if (res == SQLITE_OK)
			{
				auto status = m_Engine->sqlite3_step(m_Stmt);
				ASSERT(SQLITE_ROW == status);
				if (SQLITE_ROW == status)
					m_NumRows = sqlite3_column_int(m_Stmt, 0);
			}
			sqlite3_finalize(m_Stmt);
		}
	}

	void Loader::loadMetadata(Metadata& p_Metadata)
	{
		// FIXME: Should better just "select all" to be able to handle column addition/removal easily.
		auto status = m_Engine->sqlite3_step(m_SqlStatementGetMetadata);
		if (SQLITE_ROW == status)
		{
			int index = 0;

			auto versionString = Str::convertFromUTF8(reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, index++)));
			if (!versionString.empty())
				p_Metadata.m_Version = MFCUtil::getDWORDFromString(versionString);

			auto productString = reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, index++));
			if (nullptr != productString)
				p_Metadata.m_Product = Str::convertFromUTF8(productString);

			auto sessionRef = reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, index++));
			if (nullptr != sessionRef)
				p_Metadata.m_SessionRef = Str::convertFromUTF8(sessionRef);

			{
				const auto val = sqlite3_column_int(m_SqlStatementGetMetadata, index++);
				ASSERT(val == (int)Options::Mode::Photo || val == (int)Options::Mode::RestorePoint);
				if (val == (int)Options::Mode::Photo)
					p_Metadata.m_Mode = Options::Mode::Photo;
				else
					p_Metadata.m_Mode = Options::Mode::RestorePoint;
			}

			{
				const auto val = sqlite3_column_int(m_SqlStatementGetMetadata, index++);
				ASSERT(val == (int)Options::Access::Public || val == (int)Options::Access::Tenant);
				if (val == (int)Options::Access::Public)
					p_Metadata.m_Access = Options::Access::Public;
				else
					p_Metadata.m_Access = Options::Access::Tenant;
			}

			auto autoName = reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, index++));
			if (nullptr != autoName)
				p_Metadata.m_GridAutomationName = Str::convertFromUTF8(autoName);

			auto criteria = reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, index++));
			if (nullptr != criteria)
				p_Metadata.m_Criteria = Str::convertFromUTF8(criteria);

			auto xmlConfig = reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, index++));
			if (nullptr != xmlConfig)
				p_Metadata.m_XmlConfig = xmlConfig;

			auto processesSetup = reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, index++));
			if (nullptr != processesSetup)
				p_Metadata.m_ProcessesSetup = Str::convertFromUTF8(processesSetup);

			auto date = reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, index++));
			if (nullptr != date)
			{
				p_Metadata.m_CreationDate.emplace();
				TimeUtil::GetInstance().GetTimedateFromISO8601String(Str::convertFromUTF8(date), *p_Metadata.m_CreationDate);
			}

			auto name = reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, index++));
			if (nullptr != name)
				p_Metadata.m_CreatorName = Str::convertFromUTF8(name);

			auto techName = reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, index++));
			if (nullptr != techName)
				p_Metadata.m_CreatorTechnicalName = Str::convertFromUTF8(techName);

			p_Metadata.m_Flags = sqlite3_column_int(m_SqlStatementGetMetadata, index++);
			p_Metadata.m_IsHandleSkuCounters = sqlite3_column_int(m_SqlStatementGetMetadata, index++) == 1;

			auto moduleOptions = reinterpret_cast<const char*>(sqlite3_column_text(m_SqlStatementGetMetadata, index++));
			if (nullptr != moduleOptions)
				p_Metadata.m_ModuleOptions = Str::convertFromUTF8(moduleOptions);
		}

		m_Engine->ProcessError(status, nullptr, _YTEXT(""));

		ASSERT(SQLITE_DONE == m_Engine->sqlite3_step(m_SqlStatementGetMetadata));

		sqlite3_reset(m_SqlStatementGetMetadata);
	}
}
