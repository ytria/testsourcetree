#pragma once

#include "GridSnapshotUtil.h"
#include "SQLiteEngine.h"

namespace GridSnapshot
{
	class Loader
	{
	public:
		Loader(std::unique_ptr<SQLiteEngine>&& p_Engine);
		virtual ~Loader();

		const Metadata& GetMetadata();
		bool Load(CacheGrid& p_Grid, const std::unique_ptr<IDataCustomizer>& p_DataCustomizer);

		const wstring& GetFilePath() const;

	private:
		void init();
		void loadMetadata(Metadata& p_Metadata);

	private:
		std::unique_ptr<SQLiteEngine> m_Engine;
		sqlite3_stmt* m_SqlStatementGetMetadata;
		sqlite3_stmt* m_SqlStatementGetRowData;

		boost::YOpt<Metadata> m_Metadata;
		boost::YOpt<int> m_NumRows;
	};
}