#include "GroupSettingTemplatesRequester.h"

#include "BusinessGroupSettingTemplate.h"
#include "GroupSettingTemplateDeserializer.h"
#include "MSGraphSession.h"
#include "Sapio365Session.h"
#include "ValueListDeserializer.h"
#include "MsGraphHttpRequestLogger.h"

GroupSettingTemplatesRequester::GroupSettingTemplatesRequester(const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_Logger(p_Logger)
{
}

TaskWrapper<void> GroupSettingTemplatesRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<BusinessGroupSettingTemplate, GroupSettingTemplateDeserializer>>();

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
    return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObjects(m_Deserializer, { _YTEXT("groupSettingTemplates") }, {}, false, m_Logger, httpLogger, p_TaskData);
}

const vector<BusinessGroupSettingTemplate>& GroupSettingTemplatesRequester::GetData() const
{
    return m_Deserializer->GetData();
}
