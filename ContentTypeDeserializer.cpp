#include "ContentTypeDeserializer.h"

#include "ContentTypeOrderDeserializer.h"
#include "ItemReferenceDeserializer.h"
#include "JsonSerializeUtil.h"

void ContentTypeDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.Description, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("group"), m_Data.Group, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("hidden"), m_Data.Hidden, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.Id, p_Object, true);

    {
        ItemReferenceDeserializer ird;
        if (JsonSerializeUtil::DeserializeAny(ird, _YTEXT("inheritedFrom"), p_Object))
            m_Data.InheritedFrom = std::move(ird.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.Name, p_Object);

    {
        ContentTypeOrderDeserializer ctod;
        if (JsonSerializeUtil::DeserializeAny(ctod, _YTEXT("order"), p_Object))
            m_Data.Order = std::move(ctod.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("parentId"), m_Data.ParentId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("readOnly"), m_Data.ReadOnly, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("sealed"), m_Data.Sealed, p_Object);
}
