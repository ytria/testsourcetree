#pragma once

#include "BaseObj.h"

namespace Cosmos
{
    class Document : public Cosmos::BaseObj
    {
    public:
        boost::YOpt<PooledString> _attachments;
        web::json::value Content;
    };
}
