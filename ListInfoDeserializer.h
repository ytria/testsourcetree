#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ListInfo.h"

class ListInfoDeserializer : public JsonObjectDeserializer, public Encapsulate<ListInfo>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

