#include "O365GridSnapshot.h"

#include "CommandDispatcher.h"
#include "DlgCreateLoadSessionHTML.h"
#include "DlgLicenseMessageBoxHTML.h"
#include "DlgPasswordInput.h"
#include "GridSnapshot.h"
#include "GridSnapshotConstants.h"
#include "GridUtil.h"
#include "MainFrame.h"
#include "ModuleCriteria.h"
#include "Product.h"
#include "TryLoadSnapshotCommand.h"
#include "YFileDialog.h"

const wstring O365GridSnapshot::g_FileExtensionPhoto = _YTEXT("ytr_snap");
const wstring O365GridSnapshot::g_FileExtensionRestore = _YTEXT("ytr_rest");

void O365GridSnapshot::Load(wstring p_FilePath/* = _YTEXT("")*/, GridFrameBase* p_ParentFrame/* = nullptr*/)
{
	Load(p_FilePath, boost::none, p_ParentFrame);
}

void O365GridSnapshot::Load(boost::YOpt<GridSnapshot::Options::Mode> p_Mode, GridFrameBase* p_ParentFrame /*= nullptr*/)
{
	Load(_YTEXT(""), p_Mode, p_ParentFrame);
}

void O365GridSnapshot::Load(wstring p_FilePath, boost::YOpt<GridSnapshot::Options::Mode> p_Mode, GridFrameBase* p_ParentFrame /*= nullptr*/)
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);

	auto parentForDlg = nullptr != p_ParentFrame ? (CWnd*)p_ParentFrame : (CWnd*)mainFrame;

	{
		if (p_FilePath.empty())
		{
			const wstring filter = !p_Mode
				? _YTEXTFORMAT(L"sapio365 Files (*.%s;*%s)|*.%s;*%s|sapio365 Snapshots (*.%s)|*.%s|sapio365 Restore Points (*.%s)|*.%s||", g_FileExtensionPhoto.c_str(), g_FileExtensionRestore.c_str(), g_FileExtensionPhoto.c_str(), g_FileExtensionRestore.c_str(), g_FileExtensionPhoto.c_str(), g_FileExtensionPhoto.c_str(), g_FileExtensionRestore.c_str(), g_FileExtensionRestore.c_str())
				: GridSnapshot::Options::Mode::RestorePoint == *p_Mode
					? _YTEXTFORMAT(L"sapio365 Restore Points (*.%s)|*.%s||", g_FileExtensionRestore.c_str(), g_FileExtensionRestore.c_str())
					: GridSnapshot::Options::Mode::Photo == *p_Mode
						? _YTEXTFORMAT(L"sapio365 Snapshots (*.%s)|*.%s||", g_FileExtensionPhoto.c_str(), g_FileExtensionPhoto.c_str())
						: _YTEXTFORMAT(L"sapio365 Files (*.%s;*%s)|*.%s;*%s|sapio365 Snapshots (*.%s)|*.%s|sapio365 Restore Points (*.%s)|*.%s||", g_FileExtensionPhoto.c_str(), g_FileExtensionRestore.c_str(), g_FileExtensionPhoto.c_str(), g_FileExtensionRestore.c_str(), g_FileExtensionPhoto.c_str(), g_FileExtensionPhoto.c_str(), g_FileExtensionRestore.c_str(), g_FileExtensionRestore.c_str());

			YFileDialog dlgFile(TRUE, nullptr, nullptr, 0, filter.c_str(), parentForDlg);
			if (IDOK == dlgFile.DoModal())
				p_FilePath = dlgFile.GetPathName();
			else
				return;
		}

		boost::YOpt<std::string> password = "";
		auto loader = GridSnapshot::GetLoader(p_FilePath, *password); // First try without password.

		bool isRetry = false;
		while (password && !loader)
		{
			DlgPasswordInput dlgPwd(parentForDlg, false, isRetry ? _T("Incorrect password. Please try again.") : _YTEXT(""));
			if (IDOK == dlgPwd.DoModal())
			{
				password = dlgPwd.GetPassword();
				loader = GridSnapshot::GetLoader(p_FilePath, *password);
			}
			else
				password.reset(); // cancel
			isRetry = true;
		}

		if (loader)
		{
			const auto& metadata = loader->GetMetadata();

			if (GridSnapshot::Options::Mode::Photo == *metadata.m_Mode || Office365AdminApp::CheckAndWarn<LicenseTag::LoadRestorePoint>(parentForDlg))
			{
				if (!metadata.m_Version || *metadata.m_Version > GridSnapshot::Constants::g_Version)
				{
					LoggerService::Debug(
						!metadata.m_Mode || GridSnapshot::Options::Mode::Photo == *metadata.m_Mode
						? _YDUMPFORMAT(L"Snapshot %s too recent (v%s vs v%s)", p_FilePath.c_str(), std::to_wstring(metadata.m_Version ? *metadata.m_Version : 0).c_str(), std::to_wstring(GridSnapshot::Constants::g_Version).c_str())
						: _YDUMPFORMAT(L"Restore point %s too recent (v%s vs v%s)", p_FilePath.c_str(), std::to_wstring(metadata.m_Version ? *metadata.m_Version : 0).c_str(), std::to_wstring(GridSnapshot::Constants::g_Version).c_str())
						, parentForDlg->m_hWnd);

					YCodeJockMessageBox(parentForDlg
						, DlgMessageBox::eIcon_Error
						, YtriaTranslate::Do(MainFrame_OnLoadSession_1, _YLOC("ERROR")).c_str()
						, !metadata.m_Mode || GridSnapshot::Options::Mode::Photo == *metadata.m_Mode
						? _YFORMAT(L"This snapshot was created with a newer version of %s.\nPlease update and try again.", Product::getInstance().getApplication().c_str())
						: _YFORMAT(L"This restore point was created with a newer version of %s.\nPlease update and try again.", Product::getInstance().getApplication().c_str())
						, _YTEXT("")
						, { { IDOK, YtriaTranslate::Do(MainFrame_OnLoadSession_3, _YLOC("OK")).c_str() } }).DoModal();
				}
				else
				{
					// If mode is Restore Point, access must be Tenant.
					ASSERT((!metadata.m_Mode || GridSnapshot::Options::Mode::Photo == *metadata.m_Mode)
						|| (!metadata.m_Access || GridSnapshot::Options::Access::Tenant == *metadata.m_Access));

					boost::YOpt<SessionIdentifier> sessionId;
					if (metadata.m_Product != Product::getInstance().getApplication())
					{
						YCodeJockMessageBox(parentForDlg
							, DlgMessageBox::eIcon_Error
							, YtriaTranslate::Do(MainFrame_OnLoadSession_1, _YLOC("ERROR")).c_str()
							, !metadata.m_Mode || GridSnapshot::Options::Mode::Photo == *metadata.m_Mode
							? _YFORMAT(L"This snapshot must be opened with %s.", metadata.m_Product ? metadata.m_Product->c_str() : Str::g_EmptyString.c_str())
							: _YFORMAT(L"This restore point must be opened with %s.", metadata.m_Product ? metadata.m_Product->c_str() : Str::g_EmptyString.c_str())
							, _YTEXT("")
							, { { IDOK, YtriaTranslate::Do(MainFrame_OnLoadSession_3, _YLOC("OK")).c_str() } }).DoModal();
					}
					else
					{
						CommandInfo info;
						auto cmd = std::make_shared<TryLoadSnapshotCommand>();
						cmd->SetSnapshotLoader(loader);
						info.Data() = cmd;
						Command command(mainFrame->GetLicenseContext(), parentForDlg->m_hWnd, Command::ModuleTarget::None, Command::ModuleTask::TryLoadSnapshot, info, { nullptr, _YTEXT(""), nullptr, nullptr });
						command.SetSessionIdentifier(nullptr != p_ParentFrame ? p_ParentFrame->GetSessionInfo().GetIdentifier() : mainFrame->GetSessionInfo().GetIdentifier());
						CommandDispatcher::GetInstance().Execute(command);
					}
				}
			}
		}
	}
}
