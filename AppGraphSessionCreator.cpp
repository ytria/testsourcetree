#include "AppGraphSessionCreator.h"
#include "RESTUtils.h"
#include "NetworkUtils.h"
#include "MSGraphCommonData.h"
#include "OAuth2Authenticators.h"
#include "O365AdminErrorHandler.h"
#include "MSGraphSession.h"

AppGraphSessionCreator::AppGraphSessionCreator(const wstring& p_Tenant, const wstring& p_ClientId, const wstring& p_ClientSecret, const wstring& p_AppRefName)
	:m_Tenant(p_Tenant),
	m_ClientId(p_ClientId),
	m_ClientSecret(p_ClientSecret),
	m_ApplicationRefName(p_AppRefName)
{
	if (m_ApplicationRefName.empty())
		m_ApplicationRefName = m_ClientId;

	ASSERT(!(m_ClientId.empty() || m_Tenant.empty()));
	if (m_ClientId.empty() || m_Tenant.empty())
		throw std::invalid_argument("Client id and tenant must be filled.");
}

std::shared_ptr<MSGraphSession> AppGraphSessionCreator::Create()
{
	web::http::uri_builder tokenUri(Rest::GetAzureADEndpoint());
	tokenUri.append_path(GetTenant());
	tokenUri.append_path(_YTEXT("oauth2/v2.0/token"));

	auto session = std::make_shared<MSGraphSession>(NetworkUtils::GetHttpClientConfig(), nullptr);
	auto credAuth = std::make_unique<ClientCredentialsAuthenticator>(session, m_ClientId, m_ClientSecret, tokenUri.to_string(),	Rest::GetMsGraphEndpoint() + _YTEXT("/.default"));
	session->SetErrorHandler(std::make_shared<O365AdminErrorHandler>(session));
	session->SetAuthenticator(std::move(credAuth));
	
	auto result = session->Start(nullptr).get();
	m_Result.m_Success = result.state == RestAuthenticationResult::State::Success;
	if (m_Result.m_Success)
	{
		LoggerService::Debug(_YFORMAT(L"Successfully created application session for client id %s in tenant %s", m_ClientId.c_str(), m_Tenant.c_str()));
	}
	else
	{
		m_Result.m_GraphError = result.error_message;
		LoggerService::Debug(_YFORMAT(L"Unable to create application session for client id %s in tenant %s", m_ClientId.c_str(), m_Tenant.c_str()));
	}

	return session;
}

const wstring& AppGraphSessionCreator::GetTenant() const
{
	return m_Tenant;
}

const wstring& AppGraphSessionCreator::GetClientId() const
{
	return m_ClientId;
}

const wstring& AppGraphSessionCreator::GetClientSecret() const
{
	return m_ClientSecret;
}

const wstring& AppGraphSessionCreator::GetApplicationName() const
{
	return m_ApplicationRefName;
}
