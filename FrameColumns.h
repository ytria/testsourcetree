#pragma once

#include "GridFrameBase.h"

#include "GridColumns.h"

class FrameColumns : public GridFrameBase
{
public:
	FrameColumns(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
    virtual ~FrameColumns() = default;
    DECLARE_DYNAMIC(FrameColumns)

    virtual O365Grid& GetGrid() override;
    virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;
    virtual void createGrid() override;

    virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	void ShowColumns(const O365DataMap<BusinessSite, vector<BusinessList>>& p_Lists, bool p_FullPurge);

protected:
	virtual O365ControlConfig GetRefreshPartialControlConfig() override;
    virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
    GridColumns m_Grid;
};

