#pragma once

#include "IDeserializerFactory.h"
#include "DirectoryObjectVariantDeserializer.h"

class DirectoryObjectVariantDeserializerFactory : public IDeserializerFactory<DirectoryObjectVariantDeserializer>
{
public:
	DirectoryObjectVariantDeserializerFactory(std::shared_ptr<Sapio365Session> p_Session);

#ifdef _DEBUG
	// This ctor is for debug purpose only, as Authors Rejected/Accepted hierarchies requests don't accept $select query,
	// we want to prevent having an assert in UserDeserializer/GroupDeserializer nested in DirectoryObjectVariantDeserializer
	// Eventually removed when Graph API is fixed.
	DirectoryObjectVariantDeserializerFactory(std::shared_ptr<Sapio365Session> p_Session, bool p_NoSelectUsed);
#endif

	DirectoryObjectVariantDeserializer Create() override;

private:
	std::shared_ptr<Sapio365Session>	m_Session;
#ifdef _DEBUG
	bool m_NoSelectUsed = false; // Eventually removed when Graph API is fixed.
#endif
};
