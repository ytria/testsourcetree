#include "FramePrivateChannelMembers.h"

#include "GridUpdater.h"

IMPLEMENT_DYNAMIC(FramePrivateChannelMembers, GridFrameBase)

FramePrivateChannelMembers::FramePrivateChannelMembers(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
	m_CreateRefreshPartial = true;
}

void FramePrivateChannelMembers::ShowPrivateChannelMembers(const vector<BusinessChannel>& p_Channels, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge)
{
	m_GridPrivateChannelMembers.BuildView(p_Channels, p_UpdateOperations, p_FullPurge);
}

O365Grid& FramePrivateChannelMembers::GetGrid()
{
	return m_GridPrivateChannelMembers;
}

void FramePrivateChannelMembers::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.SetOrigin(GetModuleCriteria().m_Origin);
	ASSERT(ModuleCriteria::UsedContainer::SUBIDS == GetModuleCriteria().m_UsedContainer);
	if (ModuleCriteria::UsedContainer::SUBIDS == GetModuleCriteria().m_UsedContainer)
		info.GetSubIds() = GetModuleCriteria().m_SubIDs;
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	if (HasLastUpdateOperations())
		GridUpdater::HandleCreatedModifications(GetGrid(), AccessLastUpdateOperations());

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::ChannelMembers, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FramePrivateChannelMembers::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.Data() = p_RefreshData;
	info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

	if (HasLastUpdateOperations())
		GridUpdater::HandleCreatedModifications(GetGrid(), AccessLastUpdateOperations());

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::ChannelMembers, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

const std::map<PooledString, PooledString>& FramePrivateChannelMembers::GetChannelNames() const
{
	return m_ChannelNames;
}

void FramePrivateChannelMembers::SetChannelNames(const map<PooledString, PooledString>& p_ChannelNames)
{
	m_ChannelNames = p_ChannelNames;
}

void FramePrivateChannelMembers::createGrid()
{
	m_GridPrivateChannelMembers.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FramePrivateChannelMembers::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewDataEditGroups(tab);
	CreateApplyGroup(tab);
	CreateRevertGroup(tab);

	{
		auto group = tab.AddGroup(_T("Channel Membership"));
		setGroupImage(*group, 0);
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			{
				auto ctrl = group->Add(xtpControlButton, ID_CHANNELMEMBERSGRID_ADD);
				GridFrameBase::setImage({ ID_CHANNELMEMBERSGRID_ADD }, IDB_CHANNELMEMBERSGRID_ADD, xtpImageNormal);
				GridFrameBase::setImage({ ID_CHANNELMEMBERSGRID_ADD }, IDB_CHANNELMEMBERSGRID_ADD_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			{
				auto ctrl = group->Add(xtpControlButton, ID_CHANNELMEMBERSGRID_REMOVE);
				GridFrameBase::setImage({ ID_CHANNELMEMBERSGRID_REMOVE }, IDB_CHANNELMEMBERSGRID_REMOVE, xtpImageNormal);
				GridFrameBase::setImage({ ID_CHANNELMEMBERSGRID_REMOVE }, IDB_CHANNELMEMBERSGRID_REMOVE_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			{
				auto ctrl = group->Add(xtpControlButton, ID_CHANNELMEMBERSGRID_MAKEOWNER);
				GridFrameBase::setImage({ ID_CHANNELMEMBERSGRID_MAKEOWNER }, IDB_CHANNELMEMBERSGRID_MAKEOWNER, xtpImageNormal);
				GridFrameBase::setImage({ ID_CHANNELMEMBERSGRID_MAKEOWNER }, IDB_CHANNELMEMBERSGRID_MAKEOWNER_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}

			{
				auto ctrl = group->Add(xtpControlButton, ID_CHANNELMEMBERSGRID_MAKEMEMBER);
				GridFrameBase::setImage({ ID_CHANNELMEMBERSGRID_MAKEMEMBER }, IDB_CHANNELMEMBERSGRID_MAKEMEMBER, xtpImageNormal);
				GridFrameBase::setImage({ ID_CHANNELMEMBERSGRID_MAKEMEMBER }, IDB_CHANNELMEMBERSGRID_MAKEMEMBER_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
			}
		}
	}

	CreateLinkGroups(tab, true, false);

	return true;
}

AutomatedApp::AUTOMATIONSTATUS FramePrivateChannelMembers::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	return statusRV;
}

GridFrameBase::O365ControlConfig FramePrivateChannelMembers::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigChannels();
}

GridFrameBase::O365ControlConfig FramePrivateChannelMembers::GetApplyPartialControlConfig()
{
	return GetApplyPartialControlConfigChannels();
}

void FramePrivateChannelMembers::ApplyAllSpecific()
{
	apply(false);
}

void FramePrivateChannelMembers::ApplySelectedSpecific()
{
	apply(true);
}

void FramePrivateChannelMembers::apply(bool p_Selected)
{
	CommandInfo info;

	if (p_Selected)
	{
		std::set<GridBackendRow*> selectedTopLevelRows;
		for (auto& row : m_GridPrivateChannelMembers.GetSelectedRows())
			selectedTopLevelRows.insert(row->GetTopAncestorOrThis());
		info.Data() = m_GridPrivateChannelMembers.GetChanges(selectedTopLevelRows);
	}
	else
		info.Data() = m_GridPrivateChannelMembers.GetChanges({});

	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::ChannelMembers, Command::ModuleTask::ApplyChanges, info, { &GetGrid(), p_Selected ? AutomationConstant::val().m_ActionNameSaveSelected : AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
}
