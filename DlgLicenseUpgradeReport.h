#pragma once

#include "CRMpipe.h"
#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "YAdvancedHtmlView.h"

class DlgLicenseUpgradeReport : public ResizableDialog, public YAdvancedHtmlView::IPostedDataTarget
{
public:
	DlgLicenseUpgradeReport(const uint32_t p_NewCapacity, const CRMpipe::RequestResponse& rr, CWnd* p_Parent);

	enum { IDD = IDD_DLG_LICENSEUPGRADEREPORT};

protected:
	virtual BOOL OnInitDialogSpecificResizable() override;
	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data) override;

	ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);

private:
	CRMpipe::RequestResponse m_Report;
	uint32_t m_NewCapacity;

	static const wstring g_JSONBtnNameOK;

	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;
};