#include "DriveItemPermissionDeletion.h"

#include "ActivityLoggerUtil.h"
#include "DriveItemsUtil.h"
#include "SubElementPrefixFormatter.h"

DriveItemPermissionDeletion::DriveItemPermissionDeletion(GridDriveItems& p_Grid, const GridDriveItems::DriveItemPermissionInfo& p_PermissionInfo)
    :ISubItemDeletion(	p_Grid, SubItemKey(p_PermissionInfo.m_OriginId, p_PermissionInfo.m_DriveId, p_PermissionInfo.m_DriveItemId, p_PermissionInfo.m_PermissionId),
						p_Grid.GetColumnPermissionId(),
						p_Grid.GetColumnPermissionId(),
						p_Grid.GetName(p_PermissionInfo.m_Row)),
    m_GridDriveItems(p_Grid),
    m_PermissionInfo(p_PermissionInfo)
{
}

bool DriveItemPermissionDeletion::ShouldCreateFormatter(GridBackendRow* p_Row, size_t p_Index) const
{
    auto multiVals = p_Row->GetField(m_GridDriveItems.GetColumnRole()).GetValuesMulti<PooledString>();
    ASSERT(nullptr != multiVals && p_Index < multiVals->size());
    if (nullptr != multiVals && p_Index < multiVals->size())
        return multiVals->operator[](p_Index) != PooledString(DriveItemsUtil::ROLE_VALUE_OWNER);

    return false;
}

bool DriveItemPermissionDeletion::IsCorrespondingRow(GridBackendRow* p_Row) const
{
    return DriveItemsUtil::IsCorrespondingRow(m_GridDriveItems, p_Row, m_PermissionInfo);
}

vector<Modification::ModificationLog> DriveItemPermissionDeletion::GetModificationLogs() const
{
	return { ModificationLog(getPk(), GetObjectName(), ActivityLoggerUtil::g_ActionDelete, _YTEXT("Permission"), GetState()) };
}

GridBackendRow* DriveItemPermissionDeletion::GetMainRowIfCollapsed() const
{
	vector<GridBackendRow*> rows;
	m_GridDriveItems.GetRowsByCriteria(rows, [this](GridBackendRow* p_Row)
		{
			const auto originId = p_Row->GetField(m_GridDriveItems.GetColMetaID()).GetValueStr();
			const auto driveId = p_Row->GetField(m_GridDriveItems.GetColMetaDriveID()).GetValueStr();
			const auto driveItemId = p_Row->GetField(m_GridDriveItems.GetColId()).GetValueStr();
			return nullptr != originId && m_PermissionInfo.m_OriginId == originId
				&& nullptr != driveId && m_PermissionInfo.m_DriveId == driveId
				&& nullptr != driveItemId && m_PermissionInfo.m_DriveItemId == driveItemId;
		});

	if (rows.size() == 1 && IsCollapsedRow(rows.front()))
		return rows.front();

	return nullptr;
}
