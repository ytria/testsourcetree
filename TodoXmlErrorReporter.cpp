#include "TodoXmlErrorReporter.h"


TodoXmlErrorReporter::TodoXmlErrorReporter()
    :m_HasError(false)
{
}

void TodoXmlErrorReporter::warning(const XERCES_CPP_NAMESPACE::SAXParseException& p_Exception)
{
    ASSERT(false);
}

void TodoXmlErrorReporter::error(const XERCES_CPP_NAMESPACE::SAXParseException& p_Exception)
{
    m_HasError = true;
    ASSERT(false);
}

void TodoXmlErrorReporter::fatalError(const XERCES_CPP_NAMESPACE::SAXParseException& p_Exception)
{
    m_HasError = true;
    ASSERT(false);
}

void TodoXmlErrorReporter::resetErrors()
{
    m_HasError = false;
}

bool TodoXmlErrorReporter::HasError() const
{
    return m_HasError;
}
