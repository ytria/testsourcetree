#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerJobCenter : public IBrowserLinkHandler
{
public:
	virtual void Handle(YBrowserLink& p_Link) const override;
};