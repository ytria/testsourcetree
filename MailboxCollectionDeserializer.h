#pragma once

#include "Encapsulate.h"
#include "IPSObjectCollectionDeserializer.h"
#include "Mailbox.h"

class MailboxCollectionDeserializer : public IPSObjectCollectionDeserializer, public Encapsulate<std::vector<Mailbox>>
{
public:
	void Deserialize(IPSObjectPropertyReader& p_Reader) override;
	void SetCount(size_t p_Count) override;
};

