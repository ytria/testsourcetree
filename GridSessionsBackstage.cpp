#include "GridSessionsBackstage.h"

#include "BaseYTimeDatePropertyRenderer.h"
#include "BackstagePanelSessions.h"
#include "FileUtil.h"
#include "MainFrameSapio365Session.h"
#include "PersistentCacheUtil.h"
#include "RoleDelegationManager.h"
#include "SessionsSqlEngine.h"
#include "SessionStatus.h"
#include "SessionTypes.h"
#include "TaskDataManager.h"
#include "UISessionListSorter.h"
#include "DlgOnPremisesOptions.h"
#include "Sapio365Settings.h"
#include "GraphCache.h"
#include "O365AdminUtil.h"

bool GridSessionsBackstage::AreSupportFeaturesEnabled() const
{
	static wstring supportFeatures;
	static const bool loaded = Registry::readString(HKEY_CURRENT_USER, _YTEXT("Software\\Ytria"), _YTEXT("EnableSupportFeatures"), supportFeatures);

	return loaded ? !supportFeatures.empty() : false;
}

wstring GridSessionsBackstage::g_Favorite;

BEGIN_MESSAGE_MAP(GridSessionsBackstage, CacheGrid)
	ON_COMMAND(ID_SESSIONSGRID_CLEARSQL,			OnClearSQL)
	ON_UPDATE_COMMAND_UI(ID_SESSIONSGRID_CLEARSQL,	OnUpdateClearSQL)
	ON_COMMAND(ID_SESSIONSGRID_USEDELTAUSERS, OnUseDeltaUsers)
	ON_UPDATE_COMMAND_UI(ID_SESSIONSGRID_USEDELTAUSERS, OnUpdateUseDeltaUsers)
	ON_COMMAND(ID_SESSIONSGRID_USEDELTAGROUPS, OnUseDeltaGroups)
	ON_UPDATE_COMMAND_UI(ID_SESSIONSGRID_USEDELTAGROUPS, OnUpdateUseDeltaGroups)
	ON_COMMAND(ID_SESSIONSGRID_EDITONPREM, OnEditOnPremSettings)
	ON_UPDATE_COMMAND_UI(ID_SESSIONSGRID_EDITONPREM, OnUpdateEditOnPremSettings)
END_MESSAGE_MAP()

void GridSessionsBackstage::InitStatics()
{
	static bool init = false;
	 
	if (!init)
	{
		g_Favorite = YtriaTranslate::Do(GridSessionsBackstage_InitStatics_1, _YLOC("Favorite")).c_str();

		init = true;
	}
}

GridSessionsBackstage::GridSessionsBackstage(BackstagePanelSessions& p_PanelSessions)
    : CacheGrid(0, GridBackendUtil::CREATESORTING)
	, m_PanelSessions(p_PanelSessions)
	, m_StandardSessionIconIndex(-1)
	, m_AdvancedSessionIconIndex(-1)
	, m_UltraAdminSessionIconIndex(-1)
    , m_RoleSessionIconIndex(-1)
	, m_RoleEnforcedSessionIconIndex(-1)
	, m_ElevatedSessionIconIndex(-1)
	, m_PartnerAdvancedSessionIconIndex(-1)
	, m_PartnerElevatedSessionIconIndex(-1)
{
	InitStatics();
}

void GridSessionsBackstage::customizeGrid()
{
    SetAutomationName(_YTEXT("BackstageSessionsGrid"));
	AutomationRecordingEnable(false);

	m_FavoriteIconIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_FAVORITE));
	m_StandardSessionIconIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_STANDARD_SESSION_BMP));
	m_AdvancedSessionIconIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ADVANCED_SESSION_BMP));
    m_RoleSessionIconIndex			= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ROLE_SESSION_BMP));
	m_RoleEnforcedSessionIconIndex	= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ROLE_SESSION_ENFORCED_BMP));
	m_UltraAdminSessionIconIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ULTRAADMIN_SESSION_BMP));
	m_ElevatedSessionIconIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ELEVATED_SESSION_BMP));
	m_PartnerAdvancedSessionIconIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PARTNER_SESSION_BMP));
	m_PartnerElevatedSessionIconIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_PARTNER_ELEVATED_SESSION_BMP));
	m_SessionStatusCachedIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SESSIONSTATUS_CACHED_BMP));
	m_SessionStatusCurrentIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SESSIONSTATUS_CURRENT_BMP));
	m_SessionStatusInactiveIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SESSIONSTATUS_INACTIVE_BMP));
	m_SessionStatusLockedIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SESSIONSTATUS_LOCKED_BMP));
	m_SessionStatusStandbyIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SESSIONSTATUS_STANDBY_BMP));

	m_ColFavorite = AddColumnIcon(_YUID("FAVORITE"), YtriaTranslate::Do(GridSessionsBackstage_customizeGrid_1, _YLOC("Favorite")).c_str(), GridBackendUtil::g_DummyString);
	m_ColType = AddColumnIcon(_YUID("TYPE"), YtriaTranslate::Do(GridSessionsBackstage_customizeGrid_2, _YLOC("Type")).c_str(), GridBackendUtil::g_DummyString);

    m_ColSessionDisplayName = AddColumn(_YUID("SESSIONDISPLAYNAME"), YtriaTranslate::Do(GridSessionsBackstage_customizeGrid_3, _YLOC("Session Name")).c_str(), GridBackendUtil::g_DummyString);
    m_ColSessionDisplayName->SetAutoResizeWithGridResize(true);

    m_ColRoleName = AddColumn(_YUID("ROLENAME"), YtriaTranslate::Do(GridDriveItems_customizeGrid_58, _YLOC("Role Based")).c_str(), GridBackendUtil::g_DummyString, 80);
    m_ColRoleName->SetAutoResizeWithGridResize(true);

	m_ColLoginStatus = AddColumnIcon(_YUID("LOGINSTATUS"), YtriaTranslate::Do(GridSessionsBackstage_customizeGrid_8, _YLOC("Status")).c_str(), GridBackendUtil::g_DummyString);

    m_ColTenantNameDisplay = AddColumn(_YUID("TENANTNAME"), YtriaTranslate::Do(GridSessionsBackstage_customizeGrid_4, _YLOC("Tenant Name")).c_str(), GridBackendUtil::g_DummyString, 80);
    m_ColTenantNameDisplay->SetAutoResizeWithGridResize(true);

	m_ColTenantNamePrimary = AddColumn(_YUID("TENANTNAMELICENSE"), YtriaTranslate::Do(GridSessionsBackstage_customizeGrid_9, _YLOC("Tenant Name (Primary)")).c_str(), GridBackendUtil::g_DummyString);
	m_ColTenantNamePrimary->SetAutoResizeWithGridResize(true);

	m_ColAccessCount = AddColumnNumber(_YUID("ACCESSCOUNT"), YtriaTranslate::Do(GridSessionsBackstage_customizeGrid_7, _YLOC("Access count")).c_str(), GridBackendUtil::g_DummyString, 32);

    m_ColLastUsedOn = AddColumnDate(_YUID("LASTUSEDON"), YtriaTranslate::Do(GridSessionsBackstage_customizeGrid_5, _YLOC("Last Used On")).c_str(), GridBackendUtil::g_DummyString, 48);
    m_ColLastUsedOn->SetAutoResizeWithGridResize(true);

	/*m_ColUseDeltaUsers = AddColumnCheckBox(_YUID("DELTAUSERS"), _T("Delta for Users"), GridBackendUtil::g_DummyString);
	m_ColUseDeltaUsers->SetReadOnly(true);
	m_ColUseDeltaGroups = AddColumnCheckBox(_YUID("DELTAGROUP"), _T("Delta for Groups"), GridBackendUtil::g_DummyString);
	m_ColUseDeltaGroups->SetReadOnly(true);*/

	m_ColSessionAppID = AddColumn(_YUID("APPLICATIONID"), YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_18, _YLOC("Application ID")).c_str(), GridBackendUtil::g_DummyString, 135);
	m_ColSessionAppID->SetAutoResizeWithGridResize(true);

	m_ColCreatedOn = AddColumnDate(_YUID("CREATEDON"), YtriaTranslate::Do(GridSessionsBackstage_customizeGrid_6, _YLOC("Created On")).c_str(), GridBackendUtil::g_DummyString, 48);
	m_ColCreatedOn->SetAutoResizeWithGridResize(true);

	m_ColOnPremEnabled = AddColumnCheckBox(_YUID("ONPREMENABLED"), _T("On-Premises enabled"), GridBackendUtil::g_DummyString);
	m_ColOnPremEnabled->SetReadOnly(true);

	m_ColTypeTechnical = AddColumn(_YUID("TYPETECHNICAL"), YtriaTranslate::Do(GridSessionsBackstage_customizeGrid_10, _YLOC("Type Technical")).c_str(), GridBackendUtil::g_DummyString);
	m_ColRoleIDTechnical = AddColumn(_YUID("ROLEID"), YtriaTranslate::Do(GridSessionsBackstage_customizeGrid_12, _YLOC("RoleID Technical")).c_str(), GridBackendUtil::g_DummyString, 32);
    m_ColEmailOrAppId = AddColumn(_YUID("EMAILORAPPID"), _T("Email or App Id"), GridBackendUtil::g_DummyString);

    AddSorting(m_ColLoginStatus, GridBackendUtil::ASC);
	AddSorting(m_ColFavorite, GridBackendUtil::DESC);
	AddSorting(m_ColLastUsedOn, GridBackendUtil::DESC);

    SetColumnVisible(m_ColTypeTechnical, false);
    SetColumnVisible(m_ColEmailOrAppId, false);
    SetColumnVisible(m_ColRoleIDTechnical, false);
	SetColumnVisible(m_ColSessionAppID, false); // Will be shown if necessary in BuildView

	AddColumnForRowPK(m_ColEmailOrAppId);
	AddColumnForRowPK(m_ColTenantNamePrimary);
	AddColumnForRowPK(m_ColRoleIDTechnical);
	AddColumnForRowPK(m_ColCreatedOn);

	/*GetBackend().AddSortingPermanent(m_ColFavorite->GetID());
	GetBackend().AddSortingPermanent(m_ColLastUsedOn->GetID());*/
}

void GridSessionsBackstage::OnCustomDoubleClick()
{
    m_PanelSessions.OnLoadSession();
}

void GridSessionsBackstage::Load(bool p_KeepSelection)
{
	vector<vector<GridBackendField>> selectedPks;
	if (p_KeepSelection)
	{
		for (auto& r : GetSelectedRows())
		{
			selectedPks.emplace_back();
			GetRowPK(r, selectedPks.back());
		}
	}

	RemoveAllRows();

	auto sessions = SessionsSqlEngine::Get().GetList();
	std::sort(sessions.begin(), sessions.end(), UISessionListSorter());

	BuildView(sessions);
	UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

	for (auto& pk : selectedPks)
	{
		auto row = GetRowIndex().GetExistingRow(pk);
		if (nullptr != row)
			row->SetSelected(true);
	}
}

void GridSessionsBackstage::BuildView(const vector<PersistentSession>& p_Sessions)
{
	m_MainFrameSession.reset();
	{
		shared_ptr<Sapio365Session> mainSession = MainFrameSapio365Session();
		if (mainSession)
			m_MainFrameSession = mainSession->GetIdentifier();
	}

	bool hasAppID = false;
	m_Sessions = p_Sessions;
    size_t index = 0;
	for (const auto& session : p_Sessions)
	{
		GridBackendRow* row = AddRow();
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			if (session.IsFavorite())
				row->AddField(g_Favorite, m_ColFavorite, m_FavoriteIconIndex);
			else
				row->AddField(_YTEXT(""), m_ColFavorite);
            
            row->AddField(session.GetSessionName(), m_ColSessionDisplayName);
			row->AddField(session.GetTenantDisplayName(), m_ColTenantNameDisplay);
			row->AddField(session.GetTenantName(), m_ColTenantNamePrimary);
			row->AddField(session.GetLastUsedOn(), m_ColLastUsedOn);
			row->AddField(session.GetCreatedOn(), m_ColCreatedOn);
			row->AddField(session.GetAccessCount(), m_ColAccessCount);

            row->AddField(PersistentSession::GetStatusString(session.GetStatus(session.GetRoleId())), m_ColLoginStatus).SetIcon(GetSessionStatusIcon(session));
            row->AddField(session.GetSessionType(), m_ColTypeTechnical);
			
            row->AddField(GetSessionTypeText(session), m_ColType, GetSessionTypeIcon(session));
            row->AddField(session.GetEmailOrAppId(), m_ColEmailOrAppId);

			if (session.IsUltraAdmin())
			{
				row->AddField(session.GetEmailOrAppId(), m_ColSessionAppID);
				hasAppID = true;
			}
			else if (session.IsElevated() || session.IsPartnerElevated())
			{
				row->AddField(session.GetAppId(), m_ColSessionAppID);
				hasAppID = true;
			}

            row->AddField(std::to_wstring(session.GetRoleId()), m_ColRoleIDTechnical);
            if (session.GetRoleId() != 0)
                row->AddField(session.GetRoleNameInDb(), m_ColRoleName);

			row->AddFieldForCheckBox(session.GetUseOnPrem(), m_ColOnPremEnabled);
			/*row->AddFieldForCheckBox(session.UseDeltaUsers(), m_ColUseDeltaUsers);
			row->AddFieldForCheckBox(session.UseDeltaGroups(), m_ColUseDeltaGroups);*/

			row->SetLParam((LPARAM)&m_Sessions[index]);
            ++index;
		}
	}

	SetColumnVisible(m_ColSessionAppID, hasAppID, GridBackendUtil::SETVISIBLE_NONE);
}

wstring GridSessionsBackstage::GetSessionTypeText(const PersistentSession& p_Session)
{
    wstring text;
	if (p_Session.IsStandard())
		text = YtriaTranslate::Do(GridSessionsBackstage_BuildView_1, _YLOC("Standard")).c_str();
	else if (p_Session.IsAdvanced())
		text = YtriaTranslate::Do(GridSessionsBackstage_BuildView_2, _YLOC("Advanced")).c_str();
	else if (p_Session.IsUltraAdmin())
		text = YtriaTranslate::Do(GridSessionsBackstage_BuildView_3, _YLOC("Ultra Admin")).c_str();
	else if (p_Session.IsRole())
		text = _T("Role Based");
	else if (p_Session.IsElevated())
		text = _T("Elevated");
	else if (p_Session.IsPartnerAdvanced())
		text = _T("Partner: Advanced");
	else if (p_Session.IsPartnerElevated())
		text = _T("Partner: Elevated");
	else
		ASSERT(false);

    return text;
}

int GridSessionsBackstage::GetSessionTypeIcon(const PersistentSession& p_Session)
{
    int icon = -1;
	if (p_Session.IsStandard())
		icon = m_StandardSessionIconIndex;
	else if (p_Session.IsAdvanced())
		icon = m_AdvancedSessionIconIndex;
	else if (p_Session.IsUltraAdmin())
		icon = m_UltraAdminSessionIconIndex;
	else if (p_Session.IsRole())
		icon = RoleDelegationManager::GetInstance().IsRoleEnforced(p_Session.GetRoleId(), MainFrameSapio365Session()) ? m_RoleEnforcedSessionIconIndex : m_RoleSessionIconIndex;
	else if (p_Session.IsElevated())
		icon = m_ElevatedSessionIconIndex;
	else if (p_Session.IsPartnerAdvanced())
		icon = m_PartnerAdvancedSessionIconIndex;
	else if (p_Session.IsPartnerElevated())
		icon = m_PartnerElevatedSessionIconIndex;

    ASSERT(icon != -1);
    return icon;
}

int GridSessionsBackstage::GetSessionStatusIcon(const PersistentSession& p_Session)
{
    int statusIcon = NO_ICON;

    switch (p_Session.GetStatus(p_Session.GetRoleId()))
    {
    case SessionStatus::INVALID:
        ASSERT(false);
        break;
    case SessionStatus::ACTIVE:
        statusIcon = m_SessionStatusCurrentIndex;
        break;
    case SessionStatus::CACHED:
        statusIcon = m_SessionStatusCachedIndex;
        break;
    case SessionStatus::INACTIVE:
        statusIcon = m_SessionStatusInactiveIndex;
        break;
    case SessionStatus::LOCKED:
        statusIcon = m_SessionStatusLockedIndex;
        break;
    default:
        ASSERT(false);
        break;
    }

    return statusIcon;
}

SessionIdentifier GridSessionsBackstage::GetSessionIdentifier(const PersistentSession& p_Session)
{
	return { p_Session.GetEmailOrAppId(), p_Session.GetSessionType(), p_Session.GetRoleId() };
}

bool GridSessionsBackstage::HasNonActiveSelectedSession(bool p_OnlyOne)
{
	bool found = false;
	for (auto& row : GetSelectedRows())
	{
		if (!row->IsGroupRow() && !RowStatusIs(row, SessionStatus::ACTIVE))
		{
			if (p_OnlyOne && found)
			{
				found = false;
				break;
			}

			found = true;

			if (!p_OnlyOne)
				break;
		}
	}

	return found;
}

bool GridSessionsBackstage::HasOneUltraAdminSelectedSession()
{
	auto selectedRows = GetSelectedRows();
	if (selectedRows.size() == 1 &&
		!(*selectedRows.begin())->IsGroupRow() &&
		GetSelectedSession(*(*selectedRows.begin()))->GetSessionType() == SessionTypes::g_UltraAdmin)
		return true;
	return false;
}

bool GridSessionsBackstage::HasAnyUltraAdminSelectedSession()
{
	bool found = false;
	for (auto& row : GetSelectedRows())
	{
		if (row)
		if (row && !row->IsGroupRow() && GetSelectedSession(*row)->GetSessionType() == SessionTypes::g_UltraAdmin)
		{
			found = true;
			break;
		}
	}

	return found;
}

bool GridSessionsBackstage::AllSelectedRowsAreAlreadyFavorite()
{
	vector<GridBackendRow*> selectedRows;
	GetSelectedNonGroupRows(selectedRows);

	for (auto& row : selectedRows)
	{
		const auto& field = row->GetField(m_ColFavorite);

		auto str = field.GetValueStr();
		if (nullptr == str || wstring(str).empty())
			return false;
		ASSERT(wstring(str) == g_Favorite);
	}

	return selectedRows.size() > 0;
}

bool GridSessionsBackstage::HasNonLoggedOutSelectedSession(bool p_OnlyOne) const
{
	bool found = false;

	for (auto& row : GetSelectedRows())
	{
		if (!row->IsGroupRow() && !RowStatusIs(row, SessionStatus::LOCKED))
		{
			if (p_OnlyOne && found)
			{
				found = false;
				break;
			}

			found = true;

			if (!p_OnlyOne)
				break;
		}
	}

	return found;
}

bool GridSessionsBackstage::HasInactiveElevatedSessionSelected() const
{
	for (auto row : GetSelectedRows())
	{
		ASSERT(row != nullptr);
		if (row != nullptr)
		{
			const auto& session = reinterpret_cast<PersistentSession*>(row->GetLParam());
			if ((session->IsElevated() || session->IsPartnerElevated()) && !RowStatusIs(row, SessionStatus::ACTIVE))
				return true;
		}
	}

	return false;
}

bool GridSessionsBackstage::IsMainFrameSessionSelected() const
{
	if (!m_MainFrameSession)
		return false;

	for (auto row : GetSelectedRows())
	{
		if (IsMainFrameSession(row))
			return true;
	}

	return false;
}

const PersistentSession* GridSessionsBackstage::GetSelectedSession() const
{
	const auto& selectedRows = GetSelectedRows();
	if (selectedRows.size() == 1 && !(*selectedRows.begin())->IsGroupRow())
	{
		auto row = *selectedRows.begin();
		return reinterpret_cast<PersistentSession*>(row->GetLParam());
	}

	return nullptr;
}

const PersistentSession* GridSessionsBackstage::GetSelectedSession(GridBackendRow& p_Row) const
{
	return reinterpret_cast<PersistentSession*>(p_Row.GetLParam());
}

bool GridSessionsBackstage::RowStatusIs(GridBackendRow* p_Row, SessionStatus p_Status) const
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		const auto& field = p_Row->GetField(m_ColLoginStatus);
		return p_Status == PersistentSession::GetStatus(field.GetValueStr());
	}
	return false;
}

bool GridSessionsBackstage::IsMainFrameSession(GridBackendRow* p_Row) const
{
	if (!m_MainFrameSession)
		return false;

	ASSERT(p_Row != nullptr);
	if (p_Row != nullptr)
	{
		const auto session = reinterpret_cast<PersistentSession*>(p_Row->GetLParam());
		return nullptr != session && *m_MainFrameSession == GetSessionIdentifier(*session);
	}

	return false;
}

GridBackendColumn* GridSessionsBackstage::GetEmailOrAppIdCol() const
{
	return m_ColEmailOrAppId;
}

GridBackendColumn* GridSessionsBackstage::GetSessionTypeTechnicalCol() const
{
	return m_ColTypeTechnical;
}

GridBackendColumn* GridSessionsBackstage::GetColumnRoleID() const
{
	return m_ColRoleIDTechnical;
}

GridBackendColumn* GridSessionsBackstage::GetColumnRoleName() const
{
	return m_ColRoleName;
}

void GridSessionsBackstage::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart /*= 0*/)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		if (AreSupportFeaturesEnabled())
		{
			pPopup->ItemInsert(ID_SESSIONSGRID_USEDELTAUSERS);
			pPopup->ItemInsert(ID_SESSIONSGRID_USEDELTAGROUPS);
			pPopup->ItemInsert(); // Sep
		}
		pPopup->ItemInsert(ID_SESSIONSGRID_EDITONPREM);
		pPopup->ItemInsert(); // Sep

		// !!! KEEP THIS SANDBOXED, WE DON'T WANT TO RELEASE IT EVER (AS IS)
		if (CRMpipe().IsFeatureSandboxed())
		{
			pPopup->ItemInsert(ID_SESSIONSGRID_CLEARSQL);
			pPopup->ItemInsert(); // Sep
		}
	}
}

void GridSessionsBackstage::InitializeCommands()
{
	CacheGrid::InitializeCommands();

	//std::map<UINT, CString> acceleratorTexts;
	//const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	//ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	if (AreSupportFeaturesEnabled())
	{
		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID = ID_SESSIONSGRID_USEDELTAUSERS;
			_cmd.m_sMenuText = _T("Use Delta for Users");
			_cmd.m_sToolbarText = _YTEXT("Use Delta for Users");
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}

		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID = ID_SESSIONSGRID_USEDELTAGROUPS;
			_cmd.m_sMenuText = _YTEXT("Use Delta for Groups");
			_cmd.m_sToolbarText = _YTEXT("Use Delta for Groups");
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_SESSIONSGRID_EDITONPREM;
		_cmd.m_sMenuText = _YTEXT("Edit On-Prem settings");
		_cmd.m_sToolbarText = _YTEXT("Edit On-Prem settings");
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(profileName, _cmd);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_SESSIONSGRID_CLEARSQL;
		_cmd.m_sMenuText = _YTEXT("Clear SQL dB");
		_cmd.m_sToolbarText = _YTEXT("");
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(profileName, _cmd);
	}
}

void GridSessionsBackstage::OnClearSQL()
{
	// !!! KEEP THIS SANDBOXED, WE DON'T WANT TO RELEASE IT EVER (AS IS)
	ASSERT(CRMpipe().IsFeatureSandboxed());
	if (CRMpipe().IsFeatureSandboxed() && !TaskDataManager::Get().HasTaskRunning())
	{
		YCodeJockMessageBox dlg(this,
			DlgMessageBox::eIcon_Question,
			_YTEXT("sapio365"),
			_YTEXT("Clear selected sessions SQL cache dB?"),
			_YTEXT("For active sessions, all entries will be deleted.\nFor other sessions, the whole files will be deleted."),
			{ { IDOK, _YTEXT("OK") },{ IDCANCEL, _YTEXT("Cancel") } });

		if (IDOK == dlg.DoModal())
		{
			for (auto row : GetSelectedRows())
			{
				const wstring emailOrAppId = row->GetField(m_ColEmailOrAppId).GetValueStr();
				const wstring sessionType = row->GetField(m_ColTypeTechnical).GetValueStr();
				const int64_t roleID = row->HasField(m_ColRoleIDTechnical) ? Str::getINT64FromString(row->GetField(m_ColRoleIDTechnical).GetValueStr()) : 0;

				if (RowStatusIs(row, SessionStatus::ACTIVE))
				{
					auto session = Sapio365Session::Find({ emailOrAppId, sessionType, roleID });
					ASSERT(session);
					if (session)
					{
						const auto error = session->GetGraphCache().GetSqlCache()->Clear();
						if (!error.empty())
						{
							const PooledString roleName = row->GetField(m_ColRoleName).GetValueStr();
							const wstring message = roleName.IsEmpty()
								? _YTEXTFORMAT(L"Unable to clear SQL Cache dB for %s", emailOrAppId.c_str()).c_str()
								: _YTEXTFORMAT(L"Unable to clear SQL Cache dB for [%s] %s", roleName.c_str(), emailOrAppId.c_str()).c_str();

							YCodeJockMessageBox dlgError(this,
								DlgMessageBox::eIcon_Error,
								_YTEXT("sapio365"),
								message.c_str(),
								error.c_str(),
								{ { IDOK, _YTEXT("OK") },{ IDCANCEL, _YTEXT("Cancel") } });
							dlgError.DoModal();
						}
					}
				}
				else
				{
					const auto errorCode = PersistentCacheUtil::DeleteDatabase({ emailOrAppId, sessionType, roleID });
					if (0 != errorCode)
					{
						const PooledString roleName = row->GetField(m_ColRoleName).GetValueStr();
						const wstring message = roleName.IsEmpty()
							? _YTEXTFORMAT(L"Unable to delete SQL Cache dB file for %s", emailOrAppId.c_str()).c_str()
							: _YTEXTFORMAT(L"Unable to delete SQL Cache dB file for [%s] %s", roleName.c_str(), emailOrAppId.c_str()).c_str();

						YCodeJockMessageBox dlgError(this,
							DlgMessageBox::eIcon_Error,
							_YTEXT("sapio365"),
							message.c_str(),
							_YTEXTFORMAT(L"Error code: %s", Str::getStringHexaFromDWORD(errorCode, true).c_str()),
							{ { IDOK, _YTEXT("OK") },{ IDCANCEL, _YTEXT("Cancel") } });
						dlgError.DoModal();
					}
				}
			}
		}
	}
}

void GridSessionsBackstage::OnUpdateClearSQL(CCmdUI* pCmdUI)
{
	// !!! KEEP THIS SANDBOXED, WE DON'T WANT TO RELEASE IT EVER (AS IS)
	pCmdUI->Enable(CRMpipe().IsFeatureSandboxed() && HasOneNonGroupRowSelectedAtLeast() && !TaskDataManager::Get().HasTaskRunning());
}

void GridSessionsBackstage::OnUseDeltaUsers()
{
	auto sel = GetSelectedSession();
	ASSERT(nullptr != sel);
	if (nullptr != sel)
	{
		auto modSel = *sel;
		modSel.SetUseDeltaUsers(!modSel.UseDeltaUsers());
		SessionsSqlEngine::Get().Save(modSel);
		Load(true);
		auto session = Sapio365Session::Find(modSel.GetIdentifier());
		if (session)
			Sapio365Session::NotifyFrames(session);
	}
}

void GridSessionsBackstage::OnUpdateUseDeltaUsers(CCmdUI* pCmdUI)
{
	auto sel = GetSelectedSession();
	pCmdUI->Enable(nullptr != sel);
	pCmdUI->SetCheck(nullptr != sel && sel->UseDeltaUsers());
}

void GridSessionsBackstage::OnUseDeltaGroups()
{
	auto sel = GetSelectedSession();
	ASSERT(nullptr != sel);
	if (nullptr != sel)
	{
		auto modSel = *sel;
		modSel.SetUseDeltaGroups(!modSel.UseDeltaGroups());
		SessionsSqlEngine::Get().Save(modSel);
		Load(true);
	}
}

void GridSessionsBackstage::OnUpdateUseDeltaGroups(CCmdUI* pCmdUI)
{
	auto sel = GetSelectedSession();
	pCmdUI->Enable(nullptr != sel);
	pCmdUI->SetCheck(nullptr != sel && sel->UseDeltaGroups());
}

void GridSessionsBackstage::OnEditOnPremSettings()
{
	ASSERT(nullptr != GetSelectedSession());
	if (nullptr != GetSelectedSession())
	{
		const auto& selectedSession = *GetSelectedSession();

		PersistentSession session = SessionsSqlEngine::Get().Load(selectedSession.GetIdentifier());

		auto sessionFound = Sapio365Session::Find(session.GetIdentifier());

		DlgOnPremisesOptions dlgOpt(nullptr,
			session.GetUseOnPrem(),
			session.GetDontAskAgainUseOnPrem(),
			session.GetAADComputerName(),
			session.GetAutoLoadOnPrem(),
			session.GetUseMsDsConsistencyGuid(),
			session.GetUseRemoteRSAT(),
			session.GetADDSServer(),
			session.GetADDSUsername());
		
		if (nullptr != sessionFound)
		{
			auto hybridCheckResult = Util::DoHybridCheck(sessionFound);
			if (hybridCheckResult.m_CanDoHybrid)
			{
				dlgOpt.SetADDSDomain(hybridCheckResult.m_ADDSDomain);
				dlgOpt.SetVerifiedDomains(hybridCheckResult.m_VerifiedDomains);
			}
		}

		auto dlgResult = dlgOpt.DoModal();
		if (dlgResult == IDOK)
		{
			GraphCache* graphCache = nullptr;
			if (nullptr != sessionFound)
				graphCache = &sessionFound->GetGraphCache();
			DlgOnPremisesOptions::SaveOnPremisesOptions(dlgOpt, session, graphCache);
			Load(true);
		}
	}
}

void GridSessionsBackstage::OnUpdateEditOnPremSettings(CCmdUI* pCmdUI)
{
	auto sel = GetSelectedSession();
	pCmdUI->Enable(nullptr != sel);
}
