#include "LinkHandlerOnPremiseGroups.h"

#include "Command.h"
#include "CommandDispatcher.h"

void LinkHandlerOnPremiseGroups::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::MainModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::OnPremiseGroups, Command::ModuleTask::List, info, { nullptr, g_ActionNameShowGroups, nullptr, p_Link.GetAutomationAction() }));
}
