#include "SkuAndPlanInfoSqlEngine.h"

SkuAndPlanInfoSQLEngine::SkuAndPlanInfoSQLEngine(const wstring& p_DBfile)
	: O365SQLiteEngine(p_DBfile, "sapio365", "")
{
	init();
}

SkuAndPlanInfoSQLEngine::~SkuAndPlanInfoSQLEngine()
{
	sqlite3_finalize(m_SqlStatementGetAllSkus);
	sqlite3_finalize(m_SqlStatementGetSku);
	sqlite3_finalize(m_SqlStatementSetSku);
	sqlite3_finalize(m_SqlStatementDeleteSku);

	sqlite3_finalize(m_SqlStatementGetAllPlans);
	sqlite3_finalize(m_SqlStatementGetPlan);
	sqlite3_finalize(m_SqlStatementSetPlan);
	sqlite3_finalize(m_SqlStatementDeletePlan);
}

void SkuAndPlanInfoSQLEngine::init()
{
	{
		m_TableSku.m_Name = _YTEXT("Skus");

		{
			SQLiteUtil::Column col;
			col.m_Name = _YTEXT("id");
			col.m_Flags = SQLiteUtil::PK;
			col.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;
			m_TableSku.AddColumn(col);
		}

		{
			SQLiteUtil::Column col;
			col.m_Name = _YTEXT("name");
			col.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;
			m_TableSku.AddColumn(col);
		}

		{
			SQLiteUtil::Column col;
			col.m_Name = _YTEXT("label");
			col.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;
			m_TableSku.AddColumn(col);
		}

		{
			SQLiteUtil::Column col;
			col.m_Name = _YTEXT("plans");
			col.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;
			m_TableSku.AddColumn(col);
		}
	}

	{
		m_TablePlan.m_Name = _YTEXT("Plans");

		{
			SQLiteUtil::Column col;
			col.m_Name = _YTEXT("id");
			col.m_Flags = SQLiteUtil::PK;
			col.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;
			m_TablePlan.AddColumn(col);
		}

		{
			SQLiteUtil::Column col;
			col.m_Name = _YTEXT("name");
			col.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;
			m_TablePlan.AddColumn(col);
		}

		{
			SQLiteUtil::Column col;
			col.m_Name = _YTEXT("label");
			col.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;
			m_TablePlan.AddColumn(col);
		}

		{
			SQLiteUtil::Column col;
			col.m_Name = _YTEXT("exclusions");
			col.m_DataType = SQLiteUtil::DataType::SQL_TYPE_TEXT;
			m_TablePlan.AddColumn(col);
		}
	}

	createTables(GetTables());

	{
		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXT("SELECT * FROM Skus"));
			const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementGetAllSkus, nullptr);
			ASSERT(res == SQLITE_OK);
		}

		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXT("SELECT * FROM Skus WHERE id=@ID"));
			const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementGetSku, nullptr);
			ASSERT(res == SQLITE_OK);
		}

		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXT("INSERT OR REPLACE INTO Skus(id, name, label, plans) VALUES(@ID, @NAM, @LAB, @PLA)"));
			const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementSetSku, nullptr);
			ASSERT(res == SQLITE_OK);
		}

		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXT("DELETE FROM Skus WHERE id=@ID"));
			const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementDeleteSku, nullptr);
			ASSERT(res == SQLITE_OK);
		}
	}

	{
		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXT("SELECT * FROM Plans"));
			const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementGetAllPlans, nullptr);
			ASSERT(res == SQLITE_OK);
		}

		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXT("SELECT * FROM Plans WHERE id=@ID"));
			const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementGetPlan, nullptr);
			ASSERT(res == SQLITE_OK);
		}

		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXT("INSERT OR REPLACE INTO Plans(id, name, label, exclusions) VALUES(@ID, @NAM, @LAB, @EXC)"));
			const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementSetPlan, nullptr);
			ASSERT(res == SQLITE_OK);
		}

		{
			const auto queryAscii = MFCUtil::convertUNICODE_to_ASCII(_YTEXT("DELETE FROM Plans WHERE id=@ID"));
			const auto res = sqlite3_prepare_v2(GetDbHandle(), queryAscii.c_str(), static_cast<int>(queryAscii.size()), &m_SqlStatementDeletePlan, nullptr);
			ASSERT(res == SQLITE_OK);
		}
	}
}

SkuAndPlanInfoSQLEngine::SkuInfo SkuAndPlanInfoSQLEngine::getSkuInfo(sqlite3_stmt* p_Stmt)
{
	SkuInfo skuInfo;

	const auto numCol = sqlite3_column_count(p_Stmt);

	for (int i = 0; i < numCol; ++i)
	{
		const auto colName = Str::convertFromUTF8(reinterpret_cast<const char*>(sqlite3_column_name(p_Stmt, i)));
		if (colName == _YTEXT("id"))
		{
			auto textPtr = reinterpret_cast<const char*>(sqlite3_column_text(p_Stmt, i));
			if (nullptr != textPtr)
				skuInfo.m_Id = Str::convertFromUTF8(textPtr);
		}
		else if (colName == _YTEXT("name"))
		{
			auto textPtr = reinterpret_cast<const char*>(sqlite3_column_text(p_Stmt, i));
			if (nullptr != textPtr)
				skuInfo.m_Name = Str::convertFromUTF8(textPtr);
		}
		else if (colName == _YTEXT("label"))
		{
			auto textPtr = reinterpret_cast<const char*>(sqlite3_column_text(p_Stmt, i));
			if (nullptr != textPtr)
				skuInfo.m_Label = Str::convertFromUTF8(textPtr);
		}
		else if (colName == _YTEXT("plans"))
		{
			auto textPtr = reinterpret_cast<const char*>(sqlite3_column_text(p_Stmt, i));
			if (nullptr != textPtr)
				skuInfo.m_PlanIds = Str::explodeIntoSet(PooledString(Str::convertFromUTF8(textPtr)), PooledString(_YTEXT(";")), false, true);
		}
	}

	return skuInfo;
}

SkuAndPlanInfoSQLEngine::PlanInfo SkuAndPlanInfoSQLEngine::getPlanInfo(sqlite3_stmt* p_Stmt)
{
	PlanInfo planInfo;

	const auto numCol = sqlite3_column_count(p_Stmt);

	for (int i = 0; i < numCol; ++i)
	{
		const auto colName = Str::convertFromUTF8(reinterpret_cast<const char*>(sqlite3_column_name(p_Stmt, i)));
		if (colName == _YTEXT("id"))
		{
			auto textPtr = reinterpret_cast<const char*>(sqlite3_column_text(p_Stmt, i));
			if (nullptr != textPtr)
				planInfo.m_Id = Str::convertFromUTF8(textPtr);
		}
		else if (colName == _YTEXT("name"))
		{
			auto textPtr = reinterpret_cast<const char*>(sqlite3_column_text(p_Stmt, i));
			if (nullptr != textPtr)
				planInfo.m_Name = Str::convertFromUTF8(textPtr);
		}
		else if (colName == _YTEXT("label"))
		{
			auto textPtr = reinterpret_cast<const char*>(sqlite3_column_text(p_Stmt, i));
			if (nullptr != textPtr)
				planInfo.m_Label = Str::convertFromUTF8(textPtr);
		}
		else if (colName == _YTEXT("exclusions"))
		{
			auto textPtr = reinterpret_cast<const char*>(sqlite3_column_text(p_Stmt, i));
			if (nullptr != textPtr)
				planInfo.m_Exclusions = std::make_shared<std::set<PooledString>>(Str::explodeIntoSet(PooledString(Str::convertFromUTF8(textPtr)), PooledString(_YTEXT(";")), false, true));
		}
	}

	return planInfo;
}

vector<SQLiteUtil::Table> SkuAndPlanInfoSQLEngine::GetTables() const
{
	// Unused, but interface declares it pure virtual
	return { m_TableSku , m_TablePlan };
}

std::pair<std::map<PooledString, SkuAndPlanInfoSQLEngine::SkuInfo>, std::map<PooledString, SkuAndPlanInfoSQLEngine::PlanInfo>> SkuAndPlanInfoSQLEngine::Load() const
{
	return { LoadSkus(),  LoadPlans() };
}

void SkuAndPlanInfoSQLEngine::Save(const std::map<PooledString, SkuInfo>& p_Skus)
{
	for (const auto& sku : p_Skus)
		SaveSku(sku.second);
}

void SkuAndPlanInfoSQLEngine::Save(const std::map<PooledString, PlanInfo>& p_Plans)
{
	for (const auto& plan : p_Plans)
		SavePlan(plan.second);
}

std::map<PooledString, SkuAndPlanInfoSQLEngine::SkuInfo> SkuAndPlanInfoSQLEngine::LoadSkus() const
{
	std::map<PooledString, SkuAndPlanInfoSQLEngine::SkuInfo> skuInfos;
	int status = SQLITE_OK;
	while (SQLITE_ROW == (status = this->sqlite3_step(m_SqlStatementGetAllSkus)))
	{
		const auto sku = getSkuInfo(m_SqlStatementGetAllSkus);
		skuInfos[sku.m_Id] = sku;
	}
	ProcessError(status, nullptr, _YTEXT(""));
	sqlite3_reset(m_SqlStatementGetAllSkus);
	return skuInfos;
}

std::map<PooledString, SkuAndPlanInfoSQLEngine::PlanInfo> SkuAndPlanInfoSQLEngine::LoadPlans() const
{
	std::map<PooledString, SkuAndPlanInfoSQLEngine::PlanInfo> planInfos;
	int status = SQLITE_OK;
	while (SQLITE_ROW == (status = this->sqlite3_step(m_SqlStatementGetAllPlans)))
	{
		const auto plan = getPlanInfo(m_SqlStatementGetAllPlans);
		planInfos[plan.m_Id] = plan;
	}
	ProcessError(status, nullptr, _YTEXT(""));
	sqlite3_reset(m_SqlStatementGetAllPlans);
	return planInfos;
}

boost::YOpt<SkuAndPlanInfoSQLEngine::SkuInfo> SkuAndPlanInfoSQLEngine::LoadSku(const PooledString& p_Id) const
{
	boost::YOpt<SkuAndPlanInfoSQLEngine::SkuInfo> skuInfo;
	const auto id = Str::convertToUTF8(p_Id);
	auto status = sqlite3_bind_text(m_SqlStatementGetSku, 1, id.c_str(), -1, SQLITE_STATIC);
	if (SQLITE_OK == status)
	{
		while (SQLITE_ROW == (status = this->sqlite3_step(m_SqlStatementGetSku)))
		{
			ASSERT(!skuInfo); // Several results...?
			skuInfo = getSkuInfo(m_SqlStatementGetSku);
		}
	}
	ProcessError(status, nullptr, _YTEXT(""));
	sqlite3_reset(m_SqlStatementGetSku);
	return skuInfo;
}

boost::YOpt<SkuAndPlanInfoSQLEngine::PlanInfo> SkuAndPlanInfoSQLEngine::LoadPlan(const PooledString& p_Id) const
{
	boost::YOpt<SkuAndPlanInfoSQLEngine::PlanInfo> planInfo;
	const auto id = Str::convertToUTF8(p_Id);
	auto status = sqlite3_bind_text(m_SqlStatementGetPlan, 1, id.c_str(), -1, SQLITE_STATIC);
	if (SQLITE_OK == status)
	{
		while (SQLITE_ROW == (status = this->sqlite3_step(m_SqlStatementGetPlan)))
		{
			ASSERT(!planInfo); // Several results...?
			planInfo = getPlanInfo(m_SqlStatementGetPlan);
		}
	}
	ProcessError(status, nullptr, _YTEXT(""));
	sqlite3_reset(m_SqlStatementGetPlan);
	return planInfo;
}

void SkuAndPlanInfoSQLEngine::SaveSku(const SkuInfo& p_SkuInfo)
{
	int status = SQLITE_OK;
	ASSERT(!p_SkuInfo.m_Id.IsEmpty());
	const auto id = Str::convertToUTF8(p_SkuInfo.m_Id);
	const auto name = Str::convertToUTF8(p_SkuInfo.m_Name);
	const auto label = Str::convertToUTF8(p_SkuInfo.m_Label);
	const auto plans = Str::convertToUTF8(Str::implode(p_SkuInfo.m_PlanIds, _YTEXT(";"), false));
	
	int i = 1;
	status = sqlite3_bind_text(m_SqlStatementSetSku, i++, id.c_str(), -1, SQLITE_STATIC);
	ASSERT(status == SQLITE_OK);
	status = sqlite3_bind_text(m_SqlStatementSetSku, i++, name.c_str(), -1, SQLITE_STATIC);
	ASSERT(status == SQLITE_OK);
	status = sqlite3_bind_text(m_SqlStatementSetSku, i++, label.c_str(), -1, SQLITE_STATIC);
	ASSERT(status == SQLITE_OK);
	status = sqlite3_bind_text(m_SqlStatementSetSku, i++, plans.c_str(), -1, SQLITE_STATIC);
	ASSERT(status == SQLITE_OK);

	status = this->sqlite3_step(m_SqlStatementSetSku);
	ASSERT(status == SQLITE_DONE);
	ProcessError(status, nullptr, _YTEXT(""));
	sqlite3_reset(m_SqlStatementSetSku);
}

void SkuAndPlanInfoSQLEngine::SavePlan(const PlanInfo& p_PlanInfo)
{
	int status = SQLITE_OK;

	ASSERT(!p_PlanInfo.m_Id.IsEmpty());
	const auto id = Str::convertToUTF8(p_PlanInfo.m_Id);
	const auto name = Str::convertToUTF8(p_PlanInfo.m_Name);
	const auto label = Str::convertToUTF8(p_PlanInfo.m_Label);
	const auto plans = p_PlanInfo.m_Exclusions ? Str::convertToUTF8(Str::implode(*p_PlanInfo.m_Exclusions, _YTEXT(";"), false)) : std::string();
	
	int index = 1;
	status = sqlite3_bind_text(m_SqlStatementSetPlan, index++, id.c_str(), -1, SQLITE_STATIC);
	ASSERT(status == SQLITE_OK);
	status = sqlite3_bind_text(m_SqlStatementSetPlan, index++, name.c_str(), -1, SQLITE_STATIC);
	ASSERT(status == SQLITE_OK);
	status = sqlite3_bind_text(m_SqlStatementSetPlan, index++, label.c_str(), -1, SQLITE_STATIC);
	ASSERT(status == SQLITE_OK);
	if (p_PlanInfo.m_Exclusions)
		status = sqlite3_bind_text(m_SqlStatementSetPlan, index++, plans.c_str(), -1, SQLITE_STATIC);
	else
		status = sqlite3_bind_null(m_SqlStatementSetPlan, index++);
	ASSERT(status == SQLITE_OK);

	status = this->sqlite3_step(m_SqlStatementSetPlan);
	ASSERT(status == SQLITE_DONE);
	ProcessError(status, nullptr, _YTEXT(""));
	sqlite3_reset(m_SqlStatementSetPlan);
}
