#pragma once

#include "IRequester.h"
#include "SpUtils.h"

#include "SpUser.h"
#include "SpUserDeserializer.h"
#include "ValueListDeserializer.h"

class SingleRequestResult;

namespace Sp
{
	class SpUsersRequester : public IRequester
	{
	public:
		SpUsersRequester(PooledString p_SiteUrl);
		TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

		const vector<Sp::User>& GetData() const;

	private:
		PooledString m_SiteUrl;
		std::shared_ptr<ValueListDeserializer<Sp::User, Sp::UserDeserializer>> m_Deserializer;
		std::shared_ptr<SingleRequestResult> m_Result;
	};
}

