#pragma once

#include "PooledString.h"

struct MessageBodyData
{
    MessageBodyData(const boost::YOpt<PooledString>& p_Content)
        : m_BodyContent(p_Content)
    {}

    boost::YOpt<PooledString> m_BodyContent;
};
