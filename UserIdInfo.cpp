#include "UserIdInfo.h"

RTTR_REGISTRATION
{
    using namespace rttr;
registration::class_<Sp::UserIdInfo>("UserIdInfo") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("User ID Info"))))
.constructor()(policy::ctor::as_object);
}
