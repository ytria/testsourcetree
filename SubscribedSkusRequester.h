#pragma once

#include "IRequester.h"

#include "SingleRequestResult.h"
#include "ValueListDeserializer.h"
#include "SubscribedSkuDeserializer.h"

class IRequestLogger;

class SubscribedSkusRequester : public IRequester
{
public:
	SubscribedSkusRequester(const std::shared_ptr<IRequestLogger>& p_Logger);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	// Just a hack for when we use this requester before role ou elevated sessions are fully created.
	void SetForceUseMainMsGraphSession(bool p_Force);

	const SingleRequestResult& GetResult() const;
	const vector<BusinessSubscribedSku>& GetData() const;

private:
	std::shared_ptr<SingleRequestResult> m_Result;
	std::shared_ptr<ValueListDeserializer<BusinessSubscribedSku, SubscribedSkuDeserializer>> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;

	bool m_ForceUseMainMsGraphSession;
};

