#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "CosmosLocation.h"

class LocationDeserializer : public JsonObjectDeserializer, public Encapsulate<Location>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

