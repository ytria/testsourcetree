#include "ResponseDeserializer.h"

#include "xercesc\dom\DOMElement.hpp"
#include "xercesc\dom\DOMNodeList.hpp"
#include "xercesc\dom\DOMNode.hpp"

using XERCES_CPP_NAMESPACE::DOMElement;

Ex::ResponseDeserializer::ResponseDeserializer(const wstring& p_ResponseNode)
    :m_ResponseNode(p_ResponseNode),
    m_HasError(false)
{
}

void Ex::ResponseDeserializer::DeserializeObject(const XERCES_CPP_NAMESPACE::DOMElement& p_Envelope)
{
    auto body = GetSoapBody(p_Envelope);
    ASSERT(nullptr != body);
    if (nullptr != body)
    {
        auto responseElement = body->getFirstElementChild();
        ASSERT(nullptr != responseElement);
        if (nullptr != responseElement)
        {
            // Error?
            if (wcscmp(responseElement->getAttribute(_YTEXT("ResponseClass")), _YTEXT("Success")) != 0)
            {
                m_HasError = true;
                GetErrorInfo(responseElement);
            }
        }
    }
}

void Ex::ResponseDeserializer::GetErrorInfo(DOMElement * responseElement)
{
    auto children = responseElement->getChildNodes();
    ASSERT(nullptr != children);
    if (nullptr != children)
    {
        size_t childrenCount = children->getLength();
        for (size_t i = 0; i < childrenCount; ++i)
        {
            const auto& item = children->item(i);
            if (item->getNodeType() == XERCES_CPP_NAMESPACE::DOMNode::ELEMENT_NODE)
            {
                auto childElement = dynamic_cast<DOMElement*>(children->item(i));
                ASSERT(nullptr != childElement);
                if (nullptr != childElement)
                {
                    auto childName = childElement->getLocalName();
                    if (wcscmp(childName, _YTEXT("MessageText")) == 0)
                        m_ErrorMessage = childElement->getTextContent();
                    else if (wcscmp(childName, _YTEXT("ResponseCode")) == 0)
                        m_ErrorCode = childElement->getTextContent();
                }
            }
        }
    }
}

bool Ex::ResponseDeserializer::HasError() const
{
    return m_HasError;
}
