#include "DirectoryAuditDeserializer.h"

#include "AuditActivityInitiatorDeserializer.h"
#include "KeyValueDeserializer.h"
#include "ListDeserializer.h"
#include "TargetResourceDeserializer.h"

void DirectoryAuditDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("activityDateTime"), m_Data.m_ActivityDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("activityDisplayName"), m_Data.m_ActivityDisplayName, p_Object);
	{
		ListDeserializer<KeyValue, KeyValueDeserializer> d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("additionalDetails"), p_Object))
			m_Data.m_AdditionalDetails = std::move(d.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("category"), m_Data.m_Category, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("correlationId"), m_Data.m_CorrelationId, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	{
		AuditActivityInitiatorDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("initiatedBy"), p_Object))
			m_Data.m_InitiatedBy = std::move(d.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("loggedByService"), m_Data.m_LoggedByService, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("operationType"), m_Data.m_OperationType, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("result"), m_Data.m_Result, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("resultReason"), m_Data.m_ResultReason, p_Object);
	{
		ListDeserializer<TargetResource, TargetResourceDeserializer> d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("targetResources"), p_Object))
			m_Data.m_TargetResources = std::move(d.GetData());
	}
}
