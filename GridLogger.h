#pragma once

#include "ILogger.h"

class CacheGrid;
class GridLogger : public ILogger
{
public:
	void SetGridHWND(HWND p_hGrid);
	const HWND GetGridHWND() const;

public:
    virtual void Log(const LogEntry& p_Entry) const override;

private:
	HWND m_hGrid = nullptr;
};

