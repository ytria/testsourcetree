#pragma once

#include "BusinessAADUserConversationMember.h"
#include "IRequester.h"
#include "HttpResultWithError.h"

class UpdatePrivateChannelMemberRequester : public IRequester
{
public:
	UpdatePrivateChannelMemberRequester(BusinessAADUserConversationMember p_Member, std::wstring p_TeamId, std::wstring p_ChannelId);
	virtual TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const HttpResultWithError& GetResult() const;

private:
	BusinessAADUserConversationMember m_Member;
	std::wstring m_TeamId;
	std::wstring m_ChannelId;

	std::shared_ptr<HttpResultWithError> m_Result;
};
