#include "BusinessDirectoryRole.h"

#include "BusinessGroup.h"
#include "DirectoryRole.h"
#include "MsGraphFieldNames.h"

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessDirectoryRole>("Directory Role") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Directory Role"))))
		.constructor()(policy::ctor::as_object)
		.property(O365_DIRECTORYROLETEMPLATE_DISPLAYNAME, &BusinessDirectoryRole::m_DisplayName)
		;
}

BusinessDirectoryRole::BusinessDirectoryRole(const DirectoryRole& p_DirectoryRole)
{
    SetID(p_DirectoryRole.m_Id);
    SetDescription(p_DirectoryRole.Description);
    SetDisplayName(p_DirectoryRole.DisplayName);
    SetRoleTemplateId(p_DirectoryRole.RoleTemplateId);
}

const boost::YOpt<PooledString>& BusinessDirectoryRole::GetDescription() const
{
    return m_Description;
}

void BusinessDirectoryRole::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
    m_Description = p_Val;
}

const boost::YOpt<PooledString>& BusinessDirectoryRole::GetDisplayName() const
{
    return m_DisplayName;
}

void BusinessDirectoryRole::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
    m_DisplayName = p_Val;
}

const boost::YOpt<PooledString>& BusinessDirectoryRole::GetRoleTemplateId() const
{
    return m_RoleTemplateId;
}

void BusinessDirectoryRole::SetRoleTemplateId(const boost::YOpt<PooledString>& p_Val)
{
    m_RoleTemplateId = p_Val;
}

void BusinessDirectoryRole::AddMember(const BusinessUser& p_Member)
{
	ASSERT(m_Members.end() == std::find_if(m_Members.begin(), m_Members.end(), [=](const BusinessUser& p_Elem) { return p_Elem.GetID() == p_Member.GetID(); }));
	if (m_Members.end() == std::find_if(m_Members.begin(), m_Members.end(), [=](const BusinessUser& p_Elem) { return p_Elem.GetID() == p_Member.GetID(); }))
		m_Members.push_back(p_Member);
}

const std::vector<BusinessUser>& BusinessDirectoryRole::GetMembers() const
{
    return m_Members;
}

