#include "QueryGetCredentialsId.h"
#include "SqlQuerySelect.h"
#include "O365SQLiteEngine.h"
#include "InlineSqlQueryRowHandler.h"

QueryGetCredentialsId::QueryGetCredentialsId(O365SQLiteEngine& p_Engine, const SessionIdentifier& p_Identifier)
	: QueryGetCredentialsId(p_Engine)
{
	m_Identifier = p_Identifier;
}

QueryGetCredentialsId::QueryGetCredentialsId(O365SQLiteEngine& p_Engine, int64_t p_SessionId)
	: QueryGetCredentialsId(p_Engine)
{
	m_SessionId = p_SessionId;
}

QueryGetCredentialsId::QueryGetCredentialsId(O365SQLiteEngine& p_Engine)
	:m_Engine(p_Engine),
	m_CredId(0)
{
}

void QueryGetCredentialsId::Run()
{
	if (m_Identifier)
		QueryWithSessionIdentifier();
	else if (m_SessionId)
		QueryWithSessionId();
}

void QueryGetCredentialsId::QueryWithSessionId()
{
	auto credHandler = [this](sqlite3_stmt* p_Stmt) {
		m_CredId = sqlite3_column_int(p_Stmt, 0);
	};

	wstring queryCredIdStr = _YTEXT(R"(SELECT CredentialsId FROM "Sessions" WHERE Id=?)");
	SqlQuerySelect queryCredId(m_Engine, InlineSqlQueryRowHandler<decltype(credHandler)>(credHandler), queryCredIdStr);
	queryCredId.BindInt64(1, *m_SessionId);
	queryCredId.Run();
}

void QueryGetCredentialsId::QueryWithSessionIdentifier()
{
	auto credHandler = [this](sqlite3_stmt* p_Stmt) {
		m_CredId = sqlite3_column_int(p_Stmt, 0);
	};

	wstring queryCredIdStr = _YTEXT(R"(SELECT CredentialsId FROM "Sessions" WHERE EmailOrAppId=? AND SessionType=? AND RoleId=?)");
	SqlQuerySelect queryCredId(m_Engine, InlineSqlQueryRowHandler<decltype(credHandler)>(credHandler), queryCredIdStr);
	queryCredId.BindString(1, m_Identifier->m_EmailOrAppId);
	queryCredId.BindString(2, m_Identifier->m_SessionType);
	queryCredId.BindInt64(3, m_Identifier->m_RoleID);
	queryCredId.Run();
}

void QueryGetCredentialsId::HandleRow(sqlite3_stmt* p_Stmt)
{
	m_CredId = sqlite3_column_int(p_Stmt, 0);
}

int64_t QueryGetCredentialsId::GetCredentialsId() const
{
	return m_CredId;
}
