#include "DriveItemImporterFile.h"

#include "DlgLoading.h"
#include "GridDriveItems.h"
#include "FileUtil.h"

void DriveItemImporterFile::Run(const wstring& p_FilePath, const std::set<GridBackendRow*>& p_ParentRows, std::shared_ptr<DlgLoading>& p_Dlg/* = std::shared_ptr<DlgLoading>()*/)
{
	if (p_Dlg)
		p_Dlg->SetText(Str::Replace(p_FilePath, _YTEXT("\\"), _YTEXT("\\\\")));

	const auto fileName = FileUtil::FileGetFileName(p_FilePath);

	std::map<GridBackendRow*, GridBackendRow*> existingFileRows;
	for (auto& parentRow : p_ParentRows) // FIXME: Also check row type
		existingFileRows[parentRow] = m_Grid.GetChildRow(parentRow, fileName, m_Grid.m_ColName->GetID());

	std::map<GridBackendRow*, wstring> newFileNames;

	if (!existingFileRows.empty() && (!m_Replacing.is_initialized() || !*m_Replacing))
	{
		for (auto& item : existingFileRows)
		{
			if (item.second)
			{
				if (m_Replacing.is_initialized())
				{
					if (!*m_Replacing)
					{
						newFileNames[item.first] = generateUniqueFileName(fileName, item.first);
						item.second = nullptr;
					}
				}
				else
				{
					ASSERT(nullptr != item.first || m_Grid.m_MyDataMeHandler);
					const bool addingToRoot = nullptr == item.first || item.first->GetHierarchyLevel() == 0;

					const wstring message = addingToRoot
						? _YFORMAT(L"The destination root folder already contains a file named \"%s\". Would you like to replace it or rename the new one?", fileName.c_str())
						: _YFORMAT(L"The destination folder \"%s\" already contains a file named \"%s\". Would you like to replace it or rename the new one?", item.first->GetField(m_Grid.m_ColName).GetValueStr(), fileName.c_str());

					YCodeJockMessageBox dlg(&m_Grid,
						DlgMessageBox::eIcon_Question,
						_T("Name Conflict"),
						message,
						_YTEXT(""),
						{ { IDOK, _T("Replace") },{ IDCANCEL, _T("Rename") } },
						IDCANCEL);
					dlg.SetVerificationText(_T("Do this for all upcoming conflicts."));

					if (IDOK == dlg.DoModal())
					{
						if (dlg.IsVerificiationChecked())
							m_Replacing = true;
					}
					else
					{
						newFileNames[item.first] = generateUniqueFileName(fileName, item.first);
						item.second = nullptr;
						if (dlg.IsVerificiationChecked())
							m_Replacing = false;
					}
				}
			}
		}
	}

	BusinessDriveItem newItem;
	const auto size = FileUtil::GetFileSize(p_FilePath);
	if (size)
		newItem.SetTotalSize(*size);
	for (auto& parentRow : p_ParentRows)
	{
		auto it = existingFileRows.find(parentRow);
		auto existingRowItem = it != existingFileRows.end() ? *it : std::map<GridBackendRow*, GridBackendRow*>::value_type{};
		if (nullptr != existingRowItem.second)
		{
			// handle row replacement
			ASSERT(newFileNames[existingRowItem.first].empty());

			auto existingRow = existingRowItem.second;

			//updateParentCounters(newItem.GetTotalSize(), existingRow, false);

			const bool hadField = existingRow->HasField(m_Grid.m_ColFileToBeUploaded);
			const GridBackendField oldField = existingRow->GetField(m_Grid.m_ColFileToBeUploaded);
			const GridBackendField& newField = existingRow->AddField(p_FilePath, m_Grid.m_ColFileToBeUploaded);

			// If the old value is the same as the new value, the new field will not be set as modified.
			// This will trig the ASSERT in GridFrameBase::hasModificationsPending.
			if (newField.IsModified())
			{
				row_pk_t rowPK;
				m_Grid.GetRowPK(existingRow, rowPK);

				// Are we editing a created object?
				auto mod = m_Grid.GetModifications().GetRowModification<CreatedObjectModification>(rowPK);
				if (nullptr != mod)
				{
					CreatedObjectModification newMod(m_Grid, rowPK);
					newMod.DontRevertOnDestruction();
					*mod = newMod;
				}
				else
				{
					auto update = std::make_unique<FieldUpdateO365>(
						m_Grid,
						rowPK,
						m_Grid.m_ColFileToBeUploaded->GetID(),
						hadField ? oldField : GridBackendField::g_GenericConstField,
						newField, true);
					m_Grid.GetModifications().Add(std::move(update));
				}

				m_Grid.SetColumnVisible(m_Grid.m_ColFileToBeUploaded, true, GridBackendUtil::SETVISIBLE_NONE);

				existingRow->SetSelected(true);
			}
		}
		else
		{
			const auto& name = newFileNames[existingRowItem.first];
			newItem.SetID(m_Grid.GetNextTemporaryCreatedObjectID());
			newItem.SetName(PooledString(name.empty() ? fileName : name));
			newItem.SetLocalFilePath(PooledString(p_FilePath));
			if (nullptr == parentRow)
			{
				ASSERT(m_Grid.m_MyDataMeHandler);
				if (m_Grid.m_MyDataMeHandler)
					newItem.SetDriveId(m_Grid.m_MyDataMeHandler->GetField(m_Grid.m_ColMetaDriveId).GetValueStr());
			}
			else
			{
				newItem.SetDriveId(parentRow->GetField(m_Grid.m_ColMetaDriveId).GetValueStr());
			}
			existingRowItem.second = m_Grid.fillRow(newItem, parentRow, false, nullptr);
			ASSERT(nullptr != existingRowItem.second);
			if (existingRowItem.second)
			{
				existingRowItem.second->SetSelected(true);
				existingRowItem.second->AddField(p_FilePath, m_Grid.m_ColFileToBeUploaded);

				row_pk_t pk;
				m_Grid.GetRowPK(existingRowItem.second, pk);
				m_Grid.GetModifications().Add(std::make_unique<CreatedObjectModification>(*&m_Grid, pk));
			}

			//updateParentCounters(newItem.GetTotalSize(), existingRowItem.second, true);
		}
	}
}

wstring DriveItemImporterFile::generateUniqueFileName(const wstring& desiredFileName, GridBackendRow* p_ParentRow)
{
	const auto name = FileUtil::FileRemoveExtension(desiredFileName);
	const auto ext = FileUtil::FileGetExtension(desiredFileName);
	wstring filename = desiredFileName;

	GridBackendRow* existingRow = nullptr;
	int renameIndex = 0;
	do
	{
		if (nullptr != existingRow)
			filename = _YTEXTFORMAT(L"%s (%s)%s", name.c_str(), Str::getStringFromNumber(++renameIndex).c_str(), ext.c_str());

		existingRow = m_Grid.GetChildRow(p_ParentRow, filename, m_Grid.m_ColName->GetID());
	} while (nullptr != existingRow);

	return filename;
}

//void DriveItemImporterFile::updateParentCounters(const uintmax_t p_FileSize, GridBackendRow* p_FileRow, bool p_IsNewRow)
//{
//	ASSERT(nullptr != p_FileRow);
//	if (nullptr != p_FileRow)
//	{
//		const auto oldFileSize = !p_IsNewRow && p_FileRow->HasField(m_Grid.m_ColFileSize) ? p_FileRow->GetField(m_Grid.m_ColFileSize).GetValueLong() : 0;
//		auto parentRow = p_FileRow->GetParentRow();
//		while (nullptr != parentRow)
//		{
//			// m_ColFileCount
//			if (p_IsNewRow)
//			{
//				const bool hadField = parentRow->HasField(m_Grid.m_ColFileCount);
//				const GridBackendField oldField = parentRow->GetField(m_Grid.m_ColFileCount);
//				const GridBackendField& newField = parentRow->AddField(hadField ? oldField.GetValueLong() + 1 : 1, m_Grid.m_ColFileCount);
//
//				if (newField.IsModified())
//				{
//					row_pk_t rowPK;
//					m_Grid.GetRowPK(parentRow, rowPK);
//
//					// Are we editing a created object?
//					auto mod = m_Grid.GetModifications().GetRowModification<CreatedObjectModification>(rowPK);
//					if (nullptr != mod)
//					{
//						CreatedObjectModification newMod(m_Grid, rowPK);
//						newMod.DontRevertOnDestruction();
//						*mod = newMod;
//					}
//					else
//					{
//						auto update = std::make_unique<FieldUpdateO365>(
//							m_Grid,
//							rowPK,
//							m_Grid.m_ColFileCount->GetID(),
//							hadField ? oldField : GridBackendField::g_GenericConstField,
//							newField, true);
//						m_Grid.GetModifications().Add(std::move(update));
//					}
//				}
//			}
//
//			if (p_FileSize > 0 || oldFileSize > 0)
//			{
//				// m_ColFolderSize (only direct parent)
//				if (parentRow == p_FileRow->GetParentRow())
//				{
//					const bool hadField = parentRow->HasField(m_Grid.m_ColFolderSize);
//					const GridBackendField oldField = parentRow->GetField(m_Grid.m_ColFolderSize);
//					const GridBackendField& newField = parentRow->AddField(hadField ? oldField.GetValueLong() - oldFileSize + p_FileSize : p_FileSize, m_Grid.m_ColFolderSize);
//
//					if (newField.IsModified())
//					{
//						row_pk_t rowPK;
//						m_Grid.GetRowPK(parentRow, rowPK);
//
//						// Are we editing a created object?
//						auto mod = m_Grid.GetModifications().GetRowModification<CreatedObjectModification>(rowPK);
//						if (nullptr != mod)
//						{
//							CreatedObjectModification newMod(m_Grid, rowPK);
//							newMod.DontRevertOnDestruction();
//							*mod = newMod;
//						}
//						else
//						{
//							auto update = std::make_unique<FieldUpdateO365>(
//								m_Grid,
//								rowPK,
//								m_Grid.m_ColFolderSize->GetID(),
//								hadField ? oldField : GridBackendField::g_GenericConstField,
//								newField, true);
//							m_Grid.GetModifications().Add(std::move(update));
//						}
//					}
//				}
//
//				// m_ColTotalSize
//				{
//					const bool hadField = parentRow->HasField(m_Grid.m_ColTotalSize);
//					const GridBackendField oldField = parentRow->GetField(m_Grid.m_ColTotalSize);
//					const GridBackendField& newField = parentRow->AddField(hadField ? oldField.GetValueLong() - oldFileSize + p_FileSize : p_FileSize, m_Grid.m_ColTotalSize);
//
//					if (newField.IsModified())
//					{
//						row_pk_t rowPK;
//						m_Grid.GetRowPK(parentRow, rowPK);
//
//						// Are we editing a created object?
//						auto mod = m_Grid.GetModifications().GetRowModification<CreatedObjectModification>(rowPK);
//						if (nullptr != mod)
//						{
//							CreatedObjectModification newMod(m_Grid, rowPK);
//							newMod.DontRevertOnDestruction();
//							*mod = newMod;
//						}
//						else
//						{
//							auto update = std::make_unique<FieldUpdateO365>(
//								m_Grid,
//								rowPK,
//								m_Grid.m_ColTotalSize->GetID(),
//								hadField ? oldField : GridBackendField::g_GenericConstField,
//								newField, true);
//							m_Grid.GetModifications().Add(std::move(update));
//						}
//					}
//				}
//			}
//
//			parentRow = parentRow->GetParentRow();
//		}
//	}
//}
