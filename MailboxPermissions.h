#pragma once
#include "BusinessObject.h"

class MailboxPermissions : public BusinessObject
{
public:
	MailboxPermissions();

	std::wstring m_AccessRights;
	std::wstring m_Deny;
	std::wstring m_Identity;
	std::wstring m_InheritanceType;
	std::wstring m_RealAce;
	std::wstring m_User;
	bool m_IsInherited;
	bool m_IsValid;

	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
};

