#include "FrameSiteRoles.h"
#include "GridSiteRoles.h"
#include "Command.h"

IMPLEMENT_DYNAMIC(FrameSiteRoles, GridFrameBase)

void FrameSiteRoles::BuildView(const O365DataMap<BusinessSite, vector<Sp::RoleDefinition>>& p_SpSites, bool p_FullPurge)
{
    m_Grid.BuildView(p_SpSites, p_FullPurge);
}

O365Grid& FrameSiteRoles::GetGrid()
{
    return m_Grid;
}

void FrameSiteRoles::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameSiteRoles::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameSiteRoles::ApplySpecific(bool p_Selected)
{
    ASSERT(false); //TODO
}

void FrameSiteRoles::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::SiteRoles, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameSiteRoles::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::SiteRoles, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

bool FrameSiteRoles::HasApplyButton()
{
    return true;
}

void FrameSiteRoles::createGrid()
{
    m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameSiteRoles::GetRefreshPartialControlConfig()
{
	O365ControlConfig config;
	config.m_ControlID = ID_365REFRESHSELECTED_SITES;
	config.m_Image16x16ResourceID = IDB_REFRESHSELECTED_SITES;
	config.m_Image32x32ResourceID = IDB_REFRESHSELECTED_SITES_16X16;
	config.m_ControlText = YtriaTranslate::Do(FrameDriveItems_CreateRefreshPartial_7, _YLOC("Selected Sites")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(FrameDriveItems_CreateRefreshPartial_8, _YLOC("Refresh Selected Sites")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(FrameDriveItems_CreateRefreshPartial_9, _YLOC("This will refresh the data in the grid for all currently selected sites.")).c_str();
	return config;
}

bool FrameSiteRoles::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
    m_CreateRefreshPartial = true;
    CreateViewGroup(tab);
    CreateRefreshGroup(tab);

    return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameSiteRoles::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
    AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

    return statusRV;
}
