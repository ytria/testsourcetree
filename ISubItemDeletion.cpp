#include "ISubItemDeletion.h"

#include "BaseO365Grid.h"
#include "CompositeSubElementFormatter.h"

ISubItemDeletion::ISubItemDeletion(O365Grid& p_Grid, const SubItemKey& p_Key, GridBackendColumn* p_ColSubItemElder, GridBackendColumn* p_ColSubItemPrimaryKey, const wstring& p_ObjectName) :
    ISubItemModification(p_Grid, p_Key, p_ColSubItemElder, p_ColSubItemPrimaryKey, p_ObjectName)
{
    m_FormatterId = g_FormatterId;
}

void ISubItemDeletion::ShowAppliedLocally(GridBackendRow* p_Row) const
{
    if (!IsCollapsedRow(p_Row))
    {
        GetGrid().OnRowDeleted(p_Row, false);
        AddFormatterForExpandedRow(p_Row);
    }
    else
    {
        p_Row->SetEditModified(true);
        GetGrid().OnRowModified(p_Row);
        AddFormatterForCollapsedRow(p_Row);
    }
}

std::unique_ptr<SubElementPrefixFormatter> ISubItemDeletion::CreatePrefixFormatter() const
{
    return std::make_unique<SubElementPrefixFormatter>(_YTEXT("[DEL]"));
}

void ISubItemDeletion::AddFormatterForExpandedRow(GridBackendRow* p_Row) const
{
    // Find source row and set formatter for the row in collapsed state (so that DEL prefixes displays)
    ASSERT(nullptr != m_ColSubItemPrimaryKey);
    if (nullptr != m_ColSubItemPrimaryKey)
    {
        const MultivalueRowManager& mvMgr = GetGrid().GetMultivalueManager();
        vector<pair<GridBackendRow*, map<GridBackendColumn*, vector<GridBackendRow*>>>>	implosionSetup;// source row vs. col + exploded rows
        mvMgr.GetImplosionSetup({ p_Row }, implosionSetup);
        ASSERT(implosionSetup.size() <= 1);
        if (implosionSetup.size() == 1)
        {
            const auto sourceRow = implosionSetup.front().first;
            auto& rows = implosionSetup.front().second[m_ColSubItemPrimaryKey];
            if (sourceRow == p_Row)
                CreateOrUpdateFormatter(sourceRow, 0);
            else
            {
                ptrdiff_t pos = std::find(rows.begin(), rows.end(), p_Row) - rows.begin();
                if (pos < static_cast<ptrdiff_t>(rows.size()))
                    CreateOrUpdateFormatter(sourceRow, pos + 1);// rows does not contain the source row
                else
                    ASSERT(false);// not found!
            }
        }
    }
}

void ISubItemDeletion::CreateOrUpdateFormatter(GridBackendRow* p_Row, size_t p_Index) const
{
    for (auto sisterCol : m_ColSubItemElder->GetMultiValueExplosionSisters())
    {
        if (sisterCol->GetCellFormatter() == nullptr)
        {
            auto compositeFormatter = std::make_shared<CompositeSubElementFormatter>();
            auto prefixFormatter = CreatePrefixFormatter();

            prefixFormatter->AddElement(&p_Row->GetField(sisterCol), p_Index);
            compositeFormatter->AddFormatter(std::move(prefixFormatter), g_FormatterId);
            sisterCol->SetCellFormatter(compositeFormatter);
        }
        else
        {
            // A formatter already exists in that column
            CompositeSubElementFormatter* existingCompositeFormatter = dynamic_cast<CompositeSubElementFormatter*>(sisterCol->GetCellFormatter().get());
            ASSERT(nullptr != existingCompositeFormatter);
            if (nullptr != existingCompositeFormatter)
            {
                // Make sure a formatter for deletions exists
                if (!existingCompositeFormatter->HasFormatter(g_FormatterId))
                    existingCompositeFormatter->AddFormatter(std::move(CreatePrefixFormatter()), g_FormatterId);

                // Add the new element to that formatter
                existingCompositeFormatter->AddElement(&p_Row->GetField(sisterCol), p_Index, g_FormatterId);
            }
        }
    }
}

bool ISubItemDeletion::Refresh(GridBackendRow* p_Row, const std::map<SubItemKey, HttpResultWithError>& p_Errors)
{
    bool changed = false;
    
    // Find and save the corresponding error (if there is one)
    const auto ittErr = p_Errors.find(GetKey());
    if (ittErr != p_Errors.end())
    {
        //changed = true;
        const auto& httpResultError = ittErr->second;
        if (boost::none != httpResultError.m_Error)
        {
            changed = true;
            SetError(httpResultError.m_Error);
            m_State = Modification::State::RemoteError;
        } // Remove status icon if deleted successfully and collapsed
        else if (IsCollapsedRow(p_Row)) 
        {
            GetGrid().OnRowNothingToSave(p_Row, false);
            //m_State = State::RemoteHasNewValue;
        }
    }

    return changed;
}

void ISubItemDeletion::SetState(Modification::State p_State)
{
	m_State = p_State;
}

void ISubItemDeletion::AddFormatterForCollapsedRow(GridBackendRow* p_Row) const
{
    std::vector<PooledString>* pkIds = p_Row->GetField(m_ColSubItemPrimaryKey).GetValuesMulti<PooledString>();

    const wstring pkVal = getMostSpecificSubItemPk();

    if (nullptr != pkIds)
    {
        bool found = false;
        for (size_t i = 0; i < pkIds->size(); ++i)
        {
            if (pkIds->operator[](i) == pkVal && ShouldCreateFormatter(p_Row, i))
            {
                found = true;
                CreateOrUpdateFormatter(p_Row, i);
            }
        }
        ASSERT(found);
    }
}
