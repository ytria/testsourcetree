#include "DelegateUser.h"

RTTR_REGISTRATION
{
    using namespace rttr;
registration::class_<Ex::DelegateUser>("DelegateUser") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("DelegateUser"))))
.constructor()(policy::ctor::as_object);
}

Ex::DelegateUser::DelegateUser(const Ex::UserId& p_UserId)
    : m_UserId(p_UserId)
{
}
