#include "MsGraphHttpRequestLogger.h"
#include "LoggerService.h"
#include "SessionIdsEmitter.h"
#include "SessionTypes.h"

MsGraphHttpRequestLogger::MsGraphHttpRequestLogger(Sapio365Session::SessionType p_SessionType, const SessionIdentifier& p_Identifier)
	: m_SessionType(p_SessionType)
{
	if (p_Identifier.m_SessionType.empty())
	{
		m_SessionId = _YTEXT(":---");
	}
	else
	{
		m_SessionId = SessionIdsEmitter(p_Identifier).GetId();
		if (p_Identifier.m_SessionType == SessionTypes::g_ElevatedSession || p_Identifier.m_SessionType == SessionTypes::g_Role)
		{
			if (p_SessionType == Sapio365Session::USER_SESSION)
				m_SessionId += _YTEXT(":USR");
			else if (p_SessionType == Sapio365Session::APP_SESSION)
				m_SessionId += _YTEXT(":APP");
			else
			{
				ASSERT(false);
				m_SessionId = _YTEXT(":???");
			}
		}
	}
}

void MsGraphHttpRequestLogger::LogRequest(const wstring& p_Uri, const wstring& p_Method, const WebPayload& p_Payload, HWND p_Originator)
{
	wostringstream oss;
	oss << _YTEXT("[[") << m_SessionId << _YTEXT("]] - ");
	oss << p_Method << _YTEXT(" ") << p_Uri;
	if (!p_Payload.IsEmpty())
	{
		oss << _YTEXT(" Content-Type: ") << p_Payload.GetContentType();
		oss << std::endl << p_Payload.GetDump();
	}
	LoggerService::Debug(oss.str(), p_Originator);
}

void MsGraphHttpRequestLogger::LogResponse(const wstring& p_Uri, web::http::status_code p_StatusCode, const wstring& p_Method, HWND p_Originator)
{
	LoggerService::Debug(_YTEXTFORMAT(L"[[%s]] - HTTP %s for %s %s", m_SessionId.c_str(), std::to_wstring(p_StatusCode).c_str(), p_Method.c_str(), p_Uri.c_str()) , p_Originator);
}

shared_ptr<IHttpRequestLogger> MsGraphHttpRequestLogger::Clone() const
{
	return std::make_shared<MsGraphHttpRequestLogger>(*this);
}

Sapio365Session::SessionType MsGraphHttpRequestLogger::GetSessionType() const
{
	return m_SessionType;
}
