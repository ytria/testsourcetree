#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "CalculatedColumn.h"

class CalculatedColumnDeserializer : public JsonObjectDeserializer, public Encapsulate<CalculatedColumn>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

