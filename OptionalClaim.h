#pragma once

class OptionalClaim
{
public:
	boost::YOpt<vector<PooledString>> m_AdditionalProperties;
	boost::YOpt<bool> m_Essential;
	boost::YOpt<PooledString> m_Name;
	boost::YOpt<PooledString> m_Source;
};

