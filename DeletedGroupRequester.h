#pragma once

#include "IRequester.h"
#include "GroupRequester.h"
#include "IRequestLogger.h"
#include "IPropertySetBuilder.h"

class BusinessGroup;
class GroupDeserializer;
class SingleRequestResult;

class DeletedGroupRequester : public GroupRequester
{
public:
	using GroupRequester::GroupRequester;
    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
};

