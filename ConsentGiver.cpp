#include "ConsentGiver.h"

#include "DlgBrowser.h"
#include "SessionTypes.h"
#include "TraceIntoFile.h"
#include "YCallbackMessage.h"
#include "YCodeJockMessageBox.h"

#include <cpprest/uri.h>

ConsentGiver& ConsentGiver::GetInstance()
{
	static ConsentGiver instance;
	return instance;
}

bool ConsentGiver::GetConsent(const web::uri& p_BrowserUri, const wstring& p_SessionType, CWnd* p_Parent, const std::function<void(ConsentResult)>& p_ConsentResultCallback, const wstring& p_ConsentDlgTitle)
{
	bool requestSuccessfullyStarted = false;

	wstring redirectUri;

	{
		auto query = web::uri::split_query(p_BrowserUri.query());
		if (query.end() != query.find(_YTEXT("redirect_uri")))
			redirectUri = query.at(_YTEXT("redirect_uri"));
	}
	ASSERT(!redirectUri.empty());

	m_Parent = p_Parent;
	try
	{
		m_ConsentListener = std::make_shared<MSGraphConsentListener>(redirectUri);

		TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("GetConsent"), _YDUMPFORMAT(L"Listening to: %s", redirectUri.c_str()));
		m_ConsentListener->Listen().ThenByTask([this, parent = m_Parent, p_SessionType, p_ConsentResultCallback, p_BrowserUri, p_ConsentDlgTitle](pplx::task<ConsentResult> p_PreviousTask)
			{
				try
				{
					auto consentResult = p_PreviousTask.get();

					YCallbackMessage::DoSend([this]() {
						m_AdminConsentBrowser->PostMessage(WM_COMMAND, IDYES, 0);
						});

					m_ConsentListener.reset();

					p_ConsentResultCallback(consentResult);
				}
				catch (pplx::task_canceled&)
				{
					// Will happen if dialog gets closed as the listener shared_ptr will get deleted, which will trigger cancellation of the task
					TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("GetConsent"), _YDUMP("Canceled by user."));

					if (p_SessionType == SessionTypes::g_UltraAdmin || p_SessionType == SessionTypes::g_ElevatedSession)
					{
						YCallbackMessage::DoPost([=]() {
							YCodeJockMessageBox dlg(nullptr,
								DlgMessageBox::eIcon_ExclamationWarning,
								_T("Consent Canceled"),
								_T("We see that the consent process was unable to complete successfully.\r\nDo you want to try again?"),
								_YTEXT(""),
								{
									{ IDOK, _T("Try again") },
									{ IDCANCEL, _T("Skip") }
								});
							if (dlg.DoModal() == IDOK)
								GetConsent(p_BrowserUri, p_SessionType, parent, p_ConsentResultCallback, p_ConsentDlgTitle);
							});
					}
				}
				catch (const std::exception & e)
				{
					TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("GetConsent"), _YDUMPFORMAT(L"error: %s", Str::convertFromUTF8(e.what()).c_str()));
				}
			});

		requestSuccessfullyStarted = true;

		YCallbackMessage::DoPost([p_ConsentDlgTitle, p_BrowserUri, this]() {
			openMsGraphAdminConsentBrowser(p_BrowserUri, p_ConsentDlgTitle);
			});
	}
	catch (const std::exception & e)
	{
		{
			TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("GetConsent"), _YDUMPFORMAT(L"error: %s", Str::convertFromUTF8(e.what()).c_str()));
			// FIXME: We should use the logger service.
			// We need to rework logger channels to allow trace-only logging.
			//LoggerService::Debug(_YDUMPFORMAT(L"GetConsent error: %s", Str::convertFromUTF8(e.what()).c_str()));
		}
		YCodeJockMessageBox msgBox(m_Parent,
			DlgMessageBox::eIcon::eIcon_Error,
			YtriaTranslate::Do(Office365AdminApp_GetConsent_4, _YLOC("Consent")).c_str(),
			YtriaTranslate::Do(Office365AdminApp_GetConsent_5, _YLOC("Consent was not given. Please check information and resubmit.")).c_str(),
			YtriaTranslate::Do(Office365AdminApp_GetConsent_7, _YLOC("Error message: %1"), Str::convertFromUTF8(e.what()).c_str()),
			{ { IDOK, YtriaTranslate::Do(Office365AdminApp_GetConsent_6, _YLOC("OK")).c_str() } });
		msgBox.DoModal();
	}

	return requestSuccessfullyStarted;
}

void ConsentGiver::openMsGraphAdminConsentBrowser(const web::uri_builder& p_Uri, const wstring& p_DlgTitle)
{
	ASSERT(nullptr != m_Parent);

	const bool isAdministratorConsentURI = [p_Uri]()
	{
		bool result = false;
		auto path = web::uri::split_path(p_Uri.path());
		auto query = web::uri::split_query(p_Uri.query());
		try
		{
			result = path.back() == _YTEXT("adminconsent") || query.at(_YTEXT("prompt")) == _YTEXT("admin_consent");
		}
		catch (const std::out_of_range&)
		{
			result = false;
		}
		ASSERT(result || query.end() != query.find(_YTEXT("prompt")) && query.at(_YTEXT("prompt")) == _YTEXT("consent"));
		return result;
	}();

	ASSERT(!m_AdminConsentBrowser);

	wstring dlgTitle;
	if (p_DlgTitle.empty())
		dlgTitle = isAdministratorConsentURI ? YtriaTranslate::Do(MainFrame_OnOpenMsGraphAdminConsentBrowser_1, _YLOC("Administrator consent for sapio365")).c_str() : YtriaTranslate::Do(MainFrame_OnOpenMsGraphAdminConsentBrowser_2, _YLOC("User consent for sapio365")).c_str();
	else
		dlgTitle = p_DlgTitle;
	m_AdminConsentBrowser.reset(new DlgBrowser(p_Uri.to_string(), dlgTitle, m_Parent));

	if (IDOK != m_AdminConsentBrowser->DoModal())
		m_ConsentListener.reset();

	m_AdminConsentBrowser.reset();
}
