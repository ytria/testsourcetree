#include "EducationSchoolsListRequester.h"

#include "EducationSchool.h"
#include "EducationSchoolDeserializer.h"
#include "LoggerService.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "BasicPageRequestLogger.h"
#include "MsGraphHttpRequestLogger.h"

EducationSchoolsListRequester::EducationSchoolsListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger)
	:m_Logger(p_Logger)
{
}

TaskWrapper<void> EducationSchoolsListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<EducationSchool, EducationSchoolDeserializer>>();

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("schools"));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	MsGraphPaginator paginator(uri.to_uri(), Util::CreateDefaultGraphPageRequester(m_Deserializer, httpLogger, p_TaskData.GetOriginator()), m_Logger);
	return paginator.Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

const vector<EducationSchool>& EducationSchoolsListRequester::GetData() const
{
	return m_Deserializer->GetData();
}
