#pragma once

#include "BusinessDirectoryRole.h"
#include "BusinessGroup.h"
#include "BusinessOrgContact.h"
#include "BusinessUser.h"
#include "DirectoryObject.h"

class BusinessDirectoryObjectVariant : public BusinessObject
{
public:
    BusinessDirectoryObjectVariant() = default;

	bool IsBusinessDirectoryRole() const;
	bool IsBusinessGroup() const;
	bool IsBusinessOrgContact() const;
	bool IsBusinessUser() const;

	const BusinessGroup&			AsBusinessGroup() const;
	const BusinessOrgContact&		AsBusinessOrgContact() const;
	const BusinessUser&				AsBusinessUser() const;
	const BusinessDirectoryRole&	AsBusinessDirectoryRole() const;

protected:
	boost::YOpt<BusinessDirectoryRole>	m_BusinessDirectoryRole;
	boost::YOpt<BusinessGroup>			m_BusinessGroup;
	boost::YOpt<BusinessOrgContact>		m_BusinessOrgContact;
	boost::YOpt<BusinessUser>			m_BusinessUser;

    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND

    friend class DirectoryObjectVariantDeserializer;
};

