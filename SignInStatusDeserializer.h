#pragma once
#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "SignInStatus.h"

class SignInStatusDeserializer : public JsonObjectDeserializer, public Encapsulate<SignInStatus>
{
public:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};

