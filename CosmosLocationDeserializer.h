#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "CosmosLocation.h"

namespace Azure
{
	class CosmosLocationDeserializer : public JsonObjectDeserializer, public Encapsulate<Azure::CosmosLocation>
	{
	public:
		void DeserializeObject(const web::json::object& p_Object) override;
	};
}

