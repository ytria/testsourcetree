#include "TeamGuestSettingsSerializer.h"

#include "JsonSerializeUtil.h"

TeamGuestSettingsSerializer::TeamGuestSettingsSerializer(const TeamGuestSettings& p_Settings)
    : m_Settings(p_Settings)
{
}

void TeamGuestSettingsSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeBool(_YTEXT("allowCreateUpdateChannels"), m_Settings.GetAllowCreateUpdateChannels(), obj);
    JsonSerializeUtil::SerializeBool(_YTEXT("allowDeleteChannels"), m_Settings.GetAllowDeleteChannels(), obj);
}
