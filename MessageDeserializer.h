#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessMessage.h"

class MessageDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessMessage>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

