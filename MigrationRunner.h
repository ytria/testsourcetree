#pragma once

#include "IMigrationVersion.h"

class IMigrationsProvisioner;

class MigrationRunner
{
public:
	MigrationRunner(std::unique_ptr<IMigrationsProvisioner> p_Provisioner);
	void Run();

private:
	std::unique_ptr<IMigrationsProvisioner> m_Provisioner;
	
};

