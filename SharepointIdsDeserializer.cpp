#include "SharepointIdsDeserializer.h"

#include "JsonSerializeUtil.h"

void SharepointIdsDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("listId"), m_Data.ListId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("listItemId"), m_Data.ListItemId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("listItemUniqueId"), m_Data.ListItemUniqueId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("siteId"), m_Data.SiteId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("siteUrl"), m_Data.SiteUrl, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("webId"), m_Data.WebId, p_Object);
}
