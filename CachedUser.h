#pragma once

#include "CachedObject.h"
#include "RttrIncludes.h"

#include "User.h"

class BusinessUser;

class CachedUser : public CachedObject
{
public:
	CachedUser() = default;
	CachedUser(const User& user);
    CachedUser(const BusinessUser& p_User);

	PooledString GetDisplayName() const;
    PooledString GetUserPrincipalName() const;
	PooledString GetUserType() const;
	const boost::YOpt<YTimeDate>& GetDeletedDateTime() const;
	PooledString GetMail() const;
	bool IsEmailPresent() const;

private:
	void setDeletedDateTime(const boost::YOpt<PooledString>& p_DelDate);

    boost::YOpt<PooledString>	m_DisplayName;
    boost::YOpt<PooledString>	m_UserPrincipalName;
	boost::YOpt<PooledString>	m_UserType;
	boost::YOpt<YTimeDate>		m_DeletedDateTime; // For deleted users (Recycle Bin)
	boost::YOpt<PooledString>	m_Mail;

    // TODO: More fields

    RTTR_ENABLE(CachedObject)
    RTTR_REGISTRATION_FRIEND

    friend class CachedUserDeserializer;
};
