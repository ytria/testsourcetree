#include "OnPremiseUser.h"
#include "Sapio365Settings.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<OnPremiseUser>("OnPremiseUser") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("On-Premises User"))))
	.constructor()(policy::ctor::as_object);

	/*registration::class_<HybridUser>("HybridUser") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Hybrid User"))))
	.constructor()(policy::ctor::as_object);*/
}

OnPremiseUser::OnPremiseUser()
{
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetAuthenticationPolicy() const
{
	return m_AuthenticationPolicy;
}

void OnPremiseUser::SetAuthenticationPolicy(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_AuthenticationPolicy = p_Val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetAuthenticationPolicySilo() const
{
	return m_AuthenticationPolicySilo;
}

void OnPremiseUser::SetAuthenticationPolicySilo(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_AuthenticationPolicySilo = p_Val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetCertificates() const
{
	return m_Certificates;
}

void OnPremiseUser::SetCertificates(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_Certificates = p_Val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetCompoundIdentitySupported() const
{
	return m_CompoundIdentitySupported;
}

void OnPremiseUser::SetCompoundIdentitySupported(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_CompoundIdentitySupported = p_Val;
}

const boost::YOpt<vector<YTimeDate>>& OnPremiseUser::GetdSCorePropagationData() const
{
	return m_dSCorePropagationData;
}

void OnPremiseUser::SetdSCorePropagationData(const boost::YOpt<vector<YTimeDate>>& p_Val)
{
	m_dSCorePropagationData = p_Val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetKerberosEncryptionType() const
{
	return m_KerberosEncryptionType;
}

void OnPremiseUser::SetKerberosEncryptionType(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_KerberosEncryptionType = p_Val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetLogonHours() const
{
	return m_LogonHours;
}

void OnPremiseUser::SetLogonHours(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_LogonHours = p_Val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetMemberOf() const
{
	return m_MemberOf;
}

void OnPremiseUser::SetMemberOf(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_MemberOf = p_Val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetPrincipalsAllowedToDelegateToAccount() const
{
	return m_PrincipalsAllowedToDelegateToAccount;
}

void OnPremiseUser::SetPrincipalsAllowedToDelegateToAccount(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_PrincipalsAllowedToDelegateToAccount = p_Val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetServicePrincipalNames() const
{
	return m_ServicePrincipalNames;
}

void OnPremiseUser::SetServicePrincipalNames(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_ServicePrincipalNames = p_Val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetSIDHistory() const
{
	return m_SIDHistory;
}

void OnPremiseUser::SetSIDHistory(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_SIDHistory = p_Val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetUserCertificate() const
{
	return m_UserCertificate;
}

void OnPremiseUser::SetUserCertificate(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_UserCertificate = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetAccountExpires() const
{
	return m_AccountExpires;
}

void OnPremiseUser::SetAccountExpires(const boost::YOpt<YTimeDate>& p_Val)
{
	m_AccountExpires = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetBadPasswordTime() const
{
	return m_BadPasswordTime;
}

void OnPremiseUser::SetBadPasswordTime(const boost::YOpt<YTimeDate>& p_Val)
{
	m_BadPasswordTime = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetLastLogoff() const
{
	return m_LastLogoff;
}

void OnPremiseUser::SetLastLogoff(const boost::YOpt<YTimeDate>& p_Val)
{
	m_LastLogoff = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetLastLogon() const
{
	return m_LastLogon;
}

void OnPremiseUser::SetLastLogon(const boost::YOpt<YTimeDate>& p_Val)
{
	m_LastLogon = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetAccountLockoutTime() const
{
	return m_AccountLockoutTime;
}

void OnPremiseUser::SetAccountLockoutTime(const boost::YOpt<YTimeDate>& p_Val)
{
	m_AccountLockoutTime = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetCreated() const
{
	return m_Created;
}

void OnPremiseUser::SetCreated(const boost::YOpt<YTimeDate>& p_Val)
{
	m_Created = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetPasswordLastSet() const
{
	return m_PasswordLastSet;
}

void OnPremiseUser::SetPasswordLastSet(const boost::YOpt<YTimeDate>& p_Val)
{
	m_PasswordLastSet = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetWhenChanged() const
{
	return m_WhenChanged;
}

void OnPremiseUser::SetWhenChanged(const boost::YOpt<YTimeDate>& p_Val)
{
	m_WhenChanged = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetWhenCreated() const
{
	return m_WhenCreated;
}

void OnPremiseUser::SetWhenCreated(const boost::YOpt<YTimeDate>& p_Val)
{
	m_WhenCreated = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetLastBadPasswordAttempt() const
{
	return m_LastBadPasswordAttempt;
}

void OnPremiseUser::SetLastBadPasswordAttempt(const boost::YOpt<YTimeDate>& p_Val)
{
	m_LastBadPasswordAttempt = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetLastLogonDate() const
{
	return m_LastLogonDate;
}

void OnPremiseUser::SetLastLogonDate(const boost::YOpt<YTimeDate>& p_Val)
{
	m_LastLogonDate = p_Val;
}

const boost::YOpt<YTimeDate>& OnPremiseUser::GetModified() const
{
	return m_Modified;
}

void OnPremiseUser::SetModified(const boost::YOpt<YTimeDate>& p_Val)
{
	m_Modified = p_Val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetCanonicalName() const
{
	return m_CanonicalName;
}

void OnPremiseUser::SetCanonicalName(const boost::YOpt<PooledString>& val)
{
	m_CanonicalName = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetCity() const
{
	return m_City;
}

void OnPremiseUser::SetCity(const boost::YOpt<PooledString>& val)
{
	m_City = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetCN() const
{
	return m_CN;
}

void OnPremiseUser::SetCN(const boost::YOpt<PooledString>& val)
{
	m_CN = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetCompany() const
{
	return m_Company;
}

void OnPremiseUser::SetCompany(const boost::YOpt<PooledString>& val)
{
	m_Company = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetCountry() const
{
	return m_Country;
}

void OnPremiseUser::SetCountry(const boost::YOpt<PooledString>& val)
{
	m_Country = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetDepartment() const
{
	return m_Department;
}

void OnPremiseUser::SetDepartment(const boost::YOpt<PooledString>& val)
{
	m_Department = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetDescription() const
{
	return m_Description;
}

void OnPremiseUser::SetDescription(const boost::YOpt<PooledString>& val)
{
	m_Description = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetDisplayName() const
{
	return m_DisplayName;
}

void OnPremiseUser::SetDisplayName(const boost::YOpt<PooledString>& val)
{
	m_DisplayName = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetDistinguishedName() const
{
	return m_DistinguishedName;
}

void OnPremiseUser::SetDistinguishedName(const boost::YOpt<PooledString>& val)
{
	m_DistinguishedName = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetDivision() const
{
	return m_Division;
}

void OnPremiseUser::SetDivision(const boost::YOpt<PooledString>& val)
{
	m_Division = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetEmailAddress() const
{
	return m_EmailAddress;
}

void OnPremiseUser::SetEmailAddress(const boost::YOpt<PooledString>& val)
{
	m_EmailAddress = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetEmployeeID() const
{
	return m_EmployeeID;
}

void OnPremiseUser::SetEmployeeID(const boost::YOpt<PooledString>& val)
{
	m_EmployeeID = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetEmployeeNumber() const
{
	return m_EmployeeNumber;
}

void OnPremiseUser::SetEmployeeNumber(const boost::YOpt<PooledString>& val)
{
	m_EmployeeNumber = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetFax() const
{
	return m_Fax;
}

void OnPremiseUser::SetFax(const boost::YOpt<PooledString>& val)
{
	m_Fax = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetGivenName() const
{
	return m_GivenName;
}

void OnPremiseUser::SetGivenName(const boost::YOpt<PooledString>& val)
{
	m_GivenName = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetHomeDirectory() const
{
	return m_HomeDirectory;
}

void OnPremiseUser::SetHomeDirectory(const boost::YOpt<PooledString>& val)
{
	m_HomeDirectory = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetHomeDrive() const
{
	return m_HomeDrive;
}

void OnPremiseUser::SetHomeDrive(const boost::YOpt<PooledString>& val)
{
	m_HomeDrive = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetHomePage() const
{
	return m_HomePage;
}

void OnPremiseUser::SetHomePage(const boost::YOpt<PooledString>& val)
{
	m_HomePage = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetHomePhone() const
{
	return m_HomePhone;
}

void OnPremiseUser::SetHomePhone(const boost::YOpt<PooledString>& val)
{
	m_HomePhone = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetInitials() const
{
	return m_Initials;
}

void OnPremiseUser::SetInitials(const boost::YOpt<PooledString>& val)
{
	m_Initials = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetLastKnownParent() const
{
	return m_LastKnownParent;
}

void OnPremiseUser::SetLastKnownParent(const boost::YOpt<PooledString>& val)
{
	m_LastKnownParent = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetLogonWorkstations() const
{
	return m_LogonWorkstations;
}

void OnPremiseUser::SetLogonWorkstations(const boost::YOpt<PooledString>& val)
{
	m_LogonWorkstations = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetManager() const
{
	return m_Manager;
}

void OnPremiseUser::SetManager(const boost::YOpt<PooledString>& val)
{
	m_Manager = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetMobilePhone() const
{
	return m_MobilePhone;
}

void OnPremiseUser::SetMobilePhone(const boost::YOpt<PooledString>& val)
{
	m_MobilePhone = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetName() const
{
	return m_Name;
}

void OnPremiseUser::SetName(const boost::YOpt<PooledString>& val)
{
	m_Name = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetTSecurityDescriptor() const
{
	return m_nTSecurityDescriptor;
}

void OnPremiseUser::SetNTSecurityDescriptor(const boost::YOpt<PooledString>& val)
{
	m_nTSecurityDescriptor = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetObjectGUID() const
{
	return m_ObjectGUID;
}

void OnPremiseUser::SetObjectGUID(const boost::YOpt<PooledString>& val)
{
	m_ObjectGUID = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetObjectCategory() const
{
	return m_ObjectCategory;
}

void OnPremiseUser::SetObjectCategory(const boost::YOpt<PooledString>& val)
{
	m_ObjectCategory = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetObjectClass() const
{
	return m_ObjectClass;
}

void OnPremiseUser::SetObjectClass(const boost::YOpt<PooledString>& val)
{
	m_ObjectClass = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetObjectSid() const
{
	return m_ObjectSid;
}

void OnPremiseUser::SetObjectSid(const boost::YOpt<PooledString>& val)
{
	m_ObjectSid = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetOffice() const
{
	return m_Office;
}

void OnPremiseUser::SetOffice(const boost::YOpt<PooledString>& val)
{
	m_Office = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetOfficePhone() const
{
	return m_OfficePhone;
}

void OnPremiseUser::SetOfficePhone(const boost::YOpt<PooledString>& val)
{
	m_OfficePhone = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetOrganization() const
{
	return m_Organization;
}

void OnPremiseUser::SetOrganization(const boost::YOpt<PooledString>& val)
{
	m_Organization = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetOtherName() const
{
	return m_OtherName;
}

void OnPremiseUser::SetOtherName(const boost::YOpt<PooledString>& val)
{
	m_OtherName = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetPOBox() const
{
	return m_POBox;
}

void OnPremiseUser::SetPOBox(const boost::YOpt<PooledString>& val)
{
	m_POBox = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetPostalCode() const
{
	return m_PostalCode;
}

void OnPremiseUser::SetPostalCode(const boost::YOpt<PooledString>& val)
{
	m_PostalCode = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetPrimaryGroup() const
{
	return m_PrimaryGroup;
}

void OnPremiseUser::SetPrimaryGroup(const boost::YOpt<PooledString>& val)
{
	m_PrimaryGroup = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetProfilePath() const
{
	return m_ProfilePath;
}

void OnPremiseUser::SetProfilePath(const boost::YOpt<PooledString>& val)
{
	m_ProfilePath = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetSamAccountName() const
{
	return m_SamAccountName;
}

void OnPremiseUser::SetSamAccountName(const boost::YOpt<PooledString>& val)
{
	m_SamAccountName = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetSID() const
{
	return m_SID;
}

void OnPremiseUser::SetSID(const boost::YOpt<PooledString>& val)
{
	m_SID = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetScriptPath() const
{
	return m_ScriptPath;
}

void OnPremiseUser::SetScriptPath(const boost::YOpt<PooledString>& val)
{
	m_ScriptPath = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetState() const
{
	return m_State;
}

void OnPremiseUser::SetState(const boost::YOpt<PooledString>& val)
{
	m_State = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetStreetAddress() const
{
	return m_StreetAddress;
}

void OnPremiseUser::SetStreetAddress(const boost::YOpt<PooledString>& val)
{
	m_StreetAddress = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetSurname() const
{
	return m_Surname;
}

void OnPremiseUser::SetSurname(const boost::YOpt<PooledString>& val)
{
	m_Surname = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetTitle() const
{
	return m_Title;
}

void OnPremiseUser::SetTitle(const boost::YOpt<PooledString>& val)
{
	m_Title = val;
}

const boost::YOpt<PooledString>& OnPremiseUser::GetUserPrincipalName() const
{
	return m_UserPrincipalName;
}

void OnPremiseUser::SetUserPrincipalName(const boost::YOpt<PooledString>& val)
{
	m_UserPrincipalName = val;
}

const boost::YOpt<int64_t>& OnPremiseUser::GttUSNChanged() const
{
	return m_USNChanged;
}

void OnPremiseUser::SetUSNChanged(const boost::YOpt<int64_t>& val)
{
	m_USNChanged = val;
}

const boost::YOpt<int64_t>& OnPremiseUser::GetUSNCreated() const
{
	return m_USNCreated;
}

void OnPremiseUser::SetUSNCreated(const boost::YOpt<int64_t>& val)
{
	m_USNCreated = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetAdminCount() const
{
	return m_AdminCount;
}

void OnPremiseUser::SetAdminCount(const boost::YOpt<int32_t>& val)
{
	m_AdminCount = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetBadLogonCount() const
{
	return m_BadLogonCount;
}

void OnPremiseUser::SetBadLogonCount(const boost::YOpt<int32_t>& val)
{
	m_BadLogonCount = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetBadPwdCount() const
{
	return m_BadPwdCount;
}

void OnPremiseUser::SetBadPwdCount(const boost::YOpt<int32_t>& val)
{
	m_BadPwdCount = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetCodePage() const
{
	return m_CodePage;
}

void OnPremiseUser::SetCodePage(const boost::YOpt<int32_t>& val)
{
	m_CodePage = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetCountryCode() const
{
	return m_CountryCode;
}

void OnPremiseUser::SetCountryCode(const boost::YOpt<int32_t>& val)
{
	m_CountryCode = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetInstanceType() const
{
	return m_InstanceType;
}

void OnPremiseUser::SetInstanceType(const boost::YOpt<int32_t>& val)
{
	m_InstanceType = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetLogonCount() const
{
	return m_LogonCount;
}

void OnPremiseUser::SetLogonCount(const boost::YOpt<int32_t>& val)
{
	m_LogonCount = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetMSDSUserAccountControlComputed() const
{
	return m_MSDSUserAccountControlComputed;
}

void OnPremiseUser::SetMSDSUserAccountControlComputed(const boost::YOpt<int32_t>& val)
{
	m_MSDSUserAccountControlComputed = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetPrimaryGroupId() const
{
	return m_PrimaryGroupId;
}

void OnPremiseUser::SetPrimaryGroupId(const boost::YOpt<int32_t>& val)
{
	m_PrimaryGroupId = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetSAMAccountType() const
{
	return m_SAMAccountType;
}

void OnPremiseUser::SetSAMAccountType(const boost::YOpt<int32_t>& val)
{
	m_SAMAccountType = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetSDRightsEffective() const
{
	return m_SDRightsEffective;
}

void OnPremiseUser::SetSDRightsEffective(const boost::YOpt<int32_t>& val)
{
	m_SDRightsEffective = val;
}

const boost::YOpt<int32_t>& OnPremiseUser::GetUserAccountControl() const
{
	return m_UserAccountControl;
}

void OnPremiseUser::SetUserAccountControl(const boost::YOpt<int32_t>& val)
{
	m_UserAccountControl = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetAccountNotDelegated() const
{
	return m_AccountNotDelegated;
}

void OnPremiseUser::SetAccountNotDelegated(const boost::YOpt<bool>& val)
{
	m_AccountNotDelegated = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetAllowReversiblePasswordEncryption() const
{
	return m_AllowReversiblePasswordEncryption;
}

void OnPremiseUser::SetAllowReversiblePasswordEncryption(const boost::YOpt<bool>& val)
{
	m_AllowReversiblePasswordEncryption = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetCannotChangePassword() const
{
	return m_CannotChangePassword;
}

void OnPremiseUser::SetCannotChangePassword(const boost::YOpt<bool>& val)
{
	m_CannotChangePassword = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetDeleted() const
{
	return m_Deleted;
}

void OnPremiseUser::SetDeleted(const boost::YOpt<bool>& val)
{
	m_Deleted = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetDoesNotRequirePreAuth() const
{
	return m_DoesNotRequirePreAuth;
}

void OnPremiseUser::SetDoesNotRequirePreAuth(const boost::YOpt<bool>& val)
{
	m_DoesNotRequirePreAuth = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetEnabled() const
{
	return m_Enabled;
}

void OnPremiseUser::SetEnabled(const boost::YOpt<bool>& val)
{
	m_Enabled = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetHomedirRequired() const
{
	return m_HomedirRequired;
}

void OnPremiseUser::SetHomedirRequired(const boost::YOpt<bool>& val)
{
	m_HomedirRequired = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetIsCriticalSystemObject() const
{
	return m_IsCriticalSystemObject;
}

void OnPremiseUser::SetIsCriticalSystemObject(const boost::YOpt<bool>& val)
{
	m_IsCriticalSystemObject = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetIsDeleted() const
{
	return m_IsDeleted;
}

void OnPremiseUser::SetIsDeleted(const boost::YOpt<bool>& val)
{
	m_IsDeleted = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetLockedOut() const
{
	return m_LockedOut;
}

void OnPremiseUser::SetLockedOut(const boost::YOpt<bool>& val)
{
	m_LockedOut = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetMNSLogonAccount() const
{
	return m_MNSLogonAccount;
}

void OnPremiseUser::SetMNSLogonAccount(const boost::YOpt<bool>& val)
{
	m_MNSLogonAccount = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetPasswordExpired() const
{
	return m_PasswordExpired;
}

void OnPremiseUser::SetPasswordExpired(const boost::YOpt<bool>& val)
{
	m_PasswordExpired = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetPasswordNeverExpires() const
{
	return m_PasswordNeverExpires;
}

void OnPremiseUser::SetPasswordNeverExpires(const boost::YOpt<bool>& val)
{
	m_PasswordNeverExpires = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetPasswordNotRequired() const
{
	return m_PasswordNotRequired;
}

void OnPremiseUser::SetPasswordNotRequired(const boost::YOpt<bool>& val)
{
	m_PasswordNotRequired = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetProtectedFromAccidentalDeletion() const
{
	return m_ProtectedFromAccidentalDeletion;
}

void OnPremiseUser::SetProtectedFromAccidentalDeletion(const boost::YOpt<bool>& val)
{
	m_ProtectedFromAccidentalDeletion = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetSmartcardLogonRequired() const
{
	return m_SmartcardLogonRequired;
}

void OnPremiseUser::SetSmartcardLogonRequired(const boost::YOpt<bool>& val)
{
	m_SmartcardLogonRequired = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetTrustedForDelegation() const
{
	return m_TrustedForDelegation;
}

void OnPremiseUser::SetTrustedForDelegation(const boost::YOpt<bool>& val)
{
	m_TrustedForDelegation = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetTrustedToAuthForDelegation() const
{
	return m_TrustedToAuthForDelegation;
}

void OnPremiseUser::SetTrustedToAuthForDelegation(const boost::YOpt<bool>& val)
{
	m_TrustedToAuthForDelegation = val;
}

const boost::YOpt<bool>& OnPremiseUser::GetUseDESKeyOnly() const
{
	return m_UseDESKeyOnly;
}

void OnPremiseUser::SetUseDESKeyOnly(const boost::YOpt<bool>& val)
{
	m_UseDESKeyOnly = val;
}

const boost::YOpt<vector<uint8_t>>& OnPremiseUser::GetMsDsConsistencyGuid() const
{
	return m_MsDsConsistencyGuid;
}

wstring OnPremiseUser::GetMsDsConsistencyGuidAsString() const
{
	wstring conGui;
	if (GetMsDsConsistencyGuid() && !GetMsDsConsistencyGuid()->empty())
	{
		std::wostringstream stream;
		stream << std::hex;

		for (const auto& c : *GetMsDsConsistencyGuid())
			stream << std::setfill(L'0') << std::setw(2) << c << L' ';
		conGui = stream.str();
	}
	return conGui;
}

void OnPremiseUser::SetMsDsConsistencyGuid(const boost::YOpt<vector<uint8_t>>& val)
{
	m_MsDsConsistencyGuid = val;
}

const boost::YOpt<vector<PooledString>>& OnPremiseUser::GetProxyAddresses() const
{
	return m_ProxyAddresses;
}

void OnPremiseUser::SetProxyAddresses(const boost::YOpt<vector<PooledString>>& val)
{
	m_ProxyAddresses = val;
}

wstring OnPremiseUser::ToImmutableID(const OnPremiseUser& p_User, bool p_UseConsistencyGuid)
{
	wstring immutableId;

	if (p_User.GetMsDsConsistencyGuid() && !p_User.GetMsDsConsistencyGuid()->empty() && p_UseConsistencyGuid)
	{
		immutableId = utility::conversions::to_base64(*p_User.GetMsDsConsistencyGuid());
	}
	else if (p_User.GetObjectGUID() && !p_User.GetObjectGUID()->IsEmpty())
	{
		immutableId = BusinessObject::ObjectGuidToImmutableID(*p_User.GetObjectGUID());
	}

	return immutableId;
}
