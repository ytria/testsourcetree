#include "DlgBusinessDriveItemRename.h"

#include "AutomationNames.h"
#include "BusinessDriveItem.h"


DlgBusinessDriveItemRename::DlgBusinessDriveItemRename(const BusinessDriveItem& p_DriveItem, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
    : DlgFormsHTML(p_Action, p_Parent, g_ActionNameSelectedRename)
	, m_DriveItem(p_DriveItem)
{

}

void DlgBusinessDriveItemRename::generateJSONScriptData()
{
    initMain(_YTEXT("DlgBusinessDriveItemRename"));

    getGenerator().Add(StringEditor(_YTEXT("name")
		, YtriaTranslate::Do(DlgBusinessDriveItemRename_generateJSONScriptData_1, _YLOC("Name")).c_str()
		, EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED
		, m_DriveItem.GetName() ? *m_DriveItem.GetName() : _YTEXT("")));

    getGenerator().addApplyButton(LocalizedStrings::g_ApplyButtonText);
    getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
    getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);
}

bool DlgBusinessDriveItemRename::processPostedData(const IPostedDataTarget::PostedData& data)
{
    ASSERT(data.size() == 1);
    if (data.size() == 1)
        m_NewName = data.begin()->second;
    return true;
}

const wstring& DlgBusinessDriveItemRename::GetNewName() const
{
    return m_NewName;
}
