#include "SiteCollectionSerializer.h"

#include "JsonSerializeUtil.h"

SiteCollectionSerializer::SiteCollectionSerializer(const SiteCollection& p_SiteColl)
    : m_SiteColl(p_SiteColl)
{
}

void SiteCollectionSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("hostname"), m_SiteColl.Hostname, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("dataLocationCode"), m_SiteColl.DataLocationCode, obj);
}
