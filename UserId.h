#pragma once

namespace Ex
{
class UserId
{
public:
    boost::YOpt<PooledString> m_SID;
    boost::YOpt<PooledString> m_PrimarySmtpAddress;
    boost::YOpt<PooledString> m_DisplayName;
    boost::YOpt<PooledString> m_DistinguishedUser;
    boost::YOpt<PooledString> m_ExternalUserIdentity;
};
}
