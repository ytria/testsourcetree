#include "LicenseDetailDeserializer.h"

#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "ServicePlanInfoDeserializer.h"

void LicenseDetailDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
    
    {
        ListDeserializer<ServicePlanInfo, ServicePlanInfoDeserializer> spiDeserializer;
        if (JsonSerializeUtil::DeserializeAny(spiDeserializer, _YTEXT("servicePlans"), p_Object))
            m_Data.m_ServicePlans = std::move(spiDeserializer.GetData());
    }
    
    JsonSerializeUtil::DeserializeString(_YTEXT("skuId"), m_Data.m_SkuId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("skuPartNumber"), m_Data.m_SkuPartNumber, p_Object);
}
