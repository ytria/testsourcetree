#include "CommaSeparatedString.h"

CommaSeparatedString::CommaSeparatedString(const wstring& p_InitialStr)
	:m_Str(p_InitialStr)
{
}

void CommaSeparatedString::Add(const wstring& p_Str)
{
	if (!m_Str.empty())
		m_Str.push_back(L',');
	m_Str += p_Str;
}

const wstring& CommaSeparatedString::Get() const
{
	return m_Str;
}
