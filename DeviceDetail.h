#pragma once

class DeviceDetail
{
public:
	boost::YOpt<PooledString> m_Browser;
	boost::YOpt<PooledString> m_DeviceId;
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<bool>		  m_IsCompliant;
	boost::YOpt<bool>		  m_IsManaged;
	boost::YOpt<PooledString> m_OperatingSystem;
	boost::YOpt<PooledString> m_TrustType;
};

