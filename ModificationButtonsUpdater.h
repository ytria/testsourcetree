#pragma once

class IButtonUpdater;
class O365Grid;

class ModificationButtonsUpdater
{
public:
	ModificationButtonsUpdater(std::unique_ptr<IButtonUpdater> p_EnableSaveAll,
		std::unique_ptr<IButtonUpdater> p_EnableSaveSelected,
		std::unique_ptr<IButtonUpdater> p_EnableRevertAll,
		std::unique_ptr<IButtonUpdater> p_EnableRevertSelected);

	void UpdateSaveAll(CCmdUI* pCmdUI) const;
	void UpdateSaveSelected(CCmdUI* pCmdUI) const;
	void UpdateRevertAll(CCmdUI* pCmdUI) const;
	void UpdateRevertSelected(CCmdUI* pCmdUI) const;

	void SetSaveAllEnableUpdater(std::unique_ptr<IButtonUpdater> p_EnableSaveAll);
	void SetSaveSelectedEnableUpdater(std::unique_ptr<IButtonUpdater> p_EnableSaveSelected);
	void SetRevertAllEnableUpdater(std::unique_ptr<IButtonUpdater> p_EnableRevertAll);
	void SetRevertSelectedEnableUpdater(std::unique_ptr<IButtonUpdater> p_EnableRevertSelected);
	void SetSaveAllEnableUpdater(nullptr_t) = delete;
	void SetSaveSelectedEnableUpdater(nullptr_t) = delete;
	void SetRevertAllEnableUpdater(nullptr_t) = delete;
	void SetRevertSelectedEnableUpdater(nullptr_t) = delete;

private:
	std::unique_ptr<IButtonUpdater> m_EnableSaveAll;
	std::unique_ptr<IButtonUpdater> m_EnableSaveSelected;

	std::unique_ptr<IButtonUpdater> m_EnableRevertAll;
	std::unique_ptr<IButtonUpdater> m_EnableRevertSelected;
};

