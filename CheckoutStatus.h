#pragma once

#include "SapioError.h"

class CheckoutStatus
{
public:
	void SetError(const boost::YOpt<SapioError>& p_Error);

	boost::YOpt<PooledString>	m_CheckoutUser;
	boost::YOpt<PooledString>	m_CheckinComment;
	boost::YOpt<PooledString>	m_ComplicanceTag;
	boost::YOpt<YTimeDate>		m_ComplicanceTagWrittenTime;
	boost::YOpt<SapioError>		m_Error;
};
