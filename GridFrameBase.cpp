#include "GridFrameBase.h"

#include "ActivityLogger.h"
#include "AutomationDataStructure.h"
#include "AutomationNames.h"
#include "BusinessObjectManager.h"
#include "BaseO365Grid.h"
#include "CacheGridColumnOrg.h"
#include "CacheGridGroupArea.h"
#include "DlgBrowser.h"
#include "DlgDataContext.h"
#include "DlgSpecialExtAdminAccess.h"
#include "GridUpdater.h"
#include "IGridRefresher.h"
#include "MainFrame.h"
#include "Messages.h"
#include "ModuleBase.h"
#include "MSGraphSession.h"
#include "OAuth2AuthenticatorBase.h"
#include "OAuth2Authenticators.h"
#include "Office365Admin.h"
#include "ResProduct.h"
#include "RibbonCmdUI.h"
#include "RibbonSystemMenuBuilders.h"
#include "SessionTypes.h"
#include "TimeUtil.h"
#include "WindowsListUpdater.h"
#include "YCallbackMessage.h"
#include "YMessageGrid.h"
#include "YtriaTranslate.h"

#include "Resources/resource.h"
#include "DlgAddItemsToGroups.h"
#include "RoleDelegationManager.h"
#include "PersistentCacheUtil.h"
#include "SessionsSqlEngine.h"
#include "DlgSpecialExtAdminUltraAdminJsonGenerator.h"
#include "AzureADOAuth2BrowserAuthenticator.h"
#include "AzureADCommonData.h"
#include "DlgBrowserOpener.h"
#include "MSGraphCommonData.h"
#include "GenericSaveAllButtonUpdater.h"
#include "GenericSaveSelectedButtonUpdater.h"
#include "GenericRevertAllButtonUpdater.h"
#include "GenericRevertSelectedButtonUpdater.h"

// In-ribbon search test (not really working yet)
//#include "DlgSearchRegexGeneral.h"

const XTPRibbonTabContextColor GridFrameBase::g_GridContextColor = xtpRibbonTabContextColorCyan;
const XTPRibbonTabContextColor GridFrameBase::g_GCVDContextColor = xtpRibbonTabContextColorOrange;

wstring	GridFrameBase::g_GridContextName;
wstring	GridFrameBase::g_GCVDContextName;

wstring	GridFrameBase::g_ViewGroup;
wstring	GridFrameBase::g_ActionsDataGroup;
wstring	GridFrameBase::g_ActionsApplyGroup;
wstring	GridFrameBase::g_ActionsRevertGroup;
wstring	GridFrameBase::g_ActionsEditGroup;
wstring	GridFrameBase::g_ActionsLinkUser;
//wstring	GridFrameBase::g_ActionsQuickCollapse;
wstring	GridFrameBase::g_ActionsLinkGroup;
wstring	GridFrameBase::g_ActionsLinkSite;
wstring	GridFrameBase::g_ActionsLinkSchools;
//wstring GridFrameBase::g_ActionsDriveGroup;
wstring GridFrameBase::g_ActionsLinkConversation;
wstring GridFrameBase::g_ActionsLinkList;
wstring GridFrameBase::g_ActionsLinkChannel;
wstring GridFrameBase::g_ActionsLinkMessage;
wstring GridFrameBase::g_ActionsLinkSignIn;

bool GridFrameBase::g_staticInit = false;

DWORD GridFrameBase::g_NoWindowsListUpdate = 1;

IMPLEMENT_DYNAMIC(GridFrameBase, CodeJockFrameBase)

namespace
{
	wstring dateToString(const YTimeDate& timedate)
	{
		CString dateStr;
		TimeUtil::GetInstance().ConvertDateToText(DateTimeFormat(), timedate, dateStr);
		return (LPCTSTR)dateStr;
	}
}

GridFrameBase::GridFrameBase(const LicenseContext& p_LicenseContext
							, const SessionIdentifier& p_SessionIdentifier
							, const PooledString& p_FrameTitle
							, ModuleBase& p_Module
							, HistoryMode::Mode p_HistoryMode
							, CFrameWnd* p_PreviousFrame
							, bool p_CanRefreshFromSql/* = false*/
							, std::shared_ptr<GridFrameLogger> p_CustomLogger/* = nullptr*/
							)
	: FrameBase<CodeJockFrameBase>(true, p_LicenseContext)
	, IHistoryFrame(this, p_HistoryMode, dynamic_cast<IHistoryFrame*>(p_PreviousFrame))
	, m_CustomTitle(p_FrameTitle)
	, m_CreationDate(YTimeDate::GetCurrentTimeDate())
	, m_CreationDateString(dateToString(m_CreationDate))
	, m_RefreshDate(m_CreationDate)
	, m_module(p_Module)
    , m_explodeCellsRibbonTab(nullptr)
	, m_infoRibbonTab(nullptr)
    , m_optionsRibbonTab(nullptr)
	, m_globalRibbonTab(nullptr)
	, m_gcvdRibbonTab(nullptr)
	, m_actionsRibbonTab(nullptr)
	, m_windowsRibbonTab(nullptr)
	, m_lastGridRelatedRibbonTab(nullptr)
	, m_Origin(Origin::NotSet)
	, m_DisplayApplyConfirm(true)
	, m_BeingArrangedOrTiled(false)
	, m_sortFilterRibbonTab(nullptr)
	, m_groupRibbonTab(nullptr)
	, m_betaRibbonTab(nullptr)
	, m_hierarchyRibbonTab(nullptr)
	, m_columnFormatRibbonTab(nullptr)
	, m_HasNoTaskRunning(true)
	, m_InPlaceFilterID(ID_EXT_HEADER_FILTER_MENU_TF_CONTAINS)
	, m_InPlaceFilterAutoApply(false)
	, m_InPlaceFilterEdit(nullptr)
	, m_oauth2Browser(nullptr)
	, m_DoNotUpdateWindowsListOnClose(false)
	, m_IgnoreSessionChanged(false)
    , m_CreateAddRemoveToSelection(false)
	, m_CreateRefresh(true)
    , m_CreateRefreshPartial(false)
	, m_ForceFullRefreshAfterApply(false)
	, m_CreateShowContext(false)
    , m_DataContextLabelControl1(nullptr)
	, m_DataContextLabelControl2(nullptr)
	, m_AutomationActionRequestingExecutionPermit(nullptr)
	, m_AddRBACHideOutOfScope(false)
	, m_RBACHideOutOfScope(false)
	, m_RBACHideOutOfScopeBTN(nullptr)
	, m_CanRefreshFromSql(p_CanRefreshFromSql)
	, m_RefreshFromSql(false)
	, m_CreateApplyPartial(true)
	, m_GridLogger(p_CustomLogger)
	, m_ModBtnsUpdater(std::make_unique<GenericSaveAllButtonUpdater>(*this), std::make_unique<GenericSaveSelectedButtonUpdater>(*this), std::make_unique<GenericRevertAllButtonUpdater>(*this), std::make_unique<GenericRevertSelectedButtonUpdater>(*this))
	, m_SyncNow(false)
	, m_NextRefreshLoadMore(false)
	, m_SnapshotReconnectGroup(nullptr)
	, m_SnapshotInfoGroup(nullptr)
	, m_AlreadyChargedTokenAmount(0)
{
	FrameBase<CodeJockFrameBase>::Init();

	// This will call virtual sessionWasSet();
	SetSapio365Session(Sapio365Session::Find(p_SessionIdentifier));

	if (!m_GridLogger)
		m_GridLogger = std::make_shared<GridFrameLogger>(*this);
	LoggerService::AddLogger(m_GridLogger);

	initStaticStrings();

	setMenuBuilder(make_unique<GridFrameSystemMenuBuilder>(this));

	LicenseManager::AddLicenseManagerObserver(this);
}

GridFrameBase::~GridFrameBase()
{
	LicenseManager::RemoveLicenseManagerObserver(this);
	LoggerService::RemoveLogger(m_GridLogger);
}

const PooledString& GridFrameBase::GetCustomTitle() const
{
	return m_CustomTitle;
}

void GridFrameBase::ShowErrorMessageBar(const wstring& p_Title, const wstring& p_Message, const wstring& p_Details)
{
	m_LastErrorInfo = { p_Title , p_Message, p_Details };
	if (p_Details.empty())
		showMessageBar(p_Title, p_Message, p_Details, {}, IDB_ICON_ERROR, RGB(255, 217, 207));
	else
		showMessageBar(p_Title, p_Message, p_Details, { {ID_SHOW_ERROR_MESSAGE_BOX, _YTEXT("Show Details")} }, IDB_ICON_ERROR, RGB(255, 217, 207));
}

void GridFrameBase::ShowSuccessMessageBar(const wstring& p_Title, const wstring& p_Message, int p_ResIcon/* = NO_ICON*/)
{
	COLORREF color = RGB(215, 255, 218);
	if (IDB_ICON_SAVED_NOT_RECEIVED == p_ResIcon || IDB_ICON_SAVED_OVERWRITTEN == p_ResIcon)
	{
		// COLORREF(-1) => default color == yellow
		color = COLORREF(-1);
	}

	showMessageBar(p_Title, p_Message, {}, {}, p_ResIcon, color);
}

void GridFrameBase::RemoveMessageBar()
{
	removeMessageBar();
}

void GridFrameBase::LoadSnaphot(std::shared_ptr<GridSnapshot::Loader> p_Loader, const SessionIdentifier& p_SessionIdentifierForFutureConnnection)
{
	const auto& snapshotPath = p_Loader->GetFilePath();
	m_SnapshotFileName = FileUtil::FileGetFileName(snapshotPath);

	ASSERT(!GetSapio365Session());

	// Store session for future reconnection.
	m_SnapshotSession = Sapio365Session::Find(p_SessionIdentifierForFutureConnnection);

	sessionWasSet(false); // Will update frame title and relog button

	ASSERT(nullptr == m_SnapshotInfoGroup);
	ASSERT(nullptr != m_infoRibbonTab);
	if (nullptr != m_infoRibbonTab)
	{
		wstring groupCaption = p_Loader && p_Loader->GetMetadata().m_Mode && *p_Loader->GetMetadata().m_Mode == GridSnapshot::Options::Mode::RestorePoint
			? _T("Snapshot Info")
			: _T("Restore point Info");
		// 0 is navigation, 1 is session
		m_SnapshotInfoGroup = m_infoRibbonTab->GetGroups()->InsertAt(2, groupCaption.c_str());
		if (nullptr != m_SnapshotInfoGroup)
		{
			setGroupImage(*m_SnapshotInfoGroup, 0);

			{
				auto ctrl = m_SnapshotInfoGroup->Add(xtpControlLabel, ID_SNAPSHOT_CREATOR_NAME);
				if (p_Loader->GetMetadata().m_CreatorName)
					ctrl->SetCaption(p_Loader->GetMetadata().m_CreatorName->c_str());
			}
			{
				auto ctrl = m_SnapshotInfoGroup->Add(xtpControlLabel, ID_SNAPSHOT_CREATOR_USERNAME);
				if (p_Loader->GetMetadata().m_CreatorTechnicalName)
					ctrl->SetCaption(p_Loader->GetMetadata().m_CreatorTechnicalName->c_str());
			}
			{
				auto ctrl = m_SnapshotInfoGroup->Add(xtpControlLabel, ID_SNAPSHOT_CREATION_DATE);
				if (p_Loader->GetMetadata().m_CreationDate)
				{
					CString dateStr;
					DateTimeFormat format;
					format.m_TimeFormat.m_DisplayFlags = TimeFormat::HOURS | TimeFormat::MINUTES | TimeFormat::SECONDS;
					TimeUtil::GetInstance().ConvertDateToText(format, *p_Loader->GetMetadata().m_CreationDate, dateStr);
					ctrl->SetCaption(dateStr);
				}
			}
		}
	}

	ASSERT(nullptr == m_SnapshotReconnectGroup);
	ASSERT(nullptr != m_actionsRibbonTab);
	if (nullptr != m_actionsRibbonTab && m_SnapshotSession)
	{
		// 0 is navigation, 1 is view
		m_SnapshotReconnectGroup = m_actionsRibbonTab->GetGroups()->InsertAt(2, _T("Reconnect"));

		auto relogControl = m_SnapshotReconnectGroup->Add(xtpControlButton, ID_RELOG);
		setImage({ ID_RELOG }, IDB_RELOG, xtpImageNormal);
		setImage({ ID_RELOG }, IDB_RELOG_16X16, xtpImageNormal);
		setControlTexts(relogControl,
			YtriaTranslate::Do(GridSpGroups_customizeGrid_2, _YLOC("Login")).c_str(),
			_T("Re-Open the session"),
			_T("Re-open the session used to create this module."));
	}

	p_Loader->Load(GetGrid(), std::make_unique<O365Grid::SnapshotDataCustomizer>(GetGrid()));

	GetGrid().UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS); // Ensure annotation columns are created.

	if (p_Loader->GetMetadata().m_ProcessesSetup)
	{
		// Finally just restore what's been saved.
		//ASSERT(p_Loader->GetMetadata().m_Mode == GridSnapshot::Options::Mode::RestorePoint);
		GetGrid().RestoreProcessesSetup(*p_Loader->GetMetadata().m_ProcessesSetup, true);
	}

	if (p_Loader->GetMetadata().m_XmlConfig)
		GetGrid().GetColumnOrg()->GetGCVDGrid()->LoadFromXMLString(*(p_Loader->GetMetadata().m_XmlConfig));

	GetGrid().UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

bool GridFrameBase::IsSnapshotMode() const
{
	return (bool) m_SnapshotFileName;
}

void GridFrameBase::SetSnapshotMode(bool p_SnapshotMode)
{
	ASSERT(!m_SnapshotFileName && p_SnapshotMode); // Not reversible
	if (p_SnapshotMode)
		m_SnapshotFileName.emplace();
}

bool GridFrameBase::sessionWasSet(bool p_SessionHasChanged)
{
	if (m_IgnoreSessionChanged || !::IsWindow(m_hWnd))
		return false;

	const bool wasSnapshotMode = IsSnapshotMode();

	auto graphSession = GetSapio365Session();
	ASSERT(graphSession || wasSnapshotMode);

	if (p_SessionHasChanged || !m_BusinessObjectManager)
	{
		ASSERT(!m_BusinessObjectManager || (p_SessionHasChanged && graphSession != m_BusinessObjectManager->GetSapio365Session()));
		if (graphSession)
			m_BusinessObjectManager = std::make_shared<BusinessObjectManager>(graphSession);
		else
			m_BusinessObjectManager.reset();
	}

	if (wasSnapshotMode && IsConnected())
		m_SnapshotFileName.reset(); // We're now connected, not a snapshot anymore.

	updateLogoutControlTexts();
	updateEditElevatedButton();
	updateDumpModeDisplay(); // Update frame title

	if (nullptr != m_infoRibbonTab)
	{
		auto sessionGroup = getSessionGroup();
		if (nullptr != sessionGroup)
			updateSessionGroup(*sessionGroup);

		setSessionTabTitle();
	}

	if (graphSession)
		UpdateRefreshControls();

	if (IsConnected())
	{
		if (nullptr != m_SnapshotReconnectGroup)
		{
			ASSERT(wasSnapshotMode);
			ASSERT(nullptr != m_actionsRibbonTab);
			if (nullptr != m_actionsRibbonTab)
			{
				m_actionsRibbonTab->GetGroups()->Remove(m_SnapshotReconnectGroup);
				m_SnapshotReconnectGroup = nullptr;
			}
		}

		if (nullptr != m_SnapshotInfoGroup)
		{
			ASSERT(wasSnapshotMode);
			ASSERT(nullptr != m_infoRibbonTab);
			if (nullptr != m_infoRibbonTab)
			{
				m_infoRibbonTab->GetGroups()->Remove(m_SnapshotInfoGroup);
				m_SnapshotInfoGroup = nullptr;
			}
		}
	}
	
	return true;
}

void GridFrameBase::UpdateWindowList()
{
	if(nullptr != m_windowsRibbonTab)
	{
		CXTPRibbonGroup*  pGroup = m_windowsRibbonTab->FindGroup(ID_WINDOWS_TAB);
		if (nullptr != pGroup)
		{
			CXTPControlGallery* pGallery = dynamic_cast<CXTPControlGallery*>(pGroup->FindControl(ID_WINDOWS_QUICKACCESS));
			if (nullptr != pGallery)
				UpdateWindowListGallery(pGallery);
		}
	}
}

BEGIN_MESSAGE_MAP(GridFrameBase, FrameBase<CodeJockFrameBase>)
	ON_COMMAND(ID_APPLYALL,							OnApplyAll)
	ON_UPDATE_COMMAND_UI(ID_APPLYALL,				OnUpdateApplyAll)
    ON_COMMAND(ID_APPLYSELECTED,                    OnApplySelected)
    ON_UPDATE_COMMAND_UI(ID_APPLYSELECTED,          OnUpdateApplySelected)
	ON_COMMAND(ID_APPLYSELECTED_USERS,				OnApplySelected)
	ON_UPDATE_COMMAND_UI(ID_APPLYSELECTED_USERS,	OnUpdateApplySelected)
	ON_COMMAND(ID_APPLYSELECTED_GROUPS,				OnApplySelected)
	ON_UPDATE_COMMAND_UI(ID_APPLYSELECTED_GROUPS,	OnUpdateApplySelected)
	ON_COMMAND(ID_APPLYSELECTED_ROLES,				OnApplySelected)
	ON_UPDATE_COMMAND_UI(ID_APPLYSELECTED_ROLES,	OnUpdateApplySelected)
	ON_COMMAND(ID_APPLYSELECTED_SITES,				OnApplySelected)
	ON_UPDATE_COMMAND_UI(ID_APPLYSELECTED_SITES,	OnUpdateApplySelected)
	ON_COMMAND(ID_APPLYSELECTED_CHANNELS,			OnApplySelected)
	ON_UPDATE_COMMAND_UI(ID_APPLYSELECTED_CHANNELS, OnUpdateApplySelected)

	ON_COMMAND(ID_365REFRESH,								On365Refresh)
	ON_UPDATE_COMMAND_UI(ID_365REFRESH,						OnUpdate365Refresh)
	ON_COMMAND(ID_365REFRESH_ASFIRSTLOAD,					On365RefreshAsFirstLoad)
	ON_UPDATE_COMMAND_UI(ID_365REFRESH_ASFIRSTLOAD,			OnUpdate365RefreshAsFirstLoad)
	ON_COMMAND(ID_365REFRESH_RESYNCDELTA,					On365RefreshResyncDelta)
	ON_UPDATE_COMMAND_UI(ID_365REFRESH_RESYNCDELTA,			OnUpdate365RefreshResyncDelta)
    ON_COMMAND(ID_365REFRESHSELECTED,						On365RefreshSelection)
    ON_UPDATE_COMMAND_UI(ID_365REFRESHSELECTED,				OnUpdate365RefreshSelection)
	ON_COMMAND(ID_365REFRESHSELECTED_GROUPS,				On365RefreshSelection)
	ON_UPDATE_COMMAND_UI(ID_365REFRESHSELECTED_GROUPS,		OnUpdate365RefreshSelection)
	ON_COMMAND(ID_365REFRESHSELECTED_ROLES,					On365RefreshSelection)
	ON_UPDATE_COMMAND_UI(ID_365REFRESHSELECTED_ROLES,		OnUpdate365RefreshSelection)
	ON_COMMAND(ID_365REFRESHSELECTED_USERS,					On365RefreshSelection)
	ON_UPDATE_COMMAND_UI(ID_365REFRESHSELECTED_USERS,		OnUpdate365RefreshSelection)
	ON_COMMAND(ID_365REFRESHSELECTED_SITES,					On365RefreshSelection)
	ON_UPDATE_COMMAND_UI(ID_365REFRESHSELECTED_SITES,		OnUpdate365RefreshSelection)
	ON_COMMAND(ID_365REFRESHSELECTED_LISTS,					On365RefreshSelection)
	ON_UPDATE_COMMAND_UI(ID_365REFRESHSELECTED_LISTS,		OnUpdate365RefreshSelection)
	ON_COMMAND(ID_365REFRESHSELECTED_CHANNELS,				On365RefreshSelection)
	ON_UPDATE_COMMAND_UI(ID_365REFRESHSELECTED_CHANNELS,	OnUpdate365RefreshSelection)

	ON_COMMAND(ID_365REVERTALL,							On365RevertAll)
	ON_UPDATE_COMMAND_UI(ID_365REVERTALL,				OnUpdate365RevertAll)
	ON_COMMAND(ID_365REVERTSELECTED,					On365RevertSelected)
	ON_UPDATE_COMMAND_UI(ID_365REVERTSELECTED,			OnUpdate365RevertSelected)
	ON_COMMAND(ID_365REVERTSELECTED_USERS,				On365RevertSelected)
	ON_UPDATE_COMMAND_UI(ID_365REVERTSELECTED_USERS,	OnUpdate365RevertSelected)
	
    ON_WM_CLOSE()
	/*ON_XTP_EXECUTE(ID_GRID_RIBBON_SEARCH_EDIT, OnSearchEdit)
	ON_UPDATE_COMMAND_UI(ID_GRID_RIBBON_SEARCH_EDIT, OnUpdateSearchEdit)*/
	ON_UPDATE_COMMAND_UI(CACHEGRID_ACTIVE_FILTERS_LIST,		OnUpdateFiltersList)
	//ON_UPDATE_COMMAND_UI(ID_GRID_RIBBON_FREEZE_HEADER, OnUpdateFreezeHeader)
	//ON_XTP_EXECUTE(/*CACHEGRID_MENU_FREEZEUPTO*/CACHEGRID_MENU_FREEZECOLUMN, OnFreezeUpTo)
	// In-ribbon search test (not really working yet)
	//ON_XTP_CREATECOMMANDBAR()
	ON_COMMAND(ID_RELOG,													OnRelog)
	ON_UPDATE_COMMAND_UI(ID_RELOG,											OnUpdateRelog)
	ON_UPDATE_COMMAND_UI(ID_LOGOUT,											OnUpdateLogout)
	ON_COMMAND(ID_WINDOWS_QUICKACCESS,										OnQuickAccess)
	ON_UPDATE_COMMAND_UI(ID_WINDOWS_QUICKACCESS,							OnUpdateQuickAccess)
	ON_COMMAND(ID_MAIN_WINDOW_QUICKACCESS,									OnMainWindowQuickAccess)
	ON_UPDATE_COMMAND_UI(ID_MAIN_WINDOW_QUICKACCESS,						OnUpdateMainWindowQuickAccess)
	ON_COMMAND(ID_SHOW_DATA_CONTEXT,										OnShowDataContext)
	ON_UPDATE_COMMAND_UI(ID_SHOW_DATA_CONTEXT,								OnUpdateShowDataContext)
	ON_COMMAND(ID_CLOSE_GRID_FRAME,											OnCloseGridFrame)
	ON_UPDATE_COMMAND_UI(ID_CLOSE_GRID_FRAME,								OnUpdateCloseGridFrame)
	ON_COMMAND(ID_HIERARCHY_FILTERS,										OnHierarchyFilters)
	ON_UPDATE_COMMAND_UI(ID_HIERARCHY_FILTERS,								OnUpdateHierarchyFilters)
	ON_COMMAND(ID_GRIDFRAMEBASE_PREVIOUS,									OnPrevious)
	ON_UPDATE_COMMAND_UI(ID_GRIDFRAMEBASE_PREVIOUS,							OnUpdatePrevious)
	ON_COMMAND(ID_GRIDFRAMEBASE_NEXT,										OnNext)
	ON_UPDATE_COMMAND_UI(ID_GRIDFRAMEBASE_NEXT,								OnUpdateNext)
	ON_COMMAND(ID_GRIDFRAMEBASE_NEXTANDREFRESH,								OnNextAndRefresh)
	ON_UPDATE_COMMAND_UI(ID_GRIDFRAMEBASE_NEXTANDREFRESH,					OnUpdateNext)
	ON_UPDATE_COMMAND_UI(ID_GRIDFRAMEBASE_PREVIOUS_ITEM,					OnUpdateHistoryItem)
	ON_UPDATE_COMMAND_UI(ID_GRIDFRAMEBASE_NEXT_ITEM,						OnUpdateHistoryItem)
	ON_UPDATE_COMMAND_UI(RIBBON_INPLACE_FILTER,								OnUpdateInPlaceFilter)
	ON_COMMAND(RIBBON_INPLACE_FILTER,										OnInPlaceFilter)
	ON_UPDATE_COMMAND_UI(RIBBON_INPLACE_FILTER_COMBO,						OnUpdateInPlaceFilterCombo)
	ON_UPDATE_COMMAND_UI(RIBBON_INPLACE_FILTER_CHECKBOX,					OnUpdateInPlaceFilterCheckbox)
	ON_COMMAND(RIBBON_INPLACE_FILTER_CHECKBOX,								OnInPlaceFilterCheckbox)
	ON_UPDATE_COMMAND_UI(ID_GRIDFRAMEBASE_CACHEGRID_MENU_EXPORT,			OnUpdateCacheGridExport)
	ON_COMMAND(ID_GRIDFRAMEBASE_CACHEGRID_MENU_EXPORT,						OnCacheGridExport)
	ON_UPDATE_COMMAND_UI(ID_GRIDFRAMEBASE_CACHEGRID_MENU_COMPARATOR,		OnUpdateCacheGridComparator)
	ON_COMMAND(ID_GRIDFRAMEBASE_CACHEGRID_MENU_COMPARATOR,					OnCacheGridComparator)
	ON_UPDATE_COMMAND_UI(ID_GRIDFRAMEBASE_CACHEGRID_MENU_FILTERDLG_TEXT,	OnUpdateCacheGridFilter)
	ON_COMMAND(ID_GRIDFRAMEBASE_CACHEGRID_MENU_FILTERDLG_TEXT,				OnCacheGridFilter)
	ON_UPDATE_COMMAND_UI(ID_GRIDFRAMEBASE_CACHEGRID_MENU_DUPLICATESSHOW,	OnUpdateCacheGridDuplicates)
	ON_COMMAND(ID_GRIDFRAMEBASE_CACHEGRID_MENU_DUPLICATESSHOW,				OnCacheGridDuplicates)
	ON_UPDATE_COMMAND_UI(ID_GRIDFRAMEBASE_CACHEGRID_MENU_ANNOTATION,		OnUpdateCacheGridAnnotation)
	ON_COMMAND(ID_GRIDFRAMEBASE_CACHEGRID_MENU_ANNOTATION,					OnCacheGridAnnotation)
	ON_WM_ACTIVATE()
	ON_WM_GETMINMAXINFO()
	ON_MESSAGE(WM_XTP_COMMAND,						OnXTPCommand)
	ON_COMMAND(ID_ADD_TO_SELECTION,					OnAddToSelection)
    ON_UPDATE_COMMAND_UI(ID_ADD_TO_SELECTION,		OnUpdateAddToSelection)
	ON_COMMAND(ID_REMOVE_FROM_SELECTION,			OnRemoveFromSelection)
    ON_UPDATE_COMMAND_UI(ID_REMOVE_FROM_SELECTION,	OnUpdateRemoveFromSelection)

	ON_COMMAND(ID_DEBUG_CLEAR_GRAPH_USERS_CACHE,			OnClearGraphUsersCache_debug)
	ON_UPDATE_COMMAND_UI(ID_DEBUG_CLEAR_GRAPH_USERS_CACHE,	OnUpdateClearUsersGraphCache_debug)
	ON_COMMAND(ID_DEBUG_CLEAR_GRAPH_GROUPS_CACHE,			OnClearGraphUsersCache_debug)
	ON_UPDATE_COMMAND_UI(ID_DEBUG_CLEAR_GRAPH_GROUPS_CACHE, OnUpdateClearUsersGraphCache_debug)

	ON_REGISTERED_MESSAGE(WM_CLOSE_OAUTH2_BROWSER,		OnOAuth2BrowserClose)
	ON_REGISTERED_MESSAGE(WM_SET_OAUTH2_BROWSER,		OnSetOAuth2Browser)
	ON_REGISTERED_MESSAGE(WM_GET_OAUTH2_BROWSER,		OnGetOAuth2Browser)
	ON_REGISTERED_MESSAGE(WM_AUTHENTICATION_SUCCESS,	OnAuthenticationSuccess)
	ON_REGISTERED_MESSAGE(WM_AUTHENTICATION_CANCELED,	OnAuthenticationCanceled)
	ON_REGISTERED_MESSAGE(WM_AUTHENTICATION_FAILURE,	OnAuthenticationFailure)

	ON_COMMAND(ID_RBAC_HIDE_OUTOFSCOPE,				OnToggleRemoveRBACOutOfScope)
	ON_UPDATE_COMMAND_UI(ID_RBAC_HIDE_OUTOFSCOPE,	OnUpdateToggleRBACRemoveOutOfScope)

	ON_COMMAND(ID_SHOW_ERROR_MESSAGE_BOX,			OnShowErrorMessageBox)

	ON_COMMAND(ID_SESSION_ULTRAADMIN_EDIT,				OnEditElevated)
	ON_UPDATE_COMMAND_UI(ID_SESSION_ULTRAADMIN_EDIT,	OnUpdateEditElevated)

	ON_COMMAND(ID_RIBBON_TOKENBUY, OnTokenBuy)

END_MESSAGE_MAP()

BOOL GridFrameBase::OnFrameLoaded()
{
	// SetSapio365Session call in ctor has already called sessionWasSet()
	// but had not effect as window wasn't created yet.
	// Call it again as the window now exists.
	sessionWasSet(true);

	GetGrid().SetCustomTitle(m_CustomTitle);

	// Remove toolbar from grid (and its GCVD) before creating it.
	GetGrid().SetCreateFlags((GetGrid().GetCreateFlags() & ~GridBackendUtil::CREATETOOLBAR) | GridBackendUtil::REMOVECOLORGTOOLBAR);
	GetGrid().SetMetaDataColumnInfo(GetMetaDataColumnInfo());
	createGrid();

	SetAutomationName(wstring(_YTEXT("FRAME-")) + GetGrid().GetAutomationName());
	m_CommandBarsProfileName = wstring(g_ProfUisVersionSetting) + L"\\" + GetGrid().GetAutomationName() + L"\\";

	const bool hasPrevious = HasPrevious();

	if (hasPrevious)
		setDoNotRestorePositionAndSize(true);
	if (!FrameBase<CodeJockFrameBase>::OnFrameLoaded())
	{
		if (hasPrevious)
			setDoNotRestorePositionAndSize(false);
		return FALSE;
	}
	if (hasPrevious)
		setDoNotRestorePositionAndSize(false);

	customizeRibbonBar();

	GetGrid().AddFocusObserver(this);
	if (nullptr != GetGrid().GetColumnOrg() && nullptr != GetGrid().GetColumnOrg()->GetGCVDGrid())
		GetGrid().GetColumnOrg()->GetGCVDGrid()->AddFocusObserver(this);

	if (nullptr != GetGrid().GetColumnOrg() && nullptr != GetGrid().GetColumnOrg()->GetGCVDGrid())
		GetGrid().GetColumnOrg()->AddStateObserver(this);

	GetGrid().SetFocus();

	fixPosition();

	WindowsListUpdater::GetInstance().Add(this);

	LoadCommandBars(m_CommandBarsProfileName.c_str());

	auto s = GetSapio365Session();
	ASSERT(nullptr != s || IsSnapshotMode());
	if (nullptr != s && s->IsUseRoleDelegation())
	{
		auto role = RoleDelegationManager::GetInstance().GetDelegation(s->GetRoleDelegationID(), s);
		if (role.IsLogModuleOpening())
			ActivityLogger::GetInstance().LogBasicAction(_YFORMAT(L"Access module with role: %s", role.m_Name.c_str()), _T("OK"), GetGrid().GetAutomationName(), s);
	}

	prepareLicenseRestrictions();

	return TRUE;
}

void GridFrameBase::prepareLicenseRestrictions()
{
	if (nullptr != m_actionsRibbonTab)
	{
		// set all entries in Manage ready to be locked by AJL license
		auto groups = m_actionsRibbonTab->GetGroups();
		for (int k = 0; k < groups->GetCount(); ++k)
		{
			auto group = groups->GetAt(k);
			if (nullptr != group && 
				!MFCUtil::StringMatch(group->GetCaption(), g_ViewGroup.c_str()) && 
				!MFCUtil::StringMatch(group->GetCaption(), g_ActionsDataGroup.c_str()) &&
				!MFCUtil::StringMatch(group->GetCaption(), g_ActionsApplyGroup.c_str()) &&
				!MFCUtil::StringMatch(group->GetCaption(), g_ActionsRevertGroup.c_str()))
			{
				for (int kentry = 0; kentry < group->GetCount(); kentry++)
				{
					auto groupEntry = group->GetAt(kentry);
					if (nullptr != groupEntry)
						GetGrid().AddToLicenseDisableCommands(groupEntry->GetID());
				}
			}
		}
	}
}

void GridFrameBase::setRefreshControlTexts(CXTPControl* p_Control)
{
	ASSERT(nullptr != p_Control && p_Control->GetID() == ID_365REFRESH);
	if (nullptr != p_Control && p_Control->GetID() == ID_365REFRESH)
	{
		setControlTexts(p_Control
			, HasDeltaFeature()
				? _T("Server Sync")
				: YtriaTranslate::Do(GridFrameBase_CreateDataGroup_1, _YLOC("Refresh All")).c_str()
			, (LPCTSTR)MFCUtil::getFormattedTooltipTitleForRibbon(YtriaTranslate::Do(GridFrameBase_CreateDataGroup_2, _YLOC("Refresh All Grid Data")).c_str(), getAcceleratorString(ID_365REFRESH))
			, HasDeltaFeature()
				? _T("Retrieve latest changes (if any) from the server. All other data will remain unchanged in the grid.")
				: YtriaTranslate::Do(GridFrameBase_CreateDataGroup_3, _YLOC("Reinitialize all data in the grid.\nThis carries out a full data reload from the server. All pending changes will be reverted back to their current state.")).c_str());
	}
}

void GridFrameBase::OnThemeWasSet()
{
	updateDumpModeDisplay();
}

void GridFrameBase::logout()
{
	bool shouldRevert = false;

	if (hasLoseableModificationsPending())
	{
		if (IDOK == YCodeJockMessageBox(this,
										DlgMessageBox::eIcon_Question,
										YtriaTranslate::Do(GridFrameBase_logout_6, _YLOC("Changes pending")).c_str(),
										YtriaTranslate::Do(GridFrameBase_logout_7, _YLOC("Some changes have not been saved and will be lost by signing out.\nDo you want to continue?")).c_str(),
										YtriaTranslate::Do(GridFrameBase_logout_8, _YLOC("You can cancel and save them before signing out.")).c_str(),
										{ { IDOK, YtriaTranslate::Do(GridFrameBase_logout_9, _YLOC("Continue")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridFrameBase_logout_10, _YLOC("Cancel")).c_str() } }).DoModal())
			shouldRevert = true;
		else
			return;
	}

	if (IDOK == YCodeJockMessageBox(this,
									DlgMessageBox::eIcon_Question,
									YtriaTranslate::Do(GridFrameBase_logout_1, _YLOC("Sign out")).c_str(),
									YtriaTranslate::Do(GridFrameBase_logout_2, _YLOC("Do you really want to close the current session?")).c_str(),
									YtriaTranslate::Do(GridFrameBase_logout_3, _YLOC("Your password for this session will be lost. You will need to reenter it when signing in.")).c_str(),
									{ { IDOK, YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str() }, { IDCANCEL, YtriaTranslate::Do(GridFrameBase_logout_5, _YLOC("Cancel")).c_str() } }
									).DoModal())
	{
        auto grid = dynamic_cast<O365Grid*>(&GetGrid());
        ASSERT(nullptr != grid);
        if (nullptr != grid)
        {
            if (shouldRevert)
                grid->RevertAllModifications(true);
            grid->InitRoleDelegation();
        }
		Sapio365Session::SignOut(GetSapio365Session(), this);
	}
}

void GridFrameBase::InitModuleCriteria(const ModuleCriteria& p_ModuleCriteria)
{
	ASSERT(m_ModuleCriteria.m_Origin == Origin::NotSet
		&& m_ModuleCriteria.m_UsedContainer == ModuleCriteria::UsedContainer::NOTSET
		&& m_ModuleCriteria.m_IDs.empty()
		&& m_ModuleCriteria.m_SubIDs.empty());

	m_ModuleCriteria = p_ModuleCriteria;
}

bool GridFrameBase::CheckModuleCriteria(const ModuleCriteria& wantedCriteria)
{
	return wantedCriteria.Matches(GetModuleCriteria());
}

void GridFrameBase::ApplyAllSpecific()
{
	ASSERT(false);
}

void GridFrameBase::ApplySelectedSpecific()
{
    ASSERT(false);
}

void GridFrameBase::OnApplyAll()
{
	ASSERT(GetGrid().GetModApplyer());
	if (GetGrid().GetModApplyer())
		GetGrid().GetModApplyer()->ApplyAll();
}

void GridFrameBase::OnUpdateApplyAll(CCmdUI* pCmdUI)
{
	m_ModBtnsUpdater.UpdateSaveAll(pCmdUI);
}

void GridFrameBase::OnApplySelected()
{
	ASSERT(GetGrid().GetModApplyer());
	if (GetGrid().GetModApplyer())
		GetGrid().GetModApplyer()->ApplySelected();
}

void GridFrameBase::ApplySelectedImpl(bool p_RestoreSelection)
{
	bool apply = true;
	if (m_DisplayApplyConfirm)
	{
		auto applyConfirmationConfig = GetApplyConfirmationConfig(true);
		YCodeJockMessageBox confirmation(this,
			applyConfirmationConfig.m_Icon,
			YtriaTranslate::Do(GridFrameBase_ApplySelectedImpl_1, _YLOC("Save Selected Changes")).c_str(),
			YtriaTranslate::Do(GridFrameBase_ApplySelectedImpl_2, _YLOC("Are you sure?")).c_str(),
			applyConfirmationConfig.m_AdditionalWarning,
			{ { IDOK, YtriaTranslate::Do(GridFrameBase_ApplySelectedImpl_3, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridFrameBase_ApplySelectedImpl_4, _YLOC("No")).c_str() } });
		apply = confirmation.DoModal() == IDOK;
	}

	if (apply)
	{
		if (p_RestoreSelection)
			GetGrid().StoreSelection();
		ApplySelectedSpecific();
		if (p_RestoreSelection)
			GetGrid().RestoreSelection();
	}
}

void GridFrameBase::OnUpdateApplySelected(CCmdUI* pCmdUI)
{
	m_ModBtnsUpdater.UpdateSaveSelected(pCmdUI);
}

bool GridFrameBase::closeConfirmation(bool updateGridIfNoSave)
{
	if (hasLoseableModificationsPending())
	{
		YCodeJockMessageBox confirmation(
			this,
			DlgMessageBox::eIcon_Question,
			YtriaTranslate::Do(GridFrameBase_closeConfirmation_10, _YLOC("Changes pending")).c_str(),
			YtriaTranslate::Do(GridFrameBase_closeConfirmation_8, _YLOC("Some changes have not been saved and will be lost. Close anyway?")).c_str(),
			YtriaTranslate::Do(GridFrameBase_closeConfirmation_9, _YLOC("You could also cancel, sign in and save them before closing.")).c_str(),
			{ { IDOK, YtriaTranslate::Do(GridFrameBase_closeConfirmation_6, _YLOC("Close Anyway")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridFrameBase_closeConfirmation_7, _YLOC("Cancel")).c_str() } });

		switch (confirmation.DoModal())
		{
		case IDCANCEL:
			return false;
		case IDOK: // Don't apply
			if (updateGridIfNoSave)
			{
				auto grid = dynamic_cast<O365Grid*>(&GetGrid());
				ASSERT(nullptr != grid);
				if (nullptr != grid)
					grid->RevertAllModifications(true);
			}
			return true;
		}
	}

	return true;
}

bool GridFrameBase::closeHistoryConfirmation()
{
	static INT_PTR rememberedChoice = 0;

	if (HasPrevious())
	{
		INT_PTR choice = rememberedChoice;

		if (0 == choice)
		{
			YCodeJockMessageBox confirmation(
				this,
				DlgMessageBox::eIcon_Question,
				YtriaTranslate::Do(GridFrameBase_closeHistoryConfirmation_1, _YLOC("History available")).c_str(),
				YtriaTranslate::Do(GridFrameBase_closeHistoryConfirmation_2, _YLOC("Do you want to close the window or go back to the previous view?")).c_str(),
				L"",
				{ { IDOK, YtriaTranslate::Do(GridFrameBase_closeHistoryConfirmation_3, _YLOC("Close")).c_str() },{ IDABORT, YtriaTranslate::Do(GridFrameBase_closeHistoryConfirmation_4, _YLOC("Back")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridFrameBase_closeHistoryConfirmation_5, _YLOC("Cancel")).c_str() } });

			confirmation.SetVerificationText(YtriaTranslate::Do(GridFrameBase_closeHistoryConfirmation_6, _YLOC("Remember my choice until next launch.")).c_str());
			choice = confirmation.DoModal();

			if (confirmation.IsVerificiationChecked() && IDCANCEL != choice)
				rememberedChoice = choice;
		}

		switch (choice)
		{
		case IDOK:
			return true;
		case IDCANCEL:
			return false;
		case IDABORT:
			OnPrevious();
			return false;
		}
	}

	return true;
}

GridFrameBase::ApplyConfirmationConfig GridFrameBase::GetApplyConfirmationConfig(bool p_ApplySelected) const
{
	return ApplyConfirmationConfig();
}

void GridFrameBase::SetOptions(const ModuleOptions& p_Options)
{
	m_Options = p_Options;
}

const boost::YOpt<ModuleOptions>& GridFrameBase::GetOptions() const
{
	return m_Options;
}

bool GridFrameBase::hasModificationsPending(bool inSelectionOnly/* = false*/)
{
	return GetGrid().HasModificationsPending(inSelectionOnly);
}

bool GridFrameBase::hasLoseableModificationsPending(bool inSelectionOnly /*= false*/)
{
	return hasModificationsPending(inSelectionOnly);
}

bool GridFrameBase::hasRevertableModificationsPending(bool inSelectionOnly /*= false*/)
{
	return GetGrid().HasRevertableModificationsPending(inSelectionOnly);
}

bool GridFrameBase::hasModificationsPendingInParentOfSelected()
{
	return GetGrid().HasModificationsPendingInParentOfSelected();
}

bool GridFrameBase::hasLoseableModificationsPendingInParentOfSelected()
{
	return hasModificationsPendingInParentOfSelected();
}

std::shared_ptr<GridFrameLogger> GridFrameBase::GetGridLogger()
{
	return m_GridLogger;
}

void GridFrameBase::revertSelectedImpl()
{
	GetGrid().RevertSelectedRows(O365Grid::RevertFlags::UPDATE_GRID_IF_NEEDED);
}

void GridFrameBase::On365Refresh()
{
	ASSERT(nullptr != GetGrid().GetGridRefresher());
	if (nullptr != GetGrid().GetGridRefresher())
	{
		if (HasDeltaFeature() && (!GetOptions() || GetOptions()->m_CustomFilter.IsEmpty()))
			GetGrid().GetGridRefresher()->Sync();
		else
			GetGrid().GetGridRefresher()->RefreshAll();
	}
}

void GridFrameBase::On365RefreshAsFirstLoad()
{
	On365RefreshAsFirstLoadImpl(true);
}

void GridFrameBase::On365RefreshAsFirstLoadImpl(bool p_Warn)
{
	// FIXME: Add warning about mods too??
	ASSERT(!hasLoseableModificationsPending());
	if (!hasLoseableModificationsPending())
	{
		if (!p_Warn
			|| IDOK == YCodeJockMessageBox(this
								, DlgMessageBox::eIcon_ExclamationWarning
								, _T("Reinitialize entire data set")
								, _T("Reload from the server. All data will be reset in the grid.\nContinue?")
								, _YTEXT("")
								, { { IDOK, _T("OK") }, { IDCANCEL, _T("Cancel") } }).DoModal())
		{
			m_ForceFullRefreshAfterApply = false;
			m_NextRefreshSelectedData.reset();

			ASSERT(!m_SyncNow);
			// FIXME: We should better do this just before updating grid, in case of cancel...
			GetGrid().EmptyGrid();

			On365RefreshImpl(vector<O365UpdateOperation>{}, GetAutomationAction());
		}
	}
}

void GridFrameBase::On365RefreshImpl(vector<O365UpdateOperation>&& p_UpdateOperations, AutomationAction* p_CurrentAction)
{
	updateTimeRefreshControlToNow();

	if (GetGrid().IsGridModificationsEnabled())
	{
		if (p_UpdateOperations.empty())
		{
			GetGrid().GetModifications().RemoveFinalStateModifications();
		}
		else
		{
			GetGrid().GetModifications().RemoveErrorModifications();
			GetGrid().GetModifications().RemoveSentAndReceivedModifications();
		}

        GetGrid().GetModifications().RemoveSubItemDeletionErrors();
        GetGrid().GetModifications().RemoveSubItemUpdateErrors();
	}

	ModuleUtil::ClearErrorRowsStatus(GetGrid());

	GetGrid().AnnotationsRefresh(true);

	m_LastUpdateOperations = p_UpdateOperations; // Move assignment
	RefreshSpecific(p_CurrentAction);
}

void GridFrameBase::On365RefreshSelectionImpl(const RefreshSpecificData& p_RefreshData, vector<O365UpdateOperation>&& p_UpdateOperations, bool p_AppliedSubItems, AutomationAction* p_CurrentAction)
{
	if (GetGrid().IsGridModificationsEnabled())
	{
        if (p_UpdateOperations.empty() && !p_AppliedSubItems)
        {
			GetGrid().GetModifications().RemoveFinalStateModificationsForParentRows(p_RefreshData.m_SelectedRowsAncestors);
        }
        else
        {
			GetGrid().GetModifications().RemoveErrorModifications();
            GetGrid().GetModifications().RemoveSentAndReceivedModifications();
        }

		GetGrid().GetModifications().RemoveSubItemDeletionErrors();
		GetGrid().GetModifications().RemoveSubItemUpdateErrors();
	}

	GetGrid().AnnotationsRefresh(true);// overkill - TODO refresh cache according to p_RefreshData

	m_LastUpdateOperations = p_UpdateOperations; // Move assignment
	RefreshIdsSpecific(p_RefreshData, p_CurrentAction);
}

void GridFrameBase::OnUpdate365Refresh(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_CreateRefresh && IsConnected() && HasNoTaskRunning());
}

void GridFrameBase::OnUpdate365RefreshAsFirstLoad(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_CreateRefresh && IsConnected() && HasNoTaskRunning() && !hasLoseableModificationsPending());
}

void GridFrameBase::On365RefreshResyncDelta()
{
	SetSyncNow(false);
	SetDoLoadMoreOnNextRefresh(false);
	On365RefreshImpl(vector<O365UpdateOperation>{}, GetAutomationAction());
}

void GridFrameBase::OnUpdate365RefreshResyncDelta(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_CreateRefresh && IsConnected() && HasNoTaskRunning() && !hasLoseableModificationsPending());
}

void GridFrameBase::On365RefreshSelection()
{
	ASSERT(GetGrid().GetGridRefresher());
	if (GetGrid().GetGridRefresher())
		GetGrid().GetGridRefresher()->RefreshSelection();
}

void GridFrameBase::SelectModificationsInNextRefreshSelectedData()
{
    // Select all rows that are inside the parent that we are going to refresh
    // so that we can use "OnApplySelected"
	static const set<GridBackendRow*> temp;
	const auto& ancestors = m_NextRefreshSelectedData->m_SelectedRowsAncestors ? *m_NextRefreshSelectedData->m_SelectedRowsAncestors : temp;
	for (auto row : GetGrid().GetBackendRows())
		row->SetSelected(ancestors.find(row->GetTopAncestorOrThis()) != ancestors.end());
}

void GridFrameBase::OnUpdate365RefreshSelection(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_CreateRefresh && m_CreateRefreshPartial && IsConnected() && HasNoTaskRunning() && GetGrid().HasOneRowSelectedAtLeast());
}

void GridFrameBase::On365RevertAll()
{
	ASSERT(GetGrid().GetModReverter() != nullptr);
	if (GetGrid().GetModReverter() != nullptr)
		GetGrid().GetModReverter()->RevertAll();
}

void GridFrameBase::OnUpdate365RevertAll(CCmdUI* pCmdUI)
{
	m_ModBtnsUpdater.UpdateRevertAll(pCmdUI);
}

void GridFrameBase::On365RevertSelected()
{
	ASSERT(GetGrid().GetModReverter() != nullptr);
	if (GetGrid().GetModReverter() != nullptr)
		GetGrid().GetModReverter()->RevertSelected();
}

void GridFrameBase::OnUpdate365RevertSelected(CCmdUI* pCmdUI)
{
	m_ModBtnsUpdater.UpdateRevertSelected(pCmdUI);
}

void GridFrameBase::OnAddToSelection()
{
	if (IsConnected())
	{
		auto selectedItems = AskForItemsToAdd(this);// RBAC irrelevant here
		if (!selectedItems.empty())
			AddToSelection(selectedItems);
	}
}

void GridFrameBase::OnUpdateAddToSelection(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(IsConnected() && !GetGrid().IsLicenseDisableCommand(*pCmdUI));
}

void GridFrameBase::OnRemoveFromSelection()
{
    auto& grid = GetGrid();

    std::set<PooledString> idsToRemove;
    for (auto row : grid.GetSelectedRows())
    {
		if (!row->IsGroupRow())
		{
			GridBackendRow* topRow = row->GetTopAncestorOrThis();
			idsToRemove.insert(topRow->GetField(grid.GetColId()).GetValueStr());
		}
    }

    removeFromSelection(idsToRemove, false);
}

void GridFrameBase::OnUpdateRemoveFromSelection(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(!GetGrid().IsLicenseDisableCommand(*pCmdUI) && GetModuleCriteria().m_IDs.size() > 1 && GetGrid().IsLessThanTopHierarchyRowsIndirectlySelected(GetModuleCriteria().m_IDs.size()));
}

void GridFrameBase::OnRelog()
{
	std::function<void()> onSuccess;
	if (IsSnapshotMode() && m_SnapshotSession)
	{
		if (GetGrid().IsFunctionAnnotations())
			onSuccess = [this]()
			{
				GetGrid().AnnotationsRefresh(true);
				GetGrid().UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
			};
		ASSERT(!GetSapio365Session());
		SetSapio365Session(m_SnapshotSession);
		m_SnapshotSession.reset();
		if (IsConnected())
		{
			if (onSuccess)
				onSuccess();
			return;
		}
	}

	auto session = GetSapio365Session();
	ASSERT(session);
	if (session)
	{
		// Bug #190605.SR.00A002: Remove restriction that prevented from switching to another tenant when a frame was open on a tenant using RBAC and cosmos.
		/*MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
		ASSERT(nullptr != mainFrame);
		if (nullptr != mainFrame && session->IsUseRoleDelegation() && (!mainFrame->HasSession() || !mainFrame->IsSameTenant(session)))
		{
			YCodeJockMessageBox msgBox(this,
				DlgMessageBox::eIcon::eIcon_Information,
				YtriaTranslate::Do(GridFrameBase_OnRelog_6, _YLOC("RBAC Incompatibility")).c_str(),
				YtriaTranslate::Do(GridFrameBase_OnRelog_7, _YLOC("The main window is not connected to %1 tenant. You can't login again while it's not."), session->GetTenantName(true).c_str()),
				_YTEXT("This module will stay disconnected."),
				{ { IDOK, YtriaTranslate::Do(GridFrameBase_OnRelog_3, _YLOC("OK")).c_str() } });
			msgBox.DoModal();
		}
		else*/
		{
			bool proceedFullAdmin = false;
			auto oauth2Auth = dynamic_cast<ClientCredentialsAuthenticator*>(session->GetMainMSGraphSession()->GetAuthenticator().get());

			auto isFullAdmin = nullptr != oauth2Auth;
			if (isFullAdmin)
			{
				wstring tenant;
				wstring appId;
				wstring appClientSecret;
				wstring appDisplayName;

				if (!m_UltraAdminAppID.empty())
				{
					ASSERT(!m_UltraAdminTenant.empty());   
					ASSERT(!m_UltraAdminSecretKey.empty());
					ASSERT(!m_UltraAdminDisplayName.empty());

					appId = m_UltraAdminAppID;
					tenant = m_UltraAdminTenant;
					appClientSecret = m_UltraAdminSecretKey;
					appDisplayName = m_UltraAdminDisplayName;
				}
				else
				{
					PersistentSession persistentSession = SessionsSqlEngine::Get().Load(session->GetIdentifier());
					ASSERT(!persistentSession.GetEmailOrAppId().empty());

					oauth2_token token;
					token.set_access_token(persistentSession.GetAccessToken());
					token.set_refresh_token(persistentSession.GetRefreshToken());
					oauth2Auth->LoadToken(session->GetIdentifier().m_EmailOrAppId, token);
					appId = persistentSession.GetEmailOrAppId();
					tenant = persistentSession.GetTenantName();
					appClientSecret = persistentSession.GetAppClientSecret();
					appDisplayName = GetSessionInfo().GetSessionName();
				}

				m_UltraAdminAppID.clear();
				m_UltraAdminTenant.clear();
				m_UltraAdminSecretKey.clear();
				m_UltraAdminDisplayName.clear();

				DlgSpecialExtAdminAccess dlg(tenant, appId, appClientSecret, appDisplayName, this, session, std::make_unique<DlgSpecialExtAdminUltraAdminJsonGenerator>(true), true);
				if (IDOK == dlg.DoModal())
				{
					proceedFullAdmin = true;

					m_UltraAdminTenant			= dlg.GetTenant();
					m_UltraAdminAppID			= dlg.GetAppId();
					m_UltraAdminSecretKey		= dlg.GetClientSecret();
					m_UltraAdminDisplayName	= dlg.GetDisplayName();

					oauth2Auth->GetOAuth2Config().set_client_key(m_UltraAdminAppID);
					oauth2Auth->GetOAuth2Config().set_client_secret(m_UltraAdminSecretKey);
				}
			}

			if (!isFullAdmin || proceedFullAdmin)
			{
				ASSERT(!GetSessionInfo().IsElevated() && !GetSessionInfo().IsPartnerElevated());
				if (!isFullAdmin)
				{
					auto authenticator = dynamic_cast<OAuth2AuthenticatorBase*>(session->GetMainMSGraphSession()->GetAuthenticator().get());
					if (nullptr != authenticator)
					{
						authenticator->SetPreferredLogin(GetSessionInfo().GetSessionName());
						authenticator->GetOAuth2Config().set_client_key(SessionTypes::GetAppId(GetSessionInfo().GetSessionType()));
					}
				}

				session->GetMainMSGraphSession()->Start(this).Then([this, isFullAdmin, session, onSuccess](const RestAuthenticationResult& p_Result)
				{
					const bool success = p_Result.state == RestAuthenticationResult::State::Success;

					if (!success && isFullAdmin)
					{
						YCallbackMessage::DoPost([this, errorMsg = p_Result.error_message]()
						{
							Util::ShowApplicationConnectionError(_T("Unable to sign back in with the current Ultra Admin session."), errorMsg, this);
							OnRelog();
						});
					}
					else if (success && onSuccess)
					{
						YCallbackMessage::DoPost([onSuccess]()
						{
							onSuccess();
						});
					}
				});
			}
		}
	}
}

void GridFrameBase::OnUpdateRelog(CCmdUI* pCmdUI)
{
	pCmdUI->Enable((!HasSession() && IsSnapshotMode() && m_SnapshotSession || !IsConnected()) && !TaskDataManager::Get().HasTaskRunning());
}

void GridFrameBase::OnClose()
{
	if (GetGrid().HasLicenseDisableCommand() || closeHistoryConfirmation())
		CloseFrame();
}

bool GridFrameBase::CloseFrame()
{
	if (!closingAllowedByAutomation())
	{
		YCodeJockMessageBox dlg(this,
			DlgMessageBox::eIcon_Question,
			YtriaTranslate::Do(GridFrameBase_CloseFrame_1, _YLOC("A job is currently running")).c_str(),
			YtriaTranslate::Do(GridFrameBase_CloseFrame_2, _YLOC("Do you want to stop it and close the frame?")).c_str(),
			YtriaTranslate::Do(GridFrameBase_CloseFrame_4, _YLOC("You could also cancel and wait for the current job to complete.")).c_str(),
			{ { IDOK, YtriaTranslate::Do(GridFrameBase_CloseFrame_5, _YLOC("Stop and Close")).c_str() }, { IDCANCEL, YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str() } });
		dlg.SetBlockAutomation(true);
		if (IDOK == dlg.DoModal())
		{
			AutomatedApp* app = dynamic_cast<AutomatedApp*>(AfxGetApp());
			ASSERT(nullptr != app);
			if (nullptr != app)
				app->StopAutomation();// assuming only one automation at a time can run
		}
		else
		{
			return false;
		}
	}

	if (GetGrid().HasLicenseDisableCommand() || closeConfirmation(false))
	{
		// Remove this frame from the backstage windows list.
		WindowsListUpdater::GetInstance().Remove(this);
		m_module.RemoveGridFrame(this);

		if (!m_DoNotUpdateWindowsListOnClose)
			WindowsListUpdater::GetInstance().Update();
		m_DoNotUpdateWindowsListOnClose = false;

		if (!m_TaskData.IsComplete() && !m_TaskData.IsCanceled())
			m_TaskData.Cancel();

		m_IgnoreSessionChanged = true;
		SetSapio365Session(std::shared_ptr<Sapio365Session>());
		m_IgnoreSessionChanged = false;
		MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
		ASSERT(nullptr != mainFrame);
		if (nullptr != mainFrame)
			mainFrame->PopulateRecentSessions();

		/*const bool canClose = HasNoTaskRunning();
		if (canClose) // for Bug #180123.RL.007506
		{*/
//			ASSERT(nullptr == getStatusBar() || NULL == getStatusBar()->GetSafeHwnd() || getStatusBar()->IsVisible()); // If this assert fails, CXTPStatusBar::SaveState will save a setting that hide the frame status bar.
			SaveCommandBars(m_CommandBarsProfileName.c_str());
			FrameBase<CodeJockFrameBase>::OnClose();
		/*}
		else
			BringWindowToTop();

		return canClose;*/

		return true;
	}

	return false;
}

bool GridFrameBase::CanBeClosedWithoutConfirmation()
{
	return closingAllowedByAutomation() && !hasLoseableModificationsPending();
}

void GridFrameBase::OnUpdateLogout(CCmdUI* pCmdUI)
{
	YTestCmdUI cmdUi(pCmdUI->m_nID);
	FrameBase<CodeJockFrameBase>::OnUpdateLogout(&cmdUi);
	pCmdUI->SetCheck(cmdUi.m_bChecked);
	pCmdUI->Enable(cmdUi.m_bEnabled && IsConnected());
	if (!cmdUi.m_szText.IsEmpty())
		pCmdUI->SetText(cmdUi.m_szText);
}

void GridFrameBase::OnQuickAccess()
{
	CXTPRibbonGroup*  pGroup = m_windowsRibbonTab->FindGroup(ID_WINDOWS_TAB);
	if (nullptr != pGroup)
	{
		CXTPControlGallery* pGallery = dynamic_cast<CXTPControlGallery*>(pGroup->FindControl(ID_WINDOWS_QUICKACCESS));
		if (nullptr != pGallery)
		{
			int SelItem = pGallery->GetSelectedItem();
			CXTPControlGalleryItem* pItem = pGallery->GetItem(SelItem);
			if (nullptr != pItem)
			{
				const HWND frameHwnd = (HWND)pItem->GetData();
				if (::IsWindow(frameHwnd))
				{
					CWnd* cWnd = CWnd::FromHandle(frameHwnd);
					if (nullptr != cWnd)
					{
						cWnd->ShowWindow(SW_RESTORE);
						cWnd->SetForegroundWindow();
					}
				}
			}
		}
	}
}

void GridFrameBase::OnUpdateQuickAccess(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
}

void GridFrameBase::OnMainWindowQuickAccess()
{
	if (nullptr != AfxGetApp()->m_pMainWnd)
	{
		AfxGetApp()->m_pMainWnd->ShowWindow(SW_RESTORE);
		AfxGetApp()->m_pMainWnd->SetForegroundWindow();
	}
}

void GridFrameBase::OnUpdateMainWindowQuickAccess(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
}

void GridFrameBase::OnShowDataContext()
{
	ASSERT(m_CreateShowContext);
	if (m_CreateShowContext)
	{
		const auto& moduleCriteria = GetModuleCriteria();
		wstring selectedId;

		if (ModuleCriteria::UsedContainer::IDS == moduleCriteria.m_UsedContainer)
		{
			const auto oldContext = m_module.GetOriginIdsAndNames(GetModuleCriteria().m_IDs, GetGrid());

			DlgDataContext dlgDataContext(oldContext, m_CreateAddRemoveToSelection, this);
			const auto ret = dlgDataContext.DoModal();
			if (IDOK == ret && m_CreateAddRemoveToSelection)
			{
				auto newContext = dlgDataContext.GetContext1();
				if (newContext != boost::none)
				{
					// Get removed ids
					std::set<PooledString> removedIds;
					for (size_t i = 0; i < oldContext.size(); ++i)
					{
						const bool removed = !newContext->contains(oldContext.at(i));
						if (removed)
							removedIds.insert(oldContext.at(i));
					}

					// Get added ids
					O365IdsContainer addedIds;
					for (size_t i = 0; i < newContext->size(); ++i)
					{
						const bool added = !oldContext.contains(newContext->at(i));
						if (added)
							addedIds.insert(newContext->at(i));
					}

					if (!removedIds.empty())
						removeFromSelection(removedIds, !addedIds.empty());

					if (!addedIds.empty())
					{
						vector<SelectedItem> newItems;
						for (const auto& id : addedIds)
						{
							SelectedItem item;
							item.GetID() = id;
							newItems.push_back(item);
						}
						AddToSelection(newItems);
					}
				}
			}
			else if (ret == IDYES)
			{
				selectedId = dlgDataContext.m_SelectedId;
			}
		}
		else if (ModuleCriteria::UsedContainer::SUBIDS == moduleCriteria.m_UsedContainer)
		{
			ASSERT(!m_CreateAddRemoveToSelection);

			O365IdsContainer ids;
			std::transform(GetModuleCriteria().m_SubIDs.cbegin(), GetModuleCriteria().m_SubIDs.cend(),
				std::inserter(ids, ids.begin()),
				[](const O365SubIdsContainer::value_type& key_value)
			{ return key_value.first.GetId(); });

			const auto context = m_module.GetOriginIdsAndNames(ids, GetGrid());

			LinearMap<PooledString, std::pair<PooledString, PooledString>> fullContext;
			for (size_t i = 0; i < context.size(); ++i)
			{
				auto& id = context.at(i);

				auto it = moduleCriteria.m_SubIDs.find(id);
				ASSERT(moduleCriteria.m_SubIDs.end() != it);
				if (moduleCriteria.m_SubIDs.end() != it)
				{
					const size_t numSubIds = it->second.size();
					PooledString subIdString = YtriaTranslate::Do(GridFrameBase_OnShowDataContext_1, _YLOC("%1 items"), Str::getStringFromNumber(numSubIds).c_str());
					fullContext.push_back(id, std::make_pair(context[id], subIdString));
				}
			}

			DlgDataContext dlgDataContext(fullContext, this);
			if (IDYES == dlgDataContext.DoModal())
				selectedId = dlgDataContext.m_SelectedId;
		}
		else
		{
			ASSERT(false);
		}

		if (!selectedId.empty())
			ModuleBase::SelectOriginId(GetGrid(), selectedId);
	}
}

void GridFrameBase::OnUpdateShowDataContext(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_CreateShowContext && GetOrigin() != Origin::Tenant && GetOrigin() != Origin::NotSet);
}

void GridFrameBase::OnCloseGridFrame()
{
	PostMessage(WM_CLOSE);
}

void GridFrameBase::OnUpdateCloseGridFrame(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
}

void GridFrameBase::OnHierarchyFilters()
{
	CXTPRibbonGroup*  pGroup = m_sortFilterRibbonTab->FindGroup(ID_HIERARCHY_TAB);
	if (nullptr != pGroup)
	{
		CXTPControlGallery* pGallery = dynamic_cast<CXTPControlGallery*>(pGroup->FindControl(ID_HIERARCHY_FILTERS));
		if (nullptr != pGallery)
		{
			int SelItem = pGallery->GetSelectedItem();
			CXTPControlGalleryItem* pItem = pGallery->GetItem(SelItem);
			if (nullptr != pItem)
				GetGrid().SendMessage(WM_COMMAND, pItem->GetID(), 0);
		}
	}
}

CXTPControlGallery* GridFrameBase::GetGalleryHierchyFilter() const
{
	CXTPControlGallery* pGallery = nullptr;

	CXTPRibbonGroup*  pGroup = m_sortFilterRibbonTab->FindGroup(ID_HIERARCHY_TAB);
	if (nullptr != pGroup)
		pGallery = dynamic_cast<CXTPControlGallery*>(pGroup->FindControl(ID_HIERARCHY_FILTERS));

	return pGallery;
}

void GridFrameBase::OnUpdateHierarchyFilters(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
	UpdateHierarchyFilterGallery();
}

void GridFrameBase::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	FrameBase<CodeJockFrameBase>::OnActivate(nState, pWndOther, bMinimized);

	// Update the times in the windows list quick access.
	if (WA_ACTIVE == nState || WA_CLICKACTIVE == nState)
		UpdateWindowList();
}

void GridFrameBase::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// Bug #180411.LS.008196: [-IMPORTANT-] Frames minimum size is too large
	//if (m_BeingArrangedOrTiled)
	{
		// With arranging/tiling, we need to allow smaller frame size to avoid frame overlapping (beyond a certain number of frames, we can't prevent overlapping anyway)
		lpMMI->ptMinTrackSize.x = XTP_DPI_X(50);
		lpMMI->ptMinTrackSize.y = XTP_DPI_Y(40);
	}
	/*else
	{
		lpMMI->ptMinTrackSize.x = XTP_DPI_X(900);
		lpMMI->ptMinTrackSize.y = XTP_DPI_Y(650);
	}*/
}

LRESULT GridFrameBase::OnXTPCommand(WPARAM wParam, LPARAM lParam)
{
	if (XTP_ID_RIBBONCONTROLTAB == wParam)
	{
		NMXTPTABCHANGE* pNM = (NMXTPTABCHANGE*) lParam;
		if (TCN_SELCHANGE == pNM->hdr.code)
		{
			if (m_gcvdRibbonTab == pNM->pTab)
			{
				if (nullptr != GetGrid().GetColumnOrg()
					&& nullptr != GetGrid().GetColumnOrg()->GetGCVDGrid())
					GetGrid().GetColumnOrg()->GetGCVDGrid()->SetFocus();
			}
			else
			{
				GetGrid().SetFocus();
			}
		}
	}
	else if (ID_GRIDFRAMEBASE_PREVIOUS_ITEM == wParam || ID_GRIDFRAMEBASE_NEXT_ITEM == wParam)
	{
		NMXTPCONTROL* pNM = (NMXTPCONTROL*) lParam;
		if (CBN_XTP_EXECUTE == pNM->hdr.code)
		{
			ASSERT(nullptr != pNM->pControl);
			if (nullptr != pNM->pControl)
			{
				IHistoryFrame* historyFrame = reinterpret_cast<IHistoryFrame*>(pNM->pControl->GetTag());
				ASSERT(nullptr != historyFrame);
				if (nullptr != historyFrame)
				{
					if (canNavigate())
						JumpTo(historyFrame);
				}
			}
		}
		return TRUE;
	}
	else if (RIBBON_INPLACE_FILTER == wParam)
	{
		NMXTPCONTROL* pNM = (NMXTPCONTROL*)lParam;
		if (EN_CHANGE == pNM->hdr.code)
		{
			ASSERT(dynamic_cast<CXTPControlEdit*>(pNM->pControl) == m_InPlaceFilterEdit);
			const bool wasEmpty = m_InPlaceFilter.empty();
			m_InPlaceFilter = m_InPlaceFilterEdit->GetEditText();

			if (m_InPlaceFilterAutoApply)
				applyInPlaceFilter();
			else
				updateInPlaceFilterApplyButton();
		}
		else if (XTP_FN_BUDDYBUTTONCLICK == pNM->hdr.code)
		{
			if (!m_InPlaceFilterAutoApply)
				applyInPlaceFilter();			
		}
	}
	else if (RIBBON_INPLACE_FILTER_COMBO == wParam)
	{
		NMXTPCONTROL* pNM = (NMXTPCONTROL*)lParam;
		if (CBN_SELCHANGE == pNM->hdr.code)
		{
			auto combo = dynamic_cast<CXTPControlComboBox*>(pNM->pControl);

			const bool keepEditorText = m_InPlaceFilter != (LPCTSTR)getInPlaceFilterBackendText() && !m_InPlaceFilterAutoApply;

			m_InPlaceFilterID = (UINT) combo->GetItemData(combo->GetCurSel());

			ASSERT(dynamic_cast<CXTPControlEdit*>(combo->GetRibbonGroup()->FindControl(RIBBON_INPLACE_FILTER)) == m_InPlaceFilterEdit);
			if (nullptr != m_InPlaceFilterEdit && !keepEditorText)
				updateInPlaceFilterText();

			if (m_InPlaceFilterAutoApply)
				applyInPlaceFilter();
			else
				updateInPlaceFilterApplyButton();
		}
	}

	return FALSE;
}

void GridFrameBase::OnPrevious()
{
	if (canNavigate() && closeConfirmation(true))
		GoToPrevious();
}

void GridFrameBase::OnUpdatePrevious(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!GetGrid().IsLicenseDisableCommand(*pCmdUI) && HasPrevious());
}

void GridFrameBase::OnNext()
{
	OnNextImpl(false);
}

void GridFrameBase::OnNextAndRefresh()
{
	OnNextImpl(true);
}

void GridFrameBase::OnNextImpl(bool p_Refresh)
{
	if (canNavigate() && closeConfirmation(true))
		GoToNext(p_Refresh);
}

void GridFrameBase::OnUpdateNext(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!GetGrid().IsLicenseDisableCommand(*pCmdUI) && HasNext());
}

void GridFrameBase::OnUpdateHistoryItem(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!GetGrid().IsLicenseDisableCommand(*pCmdUI));
}

void GridFrameBase::OnUpdateInPlaceFilter(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(nullptr != GetGrid().GetColumnByFocus());
	updateInPlaceFilterText();
}

void GridFrameBase::OnInPlaceFilter()
{
	if (!m_InPlaceFilterAutoApply)
		applyInPlaceFilter();
}

void GridFrameBase::OnUpdateInPlaceFilterCombo(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(nullptr != GetGrid().GetColumnByFocus());
}

void GridFrameBase::OnUpdateInPlaceFilterCheckbox(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(nullptr != GetGrid().GetColumnByFocus());
	pCmdUI->SetCheck(m_InPlaceFilterAutoApply);
}

void GridFrameBase::OnInPlaceFilterCheckbox()
{
	m_InPlaceFilterAutoApply = !m_InPlaceFilterAutoApply;

	if (m_InPlaceFilterAutoApply)
		applyInPlaceFilter();
}

void GridFrameBase::OnUpdateCacheGridExport(CCmdUI* pCmdUI)
{
	ASSERT(gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(pCmdUI->m_nID));
	YTestCmdUI cmdUI(gridCommandsDuplicatesForRibbon.at(pCmdUI->m_nID));
	GetGrid().OnCmdMsg(cmdUI.m_nID, CN_UPDATE_COMMAND_UI, &cmdUI, NULL); //SendMessage(WM_COMMAND, MAKEWPARAM(CACHEGRID_MENU_EXPORT, CN_UPDATE_COMMAND_UI), NULL);
	pCmdUI->Enable(cmdUI.m_bEnabled);
	pCmdUI->SetText(YtriaTranslate::Do(GridFrameBase_OnUpdateCacheGridExport_1, _YLOC("Export...")).c_str());
}

void GridFrameBase::OnCacheGridExport()
{
	ASSERT(gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(ID_GRIDFRAMEBASE_CACHEGRID_MENU_EXPORT));
	SendMessage(WM_COMMAND, gridCommandsDuplicatesForRibbon.at(ID_GRIDFRAMEBASE_CACHEGRID_MENU_EXPORT));
}

void GridFrameBase::OnUpdateCacheGridComparator(CCmdUI* pCmdUI)
{
	ASSERT(gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(pCmdUI->m_nID));
	YTestCmdUI cmdUI(gridCommandsDuplicatesForRibbon.at(pCmdUI->m_nID));
	GetGrid().OnCmdMsg(cmdUI.m_nID, CN_UPDATE_COMMAND_UI, &cmdUI, NULL); //SendMessage(WM_COMMAND, MAKEWPARAM(CACHEGRID_MENU_EXPORT, CN_UPDATE_COMMAND_UI), NULL);
	pCmdUI->Enable(cmdUI.m_bEnabled);
	pCmdUI->SetText(YtriaTranslate::Do(GridFrameBase_OnUpdateCacheGridComparator_1, _YLOC("Compare...")).c_str());
}

void GridFrameBase::OnCacheGridComparator()
{
	ASSERT(gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(ID_GRIDFRAMEBASE_CACHEGRID_MENU_COMPARATOR));
	SendMessage(WM_COMMAND, gridCommandsDuplicatesForRibbon.at(ID_GRIDFRAMEBASE_CACHEGRID_MENU_COMPARATOR));
}

void GridFrameBase::OnUpdateCacheGridFilter(CCmdUI* pCmdUI)
{
	ASSERT(gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(pCmdUI->m_nID));
	YTestCmdUI cmdUI(gridCommandsDuplicatesForRibbon.at(pCmdUI->m_nID));
	GetGrid().OnCmdMsg(cmdUI.m_nID, CN_UPDATE_COMMAND_UI, &cmdUI, NULL); //SendMessage(WM_COMMAND, MAKEWPARAM(CACHEGRID_MENU_EXPORT, CN_UPDATE_COMMAND_UI), NULL);
	pCmdUI->Enable(cmdUI.m_bEnabled);
	pCmdUI->SetText(YtriaTranslate::Do(GridFrameBase_OnUpdateCacheGridFilter_1, _YLOC("Filters...")).c_str());
}

void GridFrameBase::OnCacheGridFilter()
{
	ASSERT(gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(ID_GRIDFRAMEBASE_CACHEGRID_MENU_FILTERDLG_TEXT));
	SendMessage(WM_COMMAND, gridCommandsDuplicatesForRibbon.at(ID_GRIDFRAMEBASE_CACHEGRID_MENU_FILTERDLG_TEXT));
}

void GridFrameBase::OnUpdateCacheGridDuplicates(CCmdUI* pCmdUI)
{
	ASSERT(gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(pCmdUI->m_nID));
	YTestCmdUI cmdUI(gridCommandsDuplicatesForRibbon.at(pCmdUI->m_nID));
	GetGrid().OnCmdMsg(cmdUI.m_nID, CN_UPDATE_COMMAND_UI, &cmdUI, NULL); //SendMessage(WM_COMMAND, MAKEWPARAM(CACHEGRID_MENU_EXPORT, CN_UPDATE_COMMAND_UI), NULL);
	pCmdUI->Enable(cmdUI.m_bEnabled);
	pCmdUI->SetText(YtriaTranslate::Do(GridFrameBase_OnUpdateCacheGridDuplicates_1, _YLOC("Duplicates")).c_str());
}

void GridFrameBase::OnCacheGridDuplicates()
{
	ASSERT(gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(ID_GRIDFRAMEBASE_CACHEGRID_MENU_DUPLICATESSHOW));
	SendMessage(WM_COMMAND, gridCommandsDuplicatesForRibbon.at(ID_GRIDFRAMEBASE_CACHEGRID_MENU_DUPLICATESSHOW));
}

void GridFrameBase::OnUpdateCacheGridAnnotation(CCmdUI* pCmdUI)
{
	ASSERT(gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(pCmdUI->m_nID));
	YTestCmdUI cmdUI(gridCommandsDuplicatesForRibbon.at(pCmdUI->m_nID));
	GetGrid().OnCmdMsg(cmdUI.m_nID, CN_UPDATE_COMMAND_UI, &cmdUI, NULL); //SendMessage(WM_COMMAND, MAKEWPARAM(CACHEGRID_MENU_EXPORT, CN_UPDATE_COMMAND_UI), NULL);
	pCmdUI->Enable(cmdUI.m_bEnabled);
	pCmdUI->SetText(YtriaTranslate::Do(GridFrameBase_OnUpdateCacheGridFreeColumn_1, _YLOC("Add && Edit...")).c_str());
}

void GridFrameBase::OnCacheGridAnnotation()
{
	ASSERT(gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(ID_GRIDFRAMEBASE_CACHEGRID_MENU_ANNOTATION));
	SendMessage(WM_COMMAND, gridCommandsDuplicatesForRibbon.at(ID_GRIDFRAMEBASE_CACHEGRID_MENU_ANNOTATION));
}

void GridFrameBase::OnUpdateClearUsersGraphCache_debug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(isDebugMode() && IsConnected());
}

void GridFrameBase::OnClearGraphUsersCache_debug()
{
	if (isDebugMode() && IsConnected())
		GetSapio365Session()->GetGraphCache().ClearUsers();
}

void GridFrameBase::OnUpdateClearGroupsGraphCache_debug(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(isDebugMode() && IsConnected());
}

void GridFrameBase::OnClearGraphGroupsCache_debug()
{
	if (isDebugMode() && IsConnected())
		GetSapio365Session()->GetGraphCache().ClearGroups();
}

BOOL GridFrameBase::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	if (CN_UPDATE_COMMAND_UI == nCode)
	{
		CCmdUI* pCmdUI = static_cast<CCmdUI*>(pExtra);
		ASSERT(nullptr != pCmdUI);
		RibbonCmdUI CmdUI;
		static_cast<CCmdUI&>(CmdUI) = *pCmdUI;
		BOOL result = FALSE;

		if (FALSE == result && GetGrid().OnCmdMsg(nID, nCode, &CmdUI, pHandlerInfo))
		{
			pCmdUI->SetCheck(CmdUI.m_bChecked);
			pCmdUI->Enable(CmdUI.m_bEnabled);
			pCmdUI->SetText(CmdUI.m_szText);
			result = TRUE;
		}

		if (FALSE == result
			&& nullptr != GetGrid().GetColumnOrg() && nullptr != GetGrid().GetColumnOrg()->GetGCVDGrid()
			&& GetGrid().GetColumnOrg()->GetGCVDGrid()->OnCmdMsg(nID, nCode, &CmdUI, pHandlerInfo))
		{
			pCmdUI->SetCheck(CmdUI.m_bChecked);
			pCmdUI->Enable(nullptr != m_gcvdRibbonTab && getRibbonBar().GetCurSel() == m_gcvdRibbonTab->GetIndex() ? CmdUI.m_bEnabled : FALSE);
			pCmdUI->SetText(CmdUI.m_szText);
			result = TRUE;
		}

		if (CACHEGRID_MENU_SORTBYTOTAL == pCmdUI->m_nID)
		{
			UpdateMenuSortByTotal();
		}
		else if (CACHEGRID_MENU_SETCOLUMNTOTAL == pCmdUI->m_nID)
		{
			UpdateMenuSetColumnTotal();
		}
		//else if (CACHEGRID_MENU_FREEZECOLUMN == pCmdUI->m_nID)
		//{
		//	UpdateFreezeMenu(CXTPControl::FromUI(pCmdUI));
		//}

		if (TRUE == result)
			return result;
	}

	else if (GetGrid().OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	if (nullptr != GetGrid().GetColumnOrg() && nullptr != GetGrid().GetColumnOrg()->GetGCVDGrid()
		&& GetGrid().GetColumnOrg()->GetGCVDGrid()->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	//if (XTP_CN_UPDATE_CUSTOMIZE_COMMAND_UI == nCode)
	//{
	//	CXTPControl** pControls = reinterpret_cast<CXTPControl**>(pExtra);
	//	CXTPControl* pCustomizedMenuControl = pControls[0];
	//	CXTPControl* pControl = pControls[1];
	//	ASSERT(nID == pControl->GetID());
	//	pCustomizedMenuControl->SetEnabled(TRUE);
	//	bHandled = TRUE;
	//}

	return FrameBase<CodeJockFrameBase>::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

BOOL GridFrameBase::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_SYSKEYDOWN && VK_F4 == pMsg->wParam)
	{
		// "Alt+F4" or "Alt+Shift+F4" should close the app instead of the window.
		ASSERT(MFCUtil::isALTDOWN());
		AfxGetApp()->m_pMainWnd->PostMessage(WM_CLOSE);
		return TRUE;
	}

	return FrameBase<CodeJockFrameBase>::PreTranslateMessage(pMsg);
}

void GridFrameBase::fixPosition()
{
	// There is something going on in makeDisplayRows that fucks the initial position of the grid.
	// Trigging a resize of the frame sets everything right. Kinda like a kick in the ass.
	CRect rcClient;
	GetWindowRect(&rcClient);
	PostMessage(WM_SIZE, SIZE_RESTORED, MAKELPARAM(rcClient.Width(), rcClient.Height()));
}

void GridFrameBase::RefreshSpecific()
{
	RefreshSpecific(nullptr);// WTF? QLB?
}

void GridFrameBase::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
	ASSERT(m_CreateRefresh && !m_CreateRefreshPartial);
	setAutomationGreenLight(p_CurrentAction);
}

void GridFrameBase::AddToSelection(const vector<SelectedItem>& p_SelectedItems)
{
    auto& moduleCriteria = GetModuleCriteria();

    ModuleCriteria newCriteria = moduleCriteria;
    newCriteria.m_IDs.clear();

    for (const auto& newItem : p_SelectedItems)
    {
        newCriteria.m_IDs.insert(newItem.GetID());
        moduleCriteria.m_IDs.insert(newItem.GetID());
    }

    // Show rows corresponding to new ids (in case they were previously hidden)
    /*auto& grid = GetGrid();
    for (auto row : grid.GetBackendRows())
    {
        auto ancestorRow = row->GetTopAncestorOrThis();
        if (newCriteria.m_IDs.find(ancestorRow->GetField(grid.GetColId()).GetValueStr()) != newCriteria.m_IDs.end())
            row->SetHidden(false);
    }*/
    RefreshSpecificData refreshData;
    refreshData.m_Criteria = newCriteria;
	refreshData.m_SelectedRowsAncestors.emplace();

    On365RefreshSelectionImpl(refreshData, {}, false, GetAutomationAction());
}

void GridFrameBase::removeFromSelection(const std::set<PooledString>& p_IdsToRemove, bool p_AllowEmpty)
{
	auto& criteria = GetModuleCriteria();

	auto numIdsToRemove = p_AllowEmpty	? p_IdsToRemove.size() >= criteria.m_IDs.size() ? criteria.m_IDs.size() : p_IdsToRemove.size()
										: p_IdsToRemove.size() >= criteria.m_IDs.size() ? criteria.m_IDs.size() - 1 : p_IdsToRemove.size();

	ASSERT(numIdsToRemove > 0);
	if (numIdsToRemove <= 0)
		return;

	wstring Information;
	if (GetOrigin() == Origin::User)
		Information = YtriaTranslate::Do(GridFrameBase_RemoveFromSelection_5, _YLOC("%1 Users will be removed from the grid.\nAny pending changes related to these items will be undone."), std::to_wstring(numIdsToRemove).c_str());
	else if (GetOrigin() == Origin::Group)
		Information = YtriaTranslate::Do(GridFrameBase_RemoveFromSelection_6, _YLOC("%1 Groups will be removed from the grid.\nAny pending changes related to these items will be undone."), std::to_wstring(numIdsToRemove).c_str());
	else if (GetOrigin() == Origin::Lists)
		Information = YtriaTranslate::Do(GridFrameBase_RemoveFromSelection_7, _YLOC("%1 Lists will be removed from the grid.\nAny pending changes related to these items will be undone."), std::to_wstring(numIdsToRemove).c_str());
	else if (GetOrigin() == Origin::Site)
		Information = YtriaTranslate::Do(GridFrameBase_RemoveFromSelection_8, _YLOC("%1 Sites will be removed from the grid.\nAny pending changes related to these items will be undone."), std::to_wstring(numIdsToRemove).c_str());
	else if (GetOrigin() == Origin::Conversations)
		Information = YtriaTranslate::Do(GridFrameBase_RemoveFromSelection_9, _YLOC("%1 Conversations will be removed from the grid.\nAny pending changes related to these items will be undone."), std::to_wstring(numIdsToRemove).c_str());
	else
		ASSERT(FALSE);

    YCodeJockMessageBox confirmation(
        this,
        DlgMessageBox::eIcon_Question,
        YtriaTranslate::Do(GridFrameBase_RemoveFromSelection_1, _YLOC("Remove from Selection")).c_str(),
        YtriaTranslate::Do(GridFrameBase_RemoveFromSelection_2, _YLOC("Are you sure?")).c_str(),
		Information,
        { 
			{ 
				IDOK, 
				YtriaTranslate::Do(GridFrameBase_RemoveFromSelection_3, _YLOC("Yes")).c_str() 
			},
			{ 
				IDCANCEL, 
				YtriaTranslate::Do(GridFrameBase_RemoveFromSelection_4, _YLOC("No")).c_str() 
			} 
		}
    );

    if (confirmation.DoModal() == IDOK)
    {
		std::set<PooledString> removedIds;
        for (const auto& idToRemove : p_IdsToRemove)
        {
            size_t nbElemsErased = criteria.m_IDs.erase(idToRemove);
            ASSERT(nbElemsErased == 1);

			removedIds.insert(idToRemove);
			// Keep at least one id
			if (--numIdsToRemove == 0)
				break;
        }

        std::set<GridBackendRow*> rowsToRemove;
        for (auto row : GetGrid().GetBackendRows())
        {
			if (0 == row->GetHierarchyLevel())
			{
				if (removedIds.find(row->GetField(GetGrid().GetColId()).GetValueStr()) != removedIds.end())
				{
					ASSERT(!(row->IsModified() || row->IsDeleted() || row->IsCreated()));
					rowsToRemove.insert(row);
				}
			}
			else
			{
				GridBackendRow* topRow = row->GetTopAncestorOrThis();
				ASSERT(topRow != row);

				// Is it a row that is going to be removed and modified?
				if (removedIds.find(topRow->GetField(GetGrid().GetColId()).GetValueStr()) != removedIds.end())
				{
					if (row->IsModified() || row->IsDeleted() || row->IsCreated())
					{
						vector<GridBackendField> pk;
						GetGrid().GetRowPK(row, pk);

						// Don't call revert here, we don't want to update the grid as the row will be removed anyway.
						GetGrid().GetModifications().RemoveSubItem(pk);
						GetGrid().GetModifications().RemoveRowModifications(pk);
					}

					rowsToRemove.insert(row);
				}
			}
        }
		if (m_RemoveRowsCallback)
			m_RemoveRowsCallback(rowsToRemove);

		for (auto row : rowsToRemove)
			GetGrid().RemoveRowFromProcesses(row);
		GetGrid().RemoveRows(rowsToRemove, false);


		{
			GridUpdater updater(GetGrid(), GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID));
		}
		RefreshDataContext();
    }
}

const wstring& GridFrameBase::GetFrameTitle() const
{
	ASSERT(!m_CreationDateString.empty());

	const auto role = GetSessionInfo().GetRoleNameInDb();
	if (role.empty())
	{
		if (!IsConnected())
		{
			if (IsSnapshotMode())
			{
				// restore point is always loaded with a session, snapshot not.
				if (HasSession())
					m_Title = _YFORMAT(L"Restore point: %s (Disconnected)", m_SnapshotFileName->c_str());
				else
					m_Title = _YFORMAT(L"Snapshot: %s", m_SnapshotFileName->c_str());
			}
			else
			{
				m_Title = YtriaTranslate::Do(GridFrameBase_GetFrameTitle_1, _YLOC("%1 - %2 (Disconnected - %3) - %4"),
					m_CustomTitle.c_str(),
					GetSessionInfo().GetSessionName().c_str(),
					m_CreationDateString.c_str(),
					SessionTypes::GetDisplayedSessionType(GetSessionInfo().GetSessionType()).c_str()
				);
			}
		}
		else
		{
			// Is it still a Restore point?
			/*if (IsSnapshotMode())
			{
				m_Title = _YFORMAT(L"Restore point: %s", m_SnapshotFileName->c_str());
			}
			else*/
			{
				m_Title = _YTEXTFORMAT(L"%s - %s (%s) - %s",
					m_CustomTitle.c_str(),
					GetSessionInfo().GetSessionName().c_str(),
					m_CreationDateString.c_str(),
					SessionTypes::GetDisplayedSessionType(GetSessionInfo().GetSessionType()).c_str()
				);
			}
		}
	}
	else
	{
		if (!IsConnected())
		{
			if (IsSnapshotMode())
			{
				// restore point is always loaded with a session, snapshot not.
				if (HasSession())
					m_Title = _YFORMAT(L"Restore point: %s (Disconnected)", m_SnapshotFileName->c_str());
				else
					m_Title = _YFORMAT(L"Snapshot: %s", m_SnapshotFileName->c_str());
			}
			else
			{
				m_Title = YtriaTranslate::Do(GridFrameBase_GetFrameTitle_2, _YLOC("%1 - [ %2 ] (Disconnected - %3) - %4 - %5"),
					m_CustomTitle.c_str(),
					role.c_str(),
					m_CreationDateString.c_str(),
					GetSessionInfo().GetSessionName().c_str(),
					SessionTypes::GetDisplayedSessionType(GetSessionInfo().GetSessionType()).c_str()
				);
			}
		}
		else
		{
			// Is it still a Restore point?
			/*if (IsSnapshotMode())
			{
				m_Title = _YFORMAT(L"Restore point: %s", m_SnapshotFileName->c_str());
			}
			else*/
			{
				m_Title = _YTEXTFORMAT(L"%s - [ %s ] (%s) - %s - %s",
					m_CustomTitle.c_str(),
					role.c_str(),
					m_CreationDateString.c_str(),
					GetSessionInfo().GetSessionName().c_str(),
					SessionTypes::GetDisplayedSessionType(GetSessionInfo().GetSessionType()).c_str()
				);
			}
		}
	}

	return m_Title;
}

wstring GridFrameBase::GetWindowsListQuickAccessTitle() const
{
	CString Temp;
	YTimeDate Now = YTimeDate::GetCurrentTimeDate();
	LONGLONG DeltaSec = Now - m_CreationDate;

	Temp.Format(_YTEXT("%d"), DeltaSec);
	wstring LengthTime = (DeltaSec < 5 ? YtriaTranslate::Do(GridFrameBase_GetWindowsListQuickAccessTitle_1, _YLOC("Just Now")).c_str() : YtriaTranslate::Do(GridFrameBase_GetWindowsListQuickAccessTitle_2, _YLOC("> %1 Seconds"), Temp));
	LONGLONG DeltaMin = DeltaSec / 60;
	if(DeltaMin >= 1)
	{
		Temp.Format(_YTEXT("%d"), DeltaMin);
		LengthTime = (DeltaMin == 1 ? YtriaTranslate::Do(GridFrameBase_GetWindowsListQuickAccessTitle_3, _YLOC("> %1 Minute"), Temp) : YtriaTranslate::Do(GridFrameBase_GetWindowsListQuickAccessTitle_4, _YLOC("> %1 Minutes"), Temp));
		LONGLONG DeltaHours = DeltaMin / 60;
		if (DeltaHours >= 1)
		{
			Temp.Format(_YTEXT("%d"), DeltaHours);
			LengthTime = (DeltaHours == 1 ? YtriaTranslate::Do(GridFrameBase_GetWindowsListQuickAccessTitle_5, _YLOC("More than %1 Hour"), Temp) : YtriaTranslate::Do(GridFrameBase_GetWindowsListQuickAccessTitle_6, _YLOC("More than %1 Hours"), Temp));
		}
	}

	return _YTEXTFORMAT(L"%s (%s) - %s", m_CustomTitle.c_str(), LengthTime.c_str(), GetSessionInfo().GetSessionName().c_str());
}

void GridFrameBase::ErectSpecific()
{
	// Disable existing frame windows to prevent them from going foreground during LoadFrame
	vector<CWnd*> enabledFrames;

	if (::IsWindow(AfxGetApp()->m_pMainWnd->m_hWnd) && AfxGetApp()->m_pMainWnd->IsWindowEnabled())
	{
		AfxGetApp()->m_pMainWnd->EnableWindow(FALSE);
		enabledFrames.push_back(AfxGetApp()->m_pMainWnd);
	}

	auto frames = CommandDispatcher::GetInstance().GetAllGridFrames();
	for (auto frame : frames)
	{
		// frame->IsWindowVisible() => Temporary way to handle new back/forward system. We should maybe do it differently.
		if (::IsWindow(frame->m_hWnd) && frame->IsWindowVisible() && frame->IsWindowEnabled())
		{
			frame->EnableWindow(FALSE);
			enabledFrames.push_back(frame);
		}
	}

	LoadFrame(IDR_GRIDFRAMEBASE, WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | FWS_ADDTOTITLE, nullptr, nullptr);

	// Re-enable all frame windows.
	for (auto frame : enabledFrames)
	{
		if (::IsWindow(frame->m_hWnd))
			frame->EnableWindow(TRUE);
	}

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		mainFrame->ApplyThemeTo(this);

	OnThemeWasSet();

	if (!HasPrevious()) // Handled in HistoryFrame otherwise
		ShowWindow(SW_SHOW);

	// FIXME: This call to UpdateWindow() seems useless and doing it seems to fuck grid init (assert due to header not existing in prof-uis)...
	//UpdateWindow();

	WindowsListUpdater::GetInstance().Update();

	GetGrid().SetLicenseContext(GetLicenseContext());
}

void GridFrameBase::HistoryChanged()
{
	{
		vector<IHistoryFrame*> previousFrames = GetPreviousFrames();
		updateNavigationDropDown(m_NavigatePreviousControls, ID_GRIDFRAMEBASE_PREVIOUS_ITEM, previousFrames);
	}
	{
		vector<IHistoryFrame*> nextFrames = GetNextFrames();
		updateNavigationDropDown(m_NavigateNextControls, ID_GRIDFRAMEBASE_NEXT_ITEM, nextFrames);
	}
}

void GridFrameBase::ShownViaHistory()
{
	// Show pending modal dialog.
	GetGrid().ShowPendingPostUpdateError();
}

bool GridFrameBase::HasApplyButton()
{
    O365Grid* pGrid = dynamic_cast<O365Grid*>(&GetGrid());
    ASSERT(nullptr != pGrid);
    if (nullptr != pGrid)
        return pGrid->HasEdit() || pGrid->HasCreate() || pGrid->HasDelete() || pGrid->HasView();

    return false;
}

wstring GridFrameBase::getInitFrameTitle() const
{
	return GetFrameTitle();
}

CXTPRibbonBar* GridFrameBase::createRibbonBar()
{
	// Overridden only to make it final.
	return FrameBase<CodeJockFrameBase>::createRibbonBar();
}

void GridFrameBase::customizeRibbonBar()
{
	auto& myRibbonBar = getRibbonBar();

	m_actionsRibbonTab = myRibbonBar.AddTab(YtriaTranslate::Do(GridFrameBase_customizeRibbonBar_1, _YLOC("Manage")).c_str());
	if (nullptr != m_actionsRibbonTab)
	{
		if (!customizeActionsRibbonTab(*m_actionsRibbonTab, false, false))
		{
			myRibbonBar.RemoveTab(m_actionsRibbonTab->GetIndex());
			m_actionsRibbonTab = nullptr;
		}
		else
		{
			addNavigationGroup(*m_actionsRibbonTab);
		}
	}

	createFrameSpecificTabs();

	m_globalRibbonTab = myRibbonBar.AddTab(YtriaTranslate::Do(GridFrameBase_customizeRibbonBar_2, _YLOC("Global")).c_str());
	ASSERT(nullptr != m_globalRibbonTab);
	if (nullptr != m_globalRibbonTab)
	{
		m_globalRibbonTab->SetContextTab(g_GridContextColor, g_GridContextName.c_str());
		customizeGridGlobalRibbonTab(*m_globalRibbonTab);
		addNavigationGroup(*m_globalRibbonTab);
	}

	m_sortFilterRibbonTab = myRibbonBar.AddTab(YtriaTranslate::Do(GridFrameBase_customizeRibbonBar_3, _YLOC("Sort/Filter")).c_str());
	ASSERT(nullptr != m_sortFilterRibbonTab);
	if (nullptr != m_sortFilterRibbonTab)
	{
		m_sortFilterRibbonTab->SetContextTab(g_GridContextColor, g_GridContextName.c_str());
		customizeGridColumnRibbonTab(*m_sortFilterRibbonTab);
		addNavigationGroup(*m_sortFilterRibbonTab);
	}

	m_columnFormatRibbonTab = myRibbonBar.AddTab(YtriaTranslate::Do(GridFrameBase_customizeRibbonBar_4, _YLOC("Column Format")).c_str());
	ASSERT(nullptr != m_columnFormatRibbonTab);
	if (nullptr != m_columnFormatRibbonTab)
	{
		m_columnFormatRibbonTab->SetContextTab(g_GridContextColor, g_GridContextName.c_str());
		customizeGridFormatRibbonTab(*m_columnFormatRibbonTab);
		addNavigationGroup(*m_columnFormatRibbonTab);
	}

	m_explodeCellsRibbonTab = myRibbonBar.AddTab(YtriaTranslate::Do(GridFrameBase_customizeRibbonBar_5, _YLOC("Explode Cells")).c_str());
	ASSERT(nullptr != m_explodeCellsRibbonTab);
	if (nullptr != m_explodeCellsRibbonTab)
	{
		m_explodeCellsRibbonTab->SetContextTab(g_GridContextColor, g_GridContextName.c_str());
		customizeGridCellsRibbonTab(*m_explodeCellsRibbonTab);
		addNavigationGroup(*m_explodeCellsRibbonTab);
	}

	m_groupRibbonTab = myRibbonBar.AddTab(YtriaTranslate::Do(GridFrameBase_customizeRibbonBar_6, _YLOC("Grouping")).c_str());
	ASSERT(nullptr != m_groupRibbonTab);
	if (nullptr != m_groupRibbonTab)
	{
		m_groupRibbonTab->SetContextTab(g_GridContextColor, g_GridContextName.c_str());
		customizeGridGroupRibbonTab(*m_groupRibbonTab);
		addNavigationGroup(*m_groupRibbonTab);
	}

	if (GetGrid().GetBackend().IsHierarchyReady())
	{
		m_hierarchyRibbonTab = myRibbonBar.AddTab(YtriaTranslate::Do(GridFrameBase_customizeRibbonBar_7, _YLOC("Hierarchy")).c_str());
		ASSERT(nullptr != m_hierarchyRibbonTab);
		if (nullptr != m_hierarchyRibbonTab)
		{
			m_hierarchyRibbonTab->SetContextTab(g_GridContextColor, g_GridContextName.c_str());
			customizeGridHierarchyRibbonTab(*m_hierarchyRibbonTab);
			addNavigationGroup(*m_hierarchyRibbonTab);
		}
	}

	m_optionsRibbonTab = myRibbonBar.AddTab(YtriaTranslate::Do(GridFrameBase_customizeRibbonBar_8, _YLOC("Options")).c_str());
	ASSERT(nullptr != m_optionsRibbonTab);
	if (nullptr != m_optionsRibbonTab)
	{
		m_optionsRibbonTab->SetContextTab(g_GridContextColor, g_GridContextName.c_str());
		customizeGridOptionsRibbonTab(*m_optionsRibbonTab);
		addNavigationGroup(*m_optionsRibbonTab);
	}

	// Add spaces in the name so that the tab is large enough for the context name to be displayed.
	// FIXME: Make it more automatic
	m_gcvdRibbonTab = myRibbonBar.AddTab(_YTEXT("       - - -        "));
	ASSERT(nullptr != m_gcvdRibbonTab);
	if (nullptr != m_gcvdRibbonTab)
	{
		m_gcvdRibbonTab->SetContextTab(g_GCVDContextColor, g_GCVDContextName.c_str());
		customizeGridGCVDRibbonTab(*m_gcvdRibbonTab);
		addNavigationGroup(*m_gcvdRibbonTab);
		m_gcvdRibbonTab->SetVisible(FALSE);
	}

	m_infoRibbonTab = myRibbonBar.AddTab(YtriaTranslate::Do(GridFrameBase_customizeRibbonBar_9, _YLOC("Session")).c_str());
	ASSERT(nullptr != m_infoRibbonTab);
	if (nullptr != m_infoRibbonTab)
	{
		setSessionTabTitle();
		customizeInfoRibbonTab(*m_infoRibbonTab);
		addNavigationGroup(*m_infoRibbonTab);
	}

	m_windowsRibbonTab = myRibbonBar.AddTab(YtriaTranslate::Do(GridFrameBase_customizeRibbonBar_10, _YLOC("Windows")).c_str());
	ASSERT(nullptr != m_windowsRibbonTab);
	if (nullptr != m_windowsRibbonTab)
	{
		customizeWindowsRibbonTab(*m_windowsRibbonTab);
		addNavigationGroup(*m_windowsRibbonTab);
	}

	customizeRibbonBar(myRibbonBar);

	// Done here, to be sure it's the last tab
	m_betaRibbonTab = addFeedbackRibbonTab(myRibbonBar);
	addNavigationGroup(*m_betaRibbonTab);

	if (nullptr != m_actionsRibbonTab)
		myRibbonBar.SetCurSel(m_actionsRibbonTab->GetIndex());

	m_lastGridRelatedRibbonTab = nullptr != m_actionsRibbonTab ? m_actionsRibbonTab : m_globalRibbonTab;

	automationAddGenericCommandIDsToGreenlight();
}

void GridFrameBase::customizeGridGlobalRibbonTab(CXTPRibbonTab& tab)
{
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGlobalRibbonTab_1, _YLOC("Search")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_SEARCH)->SetStyle(xtpButtonIconAndCaptionBelow);
			addGridCommandControl(*group, CACHEGRID_MENU_FIND)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_FIND_NEXT)->SetStyle(xtpButtonIconAndCaption);
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGlobalRibbonTab_2, _YLOC("Data")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			{
				auto ctrl = addGridCommandControl(*group, CACHEGRID_MENU_SELECTALL);
				ctrl->SetStyle(xtpButtonIconAndCaption);
				ctrl->SetBeginGroup(TRUE);
			}
			addGridCommandControl(*group, CACHEGRID_MENU_SELECTFROMFILE)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_SELECTIONINVERSION)->SetStyle(xtpButtonIconAndCaption);

			{
				auto ctrl = addGridCommandControl(*group, CACHEGRID_MENU_COPYFOCUSEDCOLUMN);
				ctrl->SetStyle(xtpButtonIconAndCaption);
				ctrl->SetBeginGroup(TRUE);
			}
			addGridCommandControl(*group, CACHEGRID_MENU_COPY)->SetStyle(xtpButtonIconAndCaption);

			auto control = addGridSplitButtonControl(*group, ID_GRIDFRAMEBASE_CACHEGRID_MENU_EXPORT, { CACHEGRID_MENU_EXPORT, CACHEGRID_MENU_EXPORTANDCOPYPREFERENCES });
			control->SetStyle(xtpButtonIconAndCaptionBelow);
			control->SetBeginGroup(TRUE);
		}
	}

	{
		auto group = tab.AddGroup(_T("Comments"));
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridSplitButtonControl(*group, ID_GRIDFRAMEBASE_CACHEGRID_MENU_ANNOTATION, { CACHEGRID_MENU_ANNOTATION_EDIT, CACHEGRID_MENU_ANNOTATION_DELETE })->SetStyle(xtpButtonIconAndCaptionBelow);
			addGridCommandControl(*group, CACHEGRID_MENU_ANNOTATION_REFRESH)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_ANNOTATION_VIEWALL_LIVE)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_ANNOTATION_VIEWALL_ORPHANS)->SetStyle(xtpButtonIconAndCaption);

			/*auto control = addGridPopupButtonControl(*group, 0, { CACHEGRID_MENU_ANNOTATION_VIEWALL_LIVE, CACHEGRID_MENU_ANNOTATION_VIEWALL_ORPHANS });
			control->SetIconId(RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_LIST);
			setImage({ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_LIST }, RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_LIST, xtpImageNormal);
			setImage({ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_LIST }, RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_LIST_16X16, xtpImageNormal);
			control->SetStyle(xtpButtonIconAndCaption);
			control->SetCaption(_YTEXT("View Comments..."));
			control->SetTooltip(_YTEXT("View all comments for this Module..."));
			control->SetDescription(_YTEXT("Open a view with all comments found for this module - active ones as well as orphans."));*/
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGlobalRibbonTab_3, _YLOC("Analysis")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridSplitButtonControl(*group, ID_GRIDFRAMEBASE_CACHEGRID_MENU_COMPARATOR, { CACHEGRID_MENU_COMPARATOR, CACHEGRID_MENU_COMPARATORREMOVECOLUMN, CACHEGRID_MENU_COMPARATORREMOVEROWS })->SetStyle(xtpButtonIconAndCaptionBelow);

			addGridDuplicatesControls(*group)->SetStyle(xtpButtonIconAndCaptionBelow);

			addGridCommandControl(*group, CACHEGRID_MENU_CHART)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_PIVOT)->SetStyle(xtpButtonIconAndCaption);
			addGridStatsControls(*group)->SetStyle(xtpButtonIconAndCaption);
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGlobalRibbonTab_4, _YLOC("Reset")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_RESTOREGRID)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_CLEARFILTERS)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_REINITGRID)->SetStyle(xtpButtonIconAndCaption);
		}
	}
}

void GridFrameBase::customizeGridColumnRibbonTab(CXTPRibbonTab& tab)
{
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_1, _YLOC("Sort")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_ASCSORTBYYSELECTEDCOL, xtpControlButton)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_DESCSORTBYYSELECTEDCOL, xtpControlButton)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_REMOVESELECTEDCOLUMNSORTING, xtpControlButton)->SetStyle(xtpButtonIconAndCaption);
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_2, _YLOC("Quick Filter")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			{
				auto ctrl = dynamic_cast<CXTPControlComboBox*>(group->Add(xtpControlComboBox, RIBBON_INPLACE_FILTER_COMBO));
				ASSERT(nullptr != ctrl);
				if (nullptr != ctrl)
				{
					ctrl->SetItemData(ctrl->AddString(YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_7, _YLOC("Contains")).c_str()), ID_EXT_HEADER_FILTER_MENU_TF_CONTAINS);
					ctrl->SetItemData(ctrl->AddString(YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_8, _YLOC("Is equal to")).c_str()), ID_EXT_HEADER_FILTER_MENU_TF_EQUALS);
					ctrl->SetItemData(ctrl->AddString(YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_9, _YLOC("Begins with")).c_str()), ID_EXT_HEADER_FILTER_MENU_TF_BEGINS_WITH);
					ctrl->SetCurSel(0);
					m_InPlaceFilterID = ID_EXT_HEADER_FILTER_MENU_TF_CONTAINS;
				}
			}

			{
				auto ctrl = dynamic_cast<CXTPControlEdit*>(group->Add(xtpControlEdit, RIBBON_INPLACE_FILTER));
				ASSERT(nullptr != ctrl);
				if (nullptr != ctrl)
				{
					m_InPlaceFilterEdit = ctrl;
					m_InPlaceFilterEdit->SetEditHint(YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_10, _YLOC("Enter text...")).c_str());
					setImage({ RIBBON_INPLACE_FILTER_BUTTON }, IDB_INPLACE_FILTER_APPLY_16X16, xtpImageNormal);
				}
			}

			{
				auto ctrl = dynamic_cast<CXTPControlCheckBox*>(group->Add(xtpControlCheckBox, RIBBON_INPLACE_FILTER_CHECKBOX));
				ASSERT(nullptr != ctrl);
				if (nullptr != ctrl)
				{
					ctrl->SetCaption(YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_11, _YLOC("Auto-apply")).c_str());
					ctrl->SetChecked(FALSE);
				}
			}
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_3, _YLOC("Filter")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridSplitButtonControl(*group, ID_GRIDFRAMEBASE_CACHEGRID_MENU_FILTERDLG_TEXT, { CACHEGRID_MENU_FILTERDLG_TEXT, CACHEGRID_MENU_FILTERDLG_NUMBER, CACHEGRID_MENU_FILTERDLG_DATE, CACHEGRID_MENU_FILTERDLG_TIME, CACHEGRID_MENU_FILTERDLG_CUTOFF}); // ->SetStyle(xtpButtonIconAndCaptionBelow)
			addGridCommandControl(*group, CACHEGRID_MENU_HEADER_FILTER_VALUES)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_SETVLALUEFILTERFROMFILE)->SetStyle(xtpButtonIconAndCaption);
			auto c = addGridCommandControl(*group, CACHEGRID_MENU_HEADER_FILTER_MENU_REGEX);
			c->SetStyle(xtpButtonIconAndCaption);
			c->SetBeginGroup(TRUE);
			addGridCommandControl(*group, CACHEGRID_MENU_HEADER_FILTER_MENU_EMPTY)->SetStyle(xtpButtonIconAndCaption);

			{
				auto control = addGridCommandControl(*group, CACHEGRID_MENU_HEADER_FILTER_MENU_CLEAR);
				control->SetStyle(xtpButtonIconAndCaption);
				control->SetBeginGroup(TRUE);
			}

			addGridCommandControl(*group, CACHEGRID_MENU_CLEARFILTERS)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_REMOVEROWSFILTERED)->SetStyle(xtpButtonIconAndCaption);
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_4, _YLOC("Rows")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_HIDEROWS)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_RESTOREHIDDENROWS)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_REMOVEROWSHIDDEN)->SetStyle(xtpButtonIconAndCaption);
		}
	}
	if (GetGrid().GetBackend().IsHierarchyReady())
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_5, _YLOC("Hierarchy Filter Settings")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			group->SetID(ID_HIERARCHY_TAB);

			auto label = (CXTPControlMarkupLabel*)group->Add(new CXTPControlMarkupLabel, ID_HIERARCHY_FILTERS_LABEL);
			if (nullptr != label)
			{
#define CAPTION_XAML \
				L"<StackPanel Margin='5, 3, 5, 3'>" \
				"<TextBlock TextWrapping='Wrap' Foreground='#6f6f6f' Margin='0, 3, 0, 0' Width='100'>%s</TextBlock>" \
				"</StackPanel>"

				CString caption;
				caption.Format(CAPTION_XAML,  YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_12, _YLOC("Set the levels your filters will be applied to.")).c_str());
				label->SetCaption(caption);
			}

			CXTPControlGallery* gallery = dynamic_cast<CXTPControlGallery*>(group->Add(xtpControlGallery, ID_HIERARCHY_FILTERS));
			ASSERT(gallery != nullptr && gallery->GetID() == ID_HIERARCHY_FILTERS);
			if (nullptr != gallery)
			{
				gallery->SetStyle(xtpButtonIconAndCaption);

				// Doesn't work...
				//gallery->SetFlags(gallery->GetFlags() | xtpFlagControlStretched);
				gallery->SetItems(CXTPControlGalleryItems::CreateItems(GetCommandBars(), ID_HIERARCHY_FILTERS));

				UpdateHierarchyFilterGallery(gallery);
			}
		}
	}
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_6, _YLOC("Advanced")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_VIEWHIDDENROWSROWS)->SetStyle(xtpButtonIconAndCaption);
		}
	}
}

void GridFrameBase::customizeGridFormatRibbonTab(CXTPRibbonTab& tab)
{
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridFormatRibbonTab_1, _YLOC("Resize")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_RESIZEVISIBLE)->SetStyle(xtpButtonIconAndCaptionBelow);

			auto control1 = addGridCommandControl(*group, CACHEGRID_MENU_RESIZEDATASET);
			control1->SetStyle(xtpButtonIconAndCaption);
			control1->SetBeginGroup(TRUE);
			addGridCommandControl(*group, CACHEGRID_MENU_RESIZEHEADER)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_RESIZEHEADDATA)->SetStyle(xtpButtonIconAndCaption);

			auto control2 = addGridCommandControl(*group, CACHEGRID_MENU_SETCOLUMNAUTORESIZE);
			control2->SetStyle(xtpButtonIconAndCaption);
			control2->SetBeginGroup(TRUE);
			addGridCommandControl(*group, CACHEGRID_MENU_RESIZEDEFAULT)->SetStyle(xtpButtonIconAndCaption);
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridFormatRibbonTab_2, _YLOC("Format")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_CELLFORMAT)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_GROUPFORMAT)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_HEADER_FILTER_MENU_EDIT_SETUP)->SetStyle(xtpButtonIconAndCaption);
		}
	}

	{
	 	auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridFormatRibbonTab_3, _YLOC("Column")).c_str());
	 	ASSERT(nullptr != group);
	 	if (nullptr != group)
	 	{
			setGroupImage(*group, 0);

	 		addGridCommandControl(*group, CACHEGRID_MENU_HIDECOLUMN)->SetStyle(xtpButtonIconAndCaption);
	 		addGridCommandControl(*group, CACHEGRID_MENU_UNDOHIDECOLUMN)->SetStyle(xtpButtonIconAndCaption);
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridFormatRibbonTab_4, _YLOC("Freeze")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_FREEZEUPTO)->SetStyle(xtpButtonIconAndCaptionBelow);
			addGridCommandControl(*group, CACHEGRID_MENU_UNFREEZE)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_FREEZECOLUMN)->SetStyle(xtpButtonIconAndCaption);
		}
	}
}

void GridFrameBase::customizeGridGroupRibbonTab(CXTPRibbonTab& tab)
{
	auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGroupRibbonTab_1, _YLOC("Group")).c_str());
	ASSERT(nullptr != group);
	if (nullptr != group)
	{
		setGroupImage(*group, 0);

		addGridCommandControl(*group, CACHEGRID_MENU_GROUPCOLUMN)->SetStyle(xtpButtonIconAndCaptionBelow);
		addGridCommandControl(*group, CACHEGRID_MENU_UNGROUPALL)->SetStyle(xtpButtonIconAndCaptionBelow);

		{
			auto control = addGridPopupButtonControl(*group, CACHEGRID_MENU_SETCOLUMNTOTAL, {});
			control->SetBeginGroup(TRUE);
			control->SetStyle(xtpButtonIconAndCaptionBelow);
			m_SetColumnTotalControls.insert(control);
		}
		{
			auto control = addGridCommandControl(*group, CACHEGRID_MENU_GROUPFORMAT);
			control->SetStyle(xtpButtonIconAndCaptionBelow);
			control->SetBeginGroup(TRUE);
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGroupRibbonTab_2, _YLOC("Category")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			//group->SetControlsGrouping();
			auto control = addGridCommandControl(*group, CACHEGRID_MENU_NEXTGROUPUP);
			control->SetStyle(xtpButtonIcon);
			//control->SetBeginGroup(TRUE);
			addGridCommandControl(*group, CACHEGRID_MENU_NEXTGROUPDOWN)->SetStyle(xtpButtonIcon);

			auto control1 = addGridCommandControl(*group, CACHEGRID_MENU_EXPANDALL);
			control1->SetStyle(xtpButtonIconAndCaption);
			control1->SetBeginGroup(TRUE);
			addGridCommandControl(*group, CACHEGRID_MENU_COLLAPSEALL)->SetStyle(xtpButtonIconAndCaption);
				
			auto control2 = addGridPopupButtonControl(*group, 0,
															{	CACHEGRID_MENU_ALLROWS_EXPANDLEVEL1,
																CACHEGRID_MENU_ALLROWS_EXPANDLEVEL2,
																CACHEGRID_MENU_ALLROWS_EXPANDLEVEL3,
																CACHEGRID_MENU_ALLROWS_EXPANDLEVEL4,
																CACHEGRID_MENU_ALLROWS_EXPANDLEVEL5,
																CACHEGRID_MENU_ALLROWS_EXPANDLEVEL6,
																CACHEGRID_MENU_ALLROWS_EXPANDLEVEL7,
																CACHEGRID_MENU_ALLROWS_EXPANDLEVEL8,
																CACHEGRID_MENU_ALLROWS_EXPANDLEVEL9, });
			control2->SetIconId(RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL_SELECT);
			setImage({ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL_SELECT }, RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL_SELECT, xtpImageNormal);
			setImage({ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL_SELECT }, RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL_SELECT_16X16, xtpImageNormal);
			control2->SetStyle(xtpButtonIconAndCaption);
			control2->SetCaption(_T("Expand up to..."));
			control2->SetTooltip(_T("Expand up to..."));
			control2->SetDescription(_T("Expands all top-level categories by the chosen number of levels."));

			auto control3 = addGridCommandControl(*group, CACHEGRID_MENU_EXPANDLEVEL1);
			control3->SetBeginGroup(TRUE);
			control3->SetStyle(xtpButtonIcon);
			addGridCommandControl(*group, CACHEGRID_MENU_COLLAPSELEVEL1)->SetStyle(xtpButtonIcon);
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGroupRibbonTab_3, _YLOC("Sort By")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_SORTBYCOUNT)->SetStyle(xtpButtonIconAndCaption);
			auto control = addGridPopupButtonControl(*group, CACHEGRID_MENU_SORTBYTOTAL, {});
			control->SetStyle(xtpButtonIconAndCaption);
			m_SortByTotalControls.insert(control);
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGroupRibbonTab_4, _YLOC("Show")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_HIDEGROUPCOUNT)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_HIDEEMTPYGROUPS)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_HIDESINGLEROWGROUPS)->SetStyle(xtpButtonIconAndCaption);
		}
	}
}

void GridFrameBase::customizeGridHierarchyRibbonTab(CXTPRibbonTab& tab)
{
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridHierarchyRibbonTab_1, _YLOC("Category")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_HIDEEMTPYHIERARCHIES)->SetStyle(xtpButtonIconAndCaptionBelow);
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridHierarchyRibbonTab_2, _YLOC("Information")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_SHOWHIERARCHYNUMCHILDRENCOLUMN)->SetStyle(xtpButtonIconAndCaptionBelow);
			addGridCommandControl(*group, CACHEGRID_MENU_SHOWHIERARCHYNUMDESCENDANTSCOLUMN)->SetStyle(xtpButtonIconAndCaptionBelow);
			addGridCommandControl(*group, CACHEGRID_MENU_SHOWHIERARCHYHIDDENDESCCOLUMN)->SetStyle(xtpButtonIconAndCaptionBelow);
			addGridCommandControl(*group, CACHEGRID_MENU_SHOWHIERARCHYVISBLEDESCCOLUMN)->SetStyle(xtpButtonIconAndCaptionBelow);

			auto control = addGridCommandControl(*group, CACHEGRID_MENU_SHOWHIERARCHYPOSITIONCOLUMN);
			control->SetStyle(xtpButtonIconAndCaption);
			control->SetBeginGroup(FALSE);
			addGridCommandControl(*group, CACHEGRID_MENU_SHOWHIERARCHYLEVELCOLUMN)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_SHOWHIERARCHYKEYCOLUMN)->SetStyle(xtpButtonIconAndCaption);
		}
	}
}

void GridFrameBase::customizeGridCellsRibbonTab(CXTPRibbonTab& tab)
{
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridCellsRibbonTab_1, _YLOC("Multivalues")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_MULTIVALUE_EXPLODE)->SetStyle(xtpButtonIconAndCaptionBelow);
			addGridCommandControl(*group, CACHEGRID_MENU_MULTIVALUE_IMPLODE)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_MULTIVALUE_IMPLODE_GRID)->SetStyle(xtpButtonIconAndCaption);
		}
	}
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridCellsRibbonTab_2, _YLOC("Times")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_TIMES_EXPLODE)->SetStyle(xtpButtonIconAndCaptionBelow);
			addGridCommandControl(*group, CACHEGRID_MENU_TIMES_IMPLODE)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_TIME_IMPLODE_GRID)->SetStyle(xtpButtonIconAndCaption);
		}
	}
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridCellsRibbonTab_3, _YLOC("Dates")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_DATES_EXPLODE)->SetStyle(xtpButtonIconAndCaptionBelow);
			addGridCommandControl(*group, CACHEGRID_MENU_DATES_IMPLODE)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_DATE_IMPLODE_GRID)->SetStyle(xtpButtonIconAndCaption);
		}
	}

	// NO meaning in sapio365 - so menu only for now
// 	{
// 		auto group = tab.AddGroup(_TLOC("Convert Text To"));
// 		ASSERT(nullptr != group);
// 		if (nullptr != group)
// 		{
// 			addGridCommandControl(*group, CACHEGRID_MENU_CONVERTTODATE)->SetStyle(xtpButtonIconAndCaption);
// 			addGridCommandControl(*group, CACHEGRID_MENU_REMOVEDATECONVERSION)->SetStyle(xtpButtonIconAndCaption);
// 
// 			{
// 				auto control = addGridCommandControl(*group, CACHEGRID_MENU_CONVERTTONUMBER);
// 				control->SetStyle(xtpButtonIconAndCaption);
// 				control->SetBeginGroup(TRUE);
// 				addGridCommandControl(*group, CACHEGRID_MENU_REMOVENUMBERCONVERSION)->SetStyle(xtpButtonIconAndCaption);
// 			}
// 			{
// 				auto control = addGridCommandControl(*group, CACHEGRID_MENU_REMOVEALLCONVERSIONS);
// 				control->SetStyle(xtpButtonIconAndCaption);
// 				control->SetBeginGroup(TRUE);
// 			}
// 		}
// 	}
}

void GridFrameBase::customizeGridOptionsRibbonTab(CXTPRibbonTab& tab)
{
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridOptionsRibbonTab_2, _YLOC("Show")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_SHOW_TITLES)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_SHOW_TITLES_FOCUSCOL)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_HIGHLIGHTCOLUMNS)->SetStyle(xtpButtonIconAndCaption);
		}
	}
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridOptionsRibbonTab_3, _YLOC("Multivalue Cells")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_DISPLAYMULTIVALUENUMBER)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_SORTMULTIVALUESBYCOUNT)->SetStyle(xtpButtonIconAndCaption);
		}
	}
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridOptionsRibbonTab_4, _YLOC("Advanced")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_SUSPENDUPDATES)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_TRAVELSELMODE)->SetStyle(xtpButtonIconAndCaption);

			if (GetGrid().HasLastQueryColumn())
			{
				auto ctrl = group->Add(xtpControlButton, ID_SHOWLASTQUERYCOLUMN);
				GridFrameBase::setImage({ ID_SHOWLASTQUERYCOLUMN }, IDB_SHOWLASTQUERYCOLUMN, xtpImageNormal);
				GridFrameBase::setImage({ ID_SHOWLASTQUERYCOLUMN }, IDB_SHOWLASTQUERYCOLUMN_16X16, xtpImageNormal);
				setGridControlTooltip(ctrl);
				ctrl->SetStyle(xtpButtonIconAndCaption);
			}
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridOptionsRibbonTab_5, _YLOC("Grid Components")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_SHOWGCVD)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_TOGGLEGROUPANDTOOLS)->SetStyle(xtpButtonIconAndCaption);
			addGridCommandControl(*group, CACHEGRID_MENU_TOGGLESTATUSBAR)->SetStyle(xtpButtonIconAndCaption);
		}
	}

}

void GridFrameBase::customizeGridGCVDRibbonTab(CXTPRibbonTab& tab)
{
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGCVDRibbonTab_1, _YLOC("Grid Manager Layout")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_LAYOUTFAM);
				control->SetStyle(xtpButtonIconAndCaptionBelow);
				setGridControlTooltip(control);
			}
			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_LAYOUTSORT);
				control->SetStyle(xtpButtonIconAndCaptionBelow);
				setGridControlTooltip(control);
			}
			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_LAYOUTCAT);
				control->SetStyle(xtpButtonIconAndCaptionBelow);
				setGridControlTooltip(control);
			}
			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_LAYOUTSTATS);
				control->SetStyle(xtpButtonIconAndCaptionBelow);
				setGridControlTooltip(control);
			}
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGCVDRibbonTab_2, _YLOC("Go To")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_GOTOCOLUMN);
			control->SetStyle(xtpButtonIconAndCaptionBelow);
			setGridControlTooltip(control);
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGCVDRibbonTab_3, _YLOC("Selected Columns")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_COLUMNS_SHOW);
				control->SetStyle(xtpButtonIconAndCaption);
				setGridControlTooltip(control);
			}
			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_COLUMNS_HIDE);
				control->SetStyle(xtpButtonIconAndCaption);
				setGridControlTooltip(control);
			}
			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_EDITROW);
				control->SetStyle(xtpButtonIconAndCaption);
				setGridControlTooltip(control);
			}
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGCVDRibbonTab_4, _YLOC("Resize Selected Columns")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_RESIZEVISIBLE);
				control->SetStyle(xtpButtonIconAndCaptionBelow);
				setGridControlTooltip(control);
			}
			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_RESIZEDATASET);
				control->SetStyle(xtpButtonIconAndCaption);
				control->SetBeginGroup(TRUE);
				setGridControlTooltip(control);
			}
			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_RESIZEHEADER);
				control->SetStyle(xtpButtonIconAndCaption);
				setGridControlTooltip(control);
			}
			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_RESIZEHEADDATA);
				control->SetStyle(xtpButtonIconAndCaption);
				setGridControlTooltip(control);
			}
			{
				auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_RESIZEDEFAULT);
				control->SetStyle(xtpButtonIconAndCaption);
				control->SetBeginGroup(TRUE);
				setGridControlTooltip(control);
			}
		}
	}

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeGridGCVDRibbonTab_5, _YLOC("Grid Manager")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addGridCommandControl(*group, CACHEGRID_MENU_TRIGGERHIDEGCVD/*CACHEGRID_MENU_SHOWGCVD*/)->SetStyle(xtpButtonIconAndCaption);

			// GCVD not detachable anymore
// 			auto control = group->Add(xtpControlButton, ID_CACHEGRID_COLORG_DETACHGCVD/*ID_CACHEGRID_COLORG_PINGCVD*/);
// 			control->SetStyle(xtpButtonIconAndCaption);
// 			setGridControlTooltip(control);
		}
	}

	setImage({ ID_CACHEGRID_COLORG_LAYOUTFAM,
		ID_CACHEGRID_COLORG_LAYOUTSORT,
		ID_CACHEGRID_COLORG_LAYOUTCAT,
		ID_CACHEGRID_COLORG_LAYOUTSTATS,
		ID_CACHEGRID_COLORG_RESIZEVISIBLE,
		ID_CACHEGRID_COLORG_RESIZEDATASET,
		ID_CACHEGRID_COLORG_RESIZEHEADER,
		ID_CACHEGRID_COLORG_RESIZEHEADDATA,
		ID_CACHEGRID_COLORG_RESIZEDEFAULT,
		ID_CACHEGRID_COLORG_GOTOCOLUMN }
	, RPROD_BMP_RIBBON_COLORG_MAIN_ICONS, xtpImageNormal);

	setImage({ ID_CACHEGRID_COLORG_LAYOUTFAM,
		ID_CACHEGRID_COLORG_LAYOUTSORT,
		ID_CACHEGRID_COLORG_LAYOUTCAT,
		ID_CACHEGRID_COLORG_LAYOUTSTATS,
		ID_CACHEGRID_COLORG_RESIZEVISIBLE,
		ID_CACHEGRID_COLORG_RESIZEDATASET,
		ID_CACHEGRID_COLORG_RESIZEHEADER,
		ID_CACHEGRID_COLORG_RESIZEHEADDATA,
		ID_CACHEGRID_COLORG_RESIZEDEFAULT,
		ID_CACHEGRID_COLORG_GOTOCOLUMN }
	, RPROD_BMP_RIBBON_COLORG_MAIN_ICONS_16X16, xtpImageNormal);

	/*setImage({ ID_CACHEGRID_COLORG_PINGCVD }, RPROD_BMP_RIBBON_COLORG_PIN, xtpImageNormal);
	setImage({ ID_CACHEGRID_COLORG_PINGCVD }, RPROD_BMP_RIBBON_COLORG_PIN_16X16, xtpImageNormal);
	setImage({ ID_CACHEGRID_COLORG_PINGCVD }, RPROD_BMP_RIBBON_COLORG_UNPIN, xtpImageChecked);
	setImage({ ID_CACHEGRID_COLORG_PINGCVD }, RPROD_BMP_RIBBON_COLORG_UNPIN_16X16, xtpImageChecked);*/
// 	setImage({ ID_CACHEGRID_COLORG_DETACHGCVD }, RPROD_BMP_RIBBON_COLORG_UNPIN, xtpImageNormal);
// 	setImage({ ID_CACHEGRID_COLORG_DETACHGCVD }, RPROD_BMP_RIBBON_COLORG_UNPIN_16X16, xtpImageNormal);

	setImage({ ID_CACHEGRID_COLORG_COLUMNS_SHOW }, RPROD_BMP_RIBBON_CACHEGRID_MENU_UNDOHIDECOLUMN, xtpImageNormal);
	setImage({ ID_CACHEGRID_COLORG_COLUMNS_SHOW }, RPROD_BMP_RIBBON_CACHEGRID_MENU_UNDOHIDECOLUMN_16X16, xtpImageNormal);
	setImage({ ID_CACHEGRID_COLORG_COLUMNS_HIDE }, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDECOLUMN, xtpImageNormal);
	setImage({ ID_CACHEGRID_COLORG_COLUMNS_HIDE }, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDECOLUMN_16X16, xtpImageNormal);
	setImage({ ID_CACHEGRID_COLORG_EDITROW }, RPROD_BMP_RIBBON_COLORG_EDIT_ROW, xtpImageNormal);
	setImage({ ID_CACHEGRID_COLORG_EDITROW }, RPROD_BMP_RIBBON_COLORG_EDIT_ROW_16X16, xtpImageNormal);

	setImage({ CACHEGRID_MENU_TRIGGERHIDEGCVD }, RPROD_BMP_RIBBON_CACHEGRID_MENU_TRIGGERHIDEGCVD, xtpImageNormal);
	setImage({ CACHEGRID_MENU_TRIGGERHIDEGCVD }, RPROD_BMP_RIBBON_CACHEGRID_MENU_TRIGGERHIDEGCVD_16X16, xtpImageNormal);
}

void GridFrameBase::addNavigationGroup(CXTPRibbonTab& p_Tab)
{
	CXTPRibbonGroup* navigateGroup = p_Tab.GetGroups()->InsertAt(0, _YTEXT(""));

	navigateGroup->SetControlsGrouping(TRUE);

	{
		auto ctrl = navigateGroup->Add(xtpControlSplitButtonPopup, ID_GRIDFRAMEBASE_PREVIOUS);
		GridFrameBase::setImage({ ID_GRIDFRAMEBASE_PREVIOUS }, IDB_NAVIGATE_BACKWARD, xtpImageNormal);
		GridFrameBase::setImage({ ID_GRIDFRAMEBASE_PREVIOUS }, IDB_NAVIGATE_BACKWARD_16X16, xtpImageNormal);
		setControlTexts(ctrl,
			YtriaTranslate::Do(GridFrameBase_addNavigationGroup_1, _YLOC("Back")).c_str(),
			(LPCTSTR)MFCUtil::getFormattedTooltipTitleForRibbon(YtriaTranslate::Do(GridFrameBase_addNavigationGroup_3, _YLOC("Go Back")).c_str(), getAcceleratorString(ID_GRIDFRAMEBASE_PREVIOUS)),
			YtriaTranslate::Do(GridFrameBase_addNavigationGroup_4, _YLOC("Go back to view of origin, from which we opened this view.")).c_str());
		ctrl->SetBeginGroup(TRUE);

		m_NavigatePreviousControls.insert(ctrl);
	}

	{
		auto ctrl = navigateGroup->Add(xtpControlSplitButtonPopup, ID_GRIDFRAMEBASE_NEXT);
		GridFrameBase::setImage({ ID_GRIDFRAMEBASE_NEXT }, IDB_NAVIGATE_FORWARD, xtpImageNormal);
		GridFrameBase::setImage({ ID_GRIDFRAMEBASE_NEXT }, IDB_NAVIGATE_FORWARD_16X16, xtpImageNormal);
		setControlTexts(ctrl,
			YtriaTranslate::Do(GridFrameBase_addNavigationGroup_2, _YLOC("Forward")).c_str(),
			(LPCTSTR)MFCUtil::getFormattedTooltipTitleForRibbon(YtriaTranslate::Do(GridFrameBase_addNavigationGroup_5, _YLOC("Go Forward")).c_str(), getAcceleratorString(ID_GRIDFRAMEBASE_NEXT)),
			YtriaTranslate::Do(GridFrameBase_addNavigationGroup_6, _YLOC("Go forward to view previously opened from the current view.")).c_str());

		m_NavigateNextControls.insert(ctrl);
	}

	navigateGroup->SetControlsGrouping(FALSE);
}

void GridFrameBase::setSessionTabTitle()
{
	if (nullptr != m_infoRibbonTab)
	{
		const auto title = YtriaTranslate::Do(GridFrameBase_setSessionTabTitle_1, _YLOC("Session"));
		if (IsConnected())
		{
			if (GetSapio365Session()->IsUseRoleDelegation())
				m_infoRibbonTab->SetCaption((_YTEXT("\xe1cb ") + title).c_str());
			else
				m_infoRibbonTab->SetCaption((_YTEXT("\x2714 ") + title).c_str());
		}
		else
		{
			m_infoRibbonTab->SetCaption((_YTEXT("\x274c ") + title).c_str());
		}
	}
}

void GridFrameBase::customizeInfoRibbonTab(CXTPRibbonTab& tab)
{
	addSessionGroupToRibbonTab(tab, true);

	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_1, _YLOC("Data")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			auto refreshLabel = group->Add(xtpControlLabel, ID_REFRESH_TIME_LABEL);
			refreshLabel->SetBeginGroup(TRUE);
			refreshLabel->SetCaption(YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_5, _YLOC("Last full refresh:")).c_str());

			m_RefreshTimeControl = group->Add(xtpControlLabel, ID_REFRESH_TIME);

			// Same tooltip for both so that tooltip trigger area is bigger
			refreshLabel->SetTooltip(YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_6, _YLOC("This time is updated on each 'Refresh All'.")).c_str());
			m_RefreshTimeControl->SetTooltip(YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_7, _YLOC("This time is updated on each 'Refresh All'.")).c_str());

			updateTimeRefreshControl();

			if (m_CreateRefresh)
			{
				auto refreshControl = group->Add(xtpControlButton, ID_365REFRESH);
				setRefreshControlTexts(refreshControl);
			}

			if (m_CreateShowContext)
			{
				m_DataContextLabelControl1 = group->Add(xtpControlLabel, ID_DATA_CONTEXT_LABEL1);
				m_DataContextLabelControl1->SetBeginGroup(TRUE);
				m_DataContextLabelControl1->SetCaption(YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_8, _YLOC("Global - no selection")).c_str());
				if (m_CreateAddRemoveToSelection)
					m_DataContextLabelControl2 = group->Add(xtpControlLabel, ID_DATA_CONTEXT_LABEL2);
				else
					group->Add(xtpControlLabel, ID_DATA_CONTEXT_LABEL2); // Dummy label to keep Add, Remove and Manage buttons to the right.
				group->Add(xtpControlLabel, ID_DATA_CONTEXT_LABEL3); // Dummy label to keep Add, Remove and Manage buttons to the right.

				auto showContextControl = group->Add(xtpControlButton, ID_SHOW_DATA_CONTEXT);
				// showContextControl->SetBeginGroup(TRUE);
				setImage({ ID_SHOW_DATA_CONTEXT }, IDB_SEE_SELECTION, xtpImageNormal);
				setImage({ ID_SHOW_DATA_CONTEXT }, IDB_SEE_SELECTION_16X16, xtpImageNormal);
				setControlTexts(
					showContextControl,
					YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_14, _YLOC("Manage")).c_str(),
					(LPCTSTR)MFCUtil::getFormattedTooltipTitleForRibbon(YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_10, _YLOC("See and Manage your Current Selection")).c_str(), getAcceleratorString(ID_SHOW_DATA_CONTEXT)),
					YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_11, _YLOC("See the list of entries displayed in the grid (name and ID).\nAdd or remove any entry.")).c_str());
				showContextControl->SetStyle(xtpButtonIconAndCaption); // Force small icon
			}

            if (m_CreateAddRemoveToSelection)
            {
                auto addToSelCtrl = group->Add(xtpControlButton, ID_ADD_TO_SELECTION);
                setImage({ ID_ADD_TO_SELECTION }, IDB_ADD_TO_SELECTION, xtpImageNormal);
                setImage({ ID_ADD_TO_SELECTION }, IDB_ADD_TO_SELECTION_16X16, xtpImageNormal);
                setControlTexts(
					addToSelCtrl,
                    YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_15, _YLOC("Add")).c_str(),
                    (LPCTSTR)MFCUtil::getFormattedTooltipTitleForRibbon(YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_16, _YLOC("Add To Selection")).c_str(), getAcceleratorString(ID_ADD_TO_SELECTION)),
                    YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_17, _YLOC("Add To Selection")).c_str());
                addToSelCtrl->SetStyle(xtpButtonIconAndCaption); // Force small icon

                auto removeFromSelCtrl = group->Add(xtpControlButton, ID_REMOVE_FROM_SELECTION);
                setImage({ ID_REMOVE_FROM_SELECTION }, IDB_REMOVE_FROM_SELECTION, xtpImageNormal);
                setImage({ ID_REMOVE_FROM_SELECTION }, IDB_REMOVE_FROM_SELECTION_16X16, xtpImageNormal);
                setControlTexts(
					removeFromSelCtrl,
                    YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_18, _YLOC("Remove")).c_str(),
                    (LPCTSTR)MFCUtil::getFormattedTooltipTitleForRibbon(YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_19, _YLOC("Remove From Selection")).c_str(), getAcceleratorString(ID_REMOVE_FROM_SELECTION)),
                    YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_20, _YLOC("Remove From Selection")).c_str());
                removeFromSelCtrl->SetStyle(xtpButtonIconAndCaption); // Force small icon
            }
		}
	}

	{
		auto s = GetSapio365Session();
		//if (s->IsUseRoleDelegation()) // Show those button even if they'll always be disabled when no Role Del (for advertising purpose)
		{
			auto roleInfoGroup = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_21, _YLOC("Current Role")).c_str());
			ASSERT(nullptr != roleInfoGroup);
			if (nullptr != roleInfoGroup)
			{
				auto ctrl = roleInfoGroup->Add(xtpControlButton, ID_SESSION_SEE_ROLEINFO);
				setImage({ ID_SESSION_SEE_ROLEINFO }, IDB_SESSION_SEE_ROLEINFO, xtpImageNormal);
				setImage({ ID_SESSION_SEE_ROLEINFO }, IDB_SESSION_SEE_ROLEINFO_16X16, xtpImageNormal);
				setControlTexts(ctrl,
								YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_22, _YLOC("Role Info")).c_str(),
								YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_23, _YLOC("Current Role Info")).c_str(),
								YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_24, _YLOC("Show details of the current Role.")).c_str());

				if (m_AddRBACHideOutOfScope)
				{
					m_RBACHideOutOfScopeBTN = roleInfoGroup->Add(xtpControlButton, ID_RBAC_HIDE_OUTOFSCOPE);
					ASSERT(nullptr != m_RBACHideOutOfScopeBTN);
					setImage({ ID_RBAC_HIDE_OUTOFSCOPE }, IDB_RIBBON_ROLEDELEGATION_SEEMYSCOPEONLY_32, xtpImageNormal);
					setImage({ ID_RBAC_HIDE_OUTOFSCOPE }, IDB_RIBBON_ROLEDELEGATION_SEEMYSCOPEONLY_16, xtpImageNormal);
					setControlTexts(m_RBACHideOutOfScopeBTN,
						YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_25, _YLOC("Scope Display")).c_str(),
						YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_26, _YLOC("Toggle Scope Display")).c_str(),
						YtriaTranslate::Do(GridFrameBase_customizeInfoRibbonTab_27, _YLOC("This button will toggle the display of all items - even those outside the scope of the current role. These will appear in grey.")).c_str());

					m_RBACHideOutOfScope = s && s->IsRBACHideUnscopedObjects();
					if (nullptr != m_RBACHideOutOfScopeBTN)
					{
						m_RBACHideOutOfScopeBTN->SetEnabled(!AutomatedApp::IsAutomationRunning() && s && s->IsUseRoleDelegation());
						m_RBACHideOutOfScopeBTN->SetChecked(s && (AutomatedApp::IsAutomationRunning() || m_RBACHideOutOfScope));
					}
				}
			}
		}
	}
}

void GridFrameBase::customizeWindowsRibbonTab(CXTPRibbonTab& tab)
{
	{
		auto group = tab.AddGroup(YtriaTranslate::Do(GridFrameBase_customizeWindowsRibbonTab_1, _YLOC("Quick access to other windows")).c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			group->SetID(ID_WINDOWS_TAB);

			{
				auto mainWindowControl = group->Add(xtpControlButton, ID_MAIN_WINDOW_QUICKACCESS);
				setImage({ ID_MAIN_WINDOW_QUICKACCESS }, IDB_MAIN_WINDOW_QUICKACCESS, xtpImageNormal);
				setImage({ ID_MAIN_WINDOW_QUICKACCESS }, IDB_MAIN_WINDOW_QUICKACCESS_16X16, xtpImageNormal);
				setControlTexts(mainWindowControl,
					YtriaTranslate::Do(GridFrameBase_customizeWindowsRibbonTab_2, _YLOC("Main Window")).c_str(),
					(LPCTSTR)MFCUtil::getFormattedTooltipTitleForRibbon(YtriaTranslate::Do(GridFrameBase_customizeWindowsRibbonTab_3, _YLOC("Goto Main Window")).c_str(), getAcceleratorString(ID_MAIN_WINDOW_QUICKACCESS)),
					YtriaTranslate::Do(GridFrameBase_customizeWindowsRibbonTab_4, _YLOC("Bring the main window to front.")).c_str());
			}

			CXTPControlGallery* gallery = dynamic_cast<CXTPControlGallery*>(group->Add(xtpControlGallery, ID_WINDOWS_QUICKACCESS));
			ASSERT(gallery != nullptr && gallery->GetID() == ID_WINDOWS_QUICKACCESS);
			if (nullptr != gallery)
			{
				// Doesn't work...
				//gallery->SetFlags(gallery->GetFlags() | xtpFlagControlStretched);
				gallery->SetItems(CXTPControlGalleryItems::CreateItems(GetCommandBars(), ID_WINDOWS_QUICKACCESS));
			}

			UpdateWindowListGallery(gallery);
		}
	}
}

//void GridFrameBase::OnSearchEdit(NMHDR* notifyStruct, LRESULT* result)
//{
//
//}
//
//void GridFrameBase::OnUpdateSearchEdit(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable();
//}

void GridFrameBase::OnUpdateFiltersList(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
}

void GridFrameBase::updateConnectedControl(CXTPControl& connectedControl)
{
	CXTPControl* logoutControl = nullptr;
	CXTPControl* relogControl = nullptr;
	auto group = getSessionGroup();
	if (nullptr != group)
	{
		logoutControl	= group->FindControl(ID_LOGOUT);
		relogControl	= group->FindControl(ID_RELOG);
	}

	const auto disconnectedSession = !IsConnected();
    const wstring connectedStatus = disconnectedSession
		? YtriaTranslate::Do(GridFrameBase_updateConnectedControl_1, _YLOC("Disconnected")).c_str()
		: YtriaTranslate::Do(GridFrameBase_updateConnectedControl_2, _YLOC("Connected")).c_str();
	connectedControl.SetCaption(connectedStatus.c_str());

	if (disconnectedSession)
	{
		if (nullptr != relogControl)
		{
			ASSERT(nullptr != logoutControl);
			if (nullptr != logoutControl)
				logoutControl->SetVisible(FALSE);
			relogControl->SetVisible(!IsSnapshotMode() || m_SnapshotSession);
		}
	}
	else
	{
		if (nullptr != relogControl)
		{
			ASSERT(nullptr != logoutControl);
			if (nullptr != logoutControl)
				logoutControl->SetVisible(TRUE);
			relogControl->SetVisible(FALSE);
		}
	}
}

void GridFrameBase::updateTimeRefreshControl()
{
	ASSERT(nullptr != m_RefreshTimeControl);
	if (nullptr != m_RefreshTimeControl)
	{
		CString dateStr;
		DateTimeFormat format;
		format.m_TimeFormat.m_DisplayFlags = TimeFormat::HOURS | TimeFormat::MINUTES | TimeFormat::SECONDS;
		TimeUtil::GetInstance().ConvertDateToText(format, m_RefreshDate, dateStr);
		m_RefreshTimeControl->SetCaption(dateStr);
	}
}

//void GridFrameBase::OnUpdateFreezeHeader(CCmdUI* pCmdUI)
//{
//	pCmdUI->Enable(FALSE);
//	pCmdUI->SetText(_TLOC("Freeze Up To"));
//}

//void GridFrameBase::OnFreezeUpTo(NMHDR* pNMHDR, LRESULT* pResult)
//{
//	*pResult = FALSE;
//
//	NMXTPCONTROL* tagNMCONTROL = (NMXTPCONTROL*)pNMHDR;
//	if (nullptr != tagNMCONTROL->pControl)
//	{
//		if (ExecuteFreezeUpTo(*tagNMCONTROL->pControl))
//			*pResult = TRUE; // Handled
//	}
//}
//
//bool GridFrameBase::ExecuteFreezeUpTo(CXTPControl& control)
//{
//	const int pos = control.GetIndex();
//	ASSERT(pos >= 0);
//	if (pos >= 0)
//	{
//		GetGrid().GotoColumn(GetGrid().GetBackend().GetColumnByPos(pos/* - 1*/));
//		GetGrid().SendMessage(WM_COMMAND, MAKELONG(CACHEGRID_MENU_FREEZEUPTO, 0), NULL);
//		return true;
//	}
//
//	return false;
//}

CXTPControl* GridFrameBase::addGridCommandControl(CXTPRibbonGroup& group, UINT commandID, XTPControlType controlType/* = xtpControlButton*/)
{
	if (0 != commandID)
	{
		setImage({ commandID }, getGridCommandImageID(commandID, xtpImageNormal, CommandImageSize::PX32), xtpImageNormal);
		setImage({ commandID }, getGridCommandImageID(commandID, xtpImageNormal, CommandImageSize::PX16), xtpImageNormal);
		setImage({ commandID }, getGridCommandImageID(commandID, xtpImageChecked, CommandImageSize::PX32), xtpImageChecked);
		setImage({ commandID }, getGridCommandImageID(commandID, xtpImageChecked, CommandImageSize::PX16), xtpImageChecked);
	}

	auto ctrl = group.Add(controlType, commandID);
	ASSERT(nullptr != ctrl);

	// The grid has been created and has normally set the tooltips already.
	setGridControlTooltip(ctrl);

	return ctrl;
}

CXTPControl* GridFrameBase::addGridSplitButtonControl(CXTPRibbonGroup& group, UINT commandID, const vector<UINT> dropDownCommandIDs)
{
	return addGridPopupButtonControl(group, commandID, dropDownCommandIDs, true);
}

CXTPControl* GridFrameBase::addGridPopupButtonControl(CXTPRibbonGroup& group, UINT commandID, const vector<UINT> dropDownCommandIDs, bool isSplit/* = false*/)
{
	auto button = addGridCommandControl(group, commandID, isSplit ? xtpControlSplitButtonPopup : xtpControlButtonPopup);

	CXTPControlPopup* popup = dynamic_cast<CXTPControlPopup*>(button);

	bool insertSeparator = false;
	for (auto command : dropDownCommandIDs)
	{
		if (command == ID_SEPARATOR)
		{
			insertSeparator = true;
			continue;
		}

		auto ctrl = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, command);
		auto accel = GetGrid().GetAccelerator(command);
		if (accel.IsEmpty() && gridCommandsDuplicatesForRibbon.end() !=  gridCommandsDuplicatesForRibbon.find(command))
			accel = GetGrid().GetAccelerator(gridCommandsDuplicatesForRibbon.at(command));
		ctrl->SetShortcutText(accel);
		if (insertSeparator)
		{
			ctrl->SetBeginGroup(TRUE);
			insertSeparator = false;
		}

		setImage({ command }, getGridCommandImageID(command, xtpImageNormal, CommandImageSize::PX32), xtpImageNormal);
		setImage({ command }, getGridCommandImageID(command, xtpImageNormal, CommandImageSize::PX16), xtpImageNormal);
		setImage({ command }, getGridCommandImageID(command, xtpImageChecked, CommandImageSize::PX32), xtpImageChecked);
		setImage({ command }, getGridCommandImageID(command, xtpImageChecked, CommandImageSize::PX16), xtpImageChecked);
	}

	return button;
}

UINT GridFrameBase::getGridCommandImageID(UINT command, XTPImageState xTPImageState, const CommandImageSize size)
{
	const std::map<UINT, GridCommandImages>& images = CommandImageSize::PX32 == size ? g_GridCommandImages32x32 : g_GridCommandImages16x16;

	auto it = images.find(command);
	if (images.end() == it)
	{
		if (gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(command))
		{
			it = images.find(gridCommandsDuplicatesForRibbon.at(command));
			if (images.end() == it)
				return 0;
		}
		else
		{
			return 0;
		}
	}

	if (xtpImageChecked == xTPImageState)
		return it->second.m_Checked;
	return it->second.m_Normal;
}

CXTPControl* GridFrameBase::addControlTo(const O365ControlConfig& p_Config, CXTPRibbonGroup& p_Group, UINT p_IdForAccelerator)
{
	CXTPControl* ctrl = nullptr;
	ASSERT(p_Config.m_ControlID != 0);
	if (p_Config.m_ControlID != 0)
	{
		ctrl = p_Group.Add(xtpControlButton, p_Config.m_ControlID);

		setImage({ p_Config.m_ControlID }, p_Config.m_Image16x16ResourceID, xtpImageNormal);
		setImage({ p_Config.m_ControlID }, p_Config.m_Image32x32ResourceID, xtpImageNormal);

		setControlTexts(
			ctrl,
			p_Config.m_ControlText,
			(LPCTSTR)MFCUtil::getFormattedTooltipTitleForRibbon(p_Config.m_ControlText.c_str(), getAcceleratorString(p_IdForAccelerator)),
			p_Config.m_ControlDescription.c_str());
	}
	return ctrl;
}

uint32_t GridFrameBase::getAlreadyChargedTokenAmount() const
{
	return m_AlreadyChargedTokenAmount;
}

void GridFrameBase::setAlreadyChargedTokenAmount(uint32_t p_Tokens)
{
	m_AlreadyChargedTokenAmount = p_Tokens;
}

void GridFrameBase::UpdateMenuSortByTotal()
{
	ASSERT(!m_SortByTotalControls.empty());
	const auto& summaryToobarItems = GetGrid().GetSummaryToolbarItems();
	if (summaryToobarItems != m_PreviousSummaryToobarItems)
	{
		for (auto control : m_SortByTotalControls)
		{
			ASSERT(nullptr != control);
			if (nullptr != control)
			{
				CXTPControlPopup* sortByTotalControlPopup = dynamic_cast<CXTPControlPopup*>(control);
				ASSERT(nullptr != sortByTotalControlPopup);
				if (nullptr != sortByTotalControlPopup)
				{
					auto controls = sortByTotalControlPopup->GetCommandBar()->GetControls();
					controls->RemoveAll();
					for (auto& id : summaryToobarItems)
						controls->Add(xtpControlButton, id);
				}
			}
		}

		m_PreviousSummaryToobarItems = summaryToobarItems;
	}
}

void GridFrameBase::UpdateMenuSetColumnTotal()
{
	ASSERT(!m_SetColumnTotalControls.empty());
	if (!m_SetColumnTotalControls.empty())
	{
		const auto& columnSummaryToobarItems = GetGrid().GetColumnSummaryToolbarItems();
		if (columnSummaryToobarItems != m_PreviousColumnSummaryToolbarItems)
		{
			for (auto control : m_SetColumnTotalControls)
			{
				CXTPControlPopup* setColumnTotalControlPopup = dynamic_cast<CXTPControlPopup*>(control);
				ASSERT(nullptr != setColumnTotalControlPopup);
				if (nullptr != setColumnTotalControlPopup)
				{
					auto controls = setColumnTotalControlPopup->GetCommandBar()->GetControls();
					controls->RemoveAll();
					for (auto& id : columnSummaryToobarItems)
						controls->Add(xtpControlButton, id);
				}
			}
		}

		m_PreviousColumnSummaryToolbarItems = columnSummaryToobarItems;
	}
}

void GridFrameBase::UpdateHierarchyFilterGallery(CXTPControlGallery* p_pGallery)
{
	ASSERT(p_pGallery != nullptr && p_pGallery->GetID() == ID_HIERARCHY_FILTERS);
	if (nullptr != p_pGallery)
	{
		auto items = p_pGallery->GetItems();

		auto& commands = GetGrid().GetHierarchyFilterObjectTypeCommands();

		if (m_PreviousHierachyFilterItems != commands)
		{
			auto imManag = items->GetImageManager();
			ASSERT(nullptr != imManag);
			if (nullptr != imManag)
				imManag->SetMaskColor(GridBackendUtil::g_rgbTransparentColor);

			items->RemoveAll();

			for (auto c : commands)
			{
				if (nullptr != imManag)
				{
					const auto& setup = GetGrid().GetHierarchyFilterObjectTypeCommandSetup(c);
					if (FALSE == imManag->Lookup(setup.m_IconResourceIndex))
					{
						UINT cmd[]{ (UINT)setup.m_IconResourceIndex };
						imManag->SetIcons(setup.m_IconResourceIndex, cmd, 1, CSize(0, 0), xtpImageNormal);

						// Handle grayed out icon
						{

							auto icon = MFCUtil::getCExtCmdIconFromImageResourceID(setup.m_IconResourceIndex, false);

							// Convert to gray scale and lighten a bit (+25%)
							MFCUtil::ConvertToGrayscale(icon.m_bmpNormal, 1.25);

							CXTPImageManagerIconHandle iconHandle(icon.GetBitmap().CreateBitmap(false), FALSE);
							imManag->SetIcon(iconHandle, GetGrayedHierarchyFilterTypeIconID(static_cast<UINT>(setup.m_IconResourceIndex)), CSize(0, 0), xtpImageNormal);
						}
					}
				}

				auto pItem = items->AddItem(L"", c, -1);
			}

			m_PreviousHierachyFilterItems = commands;
		}

		const auto& filterTypes = GetGrid().GetHierarchyObjectTypeToFilterOn();
		for (int i = 0; i < items->GetItemCount(); ++i)
		{
			auto		item	= items->GetItem(i);
			const auto&	setup	= GetGrid().GetHierarchyFilterObjectTypeCommandSetup(item->GetID());

			const bool	enabled = filterTypes.end() != filterTypes.find(setup.m_TypeIndex);
			const int	icon	= enabled ? setup.m_IconResourceIndex : GetGrayedHierarchyFilterTypeIconID(static_cast<UINT>(setup.m_IconResourceIndex));
			item->SetImageIndex(icon);

			CString caption;
			if (enabled)
				caption.Format(L"\u2261 %s \u2261", setup.m_TypeDisplayName.c_str());
			else
				caption.Format(L"    %s", setup.m_TypeDisplayName.c_str());
			item->SetCaption(caption);
		}
	}

	p_pGallery->SetControlSize(CSize(100, 70));
	p_pGallery->InvalidateItems();
}

void GridFrameBase::UpdateHierarchyFilterGallery()
{
	if (nullptr != m_sortFilterRibbonTab)
	{
		CXTPRibbonGroup*  pGroup = m_sortFilterRibbonTab->FindGroup(ID_HIERARCHY_TAB);
		if (nullptr != pGroup)
		{
			CXTPControlGallery* pGallery = dynamic_cast<CXTPControlGallery*>(pGroup->FindControl(ID_HIERARCHY_FILTERS));
			if (nullptr != pGallery)
				UpdateHierarchyFilterGallery(pGallery);
		}
	}
}

void GridFrameBase::UpdateWindowListGallery(CXTPControlGallery* p_pGallery)
{
	ASSERT(nullptr != p_pGallery);
	if (nullptr != p_pGallery)
	{
		auto items = p_pGallery->GetItems();
		ASSERT(nullptr != items);
		if (nullptr != items)
		{
			items->RemoveAll();

			// First, give access to the main window.
			/*CXTPControlGalleryItem* pItem = items->AddItem(_TLOC("Main Window"), -1);
			ASSERT(nullptr != pItem);
			if (nullptr != pItem)
				pItem->SetData((DWORD_PTR)AfxGetApp()->m_pMainWnd->GetSafeHwnd());*/

			// TODO: Set gallery size to the size of the longest text.
			// 	int LongestText = 0;
			// 	CFont ThisFont;
			// 	MFCUtil::getDefaultFontForUNICODE(ThisFont, GetDC());

			// Now all the other frames.
			for (const auto& byUser : CommandDispatcher::GetInstance().GetAllModules())
			{
				const auto& user = byUser.first;
				for (const auto& byTarget : byUser.second)
				{
					const std::unordered_set<GridFrameBase*>& moduleFrames = byTarget.second->GetGridFrames();
					for (const auto& frame : moduleFrames)
					{
						ASSERT(nullptr != frame && ::IsWindow(frame->GetSafeHwnd()));

						// No point in including this frame in the Window Quick Access List.
						// frame->IsWindowVisible() => Temporary way to handle new back/forward system. We should maybe do it differently.
						if (frame != this && ::IsWindow(frame->GetSafeHwnd()) && frame->IsWindowVisible())
						{
							const wstring ItemTitle(frame->GetWindowsListQuickAccessTitle());

							// TODO: Set gallery size to the size of the longest text.
							// 	int Temp = MFCUtil::GetTextSize(ItemTitle, ThisFont, this).cx;
							// 	if (Temp > LongestText)
							// 		LongestText = Temp;

							auto pItem = items->AddItem(ItemTitle.c_str(), -1);
							pItem->SetToolTip(ItemTitle.c_str());
							ASSERT(nullptr != pItem);
							if (nullptr != pItem)
								pItem->SetData((DWORD_PTR)frame->GetSafeHwnd());
						}
					}
				}
			}

			// TODO: Set gallery size to the size of the longest text.
			// p_pGallery->SetControlSize(CSize(LongestText == 0 ? 460 : LongestText, 70));

			p_pGallery->SetControlSize(CSize(460, 70));
			p_pGallery->InvalidateItems();
		}
	}
}

void GridFrameBase::customizeRibbonBar(CXTPRibbonBar& ribbonBar)
{
	// Override and customize your ribbon here.
}

bool GridFrameBase::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
    CreateDefaultGroups(tab, p_ShowLinkTabUser, p_ShowLinkTabGroup);
	return true;
}

void GridFrameBase::createFrameSpecificTabs()
{
	// .|..
}

void GridFrameBase::CreateDefaultGroups(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
    CreateViewDataEditGroups(tab);
    CreateLinkGroups(tab, p_ShowLinkTabUser, p_ShowLinkTabGroup);
}

void GridFrameBase::CreateLinkGroups(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup, bool p_ShowLinkTabSite/* = false*/)
{
    if (p_ShowLinkTabUser)
        CreateUserInfoGroup(tab);

    if (p_ShowLinkTabGroup)
        CreateGroupInfoGroup(tab);

	if (p_ShowLinkTabSite)
		CreateSiteInfoGroup(tab);
}

void GridFrameBase::CreateViewDataEditGroups(CXTPRibbonTab& tab)
{
    CreateViewGroup(tab);

	if (m_CreateRefresh)
		CreateRefreshGroup(tab);

    CreateEditGroup(tab);
}

CXTPRibbonGroup* GridFrameBase::CreateViewGroup(CXTPRibbonTab& p_Tab)
{
	const auto numTechCols		= GetGrid().GetColumnsByPresets({ CacheGrid::g_ColumnsPresetTech }).size();
	const auto numDefaultCols	= GetGrid().GetColumnsByPresets({ O365Grid::g_ColumnsPresetDefault }).size();

	size_t numCols = 0;
	for (auto col : GetGrid().GetBackend().GetColumns())
		numCols += col->IsLocked() || GetGrid().GetBackend().IsHierarchyColumn(col) ? 0 : 1;

	const bool needsAllAndDefault = (numDefaultCols > 0 && (numCols - numTechCols > numDefaultCols));
	const bool needsAdditionalData = GetGrid().GetAutomationName() == GridUtil::g_AutoNameUsers || GetGrid().GetAutomationName() == GridUtil::g_AutoNameGroups;

	CXTPRibbonGroup* viewGroup = p_Tab.AddGroup(g_ViewGroup.c_str());
	if (needsAllAndDefault || needsAdditionalData)
	{
		vector<UINT> dropDownCommands{ CACHEGRID_MENU_SHOWCOLUMNINFO };

		if (needsAllAndDefault)
		{
			GridFrameBase::setImage({ ID_SHOWCOLUMNS_ALL }, IDB_SHOWCOLUMNS_ALL, xtpImageNormal);
			GridFrameBase::setImage({ ID_SHOWCOLUMNS_ALL }, IDB_SHOWCOLUMNS_ALL_16X16, xtpImageNormal);
			GridFrameBase::setImage({ ID_SHOWCOLUMNS_DEFAULT }, IDB_SHOWCOLUMNS_DEFAULT, xtpImageNormal);
			GridFrameBase::setImage({ ID_SHOWCOLUMNS_DEFAULT }, IDB_SHOWCOLUMNS_DEFAULT_16X16, xtpImageNormal);

			dropDownCommands.insert(dropDownCommands.end(), { ID_SHOWCOLUMNS_DEFAULT, ID_SHOWCOLUMNS_ALL });
		}

		if (needsAdditionalData)
		{
			GridFrameBase::setImage({ ID_ADDITIONAL_DATA_CONFIG }, IDB_ADDITIONAL_DATA_CONFIG, xtpImageNormal);
			GridFrameBase::setImage({ ID_ADDITIONAL_DATA_CONFIG }, IDB_ADDITIONAL_DATA_CONFIG_16X16, xtpImageNormal);
			dropDownCommands.push_back(ID_SEPARATOR);
			dropDownCommands.push_back(ID_ADDITIONAL_DATA_CONFIG);
		}

		ASSERT(dropDownCommands.size() > 1);

		auto ctrl = addGridSplitButtonControl(*viewGroup, CACHEGRID_MENU_SHOWCOLUMNINFO, dropDownCommands);
		//ctrl->SetStyle(xtpButtonIconAndCaptionBelow);
		//ctrl->SetBeginGroup(TRUE);
		setGridControlTooltip(ctrl);
	}
	else
	{
		auto ctrl = viewGroup->Add(xtpControlButton, CACHEGRID_MENU_SHOWCOLUMNINFO);
		setGridControlTooltip(ctrl);
	}

	GridFrameBase::setImage({ CACHEGRID_MENU_SHOWCOLUMNINFO }, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWCOLUMNINFO, xtpImageNormal);
	GridFrameBase::setImage({ CACHEGRID_MENU_SHOWCOLUMNINFO }, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWCOLUMNINFO_16X16, xtpImageNormal);

	if (GetGrid().GetSnapshotCapabilities().HasPhoto())
	{
		{
			auto ctrl = viewGroup->Add(xtpControlButton, ID_CREATE_GRID_PHOTO_SNAPSHOT);
			GridFrameBase::setImage({ ID_CREATE_GRID_PHOTO_SNAPSHOT }, IDB_GRID_CREATE_PHOTO_SNAPSHOT, xtpImageNormal);
			GridFrameBase::setImage({ ID_CREATE_GRID_PHOTO_SNAPSHOT }, IDB_GRID_CREATE_PHOTO_SNAPSHOT_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	if (GetGrid().GetSnapshotCapabilities().HasRestore())
	{
		{
			auto ctrl = viewGroup->Add(xtpControlButton, ID_CREATE_GRID_RESTORE_SNAPSHOT);
			GridFrameBase::setImage({ ID_CREATE_GRID_RESTORE_SNAPSHOT }, IDB_GRID_CREATE_RESTORE_SNAPSHOT, xtpImageNormal);
			GridFrameBase::setImage({ ID_CREATE_GRID_RESTORE_SNAPSHOT }, IDB_GRID_CREATE_RESTORE_SNAPSHOT_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	addGridConfigControl(*viewGroup);

	return viewGroup;
}

CXTPRibbonGroup* GridFrameBase::CreateRefreshGroup(CXTPRibbonTab& p_Tab)
{
	ASSERT(m_CreateRefresh);

    CXTPRibbonGroup* pRefreshGroup = p_Tab.AddGroup(g_ActionsDataGroup.c_str());
	setGroupImage(*pRefreshGroup, 0);

	if (GetGrid().WithLoadMore())
		pRefreshGroup->Add(xtpControlSplitButtonPopup, ID_365REFRESH);
	else
		pRefreshGroup->Add(xtpControlButton, ID_365REFRESH);

    setImage({ ID_365REFRESH }, IDB_REFRESH, xtpImageNormal);
    setImage({ ID_365REFRESH }, IDB_REFRESH_16X16, xtpImageNormal);

	UpdateRefreshControls();

	if (m_CreateRefreshPartial && nullptr != pRefreshGroup)
		addControlTo(GetRefreshPartialControlConfig(), *pRefreshGroup, ID_365REFRESHSELECTED);

    return pRefreshGroup;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetRefreshPartialControlConfig()
{
	ASSERT(m_CreateRefreshPartial);

	// Implement in child class if using partial refresh.
	ASSERT(FALSE);

	O365ControlConfig config;
	//config.m_ControlID;
	//config.m_Image16x16ResourceID;
	//config.m_Image32x32ResourceID;
	//config.m_ControlText;
	//config.m_ControlTooltip;
	//config.m_ControlDescription;
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetApplyPartialControlConfig()
{
	O365ControlConfig config;
	config.m_ControlID = ID_APPLYSELECTED;
	config.m_Image16x16ResourceID = IDB_APPLY_SELECTED;
	config.m_Image32x32ResourceID = IDB_APPLY_SELECTED_16X16;
	config.m_ControlText = YtriaTranslate::Do(GridFrameBase_AddO365ApplySelectedControl_1, _YLOC("Save Selected")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(GridFrameBase_AddO365ApplySelectedControl_2, _YLOC("Save Selected")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(GridFrameBase_AddO365ApplySelectedControl_3, _YLOC("Save Selected Changes")).c_str();
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetRevertPartialControlConfig()
{
	O365ControlConfig config;
	config.m_ControlID = ID_365REVERTSELECTED;
	config.m_Image16x16ResourceID = IDB_REVERT_SELECTED;
	config.m_Image32x32ResourceID = IDB_REVERT_SELECTED_16X16;
	config.m_ControlText = YtriaTranslate::Do(GridFrameBase_AddO365RevertSelectedControl_1, _YLOC("Selected Rows")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(GridFrameBase_AddO365RevertSelectedControl_2, _YLOC("Undo on Selected Rows")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(GridFrameBase_AddO365RevertSelectedControl_3, _YLOC("Undo any selected changes currently pending in the grid.\nTip: Grouping by the status column is a quick way to check for all pending changes.\nNote: This button will NOT undo changes that have already been saved to the server.")).c_str();
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetRefreshPartialControlConfigGroups()
{
	O365ControlConfig config;
	config.m_ControlID = ID_365REFRESHSELECTED_GROUPS;
	config.m_Image16x16ResourceID = IDB_REFRESHSELECTED_GROUPS;
	config.m_Image32x32ResourceID = IDB_REFRESHSELECTED_GROUPS_16X16;
	config.m_ControlText = YtriaTranslate::Do(FrameChannels_CreateRefreshPartial_1, _YLOC("Selected Groups")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(FrameChannels_CreateRefreshPartial_2, _YLOC("Refresh Selected Groups Content")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(FrameChannels_CreateRefreshPartial_3, _YLOC("This will refresh the data in the grid for all currently selected groups.")).c_str();
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetRefreshPartialControlConfigLists()
{
	O365ControlConfig config;
	config.m_ControlID = ID_365REFRESHSELECTED_LISTS;
	config.m_Image16x16ResourceID = IDB_REFRESHSELECTED_LISTS;
	config.m_Image32x32ResourceID = IDB_REFRESHSELECTED_LISTS_16X16;
	config.m_ControlText = YtriaTranslate::Do(FrameColumns_CreateRefreshPartial_1, _YLOC("Selected Lists")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(FrameColumns_CreateRefreshPartial_2, _YLOC("Refresh Selected Lists Content")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(FrameColumns_CreateRefreshPartial_3, _YLOC("This will refresh the data in the grid for all currently selected lists.")).c_str();
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetRefreshPartialControlConfigRoles()
{
	O365ControlConfig config;
	config.m_ControlID = ID_365REFRESHSELECTED_ROLES;
	config.m_Image16x16ResourceID = IDB_REFRESHSELECTED_ROLES;
	config.m_Image32x32ResourceID = IDB_REFRESHSELECTED_ROLES_16X16;
	config.m_ControlText = YtriaTranslate::Do(FrameDirectoryRoles_CreateRefreshPartial_1, _YLOC("Selected Roles")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(FrameDirectoryRoles_CreateRefreshPartial_2, _YLOC("Refresh Selected Roles")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(FrameDirectoryRoles_CreateRefreshPartial_3, _YLOC("This will refresh the data in the grid for all currently selected roles.")).c_str();
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetRefreshPartialControlConfigSites()
{
	O365ControlConfig config;
	config.m_ControlID = ID_365REFRESHSELECTED_SITES;
	config.m_Image16x16ResourceID = IDB_REFRESHSELECTED_SITES;
	config.m_Image32x32ResourceID = IDB_REFRESHSELECTED_SITES_16X16;
	config.m_ControlText = YtriaTranslate::Do(FrameDriveItems_CreateRefreshPartial_7, _YLOC("Selected Sites")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(FrameDriveItems_CreateRefreshPartial_8, _YLOC("Refresh Selected Sites Content")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(FrameDriveItems_CreateRefreshPartial_9, _YLOC("This will refresh the data in the grid for all currently selected sites.")).c_str();
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetRefreshPartialControlConfigUsers()
{
	O365ControlConfig config;
	config.m_ControlID = ID_365REFRESHSELECTED_USERS;
	config.m_Image16x16ResourceID = IDB_REFRESHSELECTED_USERS;
	config.m_Image32x32ResourceID = IDB_REFRESHSELECTED_USERS_16X16;
	config.m_ControlText = YtriaTranslate::Do(FrameContacts_CreateRefreshPartial_1, _YLOC("Selected Users")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(FrameContacts_CreateRefreshPartial_2, _YLOC("Refresh Selected Users Content")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(FrameContacts_CreateRefreshPartial_3, _YLOC("This will refresh the data in the grid for all currently selected users.")).c_str();
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetRefreshPartialControlConfigChannels()
{
	O365ControlConfig config;
	config.m_ControlID = ID_365REFRESHSELECTED_CHANNELS;
	config.m_Image16x16ResourceID = IDB_REFRESHSELECTED_CHANNELS;
	config.m_Image32x32ResourceID = IDB_REFRESHSELECTED_CHANNELS_16X16;
	config.m_ControlText = _T("Selected Channels");
	config.m_ControlTooltipTitle = _T("Refresh Selected Channels Content");
	config.m_ControlDescription = _T("This will refresh the data in the grid for all currently selected channels.");
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetApplyPartialControlConfigGroups()
{
	O365ControlConfig config;
	config.m_ControlID = ID_APPLYSELECTED_GROUPS;
	config.m_Image16x16ResourceID = IDB_APPLY_SELECTED_GROUPS;
	config.m_Image32x32ResourceID = IDB_APPLY_SELECTED_GROUPS_16X16;
	config.m_ControlText = YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_10, _YLOC("Selected Groups")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(GridFrameBase_GetApplyPartialControlConfigGroups_2, _YLOC("Save Selected Groups")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(GridFrameBase_GetApplyPartialControlConfigGroups_3, _YLOC("This will save all changes made for all currently selected groups.")).c_str();
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetApplyPartialControlConfigUsers()
{
	O365ControlConfig config;
	config.m_ControlID = ID_APPLYSELECTED_USERS;
	config.m_Image16x16ResourceID = IDB_APPLY_SELECTED_USERS;
	config.m_Image32x32ResourceID = IDB_APPLY_SELECTED_USERS_16X16;
	config.m_ControlText = YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_9, _YLOC("Selected Users")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(GridFrameBase_GetApplyPartialControlConfigUsers_2, _YLOC("Save Selected Users")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(GridFrameBase_GetApplyPartialControlConfigUsers_3, _YLOC("This will save all changes made for all currently selected users.")).c_str();
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetApplyPartialControlConfigRoles()
{
	O365ControlConfig config;
	config.m_ControlID = ID_APPLYSELECTED_ROLES;
	config.m_Image16x16ResourceID = IDB_APPLY_SELECTED_ROLES;
	config.m_Image32x32ResourceID = IDB_APPLY_SELECTED_ROLES_16X16;
	config.m_ControlText = YtriaTranslate::Do(FrameDirectoryRoles_CreateRefreshPartial_1, _YLOC("Selected Roles")).c_str();
	config.m_ControlTooltipTitle = YtriaTranslate::Do(GridFrameBase_GetApplyPartialControlConfigRoles_2, _YLOC("Save Selected Roles")).c_str();
	config.m_ControlDescription = YtriaTranslate::Do(GridFrameBase_GetApplyPartialControlConfigRoles_3, _YLOC("This will save all changes made for all currently selected roles.")).c_str();
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetApplyPartialControlConfigSites()
{
	O365ControlConfig config;
	config.m_ControlID = ID_APPLYSELECTED_SITES;
	config.m_Image16x16ResourceID = IDB_APPLY_SELECTED_SITES;
	config.m_Image32x32ResourceID = IDB_APPLY_SELECTED_SITES_16X16;
	config.m_ControlText = YtriaTranslate::Do(FrameDriveItems_CreateRefreshPartial_7, _YLOC("Selected Sites")).c_str();
	config.m_ControlTooltipTitle = _T("Save Selected Sites");
	config.m_ControlDescription = _T("This will save all changes made for all currently selected sites.");
	return config;
}

GridFrameBase::O365ControlConfig GridFrameBase::GetApplyPartialControlConfigChannels()
{
	O365ControlConfig config;
	config.m_ControlID = ID_APPLYSELECTED_CHANNELS;
	config.m_Image16x16ResourceID = IDB_APPLY_SELECTED_CHANNELS;
	config.m_Image32x32ResourceID = IDB_APPLY_SELECTED_CHANNELS_16X16;
	config.m_ControlText = _T("Selected Channels");
	config.m_ControlTooltipTitle = _T("Save Selected Channels");
	config.m_ControlDescription = _T("This will save all changes made for all currently selected channels.");
	return config;
}

CXTPRibbonGroup* GridFrameBase::CreateEditGroup(CXTPRibbonTab& p_Tab, bool p_AddApplyAndRevertGroupsIfNecessary/* = true*/)
{
    O365Grid* pGrid = dynamic_cast<O365Grid*>(&GetGrid());

    CXTPRibbonGroup* groupCtrl = nullptr;

    if (HasApplyButton())
    {
		if (p_AddApplyAndRevertGroupsIfNecessary)
		{
			CreateApplyGroup(p_Tab);
			CreateRevertGroup(p_Tab);
		}

		groupCtrl = p_Tab.AddGroup(g_ActionsEditGroup.c_str());
		setGroupImage(*groupCtrl, 0);

        if (pGrid->HasView())
        {
            auto ctrl = groupCtrl->Add(xtpControlButton, ID_MODULEGRID_VIEW);
            GridFrameBase::setImage({ ID_MODULEGRID_VIEW }, IDB_VIEW, xtpImageNormal);
            GridFrameBase::setImage({ ID_MODULEGRID_VIEW }, IDB_VIEW_16X16, xtpImageNormal);
            setGridControlTooltip(ctrl);
        }

        if (pGrid->HasEdit())
        {
            auto ctrl = groupCtrl->Add(xtpControlButton, ID_MODULEGRID_EDIT);
            GridFrameBase::setImage({ ID_MODULEGRID_EDIT }, IDB_EDIT, xtpImageNormal);
            GridFrameBase::setImage({ ID_MODULEGRID_EDIT }, IDB_EDIT_16X16, xtpImageNormal);
            setGridControlTooltip(ctrl);
        }

		if (pGrid->HasCreate())
        {
            auto ctrl = groupCtrl->Add(xtpControlButton, ID_MODULEGRID_CREATE);
            GridFrameBase::setImage({ ID_MODULEGRID_CREATE }, IDB_CREATE, xtpImageNormal);
            GridFrameBase::setImage({ ID_MODULEGRID_CREATE }, IDB_CREATE_16X16, xtpImageNormal);
            setGridControlTooltip(ctrl);
        }

        if (pGrid->HasDelete())
        {
            auto ctrl = groupCtrl->Add(xtpControlButton, ID_MODULEGRID_DELETE);
            GridFrameBase::setImage({ ID_MODULEGRID_DELETE }, IDB_DELETE, xtpImageNormal);
            GridFrameBase::setImage({ ID_MODULEGRID_DELETE }, IDB_DELETE_16X16, xtpImageNormal);
            setGridControlTooltip(ctrl);
        }
    }

    return groupCtrl;
}

CXTPRibbonGroup* GridFrameBase::CreateUserInfoGroup(CXTPRibbonTab& p_Tab)
{
    auto& grid = GetGrid();
    CXTPRibbonGroup* userGroup = p_Tab.AddGroup(g_ActionsLinkUser.c_str());
    ASSERT(nullptr != userGroup);
    if (nullptr != userGroup)
    {
		setGroupImage(*userGroup, 0);

		//const auto& autoName = grid.GetAutomationName();

		const bool bigIconsAllowed = true;/* autoName == GridUtil::g_AutoNameUsers
									||	autoName == GridUtil::g_AutoNameUsersRecycleBin
									||	autoName == GridUtil::g_AutoNameManagerHierarchy
									||	autoName == GridUtil::g_AutoNameGroupsHierarchy
									||	autoName == GridUtil::g_AutoNameGroupsOwnersHierarchy
									||	autoName == GridUtil::g_AutoNameGroupsAuthorsHierarchy
									||	autoName == GridUtil::g_AutoNameDirectoryRoles;*/

		if (GridUtil::IsCommandAllowed(grid, ID_GRID_SHOWUSERDETAILS, GetOrigin()))
			addNewModuleCommandControl(*userGroup, ID_GRID_SHOWUSERDETAILS, { ID_GRID_SHOWUSERDETAILS_FRAME, ID_GRID_SHOWUSERDETAILS_NEWFRAME }, { IDB_GROUPS_SHOW_USER_DETAILS , IDB_GROUPS_SHOW_USER_DETAILS_16X16 })->SetStyle(xtpButtonIconAndCaptionBelow); // always big icon

		if (GridUtil::IsCommandAllowed(grid, ID_USERGRID_SHOWPARENTGROUPS, GetOrigin()))
			addNewModuleCommandControl(*userGroup, ID_USERGRID_SHOWPARENTGROUPS, { ID_USERGRID_SHOWPARENTGROUPS_FRAME, ID_USERGRID_SHOWPARENTGROUPS_NEWFRAME }, { IDB_USERS_SHOW_PARENTGROUPS, IDB_USERS_SHOW_PARENTGROUPS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_USERGRID_SHOWLICENSES, GetOrigin()))
			addNewModuleCommandControl(*userGroup, ID_USERGRID_SHOWLICENSES, { ID_USERGRID_SHOWLICENSES_FRAME, ID_USERGRID_SHOWLICENSES_NEWFRAME }, { IDB_USERS_SHOW_LICENSES , IDB_USERS_SHOW_LICENSES_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
		if (GridUtil::IsCommandAllowed(grid, ID_USERGRID_SHOWMANAGERHIERARCHY, GetOrigin()))
			addNewModuleCommandControl(*userGroup, ID_USERGRID_SHOWMANAGERHIERARCHY, { ID_USERGRID_SHOWMANAGERHIERARCHY_FRAME, ID_USERGRID_SHOWMANAGERHIERARCHY_NEWFRAME }, { IDB_USERS_SHOW_MANAGERHIERARCHY , IDB_USERS_SHOW_MANAGERHIERARCHY_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);
#endif

		if (GridUtil::IsCommandAllowed(grid, ID_USERGRID_SHOWDRIVEITEMS, GetOrigin()))
			addNewModuleCommandControl(*userGroup, ID_USERGRID_SHOWDRIVEITEMS, { ID_USERGRID_SHOWDRIVEITEMS_FRAME, ID_USERGRID_SHOWDRIVEITEMS_NEWFRAME }, { IDB_USERS_SHOW_DRIVEITEMS, IDB_USERS_SHOW_DRIVEITEMS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_USERGRID_SHOWMAILBOXPERMISSIONS, GetOrigin()))
			addNewModuleCommandControl(*userGroup, ID_USERGRID_SHOWMAILBOXPERMISSIONS, { ID_USERGRID_SHOWMAILBOXPERMISSIONS_FRAME, ID_USERGRID_SHOWMAILBOXPERMISSIONS_NEWFRAME }, { IDB_USERS_SHOW_MAILBOXPERMISSIONS , IDB_USERS_SHOW_MAILBOXPERMISSIONS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_USERGRID_SHOWMESSAGES, GetOrigin()))
			addNewModuleCommandControl(*userGroup, ID_USERGRID_SHOWMESSAGES, { ID_USERGRID_SHOWMESSAGES_FRAME, ID_USERGRID_SHOWMESSAGES_NEWFRAME }, { IDB_USERS_SHOW_MESSAGES , IDB_USERS_SHOW_MESSAGES_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_USERGRID_SHOWEVENTS, GetOrigin()))
			addNewModuleCommandControl(*userGroup, ID_USERGRID_SHOWEVENTS, { ID_USERGRID_SHOWEVENTS_FRAME, ID_USERGRID_SHOWEVENTS_NEWFRAME }, { IDB_USERS_SHOW_EVENTS , IDB_USERS_SHOW_EVENTS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_USERGRID_SHOWCONTACTS, GetOrigin()))
			addNewModuleCommandControl(*userGroup, ID_USERGRID_SHOWCONTACTS, { ID_USERGRID_SHOWCONTACTS_FRAME, ID_USERGRID_SHOWCONTACTS_NEWFRAME , }, { IDB_USERS_SHOW_CONTACTS , IDB_USERS_SHOW_CONTACTS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_USERGRID_SHOWMESSAGERULES, GetOrigin()))
			addNewModuleCommandControl(*userGroup, ID_USERGRID_SHOWMESSAGERULES, { ID_USERGRID_SHOWMESSAGERULES_FRAME, ID_USERGRID_SHOWMESSAGERULES_NEWFRAME }, { IDB_USERS_SHOW_MESSAGERULES , IDB_USERS_SHOW_MESSAGERULES_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);
    }

    return userGroup;
}

CXTPRibbonGroup* GridFrameBase::CreateGroupInfoGroup(CXTPRibbonTab& p_Tab)
{
    auto& grid = GetGrid();
    CXTPRibbonGroup* groupGroup = p_Tab.AddGroup(g_ActionsLinkGroup.c_str());
    ASSERT(nullptr != groupGroup);
    if (nullptr != groupGroup)
    {
		setGroupImage(*groupGroup, 0);

		//const auto& autoName = grid.GetAutomationName();

		const bool bigIconsAllowed = true;/*autoName == GridUtil::g_AutoNameGroups
									||	autoName == GridUtil::g_AutoNameUserGroups*/

		if (GridUtil::IsCommandAllowed(grid, ID_GRID_SHOWGROUPDETAILS, GetOrigin()))
			addNewModuleCommandControl(*groupGroup, ID_GRID_SHOWGROUPDETAILS, { ID_GRID_SHOWGROUPDETAILS_FRAME, ID_GRID_SHOWGROUPDETAILS_NEWFRAME }, { IDB_GROUPS_SHOW_GROUP_DETAILS , IDB_GROUPS_SHOW_GROUP_DETAILS_16X16 })->SetStyle(xtpButtonIconAndCaptionBelow); // always big icon

		if (GridUtil::IsCommandAllowed(grid, ID_GROUPSGRID_SHOWMEMBERS, GetOrigin()))
			addNewModuleCommandControl(*groupGroup, ID_GROUPSGRID_SHOWMEMBERS, { ID_GROUPSGRID_SHOWMEMBERS_FRAME, ID_GROUPSGRID_SHOWMEMBERS_NEWFRAME }, { IDB_GROUPS_SHOW_MEMBERS , IDB_GROUPS_SHOW_MEMBERS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_GROUPSGRID_SHOWOWNERS, GetOrigin()))
			addNewModuleCommandControl(*groupGroup, ID_GROUPSGRID_SHOWOWNERS, { ID_GROUPSGRID_SHOWOWNERS_FRAME, ID_GROUPSGRID_SHOWOWNERS_NEWFRAME }, { IDB_GROUPS_SHOW_OWNERS , IDB_GROUPS_SHOW_OWNERS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_GROUPSGRID_SHOWAUTHORS, GetOrigin()))
			addNewModuleCommandControl(*groupGroup, ID_GROUPSGRID_SHOWAUTHORS, { ID_GROUPSGRID_SHOWAUTHORS_FRAME, ID_GROUPSGRID_SHOWAUTHORS_NEWFRAME }, { IDB_GROUPS_SHOW_AUTHORS , IDB_GROUPS_SHOW_AUTHORS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_GROUPSGRID_SHOWDRIVEITEMS, GetOrigin()))
			addNewModuleCommandControl(*groupGroup, ID_GROUPSGRID_SHOWDRIVEITEMS, { ID_GROUPSGRID_SHOWDRIVEITEMS_FRAME, ID_GROUPSGRID_SHOWDRIVEITEMS_NEWFRAME }, { IDB_GROUPS_SHOW_DRIVEITEMS , IDB_GROUPS_SHOW_DRIVEITEMS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_GROUPSGRID_SHOWCALENDARS, GetOrigin()))
			addNewModuleCommandControl(*groupGroup, ID_GROUPSGRID_SHOWCALENDARS, { ID_GROUPSGRID_SHOWCALENDARS_FRAME, ID_GROUPSGRID_SHOWCALENDARS_NEWFRAME }, { IDB_GROUPS_SHOW_CALENDARS , IDB_GROUPS_SHOW_CALENDARS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_GROUPSGRID_SHOW_CHANNELS, GetOrigin()))
			addNewModuleCommandControl(*groupGroup, ID_GROUPSGRID_SHOW_CHANNELS, { ID_GROUPSGRID_SHOW_CHANNELS_FRAME, ID_GROUPSGRID_SHOW_CHANNELS_NEWFRAME }, { IDB_CHANNELS , IDB_CHANNELS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_GROUPSGRID_SHOWCONVERSATIONS, GetOrigin()))
			addNewModuleCommandControl(*groupGroup, ID_GROUPSGRID_SHOWCONVERSATIONS, { ID_GROUPSGRID_SHOWCONVERSATIONS_FRAME, ID_GROUPSGRID_SHOWCONVERSATIONS_NEWFRAME }, { IDB_GROUPS_SHOW_CONVERSATIONS , IDB_GROUPS_SHOW_CONVERSATIONS_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);

		if (GridUtil::IsCommandAllowed(grid, ID_GROUPSGRID_SHOWSITES, GetOrigin()))
			addNewModuleCommandControl(*groupGroup, ID_GROUPSGRID_SHOWSITES, { ID_GROUPSGRID_SHOWSITES_FRAME, ID_GROUPSGRID_SHOWSITES_NEWFRAME }, { IDB_GROUPS_SHOW_SITES , IDB_GROUPS_SHOW_SITES_16X16 })->SetStyle(bigIconsAllowed ? xtpButtonAutomatic : xtpButtonIcon);
	}

    return groupGroup;
}

CXTPRibbonGroup* GridFrameBase::CreateSiteInfoGroup(CXTPRibbonTab& p_Tab)
{
	auto& grid = GetGrid();
	CXTPRibbonGroup* siteGroup = p_Tab.AddGroup(g_ActionsLinkSite.c_str());
	ASSERT(nullptr != siteGroup);
	if (nullptr != siteGroup)
	{
		setGroupImage(*siteGroup, 0);

		if (GridUtil::IsCommandAllowed(grid, ID_GRID_SHOWSITEDETAILS, GetOrigin()))
			addNewModuleCommandControl(*siteGroup, ID_GRID_SHOWSITEDETAILS, { ID_GRID_SHOWSITEDETAILS_FRAME, ID_GRID_SHOWSITEDETAILS_NEWFRAME }, { IDB_SITES_SHOW_SITE_DETAILS , IDB_SITES_SHOW_SITE_DETAILS_16X16 })->SetStyle(xtpButtonIconAndCaptionBelow); // always big icon
	}

	return siteGroup;
}

CXTPControl* GridFrameBase::AddO365ApplyAllControl(CXTPRibbonGroup& p_Group)
{
	auto applyControl = p_Group.Add(xtpControlButton, ID_APPLYALL);
	setImage({ ID_APPLYALL }, IDB_APPLY, xtpImageNormal);
	setImage({ ID_APPLYALL }, IDB_APPLY_16X16, xtpImageNormal);
	setControlTexts(applyControl,
		YtriaTranslate::Do(GridFrameBase_AddO365ApplyControl_1, _YLOC("Save All")).c_str(),
		(LPCTSTR)MFCUtil::getFormattedTooltipTitleForRibbon(YtriaTranslate::Do(GridFrameBase_AddO365ApplyControl_2, _YLOC("Save All")).c_str(), getAcceleratorString(ID_APPLYALL)),
		YtriaTranslate::Do(GridFrameBase_AddO365ApplyControl_3, _YLOC("Save your changes to the server.\nAny pending changes such as edited or deleted items will be indicated in the status column.\nTip: Grouping by the status column is a quick way to check for all pending changes.\nNote: The Undo button will NOT undo changes that have been saved to the server.")).c_str());

	return applyControl;
}

void GridFrameBase::AddO365ApplySelectedControl(CXTPRibbonGroup& p_Group)
{
	addControlTo(GetApplyPartialControlConfig(), p_Group, ID_APPLYSELECTED);
}

CXTPControl* GridFrameBase::AddO365RevertAllControl(CXTPRibbonGroup& p_Group)
{
	auto revertControl = p_Group.Add(xtpControlButton, ID_365REVERTALL);
	setImage({ ID_365REVERTALL }, IDB_REVERT_ALL, xtpImageNormal);
	setImage({ ID_365REVERTALL }, IDB_REVERT_ALL_16X16, xtpImageNormal);
	setControlTexts(revertControl,
		YtriaTranslate::Do(GridFrameBase_AddO365RevertAllControl_1, _YLOC("Undo All")).c_str(),
		(LPCTSTR)MFCUtil::getFormattedTooltipTitleForRibbon(YtriaTranslate::Do(GridFrameBase_AddO365RevertAllControl_2, _YLOC("Undo All")).c_str(), getAcceleratorString(ID_365REVERTALL)),
		YtriaTranslate::Do(GridFrameBase_AddO365RevertAllControl_3, _YLOC("Undo all changes currently pending in the grid.\nTip: Grouping by the status column is a quick way to check for all pending changes.\nNote: This button will NOT undo changes that have already been saved to the server.")).c_str());

	return revertControl;
}

void GridFrameBase::AddO365RevertSelectedControl(CXTPRibbonGroup& p_Group)
{
	addControlTo(GetRevertPartialControlConfig(), p_Group, ID_365REVERTSELECTED);
}

CXTPRibbonGroup* GridFrameBase::CreateApplyGroup(CXTPRibbonTab& p_Tab)
{
	CXTPRibbonGroup* applyGroup = p_Tab.AddGroup(g_ActionsApplyGroup.c_str());
	setGroupImage(*applyGroup, 0);

	AddO365ApplyAllControl(*applyGroup);
	if (m_CreateApplyPartial)
		AddO365ApplySelectedControl(*applyGroup);

	return applyGroup;
}

CXTPRibbonGroup* GridFrameBase::CreateRevertGroup(CXTPRibbonTab& p_Tab)
{
	CXTPRibbonGroup* revertGroup = nullptr;

	if (GetGrid().IsGridModificationsEnabled())
	{
		revertGroup = p_Tab.AddGroup(g_ActionsRevertGroup.c_str());
		setGroupImage(*revertGroup, 0);

		auto control = AddO365RevertAllControl(*revertGroup);
		control->SetBeginGroup(TRUE);
		AddO365RevertSelectedControl(*revertGroup);
	}

	return revertGroup;
}

CXTPRibbonGroup* GridFrameBase::getSessionGroup()
{
	return nullptr != m_infoRibbonTab	? findGroup(*m_infoRibbonTab, g_InfoSessionGroup.c_str())
										: nullptr;
}

const YTimeDate& GridFrameBase::GetCreationDate() const
{
	return m_CreationDate;
}

const YTimeDate& GridFrameBase::GetLastRefreshDate() const
{
	return m_RefreshDate;
}

void GridFrameBase::SetLastRefreshDate(const YTimeDate& p_TimeDate)
{
	//ASSERT(m_RefreshDate < p_TimeDate);
	m_RefreshDate = p_TimeDate;
	updateTimeRefreshControl();

	// Group Area contains some SQL cache info and uses "last refresh date". Mark it for repaint.
	if (nullptr != GetGrid().GetGroupArea() && nullptr != GetGrid().GetGroupArea()->m_hWnd && ::IsWindow(GetGrid().GetGroupArea()->m_hWnd))
		GetGrid().GetGroupArea()->Invalidate();
}

void GridFrameBase::SetTaskData(const YtriaTaskData & p_TaskData)
{
	m_HasNoTaskRunning	= false;
	m_TaskData			= p_TaskData;
	ASSERT(GetSafeHwnd() == m_TaskData.GetOriginator());
}

int GridFrameBase::GetTaskID() const
{
	return m_TaskData.GetId();
}

//bool GridFrameBase::UpdateRibbonCommand(CWnd* commandTarget, UINT commandID, LPCTSTR tooltip, LPCTSTR description, BOOL checked, UINT updateCommandFlags)
//{
//	// Add code to enable the IsGridCommand call
//	if (/*IsGridCommand(commandID) &&*/ &GetGrid() == commandTarget)
//		return CodeJockFrameBase::UpdateRibbonCommand(commandTarget, commandID, tooltip, description, checked, updateCommandFlags);
//
//	return false;
//}

void GridFrameBase::UpdateContext(const YtriaTaskData& p_TaskData, HWND p_Originator)
{
	if (!IsSnapshotMode())
		GetGrid().ApplyDefaultView();

	// FIXME: Find a better way to know ProcessText has already been overwritten?
	const bool emptyGridBecauseError = IsMessageBarOnScreen() && !m_LastErrorInfo.empty() && GetGrid().GetBackendRows().empty();
	if (!emptyGridBecauseError)
		GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);

	RefreshDataContext();

	// !!! Keep this before automate(): automation requires the task to be over (m_HasNoTaskRunning is checked for command activation)
	if (0 <= p_TaskData.GetId())
		TaskDataManager::Get().RemoveTaskData(p_TaskData.GetId(), p_Originator);
	m_HasNoTaskRunning = true;
	////

	ProcessLicenseContext();

	Automate();

	AutomationCleanup();
}

void GridFrameBase::TaskFinished(const YtriaTaskData& p_TaskData, AutomationAction* p_ActionToGreenlight, HWND p_Originator)
{
	// Only made to be called by ModuleBase::taskFinished.

	// FIXME: Find a better way to know ProcessText has already been overwritten?
	const bool emptyGridBecauseError = IsMessageBarOnScreen() && !m_LastErrorInfo.empty() && GetGrid().GetBackendRows().empty();
	if (!emptyGridBecauseError)
		GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);

	TaskDataManager::Get().RemoveTaskData(p_TaskData.GetId(), p_Originator);
	m_HasNoTaskRunning = true;

	if (nullptr != p_ActionToGreenlight)
	{
		ASSERT(GetAutomationActionMother() != p_ActionToGreenlight);
		setAutomationGreenLight(p_ActionToGreenlight);
	}

	// m_AutomationActionRequestingExecutionPermit is reset to null right after automationExecuteCommand call in ExecuteAction.
	// As TaskFinished (here) is called at the end of the task (so asynchronously to automationExecuteCommand), m_AutomationActionRequestingExecutionPermit is already nullptr
	ASSERT(nullptr == m_AutomationActionRequestingExecutionPermit || p_ActionToGreenlight == m_AutomationActionRequestingExecutionPermit);
	m_AutomationActionRequestingExecutionPermit = nullptr;
}

void GridFrameBase::RefreshDataContext()
{
	if (nullptr != m_DataContextLabelControl1)
	{
		if (GetOrigin() == Origin::Tenant || GetOrigin() == Origin::NotSet)
		{
			m_DataContextLabelControl1->SetCaption(YtriaTranslate::Do(GridFrameBase_UpdateContext_1, _YLOC("Global - no selection")).c_str());
		}
		else
		{
			const size_t count = GetModuleCriteria().m_IDs.empty() ? GetModuleCriteria().m_SubIDs.size() : GetModuleCriteria().m_IDs.size();
			if (GetOrigin() == Origin::User || GetOrigin() == Origin::DeletedUser)
				m_DataContextLabelControl1->SetCaption(YtriaTranslate::Do(GridFrameBase_UpdateContext_3, _YLOC("Displaying data for %1 users"), Str::getStringFromNumber(count).c_str()).c_str());
			else if (GetOrigin() == Origin::Group)
				m_DataContextLabelControl1->SetCaption(YtriaTranslate::Do(GridFrameBase_UpdateContext_4, _YLOC("Displaying data for %1 groups"), Str::getStringFromNumber(count).c_str()).c_str());
			else if (GetOrigin() == Origin::Site)
				m_DataContextLabelControl1->SetCaption(YtriaTranslate::Do(GridFrameBase_UpdateContext_5, _YLOC("Displaying data for %1 sites"), Str::getStringFromNumber(count).c_str()).c_str());
			else if (GetOrigin() == Origin::Conversations)
				m_DataContextLabelControl1->SetCaption(YtriaTranslate::Do(GridFrameBase_UpdateContext_6, _YLOC("Displaying data for %1 conversations"), Str::getStringFromNumber(count).c_str()).c_str());
			else if (GetOrigin() == Origin::Lists)
				m_DataContextLabelControl1->SetCaption(YtriaTranslate::Do(GridFrameBase_UpdateContext_7, _YLOC("Displaying data for %1 lists"), Str::getStringFromNumber(count).c_str()).c_str());
			else if (GetOrigin() == Origin::Channel)
				m_DataContextLabelControl1->SetCaption(_YFORMAT(L"Displaying data for %s channels", Str::getStringFromNumber(count).c_str()).c_str());
			else
				ASSERT(false);

			if (nullptr != m_DataContextLabelControl2)
				m_DataContextLabelControl2->SetCaption(YtriaTranslate::Do(GridFrameBase_UpdateContext_2, _YLOC("Update your choices:")).c_str());
		}
	}
}

Origin GridFrameBase::GetOrigin() const
{
	return m_ModuleCriteria.m_Origin;
}

void GridFrameBase::FocusChanged(const CacheGrid& grid, bool hasFocus)
{
	if (&GetGrid() == &grid && hasFocus)
		onGCVDFocusChanged(false);
	else if (GetGrid().GetColumnOrg()->GetGCVDGrid() == &grid && hasFocus)
		onGCVDFocusChanged(true);
}

// In-ribbon search test (not really working yet)
 /*int GridFrameBase::OnCreateCommandBar(LPCREATEBARSTRUCT lpCreateBar)
 {
	 if (TRUE == lpCreateBar->bPopup && CACHEGRID_MENU_SEARCH == lpCreateBar->nID)
	 {
		 CXTPPopupBar* pPopupBar = new SearchRegexPopuBar;
		 pPopupBar->SetCommandBars(GetCommandBars());
		 lpCreateBar->pCommandBar = pPopupBar;
		 return TRUE;
	 }
 }*/

void GridFrameBase::onGCVDFocusChanged(bool gcvdHasFocus)
{
	if (!gcvdHasFocus) // ribbon back to grid
	{
		ASSERT(nullptr != m_globalRibbonTab);
		if (nullptr != m_globalRibbonTab)
		{
			ASSERT(m_lastGridRelatedRibbonTab != m_gcvdRibbonTab);

			const int curSel = getRibbonBar().GetCurSel();
			if (nullptr != m_lastGridRelatedRibbonTab
				&& (nullptr == m_gcvdRibbonTab || curSel == m_gcvdRibbonTab->GetIndex()))
				getRibbonBar().SetCurSel(m_lastGridRelatedRibbonTab->GetIndex());
		}
	}
	else // temp ribbon for gcvd
	{
		ASSERT(nullptr != m_globalRibbonTab);
		if (nullptr != m_globalRibbonTab)
		{
			ASSERT(m_lastGridRelatedRibbonTab != m_gcvdRibbonTab);

			auto selectedTab = getRibbonBar().GetTab(getRibbonBar().GetCurSel());
			if (selectedTab != m_gcvdRibbonTab)
				m_lastGridRelatedRibbonTab = selectedTab;

			getRibbonBar().SetCurSel(m_gcvdRibbonTab->GetIndex());
		}
	}
}

void GridFrameBase::StateChanged(const CacheGridColumnOrg& gcvd, bool pinned, bool minimized)
{
	if (minimized) // GCVD is hidden, hide gcvd tab
		m_gcvdRibbonTab->SetVisible(FALSE);
	else // GCVD is visible, show gcvd tab
		m_gcvdRibbonTab->SetVisible(TRUE);
}

void GridFrameBase::LicenseChanged()
{
	ShowTokenMenu(Office365AdminApp::Get().GetLicenseManager()->GetCachedLicense().GetFeatureToken().IsAuthorized());
}

void GridFrameBase::initStaticStrings()
{
	if (!g_staticInit)
	{
		g_staticInit = true;

		g_GridContextName			= YtriaTranslate::Do(GridFrameBase_initStaticStrings_1, _YLOC("Grid"));
		g_GCVDContextName			= YtriaTranslate::Do(GridFrameBase_initStaticStrings_2, _YLOC("Grid Manager"));

		g_ViewGroup					= YtriaTranslate::Do(GridFrameBase_CreateViewGroup_1, _YLOC("View"));
		g_ActionsDataGroup			= YtriaTranslate::Do(GridFrameBase_initStaticStrings_3, _YLOC("Refresh"));
		g_ActionsApplyGroup			= YtriaTranslate::Do(GridFrameBase_initStaticStrings_4, _YLOC("Save"));
		g_ActionsRevertGroup		= YtriaTranslate::Do(CacheGrid_InitializeCommands_552, _YLOC("Undo"));
		g_ActionsEditGroup			= YtriaTranslate::Do(GridFrameBase_initStaticStrings_5, _YLOC("Edit"));
		g_ActionsLinkUser			= YtriaTranslate::Do(GridFrameBase_initStaticStrings_6, _YLOC("User Management"));
		// g_ActionsQuickCollapse		= _TLOC("Quick Collapse");
		g_ActionsLinkGroup			= YtriaTranslate::Do(GridFrameBase_initStaticStrings_7, _YLOC("Group Management"));
		g_ActionsLinkSite			= YtriaTranslate::Do(GridFrameBase_initStaticStrings_8, _YLOC("Site Management"));
		g_ActionsLinkSchools		= YtriaTranslate::Do(GridFrameBase_initStaticStrings_8, _YLOC("Schools Management"));
        //g_ActionsDriveGroup         = _YTEXT("Item Info");
		g_ActionsLinkConversation	= YtriaTranslate::Do(GridFrameBase_initStaticStrings_9, _YLOC("Management")); // "Conversation Info" is too long, it's ugly as there's only one icon in the group.
		g_ActionsLinkList			= YtriaTranslate::Do(GridFrameBase_initStaticStrings_10, _YLOC("List Management"));
		g_ActionsLinkChannel		= YtriaTranslate::Do(GridFrameBase_initStaticStrings_11, _YLOC("Management"));
		g_ActionsLinkMessage		= YtriaTranslate::Do(GridFrameBase_initStaticStrings_12, _YLOC("Management"));
	}
}

void GridFrameBase::OnCancel(CancelContext& p_Context)
{
	LoggerService::User(p_Context.m_Message, GetSafeHwnd());
	//GetGrid().ClearLog(m_TaskData.GetId());// also done  by m_TaskData.Cancel(), but performs a quicker grid refresh
	m_TaskData.Cancel();
}

LRESULT GridFrameBase::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (YtriaTaskData::MESSAGE_CANCELTASK == message)
	{
		ASSERT(m_TaskData.GetId() == wParam);
		// Anything here?
	}
	else if (WM_CLOSE == message)
	{
		// Note that with respect to Microsoft, the wParam and lParam of WM_CLOSE are not used.
		// In this case, we are using the wParam (via SendMessage) to prevent the update of the backstage windows list.
		m_DoNotUpdateWindowsListOnClose = GridFrameBase::g_NoWindowsListUpdate == wParam;
	}

	return FrameBase<CodeJockFrameBase>::WindowProc(message, wParam, lParam);
}

const ModuleCriteria& GridFrameBase::GetModuleCriteria() const
{
	return m_ModuleCriteria;
}

ModuleCriteria& GridFrameBase::GetModuleCriteria()
{
    return m_ModuleCriteria;
}

ModuleBase& GridFrameBase::GetModuleBase()
{
	return m_module;
}

std::shared_ptr<BusinessObjectManager> GridFrameBase::GetBusinessObjectManager()
{
	return m_BusinessObjectManager;
}

void GridFrameBase::SetIsBeingAutomaticallyArrangedOrTitled(bool p_BeingArrangedOrTitled)
{
	m_BeingArrangedOrTiled = p_BeingArrangedOrTitled;
}

bool GridFrameBase::CompliesWithDataLoadPolicy()
{
	return !m_TaskData.IsCanceled() || GetGrid().GetBackend().GetDataRowCount() == 0;
}

AutomatedApp::AUTOMATIONSTATUS GridFrameBase::automationGetCommandID(UINT& p_CommandID, AutomationAction* p_Action)
{
	ASSERT(nullptr != p_Action);
	if (nullptr != p_Action)
	{
		const bool newFrame = p_Action->IsParamTrue(g_NewFrame);

		// commands available in several frames:
		// user
		if (IsActionSelectedShowParentGroups(p_Action))
			p_CommandID = newFrame ? ID_USERGRID_SHOWPARENTGROUPS_NEWFRAME : ID_USERGRID_SHOWPARENTGROUPS_FRAME;
		else if (IsActionSelectedShowDriveItemsUsers(p_Action))
			p_CommandID = newFrame ? ID_USERGRID_SHOWDRIVEITEMS_NEWFRAME : ID_USERGRID_SHOWDRIVEITEMS_FRAME;
		else if (IsActionSelectedShowMessages(p_Action))
			p_CommandID = newFrame ? ID_USERGRID_SHOWMESSAGES_NEWFRAME : ID_USERGRID_SHOWMESSAGES_FRAME;
		else if (IsActionSelectedShowManagerHierarchy(p_Action))
			p_CommandID = newFrame ? ID_USERGRID_SHOWMANAGERHIERARCHY_NEWFRAME : ID_USERGRID_SHOWMANAGERHIERARCHY_FRAME;
		else if (IsActionSelectedShowContacts(p_Action))
			p_CommandID = newFrame ? ID_USERGRID_SHOWCONTACTS_NEWFRAME : ID_USERGRID_SHOWCONTACTS_FRAME;
		else if (IsActionSelectedShowEventsUsers(p_Action))
			p_CommandID = newFrame ? ID_USERGRID_SHOWEVENTS_NEWFRAME : ID_USERGRID_SHOWEVENTS_FRAME;
		else if (IsActionSelectedShowLicenses(p_Action))
			p_CommandID = newFrame ? ID_USERGRID_SHOWLICENSES_NEWFRAME : ID_USERGRID_SHOWLICENSES_FRAME;
		else if (IsActionSelectedShowMessageRules(p_Action))
			p_CommandID = newFrame ? ID_USERGRID_SHOWMESSAGERULES_NEWFRAME : ID_USERGRID_SHOWMESSAGERULES_FRAME;
		// group
		else if (IsActionSelectedShowDriveItemsGroups(p_Action))
			p_CommandID = newFrame ? ID_GROUPSGRID_SHOWDRIVEITEMS_NEWFRAME : ID_GROUPSGRID_SHOWDRIVEITEMS_FRAME;
		else if (IsActionSelectedShowEventsGroups(p_Action))
			p_CommandID = newFrame ? ID_GROUPSGRID_SHOWCALENDARS_NEWFRAME : ID_GROUPSGRID_SHOWCALENDARS_FRAME;
		else if (IsActionSelectedShowMembers(p_Action))
			p_CommandID = newFrame ? ID_GROUPSGRID_SHOWMEMBERS_NEWFRAME : ID_GROUPSGRID_SHOWMEMBERS_FRAME;
		else if (IsActionSelectedShowOwners(p_Action))
			p_CommandID = newFrame ? ID_GROUPSGRID_SHOWOWNERS_NEWFRAME : ID_GROUPSGRID_SHOWOWNERS_FRAME;
		else if (IsActionSelectedShowAuthors(p_Action))
			p_CommandID = newFrame ? ID_GROUPSGRID_SHOWAUTHORS_NEWFRAME : ID_GROUPSGRID_SHOWAUTHORS_FRAME;
		else if (IsActionSelectedShowConversations(p_Action))
			p_CommandID = newFrame ? ID_GROUPSGRID_SHOWCONVERSATIONS_NEWFRAME : ID_GROUPSGRID_SHOWCONVERSATIONS_FRAME;
		else if (IsActionSelectedShowSites(p_Action))
			p_CommandID = newFrame ? ID_GROUPSGRID_SHOWSITES_NEWFRAME : ID_GROUPSGRID_SHOWSITES_FRAME;
		else if (IsActionSelectedShowChannels(p_Action))
			p_CommandID = newFrame ? ID_GROUPSGRID_SHOW_CHANNELS_NEWFRAME : ID_GROUPSGRID_SHOW_CHANNELS;
		// concubines
		else if (IsActionSelectedShowDetailsGroup(p_Action))
			p_CommandID = newFrame ? ID_GRID_SHOWGROUPDETAILS_NEWFRAME : ID_GRID_SHOWGROUPDETAILS_FRAME;
		else if (IsActionSelectedShowDetailsUser(p_Action))
			p_CommandID = newFrame ?  ID_GRID_SHOWUSERDETAILS_NEWFRAME : ID_GRID_SHOWUSERDETAILS_FRAME;
		// generic
		else if (IsActionRefreshAll(p_Action))
			p_CommandID = ID_365REFRESH;
		else if (IsActionRefreshSelected(p_Action))
			p_CommandID = ID_365REFRESHSELECTED;
		else if (IsActionShowColumnsDefault(p_Action))
			p_CommandID = ID_SHOWCOLUMNS_DEFAULT;
		else if (IsActionShowColumnsAll(p_Action))
			p_CommandID = ID_SHOWCOLUMNS_ALL;
		// edit/view details
		else if (IsActionSelectedDelete(p_Action))
			p_CommandID = ID_MODULEGRID_DELETE;
		else if (IsActionSelectedView(p_Action))
			p_CommandID = ID_MODULEGRID_VIEW;
		else if (IsActionSave(p_Action))
			p_CommandID = ID_APPLYALL;
		else if (IsActionSaveSelected(p_Action))
			p_CommandID = ID_APPLYSELECTED;

		if (p_CommandID != ID_SHOWCOLUMNS_DEFAULT
			&& p_CommandID != ID_SHOWCOLUMNS_ALL
			&& p_CommandID != ID_MODULEGRID_DELETE
			&& p_CommandID != ID_MODULEGRID_VIEW)
			Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}

	return automationGetCommandIDorExecuteAction(p_CommandID, p_Action);// achtung: not all actions are executed via a command ID (cf FrameLists)
}

bool GridFrameBase::closingAllowedByAutomation() const
{
	AutomatedApp* app = dynamic_cast<AutomatedApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	// stop automation if this frame is being automated (user closed shop before script end)
	// keep this before OnClose();
	if (nullptr != app && AutomatedApp::IsAutomationRunning() && !AutomatedApp::IsStopAutomation() &&
		(app->GetCurrentContext().GetFrame() == this ||
			GetAutomationAction() != nullptr /* in case frame did not become a mother yet */ ||
			GetAutomationActionMother() != nullptr /* frame was launched by automation */))
		return false;
	return true;
}

void GridFrameBase::postAutomateSpecific(AutomationAction* p_MotherAction)
{
	ASSERT(nullptr != p_MotherAction);
	if (nullptr != p_MotherAction)
	{
		const bool subModule	= Office365AdminApp::IsSubmoduleAction(p_MotherAction);
		const bool newFrame		= !subModule || p_MotherAction->IsParamTrue(g_NewFrame);
		// newFrame case is handled in Office365AdminApp::cleanUpAfterExecuteAction
		if (!newFrame)
		{
			AutomationEngine* engine = AutomatedApp::GetAutomationEngine();
			ASSERT(nullptr != engine);
			ASSERT(subModule);
			if (nullptr != engine && subModule)
			{
				const bool keepAliveTrue	= p_MotherAction->IsParamTrue(AutomationConstant::val().m_ParamNameKeepAlive);
				const bool keepAliveThis	= p_MotherAction->IsParamThis(AutomationConstant::val().m_ParamNameKeepAlive);
				const bool keepAliveParent	= !p_MotherAction->HasParam(AutomationConstant::val().m_ParamNameKeepAlive) && engine->IsKeepAlive();

				if (keepAliveTrue || keepAliveThis || keepAliveParent)
				{
					if (!engine->IsStopAutomation() && !engine->IsLastTopAction(p_MotherAction->GetMotherTopExecutableOrThis()))
					{
						engine->GetAutomatedApp()->AutomationTrace(_YFORMAT(L"Automation has stopped: the frame is instructed to stay open, therefore no further action can be processed.\n\tAction \"%s\": \"%s\" parameter is not set to \"%s\"", (LPCWSTR) p_MotherAction->toString(), AutomationConstant::val().m_ParamNameKeepAlive.c_str(), AutomationConstant::val().m_ValueFalse.c_str()), TraceOutput::TRACE_WARNING);
						engine->StopAutomation();
					}
				}
				else
					GoToPrevious();
			}
		}

		Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
			app->RegisterForPostExecutionProcess(p_MotherAction);
	}
}

AutomatedApp::AUTOMATIONSTATUS GridFrameBase::ExecuteAction(AutomationAction* p_Action)
{
	ASSERT(nullptr == m_AutomationActionRequestingExecutionPermit);

	UINT commandID = 0;
	AutomatedApp::AUTOMATIONSTATUS statusRV = automationGetCommandID(commandID, p_Action);

	if (0 != commandID)
	{
		m_AutomationActionRequestingExecutionPermit = p_Action;// for stealth Command objects as in ApplySpecific
		if (AutomatedApp::AUTOMATIONSTATUS_EXECUTED != statusRV)
			statusRV = automationExecuteCommand(commandID, p_Action);
		m_AutomationActionRequestingExecutionPermit = nullptr;
	}

	if ((AutomatedApp::AUTOMATIONSTATUS_ERROR == statusRV) || // do not freeze automation upon error: greenlight now
		(0 == commandID && AutomatedApp::AUTOMATIONSTATUS_TRY == statusRV) ||
		(m_Automation_CommandIDToGreenlight.end() != m_Automation_CommandIDToGreenlight.find(commandID)))// non-frame-task action: greenlight now
		setAutomationGreenLight(p_Action);

	return statusRV;
}

void GridFrameBase::automationAddCommandIDToGreenLight(const UINT p_CommandID)
{
	m_Automation_CommandIDToGreenlight.insert(p_CommandID);
}

CXTPControl* GridFrameBase::addNewModuleCommandControl(CXTPRibbonGroup& group, UINT commandID, const vector<UINT> menuCommandIDs, const vector<UINT> imageIDs)
{
	ASSERT(menuCommandIDs.size() == 2);
	auto ctrl = group.Add(xtpControlSplitButtonPopup, commandID);
	for (auto imageId : imageIDs)
		GridFrameBase::setImage({ commandID }, imageId, xtpImageNormal);
	setGridControlTooltip(ctrl);

	CXTPControlPopup* popup = dynamic_cast<CXTPControlPopup*>(ctrl);

	if (menuCommandIDs.size() > 0)
	{
		popup->GetCommandBar()->GetControls()->Add(xtpControlButton, menuCommandIDs[0]);
		GridFrameBase::setImage({ menuCommandIDs[0] }, IDB_OPEN_FRAME_16X16, xtpImageNormal);
	}

	if (menuCommandIDs.size() > 1)
	{
		popup->GetCommandBar()->GetControls()->Add(xtpControlButton, menuCommandIDs[1]);
		GridFrameBase::setImage({ menuCommandIDs[1] }, IDB_OPEN_NEW_FRAME_16X16, xtpImageNormal);
	}

	return ctrl;
}

void GridFrameBase::automationAddGenericCommandIDsToGreenlight()
{
	automationAddCommandIDToGreenLight(ID_SHOWCOLUMNS_DEFAULT);
	automationAddCommandIDToGreenLight(ID_SHOWCOLUMNS_ALL);
	automationAddCommandIDToGreenLight(ID_MODULEGRID_VIEW);
	automationAddCommandIDToGreenLight(ID_MODULEGRID_EDIT);
	// automationAddCommandIDToGreenLight(ID_APPLYSELECTED); wait for the save task completion! Bug #200505.YT.00C026
	automationAddCommandIDToGreenLight(ID_MODULEGRID_DELETE);
	automationAddCommandIDToGreenLight(ID_MODULEGRID_CREATE);
}

void GridFrameBase::setAutomationGreenLight(AutomationAction* p_Action)
{
	if (nullptr != p_Action)
	{
		AutomationEngine* engine = AutomatedApp::GetAutomationEngine();
		ASSERT(nullptr != engine);
		if (nullptr != engine)
			engine->GetAutomatedApp()->SetAutomationGreenLight(p_Action);
	}
}

AutomatedApp::AUTOMATIONSTATUS GridFrameBase::automationExecuteCommand(const UINT p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_ERROR;

	ASSERT(0 != p_CommandID);
	ASSERT(nullptr != p_Action);
	if (0 != p_CommandID && nullptr != p_Action)
	{
		YTestCmdUI testAvailability(p_CommandID);
		testAvailability.Enable(FALSE);
		OnCmdMsg(p_CommandID, CN_UPDATE_COMMAND_UI, &testAvailability, nullptr);
		if (!testAvailability.m_bEnabled)
		{
			p_Action->AddError(YtriaTranslate::Do(GridFrameBase_automationExecuteCommand_1, _YLOC("Action is disabled (probably because no row selection was set in the grid) or unavailable in module %1."), m_CustomTitle.c_str()).c_str(), TraceOutput::TRACE_ERROR);
			statusRV = AutomatedApp::AUTOMATIONSTATUS_ERROR;
		}
		else
		{
			//HistoryMode::SetModeFromCommand(p_CommandID); is called in OnCmdMsg
			HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);// pointless because of the above, but for some obscure reason HistoryMode needs to be initialized otherwise caca
			OnCmdMsg(p_CommandID, CN_COMMAND, nullptr, nullptr);
			statusRV = AutomatedApp::AUTOMATIONSTATUS_EXECUTED;
		}
	}

	return statusRV;
}

UINT GridFrameBase::GetGrayedHierarchyFilterTypeIconID(UINT normalId)
{
	switch (normalId)
	{
	case IDB_ICON_CONTACT:
		return IDB_ICON_CONTACT_GRAYED;
	case IDB_ICON_CONVERSATION:
		return IDB_ICON_CONVERSATION_GRAYED;
    case IDB_ICON_CONVERSATIONTHREAD:
        return IDB_ICON_CONVERSATIONTHREAD_GRAYED;
	case IDB_ICON_DRIVE:
		return IDB_ICON_DRIVE_GRAYED;
	case IDB_ICON_DRIVEITEM:
		return IDB_ICON_DRIVEITEM_GRAYED;
	case IDB_ICON_DRIVEITEM_FOLDER:
		return IDB_ICON_DRIVEITEM_FOLDER_GRAYED;
	case IDB_ICON_DRIVEITEM_NOTEBOOK:
		return IDB_ICON_DRIVEITEM_NOTEBOOK_GRAYED;
	case IDB_ICON_EVENT:
		return IDB_ICON_EVENT_GRAYED;
	case IDB_ICON_GROUP:
		return IDB_ICON_GROUP_GRAYED;
	case IDB_ICON_MAILFOLDER:
		return IDB_ICON_MAILFOLDER_GRAYED;
	case IDB_ICON_MESSAGE:
		return IDB_ICON_MESSAGE_GRAYED;
	case IDB_ICON_SITE:
		return IDB_ICON_SITE_GRAYED;
	case IDB_ICON_LIST:
		return IDB_ICON_LIST_GRAYED;
	case IDB_ICON_USER:
		return IDB_ICON_USER_GRAYED;
	case IDB_ICON_USER_2:
		return IDB_ICON_USER_GRAYED_2;
	case IDB_ICON_USER_3:
		return IDB_ICON_USER_GRAYED_3;
	case IDB_ICON_LISTITEM:
		return IDB_ICON_LISTITEM_GRAYED;
	case IDB_ICON_BUSINESSSUBSCRIBEDSKU:
		return IDB_ICON_BUSINESSSUBSCRIBEDSKU_GRAYED;
	case IDB_ICON_SERVICEPLANINFO:
		return IDB_ICON_SERVICEPLANINFO_GRAYED;
	case IDB_ICON_COLUMNDEFINITION:
		return IDB_ICON_LISTCOLUMNDEFINITION_GRAYED;
    case IDB_ICON_POST:
        return IDB_ICON_POST_GRAYED;
    case IDB_ICON_DIRECTORYROLETEMPLATE:
        return IDB_ICON_DIRECTORYROLETEMPLATE_GRAYED;
    case IDB_ICON_MESSAGERULE:
        return IDB_ICON_MESSAGERULE_GRAYED;
	case IDB_ICON_MESSAGERULE_COMPONENT:
		return IDB_ICON_MESSAGERULE_COMPONENT_GRAYED;
	case IDB_ICON_ORG_CONTACT:
		return IDB_ICON_ORG_CONTACT_GRAYED;
	case IDB_ICON_CHANNELMESSAGE:
		return IDB_ICON_CHANNELMESSAGE_GRAYED;
	case IDB_ICON_CHANNELREPLY:
		return IDB_ICON_CHANNELREPLY_GRAYED;
	case IDB_ICON_CHANNEL:
		return IDB_ICON_CHANNEL_GRAYED;
	case IDB_ICON_USER_DELETED:
		return IDB_ICON_USER_DELETED_GRAYED;
	case IDB_ICON_USER_2_DELETED:
		return IDB_ICON_USER_2_DELETED_GRAYED;
	case IDB_ICON_USER_3_DELETED:
		return IDB_ICON_USER_3_DELETED_GRAYED;
	case IDB_ICON_SPROLE:
		return IDB_ICON_SPROLE_GRAYED;
	case IDB_ICON_SIGNIN:
		return IDB_ICON_SIGNIN_GRAYED;
	case IDB_ICON_DIRECTORYAUDIT:
		return IDB_ICON_DIRECTORYAUDIT_GRAYED;
	case IDB_ICON_EDUCATIONSCHOOL:
		return IDB_ICON_EDUCATIONSCHOOL_GRAYED;
	case IDB_ICON_EDUCATIONCLASS:
		return IDB_ICON_EDUCATIONCLASS_GRAYED;
	case IDB_ICON_EDUCATIONUSER:
		return IDB_ICON_USER_DELETED_GRAYED;
	case IDB_ICON_SPUSER:
		return IDB_ICON_SPUSER_GRAYED;
	case IDB_ICON_SPGROUP:
		return IDB_ICON_SPGROUP_GRAYED;
	case IDB_ICON_PRIVATECHANMEMBER:
		return IDB_ICON_PRIVATECHANMEMBER_GRAYED;
	case IDB_ICON_MAILBOXPERMISSIONS:
		return IDB_ICON_MAILBOXPERMISSIONS_GRAYED;
	case IDB_ICON_HYBRIDUSER:
		return IDB_ICON_HYBRIDUSER_GRAYED;
	case IDB_ICON_ONPREMISEUSER:
		return IDB_ICON_ONPREMISEUSER_GRAYED;
	case IDB_ICON_ONPREMISEGROUP:
		return IDB_ICON_ONPREMISEGROUP_GRAYED;
	case IDB_ICON_HYBRIDGROUP:
		return IDB_ICON_HYBRIDGROUP_GRAYED;
	default:
		break;
	}

	ASSERT(false);
	return 0;
}

CXTPControl* GridFrameBase::addGridConfigControl(CXTPRibbonGroup& group)
{
	if (GetGrid().IsCreateSaveXML())
	{
		auto control = addGridPopupButtonControl(group, 0, { CACHEGRID_MENU_GCVDSAVE, CACHEGRID_MENU_GCVDSAVEASDEFAULT, CACHEGRID_MENU_GCVDLOAD, CACHEGRID_MENU_GCVDRESETTOSETTINGSUSER, CACHEGRID_MENU_GCVDRESETTOSETTINGSYTRIA });

		control->SetIconId(RPROD_BMP_RIBBON_CACHEGRID_MENU_GRIDCONFIG);
		setImage({ RPROD_BMP_RIBBON_CACHEGRID_MENU_GRIDCONFIG }, RPROD_BMP_RIBBON_CACHEGRID_MENU_GRIDCONFIG, xtpImageNormal);
		setImage({ RPROD_BMP_RIBBON_CACHEGRID_MENU_GRIDCONFIG }, RPROD_BMP_RIBBON_CACHEGRID_MENU_GRIDCONFIG_16X16, xtpImageNormal);

		control->SetCaption(YtriaTranslate::Do(GridFrameBase_addGridConfigControl_1, _YLOC("Grid Configuration")).c_str());
		control->SetTooltip(YtriaTranslate::Do(GridFrameBase_addGridConfigControl_2, _YLOC("Manage Grid Configuration")).c_str());
		control->SetDescription(YtriaTranslate::Do(GridFrameBase_addGridConfigControl_3, _YLOC("Save and recall your own grid configurations, set a default layout, or reset the grid to the original \"factory\" settings.")).c_str());

		return control;
	}

	return nullptr;
}

CXTPControl* GridFrameBase::addGridStatsControls(CXTPRibbonGroup& group)
{
	auto control = addGridPopupButtonControl(group, 0, {	CACHEGRID_MENU_STATS01_THREESIGMA, 
															CACHEGRID_MENU_STATS02_CENTILES, 
															CACHEGRID_MENU_STATS03_TOPBOTTOM, 
															CACHEGRID_MENU_STATS04_DELTA20,
															CACHEGRID_MENU_STATS05_DELTAVAL,
															CACHEGRID_MENU_STATS06_THREESIGMA_COUNT,
															CACHEGRID_MENU_STATS07_CENTILES_COUNT,
															CACHEGRID_MENU_STATS08_TOPBOTTOM_COUNT,
															CACHEGRID_MENU_STATS09_DELTA20_COUNT,
															CACHEGRID_MENU_STATS10_DELTAVAL_COUNT,
															CACHEGRID_MENU_STATSREMOVE });

	control->SetIconId(RPROD_BMP_RIBBON_CACHEGRID_MENU_STATISTICS);
	setImage({ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATISTICS }, RPROD_BMP_RIBBON_CACHEGRID_MENU_STATISTICS, xtpImageNormal);
	setImage({ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATISTICS }, RPROD_BMP_RIBBON_CACHEGRID_MENU_STATISTICS_16X16, xtpImageNormal);

	control->SetCaption(YtriaTranslate::Do(GridFrameBase_addGridStatsControls_1, _YLOC("Statistics")).c_str());
	control->SetTooltip(YtriaTranslate::Do(GridFrameBase_addGridStatsControls_2, _YLOC("Calculate Statistics")).c_str());
	control->SetDescription(YtriaTranslate::Do(GridFrameBase_addGridStatsControls_3, _YLOC("Perform a variety of statistical calculations for a data set defined by your currently focused column.")).c_str());

	return control;
}

CXTPControl* GridFrameBase::addGridDuplicatesControls(CXTPRibbonGroup& group)
{
	auto control = addGridSplitButtonControl(group, ID_GRIDFRAMEBASE_CACHEGRID_MENU_DUPLICATESSHOW, { CACHEGRID_MENU_DUPLICATESSHOW, CACHEGRID_MENU_DUPLICATERSEMOVE });

	control->SetIconId(RPROD_BMP_RIBBON_CACHEGRID_MENU_DUPLICATES_SHOW);
	setImage({ RPROD_BMP_RIBBON_CACHEGRID_MENU_DUPLICATES_SHOW }, RPROD_BMP_RIBBON_CACHEGRID_MENU_DUPLICATES_SHOW, xtpImageNormal);
	setImage({ RPROD_BMP_RIBBON_CACHEGRID_MENU_DUPLICATES_SHOW }, RPROD_BMP_RIBBON_CACHEGRID_MENU_DUPLICATES_SHOW_16X16, xtpImageNormal);

	return control;
}

wstring GridFrameBase::getPositionAndSizeRegistrySectionName() const
{
	return m_CommandBarsProfileName;
}

CString GridFrameBase::getAcceleratorString(UINT p_CmdID)
{
	if (m_AcceleratorTexts.empty())
	{
		auto accelTable = LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_GRIDFRAMEBASE));
		const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(accelTable, m_AcceleratorTexts);
		ASSERT(validAccelTexts && m_AcceleratorTexts.size() > 0);
	}

	return MFCUtil::getCommandAccelText(p_CmdID, m_AcceleratorTexts);
}

bool GridFrameBase::isDebugMode() const
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	return nullptr != mainFrame && mainFrame->IsDebugMode();
}

void GridFrameBase::updateTimeRefreshControlToNow()
{
	m_RefreshDate = YTimeDate::GetCurrentTimeDate();
	updateTimeRefreshControl();
}

void GridFrameBase::updateNavigationDropDown(set<CXTPControl*> p_Controls, UINT p_Id, vector<IHistoryFrame*> p_History)
{
	for (auto control : p_Controls)
	{
		ASSERT(nullptr != control);
		if (nullptr != control)
		{
			CXTPControlPopup* popup = dynamic_cast<CXTPControlPopup*>(control);
			ASSERT(nullptr != popup);
			if (nullptr != popup)
			{
				auto controls = popup->GetCommandBar()->GetControls();
				controls->RemoveAll();
				CString caption;
				for (const auto& historyFrame : p_History)
				{
					const auto frame = dynamic_cast<CFrameWnd*>(historyFrame);
					ASSERT(nullptr != frame && ::IsWindow(frame->m_hWnd));
					ASSERT(this != frame);
					if (nullptr != frame && ::IsWindow(frame->m_hWnd))
					{
						auto ctrl = controls->Add(xtpControlButton, p_Id);
						ctrl->SetTag(reinterpret_cast<DWORD_PTR>(historyFrame));
						frame->GetWindowText(caption);
						ctrl->SetCaption(caption);
					}
				}
			}
		}
	}
}

bool GridFrameBase::canNavigate()
{
	bool ok = true;

	/*if (!m_TaskData.IsComplete() || !m_TaskData.IsComplete())
	{
		YCodeJockMessageBox confirmation(
			this,
			DlgMessageBox::eIcon_Question,
			_YTEXT("The loading process is not over yet."),
			_YTEXT("It must be interrupted to continue."),
			L"",
			{ { IDOK, _YTEXT("OK") },{ IDCANCEL, _YTEXT("Cancel") } });

		if (IDOK == confirmation.DoModal())
		{
			m_TaskData.Cancel();
			ok = true;
		}
		else
		{
			ok = false;
		}
	}*/

	return ok;
}

void GridFrameBase::applyInPlaceFilter()
{
	// Prevent the grid from grabbing the focus during update (Bug #181128.BM.008F6E)
	const auto wasGrabFocus = GetGrid().IsGrabFocus();
	GetGrid().SetGrabFocus(false);
	GetGrid().AddFilter(GetGrid().GetColumnByFocus(), m_InPlaceFilterID, m_InPlaceFilter.c_str());
	GetGrid().SetGrabFocus(wasGrabFocus);

	updateInPlaceFilterApplyButton();
}

CString GridFrameBase::getInPlaceFilterBackendText()
{
	if (GetGrid().GetColumnByFocus())
	{
		if (ID_EXT_HEADER_FILTER_MENU_TF_CONTAINS == m_InPlaceFilterID)
			return GetGrid().GetColumnByFocus()->GetFilterAttribs().m_strTextFilterContains;
		else if (ID_EXT_HEADER_FILTER_MENU_TF_BEGINS_WITH == m_InPlaceFilterID)
			return GetGrid().GetColumnByFocus()->GetFilterAttribs().m_strTextFilterBeginsWith;
		else if (ID_EXT_HEADER_FILTER_MENU_TF_EQUALS == m_InPlaceFilterID)
			return GetGrid().GetColumnByFocus()->GetFilterAttribs().m_strTextFilterEquals;
		else
			ASSERT(false);
	}

	return _YTEXT("");
}

void GridFrameBase::updateInPlaceFilterApplyButton()
{
	if (nullptr != m_InPlaceFilterEdit)
	{
		if (m_InPlaceFilterAutoApply || m_InPlaceFilter == (LPCTSTR)getInPlaceFilterBackendText())
			m_InPlaceFilterEdit->ShowBuddyButton(0);
		else
			m_InPlaceFilterEdit->ShowBuddyButton(RIBBON_INPLACE_FILTER_BUTTON);
	}
}

void GridFrameBase::updateInPlaceFilterText()
{
	if (!isInPlaceFilterApplyShown() && !m_InPlaceFilterEdit->HasFocus())
	{
		m_InPlaceFilter = (LPCTSTR)getInPlaceFilterBackendText();
		m_InPlaceFilterEdit->SetEditText(m_InPlaceFilter.c_str());
	}
}

bool GridFrameBase::isInPlaceFilterApplyShown() const
{
	return nullptr != m_InPlaceFilterEdit && m_InPlaceFilterEdit->IsBuddyButtonVisible();
}

bool GridFrameBase::IsFirstLoad()
{
    return GetGrid().GetBackendRows().empty();
}

void GridFrameBase::setGridControlTooltip(CXTPControl* p_Control)
{
	ASSERT(nullptr != p_Control);
	if (nullptr != p_Control)
	{
		UINT commandID = p_Control->GetID();
		if (0 != commandID)
		{
			if (gridCommandsDuplicatesForRibbon.end() != gridCommandsDuplicatesForRibbon.find(commandID))
			{
				// Insert code here if we want different tooltips for duplicate commands!

				commandID = gridCommandsDuplicatesForRibbon.at(commandID);
			}

			ASSERT(0 != commandID);

			auto ribbonTooltip = GetGrid().GetRibbonTooltip(commandID);
			if (ribbonTooltip.m_Title.empty() && ribbonTooltip.m_Description.empty()
				&& nullptr != GetGrid().GetColumnOrg() && nullptr != GetGrid().GetColumnOrg()->GetGCVDGrid())
			{
				ribbonTooltip = GetGrid().GetColumnOrg()->GetGCVDGrid()->GetRibbonTooltip(commandID);
			}

			p_Control->SetTooltip(ribbonTooltip.m_Title.c_str());

			// If both are equal, description is ignored in tooltip. Add a space to trick the system.
			if (ribbonTooltip.m_Title == ribbonTooltip.m_Description)
				p_Control->SetDescription((ribbonTooltip.m_Description + _YTEXT(" ")).c_str());
			else
				p_Control->SetDescription(ribbonTooltip.m_Description.c_str());
		}
	}
}

bool GridFrameBase::HasNoTaskRunning() const
{
	return m_HasNoTaskRunning;
}

void GridFrameBase::SetRefreshMode(RefreshMode p_RefreshMode)
{
	m_RefreshMode = p_RefreshMode;
}

RefreshMode GridFrameBase::GetRefreshMode() const
{
	return m_RefreshMode;
}

bool GridFrameBase::ShouldForceFullRefreshAfterApply() const
{
	return m_ForceFullRefreshAfterApply;
}

bool GridFrameBase::HasNextRefreshSelectedData() const
{
	return m_NextRefreshSelectedData.is_initialized();
}

const RefreshSpecificData& GridFrameBase::GetNextRefreshSelectedData() const
{
	ASSERT(m_NextRefreshSelectedData);
	return *m_NextRefreshSelectedData;
}

bool GridFrameBase::HasLastUpdateOperations() const
{
	return !m_LastUpdateOperations.empty();
}

const vector<O365UpdateOperation>& GridFrameBase::GetLastUpdateOperations() const
{
	ASSERT(!m_LastUpdateOperations.empty());
	return m_LastUpdateOperations;
}

vector<O365UpdateOperation>& GridFrameBase::AccessLastUpdateOperations()
{
	ASSERT(!m_LastUpdateOperations.empty());
	return m_LastUpdateOperations;
}

void GridFrameBase::ForgetLastUpdateOperations()
{
	m_LastUpdateOperations.clear();
}

void GridFrameBase::SetRemoveRowsCallback(const std::function<void(const std::set<GridBackendRow*>&)>& p_Callback)
{
	m_RemoveRowsCallback = p_Callback;
}

void GridFrameBase::RefreshAfterUpdate(vector<O365UpdateOperation>&& p_O365UpdateOperations, AutomationAction* p_CurrentAction)
{
	SetDoLoadMoreOnNextRefresh(true);
	SetSyncNow(false);
	if (HasNextRefreshSelectedData())
		On365RefreshSelectionImpl(GetNextRefreshSelectedData(), std::move(p_O365UpdateOperations), false, p_CurrentAction);
	else
		On365RefreshImpl(std::move(p_O365UpdateOperations), p_CurrentAction);
}

void GridFrameBase::RefreshAfterUpdate(vector<O365UpdateOperation>&& p_O365UpdateOperations, const RefreshSpecificData& p_RefreshSpecificData, AutomationAction* p_CurrentAction)
{
	ASSERT(!HasDeltaFeature()); // It it fails, we should ensure it works and maybe remove the assert.

	// This bool is only used for Users and Groups right now, modules that normally don't use this method.
	// But just in case ...
	SetDoLoadMoreOnNextRefresh(true);

	if (HasNextRefreshSelectedData())
	{
		On365RefreshSelectionImpl(GetNextRefreshSelectedData(), std::move(p_O365UpdateOperations), false, p_CurrentAction);
	}
	else
	{
		RefreshSpecificData refreshSpecificData = p_RefreshSpecificData;

		ASSERT(refreshSpecificData.m_Criteria.m_IDs.empty() || (!refreshSpecificData.m_SelectedRowsAncestors || refreshSpecificData.m_SelectedRowsAncestors->size() == refreshSpecificData.m_Criteria.m_IDs.size()));
		if (!refreshSpecificData.m_Criteria.m_IDs.empty() && (!refreshSpecificData.m_SelectedRowsAncestors || refreshSpecificData.m_SelectedRowsAncestors->empty()))
		{
			const auto noList = !refreshSpecificData.m_SelectedRowsAncestors.is_initialized();
			if (noList)
				refreshSpecificData.m_SelectedRowsAncestors.emplace();
			vector<GridBackendRow*> rows;
			GetGrid().GetRowsByCriteria(rows, [](GridBackendRow* p_Row) {return nullptr != p_Row && 0 == p_Row->GetHierarchyLevel(); });
			for (auto row : rows)
			{
				if (refreshSpecificData.m_Criteria.m_IDs.end() != refreshSpecificData.m_Criteria.m_IDs.find(row->GetField(GetGrid().GetColId()).GetValueStr()))
					refreshSpecificData.m_SelectedRowsAncestors->insert(row);
			}
			if (noList && refreshSpecificData.m_SelectedRowsAncestors->empty())
				refreshSpecificData.m_SelectedRowsAncestors.reset();
		}
		
		On365RefreshSelectionImpl(refreshSpecificData, std::move(p_O365UpdateOperations), GetGrid().GetModifications().HasSubItemResults(), p_CurrentAction);
	}
}

void GridFrameBase::RefreshAfterSubItemUpdates(const RefreshSpecificData& p_RefreshSpecificData, AutomationAction* p_CurrentAction)
{
    // Are we coming from a "Refresh Selected" with "Save unapplied changes"?
    // If so, use the refresh data of the original selected data
    if (HasNextRefreshSelectedData())
    {
        On365RefreshSelectionImpl(GetNextRefreshSelectedData(), {}, true, p_CurrentAction);
    }
    else // We are coming from an apply, so we only refresh the "saved" data (p_RefreshSpecificData)
    {
        On365RefreshSelectionImpl(p_RefreshSpecificData, {}, true, p_CurrentAction);
    }
}

DlgBrowser* GridFrameBase::getOauth2Browser() const
{
	return m_oauth2Browser;
}

void GridFrameBase::setOauth2Browser(DlgBrowser* val)
{
	m_oauth2Browser = val;
}

void GridFrameBase::updateDumpModeDisplay()
{
	temporaryChangeStatusbarBackgroundColor(Sapio365Session::IsDumpGloballyEnabled(), RGB(255, 0, 0));
	temporarySetStatusbarLeftText(Sapio365Session::IsDumpGloballyEnabled(), MainFrame::g_DumpModeText, MainFrame::g_DumpModeTooltip);

	const wstring prefix = Sapio365Session::IsDumpGloballyEnabled() ? (MainFrame::g_DumpModeText + _YTEXT(" | ")) : _YTEXT("");
	SetWindowText((prefix + GetFrameTitle()).c_str());
}

bool GridFrameBase::showMessageBar(const wstring& p_Title, const wstring& p_Message, const wstring& /*p_Details*/, const std::unordered_map<UINT, CString>& buttons/* = {}*/, int p_ResIcon/* = NO_ICON*/, COLORREF p_CustomBackgroundColor /*= COLORREF(-1)*/)
{
	CString text;
	if (NO_ICON == p_ResIcon)
	{
#define XAML_NO_ICON \
	L"<StackPanel Orientation='Horizontal'>" \
	L"		<TextBlock Padding='10, 0, 0, 0' VerticalAlignment='Center'><Bold>%s</Bold></TextBlock>" \
	L"		<TextBlock Padding='10, 0, 0, 0' VerticalAlignment='Center'>%s</TextBlock>" \
	L"</StackPanel>" \

		text.Format(XAML_NO_ICON, p_Title.c_str(), p_Message.c_str());

#undef XAML_NO_ICON
	}
	else
	{
#define XAML_ICON \
	L"<StackPanel Orientation='Horizontal'>" \
	L"		<Image Source='res://#%s' VerticalAlignment='Top'/>" \
	L"		<TextBlock Padding='10, 0, 0, 0' VerticalAlignment='Center'><Bold>%s</Bold></TextBlock>" \
	L"		<TextBlock Padding='10, 0, 0, 0' VerticalAlignment='Center'>%s</TextBlock>" \
	L"</StackPanel>" \

		text.Format(XAML_ICON, Str::getStringFromNumber(p_ResIcon).c_str(), p_Title.c_str(), p_Message.c_str());

#undef XAML_ICON
	}

	return FrameBase<CodeJockFrameBase>::showMessageBar(text/*, p_Details.c_str()*/, buttons, p_CustomBackgroundColor);
}

void GridFrameBase::UpdateRefreshControls()
{
	if (nullptr != m_actionsRibbonTab)
	{
		auto group = findGroup(*m_actionsRibbonTab, g_ActionsDataGroup.c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			auto refreshCtrl = group->FindControl(ID_365REFRESH);
			if (nullptr == refreshCtrl)
			{
				if (GetGrid().WithLoadMore())
					refreshCtrl = group->Add(xtpControlSplitButtonPopup, ID_365REFRESH);
				else
					refreshCtrl = group->Add(xtpControlButton, ID_365REFRESH);

				// Already done in CreateRefreshGroup
				//setImage({ ID_365REFRESH }, IDB_REFRESH, xtpImageNormal);
				//setImage({ ID_365REFRESH }, IDB_REFRESH_16X16, xtpImageNormal);
			}

			ASSERT(nullptr != refreshCtrl);
			if (nullptr != refreshCtrl)
			{
				setRefreshControlTexts(refreshCtrl);

				if (GetGrid().WithLoadMore())
				{
					CXTPControlPopup* popup = dynamic_cast<CXTPControlPopup*>(refreshCtrl);
					ASSERT(nullptr != popup);
					if (nullptr != popup)
					{
						auto refreshCtrlPopup = popup->GetCommandBar()->GetControls()->FindControl(ID_365REFRESH);
						if (nullptr == refreshCtrlPopup)
							refreshCtrlPopup = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_365REFRESH);

						ASSERT(nullptr != refreshCtrlPopup);
						if (nullptr != refreshCtrlPopup)
						{
							setRefreshControlTexts(refreshCtrlPopup);

							if (HasDeltaFeature())
							{
								auto refreshResync = popup->GetCommandBar()->GetControls()->FindControl(ID_365REFRESH_RESYNCDELTA);
								if (nullptr == refreshResync)
									refreshResync = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_365REFRESH_RESYNCDELTA);
								if (nullptr != refreshResync)
								{
									setControlTexts(refreshResync,
										_T("Re-set Sync data"),
										_T("Re-set Sync data"),
										_T("Reload entire cache from the server. Additional information will remain untouched."));
								}
							}

							{
								auto refreshNoLoad = popup->GetCommandBar()->GetControls()->FindControl(ID_365REFRESH_ASFIRSTLOAD);
								if (nullptr == refreshNoLoad)
									refreshNoLoad = popup->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_365REFRESH_ASFIRSTLOAD);
								if (nullptr != refreshNoLoad)
								{
									// Specific text for delta?
									setControlTexts(refreshNoLoad,
										YtriaTranslate::Do(GridFrameBase_CreateRefreshGroup_1, _YLOC("Reinitialize entire data set")).c_str(),
										YtriaTranslate::Do(GridFrameBase_CreateRefreshGroup_1, _YLOC("Reinitialize entire data set")).c_str(),
										_T("Reload entire cache from the server. All data will be reset in the grid."));
								}
							}
						}
					}
				}
			}
		}
	}
}

vector<SelectedItem> GridFrameBase::AskForItemsToAdd(CWnd* p_Parent)
{
	ASSERT(IsConnected());
	wstring Explanation;
    auto loadType = DlgAddItemsToGroups::SELECT_USERS_FOR_SELECTION;
	// < ID, {}>
	std::map<PooledString, std::pair<PooledString, std::set<PooledString>>> alreadyLoadedIds;
	if (GetOrigin() == Origin::Group)
	{
		for (auto& id : m_ModuleCriteria.m_IDs)
			alreadyLoadedIds[id];
		loadType			= DlgAddItemsToGroups::SELECT_GROUPS_FOR_SELECTION;
		Explanation			= YtriaTranslate::Do(GridFrameBase_AskForItemsToAdd_2, _YLOC("The groups you select below will be added to the grid.\nTo see the complete list, use Load full directory.")).c_str();
	}
	else if (GetOrigin() == Origin::User)
	{
		for (auto& id : m_ModuleCriteria.m_IDs)
			alreadyLoadedIds[id];
		loadType			= DlgAddItemsToGroups::SELECT_USERS_FOR_SELECTION;
		Explanation			= YtriaTranslate::Do(GridFrameBase_AskForItemsToAdd_3, _YLOC("The users you select below will be added to the grid.\nTo see the complete list, use Load full directory.")).c_str();
	}
	else
        ASSERT(false);

    DlgAddItemsToGroups dlg( // DONE
		GetSapio365Session(),
		alreadyLoadedIds,
		loadType,
		{},
		{},
		{},
		YtriaTranslate::Do(GridFrameBase_AskForItemsToAdd_1, _YLOC("Add to selection")).c_str(),
		Explanation,
		m_ModuleCriteria.m_Privilege,
		p_Parent);
	dlg.DoModal();

    return dlg.GetSelectedItems();
}

RefreshSpecificData GridFrameBase::GetRefreshSelectedData()
{
    RefreshSpecificData data;
	data.m_SelectedRowsAncestors.emplace();
	for (auto selectedRow : GetGrid().GetSelectedRows())
	{
		if (!selectedRow->IsGroupRow())
			data.m_SelectedRowsAncestors->insert(selectedRow->GetTopAncestorOrThis());
	}
    data.m_Criteria = GetGrid().GetModuleCriteriaFromTopRows(std::vector<GridBackendRow*>(data.m_SelectedRowsAncestors->begin(), data.m_SelectedRowsAncestors->end()));

    return data;
}

bool GridFrameBase::GetAndResetSyncNow()
{
	return std::exchange(m_SyncNow, false);
}

void GridFrameBase::SetSyncNow(bool p_Val)
{
	ASSERT(!p_Val || HasDeltaFeature());
	m_SyncNow = p_Val;
}

void GridFrameBase::SetDoLoadMoreOnNextRefresh(bool p_Val)
{
	m_NextRefreshLoadMore = p_Val;
}

bool GridFrameBase::GetAndResetDoLoadMoreOnNextRefresh()
{
	return std::exchange(m_NextRefreshLoadMore, false);
}

bool GridFrameBase::HasDeltaFeature() const
{
	return false;
}

LRESULT GridFrameBase::OnOAuth2BrowserClose(WPARAM wParam, LPARAM lParam)
{
	if (nullptr != m_oauth2Browser)
		m_oauth2Browser->EndDialog(IDOK);
	return 0;
}

LRESULT GridFrameBase::OnSetOAuth2Browser(WPARAM wParam, LPARAM lParam)
{
	setOauth2Browser(reinterpret_cast<DlgBrowser*>(wParam));
	return 0;
}

LRESULT GridFrameBase::OnGetOAuth2Browser(WPARAM wParam, LPARAM lParam)
{
	return reinterpret_cast<LRESULT>(getOauth2Browser());
}

LRESULT GridFrameBase::OnAuthenticationSuccess(WPARAM wParam, LPARAM lParam)
{
	CWaitCursor cursor;
	if (0 != wParam)
	{
		// This contains API Name
		wstring* pTemp = reinterpret_cast<wstring*>(wParam);
		delete pTemp;
	}

	bool success = true;

	// Reactivate role if any
	auto session = GetSapio365Session();
	if (session && session->IsUseRoleDelegation())
	{
		if (!session->SetUseRoleDelegation(session->GetRoleDelegationID(), this))
		{
			success = false;
			// Can't reactivate role. Go back to disconnected state.
			Sapio365Session::SignOut(session, this);
		}
	}

	auto grid = dynamic_cast<O365Grid*>(&GetGrid());
	ASSERT(nullptr != grid);
	if (nullptr != grid)
		grid->InitRoleDelegation();

	if (success && session)
	{
		// Bug #190110.RL.0098AA
		// Saving the session so it can be automatically opened if the user so desires.

		PersistentSession persistentSession = SessionsSqlEngine::Get().Load(session->GetIdentifier());
		if (session->IsUltraAdmin())
		{
			ASSERT(!m_UltraAdminAppID.empty());
			persistentSession.SetAppId(m_UltraAdminAppID);
			ASSERT(!m_UltraAdminSecretKey.empty());
			if (!m_UltraAdminSecretKey.empty())
				persistentSession.SetAppClientSecret(m_UltraAdminSecretKey);
			if (!m_UltraAdminTenant.empty())
				persistentSession.SetTenantName(m_UltraAdminTenant);

			ASSERT(SessionsSqlEngine::Get().SessionExists({ m_UltraAdminAppID, SessionTypes::g_UltraAdmin, 0 }));
			if (SessionsSqlEngine::Get().SessionExists({ m_UltraAdminAppID, SessionTypes::g_UltraAdmin, 0 }))
			{
				PersistentSession persistentSession = SessionsSqlEngine::Get().Load({ m_UltraAdminAppID, SessionTypes::g_UltraAdmin, 0 });
				ASSERT(persistentSession.IsValid());
				if (persistentSession.IsValid())
				{
					ASSERT(persistentSession.GetTenantName() == m_UltraAdminTenant);
					if (persistentSession.GetSessionName() != m_UltraAdminDisplayName)
					{
						persistentSession.SetSessionName(m_UltraAdminDisplayName);
						SessionsSqlEngine::Get().Save(persistentSession);
					}
				}
			}
		}

		auto authenticator = dynamic_cast<OAuth2AuthenticatorBase*>(session->GetMainMSGraphSession()->GetAuthenticator().get());
		ASSERT(authenticator);
		if (authenticator)
			session->GetMainMSGraphSession()->GetSessionPersister()->Save(authenticator->GetOAuth2Config());

		Sapio365Session::SetSignedIn(session, this);
	}

	WindowsListUpdater::GetInstance().Update();

	return 0;
}

LRESULT GridFrameBase::OnAuthenticationCanceled(WPARAM wParam, LPARAM lParam)
{
	// Automation green light?

	if (0 != wParam)
	{
		// This contains API Name
		wstring* pTemp = reinterpret_cast<wstring*>(wParam);
		delete pTemp;
	}

	return 0;
}

LRESULT GridFrameBase::OnAuthenticationFailure(WPARAM wParam, LPARAM lParam)
{
	// Automation green light?

	if (0 != wParam)
	{
		// This contains API Name
		wstring* pTemp = reinterpret_cast<wstring*>(wParam);
		delete pTemp;
	}

	return 0;
}

void GridFrameBase::SetTaskComplete(const bool p_Complete)
{
	m_HasNoTaskRunning = p_Complete;
}

AutomationContext GridFrameBase::GetAutomationContext()
{
	return AutomationContext(&GetGrid(), this);
}

void GridFrameBase::ProcessLicenseContext()
{
	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
	{
		auto lc = GetLicenseContext();// COPY
		lc.SetTokenUnitsToCharge(GetGrid().GetRowDataCount());
		lc.SetReasonForTokenCharging(_YFORMAT(L"%s rows", std::to_wstring(GetGrid().GetRowDataCount()).c_str()));
		lc.SetAlreadyChargedTokenAmount(getAlreadyChargedTokenAmount());
		auto result = mainFrame->ProcessLicenseContext(lc);
		setAlreadyChargedTokenAmount(getAlreadyChargedTokenAmount() + result.m_ChargedTokens);
	}
}

AutomationAction* GridFrameBase::GetAutomationAction() const
{
	return m_AutomationActionRequestingExecutionPermit;
}

void GridFrameBase::AutomationCleanup()
{
	m_AutomationActionRequestingExecutionPermit = nullptr;
	SetMotherAutomationAction(nullptr);
	if (GetSapio365Session())
		GetSapio365Session()->SetCommandLineAutomationInProgress(false);
}

void GridFrameBase::OnToggleRemoveRBACOutOfScope()
{
	if (!AutomatedApp::IsAutomationRunning())
	{
		const auto session = GetSapio365Session();
		ASSERT(session && session->IsUseRoleDelegation());
		if (session && session->IsUseRoleDelegation())
		{
			m_RBACHideOutOfScope = !m_RBACHideOutOfScope;
			GetGrid().UpdateRBACinfo(m_RBACHideOutOfScope);
			m_RBACHideOutOfScopeBTN->SetChecked(m_RBACHideOutOfScope);

			getSessionInfo().SetRoleShowOwnScopeOnly(m_RBACHideOutOfScope);
			SessionsSqlEngine::Get().Save(GetSessionInfo());
			session->SetRBACHideUnscopedObjects(m_RBACHideOutOfScope);
		}
	}
}

bool GridFrameBase::IsRBACHideUnscopedObjects() const
{
	return m_RBACHideOutOfScope;
}

const boost::YOpt<MetaDataColumnInfos>& GridFrameBase::GetMetaDataColumnInfo() const
{
	return m_ModuleCriteria.m_MetaDataColumnInfo;
}

void GridFrameBase::OnUpdateToggleRBACRemoveOutOfScope(CCmdUI* pCmdUI)
{
	const auto s = GetSapio365Session();
	pCmdUI->Enable(s && s->IsUseRoleDelegation());
}

void GridFrameBase::OnShowErrorMessageBox()
{
	//removeMessageBar();
	GetGrid().ShowErrorMessage(m_LastErrorInfo.m_Title, m_LastErrorInfo.m_Message, m_LastErrorInfo.m_Details);
}

void GridFrameBase::OnEditElevated()
{
	if (IsConnected()
		&&
		(	GetSessionInfo().IsElevated()
		||	GetSessionInfo().IsAdvanced()
		||	GetSessionInfo().IsPartnerAdvanced()
		||	GetSessionInfo().IsPartnerElevated()))
	{
		auto mainframe = dynamic_cast<MainFrame*>(::AfxGetApp()->m_pMainWnd);
		ASSERT(mainframe);
		if (mainframe)
			mainframe->EditAndLoadUltraAdmin(this);
	}
}

void GridFrameBase::OnUpdateEditElevated(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsConnected()
		&& HasNoTaskRunning()
		&& (GetSessionInfo().IsElevated()
		|| GetSessionInfo().IsAdvanced()
		|| GetSessionInfo().IsPartnerAdvanced()
		|| GetSessionInfo().IsPartnerElevated()));
}

void GridFrameBase::OnTokenBuy()
{
	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	mainFrame->TokenBuy();
}

void GridFrameBase::ApplyAJLLicenseAccess()
{
	// conclusion of job execution
	// postAutomateSpecific is not the right place (runs after each frame automation)
	GetGrid().SetLicenseDisableCommand(true);
}

boost::YOpt<wstring> GridFrameBase::GetUsersDeltaLink() const
{
	return m_UsersDeltaLink;
}

boost::YOpt<wstring> GridFrameBase::GetGroupsDeltaLink() const
{
	return m_GroupsDeltaLink;
}

void GridFrameBase::SetUsersDeltaLink(const boost::YOpt<wstring>& p_Link)
{
	m_UsersDeltaLink = p_Link;
}

void GridFrameBase::SetGroupsDeltaLink(const boost::YOpt<wstring>& p_Link)
{
	m_GroupsDeltaLink = p_Link;
}

// =================================================================================================
// =================================================================================================

const std::map<UINT, GridFrameBase::GridCommandImages> GridFrameBase::g_GridCommandImages32x32
{
	{ CACHEGRID_MENU_FREEZECOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FREEZECOLUMN, 0 } },
	{ CACHEGRID_MENU_FREEZEUPTO,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FREEZEUPTO, 0 } },
	{ CACHEGRID_MENU_UNFREEZE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_UNFREEZE, 0 } },
	{ CACHEGRID_MENU_COLLAPSEALL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COLLAPSEALL, 0 } },
	{ CACHEGRID_MENU_EXPANDALL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL, 0 } },
	{ CACHEGRID_MENU_COLLAPSELEVEL1,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COLLAPSELEVEL1, 0 } },
	{ CACHEGRID_MENU_EXPANDLEVEL1,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDLEVEL1, 0 } },
	{ CACHEGRID_MENU_UNGROUPALL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_UNGROUPALL, 0 } },
	{ CACHEGRID_MENU_SELECTALL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SELECTALL, 0 } },
	{ CACHEGRID_MENU_SELECTIONINVERSION,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SELECTIONINVERSION, 0 } },
	{ CACHEGRID_MENU_COPY,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COPY, 0 } },
	{ CACHEGRID_MENU_EXPORT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPORT, 0 } },
	{ CACHEGRID_MENU_PIVOT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_PIVOT, 0 } },
	{ CACHEGRID_MENU_SORTBYCOUNT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTBYCOUNT, RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTBYCOUNT_CHECKED } },
	{ CACHEGRID_MENU_SHOW_TITLES,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOW_TITLES, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOW_TITLES_CHECKED } },
	{ CACHEGRID_MENU_CLEARFILTERS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CLEARALLFILTERS, 0 } },
	{ CACHEGRID_MENU_SEARCH,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SEARCH, 0 } },
	{ CACHEGRID_MENU_FIND,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FIND, 0 } },
	{ CACHEGRID_MENU_FIND_NEXT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FIND_NEXT, 0 } },
	//{ CACHEGRID_MENU_TOGGLECHECKBOXES,{ 0, 0 } },
	//{ CACHEGRID_MENU_BEDELETE,{ 0, 0 } },
	//{ CACHEGRID_MENU_BEAPPLYCHANGES,{ 0, 0 } },
	//{ CACHEGRID_MENU_BEAPPLYCHANGESTOSELECTED,{ 0, 0 } },
	//{ CACHEGRID_MENU_BECREATE,{ 0, 0 } },
	{ CACHEGRID_MENU_EXPORTANDCOPYPREFERENCES,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPORTANDCOPYPREFERENCES, 0 } },
	{ CACHEGRID_MENU_RESTOREGRID,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESTOREGRID, 0 } },
	{ CACHEGRID_MENU_REINITGRID,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REINITGRID, 0 } },
	{ CACHEGRID_MENU_SHOWGCVD,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWGCVD, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWGCVD_CHECKED } },
	{ CACHEGRID_MENU_COPYFOCUSEDCOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COPYFOCUSEDCOLUMN, 0 } },
	{ CACHEGRID_MENU_GCVDSAVE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GCVDSAVE, 0 } },
	{ CACHEGRID_MENU_GCVDLOAD,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GCVDLOAD, 0 } },
	{ CACHEGRID_MENU_GCVDRESETTOSETTINGSUSER,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GCVDRESETTOSETTINGSUSER, 0 } },
	{ CACHEGRID_MENU_GCVDRESETTOSETTINGSYTRIA,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GCVDRESETTOSETTINGSYTRIA, 0 } },
	{ CACHEGRID_MENU_GCVDSAVEASDEFAULT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GCVDSAVEASDEFAULT, 0 } },
	{ CACHEGRID_MENU_NEXTGROUPUP,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_NEXTGROUPUP, 0 } },
	{ CACHEGRID_MENU_NEXTGROUPDOWN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_NEXTGROUPDOWN, 0 } },
	{ CACHEGRID_MENU_HIDEROWS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEROWS, 0 } },
	{ CACHEGRID_MENU_REMOVEROWSHIDDEN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVEROWSHIDDEN, 0 } },
	{ CACHEGRID_MENU_REMOVEROWSFILTERED,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVEROWSFILTERED, 0 } },
	{ CACHEGRID_MENU_RESTOREHIDDENROWS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESTOREHIDDENROWS, 0 } },
	{ CACHEGRID_MENU_HIGHLIGHTCOLUMNS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIGHLIGHTCOLUMNS, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIGHLIGHTCOLUMNS_CHECKED } },
	//{ CACHEGRID_MENU_GCVD_RESIZEVISIBLE,{ 0, 0 } },
	{ CACHEGRID_MENU_SHOW_TITLES_FOCUSCOL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOW_TITLES_FOCUSCOL, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOW_TITLES_FOCUSCOL_CHECKED } },
	{ CACHEGRID_MENU_HIDEEMTPYGROUPS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEEMTPYGROUPS, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEEMTPYGROUPS_CHECKED } },
	{ CACHEGRID_MENU_HIDEEMTPYHIERARCHIES,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEEMTPYHIERARCHIES, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEEMTPYHIERARCHIES_CHECKED } },
	//{ CACHEGRID_MENU_COLUMNCBXCHECK,{ 0, 0 } },
	//{ CACHEGRID_MENU_COLUMNCBXUNCHECK,{ 0, 0 } },
	{ CACHEGRID_MENU_TRAVELSELMODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_TRAVELSELMODE, 0 } },
	{ CACHEGRID_MENU_ANNOTATION_EDIT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_EDIT, 0 } },
	{ CACHEGRID_MENU_ANNOTATION_DELETE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_DELETE, 0 } },
	{ CACHEGRID_MENU_ANNOTATION_VIEWALL_LIVE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_LIST, 0 } },
	{ CACHEGRID_MENU_ANNOTATION_VIEWALL_ORPHANS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_LISTORPHANS, 0 } },
	{ CACHEGRID_MENU_ANNOTATION_REFRESH,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_REFRESH, 0 } },
	{ CACHEGRID_MENU_SHOWCOLUMNINFO,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWCOLUMNINFO, 0 } },
	{ CACHEGRID_MENU_SETVLALUEFILTERFROMFILE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_VALUEFILTERFROMFILE, 0 } },
	{ CACHEGRID_MENU_SELECTFROMFILE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_VALUESELECTFROMFILE, 0 } },
	{ CACHEGRID_MENU_COMPARATOR,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COMPARATOR, 0 } },
	{ CACHEGRID_MENU_COMPARATORREMOVECOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COMPARATORREMOVECOLUMN, 0 } },
	{ CACHEGRID_MENU_COMPARATORREMOVEROWS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COMPARATORREMOVEROWS, 0 } },
	//{ CACHEGRID_MENU_GCVD_RESIZEHEADER,{ 0, 0 } },
	{ CACHEGRID_MENU_SORTBYTOTAL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTBYTOTAL, RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTBYTOTAL_CHECKED } },
	{ CACHEGRID_MENU_TOGGLEGROUPANDTOOLS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLEGROUPANDTOOLS, RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLEGROUPANDTOOLS_CHECKED } },
	//{ CACHEGRID_MENU_FREECOLUMNFORACC,{ 0, 0 } },
	{ CACHEGRID_MENU_HEADER_FILTER_MENU_EDIT_SETUP,{ RPROD_BMP_RIBBON_COLORG_EDIT_ROW, 0 } },
	{ CACHEGRID_MENU_HEADER_FILTER_MENU_CLEAR,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CLEARFILTER, 0 } },
	{ CACHEGRID_MENU_HEADER_FILTER_MENU_REGEX,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HEADER_FILTER_MENU_REGEX, RPROD_BMP_RIBBON_CACHEGRID_MENU_HEADER_FILTER_MENU_REGEX_CHECKED } },
	{ CACHEGRID_MENU_HEADER_FILTER_MENU_EMPTY,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HEADER_FILTER_MENU_EMPTY, RPROD_BMP_RIBBON_CACHEGRID_MENU_HEADER_FILTER_MENU_EMPTY_CHECKED } },
	{ CACHEGRID_MENU_HEADER_FILTER_VALUES,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HEADER_FILTER_VALUES, 0 } },
	{ CACHEGRID_MENU_PAUSE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_PAUSE, RPROD_BMP_RIBBON_CACHEGRID_MENU_PAUSE_CHECKED } },
	{ CACHEGRID_MENU_HIDECOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDECOLUMN, 0 } },
	{ CACHEGRID_MENU_GROUPCOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GROUPCOLUMN, RPROD_BMP_RIBBON_CACHEGRID_MENU_GROUPCOLUMN_CHECKED } },
	{ CACHEGRID_MENU_SUSPENDUPDATES,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SUSPENDUPDATES, 0 } },
	{ CACHEGRID_MENU_UNDOHIDECOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_UNDOHIDECOLUMN, 0 } },
	{ CACHEGRID_MENU_VIEWHIDDENROWSROWS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_VIEWHIDDENROWSROWS, RPROD_BMP_RIBBON_CACHEGRID_MENU_VIEWHIDDENROWSROWS_CHECKED } },
	{ CACHEGRID_MENU_TOGGLESTATUSBAR,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLESTATUSBAR, RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLESTATUSBAR_CHECKED } },
	//{ CACHEGRID_MENU_STATUSBAR_TOTAL,{ 0, 0 } },
	//{ CACHEGRID_MENU_STATUSBAR_VISIBLE,{ 0, 0 } },
	//{ CACHEGRID_MENU_STATUSBAR_HIDDEN,{ 0, 0 } },
	//{ CACHEGRID_MENU_STATUSBAR_SELECTED,{ 0, 0 } },
	//{ CACHEGRID_MENU_STATUSBAR_ZEROS,{ 0, 0 } },
	//{ CACHEGRID_MENU_STATUSBAR_SETASDEFAULT,{ 0, 0 } },
	//{ CACHEGRID_MENU_CONVERTTODATE,{ 0, 0 } },
	//{ CACHEGRID_MENU_CONVERTTONUMBER,{ 0, 0 } },
	{ CACHEGRID_MENU_CHART,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CHART, 0 } },
	{ CACHEGRID_MENU_CELLFORMAT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CELLFORMAT, 0 } },
	{ CACHEGRID_MENU_GROUPFORMAT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GROUPFORMAT, 0 } },
	{ CACHEGRID_MENU_RESIZEVISIBLE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESIZEVISIBLE, 0 } },
	{ CACHEGRID_MENU_RESIZEDATASET,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESIZEDATASET, 0 } },
	{ CACHEGRID_MENU_RESIZEHEADER,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESIZEHEADER, 0 } },
	{ CACHEGRID_MENU_RESIZEHEADDATA,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESIZEHEADDATA, 0 } },
	{ CACHEGRID_MENU_RESIZEDEFAULT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESIZEDEFAULT, 0 } },
	{ CACHEGRID_MENU_ASCSORTBYYSELECTEDCOL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTASCEND, 0 } },
	{ CACHEGRID_MENU_DESCSORTBYYSELECTEDCOL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTDESCEND, 0 } },
	{ CACHEGRID_MENU_REMOVESELECTEDCOLUMNSORTING,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVESORTING, 0 } },

	{ CACHEGRID_MENU_MULTIVALUE_EXPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPLODEMULTIVALUES, 0 } },
	{ CACHEGRID_MENU_MULTIVALUE_IMPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODEMULTIVALUES, 0 } },
	{ CACHEGRID_MENU_MULTIVALUE_IMPLODE_GRID,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODEALLMUTLIVALUES, 0 } },
	{ CACHEGRID_MENU_SORTMULTIVALUESBYCOUNT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTMULTIVALUES, 0 } },

	{ CACHEGRID_MENU_CONVERTTODATE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CONVERTTODATE, 0 } },
	{ CACHEGRID_MENU_CONVERTTONUMBER,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CONVERTTONUM, 0 } },
	{ CACHEGRID_MENU_REMOVEDATECONVERSION,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVECONVERSIONDATE, 0 } },
	{ CACHEGRID_MENU_REMOVENUMBERCONVERSION,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVECONVERSIONNUMBER, 0 } },
	{ CACHEGRID_MENU_REMOVEALLCONVERSIONS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVECONVERSIONALL, 0 } },

	{ CACHEGRID_MENU_DATES_EXPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPLODEDATES, 0 } },
	{ CACHEGRID_MENU_DATES_IMPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODEDATES, 0 } },
	{ CACHEGRID_MENU_TIMES_EXPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPLODETIMES, 0 } },
	{ CACHEGRID_MENU_TIMES_IMPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODETIMES, 0 } },
	{ CACHEGRID_MENU_DATE_IMPLODE_GRID,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODEALLDATES, 0 } },
	{ CACHEGRID_MENU_TIME_IMPLODE_GRID,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODEALLTIMES, 0 } },
	{ CACHEGRID_MENU_HIERARCHYTOGGLE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLEHIERARCHYVIEW, RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLEHIERARCHYVIEW_CHECKED } },

	{ CACHEGRID_MENU_SETCOLUMNAUTORESIZE ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_AUTORESIZE, RPROD_BMP_RIBBON_CACHEGRID_MENU_AUTORESIZE_CHECKED } },
	{ CACHEGRID_MENU_SETCOLUMNTOTAL ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SETCOLUMNTOTAL, RPROD_BMP_RIBBON_CACHEGRID_MENU_SETCOLUMNTOTAL_CHECKED } },
	{ CACHEGRID_MENU_HIDESINGLEROWGROUPS ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDESINGLEROWGROUPS, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDESINGLEROWGROUPS_CHECKED } },
	{ CACHEGRID_MENU_HIDEGROUPCOUNT ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEGROUPCOUNT, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEGROUPCOUNT_CHECKED } },

	{ CACHEGRID_MENU_SHOWHIERARCHYPOSITIONCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYPOSITIONCOLUMN, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYPOSITIONCOLUMN_CHECKED } },
	{ CACHEGRID_MENU_SHOWHIERARCHYLEVELCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYLEVELCOLUMN, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYLEVELCOLUMN_CHECKED } },
	{ CACHEGRID_MENU_SHOWHIERARCHYNUMCHILDRENCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYNUMCHILDRENCOLUMN, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYNUMCHILDRENCOLUMN_CHECKED } },
	{ CACHEGRID_MENU_SHOWHIERARCHYNUMDESCENDANTSCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYNUMDESCENDANTSCOLUMN, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYNUMDESCENDANTSCOLUMN_CHECKED } },
	{ CACHEGRID_MENU_SHOWHIERARCHYKEYCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYKEYCOLUMN, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYKEYCOLUMN_CHECKED } },
	{ CACHEGRID_MENU_SHOWHIERARCHYHIDDENDESCCOLUMN , { RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYHIDDENDESCCOLUMN, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYHIDDENDESCCOLUMN_CHECKED } },
	{ CACHEGRID_MENU_SHOWHIERARCHYVISBLEDESCCOLUMN , { RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYVISIBLEDESCCOLUMN, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYVISIBLEDESCCOLUMN_CHECKED } },

	{ CACHEGRID_MENU_DISPLAYMULTIVALUENUMBER ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_DISPLAYMULTIVALUENUMBER, RPROD_BMP_RIBBON_CACHEGRID_MENU_DISPLAYMULTIVALUENUMBER_CHECKED } },

	{ CACHEGRID_MENU_FILTERDLG_TEXT ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_TEXT, RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_TEXT_CHECKED } },
	{ CACHEGRID_MENU_FILTERDLG_NUMBER ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_NUMBER, RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_NUMBER_CHECKED } },
	{ CACHEGRID_MENU_FILTERDLG_DATE ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_DATE, RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_DATE_CHECKED } },
	{ CACHEGRID_MENU_FILTERDLG_TIME ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_TIME, RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_TIME_CHECKED } },
	{ CACHEGRID_MENU_FILTERDLG_CUTOFF ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_CUTOFF, RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_CUTOFF_CHECKED } },

	{ CACHEGRID_MENU_STATS01_THREESIGMA, { RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS01_THREESIGMA, 0 } },
	{ CACHEGRID_MENU_STATS02_CENTILES, { RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS02_CENTILES, 0 } },
	{ CACHEGRID_MENU_STATS03_TOPBOTTOM, { RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS03_TOPBOTTOM, 0 } },
	{ CACHEGRID_MENU_STATS04_DELTA20, { RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS04_DELTA20, 0 } },
	{ CACHEGRID_MENU_STATS05_DELTAVAL, { RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS05_DELTAVAL, 0 } },
	{ CACHEGRID_MENU_STATS06_THREESIGMA_COUNT, { RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS06_THREESIGMA_COUNT, 0 } },
	{ CACHEGRID_MENU_STATS07_CENTILES_COUNT, { RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS07_CENTILES_COUNT, 0 } },
	{ CACHEGRID_MENU_STATS08_TOPBOTTOM_COUNT, { RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS08_TOPBOTTOM_COUNT, 0 } },
	{ CACHEGRID_MENU_STATS09_DELTA20_COUNT, { RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS09_DELTA20_COUNT, 0 } },
	{ CACHEGRID_MENU_STATS10_DELTAVAL_COUNT, { RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS10_DELTAVAL_COUNT, 0 } },
	{ CACHEGRID_MENU_STATSREMOVE,  { RPROD_BMP_RIBBON_CACHEGRID_MENU_STATSREMOVE, 0 } },

	{ CACHEGRID_MENU_DUPLICATESSHOW,  { RPROD_BMP_RIBBON_CACHEGRID_MENU_DUPLICATES_SHOW, 0 } },
	{ CACHEGRID_MENU_DUPLICATERSEMOVE,  { RPROD_BMP_RIBBON_CACHEGRID_MENU_DUPLICATES_REMOVE, 0 } },

	{ ID_GRIDFRAMEBASE_CACHEGRID_MENU_FILTERDLG_TEXT,{ IDB_GRIDFRAMEBASE_CACHEGRID_MENU_FILTERDLG_TEXT, 0 } },

	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL1,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL1, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL2,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL2, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL3,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL3, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL4,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL4, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL5,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL5, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL6,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL6, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL7,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL7, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL8,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL8, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL9,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL9, 0 } },
};

const std::map<UINT, GridFrameBase::GridCommandImages> GridFrameBase::g_GridCommandImages16x16
{
	{ CACHEGRID_MENU_FREEZECOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FREEZECOLUMN_16X16, 0 } },
	{ CACHEGRID_MENU_FREEZEUPTO,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FREEZEUPTO_16X16, 0 } },
	{ CACHEGRID_MENU_UNFREEZE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_UNFREEZE_16X16, 0 } },
	{ CACHEGRID_MENU_COLLAPSEALL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COLLAPSEALL_16X16, 0 } },
	{ CACHEGRID_MENU_EXPANDALL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL_16X16, 0 } },
	{ CACHEGRID_MENU_COLLAPSELEVEL1,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COLLAPSELEVEL1_16X16, 0 } },
	{ CACHEGRID_MENU_EXPANDLEVEL1,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDLEVEL1_16X16, 0 } },
	{ CACHEGRID_MENU_UNGROUPALL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_UNGROUPALL_16X16, 0 } },
	{ CACHEGRID_MENU_SELECTALL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SELECTALL_16X16, 0 } },
	{ CACHEGRID_MENU_SELECTIONINVERSION,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SELECTIONINVERSION_16X16, 0 } },
	{ CACHEGRID_MENU_COPY,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COPY_16X16, 0 } },
	{ CACHEGRID_MENU_EXPORT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPORT_16X16, 0 } },
	{ CACHEGRID_MENU_PIVOT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_PIVOT_16X16, 0 } },
	{ CACHEGRID_MENU_SORTBYCOUNT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTBYCOUNT_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTBYCOUNT_CHECKED_16X16 } },
	{ CACHEGRID_MENU_SHOW_TITLES,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOW_TITLES_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOW_TITLES_CHECKED_16X16 } },
	{ CACHEGRID_MENU_CLEARFILTERS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CLEARALLFILTERS_16X16, 0 } },
	{ CACHEGRID_MENU_SEARCH,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SEARCH_16X16, 0 } },
	{ CACHEGRID_MENU_FIND,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FIND_16X16, 0 } },
	{ CACHEGRID_MENU_FIND_NEXT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FIND_NEXT_16X16, 0 } },
	//{ CACHEGRID_MENU_TOGGLECHECKBOXES,{ 0, 0 } },
	//{ CACHEGRID_MENU_BEDELETE,{ 0, 0 } },
	//{ CACHEGRID_MENU_BEAPPLYCHANGES,{ 0, 0 } },
	//{ CACHEGRID_MENU_BEAPPLYCHANGESTOSELECTED,{ 0, 0 } },
	//{ CACHEGRID_MENU_BECREATE,{ 0, 0 } },
	{ CACHEGRID_MENU_EXPORTANDCOPYPREFERENCES,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPORTANDCOPYPREFERENCES_16X16, 0 } },
	{ CACHEGRID_MENU_RESTOREGRID,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESTOREGRID_16X16, 0 } },
	{ CACHEGRID_MENU_REINITGRID,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REINITGRID_16X16, 0 } },
	{ CACHEGRID_MENU_SHOWGCVD,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWGCVD_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWGCVD_CHECKED_16X16 } },
	{ CACHEGRID_MENU_COPYFOCUSEDCOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COPYFOCUSEDCOLUMN_16X16, 0 } },
	{ CACHEGRID_MENU_GCVDSAVE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GCVDSAVE_16X16, 0 } },
	{ CACHEGRID_MENU_GCVDLOAD,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GCVDLOAD_16X16, 0 } },
	{ CACHEGRID_MENU_GCVDRESETTOSETTINGSUSER,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GCVDRESETTOSETTINGSUSER_16X16, 0 } },
	{ CACHEGRID_MENU_GCVDRESETTOSETTINGSYTRIA,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GCVDRESETTOSETTINGSYTRIA_16X16, 0 } },
	{ CACHEGRID_MENU_GCVDSAVEASDEFAULT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GCVDSAVEASDEFAULT_16X16, 0 } },
	{ CACHEGRID_MENU_NEXTGROUPUP,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_NEXTGROUPUP_16X16, 0 } },
	{ CACHEGRID_MENU_NEXTGROUPDOWN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_NEXTGROUPDOWN_16X16, 0 } },
	{ CACHEGRID_MENU_HIDEROWS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEROWS_16X16, 0 } },
	{ CACHEGRID_MENU_REMOVEROWSHIDDEN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVEROWSHIDDEN_16X16, 0 } },
	{ CACHEGRID_MENU_REMOVEROWSFILTERED,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVEROWSFILTERED_16X16, 0 } },
	{ CACHEGRID_MENU_RESTOREHIDDENROWS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESTOREHIDDENROWS_16X16, 0 } },
	{ CACHEGRID_MENU_HIGHLIGHTCOLUMNS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIGHLIGHTCOLUMNS_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIGHLIGHTCOLUMNS_CHECKED_16X16 } },
	//{ CACHEGRID_MENU_GCVD_RESIZEVISIBLE,{ 0, 0 } },
	{ CACHEGRID_MENU_SHOW_TITLES_FOCUSCOL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOW_TITLES_FOCUSCOL_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOW_TITLES_FOCUSCOL_CHECKED_16X16 } },
	{ CACHEGRID_MENU_HIDEEMTPYGROUPS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEEMTPYGROUPS_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEEMTPYGROUPS_CHECKED_16X16 } },
	{ CACHEGRID_MENU_HIDEEMTPYHIERARCHIES,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEEMTPYHIERARCHIES_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEEMTPYHIERARCHIES_CHECKED_16X16 } },
	//{ CACHEGRID_MENU_COLUMNCBXCHECK,{ 0, 0 } },
	//{ CACHEGRID_MENU_COLUMNCBXUNCHECK,{ 0, 0 } },
	{ CACHEGRID_MENU_TRAVELSELMODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_TRAVELSELMODE_16X16, 0 } },
	{ CACHEGRID_MENU_ANNOTATION_EDIT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_EDIT_16X16, 0 } },
	{ CACHEGRID_MENU_SHOWCOLUMNINFO,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWCOLUMNINFO_16X16, 0 } },
	{ CACHEGRID_MENU_SETVLALUEFILTERFROMFILE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_VALUEFILTERFROMFILE_16X16, 0 } },
	{ CACHEGRID_MENU_SELECTFROMFILE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_VALUESELECTFROMFILE_16X16, 0 } },
	{ CACHEGRID_MENU_ANNOTATION_DELETE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_DELETE_16X16, 0 } },
	{ CACHEGRID_MENU_ANNOTATION_VIEWALL_LIVE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_LIST_16X16, 0 } },
	{ CACHEGRID_MENU_ANNOTATION_VIEWALL_ORPHANS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_LISTORPHANS_16X16, 0 } },
	{ CACHEGRID_MENU_ANNOTATION_EDIT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_REFRESH_16X16, 0 } },
	{ CACHEGRID_MENU_COMPARATOR,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COMPARATOR_16X16, 0 } },
	{ CACHEGRID_MENU_COMPARATORREMOVECOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COMPARATORREMOVECOLUMN_16X16, 0 } },
	{ CACHEGRID_MENU_COMPARATORREMOVEROWS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_COMPARATORREMOVEROWS_16X16, 0 } },
	//{ CACHEGRID_MENU_GCVD_RESIZEHEADER,{ 0, 0 } },
	{ CACHEGRID_MENU_SORTBYTOTAL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTBYTOTAL_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTBYTOTAL_CHECKED_16X16 } },
	{ CACHEGRID_MENU_TOGGLEGROUPANDTOOLS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLEGROUPANDTOOLS_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLEGROUPANDTOOLS_CHECKED_16X16 } },
	//{ CACHEGRID_MENU_FREECOLUMNFORACC,{ 0, 0 } },
	{ CACHEGRID_MENU_HEADER_FILTER_MENU_EDIT_SETUP,{ RPROD_BMP_RIBBON_COLORG_EDIT_ROW_16X16, 0 } },
	{ CACHEGRID_MENU_HEADER_FILTER_MENU_CLEAR,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CLEARFILTER_16X16, 0 } },
	{ CACHEGRID_MENU_HEADER_FILTER_MENU_REGEX,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HEADER_FILTER_MENU_REGEX_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_HEADER_FILTER_MENU_REGEX_CHECKED_16X16 } },
	{ CACHEGRID_MENU_HEADER_FILTER_MENU_EMPTY,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HEADER_FILTER_MENU_EMPTY_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_HEADER_FILTER_MENU_EMPTY_CHECKED_16X16 } },
	{ CACHEGRID_MENU_HEADER_FILTER_VALUES,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HEADER_FILTER_VALUES_16X16, 0 } },
	{ CACHEGRID_MENU_PAUSE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_PAUSE, RPROD_BMP_RIBBON_CACHEGRID_MENU_PAUSE_CHECKED_16X16 } },
	{ CACHEGRID_MENU_HIDECOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDECOLUMN_16X16, 0 } },
	{ CACHEGRID_MENU_GROUPCOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GROUPCOLUMN, RPROD_BMP_RIBBON_CACHEGRID_MENU_GROUPCOLUMN_CHECKED_16X16 } },
	{ CACHEGRID_MENU_SUSPENDUPDATES,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SUSPENDUPDATES_16X16, 0 } },
	{ CACHEGRID_MENU_UNDOHIDECOLUMN,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_UNDOHIDECOLUMN_16X16, 0 } },
	{ CACHEGRID_MENU_VIEWHIDDENROWSROWS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_VIEWHIDDENROWSROWS_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_VIEWHIDDENROWSROWS_CHECKED_16X16 } },
	{ CACHEGRID_MENU_TOGGLESTATUSBAR,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLESTATUSBAR_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLESTATUSBAR_CHECKED_16X16 } },
	//{ CACHEGRID_MENU_STATUSBAR_TOTAL,{ 0, 0 } },
	//{ CACHEGRID_MENU_STATUSBAR_VISIBLE,{ 0, 0 } },
	//{ CACHEGRID_MENU_STATUSBAR_HIDDEN,{ 0, 0 } },
	//{ CACHEGRID_MENU_STATUSBAR_SELECTED,{ 0, 0 } },
	//{ CACHEGRID_MENU_STATUSBAR_ZEROS,{ 0, 0 } },
	//{ CACHEGRID_MENU_STATUSBAR_SETASDEFAULT,{ 0, 0 } },
	//{ CACHEGRID_MENU_CONVERTTODATE,{ 0, 0 } },
	//{ CACHEGRID_MENU_CONVERTTONUMBER,{ 0, 0 } },
	{ CACHEGRID_MENU_CHART,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CHART_16X16, 0 } },
	{ CACHEGRID_MENU_CELLFORMAT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CELLFORMAT_16X16, 0 } },
	{ CACHEGRID_MENU_GROUPFORMAT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_GROUPFORMAT_16X16, 0 } },
	{ CACHEGRID_MENU_RESIZEVISIBLE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESIZEVISIBLE_16X16, 0 } },
	{ CACHEGRID_MENU_RESIZEDATASET,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESIZEDATASET_16X16, 0 } },
	{ CACHEGRID_MENU_RESIZEHEADER,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESIZEHEADER_16X16, 0 } },
	{ CACHEGRID_MENU_RESIZEHEADDATA,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESIZEHEADDATA_16X16, 0 } },
	{ CACHEGRID_MENU_RESIZEDEFAULT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_RESIZEDEFAULT_16X16, 0 } },
	{ CACHEGRID_MENU_ASCSORTBYYSELECTEDCOL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTASCEND_16X16, 0 } },
	{ CACHEGRID_MENU_DESCSORTBYYSELECTEDCOL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTDESCEND_16X16, 0 } },
	{ CACHEGRID_MENU_REMOVESELECTEDCOLUMNSORTING,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVESORTING_16X16, 0 } },

	{ CACHEGRID_MENU_MULTIVALUE_EXPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPLODEMULTIVALUES_16X16, 0 } },
	{ CACHEGRID_MENU_MULTIVALUE_IMPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODEMULTIVALUES_16X16, 0 } },
	{ CACHEGRID_MENU_MULTIVALUE_IMPLODE_GRID,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODEALLMUTLIVALUES_16X16, 0 } },
	{ CACHEGRID_MENU_SORTMULTIVALUESBYCOUNT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SORTMULTIVALUES_16X16, 0 } },

	{ CACHEGRID_MENU_CONVERTTODATE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CONVERTTODATE_16X16, 0 } },
	{ CACHEGRID_MENU_CONVERTTONUMBER,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_CONVERTTONUM_16X16, 0 } },
	{ CACHEGRID_MENU_REMOVEDATECONVERSION,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVECONVERSIONDATE_16X16, 0 } },
	{ CACHEGRID_MENU_REMOVENUMBERCONVERSION,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVECONVERSIONNUMBER_16X16, 0 } },
	{ CACHEGRID_MENU_REMOVEALLCONVERSIONS,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_REMOVECONVERSIONALL_16X16, 0 } },

	{ CACHEGRID_MENU_DATES_EXPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPLODEDATES_16X16, 0 } },
	{ CACHEGRID_MENU_DATES_IMPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODEDATES_16X16, 0 } },
	{ CACHEGRID_MENU_TIMES_EXPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPLODETIMES_16X16, 0 } },
	{ CACHEGRID_MENU_TIMES_IMPLODE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODETIMES_16X16, 0 } },
	{ CACHEGRID_MENU_DATE_IMPLODE_GRID,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODEALLDATES_16X16, 0 } },
	{ CACHEGRID_MENU_TIME_IMPLODE_GRID,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_IMPLODEALLTIMES_16X16, 0 } },
	{ CACHEGRID_MENU_HIERARCHYTOGGLE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLEHIERARCHYVIEW_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_TOGGLEHIERARCHYVIEW_CHECKED_16X16 } },

	{ CACHEGRID_MENU_SETCOLUMNAUTORESIZE , { RPROD_BMP_RIBBON_CACHEGRID_MENU_AUTORESIZE_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_AUTORESIZE_CHECKED_16X16 } },
	{ CACHEGRID_MENU_SETCOLUMNTOTAL , { RPROD_BMP_RIBBON_CACHEGRID_MENU_SETCOLUMNTOTAL_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SETCOLUMNTOTAL_CHECKED_16X16 } },
	{ CACHEGRID_MENU_HIDESINGLEROWGROUPS ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDESINGLEROWGROUPS_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDESINGLEROWGROUPS_CHECKED_16X16 } },
	{ CACHEGRID_MENU_HIDEGROUPCOUNT ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEGROUPCOUNT_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_HIDEGROUPCOUNT_CHECKED_16X16 } },

	{ CACHEGRID_MENU_SHOWHIERARCHYPOSITIONCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYPOSITIONCOLUMN_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYPOSITIONCOLUMN_CHECKED_16X16 } },
	{ CACHEGRID_MENU_SHOWHIERARCHYLEVELCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYLEVELCOLUMN_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYLEVELCOLUMN_CHECKED_16X16 } },
	{ CACHEGRID_MENU_SHOWHIERARCHYNUMCHILDRENCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYNUMCHILDRENCOLUMN_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYNUMCHILDRENCOLUMN_CHECKED_16X16 } },
	{ CACHEGRID_MENU_SHOWHIERARCHYNUMDESCENDANTSCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYNUMDESCENDANTSCOLUMN_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYNUMDESCENDANTSCOLUMN_CHECKED_16X16 } },
	{ CACHEGRID_MENU_SHOWHIERARCHYKEYCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYKEYCOLUMN_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYKEYCOLUMN_CHECKED_16X16 } },
	{ CACHEGRID_MENU_SHOWHIERARCHYHIDDENDESCCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYHIDDENDESCCOLUMN_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYHIDDENDESCCOLUMN_CHECKED_16X16 } },
	{ CACHEGRID_MENU_SHOWHIERARCHYVISBLEDESCCOLUMN ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYVISIBLEDESCCOLUMN_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_SHOWHIERARCHYVISIBLEDESCCOLUMN_CHECKED_16X16 } },

	{ CACHEGRID_MENU_DISPLAYMULTIVALUENUMBER ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_DISPLAYMULTIVALUENUMBER_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_DISPLAYMULTIVALUENUMBER_CHECKED_16X16 } },

	{ CACHEGRID_MENU_FILTERDLG_TEXT ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_TEXT_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_TEXT_16X16_CHECKED } },
	{ CACHEGRID_MENU_FILTERDLG_NUMBER ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_NUMBER_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_NUMBER_16X16_CHECKED } },
	{ CACHEGRID_MENU_FILTERDLG_DATE ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_DATE_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_DATE_16X16_CHECKED } },
	{ CACHEGRID_MENU_FILTERDLG_TIME ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_TIME_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_TIME_16X16_CHECKED } },
	{ CACHEGRID_MENU_FILTERDLG_CUTOFF ,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_CUTOFF_16X16, RPROD_BMP_RIBBON_CACHEGRID_MENU_FILTERDLG_CUTOFF_16X16_CHECKED } },

	{ CACHEGRID_MENU_STATS01_THREESIGMA,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS01_THREESIGMA_16X16, 0 } },
	{ CACHEGRID_MENU_STATS02_CENTILES,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS02_CENTILES_16X16, 0 } },
	{ CACHEGRID_MENU_STATS03_TOPBOTTOM,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS03_TOPBOTTOM_16X16, 0 } },
	{ CACHEGRID_MENU_STATS04_DELTA20,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS04_DELTA20_16X16, 0 } },
	{ CACHEGRID_MENU_STATS05_DELTAVAL,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS05_DELTAVAL_16X16, 0 } },
	{ CACHEGRID_MENU_STATS06_THREESIGMA_COUNT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS06_THREESIGMA_COUNT_16X16, 0 } },
	{ CACHEGRID_MENU_STATS07_CENTILES_COUNT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS07_CENTILES_COUNT_16X16, 0 } },
	{ CACHEGRID_MENU_STATS08_TOPBOTTOM_COUNT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS08_TOPBOTTOM_COUNT_16X16, 0 } },
	{ CACHEGRID_MENU_STATS09_DELTA20_COUNT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS09_DELTA20_COUNT_16X16, 0 } },
	{ CACHEGRID_MENU_STATS10_DELTAVAL_COUNT,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATS10_DELTAVAL_COUNT_16X16, 0 } },
	{ CACHEGRID_MENU_STATSREMOVE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_STATSREMOVE_16X16, 0 } },

	{ CACHEGRID_MENU_DUPLICATESSHOW,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_DUPLICATES_SHOW_16X16, 0 } },
	{ CACHEGRID_MENU_DUPLICATERSEMOVE,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_DUPLICATES_REMOVE_16X16, 0 } },

	{ ID_GRIDFRAMEBASE_CACHEGRID_MENU_FILTERDLG_TEXT, { IDB_GRIDFRAMEBASE_CACHEGRID_MENU_FILTERDLG_TEXT_16X16, 0 } },

	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL1,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL1_16X16, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL2,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL2_16X16, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL3,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL3_16X16, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL4,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL4_16X16, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL5,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL5_16X16, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL6,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL6_16X16, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL7,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL7_16X16, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL8,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL8_16X16, 0 } },
	{ CACHEGRID_MENU_ALLROWS_EXPANDLEVEL9,{ RPROD_BMP_RIBBON_CACHEGRID_MENU_EXPANDALL9_16X16, 0 } },
};

const std::map<UINT, UINT> GridFrameBase::gridCommandsDuplicatesForRibbon
{
	{ ID_GRIDFRAMEBASE_CACHEGRID_MENU_EXPORT, CACHEGRID_MENU_EXPORT },
	{ ID_GRIDFRAMEBASE_CACHEGRID_MENU_COMPARATOR, CACHEGRID_MENU_COMPARATOR },
	{ ID_GRIDFRAMEBASE_CACHEGRID_MENU_FILTERDLG_TEXT, CACHEGRID_MENU_FILTERDLG_TEXT },
	{ ID_GRIDFRAMEBASE_CACHEGRID_MENU_DUPLICATESSHOW, CACHEGRID_MENU_DUPLICATESSHOW },
	{ ID_GRIDFRAMEBASE_CACHEGRID_MENU_ANNOTATION, CACHEGRID_MENU_ANNOTATION_EDIT },
};
