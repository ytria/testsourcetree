#pragma once

#include "BusinessDriveItem.h"

class BusinessDriveItemNotebook;
class BusinessDriveItemFolder : public BusinessDriveItem
{
public:
	BusinessDriveItemFolder();
	BusinessDriveItemFolder(const bool p_IsRoot);
	BusinessDriveItemFolder(const BusinessDriveItem& p_DriveItem);

	virtual void							SetDriveId(const PooledString& p_Val) override;
	void									AddChild(const BusinessDriveItem& p_Child);
	void									AddChildFolder(const BusinessDriveItem& p_Child);
	void									AddChildNotebook(const BusinessDriveItem& p_Child);
	int64_t									GetFolderSize() const;
	const vector<BusinessDriveItem>&		GetChildren() const;
	const vector<BusinessDriveItemFolder>&	GetChildrenFolder() const;
	const vector<BusinessDriveItemNotebook>& GetChildrenNotebook() const;
	size_t									GetChildrenCount() const;// first level count
	size_t									GetRecursiveCountFiles() const;
	size_t									GetRecursiveCountFolders() const;
	bool									Isinvalid() const;
	const boost::YOpt<SapioError>&			GetMainError() const;
	bool									IsRoot() const;
	const boost::YOpt<PooledString>&		GetDisplayName() const;
	void									SetDisplayName(const boost::YOpt<PooledString>& p_Val);

	virtual wstring							GetObjectName() const override;

protected:
	BusinessDriveItemFolder(const BusinessDriveItem& p_DriveItem, bool p_IsNotebook);

private:
	void updateFileAndFolderCounts(const BusinessDriveItemFolder& p_Child);
	void updateFileAndFolderCounts(const BusinessDriveItemNotebook& p_Child);
	void addToTotalSize(const BusinessDriveItem& p_Child);

	bool							m_IsRoot;
	vector<BusinessDriveItem>		m_Children;
	vector<BusinessDriveItemFolder> m_ChildrenFolder;
	vector<BusinessDriveItemNotebook> m_ChildrenNotebook;
	boost::YOpt<PooledString>		m_DisplayName;
	size_t							m_RecursiveCountFiles;
	size_t							m_RecursiveCountFolders;

	RTTR_ENABLE(BusinessDriveItem)
	RTTR_REGISTRATION_FRIEND
};

class BusinessDriveItemNotebook : public BusinessDriveItemFolder
{
public:
	BusinessDriveItemNotebook();
	BusinessDriveItemNotebook(const BusinessDriveItem& p_DriveItem);

	int64_t GetNotebookSize() const;

	RTTR_ENABLE(BusinessDriveItemFolder)
	RTTR_REGISTRATION_FRIEND
};