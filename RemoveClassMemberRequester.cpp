#include "RemoveClassMemberRequester.h"
#include "LoggerService.h"
#include "O365AdminUtil.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

RemoveClassMemberRequester::RemoveClassMemberRequester(const wstring& p_ClassId, const wstring& p_UserId)
	:m_ClassId(p_ClassId),
	m_UserId(p_UserId)
{
}

TaskWrapper<void> RemoveClassMemberRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("classes"));
	uri.append_path(m_ClassId);
	uri.append_path(_YTEXT("members"));
	uri.append_path(m_UserId);
	uri.append_path(_YTEXT("$ref"));

	LoggerService::User(_YFORMAT(L"Removing member with id %s from class id %s", m_UserId.c_str(), m_ClassId.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Delete(uri.to_uri(), httpLogger, p_TaskData).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResInfo) {
		*result = Util::GetResult(p_ResInfo);
	});
}

const HttpResultWithError& RemoveClassMemberRequester::GetResult() const
{
	return *m_Result;
}
