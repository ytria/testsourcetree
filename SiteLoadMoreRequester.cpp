#include "SiteLoadMoreRequester.h"

#include "BusinessSite.h"
#include "DriveRequester.h"
#include "O365AdminUtil.h"
#include "RESTUtils.h"
#include "RunOnScopeEnd.h"
#include "safeTaskCall.h"
#include "SiteRequester.h"

SiteLoadMoreRequester::SiteLoadMoreRequester(const wstring& p_SiteId, const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_Logger(p_Logger)
	, m_Site(std::make_shared<BusinessSite>())
{
	m_Site->SetID(p_SiteId);
}

TaskWrapper<void> SiteLoadMoreRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	return YSafeCreateTask([logger = m_Logger, site = m_Site, siteId = m_Site->GetID(), p_Session, p_TaskData]() {
		// Whatever happens, put back old message value in logger
		RunOnScopeEnd([logger, oldMessage = logger->GetLogMessage()]() {
			logger->SetLogMessage(oldMessage);
		});

		// Sync site
		auto syncRequester = std::make_shared<SiteRequester>(site->GetID(), logger);
		logger->SetLogMessage(_T("site"));

		*site = safeTaskCall(syncRequester->Send(p_Session, p_TaskData).Then([syncRequester]() {
			return syncRequester->GetData();
		}), p_Session->GetMSGraphSession(Sapio365Session::APP_SESSION), Util::ObjectErrorHandler<BusinessSite>, p_TaskData).get();

		// In case of error, a default object is returned. We have to set the id back.
		if (site->GetError() != boost::none)
			site->SetID(siteId);

		// Sync drive
		logger->SetLogMessage(_T("document library"));

		auto driveRequester = std::make_shared<DriveRequester>(DriveRequester::DriveSource::Site, siteId, logger);
		auto drive = safeTaskCall(driveRequester->Send(p_Session, p_TaskData).Then([driveRequester]() {
			return driveRequester->GetData();
		}), p_Session->GetMSGraphSession(Sapio365Session::APP_SESSION), Util::ObjectErrorHandler<BusinessDrive>, p_TaskData).get();

		// 404 (Not Found) just means there's no drive. Not a error.
		if (!drive.GetError() || drive.GetError()->GetStatusCode() != web::http::status_codes::NotFound)
			site->SetDrive(drive);
		site->SetFlags(BusinessObject::MORE_LOADED);
	});
}

const BusinessSite& SiteLoadMoreRequester::GetData() const
{
	return *m_Site;
}
