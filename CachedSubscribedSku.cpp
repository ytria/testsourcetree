#include "CachedSubscribedSku.h"

#include "BusinessSubscribedSku.h"

CachedSubscribedSku::CachedSubscribedSku(const BusinessSubscribedSku& p_SubscribedSku)
{
    if (p_SubscribedSku.GetSkuId() != boost::none)
        m_Id = *p_SubscribedSku.GetSkuId();
    if (p_SubscribedSku.GetSkuPartNumber() != boost::none)
        m_SkuPartNumber = *p_SubscribedSku.GetSkuPartNumber();
}
