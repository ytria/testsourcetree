#pragma once

#include "AnyItemAttachment.h"
#include "BusinessAttachment.h"
#include "ItemAttachment.h"
#include "OutlookItem.h"

class BusinessItemAttachment : public BusinessAttachment
{
public:
    BusinessItemAttachment();
    BusinessItemAttachment(const ItemAttachment& p_ItemAttachment);

    const boost::YOpt<AnyItemAttachment>& GetItem() const;
    void SetItem(const boost::YOpt<AnyItemAttachment>& p_Val);
private:
    boost::YOpt<AnyItemAttachment> m_Item;

    friend class ItemAttachmentDeserializer;
};

