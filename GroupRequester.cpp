#include "GroupRequester.h"

#include "BusinessOrgContact.h"
#include "BusinessUser.h"
#include "GroupDeserializer.h"
#include "IPropertySetBuilder.h"
#include "MSGraphCommonData.h"
#include "MSGraphUtil.h"
#include "MsGraphFieldNames.h"
#include "MSGraphSession.h"
#include "MultiObjectsPageRequestLogger.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "RbacRequiredPropertySet.h"
#include "MsGraphHttpRequestLogger.h"

GroupRequester::GroupRequester(const PooledString& p_GroupId, IPropertySetBuilder&& p_PropertySet, const std::shared_ptr<IRequestLogger>& p_Logger)
    : m_GroupId(p_GroupId)
	, m_Properties(std::move(p_PropertySet.GetPropertySet()))
	, m_UseBetaEndpoint(false)
	, m_Logger(p_Logger)
{
}

GroupRequester::GroupRequester(const PooledString& p_GroupId, const std::vector<rttr::property>& p_Properties, const std::shared_ptr<IRequestLogger>& p_Logger)
    : m_GroupId(p_GroupId)
	, m_Properties(p_Properties)
	, m_UseBetaEndpoint(false)
	, m_Logger(p_Logger)
{
}

void GroupRequester::SetUseBetaEndpoint(bool p_UseBetaEndpoint)
{
	m_UseBetaEndpoint = p_UseBetaEndpoint;
}

TaskWrapper<void> GroupRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<GroupDeserializer>(p_Session);
    m_Result = std::make_shared<SingleRequestResult>();

    // New modular cache, remove required props
	auto properties = RbacRequiredPropertySet<BusinessGroup>(m_Properties, *p_Session).GetPropertySet();

	// Cannot request id, the api will send invalid json..
	properties.erase(std::remove_if(properties.begin(), properties.end(), [](const auto& prop) { return prop.get_name() == "id"; }), properties.end());

    web::uri_builder uri;
    uri.append_path(Rest::GROUPS_PATH);
    uri.append_path(m_GroupId);
    uri.append_query(U("$select"), Rest::GetSelectQuery(properties));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
    return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, m_Result, uri.to_uri(), m_UseBetaEndpoint, m_Logger, httpLogger, p_TaskData);
}

BusinessGroup& GroupRequester::GetData()
{
    m_Deserializer->GetData().SetID(m_GroupId);
    return m_Deserializer->GetData();
}

const SingleRequestResult& GroupRequester::GetResult() const
{
    return *m_Result;
}
