#pragma once

#include "IRequester.h"

#include "CosmosDb.h"
#include "CosmosDbDeserializer.h"

class SingleRequestResult;

template <class T, class U>
class ValueListDeserializer;

namespace Azure
{
	class DbListRequester : public IRequester
	{
	public:
		DbListRequester(const wstring& p_SubscriptionId, const wstring& p_ResGroup, const wstring& p_DbAccountName);
		TaskWrapper<void> Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

		const vector<Azure::CosmosDb>& GetData() const;

	private:
		shared_ptr<ValueListDeserializer<Azure::CosmosDb, Azure::CosmosDbDeserializer>> m_Deserializer;
		shared_ptr<SingleRequestResult> m_Result;

		wstring m_SubscriptionId;
		wstring m_ResGroup;
		wstring m_DbAccountName;
	};
}

