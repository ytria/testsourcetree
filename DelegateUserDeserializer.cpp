#include "DelegateUserDeserializer.h"
#include "UserIdDeserializer.h"
#include "DelegatePermissionsDeserializer.h"

using namespace Ex;

void DelegateUserDeserializer::DeserializeObject(const XERCES_CPP_NAMESPACE::DOMElement& p_Element)
{
    m_DelegateUser.m_ReceiveCopiesOfMeetingMessages = DeserializeBool(p_Element, _YTEXT("ReceiveCopiesOfMeetingMessages"));
    m_DelegateUser.m_ViewPrivateItems = DeserializeBool(p_Element, _YTEXT("ViewPrivateItems"));

    auto userIdElement = GetElementByTagName(p_Element, _YTEXT("UserId"));
    ASSERT(nullptr != userIdElement);
    if (nullptr != userIdElement)
    {
        UserIdDeserializer userIdDeserializer;
        userIdDeserializer.DeserializeObject(*userIdElement);
        m_DelegateUser.m_UserId = userIdDeserializer.GetObject();
    }

    auto delegatePermissionsElement = GetElementByTagName(p_Element, _YTEXT("DelegatePermissions"));
    ASSERT(nullptr != delegatePermissionsElement);
    if (nullptr != delegatePermissionsElement)
    {
        DelegatePermissionsDeserializer delegatePermissionsDeserializer;
        delegatePermissionsDeserializer.DeserializeObject(*delegatePermissionsElement);
        m_DelegateUser.m_DelegatePermissions = delegatePermissionsDeserializer.GetObject();
    }
}

const Ex::DelegateUser& DelegateUserDeserializer::GetObject() const
{
    return m_DelegateUser;
}
