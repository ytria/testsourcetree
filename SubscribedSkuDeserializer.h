#pragma once

#include "BusinessSubscribedSku.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class SubscribedSkuDeserializer : public JsonObjectDeserializer
								, public Encapsulate<BusinessSubscribedSku>
{
public:
	SubscribedSkuDeserializer(std::shared_ptr<const Sapio365Session> p_Session);

protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;

private:
	std::shared_ptr<const Sapio365Session> m_Session;

private:
	friend class SubscribedSkuDeserializerFactory;
	SubscribedSkuDeserializer();
};

