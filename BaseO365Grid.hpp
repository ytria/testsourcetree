#pragma once

#include "BaseO365Grid.h"
#include "CommandInfo.h"
#include "CommandDispatcher.h"
#include "DlgContentViewer.h"
#include "GridFrameBase.h"
#include "GridUtil.h"
#include "MainFrame.h"
#include "MFCUtil.h"
#include "ModuleUtil.h"
#include "MsGraphFieldNames.h"
#include "Office365Admin.h"
#include "Product.h"
#include "RoleDelegationManager.h"
#include "YCodeJockMessageBox.h"

#include <boost/preprocessor/punctuation/comma.hpp>

BEGIN_TEMPLATE_MESSAGE_MAP_2(BaseO365Grid, CacheGridClass, BusinessObjectClass, BaseO365Grid<CacheGridClass BOOST_PP_COMMA() BusinessObjectClass>::BaseClass)
    ON_COMMAND(ID_MODULEGRID_EDIT,					OnEdit)
    ON_UPDATE_COMMAND_UI(ID_MODULEGRID_EDIT,		OnUpdateEdit)
    ON_COMMAND(ID_MODULEGRID_CREATE,				OnCreate)
	ON_UPDATE_COMMAND_UI(ID_MODULEGRID_CREATE,		OnUpdateCreate)
    ON_COMMAND(ID_MODULEGRID_DELETE,				OnDelete)
    ON_UPDATE_COMMAND_UI(ID_MODULEGRID_DELETE,		OnUpdateDelete)
	ON_COMMAND(ID_MODULEGRID_VIEW,					OnView)
	ON_UPDATE_COMMAND_UI(ID_MODULEGRID_VIEW,		OnUpdateBusinessType)
	ON_WM_CREATE()
END_MESSAGE_MAP()

template<class CacheGridClass, class BusinessObjectClass>
int BaseO365Grid<CacheGridClass, BusinessObjectClass>::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	SetAutomationRecordTarget(false);
	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_BASEO365GRID_ACCELERATOR));
	return IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::OnCreate(lpCreateStruct);
}

template<class CacheGridClass, class BusinessObjectClass>
BOOL BaseO365Grid<CacheGridClass, BusinessObjectClass>::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::PreTranslateMessage(pMsg);
}

template<class CacheGridClass, class BusinessObjectClass>
void BaseO365Grid<CacheGridClass, BusinessObjectClass>::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {
		const auto hasSel = GridUtil::IsSelectionAtLeastOneOfType<BusinessObjectClass>(*this);
		if (hasSel && CacheGridClass::HasView())
			pPopup->ItemInsert(ID_MODULEGRID_VIEW);

		if (hasSel && CacheGridClass::HasEdit())
			pPopup->ItemInsert(ID_MODULEGRID_EDIT);

		if (CacheGridClass::HasCreate())
			pPopup->ItemInsert(ID_MODULEGRID_CREATE);

		if (hasSel && CacheGridClass::HasDelete())
			pPopup->ItemInsert(ID_MODULEGRID_DELETE);

		if (CacheGridClass::HasCreate()
			|| hasSel && (CacheGridClass::HasEdit()
				|| CacheGridClass::HasView()
				|| CacheGridClass::HasDelete()))
			pPopup->ItemInsert(); // Sep
	
		CacheGridClass::OnCustomPopupMenu(pPopup, nStart);
	}
}

template<class CacheGridClass, class BusinessObjectClass>
void BaseO365Grid<CacheGridClass, BusinessObjectClass>::OnEdit()
{
	ASSERT(HasEdit());
	if (HasEdit())
	{
#ifdef BETA2017
		if (Office365AdminApp::ShowFeatureUnfinishedDialog(this))
#endif
		{
			ShowModificationDialog(DlgFormsHTML::Action::EDIT);
		}
	}
}

template<class CacheGridClass, class BusinessObjectClass>
void BaseO365Grid<CacheGridClass, BusinessObjectClass>::OnCreate()
{
	ASSERT(HasCreate());
	if (HasCreate())
	{
#ifdef BETA2017
		if (Office365AdminApp::ShowFeatureUnfinishedDialog(this))
#endif
		{
			ShowModificationDialog(DlgFormsHTML::Action::CREATE);
		}
	}
}

template<class CacheGridClass, class BusinessObjectClass>
void BaseO365Grid<CacheGridClass, BusinessObjectClass>::OnDelete()
{
	ASSERT(HasDelete());
	if (HasDelete())
	{
		TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool p_HasRowsToReExplode)
		{
			std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, GetPrivilegeDelete(), [this](GridBackendRow* p_Row)
			{
				return nullptr != p_Row && !p_Row->IsGroupRow() && isRegisteredType(p_Row) && 0 == (GridBackendUtil::ROWFLAGS::EDITDELETED & p_Row->GetFlags());
			});

			ASSERT(!selectedRows.empty());
			if (!selectedRows.empty())
			{
				bool update = false;
				bool promptedForEdit = false;
				bool deleteEdited = false;
				map<GridBackendRow*, row_pk_t> rowsToRevert;
				for (auto selectedRow : selectedRows)
				{
					ASSERT(nullptr != selectedRow);
					if (nullptr != selectedRow && !selectedRow->IsGroupRow())
					{
						row_pk_t rowPk;
						GetRowPK(selectedRow, rowPk);

						if (selectedRow->IsCreated())
						{
							rowsToRevert[selectedRow] = rowPk;
						}
						else if (!selectedRow->IsDeleted())
						{
							ASSERT(nullptr == GetModifications().GetRowModification<DeletedObjectModification>(rowPk));
							bool shouldDelete = true;
							if (selectedRow->IsModified())
							{
								if (!promptedForEdit)
								{
									YCodeJockMessageBox dlg(this,
										DlgMessageBox::eIcon_Question,
										YtriaTranslate::Do(DlgReplicationHardDeletion_s_STRING_BTN_OK_1, _YLOC("Delete")).c_str(),
										YtriaTranslate::Do(GridDriveItems_OnDeleteItem_2, _YLOC("The selected rows contain unsaved changes. Do you want to delete them instead?")).c_str(),
										YtriaTranslate::Do(GridDriveItems_OnDeleteItem_3, _YLOC("Pending changes will be lost.")).c_str(),
										{ { IDOK, YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str() } });

									deleteEdited = IDOK == dlg.DoModal();
									promptedForEdit = true;
								}

								if (deleteEdited)
								{
									GetModifications().Revert(rowPk, true);
									update = true;
								}
								else
								{
									shouldDelete = false;
								}
							}

							if (shouldDelete)
							{
								GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, rowPk));
								update = true;
							}
						}
					}
				}

				for (auto item : rowsToRevert)
				{
					if (GetModifications().Revert(item.second, false))
					{
						update = true;
					}
					else
					{
						ASSERT(false);
					}
				}

				if (update && !p_HasRowsToReExplode)
					UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
			}
		})();
	}
}

template<class CacheGridClass, class BusinessObjectClass>
void BaseO365Grid<CacheGridClass, BusinessObjectClass>::OnView()
{
	ASSERT(HasView());
	if (HasView())
	{
#ifdef BETA2017
		if (Office365AdminApp::ShowFeatureUnfinishedDialog(this))
#endif
		{
			ShowModificationDialog(DlgFormsHTML::Action::READ);
/*
			AutomationGreenlight();
*/
		}
	}
/*
	else
	{
// 		AutomationGreenlight( _YCOM("Invalid context"));
	}
*/
}

template<class CacheGridClass, class BusinessObjectClass>
void BaseO365Grid<CacheGridClass, BusinessObjectClass>::OnUpdateBusinessType(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(CacheGridClass::hasNoTaskRunning() && CacheGridClass::IsFrameConnected() && selectionHasRegisteredType(0));
	setTextFromProfUISCommand(*pCmdUI);
}

template<class CacheGridClass, class BusinessObjectClass>
void BaseO365Grid<CacheGridClass, BusinessObjectClass>::OnUpdateEdit(CCmdUI* pCmdUI)
{
	const auto condition = [this](GridBackendRow* p_Row)
	{
		auto editableRow = isRegisteredType(p_Row) ? p_Row : (GetEditParentWhenChildSelected() ? GridUtil::getParentOfBusinessType<BusinessObjectClass>(p_Row, true) : nullptr);
		return nullptr != editableRow && !editableRow->IsGroupRow() && isRegisteredType(editableRow) && canEdit(editableRow);
	};

	pCmdUI->Enable(HasEdit() && CacheGridClass::hasNoTaskRunning() && /*CacheGridClass::IsFrameConnected() && */GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, GetPrivilegeEdit(), condition, true));
	setTextFromProfUISCommand(*pCmdUI);
}

template<class CacheGridClass, class BusinessObjectClass>
void BaseO365Grid<CacheGridClass, BusinessObjectClass>::OnUpdateCreate(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(HasCreate() && CacheGridClass::hasNoTaskRunning() && /*CacheGridClass::IsFrameConnected() && */IsAuthorizedByRoleDelegation(GetPrivilegeCreate()));
	setTextFromProfUISCommand(*pCmdUI);
}

template<class CacheGridClass, class BusinessObjectClass>
void BaseO365Grid<CacheGridClass, BusinessObjectClass>::OnUpdateDelete(CCmdUI* pCmdUI)
{
	const auto condition = [this](GridBackendRow* p_Row)
	{
		return nullptr != p_Row
			&& !p_Row->IsGroupRow()
			&& isRegisteredType(p_Row)
			&& 0 == (GridBackendUtil::ROWFLAGS::EDITDELETED & p_Row->GetFlags())
			&& canDelete(p_Row);
	};

	pCmdUI->Enable(HasDelete() && CacheGridClass::hasNoTaskRunning() && /*CacheGridClass::IsFrameConnected() && */ GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, GetPrivilegeDelete(), condition, true));
	setTextFromProfUISCommand(*pCmdUI);
}

template<class CacheGridClass, class BusinessObjectClass>
void BaseO365Grid<CacheGridClass, BusinessObjectClass>::InitializeCommands()
{
	IGenericEditablePropertiesGrid<CacheGridClass, BusinessObjectClass>::InitializeCommands();

    m_RegisteredBusinessTypes.insert(rttr::type::get<BusinessObjectClass>().get_id());

	std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

	const auto profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MODULEGRID_VIEW;
		_cmd.m_sMenuText = YtriaTranslate::Do(GLOBAL_BOOST_PP_COMMA_10, _YLOC("View Selected")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GLOBAL_BOOST_PP_COMMA_10, _YLOC("View Selected")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_VIEW_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MODULEGRID_VIEW, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GLOBAL_BOOST_PP_COMMA_32, _YLOC("View Properties for Selected Entries")).c_str(),
			YtriaTranslate::Do(GLOBAL_BOOST_PP_COMMA_33, _YLOC("View (read-only) property information for the selected entries.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MODULEGRID_EDIT;
		_cmd.m_sMenuText = _T("Edit");
		_cmd.m_sToolbarText = _T("Edit");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_EDIT_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MODULEGRID_EDIT, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GLOBAL_BOOST_PP_COMMA_36, _YLOC("Edit Selected Entries")).c_str(),
			YtriaTranslate::Do(GLOBAL_BOOST_PP_COMMA_37, _YLOC("Edit the property information for all selected entries.\nMass-editing is possible by selecting multiple entries and modifying the desired property information. Changes will be applied to all selected entries.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MODULEGRID_CREATE;
		_cmd.m_sMenuText = YtriaTranslate::Do(DlgBusinessEditHTML_getDialogTitle_3, _YLOC("Create")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(PanelMulti_multi_7, _YLOC("Create")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CREATE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MODULEGRID_CREATE, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(DlgLogOptions_OnInitDialog_5, _YLOC("Create")).c_str(),
			YtriaTranslate::Do(GLOBAL_BOOST_PP_COMMA_17, _YLOC("Create new record.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MODULEGRID_DELETE;
		_cmd.m_sMenuText = YtriaTranslate::Do(ApplicationShortcuts_ApplicationShortcuts_166, _YLOC("Delete")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_OnDeleteItem_6, _YLOC("Delete")).c_str();
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DELETE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MODULEGRID_DELETE, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_27, _YLOC("Delete Entries")).c_str(),
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_28, _YLOC("Delete selected entries from the server.")).c_str(),
			_cmd.m_sAccelText);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////

template<class CacheGridClass, class BusinessObjectClass>
bool BaseO365Grid<CacheGridClass, BusinessObjectClass>::isDebugMode_hook() const
{
	// BaseO365Grid should probably merged with ModuleO365Grid. In this case this method should become useless.
	auto g = dynamic_cast<const O365Grid*>(this);
	ASSERT(nullptr != g);
	if (nullptr != g)
		return g->isDebugMode();
	return false;
}

// =================================================================================================


template<class BusinessObjectClass>
void ModuleO365Grid<BusinessObjectClass>::ShowItemAttachment(const BusinessItemAttachment& p_ItemAttachment)
{
    ASSERT(p_ItemAttachment.GetItem());
    if (p_ItemAttachment.GetItem())
    {
        AnyItemAttachment item = *p_ItemAttachment.GetItem();
        
		if (AnyItemAttachment::Kind::Contact == item.GetKind())
		{
			const auto& emailAddress = item.EmailAddresses.front();
			const wstring name = item.DisplayName.is_initialized() ? *item.DisplayName : (emailAddress.m_Name.is_initialized() ? *emailAddress.m_Name : _YTEXT(""));
			const wstring email = emailAddress.m_Address.is_initialized() ? *emailAddress.m_Address : _YTEXT("");
			const wstring jobTitle = item.JobTitle.is_initialized() ? *item.JobTitle : _YTEXT("");
			YCodeJockMessageBox msgBox(this,
				DlgMessageBox::eIcon::eIcon_Information,
				YtriaTranslate::Do(GLOBAL_BOOST_PP_COMMA_41, _YLOC("Contact Information")).c_str(),
				_YTEXTFORMAT(L"%s <%s>\n%s", name.c_str(), email.c_str(), jobTitle.c_str()),
				_YTEXT(""), // TODO add other information
				{ {IDOK, YtriaTranslate::Do(DlgGridCopySettings_OnInitDialog_12, _YLOC("OK")).c_str()} });
			msgBox.DoModal();
		}
		else
		{
			ASSERT(AnyItemAttachment::Kind::Event == item.GetKind() || AnyItemAttachment::Kind::Message == item.GetKind());

			PooledString bodyContent;
			PooledString bodyContentType;
			if (item.Body)
			{
				if (item.Body->Content)
					bodyContent = *item.Body->Content;
				if (item.Body->ContentType)
					bodyContentType = *item.Body->ContentType;
			}

			DlgContentViewer::HeaderConfig hc = ModuleUtil::GetDlgContentViewerHeaderConfig(item);
			DlgContentViewer viewer(this, nullptr, nullptr, YtriaTranslate::Do(GridEvents_InitializeCommands_12, _YLOC("Preview Item")).c_str(), this);
			viewer.SetContent(hc, bodyContent, bodyContentType == _YTEXT("html"), false);
			viewer.DoModal();
		}
    }
}

// =================================================================================================

template <class WizardO365Class>
void O365Grid::initWizard(const wstring& p_DebugFolderPath)
{
	if (isDebugMode())
		m_AutomationWizard = std::make_unique<AutomationWizardO365Folder>(p_DebugFolderPath);

	if (!m_AutomationWizard)
		m_AutomationWizard = std::make_unique<WizardO365Class>();
	ASSERT(IsCreateHTMLArea());
}
