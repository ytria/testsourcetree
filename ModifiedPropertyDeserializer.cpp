#include "ModifiedPropertyDeserializer.h"

void ModifiedPropertyDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("newValue"), m_Data.m_NewValue, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("oldValue"), m_Data.m_OldValue, p_Object);
}
