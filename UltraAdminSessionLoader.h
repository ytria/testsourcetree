#pragma once

#include "ISessionLoader.h"

class UltraAdminSessionLoader : public ISessionLoader
{
public:
	UltraAdminSessionLoader(const SessionIdentifier& p_Id);
	void SetIsFirstAttempt(bool p_IsFirstAttempt);

protected:
	TaskWrapper<std::shared_ptr<Sapio365Session>> Load(const std::shared_ptr<Sapio365Session>& p_SapioSession) override;

private:
	bool m_IsFirstAttempt;
};

