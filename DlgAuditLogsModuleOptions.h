#pragma once

#include "DlgModuleOptions.h"

class DlgAuditLogsModuleOptions : public DlgModuleOptions
{
public:
	using DlgModuleOptions::DlgModuleOptions;

protected:
	void generateJSONScriptData() override;
	wstring getDialogTitle() const override;
	std::map<wstring, DlgModuleOptions::FilterFieldProperties> getFilterFields() const override;
};

