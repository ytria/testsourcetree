#include "GenericModificationsApplyer.h"
#include "GridFrameBase.h"
#include "BaseO365Grid.h"

GenericModificationsApplyer::GenericModificationsApplyer(O365Grid& p_Grid)
	: m_Grid(p_Grid)
{
}

void GenericModificationsApplyer::ApplyAll()
{
	auto frame = m_Grid.GetParentGridFrameBase();
	const bool doApply = [=] {
		if (frame->m_DisplayApplyConfirm)
			return ModuleUtil::ConfirmApply(*frame);
		return true;
	}();

	if (doApply)
	{
		frame->GetGrid().StoreSelection();
		frame->SetSyncNow(frame->HasDeltaFeature());
		frame->ApplyAllSpecific();
		frame->GetGrid().RestoreSelection();
	}
}

void GenericModificationsApplyer::ApplySelected()
{
	auto frame = m_Grid.GetParentGridFrameBase();
	frame->SetSyncNow(frame->HasDeltaFeature());
	frame->ApplySelectedImpl(true);
}

bool GenericModificationsApplyer::Confirm()
{
	return true;
}
