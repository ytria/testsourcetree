#include "BusinessDriveItem.h"

#include "BusinessDriveItemFolder.h"
#include "DriveItemUpdateRequester.h"
#include "MSGraphCommonData.h"
#include "MSGraphSession.h"
#include "O365AdminUtil.h"
#include "SafeTaskCall.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "TimeUtil.h"
#include "UploadDriveFileRequester.h"
#include "MsGraphHttpRequestLogger.h"
#include "YSafeCreateTask.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessDriveItem>("Drive Item") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("File"))))
		.constructor()(policy::ctor::as_object)
        .property("name", &BusinessDriveItem::m_Name)
        .property("metaDriveId", &BusinessDriveItem::m_DriveId)
		;
}

BusinessDriveItem::BusinessDriveItem()
	: m_IsDeleted(false)
	, m_IsFolder(false)
	, m_TotalSize(0)
	, m_HierarchyLevel(0)
{
}

BusinessDriveItem::BusinessDriveItem(const DriveItem& p_DriveItem)
	: BusinessDriveItem()
{
	SetCreatedBy(p_DriveItem.CreatedBy);
    m_CreatedDateTime = p_DriveItem.CreatedDateTime;
	SetCTag(p_DriveItem.CTag);
    m_IsDeleted = boost::none != p_DriveItem.Deleted;
	SetETag(p_DriveItem.ETag);
	SetFile(p_DriveItem.File);
	SetFileSystemInfo(p_DriveItem.FileSystemInfo);

	m_Folder = p_DriveItem.Folder;
    m_IsFolder	= boost::none != p_DriveItem.Folder;

    m_Id = p_DriveItem.Id;
	//m_Image = p_DriveItem.Image;

	SetLastModifiedBy(p_DriveItem.LastModifiedBy);
    m_LastModifiedDateTime = p_DriveItem.LastModifiedDateTime;

	//m_Location = p_DriveItem.Location;

    m_Name		= p_DriveItem.Name		? *p_DriveItem.Name		: PooledString(_YTEXT(""));
	m_Package	= p_DriveItem.Package;
	m_ParentReference = p_DriveItem.ParentReference;
	//m_Photo = p_DriveItem.Photo;
	m_Publication = p_DriveItem.Publication;
	//m_RemoteItem = p_DriveItem.RemoteItem;
	//m_SearchResult = p_DriveItem.SearchResult;
    SetShared(p_DriveItem.Shared);
	m_SharepointIds = p_DriveItem.SharepointIds;

    m_TotalSize = p_DriveItem.Size		? *p_DriveItem.Size		: 0;
	//m_SpecialFolder = p_DriveItem.SpecialFolder;
	//m_Video = p_DriveItem.Video;

	m_WebDavUrl = p_DriveItem.WebDavUrl ? *p_DriveItem.WebDavUrl : PooledString(_YTEXT(""));
    m_WebUrl	= p_DriveItem.WebUrl	? *p_DriveItem.WebUrl	: PooledString(_YTEXT(""));

    if (m_DriveId.IsEmpty())
    {
        ASSERT(m_ParentReference != boost::none && m_ParentReference->DriveId != boost::none);
        if (m_ParentReference != boost::none && m_ParentReference->DriveId != boost::none)
        {
            m_DriveId = *m_ParentReference->DriveId;
        }
    }

    const wstring oneDrivePath = p_DriveItem.GetOneDrivePath();

    wstring root = _YTEXT("/root:");
    size_t rootIndex = oneDrivePath.find(root);
    if (rootIndex != wstring::npos)
        m_OneDrivePath = std::wstring(std::begin(oneDrivePath) + rootIndex + root.size(), std::end(oneDrivePath));
    else
        m_OneDrivePath = oneDrivePath;
}

pplx::task<RestResultInfo> BusinessDriveItem::Sync(const vector<rttr::property>& properties)
{
    return pplx::task<RestResultInfo>();
}

const boost::YOpt<YTimeDate>& BusinessDriveItem::GetCreatedDateTime() const
{
    return m_CreatedDateTime;
}

bool BusinessDriveItem::IsDeleted() const
{
    return m_IsDeleted;
}

const boost::YOpt<PooledString>& BusinessDriveItem::GetDescription() const
{
	return m_Description;
}

void BusinessDriveItem::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
	m_Description = p_Val;
}

bool BusinessDriveItem::IsFolder() const
{
    return m_IsFolder;
}

bool BusinessDriveItem::IsNotebook() const
{
	return m_Package && m_Package->Type && *m_Package->Type == _YTEXT("oneNote");
}

const boost::YOpt<YTimeDate>& BusinessDriveItem::GetLastModifiedDateTime() const
{
    return m_LastModifiedDateTime;
}

const boost::YOpt<PooledString>& BusinessDriveItem::GetName() const
{
    return m_Name;
}

int64_t BusinessDriveItem::GetTotalSize() const
{
    return m_TotalSize;
}

void BusinessDriveItem::SetTotalSize(const int64_t& p_Size)
{
	m_TotalSize = p_Size;
}

const PooledString& BusinessDriveItem::GetWebDavUrl() const
{
	return m_WebDavUrl;
}

const PooledString& BusinessDriveItem::GetWebUrl() const
{
    return m_WebUrl;
}

const boost::YOpt<CheckoutStatus>& BusinessDriveItem::GetCheckoutStatus() const
{
	return m_CheckoutStatus;
}

void BusinessDriveItem::SetCheckoutStatus(const boost::YOpt<CheckoutStatus>& p_Val)
{
	m_CheckoutStatus = p_Val;
}

const PooledString& BusinessDriveItem::GetOneDrivePath() const
{
    return m_OneDrivePath;
}

void BusinessDriveItem::SetOneDrivePath(const PooledString& p_Val)
{
	m_OneDrivePath = p_Val;
}

const PooledString& BusinessDriveItem::GetDriveId() const
{
    return m_DriveId;
}

void BusinessDriveItem::SetDriveId(const PooledString& p_Val)
{
    m_DriveId = p_Val;
}

const boost::YOpt<FileSystemInfo>& BusinessDriveItem::GetFileSystemInfo() const
{
    return m_FileSystemInfo;
}

void BusinessDriveItem::SetFileSystemInfo(const boost::YOpt<FileSystemInfo>& p_Val)
{
    m_FileSystemInfo = p_Val;
}

const boost::YOpt<GraphFolder>& BusinessDriveItem::GetFolder() const
{
    return m_Folder;
}

void BusinessDriveItem::SetFolder(const boost::YOpt<GraphFolder>& p_Val)
{
    m_Folder = p_Val;
}

const boost::YOpt<IdentitySet>& BusinessDriveItem::GetCreatedBy() const
{
    return m_CreatedBy;
}

void BusinessDriveItem::SetCreatedBy(const boost::YOpt<IdentitySet>& p_Val)
{
    m_CreatedBy = p_Val;
}

const boost::YOpt<IdentitySet>& BusinessDriveItem::GetLastModifiedBy() const
{
    return m_LastModifiedBy;
}

void BusinessDriveItem::SetLastModifiedBy(const boost::YOpt<IdentitySet>& p_Val)
{
    m_LastModifiedBy = p_Val;
}

const boost::YOpt<File>& BusinessDriveItem::GetFile() const
{
    return m_File;
}

void BusinessDriveItem::SetFile(const boost::YOpt<File>& p_Val)
{
    m_File = p_Val;
}

//const boost::YOpt<Image>& BusinessDriveItem::GetImage() const
//{
//    return m_Image;
//}
//
//void BusinessDriveItem::SetImage(const boost::YOpt<Image>& p_Val)
//{
//    m_Image = p_Val;
//}
//
//const boost::YOpt<GeoCoordinates>& BusinessDriveItem::GetLocation() const
//{
//    return m_Location;
//}
//
//void BusinessDriveItem::SetLocation(const boost::YOpt<GeoCoordinates>& p_Val)
//{
//    m_Location = p_Val;
//}

const boost::YOpt<Package>& BusinessDriveItem::GetPackage() const
{
    return m_Package;
}

void BusinessDriveItem::SetPackage(const boost::YOpt<Package>& p_Val)
{
    m_Package = p_Val;
}

//const boost::YOpt<Photo>& BusinessDriveItem::GetPhoto() const
//{
//    return m_Photo;
//}
//
//void BusinessDriveItem::SetPhoto(const boost::YOpt<Photo>& p_Val)
//{
//    m_Photo = p_Val;
//}
//
//const boost::YOpt<RemoteItem>& BusinessDriveItem::GetRemoteItem() const
//{
//    return m_RemoteItem;
//}
//
//void BusinessDriveItem::SetRemoteItem(const boost::YOpt<RemoteItem>& p_Val)
//{
//    m_RemoteItem = p_Val;
//}
//
//const boost::YOpt<SearchResult>& BusinessDriveItem::GetSearchResult() const
//{
//    return m_SearchResult;
//}
//
//void BusinessDriveItem::SetSearchResult(const boost::YOpt<SearchResult>& p_Val)
//{
//    m_SearchResult = p_Val;
//}
//
//const boost::YOpt<SpecialFolder>& BusinessDriveItem::GetSpecialFolder() const
//{
//    return m_SpecialFolder;
//}
//
//void BusinessDriveItem::SetSpecialFolder(const boost::YOpt<SpecialFolder>& p_Val)
//{
//    m_SpecialFolder = p_Val;
//}
//
//const boost::YOpt<Video>& BusinessDriveItem::GetVideo() const
//{
//    return m_Video;
//}
//
//void BusinessDriveItem::SetVideo(const boost::YOpt<Video>& p_Val)
//{
//    m_Video = p_Val;
//}

const boost::YOpt<Shared>& BusinessDriveItem::GetShared() const
{
    return m_Shared;
}

void BusinessDriveItem::SetShared(const boost::YOpt<Shared>& p_Val)
{
    m_Shared = p_Val;
}

const boost::YOpt<SharepointIds>& BusinessDriveItem::GetSharepointIds() const
{
	return m_SharepointIds;
}

void BusinessDriveItem::SetSharepointIds(const boost::YOpt<SharepointIds>& p_Val)
{
	m_SharepointIds = p_Val;
}

const boost::YOpt<PooledString>& BusinessDriveItem::GetCTag() const
{
    return m_CTag;
}

void BusinessDriveItem::SetCTag(const boost::YOpt<PooledString>& p_Val)
{
    m_CTag = p_Val;
}

const boost::YOpt<PooledString>& BusinessDriveItem::GetETag() const
{
    return m_ETag;
}

void BusinessDriveItem::SetETag(const boost::YOpt<PooledString>& p_Val)
{
    m_ETag = p_Val;
}

//const boost::YOpt<PooledString>& BusinessDriveItem::GetDownloadUrl() const
//{
//    return m_DownloadUrl;
//}
//
//void BusinessDriveItem::SetDownloadUrl(const boost::YOpt<PooledString>& p_Val)
//{
//    m_DownloadUrl = p_Val;
//}

const boost::YOpt<ItemReference>& BusinessDriveItem::GetParentReference() const
{
    return m_ParentReference;
}

void BusinessDriveItem::SetParentReference(const boost::YOpt<ItemReference>& p_Val)
{
    m_ParentReference = p_Val;
}

const boost::YOpt<PublicationFacet>& BusinessDriveItem::GetPublication() const
{
	return m_Publication;
}

void BusinessDriveItem::SetPublication(const boost::YOpt<PublicationFacet>& p_Val)
{
	m_Publication = p_Val;
}

//const boost::YOpt<Audio>& BusinessDriveItem::GetAudio() const
//{
//    return m_Audio;
//}
//
//void BusinessDriveItem::SetAudio(const boost::YOpt<Audio>& p_Val)
//{
//    m_Audio = p_Val;
//}
//
//const boost::YOpt<PooledString>& BusinessDriveItem::GetContent() const
//{
//    return m_Content;
//}
//
//void BusinessDriveItem::SetContent(const boost::YOpt<PooledString>& p_Val)
//{
//    m_Content = p_Val;
//}

DriveItem BusinessDriveItem::ToDriveItem(const BusinessDriveItem& p_BusinessDriveItem)
{
    DriveItem item;

    //item.Audio = p_BusinessDriveItem.GetAudio();
    //item.Content = p_BusinessDriveItem.GetContent();
    item.CreatedBy = p_BusinessDriveItem.GetCreatedBy();
    item.CreatedDateTime = p_BusinessDriveItem.GetCreatedDateTime();
    item.CTag = p_BusinessDriveItem.GetCTag();
	//item.Deleted = p_BusinessDriveItem.IsDeleted(); // ?
    item.ETag = p_BusinessDriveItem.GetETag();
    item.File = p_BusinessDriveItem.GetFile();
    item.FileSystemInfo = p_BusinessDriveItem.GetFileSystemInfo();
    item.Folder = p_BusinessDriveItem.GetFolder();
    item.Id = p_BusinessDriveItem.GetID();
    //item.Image = p_BusinessDriveItem.GetImage();
    item.LastModifiedBy = p_BusinessDriveItem.GetLastModifiedBy();
    item.LastModifiedDateTime = p_BusinessDriveItem.GetLastModifiedDateTime();
    //item.Location = p_BusinessDriveItem.GetLocation();
    item.Name = p_BusinessDriveItem.GetName();
    item.Package = p_BusinessDriveItem.GetPackage();
    item.ParentReference = p_BusinessDriveItem.GetParentReference();
    //item.Photo = p_BusinessDriveItem.GetPhoto();
	item.Publication = p_BusinessDriveItem.GetPublication();
    //item.RemoteItem = p_BusinessDriveItem.GetRemoteItem();
    //item.SearchResult = p_BusinessDriveItem.GetSearchResult();
    item.Shared = p_BusinessDriveItem.GetShared();
	item.SharepointIds = p_BusinessDriveItem.GetSharepointIds();
    item.Size = p_BusinessDriveItem.GetTotalSize();
    //item.SpecialFolder = p_BusinessDriveItem.GetSpecialFolder();
    //item.Video = p_BusinessDriveItem.GetVideo();
	item.WebDavUrl = p_BusinessDriveItem.GetWebDavUrl();
    item.WebUrl = p_BusinessDriveItem.GetWebUrl();
    item.DriveId = p_BusinessDriveItem.GetDriveId();

    return item;
}

const vector<BusinessDriveItem>& BusinessDriveItem::GetDriveItemChildren() const
{
    return m_DriveItemChildren;
}

void BusinessDriveItem::SetDriveItemChildren(const vector<BusinessDriveItem>& p_Val)
{
    m_DriveItemChildren = p_Val;
}

void BusinessDriveItem::SetDriveItemChildren(vector<BusinessDriveItem>&& p_Val)
{
    m_DriveItemChildren = std::move(p_Val);
}

const boost::YOpt<PooledString>& BusinessDriveItem::GetParentFolderId() const
{
	return m_ParentFolderId;
}

void BusinessDriveItem::SetParentFolderId(const boost::YOpt<PooledString>& p_Val)
{
	m_ParentFolderId = p_Val;
}

const boost::YOpt<PooledString>& BusinessDriveItem::GetLocalFilePath() const
{
	return m_LocalFilePath;
}

void BusinessDriveItem::SetLocalFilePath(const boost::YOpt<PooledString>& p_Val)
{
	m_LocalFilePath = p_Val;
}

const int BusinessDriveItem::GetHierarchyLevel() const
{
	return m_HierarchyLevel;
}

void BusinessDriveItem::SetHierarchyLevel(int p_Level)
{
	m_HierarchyLevel = p_Level;
}

wstring BusinessDriveItem::GetObjectName() const
{
	if (IsFolder())
		return BusinessDriveItemFolder().GetObjectName();
	if (IsNotebook())
		return BusinessDriveItemNotebook().GetObjectName();
	return BusinessObject::GetObjectName();
}

pplx::task<vector<HttpResultWithError>> BusinessDriveItem::SendEditRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	return YSafeCreateTaskMutable([that = *this, p_Sapio365Session, p_TaskData]() mutable
		{
			vector<HttpResultWithError> result;

			if (that.GetPublication())
			{
				// Check-in or check-out required
				if (that.GetPublication()->m_Level == _YTEXT("checkout") || that.GetPublication()->m_Level == _YTEXT("published"))
				{
					web::uri_builder urib;
					urib.append_path(Rest::DRIVES_PATH);
					urib.append_path(that.GetDriveId());
					urib.append_path(Rest::ITEMS_PATH);
					urib.append_path(that.GetID());
					urib.append_path(that.GetPublication()->m_Level == _YTEXT("checkout") ? _YTEXT("checkout") : _YTEXT("checkin"));

					auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Sapio365Session->GetIdentifier());
					result.push_back(safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(urib.to_uri(), httpLogger, p_TaskData).Then([](const RestResultInfo& p_Result)
						{
							return HttpResultWithError(p_Result, boost::none);
						}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData).get());
				}
				else
				{
					// Whaaaaat?
					ASSERT(false);
				}
			}

			bool didRename = false;
			if (that.GetName())
			{
				// Name update required.

				auto updater = std::make_shared<DriveItemUpdateRequester>(that.m_DriveId, that.m_Id);
				updater->SetName(*that.GetName());
				result.push_back(safeTaskCall(updater->Send(p_Sapio365Session, p_TaskData).Then([updater]()
					{
						const auto& result = updater->GetResult().GetResult();
						HttpResultWithError resultWithError;
						resultWithError.m_ResultInfo = result;
						if (result.m_Exception)
							resultWithError.m_Error = SapioError(*result.m_Exception);
						else if (result.m_RestException)
							resultWithError.m_Error = SapioError(result.m_RestException->WhatUnicode(), result.Response.status_code());
						return resultWithError;
					}), p_Sapio365Session->GetMSGraphSession(Sapio365Session::APP_SESSION), Util::WriteErrorHandler, p_TaskData).get());

				didRename = true;
			}

			if (that.GetLocalFilePath())
			{
				// Content upload required.

				if (didRename)
				{
					ASSERT(result.size() > 1);
					if (result.size() > 1 && !result.back().m_Error)
					{
						that.SetFlags((that.GetFlags() & ~RENAME_ON_CONFLICT) | REPLACE_ON_CONFLICT);
						result.push_back(that.SendCreateRequest(p_Sapio365Session, p_TaskData).GetTask().get());
					}
				}
				else
				{
					result.push_back(that.SendCreateRequest(p_Sapio365Session, p_TaskData).GetTask().get());
				}
			}

			return result;
		});
}

TaskWrapper<HttpResultWithError> BusinessDriveItem::SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
    DriveItem driveItem = BusinessDriveItem::ToDriveItem(*this);
    driveItem.DriveId = m_DriveId;
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Sapio365Session->GetIdentifier());
    return safeTaskCall(driveItem.Delete(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, p_TaskData).Then([](const RestResultInfo& p_Result) {
        return HttpResultWithError(p_Result, boost::none);
    }), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
}

TaskWrapper<HttpResultWithError> BusinessDriveItem::SendCreateRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const
{
	if (IsFolder())
	{
		json::value jsonData = json::value::object();
		ASSERT(GetName());
		if (GetName())
			jsonData.as_object()[_YTEXT("name")] = json::value(*GetName());
		jsonData.as_object()[_YTEXT("folder")] = json::value::object();
		if (HasFlag(RENAME_ON_CONFLICT))
			jsonData.as_object()[_YTEXT("@microsoft.graph.conflictBehavior")] = json::value(_YTEXT("rename"));
		else if (HasFlag(REPLACE_ON_CONFLICT))
			jsonData.as_object()[_YTEXT("@microsoft.graph.conflictBehavior")] = json::value(_YTEXT("replace"));
		else
			jsonData.as_object()[_YTEXT("@microsoft.graph.conflictBehavior")] = json::value(_YTEXT("fail"));

		WebPayloadJSON data(jsonData);

		web::uri_builder uri;
		uri.append_path(Rest::DRIVES_PATH);
		uri.append_path(GetDriveId());
		uri.append_path(Rest::ITEMS_PATH);
		if (GetParentFolderId())
		{
			uri.append_path(*GetParentFolderId());
			uri.append_path(Rest::CHILDREN_PATH);
		}

		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Sapio365Session->GetIdentifier());
		return safeTaskCall(p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_uri(), httpLogger, p_TaskData, data).Then([](const RestResultInfo& p_Result) {
			return HttpResultWithError(p_Result, boost::none);
		}), p_Sapio365Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData);
	}

	// New file to upload
	ASSERT(GetLocalFilePath());
	if (GetLocalFilePath())
	{
		std::shared_ptr<UploadDriveFileRequester> uploadReq = std::make_shared<UploadDriveFileRequester>(*GetLocalFilePath(), GetDriveId(), GetParentFolderId() ? *GetParentFolderId() : _YTEXT(""), GetName() ? *GetName() : _YTEXT(""));
		return safeTaskCall(YSafeCreateTask([uploadReq, p_Sapio365Session, p_TaskData]()
			{
				uploadReq->Send(p_Sapio365Session, p_TaskData).GetTask().wait();
				return uploadReq->GetResult();
			}), p_Sapio365Session->GetMSGraphSession(Sapio365Session::APP_SESSION), Util::WriteErrorHandler, p_TaskData);
	}

	return pplx::task_from_result(HttpResultWithError(boost::none, SapioError(_YERROR("Invalid file path."), 0)));
}

void BusinessDriveItem::SetIsFolder(const bool p_IsFolder)
{
	m_IsFolder = p_IsFolder;
}

void BusinessDriveItem::SetIsNotebook(const bool p_IsNotebook)
{
	// Shouldn't be used with false.
	ASSERT(p_IsNotebook);
	if (p_IsNotebook)
	{
		if (!m_Package)
			m_Package.emplace();
		m_Package->Type == PooledString(_YTEXT("oneNote"));
	}
}

void BusinessDriveItem::SetName(boost::YOpt<PooledString> p_Name)
{
    m_Name = p_Name;
}