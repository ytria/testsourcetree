#pragma once

#include "ISqlQuery.h"
#include "SessionIdentifier.h"

class QueryGetCredentialsToShare : public ISqlQuery
{
public:
	QueryGetCredentialsToShare(const SessionIdentifier& p_Id);
	void Run() override;
	int64_t GetCredentialsId() const;

private:
	SessionIdentifier m_Id;
	int64_t m_CredsId;
};

