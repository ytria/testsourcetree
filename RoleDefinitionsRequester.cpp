#include "RoleDefinitionsRequester.h"

#include "Sapio365Session.h"
#include "SharepointOnlineSession.h"
#include "SingleRequestResult.h"
#include "BasicHttpRequestLogger.h"

Sp::RoleDefinitionsRequester::RoleDefinitionsRequester(PooledString p_SiteUrl)
    : m_SiteUrl(p_SiteUrl)
{
}

TaskWrapper<void> Sp::RoleDefinitionsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<Sp::RoleDefinition, Sp::RoleDefinitionDeserializer>>();
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("sites"));
    uri.append_path(m_SiteUrl);
    uri.append_path(_YTEXT("_api"));
    uri.append_path(_YTEXT("web"));
    uri.append_path(_YTEXT("roleDefinitions"));

    LoggerService::User(YtriaTranslate::Do(Sp__RoleDefinitionsRequester_Send_1, _YLOC("Requesting role definitions for site \"%1\""), m_SiteUrl.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetSharepointSession()->Get(uri.to_uri(), httpLogger, m_Deserializer, p_TaskData);
}

const vector<Sp::RoleDefinition>& Sp::RoleDefinitionsRequester::GetData() const
{
    return m_Deserializer->GetData();
}
