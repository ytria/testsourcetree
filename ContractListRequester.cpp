#include "ContractListRequester.h"

#include "Contract.h"
#include "ContractDeserializer.h"
#include "MsGraphHttpRequestLogger.h"
#include "MsGraphPaginator.h"
#include "MSGraphSession.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "ValueListDeserializer.h"

ContractListRequester::ContractListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger)
    : m_Logger(p_Logger)
{
}

TaskWrapper<void> ContractListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<Contract, ContractDeserializer>>();

	web::uri_builder uri(_YTEXT("contracts"));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	auto paginatorReq = Util::CreateDefaultGraphPageRequester(m_Deserializer, httpLogger, p_TaskData.GetOriginator());

	return MsGraphPaginator(uri.to_uri(), paginatorReq, m_Logger).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

vector<Contract>& ContractListRequester::GetData()
{
    return m_Deserializer->GetData();
}
