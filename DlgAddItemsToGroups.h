#pragma once

#include "BusinessUser.h"
#include "BusinessGroup.h"
#include "BusinessOrgContact.h"
#include "CacheGrid.h"
#include "CachedUser.h"
#include "CachedGroup.h"
#include "CachedOrgContact.h"
#include "EducationClass.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "SelectedItem.h"
#include "YtriaTaskData.h"

class DlgAddItemsToGroups;
class GridAvailableUsers : public CacheGrid
{
public:
	using CacheGrid::CacheGrid;
	~GridAvailableUsers();

	void SetParentDlg(DlgAddItemsToGroups* p_Dlg);

	static const wstring g_AutomationName;
	int m_TeamIcon = NO_ICON;
	int m_EmailIcon = NO_ICON;

protected:
	virtual void customizeGrid() override;
	virtual void OnCustomDoubleClick() override;

private:
	DlgAddItemsToGroups* m_Dlg = nullptr;
};

class Commodore64CheckComboBox : public CExtCheckComboBox
{
public:
	virtual bool _OnCbLbPreWndProc(LRESULT & lResult, UINT message, WPARAM wParam, LPARAM lParam);
	void TriggerSelChange();
};

class GraphCache;
class DlgAddItemsToGroups	: public ResizableDialog
							, public GridSelectionObserver
{
public:
	enum LOADTYPE
	{
		SELECT_GROUPS_FOR_USERS,
		SELECT_GROUPS_FOR_MEMBERS, // For Copy To and Move To
		SELECT_USERSANDGROUPS_FOR_GROUPS, // ?
		SELECT_USERS_FOR_GROUPS,
		SELECT_USERS_FOR_ROLES,
        SELECT_USERS_FOR_SELECTION,
        SELECT_GROUPS_FOR_SELECTION,
		SELECT_USERSGROUPSANDORGCONTACTS_FOR_GROUPS, // For Members
		SELECT_USERSGROUPSANDORGCONTACTS_FOR_GROUPS_SHOWEMAIL, // Delivery Management, will show 'Is Email' column.
		SELECT_USERS_FOR_ROLEDELEGATION,
		SELECT_USER_FOR_MANAGER,
		SELECT_CLASSES
	};

	enum {IDD = IDD_DLG_ADDITEMSTOGROUPS};
	
	DlgAddItemsToGroups(
		std::shared_ptr<Sapio365Session>												p_Session,
		const std::map<PooledString, std::pair<PooledString, std::set<PooledString>>>&	p_SelectedParents, // <Id, <DisplayName, ChildrenIds>>
		const LOADTYPE																	p_LoadType,
		const vector<BusinessUser>&														p_InitUsers,
		const vector<BusinessGroup>&													p_InitGroups,
		const vector<BusinessOrgContact>&												p_InitOrgContacts,
		const wstring&																	p_Title,
		const wstring&																	p_Explanation,
		const RoleDelegationUtil::RBAC_Privilege										p_Privilege, // Used to filter out items excluded by RBAC
		CWnd*																			p_Parent);

	virtual void OnOK();
    virtual void OnCancel() override;

	using SelectedItems = vector<SelectedItem>;

	const SelectedItems& GetSelectedItems() const;

	static const wstring g_AutomationName;
	static const wstring g_AutomationNameSelectionGrid;

	virtual void SelectionChanged(const CacheGrid& grid) override;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialogSpecificResizable() override;
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

	afx_msg void OnListItemsLBNChange();
	afx_msg void OnClear();
	afx_msg void OnEditChange();
    afx_msg void OnRefresh();
	afx_msg void OnLoadAll();
	DECLARE_MESSAGE_MAP()

	void AddColDisplayName();

	virtual void setAutomationFieldMap() override;
	virtual void setAutomationRadioButtonGroups() override;
	virtual void setAutomationListChoicesGroups() override;

private:
	void initGrid();

    void SyncGrid(bool p_ForceSync);

    void initGridUpdate();
	void initTypeFilters();
	inline const LOADTYPE getLoadType() const;
	TaskWrapper<void> loadUsersToGrid(HWND p_Hwnd, YtriaTaskData p_TaskData, bool p_ForceSync);
	TaskWrapper<void> loadGroupsToGrid(HWND p_Hwnd, YtriaTaskData p_TaskData, bool p_ForceSync);
	TaskWrapper<void> loadOrgContactsToGrid(HWND p_Hwnd, YtriaTaskData p_TaskData, bool p_ForceSync);
	TaskWrapper<void> loadClassesToGrid(HWND p_Hwnd, YtriaTaskData p_TaskData, bool p_ForceSync);

	void loadUsersToGridSync(YtriaTaskData p_TaskData, bool p_ForceSync);
	void loadGroupsToGridSync(YtriaTaskData p_TaskData, bool p_ForceSync);
	void loadOrgContactsToGridSync(YtriaTaskData p_TaskData, bool p_ForceSync);
	void loadClassesToGridSync(YtriaTaskData p_TaskData, bool p_ForceSync);

	void addUsersTogrid(const vector<CachedUser>& p_Users);
	void addGroupsTogrid(const vector<CachedGroup>& p_Groups);
	void addOrgContactsTogrid(const vector<CachedOrgContact>& p_OrgContacts);
	void addClassesTogrid(const vector<EducationClass>& p_Classes);

	void finishGridUpdateSync();
	void shiftCtrlVertically(CWnd* p_Wnd, int offset);

	bool hasOnlyNonGuestUsers() const;
	bool hasOnlyUsers() const;
	bool hasOnlyGroups() const;
	bool hasUsersAndGroups() const;
	bool hasUsersGroupsAndOrgContacts() const;
	bool isShowEmail() const;
	bool isMultiSelection() const;

	void setRowColor(GridBackendRow* p_Row);

	void DisplayClass(const EducationClass& p_Class);

	CXTPButton	m_btnOK;
	CXTPButton	m_btnCancel;
	CXTPButton	m_BtnRefresh;
	CXTPButton	m_btnClear;
	CListBox	m_listItems;

	CXTPButton	m_BtnLoadAllLeft;
	CExtLabel	m_staticLoadAllLeft;

	CXTPButton	m_BtnLoadAllRight;
	CExtLabel	m_staticLoadAllRight;

	CExtLabel	m_staticGroupsOrUsers;
	CExtLabel	m_staticContains;
	CExtEdit	m_editContains;
	CExtLabel	m_staticExplanationText;
	
    CProgressCtrl				m_ProgressBar;
	Commodore64CheckComboBox	m_comboTypeFilters;
	GridAvailableUsers			m_Grid;

	SelectedItems		m_SelectedItems;
	GridBackendColumn*	m_pColID;
	GridBackendColumn*	m_pColPrincipalName;
	GridBackendColumn*	m_pColDisplayName;
	GridBackendColumn*	m_pColItemType;
	GridBackendColumn*	m_pColIsTeam;
	GridBackendColumn*	m_pColIsShowEmail;
	std::map<PooledString, std::pair<PooledString, std::set<PooledString>>>	m_SelectedParents;
	LOADTYPE			m_LoadType;

    YtriaTaskData	m_TaskData;
	GraphCache&		m_GraphCache;
	std::shared_ptr<Sapio365Session> m_Session;

	const vector<BusinessUser>&			m_InitUsers;
	const vector<BusinessGroup>&		m_InitGroups;
	const vector<BusinessOrgContact>&	m_InitOrgContacts;

	vector<wstring>	m_LastValueFilter;
	bool			m_FilterAutoTriggered;

	const wstring m_Title;
	const wstring m_Explanation;

	RoleDelegationUtil::RBAC_Privilege m_Privilege;

	std::set<PooledString> m_GrayedIds;
};