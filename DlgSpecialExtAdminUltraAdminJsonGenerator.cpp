#include "DlgSpecialExtAdminUltraAdminJsonGenerator.h"
#include "DlgSpecialExtAdminAccess.h"
#include "Office365Admin.h"
#include "MainFrame.h"
#include "JSONUtil.h"

DlgSpecialExtAdminUltraAdminJsonGenerator::DlgSpecialExtAdminUltraAdminJsonGenerator(bool p_EditOnlySecretKey)
    :m_EditOnlySecretKey(p_EditOnlySecretKey)
{
}

web::json::value DlgSpecialExtAdminUltraAdminJsonGenerator::Generate(const DlgSpecialExtAdminAccess& p_Dlg)
{
    Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
    MainFrame* mainFrame = nullptr;
    if (app)
        mainFrame = dynamic_cast<MainFrame*>(app->m_pMainWnd);

    const wstring tenantName = p_Dlg.GetTenant().empty() && nullptr != mainFrame ? mainFrame->GetSessionInfo().GetTenantName() : p_Dlg.GetTenant();
    const wstring displayName = p_Dlg.GetDisplayName().empty() && nullptr != mainFrame ? mainFrame->GetSessionInfo().GetTenantName() : p_Dlg.GetDisplayName();

    json::value root =
        JSON_BEGIN_OBJECT
            JSON_BEGIN_PAIR
                _YTEXT("main"),
                JSON_BEGIN_OBJECT
                JSON_PAIR_KEY(_YTEXT("id"))						JSON_PAIR_VALUE(JSON_STRING(_YTEXT("DlgSpecialExtAdminAccess"))),
                JSON_PAIR_KEY(_YTEXT("method"))					JSON_PAIR_VALUE(JSON_STRING(_YTEXT("POST"))),
                JSON_PAIR_KEY(_YTEXT("action"))					JSON_PAIR_VALUE(JSON_STRING(_YTEXT("#"))),
                JSON_PAIR_KEY(_YTEXT("automationSubmit"))		JSON_PAIR_VALUE(JSON_STRING(AutomatedApp::IsAutomationRunning() ? _YTEXT("true") : _YTEXT("")))
            JSON_END_OBJECT
            JSON_END_PAIR,
            JSON_BEGIN_PAIR
            _YTEXT("items"),
            JSON_BEGIN_ARRAY
            JSON_BEGIN_OBJECT
                JSON_PAIR_KEY(_YTEXT("hbs"))                    JSON_PAIR_VALUE(JSON_STRING(_YTEXT("fullAdminLogin"))),
                JSON_PAIR_KEY(_YTEXT("introTextOne"))           JSON_PAIR_VALUE(JSON_STRING(m_EditOnlySecretKey ? _YTEXT("") : MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_24, _YLOC("If you don't have an application in your Azure AD, clicking the 'Create New Application...' button will conveniently create one for you and then populate the fields below.\nWe strongly recommend that you not share your application ID or password with anyone.")).c_str()))),
                /*JSON_PAIR_KEY(_YTEXT("introTextTwo"))           JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesButKeepTags(_YCOM("\n[If you do not have a session open, you will be prompted to login to the relevant account.]")))),
                JSON_PAIR_KEY(_YTEXT("linkRegister"))           JSON_PAIR_VALUE(JSON_STRING(g_URL1)),
                JSON_PAIR_KEY(_YTEXT("linkRegisterText"))       JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesButKeepTags(_YCOM("Application Registration Portal")))),*/
                JSON_PAIR_KEY(_YTEXT("introTextThree"))         JSON_PAIR_VALUE(JSON_STRING(m_EditOnlySecretKey ? _YTEXT("") : MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_4, _YLOC("Need help? See: ")).c_str()))),
                JSON_PAIR_KEY(_YTEXT("linkThree"))              JSON_PAIR_VALUE(JSON_STRING(m_EditOnlySecretKey ? _YTEXT("") : DlgSpecialExtAdminAccess::g_URL2)),
                JSON_PAIR_KEY(_YTEXT("linkTextThree"))          JSON_PAIR_VALUE(JSON_STRING(m_EditOnlySecretKey ? _YTEXT("") : MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_5, _YLOC("How to Register Your Application")).c_str()))),
                //JSON_PAIR_KEY(_YTEXT("introTitle"))             JSON_PAIR_VALUE(JSON_STRING(MFCUtil::convertEscapeHTMLChar(_YCOM("Please complete the following form using the resulting registration information:")))),
                JSON_PAIR_KEY(_YTEXT("createNewAppButton"))     JSON_PAIR_VALUE(JSON_STRING(m_EditOnlySecretKey ? _YTEXT("") : MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_22, _YLOC("Create New Application...")).c_str()))),
                JSON_PAIR_KEY(_YTEXT("displayNameField"))       JSON_PAIR_VALUE(JSON_STRING(DlgSpecialExtAdminAccess::g_DisplayName)),
                JSON_PAIR_KEY(_YTEXT("displayNameLabel"))       JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(_T("Application Name:")))),
                JSON_PAIR_KEY(_YTEXT("displayNamePlaceholder")) JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(O365Session_GetDisplayedSessionType_3, _YLOC("Ultra Admin Session")).c_str()))),
                JSON_PAIR_KEY(_YTEXT("displayNameValue"))       JSON_PAIR_VALUE(JSON_STRING(displayName)),
                JSON_PAIR_KEY(_YTEXT("displayNamePopup"))       JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_26, _YLOC("Please provide a reference name for this application.")).c_str()))),
                //JSON_PAIR_KEY(_YTEXT("displayNameReadOnly"))  JSON_PAIR_VALUE(JSON_STRING(m_EditOnlySecretKey ? _YTEXT("X") : _YTEXT(""))),
                JSON_PAIR_KEY(_YTEXT("tenantField"))            JSON_PAIR_VALUE(JSON_STRING(DlgSpecialExtAdminAccess::g_TenantName)),
                JSON_PAIR_KEY(_YTEXT("tenantLabel"))            JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_7, _YLOC("Your Tenant Name:")).c_str()))),
                JSON_PAIR_KEY(_YTEXT("tenantPlaceholder"))      JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_8, _YLOC("mytenant.onmicrosoft.com")).c_str())),
                JSON_PAIR_KEY(_YTEXT("tenantValue"))            JSON_PAIR_VALUE(JSON_STRING(tenantName)),
                JSON_PAIR_KEY(_YTEXT("tenantPopup"))            JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_10, _YLOC("Please provide the full tenant name.")).c_str())),
                JSON_PAIR_KEY(_YTEXT("tenantReadOnly"))         JSON_PAIR_VALUE(JSON_STRING(m_EditOnlySecretKey ? _YTEXT("X") : _YTEXT(""))),
                JSON_PAIR_KEY(_YTEXT("appIDField"))             JSON_PAIR_VALUE(JSON_STRING(DlgSpecialExtAdminAccess::g_ApplicationId)),
                JSON_PAIR_KEY(_YTEXT("appIDLabel"))             JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_13, _YLOC("Your application ID:")).c_str()))),
                JSON_PAIR_KEY(_YTEXT("appIDPlaceholder"))       JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_9, _YLOC("xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")).c_str())),
                JSON_PAIR_KEY(_YTEXT("appIDValue"))             JSON_PAIR_VALUE(JSON_STRING(p_Dlg.GetAppId())),
                JSON_PAIR_KEY(_YTEXT("appIDPopup"))             JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_11, _YLOC("Please provide your application ID.")).c_str())),
                JSON_PAIR_KEY(_YTEXT("appIDReadOnly"))          JSON_PAIR_VALUE(JSON_STRING(m_EditOnlySecretKey ? _YTEXT("X") : _YTEXT(""))),
                JSON_PAIR_KEY(_YTEXT("secretKeyField"))         JSON_PAIR_VALUE(JSON_STRING(DlgSpecialExtAdminAccess::g_SecretKey)),
                JSON_PAIR_KEY(_YTEXT("secretKeyLabel"))         JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_12, _YLOC("Application Password:")).c_str()))),
                JSON_PAIR_KEY(_YTEXT("secretKeyPlaceholder"))   JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_14, _YLOC("Application password")).c_str())),
                JSON_PAIR_KEY(_YTEXT("secretKeyValue"))         JSON_PAIR_VALUE(JSON_STRING(p_Dlg.GetSaveSecretKey() ? DlgSpecialExtAdminAccess::g_KeyNotShown : _YTEXT(""))),
                JSON_PAIR_KEY(_YTEXT("secreKeyPopup"))          JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_15, _YLOC("Please provide the application password.")).c_str())),
                //JSON_PAIR_KEY(_YTEXT("secreKeyReadOnly"))     JSON_PAIR_VALUE(JSON_STRING(m_EditOnlySecretKey ? _YTEXT("X") : _YTEXT(""))),
                JSON_PAIR_KEY(_YTEXT("keepSecretKeyField"))     JSON_PAIR_VALUE(JSON_STRING(DlgSpecialExtAdminAccess::g_KeepSecretKey)),
                JSON_PAIR_KEY(_YTEXT("keepSecretKeyValue"))     JSON_PAIR_VALUE(JSON_STRING(_YTEXT("valueKeepSecretKey"))),
                JSON_PAIR_KEY(_YTEXT("keepSecretKeySelected"))  JSON_PAIR_VALUE(JSON_STRING(p_Dlg.GetSaveSecretKey() ? _YTEXT("X") : _YTEXT(""))),
                JSON_PAIR_KEY(_YTEXT("keepSecretKeyText"))      JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_16, _YLOC("Remember the password (encrypted)")).c_str()))),
                JSON_PAIR_KEY(_YTEXT("resetSecretKey"))			JSON_PAIR_VALUE(JSON_STRING(m_EditOnlySecretKey ? MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_25, _YLOC("Generate a new password")).c_str()) : _YTEXT(""))),
                JSON_PAIR_KEY(_YTEXT("adminConsentInfoOne"))    JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_17, _YLOC("To use this application, admin consent must be provided.")).c_str()))),
                JSON_PAIR_KEY(_YTEXT("adminConsentButton"))     JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_19, _YLOC("Provide Admin Consent")).c_str()))),
                JSON_PAIR_KEY(_YTEXT("adminConsentAction"))     JSON_PAIR_VALUE(JSON_STRING(DlgSpecialExtAdminAccess::g_URL3))
            JSON_END_OBJECT,
            JSON_BEGIN_OBJECT
                JSON_PAIR_KEY(_YTEXT("hbs"))                    JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Button"))),
                JSON_BEGIN_PAIR
                    _YTEXT("choices"),
                    JSON_BEGIN_ARRAY
		JSON_BEGIN_OBJECT
		JSON_PAIR_KEY(_YTEXT("type"))       JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
		JSON_PAIR_KEY(_YTEXT("label"))      JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_20, _YLOC("OK")).c_str()))),
		JSON_PAIR_KEY(_YTEXT("name"))       JSON_PAIR_VALUE(JSON_STRING(DlgSpecialExtAdminAccess::g_Ok)),
		JSON_PAIR_KEY(_YTEXT("attribute"))  JSON_PAIR_VALUE(JSON_STRING(_YTEXT("post"))),
		JSON_PAIR_KEY(_YTEXT("class"))      JSON_PAIR_VALUE(JSON_STRING(_YTEXT("is-primary"))),
		JSON_END_OBJECT,
		JSON_BEGIN_OBJECT
		JSON_PAIR_KEY(_YTEXT("type"))       JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
		JSON_PAIR_KEY(_YTEXT("label"))      JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_21, _YLOC("Cancel")).c_str()))),
		JSON_PAIR_KEY(_YTEXT("name"))       JSON_PAIR_VALUE(JSON_STRING(DlgSpecialExtAdminAccess::g_Cancel)),
		JSON_PAIR_KEY(_YTEXT("attribute"))  JSON_PAIR_VALUE(JSON_STRING(_YTEXT("cancel"))),
		JSON_END_OBJECT
                    JSON_END_ARRAY
                JSON_END_PAIR
            JSON_END_OBJECT
            JSON_END_ARRAY
            JSON_END_PAIR
        JSON_END_OBJECT;

    return root;
}
