#pragma once

#include "CommandInfo.h"
#include "ModuleBase.h"

class FrameMailboxPermissions;
struct RefreshSpecificData;

class ModuleMailboxPermissions : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	virtual void executeImpl(const Command& p_Command);
	void showMailboxPermissions(const Command& p_Command);
	void refresh(const Command& p_Command);
	void doRefresh(FrameMailboxPermissions* p_pFrame, const ModuleCriteria& p_ModuleCriteria, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool isUpdate, const RefreshSpecificData& p_RefreshData);
	void loadSnapshot(const Command& p_Command);
};

