#pragma once

// T must inherit from IFastDeserializer
template <typename T>
class KeyDeserializer : public JsonObjectDeserializer
{
public:
    KeyDeserializer(PooledString p_Key)
        :m_Key(p_Key)
    {}

    const T& GetInner() const;
    T& GetInner();

protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override
    {
        auto itt = p_Object.find(m_Key);
        ASSERT(itt != p_Object.end());
		if (itt != p_Object.end())
		{
			m_InnerDeserializer.SetDate(GetDate());
			m_InnerDeserializer.Deserialize(itt->second);
		}
    }

private:
    T m_InnerDeserializer;
    const PooledString m_Key;
};

template <typename T>
T& KeyDeserializer<T>::GetInner()
{
    return m_InnerDeserializer;
}

template <typename T>
const T& KeyDeserializer<T>::GetInner() const
{
    return m_InnerDeserializer;
}

