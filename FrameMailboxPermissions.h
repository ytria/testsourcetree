#pragma once

#include "GridFrameBase.h"
#include "MailboxPermissions.h"
#include "GridMailboxPermissions.h"

class FrameMailboxPermissions : public GridFrameBase
{
public:
	FrameMailboxPermissions(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

	DECLARE_DYNAMIC(FrameMailboxPermissions)
	virtual ~FrameMailboxPermissions() = default;

	void ShowMailboxPermissions(const O365DataMap<BusinessUser, vector<MailboxPermissions>> & p_MailboxPermissions, bool p_FullPurge);

	virtual void RefreshSpecific(AutomationAction * p_CurrentAction) override;
	virtual void RefreshIdsSpecific(const RefreshSpecificData & p_RefreshData, AutomationAction * p_CurrentAction) override;

	virtual O365Grid& GetGrid() override;
	GridMailboxPermissions& GetMailboxGrid();

protected:
	virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	GridMailboxPermissions m_MailboxPermissionsGrid;
};

