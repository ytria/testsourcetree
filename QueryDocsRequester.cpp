#include "QueryDocsRequester.h"
#include "CosmosDBSqlSession.h"
#include "DumpDeserializer.h"
#include "LoggerService.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "RESTUtils.h"
#include "AzureADCommonData.h"
#include "CosmosPaginator.h"
#include "BasicHttpRequestLogger.h"

Cosmos::QueryDocsRequester::QueryDocsRequester(	const wstring& p_DbName,
												const wstring& p_CollectionName,
												const wstring& p_Query,
												const vector<pair<wstring, wstring>>& p_QueryArgs) : 
	m_DbName(p_DbName),
    m_CollectionName(p_CollectionName),
    m_Query(p_Query),
    m_QueryArgs(p_QueryArgs)
{
}

TaskWrapper<void> Cosmos::QueryDocsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<Cosmos::Document, Cosmos::DocumentDeserializer>>(_YTEXT("Documents"));
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("dbs"));
    uri.append_path(m_DbName);
    uri.append_path(_YTEXT("colls"));
    uri.append_path(m_CollectionName);
    uri.append_path(_YTEXT("docs"));

    web::json::value body = web::json::value::object();
    body[_YTEXT("query")] = web::json::value::string(m_Query);
    if (!m_QueryArgs.empty())
    {
        body[_YTEXT("parameters")] = web::json::value::array();
        auto& paramsArray = body[_YTEXT("parameters")].as_array();
        for (size_t i = 0; i < m_QueryArgs.size(); ++i)
        {
            paramsArray[i] = json::value::object({
                { _YTEXT("name"), json::value::string(m_QueryArgs[i].first) },
                { _YTEXT("value"), json::value::string(m_QueryArgs[i].second) },
            });
        }
    }

	const WebPayloadJSON payload(body);
	const wstring resourceLink = wstring(_YTEXT("dbs/")) + m_DbName + wstring(_YTEXT("/colls/")) + m_CollectionName;
	const auto headers = vector<pair<wstring, wstring>>{
		{_YTEXT("x-ms-documentdb-isquery"), _YTEXT("true")},
		{ _YTEXT("Content-Type"), _YTEXT("application/query+json") },
		{ _YTEXT("x-ms-max-item-count"), _YTEXT("1000") }
	};

    LoggerService::User(YtriaTranslate::Do(Cosmos__QueryDocsRequester_Send_1, _YLOC("Querying database \"%1\" in CosmosDB"), m_DbName.c_str()), p_TaskData.GetOriginator());

	Cosmos::Paginator paginator(headers, [=](const Cosmos::Paginator::HeadersType& p_AdditionalHeaders) {
		    auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
			p_Session->GetCosmosDBSqlSession()->Post(uri.to_uri(), httpLogger, payload, _YTEXT("docs"), resourceLink, p_Session->GetCosmosDbMasterKey(), m_Result, m_Deserializer, p_TaskData, p_AdditionalHeaders).GetTask().wait();
			return m_Result;
	});

	return paginator.Paginate();
}

const vector<Cosmos::Document>& Cosmos::QueryDocsRequester::GetData() const
{
    return m_Deserializer->GetData();
}

const SingleRequestResult& Cosmos::QueryDocsRequester::GetResult() const
{
    return *m_Result;
}
