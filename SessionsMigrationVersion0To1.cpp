#include "SessionsMigrationVersion0To1.h"

#include "O365SQLiteEngine.h"
#include "OAuth2AuthenticatorBase.h"
#include "SessionIdentifier.h"
#include "SessionInfo_DEPRECATED.h"
#include "SessionsSqlEngine.h"
#include "SessionTypes.h"
#include "SqlQueryPreparedStatement.h"
#include "SQLSafeTransaction.h"
#include "TimeUtil.h"

void SessionsMigrationVersion0To1::Build()
{
	AddStep(std::bind(&SessionsMigrationVersion0To1::Exec, this));
}

VersionSourceTarget SessionsMigrationVersion0To1::GetVersionInfo() const
{
	return { 0, 1 };
}

bool SessionsMigrationVersion0To1::Exec()
{
	bool success = false;
	return SQLSafeTransaction(GetTargetSQLEngine(), [this, &success]()
		{
			success = CreateTables() && ImportData();
		}, true).Run() && success;
}

bool SessionsMigrationVersion0To1::CreateTables()
{
	static const auto queryCreateSessionsTable = _YTEXT(R"(
		CREATE TABLE "Sessions" (
			"Id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
			"EmailOrAppId"	TEXT NOT NULL,
			"SessionType"	TEXT NOT NULL,
			"DisplayName"	TEXT NOT NULL,
			"AccessCount"	INTEGER NOT NULL,
			"CreatedOn"	TEXT NOT NULL,
			"IsFavorite"	INTEGER NOT NULL,
			"UserFullName"	TEXT NOT NULL,
			"LastUsed"	TEXT NOT NULL,
			"ProfilePicture"	BLOB,
			"TenantName"	TEXT NOT NULL,
			"TenantDomain"	TEXT NOT NULL,
			"ShowOwnScopeOnly"	INTEGER NOT NULL,
			"RoleId"	INTEGER,
			"CredentialsId"	INTEGER,
			UNIQUE(EmailOrAppId, SessionType, RoleId),
			FOREIGN KEY("RoleId") REFERENCES "Roles"("Id"),
			FOREIGN KEY("CredentialsId") REFERENCES "Credentials"("Id"));
	)");
	
	static const auto queryCreateRolesTable = _YTEXT(R"(
			CREATE TABLE "Roles" (
				"Id"	INTEGER NOT NULL PRIMARY KEY UNIQUE,
				"Name"	TEXT NOT NULL
			);
	)");

	static const auto queryCreateCredentialsTable = _YTEXT(R"(
			CREATE TABLE "Credentials" (
				"Id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
				"AccessToken"	TEXT,
				"RefreshToken"	TEXT,
				"AppId"	TEXT,
				"AppTenant"	TEXT,
				"AppClientSecret"	TEXT
			);
	)");

	SqlQueryPreparedStatement queryCreateTableSessions(GetTargetSQLEngine(), queryCreateSessionsTable);
	SqlQueryPreparedStatement queryCreateTableRoles(GetTargetSQLEngine(), queryCreateRolesTable);
	SqlQueryPreparedStatement queryCreateTableCredentials(GetTargetSQLEngine(), queryCreateCredentialsTable);

	queryCreateTableSessions.Run();
	queryCreateTableRoles.Run();
	queryCreateTableCredentials.Run();

	static const auto strQueryCreateRole0 = _YTEXT(R"(INSERT INTO "Roles"(Id, Name) VALUES(0, ""))");
	SqlQueryPreparedStatement queryCreateRole0(GetTargetSQLEngine(), strQueryCreateRole0);
	queryCreateRole0.Run();

	return queryCreateTableSessions.IsSuccess() && queryCreateTableRoles.IsSuccess() && queryCreateTableCredentials.IsSuccess() && queryCreateRole0.IsSuccess();
}

bool SessionsMigrationVersion0To1::ImportData()
{
	auto sessions = SessionInfo_DEPRECATED::GetSessions(true);

	bool success = true;

	map<wstring, int64_t> sessionsCredentials; // Key: email, value: credentials id
	
	for (const auto& session : sessions)
	{
		wstring sessionType = session.m_SavedInfo.GetSessionType();
		int64_t credentialsId = 0;

		bool credentialsExists = false;
		auto existingCredentialsItt = sessionsCredentials.find(session.m_SavedInfo.GetTechnicalSessionName());
		if (existingCredentialsItt != sessionsCredentials.end() && (sessionType == Sapio365SessionSavedInfo_DEPRECATED::SessionTypes_DEPRECATED::g_StandardSession || sessionType == Sapio365SessionSavedInfo_DEPRECATED::SessionTypes_DEPRECATED::g_Role))
			credentialsExists = true;

		int accessCount = session.m_SavedInfo.GetAccessCount();
		wstring createdOn = TimeUtil::GetInstance().GetISO8601String(session.m_SavedInfo.GetCreatedOn());
		int isFavorite = session.m_SavedInfo.IsFavorite();
		wstring lastUsedOn = TimeUtil::GetInstance().GetISO8601String(session.m_SavedInfo.GetLastUsedOn());

		// Create role row
		bool showOwnScopeOnly = true;
		auto ittRole = session.m_SavedInfo.GetRoles().end();
		if (session.m_RoleId != 0)
		{
			sessionType = Sapio365SessionSavedInfo_DEPRECATED::SessionTypes_DEPRECATED::g_Role;
			ittRole = session.m_SavedInfo.GetRoles().find(session.m_RoleId);
			ASSERT(ittRole != session.m_SavedInfo.GetRoles().end());
			if (ittRole != session.m_SavedInfo.GetRoles().end())
			{
				accessCount = ittRole->m_AccessCount;
				createdOn = TimeUtil::GetInstance().GetISO8601String(ittRole->m_CreatedOn);
				isFavorite = ittRole->m_IsFavorite;
				lastUsedOn = TimeUtil::GetInstance().GetISO8601String(ittRole->m_LastUsedOn);
				showOwnScopeOnly = ittRole->m_ShowOwnScopeOnly;

				static const auto& strQueryRole = _YTEXT(R"(INSERT INTO "Roles"(Id, Name) VALUES(?, ?) ON CONFLICT(Id) DO UPDATE SET Name=excluded.Name)");
				SqlQueryPreparedStatement queryInsertRole(GetTargetSQLEngine(), strQueryRole);
				queryInsertRole.BindInt64(1, session.m_RoleId);
				queryInsertRole.BindString(2, session.GetRoleName());
				queryInsertRole.Run();

				if (!queryInsertRole.IsSuccess())
					success = false;
			}
		}

		// Create credential row
		RestCredentials_DEPRECATED creds;
		creds.SetName(session.m_SavedInfo.GetTechnicalSessionName());
		bool hasRegKey = session.m_SavedInfo.GetCredentialsManager().Load(creds);
		ASSERT(hasRegKey);
		if (hasRegKey)
		{
			auto accessTokenKeyItt = creds.GetValues().find(OAuth2AuthenticatorBase::ACCESS_TOKEN_SESSION_KEY);
			auto refreshTokenKeyItt = creds.GetValues().find(OAuth2AuthenticatorBase::REFRESH_TOKEN_SESSION_KEY);
			auto appIdItt = creds.GetValues().find(_YTEXT("app_id"));
			auto appTenantItt = creds.GetValues().find(_YTEXT("app_tenant"));
			auto appClientSecretItt = creds.GetValues().find(_YTEXT("app_client_secret"));

			if (accessTokenKeyItt != creds.GetValues().end() && !credentialsExists)
			{
				static const auto& strQueryCred = _YTEXT(R"(INSERT INTO "Credentials"(AccessToken, RefreshToken, AppId, AppTenant, AppClientSecret) VALUES(?, ?, ?, ?, ?))");
				SqlQueryPreparedStatement queryInsertCred(GetTargetSQLEngine(), strQueryCred);
				queryInsertCred.BindString(1, accessTokenKeyItt != creds.GetValues().end() ? accessTokenKeyItt->second : _YTEXT(""));
				queryInsertCred.BindString(2, refreshTokenKeyItt != creds.GetValues().end() ? refreshTokenKeyItt->second : _YTEXT(""));
				queryInsertCred.BindString(3, appIdItt != creds.GetValues().end() ? appIdItt->second : _YTEXT(""));
				queryInsertCred.BindString(4, appTenantItt != creds.GetValues().end() ? appTenantItt->second : _YTEXT(""));
				queryInsertCred.BindString(5, appClientSecretItt != creds.GetValues().end() ? appClientSecretItt->second : _YTEXT(""));
				queryInsertCred.Run();

				if (queryInsertCred.IsSuccess())
				{
					credentialsId = sqlite3_last_insert_rowid(GetTargetSQLEngine().GetDbHandle());
					if (sessionType == Sapio365SessionSavedInfo_DEPRECATED::SessionTypes_DEPRECATED::g_StandardSession || sessionType == Sapio365SessionSavedInfo_DEPRECATED::SessionTypes_DEPRECATED::g_Role)
						sessionsCredentials[session.m_SavedInfo.GetTechnicalSessionName()] = credentialsId;
				}
				else
					success = false;
			}
		}

		// Create session row
		static const auto& strQuerySession = _YTEXT(R"(INSERT INTO "Sessions"(EmailOrAppId, SessionType, DisplayName, AccessCount, CreatedOn, IsFavorite, UserFullName, LastUsed, ProfilePicture, TenantName, TenantDomain, ShowOwnScopeOnly, RoleId, CredentialsId) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?))");
		SqlQueryPreparedStatement queryInsertSession(GetTargetSQLEngine(), strQuerySession);

		wstring emailOrAppId = session.m_SavedInfo.GetSessionType() == Sapio365SessionSavedInfo_DEPRECATED::SessionTypes_DEPRECATED::g_UltraAdmin ? session.m_SavedInfo.GetTechnicalSessionName() : session.m_SavedInfo.GetSessionName();
		queryInsertSession.BindString(1, emailOrAppId);
		queryInsertSession.BindString(2, sessionType);
		queryInsertSession.BindString(3, session.m_SavedInfo.GetSessionName()); // Display Name
		queryInsertSession.BindInt(4, accessCount);
		queryInsertSession.BindString(5, createdOn);
		queryInsertSession.BindInt(6, isFavorite);
		queryInsertSession.BindString(7, session.m_SavedInfo.GetFullname());
		queryInsertSession.BindString(8, lastUsedOn);
		queryInsertSession.BindBlob(9, session.m_SavedInfo.GetProfilePhoto().data(), static_cast<int>(session.m_SavedInfo.GetProfilePhoto().size()));
		queryInsertSession.BindString(10, session.m_SavedInfo.GetTenantDisplayName());
		queryInsertSession.BindString(11, session.m_SavedInfo.GetTenantName());
		queryInsertSession.BindInt(12, showOwnScopeOnly);
		queryInsertSession.BindInt64(13, session.m_RoleId);

		if (credentialsExists)
		{
			queryInsertSession.BindInt64(14, existingCredentialsItt->second);
		}
		else
		{
			if (credentialsId != 0)
				queryInsertSession.BindInt64(14, credentialsId);
			else
				queryInsertSession.BindNull(14); // Session is logged-out
		}
		queryInsertSession.Run();

		if (!queryInsertSession.IsSuccess())
			success = false;
	}

	return true;
}

