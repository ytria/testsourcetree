#pragma once

#include "LicenseUtil.h"

class VendorLicense;
class O365LicenseValidator : public ILicenseValidator
{
public:
	O365LicenseValidator();
	virtual bool IsLicenseNone(const LicenseContext& p_Context, const VendorLicense& p_License) override;
	virtual bool IsLicenseReadOnly(const LicenseContext& p_Context, const VendorLicense& p_License) override;
	virtual const wstring& GetBuyLicenseURL() const override;

	void SetLoadingUserCount(const wstring& p_TN);
	void GetMaxCountForFreeAndTokenRates(const bool p_GoOnline);
	void SetTenantUserCount(const wstring& p_TN, const uint32_t p_TUC);// set actual user count in tenant
	bool IsTenantMaxUserCountSet(const VendorLicense& p_License) const;// is the license configured with tenant name(s) & count(s)

	virtual void SetAuthorizedCountCallback(std::function<bool (const bool, const uint32_t,  uint32_t&, uint32_t&, uint32_t&)> p_ACCallback) override;
	
	virtual uint32_t GetTokenAmountToCheckout(const LicenseContext& p_LC) const override;
	bool FirstCheckout(const LicenseContext& p_Context);

	bool IsAuthorizedForFree(const LicenseContext& p_LC) const override;

	uint32_t GetUserCountForFreeAccess() const;

	boost::YOpt<bool> PromptUserForLicenseUpgrade(CWnd* p_ParentWindow, LicenseManager& p_LicenseManager) override;

	bool IsLicenseCollaboration(const VendorLicense& p_License) const; // Cosmos
	bool IsLicenseCreateReadOnly(const VendorLicense& p_License) const;
	bool IsLicenseCreateRestorePoint(const VendorLicense& p_License) const;
	bool IsLicenseLoadRestorePoint(const VendorLicense& p_License) const;
	bool IsLicenseJobEditor(const VendorLicense& p_License) const;
	bool IsLicenseRBAC(const VendorLicense& p_License) const;
	bool IsLicenseOnPremise(const VendorLicense& p_License) const;
	bool IsLicenseAJL(const VendorLicense& p_License) const; // AJL
	bool CanLicenseAJLremoveJobs(const VendorLicense& p_License) const; // AJL
	bool IsLicensePro(const VendorLicense& p_License) const;
	
	bool		IsJobAllowedByLicense(const VendorLicense& p_License, const wstring& p_JobID) const;
	uint32_t	GetAJLCapacity(const VendorLicense& p_License) const;

private:
	__forceinline bool isAuthorizedUserCount() const;

	map<wstring, uint32_t> m_TenantUserCounts;
	std::function<bool(const bool, uint32_t, uint32_t&, uint32_t&, uint32_t&)> m_AuthorizedCountCallback;// returns user count that does not require a license
	bool m_AuthorizedUserCount;// without a full license
	uint32_t m_UserCountForFreeAccess;
	uint32_t m_TokenRateMain;
	uint32_t m_TokenRateSub;
	const uint32_t m_TokenRateFirstCheckout;
};