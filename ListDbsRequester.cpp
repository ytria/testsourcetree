#include "ListDbsRequester.h"
#include "CosmosDBSqlSession.h"
#include "DumpDeserializer.h"
#include "LoggerService.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "BasicHttpRequestLogger.h"

TaskWrapper<void> Cosmos::ListDbsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<Cosmos::Database, Cosmos::DatabaseDeserializer>>(_YTEXT("Databases"));
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("dbs"));

    LoggerService::User(YtriaTranslate::Do(Cosmos__ListDbsRequester_Send_1, _YLOC("Requesting databases list in CosmosDB")).c_str(), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetCosmosDBSqlSession()->Get(	uri.to_uri(),
													httpLogger,
													_YTEXT("dbs"),
													_YTEXT(""),
													p_Session->GetCosmosDbMasterKey(),
													m_Result,
													m_Deserializer,
													p_TaskData);
}

const vector<Cosmos::Database>& Cosmos::ListDbsRequester::GetData() const
{
    return m_Deserializer->GetData();
}

const SingleRequestResult& Cosmos::ListDbsRequester::GetResult() const
{
    return *m_Result;
}
