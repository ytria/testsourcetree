#include "EducationSchoolDeleteRequester.h"
#include "O365AdminUtil.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

EducationSchoolDeleteRequester::EducationSchoolDeleteRequester(const wstring& p_SchoolId)
	:m_SchoolId(p_SchoolId)
{
}

TaskWrapper<void> EducationSchoolDeleteRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("schools"));
	uri.append_path(m_SchoolId);

	LoggerService::User(_YFORMAT(L"Deleting school with id %s", m_SchoolId.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Delete(uri.to_uri(), httpLogger, p_TaskData).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResInfo) {
		*result = Util::GetResult(p_ResInfo);
	});
}

const HttpResultWithError& EducationSchoolDeleteRequester::GetResult() const
{
	return *m_Result;
}
