#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "SignIn.h"

class SignInDeserializer : public JsonObjectDeserializer , public Encapsulate<SignIn>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

