#pragma once

#include "IRequester.h"

#include "BusinessDrive.h"
#include "IRequestLogger.h"

class DriveDeserializer;

class DriveRequester : public IRequester
{
public:
	enum class DriveSource { Drive, User, Group, Site };

	DriveRequester(DriveSource p_Source, PooledString p_Id, const std::shared_ptr<IRequestLogger>& p_Logger, const wstring& p_SelectProperties = _YTEXT(""));
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const BusinessDrive& GetData() const;

private:
	std::shared_ptr<DriveDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;

	DriveSource m_Source;
	PooledString m_Id;
	const wstring m_SelectProperties;
};

