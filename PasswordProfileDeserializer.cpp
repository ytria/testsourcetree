#include "PasswordProfileDeserializer.h"

#include "JsonSerializeUtil.h"

void PasswordProfileDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("password"), m_Data.Password, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("forceChangePasswordNextSignIn"), m_Data.ForceChangePasswordNextSignIn, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("forceChangePasswordNextSignInWithMfa"), m_Data.ForceChangePasswordNextSignInWithMfa, p_Object);
}
