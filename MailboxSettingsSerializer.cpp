#include "MailboxSettingsSerializer.h"

#include "AutomaticRepliesSettingSerializer.h"
#include "JsonSerializeUtil.h"
#include "LocaleInfoSerializer.h"
#include "NullableSerializer.h"
#include "WorkingHoursSerializer.h"

MailboxSettingsSerializer::MailboxSettingsSerializer(const MailboxSettings& p_MailboxSettings)
    : m_MailboxSettings(p_MailboxSettings)
{
}

void MailboxSettingsSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("archiveFolder"), m_MailboxSettings.ArchiveFolder, obj);
    JsonSerializeUtil::SerializeAny(_YTEXT("automaticRepliesSetting"), NullableSerializer<AutomaticRepliesSetting, AutomaticRepliesSettingSerializer>(m_MailboxSettings.AutomaticRepliesSetting), obj);
	JsonSerializeUtil::SerializeString(_YTEXT("dateFormat"), m_MailboxSettings.DateFormat, obj);
	JsonSerializeUtil::SerializeString(_YTEXT("delegateMeetingMessageDeliveryOptions"), m_MailboxSettings.DelegateMeetingMessageDeliveryOptions, obj);
    JsonSerializeUtil::SerializeAny(_YTEXT("language"), NullableSerializer<LocaleInfo, LocaleInfoSerializer>(m_MailboxSettings.Language), obj);
	JsonSerializeUtil::SerializeString(_YTEXT("timeFormat"), m_MailboxSettings.TimeFormat, obj);
	JsonSerializeUtil::SerializeString(_YTEXT("timeZone"), m_MailboxSettings.TimeZone, obj);
	JsonSerializeUtil::SerializeAny(_YTEXT("workingHours"), NullableSerializer<WorkingHours, WorkingHoursSerializer>(m_MailboxSettings.WorkingHours), obj);

}
