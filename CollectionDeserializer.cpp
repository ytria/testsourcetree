#include "CollectionDeserializer.h"

#include "BaseObjDeserializer.h"
#include "IndexingPolicyDeserializer.h"
#include "JsonSerializeUtil.h"
#include "PartitionKeyDeserializer.h"

void Cosmos::CollectionDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    BaseObjDeserializer::Deserialize(p_Object, GetData());

    JsonSerializeUtil::DeserializeString(_YTEXT("_doc"), m_Data._doc, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("_sprocs"), m_Data._doc, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("_triggers"), m_Data._doc, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("_udfs"), m_Data._doc, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("_conflicts"), m_Data._doc, p_Object);

    {
        Cosmos::IndexingPolicyDeserializer deserializer;
		JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("indexingPolicy"), p_Object);
        m_Data.IndexingPolicy = std::move(deserializer.GetData());
    }

    {
        Cosmos::PartitionKeyDeserializer deserializer;
		JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("partitionKey"), p_Object);
        m_Data.PartitionKey = std::move(deserializer.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("indexingPolicy"), m_Data._doc, p_Object);
}
