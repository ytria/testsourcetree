#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerJobScheduleEdit : public IBrowserLinkHandler
{
public:
	virtual void Handle(YBrowserLink& p_Link) const override;

	virtual bool IsAllowedWithAJLLicense() const override
	{
		return true;
	}
};