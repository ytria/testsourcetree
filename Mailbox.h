#pragma once

#include "BusinessObject.h"

class Mailbox : public BusinessObject
{
public:
	std::wstring m_RecipientTypeDetails;
	std::wstring m_ForwardingAddress;
	std::wstring m_ForwardingSmtpAddress;
	bool m_DeliverToMailboxAndForward;
	std::vector<wstring> m_GrantSendOnBehalfTo;
};

