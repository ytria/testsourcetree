#include "DriveItemUpdateRequester.h"
#include "LoggerService.h"
#include "MSGraphCommonData.h"
#include "MSGraphSession.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "Str.h"
#include "MsGraphHttpRequestLogger.h"

DriveItemUpdateRequester::DriveItemUpdateRequester(PooledString p_DriveId, PooledString p_DriveItemId)
    : m_DriveId(p_DriveId), m_DriveItemId(p_DriveItemId)
{
}

TaskWrapper<void> DriveItemUpdateRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri;
    uri.append_path(_YTEXT("drives"));
    uri.append_path(m_DriveId);
    uri.append_path(_YTEXT("items"));
    uri.append_path(m_DriveItemId);

    json::value json = json::value::object();
    ASSERT(m_NewName != boost::none);
    if (m_NewName != boost::none)
        json[_YTEXT("name")] = json::value::string(*m_NewName);
    
	const WebPayloadJSON payload(json);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
    return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Patch(uri.to_string(), httpLogger, p_TaskData, payload).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResultInfo) {
        try
        {
            result->SetResult(p_ResultInfo.get());
        }
        catch (const RestException& e)
        {
            RestResultInfo info(e.GetRequestResult());
            info.m_RestException = std::make_shared<RestException>(e);
            result->SetResult(info);
        }
        catch (const std::exception& e)
        {
            RestResultInfo info;
            info.m_Exception = e;
            result->SetResult(info);
        }
        catch (...)
        {
            result->SetResult(RestResultInfo());
        }
    });
}

const SingleRequestResult& DriveItemUpdateRequester::GetResult() const
{
    return *m_Result;
}

void DriveItemUpdateRequester::SetName(PooledString p_NewName)
{
    m_NewName = p_NewName;
}
