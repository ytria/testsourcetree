#include "ModuleColumns.h"

#include "BusinessList.h"
#include "BusinessObjectManager.h"
#include "FrameColumns.h"
#include "GraphCache.h"
#include "O365AdminUtil.h"
#include "RefreshSpecificData.h"
#include "safeTaskCall.h"

void ModuleColumns::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
    {
    case Command::ModuleTask::List:
		showColumns(p_Command);
		break;

    default:
        ASSERT(false);
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
        break;
    }
}

void ModuleColumns::showColumns(const Command& p_Command)
{
	const auto& info = p_Command.GetCommandInfo();

	auto	existingFrame	= dynamic_cast<FrameColumns*>(info.GetFrame());
	auto&	p_Lists			= info.GetSubIds();
	auto	p_ExistingFrame	= dynamic_cast<FrameColumns*>(info.GetFrame());
	auto	p_SourceWindow	= p_Command.GetSourceWindow();
	auto	p_Action		= p_Command.GetAutomationAction();

	auto sourceCWnd = p_SourceWindow.GetCWnd();
	auto frame = p_ExistingFrame;
	if (nullptr == p_ExistingFrame)
	{
		if (FrameColumns::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin			= Origin::Lists;
			moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::SUBIDS;
			moduleCriteria.m_SubIDs			= p_Lists;
			moduleCriteria.m_Privilege		= info.GetRBACPrivilege();

			if (ShouldCreateFrame<FrameColumns>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				frame = new FrameColumns(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleColumns_showColumns_1, _YLOC("SharePoint Site List Columns")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
			else
				return;
		}
	}

	if (nullptr != frame)
	{
		doRefresh(frame, frame->GetModuleCriteria(), p_Command, nullptr != p_ExistingFrame);
	}
	else
	{
		ASSERT(nullptr == p_Action);
	}
}

void ModuleColumns::doRefresh(FrameColumns* frameColumns, const ModuleCriteria& p_ModuleCriteria, const Command& p_Command, bool p_IsUpdate)
{
    auto taskData = addFrameTask(YtriaTranslate::Do(ModuleColumns_doRefresh_1, _YLOC("Show SharePoint Site List Columns")).c_str(), frameColumns, p_Command, !p_IsUpdate);

    RefreshSpecificData refreshSpecificData;
    if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
        refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

    ASSERT(ModuleCriteria::UsedContainer::SUBIDS == p_ModuleCriteria.m_UsedContainer);

	YSafeCreateTask([taskData, p_ModuleCriteria, refreshSpecificData, p_Action = p_Command.GetAutomationSetup().m_ActionToExecute, hwnd = frameColumns->GetSafeHwnd(), bom = frameColumns->GetBusinessObjectManager(), p_IsUpdate]()
    {
		O365DataMap<BusinessSite, vector<BusinessList>> lists;
        vector<BusinessSite> sites;

        const bool partialRefresh = !refreshSpecificData.m_Criteria.m_SubIDs.empty();
		const auto& subIds = partialRefresh ? refreshSpecificData.m_Criteria.m_SubIDs : p_ModuleCriteria.m_SubIDs;
		
		auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("list columns"), subIds.size());

        for (const auto& list : subIds)
        {
			logger->IncrementObjCount();
            PooledString siteId = list.first.GetId();
			PooledString listDispName = list.first.GetDisplayName();

			BusinessSite businessSite;
			businessSite.SetID(siteId);
			logger->SetContextualInfo(bom->GetGraphCache().GetSiteContextualInfo(siteId));
			bom->GetGraphCache().SyncUncachedSite(businessSite, logger, taskData);

            for (const auto& listId : list.second)
            {
				BusinessList finalList;
				finalList.SetID(listId);
				finalList.SetDisplayName(listDispName);
				
				logger->SetContextualInfo(GetDisplayNameOrId(finalList));

				if (!taskData.IsCanceled())
					finalList = bom->GetBusinessList(siteId, listId, logger, taskData).GetTask().get();

				if (taskData.IsCanceled())
					finalList.SetFlags(BusinessObject::CANCELED);

				lists[businessSite].push_back(finalList);
            }

			if (taskData.IsCanceled())
				businessSite.SetFlags(BusinessObject::CANCELED);
        }

		YDataCallbackMessage<O365DataMap<BusinessSite, vector<BusinessList>>>::DoPost(lists, [taskData, hwnd, p_Action, partialRefresh, p_IsUpdate, isCanceled = taskData.IsCanceled()](const O365DataMap<BusinessSite, vector<BusinessList>>& p_Lists)
		{
			bool shouldFinishTask = true;
			auto frameColumns = ::IsWindow(hwnd) ? dynamic_cast<FrameColumns*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, false))
			{
				ASSERT(nullptr != frameColumns);
				frameColumns->GetGrid().SetProcessText(_YFORMAT(L"Inserting list columns for %s sites...", Str::getStringFromNumber(p_Lists.size()).c_str()));
				frameColumns->GetGrid().ClearLog(taskData.GetId());

				{
					CWaitCursor _;
					frameColumns->ShowColumns(p_Lists, !partialRefresh);
				}

				if (!p_IsUpdate)
				{
					frameColumns->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frameColumns, taskData, p_Action);
			}
		});

    });
}
