#include "IdentityDeserializer.h"

#include "JsonSerializeUtil.h"
#include "MsGraphFieldNames.h"

void IdentityDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.DisplayName, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT(O365_ID), m_Data.Id, p_Object, true);
    JsonSerializeUtil::DeserializeString(_YTEXT("email"), m_Data.Email, p_Object);
}
