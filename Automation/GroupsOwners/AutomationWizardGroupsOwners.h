#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardGroupsOwners : public AutomationWizardO365
{
public:
	AutomationWizardGroupsOwners();

	virtual wstring GetName() const override
	{
		return _YTEXT("GroupOwners");
	}
};