﻿#include "AutomationWizardLicensesSummary.h"
AutomationWizardLicensesSummary::AutomationWizardLicensesSummary()
// Generated by post commit revision: 155066
{
// [20-11-05] Generated by SVN post commit hook of LicensesSummary/XML/ChartlGlobalLicenseConsumption.xml
    addScript(_YTEXT("LicensesSummary/ChartlGlobalLicenseConsumption.xml"), _YTEXT("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n"
                                                                                   "<ytriaAutomation Console=\"False\" Application=\"sapio365\"\n"
                                                                                   "	WizardTitle=\"Chart of global license consumption\" \n"
                                                                                   "	WizardDescription=\"\" \n"
                                                                                   "	WizardTooltip=\"\"\n"
                                                                                   "	WizardVersion=\"@200903:00002\"\n"
                                                                                   "	WizardIcon=\"binoculars\">\n"
                                                                                   "	\n"
                                                                                   "	<setvar vJobName=\"Chart of global license consumption\" />\n"
                                                                                   "	\n"
                                                                                   "	\n"
                                                                                   "	<Execute FilePath=\"Common/SplashRun.xml\"/>\n"
                                                                                   "	<Execute FilePath=\"Common/initialval.xml\"/>\n"
                                                                                   "	<Execute FilePath=\"Common/ResetGridinjobs.xml\"/>\n"
                                                                                   "	<Collapse/>\n"
                                                                                   "	<Select Lines=\"all\"/>\n"
                                                                                   "	<SetVarListFromGridSelection ListName=\"SkuList\" SkuName=\"subscribedSku.skuPartNumber\"/>\n"
                                                                                   "	\n"
                                                                                   "	<userinput Title=\"Licenses to include in the chart\">\n"
                                                                                   "		<Variable name=\"varFromGridSelection\" ListName=\"LICENSEcheckList\" Type=\"CheckList\" Label=\"Select:\" Tooltip=\"Please make a selection of the relevant licenses to plot\" mandatory=\"true\"><!-- check list from grid selection -->\n"
                                                                                   "			<ListItem ListName=\"SkuList\" value=\"SkuName\" Select=\"true\"/>\n"
                                                                                   "		</Variable>\n"
                                                                                   "	</userinput>\n"
                                                                                   "	<SetVarListSize Vifselect=\"LICENSEcheckList\"/>\n"
                                                                                   "	<setvar plotLicense=\"\"/>\n"
                                                                                   "	\n"
                                                                                   "	<if target=\"Var\" Test=\"{%Vifselect%}\" Mode=\"Equals\" Value=\"0\">\n"
                                                                                   "		<Execute FilePath=\"Common/geterror.xml\"/>\n"
                                                                                   "	</if>\n"
                                                                                   "	<Loop list=\"LICENSEcheckList\">	\n"
                                                                                   "		<ExecuteListAction/>\n"
                                                                                   "		<setvar plotLicense=\"{%plotLicense%}:{%varFromGridSelection%}\" />\n"
                                                                                   "	</Loop>\n"
                                                                                   "	<SuspendRedraw/>\n"
                                                                                   "		<Filter columnID=\"subscribedSku.skuPartNumber\" Value=\"{%plotLicense%}\"/>\n"
                                                                                   "		<Remove ColumnID=\"subscribedSku.prepaidUnits.suspended\"/>\n"
                                                                                   "		<Remove ColumnID=\"subscribedSku.prepaidUnits.warning\"/>\n"
                                                                                   "		<SwitchToFlatView/>\n"
                                                                                   "	<UpdateGrid/>\n"
                                                                                   "	<Select Lines=\"All\"/>\n"
                                                                                   "	\n"
                                                                                   "   	<if Test=\"SelectionCount\" Mode=\"Equals\" Value=\"0\">\n"
                                                                                   "		\n"
                                                                                   "		<Execute FilePath=\"Common/geterror.xml\"/>\n"
                                                                                   "	</if>\n"
                                                                                   "	\n"
                                                                                   "	<Chart KeepAlive=\"this\">\n"
                                                                                   "		<SetGridChartDefault>\n"
                                                                                   "			<AddColumn ColumnID=\"subscribedSku.skuPartNumber\" Title=\"Sku Part Number\" Type=\"ValueX\"/>\n"
                                                                                   "			<AddColumn Calculation=\"0\" Color=\"255\" ColumnID=\"subscribedSku.consumedUnits\" Display=\"0\" Legend=\"Consumed\" Title=\"Consumed\" Type=\"ValueY\" Unicity=\"\">\n"
                                                                                   "			  <SetColumnFormat Type=\"TFMTRegular\"/>\n"
                                                                                   "			  <SetColumnFormat Type=\"NFMTRegular\"/>\n"
                                                                                   "			  <SetColumnFormat CBX_FMT_CBXSortFlag=\"0\" Type=\"CheckBoxFmtRegular\"/>\n"
                                                                                   "			  <SetColumnFormat TextAlignment=\"0\" TextCase=\"0\" TextColor=\"4294967295\" TextNotesName=\"False\" Type=\"TextFmtRegular\"/>\n"
                                                                                   "			</AddColumn>\n"
                                                                                   "			<AddColumn Calculation=\"0\" Color=\"3329330\" ColumnID=\"subscribedSku.prepaidUnits.enabled\" Display=\"0\" Legend=\"Enabled\" Title=\"Enabled\" Type=\"ValueY\" Unicity=\"\">\n"
                                                                                   "			  <SetColumnFormat Type=\"TFMTRegular\"/>\n"
                                                                                   "			  <SetColumnFormat Type=\"NFMTRegular\"/>\n"
                                                                                   "			  <SetColumnFormat CBX_FMT_CBXSortFlag=\"0\" Type=\"CheckBoxFmtRegular\"/>\n"
                                                                                   "			  <SetColumnFormat TextAlignment=\"0\" TextCase=\"0\" TextColor=\"4294967295\" TextNotesName=\"False\" Type=\"TextFmtRegular\"/>\n"
                                                                                   "			</AddColumn>\n"
                                                                                   "			<Option Consolidate=\"True\" Normalize=\"False\" ShowHidden=\"False\" ShowLegendRight=\"True\" ShowLegendTop=\"False\" ShowSegments=\"True\" ShowValues=\"False\" Subtype=\"2\" SwapAxis=\"False\" Title=\"Relative consumption of purchased licenses\" TitleX=\"License SKU\" TitleY=\"Units\" Type=\"0\" VisibleOnly=\"False\">\n"
                                                                                   "			  <SetColumnFormat/>\n"
                                                                                   "			  <SetColumnFormat Type=\"0\"/>\n"
                                                                                   "			  <SetColumnFormat Type=\"0\"/>\n"
                                                                                   "			</Option>\n"
                                                                                   "			</SetGridChartDefault>\n"
                                                                                   "	</Chart>\n"
                                                                                   "	\n"
                                                                                   "	<if Test=\"SelectionCount\" Mode=\"notEquals\" Value=\"0\">\n"
                                                                                   "		<Execute FilePath=\"Common/exportatt.xml\"/>\n"
                                                                                   "        \n"
                                                                                   "		<UnSelect Lines=\"All\"/>	\n"
                                                                                   "        <Execute FilePath=\"Common/getresult.xml\"/>\n"
                                                                                   "	</if>\n"
                                                                                   "	\n"
                                                                                   "</ytriaAutomation>\n"));

// [20-11-05] Generated by SVN post commit hook of LicensesSummary/XML/Licenseselection_userinput.xml
    addScript(_YTEXT("LicensesSummary/Licenseselection_userinput.xml"), _YTEXT("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n"
                                                                               "<ytriaAutomation Application=\"sapio365\"\n"
                                                                               "    WizardTitle=\"Licenses job selection\"\n"
                                                                               "	WizardDescription=\"\"\n"
                                                                               "	WizardTooltip=\"\"\n"
                                                                               "	WizardRequirements=\"\"\n"
                                                                               "	WizardVersion=\"@201105:00006\"\n"
                                                                               "	WizardFlags=\"lib\"\n"
                                                                               "	WizardIcon=\"\">\n"
                                                                               " \n"
                                                                               "	<if target=\"var\" Test=\"{%vLoaduserinput%}\" Mode=\"NotEquals\" Value=\"true\">\n"
                                                                               "		<SetVarListsFromGridColumns target=\"ShowLicenses\" licensename=\"subscribedSku.skuPartNumber\" ExecuteInPreset=\"true\"/>\n"
                                                                               "		\n"
                                                                               "		<UserInput Title=\"{%vJobName%}\">\n"
                                                                               "			<Variable name=\"state1\" Type=\"label\" label=\"This job lists who have selected licenses assigned. From there, you can remove or assign new licenses to those users:\"/>\n"
                                                                               "			<Variable name=\"GridSelectionlicense\" ListName=\"licensecheckList\" Type=\"CheckList\" Label=\"Select licenses:\" mandatory=\"true\"><!-- check list from grid selection -->\n"
                                                                               "				<ListItem ListName=\"licensename\" value=\"licensename\"/>\n"
                                                                               "			</Variable>\n"
                                                                               "			\n"
                                                                               "		</UserInput>\n"
                                                                               "		<setvar vLoaduserinput=\"true\"/>\n"
                                                                               "	</if>\n"
                                                                               "	\n"
                                                                               "</ytriaAutomation>\n"));

// [20-11-05] Generated by SVN post commit hook of LicensesSummary/XML/10-UsersForSelectedLicenses.xml
    addScript(_YTEXT("LicensesSummary/10-UsersForSelectedLicenses.xml"), _YTEXT("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n"
                                                                                "<ytriaAutomation Application=\"sapio365\"\n"
                                                                                "    WizardTitle=\"Users for selected licenses\"\n"
                                                                                "	WizardDescription=\"\"\n"
                                                                                "	WizardTooltip=\"\"\n"
                                                                                "	WizardRequirements=\"RequiredLogin\"\n"
                                                                                "	WizardVersion=\"@200623:00001\"\n"
                                                                                "	WizardIcon=\"file-certificate\">\n"
                                                                                "\n"
                                                                                "    <setvar vJobName=\"Users for selected licenses\" />\n"
                                                                                "	\n"
                                                                                "    <Execute FilePath=\"Common/initialval.xml\"/>\n"
                                                                                "\n"
                                                                                "    <if Test=\"SelectionCount\" Mode=\"Equals\" Value=\"0\">\n"
                                                                                "      <echo value=\"You need to select one or more than one Licesnses!\"/>\n"
                                                                                "      <MsgBox Title=\"Warning\" Message=\"No rows selected. You must first make a selection to run this job.\" Type=\"Warning\"/>\n"
                                                                                "      <StopAutomation/>\n"
                                                                                "    </if>\n"
                                                                                "	<Execute FilePath=\"Common/SplashRun.xml\"/>\n"
                                                                                "    <if Test=\"SelectionCount\" Mode=\"Equals\" Value=\"1\">\n"
                                                                                "      <SetVarListFromGridSelection ListName=\"licensenum\" LicenseSku=\"subscribedSku.skuPartNumber\"/>\n"
                                                                                "      <setvar vPreset_ErrorString= \"An error occurred while loading users.\"/>\n"
                                                                                "	  <ShowUsers>\n"
                                                                                "        <SelectColumn ColumnID=\"assignedLicenses\"/>\n"
                                                                                "        <Select lines=\"All\"/>\n"
                                                                                "        <ExplodeMultivalues ColumnID=\"assignedLicenses\"/>\n"
                                                                                "        <loop list=\"licensenum\">\n"
                                                                                "          <ExecuteListAction/>\n"
                                                                                "          <Filter ColumnID=\"assignedLicenses\" Value=\"{%LicenseSku%}\"/>	 \n"
                                                                                "            <setvar vPreset_ErrorString= \"An error occurred while loading licenses for users.\"/>\n"
                                                                                "			<SelectedShowLicenses KeepAlive=\"true\">\n"
                                                                                "            <Select Lines=\"ByValue\">\n"
                                                                                "              <SetParam  ColumnID =\"licenseDetails.skuPartNumber\" value=\"{%LicenseSku%}\"/>\n"
                                                                                "            </Select>\n"
                                                                                "			<if Test=\"SelectionCount\" Mode=\"Equals\" Value=\"0\">\n"
                                                                                "				\n"
                                                                                "				<Execute FilePath=\"Common/geterror.xml\"/>\n"
                                                                                "			</if>	\n"
                                                                                "			<else>\n"
                                                                                "				<Execute FilePath=\"Common/exportatt.xml\"/>\n"
                                                                                "				\n"
                                                                                "				<UnSelect Lines=\"All\"/>	\n"
                                                                                "				<Execute FilePath=\"Common/getresult.xml\"/>\n"
                                                                                "			</else>\n"
                                                                                "          </SelectedShowLicenses>\n"
                                                                                "        </loop>\n"
                                                                                "      </ShowUsers>\n"
                                                                                "    </if>\n"
                                                                                "\n"
                                                                                "    <if Test=\"SelectionCount\" Mode=\"Greaterthan\" Value=\"1\">\n"
                                                                                "      <SetVarListFromGridSelection ListName=\"licensenum\" LicenseSku=\"subscribedSku.skuPartNumber\"/>\n"
                                                                                "      <setvar vPreset_ErrorString= \"An error occurred while loading users.\"/>\n"
                                                                                "	  <ShowUsers>\n"
                                                                                "        <SelectColumn ColumnID=\"assignedLicenses\"/>\n"
                                                                                "        <Select lines=\"All\"/>\n"
                                                                                "        <ExplodeMultivalues ColumnID=\"assignedLicenses\"/>\n"
                                                                                "        <loop list=\"licensenum\">\n"
                                                                                "          <ExecuteListAction/>\n"
                                                                                "          <If index=\"first\">\n"
                                                                                "            <SetVar myVar=\"{%LicenseSku%}\"/>\n"
                                                                                "          </If>\n"
                                                                                "          <If index=\"Other\">\n"
                                                                                "            <SetVar myVar=\"{%myVar%}:{%LicenseSku%}\"/>\n"
                                                                                "          </If>\n"
                                                                                "          <If index=\"Last\">\n"
                                                                                "            <SetVar myVar=\"{%myVar%}:{%LicenseSku%}\"/>\n"
                                                                                "          </If>\n"
                                                                                "        </loop>\n"
                                                                                "        <Filter ColumnID=\"assignedLicenses\" Value=\"{%myVar%}\"/>	 \n"
                                                                                "        <setvar vPreset_ErrorString= \"An error occurred while loading licenses for users.\"/>\n"
                                                                                "		<SelectedShowLicenses KeepAlive=\"true\">              \n"
                                                                                "			<Select Lines=\"All\"/>\n"
                                                                                "			<if Test=\"SelectionCount\" Mode=\"Equals\" Value=\"0\">\n"
                                                                                "		      \n"
                                                                                "			  <Execute FilePath=\"Common/geterror.xml\"/>\n"
                                                                                "	        </if>	\n"
                                                                                "			<else>\n"
                                                                                "		      <Execute FilePath=\"Common/exportatt.xml\"/>\n"
                                                                                "			  \n"
                                                                                "			  <UnSelect Lines=\"All\"/>	\n"
                                                                                "			  <Execute FilePath=\"Common/getresult.xml\"/>\n"
                                                                                "	    	</else>\n"
                                                                                "		</SelectedShowLicenses>	\n"
                                                                                "      </ShowUsers>\n"
                                                                                "    </if>\n"
                                                                                "\n"
                                                                                "</ytriaAutomation>\n"));

// [20-11-05] Generated by SVN post commit hook of LicensesSummary/XML/005-LICSUM-Unlicensed-users.xml
    addScript(_YTEXT("LicensesSummary/005-LICSUM-Unlicensed-users.xml"), _YTEXT("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n"
                                                                                "<ytriaAutomation Application=\"sapio365\"\n"
                                                                                "    WizardTitle=\"Users without any licenses\"\n"
                                                                                "	WizardDescription=\"\"\n"
                                                                                "	WizardTooltip=\"Get a list of users with no licenses at all.\"\n"
                                                                                "	WizardRequirements=\"RequiredLogin\"\n"
                                                                                "	WizardVersion=\"@200826:00001\"\n"
                                                                                "	WizardIcon=\"user-times\">\n"
                                                                                " \n"
                                                                                "	    <!-- FROM LICENSE SUMMARY -->\n"
                                                                                "		\n"
                                                                                "		<!-- Set the number of licenses as a var -->\n"
                                                                                "		<setvar vJobName=\"Users without any licenses\" />\n"
                                                                                "		<Execute FilePath=\"Common/SplashRun.xml\"/>\n"
                                                                                "		<Execute FilePath=\"Common/initialval.xml\"/>\n"
                                                                                "		<Execute FilePath=\"Common/ResetGridinjobs.xml\"/> \n"
                                                                                "				\n"
                                                                                "		<Collapse/>\n"
                                                                                "		<Select Lines=\"All\"/>\n"
                                                                                "		<SetVarListFromGridSelection ListName=\"licensenum\" LicenseSku=\"subscribedSku.skuPartNumber\"/>\n"
                                                                                "		<SetVarListSize LicenseListSize=\"licensenum\"/>\n"
                                                                                "		<Echo value=\"Number of licenses: {%LicenseListSize%}\"/>\n"
                                                                                "			\n"
                                                                                "		<!-- Launch licenses for all users -->\n"
                                                                                "		<setvar vPreset_ErrorString= \"An error occurred while loading users.\"/>\n"
                                                                                "		<ShowUsers>	\n"
                                                                                "			<Select lines=\"All\"/>\n"
                                                                                "			<setvar vPreset_ErrorString= \"An error occurred while loading licenses for users.\"/>\n"
                                                                                "			<SelectedShowLicenses KeepAlive=\"true\">\n"
                                                                                "				<ShowUnassignedLicenses/>\n"
                                                                                "				<AddFilterTarget type=\"Subscribed sku\"/>\n"
                                                                                "				<Filter columnID=\"isAssigned\" Value=\"NO - LICENSE\" />\n"
                                                                                "				<Select Lines=\"byValue\">\n"
                                                                                "					<SetParam ColumnID=\"*H8*\" Value=\"{%LicenseListSize%}\"/>\n"
                                                                                "				</Select>\n"
                                                                                "						\n"
                                                                                "			<ClearFilters/>\n"
                                                                                "			<Filter columnID=\"OBJECTTYPE\" Value=\"User\" />\n"
                                                                                "			<Filter columnID=\"OBJECTTYPE\" Value=\"Guest\" />\n"
                                                                                "			\n"
                                                                                "			<InvertSelection/>\n"
                                                                                "			<HideSelectedRows/>\n"
                                                                                "			<ClearFilters/>\n"
                                                                                "			<Expand/>\n"
                                                                                "			<Select Lines=\"All\"/>\n"
                                                                                "			<if Test=\"SelectionCount\" Mode=\"Equals\" Value=\"0\">\n"
                                                                                "				\n"
                                                                                "				<Execute FilePath=\"Common/geterror.xml\"/>\n"
                                                                                "			</if>	\n"
                                                                                "			<else>\n"
                                                                                "				<Execute FilePath=\"Common/exportatt.xml\"/>\n"
                                                                                "				\n"
                                                                                "				<UnSelect Lines=\"All\"/>	\n"
                                                                                "				<Execute FilePath=\"Common/getresult.xml\"/>\n"
                                                                                "			</else>	\n"
                                                                                "			</SelectedShowLicenses>\n"
                                                                                "		</ShowUsers>\n"
                                                                                "		\n"
                                                                                "</ytriaAutomation>\n"
                                                                                "\n"));

}
