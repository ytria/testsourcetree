#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardLicensesSummary : public AutomationWizardO365
{
public:
	AutomationWizardLicensesSummary();

	virtual wstring GetName() const override
	{
		return _YTEXT("LicensesSummary");
	}
};