#pragma once
#include "CacheGrid.h"
#include "VendorLicense.h"
#include "VendorFeature.h"

class JobCenterGridDebug : public CacheGrid
{
public:
	JobCenterGridDebug();

protected:
	virtual void	customizeGrid() override;
	void			loadJobCenters();

private:
	int32_t ICON_SYSTEM, ICON_DISPLAYED, ICON_SHARED;

	GridBackendColumn* m_ColModuleName;
	GridBackendColumn* m_ColModuleJobCount;
	GridBackendColumn* m_ColKey;
	GridBackendColumn* m_ColIconNameFontAwesome;
	GridBackendColumn* m_ColTitle;
	GridBackendColumn* m_ColDesc;
	GridBackendColumn* m_ColTooltip;
	GridBackendColumn* m_ColPosition;
	GridBackendColumn* m_ColXML;
	GridBackendColumn* m_ColCommonRestrictions;

	GridBackendColumn* m_ColDisplayed;
	GridBackendColumn* m_ColShared;
	GridBackendColumn* m_ColSystem;// is script provided by Ytria
	GridBackendColumn* m_ColSourceFilePath;

	GridBackendColumn* m_ColDateLastUpdate;
	GridBackendColumn* m_ColDateLastUpdateBy;
	GridBackendColumn* m_ColDateLastUpdateByEmail;
	GridBackendColumn* m_ColDateLastShared;
};