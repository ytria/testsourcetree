#include "DlgAutomationWizardEditHTML.h"

#include "CRMpipe.h"

const wstring& DlgAutomationWizardEditHTML::g_VarNameTitle			= _YTEXT("JobTitle");
const wstring& DlgAutomationWizardEditHTML::g_VarNameDescription	= _YTEXT("JobDescription");
const wstring& DlgAutomationWizardEditHTML::g_VarNameTooltip		= _YTEXT("JobTooltip");
const wstring& DlgAutomationWizardEditHTML::g_VarNameIcon			= _YTEXT("JobIcon");

DlgAutomationWizardEditHTML::DlgAutomationWizardEditHTML(Script& p_Script, CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
	, m_Script(p_Script)
{
	ASSERT(!m_Script.IsSystem() || CRMpipe().IsSandbox());
	setIgnoreBlockVertResizeForHTMLContainer(true);
}

DlgAutomationWizardEditHTML::~DlgAutomationWizardEditHTML()
{
}

void DlgAutomationWizardEditHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgAutomationWizardEditHTML"));

	const bool SS = m_Script.IsSystem() && CRMpipe().IsSandbox();

	getGenerator().Add(StringEditor(g_VarNameTitle, _T("Job Title"), EditorFlags::DIRECT_EDIT | (SS ? 0 : EditorFlags::REQUIRED), m_Script.m_Title));
	getGenerator().Add(StringEditor(g_VarNameDescription, _T("Description"), 0, m_Script.m_Description));
	getGenerator().Add(StringEditor(g_VarNameTooltip, _T("Tooltip"), 0, m_Script.m_Tooltip));
	getGenerator().Add(WithTooltip<StringEditor>({ g_VarNameIcon, _T("Icon name"), 0, m_Script.m_IconFontAwesome }, _T("Type or paste the name of the Font Awesome icon only. sapio365 will do the rest. No additional info is required.")));
	getGenerator().addIconTextField(_YTEXT("infoIcon")
		, _T("How to add Font Awesome icons:<br>You only need to paste the icon name itself in the field above. sapio365 will automatically do the rest. (Refer to the <a href='https://fontawesome.com/cheatsheet/pro/regular'>Font Awesome Pro's Cheatsheet</a> for the complete list of available names.)")
		, _T("far fa-info-circle"));

	getGenerator().addApplyButton(YtriaTranslate::Do(DlgAutomationInputHTML_generateJSONScriptData_1, _YLOC("OK")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgAutomationInputHTML_generateJSONScriptData_2, _YLOC("Cancel")).c_str());
}

bool DlgAutomationWizardEditHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	if (!m_Script.IsSystem())
	{
		for (const auto& item : data)
		{
			if (MFCUtil::StringMatchW(item.first, g_VarNameTitle))
				m_Script.m_Title = Str::lrtrim(item.second);
			else if (MFCUtil::StringMatchW(item.first, g_VarNameDescription))
				m_Script.m_Description = Str::lrtrim(item.second);
			else if (MFCUtil::StringMatchW(item.first, g_VarNameTooltip))
				m_Script.m_Tooltip = Str::lrtrim(item.second);
			else if (MFCUtil::StringMatchW(item.first, g_VarNameIcon))
			{
				auto val = Str::lrtrim(item.second);
				if (!Str::beginsWith(val, JobCenterUtil::g_FAPrefixTypeIconLight)
					&& !Str::beginsWith(val, JobCenterUtil::g_FAPrefixTypeIconRegular)
					&& !Str::beginsWith(val, JobCenterUtil::g_FAPrefixTypeIconSolid))
					val = JobCenterUtil::g_FAPrefixTypeIconRegular + val;
				m_Script.m_IconFontAwesome = val;
			}
		}
	}
	
	return true;
}

wstring DlgAutomationWizardEditHTML::getDialogTitle() const
{
	return _YFORMAT(L"Edit metadata for job: %s", m_Script.m_Key.c_str());
}