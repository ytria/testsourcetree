#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardDriveItemsChannel : public AutomationWizardO365
{
public:
	AutomationWizardDriveItemsChannel();

	virtual wstring GetName() const override
	{
		return _YTEXT("DriveItemsChannel");
	}
};