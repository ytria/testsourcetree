#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardConversations : public AutomationWizardO365
{
public:
	AutomationWizardConversations();

	virtual wstring GetName() const override
	{
		return _YTEXT("Conversations");
	}
};