#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardMessages : public AutomationWizardO365
{
public:
	AutomationWizardMessages();

	virtual wstring GetName() const override
	{
		return _YTEXT("Messages");
	}
};