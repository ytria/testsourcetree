#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardGroupsMembers : public AutomationWizardO365
{
public:
	AutomationWizardGroupsMembers();

	virtual wstring GetName() const override
	{
		return _YTEXT("GroupMembers");
	}
};