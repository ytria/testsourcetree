#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardApplications : public AutomationWizardO365
{
public:
	AutomationWizardApplications();
	wstring GetName() const override
	{
		return _YTEXT("Applications");
	}
};

