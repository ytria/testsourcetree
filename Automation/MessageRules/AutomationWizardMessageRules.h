#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardMessageRules : public AutomationWizardO365
{
public:
	AutomationWizardMessageRules();

	virtual wstring GetName() const override
	{
		return _YTEXT("MessageRules");
	}
};