#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardOnPremiseUsers : public AutomationWizardO365
{
public:
	AutomationWizardOnPremiseUsers();

	wstring GetName() const override;
};

