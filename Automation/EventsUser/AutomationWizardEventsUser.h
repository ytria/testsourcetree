#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardEventsUser : public AutomationWizardO365
{
public:
	AutomationWizardEventsUser();

	virtual wstring GetName() const override
	{
		return _YTEXT("EventsUser");
	}
};