#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardLicenses : public AutomationWizardO365
{
public:
	AutomationWizardLicenses();

	virtual wstring GetName() const override
	{
		return _YTEXT("Licenses");
	}

};