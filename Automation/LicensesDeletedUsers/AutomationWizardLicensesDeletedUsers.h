#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardLicensesDeletedUsers : public AutomationWizardO365
{
public:
	AutomationWizardLicensesDeletedUsers();

	virtual wstring GetName() const override
	{
		return _YTEXT("LicensesDeletedUsers");
	}
};