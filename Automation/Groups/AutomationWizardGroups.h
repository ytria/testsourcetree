#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardGroups : public AutomationWizardO365
{
public:
	AutomationWizardGroups();

	virtual wstring GetName() const override
	{
		return _YTEXT("Groups");
	}
};