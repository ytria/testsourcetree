#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardLists : public AutomationWizardO365
{
public:
	AutomationWizardLists();

	virtual wstring GetName() const override
	{
		return _YTEXT("Lists");
	}
};