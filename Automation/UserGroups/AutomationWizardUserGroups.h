#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardUserGroups : public AutomationWizardO365
{
public:
	AutomationWizardUserGroups();

	virtual wstring GetName() const override
	{
		return _YTEXT("UserGroups");
	}
};