#pragma once

#include "AutomationWizardO365.h"
#include "JobCenterUtil.h"

class AutomationWizardO365Reports : public AutomationWizardO365
{
public:
	AutomationWizardO365Reports();

	virtual wstring GetName() const override
	{
		return _YTEXT("O365Reports");
	}
};
