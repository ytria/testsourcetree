#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardUserManagerHierarchy : public AutomationWizardO365
{
public:
	AutomationWizardUserManagerHierarchy();

	virtual wstring GetName() const override
	{
		return _YTEXT("UserManagerHierarchy");
	}
};