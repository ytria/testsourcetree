#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardColumns : public AutomationWizardO365
{
public:
	AutomationWizardColumns();

	virtual wstring GetName() const override
	{
		return _YTEXT("Columns");
	}
};