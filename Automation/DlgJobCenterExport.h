#pragma once

#include "ResizableDialog.h"
#include "..\Resource.h"

#include "FileExistsAction.h"

class DlgJobCenterExport : public ResizableDialog
{
public:
	enum { IDD = IDD_DLG_JOBCENTER_EXPORT };

    DlgJobCenterExport(CWnd* p_Parent);
    
    const wstring&		GetFolderPath() const;
	bool				IsOpenAfterDownload() const;
	FileExistsAction	GetFileExistsAction() const;

	static const wstring g_AutomationName;

protected:
    virtual void DoDataExchange(CDataExchange* pDX) override;
    virtual BOOL OnInitDialogSpecific();
    virtual void OnOK() override;

    afx_msg void OnSelectFolder();
    DECLARE_MESSAGE_MAP()

private:
    CExtEdit		m_TxtFolderPath;
    CXTPButton		m_BtnChooseFolder;
    CExtCheckBox	m_ChkOpenAfterDownload;
	
	CExtGroupBox	m_GroupBox;
	CExtRadioButton m_RadioBtnCreateNew;
	CExtRadioButton m_RadioBtnSkip;
	CExtRadioButton m_RadioBtnOverwrite;

    CXTPButton m_BtnOk;
    CXTPButton m_BtnCancel;

    wstring	m_FolderPath;
    bool	m_PreserveHierarchy;
    bool    m_OpenAfterDownload;

	FileExistsAction m_FileExistsAction;

	static const wstring g_RegKeyLastFolder;
	static const wstring g_RegKeyExistingFile;
	static const wstring g_RegKeyOpenAfterExport;
	static const wstring g_CreateNew;
	static const wstring g_Skip;
	static const wstring g_Overwrite;
};

