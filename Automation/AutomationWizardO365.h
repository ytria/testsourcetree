#pragma once

#include "AutomationWizard.h"
#include "DlgDoubleProgressCommon.h"
#include "JobCenterUtil.h"
#include "Sapio365Session.h"

class AutomationWizardO365;
using AutomationWizardPointerType = std::unique_ptr<AutomationWizardO365>; // used to be std::shared_ptr<AutomationWizard>

class AutomationWizardO365 : public AutomationWizard
{
public:
	virtual void			RunFooter() override;
	virtual uint32_t		GetScriptCount() const override;
	virtual bool			HasScript(const wstring& p_ScriptKey) const override;
	virtual bool			HasScriptPreset(const wstring& p_ScriptPresetKey) const override;
	virtual	vector<Script>	GetScripts() override;// returns module scripts + common scripts
	virtual Script			GetScript(const wstring& p_ScriptKey) const override;
	Script					GetScript(const int64_t p_ScriptID) const;
	virtual ScriptPreset	GetScriptPreset(const wstring& p_ScriptPresetKey) const override;
	virtual ScriptSchedule	GetScriptSchedule(const wstring& p_ScriptScheduleKey) const override;
	virtual bool			ImportScript(Script& p_Script) override;// add or update: replaces existing script with same key

	bool	UpdateScript(Script& p_Script);
	bool	RemoveScript(Script& p_Script);
	bool	ToggleScriptDisplay(Script& p_Script);

	bool	AddScriptPreset(const Script& p_Script, const wstring& p_PresetContent, ScriptPreset& p_ScriptPreset);
	bool	RemoveScriptPreset(ScriptPreset& p_ScriptPreset);
	bool	UpdateScriptPreset(ScriptPreset& p_Script);
	
	bool	AddScriptSchedule(const Script& p_Script, const ScriptPreset& p_ScriptPreset, const ScriptSchedule& p_ScriptSchedule);
	bool	AddScriptSchedule(const Script& p_Script, const ScriptSchedule& p_ScriptSchedule);
	bool	UpdateScriptSchedule(ScriptSchedule& p_ScriptSchedule);
	bool	RemoveScriptSchedule(ScriptSchedule& p_ScriptSchedule);
	
	ScriptSchedule GetScriptScheduleByID(const wstring& p_ScriptScheduleID) const;

	void					ClearScheduleRemoveErrors();
	const vector<wstring>&	GetScheduleRemoveErrors() const;

	void FixPositions();

	static void InitAfterSessionWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress);

	static bool CheckRequirements(const std::set<wstring>& p_Requirements, bool p_ShowDialog, const AutomationContext& p_Context, const wstring& p_ScriptKey, bool p_StopOnFirstFailure, AutomationAction* p_Action);

protected:
	virtual wstring generateJSON() override;
	virtual void	JSONgenerationPreProcess() override;
	virtual void	JSONgenerationPostProcess() override;
	virtual bool	runSpecific(const wstring& p_ScriptKey) override;
	virtual bool	scriptAuthorized(const Script& p_Script, const AutomationContext& p_Context) override;
	virtual void	setFramePostProcess() override;

	static map<wstring, Script> g_DebugScriptMap;

public:
	static const wstring g_GoBackstageBtn;

private:
	vector<wstring> m_ScheduleRemoveErrors;

	void HTMLmainframeJobRemove(const Script& p_Script);
	void HTMLmainframeJobScheduleRemove(const ScriptSchedule& p_ScriptSchedule);
	void HTMLmainframeJobSchedulesUpdate();

	template<class WizardClass> 
	static void insertYtriaJobsIntoDB(ScriptParser& p_XMLparser, std::shared_ptr<const Sapio365Session>& p_Session, DlgDoubleProgressCommon& p_Progress);
	
	void									loadFromDB(vector<Script>& p_Scripts, std::shared_ptr<const Sapio365Session>& p_Session);
	std::shared_ptr<const Sapio365Session>	getSession() const;
	std::shared_ptr<const Sapio365Session>	m_SessionForInit;
	
	static const Script& getDebugScript(const wstring& p_ScriptKey);
};

// =================================================================================================

class AutomationWizardO365Folder : public AutomationWizardO365
{
public:
	AutomationWizardO365Folder(const wstring& p_FolderPath);
	virtual wstring GetName() const override;

private:
	void processPath(const wstring& i_Path);
};