#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardPosts : public AutomationWizardO365
{
public:
	AutomationWizardPosts();

	virtual wstring GetName() const override
	{
		return _YTEXT("Posts");
	}
};