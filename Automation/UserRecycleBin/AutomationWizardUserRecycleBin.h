#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardUserRecycleBin : public AutomationWizardO365
{
public:
	AutomationWizardUserRecycleBin();

	virtual wstring GetName() const override
	{
		return _YTEXT("UserRecycleBin");
	}
};