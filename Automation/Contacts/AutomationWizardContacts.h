#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardContacts : public AutomationWizardO365
{
public:
	AutomationWizardContacts();

	virtual wstring GetName() const override
	{
		return _YTEXT("Contacts");
	}
};