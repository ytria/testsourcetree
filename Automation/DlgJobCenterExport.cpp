#include "DlgJobCenterExport.h"

#include "FileUtil.h"
#include "JobCenterUtil.h"
#include "Registry.h"
#include "YFileDialog.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTranslate.h"


const wstring DlgJobCenterExport::g_RegKeyLastFolder		= _YTEXT("JobCenterExportLastFolder");
const wstring DlgJobCenterExport::g_RegKeyOpenAfterExport	= _YTEXT("JobCenterExportOpen");
const wstring DlgJobCenterExport::g_RegKeyExistingFile		= _YTEXT("JobCenterExportLastFolderExistingFile");
const wstring DlgJobCenterExport::g_CreateNew				= _YTEXT("CreateNew");
const wstring DlgJobCenterExport::g_Skip					= _YTEXT("Skip");
const wstring DlgJobCenterExport::g_Overwrite				= _YTEXT("Overwrite");

BEGIN_MESSAGE_MAP(DlgJobCenterExport, ResizableDialog)
    ON_BN_CLICKED(IDC_DLG_JC_EXPORT_SELECTFOLDER, OnSelectFolder)
END_MESSAGE_MAP()

DlgJobCenterExport::DlgJobCenterExport(CWnd* p_Parent) : ResizableDialog(IDD, p_Parent), 
	m_OpenAfterDownload(true),
	m_FileExistsAction(FileExistsAction::Append)// in this class, "Append" means create new file (name is incremented)
{	
}

const wstring& DlgJobCenterExport::GetFolderPath() const
{
    return m_FolderPath;
}

FileExistsAction DlgJobCenterExport::GetFileExistsAction() const
{
    return m_FileExistsAction;
}

bool DlgJobCenterExport::IsOpenAfterDownload() const
{
    return m_OpenAfterDownload;
}

void DlgJobCenterExport::DoDataExchange(CDataExchange* pDX)
{
    ResizableDialog::DoDataExchange(pDX);

    DDX_Control(pDX, IDC_DLG_JC_EXPORT_FOLDERPATH,		m_TxtFolderPath);
    DDX_Control(pDX, IDC_DLG_JC_EXPORT_SELECTFOLDER,	m_BtnChooseFolder);
    
    DDX_Control(pDX, IDC_DLG_JC_EXPORT_CREATENEW,		m_RadioBtnCreateNew);
    DDX_Control(pDX, IDC_DLG_JC_EXPORT_SKIP,			m_RadioBtnSkip);
    DDX_Control(pDX, IDC_DLG_JC_EXPORT_OVERWRITE,		m_RadioBtnOverwrite);

    DDX_Control(pDX, IDC_DLG_JC_EXPORT_BOX,				m_GroupBox);

    DDX_Control(pDX, IDC_DLG_JC_EXPORT_OPENFOLDER,		m_ChkOpenAfterDownload);
    DDX_Control(pDX, IDOK,								m_BtnOk);
    DDX_Control(pDX, IDCANCEL,							m_BtnCancel);
}

BOOL DlgJobCenterExport::OnInitDialogSpecific()
{
    SetWindowText(_T("Export Selected Job To Folder"));
    m_BtnChooseFolder.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_2, _YLOC("Select Destination")).c_str());
    AddAnchor(m_BtnChooseFolder, CSize(100, 0), CSize(100, 0));

	Registry::readString(HKEY_CURRENT_USER, JobCenterUtil::g_RegPathUserSettings, g_RegKeyLastFolder, m_FolderPath);
	if (m_FolderPath.empty())
		m_FolderPath = FileUtil::getMyDocuments();
	
	bool openAfterExport = false;
	wstring oaeStrVal;
	Registry::readString(HKEY_CURRENT_USER, JobCenterUtil::g_RegPathUserSettings, g_RegKeyOpenAfterExport, oaeStrVal);
	if (!oaeStrVal.empty())
		openAfterExport = true;

	wstring existingStrVal;
	Registry::readString(HKEY_CURRENT_USER, JobCenterUtil::g_RegPathUserSettings, g_RegKeyExistingFile, existingStrVal);

	m_TxtFolderPath.SetWindowText(GetFolderPath().c_str());
    m_TxtFolderPath.SetBkColor(RGB(255, 255, 255));
    AddAnchor(m_TxtFolderPath, CSize(0, 0), CSize(100, 0));

    m_RadioBtnCreateNew.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_5, _YLOC("Create New")).c_str());
    m_RadioBtnCreateNew.SetBkColor(RGB(255, 255, 255));
    AddAnchor(m_RadioBtnCreateNew, CSize(0, 0), CSize(33, 0));

    m_RadioBtnOverwrite.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_6, _YLOC("Overwrite")).c_str());
    m_RadioBtnOverwrite.SetBkColor(RGB(255, 255, 255));
    AddAnchor(m_RadioBtnOverwrite, CSize(33, 0), CSize(67, 0));

    m_RadioBtnSkip.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_7, _YLOC("Skip")).c_str());
    m_RadioBtnSkip.SetBkColor(RGB(255, 255, 255));
    AddAnchor(m_RadioBtnSkip, CSize(67, 0), CSize(100, 0));
    
    if (MFCUtil::StringMatchW(existingStrVal, g_Overwrite))
        m_RadioBtnOverwrite.SetCheck(BST_CHECKED);
    else if (MFCUtil::StringMatchW(existingStrVal, g_Skip))
        m_RadioBtnSkip.SetCheck(BST_CHECKED);
	else
		m_RadioBtnCreateNew.SetCheck(BST_CHECKED);

    m_ChkOpenAfterDownload.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_4, _YLOC("Open folder after download")).c_str());
    m_ChkOpenAfterDownload.SetCheck(openAfterExport ? BST_CHECKED : BST_UNCHECKED);
    m_ChkOpenAfterDownload.SetBkColor(RGB(255, 255, 255));

    m_GroupBox.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_8, _YLOC("If destination file already exists")).c_str());
    m_GroupBox.SetBkColor(RGB(255, 255, 255));
    AddAnchor(m_GroupBox, CSize(0, 0), CSize(100, 0));

    AddAnchor(m_BtnOk, CSize(100, 100), CSize(100, 100));
    AddAnchor(m_BtnCancel, CSize(100, 100), CSize(100, 100));

    SetBkColor(RGB(255, 255, 255));

    return TRUE;
}

void DlgJobCenterExport::OnOK()
{
    CString folderPath;
    m_TxtFolderPath.GetWindowText(folderPath);
	m_FolderPath = FileUtil::MakeValidFilePath(folderPath.GetBuffer(), _YTEXT(""));
	if (m_FolderPath.empty())
    {
        YCodeJockMessageBox dlg(this, DlgMessageBox::eIcon_Error, YtriaTranslate::Do(DlgDownloadDriveItems_OnOK_1, _YLOC("Error")).c_str(), YtriaTranslate::Do(DlgDownloadDriveItems_OnOK_2, _YLOC("Please choose a valid destination folder.")).c_str(), _YTEXT(""), { { IDOK, YtriaTranslate::Do(DlgDownloadDriveItems_OnOK_3, _YLOC("OK")).c_str() } });
        dlg.DoModal();
	}
	else
	{
		m_OpenAfterDownload		= m_ChkOpenAfterDownload.GetCheck() == BST_CHECKED;
		m_FileExistsAction		=	m_RadioBtnOverwrite.GetCheck() == BST_CHECKED	? FileExistsAction::Overwrite :
									m_RadioBtnSkip.GetCheck() == BST_CHECKED		? FileExistsAction::Skip :
									FileExistsAction::Append;//create new

		Registry::writeString(HKEY_CURRENT_USER, JobCenterUtil::g_RegPathUserSettings, g_RegKeyLastFolder, m_FolderPath);
		Registry::writeString(HKEY_CURRENT_USER, JobCenterUtil::g_RegPathUserSettings, g_RegKeyExistingFile,	m_FileExistsAction == FileExistsAction::Overwrite	? g_Overwrite :
																												m_FileExistsAction == FileExistsAction::Skip		? g_Skip: 
																												g_CreateNew);
		Registry::writeString(HKEY_CURRENT_USER, JobCenterUtil::g_RegPathUserSettings, g_RegKeyOpenAfterExport, m_OpenAfterDownload ? _YTEXT("X") : _YTEXT(""));

		ResizableDialog::OnOK();
	}
}

void DlgJobCenterExport::OnSelectFolder()
{
    YFileDialog folderChooser(GetFolderPath().c_str(), 0, this);
    if (folderChooser.DoModal() == IDOK)
    {
        const CString folderPath = folderChooser.GetPathName()/*GetFolderPath()*/; //Bug #190201.RL.0099CE
        m_TxtFolderPath.SetWindowText(folderPath);
    }
}