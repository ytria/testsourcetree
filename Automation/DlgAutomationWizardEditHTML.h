#pragma once

#include "DlgFormsHTML.h"
#include "JobCenterUtil.h"

class DlgAutomationWizardEditHTML : public DlgFormsHTML
{
public:
	DlgAutomationWizardEditHTML(Script& p_Script, CWnd* p_Parent);
	virtual ~DlgAutomationWizardEditHTML();

protected:
	virtual void generateJSONScriptData() override;

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

	virtual wstring getDialogTitle() const override;

private:
	Script& m_Script;

	static const wstring& g_VarNameTitle;
	static const wstring& g_VarNameDescription;
	static const wstring& g_VarNameTooltip;
	static const wstring& g_VarNameIcon;
};