#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardSitePermissions : public AutomationWizardO365
{
public:
    AutomationWizardSitePermissions();

    virtual wstring GetName() const override
    {
        return _YTEXT("SitePermissions");
    }
};
