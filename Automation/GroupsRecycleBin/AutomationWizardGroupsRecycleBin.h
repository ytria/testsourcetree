#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardGroupsRecycleBin : public AutomationWizardO365
{
public:
	AutomationWizardGroupsRecycleBin();

	virtual wstring GetName() const override
	{
		return _YTEXT("GroupsRecycleBin");
	}
};