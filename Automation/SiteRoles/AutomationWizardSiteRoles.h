#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardSiteRoles : public AutomationWizardO365
{
public:
    AutomationWizardSiteRoles();

    virtual wstring GetName() const override
    {
        return _YTEXT("SiteRoles");
    }
};