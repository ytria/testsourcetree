#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardListItems : public AutomationWizardO365
{
public:
	AutomationWizardListItems();

	virtual wstring GetName() const override
	{
		return _YTEXT("ListItems");
	}
};