#pragma once

#include "AutomationDataStructure.h"

//////////////////////////////// Actions
// prefixes
static const wstring g_ActionShowPrefix			= _YTEXT("Show");
static const wstring g_ActionSelectedPrefix		= _YTEXT("Selected");
static const wstring g_ActionShowMyDataPrefix	= g_ActionShowPrefix + _YTEXT("MyData");

static const wstring g_ActionNameMainframe = _YTEXT("MainFrame");

// front panel - main views
static const wstring g_ActionNameMyDataMail					= g_ActionShowMyDataPrefix + _YTEXT("Mail");
static const wstring g_ActionNameMyDataMailFolder			= g_ActionShowMyDataPrefix + _YTEXT("MailFolder");
static const wstring g_ActionNameMyDataCalendar				= g_ActionShowMyDataPrefix + _YTEXT("Calendar");
static const wstring g_ActionNameMyDataDrive				= g_ActionShowMyDataPrefix + _YTEXT("Drive");
static const wstring g_ActionNameMyDataContacts				= g_ActionShowMyDataPrefix + _YTEXT("Contacts");
static const wstring g_ActionNameMyMessageRules				= g_ActionShowMyDataPrefix + _YTEXT("MessageRules");

static const wstring g_ActionNameShowUsers					= g_ActionShowPrefix + _YTEXT("Users");
static const wstring g_ActionNameShowGroups					= g_ActionShowPrefix + _YTEXT("Groups");
static const wstring g_ActionNameShowSites					= g_ActionShowPrefix + _YTEXT("Sites");
static const wstring g_ActionNameShowSchools				= g_ActionShowPrefix + _YTEXT("Schools");
static const wstring g_ActionNameShowApplications           = g_ActionShowPrefix + _YTEXT("Applications");
static const wstring g_ActionNameShowDirectoryRoles			= g_ActionShowPrefix + _YTEXT("DirectoryRoles");
static const wstring g_ActionNameShowLicenses				= g_ActionShowPrefix + _YTEXT("Licenses");
static const wstring g_ActionNameShowUsersRecycleBin		= g_ActionShowPrefix + _YTEXT("UsersRecycleBin");
static const wstring g_ActionNameShowGroupsRecycleBin		= g_ActionShowPrefix + _YTEXT("GroupsRecycleBin");
static const wstring g_ActionNameShowUnassignedLicenses		= g_ActionShowPrefix + _YTEXT("UnassignedLicenses");
static const wstring g_ActionNameShowServicePlans			= g_ActionShowPrefix + _YTEXT("ServicePlans");
static const wstring g_ActionNameShowUsageReport			= g_ActionShowPrefix + _YTEXT("UsageReport");
static const wstring g_ActionNameShowSignIns				= g_ActionShowPrefix + _YTEXT("SignIns");
static const wstring g_ActionNameShowAuditLogs				= g_ActionShowPrefix + _YTEXT("AuditLogs");

static const wstring g_ActionNameSelectedShowDetailsGroups	= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("GroupDetails");
static const wstring g_ActionNameSelectedShowDetailsSites   = g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("SiteDetails");
static const wstring g_ActionNameSelectedShowDetailsUsers	= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("UserDetails");

// secondary views
static const wstring g_ActionNameSelectedShowClassMembers		= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("ClassMembers");
static const wstring g_ActionNameSelectedShowParentGroups		= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("ParentGroups");
static const wstring g_ActionNameSelectedShowDriveItemsUsers	= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("DriveItemsUsers");
static const wstring g_ActionNameSelectedShowDriveItemsGroups	= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("DriveItemsGroups");
static const wstring g_ActionNameSelectedShowDriveItemsSites	= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("DriveItemsSites");
static const wstring g_ActionNameSelectedShowDriveItemsChannels = g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("DriveItemsChannels");
static const wstring g_ActionNameSelectedShowMessages			= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("Messages");
static const wstring g_ActionNameSelectedShowMailboxPermissions = g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("MailboxPermissions");
static const wstring g_ActionNameSelectedShowContacts			= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("Contacts");
static const wstring g_ActionNameSelectedShowMailFolders		= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("MailFolders");
static const wstring g_ActionNameSelectedShowEventsUsers		= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("EventsUsers");
static const wstring g_ActionNameSelectedShowEventsGroups		= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("EventsGroups");
static const wstring g_ActionNameSelectedShowLicenses			= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("Licenses");
static const wstring g_ActionNameSelectedShowManagerHierarchy	= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("ManagerHierarchy");
static const wstring g_ActionNameSelectedShowChannels			= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("Channels");
static const wstring g_ActionNameSelectedShowChannelMembers		= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("ChannelMembers");
static const wstring g_ActionNameSelectedShowChannelSites       = g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("ChannelSites");
static const wstring g_ActionNameSelectedShowChannelsMessages   = g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("ChannelMessages");
static const wstring g_ActionNameSelectedShowMessageRules		= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("MessageRules");

static const wstring g_ActionNameSelectedShowMembers			= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("Members");
static const wstring g_ActionNameSelectedShowOwners				= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("Owners");
static const wstring g_ActionNameSelectedShowAuthors			= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("Authors");
static const wstring g_ActionNameSelectedShowConversations		= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("Conversations");
static const wstring g_ActionNameSelectedShowSites				= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("Sites");

static const wstring g_ActionNameSelectedShowLists				= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("Lists");

static const wstring g_ActionNameSelectedShowPost				= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("Posts");

static const wstring g_SetCurrentSessionToBeRestoredAfterScriptCompletion_DEPRECATED	= _YTEXT("SetCurrentSessionToBeRestoredAfterScriptCompletion"); // Remove enventually
static const wstring g_SetCurrentSessionToBeSetInMainFrameAfterScriptCompletion			= _YTEXT("SetCurrentSessionToBeSetInMainFrameAfterScriptCompletion");

// show columns
static const wstring g_ActionNameShowColumnsDefault = _YTEXT("ShowDefaultColumns");
static const wstring g_ActionNameShowColumnsAll		= _YTEXT("ShowAllColumns");

// more
static const wstring g_ActionNameSelectedUserLoadMore				= g_ActionSelectedPrefix + _YTEXT("UserLoadMore");
static const wstring g_ActionNameSelectedUserLoadMailboxInfo        = g_ActionSelectedPrefix + _YTEXT("UserLoadMailboxInfo");
static const wstring g_ActionNameSelectedGroupLoadMore				= g_ActionSelectedPrefix + _YTEXT("GroupLoadMore");
static const wstring g_ActionNameSelectedSiteLoadMore				= g_ActionSelectedPrefix + _YTEXT("SiteLoadMore");
static const wstring g_ActionNameSelectedChannelLoadMore			= g_ActionSelectedPrefix + _YTEXT("ChannelLoadMore");
static const wstring g_ActionNameSelectedViewPermissions			= g_ActionSelectedPrefix + _YTEXT("ViewPermissions");
static const wstring g_ActionNameSelectedDeletePermissions			= g_ActionSelectedPrefix + _YTEXT("DeletePermissions");
static const wstring g_ActionNameSelectedDownload					= g_ActionSelectedPrefix + _YTEXT("Download");
static const wstring g_ActionNameSelectedLoadCheckoutAndRetention	= g_ActionSelectedPrefix + _YTEXT("LoadCheckoutAndRetention");

// edition
static const wstring g_ActionNameSelectedView				= g_ActionSelectedPrefix + _YTEXT("View");
static const wstring g_ActionNameSelectedDelete				= g_ActionSelectedPrefix + _YTEXT("Delete");
static const wstring g_ActionNameSelectedRestore			= g_ActionSelectedPrefix + _YTEXT("Restore");
static const wstring g_ActionNameSelectedRemoveFrom			= g_ActionSelectedPrefix + _YTEXT("RemoveFrom");
static const wstring g_ActionNameSelectedAddTo				= g_ActionSelectedPrefix + _YTEXT("AddTo");
static const wstring g_ActionNameSelectedMoveTo				= g_ActionSelectedPrefix + _YTEXT("MoveTo");
static const wstring g_ActionNameSelectedRename				= g_ActionSelectedPrefix + _YTEXT("Rename");
static const wstring g_ActionNameSelectedCreateFolder		= _YTEXT("CreateFolder");
 
// group membership edit
static const wstring g_ActionNameGroupMemberAdd				= g_ActionSelectedPrefix + _YTEXT("GroupMemberAdd");
static const wstring g_ActionNameGroupMemberRemove			= g_ActionSelectedPrefix + _YTEXT("GroupMemberRemove");
static const wstring g_ActionNameGroupMemberCopy			= g_ActionSelectedPrefix + _YTEXT("GroupMemberCopy");
static const wstring g_ActionNameGroupMemberMove			= g_ActionSelectedPrefix + _YTEXT("GroupMemberMove");

static const wstring g_ActionNameGroupMembershipAdd			= g_ActionSelectedPrefix + _YTEXT("GroupMembershipAdd");
static const wstring g_ActionNameGroupMembershipRemove		= g_ActionSelectedPrefix + _YTEXT("GroupMembershipRemove");
static const wstring g_ActionNameGroupMembershipCopy		= g_ActionSelectedPrefix + _YTEXT("GroupMembershipCopy");
static const wstring g_ActionNameGroupMembershipTransfer	= g_ActionSelectedPrefix + _YTEXT("GroupMembershipTransfer");

static const wstring g_ActionNameGroupSenderAddAccepted		= g_ActionSelectedPrefix + _YTEXT("GroupSenderAddAccepted");
static const wstring g_ActionNameGroupSenderAddRejected		= g_ActionSelectedPrefix + _YTEXT("GroupSenderAddRejected");

static const wstring g_ActionNameGroupOwnerAdd				= g_ActionSelectedPrefix + _YTEXT("GroupOwnerAdd");
static const wstring g_ActionNameGroupOwnerRemove			= g_ActionSelectedPrefix + _YTEXT("GroupOwnerRemove");

static const wstring g_ParamNameLoadDirectory				= _YTEXT("LoadDirectory");

static const wstring g_ParamNameLocation					= _YTEXT("usageLocation");

static const wstring g_ParamNameGroupAction			= _YTEXT("GroupAction");
static const wstring g_ValueGroupActionCopyGroups	= _YTEXT("CopyGroups");
static const wstring g_ValueGroupActionMoveGroups	= _YTEXT("MoveGroups");
static const wstring g_ValueGroupActionCopyMembers	= _YTEXT("CopyMembers");
static const wstring g_ValueGroupActionMoveMembers	= _YTEXT("MoveMembers");
//

static const wstring g_ActionNameSelectedEditUser				= g_ActionSelectedPrefix + _YTEXT("EditUser");
static const wstring g_ActionNameSelectedEditGroup				= g_ActionSelectedPrefix + _YTEXT("EditGroup");
static const wstring g_ActionNameSelectedEditLicense			= g_ActionSelectedPrefix + _YTEXT("EditLicense");
static const wstring g_ActionNameSelectedEditLicenseLocation	= g_ActionSelectedPrefix + _YTEXT("EditLicenseUsageLocation");
static const wstring g_ActionNameSelectedEditClass				= g_ActionSelectedPrefix + _YTEXT("EditClass");
static const wstring g_ActionNameSelectedEditSchool				= g_ActionSelectedPrefix + _YTEXT("EditSchool");
static const wstring g_ActionNameSelectedEditEducationUser		= g_ActionSelectedPrefix + _YTEXT("EditEducationUser");

static const wstring g_ActionNameCreateUser		= _YTEXT("CreateUser");
static const wstring g_ActionNameCreateGroup	= _YTEXT("CreateGroup");

static const wstring g_ActionNameCreateClass			= _YTEXT("CreateClass");
static const wstring g_ActionNameCreateSchool			= _YTEXT("CreateSchool");
static const wstring g_ActionNameCreateEducationUser	= _YTEXT("CreateEducationUser");

// specific
static const wstring g_ActionNameSelectedShowMessagesDetails	= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("MessageDetails");

static const wstring g_ActionNameSelectedShowListItems		= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("ListItems");
static const wstring g_ActionNameSelectedShowListColumns	= g_ActionSelectedPrefix + g_ActionShowPrefix + _YTEXT("ListColumns");
static const wstring g_ActionNameShowSystemLists			= _YTEXT("ShowSystemLists");
static const wstring g_ActionNameHideSystemLists			= _YTEXT("HideSystemLists");

static const wstring g_ActionNameSelectedPreviewBody			= g_ActionSelectedPrefix + _YTEXT("PreviewBody");
static const wstring g_ActionNameSelectedDownloadAsEML			= g_ActionSelectedPrefix + _YTEXT("DownloadAsEML");
static const wstring g_ActionNameSelectedAttachmentLoadInfo		= g_ActionSelectedPrefix + _YTEXT("AttachmentLoadInfo");
static const wstring g_ActionNameSelectedAttachmentDownload		= g_ActionSelectedPrefix + _YTEXT("AttachmentDownload");
static const wstring g_ActionNameUpdateGroupSettings            = g_ActionSelectedPrefix + _YTEXT("UpdateGroupSettings");
static const wstring g_ActionNameSelectedPreviewItem			= g_ActionSelectedPrefix + _YTEXT("PreviewItem");
static const wstring g_ActionNameSelectedAttachmentDelete		= g_ActionSelectedPrefix + _YTEXT("AttachmentDelete");
static const wstring g_ActionNameUpdateMailboxPermissions		= g_ActionSelectedPrefix + _YTEXT("MailboxPermissions");
static const wstring g_ActionNameShowOnPremUsers				= g_ActionSelectedPrefix + _YTEXT("OnPremUsers");
static const wstring g_ActionNameShowOnPremGroups				= g_ActionSelectedPrefix + _YTEXT("OnPremGroups");
static const wstring g_ActionNameSetNestedGroup					= _YTEXT("SetNestedGroup");

// other
static const wstring g_ActionNameRefreshAll			= _YTEXT("RefreshAll");
static const wstring g_ActionNameRefreshSelected	= _YTEXT("RefreshSelected");
static const wstring g_ActionNameLoadSession		= _YTEXT("LoadSession");

static const wstring g_ActionNameUpdateTenantSettings	= _YTEXT("UpdateTenantSettings");// TODO! not sure it is a relevant automation process
static const wstring g_ActionNamePolicyCreate			= _YTEXT("PolicyCreate");// TODO! not sure it is a relevant automation process
static const wstring g_ActionNamePolicyDelete			= _YTEXT("PolicyDelete");// TODO! not sure it is a relevant automation process
static const wstring g_ActionNamePolicyUpdate			= _YTEXT("PolicyUpdate");// TODO! not sure it is a relevant automation process

static const wstring g_ActionNameSelectedResetPassword	= g_ActionSelectedPrefix + _YTEXT("ResetPassword");
static const wstring g_ParamNamePassword				= _YTEXT("password");
static const wstring g_ParamNameAutoGenerate			= _YTEXT("AutoGenerate");
static const wstring g_ParamNameForceChange				= _YTEXT("forceChangePasswordNextSignIn"); // This one must stay alphabetically before the following one (see DlgUserPasswordHTML::processPostedDataSpecific)
static const wstring g_ParamNameForceChangeMFA			= _YTEXT("forceChangePasswordNextSignInWithMfa");
static const wstring g_ParamNameCreatePassword          = _YTEXT("CreatePassword");

static const wstring g_ActionNameSelectedRevokeUserAccess = g_ActionSelectedPrefix + _YTEXT("RevokeUserAccess");

static const wstring g_ParamNameRole = _YTEXT("Role");

static const wstring g_ParamValueRequiredLogin				= _YTEXT("RequiredLogin");
static const wstring g_ParamValueWarningIfAdmin				= _YTEXT("WarningIfAdmin");
static const wstring g_ParamValueWarningIfAdminTech			= _YTEXT("WarningIfAdminTech");
static const wstring g_ParamValueWarningIfUltraAdminTech	= _YTEXT("WarningIfUltraAdminTech");
static const wstring g_RequirementsDelimitor				= _YTEXT(";");

/////////////////////////////// Params
static const wstring g_ParamNameParent = _YTEXT("Parent");

// download attachments & drive items
static const wstring g_OwnerName	= _YTEXT("OwnerName");
static const wstring g_Hierarchy	= _YTEXT("Hierarchy");
static const wstring g_IfFileExists = _YTEXT("IfFileExists");
static const wstring g_CreateNew	= _YTEXT("CreateNew");
static const wstring g_Overwrite	= _YTEXT("Overwrite");
static const wstring g_Skip			= _YTEXT("Skip");
static const wstring g_OpenFolderAfterDownload = _YTEXT("OpenFolderAfterDownload");

// HistoryMode
static const wstring g_NewFrame			= _YTEXT("NewFrame");// true or false, defaults to false
static const wstring g_AutoCloseFrame	= _YTEXT("AutoClose");// true or false, defaults to false

static const wstring g_ColumnTransport = _YTEXT("ColumnTransport");// list of column IDs

// system lists
static const wstring g_O365Tenant_AllDomains		= _YTEXT("O365Tenant_AllDomains");
static const wstring g_O365Tenant_DomainName		= _YTEXT("O365Tenant_DomainName");

static const wstring g_O365_List_Sessions_UltraAdmin		= _YTEXT("O365Sessions_UltraAdmin");
static const wstring g_O365_List_Sessions_Advanced			= _YTEXT("O365Sessions_Advanced");
static const wstring g_O365_List_Sessions_Elevated			= _YTEXT("O365Sessions_Elevated");
static const wstring g_O365_List_Sessions_Role				= _YTEXT("O365Sessions_Role");
static const wstring g_O365_List_Sessions_Standard			= _YTEXT("O365Sessions_Standard");
static const wstring g_O365_List_Sessions_PartnerAdvanced	= _YTEXT("O365Sessions_PartnerAdvanced");
static const wstring g_O365_List_Sessions_PartnerElevated	= _YTEXT("O365Sessions_PartnerElevated");
static const wstring g_O365_ListItemSessionName				= _YTEXT("ListItemSessionName");
static const wstring g_O365_ListItemSessionLabel			= _YTEXT("ListItemSessionLabel");
static const wstring g_O365_ListItemSessionRoleSeparator	= _YTEXT("|~#~|");

// system variables
static const wstring g_O365Tenant_DefaultDomainName = AutomationConstant::val().m_VariablePrefix + _YTEXT("O365Tenant_DefaultDomainName") + AutomationConstant::val().m_VariableSuffix;
static const wstring g_O365Tenant_InitialDomainName	= AutomationConstant::val().m_VariablePrefix + _YTEXT("O365Tenant_InitialDomainName") + AutomationConstant::val().m_VariableSuffix;

static const wstring g_O365LastLoadingError = AutomationConstant::val().m_VariablePrefix + _YTEXT("LastLoadingError") + AutomationConstant::val().m_VariableSuffix;

static const wstring g_O365UserPrincipalName    = AutomationConstant::val().m_VariablePrefix + _YTEXT("PrincipalUserName") + AutomationConstant::val().m_VariableSuffix;
static const wstring g_O365UserPrincipalEmail   = AutomationConstant::val().m_VariablePrefix + _YTEXT("PrincipalUserEmail") + AutomationConstant::val().m_VariableSuffix;
static const wstring g_O365UserPrincipalType    = AutomationConstant::val().m_VariablePrefix + _YTEXT("PrincipalUserType") + AutomationConstant::val().m_VariableSuffix;
static const wstring g_O365UserDisplayedName    = AutomationConstant::val().m_VariablePrefix + _YTEXT("DisplayedUserName") + AutomationConstant::val().m_VariableSuffix;

// Usage Reports
static const wstring g_ActionNameSelectReport	= _YTEXT("SelectReport"); // Only internal! (See TranslateActionSpecific)
static const wstring g_ParamNamePeriodOrDate	= _YTEXT("PeriodOrDate"); // D7, D30, D90, D180 or iso8601
static const wstring g_ParamNameReportFile		= _YTEXT("ReportFile"); // filename if report should be saved or if opening an existing report
static const wstring g_ParamNameAdditionalInfo	= _YTEXT("AdditionalInfo"); // filename if report should be saved or if opening an existing report

// Moldule Options (Messages, Mail Folders, Events)
static const wstring g_ActionNameModuleOptions	= _YTEXT("ModuleOptions");
static const wstring g_ParamNameCutOff			= _YTEXT("CutOff");

#define IsActionMainFrame(action)						IsAutomationAction(g_ActionNameMainframe, action)
// top actions - from main frame
#define IsActionShowMyDataMail(action)					IsAutomationAction(g_ActionNameMyDataMail, action)
#define IsActionShowMyDataMailFolder(action)			IsAutomationAction(g_ActionNameMyDataMailFolder, action)
#define IsActionShowMyDataCalendar(action)				IsAutomationAction(g_ActionNameMyDataCalendar, action)
#define IsActionShowMyDataDrive(action)					IsAutomationAction(g_ActionNameMyDataDrive, action)
#define IsActionShowMyDataContact(action)				IsAutomationAction(g_ActionNameMyDataContacts, action)
#define IsActionShowMyDataMesssageRules(action)			IsAutomationAction(g_ActionNameMyMessageRules, action)

#define IsActionShowUsers(action)						IsAutomationAction(g_ActionNameShowUsers, action)
#define IsActionShowGroups(action)						IsAutomationAction(g_ActionNameShowGroups, action)
#define IsActionShowSites(action)						IsAutomationAction(g_ActionNameShowSites, action)
#define IsActionShowSchools(action)						IsAutomationAction(g_ActionNameShowSchools, action)
#define IsActionShowDirectoryRoles(action)				IsAutomationAction(g_ActionNameShowDirectoryRoles, action)
#define IsActionShowLicences(action)					IsAutomationAction(g_ActionNameShowLicenses, action)
#define IsActionShowOrgContacts(action)					IsAutomationAction(g_ActionNameShowOrgContacts, action)
#define IsActionShowUsageReport(action)					IsAutomationAction(g_ActionNameShowUsageReport, action)
#define IsActionShowSignIns(action)						IsAutomationAction(g_ActionNameShowSignIns, action)
#define IsActionShowAuditLogs(action)					IsAutomationAction(g_ActionNameShowAuditLogs, action)
#define IsActionShowUsersRecycleBin(action)				IsAutomationAction(g_ActionNameShowUsersRecycleBin, action)
#define IsActionShowGroupsRecycleBin(action)			IsAutomationAction(g_ActionNameShowGroupsRecycleBin, action)

// sub actions - from opened frames
#define IsActionSelectedShowParentGroups(action)		IsAutomationAction(g_ActionNameSelectedShowParentGroups, action)
#define IsActionSelectedShowDriveItemsUsers(action)		IsAutomationAction(g_ActionNameSelectedShowDriveItemsUsers, action)
#define IsActionSelectedShowMessages(action)			IsAutomationAction(g_ActionNameSelectedShowMessages, action)
#define IsActionSelectedShowContacts(action)			IsAutomationAction(g_ActionNameSelectedShowContacts, action)
#define IsActionSelectedShowMailFolders(action)			IsAutomationAction(g_ActionNameSelectedShowMailFolders, action)
#define IsActionSelectedShowEventsUsers(action)			IsAutomationAction(g_ActionNameSelectedShowEventsUsers, action)
#define IsActionSelectedShowManagerHierarchy(action)	IsAutomationAction(g_ActionNameSelectedShowManagerHierarchy, action)
#define IsActionSelectedShowChannels(action)			IsAutomationAction(g_ActionNameSelectedShowChannels, action)
#define IsActionSelectedShowMessageRules(action)		IsAutomationAction(g_ActionNameSelectedShowMessageRules, action)

#define IsActionSelectedShowMembers(action)				IsAutomationAction(g_ActionNameSelectedShowMembers, action)
#define IsActionSelectedShowOwners(action)				IsAutomationAction(g_ActionNameSelectedShowOwners, action)
#define IsActionSelectedShowAuthors(action)				IsAutomationAction(g_ActionNameSelectedShowAuthors, action)
#define IsActionSelectedShowConversations(action)		IsAutomationAction(g_ActionNameSelectedShowConversations, action)
#define IsActionSelectedShowSites(action)				IsAutomationAction(g_ActionNameSelectedShowSites, action)
#define IsActionSelectedShowDriveItemsGroups(action)	IsAutomationAction(g_ActionNameSelectedShowDriveItemsGroups, action)
#define IsActionSelectedShowEventsGroups(action)		IsAutomationAction(g_ActionNameSelectedShowEventsGroups, action)
#define IsActionSelectedShowLicenses(action)			IsAutomationAction(g_ActionNameSelectedShowLicenses, action)

#define IsActionSelectedShowDetailsGroup(action)		IsAutomationAction(g_ActionNameSelectedShowDetailsGroups, action)
#define IsActionSelectedShowDetailsUser(action)			IsAutomationAction(g_ActionNameSelectedShowDetailsUsers, action)

#define IsActionSelectedShowLists(action)				IsAutomationAction(g_ActionNameSelectedShowLists, action)
#define IsActionSelectedShowDriveItemsSites(action)		IsAutomationAction(g_ActionNameSelectedShowDriveItemsSites, action)

#define IsActionSelectedShowChannelMessages(action)     IsAutomationAction(g_ActionNameSelectedShowChannelsMessages, action)
#define IsActionSelectedShowChannelMembers(action)		IsAutomationAction(g_ActionNameSelectedShowChannelMembers, action)
#define IsActionSelectedShowChannelSites(action)		IsAutomationAction(g_ActionNameSelectedShowChannelSites, action)

// otre choz
#define IsActionSelectedUserLoadMore(action)			IsAutomationAction(g_ActionNameSelectedUserLoadMore, action)
#define IsActionSelectedUserLoadMailboxInfo(action)		IsAutomationAction(g_ActionNameSelectedUserLoadMailboxInfo, action)
#define IsActionSelectedGroupLoadMore(action)			IsAutomationAction(g_ActionNameSelectedGroupLoadMore, action)
#define IsActionSelectedSiteLoadMore(action)			IsAutomationAction(g_ActionNameSelectedSiteLoadMore, action)
#define IsActionSelectedChannelLoadMore(action)			IsAutomationAction(g_ActionNameSelectedChannelLoadMore, action)
#define IsActionSelectedLoadPermissions(action)			IsAutomationAction(g_ActionNameSelectedViewPermissions, action)
#define IsActionSelectedDeletePermissions(action)		IsAutomationAction(g_ActionNameSelectedDeletePermissions, action)
#define IsActionSelectedDownload(action)				IsAutomationAction(g_ActionNameSelectedDownload, action)

#define IsActionSelectedView(action)					IsAutomationAction(g_ActionNameSelectedView, action)
#define IsActionSelectedDelete(action)					IsAutomationAction(g_ActionNameSelectedDelete, action)
#define IsActionSelectedRestore(action)					IsAutomationAction(g_ActionNameSelectedRestore, action)
#define IsActionSelectedRemoveFrom(action)				IsAutomationAction(g_ActionNameSelectedRemoveFrom, action)
#define IsActionSelectedAddTo(action)					IsAutomationAction(g_ActionNameSelectedAddTo, action)
#define IsActionSelectedMoveTo(action)					IsAutomationAction(g_ActionNameSelectedMoveTo, action)

#define IsActionSelectedEditUser(action)				IsAutomationAction(g_ActionNameSelectedEditUser, action)
#define IsActionSelectedEditGroup(action)				IsAutomationAction(g_ActionNameSelectedEditGroup, action)
#define IsActionSelectedEditLicense(action)				IsAutomationAction(g_ActionNameSelectedEditLicense, action)
#define IsActionSelectedEditLicenseLocation(action)		IsAutomationAction(g_ActionNameSelectedEditLicenseLocation, action)

#define IsActionCreateUser(action)						IsAutomationAction(g_ActionNameCreateUser, action)
#define IsActionCreateGroup(action)						IsAutomationAction(g_ActionNameCreateGroup, action)

#define IsActionRefreshAll(action)						IsAutomationAction(g_ActionNameRefreshAll, action)
#define IsActionRefreshSelected(action)					IsAutomationAction(g_ActionNameRefreshSelected, action)

#define IsActionShowMessageDetails(action)				IsAutomationAction(g_ActionNameSelectedShowMessagesDetails, action)

#define IsActionSelectedShowListItems(action)			IsAutomationAction(g_ActionNameSelectedShowListItems, action)
#define IsActionSelectedShowListColumns(action)			IsAutomationAction(g_ActionNameSelectedShowListColumns, action)
#define IsActionShowSystemLists(action)					IsAutomationAction(g_ActionNameShowSystemLists, action)
#define IsActionHideSystemLists(action)					IsAutomationAction(g_ActionNameHideSystemLists, action)

#define IsActionLoadSession(action)						IsAutomationAction(g_ActionNameLoadSession, action)

#define IsActionShowColumnsDefault(action)				IsAutomationAction(g_ActionNameShowColumnsDefault, action)
#define IsActionShowColumnsAll(action)					IsAutomationAction(g_ActionNameShowColumnsAll, action)

#define IsActionSelectedPreviewBody(action)				IsAutomationAction(g_ActionNameSelectedPreviewBody, action)
#define IsActionSelectedDownloadAsEML(action)			IsAutomationAction(g_ActionNameSelectedDownloadAsEML, action)
#define IsActionSelectedPreviewItem(action)				IsAutomationAction(g_ActionNameSelectedPreviewItem, action)
#define IsActionSelectedAttachmentLoadInfo(action)		IsAutomationAction(g_ActionNameSelectedAttachmentLoadInfo, action)
#define IsActionSelectedAttachmentDownload(action)		IsAutomationAction(g_ActionNameSelectedAttachmentDownload, action)
#define IsActionSelectedAttachmentDelete(action)		IsAutomationAction(g_ActionNameSelectedAttachmentDelete, action)

#define IsActionSelectedShowPosts(action)				IsAutomationAction(g_ActionNameSelectedShowPost, action)

#define IsActionGroupMembersRemove(action)				IsAutomationAction(g_ActionNameGroupMemberRemove, action)
#define IsActionGroupMembersAdd(action)					IsAutomationAction(g_ActionNameGroupMemberAdd, action)
#define IsActionGroupMembersCopy(action)				IsAutomationAction(g_ActionNameGroupMemberCopy, action)
#define IsActionGroupMembersMove(action)				IsAutomationAction(g_ActionNameGroupMemberMove, action)

#define IsActionGroupMembershipRemove(action)			IsAutomationAction(g_ActionNameGroupMembershipRemove, action)
#define IsActionGroupMembershipAdd(action)				IsAutomationAction(g_ActionNameGroupMembershipAdd, action)
#define IsActionGroupMembershipCopy(action)				IsAutomationAction(g_ActionNameGroupMembershipCopy, action)
#define IsActionGroupMembershipTransfer(action)			IsAutomationAction(g_ActionNameGroupMembershipTransfer, action)

#define IsActionGroupSendersAddAccepted(action)			IsAutomationAction(g_ActionNameGroupSenderAddAccepted, action)
#define IsActionGroupSendersAddRejected(action)			IsAutomationAction(g_ActionNameGroupSenderAddRejected, action)

#define IsActionOwnerAdd(action)						IsAutomationAction(g_ActionNameGroupOwnerAdd, action)
#define IsActionOwnerRemove(action)						IsAutomationAction(g_ActionNameGroupOwnerRemove, action)

#define IsActionShowUnassignedLicenses(action)			IsAutomationAction(g_ActionNameShowUnassignedLicenses, action)
#define IsActionShowServicePlans(action)				IsAutomationAction(g_ActionNameShowServicePlans, action)

#define IsActionSelectedResetPassword(action)			IsAutomationAction(g_ActionNameSelectedResetPassword, action)

#define IsActionSelectedShowClassMembers(action)		IsAutomationAction(g_ActionNameSelectedShowClassMembers, action)

#define IsSetCurrentSessionToBeSetInMainFrameAfterScriptCompletion(action) IsAutomationAction(g_SetCurrentSessionToBeSetInMainFrameAfterScriptCompletion, action)

#define IsActionSelectedRevokeUserAccess(action) IsAutomationAction(g_ActionNameSelectedRevokeUserAccess, action)

#define IsActionSelectedLoadCheckoutAndRetention(action) IsAutomationAction(g_ActionNameSelectedLoadCheckoutAndRetention, action)

#define IsActionSetNestedGroup(action) IsAutomationAction(g_ActionNameSetNestedGroup, action)
