#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardMainFrame : public AutomationWizardO365
{
public:
	AutomationWizardMainFrame();

	virtual wstring GetName() const override
	{
		return JobCenterUtil::g_WizardNameMainFrame;
	}
};