#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardCommon : public AutomationWizardO365
{
public:
	AutomationWizardCommon();

	virtual wstring GetName() const override
	{
		return JobCenterUtil::g_WizardNameCommon;
	}
};