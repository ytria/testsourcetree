#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardSignIns : public AutomationWizardO365
{
public:
	AutomationWizardSignIns();
	wstring GetName() const override
	{
		return _YTEXT("SignIns");
	}
};

