#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardEduUsers : public AutomationWizardO365
{
public:
	AutomationWizardEduUsers();

	wstring GetName() const override
	{
		return _YTEXT("Education Users");
	}
};

