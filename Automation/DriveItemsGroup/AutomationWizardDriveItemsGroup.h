#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardDriveItemsGroup : public AutomationWizardO365
{
public:
	AutomationWizardDriveItemsGroup();

	virtual wstring GetName() const override
	{
		return _YTEXT("DriveItemsGroup");
	}
};