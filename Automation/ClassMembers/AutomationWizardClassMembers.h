#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardClassMembers : public AutomationWizardO365
{
public:
	AutomationWizardClassMembers();

	wstring GetName() const override
	{
		return _YTEXT("ClassMembers");
	}
};

