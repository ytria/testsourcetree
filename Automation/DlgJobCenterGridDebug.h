#pragma once

#include "JobCenterGridDebug.h"
#include "Resources\SharedResource.h" //_DEFINE_INCLUDES_RESOURCE_H_
#include "ResizableDialog.h"
#include "..\..\Resource.h"

class DlgJobCenterGridDebug : public ResizableDialog
{

public:
	DlgJobCenterGridDebug(CWnd* pParent);
	virtual ~DlgJobCenterGridDebug();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialogSpecificResizable() override;

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	enum { IDD = IDD_DLG_JOBCENTER_DEBUG };

	static TCHAR s_savedPositionKey[];

	CExtLabel	m_LabelGridHeader;
	CExtLabel	m_LabelGridArea;
	CExtButton	m_BtnOk;

	JobCenterGridDebug m_Grid;
};