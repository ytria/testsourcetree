#include "AutomationWizardO365.h"

class AutomationWizardSpGroups : public AutomationWizardO365
{
public:
    AutomationWizardSpGroups();

    virtual wstring GetName() const override
    {
        return _YTEXT("SpGroups");
    }
};
