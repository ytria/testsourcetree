#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardTeamChannelMessages : public AutomationWizardO365
{
public:
	AutomationWizardTeamChannelMessages();

	virtual wstring GetName() const override
	{
		return _YTEXT("TeamChannelMessages");
	}
};