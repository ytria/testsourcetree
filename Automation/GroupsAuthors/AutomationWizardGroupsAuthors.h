#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardGroupsAuthors : public AutomationWizardO365
{
public:
	AutomationWizardGroupsAuthors();

	virtual wstring GetName() const override
	{
		return _YTEXT("GroupAuthors");
	}
};