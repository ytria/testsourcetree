#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardChannels : public AutomationWizardO365
{
public:
	AutomationWizardChannels();

	virtual wstring GetName() const override
	{
		return _YTEXT("Channels");
	}
};