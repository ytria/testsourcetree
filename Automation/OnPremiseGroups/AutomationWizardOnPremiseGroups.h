#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardOnPremiseGroups : public AutomationWizardO365
{
public:
	AutomationWizardOnPremiseGroups();

	wstring GetName() const override;
};

