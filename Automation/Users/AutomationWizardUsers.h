#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardUsers : public AutomationWizardO365
{
public:
	AutomationWizardUsers();

	virtual wstring GetName() const override
	{
		return _YTEXT("Users");
	}
};