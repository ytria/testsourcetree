#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardSpUsers : public AutomationWizardO365
{
public:
    AutomationWizardSpUsers();

    wstring GetName() const override
    {
        return _YTEXT("SpUsers");
    }
};

