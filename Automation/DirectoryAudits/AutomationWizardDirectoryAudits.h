#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardDirectoryAudits : public AutomationWizardO365
{
public:
	AutomationWizardDirectoryAudits();
	wstring GetName() const override
	{
		return _YTEXT("DirectoryAudits");
	}
};

