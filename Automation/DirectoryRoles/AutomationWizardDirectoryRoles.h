#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardDirectoryRoles : public AutomationWizardO365
{
public:
	AutomationWizardDirectoryRoles();

	virtual wstring GetName() const override
	{
		return _YTEXT("DirectoryRoles");
	}
};