#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardSchools : public AutomationWizardO365
{
public:
	AutomationWizardSchools();

	wstring GetName() const override
	{
		return _YTEXT("Schools");
	}
};

