#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardDriveItemsUser : public AutomationWizardO365
{
public:
	AutomationWizardDriveItemsUser();

	virtual wstring GetName() const override
	{
		return _YTEXT("DriveItemsUser");
	}
};