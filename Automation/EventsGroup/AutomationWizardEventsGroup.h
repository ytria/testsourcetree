#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardEventsGroup : public AutomationWizardO365
{
public:
	AutomationWizardEventsGroup();

	virtual wstring GetName() const override
	{
		return _YTEXT("EventsGroup");
	}
};