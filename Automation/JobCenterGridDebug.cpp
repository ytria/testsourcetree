#include "JobCenterGridDebug.h"

#include "JobCenterManager.h"
#include "MainFrame.h"

JobCenterGridDebug::JobCenterGridDebug():CacheGrid(0, GridBackendUtil::CREATESORTING|GridBackendUtil::CREATEGROUPAREA|GridBackendUtil::CREATECOLORG)
{
}

void JobCenterGridDebug::customizeGrid()
{
	CWaitCursor _;

	SetAutomationName(_YTEXT("JobCenterGridDebug"));

	UINT columnSize1 = HIDPI_XW(8);
	m_ColModuleName				= AddColumn(_YUID("Name"), YtriaTranslate::Do(JobCenterGridDebug_customizeGrid_1, _YLOC("Module Name")).c_str(), GridBackendUtil::g_DummyString);
	m_ColPosition				= AddColumnNumber(_YUID("Position"), YtriaTranslate::Do(JobCenterGrid_customizeGrid_1, _YLOC("Position")).c_str(), GridBackendUtil::g_DummyString, columnSize1 * 3);
	m_ColModuleJobCount			= AddColumnNumber(_YUID("Count"), YtriaTranslate::Do(JobCenterGridDebug_customizeGrid_5, _YLOC("Module Job Count")).c_str(), GridBackendUtil::g_DummyString, columnSize1 * 3);
	m_ColIconNameFontAwesome	= AddColumn(_YUID("ScriptIcon"), _YTEXT("Icon full name"), GridBackendUtil::g_DummyString);
	m_ColTitle					= AddColumn(_YUID("ScriptTitle"), YtriaTranslate::Do(JobCenterGrid_customizeGrid_3, _YLOC("Title")).c_str(), GridBackendUtil::g_DummyString, columnSize1 * 30);
	m_ColDesc					= AddColumn(_YUID("ScriptDesc"), YtriaTranslate::Do(JobCenterGrid_customizeGrid_4, _YLOC("Description")).c_str(), GridBackendUtil::g_DummyString, columnSize1 * 40);
	m_ColTooltip				= AddColumn(_YUID("ScriptTooltip"), YtriaTranslate::Do(JobCenterGrid_customizeGrid_5, _YLOC("Tooltip")).c_str(), GridBackendUtil::g_DummyString, columnSize1 * 60);
	m_ColKey					= AddColumn(_YUID("ScriptKey"), YtriaTranslate::Do(JobCenterGrid_customizeGrid_6, _YLOC("Key")).c_str(), GridBackendUtil::g_DummyString);
	m_ColSourceFilePath			= AddColumn(_YUID("SourceFilePath"), _T("Source File Path"), GridBackendUtil::g_DummyString);;
	m_ColXML					= AddColumn(_YUID("ScriptXML"), _YTEXT("XML"), GridBackendUtil::g_DummyString);
	m_ColCommonRestrictions		= AddColumn(_YUID("CommonRestrictions"), _YTEXT("Common Restrictions"), GridBackendUtil::g_DummyString);

	m_ColShared		= AddColumnIcon(_YUID("Shared"), _T("Shared"), GridBackendUtil::g_DummyString);
	m_ColDisplayed	= AddColumnIcon(_YUID("Displayed"), YtriaTranslate::Do(JobCenterGrid_customizeGrid_2, _YLOC("Displayed")).c_str(), GridBackendUtil::g_DummyString);
	m_ColSystem		= AddColumnIcon(_YUID("System"), _T("System"), GridBackendUtil::g_DummyString);

	m_ColDateLastUpdate			= AddColumnDate(_YUID("LastUpdate"), _T("Last Updated On"), GridBackendUtil::g_DummyString);;
	m_ColDateLastUpdateBy		= AddColumn(_YUID("LastUpdateBy"), _T("Last Updated By"), GridBackendUtil::g_DummyString);
	m_ColDateLastUpdateByEmail	= AddColumn(_YUID("LastUpdateByEmail"), _T("Last Updated By (Email)"), GridBackendUtil::g_DummyString);
	m_ColDateLastShared			= AddColumnDate(_YUID("LastShare"), _T("Last Shared On"), GridBackendUtil::g_DummyString);

	m_ColTitle->SetAutoResizeWithGridResize(true);
	m_ColDesc->SetAutoResizeWithGridResize(true);
	m_ColTooltip->SetAutoResizeWithGridResize(true);
	m_ColKey->SetAutoResizeWithGridResize(true);
	m_ColXML->SetAutoResizeWithGridResize(true);
	m_ColCommonRestrictions->SetAutoResizeWithGridResize(true);

	AddColumnForRowPK(m_ColModuleName);
	AddColumnForRowPK(m_ColPosition);
	AddGroup(m_ColModuleName);
	AddSorting(m_ColPosition, GridBackendUtil::ASC);

	ICON_SYSTEM = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_JOBCENTER_SYSTEM));
	ASSERT(GetImageCount() == ICON_SYSTEM + 1);

	ICON_DISPLAYED = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_JOBCENTER_DISPLAYED));
	ASSERT(GetImageCount() == ICON_DISPLAYED + 1);

	ICON_SHARED = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_JOBCENTER_SHARED));
	ASSERT(GetImageCount() == ICON_SHARED + 1);

	loadJobCenters();
}

void JobCenterGridDebug::loadJobCenters()
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
	{
		map<wstring, vector<Script>> jobsByModule;
		{
			vector<Script> jobs;
			JobCenterManager::GetInstance().LoadAllScripts(mainFrame->GetSapio365Session(), jobs);
			for (const auto& job : jobs)
				jobsByModule[job.m_Module].push_back(job);
		}

		for (const auto& p : jobsByModule)
			for (const auto& job : p.second)
			{
				auto jobRow = AddRow();
				jobRow->AddField(p.first, m_ColModuleName);
				jobRow->AddField(p.second.size(), m_ColModuleJobCount);
				jobRow->AddField(job.m_Position + 1, m_ColPosition);
				jobRow->AddField(job.m_Key, m_ColKey);
				jobRow->AddField(job.m_Title, m_ColTitle);
				if (!job.m_Description.empty())
					jobRow->AddField(job.m_Description, m_ColDesc);
				if (!job.m_Tooltip.empty())
					jobRow->AddField(job.m_Tooltip, m_ColTooltip);
				jobRow->AddField(job.m_IconFontAwesome, m_ColIconNameFontAwesome);
				jobRow->AddField(job.m_ScriptContent, m_ColXML);
				jobRow->AddField(job.m_CommonRestrictions, m_ColCommonRestrictions);
				jobRow->AddField(job.IsShared() ?		_T("Shared") : _YTEXT(""),		m_ColShared,	job.IsShared()		? ICON_SHARED : NO_ICON);
				jobRow->AddField(job.IsDisplayed() ?	_T("Displayed") : _YTEXT(""),	m_ColDisplayed, job.IsDisplayed()	? ICON_DISPLAYED: NO_ICON);
				jobRow->AddField(job.IsSystem() ?		_T("System") : _YTEXT(""),		m_ColSystem,	job.IsSystem()		? ICON_SYSTEM : NO_ICON);
				if (job.m_SourceXMLfilePath.empty())
					jobRow->AddField(job.m_SourceXMLfilePath, m_ColSourceFilePath);
				if (!job.m_UpdDate.IsEmpty())
					jobRow->AddField(job.m_UpdDate, m_ColDateLastUpdate);
				if (!job.m_DateShared.IsEmpty())
					jobRow->AddField(job.m_DateShared, m_ColDateLastShared);
				if (!job.m_UpdateByName.empty())
					jobRow->AddField(job.m_UpdateByName, m_ColDateLastUpdateBy);
				if (!job.m_UpdateByPrincipalName.empty())
					jobRow->AddField(job.m_UpdateByPrincipalName, m_ColDateLastUpdateByEmail);
			}
	}

	UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
}