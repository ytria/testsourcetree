#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardMailboxPermissions : public AutomationWizardO365
{
public:
	AutomationWizardMailboxPermissions();

	wstring GetName() const override;
};

