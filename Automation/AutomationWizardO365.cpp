#include "AutomationWizardO365.h"

#include "JSONUtil.h"

#include "AutomationNames.h"
#include "AutomationWizardChannels.h"
#include "AutomationWizardColumns.h"
#include "AutomationWizardCommon.h"
#include "AutomationWizardContacts.h"
#include "AutomationWizardConversations.h"
#include "AutomationWizardDirectoryAudits.h"
#include "AutomationWizardDirectoryRoles.h"
#include "AutomationWizardDriveItemsGroup.h"
#include "AutomationWizardDriveItemsSite.h"
#include "AutomationWizardDriveItemsUser.h"
#include "AutomationWizardEventsGroup.h"
#include "AutomationWizardEventsUser.h"
#include "AutomationWizardGroups.h"
#include "AutomationWizardGroupsAuthors.h"
#include "AutomationWizardGroupsMembers.h"
#include "AutomationWizardGroupsOwners.h"
#include "AutomationWizardGroupsRecycleBin.h"
#include "AutomationWizardLicenses.h"
#include "AutomationWizardLicensesDeletedUsers.h"
#include "AutomationWizardLicensesSummary.h"
#include "AutomationWizardListItems.h"
#include "AutomationWizardLists.h"
#include "AutomationWizardMailboxPermissions.h"
#include "AutomationWizardMainFrame.h"
#include "AutomationWizardMessages.h"
#include "AutomationWizardMessageRules.h"
#include "AutomationWizardO365Reports.h"
#include "AutomationWizardPosts.h"
#include "AutomationWizardSitePermissions.h"
#include "AutomationWizardSites.h"
#include "AutomationWizardSpUsers.h"
#include "AutomationWizardTeamChannelMessages.h"
#include "AutomationWizardUserGroups.h"
#include "AutomationWizardUserManagerHierarchy.h"
#include "AutomationWizardUserRecycleBin.h"
#include "AutomationWizardSignIns.h"
#include "AutomationWizardSiteRoles.h"
#include "AutomationWizardSpGroups.h"
#include "AutomationWizardUsers.h"

#include "BaseO365Grid.h"
#include "DlgFeedback.h"
#include "DlgLicenseMessageBoxHTML.h"
#include "FileUtil.h"
#include "GridFrameBase.h"
#include "GridViewManager.h"
#include "JobCenterManager.h"
#include "MainFrame.h"
#include "Office365Admin.h"
#include "Registry.h"
#include "Sapio365Settings.h"
#include "TraceIntoFile.h"
#include "WindowsScheduledTask.h"
#include "YCodeJockMessageBox.h"

map<wstring, Script> AutomationWizardO365::g_DebugScriptMap;

const wstring AutomationWizardO365::g_GoBackstageBtn = _YTEXT("goBackstage");

// static
void AutomationWizardO365::InitAfterSessionWasSet(std::shared_ptr<const Sapio365Session> p_Session, DlgDoubleProgressCommon& p_Progress)
{
	// TODO move to sync SQL db when JC goes to Cosmos
	static const wstring g_RegistrySapio365Key				= _YTEXT("Software\\Ytria\\sapio365");
	static const wstring g_RegistryEntryLastUpdateVersion	= _YTEXT("JClastUpdateVersion");
	static const wstring g_ThisVersion						= Product::getInstance().getMajMinDevVersion() + _YTEXT(".") + Product::getInstance().getBuildVersion();

	// static -- only once per runtime
	static bool g_MustUpdateSystemJobs = [&p_Session]()
	{
		wstring JClastUpdateVersionValue;
		Registry::readString(HKEY_CURRENT_USER, g_RegistrySapio365Key, g_RegistryEntryLastUpdateVersion, JClastUpdateVersionValue);
		return JClastUpdateVersionValue.compare(g_ThisVersion) < 0
			|| JobCenterManager::GetInstance().IsEmpty(p_Session);
	}();

	if (g_MustUpdateSystemJobs && !AutomatedApp::IsAutomationRunning())
	{
		// All the sapio365 wizards must be added here.
		// Please keep on adding the wizard in alphabetical order for easy maintenance.

		ScriptParser xmlParser;// use one only: let us not waste time creating/deleting for each script the embedded Xerces platform
		
		static const auto numOfWizards = 39; // update total when new wizard is created
		p_Progress.SetCounterTotal2(numOfWizards);
		
		insertYtriaJobsIntoDB<AutomationWizardCommon>(xmlParser, p_Session, p_Progress);// keep Common on top

		insertYtriaJobsIntoDB<AutomationWizardChannels>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardColumns>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardContacts>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardConversations>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardDirectoryAudits>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardDirectoryRoles>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardDriveItemsGroup>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardDriveItemsSite>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardDriveItemsUser>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardEventsGroup>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardEventsUser>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardGroups>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardGroupsAuthors>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardGroupsMembers>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardGroupsOwners>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardGroupsRecycleBin>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardLicenses>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardLicensesDeletedUsers>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardLicensesSummary>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardListItems>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardLists>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardMailboxPermissions>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardMainFrame>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardMessageRules>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardMessages>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardO365Reports>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardPosts>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardSiteRoles>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardSignIns>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardSitePermissions>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardSites>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardSpGroups>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardSpUsers>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardTeamChannelMessages>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardUserGroups>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardUserManagerHierarchy>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardUserRecycleBin>(xmlParser, p_Session, p_Progress);
		insertYtriaJobsIntoDB<AutomationWizardUsers>(xmlParser, p_Session, p_Progress);

		ASSERT(numOfWizards == p_Progress.getCurrentCounter2());

		Registry::writeString(HKEY_CURRENT_USER, g_RegistrySapio365Key, g_RegistryEntryLastUpdateVersion, g_ThisVersion);// TODO move to sync SQL db when JC goes to Cosmos

		g_MustUpdateSystemJobs = false; // Be sure it's done only once
	}
}

//static 
template<class WizardClass>
void AutomationWizardO365::insertYtriaJobsIntoDB(ScriptParser& p_XMLparser, std::shared_ptr<const Sapio365Session>& p_Session, DlgDoubleProgressCommon& p_Progress)
{
	static_assert(std::is_base_of<AutomationWizardO365, WizardClass>::value, "WizardClass must inherit from AutomationWizardO365");

	AutomationWizardPointerType jc(new WizardClass);

	p_Progress.IncrementCounter2(_YFORMAT(L"Updating Job Center [%s]", jc->GetName().c_str()));

	map<wstring, Script> systemScriptsInDBByKey;
	map<wstring, Script> systemScriptsToPurgeFromDB;
	{
		vector<Script> scriptsInDB;
		JobCenterManager::GetInstance().LoadScripts(jc.get(), p_Session, scriptsInDB, true);
		for (const auto& s : scriptsInDB)
			if (s.IsSystem())
				systemScriptsInDBByKey[s.m_Key] = s;
	}
	systemScriptsToPurgeFromDB = systemScriptsInDBByKey;

	auto& yScripts = jc->getYScriptsForDBinitialization();
	std::sort(yScripts.begin(), yScripts.end(), [](const Script& first, const Script& second) { return first.m_Key.compare(second.m_Key) < 0; });
	p_XMLparser.ParseScripts(yScripts, false, {});// in ytria setup, positions are set according to key order; false: parse only header, do not serialize

	vector<Script> systemViews;
	const auto isSandbox = CRMpipe().IsSandbox();
	auto& jcm = JobCenterManager::GetInstance();
	for (auto& yScript : yScripts)
	{
		if (yScript.m_GridViewSettings_Vol.is_initialized())// it is a system view
			systemViews.push_back(yScript);
		else // it is a system job
		{
			auto foundInDB					= systemScriptsInDBByKey.find(yScript.m_Key);
			const bool newScript			= systemScriptsInDBByKey.end() == foundInDB;
			const bool newVersion			= !newScript && yScript.m_Version.compare(foundInDB->second.m_Version) > 0;
			const bool sandboxFlagChanged	= !newScript && foundInDB->second.IsSandbox() != yScript.IsSandbox();

			if (newVersion)
			{
				// visibility and position are personal settings, keep current values
				yScript.m_Position = foundInDB->second.m_Position;
				yScript.SetDisplayed(foundInDB->second.IsDisplayed());
			}

			if (sandboxFlagChanged)
				TraceIntoFile::trace(_YDUMP("AutomationWizardO365"), _YDUMP("insertYtriaJobsIntoDB"), _YDUMPFORMAT(L"Job %s changed to %s", yScript.m_Key.c_str(), yScript.IsSandbox() ? _YDUMP("SANDBOX") : _YDUMP("NOT SANDBOX")));

			yScript.m_LogUpdate_vol = false;
			if (newScript || newVersion || sandboxFlagChanged)
			{
				// needs to be serialized again to make sure the syntax is correct Bug #200714.RL.00C33A
				// not done above in order to keep job updates at startup as fast as possible
				auto validScript = p_XMLparser.ParseAndSerialize(yScript);
				if (validScript)
					jcm.Write(yScript, p_Session);
				else if (isSandbox)
					YCodeJockMessageBox(nullptr, DlgMessageBox::eIcon_Error, _YTEXTFORMAT(L"%s not updated", yScript.m_Key.c_str()), _YTEXT("See trace file validation errors"), yScript.m_Title, { { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } }).DoModal();
			}

			systemScriptsToPurgeFromDB.erase(yScript.m_Key);
		}
	}

	// create linked entries for common scripts
	// !-!-! It is presumed that AutomationWizardCommon is the first to be updated !-!-!
	static AutomationWizardCommon commonJC;	
	if (!MFCUtil::StringMatchW(jc->GetName(), commonJC.GetName()))
	{
		static vector<Script> commonScripts;
		if (commonScripts.empty())
		{
			commonScripts = commonJC.getYScriptsForDBinitialization();
			std::sort(commonScripts.begin(), commonScripts.end(), [](const Script& first, const Script& second) { return first.m_Key.compare(second.m_Key) < 0; });
			p_XMLparser.ParseScripts(commonScripts, false, {});// in ytria setup, positions are set according to key order
		}

		auto scriptCount = jc->GetScriptCount();
		for (auto& s : commonScripts)
		{
			if (s.IsSystem() && !s.m_Title.empty() && !Str::contains(s.m_Title, _YTEXT("DEBUG"), true) && !s.IsRestrictedIn(jc->GetName()))
			{
				// regenerate links in case main script was updated

				Script commonScriptLink = s;

				commonScriptLink.m_Module = jc->GetName();
				commonScriptLink.GenerateKey(s.m_Key);
				commonScriptLink.GenerateLink(s);
				commonScriptLink.m_Position = scriptCount++;
				commonScriptLink.m_LogUpdate_vol = false;
				commonScriptLink.m_Common = JobCenterUtil::g_CommonFlag;
				commonScriptLink.m_Flags = s.m_Flags;
				jcm.Write(commonScriptLink, p_Session);

				systemScriptsToPurgeFromDB.erase(commonScriptLink.m_Key);
			}
			else if (!s.IsSystem())
				systemScriptsToPurgeFromDB.erase(s.m_Key);
		}
	}

	jc->m_SessionForInit = p_Session;
	for (auto& s : systemScriptsToPurgeFromDB)
		jc->RemoveScript(s.second);
	jc->m_SessionForInit = nullptr;

	GridViewManager::GetInstance().UpdateSystemViews(jc->GetName(), systemViews, p_Session, p_XMLparser);
}

vector<Script> AutomationWizardO365::GetScripts()
{
	vector<Script> scripts;	
	loadFromDB(scripts, getSession());
	return scripts;
}

void AutomationWizardO365::setFramePostProcess()
{

}

std::shared_ptr<const Sapio365Session> AutomationWizardO365::getSession() const
{
	if (m_SessionForInit)
		return m_SessionForInit;

	auto frame		= dynamic_cast<GridFrameBase*>(getFrame());
	auto mainFrame	= dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != frame  || nullptr != mainFrame);
	return nullptr != frame ? frame->GetSapio365Session() : mainFrame->GetSapio365Session();	
}

void AutomationWizardO365::loadFromDB(vector<Script>& p_Scripts, std::shared_ptr<const Sapio365Session>& p_Session)
{
	JobCenterManager::GetInstance().LoadScripts(this, p_Session, p_Scripts);
}

uint32_t AutomationWizardO365::GetScriptCount() const
{
	return JobCenterManager::GetInstance().GetScriptCount(GetName(), getSession());
}

void AutomationWizardO365::FixPositions() // fix bad positions (wrong user edit in db, bad update, import failure... Bug #190813.RL.00A32E)
{ 
	vector<Script> scripts;
	loadFromDB(scripts, getSession());
	
	// NB: positions of removed scripts are and must be ignored

	auto updateScript = [this](Script& s, uint32_t& p)
	{
		if (s.m_Position != p)
		{
			s.m_Position		= p;
			s.m_LogUpdate_vol	= false;
			UpdateScript(s);
		}
		p++;
	};
	
	map<wstring, Script> commonLocked;
	map<wstring, Script> locked;
	for (auto iScript = scripts.begin(); iScript != scripts.end();)
	{
		auto& s = *iScript;
		if (s.IsPositionLocked())
		{
			if (s.IsCommon())
			{
				commonLocked[s.m_Key] = s;
				iScript = scripts.erase(iScript);
			}
			else
			{
				locked[s.m_Key] = s;
				iScript = scripts.erase(iScript);
			}
		}
		else
			iScript++;
	}

	uint32_t p = 0;

	// set common locked scripts on top
	for (auto cls : commonLocked)
		updateScript(cls.second, p);

	// set locked scripts after common locked scripts
	for (auto ls : locked)
		updateScript(ls.second, p);
	
	// other scripts
	for (auto& s : scripts)
		updateScript(s, p);
}

//static 
const Script& AutomationWizardO365::getDebugScript(const wstring& p_ScriptKey)
{
	const auto findIt = g_DebugScriptMap.find(p_ScriptKey);
	if (g_DebugScriptMap.end() != findIt)
		return findIt->second;

	static const Script g_Dummy;
	return g_Dummy;
}

bool AutomationWizardO365::HasScript(const wstring& p_ScriptKey) const
{
	return JobCenterManager::GetInstance().HasScript(p_ScriptKey, getSession());
}

bool AutomationWizardO365::HasScriptPreset(const wstring& p_ScriptPresetKey) const
{
	return JobCenterManager::GetInstance().HasScriptPreset(p_ScriptPresetKey, getSession());
}

Script AutomationWizardO365::GetScript(const wstring& p_ScriptKey) const
{
	Script s;
	JobCenterManager::GetInstance().GetScript(p_ScriptKey, getSession(), s);
	return s;
}

Script AutomationWizardO365::GetScript(const int64_t p_ScriptID) const
{
	return JobCenterManager::GetInstance().Read(p_ScriptID, getSession());
}

ScriptPreset AutomationWizardO365::GetScriptPreset(const wstring& p_ScriptPresetKey) const
{
	ScriptPreset sp;
	JobCenterManager::GetInstance().GetScriptPreset(p_ScriptPresetKey, getSession(), sp);
	return sp;
}

ScriptSchedule AutomationWizardO365::GetScriptSchedule(const wstring& p_ScriptScheduleKey) const
{
	ScriptSchedule sps;
	JobCenterManager::GetInstance().GetScriptSchedule(p_ScriptScheduleKey, getSession(), sps);
	return sps;
}

ScriptSchedule AutomationWizardO365::GetScriptScheduleByID(const wstring& p_ScriptScheduleID) const
{
	return JobCenterManager::GetInstance().ReadSchedule(Str::getINT64FromString(p_ScriptScheduleID), getSession());
}

bool AutomationWizardO365::ImportScript(Script& p_Script)
{
	TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("ImportScript"), _YTEXTFORMAT(L"Importing script: %s", p_Script.m_Key.c_str()));

	p_Script.m_Position = GetScriptCount();
	return UpdateScript(p_Script);
}

bool AutomationWizardO365::UpdateScript(Script& p_Script)
{
	TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("UpdateScript"), _YTEXTFORMAT(L"Updating script: %s", p_Script.m_Key.c_str()));

	return JobCenterManager::GetInstance().Write(p_Script, getSession());
}

bool AutomationWizardO365::RemoveScript(Script& p_Script)
{
	TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("removeScript"), _YTEXTFORMAT(L"Removing script: %s (set flag to REMOVED)", p_Script.m_Key.c_str()));

	vector<Script> vScript;
	AddJobSelectionFilter<JobSelectionFilter_KeyEquals>(p_Script.m_Key);
	if (JobCenterManager::GetInstance().LoadScripts(this, getSession(), vScript))
	{
		if (p_Script.m_Schedules_Vol.empty() || p_Script.m_Presets_Vol.empty())
			JobCenterManager::GetInstance().Load(p_Script, getSession());

		ASSERT(vScript.size() == 1 && vScript.front().m_Key == p_Script.m_Key);
		if (vScript.size() == 1 && vScript.front().m_Key == p_Script.m_Key)
		{
			auto& s = vScript.front();
			for (auto& sch : s.m_Schedules_Vol)
				RemoveScriptSchedule(sch);
			for (auto& p : s.m_Presets_Vol)
				RemoveScriptPreset(p);
		}
	}
	RemoveLastSelectionFilter();

	p_Script.m_Status = STATUS_REMOVED;
	const auto removed = UpdateScript(p_Script);
	if (removed)
		HTMLmainframeJobRemove(p_Script);
	return removed;
}

void AutomationWizardO365::HTMLmainframeJobRemove(const Script& p_Script)
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		mainFrame->HTMLRemoveOneJob(p_Script);
}

void AutomationWizardO365::HTMLmainframeJobScheduleRemove(const ScriptSchedule& p_ScriptSchedule)
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		mainFrame->HTMLRemoveOneSchedule(p_ScriptSchedule);
}

void AutomationWizardO365::HTMLmainframeJobSchedulesUpdate()
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		mainFrame->HTMLUpdateSchedules();
}

bool AutomationWizardO365::AddScriptPreset(const Script& p_Script, const wstring& p_PresetContent, ScriptPreset& p_ScriptPreset)
{
	TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptPreset"), _YTEXTFORMAT(L"Adding preset for script: %s", p_Script.m_Key.c_str()));

	ASSERT(p_Script.HasID());
	ASSERT(!p_PresetContent.empty());
	ASSERT(!p_ScriptPreset.m_Title.empty());
	if (p_Script.HasID() && !p_PresetContent.empty() && !p_ScriptPreset.m_Title.empty())
	{
		auto count			= JobCenterManager::GetInstance().GetScriptPresetCount(p_Script.m_ID.get(), true, getSession()) + 1;
		auto extension		= FileUtil::FileGetExtension(p_Script.m_Key);
		auto keyNoExtension = p_Script.m_Key;
		FileUtil::FileRemoveExtension(keyNoExtension);

		p_ScriptPreset.m_Key			= _YTEXTFORMAT(L"%s-%s%s", keyNoExtension.c_str(), Str::getStringFromNumber(count).c_str(), extension.c_str());
		p_ScriptPreset.m_ScriptID		= p_Script.m_ID.get();
		p_ScriptPreset.m_ScriptContent	= p_PresetContent;
		p_ScriptPreset.m_Status			= STATUS_OK;
		return AutomationWizardO365::UpdateScriptPreset(p_ScriptPreset);
	}
	else
	{
		if (!p_Script.HasID())
			TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptPreset"), _YTEXTFORMAT(L"! failure: unknown script ID"));
		if (p_PresetContent.empty())
			TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptPreset"), _YTEXTFORMAT(L"! failure: empty content"));
		if (p_ScriptPreset.m_Title.empty())
			TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptPreset"), _YTEXTFORMAT(L"! failure: empty preset title"));
	}

	return false;
}

bool AutomationWizardO365::RemoveScriptPreset(ScriptPreset& p_ScriptPreset)
{
	TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("RemoveScriptPreset"), _YTEXTFORMAT(L"Removing preset: %s", p_ScriptPreset.m_Key.c_str()));

	if (p_ScriptPreset.m_Schedules_Vol.empty())
		JobCenterManager::GetInstance().LoadSchedules(p_ScriptPreset, getSession());

	for (auto& sch : p_ScriptPreset.m_Schedules_Vol)
		RemoveScriptSchedule(sch);

	p_ScriptPreset.m_Status = STATUS_REMOVED;
	return UpdateScriptPreset(p_ScriptPreset);
}

bool AutomationWizardO365::UpdateScriptPreset(ScriptPreset& p_ScriptPreset)
{
	TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("UpdateScriptPreset"), _YTEXTFORMAT(L"Updating: %s", p_ScriptPreset.m_Key.c_str()));

	ASSERT(p_ScriptPreset.m_Status.is_initialized());
	return JobCenterManager::GetInstance().Write(p_ScriptPreset, getSession());
}

bool AutomationWizardO365::AddScriptSchedule(const Script& p_Script, const ScriptPreset& p_ScriptPreset, const ScriptSchedule& p_ScriptSchedule)
{
	TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptSchedule"), _YTEXTFORMAT(L"Adding schedule for preset: %s", p_ScriptPreset.m_Key.c_str()));

	ASSERT(p_Script.HasID());
	ASSERT(p_ScriptPreset.HasID());
	ASSERT(!p_ScriptSchedule.m_Title.empty());
	ASSERT(!p_ScriptSchedule.m_Key.empty());
	ASSERT(p_Script.m_ID && p_ScriptPreset.m_ScriptID == *p_Script.m_ID);
	if (p_Script.HasID()
		&& p_ScriptPreset.HasID()
		&& p_ScriptPreset.m_ScriptID == *p_Script.m_ID
		&& !p_ScriptSchedule.m_Title.empty()
		&& !p_ScriptSchedule.m_Key.empty())
	{
		ScriptSchedule sps		= p_ScriptSchedule;
		sps.m_ScriptID			= *p_Script.m_ID;
		sps.m_ScriptPresetID	= *p_ScriptPreset.m_ID;
		sps.m_Status			= STATUS_OK;
		return AutomationWizardO365::UpdateScriptSchedule(sps);
	}
	else
	{
		if (!p_Script.HasID())
			TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptSchedule"), _YTEXTFORMAT(L"! failure: unknown script ID"));
		if (!p_ScriptPreset.HasID())
			TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptSchedule"), _YTEXTFORMAT(L"! failure: unknown script preset ID"));
		if (p_ScriptSchedule.m_Title.empty())
			TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptSchedule"), _YTEXTFORMAT(L"! failure: empty schedule title"));
		if (p_ScriptSchedule.m_Key.empty())
			TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptSchedule"), _YTEXTFORMAT(L"! failure: empty schedule key"));
		if (p_Script.m_ID && p_ScriptPreset.m_ScriptID != *p_Script.m_ID)
			TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptSchedule"), _YTEXTFORMAT(L"! failure: unmatched script ID"));
	}

	return false;
}

bool AutomationWizardO365::AddScriptSchedule(const Script& p_Script, const ScriptSchedule& p_ScriptSchedule)
{
	TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptSchedule"), _YTEXTFORMAT(L"Adding schedule for script: %s", p_Script.m_Key.c_str()));

	ASSERT(p_Script.HasID());
	ASSERT(!p_ScriptSchedule.m_Title.empty());
	ASSERT(!p_ScriptSchedule.m_Key.empty());
	if (p_Script.HasID() && !p_ScriptSchedule.m_Title.empty() && !p_ScriptSchedule.m_Key.empty())
	{
		ScriptSchedule sps	= p_ScriptSchedule;
		sps.m_ScriptID		= p_Script.m_ID.get();
		sps.m_Status		= STATUS_OK;
		const auto added = AutomationWizardO365::UpdateScriptSchedule(sps);
		if (added)
			HTMLmainframeJobSchedulesUpdate();
		return added;
	}
	else
	{
		if (!p_Script.HasID())
			TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptSchedule"), _YTEXTFORMAT(L"! failure: unknown script ID"));
		if (p_ScriptSchedule.m_Title.empty())
			TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptSchedule"), _YTEXTFORMAT(L"! failure: empty schedule title"));
		if (p_ScriptSchedule.m_Key.empty())
			TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("AddScriptSchedule"), _YTEXTFORMAT(L"! failure: empty schedule key"));
	}
	return false;
}

bool AutomationWizardO365::UpdateScriptSchedule(ScriptSchedule& p_ScriptSchedule)
{
	TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("UpdateScriptSchedule"), _YTEXTFORMAT(L"Updating: %s", p_ScriptSchedule.m_Key.c_str()));

	ASSERT(p_ScriptSchedule.m_Status.is_initialized());
	const auto updated = JobCenterManager::GetInstance().Write(p_ScriptSchedule, getSession());
	if (updated)
		HTMLmainframeJobSchedulesUpdate();
	return updated;
}

bool AutomationWizardO365::RemoveScriptSchedule(ScriptSchedule& p_ScriptSchedule)
{
	TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("RemoveScriptSchedule"), _YTEXTFORMAT(L"Removing script Schedule: %s (set flag to REMOVED + remove from Windows Task Scheduler)", p_ScriptSchedule.m_Key.c_str()));

	auto error = WindowsScheduledTask::Delete(p_ScriptSchedule.m_Key);
	if (!error.empty())
	{
		TraceIntoFile::trace(_YTEXT("AutomationWizardO365"), _YTEXT("RemoveScriptSchedule"), _YTEXTFORMAT(L"Removing task from Windows Task Scheduler error: %s (schedule removed from DB)", error.c_str()));
		m_ScheduleRemoveErrors.push_back(error);
	}

	p_ScriptSchedule.m_Status = STATUS_REMOVED;
	const auto removed = UpdateScriptSchedule(p_ScriptSchedule);
	if (removed)
		HTMLmainframeJobScheduleRemove(p_ScriptSchedule);
	return removed;
}

void AutomationWizardO365::ClearScheduleRemoveErrors()
{
	m_ScheduleRemoveErrors.clear();
}

const vector<wstring>& AutomationWizardO365::GetScheduleRemoveErrors() const
{
	return m_ScheduleRemoveErrors;
}

bool AutomationWizardO365::ToggleScriptDisplay(Script& p_Script)
{
	if (!p_Script.IsPositionLocked())
	{
		p_Script.SetDisplayed(!p_Script.IsDisplayed());
		return UpdateScript(p_Script);
	}
	return false;
}

bool AutomationWizardO365::runSpecific(const wstring& p_ScriptID)
{
	return false;// if something must be run outside the regular job center context, put it here
}

bool AutomationWizardO365::CheckRequirements(const std::set<wstring>& p_Requirements, bool p_ShowDialog, const AutomationContext& p_Context, const wstring& p_ScriptKey, bool p_StopOnFirstFailure, AutomationAction* p_Action)
{
	ASSERT(p_ShowDialog || nullptr != p_Action); // Otherwise, why are you calling this?

	bool rvAuthorized = true;

	auto frame = dynamic_cast<GridFrameBase*>(p_Context.GetFrame());
	auto mainFrame = dynamic_cast<MainFrame*>(p_Context.GetFrame());
	ASSERT(nullptr != frame || nullptr != mainFrame);
	if (nullptr != frame || nullptr != mainFrame)
	{
		bool isConnected = false;
		bool isAdvanced = false;
		bool isUltraAdmin = false;
		const auto sessionFrame = nullptr != frame ? frame->GetSapio365Session() : nullptr != mainFrame ? mainFrame->GetSapio365Session() : nullptr;
		if (sessionFrame)
		{
			isConnected = sessionFrame->IsConnected();
			isAdvanced = sessionFrame->IsAdvanced();
			isUltraAdmin = sessionFrame->IsUltraAdmin();
		}

		for (const auto& r : p_Requirements)
		{
			auto wRequirement = Str::lrtrim(r);

			if (!MFCUtil::StringMatchW(wRequirement, g_ParamValueRequiredLogin)
				&& !MFCUtil::StringMatchW(wRequirement, g_ParamValueWarningIfAdmin)
				&& !MFCUtil::StringMatchW(wRequirement, g_ParamValueWarningIfAdminTech)
				&& !MFCUtil::StringMatchW(wRequirement, g_ParamValueWarningIfUltraAdminTech))
			{
				wstring errorText = _YFORMATERROR(L"Unknown wizard requirement '%s'", wRequirement.c_str());
				if (nullptr != p_Action)
					p_Action->AddError(errorText.c_str());

				if (p_ShowDialog)
				{
					Util::ShowRestrictedAccessDialog(DlgRestrictedAccess::Image::PERMISSIONS_REMINDER
						, { _T("Error in job header."),
							errorText }
						, NoSetting<bool>()
						, frame
						, true);
				}

				rvAuthorized = false;
				break;
			}

			if (!isConnected && MFCUtil::StringMatchW(wRequirement, g_ParamValueRequiredLogin))
			{
				rvAuthorized = false;
				const wstring errorText = YtriaTranslate::Do(AutomationWizardO365_scriptAuthorized_2, _YLOC("Please log in to run this job.")).c_str();
				if (nullptr != p_Action)
					p_Action->AddError(errorText.c_str());
				if (p_ShowDialog)
				{
					Util::ShowRestrictedAccessDialog(DlgRestrictedAccess::Image::PERMISSIONS_REMINDER
						, { YtriaTranslate::Do(AutomationWizardO365_scriptAuthorized_1, _YLOC("Active session required")).c_str(),
							errorText }
						, HideJobCenterLoginRequiredDialogSetting(p_ScriptKey)
						, frame
						, true);
				}
			}
			else if (isAdvanced && MFCUtil::StringMatchW(wRequirement, g_ParamValueWarningIfAdmin))
			{
				if (p_ShowDialog)
				{
					rvAuthorized = Util::ShowRestrictedAccessDialog(DlgRestrictedAccess::Image::PERMISSIONS_REMINDER
						, { YtriaTranslate::Do(O365Grid_showUserRestrictedAccessDialog_3, _YLOC("Reminder: You will need the correct permissions.")),
							YtriaTranslate::Do(O365Grid_showUserRestrictedAccessDialog_4, _YLOC("This is a friendly reminder that, if your Office365 credentials do not have the correct permissions, you may not have access to the items you select.")) }
						, HideHideJobCenterAdminRequiredDialogSetting(p_ScriptKey)
						, frame);
					if (!rvAuthorized && nullptr != p_Action)
						p_Action->AddError(YtriaTranslate::Do(AutomationWizardO365_CheckRequirements_1, _YLOC("Canceled by user.")).c_str());
				}
				else
				{
					if (nullptr != p_Action)
						p_Action->AddError(_T("You may not have access to the items you select."));
					rvAuthorized = false;
				}
			}
			else if (isAdvanced && MFCUtil::StringMatchW(wRequirement, g_ParamValueWarningIfAdminTech))
			{
				const wstring errorTitle = YtriaTranslate::Do(O365Grid_showUserRestrictedAccessDialog_2, _YLOC("Reminder: The Microsoft Graph API may not fully support this yet in Advanced sessions.")).c_str();
				if (p_ShowDialog)
				{
					rvAuthorized = Util::ShowRestrictedAccessDialog(DlgRestrictedAccess::Image::GRAPHAPI_SUPPORT
						, { errorTitle,
							YtriaTranslate::Do(O365Grid_showUserRestrictedAccessDialog_1, _YLOC("At the moment this query may require an Advanced session with elevated privileges to work as intended.")) }
						, HideJobCenterUltraAdminRequiredDialogSetting(p_ScriptKey)
						, frame);
					if (!rvAuthorized && nullptr != p_Action)
						p_Action->AddError(YtriaTranslate::Do(AutomationWizardO365_CheckRequirements_1, _YLOC("Canceled by user.")).c_str());
				}
				else
				{
					if (nullptr != p_Action)
						p_Action->AddError(errorTitle.c_str());
					rvAuthorized = false;
				}
			}
			else if (isUltraAdmin && MFCUtil::StringMatchW(wRequirement, g_ParamValueWarningIfUltraAdminTech))
			{
				const wstring errorTitle = YtriaTranslate::Do(O365Grid_showGroupRestrictedAccessDialog_1, _YLOC("Reminder: The Microsoft Graph API may not fully support this yet in Ultra Admin mode."));
				if (p_ShowDialog)
				{
					rvAuthorized = Util::ShowRestrictedAccessDialog(DlgRestrictedAccess::Image::GRAPHAPI_SUPPORT
						, { errorTitle,
							YtriaTranslate::Do(O365Grid_showGroupRestrictedAccessDialog_2, _YLOC("At the moment this query may require an Advanced session to work as intended.")) }
						, HideHideJobCenterNoUltraAdminRequiredDialogSetting(p_ScriptKey)
						, frame);
					if (!rvAuthorized && nullptr != p_Action)
						p_Action->AddError(YtriaTranslate::Do(AutomationWizardO365_CheckRequirements_1, _YLOC("Canceled by user.")).c_str());
				}
				else
				{
					p_Action->AddError(errorTitle.c_str());
					rvAuthorized = false;
				}
			}

			if (p_StopOnFirstFailure && !rvAuthorized)
				break;
		}
	}

	return rvAuthorized;
}

bool AutomationWizardO365::scriptAuthorized(const Script& p_Script, const AutomationContext& p_Context)
{
	auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	const auto authorizedByLicense = p_Script.IsCommon() || nullptr != app && app->IsJobAllowedByLicense(p_Script.m_LibraryID);// executable Common scripts are generic application features (e.g. Reset Grid), let them pass
	if (!authorizedByLicense)
	{
		DlgLicenseMessageBoxHTML dlg(nullptr,
			DlgMessageBox::eIcon_ExclamationWarning,
			_T("This job is not allowed with your license."),
			p_Script.m_Title,
			_YTEXT(""),
			{ { IDOK, YtriaTranslate::Do(LicenseManager_isVendorInitOK_1, _YLOC("OK")).c_str() } },
			{ 0, _YTEXT("") },
			app->GetApplicationColor());
		dlg.DoModal();
	}

	return authorizedByLicense && CheckRequirements(Str::explodeIntoSet(p_Script.m_Requirements, g_RequirementsDelimitor), true, p_Context, p_Script.m_Key, true, nullptr);
}

void AutomationWizardO365::JSONgenerationPreProcess()
{
}

void AutomationWizardO365::JSONgenerationPostProcess()
{
}

wstring AutomationWizardO365::generateJSON()
{
	wstring rvJSON;

	wstring color;
	AutomatedApp* app = dynamic_cast<AutomatedApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != app)
		color = _YTEXT("#") + app->GetApplicationColor();

	vector<web::json::value> dlgJsonItems;
	web::json::value intro;
	intro[_YTEXT("hbs")]		= web::json::value(_YTEXT("Intro"));
	intro[_YTEXT("Title")]		= web::json::value(YtriaTranslate::Do(AutomationWizardO365_generateJSON_1, _YLOC("Job Center")).c_str());
	intro[_YTEXT("subTitile")]	= web::json::value(YtriaTranslate::Do(AutomationWizardO365_generateJSON_2, _YLOC("What would you like to do?")).c_str());
	intro[_YTEXT("nameBtn")]	= web::json::value(g_GoBackstageBtn);
	dlgJsonItems.push_back(intro);

	FixPositions();
	for (const auto& script : GetScripts())
		if (script.IsDisplayed() && script.IsRunnable())
		{
			web::json::value scriptEntry;
			scriptEntry[_YTEXT("hbs")]			= web::json::value(_YTEXT("Action"));
			scriptEntry[g_ScriptKey]			= web::json::value(script.m_Key);
			scriptEntry[_YTEXT("Icon")]			= web::json::value(script.m_IconFontAwesome);
			// Note:	'far fa-' has been added by ScriptParser::loadMetaData() if not set by WizardIconType
			//			and DlgAutomationWizardEditHTML::processPostedData() will enforce it by default
			scriptEntry[_YTEXT("Label")]		= script.IsSandbox() ? web::json::value(script.m_Title + _YTEXT("-SANDBOX-")) : web::json::value(script.m_Title);
			scriptEntry[_YTEXT("SubLabel")]		= web::json::value(script.m_Description);
			if (!script.m_Tooltip.empty())
				scriptEntry[_YTEXT("Popup")]	= web::json::value(script.m_Tooltip);
			dlgJsonItems.push_back(scriptEntry);
		}

	web::json::value footer;
	footer[_YTEXT("hbs")]		= web::json::value(_YTEXT("Footer"));
	footer[_YTEXT("Title")]		= web::json::value(YtriaTranslate::Do(AutomationWizardO365_generateJSON_3, _YLOC("Suggest a new 'job'")).c_str());
	footer[g_FooterNameBtn]		= web::json::value(g_FooterNameBtn);
	footer[_YTEXT("iconBtn")]	= web::json::value(_YTEXT("fas fa-lightbulb-on"));
	dlgJsonItems.push_back(footer);

	web::json::value root =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("main"),
				JSON_BEGIN_OBJECT
					JSON_PAIR_KEY(_YTEXT("id"))							JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Wizard"))),
					JSON_PAIR_KEY(_YTEXT("method"))						JSON_PAIR_VALUE(JSON_STRING(_YTEXT("POST"))),
					JSON_PAIR_KEY(_YTEXT("action"))						JSON_PAIR_VALUE(JSON_STRING(_YTEXT("#"))),
					JSON_PAIR_KEY(_YTEXT("name"))						JSON_PAIR_VALUE(JSON_STRING(_YTEXT("collapse"))),
					JSON_PAIR_KEY(_YTEXT("value"))						JSON_PAIR_VALUE(JSON_STRING(_YTEXT("valueCollapse"))),
					JSON_PAIR_KEY(_YTEXT("icon"))						JSON_PAIR_VALUE(JSON_STRING(_YTEXT("fal fa-angle-left"))),
					JSON_PAIR_KEY(_YTEXT("searchIcon"))					JSON_PAIR_VALUE(JSON_STRING(_YTEXT("far fa-search"))),
					JSON_PAIR_KEY(_YTEXT("autocompletePlaceholder"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Search")))
				JSON_END_OBJECT
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("items"),
				web::json::value::array(dlgJsonItems)
			JSON_END_PAIR
		JSON_END_OBJECT;

	rvJSON = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());

	return rvJSON;
}

void AutomationWizardO365::RunFooter()
{
	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != app)
		app->SuggestNewJob(getFrame());
}

// =================================================================================================

AutomationWizardO365Folder::AutomationWizardO365Folder(const wstring& p_FolderPath)
{
	wstring path = p_FolderPath;
	if (FileUtil::PathIsRelative(path))
	{
		auto app = dynamic_cast<AutomatedApp*>(AfxGetApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
		{
			auto appFullPath = app->getCurrentAppFullPath();
			appFullPath.erase(appFullPath.find_last_of(_YTEXT('\\')));
			path = wstring(appFullPath) + _YTEXT("\\") + p_FolderPath;
		}
	}

	processPath(path);

	ScriptParser XMLparser;
	auto& yScripts = getYScriptsForDBinitialization();
	std::sort(yScripts.begin(), yScripts.end(), [](const Script& first, const Script& second) { return first.m_Key.compare(second.m_Key) < 0; });
	XMLparser.ParseScripts(yScripts, false, {});// in ytria setup, positions are set according to key order
	for (auto& yScript : yScripts)
		g_DebugScriptMap[yScript.m_Key] = yScript;
}

void AutomationWizardO365Folder::processPath(const wstring& i_Path)
{
	CFileFind finder;
	if (finder.FindFile(i_Path.c_str()))
	{
		finder.FindNextFile();

		if (finder.IsDirectory())
		{
			CString strWildcard = finder.GetFilePath() + _YTEXT("\\*.xml");
			auto exploreMe = finder.FindFile(strWildcard);
			while (exploreMe)
			{
				exploreMe = finder.FindNextFile();
				if (!finder.IsDots())
					processPath(finder.GetFilePath().GetBuffer());
			}
		}
		else
		{
			const wstring filePath = finder.GetFilePath();
			std::wifstream ifs(filePath);
			addScript(filePath, std::wstring(std::istreambuf_iterator<wchar_t>(ifs), std::istreambuf_iterator<wchar_t>()));
		}
	}
	finder.Close();
}

wstring AutomationWizardO365Folder::GetName() const
{
	return _YTEXT("-- Debug wizard --");
}