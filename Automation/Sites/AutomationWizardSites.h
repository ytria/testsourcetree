#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardSites : public AutomationWizardO365
{
public:
	AutomationWizardSites();

	virtual wstring GetName() const override
	{
		return _YTEXT("Sites");
	}
};