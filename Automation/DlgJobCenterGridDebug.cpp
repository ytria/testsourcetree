#include "includes_WIN_Generic.h" //_DEFINE_INCLUDES_WIN_H_
#include "includes_STL_Generic.h" //_DEFINE_INCLUDES_STL_H_

#include "YtriaTranslate.h"

#include "DlgJobCenterGridDebug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(DlgJobCenterGridDebug, ResizableDialog)
	//{{AFX_MSG_MAP(DlgLicenseActivation)
	ON_BN_CLICKED(IDOK,	OnOK)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

TCHAR DlgJobCenterGridDebug::s_savedPositionKey[] = _YTEXT("DlgJobCenterDebug-Saved-Coordinates");

DlgJobCenterGridDebug::DlgJobCenterGridDebug(CWnd* pParent) : ResizableDialog(IDD, pParent)
{
}

DlgJobCenterGridDebug::~DlgJobCenterGridDebug()
{
}

void DlgJobCenterGridDebug::DoDataExchange(CDataExchange* pDX)
{
	ResizableDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID_GRIDAREA,	m_LabelGridArea);
	DDX_Control(pDX, IDC_GRID_HEADER,	m_LabelGridHeader);
	DDX_Control(pDX, IDOK,			m_BtnOk);
}

BOOL DlgJobCenterGridDebug::OnInitDialogSpecificResizable()
{
	SetWindowText(CString(_YDUMP("Job Center viewer")));
	
	m_LabelGridHeader.SetWindowText(_YDUMP("Jobs available in this instance of sapio365:"));
	m_BtnOk.SetWindowText(_YDUMP("Ok"));

	CRect aRect;
	m_LabelGridArea.GetWindowRect(&aRect);
	ScreenToClient(&aRect);
	if (!m_Grid.Create(this, aRect))
	{
		// Creation of grid failed.
		ASSERT(FALSE);
		return -1;
	}

	m_Grid.ShowWindow(SW_SHOW);
	m_Grid.BringWindowToTop();

	AddAnchor(m_LabelGridHeader,	CSize(0, 0),		CSize(0, 10));
	AddAnchor(m_Grid,				CSize(0, 10),		CSize(100, 100));
	AddAnchor(IDOK,					CSize(100, 100),	CSize(100, 100));
	AddAnchor(IDC_BUTTON_TEST,		CSize(0, 100),		CSize(0, 100));

	restoreSavedSizePosition(s_savedPositionKey);

	return TRUE;
}