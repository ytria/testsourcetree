#pragma once

#include "AutomationWizardO365.h"

class AutomationWizardDriveItemsSite : public AutomationWizardO365
{
public:
	AutomationWizardDriveItemsSite();

	virtual wstring GetName() const override
	{
		return _YTEXT("DriveItemsSite");
	}
};