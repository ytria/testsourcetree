#include "IdentitySetDeserializer.h"

#include "IdentityDeserializer.h"
#include "JsonSerializeUtil.h"


void IdentitySetDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        IdentityDeserializer id;
        if (JsonSerializeUtil::DeserializeAny(id, _YTEXT("application"), p_Object))
            m_Data.Application = id.GetData();
    }

    {
        IdentityDeserializer id;
        if (JsonSerializeUtil::DeserializeAny(id, _YTEXT("device"), p_Object))
            m_Data.Device = id.GetData();
    }

    {
        IdentityDeserializer id;
        if (JsonSerializeUtil::DeserializeAny(id, _YTEXT("user"), p_Object))
            m_Data.User = id.GetData();
    }
}
