#include "SessionsMigrationAddUseOnPremVersion5To6.h"
#include "SessionMigrationUtil.h"
#include "SessionsSqlEngine.h"
#include "SQLSafeTransaction.h"
#include "SqlQueryMultiPreparedStatement.h"
#include "ISessionsMigrationVersion.h"
#include "Sapio365Settings.h"

void SessionsMigrationAddUseOnPremVersion5To6::Build()
{
	AddStep(std::bind(&SessionsMigrationAddUseOnPremVersion5To6::CreateNewFromOld, this));
	AddStep(std::bind(&SessionsMigrationAddUseOnPremVersion5To6::AddUseOnPremFlag, this));
}

VersionSourceTarget SessionsMigrationAddUseOnPremVersion5To6::GetVersionInfo() const
{
	return { 5, 6 };
}

bool SessionsMigrationAddUseOnPremVersion5To6::CreateNewFromOld()
{
	return SessionMigrationUtil::CopyDatabase(GetVersionInfo());
}

bool SessionsMigrationAddUseOnPremVersion5To6::AddUseOnPremFlag()
{
	bool success = false;

	wstring aadCompName = OnPremAADComputerNameSetting().Get() ? *OnPremAADComputerNameSetting().Get() : _YTEXT("");
	bool useConsistencyGuid = OnPremUseMsDsConsistencyGuidSetting().Get() ? *OnPremUseMsDsConsistencyGuidSetting().Get() : _YTEXT("");

	SQLSafeTransaction transaction(GetTargetSQLEngine(), [this, &success, &aadCompName, useConsistencyGuid] {
		const std::vector<wstring> statements =
		{
			LR"(ALTER TABLE Sessions ADD COLUMN UseOnPrem INTEGER DEFAULT 0;)",
			LR"(ALTER TABLE Sessions ADD COLUMN DontAskAgainUseOnPrem INTEGER DEFAULT 0;)",
			_YTEXTFORMAT(LR"(ALTER TABLE Sessions ADD COLUMN AADComputerName TEXT DEFAULT "%s";)", aadCompName.c_str()),
			LR"(ALTER TABLE Sessions ADD COLUMN UseRemoteRSAT INTEGER DEFAULT 0;)",
			_YTEXTFORMAT(LR"(ALTER TABLE Sessions ADD COLUMN UseMsDsConsistencyGuid INTEGER DEFAULT %s;)", useConsistencyGuid ? _YTEXT("1") : _YTEXT("0")),
			LR"(ALTER TABLE Sessions ADD COLUMN ADDSServer TEXT;)",
			LR"(ALTER TABLE Sessions ADD COLUMN ADDSUsername TEXT;)",
			LR"(ALTER TABLE Sessions ADD COLUMN ADDSPassword TEXT;)"
		};

		SqlQueryMultiPreparedStatement queryAlter(GetTargetSQLEngine(), statements);
		queryAlter.Run();

		success = queryAlter.IsSuccess();
	}, true);

	return transaction.Run() && success;
}
