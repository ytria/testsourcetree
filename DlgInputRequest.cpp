#include "DlgInputRequest.h"

#include "ChilkatUtils.h"
#include "O365AdminUtil.h"
#include "SafeTaskCall.h"

#include "Resource.h"

BEGIN_MESSAGE_MAP(DlgInputRequest, ResizableDialog)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelType)
END_MESSAGE_MAP()

DlgInputRequest::DlgInputRequest(std::shared_ptr<Sapio365Session> p_Session, const wstring& p_SessionDisplayName, CWnd* p_Parent)
	: ResizableDialog(IDD_INPUT_REQUEST, p_Parent)
	, m_Session(p_Session)
	, m_SessionDisplayName(p_SessionDisplayName)
{
	ASSERT(m_Session);
}

BOOL DlgInputRequest::OnInitDialogSpecificResizable()
{
	SetWindowText(_YTEXT("Graph Request Tester"));

	m_SessionName.SetWindowText(m_SessionDisplayName.c_str());

	m_TypeCombo.AddStringUnique(_YTEXT("GET"));
	// FIXME: handle those?
	m_TypeCombo.AddStringUnique(_YTEXT("POST"));
	m_TypeCombo.AddStringUnique(_YTEXT("PATCH"));
	m_TypeCombo.AddStringUnique(_YTEXT("DEL"));
	m_TypeCombo.AddStringUnique(_YTEXT("PUT"));
	//m_TypeCombo.SetWindowText(_YTEXT("GET"));
	m_TypeCombo.SetCurSel(m_TypeCombo.FindString(-1, _YTEXT("GET")));


	m_APICombo.AddStringUnique(_YTEXT("v1.0"));
	m_APICombo.AddStringUnique(_YTEXT("beta"));
	//m_APICombo.SetWindowText(_YTEXT("v1.0"));
	m_APICombo.SetCurSel(m_APICombo.FindString(-1, _YTEXT("v1.0")));

	m_SessionCombo.AddStringUnique(_YTEXT("[USR]"));
	m_SessionCombo.AddStringUnique(_YTEXT("[APP]"));
	//m_SessionCombo.SetWindowText(_YTEXT("[USR]"));
	m_SessionCombo.SetCurSel(m_SessionCombo.FindString(-1, _YTEXT("[USR]")));
	if (!(m_Session->IsRole() || m_Session->IsElevated()))
	{
		m_SessionCombo.EnableWindow(FALSE);
		m_SessionCombo.ShowWindow(SW_HIDE);
	}

	m_JSonBodyEdit.SetCueBanner(_YTEXT("JSON body for POST, PATCH or PUT"));
	m_JSonBodyEdit.EnableWindow(FALSE);
	m_URLEdit.SetCueBanner(_YTEXT("e.g. users?$select=id,displayName"));

	m_RunButton.SetWindowText(_YTEXT("Run"));

	m_ResultEdit.SetCueBanner(_YTEXT("Status will be shown here."));
	m_ResultEdit.SetReadOnly(TRUE);

	m_ResponseEdit.SetCueBanner(_YTEXT("Response body will be shown here."));
	m_ResponseEdit.SetReadOnly(TRUE);

	AddAnchor(IDC_STATIC1, CSize(0, 0), CSize(100, 0));
	AddAnchor(IDC_COMBO1, CSize(0, 0), CSize(33, 0));
	AddAnchor(IDC_COMBO2, CSize(33, 0), CSize(66, 0));
	AddAnchor(IDC_COMBO4, CSize(66, 0), CSize(100, 0));
	AddAnchor(IDC_EDIT1, CSize(0, 0), CSize(100, 0));
	AddAnchor(IDC_EDIT3, CSize(0, 0), CSize(100, 10));
	AddAnchor(IDC_EDIT4, CSize(0, 10), CSize(100, 10));
	AddAnchor(IDC_EDIT2, CSize(0, 10), CSize(100, 100));
	AddAnchor(IDOK, CSize(100, 100), CSize(100, 100));

	return TRUE;
}

void DlgInputRequest::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDC_STATIC1, m_SessionName);
	DDX_Control(pDX, IDC_COMBO1, m_TypeCombo);
	DDX_Control(pDX, IDC_COMBO2, m_APICombo);
	DDX_Control(pDX, IDC_COMBO4, m_SessionCombo);
	DDX_Control(pDX, IDC_EDIT1, m_URLEdit);
	DDX_Control(pDX, IDC_EDIT2, m_ResponseEdit);
	DDX_Control(pDX, IDC_EDIT3, m_JSonBodyEdit);
	DDX_Control(pDX, IDC_EDIT4, m_ResultEdit);
	DDX_Control(pDX, IDOK, m_RunButton);
}

void DlgInputRequest::OnOK()
{
	// Process.

	CString url;
	m_URLEdit.GetWindowText(url);

	CString api;
	m_APICombo.GetWindowText(api);

	CString type;
	m_TypeCombo.GetWindowText(type);

	CString session;
	m_SessionCombo.GetWindowText(session);

	CString jsonBody;
	m_JSonBodyEdit.GetWindowText(jsonBody);

	m_ResultEdit.SetWindowText(CString());
	m_ResponseEdit.SetWindowText(CString());

	web::json::value jsonValue;
	const auto allowJson = type == _YTEXT("POST") || type == _YTEXT("PUT") || type == _YTEXT("PATCH") || type == _YTEXT("DEL");
	if (allowJson)
	{
		std::error_code err;
		jsonValue = web::json::value::parse((LPCTSTR)jsonBody, err);
	}

	const auto validURL = !url.IsEmpty() && web::uri::validate((LPCTSTR)url);
	if (validURL && (!allowJson || jsonBody.IsEmpty() || !jsonValue.is_null()))
	{
		web::uri uri((LPCTSTR)url);

		auto msgsession = session == _YTEXT("[USR]")
			? m_Session->GetMSGraphSession(Sapio365Session::USER_SESSION)
			: m_Session->GetMSGraphSession(Sapio365Session::APP_SESSION);

		auto req =
				type == _YTEXT("GET")	? msgsession->Read	(uri, nullptr, YtriaTaskData(), WebPayload(), (LPCTSTR)api)
			:	type == _YTEXT("POST")	? msgsession->Post	(uri, nullptr, YtriaTaskData(), WebPayloadJSON(jsonValue), (LPCTSTR)api)
			:	type == _YTEXT("PUT")	? msgsession->Put	(uri, nullptr, YtriaTaskData(), WebPayloadJSON(jsonValue), (LPCTSTR)api)
			:	type == _YTEXT("PATCH")	? msgsession->Patch	(uri, nullptr, YtriaTaskData(), WebPayloadJSON(jsonValue), (LPCTSTR)api)
			:	/*type == _YTEXT("DEL")	?*/ msgsession->Delete(uri, nullptr, YtriaTaskData(), (LPCTSTR)api);

		auto errorHandler = [](const std::exception_ptr& p_ExceptPtr) -> boost::YOpt<wstring>
		{
			ASSERT(p_ExceptPtr);
			if (p_ExceptPtr)
			{
				try
				{
					std::rethrow_exception(p_ExceptPtr);
				}
				catch (const RestException& e)
				{
					return HttpError(e).GetFullErrorMessage();
				}
				catch (const std::exception& e)
				{
					return SapioError(e).GetFullErrorMessage();
				}
			}

			ASSERT(false);
			return wstring(_YTEXT("!! Unknown error. !!"));
		};

		wstring statusString;
		auto response = safeTaskCall(req.Then([msgsession, &uri, &api, &statusString](RestResultInfo result)
			{
				auto& err = result.GetErrorMessage();
				auto& reason = result.Response.reason_phrase();
				auto httpStatusCode = result.Response.status_code();
				if (err.empty())
				{
					statusString = wstring(_YTEXT("HTTP ")) +
						std::to_wstring(httpStatusCode) +
						wstring(_YTEXT(" - ")) +
						reason;
				}
				else
				{
					statusString = wstring(_YTEXT("HTTP ")) +
						std::to_wstring(httpStatusCode) +
						wstring(_YTEXT(" - ")) +
						reason +
						wstring(_YTEXT(" - ")) + err;
				}
				return result.GetBodyAsString();
			}), msgsession, errorHandler, YtriaTaskData()).get();

		if (statusString.empty())
		{
			// Went through error handler
			// Use response as status
			if (response)
				m_ResultEdit.SetWindowText(response->c_str());
			else
				m_ResultEdit.SetWindowText(_YTEXT("!! No error, no result !!"));
		}
		else
		{
			m_ResultEdit.SetWindowText(statusString.c_str());
			if (response)
				m_ResponseEdit.SetWindowText(ChilkatUtils::BeautifyJson(*response).c_str());
			else
				m_ResponseEdit.SetWindowText(_YTEXT("!! No Response :-o !!"));
		}
	}
	else
	{
		if (!validURL)
		{
			m_ResultEdit.SetWindowText(_YTEXT("Invalid URL."));
		}
		else
		{
			ASSERT(allowJson && !jsonBody.IsEmpty());
			m_ResultEdit.SetWindowText(_YTEXT("Invalid JSON body."));
		}
	}
}

void DlgInputRequest::OnSelType()
{
	CString type;
	m_TypeCombo.GetWindowText(type);
	m_JSonBodyEdit.EnableWindow(type == _YTEXT("POST") || type == _YTEXT("PATCH") || type == _YTEXT("PUT"));
}

COLORREF DlgInputRequest::MyTextEdit::OnQueryBackColor() const
{
	return RGB(255, 255, 255);
}
