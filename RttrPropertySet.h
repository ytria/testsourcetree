#pragma once

#include "IPropertySetBuilder.h"


enum class PropertySetType { Sync, Create, Update, List, Cache }; // Analog to functions in IApiObject

template <class T>
class RttrPropertySet : public IPropertySetBuilder
{
public:
	RttrPropertySet(PropertySetType p_Type)
		:m_Type(p_Type)
	{
	}

	vector<rttr::property> GetPropertySet() const override
	{
		T m_Dummy;
		switch (m_Type)
		{
		case PropertySetType::Sync:
			return m_Dummy.GetListProperties(m_Dummy);
			break;
		case PropertySetType::Create:
			return m_Dummy.GetCreateProperties(m_Dummy);
			break;
		case PropertySetType::Update:
			return m_Dummy.GetUpdateProperties(m_Dummy);
			break;
		case PropertySetType::List:
			return m_Dummy.GetListProperties(m_Dummy);
			break;
		case PropertySetType::Cache:
			return m_Dummy.GetCacheProperties(m_Dummy);
			break;
		default:
			break;
		}

		ASSERT(false);
		return vector<rttr::property>();
	}

private:
	PropertySetType m_Type;
};

