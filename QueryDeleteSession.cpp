#include "QueryDeleteSession.h"
#include "SqlQueryPreparedStatement.h"
#include "O365SQLiteEngine.h"

QueryDeleteSession::QueryDeleteSession(O365SQLiteEngine& p_Engine, const SessionIdentifier& p_Identifier)
	: m_Engine(p_Engine),
	m_Identifier(p_Identifier)
{
}

void QueryDeleteSession::Run()
{
	wstring queryStr = _YTEXT(R"(DELETE FROM Sessions WHERE EmailOrAppId=? AND SessionType=? AND RoleId=?)");
	SqlQueryPreparedStatement query(m_Engine, queryStr);
	query.BindString(1, m_Identifier.m_EmailOrAppId);
	query.BindString(2, m_Identifier.m_SessionType);
	query.BindInt64(3, m_Identifier.m_RoleID);
	query.Run();

	m_Status = query.GetStatus();
	ASSERT(query.GetNbRowsChanged() == 1);
}
