#pragma once

#include "BusinessObject.h"

#include "BusinessListItem.h"
#include "BusinessColumnDefinition.h"
#include "ContentType.h"
#include "IdentitySet.h"
#include "List.h"
#include "ListInfo.h"
#include "SystemFacet.h"

class BusinessList : public BusinessObject
{
public:
    BusinessList() = default;
    BusinessList(const List& p_JsonList);

    boost::YOpt<PooledString> GetName() const;
    void SetName(const boost::YOpt<PooledString>& val);

	const boost::YOpt<PooledString>& GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& p_Val);

    void SetCreatedBy(const boost::YOpt<IdentitySet>& val);
    const boost::YOpt<IdentitySet>& GetCreatedBy() const;

    const boost::YOpt<YTimeDate>& GetCreatedDateTime() const;
    void SetCreatedDateTime(const boost::YOpt<YTimeDate>& val);

    const boost::YOpt<PooledString>& GetDescription() const;
    void SetDescription(const boost::YOpt<PooledString>& val);

    const boost::YOpt<IdentitySet>& GetLastModifiedBy() const;
    void SetLastModifiedBy(const boost::YOpt<IdentitySet>& val);

    const boost::YOpt<YTimeDate>& GetLastModifiedDateTime() const;
    void SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& val);

    const boost::YOpt<PooledString>& GetWebUrl() const;
    void SetWebUrl(const boost::YOpt<PooledString>& val);

    const vector<BusinessColumnDefinition>& GetColumns() const;
    void SetColumns(const vector<BusinessColumnDefinition>& val);

    const vector<ContentType>& GetContentTypes() const;
    void SetContentTypes(const vector<ContentType>& val);

    const boost::YOpt<ListInfo>& GetListInfo() const;
    void SetListInfo(const boost::YOpt<ListInfo>& val);

    const boost::YOpt<SystemFacet>& GetSystem() const;
    void SetSystem(const boost::YOpt<SystemFacet>& val);

    const vector<BusinessListItem>& GetListItems() const;
    void SetListItems(const vector<BusinessListItem>& p_Val);

    const boost::YOpt<PooledString>& GetEtag() const;
    void SetEtag(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<ItemReference>& GetParentReference() const;
    void SetParentReference(const boost::YOpt<ItemReference>& p_Val);

private:
    boost::YOpt<PooledString> m_Name;
	boost::YOpt<PooledString> m_DisplayName;
    boost::YOpt<IdentitySet> m_CreatedBy;
    boost::YOpt<YTimeDate> m_CreatedDateTime;
    boost::YOpt<PooledString> m_Description;
    boost::YOpt<IdentitySet> m_LastModifiedBy;
    boost::YOpt<YTimeDate> m_LastModifiedDateTime;
    boost::YOpt<ItemReference> m_ParentReference;
    boost::YOpt<PooledString> m_WebUrl;

    vector<BusinessColumnDefinition> m_Columns;
    vector<ContentType> m_ContentTypes;
    vector<BusinessListItem> m_ListItems;
    boost::YOpt<ListInfo> m_ListInfo;
    boost::YOpt<PooledString> m_ETag;
    boost::YOpt<SystemFacet> m_System;

    RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
    friend class GraphListDeserializer;
};

