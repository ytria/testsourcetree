#include "RequestDumper.h"
#include "LoggerService.h"
#include "MFCUtil.h"
#include "FileUtil.h"
#include "Registry.h"
#include "Office365Admin.h"
#include "Product.h"

const int RequestDumper::g_MaxFileSizeInBytes = 1024 * 1024 * 100; // 100MB
vector<wstring> RequestDumper::g_DumpFilePaths;

const wstring RequestDumper::g_RequestHeaderPrefix = _YTEXT(">>> ");
const wstring RequestDumper::g_ResponseHeaderPrefix = _YTEXT("<<< ");

RequestDumper::RequestDumper(const SessionIdentifier& p_SessionIdentifier)
	: m_SessionIdentifier(p_SessionIdentifier)
    , m_FolderPath(GetDumpPath())
{
    ASSERT(!m_FolderPath.empty());

    WORD milliSeconds = 0;
    if (m_SessionIdentifier.m_RoleID != 0)
    {
        LoggerService::Debug(_YFORMAT(L"Debug mode started for %s (%s %s)", m_SessionIdentifier.m_EmailOrAppId.c_str(), m_SessionIdentifier.m_SessionType.c_str()));
        m_FileNameBase = m_FolderPath + MFCUtil::getSystemDateString(wstring(_YTEXT("%Y-%m-%d_%H%M%S")), &milliSeconds) + _YTEXT("_") + m_SessionIdentifier.m_EmailOrAppId + m_SessionIdentifier.m_SessionType + _YTEXT("_") + std::to_wstring(m_SessionIdentifier.m_RoleID);
    }
    else
    {
        LoggerService::Debug(YtriaTranslate::Do(RequestDumper_RequestDumper_1, _YLOC("Debug mode started for %1 (%2)"), m_SessionIdentifier.m_EmailOrAppId.c_str(), m_SessionIdentifier.m_SessionType.c_str()));
        m_FileNameBase = m_FolderPath + MFCUtil::getSystemDateString(wstring(_YTEXT("%Y-%m-%d_%H%M%S")), &milliSeconds) + _YTEXT("_") + m_SessionIdentifier.m_EmailOrAppId + m_SessionIdentifier.m_SessionType;
    }

    FileUtil::CreateFolderHierarchy(m_FolderPath);
    newFile();
}

RequestDumper::~RequestDumper()
{
    if (m_SessionIdentifier.m_RoleID != 0)
        LoggerService::Debug(_YFORMAT(L"Debug mode stopped for %s (%s %s)", m_SessionIdentifier.m_EmailOrAppId.c_str(), m_SessionIdentifier.m_SessionType.c_str()));
    else
        LoggerService::Debug(YtriaTranslate::Do(RequestDumper__RequestDumper_1, _YLOC("Debug mode stopped for %1 (%2)"), m_SessionIdentifier.m_EmailOrAppId.c_str(), m_SessionIdentifier.m_SessionType.c_str()));

    const bool emptyFile = m_FileStream && m_FileStream->is_open() && 0 == m_FileStream->tellp();
    m_FileStream.reset();
    if (emptyFile)
    {
        auto it = std::find(g_DumpFilePaths.begin(), g_DumpFilePaths.end(), m_FilePath);
        ASSERT(g_DumpFilePaths.end() != it);
        if (g_DumpFilePaths.end() != it)
        {
            g_DumpFilePaths.erase(it);
            DeleteFile(m_FilePath.c_str());
        }
    }
}

void RequestDumper::DumpResponse(uint32_t p_RequestID, const RestResultInfo& p_Info)
{
    auto uri = p_Info.Request.absolute_uri().to_string();

    boost::YOpt<wstring> responseBody;
    try
    {
        responseBody = p_Info.GetBodyAsString();
    }
    catch (const std::exception&)
    {
    }

    Dump(p_RequestID, p_Info.Request, responseBody ? *responseBody : _YTEXT(""), true, p_Info.Response.status_code());
}

void RequestDumper::DumpRequest(uint32_t p_RequestID, const web::http::http_request& p_Request, const WebPayload& p_Body)
{
    Dump(p_RequestID, p_Request, p_Body.GetDump(), false, 0);
}

void RequestDumper::Dump(uint32_t p_RequestID, const web::http::http_request& p_Request, const wstring& p_Body, bool p_Response, web::http::status_code p_ResponseCode)
{
    auto uri = p_Request.absolute_uri().to_string();
    auto method = p_Request.method();

    const auto header = (p_Response ? g_ResponseHeaderPrefix : g_RequestHeaderPrefix) + std::to_wstring(p_RequestID);
    m_FileStream->write((char*)header.c_str(), header.size() * 2);
    m_FileStream->write((char*)L"\r\n", 4);

    wstring prefix;
    if (p_Response)
        prefix = _YTEXTFORMAT(L"HTTP %s for %s ", std::to_wstring(p_ResponseCode).c_str(), p_Request.method().c_str());
    else
        prefix = _YTEXTFORMAT(L"%s ", p_Request.method().c_str());

    m_FileStream->write((char*)prefix.c_str(), prefix.size() * 2);
    m_FileStream->write((char*)uri.c_str(), uri.size() * 2);
    m_FileStream->write((char*)L"\r\n", 4);
    if (!p_Body.empty())
    {
        m_FileStream->write((char*)p_Body.c_str(), p_Body.size() * 2);
        m_FileStream->write((char*)L"\r\n", 4);
    }
    m_FileStream->write((char*)L"\r\n", 4);
    m_FileStream->flush();

    if (m_FileStream->tellp() >= g_MaxFileSizeInBytes)
        newFile();
}

vector<wstring> RequestDumper::ClearPaths()
{
    vector<wstring> paths;
    std::swap(g_DumpFilePaths, paths); // empty list
    return paths;
}

wstring RequestDumper::GetDumpPath()
{
    static const wstring folderPath = []()
    {
        wstring folderPath;

        wstring traceFilePath;
        if (Registry::readString(HKEY_CURRENT_USER, _YTEXT("Software\\Ytria"), _YTEXT("YtriaEnableTrace"), traceFilePath) && !traceFilePath.empty())
        {
            auto ok = FileUtil::GetFileFolder(traceFilePath, folderPath);
            ASSERT(ok);
            if (!ok)
                folderPath.clear();
        }

        if (folderPath.empty())
        {
            // Default

            auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
            ASSERT(nullptr != app);
            if (nullptr != app)
            {
                folderPath = app->GetGridConfigFolderPath();

                if (Str::endsWith(folderPath, Product::s_STRING_ALL_GRIDS_CONFIG_DIR))
                {
                    folderPath = folderPath.substr(0, folderPath.size() - Product::s_STRING_ALL_GRIDS_CONFIG_DIR.size());
                    ASSERT(Str::endsWith(folderPath, _YTEXT("\\")));
                }
                else
                {
                    ASSERT(false);
                }
            }
            else
            {
                ASSERT(false);
            }
        }

        if (!Str::endsWith(folderPath, _YTEXT("\\")))
            folderPath += _YTEXT("\\");
        folderPath += _YTEXT("Dump\\");
        return folderPath;
    }();

    return folderPath;
}

void RequestDumper::newFile()
{
    m_FilePath = m_FileNameBase + _YTEXT(".") + std::to_wstring(m_FileCounter) + _YTEXT(".log");
    m_FileStream = std::make_unique<std::ofstream>(m_FilePath, std::ios::binary);
    ++m_FileCounter;

    LoggerService::Debug(_YTEXTFORMAT(L"New dump file: %s", m_FilePath.c_str()));

    g_DumpFilePaths.push_back(m_FilePath);
}
