#include "GridPrivateChannelMembers.h"

#include "BasicGridSetup.h"
#include "BusinessAADUserConversationMemberDeserializer.h"
#include "BusinessUserGuest.h"
#include "DlgAddItemsToGroups.h"
#include "GridUpdater.h"
#include "GridUpdaterOptions.h"
#include "SisterhoodFieldUpdate.h"

BEGIN_MESSAGE_MAP(GridPrivateChannelMembers, O365Grid)
	ON_COMMAND(ID_CHANNELMEMBERSGRID_ADD, OnAddMember)
	ON_UPDATE_COMMAND_UI(ID_CHANNELMEMBERSGRID_ADD, OnUpdateAddMember)
	ON_COMMAND(ID_CHANNELMEMBERSGRID_REMOVE, OnRemoveMember)
	ON_UPDATE_COMMAND_UI(ID_CHANNELMEMBERSGRID_REMOVE, OnUpdateRemoveMember)
	ON_COMMAND(ID_CHANNELMEMBERSGRID_MAKEOWNER, OnMakeOwner)
	ON_UPDATE_COMMAND_UI(ID_CHANNELMEMBERSGRID_MAKEOWNER, OnUpdateMakeOwner)
	ON_COMMAND(ID_CHANNELMEMBERSGRID_MAKEMEMBER, OnMakeMember)
	ON_UPDATE_COMMAND_UI(ID_CHANNELMEMBERSGRID_MAKEMEMBER, OnUpdateMakeMember)

	NEWMODULE_COMMAND(ID_USERGRID_SHOWPARENTGROUPS, ID_USERGRID_SHOWPARENTGROUPS_FRAME, ID_USERGRID_SHOWPARENTGROUPS_NEWFRAME, OnShowParentGroups, O365Grid::OnUpdateShowBusinessUser)
	NEWMODULE_COMMAND(ID_USERGRID_SHOWLICENSES, ID_USERGRID_SHOWLICENSES_FRAME, ID_USERGRID_SHOWLICENSES_NEWFRAME, OnShowLicenses, O365Grid::OnUpdateShowBusinessUser)
	NEWMODULE_COMMAND(ID_GRID_SHOWUSERDETAILS, ID_GRID_SHOWUSERDETAILS_FRAME, ID_GRID_SHOWUSERDETAILS_NEWFRAME, OnShowUserDetails, O365Grid::OnUpdateShowBusinessUser)
END_MESSAGE_MAP()

GridPrivateChannelMembers::GridPrivateChannelMembers()
	: m_ColTeamID(nullptr)
	, m_ColTeamDisplayName(nullptr)
	, m_ColChannelID(nullptr)
	, m_ColChannelDisplayName(nullptr)
	, m_ColMemberUserID(nullptr)
	, m_ColMemberDisplayName(nullptr)
	, m_ColMemberEmail(nullptr)
	, m_ColMemberRoles(nullptr)
	, m_ColMemberMembershipType(nullptr)
{
	EnableGridModifications();

	// FIXME: add wizard
	//initWizard<AutomationWizardChannels>(_YTEXT("Automation\\Channels"));
}

void GridPrivateChannelMembers::customizeGrid()
{
	//m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_GRIDCHANNELS_ACCELERATOR));

	const wstring familyChannelInfo = _T("Channel Info");
	{
		BasicGridSetup gridSetup(GridUtil::g_AutoNamePrivateChannelMembers, LicenseUtil::g_codeSapio365channels);
		gridSetup.AddMetaColumn(GridSetupMetaColumn(&m_ColTeamID, _T("Team ID"), familyChannelInfo, _YUID("teamId")));
		gridSetup.AddMetaColumn(GridSetupMetaColumn(&m_ColChannelID, _T("Channel ID"), familyChannelInfo, _YUID("channelId")));
		gridSetup.Setup(*this, false);
	}

	setBasicGridSetupHierarchy({ { { m_ColChannelID, 0 } }
								,{ rttr::type::get<BusinessAADUserConversationMember>().get_id() }
								,{ rttr::type::get<BusinessChannel>().get_id(), rttr::type::get<BusinessAADUserConversationMember>().get_id() } });

	m_ColTeamDisplayName = AddColumn(_YUID("teamDisplayName"), _T("Team Display Name"), familyChannelInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColChannelDisplayName = AddColumn(_YUID("channelDisplayName"), _T("Channel Display Name"), familyChannelInfo, g_ColumnSize12, { g_ColumnsPresetDefault });

	const wstring memberInfo = _T("Member Info");
	m_ColMemberDisplayName = AddColumn(_YUID("displayName"), _T("Display Name"), memberInfo, g_ColumnSize12, { g_ColumnsPresetDefault });

	// WARNING: If you change this unique ID ("userId"), fix code in O365Grid::IsUserAuthorizedByRoleDelegation
	m_ColMemberUserID = AddColumn(_YUID("userId"), _T("User ID"), memberInfo, g_ColumnSize12, { g_ColumnsPresetTech });

	m_ColMemberEmail = AddColumn(_YUID("email"), _T("Email"), memberInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColMemberMembershipType = AddColumn(_YUID("MEMBERSHIPTYPE"), _T("Membership Type"), memberInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColMemberRoles = AddColumn(_YUID("roles"), _T("Roles"), memberInfo, g_ColumnSize12);

	addColumnGraphID();
	AddColumnForRowPK(m_ColChannelID);
	AddColumnForRowPK(GetColId());
}

void GridPrivateChannelMembers::OnShowUserDetails()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_GRID_SHOWUSERDETAILS));
		getUserIDs(commandInfo.GetIds(), true, commandInfo.GetRBACPrivilege());

		if (!commandInfo.GetIds().empty())
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::User, Command::ModuleTask::ListDetails, commandInfo, { this, g_ActionNameSelectedShowDetailsUsers, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		else
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"), _YR("Y2395")).c_str());
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"), _YR("Y2396")).c_str());
	}
}

void GridPrivateChannelMembers::OnShowParentGroups()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		commandInfo.SetOrigin(Origin::User);
		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_USERGRID_SHOWPARENTGROUPS));
		getUserIDs(commandInfo.GetIds(), true, commandInfo.GetRBACPrivilege());

		if (!commandInfo.GetIds().empty())
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::User, Command::ModuleTask::ListOwners, commandInfo, { this, g_ActionNameSelectedShowParentGroups, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"), _YR("Y2369")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"), _YR("Y2370")).c_str());
	}
}

void GridPrivateChannelMembers::OnShowLicenses()
{
	if (openModuleConfirmation())
	{
		CommandInfo commandInfo;
		if (GetAutomationName() == GridUtil::g_AutoNameUsersRecycleBin || Origin::DeletedUser == GetOrigin())
			commandInfo.SetOrigin(Origin::DeletedUser);
		else
			commandInfo.SetOrigin(Origin::User);

		commandInfo.SetRBACPrivilege(getCommandRBACPrivilege(ID_USERGRID_SHOWLICENSES, false));
		getUserIDs(commandInfo.GetIds(), true, commandInfo.GetRBACPrivilege());

		if (!commandInfo.GetIds().empty())
		{
			commandInfo.SetMetaDataColumnInfo(getMetaDataColumnInfoFor(commandInfo.GetOrigin()));
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Licenses, Command::ModuleTask::List, commandInfo, { this, g_ActionNameSelectedShowLicenses, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_1, _YLOC("No User ID"), _YR("Y2388")).c_str());
		}
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"), _YR("Y2389")).c_str());
	}
}

void GridPrivateChannelMembers::OnAddMember()
{
	if (IsFrameConnected())
	{
		const auto privilege = 0;// getPrivilege();
		const bool hasOutOfScopePrivilege = true; // IsAuthorizedByRoleDelegation(getOutOfScopePrivilege());

		// <Group Id, <Group name, User ids>>
		std::map<PooledString, std::pair<PooledString, std::set<PooledString>>> parentsForSelection;
		set<GridBackendRow*>	channelRows;

		auto addTargetChannel = [this, &channelRows, &parentsForSelection, &privilege](GridBackendRow* row)
		{
			GridBackendRow* channelRow = row->GetTopAncestorOrThis();
			// FIXME: handle RBAC?
			if (nullptr != channelRow/* && IsGroupAuthorizedByRoleDelegation(businessGroupRow, privilege)*/)
			{
				ASSERT(GridUtil::IsBusinessChannel(channelRow));
				if (GridUtil::IsBusinessChannel(channelRow) && channelRows.insert(channelRow).second)
					parentsForSelection[channelRow->GetField(GetColId()).GetValueStr()] = { channelRow->GetField(m_ColChannelDisplayName).GetValueStr(), GetIdsOfChildRowsThatAllMatch({ channelRow }, m_ColMemberUserID) };
			}

			return true;
		};

		for (auto row : GetSelectedRows())
		{
			ASSERT(nullptr != row);
			if (nullptr != row && !row->IsGroupRow())
			{
				if (!addTargetChannel(row))
					return;
			}
		}

		ASSERT(!channelRows.empty());
		if (!channelRows.empty())
		{
			const wstring title = _YFORMAT(L"Add users as members to %s selected channels", Str::getStringFromNumber(channelRows.size()).c_str());
			const wstring explanation = _YFORMAT(L"These %s channels will have the users you select below added as members.\nTo see the complete list, use Load full directory.", Str::getStringFromNumber(channelRows.size()).c_str());

			const auto users = getUsers();

			DlgAddItemsToGroups dlg(
				GetSession(),
				parentsForSelection,
				DlgAddItemsToGroups::SELECT_USERS_FOR_GROUPS,
				users,
				{},
				{},
				title,
				explanation,
				hasOutOfScopePrivilege ? RoleDelegationUtil::RBAC_Privilege::RBAC_NONE
				: RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE,
				this);

			if (IDOK == dlg.DoModal())
			{
				GridBackendRow* rowToScrollTo = nullptr;
				bool update = false;

				if (nullptr != GetColId())
				{
					// List of all new users to add to selected groups.
					auto& newItems = dlg.GetSelectedItems();

					// Add new users.
					for (auto channelRow : channelRows)
					{
						ASSERT(nullptr != channelRow);
						if (nullptr != channelRow)
						{
							const GridBackendField& fChannelGraphID = channelRow->GetField(GetColId());

							vector<vector<GridBackendRow*>> addedRows;
							for (const auto& item : newItems)
							{
								if (addedRows.empty() || !addedRows.back().empty())
									addedRows.emplace_back();

								{
									// Recycle the row if already in.
									GridBackendRow* pURow = GetChildRow(channelRow, item.GetID(), m_ColMemberUserID->GetID());

									if (nullptr == pURow)
									{
										GridBackendField fieldId(GetNextTemporaryCreatedObjectID(), GetColId());
										pURow = GetRowIndex().GetRow({ GridBackendField(fChannelGraphID.GetValueStr(), m_ColChannelID), fieldId }, true, false);

										update = true;

										ASSERT(nullptr != pURow);
										if (nullptr != pURow)
										{
											pURow->SetParentRow(channelRow);
											ASSERT(item.GetObjectType() == BusinessUser().get_type().get_id() || item.GetObjectType() == BusinessUserGuest().get_type().get_id());
											SetRowObjectType(pURow, BusinessAADUserConversationMember());

											if (GridUtil::IsBusinessAADUserConversationMember(pURow))
											{
												for (auto c : { m_ColTeamID, m_ColTeamDisplayName, m_ColChannelID, m_ColChannelDisplayName })
												{
													if (channelRow->HasField(c))
														pURow->AddField(channelRow->GetField(c));
													else
														pURow->RemoveField(c);
												}

												pURow->AddField(item.GetID(), m_ColMemberUserID);
												pURow->AddField(item.GetDisplayName(), m_ColMemberDisplayName);
												// FIXME: really?
												pURow->AddField(item.GetPrincipalName(), m_ColMemberEmail);
												if (item.GetObjectType() == BusinessUserGuest().get_type().get_id())
												{
													pURow->AddField(_YTEXT("guest"), m_ColMemberRoles);
													pURow->AddField(_YTEXT("guest"), m_ColMemberMembershipType);
												}
												else
												{
													pURow->AddField(_YTEXT(""), m_ColMemberRoles);
													pURow->AddField(_YTEXT("member"), m_ColMemberMembershipType);
												}
											}
											else
											{
												ASSERT(false);
											}

											addedRows.back().push_back(pURow);
										}
									}
									else
									{
										if (pURow->IsDeleted())
										{
											row_pk_t rowPk;
											GetRowPK(pURow, rowPk);
											update = GetModifications().Revert(rowPk, false);
										}
									}
								}
							}

							if (nullptr == rowToScrollTo && !addedRows.empty() && !addedRows[0].empty())
							{
								OnUnSelectAll();
								rowToScrollTo = addedRows[0][0];
							}

							for (auto& rows : addedRows)
							{
								CreatedObjectModification* mainMod = nullptr;
								for (auto& row : rows)
								{
									row_pk_t rowPk;
									GetRowPK(row, rowPk);
									GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPk));
									SelectRowAndExpandParentGroups(row, false);
								}
							}
						}
					}

					if (update)
						UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

					if (nullptr != rowToScrollTo)
						ScrollToRow(rowToScrollTo);
				}
			}
		}
	}
}

void GridPrivateChannelMembers::OnUpdateAddMember(CCmdUI* pCmdUI)
{
	//const bool hasOutOfScopePrivilege = true;//IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE);
	//const bool enable = std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, hasOutOfScopePrivilege](auto& p_Row)
	//	{
	//		auto ref = nullptr != p_Row ? p_Row->GetTopAncestor() : nullptr;
	//		return nullptr != ref
	//			&& !p_Row->IsGroupRow()
	//			&& !p_Row->IsDeleted()
	//			&& (!p_Row->HasField(m_ColMemberRoles) || wstring(_YTEXT("owner")) != p_Row->GetField(m_ColMemberRoles).GetValueStr())
	//			//&& IsGroupAuthorizedByRoleDelegation(ref, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT)
	//			&& (hasOutOfScopePrivilege || IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
	//	});

	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& IsFrameConnected()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& p_Row)
		{
			return nullptr != p_Row
				&& !p_Row->IsGroupRow()
				//&& IsGroupAuthorizedByRoleDelegation(p_Row->GetTopAncestorOrThis()->GetField(m_ColTeamID).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_MEMBERS_EDIT)
				;
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridPrivateChannelMembers::OnRemoveMember()
{
	const auto privilege = 0/*getPrivilege()*/;
	const bool hasOutOfScopePrivilege = true;// IsAuthorizedByRoleDelegation(getOutOfScopePrivilege());
	bool promptedForEdit = false;
	bool deleteEdited = false;
	bool update = false;
	std::map<GridBackendRow*, row_pk_t> rowsToRevert;
	std::set<GridBackendRow*> modifiedRows;
	for (auto row : GetSelectedRows())
	{
		ASSERT(nullptr != row);

		if (nullptr != row && !row->IsGroupRow() && nullptr != row->GetParentRow())
		{
			//if (IsGroupAuthorizedByRoleDelegation(row->GetParentRow(), privilege))
			{
				DeletedObjectModification* mainMod = nullptr;
				vector<GridBackendRow*>* currentRevertList = nullptr;
				{
					ASSERT(GridUtil::IsBusinessAADUserConversationMember(row));

					if (hasOutOfScopePrivilege
						|| IsUserAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE))
					{
						row_pk_t rowPk;
						GetRowPK(row, rowPk);

						if (row->IsCreated())
						{
							ASSERT(nullptr != GetModifications().GetRowModification<CreatedObjectModification>(rowPk));
							rowsToRevert[row] = rowPk;
						}
						else if (!row->IsDeleted())
						{
							ASSERT(nullptr == GetModifications().GetRowModification<DeletedObjectModification>(rowPk));

							bool shouldDelete = true;
							if (row->IsModified())
							{
								if (!promptedForEdit)
								{
									YCodeJockMessageBox dlg(this,
										DlgMessageBox::eIcon_Question,
										_T("Remove Member"),
										YtriaTranslate::Do(GridDriveItems_OnDeleteItem_2, _YLOC("The selected rows contain unsaved changes. Do you want to delete them instead?")).c_str(),
										YtriaTranslate::Do(GridDriveItems_OnDeleteItem_3, _YLOC("Pending changes will be lost.")).c_str(),
										{ { IDOK, YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str() } });

									deleteEdited = IDOK == dlg.DoModal();
									promptedForEdit = true;
								}

								if (deleteEdited)
								{
									GetModifications().Revert(rowPk, true);
									update = true;
								}
								else
								{
									shouldDelete = false;
								}
							}

							if (shouldDelete)
							{
								GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, rowPk));
								update = true;
							}
						}
						else
						{
							ASSERT(nullptr != GetModifications().GetRowModification<DeletedObjectModification>(rowPk));
						}
					}
				}
			}
		}
	}

	if (!modifiedRows.empty())
	{
		OnUnSelectAll();
		for (auto row : modifiedRows)
			row->SetSelected(true);
	}

	for (auto item : rowsToRevert)
	{
		if (GetModifications().Revert(item.second, false))
		{
			update = true;
		}
		else
		{
			ASSERT(false);
		}
	}

	if (update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridPrivateChannelMembers::OnUpdateRemoveMember(CCmdUI* pCmdUI)
{
	const bool hasOutOfScopePrivilege = true;//IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE);

	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, hasOutOfScopePrivilege](auto& p_Row)
		{
			auto ref = nullptr != p_Row ? p_Row->GetTopAncestor() : nullptr;
			return nullptr != ref
				&& !p_Row->IsGroupRow()
				&& !p_Row->IsDeleted()
				&& (!p_Row->HasField(m_ColMemberRoles) || wstring(_YTEXT("owner")) != p_Row->GetField(m_ColMemberRoles).GetValueStr())
				//&& IsGroupAuthorizedByRoleDelegation(ref, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT)
				&& (hasOutOfScopePrivilege || IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridPrivateChannelMembers::OnMakeOwner()
{
	const auto privilege = 0/*getPrivilege()*/;
	const bool hasOutOfScopePrivilege = true;// IsAuthorizedByRoleDelegation(getOutOfScopePrivilege());
	bool update = false;

	map<GridBackendRow*, row_pk_t> rowsToRevert;
	std::set<GridBackendRow*> modifiedRows;
	for (auto row : GetSelectedRows())
	{
		ASSERT(nullptr != row);

		if (nullptr != row && !row->IsGroupRow() && nullptr != row->GetParentRow())
		{
			//if (IsGroupAuthorizedByRoleDelegation(row->GetParentRow(), privilege))
			{
				DeletedObjectModification* mainMod = nullptr;
				vector<GridBackendRow*>* currentRevertList = nullptr;
				{
					ASSERT(GridUtil::IsBusinessAADUserConversationMember(row));

					if ((!row->HasField(m_ColMemberRoles) || wstring(_YTEXT("owner")) != row->GetField(m_ColMemberRoles).GetValueStr())
						&& !row->IsDeleted()
						&& (hasOutOfScopePrivilege
							|| IsUserAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)))
					{
						const bool hadField = row->HasField(m_ColMemberRoles);
						const GridBackendField oldField = row->GetField(m_ColMemberRoles);
						const GridBackendField& newField = row->AddField(_YTEXT("owner"), m_ColMemberRoles);

						const GridBackendField oldFieldMT = row->GetField(m_ColMemberMembershipType);
						row->AddField(_YTEXT("owner"), m_ColMemberMembershipType);

						row_pk_t rowPk;
						GetRowPK(row, rowPk);

						ASSERT(nullptr == GetModifications().GetRowModification<DeletedObjectModification>(rowPk));

						if (newField.IsModified())
						{
							// Are we editing a created object?
							auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPk);
							if (nullptr != mod
								&& !Modification::IsSentAndReceived()(mod))
							{
								CreatedObjectModification newMod(*this, rowPk);
								newMod.DontRevertOnDestruction();
								*mod = newMod;
							}
							else
							{
								if (oldField.IsModified())
								{
									ASSERT(nullptr != GetModifications().GetRowModification<RowFieldUpdates<Scope::O365>>(rowPk));
									rowsToRevert[row] = rowPk;
								}
								else
								{
									auto update = std::make_unique<SisterhoodFieldUpdate>(
										*this,
										rowPk,
										m_ColMemberRoles->GetID(),
										hadField ? oldField : GridBackendField::g_GenericConstField,
										newField,
										std::map<UINT, const GridBackendField*>{ { m_ColMemberMembershipType->GetID(), &oldFieldMT } }
										);
									GetModifications().Add(std::move(update));
								}
							}

							modifiedRows.insert(row);
						}
					}
				}
			}
		}
	}

	for (auto item : rowsToRevert)
	{
		if (GetModifications().Revert(item.second, false))
		{
			update = true;
		}
		else
		{
			ASSERT(false);
		}
	}

	if (!modifiedRows.empty())
	{
		OnUnSelectAll();
		for (auto row : modifiedRows)
			row->SetSelected(true);
	}

	if (update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridPrivateChannelMembers::OnUpdateMakeOwner(CCmdUI* pCmdUI)
{
	const bool hasOutOfScopePrivilege = true;//IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE);

	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, hasOutOfScopePrivilege](auto& p_Row)
		{
			auto ref = nullptr != p_Row ? p_Row->GetTopAncestor() : nullptr;
			return nullptr != ref
				&& !p_Row->IsGroupRow()
				&& !p_Row->IsDeleted()
				&& (!p_Row->HasField(m_ColMemberRoles) || wstring(_YTEXT("")) == p_Row->GetField(m_ColMemberRoles).GetValueStr())
				//&& IsGroupAuthorizedByRoleDelegation(ref, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT)
				&& (hasOutOfScopePrivilege || IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridPrivateChannelMembers::OnMakeMember()
{
	const auto privilege = 0/*getPrivilege()*/;
	const bool hasOutOfScopePrivilege = true;// IsAuthorizedByRoleDelegation(getOutOfScopePrivilege());
	bool update = false;

	map<GridBackendRow*, row_pk_t> rowsToRevert;
	std::set<GridBackendRow*> modifiedRows;
	for (auto row : GetSelectedRows())
	{
		ASSERT(nullptr != row);

		if (nullptr != row && !row->IsGroupRow() && nullptr != row->GetParentRow())
		{
			//if (IsGroupAuthorizedByRoleDelegation(row->GetParentRow(), privilege))
			{
				DeletedObjectModification* mainMod = nullptr;
				vector<GridBackendRow*>* currentRevertList = nullptr;
				{
					ASSERT(GridUtil::IsBusinessAADUserConversationMember(row));

					if ((row->HasField(m_ColMemberRoles) && wstring(_YTEXT("owner")) == row->GetField(m_ColMemberRoles).GetValueStr())
						&& !row->IsDeleted()
						&& (hasOutOfScopePrivilege
							|| IsUserAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE)))
					{
						const bool hadField = row->HasField(m_ColMemberRoles);
						const GridBackendField oldField = row->GetField(m_ColMemberRoles);
						const GridBackendField& newField = row->AddField(_YTEXT(""), m_ColMemberRoles);

						const GridBackendField oldFieldMT = row->GetField(m_ColMemberMembershipType);
						row->AddField(_YTEXT("member"), m_ColMemberMembershipType);

						row_pk_t rowPk;
						GetRowPK(row, rowPk);

						ASSERT(nullptr == GetModifications().GetRowModification<DeletedObjectModification>(rowPk));

						if (newField.IsModified())
						{
							// Are we editing a created object?
							auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPk);
							if (nullptr != mod)
							{
								CreatedObjectModification newMod(*this, rowPk);
								newMod.DontRevertOnDestruction();
								*mod = newMod;
							}
							else
							{
								if (oldField.IsModified())
								{
									ASSERT(nullptr != GetModifications().GetRowModification<RowFieldUpdates<Scope::O365>>(rowPk));
									rowsToRevert[row] = rowPk;
								}
								else
								{
									auto update = std::make_unique<SisterhoodFieldUpdate>(
										*this,
										rowPk,
										m_ColMemberRoles->GetID(),
										hadField ? oldField : GridBackendField::g_GenericConstField,
										newField,
										std::map<UINT, const GridBackendField*>{ { m_ColMemberMembershipType->GetID(), &oldFieldMT } });
									GetModifications().Add(std::move(update));
								}
							}

							modifiedRows.insert(row);
						}
					}
				}
			}
		}
	}

	for (auto item : rowsToRevert)
	{
		if (GetModifications().Revert(item.second, false))
		{
			update = true;
		}
		else
		{
			ASSERT(false);
		}
	}

	if (!modifiedRows.empty())
	{
		OnUnSelectAll();
		for (auto row : modifiedRows)
			row->SetSelected(true);
	}

	if (update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridPrivateChannelMembers::OnUpdateMakeMember(CCmdUI* pCmdUI)
{
	const bool hasOutOfScopePrivilege = true;//IsAuthorizedByRoleDelegation(RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT_OUT_OF_SCOPE);

	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, hasOutOfScopePrivilege](auto& p_Row)
		{
			auto ref = nullptr != p_Row ? p_Row->GetTopAncestor() : nullptr;
			return nullptr != ref
				&& !p_Row->IsGroupRow()
				&& !p_Row->IsDeleted()
				&& p_Row->HasField(m_ColMemberRoles) && wstring(_YTEXT("owner")) == p_Row->GetField(m_ColMemberRoles).GetValueStr()
				//&& IsGroupAuthorizedByRoleDelegation(ref, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_OWNERS_EDIT)
				&& (hasOutOfScopePrivilege || IsUserAuthorizedByRoleDelegation(p_Row, RoleDelegationUtil::RBAC_Privilege::RBAC_FAKE_PRIVILEGE_IS_IN_SCOPE));
		}))
		enable = TRUE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridPrivateChannelMembers::InitializeCommands()
{
	/*std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);*/

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_CHANNELMEMBERSGRID_ADD;
		_cmd.m_sMenuText = _T("Add Member");
		_cmd.m_sToolbarText = _T("Add Member");
		_cmd.m_sAccelText = _YTEXT("");//MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd, true);

		auto hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANNELMEMBERSGRID_ADD_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CHANNELMEMBERSGRID_ADD, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Add to Channel"),
			_T("Add members to all selected channels.\nYou will be able to select users or groups from a directory list."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_CHANNELMEMBERSGRID_REMOVE;
		_cmd.m_sMenuText = _T("Remove Member");
		_cmd.m_sToolbarText = _T("Remove Member");
		_cmd.m_sAccelText = _YTEXT("");//MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd, true);

		auto hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANNELMEMBERSGRID_REMOVE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CHANNELMEMBERSGRID_REMOVE, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Remove Selected Channel Memberships"),
			_T("Remove all selected channel memberships. Clicking this button will add a trash can icon in the status column. To save your changes to the server, use one of the Save buttons."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_CHANNELMEMBERSGRID_MAKEOWNER;
		_cmd.m_sMenuText = _T("Promote to Owner");
		_cmd.m_sToolbarText = _T("Promote to Owner");
		_cmd.m_sAccelText = _YTEXT("");//MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd, true);

		auto hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANNELMEMBERSGRID_MAKEOWNER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CHANNELMEMBERSGRID_MAKEOWNER, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Promote to Owner"),
			_T("Promote the selected members to owner of their channel."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_CHANNELMEMBERSGRID_MAKEMEMBER;
		_cmd.m_sMenuText = _T("Demote to Member");
		_cmd.m_sToolbarText = _T("Demote to Member");
		_cmd.m_sAccelText = _YTEXT("");//MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd, true);

		auto hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANNELMEMBERSGRID_MAKEMEMBER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CHANNELMEMBERSGRID_MAKEMEMBER, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Demote to Member"),
			_T("Demote the selected owners to member of their channel."),
			_cmd.m_sAccelText);
	}

	O365Grid::InitializeCommands();
}

void GridPrivateChannelMembers::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_CHANNELMEMBERSGRID_ADD);
		pPopup->ItemInsert(ID_CHANNELMEMBERSGRID_REMOVE);
		pPopup->ItemInsert(ID_CHANNELMEMBERSGRID_MAKEOWNER);
		pPopup->ItemInsert(ID_CHANNELMEMBERSGRID_MAKEMEMBER);
		pPopup->ItemInsert(); // Sep
	}
	O365Grid::OnCustomPopupMenu(pPopup, nStart);
}

void GridPrivateChannelMembers::getUserIDs(O365IdsContainer& p_IDs, bool p_Selected, RoleDelegationUtil::RBAC_Privilege p_Privilege) const
{
	p_IDs.clear();

	ASSERT(nullptr != m_ColMemberUserID);
	if (nullptr != m_ColMemberUserID)
	{
		const auto session = GetSession();
		const bool roleDelegation = session && session->IsUseRoleDelegation();

		const vector<GridBackendRow*>& rows = p_Selected ? GetBackend().GetSelectedRowsInOrder() : GetBackend().GetBackendRowsForDisplay();

		for (auto row : rows)
		{
			ASSERT(nullptr != row);
			if (GridUtil::IsBusinessAADUserConversationMember(row))
			{
				const GridBackendField& f = row->GetField(m_ColMemberUserID);
				ASSERT(f.HasValue());
				if (f.HasValue())
				{
					wstring id(f.GetValueStr());
					if (!id.empty())
					{
						// FIXME: Should an AADUserConversationMember be a deleted user ?
						if (!roleDelegation || IsUserAuthorizedByRoleDelegation(id, false, p_Privilege))
							p_IDs.insert(f.GetValueStr());
					}
				}
			}
		}
	}
}

vector<BusinessUser> GridPrivateChannelMembers::getUsers() const
{
	vector<BusinessUser> users;

	CWaitCursor _;
	for (auto& row : GetBackendRows())
	{
		if (!row->IsExplosionAdded())
		{
			if (GridUtil::IsBusinessAADUserConversationMember(row))
			{
				BusinessUser user;

				{
					const auto& field = row->GetField(m_ColMemberUserID);
					if (field.HasValue())
					{
						user.SetID(field.GetValueStr());
						user.SetRBACDelegationID(GetGraphCache().GetUserInCache(user.GetID(), false).GetRBACDelegationID());
					}
				}
				{
					const auto& field = row->GetField(m_ColMemberDisplayName);
					if (field.HasValue())
						user.SetDisplayName(PooledString(field.GetValueStr()));
				}
				{
					const auto& field = row->GetField(m_ColMemberEmail);
					if (field.HasValue())
						user.SetUserPrincipalName(PooledString(field.GetValueStr()));
				}

				user.SetUserType(PooledString(BusinessUser::g_UserTypeMember));

				if (users.end() == std::find_if(users.begin(), users.end(), [&user](const BusinessUser& bu) { return bu.GetID() == user.GetID(); }))
					users.emplace_back(user);
			}
		}
	}

	return users;
}

BusinessAADUserConversationMember GridPrivateChannelMembers::getMember(GridBackendRow* p_Row) const
{
	ASSERT(GridUtil::IsBusinessAADUserConversationMember(p_Row));

	BusinessAADUserConversationMember member;

	if (p_Row->HasField(GetColId()))
		member.SetID(PooledString(p_Row->GetField(GetColId()).GetValueStr()));

	if (p_Row->HasField(m_ColMemberUserID))
		member.SetUserID(PooledString(p_Row->GetField(m_ColMemberUserID).GetValueStr()));

	if (p_Row->HasField(m_ColMemberRoles))
	{
		const auto& field = p_Row->GetField(m_ColMemberRoles);
		ASSERT(!field.IsMulti());

		member.SetRoles(vector<PooledString>{ field.GetValueStr() });
	}

	// FIXME: Do we need more info?

	return member;
}

void GridPrivateChannelMembers::BuildView(const vector<BusinessChannel>& p_Channels, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge)
{
	const uint32_t options = GridUpdaterOptions::UPDATE_GRID
		| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
		| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		/*| GridUpdaterOptions::REFRESH_PROCESS_DATA*/
		/*| GridUpdaterOptions::PROCESS_LOADMORE*/
		;

	GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateOperations);
	GridIgnoreModification ignoramus(*this);

	for (const auto& channel : p_Channels)
	{
		ASSERT(channel.GetGroupId());

		auto channelRow = GetRowIndex().GetRow({ GridBackendField(channel.GetID(), m_ColChannelID), GridBackendField(channel.GetID(), GetColId()) }, true, true);
		ASSERT(nullptr != channelRow);

		if (nullptr != channelRow)
		{
			updater.GetOptions().AddRowWithRefreshedValues(channelRow);
			updater.GetOptions().AddPartialPurgeParentRow(channelRow);
			SetRowObjectType(channelRow, channel, channel.HasFlag(BusinessObject::CANCELED));
			if (channel.GetGroupId())
			{
				auto cachedGroup = GetGraphCache().GetGroupInCache(*channel.GetGroupId());
				ASSERT(cachedGroup.GetID() == *channel.GetGroupId());
				channelRow->AddField(cachedGroup.GetDisplayName(), m_ColTeamDisplayName);
			}
			channelRow->AddField(channel.GetGroupId(), m_ColTeamID);
			channelRow->AddField(channel.GetDisplayName(), m_ColChannelDisplayName);

			if (channel.GetPrivateChannelMembers())
			{
				for (auto& member : *channel.GetPrivateChannelMembers())
				{
					auto memberRow = GetRowIndex().GetRow({ GridBackendField(channel.GetID(), m_ColChannelID), GridBackendField(member.GetID(), GetColId()) }, true, true);
					ASSERT(nullptr != memberRow);
					if (nullptr != memberRow)
					{
						updater.GetOptions().AddRowWithRefreshedValues(memberRow);
						SetRowObjectType(memberRow, member, member.HasFlag(BusinessObject::CANCELED));
						memberRow->AddField(channelRow->GetField(m_ColTeamDisplayName));
						memberRow->AddField(channelRow->GetField(m_ColTeamID));
						memberRow->AddField(channelRow->GetField(m_ColChannelDisplayName));

						memberRow->AddField(member.GetDisplayName(), m_ColMemberDisplayName);
						memberRow->AddField(member.GetUserID(), m_ColMemberUserID);
						memberRow->AddField(member.GetEmail(), m_ColMemberEmail);
						const auto& roleField = memberRow->AddField(member.GetRoles(), m_ColMemberRoles);
						if (!roleField.HasValue() || roleField.IsString() && PooledString(roleField.GetValueStr()).IsEmpty())
							memberRow->AddField(_YTEXT("member"), m_ColMemberMembershipType);
						else
							memberRow->AddField(roleField, m_ColMemberMembershipType->GetID());

						memberRow->SetParentRow(channelRow);
					}
				}
			}
		}
	}
}

wstring GridPrivateChannelMembers::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { { _T("Team"),  m_ColTeamDisplayName}, { _T("Channel"), m_ColChannelDisplayName } }, m_ColMemberDisplayName);
}

ModuleCriteria GridPrivateChannelMembers::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
	GridFrameBase* frame = dynamic_cast<GridFrameBase*>(GetParent());
	ASSERT(nullptr != frame);
	if (nullptr != frame)
	{
		ModuleCriteria criteria = frame->GetModuleCriteria();
		criteria.m_SubIDs.clear();

		for (auto row : p_Rows)
		{
			ASSERT(GridUtil::IsBusinessChannel(row));
			if (GridUtil::IsBusinessChannel(row))
				criteria.m_SubIDs[PooledString(row->GetField(m_ColTeamID).GetValueStr())].insert(row->GetField(m_ColChannelID).GetValueStr());
		}

		return criteria;
	}

	return ModuleCriteria();
}

GridPrivateChannelMembers::Changes GridPrivateChannelMembers::GetChanges(const std::set<GridBackendRow*>& p_Parents)
{
	Changes changes;

	const bool all = p_Parents.empty();

	for (auto& rowMod : GetModifications().GetRowFieldModifications())
	{
		auto row = GetRowIndex().GetExistingRow(rowMod->GetRowKey());
		ASSERT(GridUtil::IsBusinessAADUserConversationMember(row));

		if (nullptr != row && (all || p_Parents.end() != p_Parents.find(row->GetTopAncestor())))
		{
			changes.Add(Changes::MemberRoleUpdate{ rowMod->GetRowKey(), row->GetField(m_ColTeamID).GetValueStr(), row->GetField(m_ColChannelID).GetValueStr(), getMember(row) });
		}
	}

	for (auto& rowMod : GetModifications().GetRowModifications())
	{
		if (nullptr != dynamic_cast<CreatedObjectModification*>(rowMod.get()))
		{
			auto row = GetRowIndex().GetExistingRow(rowMod->GetRowKey());
			ASSERT(GridUtil::IsBusinessAADUserConversationMember(row));

			if (nullptr != row && (all || p_Parents.end() != p_Parents.find(row->GetTopAncestor())))
				changes.Add(Changes::MemberAddition{ rowMod->GetRowKey(), row->GetField(m_ColTeamID).GetValueStr(), row->GetField(m_ColChannelID).GetValueStr(), getMember(row) });
		}
		else if (nullptr != dynamic_cast<DeletedObjectModification*>(rowMod.get()))
		{
			auto row = GetRowIndex().GetExistingRow(rowMod->GetRowKey());
			ASSERT(GridUtil::IsBusinessAADUserConversationMember(row));

			if (nullptr != row && (all || p_Parents.end() != p_Parents.find(row->GetTopAncestor())))
				changes.Add(Changes::MemberRemoval{ rowMod->GetRowKey(), row->GetField(m_ColTeamID).GetValueStr(), row->GetField(m_ColChannelID).GetValueStr(), row->GetField(GetColId()).GetValueStr() });
		}
		else
		{
			ASSERT(false);
		}
	}

	return changes;
}

row_pk_t GridPrivateChannelMembers::UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody)
{
	ASSERT(IsGridModificationsEnabled());
	if (IsGridModificationsEnabled())
	{
		auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPk);
		ASSERT(nullptr != mod);
		if (nullptr != mod)
		{
			wstring channelId;
			auto row = GetRowIndex().GetExistingRow(rowPk);
			ASSERT(nullptr != row);
			if (nullptr != row)
				channelId = row->GetField(m_ColChannelID).GetValueStr();

			// Check Create call that generated this data: Need to request RBAC props?
			BusinessAADUserConversationMemberDeserializer deserializer;
			deserializer.Deserialize(json::value::parse(creationResultBody));
			const auto& member = deserializer.GetData();
			row_pk_t newPk{ GridBackendField(channelId, m_ColChannelID), GridBackendField(member.GetID(), GetColId()) };
			mod->SetNewPK(newPk);

			return newPk;
		}
	}

	return rowPk;
}