#include "DlgModuleOptions.h"

#include "AutomatedApp.h"
#include "AutomationEngine.h"
#include "AutomationNames.h"
#include "JsonSerializeUtil.h"
#include "RoleDelegationManager.h"
#include "YCodeJockMessageBox.h"

#include <boost/assign.hpp>

namespace
{
	// <odatafilter, filterbuilder>
	static const boost::bimap<wstring, wstring> g_FilterOperators = boost::assign::list_of< boost::bimap<wstring, wstring>::relation >
		(_YTEXT("eq"), _YTEXT("equal"))
		(_YTEXT("ne"), _YTEXT("not_equal"))
		(_YTEXT("gt"), _YTEXT("greater"))
		(_YTEXT("ge"), _YTEXT("greater_or_equal"))
		(_YTEXT("lt"), _YTEXT("less"))
		(_YTEXT("le"), _YTEXT("less_or_equal"))
		(_YTEXT("startsWith"), _YTEXT("begins_with"))
		;

	bool IsOpeningParenthese(const ODataFilter::FilterBlock& p_FilterBlock)
	{
		return ODataFilter::FilterBlock::Parenthese == p_FilterBlock.GetType() && _YTEXT("(") == p_FilterBlock.GetOperator();
	}

	bool IsClosingParenthese(const ODataFilter::FilterBlock& p_FilterBlock)
	{
		return ODataFilter::FilterBlock::Parenthese == p_FilterBlock.GetType() && _YTEXT(")") == p_FilterBlock.GetOperator();
	}

	class FilterBuilderJson
	{
	public:
		FilterBuilderJson(const ODataFilter& p_ODataFilter, std::map<wstring, DlgModuleOptions::FilterFieldProperties> p_Fields)
			: m_Fields(p_Fields)
			, m_Json(getFilterObject(p_ODataFilter.GetFilterBlocks()))
		{
		}
		const web::json::value& GetJson() const
		{
			return m_Json;
		}
		operator const web::json::value& () const
		{
			return GetJson();
		}

	protected:
		web::json::value getFilterObject(const vector<ODataFilter::FilterBlock>& p_FilterBlocks, size_t& p_Index)
		{
			web::json::value jsonObject = web::json::value::object();

			vector<web::json::value> rules;
			wstring condition; // and, or
			while (p_Index < p_FilterBlocks.size())
			{
				const auto& fb = p_FilterBlocks[p_Index];
				if (IsOpeningParenthese(fb))
				{
					rules.push_back(getFilterObject(p_FilterBlocks, ++p_Index));
				}
				else if (IsClosingParenthese(fb))
				{
					break;
				}
				else if (ODataFilter::FilterBlock::Logical == fb.GetType())
				{
					if (fb.GetOperator() == _YTEXT("and"))
					{
						ASSERT(condition.empty() || condition == _YTEXT("AND"));
						condition = _YTEXT("AND");
					}
					else if (fb.GetOperator() == _YTEXT("or"))
					{
						ASSERT(condition.empty() || condition == _YTEXT("OR"));
						condition = _YTEXT("OR");
					}
					else
					{
						ASSERT(false);
					}
				}
				else if (ODataFilter::FilterBlock::Comparison == fb.GetType())
				{
					auto rule = web::json::value::object();
					rule[_YTEXT("id")] = web::json::value::string(fb.GetField());
					rule[_YTEXT("operator")] = web::json::value::string(g_FilterOperators.left.at(fb.GetOperator()));

					// For boolean, we need to use web::json::value::boolean() instead of web::json::value::string

					bool isBool = false, isInteger = false;
					{
						auto it = m_Fields.find(fb.GetField());
						ASSERT(m_Fields.end() != it);
						if (m_Fields.end() != it)
						{
							// Only way to identify the type is to serialize it in json.
							auto v = web::json::value::object();
							it->second.m_Type.FillJson(v);

							if (v.has_field(_YTEXT("type")))
							{
								auto type = v[_YTEXT("type")].as_string();
								isBool = type == _YTEXT("boolean");
								isInteger = type == _YTEXT("integer");
							}
						}
					}

					if (isBool)
					{
						ASSERT(_YTEXT("true") == fb.GetValue() || _YTEXT("false") == fb.GetValue());
						rule[_YTEXT("value")] = web::json::value::boolean(_YTEXT("true") == fb.GetValue());
					}
					else if (isInteger)
					{
						rule[_YTEXT("value")] = Str::GetNumber(fb.GetValue());
					}
					else
					{
						rule[_YTEXT("value")] = web::json::value::string(fb.GetValue());
					}

					rules.push_back(rule);
				}
				else if (ODataFilter::FilterBlock::StartsWith == fb.GetType())
				{
					auto rule = web::json::value::object();
					rule[_YTEXT("id")] = web::json::value::string(fb.GetField());
					rule[_YTEXT("operator")] = web::json::value::string(g_FilterOperators.left.at(_YTEXT("startsWith")));
					rule[_YTEXT("value")] = web::json::value::string(fb.GetValue());
					rules.push_back(rule);
				}

				++p_Index;
			}

			if (!rules.empty())
			{
				if (condition.empty())
					condition = _YTEXT("AND");
				jsonObject[_YTEXT("condition")] = web::json::value::string(condition);
				jsonObject[_YTEXT("rules")] = web::json::value::array(rules);
			}

			if (jsonObject.as_object().empty())
				jsonObject = web::json::value::null();
			return jsonObject;
		}

		web::json::value getFilterObject(const vector<ODataFilter::FilterBlock>& p_FilterBlocks)
		{
			size_t i = 0;
			return getFilterObject(p_FilterBlocks, i);
		}

	private:
		const std::map<wstring, DlgModuleOptions::FilterFieldProperties> m_Fields;
		const web::json::value m_Json;
	};
}

class DlgModuleOptions::FilterParser
{
public:
	FilterParser(const DlgModuleOptions& p_Dialog, const wstring& p_FilterString)
		: m_Dialog(p_Dialog)
		, m_FilterString(p_FilterString)
		, m_Fields(p_Dialog.getFilterFields())
	{

	}

	ODataFilter operator()() const
	{
		ODataFilter filter;

		try
		{
			auto val = web::json::value::parse(m_FilterString);
			ASSERT(val.is_object());
			if (val.is_object())
			{
				boost::YOpt<bool> isValid;
				const auto& obj = val.as_object();
				JsonSerializeUtil::DeserializeBool(_YTEXT("valid"), isValid, obj);
				if (isValid && *isValid)
					add(filter, nullptr, deserialize(obj));
			}
		}
		catch (web::json::json_exception&)
		{
			ASSERT(false);
		}

		return filter;
	}

private:
	struct FilterRule
	{
		wstring m_id;
		wstring m_field;
		wstring m_type;
		wstring m_input;
		wstring m_operator;
		wstring m_value;
	};

	struct FilterElement;
	struct FilterGroup
	{
		wstring m_Condition;
		vector<std::unique_ptr<FilterElement>> m_Rules;
	};

	struct FilterElement
	{
		std::unique_ptr<FilterRule> m_Rule;
		std::unique_ptr<FilterGroup> m_Group;
	};

private:
	std::unique_ptr<FilterElement> deserialize(const web::json::object& p_Obj) const
	{
		auto element = std::make_unique<FilterElement>();
		wstring condition;
		JsonSerializeUtil::DeserializeString(_YTEXT("condition"), condition, p_Obj);
		if (!condition.empty())
		{
			// real group
			element->m_Group = std::make_unique<FilterGroup>();
			element->m_Group->m_Condition = condition;

			auto itt = p_Obj.find(_YTEXT("rules"));
			if (itt != p_Obj.end() && itt->second.is_array())
			{
				const auto& rulesArray = itt->second.as_array();
				for (auto& r : rulesArray)
				{
					ASSERT(r.is_object());
					element->m_Group->m_Rules.push_back(deserialize(r.as_object()));
				}
			}
		}
		else
		{
			// rule
			element->m_Rule = std::make_unique<FilterRule>();
			JsonSerializeUtil::DeserializeString(_YTEXT("id"), element->m_Rule->m_id, p_Obj);
			JsonSerializeUtil::DeserializeString(_YTEXT("field"), element->m_Rule->m_field, p_Obj);
			JsonSerializeUtil::DeserializeString(_YTEXT("type"), element->m_Rule->m_type, p_Obj);
			JsonSerializeUtil::DeserializeString(_YTEXT("input"), element->m_Rule->m_input, p_Obj);
			JsonSerializeUtil::DeserializeString(_YTEXT("operator"), element->m_Rule->m_operator, p_Obj);
			
			if (element->m_Rule->m_type == _YTEXT("boolean"))
			{
				ASSERT(element->m_Rule->m_input == _YTEXT("radio"));
				boost::YOpt<bool> value;
				JsonSerializeUtil::DeserializeBool(_YTEXT("value"), value, p_Obj);
				ASSERT(value);
				if (value)
					element->m_Rule->m_value = *value ? _YTEXT("true") : _YTEXT("false");
			}
			else if (element->m_Rule->m_type == _YTEXT("datetime"))
			{
				JsonSerializeUtil::DeserializeString(_YTEXT("value"), element->m_Rule->m_value, p_Obj);
				auto ytd = DlgFormsHTML::DateTimeFromText(element->m_Rule->m_value);

				ASSERT(ytd);
				if (ytd)
					element->m_Rule->m_value = TimeUtil::GetInstance().GetISO8601String(*ytd, TimeUtil::ISO8601_HAS_DATE_AND_TIME);
			}
			else if (element->m_Rule->m_type == _YTEXT("date"))
			{
				JsonSerializeUtil::DeserializeString(_YTEXT("value"), element->m_Rule->m_value, p_Obj);
				auto ytd = DlgFormsHTML::DateTimeFromText(element->m_Rule->m_value);

				ASSERT(ytd);
				if (ytd)
					element->m_Rule->m_value = TimeUtil::GetInstance().GetISO8601String(*ytd, TimeUtil::ISO8601_HAS_DATE);
			}
			else if (element->m_Rule->m_type == _YTEXT("integer"))
			{
				boost::YOpt<int32_t> value;
				JsonSerializeUtil::DeserializeInt32(_YTEXT("value"), value, p_Obj);
				ASSERT(value);
				if (value)
					element->m_Rule->m_value = std::to_wstring(*value);
			}
			else
			{
				ASSERT(element->m_Rule->m_type == _YTEXT("string") || element->m_Rule->m_type == _YTEXT("enum"));
				JsonSerializeUtil::DeserializeString(_YTEXT("value"), element->m_Rule->m_value, p_Obj);
			}
		}

		return std::move(element);
	}

	void addRule(ODataFilter& p_OdataFilter, const std::unique_ptr<FilterRule>& p_Rule) const
	{
		if (_YTEXT("equal") == p_Rule->m_operator)
		{
			ASSERT(m_Fields.end() != m_Fields.find(p_Rule->m_id));
			const auto needsQuotes = m_Fields.end() != m_Fields.find(p_Rule->m_id) && m_Fields.at(p_Rule->m_id).m_AddQuotes;
			p_OdataFilter.Equal(p_Rule->m_id, p_Rule->m_value, needsQuotes);
		}
		else if (_YTEXT("not_equal") == p_Rule->m_operator)
		{
			ASSERT(m_Fields.end() != m_Fields.find(p_Rule->m_id));
			const auto needsQuotes = m_Fields.end() != m_Fields.find(p_Rule->m_id) && m_Fields.at(p_Rule->m_id).m_AddQuotes;
			p_OdataFilter.NotEqual(p_Rule->m_id, p_Rule->m_value, needsQuotes);
		}
		else if (_YTEXT("greater") == p_Rule->m_operator)
		{
			ASSERT(m_Fields.end() != m_Fields.find(p_Rule->m_id));
			const auto needsQuotes = m_Fields.end() != m_Fields.find(p_Rule->m_id) && m_Fields.at(p_Rule->m_id).m_AddQuotes;
			p_OdataFilter.GreaterThan(p_Rule->m_id, p_Rule->m_value, needsQuotes);
		}
		else if (_YTEXT("greater_or_equal") == p_Rule->m_operator)
		{
			ASSERT(m_Fields.end() != m_Fields.find(p_Rule->m_id));
			const auto needsQuotes = m_Fields.end() != m_Fields.find(p_Rule->m_id) && m_Fields.at(p_Rule->m_id).m_AddQuotes;
			p_OdataFilter.GreaterThanOrEquals(p_Rule->m_id, p_Rule->m_value, needsQuotes);
		}
		else if (_YTEXT("less") == p_Rule->m_operator)
		{
			ASSERT(m_Fields.end() != m_Fields.find(p_Rule->m_id));
			const auto needsQuotes = m_Fields.end() != m_Fields.find(p_Rule->m_id) && m_Fields.at(p_Rule->m_id).m_AddQuotes;
			p_OdataFilter.LessThan(p_Rule->m_id, p_Rule->m_value, needsQuotes);
		}
		else if (_YTEXT("less_or_equal") == p_Rule->m_operator)
		{
			ASSERT(m_Fields.end() != m_Fields.find(p_Rule->m_id));
			const auto needsQuotes = m_Fields.end() != m_Fields.find(p_Rule->m_id) && m_Fields.at(p_Rule->m_id).m_AddQuotes;
			p_OdataFilter.LessThanOrEquals(p_Rule->m_id, p_Rule->m_value, needsQuotes);
		}
		else if (_YTEXT("begins_with") == p_Rule->m_operator)
		{
			p_OdataFilter.StartsWith(p_Rule->m_id, p_Rule->m_value);
		}
	}

	void add(ODataFilter& p_OdataFilter, const std::unique_ptr<FilterGroup>& p_ParentGroup, const std::unique_ptr<FilterElement>& p_Element) const
	{
		if (p_ParentGroup && !(p_OdataFilter.IsEmpty() || IsOpeningParenthese(p_OdataFilter.GetFilterBlocks().back())))
		{
			if (p_ParentGroup->m_Condition == _YTEXT("AND"))
				p_OdataFilter.And();
			else if (p_ParentGroup->m_Condition == _YTEXT("OR"))
				p_OdataFilter.Or();
			else
				ASSERT(false);
		}

		if (p_Element->m_Group)
		{
			const bool parentheses = !p_OdataFilter.IsEmpty();
			if (parentheses)
				p_OdataFilter.OpenParenthese();
			for (auto& r : p_Element->m_Group->m_Rules)
				add(p_OdataFilter, p_Element->m_Group, r);
			if (parentheses)
				p_OdataFilter.CloseParenthese();
		}
		else if (p_Element->m_Rule)
		{
			addRule(p_OdataFilter, p_Element->m_Rule);
		}
		else
		{
			ASSERT(false);
		}
	}

private:
	const DlgModuleOptions& m_Dialog;
	const wstring& m_FilterString;
	std::map<wstring, FilterFieldProperties> m_Fields;
};

//const wstring DlgModuleOptions::g_AutoParamMessageType = _YTEXT("MessageType");
//const wstring DlgModuleOptions::g_AutoParamEmailsAndChats = _YTEXT("EmailsAndChats");
const wstring DlgModuleOptions::g_AutoParamEmails			= _YTEXT("Emails");
const wstring DlgModuleOptions::g_AutoParamChats			= _YTEXT("Chats");

const wstring DlgModuleOptions::g_AutoParamOnlineInPlaceArchive  = _YTEXT("OnlineInPlaceArchive");
const wstring DlgModuleOptions::g_AutoParamClutterFolders		= _YTEXT("ClutterFolders");
const wstring DlgModuleOptions::g_AutoParamSoftDeletedFolders	= _YTEXT("SoftDeletedfolders");
const wstring DlgModuleOptions::g_AutoParamScheduledFolders		= _YTEXT("ScheduledFolders");
const wstring DlgModuleOptions::g_AutoParamSearchFolders			= _YTEXT("SearchFolders");

const wstring DlgModuleOptions::g_AutoParamBodyPreview		= _YTEXT("BodyPreview");
const wstring DlgModuleOptions::g_AutoParamFullBody			= _YTEXT("FullBody");
const wstring DlgModuleOptions::g_AutoParamMailHeaders		= _YTEXT("MailHeaders");
const wstring DlgModuleOptions::g_AutoParamFilterBuilder	= _YTEXT("FilterBuilder");
const wstring DlgModuleOptions::g_AutoParamFilter			= _YTEXT("Filter");

const wstring DlgModuleOptions::g_CutOffDate = _YTEXT("cutOffDate");
const wstring DlgModuleOptions::g_CutOffType = _YTEXT("cutOffType");

const vector<wstring> DlgModuleOptions::g_FilterOperatorsNoEQ{ _YTEXT("ne"), _YTEXT("gt"), _YTEXT("ge"), _YTEXT("lt"), _YTEXT("le"), _YTEXT("startsWith") };
const vector<wstring> DlgModuleOptions::g_FilterOperatorsNoEQNE{ _YTEXT("gt"), _YTEXT("ge"), _YTEXT("lt"), _YTEXT("le"), _YTEXT("startsWith") };
const vector<wstring> DlgModuleOptions::g_FilterOperatorsNoStartWith{ _YTEXT("eq"), _YTEXT("ne"), _YTEXT("gt"), _YTEXT("ge"), _YTEXT("lt"), _YTEXT("le") };
const vector<wstring> DlgModuleOptions::g_FilterOperatorsEQStartsWith{ _YTEXT("eq"), _YTEXT("startsWith") };
const vector<wstring> DlgModuleOptions::g_FilterOperatorsEQ{ _YTEXT("eq")  };
const vector<wstring> DlgModuleOptions::g_FilterOperatorsEQGELE{ _YTEXT("eq"), _YTEXT("ge"), _YTEXT("le") };
const vector<wstring> DlgModuleOptions::g_FilterOperatorsGELE{ _YTEXT("ge"), _YTEXT("le") };


DlgModuleOptions::DlgModuleOptions(CWnd* p_Parent, const std::shared_ptr<Sapio365Session>& p_Session, const ModuleOptions& p_Options/* = ModuleOptions()*/)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent, g_ActionNameModuleOptions)
    , m_UpdateOptions(p_Options.m_ReferenceTimeDate.is_initialized())
	, m_Session(p_Session)
	, m_CurrentMotherAction(nullptr)
	, m_CurrentSetParamTotalCount(0)
	, m_ModuleOpts(p_Options)
{
}

const ModuleOptions& DlgModuleOptions::GetOptions() const
{
	return m_ModuleOpts;
}

void DlgModuleOptions::addCutOffEditor(const wstring& p_TypeLabel, const wstring& p_DateLabel)
{
	static const wstring g_RefDate = _YTEXT("refDate");
	if (m_UpdateOptions)
	{
		ASSERT(m_ModuleOpts.m_ReferenceTimeDate);
		CString value;
		TimeUtil::GetInstance().ConvertDateToText(getGenerator().GetDateTimeFormat(), *m_ModuleOpts.m_ReferenceTimeDate, value);
		getGenerator().Add(StringEditor(g_RefDate, _T("Reference Date (First load)"), EditorFlags::READONLY, (LPCTSTR)value));
	}

	{
		getGenerator().Add(Expanded<AsRadio<CheckListEditor>>({ g_CutOffType, p_TypeLabel, EditorFlags::DIRECT_EDIT, { GetLoadingScopeStr(m_ModuleOpts.m_LoadingScope) } },
				{
					{ GetLoadingScopeFriendlyStr(LoadingScope::All), GetLoadingScopeStr(LoadingScope::All) },
					{ GetLoadingScopeFriendlyStr(LoadingScope::LastHour), GetLoadingScopeStr(LoadingScope::LastHour) },
					{ GetLoadingScopeFriendlyStr(LoadingScope::Last24h), GetLoadingScopeStr(LoadingScope::Last24h) },
					{ GetLoadingScopeFriendlyStr(LoadingScope::LastWeek), GetLoadingScopeStr(LoadingScope::LastWeek) },
					{ GetLoadingScopeFriendlyStr(LoadingScope::LastMonth), GetLoadingScopeStr(LoadingScope::LastMonth)  },
					{ GetLoadingScopeFriendlyStr(LoadingScope::Other), GetLoadingScopeStr(LoadingScope::Other) }
				}));

		wstring defaultDate;
		if (m_UpdateOptions && m_ModuleOpts.m_CutOffDateTime)
		{
			YTimeDate ytd;
			auto res = TimeUtil::GetInstance().GetTimedateFromISO8601String(*m_ModuleOpts.m_CutOffDateTime, ytd, true);
			ASSERT(TimeUtil::ISO8601_HAS_DATE == res || TimeUtil::ISO8601_HAS_DATE_AND_TIME == res);
			if (TimeUtil::ISO8601_HAS_DATE == res || TimeUtil::ISO8601_HAS_DATE_AND_TIME == res)
			{
				CString value;
				TimeUtil::GetInstance().ConvertDateToText(getGenerator().GetDateFormat(), ytd, value);
				defaultDate = (LPCTSTR)value;
			}
		}

		getGenerator().Add(DateEditor({ g_CutOffDate, p_DateLabel, EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, defaultDate }, Str::g_EmptyString));

		{
			EventTarget eventTarget;
			eventTarget.m_TargetName = g_CutOffDate;
			eventTarget.m_TargetCondition = EventTarget::g_DisableIfNotEqual;
			eventTarget.m_TargetValue = GetLoadingScopeStr(LoadingScope::Other);
			getGenerator().addEvent(g_CutOffType, eventTarget);
		}
		if (m_UpdateOptions)
		{
			EventTarget eventTarget;
			eventTarget.m_TargetName = g_RefDate;
			eventTarget.m_TargetCondition = EventTarget::g_HideIfEqual;
			eventTarget.m_TargetValue = GetLoadingScopeStr(LoadingScope::Other);
			getGenerator().addEvent(g_CutOffType, eventTarget);
			eventTarget.m_TargetValue = GetLoadingScopeStr(LoadingScope::All);
			getGenerator().addEvent(g_CutOffType, eventTarget);
		}
		getGenerator().addGlobalEventTrigger(g_CutOffType);
	}
}

void DlgModuleOptions::addFilterEditor(bool p_DirectEdit/* = false*/)
{
	if (!getFilterFields().empty())
	{
		wstring val;
		uint32_t flags = 0;
		const bool automated = AutomationMustClose() && getAutomationValue(g_AutoParamFilter, val, flags);
		if (automated)
		{
			static wstring filterToggle = _YTEXT("FilTog");
			getGenerator().Add(BoolToggleEditor({ { filterToggle, _T("Override provided filter"), EditorFlags::DIRECT_EDIT, false }, { } }));

			getGenerator().addIconTextField(g_AutoParamFilter, val, _T("Advanced filter settings:"), _YTEXT("fas fa-filter"), COLORREF(-1), val, true);
			getGenerator().addEvent(filterToggle, { g_AutoParamFilter, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueTrue });
			getGenerator().addEvent(filterToggle, { g_AutoParamFilterBuilder, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueFalse });
		}
		else if (!p_DirectEdit)
		{
			static wstring filterToggle = _YTEXT("FilTog");
			getGenerator().Add(BoolToggleEditor({ { filterToggle, _T("Use advanced filter"), EditorFlags::DIRECT_EDIT, !GetOptions().m_CustomFilter.IsEmpty() }, { } }));

			getGenerator().addEvent(filterToggle, { g_AutoParamFilterBuilder, EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueFalse });
		}
		
		getGenerator().Add(FilterBuilderEditor({ g_AutoParamFilterBuilder, _T("Advanced filter settings:"), EditorFlags::DIRECT_EDIT, FilterBuilderJson(GetOptions().m_CustomFilter, getFilterFields()) }, getFilterConfig(), GetOptions().m_CustomFilter.ToString()));
	}
}

bool DlgModuleOptions::setParamSpecific(AutomationAction* i_Action)
{
	ASSERT(nullptr == m_CurrentMotherAction || m_CurrentMotherAction == i_Action->GetMother());
	if (m_CurrentMotherAction != i_Action->GetMother())
	{
		m_CurrentMotherAction = i_Action->GetMother();
		ASSERT(IsActionShowMyDataMail(m_CurrentMotherAction)
			|| IsActionShowMyDataMailFolder(m_CurrentMotherAction)
			|| IsActionShowMyDataCalendar(m_CurrentMotherAction)
			|| IsActionSelectedShowMessages(m_CurrentMotherAction)
			|| IsActionSelectedShowMailFolders(m_CurrentMotherAction)
			|| IsActionSelectedShowEventsUsers(m_CurrentMotherAction)
			|| IsActionSelectedShowEventsGroups(m_CurrentMotherAction)
			|| IsActionShowSignIns(m_CurrentMotherAction)
			|| IsActionShowAuditLogs(m_CurrentMotherAction)
			|| IsActionShowUsers(m_CurrentMotherAction)
			|| IsActionShowGroups(m_CurrentMotherAction)
		);

		m_CurrentSetParamTotalCount = 0;
		for (auto c : m_CurrentMotherAction->GetChildren())
		{
			if (IsActionSetParam(c) && MFCUtil::StringMatchW(c->GetParamValue(AutomationConstant::val().m_ParamNameTarget), GetAutomationTargetName())
				&& !MFCUtil::StringMatchW(c->GetParamValue(AutomationConstant::val().m_ParamNameName), AutomationConstant::val().m_ParamNameKeepAlive))
				++m_CurrentSetParamTotalCount;
		}
	}

	const bool onlyFilter = IsActionShowUsers(m_CurrentMotherAction) || IsActionShowGroups(m_CurrentMotherAction);

	if (DlgFormsHTML::setParamSpecific(i_Action))
	{
		// Only analyze/translate field values when all setparams have been processed.
		if (m_CurrentSetParamTotalCount == m_AutomationFieldValues.size())
		{
			const auto itCutOff = m_AutomationFieldValues.find(g_ParamNameCutOff);
			const auto hasParamCutOff = m_AutomationFieldValues.end() != itCutOff;
			const auto cutoff = hasParamCutOff ? itCutOff->second : Str::g_EmptyString;
			if (hasParamCutOff)
				m_AutomationFieldValues.erase(itCutOff);

			if (!cutoff.empty())
			{
				ASSERT(!onlyFilter);

				if (m_AutomationFieldValues.find(g_CutOffType) == m_AutomationFieldValues.end())
				{
					wstring cutoffType;
					if (MFCUtil::StringMatchW(cutoff, GetLoadingScopeStr(LoadingScope::LastHour)))
					{
						cutoffType = GetLoadingScopeStr(LoadingScope::LastHour);
					}
					else if (MFCUtil::StringMatchW(cutoff, GetLoadingScopeStr(LoadingScope::Last24h)))
					{
						cutoffType = GetLoadingScopeStr(LoadingScope::Last24h);
					}
					else if (MFCUtil::StringMatchW(cutoff, GetLoadingScopeStr(LoadingScope::LastWeek)))
					{
						cutoffType = GetLoadingScopeStr(LoadingScope::LastWeek);
					}
					else if (MFCUtil::StringMatchW(cutoff, GetLoadingScopeStr(LoadingScope::LastMonth)))
					{
						cutoffType = GetLoadingScopeStr(LoadingScope::LastMonth);
					}
					else
					{
						cutoffType = GetLoadingScopeStr(LoadingScope::Other);
					}

					m_AutomationFieldValues.insert({ g_CutOffType, cutoffType });

					if (cutoffType == GetLoadingScopeStr(LoadingScope::Other)
						&& m_AutomationFieldValues.find(g_CutOffDate) == m_AutomationFieldValues.end())
						m_AutomationFieldValues.insert({ g_CutOffDate, cutoff });
				}
			}
			else if (hasParamCutOff) // Has empty cutoff
			{
				ASSERT(!onlyFilter);
				m_AutomationFieldValues.insert({ g_CutOffType, GetLoadingScopeStr(LoadingScope::All) });
			}

			const bool hasOtherParam = m_AutomationFieldValues.end() != m_AutomationFieldValues.find(g_AutoParamEmails)
				|| m_AutomationFieldValues.end() != m_AutomationFieldValues.find(g_AutoParamChats)
				|| m_AutomationFieldValues.end() != m_AutomationFieldValues.find(g_AutoParamOnlineInPlaceArchive)
				|| m_AutomationFieldValues.end() != m_AutomationFieldValues.find(g_AutoParamClutterFolders)
				|| m_AutomationFieldValues.end() != m_AutomationFieldValues.find(g_AutoParamSoftDeletedFolders)
				|| m_AutomationFieldValues.end() != m_AutomationFieldValues.find(g_AutoParamScheduledFolders)
				|| m_AutomationFieldValues.end() != m_AutomationFieldValues.find(g_AutoParamSearchFolders)
				|| m_AutomationFieldValues.end() != m_AutomationFieldValues.find(g_AutoParamBodyPreview)
				|| m_AutomationFieldValues.end() != m_AutomationFieldValues.find(g_AutoParamFullBody)
				|| m_AutomationFieldValues.end() != m_AutomationFieldValues.find(g_AutoParamMailHeaders)
				|| m_AutomationFieldValues.end() != m_AutomationFieldValues.find(g_AutoParamFilter);

			if (hasOtherParam && !hasParamCutOff && !onlyFilter)
				m_CurrentMotherAction->AddError(_YFORMATERROR(L"Missing %s parameter.", g_ParamNameCutOff.c_str()).c_str(), TraceOutput::TRACE_ERROR);

			m_CurrentMotherAction = nullptr;
			m_CurrentSetParamTotalCount = 0;
		}

		return true;
	}

	return false;
}

std::map<wstring, DlgModuleOptions::FilterFieldProperties> DlgModuleOptions::getFilterFields() const
{
	return {};
}

web::json::value DlgModuleOptions::getFilterConfig() const
{
	auto getOperators = [=](const vector<wstring>& p_OperatorRestrictions)
	{
		vector<web::json::value> operators;
		if (p_OperatorRestrictions.empty())
		{
			for (auto& op : g_FilterOperators.right) // Labels
				operators.push_back(web::json::value::string(op.first));
		}
		else
		{
			for (auto& op : p_OperatorRestrictions) // Labels
				operators.push_back(web::json::value::string(g_FilterOperators.left.at(op)));
		}
		return operators;
	};

	vector<web::json::value> filters;
	for (auto& item : getFilterFields())
	{
		filters.push_back(web::json::value::object());
		auto& f = filters.back();
		f[_YTEXT("id")] = web::json::value::string(item.first);
		f[_YTEXT("label")] = web::json::value::string(item.second.m_DisplayName);
		f[_YTEXT("operators")] = web::json::value::array(getOperators(item.second.m_OperatorRestrictions));
		item.second.m_Type.FillJson(f);
	}

	return web::json::value::array(filters);
}

bool DlgModuleOptions::processPostedData(const IPostedDataTarget::PostedData& data)
{
	boost::YOpt<YTimeDate> scopeDateTime;

	auto ittCustomScope = data.find(g_CutOffDate);
	if (ittCustomScope != data.end() && !ittCustomScope->second.empty())
	{
		YTimeDate timeDate;
		const auto valid = TimeUtil::GetInstance().ConvertTextToDate(ittCustomScope->second, timeDate);
		ASSERT(valid);
		if (valid)
			scopeDateTime = timeDate;
	}

	auto ittScope = data.find(g_CutOffType);
	if (ittScope != data.end())
		UpdateCutOffDateTime(ittScope->second, scopeDateTime);

	auto ittSoftDeleted = data.find(g_AutoParamSoftDeletedFolders);
	if (ittSoftDeleted != data.end())
		m_ModuleOpts.m_RequestSoftDeletedFolders = ittSoftDeleted->second == UsefulStrings::g_ToggleValueTrue;

	auto ittBodyPreview = data.find(g_AutoParamBodyPreview);
	if (ittBodyPreview != data.end())
		m_ModuleOpts.m_BodyPreview = ittBodyPreview->second == UsefulStrings::g_ToggleValueTrue;

	auto ittFullBody = data.find(g_AutoParamFullBody);
	if (ittFullBody != data.end())
		m_ModuleOpts.m_BodyContent = ittFullBody->second == UsefulStrings::g_ToggleValueTrue;

	auto ittMailHeaders = data.find(g_AutoParamMailHeaders);
	if (ittMailHeaders != data.end())
		m_ModuleOpts.m_MailHeaders = ittMailHeaders->second == UsefulStrings::g_ToggleValueTrue;

	auto ittFilter = data.find(g_AutoParamFilterBuilder);
	ASSERT(ittFilter == data.end() || !ittFilter->second.empty());
	if (ittFilter != data.end() && !ittFilter->second.empty())
		m_ModuleOpts.m_CustomFilter = FilterParser(*this, ittFilter->second)();
	
	bool filterIsString = false;
	if (AutomationMustClose())
	{
		auto ittFilter = data.find(g_AutoParamFilter);
		if (ittFilter != data.end())
		{
			m_ModuleOpts.m_CustomFilter = ODataFilter(ittFilter->second);
			filterIsString = true;
		}
	}

	bool shouldCloseDialog = true;

	if (m_ModuleOpts.m_CutOffDateTime && !m_ModuleOpts.m_CustomFilter.IsEmpty())
	{
		wstring filterString = m_ModuleOpts.m_CustomFilter.ToString();
		bool filterConflict = false;
		for (auto& ff : getFilterFields())
		{
			if (ff.second.m_Type.IsDate() || ff.second.m_Type.IsDateTime())
			{
				if (filterIsString)
				{
					// Handle? (Automation only)
					filterString;
				}
				else
				{
					for (const auto& fb : m_ModuleOpts.m_CustomFilter.GetFilterBlocks())
					{
						if (fb.GetField() == ff.first
							&& *m_ModuleOpts.m_CutOffDateTime > fb.GetValue())
						{
							filterConflict = true;
							break;
						}
					}
				}
			}

			if (filterConflict)
				break;
		}

		if (filterConflict)
		{
			auto res = YCodeJockMessageBox(this
				, DlgMessageBox::eIcon_ExclamationWarning
				, _T("Possible conflict")
				, _YFORMAT(L"The filter you defined may conflict with the current cutoff setting: %s\nDo you want to keep this?", GetLoadingScopeFriendlyStr(m_ModuleOpts.m_LoadingScope).c_str())
				, _YTEXT("")
				, { { IDOK, _T("Continue") }, { IDCANCEL, _T("Edit") } }).DoModal();

			if (res == IDCANCEL)
				shouldCloseDialog = false;
		}
	}

	return shouldCloseDialog;
}

void DlgModuleOptions::UpdateCutOffDateTime(const wstring& p_CutOffType, const boost::YOpt<YTimeDate>& p_ScopeDateTime)
{
	if (!m_UpdateOptions)
	{
		ASSERT(!m_ModuleOpts.m_ReferenceTimeDate);
		m_ModuleOpts.m_ReferenceTimeDate = YTimeDate::GetCurrentTimeDate();
	}

	if (p_CutOffType == GetLoadingScopeStr(LoadingScope::All))
	{
		m_ModuleOpts.m_CutOffDateTime.reset();
		m_ModuleOpts.m_LoadingScope = LoadingScope::All;
	}
	else
	{
		ASSERT(m_ModuleOpts.m_ReferenceTimeDate);
		YTimeDate cutOffDateTime = *m_ModuleOpts.m_ReferenceTimeDate;
		if (p_CutOffType == GetLoadingScopeStr(LoadingScope::LastHour))
		{
			cutOffDateTime.Adjust(0, 0, 0, -1, 0, 0);
			m_ModuleOpts.m_LoadingScope = LastHour;
		}
		else if (p_CutOffType == GetLoadingScopeStr(LoadingScope::Last24h))
		{
			cutOffDateTime.Adjust(0, 0, 0, -24, 0, 0);
			m_ModuleOpts.m_LoadingScope = Last24h;
		}
		else if (p_CutOffType == GetLoadingScopeStr(LoadingScope::LastWeek))
		{
			cutOffDateTime.Adjust(0, 0, -7, 0, 0, 0);
			m_ModuleOpts.m_LoadingScope = LastWeek;
		}
		else if (p_CutOffType == GetLoadingScopeStr(LoadingScope::LastMonth))
		{
			cutOffDateTime.Adjust(0, -1, 0, 0, 0, 0);
			m_ModuleOpts.m_LoadingScope = LastMonth;
		}
		else if (p_CutOffType == GetLoadingScopeStr(LoadingScope::Other))
		{
			ASSERT(p_ScopeDateTime);
			if (p_ScopeDateTime)
			{
				cutOffDateTime = *p_ScopeDateTime;
				m_ModuleOpts.m_LoadingScope = Other;
			}
		}
		m_ModuleOpts.m_CutOffDateTime = TimeUtil::GetInstance().GetISO8601String(cutOffDateTime);
	}
}

wstring DlgModuleOptions::externalFilterProcessCallback(const wstring p_PropertyName, const wstring p_CurrentFilterJson)
{
	return FilterParser(*this, p_CurrentFilterJson)().ToString();
}

wstring DlgModuleOptions::GetLoadingScopeStr(LoadingScope scope)
{
	static const wstring str[] =
	{
		_YTEXT("all"),
		_YTEXT("lastHour"),
		_YTEXT("lastDay"),
		_YTEXT("lastWeek"),
		_YTEXT("lastMonth"),
		_YTEXT("other"),
	};

	return str[scope];
}

wstring DlgModuleOptions::GetLoadingScopeFriendlyStr(LoadingScope scope)
{
	static const wstring str[] =
	{
		_T("All"),
		YtriaTranslate::Do(DlgMessageModuleOptions_generateJSONScriptData_6, _YLOC("Last 60 minutes")).c_str(),
		_T("Last 24 hours"),
		_T("Last 7 days"),
		_T("Last 1 month"),
		YtriaTranslate::Do(DlgMessageModuleOptions_generateJSONScriptData_53, _YLOC("Specify range")).c_str(),
	};

	return str[scope];
}

void DlgModuleOptions::TranslateAction(AutomatedApp* p_App, AutomationAction* p_Action, list<AutomationAction*>& p_ActionList)
{
	// Handle other actions?
	ASSERT(IsActionShowMyDataMail(p_Action)
		|| IsActionShowMyDataMailFolder(p_Action)
		|| IsActionShowMyDataCalendar(p_Action)
		|| IsActionSelectedShowMessages(p_Action)
		|| IsActionSelectedShowMailFolders(p_Action)
		|| IsActionSelectedShowEventsUsers(p_Action)
		|| IsActionSelectedShowEventsGroups(p_Action)
		|| IsActionShowSignIns(p_Action)
		|| IsActionShowAuditLogs(p_Action)
		|| IsActionShowUsers(p_Action)
		|| IsActionShowGroups(p_Action)
	);

	p_ActionList.push_back(p_Action);

	p_App->generateSetParamAction(g_ParamNameCutOff, g_ActionNameModuleOptions, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_AutoParamEmails, g_ActionNameModuleOptions, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_AutoParamChats,	g_ActionNameModuleOptions, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_AutoParamOnlineInPlaceArchive, g_ActionNameModuleOptions, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_AutoParamClutterFolders, g_ActionNameModuleOptions, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_AutoParamSoftDeletedFolders, g_ActionNameModuleOptions, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_AutoParamScheduledFolders, g_ActionNameModuleOptions, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_AutoParamSearchFolders,	g_ActionNameModuleOptions, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_AutoParamBodyPreview,	g_ActionNameModuleOptions, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_AutoParamFullBody,		g_ActionNameModuleOptions, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_AutoParamMailHeaders,	g_ActionNameModuleOptions, p_Action, p_ActionList);
	p_App->generateSetParamAction(g_AutoParamFilter,		g_ActionNameModuleOptions, p_Action, p_ActionList);
}

// ==============================================================================================================

DlgModuleOptions::FilterFieldProperties::Type::~Type() = default;

void DlgModuleOptions::FilterFieldProperties::Type::FillJson(web::json::value& p_Properties) const
{
	ASSERT(m_Properties.is_object());
	auto& obj = m_Properties.as_object();
	for (auto& p : obj)
		p_Properties[p.first] = p.second;
}

bool DlgModuleOptions::FilterFieldProperties::Type::IsDate() const
{
	return m_Properties.has_field(_YTEXT("type")) && m_Properties.at(_YTEXT("type")) == web::json::value::string(_YTEXT("date"));
}

bool DlgModuleOptions::FilterFieldProperties::Type::IsDateTime() const
{
	return m_Properties.has_field(_YTEXT("type")) && m_Properties.at(_YTEXT("type")) == web::json::value::string(_YTEXT("datetime"));
}

DlgModuleOptions::FilterFieldProperties::StringType::StringType()
{
	m_Properties = web::json::value::object();
	m_Properties[_YTEXT("type")] = web::json::value::string(_YTEXT("string"));
}

DlgModuleOptions::FilterFieldProperties::BoolType::BoolType(const std::pair<wstring, wstring>& p_TrueFalseLabelOverride/* = {}*/)
{
	m_Properties = web::json::value::object();
	m_Properties[_YTEXT("type")] = web::json::value::string(_YTEXT("boolean"));
	m_Properties[_YTEXT("input")] = web::json::value::string(_YTEXT("radio"));

	std::vector<web::json::value> values
	{
		web::json::value::object({ { wstring(_YTEXT("true")), p_TrueFalseLabelOverride.first.empty() ? web::json::value::string(_T("TRUE")) : web::json::value::string(p_TrueFalseLabelOverride.first) } }),
		web::json::value::object({ { wstring(_YTEXT("false")), p_TrueFalseLabelOverride.second.empty() ? web::json::value::string(_T("FALSE")) : web::json::value::string(p_TrueFalseLabelOverride.second) } }),
	};

	m_Properties[_YTEXT("values")] = web::json::value::array(values);
}

DlgModuleOptions::FilterFieldProperties::EnumType::EnumType(const std::vector<std::pair<wstring, wstring>>& p_Values)
{
	m_Properties = web::json::value::object();
	m_Properties[_YTEXT("input")] = web::json::value::string(_YTEXT("select"));

	std::vector<web::json::value> values;
	std::transform(p_Values.begin(), p_Values.end(), std::back_inserter(values), [](auto& p) {return web::json::value::object({ { p.first, web::json::value::string(p.second) } }); });
	m_Properties[_YTEXT("values")] = web::json::value::array(values);
}

DlgModuleOptions::FilterFieldProperties::DateType::DateType()
{
	m_Properties = web::json::value::object();
	m_Properties[_YTEXT("type")] = web::json::value::string(_YTEXT("date"));
	m_Properties[_YTEXT("placeholder")] = web::json::value::string(_YTEXT("____-__-__"));
	
	{
		auto validationObj = web::json::value::object();
		// Must match the one in DlgFormsHTML::initMain, be careful !! they don't use the same system !! "YYYY-MM-DD" == "yy-mm-dd"
		validationObj[_YTEXT("format")] = web::json::value::string(_YTEXT("YYYY-MM-DD"));
		{
			auto messagesObj = web::json::value::object();
			messagesObj[_YTEXT("format")] = web::json::value::string(_T("Invalid Date provided"));
			validationObj[_YTEXT("messages")] = messagesObj;
		}
		m_Properties[_YTEXT("validation")] = validationObj;
	}
}

DlgModuleOptions::FilterFieldProperties::DateTimeType::DateTimeType()
{
	m_Properties = web::json::value::object();
	m_Properties[_YTEXT("type")] = web::json::value::string(_YTEXT("datetime"));
	m_Properties[_YTEXT("placeholder")] = web::json::value::string(_YTEXT("____-__-__ __:__"));

	{
		auto validationObj = web::json::value::object();
		validationObj[_YTEXT("format")] = web::json::value::string(_YTEXT("YYYY-MM-DD hh:mm"));
		{
			auto messagesObj = web::json::value::object();
			messagesObj[_YTEXT("format")] = web::json::value::string(_T("Invalid DateTime provided"));
			validationObj[_YTEXT("messages")] = messagesObj;
		}
		m_Properties[_YTEXT("validation")] = validationObj;
	}
}

DlgModuleOptions::FilterFieldProperties::IntegerType::IntegerType()
{
	m_Properties = web::json::value::object();
	m_Properties[_YTEXT("type")] = web::json::value::string(_YTEXT("integer"));
}
