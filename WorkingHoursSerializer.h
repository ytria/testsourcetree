#pragma once

#include "IJsonSerializer.h"

class WorkingHoursSerializer : public IJsonSerializer
{
public:
    WorkingHoursSerializer(const WorkingHours& p_WorkingHours);
    void Serialize() override;

private:
    const WorkingHours& m_WorkingHours;
};

