#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "DeviceDetail.h"

class DeviceDetailDeserializer : public JsonObjectDeserializer, public Encapsulate<DeviceDetail>
{
public:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};

