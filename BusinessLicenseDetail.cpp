#include "BusinessLicenseDetail.h"
#include "Sapio365Session.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessLicenseDetail>("LicenseDetail") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("License Detail"))))
		.constructor()(policy::ctor::as_object)
		.property("servicePlans", &BusinessLicenseDetail::m_ServicePlans)
		.property("skuId", &BusinessLicenseDetail::m_SkuId)
		.property("skuPartNumber", &BusinessLicenseDetail::m_SkuPartNumber)
		;
}

BusinessLicenseDetail::BusinessLicenseDetail(const LicenseDetail& p_LicenseDetail)
{
	SetID(p_LicenseDetail.m_Id);
	SetServicePlanInfos(p_LicenseDetail.m_ServicePlans);
	SetSkuId(p_LicenseDetail.m_SkuId);
	SetSkuPartNumber(p_LicenseDetail.m_SkuPartNumber);
}

const vector<ServicePlanInfo>& BusinessLicenseDetail::GetServicePlanInfos() const
{
	return m_ServicePlans;
}

void BusinessLicenseDetail::SetServicePlanInfos(const vector<ServicePlanInfo>& val)
{
	m_ServicePlans = val;
}

const boost::YOpt<PooledString>& BusinessLicenseDetail::GetSkuId() const
{
	return m_SkuId;
}

void BusinessLicenseDetail::SetSkuId(const boost::YOpt<PooledString>& val)
{
	m_SkuId = val;
}

const boost::YOpt<PooledString>& BusinessLicenseDetail::GetSkuPartNumber() const
{
	return m_SkuPartNumber;
}

void BusinessLicenseDetail::SetSkuPartNumber(const boost::YOpt<PooledString>& val)
{
	m_SkuPartNumber = val;
}

LicenseDetail BusinessLicenseDetail::ToLicenseDetail(const BusinessLicenseDetail& p_BusinessLicenseDetail)
{
	LicenseDetail licenseDetail;

	licenseDetail.m_Id				= p_BusinessLicenseDetail.GetID();
	licenseDetail.m_ServicePlans	= p_BusinessLicenseDetail.GetServicePlanInfos();
	licenseDetail.m_SkuId			= p_BusinessLicenseDetail.GetSkuId();
	licenseDetail.m_SkuPartNumber	= p_BusinessLicenseDetail.GetSkuPartNumber();

	return licenseDetail;
}

pplx::task<vector<HttpResultWithError>> BusinessLicenseDetail::SendEditRequest(std::shared_ptr<Sapio365Session> p_MSGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(false);
	return pplx::task_from_result(vector<HttpResultWithError>{ });
}

TaskWrapper<HttpResultWithError> BusinessLicenseDetail::SendCreateRequest(std::shared_ptr<Sapio365Session> p_MSGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(false);
    return pplx::task_from_result(HttpResultWithError());
}

TaskWrapper<HttpResultWithError> BusinessLicenseDetail::SendDeleteRequest(std::shared_ptr<Sapio365Session> p_MSGraphSession, YtriaTaskData p_TaskData) const
{
	ASSERT(false);
    return pplx::task_from_result(HttpResultWithError());
}
