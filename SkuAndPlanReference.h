#pragma once

#include "BusinessSubscribedSku.h"
#include "SkuAndPlanInfoSQLEngine.h"

class ISkuAndPlanInfoProvider
{
public:
	virtual ~ISkuAndPlanInfoProvider() = default;
	virtual const PooledString& GetSkuLabelById(const PooledString& p_SkuId) = 0;
	virtual const PooledString& GetSkuPartNumberById(const PooledString& p_SkuId) = 0;
	virtual const PooledString& GetPlanLabelById(const PooledString& p_PlanId) = 0;
	virtual const PooledString& GetPlanNameById(const PooledString& p_PlanId) = 0;
	virtual const std::set<PooledString>& GetPlansForSkuById(const PooledString& p_SkuId) = 0; // Returns plan Ids
	virtual const std::set<PooledString>& GetPlanExclusionsForPlanById(const PooledString& p_PlanId) = 0; // Returns plan Ids
};

class MappedSkuAndPlanInfoProvider : public ISkuAndPlanInfoProvider
{
public:
	void Clear();

	virtual const PooledString& GetSkuLabelById(const PooledString& p_SkuId) override;
	virtual const PooledString& GetSkuPartNumberById(const PooledString& p_SkuId) override;
	virtual const PooledString& GetPlanLabelById(const PooledString& p_PlanId) override;
	virtual const PooledString& GetPlanNameById(const PooledString& p_PlanId) override;
	virtual const std::set<PooledString>& GetPlansForSkuById(const PooledString& p_SkuId) override;
	virtual const std::set<PooledString>& GetPlanExclusionsForPlanById(const PooledString& p_PlanId) override;

	const std::map<PooledString, SkuAndPlanInfoSQLEngine::SkuInfo> GetSkuInfos() const;
	const std::map<PooledString, SkuAndPlanInfoSQLEngine::PlanInfo> GetPlanInfos() const;

protected:
	std::map<PooledString, SkuAndPlanInfoSQLEngine::SkuInfo> m_SkuInfoById;
	std::map<PooledString, SkuAndPlanInfoSQLEngine::PlanInfo> m_PlanInfoById;
};

class JsonSkuAndPlanInfoProvider : public MappedSkuAndPlanInfoProvider
{
public:
	JsonSkuAndPlanInfoProvider();

	bool Load(const char* p_UTF8JsonString, size_t p_Length);
	bool Load(std::string& p_UTF8JsonString);
	bool Load(const wstring& p_JsonString);
};

class StaticSkuAndPlanInfoProvider : virtual public JsonSkuAndPlanInfoProvider
{
public:
	StaticSkuAndPlanInfoProvider(bool p_Load);
	void Load();
};

class FileSkuAndPlanInfoProvider : virtual public JsonSkuAndPlanInfoProvider
{
public:
	FileSkuAndPlanInfoProvider(const wstring& p_FilePath);
	bool LoadFile(const wstring& p_FilePath);
};

class OnlineFileSkuAndPlanInfoProvider : public FileSkuAndPlanInfoProvider
{
public:
	OnlineFileSkuAndPlanInfoProvider(const wstring& p_FileURL);
	wstring LoadURL(const wstring& p_FilePath, bool p_OnlyDownload = false); // Returns local file path
};

class CachedOnlineFileSkuAndPlanInfoProvider : public OnlineFileSkuAndPlanInfoProvider, public StaticSkuAndPlanInfoProvider
{
public:
	CachedOnlineFileSkuAndPlanInfoProvider(const wstring& p_FileURL, const wstring& p_CacheFilePath, int p_DayIntervalForRefresh, bool p_Init);

protected:
	void Init(const wstring& p_FileURL, const wstring& p_CacheFilePath, int p_DayIntervalForRefresh);
};

class SQLSkuAndPlanInfoProvider : public MappedSkuAndPlanInfoProvider
{
public:
	SQLSkuAndPlanInfoProvider(std::initializer_list<std::unique_ptr<MappedSkuAndPlanInfoProvider>> p_AltenativeSkuAndPlanInfoProviders);

	void LoadFromSql();
	void SaveToSql() const;
	void UpdateWith(const std::unique_ptr<MappedSkuAndPlanInfoProvider>& p_AltenativeSkuAndPlanInfoProvider, bool p_Reload = true);

private:
	std::unique_ptr<SkuAndPlanInfoSQLEngine> m_Engine;
};

class TenantSkuAndPlanInfoProvider : public MappedSkuAndPlanInfoProvider
{
public:
	TenantSkuAndPlanInfoProvider(const vector<BusinessSubscribedSku>& p_Skus);
};

class SkuAndPlanReference
{
public:
	static SkuAndPlanReference& GetInstance();
	static void DeleteInstance();

private: // For now, rely on GetInstance to init the static instance when needed. Get this back to public if default behavior is not enough anymore.
	static SkuAndPlanReference& InitInstance(std::unique_ptr<ISkuAndPlanInfoProvider>&& p_SkuAndPlanInfoProvider); // Init/re-init with given provider

public:
	const PooledString& GetSkuLabel(const PooledString& p_SkuId);
	const PooledString& GetSkuPartNumber(const PooledString& p_SkuId);
	const PooledString& GetPlanLabel(const PooledString& p_PlanId);
	const PooledString& GetPlanName(const PooledString& p_PlanId);

	const std::set<PooledString>& GetPlansForSku(const PooledString& p_SkuId);
	const std::set<PooledString>& GetPlanExclusionsForPlan(const PooledString& p_SkuId);

	void Update(const vector<BusinessSubscribedSku>& p_Skus);

private:
	// SkuAndPlanInfo ctor below should be private but usage of unique_ptr forces it to be public.
	// To avoid anyone from using it, we use a private object whose only goals is to make sure
	// no SkuAndPlanInfo is constructed outside of this class.
	// https://rienajouter.blogspot.com/2014/10/makeshared-and-makeunique-for-classes.html
	struct _ConstructorCookie { explicit _ConstructorCookie() = default; };

public:
	SkuAndPlanReference(std::unique_ptr<ISkuAndPlanInfoProvider>&& p_SkuAndPlanInfoProvider, _ConstructorCookie);

private:
	std::unique_ptr<ISkuAndPlanInfoProvider> m_SkuAndPlanInfoProvider;

	static std::unique_ptr<SkuAndPlanReference> g_Instance;
};