#pragma once

#include "IButtonUpdater.h"

class GridFrameBase;

template<class FrameClass>
class OnPremRevertAllButtonUpdater : public IButtonUpdater
{
public:
	OnPremRevertAllButtonUpdater(FrameClass& p_Frame);
	void Update(CCmdUI* p_CmdUi) override;

private:
	FrameClass& m_Frame;
};

template<class FrameClass>
OnPremRevertAllButtonUpdater<FrameClass>::OnPremRevertAllButtonUpdater(FrameClass& p_Frame)
	: m_Frame(p_Frame)
{

}

template<class FrameClass>
void OnPremRevertAllButtonUpdater<FrameClass>::Update(CCmdUI* p_CmdUi)
{
	p_CmdUi->Enable(m_Frame.HasNoTaskRunning()
		&& (m_Frame.hasRevertableModificationsPending(false) || m_Frame.hasOnPremiseChanges(false)));
}

