#pragma once

class UserIdentity
{
public:
	boost::YOpt<PooledString> m_Id;
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<PooledString> m_IpAddress;
	boost::YOpt<PooledString> m_UserPrincipalName;
};

