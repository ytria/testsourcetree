#include "CosmosDbDeserializer.h"

void Azure::CosmosDbDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	auto itt = p_Object.find(_YTEXT("properties"));
	ASSERT(itt != p_Object.end());
	if (itt != p_Object.end())
	{
		ASSERT(itt->second.is_object());
		if (itt->second.is_object())
		{
			const auto& propsObj = itt->second.as_object();

			JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, propsObj, true);
		}
	}
}
