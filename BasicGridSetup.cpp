#include "BasicGridSetup.h"
#include "GridUtil.h"
#include "BaseO365Grid.h"

const wstring BasicGridSetup::g_LastQueryColumnUID = _YUID("lastQueriedOn");

BasicGridSetup::BasicGridSetup(const wstring& p_AutomationName, const wstring& p_LicenseTrackingCode)
    : m_AutomationName(p_AutomationName)
	, m_LicenseTrackingCode(p_LicenseTrackingCode)
	, m_ColumnLastRequestDateTime(nullptr)
{
}

BasicGridSetup::BasicGridSetup(const wstring& p_AutomationName, const wstring& p_LicenseTrackingCode, const std::vector<GridSetupMetaColumn>& p_DefaultMetaColumns)
    : BasicGridSetup(p_AutomationName, p_LicenseTrackingCode, p_DefaultMetaColumns, {})
{
}

BasicGridSetup::BasicGridSetup(const wstring& p_AutomationName,
    const wstring& p_LicenseTrackingCode,
    const std::vector<GridSetupMetaColumn>& p_DefaultMetaColumns,
    const std::vector<rttr::variant>& p_LoadMoreObjectTypes)
    : m_AutomationName(p_AutomationName)
	, m_LicenseTrackingCode(p_LicenseTrackingCode)
	, m_LoadMoreObjectTypes(p_LoadMoreObjectTypes)
	, m_ColumnLastRequestDateTime(nullptr)
{
    setDefaultMetaColumns(p_DefaultMetaColumns);
}

void BasicGridSetup::AddMetaColumn(const GridSetupMetaColumn& p_MetaCol)
{
    m_MetaColumns.push_back(p_MetaCol);
}

GridBackendColumn* BasicGridSetup::GetColumnLastRequestDateTime() const
{
	return m_ColumnLastRequestDateTime;
}

void BasicGridSetup::setDefaultMetaColumns(const vector<GridSetupMetaColumn>& p_MetaCols)
{
    m_MetaColumns = p_MetaCols;

    const size_t size = p_MetaCols.size();
    ASSERT(size == 3); // If using custom meta columns, you should use AddMetaColumn instead.
    if (size == 3)
    {
        auto& col0 = m_MetaColumns[0];
        col0 = GridSetupMetaColumn(col0.GetColumn(), col0.GetDisplayName(), col0.GetFamily(), GridSetupMetaColumn::g_ColUIDMetaDisplayName);
        
        auto& col1 = m_MetaColumns[1];
        col1 = GridSetupMetaColumn(col1.GetColumn(), col1.GetDisplayName(), col1.GetFamily(), GridSetupMetaColumn::g_ColUIDMetaUserName);

        auto& col2 = m_MetaColumns[2];
        col2 = GridSetupMetaColumn(col2.GetColumn(), col2.GetDisplayName(), col2.GetFamily(), GridSetupMetaColumn::g_ColUIDMetaId);
    }
}

void BasicGridSetup::Setup(CacheGrid& p_Grid, bool p_AddColumnLastRequestDateTime)
{
    GridUtil::Add365ObjectTypes(p_Grid);
    ASSERT(!m_AutomationName.empty());
    p_Grid.SetAutomationName(m_AutomationName);
    p_Grid.SetAnalyticsTrackingCode(m_LicenseTrackingCode);
    p_Grid.SetObjectTypesForLoadMore(m_LoadMoreObjectTypes);

    if (p_AddColumnLastRequestDateTime)
    {
        m_ColumnLastRequestDateTime = p_Grid.AddColumnDate(g_LastQueryColumnUID, YtriaTranslate::Do(BasicGridSetup_Setup_1, _YLOC("Last queried on")).c_str(), GridBackendUtil::g_ColumnFamilyMetaData, CacheGrid::g_ColumnSize16, { O365Grid::g_ColumnsPresetTech });
        m_ColumnLastRequestDateTime->SetFlag(GridBackendUtil::CANNOTBEGHOST);
    }

    GridBackendColumn* colObjType = p_Grid.AddColumnObjectType();
    ASSERT(nullptr != colObjType);
    if (nullptr != colObjType)
    {
        colObjType->SetLocked(true);
        /*colObjType->SetPresetFlags({ O365Grid::g_ColumnsPresetDefault });*/// column is locked: no need to use the g_ColumnsPresetDefault
    }

	p_Grid.AddGenericColumns();
    p_Grid.AddColumnStatus(GridBackendUtil::g_ColumnTitleStatus);// column is locked: no need to use the g_ColumnsPresetDefault

    for (auto metaCol : m_MetaColumns)
    {
        if (nullptr != metaCol.GetColumn())
        {
            GridBackendColumn* col = p_Grid.AddColumn(metaCol.GetUID(), metaCol.GetDisplayName(), metaCol.GetFamily(), CacheGrid::g_ColumnSize22);
            *metaCol.GetColumn() = col;

            ASSERT(nullptr != col);
            if (nullptr != col)
            {
                if (col->GetUniqueID() == GridSetupMetaColumn::g_ColUIDMetaId)
                {
                    col->SetFlag(GridBackendUtil::CASESENSITIVEFIELDS);
                    col->SetWidth(CacheGrid::g_ColumnSize12);
                    col->SetPresetFlags({ O365Grid::g_ColumnsPresetTech });
                }
                else if (col->GetUniqueID() == GridSetupMetaColumn::g_ColUIDMetaDisplayName)
                    col->SetPresetFlags({ O365Grid::g_ColumnsPresetDefault });
            }
        }
    }
}