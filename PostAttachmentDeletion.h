#pragma once

#include "ISubItemDeletion.h"
#include "AttachmentInfo.h"

class PostAttachmentDeletion : public ISubItemDeletion
{
public:
    PostAttachmentDeletion(GridPosts& p_Grid, const AttachmentInfo& p_Info);
    
	virtual bool IsCorrespondingRow(GridBackendRow* p_Row) const override;

	virtual vector<ModificationLog> GetModificationLogs() const override;

	GridBackendRow* GetMainRowIfCollapsed() const override;

private:
    GridPosts& m_GridPosts;
    AttachmentInfo m_AttachmentInfo;
};