#include "DelegatesAdder.h"

#include "AddDelegateSerializer.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "SoapDocument.h"
#include "XMLUtil.h"

void Ex::DelegatesAdder::AddDelegate(const Ex::AddDelegate& p_AddDelegate)
{
    m_AddDelegates.push_back(p_AddDelegate);
}

TaskWrapper<void> Ex::DelegatesAdder::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Result = std::make_shared<SingleRequestResult>();
    //m_Deserializer = std::make_shared<AddDelegatesResponseDeserializer>();

    SoapDocument soapDoc;

    for (const auto& newDelegate : m_AddDelegates)
    {
        Ex::AddDelegateSerializer serializer(*soapDoc.GetDocument(), newDelegate);
        soapDoc.AddToBody(serializer);
    }
    wstring xml = XMLUtil::GetDocumentAsString(soapDoc.GetDocument());

    const WebPayloadText payload(XMLUtil::GetDocumentAsString(soapDoc.GetDocument()));

    return pplx::task<void>();// p_Session->GetExchangeSession()->Send(m_Deserializer, payload, p_TaskData);
}
