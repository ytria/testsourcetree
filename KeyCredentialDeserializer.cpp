#include "KeyCredentialDeserializer.h"
#include "JsonSerializeUtil.h"

void KeyCredentialDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("customKeyIdentifier"), m_Data.m_CustomKeyIdentifier, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("endDateTime"), m_Data.m_EndDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("keyId"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("startDateTime"), m_Data.m_StartDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.m_Type, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("usage"), m_Data.m_Usage, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("key"), m_Data.m_Key, p_Object);
}
