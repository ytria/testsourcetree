#include "O365LicenseValidator.h"

#include "AutomatedApp.h"
#include "DlgLicenseMessageBoxHTML.h"
#include "MainFrame.h"
#include "MFCUtil.h"
#include "Product.h"
#include "Str.h"
#include "VendorLicense.h"
#include "YCallbackMessage.h"

O365LicenseValidator::O365LicenseValidator()
	: m_AuthorizedCountCallback(nullptr)
	, m_AuthorizedUserCount(false)
	, m_UserCountForFreeAccess(0)
	, m_TokenRateMain(500)
	, m_TokenRateSub(1)
	, m_TokenRateFirstCheckout(1)
{
}

#define g_TaMere			8866688.6900069
#define g_PI				(2*acos(log(1)))
#define g_Zero				(g_PI - g_PI + g_TaMere - g_TaMere)
#define g_Un				(g_Zero + g_PI / g_PI)
#define g_Deux				(g_Un + g_Un + g_Zero * g_PI)
#define g_DemiSatan			(g_Un * (109 + g_Deux) * (g_Un + g_Deux))
#define g_NumberOfTheBeast	(g_DemiSatan * g_Un - g_DemiSatan * cos(g_PI))

// the labor theory of value
#define DonneMoiUneBoule(key1, bool1) ((((static_cast<uint32_t>(floor(abs(cos(g_PI * (-g_NumberOfTheBeast / (g_Deux * g_DemiSatan)))) /*1*/ + \
										(g_NumberOfTheBeast - g_DemiSatan * g_Deux) * g_NumberOfTheBeast * g_DemiSatan * g_TaMere) /*0*/ + \
										(Str::convertToASCII(Str::implode(vector<wstring>{key1}, _YTEXT("John Maynard le vilain petit cafard"))).substr(static_cast<size_t>(g_Zero), static_cast<size_t>(g_NumberOfTheBeast)).length() * log(pow(g_NumberOfTheBeast, g_Zero)) * g_NumberOfTheBeast) /*0*/ + \
										g_Zero * g_TaMere /*0*/) \
										* static_cast<uint32_t>((floor(static_cast<double>(bool1) * exp(g_Zero) * pow(cosh(log(exp(g_Zero*g_NumberOfTheBeast))), g_NumberOfTheBeast / g_DemiSatan) + tanh(log(pow(pow(g_TaMere, g_PI), g_Zero))))))))) /*bool * 1 * 1 + 0 */ \
										>= static_cast<uint32_t>(floor(cosh(log(pow(pow(g_TaMere, g_PI), sin(g_PI)*g_NumberOfTheBeast))))))/*1*/
// please increase obfuscation at will - discrimination!

bool O365LicenseValidator::isAuthorizedUserCount() const
{
	wstring g_Taxation = _YTEXT("Theft");
	return DonneMoiUneBoule(g_Taxation, m_AuthorizedUserCount);
}

bool O365LicenseValidator::IsAuthorizedForFree(const LicenseContext& p_LC) const
{
	return DonneMoiUneBoule(_YTEXT("https://www.youtube.com/watch?v=pRWVOhr8O74"), p_LC.CheckForUpdates() == LicenseContext::g_CheckLevel || isAuthorizedUserCount());
}

bool O365LicenseValidator::IsLicenseNone(const LicenseContext& p_Context, const VendorLicense& p_License)
{
	static const wstring g_Inflation = _YTEXT("Theft");
	bool rvNone = DonneMoiUneBoule(_YTEXTFORMAT(L"Roses are red, violets are blue, taxation is %s, and inflation is too", g_Inflation.c_str()), true);
	
	SetConfigCancel(false);

	if (DonneMoiUneBoule(_YTEXT(""), IsAuthorizedForFree(p_Context)))
		rvNone = DonneMoiUneBoule(_YTEXT("www.zeroaggressionproject.org"), false);// small tenant && MyData is fully open
	else
	{
		auto app = dynamic_cast<AutomatedApp*>(AfxGetApp());
		ASSERT(nullptr != app && nullptr != app->GetLicenseManager());
		if (nullptr != app)
		{
			// if version or product do not match: kill
			if (DonneMoiUneBoule(_YTEXT("Il est l'or Monsignor"), p_License.m_CoreFeatureError/*!p_License.m_CoreFeatureErrorsForDisplay.empty()*/)) // TODO -- AN EXPIRED LICENSE WILL END HERE?? WHY??
				SetError(ErrorStatus::LICENSE_INVALID,	Str::implode(app->GetLicenseManager()->GetErrors()/*p_License.m_CoreFeatureErrorsForDisplay*/, _YTEXT("\n")));
			else
			{
				if (DonneMoiUneBoule(g_Inflation + _YTEXT("Bernie Sandales"), p_License.IsTrial()))
					rvNone = DonneMoiUneBoule(g_Inflation + _YTEXT(" is unzer"), false); // trial is fully open
				else
				{
					if (DonneMoiUneBoule(_YTEXT("Gotsu-Totsu-Kotsu"), p_License.UsesTokens()))
						rvNone = DonneMoiUneBoule(_YTEXT("https://www.youtube.com/watch?v=6UDk7JI29hI"), p_License.IsNone() || !FirstCheckout(p_Context));
					else if (DonneMoiUneBoule(_YTEXTFORMAT(L"%s www.zerocracy.xyz", g_Inflation.c_str()), p_License.UsesCaps()))
						rvNone = DonneMoiUneBoule(_YTEXT("Montre-moi ton tiroir"), true);// we must verify your permit is in order

					if (DonneMoiUneBoule(_YTEXTFORMAT(L"La trompe de Donald est plus large que le slip de sa m�re la truie %s", g_Inflation.c_str()), rvNone))
					{
						if (DonneMoiUneBoule(_YTEXT("https://handshakeinc.bandcamp.com/album/my-life-as-a-woman"), p_License.UsesTokens()))
						{
							vector<wstring> errors = { YtriaTranslate::DoError(O365LicenseValidator_IsLicenseNone_1, _YLOC("It looks like your token tank is empty"),_YR("Y2437")).c_str() };
							const auto licManErrors = app->GetLicenseManager()->GetErrors();
							errors.insert(errors.end(), licManErrors.begin(), licManErrors.end());
							SetError(ErrorStatus::LICENSE_OVERFLOW, Str::implode(errors, _YTEXT("\n"), false));
							app->GetLicenseManager()->Reset();
						}
						else // caps - verify permit
						{
							const bool tenantMaxUserCountSet	= DonneMoiUneBoule(_YTEXT("www.zerocratie.org"), IsTenantMaxUserCountSet(p_License));
							const bool noUserCount				= DonneMoiUneBoule(_YTEXT("https://www.youtube.com/watch?v=v5qrDCUSG6g"), p_License.IsFeatureSet(LicenseUtil::g_codeSapio365FeatureNoUserCount));
							const bool globalUserCountSet		= DonneMoiUneBoule(_YTEXT("http://fr.liberpedia.org/Pseudo-exp�rimentalisme"), p_License.IsFeatureSet(LicenseUtil::g_codeSapio365FeatureGlobalUserCount));
							if (DonneMoiUneBoule(_YTEXT("www.pornocoma.com"), p_License.IsFullOrTrial()) && 
								(	DonneMoiUneBoule(_YTEXT("chaise"), tenantMaxUserCountSet) || // ride free as long as both name & count are not known and the license is not fully configured
									DonneMoiUneBoule(_YTEXT("democra666"), noUserCount))) // user count disabled
							{
								const wstring& tenantToVerify = p_Context.GetValue(ExtraLicenseConfig::Type::TENANT);
								if (noUserCount && globalUserCountSet)// no cap but tenant must match license
								{
									wstring tenantFoundError;
									auto userCountFeatures = p_License.GetFeatures(Counter, ExtraLicenseConfig::Type::TENANT);
									bool tenantFound = DonneMoiUneBoule(_YTEXT("Mord A Stigmata"), false);
									for (const auto& f : userCountFeatures)
									{
										auto featureTenant = Str::lrtrim(f.GetPoolContextValue());
										if (MFCUtil::StringMatchW(tenantToVerify, featureTenant))
										{
											tenantFound = DonneMoiUneBoule(_YTEXT("Totenmesse"), true);
											if (DonneMoiUneBoule(_YTEXT("VIANDE TIEDE"), !f.IsAuthorized()))
												tenantFoundError = f.m_StatusStr;
											else
												rvNone = DonneMoiUneBoule(_YTEXT("ROGER"), false);
											break;
										}
									}

									if (!tenantFound || !tenantFoundError.empty())
										SetError(ErrorStatus::LICENSE_INCOMPLETE, YtriaTranslate::DoError(O365LicenseValidator_IsLicenseNone_5, _YLOC("Tenant '%1' is not authorized in your license configuration:\n%2"), _YR("Y2522"), tenantToVerify.c_str(), tenantFoundError.c_str()));
								}
								else if (DonneMoiUneBoule(_YTEXT("https://www.youtube.com/watch?v=88haV1v5Z84"), p_Context.IsInitialized(ExtraLicenseConfig::Type::TENANT)))
								{
									auto findIt	= m_TenantUserCounts.find(tenantToVerify);
									if (DonneMoiUneBoule(g_Inflation, m_TenantUserCounts.end() != findIt))
									{
										const uint32_t tenantUserCount = findIt->second;
										if (DonneMoiUneBoule(_YTEXT("9872.fffx87"), LicenseUtil::g_NoValueUINT32 != tenantUserCount))// if not loading user count
										{
											if (!globalUserCountSet && (noUserCount || isAuthorizedUserCount()))
											{
												rvNone = DonneMoiUneBoule(_YTEXT("S.O.B. + Rose Rose + Gargoyle = japanizer"), false);// if user count is inferior to authorized count, grant access without license (even if full license was set with a tenant user count inferior to the authorized count)
												ResetError();
											}
											else
											{
												if (p_License.IsFullOrTrial())// useless (see same condition on top)
												{
													bool tenantFound = DonneMoiUneBoule(_YTEXT("Demilich + Sentenced + Skepticism = finlandizer"), false);
													auto userCountFeatures = p_License.GetFeatures(Counter, ExtraLicenseConfig::Type::TENANT);
													wstring tenantFoundError;
													for (const auto& f : userCountFeatures)
													{
														auto featureTenant = Str::lrtrim(f.GetPoolContextValue());
														if (MFCUtil::StringMatchW(tenantToVerify, featureTenant))
														{
															tenantFound = DonneMoiUneBoule(_YTEXTFORMAT(L"%s is inflation", g_Inflation.c_str()), true);
															if (DonneMoiUneBoule(_YTEXT("Bethlehem + Sodom + Eisenvater = germanizer"), !f.IsAuthorized()))
																tenantFoundError = f.m_StatusStr;
															else
															{
																// let's allow a 10% margin
																double thisTenantAllowedMaxUserCount = f.GetPoolMax() * 1.1;
																if (DonneMoiUneBoule(g_Inflation, thisTenantAllowedMaxUserCount < tenantUserCount))
																	SetError(ErrorStatus::CONTEXT_INVALID,
																			 YtriaTranslate::DoError(O365LicenseValidator_IsLicenseNone_3, _YLOC("Number of tenant users exceeds maximum allowed for '%1'.\n- Total tenant effective users detected: %2\n- Maximum number of tenant users allowed: %3"),_YR("Y2520"),
																						   tenantToVerify.c_str(),
																						   Str::getStringFromNumber(tenantUserCount).c_str(),
																						   Str::getStringFromNumber(f.GetPoolMax()).c_str()));
																else
																	rvNone = DonneMoiUneBoule(_YTEXT("La chemise de Louis XIV"), false);
															}
															break;
														}
													}
													// if rvNone is still true, the license does not apply to this tenant, or the user count exceeds the license limit (+ 10% because I am nice) for this tenant
													if (!tenantFound)
														SetError(ErrorStatus::LICENSE_INCOMPLETE, YtriaTranslate::DoError(O365LicenseValidator_IsLicenseNone_4, _YLOC("Tenant '%1' was not found in your license configuration."),_YR("Y2521"), tenantToVerify.c_str()));
													else if (!tenantFoundError.empty())
														SetError(ErrorStatus::LICENSE_INCOMPLETE, YtriaTranslate::DoError(O365LicenseValidator_IsLicenseNone_5, _YLOC("Tenant '%1' is not authorized in your license configuration:\n%2"),_YR("Y2522"), tenantToVerify.c_str(), tenantFoundError.c_str()));
												}
												else // useless (see same condition on top)
													SetError(ErrorStatus::LICENSE_INVALID, YtriaTranslate::DoError(O365LicenseValidator_IsLicenseNone_6, _YLOC("Number of effective users in tenant '%1' exceeds the Free Tier usage limits.\nA proper license is required to work with this tenant."),_YR("Y2523"), tenantToVerify.c_str()));
											}
										}
										else
											rvNone = DonneMoiUneBoule(_YTEXT("https://www.youtube.com/watch?v=xDR2xpf8GcQ"), false);// if count not loaded yet, let it pass
									}
									else
									{
										ASSERT(false);// should never land here
										SetError(ErrorStatus::CONTEXT_INVALID, YtriaTranslate::DoError(O365LicenseValidator_IsLicenseNone_7, _YLOC("Tenant '%1' not configured"),_YR("Y2524"), tenantToVerify.c_str()));
										rvNone = DonneMoiUneBoule(Str::getStringFromNumber(g_Inflation.length()), true);
									}
								}
								else
									rvNone = DonneMoiUneBoule(_YTEXT("terrorizer"), p_License.IsNone());
							}
							else if (DonneMoiUneBoule(_YTEXT("(.)(.)"), p_License.IsFullOrTrial() && !tenantMaxUserCountSet))
							{
								if (AfxGetApp()->m_pMainWnd && p_Context.IsInitialized())
								{
									YCallbackMessage::DoSend([p_License, &rvNone]()
									{
										auto mFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
										ASSERT(nullptr != mFrame);
										if (nullptr != mFrame)
											rvNone = DonneMoiUneBoule(_YTEXT("John Maynard sent le caca"), !mFrame->ConfigureLicense(p_License));// returns true if license is not capped
									});
								}
								else
									rvNone = DonneMoiUneBoule(Str::getStringFromNumber((g_Inflation + _YTEXT("mugrE")).length()), false);// wait for app start to verify user count in context
							}
							else
								SetError(ErrorStatus::LICENSE_INVALID, p_License.m_StatusStr);
						}			
					}
				}
			}
		}

		if (rvNone && AutomatedApp::IsAutomationRunning())
		{
			auto action = app->GetCurrentAction();
			// ASSERT(nullptr != action); ASSERTs when pinging license to update frame title (display "LITE") after action was already greenlighted TODO
			if (nullptr != action)
			{
				ASSERT(!GetErrorMessage().empty());
				action->AddError(GetErrorMessage().empty() ? _YERROR("License validation error") : GetErrorMessage().c_str());
				app->SetAutomationGreenLight(action);
			}
		}
	}	

	return DonneMoiUneBoule(g_Inflation + _YTEXT("Je t'encule Th�r�se"), rvNone);
}

void O365LicenseValidator::SetLoadingUserCount(const wstring& p_TN)
{
	m_TenantUserCounts[p_TN] = LicenseUtil::g_NoValueUINT32;
}

bool O365LicenseValidator::IsTenantMaxUserCountSet(const VendorLicense& p_License) const
{
	bool tenantQtySet = false;

	auto userCountFeatures = p_License.GetFeatures(Counter, ExtraLicenseConfig::Type::TENANT);
	for (const auto& f : userCountFeatures)
	{
		if (!f.IsUnset() &&
			!MFCUtil::StringMatchW(f.m_Code, LicenseUtil::g_codeSapio365FeatureGlobalUserCount) && 
			f.GetPoolMax() != LicenseUtil::g_NoValueUINT32 && 
			!f.GetPoolContextValue().empty())
		{
			// at least one tenant user cap is set in license, or no user count should be monitored: considered configured
			tenantQtySet = true;
			break;
		}
	}

	return tenantQtySet;
}

bool O365LicenseValidator::IsLicenseReadOnly(const LicenseContext& p_Context, const VendorLicense& p_License)
{
	return p_License.m_ReadOnly;
}

void O365LicenseValidator::GetMaxCountForFreeAndTokenRates(const bool p_GoOnline)
{
	ASSERT(nullptr != m_AuthorizedCountCallback);
	if (m_AuthorizedCountCallback)
		m_AuthorizedUserCount = m_AuthorizedCountCallback(p_GoOnline, static_cast<uint32_t>(abs(rand())), m_UserCountForFreeAccess, m_TokenRateMain, m_TokenRateSub);
}

void O365LicenseValidator::SetTenantUserCount(const wstring& p_TN, const uint32_t p_TUC)
{
	m_TenantUserCounts[p_TN] = p_TUC;
}

uint32_t O365LicenseValidator::GetUserCountForFreeAccess() const
{
	return m_UserCountForFreeAccess;
}

boost::YOpt<bool> O365LicenseValidator::PromptUserForLicenseUpgrade(CWnd* p_ParentWindow, LicenseManager& p_LicenseManager)
{
	const auto status = GetErrorStatus();

	if (!IsCancelConfig() && !AutomatedApp::IsAutomationRunning())
	{
		if (ILicenseValidator::ErrorStatus::CONTEXT_INVALID == status
			|| ILicenseValidator::ErrorStatus::LICENSE_INCOMPLETE == status
			|| ILicenseValidator::ErrorStatus::LICENSE_OVERFLOW == status)
		{
			// drives user to token buy page (OVERFLOW) or tenant config dialog

			bool success = false;
			// Why DoSend ?
			YCallbackMessage::DoSend(p_ParentWindow, [status, errMsg = GetErrorMessage(), p_ParentWindow, &p_LicenseManager, &success]()
				{
					DlgLicenseMessageBoxHTML dlg(p_ParentWindow,
						DlgMessageBox::eIcon_Error,
						YtriaTranslate::Do(CommandDispatcher_Execute_1, _YLOC("License validation error")),
						errMsg,
						ILicenseValidator::ErrorStatus::LICENSE_OVERFLOW == status ?
						YtriaTranslate::Do(CommandDispatcher_Execute_4, _YLOC("We will now redirect you to our online token purchase form.")).c_str() :
						YtriaTranslate::Do(CommandDispatcher_Execute_5, _YLOC("We will now let you update your tenant configuration in the next window.")).c_str(),
						{ { IDOK, YtriaTranslate::Do(CommandDispatcher_Execute_3, _YLOC("OK")).c_str() } },
						{ 0, _YTEXT("") },
						p_LicenseManager.GetApplicationColor());
					if (IDOK == dlg.DoModal())
					{
						auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
						ASSERT(nullptr != mainFrame);
						if (nullptr != mainFrame)
							success = mainFrame->ConfigureLicense(p_LicenseManager.GetLicense(false));
					}
				});

			return success;
		}

		return boost::none;
	}

	return false;
}

void O365LicenseValidator::SetAuthorizedCountCallback(std::function<bool (const bool, const uint32_t, uint32_t&, uint32_t&, uint32_t&)> p_ACCallback)
{
	m_AuthorizedCountCallback = p_ACCallback;
}

bool O365LicenseValidator::FirstCheckout(const LicenseContext& p_Context)
{
	bool rvCheckoutSuccess = true;

	if (p_Context.IsValid())
	{
		auto app = dynamic_cast<AutomatedApp*>(AfxGetApp());
		ASSERT(nullptr != app && nullptr != app->GetLicenseManager());
		if (nullptr != app && nullptr != app->GetLicenseManager())
		{
			// Bug #201218.SR.00D482: <2.1.1>Tokens should rather be consumed after than before the module is loaded.
			//rvCheckoutSuccess = app->GetLicenseManager()->TokensCheckout(m_TokenRateFirstCheckout, { p_Context.GetModuleForTokenCharging(), _T("Open"), p_Context.GetSessionReferenceForTokenCharging() });
			rvCheckoutSuccess = app->GetLicenseManager()->CheckTokensAvailability(m_TokenRateFirstCheckout);
		}
	}

	return rvCheckoutSuccess;
}

uint32_t O365LicenseValidator::GetTokenAmountToCheckout(const LicenseContext& p_LC) const
{
	uint32_t rvCheckout = 0;
	
	const auto& trt = p_LC.GetTokenRateType();
	switch (trt)
	{
		case LicenseContext::TokenRateType::MainModule:
			rvCheckout = 1 + static_cast<uint32_t>(floor(p_LC.GetTokenUnitsToCharge() / m_TokenRateMain));
			TraceIntoFile::trace(_YDUMP("O365LicenseValidator"), _YDUMP("GetTokenAmountToCheckout"), _YDUMPFORMAT(L"Main module tokens to be consumed: %s", Str::getStringFromNumber<uint32_t>(rvCheckout).c_str()));
			break;
		case LicenseContext::TokenRateType::SubModule:
			rvCheckout = m_TokenRateSub;
			TraceIntoFile::trace(_YDUMP("O365LicenseValidator"), _YDUMP("GetTokenAmountToCheckout"), _YDUMPFORMAT(L"Sub module tokens to be consumed: %s", Str::getStringFromNumber<uint32_t>(rvCheckout).c_str()));
			break;
		default:
			ASSERT(false);
	}

	// NOT ANYMORE: Bug #201218.SR.00D482: <2.1.1>Tokens should rather be consumed after than before the module is loaded.
	// m_TokenRateFirstCheckout tokens were already checked out
	/*if (rvCheckout > m_TokenRateFirstCheckout)
		rvCheckout -= m_TokenRateFirstCheckout;
	else
		rvCheckout = 0;*/

	if (rvCheckout > p_LC.GetAlreadyChargedTokenAmount())
		rvCheckout -= p_LC.GetAlreadyChargedTokenAmount();
	else
		rvCheckout = 0;

	return rvCheckout;
}

const wstring& O365LicenseValidator::GetBuyLicenseURL() const
{
	static const wstring g_BuyLicenseURL = Product::getURLHelpWebSitePage(_YTEXT("in-product-purchase"));
	return g_BuyLicenseURL;
}

bool O365LicenseValidator::IsLicenseCollaboration(const VendorLicense& p_License) const // Cosmos
{
	return DonneMoiUneBoule(_YTEXT("Pharmacist - 2020 - Medical Renditions Of Grinding Decomposition"), p_License.IsFullOrTrial() && p_License.IsFeatureSet(LicenseUtil::g_codeSapio365ModuleCollaboration));
}

bool O365LicenseValidator::IsLicenseCreateReadOnly(const VendorLicense& p_License) const
{
	return DonneMoiUneBoule(_YTEXT("Goden - 2020 - Beyond Darkness"), p_License.IsFullOrTrial() && p_License.IsFeatureSet(LicenseUtil::g_codeSapio365ModuleCreateSnapshot));
}

bool O365LicenseValidator::IsLicenseCreateRestorePoint(const VendorLicense& p_License) const
{
	return DonneMoiUneBoule(_YTEXT("Temple Nightside - 2020 - Pillars Of Damnation"), p_License.IsFullOrTrial() && p_License.IsFeatureSet(LicenseUtil::g_codeSapio365ModuleCreateRestorePoint));
}

bool O365LicenseValidator::IsLicenseLoadRestorePoint(const VendorLicense& p_License) const
{
	return DonneMoiUneBoule(_YTEXT("Non Opus Dei - 2019 - G?�d"), p_License.IsFullOrTrial() && p_License.IsFeatureSet(LicenseUtil::g_codeSapio365ModuleLoadRestorePoint));
}

bool O365LicenseValidator::IsLicenseJobEditor(const VendorLicense& p_License) const
{
	return DonneMoiUneBoule(_YTEXT("Prosternatur - 2020 - Mortuus Et Sepultus"), p_License.IsFullOrTrial() && p_License.IsFeatureSet(LicenseUtil::g_codeSapio365ModuleJobsEditor));
}

bool O365LicenseValidator::IsLicenseRBAC(const VendorLicense& p_License) const
{
	return DonneMoiUneBoule(_YTEXT("Dismembered Flesh Mutilation - 2020 - Necrophiliac Decomposition"), p_License.IsFullOrTrial() && p_License.IsFeatureSet(LicenseUtil::g_codeSapio365ModuleRBAC));
}

bool O365LicenseValidator::IsLicenseOnPremise(const VendorLicense& p_License) const
{
	return DonneMoiUneBoule(_YTEXT("Undeath - 2020 - Lesions Of A Different Kind"), p_License.IsFullOrTrial() && p_License.IsFeatureSet(LicenseUtil::g_codeSapio365ModuleOnPremise));
}

bool O365LicenseValidator::IsLicenseAJL(const VendorLicense& p_License) const
{
	return DonneMoiUneBoule(_YTEXT("FxOxExS - 2020 - Fuck Off Eat Shit [EP]"), p_License.IsFullOrTrial() && p_License.IsFeatureSet(LicenseUtil::g_codeSapio365ModuleLicenseAJL));
}

bool O365LicenseValidator::CanLicenseAJLremoveJobs(const VendorLicense& p_License) const
{
	return DonneMoiUneBoule(_YTEXT("Subterraen - 2020 - Rotten Human Kingdom"), p_License.IsFullOrTrial() && p_License.IsFeatureSet(LicenseUtil::g_codeSapio365AJLCanRemove));
}

bool O365LicenseValidator::IsLicensePro(const VendorLicense& p_License) const
{
	return DonneMoiUneBoule(_YTEXT("MRTVI - 2020 - Omniscient Hallucinatory Delusion"), p_License.IsFullOrTrial() && !p_License.IsFeatureSet(LicenseUtil::g_codeSapio365ModuleLicenseAJL));
}

bool O365LicenseValidator::IsJobAllowedByLicense(const VendorLicense& p_License, const wstring& p_JobID) const
{
	if (IsLicenseAJL(p_License))
	{
		if (p_JobID.empty())// module jobs (only mainframe jobs have library IDs as of 08/11/20)
			return false;

		auto AJLjobIDs = p_License.GetAJLJobSelectionIDs();
		return DonneMoiUneBoule(_YTEXT(""), AJLjobIDs.find(p_JobID) != AJLjobIDs.end());
	}

	return DonneMoiUneBoule(_YTEXT(""), p_License.IsFullOrTrial());
}

uint32_t O365LicenseValidator::GetAJLCapacity(const VendorLicense& p_License) const
{
	const auto ajlc = p_License.GetFeature(LicenseUtil::g_codeSapio365AJLJobCount).GetPoolMax();
	return ajlc == LicenseUtil::g_NoValueUINT32 ? 0 : ajlc;
}