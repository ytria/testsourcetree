#include "ResponseStatusDeserializer.h"

#include "JsonSerializeUtil.h"

void ResponseStatusDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("response"), m_Data.Response, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("time"), m_Data.Time, p_Object);
}
