#pragma once

#include "RowModification.h"

class FakeModification : public RowModification
{
public:
	FakeModification(O365Grid& p_Grid, const row_pk_t& p_RowKey);
	~FakeModification();

	vector<ModificationLog> GetModificationLogs() const override;

	void RefreshState() override;
};
