#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "ObjectIdentity.h"

class ObjectIdentityDeserializer : public JsonObjectDeserializer, public Encapsulate<ObjectIdentity>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};
