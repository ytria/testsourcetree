#include "FrameSpGroups.h"
#include "GridSpGroups.h"
#include "Command.h"

IMPLEMENT_DYNAMIC(FrameSpGroups, GridFrameBase)

void FrameSpGroups::BuildView(const O365DataMap<BusinessSite, vector<Sp::Group>>& p_SpSites, bool p_FullPurge)
{
    m_Grid.BuildView(p_SpSites, p_FullPurge);
}

O365Grid& FrameSpGroups::GetGrid()
{
    return m_Grid;
}

void FrameSpGroups::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameSpGroups::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameSpGroups::ApplySpecific(bool p_Selected)
{
    ASSERT(false); //TODO
}

void FrameSpGroups::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::SpGroup, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameSpGroups::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::SpGroup, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

bool FrameSpGroups::HasApplyButton()
{
    return true;
}

void FrameSpGroups::createGrid()
{
    m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameSpGroups::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigUsers();
}

bool FrameSpGroups::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
    m_CreateRefreshPartial = true;
    CreateViewGroup(tab);
    CreateRefreshGroup(tab);

    return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameSpGroups::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
    AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

    return statusRV;
}
