#include "RecipientDeserializer.h"

#include "EmailAddressDeserializer.h"
#include "JsonSerializeUtil.h"

void RecipientDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    EmailAddressDeserializer ead;
    if (JsonSerializeUtil::DeserializeAny(ead, _YTEXT("emailAddress"), p_Object))
        m_Data.m_EmailAddress = ead.GetData();
}
