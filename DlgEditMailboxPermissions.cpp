#include "DlgEditMailboxPermissions.h"

#include "GridMailboxPermissions.h"
#include "DlgFormsHTMLConfig.h"
#include "MailboxPermissionsAccessRights.h"

DlgEditMailboxPermissions::DlgEditMailboxPermissions(GridMailboxPermissions& p_Grid, CWnd& p_Parent)
	: DlgFormsHTML(Action::EDIT, &p_Parent)
	, m_Grid(p_Grid)
{
}

const map<wstring, bool>& DlgEditMailboxPermissions::GetChanges() const
{
	return m_Changes;
}

const vector<vector<GridBackendField>>& DlgEditMailboxPermissions::GetRowPks() const
{
	return m_Pks;
}

void DlgEditMailboxPermissions::generateJSONScriptData()
{
	initMain(_YTEXT("DlgEditMailboxPermissions"));
	
	boost::YOpt<bool> changeOwner, changePermission, deleteItem, externalAccount, fullAccess, readPermission;
	getAccessRightsValues(changeOwner, changePermission, deleteItem, externalAccount, fullAccess, readPermission);

	getGenerator().Add(BoolToggleEditor({ {MailboxPermissionsAccessRights::g_FullAccess, _YTEXT("Full Access"), static_cast<uint32_t>(fullAccess ? EditorFlags::DIRECT_EDIT : EditorFlags::NOCHANGE), fullAccess ? *fullAccess : false}, {_YTEXT(""), _YTEXT("")} }));
	getGenerator().Add(BoolToggleEditor({ {MailboxPermissionsAccessRights::g_ExternalAccount, _YTEXT("External Account"), static_cast<uint32_t>(externalAccount ? EditorFlags::DIRECT_EDIT : EditorFlags::NOCHANGE), externalAccount ? *externalAccount : false}, {_YTEXT(""), _YTEXT("")} }));
	getGenerator().Add(BoolToggleEditor({ {MailboxPermissionsAccessRights::g_DeleteItem, _YTEXT("Delete Item"), static_cast<uint32_t>(deleteItem ? EditorFlags::DIRECT_EDIT : EditorFlags::NOCHANGE), deleteItem ? *deleteItem : false}, {_YTEXT(""), _YTEXT("")} }));
	getGenerator().Add(BoolToggleEditor({ {MailboxPermissionsAccessRights::g_ReadPermission, _YTEXT("Read Permission"), static_cast<uint32_t>(readPermission ? EditorFlags::DIRECT_EDIT : EditorFlags::NOCHANGE), readPermission ? *readPermission : false}, {_YTEXT(""), _YTEXT("")} }));
	getGenerator().Add(BoolToggleEditor({ {MailboxPermissionsAccessRights::g_ChangePermission, _YTEXT("Change Permission"), static_cast<uint32_t>(changePermission ? EditorFlags::DIRECT_EDIT : EditorFlags::NOCHANGE), changePermission ? *changePermission : false}, {_YTEXT(""), _YTEXT("")} }));
	getGenerator().Add(BoolToggleEditor({ {MailboxPermissionsAccessRights::g_ChangeOwner, _YTEXT("Change Owner"), static_cast<uint32_t>(changeOwner ? EditorFlags::DIRECT_EDIT : EditorFlags::NOCHANGE), changeOwner ? *changeOwner : false}, {_YTEXT(""), _YTEXT("")} }));
	
	getGenerator().addApplyButton(LocalizedStrings::g_ApplyButtonText);
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
	getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);
}

bool DlgEditMailboxPermissions::processPostedData(const IPostedDataTarget::PostedData& data)
{
	for (const auto& kv : data)
		m_Changes[kv.first] = kv.second == _YTEXT("true") ? true : false;
	
	return true;
}

vector<GridBackendRow*> DlgEditMailboxPermissions::getSelectedMailboxPermissionsRows()
{
	vector<GridBackendRow*> rows;

	for (auto row : m_Grid.GetSelectedRows())
	{
		if (nullptr != row && GridUtil::isBusinessType<MailboxPermissions>(row) && !row->IsGroupRow())
			rows.push_back(row);
	}

	return rows;
}

void DlgEditMailboxPermissions::getAccessRightsValues(boost::YOpt<bool>& p_ChangeOwner, boost::YOpt<bool>& p_ChangePermission, boost::YOpt<bool>& p_DeleteItem, boost::YOpt<bool>& p_ExternalAccount, boost::YOpt<bool>& p_FullAccess, boost::YOpt<bool>& p_ReadPermission)
{
	bool isFirst = true;
	vector<GridBackendRow*> mailboxRows = getSelectedMailboxPermissionsRows();
	for (const auto& row : mailboxRows)
	{
		vector<wstring> accessRights = m_Grid.GetAccessRights(*row);
		if (isFirst)
		{
			p_ChangeOwner = std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_ChangeOwner) != accessRights.end();
			p_ChangePermission = std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_ChangePermission) != accessRights.end();
			p_DeleteItem = std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_DeleteItem) != accessRights.end();
			p_ExternalAccount = std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_ExternalAccount) != accessRights.end();
			p_FullAccess = std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_FullAccess) != accessRights.end();
			p_ReadPermission = std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_ReadPermission) != accessRights.end();
			isFirst = false;
		}
		else
		{
			if (p_ChangeOwner && ((std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_ChangeOwner) != accessRights.end()) != *p_ChangeOwner))
				p_ChangeOwner = boost::none;
			if (p_ChangePermission && ((std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_ChangePermission) != accessRights.end()) != *p_ChangePermission))
				p_ChangePermission = boost::none;
			if (p_DeleteItem && ((std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_DeleteItem) != accessRights.end()) != *p_DeleteItem))
				p_DeleteItem = boost::none;
			if (p_ExternalAccount && ((std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_ExternalAccount) != accessRights.end()) != *p_ExternalAccount))
				p_ExternalAccount = boost::none;
			if (p_FullAccess && ((std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_FullAccess) != accessRights.end()) != *p_FullAccess))
				p_FullAccess = boost::none;
			if (p_ReadPermission && ((std::find(accessRights.begin(), accessRights.end(), MailboxPermissionsAccessRights::g_ReadPermission) != accessRights.end()) != *p_ReadPermission))
				p_ReadPermission = boost::none;
		}
		addToPks(row);
	}
}

void DlgEditMailboxPermissions::addToPks(GridBackendRow* p_Row)
{
	row_pk_t rowPk;
	m_Grid.GetRowPK(p_Row, rowPk);
	m_Pks.push_back(rowPk);
}

