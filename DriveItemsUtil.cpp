#include "DriveItemsUtil.h"

bool DriveItemsUtil::IsCorrespondingRow(GridDriveItems& p_Grid, GridBackendRow* p_Row, const GridDriveItems::DriveItemPermissionInfo& p_PermissionInfo)
{
    if (p_Grid.GetMultivalueManager().IsRowExploded(p_Row, p_Grid.m_ColPermissionId)) // !Collapsed
    {
        // Exploded row
        if (p_Row->GetField(p_Grid.m_ColRole).GetValueStr() != ROLE_VALUE_OWNER && // Can't modify a permission with "owner" role
            PooledString(p_Row->GetField(p_Grid.m_ColMetaDriveId).GetValueStr()) == p_PermissionInfo.m_DriveId &&
            PooledString(p_Row->GetField(p_Grid.m_ColId).GetValueStr()) == p_PermissionInfo.m_DriveItemId &&
            PooledString(p_Row->GetField(p_Grid.GetColumnPermissionId()).GetValueStr()) == p_PermissionInfo.m_PermissionId)
        {
            return true;
        }
    }
    else
    {
        // Collapsed row
        GridBackendField& fieldDriveId = p_Row->GetField(p_Grid.m_ColMetaDriveId);
        GridBackendField& fieldDriveItemId = p_Row->GetField(p_Grid.m_ColId);
        GridBackendField& fieldPermissionId = p_Row->GetField(p_Grid.GetColumnPermissionId());
        if (fieldDriveId.HasValue() && fieldDriveItemId.HasValue() && fieldPermissionId.HasValue())
        {
            // Drive id and drive item id is corresponding?
            if (PooledString(fieldDriveId.GetValueStr()) == p_PermissionInfo.m_DriveId &&
                PooledString(fieldDriveItemId.GetValueStr()) == p_PermissionInfo.m_DriveItemId)
            {
                // Permission id corresponding?
                std::vector<PooledString>* permissionIds = fieldPermissionId.GetValuesMulti<PooledString>();
                if (nullptr != permissionIds)
                {
                    for (const auto& permId : *permissionIds)
                    {
                        if (permId == p_PermissionInfo.m_PermissionId)
                            return true;
                    }
                }
                else if (PooledString(fieldPermissionId.GetValueStr()) == p_PermissionInfo.m_PermissionId) // Row with only one permission
                {
                    return true;
                }
            }
        }
    }
    return false;
}

const wstring DriveItemsUtil::ROLE_VALUE_READ = _YTEXT("read");
const wstring DriveItemsUtil::ROLE_VALUE_WRITE = _YTEXT("write");
const wstring DriveItemsUtil::ROLE_VALUE_OWNER = _YTEXT("owner");
const wstring DriveItemsUtil::TARGET_ANONYMOUS = _YTEXT("[-anonymous-]");
const wstring DriveItemsUtil::TARGET_ORGANIZATION = _YTEXT("[-organization-]");
// const wstring DriveItemsUtil::TARGET_USERS = _YTEXT("[-users-]");
const wstring DriveItemsUtil::TARGET_ANONYMOUS_NUDE = _YTEXT("anonymous");
const wstring DriveItemsUtil::TARGET_ORGANIZATION_NUDE = _YTEXT("organization");
const wstring DriveItemsUtil::TARGET_USERS_NUDE = _YTEXT("users");
