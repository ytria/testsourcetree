#pragma once

#include "DlgFormsHTML.h"

class DlgOnPremServerConfig : public DlgFormsHTML
{
public:
	DlgOnPremServerConfig(CWnd* p_Parent);

	//Returns true if processed
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

protected:
	virtual wstring getDialogTitle() const override;

private:
	virtual void generateJSONScriptData() override;

	boost::YOpt<wstring> m_Server;
	boost::YOpt<wstring> m_Username;
	boost::YOpt<wstring> m_Password;
};
