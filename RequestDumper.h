#pragma once

#include "IRequestDumper.h"
#include "SessionIdentifier.h"

class RequestDumper : public IRequestDumper
{
public:
	RequestDumper(const SessionIdentifier& p_SessionIdentifier);
	~RequestDumper() override;

	virtual void DumpResponse(uint32_t p_RequestID, const RestResultInfo& p_Info) override;
	virtual void DumpRequest(uint32_t p_RequestID, const web::http::http_request& p_Request, const WebPayload& p_Body) override;

	static vector<wstring> ClearPaths();
	static wstring GetDumpPath();

private:
	void Dump(uint32_t p_RequestID, const web::http::http_request& p_Request, const wstring& p_Body, bool p_Response, web::http::status_code p_ResponseCode);
	void newFile();

private:
	SessionIdentifier m_SessionIdentifier;
	wstring m_FolderPath;

	// Change this if we want a separate file per request
	wstring m_FilePath;
	wstring m_FileNameBase;
	int m_FileCounter;
	std::unique_ptr<std::ofstream> m_FileStream;

	static const int g_MaxFileSizeInBytes;
	static vector<wstring> g_DumpFilePaths;
	static const wstring g_RequestHeaderPrefix;
	static const wstring g_ResponseHeaderPrefix;
};

