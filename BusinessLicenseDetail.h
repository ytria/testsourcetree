#pragma once

#include "BusinessObject.h"
#include "LicenseDetail.h"

class Sapio365Session;

class BusinessLicenseDetail : public BusinessObject
{
public:
	BusinessLicenseDetail() = default;
	BusinessLicenseDetail(const LicenseDetail& p_LicenseDetail);

	const vector<ServicePlanInfo>& GetServicePlanInfos() const;
	void SetServicePlanInfos(const vector<ServicePlanInfo>& val);

	const boost::YOpt<PooledString>& GetSkuId() const;
	void SetSkuId(const boost::YOpt<PooledString>& val);

	const boost::YOpt<PooledString>& GetSkuPartNumber() const;
	void SetSkuPartNumber(const boost::YOpt<PooledString>& val);

	static LicenseDetail ToLicenseDetail(const BusinessLicenseDetail& p_BusinessLicenseDetail);

protected:
	virtual pplx::task<vector<HttpResultWithError>> SendEditRequest(std::shared_ptr<Sapio365Session> p_MSGraphSession, YtriaTaskData p_TaskData) const override;
	virtual TaskWrapper<HttpResultWithError> SendCreateRequest(std::shared_ptr<Sapio365Session> p_MSGraphSession, YtriaTaskData p_TaskData) const override;
	virtual TaskWrapper<HttpResultWithError> SendDeleteRequest(std::shared_ptr<Sapio365Session> p_MSGraphSession, YtriaTaskData p_TaskData) const override;

private:
	vector<ServicePlanInfo> m_ServicePlans;
	boost::YOpt<PooledString> m_SkuId;
	boost::YOpt<PooledString> m_SkuPartNumber;

    RTTR_ENABLE(BusinessObject)
        RTTR_REGISTRATION_FRIEND
        friend class LicenseDetailDeserializer;
};