#include "GetDelegatesResponseDeserializer.h"

#include "xercesc\dom\DOMNodeList.hpp"
#include "DelegateUserDeserializer.h"

void Ex::GetDelegatesResponseDeserializer::DeserializeObject(const DOMElement& p_Envelope)
{
    DOMElement* body = GetSoapBody(p_Envelope);
    ASSERT(nullptr != body);
    if (nullptr != body)
    {
        auto getDelegateResponse = body->getFirstElementChild();
        ASSERT(nullptr != getDelegateResponse);
        if (nullptr != getDelegateResponse)
        {
            if (wcscmp(getDelegateResponse->getAttribute(_YTEXT("ResponseClass")), _YTEXT("Success")) == 0)
            {
                // Global success
                auto responseMessages = GetElementByTagName(*getDelegateResponse, _YTEXT("ResponseMessages"));
                // If we haven't received ResponseMessages node, it means the user doesn't have any delegate
                if (nullptr != responseMessages) 
                {
                    auto delegateUserResponses = responseMessages->getElementsByTagName(_YTEXT("DelegateUserResponseMessageType"));
                    for (size_t i = 0; i < delegateUserResponses->getLength(); ++i)
                    {
                        auto delegateUserResponseMessageType = dynamic_cast<DOMElement*>(delegateUserResponses->item(i));
                        ASSERT(nullptr != delegateUserResponseMessageType);
                        if (nullptr != delegateUserResponseMessageType)
                        {
                            bool hasSpecificError = ElementHasError(*delegateUserResponseMessageType);
                            if (hasSpecificError)
                            {
                                auto errorCodeAndMessage = GetError(*delegateUserResponseMessageType);

                                Ex::DelegateUser delegate;
                                SapioError error(errorCodeAndMessage.first + wstring(_YTEXT(" - ")) + errorCodeAndMessage.second, 0);
                                delegate.SetError(error);
                                m_DelegateUsers.push_back(delegate);
                            }
                            else
                            {
                                DelegateUserDeserializer userDeserializer;
                                auto delegateUser = GetElementByTagName(*delegateUserResponseMessageType, _YTEXT("DelegateUser"));
                                ASSERT(nullptr != delegateUser);
                                if (nullptr != delegateUser)
                                {
                                    userDeserializer.DeserializeObject(*delegateUser);
                                    m_DelegateUsers.push_back(userDeserializer.GetObject());
                                }
                            }

                        }
                    }

                }
                m_DeliverMeetingRequests = DeserializeString(*getDelegateResponse, _YTEXT("DeliverMeetingRequests"));
            }
            else
            {
                // Error
                m_HasGlobalError = true;

                auto errorCodeAndMessage = GetError(*getDelegateResponse);
                m_GlobalErrorResponseCode = errorCodeAndMessage.first;
                m_GlobalErrorMessage = errorCodeAndMessage.second;
                
                DelegateUser delegateUserError;
                SapioError error(m_GlobalErrorResponseCode + _YTEXT(" - ") + m_GlobalErrorMessage, 0);
                delegateUserError.SetError(error);

                m_DelegateUsers.push_back(delegateUserError);
            }
        }
    }
}

const vector<Ex::DelegateUser>& Ex::GetDelegatesResponseDeserializer::GetDelegateUsers() const
{
    return m_DelegateUsers;
}
