#pragma once

#include "AutomationUtil.h"

class IActionCommand
{
public:
	IActionCommand(const wstring& p_Name);
	virtual ~IActionCommand() = default;

	void Execute() const;
	void SetCallback(std::function<void()> p_Callback);

	const wstring& GetName() const;
	void SetAutomationSetup(const AutomationSetup& p_Setup);

protected:
	virtual void ExecuteImpl() const = 0;

	boost::YOpt<AutomationSetup> m_Setup;
	std::function<void()> m_Callback; // Derived class is responsible to call it whenever is the right moment

private:
	const wstring m_Name;
};

