#pragma once

#include "IModificationApplyer.h"

class O365Grid;

class GenericModificationsApplyer : public IModificationApplyer
{
public:
	GenericModificationsApplyer(O365Grid& p_Grid);

	void ApplyAll() override;
	void ApplySelected() override;

	static bool Confirm();

private:
	O365Grid& m_Grid;
};

