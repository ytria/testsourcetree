#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "RecurrencePattern.h"

class RecurrencePatternDeserializer : public JsonObjectDeserializer, public Encapsulate<RecurrencePattern>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

