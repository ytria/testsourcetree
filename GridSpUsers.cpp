#include "GridSpUsers.h"
#include "AutomationWizardSpUsers.h"
#include "BaseO365Grid.h"
#include "BasicGridSetup.h"
#include "FrameSpUsers.h"
#include "GridUtil.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"

BEGIN_MESSAGE_MAP(GridSpUsers, ModuleO365Grid<Sp::User>)
END_MESSAGE_MAP()

GridSpUsers::GridSpUsers()
    : m_Frame(nullptr)
{
    // FIXME: Add desired actions when the required methods are implemented below!
    UseDefaultActions(/*O365Grid::ACTION_CREATE | *//*O365Grid::ACTION_DELETE *//*| O365Grid::ACTION_EDIT | O365Grid::ACTION_VIEW*/0);
    EnableGridModifications();

    initWizard<AutomationWizardSpUsers>(_YTEXT("Automation\\SpUser"));
}

GridSpUsers::~GridSpUsers()
{
    GetBackend().Clear();
}

ModuleCriteria GridSpUsers::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_Frame->GetModuleCriteria();

    criteria.m_IDs.clear();
    for (auto row : p_Rows)
        criteria.m_IDs.insert(row->GetField(m_ColMetaId).GetValueStr());

    return criteria;
}

void GridSpUsers::BuildView(const O365DataMap<BusinessSite, vector<Sp::User>>& p_SpUsers, bool p_FullPurge)
{
    GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
        | (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
        | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
        | GridUpdaterOptions::REFRESH_PROCESS_DATA
        );
    GridUpdater updater(*this, options);

    GridIgnoreModification ignoramus(*this);
    for (const auto& keyVal : p_SpUsers)
    {
        const auto& site = keyVal.first;
        const auto& spUsers = keyVal.second;

        GridBackendField fID(site.GetID(), m_ColMetaId);
        GridBackendRow* siteRow = m_RowIndex.GetRow({ fID }, true, true);
        ASSERT(nullptr != siteRow);
        if (nullptr != siteRow)
        {
            updater.GetOptions().AddRowWithRefreshedValues(siteRow);
            updater.GetOptions().AddPartialPurgeParentRow(siteRow);

            siteRow->SetHierarchyParentToHide(true);
            siteRow->AddField(site.GetDisplayName(), m_ColMetaDisplayName);

            SetRowObjectType(siteRow, site);
            if (!site.HasFlag(BusinessObject::CANCELED))
            {
                if (keyVal.second.size() == 1 && keyVal.second[0].GetError())
                {
                    updater.OnLoadingError(siteRow, *keyVal.second[0].GetError());
                }
                else
                {
					// Clear previous error
					if (siteRow->IsError())
						siteRow->ClearStatus();

                    for (const auto& spUser : spUsers)
                    {
                        GridBackendField fieldUserId(spUser.Id ? std::to_wstring(*spUser.Id) : _YTEXT(""), m_ColUserId);
                        GridBackendField fieldSiteId(site.GetID(), m_ColMetaId);
                        GridBackendRow* userRow = m_RowIndex.GetRow({ fieldUserId, fieldSiteId }, true, true);
                        ASSERT(nullptr != userRow);
                        if (nullptr != userRow)
                        {
                            updater.GetOptions().AddRowWithRefreshedValues(userRow);
                            userRow->SetParentRow(siteRow);

                            SetRowObjectType(userRow, spUser, spUser.HasFlag(BusinessObject::CANCELED));
                            if (spUser.GetError())
                            {
                                updater.OnLoadingError(userRow, *spUser.GetError());
                            }
                            else
                            {
								// Clear previous error
								if (userRow->IsError())
									userRow->ClearStatus();

                                siteRow->AddField(site.GetDisplayName(), m_ColMetaDisplayName);
                                FillUserFields(site, spUser, userRow, updater);
                            }
                        }
                    }
                }
            }
        }
    }
}

void GridSpUsers::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameSpUsers, LicenseUtil::g_codeSapio365spUsers,
		{
			{ &m_ColMetaDisplayName, YtriaTranslate::Do(GridLists_customizeGrid_1, _YLOC("Site Display Name")).c_str(), g_FamilyOwner },
			{ &m_ColMetaUserDisplayName, g_TitleOwnerUserName, g_FamilyOwner },
			{ &m_ColMetaId, g_TitleOwnerID,	g_FamilyOwner }
		}, { Sp::User() });
		setup.Setup(*this, false);
	}

    setBasicGridSetupHierarchy({ { { m_ColMetaDisplayName, 0 } }
								,{ rttr::type::get<Sp::User>().get_id() }
								,{ rttr::type::get<Sp::User>().get_id(), rttr::type::get<BusinessSite>().get_id() } });

    addColumnGraphID(); // FIXME: Shouldn't this replace m_ColUserId?

    m_ColEmail					= AddColumn(_YUID("email"), YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_11, _YLOC("Email")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColTitle					= AddColumn(_YUID("title"), YtriaTranslate::Do(DlgBasicProperties_OnInitDialog_2, _YLOC("Title")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColLoginName				= AddColumn(_YUID("loginName"), YtriaTranslate::Do(GridSpGroups_customizeGrid_2, _YLOC("Login Name")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColPrincipalType			= AddColumn(_YUID("principalType"), YtriaTranslate::Do(GridSpGroups_customizeGrid_4, _YLOC("Principal Type")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColIsSiteAdmin			= AddColumnCheckBox(_YUID("isSiteAdmin"), YtriaTranslate::Do(GridSpUsers_customizeGrid_6, _YLOC("Is Site Admin")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColIsHiddenInUI			= AddColumnCheckBox(_YUID("isHiddenInUI"), YtriaTranslate::Do(GridSpGroups_customizeGrid_15, _YLOC("Is Hidden in UI")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
	// FIXME: Shouldn't we use O365Grid::m_ColId instead?
    m_ColUserId					= AddColumn(_YUID("id"), YtriaTranslate::Do(DlgGCVDGridSetupColumnMismatch__YTriaGridGCVDetupColumnMismatch_customizeGrid_1, _YLOC("ID")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });

    m_ColUserIdInfoNameId		= AddColumn(_YUID("userIdInfoNameID"), YtriaTranslate::Do(GridSpUsers_customizeGrid_9, _YLOC("User ID Info - Name ID")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColUserIdInfoNameIdIssuer = AddColumn(_YUID("userIdInfoNameIDIssuer"), YtriaTranslate::Do(GridSpUsers_customizeGrid_10, _YLOC("User ID Info - Name ID Issuer")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });

    AddColumnForRowPK(m_ColMetaId); // Site id
	// FIXME: Shouldn't we use O365Grid::m_ColId instead?
    AddColumnForRowPK(m_ColUserId); // User id

    m_Frame = dynamic_cast<FrameSpUsers*>(GetParentFrame());

    if (nullptr != GetAutomationWizard())
        GetAutomationWizard()->SetFrame(m_Frame);
}

wstring GridSpUsers::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Site"), m_ColMetaDisplayName } }, m_ColTitle);
}

Sp::User GridSpUsers::getBusinessObject(GridBackendRow* p_Row) const
{
    ASSERT(GridUtil::isBusinessType<Sp::User>(p_Row));
    return Sp::User();
}

void GridSpUsers::UpdateBusinessObjects(const vector<Sp::User>& p_Users, bool p_SetModifiedStatus)
{
    ASSERT(false);
}

void GridSpUsers::RemoveBusinessObjects(const vector<Sp::User>& p_Users)
{
    ASSERT(false);
}

void GridSpUsers::InitializeCommands()
{
    ModuleO365Grid<Sp::User>::InitializeCommands();

    const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);
}

void GridSpUsers::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {

    }

    ModuleO365Grid<Sp::User>::OnCustomPopupMenu(pPopup, nStart);
}

void GridSpUsers::FillUserFields(const BusinessSite& p_Site, const Sp::User& p_SpUser, GridBackendRow* p_Row, GridUpdater& p_Updater)
{
    p_Row->AddField(p_SpUser.Email, m_ColEmail);
    p_Row->AddField(p_SpUser.Title, m_ColTitle);
    p_Row->AddField(p_SpUser.Id, m_ColUserId);
    p_Row->AddField(p_SpUser.IsHiddenInUI, m_ColIsHiddenInUI);
    p_Row->AddField(p_SpUser.IsSiteAdmin, m_ColIsSiteAdmin);
    p_Row->AddField(p_SpUser.LoginName, m_ColLoginName);
    p_Row->AddField(p_SpUser.PrincipalType, m_ColPrincipalType);

    if (p_SpUser.UserId != boost::none)
    {
        p_Row->AddField(p_SpUser.UserId->NameId, m_ColUserIdInfoNameId);
        p_Row->AddField(p_SpUser.UserId->NameIdIssuer, m_ColUserIdInfoNameIdIssuer);
    }
}
