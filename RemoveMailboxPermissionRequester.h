#pragma once

#include "IRequester.h"
#include "InvokeResultWrapper.h"
#include "IRequestLogger.h"

class RemoveMailboxPermissionRequester : public IRequester
{
public:
	RemoveMailboxPermissionRequester(const wstring& p_MailboxOwner, const wstring& p_UserThatLosesRights, const wstring& p_AccessRights, const std::shared_ptr<IRequestLogger>& p_RequestLogger);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const std::shared_ptr<InvokeResultWrapper>& GetResult() const;

private:
	std::shared_ptr<InvokeResultWrapper> m_Result;
	std::shared_ptr<IRequestLogger> m_Logger;

	wstring m_MailboxOwner;
	wstring m_UserThatLosesRights;
	wstring m_AccessRights;
};

