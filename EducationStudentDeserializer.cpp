#include "EducationStudentDeserializer.h"

void EducationStudentDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("birthDate"), m_Data.m_BirthDate, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("externalId"), m_Data.m_ExternalId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("gender"), m_Data.m_Gender, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("grade"), m_Data.m_Grade, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("graduationYear"), m_Data.m_GraduationYear, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("studentNumber"), m_Data.m_StudentNumber, p_Object);
}
