#include  "RegistrySetting.h"

#include "Registry.h"
#include "SessionIdentifier.h"
#include "Str.h"

GenericRegistrySetting::GenericRegistrySetting(const wstring& p_RegistryKey, const wstring& p_RegistryValue)
    : m_RegistryKey(p_RegistryKey)
    , m_RegistryValue(p_RegistryValue)
{
	ASSERT(!m_RegistryKey.empty());
}

std::pair<bool, wstring> GenericRegistrySetting::readVal() const
{
	std::pair<bool, wstring> res;
	ASSERT(!m_RegistryValue.empty());
	if (!m_RegistryValue.empty() && (res.first = Registry::readString(HKEY_CURRENT_USER, m_RegistryKey, m_RegistryValue, res.second)))
		return res;
	res.second.clear();
	return res;
}

bool GenericRegistrySetting::writeVal(const wstring& p_Value) const
{
	ASSERT(!m_RegistryValue.empty());
	if (!m_RegistryValue.empty())
		return Registry::writeString(HKEY_CURRENT_USER, m_RegistryKey, m_RegistryValue, p_Value);
	return false;
}

bool GenericRegistrySetting::removeVal() const
{
	if (m_RegistryValue.empty())
		return Registry::eraseKey(HKEY_CURRENT_USER, m_RegistryKey);
	return Registry::erase(HKEY_CURRENT_USER, m_RegistryKey, m_RegistryValue);
}

// ==================================================================================================

// Bool specialization
static const wstring g_OptionSet = _YTEXT("1");
static const wstring g_OptionUnset = _YTEXT("0");

template<>
boost::YOpt<bool> RegistrySetting<bool>::Get() const
{
	boost::YOpt<bool> val;
	auto res = readVal();
	if (res.first)
		val = res.second == g_OptionSet;
	return val;
}

template<>
bool RegistrySetting<bool>::Set(boost::YOpt<bool> p_Val) const
{
	if (p_Val.is_initialized())
		return writeVal(*p_Val ? g_OptionSet : g_OptionUnset);

	return removeVal();
}

// ==================================================================================================

// String specialization
template<>
boost::YOpt<wstring> RegistrySetting<wstring>::Get() const
{
	boost::YOpt<wstring> val;
	auto res = readVal();
	if (res.first)
		val = res.second;
	return val;
}

template<>
bool RegistrySetting<wstring>::Set(boost::YOpt<wstring> p_Val) const
{
	if (p_Val.is_initialized())
		return writeVal(*p_Val);

	return removeVal();
}

// ==================================================================================================

// SessionIdentifier specialization
template<>
boost::YOpt<SessionIdentifier> RegistrySetting<SessionIdentifier>::Get() const
{
	boost::YOpt<SessionIdentifier> val;
	auto res = readVal();
	if (res.first)
	{
		val.emplace();
		if (!val->Deserialize(res.second))
			val.reset();

		ASSERT(val);
	}
	return val;
}

template<>
bool RegistrySetting<SessionIdentifier>::Set(boost::YOpt<SessionIdentifier> p_Val) const
{
	if (p_Val.is_initialized())
		return writeVal(p_Val->Serialize());

	return removeVal();
}

// ==================================================================================================

// Int specialization

template<>
boost::YOpt<int> RegistrySetting<int>::Get() const
{
	boost::YOpt<int> val;
	auto res = readVal();
	if (res.first)
		val = Str::getIntFromString(res.second);
	return val;
}

template<>
bool RegistrySetting<int>::Set(boost::YOpt<int> p_Val) const
{
	if (p_Val.is_initialized())
		return writeVal(std::to_wstring(*p_Val));

	return removeVal();
}
