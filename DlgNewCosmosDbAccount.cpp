#include "DlgNewCosmosDbAccount.h"


DlgNewCosmosDbAccount::DlgNewCosmosDbAccount(const map<Azure::Subscription, vector<Azure::ResourceGroup>>& p_Choices, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent)
	:ResizableDialog(IDD, p_Parent),
	m_Choices(p_Choices),
	m_Session(p_Session),
	m_HtmlView(make_unique<YAdvancedHtmlView>(true))
{
	// TODO: Load hbs sizes (see DlgEnterCosmosCNX.cpp)
	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_COSMOSDBINFO_SIZE, g_Sizes);
		ASSERT(success);
	}
}

bool DlgNewCosmosDbAccount::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	const auto it = data.find(_YTEXT("cancel"));
	if (data.end() != it)
	{
		endDialogFixed(IDCANCEL);
		return false;
	}

	auto itt = data.find(g_SubscriptionIdKey);
	ASSERT(itt != data.end());
	if (itt != data.end())
	{
		auto resGroupNameItt = data.find(itt->second);
		ASSERT(resGroupNameItt != data.end());
		if (resGroupNameItt != data.end())
		{
			m_SubscriptionId = itt->second;
			m_ResGroupName = resGroupNameItt->second;
		}
	}

	endDialogFixed(IDOK);
	return false;
}

const wstring& DlgNewCosmosDbAccount::GetSubscriptionId() const
{
	return m_SubscriptionId;
}

const wstring& DlgNewCosmosDbAccount::GetResGroupName() const
{
	return m_ResGroupName;
}

BOOL DlgNewCosmosDbAccount::OnInitDialogSpecificResizable()
{
	SetWindowText(YtriaTranslate::Do(DlgSelectSubscriptionAndResourceGroup_OnInitDialogSpecificResizable_1, _YLOC("Create Cosmos DB Account")).c_str());

	CWnd* pCtrl = GetDlgItem(IDC_GRID_PLACEHOLDER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, NULL);

	m_HtmlView->SetLinkOpeningPolicy(true, true);
	m_HtmlView->SetPostedDataTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
		{
			{ _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
			,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-cosmosDbInfo.js"), _YTEXT("") } } // This file is in resources
			,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("cosmosDbInfo.js"), _YTEXT("") } }, // This file is in resources
		}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("cosmosDbInfo.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-height"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-width"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);

	BlockVResize(true);

	return TRUE;
}

void DlgNewCosmosDbAccount::generateJSONScriptData(wstring& p_Output)
{
	json::value main = json::value::object({
		{ _YTEXT("id"), json::value::string(_YTEXT("DlgNewCosmosDbAccount")) },
		{ _YTEXT("method"), json::value::string(_YTEXT("POST")) },
		{ _YTEXT("action"), json::value::string(_YTEXT("#")) },
	});

	json::value items = json::value::array();
	items.as_array()[0] = json::value::object({
		{ _YTEXT("hbs"), json::value::string(_YTEXT("cosmosDbSelect")) },
		{ _YTEXT("introTextOne"), json::value::string(YtriaTranslate::Do(DlgDeleteCosmosDbAccount_generateJSONScriptData_1, _YLOC("This requires an active Azure Cosmos DB account. This account has to be set in your Microsoft Azure environment.")).c_str()) },
		{ _YTEXT("introTextTwo"), json::value::string(YtriaTranslate::Do(DlgDeleteCosmosDbAccount_generateJSONScriptData_2, _YLOC("If you haven't created one yet, please go to the ")).c_str()) },
		{ _YTEXT("linkRegisterText"), json::value::string(YtriaTranslate::Do(DlgDeleteCosmosDbAccount_generateJSONScriptData_3, _YLOC("Azure Cosmos DB portal")).c_str()) },
		{ _YTEXT("linkRegister"), json::value::string(_YTEXT("https://portal.azure.com/#blade/HubsExtension/Resources/resourceType/Microsoft.DocumentDb%2FdatabaseAccounts")) },
		{ _YTEXT("introTextThree"), json::value::string(YtriaTranslate::Do(DlgSpecialExtAdminAccess_generateJSONScriptData_4, _YLOC("Need help? See ")).c_str()) },
		{ _YTEXT("linkTextThree"), json::value::string(YtriaTranslate::Do(DlgSelectSubscriptionAndResourceGroup_generateJSONScriptData_5, _YLOC("How to create an Azure Cosmos DB Account")).c_str()) },
		{ _YTEXT("linkThree"), json::value::string(_YTEXT("https://www.ytria.com/help/sapio365/cosmosdb-setup")) },
		{ _YTEXT("subscriptionField"), json::value::string(g_SubscriptionIdKey) },
		{ _YTEXT("subscriptionLabel"), json::value::string(YtriaTranslate::Do(DlgSelectSubscriptionAndResourceGroup_generateJSONScriptData_6, _YLOC("Available Subscriptions")).c_str()) },
		{ _YTEXT("noSizeLimit"), json::value::string(_YTEXT("X")) },
		{ _YTEXT("allResGroupLabel"), json::value::string(YtriaTranslate::Do(DlgSelectSubscriptionAndResourceGroup_generateJSONScriptData_7, _YLOC("Available Resource Groups")).c_str()) },
		{ _YTEXT("deleteOldAccount"), json::value::string(YtriaTranslate::Do(DlgSelectSubscriptionAndResourceGroup_generateJSONScriptData_8, _YLOC("To delete an old CosmosDB account ")).c_str()) },
		{ _YTEXT("deleteOldAccountLink"), json::value::string(YtriaTranslate::Do(YTriaReportGridColumn_InplaceEditWndProcNumberFilter_8, _YLOC("click here")).c_str()) },
	});

	AddDynamicFields(items.as_array()[0].as_object());

	items.as_array()[1] = json::value::object({
		{ _YTEXT("hbs"), json::value::string(_YTEXT("Button")) },
		{
			_YTEXT("choices"), json::value::array({
				json::value::object({
					{ _YTEXT("type"), json::value::string(_YTEXT("submit")) },
					{ _YTEXT("label"), json::value::string(_YTEXT("OK")) },
					{ _YTEXT("attribute"), json::value::string(_YTEXT("post")) },
					{ _YTEXT("name"), json::value::string(_YTEXT("ok")) },
					{ _YTEXT("class"), json::value::string(_YTEXT("is-primary")) }
				}),
				json::value::object({
					{ _YTEXT("type"), json::value::string(_YTEXT("submit")) },
					{ _YTEXT("label"), json::value::string(_YTEXT("Cancel")) },
					{ _YTEXT("name"), json::value::string(_YTEXT("cancel")) },
					{ _YTEXT("attribute"), json::value::string(_YTEXT("cancel")) },
				})
			})
		}
	});

	auto root = json::value::object({
		{ _YTEXT("main"), main },
		{ _YTEXT("items"), items }
	});

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}

void DlgNewCosmosDbAccount::AddDynamicFields(web::json::object& p_Object)
{
	p_Object[_YTEXT("subscriptionChoice")] = web::json::value::array();
	auto& arrSubscriptions = p_Object[_YTEXT("subscriptionChoice")].as_array();

	p_Object[_YTEXT("allResGroupChoice")] = web::json::value::array();
	auto& arrResGroups = p_Object[_YTEXT("allResGroupChoice")].as_array();

	size_t iSubscriptions = 0;
	size_t iResGroups = 0;
	for (const auto& keyVal : m_Choices)
	{
		const auto& subscription = keyVal.first;
		const auto& resGroups = keyVal.second;

		arrSubscriptions[iSubscriptions++] = web::json::value::object({
			{ _YTEXT("value"), json::value::string(subscription.m_SubscriptionId ? *subscription.m_SubscriptionId : _YTEXT("")) },
			{ _YTEXT("label"), json::value::string(subscription.m_DisplayName ? *subscription.m_DisplayName : _YTEXT("")) },
			{ _YTEXT("default"), json::value::string(iSubscriptions == 0 ? _YTEXT("X") : _YTEXT("")) }
		}, true);

		arrResGroups[iResGroups] = web::json::value::object({
			{ _YTEXT("resgroupField"), json::value::string(subscription.m_SubscriptionId ? *subscription.m_SubscriptionId : _YTEXT("")) },
			{ _YTEXT("noSizeLimit"), json::value::string(_YTEXT("X")) },
			{ _YTEXT("resgroupChoice"), json::value::array() }
		}, true);

		auto& choices = arrResGroups[iResGroups].as_object()[_YTEXT("resgroupChoice")].as_array();
		bool first = true;
		for (const auto& resGroup : resGroups)
		{
			choices[iResGroups++] = json::value::object({
				{ _YTEXT("value"), json::value::string(resGroup.m_Name ? *resGroup.m_Name : _YTEXT("")) },
				{ _YTEXT("label"), json::value::string(resGroup.m_Name ? *resGroup.m_Name : _YTEXT("")) },
				{ _YTEXT("default"), json::value::string(first ? _YTEXT("X") : _YTEXT("")) }
			}, true);
			first = false;
		}
	}
}

const wstring DlgNewCosmosDbAccount::g_SubscriptionIdKey = _YTEXT("SUBSCRIPTIONID");

std::map<wstring, int, Str::keyLessInsensitive> DlgNewCosmosDbAccount::g_Sizes;
