#include "WholeUserDeserializer.h"

#include "JsonSerializeUtil.h"
#include "UserDeserializer.h"
#include "UserNonPropDeserializer.h"

WholeUserDeserializer::WholeUserDeserializer(const std::shared_ptr<Sapio365Session>& p_Session)
    : m_Session(p_Session)
{
}

void WholeUserDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
#ifdef _DEBUG
        UserDeserializer d(m_Session, true);
#else
		UserDeserializer d(m_Session);
#endif
		JsonSerializeUtil::DeserializeObject(d, p_Object);
        m_Data = std::move(d.GetData());
    }

    {
        UserNonPropDeserializer d;
		JsonSerializeUtil::DeserializeObject(d, p_Object);
        m_Data.MergeWith(d.GetData());
    }
}
