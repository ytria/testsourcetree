#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerSchools : public IBrowserLinkHandler
{
public:
	void Handle(YBrowserLink& p_Link) const override;
};

