#include "Application.h"

RTTR_REGISTRATION
{
	using namespace rttr;
registration::class_<Application>("Application") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Application"))))
.constructor()(policy::ctor::as_object);
}