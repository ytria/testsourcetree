#pragma once

#include "CachedObject.h"
#include "PooledString.h"
#include "RttrIncludes.h"

#include "Group.h"

class BusinessGroup;

class CachedGroup : public CachedObject
{
public:
	CachedGroup() = default;
	CachedGroup(const Group& group);
    CachedGroup(const BusinessGroup& group);

	const PooledString& GetDisplayName() const;
	const boost::YOpt<PooledString>& GetMail() const;
	const boost::YOpt<vector<PooledString>>& GetGroupTypes() const;
	const boost::YOpt<bool>& GetMailEnabled() const;
	const boost::YOpt<bool>& GetSecurityEnabled() const;
	bool GetIsTeam() const;
	const boost::YOpt<vector<PooledString>>	GetResourceProvisioningOptions() const;

private:
    PooledString m_DisplayName;
	boost::YOpt<PooledString> m_Mail;
	boost::YOpt<vector<PooledString>> m_GroupTypes;
	boost::YOpt<bool> m_MailEnabled;
	boost::YOpt<bool> m_SecurityEnabled;
	boost::YOpt<vector<PooledString>>	m_ResourceProvisioningOptions;
	// TODO: More fields
   
    RTTR_ENABLE(CachedObject)
	RTTR_REGISTRATION_FRIEND
};
