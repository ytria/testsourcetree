#pragma once

#include "CommandInfo.h"
#include "ModuleBase.h"

class FrameOnPremiseGroups;
struct RefreshSpecificData;

class ModuleOnPremiseGroups : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	virtual void executeImpl(const Command& p_Command);
	void showOnPremiseGroups(const Command& p_Command);
	void refresh(const Command& p_Command);
};

