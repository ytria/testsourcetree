#include "GenericSaveAllButtonUpdater.h"

#include "BaseO365Grid.h"
#include "GridFrameBase.h"

GenericSaveAllButtonUpdater::GenericSaveAllButtonUpdater(GridFrameBase& p_Frame)
	: m_Frame(p_Frame)
{
}

void GenericSaveAllButtonUpdater::Update(CCmdUI* p_CmdUi)
{
	p_CmdUi->Enable(m_Frame.IsConnected() && m_Frame.HasNoTaskRunning() && m_Frame.hasModificationsPending());
}
