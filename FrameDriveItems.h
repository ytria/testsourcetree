#pragma once

#include "GridDriveItems.h"

#include "BusinessDriveItemFolder.h"
#include "BusinessPermission.h"
#include "CommandInfo.h"
#include "GridFrameBase.h"

class FrameDriveItems : public GridFrameBase
{
public:
    DECLARE_DYNAMIC(FrameDriveItems)
	FrameDriveItems(Origin p_Origin, bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
    virtual ~FrameDriveItems() = default;

    void ShowDriveItem(const O365DataMap<BusinessUser, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses);
	void ShowDriveItem(const O365DataMap<BusinessGroup, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses);
	void ShowDriveItem(const O365DataMap<BusinessSite, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses);
	void ShowDriveItem(const O365DataMap<BusinessChannel, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses);

	virtual O365Grid& GetGrid() override;
    virtual void ApplyAllSpecific() override;
    virtual void ApplySelectedSpecific() override;

    void ApplySpecific(bool p_Selected);
    virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;
	void ViewPermissionsExplode(const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions);
	void ViewPermissions(const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, bool p_RefreshUpdates);
	void ViewPermissionsExplodeFlat(const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions);

	void ViewCheckoutStatuses(const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses);

    virtual bool CloseFrame() override;
	virtual bool CanBeClosedWithoutConfirmation() override;
    virtual bool HasApplyButton() override;
    bool downloadConfirmation();

	// For Origin::Channel only!!!
	const std::map<PooledString, PooledString>& GetChannelNames() const;
	void SetChannelNames(const std::map<PooledString, PooledString>& p_ChannelNames);

	// Especially used for Grid logger (Bug #200511.SR.00C04E)
	void SetDownloadProcessRunning(bool p_Running);

	void SetSearchCriteria(const boost::YOpt<wstring>& p_SearchCriteria);
	const boost::YOpt<wstring>& GetSearchCriteria() const;
	void RefreshWithNewSearchCriteria(const wstring& p_SearchCriteria);

protected:
    DECLARE_MESSAGE_MAP()

    afx_msg void OnUpdate365Refresh(CCmdUI* pCmdUI);
	afx_msg void OnUpdate365RefreshSelection(CCmdUI* pCmdUI);

	virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;
	virtual O365ControlConfig GetApplyPartialControlConfig() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

	virtual void revertSelectedImpl() override;

	// Especially used by Grid logger (Bug #200511.SR.00C04E)
	bool isDownloadProcessRunning() const;

private:
	GridDriveItems m_driveItemsGrid;
	std::map<PooledString, PooledString> m_ChannelNames;

	// Especially used by Grid logger (Bug #200511.SR.00C04E)
	bool m_DownloadRunning;

	boost::YOpt<wstring> m_SearchCriteria;

	class GridLogger;
};
