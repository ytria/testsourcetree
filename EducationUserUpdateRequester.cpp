#include "EducationUserUpdateRequester.h"
#include "LoggerService.h"
#include "Sapio365Session.h"
#include "O365AdminUtil.h"
#include "MsGraphHttpRequestLogger.h"

EducationUserUpdateRequester::EducationUserUpdateRequester(const wstring& p_UserId, const web::json::value& p_UserChanges)
	:m_UserId(p_UserId),
	m_UserChanges(p_UserChanges)
{
	ASSERT(p_UserChanges.is_object());

}

TaskWrapper<void> EducationUserUpdateRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("users"));
	uri.append_path(m_UserId);

	LoggerService::User(_YFORMAT(L"Updating education user with id %s", m_UserId.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Patch(uri.to_uri(), httpLogger, p_TaskData, WebPayloadJSON(m_UserChanges)).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResInfo) {
		*result = Util::GetResult(p_ResInfo);
		});
}

const HttpResultWithError& EducationUserUpdateRequester::GetResult() const
{
	return *m_Result;
}
