#include "SapioErrorSerializer.h"

SapioErrorSerializer::SapioErrorSerializer(const SapioError& p_SapioError)
	: m_SapioError(p_SapioError)
{

}

void SapioErrorSerializer::Serialize()
{
	m_Json = web::json::value::object();
	auto& obj = m_Json.as_object();

	JsonSerializeUtil::SerializeUint32(_YTEXT("statusCode"), m_SapioError.GetStatusCode(), obj);
	JsonSerializeUtil::SerializeString(_YTEXT("errorMessage"), PooledString(m_SapioError.GetFullErrorMessage()), obj);
}
