#include "UserRequester.h"

#include "BusinessGroup.h"
#include "MSGraphCommonData.h"
#include "MSGraphUtil.h"
#include "UserDeserializer.h"
#include "MsGraphFieldNames.h"
#include "MSGraphSession.h"
#include "MultiObjectsPageRequestLogger.h"
#include "IPropertySetBuilder.h"
#include "SQL + Cloud/Role Delegation/RoleDelegationManager.h"
#include "SingleRequestResult.h"
#include "RbacRequiredPropertySet.h"
#include "MsGraphHttpRequestLogger.h"

UserRequester::UserRequester(const PooledString& p_UserId, IPropertySetBuilder&& p_PropertySet, bool p_SwallowExceptions, const std::shared_ptr<IRequestLogger>& p_Logger)
    : m_UserId(p_UserId)
	, m_Properties(std::move(p_PropertySet.GetPropertySet()))
	, m_UseBetaEndpoint(false)
	, m_SwallowExceptions(p_SwallowExceptions)
	, m_Logger(p_Logger)
	, m_RbacSessionMode(Sapio365Session::USER_SESSION)
{
}

UserRequester::UserRequester(const PooledString& p_UserId, const std::vector<rttr::property>& p_Properties, const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_UserId(p_UserId)
	, m_Properties(p_Properties)
	, m_UseBetaEndpoint(false)
	, m_SwallowExceptions(false)
	, m_Logger(p_Logger)
	, m_RbacSessionMode(Sapio365Session::USER_SESSION)
{

}

void UserRequester::SetUseBetaEndpoint(bool p_UseBetaEndpoint)
{
	m_UseBetaEndpoint = p_UseBetaEndpoint;
}

void UserRequester::SetRbacSessionMode(Sapio365Session::SessionType p_RbacSessionMode)
{
	m_RbacSessionMode = p_RbacSessionMode;
}

TaskWrapper<void> UserRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer	= std::make_shared<UserDeserializer>(p_Session);
    m_Result		= std::make_shared<SingleRequestResult>();

	const auto& properties = m_Properties;

    web::uri_builder uri;
    uri.append_path(Rest::USERS_PATH);
    uri.append_path(m_UserId);
    uri.append_query(U("$select"), Rest::GetSelectQuery(properties));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(m_RbacSessionMode, p_Session->GetIdentifier());
	if (!m_SwallowExceptions)
	{
		return p_Session->GetMSGraphSession(m_RbacSessionMode)->getObject(m_Deserializer, m_Result, uri.to_uri(), m_UseBetaEndpoint, m_Logger, httpLogger, p_TaskData);
	}
	else
	{
		return p_Session->GetMSGraphSession(m_RbacSessionMode)->getObject(m_Deserializer, m_Result, uri.to_uri(), m_UseBetaEndpoint, m_Logger, httpLogger, p_TaskData).ThenByTask([result = m_Result](pplx::task<void> p_Task) {
			try
			{
				p_Task.wait();
			}
			catch (const RestException& e)
			{
				RestResultInfo info(e.GetRequestResult());
				info.m_RestException = std::make_shared<RestException>(e);
				result->SetResult(info);
			}
			catch (const std::exception& e)
			{
				RestResultInfo info;
				info.m_Exception = e;
				result->SetResult(info);
			}
			catch (...)
			{
				result->SetResult(RestResultInfo());
			}
		});
	}
}

BusinessUser& UserRequester::GetData()
{
    m_Deserializer->GetData().SetID(m_UserId);
    return m_Deserializer->GetData();
}

const SingleRequestResult& UserRequester::GetResult() const
{
    return *m_Result;
}
