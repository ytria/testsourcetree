#pragma once

#include "AutomationNames.h"
#include "YBrowserLink.h"

class ModuleBase;

class IBrowserLinkHandler
{
public:
	IBrowserLinkHandler(const IBrowserLinkHandler&) = default;
	IBrowserLinkHandler(IBrowserLinkHandler&&) = default;
    IBrowserLinkHandler& operator=(const IBrowserLinkHandler&) & = default;
    IBrowserLinkHandler& operator=(IBrowserLinkHandler&&) & = default;
    virtual ~IBrowserLinkHandler() = default;

    virtual void Handle(YBrowserLink& click) const = 0;
    virtual bool IsAllowedDuringAutomation()
    {
        return false; // Most of instances are for modules. Override for specific cases
    }

    virtual bool IsAllowedWithAJLLicense() const
    {
        return false; // only MyData is allowed
    }

protected:
    IBrowserLinkHandler() = default;
};
