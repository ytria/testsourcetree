#pragma once

#include "Encapsulate.h"
#include "IPSObjectCollectionDeserializer.h"
#include "MailboxPermissions.h"

#include <string>
#include <vector>

class MailboxPermissionsCollectionDeserializer : public IPSObjectCollectionDeserializer, public Encapsulate<std::vector<MailboxPermissions>>
{
public:
	void Deserialize(IPSObjectPropertyReader& p_Reader) override;
	void SetCount(size_t p_Count) override;
};

