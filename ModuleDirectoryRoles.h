#pragma once

#include "ModuleBase.h"

class ModuleDirectoryRoles : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

public:
    virtual void executeImpl(const Command& p_Command) override;

private:
    void showDirectoryRoles(const Command& p_Command);
	void updateDirectoryRoles(const Command& p_Command);
    void loadSnapshot(const Command& p_Command);
};

