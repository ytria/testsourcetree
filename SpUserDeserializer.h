#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "SpUser.h"

namespace Sp
{
class UserDeserializer : public JsonObjectDeserializer, public Encapsulate<Sp::User>
{
protected:
    void DeserializeObject(const web::json::object& p_Object) override;
};
}

