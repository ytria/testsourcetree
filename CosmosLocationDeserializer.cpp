#include "CosmosLocationDeserializer.h"
#include "JsonSerializeUtil.h"

void Azure::CosmosLocationDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	JsonSerializeUtil::DeserializeString(_YTEXT("locationName"), m_Data.m_LocationName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("documentEndpoint"), m_Data.m_DocumentEndpoint, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("provisioningState"), m_Data.m_ProvisioningState, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YTEXT("failoverPriority"), m_Data.m_FailoverPriority, p_Object);
}
