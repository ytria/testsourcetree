#include "SubItemFieldValue.h"

#include "GridBackendField.h"

SubItemFieldValue::SubItemFieldValue()
{
    m_HasValue = false;
    m_BoolValue = false;
    m_FieldType = GridBackendUtil::UNKNOWN;
}

SubItemFieldValue::SubItemFieldValue(const GridBackendField& p_Field)
    : SubItemFieldValue(p_Field, 0)
{
    ASSERT(!p_Field.IsMulti()); // If multivalue, use constructor with multivalue index
    if (!p_Field.IsMulti())
    {
        m_HasValue	= p_Field.HasValue();
        m_FieldType = p_Field.GetDataType();
        if (m_HasValue)
        {
            switch (m_FieldType)
            {
            case GridBackendUtil::STRING:
                m_StrValue = p_Field.GetValueStr();
                break;
            case GridBackendUtil::DATE:
                m_TimeDateValue = p_Field.GetValueTimeDate();
                break;
            case GridBackendUtil::BOOL:
                m_BoolValue = p_Field.GetValueBool();
                break;
            default:
                ASSERT(false);
                break;
            }
        }
    }
}

SubItemFieldValue::SubItemFieldValue(const GridBackendField& p_Field, size_t p_MultiValueIndex)
    : SubItemFieldValue()
{
    m_HasValue	= p_Field.HasValue();
    m_FieldType = p_Field.GetDataType();

	if (p_Field.IsMulti())
	{
		ASSERT(m_HasValue && p_MultiValueIndex < p_Field.GetMultiValueSize());
		if (m_HasValue && p_MultiValueIndex < p_Field.GetMultiValueSize())
		{
			switch (m_FieldType)
			{
			case GridBackendUtil::STRING_LIST:
				m_StrValue = p_Field.GetValuesMulti<PooledString>()->operator[](p_MultiValueIndex);
				break;
			case GridBackendUtil::DATE_LIST:
				m_TimeDateValue = p_Field.GetValuesMulti<YTimeDate>()->operator[](p_MultiValueIndex);
				break;
			case GridBackendUtil::BOOL_LIST:
				m_BoolValue = p_Field.GetValuesMulti<bool>()->operator[](p_MultiValueIndex);
				break;
			default:
				ASSERT(false);
				break;
			}
		}
	}
	else
	{
		ASSERT(0 == p_MultiValueIndex);
		if (m_HasValue)
		{
			switch (m_FieldType)
			{
			case GridBackendUtil::STRING:
				m_StrValue = p_Field.GetValueStr();
				break;
			case GridBackendUtil::DATE:
				m_TimeDateValue = p_Field.GetValueTimeDate();
				break;
			case GridBackendUtil::BOOL:
				m_BoolValue = p_Field.GetValueBool();
				break;
			default:
				ASSERT(false);
				break;
			}
		}
	}
}

const PooledString& SubItemFieldValue::GetValueStr() const
{
    return m_StrValue;
}

const YTimeDate& SubItemFieldValue::GetValueTimeDate() const
{
    return m_TimeDateValue;
}

bool SubItemFieldValue::GetValueBool() const
{
    return m_BoolValue;
}

bool SubItemFieldValue::HasValue() const
{
    return m_HasValue;
}

GridBackendUtil::DATATYPE SubItemFieldValue::GetFieldDataType() const
{
    return m_FieldType;
}

bool SubItemFieldValue::operator==(const SubItemFieldValue& p_Rhs) const
{
    if (m_HasValue == p_Rhs.m_HasValue)
        return m_StrValue == p_Rhs.m_StrValue &&
               m_TimeDateValue == p_Rhs.m_TimeDateValue &&
               m_BoolValue == p_Rhs.m_BoolValue;

    return false;
}

bool SubItemFieldValue::operator!=(const SubItemFieldValue& p_Rhs) const
{
    return !(*this == p_Rhs);
}

wstring SubItemFieldValue::ToString() const
{
	wstring strValue = GridBackendUtil::g_NoValueString;

	if (m_HasValue)
	{
		switch (m_FieldType)
		{
			case GridBackendUtil::STRING:
			case GridBackendUtil::STRING_LIST:
				strValue = GetValueStr();
				break;
			case GridBackendUtil::DATE:
			case GridBackendUtil::DATE_LIST:
				strValue = TimeUtil::GetInstance().GetISO8601String(GetValueTimeDate());
				break;
			case GridBackendUtil::BOOL:
			case GridBackendUtil::BOOL_LIST:
				strValue = GetValueBool() ? _YTEXT("True") : _YTEXT("False");
				break;
			default:
				ASSERT(false);
				break;
		}
	}

	return strValue;
}