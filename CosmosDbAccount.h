#pragma once

#include "CosmosLocation.h"

namespace Azure
{
	class CosmosDbAccount
	{
	public:
		boost::YOpt<PooledString> m_Id;
		boost::YOpt<PooledString> m_Name;
		boost::YOpt<vector<Azure::CosmosLocation>> m_WriteLocations;
	};
}
