#include "MailboxPermissionsCollectionDeserializer.h"

void MailboxPermissionsCollectionDeserializer::Deserialize(IPSObjectPropertyReader& p_Reader)
{
	MailboxPermissions object;

	if (p_Reader.HasProperty(L"AccessRights"))
		object.m_AccessRights = p_Reader.GetStringProperty(L"AccessRights");
	if (p_Reader.HasProperty(L"Deny"))
		object.m_Deny = p_Reader.GetStringProperty(L"Deny");
	if (p_Reader.HasProperty(L"Identity"))
		object.m_Identity = p_Reader.GetStringProperty(L"Identity");
	if (p_Reader.HasProperty(L"InheritanceType"))
		object.m_InheritanceType = p_Reader.GetStringProperty(L"InheritanceType");
	if (p_Reader.HasProperty(L"IsInherited"))
		object.m_IsInherited = p_Reader.GetBoolProperty(L"IsInherited");
	if (p_Reader.HasProperty(L"IsValid"))
		object.m_IsValid = p_Reader.GetBoolProperty(L"IsValid");
	if (p_Reader.HasProperty(L"RealAce"))
		object.m_RealAce = p_Reader.GetStringProperty(L"RealAce");
	if (p_Reader.HasProperty(L"User"))
		object.m_User = p_Reader.GetStringProperty(L"User");

	m_Data.push_back(object);
}

void MailboxPermissionsCollectionDeserializer::SetCount(size_t p_Count)
{
	m_Data.reserve(p_Count);
}
