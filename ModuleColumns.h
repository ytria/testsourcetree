#pragma once

#include "ModuleBase.h"

#include "CommandInfo.h"

class AutomationAction;
class FrameColumns;

class ModuleColumns : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

private:
    virtual void executeImpl(const Command& p_Command) override;
	void showColumns(const Command& p_Command);
    void doRefresh(FrameColumns* frameColumns, const ModuleCriteria& p_ModuleCriteria, const Command& p_Command, bool p_IsUpdate);
};

