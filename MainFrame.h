#pragma once

#include "AutomatedMainFrame.h"
#include "AutomationWizardO365.h"
#include "CodeJockThemed.h"
#include "FrameBase.h"
#include "LicenseManager.h"
#include "SessionIdentifier.h"

#include <memory>

class DlgBrowser;
class GridFrameBase;
class O365HtmlMainView;
class BackstagePanelSessions;
class O365LicenseValidator;
class DlgSpecialExtAdminAccess;
class DlgInputRequest;

using MainFrameBase = CodeJockThemed<FrameBase<AutomatedMainFrame>>;

class MainFrame : public MainFrameBase
				, public LicenseManagerObserver
{
public:
	DECLARE_DYNCREATE(MainFrame)
	MainFrame();
	virtual ~MainFrame();

	PersistentSession PopulateRecentSessions();

	/** AutomatedMainFrame **/
	virtual const bool HasModifiedItems();

	bool IsDebugMode();

	bool IsSyncInProgresss() const;

	bool WaitSyncCacheEvent() const;
	static std::pair<bool, PersistentSession> IsOverwritingExistingElevated(const wstring& p_SessionBeingLoadedEmail, const wstring& p_SessionBeingLoadedType);

	static void DeleteNewlyCreatedApp(DlgSpecialExtAdminAccess& dlg, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent);
	static void GiveConsentForApp(const wstring& p_Tenant, const wstring& p_AppID, CWnd* p_Parent);

	void SetBackstagePanelSessions(BackstagePanelSessions* p_BackstagePanelSessions);
	void NotifyBackstagePanelSessions();

	ILicenseValidator::ErrorStatus	GetLicenseValidatorError(wstring& p_ErrMsg) const;

	bool ConfigureLicense(const VendorLicense& p_License);
	bool UpgradeLicense(const VendorLicense& p_License);
	LicenseManager::ProcessLicenseContextResult ProcessLicenseContext(const LicenseContext& p_LC);
	bool IgnoreLicenseError() const;	

	void SendTrace(const vector<wstring>& p_AdditionnalFiles, CWnd* p_ParentForDialogs);
	static void SaveAsElevatedSession(PersistentSession& p_Session, const wstring& p_AppId, const wstring& p_ClientSecret);
	void ToggleDumpMode(CWnd* p_ParentForDialogs, bool p_SaveDisableToRegistry);

    void UpdateCanOpenWithRole(const wstring& p_ConnectedUserId, const wstring& p_SessionType, int64_t p_RoleId);

	void UpdateSessionNSATracking();

	AutomationWizardO365* GetAutomationWizard() const;

	static void SetupElevatedSessionFromAdvancedSession(std::shared_ptr<Sapio365Session> p_Session, PersistentSession p_PersistentSession, const wstring& p_AppId, const wstring& p_AppClientSecret, const std::shared_ptr<MSGraphSession>& p_AppSession, CFrameWnd* p_FrameSource);

	void DownloadAutomationFileCallback(const wstring& p_Message);

	static void EditAndLoadUltraAdmin(GridFrameBase* p_ParentFrame);

	void LicenseChanged() override;

	void OpenFile(const wstring& p_FilePath); // Used when coming from command line. Only snapshot files for now.

	wstring GetApplicationColor() const;

	bool IsLicensePro() const;

	void HTMLRefreshAll();
	void HTMLRemoveOneJob(const Script& p_Job);
	void HTMLRemoveOneSchedule(const ScriptSchedule& p_JobSchedule);
	void HTMLUpdateJobs();
	void HTMLUpdateSchedules();
	void HTMLUpdateAJL();
	void HTMLUpdateAJLcanRemoveJobs();

	void TokenBuy();

	static const wstring g_homePageResourceName;

protected:
	afx_msg LRESULT OnClose(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTrayIconNotify(WPARAM wParam, LPARAM lParam);

    afx_msg void OnNewStandardSession();
    afx_msg void OnNewDelegatedAdminSession();
	afx_msg void OnConnectToCustomer();
    afx_msg void OnNewUltraAdminSession();
    afx_msg void OnViewSessions();
	afx_msg void OnEditOnPremSettings();
	afx_msg void OnViewSchools();
	afx_msg void OnViewEduUsers();
	afx_msg void OnViewApplications();
	afx_msg void OnViewWebConsole();
    afx_msg void OnLoadSession();
	afx_msg void OnSelectSession();
	afx_msg void OnApplyRole();
	afx_msg void OnEditAndLoadUltraAdmin();
	afx_msg void OnLoadPhotoSnapshot();
	afx_msg void OnLoadRestoreSnapshot();

    afx_msg void OnUpdateNewStandardSession(CCmdUI* pCmdUI);
    afx_msg void OnUpdateNewDelegatedAdminSession(CCmdUI* pCmdUI);
	afx_msg void OnUpdateConnectToCustomer(CCmdUI* pCmdUI);
    afx_msg void OnUpdateNewUltraAdminSession(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditOnPremSettings(CCmdUI* pCmdUI);
    afx_msg void OnUpdateViewSessions(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewSchools(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewEduUsers(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditPowerShellADDSCredentials(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewApplications(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewWebConsole(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSelectSession(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLoadSession(CCmdUI* pCmdUI);
	afx_msg void OnUpdateApplyRole(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditUltraAdmin(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLoadPhotoSnapshot(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLoadRestoreSnapshot(CCmdUI* pCmdUI);

    afx_msg LRESULT OnOAuth2BrowserClose(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT OnUserChange(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAuthenticationCanceled(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAuthenticationFailure(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReadyToTestAuthentication(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnProductUpdateAvailable(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnProductUpdateQuitAndInstall(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetOAuth2Browser(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetOAuth2Browser(WPARAM wParam, LPARAM lParam);

	afx_msg void OnLoadUserHtml_debug();
	afx_msg void OnReloadCurrentHtml_debug();
	afx_msg void OnReinitHomePage_debug();
	afx_msg void OnLoadModulesHtml_debug();
	afx_msg void OnLoadHtmlInDlg_debug();
	afx_msg void OnSendRulesToClipboard_debug();
	afx_msg void OnSendSystemVarToClipboard_debug();
	afx_msg void OnShowRequestTester_debug();
	afx_msg void OnUpdateLoadUserHtml_debug(CCmdUI* pCmdUI);
	afx_msg void OnUpdateReloadCurrentHtml_debug(CCmdUI* pCmdUI);
	afx_msg void OnUpdateReinitHomePage_debug(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLoadModulesHtml_debug(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLoadHtmlInDlg_debug(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSendRulesToClipboard_debug(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSendSystemVarToClipboard_debug(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowRequestTester_debug(CCmdUI* pCmdUI);

	afx_msg void OnUpdateLogout(CCmdUI* pCmdUI);

	afx_msg void OnDebugMode();
	afx_msg void OnUpdateDebugMode(CCmdUI* pCmdUI);

    afx_msg void OnNewStandardSessionHelpWhatCanIDo();
    afx_msg void OnUpdateNewStandardSessionHelpWhatCanIDo(CCmdUI* pCmdUI);

    afx_msg void OnNewStandardSessionReprovideConsent();
    afx_msg void OnUpdateNewStandardSessionReprovideConsent(CCmdUI* pCmdUI);

    afx_msg void OnNewAdvancedUserSessionHelpWhatCanIDo();
    afx_msg void OnUpdateNewAdvancedUserSessionHelpWhatCanIDo(CCmdUI* pCmdUI);

    afx_msg void OnNewAdvancedUserSessionGiveAdminConsent();

    afx_msg void OnUpdateNewAdvancedUserSessionGiveAdminConsent(CCmdUI* pCmdUI);

    //afx_msg void OnNewAdvancedUserSessionSendEmailToGetConsent();
    //afx_msg void OnUpdateNewAdvancedUserSessionSendEmailToGetConsent(CCmdUI* pCmdUI);

    afx_msg void OnNewUltraAdminSessionHelpWhatCanIDo();
    afx_msg void OnUpdateNewUltraAdminSessionHelpWhatCanIDo(CCmdUI* pCmdUI);

    afx_msg void OnNewUltraAdminSessionHowToCreateAnApp();
    afx_msg void OnUpdateNewUltraAdminSessionHowToCreateAnApp(CCmdUI* pCmdUI);

    afx_msg void OnNewUltraAdminSessionGiveAdminConsent();
    afx_msg void OnUpdateNewUltraAdminSessionGiveAdminConsent(CCmdUI* pCmdUI);

	afx_msg void OnUltraAdminRequestAccessProtectedApi();
	afx_msg void OnUpdateUltraAdminRequestAccessProtectedApi(CCmdUI* pCmdUI);

	afx_msg void OnTokenBuy();

    //afx_msg void OnNewUltraAdminSessionSendEmailToGetConsent();
    //afx_msg void OnUpdateNewUltraAdminSessionSendEmailToGetConsent(CCmdUI* pCmdUI);

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);

	afx_msg void OnMessageBarButton();
	afx_msg void OnAutomationRecorder();

	afx_msg void OnViewJobCenterGridDebug();

	afx_msg void OnResizeSessionGalleryS();
	afx_msg void OnResizeSessionGalleryM();
	afx_msg void OnResizeSessionGalleryL();

	DECLARE_MESSAGE_MAP()

	virtual BOOL OnFrameLoaded();

	void syncSessionCache();

	/** CodeJockFrameBase **/
	virtual wstring getInitFrameTitle() const;

	/**  WindowShowingState **/
	virtual AutomatedApp* GetAutomatedApp() const;

	/** AutomatedMainFrame **/
	virtual CacheGrid*						GetGrid(const wstring& i_GridName);
	virtual AutomatedApp::AUTOMATIONSTATUS	automationProcessOption(AutomationAction* i_Action);
	virtual void							automationSave(AutomationAction* i_Action);
	virtual void							automationSaveSelected(AutomationAction* i_Action);
	virtual bool							IsThreadRunning();
	virtual CacheGrid*						GetMainGrid();
	virtual void	InitApp();
	virtual void	LoadXMLSelectionSpecific(const wstring& i_XmlFilePath);
	virtual void	GetItemsToAddToFavoriteList(set<wstring>& items);
	virtual afx_msg void	OnUpdateFavoriteAddToList(CCmdUI* pCmdUI);
	virtual void			ProcessFavoriteOrRecent(const wstring& favoriteOrRecent);
	virtual void anotherInstanceTriedToStart() override;

	/** FrameBase **/
	virtual CXTPRibbonGroup* getSessionGroup();
	virtual bool sessionWasSet(bool p_SessionHasChanged) override;
	virtual void logout() override;

	void showStatusBarProgress(bool p_UseMarquee, const wstring& p_Text);
	void hideStatusBarProgress();

	void postSyncCache(bool p_CreateElevatedIfNeeded = true);

	void CreateElevatedSession(std::shared_ptr<Sapio365Session> p_SessionBeingCreated);

	void CreateMissingRoles();

	enum class DisplayMode
	{
		USER,
		ADMIN
	};
	void setDisplayMode(DisplayMode p_DisplayMode);

	void updateConnectToCustomerButton();

private:
	virtual CXTPRibbonBar* createRibbonBar() override final;
	
	void resizeSessionGallery(int p_GallerySizeEnumVal, bool recalcLayout);

    void fillSessionGroup(CXTPRibbonGroup * groupConnect);

	const wstring getFrameTitle() const;
	void refreshFrameTitle();
	void enableUITheme();

	virtual wstring getPositionAndSizeRegistrySectionName() const;

	void showWindowsNotification(const wstring& p_Title, const wstring& p_Message, UINT notificationID);
	void showBackstageMenuUpdateEntry(CXTPRibbonBar& p_RibbonBar, bool p_Show);

	void trackSessionUsageStop();

	// For DlgBrowserOpener
	DlgBrowser* getOauth2Browser() const;
	void setOauth2Browser(DlgBrowser* val);

	bool updateLicenseContext();
	void asyncCountUsers(const shared_ptr<O365LicenseValidator> p_Validator, std::function<void(int32_t)> p_Callback);
	LicenseManager* getLicenseManager() const;
	const std::shared_ptr<O365LicenseValidator>& getLicenseValidator() const;

	void initJobCenter();

	bool createProductUpdateMessageBar(const wstring& title, const wstring& message);

	void sessionWasSetHMTLupdate();

private:
	std::unique_ptr<AutomationWizardO365> m_AutomationWizard;
	
	DlgBrowser* m_oauth2Browser; // For DlgBrowserOpener
    O365HtmlMainView* m_HtmlView;
	CProgressCtrl m_wndProgCtrl;

	CXTPRibbonTab* m_connectTab;
	CXTPRibbonTab* m_feedbackTab;

	wstring m_UserHTML;
	static const wstring g_debug_homePageResourceName;
	static const wstring g_debug_debugGroup;
	static wstring g_AvailableSessionGroupName;

	CEvent m_SyncCacheEvent;
	bool m_SyncInProgress;
	bool m_DebugMode;
    bool m_CanOpenWithRole;
	bool m_HasContracts;
	
	set<SessionIdentifier> m_SessionIdentifiers;

	CXTPTrayIcon m_TrayIcon;
	UINT m_CurrentWindowsNotificationID;
	static const UINT g_UpdateNotificationCode;
	bool m_UpdateOnQuit;

	class DoModalHook;
	std::shared_ptr<DoModalHook> m_DoModalHook;

	class MouseInputHook;
	std::unique_ptr<MouseInputHook> m_MouseInputHook;

	wstring m_LoginSessionTrackName;

	BackstagePanelSessions* m_BackstagePanelSessions;

	mutable LicenseManager* m_LicenseManager;
	mutable std::shared_ptr<O365LicenseValidator> m_Validator;

	CXTPControl* m_RoleInfoControl;
	CXTPControl* m_RoleSelectControl;
	CXTPControl* m_ConnectToCustomerControl;

	bool m_AlreadyShownMessageBar;
	static bool askToCombineWithUltraAdmin(std::shared_ptr<Sapio365Session> p_Session, PersistentSession& p_PersistentSession, bool p_JustCreatedFullSession, CFrameWnd* p_FrameSource);

	static bool elevate(std::shared_ptr<Sapio365Session> p_Session, PersistentSession& p_PersistentSession, bool p_JustCreatedFullSession, CFrameWnd* p_FrameSource);
	static bool createUltraAdminForElevatedSession(std::shared_ptr<Sapio365Session> p_Session, PersistentSession& p_PersistentSession, bool p_ShowCreateUAdminDlg, CFrameWnd* p_FrameSource);

public:
	static wstring g_DumpModeText;
	static wstring g_DumpModeTooltip;

private:
	std::unique_ptr<DlgInputRequest> m_DlgInputRequest;
};