#include "DlgSnapshotOptions.h"

#include "BaseO365Grid.h"
#include "YCodeJockMessageBox.h"

namespace
{
	const wstring g_Pwd = _YTEXT("pwd");
	const wstring g_Pwd2 = _YTEXT("pwd2");
	const wstring g_Access = _YTEXT("access");
	const wstring g_HiddenCol = _YTEXT("hiddenCol");
	const wstring g_HiddenRow = _YTEXT("hiddenRow");
	const wstring g_PermanentComments = _YTEXT("permCom");
}

DlgSnapshotOptions::DlgSnapshotOptions(GridSnapshot::Options::Mode p_Mode, bool p_SessionIsRBAC, O365Grid* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
	, m_HasHiddenColumns(p_Parent->HasHiddenColumns())
	, m_HasHiddenRows(std::any_of(p_Parent->GetBackendRows().begin(), p_Parent->GetBackendRows().end(), [](auto& row) { return !row->IsInGrid(); }))
	, m_HasVisiblePermanentComments(std::any_of(p_Parent->GetBackendColumns().begin(), p_Parent->GetBackendColumns().end(), [](auto& col) { return col->IsAnnotation() && col->IsVisible(); }))
	, m_HasComparator(p_Parent->HasComparator())
	, m_HasPivot(nullptr != p_Parent->GetCurrentDlgPivot())
	, m_HasChart(false) // FIXME: Find a way to know some chart has been created.
	, m_HasRBACUnauthorizedRows(p_SessionIsRBAC && std::any_of(p_Parent->GetBackendRows().begin(), p_Parent->GetBackendRows().end(), [p_Parent](auto& row) { return !p_Parent->IsRbacAuthorized(row); }))
{
	m_Options.m_Mode = p_Mode;
	if (!m_HasHiddenColumns)
		m_Options.m_IncludeHiddenColumns = true;
	if (!m_HasHiddenRows)
		m_Options.m_IncludeHiddenRows = true;
	if (!m_HasVisiblePermanentComments)
		m_Options.m_IncludePermanentCommentColumns = true;
	if (GridSnapshot::Options::Mode::RestorePoint == m_Options.m_Mode)
		m_Options.m_Access = GridSnapshot::Options::Access::Tenant;
}

DlgSnapshotOptions::~DlgSnapshotOptions()
{
}

wstring DlgSnapshotOptions::getDialogTitle() const
{
	if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
		return _T("Create Snapshot - Options");

	ASSERT(GridSnapshot::Options::Mode::RestorePoint == m_Options.m_Mode);
	return _T("Create Restore Point - Options");
}

wstring DlgSnapshotOptions::externalValidationCallback(const wstring p_PropertyName, const wstring p_CurrentValue)
{
	ASSERT(p_PropertyName == g_Pwd);

	auto asciiPwd = Str::convertToASCII(p_CurrentValue);
	return p_CurrentValue == Str::convertFromUTF8(asciiPwd) ? _YTEXT("") : _YTEXT("NotAscii");
}

void DlgSnapshotOptions::generateJSONScriptData()
{
	initMain(_YTEXT("DlgSnapshotOptions"));

	ASSERT(isEditDialog());

	if (m_HasRBACUnauthorizedRows)
	{
		if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
			getGenerator().addIconTextField(_YTEXT("warning1"), _T("Items that fall outside the scope of the current role will not be saved with your snapshot."), _YTEXT("fas fa-exclamation-triangle"), _YTEXT("#ffcccc"), _YTEXT("#ff0000"));
		else
			getGenerator().addIconTextField(_YTEXT("warning1"), _T("Items that fall outside the scope of the current role will not be saved with your restore point."), _YTEXT("fas fa-exclamation-triangle"), _YTEXT("#ffcccc"), _YTEXT("#ff0000"));
	}

	if (m_HasComparator || m_HasPivot || m_HasChart)
	{
		wstring warningText;
		const int flags = (m_HasComparator ? 0x1 : 0) | (m_HasPivot ? 0x2 : 0) | (m_HasChart ? 0x4 : 0);
		ASSERT(0 < flags && flags <= 7);
		switch (flags)
		{
		case 1:
			if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
				warningText = _T("Comparator will not be saved with your snapshot.");
			else
				warningText = _T("Comparator will not be saved with your restore point.");
			break;
		case 2:
			if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
				warningText = _T("Pivot will not be saved with your snapshot.");
			else
				warningText = _T("Pivot will not be saved with your restore point.");
			break;
		case 3:
			if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
				warningText = _T("Comparator and Pivot will not be saved with your snapshot.");
			else
				warningText = _T("Comparator and Pivot will not be saved with your restore point.");
			break;
		case 4:
			if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
				warningText = _T("Chart will not be saved with your snapshot.");
			else
				warningText = _T("Chart will not be saved with your restore point.");
			break;
		case 5:
			if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
				warningText = _T("Comparator and Chart will not be saved with your snapshot.");
			else
				warningText = _T("Comparator and Chart will not be saved with your restore point.");
			break;
		case 6:
			if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
				warningText = _T("Pivot and Chart will not be saved with your snapshot.");
			else
				warningText = _T("Pivot and Chart will not be saved with your restore point.");
			break;
		case 7:
			if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
				warningText = _T("Comparator, Pivot and Chart will not be saved with your snapshot.");
			else
				warningText = _T("Comparator, Pivot and Chart will not be saved with your restore point.");
			break;
		}
		getGenerator().addIconTextField(_YTEXT("warning2"), warningText, _YTEXT("fas fa-exclamation-triangle"), _YTEXT("#ffcccc"), _YTEXT("#ff0000"));
	}

	//addCategory(_T("Password"), EXPAND_BY_DEFAULT);
	if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
		getGenerator().addIconTextField(_YTEXT("pwdTxt"), _T("If your snapshot file contains confidential information, you can protect it by requiring a password."), _YTEXT("fas fa-info-circle"));
	else
		getGenerator().addIconTextField(_YTEXT("pwdTxt"), _T("If your restore point file contains confidential information, you can protect it by requiring a password."), _YTEXT("fas fa-info-circle"));

	getGenerator().Add(WithExternalValidation<WithTooltip<PasswordEditor>>({ g_Pwd, _T("Password"), EditorFlags::DIRECT_EDIT/* | EditorFlags::REQUIRED*/, _YTEXT("") }, _YTEXT("Password must contain only ASCII characters.")));
	getGenerator().Add(PasswordEditor(g_Pwd2, _T("Confirm Password"), EditorFlags::DIRECT_EDIT/* | EditorFlags::REQUIRED*/, _YTEXT("")));
	getGenerator().addEvent(g_Pwd, { g_Pwd2, EventTarget::g_HideIfEmpty, _YTEXT("") });

	getGenerator().addCategory(_T("Options"), CategoryFlags::EXPAND_BY_DEFAULT);

	if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
	{
		if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
			getGenerator().addIconTextField(_YTEXT("accessTxt"), _T("If set to 'restricted', a login to the tenant will be required to access the snapshot."), _YTEXT("fas fa-info-circle"));
		else
			getGenerator().addIconTextField(_YTEXT("accessTxt"), _T("If set to 'restricted', a login to the tenant will be required to access the restore point."), _YTEXT("fas fa-info-circle"));
		getGenerator().Add(BoolToggleEditor({ { g_Access, _YTEXT("Access"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, false }, { _T("Public"), _T("Restricted to tenant") } }));
	}
	
	if (m_HasVisiblePermanentComments)
	{
		if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
			getGenerator().addIconTextField(_YTEXT("commentsTxt"), _T("If set to 'false', permanent comments will not be included in the snapshot."), _YTEXT("fas fa-info-circle"));
		else
			getGenerator().addIconTextField(_YTEXT("commentsTxt"), _T("If set to 'false', permanent comments will not be included in the restore point."), _YTEXT("fas fa-info-circle"));
		getGenerator().Add(BoolToggleEditor({ { g_PermanentComments, _YTEXT("Include visible permanent comments "), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, false } }));
	}

	if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
	{
		if (m_HasHiddenColumns)
		{
			if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
				getGenerator().addIconTextField(_YTEXT("columnTxt"), _T("If set to 'false', data from hidden columns will not be included in the snapshot. (Note: some columns are mandatory and will always be included.)"), _YTEXT("fas fa-info-circle"));
			else
				getGenerator().addIconTextField(_YTEXT("columnTxt"), _T("If set to 'false', data from hidden columns will not be included in the restore point. (Note: some columns are mandatory and will always be included.)"), _YTEXT("fas fa-info-circle"));
			getGenerator().Add(BoolToggleEditor({ { g_HiddenCol, _YTEXT("Include data from hidden columns"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, false } }));
		}

		if (m_HasHiddenRows)
		{
			if (GridSnapshot::Options::Mode::Photo == m_Options.m_Mode)
				getGenerator().addIconTextField(_YTEXT("columnTxt"), _T("If set to 'false', hidden rows will not be included in the snapshot."), _YTEXT("fas fa-info-circle"));
			else
				getGenerator().addIconTextField(_YTEXT("columnTxt"), _T("If set to 'false', hidden rows will not be included in the restore point."), _YTEXT("fas fa-info-circle"));
			getGenerator().Add(BoolToggleEditor({ { g_HiddenRow, _YTEXT("Include hidden rows"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, false } }));
		}
	}

	getGenerator().addApplyButton(_T("CREATE"));
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
}

bool DlgSnapshotOptions::processPostedData(const IPostedDataTarget::PostedData& data)
{
	std::string confirmPwd;
	for (const auto& d : data)
	{
		if (g_Pwd == d.first)
		{
			m_Password = Str::convertToUTF8(d.second);
		}
		else if (g_Pwd2 == d.first)
		{
			confirmPwd = Str::convertToUTF8(d.second);
		}
		else if (g_Access == d.first)
		{
			m_Options.m_Access = Str::getBoolFromString(d.second) ? GridSnapshot::Options::Access::Public : GridSnapshot::Options::Access::Tenant;
		}
		else if (g_HiddenCol == d.first)
		{
			ASSERT(m_HasHiddenColumns);
			m_Options.m_IncludeHiddenColumns = Str::getBoolFromString(d.second);
		}
		else if (g_HiddenRow == d.first)
		{
			ASSERT(m_HasHiddenRows);
			m_Options.m_IncludeHiddenRows = Str::getBoolFromString(d.second);
		}
		else if (g_PermanentComments == d.first)
		{
			ASSERT(m_HasVisiblePermanentComments);
			m_Options.m_IncludePermanentCommentColumns = Str::getBoolFromString(d.second);
		}
	}

	if (!m_Password.empty() && confirmPwd != m_Password)
	{
		YCodeJockMessageBox(this, DlgMessageBox::eIcon_Error, _T("Passwords do not match."), _T("Please check and try again."), _YTEXT(""), { {IDOK, _T("OK")} }).DoModal();
		return false;
	}

	return true;
}

const std::string& DlgSnapshotOptions::GetPassword()
{
	return m_Password;
}

const GridSnapshot::Options& DlgSnapshotOptions::GetOptions() const
{
	return m_Options;
}
