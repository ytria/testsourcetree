#pragma once

#include "IDeserializerFactory.h"
#include "Sapio365Session.h"
#include "SubscribedSkuDeserializer.h"

class SubscribedSkuDeserializerFactory : public IDeserializerFactory<SubscribedSkuDeserializer>
{
public:
	SubscribedSkuDeserializerFactory(std::shared_ptr<const Sapio365Session> p_Session);
	SubscribedSkuDeserializer Create() override;

private:
	std::shared_ptr<const Sapio365Session> m_Session;
};
