#pragma once

#include "BusinessObject.h"

#include "BusinessConversationThread.h"
#include "Conversation.h"

class BusinessConversation : public BusinessObject
{
public:
    BusinessConversation() = default;
    BusinessConversation(const Conversation& p_Conversation);

    const boost::YOpt<bool>& GetHasAttachments() const;
    void SetHasAttachments(const boost::YOpt<bool>& p_Val);
    
    const boost::YOpt<YTimeDate>& GetLastDeliveredDateTime() const;
    void SetLastDeliveredDateTime(const boost::YOpt<YTimeDate>& p_Val);
    
    const boost::YOpt<PooledString>& GetPreview() const;
    void SetPreview(const boost::YOpt<PooledString>& p_Val);
    
    const boost::YOpt<PooledString>& GetTopic() const;
    void SetTopic(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<vector<PooledString>>& GetUniqueSenders() const;
    void SetUniqueSenders(const boost::YOpt<vector<PooledString>>& p_Val);

    const vector<BusinessConversationThread>& GetConversationThreads() const;
    void SetConversationThreads(const vector<BusinessConversationThread>& p_Val);

	const boost::YOpt<PooledString>& GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& p_Val);

private:
    boost::YOpt<bool> m_HasAttachments;
    boost::YOpt<YTimeDate> m_LastDeliveredDateTime;
    boost::YOpt<PooledString> m_Preview;
    boost::YOpt<PooledString> m_Topic;
    boost::YOpt<vector<PooledString>> m_UniqueSenders;
	boost::YOpt<PooledString> m_DisplayName;

    vector<BusinessConversationThread> m_Threads;

    RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
    friend class ConversationDeserializer;
};
