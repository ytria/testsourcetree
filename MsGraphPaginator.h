#pragma once

class IMsGraphPageRequester;
class IPageRequestLogger;

class MsGraphPaginator
{
public:
	MsGraphPaginator(const web::uri& p_Uri, const shared_ptr<IMsGraphPageRequester>& p_Requester, const std::shared_ptr<IPageRequestLogger>& p_Logger);
	MsGraphPaginator(const web::uri& p_Uri, const shared_ptr<IMsGraphPageRequester>& p_Requester, const std::shared_ptr<IPageRequestLogger>& p_Logger, size_t p_PageSize);

	size_t GetPageSize() const;

	TaskWrapper<void> Paginate(const shared_ptr<MSGraphSession>& p_Session, YtriaTaskData p_TaskData);

	const web::uri& GetLatestRequest() const;

	const IMsGraphPageRequester& GetPageRequester() const;
	IMsGraphPageRequester& GetPageRequester();

	void SetLogger(const shared_ptr<IPageRequestLogger>& p_Logger);

	static const size_t g_DefaultPageSize;
	static void Minimize(web::uri_builder& p_Builder);

private:
	void ValidateObj();

	web::uri m_OriginalUri;
	const size_t m_PageSize;
	shared_ptr<IMsGraphPageRequester> m_Requester;
	shared_ptr<web::uri> m_LatestRequest;
	shared_ptr<IPageRequestLogger> m_RequestLogger;

	static web::uri CreateInitialUri(const web::uri& p_OriginalUri, size_t p_PageSize);
	static bool HasNextPage(const web::json::value& p_Json);

	static const wstring g_NextLinkKey;
};
