#include "ItemBodyDeserializer.h"

#include "JsonSerializeUtil.h"

void ItemBodyDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("content"), m_Data.Content, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("contentType"), m_Data.ContentType, p_Object);
}
