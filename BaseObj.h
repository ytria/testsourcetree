#pragma once

#include "BusinessObject.h"

namespace Cosmos
{
    class BaseObj : public BusinessObject
    {
    public:
        boost::YOpt<PooledString> _id;
        boost::YOpt<PooledString> _etag;
        boost::YOpt<PooledString> _ts;
        boost::YOpt<PooledString> _rid;
        boost::YOpt<PooledString> _self;
    };
}
