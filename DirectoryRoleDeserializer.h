#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessDirectoryRole.h"

class DirectoryRoleDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessDirectoryRole>
{
public:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

