#include "YBrowserLink.h"

YBrowserLink::YBrowserLink(const LicenseContext& p_LicenseContext, const web::uri& p_Uri)
	: m_Uri(p_Uri)
	, m_LicenseContext(p_LicenseContext)
	, m_Action(nullptr)
{
}

YBrowserLink::YBrowserLink(const LicenseContext& p_LicenseContext, const web::uri& p_Uri, AutomationAction* p_Action)
	: m_Uri(p_Uri)
	, m_LicenseContext(p_LicenseContext)
	, m_Action(p_Action)
{
}

const web::uri & YBrowserLink::GetUrl() const
{
    return m_Uri;
}

const LicenseContext& YBrowserLink::GetLicenseContext() const
{
	// ASSERT(m_LicenseContext.IsInitialized()); user might not be logged in, session not set: context is then legitimately not initialized (e.g. first launch or explicit logout)
	return m_LicenseContext;
}

const LicenseContext& YBrowserLink::GetLicenseContext(const LicenseContext::TokenRateType p_TR)
{
	// ASSERT(m_LicenseContext.IsInitialized()); user might not be logged in, session not set: context is then legitimately not initialized (e.g. first launch or explicit logout)
	m_LicenseContext.SetTokenRateType(p_TR);
	return m_LicenseContext;
}

AutomationAction* YBrowserLink::GetAutomationAction() const
{
	return m_Action;
}

void YBrowserLink::ENABLE_FREE_ACCESS()// set license free access
{
	m_LicenseContext.ENABLE_FREE_ACCESS(LicenseContext::g_CheckLevel);
}

void YBrowserLink::SetIsLicenseAJL(const bool p_LicenseIsAJL)
{
	m_LicenseContext.SetIsAJL(p_LicenseIsAJL);
}

bool YBrowserLink::IsAllowedWithAJLLicense() const
{
	return m_LicenseContext.IsAJLauthorized();
}