#pragma once

#include "Encapsulate.h"
#include "Deployment.h"
#include "JsonObjectDeserializer.h"

namespace Azure
{
	class DeploymentDeserializer: public JsonObjectDeserializer, public Encapsulate<Azure::Deployment>
	{
	public:
		void DeserializeObject(const web::json::object& p_Object) override;
	};
}
