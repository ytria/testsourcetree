#pragma once

#include "DlgFormsHTML.h"
#include "GridSnapshotUtil.h"

class O365Grid;
class DlgSnapshotOptions : public DlgFormsHTML
{
public:
	DlgSnapshotOptions(GridSnapshot::Options::Mode p_Mode, bool p_SessionIsRBAC, O365Grid* p_Parent);
	virtual ~DlgSnapshotOptions();

	//Returns true if processed
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

	const std::string& GetPassword();
	const GridSnapshot::Options& GetOptions() const;

protected:
	virtual wstring getDialogTitle() const override;
	virtual wstring externalValidationCallback(const wstring p_PropertyName, const wstring p_CurrentValue) override;

private:
	virtual void generateJSONScriptData() override;

	const wstring m_WarningIntroText;

	std::string m_Password;
	GridSnapshot::Options m_Options;

	const bool m_HasHiddenColumns;
	const bool m_HasHiddenRows;
	const bool m_HasVisiblePermanentComments;
	const bool m_HasComparator;
	const bool m_HasPivot;
	const bool m_HasChart;
	const bool m_HasRBACUnauthorizedRows;
};
