#pragma once

#include "CommandInfo.h"
#include "MetaDataColumnInfo.h"
#include "O365ReportCriteria.h"

struct ModuleCriteria
{
	ModuleCriteria() = default;

	Origin m_Origin = Origin::NotSet;

	enum class UsedContainer
	{
		NOTSET,
		IDS,
		SUBIDS,
        SUBSUBIDS,
		O365REPORTCRITERIA,
	}									m_UsedContainer = UsedContainer::NOTSET;

	O365IdsContainer					m_IDs;
	O365SubIdsContainer					m_SubIDs;
    O365SubSubIdsContainer				m_SubSubIDs;
	O365ReportCriteria					m_ReportCriteria;
	RoleDelegationUtil::RBAC_Privilege	m_Privilege		= RoleDelegationUtil::RBAC_NONE;

	boost::YOpt<vector<MetaDataColumnInfo>> m_MetaDataColumnInfo;

	bool Matches(const ModuleCriteria& other) const;

	wstring Serialize() const;
	bool Deserialize(const wstring& p_String);
};
