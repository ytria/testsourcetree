#pragma once

#include "IJsonSerializer.h"

#include "AssignedPlan.h"

class AssignedPlanSerializer : public IJsonSerializer
{
public:
    AssignedPlanSerializer(const AssignedPlan& p_AssignedPlan);
    void Serialize() override;

private:
    const AssignedPlan& m_AssignedPlan;
};

