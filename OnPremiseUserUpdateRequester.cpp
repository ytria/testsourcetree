#include "OnPremiseUserUpdateRequester.h"
#include "PSUtil.h"
#include "Sapio365Session.h"

OnPremiseUserUpdateRequester::OnPremiseUserUpdateRequester(const OnPremiseUser& p_User)
	: m_User(p_User)
{
}

TaskWrapper<void> OnPremiseUserUpdateRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	wstring psCmd = _YTEXT("Set-ADUser ");

	ASSERT(m_User.GetObjectGUID()/* || m_User.GetSamAccountName()*/);
	if (m_User.GetObjectGUID())
		psCmd += wstring(_YTEXT("-Identity ")) + m_User.GetObjectGUID()->c_str();
	/*else if (m_User.GetSamAccountName())
		psCmd += wstring(_YTEXT("-Identity ")) + m_User.GetSamAccountName()->c_str();*/

	PSUtil::AddParamToCmd(psCmd, _YTEXT("AccountExpirationDate"), m_User.GetAccountExpires());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("AccountNotDelegated"), m_User.GetAccountNotDelegated());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("AllowReversiblePasswordEncryption"), m_User.GetAllowReversiblePasswordEncryption());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("AuthType"), m_AuthType);
	PSUtil::AddParamToCmd(psCmd, _YTEXT("CannotChangePassword"), m_User.GetCannotChangePassword());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("ChangePasswordAtLogon"), m_ChangePasswordAtLogon);
	PSUtil::AddParamToCmd(psCmd, _YTEXT("City"), m_User.GetCity());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Company"), m_User.GetCompany());
	//PSUtil::AddParamToCmd(psCmd, _YTEXT("CompoundIdentitySupported"), m_User.GetCompoundIdentitySupported());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Country"), m_User.GetCountry());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Department"), m_User.GetDepartment());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Description"), m_User.GetDescription());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("DisplayName"), m_User.GetDisplayName());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Division"), m_User.GetDivision());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("EmailAddress"), m_User.GetEmailAddress());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("EmployeeID"), m_User.GetEmployeeID());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("EmployeeNumber"), m_User.GetEmployeeNumber());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Enabled"), m_User.GetEnabled());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Fax"), m_User.GetFax());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("GivenName"), m_User.GetGivenName());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("HomeDirectory"), m_User.GetHomeDirectory());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("HomeDrive"), m_User.GetHomeDrive());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("HomePage"), m_User.GetHomePage());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("HomePhone"), m_User.GetHomePhone());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Initials"), m_User.GetInitials());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("ProxyAddresses"), m_User.GetProxyAddresses());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("LogonWorkstations"), m_User.GetLogonWorkstations());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Manager"), m_User.GetManager());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("MobilePhone"), m_User.GetMobilePhone());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Office"), m_User.GetOffice());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("OfficePhone"), m_User.GetOfficePhone());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Organization"), m_User.GetOrganization());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("OtherName"), m_User.GetOtherName());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("POBox"), m_User.GetPOBox());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("PasswordNeverExpires"), m_User.GetPasswordNeverExpires());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("PasswordNotRequired"), m_User.GetPasswordNotRequired());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("PostalCode"), m_User.GetPostalCode());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("ProfilePath"), m_User.GetProfilePath());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("ProfilePath"), m_User.GetProfilePath());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("SamAccountName"), m_User.GetSamAccountName());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("ScriptPath"), m_User.GetScriptPath());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("SmartcardLogonRequired"), m_User.GetSmartcardLogonRequired());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("State"), m_User.GetState());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("StreetAddress"), m_User.GetStreetAddress());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Surname"), m_User.GetSurname());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("Title"), m_User.GetTitle());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("TrustedForDelegation"), m_User.GetTrustedForDelegation());
	PSUtil::AddParamToCmd(psCmd, _YTEXT("UserPrincipalName"), m_User.GetUserPrincipalName());

	ASSERT(!m_Result);
	m_Result = std::make_shared<InvokeResultWrapper>();
	return YSafeCreateTaskMutable([psCmd, result = m_Result, deserializer = m_Deserializer, session = p_Session->GetBasicPowershellSession()/*, logger = m_Logger*/, taskData = p_TaskData]() {
		if (session)
		{
			//logger->Log(taskData.GetOriginator());

			session->AddScript(psCmd.c_str(), taskData.GetOriginator());

			result->SetResult(session->Invoke(taskData.GetOriginator()));
			//if (result->Get().m_Success && nullptr != result->Get().m_Collection)
			//	result->Get().m_Collection->Deserialize(*deserializer);
		}
	});
}

std::shared_ptr<InvokeResultWrapper> OnPremiseUserUpdateRequester::GetResult()
{
	return m_Result;
}

//void OnPremiseUserUpdateRequester::SetUser(const OnPremiseUser& p_OnPremiseUser)
//{
//	m_User = p_OnPremiseUser;
//}

void OnPremiseUserUpdateRequester::SetAuthType(const boost::YOpt<wstring>& p_AuthType)
{
	m_AuthType = p_AuthType;
}

void OnPremiseUserUpdateRequester::SetChangePasswordAtLogon(boost::YOpt<bool>& p_Change)
{
	m_ChangePasswordAtLogon = p_Change;
}
