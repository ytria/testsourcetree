#include "CreateDbRequester.h"
#include "CosmosDBSqlSession.h"
#include "DumpDeserializer.h"
#include "LoggerService.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "AzureADCommonData.h"
#include "BasicHttpRequestLogger.h"

Cosmos::CreateDbRequester::CreateDbRequester(const wstring& p_DatabaseName)
    : m_DbName(p_DatabaseName)
{
}

TaskWrapper<void> Cosmos::CreateDbRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<DumpDeserializer>();
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("dbs"));

    web::json::value body = web::json::value::object();
    body[_YTEXT("id")] = web::json::value::string(m_DbName);

    const WebPayloadJSON payload(body);

    LoggerService::User(YtriaTranslate::Do(Cosmos__CreateDbRequester_Send_1, _YLOC("Creating database with name \"%1\" in CosmosDB"), m_DbName.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetCosmosDBSqlSession()->Post(uri.to_uri(),
        httpLogger,
        payload,
        _YTEXT("dbs"),
        _YTEXT(""),
        p_Session->GetCosmosDbMasterKey(),
        m_Result,
        m_Deserializer,
        p_TaskData);
}

const wstring& Cosmos::CreateDbRequester::GetData() const
{
    return m_Deserializer->GetData();
}

const SingleRequestResult& Cosmos::CreateDbRequester::GetResult() const
{
    return *m_Result;
}
