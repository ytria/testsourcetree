#include "GridSetupMetaColumn.h"

#include "MsGraphFieldNames.h"

const wstring GridSetupMetaColumn::g_ColUIDMetaDisplayName = _YUID(O365_METADISPLAYNAME);
const wstring GridSetupMetaColumn::g_ColUIDMetaUserName = _YUID(O365_METAUSERNAME);
const wstring GridSetupMetaColumn::g_ColUIDMetaId = _YUID(O365_METAID);

GridSetupMetaColumn::GridSetupMetaColumn(GridBackendColumn** p_Column, const wstring& p_DisplayName, const wstring& p_Family)
    :GridSetupMetaColumn(p_Column, p_DisplayName, p_Family, _YTEXT(""))
{

}

GridSetupMetaColumn::GridSetupMetaColumn(GridBackendColumn** p_Column, const wstring& p_DisplayName, const wstring& p_Family, const wstring& p_UID)
    :m_Column(p_Column),
    m_DisplayName(p_DisplayName),
    m_Family(p_Family),
    m_UID(p_UID)
{
}

GridBackendColumn** GridSetupMetaColumn::GetColumn() const
{
    return m_Column;
}

const wstring& GridSetupMetaColumn::GetDisplayName() const
{
    return m_DisplayName;
}

const wstring& GridSetupMetaColumn::GetFamily() const
{
    return m_Family;
}

const wstring& GridSetupMetaColumn::GetUID() const
{
    return m_UID;
}
