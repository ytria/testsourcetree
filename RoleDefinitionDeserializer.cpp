#include "RoleDefinitionDeserializer.h"

#include "BasePermissionsDeserializer.h"
#include "JsonSerializeUtil.h"

void Sp::RoleDefinitionDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("Description"), m_Data.Description, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("Hidden"), m_Data.Hidden, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YTEXT("Id"), m_Data.Id, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("Name"), m_Data.Name, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YTEXT("Order"), m_Data.Order, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YTEXT("RoleTypeKind"), m_Data.RoleTypeKind, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("Name"), m_Data.Name, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("odata.editLink"), m_Data.EditLink, p_Object);

    {
        Sp::BasePermissionsDeserializer deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("BasePermissions"), p_Object))
            m_Data.BasePermissions = std::move(deserializer.GetData());
    }
}
