#pragma once

#include "DlgContentViewer.h"
#include "BusinessEvent.h"
#include "BusinessChatMessage.h"
#include "BusinessMessage.h"
#include "BusinessPost.h"
#include "Event.h"
#include "Message.h"
#include "Post.h"
#include "PooledString.h"
#include "ModuleBase.h"

class AnyItemAttachment;
class O365Grid;

class ModuleUtil
{
public:
    struct PostMetaInfo
    {
        bool operator<(const PostMetaInfo& p_Other) const;
		bool operator==(const PostMetaInfo& p_Other) const;
        PooledString GroupId;
        PooledString ThreadId;
        PooledString ConversationId;
        PooledString ConversationTopic;
        PooledString PostId;
		PooledString m_Context;
    };

    template <class T>
    static vector<std::tuple<T, T, T>> VecPairToVecTuple(const vector<std::pair<T, T>>& p_VecPair)
    {
        vector<std::tuple<T, T, T>> tuples;
        for (const auto& pair : p_VecPair)
        {
            std::tuple<T, T, T> newIds;
            std::get<0>(newIds) = pair.first;
            std::get<1>(newIds) = pair.second;
            tuples.push_back(newIds);
        }
        return tuples;
    }

	template <class T>
	static vector<std::tuple<T, T, T>> SetPairToVecTuple(const set<std::pair<T, T>>& p_SetPair)
	{
		vector<std::tuple<T, T, T>> tuples;
		for (const auto& pair : p_SetPair)
		{
			std::tuple<T, T, T> newIds;
			std::get<0>(newIds) = pair.first;
			std::get<1>(newIds) = pair.second;
			tuples.push_back(newIds);
		}
		return tuples;
	}

	template <class T>
	static vector<T> SetToVector(const set<T>& p_Set)
	{
		return vector<T>(p_Set.begin(), p_Set.end());
	}

	static bool ConfirmApply(GridFrameBase& p_Frame);
	enum class eConfirmRefreshPendingChanges { Cancel, Refresh, SaveAndRefresh };
	static eConfirmRefreshPendingChanges ConfirmRefreshPendingChanges(GridFrameBase& p_Frame);
	static wstring ToString(const Recipient& recipient);
	static wstring ToString(const vector<Recipient>& recipients);

	static DlgContentViewer::HeaderConfig GetDlgContentViewerHeaderConfig(const AnyItemAttachment& p_AnyItemAttachment);
	static DlgContentViewer::HeaderConfig GetDlgContentViewerHeaderConfig(const Message& p_Message);
	static DlgContentViewer::HeaderConfig GetDlgContentViewerHeaderConfig(const Event& p_Event);
	static DlgContentViewer::HeaderConfig GetDlgContentViewerHeaderConfig(const Post& p_Post);
	static DlgContentViewer::HeaderConfig GetDlgContentViewerHeaderConfig(const BusinessMessage& p_Message);
	static DlgContentViewer::HeaderConfig GetDlgContentViewerHeaderConfig(const BusinessEvent& p_Event);
	static DlgContentViewer::HeaderConfig GetDlgContentViewerHeaderConfig(const BusinessPost& p_Post);
	static DlgContentViewer::HeaderConfig GetDlgContentViewerHeaderConfig(const BusinessChatMessage& p_ChatMessage);

	static void ClearModifiedRowsStatus(O365Grid& p_grid);
	static void ClearErrorRowsStatus(O365Grid& p_grid);
    static void MergeAttachments(ModuleBase::AttachmentsContainer& p_OldAttachments, const ModuleBase::AttachmentsContainer& p_NewAttachments);

    static std::vector<BusinessAttachment> SortAttachments(const std::vector<BusinessAttachment>& p_Attachments);
	static bool WarnIfPowerShellHostIncompatible(CWnd* p_Window);
	static bool AskUserPowerShellAuth(const std::shared_ptr<Sapio365Session>& p_Session, CWnd* p_Parent);
};

template<>
struct std::hash<ModuleUtil::PostMetaInfo>
{
	size_t operator()(const ModuleUtil::PostMetaInfo& _Keyval) const
	{
		return (fnv1a_hash_bytes((const unsigned char*)_Keyval.PostId.c_str(), ((const wstring&)_Keyval.PostId).size() * sizeof(wchar_t)));
	}
};
