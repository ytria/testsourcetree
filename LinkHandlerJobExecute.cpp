#include "LinkHandlerJobExecute.h"

#include "MainFrame.h"

void LinkHandlerJobExecute::Handle(YBrowserLink& p_Link) const
{
	// run job whose key is the uri path
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame && mainFrame->GetAutomationWizard() != nullptr);
	if (nullptr != mainFrame && mainFrame->GetAutomationWizard() != nullptr)
	{
		const auto link = p_Link.GetUrl().path().substr(1);// .substr(1): path starts with '/'
		const auto keys = Str::explodeIntoTokenVector(link, JobCenterUtil::g_URLTAG_Preset);
		ASSERT(!keys.empty());
		if (!keys.empty())
		{
			wstring presetK;
			if (keys.size() > 1)
				presetK = keys[1];
			mainFrame->GetAutomationWizard()->Run(keys[0], presetK);
		}
	}
}