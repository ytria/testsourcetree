#pragma once

class Sapio365Session;

namespace Util
{
	pair<wstring, wstring> CreateGlobalAdmin(const wstring& p_TenantName, std::shared_ptr<Sapio365Session> p_Session);
}
