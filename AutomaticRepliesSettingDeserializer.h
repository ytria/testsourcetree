#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class AutomaticRepliesSettingDeserializer : public JsonObjectDeserializer, public Encapsulate<AutomaticRepliesSetting>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

