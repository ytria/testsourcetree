#include "ModificationWithRequestFeedback.h"

ModificationWithRequestFeedback::ModificationWithRequestFeedback(const wstring& p_ObjName)
	:IMainItemModification(Modification::State::AppliedLocally, p_ObjName)
{
}

void ModificationWithRequestFeedback::RefreshState()
{
	if (HasResultError())
		m_State = Modification::State::RemoteError;
	else
		m_State = Modification::State::RemoteHasNewValue;
}

void ModificationWithRequestFeedback::SetResultError(const wstring& p_Error)
{
	m_Error = p_Error;
}

bool ModificationWithRequestFeedback::HasResultError() const
{
	return !m_Error.empty();
}

const wstring& ModificationWithRequestFeedback::GetResultError() const
{
	return m_Error;
}

const row_pk_t& ModificationWithRequestFeedback::GetRowKey() const
{
	return m_RowKey;
}
