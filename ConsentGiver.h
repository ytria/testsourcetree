#pragma once

#include "MSGraphConsentListener.h"

class DlgBrowser;
struct ConsentGiver
{
public:
	static ConsentGiver& GetInstance();

public:
	bool GetConsent(const web::uri& p_BrowserUri, const wstring& p_SessionType, CWnd* p_Parent, const std::function<void(ConsentResult)>& p_ConsentResultCallback, const wstring& p_ConsentDlgTitle);

private:
	void openMsGraphAdminConsentBrowser(const web::uri_builder& p_Uri, const wstring& p_DlgTitle);

private:
	std::shared_ptr<MSGraphConsentListener> m_ConsentListener;
	CWnd* m_Parent;
	std::unique_ptr<DlgBrowser> m_AdminConsentBrowser;
};