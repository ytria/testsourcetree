#pragma once

#include "IDatedJsonDeserializer.h"

template<typename T>
class IDeserializerFactory
{
public:
    using DeserializerType = T;

	IDeserializerFactory() = default;
    virtual ~IDeserializerFactory() = default;

    virtual DeserializerType Create() = 0;
};

template <typename T>
class DefaultFactory : public IDeserializerFactory<T>
{
public:
    DeserializerType Create() override
    {
        return DeserializerType();
    }
};
