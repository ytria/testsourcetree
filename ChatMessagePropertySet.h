#pragma once

#include "IPropertySetBuilder.h"

class ChatMessagePropertySet : public IPropertySetBuilder
{
public:
    virtual vector<rttr::property> GetPropertySet() const override;
	virtual vector<PooledString> GetStringPropertySet() const override;
};

