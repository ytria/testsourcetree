#pragma once

#include "DlgGenPropModifHTML.h"

template<class T>
DlgGenPropModifHTML<T>::DlgGenPropModifHTML(vector<T>& p_Objects, DlgFormsHTML::Action p_Action, bool p_ObjectsAreMoreLoaded, CWnd* p_Parent)
	: DlgFormsHTML(p_Action, p_Parent)
	, m_Objects(p_Objects)
	, m_ObjectsAreMoreLoaded(p_ObjectsAreMoreLoaded)
{
	ASSERT(!m_Objects.empty());
}

template<class T>
DlgGenPropModifHTML<T>::~DlgGenPropModifHTML()
{
}

template<class T>
void DlgGenPropModifHTML<T>::generateJSONScriptData()
{
	initMain(_YTEXT("DlgGenPropModifHTML"));

	rttr::type classType = rttr::type::get<T>();

	if (classType.is_valid())
	{
		bool firstCategory = true;
		for (const auto& prop : classType.get_properties())
		{
			// Not used for now, but might be in the future
			const bool hidden = prop.get_metadata(MetadataKeys::Hidden).is_valid() && prop.get_metadata(MetadataKeys::Hidden).get_value<bool>();

			if (!hidden)
			{
				ASSERT(FALSE); // The following code is deprecated
				const bool isLoadMore = prop.get_metadata(MetadataKeys::SyncOnly).is_valid()
					&& prop.get_metadata(MetadataKeys::SyncOnly).get_value<bool>();

				const bool readOnly = prop.get_metadata(MetadataKeys::ReadOnly).is_valid() && prop.get_metadata(MetadataKeys::ReadOnly).get_value<bool>()
					|| isLoadMore && !m_ObjectsAreMoreLoaded;

				ASSERT(FALSE); // The following code is deprecated
				const bool required = isCreateDialog()
					&& prop.get_metadata(MetadataKeys::ValidForCreation).is_valid()
					&& prop.get_metadata(MetadataKeys::ValidForCreation).get_value<bool>();

				const wstring propName = MFCUtil::convertASCII_to_UNICODE(prop.get_name().to_string());
				const wstring displayName = [&prop, &propName]() {
					ASSERT(FALSE); // The following code is deprecated
					auto varDisplayName = prop.get_metadata(MetadataKeys::TypeDisplayName);
					return varDisplayName.is_valid() ? varDisplayName.get_value<wstring>() : propName;
				}();
				const wstring category = [&prop]() {
					auto varCategory = prop.get_metadata(MetadataKeys::Category);
					return varCategory.is_valid() ? varCategory.get_value<wstring>() : _YTEXT("");
				}();
				//const auto listOfValues = [&prop, &propName]() {
				//	ASSERT(FALSE); // The following code is deprecated
				//	auto varListOfValues = prop.get_metadata(MetadataKeys::ListOfValues);
				//	return varListOfValues.is_valid() ? varListOfValues.get_value<map<wstring, wstring>>() : map<wstring, wstring>{};
				//}();

				if (!category.empty())
				{
					getGenerator().addCategory(category, firstCategory ? CategoryFlags::EXPAND_BY_DEFAULT : CategoryFlags::NOFLAG);
					firstCategory = false;
				}

				wstring stringValue;
				bool boolValue = false;
				bool noChange = false;

				if (isEditDialog() || isReadDialog())
				{
					if (prop.get_type() == rttr::type::get<boost::YOpt<PooledString>>())
					{
						bool allTheSame = true;
						const auto val = prop.get_value(m_Objects.front()).get_value<boost::YOpt<PooledString>>();
						for (const auto& businessObject : m_Objects)
						{
							if (val != prop.get_value(businessObject).get_value<boost::YOpt<PooledString>>())
							{
								allTheSame = false;
								break;
							}
						}

						if (allTheSame)
							stringValue = val.is_initialized() ? (const wstring)val.get() : L"";
						noChange = !allTheSame;
					}
					else if (prop.get_type() == rttr::type::get<PooledString>())
					{
						bool allTheSame = true;
						const auto val = prop.get_value(m_Objects.front()).get_value<PooledString>();
						for (const auto& businessObject : m_Objects)
						{
							if (val != prop.get_value(businessObject).get_value<PooledString>())
							{
								allTheSame = false;
								break;
							}
						}

						if (allTheSame)
							stringValue = val;
						noChange = !allTheSame;
					}
					else if (prop.get_type() == rttr::type::get<boost::YOpt<bool>>())
					{
						bool allTheSame = true;
						const auto val = prop.get_value(m_Objects.front()).get_value<boost::YOpt<bool>>();
						for (const auto& businessObject : m_Objects)
						{
							if (val != prop.get_value(businessObject).get_value<boost::YOpt<bool>>())
							{
								allTheSame = false;
								break;
							}
						}

						if (allTheSame)
							boolValue = val.is_initialized() ? val.get() : false;

						noChange = !allTheSame;
					}
					else if (prop.get_type() == rttr::type::get<bool>())
					{
						bool allTheSame = true;
						const auto val = prop.get_value(m_Objects.front()).get_value<bool>();
						for (const auto& businessObject : m_Objects)
						{
							if (val != prop.get_value(businessObject).get_value<bool>())
							{
								allTheSame = false;
								break;
							}
						}

						if (allTheSame)
							boolValue = val;
						noChange = !allTheSame;
					}
					else
					{
						// FIXME: Handle other types!
						//ASSERT(false);
						continue;
					}
				}

				const uint32_t flags =	(readOnly ? EditorFlags::READONLY : 0)
									|	(required ? EditorFlags::REQUIRED : 0)
									|	(noChange ? EditorFlags::NOCHANGE : 0);


				if (prop.get_type() == rttr::type::get<boost::YOpt<PooledString>>()
					|| prop.get_type() == rttr::type::get<PooledString>())
				{
					//if (listOfValues.empty())
						getGenerator().Add(StringEditor(propName, displayName, flags, stringValue));
					//else
					//	getGenerator().Add(ComboEditor({propName, displayName, flags, stringValue}, listOfValues));
				}
				else if (prop.get_type() == rttr::type::get<boost::YOpt<bool>>()
					|| prop.get_type() == rttr::type::get<bool>())
				{
					getGenerator().Add(BoolEditor({ propName, displayName, flags, boolValue }));
				}
			}
		}
	}

	if (isReadDialog())
	{
		getGenerator().addCloseButton(LocalizedStrings::g_CloseButtonText);
	}
	else
	{
		getGenerator().addApplyButton(isEditDialog() ? LocalizedStrings::g_ApplyButtonText : LocalizedStrings::g_CreateButtonText);
		getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
		getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);
	}
}

template<class T>
bool DlgGenPropModifHTML<T>::processPostedData(const IPostedDataTarget::PostedData& data)
{
	for (const auto& item : data)
	{
		const auto& propName = item.first;
		const auto& propValue = item.second;

		rttr::type classType = rttr::type::get<T>();
		rttr::property prop = classType.get_property(MFCUtil::convertUNICODE_to_ASCII(propName));

		if (prop.is_valid())
		{
			for (auto& businessObject : m_Objects)
			{
				if (prop.get_type() == rttr::type::get<boost::YOpt<PooledString>>())
				{
					bool success = prop.set_value(businessObject, boost::YOpt<PooledString>(propValue));
					ASSERT(success);
				}
				else if (prop.get_type() == rttr::type::get<PooledString>())
				{
					bool success = prop.set_value(businessObject, PooledString(propValue));
					ASSERT(success);
				}
				else if (prop.get_type() == rttr::type::get<boost::YOpt<bool>>())
				{
					bool success = prop.set_value(businessObject, boost::YOpt<bool>(Str::getBoolFromString(propValue)));
					ASSERT(success);
				}
				else if (prop.get_type() == rttr::type::get<bool>())
				{
					bool success = prop.set_value(businessObject, Str::getBoolFromString(propValue));
					ASSERT(success);
				}
				else
				{
					// TODO: Other types?
					ASSERT(false);
				}
			}
		}
	}

	return true;
}
