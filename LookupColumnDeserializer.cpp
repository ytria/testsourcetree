#include "LookupColumnDeserializer.h"

#include "JsonSerializeUtil.h"

void LookupColumnDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowMultipleValues"), m_Data.AllowMultipleValues, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowUnlimitedLength"), m_Data.AllowUnlimitedLength, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("columnName"), m_Data.ColumnName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("listId"), m_Data.ListId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("primaryLookupColumnId"), m_Data.PrimaryLookupColumnId, p_Object);
}
