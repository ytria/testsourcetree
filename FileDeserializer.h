#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "File.h"

class FileDeserializer : public JsonObjectDeserializer, public Encapsulate<File>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

