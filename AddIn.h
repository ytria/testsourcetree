#pragma once

#include "KeyValue.h"

class AddIn
{
public:
	boost::YOpt<PooledString> m_Id;
	boost::YOpt<vector<KeyValue>> m_Properties;
	boost::YOpt<PooledString> m_Type;
};

