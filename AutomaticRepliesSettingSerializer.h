#pragma once

#include "IJsonSerializer.h"

class AutomaticRepliesSettingSerializer : public IJsonSerializer
{
public:
    AutomaticRepliesSettingSerializer(const AutomaticRepliesSetting& p_AutoRepSet);
    void Serialize() override;

private:
    const AutomaticRepliesSetting& m_AutoRepSet;
};

