#pragma once

#include <string>
#include <vector>

#include "IPSObjectCollectionDeserializer.h"

class ADDomainControllerCollectionDeserializer : public IPSObjectCollectionDeserializer
{
public:
	void Deserialize(IPSObjectPropertyReader& p_Reader) override;
	void SetCount(size_t p_Count) override;

	wstring GetDomain() const;

private:
	std::vector<wstring> m_Domains;
};

