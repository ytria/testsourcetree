#pragma once

#include "BusinessObject.h"

#include "BusinessAssignedLicense.h"
#include "BusinessDrive.h"
#include "BusinessGroup.h"
#include "OnPremiseUser.h"
#include "PooledString.h"
#include "User.h"
#include "HttpResultWithError.h"

#include <vector>

#define ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING 0

//class BusinessGroup;
class CachedUser;
class Sapio365Session;

class BusinessUser : public BusinessObject
{
public:
	static const wstring g_UserTypeMember;
	static const wstring g_UserTypeGuest;

public:
	BusinessUser();
    BusinessUser(const User& p_User);
	BusinessUser(const CachedUser& p_User);

	BusinessUser& MergeWith(const BusinessUser& p_Other);

	void SetValuesFrom(const User& p_User);
	void SetValuesFrom(const CachedUser& p_User);

	bool IsMoreLoaded() const override;

    using byte_t = uint8_t;

	const vector<PooledString>&	GetParentGroups() const;
	const vector<PooledString>&	GetParentGroupsIDsFirstLevel() const;
	void SetParentGroups(const vector<PooledString>& p_GroupIds);
	void SetFirstLevelParentGroups(const vector<PooledString>& p_Groups);
	bool IsDirectMemberOf(const BusinessGroup& p_Group) const;

#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
	void							SetManager(const BusinessUser& p_Manager);
	std::shared_ptr<BusinessUser>&	GetManager();
	BusinessUser*					GetManagerTop();// ptr to highest personager in hierarchy - 'this' if none set - never nullptr
	bool							HasManager() const;

	void							AddDirectReport(const BusinessUser& p_ManagerID);
	const vector<std::unique_ptr<BusinessUser>>&	GetDirectReports() const;
	bool							HasDirecReports() const;
	bool							IntegrateIntoManagerHierarchy(BusinessUser& p_User);// if p_User is a direct report to this user or to any of its direct reports, add it to the relevant user's DRs and retrun true
#endif
	void SetManagerID(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetManagerID() const;
	void SetManagerError(const boost::YOpt<SapioError>& p_Val);
	const boost::YOpt<SapioError>& GetManagerError() const;
	boost::YOpt<SapioError> GetMailboxInfoError() const;
	void SetMailboxInfoError(boost::YOpt<SapioError> val);
	boost::YOpt<SapioError> GetRecipientPermissionsError() const;
	void SetRecipientPermissionsError(boost::YOpt<SapioError> val);
	

	const boost::YOpt<PooledString>& GetAboutMe() const;
	void SetAboutMe(const boost::YOpt<PooledString>& val);
	const boost::YOpt<bool>& GetAccountEnabled() const;
	void SetAccountEnabled(const boost::YOpt<bool>& val);
	const boost::YOpt<PooledString>& GetAgeGroup() const;
	void SetAgeGroup(const boost::YOpt<PooledString>& val);
	const boost::YOpt<vector<BusinessAssignedLicense>>& GetAssignedLicenses() const;
	boost::YOpt<vector<BusinessAssignedLicense>>& GetAssignedLicenses();
	void SetAssignedLicenses(const vector<BusinessAssignedLicense>& val);
	const boost::YOpt<vector<AssignedPlan>>& GetAssignedPlans() const;
	void SetAssignedPlans(const vector<AssignedPlan>& val);
	const boost::YOpt<YTimeDate>& GetBirthday() const;
	void SetBirthday(const boost::YOpt<YTimeDate>& val);

	// In Graph this is a multivalue, but in reality it's not. That's why we have both accessors.
	const boost::YOpt<PooledString>& GetBusinessPhone() const;
	void SetBusinessPhone(const boost::YOpt<PooledString>& val);
	const boost::YOpt<vector<PooledString>>& GetBusinessPhones() const;
	void SetBusinessPhones(const vector<PooledString>& val);

	const boost::YOpt<PooledString>& GetCity() const;
	void SetCity(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetConsentProvidedForMinor() const;
	void SetConsentProvidedForMinor(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetCountry() const;
	void SetCountry(const boost::YOpt<PooledString>& val);
	const boost::YOpt<YTimeDate>& GetCreatedDateTime() const;
	void SetCreatedDateTime(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<PooledString>& GetDepartment() const;
	void SetDepartment(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<PooledString>& GetGivenName() const;
	void SetGivenName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<YTimeDate>& GetHireDate() const;
	void SetHireDate(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<vector<PooledString>>& GetInterests() const;
	void SetInterests(const boost::YOpt<vector<PooledString>>& val);
	const boost::YOpt<PooledString>& GetJobTitle() const;
	void SetJobTitle(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetLegalAgeGroupClassification() const;
	void SetLegalAgeGroupClassification(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetMail() const;
	void SetMail(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetMailNickname() const;
	void SetMailNickname(const boost::YOpt<PooledString>& val);
	const boost::YOpt<vector<PooledString>>& GetMailboxCanSendOnBehalf() const;
	void SetMailboxCanSendOnBehalf(const boost::YOpt<vector<PooledString>>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetMailboxCanSendAsUser() const;
	void SetMailboxCanSendAsUser(const boost::YOpt<vector<PooledString>>& p_Val);

	const boost::YOpt<BusinessDrive>& GetDrive() const;
	void SetDrive(const boost::YOpt<BusinessDrive>& p_Val);
	const boost::YOpt<SapioError>& GetDriveError() const;
	void SetDriveError(const boost::YOpt<SapioError>& p_Val);

	const boost::YOpt<MailboxSettings>& GetMailboxSettings() const;
	//const boost::YOpt<PooledString>& GetMailboxSettingsArchiveFolder() const;
	//const boost::YOpt<AutomaticRepliesSetting>& GetMailboxSettingsAutomaticRepliesSetting() const;
	//const boost::YOpt<PooledString>& GetMailboxSettingsLanguage() const; // == LocaleInfo.Locale as LocaleInfo.DisplayName is not editable
	//const boost::YOpt<PooledString>& GetMailboxSettingsTimeZone() const;

	void SetMailboxSettings(const boost::YOpt<MailboxSettings>& val);
	//void SetMailboxSettingsArchiveFolder(const boost::YOpt<PooledString>&) const;
	//void SetMailboxSettingsAutomaticRepliesSetting(const boost::YOpt<AutomaticRepliesSetting>&) const;
	//void SetMailboxSettingsLanguage(const boost::YOpt<PooledString>&) const; // == LocaleInfo.Locale as LocaleInfo.DisplayName is not editable
	//void SetMailboxSettingsTimeZone(const boost::YOpt<PooledString>&) const;

	const boost::YOpt<SapioError>&	GetMailboxSettingsError() const;
	void							SetMailboxSettingsError(const boost::YOpt<SapioError>& p_Val);

	const boost::YOpt<PooledString>& GetMobilePhone() const;
	void SetMobilePhone(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetMySite() const;
	void SetMySite(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetOfficeLocation() const;
	void SetOfficeLocation(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetOnPremisesDomainName() const;
	void SetOnPremisesDomainName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetOnPremisesImmutableId() const;
	void SetOnPremisesImmutableId(const boost::YOpt<PooledString>& val);
	const boost::YOpt<YTimeDate>& GetOnPremisesLastSyncDateTime() const;
	void SetOnPremisesLastSyncDateTime(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<vector<OnPremisesProvisioningError>>& GetOnPremisesProvisioningErrors() const;
	void SetOnPremisesProvisioningErrors(const boost::YOpt<vector<OnPremisesProvisioningError>>& val);
	const boost::YOpt<PooledString>& GetOnPremisesSamAccountName() const;
	void SetOnPremisesSamAccountName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetOnPremisesSecurityIdentifier() const;
	void SetOnPremisesSecurityIdentifier(const boost::YOpt<PooledString>& val);
	const boost::YOpt<bool>& GetOnPremisesSyncEnabled() const;
	void SetOnPremisesSyncEnabled(const boost::YOpt<bool>& val);
	const boost::YOpt<PooledString>& GetOnPremisesUserPrincipalName() const;
	void SetOnPremisesUserPrincipalName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetPasswordPolicies() const;
	void SetPasswordPolicies(const boost::YOpt<PooledString>& val);
	void SetPasswordProfile(const boost::YOpt<PasswordProfile>& val);
	const boost::YOpt<PooledString>& GetPassword() const;
	void SetPassword(const boost::YOpt<PooledString>& val);
	const boost::YOpt<bool>& GetForceChangePassword() const;
	void SetForceChangePassword(const boost::YOpt<bool>& p_Val);
	const boost::YOpt<bool>& GetForceChangePasswordWithMfa() const;
	void SetForceChangePasswordWithMfa(const boost::YOpt<bool>& p_Val);
	const boost::YOpt<vector<PooledString>>& GetPastProjects() const;
	void SetPastProjects(const boost::YOpt<vector<PooledString>>& val);
	const boost::YOpt<PooledString>& GetPostalCode() const;
	void SetPostalCode(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetPreferredLanguage() const;
	void SetPreferredLanguage(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetPreferredName() const;
	void SetPreferredName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<vector<ProvisionedPlan>>& GetProvisionedPlans() const;
	void SetProvisionedPlans(const vector<ProvisionedPlan>& val);
	const boost::YOpt<vector<PooledString>>& GetProxyAddresses() const;
	void SetProxyAddresses(const vector<PooledString>& val);

	const boost::YOpt<YTimeDate>& GetSignInSessionsValidFromDateTime() const;
	void SetSignInSessionsValidFromDateTime(const boost::YOpt<YTimeDate>& val);
	
	const boost::YOpt<vector<PooledString>>& GetResponsibilities() const;
	void SetResponsibilities(const boost::YOpt<vector<PooledString>>& val);
	const boost::YOpt<vector<PooledString>>& GetSchools() const;
	void SetSchools(const boost::YOpt<vector<PooledString>>& val);
	const boost::YOpt<vector<PooledString>>& GetSkills() const;
	void SetSkills(const boost::YOpt<vector<PooledString>>& val);
	const boost::YOpt<PooledString>& GetState() const;
	void SetState(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetStreetAddress() const;
	void SetStreetAddress(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetSurname() const;
	void SetSurname(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetUsageLocation() const;
	void SetUsageLocation(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetUserPrincipalName() const;
	void SetUserPrincipalName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetUserType() const;
	void SetUserType(const boost::YOpt<PooledString>& val);
    const boost::YOpt<vector<PooledString>>& GetImAddresses() const;
    void SetImAddresses(const boost::YOpt<vector<PooledString>>& p_Val);
    const boost::YOpt<PooledString>& GetCompanyName() const;
    void SetCompanyName(const boost::YOpt<PooledString>& p_Val);

	void SetUserPrincipalNameWithoutDomain(const boost::YOpt<PooledString>& val);
	void SetUserPrincipalNameDomain(const boost::YOpt<PooledString>& val);

	const boost::YOpt<vector<PooledString>>&	GetOtherMails() const;
	void SetOtherMails(const boost::YOpt<vector<PooledString>>& val);
	const boost::YOpt<PooledString>&			GetPreferredDataLocation() const;
	void SetPreferredDataLocation(const boost::YOpt<PooledString>& val);
	const boost::YOpt<bool>&					GetShowInAddressList() const;
	void SetShowInAddressList(const boost::YOpt<bool>& val);
	const boost::YOpt<PooledString>&			GetExternalUserStatus() const;
	void SetExternalUserStatus(const boost::YOpt<PooledString>& val);
	const boost::YOpt<YTimeDate>&				GetExternalUserStatusChangedOn() const;
	void SetExternalUserStatusChangedOn(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<OnPremisesExtensionAttributes>&	GetOnPremisesExtensionAttributes() const;
	void SetOnPremisesExtensionAttributes(const boost::YOpt<OnPremisesExtensionAttributes>& val);

	const boost::YOpt<PooledString>&	GetEmployeeID() const;
	void SetEmployeeID(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>&	GetFaxNumber() const;
	void SetFaxNumber(const boost::YOpt<PooledString>& val);
	const boost::YOpt<bool>&	GetIsResourceAccount() const;
	void SetIsResourceAccount(const boost::YOpt<bool>& val);
	const boost::YOpt<PooledString>&	GetOnPremisesDistinguishedName() const;
	void SetOnPremisesDistinguishedName(const boost::YOpt<PooledString>& val);

	const boost::YOpt<PooledString>& GetCreationType() const;
	void SetCreationType(const boost::YOpt<PooledString>& val);
	const boost::YOpt<vector<ObjectIdentity>>& GetIdentities() const;
	void SetIdentities(const boost::YOpt<vector<ObjectIdentity>>& val);
	const boost::YOpt<YTimeDate>& GetLastPasswordChangeDateTime() const;
	void SetLastPasswordChangeDateTime(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<vector<LicenseAssignmentState>>& GetLicenseAssignmentStates() const;
	boost::YOpt<vector<LicenseAssignmentState>>& GetLicenseAssignmentStates(); // For specific case in ModuleUser.cpp
	void SetLicenseAssignmentStates(const boost::YOpt<vector<LicenseAssignmentState>>& val);
	const boost::YOpt<YTimeDate>& GetRefresTokenValidFromDateTime() const;
	void SetRefresTokenValidFromDateTime(const boost::YOpt<YTimeDate>& val);

	boost::YOpt<bool> GetMailboxDeliverToMailboxAndForward() const;
	void SetMailboxDeliverToMailboxAndForward(boost::YOpt<bool> val);
	boost::YOpt<PooledString> GetMailboxForwardingSmtpAddress() const;
	void SetMailboxForwardingSmtpAddress(boost::YOpt<PooledString> val);
	boost::YOpt<PooledString> GetMailboxType() const;
	void SetMailboxType(const boost::YOpt<PooledString>& val);
	boost::YOpt<PooledString> GetMailboxForwardingAddress() const;
	void SetMailboxForwardingAddress(const boost::YOpt<PooledString>& val);

	const boost::YOpt<OnPremiseUser>& GetOnPremiseUser() const;
	boost::YOpt<OnPremiseUser>& GetOnPremiseUser();

	// For deleted user
	void							SetDeletedDateTime(const boost::YOpt<YTimeDate>& p_DelDate);
	void							SetDeletedDateTime(const boost::YOpt<PooledString>& p_DelDate);
	const boost::YOpt<YTimeDate>&	GetDeletedDateTime() const;
	bool							IsFromRecycleBin() const;

    // Non property
    const boost::YOpt<PooledString>& GetArchiveFolderName() const;
    void SetArchiveFolderName(const boost::YOpt<PooledString>& p_Val);
	void SetArchiveFolderNameError(const boost::YOpt<SapioError>& p_Val);
	const boost::YOpt<SapioError>& GetArchiveFolderNameError() const;

    static User ToUser(const BusinessUser& p_BusinessUser);

	bool operator<(const BusinessUser& p_Other) const;

	virtual wstring GetValue(const wstring& p_PropName) const override;
	virtual vector<PooledString> GetProperties(const wstring& p_PropName) const override;

	void ClearLicenseData(); // Clear m_AssignedLicenses, m_AssignedPlans, m_ProvisionedPlans

	// Only used when applying Revoke Access
	void SetShouldRevokeAccess(bool p_Revoke);
	bool GetShouldRevokeAccess() const;

public: // For edition only
#define DeclAccessOnPremisesExtensionAttribute(_index) \
	const boost::YOpt<PooledString>& GetOnPremisesExtensionAttribute##_index() const; \
	void SetOnPremisesExtensionAttribute##_index(const boost::YOpt<PooledString>& val); \

	DeclAccessOnPremisesExtensionAttribute(1)
	DeclAccessOnPremisesExtensionAttribute(2)
	DeclAccessOnPremisesExtensionAttribute(3)
	DeclAccessOnPremisesExtensionAttribute(4)
	DeclAccessOnPremisesExtensionAttribute(5)
	DeclAccessOnPremisesExtensionAttribute(6)
	DeclAccessOnPremisesExtensionAttribute(7)
	DeclAccessOnPremisesExtensionAttribute(8)
	DeclAccessOnPremisesExtensionAttribute(9)
	DeclAccessOnPremisesExtensionAttribute(10)
	DeclAccessOnPremisesExtensionAttribute(11)
	DeclAccessOnPremisesExtensionAttribute(12)
	DeclAccessOnPremisesExtensionAttribute(13)
	DeclAccessOnPremisesExtensionAttribute(14)
	DeclAccessOnPremisesExtensionAttribute(15)

#undef AccessOnPremisesExtensionAttribute

protected:
	virtual pplx::task<vector<HttpResultWithError>> SendEditRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	virtual TaskWrapper<HttpResultWithError> SendCreateRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	virtual TaskWrapper<HttpResultWithError> SendDeleteRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	virtual TaskWrapper<HttpResultWithError> SendRestoreRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	virtual TaskWrapper<HttpResultWithError> SendHardDeleteRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;

private:
	void SetAssignedLicenses(const vector<AssignedLicense>& val);
	void CopyAssignedLicensesTo(vector<AssignedLicense>& val) const;

private:
	/**********************************************************/
	/*** WARNING: If adding a field, update MergeWith !!!!! ***/
	/**********************************************************/
	boost::YOpt<PooledString>							m_AboutMe;
	boost::YOpt<bool>									m_AccountEnabled;
	boost::YOpt<PooledString>							m_AgeGroup;
	boost::YOpt<vector<BusinessAssignedLicense>>		m_AssignedLicenses;
	boost::YOpt<vector<AssignedPlan>>					m_AssignedPlans;// TODO BusinessPlan
	boost::YOpt<YTimeDate>								m_Birthday;
	boost::YOpt<vector<PooledString>>					m_BusinessPhones;
	mutable boost::YOpt<PooledString>					m_LastGetBusinessPhone; // Just for GetBusinessPhone()
	boost::YOpt<PooledString>							m_City;
    boost::YOpt<PooledString>							m_CompanyName;
	boost::YOpt<PooledString>							m_ConsentProvidedForMinor;
	boost::YOpt<PooledString>							m_Country;
	boost::YOpt<YTimeDate>								m_CreatedDateTime;
	boost::YOpt<PooledString>							m_Department;
	boost::YOpt<PooledString>							m_DisplayName;	
	boost::YOpt<bool>									m_ForceChangePassword;
	boost::YOpt<bool>									m_ForceChangePasswordWithMfa;
	boost::YOpt<PooledString>							m_GivenName;
	boost::YOpt<YTimeDate>								m_HireDate;
	boost::YOpt<vector<PooledString>>					m_ImAddresses;
    boost::YOpt<vector<PooledString>>					m_Interests;
	boost::YOpt<PooledString>							m_JobTitle;
	boost::YOpt<PooledString>							m_LegalAgeGroupClassification;
	boost::YOpt<PooledString>							m_Mail;
	boost::YOpt<PooledString>							m_MailNickname;
	boost::YOpt<MailboxSettings>   						m_MailboxSettings;
	boost::YOpt<PooledString>							m_MobilePhone;
	boost::YOpt<PooledString>							m_MySite;
	boost::YOpt<PooledString>							m_OfficeLocation;
	boost::YOpt<PooledString>							m_OnPremisesDomainName;
	boost::YOpt<PooledString>							m_OnPremisesImmutableId;
	boost::YOpt<YTimeDate>								m_OnPremisesLastSyncDateTime;
	boost::YOpt<vector<OnPremisesProvisioningError>>	m_OnPremisesProvisioningErrors;
	boost::YOpt<PooledString>							m_OnPremisesSamAccountName;
	boost::YOpt<PooledString>							m_OnPremisesSecurityIdentifier;
	boost::YOpt<bool>									m_OnPremisesSyncEnabled;
	boost::YOpt<PooledString>							m_OnPremisesUserPrincipalName;
	boost::YOpt<PooledString>							m_PasswordPolicies;
	boost::YOpt<PooledString>							m_Password;
	boost::YOpt<vector<PooledString>>					m_PastProjects;
	boost::YOpt<PooledString>							m_PostalCode;
	boost::YOpt<PooledString>							m_PreferredLanguage;
	boost::YOpt<PooledString>							m_PreferredName;
	boost::YOpt<vector<ProvisionedPlan>>				m_ProvisionedPlans;
	boost::YOpt<vector<PooledString>>					m_ProxyAddresses;
	boost::YOpt<YTimeDate>								m_SignInSessionsValidFromDateTime;
	boost::YOpt<vector<PooledString>>					m_Responsibilities;
	boost::YOpt<vector<PooledString>>					m_Schools;
	boost::YOpt<vector<PooledString>>					m_Skills;
	boost::YOpt<PooledString>							m_State;
	boost::YOpt<PooledString>							m_StreetAddress;
	boost::YOpt<PooledString>							m_Surname;
	boost::YOpt<PooledString>							m_UsageLocation;
	boost::YOpt<PooledString>							m_UserPrincipalName;
	boost::YOpt<PooledString>							m_UserType;

	boost::YOpt<PooledString>							m_ArchiveFolderName;

	boost::YOpt<SapioError>								m_ArchiveFolderNameError;
	boost::YOpt<SapioError>								m_MailboxSettingsError;
	boost::YOpt<SapioError>								m_DriveError;
	boost::YOpt<SapioError>								m_ManagerError;
	boost::YOpt<SapioError>								m_MailboxInfoError;
	boost::YOpt<SapioError>								m_RecipientPermissionsError;

	boost::YOpt<YTimeDate>								m_DeletedDateTime; // For deleted user

	vector<PooledString>								m_ParentGroups;
	vector<PooledString>								m_ParentGroupIDsFirstLevel;


	// Recently added (2019/03/20)
	boost::YOpt<PooledString>							m_EmployeeID;
	boost::YOpt<PooledString>							m_FaxNumber;
	boost::YOpt<bool>									m_IsResourceAccount; // none == false
	boost::YOpt<PooledString>							m_OnPremisesDistinguishedName; // RO

	// Recently added to v1.0 (2019/03/22)
	boost::YOpt<vector<PooledString>>					m_OtherMails;
	boost::YOpt<PooledString>							m_PreferredDataLocation;
	boost::YOpt<bool>									m_ShowInAddressList;
	boost::YOpt<OnPremisesExtensionAttributes>			m_OnPremisesExtensionAttributes;

	// Recently added (2020/05/04)
	boost::YOpt<PooledString>							m_CreationType;
	boost::YOpt<vector<ObjectIdentity>>					m_Identities;
	boost::YOpt<YTimeDate>								m_LastPasswordChangeDateTime;
	boost::YOpt<vector<LicenseAssignmentState>>			m_LicenseAssignmentStates;
	boost::YOpt<YTimeDate>								m_RefresTokenValidFromDateTime;

	// Powershell fields
	boost::YOpt<PooledString>							m_MailboxType;
	boost::YOpt<bool>									m_DeliverToMailboxAndForward;
	boost::YOpt<PooledString>							m_ForwardingSmtpAddress;
	boost::YOpt<PooledString>							m_ForwardingAddress;
	boost::YOpt<vector<PooledString>>					m_MailboxCanSendOnBehalf;
	boost::YOpt<vector<PooledString>>					m_MailboxCanSendAsUser;

	// manager hierarchy
#if ENABLE_ADVANCED_MANAGER_HIERARCHY_HANDLING
	std::shared_ptr<BusinessUser>	m_Manager;
	vector<std::unique_ptr<BusinessUser>>	m_DirectReports;
#endif
	boost::YOpt<PooledString>							m_ManagerID;

	/* Beta API */
	boost::YOpt<PooledString>							m_ExternalUserStatus;
	boost::YOpt<YTimeDate>								m_ExternalUserStatusChangedOn;

	boost::YOpt<BusinessDrive>							m_Drive;

	boost::YOpt<OnPremiseUser>							m_OnPremUser;

	bool												m_ShouldRevokeAccess = false; // Only used when applying Revoke Access

    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND

    friend class UserDeserializer;
	friend class EducationUserDeserializer;
    friend class UserNonPropDeserializer;
    friend class DbUserSerializer;
};

DECLARE_BUSINESSOBJECT_EQUALTO_AND_HASH_SPECIALIZATION(BusinessUser)