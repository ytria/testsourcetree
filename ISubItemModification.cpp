#include "ISubItemModification.h"

#include "BaseO365Grid.h"
#include "CompositeSubElementFormatter.h"

#undef max

ISubItemModification::ISubItemModification(O365Grid& p_Grid, const SubItemKey& p_Key, GridBackendColumn* p_ColSubItemElder, GridBackendColumn* p_ColSubItemPrimaryKey, const wstring& p_ObjectName)
    : Modification(State::AppliedLocally, p_ObjectName)
	, m_Key(p_Key)
	, m_Grid(p_Grid)
	, m_ColSubItemElder(p_ColSubItemElder)
	, m_ColSubItemPrimaryKey(p_ColSubItemPrimaryKey)
	, m_FormatterId(0)
{
}

ISubItemModification::~ISubItemModification()
{
    // TODO When handling sub item modifications individually
}

bool ISubItemModification::ShouldCreateFormatter(GridBackendRow* p_Row, size_t p_Index) const
{
    return true;
}

bool ISubItemModification::RefreshIfCorrespondingRow(GridBackendRow* p_Row, const std::map<SubItemKey, HttpResultWithError>& p_Errors)
{
    bool changed = false;

    if (IsCorrespondingRow(p_Row))
        changed = Refresh(p_Row, p_Errors);

    return changed;
}

const SubItemKey& ISubItemModification::GetKey() const
{
    return m_Key;
}

O365Grid& ISubItemModification::GetGrid() const
{
    return m_Grid;
}

const boost::YOpt<SapioError>& ISubItemModification::GetError() const
{
    return m_Error;
}

void ISubItemModification::SetError(const boost::YOpt<SapioError>& p_Error)
{
    m_Error = p_Error;
}

GridBackendColumn* ISubItemModification::GetSubItemElderCol() const
{
    return m_ColSubItemElder;
}

GridBackendColumn* ISubItemModification::GetSubItemPkCol() const
{
    return m_ColSubItemPrimaryKey;
}

size_t ISubItemModification::GetMultiValueIndex(GridBackendRow* p_Row) const
{
	size_t index = 0;

	std::vector<PooledString>* subItemPrimaryKeys = p_Row->GetField(m_ColSubItemPrimaryKey).GetValuesMulti<PooledString>();
	if (nullptr != subItemPrimaryKeys)
	{
		const auto itemKey = getMostSpecificSubItemPk();
		for (auto& key : *subItemPrimaryKeys)
		{
			if (key == itemKey)
				return index;
			++index;
		}
	}

	return index;
}

void ISubItemModification::Revert(GridBackendRow* p_Row/* = nullptr*/)
{
    GridBackendRow* modRow = p_Row;

	ASSERT(nullptr == modRow || IsCorrespondingRow(modRow));
	if (nullptr == modRow || !IsCorrespondingRow(modRow))
	{
		for (auto row : GetGrid().GetBackendRows())
		{
			if (IsCorrespondingRow(row))
			{
				modRow = row;
				GetGrid().OnRowNothingToSave(modRow, false);
				break;
			}
		}
	}

    SubItemInfo subItemInfo;
    if (nullptr != modRow)
    {
		GridBackendRow* formatterRow = nullptr;
        formatterRow = modRow;
        GridBackendField& field = modRow->GetField(m_ColSubItemPrimaryKey);
        vector<PooledString>* primaryKeys = field.GetValuesMulti<PooledString>();
        size_t index = 0;
        if (nullptr != primaryKeys)
        {
            modRow->ClearStatus();
            subItemInfo.m_Collapsed = true;
            subItemInfo.m_Row = modRow;
            // Collapsed row
            bool found = false;
            while (index < primaryKeys->size() && !found)
            {
                if (primaryKeys->operator[](index) == getMostSpecificSubItemPk())
                    found = true;
                else
                    ++index;
            }
            subItemInfo.m_Index = index;
            ASSERT(found);
            RevertSpecific(subItemInfo);
        }
        else
        {
            // Expanded row
            subItemInfo.m_Collapsed = false;

            const MultivalueRowManager& mvMgr = GetGrid().GetMultivalueManager();
            vector<pair<GridBackendRow*, map<GridBackendColumn*, vector<GridBackendRow*>>>>	implosionSetup;// source row vs. col + exploded rows
            mvMgr.GetImplosionSetup({ modRow }, implosionSetup);
            ASSERT(implosionSetup.size() <= 1);
            if (implosionSetup.size() == 1)
            {
                // Save the first row of the explosion (the one with the GridBackendField used by the formatter
                formatterRow = implosionSetup.front().first;

                const auto sourceRow = implosionSetup.front().first;
                auto& rows = implosionSetup.front().second[m_ColSubItemPrimaryKey];
                if (sourceRow == modRow)
                    index = 0;
                else
                {
                    ptrdiff_t pos = std::find(rows.begin(), rows.end(), modRow) - rows.begin();
                    if (pos < static_cast<ptrdiff_t>(rows.size()))
                        index = pos + 1;// rows does not contain the source row
                    else
                        ASSERT(false); // Item index not found
                }
            }

            // Find the exact row corresponding to the subitem
            bool subItemFound = false;
            const auto& allRows = GetGrid().GetBackendRows();
            for (auto row : allRows)
            {
                if (IsCorrespondingRow(row))
                {
                    modRow = row;
                    subItemInfo.m_Row = modRow;
                    subItemFound = true;
                    modRow->ClearStatus();
                    break;
                }
            }
            ASSERT(subItemFound);
            RevertSpecific(subItemInfo);
        }

        // Remove the found index from the formatter
        ASSERT(m_FormatterId != std::numeric_limits<size_t>::max());
        if (m_FormatterId != std::numeric_limits<size_t>::max())
        {
            for (auto sisterCol : m_ColSubItemElder->GetMultiValueExplosionSisters())
                removeIndexFromFormatter(formatterRow, index, m_FormatterId, sisterCol);
        }
    }
}

void ISubItemModification::RevertSpecific(const SubItemInfo& p_Info)
{

}

bool ISubItemModification::IsCollapsedRow(GridBackendRow* p_Row) const
{
	return !(p_Row->IsExplosionFragment() && m_Grid.GetMultivalueManager().IsRowExploded(p_Row, m_ColSubItemPrimaryKey));
}

wstring ISubItemModification::getMostSpecificSubItemPk() const
{
    if (!GetKey().m_Id4.empty())
        return GetKey().m_Id4;
    if (!GetKey().m_Id3.empty())
        return GetKey().m_Id3;
    if (!GetKey().m_Id2.empty())
        return GetKey().m_Id2;
    if (!GetKey().m_Id1.empty())
        return GetKey().m_Id1;

    ASSERT(!GetKey().m_Id0.empty());
    return GetKey().m_Id0;
}

vector<wstring> ISubItemModification::getPk() const
{
	vector<wstring> pk;

	if (!GetKey().m_Id0.empty())
		pk.push_back(GetKey().m_Id0);
	if (!GetKey().m_Id1.empty())
		pk.push_back(GetKey().m_Id1);
	if (!GetKey().m_Id2.empty())
		pk.push_back(GetKey().m_Id2);
	if (!GetKey().m_Id3.empty())
		pk.push_back(GetKey().m_Id3);
	if (!GetKey().m_Id4.empty())
		pk.push_back(GetKey().m_Id4);

	ASSERT(!pk.empty());
	return pk;
}

void ISubItemModification::removeIndexFromFormatter(GridBackendRow* p_Row, size_t p_Index, size_t p_FormatterId, GridBackendColumn* p_Column)
{
    CompositeSubElementFormatter* compositeFormatter = dynamic_cast<CompositeSubElementFormatter*>(p_Column->GetCellFormatter().get());
    if (nullptr != compositeFormatter)
    {
        ASSERT(nullptr != p_Row);
        if (nullptr != p_Row)
        {
            // We assume that the row is the first row of the explosion set, so the one who contains the field used by the formatter
            compositeFormatter->RemoveElement(&p_Row->GetField(p_Column), p_Index, p_FormatterId);
        }
    }
}