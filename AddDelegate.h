#pragma once

#include "DelegateUser.h"

namespace Ex
{
class AddDelegate
{
public:
    AddDelegate(PooledString p_Mailbox, PooledString p_DeliverMeetingRequests);

    PooledString m_Mailbox;
    PooledString m_DeliverMeetingRequests;
    boost::YOpt<vector<Ex::DelegateUser>> m_DelegateUsers;
};
}
