#pragma once

#include <boost/bimap.hpp>

struct Languages
{
public:
	static const boost::bimap<wstring, wstring>& Get();	
};