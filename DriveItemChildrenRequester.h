#pragma once

#include "IRequester.h"
#include "BusinessDriveItem.h"
#include "IPageRequestLogger.h"

class DriveItemDeserializer;
class PaginatedRequestResults;

template <class T, class U>
class ValueListDeserializer;

class DriveItemChildrenRequester : public IRequester
{
public:
    DriveItemChildrenRequester(PooledString p_DriveId, const BusinessDriveItem& p_DriveItem, bool p_Recursive, const std::shared_ptr<IPageRequestLogger>& p_Logger);
    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    vector<BusinessDriveItem>& GetData();
    const PaginatedRequestResults& GetResult() const;

private:
    PooledString m_DriveId;
    BusinessDriveItem m_DriveItem;
    bool m_Recursive;

    std::shared_ptr<ValueListDeserializer<BusinessDriveItem, DriveItemDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;
    std::shared_ptr<PaginatedRequestResults> m_Result; // TODO: Recursive paginated results
};

