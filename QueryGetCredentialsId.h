#pragma once

#include "ISqlQuery.h"
#include "SessionIdentifier.h"

class O365SQLiteEngine;

class QueryGetCredentialsId : public ISqlQuery
{
public:
	QueryGetCredentialsId(O365SQLiteEngine& p_Engine, const SessionIdentifier& p_Identifier);
	QueryGetCredentialsId(O365SQLiteEngine& p_Engine, int64_t p_SessionId);

	void Run() override;

	int64_t GetCredentialsId() const;

private:
	QueryGetCredentialsId(O365SQLiteEngine& p_Engine);

	void QueryWithSessionId();
	void QueryWithSessionIdentifier();

	void HandleRow(sqlite3_stmt* p_Stmt);

	boost::YOpt<SessionIdentifier> m_Identifier;
	boost::YOpt<int64_t> m_SessionId;

	O365SQLiteEngine& m_Engine;

	int64_t m_CredId;
};

