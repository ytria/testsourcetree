#pragma once

#include "BusinessAssignedLicense.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class AssignedLicenseNonPropDeserializer : public JsonObjectDeserializer
                                         , public Encapsulate<BusinessAssignedLicense>
{
public:
    AssignedLicenseNonPropDeserializer() = default;
    void DeserializeObject(const web::json::object& p_Object) override;
};

