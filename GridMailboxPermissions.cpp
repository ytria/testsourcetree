#include "GridMailboxPermissions.h"

#include "AutomationWizardMailboxPermissions.h"
#include "BasicGridSetup.h"
#include "GridSnapshotConstants.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"
#include "FrameMailboxPermissions.h"
#include "DlgEditMailboxPermissions.h"
#include "CommaSeparatedString.h"
#include "DlgAddItemsToGroups.h"
#include "MailboxPermissionsAddModification.h"
#include "MailboxPermissionsAccessRights.h"
#include "MailboxPermissionsRemoveModification.h"
#include "CacheGridTools.h"

BEGIN_MESSAGE_MAP(GridMailboxPermissions, ModuleO365Grid<MailboxPermissions>)
	ON_COMMAND(ID_MAILBOXPERMISSIONSGRID_TOGGLESYSTEMROWS, OnToggleSystemRows)
	ON_UPDATE_COMMAND_UI(ID_MAILBOXPERMISSIONSGRID_TOGGLESYSTEMROWS, OnUpdateToggleSystemRows)
	ON_COMMAND(ID_MAILBOXPERMISSIONSGRID_EDIT, OnEditMailboxPermissions)
	ON_UPDATE_COMMAND_UI(ID_MAILBOXPERMISSIONSGRID_EDIT, OnUpdateEditMailboxPermissions)
	ON_COMMAND(ID_MAILBOXPERMISSIONSGRID_ADD, OnAddMailboxPermissions)
	ON_UPDATE_COMMAND_UI(ID_MAILBOXPERMISSIONSGRID_ADD, OnUpdateAddMailboxPermissions)
	ON_COMMAND(ID_MAILBOXPERMISSIONSGRID_REMOVE, OnRemoveMailboxPermissions)
	ON_UPDATE_COMMAND_UI(ID_MAILBOXPERMISSIONSGRID_REMOVE, OnUpdateRemoveMailboxPermissions)
END_MESSAGE_MAP()

GridMailboxPermissions::GridMailboxPermissions()
	: m_Frame(nullptr),
	m_HasLoadMoreAdditionalInfo(false),
	m_ColAccessRights(nullptr),
	m_ColDeny(nullptr),
	m_ColIdentity(nullptr),
	m_ColInheritanceType(nullptr),
	m_ColRealAce(nullptr),
	m_ColIsInherited(nullptr),
	m_ColIsValid(nullptr),
	m_ColSystem(nullptr),
	m_ShowSystemRows(false)
{
	//UseDefaultActions(/*flags*//*O365Grid::ACTION_CREATE | O365Grid::ACTION_DELETE | */O365Grid::ACTION_EDIT/*0*/);
	if (CRMpipe().IsFeatureSandboxed())
	{
		EnableGridModifications();
		GetModifications().SetModsWithRequestFeedbackProvider(std::make_unique<MailboxPermissionsModsWithRequestFeedbackProvider>(*this));
	}

	m_Reverter = std::make_shared<MailboxPermissionsModificationsReverter>(*this);
	SetModReverter(m_Reverter);
	m_Applyer = std::make_shared<MailboxPermissionsModificationsApplyer>(*this);
	SetModApplyer(m_Applyer);
	SetGridRefresher(std::make_shared<MailboxPermissionsGridRefresher>(*this));

	initWizard<AutomationWizardMailboxPermissions>(_YTEXT("Automation\\MailboxPermissions"));
}

void GridMailboxPermissions::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameMailboxPermissions, LicenseUtil::g_codeSapio365mailboxPermissions,
			{
				{ &m_Template.m_ColumnDisplayName, g_TitleOwnerDisplayName, g_FamilyOwner },
				{ &m_Template.m_ColumnUserPrincipalName, g_TitleOwnerUserName, g_FamilyOwner },
				{ &m_Template.m_ColumnID, g_TitleOwnerID, g_FamilyOwner }
			});
		setup.Setup(*this, true);
		m_ColumnLastRequestDateTime = setup.GetColumnLastRequestDateTime();
	}

	m_MetaColumns.push_back(m_Template.m_ColumnID);
	m_MetaColumns.push_back(m_Template.m_ColumnDisplayName);
	m_MetaColumns.push_back(m_Template.m_ColumnUserPrincipalName);

	setBasicGridSetupHierarchy({ { { m_Template.m_ColumnUserPrincipalName, 0 } }
								,{ rttr::type::get<MailboxPermissions>().get_id() }
								,{ rttr::type::get<MailboxPermissions>().get_id(), rttr::type::get<BusinessUser>().get_id() } });

	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		for (const auto& c : *metaDataColumnInfos)
		{
			ASSERT(nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()));
			if (nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()))
			{
				auto col = c.AddTo(*this, g_FamilyOwner);
				m_Template.GetColumnForProperty(c.GetPropertyName()) = col;
				m_MetaColumns.push_back(col);
				m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
			}
		}
	}

	addColumnGraphID();

	m_ColUser = AddColumn(_YUID("user"), _T("User"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAccessRights = AddColumn(_YUID("accessRights"), _T("Access Rights"), g_FamilyInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColDeny = AddColumnCheckBox(_YUID("deny"), _T("Deny"), g_FamilyInfo, g_ColumnSize12);
	m_ColIdentity = AddColumn(_YUID("identity"), _T("Identity"), g_FamilyInfo, g_ColumnSize12);
	m_ColInheritanceType = AddColumn(_YUID("inheritanceType"), _T("Inheritance Type"), g_FamilyInfo, g_ColumnSize12);
	m_ColRealAce = AddColumn(_YUID("realAce"), _T("Real Ace"), g_FamilyInfo, g_ColumnSize12);
	m_ColIsInherited = AddColumnCheckBox(_YUID("isInherited"), _T("Is Inherited"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColIsValid = AddColumnCheckBox(_YUID("isValid"), _T("Is Valid"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });

	m_ColSystem = AddColumnCheckBox(_YUID("system"), _T("Is System Default"), g_FamilyInfo, g_ColumnSize6, { g_ColumnsPresetDefault });
	
	AddColumnForRowPK(m_Template.m_ColumnID);
	AddColumnForRowPK(m_ColUser);
	AddColumnForRowPK(m_ColInheritanceType);

	m_Template.CustomizeGrid(*this);

	m_Frame = dynamic_cast<FrameMailboxPermissions*>(GetParentFrame());
	ASSERT(nullptr != m_Frame);
	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_Frame);
}

void GridMailboxPermissions::BuildTreeView(const O365DataMap<BusinessUser, vector<MailboxPermissions>>& p_Permissions, bool p_FullPurge)
{
	{
		GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID
			| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
			| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			| GridUpdaterOptions::REFRESH_PROCESS_DATA)
		);
		GridIgnoreModification ignoramus(*this);

		bool oneCanceled = false;
		bool oneOK = false;
		for (const auto& keyVal : p_Permissions)
		{
			const auto& user = keyVal.first;
			const auto& permissions = keyVal.second;
			auto userRow = m_Template.AddRow(*this, user, {}, updater, false, true);

			ASSERT(nullptr != userRow);
			if (nullptr != userRow)
			{
				userRow->AddField(user.GetID(), GetColId());
				updater.GetOptions().AddRowWithRefreshedValues(userRow);
				userRow->SetHierarchyParentToHide(true);

				if (user.HasFlag(BusinessObject::CANCELED))
				{
					oneCanceled = true;
				}
				else
				{
					oneOK = true;

					updater.GetOptions().AddPartialPurgeParentRow(userRow);
					if (permissions.size() == 1 && permissions[0].GetError())
					{
						updater.OnLoadingError(userRow, *permissions[0].GetError());
					}
					else
					{
						if (userRow->IsError())
							userRow->ClearStatus();

						for (const auto& permission : permissions)
						{
							auto permRow = FillRow(permission, userRow);
							ASSERT(nullptr != userRow);
							if (nullptr != userRow)
								updater.GetOptions().AddRowWithRefreshedValues(permRow);
						}
					}
				}
				AddRoleDelegationFlag(userRow, user);
			}
		}

		if (oneCanceled)
		{
			if (oneOK)
				updater.GetOptions().SetPurgeFlag(GridUpdaterOptions::PARTIALPURGE); // Only purge not-canceled top rows
			else
				updater.GetOptions().SetPurgeFlag(0); // Everything canceled, don't purge anything.
		}
	}

	bool hasChangesApplied = !m_AppliedEditions.empty() || !m_AppliedAdditions.empty() || !m_AppliedRemovals.empty();
	wstring editError = HandleUpdatedEditions();
	wstring additionErrors = HandleUpdatedAdditions();
	wstring removalErrors = HandleUpdatedRemovals();

	if (hasChangesApplied)
	{
		wstring displayedError = editError;
		if (displayedError.empty())
			displayedError = additionErrors;
		if (displayedError.empty())
			displayedError = removalErrors;

		if (!displayedError.empty())
			HandlePostUpdateError(false, true, displayedError);
		else
			m_Frame->ShowSuccessMessageBar(_T("Success"), _T("Changes successfully saved."), IDB_ICON_SAVED);

		// Restore pending changes
		for (auto& edit : m_MailboxPermEditMods)
			edit.second.Apply();
		for (auto& add : m_MailboxPermAddMods)
			add.second.Apply();
		for (auto& remove : m_MailboxPermRemoveMods)
			remove.second.Apply();

		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
	}
}

ModuleCriteria GridMailboxPermissions::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
	ModuleCriteria criteria;

	auto frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
	ASSERT(nullptr != frame);
	if (nullptr != frame)
	{
		criteria = frame->GetModuleCriteria();
		criteria.m_IDs.clear();
		for (auto row : p_Rows)
			criteria.m_IDs.insert(row->GetField(m_Template.m_ColumnID).GetValueStr());
	}

	return criteria;
}

wstring GridMailboxPermissions::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Owner"), m_Template.m_ColumnUserPrincipalName } }, m_ColUser);
}

RoleDelegationUtil::RBAC_Privilege GridMailboxPermissions::GetPrivilegeEdit() const
{
	return RoleDelegationUtil::RBAC_NONE;
}

void GridMailboxPermissions::SetAccessRights(GridBackendRow& p_Row, const vector<wstring>& p_AccessRights)
{
	p_Row.AddField(p_AccessRights, m_ColAccessRights);
}

vector<wstring> GridMailboxPermissions::GetAccessRights(const GridBackendRow& p_Row)
{
	vector<PooledString> accessRights;
	auto accessRightsPtr = p_Row.GetField(m_ColAccessRights).GetValuesMulti<PooledString>();
	if (nullptr != accessRightsPtr)
		accessRights = *accessRightsPtr;
	else
		accessRights = { p_Row.GetField(m_ColAccessRights).GetValueStr() };

	return Str::ToStrVector(accessRights);
}

GridBackendColumn* GridMailboxPermissions::GetColAccessRights()
{
	return m_ColAccessRights;
}

GridBackendColumn* GridMailboxPermissions::GetColOwnerId()
{
	return GetTemplateUsers().m_ColumnID;
}

GridBackendColumn* GridMailboxPermissions::GetColOwnerDisplayName()
{
	return GetTemplateUsers().m_ColumnDisplayName;
}

GridBackendColumn* GridMailboxPermissions::GetColPermissionsUser()
{
	return m_ColUser;
}

GridBackendColumn* GridMailboxPermissions::GetColInheritanceType()
{
	return m_ColInheritanceType;
}

const GridTemplateUsers& GridMailboxPermissions::GetTemplateUsers() const
{
	return m_Template;
}

bool GridMailboxPermissions::HasModificationUnder(GridBackendRow& p_Row)
{
	for (auto row : GetChildRows(&p_Row))
	{
		row_pk_t rowPk;
		GetRowPK(row, rowPk);

		if (m_MailboxPermAddMods.find(rowPk) != m_MailboxPermAddMods.end() ||
			m_MailboxPermEditMods.find(rowPk) != m_MailboxPermEditMods.end() ||
			m_MailboxPermRemoveMods.find(rowPk) != m_MailboxPermRemoveMods.end())
			return true;
	}

	return false;
}

wstring GridMailboxPermissions::HandleUpdatedEditions()
{
	wstring error;
	for (const auto& mod : m_AppliedEditions)
	{
		GridBackendRow* row = GetRowIndex().GetExistingRow(mod.GetRowKey());
		// No assert, row might not exist
		auto itt = m_MailboxPermEditMods.find(mod.GetRowKey());
		if (itt != m_MailboxPermEditMods.end())
		{
			m_MailboxPermEditMods.erase(itt);
			if (mod.HasResultError())
			{
				error = mod.GetResultError();
				if (nullptr != row)
					OnRowError(row, false, mod.GetResultError());
			}
			else
			{
				if (nullptr != row)
					OnRowSaved(row, false);
			}
		}
	}
	m_AppliedEditions.clear();
	
	return error;
}

wstring GridMailboxPermissions::HandleUpdatedAdditions()
{
	wstring error;
	for (const auto& mod : m_AppliedAdditions)
	{
		GridBackendRow* row = GetRowIndex().GetExistingRow(mod.GetRowKey());
		auto itt = m_MailboxPermAddMods.find(mod.GetRowKey());
		ASSERT(itt != m_MailboxPermAddMods.end());
		if (itt != m_MailboxPermAddMods.end())
		{
			m_MailboxPermAddMods.erase(itt);
			if (mod.HasResultError())
			{
				error = mod.GetResultError();
				if (nullptr != row)
					OnRowError(row, false, mod.GetResultError());
			}
			else
			{
				if (nullptr != row)
					OnRowSaved(row, false);
			}
		}
	}
	m_AppliedAdditions.clear();
	
	return error;
}

wstring GridMailboxPermissions::HandleUpdatedRemovals()
{
	wstring error;
	for (const auto& mod : m_AppliedRemovals)
	{
		GridBackendRow* row = GetRowIndex().GetExistingRow(mod.GetRowKey());
		auto itt = m_MailboxPermRemoveMods.find(mod.GetRowKey());
		ASSERT(itt != m_MailboxPermRemoveMods.end());
		if (itt != m_MailboxPermRemoveMods.end())
		{
			m_MailboxPermRemoveMods.erase(itt);
			if (mod.HasResultError())
			{
				error = mod.GetResultError();
				if (nullptr != row)
					OnRowError(row, false, mod.GetResultError());
			}
		}
	}
	m_AppliedRemovals.clear();

	return error;
}

void GridMailboxPermissions::SelectModificationsInSelectionParents()
{
	set<GridBackendRow*> selectedRowsParents;
	for (auto row : GetSelectedRows())
	{
		auto topRow = row->GetTopAncestorOrThis();
		if (selectedRowsParents.insert(topRow).second)
		{
			for (auto childRow : GetChildRows(topRow))
			{
				row_pk_t rowPk;
				GetRowPK(childRow, rowPk);

				if (m_MailboxPermAddMods.find(rowPk) != m_MailboxPermAddMods.end() ||
					m_MailboxPermEditMods.find(rowPk) != m_MailboxPermEditMods.end() ||
					m_MailboxPermRemoveMods.find(rowPk) != m_MailboxPermRemoveMods.end())
					childRow->SetSelected(true);
			}
		}
	}
}

vector<GridBackendField> GridMailboxPermissions::CreatePk(const wstring& p_UserId, const wstring& p_InheritanceType, const wstring& p_OwnerId)
{
	return vector<GridBackendField> {
		GridBackendField(p_OwnerId, GetTemplateUsers().m_ColumnID),
		GridBackendField(p_UserId, GetColPermissionsUser()),
		GridBackendField(p_InheritanceType, GetColInheritanceType()),
	};
}

bool GridMailboxPermissions::AreSimilarRows(const row_pk_t& p_Pk1, const row_pk_t& p_Pk2) const
{
	return std::equal(p_Pk1.begin(), p_Pk1.end() - 1, p_Pk2.begin());
}

O365Grid::SnapshotCaps GridMailboxPermissions::GetSnapshotCapabilities() const
{
	return { true };
}

MailboxPermissions GridMailboxPermissions::getBusinessObject(GridBackendRow*) const
{
	return MailboxPermissions();
}

void GridMailboxPermissions::UpdateBusinessObjects(const vector<MailboxPermissions>& p_Permissions, bool p_SetModifiedStatus)
{

}

void GridMailboxPermissions::RemoveBusinessObjects(const vector<MailboxPermissions>& p_Permissions)
{

}

bool GridMailboxPermissions::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	return m_Template.GetSnapshotValue(p_Value, p_Field, p_Column)
		|| ModuleO365Grid<MailboxPermissions>::GetSnapshotValue(p_Value, p_Field, p_Column);

}

bool GridMailboxPermissions::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (&p_Column == m_ColSystem)
		p_Row.SetTechHidden(p_Value == GridSnapshot::Constants::g_True && !m_ShowSystemRows);

	return m_Template.LoadSnapshotValue(p_Value, p_Row, p_Column)
		|| ModuleO365Grid<MailboxPermissions>::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

bool GridMailboxPermissions::HasModificationsPending(bool inSelectionOnly)
{
	if (inSelectionOnly)
		return hasSelectedMod();

	return !m_MailboxPermEditMods.empty() || !m_MailboxPermAddMods.empty() || !m_MailboxPermRemoveMods.empty();
}

bool GridMailboxPermissions::HasModificationsPendingInParentOfSelected()
{
	if (GetBackend().IsHierarchyReady())
	{
		set<GridBackendRow*> selectedRowsParents;
		for (auto row : GetSelectedRows())
		{
			auto topRow = row->GetTopAncestorOrThis();
			if (selectedRowsParents.insert(topRow).second && HasModificationUnder(*topRow))
				return true;
		}
	}
	else
	{
		return HasModificationsPending(true);
	}

	return false;
}

void GridMailboxPermissions::SetUpdatedEditions(vector<MailboxPermissionsEditModification>&& p_Editions)
{
	m_AppliedEditions = std::move(p_Editions);
}

void GridMailboxPermissions::SetUpdatedAdditions(vector<MailboxPermissionsAddModification>&& p_Additions)
{
	m_AppliedAdditions = std::move(p_Additions);
}

void GridMailboxPermissions::SetUpdatedRemovals(vector<MailboxPermissionsRemoveModification>&& p_Removals)
{
	m_AppliedRemovals = std::move(p_Removals);
}

void GridMailboxPermissions::createGridEditModifications(const vector<vector<GridBackendField>>& p_Pks, const map<wstring, bool>& p_Changes)
{
	for (const auto& rowPk : p_Pks)
	{
		auto row = GetRowIndex().GetExistingRow(rowPk);
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			vector<PooledString>* accessRights = row->GetField(m_ColAccessRights).GetValuesMulti<PooledString>();
			vector<wstring> toAdd, toRemove;

			for (const auto& kv : p_Changes)
			{
				const wstring& rightName = kv.first;
				bool add = kv.second;

				bool found = false;
				if (nullptr != accessRights)
					found = std::find(accessRights->begin(), accessRights->end(), rightName) != accessRights->end();
				else
					found = rightName == row->GetField(m_ColAccessRights).GetValueStr();

				if (add && !found)
					toAdd.push_back(rightName);
				else if (!add && found)
					toRemove.push_back(rightName);
			}

			auto ittAdd = m_MailboxPermAddMods.find(rowPk);
			if (ittAdd != m_MailboxPermAddMods.end())
			{
				auto& addMod = ittAdd->second;

				addMod.SetPermissions(
					MailboxPermissionsEditModification::GetCombinedAccessRights(
						nullptr != accessRights ? Str::ToStrVector(*accessRights) : vector<wstring>{ row->GetField(m_ColAccessRights).GetValueStr() },
						toAdd,
						toRemove)
				);
			}
			else if (!toAdd.empty() || !toRemove.empty())
			{
				MailboxPermissionsEditModification mod(*this, rowPk, toAdd, toRemove);
				mod.Apply();
				AddModification(mod);
			}
		}
	}
	UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
}

bool GridMailboxPermissions::confirmRemove()
{
	YCodeJockMessageBox confirmation(
		GetParentFrame(),
		DlgMessageBox::eIcon_ExclamationWarning,
		_T("Remove"),
		_T("One or more selected rows contain unsaved changes. Do you want to delete them instead?"),
		_T("Pending changes will be lost."),
		{ { IDOK, _T("Yes") },{ IDCANCEL, _T("No") } }
	);
	return IDOK == confirmation.DoModal();
}

void GridMailboxPermissions::AddModification(MailboxPermissionsEditModification p_Mod)
{
	const row_pk_t& rowPk = p_Mod.GetRowKey();
	const auto& existingEditItt = m_MailboxPermEditMods.find(rowPk);
	if (existingEditItt == m_MailboxPermEditMods.end())
	{
		auto ittSimilar = std::find_if(m_MailboxPermEditMods.begin(), m_MailboxPermEditMods.end(), [=](const std::pair<row_pk_t, MailboxPermissionsEditModification>& p_TempMod) {
			return AreSimilarRows(p_TempMod.first, rowPk);
		});
		if (ittSimilar == m_MailboxPermEditMods.end())
			m_MailboxPermEditMods.insert({ rowPk, p_Mod });
		else
		{
			ittSimilar->second.Revert();
			p_Mod.Apply();
			ittSimilar->second = p_Mod;
		}
	}
	else
	{
		p_Mod.Merge(existingEditItt->second);
		existingEditItt->second = p_Mod;
	}
}

void GridMailboxPermissions::AddModification(const MailboxPermissionsAddModification& p_Mod)
{
	const row_pk_t& rowPk = p_Mod.GetRowKey();
	const auto& existingAddItt = m_MailboxPermAddMods.find(rowPk);
	if (existingAddItt == m_MailboxPermAddMods.end() && m_MailboxPermEditMods.find(rowPk) == m_MailboxPermEditMods.end())
		m_MailboxPermAddMods.insert({ rowPk, p_Mod });
}

GridBackendRow* GridMailboxPermissions::FillRow(const MailboxPermissions& p_Permissions, GridBackendRow* p_ParentUserRow)
{
	const auto& pk = CreatePk(p_Permissions.m_User, p_Permissions.m_InheritanceType, p_ParentUserRow->GetField(m_Template.m_ColumnID).GetValueStr());
	GridBackendRow* row = m_RowIndex.GetRow(pk, true, true);

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		for (auto c : m_MetaColumns)
		{
			if (p_ParentUserRow->HasField(c))
				row->AddField(p_ParentUserRow->GetField(c));
			else
				row->RemoveField(c);
		}
		
		SetRowObjectType(row, MailboxPermissions(), p_Permissions.HasFlag(BusinessObject::CANCELED));

		auto accessRights = Str::explodeIntoVector(p_Permissions.m_AccessRights, wstring(_YTEXT(",")), false, true);
		row->AddField(accessRights, m_ColAccessRights);
		row->AddField(p_Permissions.m_Deny, m_ColDeny);
		row->AddField(p_Permissions.m_Identity, m_ColIdentity);
		row->AddField(p_Permissions.m_RealAce, m_ColRealAce);
		row->AddField(p_Permissions.m_IsInherited, m_ColIsInherited);
		row->AddField(p_Permissions.m_IsValid, m_ColIsValid);

		if (p_Permissions.m_IsInherited || p_Permissions.m_User == _YTEXT("NT AUTHORITY\\SELF"))
		{
			row->AddField(true, m_ColSystem);
			row->SetTechHidden(!m_ShowSystemRows);
		}
		else
		{
			row->RemoveField(m_ColSystem);
		}

		row->SetParentRow(p_ParentUserRow);
		if (p_ParentUserRow->HasField(m_ColumnLastRequestDateTime))
			row->AddField(p_ParentUserRow->GetField(m_ColumnLastRequestDateTime));
		else
			row->RemoveField(m_ColumnLastRequestDateTime);

		row->ClearStatus();
	}

	return row;
}

void GridMailboxPermissions::UpdateRow(const MailboxPermissions& p_Permissions, GridBackendRow* p_Row)
{

}

void GridMailboxPermissions::InitializeCommands()
{
	ModuleO365Grid<MailboxPermissions>::InitializeCommands();
	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_MAILBOXPERMISSIONSGRID_TOGGLESYSTEMROWS;
		_cmd.m_sMenuText = _T("Show System Defaults");
		_cmd.m_sToolbarText = _T("Show System Defaults");
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MAILBOXPERMISSIONS_TOGGLE_SYSTEMROWS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MAILBOXPERMISSIONSGRID_TOGGLESYSTEMROWS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Show System Defaults"),
			_T("This will toggle the display of all system defaults in the current grid.\r\nThese rows are hidden by default.\r\nThe 'Is System Default' column can be managed in the Grid Manager."),
			_cmd.m_sAccelText);
	}

	if (CRMpipe().IsFeatureSandboxed())
	{
		CExtCmdItem cmdEdit;
		cmdEdit.m_nCmdID = ID_MAILBOXPERMISSIONSGRID_EDIT;
		cmdEdit.m_sMenuText = _T("Edit Permissions");
		cmdEdit.m_sToolbarText = _T("Edit Permissions");
		cmdEdit.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, cmdEdit, true);

		HICON hIconEdit = MFCUtil::getHICONFromImageResourceID(IDB_MAILBOXPERMISSIONSGRID_EDIT_16X16);
		ASSERT(hIconEdit);
		g_CmdManager->CmdSetIcon(profileName, ID_MAILBOXPERMISSIONSGRID_EDIT, hIconEdit, false);

		setRibbonTooltip(cmdEdit.m_nCmdID,
			_T("Edit Permissions"),
			_T("Edit Permissions"),
			cmdEdit.m_sAccelText);

		CExtCmdItem cmdAdd;
		cmdAdd.m_nCmdID = ID_MAILBOXPERMISSIONSGRID_ADD;
		cmdAdd.m_sMenuText = _T("Add Permissions");
		cmdAdd.m_sToolbarText = _T("Add Permissions");
		cmdAdd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, cmdAdd, true);

		HICON hIconAdd = MFCUtil::getHICONFromImageResourceID(IDB_MAILBOXPERMISSIONSGRID_ADD_16X16);
		ASSERT(hIconAdd);
		g_CmdManager->CmdSetIcon(profileName, ID_MAILBOXPERMISSIONSGRID_ADD, hIconAdd, false);

		setRibbonTooltip(cmdAdd.m_nCmdID,
			_T("Add Permissions"),
			_T("Add Permissions"),
			cmdAdd.m_sAccelText);

		CExtCmdItem cmdRemove;
		cmdRemove.m_nCmdID = ID_MAILBOXPERMISSIONSGRID_REMOVE;
		cmdRemove.m_sMenuText = _T("Remove Permissions");
		cmdRemove.m_sToolbarText = _T("Remove Permissions");
		cmdRemove.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, cmdRemove, true);

		HICON hIconRemove = MFCUtil::getHICONFromImageResourceID(IDB_MAILBOXPERMISSIONSGRID_REMOVE_16X16);
		ASSERT(hIconRemove);
		g_CmdManager->CmdSetIcon(profileName, ID_MAILBOXPERMISSIONSGRID_REMOVE, hIconRemove, false);

		setRibbonTooltip(cmdRemove.m_nCmdID,
			_T("Remove Permissions"),
			_T("Remove Permissions"),
			cmdRemove.m_sAccelText);
	}
}

void GridMailboxPermissions::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ModuleO365Grid<MailboxPermissions>::OnCustomPopupMenu(pPopup, nStart);

	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_MAILBOXPERMISSIONSGRID_TOGGLESYSTEMROWS);
		pPopup->ItemInsert(ID_MAILBOXPERMISSIONSGRID_EDIT);
		pPopup->ItemInsert(ID_MAILBOXPERMISSIONSGRID_ADD);
		pPopup->ItemInsert(ID_MAILBOXPERMISSIONSGRID_REMOVE);
	}
}

bool GridMailboxPermissions::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return false;
}

void GridMailboxPermissions::ShowSystemRows(bool p_Show)
{
	m_ShowSystemRows = p_Show;
	const vector< GridBackendRow* >& Rows = GetBackend().GetBackendRows();
	for (auto& row : Rows)
	{
		ASSERT(nullptr != row);
		if (nullptr != row && !row->IsGroupRow())
		{
			const GridBackendField& field = row->GetField(m_ColSystem);
			if (field.HasValue() && field.GetValueBool())
				row->SetTechHidden(!m_ShowSystemRows);
		}
	}

	UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
}

void GridMailboxPermissions::OnEditMailboxPermissions()
{
	TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [=] (const bool hasRowsToReExplode) {
		DlgEditMailboxPermissions editor(*this, *m_Frame);
		if (IDOK == editor.DoModal())
			createGridEditModifications(editor.GetRowPks(), editor.GetChanges());
	})();
}

void GridMailboxPermissions::OnUpdateEditMailboxPermissions(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && hasSelectedPermission());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridMailboxPermissions::OnAddMailboxPermissions()
{
	if (IsFrameConnected())
	{
		DlgAddItemsToGroups dlg(
			GetSession(),
			{},
			DlgAddItemsToGroups::SELECT_USERS_FOR_SELECTION,
			{}, {}, {},
			_T("Select users to add"),
			_T(""), RoleDelegationUtil::RBAC_Privilege::RBAC_NONE, this);
		if (dlg.DoModal() == IDOK)
		{
			const auto& usersToAdd = dlg.GetSelectedItems();
			for (const auto& userToAdd : usersToAdd)
			{
				for (auto row : GetSelectedRows())
				{
					if (nullptr != row && !row->IsGroupRow() && !row->IsError())
					{
						row_pk_t pk;
						if (GridUtil::isBusinessType<MailboxPermissions>(row))
						{
							GridBackendRow* parentRow = row->GetParentRow();
							if (nullptr != parentRow)
							{
								auto rights = row->GetField(m_ColAccessRights).GetValuesMulti<PooledString>();
								vector<wstring> rightsVec;
								if (nullptr != rights)
									rightsVec = Str::ToStrVector(*rights);
								else
									rightsVec = { row->GetField(m_ColAccessRights).GetValueStr() };

								pk = MailboxPermissionsAddModification::CreateRow(*this,
									*parentRow,
									userToAdd.GetPrincipalName(),
									_YTEXT("None"),
									rightsVec
								);
							}
						}
						else if (GridUtil::isBusinessType<BusinessUser>(row))
						{
							pk = MailboxPermissionsAddModification::CreateRow(*this,
								*row,
								userToAdd.GetPrincipalName(),
								_YTEXT("None"),
								vector<wstring>{ MailboxPermissionsAccessRights::g_FullAccess }
							);
						}
						if (!pk.empty())
						{
							MailboxPermissionsAddModification mod(*this, pk, userToAdd.GetID());
							mod.Apply();
							AddModification(mod);
						}
					}
				}
			}
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
		}
	}
}

void GridMailboxPermissions::OnUpdateAddMailboxPermissions(CCmdUI* pCmdUI)
{
	const auto& selectedRows = GetSelectedRows();
	bool hasSelectedRowsWithoutErrors = std::any_of(selectedRows.begin(), selectedRows.end(), [](GridBackendRow* p_Row) {
		return !p_Row->IsError();
	});

	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && hasSelectedRowsWithoutErrors);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridMailboxPermissions::OnRemoveMailboxPermissions()
{
	bool hasConfirmed = false;
	bool confirmedRemove = false;

	using itt_type = map<row_pk_t, MailboxPermissionsAddModification>::iterator;
	vector<itt_type> revertList;

	const auto& selectedRows = GetSelectedRows();
	for (auto row : selectedRows)
	{
		if (nullptr != row && !row->IsGroupRow() && GetRowObjectType(row) == rttr::type::get<MailboxPermissions>().get_id())
		{
			GridBackendRow* parentRow = row->GetParentRow();
			ASSERT(nullptr != parentRow);
			if (nullptr == parentRow)
				break;

			const auto& rowPk = CreatePk(row->GetField(m_ColUser).GetValueStr(), row->GetField(m_ColInheritanceType).GetValueStr(), parentRow->GetField(m_Template.m_ColumnID).GetValueStr());
			auto editModItt = m_MailboxPermEditMods.find(rowPk);
			if (editModItt != m_MailboxPermEditMods.end())
			{
				if (!hasConfirmed)
				{
					confirmedRemove = confirmRemove();
					hasConfirmed = true;
				}
				if (confirmedRemove)
				{
					editModItt->second.Revert();
					m_MailboxPermEditMods.erase(editModItt);

					MailboxPermissionsRemoveModification mod(*this, rowPk);
					mod.Apply();
					m_MailboxPermRemoveMods.insert({ rowPk, mod });
				}
			}
			else
			{
				MailboxPermissionsRemoveModification mod(*this, rowPk);
				mod.Apply();

				auto addModItt = m_MailboxPermAddMods.find(rowPk);
				if (addModItt != m_MailboxPermAddMods.end())
					revertList.push_back(addModItt);
				else
					m_MailboxPermRemoveMods.insert({ rowPk, mod });
			}
		}
	}

	for (const auto& item : revertList)
	{
		item->second.Revert();
		m_MailboxPermAddMods.erase(item);
	}

	UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
}

void GridMailboxPermissions::OnUpdateRemoveMailboxPermissions(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && hasSelectedPermission());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridMailboxPermissions::OnToggleSystemRows()
{
	ShowSystemRows(!m_ShowSystemRows);
}

void GridMailboxPermissions::OnUpdateToggleSystemRows(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning());
	pCmdUI->SetCheck(m_ShowSystemRows);
	setTextFromProfUISCommand(*pCmdUI);
}

bool GridMailboxPermissions::hasSelectedMod()
{
	bool hasMods = std::any_of(m_MailboxPermEditMods.begin(), m_MailboxPermEditMods.end(), [this](const std::pair<row_pk_t, MailboxPermissionsEditModification>& kv) {
		const auto& pk = kv.first;
		const auto& mod = kv.second;

		bool eligible = false;
		GridBackendRow* row = GetRowIndex().GetExistingRow(pk);
		if (nullptr != row)
			eligible = row->IsSelected();
		return eligible;
	});
	if (!hasMods)
	{
		hasMods = std::any_of(m_MailboxPermAddMods.begin(), m_MailboxPermAddMods.end(), [this](const std::pair<row_pk_t, MailboxPermissionsAddModification>& kv) {
			const auto& pk = kv.first;
			const auto& mod = kv.second;

			bool eligible = false;
			GridBackendRow* row = GetRowIndex().GetExistingRow(pk);
			if (nullptr != row)
				eligible = row->IsSelected();
			return eligible;
		});
	}
	if (!hasMods)
	{
		hasMods = std::any_of(m_MailboxPermRemoveMods.begin(), m_MailboxPermRemoveMods.end(), [this](const std::pair<row_pk_t, MailboxPermissionsRemoveModification>& kv) {
			const auto& pk = kv.first;
			const auto& mod = kv.second;

			bool eligible = false;
			GridBackendRow* row = GetRowIndex().GetExistingRow(pk);
			if (nullptr != row)
				eligible = row->IsSelected();
			return eligible;
		});
	}

	return hasMods;
}

bool GridMailboxPermissions::hasSelectedPermission()
{
	const auto& selectedRows = GetSelectedRows();
	return std::any_of(selectedRows.begin(), selectedRows.end(), [this](GridBackendRow* p_Row) {
		return GetRowObjectType(p_Row) == rttr::type::get<MailboxPermissions>().get_id();
	});
}

GridMailboxPermissions::MailboxPermissionsSaveAllUpdater::MailboxPermissionsSaveAllUpdater(GridFrameBase& p_Frame, GridMailboxPermissions& p_Grid) : m_Frame(p_Frame),
m_Grid(p_Grid)
{
}

void GridMailboxPermissions::MailboxPermissionsSaveAllUpdater::Update(CCmdUI* p_CmdUi)
{
	bool hasMod = !m_Grid.m_MailboxPermAddMods.empty() || !m_Grid.m_MailboxPermEditMods.empty() || !m_Grid.m_MailboxPermRemoveMods.empty();
	p_CmdUi->Enable(m_Frame.IsConnected() && hasMod && m_Frame.HasNoTaskRunning());
}

GridMailboxPermissions::MailboxPermissionsSaveSelectedUpdater::MailboxPermissionsSaveSelectedUpdater(GridFrameBase& p_Frame, GridMailboxPermissions& p_Grid)
	:m_Frame(p_Frame),
	m_Grid(p_Grid)
{
}

void GridMailboxPermissions::MailboxPermissionsSaveSelectedUpdater::Update(CCmdUI* p_CmdUi)
{
	p_CmdUi->Enable(m_Frame.IsConnected() && m_Grid.hasSelectedMod() && m_Frame.HasNoTaskRunning());
}

GridMailboxPermissions::MailboxPermissionsRevertAllUpdater::MailboxPermissionsRevertAllUpdater(GridFrameBase& p_Frame, GridMailboxPermissions& p_Grid)
	:m_Frame(p_Frame),
	m_Grid(p_Grid)
{
}

void GridMailboxPermissions::MailboxPermissionsRevertAllUpdater::Update(CCmdUI* p_CmdUi)
{
	bool hasMod = !m_Grid.m_MailboxPermAddMods.empty() || !m_Grid.m_MailboxPermEditMods.empty() || !m_Grid.m_MailboxPermRemoveMods.empty();
	p_CmdUi->Enable(hasMod && m_Frame.HasNoTaskRunning());
}

GridMailboxPermissions::MailboxPermissionsRevertSelectedUpdater::MailboxPermissionsRevertSelectedUpdater(GridFrameBase& p_Frame, GridMailboxPermissions& p_Grid)
	:m_Frame(p_Frame),
	m_Grid(p_Grid)
{
}

void GridMailboxPermissions::MailboxPermissionsRevertSelectedUpdater::Update(CCmdUI* p_CmdUi)
{
	p_CmdUi->Enable(m_Grid.hasSelectedMod() && m_Frame.HasNoTaskRunning());
}

GridMailboxPermissions::MailboxPermissionsGridRefresher::MailboxPermissionsGridRefresher(GridMailboxPermissions& p_Grid)
	:m_Grid(p_Grid)
{
}

void GridMailboxPermissions::MailboxPermissionsGridRefresher::RefreshAll()
{
	ASSERT(nullptr != *m_Grid.m_Frame);
	if (nullptr != *m_Grid.m_Frame)
	{
		ModuleUtil::eConfirmRefreshPendingChanges confirmation = ModuleUtil::eConfirmRefreshPendingChanges::Refresh;
		if (m_Grid.HasModificationsPending(false))
			confirmation = ModuleUtil::ConfirmRefreshPendingChanges(*m_Grid.m_Frame);

		switch (confirmation)
		{
		case ModuleUtil::eConfirmRefreshPendingChanges::Cancel:
			break;
		case ModuleUtil::eConfirmRefreshPendingChanges::SaveAndRefresh:
			m_Grid.m_Applyer->SetConfirm(false);
			m_Grid.m_Applyer->SetRefreshAfterApply(true);
			m_Grid.GetModApplyer()->ApplyAll();
			m_Grid.m_Applyer->SetConfirm(true);
			break;
		case ModuleUtil::eConfirmRefreshPendingChanges::Refresh:
			m_Grid.m_Reverter->SetConfirm(false);
			m_Grid.GetModReverter()->RevertAll();
			m_Grid.m_Reverter->SetConfirm(true);
			m_Grid.m_Frame->RefreshSpecific(m_Grid.m_Frame->GetAutomationAction());
			break;
		default:
			break;
		}
	}
}

void GridMailboxPermissions::MailboxPermissionsGridRefresher::RefreshSelection()
{
	ASSERT(nullptr != *m_Grid.m_Frame);
	if (nullptr != *m_Grid.m_Frame)
	{
		ModuleUtil::eConfirmRefreshPendingChanges confirmation = ModuleUtil::eConfirmRefreshPendingChanges::Refresh;
		if (m_Grid.HasModificationsPendingInParentOfSelected())
			confirmation = ModuleUtil::ConfirmRefreshPendingChanges(*m_Grid.m_Frame);

		switch (confirmation)
		{
		case ModuleUtil::eConfirmRefreshPendingChanges::Cancel:
			break;
		case ModuleUtil::eConfirmRefreshPendingChanges::SaveAndRefresh:
			m_Grid.m_Frame->GetGrid().StoreSelection();
			m_Grid.m_Applyer->SetConfirm(false);
			m_Grid.m_Applyer->SetRefreshAfterApply(true);
			m_Grid.SelectModificationsInSelectionParents();
			m_Grid.GetModApplyer()->ApplySelected();
			m_Grid.m_Applyer->SetConfirm(true);
			m_Grid.m_Frame->GetGrid().RestoreSelection();
			break;
		case ModuleUtil::eConfirmRefreshPendingChanges::Refresh:
			m_Grid.m_Frame->GetGrid().StoreSelection();
			m_Grid.m_Reverter->SetConfirm(false);
			m_Grid.SelectModificationsInSelectionParents();
			m_Grid.GetModReverter()->RevertSelected();
			m_Grid.m_Reverter->SetConfirm(true);
			m_Grid.m_Frame->GetGrid().RestoreSelection();
			m_Grid.m_Frame->RefreshIdsSpecific(m_Grid.m_Frame->GetRefreshSelectedData(), m_Grid.m_Frame->GetAutomationAction());
			break;
		default:
			break;
		}
	}
}

GridMailboxPermissions::MailboxPermissionsModsWithRequestFeedbackProvider::MailboxPermissionsModsWithRequestFeedbackProvider(GridMailboxPermissions& p_Grid)
	: m_Grid(p_Grid)
{
}

vector<const Modification*> GridMailboxPermissions::MailboxPermissionsModsWithRequestFeedbackProvider::Provide()
{
	vector<const Modification*> mods;

	for (const auto& mod : m_Grid.m_AppliedEditions)
		mods.push_back(&mod);

	for (const auto& mod : m_Grid.m_AppliedAdditions)
		mods.push_back(&mod);

	for (const auto& mod : m_Grid.m_AppliedRemovals)
		mods.push_back(&mod);

	return mods;
}
