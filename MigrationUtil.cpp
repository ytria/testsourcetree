#include "MigrationUtil.h"
#include "O365SQLiteEngine.h"

int32_t MigrationUtil::GetCurrentMigrationVersion()
{
	return CurrentMigrationVersion;
}

wstring MigrationUtil::GetCurrentMigrationFilePath()
{
	return O365SQLiteEngine::getLocalAppDataDBFileFolder() + GetCurrentMigrationFileName();
}

wstring MigrationUtil::GetMigrationFilePath(int p_Version)
{
	return O365SQLiteEngine::getLocalAppDataDBFileFolder() + GetMigrationFileName(p_Version);
}

wstring MigrationUtil::GetCurrentMigrationFileName()
{
	return GetMigrationFileName(GetCurrentMigrationVersion());
}

wstring MigrationUtil::GetMigrationFileName(int p_Version)
{
	return _YTEXTFORMAT(L"_srvd%s_.ytr", std::to_wstring(p_Version).c_str());
}
