#pragma once

#include "BusinessAssignedLicense.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class AssignedLicenseDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessAssignedLicense>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

