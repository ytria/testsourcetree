#include "TeamFunSettingsSerializer.h"

#include "JsonSerializeUtil.h"

TeamFunSettingsSerializer::TeamFunSettingsSerializer(const TeamFunSettings& p_Settings)
    : m_Settings(p_Settings)
{
}

void TeamFunSettingsSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeBool(_YTEXT("allowGiphy"), m_Settings.GetAllowGiphy(), obj);
    JsonSerializeUtil::SerializeString(_YTEXT("giphyContentRating"), m_Settings.GetGiphyContentRating(), obj);
    JsonSerializeUtil::SerializeBool(_YTEXT("allowStickersAndMemes"), m_Settings.GetAllowStickersAndMemes(), obj);
    JsonSerializeUtil::SerializeBool(_YTEXT("allowCustomMemes"), m_Settings.GetAllowCustomMemes(), obj);
}
