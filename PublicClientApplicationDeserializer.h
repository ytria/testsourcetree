#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "PublicClientApplication.h"

class PublicClientApplicationDeserializer : public JsonObjectDeserializer, public Encapsulate<PublicClientApplication>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

