#pragma once

#include "ModuleBase.h"
#include "ModuleCriteria.h"

class AutomationAction;
class FrameConversations;
struct RefreshSpecificData;

class ModuleConversation : public ModuleBase
{
public:
    using::ModuleBase::ModuleBase;

private:
    virtual void executeImpl(const Command& p_Command) override;
	void		showConversations(const Command& p_Command);
    void        refresh(const Command& p_Command);
    void        doRefresh(FrameConversations* p_pFrame, const ModuleCriteria& p_ModuleCriteria, AutomationAction * p_Action, YtriaTaskData p_TaskData, bool isUpdate, const RefreshSpecificData& p_RefreshData);
};

