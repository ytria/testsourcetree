#include "MailFoldersTopLevelRequester.h"

#include "MultiObjectsPageRequestLogger.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

MailFoldersTopLevelRequester::MailFoldersTopLevelRequester(const wstring& p_UserId, const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_UserId(p_UserId)
	, m_Logger(p_Logger)
{
}

TaskWrapper<void> MailFoldersTopLevelRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<DeserializerType>();

	web::uri_builder uri(_YTEXT("users"));
	uri.append_path(m_UserId);
	uri.append_path(_YTEXT("mailFolders"));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Deserializer, 100, m_Logger, httpLogger, p_TaskData.GetOriginator()).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

vector<BusinessMailFolder>& MailFoldersTopLevelRequester::GetData()
{
	return m_Deserializer->GetData();
}
