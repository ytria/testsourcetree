#pragma once

#include "GridBackendField.h"
#include "HttpResultWithError.h"
#include "RowModification.h"
#include "SingleRequestResult.h"

class GridLicenses;

class UserLicenseModification : public RowModification
{
public:
    UserLicenseModification(GridLicenses& p_Grid, const row_pk_t& p_UserPk);
    ~UserLicenseModification();

    virtual void RefreshState() override;
    virtual void RefreshStatesPostProcess(GridUpdater& p_GridUpdater) override;

    void SetResult(const SingleRequestResult& p_Result);
	void SetUsageLocationResult(const HttpResultWithError& p_Result);
    void Merge(const UserLicenseModification& p_Mod);

    void AddSkuToEnable(PooledString p_SkuId, const row_pk_t& p_SkuRowPk, bool p_MarkInGrid);
    void AddSkuToDisable(PooledString p_SkuId, const row_pk_t& p_SkuRowPk, bool  p_MarkInGrid);
    void AddPlanToDisable(PooledString p_SkuId, PooledString p_PlanId, const row_pk_t& p_PlanRowPk, bool p_MarkInGrid);
    void AddPlanToEnable(PooledString p_SkuId, PooledString p_PlanId, const row_pk_t& p_PlanRowPk, bool p_MarkInGrid);

    PooledString GetUserId() const;
    const map<PooledString, std::set<PooledString>>& GetModifiedSkus() const;
    const std::set<PooledString>& GetDisabledSkus() const;

	void AddUsageLocationChange(const boost::YOpt<PooledString>& p_Old, const boost::YOpt<PooledString>& p_New);
	bool HasUsageLocationChange() const;
	const boost::YOpt<PooledString>& GetNewUsageLocation();
	const boost::YOpt<PooledString>& GetOldUsageLocation();

    void ShowUsageLocationAppliedLocally();
    const map<PooledString, int32_t>& GetSkusDelta() const;

    static void ComputeConsumedSkusWithMods(GridLicenses& p_GridLicenses, const map<PooledString, int32_t>& p_SkuDeltas);

	virtual vector<ModificationLog> GetModificationLogs() const override;

private:
    GridLicenses* m_Grid;
    PooledString m_UserId;
    row_pk_t m_UserPk;
	boost::YOpt<SingleRequestResult> m_Result;

    // Can contain either a pk to a sku row or a plan row
    struct LicenseModifiedInfo
    {
        row_pk_t Pk;
        GridBackendField OldIsAssignedField;
        GridBackendField OldIsAssignedBoolField;
    };

    map<std::pair<PooledString, PooledString>, LicenseModifiedInfo> m_PlanData; // Key: Sku Id and Plan Id
    map<PooledString, LicenseModifiedInfo> m_SkuData; // Key: Sku Id

    map<PooledString, std::set<PooledString>> m_ModifiedSkus; // Key: sku id | Value: plans to be disabled ids
    std::set<PooledString> m_DisabledSkus;

    map<PooledString, int32_t> m_SkusDelta; // Key: Sku Id. Value: Delta nb consumed from this modification

	boost::YOpt<PooledString> m_UsageLocationOld;
	boost::YOpt<PooledString> m_UsageLocationNew;
	boost::YOpt<HttpResultWithError> m_UsageLocationResult;
};

