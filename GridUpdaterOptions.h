#pragma once

#include "GridBackendUtil.h"

class GridUpdaterOptions
{
public:
	enum Flags
	{
		UPDATE_GRID = 0x1,
		FULLPURGE = UPDATE_GRID << 1,
		REFRESH_MODIFICATION_STATES = FULLPURGE << 1,
		HANDLE_POSTUPDATE_ERRORS = REFRESH_MODIFICATION_STATES << 1,
		REFRESH_PROCESS_DATA = HANDLE_POSTUPDATE_ERRORS << 1,
		PROCESS_LOADMORE = REFRESH_PROCESS_DATA << 1,
        PARTIALPURGE = PROCESS_LOADMORE << 1,
		DO_NOT_SHOW_LOADING_ERROR_DIALOG = PARTIALPURGE << 1,
	};

	//GridUpdaterOptions::UPDATE_GRID | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE
	GridUpdaterOptions(int32_t p_Flags);// if REFRESH_PROCESS_DATA is set, the entire set of rows & columns is processed - Load More: always use the above
	explicit GridUpdaterOptions(int32_t p_Flags, const vector<GridBackendColumn*>& p_ColumnsWithRefreshedValues);// when rows are partially updated, tell us what is new

	bool IsUpdateGrid() const;
    bool IsFullPurge() const;
    bool IsRefreshModificationStates() const;
	bool IsHandlePostUpdateError() const;
	bool IsShowLoadingErrorDialog();
	bool IsRefreshProcessData() const;
	bool IsProcessLoadMore() const;
    bool IsPartialPurge() const;

	const set<GridBackendColumn*>&	GetColumnsWithRefreshedValues() const;
	const set<GridBackendRow*>&		GetRowsWithRefreshedValues() const;
    const set<GridBackendRow*>&     GetPartialPurgeParentRows() const;

	void SetRefreshProcessData(bool p_Refresh);
	void SetPurgeFlag(int32_t p_Flag);
	void AddRowWithRefreshedValues(GridBackendRow* p_RowWithRefreshedValues);// use for partial refresh of grid, do not use when processing a grid "refresh all"
	void AddRowsWithRefreshedValues(const std::unordered_set<GridBackendRow*>& p_RowsWithRefreshedValues);// use for partial refresh of grid, do not use when processing a grid "refresh all"
	void AddColumnWithRefreshedValues(GridBackendColumn* p_ColumnWithRefreshedValues);// use for partial refresh of grid, do not use when processing a grid "refresh all"
    void AddPartialPurgeParentRow(GridBackendRow* p_Row);
	
	void												AddAction(const GridBackendUtil::GRIDPOSTUPATEACTION p_Action);
	const vector<GridBackendUtil::GRIDPOSTUPATEACTION>&	GetActions() const;

private:
	int32_t m_Flags;

	vector<GridBackendUtil::GRIDPOSTUPATEACTION> m_Actions;

	set<GridBackendColumn*>	m_ColumnsWithRefreshedValues;
	set<GridBackendRow*>	m_RowsWithRefreshedValues;
    set<GridBackendRow*>	m_PartialPurgeParentRows;
};
