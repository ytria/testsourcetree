#include "GridTemplate.h"

#include "BaseO365Grid.h"

bool GridTemplate::g_Initialized = false;

PooledString GridTemplate::g_TypeGroup;
PooledString GridTemplate::g_TypeUser;

GridTemplate::GridTemplate()
	: m_ColumnID(nullptr)
	, m_ColumnMetaID(nullptr)
	, m_ColumnDisplayName(nullptr)
	, m_ColumnMetaType(nullptr)
{
	if (!g_Initialized)
	{
		g_TypeGroup = YtriaTranslate::Do(GridTemplate_GridTemplate_1, _YLOC("Group")).c_str();
		g_TypeUser	= YtriaTranslate::Do(GridTemplate_GridTemplate_2, _YLOC("User")).c_str();

		g_Initialized = true;
	}
}

GridBackendField GridTemplate::GetMetaIDField(const vector<PooledString>& p_MetaID)
{
	ASSERT(nullptr != m_ColumnMetaID);
	return nullptr != m_ColumnMetaID && !p_MetaID.empty() ? GridBackendField(p_MetaID, m_ColumnMetaID->GetID()) : GridBackendField::g_GenericField;
}

GridBackendField GridTemplate::GetMetaIDField(const PooledString& p_MetaID)
{
	ASSERT(nullptr != m_ColumnMetaID);
	return nullptr != m_ColumnMetaID ? GridBackendField(p_MetaID, m_ColumnMetaID->GetID()) : GridBackendField::g_GenericField;
}

void GridTemplate::CustomizeGrid(O365Grid& p_Grid)
{
	// Make all columns read-only to avoid bad surprises.
	// Exception: Hyperlinks don't work if hyperlink cells are read-only.
	for (auto c : p_Grid.GetBackendColumns())
		if (nullptr != c)
			c->SetReadOnly(!c->IsHyperlink());

	m_O365IconId = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDR_O365));
	m_OnPremIconId = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDR_ONPREM));
	m_HybridIconId = p_Grid.AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDR_HYBRID));
}

bool GridTemplate::hasValidErrorlessValue(const GridBackendField& p_Field)
{
	return p_Field.HasValue() && GridBackendUtil::ICON_ERROR != p_Field.GetIcon();
}

bool GridTemplate::isError(const GridBackendField& p_Field)
{
	return GridBackendUtil::ICON_ERROR == p_Field.GetIcon();
}

GridBackendField& GridTemplate::AddDateOrNeverField(const boost::YOpt<YTimeDate>& p_Date, GridBackendRow& p_Row, GridBackendColumn& p_Col)
{
	if (p_Date)
		return p_Row.AddField(*p_Date, &p_Col);
		
	return p_Row.AddField(_T("Never"), &p_Col);
}

bool GridTemplate::IsO365Metatype(const GridBackendRow& p_Row) const
{
	return (nullptr == m_ColumnMetaType || m_O365IconId == p_Row.GetField(m_ColumnMetaType).GetIcon());
}

bool GridTemplate::IsOnPremMetatype(const GridBackendRow& p_Row) const
{
	return nullptr != m_ColumnMetaType && m_OnPremIconId == p_Row.GetField(m_ColumnMetaType).GetIcon();
}

bool GridTemplate::IsHybridMetatype(const GridBackendRow& p_Row) const
{
	return nullptr != m_ColumnMetaType && m_HybridIconId == p_Row.GetField(m_ColumnMetaType).GetIcon();
}

void GridTemplate::SetO365Metatype(GridBackendRow& p_Row)
{
	if (nullptr != m_ColumnMetaType)
		p_Row.AddField(_T("O365"), m_ColumnMetaType, m_O365IconId).RemoveModified();
}

void GridTemplate::SetOnPremMetatype(GridBackendRow& p_Row)
{
	if (nullptr != m_ColumnMetaType)
		p_Row.AddField(_T("On-Premises"), m_ColumnMetaType, m_OnPremIconId).RemoveModified();
}

void GridTemplate::SetHybridMetatype(GridBackendRow& p_Row)
{
	if (nullptr != m_ColumnMetaType)
		p_Row.AddField(_T("Hybrid"), m_ColumnMetaType, m_HybridIconId).RemoveModified();
}
