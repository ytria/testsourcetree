#pragma once

#include "IPropertySetBuilder.h"

class UserListNoLicensePlansInfoPropertySet : public IPropertySetBuilder
{
public:
    virtual vector<rttr::property> GetPropertySet() const override;
};

class DeletedUserListNoLicensePlansInfoPropertySet : public UserListNoLicensePlansInfoPropertySet
{
public:
	virtual vector<rttr::property> GetPropertySet() const override;
};
