#pragma once

#include "IJsonSerializer.h"

#include "SiteCollection.h"

class SiteCollectionSerializer : public IJsonSerializer
{
public:
    SiteCollectionSerializer(const SiteCollection& p_SiteColl);
    void Serialize() override;

private:
    const SiteCollection& m_SiteColl;
};

