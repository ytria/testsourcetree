#include "GenericRevertSelectedButtonUpdater.h"

#include "GridFrameBase.h"
#include "BaseO365Grid.h"

GenericRevertSelectedButtonUpdater::GenericRevertSelectedButtonUpdater(GridFrameBase& p_Frame)
	:m_Frame(p_Frame)
{
}

void GenericRevertSelectedButtonUpdater::Update(CCmdUI* p_CmdUi)
{
	p_CmdUi->Enable(m_Frame.GetGrid().IsGridModificationsEnabled() && m_Frame.hasRevertableModificationsPending(true) && m_Frame.HasNoTaskRunning());
}
