#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessEvent.h"

class EventDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessEvent>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

