#include "O365ReportRequester.h"

#include "CStdioFileEx.h"
#include "FileUtil.h"
#include "ModuleO365Reports.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "MsGraphHttpRequestLogger.h"

O365ReportRequester::O365ReportRequester(const wstring& p_ReportApiCall, const boost::YOpt<O365ReportCriteria>& p_Criteria, size_t p_Index)
	: m_ReportApiCall(p_ReportApiCall)
	, m_Criteria(p_Criteria)
	, m_MetadataWritten(false)
	, m_Index(p_Index)
{
	ASSERT(m_Criteria);
}

TaskWrapper<void> O365ReportRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<SingleRequestResult>();
	return YSafeCreateTaskMutable([taskData = p_TaskData, session = p_Session, this]() mutable
	{
		web::uri_builder uri;
		uri.append_path(_YTEXT("reports"));

		// FIXME: Specific case - Office365ActivationsUserDetail has no period or date.
		// To make it simpler for caller, just ignore those parameters for that report. 
		if (m_ReportApiCall != O365Report<O365ReportID::Office365ActivationsUserDetail>::GetAPICall())
		{
			if (IsPeriod())
				uri.append_path(_YTEXTFORMAT(L"%s(period='%s')", m_ReportApiCall.c_str(), m_Criteria->m_PeriodOrDate.c_str()));
			else if (m_Criteria && !m_Criteria->m_PeriodOrDate.empty())
				uri.append_path(_YTEXTFORMAT(L"%s(date=%s)", m_ReportApiCall.c_str(), m_Criteria->m_PeriodOrDate.c_str()));
			else
				uri.append_path(m_ReportApiCall);
		}
		else
		{
			uri.append_path(m_ReportApiCall);
		}

		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, session->GetIdentifier());
		m_Result->SetResult(session->GetMSGraphSession(httpLogger->GetSessionType())->Read(uri.to_uri(), httpLogger, taskData).GetTask().get());

		// 302 redirection is handled by cpprestsdk, result contains the csv
		ASSERT(m_Result->GetResult().Response.status_code() != 302);
		if (m_Result->GetResult().Response.status_code() == 200)
		{
			{
				auto it = m_Result->GetResult().Response.headers().find(http::header_names::content_disposition);
				if (m_Result->GetResult().Response.headers().end() != it)
				{
					auto str = it->second;
					static const wstring ref = _YTEXT("filename=\"");
					auto p = Str::find(str, ref);
					if (std::wstring::npos != p)
					{
						str = str.substr(p + ref.size());
						auto p2 = Str::find(str, _YTEXT("\""));
						m_OriginalFileName = str.substr(0, p2);
					}
				}
			}

			auto it = m_Result->GetResult().Response.headers().find(_YTEXT("Date"));
			if (m_Result->GetResult().Response.headers().end() != it)
			{
				std::tm tm = {};
				std::wstringstream ss(it->second);
				ss >> std::get_time(&tm, _YTEXT("%a, %d %b %Y %H:%M:%S GMT"));
				if (m_Criteria)
					m_Criteria->m_CreationDate.emplace(CTime(std::mktime(&tm)), 0);
			}

			auto bodyStream = m_Result->GetResult().Response.body();

			m_OutputFilePath = FileUtil::GetTempFilePath(ModuleO365Reports::g_ReportFileExtension);

			if (m_Criteria)
				m_OutputFilePath += _YTEXT("_");

			static const size_t CHUNK_SIZE = 32768;

			{
				std::ofstream diskFile(m_OutputFilePath, std::ofstream::binary);
				size_t bytesRead = 0;
				pplx::streams::container_buffer<std::string> fileBuffer;
				while (!taskData.IsCanceled())
				{
					const auto read = bodyStream.read(fileBuffer, CHUNK_SIZE).get();

					if (0 == read)
						break;

					fileBuffer.collection().resize(read);
					diskFile << fileBuffer.collection();
					diskFile.flush();
					fileBuffer = pplx::streams::container_buffer<std::string>(); // TODO: Improve this by not creating a container_buffer on every iteration

					bytesRead += read;

					if (!bodyStream.is_valid() || bodyStream.is_eof() || CHUNK_SIZE > read)
						break;
				}
				diskFile.close();
			}
			
			m_Result->GetResult().Response.body().streambuf().close();

			wstring encoding;
			size_t bomSize = 0;
			{
				CStdioFileEx fileEx;
				CFileException e;
				if (TRUE == fileEx.Open(m_OutputFilePath.c_str(), CFile::modeRead, &e))
				{
					encoding = fileEx.GetEncoding();
					bomSize = CStdioFileEx::GetBOMSize(encoding);
				}
				else
				{
					TCHAR   szCause[255];
					CString strFormatted;
					e.GetErrorMessage(szCause, 255);
					LoggerService::Debug(_YDUMPFORMAT(L"Can't write Report file metadata. Unable to open temp file ().", szCause));
				}
			}

			ASSERT(0 == bomSize && encoding == _YTEXT("UTF8") || _YTEXT("UTF8 BOM"));
			if (!(0 == bomSize && encoding == _YTEXT("UTF8") || _YTEXT("UTF8 BOM")))
			{
				m_MetadataWritten = false;
				if (!encoding.empty())
					LoggerService::Debug(_YDUMPFORMAT(L"Can't write Report file metadata. Encoding issue (%s).", encoding.c_str()));
			}
			else
			{
				auto newFilePath = m_OutputFilePath.substr(0, m_OutputFilePath.size() - 1);
				{
					std::ifstream oldFile(m_OutputFilePath, std::ofstream::binary);
					if (!oldFile.is_open())
					{
						std::string err = strerror(errno);
					}

					std::ofstream newFile(newFilePath, std::ofstream::binary);
					if (!newFile.is_open())
					{
						std::string err = strerror(errno);
					}

					if (oldFile.is_open() && newFile.is_open())
					{
						std::string buffer;
						buffer.resize(CHUNK_SIZE);

						if (0 != bomSize)
						{
							oldFile.read(&buffer[0], bomSize);
							newFile.write(&buffer[0], bomSize);
						}

						auto metadata = Str::convertToUTF8(m_Criteria->Serialize(m_Index));
						if (!Str::endsWith(metadata, "\r\n") && !Str::endsWith(metadata, "\n"))
							metadata += "\r\n";
						newFile.write(&metadata[0], metadata.size());

						while (!oldFile.eof())
						{
							oldFile.read(&buffer[0], CHUNK_SIZE);
							newFile.write(&buffer[0], oldFile.gcount());
						}
					}
				}
				::DeleteFile(m_OutputFilePath.c_str());
				m_OutputFilePath = newFilePath;

				m_MetadataWritten = true;
			}

			if (taskData.IsCanceled())
			{
				::DeleteFile(m_OutputFilePath.c_str()); // Delete temp file if download is interrupted
				pplx::cancel_current_task();
				m_OutputFilePath.clear();
			}
		}
	});
}

const wstring& O365ReportRequester::GetReportFilePath() const
{
	return m_OutputFilePath;
}

const wstring& O365ReportRequester::GetReportOriginalFileName() const
{
	return m_OriginalFileName;
}

const boost::YOpt<YTimeDate>& O365ReportRequester::GetReportDate() const
{
	return m_Criteria ? m_Criteria->m_CreationDate : boost::none;
}

const SingleRequestResult& O365ReportRequester::GetResult() const
{
	ASSERT(m_Result);
	return *m_Result;
}

bool O365ReportRequester::CouldwriteMetadata() const
{
	return m_MetadataWritten;
}

bool O365ReportRequester::IsPeriod() const
{
	bool ret = m_Criteria && Str::beginsWith(m_Criteria->m_PeriodOrDate, _YTEXT("D"));
	ASSERT(!ret
		|| m_Criteria->m_PeriodOrDate == _YTEXT("D7")
		|| m_Criteria->m_PeriodOrDate == _YTEXT("D30")
		|| m_Criteria->m_PeriodOrDate == _YTEXT("D90")
		|| m_Criteria->m_PeriodOrDate == _YTEXT("D180"));
	return ret;
}
