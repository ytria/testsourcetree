#include "ModuleLists.h"

#include "AutomationDataStructure.h"
#include "BusinessObjectManager.h"
#include "CommandInfo.h"
#include "FrameLists.h"
#include "GraphCache.h"
#include "RefreshSpecificData.h"
#include "TaskDataManager.h"
#include "MultiObjectsPageRequestLogger.h"
#include "MultiObjectsRequestLogger.h"
#include "YSafeCreateTask.h"

void ModuleLists::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
    {
    case Command::ModuleTask::List:
		showLists(p_Command);
    break;
    default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
		break;
    }
}

void ModuleLists::showLists(const Command& p_Command)
{
	auto p_Action		= p_Command.GetAutomationAction();
	const auto& info	= p_Command.GetCommandInfo();

	auto		p_ExistingFrame = dynamic_cast<FrameLists*>(info.GetFrame());
	const auto&	p_SiteIDs		= info.GetIds();
	auto		p_SourceWindow	= p_Command.GetSourceWindow();

	auto sourceCWnd			= p_SourceWindow.GetCWnd();
	auto frame				= p_ExistingFrame;
	const bool isRefresh	= nullptr != p_ExistingFrame;
	if (nullptr == p_ExistingFrame)
	{
		if (FrameLists::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin			= Origin::Site;
			moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::IDS;
			moduleCriteria.m_IDs			= p_SiteIDs;
			moduleCriteria.m_Privilege		= info.GetRBACPrivilege();
			if (ShouldCreateFrame<FrameLists>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				frame = new FrameLists(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleLists_showLists_1, _YLOC("SharePoint Site Lists")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
				frame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(frame);
				frame->Erect();
			}
			else
				return;
		}
	}

    if (nullptr != frame)
    {
        RefreshSpecificData refreshSelectedData;
        if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
            refreshSelectedData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

		doRefresh(frame, frame->GetModuleCriteria(), p_Command, refreshSelectedData, isRefresh);
    }
}

void ModuleLists::doRefresh(FrameLists* newFrame, const ModuleCriteria& p_ModuleCriteria, const Command& p_Command, const RefreshSpecificData& p_RefreshData, bool p_IsRefresh)
{
    auto taskData	= addFrameTask(YtriaTranslate::Do(ModuleLists_doRefresh_1, _YLOC("Show SharePoint Site Lists")).c_str(), newFrame, p_Command, !p_IsRefresh);

	ASSERT(ModuleCriteria::UsedContainer::IDS == p_ModuleCriteria.m_UsedContainer);

	const auto origin = p_ModuleCriteria.m_Origin;

	YSafeCreateTask([taskData, currentModuleCriteriaIds = newFrame->GetModuleCriteria().m_IDs, p_RefreshData, hwnd = newFrame->GetSafeHwnd(), origin, p_Action = p_Command.GetAutomationSetup().m_ActionToExecute, p_ModuleCriteria, bom = newFrame->GetBusinessObjectManager(), p_IsRefresh]()
    {
        const bool partialRefresh = !p_RefreshData.m_Criteria.m_IDs.empty();
		const auto& ids = partialRefresh ? p_RefreshData.m_Criteria.m_IDs : currentModuleCriteriaIds;

		auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("lists"), ids.size());

		O365DataMap<BusinessSite, vector<BusinessList>> allLists;
        for (const auto& id : ids)
        {
			BusinessSite site;
			site.SetID(id);
			logger->SetContextualInfo(bom->GetGraphCache().GetSiteContextualInfo(id));
			bom->GetGraphCache().SyncUncachedSite(site, std::make_shared<MultiObjectsRequestLogger>(*logger), taskData);

			logger->IncrementObjCount();
			logger->SetContextualInfo(GetDisplayNameOrId(site));

			vector<BusinessList> lists;
			if (!taskData.IsCanceled())
				lists = bom->GetBusinessLists(id, logger, taskData).GetTask().get();

			if (taskData.IsCanceled())
			{
				site.SetFlags(BusinessObject::CANCELED);
				allLists[site] = {};
			}
			else
			{
				allLists[site] = lists;
			}
        }

		YDataCallbackMessage<O365DataMap<BusinessSite, vector<BusinessList>>>::DoPost(allLists, [hwnd, partialRefresh, taskData, origin, p_Action, p_IsRefresh, isCanceled = taskData.IsCanceled()](const O365DataMap<BusinessSite, vector<BusinessList>>& p_Lists)
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameLists*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, taskData, false))
			{
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting lists for %s sites...", Str::getStringFromNumber(p_Lists.size()).c_str()));
				frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
				frame->GetGrid().ClearLog(taskData.GetId());

				{
					CWaitCursor _;
					frame->ShowLists(p_Lists, origin, !partialRefresh);
				}

				if (!p_IsRefresh)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, taskData, p_Action);
			}
		});
    });
}
