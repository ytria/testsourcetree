#include "FieldValueSetDeserializer.h"

void FieldValueSetDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    for (const auto& keyVal : p_Object)
    {
        const auto& name = keyVal.first;
        const auto& value = keyVal.second;
        if (value.is_string())
            m_Data.GetUnknownProperties()[name] = value.as_string();
        else if (value.is_boolean())
            m_Data.GetUnknownProperties()[name] = value.as_bool();
        else if (value.is_integer())
            m_Data.GetUnknownProperties()[name] = value.as_integer();
        else if (value.is_double())
            m_Data.GetUnknownProperties()[name] = value.as_double();
    }
}
