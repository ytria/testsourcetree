#include "UserMacroRequest.h"

#include "CachedUserPropertySet.h"
#include "MultiObjectsPageRequestLogger.h"
#include "UserDeserializer.h"
#include "UserRequester.h"
#include "UserMailboxSettingsPropertySet.h"
#include "safeTaskCall.h"
#include "BusinessUser.h"
#include "RbacRequiredPropertySet.h"
#include "MsGraphHttpRequestLogger.h"
#include "SqlCacheConfig.h"

UserMacroRequest::UserMacroRequest(BusinessUser& p_User, const std::unordered_map<std::vector<UBI>, std::vector<rttr::property>>& p_Properties, uint32_t p_Flags, std::shared_ptr<BusinessObjectManager> p_BOM, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData)
	: m_User(p_User)
	, m_BOM(p_BOM)
	, m_TaskData(p_TaskData)
	, m_Properties(p_Properties)
	, m_Logger(p_Logger)
{
	auto session = m_BOM->GetSapio365Session();

	// We MUST pass at least one property list
	ASSERT(!m_Properties.empty() && !m_Properties.begin()->second.empty());
	size_t i = 0;
	for (auto& p : m_Properties)
	{
		addPropertiesStep(i, p.second, i == 0 ? KeepOnlyNullValuesMode::ONLYIFERROR : KeepOnlyNullValuesMode::YES, p.first);
		++i;
	}

	bool requestMailboxSettings = isFlagSet(BusinessObjectManager::UserAdditionalRequest::RequestMailboxSettingsForNonGuest, p_Flags);
	if (requestMailboxSettings && p_User.GetUserType() && *p_User.GetUserType() != BusinessUser::g_UserTypeGuest)
	{
		AddStep(CreateMailboxSettingsPropertiesStep(p_User, p_Flags));
		AddStep(CreateMailFolderStep(p_User, p_Flags));
	}

	bool requestDrive = isFlagSet(BusinessObjectManager::UserAdditionalRequest::RequestDriveForNonGuest, p_Flags);
	if (requestDrive && p_User.GetUserType() && *p_User.GetUserType() != BusinessUser::g_UserTypeGuest)
		AddStep(CreateDriveStep(p_User, p_Flags));

	if (isFlagSet(BusinessObjectManager::UserAdditionalRequest::RequestManager, p_Flags))
		AddStep(CreateManagerStep(p_User, p_Flags));
}

std::unique_ptr<MacroRequest::Step> UserMacroRequest::CreateMailboxSettingsPropertiesStep(BusinessUser& p_User, int p_Flags) const
{
	return std::make_unique<Step>
	(
		[&p_User]()
		{
#if SKIP_NEXT_REQUESTS_ON_ERROR
			if (p_User.GetError())
			{
				p_User.SetMailboxSettingsError(SkippedError());
				return false;
			}
#endif
			return true;
		},
		[session = m_BOM->GetSapio365Session(), &p_User, taskData = m_TaskData, logger = m_Logger]()
		{
			if (taskData.IsCanceled())
				return false;

			boost::YOpt<SapioError> error;

			auto properties = UserMailboxSettingsPropertySet();
			logger->SetLogMessage(_T("user mailbox settings properties"));
			UserRequester userSync(p_User.GetID(), std::move(properties), false, logger);
			userSync.SetRbacSessionMode(Sapio365Session::APP_SESSION);
			safeTaskCall(userSync.Send(session, taskData), session->GetMSGraphSession(Sapio365Session::APP_SESSION), [&error](const std::exception_ptr& p_ExceptPtr)
				{
					ASSERT(p_ExceptPtr);
					if (p_ExceptPtr)
					{
						try
						{
							std::rethrow_exception(p_ExceptPtr);
						}
						catch (const RestException& e)
						{
							error = HttpError(e);
						}
						catch (const std::exception& e)
						{
							error = SapioError(e);
						}
					}
				}, taskData).GetTask().wait();

				if (taskData.IsCanceled())
					return false;

				p_User.SetMailboxSettings(userSync.GetData().GetMailboxSettings());

				if (error && error->GetStatusCode() != 404)
				{
					//p_User.SetDataError(UBI::MAILBOXSETTINGS, error);
					p_User.SetMailboxSettingsError(error);
				}

				p_User.SetDataDate(UBI::MAILBOXSETTINGS, taskData.GetLastRequestOn());

				/*if (error)
					bu.SetError(error);*/

				return true;
		}
	);
}

std::unique_ptr<MacroRequest::Step> UserMacroRequest::CreateMailFolderStep(BusinessUser& p_User, int p_Flags) const
{
	return std::make_unique<Step>
		(
			[&p_User]()
			{
				if (
#if SKIP_NEXT_REQUESTS_ON_ERROR
					p_User.GetError() ||
#endif
					p_User.GetMailboxSettingsError()/*p_User.GetDataError(UBI::MAILBOXSETTINGS)*/)
				{
					p_User.SetArchiveFolderNameError(SkippedError());
					//p_User.SetDataError(UBI::ARCHIVEFOLDERNAME, SkippedError());
					p_User.SetDataDate(UBI::ARCHIVEFOLDERNAME, p_User.GetDataDate(UBI::MAILBOXSETTINGS)); // Otherwise, it won't be saved in SQL
					return false;
				}
				return p_User.GetMailboxSettings() && p_User.GetMailboxSettings()->ArchiveFolder;
			},
			[session = m_BOM->GetSapio365Session(), &p_User, bom = m_BOM, taskData = m_TaskData, logger = m_Logger]()
			{
				logger->SetLogMessage(_T("user mail folder"));
				BusinessMailFolder mailFolder = bom->GetUserBusinessMailFolder(p_User.GetID(), *p_User.GetMailboxSettings()->ArchiveFolder, logger, taskData).GetTask().get();

				if (taskData.IsCanceled())
					return false;

				if (mailFolder.GetError())
				{
					p_User.SetArchiveFolderNameError(mailFolder.GetError());
					//p_User.SetDataError(UBI::ARCHIVEFOLDERNAME, mailFolder.GetError());
				}
				else
					p_User.SetArchiveFolderName(mailFolder.m_DisplayName);

				p_User.SetDataDate(UBI::ARCHIVEFOLDERNAME, taskData.GetLastRequestOn());

				return true;
			}
			);
}

std::unique_ptr<MacroRequest::Step> UserMacroRequest::CreateDriveStep(BusinessUser& p_User, int p_Flags) const
{
	return std::make_unique<Step>
		(
			[&p_User]()
			{
#if SKIP_NEXT_REQUESTS_ON_ERROR
				if (
					p_User.GetError() ||
					p_User.GetMailboxSettingsError())
				{
					p_User.SetDriveError(SkippedError());
					return false;
				}
#endif
				return true;
			},
			[session = m_BOM->GetSapio365Session(), &p_User, bom = m_BOM, taskData = m_TaskData, logger = m_Logger]()
			{
				if (taskData.IsCanceled())
					return false;

				logger->SetLogMessage(_T("drive"));
				auto drive = bom->GetBusinessUserDrive(p_User.GetID(), logger, taskData).GetTask().get();
				if (!drive.GetError())
					p_User.SetDrive(drive);
				else if (404 != drive.GetError()->GetStatusCode())
				{
					p_User.SetDriveError(drive.GetError());
					//p_User.SetDataError(UBI::DRIVE, drive.GetError());
				}

				p_User.SetDataDate(UBI::DRIVE, taskData.GetLastRequestOn());

				return true;
			}
		);
}

std::unique_ptr<MacroRequest::Step> UserMacroRequest::CreateManagerStep(BusinessUser& p_User, int p_Flags) const
{
	return std::make_unique<Step>
		(
			[&p_User]()
			{
#if SKIP_NEXT_REQUESTS_ON_ERROR
				if (p_User.GetError() || p_User.GetMailboxSettingsError() || p_User.GetDriveError())
				{
					user.SetManagerError(SkippedError());
					return false;
				}
#endif
				return true;
			},
			[session = m_BOM->GetSapio365Session(), &p_User, bom = m_BOM, taskData = m_TaskData, logger = m_Logger]()
			{
				if (taskData.IsCanceled())
					return false;

				boost::YOpt<SapioError> error;

				logger->SetLogMessage(_T("user manager"));

				auto deserializer = std::make_shared<UserDeserializer>(session);
				const auto properties = CachedUserPropertySet().GetPropertySet();
				auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, session->GetIdentifier());
				BusinessUser manager = safeTaskCall
				(
					session->GetMSGraphSession(httpLogger->GetSessionType())->GetUserManager(deserializer, p_User.GetID(), properties, logger, httpLogger, taskData).Then
					(
						[deserializer, httpLogger, bom]
						{
							bom->GetGraphCache().AddUser(deserializer->GetData(), true, { UBI::MIN });
							return deserializer->GetData();
						}), session->GetMSGraphSession(httpLogger->GetSessionType()),
							[&error](const std::exception_ptr& p_ExceptPtr)
						{
							ASSERT(p_ExceptPtr);
							if (p_ExceptPtr)
							{
								try
								{
									std::rethrow_exception(p_ExceptPtr);
								}
								catch (const RestException& e)
								{
									error = HttpError(e);
								}
								catch (const std::exception& e)
								{
									error = SapioError(e);
								}
								catch (...)
								{
									ASSERT(false);
								}
							}

							return BusinessUser();
						}, taskData
						).get();

						if (taskData.IsCanceled())
							return false;

						if (error && error->GetStatusCode() != 404)
						{
							p_User.SetManagerError(error);
							//p_User.SetDataError(UBI::MANAGER, error);
						}
						else if (!error)
							p_User.SetManagerID(manager.GetID());

						p_User.SetDataDate(UBI::MANAGER, taskData.GetLastRequestOn());

						return true;
			}
		);
}

void UserMacroRequest::addPropertiesStep(size_t index, const vector<rttr::property>& p_Properties, KeepOnlyNullValuesMode p_KeepOnlyNullValuesMode, const std::vector<UBI>& p_BlockIds)
{
	ASSERT(!p_Properties.empty());
	if (p_Properties.empty())
		return;

	const auto betaApi = p_Properties[0].get_metadata(PropertyMetaDataKey::BetaAPI).is_valid() && p_Properties[0].get_metadata(PropertyMetaDataKey::BetaAPI).get_value<bool>();
	// Property set must be consistent (no mix of beta/normal properties).
	ASSERT(std::none_of(p_Properties.begin(), p_Properties.end(), [betaApi](const auto& p) { return betaApi != (p.get_metadata(PropertyMetaDataKey::BetaAPI).is_valid() && p.get_metadata(PropertyMetaDataKey::BetaAPI).get_value<bool>()); }));

	auto session = m_BOM->GetSapio365Session();
	auto& bu = m_User;
	AddStep(std::make_unique<Step>
		(
			[&bu]()
			{
#if SKIP_NEXT_REQUESTS_ON_ERROR
				return !bu.GetError();
#else
				return true;
#endif
			}, // Always process this step if no error previously

			[&bu, session, p_Properties, p_KeepOnlyNullValuesMode, betaApi, taskData = m_TaskData, logger = m_Logger, index, p_BlockIds]()
			{
				if (taskData.IsCanceled())
					return false;

				boost::YOpt<SapioError> error;

				logger->SetLogMessage(_YFORMAT(L"user properties (batch #%s)", std::to_wstring(index + 1).c_str()));
				UserRequester userSync(bu.GetID(), p_Properties, logger);
				userSync.SetUseBetaEndpoint(betaApi);
				userSync.SetRbacSessionMode(Sapio365Session::USER_SESSION);
				safeTaskCall(userSync.Send(session, taskData), session->GetMSGraphSession(Sapio365Session::USER_SESSION), [&error](const std::exception_ptr& p_ExceptPtr)
					{
						ASSERT(p_ExceptPtr);
						if (p_ExceptPtr)
						{
							try
							{
								std::rethrow_exception(p_ExceptPtr);
							}
							catch (const RestException& e)
							{
								error = HttpError(e);
							}
							catch (const std::exception& e)
							{
								error = SapioError(e);
							}
						}
					}, taskData).GetTask().wait();

					if (taskData.IsCanceled())
						return false;

					if (KeepOnlyNullValuesMode::YES == p_KeepOnlyNullValuesMode
						|| KeepOnlyNullValuesMode::ONLYIFERROR == p_KeepOnlyNullValuesMode && error)
					{
						bu.MergeWith(userSync.GetData());
					}
					else
					{
						bu = std::move(userSync.GetData());
					}

					if (error)
						bu.SetError(error);

					for (auto b : p_BlockIds)
					{
						/*if (error)
							bu.SetDataError(b, error);*/
						bu.SetDataDate(b, taskData.GetLastRequestOn());
					}

					return true;
			}
		)
	);
}