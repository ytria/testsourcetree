#include "OnPremisesProvisioningErrorSerializer.h"

#include "JsonSerializeUtil.h"

OnPremisesProvisioningErrorSerializer::OnPremisesProvisioningErrorSerializer(const OnPremisesProvisioningError& p_OnPremisesProvisioningError)
    : m_OnPremProvErr(p_OnPremisesProvisioningError)
{
}

void OnPremisesProvisioningErrorSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeString(_YTEXT("category"), m_OnPremProvErr.m_Category, obj);
    JsonSerializeUtil::SerializeTimeDate(_YTEXT("occurredDateTime"), m_OnPremProvErr.m_OccurredDateTime, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("propertyCausingError"), m_OnPremProvErr.m_PropertyCausingError, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("value"), m_OnPremProvErr.m_Value, obj);
}
