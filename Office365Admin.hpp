#pragma once

#include "DlgLicenseMessageBoxHTML.h"

template<LicenseTag licenseTag>
bool Office365AdminApp::CheckAndWarn(CWnd* p_Parent)
{
	const bool authorized = IsLicense<licenseTag>();

	if (!authorized)
	{
		DlgLicenseMessageBoxHTML dlg(p_Parent,
			DlgMessageBox::eIcon_Information,
			GetLicenseWarnMessage<licenseTag>(),
			_YTEXT(""),
			_YTEXT(""),
			{ { IDOK, YtriaTranslate::Do(BackstagePanelAbout_OnDeauthorizeComputer_4, _YLOC("OK")).c_str() } },
			{ 0, _YTEXT("") },
			_YTEXT("#") + theApp.GetApplicationColor());
		dlg.DoModal();
	}

	return authorized;
}

