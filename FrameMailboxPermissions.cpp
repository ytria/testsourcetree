#include "FrameMailboxPermissions.h"
#include "IButtonUpdater.h"
#include "UpdatedObjectsGenerator.h"

IMPLEMENT_DYNAMIC(FrameMailboxPermissions, GridFrameBase)

FrameMailboxPermissions::FrameMailboxPermissions(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, p_Module, p_HistoryMode, p_PreviousFrame)
{
	m_ModBtnsUpdater.SetSaveAllEnableUpdater(std::make_unique<GridMailboxPermissions::MailboxPermissionsSaveAllUpdater>(*this, m_MailboxPermissionsGrid));
	m_ModBtnsUpdater.SetSaveSelectedEnableUpdater(std::make_unique<GridMailboxPermissions::MailboxPermissionsSaveSelectedUpdater>(*this, m_MailboxPermissionsGrid));
	m_ModBtnsUpdater.SetRevertAllEnableUpdater(std::make_unique<GridMailboxPermissions::MailboxPermissionsRevertAllUpdater>(*this, m_MailboxPermissionsGrid));
	m_ModBtnsUpdater.SetRevertSelectedEnableUpdater(std::make_unique<GridMailboxPermissions::MailboxPermissionsRevertSelectedUpdater>(*this, m_MailboxPermissionsGrid));

	m_CreateAddRemoveToSelection = !p_IsMyData;
	m_CreateRefreshPartial = true;
	m_CreateShowContext = !p_IsMyData;
}

void FrameMailboxPermissions::ShowMailboxPermissions(const O365DataMap<BusinessUser, vector<MailboxPermissions>>& p_MailboxPermissions, bool p_FullPurge)
{
	m_MailboxPermissionsGrid.BuildTreeView(p_MailboxPermissions, p_FullPurge);
}

void FrameMailboxPermissions::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	updateTimeRefreshControlToNow();
	GetGrid().AnnotationsRefresh(true);

	auto updatedObjectsGen = UpdatedObjectsGenerator<MailboxPermissions>();

	CommandInfo info;
	info.Data() = updatedObjectsGen;
	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::MailboxPermissions, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameMailboxPermissions::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.Data() = p_RefreshData;
	info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::MailboxPermissions, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

O365Grid& FrameMailboxPermissions::GetGrid()
{
	return m_MailboxPermissionsGrid;
}

GridMailboxPermissions& FrameMailboxPermissions::GetMailboxGrid()
{
	return m_MailboxPermissionsGrid;
}

void FrameMailboxPermissions::createGrid()
{
	m_MailboxPermissionsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameMailboxPermissions::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigUsers();
}

bool FrameMailboxPermissions::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	GridFrameBase::customizeActionsRibbonTab(tab, false, false);

	if (CRMpipe().IsFeatureSandboxed())
	{
		CreateApplyGroup(tab);
		CreateRevertGroup(tab);
	
		auto editGroup = tab.AddGroup(_YTEXT("Edit"));
		auto btnEdit = editGroup->Add(xtpControlButton, ID_MAILBOXPERMISSIONSGRID_EDIT);
		setImage({ ID_MAILBOXPERMISSIONSGRID_EDIT }, IDB_MAILBOXPERMISSIONSGRID_EDIT, xtpImageNormal);
		setImage({ ID_MAILBOXPERMISSIONSGRID_EDIT }, IDB_MAILBOXPERMISSIONSGRID_EDIT_16X16, xtpImageNormal);
		setGridControlTooltip(btnEdit);

		auto btnAdd = editGroup->Add(xtpControlButton, ID_MAILBOXPERMISSIONSGRID_ADD);
		setImage({ ID_MAILBOXPERMISSIONSGRID_ADD }, IDB_MAILBOXPERMISSIONSGRID_ADD, xtpImageNormal);
		setImage({ ID_MAILBOXPERMISSIONSGRID_ADD }, IDB_MAILBOXPERMISSIONSGRID_ADD_16X16, xtpImageNormal);
		setGridControlTooltip(btnAdd);

		auto btnRemove = editGroup->Add(xtpControlButton, ID_MAILBOXPERMISSIONSGRID_REMOVE);
		setImage({ ID_MAILBOXPERMISSIONSGRID_REMOVE }, IDB_MAILBOXPERMISSIONSGRID_REMOVE, xtpImageNormal);
		setImage({ ID_MAILBOXPERMISSIONSGRID_REMOVE }, IDB_MAILBOXPERMISSIONSGRID_REMOVE_16X16, xtpImageNormal);
		setGridControlTooltip(btnRemove);
	}

	auto systemGroup = tab.AddGroup(_T("System"));
	auto ctrl = systemGroup->Add(xtpControlButton, ID_MAILBOXPERMISSIONSGRID_TOGGLESYSTEMROWS);
	GridFrameBase::setImage({ ID_MAILBOXPERMISSIONSGRID_TOGGLESYSTEMROWS }, IDB_MAILBOXPERMISSIONS_TOGGLE_SYSTEMROWS, xtpImageNormal);
	GridFrameBase::setImage({ ID_MAILBOXPERMISSIONSGRID_TOGGLESYSTEMROWS }, IDB_MAILBOXPERMISSIONS_TOGGLE_SYSTEMROWS_16X16, xtpImageNormal);
	setGridControlTooltip(ctrl);

	CreateUserInfoGroup(tab);

	automationAddCommandIDToGreenLight(ID_MAILBOXPERMISSIONSGRID_EDIT);

	return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameMailboxPermissions::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;
	return statusRV;
}
