#pragma once

#include "SingleRequestResult.h"

#include <memory>
using std::shared_ptr;
#include <string>
using std::wstring;
#include <utility>
using std::pair;
#include <vector>
using std::vector;

namespace Cosmos
{
	class Paginator
	{
	public:
		using HeadersType = vector<pair<wstring, wstring>>;
		using FunctionType = std::function<shared_ptr<SingleRequestResult>(const HeadersType&)>;

		Paginator(const HeadersType& p_Headers, FunctionType p_RequestFunction);
		pplx::task<void> Paginate();

	private:
		shared_ptr<SingleRequestResult> m_Result;
		HeadersType m_AdditionalHeaders;

		FunctionType m_RequestFunction;
	};
}

