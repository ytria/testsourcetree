#pragma once

#include "ISqlQuery.h"
#include "MFCUtil.h"
#include "Str.h"

class SQLiteEngine;

// This splits p_Query using ";" delimiter into simples statements and create a sqlite3_stmt for each
class SqlQueryMultiPreparedStatement : public ISqlQuery
{
public:
	SqlQueryMultiPreparedStatement(SQLiteEngine& p_Engine, const std::vector<wstring>& p_Statements);
	~SqlQueryMultiPreparedStatement();

	void Run() override; // Runs every statement (stops is one is not success)
	int GetNbRowsChanged() const;

	// Binding have never been tested!!!!
	void BindString(size_t p_StatementIndex, int p_ParamIndex, const wstring& p_Value);
	void BindInt(size_t p_StatementIndex, int p_ParamIndex, int p_Value);
	void BindNull(size_t p_StatementIndex, int p_ParamIndex);
	void BindInt64(size_t p_StatementIndex, int p_ParamIndex, int64_t p_Value);
	void BindBlob(size_t p_StatementIndex, int p_ParamIndex, const void* p_Data, int p_DataSize);

private:
	void HandleError(size_t p_StatementIndex) const;

	std::vector<wstring> m_StatementsWstring;
	std::vector<std::string> m_Statements;
	std::vector<sqlite3_stmt*> m_Stmts;
	SQLiteEngine& m_Engine;

	struct Binding
	{
	public:
		Binding(const wstring& p_Value);
		Binding(int p_Value);
		Binding(); // For null
		Binding(int64_t p_Value);
		Binding(const void* p_Data, int p_DataSize);

		int bind(sqlite3_stmt* p_Stmt, int p_ParamIndex);

	private:
		enum Type
		{
			BIND_STRING = 0,
			BIND_INT,
			BIND_NULL,
			BIND_INT64,
			BIND_BLOB
		};
		Type m_Type;

		const wstring* p_StrValue = nullptr;
		int m_IntValue = 0;
		int64_t m_Int64Value = 0;
		const void* m_Data = nullptr;
	};

	std::vector<std::map<int, Binding>> m_Bindings;
};

