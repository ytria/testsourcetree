#pragma once

#include "IRequester.h"
#include "UsageDeserializer.h"

class SingleRequestResult;

template <class T, class U>
class ValueListDeserializer;

namespace Azure
{
	class DbAccountUsageRequester : public IRequester
	{
	public:
		DbAccountUsageRequester(const wstring& p_SubscriptionId, const wstring& p_ResGroup, const wstring& p_DbAccountName);
		TaskWrapper<void> Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

		const vector<Azure::Usage>& GetData() const;

	private:
		shared_ptr<ValueListDeserializer<Azure::Usage, Azure::UsageDeserializer>> m_Deserializer;
		shared_ptr<SingleRequestResult> m_Result;

		wstring m_SubscriptionId;
		wstring m_ResGroup;
		wstring m_DbAccountName;
	};
}
