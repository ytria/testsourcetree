#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "GeoCoordinates.h"

class GeoCoordinatesDeserializer : public JsonObjectDeserializer, public Encapsulate<GeoCoordinates>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

