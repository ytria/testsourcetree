#include "GridLogger.h"

#include "MessageContainerObjects.h"

#include "CacheGrid.h"

#include <sstream>
using std::wostringstream;

void GridLogger::SetGridHWND(HWND p_hGrid)
{
	m_hGrid = p_hGrid;
}

const HWND GridLogger::GetGridHWND() const
{
	return m_hGrid;
}

void GridLogger::Log(const LogEntry& p_Entry) const
{
	if (nullptr != GetGridHWND() && ::IsWindow(GetGridHWND()))
	{
		if(p_Entry.m_LogLevel == LogLevel::Friendly)
		{
			wostringstream* ossStr = new wostringstream;
			*ossStr << p_Entry.m_Message;
			::PostMessage(GetGridHWND(), WM_GENERIC_UPDATETRACE, 0, (LPARAM)ossStr);
		}
	}
}

