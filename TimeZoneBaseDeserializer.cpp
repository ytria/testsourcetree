#include "TimeZoneBaseDeserializer.h"

#include "JsonSerializeUtil.h"

void TimeZoneBaseDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.Name, p_Object);
}
