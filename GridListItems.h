#pragma once

#include "BaseO365Grid.h"

#include "BusinessList.h"
#include "BusinessListItem.h"
#include "ColumnDefinition.h"

class GridListItems : public ModuleO365Grid<BusinessListItem>
{
public:
    GridListItems();
    virtual void customizeGrid() override;

    void BuildView(const O365DataMap<wstring, vector<BusinessList>>& p_Lists, Origin p_Origin, bool p_FullPurge);

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
    virtual BusinessListItem getBusinessObject(GridBackendRow* p_Row) const override;

    void clearGrid();

    struct ListColumn
    {
		ListColumn(const BusinessList* p_List, const BusinessColumnDefinition* p_Column)
			: m_List(p_List)
			, m_Column(p_Column)
		{}
        const BusinessList* m_List;
        const BusinessColumnDefinition* m_Column;
        bool operator<(const ListColumn& p_Other) const
        {
            return nullptr != m_Column && m_Column->GetDisplayName()
				&& nullptr != p_Other.m_Column && p_Other.m_Column->GetDisplayName()
				&& wcscmp(m_Column->GetDisplayName()->c_str(), p_Other.m_Column->GetDisplayName()->c_str()) < 0;
        }
    };

    void createColumns(const vector<ListColumn>& p_ListColumns);
    void fillRow(GridBackendRow* p_Row, const BusinessListItem& p_ListItem, const BusinessList& p_List);

    void PopulateFields(const PooledString& p_ListId, const BusinessListItem &p_ListItem, GridBackendRow* p_Row);

    void UpdateListColumnsCache(const PooledString& p_ListId, const wstring& p_PropertyName);

    void AddIdentityField(const boost::YOpt<IdentitySet>& identitySet, GridBackendRow* p_Row, GridBackendColumn* p_Column);

private:
    map<PooledString, GridBackendColumn*> m_Columns; // Key: sharepoint column name
    map<PooledString, std::set<PooledString>> m_ListColumns; // Key: List id; value: list of column unique identifiers that the list contains
    
    GridBackendColumn* m_ColMetaSiteId;
    GridBackendColumn* m_ColMetaSiteDisplayName;
    GridBackendColumn* m_ColMetaId;
    GridBackendColumn* m_ColMetaDisplayName;
    GridBackendColumn* m_ColMetaName;

    GridBackendColumn* m_ColListItemName;
    GridBackendColumn* m_ColListItemCreatedByUserName;
    GridBackendColumn* m_ColListItemCreatedByUserId;
    GridBackendColumn* m_ColListItemCreatedByUserEmail;
    GridBackendColumn* m_ColListItemCreatedByApplicationName;
    GridBackendColumn* m_ColListItemCreatedByApplicationId;
    GridBackendColumn* m_ColListItemCreatedByDeviceName;
    GridBackendColumn* m_ColListItemCreatedByDeviceId;
    GridBackendColumn* m_ColListItemCreatedDateTime;
    GridBackendColumn* m_ColListItemDescription;
    GridBackendColumn* m_ColListItemLastModifiedByUserName;
    GridBackendColumn* m_ColListItemLastModifiedByUserId;
    GridBackendColumn* m_ColListItemLastModifiedByUserEmail;
    GridBackendColumn* m_ColListItemLastModifiedByAppName;
    GridBackendColumn* m_ColListItemLastModifiedByApplicationId;
    GridBackendColumn* m_ColListItemLastModifiedByDeviceName;
    GridBackendColumn* m_ColListItemLastModifiedByDeviceId;
    GridBackendColumn* m_ColListItemLastModifiedDateTime;
    GridBackendColumn* m_ColListItemWebUrl;
};
