#include "SiteDeserializerFactory.h"
#include "Sapio365Session.h"

SiteDeserializerFactory::SiteDeserializerFactory(std::shared_ptr<const Sapio365Session> p_Session)
	: m_Session(p_Session)
{
}

SiteDeserializer SiteDeserializerFactory::Create()
{
    return SiteDeserializer(m_Session);
}
