#pragma once

class MigrationUtil
{
public:
	static int32_t GetCurrentMigrationVersion();

	static wstring GetCurrentMigrationFilePath();
	static wstring GetMigrationFilePath(int p_Version);

	static wstring GetCurrentMigrationFileName();
	static wstring GetMigrationFileName(int p_Version);

private:
	static constexpr int CurrentMigrationVersion = 6;
};

