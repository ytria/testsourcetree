#include "DbUserSerializer.h"

#include "AssignedLicenseSerializer.h"
#include "AssignedPlanSerializer.h"
#include "BusinessDrive.h"
#include "ConcatVector.h"
#include "DbDriveSerializer.h"
#include "JsonSerializeUtil.h"
#include "LicenseAssignmentStateSerializer.h"
#include "ListSerializer.h"
#include "MsGraphFieldNames.h"
#include "MailboxSettingsSerializer.h"
#include "NullableSerializer.h"
#include "ObjectIdentitySerializer.h"
#include "OnPremisesExtensionAttributesSerializer.h"
#include "PasswordProfileSerializer.h"
#include "ProvisionedPlanSerializer.h"
#include "OnPremisesProvisioningErrorSerializer.h"
#include "SapioErrorSerializer.h"
#include "YtriaFieldsConstants.h"

namespace
{
    std::map<PooledString, std::function<void(const BusinessUser&, web::json::object&)>> g_Serializers
    {
            { _YUID(O365_ID),									[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_ID), u.m_Id, o); } }
        ,   { _YUID(O365_USER_PASSWORDPROFILE),					[](const BusinessUser& u, web::json::object& o)
                {
			        PasswordProfile pp;
	                pp.Password = u.GetPassword();
	                pp.ForceChangePasswordNextSignIn = u.GetForceChangePassword();
	                pp.ForceChangePasswordNextSignInWithMfa = u.GetForceChangePasswordWithMfa();
	                JsonSerializeUtil::SerializeAny(_YUID(O365_USER_PASSWORDPROFILE), NullableSerializer<PasswordProfile, PasswordProfileSerializer>(pp), o); 
                } }
        ,   { _YUID(O365_USER_ABOUTME),							[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_ABOUTME), u.GetAboutMe(), o); } }
		,	{ _YUID(O365_USER_ACCOUNTENABLED),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(O365_USER_ACCOUNTENABLED), u.GetAccountEnabled(), o); } }
		,	{ _YUID(O365_USER_ASSIGNEDLICENSES),				[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(O365_USER_ASSIGNEDLICENSES), ListSerializer<BusinessAssignedLicense, AssignedLicenseSerializer>(u.GetAssignedLicenses()), o); } }
		,	{ _YUID(O365_USER_ASSIGNEDPLANS),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(O365_USER_ASSIGNEDPLANS), ListSerializer<AssignedPlan, AssignedPlanSerializer>(u.GetAssignedPlans()), o); } }
		,	{ _YUID(O365_USER_BIRTHDAY),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_USER_BIRTHDAY), u.GetBirthday(), o); } }
		,	{ _YUID(O365_USER_BUSINESSPHONES),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_USER_BUSINESSPHONES), u.GetBusinessPhones(), o); } }
		,	{ _YUID(O365_USER_CITY),							[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_CITY), u.GetCity(), o); } }
		,	{ _YUID(O365_USER_COMPANYNAME),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_COMPANYNAME), u.GetCompanyName(), o); } }
		,	{ _YUID(O365_USER_COUNTRY),							[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_COUNTRY), u.GetCountry(), o); } }
		,	{ _YUID(O365_USER_DEPARTMENT),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_DEPARTMENT), u.GetDepartment(), o); } }
		,	{ _YUID(O365_USER_DISPLAYNAME),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_DISPLAYNAME), u.GetDisplayName(), o); } }
		,	{ _YUID(O365_USER_GIVENNAME),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_GIVENNAME), u.GetGivenName(), o); } }
		,	{ _YUID(O365_USER_HIREDATE),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_USER_HIREDATE), u.GetHireDate(), o); } }
		,	{ _YUID(O365_USER_IMADDRESSES),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_USER_IMADDRESSES), u.GetImAddresses(), o); } }
		,	{ _YUID(O365_USER_INTERESTS),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_USER_INTERESTS), u.GetInterests(), o); } }
		,	{ _YUID(O365_USER_JOBTITLE),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_JOBTITLE), u.GetJobTitle(), o); } }
		,	{ _YUID(O365_USER_MAIL),							[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_MAIL), u.GetMail(), o); } }
		,	{ _YUID(O365_USER_MAILBOXSETTINGS),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(O365_USER_MAILBOXSETTINGS), NullableSerializer<MailboxSettings, MailboxSettingsSerializer>(u.GetMailboxSettings()), o); } }
		,	{ _YUID(O365_USER_MAILNICKNAME),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_MAILNICKNAME), u.GetMailNickname(), o); } }
		,	{ _YUID(O365_USER_MOBILEPHONE),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_MOBILEPHONE), u.GetMobilePhone(), o); } }
		,	{ _YUID(O365_USER_MYSITE),							[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_MYSITE), u.GetMySite(), o); } }
		,	{ _YUID(O365_USER_OFFICELOCATION),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_OFFICELOCATION), u.GetOfficeLocation(), o); } }
		,	{ _YUID(O365_USER_ONPREMISESIMMUTABLEID),			[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_ONPREMISESIMMUTABLEID), u.GetOnPremisesImmutableId(), o); } }
		,	{ _YUID(O365_USER_ONPREMISESLASTSYNCDATETIME),		[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_USER_ONPREMISESLASTSYNCDATETIME), u.GetOnPremisesLastSyncDateTime(), o); } }
		,	{ _YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER),	[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER), u.GetOnPremisesSecurityIdentifier(), o); } }
		,	{ _YUID(O365_USER_ONPREMISESSYNCENABLED),			[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(O365_USER_ONPREMISESSYNCENABLED), u.GetOnPremisesSyncEnabled(), o); } }
		,	{ _YUID(O365_USER_PASSWORDPOLICIES),				[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_PASSWORDPOLICIES), u.GetPasswordPolicies(), o); } }
		,	{ _YUID(O365_USER_PASTPROJECTS),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_USER_PASTPROJECTS), u.GetPastProjects(), o); } }
		,	{ _YUID(O365_USER_POSTALCODE),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_POSTALCODE), u.GetPostalCode(), o); } }
		,	{ _YUID(O365_USER_PREFERREDLANGUAGE),				[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_PREFERREDLANGUAGE), u.GetPreferredLanguage(), o); } }
		,	{ _YUID(O365_USER_PREFERREDNAME),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_PREFERREDNAME), u.GetPreferredName(), o); } }
		,	{ _YUID(O365_USER_PROVISIONEDPLANS),				[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(O365_USER_PROVISIONEDPLANS), ListSerializer<ProvisionedPlan, ProvisionedPlanSerializer>(u.GetProvisionedPlans()), o); } }
		,	{ _YUID(O365_USER_PROXYADDRESSES),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_USER_PROXYADDRESSES), u.GetProxyAddresses(), o); } }
		,	{ _YUID(O365_USER_RESPONSIBILITIES),				[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_USER_RESPONSIBILITIES), u.GetResponsibilities(), o); } }
		,	{ _YUID(O365_USER_SCHOOLS),							[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_USER_SCHOOLS), u.GetSchools(), o); } }
		,	{ _YUID(O365_USER_SKILLS),							[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_USER_SKILLS), u.GetSkills(), o); } }
		,	{ _YUID(O365_USER_STATE),							[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_STATE), u.GetState(), o); } }
		,	{ _YUID(O365_USER_STREETADDRESS),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_STREETADDRESS), u.GetStreetAddress(), o); } }
		,	{ _YUID(O365_USER_SURNAME),							[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_SURNAME), u.GetSurname(), o); } }
		,	{ _YUID(O365_USER_USAGELOCATION),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_USAGELOCATION), u.GetUsageLocation(), o); } }
		,	{ _YUID(O365_USER_USERPRINCIPALNAME),				[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_USERPRINCIPALNAME), u.GetUserPrincipalName(), o); } }
		,	{ _YUID(O365_USER_USERTYPE),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_USERTYPE), u.GetUserType(), o); } }
		,	{ _YUID(O365_USER_DELETEDDATETIME),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_USER_DELETEDDATETIME), u.GetDeletedDateTime(), o); } }
		,	{ _YUID(O365_USER_AGEGROUP),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_AGEGROUP), u.GetAgeGroup(), o); } }
		,	{ _YUID(O365_USER_CREATEDDATETIME),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_USER_CREATEDDATETIME), u.GetCreatedDateTime(), o); } }
		,	{ _YUID(O365_USER_CONSENTPROVIDEDFORMINOR),			[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_CONSENTPROVIDEDFORMINOR), u.GetConsentProvidedForMinor(), o); } }
		,	{ _YUID(O365_USER_LEGALAGEGROUPCLASSIFICATION),		[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_LEGALAGEGROUPCLASSIFICATION), u.GetLegalAgeGroupClassification(), o); } }
		,	{ _YUID(O365_USER_ONPREMISESDOMAINNAME),			[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_ONPREMISESDOMAINNAME), u.GetOnPremisesDomainName(), o); } }
		,	{ _YUID(O365_USER_ONPREMISESSAMACCOUNTNAME),		[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_ONPREMISESSAMACCOUNTNAME), u.GetOnPremisesSamAccountName(), o); } }
		,	{ _YUID(O365_USER_ONPREMISESUSERPRINCIPALNAME),		[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_ONPREMISESUSERPRINCIPALNAME), u.GetOnPremisesUserPrincipalName(), o); } }
		,	{ _YUID(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME),	[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME), u.GetSignInSessionsValidFromDateTime(), o); } }
		,	{ _YUID(O365_USER_ONPREMISESPROVISIONINGERRORS),	[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(O365_USER_ONPREMISESPROVISIONINGERRORS), ListSerializer<OnPremisesProvisioningError, OnPremisesProvisioningErrorSerializer>(u.GetOnPremisesProvisioningErrors()), o); } }
		,	{ _YUID(O365_USER_OTHERMAILS),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(O365_USER_OTHERMAILS), u.GetOtherMails(), o); } }
		,	{ _YUID(O365_USER_PREFERREDDATALOCATION),			[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_PREFERREDDATALOCATION), u.GetPreferredDataLocation(), o); } }
		,	{ _YUID(O365_USER_SHOWINADDRESSLIST),				[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(O365_USER_SHOWINADDRESSLIST), u.GetShowInAddressList(), o); } }
		,	{ _YUID(O365_USER_EXTERNALUSERSTATUS),				[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_EXTERNALUSERSTATUS), u.GetExternalUserStatus(), o); } }
		,	{ _YUID(O365_USER_EXTERNALUSERSTATUSCHANGEDON),		[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_USER_EXTERNALUSERSTATUSCHANGEDON), u.GetExternalUserStatusChangedOn(), o); } }
		,	{ _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES),	[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES), NullableSerializer<OnPremisesExtensionAttributes, OnPremisesExtensionAttributesSerializer>(u.GetOnPremisesExtensionAttributes()), o); } }
		,	{ _YUID(O365_USER_EMPLOYEEID),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_EMPLOYEEID), u.GetEmployeeID(), o); } }
		,	{ _YUID(O365_USER_FAXNUMBER),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_FAXNUMBER), u.GetFaxNumber(), o); } }
		,	{ _YUID(O365_USER_ISRESOURCEACCOUNT),				[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(O365_USER_ISRESOURCEACCOUNT), u.GetIsResourceAccount(), o); } }
		,	{ _YUID(O365_USER_ONPREMISESDISTINGUISHEDNAME),		[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_ONPREMISESDISTINGUISHEDNAME), u.GetOnPremisesDistinguishedName(), o); } }
		,	{ _YUID(O365_USER_CREATIONTYPE),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(O365_USER_CREATIONTYPE), u.GetCreationType(), o); } }
		,	{ _YUID(O365_USER_IDENTITIES),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(O365_USER_IDENTITIES), ListSerializer<ObjectIdentity, ObjectIdentitySerializer>(u.GetIdentities()), o); } }
		,	{ _YUID(O365_USER_LASTPASSWORDCHANGEDATETIME),		[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_USER_LASTPASSWORDCHANGEDATETIME), u.GetLastPasswordChangeDateTime(), o); } }
		,	{ _YUID(O365_USER_LICENSEASSIGNMENTSTATES),			[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(O365_USER_LICENSEASSIGNMENTSTATES), ListSerializer<LicenseAssignmentState, LicenseAssignmentStateSerializer>(u.GetLicenseAssignmentStates()), o); } }
		,	{ _YUID(O365_USER_REFRESTOKENVALIDFROMDATETIME),	[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(O365_USER_REFRESTOKENVALIDFROMDATETIME), u.GetRefresTokenValidFromDateTime(), o); } }
		// Ytria fields
		//,	{ _YUID(YTRIA_LASTREQUESTEDDATETIME),				[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeTimeDate(_YUID(YTRIA_LASTREQUESTEDDATETIME), u.GetLastRequestTime(), o); } }
		//,	{ _YUID(YTRIA_FLAGS),								[](const BusinessUser& u, web::json::object& o)
		//	{
		//		ASSERT(!u.HasFlag(BusinessObject::CANCELED));  // Don't want to store canceled users
		//		JsonSerializeUtil::SerializeUint32(_YUID(YTRIA_FLAGS), u.GetFlags(), o);
		//	} }
		,	{ _YUID(YTRIA_USER_ARCHIVEFOLDERNAME),				[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(YTRIA_USER_ARCHIVEFOLDERNAME), u.GetArchiveFolderName(), o); } }

		,	{ _YUID(YTRIA_USER_MAILBOX_DELIVERTOMAILBOXANDFORWARD),	[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeBool(_YUID(YTRIA_USER_MAILBOX_DELIVERTOMAILBOXANDFORWARD), u.GetMailboxDeliverToMailboxAndForward(), o); } }
		,	{ _YUID(YTRIA_USER_MAILBOX_FORWARDINGSMTPADDRESS),		[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(YTRIA_USER_MAILBOX_FORWARDINGSMTPADDRESS), u.GetMailboxForwardingSmtpAddress(), o); } }
		,	{ _YUID(YTRIA_USER_MAILBOX_TYPE),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(YTRIA_USER_MAILBOX_TYPE), u.GetMailboxType(), o); } }
		,	{ _YUID(YTRIA_USER_MAILBOX_FORWARDINGADDRESS),			[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(YTRIA_USER_MAILBOX_FORWARDINGADDRESS), u.GetMailboxForwardingAddress(), o); } }
		,	{ _YUID(YTRIA_USER_MAILBOX_CAN_SEND_AS_USER),			[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(YTRIA_USER_MAILBOX_CAN_SEND_AS_USER), u.GetMailboxCanSendAsUser(), o); } }
		,	{ _YUID(YTRIA_USER_MAILBOX_CAN_SEND_ON_BEHALF),			[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeListString(_YUID(YTRIA_USER_MAILBOX_CAN_SEND_ON_BEHALF), u.GetMailboxCanSendOnBehalf(), o); } }

		,	{ _YUID(YTRIA_USER_ARCHIVEFOLDERNAMEERROR),			[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_USER_ARCHIVEFOLDERNAMEERROR), NullableSerializer<SapioError, SapioErrorSerializer>(u.GetArchiveFolderNameError()), o); } }
		,	{ _YUID(YTRIA_USER_MAILBOXSETTINGSERROR),			[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_USER_MAILBOXSETTINGSERROR), NullableSerializer<SapioError, SapioErrorSerializer>(u.GetMailboxSettingsError()), o); } }
		//,	{ _YUID(YTRIA_ERROR),								[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(u.GetError()), o); } }
		,	{ _YUID(YTRIA_DRIVE),								[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_DRIVE), NullableSerializer<BusinessDrive, DbDriveSerializer>(u.GetDrive()), o); } }
		,	{ _YUID(YTRIA_USER_MANAGER),						[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeString(_YUID(YTRIA_USER_MANAGER), u.GetManagerID(), o); } }
		,	{ _YUID(YTRIA_USER_MANAGER_ERROR),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_USER_MANAGER_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(u.GetManagerError()), o); } }
		,	{ _YUID(YTRIA_DRIVE_ERROR),							[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_DRIVE_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(u.GetManagerError()), o); } }
		,	{ _YUID(YTRIA_USER_MAILBOX_ERROR),					[](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_USER_MAILBOX_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(u.GetMailboxInfoError()), o); } }
		,	{ _YUID(YTRIA_USER_MAILBOX_RECIPIENTPERMISSIONS_ERROR), [](const BusinessUser& u, web::json::object& o) { JsonSerializeUtil::SerializeAny(_YUID(YTRIA_USER_MAILBOX_RECIPIENTPERMISSIONS_ERROR), NullableSerializer<SapioError, SapioErrorSerializer>(u.GetRecipientPermissionsError()), o); } }

		// request date per block
#define DATE_FIELD_FOR(_blockId_) \
		,	{ SqlCacheConfig<UserCache>::DataBlockDateFieldById((_blockId_)).c_str(), [](const BusinessUser& u, web::json::object& o) \
				{ \
					const auto& date = u.GetDataDate(_blockId_); \
					/* You must provide a date... */ \
					ASSERT(date.is_initialized()); \
					JsonSerializeUtil::SerializeTimeDate(SqlCacheConfig<UserCache>::DataBlockDateFieldById((_blockId_)), date ? date : u.GetLastRequestTime(), o); \
				} \
			} \

		DATE_FIELD_FOR(UBI::MIN)
		DATE_FIELD_FOR(UBI::LIST)
		DATE_FIELD_FOR(UBI::SYNCV1)
		DATE_FIELD_FOR(UBI::SYNCBETA)
		DATE_FIELD_FOR(UBI::MAILBOXSETTINGS)
		DATE_FIELD_FOR(UBI::ARCHIVEFOLDERNAME)
		DATE_FIELD_FOR(UBI::DRIVE)
		DATE_FIELD_FOR(UBI::MANAGER)
		DATE_FIELD_FOR(UBI::MAILBOX)
		DATE_FIELD_FOR(UBI::RECIPIENTPERMISSIONS)
		DATE_FIELD_FOR(UBI::ASSIGNEDLICENSES)

#undef DATE_FIELD_FOR
	};
}

const std::vector<PooledString>& DbUserSerializer::MemoryCacheRequestFields()
{
	static const std::vector<PooledString> g_MemoryCacheRequestFields
	{
		_YUID(O365_ID),
		_YUID(O365_USER_DISPLAYNAME),
		_YUID(O365_USER_MAIL),
		_YUID(O365_USER_USERPRINCIPALNAME),
		_YUID(O365_USER_USERTYPE),
	};

	return g_MemoryCacheRequestFields;
}

const std::vector<PooledString>& DbUserSerializer::AssignedLicensesRequestFields()
{
	static const std::vector<PooledString> g_AssignedLicensesRequestFields
	{
		_YUID(O365_USER_ASSIGNEDLICENSES),
	};

	return g_AssignedLicensesRequestFields;
}

const std::vector<PooledString>& DbUserSerializer::NotInMemoryCacheListRequestFields()
{
	static const std::vector<PooledString> g_NotCachedListRequestFields
	{
		_YUID(O365_USER_PASSWORDPROFILE),
		_YUID(O365_USER_ACCOUNTENABLED),
		//_YUID(O365_USER_ASSIGNEDLICENSES),
		_YUID(O365_USER_ASSIGNEDPLANS),
		_YUID(O365_USER_BUSINESSPHONES),
		_YUID(O365_USER_CITY),
		_YUID(O365_USER_COMPANYNAME),
		_YUID(O365_USER_COUNTRY),
		_YUID(O365_USER_DEPARTMENT),
		_YUID(O365_USER_GIVENNAME),
		_YUID(O365_USER_IMADDRESSES),
		_YUID(O365_USER_JOBTITLE),
		_YUID(O365_USER_MAILNICKNAME),
		_YUID(O365_USER_MOBILEPHONE),
		_YUID(O365_USER_OFFICELOCATION),
		_YUID(O365_USER_ONPREMISESIMMUTABLEID),
		_YUID(O365_USER_ONPREMISESLASTSYNCDATETIME),
		_YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER),
		_YUID(O365_USER_ONPREMISESSYNCENABLED),
		_YUID(O365_USER_PASSWORDPOLICIES),
		_YUID(O365_USER_POSTALCODE),
		_YUID(O365_USER_PREFERREDLANGUAGE),
		_YUID(O365_USER_PROVISIONEDPLANS),
		_YUID(O365_USER_PROXYADDRESSES),
		_YUID(O365_USER_STATE),
		_YUID(O365_USER_STREETADDRESS),
		_YUID(O365_USER_SURNAME),
		_YUID(O365_USER_USAGELOCATION),
		_YUID(O365_USER_AGEGROUP),
		_YUID(O365_USER_CREATEDDATETIME),
		_YUID(O365_USER_CONSENTPROVIDEDFORMINOR),
		_YUID(O365_USER_LEGALAGEGROUPCLASSIFICATION),
		_YUID(O365_USER_ONPREMISESDOMAINNAME),
		_YUID(O365_USER_ONPREMISESSAMACCOUNTNAME),
		_YUID(O365_USER_ONPREMISESUSERPRINCIPALNAME),
		_YUID(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME),
		_YUID(O365_USER_ONPREMISESPROVISIONINGERRORS),
		_YUID(O365_USER_EMPLOYEEID),
		_YUID(O365_USER_FAXNUMBER),
		_YUID(O365_USER_ISRESOURCEACCOUNT),
		_YUID(O365_USER_ONPREMISESDISTINGUISHEDNAME),
		_YUID(O365_USER_OTHERMAILS),
		_YUID(O365_USER_SHOWINADDRESSLIST),
		_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES),
		_YUID(O365_USER_CREATIONTYPE),
		_YUID(O365_USER_IDENTITIES),
		_YUID(O365_USER_LASTPASSWORDCHANGEDATETIME),
		_YUID(O365_USER_LICENSEASSIGNMENTSTATES),
	};

	return g_NotCachedListRequestFields;
}

const std::vector<PooledString>& DbUserSerializer::SyncOnlyRequestFields_v1()
{
	static const std::vector<PooledString> g_SyncOnlyRequestFieldsV1
	{
		_YUID(O365_USER_ABOUTME),
		_YUID(O365_USER_BIRTHDAY),
		_YUID(O365_USER_HIREDATE),
		_YUID(O365_USER_INTERESTS),
		_YUID(O365_USER_MYSITE),
		_YUID(O365_USER_PASTPROJECTS),
		_YUID(O365_USER_PREFERREDNAME),
		_YUID(O365_USER_RESPONSIBILITIES),
		_YUID(O365_USER_SCHOOLS),
		_YUID(O365_USER_SKILLS),
	};

	return g_SyncOnlyRequestFieldsV1;
}

const std::vector<PooledString>& DbUserSerializer::SyncOnlyRequestFields_beta()
{
	static const std::vector<PooledString> g_SyncOnlyRequestFieldsBeta
	{
		/* Beta API */
		_YUID(O365_USER_PREFERREDDATALOCATION),
		_YUID(O365_USER_EXTERNALUSERSTATUS),
		_YUID(O365_USER_EXTERNALUSERSTATUSCHANGEDON),
	};

	return g_SyncOnlyRequestFieldsBeta;
}

const std::vector<PooledString>& DbUserSerializer::SyncOnlyRequestFields_all()
{
	static const std::vector<PooledString> g_SyncOnlyRequestFieldsAll = concat(SyncOnlyRequestFields_v1(), SyncOnlyRequestFields_beta());

	return g_SyncOnlyRequestFieldsAll;
}

const std::vector<PooledString>& DbUserSerializer::MailboxSettingsRequestFields()
{
	static const std::vector<PooledString> g_MailboxSettingsRequestFields
	{
		_YUID(O365_USER_MAILBOXSETTINGS),
		_YUID(YTRIA_USER_MAILBOXSETTINGSERROR),
	};

	return g_MailboxSettingsRequestFields;
}

const std::vector<PooledString>& DbUserSerializer::ArchiveFolderNameRequestFields()
{
	static const std::vector<PooledString> g_ArchiveFolderNameRequestFields
	{
		_YUID(YTRIA_USER_ARCHIVEFOLDERNAME),
		_YUID(YTRIA_USER_ARCHIVEFOLDERNAMEERROR),
	};

	return g_ArchiveFolderNameRequestFields;
}

const std::vector<PooledString>& DbUserSerializer::DriveRequestFields()
{
	static const std::vector<PooledString> g_DriveRequestFields
	{
		_YUID(YTRIA_DRIVE),
		_YUID(YTRIA_DRIVE_ERROR), //FIXME: Not handled yet
	};

	return g_DriveRequestFields;
}

const std::vector<PooledString>& DbUserSerializer::ManagerRequestFields()
{
	static const std::vector<PooledString> g_ManagerRequestFields
	{
		_YUID(YTRIA_USER_MANAGER),
		_YUID(YTRIA_USER_MANAGER_ERROR),
	};

	return g_ManagerRequestFields;
}

const std::vector<PooledString>& DbUserSerializer::MailboxRequestFields()
{
	static const std::vector<PooledString> g_MailboxRequestFields
	{
		_YUID(YTRIA_USER_MAILBOX_DELIVERTOMAILBOXANDFORWARD),
		_YUID(YTRIA_USER_MAILBOX_FORWARDINGSMTPADDRESS),
		_YUID(YTRIA_USER_MAILBOX_TYPE),
		_YUID(YTRIA_USER_MAILBOX_FORWARDINGADDRESS),
		_YUID(YTRIA_USER_MAILBOX_CAN_SEND_ON_BEHALF),
		_YUID(YTRIA_USER_MAILBOX_ERROR),
	};

	return g_MailboxRequestFields;
}


const std::vector<PooledString>& DbUserSerializer::RecipientPermissionsRequestFields()
{
	static const std::vector<PooledString> g_RecipientPermissionsRequestFields
	{
		_YUID(YTRIA_USER_MAILBOX_CAN_SEND_AS_USER),
		_YUID(YTRIA_USER_MAILBOX_RECIPIENTPERMISSIONS_ERROR),
	};

	return g_RecipientPermissionsRequestFields;
}

DbUserSerializer::DbUserSerializer(const BusinessUser& p_User, const std::vector<PooledString>& p_Fields)
	: m_User(p_User)
	, m_Fields(p_Fields)
{
	// No duplicates allowed. Eventually consider automatically removing duplicates.
	ASSERT(std::set<PooledString>(m_Fields.begin(), m_Fields.end()).size() == m_Fields.size());
}

DbUserSerializer::DbUserSerializer(const BusinessUser& p_User, const UBI p_BlockId)
	: DbUserSerializer(p_User, SqlCacheConfig<UserCache>::DataBlockFieldsById(p_BlockId))
{
	m_DataDateField = SqlCacheConfig<UserCache>::DataBlockDateFieldById(p_BlockId);
}

void DbUserSerializer::Serialize()
{
	m_Json = web::json::value::object();
	auto& obj = m_Json.as_object();

	for (const auto& field : m_Fields)
	{
		ASSERT(g_Serializers.end() != g_Serializers.find(field));
		g_Serializers[field](m_User, obj);
	}

	if (!m_DataDateField.IsEmpty())
	{
		ASSERT(g_Serializers.end() != g_Serializers.find(m_DataDateField));
		g_Serializers[m_DataDateField](m_User, obj);
	}
}
