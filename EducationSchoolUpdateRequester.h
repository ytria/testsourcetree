#pragma once

#include "IRequester.h"
#include "EducationSchool.h"

class EducationSchoolUpdateRequester : public IRequester
{
public:
	EducationSchoolUpdateRequester(const wstring& p_SchoolId, const web::json::value& p_SchoolChanges);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const HttpResultWithError& GetResult() const;

private:
	std::shared_ptr<HttpResultWithError> m_Result;

	wstring m_SchoolId;
	web::json::value m_SchoolChanges;
};

