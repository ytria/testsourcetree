#include "DriveRequester.h"
#include "DriveDeserializer.h"
#include "MSGraphSession.h"
#include "MultiObjectsPageRequestLogger.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

DriveRequester::DriveRequester(DriveSource p_Source, PooledString p_Id, const std::shared_ptr<IRequestLogger>& p_Logger, const wstring& p_SelectProperties/* = _YTEXT("")*/)
	: m_Source(p_Source)
	, m_Id(p_Id)
	, m_Logger(p_Logger)
	, m_SelectProperties(p_SelectProperties)
{
}

TaskWrapper<void> DriveRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<DriveDeserializer>();

	web::uri_builder uri;
	if (m_Source == DriveRequester::DriveSource::Drive)
	{
		uri.append_path(_YTEXT("drives"));
		uri.append_path(m_Id);
	}
	else
	{
		switch (m_Source)
		{
		case DriveRequester::DriveSource::User:
			uri.append_path(_YTEXT("users"));
			break;
		case DriveRequester::DriveSource::Group:
			uri.append_path(_YTEXT("groups"));
			break;
		case DriveRequester::DriveSource::Site:
			uri.append_path(_YTEXT("sites"));
			break;
		default:
			ASSERT(false);
			break;
		}
		uri.append_path(m_Id);
		uri.append_path(_YTEXT("drive"));
	}

	if (!m_SelectProperties.empty())
		uri.append_query(_YTEXT("$select"), m_SelectProperties);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, uri.to_uri(), false, m_Logger, httpLogger, p_TaskData);
}

const BusinessDrive& DriveRequester::GetData() const
{
	return m_Deserializer->GetData();
}
