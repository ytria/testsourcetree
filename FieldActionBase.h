#pragma once

#include "IRestorableGridAction.h"
#include "GridBackendField.h"

#include <vector>

class FieldActionBase : public IRestorableGridAction
{
public:
    using row_primary_key_t = std::vector<GridBackendField>;

public:
    FieldActionBase(O365Grid& p_Grid, GridBackendRow* p_Row, GridBackendColumn* p_Column);

protected:
    const row_primary_key_t& GetPrimaryKey() const;
    UINT GetColumnId() const;

private:
    row_primary_key_t m_PrimaryKey;
    UINT m_ColId;
};

