#include "SessionIdsEmitter.h"
#include "SessionTypes.h"

map<SessionIdentifier, wstring> SessionIdsEmitter::g_Ids;

SessionIdsEmitter::SessionIdsEmitter(const SessionIdentifier& p_Identifier)
	:m_Identifier(p_Identifier)
{
}

wstring SessionIdsEmitter::GetId() const
{
	wstring id;
	auto itt = g_Ids.find(m_Identifier);
	if (itt != g_Ids.end())
	{
		id = itt->second;
	}
	else
	{
		id = CreateId();
		g_Ids[m_Identifier] = id;
	}
	return id;
}

wstring SessionIdsEmitter::CreateId() const
{
	static int g_IdCounter = 0;
	wstring id;

	wstring sessionType;
	if (!m_Identifier.m_SessionType.empty())
	{
		if (m_Identifier.m_SessionType == SessionTypes::g_StandardSession)
			sessionType = _YTEXT("S");
		else if (m_Identifier.m_SessionType == SessionTypes::g_AdvancedSession)
			sessionType = _YTEXT("A");
		else if (m_Identifier.m_SessionType == SessionTypes::g_UltraAdmin)
			sessionType = _YTEXT("U");
		else if (m_Identifier.m_SessionType == SessionTypes::g_Role)
			sessionType = _YTEXT("R");
		else if (m_Identifier.m_SessionType == SessionTypes::g_ElevatedSession)
			sessionType = _YTEXT("E");
		else if (m_Identifier.m_SessionType == SessionTypes::g_PartnerAdvancedSession)
			sessionType = _YTEXT("P");
		else if (m_Identifier.m_SessionType == SessionTypes::g_PartnerElevatedSession)
			sessionType = _YTEXT("W");
		else
			ASSERT(false);
	}

	id += sessionType;
	if (!id.empty())
		id += _YTEXT(":");
	id += std::to_wstring(g_IdCounter++);

	return id;
}
