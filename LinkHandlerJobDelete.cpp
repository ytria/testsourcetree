#include "LinkHandlerJobDelete.h"

#include "MainFrame.h"

void LinkHandlerJobDelete::Handle(YBrowserLink& p_Link) const
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame && mainFrame->GetAutomationWizard() != nullptr);
	if (nullptr != mainFrame && mainFrame->GetAutomationWizard() != nullptr)
	{
		auto scriptID = Str::getINT64FromString(p_Link.GetUrl().path().substr(1));// .substr(1): path starts with '/'
		auto s = mainFrame->GetAutomationWizard()->GetScript(scriptID);
		ASSERT(s.HasID());
		if (s.HasID())
		{
			YCodeJockMessageBox dlg(mainFrame,
				DlgMessageBox::eIcon_Question,
				_T("Confirm"),
				_YFORMAT(L"Delete Job '%s'?", s.m_Title.c_str()),
				_T("Note: any related scheduled tasks will be deleted from Windows Task Scheduler. This operation cannot be undone."),
				{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
			if (IDOK == dlg.DoModal())
				mainFrame->GetAutomationWizard()->RemoveScript(s);
		}
		else
			YCodeJockMessageBox (mainFrame,
				DlgMessageBox::eIcon_ExclamationWarning,
				_T("Error"),
				_T("Script not found"),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } } ).DoModal();
	}
}