#pragma once

enum class FileExistsAction
{
    Append,
    Overwrite,
    Skip
};
