#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class LocaleInfoDeserializer : public JsonObjectDeserializer, public Encapsulate<LocaleInfo>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

