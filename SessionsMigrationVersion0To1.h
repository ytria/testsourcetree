#pragma once

#include "ISessionsMigrationVersion.h"

class SessionsMigrationVersion0To1 : public ISessionsMigrationVersion
{
public:
	void Build() override;
	VersionSourceTarget GetVersionInfo() const override;

private:
	bool Exec();
	bool CreateTables();
	bool ImportData();
};

