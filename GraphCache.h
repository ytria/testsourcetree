#pragma once

#include "CachedUser.h"
#include "CachedGroup.h"
#include "CachedOrgContact.h"
#include "CachedSite.h"
#include "CommandInfo.h"

#include "BusinessUser.h"
#include "BusinessGroup.h"
#include "BusinessOrgContact.h"
#include "BusinessSite.h"
#include "BusinessSubscribedSku.h"
#include "CachedObjectsSqlEngine.h"
#include "CachedSubscribedSku.h"
#include "IPageRequestLogger.h"
#include "IRequestLogger.h"
#include "ODataFilter.h"
#include "Organization.h"
#include "PersistentSession.h"
#include "SqlCacheConfig.h"
#include "YSafeTaskWrapper.h"
#include "YtriaTaskData.h"

#include <atomic>

class Sapio365Session;
class IPropertySetBuilder;

class GraphCache
{
public:
	GraphCache(std::shared_ptr<Sapio365Session> p_MSGraphSession);

	void SetPersistentSession(const PersistentSession& p_PersistentSession);
	bool HasUsersDeltaCapabilities() const;
	bool HasGroupsDeltaCapabilities() const;

	void InitUserCount(int32_t p_Count);
	int32_t GetUserCount() const;

    void Clear();
	void ClearObjectLists();
	void ClearOrgAndConnectedUser();
	void ClearUsers();
	void ClearGroups();
	void ClearOrgContacts();
	void ClearSites();
	void ClearDeletedUsers();
	void ClearDeletedGroups();

	bool IsUsersDeltaSyncAvailable() const;
	bool IsGroupsDeltaSyncAvailable() const;

	bool GetAutoLoadOnPrem() const;
	bool GetAskedAutoLoadOnPrem() const;

	bool GetUseOnPrem() const;
	bool GetDontAskAgainUseOnPrem() const;

	void SetHybridCheckSuccess(bool p_Value);
	bool GetHybridCheckSuccess() const;

	bool GetUseConsistencyGuid() const;

	wstring GetAADComputerName() const;
	wstring GetADDSServer() const;
	wstring GetADDSUsername() const;
	wstring GetADDSPassword() const;

	void Sync();
	void SetSyncError(const wstring& p_Error);
	const wstring& GetSyncError() const;

	// p_TaskData used for cancel
	vector<BusinessUser> GetSqlCachedUsers(HWND p_Hwnd, const std::vector<UBI>& p_RequiredBlocks, YtriaTaskData p_TaskData = YtriaTaskData());
	vector<BusinessUser> GetSqlCachedUsers(HWND p_Hwnd, const O365IdsContainer& p_IDs, bool p_EnsureOrder, YtriaTaskData p_TaskData = YtriaTaskData());
	vector<BusinessUser> GetSqlCachedUsers(HWND p_Hwnd, const O365IdsContainer& p_IDs, const std::vector<UBI>& p_RequiredBlocks, bool p_EnsureOrder, YtriaTaskData p_TaskData = YtriaTaskData());

	// p_TaskData used for cancel
	vector<BusinessGroup> GetSqlCachedGroups(HWND p_Hwnd, const std::vector<GBI>& p_RequiredBlocks, YtriaTaskData p_TaskData = YtriaTaskData());
	vector<BusinessGroup> GetSqlCachedGroups(HWND p_Hwnd, const O365IdsContainer& p_IDs, bool p_EnsureOrder, YtriaTaskData p_TaskData = YtriaTaskData());
	vector<BusinessGroup> GetSqlCachedGroups(HWND p_Hwnd, const O365IdsContainer& p_IDs, const std::vector<GBI>& p_RequiredBlocks, bool p_EnsureOrder, YtriaTaskData p_TaskData = YtriaTaskData());

	bool IsSqlUsersCacheFull(); // true if GetSqlUsersCacheFullListSize() > -1
	bool IsSqlGroupsCacheFull(); // true if GetSqlGroupsCacheFullListSize() > -1
	int GetSqlUsersCacheFullListSize(); // -1 if !IsSqlUsersCacheFull()
	int GetSqlGroupsCacheFullListSize(); // -1 if !IsSqlGroupsCacheFull()
	const boost::YOpt<YTimeDate> GetSqlUsersCacheLastFullRefreshDate();
	const boost::YOpt<YTimeDate> GetSqlGroupsCacheLastFullRefreshDate();
	int CountSqlUsersMissingRequirements(const std::vector<UBI>& p_RequiredBlocks);
	int CountSqlGroupsMissingRequirements(const std::vector<GBI>& p_RequiredBlocks);

	const std::shared_ptr<CachedObjectsSqlEngine>& GetSqlCache() const;
	bool IsSqlUsersCacheLoaded() const;
	bool IsSqlGroupsCacheLoaded() const;

	void LoadFromSqlUsersCacheIfNotAlreadyInMemory(HWND p_Hwnd, const vector<PooledString>& p_Ids);
	void LoadFromSqlGroupsCacheIfNotAlreadyInMemory(HWND p_Hwnd, const vector<PooledString>& p_Ids);

	bool IsSqlUsersCacheExpired() const;
	bool IsSqlGroupsCacheExpired() const;

	const boost::YOpt<vector<uint8_t>>&	GetCachedProfilePicture() const;
	const boost::YOpt<BusinessUser>&	GetCachedConnectedUser() const;

    size_t GetUserCacheSize() const;
    size_t GetGroupCacheSize() const;
	size_t GetOrgContactCacheSize() const;
    size_t GetSiteCacheSize() const;
	size_t GetDeletedUserCacheSize() const;
	size_t GetDeletedGroupCacheSize() const;
    
	vector<CachedUser>			GetCachedUsers() const;
	vector<CachedUser>			GetCachedUsers(const O365IdsContainer& p_Ids) const;
    vector<CachedGroup>			GetCachedGroups() const;
	vector<CachedGroup>			GetCachedGroups(const O365IdsContainer& p_Ids) const;
	vector<CachedOrgContact>	GetCachedOrgContacts() const;
    map<PooledString, CachedSubscribedSku>	GetCachedSubscribedSkus() const;
    vector<CachedSite>			GetCachedSites() const;
	vector<CachedUser>			GetCachedDeletedUsers() const;
	vector<CachedGroup>			GetCachedDeletedGroups() const;

	wstring GetUserContextualInfo(const wstring& p_UserId) const;
	wstring GetGroupContextualInfo(const wstring& p_GroupId) const;
	wstring GetSiteContextualInfo(const wstring& p_SiteId) const;

	CachedUser	GetUserInCache(const PooledString& p_Id, bool p_AllowFallbackToDeletedUser) const;
	CachedUser	GetUserInCacheByEmail(const PooledString& p_Email, bool p_AllowFallbackToDeletedUser) const;
	CachedUser	GetDeletedUserInCache(const PooledString& p_Id) const;
	CachedUser	GetDeletedUserInCacheByEmail(const PooledString& p_Email) const;
	CachedGroup	GetGroupInCache(const PooledString& p_Id) const;
	CachedGroup	GetDeletedGroupInCache(const PooledString& p_Id) const;
	CachedSite	GetSiteInCache(const PooledString& p_Id) const;

	BusinessOrgContact	GetCachedOrgContact(const PooledString& p_Id) const;

    void	SetSiteHierarchyDisplayName(const PooledString& p_Id, const PooledString& p_HierarchyDisplayName);
    wstring GetSiteHierarchyDisplayName(const PooledString& p_Id) const;

	void AddUser(const BusinessUser& p_User, bool p_SaveInSqlCache = false, const std::vector<UBI>& p_Blocks = {});
    void AddGroup(const BusinessGroup& p_Group, bool p_SaveInSqlCache = false, const std::vector<GBI>& p_Blocks = {});
    void AddSite(const BusinessSite& p_Site);
    void Add(const BusinessOrgContact& p_OrgContact);

	// When deleting an object, we must remove it from the cache
	void Remove(const User& p_User);
	void Remove(const Group& p_Group);
	void RemoveDeletedUser(const User& p_DeletedUser);
	void RemoveDeletedGroup(const Group& p_DeletedGroup);

	void AddUsers(vector<BusinessUser>& p_Users, bool p_SaveInSqlCache = false, const std::vector<UBI>& p_Blocks = {}, const boost::YOpt<YTimeDate>& p_LastFullRefreshDate = boost::none, bool p_ShouldClearSql = false);
    void AddGroups(vector<BusinessGroup>& p_Groups, bool p_SaveInSqlCache = false, const std::vector<GBI>& p_Blocks = {}, const boost::YOpt<YTimeDate>& p_LastFullRefreshDate = boost::none, bool p_ShouldClearSql = false);
    void AddSites(const vector<BusinessSite>& p_Sites);
	void Add(const vector<BusinessOrgContact>& p_OrgContacts);
    void Add(const vector<BusinessSubscribedSku>& p_SubscribedSkus);
	void AddDeletedUsers(const vector<BusinessUser>& p_DeletedUsers);
	void AddDeletedGroups(const vector<BusinessGroup>& p_DeletedGroups);

	// If Business user if not found in cache (using Id or email - the first non-empty), it will be cached.
	// Note: definitely slower by email (map indexed on id).
	BusinessUser&		SyncUncachedUser(BusinessUser& p_User, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData, bool p_AllowFallbackToDeletedUser);

	// If Business object if not found in cache (using Id), it will be cached.
	BusinessGroup&		SyncUncachedGroup(BusinessGroup& p_Group, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData, bool p_AllowOverrideLoggerMessage = true);
	BusinessOrgContact& SyncUncachedOrgContact(BusinessOrgContact& p_OrgContact, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
	BusinessSite& SyncUncachedSite(BusinessSite& p_Site, const std::shared_ptr<IRequestLogger>& p_Logger, YtriaTaskData p_TaskData);

    void SyncUncachedUsers(vector<BusinessUser>& p_Users, YtriaTaskData p_TaskData, bool p_AllowFallbackToDeletedUser);

	enum class PreferredMode
	{
		DELTASYNC,
		INITDELTA,
		LIST
	};
	// Update sql and memory caches and return full users list
	template<class UserObjectType>
	TaskWrapper<std::vector<UserObjectType>> GetUpToDateUsers(const O365IdsContainer& p_UserIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink = nullptr);

	TaskWrapper<std::vector<BusinessUser>> SyncUsersFromDeltaLink(YtriaTaskData p_TaskData, const std::shared_ptr<IPageRequestLogger>& p_Logger, std::shared_ptr<wstring> p_OverrideDeltaLink = nullptr, bool p_IsAllUsers = true);
	TaskWrapper<std::vector<BusinessUser>> GetPrefilteredUsers(YtriaTaskData p_TaskData, const ODataFilter& p_Filter, const std::shared_ptr<IPageRequestLogger>& p_Logger);
	TaskWrapper<std::vector<BusinessUser>> InitPartialUsersDelta(YtriaTaskData p_TaskData, const O365IdsContainer& p_UserIds, std::shared_ptr<wstring> p_OutputDeltaLink, const std::shared_ptr<IPageRequestLogger>& p_Logger);

	// Update sql and memory caches and return full groups list
	template<class GroupObjectType>
	TaskWrapper<std::vector<GroupObjectType>> GetUpToDateGroups(const O365IdsContainer& p_GroupIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink = nullptr);

	TaskWrapper<std::vector<BusinessGroup>> SyncGroupsFromDeltaLink(YtriaTaskData p_TaskData, const std::shared_ptr<IPageRequestLogger>& p_Logger, std::shared_ptr<wstring> p_OverrideDeltaLink = nullptr, bool p_IsAllGroups = true);
	TaskWrapper<std::vector<BusinessGroup>> GetPrefilteredGroups(YtriaTaskData p_TaskData, const ODataFilter& p_Filter, const std::shared_ptr<IPageRequestLogger>& p_Logger);
	TaskWrapper<std::vector<BusinessGroup>> InitPartialGroupsDelta(YtriaTaskData p_TaskData, const O365IdsContainer& p_GroupIds, std::shared_ptr<wstring> p_OutputDeltaLink, const std::shared_ptr<IPageRequestLogger>& p_Logger);
    
	TaskWrapper<std::vector<CachedOrgContact>> SyncAllOrgContacts(const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
	TaskWrapper<std::vector<CachedOrgContact>> SyncAllOrgContactsOnce(const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData);
	
	TaskWrapper<std::vector<CachedUser>> SyncAllDeletedUsers(YtriaTaskData p_TaskData);
	TaskWrapper<std::vector<CachedUser>> SyncAllDeletedUsersOnce(YtriaTaskData p_TaskData);

	TaskWrapper<std::vector<CachedGroup>> SyncAllDeletedGroups(YtriaTaskData p_TaskData);
	TaskWrapper<std::vector<CachedGroup>> SyncAllDeletedGroupsOnce(YtriaTaskData p_TaskData);

	void UpdateUsersDeltaLinkFromSql();
	void UpdateGroupsDeltaLinkFromSql();
	std::shared_ptr<wstring> GetUsersDeltaLink() const;
	std::shared_ptr<wstring> GetGroupsDeltaLink() const;
	void ClearUsersDeltaLink();
	void ClearGroupsDeltaLink();

	void SetAllOrgContactsSynced();
	void SetAllDeletedUsersSynced();
	void SetAllDeletedGroupsSynced();

	bool IsAllUsersSynced();
	bool IsAllGroupsSynced();
	bool IsAllOrgContactsSynced();
	bool IsAllDeletedUsersSynced();
	bool IsAllDeletedGroupsSynced();

	void						SetOrganization(const Organization& p_Organization);
	TaskWrapper<Organization>	GetOrganization(YtriaTaskData p_Task) const;
	const boost::YOpt<Organization>&	GetCachedOrganization() const;

	void						SetRBACOrganization(const boost::YOpt<Organization>& p_Organization);
	// If current session is using RBAC, it returns the organization of the RBAC credentials tenant.
	// Else, it returns GetCachedOrganization().
	const boost::YOpt<Organization>& GetRBACOrganization() const;

	void StoreGroupMembers(const BusinessGroup& p_Group);
	bool GetGroupMembers(BusinessGroup& p_Group);

	std::map<PooledString, int32_t> GetSkuCounters() const; // Sku ID to count
	bool IsHandleSkuCounters() const;
	void InitSqlCache();

	void SaveUsersInSqlCache(vector<BusinessUser>& p_Objs, const std::vector<UBI>& p_Blocks, const boost::YOpt<YTimeDate>& p_LastFullRefreshDate, bool p_ShouldClearSql);
	void SaveGroupsInSqlCache(vector<BusinessGroup>& p_Objs, const std::vector<GBI>& p_Blocks, const boost::YOpt<YTimeDate>& p_LastFullRefreshDate, bool p_ShouldClearSql);

protected:
	void setSkuCounters(std::map<PooledString, int32_t>&& p_SkuCunters);

private:
	void saveUsersDeltaLink(const wstring& p_DeltaLink, const std::shared_ptr<CachedObjectsSqlEngine>& p_SqlCache2);
	void saveGroupsDeltaLink(const wstring& p_DeltaLink, const std::shared_ptr<CachedObjectsSqlEngine>& p_SqlCache2);

	void setAllUsersSynced();
	void setAllGroupsSynced();

	// p_TaskData used for cancel
	vector<BusinessUser> getSqlCachedUsers(HWND p_Hwnd, const std::vector<UBI>& p_RequiredBlocks, const O365IdsContainer& p_IDs, bool p_StoreInGraphCache, bool p_EnsureOrder, YtriaTaskData p_TaskData = YtriaTaskData());
	vector<BusinessGroup> getSqlCachedGroups(HWND p_Hwnd, const std::vector<GBI>& p_RequiredBlocks, const O365IdsContainer& p_IDs, bool p_StoreInGraphCache, bool p_EnsureOrder, YtriaTaskData p_TaskData = YtriaTaskData());

	// As GraphCache is a shared_ptr in Sapio365Session, we can't store the session's shared_ptr
	// without creating a circular dependency 
	//std::shared_ptr<Sapio365Session> m_Sapio365Session;
	std::weak_ptr<Sapio365Session> m_Sapio365Session;
	std::shared_ptr<Sapio365Session> getSapio365Session() const;

	TaskWrapper<BusinessUser>	getConnectedUser(YtriaTaskData p_Task);
	TaskWrapper<vector<uint8_t>>	getProfilePicture(YtriaTaskData p_Task);

    void updateUser(const User& p_User);
    void updateUser(const BusinessUser& p_User);
    void updateGroup(const Group& p_Group);
    void updateGroup(const BusinessGroup& p_Group);
	void updateOrgContact(const BusinessOrgContact& p_OrgContact);
    void updateSite(const BusinessSite& p_Site);
	void updateDeletedUser(const BusinessUser& p_User);
	void updateDeletedGroup(const BusinessGroup& p_Group);

	void addUsersAndCountSkus(vector<BusinessUser>& p_Users, bool p_SaveInSqlCache = false, const std::vector<UBI>& p_Blocks = {}, const boost::YOpt<YTimeDate>& p_LastFullRefreshDate = boost::none, bool p_ShouldClearSql = false);

	web::json::value mergeBodies(const std::map<wstring, wstring>& p_Bodies);

    std::map<PooledString, CachedUser> m_CachedUsers;
    mutable std::mutex m_CachedUsersLock;

    std::map<PooledString, CachedGroup> m_CachedGroups;
    mutable std::mutex m_CachedGroupsLock;

	std::map<PooledString, CachedOrgContact> m_CachedOrgContacts;
	mutable std::mutex m_CachedOrgContactsLock;

    std::map<PooledString, CachedSubscribedSku> m_CachedSubscribedSkus;
    mutable std::mutex m_CachedSubscribedSkusLock;

    std::map<PooledString, CachedSite> m_CachedSites;
    mutable std::mutex m_CachedSitesLock;

	std::map<PooledString, CachedUser> m_CachedDeletedUsers;
	mutable std::mutex m_CachedDeletedUsersLock;

	std::map<PooledString, CachedGroup> m_CachedDeletedGroups;
	mutable std::mutex m_CachedDeletedGroupsLock;

    std::atomic_bool m_SyncedAllUsers;
    std::atomic_bool m_SyncedAllGroups;
	std::atomic_bool m_SyncedAllOrgContacts;
	std::atomic_bool m_SyncedAllDeletedUsers;
	std::atomic_bool m_SyncedAllDeletedGroups;

	// Connected user and related stuff
	boost::YOpt<BusinessUser>		m_ConnectedUser;
	boost::YOpt<vector<uint8_t>>	m_ConnectedUserProfilePicture;
	boost::YOpt<Organization>		m_Organization;
	mutable boost::YOpt<Organization>		m_RBACOrganization;

	boost::YOpt<std::map<PooledString, int32_t>> m_SkuCounters;
	int32_t m_UserCount = 0;

	wstring m_SyncError;
	bool m_HybridCheckSuccess;

	shared_ptr<wstring> m_UsersDeltaLink;
	shared_ptr<wstring> m_GroupsDeltaLink;

	struct GroupMembers
	{
		std::vector<PooledString> m_Groups;
		std::vector<PooledString> m_OrgContacts;
		std::vector<PooledString> m_Users;

		boost::YOpt<SapioError> m_Error;
	};
	std::map<PooledString, GroupMembers> m_GroupMembers;
	mutable std::mutex m_GroupMembersLock;

	std::shared_ptr<CachedObjectsSqlEngine> m_SqlCache;
	
	std::atomic_bool m_SqlUsersCacheLoaded;
	std::atomic_bool m_SqlGroupsCacheLoaded;

	PersistentSession m_PersistentSession;
};

// Definitions are put in cpp for header clarity, hence the 'extern template'
extern template
TaskWrapper<std::vector<CachedUser>> GraphCache::GetUpToDateUsers<CachedUser>(const O365IdsContainer& p_UserIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/);
extern template
TaskWrapper<std::vector<BusinessUser>> GraphCache::GetUpToDateUsers<BusinessUser>(const O365IdsContainer& p_UserIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/);

extern template
TaskWrapper<std::vector<CachedGroup>> GraphCache::GetUpToDateGroups<CachedGroup>(const O365IdsContainer& p_GroupIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/);
extern template
TaskWrapper<std::vector<BusinessGroup>> GraphCache::GetUpToDateGroups<BusinessGroup>(const O365IdsContainer& p_GroupIds, const std::shared_ptr<IPageRequestLogger>& p_Logger, PreferredMode p_PreferredMode, YtriaTaskData p_TaskData, const std::shared_ptr<wstring> p_OverrideDeltaLink/* = nullptr*/);
