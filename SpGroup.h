#pragma once

#include "BusinessObject.h"

namespace Sp
{
    class Group : public BusinessObject
    {
    public:
        boost::YOpt<bool> AllowMembersEditMembership;
        boost::YOpt<bool> AllowRequestToJoinLeave;
        boost::YOpt<bool> AutoAcceptRequestToJoinLeave;
        boost::YOpt<bool> CanCurrentUserEditMembership;
        boost::YOpt<bool> CanCurrentUserManageGroup;
        boost::YOpt<bool> CanCurrentUserViewMembership;
        boost::YOpt<PooledString> Description;
        boost::YOpt<int32_t> Id;
        boost::YOpt<bool> IsHiddenInUI;
        boost::YOpt<PooledString> LoginName;
        boost::YOpt<bool> OnlyAllowMembersViewMembership;
        boost::YOpt<PooledString> OwnerTitle;
        boost::YOpt<PooledString> RequestToJoinLeaveEmailSetting;
        boost::YOpt<int32_t> PrincipalType;
        boost::YOpt<PooledString> Title;

    private:
        RTTR_ENABLE(BusinessObject)
        RTTR_REGISTRATION_FRIEND
    };
}

