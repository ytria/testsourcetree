#include "LinkHandlerDrive.h"

#include "Command.h"
#include "CommandDispatcher.h"
#include "CommandInfo.h"
#include "FrameDriveItems.h"
#include "ModuleDrive.h"
#include "Office365Admin.h"

void LinkHandlerDrive::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	auto task = MFCUtil::isALTDOWN() && CRMpipe().IsFeatureSandboxed() ? Command::ModuleTask::SearchMe : Command::ModuleTask::ListMe;
	CommandInfo info;
    info.SetOrigin(Origin::User);
	p_Link.ENABLE_FREE_ACCESS();// free access

	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Drive, task, info, { nullptr, g_ActionNameMyDataDrive, nullptr, p_Link.GetAutomationAction() }));
}

bool LinkHandlerDrive::IsAllowedWithAJLLicense() const
{
	return true; // only MyData is allowed
}