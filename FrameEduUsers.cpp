#include "FrameEduUsers.h"

IMPLEMENT_DYNAMIC(FrameEduUsers, GridFrameBase)

FrameEduUsers::FrameEduUsers(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
	m_CreateRefreshPartial = false;
}

void FrameEduUsers::BuildView(const vector<EducationUser>& p_EduUsers, const vector<O365UpdateOperation>& p_Updates, bool p_FullPurge)
{
	m_Grid.BuildView(p_EduUsers, p_Updates, p_FullPurge);
}

O365Grid& FrameEduUsers::GetGrid()
{
	return m_Grid;
}

void FrameEduUsers::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	ASSERT(info.GetOrigin() == Origin::Tenant);

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::EduUsers, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameEduUsers::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
	ASSERT(false);
}

bool FrameEduUsers::HasApplyButton()
{
	return true;
}

void FrameEduUsers::ApplySpecific(bool p_Selected)
{
	auto grid = dynamic_cast<GridEduUsers*>(&GetGrid());

	ASSERT(nullptr != grid);
	if (nullptr != grid)
	{
		CommandInfo info;
		if (p_Selected)
		{
			vector<GridBackendRow*> selectedRows(grid->GetSelectedRows().begin(), grid->GetSelectedRows().end());
			info.Data() = grid->GetChanges(selectedRows);
		}
		else
			info.Data() = grid->GetChanges();
		info.SetFrame(this);
		info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::EduUsers, Command::ModuleTask::ApplyChanges, info, { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
	}
}

void FrameEduUsers::ApplyAllSpecific()
{
	ApplySpecific(false);
}

void FrameEduUsers::ApplySelectedSpecific()
{
	ApplySpecific(true);
}

void FrameEduUsers::revertSelectedImpl()
{
	GetGrid().RevertSelectedRows(O365Grid::RevertFlags::UPDATE_GRID_IF_NEEDED | O365Grid::RevertFlags::ANY_LEVEL_REVERT_CHILDREN);
}

void FrameEduUsers::createGrid()
{
	m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FrameEduUsers::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(tab);
	CreateRefreshGroup(tab);
	CreateApplyGroup(tab);
	CreateRevertGroup(tab);
	CreateEditGroup(tab, false);

	return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameEduUsers::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	bool newFrame = p_Action && p_Action->IsParamTrue(g_NewFrame);

	/*if (IsActionSelectedShowClassMembers(p_Action))
	{
		p_CommandID = newFrame ? ID_SCHOOLSGRID_SHOWCLASS_MEMBERS_NEWFRAME : ID_SCHOOLSGRID_SHOWCLASS_MEMBERS_FRAME;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}*/

	return statusRV;
}