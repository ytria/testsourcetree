#pragma once

// User Block Id
struct UBIEnum
{
	enum _ : uint32_t
	{
		MIN						= 0x001,
		LIST					= 0x002,
		SYNCV1					= 0x004,
		SYNCBETA				= 0x008,
		MAILBOXSETTINGS			= 0x010,
		ARCHIVEFOLDERNAME		= 0x020,
		DRIVE					= 0x040,
		MANAGER					= 0x080,
		MAILBOX					= 0x100, // powershell
		RECIPIENTPERMISSIONS	= 0x200, // powershell
		// FIXME: Eventually remove this when license module correctly uses Delta/Sync to count licenses
		ASSIGNEDLICENSES		= 0x400, // assignedlicenses sometimes needed with MIN (for rbac) so can't be part of LIST
	};
};
using UBI = UBIEnum::_;

// Group Block Id
struct GBIEnum
{
	enum _ : uint32_t
	{
		MIN					= 0x001,
		LIST				= 0x002,
		SYNCV1				= 0x004,
		SYNCBETA			= 0x008,
		CREATEDONBEHALFOF	= 0x010,
		TEAM				= 0x020,
		GROUPSETTINGS		= 0x040,
		LIFECYCLEPOLICIES	= 0x080,
		OWNERS				= 0x100,
		MEMBERSCOUNT		= 0x200,
		DRIVE				= 0x400,
		ISYAMMER			= 0x800,
	};
};
using GBI = GBIEnum::_;

// Tags for template specializations.
struct UserCache {};
struct GroupCache {};

// ============================================================================

template <uint32_t x>
struct _log2
{
	static constexpr uint32_t value = 1 + _log2<x / 2>::value;
};
template<> struct _log2<1> { static constexpr uint32_t value = 1; };

// ==============================================================================

template<class Object>
class SqlCacheObjectSpecific
{
public:
	using BlockId = uint32_t;

	static const std::map<wstring, std::vector<PooledString>>& DataBlocks();
	static const wstring& DataBlockName(typename BlockId);
};

template<class Object>
class SqlCacheConfig
{
public:
	static const std::vector<PooledString>& DataBlockFieldsByName(const wstring& p_BlockName);
	static const std::vector<PooledString>& DataBlockFieldsById(typename SqlCacheObjectSpecific<Object>::BlockId);
	static const std::vector<wstring> DataBlockNames(const std::vector<typename SqlCacheObjectSpecific<Object>::BlockId>& p_BlockIds);
	static const std::vector<wstring>& DataBlockNames();
	static wstring DataBlockDateFieldByName(const wstring& p_BlockName);
	static wstring DataBlockDateFieldById(typename SqlCacheObjectSpecific<Object>::BlockId);
};

// ============================================================================
// User cache specialization
template<>
class SqlCacheObjectSpecific<UserCache>
{
public:
	using BlockId = UBI;

	template<BlockId blockId>
	struct BlockIdIndex
	{
		static constexpr auto value = _log2<(uint32_t)blockId>::value;
	};

	static const std::map<wstring, std::vector<PooledString>>& DataBlocks();
	static const wstring& DataBlockName(typename BlockId);
};

// ============================================================================
// Group cache specialization
template<>
class SqlCacheObjectSpecific<GroupCache>
{
public:
	using BlockId = GBI;

	template<BlockId blockId>
	struct BlockIdIndex
	{
		static constexpr auto value = _log2<(uint32_t)blockId>::value;
	};

	static const std::map<wstring, std::vector<PooledString>>& DataBlocks();
	static const wstring& DataBlockName(typename BlockId);
};

#include "SqlCacheConfig.hpp"
