#pragma once

#include "GridBackendColumn.h"

class GridSetupMetaColumn
{
public:
    GridSetupMetaColumn(GridBackendColumn** p_Column, const wstring& p_DisplayName, const wstring& p_Family);
    GridSetupMetaColumn(GridBackendColumn** p_Column, const wstring& p_DisplayName, const wstring& p_Family, const wstring& p_UID);

    GridBackendColumn** GetColumn() const;    
    const wstring& GetDisplayName() const;
    const wstring& GetFamily() const;
    const wstring& GetUID() const;

public:
    static const wstring g_ColUIDMetaDisplayName;
    static const wstring g_ColUIDMetaUserName;
    static const wstring g_ColUIDMetaId;

private:
    GridBackendColumn** m_Column;
    wstring m_DisplayName;
    wstring m_Family;
    wstring m_UID;
};

