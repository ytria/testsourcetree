#include "DbDriveSerializer.h"
#include "IdentitySet.h"
#include "MsGraphFieldNames.h"
#include "NullableSerializer.h"
#include "IdentitySetSerializer.h"
#include "QuotaSerializer.h"

DbDriveSerializer::DbDriveSerializer(const BusinessDrive& p_Drive)
	:m_Drive(p_Drive)
{
}

void DbDriveSerializer::Serialize()
{
	m_Json = web::json::value::object();
	auto& obj = m_Json.as_object();

	JsonSerializeUtil::SerializeAny(_YTEXT(O365_DRIVECREATEDBY), NullableSerializer<IdentitySet, IdentitySetSerializer>(m_Drive.m_CreatedBy), obj);
	JsonSerializeUtil::SerializeTimeDate(_YTEXT(O365_DRIVECREATEDDATETIME), m_Drive.m_CreatedDateTime, obj);
	JsonSerializeUtil::SerializeString(_YTEXT(O365_DRIVEDESCRIPTION), m_Drive.m_Description, obj);
	JsonSerializeUtil::SerializeString(_YTEXT(O365_DRIVETYPE), m_Drive.m_DriveType, obj);
	JsonSerializeUtil::SerializeString(_YTEXT(O365_ID), m_Drive.m_Id, obj);
	JsonSerializeUtil::SerializeTimeDate(_YTEXT(O365_DRIVELASTMODIFIEDDATETIME), m_Drive.m_LastModifiedDateTime, obj);
	JsonSerializeUtil::SerializeString(_YTEXT(O365_DRIVENAME), m_Drive.m_Name, obj);
	JsonSerializeUtil::SerializeString(_YTEXT(O365_DRIVEWEBURL), m_Drive.m_WebUrl, obj);
	JsonSerializeUtil::SerializeAny(_YTEXT(O365_DRIVEOWNER), NullableSerializer<IdentitySet, IdentitySetSerializer>(m_Drive.m_Owner), obj);
	JsonSerializeUtil::SerializeAny(_YTEXT(O365_DRIVEQUOTA), NullableSerializer<Quota, QuotaSerializer>(m_Drive.m_Quota), obj);
}
