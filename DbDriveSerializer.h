#pragma once

#include "IJsonSerializer.h"
#include "BusinessDrive.h"

class DbDriveSerializer : public IJsonSerializer
{
public:
	DbDriveSerializer(const BusinessDrive& p_Drive);
	void Serialize() override;

private:
	BusinessDrive m_Drive;
};

