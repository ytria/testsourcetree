#include "BusinessMessageRule.h"

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessMessageRule>("Business Message Rule") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Message Rule"))))
		.constructor()(policy::ctor::as_object)
		;

	registration::class_<BusinessMessageRuleComponent>("Business Message Rule Component") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Rule Component"))))
		.constructor()(policy::ctor::as_object)
		;
}

void SapioGraphObject::SetError(const boost::YOpt<SapioError>& p_Error)
{
	m_Error = p_Error;
}

const boost::YOpt<SapioError>& SapioGraphObject::GetError() const
{
	return m_Error;
}

void SapioGraphObject::SetFlags(uint32_t p_Flags)
{
	m_Flags = p_Flags;
}

uint32_t SapioGraphObject::GetFlags() const
{
	return m_Flags;
}

bool SapioGraphObject::HasFlag(uint32_t p_Flag) const
{
	return p_Flag == (GetFlags() & p_Flag);
}
