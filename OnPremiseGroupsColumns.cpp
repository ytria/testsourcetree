#include "OnPremiseGroupsColumns.h"

#include "BaseO365Grid.h"
#include "GridTemplate.h"

bool OnPremiseGroupsColumns::Create(O365Grid& p_Grid)
{
	if (!m_Created)
	{
		m_ColCommonDisplayName = p_Grid.AddColumn(_YUID("commonDisplayName"), GetTitle(_YUID("commonDisplayName")), O365Grid::g_FamilyCommonInfo, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCommonMetaType = p_Grid.AddColumnIcon(_YUID("metaType"), GetTitle(_YUID("metaType")), O365Grid::g_FamilyCommonInfo);
		m_ColCommonMetaType->SetPresetFlags({ O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });

		m_ColAdminCount = p_Grid.AddColumnNumberInt(_YUID("onPremAdminCount"), GetTitle(_YUID("onPremAdminCount")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCanonicalName = p_Grid.AddColumn(_YUID("onPremCanonicalName"), GetTitle(_YUID("onPremCanonicalName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCN = p_Grid.AddColumn(_YUID("onPremCn"), GetTitle(_YUID("onPremCn")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCreated = p_Grid.AddColumnDate(_YUID("onPremCreated"), GetTitle(_YUID("onPremCreated")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColCreateTimeStamp = p_Grid.AddColumnDate(_YUID("onPremCreateTimeStamp"), GetTitle(_YUID("onPremCreateTimeStamp")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDeleted = p_Grid.AddColumnCheckBox(_YUID("onPremDeleted"), GetTitle(_YUID("onPremDeleted")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDescription = p_Grid.AddColumn(_YUID("onPremDescription"), GetTitle(_YUID("onPremDescription")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDisplayName = p_Grid.AddColumn(_YUID("onPremDisplayName"), GetTitle(_YUID("onPremDisplayName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDistinguishedName = p_Grid.AddColumn(_YUID("onPremDistinguishedName"), GetTitle(_YUID("onPremDistinguishedName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColDSCorePropagationData = p_Grid.AddColumn(_YUID("onPremDsCorePropagationData"), GetTitle(_YUID("onPremDsCorePropagationData")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColGroupCategory = p_Grid.AddColumn(_YUID("onPremGroupCategory"), GetTitle(_YUID("onPremGroupCategory")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColGroupScope = p_Grid.AddColumn(_YUID("onPremGroupScope"), GetTitle(_YUID("onPremGroupScope")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColGroupType = p_Grid.AddColumnNumberInt(_YUID("onPremGroupType"), GetTitle(_YUID("onPremGroupType")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColHomePage = p_Grid.AddColumn(_YUID("onPremHomePage"), GetTitle(_YUID("onPremHomePage")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColInstanceType = p_Grid.AddColumnNumberInt(_YUID("onPremInstanceType"), GetTitle(_YUID("onPremInstanceType")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColIsCriticalSystemObject = p_Grid.AddColumnCheckBox(_YUID("onPremIsCriticalSystemObject"), GetTitle(_YUID("onPremIsCriticalSystemObject")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColIsDeleted = p_Grid.AddColumnCheckBox(_YUID("onPremIsDeleted"), GetTitle(_YUID("onPremIsDeleted")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColLastKnownParent = p_Grid.AddColumn(_YUID("onPremLastKnownParent"), GetTitle(_YUID("onPremLastKnownParent")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColManagedBy = p_Grid.AddColumn(_YUID("onPremManagedBy"), GetTitle(_YUID("onPremManagedBy")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColMember = p_Grid.AddColumn(_YUID("onPremMember"), GetTitle(_YUID("onPremMember")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColMemberOf = p_Grid.AddColumn(_YUID("onPremMemberOf"), GetTitle(_YUID("onPremMemberOf")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColMembers = p_Grid.AddColumn(_YUID("onPremMembers"), GetTitle(_YUID("onPremMembers")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColModified = p_Grid.AddColumnDate(_YUID("onPremModified"), GetTitle(_YUID("onPremModified")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColModifyTimeStamp = p_Grid.AddColumnDate(_YUID("onPremModifyTimeStamp"), GetTitle(_YUID("onPremModifyTimeStamp")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColName = p_Grid.AddColumn(_YUID("onPremName"), GetTitle(_YUID("onPremName")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		//m_ColNTSecurityDescriptor = nullptr;
		m_ColObjectCategory = p_Grid.AddColumn(_YUID("onPremObjectCategory"), GetTitle(_YUID("onPremObjectCategory")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColObjectClass = p_Grid.AddColumn(_YUID("onPremObjectClass"), GetTitle(_YUID("onPremObjectClass")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColObjectGUID = p_Grid.AddColumn(_YUID("onPremObjectGUID"), GetTitle(_YUID("onPremObjectGUID")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColObjectSid = p_Grid.AddColumn(_YUID("onPremObjectSid"), GetTitle(_YUID("onPremObjectSid")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColProtectedFromAccidentalDeletion = p_Grid.AddColumnCheckBox(_YUID("onPremProtectedFromAccidentalDeletion"), GetTitle(_YUID("onPremProtectedFromAccidentalDeletion")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColSamAccountName = p_Grid.AddColumn(_YUID("samAccountname"), GetTitle(_YUID("samAccountname")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColSAMAccountType = p_Grid.AddColumnNumberInt(_YUID("onPremSAMAccountType"), GetTitle(_YUID("onPremSAMAccountType")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColSDRightsEffective = p_Grid.AddColumnNumberInt(_YUID("onPremSDRightsEffective"), GetTitle(_YUID("onPremSDRightsEffective")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColSID = p_Grid.AddColumn(_YUID("onPremSid"), GetTitle(_YUID("onPremSid")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize22, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		//m_ColSIDHistory = nullptr;
		m_ColSystemFlags = p_Grid.AddColumnNumberInt(_YUID("onPremSystemFlags"), GetTitle(_YUID("onPremSystemFlags")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColUSNChanged = p_Grid.AddColumnDate(_YUID("onPremUSNChanged"), GetTitle(_YUID("onPremUSNChanged")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColUSNCreated = p_Grid.AddColumnDate(_YUID("onPremUsnCreated"), GetTitle(_YUID("onPremUsnCreated")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColWhenChanged = p_Grid.AddColumnDate(_YUID("onPremWhenChanged"), GetTitle(_YUID("onPremWhenChanged")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });
		m_ColWhenCreated = p_Grid.AddColumnDate(_YUID("onPremWhenCreated"), GetTitle(_YUID("onPremWhenCreated")), O365Grid::g_FamilyOnPrem, O365Grid::g_ColumnSize8, { O365Grid::g_ColumnsPresetOnPrem, O365Grid::g_ColumnsPresetOnPremDefault });

		m_Columns =
		{
			m_ColAdminCount,
			m_ColCanonicalName,
			m_ColCN,
			m_ColCreated,
			m_ColCreateTimeStamp,
			m_ColDeleted,
			m_ColDescription,
			m_ColDisplayName,
			m_ColDistinguishedName,
			m_ColDSCorePropagationData,
			m_ColGroupCategory,
			m_ColGroupScope,
			m_ColGroupType,
			m_ColHomePage,
			m_ColInstanceType,
			m_ColIsCriticalSystemObject,
			m_ColIsDeleted,
			m_ColLastKnownParent,
			m_ColManagedBy,
			m_ColMember,
			m_ColMemberOf,
			m_ColMembers,
			m_ColModified,
			m_ColModifyTimeStamp,
			m_ColName,
			//m_ColNTSecurityDescriptor,
			m_ColObjectCategory,
			m_ColObjectClass,
			m_ColObjectGUID,
			m_ColObjectSid,
			m_ColProtectedFromAccidentalDeletion,
			m_ColSamAccountName,
			m_ColSAMAccountType,
			m_ColSDRightsEffective,
			m_ColSID,
			//m_ColSIDHistory,
			m_ColSystemFlags,
			m_ColUSNChanged,
			m_ColUSNCreated,
			m_ColWhenChanged,
			m_ColWhenCreated
		};

		m_CommonColumns =
		{
			m_ColCommonDisplayName,
			m_ColCommonMetaType,
		};

		p_Grid.AddCategoryTop(_T("On-Premises fields"), m_Columns);
		p_Grid.AddCategoryTop(_T("Common Info"), m_CommonColumns);

		// Usually done in O365Grid::customizeGridPostProcess(), but we are called outside so do it manually
		{
			auto process = [&p_Grid](auto& c)
			{
				ASSERT(nullptr != c);
				if (nullptr != c)
				{
					if (c->IsCheckbox())
						c->SetReadOnly(true);
					if (c->IsCombo())
						ASSERT(false);
					if (c->HasPresetFlag(O365Grid::g_ColumnsPresetOnPremDefault) && !c->HasPresetFlag(O365Grid::g_ColumnsPresetTech))
						p_Grid.SetColumnVisible(c, false, GridBackendUtil::SETVISIBLE_NONE);
				}
			};

			for (auto c : m_Columns)
				process(c);

			for (auto c : m_CommonColumns)
				process(c);
		}

		// Init setters maps
		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseGroupsColumns::*, std::function<void(OnPremiseGroup&, const boost::YOpt<PooledString>&)>>> stringSetters
			{
				{ &OnPremiseGroupsColumns::m_ColCanonicalName, &OnPremiseGroup::SetCanonicalName },
				{ &OnPremiseGroupsColumns::m_ColCN, &OnPremiseGroup::SetCN },
				{ &OnPremiseGroupsColumns::m_ColDescription, &OnPremiseGroup::SetDescription },
				{ &OnPremiseGroupsColumns::m_ColDisplayName, &OnPremiseGroup::SetDisplayName },
				{ &OnPremiseGroupsColumns::m_ColDistinguishedName, &OnPremiseGroup::SetDistinguishedName },
				{ &OnPremiseGroupsColumns::m_ColGroupCategory, &OnPremiseGroup::SetGroupCategory },
				{ &OnPremiseGroupsColumns::m_ColGroupScope, &OnPremiseGroup::SetGroupScope },
				{ &OnPremiseGroupsColumns::m_ColHomePage, &OnPremiseGroup::SetHomePage },
				{ &OnPremiseGroupsColumns::m_ColLastKnownParent, &OnPremiseGroup::SetLastKnownParent },
				{ &OnPremiseGroupsColumns::m_ColManagedBy, &OnPremiseGroup::SetManagedBy },
				{ &OnPremiseGroupsColumns::m_ColName, &OnPremiseGroup::SetName },
				{ &OnPremiseGroupsColumns::m_ColObjectCategory, &OnPremiseGroup::SetObjectCategory },
				{ &OnPremiseGroupsColumns::m_ColObjectClass, &OnPremiseGroup::SetObjectClass },
				{ &OnPremiseGroupsColumns::m_ColObjectGUID, &OnPremiseGroup::SetObjectGUID },
				{ &OnPremiseGroupsColumns::m_ColObjectSid, &OnPremiseGroup::SetObjectSid },
				{ &OnPremiseGroupsColumns::m_ColSamAccountName, &OnPremiseGroup::SetSamAccountName },
				{ &OnPremiseGroupsColumns::m_ColSID, &OnPremiseGroup::SetSID },
			};

			for (const auto& s : stringSetters)
				m_StringSetters[this->*(s.first)] = s.second;
		}

		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseGroupsColumns::*, std::function<void(OnPremiseGroup&, const boost::YOpt<bool>&)>>> boolSetters
			{
				{ &OnPremiseGroupsColumns::m_ColDeleted, &OnPremiseGroup::SetDeleted },
				{ &OnPremiseGroupsColumns::m_ColIsCriticalSystemObject, &OnPremiseGroup::SetIsCriticalSystemObject },
				{ &OnPremiseGroupsColumns::m_ColIsDeleted, &OnPremiseGroup::SetIsDeleted },
				{ &OnPremiseGroupsColumns::m_ColProtectedFromAccidentalDeletion, &OnPremiseGroup::SetProtectedFromAccidentalDeletion },
			};

			for (const auto& b : boolSetters)
				m_BoolSetters[this->*(b.first)] = b.second;
		}

		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseGroupsColumns::*, std::function<void(OnPremiseGroup&, const boost::YOpt<YTimeDate>&)>>> dateSetters
			{
				{ &OnPremiseGroupsColumns::m_ColCreated, &OnPremiseGroup::SetCreated },
				{ &OnPremiseGroupsColumns::m_ColCreateTimeStamp, &OnPremiseGroup::SetCreateTimeStamp },
				{ &OnPremiseGroupsColumns::m_ColModified, &OnPremiseGroup::SetModified },
				{ &OnPremiseGroupsColumns::m_ColModifyTimeStamp, &OnPremiseGroup::SetModifyTimeStamp },
				{ &OnPremiseGroupsColumns::m_ColUSNChanged, &OnPremiseGroup::SetUSNChanged },
				{ &OnPremiseGroupsColumns::m_ColUSNCreated, &OnPremiseGroup::SetUSNCreated },
				{ &OnPremiseGroupsColumns::m_ColWhenChanged, &OnPremiseGroup::SetWhenChanged },
				{ &OnPremiseGroupsColumns::m_ColWhenCreated, &OnPremiseGroup::SetWhenCreated },
			};

			for (const auto& d : dateSetters)
				m_DateSetters[this->*(d.first)] = d.second;
		}

		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseGroupsColumns::*, std::function<void(OnPremiseGroup&, const boost::YOpt<int32_t>&)>>> intSetters
			{
				{ &OnPremiseGroupsColumns::m_ColAdminCount, &OnPremiseGroup::SetAdminCount },
				{ &OnPremiseGroupsColumns::m_ColGroupType, &OnPremiseGroup::SetGroupType },
				{ &OnPremiseGroupsColumns::m_ColInstanceType, &OnPremiseGroup::SetInstanceType },
				{ &OnPremiseGroupsColumns::m_ColSAMAccountType, &OnPremiseGroup::SetSAMAccountType },
				{ &OnPremiseGroupsColumns::m_ColSDRightsEffective, &OnPremiseGroup::SetSDRightsEffective },
				{ &OnPremiseGroupsColumns::m_ColSystemFlags, &OnPremiseGroup::SetSystemFlags },
			};

			for (const auto& i : intSetters)
				m_IntSetters[this->*(i.first)] = i.second;
		}

		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseGroupsColumns::*, std::function<void(OnPremiseGroup&, const boost::YOpt<vector<PooledString>>&)>>> stringListSetters
			{
				{ &OnPremiseGroupsColumns::m_ColMember, &OnPremiseGroup::SetMember },
				{ &OnPremiseGroupsColumns::m_ColMemberOf, &OnPremiseGroup::SetMemberOf },
				{ &OnPremiseGroupsColumns::m_ColMembers, &OnPremiseGroup::SetMembers },
			};

			for (const auto& sl : stringListSetters)
				m_StringListSetters[this->*(sl.first)] = sl.second;
		}

		{
			static std::vector<std::pair<GridBackendColumn* OnPremiseGroupsColumns::*, std::function<void(OnPremiseGroup&, const boost::YOpt<vector<YTimeDate>>&)>>> dateListSetters
			{
				{ &OnPremiseGroupsColumns::m_ColDSCorePropagationData, &OnPremiseGroup::SetDSCorePropagationData },
			};

			for (const auto& dl : dateListSetters)
				m_DateListSetters[this->*(dl.first)] = dl.second;
		}

		m_Created = true;
		return true;
	}

	return false;
}

const wstring& OnPremiseGroupsColumns::GetTitle(const wstring& p_YUID)
{
	static std::map<wstring, wstring> columnTitles
	{
		{ _YUID("metaType"), _T("O365/On-Premises") },
		{ _YUID("commonDisplayName"), _T("Display Name") },
		{ _YUID("onPremAdminCount"), _T("Admin Count") },
		{ _YUID("onPremCanonicalName"), _T("Canonical name") },
		{ _YUID("onPremCn"), _T("CN") },
		{ _YUID("onPremCreated"), _T("Created") },
		{ _YUID("onPremCreateTimeStamp"), _T("Create TimeStamp") },
		{ _YUID("onPremDeleted"), _T("Deleted") },
		{ _YUID("onPremDescription"), _T("Description") },
		{ _YUID("onPremDisplayName"), _T("Display Name") },
		{ _YUID("onPremDistinguishedName"), _T("Distinguished Name") },
		{ _YUID("onPremDsCorePropagationData"), _T("DS Core Propagation Data") },
		{ _YUID("onPremGroupCategory"), _T("Group Category") },
		{ _YUID("onPremGroupScope"), _T("Group Scope") },
		{ _YUID("onPremGroupType"), _T("Group Type") },
		{ _YUID("onPremHomePage"), _T("HomePage") },
		{ _YUID("onPremInstanceType"), _T("Instance Type") },
		{ _YUID("onPremIsCriticalSystemObject"), _T("Is Critical System Object") },
		{ _YUID("onPremIsDeleted"), _T("Is Deleted") },
		{ _YUID("onPremLastKnownParent"), _T("Last Known Parent") },
		{ _YUID("onPremManagedBy"), _T("Managed By") },
		{ _YUID("onPremMember"), _T("Member") },
		{ _YUID("onPremMemberOf"), _T("Member of") },
		{ _YUID("onPremMembers"), _T("Members") },
		{ _YUID("onPremModified"), _T("Modified") },
		{ _YUID("onPremModifyTimeStamp"), _T("Modify TimeStamp") },
		{ _YUID("onPremName"), _T("Name") },
		//m_ColNTSecurityDescriptor = nullptr;
		{ _YUID("onPremObjectCategory"), _T("Object Category") },
		{ _YUID("onPremObjectClass"), _T("Object Class") },
		{ _YUID("onPremObjectGUID"), _T("Object GUID") },
		{ _YUID("onPremObjectSid"), _T("Object Sid") },
		{ _YUID("onPremProtectedFromAccidentalDeletion"), _T("Protected from accidental deletion") },
		{ _YUID("samAccountname"), _T("Sam Account Name") },
		{ _YUID("onPremSAMAccountType"), _T("SAM Account Type") },
		{ _YUID("onPremSDRightsEffective"), _T("SD Rights Effective") },
		{ _YUID("onPremSid"), _T("SID") },
		//m_ColSIDHistory = nullptr;
		{ _YUID("onPremSystemFlags"), _T("System Flags") },
		{ _YUID("onPremUSNChanged"), _T("USN Changed") },
		{ _YUID("onPremUsnCreated"), _T("USN Created") },
		{ _YUID("onPremWhenChanged"), _T("When Changed") },
		{ _YUID("onPremWhenCreated"), _T("When Created") },
	};

	const auto findIt = columnTitles.find(p_YUID);
	if (findIt != columnTitles.end())
		return findIt->second;

	ASSERT(false);
	return Str::g_EmptyString;
}

vector<GridBackendField> OnPremiseGroupsColumns::GetFields(GridBackendRow& p_Row)
{
	vector<GridBackendField> fields;
	fields.reserve(m_Columns.size());

	for (auto col : m_Columns)
		fields.emplace_back(p_Row.GetField(col));

	return fields;
}

void OnPremiseGroupsColumns::SetBlankFields(GridBackendRow& p_Row)
{
	p_Row.RemoveFields(m_Columns);
}

const vector<GridBackendColumn*>& OnPremiseGroupsColumns::GetColumns() const
{
	return m_Columns;
}

const vector<GridBackendColumn*>& OnPremiseGroupsColumns::GetCommonColumns() const
{
	return m_CommonColumns;
}

void OnPremiseGroupsColumns::SetValue(OnPremiseGroup& p_OnPremGroup, GridBackendColumn* p_Col, const GridBackendField& p_Field) const
{
	if (nullptr == p_Col || !GridTemplate::hasValidErrorlessValue(p_Field))
		return;

	// String properties
	{
		auto it = m_StringSetters.find(p_Col);
		if (m_StringSetters.end() != it)
		{
			if (p_Field.IsString())
				(it->second)(p_OnPremGroup, PooledString(p_Field.GetValueStr()));

			return;
		}
	}

	// Bool properties
	{
		auto it = m_BoolSetters.find(p_Col);
		if (m_BoolSetters.end() != it)
		{
			if (p_Field.IsBool()
				|| p_Field.IsNumber()) // GridBackendRow::AddFieldForCheckBox sets an int.
				(it->second)(p_OnPremGroup, p_Field.GetValueBool());

			return;
		}
	}

	// Int32 properties
	{
		auto it = m_IntSetters.find(p_Col);
		if (m_IntSetters.end() != it)
		{
			if (p_Field.IsNumber())
				(it->second)(p_OnPremGroup, static_cast<int32_t>(p_Field.GetValueDouble()));

			return;
		}
	}

	// Datetime properties
	{
		auto it = m_DateSetters.find(p_Col);
		if (m_DateSetters.end() != it)
		{
			if (p_Field.IsDate())
				(it->second)(p_OnPremGroup, p_Field.GetValueTimeDate());

			return;
		}
	}

	// String list properties
	{
		auto it = m_StringListSetters.find(p_Col);
		if (m_StringListSetters.end() != it)
		{
			if (p_Field.IsMulti() && nullptr != p_Field.GetValuesMulti<PooledString>())
				(it->second)(p_OnPremGroup, *p_Field.GetValuesMulti<PooledString>());
			else if (p_Field.IsString())
				(it->second)(p_OnPremGroup, { { PooledString(p_Field.GetValueStr()) } });

			return;
		}
	}

	// Datetime list properties
	{
		auto it = m_DateListSetters.find(p_Col);
		if (m_DateListSetters.end() != it)
		{
			if (p_Field.IsMulti() && nullptr != p_Field.GetValuesMulti<YTimeDate>())
				(it->second)(p_OnPremGroup, *p_Field.GetValuesMulti<YTimeDate>());
			else if (p_Field.IsDate())
				(it->second)(p_OnPremGroup, { { p_Field.GetValueTimeDate() } });

			return;
		}
	}

	// This is not OnPremiseGroup data, perfectly normal to reach this point for those columns only.
	ASSERT(m_ColCommonDisplayName == p_Col
		|| m_ColCommonMetaType == p_Col);
}
