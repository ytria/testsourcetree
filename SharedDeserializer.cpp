#include "SharedDeserializer.h"

#include "IdentitySetDeserializer.h"
#include "JsonSerializeUtil.h"

void SharedDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        IdentitySetDeserializer isd;
        if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("owner"), p_Object))
            m_Data.Owner = isd.GetData();
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("scope"), m_Data.Scope, p_Object);

    {
        IdentitySetDeserializer isd;
        if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("sharedBy"), p_Object))
            m_Data.SharedBy = isd.GetData();
    }

	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("sharedDateTime"), m_Data.SharedDateTime, p_Object);
}
