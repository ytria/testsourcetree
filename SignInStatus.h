#pragma once

class SignInStatus
{
public:
	boost::YOpt<PooledString> m_AdditionalDetails;
	boost::YOpt<int32_t>	  m_ErrorCode;
	boost::YOpt<PooledString> m_FailureReason;
};

