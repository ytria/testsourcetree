#pragma once

#include "IRequester.h"
#include "Subscription.h"
#include "SubscriptionDeserializer.h"

class SingleRequestResult;

template <class T, class U>
class ValueListDeserializer;

class SubscriptionsListRequester : public IRequester
{
public:
	TaskWrapper<void> Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const vector<Azure::Subscription>& GetData() const;

private:
	shared_ptr<ValueListDeserializer<Azure::Subscription, Azure::SubscriptionDeserializer>> m_Deserializer;
	shared_ptr<SingleRequestResult> m_Result;
};

