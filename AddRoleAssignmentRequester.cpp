#include "AddRoleAssignmentRequester.h"
#include "Sapio365Session.h"
#include "SharepointOnlineSession.h"
#include "SingleRequestResult.h"
#include "BasicHttpRequestLogger.h"

Sp::AddRoleAssignmentRequester::AddRoleAssignmentRequester(PooledString p_SiteName, PooledString p_PrincipalId, PooledString p_RoleDefId)
    : m_SiteName(p_SiteName)
	, m_PrincipalId(p_PrincipalId)
	, m_RoleDefId(p_RoleDefId)
{
}

TaskWrapper<void> Sp::AddRoleAssignmentRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("sites"));
    uri.append_path(m_SiteName);
    uri.append_path(_YTEXT("_api"));
    uri.append_path(_YTEXT("web"));
    uri.append_path(_YTEXT("roleDefinitions"));

    auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetSharepointSession()->Post(uri.to_uri(), httpLogger, m_Result, nullptr, p_TaskData);
}
