#pragma once

#include "ModuleBase.h"

#include "BusinessChannel.h"
#include "BusinessChannel.h"
#include "CommandInfo.h"

class ModulePrivateChannelMembers : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	virtual void executeImpl(const Command& p_Command);
	void show(const Command& p_Command);
	void apply(const Command& p_Command);
};

