#pragma once

#include "ModuleBase.h"

class ModuleSchools : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	void executeImpl(const Command& p_Command) override;
	void showSchools(const Command& p_Command);
	void applyChanges(const Command& p_Command);
};
