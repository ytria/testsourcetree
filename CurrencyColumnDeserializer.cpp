#include "CurrencyColumnDeserializer.h"

#include "JsonSerializeUtil.h"

void CurrencyColumnDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("locale"), m_Data.Locale, p_Object);
}
