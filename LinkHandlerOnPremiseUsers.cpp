#include "LinkHandlerOnPremiseUsers.h"

#include "Command.h"
#include "CommandDispatcher.h"

void LinkHandlerOnPremiseUsers::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::MainModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::OnPremiseUsers, Command::ModuleTask::List, info, { nullptr, g_ActionNameShowUsers, nullptr, p_Link.GetAutomationAction() }));
}
