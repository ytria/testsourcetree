#include "SignInsRequester.h"

#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "SignIn.h"
#include "MsGraphPaginator.h"
#include "DateFieldCutOffCondition.h"
#include "BasicPageRequestLogger.h"
#include "MsGraphHttpRequestLogger.h"

SignInsRequester::SignInsRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_Logger(p_Logger)
{
}

void SignInsRequester::SetCutOffDateTime(const boost::YOpt<wstring>& p_CutOff)
{
	wstring cutoff;
	if (p_CutOff)
		cutoff = *p_CutOff;
	m_CutOffDateTime = p_CutOff;
}

void SignInsRequester::SetCustomFilter(const ODataFilter& p_CustomFilter)
{
	m_CustomFilter = p_CustomFilter;
}

TaskWrapper<void> SignInsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<SignIn, SignInDeserializer>>();

	web::uri_builder uri(_YTEXT("auditlogs"));
	uri.append_path(_YTEXT("signIns"));

	if (m_CutOffDateTime || !m_CustomFilter.IsEmpty())
	{
		ODataFilter oDataFilter;
		if (m_CutOffDateTime)
		{
			ASSERT(!m_CutOffDateTime->empty());
			oDataFilter.GreaterThan(_YTEXT("createdDateTime"), *m_CutOffDateTime, false);
			if (!m_CustomFilter.IsEmpty())
				oDataFilter.And(m_CustomFilter);
		}
		else if (!m_CustomFilter.IsEmpty())
			oDataFilter = m_CustomFilter;

		ASSERT(!oDataFilter.IsEmpty());
		oDataFilter.ApplyTo(uri);
	}

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	m_Paginator.emplace(CreatePaginator(uri.to_uri(), m_Deserializer, m_Logger, httpLogger, p_TaskData.GetOriginator()));

	return m_Paginator->Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

const vector<SignIn>& SignInsRequester::GetData() const
{
	return m_Deserializer->GetData();
}

MsGraphPaginator SignInsRequester::CreatePaginator(const web::uri& p_Uri, const std::shared_ptr<DeserializerType>& p_Deserializer, const std::shared_ptr<IPageRequestLogger>& p_Logger, const std::shared_ptr<IHttpRequestLogger>& p_HttpLogger, HWND p_Hwnd) const
{
	return MsGraphPaginator(p_Uri, Util::CreateDefaultGraphPageRequester(p_Deserializer, p_HttpLogger, p_Hwnd), p_Logger);
}
