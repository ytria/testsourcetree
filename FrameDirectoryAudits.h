#pragma once

#include "GridFrameBase.h"
#include "GridDirectoryAudits.h"
#include "DirectoryAudit.h"
#include "DirectoryAuditsRequester.h"

class FrameDirectoryAudits : public GridFrameBase
{
public:
	DECLARE_DYNAMIC(FrameDirectoryAudits)
	FrameDirectoryAudits(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
	virtual ~FrameDirectoryAudits() = default;

	void BuildView(const vector<DirectoryAudit>& p_DirAudits, bool p_FullPurge, bool p_NoPurge);

	O365Grid& GetGrid() override;

	void RefreshSpecific(AutomationAction* p_CurrentAction) override;
	void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;
	bool HasApplyButton() override;

protected:
	void createGrid() override;
	bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;
	AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	GridDirectoryAudits m_Grid;
};

