#include "OAuth2BrowserSessionLoader.h"
#include "StandardSessionLoader.h"
#include "SessionsSqlEngine.h"
#include "SessionTypes.h"

StandardSessionLoader::StandardSessionLoader(const SessionIdentifier& p_Identifier)
	:ISessionLoader(p_Identifier)
{
	ASSERT(p_Identifier.m_SessionType == SessionTypes::g_StandardSession);
}

TaskWrapper<std::shared_ptr<Sapio365Session>> StandardSessionLoader::Load(const std::shared_ptr<Sapio365Session>& p_SapioSession)
{
	OAuth2BrowserSessionLoader loader(GetSessionId(), GetPersistentSession(p_SapioSession));
	return loader.Load(p_SapioSession);
}
