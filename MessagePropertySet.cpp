#include "MessagePropertySet.h"


MessagePropertySet::MessagePropertySet()
	: m_BodyPreview(true),
	m_FullBody(true),
	m_MailHeaders(true)
{
}

void MessagePropertySet::SetMailPreview(bool p_Set)
{
	m_BodyPreview = p_Set;
}

void MessagePropertySet::SetBodyContent(bool p_Set)
{
	m_FullBody = p_Set;
}

void MessagePropertySet::SetMailHeaders(bool p_Set)
{
	m_MailHeaders = p_Set;
}

vector<rttr::property> MessagePropertySet::GetPropertySet() const
{
	ASSERT(false);
	return {};
}

vector<PooledString> MessagePropertySet::GetStringPropertySet() const
{
	vector<PooledString> properties = {
		_YTEXT("bccRecipients"),
		_YTEXT("categories"),
		_YTEXT("ccRecipients"),
		_YTEXT("changeKey"),
		_YTEXT("conversationId"),
		_YTEXT("createdDateTime"),
		_YTEXT("from"),
		_YTEXT("hasAttachments"),
		_YTEXT("id"),
		_YTEXT("importance"),
		_YTEXT("inferenceClassification"),
		_YTEXT("internetMessageId"),
		_YTEXT("isDeliveryReceiptRequested"),
		_YTEXT("isDraft"),
		_YTEXT("isRead"),
		_YTEXT("isReadReceiptRequested"),
		_YTEXT("lastModifiedDateTime"),
		_YTEXT("parentFolderId"),
		_YTEXT("receivedDateTime"),
		_YTEXT("replyTo"),
		_YTEXT("sentDateTime"),
		_YTEXT("sender"),
		_YTEXT("subject"),
		_YTEXT("toRecipients"),
		_YTEXT("webLink"),
	};

	if (m_BodyPreview)
		properties.push_back(_YTEXT("bodyPreview"));
	if (m_FullBody)
		properties.push_back(_YTEXT("body"));
	if (m_MailHeaders)
		properties.push_back(_YTEXT("internetMessageHeaders"));

	return properties;
}
