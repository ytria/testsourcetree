#include "ItemAttachmentDeserializer.h"

#include "AnyItemAttachmentDeserializer.h"
#include "JsonSerializeUtil.h"

void ItemAttachmentDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("contentType"), m_Data.m_ContentType, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isInline"), m_Data.m_IsInline, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.m_LastModifiedDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YTEXT("size"), m_Data.m_Size, p_Object);

    {
        AnyItemAttachmentDeserializer aiad;
        if (JsonSerializeUtil::DeserializeAny(aiad, _YTEXT("item"), p_Object))
            m_Data.m_Item = std::move(aiad.GetData());
    }
}
