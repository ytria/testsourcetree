#include "DlgSelectRecipients.h"

#include "CacheGrid.h"
#include "GridSelectRecipients.h"

DlgSelectRecipients::DlgSelectRecipients(GridSelectRecipients* p_Grid, CWnd* p_Parent)
    : ResizableDialog(DlgSelectRecipients::IDD, p_Parent),
    m_Grid(nullptr)
{
    ASSERT(p_Grid);
    m_Grid = p_Grid;
}

vector<PooledString> DlgSelectRecipients::GetSelectedRecipients()
{
    return m_Grid->GetSelectedEmails();
}

void DlgSelectRecipients::DoDataExchange(CDataExchange* pDX)
{
    ResizableDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDOK, m_BtnOk);
}

BOOL DlgSelectRecipients::OnInitDialogSpecificResizable()
{
    SetWindowText(YtriaTranslate::Do(DlgSelectRecipients_OnInitDialogSpecificResizable_1, _YLOC("Select Email Recipients")).c_str());

    createGrid();
    ModifyStyleEx(0, WS_EX_APPWINDOW);
    if (m_Grid)
        AddAnchor(*m_Grid, CSize(0, 0), CSize(100, 100));

    AddAnchor(m_BtnOk, CSize(100, 100), CSize(100, 100));

    return TRUE;
}

void DlgSelectRecipients::createGrid()
{
    CWnd* gridPlacement = GetDlgItem(IDC_DLG_SELECTRECIPIENTS_GRID);

    ASSERT(gridPlacement != nullptr);
    if (m_Grid && gridPlacement != nullptr)
    {
        CRect rect;
        gridPlacement->GetWindowRect(&rect);
        ScreenToClient(&rect);
        if (!m_Grid->Create(this, rect))
        {
            ASSERT(FALSE);
            return;
        }
        m_Grid->AdjustAutoResizeWithGridResize();

        m_Grid->BuildView();
    }
}
