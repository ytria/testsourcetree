#include "UserLicenseModification.h"

#include "ActivityLoggerUtil.h"
#include "GridLicenses.h"
#include "GridUpdater.h"
#include "GridUtil.h"

UserLicenseModification::UserLicenseModification(GridLicenses& p_Grid, const row_pk_t& p_UserPk) : RowModification(p_Grid, p_UserPk),
	m_Grid(&p_Grid),
    m_UserPk(p_UserPk)
{
    auto userRow = p_Grid.GetRowIndex().GetExistingRow(m_UserPk);
    ASSERT(nullptr != userRow);
    if (nullptr != userRow)
        m_UserId = userRow->GetField(p_Grid.GetColMetaId()).GetValueStr();
}

UserLicenseModification::~UserLicenseModification()
{
    if (shouldRevert())
    {
        // Put back the original field in license status column for skus and plans
        for (const auto& modPlan : m_SkuData)
        {
            const auto& skuId		= modPlan.first;
            const auto& licenseInfo = modPlan.second;
            
            auto skuRow = getGrid().GetRowIndex().GetExistingRow(licenseInfo.Pk);
            ASSERT(nullptr != skuRow);
            if (nullptr != skuRow)
            {
                skuRow->SetField(m_Grid->GetColIsAssigned()->GetID(), licenseInfo.OldIsAssignedField).RemoveModified();
                skuRow->SetField(m_Grid->GetColIsAssignedBool()->GetID(), licenseInfo.OldIsAssignedBoolField).RemoveModified();
            }
        }

        for (const auto& modPlan : m_PlanData)
        {
            const auto& licenseInfo = modPlan.second;

            auto planRow = getGrid().GetRowIndex().GetExistingRow(licenseInfo.Pk);
            ASSERT(nullptr != planRow);
            if (nullptr != planRow)
            {
                planRow->SetField(m_Grid->GetColIsAssigned()->GetID(), licenseInfo.OldIsAssignedField).RemoveModified();
                planRow->SetField(m_Grid->GetColIsAssignedBool()->GetID(), licenseInfo.OldIsAssignedBoolField).RemoveModified();
            }
        }

        auto gridLicenses = dynamic_cast<GridLicenses*>(&getGrid());
        ASSERT(nullptr != gridLicenses);
        if (nullptr != gridLicenses)
        {
            // For all rows, update sku count
            for (auto row : gridLicenses->GetBackendRows())
            {
                // Is it a sku row?
                if (GridUtil::isBusinessType<BusinessSubscribedSku>(row))
                {
                    // Does it has a sku concerned with this modification?
                    auto sku = row->GetField(gridLicenses->GetColSkuId()).GetValueStr();
                    auto ittData = m_SkusDelta.find(sku);
                    if (ittData != m_SkusDelta.end())
                    {
                        const auto consumed = row->GetField(gridLicenses->GetColConsumedUnits()).GetValueLong();
						// Remove delta from currently consumed value
                        row->AddField(consumed - ittData->second, gridLicenses->GetColConsumedUnits());

						if (nullptr != gridLicenses->GetColConsumedUnitsInRbacScope())
						{
							const auto consumed = row->GetField(gridLicenses->GetColConsumedUnitsInRbacScope()).GetValueLong();
							// Remove delta from currently consumed value
							row->AddField(consumed - ittData->second, gridLicenses->GetColConsumedUnitsInRbacScope());
						}
                    }
                }
            }
        }

		// Remove status from all related rows
		GridBackendRow* userRow = getGrid().GetRowIndex().GetExistingRow(GetRowKey());
		ASSERT(nullptr != userRow);
		if (nullptr != userRow)
		{
			const auto id = userRow->GetField(m_Grid->GetColMetaId()).GetValueStr();
			auto rows = getGrid().GetRows(id, m_Grid->GetColMetaId()->GetID());
			for (auto& row : rows)
			{
				if (m_UsageLocationNew)
				{
					auto& f = row->AddField(m_UsageLocationOld, m_Grid->GetColMetaUsageLocation());
					if (m_UsageLocationOld)
						f.RemoveModified();
				}
				getGrid().OnRowNothingToSave(row, false);
			}
		}
    }
}

void UserLicenseModification::RefreshState()
{
    auto userRow = m_Grid->GetRowIndex().GetExistingRow(m_UserPk);
    ASSERT(nullptr != userRow);
    if (nullptr != userRow)
    {
		// If no result, why refreshing?
		ASSERT(m_Result || m_UsageLocationResult);

		bool hasError = false;
		vector<wstring> statusMessages;
		{
			if (m_UsageLocationResult)
			{
				if (m_UsageLocationResult->m_Error)
				{
					statusMessages.push_back(_YTEXTFORMAT(L"Usage Location: %s", m_UsageLocationResult->m_Error->GetFullErrorMessage().c_str()));
					hasError = true;
				}
				else
				{
					statusMessages.push_back(_YTEXTFORMAT(L"Usage Location: %s", GridBackend::g_RowStatusSaved.c_str()));
				}
			}

			if (m_Result)
			{
				const auto licenseModStatusCode = m_Result->GetResult().Response.status_code();
				if (licenseModStatusCode < 200 || 300 <= licenseModStatusCode) // not success
				{
					ASSERT(!hasError); // We can have both errors
					statusMessages.push_back(_YTEXTFORMAT(L"License Information: %s", m_Result->GetResult().GetErrorMessage().c_str()));
					hasError = true;
				}
				else
				{
					statusMessages.push_back(_YTEXTFORMAT(L"License Information: %s", GridBackend::g_RowStatusSaved.c_str()));
				}
			}
		}

        // For now, we trust the http status code without reading the grid's state
        if (!hasError)
        {
            m_State = State::RemoteHasNewValue;
			const auto id = userRow->GetField(m_Grid->GetColMetaId()).GetValueStr();
			auto rows = getGrid().GetRows(id, m_Grid->GetColMetaId()->GetID());
			for (auto& row : rows)
				if (row->IsModified())
					getGrid().OnRowSaved(row, false);
        }
        else
        {
            m_State = State::RemoteError;
			const auto id = userRow->GetField(m_Grid->GetColMetaId()).GetValueStr();
			auto rows = getGrid().GetRows(id, m_Grid->GetColMetaId()->GetID());
			for (auto& row : rows)
			{
				if (row->IsModified()) 
				{
					getGrid().OnRowError(row, false);
					row->AddField(statusMessages, getGrid().GetBackend().GetStatusColumn(), row->GetField(getGrid().GetBackend().GetStatusColumn()).GetIcon());
				}
			}
        }
    }
}

void UserLicenseModification::RefreshStatesPostProcess(GridUpdater& p_GridUpdater)
{
    if (State::RemoteError == GetState())
    {
		boost::YOpt<SapioError> error;
		if (m_UsageLocationResult)
			error = m_UsageLocationResult->m_Error;

		if (m_Result && !error)
		{
			const auto licenseModStatusCode = m_Result->GetResult().Response.status_code();
			if (licenseModStatusCode < 200 || 300 <= licenseModStatusCode) // not success
				error = SapioError(m_Result->GetResult().GetErrorMessage(), licenseModStatusCode);
		}

		ASSERT(error);
		if (error)
			p_GridUpdater.OnSavingError(nullptr, *error);
    }
}

void UserLicenseModification::ShowUsageLocationAppliedLocally()
{
    auto userRow = getGrid().GetRowIndex().GetExistingRow(m_UserPk);
    ASSERT(nullptr != userRow);
    if (nullptr != userRow)
    {
        userRow->SetEditModified(true);

		bool hasUsageLocationMod = m_UsageLocationNew.is_initialized();
		bool hasLicenseMod = !m_PlanData.empty() || !m_SkuData.empty() || !m_ModifiedSkus.empty() || !m_DisabledSkus.empty();
		ASSERT(hasUsageLocationMod || hasLicenseMod); // Why calling this if no mod?
		vector<wstring> statusText;
		if (hasUsageLocationMod)
			statusText.push_back(YtriaTranslate::Do(UserLicenseModification_ShowAppliedLocally_1, _YLOC("Usage Location: %1"), GridBackend::g_RowStatusModified.c_str()));
		if (hasLicenseMod)
			statusText.push_back(YtriaTranslate::Do(UserLicenseModification_ShowAppliedLocally_2, _YLOC("License Information: %1"), GridBackend::g_RowStatusModified.c_str()));

		getGrid().GetBackend().OnRowModified(userRow, GridBackend::g_RowStatusModified);
		userRow->AddField(statusText, getGrid().GetBackend().GetStatusColumn(), userRow->GetField(getGrid().GetBackend().GetStatusColumn()).GetIcon());

		const auto id = userRow->GetField(m_Grid->GetColMetaId()).GetValueStr();
		const auto usageLocation = userRow->GetField(m_Grid->GetColMetaUsageLocation()).GetValueStr();
		auto rows = getGrid().GetRows(id, m_Grid->GetColMetaId()->GetID());
		for (auto& row : rows)
		{
			if (row != userRow)
			{
				if (row->IsChangesSaved() || row->IsError())
					row->ClearStatus();

				auto& field = row->AddField(usageLocation, m_Grid->GetColMetaUsageLocation());
				if (hasUsageLocationMod)
					field.SetModified();
				else
					field.RemoveModified();
			}
		}
    }
}

const map<PooledString, int32_t>& UserLicenseModification::GetSkusDelta() const
{
    return m_SkusDelta;
}

void UserLicenseModification::ComputeConsumedSkusWithMods(GridLicenses& p_GridLicenses, const map<PooledString, int32_t>& p_SkuDeltas)
{
    for (auto row : p_GridLicenses.GetBackendRows())
    {
        if (GridUtil::isBusinessType<BusinessSubscribedSku>(row))
        {
            PooledString rowSkuId = row->GetField(p_GridLicenses.GetColSkuId()).GetValueStr();
            auto itt = p_SkuDeltas.find(rowSkuId);
            if (itt != p_SkuDeltas.end())
            {
                auto itt = p_SkuDeltas.find(rowSkuId);
                if (itt != p_SkuDeltas.end())
                {
                    const int32_t consumed = p_GridLicenses.GetNbConsumedBySku(rowSkuId, false);
                    row->AddField(consumed + itt->second, p_GridLicenses.GetColConsumedUnits());

					if (nullptr != p_GridLicenses.GetColConsumedUnitsInRbacScope())
					{
						const int32_t consumed = p_GridLicenses.GetNbConsumedBySku(rowSkuId, true);
						row->AddField(consumed + itt->second, p_GridLicenses.GetColConsumedUnitsInRbacScope());
					}
                }
            }
        }
    }
}

void UserLicenseModification::SetResult(const SingleRequestResult& p_Result)
{
	// If UsageLocation update request has returned an error, we don't process License info update.
	ASSERT(!m_UsageLocationResult || !m_UsageLocationResult->m_Error);
    m_Result = p_Result;
}

void UserLicenseModification::SetUsageLocationResult(const HttpResultWithError& p_Result)
{
	ASSERT(!m_Result); // Always process UsageLocation before license information.
	m_UsageLocationResult = p_Result;
}

void UserLicenseModification::Merge(const UserLicenseModification& p_NewMod)
{
    ASSERT(m_Grid == p_NewMod.m_Grid);
    ASSERT(m_UserId == p_NewMod.m_UserId);
    ASSERT(m_UserPk == p_NewMod.m_UserPk);

    for (const auto& newDisabledSkuId : p_NewMod.m_DisabledSkus)
    {
        auto thisModSku = m_ModifiedSkus.find(newDisabledSkuId);
        if (thisModSku != m_ModifiedSkus.end())
            m_ModifiedSkus.erase(thisModSku);

        auto skuData = m_SkuData.find(newDisabledSkuId);

        // If the original value of field "Enabled" is true, add the sku as "to be disabled".
        if (skuData == m_SkuData.end() || skuData->second.OldIsAssignedBoolField.GetValueBool() == true)
            m_DisabledSkus.insert(newDisabledSkuId);
    }

    for (const auto& newModifiedSku : p_NewMod.m_ModifiedSkus)
    {
        const auto& newModifiedSkuId = newModifiedSku.first;
        auto thisDisabledSku = m_DisabledSkus.find(newModifiedSkuId);
        if (thisDisabledSku != m_DisabledSkus.end())
            m_DisabledSkus.erase(newModifiedSkuId);

        m_ModifiedSkus[newModifiedSkuId] = newModifiedSku.second;
    }

    // Copy any new item in m_SkuData or m_PlanData in this object
    for (const auto& newSkuData : p_NewMod.m_SkuData)
    {
        auto oldSkuData = m_SkuData.find(newSkuData.first);
        if (oldSkuData == m_SkuData.end())
            m_SkuData[newSkuData.first] = newSkuData.second;
    }

    for (const auto& newPlanData : p_NewMod.m_PlanData)
    {
        auto oldPlanData = m_PlanData.find(newPlanData.first);
        if (oldPlanData == m_PlanData.end())
            m_PlanData[newPlanData.first] = newPlanData.second;
    }

    // Merge delta maps
    for (const auto& otherSku : p_NewMod.m_SkusDelta)
    {
        const auto& otherSkuId = otherSku.first;
        const auto& otherSkuDelta = otherSku.second;
        
        auto itt = m_SkusDelta.find(otherSkuId);
        if (itt != m_SkusDelta.end())
            m_SkusDelta[otherSkuId] += otherSkuDelta;
        else
            m_SkusDelta[otherSkuId] = otherSkuDelta;
    }

	if (p_NewMod.m_UsageLocationOld != p_NewMod.m_UsageLocationNew)
		AddUsageLocationChange(p_NewMod.m_UsageLocationOld, p_NewMod.m_UsageLocationNew);
}

void UserLicenseModification::AddSkuToEnable(PooledString p_SkuId, const row_pk_t& p_SkuRowPk, bool p_MarkInGrid)
{
    ASSERT(m_DisabledSkus.find(p_SkuId) == m_DisabledSkus.end());
    if (m_ModifiedSkus.find(p_SkuId) == m_ModifiedSkus.end())
    {
        auto skuRow = m_Grid->GetRowIndex().GetExistingRow(p_SkuRowPk);
        ASSERT(nullptr != skuRow);
        if (nullptr != skuRow)
        {
			m_ModifiedSkus[p_SkuId] = {};

            LicenseModifiedInfo info;
            info.Pk						= p_SkuRowPk;
            info.OldIsAssignedField		= skuRow->GetField(m_Grid->GetColIsAssigned());
            info.OldIsAssignedBoolField = skuRow->GetField(m_Grid->GetColIsAssignedBool());

            m_SkuData[p_SkuId] = info;

            if (p_MarkInGrid)
			{
				ASSERT(m_SkusDelta.find(p_SkuId) == m_SkusDelta.end()); // We can't enable the same sku twice for a user
				if (m_SkusDelta.find(p_SkuId) == m_SkusDelta.end())
					m_SkusDelta[p_SkuId] = 1;
				else
					++m_SkusDelta[p_SkuId];
				ASSERT(m_SkusDelta[p_SkuId] == 1); // We can't enable the same sku twice for a user

				skuRow->ClearStatus();
				skuRow->AddField(YtriaTranslate::Do(UserLicenseModification_AddSkuToEnable_1, _YLOC("[Sku marked for activation]")).c_str(), m_Grid->GetColIsAssigned(), m_Grid->GetSkuMarkedForActivationId());
				if (skuRow->AddField(true, m_Grid->GetColIsAssignedBool()).IsModified())
					skuRow->SetCollapsed(false);
				m_Grid->OnRowModified(skuRow);
			}
        }
    }
}

void UserLicenseModification::AddSkuToDisable(PooledString p_SkuId, const row_pk_t& p_SkuRowPk, bool p_MarkInGrid)
{
    ASSERT(m_ModifiedSkus.find(p_SkuId) == m_ModifiedSkus.end());

    auto skuRow = m_Grid->GetRowIndex().GetExistingRow(p_SkuRowPk);
    ASSERT(nullptr != skuRow);
    if (nullptr != skuRow)
    {
        m_DisabledSkus.insert(p_SkuId);

        LicenseModifiedInfo info;
        info.Pk						= p_SkuRowPk;
        info.OldIsAssignedField		= skuRow->GetField(m_Grid->GetColIsAssigned());
        info.OldIsAssignedBoolField = skuRow->GetField(m_Grid->GetColIsAssignedBool());

        m_SkuData[p_SkuId] = info;

        if (p_MarkInGrid)
		{
			ASSERT(m_SkusDelta.find(p_SkuId) == m_SkusDelta.end()); // We can't disable the same sku twice for a user
			if (m_SkusDelta.find(p_SkuId) == m_SkusDelta.end())
				m_SkusDelta[p_SkuId] = -1;
			else
				--m_SkusDelta[p_SkuId];
			ASSERT(m_SkusDelta[p_SkuId] == -1); // We can't disable the same sku twice for a user

			skuRow->ClearStatus();
			skuRow->AddField(YtriaTranslate::Do(UserLicenseModification_AddSkuToDisable_1, _YLOC("[Sku marked for deactivation]")).c_str(), m_Grid->GetColIsAssigned(), m_Grid->GetSkuMarkedForDeactivationId());
			skuRow->AddField(false, m_Grid->GetColIsAssignedBool());
			m_Grid->OnRowModified(skuRow);
		}
    }
}

void UserLicenseModification::AddPlanToDisable(PooledString p_SkuId, PooledString p_PlanId, const row_pk_t& p_PlanRowPk, bool p_MarkInGrid)
{
    auto planRow = m_Grid->GetRowIndex().GetExistingRow(p_PlanRowPk);
    ASSERT(nullptr != planRow);
    if (nullptr != planRow)
    {
        if (m_ModifiedSkus.find(p_SkuId) != m_ModifiedSkus.end())
            m_ModifiedSkus[p_SkuId].insert(p_PlanId);

        LicenseModifiedInfo info;
        info.Pk						= p_PlanRowPk;
        info.OldIsAssignedField		= planRow->GetField(m_Grid->GetColIsAssigned());
        info.OldIsAssignedBoolField = planRow->GetField(m_Grid->GetColIsAssignedBool());

        m_PlanData[std::make_pair(p_SkuId, p_PlanId)] = info;
        if (p_MarkInGrid)
		{
			planRow->ClearStatus();
			planRow->AddField(YtriaTranslate::Do(UserLicenseModification_AddPlanToDisable_1, _YLOC("[Plan marked for deactivation]")).c_str(), m_Grid->GetColIsAssigned(), m_Grid->GetPlanMarkedForDeactivationId());
			planRow->AddField(false, m_Grid->GetColIsAssignedBool());
			m_Grid->OnRowModified(planRow);
		}
    }
}

void UserLicenseModification::AddPlanToEnable(PooledString p_SkuId, PooledString p_PlanId, const row_pk_t& p_PlanRowPk, bool p_MarkInGrid)
{
    auto ittSku = m_ModifiedSkus.find(p_SkuId);

    // Erase from disabled plans (in case)
    if (ittSku != m_ModifiedSkus.end())
    {
        const auto nbErased = ittSku->second.erase(p_PlanId);
        ASSERT(nbErased == 0); // If this asserts, it means at some point we added the plan as to be disabled. Probably an error.
    }

    auto planRow = m_Grid->GetRowIndex().GetExistingRow(p_PlanRowPk);
    ASSERT(nullptr != planRow);
    if (nullptr != planRow)
    {
        LicenseModifiedInfo info;
        info.Pk						= p_PlanRowPk;
        info.OldIsAssignedField		= planRow->GetField(m_Grid->GetColIsAssigned());
        info.OldIsAssignedBoolField = planRow->GetField(m_Grid->GetColIsAssignedBool());

        m_PlanData[std::make_pair(p_SkuId, p_PlanId)] = info;
        if (p_MarkInGrid)
		{
			planRow->ClearStatus();
			planRow->AddField(YtriaTranslate::Do(UserLicenseModification_AddPlanToEnable_1, _YLOC("[Plan marked for activation]")).c_str(), m_Grid->GetColIsAssigned(), m_Grid->GetPlanMarkedForActivationId()); // We don't know which icon is going to appear after refresh
			planRow->AddField(true, m_Grid->GetColIsAssignedBool());
			m_Grid->OnRowModified(planRow);
		}
   }
}

PooledString UserLicenseModification::GetUserId() const
{
    return m_UserId;
}

const map<PooledString, std::set<PooledString>>& UserLicenseModification::GetModifiedSkus() const
{
    return m_ModifiedSkus;
}

const std::set<PooledString>& UserLicenseModification::GetDisabledSkus() const
{
    return m_DisabledSkus;
}

void UserLicenseModification::AddUsageLocationChange(const boost::YOpt<PooledString>& p_Old, const boost::YOpt<PooledString>& p_New)
{
	if (!m_UsageLocationNew)
	{
		ASSERT(!m_UsageLocationOld);
		m_UsageLocationOld = p_Old;
		m_UsageLocationNew = p_New;
	}
	else if (p_New == m_UsageLocationOld)
	{
		ASSERT(p_Old == m_UsageLocationNew);
		m_UsageLocationOld.reset();
		m_UsageLocationNew.reset();
	}
	else
	{
		ASSERT(p_Old == m_UsageLocationNew);
		m_UsageLocationNew = p_New;
	}
}

bool UserLicenseModification::HasUsageLocationChange() const
{
	return m_UsageLocationNew.is_initialized();
}

const boost::YOpt<PooledString>& UserLicenseModification::GetNewUsageLocation()
{
	return m_UsageLocationNew;
}

const boost::YOpt<PooledString>& UserLicenseModification::GetOldUsageLocation()
{
	return m_UsageLocationOld;
}

vector<Modification::ModificationLog> UserLicenseModification::GetModificationLogs() const
{
	vector<Modification::ModificationLog> mlogs;

	auto pk = GridBackendUtil::ToString(GetRowKey());
	if (HasUsageLocationChange())
		mlogs.push_back(ModificationLog(pk, GetObjectName(), ActivityLoggerUtil::g_ActionUpdate, m_Grid->GetColMetaUsageLocation()->GetTitleDisplayed(), m_UsageLocationOld.is_initialized() ? m_UsageLocationOld.get() : _YTEXT(""), m_UsageLocationNew.get(), GetState()));

	for (const auto& skuData : m_SkuData)
	{
		const auto& skuId = skuData.first;
		const bool	enabledOld = skuData.second.OldIsAssignedBoolField.GetValueBool();

		auto skuPartNumber = m_Grid->GetSkuPartNumber(skuId);

		mlogs.push_back(ModificationLog(pk, YtriaTranslate::Do(UserLicenseModification_GetModificationLogs_1, _YLOC("SKU : %1"), skuPartNumber.c_str()), ActivityLoggerUtil::g_ActionUpdate, m_Grid->GetColIsAssigned()->GetTitleDisplayed(), enabledOld ? _YTEXT("ENABLED") : _YTEXT("DISABLED"), enabledOld ? _YTEXT("DISABLED") : _YTEXT("ENABLED"), GetState()));
	}

	for (const auto& modPlan : m_PlanData)
	{
		const bool enabledOld = modPlan.second.OldIsAssignedBoolField.GetValueBool();

		auto planNumber = m_Grid->GetPlanPartNumber(modPlan.first.first, modPlan.first.second);

		mlogs.push_back(ModificationLog(pk, YtriaTranslate::Do(UserLicenseModification_GetModificationLogs_2, _YLOC("Plan : %1"), planNumber.c_str()), ActivityLoggerUtil::g_ActionUpdate, m_Grid->GetColIsAssigned()->GetTitleDisplayed(), enabledOld ? _YTEXT("ENABLED") : _YTEXT("DISABLED"), enabledOld ? _YTEXT("DISABLED") : _YTEXT("ENABLED"), GetState()));
	}

	/*f (HasUsageLocationChange())
		mlogs.push_back(ModificationLog(pk, GetObjectName(), ActivityLoggerUtil::g_ActionUpdate, m_Grid->m_ColumnMetaUserUsageLocation->GetTitleDisplayed(), m_UsageLocationOld.is_initialized() ? m_UsageLocationOld.get() : _YTEXT(""), m_UsageLocationNew.get(), GetState()));

	for (const auto& modPlan : m_SkuData)
	{
		const auto& skuId = modPlan.first;
		const bool	enabledOld = modPlan.second.OldIsAssignedBoolField.GetValueBool();
		mlogs.push_back(ModificationLog(pk, GetObjectName(), ActivityLoggerUtil::g_ActionUpdate, m_Grid->GetColIsAssigned()->GetTitleDisplayed(), enabledOld ? _YTEXT("ENABLED") : _YTEXT("DISABLED"), enabledOld ? _YTEXT("DISABLED") : _YTEXT("ENABLED"), GetState()));
	}

	for (const auto& modPlan : m_PlanData)
	{
		const auto& SKUandPLAN = modPlan.first;
		const bool enabledOld = modPlan.second.OldIsAssignedBoolField.GetValueBool();
		mlogs.push_back(ModificationLog(pk, GetObjectName(), ActivityLoggerUtil::g_ActionUpdate, m_Grid->GetColIsAssigned()->GetTitleDisplayed(), enabledOld ? _YTEXT("ENABLED") : _YTEXT("DISABLED"), enabledOld ? _YTEXT("DISABLED") : _YTEXT("ENABLED"), GetState()));
	}*/

	return mlogs;

}
