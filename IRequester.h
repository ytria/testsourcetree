#pragma once

#include "YtriaTaskData.h"

class Sapio365Session;

class IRequester
{
public:
    IRequester() = default;

    virtual TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) = 0;
    virtual ~IRequester() = default;
};

