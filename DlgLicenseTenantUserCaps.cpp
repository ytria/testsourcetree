#include "DlgLicenseTenantUserCaps.h"

#include "JSONUtil.h"
#include "LicenseUtil.h"
#include "MainFrameSapio365Session.h"
#include "MFCUtil.h"
#include "Sapio365Session.h"

std::map<wstring, int, Str::keyLessInsensitive> DlgLicenseTenantUserCaps::g_Sizes;
const wstring DlgLicenseTenantUserCaps::g_JSONBtnNameCancel = _YTEXT("BTN_CANCEL");
const wstring DlgLicenseTenantUserCaps::g_JSONBtnNameOK		= _YTEXT("BTN_OK");
const wstring DlgLicenseTenantUserCaps::g_JSONTenantKey		= _YTEXT("tenant");
const wstring DlgLicenseTenantUserCaps::g_JSONQuantityKey	= _YTEXT("quantity");
const wstring DlgLicenseTenantUserCaps::g_JSONKeyValidation = _YTEXT("KeyValidation");

DlgLicenseTenantUserCaps::DlgLicenseTenantUserCaps(const VendorLicense& p_License, const uint32_t p_MaxQuantity, CWnd* p_Parent) :
	ResizableDialog(IDD, p_Parent),
	m_License(p_License),
	m_HtmlView(make_unique<YAdvancedHtmlView>(true)),
	m_MaxQuantity(p_MaxQuantity)
{
	const bool success = Str::LoadSizeFile(IDR_LICENSETENANTNAMES_SIZE, g_Sizes);
	ASSERT(success);
}

bool DlgLicenseTenantUserCaps::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	bool RetVal = true;

	m_UserTenantCaps.clear();
	ASSERT(data.size() >= 1);
	if (data.size() >= 1)
	{
		const wstring& BtnClick = data.begin()->first;

		if (BtnClick == g_JSONBtnNameCancel)
		{
			endDialogFixed(IDCANCEL);
			RetVal = false;
		}
		else if (BtnClick == g_JSONBtnNameOK)
		{
			vector<wstring>		tenantNames;
			vector<uint32_t>	userCaps;
			tenantNames.resize(LicenseUtil::g_MaxTenants);
			userCaps.resize(LicenseUtil::g_MaxTenants, 0);
			size_t k = 0;
			for (const auto& d : data)
			{
				bool hn = false;
				if (wcsncmp(d.first.c_str(), g_JSONTenantKey.c_str(), g_JSONTenantKey.length()) == 0)
				{
					size_t tenantIndex = static_cast<size_t>(Str::GetFirstNumber(d.first, hn));
					ASSERT(hn);
					if (hn)
						tenantNames[tenantIndex] = Str::lrtrim(d.second);
				}
				else if (wcsncmp(d.first.c_str(), g_JSONQuantityKey.c_str(), g_JSONQuantityKey.length()) == 0)
				{
					size_t tenantIndex = static_cast<size_t>(Str::GetFirstNumber(d.first, hn));
					ASSERT(hn);
					if (hn)
						userCaps[tenantIndex] = static_cast<uint32_t>(Str::GetNumber(d.second));
				}
				else if (MFCUtil::StringMatchW(d.first, g_JSONKeyValidation))
					m_KeyValidation = d.second;
			}
			for (size_t k = 0; k < tenantNames.size(); k++)
				if (!tenantNames[k].empty())
					m_UserTenantCaps.push_back(tenantNames[k], userCaps[k]);

			endDialogFixed(IDOK);
			RetVal = false;
		}
	}

	return RetVal;
}

const LinearMap<wstring, uint32_t>& DlgLicenseTenantUserCaps::GetUserTenantCaps() const
{
	return m_UserTenantCaps;
}

const wstring& DlgLicenseTenantUserCaps::GetKeyValidation() const
{
	return m_KeyValidation;
}

bool DlgLicenseTenantUserCaps::OnNewPostedURL(const wstring& url)
{
	ASSERT(false);
	return true;
}

BOOL DlgLicenseTenantUserCaps::OnInitDialogSpecificResizable()
{
	SetWindowText(YtriaTranslate::Do(DlgLicenseTenantUserCaps_OnInitDialogSpecificResizable_1, _YLOC("License Configuration Settings")).c_str());

	CWnd* pCtrl = GetDlgItem(IDC_DLG_TENANTCAPS_HTML);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_DLG_TENANTCAPS_HTML, NULL);

	// FIXME: Temporary - Remove when no more needed.
	//m_HtmlView->SetDebugDisplayPostedData(true);

	m_HtmlView->SetPostedDataTarget(this);
	m_HtmlView->SetPostedURLTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
	{
		{ _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } },
		{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-licenseTenantNames.js"), _YTEXT("") } }, // This file is in resources
		{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("licenseTenantNames.js"), _YTEXT("") } } // This file is in resources
	}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("licenseTenantNames.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-height"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-width"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);

	return TRUE;
}

void DlgLicenseTenantUserCaps::generateJSONScriptData(wstring& p_Output)
{
	const bool noUserCount			= m_License.IsNoUserCount();// NUC
	const auto fguc					= m_License.GetFeatureGlobalUserCount();// GUC
	const bool poolNUCwithTenants	= m_License.IsNoUserCountPoolWithTenants();// GUC contains tenant count

	auto existingTenantUserCountFeatures = m_License.GetFeatures(Counter, ExtraLicenseConfig::Type::TENANT);
	vector<web::json::value> dlgJsonItems;

	std::shared_ptr<Sapio365Session> session = MainFrameSapio365Session();
	const wstring defaultTenant = session ? session->GetRBACTenantName(true) : Str::g_EmptyString;

	wstring footer;
	if (poolNUCwithTenants)
	{
		if (m_MaxQuantity>1)
			footer = _YFORMAT(L"You may link up to %s tenants to your license. Just enter the tenant names.<BR><BR>You can update these settings a maximum of once a month.", Str::getStringFromNumber(m_MaxQuantity).c_str());
		else
			footer = _T("You can link just one tenant to your license. Just enter the tenant name.<BR><BR>You can update these settings a maximum of once a month.");

		uint32_t k = 0;
		for (const auto& p : m_License.GetAgilityTenants())
		{
			web::json::value tenantCap;
			tenantCap[_YTEXT("hbs")]	= web::json::value(_YTEXT("oneTenant"));
			tenantCap[_YTEXT("id")]		= web::json::value(Str::getStringFromNumber<uint32_t>(k));
			tenantCap[g_JSONTenantKey]	= web::json::value(p.second);
			dlgJsonItems.push_back(tenantCap);
			k++;
		}
	}
	else
	{
		footer = YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_10, _YLOC("You may link more than one tenant to your license. Just enter the tenant name and the total number of users in the tenant.<BR><BR>Please make sure that the total user amount you enter for each tenant is equal to or greater than the actual number of users in the tenant. Otherwise sapio365 will not function properly.<BR><BR>You can update these settings a maximum of once a month."));
		int k = 0;
		for (const auto& f : existingTenantUserCountFeatures)
		{
			if (f.IsAuthorized() && !f.GetPoolContextValue().empty() && f.GetPoolMax() != LicenseUtil::g_NoValueUINT32)
			{
				bool hasNumber		= false;
				auto featureIndex	= static_cast<int>(Str::GetFirstNumber(f.m_Code, hasNumber));
				ASSERT(hasNumber);
				if (!hasNumber)
					featureIndex = k;
				web::json::value tenantCap;
				tenantCap[_YTEXT("hbs")]		= web::json::value(_YTEXT("oneTenant"));
				tenantCap[_YTEXT("id")]			= web::json::value(Str::getStringFromNumber(featureIndex));
				tenantCap[g_JSONTenantKey]		= web::json::value(f.GetPoolContextValue());
				tenantCap[g_JSONQuantityKey]	= web::json::value(Str::getStringFromNumber(f.GetPoolMax()));
				dlgJsonItems.push_back(tenantCap);
			}
			k++;
		}
	}

	if (dlgJsonItems.empty())
	{
		// pre-load form (1st use, already logged in)
		wstring defaultGUC;

		if (fguc.GetPoolMax() > 0)
			defaultGUC = Str::getStringFromNumber(fguc.GetPoolMax());

		web::json::value tenantCap;
		tenantCap[_YTEXT("hbs")]		= web::json::value(_YTEXT("oneTenant"));
		tenantCap[_YTEXT("id")]			= web::json::value(_YTEXT("0"));
		tenantCap[g_JSONTenantKey]		= web::json::value(defaultTenant);
		tenantCap[g_JSONQuantityKey]	= web::json::value(defaultGUC);
		dlgJsonItems.push_back(tenantCap);
	}

	dlgJsonItems.push_back(JSON_BEGIN_OBJECT
								JSON_PAIR_KEY(_YTEXT("hbs")) JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Button"))),
								JSON_BEGIN_PAIR
									   _YTEXT("choices"),
										JSON_BEGIN_ARRAY
											JSON_BEGIN_OBJECT
												JSON_PAIR_KEY(_YTEXT("type"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
												JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Apply"))),
												JSON_PAIR_KEY(_YTEXT("attribute"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("post"))),
												JSON_PAIR_KEY(_YTEXT("name"))		JSON_PAIR_VALUE(JSON_STRING(g_JSONBtnNameOK)),
											JSON_END_OBJECT,
											JSON_BEGIN_OBJECT
												JSON_PAIR_KEY(_YTEXT("type"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
												JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Cancel"))),
												JSON_PAIR_KEY(_YTEXT("attribute"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("cancel"))),
												JSON_PAIR_KEY(_YTEXT("name"))		JSON_PAIR_VALUE(JSON_STRING(g_JSONBtnNameCancel)),
										   JSON_END_OBJECT
									   JSON_END_ARRAY
								   JSON_END_PAIR
						   JSON_END_OBJECT);


	auto maxQty = Str::getStringFromNumber(m_MaxQuantity);
	json::value root =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("main"),
				JSON_BEGIN_OBJECT
					JSON_PAIR_KEY(_YTEXT("id"))						JSON_PAIR_VALUE(JSON_STRING(_YTEXT("DlgLicenseTenantUserCaps"))),
					JSON_PAIR_KEY(_YTEXT("method"))					JSON_PAIR_VALUE(JSON_STRING(_YTEXT("POST"))),
					JSON_PAIR_KEY(_YTEXT("action"))					JSON_PAIR_VALUE(JSON_STRING(_YTEXT("#"))),
					JSON_PAIR_KEY(_YTEXT("maxQuantity"))			JSON_PAIR_VALUE(JSON_STRING(maxQty)),
					JSON_PAIR_KEY(_YTEXT("currentTenant"))			JSON_PAIR_VALUE(JSON_STRING(defaultTenant)),
					JSON_PAIR_KEY(_YTEXT("exceedMessage"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_11, _YLOC("The total of entered quantities exceeds the maximum in the current license: %1"), maxQty.c_str())))),
					JSON_PAIR_KEY(_YTEXT("labelIntro"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_1, _YLOC("Welcome to sapio365!")).c_str()))),
					JSON_PAIR_KEY(_YTEXT("labelSubIntro"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_2, _YLOC("Please configure the user allocation for your linked Office 365 tenants.")).c_str()))),
					JSON_PAIR_KEY(_YTEXT("tenantName_PlaceHolder"))	JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_3, _YLOC("Tenant name")).c_str()))),
					JSON_PAIR_KEY(_YTEXT("tenantName_popup"))		JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgTenantInfo_generateJSONScriptData_3, _YLOC("mytenant.onmicrosoft.com")).c_str())),
					JSON_PAIR_KEY(_YTEXT("quantity_PlaceHolder"))	JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_4, _YLOC("User count")).c_str()))),
					JSON_PAIR_KEY(_YTEXT("totalMsg"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_5, _YLOC("Total")).c_str()))),
					JSON_PAIR_KEY(_YTEXT("maxMsg"))					JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_6, _YLOC("Maximum tenant user capacity for this license")).c_str()))),
					JSON_PAIR_KEY(_YTEXT("adduserMsg"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_7, _YLOC("Add Tenant")).c_str()))),
					JSON_PAIR_KEY(_YTEXT("removeusrMsg"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_8, _YLOC("Remove Last Tenant")).c_str()))),
					JSON_PAIR_KEY(_YTEXT("warningIcon"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("fas fa-sign-in"))),
					JSON_PAIR_KEY(_YTEXT("keyValidation"))			JSON_PAIR_VALUE(JSON_STRING(g_JSONKeyValidation)),
					JSON_PAIR_KEY(_YTEXT("keyVal_popup"))			JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Sxxxxxxxx"))),
					JSON_PAIR_KEY(_YTEXT("warningMsg"))				JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_9, _YLOC("To verify you are the authorised user, please enter the subscription code (Sxxxxxxxx) for this license.")).c_str()))),
					JSON_PAIR_KEY(_YTEXT("warningSubMsg"))			JSON_PAIR_VALUE(JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgLicenseUpgrade_generateJSONScriptData_20, _YLOC("Your subscription code is located on your invoice below the cleverbridge reference number.")).c_str()))),
					JSON_PAIR_KEY(_YTEXT("noQuantity"))				JSON_PAIR_VALUE(JSON_STRING(noUserCount ? _YTEXT("X") : _YTEXT(""))),
					//have to put this in one line due to localization reason
					JSON_PAIR_KEY(_YTEXT("labelFooter"))			JSON_PAIR_VALUE(JSON_STRING(footer))
				JSON_END_OBJECT
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("items"),
				web::json::value::array(dlgJsonItems)
			JSON_END_PAIR
		JSON_END_OBJECT;

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}