#include "DelegateUserSerializer.h"

#include "DelegatePermissionsSerializer.h"
#include "DelegateUser.h"
#include "XmlSerializeUtil.h"
#include "UserIdSerializer.h"

Ex::DelegateUserSerializer::DelegateUserSerializer(XERCES_CPP_NAMESPACE::DOMDocument& p_Document, const Ex::DelegateUser& p_DelegateUser)
    : IXmlSerializer(p_Document),
    m_DelegateUser(p_DelegateUser)
{
}

void Ex::DelegateUserSerializer::SerializeImpl(DOMElement& p_Element)
{
    XmlSerializeUtil::SerializeObject(Ex::UserIdSerializer(m_Document, m_DelegateUser.m_UserId), p_Element, _YTEXT("t:UserId"));
    if (m_DelegateUser.m_DelegatePermissions != boost::none)
        XmlSerializeUtil::SerializeObject(Ex::DelegatePermissionsSerializer(m_Document, *m_DelegateUser.m_DelegatePermissions), p_Element, _YTEXT("t:DelegatePermissions"));
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("ReceiveCopiesOfMeetingMessages"), m_DelegateUser.m_ReceiveCopiesOfMeetingMessages);
    XmlSerializeUtil::SerializePrimitive(m_Document, p_Element, _YTEXT("ViewPrivateItems"), m_DelegateUser.m_ViewPrivateItems);
}
