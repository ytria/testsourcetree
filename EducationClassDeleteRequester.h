#pragma once

#include "IRequester.h"
#include "HttpResultWithError.h"

class EducationClassDeleteRequester : public IRequester
{
public:
	EducationClassDeleteRequester(const wstring& p_ClassId);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const HttpResultWithError& GetResult() const;

private:
	std::shared_ptr<HttpResultWithError> m_Result;
	wstring m_ClassId;
};

