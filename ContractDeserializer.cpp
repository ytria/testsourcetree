#include "ContractDeserializer.h"

#include "JsonSerializeUtil.h"

void ContractDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("contractType"), m_Data.m_ContractType, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("customerId"), m_Data.m_CustomerId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("defaultDomainName"), m_Data.m_DefaultDomainName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
}
