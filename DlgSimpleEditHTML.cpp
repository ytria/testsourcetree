#include "DlgSimpleEditHTML.h"

const wstring DlgSimpleEditHTML::g_PropName = _YTEXT("Input");

DlgSimpleEditHTML::DlgSimpleEditHTML(CWnd* p_Parent)
	: DlgBusinessEditHTML(DlgBusinessEditHTML::Action::EDIT, p_Parent)
{

}

DlgSimpleEditHTML::~DlgSimpleEditHTML()
{

}

const wstring& DlgSimpleEditHTML::GetText() const
{
	return m_Text;
}

void DlgSimpleEditHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgSimpleEditHTML"));
	addStringEditor(g_PropName, YtriaTranslate::Do(GridFrameBase_customizeGridColumnRibbonTab_10, _YLOC("Enter text:")).c_str(), _YTEXT(""), DlgBusinessEditHTML::DIRECT_EDIT, _YTEXT(""));
	addApplyButton(YtriaTranslate::Do(CosmosUtil__detail_DeleteCosmosDbAccountImpl_1, _YLOC("OK")).c_str());
	addCancelButton(YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str());
}

bool DlgSimpleEditHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	ASSERT(data.size() == 1);
	for (const auto& item : data)
	{
		ASSERT(item.first == g_PropName);
		if (item.first == g_PropName)
		{
			m_Text = item.second;
			break;
		}
	}
	return true;
}

wstring DlgSimpleEditHTML::getDialogTitle() const
{
	return YtriaTranslate::Do(YtriaGridFields_LocalizeStaticMembers_7, _YLOC("Input")).c_str();
}
