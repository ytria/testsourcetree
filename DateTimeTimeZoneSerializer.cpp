#include "DateTimeTimeZoneSerializer.h"

#include "JsonSerializeUtil.h"

DateTimeTimeZoneSerializer::DateTimeTimeZoneSerializer(const DateTimeTimeZone& p_DateTimeTimeZone)
    : m_Dttz(p_DateTimeTimeZone)
{
}

void DateTimeTimeZoneSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    JsonSerializeUtil::SerializeTimeDate(_YTEXT("dateTime"), m_Dttz.DateTime, obj);
    JsonSerializeUtil::SerializeString(_YTEXT("timeZone"), m_Dttz.TimeZone, obj);
}
