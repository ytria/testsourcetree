#include "RecurrenceRangeDeserializer.h"

#include "JsonSerializeUtil.h"

void RecurrenceRangeDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeDateOnly(_YTEXT("endDate"), m_Data.EndDate, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("numberOfOccurrences"), m_Data.NumberOfOccurences, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("recurrenceTimeZone"), m_Data.RecurrenceTimeZone, p_Object);
	JsonSerializeUtil::DeserializeDateOnly(_YTEXT("startDate"), m_Data.StartDate, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.Type, p_Object);
}
