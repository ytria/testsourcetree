#pragma once

#include "IOAuth2SessionPersister.h"
#include "PersistentSession.h"

class DbSessionPersister : public IOAuth2SessionPersister
{
public:
	DbSessionPersister(const PersistentSession& p_Session);

	bool Save(const oauth2_config& p_Token) override;
	bool Load(oauth2_config& p_Token) override;

private:
	int64_t m_SessionId;
	PersistentSession m_Session;
};

