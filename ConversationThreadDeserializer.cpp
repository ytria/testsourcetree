#include "ConversationThreadDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "MsGraphFieldNames.h"
#include "RecipientDeserializer.h"

void ConversationThreadDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT(O365_ID), m_Data.m_Id, p_Object, true);
    
    {
        ListDeserializer<Recipient, RecipientDeserializer> deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("toRecipients"), p_Object))
            m_Data.SetToRecipients(deserializer.GetData());
    }

    {
        ListDeserializer<Recipient, RecipientDeserializer> deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("ccRecipients"), p_Object))
            m_Data.SetCcRecipients(deserializer.GetData());
    }
    
    JsonSerializeUtil::DeserializeString(_YTEXT("topic"), m_Data.m_Topic, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("hasAttachments"), m_Data.m_HasAttachments, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastDeliveredDateTime"), m_Data.m_LastDeliveredDateTime, p_Object);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("uniqueSenders"), p_Object))
            m_Data.SetUniqueSenders(lsd.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("preview"), m_Data.m_Preview, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isLocked"), m_Data.m_IsLocked, p_Object);
}
