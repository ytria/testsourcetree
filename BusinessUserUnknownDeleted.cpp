#include "BusinessUserUnknownDeleted.h"

#include "BusinessGroup.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessUserUnknownDeleted>("Deleted Unknown") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Deleted Unknown"))))
		.constructor()(policy::ctor::as_object)
	;
}
