#include "TextColumnDeserializer.h"

#include "JsonSerializeUtil.h"

void TextColumnDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowMultipleLines"), m_Data.AllowMultipleLines, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("appendChangesToExistingText"), m_Data.AppendChangesToExistingText, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("linesForEditing"), m_Data.LinesForEditing, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("maxLength"), m_Data.MaxLength, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("textType"), m_Data.TextType, p_Object);
}
