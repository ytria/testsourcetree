#include "EducationClassAddToSchoolRequester.h"
#include "LoggerService.h"
#include "O365AdminUtil.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

EducationClassAddToSchoolRequester::EducationClassAddToSchoolRequester(const wstring& p_SchoolId, const wstring& p_ClassId)
	: m_SchoolId(p_SchoolId),
	m_ClassId(p_ClassId)
{
}

TaskWrapper<void> EducationClassAddToSchoolRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("schools"));
	uri.append_path(m_SchoolId);
	uri.append_path(_YTEXT("classes"));
	uri.append_path(_YTEXT("$ref"));

	LoggerService::User(_YFORMAT(L"Adding class with id %s to school with id %s", m_ClassId.c_str(), m_SchoolId.c_str()), p_TaskData.GetOriginator());

	web::json::value json = web::json::value::object();
	json.as_object()[_YTEXT("@odata.id")] = web::json::value::string(_YTEXTFORMAT(L"https://graph.microsoft.com/v1.0/education/classes/%s", m_ClassId.c_str()));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_uri(), httpLogger, p_TaskData, WebPayloadJSON(json)).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResInfo) {
		*result = Util::GetResult(p_ResInfo);
	});
}

const HttpResultWithError& EducationClassAddToSchoolRequester::GetResult() const
{
	return *m_Result;
}
