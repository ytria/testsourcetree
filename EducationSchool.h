#pragma once

#include "BusinessObject.h"

#include "EducationClass.h"
#include "IdentitySet.h"
#include "PhysicalAddress.h"

class EducationSchool : public BusinessObject
{
public:

	// Non properties
	vector<EducationClass> m_Classes;

	bool operator==(const EducationSchool& p_Other) const;
	bool operator<(const EducationSchool& p_Other) const;

	const boost::YOpt<PooledString>& GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetDescription() const;
	void SetDescription(const boost::YOpt<PooledString>& p_Val);
	
	const boost::YOpt<PooledString>& GetStatus() const;
	void SetStatus(const boost::YOpt<PooledString>& p_Val);
	
	const boost::YOpt<PooledString>& GetExternalSource() const;
	void SetExternalSource(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetPrincipalEmail() const;
	void SetPrincipalEmail(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetPrincipalName() const;
	void SetPrincipalName(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetExternalPrincipalId() const;
	void SetExternalPrincipalId(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetPhone() const;
	void SetPhone(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetFax() const;
	void SetFax(const boost::YOpt<PooledString>& p_Val);


	const boost::YOpt<PooledString>& GetHighestGrade() const;
	void SetHighestGrade(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetLowestGrade() const;
	void SetLowestGrade(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetSchoolNumber() const;
	void SetSchoolNumber(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetExternalId() const;
	void SetExternalId(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<IdentitySet>& GetCreatedBy() const;
	void SetCreatedBy(const boost::YOpt<IdentitySet>& p_Val);

	const boost::YOpt<PhysicalAddress>& GetAddress() const;
	void SetAddress(const boost::YOpt<PhysicalAddress>& p_Val);

	const boost::YOpt<PooledString>& GetAddressCity() const;
	void SetAddressCity(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetAddressCountryOrRegion() const;
	void SetAddressCountryOrRegion(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetAddressPostalCode() const;
	void SetAddressPostalCode(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetAddressState() const;
	void SetAddressState(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetAddressStreet() const;
	void SetAddressStreet(const boost::YOpt<PooledString>& p_Val);


private:
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<PooledString> m_Description;
	boost::YOpt<PooledString> m_Status;
	boost::YOpt<PooledString> m_ExternalSource;
	boost::YOpt<PooledString> m_PrincipalEmail;
	boost::YOpt<PooledString> m_PrincipalName;
	boost::YOpt<PooledString> m_ExternalPrincipalId;
	boost::YOpt<PooledString> m_HighestGrade;
	boost::YOpt<PooledString> m_LowestGrade;
	boost::YOpt<PooledString> m_SchoolNumber;
	boost::YOpt<PooledString> m_ExternalId;
	boost::YOpt<PooledString> m_Phone;
	boost::YOpt<PooledString> m_Fax;
	boost::YOpt<PhysicalAddress> m_Address;
	boost::YOpt<IdentitySet> m_CreatedBy;

	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND

	friend class EducationSchoolDeserializer;
};

