#include "OnPremiseExecScriptRequester.h"

#include "IPSObjectCollectionDeserializer.h"
#include "Sapio365Session.h"

OnPremiseExecScriptRequester::OnPremiseExecScriptRequester(const wstring& p_Script, const std::shared_ptr<IRequestLogger>& p_RequestLogger)
	: m_Script(p_Script)
	, m_Logger(p_RequestLogger)
	, m_Result(std::make_shared<InvokeResultWrapper>())
{
}

TaskWrapper<void> OnPremiseExecScriptRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	return YSafeCreateTaskMutable([deserializer = m_Deserializer, result = m_Result, session = p_Session->GetBasicPowershellSession(), logger = m_Logger, taskData = p_TaskData, script = m_Script]() {
		if (nullptr != session)
		{
			logger->Log(taskData.GetOriginator());

			session->AddScriptStopOnError(script.c_str(), taskData.GetOriginator());

			result->SetResult(session->Invoke(taskData.GetOriginator()));
			if (nullptr != deserializer && result->Get().m_Success && nullptr != result->Get().m_Collection)
				result->Get().m_Collection->Deserialize(*deserializer);
		}
	});
}

void OnPremiseExecScriptRequester::SetDeserializer(const std::shared_ptr<IPSObjectCollectionDeserializer>& p_Deserializer)
{
	m_Deserializer = p_Deserializer;
}

const std::shared_ptr<InvokeResultWrapper>& OnPremiseExecScriptRequester::GetResult() const
{
	return m_Result;
}
