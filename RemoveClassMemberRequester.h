#pragma once

#include "IRequester.h"
#include "HttpResultWithError.h"

class RemoveClassMemberRequester : public IRequester
{
public:
	RemoveClassMemberRequester(const wstring& p_ClassId, const wstring& p_UserId);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const HttpResultWithError& GetResult() const;

private:
	std::shared_ptr<HttpResultWithError> m_Result;

	wstring m_ClassId;
	wstring m_UserId;
};

