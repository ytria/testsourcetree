#include "UserNonPropDeserializer.h"

#include "DriveDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "SapioErrorDeserializer.h"
#include "SqlCacheConfig.h"
#include "YtriaFieldsConstants.h"

void UserNonPropDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	// Shouldn't exist anymore.
	ASSERT(p_Object.end() == p_Object.find(_YUID(YTRIA_LASTREQUESTEDDATETIME)));
    //JsonSerializeUtil::DeserializeTimeDate(_YUID(YTRIA_LASTREQUESTEDDATETIME), m_Data.m_LastRequestTime, p_Object);

	// date per block
#define DATE_FIELD_FOR(_blockId_) JsonSerializeUtil::DeserializeTimeDate(SqlCacheConfig<UserCache>::DataBlockDateFieldById((_blockId_)), m_Data.m_DataBlockDates[_blockId_], p_Object);

	DATE_FIELD_FOR(UBI::MIN)
	DATE_FIELD_FOR(UBI::LIST)
	DATE_FIELD_FOR(UBI::SYNCV1)
	DATE_FIELD_FOR(UBI::SYNCBETA)
	DATE_FIELD_FOR(UBI::MAILBOXSETTINGS)
	DATE_FIELD_FOR(UBI::ARCHIVEFOLDERNAME)
	DATE_FIELD_FOR(UBI::DRIVE)
	DATE_FIELD_FOR(UBI::MANAGER)
	DATE_FIELD_FOR(UBI::MAILBOX)
	DATE_FIELD_FOR(UBI::RECIPIENTPERMISSIONS)
	DATE_FIELD_FOR(UBI::ASSIGNEDLICENSES)

#undef DATE_FIELD_FOR

	// Shouldn't exist anymore.
	ASSERT(p_Object.end() == p_Object.find(_YUID(YTRIA_FLAGS)));
    //JsonSerializeUtil::DeserializeUint32(_YUID(YTRIA_FLAGS), m_Data.m_Flags, p_Object);

    JsonSerializeUtil::DeserializeString(_YUID(YTRIA_USER_ARCHIVEFOLDERNAME), m_Data.m_ArchiveFolderName, p_Object);
	JsonSerializeUtil::DeserializeString(_YUID(YTRIA_USER_MANAGER), m_Data.m_ManagerID, p_Object);

	{
		DriveDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_DRIVE), p_Object))
			m_Data.m_Drive = std::move(d.GetData());
	}

	// Mailbox (powershell)
	JsonSerializeUtil::DeserializeBool(_YUID(YTRIA_USER_MAILBOX_DELIVERTOMAILBOXANDFORWARD), m_Data.m_DeliverToMailboxAndForward, p_Object);
	JsonSerializeUtil::DeserializeString(_YUID(YTRIA_USER_MAILBOX_FORWARDINGSMTPADDRESS), m_Data.m_ForwardingSmtpAddress, p_Object);
	JsonSerializeUtil::DeserializeString(_YUID(YTRIA_USER_MAILBOX_TYPE), m_Data.m_MailboxType, p_Object);
	JsonSerializeUtil::DeserializeString(_YUID(YTRIA_USER_MAILBOX_FORWARDINGADDRESS), m_Data.m_ForwardingAddress, p_Object);

	{
		ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(lsd, _YUID(YTRIA_USER_MAILBOX_CAN_SEND_ON_BEHALF), p_Object))
			m_Data.m_MailboxCanSendOnBehalf = std::move(lsd.GetData());
	}
	{
		ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(lsd, _YUID(YTRIA_USER_MAILBOX_CAN_SEND_AS_USER), p_Object))
			m_Data.m_MailboxCanSendAsUser = std::move(lsd.GetData());
	}

	// Errors

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_USER_ARCHIVEFOLDERNAMEERROR), p_Object))
			m_Data.SetArchiveFolderNameError(d.GetData());
	}

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_USER_MAILBOXSETTINGSERROR), p_Object))
			m_Data.SetMailboxSettingsError(d.GetData());
	}

	// Shouldn't exist anymore
	ASSERT(p_Object.end() == p_Object.find(_YUID(YTRIA_ERROR)));
	/*{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_ERROR), p_Object))
			m_Data.m_ErrorMessage = std::move(d.GetData());
	}*/

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_USER_MANAGER_ERROR), p_Object))
			m_Data.m_ManagerError = std::move(d.GetData());
	}

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_DRIVE_ERROR), p_Object))
			m_Data.m_DriveError = std::move(d.GetData());
	}

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_USER_MAILBOX_ERROR), p_Object))
			m_Data.m_MailboxInfoError = std::move(d.GetData());
	}

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_USER_MAILBOX_RECIPIENTPERMISSIONS_ERROR), p_Object))
			m_Data.m_RecipientPermissionsError = std::move(d.GetData());
	}
}
