#include "IMigrationVersion.h"

bool IMigrationVersion::Apply()
{
	return m_Migration.Apply();
}

void IMigrationVersion::AddStep(const Migration::StepType& p_Step)
{
	m_Migration.AddStep(p_Step);
}

bool VersionSourceTarget::operator==(const VersionSourceTarget& p_Other)
{
	return m_SourceVersion == p_Other.m_SourceVersion && m_TargetVersion == p_Other.m_TargetVersion;
}
