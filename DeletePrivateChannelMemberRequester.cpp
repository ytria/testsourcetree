#include "DeletePrivateChannelMemberRequester.h"

#include "O365AdminUtil.h"
#include "MsGraphHttpRequestLogger.h"

DeletePrivateChannelMemberRequester::DeletePrivateChannelMemberRequester(std::wstring p_MemberId, std::wstring p_TeamId, std::wstring p_ChannelId)
	: m_MemberId(p_MemberId)
	, m_TeamId(p_TeamId)
	, m_ChannelId(p_ChannelId)
{

}

TaskWrapper<void> DeletePrivateChannelMemberRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	web::uri_builder uri(_YTEXT("teams"));
	uri.append_path(m_TeamId);
	uri.append_path(_YTEXT("channels"));
	uri.append_path(m_ChannelId);
	uri.append_path(_YTEXT("members"));
	uri.append_path(m_MemberId);

	LoggerService::User(_YFORMAT(L"Deleting member with id %s from channel id %s (group id %s)", m_MemberId.c_str(), m_ChannelId.c_str(), m_TeamId.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Delete(uri.to_uri(), httpLogger, p_TaskData, _YTEXT("beta")).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResInfo) {
		*result = Util::GetResult(p_ResInfo);
	});
}

const HttpResultWithError& DeletePrivateChannelMemberRequester::GetResult() const
{
	ASSERT(m_Result);
	return *m_Result;
}
