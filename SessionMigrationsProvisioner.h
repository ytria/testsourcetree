#pragma once

#include "IMigrationsProvisioner.h"

class IMigrationVersion;
class MigrationRegistrations;

class SessionMigrationsProvisioner : public IMigrationsProvisioner
{
public:
	SessionMigrationsProvisioner(MigrationRegistrations& p_Registrations);
	void Provision() override;

private:
	MigrationRegistrations& m_Registrations;
};

