#include "GridDriveItems.h"

#include "AutomationWizardDriveItemsChannel.h"
#include "AutomationWizardDriveItemsGroup.h"
#include "AutomationWizardDriveItemsSite.h"
#include "AutomationWizardDriveItemsUser.h"
#include "BasicGridSetup.h"
#include "BaseOptionalPropertyRenderer.h"
#include "BusinessGroup.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessUser.h"
#include "BusinessSite.h"
#include "CacheGridTools.h"
#include "Command.h"
#include "DlgBusinessDriveItemRename.h"
#include "DlgDownloadDriveItems.h"
#include "DlgInput.h"
#include "DlgLoading.h"
#include "DlgNameInputHTML.h"
#include "DriveItemDeserializer.h"
#include "DriveItemDownloadInfo.h"
#include "DriveItemImporterFile.h"
#include "DriveItemImporterFolder.h"
#include "DriveItemsUtil.h"
#include "DriveItemPermissionDeletion.h"
#include "DriveItemPermissionFieldUpdate.h"
#include "FakeModification.h"
#include "FileUtil.h"
#include "FrameDriveItems.h"
#include "GridBackendUtil.h"
#include "GridUpdater.h"
#include "GridUtil.h"
#include "O365AdminUtil.h"
#include "ScopedTemporarySelection.h"
#include "SisterhoodFieldUpdate.h"
#include "SubItemsFlagGetter.h"
#include "TimeUtil.h"
#include "YCallbackMessage.h"
#include "YFileDialog.h"

wstring GridDriveItems::g_SharedPrivate;
wstring GridDriveItems::g_SharedShared;
wstring GridDriveItems::g_OwnerOnlyText;
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
wstring GridDriveItems::g_CheckoutNoFieldText;
wstring GridDriveItems::g_CheckoutNoFieldTooltip;
#endif

const int GridDriveItems::g_HiddenOwnerLparam = 666;

//const GridBackendUtil::ROWFLAGS GridDriveItems::g_RenameOnConflictFlag	= ???;
const GridBackendUtil::ROWFLAGS GridDriveItems::g_ReplaceContentFlag		= GridBackendUtil::CUSTOMDEFINED1;

BEGIN_MESSAGE_MAP(GridDriveItems, GridDriveItemsBaseClass)
    ON_COMMAND(ID_DRIVEITEMSGRID_VIEWPERMISSIONS,							OnViewPermissions)
    ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_VIEWPERMISSIONS,					OnUpdateShowPermissions)
	ON_COMMAND(ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODE,					OnViewPermissionsExplode)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODE,			OnUpdateShowPermissions)
	ON_COMMAND(ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODEANDFLAT,			OnViewPermissionsExplodeFlat)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODEANDFLAT,	OnUpdateShowPermissions)
    ON_COMMAND(ID_DRIVEITEMSGRID_DELETEPERMISSIONS,							OnDeletePermissions)
    ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_DELETEPERMISSIONS,				OnUpdateDeletePermissions)
    ON_COMMAND(ID_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD,                   OnSetPermissionRoleToRead)
    ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD,         OnUpdateSetPermissionRoleToRead)
    ON_COMMAND(ID_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE,                  OnSetPermissionRoleToWrite)
    ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE,        OnUpdateSetPermissionRoleToWrite)
    ON_COMMAND(ID_DRIVEITEMSGRID_DOWNLOAD,									OnDownload)
    ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_DOWNLOAD,						OnUpdateDownload)
	ON_COMMAND(ID_DRIVEITEMSGRID_DELETE,									OnDeleteItem)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_DELETE,							OnUpdateDeleteItem)
    ON_COMMAND(ID_DRIVEITEMSGRID_RENAME,                                    OnRenameItem)
    ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_RENAME,                          OnUpdateRenameItem)
	ON_COMMAND(ID_DRIVEITEMSGRID_TOGGLEOWNERPERMISSIONS,					OnToggleOwnerPermissions)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_TOGGLEOWNERPERMISSIONS,			OnUpdateToggleOwnerPermissions)
	ON_COMMAND(ID_DRIVEITEMSGRID_CREATEFOLDER,								OnCreateFolder)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_CREATEFOLDER,					OnUpdateCreateFolder)
	ON_COMMAND(ID_DRIVEITEMSGRID_ADDFILE,									OnAddFile)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_ADDFILE,							OnUpdateAddFile)
	ON_COMMAND(ID_DRIVEITEMSGRID_IMPORTFOLDER,								OnImportFolder)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_IMPORTFOLDER,					OnUpdateImportFolder)
	ON_COMMAND(ID_DRIVEITEMSGRID_CHECKIN,									OnCheckIn)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_CHECKIN,							OnUpdateCheckIn)
	ON_COMMAND(ID_DRIVEITEMSGRID_CHECKOUT,									OnCheckOut)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_CHECKOUT,						OnUpdateCheckOut)
	ON_COMMAND(ID_DRIVEITEMSGRID_SHOWFOLDERINFLAT,							OnShowFoldersInFlatToggle)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_SHOWFOLDERINFLAT,				OnUpdateShowFoldersInFlatToggle)
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
	ON_COMMAND(ID_DRIVEITEMSGRID_LOADCHECKOUTSTATUS,						OnLoadCkeckoutStatus)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_LOADCHECKOUTSTATUS,				OnUpdateLoadCkeckoutStatus)
#endif
	ON_COMMAND(ID_DRIVEITEMSGRID_CHANGESEARCHCRITERIA,						OnChangeSearchCriteria)
	ON_UPDATE_COMMAND_UI(ID_DRIVEITEMSGRID_CHANGESEARCHCRITERIA,			OnUpdateChangeSearchCriteria)

    ON_WM_TIMER()
END_MESSAGE_MAP()

GridDriveItems::GridDriveItems(Origin p_Origin, bool p_IsMyData/* = false*/)
    : m_DownloadTaskPool(std::make_shared<YTaskPool>(5))
	, m_NbDownloadsOccuring(0)
	, m_ShowOwnerPermissions(false)
	, m_HasLoadMoreAdditionalInfo(false)
	, m_ShowFoldersInFlatView(false)
	, m_IsMyData(p_IsMyData)
	, m_ColPublicationLevel(nullptr)
	, m_ColCheckoutLoaded(nullptr)
	, m_ColCheckoutUser(nullptr)
	, m_ColCheckinComment(nullptr)
	, m_ColComplianceTag(nullptr)
	, m_ColComplianceTimeWrittenTime(nullptr)
{
	m_WithOwnerAsTopAncestor = true; // FIXME: Should it be false in mydata?

	if (g_SharedPrivate.empty())
		g_SharedPrivate = YtriaTranslate::Do(GridDriveItems_fillRow_2, _YLOC("Private")).c_str();
	if (g_SharedShared.empty())
		g_SharedShared = YtriaTranslate::Do(GridDriveItems_fillRow_1, _YLOC("Shared")).c_str();
	if (g_OwnerOnlyText.empty())
		g_OwnerOnlyText = YtriaTranslate::Do(GridDriveItems_GetNoFieldText_1, _YLOC("Owner only")).c_str();
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
	if (g_CheckoutNoFieldText.empty())
		g_CheckoutNoFieldText = _T(" - Use the 'Checkout Info' button - ");
	if (g_CheckoutNoFieldTooltip.empty())
		g_CheckoutNoFieldTooltip = _T("To see this information, use the 'Checkout Info' button");
#endif
	UseDefaultActions(0);
	EnableGridModifications();

	m_Origin = p_Origin;
	// FIXME: Do we want to separate wizards?
	ASSERT(m_Origin == Origin::User || m_Origin == Origin::DeletedUser || m_Origin == Origin::Group || m_Origin == Origin::Site || m_Origin == Origin::Channel);
	if (m_Origin == Origin::User || m_Origin == Origin::DeletedUser)
	{
		m_TemplateUsers = std::make_unique<GridTemplateUsers>();
		initWizard<AutomationWizardDriveItemsUser>(_YTEXT("Automation\\DriveItemsUser"));
	}
	else if (m_Origin == Origin::Group)
	{
		m_TemplateGroups = std::make_unique<GridTemplateGroups>();
		initWizard<AutomationWizardDriveItemsGroup>(_YTEXT("Automation\\DriveItemsGroup"));
	}
	else if (m_Origin == Origin::Site)
	{
		m_SiteMetaColumns = std::make_unique<SiteMetaColumns>();
		initWizard<AutomationWizardDriveItemsSite>(_YTEXT("Automation\\DriveItemsSite"));
	}
	else if (m_Origin == Origin::Channel)
	{
		m_ChannelMetaColumns = std::make_unique<ChannelMetaColumns>();
		initWizard<AutomationWizardDriveItemsChannel>(_YTEXT("Automation\\DriveItemsChannel"));
	}

	if (IsMyData())
		m_MyDataMeHandler.emplace();
}

void GridDriveItems::customizeGrid()
{
	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_GRIDDRIVEITEMS_ACCELERATOR));

	if (m_TemplateUsers || m_TemplateGroups)
	{
		BasicGridSetup setup(GridUtil::g_AutoNameDriveItems, LicenseUtil::g_codeSapio365drives,
		{
			{ m_TemplateUsers ? &m_TemplateUsers->m_ColumnDisplayName		: &m_TemplateGroups->m_ColumnDisplayName,  g_TitleOwnerDisplayName, g_FamilyOwner },
			{ m_TemplateUsers ? &m_TemplateUsers->m_ColumnUserPrincipalName : nullptr, g_TitleOwnerUserName, g_FamilyOwner },
			{ m_TemplateUsers ? &m_TemplateUsers->m_ColumnID				: &m_TemplateGroups->m_ColumnID, g_TitleOwnerID, g_FamilyOwner }
		},
		{ BusinessDriveItem(), BusinessDriveItemFolder(), BusinessDriveItemNotebook() });
		setup.Setup(*this, false);

		if (m_TemplateUsers)
		{
			m_MetaColumns.insert(m_MetaColumns.end(), { m_TemplateUsers->m_ColumnID, m_TemplateUsers->m_ColumnDisplayName, m_TemplateUsers->m_ColumnUserPrincipalName });
			if (IsMyData())
				m_TemplateUsers->m_ColumnDisplayName->RemovePresetFlags(g_ColumnsPresetDefault);
		}
		else
			m_MetaColumns.insert(m_MetaColumns.end(), { m_TemplateGroups->m_ColumnID, m_TemplateGroups->m_ColumnDisplayName });

		if (m_TemplateGroups)
		{
			m_TemplateGroups->m_ColumnIsTeam = AddColumnIcon(_YUID("meta.isTeam"), g_TitleOwnerIsTeam, g_FamilyOwner);
			SetColumnPresetFlags(m_TemplateGroups->m_ColumnIsTeam, { g_ColumnsPresetDefault });
			MoveColumnBefore(m_TemplateGroups->m_ColumnIsTeam, m_TemplateGroups->m_ColumnID);
			m_TemplateGroups->m_ColumnCombinedGroupType = AddColumn(_YUID("meta.groupType"), g_TitleOwnerGroupType, g_FamilyOwner, g_ColumnSize12, { g_ColumnsPresetDefault });
			MoveColumnBefore(m_TemplateGroups->m_ColumnCombinedGroupType, m_TemplateGroups->m_ColumnID);

			m_MetaColumns.insert(m_MetaColumns.end(), { m_TemplateGroups->m_ColumnIsTeam, m_TemplateGroups->m_ColumnCombinedGroupType });
		}

		/*Add additional meta columns*/
		const auto& metaDataColumnInfos = getMetaDataColumnInfo();
		if (metaDataColumnInfos)
		{
			auto processTemplate = [this, &metaDataColumnInfos](auto& p_Template)
			{
				for (const auto& c : *metaDataColumnInfos)
				{
					ASSERT(nullptr == p_Template->GetColumnForProperty(c.GetPropertyName()));
					if (nullptr == p_Template->GetColumnForProperty(c.GetPropertyName()))
					{
						auto col = c.AddTo(*this, g_FamilyOwner);
						p_Template->GetColumnForProperty(c.GetPropertyName()) = col;
						m_MetaColumns.emplace_back(col);
						m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
					}
				}
			};

			if (m_TemplateUsers)
				processTemplate(m_TemplateUsers);
			else
				processTemplate(m_TemplateGroups);
		}
	}
	else if (m_SiteMetaColumns)
	{
		{
			BasicGridSetup setup(GridUtil::g_AutoNameDriveItems, LicenseUtil::g_codeSapio365drives,
				{
					{ &m_SiteMetaColumns->m_ColumnDisplayName, g_TitleOwnerDisplayName, g_FamilyOwner },
					{ nullptr, _YTEXT(""), _YTEXT("") },
					{ &m_SiteMetaColumns->m_ColumnID, g_TitleOwnerID, g_FamilyOwner }
				},
				{ BusinessDriveItem(), BusinessDriveItemFolder(), BusinessDriveItemNotebook() });
			setup.Setup(*this, false);
		}

		m_MetaColumns.insert(m_MetaColumns.end(), { m_SiteMetaColumns->m_ColumnID, m_SiteMetaColumns->m_ColumnDisplayName });
	}
	else if (m_ChannelMetaColumns)
	{
		static const wstring g_FamilyChannel = _T("Channel");
		{
			BasicGridSetup setup(GridUtil::g_AutoNameDriveItems, LicenseUtil::g_codeSapio365drives);
			setup.Setup(*this, false);
		}
		SetObjectTypesForLoadMore({ BusinessDriveItem(), BusinessDriveItemFolder(), BusinessDriveItemNotebook() });

		m_ChannelMetaColumns->m_ColumnTeamDisplayName = AddColumn(_YUID("teamDisplayName"), _T("Team Display Name"), g_FamilyChannel, g_ColumnSize12, { g_ColumnsPresetDefault });
		m_ChannelMetaColumns->m_ColumnChannelDisplayName = AddColumn(_YUID("channelDisplayName"), _T("Channel Display Name"), g_FamilyChannel, g_ColumnSize12, { g_ColumnsPresetDefault });

		m_ChannelMetaColumns->m_ColumnTeamID = AddColumn(_YUID("teamId"), _T("Team ID"), g_FamilyChannel, g_ColumnSize12, { g_ColumnsPresetTech });
		m_ChannelMetaColumns->m_ColumnTeamID->SetFlag(GridBackendUtil::CASESENSITIVEFIELDS);
		m_ChannelMetaColumns->m_ColumnChannelID = AddColumn(_YUID("channelId"), _T("Channel ID"), g_FamilyChannel, g_ColumnSize12, { g_ColumnsPresetTech });
		m_ChannelMetaColumns->m_ColumnTeamID->SetFlag(GridBackendUtil::CASESENSITIVEFIELDS);

		m_MetaColumns.insert(m_MetaColumns.end(), { m_ChannelMetaColumns->m_ColumnTeamID, m_ChannelMetaColumns->m_ColumnChannelID, m_ChannelMetaColumns->m_ColumnTeamDisplayName, m_ChannelMetaColumns->m_ColumnChannelDisplayName });
	}
	else
	{
		ASSERT(false);
	}

	static const wstring g_FamilyDrive = YtriaTranslate::Do(GridDriveItems_customizeGrid_1, _YLOC("Drive")).c_str();
    m_ColMetaDriveId								= AddColumn(_YUID("metaDriveId"),									YtriaTranslate::Do(GridDriveItems_customizeGrid_2, _YLOC("Drive ID")).c_str(),													g_FamilyDrive,				g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColDirectory									= AddColumn(_YUID("DIRECTORY"),										YtriaTranslate::Do(GridDriveItems_customizeGrid_3, _YLOC("Directory")).c_str(),													g_FamilyInfo,				g_ColumnSize32);
 	GridBackendColumn* c							= AddColumnFileType(												YtriaTranslate::Do(GridDriveItems_customizeGrid_4, _YLOC("Icon")).c_str(),														g_FamilyInfo);
	SetColumnPresetFlags(c, { g_ColumnsPresetDefault });
	c->SetFlag(GridBackendUtil::IGNOREINSNAPSHOT);

	m_ColFileToBeUploaded = AddColumn(_YUID("fileToUpload"), _T("File To Upload"), g_FamilyInfo, g_ColumnSize32);
	SetColumnVisible(m_ColFileToBeUploaded, false, GridBackendUtil::SETVISIBLE_NONE); // Hidden by default

    m_ColDownloadProgress                           = AddColumnProgressBar(_YUID("progressBar"),                        YtriaTranslate::Do(GridDriveItems_customizeGrid_5, _YLOC("Download Progress Bar")).c_str(),										g_FamilyInfo, GridBackendUtil::CELL_TYPE_STRING);

    ProgressFormat progFmt;
    progFmt.m_ProgressText = YtriaTranslate::Do(GridDriveItems_customizeGrid_104, _YLOC("Downloading")).c_str();
    m_ColDownloadProgress->SetProgressFormat(progFmt, false);
    
	m_ColName										= AddColumn(_YUID("name"),											YtriaTranslate::Do(GridDriveItems_customizeGrid_6, _YLOC("File Name")).c_str(),													g_FamilyInfo,				g_ColumnSize32, { g_ColumnsPresetDefault });
	ASSERT(nullptr != m_ColName);

	if (m_TemplateUsers)
	{
		setBasicGridSetupHierarchy({ { { m_TemplateUsers->m_ColumnUserPrincipalName, 0 }, { m_ColName, GridBackendUtil::g_AllHierarchyLevels } }
										,{ rttr::type::get<BusinessDriveItem>().get_id() }
										,{ rttr::type::get<BusinessUser>().get_id(), rttr::type::get<BusinessDriveItemFolder>().get_id(), rttr::type::get<BusinessDriveItemNotebook>().get_id(), rttr::type::get<BusinessDriveItem>().get_id() } });
	}
	else if (m_TemplateGroups)
	{
		setBasicGridSetupHierarchy({ { { m_TemplateGroups->m_ColumnDisplayName, 0 }, { m_ColName, GridBackendUtil::g_AllHierarchyLevels } }
										,{ rttr::type::get<BusinessDriveItem>().get_id() }
										,{ m_TemplateGroups ? rttr::type::get<BusinessGroup>().get_id() : rttr::type::get<BusinessSite>().get_id(), rttr::type::get<BusinessDriveItemFolder>().get_id(), rttr::type::get<BusinessDriveItemNotebook>().get_id(), rttr::type::get<BusinessDriveItem>().get_id() } });
	}
	else if (m_SiteMetaColumns)
	{
		setBasicGridSetupHierarchy({ { { m_SiteMetaColumns->m_ColumnDisplayName, 0 }, { m_ColName, GridBackendUtil::g_AllHierarchyLevels } }
										,{ rttr::type::get<BusinessDriveItem>().get_id() }
										,{ rttr::type::get<BusinessSite>().get_id(), rttr::type::get<BusinessDriveItemFolder>().get_id(), rttr::type::get<BusinessDriveItemNotebook>().get_id(), rttr::type::get<BusinessDriveItem>().get_id() } });
	}
	else if (m_ChannelMetaColumns)
	{
		setBasicGridSetupHierarchy({ { { m_ChannelMetaColumns->m_ColumnChannelDisplayName, 0 }, { m_ColName, GridBackendUtil::g_AllHierarchyLevels } }
										,{ rttr::type::get<BusinessDriveItem>().get_id() }
										,{ rttr::type::get<BusinessChannel>().get_id(), rttr::type::get<BusinessDriveItemFolder>().get_id(), rttr::type::get<BusinessDriveItemNotebook>().get_id(), rttr::type::get<BusinessDriveItem>().get_id() } });
	}
	else
	{
		ASSERT(false);
	}

	if (nullptr != m_ColName)
		m_ColName->SetManageFileFormat(true);
	m_ColNameExtOnly								= AddColumn(_YUID("NAMEEXTONLY"),									YtriaTranslate::Do(GridDriveItems_customizeGrid_7, _YLOC("Extension")).c_str(),													g_FamilyInfo,				g_ColumnSize6);
	if (IsMyData())
	{
		m_ColPublicationLevel							= AddColumnIcon(_YUID("publication.level"),							_T("Publication Level"), g_FamilyInfo);
		SetColumnPresetFlags(m_ColPublicationLevel, { g_ColumnsPresetDefault });
	}
	m_ColPublicationVersionId						= AddColumn(_YUID("publication.versionId"),							_T("Publication Version"), g_FamilyInfo, g_ColumnSize4);
	m_ColWebUrl										= AddColumnHyperlink(_YUID("WEBURL"),								YtriaTranslate::Do(GridDriveItems_customizeGrid_8, _YLOC("URL")).c_str(),														g_FamilyInfo,				g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColLinkWebDavURL								= AddColumnHyperlink(_YUID("webDavUrl"),							_T("WebDAV URL"), g_FamilyInfo, g_ColumnSize8);
	m_ColFileCount									= AddColumnNumber(_YUID("FILECOUNT"),								YtriaTranslate::Do(GridDriveItems_customizeGrid_106, _YLOC("File Count")).c_str(),												g_FamilyInfo,				g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColFolderCount								= AddColumnNumber(_YUID("FOLDERCOUNT"),								YtriaTranslate::Do(GridDriveItems_customizeGrid_107, _YLOC("Folder Count")).c_str(),											g_FamilyInfo,				g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColFileSize									= AddColumnNumber(_YUID("SIZE"),									YtriaTranslate::Do(GridDriveItems_customizeGrid_9, _YLOC("File Size")).c_str(),													g_FamilyInfo,				g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColFolderSize									= AddColumnNumber(_YUID("FOLDERSIZE"),								YtriaTranslate::Do(GridDriveItems_customizeGrid_10, _YLOC("Folder Size")).c_str(),												g_FamilyInfo,				g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColTotalSize									= AddColumnNumber(_YUID("TOTALSIZE"),								YtriaTranslate::Do(GridDriveItems_customizeGrid_11, _YLOC("Total Size")).c_str(),												g_FamilyInfo,				g_ColumnSize8, { g_ColumnsPresetDefault });
	static const wstring g_FamilyLastModification = YtriaTranslate::Do(GridDriveItems_customizeGrid_12, _YLOC("Last Modification")).c_str();
	m_ColLastModifiedDateTime						= AddColumnDate(_YUID("LASTMODIFIEDDATETIME"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_13, _YLOC("Last Modified")).c_str(),											g_FamilyLastModification,	g_ColumnSize16, { g_ColumnsPresetDefault });
	static const wstring g_FamilySystemInfo = YtriaTranslate::Do(GridDriveItems_customizeGrid_14, _YLOC("File System Info")).c_str();
	m_ColFileSystemInfoLastModifiedTime				= AddColumnDate(_YUID("fileSystemInfo.lastModifiedTime"),			YtriaTranslate::Do(GridDriveItems_customizeGrid_15, _YLOC("File System Info - Last Modified")).c_str(),							g_FamilySystemInfo,			g_ColumnSize16);
    m_ColLastModifiedByUserName						= AddColumn(_YUID("lastModifiedBy.user.name"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_16, _YLOC("Last Modified - Username")).c_str(),									g_FamilyLastModification,	g_ColumnSize32, { g_ColumnsPresetDefault });
    m_ColLastModifiedByUserId						= AddColumn(_YUID("lastModifiedBy.user.id"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_17, _YLOC("Last Modified - User ID")).c_str(),									g_FamilyLastModification,	g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColLastModifiedByUserEmail					= AddColumn(_YUID("lastModifiedBy.user.email"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_18, _YLOC("Last Modified - Email")).c_str(),									g_FamilyLastModification,	g_ColumnSize32);
    m_ColLastModifiedByApplicationName				= AddColumn(_YUID("lastModifiedBy.application.name"),				YtriaTranslate::Do(GridDriveItems_customizeGrid_19, _YLOC("Last Modified - Application Name")).c_str(), 						g_FamilyLastModification,	g_ColumnSize22);
    m_ColLastModifiedByApplicationId				= AddColumn(_YUID("lastModifiedBy.application.id"),					YtriaTranslate::Do(GridDriveItems_customizeGrid_20, _YLOC("Last Modified - Application ID")).c_str(),							g_FamilyLastModification,	g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColLastModifiedByDeviceName					= AddColumn(_YUID("lastModifiedBy.device.name"),					YtriaTranslate::Do(GridDriveItems_customizeGrid_21, _YLOC("Last Modified - Device Name")).c_str(),								g_FamilyLastModification,	g_ColumnSize22);
    m_ColLastModifiedByDeviceId						= AddColumn(_YUID("lastModifiedBy.device.id"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_22, _YLOC("Last Modified - Device ID")).c_str(),								g_FamilyLastModification,	g_ColumnSize12, { g_ColumnsPresetTech });
	static const wstring g_FamilyCreation = YtriaTranslate::Do(GridDriveItems_customizeGrid_23, _YLOC("Creation")).c_str();
    m_ColCreatedDateTime							= AddColumnDate(_YUID("CREATEDDATETIME"),							YtriaTranslate::Do(GridDriveItems_customizeGrid_24, _YLOC("Created On")).c_str(),												g_FamilyCreation,			g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColFileSystemInfoCreatedDateTime				= AddColumnDate(_YUID("fileSystemInfo.createdTime"),				YtriaTranslate::Do(GridDriveItems_customizeGrid_25, _YLOC("File System Info - Created")).c_str(),								g_FamilySystemInfo,			g_ColumnSize16);
    m_ColCreatedByUserName							= AddColumn(_YUID("createdBy.user.name"),							YtriaTranslate::Do(GridDriveItems_customizeGrid_26, _YLOC("Created By - Username")).c_str(),									g_FamilyCreation,			g_ColumnSize32, { g_ColumnsPresetDefault });
    m_ColCreatedByUserId							= AddColumn(_YUID("createdBy.user.id"),								YtriaTranslate::Do(GridDriveItems_customizeGrid_27, _YLOC("Created By - User ID")).c_str(),										g_FamilyCreation,			g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColCreatedByUserEmail							= AddColumn(_YUID("createdBy.user.email"),							YtriaTranslate::Do(GridDriveItems_customizeGrid_28, _YLOC("Created By - Email")).c_str(),										g_FamilyCreation,			g_ColumnSize32);
    m_ColCreatedByAppName							= AddColumn(_YUID("createdBy.application.name"),					YtriaTranslate::Do(GridDriveItems_customizeGrid_29, _YLOC("Created By - Application Name")).c_str(),							g_FamilyCreation,			g_ColumnSize22);
    m_ColCreatedByAppId								= AddColumn(_YUID("createdBy.application.id"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_30, _YLOC("Created By - Application ID")).c_str(),								g_FamilyCreation,			g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColCreatedByDeviceName						= AddColumn(_YUID("createdBy.device.name"),							YtriaTranslate::Do(GridDriveItems_customizeGrid_31, _YLOC("Created By - Device Name")).c_str(),									g_FamilyCreation,			g_ColumnSize22);
    m_ColCreatedByDeviceId							= AddColumn(_YUID("createdBy.device.id"),							YtriaTranslate::Do(GridDriveItems_customizeGrid_32, _YLOC("Created By - Device ID")).c_str(),									g_FamilyCreation,			g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColOneDrivePath								= AddColumn(_YUID("ONEDRIVEPATH"),									YtriaTranslate::Do(GridDriveItems_customizeGrid_33, _YLOC("Full Path")).c_str(),												g_FamilyInfo,				g_ColumnSize12);
	m_ColOneDrivePath->SetFlag(GridBackendUtil::ALWAYSINSNAPSHOT);
	m_ColFileHashes									= AddColumn(_YUID("file.hashes"),									YtriaTranslate::Do(GridDriveItems_customizeGrid_34, _YLOC("Hash Code")).c_str(),												g_FamilyInfo,				g_ColumnSize12);
	m_ColFileMimeType								= AddColumn(_YUID("file.mimeType"),									YtriaTranslate::Do(GridDriveItems_customizeGrid_35, _YLOC("MIME Type")).c_str(),												g_FamilyInfo,				g_ColumnSize12);
	m_ColETag										= AddColumn(_YUID("eTag"),											YtriaTranslate::Do(GridDriveItems_customizeGrid_36, _YLOC("eTag")).c_str(),														g_FamilyInfo,				g_ColumnSize12);
	m_ColCTag										= AddColumn(_YUID("cTag"),											YtriaTranslate::Do(GridDriveItems_customizeGrid_37, _YLOC("cTag")).c_str(),														g_FamilyInfo,				g_ColumnSize12);
	static const wstring g_FamilyShared = YtriaTranslate::Do(GridDriveItems_customizeGrid_38, _YLOC("Shared(?)")).c_str();
    m_ColPackageType                                = AddColumn(_YUID("package.type"),                                  YtriaTranslate::Do(GridDriveItems_customizeGrid_105, _YLOC("Package Type")).c_str(),                                            g_FamilyInfo,               g_ColumnSize12, { g_ColumnsPresetTech });

	{
		static const wstring g_FamilyCheckoutStatus = _T("Checkout & retention");
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
		m_ColCheckoutLoaded				= AddColumnCheckBox(_YUID("checkoutStatus"), _T("Status - Load checkout and retention info"), g_FamilyCheckoutStatus);
		// Don't (Bug #200122.SB.00B392)
		//SetColumnPresetFlags(m_ColCheckoutLoaded, { g_ColumnsPresetDefault });
#endif
		m_ColCheckoutUser				= AddColumn(		_YUID("checkedOutTo"),				_T("Checked out to"),				g_FamilyCheckoutStatus, g_ColumnSize22, { g_ColumnsPresetDefault });
		m_ColCheckinComment				= AddColumnRichText(_YUID("checkInComment"),			_T("Check in comment"),				g_FamilyCheckoutStatus, g_ColumnSize22);
		m_ColComplianceTag				= AddColumn(		_YUID("appliedRetentionLabel"),		_T("Applied retention label"),		g_FamilyCheckoutStatus, g_ColumnSize22, { g_ColumnsPresetDefault });
		m_ColComplianceTimeWrittenTime	= AddColumnDate(	_YUID("retentionLabelAppliedOn"),	_T("Retention label applied on"),	g_FamilyCheckoutStatus, g_ColumnSize16);
	}

    m_ColShared                                     = AddColumn(_YUID("shared"),                                        YtriaTranslate::Do(GridDriveItems_customizeGrid_39, _YLOC("Shared")).c_str(),                                                   g_FamilyShared,             g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColSharedScope								= AddColumn(_YUID("shared.scope"),									YtriaTranslate::Do(GridDriveItems_customizeGrid_40, _YLOC("Shared - Scope")).c_str(),											g_FamilyShared,				g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSharedSharedDateTime						= AddColumn(_YUID("shared.sharedDateTime"),							YtriaTranslate::Do(GridDriveItems_customizeGrid_41, _YLOC("Shared - Shared On")).c_str(),										g_FamilyShared,				g_ColumnSize16, { g_ColumnsPresetTech });
	m_ColSharedOwnerUserName						= AddColumn(_YUID("shared.owner.user.name"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_42, _YLOC("Owner - Username")).c_str(),											g_FamilyShared,				g_ColumnSize32, { g_ColumnsPresetTech });
	m_ColSharedOwnerUserEmail						= AddColumn(_YUID("shared.owner.user.email"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_43, _YLOC("Owner - Email")).c_str(),											g_FamilyShared,				g_ColumnSize32, { g_ColumnsPresetTech });
	m_ColSharedOwnerUserId							= AddColumn(_YUID("shared.owner.user.id"),							YtriaTranslate::Do(GridDriveItems_customizeGrid_44, _YLOC("Owner - User ID")).c_str(),											g_FamilyShared,				g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSharedOwnerApplicationName					= AddColumn(_YUID("shared.owner.application.name"),					YtriaTranslate::Do(GridDriveItems_customizeGrid_45, _YLOC("Owner - Application Name")).c_str(),									g_FamilyShared,				g_ColumnSize22, { g_ColumnsPresetTech });
	m_ColSharedOwnerApplicationId					= AddColumn(_YUID("shared.owner.application.id"),					YtriaTranslate::Do(GridDriveItems_customizeGrid_46, _YLOC("Owner - Application ID")).c_str(),									g_FamilyShared,				g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSharedOwnerDeviceName						= AddColumn(_YUID("shared.owner.device.name"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_47, _YLOC("Owner - Device Name")).c_str(),										g_FamilyShared,				g_ColumnSize22, { g_ColumnsPresetTech });
	m_ColSharedOwnerDeviceId						= AddColumn(_YUID("shared.owner.device.id"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_48, _YLOC("Owner - Device ID")).c_str(),										g_FamilyShared,				g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSharedSharedByUserName						= AddColumn(_YUID("shared.sharedBy.user.name"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_49, _YLOC("Shared By - Username")).c_str(),										g_FamilyShared,				g_ColumnSize32, { g_ColumnsPresetTech });
	m_ColSharedSharedByUserId						= AddColumn(_YUID("shared.sharedBy.user.id"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_50, _YLOC("Shared By - User ID")).c_str(),										g_FamilyShared,				g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSharedSharedByUserEmail					= AddColumn(_YUID("shared.sharedBy.user.email"),					YtriaTranslate::Do(GridDriveItems_customizeGrid_51, _YLOC("Shared By - Email")).c_str(),										g_FamilyShared,				g_ColumnSize32, { g_ColumnsPresetTech });
	m_ColSharedSharedByApplicationName				= AddColumn(_YUID("shared.sharedBy.application.name"),				YtriaTranslate::Do(GridDriveItems_customizeGrid_52, _YLOC("Shared By - Application Name")).c_str(),								g_FamilyShared,				g_ColumnSize22, { g_ColumnsPresetTech });
	m_ColSharedSharedByApplicationId				= AddColumn(_YUID("shared.sharedBy.application.id"),				YtriaTranslate::Do(GridDriveItems_customizeGrid_53, _YLOC("Shared By - Application ID")).c_str(),								g_FamilyShared,				g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSharedSharedByDeviceName					= AddColumn(_YUID("shared.sharedBy.device.name"),					YtriaTranslate::Do(GridDriveItems_customizeGrid_54, _YLOC("Shared By - Device Name")).c_str(),									g_FamilyShared,				g_ColumnSize22, { g_ColumnsPresetTech });
	m_ColSharedSharedByDeviceId						= AddColumn(_YUID("shared.sharedBy.device.id"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_55, _YLOC("Shared By - Device ID")).c_str(),									g_FamilyShared,				g_ColumnSize12, { g_ColumnsPresetTech });
	addColumnGraphID();// added here to keep in main top category

	static const wstring g_FamilyPermissions = YtriaTranslate::Do(GridDriveItems_customizeGrid_56, _YLOC("Permissions")).c_str();
	addColumnIsLoadMore(_YUID("permissionInfoLoaded"), YtriaTranslate::Do(GridDriveItems_customizeGrid_57, _YLOC("Status - Load Permission Info")).c_str());
	m_ColRole										= AddColumn(_YUID("ROLE"),											YtriaTranslate::Do(GridDriveItems_customizeGrid_58, _YLOC("Role Based")).c_str(),														g_FamilyPermissions,		g_ColumnSize12,	{ g_ColumnsPresetDefault });
	static const wstring g_FamilyPermissionUserType = YtriaTranslate::Do(GridDriveItems_customizeGrid_59, _YLOC("Permissions - User Type")).c_str();
	m_ColTarget										= AddColumn(_YUID("TARGET"),										YtriaTranslate::Do(GridDriveItems_customizeGrid_60, _YLOC("Target")).c_str(),													g_FamilyPermissions,		g_ColumnSize32, { g_ColumnsPresetDefault });
	m_ColGrantedToUserIdentityName					= AddColumn(_YUID("GRANTEDTOUSERIDENTITYNAME"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_61, _YLOC("Granted To - Username")).c_str(),									g_FamilyPermissionUserType, g_ColumnSize32);
	m_ColGrantedToUserIdentityID					= AddColumn(_YUID("GRANTEDTOUSERIDENTITYID"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_62, _YLOC("Granted To - User ID")).c_str(),										g_FamilyPermissionUserType, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColGrantedToUserIdentityEmail					= AddColumn(_YUID("GRANTEDTOUSERIDENTITYEMAIL"),					YtriaTranslate::Do(GridDriveItems_customizeGrid_63, _YLOC("Granted To - Email")).c_str(),										g_FamilyPermissionUserType, g_ColumnSize32);
    m_ColGrantedToApplicationIdentityName			= AddColumn(_YUID("GRANTEDTOAPPLICATIONIDENTITYNAME"),				YtriaTranslate::Do(GridDriveItems_customizeGrid_64, _YLOC("Granted To - Application Name")).c_str(),							g_FamilyPermissionUserType, g_ColumnSize22);
    m_ColGrantedToApplicationIdentityID				= AddColumn(_YUID("GRANTEDTOAPPLICATIONIDENTITYID"),				YtriaTranslate::Do(GridDriveItems_customizeGrid_65, _YLOC("Granted To - Application ID")).c_str(),								g_FamilyPermissionUserType, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColGrantedToDeviceIdentityName				= AddColumn(_YUID("GRANTEDTODEVICEIDENTITYNAME"),					YtriaTranslate::Do(GridDriveItems_customizeGrid_66, _YLOC("Granted To - Device Name")).c_str(),									g_FamilyPermissionUserType, g_ColumnSize22);
    m_ColGrantedToDeviceIdentityID					= AddColumn(_YUID("GRANTEDTODEVICEIDENTITYID"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_67, _YLOC("Granted To - Device ID")).c_str(),									g_FamilyPermissionUserType, g_ColumnSize12, { g_ColumnsPresetTech });
	static const wstring g_FamilyPermissionsLink = YtriaTranslate::Do(GridDriveItems_customizeGrid_68, _YLOC("Permissions - Link")).c_str();
    m_ColLinkScope									= AddColumn(_YUID("LINKSCOPE"),										YtriaTranslate::Do(GridDriveItems_customizeGrid_69, _YLOC("Link - Scope")).c_str(),												g_FamilyPermissionsLink,	g_ColumnSize12);
	m_ColLinkType									= AddColumn(_YUID("LINKTYPE"),										YtriaTranslate::Do(GridDriveItems_customizeGrid_70, _YLOC("Link - Type")).c_str(),												g_FamilyPermissionsLink,	g_ColumnSize12);
    m_ColLinkWebURL									= AddColumnHyperlink(_YUID("LINKWEBURL"),							YtriaTranslate::Do(GridDriveItems_customizeGrid_71, _YLOC("Link - Web URL")).c_str(),											g_FamilyPermissionsLink,	g_ColumnSize8,	{ g_ColumnsPresetDefault });
    m_ColLinkApplicationIdentityName				= AddColumn(_YUID("LINKAPPLICATIONIDENTITYNAME"),					YtriaTranslate::Do(GridDriveItems_customizeGrid_72, _YLOC("Link - Application Name")).c_str(),									g_FamilyPermissionsLink,	g_ColumnSize22);
    m_ColLinkApplicationIdentityId					= AddColumn(_YUID("LINKAPPLICATIONIDENTITYID"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_73, _YLOC("Link - Application ID")).c_str(),									g_FamilyPermissionsLink,	g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColLinkWebHtml								= AddColumn(_YUID("LINKWEBHTML"),									YtriaTranslate::Do(GridDriveItems_customizeGrid_74, _YLOC("Link - Web Html")).c_str(),											g_FamilyPermissionsLink,	g_ColumnSize12);
	static const wstring g_FamilyPermissionsInvit = YtriaTranslate::Do(GridDriveItems_customizeGrid_75, _YLOC("Permissions - Invitation")).c_str();
	m_ColInvitationEmail							= AddColumn(_YUID("INVITATIONEMAIL"),								YtriaTranslate::Do(GridDriveItems_customizeGrid_76, _YLOC("Invitation - Email")).c_str(),										g_FamilyPermissionsInvit,	g_ColumnSize32);
	m_ColInvitationSignInRequired					= AddColumn(_YUID("INVITATIONSIGNINREQUIRED"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_77, _YLOC("Invited By - Sign In Requested")).c_str(),							g_FamilyPermissionsInvit,	g_ColumnSize6);
	m_ColInvitationSignInRequired->SetDisplayedColumnType(YtriaTranslate::Do(GridDriveItems_customizeGrid_78, _YLOC("Boolean")).c_str());
    m_ColInvitationInvitedByUserIdentityName		= AddColumn(_YUID("INVITATIONINVITEDBYUSERIDENTITYNAME"),			YtriaTranslate::Do(GridDriveItems_customizeGrid_79, _YLOC("Invited By - Username")).c_str(),									g_FamilyPermissionsInvit,	g_ColumnSize32);
	m_ColInvitationInvitedByUserIdentityId			= AddColumn(_YUID("INVITATIONINVITEDBYUSERIDENTITYID"),				YtriaTranslate::Do(GridDriveItems_customizeGrid_80, _YLOC("Invited By - User ID")).c_str(),										g_FamilyPermissionsInvit,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColInvitationInvitedByUserIdentityEmail		= AddColumn(_YUID("INVITATIONINVITEDBYUSERIDENTITYEMAIL"),			YtriaTranslate::Do(GridDriveItems_customizeGrid_81, _YLOC("Invited By - Email")).c_str(),										g_FamilyPermissionsInvit,	g_ColumnSize32);
    m_ColInvitationInvitedByApplicationIdentityName = AddColumn(_YUID("INVITATIONINVITEDBYAPPLICATIONIDENTITYNAME"),	YtriaTranslate::Do(GridDriveItems_customizeGrid_82, _YLOC("Invited By - Application Name")).c_str(),							g_FamilyPermissionsInvit,	g_ColumnSize22);
	m_ColInvitationInvitedByApplicationIdentityId	= AddColumn(_YUID("INVITATIONINVITEDBYAPPLICATIONIDENTITYID"),		YtriaTranslate::Do(GridDriveItems_customizeGrid_83, _YLOC("Invited By - Application ID")).c_str(),								g_FamilyPermissionsInvit,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColInvitationInvitedByDeviceIdentityName		= AddColumn(_YUID("INVITATIONINVITEDBYDEVICEIDENTITYNAME"),			YtriaTranslate::Do(GridDriveItems_customizeGrid_84, _YLOC("Invited By - Device Name")).c_str(),									g_FamilyPermissionsInvit,	g_ColumnSize22);
    m_ColInvitationInvitedByDeviceIdentityId		= AddColumn(_YUID("INVITATIONINVITEDBYDEVICEIDENTITYID"),			YtriaTranslate::Do(GridDriveItems_customizeGrid_85, _YLOC("Invited By - Device ID")).c_str(),									g_FamilyPermissionsInvit,	g_ColumnSize12, { g_ColumnsPresetTech });
	static const wstring g_FamilyPermissionsInherit = YtriaTranslate::Do(GridDriveItems_customizeGrid_86, _YLOC("Permissions - Inherited")).c_str();
    m_ColInheritedFromDriveType						= AddColumn(_YUID("INHERITEDFROMDRIVETYPE"),						YtriaTranslate::Do(GridDriveItems_customizeGrid_87, _YLOC("Inherited From - Drive Type")).c_str(),								g_FamilyPermissionsInherit,	g_ColumnSize12);
    m_ColInheritedFromDriveID						= AddColumn(_YUID("INHERITEDFROMDRIVEID"),							YtriaTranslate::Do(GridDriveItems_customizeGrid_88, _YLOC("Inherited From - Drive ID")).c_str(),								g_FamilyPermissionsInherit,	g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColInheritedFromName							= AddColumn(_YUID("INHERITEDFROMNAME"),								YtriaTranslate::Do(GridDriveItems_customizeGrid_89, _YLOC("Inherited From - Name")).c_str(),									g_FamilyPermissionsInherit,	g_ColumnSize22);
    m_ColInheritedFromPath							= AddColumn(_YUID("INHERITEDFROMPATH"),								YtriaTranslate::Do(GridDriveItems_customizeGrid_90, _YLOC("Inherited From - Path")).c_str(),									g_FamilyPermissionsInherit,	g_ColumnSize12);
    m_ColInheritedFromId							= AddColumn(_YUID("INHERITEDFROMID"),								YtriaTranslate::Do(GridDriveItems_customizeGrid_91, _YLOC("Inherited From - ID")).c_str(),										g_FamilyPermissionsInherit,	g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColInheritedFromShareID						= AddColumn(_YUID("INHERITEDFROMSHAREID"),							YtriaTranslate::Do(GridDriveItems_customizeGrid_92, _YLOC("Inherited From - Share ID")).c_str(),								g_FamilyPermissionsInherit,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColInheritedFromSharepointIDsListID			= AddColumn(_YUID("INHERITEDFROMSHAREPOINTIDSLISTID"),				YtriaTranslate::Do(GridDriveItems_customizeGrid_93, _YLOC("Inherited From - SharePoint IDs - List ID")).c_str(),				g_FamilyPermissionsInherit,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColInheritedFromSharepointIDsListItemID		= AddColumn(_YUID("INHERITEDFROMSHAREPOINTIDSLISTITEMID"),			YtriaTranslate::Do(GridDriveItems_customizeGrid_94, _YLOC("Inherited From - SharePoint IDs - List Item ID")).c_str(),			g_FamilyPermissionsInherit,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColInheritedFromSharepointIDsListItemUniqueID = AddColumn(_YUID("INHERITEDFROMSHAREPOINTIDSLISTITEMUNIQUEID"),	YtriaTranslate::Do(GridDriveItems_customizeGrid_95, _YLOC("Inherited From - SharePoint IDs - List Item Unique ID")).c_str(),	g_FamilyPermissionsInherit,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColInheritedFromSharepointIDsSiteURL			= AddColumnHyperlink(_YUID("INHERITEDFROMSHAREPOINTIDSSITEURL"),	YtriaTranslate::Do(GridDriveItems_customizeGrid_96, _YLOC("Inherited From - SharePoint IDs - Site URL")).c_str(),				g_FamilyPermissionsInherit,	g_ColumnSize8,	{ g_ColumnsPresetTech });
	m_ColInheritedFromSharepointIDsSiteID			= AddColumn(_YUID("INHERITEDFROMSHAREPOINTIDSSITEID"),				YtriaTranslate::Do(GridDriveItems_customizeGrid_97, _YLOC("Inherited From - SharePoint IDs - Site ID")).c_str(),				g_FamilyPermissionsInherit,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColInheritedFromSharepointIDsWebID			= AddColumn(_YUID("INHERITEDFROMSHAREPOINTIDSWEBID"),				YtriaTranslate::Do(GridDriveItems_customizeGrid_98, _YLOC("Inherited From - SharePoint IDs - Web ID")).c_str(),					g_FamilyPermissionsInherit,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColPermissionId								= AddColumn(_YUID("PERMISSIONID"),									YtriaTranslate::Do(GridDriveItems_customizeGrid_99, _YLOC("Permission - ID")).c_str(),											g_FamilyPermissions,		g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColShareId									= AddColumn(_YUID("SHAREID"),										YtriaTranslate::Do(GridDriveItems_customizeGrid_100, _YLOC("Permission - Share ID")).c_str(),									g_FamilyPermissions,		g_ColumnSize12, { g_ColumnsPresetTech });

	m_ColDescription					= AddColumn(_YUID("description"),						_T("Description"),							g_FamilyInfo, g_ColumnSize32);	
	m_ColSharepointIDsListID			= AddColumn(_YUID("sharepointIds.listId"),				_T("SharePoint IDs - List ID"),				g_FamilyInfo, g_ColumnSize12,	{ g_ColumnsPresetTech });
	m_ColSharepointIDsListItemID		= AddColumn(_YUID("sharepointIds.listItemId"),			_T("SharePoint IDs - List Item ID"),		g_FamilyInfo, g_ColumnSize12,	{ g_ColumnsPresetTech });
	m_ColSharepointIDsListItemUniqueID	= AddColumn(_YUID("sharepointIds.listItemUniqueId"),	_T("SharePoint IDs - List Item Unique ID"), g_FamilyInfo, g_ColumnSize12,	{ g_ColumnsPresetTech });
	m_ColSharepointIDsSiteURL			= AddColumnHyperlink(_YUID("sharepointIds.siteUrl"),	_T("SharePoint IDs - Site URL"),			g_FamilyInfo, g_ColumnSize8,	{ g_ColumnsPresetTech });
	m_ColSharepointIDsSiteID			= AddColumn(_YUID("sharepointIds.siteId"),				_T("SharePoint IDs - Site ID"),				g_FamilyInfo, g_ColumnSize12,	{ g_ColumnsPresetTech });
	m_ColSharepointIDsWebID				= AddColumn(_YUID("sharepointIds.webId"),				_T("SharePoint IDs - Web ID"),				g_FamilyInfo, g_ColumnSize12,	{ g_ColumnsPresetTech });

    //m_ColDeleted				= AddColumnCheckBox(_YUID("DELETED"), _TLOC("Deleted"),  _TLOC("Details"));
	AddColumnForRowPK(GetColId());
	AddColumnForRowPK(GetColMetaID());

	// This column will be the elder of the sisterhood. Keep it the same as the one used in row flagger to identify subitems. 
	m_ColPermissionId->SetMultivalueFamily(YtriaTranslate::Do(GridDriveItems_customizeGrid_101, _YLOC("Permissions")).c_str());
	m_ColPermissionId->AddMultiValueExplosionSister(m_ColTarget);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColGrantedToApplicationIdentityID);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColGrantedToApplicationIdentityName);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColGrantedToDeviceIdentityID);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColGrantedToDeviceIdentityName);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColGrantedToUserIdentityID);	
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColGrantedToUserIdentityName);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColGrantedToUserIdentityEmail);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInvitationEmail);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInvitationInvitedByApplicationIdentityId);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInvitationInvitedByApplicationIdentityName);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInvitationInvitedByDeviceIdentityId);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInvitationInvitedByDeviceIdentityName);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInvitationInvitedByUserIdentityId);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInvitationInvitedByUserIdentityName);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInvitationInvitedByUserIdentityEmail);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInvitationSignInRequired);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromDriveID);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromDriveType);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromId);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromName);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromPath);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromShareID);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromSharepointIDsListID);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromSharepointIDsListItemID);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromSharepointIDsListItemUniqueID);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromSharepointIDsSiteID);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromSharepointIDsSiteURL);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColInheritedFromSharepointIDsWebID);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColLinkApplicationIdentityId);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColLinkApplicationIdentityName);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColLinkType);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColLinkScope);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColLinkWebHtml);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColLinkWebURL);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColRole);
    m_ColPermissionId->AddMultiValueExplosionSister(m_ColShareId);

	SetColumnsPresetFlags( {m_ColPermissionId,
							m_ColGrantedToApplicationIdentityID,
							m_ColGrantedToApplicationIdentityName,
							m_ColGrantedToDeviceIdentityID,
							m_ColGrantedToDeviceIdentityName,
							m_ColGrantedToUserIdentityID,
							m_ColTarget,
							m_ColGrantedToUserIdentityName,
							m_ColGrantedToUserIdentityEmail,
							m_ColInvitationEmail,
							m_ColInvitationInvitedByApplicationIdentityId,
							m_ColInvitationInvitedByApplicationIdentityName,
							m_ColInvitationInvitedByDeviceIdentityId,
							m_ColInvitationInvitedByDeviceIdentityName,
							m_ColInvitationInvitedByUserIdentityId,
							m_ColInvitationInvitedByUserIdentityName,
							m_ColInvitationInvitedByUserIdentityEmail,
							m_ColInvitationSignInRequired,
							m_ColInheritedFromDriveID,
							m_ColInheritedFromDriveType,
							m_ColInheritedFromId,
							m_ColInheritedFromName,
							m_ColInheritedFromPath,
							m_ColInheritedFromShareID,
							m_ColInheritedFromSharepointIDsListID,
							m_ColInheritedFromSharepointIDsListItemID,
							m_ColInheritedFromSharepointIDsListItemUniqueID,
							m_ColInheritedFromSharepointIDsSiteID,
							m_ColInheritedFromSharepointIDsSiteURL,
							m_ColInheritedFromSharepointIDsWebID,
							m_ColLinkApplicationIdentityId,
							m_ColLinkApplicationIdentityName,
							m_ColLinkType,
							m_ColLinkScope,
							m_ColLinkWebHtml,
							m_ColLinkWebURL,
							m_ColRole,
							m_ColShareId }, 
							{ g_ColumnsPresetMore });
	
	addTopCategories(YtriaTranslate::Do(GridDriveItems_customizeGrid_102, _YLOC("Drive Item Info")).c_str(), YtriaTranslate::Do(GridDriveItems_customizeGrid_103, _YLOC("Drive Item Permissions")).c_str(), true);

	if (m_TemplateUsers)
		m_TemplateUsers->CustomizeGrid(*this);
	if (m_TemplateGroups)
		m_TemplateGroups->CustomizeGrid(*this);

	// If grid has no edit available, we must disable checkboxes in-grid-editing.
	//if (!HasEdit())
		//m_ColDeleted->SetReadOnly(true);
    
    ASSERT(nullptr != m_ColFileSize);
    if (nullptr != m_ColFileSize)
        m_ColFileSize->SetXBytesColumnFormat();

    ASSERT(nullptr != m_ColFolderSize);
    if (nullptr != m_ColFolderSize)
        m_ColFolderSize->SetXBytesColumnFormat();

    ASSERT(nullptr != m_ColTotalSize);
    if (nullptr != m_ColTotalSize)
        m_ColTotalSize->SetXBytesColumnFormat();

	m_PublicationLevelIcons[_YTEXT("published")] = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_DRIVEITEM_PUBLISHED));
	m_PublicationLevelIcons[_YTEXT("checkout")] = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_DRIVEITEM_CHECKOUT));

	m_FrameDriveItems = dynamic_cast<FrameDriveItems*>(GetParentFrame());
	ASSERT(nullptr != m_FrameDriveItems);

	if (nullptr != m_ColCheckoutUser)
	{
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
		for (auto col : getCheckoutStatusFamilyColumns())
		{
			if (isCheckoutStatusDataColumn(col))
			{
				col->SetNoFieldTooltip(g_CheckoutNoFieldTooltip);
				col->SetNoFieldText(g_CheckoutNoFieldText);
				col->SetNoFieldIcon(AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_LM_CHECKOUT)));
			}
		}
#endif
		auto& persistentSession = m_FrameDriveItems->GetSessionInfo();
		if (persistentSession.IsRole() || persistentSession.IsElevated() || persistentSession.IsPartnerElevated() || persistentSession.IsUltraAdmin())
			m_NameForCheckout = _YTEXT("SharePoint App");
		else
			m_NameForCheckout = persistentSession.GetFullname();
	}


	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_FrameDriveItems);

    SetRowFlagger(std::make_unique<SubItemsFlagGetter>(GetModifications(), m_ColPermissionId->GetMultiValueElderColumn()));
}

wstring GridDriveItems::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("User"), getColMetaDisplayName() } }, m_ColName);
}

row_pk_t GridDriveItems::UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody)
{
	ASSERT(IsGridModificationsEnabled());
	if (IsGridModificationsEnabled())
	{
		auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPk);
		ASSERT(nullptr != mod);
		if (nullptr != mod)
		{
			DriveItemDeserializer did;
			did.Deserialize(json::value::parse(creationResultBody));
			auto& di = did.GetData();

			for (auto& f : rowPk)
			{
				if (f.GetColID() == GetColId()->GetID())
				{
					f.SetValue(di.GetID());
					break;
				}
			}

			mod->SetNewPK(rowPk);

			return rowPk;
		}
	}

	return rowPk;
}

bool GridDriveItems::IsMyData() const
{
	return m_IsMyData;
}

bool GridDriveItems::GetRowsWithCheckoutStatusLoaded(vector<GridBackendRow*>& p_RowsWithCheckoutStatus)
{
	p_RowsWithCheckoutStatus.clear();

	if (nullptr != m_ColCheckoutUser)
	{
		for (auto row : GetBackendRows())
		{
			ASSERT(nullptr != row);
			if (HasRowCheckoutStatus(row))
				p_RowsWithCheckoutStatus.push_back(row);
		}
	}

	return !p_RowsWithCheckoutStatus.empty();
}

O365Grid::SnapshotCaps GridDriveItems::GetSnapshotCapabilities() const
{
	// Not for channel files for now (but it works).
	return { Origin::Channel != GetOrigin() };
}

void GridDriveItems::customizeGridPostProcess()
{
	O365Grid::customizeGridPostProcess();
	SetNoFieldTextAndIcon(GetColumnsByPresets({ g_ColumnsPresetMore }), IDB_ICON_LM_PERMISSIONS, YtriaTranslate::Do(GridDriveItems_customizeGridPostProcess_1, _YLOC("To see this information, use the 'Load Permission Info' button")).c_str(), YtriaTranslate::Do(GridDriveItems_customizeGridPostProcess_2, _YLOC(" - Use the 'Load Permission Info' button to display additional information - ")).c_str());
	SetLoadMoreStatus(true,		YtriaTranslate::Do(GridDriveItems_customizeGridPostProcess_3, _YLOC("Loaded")).c_str(),		IDB_ICON_LMSTATUS_LOADED_PERMISSIONS);
	SetLoadMoreStatus(false,	YtriaTranslate::Do(GridDriveItems_customizeGridPostProcess_4, _YLOC("Not loaded")).c_str(),	IDB_ICON_LMSTATUS_NOTLOADED_PERMISSIONS);
}

void GridDriveItems::BuildTreeView(const O365DataMap<BusinessUser, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_Updates, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses)
{
	ASSERT(Origin::User == m_Origin || Origin::DeletedUser == m_Origin);
	buildTreeViewImpl(p_DriveItems, p_Updates, p_UpdateGrid, p_FullPurge, p_Permissions, p_CheckoutStatuses);
}

void GridDriveItems::BuildTreeView(const O365DataMap<BusinessGroup, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_Updates, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses)
{
	ASSERT(Origin::Group == m_Origin);
	buildTreeViewImpl(p_DriveItems, p_Updates, p_UpdateGrid, p_FullPurge, p_Permissions, p_CheckoutStatuses);
}

void GridDriveItems::BuildTreeView(const O365DataMap<BusinessSite, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_Updates, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses)
{
	ASSERT(Origin::Site == m_Origin);
	buildTreeViewImpl(p_DriveItems, p_Updates, p_UpdateGrid, p_FullPurge, p_Permissions, p_CheckoutStatuses);
}

void GridDriveItems::BuildTreeView(const O365DataMap<BusinessChannel, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_Updates, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses)
{
	ASSERT(Origin::Channel == m_Origin);
	buildTreeViewImpl(p_DriveItems, p_Updates, p_UpdateGrid, p_FullPurge, p_Permissions, p_CheckoutStatuses);
}

GridBackendColumn* GridDriveItems::GetColMetaID() const
{
	return getColMetaID();
}

GridBackendColumn* GridDriveItems::GetColMetaDriveID() const
{
	return m_ColMetaDriveId;
}

GridBackendColumn* GridDriveItems::GetColDownloadProgress() const
{
    return m_ColDownloadProgress;
}

ModuleCriteria GridDriveItems::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_FrameDriveItems->GetModuleCriteria();

    criteria.m_IDs.clear();
    for (auto row : p_Rows)
        criteria.m_IDs.insert(row->GetField(GetColId()).GetValueStr());

    return criteria;
}

void GridDriveItems::loadPermissions(const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, GridUpdater& updater)
{
	for (const auto& keyVal : p_Permissions)
	{
		vector<GridBackendField> pk = { {keyVal.first.first, GetColMetaID()}, {keyVal.first.second, GetColId()} };

		GridBackendRow* row = GetRowIndex().GetExistingRow(pk, updater);

		if (nullptr != row)
		{
			updater.GetOptions().AddRowWithRefreshedValues(row);

			auto& permissions = keyVal.second;
			if (permissions.size() == 1 && permissions[0].GetError())
			{
				ClearFieldsByPreset(row, O365Grid::g_ColumnsPresetMore);
				updater.OnLoadingError(row, *permissions[0].GetError());
			}
			else
			{
				fillPermissionFields(row, permissions);
			}
		}
	}
}

void GridDriveItems::loadCkeckoutStatuses(const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses, GridUpdater& updater)
{
	ASSERT(nullptr != m_ColCheckoutLoaded);
	for (const auto& keyVal : p_CheckoutStatuses)
	{
		vector<GridBackendField> pk = { {keyVal.first.first, GetColMetaID()}, {keyVal.first.second, GetColId()} };

		GridBackendRow* row = GetRowIndex().GetExistingRow(pk, updater);

		if (nullptr != row)
		{
			auto& cs = keyVal.second;
			updater.GetOptions().AddRowWithRefreshedValues(row);
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
			row->AddFieldForCheckBox(true, m_ColCheckoutLoaded);
#endif
			fillCheckoutStatus(row, cs, &updater);
		}
	}
}

void GridDriveItems::ViewPermissions(const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const bool p_RefreshUpdates, const bool p_Explode, const bool p_Flat, bool p_UpdateGrid)
{
	// Merge permissions
	for (const auto& val : p_Permissions)
		m_AllPermissions[val.first] = val.second;

	ASSERT(!p_RefreshUpdates || !p_Explode && !p_Flat);

	GridIgnoreModification ignoramus(*this);
	const uint32_t options	= (p_UpdateGrid ? GridUpdaterOptions::UPDATE_GRID : 0)
							/*| GridUpdaterOptions::PURGE*/
							| (p_RefreshUpdates ? GridUpdaterOptions::REFRESH_MODIFICATION_STATES : 0)
							| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
							/*| GridUpdaterOptions::REFRESH_PROCESS_DATA*/
							| GridUpdaterOptions::PROCESS_LOADMORE;
	GridUpdaterOptions updateOptions(options, GetColumnsByPresets({ g_ColumnsPresetMore }));
	GridUpdater updater(*this, updateOptions);

	loadPermissions(p_Permissions, updater);
	
	if (p_Flat)
		updater.GetOptions().AddAction(GridBackendUtil::GRIDPOSTUPATEACTION::SETFLATVIEW);

	if (p_Explode)
		updater.GetOptions().AddAction(GridBackendUtil::GRIDPOSTUPATEACTION::EXPLODEMULTIVALUES);
}

#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
void GridDriveItems::ViewCheckoutStatuses(const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses, bool p_UpdateGrid)
{
	ASSERT(nullptr != m_ColCheckoutLoaded);

	GridIgnoreModification ignoramus(*this);
	const uint32_t options = (p_UpdateGrid ? GridUpdaterOptions::UPDATE_GRID : 0)
		/*| GridUpdaterOptions::PURGE*/
		/*| GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		/*| GridUpdaterOptions::REFRESH_PROCESS_DATA*/
		/*| GridUpdaterOptions::PROCESS_LOADMORE*/;
	GridUpdaterOptions updateOptions(options, getCheckoutStatusFamilyColumns());
	GridUpdater updater(*this, updateOptions);

	loadCkeckoutStatuses(p_CheckoutStatuses, updater);
}

bool GridDriveItems::HasRowCheckoutStatus(const GridBackendRow* p_Row) const
{
	return nullptr != p_Row && p_Row->GetField(m_ColCheckoutLoaded).GetValueBool();
}

#endif

DriveChanges GridDriveItems::GetDriveChanges(bool p_Selected)
{
    DriveChanges changes;

	// Remove if we don't want to apply whole users but single messages.
	auto sts = p_Selected	? std::make_unique<ScopedTemporarySelection>(*this, ScopedTemporarySelection::Mode::SELECT_SELECTION_TOP_LEVEL_AND_ALL_CHILDREN, true)
							: std::unique_ptr<ScopedTemporarySelection>();

    // Drive item modifications
	TemporarilyImplodeNonGroupRowsAndExec(*this
		, p_Selected ? TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS : TemporarilyImplodeNonGroupRowsAndExec::ALL_ROWS
		, [&changes, this, p_Selected](const bool /*p_HasRowsToReExplode*/) mutable
		{
			ASSERT(changes.m_Updates.m_ObjectsToUpdate.empty());

			vector<GridBackendRow*> rows;
			if (p_Selected)
				GetSelectedNonGroupRows(rows);
			else
				GetAllNonGroupRows(rows);

			for (const auto& row : rows)
			{
				// FIXME: We should use GridModifications instead. Row status is not 100% guaranteed to be up-to-date...
				ASSERT(nullptr != row);
				if (GridUtil::IsBusinessDriveItem(row) || GridUtil::IsBusinessDriveItemFolder(row) || GridUtil::IsBusinessDriveItemNotebook(row))
				{
					if (row->IsModified())
					{
						ASSERT(!row->IsError());
						auto obj = getBusinessObject(row);
						obj.SetUpdateStatus(BusinessObject::UPDATE_TYPE_MODIFY);

						// FIXME: Use GridModifications

						// Crappy
						// Only two fields can be modified : Name and Checkout User
						// If none of them, nor an awaiting upload, it's a sub item modification... (see below)
						if (row->GetField(m_ColName).IsModified() // Name mod?
							|| row->GetField(m_ColCheckoutUser).IsModified() // Checkout status mod?
							|| row->HasField(m_ColFileToBeUploaded) //File upload?
							)
						{
							// Clear them if not modified (was filled by getBusinessObject)
							if (!row->GetField(m_ColName).IsModified())
								obj.SetName(boost::none);

							// FIXME: Should we move this to getBusinessObject (as for the Name) ?
							if (row->GetField(m_ColCheckoutUser).IsModified())
							{
								// FIXME: Adapted from old code, that's why it still uses Publication as container for checkin/checkout.

								PublicationFacet pub;
								if (row->HasField(m_ColCheckoutUser)
									&& nullptr != row->GetField(m_ColCheckoutUser).GetValueStr()
									&& !wstring(row->GetField(m_ColCheckoutUser).GetValueStr()).empty())
								{
									pub.m_Level = _YTEXT("checkout");
								}
								else
								{
									pub.m_Level = _YTEXT("published");
								}
								obj.SetPublication(pub);
							}
							else
							{
								obj.SetPublication(boost::none);
							}

							changes.m_Updates.m_ObjectsToUpdate.push_back(obj);

							vector<GridBackendField> rowPk;
							GetRowPK(row, rowPk);
							changes.m_Updates.m_PrimaryKeys.push_back(rowPk);
						}
					}
					else if (row->IsCreated())
					{
						ASSERT(!row->IsError());
						auto obj = getBusinessObject(row);
						obj.SetUpdateStatus(BusinessObject::UPDATE_TYPE_CREATE);
						changes.m_Updates.m_ObjectsToUpdate.push_back(obj);

						vector<GridBackendField> rowPk;
						GetRowPK(row, rowPk);
						changes.m_Updates.m_PrimaryKeys.push_back(rowPk);
					}
					else if (row->IsDeleted())
					{
						ASSERT(!row->IsError());
						auto obj = getBusinessObject(row);
						obj.SetUpdateStatus(BusinessObject::UPDATE_TYPE_DELETE);
						changes.m_Updates.m_ObjectsToUpdate.push_back(obj);

						vector<GridBackendField> rowPk;
						GetRowPK(row, rowPk);
						changes.m_Updates.m_PrimaryKeys.push_back(rowPk);
					}
				}
			}
		})();

    // Permission deletions
	for (const auto& deletedPermission : GetModifications().GetSubItemDeletions())
	{
		if (!p_Selected || (p_Selected && isModInSelectedRows(deletedPermission.get())))
			changes.m_PermsToDelete.emplace_back(deletedPermission->GetKey());
	}

    // Permission updates
    for (const auto& permissionUpdates : GetModifications().GetSubItemUpdates())
    {
        ASSERT(nullptr != permissionUpdates);
        if (nullptr != permissionUpdates)
        {
            if (!p_Selected || (p_Selected && isModInSelectedRows(permissionUpdates.get())))
            {
                // Generate a BusinessPermission for every RowSubItemFieldUpdates object
                BusinessPermission permission;
                permission.SetID(permissionUpdates->GetKey().m_Id3);

                // As long as we handle only the "Role" in permissions, this assert should not trigger.
                // If we handle more properties, this code should be updated in consequence and this assert should be removed.
                ASSERT(permissionUpdates->GetFieldUpdates().empty() || permissionUpdates->GetFieldUpdates().size() == 1);
                for (const auto& fieldUpdate : permissionUpdates->GetFieldUpdates())
                {
                    if (fieldUpdate->GetColumnKey() == m_ColRole->GetID())
                        permission.SetRole(vector<PooledString>{ fieldUpdate->GetNewValue().GetValueStr() });
                }
                auto permToUpdate = std::make_pair(permission, permissionUpdates->GetKey());
                changes.m_PermsToUpdate.emplace_back(permToUpdate);
            }
        }
    }

	return changes;
}

void GridDriveItems::loadMoreImpl(const std::vector<GridBackendRow*>& p_Rows)
{
    requestViewPermissions(p_Rows, Command::ModuleTask::ListPermissions);
}

bool GridDriveItems::isModInSelectedRows(ISubItemModification* p_Mod)
{
    if (nullptr == p_Mod)
        return false;

    for (auto row : GetSelectedRows())
    {
        if (p_Mod->IsCorrespondingRow(row))
            return true;
    }

    return false;
}

void GridDriveItems::buildTreeViewGenericImpl(const BusinessDriveItem& p_DriveItem, GridBackendRow* p_ParentRow, GridUpdater& p_Updater)
{
	const BusinessDriveItemFolder*		folderItem		= nullptr;
	const BusinessDriveItemNotebook*	notebookItem	= nullptr;
	bool createRow = true;
	if (p_DriveItem.IsFolder())
	{
		folderItem = dynamic_cast<const BusinessDriveItemFolder*>(&p_DriveItem);
		ASSERT(nullptr != folderItem && nullptr == dynamic_cast<const BusinessDriveItemNotebook*>(&p_DriveItem));
		if (nullptr != folderItem && folderItem->IsRoot())
			createRow = false;
	}
	else if (p_DriveItem.IsNotebook())
	{
		notebookItem = dynamic_cast<const BusinessDriveItemNotebook*>(&p_DriveItem);
	}
	
	GridBackendRow* row = createRow ? fillRow(p_DriveItem, p_ParentRow, true, &p_Updater) : p_ParentRow;
	if (nullptr != row)
	{
		row->SetHierarchyParentToHide(p_DriveItem.IsFolder() || p_DriveItem.IsNotebook());

		if (!createRow) // Root
		{
			fillCountsAndSizes(folderItem, row);
			if (!IsMyData())
				p_Updater.GetOptions().AddRowWithRefreshedValues(row);
		}
		else
		{
			p_Updater.GetOptions().AddRowWithRefreshedValues(row);
		}

		if (p_DriveItem.IsFolder())
		{
			ASSERT(nullptr != folderItem);
			if (nullptr != folderItem)
			{
				if (!createRow) // This is the root. Set drive ID.
					row->AddField(p_DriveItem.GetDriveId(), m_ColMetaDriveId);

				for (const auto& child : folderItem->GetChildrenFolder())
				{
					if (child.GetError())
					{
						p_Updater.OnLoadingError(row, *child.GetError());
					}
					else
					{
						// Clear previous error
						if (row->IsError())
							row->ClearStatus();

						buildTreeViewGenericImpl(child, row, p_Updater);
					}
				}
				for (const auto& child : folderItem->GetChildrenNotebook())
				{
					if (child.GetError())
					{
						p_Updater.OnLoadingError(row, *child.GetError());
					}
					else
					{
						// Clear previous error
						if (row->IsError())
							row->ClearStatus();

						buildTreeViewGenericImpl(child, row, p_Updater);
					}
				}
				for (const auto& child : folderItem->GetChildren())
				{
					if (child.GetError())
					{
						p_Updater.OnLoadingError(row, *child.GetError());
					}
					else
					{
						// Clear previous error
						if (row->IsError())
							row->ClearStatus();

						buildTreeViewGenericImpl(child, row, p_Updater);
					}
				}
			}
		}
		else if (p_DriveItem.IsNotebook())
		{
			ASSERT(nullptr != notebookItem);
			if (nullptr != notebookItem)
			{
				if (!createRow) // This is the root. Set drive ID.
					row->AddField(p_DriveItem.GetDriveId(), m_ColMetaDriveId);

				for (const auto& child : notebookItem->GetChildrenFolder())
				{
					if (child.GetError())
					{
						p_Updater.OnLoadingError(row, *child.GetError());
					}
					else
					{
						// Clear previous error
						if (row->IsError())
							row->ClearStatus();

						buildTreeViewGenericImpl(child, row, p_Updater);
					}
				}
				for (const auto& child : notebookItem->GetChildrenNotebook())
				{
					if (child.GetError())
					{
						p_Updater.OnLoadingError(row, *child.GetError());
					}
					else
					{
						// Clear previous error
						if (row->IsError())
							row->ClearStatus();

						buildTreeViewGenericImpl(child, row, p_Updater);
					}
				}
				for (const auto& child : notebookItem->GetChildren())
				{
					if (child.GetError())
					{
						p_Updater.OnLoadingError(row, *child.GetError());
					}
					else
					{
						// Clear previous error
						if (row->IsError())
							row->ClearStatus();

						buildTreeViewGenericImpl(child, row, p_Updater);
					}
				}
			}
		}
	}
}

void GridDriveItems::fillCountsAndSizes(const BusinessDriveItemFolder* p_FolderItem, GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_Row);
	ASSERT(nullptr != p_FolderItem);

	if (nullptr != p_Row && nullptr != p_FolderItem)
	{
		if (p_FolderItem->GetRecursiveCountFiles() > 0)
			p_Row->AddField(p_FolderItem->GetRecursiveCountFiles(), m_ColFileCount);
		else
			p_Row->RemoveField(m_ColFileCount);

		if (p_FolderItem->GetRecursiveCountFolders() > 0)
			p_Row->AddField(p_FolderItem->GetRecursiveCountFolders(), m_ColFolderCount);
		else
			p_Row->RemoveField(m_ColFolderCount);

		if (p_FolderItem->GetFolderSize() > 0)
			p_Row->AddField(p_FolderItem->GetFolderSize(), m_ColFolderSize);
		else
			p_Row->RemoveField(m_ColFolderSize);

		if (p_FolderItem->GetTotalSize() > 0)
			p_Row->AddField(p_FolderItem->GetTotalSize(), m_ColTotalSize);
		else			
			p_Row->RemoveField(m_ColTotalSize);
	}
}

GridBackendColumn*& GridDriveItems::getColMetaDisplayName() const
{
	return m_TemplateUsers	? m_TemplateUsers->m_ColumnDisplayName
							: m_TemplateGroups	? m_TemplateGroups->m_ColumnDisplayName
												: m_SiteMetaColumns ? m_SiteMetaColumns->m_ColumnDisplayName
																	: m_ChannelMetaColumns->m_ColumnChannelDisplayName;
}

GridBackendColumn*& GridDriveItems::getColMetaID() const
{
	return m_TemplateUsers	? m_TemplateUsers->m_ColumnID
							: m_TemplateGroups	? m_TemplateGroups->m_ColumnID
												: m_SiteMetaColumns ? m_SiteMetaColumns->m_ColumnID
																	: m_ChannelMetaColumns->m_ColumnChannelID;
}

GridBackendRow* GridDriveItems::addParentRow(const BusinessUser& p_User, GridUpdater& p_Updater)
{
	if (m_HasLoadMoreAdditionalInfo)
	{
		if (p_User.IsMoreLoaded())
			m_MetaIDsMoreLoaded.insert(p_User.GetID());
		else
			m_MetaIDsMoreLoaded.erase(p_User.GetID());
	}

	ASSERT(m_TemplateUsers);
	if (m_TemplateUsers)
		return m_TemplateUsers->AddRow(*this, p_User, {}, p_Updater, false, true);
	return nullptr;
}

GridBackendRow* GridDriveItems::addParentRow(const BusinessGroup& p_Group, GridUpdater& p_Updater)
{
	if (m_HasLoadMoreAdditionalInfo)
	{
		if (p_Group.IsMoreLoaded())
			m_MetaIDsMoreLoaded.insert(p_Group.GetID());
		else
			m_MetaIDsMoreLoaded.erase(p_Group.GetID());
	}

	ASSERT(m_TemplateGroups);
	if (m_TemplateGroups)
		return m_TemplateGroups->AddRow(*this, p_Group, {}, p_Updater, false, nullptr, {}, true);
	return nullptr;
}

GridBackendRow* GridDriveItems::addParentRow(const BusinessSite& p_Site, GridUpdater& p_Updater)
{
	ASSERT(m_SiteMetaColumns);
	if (m_SiteMetaColumns)
	{
		GridBackendField fID(p_Site.GetID(), GetColId());
		GridBackendField fMetaID(p_Site.GetID(), GetColMetaID());
		GridBackendRow* siteRow = GetRowIndex().GetRow({ fID, fMetaID }, true, true);
		ASSERT(nullptr != siteRow);
		if (nullptr != siteRow)
		{
			GridUtil::SetRowObjectType(*this, siteRow, p_Site);
			siteRow->AddField(p_Site.GetDisplayName(), getColMetaDisplayName());
		}
		return siteRow;
	}
	return nullptr;
}

GridBackendRow* GridDriveItems::addParentRow(const BusinessChannel& p_Channel, GridUpdater& p_Updater)
{
	ASSERT(m_ChannelMetaColumns);
	if (m_ChannelMetaColumns)
	{
		GridBackendField fID(p_Channel.GetID(), GetColId());
		GridBackendField fMetaID(p_Channel.GetID(), GetColMetaID());
		GridBackendRow* channelRow = GetRowIndex().GetRow({ fID, fMetaID }, true, true);
		ASSERT(nullptr != channelRow);
		if (nullptr != channelRow)
		{
			GridUtil::SetRowObjectType(*this, channelRow, p_Channel);

			channelRow->AddField(p_Channel.GetDisplayName(), m_ChannelMetaColumns->m_ColumnChannelDisplayName);
			channelRow->AddField(p_Channel.GetID(), m_ChannelMetaColumns->m_ColumnChannelID);
			channelRow->AddField(p_Channel.GetGroupId(), m_ChannelMetaColumns->m_ColumnTeamID);
			ASSERT(p_Channel.GetGroupId());
			if (p_Channel.GetGroupId())
			{
				auto cachedGroup = GetGraphCache().GetGroupInCache(*p_Channel.GetGroupId());
				ASSERT(cachedGroup.GetID() == *p_Channel.GetGroupId());
				channelRow->AddField(cachedGroup.GetDisplayName(), m_ChannelMetaColumns->m_ColumnTeamDisplayName);
			}
		}
		return channelRow;
	}
	return nullptr;
}

GridBackendRow* GridDriveItems::fillRow(const BusinessDriveItem& p_BusinessDriveItem, GridBackendRow* p_ParentRow, bool p_LoadOrRefresh, GridUpdater* p_Updater)
{
	ASSERT(nullptr != p_ParentRow || m_MyDataMeHandler && m_MyDataMeHandler->HasField(GetColMetaID()));

	GridBackendField fID(p_BusinessDriveItem.GetID(), GetColId());
	GridBackendRow* row = nullptr;
	if (nullptr != p_ParentRow)
	{
		row = GetRowIndex().GetRow({ p_ParentRow->GetField(GetColMetaID()), fID }, true, p_LoadOrRefresh);
	}
	else
	{
		ASSERT(m_MyDataMeHandler && m_MyDataMeHandler->HasField(GetColMetaID()));
		if (m_MyDataMeHandler)
			row = GetRowIndex().GetRow({ m_MyDataMeHandler->GetField(GetColMetaID()), fID }, true, p_LoadOrRefresh);
	}

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		row->SetParentRow(p_ParentRow);

		const BusinessDriveItemFolder* folderItem = nullptr;
		const BusinessDriveItemNotebook* notebookItem = nullptr;
		if (p_BusinessDriveItem.IsFolder())
		{
			folderItem = dynamic_cast<const BusinessDriveItemFolder*>(&p_BusinessDriveItem);
			ASSERT(nullptr != folderItem && nullptr == dynamic_cast<const BusinessDriveItemNotebook*>(&p_BusinessDriveItem));
			if (nullptr != folderItem)
				SetRowObjectType(row, *folderItem, folderItem->HasFlag(BusinessObject::CANCELED));
		}
		else if (p_BusinessDriveItem.IsNotebook())
		{
			notebookItem = dynamic_cast<const BusinessDriveItemNotebook*>(&p_BusinessDriveItem);
			ASSERT(nullptr != notebookItem);
			if (nullptr != notebookItem)
				SetRowObjectType(row, *notebookItem, notebookItem->HasFlag(BusinessObject::CANCELED));
		}
		else
		{
			SetRowObjectType(row, p_BusinessDriveItem, p_BusinessDriveItem.HasFlag(BusinessObject::CANCELED));
		}

		// Meta fields
		if (nullptr != p_ParentRow)
		{
			for (auto c : m_MetaColumns)
			{
				if (p_ParentRow->HasField(c))
					row->AddField(p_ParentRow->GetField(c));
				else
					row->RemoveField(c);
			}
		}
		else
		{
			ASSERT(m_MyDataMeHandler);
			if (m_MyDataMeHandler)
			{
				for (auto c : m_MetaColumns)
				{
					if (m_MyDataMeHandler->HasField(c))
						row->AddField(m_MyDataMeHandler->GetField(c));
					else
						row->RemoveField(c);
				}
			}
		}

		row->AddField(p_BusinessDriveItem.GetDriveId(), m_ColMetaDriveId);

		wstring extension;
		if (p_BusinessDriveItem.GetLocalFilePath())
			extension = SetRowFileType(row, *p_BusinessDriveItem.GetLocalFilePath(), p_BusinessDriveItem.IsFolder() || p_BusinessDriveItem.IsNotebook());
		else
			extension = SetRowFileType(row, p_BusinessDriveItem.GetOneDrivePath(), p_BusinessDriveItem.IsFolder() || p_BusinessDriveItem.IsNotebook());
		row->AddField(extension, m_ColNameExtOnly);

		const wstring nameWithExt(p_BusinessDriveItem.GetName() ? *p_BusinessDriveItem.GetName() : _YTEXT(""));
		wstring directory{ p_BusinessDriveItem.GetOneDrivePath() };
		size_t nameOffset = directory.find(nameWithExt);
		if (nameOffset != std::wstring::npos)
			directory.erase(nameOffset);
		row->AddField(directory, m_ColDirectory);
		row->AddField(p_BusinessDriveItem.GetName(), m_ColName);

		row->AddField(p_BusinessDriveItem.GetOneDrivePath(), m_ColOneDrivePath);
		row->AddField(p_BusinessDriveItem.GetCreatedDateTime(), m_ColCreatedDateTime);
		row->AddField(p_BusinessDriveItem.GetLastModifiedDateTime(), m_ColLastModifiedDateTime);
		if (!p_BusinessDriveItem.GetWebUrl().IsEmpty())
			row->AddFieldForHyperlink(p_BusinessDriveItem.GetWebUrl(), m_ColWebUrl);

		if (nullptr != folderItem)
			fillCountsAndSizes(folderItem, row);
		else if (nullptr != notebookItem)
			fillCountsAndSizes(notebookItem, row);
		else
		{
			row->RemoveField(m_ColFileCount);
			row->RemoveField(m_ColFolderCount);
			row->RemoveField(m_ColFolderSize);
			row->RemoveField(m_ColTotalSize);
			row->AddField(p_BusinessDriveItem.GetTotalSize(), m_ColFileSize);
		}

		if (p_BusinessDriveItem.GetFileSystemInfo())
		{
			const FileSystemInfo &fileSystemInfo = *p_BusinessDriveItem.GetFileSystemInfo();
			row->AddField(fileSystemInfo.CreatedDateTime, m_ColFileSystemInfoCreatedDateTime);
			row->AddField(fileSystemInfo.LastModifiedDateTime, m_ColFileSystemInfoLastModifiedTime);
		}
		else
		{
			row->RemoveField(m_ColFileSystemInfoCreatedDateTime);
			row->RemoveField(m_ColFileSystemInfoLastModifiedTime);
		}

		if (p_BusinessDriveItem.GetCreatedBy())
		{
			const IdentitySet& createdBy = *p_BusinessDriveItem.GetCreatedBy();
			if (createdBy.User)
			{
				row->AddField(createdBy.User->DisplayName, m_ColCreatedByUserName);
				row->AddField(createdBy.User->Id, m_ColCreatedByUserId);
				row->AddField(createdBy.User->Email, m_ColCreatedByUserEmail);
			}
			else
			{
				row->RemoveField(m_ColCreatedByUserName);
				row->RemoveField(m_ColCreatedByUserId);
				row->RemoveField(m_ColCreatedByUserEmail);
			}
			if (createdBy.Application)
			{
				row->AddField(createdBy.Application->DisplayName, m_ColCreatedByAppName);
				row->AddField(createdBy.Application->Id, m_ColCreatedByAppId);
			}
			else
			{
				row->RemoveField(m_ColCreatedByAppName);
				row->RemoveField(m_ColCreatedByAppId);
			}
			if (createdBy.Device)
			{
				row->AddField(createdBy.Device->DisplayName, m_ColCreatedByDeviceName);
				row->AddField(createdBy.Device->Id, m_ColCreatedByDeviceId);
			}
			else
			{
				row->RemoveField(m_ColCreatedByDeviceName);
				row->RemoveField(m_ColCreatedByDeviceId);
			}
		}
		else
		{
			row->RemoveField(m_ColCreatedByUserName);
			row->RemoveField(m_ColCreatedByUserId);
			row->RemoveField(m_ColCreatedByUserEmail);
			row->RemoveField(m_ColCreatedByAppName);
			row->RemoveField(m_ColCreatedByAppId);
			row->RemoveField(m_ColCreatedByDeviceName);
			row->RemoveField(m_ColCreatedByDeviceId);
		}

		if (p_BusinessDriveItem.GetLastModifiedBy())
		{
			const IdentitySet& lastModifiedBy = *p_BusinessDriveItem.GetLastModifiedBy();
			if (lastModifiedBy.User)
			{
				row->AddField(lastModifiedBy.User->DisplayName, m_ColLastModifiedByUserName);
				row->AddField(lastModifiedBy.User->Id, m_ColLastModifiedByUserId);
				row->AddField(lastModifiedBy.User->Email, m_ColLastModifiedByUserEmail);
			}
			else
			{
				row->RemoveField(m_ColLastModifiedByUserName);
				row->RemoveField(m_ColLastModifiedByUserId);
				row->RemoveField(m_ColLastModifiedByUserEmail);
			}
			if (lastModifiedBy.Application)
			{
				row->AddField(lastModifiedBy.Application->DisplayName, m_ColLastModifiedByApplicationName);
				row->AddField(lastModifiedBy.Application->Id, m_ColLastModifiedByApplicationId);
			}
			else
			{
				row->RemoveField(m_ColLastModifiedByApplicationName);
				row->RemoveField(m_ColLastModifiedByApplicationId);
			}
			if (lastModifiedBy.Device)
			{
				row->AddField(lastModifiedBy.Device->DisplayName, m_ColLastModifiedByDeviceName);
				row->AddField(lastModifiedBy.Device->Id, m_ColLastModifiedByDeviceId);
			}
			else
			{
				row->RemoveField(m_ColLastModifiedByDeviceName);
				row->RemoveField(m_ColLastModifiedByDeviceId);
			}
		}
		else
		{
			row->RemoveField(m_ColLastModifiedByUserName);
			row->RemoveField(m_ColLastModifiedByUserId);
			row->RemoveField(m_ColLastModifiedByUserEmail);
			row->RemoveField(m_ColLastModifiedByApplicationName);
			row->RemoveField(m_ColLastModifiedByApplicationId);
			row->RemoveField(m_ColLastModifiedByDeviceName);
			row->RemoveField(m_ColLastModifiedByDeviceId);
		}

		if (p_BusinessDriveItem.GetFile())
		{
			const File& file = *p_BusinessDriveItem.GetFile();
			if (file.Hashes && file.Hashes->QuickXorHash)
				row->AddField(*file.Hashes->QuickXorHash, m_ColFileHashes);
			else
				row->RemoveField(m_ColFileHashes);

			if (file.MimeType)
				row->AddField(*file.MimeType, m_ColFileMimeType);
			else
				row->RemoveField(m_ColFileMimeType);
		}
		else
		{
			row->RemoveField(m_ColFileHashes);
			row->RemoveField(m_ColFileMimeType);
		}

        if (p_BusinessDriveItem.GetShared())
			row->AddField(g_SharedShared, m_ColShared);
        else if (m_FrameDriveItems->GetOrigin() == Origin::User)
			row->AddField(g_SharedPrivate, m_ColShared);
		else
			row->RemoveField(m_ColShared);

		if (p_BusinessDriveItem.GetShared())
		{
			const Shared& shared = *p_BusinessDriveItem.GetShared();
			if (shared.Owner)
			{
				const IdentitySet& owner = *shared.Owner;
				if (owner.User)
				{
					row->AddField(owner.User->DisplayName, m_ColSharedOwnerUserName);
					row->AddField(owner.User->Id, m_ColSharedOwnerUserId);
					row->AddField(owner.User->Email, m_ColSharedOwnerUserEmail);
				}
				else
				{
					row->RemoveField(m_ColSharedOwnerUserName);
					row->RemoveField(m_ColSharedOwnerUserId);
					row->RemoveField(m_ColSharedOwnerUserEmail);
				}

				if (owner.Application)
				{
					row->AddField(owner.Application->DisplayName, m_ColSharedOwnerApplicationName);
					row->AddField(owner.Application->Id, m_ColSharedOwnerApplicationId);
				}
				else
				{
					row->RemoveField(m_ColSharedOwnerApplicationName);
					row->RemoveField(m_ColSharedOwnerApplicationId);
				}

				if (owner.Device)
				{
					row->AddField(owner.Device->DisplayName, m_ColSharedOwnerDeviceName);
					row->AddField(owner.Device->Id, m_ColSharedOwnerDeviceId);
				}
				else
				{
					row->RemoveField(m_ColSharedOwnerDeviceName);
					row->RemoveField(m_ColSharedOwnerDeviceId);
				}
			}
			row->AddField(shared.Scope, m_ColSharedScope);
			if (shared.SharedBy)
			{
				const IdentitySet& sharedBy = *shared.SharedBy;
				if (sharedBy.User)
				{
					row->AddField(sharedBy.User->DisplayName, m_ColSharedSharedByUserName);
					row->AddField(sharedBy.User->Id, m_ColSharedSharedByUserId);
					row->AddField(sharedBy.User->Email, m_ColSharedSharedByUserEmail);
				}
				else
				{
					row->RemoveField(m_ColSharedSharedByUserName);
					row->RemoveField(m_ColSharedSharedByUserId);
					row->RemoveField(m_ColSharedSharedByUserEmail);
				}

				if (sharedBy.Application)
				{
					row->AddField(sharedBy.Application->DisplayName, m_ColSharedSharedByApplicationName);
					row->AddField(sharedBy.Application->Id, m_ColSharedSharedByApplicationId);
				}
				else
				{
					row->RemoveField(m_ColSharedSharedByApplicationName);
					row->RemoveField(m_ColSharedSharedByApplicationId);
				}

				if (sharedBy.Device)
				{
					row->AddField(sharedBy.Device->DisplayName, m_ColSharedSharedByDeviceName);
					row->AddField(sharedBy.Device->Id, m_ColSharedSharedByDeviceId);
				}
				else
				{
					row->RemoveField(m_ColSharedSharedByDeviceName);
					row->RemoveField(m_ColSharedSharedByDeviceId);
				}
			}
			row->AddField(shared.SharedDateTime, m_ColSharedSharedDateTime);
		}
		else
		{
			row->RemoveField(m_ColSharedOwnerUserName);
			row->RemoveField(m_ColSharedOwnerUserId);
			row->RemoveField(m_ColSharedOwnerUserEmail);
			row->RemoveField(m_ColSharedOwnerApplicationName);
			row->RemoveField(m_ColSharedOwnerApplicationId);
			row->RemoveField(m_ColSharedOwnerDeviceName);
			row->RemoveField(m_ColSharedOwnerDeviceId);
			row->RemoveField(m_ColSharedScope);
			row->RemoveField(m_ColSharedSharedByUserName);
			row->RemoveField(m_ColSharedSharedByUserId);
			row->RemoveField(m_ColSharedSharedByUserEmail);
			row->RemoveField(m_ColSharedSharedByApplicationName);
			row->RemoveField(m_ColSharedSharedByApplicationId);
			row->RemoveField(m_ColSharedSharedByDeviceName);
			row->RemoveField(m_ColSharedSharedByDeviceId);
			row->RemoveField(m_ColSharedSharedDateTime);
		}

        if (p_BusinessDriveItem.GetPackage() != boost::none)
            row->AddField(*p_BusinessDriveItem.GetPackage()->Type, m_ColPackageType);
		row->AddField(p_BusinessDriveItem.GetETag(), m_ColETag);
		row->AddField(p_BusinessDriveItem.GetCTag(), m_ColCTag);

		row->AddField(p_BusinessDriveItem.GetDescription(), m_ColDescription);

		
		if (p_BusinessDriveItem.GetPublication())
		{
			if (nullptr != m_ColPublicationLevel)
				row->AddField(p_BusinessDriveItem.GetPublication()->m_Level, m_ColPublicationLevel, m_PublicationLevelIcons[p_BusinessDriveItem.GetPublication()->m_Level]);
			row->AddField(p_BusinessDriveItem.GetPublication()->m_VersionId, m_ColPublicationVersionId);
		}
		else
		{
			if (nullptr != m_ColPublicationLevel)
				row->RemoveField(m_ColPublicationLevel);
			row->RemoveField(m_ColPublicationVersionId);
		}

		if (p_BusinessDriveItem.GetSharepointIds())
		{
			row->AddField(p_BusinessDriveItem.GetSharepointIds()->ListId, m_ColSharepointIDsListID);
			row->AddField(p_BusinessDriveItem.GetSharepointIds()->ListItemId, m_ColSharepointIDsListItemID);
			row->AddField(p_BusinessDriveItem.GetSharepointIds()->ListItemUniqueId, m_ColSharepointIDsListItemUniqueID);
			row->AddField(p_BusinessDriveItem.GetSharepointIds()->SiteId, m_ColSharepointIDsSiteID);
			row->AddFieldForHyperlink(p_BusinessDriveItem.GetSharepointIds()->SiteUrl, m_ColSharepointIDsSiteURL);
			row->AddField(p_BusinessDriveItem.GetSharepointIds()->WebId, m_ColSharepointIDsWebID);
		}
		else
		{
			row->RemoveField(m_ColSharepointIDsListID);
			row->RemoveField(m_ColSharepointIDsListItemID);
			row->RemoveField(m_ColSharepointIDsListItemUniqueID);
			row->RemoveField(m_ColSharepointIDsSiteID);
			row->RemoveField(m_ColSharepointIDsSiteURL);
			row->RemoveField(m_ColSharepointIDsWebID);
		}

		if (!p_BusinessDriveItem.GetWebDavUrl().IsEmpty())
			row->AddFieldForHyperlink(p_BusinessDriveItem.GetWebDavUrl(), m_ColLinkWebDavURL);
		else
			row->RemoveField(m_ColLinkWebDavURL);

		if (p_BusinessDriveItem.IsDeleted())
			row->SetColor(RGB(255, 0, 0));

		row->RemoveField(m_ColFileToBeUploaded);

#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
		if (!row->HasField(m_ColCheckoutLoaded))
			row->AddFieldForCheckBox(false, m_ColCheckoutLoaded);
#else
		if (p_BusinessDriveItem.GetCheckoutStatus())
			fillCheckoutStatus(row, *p_BusinessDriveItem.GetCheckoutStatus(), p_Updater);
		else
			clearCheckoutStatus(row);
#endif
	}
 
	return row;
}

void GridDriveItems::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart /*= 0*/)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_CHANGESEARCHCRITERIA);
		pPopup->ItemInsert(); // Sep

		pPopup->ItemInsert(ID_DRIVEITEMSGRID_DELETE);
        pPopup->ItemInsert(ID_DRIVEITEMSGRID_RENAME);
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_CREATEFOLDER);
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_IMPORTFOLDER);
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_ADDFILE);
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_SHOWFOLDERINFLAT);
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_DOWNLOAD);
		pPopup->ItemInsert(); // Sep
		if (nullptr != m_ColCheckoutUser)
		{
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
			pPopup->ItemInsert(ID_DRIVEITEMSGRID_LOADCHECKOUTSTATUS);
#endif
			pPopup->ItemInsert(ID_DRIVEITEMSGRID_CHECKIN);
			pPopup->ItemInsert(ID_DRIVEITEMSGRID_CHECKOUT);
		}
		pPopup->ItemInsert(); // Sep
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_VIEWPERMISSIONS);
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODE);
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODEANDFLAT);
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_TOGGLEOWNERPERMISSIONS);
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_DELETEPERMISSIONS);
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD);
		pPopup->ItemInsert(ID_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE);
		pPopup->ItemInsert(); // Sep
	}

	GridDriveItemsBaseClass::OnCustomPopupMenu(pPopup, nStart);
}

void GridDriveItems::InitializeCommands()
{
    GridDriveItemsBaseClass::InitializeCommands();

	const auto tableResult = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific);
    const auto& acceleratorTexts = tableResult.second;
	ASSERT(tableResult.first && acceleratorTexts.size() > 0);

    const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

    {
        CExtCmdItem _cmd;

        // View permissions
        _cmd.m_nCmdID		= ID_DRIVEITEMSGRID_VIEWPERMISSIONS;
        _cmd.m_sMenuText	= YtriaTranslate::Do(GridDriveItems_InitializeCommands_1, _YLOC("Load Permission Info")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_6, _YLOC("Load Permission Info")).c_str();
        _cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
        HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_VIEWPERMISSIONS_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_VIEWPERMISSIONS, hIcon, false);
        setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_11, _YLOC("Load Permission Info")).c_str(),
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_12, _YLOC("Display a list of all permissions for the selected items.\nFor a more granular permission view, explode multivalue cells.")).c_str(),
			_cmd.m_sAccelText);

		_cmd.m_nCmdID		= ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODE;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridDriveItems_InitializeCommands_21, _YLOC("Load Permission Info + Explode any multivalue cells")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_22, _YLOC("Load Permission Info + Explode any multivalue cells")).c_str();
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODE, hIcon, false);

		_cmd.m_nCmdID		= ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODEANDFLAT;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridDriveItems_InitializeCommands_23, _YLOC("Load Permission Info + Set to Flat View + Explode any multivalue cells")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_24, _YLOC("Load Permission Info + Set to Flat View + Explode any multivalue cells")).c_str();
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODEANDFLAT_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODEANDFLAT, hIcon, false);

        // Delete permissions
        _cmd.m_nCmdID		= ID_DRIVEITEMSGRID_DELETEPERMISSIONS;
        _cmd.m_sMenuText	= YtriaTranslate::Do(GridDriveItems_InitializeCommands_2, _YLOC("Delete Permissions")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_7, _YLOC("Delete")).c_str();
        _cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
        hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_DELETEPERMISSIONS_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_DELETEPERMISSIONS, hIcon, false);
        setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_13, _YLOC("Delete Permissions")).c_str(),
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_14, _YLOC("Delete the selected file permissions.\nIf an unexploded grid entry is selected and deleted, all file permissions will be removed.")).c_str(),
			_cmd.m_sAccelText);

        // Set permission role to "Read"
        _cmd.m_nCmdID		= ID_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD;
        _cmd.m_sMenuText	= YtriaTranslate::Do(GridDriveItems_InitializeCommands_3, _YLOC("Set Permissions Roles to 'Read'")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_8, _YLOC("Set to 'Read'")).c_str();
        _cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
        hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_DELETEPERMISSIONS_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD, hIcon, false);
        setRibbonTooltip(_cmd.m_nCmdID,
            YtriaTranslate::Do(GridDriveItems_InitializeCommands_15, _YLOC("Set Permissions Roles to 'Read'")).c_str(),
            YtriaTranslate::Do(GridDriveItems_InitializeCommands_16, _YLOC("Sets the role of the selected permissions to 'Read'")).c_str(),
            _cmd.m_sAccelText);

        // Set permission role to "Write"
        _cmd.m_nCmdID		= ID_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE;
        _cmd.m_sMenuText	= YtriaTranslate::Do(GridDriveItems_InitializeCommands_4, _YLOC("Set Permissions Roles to 'Write'")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_9, _YLOC("Set to 'Write'")).c_str();
        _cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
        hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_DELETEPERMISSIONS_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE, hIcon, false);
        setRibbonTooltip(_cmd.m_nCmdID,
            YtriaTranslate::Do(GridDriveItems_InitializeCommands_17, _YLOC("Set Permissions Roles to 'Write'")).c_str(),
            YtriaTranslate::Do(GridDriveItems_InitializeCommands_18, _YLOC("Sets the role of the selected permissions to 'Write'")).c_str(),
            _cmd.m_sAccelText);

        // Download
        _cmd.m_nCmdID		= ID_DRIVEITEMSGRID_DOWNLOAD;
        _cmd.m_sMenuText	= YtriaTranslate::Do(GridDriveItems_InitializeCommands_5, _YLOC("Download Files...")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_10, _YLOC("Download Files...")).c_str();
        _cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
        hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_DOWNLOAD_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_DOWNLOAD, hIcon, false);
        setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_19, _YLOC("Download Selected Files from the Document Library")).c_str(),
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_20, _YLOC("Download the selected files from the document library and save them to a destination folder of your choice on your computer.")).c_str(),
			_cmd.m_sAccelText);

		// Delete
		_cmd.m_nCmdID		= ID_DRIVEITEMSGRID_DELETE;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridDriveItems_InitializeCommands_25, _YLOC("Delete")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_26, _YLOC("Delete")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DELETE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_DELETE, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_27, _YLOC("Delete Entries")).c_str(),
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_28, _YLOC("Delete selected entries from the server.")).c_str(),
			_cmd.m_sAccelText);

        // Rename
        _cmd.m_nCmdID		= ID_DRIVEITEMSGRID_RENAME;
        _cmd.m_sMenuText	= YtriaTranslate::Do(GridDriveItems_InitializeCommands_29, _YLOC("Rename")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_30, _YLOC("Rename")).c_str();
        _cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
        hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_RENAME_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_RENAME, hIcon, false);
        setRibbonTooltip(_cmd.m_nCmdID,
            YtriaTranslate::Do(GridDriveItems_InitializeCommands_31, _YLOC("Rename selected entry")).c_str(),
            YtriaTranslate::Do(GridDriveItems_InitializeCommands_32, _YLOC("Edit the name of the file in the document library.")).c_str(),
            _cmd.m_sAccelText);

		// Create Folder
		_cmd.m_nCmdID		= ID_DRIVEITEMSGRID_CREATEFOLDER;
		_cmd.m_sMenuText	= _T("Create Folder");
		_cmd.m_sToolbarText = _T("Create Folder");
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_CREATEFOLDER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_CREATEFOLDER, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Create a New Folder"),
			_T("Create a new folder in the document library."),
			_cmd.m_sAccelText);

		// Add File
		_cmd.m_nCmdID		= ID_DRIVEITEMSGRID_ADDFILE;
		_cmd.m_sMenuText	= _T("Add File");
		_cmd.m_sToolbarText = _T("Add File");
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_ADDFILE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_ADDFILE, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Add a New File"),
			_T("Add a new file to the selected document library."),
			_cmd.m_sAccelText);

		// Add Folder
		_cmd.m_nCmdID		= ID_DRIVEITEMSGRID_IMPORTFOLDER;
		_cmd.m_sMenuText	= _T("Upload Folder");
		_cmd.m_sToolbarText = _T("Upload Folder");
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		// FIXME: Change icon
		hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_IMPORTFOLDER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_IMPORTFOLDER, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Upload a Folder from your Computer"),
			_T("Upload a folder from your computer to the document library.\nThere are 3 options:\n- Replace target folder's contents with source's contents\n- Import both the source folder and its contents\n- Import source folder's contents only"),
			_cmd.m_sAccelText);
    }

	{
        CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_DRIVEITEMSGRID_SHOWFOLDERINFLAT;
        _cmd.m_sMenuText	= _T("Show Folders and Notebooks");
        _cmd.m_sToolbarText = _T("Show Folders");
        _cmd.m_sAccelText	= _YTEXT("");
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

        HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_SHOWFOLDERINFLAT_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_SHOWFOLDERINFLAT, hIcon, false);

        setRibbonTooltip(_cmd.m_nCmdID,
			_T("Show Folders and Notebooks"),
			_T("Toggle the display of folders and notebooks (hidden by default).\nOnly available in flat view (Ctrl+F11)."),
            _cmd.m_sAccelText);
    }

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_DRIVEITEMSGRID_TOGGLEOWNERPERMISSIONS;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridDriveItems_InitializeCommands_33, _YLOC("Show Owner Permissions")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridDriveItems_InitializeCommands_34, _YLOC("Show Owner")).c_str();
		_cmd.m_sAccelText	= _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_TOGGLEOWNERPERMISSIONS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_TOGGLEOWNERPERMISSIONS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_35, _YLOC("Show Owner Permissions")).c_str(),
			YtriaTranslate::Do(GridDriveItems_InitializeCommands_36, _YLOC("Include owner permissions in the grid.\nBy default, owner permissions are not listed.\nTo go back to default, use the Refresh All button.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_DRIVEITEMSGRID_CHECKIN;
		_cmd.m_sMenuText = _T("Check-in");
		_cmd.m_sToolbarText = _T("Check-in");
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_CHECKIN_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_CHECKIN, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Check-in Selected Files"),
			_T("Check-in files by discarding the check-out. The files will become available for co-authoring."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_DRIVEITEMSGRID_CHECKOUT;
		_cmd.m_sMenuText = _T("Check-out");
		_cmd.m_sToolbarText = _T("Check-out");
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_CHECKOUT_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_CHECKOUT, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Check-out Selected Files"),
			_T("Check-out your files to ensure that no one else can edit them."),
			_cmd.m_sAccelText);
	}

#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_DRIVEITEMSGRID_LOADCHECKOUTSTATUS;
		_cmd.m_sMenuText = _T("Checkout Info");
		_cmd.m_sToolbarText = _T("Checkout Info");
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_DRIVEITEMSGRID_LOADCHECKOUTSTATUS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_LOADCHECKOUTSTATUS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Load Checkout and Retention Info"),
			_T("This option shows which files are checked out and by whom, and if any retention labels are applied."),
			_cmd.m_sAccelText);
	}
#endif

	if (CRMpipe().IsFeatureSandboxed())
	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_DRIVEITEMSGRID_CHANGESEARCHCRITERIA;
		_cmd.m_sMenuText = _T("Change Options");
		_cmd.m_sToolbarText = _T("Change Options");
		_cmd.m_sTipTool = _T("Adjust search criteria.");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		// FIXME: specific image
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANGEMODULEOPTIONS_16x16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_DRIVEITEMSGRID_CHANGESEARCHCRITERIA, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Change Options"),
			_T("Adjust search criteria."),
			_cmd.m_sAccelText);
	}
}

bool GridDriveItems::HasModificationsPending(bool inSelectionOnly /*= false*/)
{
	if (inSelectionOnly)
		return HasModificationsPendingInParentOfSelected();
	return O365Grid::HasModificationsPending(false);
}

void GridDriveItems::OnViewPermissions()
{
	if (IsFrameConnected())
	{
		std::vector<GridBackendRow*> rows;
		GetSelectedNonGroupRows(rows);
		requestViewPermissions(rows, Command::ModuleTask::ListPermissions);
	}
}

void GridDriveItems::OnViewPermissionsExplode()
{
	if (IsFrameConnected())
	{
		std::vector<GridBackendRow*> rows;
		GetSelectedNonGroupRows(rows);
		requestViewPermissions(rows, Command::ModuleTask::ListPermissionsExplode);
	}
}

void GridDriveItems::OnViewPermissionsExplodeFlat()
{
	if (IsFrameConnected())
	{
		std::vector<GridBackendRow*> rows;
		GetSelectedNonGroupRows(rows);
		requestViewPermissions(rows, Command::ModuleTask::ListPermissionsExplodeFlat);
	}
}

void GridDriveItems::OnDeletePermissions()
{
	const auto deletePermission = [this](GridBackendRow* p_Row, const PooledString& p_PermId)
	{
		DriveItemPermissionInfo permissionInfo(*this, p_PermId, p_Row);

		auto deletion = std::make_unique<DriveItemPermissionDeletion>(*this, permissionInfo);
		deletion->ShowAppliedLocally(p_Row);
		if (GetModifications().Add(std::move(deletion)))
		{
			// Successfully added deletion? Remove error flag in case we just overwrote an old error with the new deletion
			p_Row->SetEditError(false);
		}
	};

	const auto canDeletePermission = [this](GridBackendRow* p_Row, const size_t p_PermIndex)
	{
		if (authorizedByRBAC(p_Row, ID_DRIVEITEMSGRID_DELETEPERMISSIONS))
		{
			const auto& roleField = p_Row->GetField(m_ColRole);
			auto* roles = roleField.GetValuesMulti<PooledString>();
			if (nullptr != roles)
				return PooledString(DriveItemsUtil::ROLE_VALUE_OWNER) != roles->operator[](p_PermIndex);
			ASSERT(p_PermIndex == 0);
			return PooledString(DriveItemsUtil::ROLE_VALUE_OWNER) != roleField.GetValueStr();
		}

		return false;
	};

	std::map<GridBackendRow*, bool> alreadyCheckedAncestors;
	boost::YOpt<bool> shouldRevertExistingModifications;
	const auto checkExistingAncestorMod = [this, &alreadyCheckedAncestors, &shouldRevertExistingModifications](GridBackendRow* p_Row)
	{
		if (nullptr != p_Row && alreadyCheckedAncestors.end() == alreadyCheckedAncestors.find(p_Row))
		{
			bool proceedWithDeletion = true;
			row_pk_t pk = GetRowPKNoDynamicColumns(*p_Row);

			auto mod = GetModifications().GetRowModification<RowModification>(pk);

			if ((nullptr != mod) && (nullptr == dynamic_cast<FakeModification*>(mod)))
			{
				if (!shouldRevertExistingModifications)
				{
					YCodeJockMessageBox errorMessage(this,
						DlgMessageBox::eIcon_Question,
						YtriaTranslate::Do(GridDriveItems_OnDeletePermissions_1, _YLOC("Delete Permissions")).c_str(),
						YtriaTranslate::Do(GridDriveItems_OnDeletePermissions_4, _YLOC("Some files are flagged for deletion. Do you want to restore them and delete permission instead?")).c_str(),
						_YTEXT(""),
						{ { IDOK, YtriaTranslate::Do(GridDriveItems_OnDeletePermissions_5, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_OnDeletePermissions_7, _YLOC("No")).c_str() } });

					shouldRevertExistingModifications = errorMessage.DoModal() == IDOK;
				}

				ASSERT(shouldRevertExistingModifications);
				if (shouldRevertExistingModifications && *shouldRevertExistingModifications)
				{
					GetModifications().RevertRowModifications(pk);
					proceedWithDeletion = true;
				}
				else
				{
					proceedWithDeletion = false;
				}
			}

			alreadyCheckedAncestors[p_Row] = proceedWithDeletion;
		}

		return alreadyCheckedAncestors[p_Row];
	};

	boost::YOpt<bool> shouldRevertExistingSubModifications;
	const auto checkExistingSubItemMod = [this, &shouldRevertExistingSubModifications](GridBackendRow* p_Row, const PooledString& p_PermId)
	{
		bool proceedWithDeletion = true;

		DriveItemPermissionInfo permissionInfo(*this, p_PermId, p_Row);
		SubItemKey key(permissionInfo.m_OriginId, permissionInfo.m_DriveId, permissionInfo.m_DriveItemId, permissionInfo.m_PermissionId);
		if (GetModifications().HasSubItemUpdate(key, true))
		{
			if (!shouldRevertExistingSubModifications)
			{
				YCodeJockMessageBox errorMessage(this,
					DlgMessageBox::eIcon_Question,
					YtriaTranslate::Do(GridDriveItems_OnDeletePermissions_1, _YLOC("Delete Permissions")).c_str(),
					YtriaTranslate::Do(GridDriveItems_OnDeletePermissions_3, _YLOC("Some permissions are flagged for update. Do you want to delete them instead?")).c_str(),
					_YTEXT(""),
					{ { IDOK, YtriaTranslate::Do(GridDriveItems_OnDeletePermissions_6, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_OnDeletePermissions_8, _YLOC("No")).c_str() } });

				shouldRevertExistingSubModifications = errorMessage.DoModal() == IDOK;
			}

			ASSERT(shouldRevertExistingSubModifications);
			if (shouldRevertExistingSubModifications && *shouldRevertExistingSubModifications)
			{
				GetModifications().RevertSubItemUpdate(key);
				proceedWithDeletion = true;
			}
			else
			{
				proceedWithDeletion = false;
			}
		}

		return proceedWithDeletion;
	};

	std::vector<GridBackendRow*> rows;
	GetSelectedNonGroupRows(rows);
	TemporarilyImplodeOutOfSisterhoodColumns(*this, m_ColPermissionId, rows, [=](const bool p_HasRowsToReExplode)
	{
		for (auto row : GetSelectedRows())
		{
			if ((GridUtil::IsBusinessDriveItem(row) || GridUtil::IsBusinessDriveItemFolder(row) || GridUtil::IsBusinessDriveItemNotebook(row))
				&& !row->HasFlag(GridBackendUtil::ROWFLAGS::PARENTDELETED))
			{
				auto& field = row->GetField(m_ColPermissionId);
				if (field.HasValue())
				{
					if (GetMultivalueManager().IsRowExploded(row, m_ColPermissionId))
					{
						ASSERT(row->IsExplosionFragment());
						ASSERT(!field.IsMulti());
						if (field.HasValue())
						{
							auto topRow = GetMultivalueManager().GetTopSourceRow(row);
							if (canDeletePermission(row, 0) && checkExistingAncestorMod(topRow) && checkExistingSubItemMod(row, field.GetValueStr()))
								deletePermission(row, field.GetValueStr());
						}
					}
					else
					{
						if (field.IsMulti())
						{
							std::vector<PooledString>* permIds = row->GetField(m_ColPermissionId).GetValuesMulti<PooledString>();
							ASSERT(nullptr != permIds);
							if (nullptr != permIds)
							{

								size_t permIndex = 0;
								for (const auto& permId : *permIds)
								{
									if (canDeletePermission(row, permIndex) && checkExistingAncestorMod(row) && checkExistingSubItemMod(row, permId))
										deletePermission(row, permId);
									++permIndex;
								}
							}
						}
						else
						{
							if (canDeletePermission(row, 0) && checkExistingAncestorMod(row) && checkExistingSubItemMod(row, field.GetValueStr()))
								deletePermission(row, field.GetValueStr());
						}
					}

					GetModifications().ShowSubItemModificationAppliedLocally(row);
				}
			}
		}

		if (!p_HasRowsToReExplode)
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	})();
}

void GridDriveItems::OnSetPermissionRoleToRead()
{
	SetPermissionRole(DriveItemsUtil::ROLE_VALUE_READ, ID_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD);
}

void GridDriveItems::SetPermissionRole(const wstring& p_RoleValue, const int p_CommandID)
{
	const auto updatePermission = [this, p_RoleValue](GridBackendRow* p_Row, const PooledString& p_PermId, const size_t p_PermIndex)
	{
		auto& roleField = p_Row->GetField(m_ColRole);
		auto* roles = roleField.GetValuesMulti<PooledString>();

		SubItemFieldValue oldValue;
		SubItemFieldValue newValue;
		if (nullptr != roles)
		{
			ASSERT(roles->size() > p_PermIndex);
			if (roles->size() <= p_PermIndex)
				return;

			oldValue = SubItemFieldValue(roleField, p_PermIndex);
			(*roles)[p_PermIndex] = PooledString(p_RoleValue);
			newValue = SubItemFieldValue(roleField, p_PermIndex);
		}
		else
		{
			ASSERT(0 == p_PermIndex);
			oldValue = SubItemFieldValue(roleField);
			roleField.SetValue(PooledString(p_RoleValue));
			newValue = SubItemFieldValue(roleField);
		}

		DriveItemPermissionInfo permissionInfo(*this, p_PermId, p_Row);

		auto fieldUpdate = std::make_unique<DriveItemPermissionFieldUpdate>(oldValue,
			newValue,
			m_ColRole->GetID(),
			*this,
			permissionInfo);
		fieldUpdate->ShowAppliedLocally(p_Row);
		GetModifications().Add(std::move(fieldUpdate), p_Row);
	};

	const auto canUpdatePermission = [this, &p_RoleValue, p_CommandID](GridBackendRow* p_Row, const size_t p_PermIndex)
	{
		if (authorizedByRBAC(p_Row, p_CommandID))
		{
			bool roleOk = false;
			bool scopeOK = false;
			const auto& scopeField = p_Row->GetField(m_ColLinkScope);
			if(!scopeField.HasValue())
			{
				scopeOK = true;

				const auto& roleField = p_Row->GetField(m_ColRole);
				auto* roles = roleField.GetValuesMulti<PooledString>();
				if(nullptr != roles)
					roleOk = nullptr != roles && roles->size() > p_PermIndex && (*roles)[p_PermIndex] != p_RoleValue && (*roles)[p_PermIndex] != DriveItemsUtil::ROLE_VALUE_OWNER;
				else
				{
					ASSERT(0 == p_PermIndex);
					roleOk = 0 == p_PermIndex && roleField.GetValueStr() != p_RoleValue && roleField.GetValueStr() != DriveItemsUtil::ROLE_VALUE_OWNER;
				}
			}

			return roleOk && scopeOK;
		}

		return false;
	};

	std::map<GridBackendRow*, bool> alreadyCheckedAncestors;
	boost::YOpt<bool> shouldRevertExistingModifications;
	const auto checkExistingAncestorMod = [this, &alreadyCheckedAncestors, &shouldRevertExistingModifications](GridBackendRow* p_Row)
	{
		if (nullptr != p_Row && alreadyCheckedAncestors.end() == alreadyCheckedAncestors.find(p_Row))
		{
			bool proceedWithDeletion = true;
			row_pk_t pk = GetRowPKNoDynamicColumns(*p_Row);

			auto mod = GetModifications().GetRowModification<RowModification>(pk);
            RowFieldUpdates<Scope::O365>* rowFieldUpdates = nullptr;
            if (nullptr == mod)
            {
                rowFieldUpdates = GetModifications().GetRowFieldModification(pk);
                if (nullptr != rowFieldUpdates)
                {
                    if (rowFieldUpdates->GetState() != Modification::State::AppliedLocally)
                        rowFieldUpdates = nullptr;
                }
            }

			if (nullptr != mod || nullptr != rowFieldUpdates)
			{
				if (!shouldRevertExistingModifications)
				{
					YCodeJockMessageBox errorMessage(this,
						DlgMessageBox::eIcon_Question,
						YtriaTranslate::Do(GridDriveItems_SetPermissionRole_3, _YLOC("Update Permissions")).c_str(),
						YtriaTranslate::Do(GridDriveItems_SetPermissionRole_4, _YLOC("Some files are flagged for modification. Do you want to discard those and update permissions instead?")).c_str(),
						_YTEXT(""),
						{ { IDOK, YtriaTranslate::Do(GridDriveItems_SetPermissionRole_5, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_SetPermissionRole_7, _YLOC("No")).c_str() } });

					shouldRevertExistingModifications = errorMessage.DoModal() == IDOK;
				}

				ASSERT(shouldRevertExistingModifications);
				if (shouldRevertExistingModifications && *shouldRevertExistingModifications)
				{
					GetModifications().RevertRowModifications(pk);
					proceedWithDeletion = true;
				}
				else
				{
					proceedWithDeletion = false;
				}
			}

			alreadyCheckedAncestors[p_Row] = proceedWithDeletion;
		}

		return alreadyCheckedAncestors[p_Row];
	};

	boost::YOpt<bool> shouldRevertExistingSubModifications;
	const auto checkExistingSubItemMod = [this, &shouldRevertExistingSubModifications](GridBackendRow* p_Row, const PooledString& p_PermId)
	{
		bool proceedWithEdition = true;

		DriveItemPermissionInfo permissionInfo(*this, p_PermId, p_Row);
		SubItemKey key(permissionInfo.m_OriginId, permissionInfo.m_DriveId, permissionInfo.m_DriveItemId, permissionInfo.m_PermissionId);
		if (GetModifications().HasSubItemDeletion(key, true))
		{
			if (!shouldRevertExistingSubModifications)
			{
				YCodeJockMessageBox errorMessage(this->GetParentFrame(),
					DlgMessageBox::eIcon_Question,
					YtriaTranslate::Do(GridDriveItems_SetPermissionRole_1, _YLOC("Update Permissions")).c_str(),
					YtriaTranslate::Do(GridDriveItems_SetPermissionRole_2, _YLOC("Some permissions are flagged for deletion. Do you want to update them instead?")).c_str(),
					_YTEXT(""),
					{ { IDOK, YtriaTranslate::Do(GridDriveItems_SetPermissionRole_6, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_SetPermissionRole_8, _YLOC("No")).c_str() } });

				shouldRevertExistingSubModifications = errorMessage.DoModal() == IDOK;
			}

			ASSERT(shouldRevertExistingSubModifications);
			if (shouldRevertExistingSubModifications && *shouldRevertExistingSubModifications)
			{
				GetModifications().RevertSubItemDeletion(key);
				proceedWithEdition = true;
			}
			else
			{
				proceedWithEdition = false;
			}
		}

		return proceedWithEdition;
	};

	std::vector<GridBackendRow*> rows;
	GetSelectedNonGroupRows(rows);
	TemporarilyImplodeOutOfSisterhoodColumns(*this, m_ColPermissionId, rows, [=](const bool p_HasRowsToReExplode)
	{
		for (auto row : GetSelectedRows())
		{
			if (row->IsMoreLoaded()
				&& (GridUtil::IsBusinessDriveItem(row) || GridUtil::IsBusinessDriveItemFolder(row))
				&& !row->HasFlag(GridBackendUtil::ROWFLAGS::PARENTDELETED))
			{
				auto& field = row->GetField(m_ColPermissionId);
				if (field.HasValue())
				{
					if (GetMultivalueManager().IsRowExploded(row, m_ColPermissionId))
					{
						ASSERT(row->IsExplosionFragment());
						ASSERT(!field.IsMulti());
						if (field.HasValue())
						{
							auto topRow = GetMultivalueManager().GetTopSourceRow(row);
							if (canUpdatePermission(row, 0) && checkExistingAncestorMod(topRow) && checkExistingSubItemMod(row, field.GetValueStr()))
								updatePermission(row, field.GetValueStr(), 0);
						}
					}
					else
					{
						if (field.IsMulti())
						{
							std::vector<PooledString>* permIds = row->GetField(m_ColPermissionId).GetValuesMulti<PooledString>();
							ASSERT(nullptr != permIds);
							if (nullptr != permIds)
							{

								size_t permIndex = 0;
								for (const auto& permId : *permIds)
								{
									if (canUpdatePermission(row, permIndex) && checkExistingAncestorMod(row) && checkExistingSubItemMod(row, permId))
										updatePermission(row, permId, permIndex);
									++permIndex;
								}
							}
						}
						else
						{
							if (canUpdatePermission(row, 0) && checkExistingAncestorMod(row) && checkExistingSubItemMod(row, field.GetValueStr()))
								updatePermission(row, field.GetValueStr(), 0);
						}
					}

					GetModifications().ShowSubItemModificationAppliedLocally(row);
				}
			}
		}

		if (!p_HasRowsToReExplode)
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_PAINTNOW);
	})();
}

void GridDriveItems::OnSetPermissionRoleToWrite()
{
	SetPermissionRole(DriveItemsUtil::ROLE_VALUE_WRITE, ID_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE);
}

void GridDriveItems::OnUpdateSetPermissionRoleToRead(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(hasNoTaskRunning() && /*IsFrameConnected() && */EnableSetPermissionRole(DriveItemsUtil::ROLE_VALUE_READ, ID_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD));
    setTextFromProfUISCommand(*pCmdUI);
}

bool GridDriveItems::EnableSetPermissionRole(const wstring& p_PermissionRole, const int p_CommandID)
{
    bool enable = false;
    for (auto row : GetSelectedRows())
    {
		if (row->IsMoreLoaded()
			&& (GridUtil::IsBusinessDriveItem(row) || GridUtil::IsBusinessDriveItemFolder(row) || GridUtil::IsBusinessDriveItemNotebook(row))
			&& !row->HasFlag(GridBackendUtil::ROWFLAGS::PARENTDELETED)
			&& authorizedByRBAC(row, p_CommandID))
        {
            if (!row->IsExplosionFragment() || (row->IsExplosionFragment() && !row->GetField(m_ColInheritedFromId).HasValue()))
            {
				GridBackendField& fieldRole = row->GetField(m_ColRole);
				GridBackendField& fieldScope = row->GetField(m_ColLinkScope);
				if(fieldRole.HasValue() && !fieldScope.HasValue())
				{
					auto roles = fieldRole.GetValuesMulti<PooledString>();
					if(nullptr != roles)
					{
						for(size_t i = 0; i < roles->size(); ++i)
						{
							const auto& role = roles->operator[](i);
							if(role != DriveItemsUtil::ROLE_VALUE_OWNER && role != p_PermissionRole)
							{
								enable = true;
								break;
							}
						}
					}
					else if(fieldRole.GetValueStr() != DriveItemsUtil::ROLE_VALUE_OWNER && fieldRole.GetValueStr() != p_PermissionRole)
						enable = true;

					if(enable)
						break;
				}
            }
        }
    }

    return enable;
}

void GridDriveItems::OnUpdateSetPermissionRoleToWrite(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && /*IsFrameConnected() && */EnableSetPermissionRole(DriveItemsUtil::ROLE_VALUE_WRITE, ID_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE));
    setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnDownload()
{
	if (IsFrameConnected())
	{
		DlgDownloadDriveItems dlg(this);
		if (dlg.DoModal() == IDOK)
		{
			const wstring& destFolder = dlg.GetFolderPath();
			const bool preserveHierarchy = dlg.GetPreserveHierarchy();
			const bool useOwnerName = dlg.GetUseOwnerName();
			const FileExistsAction fileExistsAction = dlg.GetFileExistsAction();
			const bool openAfterDownload = dlg.GetOpenAfterDownload();

			CommandInfo info;

			vector<DriveItemDownloadInfo> downloads;

			auto addToDownloads = [this, &downloads, destFolder, preserveHierarchy, useOwnerName, fileExistsAction, openAfterDownload](GridBackendRow* p_Row)
			{
				if (authorizedByRBAC(p_Row, ID_DRIVEITEMSGRID_DOWNLOAD))
				{
					DriveItemDownloadInfo download;
					download.m_DriveId = p_Row->GetField(m_ColMetaDriveId).GetValueStr();
					download.m_DriveItemId = p_Row->GetField(GetColId()).GetValueStr();
					download.m_Filepath = destFolder;
					download.m_Filename = p_Row->GetField(m_ColName).GetValueStr();
					download.m_DrivePath = p_Row->GetField(m_ColDirectory).GetValueStr();
					download.m_OpenAfterDownload = openAfterDownload;
					download.m_PreserveHierarchy = preserveHierarchy;
					if (useOwnerName)
						download.m_OwnerName = p_Row->GetField(getColMetaDisplayName()).GetValueStr();
					download.m_Row = p_Row;
					download.m_FileExistsAction = fileExistsAction;
					download.m_CreatedDateTime = p_Row->GetField(m_ColCreatedDateTime).GetValueTimeDate();
					download.m_ModifiedDateTime = p_Row->GetField(m_ColLastModifiedDateTime).GetValueTimeDate();
					downloads.emplace_back(download);
				}
			};

			std::set<GridBackendRow*> alreadyAddedWithFolder;
			for (auto row : GetBackend().GetSelectedRowsInOrder())
			{
				if (!row->IsCreated())
				{
					if (alreadyAddedWithFolder.find(row) != std::end(alreadyAddedWithFolder))
						continue;

					const auto& packageField = row->GetField(m_ColPackageType);
					if (GridUtil::isBusinessType<BusinessDriveItem>(row))
					{
						addToDownloads(row);
					}
					else if (GridUtil::IsBusinessDriveItemFolder(row) || GridUtil::IsBusinessDriveItemNotebook(row))
					{
						// If a folder or notebook is selected, download all drive items in it.
						vector<GridBackendRow*> children;
						GetBackend().GetHierarchyChildren(row, children);

						for (auto child : children)
						{
							if (!child->IsCreated())
							{
								alreadyAddedWithFolder.insert(child);
								const auto& packageFieldChild = child->GetField(m_ColPackageType);
								if (GridUtil::isBusinessType<BusinessDriveItem>(child))
									addToDownloads(child);
							}
						}
					}
				}
			}

			info.Data() = downloads;
			ASSERT(nullptr != m_FrameDriveItems);
			if (nullptr != m_FrameDriveItems)
			{
				info.SetFrame(m_FrameDriveItems);
				info.SetRBACPrivilege(m_FrameDriveItems->GetModuleCriteria().m_Privilege);
			}
			CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Drive, Command::ModuleTask::DownloadDriveItems, info, { this, g_ActionNameSelectedDownload, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
	}
}

void GridDriveItems::OnDeleteItem()
{
	TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool p_HasRowsToReExplode)
	{
		ASSERT(!GetSelectedRows().empty());
		if (!GetSelectedRows().empty())
		{
			bool update = false;
			bool promptedForEdit = false;
			bool promptedForFolder = false;
			bool deleteEdited = false;
			bool shouldRevertModifiedChild = false;
			map<GridBackendRow*, row_pk_t> rowsToRevert;
			for (auto selectedRow : GetSelectedRows())
			{
				ASSERT(nullptr != selectedRow);
				if ((GridUtil::IsBusinessDriveItem(selectedRow) || GridUtil::IsBusinessDriveItemFolder(selectedRow) || GridUtil::IsBusinessDriveItemNotebook(selectedRow))
					&& authorizedByRBAC(selectedRow, ID_DRIVEITEMSGRID_DELETE))
				{
					row_pk_t rowPk;
					GetRowPK(selectedRow, rowPk);

					if (!selectedRow->IsParentRowDeleted())
					{
						if (selectedRow->IsCreated() || IsTemporaryCreatedObjectID(selectedRow->GetField(GetColId()).GetValueStr()))
						{
							rowsToRevert[selectedRow] = rowPk;
						}
						else
						{
							if (!selectedRow->IsDeleted())
							{
								ASSERT(nullptr == GetModifications().GetRowModification<DeletedObjectModification>(rowPk));
								bool shouldDelete = true;
								if (selectedRow->IsModified())
								{
									if (!promptedForEdit)
									{
										YCodeJockMessageBox dlg(this,
											DlgMessageBox::eIcon_Question,
											YtriaTranslate::Do(GridDriveItems_OnDeleteItem_1, _YLOC("Delete")).c_str(),
											YtriaTranslate::Do(GridDriveItems_OnDeleteItem_2, _YLOC("The selected rows contain unsaved changes. Do you want to delete them instead?")).c_str(),
											YtriaTranslate::Do(GridDriveItems_OnDeleteItem_3, _YLOC("Pending changes will be lost.")).c_str(),
											{ { IDOK, YtriaTranslate::Do(GridDriveItems_OnDeleteItem_4, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_OnDeleteItem_5, _YLOC("No")).c_str() } });

										deleteEdited = IDOK == dlg.DoModal();
										promptedForEdit = true;
									}

									if (deleteEdited)
									{
										GetModifications().Revert(rowPk, true);
										update = true;
									}
									else
									{
										shouldDelete = false;
									}
								}

								if (shouldDelete && (GridUtil::IsBusinessDriveItemFolder(selectedRow) || GridUtil::IsBusinessDriveItemNotebook(selectedRow)))
								{
									if (selectedRow->HasChildrenRows())
									{
										vector<GridBackendRow*> childRows;
										GetBackend().GetHierarchyChildren(selectedRow, childRows);
										ASSERT(!childRows.empty());
										if (!childRows.empty())
										{
											for (auto childRow : childRows)
											{
												if (childRow->IsModified() || childRow->IsDeleted() || childRow->IsCreated())
												{
													if (!promptedForFolder)
													{
														YCodeJockMessageBox dlg(this,
															DlgMessageBox::eIcon_Question,
															YtriaTranslate::Do(GridDriveItems_OnDeleteItem_6, _YLOC("Delete")).c_str(),
															YtriaTranslate::Do(GridDriveItems_OnDeleteItem_7, _YLOC("Some selected folders contain unsaved changes. Do you want to delete them anyway?")).c_str(),
															YtriaTranslate::Do(GridDriveItems_OnDeleteItem_8, _YLOC("Pending changes will be lost.")).c_str(),
															{ { IDOK, YtriaTranslate::Do(GridDriveItems_OnDeleteItem_9, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_OnDeleteItem_10, _YLOC("No")).c_str() } });

														shouldRevertModifiedChild = IDOK == dlg.DoModal();
														promptedForFolder = true;
													}

													if (shouldRevertModifiedChild)
													{
														row_pk_t childRowPk;
														GetRowPK(childRow, childRowPk);
														GetModifications().Revert(childRowPk, true);
														update = true;
													}
													else
													{
														shouldDelete = false;
														break;
													}
												}
											}
										}
									}
								}

								if (shouldDelete)
								{
									if (GridUtil::IsBusinessDriveItemFolder(selectedRow) || GridUtil::IsBusinessDriveItemNotebook(selectedRow))
										GetModifications().Add(std::make_unique<DeletedFolderModification>(*this, rowPk));
									else
										GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, rowPk));
									update = true;
								}
							}
						}
					}
				}
			}

			for (auto item : rowsToRevert)
			{
				if (GetModifications().Revert(item.second, false))
				{
					update = true;
				}
				else
				{
					ASSERT(false);
				}
			}

			if (update && !p_HasRowsToReExplode)
				UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		}
	})();
}

void GridDriveItems::OnRenameItem()
{
	TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this](const bool p_HasRowsToReExplode)
	{
		ASSERT(GetSelectedRows().size() == 1);
		if (GetSelectedRows().size() == 1)
		{
			auto row = *GetSelectedRows().begin();
			if (authorizedByRBAC(row, ID_DRIVEITEMSGRID_RENAME))
			{
				const auto driveItem = getBusinessObject(row);
				DlgBusinessDriveItemRename dlg(driveItem, DlgFormsHTML::Action::EDIT, this);

				vector<GridBackendField> rowPK;
				GetRowPK(row, rowPK);

				bool proceed = true;
				if (row->IsDeleted())
				{
					YCodeJockMessageBox dlgPrompt(this,
						DlgMessageBox::eIcon_Question,
						YtriaTranslate::Do(GridDriveItems_OnRenameItem_1, _YLOC("Rename")).c_str(),
						YtriaTranslate::Do(GridDriveItems_OnRenameItem_2, _YLOC("The selected row contains unsaved deletion. Do you want to rename the file instead?")).c_str(),
						YtriaTranslate::Do(GridDriveItems_OnRenameItem_3, _YLOC("Pending changes will be lost")).c_str(),
						{ { IDOK, YtriaTranslate::Do(GridDriveItems_OnRenameItem_4, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_OnRenameItem_5, _YLOC("No")).c_str() } });

					proceed = dlgPrompt.DoModal() == IDOK;
					if (proceed)
					{
						auto* mod = GetModifications().GetRowModification<DeletedObjectModification>(rowPK);
						ASSERT(nullptr != mod);
						if (nullptr != mod)
							GetModifications().Revert(rowPK, false);
					}
				}
				else // Has sub item mods
				{
					bool hasSubItemMods = false;
					for (const auto& subDel : GetModifications().GetSubItemDeletions())
					{
						if (subDel->IsCorrespondingRow(row))
						{
							hasSubItemMods = true;
							break;
						}
					}

					if (!hasSubItemMods)
					{
						for (const auto& subUpdate : GetModifications().GetSubItemUpdates())
						{
							if (subUpdate->IsCorrespondingRow(row))
							{
								hasSubItemMods = true;
								break;
							}
						}
					}

					if (hasSubItemMods)
					{
						YCodeJockMessageBox dlgPrompt(this,
							DlgMessageBox::eIcon_Question,
							YtriaTranslate::Do(GridDriveItems_OnRenameItem_1, _YLOC("Rename")).c_str(),
							YtriaTranslate::Do(GridDriveItems_OnRenameItem_7, _YLOC("The selected row contains unsaved changes. Do you want to rename the file?")).c_str(),
							YtriaTranslate::Do(GridDriveItems_OnRenameItem_3, _YLOC("Pending changes will be lost")).c_str(),
							{ { IDOK, YtriaTranslate::Do(GridDriveItems_OnRenameItem_4, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_OnRenameItem_5, _YLOC("No")).c_str() } });
						proceed = dlgPrompt.DoModal() == IDOK;

						if (proceed)
						{
							GetModifications().Revert(rowPK, true);
							UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
						}
					}


					if (proceed && dlg.DoModal() == IDOK)
					{
						const bool hadField = row->HasField(m_ColName->GetID());
						const GridBackendField oldField = row->GetField(m_ColName->GetID());
						row->AddField(dlg.GetNewName(), m_ColName);
						const GridBackendField newField = row->GetField(m_ColName->GetID());

						// If the old value is the same as the new value, the new field will not be set as modified.
						// This will trig the ASSERT in GridFrameBase::hasModificationsPending.
						if (newField.IsModified())
						{
							// Are we editing a created object?
							auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPK);
							if (nullptr != mod
								&& !Modification::IsSentAndReceived()(mod)) // Bug #200130.RO.00B442: Renaming a created folder will create another folder 
							{
								CreatedObjectModification newMod(*this, rowPK);
								newMod.DontRevertOnDestruction();
								*mod = newMod;
							}
							else
							{
								auto update = std::make_unique<FieldUpdateO365>(
									*this,
									rowPK,
									m_ColName->GetID(),
									hadField ? oldField : GridBackendField::g_GenericConstField,
									newField);
								GetModifications().Add(std::move(update));
							}
							UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
						}
					}
				}
			}
		}
	})();
}

void GridDriveItems::OnToggleOwnerPermissions()
{
	if (!m_ShowOwnerPermissions)
	{
		bool proceed = true;
		if (!GetModifications().GetSubItemDeletions().empty() || !GetModifications().GetSubItemUpdates().empty())
		{
			YCodeJockMessageBox dlg(this,
				DlgMessageBox::eIcon_ExclamationWarning,
				YtriaTranslate::Do(GridDriveItems_OnToggleOwnerPermissions_1, _YLOC("Show Owner Permissions")).c_str(),
				YtriaTranslate::Do(GridDriveItems_OnToggleOwnerPermissions_2, _YLOC("Some permissions are marked for modification. Those unsaved modifications will be undone.")).c_str(),
				YtriaTranslate::Do(GridDriveItems_OnToggleOwnerPermissions_3, _YLOC("If you want to keep them, you can cancel and save them before showing owner permissions.")).c_str(),
				{ { IDOK, YtriaTranslate::Do(GridDriveItems_OnToggleOwnerPermissions_4, _YLOC("OK")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_OnToggleOwnerPermissions_5, _YLOC("Cancel")).c_str() } });

			proceed = IDOK == dlg.DoModal();
			if (proceed)
				GetModifications().RevertAllSubItemModifications();
		}

		if (proceed)
		{
			TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::ALL_ROWS, [this](const bool p_HasRowsToReExplode)
			{
				SetShowOwnerPermissions(true);
				ModuleUtil::ClearModifiedRowsStatus(*this);
				ViewPermissions(m_AllPermissions, false, false, false, !p_HasRowsToReExplode);
			})();
		}
	}
}

void GridDriveItems::OnCreateFolder()
{
	const bool ownerChannel = m_FrameDriveItems->GetOrigin() == Origin::Channel;
	const bool ownerGroup = m_FrameDriveItems->GetOrigin() == Origin::Group;
	const bool ownerSite = m_FrameDriveItems->GetOrigin() == Origin::Site;
	const bool ownerUser = m_FrameDriveItems->GetOrigin() == Origin::User || m_FrameDriveItems->GetOrigin() == Origin::DeletedUser;

	ASSERT(ownerChannel || ownerGroup || ownerSite || ownerUser);

	std::function<bool(GridBackendRow*)> isOwnerRow;
	if (ownerUser)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessUser(p_Row); };
	else if (ownerGroup)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessGroup(p_Row); };
	else if (ownerSite)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessSite(p_Row); };
	else if (ownerChannel)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessChannel(p_Row); };
	else
	{
		isOwnerRow = [](auto p_Row) { return false; };
		ASSERT(false);
	}

	std::set<GridBackendRow*> parentRows;
	for (auto& row : GetSelectedRows())
	{
		if (authorizedByRBAC(row, ID_DRIVEITEMSGRID_CREATEFOLDER))
		{
			if (GridUtil::IsBusinessDriveItemFolder(row) || GridUtil::IsBusinessDriveItemNotebook(row))
				parentRows.insert(row);
			else if (isOwnerRow(row))
				parentRows.insert(row);
			else
				parentRows.insert(row->GetParentRow());
		}
	}

	DlgCreateFolderHTML dlg(this);
	if (IDOK == dlg.DoModal())
	{
		OnUnSelectAll();

		const auto folderName = dlg.GetNewName();
		vector<GridBackendRow*> newFolderRows;
		{
			GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));

			BusinessDriveItemFolder newFolder;
			bool askOnConflict = true;
			bool replaceOnConflict = false;
			for (auto parentRow : parentRows)
			{
				ASSERT(nullptr != parentRow || IsMyData());
				newFolder.SetName(PooledString(folderName.c_str()));

				auto existingRow = GetChildRow(parentRow, *newFolder.GetName(), m_ColName->GetID());
				if (nullptr != existingRow)
				{
					ASSERT(GridUtil::IsBusinessDriveItemFolder(existingRow));
					bool alreadyAskedForThisParent = false;
					int renameIndex = 1;
					do
					{
						existingRow = GetChildRow(parentRow, *newFolder.GetName(), m_ColName->GetID());
						if (nullptr != existingRow)
						{
							newFolder.SetName(PooledString(_YTEXTFORMAT(L"%s (%s)", folderName.c_str(), Str::getStringFromNumber(renameIndex++).c_str())));
							existingRow = GetChildRow(parentRow, *newFolder.GetName(), m_ColName->GetID());
							ASSERT(nullptr == existingRow || GridUtil::IsBusinessDriveItemFolder(existingRow));
						}
					} while (nullptr != existingRow);
				}

				newFolder.SetID(GetNextTemporaryCreatedObjectID());
				GridBackendRow* newRow = nullptr;
				if (nullptr == parentRow)
				{
					ASSERT(m_MyDataMeHandler);
					if (m_MyDataMeHandler)
						newFolder.SetDriveId(m_MyDataMeHandler->GetField(m_ColMetaDriveId).GetValueStr());
				}
				else
				{
					newFolder.SetDriveId(parentRow->GetField(m_ColMetaDriveId).GetValueStr());
				}
				newRow = fillRow(newFolder, parentRow, false, nullptr);
				newRow->SetSelected(true);

				//if (p_ShouldRenameOnConflict)
				//	newRow->SetFlag(g_RenameOnConflictFlag);
				//else
				//	newRow->RemoveFlag(g_RenameOnConflictFlag);

				row_pk_t pk;
				GetRowPK(newRow, pk);
				GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, pk));

				newFolderRows.push_back(newRow);
			}

			for (auto& row : GetSelectedRows())
				SelectRowAndExpandParentGroups(row, false);
		}

		if (!GetSelectedRows().empty())
			ScrollToRow(*GetSelectedRows().begin());
	}
}

void GridDriveItems::OnAddFile()
{
	const bool ownerChannel = m_FrameDriveItems->GetOrigin() == Origin::Channel;
	const bool ownerGroup = m_FrameDriveItems->GetOrigin() == Origin::Group;
	const bool ownerSite = m_FrameDriveItems->GetOrigin() == Origin::Site;
	const bool ownerUser = m_FrameDriveItems->GetOrigin() == Origin::User || m_FrameDriveItems->GetOrigin() == Origin::DeletedUser;

	ASSERT(ownerChannel || ownerGroup || ownerSite || ownerUser);

	std::function<bool(GridBackendRow*)> isOwnerRow;
	if (ownerUser)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessUser(p_Row); };
	else if (ownerGroup)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessGroup(p_Row); };
	else if (ownerSite)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessSite(p_Row); };
	else if (ownerChannel)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessChannel(p_Row); };
	else
	{
		isOwnerRow = [](auto p_Row) { return false; };
		ASSERT(false);
	}

	std::set<GridBackendRow*> parentRows;
	for (auto& row : GetSelectedRows())
	{
		if (authorizedByRBAC(row, ID_DRIVEITEMSGRID_ADDFILE))
		{
			if (GridUtil::IsBusinessDriveItemFolder(row) || GridUtil::IsBusinessDriveItemNotebook(row))
				parentRows.insert(row);
			else if (isOwnerRow(row))
				parentRows.insert(row);
			else
				parentRows.insert(row->GetParentRow());
		}
	}

	wstring filePath;
	YFileDialog dlgFile(TRUE, nullptr, nullptr, 0, nullptr, this, nullptr, nullptr);
	if (IDOK == dlgFile.DoModal())
		filePath = (LPCTSTR)dlgFile.GetPathName();

	if (!filePath.empty())
	{
		OnUnSelectAll();

		std::vector<GridBackendRow*> newFileRows;
		{
			GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
			DriveItemImporterFile importer(*this);
			importer.Run(filePath, parentRows);

			for (auto& row : GetSelectedRows())
				SelectRowAndExpandParentGroups(row, false);
		}

		if (!GetSelectedRows().empty())
			ScrollToRow(*GetSelectedRows().begin());
	}
}

void GridDriveItems::OnImportFolder()
{
	const bool ownerChannel = m_FrameDriveItems->GetOrigin() == Origin::Channel;
	const bool ownerGroup = m_FrameDriveItems->GetOrigin() == Origin::Group;
	const bool ownerSite = m_FrameDriveItems->GetOrigin() == Origin::Site;
	const bool ownerUser = m_FrameDriveItems->GetOrigin() == Origin::User || m_FrameDriveItems->GetOrigin() == Origin::DeletedUser;

	ASSERT(ownerChannel || ownerGroup || ownerSite || ownerUser);

	std::function<bool(GridBackendRow*)> isOwnerRow;
	if (ownerUser)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessUser(p_Row); };
	else if (ownerGroup)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessGroup(p_Row); };
	else if (ownerSite)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessSite(p_Row); };
	else if (ownerChannel)
		isOwnerRow = [](auto p_Row) { return GridUtil::IsBusinessChannel(p_Row); };
	else
	{
		isOwnerRow = [](auto p_Row) { return false; };
		ASSERT(false);
	}

	std::set<GridBackendRow*> parentRows;
	for (auto& row : GetSelectedRows())
	{
		if (authorizedByRBAC(row, ID_DRIVEITEMSGRID_CREATEFOLDER) && authorizedByRBAC(row, ID_DRIVEITEMSGRID_ADDFILE))
		{
			if (GridUtil::IsBusinessDriveItemFolder(row) || GridUtil::IsBusinessDriveItemNotebook(row))
				parentRows.insert(row);
			else if (isOwnerRow(row))
				parentRows.insert(row);
			else
				parentRows.insert(row->GetParentRow());
		}
	}

	wstring folderPath;
	YFileDialog dlgFile(nullptr, 0, this, nullptr, nullptr);
	if (IDOK == dlgFile.DoModal())
		folderPath = (LPCTSTR)dlgFile.GetFolderPath();

	if (!folderPath.empty())
	{
		YCodeJockMessageBox dlg(this,
			DlgMessageBox::eIcon_Question,
			_T("What do you want to do?"),
			_T("REPLACE - Replace target folder's contents with source contents.\nALL - Import both the source folder and its contents.\nCONTENTS ONLY - Import source folder's contents only."),
			_YTEXT(""),
			{ { IDYES, _T("Replace") },{ IDNO, _T("All") },{ IDOK, _T("Contents Only") }, { IDCANCEL, _T("Cancel") } },
			IDOK);

		const auto res = dlg.DoModal();
		if (IDCANCEL != res)
		{
			const auto mode = IDYES == res	? DriveItemImporterFolder::Mode::SYNC
											: IDNO == res	? DriveItemImporterFolder::Mode::IMPORT_FOLDER_AND_CONTENT
															: DriveItemImporterFolder::Mode::IMPORT_CONTENT_ONLY;

			OnUnSelectAll();

			{
				GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));

				DlgLoading::RunModal(_T("Importing Folder"), this, [this, mode, folderPath, &parentRows](std::shared_ptr<DlgLoading> p_Dlg)
					{
						DriveItemImporterFolder importer(*this, mode);
						importer.Run(folderPath, parentRows, p_Dlg);
					});

				/*DriveItemImporterFolder importer(*this, mode);
				importer.Run(folderPath, parentRows);*/

				for (auto& row : GetSelectedRows())
					SelectRowAndExpandParentGroups(row, false);
			}

			if (!GetSelectedRows().empty())
				ScrollToRow(*GetSelectedRows().begin());
		}
	}
}

void GridDriveItems::checkInOut(bool p_CheckIn)
{
	if (nullptr != m_ColCheckoutUser)
	{
		TemporarilyImplodeNonGroupRowsAndExec(*this, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [this, p_CheckIn](const bool p_HasRowsToReExplode)
			{
				const auto permission = getPermission(p_CheckIn ? ID_DRIVEITEMSGRID_CHECKIN : ID_DRIVEITEMSGRID_CHECKOUT);
				std::vector<GridBackendRow*> selectedRows = GridUtil::GetSelectionAuthorizedByRoleDelegation(*this, permission, [this, p_CheckIn](GridBackendRow* p_Row)
					{
						return !p_Row->IsCreated()
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
							&& HasRowCheckoutStatus(p_Row)
#endif
							&& p_CheckIn == (p_Row->HasField(m_ColCheckoutUser) && !(p_Row->GetField(m_ColCheckoutUser).IsModified() && PooledString(p_Row->GetField(m_ColCheckoutUser).GetValueStr()).IsEmpty()));
					});

				bool update = false;
				for (auto row : selectedRows)
				{
					ASSERT(authorizedByRBAC(row, p_CheckIn ? ID_DRIVEITEMSGRID_CHECKIN : ID_DRIVEITEMSGRID_CHECKOUT));
					if (authorizedByRBAC(row, p_CheckIn ? ID_DRIVEITEMSGRID_CHECKIN : ID_DRIVEITEMSGRID_CHECKOUT))
					{
						vector<GridBackendField> rowPK;
						GetRowPK(row, rowPK);

						bool proceed = true;
						if (row->IsDeleted())
						{
							YCodeJockMessageBox dlgPrompt(this,
								DlgMessageBox::eIcon_Question,
								p_CheckIn	? _T("Check-in")
											: _T("Check-out"),
								p_CheckIn	? _T("The selected row contains unsaved deletion. Do you want to continue and check in the file?")
											: _T("The selected row contains unsaved deletion. Do you want to continue and check out the file?"),
								YtriaTranslate::Do(GridDriveItems_OnRenameItem_3, _YLOC("Pending changes will be lost")).c_str(),
								{ { IDOK, YtriaTranslate::Do(GridDriveItems_OnRenameItem_4, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_OnRenameItem_5, _YLOC("No")).c_str() } });

							proceed = dlgPrompt.DoModal() == IDOK;
							if (proceed)
							{
								auto* mod = GetModifications().GetRowModification<DeletedObjectModification>(rowPK);
								ASSERT(nullptr != mod);
								if (nullptr != mod)
									GetModifications().Revert(rowPK, false);
							}
						}
						else
						{
							bool hasSubItemMods = false;
							for (const auto& subDel : GetModifications().GetSubItemDeletions())
							{
								if (subDel->IsCorrespondingRow(row))
								{
									hasSubItemMods = true;
									break;
								}
							}

							if (!hasSubItemMods)
							{
								for (const auto& subUpdate : GetModifications().GetSubItemUpdates())
								{
									if (subUpdate->IsCorrespondingRow(row))
									{
										hasSubItemMods = true;
										break;
									}
								}
							}

							if (hasSubItemMods)
							{
								YCodeJockMessageBox dlgPrompt(this,
									DlgMessageBox::eIcon_Question,
									p_CheckIn	? _T("Check-in")
												: _T("Check-out"),
									p_CheckIn	? _T("The selected row contains unsaved changes. Do you want to check-in the file instead?")
												: _T("The selected row contains unsaved changes. Do you want to check-out the file instead?"),
									YtriaTranslate::Do(GridDriveItems_OnRenameItem_3, _YLOC("Pending changes will be lost")).c_str(),
									{ { IDOK, YtriaTranslate::Do(GridDriveItems_OnRenameItem_4, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_OnRenameItem_5, _YLOC("No")).c_str() } });
								proceed = dlgPrompt.DoModal() == IDOK;

								if (proceed)
								{
									GetModifications().Revert(rowPK, true);
									update = true;
								}
							}

							if (proceed)
							{
								const PooledString newCheckoutUser = p_CheckIn ? PooledString() : m_NameForCheckout;
								bool shouldJustRevert = false;
								auto mod = GetModifications().GetRowFieldModification(rowPK);
								if (nullptr != mod)
								{
									for (auto& fieldMod : mod->GetFieldUpdates())
									{
										if (fieldMod->GetColumnID() == m_ColCheckoutUser->GetID())
										{
											if (fieldMod->GetOldValue() && fieldMod->GetOldValue()->GetValueStr() == newCheckoutUser.c_str()
												|| !fieldMod->GetOldValue() && newCheckoutUser.IsEmpty())
											{
												shouldJustRevert = true;
											}
											break;
										}
									}
								}

								if (shouldJustRevert)
								{
									GetModifications().RevertFieldUpdates(rowPK, { m_ColCheckoutUser->GetID() });
								}
								else
								{
									// m_NameForCheckout
									const bool hadField = row->HasField(m_ColCheckoutUser);
									ASSERT(hadField == p_CheckIn);
									const GridBackendField oldField = row->GetField(m_ColCheckoutUser);
									const GridBackendField newField =
										p_CheckIn	? row->AddField(PooledString(), m_ColCheckoutUser)
													: row->AddField(m_NameForCheckout, m_ColCheckoutUser);

									const GridBackendField oldPubLevelField = nullptr != m_ColPublicationLevel ? row->GetField(m_ColPublicationLevel) : GridBackendField();
									if (nullptr != m_ColPublicationLevel)
									{
										PooledString newValue = p_CheckIn ? _YTEXT("published") : _YTEXT("checkout");
										row->AddField(newValue, m_ColPublicationLevel, m_PublicationLevelIcons[newValue]);
									}

									// If the old value is the same as the new value, the new field will not be set as modified.
									// This will trig the ASSERT in GridFrameBase::hasModificationsPending.
									ASSERT(newField.IsModified());
									if (newField.IsModified())
									{
										// Are we editing a created object?
										/*auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPK);
										if (nullptr != mod)
										{
											CreatedObjectModification newMod(*this, rowPK);
											newMod.DontRevertOnDestruction();
											*mod = newMod;
										}
										else*/
										{

											if (nullptr == m_ColPublicationLevel)
											{
												auto fieldUpdate = std::make_unique<FieldUpdateO365>(
													*this,
													rowPK,
													m_ColCheckoutUser->GetID(),
													hadField ? oldField : GridBackendField::g_GenericConstField,
													newField,
													FieldUpdateFlags::IgnoreRemoteValue);
												GetModifications().Add(std::move(fieldUpdate));
											}
											else
											{
												auto fieldUpdate = std::make_unique<SisterhoodFieldUpdate>(
													*this,
													rowPK,
													m_ColCheckoutUser->GetID(),
													hadField ? oldField : GridBackendField::g_GenericConstField,
													newField,
													std::map<UINT, const GridBackendField*>{ { m_ColPublicationLevel->GetID(), &oldPubLevelField } },
													FieldUpdateFlags::IgnoreRemoteValue);
												GetModifications().Add(std::move(fieldUpdate));
											}
										}
									}
								}
								update = true;
							}
						}
					}
				}

				if (update && !p_HasRowsToReExplode)
					UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
			})();
	}
}

void GridDriveItems::OnCheckIn()
{
	checkInOut(true);
}

void GridDriveItems::OnCheckOut()
{
	checkInOut(false);
}

void GridDriveItems::OnUpdateCheckIn(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning() && nullptr != m_ColCheckoutUser)
	{
		for (auto& row : GetSelectedRows())
		{
			if (!row->IsCreated()
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
				&& HasRowCheckoutStatus(row)
#endif
				&& row->HasField(m_ColCheckoutUser) && !(row->GetField(m_ColCheckoutUser).IsModified() && PooledString(row->GetField(m_ColCheckoutUser).GetValueStr()).IsEmpty())
				&& authorizedByRBAC(row, ID_DRIVEITEMSGRID_CHECKIN))
			{
				enable = TRUE;
				break;
			}
		}
	}

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnUpdateCheckOut(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning() && nullptr != m_ColCheckoutUser)
	{
		for (auto& row : GetSelectedRows())
		{
			if (!row->IsCreated()
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
				&& HasRowCheckoutStatus(row)
#endif
				&& (!row->HasField(m_ColCheckoutUser)
					|| row->GetField(m_ColCheckoutUser).IsModified() && PooledString(row->GetField(m_ColCheckoutUser).GetValueStr()).IsEmpty())
				&& authorizedByRBAC(row, ID_DRIVEITEMSGRID_CHECKOUT))
			{
				enable = TRUE;
				break;
			}
		}
	}

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnUpdateShowFoldersInFlatToggle(CCmdUI* pCmdUI)
{
	ASSERT(nullptr != pCmdUI);
	if (nullptr != pCmdUI)
	{
		pCmdUI->Enable((hasNoTaskRunning() && !GetBackend().IsHierarchyView()) ? TRUE : FALSE);
		pCmdUI->SetCheck(m_ShowFoldersInFlatView ? TRUE : FALSE);
		setTextFromProfUISCommand(*pCmdUI);
	}
}

#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
void GridDriveItems::OnUpdateLoadCkeckoutStatus(CCmdUI* pCmdUI)
{
	auto enable = hasNoTaskRunning()
		&& IsFrameConnected()
		&& nullptr != m_ColCheckoutUser
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [](auto& p_Row)
		{
			return !p_Row->IsCreated() && (GridUtil::IsBusinessDriveItem(p_Row) || GridUtil::IsBusinessDriveItemFolder(p_Row));
		});
	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}
#endif

void GridDriveItems::OnShowFoldersInFlatToggle()
{
	ASSERT(!GetBackend().IsHierarchyView());
	if (!GetBackend().IsHierarchyView())
		setFoldersVisibilityInFlatView(!m_ShowFoldersInFlatView);
}

#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
void GridDriveItems::OnLoadCkeckoutStatus()
{
	if (IsFrameConnected())
	{
		vector<GridBackendRow*> rows;

		{
			for (auto& row : GetSelectedRows())
			{
				ASSERT(nullptr != row);
				if (!row->IsCreated() && (GridUtil::IsBusinessDriveItem(row) || GridUtil::IsBusinessDriveItemFolder(row)))
				{
					rows.emplace_back(row);
				}
			}
		}

		if (!rows.empty())
		{
			CommandInfo info;
			info.Data() = GetSubRequestInfo(rows, {});
			info.SetFrame(m_FrameDriveItems);
			info.SetRBACPrivilege(m_FrameDriveItems->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Drive, Command::ModuleTask::LoadCheckoutStatus, info, { this, g_ActionNameSelectedLoadCheckoutAndRetention, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
	}
}
#endif

void GridDriveItems::setFoldersVisibilityInFlatView(const bool p_ShowFoldersInFlatView)
{
	m_ShowFoldersInFlatView = p_ShowFoldersInFlatView;
	GetBackend().SetForceShowHierarchyParentsInFlat(m_ShowFoldersInFlatView);
	UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridDriveItems::postSetHierarchyViewProcess()
{
	SetColumnVisible(m_ColDirectory, !GetBackend().IsHierarchyView(), GridBackendUtil::SETVISIBLE_NONE);
	if (!GetBackend().IsHierarchyView())
		MoveColumnAfter(m_ColDirectory, getColMetaDisplayName(), false);
}

vector<GridBackendColumn*> GridDriveItems::getCheckoutStatusFamilyColumns() const
{
	return { 
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
		m_ColCheckoutLoaded,
#endif
		m_ColCheckoutUser, 
		m_ColCheckinComment, 
		m_ColComplianceTag, 
		m_ColComplianceTimeWrittenTime };
}

bool GridDriveItems::isCheckoutStatusDataColumn(const GridBackendColumn* p_Col) const
{
	return p_Col == m_ColCheckoutUser
		|| p_Col == m_ColCheckinComment
		|| p_Col == m_ColComplianceTag
		|| p_Col == m_ColComplianceTimeWrittenTime
		;
}

void GridDriveItems::fillCheckoutStatus(GridBackendRow* p_Row, const CheckoutStatus& p_CheckoutStatus, GridUpdater* p_Updater)
{
	if (p_CheckoutStatus.m_Error)
	{
		// show error in status column?
		p_Row->AddField(p_CheckoutStatus.m_Error->GetFullErrorMessage(), m_ColCheckoutUser, GridBackendUtil::ICON_ERROR);
		p_Row->AddField(p_CheckoutStatus.m_Error->GetFullErrorMessage(), m_ColCheckinComment, GridBackendUtil::ICON_ERROR);
		p_Row->AddField(p_CheckoutStatus.m_Error->GetFullErrorMessage(), m_ColComplianceTag, GridBackendUtil::ICON_ERROR);
		p_Row->AddField(p_CheckoutStatus.m_Error->GetFullErrorMessage(), m_ColComplianceTimeWrittenTime, GridBackendUtil::ICON_ERROR);

		if (nullptr != p_Updater)
			p_Updater->OnLoadingError(p_Row, *p_CheckoutStatus.m_Error);
	}
	else
	{
		p_Row->AddField(p_CheckoutStatus.m_CheckoutUser, m_ColCheckoutUser, (p_CheckoutStatus.m_CheckoutUser && !p_CheckoutStatus.m_CheckoutUser->IsEmpty()) ? m_PublicationLevelIcons[_YTEXT("checkout")] : NO_ICON);
		p_Row->AddFieldForRichText(p_CheckoutStatus.m_CheckinComment, m_ColCheckinComment);
		p_Row->AddField(p_CheckoutStatus.m_ComplicanceTag, m_ColComplianceTag);
		p_Row->AddField(p_CheckoutStatus.m_ComplicanceTagWrittenTime, m_ColComplianceTimeWrittenTime);
	}
}

void GridDriveItems::clearCheckoutStatus(GridBackendRow* p_Row)
{
	p_Row->RemoveField(m_ColCheckoutUser);
	p_Row->RemoveField(m_ColCheckinComment);
	p_Row->RemoveField(m_ColComplianceTag);
	p_Row->RemoveField(m_ColComplianceTimeWrittenTime);
}

RoleDelegationUtil::RBAC_Privilege GridDriveItems::getPermission(const int p_CommandID) const
{
	const bool ownerGroup			= m_FrameDriveItems->GetOrigin() == Origin::Group;
	const bool ownerSite			= m_FrameDriveItems->GetOrigin() == Origin::Site;
	const bool ownerUser			= m_FrameDriveItems->GetOrigin() == Origin::User;
	const bool ownerDeletedUser		= m_FrameDriveItems->GetOrigin() == Origin::DeletedUser;
	const bool ownerDeletedGroup	= false;//m_FrameDriveItems->GetOrigin() == Origin::DeletedGroup;
	const bool ownerChannel			= m_FrameDriveItems->GetOrigin() == Origin::Channel;
	ASSERT(ownerGroup || ownerSite || ownerUser || ownerDeletedUser || ownerDeletedGroup || ownerChannel);

	switch (p_CommandID)
	{
		case ID_DRIVEITEMSGRID_VIEWPERMISSIONS:
		case ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODE:
		case ID_DRIVEITEMSGRID_VIEWPERMISSIONS_EXPLODEANDFLAT:
			if (ownerChannel)
				return RoleDelegationUtil::RBAC_CHANNEL_ONEDRIVE_LOADMORE;
			if (ownerGroup || ownerDeletedGroup)
				return RoleDelegationUtil::RBAC_GROUP_ONEDRIVE_LOADMORE;
			if (ownerSite)
				return RoleDelegationUtil::RBAC_SITE_ONEDRIVE_LOADMORE;
			if (ownerUser || ownerDeletedUser)
				return RoleDelegationUtil::RBAC_USER_ONEDRIVE_LOADMORE;
			break;
		case ID_DRIVEITEMSGRID_DELETEPERMISSIONS:
		case ID_DRIVEITEMSGRID_SETPERMISSIONROLETOREAD:
		case ID_DRIVEITEMSGRID_SETPERMISSIONROLETOWRITE:
			if (ownerChannel)
				return RoleDelegationUtil::RBAC_CHANNEL_ONEDRIVE_PERMISSIONS_EDIT;
			if (ownerGroup || ownerDeletedGroup)
				return RoleDelegationUtil::RBAC_GROUP_ONEDRIVE_PERMISSIONS_EDIT;
			if (ownerSite)
				return RoleDelegationUtil::RBAC_SITE_ONEDRIVE_PERMISSIONS_EDIT;
			if (ownerUser || ownerDeletedUser)
				return RoleDelegationUtil::RBAC_USER_ONEDRIVE_PERMISSIONS_EDIT;
			break;
		case ID_DRIVEITEMSGRID_DOWNLOAD:
			if (ownerChannel)
				return RoleDelegationUtil::RBAC_CHANNEL_ONEDRIVE_DOWNLOAD;
			if (ownerGroup || ownerDeletedGroup)
				return RoleDelegationUtil::RBAC_GROUP_ONEDRIVE_DOWNLOAD;
			if (ownerSite)
				return RoleDelegationUtil::RBAC_SITE_ONEDRIVE_DOWNLOAD;
			if (ownerUser || ownerDeletedUser)
				return RoleDelegationUtil::RBAC_USER_ONEDRIVE_DOWNLOAD;
			break;
		case ID_DRIVEITEMSGRID_DELETE:
			if (ownerChannel)
				return RoleDelegationUtil::RBAC_CHANNEL_ONEDRIVE_DELETE;
			if (ownerGroup || ownerDeletedGroup)
				return RoleDelegationUtil::RBAC_GROUP_ONEDRIVE_DELETE;
			if (ownerSite)
				return RoleDelegationUtil::RBAC_SITE_ONEDRIVE_DELETE;
			if (ownerUser || ownerDeletedUser)
				return RoleDelegationUtil::RBAC_USER_ONEDRIVE_DELETE;
			break;
		case ID_DRIVEITEMSGRID_RENAME:
			if (ownerChannel)
				return RoleDelegationUtil::RBAC_CHANNEL_ONEDRIVE_RENAME;
			if (ownerGroup || ownerDeletedGroup)
				return RoleDelegationUtil::RBAC_GROUP_ONEDRIVE_RENAME;
			if (ownerSite)
				return RoleDelegationUtil::RBAC_SITE_ONEDRIVE_RENAME;
			if (ownerUser || ownerDeletedUser)
				return RoleDelegationUtil::RBAC_USER_ONEDRIVE_RENAME;
			break;
		case ID_DRIVEITEMSGRID_CREATEFOLDER:
			if (ownerChannel)
				return RoleDelegationUtil::RBAC_CHANNEL_ONEDRIVE_CREATEFOLDER;
			if (ownerGroup || ownerDeletedGroup)
				return RoleDelegationUtil::RBAC_GROUP_ONEDRIVE_CREATEFOLDER;
			if (ownerSite)
				return RoleDelegationUtil::RBAC_SITE_ONEDRIVE_CREATEFOLDER;
			if (ownerUser || ownerDeletedUser)
				return RoleDelegationUtil::RBAC_USER_ONEDRIVE_CREATEFOLDER;
			break;
		case ID_DRIVEITEMSGRID_ADDFILE:
			if (ownerChannel)
				return RoleDelegationUtil::RBAC_CHANNEL_ONEDRIVE_ADDFILE;
			if (ownerGroup || ownerDeletedGroup)
				return RoleDelegationUtil::RBAC_GROUP_ONEDRIVE_ADDFILE;
			if (ownerSite)
				return RoleDelegationUtil::RBAC_SITE_ONEDRIVE_ADDFILE;
			if (ownerUser || ownerDeletedUser)
				return RoleDelegationUtil::RBAC_USER_ONEDRIVE_ADDFILE;
			break;

		case ID_DRIVEITEMSGRID_CHECKIN:
		case ID_DRIVEITEMSGRID_CHECKOUT:
			return RoleDelegationUtil::RBAC_NONE; // FIXME!!
			break;
	}

	ASSERT(false);
	return RoleDelegationUtil::RBAC_NONE;
}

bool GridDriveItems::authorizedByRBAC(GridBackendRow* p_Row, const int p_CommandID) const
{
	bool rvRBACauthorized = false;

	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
		rvRBACauthorized = O365Grid::IsAuthorizedByRoleDelegation(p_Row, getPermission(p_CommandID), true);

	return rvRBACauthorized;
}

void GridDriveItems::OnUpdateShowPermissions(CCmdUI* pCmdUI)
{
	bool enable = false;

    if (hasNoTaskRunning() && IsFrameConnected())
    {
		ASSERT(nullptr != m_FrameDriveItems);
        for (auto row : GetSelectedRows())
        {
			if (!row->IsCreated()
				&& (GridUtil::IsBusinessDriveItem(row) || GridUtil::IsBusinessDriveItemFolder(row) || GridUtil::IsBusinessDriveItemNotebook(row))
				&& authorizedByRBAC(row, ID_DRIVEITEMSGRID_VIEWPERMISSIONS))
            {
				enable = true;
                break;
            }
        }
    }

    pCmdUI->Enable(enable ? TRUE : FALSE);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnUpdateDeletePermissions(CCmdUI* pCmdUI)
{
    BOOL enable = FALSE;

	if (hasNoTaskRunning())
	{
		for (auto row : GetSelectedRows())
		{
			if (row->IsMoreLoaded()
				&& (GridUtil::IsBusinessDriveItem(row) || GridUtil::IsBusinessDriveItemFolder(row) || GridUtil::IsBusinessDriveItemNotebook(row))
				&& !row->HasFlag(GridBackendUtil::ROWFLAGS::PARENTDELETED)
				&& authorizedByRBAC(row, ID_DRIVEITEMSGRID_DELETEPERMISSIONS))
			{
				if (!row->IsExplosionFragment() || (row->IsExplosionFragment() && !row->GetField(m_ColInheritedFromId).HasValue()))
				{
					GridBackendField& fieldRole = row->GetField(m_ColRole);

					if (fieldRole.HasValue())
					{
						auto roles = fieldRole.GetValuesMulti<PooledString>();
						if (nullptr != roles)
						{
							for (const auto& role : *roles)
							{
								if (role != DriveItemsUtil::ROLE_VALUE_OWNER)
								{
									enable = TRUE;
									break;
								}
							}
						}
						else if (fieldRole.GetValueStr() != DriveItemsUtil::ROLE_VALUE_OWNER)
							enable = TRUE;

						if (enable)
							break;
					}
				}
			}
		}
	}

    pCmdUI->Enable(enable);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnUpdateDownload(CCmdUI* pCmdUI)
{
    BOOL enable = FALSE;

	if (hasNoTaskRunning() && IsFrameConnected())
	{
		for (auto row : GetSelectedRows())
		{
			if (!row->IsCreated())
			{
				if (GridUtil::isBusinessType<BusinessDriveItem>(row) && authorizedByRBAC(row, ID_DRIVEITEMSGRID_DOWNLOAD))
				{
					enable = TRUE;
					break;
				}
				else if ((GridUtil::IsBusinessDriveItemFolder(row) || GridUtil::IsBusinessDriveItemNotebook(row))
					&& authorizedByRBAC(row, ID_DRIVEITEMSGRID_DOWNLOAD))
				{
					// If a folder is selected, download all drive items in it.
					vector<GridBackendRow*> children;
					GetBackend().GetHierarchyChildren(row, children);

					for (auto child : children)
					{
						if (GridUtil::isBusinessType<BusinessDriveItem>(child))
						{
							enable = TRUE;
							break;
						}
					}
				}
			}
		}
    }

    pCmdUI->Enable(enable);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnUpdateDeleteItem(CCmdUI* pCmdUI)
{
	BOOL enabled = FALSE;
	if (hasNoTaskRunning())
	{
		for (auto& row : GetSelectedRows())
		{
			if ((GridUtil::IsBusinessDriveItem(row) || GridUtil::IsBusinessDriveItemFolder(row) || GridUtil::IsBusinessDriveItemNotebook(row))
				&& !row->HasFlag(GridBackendUtil::ROWFLAGS::PARENTDELETED)
				&& authorizedByRBAC(row, ID_DRIVEITEMSGRID_DELETE)
				//&& !row->IsDeleted()	// On exploded row, deleted status is for permission deletion, we can't use it here.
										// we should use nullptr == GetModifications().GetRowModification<DeletedObjectModification>(rowPk)
										// but getting the row PK and finding a mod can take a bit of time.
										// For now, just allow deletion even if already deleted (handled in OnDeleteItem())
				)
			{
				enabled = TRUE;
				break;
			}
		}
	}
	pCmdUI->Enable(enabled);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnUpdateRenameItem(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;
	if (hasNoTaskRunning() && GetSelectedRows().size() == 1)
	{
		auto& selRow = *(GetSelectedRows().begin());
		enable = ((GridUtil::isBusinessType<BusinessDriveItem>(selRow) || GridUtil::IsBusinessDriveItemFolder(selRow) || GridUtil::IsBusinessDriveItemNotebook(selRow))
				&& authorizedByRBAC(selRow, ID_DRIVEITEMSGRID_RENAME))
				? TRUE
				: FALSE;
	}

	if (CRMpipe().IsFeatureSandboxed() && nullptr != m_FrameDriveItems && m_FrameDriveItems->GetSearchCriteria())
		enable = FALSE;

    pCmdUI->Enable(enable);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnUpdateToggleOwnerPermissions(CCmdUI* pCmdUI)
{
	setTextFromProfUISCommand(*pCmdUI);
	pCmdUI->Enable(hasNoTaskRunning() && !m_ShowOwnerPermissions && !m_AllPermissions.empty());
}

void GridDriveItems::OnUpdateCreateFolder(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;

	if (hasNoTaskRunning())
	{
		for (auto& row : GetSelectedRows())
		{
			if (authorizedByRBAC(row, ID_DRIVEITEMSGRID_CREATEFOLDER))
			{
				enable = TRUE;
				break;
			}
		}
	}

	if (CRMpipe().IsFeatureSandboxed() && nullptr != m_FrameDriveItems && m_FrameDriveItems->GetSearchCriteria())
		enable = FALSE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnUpdateAddFile(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;

	if (hasNoTaskRunning())
	{
		for (auto& row : GetSelectedRows())
		{
			if (authorizedByRBAC(row, ID_DRIVEITEMSGRID_ADDFILE))
			{
				enable = TRUE;
				break;
			}
		}
	}

	if (CRMpipe().IsFeatureSandboxed() && nullptr != m_FrameDriveItems && m_FrameDriveItems->GetSearchCriteria())
		enable = FALSE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnUpdateImportFolder(CCmdUI* pCmdUI)
{
	BOOL enable = FALSE;

	if (hasNoTaskRunning())
	{
		for (auto& row : GetSelectedRows())
		{
			if (authorizedByRBAC(row, ID_DRIVEITEMSGRID_CREATEFOLDER) && authorizedByRBAC(row, ID_DRIVEITEMSGRID_ADDFILE))
			{
				enable = TRUE;
				break;
			}
		}
	}

	if (CRMpipe().IsFeatureSandboxed() && nullptr != m_FrameDriveItems && m_FrameDriveItems->GetSearchCriteria())
		enable = FALSE;

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnChangeSearchCriteria()
{
	const auto& crit = m_FrameDriveItems->GetSearchCriteria();
	if (CRMpipe().IsFeatureSandboxed() && hasNoTaskRunning() && IsFrameConnected() && crit)
	{
		DlgInput dlg(_YTEXT("Optional search criteria"), _YTEXT(""), *crit, this);
		if (IDOK == dlg.DoModal())
			m_FrameDriveItems->RefreshWithNewSearchCriteria(dlg.GetText());
	}
}

void GridDriveItems::OnUpdateChangeSearchCriteria(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(CRMpipe().IsFeatureSandboxed() && hasNoTaskRunning() && IsFrameConnected() && m_FrameDriveItems->GetSearchCriteria());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridDriveItems::OnTimer(UINT_PTR nIDEvent)
{
    if (nIDEvent == g_DownloadTimerId)
    {
        if (m_NbDownloadsOccuring > 0)
            UpdateMegaShark();
    }
    else
    {
        GridDriveItemsBaseClass::OnTimer(nIDEvent);
    }
}

void GridDriveItems::AddOccuringDownload(GridBackendRow* p_Row)
{
	ClearStatus({ p_Row });

    if (m_NbDownloadsOccuring++ == 0)
        SetTimer(g_DownloadTimerId, 1000, nullptr);
    UpdateMegaShark();
}

void GridDriveItems::RemoveOccuringDownload(const DriveItemDownloadInfo& p_DownloadInfo, const boost::YOpt<wstring>& p_FileLocation)
{
    if (--m_NbDownloadsOccuring == 0)
        KillTimer(g_DownloadTimerId);

    if (p_FileLocation != boost::none)
    {
        GridBackendField& field = p_DownloadInfo.m_Row->GetField(GetColDownloadProgress());
        field.SetCellType(GridBackendUtil::CELL_TYPE_HYPERLINK);
        p_DownloadInfo.m_Row->AddFieldForHyperlink(PooledString(*p_FileLocation), GetColDownloadProgress());

        if (p_DownloadInfo.m_OpenAfterDownload)
            ::ShellExecute(NULL, _YTEXT("open"), wstring(p_DownloadInfo.m_Filepath).c_str(), NULL, NULL, SW_SHOWNORMAL);

        UpdateMegaShark();
    }
}

std::shared_ptr<DownloadTaskPool> GridDriveItems::GetDownloadTaskPool(YtriaTaskData p_TaskData, HWND p_Originator)
{
    return std::make_shared<DownloadTaskPool>(m_DownloadTaskPool, p_TaskData.GetId(), p_Originator);
}

int GridDriveItems::GetNbDownloadsOccuring() const
{
    return m_NbDownloadsOccuring;
}

BOOL GridDriveItems::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return GridDriveItemsBaseClass::PreTranslateMessage(pMsg);
}

BusinessDriveItem GridDriveItems::getBusinessObject(GridBackendRow* p_Row) const
{
	const bool created = p_Row->IsCreated(); // FIXME: Should we check GridModifications?
	const bool modified = p_Row->IsModified(); // FIXME: Should we check GridModifications?

	BusinessDriveItem bo;

	{
		const auto& field = p_Row->GetField(GetColId());
		if (field.HasValue())
			bo.SetID(field.GetValueStr());
	}

	{
		const auto& field = p_Row->GetField(m_ColMetaDriveId);
		if (field.HasValue())
			bo.SetDriveId(field.GetValueStr());
	}

    {
        const auto& field = p_Row->GetField(m_ColName);
        if (field.HasValue())
            bo.SetName(PooledString(field.GetValueStr()));
    }

	{
		const auto& field = p_Row->GetField(m_ColOneDrivePath);
		if (field.HasValue())
			bo.SetOneDrivePath(PooledString(field.GetValueStr()));
	}

	{
		const auto& field = p_Row->GetField(m_ColDescription);
		if (field.HasValue())
			bo.SetDescription(PooledString(field.GetValueStr()));
	}

	{
		boost::YOpt<PublicationFacet> pub;
		if (nullptr != m_ColPublicationLevel)
		{
			const auto& field = p_Row->GetField(m_ColPublicationLevel);
			if (field.HasValue())
			{
				if (!pub)
					pub.emplace();
				pub->m_Level = field.GetValueStr();
			}
		}
		{
			const auto& field = p_Row->GetField(m_ColPublicationVersionId);
			if (field.HasValue())
			{
				if (!pub)
					pub.emplace();
				pub->m_VersionId = field.GetValueStr();
			}
		}
		bo.SetPublication(pub);
	}

	if (GridUtil::IsBusinessDriveItemFolder(p_Row))
		bo.SetIsFolder(true);
	else if (GridUtil::IsBusinessDriveItemNotebook(p_Row))
		bo.SetIsNotebook(true);

	if (created)
	{
		if (nullptr != p_Row->GetParentRow() && GridUtil::IsBusinessDriveItemFolder(p_Row->GetParentRow()))
			bo.SetParentFolderId(PooledString(p_Row->GetParentRow()->GetField(GetColId()).GetValueStr()));
	}

	// For creation or replacement
	if (created || modified) // FIXME: Should we check GridModifications?
	{
		const auto& field = p_Row->GetField(m_ColFileToBeUploaded);
		ASSERT(!(created && GridUtil::IsBusinessDriveItem(p_Row)) || field.HasValue());
		if (field.HasValue())
			bo.SetLocalFilePath(PooledString(field.GetValueStr()));
	}

	if (!created)
	{
		// TODO: other properties?
	}
	else
	{
		bo.SetUpdateStatus(BusinessObject::UPDATE_TYPE::UPDATE_TYPE_CREATE);
		//if (p_Row->HasFlag(g_RenameOnConflictFlag))
		//	bo.SetFlags(bo.GetFlags() | BusinessDriveItem::RENAME_ON_CONFLICT);
		//else
		//	bo.SetFlags(bo.GetFlags() & ~BusinessDriveItem::RENAME_ON_CONFLICT);
	}

	bo.SetHierarchyLevel(p_Row->GetHierarchyLevel());

	return bo;
}

void GridDriveItems::fillPermissionFields(GridBackendRow* p_Row, const vector<BusinessPermission>& p_Permissions)
{
    ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		vector<PooledString> permissionId;

		vector<PooledString> grantedToApplicationIdentityID;
		vector<PooledString> grantedToApplicationIdentityName;
		vector<PooledString> grantedToDeviceIdentityID;
		vector<PooledString> grantedToDeviceIdentityName;
		vector<PooledString> grantedToUserIdentityID;
		vector<PooledString> grantedToUserIdentityName;
		vector<PooledString> grantedToUserIdentityEmail;

		vector<PooledString> invitationEmail;
		vector<PooledString> invitationInvitedByApplicationIdentityId;
		vector<PooledString> invitationInvitedByApplicationIdentityName;
		vector<PooledString> invitationInvitedByDeviceIdentityId;
		vector<PooledString> invitationInvitedByDeviceIdentityName;
		vector<PooledString> invitationInvitedByUserIdentityId;
		vector<PooledString> invitationInvitedByUserIdentityName;
		vector<PooledString> invitationInvitedByUserIdentityEmail;
		vector<BOOLItem> invitationSignInRequired;

		vector<PooledString> inheritedFromDriveId;
		vector<PooledString> inheritedFromDriveType;
		vector<PooledString> inheritedFromId;
		vector<PooledString> inheritedFromName;
		vector<PooledString> inheritedFromPath;
		vector<PooledString> inheritedFromShareID;
		vector<PooledString> inheritedFromSharepointIdsListID;
		vector<PooledString> inheritedFromSharepointIdsListItemID;
		vector<PooledString> inheritedFromSharepointIdsListItemUniqueID;
		vector<PooledString> inheritedFromSharepointIdsSiteID;
		vector<HyperlinkItem> inheritedFromSharepointIdsSiteURL;
		vector<PooledString> inheritedFromSharepointIdsWebID;

		vector<PooledString> linkApplicationIdentityId;
		vector<PooledString> linkApplicationIdentityName;
		vector<PooledString> linkType;
		vector<PooledString> linkScope;
		vector<HyperlinkItem> linkWebHtml;
		vector<HyperlinkItem> linkWebUrl;

		vector<PooledString> role;
		vector<PooledString> shareId;

		bool hiddenOwnerPermission = false;
		for (const auto& permission : p_Permissions)
		{
			if (permission.GetRole())
			{
				const auto& roleObj = *permission.GetRole();

				wstring str;
				bool first = true;
				bool isOwner = false;
				for (const auto& rolePart : roleObj)
				{
					if (rolePart == DriveItemsUtil::ROLE_VALUE_OWNER)
					{
						ASSERT(first); // log this?
						isOwner = true;
					}

					if (first)
					{
						first = false;
					}
					else
					{
						ASSERT(!isOwner); // log this?
						str += _YTEXT(" | ");
					}
					str += rolePart;
				}

				if (!m_ShowOwnerPermissions && isOwner)
				{
					hiddenOwnerPermission = true;
					continue; // Go to next permission
				}

				role.emplace_back(str);
			}
			else
			{
				role.emplace_back(GridBackendUtil::g_NoValueString);
			}

			permissionId.emplace_back(permission.GetID().IsEmpty() ? PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString) : permission.GetID());

			fillGrantedTo(permission, grantedToApplicationIdentityID, grantedToApplicationIdentityName, grantedToDeviceIdentityID,
				grantedToDeviceIdentityName, grantedToUserIdentityID, grantedToUserIdentityName, grantedToUserIdentityEmail);
			fillInvitation(permission, invitationEmail, invitationInvitedByApplicationIdentityId,
				invitationInvitedByApplicationIdentityName, invitationInvitedByDeviceIdentityId, invitationInvitedByDeviceIdentityName,
				invitationInvitedByUserIdentityId, invitationInvitedByUserIdentityName, invitationInvitedByUserIdentityEmail, invitationSignInRequired);
			fillInheritedFrom(permission, inheritedFromDriveId, inheritedFromDriveType, inheritedFromId, inheritedFromName, inheritedFromPath,
				inheritedFromShareID, inheritedFromSharepointIdsListID, inheritedFromSharepointIdsListItemID, inheritedFromSharepointIdsListItemUniqueID,
				inheritedFromSharepointIdsSiteID, inheritedFromSharepointIdsSiteURL, inheritedFromSharepointIdsWebID);
			fillLink(permission, linkApplicationIdentityId, linkApplicationIdentityName, linkType, linkScope, linkWebHtml, linkWebUrl);

			if (permission.GetShareId())
				shareId.emplace_back(*permission.GetShareId());
			else
				shareId.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
		}

		if (!permissionId.empty())
		{
			p_Row->SetLParam(0);
			p_Row->AddField(permissionId, m_ColPermissionId);

			p_Row->AddField(grantedToApplicationIdentityID, m_ColGrantedToApplicationIdentityID);
			p_Row->AddField(grantedToApplicationIdentityName, m_ColGrantedToApplicationIdentityName);
			p_Row->AddField(grantedToDeviceIdentityID, m_ColGrantedToDeviceIdentityID);
			p_Row->AddField(grantedToDeviceIdentityName, m_ColGrantedToDeviceIdentityName);
			p_Row->AddField(grantedToUserIdentityID, m_ColGrantedToUserIdentityID);
			p_Row->AddField(grantedToUserIdentityName, m_ColGrantedToUserIdentityName);
			p_Row->AddField(grantedToUserIdentityEmail, m_ColGrantedToUserIdentityEmail);

			p_Row->AddField(invitationEmail, m_ColInvitationEmail);
			p_Row->AddField(invitationInvitedByApplicationIdentityId, m_ColInvitationInvitedByApplicationIdentityId);
			p_Row->AddField(invitationInvitedByApplicationIdentityName, m_ColInvitationInvitedByApplicationIdentityName);
			p_Row->AddField(invitationInvitedByDeviceIdentityId, m_ColInvitationInvitedByDeviceIdentityId);
			p_Row->AddField(invitationInvitedByDeviceIdentityName, m_ColInvitationInvitedByDeviceIdentityName);
			p_Row->AddField(invitationInvitedByUserIdentityId, m_ColInvitationInvitedByUserIdentityId);
			p_Row->AddField(invitationInvitedByUserIdentityName, m_ColInvitationInvitedByUserIdentityName);
			p_Row->AddField(invitationInvitedByUserIdentityEmail, m_ColInvitationInvitedByUserIdentityEmail);
			p_Row->AddField(invitationSignInRequired, m_ColInvitationSignInRequired);

			p_Row->AddField(inheritedFromDriveId, m_ColInheritedFromDriveID);
			p_Row->AddField(inheritedFromDriveType, m_ColInheritedFromDriveType);
			p_Row->AddField(inheritedFromId, m_ColInheritedFromId);
			p_Row->AddField(inheritedFromName, m_ColInheritedFromName);
			p_Row->AddField(inheritedFromPath, m_ColInheritedFromPath);
			p_Row->AddField(inheritedFromShareID, m_ColInheritedFromShareID);
			p_Row->AddField(inheritedFromSharepointIdsListID, m_ColInheritedFromSharepointIDsListID);
			p_Row->AddField(inheritedFromSharepointIdsListItemID, m_ColInheritedFromSharepointIDsListItemID);
			p_Row->AddField(inheritedFromSharepointIdsListItemUniqueID, m_ColInheritedFromSharepointIDsListItemUniqueID);
			p_Row->AddField(inheritedFromSharepointIdsSiteID, m_ColInheritedFromSharepointIDsSiteID);
			p_Row->AddFieldForHyperlink(inheritedFromSharepointIdsSiteURL, m_ColInheritedFromSharepointIDsSiteURL);
			p_Row->AddField(inheritedFromSharepointIdsWebID, m_ColInheritedFromSharepointIDsWebID);

			p_Row->AddField(linkApplicationIdentityId, m_ColLinkApplicationIdentityId);
			p_Row->AddField(linkApplicationIdentityName, m_ColLinkApplicationIdentityName);
			p_Row->AddField(linkType, m_ColLinkType);
			p_Row->AddField(linkScope, m_ColLinkScope);
			p_Row->AddField(linkWebHtml, m_ColLinkWebHtml);
			p_Row->AddFieldForHyperlink(linkWebUrl, m_ColLinkWebURL);

			// Make two vectors into one for the "Target" column.
			// Remove the 666 from the resulting list which should be equal in size to the largest list.
			// Also, values from Link-Scope has to be in between [--].
			vector<PooledString> tempList;
			tempList.insert(tempList.end(), linkScope.begin(), linkScope.end());
			tempList.erase(std::remove_if(tempList.begin(), tempList.end(), [](const PooledString& p_Elem) { return p_Elem == GridBackendUtil::g_NoValueString; }), tempList.end());
			std::transform(
				tempList.begin(), 
				tempList.end(), 
				tempList.begin(), 
				[](const PooledString& p_Elem)
				{ 
					wstring modified;
					PooledString Temp(p_Elem);
					if(p_Elem == DriveItemsUtil::TARGET_USERS_NUDE) // users
						Temp = YtriaTranslate::Do(GridDriveItems_fillPermissionFields_1, _YLOC("Specific people link")).c_str();
					else if(p_Elem == DriveItemsUtil::TARGET_ANONYMOUS_NUDE) // anonymous
						Temp = YtriaTranslate::Do(GridDriveItems_fillPermissionFields_2, _YLOC("Anonymous link")).c_str();
					else if(p_Elem == DriveItemsUtil::TARGET_ORGANIZATION_NUDE) // organization
						Temp = YtriaTranslate::Do(GridDriveItems_fillPermissionFields_3, _YLOC("Internal link")).c_str();
					modified.append(_YTEXT("[-"));
					modified.append(Temp);
					modified.append(_YTEXT("-]"));
					return modified;
				});
			tempList.insert(tempList.end(), grantedToUserIdentityName.begin(), grantedToUserIdentityName.end());
			tempList.erase(std::remove_if(tempList.begin(), tempList.end(), [](const PooledString& p_Elem) { return p_Elem == GridBackendUtil::g_NoValueString; }), tempList.end());
			ASSERT(tempList.size() == grantedToUserIdentityName.size() || tempList.size() == linkScope.size());

			p_Row->AddField(tempList, m_ColTarget);
			p_Row->AddField(role, m_ColRole);
			p_Row->AddField(shareId, m_ColShareId);

			if (!hiddenOwnerPermission && role.size() > 1 || hiddenOwnerPermission && role.size() > 0)
			{
				p_Row->AddField(g_SharedShared, m_ColShared);
			}
			else
			{
				ASSERT(role.size() > 0 && role[0] == DriveItemsUtil::ROLE_VALUE_OWNER);
				p_Row->AddField(g_SharedPrivate, m_ColShared);
			}
		}
		else
		{
			p_Row->SetLParam(hiddenOwnerPermission ? g_HiddenOwnerLparam : 0);
			p_Row->RemoveField(m_ColPermissionId);
			p_Row->RemoveField(m_ColGrantedToApplicationIdentityID);
			p_Row->RemoveField(m_ColGrantedToApplicationIdentityName);
			p_Row->RemoveField(m_ColGrantedToDeviceIdentityID);
			p_Row->RemoveField(m_ColGrantedToDeviceIdentityName);
			p_Row->RemoveField(m_ColGrantedToUserIdentityID);
			p_Row->RemoveField(m_ColGrantedToUserIdentityName);
			p_Row->RemoveField(m_ColGrantedToUserIdentityEmail);
			p_Row->RemoveField(m_ColInvitationEmail);
			p_Row->RemoveField(m_ColInvitationInvitedByApplicationIdentityId);
			p_Row->RemoveField(m_ColInvitationInvitedByApplicationIdentityName);
			p_Row->RemoveField(m_ColInvitationInvitedByDeviceIdentityId);
			p_Row->RemoveField(m_ColInvitationInvitedByDeviceIdentityName);
			p_Row->RemoveField(m_ColInvitationInvitedByUserIdentityId);
			p_Row->RemoveField(m_ColInvitationInvitedByUserIdentityName);
			p_Row->RemoveField(m_ColInvitationInvitedByUserIdentityEmail);
			p_Row->RemoveField(m_ColInvitationSignInRequired);
			p_Row->RemoveField(m_ColInheritedFromDriveID);
			p_Row->RemoveField(m_ColInheritedFromDriveType);
			p_Row->RemoveField(m_ColInheritedFromId);
			p_Row->RemoveField(m_ColInheritedFromName);
			p_Row->RemoveField(m_ColInheritedFromPath);
			p_Row->RemoveField(m_ColInheritedFromShareID);
			p_Row->RemoveField(m_ColInheritedFromSharepointIDsListID);
			p_Row->RemoveField(m_ColInheritedFromSharepointIDsListItemID);
			p_Row->RemoveField(m_ColInheritedFromSharepointIDsListItemUniqueID);
			p_Row->RemoveField(m_ColInheritedFromSharepointIDsSiteID);
			p_Row->RemoveField(m_ColInheritedFromSharepointIDsSiteURL);
			p_Row->RemoveField(m_ColInheritedFromSharepointIDsWebID);
			p_Row->RemoveField(m_ColLinkApplicationIdentityId);
			p_Row->RemoveField(m_ColLinkApplicationIdentityName);
			p_Row->RemoveField(m_ColLinkType);
			p_Row->RemoveField(m_ColLinkScope);
			p_Row->RemoveField(m_ColLinkWebHtml);
			p_Row->RemoveField(m_ColLinkWebURL);
			p_Row->RemoveField(m_ColTarget);
			p_Row->RemoveField(m_ColRole);
			p_Row->RemoveField(m_ColShareId);

			p_Row->AddField(g_SharedPrivate, m_ColShared);
		}
	}
}

void GridDriveItems::fillGrantedTo(const BusinessPermission& p_Permission, vector<PooledString>& p_GrantedToApplicationIdentityID, vector<PooledString>& p_GrantedToApplicationIdentityName, vector<PooledString>& p_GrantedToDeviceIdentityID, vector<PooledString>& p_GrantedToDeviceIdentityName, vector<PooledString>& p_GrantedToUserIdentityID, vector<PooledString>& p_GrantedToUserIdentityName, vector<PooledString>& p_GrantedToUserIdentityEmail)
{
    const auto& grantedTo = p_Permission.GetGrantedTo();
    if (grantedTo)
    {
        if (grantedTo->Application)
        {
            p_GrantedToApplicationIdentityID.emplace_back(grantedTo->Application->Id ? *grantedTo->Application->Id : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
            p_GrantedToApplicationIdentityName.emplace_back(grantedTo->Application->DisplayName ? *grantedTo->Application->DisplayName : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
        }
        else
        {
            p_GrantedToApplicationIdentityID.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
            p_GrantedToApplicationIdentityName.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
        }

        if (grantedTo->Device)
        {
            p_GrantedToDeviceIdentityID.emplace_back(grantedTo->Device->Id ? *grantedTo->Device->Id : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
            p_GrantedToDeviceIdentityName.emplace_back(grantedTo->Device->DisplayName ? *grantedTo->Device->DisplayName : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
        }
        else
        {
            p_GrantedToDeviceIdentityID.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
            p_GrantedToDeviceIdentityName.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
        }

        if (grantedTo->User)
        {
            p_GrantedToUserIdentityID.emplace_back(grantedTo->User->Id ? *grantedTo->User->Id : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
            p_GrantedToUserIdentityName.emplace_back(grantedTo->User->DisplayName ? *grantedTo->User->DisplayName : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
            p_GrantedToUserIdentityEmail.emplace_back(grantedTo->User->Email ? *grantedTo->User->Email : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
        }
        else
        {
            p_GrantedToUserIdentityID.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
            p_GrantedToUserIdentityName.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
            p_GrantedToUserIdentityEmail.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
        }
    }
    else
    {
        p_GrantedToApplicationIdentityID.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
        p_GrantedToApplicationIdentityName.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
        p_GrantedToDeviceIdentityID.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
        p_GrantedToDeviceIdentityName.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
        p_GrantedToUserIdentityID.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
        p_GrantedToUserIdentityName.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
        p_GrantedToUserIdentityEmail.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
    }

    
}

void GridDriveItems::fillInvitation(const BusinessPermission& p_Permission, vector<PooledString>& p_InvitationEmail, vector<PooledString>& p_InvitationInvitedByApplicationIdentityId, vector<PooledString>& p_InvitationInvitedByApplicationIdentityName, vector<PooledString>& p_InvitationInvitedByDeviceIdentityId, vector<PooledString>& p_InvitationInvitedByDeviceIdentityName, vector<PooledString>& p_InvitationInvitedByUserIdentityId, vector<PooledString>& p_InvitationInvitedByUserIdentityName, vector<PooledString>& p_InvitationInvitedByUserIdentityEmail, vector<BOOLItem>& p_InvitationSignInRequired)
{
    const auto& invitation = p_Permission.GetInvitation();
    if (invitation)
    {
        p_InvitationEmail.emplace_back(invitation->Email ? *invitation->Email : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
        if (invitation->InvitedBy)
        {
            if (invitation->InvitedBy->Application)
            {
                p_InvitationInvitedByApplicationIdentityId.emplace_back(invitation->InvitedBy->Application->Id ? *invitation->InvitedBy->Application->Id : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
                p_InvitationInvitedByApplicationIdentityName.emplace_back(invitation->InvitedBy->Application->DisplayName ? *invitation->InvitedBy->Application->DisplayName : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
            }
            else
            {
                p_InvitationInvitedByApplicationIdentityId.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
                p_InvitationInvitedByApplicationIdentityName.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
            }

            if (invitation->InvitedBy->Device)
            {
                p_InvitationInvitedByDeviceIdentityId.emplace_back(invitation->InvitedBy->Device->Id ? *invitation->InvitedBy->Device->Id : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
                p_InvitationInvitedByDeviceIdentityName.emplace_back(invitation->InvitedBy->Device->DisplayName ? *invitation->InvitedBy->Device->DisplayName : PooledString(GridBackendUtil::GridBackendUtil::g_NoValueString));
            }
            else
            {
                p_InvitationInvitedByDeviceIdentityId.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
                p_InvitationInvitedByDeviceIdentityName.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
            }

            if (invitation->InvitedBy->User)
            {
                p_InvitationInvitedByUserIdentityId.emplace_back(invitation->InvitedBy->User->Id ? *invitation->InvitedBy->User->Id : PooledString(GridBackendUtil::g_NoValueString));
                p_InvitationInvitedByUserIdentityName.emplace_back(invitation->InvitedBy->User->DisplayName ? *invitation->InvitedBy->User->DisplayName : PooledString(GridBackendUtil::g_NoValueString));
                p_InvitationInvitedByUserIdentityEmail.emplace_back(invitation->InvitedBy->User->Email ? *invitation->InvitedBy->User->Email : PooledString(GridBackendUtil::g_NoValueString));
            }
            else
            {
                p_InvitationInvitedByUserIdentityId.emplace_back(GridBackendUtil::g_NoValueString);
                p_InvitationInvitedByUserIdentityName.emplace_back(GridBackendUtil::g_NoValueString);
                p_InvitationInvitedByUserIdentityEmail.emplace_back(GridBackendUtil::g_NoValueString);
            }

			if (invitation->SignInRequired)
			{
				if (*invitation->SignInRequired)
					p_InvitationSignInRequired.emplace_back(BOOLItem(TRUE));
				else
					p_InvitationSignInRequired.emplace_back(BOOLItem(FALSE));
			}
			else
			{
				p_InvitationSignInRequired.emplace_back(BOOLItem());
			}
        }
        else
        {
            p_InvitationInvitedByApplicationIdentityId.emplace_back(GridBackendUtil::g_NoValueString);
            p_InvitationInvitedByApplicationIdentityName.emplace_back(GridBackendUtil::g_NoValueString);
            p_InvitationInvitedByDeviceIdentityId.emplace_back(GridBackendUtil::g_NoValueString);
            p_InvitationInvitedByDeviceIdentityName.emplace_back(GridBackendUtil::g_NoValueString);
            p_InvitationInvitedByUserIdentityId.emplace_back(GridBackendUtil::g_NoValueString);
            p_InvitationInvitedByUserIdentityName.emplace_back(GridBackendUtil::g_NoValueString);
            p_InvitationInvitedByUserIdentityEmail.emplace_back(GridBackendUtil::g_NoValueString);
			p_InvitationSignInRequired.emplace_back(BOOLItem());
        }
    }
    else
    {
        p_InvitationEmail.emplace_back(GridBackendUtil::GridBackendUtil::g_NoValueString);
        p_InvitationInvitedByApplicationIdentityId.emplace_back(GridBackendUtil::g_NoValueString);
        p_InvitationInvitedByApplicationIdentityName.emplace_back(GridBackendUtil::g_NoValueString);
        p_InvitationInvitedByDeviceIdentityId.emplace_back(GridBackendUtil::g_NoValueString);
        p_InvitationInvitedByDeviceIdentityName.emplace_back(GridBackendUtil::g_NoValueString);
        p_InvitationInvitedByUserIdentityId.emplace_back(GridBackendUtil::g_NoValueString);
        p_InvitationInvitedByUserIdentityName.emplace_back(GridBackendUtil::g_NoValueString);
        p_InvitationInvitedByUserIdentityEmail.emplace_back(GridBackendUtil::g_NoValueString);
		p_InvitationSignInRequired.emplace_back(BOOLItem());
    }
}

void GridDriveItems::fillInheritedFrom(const BusinessPermission& p_Permission,	vector<PooledString>& p_InheritedFromDriveId, 
																				vector<PooledString>& p_InheritedFromDriveType, 
																				vector<PooledString>& p_InheritedFromId, 
																				vector<PooledString>& p_InheritedFromName, 
																				vector<PooledString>& p_InheritedFromPath, 
																				vector<PooledString>& p_InheritedFromShareID, 
																				vector<PooledString>& p_InheritedFromSharepointIdsListID, 
																				vector<PooledString>& p_InheritedFromSharepointIdsListItemID, 
																				vector<PooledString>& p_InheritedFromSharepointIdsListItemUniqueID, 
																				vector<PooledString>& p_InheritedFromSharepointIdsSiteID, 
																				vector<HyperlinkItem>& p_InheritedFromSharepointIdsSiteURL, 
																				vector<PooledString>& p_InheritedFromSharepointIdsWebID)
{
    const auto& inheritedFrom = p_Permission.GetInheritedFrom();
    if (inheritedFrom)
    {
        if (inheritedFrom->DriveId)
            p_InheritedFromDriveId.emplace_back(*inheritedFrom->DriveId);
        else
            p_InheritedFromDriveId.emplace_back(GridBackendUtil::g_NoValueString);

        if (inheritedFrom->DriveType)
			p_InheritedFromDriveType.emplace_back(*inheritedFrom->DriveType);
        else
			p_InheritedFromDriveType.emplace_back(GridBackendUtil::g_NoValueString);

        if (inheritedFrom->Id)
			p_InheritedFromId.emplace_back(*inheritedFrom->Id);
        else
			p_InheritedFromId.emplace_back(GridBackendUtil::g_NoValueString);

        if (inheritedFrom->Name)
			p_InheritedFromName.emplace_back(*inheritedFrom->Name);
        else
			p_InheritedFromName.emplace_back(GridBackendUtil::g_NoValueString);

        if (inheritedFrom->Path)
			p_InheritedFromPath.emplace_back(*inheritedFrom->Path);
        else
			p_InheritedFromPath.emplace_back(GridBackendUtil::g_NoValueString);

        if (inheritedFrom->ShareId)
			p_InheritedFromShareID.emplace_back(*inheritedFrom->ShareId);
        else
			p_InheritedFromShareID.emplace_back(GridBackendUtil::g_NoValueString);

        if (inheritedFrom->SharepointIds)
        {
            if (inheritedFrom->SharepointIds->ListId)
                p_InheritedFromSharepointIdsListID.emplace_back(*inheritedFrom->SharepointIds->ListId);
            else
                p_InheritedFromSharepointIdsListID.emplace_back(GridBackendUtil::g_NoValueString);

            if (inheritedFrom->SharepointIds->ListItemId)
                p_InheritedFromSharepointIdsListItemID.emplace_back(*inheritedFrom->SharepointIds->ListItemId);
            else
                p_InheritedFromSharepointIdsListItemID.emplace_back(GridBackendUtil::g_NoValueString);

            if (inheritedFrom->SharepointIds->ListItemUniqueId)
                p_InheritedFromSharepointIdsListItemUniqueID.emplace_back(*inheritedFrom->SharepointIds->ListItemUniqueId);
            else
                p_InheritedFromSharepointIdsListItemUniqueID.emplace_back(GridBackendUtil::g_NoValueString);

            if (inheritedFrom->SharepointIds->SiteId)
                p_InheritedFromSharepointIdsSiteID.emplace_back(*inheritedFrom->SharepointIds->SiteId);
            else
                p_InheritedFromSharepointIdsSiteID.emplace_back(GridBackendUtil::g_NoValueString);

            if (inheritedFrom->SharepointIds->SiteUrl)
                p_InheritedFromSharepointIdsSiteURL.emplace_back(*inheritedFrom->SharepointIds->SiteUrl);
            else
                p_InheritedFromSharepointIdsSiteURL.emplace_back(GridBackendUtil::g_NoValueString);

            if (inheritedFrom->SharepointIds->WebId)
                p_InheritedFromSharepointIdsWebID.emplace_back(*inheritedFrom->SharepointIds->WebId);
            else
                p_InheritedFromSharepointIdsWebID.emplace_back(GridBackendUtil::g_NoValueString);
        }
        else
        {
            p_InheritedFromSharepointIdsListID.emplace_back(GridBackendUtil::g_NoValueString);
            p_InheritedFromSharepointIdsListItemID.emplace_back(GridBackendUtil::g_NoValueString);
            p_InheritedFromSharepointIdsListItemUniqueID.emplace_back(GridBackendUtil::g_NoValueString);
            p_InheritedFromSharepointIdsSiteID.emplace_back(GridBackendUtil::g_NoValueString);
            p_InheritedFromSharepointIdsSiteURL.emplace_back(GridBackendUtil::g_NoValueString);
            p_InheritedFromSharepointIdsWebID.emplace_back(GridBackendUtil::g_NoValueString);
        }
    }
    else
    {
        p_InheritedFromDriveId.emplace_back(GridBackendUtil::g_NoValueString);
        p_InheritedFromDriveType.emplace_back(GridBackendUtil::g_NoValueString);
        p_InheritedFromId.emplace_back(GridBackendUtil::g_NoValueString);
        p_InheritedFromName.emplace_back(GridBackendUtil::g_NoValueString);
        p_InheritedFromPath.emplace_back(GridBackendUtil::g_NoValueString);
        p_InheritedFromShareID.emplace_back(GridBackendUtil::g_NoValueString);
        p_InheritedFromSharepointIdsListID.emplace_back(GridBackendUtil::g_NoValueString);
        p_InheritedFromSharepointIdsListItemID.emplace_back(GridBackendUtil::g_NoValueString);
        p_InheritedFromSharepointIdsListItemUniqueID.emplace_back(GridBackendUtil::g_NoValueString);
        p_InheritedFromSharepointIdsSiteID.emplace_back(GridBackendUtil::g_NoValueString);
        p_InheritedFromSharepointIdsSiteURL.emplace_back(GridBackendUtil::g_NoValueString);
        p_InheritedFromSharepointIdsWebID.emplace_back(GridBackendUtil::g_NoValueString);
    }
}

void GridDriveItems::fillLink(
	const BusinessPermission& p_Permission, 
	vector<PooledString>& p_LinkApplicationIdentityId, 
	vector<PooledString>& p_LinkApplicationIdentityName, 
	vector<PooledString>& p_LinkType, 
	vector<PooledString>& p_LinkScope, 
	vector<HyperlinkItem>& p_LinkWebHtml, 
	vector<HyperlinkItem>& p_LinkWebUrl)
{
    const auto& link = p_Permission.GetLink();
    if (link)
    {
        if (link->Application)
        {
            if (link->Application->Id)
                p_LinkApplicationIdentityId.emplace_back(*link->Application->Id);
            else
                p_LinkApplicationIdentityId.emplace_back(GridBackendUtil::g_NoValueString);

            if (link->Application->DisplayName)
                p_LinkApplicationIdentityName.emplace_back(*link->Application->DisplayName);
            else
                p_LinkApplicationIdentityName.emplace_back(GridBackendUtil::g_NoValueString);
        }
        else
        {
            p_LinkApplicationIdentityId.emplace_back(GridBackendUtil::g_NoValueString);
            p_LinkApplicationIdentityName.emplace_back(GridBackendUtil::g_NoValueString);
        }
        if (link->Type)
            p_LinkType.emplace_back(*link->Type);
        else
            p_LinkType.emplace_back(GridBackendUtil::g_NoValueString);

		if(link->Scope)
			p_LinkScope.emplace_back(*link->Scope);
        else
            p_LinkScope.emplace_back(GridBackendUtil::g_NoValueString);

        if (link->WebHtml)
            p_LinkWebHtml.emplace_back(HyperlinkItem(*link->WebHtml));
        else
            p_LinkWebHtml.emplace_back(HyperlinkItem(GridBackendUtil::g_NoValueString));

		if (link->WebUrl)
			p_LinkWebUrl.emplace_back(HyperlinkItem(*link->WebUrl));
		else
			p_LinkWebUrl.emplace_back(HyperlinkItem(GridBackendUtil::g_NoValueString));
    }
    else
    {
        p_LinkApplicationIdentityId.emplace_back(GridBackendUtil::g_NoValueString);
        p_LinkApplicationIdentityName.emplace_back(GridBackendUtil::g_NoValueString);
        p_LinkType.emplace_back(GridBackendUtil::g_NoValueString);
        p_LinkScope.emplace_back(GridBackendUtil::g_NoValueString);
        p_LinkWebHtml.emplace_back(HyperlinkItem(GridBackendUtil::g_NoValueString));
        p_LinkWebUrl.emplace_back(HyperlinkItem(GridBackendUtil::g_NoValueString));
    }
}

void GridDriveItems::requestViewPermissions(const std::vector<GridBackendRow*>& p_Rows, const Command::ModuleTask p_TaskType)
{
	vector<GridBackendRow*> rows;

	bool hasPrivate = false;
	for (auto row : p_Rows)
	{
		if (!row->IsCreated() && authorizedByRBAC(row, ID_DRIVEITEMSGRID_VIEWPERMISSIONS))
		{
			rows.emplace_back(row);
			if (row->GetField(m_ColShared).GetValueStr() == g_SharedPrivate)
				hasPrivate = true;
		}
	}

	if (hasPrivate)
	{
		YCodeJockMessageBox dlg(this,
			DlgMessageBox::eIcon_Question,
			YtriaTranslate::Do(GridDriveItems_requestViewPermissions_1, _YLOC("Load Permission Info")).c_str(),
			YtriaTranslate::Do(GridDriveItems_requestViewPermissions_2, _YLOC("Some selected items are private (not shared). Do you want to load their permissions too?")).c_str(),
			YtriaTranslate::Do(GridDriveItems_requestViewPermissions_3, _YLOC("They will only have one permission of type 'owner'.")).c_str(),
			{ { IDYES, YtriaTranslate::Do(GridDriveItems_requestViewPermissions_4, _YLOC("Yes")).c_str() },{ IDNO, YtriaTranslate::Do(GridDriveItems_requestViewPermissions_5, _YLOC("No")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridDriveItems_requestViewPermissions_6, _YLOC("Cancel")).c_str() } },
			IDNO);

		const auto res = dlg.DoModal();
		if (IDNO == res)
			rows.erase(std::remove_if(rows.begin(), rows.end(), [=](GridBackendRow* p_Row) { return p_Row->GetField(m_ColShared).GetValueStr() == g_SharedPrivate; }), rows.end());
		else if (IDCANCEL == res)
			rows.clear();
	}

	if (!rows.empty())
	{
		CommandInfo info;
		info.Data() = GetSubRequestInfo(rows, {});
		info.SetFrame(m_FrameDriveItems);
		info.SetRBACPrivilege(m_FrameDriveItems->GetModuleCriteria().m_Privilege);
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Drive, p_TaskType, info, { this, g_ActionNameSelectedViewPermissions, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
}

const wstring& GridDriveItems::GetNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
	if (isCheckoutStatusDataColumn(p_Col))
	{
		if (isRowWithUnloadedCheckout(p_Row))
			return g_CheckoutNoFieldText;
		return Str::g_EmptyString;
	}
#endif

	ASSERT(nullptr != p_Row && nullptr != p_Col);
	if (nullptr != p_Col && p_Col->HasPresetFlag(g_ColumnsPresetMore)
		&& nullptr != p_Row && p_Row->GetLParam() == g_HiddenOwnerLparam)
		return g_OwnerOnlyText;

	return GridDriveItemsBaseClass::GetNoFieldText(p_Row, p_Col);
}

wstring GridDriveItems::GetNoFieldTooltip(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
	if (isCheckoutStatusDataColumn(p_Col))
	{
		if (isRowWithUnloadedCheckout(p_Row))
			return g_CheckoutNoFieldTooltip;
		return _YTEXT("");
	}
#endif

	ASSERT(nullptr != p_Row && nullptr != p_Col);
	if (p_Col->HasPresetFlag(g_ColumnsPresetMore) && p_Row->GetLParam() == g_HiddenOwnerLparam)
		return YtriaTranslate::Do(GridDriveItems_GetNoFieldTooltip_1, _YLOC("To see this information, use the 'Show Owner Permissions' button")).c_str();

	return GridDriveItemsBaseClass::GetNoFieldTooltip(p_Row, p_Col);
}

bool GridDriveItems::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return
#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
		isCheckoutStatusDataColumn(p_Col)
		? isRowWithUnloadedCheckout(p_Row)
		:
#endif
		(GridDriveItemsBaseClass::IsRowValidForNoFieldText(p_Row, p_Col)
			&& (!p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore)
				|| m_MetaIDsMoreLoaded.end() == m_MetaIDsMoreLoaded.find(p_Row->GetField(GetColMetaID()).GetValueStr()))
			)
		;
}

#if USE_SPECIFIC_LOAD_FOR_CHECKOUT_STATUS
bool GridDriveItems::isRowWithUnloadedCheckout(const GridBackendRow* p_Row) const
{
	return (GridUtil::IsBusinessDriveItem(p_Row) || GridUtil::IsBusinessDriveItemFolder(p_Row))
		&& !HasRowCheckoutStatus(p_Row)
		;
}
#endif

DriveSubInfoIds GridDriveItems::GetSubRequestInfo(const std::vector<GridBackendRow *>& p_Rows, const O365IdsContainer& p_ItemIds)
{
	DriveSubInfoIds ids;

    for (auto& row : p_Rows)
    {
		ASSERT(nullptr != row);
		if (nullptr != row && (GridUtil::IsBusinessDriveItem(row) || GridUtil::IsBusinessDriveItemFolder(row)))
		{
			std::tuple<PooledString, PooledString, PooledString, PooledString> currentId;

			auto metaId = wstring(row->GetField(GetColMetaID()).GetValueStr());
			if (!metaId.empty())
				std::get<0>(currentId) = metaId;

			auto strDriveId = wstring(row->GetField(m_ColMetaDriveId).GetValueStr());
			if (!strDriveId.empty())
				std::get<1>(currentId) = strDriveId;

			auto strId = wstring(row->GetField(GetColId()).GetValueStr());
			ASSERT(!strId.empty());
			if (!strId.empty())
				std::get<2>(currentId) = strId;

			auto strFilename = wstring(row->GetField(m_ColName).GetValueStr());
			ASSERT(!strFilename.empty());
			if (!strFilename.empty())
				std::get<3>(currentId) = strFilename;

            if (p_ItemIds.empty() || p_ItemIds.find(strId) != p_ItemIds.end())
			    ids.insert(currentId);
		}
    }

    return ids;
}

GridBackendColumn* GridDriveItems::GetColumnPermissionId() const
{
    return m_ColPermissionId;
}

GridBackendColumn* GridDriveItems::GetColumnRole() const
{
    return m_ColRole;
}

void GridDriveItems::SetShowOwnerPermissions(bool p_Val)
{
	m_ShowOwnerPermissions = p_Val;
}

GridDriveItems::DriveItemPermissionInfo::DriveItemPermissionInfo(GridDriveItems& p_Grid, const wstring& p_PermissionId, GridBackendRow* p_Row)
{
	m_Row = p_Row;
	
	ASSERT(nullptr != m_Row);
	if (nullptr != m_Row)
	{
		m_OriginId		= m_Row->GetField(p_Grid.GetColMetaID()).GetValueStr();
		m_DriveId		= m_Row->GetField(p_Grid.m_ColMetaDriveId).GetValueStr();
		m_DriveItemId	= m_Row->GetField(p_Grid.GetColId()).GetValueStr();

		std::vector<PooledString>* permissionIds = m_Row->GetField(p_Grid.GetColumnPermissionId()).GetValuesMulti<PooledString>();
		if (nullptr != permissionIds)
		{
			bool found = false;
			size_t index = 0;
			for (size_t i = 0; index < permissionIds->size() && !found; ++i)
			{
				if (permissionIds->operator[](i) == p_PermissionId)
				{
					index = i;
					found = true;
				}
			}
			ASSERT(found);
			if (found)
				m_PermissionId = p_PermissionId;
		}
		else // Already exploded row
		{
			m_PermissionId = p_PermissionId;
		}
	}
}

template <class BusinessType>
void GridDriveItems::buildTreeViewImpl(const O365DataMap<BusinessType, BusinessDriveItemFolder>& p_DriveItems, const vector<O365UpdateOperation>& p_Updates, bool p_UpdateGrid, bool p_FullPurge, const map<std::pair<PooledString, PooledString>, vector<BusinessPermission>>& p_Permissions, const map<std::pair<PooledString, PooledString>, CheckoutStatus>& p_CheckoutStatuses)
{
	for (const auto& val : p_Permissions)
		m_AllPermissions[val.first] = val.second;

	// Full purge with updates will fail.
	ASSERT(!p_FullPurge || p_Updates.empty());

	const uint32_t options = (p_UpdateGrid ? GridUpdaterOptions::UPDATE_GRID : 0)
		| (p_FullPurge || IsMyData() ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
		| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		| GridUpdaterOptions::REFRESH_PROCESS_DATA
		/*| GridUpdaterOptions::PROCESS_LOADMORE*/
		| (!p_Updates.empty() ? GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG : 0)
		;

	GridUpdater updater(*this, GridUpdaterOptions(options), p_Updates);
	GridIgnoreModification ignoramus(*this);
	//DlgLoading::RunModal(_T("Loadind data into grid"), this, [this, &p_DriveItems, &p_Permissions, &p_CheckoutStatuses, &updater](std::shared_ptr<DlgLoading> p_Dlg)
	//	{
			for (const auto& keyVal : p_DriveItems)
			{
				GridBackendRow* parentRow = addParentRow(keyVal.first, updater);
				ASSERT(nullptr != parentRow);
				if (nullptr != parentRow)
				{
					if (!IsMyData()) // Row will be deleted below
					{
						updater.GetOptions().AddRowWithRefreshedValues(parentRow);
						updater.GetOptions().AddPartialPurgeParentRow(parentRow);
					}

					parentRow->SetHierarchyParentToHide(true);

					if (!keyVal.first.HasFlag(BusinessObject::CANCELED))
					{
						if (keyVal.second.GetError())
						{
							updater.OnLoadingError(parentRow, *keyVal.second.GetError());
						}
						else
						{
							// Clear previous error
							if (parentRow->IsError())
								parentRow->ClearStatus();
							buildTreeViewGenericImpl(keyVal.second, parentRow, updater);
						}
					}
					AddRoleDelegationFlag(parentRow, keyVal.first);

					if (IsMyData())
					{
						ASSERT(m_MyDataMeHandler);
						if (m_MyDataMeHandler)
							m_MyDataMeHandler->GetRidOfMeRow(*this, parentRow);
					}
				}
			}

			loadPermissions(p_Permissions, updater);
			if (nullptr != m_ColCheckoutUser)
				loadCkeckoutStatuses(p_CheckoutStatuses, updater);
		//});
}

/*
RBAC_Privilege GridDriveItems::GetPrivilegeEdit() const
{
	return	m_Origin == Origin::DeletedUser || m_Origin == Origin::User ?	RBAC_Privilege::RBAC_USER_ONEDRIVE_RENAME : 
			m_Origin == Origin::Group ?										RBAC_Privilege::RBAC_GROUP_ONEDRIVE_RENAME :
																			RBAC_Privilege::RBAC_SITE_ONEDRIVE_RENAME;
}

RBAC_Privilege GridDriveItems::GetPrivilegeDelete() const
{
	return	m_Origin == Origin::DeletedUser || m_Origin == Origin::User ?	RBAC_Privilege::RBAC_USER_ONEDRIVE_DELETE : 
			m_Origin == Origin::Group ?										RBAC_Privilege::RBAC_GROUP_ONEDRIVE_DELETE :
																			RBAC_Privilege::RBAC_SITE_ONEDRIVE_DELETE;
}
*/

bool GridDriveItems::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	return m_TemplateUsers && m_TemplateUsers->GetSnapshotValue(p_Value, p_Field, p_Column)
		|| m_TemplateGroups && m_TemplateGroups->GetSnapshotValue(p_Value, p_Field, p_Column)
		|| GridDriveItemsBaseClass::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridDriveItems::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (m_TemplateUsers && m_TemplateUsers->LoadSnapshotValue(p_Value, p_Row, p_Column)
		|| m_TemplateGroups && m_TemplateGroups->LoadSnapshotValue(p_Value, p_Row, p_Column))
		return true;

	if (&p_Column == m_ColPublicationLevel)
	{
		if (p_Value == _YTEXT("published") || p_Value == _YTEXT("checkout"))
		{
			p_Row.AddField(p_Value, &p_Column, m_PublicationLevelIcons[p_Value]);
			return true;
		}

		ASSERT(false);
	}
	else if (&p_Column == m_ColCheckoutUser)
	{
		p_Row.AddField(p_Value, &p_Column, !p_Value.empty() ? m_PublicationLevelIcons[_YTEXT("checkout")] : NO_ICON);
		return true;
	}
	else if (&p_Column == m_ColOneDrivePath)
	{
		ASSERT(p_Row.HasField(GetColumnObjectType()) && !p_Row.HasField(GetColumnFileType()));
		SetRowFileType(&p_Row, p_Value, GridUtil::IsBusinessDriveItemFolder(&p_Row) || GridUtil::IsBusinessDriveItemNotebook(&p_Row));
		return true;
	}	

	return GridDriveItemsBaseClass::LoadSnapshotValue(p_Value, p_Row, p_Column);
}