#pragma once

#include "IRequester.h"
#include "BusinessSite.h"
#include "IRequestLogger.h"

class ChannelSiteRequester : public IRequester
{
public:
	ChannelSiteRequester(const wstring& p_TeamId, const wstring& p_channelId, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const BusinessSite& GetData() const;

private:
	wstring m_TeamId;
	wstring m_ChannelId;
	std::shared_ptr<IRequestLogger> m_Logger;
	std::shared_ptr<BusinessSite> m_Site;
};

