#pragma once

#include "CacheGrid.h"

class GridFrameBase;
class WindowsGrid : public CacheGrid
{
public:
	WindowsGrid();
	virtual ~WindowsGrid() override;

	void FillGrid();
	void ShowSelected(UINT p_ShowFlag);
	void CloseSelected(bool& p_ClosedParentFrame);
	void BringSelectedToForeground();
	void BringToForeground(HWND p_Frame);

	GridFrameBase* GetGridFrameParent();

protected:
	virtual void customizeGrid() override;
	virtual void OnCustomDoubleClick() override;

private:
	GridBackendColumn*	m_CaptionColumn;
	GridBackendColumn*	m_SessionColumn;
	GridBackendColumn*	m_ModuleColumn;
	GridBackendColumn*	m_DateCreatedColumn;
	GridBackendColumn*	m_IsConnected;
	GridBackendColumn*	m_LastRefreshDate;

	GridFrameBase*		m_GridFrameParent;

	int m_ConnectedIconIndex;
	int m_DicsconnectedIconIndex;
};