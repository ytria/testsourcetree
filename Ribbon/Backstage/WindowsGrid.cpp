#include "WindowsGrid.h"

#include "CommandDispatcher.h"
#include "GridFrameBase.h"
#include "WindowsListUpdater.h"

WindowsGrid::WindowsGrid()
	: CacheGrid(0, /*GridBackendUtil::CREATECOLORG | */GridBackendUtil::CREATESORTING)
	, m_GridFrameParent(nullptr)
	, m_CaptionColumn(nullptr)
	, m_SessionColumn(nullptr)
	, m_ModuleColumn(nullptr)
	, m_DateCreatedColumn(nullptr)
	, m_IsConnected(nullptr)
	, m_LastRefreshDate(nullptr)
{}

WindowsGrid::~WindowsGrid()
{}

void WindowsGrid::customizeGrid()
{
	SetAutomationName(_YTEXT("BackstageWindowsGrid"));
	AutomationRecordingEnable(false);

	m_ConnectedIconIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_CONNECTED_BMP));
	m_DicsconnectedIconIndex = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_DISCONNECTED_BMP));

	m_IsConnected		= AddColumnIcon(_YUID("WG5"), YtriaTranslate::Do(WindowsGrid_customizeGrid_1, _YLOC("State")).c_str(), GridBackendUtil::g_DummyString);
	m_DateCreatedColumn = AddColumnDate(_YUID("WG1"), YtriaTranslate::Do(WindowsGrid_customizeGrid_2, _YLOC("Creation")).c_str(), GridBackendUtil::g_DummyString, HIDPI_XW(88));
	m_LastRefreshDate	= AddColumnDate(_YUID("WG6"), YtriaTranslate::Do(WindowsGrid_customizeGrid_3, _YLOC("Refresh")).c_str(), GridBackendUtil::g_DummyString, HIDPI_XW(88));
	m_ModuleColumn		= AddColumn(_YUID("WG2"), YtriaTranslate::Do(WindowsGrid_customizeGrid_4, _YLOC("Module")).c_str(), GridBackendUtil::g_DummyString, HIDPI_XW(80));		
	m_SessionColumn		= AddColumn(_YUID("WG3"), YtriaTranslate::Do(WindowsGrid_customizeGrid_5, _YLOC("Current Session")).c_str(), GridBackendUtil::g_DummyString);	
	m_CaptionColumn		= AddColumn(_YUID("WG4"), YtriaTranslate::Do(WindowsGrid_customizeGrid_6, _YLOC("Caption")).c_str(), GridBackendUtil::g_DummyString);
	
	m_SessionColumn->SetAutoResizeWithGridResize(true);
	m_CaptionColumn->SetAutoResizeWithGridResize(true);
	
	AddSorting(m_DateCreatedColumn, GridBackendUtil::DESC);

	m_GridFrameParent = MFCUtil::FindFirstParentOfType<GridFrameBase>(this);
}

void WindowsGrid::OnCustomDoubleClick()
{
	BringSelectedToForeground();
}

void WindowsGrid::FillGrid()
{
	for (const auto& bySession : CommandDispatcher::GetInstance().GetAllModules())
	{
		const auto& session = bySession.first;
		for (const auto& byTarget : bySession.second)
		{
			for (const auto& frame : byTarget.second->GetGridFrames())
			{
				// Temporary way to handle new back/forward system. We should maybe do it differently.
				if (!::IsWindow(frame->GetSafeHwnd()) || !frame->IsWindowVisible())
					continue;

				GridBackendRow* row = AddRow(false);
				if (!frame->IsSnapshotMode())
				{
					ASSERT(session.m_EmailOrAppId == frame->GetSessionInfo().GetEmailOrAppId());
					row->AddField(frame->GetSessionInfo().GetSessionName(), m_SessionColumn);
				}
				row->AddField(CommandDispatcher::getModuleName(static_cast<Command::ModuleTarget>(byTarget.first)), m_ModuleColumn);
				row->AddField(frame->GetFrameTitle(), m_CaptionColumn);
				row->AddField(frame->GetCreationDate(), m_DateCreatedColumn);

				{
					int icon = m_DicsconnectedIconIndex;
					wstring state = YtriaTranslate::Do(WindowsGrid_FillGrid_1, _YLOC("Disconnected")).c_str();
					if (frame->IsConnected())
					{
						state = YtriaTranslate::Do(WindowsGrid_FillGrid_2, _YLOC("Connected")).c_str();
						icon = m_ConnectedIconIndex;
					}

					row->AddField(state, m_IsConnected).SetIcon(icon);
				}

				row->AddField(frame->GetLastRefreshDate(), m_LastRefreshDate);
				row->SetLParam((LPARAM)frame->GetSafeHwnd());
				if (frame == m_GridFrameParent)
				{
					// FIXME: Find a better way to identify current frame
					row->SetColor(RGB(0xed, 0xe7, 0xf6));
				}
			}
		}
	}
}

void WindowsGrid::ShowSelected(UINT p_ShowFlag)
{
	const auto& selectedRows = GetSelectedRows();
	for (const auto& row : selectedRows)
	{
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			const HWND frameHwnd = reinterpret_cast<HWND>(row->GetLParam());
			if (::IsWindow(frameHwnd))
			{
				CWnd* cWnd = CWnd::FromHandle(frameHwnd);
				if (nullptr != cWnd)
					cWnd->ShowWindow(p_ShowFlag);
			}
		}
	}
}

void WindowsGrid::CloseSelected(bool& p_ClosedParentFrame)
{
	bool shouldCloseThis = false;

	// First, close all the frames.
	vector< GridBackendRow* > selectedRows;
	GetSelectedRows(selectedRows);
	for (const auto& row : selectedRows)
	{
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			const HWND frameHwnd = reinterpret_cast<HWND>(row->GetLParam());
			if (::IsWindow(frameHwnd))
			{
				if (nullptr != m_GridFrameParent && frameHwnd == m_GridFrameParent->m_hWnd)
				{
					// We'll close the parent frame after all the others, otherwise selectedRows would become invalid.
					shouldCloseThis = true;
					continue;
				}

				GridFrameBase* pPanel = dynamic_cast<GridFrameBase*>(CWnd::FromHandle(frameHwnd));
				if (nullptr != pPanel)
				{
					// Close the frame making sure that this windows list is NOT updated. Causes crash.
					pPanel->SendMessage(WM_CLOSE, GridFrameBase::g_NoWindowsListUpdate, 0);
				}
			}
		}
	}

	// Second, remove the selected rows in this list.
	RemoveRows(selectedRows, true);

	// Now, update this list.
	WindowsListUpdater::GetInstance().Update();

	if (shouldCloseThis)
	{
		p_ClosedParentFrame = true;
		m_GridFrameParent->SendMessage(WM_CLOSE, 0, 0);
	}
}

void WindowsGrid::BringSelectedToForeground()
{
	const auto& selectedRows = GetSelectedRows();
	for (const auto& row : selectedRows)
	{
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			const HWND frameHwnd = reinterpret_cast<HWND>(row->GetLParam());
			if (::IsWindow(frameHwnd))
				BringToForeground(frameHwnd);
		}
	}
}

void WindowsGrid::BringToForeground(HWND p_Frame)
{
	WINDOWPLACEMENT wd;
	UINT showFlag = SW_SHOWNORMAL;
	if (::GetWindowPlacement(p_Frame, &wd))
	{
		if (wd.showCmd == SW_SHOWMAXIMIZED || wd.showCmd == SW_SHOWMINIMIZED && WPF_RESTORETOMAXIMIZED == (wd.flags & WPF_RESTORETOMAXIMIZED))
			showFlag = SW_SHOWMAXIMIZED;
	}

	CWnd* cWnd = CWnd::FromHandle(p_Frame);
	if (nullptr != cWnd)
	{
		cWnd->ShowWindow(showFlag);
		cWnd->SetForegroundWindow();
	}
}

GridFrameBase* WindowsGrid::GetGridFrameParent()
{
	return m_GridFrameParent;
}
