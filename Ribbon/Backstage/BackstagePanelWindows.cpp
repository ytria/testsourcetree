#include "BackstagePanelWindows.h"

#include "CommandDispatcher.h"
#include "GridFrameBase.h"
#include "MainFrame.h"
#include "WindowsListUpdater.h"

#include "YtriaTranslate.h"

BEGIN_MESSAGE_MAP(BackstagePanelWindows, BackstagePanelWithGrid<WindowsGrid>)
	ON_BN_CLICKED(ID_BACKSTAGE_WINDOWS_ACTIVATE,	OnActivateSelected)
	ON_BN_CLICKED(ID_BACKSTAGE_WINDOWS_CLOSE,		OnCloseSelected)
	ON_BN_CLICKED(ID_BACKSTAGE_WINDOWS_MAX,			OnMaxSelected)
	ON_BN_CLICKED(ID_BACKSTAGE_WINDOWS_MIN,			OnMinSelected)
	ON_BN_CLICKED(ID_BACKSTAGE_WINDOWS_RESTORE,		OnRestoreSelected)
	ON_BN_CLICKED(ID_BACKSTAGE_WINDOWS_MAINWIN,		OnGotoMainWindow)
	ON_CBN_DROPDOWN(ID_BACKSTAGE_WINDOWS_ARRANGE,	OnArrange)
	ON_WM_DESTROY()
END_MESSAGE_MAP()

BackstagePanelWindows::BackstagePanelWindows(UINT resourceID)
	: BackstagePanelWithGrid<WindowsGrid>(resourceID)
{
	m_Grid = std::make_shared<WindowsGrid>();

	// Fixes Bug #180926.LS.008C0A and Bug #180816.RO.0089D6
	m_Grid->SetGrabFocus(false);
}

BackstagePanelWindows::~BackstagePanelWindows()
{
}

void BackstagePanelWindows::OnDestroy()
{
	BackstagePanelWithGrid<WindowsGrid>::OnDestroy();

	WindowsListUpdater::GetInstance().Remove(this);
}

void BackstagePanelWindows::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelWithGrid<WindowsGrid>::DoDataExchange(pDX);

	DDX_Control(pDX, ID_BACKSTAGE_WINDOWS_ACTIVATE, m_btnActivateSelected);
	DDX_Control(pDX, ID_BACKSTAGE_WINDOWS_CLOSE, m_btnCloseSelected);
	DDX_Control(pDX, ID_BACKSTAGE_WINDOWS_MAX, m_btnMaxSelected);
	DDX_Control(pDX, ID_BACKSTAGE_WINDOWS_MIN, m_btnMinSelected);
	DDX_Control(pDX, ID_BACKSTAGE_WINDOWS_RESTORE, m_btnRestoreSelected);

	// We don't need the goto main window button in the main window.
	if (IDD_FOR_MAIN != m_nDialogID)
		DDX_Control(pDX, ID_BACKSTAGE_WINDOWS_MAINWIN, m_btnGotoMainWindow);

	DDX_Control(pDX, ID_BACKSTAGE_WINDOWS_ARRANGE, m_btnArrange);
}

BOOL BackstagePanelWindows::OnInitDialog()
{
	BackstagePanelWithGrid<WindowsGrid>::OnInitDialog();

	setCaption(YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_1, _YLOC("Open Windows")).c_str());

	WindowsListUpdater::GetInstance().Add(this);

	m_Grid->AddSelectionObserver(this);

	{
		UINT ids[]{ ID_BACKSTAGE_WINDOWS_ACTIVATE	,
					ID_BACKSTAGE_WINDOWS_CLOSE		,
					ID_BACKSTAGE_WINDOWS_MIN		,
					ID_BACKSTAGE_WINDOWS_MAX		,
					ID_BACKSTAGE_WINDOWS_RESTORE	,
					ID_BACKSTAGE_WINDOWS_ARRANGE };
		m_imagelist.SetIcons(IDB_BACKSTAGE_WINDOWS_BUTTON_ICONS
			, ids
			, 6
			, CSize(0, 0)
			, xtpImageNormal);
	}

	SetupButton(m_btnActivateSelected, YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_2, _YLOC("Bring to Front")).c_str(), YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_8, _YLOC("Bring the selected window to the front.")).c_str(), m_imagelist);
	SetupButton(m_btnCloseSelected, YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_3, _YLOC("Close")).c_str(), YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_9, _YLOC("Close all selected windows.")).c_str(), m_imagelist);
	SetupButton(m_btnMaxSelected, YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_4, _YLOC("Maximize")).c_str(), YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_10, _YLOC("Maximize all selected windows.")).c_str(), m_imagelist);
	SetupButton(m_btnMinSelected, YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_5, _YLOC("Minimize")).c_str(), YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_11, _YLOC("Minimize all selected windows.")).c_str(), m_imagelist);
	SetupButton(m_btnRestoreSelected, YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_6, _YLOC("Restore")).c_str(), YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_12, _YLOC("Restore all selected windows.")).c_str(), m_imagelist);

	if (IDD_FOR_MAIN != m_nDialogID)
	{
		UINT ids[]{ ID_BACKSTAGE_WINDOWS_MAINWIN };
		BOOL res = m_imagelist.SetIcons(IDB_MAIN_WINDOW_QUICKACCESS, ids, 1, CSize(0, 0), xtpImageNormal);
		ASSERT(res);
		SetupButton(m_btnGotoMainWindow, YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_15, _YLOC("Main Window")).c_str(), YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_14, _YLOC("Bring the main window to front.")).c_str(), m_imagelist);
		m_btnGotoMainWindow.EnableWindow(TRUE);
	}

	// Dropdown menu for arrangement selection.
	m_btnArrange.SetPushButtonStyle(xtpButtonDropDown);
	SetupButton(m_btnArrange, YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_7, _YLOC("Arrange")).c_str(), YtriaTranslate::Do(BackstagePanelWindows_OnInitDialog_13, _YLOC("Choose the arrangement pattern for open windows.")).c_str(), m_imagelist);	

	UINT nIDs[] = {	ID_BACKSTAGE_WINDOWS_MENU_CASCADE,
					ID_BACKSTAGE_WINDOWS_MENU_HORIZONTAL,
					ID_BACKSTAGE_WINDOWS_MENU_VERTICAL,
					ID_BACKSTAGE_WINDOWS_MENU_TILEHORIZONTAL,
					ID_BACKSTAGE_WINDOWS_MENU_TILEVERTICALLY };
	XTPImageManager()->SetIcons(IDB_BACKSTAGE_WINDOWS_ARRANGE_ICONS, nIDs, 5, CSize(0, 0), xtpImageNormal);

	if (IDD_FOR_MAIN != m_nDialogID)
		SetResize(ID_BACKSTAGE_WINDOWS_MAINWIN, CXTPResizePoint(0, 0), CXTPResizePoint(.5f, 0));

	SetResize(ID_BACKSTAGE_WINDOWS_ACTIVATE, CXTPResizePoint(0, 0), CXTPResizePoint(.5f, 0));
	SetResize(ID_BACKSTAGE_WINDOWS_CLOSE, CXTPResizePoint(0, 0), CXTPResizePoint(.5f, 0));
	SetResize(ID_BACKSTAGE_WINDOWS_MAX, CXTPResizePoint(0, 0), CXTPResizePoint(.5f, 0));
	SetResize(ID_BACKSTAGE_WINDOWS_MIN, CXTPResizePoint(0, 0), CXTPResizePoint(.5f, 0));
	SetResize(ID_BACKSTAGE_WINDOWS_RESTORE, CXTPResizePoint(0, 0), CXTPResizePoint(.5f, 0));
	SetResize(ID_BACKSTAGE_WINDOWS_ARRANGE, CXTPResizePoint(0, 0), CXTPResizePoint(.5f, 0));

	RemoveResize(IDC_BACKSTAGE_GRID);
	SetResize(IDC_BACKSTAGE_GRID, CXTPResizePoint(.5f, 0), CXTPResizePoint(1.f, 1.f));

	UpdateWindowList();

	return TRUE;
}

BOOL BackstagePanelWindows::OnSetActive()
{
	BackstagePanelWithGrid<WindowsGrid>::OnSetActive();

	UpdateWindowList();

	return TRUE;
}

void BackstagePanelWindows::SelectionChanged(const CacheGrid& grid)
{
	vector< GridBackendRow* > SelRows;
	grid.GetSelectedNonGroupRows(SelRows);
	BOOL Enable = SelRows.size() > 0;
	m_btnActivateSelected.EnableWindow(Enable);
	m_btnCloseSelected.EnableWindow(Enable);
	m_btnMaxSelected.EnableWindow(Enable);
	m_btnMinSelected.EnableWindow(Enable);
	m_btnRestoreSelected.EnableWindow(Enable);
	m_btnArrange.EnableWindow(Enable);
}

void BackstagePanelWindows::UpdateWindowList()
{
	// Called by WindowsListUpdater::GetInstance().Update()
	ASSERT(m_Grid);
	m_Grid->RemoveAllRows();
	m_Grid->FillGrid();
	m_Grid->UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

	SelectionChanged(*m_Grid.get());

	// TODO: Reselect rows.
}

void BackstagePanelWindows::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelWithGrid<WindowsGrid>::SetTheme(nTheme);

	m_btnActivateSelected.SetTheme(nTheme);
	m_btnCloseSelected.SetTheme(nTheme);
	m_btnMaxSelected.SetTheme(nTheme);
	m_btnMinSelected.SetTheme(nTheme);
	m_btnRestoreSelected.SetTheme(nTheme);
	m_btnGotoMainWindow.SetTheme(nTheme);
	m_btnArrange.SetTheme(nTheme);
}

void BackstagePanelWindows::OnActivateSelected()
{
	ASSERT(m_Grid);
	m_Grid->BringSelectedToForeground();
}

void BackstagePanelWindows::OnCloseSelected()
{
	ASSERT(m_Grid);

	bool closedThis = false;
	m_Grid->CloseSelected(closedThis);

	if (!closedThis)
	{
		m_Grid->UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		SelectionChanged(*m_Grid.get());
	}
}

void BackstagePanelWindows::OnMaxSelected()
{
	ASSERT(m_Grid);
	if (m_Grid)
		m_Grid->ShowSelected(SW_MAXIMIZE);
}

void BackstagePanelWindows::OnMinSelected()
{
	ASSERT(m_Grid);
	if (m_Grid)
		m_Grid->ShowSelected(SW_MINIMIZE);
}

void BackstagePanelWindows::OnRestoreSelected()
{
	ASSERT(m_Grid);
	if (m_Grid)
		m_Grid->ShowSelected(SW_RESTORE);
}

void BackstagePanelWindows::OnGotoMainWindow()
{
	ASSERT(m_Grid);
	m_Grid->BringToForeground(AfxGetApp()->m_pMainWnd->GetSafeHwnd());
}

void BackstagePanelWindows::OnArrange()
{
	auto frame = MFCUtil::FindFirstParentOfType<CXTPFrameWnd>(this);

	CXTPPopupBar* pPopupBar = CXTPPopupBar::CreatePopupBar(frame->GetCommandBars());
	pPopupBar->SetDefaultButtonStyle(xtpButtonCaptionAndDescription);
	pPopupBar->SetShowGripper(FALSE);
	pPopupBar->SetIconSize(CSize(32, 32));

	auto control = pPopupBar->GetControls()->Add(xtpControlButton, ID_BACKSTAGE_WINDOWS_MENU_CASCADE);
	control->SetCaption(YtriaTranslate::Do(BackstagePanelWindows_OnArrange_1, _YLOC("Cascade")).c_str());
	control->SetEnabled(TRUE);
	pPopupBar->GetControls()->Add(xtpControlButton, ID_BACKSTAGE_WINDOWS_MENU_HORIZONTAL)->SetCaption(YtriaTranslate::Do(BackstagePanelWindows_OnArrange_2, _YLOC("Horizontal")).c_str());
	pPopupBar->GetControls()->Add(xtpControlButton, ID_BACKSTAGE_WINDOWS_MENU_VERTICAL)->SetCaption(YtriaTranslate::Do(BackstagePanelWindows_OnArrange_3, _YLOC("Vertical")).c_str());
	pPopupBar->GetControls()->Add(xtpControlButton, ID_BACKSTAGE_WINDOWS_MENU_TILEHORIZONTAL)->SetCaption(YtriaTranslate::Do(BackstagePanelWindows_OnArrange_4, _YLOC("Tile Horizontally")).c_str());
	pPopupBar->GetControls()->Add(xtpControlButton, ID_BACKSTAGE_WINDOWS_MENU_TILEVERTICALLY)->SetCaption(YtriaTranslate::Do(BackstagePanelWindows_OnArrange_5, _YLOC("Tile Vertically")).c_str());

	CXTPWindowRect rcButton(&m_btnArrange);

	// Note: As is, the menu doesn't post command messages but just return the result (as a DoModal).
	// We could remove the flags to  make it more automatic, but the ON_COMMAND and ON_UPDATE_CONMMAND will be triggered
	// in the parent frame (commandbars' site), not very handy for us.
	const UINT nID = CXTPCommandBars::TrackPopupMenu(pPopupBar, TPM_RETURNCMD | TPM_NONOTIFY, rcButton.left, rcButton.bottom, this);
	switch (nID)
	{
	case ID_BACKSTAGE_WINDOWS_MENU_CASCADE:
		DoCascade();
		break;
	case ID_BACKSTAGE_WINDOWS_MENU_HORIZONTAL:
		DoHorizontal();
		break;
	case ID_BACKSTAGE_WINDOWS_MENU_VERTICAL:
		DoVertical();
		break;
	case ID_BACKSTAGE_WINDOWS_MENU_TILEHORIZONTAL:
		DoTileHorizontal();
		break;
	case ID_BACKSTAGE_WINDOWS_MENU_TILEVERTICALLY:
		DoTileVertically();
		break;
	default:
		break;
	}

	pPopupBar->InternalRelease();
}

void BackstagePanelWindows::DoCascade()
{
	CascadeModules();
}

void BackstagePanelWindows::DoHorizontal()
{
	ArrangeModules(MDITILE_HORIZONTAL);
}

void BackstagePanelWindows::DoVertical()
{
	ArrangeModules(MDITILE_VERTICAL);
}

void BackstagePanelWindows::DoTileHorizontal()
{
	TileModules(MDITILE_HORIZONTAL);
}

void BackstagePanelWindows::DoTileVertically()
{
	TileModules(MDITILE_VERTICAL);
}

void BackstagePanelWindows::ArrangeModules(const UINT p_mdiTile)
{
	// The currently selected rows.
	// LParam contains the HWND of the frame.
	const auto& selectedRows = m_Grid->GetSelectedRows();

	CRect DeskRect;
	GetMainMonitorWorkingArea(DeskRect);

	CWnd* pInsertAfter = AfxGetApp()->m_pMainWnd;

	// There must be at least one frame.
	const auto nbChild = static_cast<int>(selectedRows.size());
	ASSERT(0 < nbChild);
	if (0 < nbChild)
	{
		int newHeight = DeskRect.Height() / nbChild;
		int newWidth = DeskRect.Width() / nbChild;
		int count = 0, x = DeskRect.left, y = DeskRect.top;
		for (const auto& row : selectedRows)
		{
			const HWND frameHwnd = reinterpret_cast<HWND>(row->GetLParam());
			if (::IsWindow(m_hWnd))
			{
				GridFrameBase* aChildFrame = dynamic_cast<GridFrameBase*>(CWnd::FromHandle(frameHwnd));
				if (nullptr != aChildFrame)
				{
					aChildFrame->SetIsBeingAutomaticallyArrangedOrTitled(true);
					if (aChildFrame->IsIconic())
						aChildFrame->ShowWindow(SW_RESTORE);

					switch (p_mdiTile)
					{
						case MDITILE_HORIZONTAL:
							newWidth = DeskRect.Width();
							x = 0;
							y = count * newHeight;
							break;

						case MDITILE_VERTICAL:
							newHeight = DeskRect.Height();
							x = count * newWidth;
							y = 0;
							break;

						default:
							ASSERT(FALSE);
							break;
					}

					::MoveWindow(aChildFrame->GetSafeHwnd(), x, y, newWidth, newHeight, true);
					aChildFrame->SetIsBeingAutomaticallyArrangedOrTitled(false);
					++count;
				}
			}
		}
	}
}

void BackstagePanelWindows::TileModules(const UINT p_mdiTile)
{
	const auto& selectedRows = m_Grid->GetSelectedRows();
	const auto nbChild = static_cast<int>(selectedRows.size());
	ASSERT(0 < nbChild);
	if (0 < nbChild)
	{
		// Values needed to reorganize windows
		const int aNb1 = nbChild == 0 ? 1 : (int)sqrt((float)nbChild);
		const int aNb2 = nbChild == 0 ? 1 : nbChild / aNb1;
		const int aNb3 = nbChild % aNb1;

		CRect DeskRect;
		GetMainMonitorWorkingArea(DeskRect);

		CWnd* pInsertAfter = AfxGetApp()->m_pMainWnd;

		// Calculating dimensions for new windows
		if (MDITILE_HORIZONTAL == p_mdiTile)
		{
			// In 'Horizontal Tile':
			// aNb1: Is the number of columns
			// aNb2: Is the number of rows
			// aNb3: Is the number of additional windows in the last row.

			int aChildHeight = DeskRect.Height() / aNb2;
			int aFirstRowsChildWidth = DeskRect.Width() / aNb1;
			int aLastRowChildWidth = DeskRect.Width() / (aNb1 + aNb3);
			int aCurrentWidth = aFirstRowsChildWidth;
			int count = 0;
			int x = DeskRect.left;
			int y = DeskRect.top;
			for (const auto& row : selectedRows)
			{
				// LParam contains the HWND of the frame.
				const HWND frameHwnd = reinterpret_cast<HWND>(row->GetLParam());
				if (::IsWindow(m_hWnd))
				{
					GridFrameBase* aChildFrame = dynamic_cast<GridFrameBase*>(CWnd::FromHandle(frameHwnd));
					if (nullptr != aChildFrame)
					{
						aChildFrame->SetIsBeingAutomaticallyArrangedOrTitled(true);
						if (aChildFrame->IsIconic())
							aChildFrame->ShowWindow(SW_RESTORE);

						// See if we start the last row
						if (aNb1 * (aNb2 - 1) == count)
							aCurrentWidth = aLastRowChildWidth;

						// If a new row must be built
						if (0 == (count % aNb1) && aNb1 * (aNb2 - 1) >= count)
						{
							if (0 < count)
							{
								x = 0;
								y += aChildHeight;
							}
						}
						else
						{
							x += aCurrentWidth;
						}

						aChildFrame->SetWindowPos(pInsertAfter, x, y, aCurrentWidth, aChildHeight, SWP_FRAMECHANGED);
						aChildFrame->SetIsBeingAutomaticallyArrangedOrTitled(false);
						pInsertAfter = aChildFrame;
						++count;
					}
				}
			}
		}
		else
		{
			ASSERT(MDITILE_VERTICAL == p_mdiTile);

			// In 'Vertical Tile':
			// aNb1: Is the number of rows
			// aNb2: Is the number of columns
			// aNb3: Is the number of additional windows in the last column.

			int aChildWidth = DeskRect.Width() / aNb2;
			int aFirstColsChildHeight = DeskRect.Height() / aNb1;
			int aLastColChildHeight = DeskRect.Height() / (aNb1 + aNb3);
			int aCurrentHeight = aFirstColsChildHeight;
			int count = 0;
			int x = DeskRect.left;
			int y = DeskRect.top;
			for (const auto& row : selectedRows)
			{
				const HWND frameHwnd = reinterpret_cast<HWND>(row->GetLParam());
				if (::IsWindow(m_hWnd))
				{
					GridFrameBase* aChildFrame = dynamic_cast<GridFrameBase*>(CWnd::FromHandle(frameHwnd));
					if (nullptr != aChildFrame)
					{
						aChildFrame->SetIsBeingAutomaticallyArrangedOrTitled(true);
						if (aChildFrame->IsIconic())
							aChildFrame->ShowWindow(SW_RESTORE);

						// See if we're in the last column
						if (aNb1 * (aNb2 - 1) == count)
							aCurrentHeight = aLastColChildHeight;

						// If a new column must be built
						if (0 == (count % aNb1) && aNb1 * (aNb2 - 1) >= count)
						{
							if (0 < count)
							{
								y = 0;
								x += aChildWidth;
							}
						}
						else
						{
							y += aCurrentHeight;
						}

						aChildFrame->SetWindowPos(pInsertAfter, x, y, aChildWidth, aCurrentHeight, SWP_FRAMECHANGED);
						aChildFrame->SetIsBeingAutomaticallyArrangedOrTitled(false);
						pInsertAfter = aChildFrame;
						++count;
					}
				}
			}
		}
	}
}

void BackstagePanelWindows::CascadeModules()
{	
	const auto& selectedRows = m_Grid->GetSelectedRows();
	const auto nbChild = static_cast<int>(selectedRows.size());
	ASSERT(0 < nbChild);
	if (0 < nbChild)
	{
		const int delta = 20;

		CRect DeskRect;
		GetMainMonitorWorkingArea(DeskRect);

		const bool mainWindowIsOnTop = [=] {
			CRect MainRect;
			AfxGetApp()->m_pMainWnd->GetWindowRect(&MainRect);
			return MainRect.top <= DeskRect.top;
		}();

		const int width = max(DeskRect.Width() - (static_cast<int>(selectedRows.size()) - (mainWindowIsOnTop ? 0 : 1)) * delta, 500);
		const int height = DeskRect.Height() - (static_cast<int>(selectedRows.size()) - (mainWindowIsOnTop ? 0 : 1)) * delta;
		int x = DeskRect.left + mainWindowIsOnTop ? delta : 0;
		int y = DeskRect.top + mainWindowIsOnTop ? delta : 0;

		CWnd* pInsertAfter = AfxGetApp()->m_pMainWnd;
		for (const auto& row : selectedRows)
		{
			// LParam contains the HWND of the frame.
			const HWND frameHwnd = reinterpret_cast<HWND>(row->GetLParam());
			if (::IsWindow(m_hWnd))
			{
				CWnd* aChildFrame = CWnd::FromHandle(frameHwnd);
				if (nullptr != aChildFrame)
				{
					if (aChildFrame->IsIconic())
						aChildFrame->ShowWindow(SW_RESTORE);

					aChildFrame->SetWindowPos(pInsertAfter, x, y, width, height, SWP_FRAMECHANGED);
					pInsertAfter = aChildFrame;
					x += delta;
					y += delta;
				}
			}
		}
	}
}

bool BackstagePanelWindows::GetMainMonitorWorkingArea(CRect& rect)
{
	// This gives the main monitor area
	GetDesktopWindow()->GetWindowRect(&rect);

	// We need to remove taskbar area.
	CWnd* pTaskbar = FindWindow(L"Shell_traywnd", NULL);
	if (nullptr != pTaskbar)
	{
		CRect TBRect;
		pTaskbar->GetWindowRect(&TBRect);
		ASSERT(!TBRect.IsRectEmpty() && !TBRect.IsRectNull());
		if (!TBRect.IsRectEmpty() && !TBRect.IsRectNull())
			rect.SubtractRect(rect, TBRect);
		return true;
	}

	return false;
}
