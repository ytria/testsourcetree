#pragma once

#include "LogUserActivityGridCosmos.h"
#include "..\..\Resource.h"

class BackstageTabPanelLogUserActivityCloud : public CExtResizableDialog, public GridSelectionObserver
{
public:
	BackstageTabPanelLogUserActivityCloud();
	~BackstageTabPanelLogUserActivityCloud() override;

	enum { IDD = IDD_BACKSTAGEPAGE_LOG_USERACTIVITY_CLOUD };

	void DoDataExchange(CDataExchange* pDX);

	void SetTheme(const XTPControlTheme nTheme);

	virtual BOOL OnInitDialog() override;
	BOOL OnSetActive();// loads data from Cosmos - can be a long process
	BOOL OnKillActive();
	bool IsDataLoaded() const;

	void ClearGrid();

	virtual void SelectionChanged(const CacheGrid& grid) override;

protected:
	DECLARE_MESSAGE_MAP();

private:
	std::shared_ptr<Sapio365Session> getSession() const;

	afx_msg void OnBnClickedBackstageButtonUseractivityFilterByOwner();
	afx_msg void OnBnClickedBackstageButtonUseractivityFilterClear();

	void updateButtons();

	CXTPRibbonBackstageButton	m_BtnFilterByOwner;
	CXTPRibbonBackstageButton	m_BtnFitlerClear;
	CXTPImageManager			m_ImageList;

	CXTPRibbonBackstageLabel m_staticGridArea;

	LogUserActivityGridCosmos m_Grid;
};