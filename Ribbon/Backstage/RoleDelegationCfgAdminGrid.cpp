#include "RoleDelegationCfgAdminGrid.h"

#include "../Resource.h"
#include "RoleDelegationManager.h"

vector<std::tuple<rttr::type::type_id, int32_t, wstring>> RoleDelegationCfgAdminGrid::g_ObjectTypes;

wstring RoleDelegationCfgAdminGrid::g_ElementTypeAdminScope;
wstring RoleDelegationCfgAdminGrid::g_ElementTypeUser;

const rttr::type::type_id RoleDelegationCfgAdminGrid::g_ElementTypeIDAdminScope = 1;
const rttr::type::type_id RoleDelegationCfgAdminGrid::g_ElementTypeIDUser		= 2;

RoleDelegationCfgAdminGrid::RoleDelegationCfgAdminGrid() : RoleDelegationCfgGridBase(),
	m_ColumnAdminScope(nullptr),
	m_ColumnAdminScopeName(nullptr),
	m_ColumnUserGraphID(nullptr),
	m_ColumnUserDisplayName(nullptr)
{
	m_UseParentRowsToSelectIDs = true;
}

void RoleDelegationCfgAdminGrid::customizeGridSpecific()
{
	SetAutomationName(_YTEXT("RoleDelegationCfgAdminGrid"));
	SetAutomationActionRecording(false);
	AddColumnObjectType();

	m_Icons[RoleDelegationUtil::RBAC_ADMIN_MANAGER]		= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_RBAC_SAPIO365_FULLACCESS));
	m_Icons[RoleDelegationUtil::RBAC_ADMIN_ANNOTATIONS] = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_RBAC_SAPIO365_ANNOTATIONS));
	m_Icons[RoleDelegationUtil::RBAC_ADMIN_LOGS]		= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_RBAC_SAPIO365_LOGS));
	m_Icons[RoleDelegationUtil::RBAC_ADMIN_RBAC]		= AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_RBAC_SAPIO365_RBAC));

	SetSQLiteEngine(RoleDelegationManager::GetInstance().GetRoleDelegationSQLite(), RoleDelegationManager::GetInstance().GetRoleDelegationSQLite().GetTableAdmin());
}

void RoleDelegationCfgAdminGrid::customizeGridPostProcessSpecific()
{
	SetHierarchyColumnsParentPKs({ { GetColumnRowID(), GridBackendUtil::g_AllHierarchyLevels}, { GetColumnObjectType(), GridBackendUtil::g_AllHierarchyLevels} });

	// object types
	if (g_ObjectTypes.empty())
	{
		g_ElementTypeAdminScope		= _T("Admin Scope");
		g_ElementTypeUser			= _T("User");

		g_ObjectTypes.push_back(make_tuple(g_ElementTypeIDAdminScope,	IDB_ICON_ROLEDELEGATIONCFG_FILTERS, g_ElementTypeAdminScope));
		g_ObjectTypes.push_back(make_tuple(g_ElementTypeIDUser,			IDB_ICON_ROLEDELEGATIONCFG_MEMBER,	g_ElementTypeUser));
	}

	for (const auto& t : g_ObjectTypes)
		AddObjectTypeIcon(std::get<0>(t), std::get<1>(t), std::get<2>(t));

	AddHierarchyLowestChildrenObjectTypeId(g_ElementTypeIDUser);

	m_ColumnAdminScope = GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameAdminScope);
	AddColumnForRowPK(m_ColumnAdminScope);

	m_ColumnAdminScopeName	= GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameName);
	m_ColumnAdminScopeName->SetTitle(_T("Administration Role"));
	m_ColumnAdminScopeName->SetTitleDisplayed(_T("Administration Role"));
	m_ColumnUserGraphID		= GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameUserID);
	m_ColumnUserDisplayName = GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameUserName);
	ASSERT(nullptr != m_ColumnUserGraphID);
	ASSERT(nullptr != m_ColumnUserDisplayName);

	GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameUserPrincipalName)->SetWidth(m_ColumnSize32);
	GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameInfo)->SetWidth(m_ColumnSize32 + m_ColumnSize22);
	m_ColumnUserDisplayName->SetWidth(m_ColumnSize32);
	
	SetColumnsVisible({ GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameUserID), GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameUserPrincipalName), m_ColumnAdminScope }, false, false);
	SetColumnsVisible({ GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameUpdName) }, true, false);

	auto columnInfo = GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameInfo);
	MoveColumnAfter(columnInfo, GetColumnObjectType());
	MoveColumnAfter(m_ColumnAdminScopeName, GetColumnObjectType());

	static const COLORREF g_ColorMain = RGB(195, 205, 237);  // darker blue
	for (const auto& p : RoleDelegationUtil::GetAdminScopes())
	{
		auto r = AddRow();
		addRowToSafeSpace(r);// index shall not purge it
		SetRowObjectType(r, g_ElementTypeIDAdminScope);
		r->AddField(static_cast<long>(p.first), m_ColumnAdminScope);
		r->AddField(p.second.first, m_ColumnAdminScopeName, m_Icons.at(p.first));
		r->AddField(p.second.second, columnInfo);
		r->SetColor(g_ColorMain);
		m_MainRows[p.first] = r;
	}
}

void RoleDelegationCfgAdminGrid::loadPostProcessSpecific(GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		const auto& fAdminScope = p_Row->GetField(m_ColumnAdminScope);
		auto		findIt		= m_MainRows.find(static_cast<RoleDelegationUtil::RBAC_AdminScope>(fAdminScope.GetValueLong()));
		ASSERT(m_MainRows.end() != findIt && nullptr != findIt->second);
		if (m_MainRows.end() != findIt && nullptr != findIt->second)
		{
			SetRowObjectType(p_Row, g_ElementTypeIDUser);
			p_Row->SetParentRow(findIt->second);
			p_Row->AddField(findIt->second->GetField(m_ColumnAdminScopeName));
		}
	}
}

void RoleDelegationCfgAdminGrid::prepareSQLoadSpecific()
{
}

void RoleDelegationCfgAdminGrid::GetSelection(map<RoleDelegationUtil::RBAC_AdminScope, map<wstring, wstring>>& p_Selection) const // map: scope vs. graphID vs. Display Name
{
	p_Selection.clear();

	vector<GridBackendRow*> rows;
	GetSelectedNonGroupRowsWithoutCompareRows(rows);
	for (auto r : rows)
	{
		ASSERT(nullptr != r);
		if (nullptr != r)
			p_Selection[static_cast<RoleDelegationUtil::RBAC_AdminScope>(r->GetField(m_ColumnAdminScope).GetValueLong())][r->GetField(m_ColumnUserGraphID).GetValueStr()] = r->GetField(m_ColumnUserDisplayName).GetValueStr();
	}
}