#pragma once

class BackstagePanelBase : public CXTPRibbonBackstagePage
{
public:
	virtual void SetTheme(const XTPControlTheme nTheme) override;

	virtual void DoDataExchange(CDataExchange* pDX) override;
	virtual BOOL OnInitDialog() override;

	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

	void CloseBackstage();

	static void SetupButton(CXTPRibbonBackstageButton& p_Button, const wstring& p_Title, const wstring& p_Description, const CXTPImageManager& p_ImageManager);

protected:
	DECLARE_MESSAGE_MAP()

	bool IsActive() const;	
	BackstagePanelBase(UINT p_nID);

	void setCaption(const wstring& p_Caption);

	static void setupButtonText(CXTPRibbonBackstageButton& p_Button, const wstring& p_Title, const wstring& p_Description);

private:
	CXTPRibbonBackstageLabel		m_caption;
	CXTPRibbonBackstageSeparator	m_separator;
	bool m_isActive = false;
};
