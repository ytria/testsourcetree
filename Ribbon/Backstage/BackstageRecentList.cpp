#include "BackstageRecentList.h"

#include "CodeJockFrameBase.h"

namespace
{
	class RecentFileListItem : public CXTPRecentFileListItem
	{
	public:
		using CXTPRecentFileListItem::CXTPRecentFileListItem;
		void SetPath(CString& path)
		{
			m_strPathName = path;
		}
	};
}

class BackstageRecentList::CXTPRecentFileListDecorator : public CXTPRecentFileList
{
public:
	CXTPRecentFileListDecorator(IRecentList& recentOpeningManager)
		: CXTPRecentFileList(0, _YTEXT(""), _YTEXT(""), recentOpeningManager.GetMaxRecentListSize(), recentOpeningManager.GetMaxRecentListSize())
		, m_RecentOpeningManager(recentOpeningManager)
	{
	}

	virtual void Remove(int nIndex) override
	{
		ASSERT(false);
	}

	virtual void Add(LPCTSTR lpszPathName) override
	{
		ASSERT(false);
	}

	virtual void ReadList() override
	{
		clear();

		std::list<wstring> paths;
		m_RecentOpeningManager.GetRecentList(paths);

		ASSERT(paths.size() <= (size_t) m_nSize);

		if (paths.size() > (size_t) m_nSize)
			paths.resize(m_nSize);

		int i = 0;
		for (const auto& path : paths)
		{
			ASSERT(i < m_nSize);

			CMDTARGET_RELEASE(m_pItems[i]);

			m_arrNames[i] = CString(path.c_str());

			if (!m_arrNames[i].IsEmpty())
			{
				m_pItems[i] = new RecentFileListItem(this);

				// handle this!
				m_pItems[i]->SetPinned(m_RecentOpeningManager.IsPinned(path) ? TRUE : FALSE);

				// handle this?
				// m_pItems[i]->SetIconId();

				// handle this?
				//m_pItems[i]->SetCaption();

				// handle this?
				//m_pItems[i]->SetTag();

				ASSERT(nullptr != dynamic_cast<RecentFileListItem*>(m_pItems[i]));
				static_cast<RecentFileListItem*>(m_pItems[i])->SetPath(m_arrNames[i]);
			}

			++i;
		}
	}

	virtual void WriteList() override
	{
		ASSERT(false);
	}

private:
	void clear()
	{
		for (int i = 0; i < m_nSize; ++i)
		{
			CMDTARGET_RELEASE(m_pItems[i]);
			m_arrNames[i].Empty();
		}
	}

	IRecentList& m_RecentOpeningManager;
};

//==================================================================================================

BackstageRecentList::BackstageRecentList() : 
	m_RecentOpeningManager(nullptr)
{
}

void BackstageRecentList::SetRecentOpeningManager(RecentOpeningManager* recentOpeningManager)
{
	if (m_RecentOpeningManager != recentOpeningManager)
	{
		m_RecentOpeningManager = recentOpeningManager;
		m_RecentFileList = make_unique<CXTPRecentFileListDecorator>(*m_RecentOpeningManager);
	}
}

void BackstageRecentList::SavePinStates()
{
	ASSERT(m_RecentFileList && nullptr != m_RecentOpeningManager);
	if (m_RecentFileList && nullptr != m_RecentOpeningManager)
	{
		auto galleryItems = GetGallery()->GetItems();
		if (nullptr != galleryItems)
		{
			const int numItems = galleryItems->GetItemCount();
			for (int i = 0; i < numItems; ++i)
			{
				auto listBoxItem = dynamic_cast<CXTPRecentFileListBoxItem*>(galleryItems->GetItem(i));
				if (nullptr != listBoxItem)
				{
					auto item = listBoxItem->GetRecentItem();
					if (nullptr != item)
					{
						const bool wasPinned = m_RecentOpeningManager->IsPinned((LPCTSTR)item->GetPathName());
						const bool isPinned = item->IsPinned() == TRUE;

						if (!wasPinned || !isPinned)
							m_RecentOpeningManager->SetPinned((LPCTSTR)item->GetPathName(), isPinned);
					}
				}
			}
		}
	}
}

void BackstageRecentList::BuildItems()
{
	ASSERT(m_RecentFileList);
	if (m_RecentFileList)
	{
		m_RecentFileList->ReadList();
		CXTPRecentFileListBox::BuildItems(m_RecentFileList.get());
	}
}

int BackstageRecentList::GetSelectedItemCommand() const
{
	int command = -1;
	CXTPControlGalleryItem* pItem = GetGallery()->GetItem(GetGallery()->GetSelectedItem());
	if (nullptr != pItem)
	{
		// Item ID is ID_FILE_MRU_FILE1 + nIndex
		const int nIndex = pItem->GetID() - ID_FILE_MRU_FILE1;

		if (nullptr != m_RecentOpeningManager)
		{
			const RecentOpeningManager::RecentIDsMap& items = m_RecentOpeningManager->GetRecentIDsMap();
			ASSERT(!items.empty());
			if (!items.empty())
			{
				auto it = items.begin();
				std::advance(it, nIndex);
				ASSERT(items.end() != it);
				if (items.end() != it)
					command = it->first;
			}
		}
	}

	return command;
}

wstring BackstageRecentList::GetSelectedItemPath() const
{
	wstring XMLpath;

	if (nullptr != m_RecentOpeningManager)
		XMLpath = m_RecentOpeningManager->GetRecentIDsMap().at(GetSelectedItemCommand());

	return XMLpath;
}