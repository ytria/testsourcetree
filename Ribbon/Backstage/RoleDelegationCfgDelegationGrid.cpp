#include "RoleDelegationCfgDelegationGrid.h"

#include "../Resource.h"
#include "BusinessUserConfiguration.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessSiteConfiguration.h"
#include "GridBackendUtil.h"
#include "MainFrameSapio365Session.h"
#include "RoleDelegationManager.h"
#include "YCodeJockMessageBox.h"

wstring RoleDelegationCfgDelegationGrid::g_ElementTypeFiltersParent;
wstring RoleDelegationCfgDelegationGrid::g_ElementTypeMembersParent;
wstring RoleDelegationCfgDelegationGrid::g_ElementTypeDelegation;
wstring RoleDelegationCfgDelegationGrid::g_ElementTypeFilter;
wstring RoleDelegationCfgDelegationGrid::g_ElementTypeMember;
wstring RoleDelegationCfgDelegationGrid::g_ElementTypePrivilege;
wstring RoleDelegationCfgDelegationGrid::g_ElementTypeRightsParent;
wstring RoleDelegationCfgDelegationGrid::g_ElementTypeLicensePool;
wstring RoleDelegationCfgDelegationGrid::g_ElementTypeLicensePoolsParent;

const rttr::type::type_id RoleDelegationCfgDelegationGrid::g_ElementTypeIDPrivilegesParent	 = 1;
const rttr::type::type_id RoleDelegationCfgDelegationGrid::g_ElementTypeIDFiltersParent		 = 2;
const rttr::type::type_id RoleDelegationCfgDelegationGrid::g_ElementTypeIDMembersParent		 = 3;
const rttr::type::type_id RoleDelegationCfgDelegationGrid::g_ElementTypeIDLicensePoolsParent = 4;
const rttr::type::type_id RoleDelegationCfgDelegationGrid::g_ElementTypeIDDelegation		 = 5;
const rttr::type::type_id RoleDelegationCfgDelegationGrid::g_ElementTypeIDPrivilege			 = 6;
const rttr::type::type_id RoleDelegationCfgDelegationGrid::g_ElementTypeIDFilter			 = 7;
const rttr::type::type_id RoleDelegationCfgDelegationGrid::g_ElementTypeIDMember			 = 8;
const rttr::type::type_id RoleDelegationCfgDelegationGrid::g_ElementTypeIDLicensePool		 = 9;

vector<std::tuple<rttr::type::type_id, int32_t, wstring>> RoleDelegationCfgDelegationGrid::g_ObjectTypes;

RoleDelegationCfgDelegationGrid::RoleDelegationCfgDelegationGrid() : RoleDelegationCfgGridBase(), 
	m_ColumnFilterType(nullptr)
{
}

void RoleDelegationCfgDelegationGrid::customizeGridSpecific()
{
	SetAutomationName(_YTEXT("RoleDelegationCfgDelegationGrid"));
	SetAutomationActionRecording(false);
	GetBackend().SetIgnoreFieldModifications(true);

	AddColumnObjectType();
	AddColumnForRowPK(GetColumnObjectType());

	m_ColumnKey						= AddColumn(_YUID("ACCESSKEY"),						YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_6, _YLOC("Credentials")).c_str(),				GridBackendUtil::g_DummyString);
	m_ColumnFilterType				= AddColumnIcon(_YUID("FILTERTYPE"),				YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_customizeGridSpecific_2, _YLOC("Target of Scope")).c_str(), 	GridBackendUtil::g_DummyString);
	m_ColumnMemberPrincipalName		= AddColumn(_YUID("MEMBER.PRINCIPALNAME"),			YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_customizeGridSpecific_3, _YLOC("Assigned To")).c_str(),		GridBackendUtil::g_DummyString);
	m_ColumnMemberGraphID			= AddColumn(_YUID("MEMBER.GRAPHID"),				YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_customizeGridSpecific_4, _YLOC("Assigned To (ID)")).c_str(),	GridBackendUtil::g_DummyString);
	m_ColumnOptionLogSessionLogin	= AddColumnIcon(_YUID("OPTIONLOGSESSION"),			_T("Log role usage"),																						GridBackendUtil::g_DummyString);
	m_ColumnOptionLogModuleOpen		= AddColumnIcon(_YUID("OPTIONMODULEOPEN"),			_T("Log module access"),																						GridBackendUtil::g_DummyString);
	m_ColumnOptionEnforceRole		= AddColumnIcon(_YUID("OPTIONENFORCEROLE"),			_T("Enforce role"),																								GridBackendUtil::g_DummyString);

	AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_ROLEDELEGATIONCFG_KEYS));
	// filter types
	AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_USER));
	AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SITE));
	AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_GROUP));
	// options
	AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_RBAC_SAPIO365_OPTIONENFORCEROLE));
	AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_RBAC_SAPIO365_OPTIONLOGMODULEOPEN));
	AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_RBAC_SAPIO365_OPTIONLOGSESSIONLOGIN));

	SetSQLiteEngine(RoleDelegationManager::GetInstance().GetRoleDelegationSQLite(), RoleDelegationManager::GetInstance().GetRoleDelegationSQLite().GetTableDelegations());
}

void RoleDelegationCfgDelegationGrid::customizeGridPostProcessSpecific()
{
	SetHierarchyColumnsParentPKs({ { GetColumnRowID(), GridBackendUtil::g_AllHierarchyLevels}, { GetColumnObjectType(), GridBackendUtil::g_AllHierarchyLevels} });

	// object types
	if (g_ObjectTypes.empty())
	{
		g_ElementTypeRightsParent		= YtriaTranslate::Do(FrameDriveItems_customizeActionsRibbonTab_1, _YLOC("Permissions")).c_str();
		g_ElementTypeFiltersParent		= YtriaTranslate::Do(DlgRoleSeeInfo_generateJSONScriptDataSpecific_4, _YLOC("Scopes")).c_str();
		g_ElementTypeMembersParent		= YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_customizeGridPostProcessSpecific_3, _YLOC("Assigned Users")).c_str();
		g_ElementTypeLicensePoolsParent	= YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_customizeGridPostProcessSpecific_4, _YLOC("License Delegations")).c_str();
		g_ElementTypeDelegation			= YtriaTranslate::Do(GridDriveItems_customizeGrid_58, _YLOC("Role Based")).c_str();
		g_ElementTypePrivilege			= YtriaTranslate::Do(GridDriveItems_customizeGrid_99, _YLOC("Permission")).c_str();
		g_ElementTypeFilter				= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_2, _YLOC("Scope")).c_str();
		g_ElementTypeMember				= YtriaTranslate::Do(RoleDelegationSQLite_RoleDelegationSQLite_24, _YLOC("Assigned User")).c_str();
		g_ElementTypeLicensePool		= YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_customizeGridPostProcessSpecific_4, _YLOC("License Delegation")).c_str();

		g_ObjectTypes.push_back(make_tuple(g_ElementTypeIDPrivilegesParent,	  IDB_ICON_ROLEDELEGATIONCFG_RIGHT,	  g_ElementTypeRightsParent));
		g_ObjectTypes.push_back(make_tuple(g_ElementTypeIDFiltersParent,	  IDB_ICON_ROLEDELEGATIONCFG_FILTERS, g_ElementTypeFiltersParent));
		g_ObjectTypes.push_back(make_tuple(g_ElementTypeIDMembersParent,	  IDB_ICON_ROLEDELEGATIONCFG_MEMBER,  g_ElementTypeMembersParent));
		g_ObjectTypes.push_back(make_tuple(g_ElementTypeIDLicensePoolsParent, IDB_ICON_ROLEDELEGATIONCFG_LICPOOL, g_ElementTypeLicensePoolsParent));
		g_ObjectTypes.push_back(make_tuple(g_ElementTypeIDDelegation,		  IDB_ICON_ROLEDELEGATIONCFG_MAIN,	  g_ElementTypeDelegation));
		g_ObjectTypes.push_back(make_tuple(g_ElementTypeIDPrivilege,		  IDB_ICON_ROLEDELEGATIONCFG_RIGHT,	  g_ElementTypePrivilege));
		g_ObjectTypes.push_back(make_tuple(g_ElementTypeIDFilter,			  IDB_ICON_ROLEDELEGATIONCFG_FILTERS, g_ElementTypeFilter));
		g_ObjectTypes.push_back(make_tuple(g_ElementTypeIDMember,			  IDB_ICON_ROLEDELEGATIONCFG_MEMBER,  g_ElementTypeMember));
		g_ObjectTypes.push_back(make_tuple(g_ElementTypeIDLicensePool,		  IDB_ICON_ROLEDELEGATIONCFG_LICPOOL, g_ElementTypeLicensePool));
	}

	m_ColumnName = GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameName);
	AddColumnForRowPK(m_ColumnName);

		for (const auto& t : g_ObjectTypes)
			AddObjectTypeIcon(std::get<0>(t), std::get<1>(t), std::get<2>(t));

	AddHierarchyLowestChildrenObjectTypeId(g_ElementTypeIDPrivilege);
	AddHierarchyLowestChildrenObjectTypeId(g_ElementTypeIDFilter);
	AddHierarchyLowestChildrenObjectTypeId(g_ElementTypeIDMember);
	AddHierarchyLowestChildrenObjectTypeId(g_ElementTypeIDLicensePool);

	if (nullptr != m_ColumnStatus)
		MoveColumnAfter(GetColumnObjectType(), m_ColumnStatus);
	else 
		MoveColumnBefore(GetColumnObjectType(), m_ColumnName);
	MoveColumnBefore(m_ColumnOptionLogSessionLogin,		m_ColumnName);
	MoveColumnBefore(m_ColumnOptionLogModuleOpen,		m_ColumnName);
	MoveColumnBefore(m_ColumnOptionEnforceRole,			m_ColumnName);
	MoveColumnAfter(m_ColumnFilterType, GetColumnObjectType());

	SetColumnsVisible({ m_ColumnMemberPrincipalName, m_ColumnMemberGraphID }, false, GridBackendUtil::SETVISIBLE_NONE);
	// SetColumnsVisible({ GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameIDKey), m_ColumnMemberPrincipalName, m_ColumnMemberGraphID }, false, GridBackendUtil::SETVISIBLE_NONE);

	GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameInfo)->SetWidth(m_ColumnSize32*2);
}

void RoleDelegationCfgDelegationGrid::prepareSQLoadSpecific()
{
	RemoveAllRows();// because of mixed object types
}

const wstring& RoleDelegationCfgDelegationGrid::GetTypeDelegation() const
{
	return g_ElementTypeDelegation;
}

const wstring& RoleDelegationCfgDelegationGrid::GetTypeMember() const
{
	return g_ElementTypeMember;
}

const wstring& RoleDelegationCfgDelegationGrid::GetTypeFilter() const
{
	return g_ElementTypeFilter;
}

const wstring& RoleDelegationCfgDelegationGrid::GetTypeParentMembers() const
{
	return g_ElementTypeMembersParent;
}

const wstring& RoleDelegationCfgDelegationGrid::GetTypeParentFilters() const
{
	return g_ElementTypeFiltersParent;
}

void RoleDelegationCfgDelegationGrid::setKey(GridBackendRow* p_Row, const RoleDelegation& p_rd)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
		p_Row->AddField(p_rd.GetKey().m_Name, m_ColumnKey);
		//p_Row->AddField(p_rd.GetKey().m_Name, m_ColumnKey, ICON_KEY);
}

bool RoleDelegationCfgDelegationGrid::finalizeSQLoad()
{
	static const COLORREF g_ColorMain      = RGB(195, 205, 237);  // darker blue
	static const COLORREF g_ColorFilter    = RGB(228, 237, 248);  // RGB(253, 237, 227); orange
	static const COLORREF g_ColorUser	   = RGB(228, 237, 248);  // blue
	static const COLORREF g_ColorPrivilege = RGB(228, 237, 248);  // RGB(235, 244, 230); green
	static const COLORREF g_ColorLicPool   = RGB(228, 237, 248);  // RGB(254, 245, 217); yellow

	static const auto& g_FilterTypes = RoleDelegationUtil::FilterTypes::GetInstance();

	wstring infoString, tmpStr;
	static const auto& ucfg = BusinessUserConfiguration::GetInstance();
	static const auto& gcfg = BusinessGroupConfiguration::GetInstance();
	static const auto& scfg = BusinessSiteConfiguration::GetInstance();

	const auto rows = GetBackendRows(); // Copy because we add new rows in the loop
	bool flags = false;
	for (auto r : rows)
	{
		ASSERT(nullptr != r);
		if (nullptr != r)
		{
			SetRowObjectType(r, g_ElementTypeIDDelegation);
			r->SetColor(g_ColorMain);

			auto IDdelegation = GetID(r);
			ASSERT(IDdelegation > 0);
			if (IDdelegation > 0)
			{
				RoleDelegation rd = RoleDelegationManager::GetInstance().LoadRoleDelegationFromSQLite(IDdelegation, MainFrameSapio365Session());
				if (rd.IsLoaded())
				{
					setKey(r, rd);

					if (rd.GetDelegation().IsLogSessionLogin())
						r->AddField(RoleDelegationUtil::BusinessDelegation::g_LabelLogSessionLogin, m_ColumnOptionLogSessionLogin, ICON_OPTION_LOGSESSIONLOGIN);
					if (rd.GetDelegation().IsLogModuleOpening())
						r->AddField(RoleDelegationUtil::BusinessDelegation::g_LabelLogModuleOpening, m_ColumnOptionLogModuleOpen, ICON_OPTION_LOGMODULEOPEN);
					if (rd.GetDelegation().IsEnforceRole())
						r->AddField(RoleDelegationUtil::BusinessDelegation::g_LabelEnforceRole, m_ColumnOptionEnforceRole, ICON_OPTION_ENFORCEROLE);

					// add privileges
					auto privilegesMasterRow = AddRow();
					ASSERT(nullptr != privilegesMasterRow);
					if (nullptr != privilegesMasterRow)
					{
						SetRowObjectType(privilegesMasterRow, g_ElementTypeIDPrivilegesParent);
						setKey(privilegesMasterRow, rd);

						privilegesMasterRow->SetParentRow(r);
						privilegesMasterRow->SetColor(g_ColorPrivilege);
						privilegesMasterRow->AddField(rd.GetDelegation().m_Name, m_ColumnName);

						size_t nb = rd.GetPrivileges().size();
						if (nb == 0)
							privilegesMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_1, _YLOC("No Permission")).c_str(), m_ColumnInfo);
						else if(nb == 1)
							privilegesMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_2, _YLOC("1 Permission")).c_str(), m_ColumnInfo);
						else
							privilegesMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_11, _YLOC("%1 Permissions"), Str::getStringFromNumber<size_t>(nb).c_str()), m_ColumnInfo);
						

						for (const auto& p : rd.GetPrivileges())
						{
							ASSERT(p.IsValid());
							if (p.IsValid())
							{
								auto privRow = AddRow();
								ASSERT(nullptr != privRow);
								if (nullptr != privRow)
								{
									SetRowObjectType(privRow, g_ElementTypeIDPrivilege);
									setKey(privRow, rd);

									privRow->AddField(p.m_Name,						m_ColumnName);
									//privRow->AddField(p.m_Info,					m_ColumnInfo);// info is set automatically, irrelevant here
									privRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_12, _YLOC("For delegation: %1"), rd.GetDelegation().m_Name.c_str()),	m_ColumnInfo);

									privRow->AddField(p.m_UpdDate,					m_ColumnUpdDate);
									privRow->AddField(p.m_UpdateByID,				m_ColumnUpdID);
									privRow->AddField(p.m_UpdateByName,				m_ColumnUpdName);
									privRow->AddField(p.m_UpdateByPrincipalName,	m_ColumnUpdPrincipalName);
									privRow->SetParentRow(privilegesMasterRow);
									loadPostProcess(privRow);
								}
							}
						}
					}

					// add filters
					auto filtersMasterRow = AddRow();
					ASSERT(nullptr != filtersMasterRow);
					if (nullptr != filtersMasterRow)
					{
						SetRowObjectType(filtersMasterRow, g_ElementTypeIDFiltersParent);
						setKey(filtersMasterRow, rd);

						filtersMasterRow->SetParentRow(r);
						filtersMasterRow->SetColor(g_ColorFilter);
						filtersMasterRow->AddField(rd.GetDelegation().m_Name, m_ColumnName);

						size_t nbFilters = 0;
						for (const auto& f : rd.GetFilters())
						{
							const auto& fType	= f.first;
							const auto& filters = f.second;
							nbFilters += filters.size();
							for (const auto& filter : filters)
							{
								ASSERT(filter.IsValid());
								if (filter.IsValid())
								{
									auto fRow = AddRow();
									ASSERT(nullptr != fRow);
									if (nullptr != fRow)
									{
										SetRowObjectType(fRow, g_ElementTypeIDFilter);
										setKey(fRow, rd);

										fRow->AddField(Str::getStringFromNumber<int64_t>(filter.m_ID.get()), GetColumnRowID());

										if (nullptr != m_ColumnStatus)
											fRow->AddField(static_cast<uint32_t>(filter.m_Status.get()),	m_ColumnStatus);

										fRow->AddField(filter.m_Name,					m_ColumnName);
										fRow->AddField(filter.m_UpdDate,				m_ColumnUpdDate);
										fRow->AddField(filter.m_UpdateByID,				m_ColumnUpdID);
										fRow->AddField(filter.m_UpdateByName,			m_ColumnUpdName);
										fRow->AddField(filter.m_UpdateByPrincipalName,	m_ColumnUpdPrincipalName);

										if (fType == g_FilterTypes.GetTypeGroup())
										{
											fRow->AddField(g_FilterTypes.GetNameGroup(), m_ColumnFilterType, ICON_GROUP);
											tmpStr = gcfg.GetTitle(filter.m_Property);
										}
										else if (fType == g_FilterTypes.GetTypeSite())
										{
											fRow->AddField(g_FilterTypes.GetNameSite(), m_ColumnFilterType, ICON_SITE);
											tmpStr = scfg.GetTitle(filter.m_Property);
										}
										else if (fType == g_FilterTypes.GetTypeUser())
										{
											fRow->AddField(g_FilterTypes.GetNameUser(), m_ColumnFilterType, ICON_USER);
											tmpStr = ucfg.GetTitle(filter.m_Property);
										}

										fRow->AddField(filter.m_Info.empty() ? 
															YtriaTranslate::Do(sItem_getValue_33, _YLOC("%1 :: %2 :: '%3'"),			tmpStr.c_str(), RoleDelegationUtil::GetTaggerMatchMethod(filter.m_TaggerMatchMethod).c_str(), filter.m_Value.c_str()) :
															YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_14, _YLOC("%1 :: %2 :: '%3' :: [%4]"),	tmpStr.c_str(), RoleDelegationUtil::GetTaggerMatchMethod(filter.m_TaggerMatchMethod).c_str(), filter.m_Value.c_str(), filter.m_Info.c_str()),
														m_ColumnInfo);

										fRow->SetParentRow(filtersMasterRow);
										loadPostProcess(fRow);
									}
								}
							}
						}
						if (nbFilters == 0)
							filtersMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_3, _YLOC("No Scope")).c_str(), m_ColumnInfo);
						else if (nbFilters == 1)
							filtersMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_4, _YLOC("1 Scope")).c_str(), m_ColumnInfo);
						else
							filtersMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_15, _YLOC("%1 Scopes"), Str::getStringFromNumber<size_t>(nbFilters).c_str()), m_ColumnInfo);
					}

					// add user members
					auto usersMasterRow = AddRow();
					ASSERT(nullptr != usersMasterRow);
					if (nullptr != usersMasterRow)
					{
						SetRowObjectType(usersMasterRow, g_ElementTypeIDMembersParent);
						setKey(usersMasterRow, rd);

						usersMasterRow->SetParentRow(r);
						usersMasterRow->SetColor(g_ColorUser);
						usersMasterRow->AddField(rd.GetDelegation().m_Name, m_ColumnName);
						size_t nb = rd.GetMembers().size();
						if (nb == 0)
							usersMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_5, _YLOC("No Assigned User")).c_str(), m_ColumnInfo);
						else if (nb == 1)
							usersMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_6, _YLOC("1 Assigned User")).c_str(), m_ColumnInfo);
						else
							usersMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_16, _YLOC("%1 Assigned Users"), Str::getStringFromNumber<size_t>(nb).c_str()), m_ColumnInfo);

						for (const auto& member : rd.GetMembers())
						{
							ASSERT(member.IsValid());
							if (member.IsValid())
							{
								auto mRow = AddRow();
								ASSERT(nullptr != mRow);
								if (nullptr != mRow)
								{
									SetRowObjectType(mRow, g_ElementTypeIDMember);
									setKey(mRow, rd);

									mRow->AddField(Str::getStringFromNumber<int64_t>(member.m_ID.get()), GetColumnRowID());

									if (nullptr != m_ColumnStatus)
										mRow->AddField(static_cast<uint32_t>(member.m_Status.get()), m_ColumnStatus);
									
									mRow->AddField(member.m_UserName,			m_ColumnName);
									mRow->AddField(member.m_UserPrincipalName,	m_ColumnMemberPrincipalName);
									mRow->AddField(member.m_UserID,				m_ColumnMemberGraphID);

									mRow->AddField(member.m_Name,					m_ColumnInfo);
									mRow->AddField(member.m_UpdDate,				m_ColumnUpdDate);
									mRow->AddField(member.m_UpdateByID,				m_ColumnUpdID);
									mRow->AddField(member.m_UpdateByName,			m_ColumnUpdName);
									mRow->AddField(member.m_UpdateByPrincipalName,	m_ColumnUpdPrincipalName);
									mRow->SetParentRow(usersMasterRow);
									loadPostProcess(mRow);
								}
							}
						}
					}

					// add License Pools
					auto licPoolMasterRow = AddRow();
					ASSERT(nullptr != licPoolMasterRow);
					if (nullptr != licPoolMasterRow)
					{
						SetRowObjectType(licPoolMasterRow, g_ElementTypeIDLicensePoolsParent);
						setKey(licPoolMasterRow, rd);

						licPoolMasterRow->SetParentRow(r);
						licPoolMasterRow->SetColor(g_ColorLicPool);
						licPoolMasterRow->AddField(rd.GetDelegation().m_Name, m_ColumnName);

						size_t nbLicPools = 0;
						for (const auto& skuLimit : rd.GetDelegation().GetSkuAccessLimits())
						{
							if (skuLimit.second >= 0)
							{
								const auto& skuCode = skuLimit.first;
								const auto& skuQtty = skuLimit.second;

								auto lRow = AddRow();
								ASSERT(nullptr != lRow);
								if (nullptr != lRow)
								{
									nbLicPools++;
									SetRowObjectType(lRow, g_ElementTypeIDLicensePool);
									setKey(lRow, rd);

									lRow->AddField(skuCode, m_ColumnName);
									lRow->AddField(skuQtty, m_ColumnInfo);
									lRow->SetParentRow(licPoolMasterRow);
									loadPostProcess(lRow);
								}
							}
						}

						if (nbLicPools == 0)
							licPoolMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_7, _YLOC("No License Delegation")).c_str(), m_ColumnInfo);
						else if (nbLicPools == 1)
							licPoolMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_8, _YLOC("1 License Delegation")).c_str(), m_ColumnInfo);
						else
							licPoolMasterRow->AddField(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_17, _YLOC("%1 License Delegations"), Str::getStringFromNumber<size_t>(nbLicPools).c_str()), m_ColumnInfo);
					}
				}
				else
				{
					if (nullptr != m_ColumnStatus)
						r->AddField(YtriaTranslate::Do(DlgActions_doValidate_1, _YLOC("INVALID")).c_str(), m_ColumnStatus, GridBackendUtil::ICON_ERROR);
				}
			}
			else
			{
				if (nullptr != m_ColumnStatus)
					r->AddField(YtriaTranslate::Do(DlgActions_doValidate_1, _YLOC("INVALID")).c_str(), m_ColumnStatus, GridBackendUtil::ICON_ERROR);
			}
		}
	}

	return !rows.empty();
}

vector<int64_t> RoleDelegationCfgDelegationGrid::GetRoleDelegationIDsFromSelection() const
{
	vector<int64_t> rvIDs;

	vector<GridBackendRow*> selectedNonGroupRows;
	GetSelectedNonGroupRows(selectedNonGroupRows);

	GridBackendRow* ancestor = nullptr;
	for (auto row : selectedNonGroupRows)
		rvIDs.push_back(GetDelegationID(row));

	return rvIDs;
}

vector<int64_t> RoleDelegationCfgDelegationGrid::GetAllRoleDelegationIDs() const
{
	vector<int64_t> ids;

	for (auto row : GetBackendRows())
		if (!row->IsGroupRow() && nullptr == row->GetParentRow())
			ids.push_back(GetDelegationID(row));

	return ids;
}

int64_t RoleDelegationCfgDelegationGrid::GetDelegationID(GridBackendRow* p_Row) const
{
	int64_t rvID = 0;

	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		auto ancestor = p_Row->GetTopAncestorOrThis();
		ASSERT(nullptr != ancestor);
		if (nullptr != ancestor)
			rvID = GetID(ancestor);
	}

	return rvID;
}

map<wstring, vector<pair<int64_t, int64_t>>> RoleDelegationCfgDelegationGrid::GetElementIDsFromSelection(const vector<GridBackendRow*>& p_SelectedNonGroupRows) const
{
	map<wstring, vector<pair<int64_t, int64_t>>> rvIDs;
	
	for (auto row : p_SelectedNonGroupRows)
	{
		ASSERT(nullptr != row);
		if (nullptr != row && !row->IsGroupRow())
			rvIDs[row->GetField(GetColumnObjectType()).GetValueStr()].push_back(std::make_pair(GetID(row), GetDelegationID(row)));
	}

	return rvIDs;
}

void RoleDelegationCfgDelegationGrid::loadPostProcessSpecific(GridBackendRow* p_Row)
{

}