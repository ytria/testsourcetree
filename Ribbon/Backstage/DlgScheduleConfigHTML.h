#pragma once

#include "DlgFormsHTML.h"

class TaskSchedule;

class DlgScheduleConfigHTML : public DlgFormsHTML
{
public:
	DlgScheduleConfigHTML(const wstring& p_DefaultDisplayName, const wstring& p_JobTitle, CWnd* p_Parent);

	const std::unique_ptr<TaskSchedule>& GetTaskSchedule() const;
	const wstring& GetDisplayName() const;

protected:
	virtual void generateJSONScriptData() override;

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

	virtual wstring getDialogTitle() const override;

private:
	std::unique_ptr<TaskSchedule> m_TaskSchedule;
	wstring m_JobTitle;
	wstring m_DisplayName;
};