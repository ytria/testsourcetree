#pragma once

#include "BackstagePanelWithGrid.h"
#include "LogGrid.h"
#include "..\..\Resource.h"
#include "ViewGrid.h"

class GridFrameBase;
class BackstagePanelViews : public BackstagePanelWithGrid<ViewGrid>, public GridSelectionObserver
{
public:
	BackstagePanelViews(GridFrameBase& p_Frame);
	~BackstagePanelViews() override;

	enum { IDD = IDD_BACKSTAGEPAGE_VIEWS };

	virtual BOOL OnInitDialog() override;
	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

	void			OnGridDoubleClick();
	virtual void	SelectionChanged(const CacheGrid& grid) override;

protected:
	void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP();

	virtual void SetTheme(const XTPControlTheme nTheme) override;

private:
	void updateMainGrid() const;

	afx_msg void onEdit();
	afx_msg void onDelete();
	afx_msg void onSetAsDefault();

	GridFrameBase&				m_Frame;
	CXTPRibbonBackstageButton	m_BtnEdit;
	CXTPRibbonBackstageButton	m_BtnDelete;
	CXTPRibbonBackstageButton	m_BtnSetAsDefault;
	CXTPImageManager			m_imagelist;
};