#pragma once

#include "BackstagePanelJobCenterBase.h"

#include "..\..\Resource.h"

class CodeJockFrameBase;

class BackstagePanelJobCenterCommon : public BackstagePanelJobCenterBase
{
public:
	BackstagePanelJobCenterCommon(CodeJockFrameBase* p_Frame);
	~BackstagePanelJobCenterCommon() override;

	enum { IDD = IDD_BACKSTAGEPAGE_JOBCENTER_COMMON };

	void DoDataExchange(CDataExchange* pDX) override;
	virtual BOOL OnInitDialogSpecific() override;
	
	virtual void OnSetActive() override;

	virtual void SetSimpleView(bool p_SimpleView) override;

protected:
	DECLARE_MESSAGE_MAP()
	
private:
	virtual void setThemeSpecific(const XTPControlTheme nTheme) override;
	virtual void updateButtons() override;
};