#pragma once

#include "BackstagePanelBase.h"

#include "BackstageTabPanelRoleDelegationConfigurationFilters.h"
#include "BackstageTabPanelRoleDelegationConfigurationKeys.h"
#include "BackstageTabPanelRoleDelegationConfigurationDelegation.h"

#include "..\..\Resource.h"

class Sapio365Session;

class BackstagePanelRoleDelegationConfigurationTop : public BackstagePanelBase
{
public:
	BackstagePanelRoleDelegationConfigurationTop();
	~BackstagePanelRoleDelegationConfigurationTop() override;

	enum { IDD = IDD_BACKSTAGEPAGE_ROLEDELEGATION_CFG_TOP };
	
	void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP();

	virtual void SetTheme(const XTPControlTheme nTheme) override;

	virtual BOOL OnInitDialog() override;
	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

private:
	afx_msg void OnShowRoles();
	afx_msg void OnShowFilters();
	afx_msg void OnShowKeys();
	afx_msg void OnReload();// from cloud
	afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);

	void showPanel(const int p_PanelIndex); // Hides every panel whose index is not p_PanelIndex
	void hideEverything();
	void showButtons(const bool p_Show);
	void enableButtons();

	std::shared_ptr<Sapio365Session>	getSession() const;
	const PersistentSession&		getSessionInfo() const;

	bool hasSessionAccessToConfig() const;

	CStatic						m_PseudoTab;
	CXTPImageManager			m_ImageList;
	CXTPRibbonBackstageLabel	m_Top;
	
	bool m_ReadyToRoll;

	map<int, CWnd*>	m_Panels;

	BackstageTabPanelRoleDelegationConfigurationDelegation	m_TabPanelRoles;
	BackstageTabPanelRoleDelegationConfigurationFilters		m_TabPanelFilters;
	BackstageTabPanelRoleDelegationConfigurationKeys		m_TabPanelKeys;

	CXTPRibbonBackstageButton m_BtnShowDelegations;
	CXTPRibbonBackstageButton m_BtnShowFilters;
	CXTPRibbonBackstageButton m_BtnShowKeys;
	CXTPRibbonBackstageButton m_BtnReload;

	CXTPBrush m_brushButtonBack;
	static const COLORREF g_ButtonBackColor;
};
