#include "LogGrid.h"

#include "BackstageLogger.h"
#include "Resources/Resource.h"

const int64_t LogGrid::g_UpdateMinPeriodMs = 400;

wstring LogGrid::g_StrDebugLevel;
wstring LogGrid::g_StrInfoLevel;
wstring LogGrid::g_StrWarningLevel;
wstring LogGrid::g_StrErrorLevel;
wstring LogGrid::g_StrFatalLevel;
wstring LogGrid::g_StrUserLevel;
bool LogGrid::g_HasLocalizedStaticMembers = false;

void LogGrid::LocalizeStaticMembers()
{
	if (!g_HasLocalizedStaticMembers)
	{
		g_StrDebugLevel = YtriaTranslate::Do(LogGrid_getLogLevelText_1, _YLOC("DEBUG"));
		g_StrInfoLevel = YtriaTranslate::Do(LogGrid_getLogLevelText_2, _YLOC("INFO"));
		g_StrWarningLevel = YtriaTranslate::Do(LogGrid_getLogLevelText_3, _YLOC("WARNING"));
		g_StrErrorLevel = YtriaTranslate::Do(LogGrid_getLogLevelText_4, _YLOC("ERROR"));
		g_StrFatalLevel = YtriaTranslate::Do(LogGrid_getLogLevelText_5, _YLOC("FATAL"));
		g_StrUserLevel = _T("USER");

		g_HasLocalizedStaticMembers = true;
	}
}

const int LogGrid::g_TimerID = 6969;

BEGIN_MESSAGE_MAP(LogGrid, CacheGrid)
	ON_WM_TIMER()
END_MESSAGE_MAP()

LogGrid::LogGrid(CFrameWnd& contextFrame, bool p_LogOnlyFromThisFrame)
	: CacheGrid(0, /*GridBackendUtil::CREATETOOLBAR | */GridBackendUtil::CREATEGROUPAREA | GridBackendUtil::CREATECOLORG | GridBackendUtil::CREATESORTING)
	, FrameLogger(contextFrame)
	, m_TimerRunning(false)
	, m_Active(false)
	, m_UpdatePending(false)
{
	SetLogFromAnyFrame(!p_LogOnlyFromThisFrame);
	SetLogFromUnknownFrame(::AfxGetApp()->m_pMainWnd->m_hWnd == contextFrame.m_hWnd);

	LocalizeStaticMembers();
	SetLiveGrid(true);
}

void LogGrid::customizeGrid()
{
	SetAutomationName(_YTEXT("BackstageLogGrid"));
	SetAutomationActionRecording(false);

	m_DateReceivedColumn	= AddColumnDate(_YUID("LG1"), YtriaTranslate::Do(LogGrid_customizeGrid_1, _YLOC("Time")).c_str(), GridBackendUtil::g_DummyString, HIDPI_XW(110));
	m_LevelColumn			= AddColumn(_YUID("LG2"), YtriaTranslate::Do(LogGrid_customizeGrid_2, _YLOC("Level")).c_str(), GridBackendUtil::g_DummyString);
	m_MessageColumn			= AddColumn(_YUID("LG3"), YtriaTranslate::Do(LogGrid_customizeGrid_3, _YLOC("Message")).c_str(), GridBackendUtil::g_DummyString);
	
	m_MessageColumn->SetAutoResizeWithGridResize(true);

	DateTimeFormat format;
	format.m_ShowDate = false;
	format.m_ShowTime = true;
	format.m_TimeFormat.m_DisplayFlags = TimeFormat::ALL_STANDARD;
	m_DateReceivedColumn->SetDateTimeFormat(format, false);
		
	AddSorting(m_DateReceivedColumn, GridBackendUtil::DESC);

	// When loading a module, somewhere in the call stack, this grid is updated and focus is set on it.
	// The frame of the module will end up in the back ground. Set this grid so that is does not grab the focus when updated.
	SetGrabFocus(false);
}

void LogGrid::LogSpecific(const ILogger::LogEntry& p_Entry) const
{
	if (p_Entry.m_LogLevel != LogLevel::Debug || CRMpipe().IsFeatureSandboxed())
	{
		YCallbackMessage::DoPost([grid = const_cast<LogGrid*>(this)->shared_from_this(), entry = p_Entry]()
			{
				grid->InsertLogCallback(entry);
			});
	}
}

void LogGrid::SetIsActive(const bool p_Active)
{
	m_Active = p_Active;

	if (m_Active && m_UpdatePending)
	{
		m_UpdatePending = false;
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}
}

void LogGrid::InsertLogCallback(const ILogger::LogEntry& p_Entry)
{
	if (::IsWindow(m_hWnd))
	{
		GridBackendRow* row = AddRow(false);
		ASSERT(nullptr != row);

		if (nullptr != row)
		{
			row->AddField(p_Entry.m_TimeDate, m_DateReceivedColumn);
			row->AddField(getLogLevelText(static_cast<LogLevel>(p_Entry.m_LogLevel)), m_LevelColumn);
			row->AddField(p_Entry.m_Message, m_MessageColumn);
		}

		if (m_Active && !m_TimerRunning)
		{
			m_UpdatePending = false;
			m_TimerRunning = true;
			SetTimer(g_TimerID, g_UpdateMinPeriodMs, nullptr);
		}
		else
		{
			m_UpdatePending = true;
		}
	}
}

void LogGrid::OnTimer(UINT_PTR nIDEvent)
{
	if (g_TimerID == nIDEvent)
	{
		//if (!IsPaused())
		{
			m_TimerRunning = false;
			KillTimer(g_TimerID);
			// can't be used as we add rows...
			//UpdateMegaSharkOptimizedAllColumns();
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		}
	}
	else
	{
		CacheGrid::OnTimer(nIDEvent);
	}
}

wstring LogGrid::getLogLevelText(LogLevel level)
{
	switch (level)
	{
	case LogLevel::Debug:
		return g_StrDebugLevel;
	case LogLevel::Information:
		return g_StrInfoLevel;
	case LogLevel::Warning:
		return g_StrWarningLevel;
	case LogLevel::Error:
		return g_StrErrorLevel;
	case LogLevel::Fatal:
		return g_StrFatalLevel;
	case LogLevel::User:
		return g_StrUserLevel;
	case LogLevel::numLogLevels:
	default:
		break;
	}

	ASSERT(false);
	return _YTEXT("");
}

//void LogGrid::postSetPaused()
//{
//	if (!IsPaused())
//	{
//	}
//}