#include "BackstagePanelJobCenterCommon.h"

#include "AutomationWizardCommon.h"

BEGIN_MESSAGE_MAP(BackstagePanelJobCenterCommon, BackstagePanelJobCenterBase)
END_MESSAGE_MAP()

BackstagePanelJobCenterCommon::BackstagePanelJobCenterCommon(CodeJockFrameBase* p_Frame)
	: BackstagePanelJobCenterBase(p_Frame, IDD, false)
{
	BackstagePanelJobCenterBase::SetSimpleView(false);
}

BackstagePanelJobCenterCommon::~BackstagePanelJobCenterCommon()
{
}

void BackstagePanelJobCenterCommon::setThemeSpecific(const XTPControlTheme nTheme)
{
}

void BackstagePanelJobCenterCommon::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelJobCenterBase::DoDataExchange(pDX);
}

BOOL BackstagePanelJobCenterCommon::OnInitDialogSpecific()
{
	return TRUE;
}

void BackstagePanelJobCenterCommon::OnSetActive()
{
	SetConfigurationWasUpdated(false);
	isDebugMode();

	auto gridFrame = dynamic_cast<GridFrameBase*>(m_Frame);
	auto mainFrame = dynamic_cast<MainFrame*>(m_Frame);

	static AutomationWizardCommon commonJC;
	m_JobCenter = &commonJC;
	if (nullptr != gridFrame)
	{
		m_Grid.LoadScripts(m_JobCenter);
	}
	else if (nullptr != mainFrame)
	{
		if (mainFrame->GetSapio365Session())// main frame grid cannot be loaded before a session is set
			m_Grid.LoadScripts(m_JobCenter);
	}
	else
	{
		m_JobCenter = nullptr;
		ASSERT(false);
		return;
	}

	updateButtons();
}

void BackstagePanelJobCenterCommon::SetSimpleView(bool p_SimpleView)
{
	// Only exists in regular view.
	ASSERT(!p_SimpleView);
}

void BackstagePanelJobCenterCommon::updateButtons()
{
	m_BtnImport.EnableWindow(FALSE);
	m_BtnExport.EnableWindow(FALSE);
	m_BtnUpdate.EnableWindow(FALSE);
	m_BtnRemove.EnableWindow(FALSE);
	m_BtnEdit.EnableWindow(FALSE);

	if (!isDebugMode() && getSession())
	{
		vector<Script>			scripts;
		vector<ScriptPreset>	scriptPresets;
		vector<ScriptSchedule>	scriptSchedules;
		m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);

		ASSERT(scriptPresets.empty());
		ASSERT(scriptSchedules.empty());

		bool uUpdate = false;
		bool uRemove = false;
		for (const auto& s : scripts)
		{
			const bool editable = !s.IsSystem();
			if (editable)
				uRemove = true;
			if (editable && FileUtil::FileExists(s.m_SourceXMLfilePath))
				uUpdate = true;
			if (uUpdate && uRemove)
				break;
		}

		m_BtnImport.EnableWindow(TRUE);
		m_BtnExport.EnableWindow(!scripts.empty() ? TRUE : FALSE);
		m_BtnUpdate.EnableWindow(uUpdate ? TRUE : FALSE);
		m_BtnRemove.EnableWindow(uRemove ? TRUE : FALSE);
		m_BtnEdit.EnableWindow(uRemove && scripts.size() == 1 ? TRUE : FALSE);
	}
}