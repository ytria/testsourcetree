#include "BackstagePanelRoleDelegationConfigurationKeys.h"

#include "BackstageLogger.h"
#include "FileUtil.h"
#include "MainFrame.h"
#include "Sapio365Session.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTranslate.h"

BEGIN_MESSAGE_MAP(BackstagePanelRoleDelegationConfigurationKeys, BackstagePanelWithGrid<RoleDelegationCfgKeysGrid>)
END_MESSAGE_MAP()


BackstagePanelRoleDelegationConfigurationKeys::BackstagePanelRoleDelegationConfigurationKeys()
	: BackstagePanelWithGrid<RoleDelegationCfgKeysGrid>(IDD)
{
	m_Grid = std::make_shared<RoleDelegationCfgKeysGrid>();
}

BackstagePanelRoleDelegationConfigurationKeys::~BackstagePanelRoleDelegationConfigurationKeys()
{
}

BOOL BackstagePanelRoleDelegationConfigurationKeys::OnInitDialog()
{
	BackstagePanelWithGrid<RoleDelegationCfgKeysGrid>::OnInitDialog();
	setCaption(YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationKeys_OnInitDialog_1, _YLOC("Admin Connection Keys")).c_str());
	return TRUE;
}

void BackstagePanelRoleDelegationConfigurationKeys::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelWithGrid<RoleDelegationCfgKeysGrid>::DoDataExchange(pDX);
}

void BackstagePanelRoleDelegationConfigurationKeys::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelWithGrid<RoleDelegationCfgKeysGrid>::SetTheme(nTheme);
}

BOOL BackstagePanelRoleDelegationConfigurationKeys::OnSetActive()
{
	if (BackstagePanelWithGrid<RoleDelegationCfgKeysGrid>::OnSetActive() == TRUE)
	{
		m_Grid->SetIsActive(true);
		return TRUE;
	}

	return FALSE;
}

BOOL BackstagePanelRoleDelegationConfigurationKeys::OnKillActive()
{
	if (BackstagePanelWithGrid<RoleDelegationCfgKeysGrid>::OnKillActive() == TRUE)
	{
		m_Grid->SetIsActive(false);
		return TRUE;
	}

	return FALSE;
}