#include "BackstagePanelAutomationShortcuts.h"

#include "AutomatedApp.h"
#include "SharedResource.h"
#include "ShortcutsManager.h"
#include "YtriaTranslate.h"

BEGIN_MESSAGE_MAP(BackstagePanelAutomationShortcuts, CXTPRibbonBackstagePage)
	ON_UPDATE_COMMAND_UI(IDOK, OnUpdateOK)
	ON_UPDATE_COMMAND_UI(IDCANCEL, OnUpdateCancel)
END_MESSAGE_MAP()

BackstagePanelAutomationShortcuts::BackstagePanelAutomationShortcuts(AutomatedApp* i_TheApp)
	: CXTPRibbonBackstagePage(IDD)
	, m_TheApp(i_TheApp)
	, m_shortcutsManager(nullptr)
{
	ASSERT(nullptr != m_TheApp);

	m_shortcutsManager = i_TheApp->GetShortcutManager();
	ASSERT(nullptr != m_shortcutsManager);
	if (nullptr != m_shortcutsManager)
	{
		m_shortcutsManager->UpdateData();
		const PRODUCT_SHORTCUTS& pdtMap = m_shortcutsManager->GetProductShortcuts();
		for (const auto& item : pdtMap)
			m_mapData[item.first] = pair<wstring, bool>(item.second, m_shortcutsManager->IsAppliedForAll(item.first, item.second));
	}
}

void BackstagePanelAutomationShortcuts::SetTheme(const XTPControlTheme nTheme)
{
	CXTPRibbonBackstagePage::SetTheme(nTheme);

	m_btnOk.SetTheme(GetTheme());
	m_btnCancel.SetTheme(GetTheme());
	m_staticExplanation.SetTheme(GetTheme());
}

void BackstagePanelAutomationShortcuts::DoDataExchange(CDataExchange* pDX)
{
	CXTPRibbonBackstagePage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BACKSTAGE_CAPTION, m_staticExplanation);
	DDX_Control(pDX, IDC_BACKSTAGE_GRID_AREA, m_staticGridArea);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDOK, m_btnOk);
}

BOOL BackstagePanelAutomationShortcuts::OnInitDialog()
{
	CXTPRibbonBackstagePage::OnInitDialog();

	ModifyStyleEx(0, WS_EX_CONTROLPARENT);

	m_staticExplanation.SetWindowText(CString(YtriaTranslate::Do(DlgAutomationShortcuts_OnInitDialogSpecificResizable_2, _YLOC("Set the path of each automation file to be launched by the keyboard shortcut: Ctrl+Shift+<Number>:")).c_str()));

	// Current non editable values of the column
	CRect aRect;
	m_staticGridArea.GetWindowRect(&aRect);
	m_staticGridArea.ShowWindow(SW_HIDE);
	ScreenToClient(&aRect);
	if (!m_gridShortcuts.Create(this, aRect, IDC_BACKSTAGE_GRID))
	{
		// Creation of grid failed.
		ASSERT(FALSE);
		return -1;
	}
	m_gridShortcuts.ShowWindow(SW_SHOW);
	m_gridShortcuts.BringWindowToTop();

	m_gridShortcuts.AddData(m_mapData, m_TheApp);

	m_gridShortcuts.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	m_btnCancel.SetWindowText(CString(YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("&Cancel")).c_str()));
	m_btnOk.SetWindowText(CString(YtriaTranslate::Do(CDlg_OnInitDialog_2, _YLOC("&OK")).c_str()));

	SetResize(IDC_BACKSTAGE_CAPTION, XTP_ATTR_HORRESIZE(1.f));
	SetResize(IDC_BACKSTAGE_GRID, XTP_ATTR_HORRESIZE(1.f));
	SetResize(IDOK, XTP_ATTR_HORREPOS(1.f));
	SetResize(IDCANCEL, XTP_ATTR_HORREPOS(1.f));

	return TRUE;
}

void BackstagePanelAutomationShortcuts::OnOK()
{
	//if (m_gridShortcuts.HasOneNonGroupRowModifiedAtLeast())
	{
		for (auto pRow : m_gridShortcuts.GetBackendRows())
		{
			if (pRow->GridBackendRow::IsDeleted())
				pRow->AddFieldByTitle(YtriaTranslate::Do(YtriaGridAutoShortcuts_customizeGrid_4, _YLOC("Filepath")).c_str(), wstring());
		}

		m_gridShortcuts.GetGridData(m_mapData);

		// Data have been modified
		PRODUCT_SHORTCUTS newShorts;
		map<int, pair<wstring, bool>>::const_iterator itShort = m_mapData.cbegin();
		for (; itShort != m_mapData.cend(); ++itShort)
		{
			// Apply to all ?
			bool isApplyAll = itShort->second.second;
			wstring filepath(itShort->second.first);
			bool oldAll = m_shortcutsManager->IsAppliedForAll(itShort->first, filepath);
			if (isApplyAll != oldAll && !filepath.empty())
				m_shortcutsManager->SetShorcutForAllProducts(itShort->first, filepath, !isApplyAll);

			newShorts[itShort->first] = filepath;
		}
		m_shortcutsManager->SetProductShortcuts(newShorts);

		m_shortcutsManager->StoreData();
	}
}

void BackstagePanelAutomationShortcuts::OnCancel()
{
	//if (m_gridShortcuts.HasOneNonGroupRowModifiedAtLeast())
	{
		m_gridShortcuts.RemoveAllRows();
		m_gridShortcuts.AddData(m_mapData, m_TheApp);
		m_gridShortcuts.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}
}

void BackstagePanelAutomationShortcuts::OnUpdateOK(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(/*m_gridShortcuts.HasOneNonGroupRowModifiedAtLeast() ? */TRUE/* : FALSE*/);
}

void BackstagePanelAutomationShortcuts::OnUpdateCancel(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(/*m_gridShortcuts.HasOneNonGroupRowModifiedAtLeast() ? */TRUE/* : FALSE*/);
}

BOOL BackstagePanelAutomationShortcuts::OnKillActive()
{
	ASSERT(false);
	//// Finish this !!!
	//// We could probably save changes here and get rid of OK button.
	//OnOK();
	return TRUE;
}
