#include "RoleDelegationCfgFiltersGrid.h"

#include "BusinessUserConfiguration.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessSiteConfiguration.h"
#include "../Resource.h"
#include "RoleDelegationManager.h"

RoleDelegationCfgFiltersGrid::RoleDelegationCfgFiltersGrid() : RoleDelegationCfgGridBase(),
	m_ColumnFilterType(nullptr),
	m_ColumnProperties(nullptr),
	m_ColumnTaggerMatchMethod(nullptr)
{
}

void RoleDelegationCfgFiltersGrid::customizeGridSpecific()
{
	SetAutomationName(_YTEXT("RoleDelegationCfgFiltersGrid"));
	SetAutomationActionRecording(false);

	// filter types
	{
		auto icon = MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_USER);
		AddImage(icon);
	}
	{
		auto icon = MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_SITE);
		AddImage(icon);
	}
	{
		auto icon = MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_GROUP);
		AddImage(icon);
	}

	SetSQLiteEngine(RoleDelegationManager::GetInstance().GetRoleDelegationSQLite(), RoleDelegationManager::GetInstance().GetRoleDelegationSQLite().GetTableFilters());
}

void RoleDelegationCfgFiltersGrid::customizeGridPostProcessSpecific()
{
	m_ColumnFilterType = GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameObjectType);
	// Size of an IconColumn
	m_ColumnFilterType->SetWidthMin(HIDPI_XW(GridBackendUtil::g_MinColWidth));
	m_ColumnFilterType->SetDefaultWidth(HIDPI_XW(GridBackendUtil::g_MinColWidth));
	m_ColumnFilterType->SetWidth(HIDPI_XW(GridBackendUtil::g_MinColWidth));
	MoveColumnBefore(m_ColumnFilterType, m_ColumnName);

	m_ColumnProperties = GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameProperty);
	m_ColumnProperties->SetWidth(m_ColumnSize22);

	GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameValue)->SetWidth(m_ColumnSize32);

	m_ColumnTaggerMatchMethod = GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameTaggerMatchMethod);
	m_ColumnTaggerMatchMethod->SetWidth(m_ColumnSize22);
}

void RoleDelegationCfgFiltersGrid::loadPostProcessSpecific(GridBackendRow* p_Row)
{
	const auto& g_FilterTypes = RoleDelegationUtil::FilterTypes::GetInstance();

	const auto& ucfg = BusinessUserConfiguration::GetInstance();
	const auto& gcfg = BusinessGroupConfiguration::GetInstance();
	const auto& scfg = BusinessSiteConfiguration::GetInstance();

	wstring fType		= p_Row->GetField(m_ColumnFilterType).GetValueStr();
	wstring fProperty	= p_Row->GetField(m_ColumnProperties).GetValueStr();

	if (fType == g_FilterTypes.GetTypeGroup())
	{
		p_Row->AddField(g_FilterTypes.GetNameGroup(), m_ColumnFilterType, ICON_GROUP);
		p_Row->AddField(gcfg.GetTitle(fProperty), m_ColumnProperties);
	}
	else if (fType == g_FilterTypes.GetTypeSite())
	{
		p_Row->AddField(g_FilterTypes.GetNameSite(), m_ColumnFilterType, ICON_SITE);
		p_Row->AddField(scfg.GetTitle(fProperty), m_ColumnProperties);
	}
	else if (fType == g_FilterTypes.GetTypeUser())
	{
		p_Row->AddField(g_FilterTypes.GetNameUser(), m_ColumnFilterType, ICON_USER);
		p_Row->AddField(ucfg.GetTitle(fProperty), m_ColumnProperties);
	}

	const uint32_t tmm = static_cast<uint32_t>(p_Row->GetField(m_ColumnTaggerMatchMethod).GetValueULong());
	p_Row->AddField(RoleDelegationUtil::GetTaggerMatchMethod(tmm), m_ColumnTaggerMatchMethod);
}

void RoleDelegationCfgFiltersGrid::prepareSQLoadSpecific()
{

}