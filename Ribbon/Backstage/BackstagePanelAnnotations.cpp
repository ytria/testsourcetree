#include "BackstagePanelAnnotations.h"

#include "AnnotationManager.h"
//#include "DlgGridAnnotations.h"
#include "GridUtil.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "YtriaTranslate.h"

//BEGIN_MESSAGE_MAP(BackstagePanelAnnotations, BackstagePanelWithGrid<GridModuleAnnotations>)
	//ON_BN_CLICKED(ID_BACKSTAGE_LOG_PAUSE, OnPause)
// 	ON_BN_CLICKED(ID_BACKSTAGE_ANNOTATION_EDIT,		OnEdit)
// 	ON_BN_CLICKED(ID_BACKSTAGE_ANNOTATION_DELETE,	OnDelete)
//END_MESSAGE_MAP()

BackstagePanelAnnotations::BackstagePanelAnnotations() : BackstagePanelWithGrid<GridModuleAnnotations>(IDD)
{
	m_Grid = std::make_shared<GridModuleAnnotations>(ID_BACKSTAGE_ANNOTATION_EDIT, GridUtil::ModuleName);
}

BackstagePanelAnnotations::~BackstagePanelAnnotations()
{
}

void BackstagePanelAnnotations::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelWithGrid<GridModuleAnnotations>::DoDataExchange(pDX);
// 
// 	DDX_Control(pDX, ID_BACKSTAGE_ANNOTATION_EDIT,		m_BtnEdit);
// 	DDX_Control(pDX, ID_BACKSTAGE_ANNOTATION_DELETE,	m_BtnDelete);
}
// 
// void BackstagePanelAnnotations::SelectionChanged(const CacheGrid& grid)
// {
// 	BOOL enable = grid.HasOneNonGroupRowSelectedAtLeast() ? TRUE : FALSE;
// // 	m_BtnEdit.EnableWindow(enable);
// // 	m_BtnDelete.EnableWindow(enable);
// }

BOOL BackstagePanelAnnotations::OnInitDialog()
{
	BackstagePanelWithGrid<GridModuleAnnotations>::OnInitDialog();
// 
// 	ASSERT(m_Grid);
// 	if (m_Grid)
// 		m_Grid->AddSelectionObserver(this);

	setCaption(YtriaTranslate::Do(BackstagePanelAnnotations_OnInitDialog_4, _YLOC("All Comments")).c_str());
// 	
// 	{
// 		UINT ids[]{ ID_BACKSTAGE_ANNOTATION_EDIT };
// 		m_imagelist.SetIcons(RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_EDIT, ids, 1, CSize(0, 0), xtpImageNormal);
// 		SetupButton(m_BtnEdit, YtriaTranslate::Do(DlgBusinessEditHTML_getDialogTitle_2, _YLOC("Edit")).c_str(), _YTEXT(""), m_imagelist);
// 		m_BtnEdit.EnableWindow(FALSE);
// 	}
// 	{
// 		UINT ids[]{ ID_BACKSTAGE_ANNOTATION_DELETE };
// 		m_imagelist.SetIcons(RPROD_BMP_RIBBON_CACHEGRID_MENU_ANNOTATION_DELETE, ids, 1, CSize(0, 0), xtpImageNormal);
// 		SetupButton(m_BtnDelete, YtriaTranslate::Do(ApplicationShortcuts_ApplicationShortcuts_166, _YLOC("Delete")).c_str(), _YTEXT(""), m_imagelist);
// 		m_BtnDelete.EnableWindow(FALSE);
// 	}

	return TRUE;
}

void BackstagePanelAnnotations::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelWithGrid<GridModuleAnnotations>::SetTheme(nTheme);
}

void BackstagePanelAnnotations::writeAnnotation(GridAnnotation& p_Annotation)
{	
	p_Annotation.m_UpdDate = YTimeDate::GetCurrentTimeDate();
	AnnotationManager::GetInstance().Write(p_Annotation, getSession());
	if (!p_Annotation.m_WriteError_VOL.empty())
	{
		YCodeJockMessageBox dlgWhine(this,
									 DlgMessageBox::eIcon_ExclamationWarning,
									 p_Annotation.m_WriteError_VOL.c_str(),
									 _YTEXT(""),
									 _YTEXT(""),
									 { { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });

		dlgWhine.DoModal();
	}
}

BOOL BackstagePanelAnnotations::OnSetActive()
{
	if (BackstagePanelWithGrid<GridModuleAnnotations>::OnSetActive() == TRUE)
	{
		fillgrid();
// 		SelectionChanged(*m_Grid);
		return TRUE;
	}

	return FALSE;
}

BOOL BackstagePanelAnnotations::OnKillActive()
{
	if (BackstagePanelWithGrid<GridModuleAnnotations>::OnKillActive() == TRUE)
	{
		return TRUE;
	}

	return FALSE;
}

std::shared_ptr<Sapio365Session> BackstagePanelAnnotations::getSession() const
{
	return MainFrameSapio365Session();
}

void BackstagePanelAnnotations::fillgrid()
{
	CWaitCursor _;
	ASSERT(m_Grid);
	if (getSession() && m_Grid)
	{
		vector<GridAnnotation> annotations;
		AnnotationManager::GetInstance().UpdateFromCloudAndLoadAnnotationsAll(getSession(), annotations, this);
		m_Grid->SetAnnotations(annotations);
		m_Grid->FillGrid();
	}
}
