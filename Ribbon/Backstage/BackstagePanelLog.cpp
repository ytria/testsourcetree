#include "BackstagePanelLog.h"

#include "ActivitySQLiteLogger.h"
#include "AnnotationSQLite.h"
#include "BackstageLogger.h"
#include "FileUtil.h"
#include "JobCenterSQLite.h"
#include "MainFrame.h"
#include "O365SQLiteEngine.h"
#include "RoleDelegationSQLite.h"
#include "Sapio365Session.h"
#include "SQLiteUtil.h"
#include "TraceIntoFile.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTranslate.h"

BEGIN_MESSAGE_MAP(BackstagePanelLog, BackstagePanelWithGrid<LogGrid>)
	//ON_BN_CLICKED(ID_BACKSTAGE_LOG_PAUSE, OnPause)
	ON_BN_CLICKED(ID_BACKSTAGE_LOG_CLEAR,		OnClear)
	ON_BN_CLICKED(ID_BACKSTAGE_LOG_SENDTRACE,	OnSend)
	ON_BN_CLICKED(ID_BACKSTAGE_LOG_DUMPMODE,	OnDumpMode)
END_MESSAGE_MAP()


BackstagePanelLog::BackstagePanelLog(CFrameWnd& frame)
	: BackstagePanelWithGrid<LogGrid>(IDD)
{
	CWnd* mainWnd = ::AfxGetApp()->GetMainWnd();
	bool isMainframe = mainWnd != nullptr && frame.GetSafeHwnd() == mainWnd->GetSafeHwnd();
	m_Grid = std::make_shared<LogGrid>(frame, !isMainframe);
	ASSERT(BackstageLogger::HasInstance());
	if (BackstageLogger::HasInstance())
		BackstageLogger::Instance()->AddLogger(m_Grid);
}

BackstagePanelLog::~BackstagePanelLog()
{
	ASSERT(BackstageLogger::HasInstance());
	if (BackstageLogger::HasInstance())
		BackstageLogger::Instance()->RemoveLogger(m_Grid);
}

BOOL BackstagePanelLog::OnInitDialog()
{
	BackstagePanelWithGrid<LogGrid>::OnInitDialog();

	setCaption(YtriaTranslate::Do(BackstagePanelLog_OnInitDialog_1, _YLOC("Process Log")).c_str());

	{
		UINT ids[]{ ID_BACKSTAGE_LOG_CLEAR };
		m_imagelist.SetIcons(IDB_BACKSTAGE_LOGS_CLEAR
			, ids
			, 1
			, CSize(0, 0)
			, xtpImageNormal);

		// Load button
		SetupButton(m_btnClear
			, YtriaTranslate::Do(BackstagePanelLog_OnInitDialog_2, _YLOC("Clear All Log Entries")).c_str()
			, _YTEXT("")
			, m_imagelist);
	}

	m_btnClear.EnableWindow();

	if (nullptr != MFCUtil::FindFirstParentOfType<MainFrame>(this)) // Are we in Main Frame backstage?
	{
		{
			UINT ids[]{ ID_BACKSTAGE_LOG_SENDTRACE };
			m_imagelist.SetIcons(IDB_BACKSTAGE_LOGS_BTN_SENDTRACE
				, ids
				, 1
				, CSize(0, 0)
				, xtpImageNormal);

			// Load button
			SetupButton(m_btnSend
				, YtriaTranslate::Do(BackstagePanelLog_OnInitDialog_3, _YLOC("Send trace to Ytria")).c_str()
				, _YTEXT("")
				, m_imagelist);
		}

		{
			UINT ids[]{ ID_BACKSTAGE_LOG_DUMPMODE };
			m_imagelist.SetIcons(IDB_BACKSTAGE_LOGS_BTN_DUMPMODE
				, ids
				, 1
				, CSize(0, 0)
				, xtpImageNormal);

			// Dump Mode button
			SetupButton(m_btnDump
				, YtriaTranslate::Do(BackstagePanelLog_OnInitDialog_4, _YLOC("Toggle Debug mode")).c_str()
				, _YTEXT("\x26a0 ") + wstring(YtriaTranslate::Do(BackstagePanelLog_OnInitDialog_5, _YLOC("(recommended for support only)")).c_str()) + _YTEXT(" \x26a0")
				, m_imagelist);
		}

		m_btnDump.EnableWindow();
		m_btnDump.ShowWindow(SW_SHOW);
		m_btnDump.SetCheck(Sapio365Session::IsDumpGloballyEnabled());

		m_btnSend.EnableWindow(/*TraceIntoFile::IsEnabled() && */!Sapio365Session::IsDumpGloballyEnabled());
		m_btnSend.ShowWindow(SW_SHOW);
	}
	else
	{
		m_btnDump.EnableWindow(FALSE);
		m_btnDump.ShowWindow(SW_HIDE);

		m_btnSend.EnableWindow(FALSE);
		m_btnSend.ShowWindow(SW_HIDE);
	}

	return TRUE;
}

void BackstagePanelLog::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelWithGrid<LogGrid>::DoDataExchange(pDX);

	//DDX_Control(pDX, ID_BACKSTAGE_LOG_PAUSE, m_btnPause);
	DDX_Control(pDX, ID_BACKSTAGE_LOG_CLEAR, m_btnClear);
	DDX_Control(pDX, ID_BACKSTAGE_LOG_SENDTRACE, m_btnSend);
	DDX_Control(pDX, ID_BACKSTAGE_LOG_DUMPMODE, m_btnDump);
}

void BackstagePanelLog::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelWithGrid<LogGrid>::SetTheme(nTheme);

	m_btnClear.SetTheme(nTheme);
	m_btnSend.SetTheme(nTheme);
	m_btnDump.SetTheme(nTheme);
}

BOOL BackstagePanelLog::OnSetActive()
{
	m_btnDump.SetCheck(Sapio365Session::IsDumpGloballyEnabled());
	if (BackstagePanelWithGrid<LogGrid>::OnSetActive() == TRUE)
	{
		m_Grid->SetIsActive(true);
		return TRUE;
	}

	return FALSE;
}

BOOL BackstagePanelLog::OnKillActive()
{
	if (BackstagePanelWithGrid<LogGrid>::OnKillActive() == TRUE)
	{
		m_Grid->SetIsActive(false);
		return TRUE;
	}

	return FALSE;
}

//void BackstagePanelLog::OnPause()
//{
//	m_Grid->SetPaused(!m_Grid->IsPaused());
//}

void BackstagePanelLog::OnClear()
{
	m_Grid->RemoveAllRows();
}

void BackstagePanelLog::OnSend()
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	if (nullptr != mainFrame)
	{
		ASSERT(FileUtil::ListFiles(Sapio365Session::GetDumpPath(), _YTEXT("log")).empty());
		ASSERT(mainFrame->GetSapio365Session());

		vector<wstring> additionalFiles;
		if (SQLiteUtil::SQLAppContext().IsDumpMode())
		{
			if (mainFrame->GetSapio365Session())
			{
				YCodeJockMessageBox dlg(this,
					DlgMessageBox::eIcon_ExclamationWarning,
					_T("Ytria Special Support"),
					_T("We're going to gather extended information. This may take a while."),
					_YTEXT(""),
					{ { IDOK, _T("Ok") }, { IDCANCEL, _T("Cancel") } });

				if (IDCANCEL == dlg.DoModal())
					return;

				static const wstring g_SQLDumpSite = FileUtil::GetTempFolder();

				CWaitCursor _;

				TraceIntoFile::trace(_YDUMP("DUMP"), _YDUMP("ORG"), _YTEXT("START"));

				wstring suffix;
				{
					UUID uuid;
					wchar_t* str;
					if (RPC_S_OK == UuidCreate(&uuid) && RPC_S_OK == UuidToString(&uuid, (RPC_WSTR*)&str))
					{
						if (nullptr != str)
							suffix = _YTEXTFORMAT(L"_%s", str);
						RpcStringFree((RPC_WSTR*)&str);
					}
				}

				auto app = AutomatedApp::GetAutomatedApp();
				auto dumpFileRBAC = RoleDelegationSQLite().Dump(g_SQLDumpSite, O365Krypter(mainFrame->GetSapio365Session()), suffix);
				if (!dumpFileRBAC.empty())
				{
					additionalFiles.push_back(dumpFileRBAC);
					app->addTemporaryFiles(dumpFileRBAC);
				}

				auto dumpFileAnnotations = AnnotationSQLite().Dump(g_SQLDumpSite, O365Krypter(mainFrame->GetSapio365Session()), suffix);
				if (!dumpFileAnnotations.empty())
				{
					additionalFiles.push_back(dumpFileAnnotations);
					app->addTemporaryFiles(dumpFileAnnotations);
				}

				auto dumpFileJobCenter = JobCenterSQLite().Dump(g_SQLDumpSite, O365Krypter(mainFrame->GetSapio365Session()), suffix);
				if (!dumpFileJobCenter.empty())
				{
					additionalFiles.push_back(dumpFileJobCenter);
					app->addTemporaryFiles(dumpFileJobCenter);
				}

				auto dumpFileLogs = ActivitySQLiteLogger().Dump(g_SQLDumpSite, O365Krypter(mainFrame->GetSapio365Session()), suffix);
				if (!dumpFileLogs.empty())
				{
					additionalFiles.push_back(dumpFileLogs);
					app->addTemporaryFiles(dumpFileLogs);
				}

				TraceIntoFile::trace(_YDUMP("DUMP"), _YDUMP("ORG"), mainFrame->GetSapio365Session()->GetGraphCache().GetOrganization(YtriaTaskData()).get().m_Id.c_str());
			}
			else
			{
				YCodeJockMessageBox errorMessage(	this,
													DlgMessageBox::eIcon_ExclamationWarning,
													_T("No active session"),
													_T("Please log into a session to send your SQL content"),
													_YTEXT(""),
													{ { IDOK, _T("Ok") } });
				errorMessage.DoModal();
			}
		}

		mainFrame->SendTrace(additionalFiles, this);
	}
}

void BackstagePanelLog::OnDumpMode()
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	if (nullptr != mainFrame)
		mainFrame->ToggleDumpMode(this, true);

	m_btnDump.SetCheck(Sapio365Session::IsDumpGloballyEnabled());
	m_btnSend.EnableWindow(/*TraceIntoFile::IsEnabled() && */!Sapio365Session::IsDumpGloballyEnabled());
}
