#include "BackstagePanelJobCenterBase.h"

#include "AutomationWizardCommon.h"
#include "BackstagePanelBase.h"
#include "DlgAutomationWizardEditHTML.h"
#include "DlgJobCenterExport.h"
#include "DlgJobCenterImportConfirmReplace.h"
#include "DlgJobPresetEdit.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "..\Resource.h"
#include "YFileDialog.h"

BEGIN_MESSAGE_MAP(BackstagePanelJobCenterBase, CExtResizableDialog)
	ON_WM_CREATE()
	ON_WM_CTLCOLOR() // Copy pasted from CXTPRibbonBackstagePage for color handling
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_IMPORT,	onImport)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_EXPORT,	onExport)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_EDIT,		OnEdit)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_REMOVE,	onRemove)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_UPDATE,	onUpdate)
END_MESSAGE_MAP()

BackstagePanelJobCenterBase::BackstagePanelJobCenterBase(CodeJockFrameBase* p_Frame, UINT nID, bool p_ForModule)
	: CExtResizableDialog(nID)
	, m_Grid(p_ForModule)
	, m_Frame(p_Frame)
	, m_ConfigurationWasUpdated(false)
	, m_JobCenter(nullptr)
	, m_ForModule(p_ForModule)
{
}

void BackstagePanelJobCenterBase::SetTheme(const XTPControlTheme nTheme)
{
	/* Copy pasted from CXTPRibbonBackstagePage::SetTheme for color handling */
	// get the background color from the selected theme if available.
	m_clrText = XTPIniColor(_YTEXT("CommandBars.Ribbon.Backstage"), _YTEXT("PageText"), ::GetSysColor(COLOR_WINDOWTEXT));
	m_clrBack = XTPIniColor(_YTEXT("CommandBars.Ribbon.Backstage"), _YTEXT("PageBackground"), ::GetSysColor(COLOR_WINDOW));

	// destroy the previous brush.
	if (m_xtpBrushBack.GetSafeHandle())
		m_xtpBrushBack.DeleteObject();

	// create a new brush.
	m_xtpBrushBack.CreateSolidBrush(m_clrBack);
	/**********************/

	m_BtnImport.SetTheme(nTheme);
	m_BtnExport.SetTheme(nTheme);
	m_BtnEdit.SetTheme(nTheme);
	m_BtnRemove.SetTheme(nTheme);
	m_BtnUpdate.SetTheme(nTheme);
	setThemeSpecific(nTheme);
}

HBRUSH BackstagePanelJobCenterBase::OnCtlColor(CDC* pDC, CWnd* /*pWnd*/, UINT /*nCtlColor*/)
{
	/* Copy pasted from CXTPRibbonBackstagePage::OnCtlColor for color handling */
	pDC->SetTextColor(m_clrText);
	pDC->SetBkColor(m_clrBack);
	return (HBRUSH)m_xtpBrushBack;
}

void BackstagePanelJobCenterBase::DoDataExchange(CDataExchange* pDX)
{
	CWnd::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_IMPORT, m_BtnImport);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_EXPORT, m_BtnExport);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_EDIT,	m_BtnEdit);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_REMOVE, m_BtnRemove);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_UPDATE, m_BtnUpdate);
	DDX_Control(pDX, IDC_BACKSTAGE_GRID_AREA,				m_staticGridArea);
}

BOOL BackstagePanelJobCenterBase::OnInitDialog()
{
	BOOL rv = CDialog::OnInitDialog();

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_IMPORT };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_IMPORT, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnImport, _YTEXT("IMPORT"), _YTEXT(""), m_ImageList);

		// FIXME: Finish this

		// REGULAR tooltip
		{
			m_BtnImport.SetTooltip(_T("Add new Job...\nSelect an XML script to add to the Job Center in sapio365."));
		}
		// or
		// HTML tooltip
		/*{
			m_BtnImport.GetToolTipContext()->SetTipBkColor(RGB(255, 255, 255));
			m_BtnImport.GetToolTipContext()->SetStyle(xtpToolTipHTML);
			m_BtnImport.SetTooltip(_T("<b>Add new Job...</b><br/>Select an XML script to add to the Job Center in sapio365."));
		}*/
		// or
		// XAML Tooltip
		/*{
			m_BtnImport.GetToolTipContext()->SetStyle(xtpToolTipMarkup);
			m_BtnImport.SetTooltip(_T("<StackPanel Margin='5, 3, 5, 3'><TextBlock FontSize='12' FontFamily='Segoe UI' Foreground='#000000'><Bold>Add new Job...</Bold></TextBlock><TextBlock FontSize='12' FontFamily='Segoe UI' Foreground='#000000'>Select an XML script to add to the Job Center in sapio365.</TextBlock></StackPanel>"));
		}*/
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_EXPORT };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_EXPORT, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnExport, _YTEXT("EXPORT"), _YTEXT(""), m_ImageList);
		m_BtnExport.SetTooltip(_T("Save to file...\nBackup all selected jobs to individual XML files."));
		if (IsSimpleView())
			m_BtnExport.ShowWindow(SW_HIDE);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_UPDATE };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_UPDATE, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnUpdate, _YTEXT("REFRESH"), _YTEXT(""), m_ImageList);
		m_BtnUpdate.SetTooltip(_T("Update from file\nUpdate all selected personally added jobs from their original XML source files."));
		if (IsSimpleView())
			m_BtnUpdate.ShowWindow(SW_HIDE);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_EDIT };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_EDIT, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnEdit, _YTEXT("EDIT"), _YTEXT(""), m_ImageList);
		m_BtnEdit.SetTooltip(_T("Edit Job...\nChange the job's meta data: title, description, tooltip and icon."));
		if (IsSimpleView())
			m_BtnEdit.ShowWindow(SW_HIDE);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_REMOVE };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_REMOVE, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnRemove, _YTEXT("DELETE"), _YTEXT(""), m_ImageList);
		m_BtnRemove.SetTooltip(_T("Remove from Grid\nEliminate selected jobs and 'presets' from the list. Note that system-level jobs are protected and cannot be removed."));
	}

	CRect aRect;
	m_staticGridArea.GetWindowRect(&aRect);
	m_staticGridArea.ShowWindow(SW_HIDE);
	ScreenToClient(&aRect);

	if (m_Grid.Create(this, aRect, IDC_BACKSTAGE_GRID_AREA))
	{
		m_Grid.ModifyStyle(0, WS_BORDER);
		m_Grid.ShowWindow(SW_SHOW);
		m_Grid.BringWindowToTop();
	}
	else
	{
		ASSERT(FALSE);
		rv = -1;
	}

	SetBkColor(RGB(255, 255, 255));
	AddAnchor(IDC_BACKSTAGE_GRID_AREA, CSize(0, 0), CSize(100, 100));
	m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);// create headers
	m_Grid.AddSelectionObserver(this);

	OnInitDialogSpecific();

	if (IsSimpleView())
	{
		if (!m_ForModule)
		{
			invertRemoveAndImport();
		}
		else
		{
			m_BtnRemove.ShowWindow(SW_HIDE);
		}
		m_BtnImport.ShowWindow(SW_HIDE);
	}

	return rv;
}

void BackstagePanelJobCenterBase::SetConfigurationWasUpdated(const bool p_Updated)
{
	m_ConfigurationWasUpdated = p_Updated;
}

void BackstagePanelJobCenterBase::SetSimpleView(bool p_SimpleView)
{
	const bool wasSimpleView = IsSimpleView();
	m_Grid.SetSimpleView(p_SimpleView);

	if (::IsWindow(m_BtnRemove.GetSafeHwnd())) // Did we init the dialog already?
	{
		if (wasSimpleView != IsSimpleView() && !m_ForModule)
			invertRemoveAndImport();

		if (IsSimpleView())
		{
			if (m_ForModule)
				m_BtnRemove.ShowWindow(SW_HIDE);
			m_BtnEdit.ShowWindow(SW_HIDE);
			m_BtnUpdate.ShowWindow(SW_HIDE);
			m_BtnExport.ShowWindow(SW_HIDE);
			m_BtnImport.ShowWindow(SW_HIDE);
		}
		else
		{
			if (m_ForModule)
				m_BtnRemove.ShowWindow(SW_SHOW);
			m_BtnEdit.ShowWindow(SW_SHOW);
			m_BtnUpdate.ShowWindow(SW_SHOW);
			m_BtnExport.ShowWindow(SW_SHOW);
			m_BtnImport.ShowWindow(SW_SHOW);
		}
	}
}

bool BackstagePanelJobCenterBase::IsSimpleView() const
{
	return m_Grid.IsSimpleView();
}

void BackstagePanelJobCenterBase::OnKillActive()
{
	ASSERT(nullptr != m_Frame);
	if (nullptr != m_Frame)
	{
		if (m_ConfigurationWasUpdated)
		{
			auto gfb = dynamic_cast<GridFrameBase*>(m_Frame);
			if (nullptr != gfb)
				gfb->GetGrid().CreateHTMLArea();
			else
			{
				auto mf = dynamic_cast<MainFrame*>(m_Frame);
				ASSERT(nullptr != mf);
				if (nullptr != mf)
				{
					mf->HTMLUpdateJobs();
					mf->HTMLUpdateSchedules();
				}
			}
		}
	}
}

void BackstagePanelJobCenterBase::invertRemoveAndImport()
{
	CRect remRect;
	m_BtnRemove.GetWindowRect(remRect);
	ScreenToClient(&remRect);

	CRect impRect;
	m_BtnImport.GetWindowRect(impRect);
	ScreenToClient(&impRect);

	m_BtnRemove.MoveWindow(impRect);
	m_BtnImport.MoveWindow(remRect);
}

bool BackstagePanelJobCenterBase::edit(Script& p_Script)
{
	ASSERT(nullptr != m_JobCenter);
	ASSERT(!p_Script.m_Key.empty());
	if (nullptr != m_JobCenter && !p_Script.m_Key.empty() && !p_Script.IsSystem() && !isDebugMode())
	{
		DlgAutomationWizardEditHTML dlg(p_Script, this);
		if (dlg.DoModal() == IDOK)
			return m_JobCenter->UpdateScript(p_Script);
	}

	return false;
}

bool BackstagePanelJobCenterBase::edit(ScriptPreset& p_ScriptPreset)
{
	ASSERT(nullptr != m_JobCenter);
	ASSERT(!p_ScriptPreset.m_Key.empty());
	if (nullptr != m_JobCenter && !p_ScriptPreset.m_Key.empty() && !isDebugMode())
	{
		auto s = m_JobCenter->GetScript(p_ScriptPreset.m_ScriptID);
		DlgJobPresetEdit dlg(s, p_ScriptPreset, this);
		if (IDOK == dlg.DoModal())
			return m_JobCenter->UpdateScriptPreset(dlg.GetPreset());
	}

	return false;
}

void BackstagePanelJobCenterBase::OnEdit()
{
	vector<Script>			scripts;
	vector<ScriptPreset>	scriptPresets;
	vector<ScriptSchedule>	scriptSchedules;
	m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);
	ASSERT((scripts.size() == 1 && scriptPresets.empty()) || (scriptPresets.size() == 1 && scripts.empty()));
	if (scripts.size() == 1)
	{
		if (edit(scripts.front()))
		{
			m_Grid.LoadScripts(m_JobCenter);
			SetConfigurationWasUpdated(true);
		}
	}
	else if (scriptPresets.size() == 1)
	{
		if (edit(scriptPresets.front()))
		{
			m_Grid.LoadScripts(m_JobCenter);
			SetConfigurationWasUpdated(true);
		}
	}
}

void BackstagePanelJobCenterBase::onUpdate()
{
	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter)
	{
		vector<Script>			scripts;
		vector<ScriptPreset>	scriptPresets;
		vector<ScriptSchedule>	scriptSchedules;
		m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);
		ASSERT(!scripts.empty() || !scriptPresets.empty());
		if (!scripts.empty() || !scriptPresets.empty())
		{
			vector<Script>			updateScripts;
			vector<ScriptPreset>	updateScriptPresets;
			for (auto& s : scripts)
				if (!s.IsSystem() && FileUtil::FileExists(s.m_SourceXMLfilePath))
					updateScripts.push_back(s);
			for (auto& sp : scriptPresets)
				if (!sp.m_SourceXMLfilePath.empty() && FileUtil::FileExists(sp.m_SourceXMLfilePath))
					updateScriptPresets.push_back(sp);

			ASSERT(!updateScripts.empty() || !updateScriptPresets.empty());
			if (!updateScripts.empty() || !updateScriptPresets.empty())
			{
				auto infoMsg = !updateScripts.empty() && updateScriptPresets.empty() ?
					_YFORMAT(L"This will update the %s selected job(s) from their XML source files.", Str::getStringFromNumber<size_t>(updateScripts.size()).c_str()) :
					!updateScriptPresets.empty() && updateScripts.empty() ?
					_YFORMAT(L"This will update the %s selected job preset(s) from their XML source files.", Str::getStringFromNumber<size_t>(updateScriptPresets.size()).c_str()) :
					_YFORMAT(L"This will update the %s selected job(s) and %s job preset(s) from their XML source files.", Str::getStringFromNumber<size_t>(updateScripts.size()).c_str(), Str::getStringFromNumber<size_t>(updateScriptPresets.size()).c_str());

				YCodeJockMessageBox dlg(this,
					DlgMessageBox::eIcon_Question,
					_T("Update Jobs?"),
					_YFORMAT(L"This will update the %s selected job(s) from their XML source files.", Str::getStringFromNumber<size_t>(updateScripts.size()).c_str()),
					_YTEXT(""),
					{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
				if (IDOK == dlg.DoModal())
				{
					vector<wstring> updateErrors;
					for (auto& s : updateScripts)
					{
						auto newScript = ScriptParser().MakeScriptFromXMLFile(m_JobCenter, s.m_SourceXMLfilePath);
						if (newScript.m_LoadError_vol.empty())
						{
							s.m_ScriptContent = newScript.m_ScriptContent;
							m_JobCenter->UpdateScript(s);
						}
						else
							updateErrors.push_back(_YFORMAT(L"%s: %s", newScript.m_Key.c_str(), Str::implode(newScript.m_LoadError_vol, _YTEXT("\n")).c_str()));
					}

					for (auto& sp : updateScriptPresets)
					{
						auto newScriptPreset = ScriptParser().MakeScriptPresetFromXMLFile(m_JobCenter, sp.m_SourceXMLfilePath);
						if (newScriptPreset.m_LoadError_vol.empty())
						{
							sp.m_ScriptContent = newScriptPreset.m_ScriptContent;
							m_JobCenter->UpdateScriptPreset(sp);
						}
						else
							updateErrors.push_back(_YFORMAT(L"%s: %s", sp.m_Key.c_str(), Str::implode(newScriptPreset.m_LoadError_vol, _YTEXT("\n")).c_str()));
					}

					m_Grid.LoadScripts(m_JobCenter);
					SetConfigurationWasUpdated(true);

					if (!updateErrors.empty())
					{
						YCodeJockMessageBox dlg(this,
							DlgMessageBox::eIcon_Error,
							_T("Error"),
							_YFORMAT(L"Unable to update %s job(s)", Str::getStringFromNumber<size_t>(updateErrors.size()).c_str()),
							Str::implode(updateErrors, _YTEXT("\n")).c_str(),
							{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
						dlg.DoModal();
					}
				}
			}
		}
	}
}

void BackstagePanelJobCenterBase::ImportScripts(const vector<wstring>& p_FilePaths)
{
	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter)
	{
		vector<Script>				newScripts;
		map<wstring, Script>		existingScripts;
		map<wstring, ScriptPreset>	existingPresets;
		map<wstring, wstring>		pathsErrors;

		const bool sandbox = CRMpipe().IsSandbox();
		ScriptParser parser;
		bool cancel = true;
		for (const auto& filePath : p_FilePaths)
		{
			if (!filePath.empty())
			{
				cancel = false;

				// if it is this an existing preset, update it
				auto presetKey = JobCenterUtil::GenerateKey(m_JobCenter->GetName(), filePath);
				auto preset = m_JobCenter->GetScriptPreset(presetKey);
				if (preset.HasID())
					existingPresets[presetKey] = preset;
				else
				{
					// process the file as a script:
					auto fileName = FileUtil::FileGetFileName(filePath);
					auto newScript = parser.MakeScriptFromXMLFile(m_JobCenter, filePath);
					if (newScript.m_LoadError_vol.empty())
					{
						auto s = m_JobCenter->GetScript(newScript.m_Key);
						if (s.HasID())
						{
							newScript.m_ID = s.m_ID;
							if (s.IsSystem())
							{
								if (sandbox)
								{
									newScript.m_SourceXMLfilePath.clear();
									existingScripts[newScript.m_Key] = newScript;
								}
								else
									pathsErrors[filePath] = _YFORMAT(L"A system job already exists with the key '%s'.\nPlease modify the XML file name to import it as a new job.", newScript.m_Key.c_str());
							}
							else
								existingScripts[newScript.m_Key] = newScript;
						}
						else
							newScripts.push_back(newScript);
					}
					else
						pathsErrors[filePath] = Str::implode(newScript.m_LoadError_vol, _YTEXT("\n"));
				}
			}
		}

		if (!pathsErrors.empty())
		{
			wstring errorMsg;
			for (const auto& p : pathsErrors)
				errorMsg += _YFORMAT(L"%s: %s\n", p.first.c_str(), p.second.c_str());
			if (existingScripts.empty() && newScripts.empty())
			{
				YCodeJockMessageBox(this, DlgMessageBox::eIcon_Error, _T("Error"), _T("Unable to import: "), errorMsg,
					{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } }).DoModal();
				cancel = true;
			}
			else
				cancel = YCodeJockMessageBox(this, DlgMessageBox::eIcon_Error, _T("Error"), _T("Unable to import: "), errorMsg,
					{ { IDOK, _T("Continue import with valid") },
						{ IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } }).DoModal();
		}

		if (!cancel)
		{
			vector<wstring> successfulImports;
			vector<wstring> failedImports;
			auto importScript = [this, &successfulImports, &failedImports](Script& newScript)
			{
				if (m_JobCenter->GetName() == AutomationWizardCommon().GetName())
					newScript.m_Common = JobCenterUtil::g_CommonFlag;
				if (m_JobCenter->ImportScript(newScript))
					successfulImports.push_back(newScript.m_Key);
				else
					failedImports.push_back(newScript.m_Key);
			};

			auto updatePreset = [this, &successfulImports, &failedImports, &parser](ScriptPreset& preset)
			{
				auto newPreset = parser.MakeScriptPresetFromXMLFile(m_JobCenter, preset.m_SourceXMLfilePath);
				if (m_JobCenter->UpdateScriptPreset(newPreset))
					successfulImports.push_back(newPreset.m_Key);
				else
					failedImports.push_back(newPreset.m_Key);
			};

			if (!existingScripts.empty() || !existingPresets.empty())
			{
				cancel = IDCANCEL == DlgJobCenterImportConfirmReplace(existingScripts, existingPresets, this).DoModal();
				if (!cancel)
				{
					for (auto& p : existingScripts)
						importScript(p.second);
					for (auto& p : existingPresets)
						updatePreset(p.second);
				}
			}

			// if a preset was not found in the db, it is considered a script
			if (!cancel && !newScripts.empty())
			{
				for (auto& newScript : newScripts)
					if (DlgAutomationWizardEditHTML(newScript, this).DoModal() == IDOK)
						importScript(newScript);
					else
						break;
			}

			cancel = successfulImports.empty() && failedImports.empty();
			if (!cancel)
			{
				m_Grid.LoadScripts(m_JobCenter);
				SetConfigurationWasUpdated(true);

				auto imported = successfulImports.empty() ? _T("None") : Str::implode(successfulImports, _YTEXT("\n"));
				TraceIntoFile::trace(_YDUMP("BackstagePanelJobCenterBase"), _YDUMP("ImportScripts - Successfully imported: "), imported);
				if (!pathsErrors.empty() || successfulImports.size() != (newScripts.size() + existingScripts.size()))
					YCodeJockMessageBox(this, DlgMessageBox::eIcon_Information, _T("Job Import"), _T("Successfully imported"),
						imported.c_str(),
						{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } }).DoModal();

				if (!failedImports.empty())
				{
					auto failed = Str::implode(failedImports, _YTEXT("\n"));
					TraceIntoFile::trace(_YDUMP("BackstagePanelJobCenterBase"), _YDUMP("ImportScripts - Failed imports: "), failed);
					YCodeJockMessageBox(this, DlgMessageBox::eIcon_Error, _T("Job Import"), _T("Feiled imports"),
						failed.c_str(),
						{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } }).DoModal();
				}
			}
		}
	}
}

void BackstagePanelJobCenterBase::onImport()
{
	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter && !isDebugMode())
		ImportScripts({ YFileDialog::AskOpenXmlFile(nullptr, this) });
}

void BackstagePanelJobCenterBase::onExport()
{
	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter && !isDebugMode())
	{
		vector<Script>			scripts;
		vector<ScriptPreset>	scriptPresets;
		vector<ScriptSchedule>	scriptSchedules;
		m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);
		ASSERT(!scripts.empty() || !scriptPresets.empty());

		DlgJobCenterExport dlg(this);
		if (dlg.DoModal() == IDOK)
		{
			vector<wstring> exportedFiles;
			const wstring& exportFolder = dlg.GetFolderPath();
			for (const auto& s : scripts)
			{
				auto exportedFile = m_JobCenter->Export(s.m_Key, exportFolder + _YTEXT("\\") + JobCenterUtil::GetFileNameForExport(s.m_Key), dlg.GetFileExistsAction(), true);
				if (!exportedFile.empty())
					exportedFiles.push_back(exportedFile);
			}
			for (const auto& sp : scriptPresets)
			{
				auto exportedFile = m_JobCenter->Export(sp.m_Key, exportFolder + _YTEXT("\\") + JobCenterUtil::GetFileNameForExport(sp.m_Key), dlg.GetFileExistsAction(), false);
				if (!exportedFile.empty())
					exportedFiles.push_back(exportedFile);
			}

			if (dlg.IsOpenAfterDownload())
				FileUtil::OpenFolderAndSelectFiles(exportedFiles);
		}
	}
}

void BackstagePanelJobCenterBase::onRemove()
{
	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter && !isDebugMode())
	{
		vector<Script>			scripts;
		vector<ScriptPreset>	scriptPresets;
		vector<ScriptSchedule>	scriptSchedules;
		m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);

		ASSERT(!scripts.empty() || !scriptPresets.empty());
		if (!scripts.empty() || !scriptPresets.empty())
		{
			vector<Script> removeScripts;
			vector<Script> systemScripts;
			for (auto& s : scripts)
				if (s.IsSystem())
					systemScripts.push_back(s);
				else
					removeScripts.push_back(s);

			if (systemScripts.empty())
			{
				auto question =
					!removeScripts.empty() && !scriptPresets.empty() ?
					_YFORMAT(L"Remove %s job(s) and %s job preset(s)?", Str::getStringFromNumber<size_t>(removeScripts.size()).c_str(), Str::getStringFromNumber<size_t>(scriptPresets.size()).c_str()) :
					!removeScripts.empty() ?
					_YFORMAT(L"Remove %s job(s)?", Str::getStringFromNumber<size_t>(removeScripts.size()).c_str()) :
					_YFORMAT(L"Remove %s job preset(s)?", Str::getStringFromNumber<size_t>(scriptPresets.size()).c_str());

				auto mainFrame = dynamic_cast<CodeJockFrameBase*>(AfxGetApp()->m_pMainWnd);
				ASSERT(nullptr != mainFrame);

				YCodeJockMessageBox dlg(this,
					DlgMessageBox::eIcon_Question,
					_T("Confirm"),
					question,
					mainFrame == m_Frame ? _T("Note: any related scheduled tasks will be deleted from Windows Task Scheduler. This operation cannot be undone.")
					: _YTEXT(""),
					{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
				if (IDOK == dlg.DoModal())
				{
					m_JobCenter->ClearScheduleRemoveErrors();

					for (auto& s : removeScripts)
						m_JobCenter->RemoveScript(s);
					for (auto& sp : scriptPresets)
						m_JobCenter->RemoveScriptPreset(sp);

					m_Grid.FixPositions();
					m_Grid.LoadScripts(m_JobCenter);
					SetConfigurationWasUpdated(true);

					processScheduleRemoveErrors();
				}
			}
			else
			{
				YCodeJockMessageBox dlg(this,
					DlgMessageBox::eIcon_ExclamationWarning,
					_T("Warning"),
					_YFORMAT(L"Your selection contains %s system job(s), which cannot be removed.", Str::getStringFromNumber<size_t>(systemScripts.size()).c_str()),
					_T("Please select only non-system jobs and try again.\nOperation canceled."),
					{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
				dlg.DoModal();
			}
		}
	}
}

void BackstagePanelJobCenterBase::SelectionChanged(const CacheGrid& grid)
{
	updateButtons();
}

bool BackstagePanelJobCenterBase::isDebugMode()
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	const bool debugMode = nullptr != mainFrame && mainFrame->IsDebugMode();
	return debugMode;
}

std::shared_ptr<const Sapio365Session> BackstagePanelJobCenterBase::getSession() const
{
	return MainFrameSapio365Session();
}

void BackstagePanelJobCenterBase::processScheduleRemoveErrors()
{
	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter)
	{
		const auto& scheduleRemoveErrors = m_JobCenter->GetScheduleRemoveErrors();
		if (!scheduleRemoveErrors.empty())
		{
			YCodeJockMessageBox dlg(this,
				DlgMessageBox::eIcon_Error,
				_T("Error"),
				_T("Error removing task(s) from Windows Task Scheduler"),
				Str::implode(scheduleRemoveErrors, _YTEXT("\n"), false),
				{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
			dlg.DoModal();
		}
	}
}

void BackstagePanelJobCenterBase::OnGridDoubleClick()
{
	OnEdit();// redefine locally
}