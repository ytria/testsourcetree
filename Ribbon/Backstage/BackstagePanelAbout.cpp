#include "BackstagePanelAbout.h"

#include "ActivityLogger.h"
#include "AnnotationManager.h"
#include "AutomatedApp.h"
#include "CodeJockThemed.h"
#include "CosmosManager.h"
#include "RoleDelegationCosmos.h"
#include "DlgEnterCosmosCNX.h"
#include "DlgLicenseHistory.h"
#include "DlgLicenseMessageBoxHTML.h"
#include "InitAfterSessionOrLicenseWasSet.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "NalpeironLicenseManager.h"
#include "O365LicenseValidator.h"
#include "Product.h"
#include "RoleDelegationManager.h"
#include "TimeUtil.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTranslate.h"

BEGIN_MESSAGE_MAP(CXTPRibbonBackstageLabelRightAlign, CXTPRibbonBackstageLabel)
	ON_WM_PAINT()
END_MESSAGE_MAP()

void CXTPRibbonBackstageLabelRightAlign::OnPaint()
{
	// Copy pasted from CXTPRibbonBackstageLabel::OnPaint just to use DT_RIGHT instead of DT_LEFT

	CPaintDC dcPaint(this); // device context for painting

	CXTPClientRect rc(this);

	CXTPBufferDC dc(dcPaint);
	dc.FillSolidRect(rc, m_clrBack);

	CXTPFontDC font(&dc, GetFont());

	CString strText;
	GetWindowText(strText);

	dc.SetTextColor(m_clrText);
	dc.DrawText(strText, rc, DT_RIGHT | DT_VCENTER | DT_SINGLELINE | DT_NOPREFIX);
}


// =================================================================================================

BEGIN_MESSAGE_MAP(BackstagePanelAbout, BackstagePanelBase)
	ON_BN_CLICKED(IDC_BACKSTAGE_ABOUT_BTN_SITE,					OnWebsite)
	ON_BN_CLICKED(IDC_BACKSTAGE_ABOUT_BTN_LICENSEKEY,			OnEnterLicenseKey)
	ON_BN_CLICKED(IDC_BACKSTAGE_ABOUT_BTN_CONFIGURELICENSE,		OnConfigureLicense)
	ON_BN_CLICKED(IDC_BACKSTAGE_ABOUT_BTN_UPGRADE,				OnUpgradeLicense)
	ON_BN_CLICKED(IDC_BACKSTAGE_ABOUT_BTN_REFRESHLICENSE,		OnRefreshLicense)
	ON_BN_CLICKED(IDC_BACKSTAGE_ABOUT_BTN_CHECKFORUPDATE,		OnCheckForUpdate)
	ON_BN_CLICKED(IDC_BACKSTAGE_ABOUT_BTN_DEAUTHORIZECOMPUTER,	OnDeauthorizeComputer)
	ON_BN_CLICKED(IDC_BACKSTAGE_ABOUT_BTN_COSMOS,				OnManageCosmosAccount)
	ON_BN_CLICKED(IDC_BACKSTAGE_ABOUT_BTN_LICENSEHISTORY,		OnManageLicenseHistory)
END_MESSAGE_MAP()

BackstagePanelAbout::BackstagePanelAbout()
	: BackstagePanelBase(IDD)
	, m_IsCosmosSetForAnotherTenant(false)
	, m_LicenseChanged(false)
	, m_CrashSequenceIndex(0)
{

}

BackstagePanelAbout::~BackstagePanelAbout()
{
}

void BackstagePanelAbout::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelBase::SetTheme(nTheme);

	m_PatentLabel.SetTheme(nTheme);
	m_CopyrightsLabel.SetTheme(nTheme);
	m_WebsiteButton.SetTheme(nTheme);
	m_LicenseKeyButton.SetTheme(nTheme);
	m_LicenseRefreshButton.SetTheme(nTheme);
	m_CheckForUpdateButton.SetTheme(nTheme);
	m_LicenseCfgButton.SetTheme(nTheme);
	m_LicenseUpgradeButton.SetTheme(nTheme);
	m_LicenseDeauthorizeComputerButton.SetTheme(nTheme);
	m_LicenseCosmosCNX.SetTheme(nTheme);
	m_LicenseHistoryButton.SetTheme(nTheme);
}

void BackstagePanelAbout::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelBase::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_PATENT,					m_PatentLabel);
	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_COPYRIGHTS,				m_CopyrightsLabel);
	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_BTN_SITE,					m_WebsiteButton);
	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_LICENSE,					m_LicenseLabel);
	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_BTN_LICENSEKEY,			m_LicenseKeyButton);
	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_BTN_CONFIGURELICENSE,		m_LicenseCfgButton);
	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_BTN_UPGRADE,				m_LicenseUpgradeButton);
	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_BTN_REFRESHLICENSE,		m_LicenseRefreshButton);
	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_BTN_CHECKFORUPDATE,		m_CheckForUpdateButton);
	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_BTN_DEAUTHORIZECOMPUTER,	m_LicenseDeauthorizeComputerButton);
	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_BTN_COSMOS,				m_LicenseCosmosCNX);
	DDX_Control(pDX, IDC_BACKSTAGE_ABOUT_BTN_LICENSEHISTORY,		m_LicenseHistoryButton);
}

BOOL BackstagePanelAbout::OnInitDialog()
{
	BackstagePanelBase::OnInitDialog();

	setCaption(YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_1, _YLOC("About sapio365")).c_str());

	SetDlgCtrlID(IDD);
	ModifyStyleEx(0, WS_EX_CONTROLPARENT);

	m_PatentLabel.SetWindowText(CString(Product::getInstance().getPatents().c_str()));
	m_CopyrightsLabel.SetWindowText(CString(Product::getInstance().getCopyrights().c_str()));

	m_LicenseLabel.ShowWindow(SW_SHOW);

	{
		// Website button

		UINT ids[]{ IDC_BACKSTAGE_ABOUT_BTN_SITE };
		m_imagelist.SetIcons(IDB_BACKSTAGE_ABOUT_LOGO
			, ids
			, 1
			, CSize(0, 0)
			, xtpImageNormal);

		SetupButton(m_WebsiteButton
			, YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_2, _YLOC("Visit product page")).c_str()
			, _YTEXT("www.ytria.com/sapio365")
			, m_imagelist);

		m_WebsiteButton.EnableWindow(TRUE);
	}

	{
		// New license code button

		UINT ids[]{ IDC_BACKSTAGE_ABOUT_BTN_LICENSEKEY };
		m_imagelist.SetIcons(IDB_BACKSTAGE_BUTTON_LICENSEKEY
			, ids
			, 1
			, CSize(0, 0)
			, xtpImageNormal);

		SetupButton(m_LicenseKeyButton
			, YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_3, _YLOC("Enter New License Code")).c_str()
			, YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_7, _YLOC("Activate a new license code in sapio365.")).c_str()
			, m_imagelist);

		m_LicenseKeyButton.EnableWindow(TRUE);
	}

	{
		// configure license button

		UINT ids[]{ IDC_BACKSTAGE_ABOUT_BTN_CONFIGURELICENSE };
		m_imagelist.SetIcons(IDB_BACKSTAGE_BUTTON_LICENSECFG
			, ids
			, 1
			, CSize(0, 0)
			, xtpImageNormal);

		SetupButton(m_LicenseCfgButton
			, YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_9, _YLOC("License Configuration Settings")).c_str()
			, YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_10, _YLOC("Manage the tenants linked to your sapio365 license.")).c_str()
			, m_imagelist);

		m_LicenseCfgButton.EnableWindow(FALSE);
	}

	{
		// upgrade license button

		UINT ids[]{ IDC_BACKSTAGE_ABOUT_BTN_UPGRADE };
		m_imagelist.SetIcons(IDB_BACKSTAGE_BUTTON_LICENSEPLUS
			, ids
			, 1
			, CSize(0, 0)
			, xtpImageNormal);

		SetupButton(m_LicenseUpgradeButton
			, YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_11, _YLOC("License Modification")).c_str()
			, YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_12, _YLOC("Modify the tenant user capacity of your license.")).c_str()
			, m_imagelist);

		m_LicenseUpgradeButton.EnableWindow(FALSE);
	}

	{
		// Refresh button

		UINT ids[]{ IDC_BACKSTAGE_ABOUT_BTN_REFRESHLICENSE };
		m_imagelist.SetIcons(IDB_BACKSTAGE_BUTTON_REFRESHLICENSEKEY
			, ids
			, 1
			, CSize(0, 0)
			, xtpImageNormal);

		SetupButton(m_LicenseRefreshButton
			, YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_4, _YLOC("Refresh License Information")).c_str()
			, YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_8, _YLOC("Get the most recent status of the current license code from the Ytria registration server.")).c_str()
			, m_imagelist);
		m_LicenseRefreshButton.EnableWindow(TRUE);
	}

	{
		// Check for update

		UINT ids[]{ IDC_BACKSTAGE_ABOUT_BTN_CHECKFORUPDATE };
		m_imagelist.SetIcons(IDB_BACKSTAGE_BUTTON_CHECKFORUPDATE
			, ids
			, 1
			, CSize(0, 0)
			, xtpImageNormal);

		SetupButton(m_CheckForUpdateButton
			, YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_5, _YLOC("Check for Updates")).c_str()
			, _YTEXT("")
			, m_imagelist);
		m_CheckForUpdateButton.EnableWindow(TRUE);
	}

	{
		// Deauthorize computer

		UINT ids[]{ IDC_BACKSTAGE_ABOUT_BTN_DEAUTHORIZECOMPUTER };
		m_imagelist.SetIcons(IDB_BACKSTAGE_BUTTON_DEAUTHORIZECOMPUTER
			, ids
			, 1
			, CSize(0, 0)
			, xtpImageNormal);

		SetupButton(m_LicenseDeauthorizeComputerButton
			, YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_6, _YLOC("Deauthorize Computer")).c_str()
			, _YTEXT("")
			, m_imagelist);

		m_LicenseDeauthorizeComputerButton.EnableWindow(TRUE);
	}

	{
		// Cosmos login

		UINT ids[]{ IDC_BACKSTAGE_ABOUT_BTN_COSMOS };
		m_imagelist.SetIcons(IDC_BACKSTAGE_ABOUT_BUTTON_COSMOS, ids, 1, CSize(0, 0), xtpImageNormal);
		SetupButton(m_LicenseCosmosCNX, 
					YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_13, _YLOC("Set Cosmos DB Connection Info")).c_str(), 
					YtriaTranslate::Do(BackstagePanelAbout_OnInitDialog_14, _YLOC("Enable cloud-based sapio365 Role-Based Access, logging and comments for your tenant.")).c_str(),
					m_imagelist);

		m_LicenseCosmosCNX.EnableWindow(TRUE);
	}

	{
		UINT ids[]{ IDC_BACKSTAGE_ABOUT_BTN_LICENSEHISTORY };
		m_imagelist.SetIcons(IDC_BACKSTAGE_ABOUT_BUTTON_MANAGEHISTORY, ids, 1, CSize(0, 0), xtpImageNormal);
		SetupButton(m_LicenseHistoryButton, 
					_YTEXT(""), // set in updateLicenseInfo
					_T("See license history and assign licenses."),
					m_imagelist);

		m_LicenseHistoryButton.EnableWindow(FALSE);
		m_LicenseHistoryButton.ShowWindow(SW_HIDE);
	}

	SetResize(IDC_BACKSTAGE_ABOUT_LICENSE,					{ 0, 0 },	{ .75, 1 });
	SetResize(IDC_BACKSTAGE_ABOUT_BTN_LICENSEKEY,			{ .75, 0 }, { 1, 0 });
	SetResize(IDC_BACKSTAGE_ABOUT_BTN_CONFIGURELICENSE,		{ .75, 0 }, { 1, 0 });
	SetResize(IDC_BACKSTAGE_ABOUT_BTN_UPGRADE,				{ .75, 0 }, { 1, 0 });
	SetResize(IDC_BACKSTAGE_ABOUT_BTN_REFRESHLICENSE,		{ .75, 0 }, { 1, 0 });
	SetResize(IDC_BACKSTAGE_ABOUT_BTN_CHECKFORUPDATE,		{ .75, 0 }, { 1, 0 });
	SetResize(IDC_BACKSTAGE_ABOUT_BTN_DEAUTHORIZECOMPUTER,	{ .75, 0 }, { 1, 0 });
	SetResize(IDC_BACKSTAGE_ABOUT_BTN_COSMOS,				{ .75, 0 }, { 1, 0 });
	SetResize(IDC_BACKSTAGE_ABOUT_BTN_LICENSEHISTORY,		{ .75, 0 }, { 1, 0 });

	SetResize(IDC_BACKSTAGE_ABOUT_BTN_SITE, { 0, 1 }, { 0, 1 });
	SetResize(IDC_BACKSTAGE_ABOUT_COPYRIGHTS, { .75, 1 }, { 1, 1 });
	SetResize(IDC_BACKSTAGE_ABOUT_PATENT, { .75, 1 }, { 1, 1 });

	return TRUE;
}

void BackstagePanelAbout::OnWebsite()
{
	Product::getInstance().gotoAbsoluteUrl(RegistryYtria().getURLHTTPS(_YTEXT("sapio365")));
}

void BackstagePanelAbout::OnEnterLicenseKey()
{
	AutomatedApp* o365App = dynamic_cast<AutomatedApp*>(AfxGetApp());
	ASSERT(nullptr != o365App);
	if (nullptr != o365App && o365App->LoadNewLicense(this))
	{
		// new license may contain new or no Cosmos credentials: update
		std::shared_ptr<Sapio365Session> mainFrameSession = MainFrameSapio365Session();
		ASSERT(mainFrameSession);
		if (mainFrameSession)
			InitAfterSessionOrLicenseWasSet::Do(mainFrameSession, this);

		updateLicenseInfo(true);
	}
}

void BackstagePanelAbout::OnConfigureLicense()
{
	AutomatedApp* o365App = dynamic_cast<AutomatedApp*>(AfxGetApp());
	ASSERT(nullptr != o365App);
	if (nullptr != o365App)
	{
		auto mainFrame	= dynamic_cast<MainFrame*>(o365App->m_pMainWnd);
		auto licenseMgr = o365App->GetLicenseManager();
		ASSERT(nullptr != licenseMgr);
		ASSERT(nullptr != mainFrame);
		if (nullptr != licenseMgr && nullptr != mainFrame)
		{
			mainFrame->ConfigureLicense(licenseMgr->GetLicense(false));
			updateLicenseInfo(true);
		}
	}
}

void BackstagePanelAbout::OnUpgradeLicense()
{
	AutomatedApp* o365App = dynamic_cast<AutomatedApp*>(AfxGetApp());
	ASSERT(nullptr != o365App);
	if (nullptr != o365App)
	{
		auto mainFrame	= dynamic_cast<MainFrame*>(o365App->m_pMainWnd);
		auto licenseMgr = o365App->GetLicenseManager();
		ASSERT(nullptr != licenseMgr);
		ASSERT(nullptr != mainFrame);
		if (nullptr != licenseMgr && nullptr != mainFrame)
			mainFrame->UpgradeLicense(licenseMgr->GetLicense(false));
		updateLicenseInfo(true);
	}
}

void BackstagePanelAbout::OnRefreshLicense()
{
	updateLicenseInfo(true);

	// new license info may contain new Cosmos credentials or AJL info: update
	std::shared_ptr<Sapio365Session> mainFrameSession = MainFrameSapio365Session();
	ASSERT(mainFrameSession);
	if (mainFrameSession)
		InitAfterSessionOrLicenseWasSet::Do(mainFrameSession, this);
}

void BackstagePanelAbout::OnCheckForUpdate()
{
	AutomatedApp* o365App = dynamic_cast<AutomatedApp*>(AfxGetApp());
	ASSERT(nullptr != o365App);
	if (nullptr != o365App)
	{
		LicenseManager* licenseMgr = o365App->GetLicenseManager();
		ASSERT(nullptr != licenseMgr);
		if (nullptr != licenseMgr)
		{
			if (licenseMgr->CheckUpdate(true))
			{
				auto mainFrame = dynamic_cast<MainFrame*>(o365App->m_pMainWnd);
				mainFrame->showBackstageTab(ID_PRODUCT_UPDATE);
			}
			else
			{
				YCodeJockMessageBox msgBox(this
					, DlgMessageBox::eIcon_Information
					, YtriaTranslate::Do(BackstagePanelAbout_OnCheckForUpdate_1, _YLOC("No update available.")).c_str()
					, YtriaTranslate::Do(BackstagePanelAbout_OnCheckForUpdate_2, _YLOC("You are currently using the latest version.")).c_str()
					, _YTEXT("")
					, { {IDOK, YtriaTranslate::Do(BackstagePanelAbout_OnCheckForUpdate_3, _YLOC("OK")).c_str()} });

				msgBox.DoModal();
			}
		}
	}
}

void BackstagePanelAbout::OnDeauthorizeComputer()
{
	AutomatedApp* o365App = dynamic_cast<AutomatedApp*>(AfxGetApp());
	ASSERT(nullptr != o365App);
	if (nullptr != o365App)
	{
		DlgLicenseMessageBoxHTML dlg(this,
									 DlgMessageBox::eIcon_Question,
									 YtriaTranslate::Do(BackstagePanelAbout_OnDeauthorizeComputer_1, _YLOC("Deactivate license on this computer?")).c_str(),
									 YtriaTranslate::Do(BackstagePanelAbout_OnDeauthorizeComputer_2, _YLOC("The license will be returned and available for future activation.")).c_str(),
									 _YTEXT(""),
									 { { IDCANCEL, YtriaTranslate::Do(BackstagePanelAbout_OnDeauthorizeComputer_3, _YLOC("Cancel")).c_str() }, { IDOK, YtriaTranslate::Do(BackstagePanelAbout_OnDeauthorizeComputer_4, _YLOC("OK")).c_str() } },
									 { 0, _YTEXT("") },
									 _YTEXT("#") + o365App->GetApplicationColor());
		if (IDOK == dlg.DoModal())
		{
			LicenseManager::ReturnLicense();
			updateLicenseInfo(true);
		}
	}
}

void BackstagePanelAbout::OnManageCosmosAccount()
{
	auto o365App = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(nullptr != o365App);
	if (nullptr != o365App)
	{
		wstring licenseStatus;
		if (!o365App->IsLicenseFullOrTrial(licenseStatus))
		{
			DlgLicenseMessageBoxHTML dlg(this,
				DlgMessageBox::eIcon_Information,
				_T("This feature is only available with a full license."),
				licenseStatus,
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(BackstagePanelAbout_OnDeauthorizeComputer_4, _YLOC("OK")).c_str() } },
				{ 0, _YTEXT("") },
				_YTEXT("#") + o365App->GetApplicationColor());
			dlg.DoModal();
		}
		else if (Office365AdminApp::CheckAndWarn<LicenseTag::Collaboration>(this))
		{
			std::shared_ptr<Sapio365Session> session = MainFrameSapio365Session();
			ASSERT(session);
			if (session)
			{
				DlgEnterCosmosCNX dlg(session, m_IsCosmosSetForAnotherTenant, this);
				dlg.SetOnOkValidation([this, session](auto& p_Dlg) mutable
					{
						bool success = true;
						auto cnx = p_Dlg.GetConnector();
						auto cls = p_Dlg.IsClear();
						if ((p_Dlg.NewConnectorEntered() && !m_IsCosmosSetForAnotherTenant) || cls)// empty: remove
						{
							CWaitCursor _;
							CosmosManager cosmosManager;
							success = cosmosManager.SendCnxToCRM(cnx, session, cls);
							if (success)
							{
								cnx = cosmosManager.GetConnectorFromLicense(session);
								session->SetCosmosDbMasterKey(cnx.m_MasterKey);
								session->SetCosmosDbSqlUri(cnx.m_URL);

								if (!cls && cosmosManager.IsValid(cnx))
								{
									InitAfterSessionOrLicenseWasSet::Do(session, &p_Dlg);

									vector<wstring> cfgErrors;
									if (!RoleDelegationManager::GetInstance().IsReadyToRun())
										cfgErrors.push_back(RoleDelegationManager::GetInstance().GetLastError());
									if (!ActivityLogger::GetInstance().IsReadyToRun())
										cfgErrors.push_back(ActivityLogger::GetInstance().GetLastError());
									if (!AnnotationManager::GetInstance().IsReadyToRun())
										cfgErrors.push_back(AnnotationManager::GetInstance().GetLastError());
									if (!cfgErrors.empty())
									{
										YCodeJockMessageBox msgBox(&p_Dlg, DlgMessageBox::eIcon_ExclamationWarning, Str::implode(cfgErrors, _YTEXT("\n")), _YTEXT(""), _YTEXT(""),
											{ {IDOK, YtriaTranslate::Do(BackstagePanelAbout_OnCheckForUpdate_3, _YLOC("OK")).c_str()} });
										msgBox.DoModal();
										success = false;
									}
								}
							}
						}
						return success;
					});

				if (IDOK == dlg.DoModal())
					updateLicenseInfo(false);
			}
		}
	}
}

BOOL BackstagePanelAbout::OnSetActive()
{
	m_LicenseChanged = false;
	updateLicenseInfo(false);
	return BackstagePanelBase::OnSetActive();
}

BOOL BackstagePanelAbout::OnKillActive()
{
	if (m_LicenseChanged)
	{
		AutomatedApp* o365App = dynamic_cast<AutomatedApp*>(AfxGetApp());
		ASSERT(nullptr != o365App);
		if (nullptr != o365App)
		{
			auto mainFrame = dynamic_cast<MainFrame*>(o365App->m_pMainWnd);
			ASSERT(nullptr != mainFrame);
			if (nullptr != mainFrame)
			{
				mainFrame->HTMLUpdateJobs();
				mainFrame->HTMLUpdateAJL();
				mainFrame->HTMLUpdateAJLcanRemoveJobs();
			}
		}
	}

	return BackstagePanelBase::OnKillActive();
}

void BackstagePanelAbout::OnManageLicenseHistory()
{
	std::shared_ptr<Sapio365Session> session = MainFrameSapio365Session();
	DlgLicenseHistory dlg(session ? session.get()->GetConnectedUserPrincipalName() : wstring(), m_CurrentLicenseCode, this);
	dlg.DoModal();
	updateLicenseInfo(false);
}

void BackstagePanelAbout::updateLicenseInfo(const bool p_Refresh)
{
	if (p_Refresh)
		m_LicenseChanged = true;

	m_LicenseUpgradeButton.EnableWindow(FALSE);

	auto& app = Office365AdminApp::Get();
	VendorLicense license = app.GetRefreshedLicense(p_Refresh);

	std::shared_ptr<Sapio365Session> session = MainFrameSapio365Session();

	m_LicenseCosmosCNX.EnableWindow(session != nullptr);

	LicenseManager* licenseMgr = app.GetLicenseManager();
	ASSERT(nullptr != licenseMgr);
	if (nullptr != licenseMgr)
	{
		wstring formatter;

		m_CurrentLicenseCode = license.m_LicenseCode;
		const auto licenseHistorySize = licenseMgr->GetComputerLicenseHistorySize();
		m_LicenseHistoryButton.ShowWindow(licenseHistorySize > 1 ? SW_SHOW : SW_HIDE);
		m_LicenseHistoryButton.EnableWindow(licenseHistorySize > 1 ? TRUE : FALSE);
		if (licenseHistorySize > 1)
			SetupButton(m_LicenseHistoryButton,
						_YFORMAT(L"License History [%s]", Str::getStringFromNumber<uint32_t>(licenseHistorySize).c_str()),
						_T("Show license history and manage login assignments."),
						m_imagelist);

		CosmosManager cosmosKeyManager;// Cosmos login storage (URL, Master Key)

		const CRMpipe::RegistrationData&	userData	= licenseMgr->GetUserData();
		const bool							cosmosCNX	= session && session->IsReadyForCosmos();
		const bool							cosmosNLP	= cosmosKeyManager.IsValid(license.GetAgilityValue(LicenseUtil::g_AgilityC00)) && cosmosKeyManager.IsValid(license.GetAgilityValue(LicenseUtil::g_AgilityC99));
		m_IsCosmosSetForAnotherTenant					= session && !cosmosCNX && cosmosNLP;

		formatter += _YTEXT("<StackPanel Orientation='Vertical'>");
		formatter += _YTEXT("    <StackPanel VerticalAlignment='Top' HorizontalAlignment='Left'>");
		static const wstring installBlockXaml =
			L"    <StackPanel Margin='0, 0, 10, 10' VerticalAlignment='Top' Orientation='Horizontal' Background='#fafafa' Width='666'>"
			L"        <TextBlock Background='#"+ app.GetApplicationColor() +"'>"
			L"            <Image Source='res://#%s' VerticalAlignment='Center' Margin='5, 12, 5, 12'/>"
			L"        </TextBlock>"
			L"       <TextBlock Margin='10, 7, 10, 0' FontFamily='segoe UI' FontSize='12'>"
			L"            <Span Foreground='#262626' FontWeight='bold'>%s</Span>"
			L"            <LineBreak/>"
			L"            <Span Foreground='#262626'>%s</Span>"
			L"        </TextBlock>"
			L"    </StackPanel>"
			;

		static const wstring installBlockXaml2lines =
			L"    <StackPanel Margin='0, 0, 10, 10' VerticalAlignment='Top' Orientation='Horizontal' Background='#fafafa' Width='666'>"
			L"        <TextBlock Background='#" + app.GetApplicationColor() + "'>"
			L"            <Image Source='res://#%s' VerticalAlignment='Center' Margin='5, 12, 5, 12'/>"
			L"        </TextBlock>"
			L"       <TextBlock Margin='10, 7, 10, 0' FontFamily='segoe UI' FontSize='12'>"
			L"            <Span Foreground='#262626' FontWeight='bold'>%s</Span>"
			L"            <LineBreak/>"
			L"            <Span Foreground='#262626'>%s</Span>"
			L"            <LineBreak/>"
			L"            <Span Foreground='#262626'>%s</Span>"
			L"        </TextBlock>"
			L"    </StackPanel>"
			;

		uint32_t freeUserCount = 666;
		auto licenseValidator = std::dynamic_pointer_cast<O365LicenseValidator>(licenseMgr->GetLicenseValidator());
		ASSERT(licenseValidator);
		if (licenseValidator)
			freeUserCount = licenseValidator->GetUserCountForFreeAccess();

		/*formatter += _YTEXT("<TextBlock VerticalAlignment='Center' Margin='10, 0, 10, 10' Foreground='#434343' FontSize='21' FontFamily='segoe UI'>");
		formatter += _TLOC("About this install");
		formatter += _YTEXT("</TextBlock>");*/
		formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_VERSION).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_1, _YLOC("Version")).c_str(), Product::getInstance().getVersion().c_str());

		const wstring owner = userData.m_Firstname + _YTEXT(" ") + userData.m_LastName + _YTEXT(" (") + userData.m_Email + _YTEXT("), ") + userData.m_Company;
		formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_OWNER).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_2, _YLOC("Owner")).c_str(), owner.c_str());

		if (license.IsTrial())
			formatter += _YTEXTFORMAT(installBlockXaml2lines.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_EXPIRATION).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_3, _YLOC("Trial")).c_str(),
				YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_19, _YLOC("Expires on %1"), license.GetExpirationTrialStr().c_str()).c_str(),
				_YFORMAT(L"Remaining: %s", TimeUtil::GetInstance().GetDayHours(license.m_RemainingTrial).c_str()).c_str());
		else if (license.IsFullOrTrial())
		{
			formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_CODE).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_17, _YLOC("License code")).c_str(), license.m_LicenseCode.c_str());

			if (license.IsPerpetual())
				formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_EXPIRATION).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_7, _YLOC("Permanent")).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_18, _YLOC("Does not expire")).c_str());
			else if (license.IsSubscription())
				formatter += _YTEXTFORMAT(installBlockXaml2lines.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_EXPIRATION).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_8, _YLOC("Subscription period")).c_str(),
					YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_20, _YLOC("Expires on %1"), license.GetExpirationSubscriptionStr().c_str()).c_str(),
					_YFORMAT(L"Remaining: %s", TimeUtil::GetInstance().GetDayHours(license.m_RemainingSubscription).c_str()).c_str());
			else
				formatter += _YTEXTFORMAT(installBlockXaml2lines.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_EXPIRATION).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_9, _YLOC("Expiry date")).c_str(),
					YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_21, _YLOC("Expires on %1"), license.GetExpirationMaintenanceStr().c_str()).c_str(),
					_YFORMAT(L"Remaining: %s", TimeUtil::GetInstance().GetDayHours(license.m_RemainingMaintenance).c_str()).c_str());
		}
		else
			formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_EXPIRATION).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_12, _YLOC("No valid license found")).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_22, _YLOC("%1 - sapio365 may still be used for free on tenants with fewer than %2 users."), license.m_StatusStr.c_str(), Str::getStringFromNumber(freeUserCount).c_str()).c_str());

		// if AJL, add info
		if (license.IsFeatureSet(LicenseUtil::g_codeSapio365ModuleLicenseAJL))
		{
			const auto AJLcapacity = license.GetFeature(LicenseUtil::g_codeSapio365AJLJobCount).GetPoolMax();
			const auto AJLconsumed = Str::explodeIntoSet(license.GetAgilityValue(LicenseUtil::g_AgilityAJL), LicenseUtil::g_AgilityAJLSeparator).size();
			formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_AJL).c_str(), _T("AJL"), _YFORMAT(L"Capacity: %s jobs - Remaining: %s", Str::getStringFromNumber(AJLcapacity).c_str(), Str::getStringFromNumber(AJLcapacity - AJLconsumed).c_str()).c_str());
		}

		const bool		noUserCount		= license.IsFeatureSet(LicenseUtil::g_codeSapio365FeatureNoUserCount);
		VendorFeature	tokenFeature	= license.GetFeatureToken();
		if (noUserCount)
		{
			auto existingTenantUserCountFeatures = license.GetFeatures(Counter, ExtraLicenseConfig::Type::TENANT);
			size_t nTenantsSet = 0;
			for (const auto& f : existingTenantUserCountFeatures)
				if (!f.GetPoolContextValue().empty())
					nTenantsSet++;

			formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_MAINTENANCE).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_33, _YLOC("Unlimited users")).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_35, _YLOC("%1 tenant(s) configured"), Str::getStringFromNumber(nTenantsSet).c_str()).c_str());
		}
		else if (tokenFeature.IsAuthorized())
		{
			uint32_t availableTokens = licenseMgr->CRMGetLicenseAvailableTokens();
			formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_MAINTENANCE).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_10, _YLOC("Tokens")).c_str(),
										YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_23, _YLOC("Activated: %1 token(s) - Remaining: %2 token(s)"),
															Str::getStringFromNumber(tokenFeature.GetPoolMax()).c_str(), 
															availableTokens != LicenseUtil::g_NoValueUINT32 ? 
															Str::getStringFromNumber(availableTokens).c_str() :
															YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_25, _YLOC("[unknown number]")).c_str()).c_str());
		}
		else
		{
			const auto fguc = license.GetFeatureGlobalUserCount();
			if (fguc.IsAuthorized())
			{
				size_t nMax = fguc.GetPoolMax();
				auto existingTenantUserCountFeatures = license.GetFeatures(Counter, ExtraLicenseConfig::Type::TENANT);
				size_t nTenantsSet	= 0;
				size_t nRemaining	= 0;
				for (const auto& f : existingTenantUserCountFeatures)
					if (!f.GetPoolContextValue().empty() && f.GetPoolMax() != LicenseUtil::g_NoValueUINT32)
					{
						nTenantsSet++;
						nRemaining += f.GetPoolMax();
					}
				nRemaining = nMax - nRemaining;
				formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_MAINTENANCE).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_11, _YLOC("Authorized user count")).c_str(), 
											YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_24, _YLOC("%1 users - %2 remaining to allocate - %3 tenant(s) configured"), Str::getStringFromNumber(nMax).c_str(), Str::getStringFromNumber(nRemaining).c_str(), Str::getStringFromNumber(nTenantsSet).c_str()).c_str());
			}
		}

		// find "ytria" in "https://ytria.documents.azure.com:443/"
		const wstring cosmosAccount = session ? session->GetCosmosAccount() : _T("~ no Office365 logged in ~");

		if (!session)
			formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_COSMOS).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_26, _YLOC("Cosmos DB Connection")).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_27, _YLOC("Load a session to see status")).c_str());
		else if (cosmosCNX)
			formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_COSMOS).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_26, _YLOC("Cosmos DB Connection")).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_36, _YLOC("Set with Azure Cosmos DB account: %1"), cosmosAccount.c_str()).c_str());
		else if (cosmosNLP)
			formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_COSMOS).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_26, _YLOC("Cosmos DB Connection")).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_30, _YLOC("Set for another Office365 tenant")).c_str());
		else
			formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_COSMOS).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_26, _YLOC("Cosmos DB Connection")).c_str(), YtriaTranslate::Do(DlgSettingsSignEZLite_GetConfigurationSummary_6, _YLOC("- Not set -")).c_str());

// 			auto maintenanceExpiration = license.GetExpirationMaintenanceStr();
// 			if (maintenanceExpiration.empty())
// 				maintenanceExpiration = _TLOC("N/A");
// 			formatter += _YTEXTFORMAT(installBlockXaml.c_str(), Str::getStringFromNumber(IDB_ABOUTLICENSEINFO_MAINTENANCE).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_4, _YLOC("Maintenance")).c_str(), maintenanceExpiration.c_str());

		formatter += _YTEXT("    </StackPanel>");

		auto moduleFeatures = license.GetFeatures(FeatureType::Module, ExtraLicenseConfig::Type::NONE);

		if (!moduleFeatures.empty())
		{
			formatter += _YTEXT("    <StackPanel Margin='0, 30, 0, 15' Background='#fafafa' Width='666' HorizontalAlignment='Left'>");

			formatter += _YTEXT("        <StackPanel Margin='10, 15, 0, 15' Orientation='Vertical'>");
			formatter += _YTEXT("            <TextBlock VerticalAlignment='Center' Margin='0, 0, 10, 10' Foreground='#262626' FontWeight='normal' FontSize='16' FontFamily='segoe UI'>");
			formatter += YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_5, _YLOC("Included in this license of sapio365:")).c_str();
			formatter += _YTEXT("            </TextBlock>");
			formatter += _YTEXT("        </StackPanel>");

			formatter += _YTEXT("        <StackPanel>")
							_YTEXT("			  <WrapPanel Margin='0' Orientation='Horizontal' VerticalAlignment='Top'>")
						;

			static const wstring moduleFeatureXaml =
						_YTEXT("                <StackPanel Margin='0' VerticalAlignment='Top' Orientation='Horizontal'>")
						_YTEXT("                    <TextBlock Width='10' Margin='10, 5, 0, 0' Height='10' Background='#88888a' VerticalAlignment='Top'></TextBlock>")
						_YTEXT("                	<TextBlock VerticalAlignment='Center' Margin='8, 0, 10, 0' Foreground='#262626' FontWeight='normal' FontSize='12' FontFamily='segoe UI'><Bold>%s</Bold>") //"Feature name"
						_YTEXT("                	<LineBreak/>%s</TextBlock>") // "Authorized"
						_YTEXT("                </StackPanel>")
				;

			static const wstring moduleFeatureSpacerXaml =
						_YTEXT("                <StackPanel Margin='35, 0, 0, 0' VerticalAlignment='Top' Orientation='Horizontal'>")
						_YTEXT("                </StackPanel>")
				;

			for (const auto& f : moduleFeatures)
				if (f.m_Code != LicenseUtil::g_codeSapio365ModuleLicenseAJL)
				{
					formatter += _YTEXTFORMAT(moduleFeatureXaml.c_str(), f.m_Name.c_str(), f.m_StatusStr.c_str());
					formatter += moduleFeatureSpacerXaml;
				}

			formatter += _YTEXT("            </WrapPanel>");
			formatter += _YTEXT("        </StackPanel>");
			formatter += _YTEXT("    </StackPanel>");
		}

		m_LicenseCfgButton.EnableWindow(FALSE);
		if (noUserCount)
		{
			m_LicenseCfgButton.EnableWindow(TRUE);
			setupButtonText(m_LicenseCfgButton, YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_15, _YLOC("License Configuration Settings")).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_34, _YLOC("Enter your tenant in sapio365.")).c_str());
		}
		else if (license.UsesTokens())
		{
			m_LicenseCfgButton.EnableWindow(TRUE);
			setupButtonText(m_LicenseCfgButton, YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_13, _YLOC("License Token Management")).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_14, _YLOC("Get more tokens.")).c_str());
		}
		else if (license.UsesCaps())
		{
			m_LicenseCfgButton.EnableWindow(TRUE);
			const auto fuc = license.IsFeatureSet(LicenseUtil::g_codeSapio365featureFrozenUserCount);
			m_LicenseUpgradeButton.EnableWindow(fuc ? FALSE : TRUE);
			setupButtonText(m_LicenseCfgButton, YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_15, _YLOC("License Configuration Settings")).c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_16, _YLOC("Enter new tenants and number of users in sapio365.")).c_str());
		}

// 			static const wstring leaseEndXaml =
// 				_YTEXT("        <TextBlock VerticalAlignment='Center' Margin='10, 10, 0, 15' Foreground='#262626' FontWeight='normal' FontSize='12' FontFamily='segoe UI'>")
// 				_YTEXT("            %s")
// 				_YTEXT("            <LineBreak/>")
// 				_YTEXT("            <Bold>%s</Bold>")
// 				_YTEXT("        </TextBlock>")
// 				;
// 
// 			const wstring leaseEnd = _YTEXTFORMAT(leaseEndXaml.c_str(), YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_6, _YLOC("Your account will be referenced again at Lease end")).c_str(), 
// 												  _YTEXTFORMAT(L"%s [%s]", license.GetExpirationLeaseStr().c_str(), TimeUtil::GetInstance().GetDayHourMinuteSceonds(license.m_RemainingLease).c_str()).c_str());
// 
// 			formatter += leaseEnd;


		formatter += _YTEXT("</StackPanel>");

		m_LicenseLabel.SetMarkupText(formatter.c_str());

		m_LicenseRefreshButton.EnableWindow(license.IsNone() ? FALSE : TRUE);
		m_LicenseDeauthorizeComputerButton.EnableWindow(license.IsNone() ? FALSE : TRUE);
	}
}

// ============================================================================

const std::vector<UINT> BackstagePanelAbout::g_CrashSequence{ 'C', 'R' };

BOOL BackstagePanelAbout::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYUP && pMsg->wParam == g_CrashSequence[m_CrashSequenceIndex] && MFCUtil::isSHIFTDOWN())
	{
		wostringstream oss;
		oss << std::endl << "Seq " << g_CrashSequence[m_CrashSequenceIndex];
		OutputDebugString(oss.str().c_str());
		if (++m_CrashSequenceIndex == g_CrashSequence.size())
		{
			doCrashPureVirtualCall();
			m_CrashSequenceIndex = 0;
		}
	}
	else if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		if (m_CrashSequenceIndex > 0 && pMsg->wParam != g_CrashSequence[m_CrashSequenceIndex - 1])
			m_CrashSequenceIndex = 0;
	}

	return BackstagePanelBase::PreTranslateMessage(pMsg);
}

void BackstagePanelAbout::doCrashWinChangeNullPointer()
{
	int* p = NULL;
	*p = 0;
}

namespace
{
	struct Bar
	{
		virtual ~Bar()
		{
			bar();
		}

		void bar()
		{
			vbar();
		}
		virtual void vbar() = 0;
	};

	struct Foo : public Bar
	{
		virtual void vbar() {}
	};
}

void
BackstagePanelAbout::doCrashPureVirtualCall()
{
	Foo foobar;
}

