#pragma once

#include "CompositeLogger.h"

class BackstageLogger : public CompositeLogger
{
public:
	BackstageLogger();
	virtual ~BackstageLogger() override;

	static bool HasInstance();
	static BackstageLogger* Instance();

private:
	static BackstageLogger* g_Instance;
	static bool g_createdOnce;
};
