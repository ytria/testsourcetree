#include "BackstagePanelSettings.h"

#include "BackstagePanelSettingsContent.h"
#include "BaseO365Grid.h"

#include "CodeJockThemed.h"
#include "HistoryMode.h"
#include "ModuleBase.h"
#include "NetworkUtils.h"
#include "RegistryURLProtocol.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTranslate.h"

BEGIN_MESSAGE_MAP(BackstagePanelSettings, BackstagePanelBase)
	ON_BN_CLICKED(ID_BACKSTAGE_SETTINGS_RESET,	OnReset)
	ON_BN_CLICKED(ID_BACKSTAGE_SETTINGS_PROXY,	OnProxySettings)
	ON_BN_CLICKED(ID_BACKSTAGE_SETTINGS_URL,	OnEnableURL)
END_MESSAGE_MAP()

BackstagePanelSettings::BackstagePanelSettings()
	: BackstagePanelBase(IDD)

{
}

BackstagePanelSettings::~BackstagePanelSettings()
{
}

BOOL BackstagePanelSettings::OnInitDialog() 
{
	BackstagePanelBase::OnInitDialog();

	setCaption(YtriaTranslate::Do(BackstagePanelSettings_OnInitDialog_1, _YLOC("Preferences")).c_str());
	//ModifyStyleEx(0, WS_EX_CONTROLPARENT);

	UINT ids[]{ ID_BACKSTAGE_SETTINGS_RESET, ID_BACKSTAGE_SETTINGS_PROXY, ID_BACKSTAGE_SETTINGS_URL };
	m_imagelist.SetIcons(IDB_BACKSTAGE_SETTINGS_BUTTON_ICONS, ids, sizeof(ids) / sizeof(UINT), CSize(0, 0), xtpImageNormal);

	SetupButton(
		m_btnReset,
		YtriaTranslate::Do(BackstagePanelSettings_OnInitDialog_2, _YLOC("Reset")).c_str(),
		YtriaTranslate::Do(BackstagePanelSettings_OnInitDialog_5, _YLOC("Reset the list of sapio365 preferences to default.")).c_str(),
		m_imagelist);

	SetupButton(
		m_btnProxySettings,
		YtriaTranslate::Do(BackstagePanelSettings_OnInitDialog_4, _YLOC("Proxy Settings...")).c_str(),
		YtriaTranslate::Do(BackstagePanelSettings_OnInitDialog_6, _YLOC("Edit Proxy settings.")).c_str(),
		m_imagelist);

	SetupButton(
		m_btnEnableUrlLaunch,
		_T("Enable launch from URL"),
		_T("Enable 'sapio365://'-type URLs"),
		m_imagelist);

	m_Content = std::make_unique<BackstagePanelSettingsContent>(this);

	CRect rc;
	GetDlgItem(IDC_BACKSTAGE_SETTINGS_PLACEHOLDER)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BACKSTAGE_SETTINGS_PLACEHOLDER)->GetWindowRect(rc);
	ScreenToClient(&rc);
	m_Content->MoveWindow(rc);
	SetResize(m_Content.get(), XTP_ATTR_RESIZE(1.f), rc);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void BackstagePanelSettings::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelBase::DoDataExchange(pDX);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_RESET,							m_btnReset);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_PROXY,							m_btnProxySettings);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_URL,								m_btnEnableUrlLaunch);
}

BOOL BackstagePanelSettings::OnSetActive()
{
	m_Content->SetActive();

	m_btnEnableUrlLaunch.SetCheck(RegistryURLProtocol::IsURLProtocolRegistered() ? BST_CHECKED : BST_UNCHECKED);

	return BackstagePanelBase::OnSetActive();
}

BOOL BackstagePanelSettings::OnKillActive()
{
	return BackstagePanelBase::OnKillActive();
}

void BackstagePanelSettings::OnReset()
{
	YCodeJockMessageBox msgBox(
		nullptr,
		DlgMessageBox::eIcon_Question,
		YtriaTranslate::Do(BackstagePanelSettings_OnReset_1, _YLOC("Reset Settings")).c_str(),
		YtriaTranslate::Do(BackstagePanelSettings_OnReset_2, _YLOC("Are you sure?")).c_str(),
		_YTEXT(""),
		{ { IDYES, YtriaTranslate::Do(BackstagePanelSettings_OnReset_3, _YLOC("Yes")).c_str() },{ IDNO, YtriaTranslate::Do(BackstagePanelSettings_OnReset_4, _YLOC("No")).c_str() } });
	if (msgBox.DoModal() == IDYES)
	{
		m_Content->Reset();
	}
}

void BackstagePanelSettings::SetTheme(const XTPControlTheme nTheme)
{
 	BackstagePanelBase::SetTheme(nTheme);
 	m_btnReset.SetTheme(nTheme);
 	m_btnProxySettings.SetTheme(nTheme);
	m_btnEnableUrlLaunch.SetTheme(nTheme);
	m_Content->SetTheme();
	m_Content->ShowScrollBar(SB_VERT, FALSE);
 }

void BackstagePanelSettings::OnProxySettings()
{
	NetworkUtils::ShowProxyDialog(this);
}

void BackstagePanelSettings::OnEnableURL()
{
	if (RegistryURLProtocol::IsURLProtocolRegistered())
		RegistryURLProtocol::RemoveURLProtocol();
	else
		RegistryURLProtocol().CreateURLProtocol();

	m_btnEnableUrlLaunch.SetCheck(RegistryURLProtocol::IsURLProtocolRegistered() ? BST_CHECKED : BST_UNCHECKED);
}
