#pragma once

#include "BackstagePanelWithGrid.h"
#include "LogGrid.h"
#include "..\..\Resource.h"

class BackstagePanelLog : public BackstagePanelWithGrid<LogGrid>
{
public:
	BackstagePanelLog(CFrameWnd& frame);
	~BackstagePanelLog() override;

	enum { IDD = IDD_BACKSTAGEPAGE_LOG };

	virtual BOOL OnInitDialog() override;
	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

	void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP();

	virtual void SetTheme(const XTPControlTheme nTheme) override;

private:
	//afx_msg void OnPause();
	afx_msg void OnClear();
	afx_msg void OnSend();
	afx_msg void OnDumpMode();

	//CXTPRibbonBackstageButton m_btnPause;
	CXTPRibbonBackstageButton m_btnClear;
	CXTPRibbonBackstageButton m_btnSend;
	CXTPRibbonBackstageButton m_btnDump;
	CXTPImageManager m_imagelist;
};