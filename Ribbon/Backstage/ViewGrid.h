#pragma once

#include "GridView.h"
#include "CacheGrid.h"

class BackstagePanelViews;
class ViewGrid : public CacheGrid // cannot be SQLiteGrid because of m_ColumnSystem
{
public:
	ViewGrid(const wstring& p_Module);
	~ViewGrid();

	void LoadViews();
	void GetSelectedViews(vector<GridView>& p_Views) const;
	void GetSelectedIDs(vector<int64_t>& p_ViewIDs) const;

	const wstring& GetModuleName() const;

private:
	virtual void customizeGrid() override;
	virtual void OnCustomDoubleClick() override;

	GridBackendColumn* m_ColumnRowID;
	GridBackendColumn* m_ColumnDefault;
	GridBackendColumn* m_ColumnSystem;
	GridBackendColumn* m_ColumnName;
	GridBackendColumn* m_ColumnDescription;
	GridBackendColumn* m_ColumnDateLastUpdate;
	GridBackendColumn* m_ColumnDateLastUpdateBy;
	GridBackendColumn* m_ColumnDateLastUpdateByEmail;
	GridBackendColumn* m_ColumnDateLastUpdateByID;// user graph ID

	BackstagePanelViews* m_PanelViews;

	int32_t ICON_SYSTEM, ICON_DEFAULT;

	const wstring m_Module;
};