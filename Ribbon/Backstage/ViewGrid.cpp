#include "ViewGrid.h"

#include "BackstagePanelViews.h"
#include "GridViewManager.h"
#include "..\..\Resource.h"

ViewGrid::ViewGrid(const wstring& p_Module) : CacheGrid(0, GridBackendUtil::CREATESORTING | GridBackendUtil::CREATESTATUSBAR),
	m_ColumnDefault(nullptr),
	m_Module(p_Module),
	m_PanelViews(nullptr)
{
}

ViewGrid::~ViewGrid()
{
}

void ViewGrid::customizeGrid()
{
	m_PanelViews = dynamic_cast<BackstagePanelViews*>(GetParent());
	ASSERT(nullptr != m_PanelViews);	
	
	SetAutomationName(_YTEXT("ViewGrid"));
	SetAutomationActionRecording(false);

	ICON_SYSTEM = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_VIEWS_SYSTEM));
	ASSERT(GetImageCount() == ICON_SYSTEM + 1);

	ICON_DEFAULT = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_VIEWS_DEFAULT));
	ASSERT(GetImageCount() == ICON_DEFAULT + 1);

	m_ColumnSystem					= AddColumnIcon(_YUID("System"), _T("Is System"), GridBackendUtil::g_DummyString);
	m_ColumnDefault					= AddColumnIcon(GridViewUtil::g_ColumnNameDefault, _T("Is Default"), GridBackendUtil::g_DummyString);
	m_ColumnName					= AddColumn(GridViewUtil::g_ColumnNameViewName, _T("Name"), GridBackendUtil::g_DummyString);
	m_ColumnDescription				= AddColumn(GridViewUtil::g_ColumnNameDescription, _T("Description"), GridBackendUtil::g_DummyString);
	m_ColumnDateLastUpdate			= AddColumnDate(_YUID("LastUpdate"), _T("Last Updated On"), GridBackendUtil::g_DummyString);
	m_ColumnDateLastUpdateBy		= AddColumn(_YUID("LastUpdateBy"), _T("Last Updated By"), GridBackendUtil::g_DummyString);
	m_ColumnDateLastUpdateByEmail	= AddColumn(_YUID("LastUpdateByEmail"), _T("Last Updated By (Email)"), GridBackendUtil::g_DummyString);
	m_ColumnDateLastUpdateByID		= AddColumn(_YUID("LastUpdateByID"), _T("Last Updated By (Graph ID)"), GridBackendUtil::g_DummyString);
	m_ColumnRowID					= AddColumn(_YUID("ROWID"), _T("SQL ID"), GridBackendUtil::g_DummyString);

	SetColumnsVisibleAndOrdered({ m_ColumnSystem, m_ColumnDefault, m_ColumnName, m_ColumnDescription });

	SetColumnWidths(map<GridBackendColumn*, LONG>({ 
						{ m_ColumnSystem,					g_ColumnSize3 },
						{ m_ColumnDefault,					g_ColumnSize3 },
						{ m_ColumnDateLastUpdate,			g_ColumnSize16 },
						{ m_ColumnDateLastUpdateByEmail,	g_ColumnSize12 },
						{ m_ColumnDescription,				g_ColumnSize32*2 },
						{ m_ColumnName,						g_ColumnSize32 },
						{ m_ColumnDateLastUpdateBy,			g_ColumnSize16 },
						{ m_ColumnDateLastUpdateByID,		g_ColumnSize16 }
					}));
	AddColumnForRowPK(m_ColumnRowID);

	m_ColumnName->SetAutoResizeWithGridResize(true);
	m_ColumnDescription->SetAutoResizeWithGridResize(true);
}

void ViewGrid::LoadViews()
{
	CWaitCursor _;
	StoreSelection();// for updates via panel button when plugged into Cosmos
	RemoveAllRows();
	vector<GridView> views;
	GridViewManager::GetInstance().LoadViews(m_Module, views);
	for (const auto& v : views)
	{
		ASSERT(v.HasID());
		if (v.HasID() && (!v.IsSandbox() || CRMpipe().IsFeatureSandboxed()))
		{
			GridBackendRow* row = AddRow();
			ASSERT(nullptr != row);
			if (nullptr != row)
			{
				row->AddField(v.IsSystem() ? _T("System") : _YTEXT(""), m_ColumnSystem, v.IsSystem() ? ICON_SYSTEM : NO_ICON);
				row->AddField(v.IsDefault() ? _T("Default") : _YTEXT(""), m_ColumnDefault, v.IsDefault() ? ICON_DEFAULT : NO_ICON);
				row->AddField(v.m_Name, m_ColumnName);
				row->AddField(v.m_Description, m_ColumnDescription);
				row->AddField(v.m_UpdDate, m_ColumnDateLastUpdate);
				row->AddField(v.m_UpdateByName, m_ColumnDateLastUpdateBy);
				row->AddField(v.m_UpdateByPrincipalName, m_ColumnDateLastUpdateByEmail);
				row->AddField(v.m_UpdateByID, m_ColumnDateLastUpdateByID);
				row->AddField(Str::getStringFromNumber<int64_t>(v.GetID()), m_ColumnRowID);
				row->SetBold(v.IsDefault());
				if (v.IsSandbox())
					row->SetColor(RGB(254, 163, 170));
			}
		}
	}

	UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	RestoreSelection();
	m_PanelViews->SelectionChanged(*this);
}

void ViewGrid::GetSelectedIDs(vector<int64_t>& p_ViewIDs) const
{
	p_ViewIDs.clear();
	vector<GridBackendRow*> rows;
	GetSelectedNonGroupRows(rows);
	for (auto r : rows)
		if (nullptr != r)
			p_ViewIDs.push_back(Str::getINT64FromString(r->GetField(m_ColumnRowID).GetValueStr()));
}

void ViewGrid::GetSelectedViews(vector<GridView>& p_Views) const
{
	p_Views.clear();
	vector<int64_t> vIDs;
	GetSelectedIDs(vIDs);
	if (!vIDs.empty())
		GridViewManager::GetInstance().LoadViews(vIDs, p_Views);
}

void ViewGrid::OnCustomDoubleClick()
{
	ASSERT(nullptr != m_PanelViews);
	if (nullptr != m_PanelViews)
		m_PanelViews->OnGridDoubleClick();
}

const wstring& ViewGrid::GetModuleName() const
{
	return m_Module;
}