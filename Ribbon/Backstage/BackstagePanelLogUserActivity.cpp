#include "BackstagePanelLogUserActivity.h"

#include "ActivityLogger.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "YtriaTranslate.h"

const COLORREF BackstagePanelLogUserActivity::g_ButtonBackColor = RGB(240, 240, 240);

BEGIN_MESSAGE_MAP(BackstagePanelLogUserActivity, BackstagePanelBase)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWLOCAL,		OnBnClickedBackstageButtonUseractivityShowLocal)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWCLOUD,		OnBnClickedBackstageButtonUseractivityShowCloud)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_LOGS_RELOAD,					OnReload)
END_MESSAGE_MAP()

BackstagePanelLogUserActivity::BackstagePanelLogUserActivity() : BackstagePanelBase(IDD)
{
}

BackstagePanelLogUserActivity::~BackstagePanelLogUserActivity()
{
}

BOOL BackstagePanelLogUserActivity::OnInitDialog()
{
	BackstagePanelBase::OnInitDialog();
	setCaption(YtriaTranslate::Do(BackstagePanelLogUserActivity_OnInitDialog_1, _YLOC("User Activity Logs")).c_str());

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWLOCAL };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_USERACTIVITY_SHOWLOCAL, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnShowLocal, YtriaTranslate::Do(BackstagePanelLogUserActivity_OnInitDialog_2, _YLOC("View logs from this computer")).c_str(), _YTEXT(""), m_ImageList);
	}
	m_BtnShowLocal.EnableWindow();

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_LOGS_RELOAD };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_LOGS_RELOAD, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnReload, _YTEXT(""), _YTEXT(""), m_ImageList);
		m_BtnReload.SetCheck(BST_UNCHECKED);
		m_BtnReload.EnableWindow();
	}

	auto panel1 = m_TabPanelLocal.Create(BackstageTabPanelLogUserActivityLocal::IDD, this);
	auto panel2 = m_TabPanelCloud.Create(BackstageTabPanelLogUserActivityCloud::IDD, this);
	ASSERT(panel1);
	ASSERT(panel2);

	m_Panels[IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWLOCAL] = &m_TabPanelLocal;
	m_Panels[IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWCLOUD] = &m_TabPanelCloud;

	CRect aRect;
	m_PseudoTab.GetWindowRect(&aRect);
	m_PseudoTab.ShowWindow(SW_HIDE);
	ScreenToClient(&aRect);

	SetResize(IDC_BACKSTAGE_GRID_AREA, XTP_ATTR_RESIZE(1.f));
	SetResize(&m_TabPanelLocal, XTP_ATTR_RESIZE(1.f), aRect);
	SetResize(&m_TabPanelCloud, XTP_ATTR_RESIZE(1.f), aRect);

	SetResize(IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWLOCAL,	{ 0, 0 },		{ .50f, 0 });
	SetResize(IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWCLOUD,	{ .50f, 0 },	{ 1, 0 });
	SetResize(IDC_BACKSTAGE_BUTTON_LOGS_RELOAD,				{ 1, 0 },		{ 1, 0 });

	m_brushButtonBack.CreateSolidBrush(g_ButtonBackColor);

	showPanel(IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWLOCAL);

	return TRUE == panel1 && TRUE == panel2;
}

void BackstagePanelLogUserActivity::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelBase::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BACKSTAGE_GRID_AREA,						m_PseudoTab);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWLOCAL,	m_BtnShowLocal);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWCLOUD,	m_BtnShowCloud);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_LOGS_RELOAD,				m_BtnReload);
}

void BackstagePanelLogUserActivity::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelBase::SetTheme(nTheme);

	/* Copy pasted from CXTPRibbonBackstagePage::SetTheme for color handling */
	// get the background color from the selected theme if available.
	m_clrText = XTPIniColor(_YTEXT("CommandBars.Ribbon.Backstage"), _YTEXT("PageText"), ::GetSysColor(COLOR_WINDOWTEXT));
	m_clrBack = XTPIniColor(_YTEXT("CommandBars.Ribbon.Backstage"), _YTEXT("PageBackground"), ::GetSysColor(COLOR_WINDOW));

	// destroy the previous brush.
	if (m_xtpBrushBack.GetSafeHandle())
		m_xtpBrushBack.DeleteObject();

	// create a new brush.
	m_xtpBrushBack.CreateSolidBrush(m_clrBack);
	/**********************/

	m_BtnShowLocal.SetTheme(nTheme);
	m_BtnShowCloud.SetTheme(nTheme);
	m_BtnReload.SetTheme(nTheme);
			
	m_TabPanelLocal.SetTheme(nTheme);
	m_TabPanelCloud.SetTheme(nTheme);
}

HBRUSH BackstagePanelLogUserActivity::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
	// Give 'tab' buttons a gray background.
	if (m_BtnShowLocal.m_hWnd == pWnd->m_hWnd || m_BtnShowCloud.m_hWnd == pWnd->m_hWnd)
	{
		// Copy pasted from CXTPRibbonBackstagePage::OnCtlColor
		pDC->SetBkColor(g_ButtonBackColor);
		return (HBRUSH)m_brushButtonBack;
	}

	return BackstagePanelBase::OnCtlColor(pDC, pWnd, nCtlColor);
}

BOOL BackstagePanelLogUserActivity::OnSetActive()
{
	CWaitCursor _;
	if (BackstagePanelBase::OnSetActive() == TRUE)
	{
		const bool isCompanyAdmin = Sapio365Session::IsAdminLogs(getSession());
		m_BtnShowCloud.EnableWindow(isCompanyAdmin);
		{
			UINT ids[]{ IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWCLOUD };
			m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_USERACTIVITY_SHOWCLOUD, ids, 1, CSize(0, 0), xtpImageNormal);
			BackstagePanelBase::SetupButton(m_BtnShowCloud, YtriaTranslate::Do(BackstagePanelLogUserActivity_OnSetActive_1, _YLOC("View all logs")).c_str(), 
											isCompanyAdmin ? _YTEXT("") : YtriaTranslate::Do(BackstagePanelLogUserActivity_OnSetActive_2, _YLOC("Login as a company administrator to view all logs.")).c_str(), 
											m_ImageList);
		}

		if (ActivityLogger::GetInstance().MustClearGrids())
			ClearGrids();

		m_TabPanelLocal.OnSetActive();
		//m_TabPanelCloud.OnSetActive();

		return TRUE;
	}

	return FALSE;
}

BOOL BackstagePanelLogUserActivity::OnKillActive()
{
	if (BackstagePanelBase::OnKillActive() == TRUE)
	{
		m_TabPanelLocal.OnKillActive();
		m_TabPanelCloud.OnKillActive();

		return TRUE;
	}

	return FALSE;
}

void BackstagePanelLogUserActivity::OnBnClickedBackstageButtonUseractivityShowLocal()
{
	showPanel(IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWLOCAL);
}

void BackstagePanelLogUserActivity::OnBnClickedBackstageButtonUseractivityShowCloud()
{
	showPanel(IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWCLOUD);
}

void BackstagePanelLogUserActivity::showPanel(const int p_PanelIndex)
{
	for (const auto& p : m_Panels)
	{
		ASSERT(p.second != nullptr);
		if (p.second != nullptr)
		{
			const bool thisPanel = p.first == p_PanelIndex;

			p.second->ShowWindow(thisPanel ? SW_SHOW : SW_HIDE);

			auto btn = dynamic_cast<CXTPRibbonBackstageButton*>(GetDlgItem(p.first));
			ASSERT(nullptr != btn);
			if (nullptr != btn)
				btn->SetCheck(thisPanel ? BST_CHECKED : BST_UNCHECKED);
		}
	}

	RedrawWindow();

	// do this now to show grid loading in situ
	if (p_PanelIndex == IDC_BACKSTAGE_BUTTON_USERACTIVITY_SHOWCLOUD && !m_TabPanelCloud.IsDataLoaded())
		m_TabPanelCloud.OnSetActive();
}

std::shared_ptr<Sapio365Session> BackstagePanelLogUserActivity::getSession() const
{
	return MainFrameSapio365Session();
}

void BackstagePanelLogUserActivity::ClearGrids()
{
	m_TabPanelLocal.ClearGrid();
	m_TabPanelCloud.ClearGrid();
	ActivityLogger::GetInstance().GridsCleared();
}

void BackstagePanelLogUserActivity::OnReload()
{
	m_TabPanelLocal.OnSetActive();
	m_TabPanelCloud.OnSetActive();
}