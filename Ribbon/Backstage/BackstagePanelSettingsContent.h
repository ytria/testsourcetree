#pragma once

#define ENABLE_DRAGGING 0

#include "GridBackendUtil.h"

class BackstagePanelSettingsContent : public CExtResizableDialog
{
public:
	BackstagePanelSettingsContent(CWnd* pParent = NULL);
	void SetTheme();

	void Reset();
	void SetActive();

protected:
#if ENABLE_DRAGGING
	void EndDrag();
#endif
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()

private:
	afx_msg void OnUseCache(UINT p_Id);
	afx_msg void OnAutoReload(UINT p_Id);
	afx_msg void OnEnableAutoLoadLastSession(UINT p_Id);

	afx_msg void OnRadioHistoryMode(UINT p_Id);
	afx_msg void OnHideDLGRAGroupsL(UINT p_Id);
	afx_msg void OnHideDLGRAGroups(UINT p_Id);
	afx_msg void OnHideDLGRAUsersUA(UINT p_Id);
	afx_msg void OnHideDLGRAUsers(UINT p_Id);
	afx_msg void OnHideDLGRAConsent(UINT p_Id);
	afx_msg void OnHideUAdminDeprecated(UINT p_Id);
	afx_msg void OnFlatViewGroupByHPK(UINT p_Id);
	afx_msg void OnFlatViewHideHTop(UINT p_Id);
	afx_msg void OnFlatViewAskForceUponGroup(UINT p_Id);
	afx_msg void OnHierarchyShowHideDescendantCounters(UINT p_Id);
	afx_msg void OnRadioExplodeAllWhenShortSelection(UINT p_Id);

	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
#if ENABLE_DRAGGING
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
#endif

	void initUseCache();
	void initAutoReload();
	void initEnableAutoLoadLastSession();
	void initHistoryMode();
	void initHideDLGRAGroupsL();
	void initHideDLGRAGroups();
	void initHideDLGRAUsersUA();
	void initHideDLGRAUsers();
	void initHideDLGRAConsent();
	void initHideUAdminDeprecated();
	void initFlatViewGroupByHPK();
	void initViewHideHTop();
	void initViewAskForceUponGroup();
	void initShowHideHierarchyCounters();
	void initExplodeAllWhenShortSelection();

	// GLOBAL
	CExtGroupBox					m_BoxGlobal;

	CExtLabel						m_LabelUseCache;
	CExtRadioButton					m_RadioUseCacheYes;
	CExtRadioButton					m_RadioUseCacheNo;
	CExtRadioButton					m_RadioUseCacheAsk;

	CExtLabel						m_LabelAutoLoadLastSession;
	CExtRadioButton					m_RadioAutoLoadLastSessionYes;
	CExtRadioButton					m_RadioAutoLoadLastSessionNo;

	CExtLabel						m_LabelAutoReload;
	CExtRadioButton					m_RadioAutoReloadYes;
	CExtRadioButton					m_RadioAutoReloadNo;

	// REMINDERS
	CExtGroupBox					m_BoxReminders;

	CExtLabel						m_LabelHideDLGRAGroupsL;
	CExtRadioButton					m_RadioHideDLGRAGroupsLYes;
	CExtRadioButton					m_RadioHideDLGRAGroupsLNo;
	CExtRadioButton					m_RadioHideDLGRAGroupsLAsk;

	CExtLabel						m_LabelHideDLGRAGroups;
	CExtRadioButton					m_RadioHideDLGRAGroupsYes;
	CExtRadioButton					m_RadioHideDLGRAGroupsNo;
	CExtRadioButton					m_RadioHideDLGRAGroupsAsk;

	CExtLabel						m_LabelHideDLGRAUsersUA;
	CExtRadioButton					m_RadioHideDLGRAUsersUAYes;
	CExtRadioButton					m_RadioHideDLGRAUsersUANo;
	CExtRadioButton					m_RadioHideDLGRAUsersUAAsk;

	CExtLabel						m_LabelHideDLGRAUsers;
	CExtRadioButton					m_RadioHideDLGRAUsersYes;
	CExtRadioButton					m_RadioHideDLGRAUsersNo;
	CExtRadioButton					m_RadioHideDLGRAUsersAsk;
		
	CExtLabel						m_LabelHideDLGRAConsent;
	CExtRadioButton					m_RadioHideDLGRAConsentYes;
	CExtRadioButton					m_RadioHideDLGRAConsentNo;
	CExtRadioButton					m_RadioHideDLGRAConsentAsk;

	CExtLabel						m_LabelHideUltraAdminDeprecated;
	CExtRadioButton					m_RadioHideUltraAdminDeprecatedYes;
	CExtRadioButton					m_RadioHideUltraAdminDeprecatedNo;
	CExtRadioButton					m_RadioHideUltraAdminDeprecatedAsk;
	//////////////////

	// HISTORY MODE
	CExtGroupBox					m_BoxHistoryMode;
	CExtRadioButton					m_RadioAskEveryTime;
	CExtRadioButton					m_RadioNewHistory;
	CExtRadioButton					m_RadioDetachHistory;
	CExtRadioButton					m_RadioRetainHistory;
	//////////////////

	// GRID OPTIONS
	CExtGroupBox					m_GridOptionBox;
	CExtLabel						m_LabelFlatViewGroupByHPK;
	CExtRadioButton					m_RadioFlatViewGroupByHPKYes;
	CExtRadioButton					m_RadioFlatViewGroupByHPKNo;

	CExtLabel						m_LabelFlatViewHideTopRows;
	CExtRadioButton					m_RadioFlatViewHideTopRowsYes;
	CExtRadioButton					m_RadioFlatViewHideTopRowsNo;

	CExtLabel						m_LabelFlatViewAskForceUponGrouping;
	CExtRadioButton					m_RadioFlatViewAskForceUponGroupingYes;
	CExtRadioButton					m_RadioFlatViewAskForceUponGroupingNo;

	CExtLabel						m_LabelHierarchyShowHideDescendantCounters;
	CExtRadioButton					m_RadioHierarchyShowHideDescendantCountersYes;
	CExtRadioButton					m_RadioHierarchyShowHideDescendantCountersNo;

	CExtLabel						m_LabelExplodeAllWShortSelection;
	CExtRadioButton					m_RadioExplodeAllWShortSelectionYes;
	CExtRadioButton					m_RadioExplodeAllWShortSelectionNo;
	CExtRadioButton					m_RadioExplodeAllWShortSelectionAsk;
	//////////////////
	
	GridOptionsRegistry m_GridOptions;

	// For scrolling
	CRect	m_rcOriginalRect;
#if ENABLE_DRAGGING
	bool	m_bDragging;
	CPoint	m_ptDragPoint;
#endif
	int		m_nScrollPos;
	int		m_nCurHeight;
	//////////////////
};
