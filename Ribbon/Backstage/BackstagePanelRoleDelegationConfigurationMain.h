#pragma once

#include "BackstagePanelWithGrid.h"
#include "RoleDelegationCfgMainGrid.h"
#include "..\..\Resource.h"

class BackstagePanelRoleDelegationConfigurationMain : public BackstagePanelWithGrid<RoleDelegationCfgMainGrid>
{
public:
	BackstagePanelRoleDelegationConfigurationMain();
	~BackstagePanelRoleDelegationConfigurationMain() override;

	enum { IDD = IDD_BACKSTAGEPAGE_ROLEDELEGATION_CFG_MAIN };

	void DoDataExchange(CDataExchange* pDX);
	virtual void SetTheme(const XTPControlTheme nTheme) override;

	virtual BOOL OnInitDialog() override;
	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

	DECLARE_MESSAGE_MAP();
};