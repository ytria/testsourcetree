#include "BackstageTabPanelRoleDelegationConfigurationFilters.h"

#include "DlgRoleEditFilter.h"
#include "RoleDelegationManager.h"
#include "YtriaTranslate.h"

BackstageTabPanelRoleDelegationConfigurationFilters::BackstageTabPanelRoleDelegationConfigurationFilters() : BackstageTabPanelRoleDelegationConfigurationBase<RoleDelegationCfgFiltersGrid>()
{
	m_Grid = std::make_shared<RoleDelegationCfgFiltersGrid>();
}

BackstageTabPanelRoleDelegationConfigurationFilters::~BackstageTabPanelRoleDelegationConfigurationFilters()
{
}

void BackstageTabPanelRoleDelegationConfigurationFilters::OnInitDialogSpecific()
{
}

void BackstageTabPanelRoleDelegationConfigurationFilters::add()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		DlgRoleEditFilter dlg(this);
		if (IDOK == dlg.DoModal())
		{
			auto roleFilter = dlg.GetFilter();
			roleFilter.m_Status = RoleDelegationUtil::RoleStatus::STATUS_OK;
			RoleDelegationManager::GetInstance().Write(roleFilter, getSession());
			m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationFilters::edit()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		const auto filterID = m_Grid->GetSelectedID();
		if (filterID > 0)
		{
			const auto roleFilter = RoleDelegationManager::GetInstance().GetFilter(filterID, getSession());
			ASSERT(roleFilter.IsValid());
			if (roleFilter.IsValid())
			{
				DlgRoleEditFilter dlg(this);
				dlg.SetFilter(roleFilter);
				if (IDOK == dlg.DoModal())
				{
					auto roleFilterEdited = dlg.GetFilter();
					RoleDelegationManager::GetInstance().Write(roleFilterEdited, getSession());
					m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
				}
			}
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationFilters::remove()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		auto selectedFilterIDs = m_Grid->GetSelectedIDs();
		if (!selectedFilterIDs.empty())
		{
			YCodeJockMessageBox dlg(this,
									DlgMessageBox::eIcon_Question,
									YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_OnRemoveFilter_3, _YLOC("%1 Scope(s) selected for removal\nAre you sure?"), Str::getStringFromNumber<size_t>(selectedFilterIDs.size()).c_str()),
									_YTEXT(""),
									_YTEXT(""),
									{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
			if (IDOK == dlg.DoModal())
			{
				bool removed = false;
				map<wstring, set<wstring>> delegationsByUndeadFilter;
				for (const auto& filterID : selectedFilterIDs)
				{
					auto filter = RoleDelegationManager::GetInstance().GetFilter(filterID, getSession());
					ASSERT(filter.IsValid());
					if (filter.IsValid())
					{
						const auto delegations = RoleDelegationManager::GetInstance().ReadDelegationsFeaturingThisFilter(filterID, getSession());
						if (!delegations.empty())
						{
							for (const auto& d : delegations)
								delegationsByUndeadFilter[filter.m_Name].insert(d.m_Delegation.m_Name);
						}
						else
						{
							filter.m_Status = RoleDelegationUtil::STATUS_REMOVED;
							RoleDelegationManager::GetInstance().Write(filter, getSession());
							removed = true;
						}
					}
				}

				if (removed)
					m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));

				vector<wstring> errorList;
				for (const auto& p : delegationsByUndeadFilter)
					errorList.push_back(YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationFilters_remove_6, _YLOC("Scope %1 was not removed because it is used in roles: %2"), p.first.c_str(), Str::implode(p.second, _YTEXT(", ")).c_str()));
				if (!errorList.empty())
				{
					YCodeJockMessageBox dlg(this,
											DlgMessageBox::eIcon_ExclamationWarning,
											YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationFilters_remove_3, _YLOC("Unable to remove:")).c_str(),
											Str::implode(errorList, _YTEXT("\n")),
											_YTEXT(""),
											{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
					dlg.DoModal();
				}
			}
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationFilters::updateButtons(const vector<GridBackendRow*>& p_SelectedNonGroupRows)
{
	m_BtnAdd.EnableWindow();
	m_BtnEdit.EnableWindow(p_SelectedNonGroupRows.size() == 1 ? TRUE : FALSE);
	m_BtnRemove.EnableWindow(p_SelectedNonGroupRows.empty() ? FALSE : TRUE);
}
