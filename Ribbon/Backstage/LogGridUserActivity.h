#pragma once

#include "ActivitySQLiteLogger.h"
#include "CacheGrid.h"
#include "FrameLogger.h"

class LogGridUserActivity : public CacheGrid
{
public:
	LogGridUserActivity(CFrameWnd& contextFrame);

	void SetIsActive(bool p_Active);
	void LoadSQLiintoGrid();

protected:
	virtual void customizeGrid() override;

private:

	DECLARE_MESSAGE_MAP()
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	bool isError();

	ActivitySQLiteLogger m_SQLsource;
	map<wstring, GridBackendColumn*> m_SQLColumns;

	bool m_Active;

	int64_t m_LastLoadedRowID;

	static const int		g_TimerID;
	static const int64_t	g_UpdateMinPeriodMs;
};