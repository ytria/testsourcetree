#pragma once

#include "BackstagePanelWithGrid.h"
#include "RoleDelegationCfgKeysGrid.h"
#include "..\..\Resource.h"

class BackstagePanelRoleDelegationConfigurationKeys : public BackstagePanelWithGrid<RoleDelegationCfgKeysGrid>
{
public:
	BackstagePanelRoleDelegationConfigurationKeys();
	~BackstagePanelRoleDelegationConfigurationKeys() override;

	enum { IDD = IDD_BACKSTAGEPAGE_ROLEDELEGATION_CFG_KEYS };

	void DoDataExchange(CDataExchange* pDX);
	virtual void SetTheme(const XTPControlTheme nTheme) override;

	virtual BOOL OnInitDialog() override;
	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

	DECLARE_MESSAGE_MAP();
};