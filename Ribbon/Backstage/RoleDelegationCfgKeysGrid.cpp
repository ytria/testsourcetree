#include "RoleDelegationCfgKeysGrid.h"

#include "RoleDelegationManager.h"

RoleDelegationCfgKeysGrid::RoleDelegationCfgKeysGrid() : RoleDelegationCfgGridBase()
{
}

void RoleDelegationCfgKeysGrid::customizeGridSpecific()
{
	SetAutomationName(_YTEXT("RoleDelegationCfgKeysGrid"));
	SetAutomationActionRecording(false);

	SetSQLiteEngine(RoleDelegationManager::GetInstance().GetRoleDelegationSQLite(), RoleDelegationManager::GetInstance().GetRoleDelegationSQLite().GetTableKeys());
}

void RoleDelegationCfgKeysGrid::customizeGridPostProcessSpecific()
{
	GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameAppID)->SetWidth(m_ColumnSize32);
	GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameLogin)->SetWidth(m_ColumnSize32);
}

void RoleDelegationCfgKeysGrid::loadPostProcessSpecific(GridBackendRow* p_Row)
{
}

void RoleDelegationCfgKeysGrid::prepareSQLoadSpecific()
{
}