#include "BackstageTabPanelLogUserActivityCloud.h"

#include "BackstagePanelLogUserActivity.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "Sapio365Session.h"

BEGIN_MESSAGE_MAP(BackstageTabPanelLogUserActivityCloud, CExtResizableDialog)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_USERACTIVITY_FILTERBYOWNER,	OnBnClickedBackstageButtonUseractivityFilterByOwner)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_USERACTIVITY_FILTERCLEAR,	OnBnClickedBackstageButtonUseractivityFilterClear)
END_MESSAGE_MAP()

BackstageTabPanelLogUserActivityCloud::BackstageTabPanelLogUserActivityCloud()
{
}

BackstageTabPanelLogUserActivityCloud::~BackstageTabPanelLogUserActivityCloud()
{
}

BOOL BackstageTabPanelLogUserActivityCloud::OnInitDialog()
{
	BOOL rv = CExtResizableDialog::OnInitDialog();

	CRect aRect;
	m_staticGridArea.GetWindowRect(&aRect);
	m_staticGridArea.ShowWindow(SW_HIDE);
	ScreenToClient(&aRect);

	if (m_Grid.Create(this, aRect, IDC_BACKSTAGE_GRID))
	{
		m_Grid.ModifyStyle(0, WS_BORDER);
		m_Grid.ShowWindow(SW_SHOW);
		m_Grid.BringWindowToTop();
	}
	else
	{
		ASSERT(FALSE);
		rv = -1;
	}

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_USERACTIVITY_FILTERBYOWNER };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_USERACTIVITY_FILTERBYOWNER, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnFilterByOwner, _T("Show Logs related to Selection"), _YTEXT(""), m_ImageList);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_USERACTIVITY_FILTERCLEAR };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_USERACTIVITY_FILTERCLEAR, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnFitlerClear, _T("Show All Logs"), _YTEXT(""), m_ImageList);
	}

	SetBkColor(RGB(255, 255, 255));
	AddAnchor(IDC_BACKSTAGE_GRID, CSize(0, 0), CSize(100, 100));
	m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);// create headers
	m_Grid.AddSelectionObserver(this);

	return rv;
}

void BackstageTabPanelLogUserActivityCloud::DoDataExchange(CDataExchange* pDX)
{
	CExtResizableDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BACKSTAGE_GRID_AREA,							m_staticGridArea);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_USERACTIVITY_FILTERBYOWNER,	m_BtnFilterByOwner);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_USERACTIVITY_FILTERCLEAR,		m_BtnFitlerClear);
}

void BackstageTabPanelLogUserActivityCloud::SetTheme(const XTPControlTheme nTheme)
{
	m_BtnFilterByOwner.SetTheme(nTheme);
	m_BtnFitlerClear.SetTheme(nTheme);
}

BOOL BackstageTabPanelLogUserActivityCloud::OnSetActive()
{
	auto session = getSession();
	if (Sapio365Session::IsAdminLogs(session))
	{
		m_Grid.SetIsActive(true);// loads data from Cosmos - can be a long process
	}
	else
	{
		ClearGrid();
		m_Grid.SetIsActive(false);
	}

	return TRUE;
}

bool BackstageTabPanelLogUserActivityCloud::IsDataLoaded() const
{
	return m_Grid.IsActive();
}

BOOL BackstageTabPanelLogUserActivityCloud::OnKillActive()
{
	m_Grid.SetIsActive(false);
	return TRUE;
}

std::shared_ptr<Sapio365Session> BackstageTabPanelLogUserActivityCloud::getSession() const
{
	return MainFrameSapio365Session();
}

void BackstageTabPanelLogUserActivityCloud::ClearGrid()
{
	m_Grid.RemoveAllRows();
	m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void BackstageTabPanelLogUserActivityCloud::SelectionChanged(const CacheGrid& grid)
{
	updateButtons();
}

void BackstageTabPanelLogUserActivityCloud::OnBnClickedBackstageButtonUseractivityFilterByOwner()
{
	m_Grid.FilterOwnerSelection();
	updateButtons();
}

void BackstageTabPanelLogUserActivityCloud::OnBnClickedBackstageButtonUseractivityFilterClear()
{
	m_Grid.FilterOnwerClear();
	updateButtons();
}

void BackstageTabPanelLogUserActivityCloud::updateButtons()
{
	m_BtnFilterByOwner.EnableWindow(m_Grid.HasOwnerFilter() ? FALSE : TRUE);
	m_BtnFitlerClear.EnableWindow(m_Grid.HasOwnerFilter() ? TRUE : FALSE);
}