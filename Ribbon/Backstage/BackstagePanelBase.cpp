#include "BackstagePanelBase.h"
#include "..\Resource.h"

BEGIN_MESSAGE_MAP(BackstagePanelBase, CXTPRibbonBackstagePage)
END_MESSAGE_MAP()

BackstagePanelBase::BackstagePanelBase(UINT nID)
	: CXTPRibbonBackstagePage(nID)
{
	// Customize m_xtpFontCaption
	{
		LOGFONT lf;
		XTPDrawHelpers()->GetIconLogFont(&lf);

		lf.lfHeight = XTP_DPI_Y(30);
		STRCPY_S(lf.lfFaceName, LF_FACESIZE, _YTEXT("Segoe UI"));
		lf.lfWeight = FW_NORMAL;

		m_xtpFontCaption.CreateFontIndirect(&lf);
	}
}

void BackstagePanelBase::SetTheme(const XTPControlTheme nTheme)
{
	CXTPRibbonBackstagePage::SetTheme(nTheme);

	m_separator.SetTheme(GetTheme());
	m_caption.SetTheme(GetTheme());
}

void BackstagePanelBase::DoDataExchange(CDataExchange* pDX)
{
	CXTPRibbonBackstagePage::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BACKSTAGE_SEPARATOR, m_separator);
	DDX_Control(pDX, IDC_BACKSTAGE_CAPTION, m_caption);
}

BOOL BackstagePanelBase::OnInitDialog()
{
	CXTPRibbonBackstagePage::OnInitDialog();

	ModifyStyleEx(0, WS_EX_CONTROLPARENT);

	m_caption.SetFont(&m_xtpFontCaption);

	SetResize(IDC_BACKSTAGE_SEPARATOR, XTP_ATTR_HORRESIZE(1.f));
	SetResize(IDC_BACKSTAGE_CAPTION, XTP_ATTR_HORRESIZE(1.f));
	m_separator.ShowWindow(SW_HIDE);


	return TRUE;
}

BOOL BackstagePanelBase::OnSetActive()
{
	if (TRUE == CXTPRibbonBackstagePage::OnSetActive())
	{
		m_isActive = true;
		return TRUE;
	}

	return FALSE;
}

BOOL BackstagePanelBase::OnKillActive()
{
	if (TRUE == CXTPRibbonBackstagePage::OnKillActive())
	{
		m_isActive = false;
		return TRUE;
	}

	return FALSE;
}

bool BackstagePanelBase::IsActive() const
{
	return m_isActive;
}

void BackstagePanelBase::setCaption(const wstring& p_Caption)
{
	m_caption.SetWindowText(CString(p_Caption.c_str()));
}

void BackstagePanelBase::SetupButton(CXTPRibbonBackstageButton& p_Button, const wstring& p_Title, const wstring& p_Description, const CXTPImageManager& p_ImageManager)
{
	p_Button.SetTextAlignment(BS_LEFT | BS_VCENTER);
	p_Button.SetImageAlignment(BS_LEFT | BS_VCENTER);

	p_Button.SetFlatStyle(TRUE);
	p_Button.ShowShadow(FALSE);

	p_Button.EnableMarkup(TRUE);
	p_Button.SetDpiImageScaling(TRUE);

	setupButtonText(p_Button, p_Title, p_Description);

	CXTPImageManagerIcon* icon = p_ImageManager.GetImage(p_Button.GetDlgCtrlID());
	//ASSERT(nullptr != icon);
	if (nullptr != icon)
	{
		p_Button.SetIcon(icon->GetExtent(), icon);
		icon->InternalAddRef();
	}
}

void BackstagePanelBase::setupButtonText(CXTPRibbonBackstageButton& p_Button, const wstring& p_Title, const wstring& p_Description)
{
#define BUTTON_MARKUP \
	L"<StackPanel Margin='5, 3, 5, 3'>" \
	"<TextBlock FontSize='12' FontFamily='Segoe UI' Foreground='#262626'><Bold>%s</Bold></TextBlock>" \
	"<TextBlock FontSize='11' FontFamily='Segoe UI' TextWrapping='Wrap' Foreground='#6a6a6a' Margin='0, 3, 0, 0'>%s</TextBlock>" \
	"</StackPanel>"

#define BUTTON_MARKUP_NO_TITLE \
	L"<StackPanel Margin='5, 3, 5, 3'>" \
	"<TextBlock FontSize='11' FontFamily='Segoe UI' TextWrapping='Wrap' Foreground='#6a6a6a' Margin='0, 3, 0, 0'>%s</TextBlock>" \
	"</StackPanel>"

#define BUTTON_MARKUP_NO_DESC \
	L"<StackPanel Margin='5, 3, 5, 3'>" \
	"<TextBlock FontSize='12' FontFamily='Segoe UI' Foreground='#262626'><Bold>%s</Bold></TextBlock>" \
	"</StackPanel>"

	CString strWindowText;
	if (p_Title.empty())
		strWindowText.Format(BUTTON_MARKUP_NO_TITLE, p_Description.c_str());
	else if (p_Description.empty())
		strWindowText.Format(BUTTON_MARKUP_NO_DESC, p_Title.c_str());
	else
		strWindowText.Format(BUTTON_MARKUP, p_Title.c_str(), p_Description.c_str());
	p_Button.SetWindowText(strWindowText);
}

void BackstagePanelBase::CloseBackstage()
{
	CXTPRibbonBackstageView* pView = dynamic_cast<CXTPRibbonBackstageView*>(GetParent());
	ASSERT(nullptr != pView);
	if (nullptr != pView)
		pView->OnCancel();
}
