#pragma once

#include "BackstagePanelWithGrid.h"
#include "RoleDelegationSelectionGrid.h"
#include "Sapio365Session.h"
#include "..\..\Resource.h"

class BackstagePanelRoleDelegation : public BackstagePanelWithGrid<RoleDelegationSelectionGrid>, public GridSelectionObserver
{
public:
	BackstagePanelRoleDelegation();
	~BackstagePanelRoleDelegation() override;

	enum { IDD = IDD_BACKSTAGEPAGE_ROLEDELEGATION };

	void			DoDataExchange(CDataExchange* pDX);
	virtual void	SelectionChanged(const CacheGrid& grid) override;

protected:
	DECLARE_MESSAGE_MAP();
	virtual void SetTheme(const XTPControlTheme nTheme) override;

	virtual BOOL OnInitDialog() override;
	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

private:
	std::shared_ptr<Sapio365Session>	getSession() const;
	void								updateButton();
	void								manageSession(const bool p_Connect);

	afx_msg void OnSeeInfo();
	afx_msg void OnReload();// from cloud

	bool m_ReadyToRoll;

	CXTPRibbonBackstageLabel	m_LabelError;
	CXTPRibbonBackstageButton	m_BtnSeeInfo;
	CXTPRibbonBackstageButton	m_BtnReload;
	CXTPImageManager			m_ImageList;
};