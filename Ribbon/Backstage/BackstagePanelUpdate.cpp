#include "BackstagePanelUpdate.h"

#include "AutomatedApp.h"
#include "AutomatedMainFrame.h"
#include "CodeJockThemed.h"
#include "MainFrame.h"
#include "Product.h"
#include "ProductUpdate.h"
#include "TimeUtil.h"
#include "YtriaTranslate.h"

static const UINT ON_PROGRESS = ::RegisterWindowMessage(_YTEXT("on_progress"));

BEGIN_MESSAGE_MAP(BackstagePanelUpdate, BackstagePanelBase)
	ON_BN_CLICKED(IDC_BACKSTAGE_UPDATE_BTN_UPDATE, OnProceedUpdate)
	ON_REGISTERED_MESSAGE(ON_PROGRESS, OnProgress)
END_MESSAGE_MAP()

BackstagePanelUpdate::BackstagePanelUpdate()
	: BackstagePanelBase(IDD)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
{

}

BackstagePanelUpdate::~BackstagePanelUpdate()
{
}

void BackstagePanelUpdate::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelBase::SetTheme(nTheme);

	m_UpdateButton.SetTheme(nTheme);
	m_Progress.SetTheme(nTheme);
}

void BackstagePanelUpdate::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelBase::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BACKSTAGE_UPDATE_BTN_UPDATE,	m_UpdateButton);
	DDX_Control(pDX, IDC_BACKSTAGE_UPDATE_PROGRESS,		m_Progress);
}

BOOL BackstagePanelUpdate::OnInitDialog()
{
	BackstagePanelBase::OnInitDialog();

	setCaption(YtriaTranslate::Do(BackstagePanelUpdate_OnInitDialog_1, _YLOC("Update sapio365")).c_str());

	SetDlgCtrlID(IDD);
	ModifyStyleEx(0, WS_EX_CONTROLPARENT);

	m_Progress.SetRange(0, 100);

	{
		CWnd* pCtrl = GetDlgItem(IDC_BACKSTAGE_HTMLRELEASENOTES_PLACEHOLDER);
		ASSERT(nullptr != pCtrl);
		CRect htmlRect;
		pCtrl->GetWindowRect(&htmlRect);
		pCtrl->DestroyWindow();
		ScreenToClient(&htmlRect);
		ASSERT(m_HtmlView);
		m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_BACKSTAGE_HTMLRELEASENOTES_PLACEHOLDER, NULL);
		SetResize(IDC_BACKSTAGE_HTMLRELEASENOTES_PLACEHOLDER, { 0, 0 }, { 1, 1 });
		m_HtmlView->ShowWindow(SW_HIDE);
	}

	updateUpdateButton();
	updateHtml();

	return TRUE;
}

void BackstagePanelUpdate::OnProceedUpdate()
{
	ASSERT(nullptr != AfxGetApp());
	if (nullptr != AfxGetApp())
	{
		const bool progress = !ProductUpdate::GetInstance().IsUpdateDownloaded();
		if (progress)
			StartProgress();

		if (!ProductUpdate::GetInstance().Proceed(AfxGetApp()->m_pMainWnd) && progress)
			EndProgress();
	}
}

LRESULT BackstagePanelUpdate::OnProgress(WPARAM, LPARAM)
{
	if (m_Progress.IsWindowVisible())
	{
		m_Progress.SetPos(static_cast<int>(m_NextProgressPercentage + .5));
		m_Progress.SetWindowText(CString(m_NextShortMessage.c_str()));
	}

	if (m_DownloadComplete)
	{
		m_DownloadComplete = false;
		EndProgress();
	}

	return 0;
}

BOOL BackstagePanelUpdate::OnSetActive()
{
	updateUpdateButton();
	updateHtml();
	return BackstagePanelBase::OnSetActive();
}

BOOL BackstagePanelUpdate::OnKillActive()
{
	return BackstagePanelBase::OnKillActive();
}

void BackstagePanelUpdate::Progress(const ProductUpdate& productUpdate, double progressPercentage, const wstring& shortMessage, bool downloadComplete)
{
	m_NextProgressPercentage = progressPercentage;
	m_NextShortMessage = shortMessage;
	m_DownloadComplete = downloadComplete;
	PostMessage(ON_PROGRESS);
}

void BackstagePanelUpdate::updateUpdateButton()
{
	if (ProductUpdate::GetInstance().IsUpdateDownloaded())
	{
		HICON fileIcon = ProductUpdate::GetInstance().GetDownloadedFileIcon();
		if (nullptr != fileIcon)
		{
			m_imagelist.SetIcon(CXTPImageManagerIconHandle(fileIcon, FALSE, FALSE), IDC_BACKSTAGE_UPDATE_BTN_UPDATE);
		}
		else
		{
			UINT ids[]{ IDC_BACKSTAGE_UPDATE_BTN_UPDATE };
			m_imagelist.SetIcons(IDB_BACKSTAGE_BUTTON_UPDATE_INSTALL
				, ids
				, 1
				, CSize(0, 0)
				, xtpImageNormal);
		}

		SetupButton(m_UpdateButton
			, YtriaTranslate::Do(BackstagePanelUpdate_updateUpdateButton_3, _YLOC("New version %1 downloaded"), ProductUpdate::GetInstance().GetAvailableProductVersion().c_str())
			, YtriaTranslate::Do(BackstagePanelUpdate_updateUpdateButton_1, _YLOC("Click to install now.")).c_str()
			, m_imagelist);
		m_UpdateButton.ShowWindow(SW_SHOW);
		m_UpdateButton.EnableWindow(TRUE);
	}
	else if (ProductUpdate::GetInstance().IsUpdateAvailable())
	{
		UINT ids[]{ IDC_BACKSTAGE_UPDATE_BTN_UPDATE };
		m_imagelist.SetIcons(IDB_BACKSTAGE_BUTTON_UPDATE_DOWNLOAD
			, ids
			, 1
			, CSize(0, 0)
			, xtpImageNormal);

		SetupButton(m_UpdateButton
			, YtriaTranslate::Do(BackstagePanelUpdate_updateUpdateButton_4, _YLOC("New version %1 available"), ProductUpdate::GetInstance().GetAvailableProductVersion().c_str())
			, YtriaTranslate::Do(BackstagePanelUpdate_updateUpdateButton_2, _YLOC("Click to proceed.")).c_str()
			, m_imagelist);
		m_UpdateButton.ShowWindow(SW_SHOW);
		m_UpdateButton.EnableWindow(TRUE);
	}
	else
	{
		UINT ids[]{ IDC_BACKSTAGE_UPDATE_BTN_UPDATE };
		m_imagelist.SetIcons(IDB_BACKSTAGE_BUTTON_UPDATE_DOWNLOAD
			, ids
			, 1
			, CSize(0, 0)
			, xtpImageNormal);

		SetupButton(m_UpdateButton
			, _YTEXT("")
			, _YTEXT("")
			, m_imagelist);
		m_UpdateButton.EnableWindow(FALSE);
		m_UpdateButton.ShowWindow(SW_HIDE);
	}	
}

void BackstagePanelUpdate::updateHtml()
{
	ASSERT(m_HtmlView && ::IsWindow(m_HtmlView->m_hWnd));
	if (m_HtmlView && ::IsWindow(m_HtmlView->m_hWnd))
	{
		auto& relNotes = ProductUpdate::GetInstance().GetAvailableProductHTMLReleaseNote();
		if (!relNotes.empty())
		{
			wstring content = relNotes;
			if (content.empty())
				content = YtriaTranslate::Do(BackstagePanelUpdate_updateHtml_1, _YLOC("No release note available.")).c_str();

			m_HtmlView->ShowWindow(SW_SHOW);

			const wstring htmlHeader =
				L"<head>"
				L"<style>"
				L"* {font-family: 'Segoe UI'; font-size:12px; color:#262626; line-height:1.5; }"
				L".version-release-note h1{font-size:18.55px; margin:5px 0; font-weight:normal; color:#336699 }"
				L".version-release-note h2{font-size:15.26px; margin:5px 0; font-weight:normal}"
				L".version-release-note h3{font-size:12px; margin:5px 0; color:#6a6a6a}"
				L".version-release-note p{font-size:12px; margin:0; }"
				L".version-release-note ul{font-size:12px; margin:0; list-style-type: none; padding-left: 30px; }"
				L".version-release-note ul li{font-size:12px; margin:0; list-style-type: none ; /*list-style-type: square;*/}"
				L".version-release-note ul li::before { content: '\25AA'; color: #88888a; font-size: 2rem; line-height: 1rem; margin-right: 5px; position: relative; top: 4px; }"
				L".version-release-note i{font-size:12px; margin:0; color:#6a6a6a }"
				L".version-release-note blockquote {margin:10px 0px 0px 6px; padding: 2px 6px 3px 6px; background-color: #ebebeb; color:#6a6a6a; font-weight:bold; border-bottom:1px solid #e2e4e7}"
				L"</style>"
				L"</head>"
				;
			const wstring htmlBodyStart =
				L"<body>"
				L"<div class=\"version-release-note\">"
				;

			const wstring htmlBodyEnd =
				L"</div>"
				L"</body>";

			m_HtmlView->YLoadFromString(htmlHeader, htmlBodyStart + content + htmlBodyEnd);
		}
		else
		{
			m_HtmlView->ShowWindow(SW_HIDE);
		}
	}
}

void BackstagePanelUpdate::StartProgress()
{
	m_Progress.ShowWindow(SW_SHOW);
	ProductUpdate::GetInstance().AddProductUpdateObserver(this);
}

void BackstagePanelUpdate::EndProgress()
{
	ProductUpdate::GetInstance().RemoveProductUpdateObserver(this);
	m_Progress.ShowWindow(SW_HIDE);

	if (IsActive())
	{
		updateUpdateButton();
	}
	else
	{
		ASSERT(nullptr != AfxGetApp());
		auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
		ASSERT(nullptr != mainFrame);
		if (nullptr != mainFrame)
			mainFrame->showBackstageTab(IDD);
	}
}
