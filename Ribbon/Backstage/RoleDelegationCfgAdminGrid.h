#pragma once

#include "RoleDelegationCfgGridBase.h"
#include "RoleDelegationUtil.h"

class RoleDelegationCfgAdminGrid : public RoleDelegationCfgGridBase
{
public:
	RoleDelegationCfgAdminGrid();

	void GetSelection(map<RoleDelegationUtil::RBAC_AdminScope, map<wstring, wstring>>& p_Selection) const;// map: scope vs. graphID vs. Display Name
	
protected:
	virtual void customizeGridSpecific() override;
	virtual void customizeGridPostProcessSpecific() override;
	virtual void loadPostProcessSpecific(GridBackendRow* p_Row) override;
	virtual void prepareSQLoadSpecific() override;

private:
	map<RoleDelegationUtil::RBAC_AdminScope, GridBackendRow*> m_MainRows;

	GridBackendColumn* m_ColumnAdminScope;
	GridBackendColumn* m_ColumnAdminScopeName;
	GridBackendColumn* m_ColumnUserGraphID;
	GridBackendColumn* m_ColumnUserDisplayName;

	map<RoleDelegationUtil::RBAC_AdminScope, int32_t> m_Icons;

	static vector<std::tuple<rttr::type::type_id, int32_t, wstring>> g_ObjectTypes;
	
	static wstring g_ElementTypeAdminScope;
	static wstring g_ElementTypeUser;

	const static rttr::type::type_id g_ElementTypeIDAdminScope;
	const static rttr::type::type_id g_ElementTypeIDUser;
};