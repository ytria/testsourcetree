#pragma once

#include "BackstagePanelWithGrid.h"
#include "RoleDelegationCfgAdminGrid.h"
#include "..\..\Resource.h"

class Sapio365Session;

class BackstagePanelAdmin : public BackstagePanelWithGrid<RoleDelegationCfgAdminGrid>, public GridSelectionObserver
{
public:
	BackstagePanelAdmin();
	~BackstagePanelAdmin() override;

	enum { IDD = IDD_BACKSTAGEPAGE_SAPIO_ADMIN };

	virtual BOOL OnInitDialog() override;
	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

	void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP();

	virtual void SetTheme(const XTPControlTheme nTheme) override;

private:
	afx_msg void onAdd();
	afx_msg void onRemove();
	
	void showComponents(const bool p_Show);
	void updateButtons(const vector<GridBackendRow*>& p_SelectedNonGroupRows);

	std::shared_ptr<Sapio365Session> getSession() const;

	virtual void SelectionChanged(const CacheGrid& grid) override;

	CXTPRibbonBackstageButton m_BtnAdd;
	CXTPRibbonBackstageButton m_BtnRemove;

	CXTPRibbonBackstageLabel m_Top;

	CXTPImageManager m_ImageManager;

	bool m_ReadyToRoll;
};