#pragma once

#include "DlgFormsHTML.h"
#include "JobCenterUtil.h"

class DlgJobSelectionValidation : public DlgFormsHTML
{
public:
	DlgJobSelectionValidation(CWnd* p_Parent);
	virtual ~DlgJobSelectionValidation();

	const wstring& GetVN() const;

protected:
	virtual void generateJSONScriptData() override;

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

	virtual wstring getDialogTitle() const override;

private:
	wstring m_VN;

	static const wstring& g_VarNameVN;
};