#pragma once

#include "BackstagePanelWithGrid.h"
#include "..\Resource.h"

BEGIN_TEMPLATE_MESSAGE_MAP(BackstagePanelWithGrid, CacheGridClass, BackstagePanelBase)
END_MESSAGE_MAP()

template <class CacheGridClass>
void BackstagePanelWithGrid<CacheGridClass>::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelBase::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BACKSTAGE_GRID_AREA,		m_staticGridArea);
	DDX_Control(pDX, IDC_BACKSTAGE_DETAILED_VIEW,	m_DetailedView);
}

template <class CacheGridClass>
BOOL BackstagePanelWithGrid<CacheGridClass>::OnInitDialog()
{
	// You must create the grid before calling this.
	// m_Grid = std::make_shared<CacheGridClass>();
	ASSERT(m_Grid);

	BackstagePanelBase::OnInitDialog();

	// Current non editable values of the column
	CRect aRect;
	m_staticGridArea.GetWindowRect(&aRect);
	m_staticGridArea.ShowWindow(SW_HIDE);
	ScreenToClient(&aRect);

	if (!m_Grid->Create(this, aRect, IDC_BACKSTAGE_GRID))
	{
		// Creation of grid failed.
		ASSERT(FALSE);
		return -1;
	}
	m_Grid->ModifyStyle(0, WS_BORDER);
	m_Grid->ShowWindow(SW_SHOW);
	m_Grid->BringWindowToTop();

	SetResize(IDC_BACKSTAGE_GRID,			XTP_ATTR_RESIZE(1.f));
	SetResize(IDC_BACKSTAGE_DETAILED_VIEW,	XTP_ATTR_VERREPOS(1.f) + XTP_ATTR_HORRESIZE(1.f));

	m_DetailedView.SetMarkupText(CString());
	m_DetailedView.ShowWindow(SW_HIDE);

	return TRUE;
}

template <class CacheGridClass>
void BackstagePanelWithGrid<CacheGridClass>::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelBase::SetTheme(nTheme);
}
