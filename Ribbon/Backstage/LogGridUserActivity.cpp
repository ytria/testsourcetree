#include "LogGridUserActivity.h"

#include "YCodeJockMessageBox.h"

BEGIN_MESSAGE_MAP(LogGridUserActivity, CacheGrid)
	ON_WM_TIMER()
END_MESSAGE_MAP()

const int64_t	LogGridUserActivity::g_UpdateMinPeriodMs	= 400;
const int		LogGridUserActivity::g_TimerID				= 6969;

LogGridUserActivity::LogGridUserActivity(CFrameWnd& contextFrame) : CacheGrid(0, /*GridBackendUtil::CREATETOOLBAR | */GridBackendUtil::CREATEGROUPAREA | GridBackendUtil::CREATECOLORG | GridBackendUtil::CREATESORTING),
	m_Active(false),
	m_LastLoadedRowID(0)
{
	SetLiveGrid(true);
}

void LogGridUserActivity::customizeGrid()
{
	SetAutomationName(_YTEXT("BackstageLogGridUserActivity"));
	SetAutomationActionRecording(false);

	const auto& SQLtable	= m_SQLsource.GetActivityTable();
	const auto& columnROWID = SQLtable.GetColumnROWID();

	static const wstring g_YUID = _YTEXT("LUA");
	uint32_t k = 1;
	for (const auto& c : SQLtable.m_Columns)
	{
		GridBackendColumn* gridColumn = nullptr;

		wstring YUID = g_YUID + Str::getStringFromNumber(k++);
		if (c.IsBool())
			gridColumn = AddColumnCheckBox(YUID, c.m_Name, GridBackendUtil::g_DummyString);
		else if (c.IsDate())
			gridColumn = AddColumnDate(YUID, c.m_Name, GridBackendUtil::g_DummyString);
		else if (c.IsNumber())
			gridColumn = AddColumnNumber(YUID, c.m_Name, GridBackendUtil::g_DummyString);
		else
			gridColumn = AddColumn(YUID, c.m_Name, GridBackendUtil::g_DummyString);
		
		ASSERT(nullptr != gridColumn);
		if (nullptr != gridColumn)
		{
			gridColumn->SetReadOnly(true);
			m_SQLColumns[c.GetSQLcompliantName()] = gridColumn;
			if (c == columnROWID)
			{
				SetColumnVisible(gridColumn, false, GridBackendUtil::GridBackendUtil::SETVISIBLE_NONE);
				AddSorting(gridColumn, GridBackendUtil::DESC);
			}
		}
	}

	LoadSQLiintoGrid();

	SetTimer(g_TimerID, g_UpdateMinPeriodMs, nullptr);
}

void LogGridUserActivity::LoadSQLiintoGrid()
{
	if (m_Active)
	{
		vector<map<SQLiteUtil::Column, SQLiteUtil::SelectResult>> SQLdata;

		if (!isError())
		{
			SQLiteUtil::SelectQuery select;
			select.m_Table = m_SQLsource.GetActivityTable();

			int64_t intValue;

			m_SQLsource.SelectAll(select, m_LastLoadedRowID, SQLdata);
			if (!isError() && !SQLdata.empty())
			{
				const auto& columnROWID = select.m_Table.GetColumnROWID();
				for (const auto& sqlResult : SQLdata)
				{
					auto row = AddRow();
					for (const auto& result : sqlResult)
					{
						const auto& c = result.first;
						const auto& r = result.second;

						const bool rowID = columnROWID == c;

						auto findIt = m_SQLColumns.find(c.GetSQLcompliantName());
						if (m_SQLColumns.end() != findIt)
						{
							auto gridColumn = findIt->second;
							ASSERT(nullptr != gridColumn);
							if (nullptr != gridColumn)
							{
								if (c.IsBool())
									row->AddFieldForCheckBox(r.m_Double != 0, m_SQLColumns[c.GetSQLcompliantName()]);
								else if (c.IsDate())
								{
									YTimeDate ytd;
									TimeUtil::GetInstance().ConvertTextToDate(r.m_Text, ytd);
									row->AddField(ytd, gridColumn);
								}
								else
								{
									switch (c.m_DataType)
									{
										case SQLiteUtil::DataType::TYPE_INTEGER:
											intValue = static_cast<int64_t>(r.m_Double);
											row->AddField(intValue, gridColumn);
											if (rowID && intValue > m_LastLoadedRowID)
												m_LastLoadedRowID = intValue;
											break;
										case SQLiteUtil::DataType::TYPE_REAL:
											row->AddField(r.m_Double, gridColumn);
											break;
										case SQLiteUtil::DataType::TYPE_TEXT:
											if (c.m_MultivalueDelimiter.empty())
												row->AddField(r.m_Text, gridColumn);
											else
												row->AddField(Str::explodeIntoVector(r.m_Text, c.m_MultivalueDelimiter, true), gridColumn);
											break;
										default:
											ASSERT(false);
											break;
									}
								}
							}
							else
								ASSERT(false);
						}
					}
				}
				UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
			}
		}
	}
}

bool LogGridUserActivity::isError()
{
	bool rvIsError = false;

	const auto& SQLerror = m_SQLsource.GetError();
	rvIsError = !SQLerror.empty();
	if (rvIsError)
	{
		const auto& columns = GetBackend().GetColumns();
		if (columns.empty())
		{
			YCodeJockMessageBox errorMessage(this,
											 DlgMessageBox::eIcon_Error,
											 YtriaTranslate::DoError(LogGridUserActivity_isError_1, _YLOC("Activity Logger configuration"),_YR("Y2430")).c_str(),
											 SQLerror,
											 _YTEXT("- no column -"),
											 { { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
		}
		else
		{
			auto row = AddRow();
			row->AddField(YtriaTranslate::Do(LogGridUserActivity_isError_2, _YLOC("Activity Logger configuration error: %1"), SQLerror.c_str()), columns.front(), GridBackendUtil::ICON_ERROR);
		}
	}

	return rvIsError;
}

void LogGridUserActivity::OnTimer(UINT_PTR nIDEvent)
{
	if (g_TimerID == nIDEvent)
		LoadSQLiintoGrid();
	else
		CacheGrid::OnTimer(nIDEvent);
}

void LogGridUserActivity::SetIsActive(const bool p_Active)
{
	m_Active = p_Active;
}