#pragma once

#include "BackstagePanelBase.h"

#include "BackstagePanelJobCenter.h"
#include "BackstagePanelJobCenterMainFrame.h"
#include "BackstagePanelJobCenterCommon.h"

#include "..\..\Resource.h"

class Sapio365Session;

template <class BackstagePanelJobCenterClass>
class BackstagePanelJobCenterTop : public BackstagePanelBase
{
public:
	BackstagePanelJobCenterTop(CodeJockFrameBase* p_Frame);
	~BackstagePanelJobCenterTop() override;

	enum { IDD = IDD_BACKSTAGEPAGE_JOBCENTER_TOP };
	
	void DoDataExchange(CDataExchange* pDX);

protected:
	DECLARE_MESSAGE_MAP();

	virtual void SetTheme(const XTPControlTheme nTheme) override;

	virtual BOOL OnInitDialog() override;

	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

private:
	afx_msg void OnShowJobs();
	afx_msg void OnShowJobsCommon();
	afx_msg void OnToggleView();

	afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);

	void showPanel(const int p_PanelIndex); // Hides every panel whose index is not p_PanelIndex
	void updateView();

	CStatic				m_PseudoTab;
	CXTPImageManager	m_ImageList;
	
	CodeJockFrameBase*	m_Frame;
	map<int, CWnd*>		m_Panels;
	map<int, bool>		m_SetActiveNeeded;

	bool				m_SimpleView;

	unique_ptr<BackstagePanelJobCenterClass>	m_TabPanelJobs;
	unique_ptr<BackstagePanelJobCenterCommon>	m_TabPanelJobsCommon;

	CXTPRibbonBackstageButton m_BtnShowJobs;
	CXTPRibbonBackstageButton m_BtnShowJobsCommon;

	CXTPBrush m_brushButtonBack;
	static const COLORREF g_ButtonBackColor;
};

#include "BackstagePanelJobCenterTop.hpp"