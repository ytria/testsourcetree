#pragma once

#include "BackstagePanelBase.h"
#include "BackstageRecentList.h"
#include "..\..\Resource.h"

class CodeJockFrameBase;
class MainFrame;

class BackstagePanelAutomation : public BackstagePanelBase
{
public:
	BackstagePanelAutomation();
	BackstagePanelAutomation(CodeJockFrameBase* p_Frame);
	~BackstagePanelAutomation() override;

	enum { IDD = IDD_BACKSTAGEPAGE_AUTOMATION };

	virtual void SetTheme(const XTPControlTheme nTheme) override;

	void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog() override;
	afx_msg void OnRecentFileListClick();
	afx_msg void OnLoad();
	afx_msg void OnRecorder();
	afx_msg void OnConsole();
	afx_msg void OnQuickLaunch();

	DECLARE_MESSAGE_MAP()

	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

private:
	void updateButtons();
	void updateRecentList();
	void saveRecentListPinStates();
	MainFrame* getMainWindow() const;

	static void setExecutionContext(const AutomationContext& autoContext);

	bool isAuthorizedByLicense(const wstring& p_Message) const;

private:
	std::unique_ptr<BackstageRecentList>	m_RecentList;

	CXTPRibbonBackstageButton				m_LoadFileButton;
	CXTPRibbonBackstageButton				m_RecorderButton;
	CXTPRibbonBackstageButton				m_ConsoleButton;
	CXTPRibbonBackstageButton				m_QuickLaunchButton;
	CXTPRibbonBackstageLabel				m_RecentCaption;
	CXTPRibbonBackstageSeparator			m_RecentSeparator;

	CXTPImageManager						m_imagelist;
	
	CodeJockFrameBase*						m_Frame;
};