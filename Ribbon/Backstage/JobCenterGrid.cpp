#include "JobCenterGrid.h"

#include "AutomationWizardO365.h"
#include "BackstagePanelJobCenter.h"
#include "DlgSelectGrid.h"
#include "ResUtil.h"

wstring JobCenterGrid::g_ElementTypeScript;
wstring JobCenterGrid::g_ElementTypeScriptPreset;
wstring JobCenterGrid::g_ElementTypeScriptSchedule;


const rttr::type::type_id JobCenterGrid::g_ElementTypeIDScript			= 1;
const rttr::type::type_id JobCenterGrid::g_ElementTypeIDScriptPreset	= 2;
const rttr::type::type_id JobCenterGrid::g_ElementTypeIDScriptSchedule	= 3;

BEGIN_MESSAGE_MAP(JobCenterGrid, CacheGrid)
	ON_WM_DROPFILES()

	ON_COMMAND(ID_JOBCENTERGRID_SELECTJOBS,				OnSelectAJLjobs)
	ON_UPDATE_COMMAND_UI(ID_JOBCENTERGRID_SELECTJOBS,	OnUpdateSelectJobs)

END_MESSAGE_MAP()

JobCenterGrid::JobCenterGrid(bool p_WithJobsOrdering)
	: CacheGrid(0, GridBackendUtil::CREATESTATUSBAR | (p_WithJobsOrdering ? 0 : GridBackendUtil::CREATESORTING))
	, m_JobCenter(nullptr)
	, m_ColumnMoveUp(nullptr)
	, m_ColumnMoveDown(nullptr)
	, m_ColumnDisplayed(nullptr)
	, m_PanelJobCenter(nullptr)
	, m_ColumnShared(nullptr)
	, m_ColumnDateLastShared(nullptr)
	, m_ColumnPosition(nullptr)
	, m_SimpleView(true)
	, m_WithJobsOrdering(p_WithJobsOrdering)
	, m_HasTooltips(false)
{
}

JobCenterGrid::~JobCenterGrid()
{
	RemoveFilterObserver(this);
}

void JobCenterGrid::customizeGrid()
{
	m_PanelJobCenter = dynamic_cast<BackstagePanelJobCenterBase*>(GetParent());
	ASSERT(nullptr != m_PanelJobCenter);

	SetAutomationName(_YTEXT("JobCenter"));
	SetAutomationActionRecording(false);

	AddColumnObjectType();

	AddColumnForRowPK(GetColumnObjectType());

	if (m_WithJobsOrdering)
	{
		m_ColumnMoveUp		= AddColumnIcon(_YUID("UP"), YtriaTranslate::Do(ColumnSelectionGrid_customizeGrid_1, _YLOC("Move Up")).c_str(), GridBackendUtil::g_DummyString);
		m_ColumnMoveDown	= AddColumnIcon(_YUID("DOWN"), YtriaTranslate::Do(ColumnSelectionGrid_customizeGrid_2, _YLOC("Move Down")).c_str(), GridBackendUtil::g_DummyString);
		addImageList(IDB_GRID_UPDOWN);
	}

	ICON_SYSTEM = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_JOBCENTER_SYSTEM));
	ASSERT(GetImageCount() == ICON_SYSTEM + 1);

	ICON_DISPLAYED = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_JOBCENTER_DISPLAYED));
	ASSERT(GetImageCount() == ICON_DISPLAYED + 1);

	ICON_SHARED = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_JOBCENTER_SHARED));
	ASSERT(GetImageCount() == ICON_SHARED + 1);

	ICON_LIB = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_JOBCENTER_LIB));
	ASSERT(GetImageCount() == ICON_LIB + 1);

	if (g_ElementTypeScript.empty())
	{
		g_ElementTypeScript			= _T("Job");
		g_ElementTypeScriptPreset	= _T("Preset");
		g_ElementTypeScriptSchedule	= _T("Scheduled Task");
	}
	for (const auto& t : {
			make_tuple(g_ElementTypeIDScript,			IDB_ICON_JOBCENTER_SCRIPT,			g_ElementTypeScript),
			make_tuple(g_ElementTypeIDScriptPreset,		IDB_ICON_JOBCENTER_SCRIPTPRESET,	g_ElementTypeScriptPreset),
			make_tuple(g_ElementTypeIDScriptSchedule,	IDB_ICON_JOBCENTER_SCRIPTSCHEDULE,	g_ElementTypeScriptSchedule)
		})
	{
		AddObjectTypeIcon(std::get<0>(t), std::get<1>(t), std::get<2>(t));
	}

	UINT columnSize1 = HIDPI_XW(8);

	if (m_WithJobsOrdering)
	{
		m_ColumnPosition = AddColumn(_YUID("Position"), YtriaTranslate::Do(JobCenterGrid_customizeGrid_1, _YLOC("Position")).c_str(), GridBackendUtil::g_DummyString);
		SetColumnsVisible({ m_ColumnPosition }, false, false);
	}
	// RE-add this column when  jobcenter is synced to cosmos
	//m_ColumnShared		= AddColumnIcon(_YUID("Shared"),	_T("Shared"),																	GridBackendUtil::g_DummyString);
	if (m_WithJobsOrdering)
		m_ColumnDisplayed	= AddColumnIcon(_YUID("Displayed"),	YtriaTranslate::Do(JobCenterGrid_customizeGrid_2, _YLOC("Displayed")).c_str(),	GridBackendUtil::g_DummyString);
	m_ColumnSystem			= AddColumnIcon(_YUID("System"),	_T("System"),																	GridBackendUtil::g_DummyString);
	m_ColumnLib				= AddColumnIcon(_YUID("Lib"),		_T("Lib"),																		GridBackendUtil::g_DummyString);

	m_ColumnTitle			= AddColumn(_YUID("ScriptTitle"),	YtriaTranslate::Do(JobCenterGrid_customizeGrid_3, _YLOC("Title")).c_str(),			GridBackendUtil::g_DummyString, columnSize1*30);
	m_ColumnIcon			= AddColumn(_YUID("ScriptIcon"),	_T("Icon"),																			GridBackendUtil::g_DummyString);
	m_ColumnDesc			= AddColumn(_YUID("ScriptDesc"),	YtriaTranslate::Do(JobCenterGrid_customizeGrid_4, _YLOC("Description")).c_str(),	GridBackendUtil::g_DummyString, columnSize1*40);
	m_ColumnTooltip			= AddColumn(_YUID("ScriptTooltip"), YtriaTranslate::Do(JobCenterGrid_customizeGrid_5, _YLOC("Tooltip")).c_str(),		GridBackendUtil::g_DummyString, columnSize1*60);
	SetColumnsVisible({ m_ColumnTooltip }, false, false); // Never used so far. Will be displayed in loadScripts if one value is found
	m_ColumnKey				= AddColumn(_YUID("ScriptKey"),		YtriaTranslate::Do(JobCenterGrid_customizeGrid_6, _YLOC("Key")).c_str(),			GridBackendUtil::g_DummyString);
	m_ColumnSourceFilePath	= AddColumn(_YUID("SourceFilePath"), _T("Source File Path"),															GridBackendUtil::g_DummyString);

	m_ColumnDateLastUpdate			= AddColumnDate(_YUID("LastUpdate"), _T("Last Updated On"),													GridBackendUtil::g_DummyString, columnSize1*6);
	m_ColumnDateLastUpdateBy		= AddColumn(_YUID("LastUpdateBy"), _T("Last Updated By"),													GridBackendUtil::g_DummyString, columnSize1*8);
	m_ColumnDateLastUpdateByEmail	= AddColumn(_YUID("LastUpdateByEmail"), _T("Last Updated By (Email)"),										GridBackendUtil::g_DummyString);
	m_ColumnDateLastUpdateByID		= AddColumn(_YUID("LastUpdateByID"), _T("Last Updated By (Graph ID)"),										GridBackendUtil::g_DummyString);
	// RE-add this column when  jobcenter is synced to cosmos
	//m_ColumnDateLastShared			= AddColumnDate(_YUID("LastShare"), _T("Last Shared On"),													GridBackendUtil::g_DummyString);

	SetColumnsVisible({ m_ColumnDateLastUpdateByEmail, m_ColumnDateLastUpdateByID }, false, false);

	AddColumnForRowPK(m_ColumnKey);

	SetHierarchyColumnsParentPKs({ { m_ColumnKey, GridBackendUtil::g_AllHierarchyLevels}, { GetColumnObjectType(),								GridBackendUtil::g_AllHierarchyLevels} });
	AddHierarchyLowestChildrenObjectTypeId(g_ElementTypeIDScriptSchedule);

	m_ColumnTitle->SetAutoResizeWithGridResize(true);
	m_ColumnDesc->SetAutoResizeWithGridResize(true);
	m_ColumnTooltip->SetAutoResizeWithGridResize(true);
	m_ColumnKey->SetAutoResizeWithGridResize(true);
	m_ColumnSourceFilePath->SetAutoResizeWithGridResize(true);
	m_ColumnDateLastUpdate->SetAutoResizeWithGridResize(true);

	if (m_WithJobsOrdering)
		SetShowSortArrows(false);

	AddFilterObserver(this);
	DragAcceptFiles();
}

void JobCenterGrid::customizeGridPostProcess()
{
	SetSimpleView(m_SimpleView, false);
}

void JobCenterGrid::LoadScripts(AutomationWizardO365* p_JC)
{
	m_JobCenter = p_JC;
	FixPositions();
	loadScripts();
}

void JobCenterGrid::FixPositions()
{
	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter)
		m_JobCenter->FixPositions();
}

void JobCenterGrid::manageUpDownArrows()
{
	if (m_WithJobsOrdering)
	{
		// the simple view hides rows, must reset up/down
		const auto& rows = GetBackendRows();
		for (size_t k = 0; k < rows.size(); k++)
		{
			GridBackendRow* row		= rows.at(k);
			GridBackendRow* rowPrev = k > 0 ? rows.at(k - 1) : nullptr;
			GridBackendRow* rowNext = k < rows.size() - 1 ? rows.at(k + 1) : nullptr;
			if (nullptr != rowPrev && (!rowPrev->IsVisible() || rowPrev->GetField(m_ColumnMoveDown).GetIcon() == ICON_DOWNDISABLED))
				row->AddField(wstring(), m_ColumnMoveUp, ICON_UPDISABLED).SetIgnoreModifications(true);
			if (nullptr != rowNext && (!rowNext->IsVisible() || rowNext->GetField(m_ColumnMoveUp).GetIcon() == ICON_UPDISABLED))
				row->AddField(wstring(), m_ColumnMoveDown, ICON_DOWNDISABLED).SetIgnoreModifications(true);
		}
		// TODO call this upon filtering, hiding rows etc.
	}
}

void JobCenterGrid::loadScripts()
{
	StoreSelection();
	
	RemoveAllRows();

	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter)
	{
		const auto isMainFrame = m_JobCenter->GetName() == JobCenterUtil::g_WizardNameMainFrame;

		auto moduleScripts = m_JobCenter->GetScripts();

		m_HasTooltips = false; // Set in updateRow
		if (m_WithJobsOrdering)
		{
			for (size_t k = 0; k < moduleScripts.size(); ++k, isMainFrame)
				addRow(k, moduleScripts, isMainFrame);
		}
		else
		{
			// We want the latest first: as scripts are ordered by position and the oldest script has position 0 by default, insert them reversely
			for (size_t k = moduleScripts.size(); k > 0; --k)
				addRow(k - 1, moduleScripts, isMainFrame);
		}

		manageUpDownArrows();

		SetColumnsVisible({ m_ColumnTooltip }, !m_SimpleView && m_HasTooltips);

		// Bug #200421.SR.00BFAE: In job center grid (backstage), filters should only apply on top level rows (jobs).
		// Ensure hierarchy filter is only on scripts
		AddHierarchyObjectTypeToFilterOn(GetObjectTypeIndex(g_ElementTypeIDScript), false);
		RemoveHierarchyObjectTypeToFilterOn(GetObjectTypeIndex(g_ElementTypeIDScriptSchedule), false);

		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);

		RestoreSelection();
	}
}

void JobCenterGrid::addRow(const size_t p_ScriptIndex, const vector<Script>& p_Scripts, const bool p_IsMainFrame)
{
	ASSERT(p_ScriptIndex < p_Scripts.size());
	if (p_ScriptIndex < p_Scripts.size())
	{
		const auto& p_Script = p_Scripts.at(p_ScriptIndex);
		if (!p_IsMainFrame || !p_Script.IsCommon())// no common job in MainFrame (e.g. ResetGrid)
		{
			GridBackendRow* p_Row = AddRow();			
			ASSERT(nullptr != p_Row);
			if (nullptr != p_Row)
			{
				if (m_WithJobsOrdering)
				{
					ASSERT(p_ScriptIndex == p_Script.m_Position);// scripts are expected to be sorted by position at this point

					const bool isFirst	= 0 == p_ScriptIndex;
					const bool isLast	= p_Scripts.size() == p_ScriptIndex + 1;

					const bool canMoveUp	= !(isFirst || p_Script.IsPositionLocked() || p_Script.IsLib() || p_Scripts.at(p_ScriptIndex - 1).IsPositionLocked());
					const bool canMoveDown	= !(isLast || p_Script.IsPositionLocked() || p_Script.IsLib() || p_Scripts.at(p_ScriptIndex + 1).IsPositionLocked());// locked scripts are not supposed to be at the bottom, but

					p_Row->AddField(wstring(), m_ColumnMoveUp, canMoveUp ? ICON_UP : ICON_UPDISABLED);
					p_Row->AddField(wstring(), m_ColumnMoveDown, canMoveDown ? ICON_DOWN : ICON_DOWNDISABLED);

					if (nullptr != m_ColumnDisplayed)
						p_Row->AddField(p_Script.IsDisplayed() && p_Script.IsRunnable() ? _T("Displayed") : _YTEXT(""), m_ColumnDisplayed, p_Script.IsDisplayed() && p_Script.IsRunnable() ? ICON_DISPLAYED : NO_ICON);
					if (nullptr != m_ColumnPosition)
						p_Row->AddField(p_Script.m_Position, m_ColumnPosition);
				}

				if (nullptr != m_ColumnShared)
					p_Row->AddField(p_Script.IsShared() ? _T("Shared") : _YTEXT(""), m_ColumnShared, p_Script.IsShared() ? ICON_SHARED : NO_ICON);

				p_Row->AddField(p_Script.IsSystem() ? _T("System") : _YTEXT(""), m_ColumnSystem, p_Script.IsSystem() ? ICON_SYSTEM : NO_ICON);
				p_Row->AddField(p_Script.IsLib() ? _T("Lib") : _YTEXT(""), m_ColumnLib, p_Script.IsLib() ? ICON_LIB : NO_ICON);
				p_Row->SetTechHidden(m_SimpleView && p_Script.IsLib());

				p_Row->AddField(p_Script.m_Key, m_ColumnKey);
				p_Row->AddField(p_Script.m_Title, m_ColumnTitle);
				p_Row->AddField(p_Script.m_IconFontAwesome, m_ColumnIcon);
				if (!p_Script.m_Description.empty())
					p_Row->AddField(p_Script.m_Description, m_ColumnDesc);
				if (!p_Script.m_Tooltip.empty())
				{
					m_HasTooltips = true;
					p_Row->AddField(p_Script.m_Tooltip, m_ColumnTooltip);
				}

				if (!p_Script.m_SourceXMLfilePath.empty())
					p_Row->AddField(p_Script.m_SourceXMLfilePath, m_ColumnSourceFilePath);
				if (!p_Script.m_UpdDate.IsEmpty())
					p_Row->AddField(p_Script.m_UpdDate, m_ColumnDateLastUpdate);
				if (nullptr != m_ColumnDateLastShared && !p_Script.m_DateShared.IsEmpty())
					p_Row->AddField(p_Script.m_DateShared, m_ColumnDateLastShared);
				if (!p_Script.m_UpdateByName.empty())
					p_Row->AddField(p_Script.m_UpdateByName, m_ColumnDateLastUpdateBy);
				if (!p_Script.m_UpdateByPrincipalName.empty())
					p_Row->AddField(p_Script.m_UpdateByPrincipalName, m_ColumnDateLastUpdateByEmail);
				if (!p_Script.m_UpdateByID.empty())
					p_Row->AddField(p_Script.m_UpdateByID, m_ColumnDateLastUpdateByID);

				if (p_Script.IsSandbox())
					p_Row->SetColor(RGB(254, 163, 170));

				SetRowObjectType(p_Row, g_ElementTypeIDScript);

				if (m_SelectedJobIDs)
				{
					const auto& selectedIDs = *m_SelectedJobIDs;
					if (selectedIDs.find(p_Script.m_LibraryID) == selectedIDs.end())
						p_Row->BlendTextColor(GridBackendUtil::g_ColorDimmed, false);
					else
						p_Row->SetBold(true);
				}

				for (const auto& s : p_Script.m_Schedules_Vol)
				{
					auto scheduleRow = AddRow();
					ASSERT(nullptr != scheduleRow);
					if (nullptr != scheduleRow)
					{
						scheduleRow->SetParentRow(p_Row);
						SetRowObjectType(scheduleRow, g_ElementTypeIDScriptSchedule);

						scheduleRow->AddField(s.m_Key, m_ColumnKey);
						scheduleRow->AddField(s.m_Title, m_ColumnTitle);

						if (!s.m_Description.empty())
							scheduleRow->AddField(s.m_Description, m_ColumnDesc);
						scheduleRow->AddField(s.m_UpdDate, m_ColumnDateLastUpdate);
						scheduleRow->AddField(s.m_UpdateByName, m_ColumnDateLastUpdateBy);
						scheduleRow->AddField(s.m_UpdateByPrincipalName, m_ColumnDateLastUpdateByID);
						scheduleRow->AddField(s.m_UpdateByID, m_ColumnDateLastUpdateByID);
					}
				}

				for (const auto& p : p_Script.m_Presets_Vol)
				{
					auto presetRow = AddRow();
					ASSERT(nullptr != presetRow);
					if (nullptr != presetRow)
					{
						presetRow->SetParentRow(p_Row);
						SetRowObjectType(presetRow, g_ElementTypeIDScriptPreset);

						presetRow->AddField(p.m_Key, m_ColumnKey);
						presetRow->AddField(p.m_Title, m_ColumnTitle);

						if (!p.m_Description.empty())
							presetRow->AddField(p.m_Description, m_ColumnDesc);
						if (!p.m_SourceXMLfilePath.empty())
							p_Row->AddField(p_Script.m_SourceXMLfilePath, m_ColumnSourceFilePath);
						presetRow->AddField(p.m_UpdDate, m_ColumnDateLastUpdate);
						presetRow->AddField(p.m_UpdateByName, m_ColumnDateLastUpdateBy);
						presetRow->AddField(p.m_UpdateByPrincipalName, m_ColumnDateLastUpdateByID);
						presetRow->AddField(p.m_UpdateByID, m_ColumnDateLastUpdateByID);

						for (const auto& s : p.m_Schedules_Vol)
						{
							auto scheduleRow = AddRow();
							ASSERT(nullptr != scheduleRow);
							if (nullptr != scheduleRow)
							{
								scheduleRow->SetParentRow(presetRow);
								SetRowObjectType(scheduleRow, g_ElementTypeIDScriptSchedule);

								scheduleRow->AddField(s.m_Key, m_ColumnKey);
								scheduleRow->AddField(s.m_Title, m_ColumnTitle);

								if (!s.m_Description.empty())
									scheduleRow->AddField(s.m_Description, m_ColumnDesc);
								scheduleRow->AddField(s.m_UpdDate, m_ColumnDateLastUpdate);
								scheduleRow->AddField(s.m_UpdateByName, m_ColumnDateLastUpdateBy);
								scheduleRow->AddField(s.m_UpdateByPrincipalName, m_ColumnDateLastUpdateByID);
								scheduleRow->AddField(s.m_UpdateByID, m_ColumnDateLastUpdateByID);
							}
						}
					}
				}
			}
		}
	}
}

void JobCenterGrid::OnCustomClickUp()
{
	auto clickedRow = GetLastClickedRow();
	if (nullptr != clickedRow && m_WithJobsOrdering)
		manageScriptPosition(clickedRow, getClickedForData());
}

void JobCenterGrid::FilterChanged(const CacheGrid& grid)
{
	if (m_WithJobsOrdering)
		SetColumnsVisible({ m_ColumnMoveUp, m_ColumnMoveDown }, GetBackend().GetFilters().empty());
}

void JobCenterGrid::manageScriptPosition(GridBackendRow* p_RowToMove, GridBackendColumn* p_MoverColumn)
{
	ASSERT(m_WithJobsOrdering);
	ASSERT(nullptr != m_JobCenter);
	ASSERT(nullptr != p_RowToMove);
	ASSERT(nullptr != m_ColumnKey);
	ASSERT(nullptr != m_PanelJobCenter);

	if (nullptr != m_PanelJobCenter && p_RowToMove != nullptr && nullptr != m_ColumnKey && (p_MoverColumn == m_ColumnMoveDown || p_MoverColumn == m_ColumnMoveUp))
	{
		const bool moveUp	= p_MoverColumn == m_ColumnMoveUp;
		const bool moveDown	= !moveUp;
		const auto& f		= p_RowToMove->GetField(p_MoverColumn);
		if (moveUp && f.GetIcon() == ICON_UP || moveDown && f.GetIcon() == ICON_DOWN)
		{
			bool permutation = false;

			const auto& allRows = GetBackend().GetBackendRowsForDisplay();
			for (long k = 0; !permutation && k < (long) allRows.size(); k++)
			{
				GridBackendRow* row = allRows[k];
				ASSERT(row != nullptr);
				if (row != nullptr && p_RowToMove == row)
				{
					size_t permuteRowIndex = moveDown ? k + 1 : k - 1;
					ASSERT(permuteRowIndex >= 0 && permuteRowIndex < allRows.size());
					if (permuteRowIndex >= 0 && permuteRowIndex < allRows.size())
					{
						GridBackendRow* rowToPermuteWith = allRows[permuteRowIndex];
						ASSERT(rowToPermuteWith != nullptr);
						if (rowToPermuteWith != nullptr)
						{
							auto script1 = m_JobCenter->GetScript(row->GetField(m_ColumnKey).GetValueStr());
							auto script2 = m_JobCenter->GetScript(rowToPermuteWith->GetField(m_ColumnKey).GetValueStr());

							const auto p2 = script2.m_Position;

							script2.m_Position = script1.m_Position;
							script1.m_Position = p2;


							script1.m_LogUpdate_vol = false;
							script2.m_LogUpdate_vol = false;

							m_JobCenter->UpdateScript(script1);
							m_JobCenter->UpdateScript(script2);
							loadScripts();

							permutation = true;
						}
					}
				}
			}

			m_PanelJobCenter->SetConfigurationWasUpdated(permutation);
		}
	}
}

void JobCenterGrid::OnCustomDoubleClick()
{
	ASSERT(nullptr != m_PanelJobCenter);
	if (nullptr != m_PanelJobCenter)
		m_PanelJobCenter->OnGridDoubleClick();
}

void JobCenterGrid::SetSimpleView(bool p_SimpleView, bool p_TriggerUpdate/* = true*/)
{
	m_SimpleView = p_SimpleView;

	if (nullptr != m_ColumnLib) // Grid has been customized already
	{
		for (auto row : GetBackendRows())
			row->SetTechHidden(m_SimpleView && row->GetField(m_ColumnLib).GetIcon() != NO_ICON);
		manageUpDownArrows();

		SetColumnsVisible({ m_ColumnTooltip }, !m_SimpleView && m_HasTooltips, false);

		SetColumnsVisible(
			{
				m_ColumnLib,
				m_ColumnKey,
				m_ColumnIcon,
				m_ColumnSourceFilePath,
				m_ColumnDateLastUpdate,
				m_ColumnDateLastUpdateBy,
			}, !m_SimpleView, false);

		if (p_TriggerUpdate)
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS | GridBackendUtil::GRIDUPDATE_MAIN_GCVD_NOFILTERS);
	}
}

bool JobCenterGrid::IsSimpleView() const
{
	return m_SimpleView;
}

void JobCenterGrid::GetSelected(vector<Script>& p_Scripts, vector<ScriptPreset>& p_ScriptPresets, vector<ScriptSchedule>& p_ScriptSchedules)
{
	p_Scripts.clear();
	p_ScriptPresets.clear();

	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter)
	{
		vector<GridBackendRow*> rows;
		GetSelectedNonGroupRowsWithoutCompareRows(rows);
		for (auto r : rows)
		{
			ASSERT(nullptr != r);
			if (nullptr != r)
			{
				if (GetRowObjectType(r) == g_ElementTypeIDScript)
					p_Scripts.push_back(m_JobCenter->GetScript(r->GetField(m_ColumnKey).GetValueStr()));
				else if (GetRowObjectType(r) == g_ElementTypeIDScriptPreset)
					p_ScriptPresets.push_back(m_JobCenter->GetScriptPreset(r->GetField(m_ColumnKey).GetValueStr()));
				else if (GetRowObjectType(r) == g_ElementTypeIDScriptSchedule)
					p_ScriptSchedules.push_back(m_JobCenter->GetScriptSchedule(r->GetField(m_ColumnKey).GetValueStr()));
				else
					ASSERT(false);
			}
		}
	}
}

void JobCenterGrid::OnDropFiles(HDROP dropInfo)
{
	CString sFile;
	DWORD   nBuffer = 0;

	vector<wstring> scriptFilePaths;
	// Get the number of files dropped 
	int nFilesDropped = DragQueryFile(dropInfo, 0xFFFFFFFF, nullptr, 0);
	for (int i = 0; i < nFilesDropped; i++)
	{
		// Get the buffer size of the file. 
		nBuffer = DragQueryFile(dropInfo, i, nullptr, 0);

		// Get path and name of the file 
		DragQueryFile(dropInfo, i, sFile.GetBuffer(nBuffer + 1), nBuffer + 1);
			
		scriptFilePaths.push_back(sFile.GetBuffer());	

		sFile.ReleaseBuffer();
	}

	// Free the memory block containing the dropped-file information 
	DragFinish(dropInfo);

	ASSERT(nullptr != m_PanelJobCenter);
	if (nullptr != m_PanelJobCenter)
		m_PanelJobCenter->ImportScripts(scriptFilePaths);
}

void JobCenterGrid::SetJobSelectionIDs(const set<wstring>& p_SelectedJobIDs)
{
	m_SelectedJobIDs = p_SelectedJobIDs;
}

void JobCenterGrid::ResetJobSelectionIDs()
{
	m_SelectedJobIDs.reset();
}

void JobCenterGrid::InitializeCommands()
{
	CacheGrid::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID		= ID_JOBCENTERGRID_SELECTJOBS;
		_cmd.m_sMenuText	= _T("Select Jobs for your AJL license");
		_cmd.m_sToolbarText = _T("Select Jobs");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Select jobs."),
			_T("Make a selection in the job catalog and attach them to your license."),
			_cmd.m_sAccelText);
	}
}

void JobCenterGrid::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		if (Office365AdminApp::IsLicense<LicenseTag::AJL>())
			pPopup->ItemInsert(ID_JOBCENTERGRID_SELECTJOBS);
	}
}

void JobCenterGrid::OnUpdateSelectJobs(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
}

void JobCenterGrid::OnSelectAJLjobs()
{
	ASSERT(m_SelectedJobIDs);
	if (m_SelectedJobIDs)
	{
		CWaitCursor _;
		const auto& selectedIDs = *m_SelectedJobIDs;
		auto moduleScripts = m_JobCenter->GetScripts();
		vector<Script*> scriptPtrs;
		vector<Script*> selectedScriptPtrs;
		vector<wstring> scriptTitles;
		std::sort(moduleScripts.begin(), moduleScripts.end(), [](const Script& s1, const Script& s2) { return Str::toUpper(s1.m_Title).compare(Str::toUpper(s2.m_Title).c_str()) < 0; });
		for (auto& s : moduleScripts)
			if (s.IsSystem() && s.IsRunnable())// user jobs are irrelevant
			{
				// ASSERT(!s.m_LibraryID.empty());
				if (!s.m_LibraryID.empty())
				{
					scriptPtrs.push_back(&s);
					scriptTitles.push_back(s.m_Title);
					if (selectedIDs.find(s.m_LibraryID) != selectedIDs.end())
						selectedScriptPtrs.push_back(&s);
				}
				else
					TRACE(_YDUMPFORMAT(L"NO LIBRARY ID: %s (%s)\n", s.m_Title.c_str(), s.m_Key.c_str()).c_str());
			}

		const auto maxJobCount = []()
		{
			auto o365 = dynamic_cast<Office365AdminApp*>(AfxGetApp());
			ASSERT(nullptr != o365);
			return nullptr != o365 ? o365->GetAJLCapacity() : 0;
		}();

		DlgSelectGrid<Script> dlg(_T("AJL License Configuration"), _T("Available Jobs"), _YFORMAT(L"Select the jobs you want attached to your AJL license.\nYour selection is limited to %s.", Str::getStringFromNumber(maxJobCount).c_str()), scriptPtrs, selectedScriptPtrs, scriptTitles, this);
		dlg.SetOkEnabledIfNoSelection(false);
		dlg.SetMaxSelection(maxJobCount);
		if (dlg.DoModal() == IDOK)
		{
			auto newJobSelection = dlg.GetSelection();
			if (!newJobSelection.empty())
			{
				auto o365 = dynamic_cast<Office365AdminApp*>(AfxGetApp());
				ASSERT(nullptr != o365);
				if (nullptr != o365)
				{
					set<wstring> jobIDs;
					for (auto s : newJobSelection)
						if (nullptr != s && !s->m_LibraryID.empty())
							jobIDs.insert(s->m_LibraryID);
					if (o365->UpdateJobSelectionIDs(jobIDs, this))
					{
						SetJobSelectionIDs(jobIDs);
						loadScripts();
						auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
						ASSERT(nullptr != mainFrame);
						if (nullptr != mainFrame)
							mainFrame->HTMLUpdateAJL();
					}
				}
			}
		}
	}
}