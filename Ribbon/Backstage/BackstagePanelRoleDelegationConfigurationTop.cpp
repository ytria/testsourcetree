#include "BackstagePanelRoleDelegationConfigurationTop.h"

#include "BusinessDirectoryRole.h"
#include "DirectoryRoleMembersRequester.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "RoleDelegationManager.h"
#include "Sapio365Session.h"
#include "YtriaTranslate.h"

const COLORREF BackstagePanelRoleDelegationConfigurationTop::g_ButtonBackColor = RGB(240, 240, 240);

BEGIN_MESSAGE_MAP(BackstagePanelRoleDelegationConfigurationTop, BackstagePanelBase)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_SHOWROLES,		OnShowRoles)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_SHOWFILTERS,	OnShowFilters)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_SHOWKEYS,		OnShowKeys)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_RELOAD,			OnReload)
END_MESSAGE_MAP()

BackstagePanelRoleDelegationConfigurationTop::BackstagePanelRoleDelegationConfigurationTop()
	: BackstagePanelBase(IDD)
	, m_ReadyToRoll(false)
{
}

BackstagePanelRoleDelegationConfigurationTop::~BackstagePanelRoleDelegationConfigurationTop()
{
}

void BackstagePanelRoleDelegationConfigurationTop::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelBase::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_STATIC_ROLEDELEGATION_TOP,				m_Top);
	DDX_Control(pDX, IDD_BACKSTAGEPAGE_ROLEDELEGATION_CFG_TAB,	m_PseudoTab);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_SHOWROLES,		m_BtnShowDelegations);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_SHOWFILTERS,		m_BtnShowFilters);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_SHOWKEYS,		m_BtnShowKeys);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_RELOAD,			m_BtnReload);
}

BOOL BackstagePanelRoleDelegationConfigurationTop::OnInitDialog()
{
	BackstagePanelBase::OnInitDialog();
	setCaption(YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationTop_OnInitDialog_1, _YLOC("Role-Based Access Control - Configuration")).c_str());

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_SHOWROLES };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_ROLE_SHOWMAIN, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnShowDelegations, YtriaTranslate::Do(DlgEffectiveAccess_OnInitDialog_2, _YLOC("Roles")).c_str(), _YTEXT(""), m_ImageList);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_SHOWFILTERS };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_ROLE_SHOWFILTERS, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnShowFilters, YtriaTranslate::Do(DlgRoleSeeInfo_generateJSONScriptDataSpecific_4, _YLOC("Scopes")).c_str(), _YTEXT(""), m_ImageList);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_SHOWKEYS };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_ROLE_SHOWKEYS, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnShowKeys, YtriaTranslate::Do(DlgRoleEditKey_generateJSONScriptDataSpecific_6, _YLOC("Credentials")).c_str(), _YTEXT(""), m_ImageList);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_RELOAD };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_ROLE_RELOAD, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnReload, _YTEXT(""), _YTEXT(""), m_ImageList);
// //		BackstagePanelBase::SetupButton(m_BtnReload,  _YCOM("Refresh"), _YTEXT(""), m_ImageList);
	}

	auto panel1 = m_TabPanelRoles.Create(BackstageTabPanelRoleDelegationConfigurationDelegation::IDD, this);
	auto panel2 = m_TabPanelFilters.Create(BackstageTabPanelRoleDelegationConfigurationFilters::IDD, this);
	auto panel3 = m_TabPanelKeys.Create(BackstageTabPanelRoleDelegationConfigurationKeys::IDD, this);
	ASSERT(panel1);
	ASSERT(panel2);
	ASSERT(panel3);

	m_Panels[IDC_BACKSTAGE_BUTTON_ROLE_SHOWROLES]	= &m_TabPanelRoles;
	m_Panels[IDC_BACKSTAGE_BUTTON_ROLE_SHOWFILTERS] = &m_TabPanelFilters;
	m_Panels[IDC_BACKSTAGE_BUTTON_ROLE_SHOWKEYS]	= &m_TabPanelKeys;

	m_TabPanelRoles.SetOnModifyPostProcessFunction(std::bind(&BackstagePanelRoleDelegationConfigurationTop::enableButtons, this));
	m_TabPanelFilters.SetOnModifyPostProcessFunction(std::bind(&BackstagePanelRoleDelegationConfigurationTop::enableButtons, this));
	m_TabPanelKeys.SetOnModifyPostProcessFunction(std::bind(&BackstagePanelRoleDelegationConfigurationTop::enableButtons, this));

	CRect aRect;
	m_PseudoTab.GetWindowRect(&aRect);
	m_PseudoTab.ShowWindow(SW_HIDE);
	ScreenToClient(&aRect);

	SetResize(IDD_BACKSTAGEPAGE_ROLEDELEGATION_CFG_TAB, XTP_ATTR_RESIZE(1.f));
	SetResize(&m_TabPanelRoles,		XTP_ATTR_RESIZE(1.f), aRect);
	SetResize(&m_TabPanelFilters,	XTP_ATTR_RESIZE(1.f), aRect);
	SetResize(&m_TabPanelKeys,		XTP_ATTR_RESIZE(1.f), aRect);

	SetResize(IDC_BACKSTAGE_BUTTON_ROLE_SHOWROLES,		{ 0, 0 },		{ .33f, 0 });
	SetResize(IDC_BACKSTAGE_BUTTON_ROLE_SHOWFILTERS,	{ .33f, 0 },	{ .66f, 0 });
	SetResize(IDC_BACKSTAGE_BUTTON_ROLE_SHOWKEYS,		{ .66f, 0 },	{ 1, 0 });
	SetResize(IDC_BACKSTAGE_BUTTON_ROLE_RELOAD,			{ 1, 0 },		{ 1, 0 });
	SetResize(IDC_STATIC_ROLEDELEGATION_TOP,			{ 0, 0 },		{ 1, 0 });

	m_brushButtonBack.CreateSolidBrush(g_ButtonBackColor);

	return TRUE == panel1 && TRUE == panel2 && TRUE == panel3;
}

void BackstagePanelRoleDelegationConfigurationTop::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelBase::SetTheme(nTheme);
	m_Top.SetTheme(nTheme);
	m_TabPanelRoles.SetTheme(nTheme);
	m_TabPanelFilters.SetTheme(nTheme);
	m_TabPanelKeys.SetTheme(nTheme);

	//m_TabCtrl.SetTheme(nTheme);
	m_BtnShowDelegations.SetTheme(nTheme);
	m_BtnShowFilters.SetTheme(nTheme);
	m_BtnShowKeys.SetTheme(nTheme);
	m_BtnReload.SetTheme(nTheme);
}

void BackstagePanelRoleDelegationConfigurationTop::OnShowRoles()
{
	showPanel(IDC_BACKSTAGE_BUTTON_ROLE_SHOWROLES);
}

void BackstagePanelRoleDelegationConfigurationTop::OnShowFilters()
{
	showPanel(IDC_BACKSTAGE_BUTTON_ROLE_SHOWFILTERS);
}

void BackstagePanelRoleDelegationConfigurationTop::OnShowKeys()
{
	showPanel(IDC_BACKSTAGE_BUTTON_ROLE_SHOWKEYS);
}

void BackstagePanelRoleDelegationConfigurationTop::OnReload()
{
	CWaitCursor c;

	RoleDelegationManager::GetInstance().UpdateSQLiteFromCloud(getSession(), this);

	m_TabPanelRoles.OnReload();
	m_TabPanelFilters.OnReload();
	m_TabPanelKeys.OnReload();
}

HBRUSH BackstagePanelRoleDelegationConfigurationTop::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
	// Give 'tab' buttons a gray background.
	if (m_BtnShowDelegations.m_hWnd == pWnd->m_hWnd || 
		m_BtnShowFilters.m_hWnd == pWnd->m_hWnd || 
		m_BtnShowKeys.m_hWnd == pWnd->m_hWnd)
	{
		// Copy pasted from CXTPRibbonBackstagePage::OnCtlColor
		pDC->SetBkColor(g_ButtonBackColor);
		return (HBRUSH)m_brushButtonBack;
	}

	return BackstagePanelBase::OnCtlColor(pDC, pWnd, nCtlColor);
}

void BackstagePanelRoleDelegationConfigurationTop::showPanel(const int p_PanelIndex)
{
	for (const auto& p : m_Panels)
	{
		ASSERT(p.second != nullptr);
		if (p.second != nullptr)
		{
			const bool thisPanel = p.first == p_PanelIndex;
			p.second->ShowWindow(thisPanel ? SW_SHOW : SW_HIDE);

			auto panel = dynamic_cast<IActiveSetter*>(p.second);
			ASSERT(nullptr != panel);
			if (nullptr != panel)
			{
				if (thisPanel)
					panel->OnSetActive();
				else
					panel->OnKillActive();
			}

			auto btn = dynamic_cast<CXTPRibbonBackstageButton*>(GetDlgItem(p.first));
			ASSERT(nullptr != btn);
			if (nullptr != btn)
				btn->SetCheck(thisPanel ? BST_CHECKED : BST_UNCHECKED);
		}
	}

	//RedrawWindow();
}

void BackstagePanelRoleDelegationConfigurationTop::hideEverything()
{
	showPanel(666); // Giving an out-of-range index will hide all the panels without showing anyone.
	showButtons(false);
}

void BackstagePanelRoleDelegationConfigurationTop::showButtons(const bool p_Show)
{
	m_BtnShowDelegations.ShowWindow(p_Show ? SW_SHOW : SW_HIDE);
	m_BtnShowFilters.ShowWindow(p_Show ? SW_SHOW : SW_HIDE);
	m_BtnShowKeys.ShowWindow(p_Show ? SW_SHOW : SW_HIDE);
	m_BtnReload.ShowWindow(p_Show ? SW_SHOW : SW_HIDE);

	if (p_Show)
	{
		enableButtons();
		if (m_BtnShowDelegations.IsWindowEnabled())
			PostMessage(WM_COMMAND, MAKEWPARAM(IDC_BACKSTAGE_BUTTON_ROLE_SHOWROLES, BN_CLICKED), NULL); // Do not directly call OnShowRoles(), grid status bar would not update correctly.
	}
}

void BackstagePanelRoleDelegationConfigurationTop::enableButtons()
{
	m_BtnShowDelegations.EnableWindow();
	m_BtnShowFilters.EnableWindow();
	m_BtnShowKeys.EnableWindow();
	m_BtnReload.EnableWindow();
}

BOOL BackstagePanelRoleDelegationConfigurationTop::OnSetActive()
{
	hideEverything();
	if (BackstagePanelBase::OnSetActive() == TRUE)
	{
		wstring topText;
		CWaitCursor c;

		auto session = getSession();
		RoleDelegationManager::GetInstance().UpdateSQLiteFromCloud(session, this);

		m_ReadyToRoll = false;

		auto o365App = dynamic_cast<Office365AdminApp*>(AfxGetApp());
		ASSERT(nullptr != o365App);
		wstring licenseStatus;
		if (nullptr != o365App && !o365App->IsLicenseFullOrTrial(licenseStatus))
			topText = _YFORMAT(L"This feature is only available with a full or trial license. Your license status: %s", licenseStatus.c_str());
		else if (!Office365AdminApp::IsLicense<LicenseTag::RBAC>())
			topText = Office365AdminApp::GetLicenseWarnMessage<LicenseTag::RBAC>();
		else if (!hasSessionAccessToConfig())
			topText = YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationTop_OnSetActive_1, _YLOC("Only current Tenant's Global Administrator or sapio365 Administrators can configure Role-Based Access Control.")).c_str();
		else if (!RoleDelegationManager::GetInstance().IsReadyToRun())
			topText = RoleDelegationManager::GetInstance().GetLastError();
		else
		{
			m_ReadyToRoll = true;

			topText = session.get()->IsReadyForCosmos() ?
				YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationTop_OnSetActive_2, _YLOC("SHARED CONFIGURATION - Configuration shared in Cosmos DB - available to all users using the same License Code in your organization.")).c_str() :
				YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationTop_OnSetActive_3, _YLOC("LOCAL ONLY CONFIGURATION - No Cosmos DB account configured: Role-Based Access Control settings will remain local to this computer.")).c_str();

			showButtons(true);
		}

		m_Top.SetWindowText(topText.c_str());

		return TRUE;
	}

	return FALSE;
}

BOOL BackstagePanelRoleDelegationConfigurationTop::OnKillActive()
{
	if (BackstagePanelBase::OnKillActive() == TRUE)
	{
		if (m_ReadyToRoll)
		{
			m_TabPanelRoles.OnKillActive();
			m_TabPanelFilters.OnKillActive();
			m_TabPanelKeys.OnKillActive();
		}

		return TRUE;
	}

	return FALSE;
}

std::shared_ptr<Sapio365Session> BackstagePanelRoleDelegationConfigurationTop::getSession() const
{
	return MainFrameSapio365Session();
}

const PersistentSession&	BackstagePanelRoleDelegationConfigurationTop::getSessionInfo() const
{
	static const PersistentSession g_EmptySessionInfo;

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->GetMainWnd());
	ASSERT(nullptr != mainFrame);
    if (nullptr != mainFrame)
        return mainFrame->GetSessionInfo();

	return g_EmptySessionInfo;
}

bool BackstagePanelRoleDelegationConfigurationTop::hasSessionAccessToConfig() const
{
	return RoleDelegationManager::GetInstance().HasSessionAccessToConfig(getSession());
}