#pragma once

#include "BackstagePanelWithGrid.h"
#include "GridModuleAnnotations.h"
#include "..\..\Resource.h"
#include "Sapio365Session.h"

class BackstagePanelAnnotations : public BackstagePanelWithGrid<GridModuleAnnotations>/*, public GridSelectionObserver*/
{
public:
	BackstagePanelAnnotations();
	~BackstagePanelAnnotations() override;

	enum { IDD = IDD_BACKSTAGEPAGE_ANNOTATIONS };

	virtual BOOL OnInitDialog() override;

	void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

//DECLARE_MESSAGE_MAP();

	virtual void SetTheme(const XTPControlTheme nTheme) override;

private:
// 	virtual void						SelectionChanged(const CacheGrid& grid) override;
	void								fillgrid();
	std::shared_ptr<Sapio365Session>	getSession() const;
	
	void writeAnnotation(GridAnnotation& p_Annotation);

	//afx_msg void OnPause();
// 	afx_msg void OnEdit();
// 	afx_msg void OnDelete();

	//CXTPRibbonBackstageButton m_btnPause;
// 	CXTPRibbonBackstageButton m_BtnEdit;
// 	CXTPRibbonBackstageButton m_BtnDelete;
};