#pragma once

#include "BackstageTabPanelRoleDelegationConfigurationBase.h"
#include "RoleDelegationCfgDelegationGrid.h"
#include "..\..\Resource.h"

class LambdaCredentialAdder;
class Sapio365Session;

class BackstageTabPanelRoleDelegationConfigurationDelegation : public BackstageTabPanelRoleDelegationConfigurationBase<RoleDelegationCfgDelegationGrid>
{
public:
	BackstageTabPanelRoleDelegationConfigurationDelegation();
	~BackstageTabPanelRoleDelegationConfigurationDelegation() override;

	void DoDataExchange(CDataExchange* pDX) override;

	enum { IDD = IDD_BACKSTAGEPAGE_ROLEDELEGATION_CFG_DELEGATION };

protected:
	DECLARE_MESSAGE_MAP()

	virtual void OnInitDialogSpecific() override;

private:
	afx_msg void OnAddFilter();
	afx_msg void OnRemoveFilter();
	afx_msg void OnAddMember();
	afx_msg void OnRemoveMember();

	virtual void add() override;
	virtual void edit() override;
	virtual void remove() override;
	virtual void updateButtons(const vector<GridBackendRow*>& p_SelectedNonGroupRows) override;	
	virtual void setThemeSpecific(const XTPControlTheme nTheme) override;

	auto retrieveSkus();
	auto getAlreadyAssignedSkus() const;

	void customDoubleClick();
	void editFilterFromFocusedRow();

	std::shared_ptr<Sapio365Session>	getSession() const;
	const PersistentSession& getSessionInfo() const;

	bool writeDelegation(RoleDelegation& p_Delegation);

	CXTPRibbonBackstageButton m_BtnAddFilter;
	CXTPRibbonBackstageButton m_btnRemoveFilter;
	CXTPRibbonBackstageButton m_BtnAddMember;
	CXTPRibbonBackstageButton m_btnRemoveMember;

	CXTPImageManager m_Imagelist;

	std::shared_ptr<LambdaCredentialAdder> m_LambdaCredentialAdder;
};
