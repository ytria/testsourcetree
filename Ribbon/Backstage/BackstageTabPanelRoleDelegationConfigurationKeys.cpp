#include "BackstageTabPanelRoleDelegationConfigurationKeys.h"

#include "DlgRoleEditKey.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "RoleDelegationManager.h"
#include "Sapio365Session.h"
#include "YtriaTranslate.h"

BackstageTabPanelRoleDelegationConfigurationKeys::BackstageTabPanelRoleDelegationConfigurationKeys()
	: BackstageTabPanelRoleDelegationConfigurationBase<RoleDelegationCfgKeysGrid>()
{
	m_Grid = std::make_shared<RoleDelegationCfgKeysGrid>();
}

BackstageTabPanelRoleDelegationConfigurationKeys::~BackstageTabPanelRoleDelegationConfigurationKeys()
{
}

void BackstageTabPanelRoleDelegationConfigurationKeys::OnInitDialogSpecific()
{
}

void BackstageTabPanelRoleDelegationConfigurationKeys::add()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		DlgRoleEditKey dlg(getSession(), this);
		if (IDOK == dlg.DoModal())
		{
			auto key		= dlg.GetKey();
			key.m_Status	= RoleDelegationUtil::RoleStatus::STATUS_OK;
			RoleDelegationManager::GetInstance().Write(key, getSession());
			m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationKeys::edit()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		auto keyID = m_Grid->GetSelectedID();
		if (keyID > 0)
		{
			auto roleKey = RoleDelegationManager::GetInstance().GetKey(keyID, getSession());
			ASSERT(roleKey.IsValid());
			if (roleKey.IsValid())
			{
				DlgRoleEditKey dlg(getSession(), this);
				dlg.SetKey(roleKey);
				if (IDOK == dlg.DoModal())
				{
					auto key = dlg.GetKey();
					RoleDelegationManager::GetInstance().Write(key, getSession());
					m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
				}
			}
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationKeys::remove()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		auto selectedKeyIDs = m_Grid->GetSelectedIDs();
		if (!selectedKeyIDs.empty())
		{
			YCodeJockMessageBox dlg(this,
									DlgMessageBox::eIcon_Question,
									YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationKeys_remove_5, _YLOC("%1 Key(s) selected for removal\nAre you sure?"), Str::getStringFromNumber<size_t>(selectedKeyIDs.size()).c_str()),
									_YTEXT(""),
									_YTEXT(""),
									{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
			if (IDOK == dlg.DoModal())
			{
				bool removed = false;
				map<wstring, set<wstring>> delegationsByUndeadKey;
				for (const auto& keyID : selectedKeyIDs)
				{
					auto key = RoleDelegationManager::GetInstance().GetKey(keyID, getSession());
					ASSERT(key.IsValid());
					if (key.IsValid())
					{
						auto delegations = RoleDelegationManager::GetInstance().ReadDelegationsFeaturingThisKey(keyID, getSession());
						if (!delegations.empty())
						{
							for (const auto& d : delegations)
								delegationsByUndeadKey[key.m_Name].insert(d.m_Delegation.m_Name);
						}
						else
						{
							key.m_Status = RoleDelegationUtil::STATUS_REMOVED;
							RoleDelegationManager::GetInstance().Write(key, getSession());
							removed = true;
						}
					}
				}

				if (removed)
					m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));

				vector<wstring> errorList;
				for (const auto& p : delegationsByUndeadKey)
					errorList.push_back(YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationKeys_remove_6, _YLOC("Key %1 was not removed because it is used in delegations: %2"), p.first.c_str(), Str::implode(p.second, _YTEXT(", ")).c_str()));
				if (!errorList.empty())
				{
					YCodeJockMessageBox dlg(this,
											DlgMessageBox::eIcon_ExclamationWarning,
											YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationFilters_remove_3, _YLOC("Unable to remove:")).c_str(),
											Str::implode(errorList, _YTEXT("\n")),
											_YTEXT(""),
											{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
					dlg.DoModal();
				}
			}
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationKeys::updateButtons(const vector<GridBackendRow*>& p_SelectedNonGroupRows)
{
	m_BtnAdd.EnableWindow();
	m_BtnEdit.EnableWindow(p_SelectedNonGroupRows.size() == 1 ? TRUE : FALSE);
	m_BtnRemove.EnableWindow(p_SelectedNonGroupRows.empty() ? FALSE : TRUE);
}

std::shared_ptr<const Sapio365Session> BackstageTabPanelRoleDelegationConfigurationKeys::getSession() const
{
	return MainFrameSapio365Session();
}
