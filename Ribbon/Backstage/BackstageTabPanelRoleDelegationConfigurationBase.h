#pragma once

#include "CacheGrid.h"

class Sapio365Session;

class IActiveSetter
{
public:
	virtual ~IActiveSetter() = default;
	virtual void OnSetActive() = 0;
	virtual void OnKillActive() = 0;
};

template<class RoleCacheGridClass>
class BackstageTabPanelRoleDelegationConfigurationBase : public CExtResizableDialog, public GridSelectionObserver, public IActiveSetter
{
public:
	using OnModifyPostProcessFunction = std::function<void()>;

	BackstageTabPanelRoleDelegationConfigurationBase();

	void DoDataExchange(CDataExchange* pDX) override;
	virtual void OnSetActive() override;
	virtual void OnKillActive() override;
	void OnReload();

	virtual void SetTheme(const XTPControlTheme nTheme);

	void SetOnModifyPostProcessFunction(OnModifyPostProcessFunction p_OnModifyPostProcessFunction);
	bool IsSelectAllDecryptionFailure() const;

protected:
	DECLARE_MESSAGE_MAP()

	virtual BOOL OnInitDialog() override;
	virtual void OnInitDialogSpecific() = 0;

	std::shared_ptr<RoleCacheGridClass> m_Grid;

	CXTPRibbonBackstageButton m_BtnAdd;
	CXTPRibbonBackstageButton m_BtnEdit;
	CXTPRibbonBackstageButton m_BtnRemove;

	std::shared_ptr<const Sapio365Session> getSession() const;

	afx_msg void OnEdit();

private:
	afx_msg void OnAdd();
	afx_msg void OnRemove();
	afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor); // Copy pasted from CXTPRibbonBackstagePage::SetTheme for color handling

	virtual void SelectionChanged(const CacheGrid& grid) override;

	virtual void add() = 0;
	virtual void edit() = 0;
	virtual void remove() = 0;
	virtual void updateButtons(const vector<GridBackendRow*>& p_SelectedNonGroupRows) = 0;
	virtual void setThemeSpecific(const XTPControlTheme nTheme);

	void setInfo(const wstring& p_Info);

	OnModifyPostProcessFunction m_OnModifyPostProcessFunction;

	CXTPImageManager m_Imagelist;

	CXTPRibbonBackstageLabel	m_staticGridArea;
	CXTPMarkupStatic			m_Info;

private:/* Copy pasted from CXTPRibbonBackstagePage for color handling */
	CXTPBrush m_xtpBrushBack;
	XTP_SUBSTITUTE_GDI_MEMBER_WITH_CACHED(CBrush, m_brBack, m_xtpBrushBack, GetBackBrushHandle);
	COLORREF m_clrBack;
	COLORREF m_clrText;
};

#include "BackstageTabPanelRoleDelegationConfigurationBase.hpp"
