#include "BackstagePanelAdmin.h"

#include "DlgAddItemsToGroups.h"
#include "MainFrameSapio365Session.h"
#include "RoleDelegationManager.h"
#include "Sapio365Session.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTranslate.h"

BEGIN_MESSAGE_MAP(BackstagePanelAdmin, BackstagePanelWithGrid<RoleDelegationCfgAdminGrid>)
	ON_WM_CREATE()
	ON_WM_CTLCOLOR() // Copy pasted from CXTPRibbonBackstagePage for color handling
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLESAPIO_ADD, onAdd)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLESAPIO_REMOVE, onRemove)
END_MESSAGE_MAP()

BackstagePanelAdmin::BackstagePanelAdmin() : BackstagePanelWithGrid<RoleDelegationCfgAdminGrid>(IDD)
{
	m_Grid = std::make_shared<RoleDelegationCfgAdminGrid>();
}

BackstagePanelAdmin::~BackstagePanelAdmin()
{
}

BOOL BackstagePanelAdmin::OnInitDialog()
{
	BackstagePanelWithGrid<RoleDelegationCfgAdminGrid>::OnInitDialog();

	setCaption(_T("sapio365 Access & Restrictions"));

	ASSERT(m_Grid);
	if (m_Grid)
		m_Grid->AddSelectionObserver(this);

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLESAPIO_ADD };
		m_ImageManager.SetIcons(IDC_BACKSTAGE_BTN_ROLE_ADD_MEMBER, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnAdd, _T("Assign Users"), _YTEXT(""), m_ImageManager);
		m_BtnAdd.EnableWindow(FALSE);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLESAPIO_REMOVE };
		m_ImageManager.SetIcons(IDC_BACKSTAGE_BTN_ROLE_REMOVE_MEMBER, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnRemove, YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_OnInitDialogSpecific_4, _YLOC("Un-assign")).c_str(), _YTEXT(""), m_ImageManager);
		m_BtnAdd.EnableWindow(FALSE);
	}

	SetResize(IDC_BACKSTAGE_BUTTON_ROLESAPIO_ADD, { 0, 0 }, { .50f, 0 });
	SetResize(IDC_BACKSTAGE_BUTTON_ROLESAPIO_REMOVE, { 0.50f, 0 }, { 1, 0 });

	m_Grid->UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);// create headers

	return TRUE;
}

void BackstagePanelAdmin::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelWithGrid<RoleDelegationCfgAdminGrid>::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_STATIC_ADMIN_TOP, m_Top);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLESAPIO_ADD, m_BtnAdd);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLESAPIO_REMOVE, m_BtnRemove);
}

void BackstagePanelAdmin::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelWithGrid<RoleDelegationCfgAdminGrid>::SetTheme(nTheme);

	m_BtnAdd.SetTheme(nTheme);
	m_BtnRemove.SetTheme(nTheme);
}

void BackstagePanelAdmin::showComponents(const bool p_Show)
{
	m_Grid->ShowWindow(p_Show ? TRUE : FALSE);
	m_BtnAdd.ShowWindow(p_Show ? TRUE : FALSE);
	m_BtnRemove.ShowWindow(p_Show ? TRUE : FALSE);
}

BOOL BackstagePanelAdmin::OnSetActive()
{
	m_Grid->SetIsActive(false);
	showComponents(false);
	if (BackstagePanelWithGrid<RoleDelegationCfgAdminGrid>::OnSetActive() == TRUE)
	{
		wstring topText;
		CWaitCursor c;

		m_ReadyToRoll = false;

		auto session = getSession();
		RoleDelegationManager::GetInstance().UpdateSQLiteFromCloud(session, this);
		if (Sapio365Session::IsAdminManager(session))
		{
			m_ReadyToRoll = true;

			topText = session.get()->IsReadyForCosmos() ?
				YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationTop_OnSetActive_2, _YLOC("SHARED CONFIGURATION - Configuration shared in Cosmos DB - available to all users using the same License Code in your organization.")).c_str() :
				YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationTop_OnSetActive_3, _YLOC("LOCAL ONLY CONFIGURATION - No Cosmos DB account configured: Role-Based Access Control settings will remain local to this computer.")).c_str();

			showComponents(true);
			m_Grid->SetIsActive(true);
			m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
		}
		else
			topText = _T("Only current Tenant's Global Administrator or sapio365 Administrators can configure sapio365 Access & Restrictions.");

		m_Top.SetWindowText(topText.c_str());

		return TRUE;
	}

	return FALSE;
}

BOOL BackstagePanelAdmin::OnKillActive()
{
	if (BackstagePanelWithGrid<RoleDelegationCfgAdminGrid>::OnKillActive() == TRUE)
	{
		m_Grid->SetIsActive(false);
		return TRUE;
	}

	return FALSE;
}

void BackstagePanelAdmin::onAdd()
{
	auto session = getSession();
	ASSERT(session);
	ASSERT(m_Grid);
	if (m_Grid && session)
	{
		const auto& scopes = RoleDelegationUtil::GetAdminScopes();
		map<PooledString, pair<PooledString, set<PooledString>>>		parentsForSelection;
		set<RoleDelegationUtil::RBAC_AdminScope>						scopeIDs;
		set<wstring>													scopeNames;
		map<RoleDelegationUtil::RBAC_AdminScope, map<wstring, wstring>>	selection;

		m_Grid->GetSelection(selection);
		for (const auto& p : selection)
		{
			scopeIDs.insert(p.first);
			scopeNames.insert(scopes.at(p.first).first);
			for (const auto& pUsers : p.second)
				parentsForSelection[pUsers.first] = make_pair(pUsers.second, set<PooledString>());
		}

		DlgAddItemsToGroups dlg(session,
			parentsForSelection,
			DlgAddItemsToGroups::SELECT_USERS_FOR_ROLEDELEGATION,
			{},
			{},
			{},
			_YFORMAT(L"Assign users to sapio365 admin role(s): %s", Str::implode(scopeNames, _YTEXT(", ")).c_str()),
			YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_OnAddMember_2, _YLOC("Selected users from the list below will be assigned to the selected roles.")).c_str(),
			RoleDelegationUtil::RBAC_NONE,
			this);

		if (IDOK == dlg.DoModal())
		{
			CWaitCursor c;
			auto& newMembers = dlg.GetSelectedItems();
			bool updateGrid = false;

			for (const auto nm : newMembers)
				for (const auto scopeID : scopeIDs)
					if (!RoleDelegationManager::GetInstance().IsSapio365AdminScopeSetForUser(nm.GetID(), scopeID, session))
					{
						RoleDelegationUtil::BusinessSapio365Admin a;
						a.m_Status = RoleDelegationUtil::RoleStatus::STATUS_OK;
						a.m_AdminScope = scopeID;
						a.m_UserID = nm.GetID();
						a.m_UserName = nm.GetDisplayName();
						a.m_UserPrincipalName = nm.GetPrincipalName();

						RoleDelegationManager::GetInstance().Write(a, getSession());

						updateGrid = true;
					}

			if (updateGrid)
				m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
		}
	}
}

void BackstagePanelAdmin::onRemove()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		auto selectedIDs = m_Grid->GetSelectedIDs();

		if (!selectedIDs.empty())
		{
			YCodeJockMessageBox dlg(this,
				DlgMessageBox::eIcon_Question,
				_YFORMAT(L"%s admin role(s) selected for removal\nAre you sure?", Str::getStringFromNumber<size_t>(selectedIDs.size()).c_str()).c_str(),
				_YTEXT(""),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });

			if (IDOK == dlg.DoModal())
			{
				bool removed = false;

				map<wstring, set<wstring>> delegationsByUndeadKey;
				for (const auto& aID : selectedIDs)
				{
					RoleDelegationUtil::BusinessSapio365Admin a;
					a.m_ID = aID;
					a.m_Status = RoleDelegationUtil::RoleStatus::STATUS_REMOVED;
					RoleDelegationManager::GetInstance().Write(a, getSession());
					removed = true;
				}

				if (removed)
				{
					m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
					SelectionChanged(*m_Grid);
				}
			}
		}
	}
}

void BackstagePanelAdmin::SelectionChanged(const CacheGrid& grid)
{
	vector<GridBackendRow*> selectedRows;
	grid.GetSelectedNonGroupRows(selectedRows);
	updateButtons(selectedRows);
}

void BackstagePanelAdmin::updateButtons(const vector<GridBackendRow*>& p_SelectedNonGroupRows)
{
	m_BtnAdd.EnableWindow(p_SelectedNonGroupRows.empty() ? FALSE : TRUE);
	ASSERT(m_Grid);
	bool hasUser = false;
	if (m_Grid)
		for (auto r : p_SelectedNonGroupRows)
			if (hasUser = (m_Grid->IsHierarchyLowestChildType(r) || (nullptr != r && r->HasChildrenRows())))
				break;
	m_BtnRemove.EnableWindow(hasUser ? TRUE : FALSE);
}

std::shared_ptr<Sapio365Session> BackstagePanelAdmin::getSession() const
{
	return MainFrameSapio365Session();
}