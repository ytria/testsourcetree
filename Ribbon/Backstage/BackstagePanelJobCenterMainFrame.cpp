#include "BackstagePanelJobCenterMainFrame.h"

#include "AutomatedApp.h"
#include "AutomatedMainFrame.h"
#include "AutomationPresetMaker.h"
#include "BackstagePanelBase.h"
#include "CodeJockThemed.h"
#include "CodeJockFrameBase.h"
#include "DlgJobPresetEdit.h"
#include "DlgLicenseMessageBoxHTML.h"
#include "DlgScheduleConfigHTML.h"
#include "FileUtil.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "SharedResource.h"
#include "WindowsScheduledTask.h"
#include "YCodeJockMessageBox.h"
#include "YFileDialog.h"

BEGIN_MESSAGE_MAP(BackstagePanelJobCenterMainFrame, BackstagePanelJobCenterBase)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETADD,			onPresetAdd)
//	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETIMPORT,		onPresetImport)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_RUN,				onRun)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_SCHEDULE,			onSchedule)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_SCHEDULE_REMOVE,	onScheduleRemove)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_TOGGLEVIEW,		onToggleView)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_SUGGEST,			onSuggest)
END_MESSAGE_MAP()

BackstagePanelJobCenterMainFrame::BackstagePanelJobCenterMainFrame(CodeJockFrameBase* p_Frame)
	: BackstagePanelJobCenterBase(p_Frame, IDD, false)
{
}

BackstagePanelJobCenterMainFrame::~BackstagePanelJobCenterMainFrame()
{
}

void BackstagePanelJobCenterMainFrame::setThemeSpecific(const XTPControlTheme nTheme)
{
	m_BtnPresetAdd.SetTheme(nTheme);
//	m_BtnPresetImport.SetTheme(nTheme);
	m_BtnRun.SetTheme(nTheme);
	m_BtnSchedule.SetTheme(nTheme);
	m_BtnScheduleRemove.SetTheme(nTheme);
	m_BtnToggleView.SetTheme(nTheme);
	m_BtnSuggest.SetTheme(nTheme);
}

void BackstagePanelJobCenterMainFrame::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelJobCenterBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETADD,			m_BtnPresetAdd);
//	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETIMPORT,		m_BtnPresetImport);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_RUN,				m_BtnRun);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_SCHEDULE,			m_BtnSchedule);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_SCHEDULE_REMOVE,	m_BtnScheduleRemove);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_TOGGLEVIEW,			m_BtnToggleView);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_SUGGEST,			m_BtnSuggest);
}

BOOL BackstagePanelJobCenterMainFrame::OnInitDialogSpecific()
{
	//{
	//	UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETIMPORT };
	//	m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_PRESETIMPORT, ids, 1, CSize(0, 0), xtpImageNormal);
	//	BackstagePanelBase::SetupButton(m_BtnPresetImport, _T("Import Saved Settings"), _T("Import the saved input settings from an XML file for the selected job."), m_ImageList);
	//	m_BtnPresetImport.SetTooltip(_T("Import the saved input settings from an XML file for the selected job."));
	//}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_RUN };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_RUN, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnRun, _YTEXT("RUN"), _YTEXT(""), m_ImageList);
		m_BtnRun.SetTooltip(_T("Run the Job\nExecute the selected job (along with its associated 'preset', if selected)."));
	}

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETADD };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_PRESETADD, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnPresetAdd, _YTEXT("NEW PRESET"), _YTEXT(""), m_ImageList);
		m_BtnPresetAdd.SetTooltip(_T("Create Job Preset\nSave a pre-defined set of user inputs, as well as email notification and session settings, for re-use with the selected job."));
		if (m_Grid.IsSimpleView())
			m_BtnPresetAdd.ShowWindow(SW_HIDE);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_SCHEDULE };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_SCHEDULE, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnSchedule, _YTEXT("SCHEDULE"), _YTEXT(""), m_ImageList);
		m_BtnSchedule.SetTooltip(_T("Add Job to Windows Scheduler\nSimply fill in the scheduler properties, and sapio365 will take the selected job\nand create a scheduled task in Windows Task Scheduler."));
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_SCHEDULE_REMOVE };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_SCHEDULE_REMOVE, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnScheduleRemove, _YTEXT("UNSCHEDULE"), _YTEXT(""), m_ImageList);
		m_BtnScheduleRemove.SetTooltip(_T("Delete Job from Windows Scheduler\nsapio365 will delete the selected job's scheduled task from Windows Task Scheduler."));
	}

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_SUGGEST };
		m_ImageList.SetIcons(IDB_BACKSTAGE_BUTTON_JOBCENTER_SUGGEST, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnSuggest, _YTEXT("SUGGEST"), _YTEXT(""), m_ImageList);
		m_BtnSuggest.SetTooltip(_T("Suggest a new 'job'"));

		AddAnchor(IDC_BACKSTAGE_BUTTON_JOBCENTER_SUGGEST, CSize(100, 0), CSize(100, 0));
	}

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_TOGGLEVIEW };
		m_ImageList.SetIcons(IDB_BACKSTAGE_BUTTON_JOBCENTER_TOGGLEVIEW, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnToggleView, _YTEXT("ADVANCED"), _YTEXT(""), m_ImageList);
		m_BtnToggleView.SetTooltip(_T("Toggle between simple and avdanced view."));

		m_BtnToggleView.SetCheck(!m_Grid.IsSimpleView() ? BST_CHECKED : BST_UNCHECKED);

		AddAnchor(IDC_BACKSTAGE_BUTTON_JOBCENTER_TOGGLEVIEW, CSize(100, 0), CSize(100, 0));
	}

	return TRUE;
}

void BackstagePanelJobCenterMainFrame::OnSetActive()
{
	SetConfigurationWasUpdated(false);
	isDebugMode();

	auto mainFrame = dynamic_cast<MainFrame*>(m_Frame);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
	{
		m_JobCenter = mainFrame->GetAutomationWizard();
		m_Grid.ResetJobSelectionIDs();

		auto o365 = dynamic_cast<Office365AdminApp*>(AfxGetApp());
		ASSERT(nullptr != o365);
		if (nullptr != o365 && Office365AdminApp::IsLicense<LicenseTag::AJL>())
			m_Grid.SetJobSelectionIDs(o365->GetAJLJobIDs());

		m_Grid.LoadScripts(m_JobCenter);
	}
	else
	{
		m_JobCenter = nullptr;
		ASSERT(false);
	}

	updateButtons();
}

void BackstagePanelJobCenterMainFrame::updateButtons()
{
	m_BtnImport.EnableWindow(FALSE);
	m_BtnExport.EnableWindow(FALSE);
	m_BtnUpdate.EnableWindow(FALSE);
	m_BtnRemove.EnableWindow(FALSE);
	m_BtnEdit.EnableWindow(FALSE);
	m_BtnPresetAdd.EnableWindow(FALSE);
//	m_BtnPresetImport.EnableWindow(FALSE);
	m_BtnRun.EnableWindow(FALSE);
	m_BtnScheduleRemove.EnableWindow(FALSE);
	m_BtnSchedule.EnableWindow(FALSE);
	m_BtnToggleView.EnableWindow(TRUE);
	m_BtnSuggest.EnableWindow(TRUE);

	if (!isDebugMode() && getSession())
	{
		vector<Script>			scripts;
		vector<ScriptPreset>	scriptPresets;
		vector<ScriptSchedule>	scriptSchedules;
		m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);

		bool uUpdate = false;
		bool uRemove = !scriptPresets.empty();
		for (const auto& s : scripts)
		{
			if (!s.IsSystem())
				uRemove = true;
			if (!s.IsSystem() && FileUtil::FileExists(s.m_SourceXMLfilePath))
				uUpdate = true;
			if (uUpdate && uRemove)
				break;
		}

		for (const auto& sp : scriptPresets)
		{
			if (uUpdate && uRemove)
				break;
			if (!uUpdate && !sp.m_SourceXMLfilePath.empty() && FileUtil::FileExists(sp.m_SourceXMLfilePath))
				uUpdate = true;
		}
		
		const bool runnableScript = scripts.size() == 1 && scripts.front().IsRunnable();
		const bool runnablePreset = [&scripts, &scriptPresets, this]()
		{
			if (scriptPresets.size() == 1 && scripts.empty())
			{
				auto script = m_JobCenter->GetScript(scriptPresets.front().m_ScriptID);
				if (script.IsRunnable())
					return true;
			}

			return false;
		}();

		m_BtnImport.EnableWindow(TRUE);
		m_BtnExport.EnableWindow(!scripts.empty() || !scriptPresets.empty() ? TRUE : FALSE);
		m_BtnUpdate.EnableWindow(uUpdate ? TRUE : FALSE);
		m_BtnRemove.EnableWindow(uRemove ? TRUE : FALSE);
		m_BtnEdit.EnableWindow(uRemove && ((scripts.size() == 1 && scriptPresets.empty()) || (scriptPresets.size() == 1 && scripts.empty())) ? TRUE : FALSE);
		m_BtnPresetAdd.EnableWindow(runnableScript ? TRUE : FALSE);
//		m_BtnPresetImport.EnableWindow(scripts.size() == 1 ? TRUE : FALSE);
		m_BtnRun.EnableWindow((runnableScript && scriptPresets.empty()) || runnablePreset ? TRUE : FALSE);
		const bool canSchedule = scriptSchedules.empty() && (runnableScript || runnablePreset);
		m_BtnSchedule.EnableWindow(canSchedule ? TRUE : FALSE);
		m_BtnScheduleRemove.EnableWindow(!scriptSchedules.empty() ? TRUE : FALSE);
	}
}

void BackstagePanelJobCenterMainFrame::onSchedule()
{
	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter)
	{
		boost::YOpt<Script>			selectedScript;
		boost::YOpt<ScriptPreset>	selectedPreset;
		{
			vector<Script>			scripts;
			vector<ScriptPreset>	scriptPresets;
			vector<ScriptSchedule>	scriptSchedules;
			m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);

			if (scripts.size() == 1 && scriptPresets.empty() && scriptSchedules.empty())
				selectedScript = scripts.back();
			else if (scriptPresets.size() == 1 && scripts.empty() && scriptSchedules.empty())
				selectedPreset = scriptPresets.front();
		}

		ASSERT(selectedScript && !selectedPreset || selectedPreset && !selectedScript);
		if (selectedScript && !selectedPreset || selectedPreset && !selectedScript)
		{
			if (!selectedScript)
				selectedScript = m_JobCenter->GetScript(selectedPreset.get().m_ScriptID);

			ASSERT(selectedScript->IsRunnable());
			if (selectedScript->IsRunnable())
			{
				auto o365 = dynamic_cast<Office365AdminApp*>(AfxGetApp());
				ASSERT(nullptr != o365);
				if (nullptr != o365)
				{
					if (Office365AdminApp::IsLicense<LicenseTag::AJL>() && !o365->IsJobAllowedByLicense(selectedScript->m_LibraryID))
					{
						DlgLicenseMessageBoxHTML dlg(nullptr,
							DlgMessageBox::eIcon_ExclamationWarning,
							_T("This job is not part of your AJL license."),
							_YTEXT(""),
							_YTEXT(""),
							{ { IDOK, YtriaTranslate::Do(LicenseManager_isVendorInitOK_1, _YLOC("OK")).c_str() } },
							{ 0, _YTEXT("") },
							o365->GetApplicationColor());
						dlg.DoModal();
					}
					else
					{
						wstring taskName = _YTEXTFORMAT(L"sapio365_%s_%s", selectedScript.get().m_Key.c_str(), YTimeDate::GetCurrentTimeDate().GetCOleDateTime().Format(_YTEXT("%Y%m%d%H%M%S")));
						wstring jobtitle = selectedScript.get().m_Title;

						DlgScheduleConfigHTML dlg(taskName, jobtitle, this);
						if (IDOK == dlg.DoModal())
						{
							// if script selected, create preset on the fly
							if (!selectedPreset)
							{
								auto&	script		= selectedScript.get();
								auto	newPreset	= addPreset(script);
								if (script.m_PresetRequired_vol)
								{
									if (newPreset.HasID())
										selectedPreset = newPreset;
									else
										return;// a preset was needed but there was a creation error or cancellation - error already displayed in addPreset
								}
							}

							const auto& taskSchedule = dlg.GetTaskSchedule();
							ASSERT(taskSchedule);
							if (taskSchedule)
							{
								taskName = dlg.GetDisplayName();
								const wstring commandLine = selectedPreset
									? _YTEXTFORMAT(L"%s -j \"%s\" -pj \"%s\"", (Product::getInstance().getModulePath() + Product::getInstance().getModuleName()).c_str(), selectedScript.get().m_Key.c_str(), selectedPreset.get().m_Key.c_str())
									: _YTEXTFORMAT(L"%s -j \"%s\"", (Product::getInstance().getModulePath() + Product::getInstance().getModuleName()).c_str(), selectedScript.get().m_Key.c_str());

								const auto error = WindowsScheduledTask::Create(taskName, commandLine, *taskSchedule);
								if (error.empty())
								{
									//auto taskInfo = WindowsScheduledTask::Get(taskName);

									ScriptSchedule sps;
									sps.m_Title = dlg.GetDisplayName();
									sps.m_Key = taskName;
									sps.m_Description = taskSchedule->GetDescription();
									if (selectedPreset)
										m_JobCenter->AddScriptSchedule(selectedScript.get(), selectedPreset.get(), sps);
									else
										m_JobCenter->AddScriptSchedule(*selectedScript, sps);

									m_Grid.LoadScripts(m_JobCenter);
									SetConfigurationWasUpdated(true);

									//YCodeJockMessageBox dlg(this,
									//	DlgMessageBox::eIcon_Information,
									//	_YTEXT("Success"),
									//	_YTEXTFORMAT(L"Successfully create task \"%s\"", taskName.c_str()),
									//	_YTEXT(""),
									//	{ { IDOK, _YTEXT("OK") }, { IDYES, _YTEXT("Open Task Scheduler") } });
									//if (IDYES == dlg.DoModal())
									//	FileUtil::ExecOpenFile(_YTEXT("taskschd.msc"));
								}
								else
								{
									YCodeJockMessageBox dlg(this,
										DlgMessageBox::eIcon_Error,
										_YTEXT("Error"),
										_YTEXTFORMAT(L"Failed to create the Windows Scheduler Task \"%s\"", taskName.c_str()),
										error,
										{ { IDOK, _YTEXT("OK") } });
									dlg.DoModal();
								}
							}
						}
					}
				}
			}
		}
	}
}

void BackstagePanelJobCenterMainFrame::onScheduleRemove()
{
	// do not block AJL here - let them remove any schedule, in case a license was modified and the job cannot run anymore because it is not part of the AJL

	vector<Script>			scripts;
	vector<ScriptPreset>	scriptPresets;
	vector<ScriptSchedule>	scriptSchedules;
	m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);
	ASSERT(nullptr != m_JobCenter);
	ASSERT(!scriptSchedules.empty());
	if (!scriptSchedules.empty() && nullptr != m_JobCenter)
	{
		const auto title = scriptSchedules.size() == 1
			? _T("Remove Task?")
			: _T("Remove Tasks?");

		const auto message = scriptSchedules.size() == 1
			? _YFORMAT(L"This will remove the selected task \"%s\" from Windows Scheduler.", scriptSchedules.back().m_Key.c_str())
			: _YFORMAT(L"This will remove the %s selected tasks from Windows Scheduler.", Str::getStringFromNumber<size_t>(scriptSchedules.size()).c_str());

		YCodeJockMessageBox dlg(this,
								DlgMessageBox::eIcon_Question,
								title,
								message,
								_YTEXT(""),
								{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
		if (dlg.DoModal() == IDOK)
		{
			m_JobCenter->ClearScheduleRemoveErrors();
			for (auto& ss : scriptSchedules)
				m_JobCenter->RemoveScriptSchedule(ss);
			m_Grid.LoadScripts(m_JobCenter);
			processScheduleRemoveErrors();
			SetConfigurationWasUpdated(true);
		}
	}
}

void BackstagePanelJobCenterMainFrame::onToggleView()
{
	GetParent()->SendMessage(WM_COMMAND, MAKELONG(IDC_BACKSTAGE_JOBCENTER_TOGGLE_VIEW, 0), NULL);
	m_BtnToggleView.SetCheck	(m_Grid.IsSimpleView() ? BST_UNCHECKED : BST_CHECKED);
	m_BtnPresetAdd.ShowWindow	(m_Grid.IsSimpleView() ? SW_HIDE : SW_SHOW);
}

void BackstagePanelJobCenterMainFrame::onSuggest()
{
	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != app)
		app->SuggestNewJob(m_Frame);
}

ScriptPreset BackstagePanelJobCenterMainFrame::addPreset(Script& p_Script)
{
	ScriptPreset rvPreset;

	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter)
	{
		AutomationPresetMaker maker(p_Script, m_JobCenter, this);
		const auto& errors = maker.GetErrors();
		if (errors.empty() && !maker.Cancel())
		{
			DlgJobPresetEdit dlg(p_Script, this);
			if (IDOK == dlg.DoModal())
			{
				rvPreset = dlg.GetPreset();
				m_JobCenter->AddScriptPreset(p_Script, maker.GetGeneratedPresetContent(), rvPreset);
				m_Grid.LoadScripts(m_JobCenter);

				if (!maker.GetWarnings().empty())
				{
				/*	YCodeJockMessageBox dlg(this,
											DlgMessageBox::eIcon_Warning,
											_T("Job preset generated with warnings"),
											Str::implode(maker.GetWarnings(), _YTEXT("\n")),
											_YFORMAT(L"Job: %s", s.m_Title.c_str()),
											{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
					dlg.DoModal();*/
				}
			}
		}
		else
		{
			if (maker.isEmptyPreset())
			{
				YCodeJockMessageBox dlg(this,
					DlgMessageBox::eIcon_Information,
					_T("No specific preset required"),
					Str::implode(errors, _YTEXT("\n")),
					_YFORMAT(L"For job: %s", p_Script.m_Title.c_str()),
					{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
				dlg.DoModal();
			}
			else
			{
				YCodeJockMessageBox dlg(this,
					DlgMessageBox::eIcon_ExclamationWarning,
					_T("Job preset not created"),
					Str::implode(errors, _YTEXT("\n")),
					_YFORMAT(L"For job: %s", p_Script.m_Title.c_str()),
					{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
				dlg.DoModal();
			}
		}
	}

	return rvPreset;
}

void BackstagePanelJobCenterMainFrame::onPresetAdd()
{
	vector<Script>			scripts;
	vector<ScriptPreset>	scriptPresets;
	vector<ScriptSchedule>	scriptSchedules;
	m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);
	ASSERT(scripts.size() == 1 && nullptr != m_JobCenter);
	if (scripts.size() == 1 && nullptr != m_JobCenter)
		addPreset(scripts.front());
}

//void BackstagePanelJobCenter::onPresetImport()
//{
//	ASSERT(nullptr != m_JobCenter && nullptr != m_Grid);
//	if (nullptr != m_JobCenter && nullptr != m_Grid && !isDebugMode())
//	{
//		vector<Script>			scripts;
//		vector<ScriptPreset>	scriptPresets;
//		vector<ScriptSchedule>	scriptSchedules;
//		m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);
//		ASSERT(scripts.size() == 1);
//		if (scripts.size() == 1)
//		{
//			wstring path = YFileDialog::AskOpenXmlFile(nullptr, this);
//			if (!path.empty())
//			{
//				auto newScriptPreset = ScriptParser().MakeScriptPresetFromXMLFile(m_JobCenter, path);
//				if (newScriptPreset.m_LoadError_vol.empty())
//				{
//					const auto& s = scripts.front();
//					DlgJobPresetEdit dlg(s, this);
//					if (IDOK == dlg.DoModal())
//					{
//						m_JobCenter->AddScriptPreset(s, newScriptPreset.m_ScriptContent, dlg.GetPreset());
//						m_Grid.LoadScripts(m_JobCenter);
//						SetConfigurationWasUpdated(true);
//					}
//				}
//				else
//				{
//					YCodeJockMessageBox dlg(this,
//											DlgMessageBox::eIcon_Error,
//											_YFORMAT(L"Unable to import job from: %s", path.c_str()),
//											Str::implode(newScriptPreset.m_LoadError_vol, _YTEXT("\n")).c_str(),
//											_YTEXT(""),
//											{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
//					dlg.DoModal();
//				}
//			}
//		}
//	}
//}

void BackstagePanelJobCenterMainFrame::onRun()
{
	runSelected();
}

void BackstagePanelJobCenterMainFrame::runSelected()
{
	ASSERT(m_Grid);
	ASSERT(nullptr != m_JobCenter);
	if (m_Grid && nullptr != m_JobCenter)
	{
		const auto keys = [this]()
		{
			vector<Script>			scripts;
			vector<ScriptPreset>	scriptPresets;
			vector<ScriptSchedule>	scriptSchedules;
			m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);

			if (scripts.size() == 1 && scripts.front().IsRunnable() && scriptPresets.empty())
				return std::make_pair(scripts.front().m_Key, wstring());

			if (scriptPresets.size() == 1 && scripts.empty())
			{
				auto script = m_JobCenter->GetScript(scriptPresets.front().m_ScriptID);
				if (script.IsRunnable())
					return std::make_pair(script.m_Key, scriptPresets.front().m_Key);
			}

			return std::make_pair(wstring(), wstring());
		}();

		ASSERT(!keys.first.empty());
		if (!keys.first.empty())
		{
			BackstagePanelBase* motherPanel = dynamic_cast<BackstagePanelBase*>(GetParent());
			ASSERT(nullptr != motherPanel);
			if (nullptr != motherPanel)
				motherPanel->CloseBackstage();
			m_JobCenter->Run(keys.first, keys.second);
		}
	}
}

void BackstagePanelJobCenterMainFrame::OnGridDoubleClick()
{
	onRun();
}