#pragma once

#include "BackstagePanelBase.h"
#include "YAdvancedHtmlView.h"
#include "..\..\Resource.h"

#include "ProductUpdate.h"

class BackstagePanelUpdate	: public BackstagePanelBase
							, public ProductUpdateObserver
{
public:
	BackstagePanelUpdate();
	~BackstagePanelUpdate() override;

	enum { IDD = IDD_BACKSTAGEPAGE_UPDATE };

	virtual void SetTheme(const XTPControlTheme nTheme) override;

	void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog() override;
	afx_msg void OnProceedUpdate();
	afx_msg LRESULT OnProgress(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

	virtual void Progress(const ProductUpdate& productUpdate, double progressPercentage, const wstring& shortMessage, bool downloadComplete);

private:
	void updateUpdateButton();
	void updateHtml();
	void StartProgress();
	void EndProgress();

	CXTPRibbonBackstageButton	m_UpdateButton;
	CXTPImageManager			m_imagelist;
	CXTPProgressCtrl			m_Progress;

	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;

	double m_NextProgressPercentage;
	wstring m_NextShortMessage;
	bool m_DownloadComplete;

};