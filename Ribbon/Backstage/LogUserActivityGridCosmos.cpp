#include "LogUserActivityGridCosmos.h"

#include "ActivitySQLiteLogger.h"
#include "DlgDoubleProgressCommon.h"
#include "FileUtil.h"
#include "GridUtil.h"
#include "MainFrameSapio365Session.h"
#include "YCodeJockMessageBox.h"
#include "YEncryption.h"

LogUserActivityGridCosmos::LogUserActivityGridCosmos()
{
}

void LogUserActivityGridCosmos::loadCosmosIntoGrid()
{
	if (IsActive())
	{
		if (GetRowDataCount() == 0)
			m_MostRecentLoaded.clear();

		StoreSelection();

		std::shared_ptr<Sapio365Session> session365 = MainFrameSapio365Session();
		ASSERT(session365);
		if (session365)
		{
			{
				DlgDoubleProgressCommon::RunModal(_T("Getting logs from Cosmos"), this,
				[this, session365](std::shared_ptr<DlgDoubleProgressCommon> prog)
				{
					prog->SetCounterTotal1(3);
					prog->SetCounterTotal2(2);

					prog->SetText1(_T("Connecting to Cosmos"));
					prog->IncrementCounter2(_T("Setting up connection"));

					m_CosmosSource.SetSource(ActivitySQLiteLogger(), { m_Table }, m_DBName, session365->IsCosmosSingleContainer());

					const SQLiteUtil::Column& columnROWID	= m_Table.GetColumnROWID();
					const SQLiteUtil::Column& columnDate	= m_Table.GetColumnForQuery(ActivityLogUtil::g_ColumnNameDate);

					vector <SQLiteUtil::WhereClause> whereClauses;
					if (!m_MostRecentLoaded.empty())
					{
						SQLiteUtil::WhereClause wc;
						wc.m_Column		= columnDate;
						wc.m_Operator	= SQLiteUtil::Operator::Greater;
						wc.m_Values.push_back(m_MostRecentLoaded);

						whereClauses.push_back(wc);
					}

					LinearMap<SQLiteUtil::Column, bool> orderBy;
					orderBy.push_front(columnDate, false);

					prog->IncrementCounter1(_T("Requesting logs..."));
					prog->IncrementCounter2(_T("Receiving logs. Please wait..."));

					CosmosUtil::SQLupdateData documents;
					m_CosmosSource.SelectAll(documents, session365, m_Table, whereClauses, orderBy, true, false);

					YTimeDate	ytd;
					wstring		textValue;
					bool		dkryptOK;

					const auto& myDocuments = documents[m_Table];
					prog->IncrementCounter1(_T("Loading logs in view..."));
					prog->SetCounterTotal2(static_cast<DWORD>(myDocuments.size()));
					for (const auto& cd : myDocuments)
					{
						prog->IncrementCounter2(_T("Loading log:"));

						const auto& object = cd.Content.as_object();

						const auto rowIDcolumnName = columnROWID.GetSQLcompliantName();
						auto findID = object.find(rowIDcolumnName);
						ASSERT(object.end() != findID);// I want a ROWID
						if (findID != object.end())
						{
							const auto& jsonValueID = findID->second;
							ASSERT(jsonValueID.is_string());
							if (jsonValueID.is_string())// everything a string
							{
								GridBackendField fID(jsonValueID.as_string(), GetColumnByUniqueID(columnROWID.m_Name));// store ID as string as long as Bug #190129.LS.0099A2 is not fixed
								auto row = m_RowIndex.GetRow({ fID }, true, false);
								ASSERT(nullptr != row);// I want a ROWID
								if (nullptr != row)
								{
									for (const auto& c : m_Table.m_Columns)
									{
										if (c != columnROWID)// ROWID already set
										{
											auto findValue = object.find(c.GetSQLcompliantName());
											if (findValue != object.end())
											{
												const auto& jsonValue = findValue->second;
												ASSERT(jsonValue.is_string());
												if (jsonValue.is_string())
												{
													textValue = jsonValue.as_string();

													auto gridColumn = GetColumnByUniqueID(c.m_Name);
													ASSERT(nullptr != gridColumn);
													if (nullptr != gridColumn)
													{
														if (c == columnDate && _wcsicmp(textValue.c_str(), m_MostRecentLoaded.c_str()) > 0)
															m_MostRecentLoaded = textValue;

														if (c.IsBool())
															row->AddFieldForCheckBox(Str::getINT64FromString(textValue) != 0, gridColumn);
														else if (c.IsDate())
														{
															TimeUtil::GetInstance().ConvertTextToDate(textValue, ytd);
															row->AddField(ytd, gridColumn);
														}
														else
														{
															switch (c.m_DataType)
															{
															case SQLiteUtil::DataType::SQL_TYPE_INTEGER:
																row->AddField(Str::getINT64FromString(textValue), gridColumn);
																break;
															case SQLiteUtil::DataType::SQL_TYPE_REAL:
																row->AddField(Str::getDoubleFromString(textValue), gridColumn);
																break;
															case SQLiteUtil::DataType::SQL_TYPE_TEXT:
																if (c.IsEncrypt() && !m_Krypt.empty())
																{
																	std::string dkryptTxt;
																	dkryptOK = 0 == YEncryption::decode(MFCUtil::convertUNICODE_to_ASCII(textValue), dkryptTxt, m_Krypt);
																	if (dkryptOK)
																		textValue = Str::convertFromUTF8(dkryptTxt);
																}
																else
																	dkryptOK = true;

																if (dkryptOK)
																{
																	if (c.m_MultivalueDelimiter.empty())
																	{
																		if (ActivityLogUtil::g_ColumnNameModuleName == c.m_Name)
																			row->AddField(GridUtil::ModuleName(textValue), gridColumn);
																		else
																			row->AddField(textValue, gridColumn);
																	}
																	else
																		row->AddField(Str::explodeIntoVector(textValue, c.m_MultivalueDelimiter, true), gridColumn);
																}
																break;
															default:
																ASSERT(false);
																break;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

					prog->IncrementCounter1(_T("Logs received"));
					prog->SetText2(_T("Refreshing view. Please wait..."));
				});
			}

			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
			RestoreSelection();
		}
	}
	else
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);// create headers
}

void LogUserActivityGridCosmos::customizeGridSpecific()
{
	SetAutomationName(_YTEXT("LogUserActivityGridCosmos"));
	SetAutomationActionRecording(false);

	ActivitySQLiteLogger sqlEngine;
	m_Table		= sqlEngine.GetActivityTable();
	m_DBName	= FileUtil::FileGetFileName(sqlEngine.GetDBfilename());

	SetSQLiteTable(m_Table);
}

void LogUserActivityGridCosmos::customizeGridPostProcess()
{
	AddSorting(GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameDate), GridBackendUtil::DESC);

	SetColumnsVisibleAndOrdered({	GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameDate),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameUserName),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameFullAdmin),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameAction),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameModuleName),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameObjectName),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameProperty),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameOldValue),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameNewValue)
		});

	SetColumnWidths(map<GridBackendColumn*, LONG>
					({	{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameDate),				g_ColumnSize16 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameTenantDisplayName),	g_ColumnSize6 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameUserName),			g_ColumnSize12 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameFullAdmin),			g_ColumnSize2 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameAction),				g_ColumnSize6 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameModuleName),			g_ColumnSize8 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameObjectName),			g_ColumnSize32 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameProperty),			g_ColumnSize16 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameOldValue),			g_ColumnSize16 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameNewValue),			g_ColumnSize16 }
					}));
}

void LogUserActivityGridCosmos::prepareSQLoad()
{
}

bool LogUserActivityGridCosmos::finalizeSQLoad()
{
	return false;
}

void LogUserActivityGridCosmos::SetIsActive(const bool p_Active)
{
	SQLiteGrid::SetIsActive(p_Active);

	if (IsActive())
	{
		// Update krypt with current main frame session.
		m_Krypt = O365MainFrameKrypter().getKrypt();
		loadCosmosIntoGrid();
	}
}

void LogUserActivityGridCosmos::FilterOwnerSelection()
{
	vector<wstring> uniqueObjectPKsFromSelection;
	auto cpk = GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameObjectKey);
	GetUniqueValuesAsStr(cpk, uniqueObjectPKsFromSelection, false, true, true);
	ClearFilters(-1, false);
	AddValueFilter(cpk, uniqueObjectPKsFromSelection);
	UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void LogUserActivityGridCosmos::FilterOnwerClear()
{
	ClearFilters();
}

bool LogUserActivityGridCosmos::HasOwnerFilter()
{
	auto cpk = GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameObjectKey);
	ASSERT(nullptr != cpk);
	if (nullptr != cpk)
		return cpk->GetFilterAttribs().m_bFilterByValues;
	return false;
}