#pragma once

#include "ActivitySQLiteLogger.h"
#include "SQLiteGrid.h"

class LogUserActivityGrid : public SQLiteGrid
{
public:
	LogUserActivityGrid();

	void FilterOwnerSelection();
	void FilterOnwerClear();
	bool HasOwnerFilter();

protected:
	virtual void customizeGridSpecific() override;
	virtual void customizeGridPostProcess() override;
	virtual void prepareSQLoad() override;
	virtual bool finalizeSQLoad() override;

private:
	virtual void loadPostProcess(GridBackendRow* p_Row) override;

	ActivitySQLiteLogger	m_SQLsource;
	wstring					m_MostRecentLoaded;
};