#pragma once

#include "BackstagePanelBase.h"

template <class CacheGridClass>
class BackstagePanelWithGrid : public BackstagePanelBase
{
public:
	void			DoDataExchange(CDataExchange* pDX) override;
	virtual BOOL	OnInitDialog() override;

protected:
	DECLARE_MESSAGE_MAP()
	virtual void SetTheme(const XTPControlTheme nTheme) override;

	using BackstagePanelBase::BackstagePanelBase;
	std::shared_ptr<CacheGridClass>	m_Grid;
	CXTPMarkupStatic				m_DetailedView;

private:
	CXTPRibbonBackstageLabel m_staticGridArea;
};

#include "BackstagePanelWithGrid.hpp"
