#include "LogUserActivityGrid.h"

#include "GridUtil.h"
#include "YCallbackMessage.h"
#include "YCodeJockMessageBox.h"

LogUserActivityGrid::LogUserActivityGrid() : SQLiteGrid()
{
}

void LogUserActivityGrid::customizeGridSpecific()
{
	SetAutomationName(_YTEXT("LogUserActivityGrid"));
	SetAutomationActionRecording(false);

	SetSQLiteEngine(m_SQLsource, m_SQLsource.GetActivityTable());
}

void LogUserActivityGrid::customizeGridPostProcess()
{
	AddSorting(GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameDate), GridBackendUtil::DESC);
	
	SetColumnsVisibleAndOrdered({	GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameDate),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameUserName),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameFullAdmin),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameAction),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameModuleName),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameObjectName),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameProperty),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameOldValue),
									GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameNewValue)
								});

	SetColumnWidths(map<GridBackendColumn*, LONG>
					({	{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameDate),				g_ColumnSize16 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameTenantDisplayName),	g_ColumnSize6 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameUserName),			g_ColumnSize12 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameFullAdmin),			g_ColumnSize2 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameAction),				g_ColumnSize6 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameModuleName),			g_ColumnSize8 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameObjectName),			g_ColumnSize32 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameProperty),			g_ColumnSize16 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameOldValue),			g_ColumnSize16 },
						{ GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameNewValue),			g_ColumnSize16 }
					}));
}

void LogUserActivityGrid::prepareSQLoad()
{
	if (GetRowDataCount() == 0)
		m_MostRecentLoaded.clear();

	const SQLiteUtil::Column& columnDate = m_SQLsource.GetActivityTable().GetColumnForQuery(ActivityLogUtil::g_ColumnNameDate);

	if (!m_MostRecentLoaded.empty())
	{
		SQLiteUtil::WhereClause wc;
		wc.m_Column		= columnDate;
		wc.m_Operator	= SQLiteUtil::Operator::Greater;
		wc.m_Values.push_back(m_MostRecentLoaded);

		m_SelectQuery.m_Where.push_back(wc);
	}

	m_SelectQuery.m_OrderBy.push_front(columnDate, false);
}

void LogUserActivityGrid::loadPostProcess(GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		auto dateColumn = GetColumnByUniqueID(m_SQLsource.GetActivityTable().GetColumnForQuery(ActivityLogUtil::g_ColumnNameDate).m_Name);
		ASSERT(nullptr != dateColumn);
		if (nullptr != dateColumn)
		{
			const auto& f = p_Row->GetField(dateColumn);
			ASSERT(f.HasValue());
			if (_wcsicmp(f.GetValueStr(), m_MostRecentLoaded.c_str()) > 0)
				m_MostRecentLoaded = f.GetValueStr();
		}

		auto moduleNameColumn = GetColumnByUniqueID(m_SQLsource.GetActivityTable().GetColumnForQuery(ActivityLogUtil::g_ColumnNameModuleName).m_Name);
		ASSERT(nullptr != moduleNameColumn);
		if (nullptr != moduleNameColumn)
		{
			const auto& f = p_Row->GetField(moduleNameColumn);
			ASSERT(f.HasValue() && f.IsString());
			p_Row->AddField(GridUtil::ModuleName(f.GetValueStr()), moduleNameColumn);

		}
	}
}

bool LogUserActivityGrid::finalizeSQLoad()
{
	return false;
}

void LogUserActivityGrid::FilterOwnerSelection()
{
	vector<wstring> uniqueObjectPKsFromSelection;
	auto cpk = GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameObjectKey);
	GetUniqueValuesAsStr(cpk, uniqueObjectPKsFromSelection, false, true, true);
	ClearFilters(-1, false);
	AddValueFilter(cpk, uniqueObjectPKsFromSelection);
	UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void LogUserActivityGrid::FilterOnwerClear()
{
	ClearFilters();
}

bool LogUserActivityGrid::HasOwnerFilter()
{
	auto cpk = GetColumnByUniqueID(ActivityLogUtil::g_ColumnNameObjectKey);
	ASSERT(nullptr != cpk);
	if (nullptr != cpk)
		return cpk->GetFilterAttribs().m_bFilterByValues;
	return false;
}