#pragma once

#include "CosmosSQLiteBridge.h"
#include "SQLiteGrid.h"
#include "FrameLogger.h"

class LogUserActivityGridCosmos : public SQLiteGrid // extends SQLiteGrid to have the grid automatically customized - and we do not need a Cosmos grid (Cosmos data usually transits via SQLite)
{
public:
	LogUserActivityGridCosmos();

	void SetIsActive(bool p_Active) override;

	void FilterOwnerSelection();
	void FilterOnwerClear();
	bool HasOwnerFilter();

protected:
	virtual void customizeGridSpecific() override;
	virtual void customizeGridPostProcess() override;
	virtual void prepareSQLoad() override;
	virtual bool finalizeSQLoad() override;

private:
	void loadCosmosIntoGrid();

	CosmosSQLiteBridge					m_CosmosSource;
	SQLiteUtil::Table					m_Table;
	wstring								m_DBName;
	wstring								m_MostRecentLoaded;
	std::string							m_Krypt;
};