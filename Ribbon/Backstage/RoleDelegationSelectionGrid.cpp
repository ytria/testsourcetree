#include "RoleDelegationSelectionGrid.h"

#include "../Resource.h"
#include "BusinessUserConfiguration.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessSiteConfiguration.h"
#include "RoleDelegationManager.h"
#include "YCodeJockMessageBox.h"

RoleDelegationSelectionGrid::RoleDelegationSelectionGrid() : RoleDelegationCfgGridBase(),
	m_ActiveDelegationID(0),
	m_ColumnPrivilege(nullptr),
	m_ColumnFilters(nullptr)
{
	GetBackend().SetIgnoreFieldModifications(true);
}

void RoleDelegationSelectionGrid::customizeGridSpecific()
{
	SetAutomationName(_YTEXT("RoleDelegationSelectionGrid"));
	SetAutomationActionRecording(false);

	SetSQLiteEngine(RoleDelegationManager::GetInstance().GetRoleDelegationSQLite(), RoleDelegationManager::GetInstance().GetRoleDelegationSQLite().GetTableDelegations());
}

void RoleDelegationSelectionGrid::SetSession(std::shared_ptr<Sapio365Session> p_Session)
{
	m_Session = p_Session;
	ASSERT(m_Session);
	if (m_Session)
		m_SessionUserID = m_Session->GetConnectedUserID();
}

void RoleDelegationSelectionGrid::prepareSQLoadSpecific()
{
	m_SelectQuery.Clear();
	m_SelectQuery.m_UseSQLStatement = true;
	if (!m_SessionUserID.empty())
		m_SelectQuery.m_SQLStatement = RoleDelegationManager::GetInstance().GetRoleDelegationSQLite().GetSQLSelectStatementDelegationsForThisUser(m_SessionUserID);
}

void RoleDelegationSelectionGrid::colorize(GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		p_Row->SetColorDefault();
		if (Str::getINT64FromString(p_Row->GetField(GetColumnRowID()).GetValueStr()) == m_ActiveDelegationID)
			p_Row->SetColor(RGB(235, 244, 230));
	}
}

int64_t RoleDelegationSelectionGrid::GetRoleDelegationIDFromSelection() const
{
	int64_t rvID = 0;

	vector<GridBackendRow*> selectedNonGroupRows;
	GetSelectedNonGroupRows(selectedNonGroupRows);

	if (selectedNonGroupRows.size() == 1)
	{
		auto row = selectedNonGroupRows.back();
		ASSERT(nullptr != row);
		if (nullptr != row)
			rvID = Str::getINT64FromString(row->GetField(GetColumnRowID()).GetValueStr());
	}

	return rvID;
}

void RoleDelegationSelectionGrid::SetRoleDelegationID(const int64_t p_AciveDelegationID)
{
	m_ActiveDelegationID = p_AciveDelegationID;
}

void RoleDelegationSelectionGrid::loadPostProcessSpecific(GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		colorize(p_Row);

		static const auto& g_FilterTypes = RoleDelegationUtil::FilterTypes::GetInstance();

		static const auto& ucfg = BusinessUserConfiguration::GetInstance();
		static const auto& gcfg = BusinessGroupConfiguration::GetInstance();
		static const auto& scfg = BusinessSiteConfiguration::GetInstance();

		const auto rd = RoleDelegationManager::GetInstance().LoadRoleDelegationFromSQLite(Str::getINT64FromString(p_Row->GetField(GetColumnRowID()).GetValueStr()), m_Session);
		vector<wstring> v;
		for (const auto p : rd.GetPrivileges())
			v.push_back(RoleDelegationUtil::GetPrivilegeLabel(p.m_Privilege));
		p_Row->AddField(v, m_ColumnPrivilege);

		v.clear();
		for (const auto& p : rd.GetFilters())
		{
			for (const auto& f : p.second)
			{
				wstring propTitle = f.m_ObjectType == g_FilterTypes.GetTypeGroup() ?
										gcfg.GetTitle(f.m_Property) :
									f.m_ObjectType == g_FilterTypes.GetTypeSite() ?
										scfg.GetTitle(f.m_Property) :
									ucfg.GetTitle(f.m_Property);

				v.push_back(YtriaTranslate::Do(RoleDelegationCfgDelegationGrid_finalizeSQLoad_14, _YLOC("%1: %2 %3 '%4'"), f.m_ObjectType.c_str(), propTitle.c_str(), RoleDelegationUtil::GetTaggerMatchMethod(f.m_TaggerMatchMethod).c_str(), f.m_Value.c_str()));
			}
		}
		p_Row->AddField(v, m_ColumnFilters);
	}
}

void RoleDelegationSelectionGrid::customizeGridPostProcessSpecific()
{
	m_ColumnPrivilege = AddColumn(_YUID("PRIVILEGES"), YtriaTranslate::Do(FrameDriveItems_customizeActionsRibbonTab_1, _YLOC("Permissions")).c_str(), _YTEXT(""));
	ASSERT(nullptr != m_ColumnPrivilege);
	if (nullptr != m_ColumnPrivilege)
		m_ColumnPrivilege->SetWidth(m_ColumnSize32);

	m_ColumnFilters = AddColumn(_YUID("SCOPE"), YtriaTranslate::Do(DlgRoleSeeInfo_generateJSONScriptDataSpecific_4, _YLOC("Scopes")).c_str(), _YTEXT(""));
	ASSERT(nullptr != m_ColumnFilters);
	if (nullptr != m_ColumnFilters)
		m_ColumnFilters->SetWidth(m_ColumnSize32);
}