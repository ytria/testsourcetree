#pragma once

#include "YtriaGridAutoShortcuts.h"
#include "..\..\Resource.h"

class AutomatedApp;
class ShortcutsManager;

class BackstagePanelAutomationShortcuts : public CXTPRibbonBackstagePage
{
public:
	BackstagePanelAutomationShortcuts(AutomatedApp* i_TheApp);

	enum { IDD = IDD_BACKSTAGEPAGE_AUTOMATIONSHORTCUTS };

	virtual void SetTheme(const XTPControlTheme nTheme);

	void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnUpdateOK(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCancel(CCmdUI* pCmdUI);

	virtual BOOL OnKillActive() override;

	DECLARE_MESSAGE_MAP()

private:
	map<int, pair<wstring, bool>>	m_mapData;

	YtriaGridAutoShortcuts			m_gridShortcuts;
	AutomatedApp*					m_TheApp;
	ShortcutsManager*				m_shortcutsManager;

	CXTPRibbonBackstageButton		m_btnOk;
	CXTPRibbonBackstageButton		m_btnCancel;
	CXTPRibbonBackstageLabel		m_staticGridArea;
	CXTPRibbonBackstageLabel		m_staticExplanation;
};