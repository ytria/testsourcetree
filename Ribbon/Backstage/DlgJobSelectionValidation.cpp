#include "DlgJobSelectionValidation.h"

#include "CRMpipe.h"

const wstring& DlgJobSelectionValidation::g_VarNameVN = _YTEXT("ValidationNumber");

DlgJobSelectionValidation::DlgJobSelectionValidation(CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
{
	setIgnoreBlockVertResizeForHTMLContainer(true);
}

DlgJobSelectionValidation::~DlgJobSelectionValidation()
{
}

void DlgJobSelectionValidation::generateJSONScriptData()
{
	initMain(_YTEXT("DlgJobSelectionValidation"));

	getGenerator().addIconTextField(_YTEXT("infoIcon")
		, YtriaTranslate::Do(DlgLicenseTenantUserCaps_generateJSONScriptData_9, _YLOC("To verify you are the authorised user, please enter the subscription code (Sxxxxxxxx) for this license."))
		, _T("far fa-info-circle"));

	getGenerator().Add(StringEditor(g_VarNameVN, _T("Subscription Code"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, _YTEXT("")));

	getGenerator().addApplyButton(YtriaTranslate::Do(DlgAutomationInputHTML_generateJSONScriptData_1, _YLOC("OK")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgAutomationInputHTML_generateJSONScriptData_2, _YLOC("Cancel")).c_str());
}

bool DlgJobSelectionValidation::processPostedData(const IPostedDataTarget::PostedData& data)
{
	for (const auto& item : data)
		if (MFCUtil::StringMatchW(item.first, g_VarNameVN))
			m_VN = Str::lrtrim(item.second);

	return true;
}

wstring DlgJobSelectionValidation::getDialogTitle() const
{
	return _T("License Validation");
}

const wstring& DlgJobSelectionValidation::GetVN() const
{
	return m_VN;
}
