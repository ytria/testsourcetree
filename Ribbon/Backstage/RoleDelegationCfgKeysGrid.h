#pragma once

#include "RoleDelegationCfgGridBase.h"

class RoleDelegationCfgKeysGrid : public RoleDelegationCfgGridBase
{
public:
	RoleDelegationCfgKeysGrid();

protected:
	virtual void customizeGridSpecific() override;
	virtual void customizeGridPostProcessSpecific() override;
	virtual void loadPostProcessSpecific(GridBackendRow* p_Row) override;
	virtual void prepareSQLoadSpecific() override;
};