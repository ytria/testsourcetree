#pragma once

#include "RoleDelegationCfgGridBase.h"

class Sapio365Session;

class RoleDelegationSelectionGrid : public RoleDelegationCfgGridBase
{
public:
	RoleDelegationSelectionGrid();

	int64_t	GetRoleDelegationIDFromSelection() const;
	void	SetRoleDelegationID(const int64_t p_AciveDelegationID);// inform the grid the delegation was successfully set in the sapio365 session
	void	SetSession(std::shared_ptr<Sapio365Session> p_Session);

protected:
	virtual void customizeGridSpecific() override;
	virtual void customizeGridPostProcessSpecific() override;
	virtual void prepareSQLoadSpecific() override;
	virtual void loadPostProcessSpecific(GridBackendRow* p_Row) override;

private:
	void colorize(GridBackendRow* p_Row);

	int64_t m_ActiveDelegationID;
	std::shared_ptr<Sapio365Session> m_Session;
	wstring m_SessionUserID;

	GridBackendColumn* m_ColumnPrivilege;
	GridBackendColumn* m_ColumnFilters;
};