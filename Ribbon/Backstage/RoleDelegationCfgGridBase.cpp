#include "RoleDelegationCfgGridBase.h"

#include "RoleDelegationUtil.h"

RoleDelegationCfgGridBase::RoleDelegationCfgGridBase()
	: SQLiteGrid()
	, m_ColumnStatus(nullptr)
	, m_ColumnName(nullptr)
	, m_ColumnInfo(nullptr)
	, m_ColumnUpdDate(nullptr)
	, m_ColumnUpdID(nullptr)
	, m_ColumnUpdName(nullptr)
	, m_ColumnUpdPrincipalName(nullptr)
	, m_ColumnSize1(0)
{
	m_CreateFlags = GridBackendUtil::CREATESTATUSBAR | GridBackendUtil::CREATESORTING;
	GetBackend().SetIgnoreFieldModifications(true);
}

void RoleDelegationCfgGridBase::SetOnDoubleClickRowFunction(OnDoubleClickRowFunction p_OnDoubleClickRowFunction)
{
	m_OnDoubleClickRowFunction = p_OnDoubleClickRowFunction;
}

void RoleDelegationCfgGridBase::OnCustomDoubleClick()
{
	if (nullptr != m_OnDoubleClickRowFunction)
		m_OnDoubleClickRowFunction();
}

void RoleDelegationCfgGridBase::customizeGridPostProcess()
{
	m_ColumnStatus				= GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameStatus);
	m_ColumnName				= GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameName);
	m_ColumnInfo				= GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameInfo);
	m_ColumnUpdDate				= GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameDate);
	m_ColumnUpdID				= GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameUpdID);
	m_ColumnUpdName				= GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameUpdName);
	m_ColumnUpdPrincipalName	= GetColumnByUniqueID(RoleDelegationUtil::g_ColumnNameUpdEmail);

	m_ColumnSize1	= HIDPI_XW(8);
	m_ColumnSize3	= 3		* m_ColumnSize1;
	m_ColumnSize12	= 12	* m_ColumnSize1;
	m_ColumnSize22	= 22	* m_ColumnSize1;
	m_ColumnSize32	= 32	* m_ColumnSize1;

	if (nullptr != m_ColumnStatus)
		m_ColumnStatus->SetWidth(m_ColumnSize3);

	ASSERT(nullptr != m_ColumnName);
	if (nullptr != m_ColumnName)
		m_ColumnName->SetWidth(m_ColumnSize32);

	ASSERT(nullptr != m_ColumnUpdName);
	if (nullptr != m_ColumnUpdName)
		m_ColumnUpdName->SetWidth(m_ColumnSize22);

	ASSERT(nullptr != m_ColumnInfo);
	if (nullptr != m_ColumnInfo)
		m_ColumnInfo->SetWidth(m_ColumnSize32);

	vector<GridBackendColumn*> udpateColumns = { m_ColumnUpdID, m_ColumnUpdPrincipalName, m_ColumnUpdName, m_ColumnUpdDate };
	for (auto uc : udpateColumns)
		uc->SetFamily(YtriaTranslate::Do(RoleDelegationCfgGridBase_customizeGridPostProcess_1, _YLOC("Update Information")).c_str());

	vector < GridBackendColumn* > columns;
	GetBackend().GetAllColumnsSortedByPositionVirtual(columns);
	if (!columns.empty())
		for (const auto& uc : udpateColumns)
			MoveColumnAfter(uc, columns.back());
	
	if (nullptr != m_ColumnStatus)
		MoveColumnBefore(m_ColumnStatus, columns.front());
	if (nullptr != m_ColumnName)
		MoveColumnBefore(m_ColumnName, columns.front());
	if (nullptr != m_ColumnInfo)
		MoveColumnBefore(m_ColumnInfo, columns.front());

	SetColumnsVisible(udpateColumns, false, false);
	SetColumnsVisible({m_ColumnUpdDate}, true, false);
	AddSorting(m_ColumnUpdDate, GridBackendUtil::DESC);

	customizeGridPostProcessSpecific();
}

void RoleDelegationCfgGridBase::loadPostProcess(GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		if (nullptr != m_ColumnStatus)
		{
			auto& f = p_Row->GetField(m_ColumnStatus);
			if (f.HasValue())
			{
				static map<uint64_t, int32_t> g_Icons;
				if (g_Icons.empty())
				{
					g_Icons[RoleDelegationUtil::RoleStatus::STATUS_OK]		= GridBackendUtil::ICON_OK16;
					g_Icons[RoleDelegationUtil::RoleStatus::STATUS_LOCKED]	= GridBackendUtil::ICON_LOCK;
					g_Icons[RoleDelegationUtil::RoleStatus::STATUS_REMOVED] = GridBackendUtil::ICON_DELETED;
				}

				f.SetIgnoreModifications(true);
				f.SetValue(RoleDelegationUtil::GetStatus(static_cast<uint32_t>(f.GetValueULong())), g_Icons[f.GetValueULong()]);
			}
		}

		loadPostProcessSpecific(p_Row);
	}
}

void RoleDelegationCfgGridBase::prepareSQLoad()
{
	ASSERT(nullptr != m_SQLTable);
	if (nullptr != m_SQLTable)
	{
		SQLiteUtil::WhereClause whereClause;
		whereClause.m_Column	= m_SQLTable->GetColumnForQuery(RoleDelegationUtil::g_ColumnNameStatus);
		whereClause.m_Operator	= SQLiteUtil::Operator::NotEqual;
		whereClause.m_Values.push_back(Str::getStringFromNumber<int64_t>(RoleDelegationUtil::STATUS_REMOVED));
		m_SelectQuery.m_Where.push_back(whereClause);
	}

	prepareSQLoadSpecific();
}

bool RoleDelegationCfgGridBase::finalizeSQLoad()
{
	return false;
}