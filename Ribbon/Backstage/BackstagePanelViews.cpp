#include "BackstagePanelViews.h"

#include "BaseO365Grid.h"
#include "CacheGridGroupArea.h"
#include "DlgGridViewConfig.h"
#include "GridFrameBase.h"
#include "GridViewManager.h"
#include "MainFrameSapio365Session.h"

BEGIN_MESSAGE_MAP(BackstagePanelViews, BackstagePanelWithGrid)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_VIEWS_EDIT,			onEdit)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_VIEWS_DELETE,		onDelete)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_VIEWS_SETASDEFAULT,	onSetAsDefault)
END_MESSAGE_MAP()

BackstagePanelViews::BackstagePanelViews(GridFrameBase& p_Frame) : BackstagePanelWithGrid<ViewGrid>(IDD),
	m_Frame(p_Frame)
{
	m_Grid = std::make_shared<ViewGrid>(m_Frame.GetGrid().GetViewModuleName());
}

BackstagePanelViews::~BackstagePanelViews()
{
	m_Grid->RemoveSelectionObserver(this);
}

void BackstagePanelViews::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelWithGrid<ViewGrid>::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_VIEWS_EDIT,			m_BtnEdit);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_VIEWS_DELETE,			m_BtnDelete);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_VIEWS_SETASDEFAULT,	m_BtnSetAsDefault);
}

BOOL BackstagePanelViews::OnInitDialog()
{
	BackstagePanelWithGrid<ViewGrid>::OnInitDialog();
	ASSERT(m_Grid);
	if (m_Grid)
		m_Grid->AddSelectionObserver(this);

	setCaption(_T("Views"));

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_VIEWS_EDIT };
		m_imagelist.SetIcons(IDB_ICON_BUTTON_VIEWS_EDIT, ids, 1, CSize(0, 0), xtpImageNormal);
		SetupButton(m_BtnEdit, _T("Edit"), _YTEXT(""), m_imagelist);
		m_BtnEdit.EnableWindow(FALSE);
	}

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_VIEWS_DELETE };
		m_imagelist.SetIcons(IDB_ICON_BUTTON_VIEWS_DELETE, ids, 1, CSize(0, 0), xtpImageNormal);
		SetupButton(m_BtnDelete, YtriaTranslate::Do(ApplicationShortcuts_ApplicationShortcuts_166, _YLOC("Delete")).c_str(), _YTEXT(""), m_imagelist);
		m_BtnDelete.EnableWindow(FALSE);
	}

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_VIEWS_SETASDEFAULT };
		m_imagelist.SetIcons(IDB_ICON_BUTTON_VIEWS_DEFAULT, ids, 1, CSize(0, 0), xtpImageNormal);
		SetupButton(m_BtnSetAsDefault, _T("Set As Default"), _YTEXT(""), m_imagelist);
		m_BtnSetAsDefault.EnableWindow(FALSE);
	}

	return TRUE;
}

void BackstagePanelViews::updateMainGrid() const
{
	ASSERT(m_Frame.GetGrid().GetGroupArea() != nullptr);
	if (m_Frame.GetGrid().GetGroupArea() != nullptr)
		m_Frame.GetGrid().GetGroupArea()->UpdateViews();
}

void BackstagePanelViews::onEdit()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		vector<GridView> view;
		m_Grid->GetSelectedViews(view);
		ASSERT(view.size() == 1);
		if (view.size() == 1)
		{
			auto& v = view.front();
			if (!v.IsSystem() && DlgGridViewConfig(this, v, m_Grid->GetModuleName()).DoModal() == IDOK)
			{
				GridViewManager::GetInstance().Write(v, MainFrameSapio365Session());
				m_Grid->LoadViews();
				updateMainGrid();
			}
			SelectionChanged(*m_Grid);
		}
	}
}

void BackstagePanelViews::onDelete()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		vector<int64_t> vIDs;
		m_Grid->GetSelectedIDs(vIDs);
		GridViewManager::GetInstance().DeleteViews(vIDs);
		m_Grid->LoadViews();
		updateMainGrid();
		SelectionChanged(*m_Grid);
	}
}

void BackstagePanelViews::onSetAsDefault()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		vector<GridView> view;
		m_Grid->GetSelectedViews(view);
		ASSERT(view.size() == 1);
		if (view.size() == 1)
		{
			auto& v = view.front();
			ASSERT(!v.IsDefault());
			if (!v.IsDefault())
			{
				v.SetDefault(true);
				GridViewManager::GetInstance().Write(v, MainFrameSapio365Session());
				m_Grid->LoadViews();
				updateMainGrid();
			}
		}
		SelectionChanged(*m_Grid);
	}
}

void BackstagePanelViews::SetTheme(const XTPControlTheme nTheme)
{
	m_BtnEdit.SetTheme(nTheme);
	m_BtnDelete.SetTheme(nTheme);
	m_BtnSetAsDefault.SetTheme(nTheme);
	BackstagePanelWithGrid<ViewGrid>::SetTheme(nTheme);
}

BOOL BackstagePanelViews::OnSetActive()
{
	ASSERT(m_Grid);
	if (BackstagePanelWithGrid<ViewGrid>::OnSetActive() == TRUE && m_Grid)
	{
		m_Grid->LoadViews();
		return TRUE;
	}

	return FALSE;
}

BOOL BackstagePanelViews::OnKillActive()
{
	return TRUE;
}

void BackstagePanelViews::OnGridDoubleClick()
{
	onEdit();
}

void BackstagePanelViews::SelectionChanged(const CacheGrid& grid)
{
	m_BtnEdit.EnableWindow(FALSE);
	m_BtnDelete.EnableWindow(FALSE);
	m_BtnSetAsDefault.EnableWindow(FALSE);

	ASSERT(m_Grid);
	if (m_Grid)
	{
		vector<GridView> views;
		m_Grid->GetSelectedViews(views);
		auto hasOneSystem	= false;
		auto hasDefault		= false;
		for (const auto& v : views)
		{
			if (v.IsSystem())
				hasOneSystem = true;
			if (v.IsDefault())
				hasDefault = true;
			if (hasDefault && hasOneSystem)
				break;
		}
		m_BtnEdit.EnableWindow(views.size() != 1 || hasOneSystem ? FALSE : TRUE);
		m_BtnDelete.EnableWindow(views.empty() || hasOneSystem ? FALSE : TRUE);
		m_BtnSetAsDefault.EnableWindow(views.size() != 1 || hasDefault ? FALSE : TRUE);
	}
}