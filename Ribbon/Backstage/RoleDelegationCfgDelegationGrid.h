#pragma once

#include "RoleDelegationUtil.h"
#include "RoleDelegationCfgGridBase.h"
#include "RttrIncludes.h"

class RoleDelegationCfgDelegationGrid : public RoleDelegationCfgGridBase
{
public:
	RoleDelegationCfgDelegationGrid();

	vector<int64_t>									GetRoleDelegationIDsFromSelection() const;
	vector<int64_t>									GetAllRoleDelegationIDs() const;
	map<wstring, vector<pair<int64_t, int64_t>>>	GetElementIDsFromSelection(const vector<GridBackendRow*>& p_SelectedNonGroupRows) const;// return a list of { item id, mother delegation id } by object type

	const wstring& GetTypeDelegation() const;
	const wstring& GetTypeMember() const;
	const wstring& GetTypeFilter() const;
	const wstring& GetTypeParentMembers() const;
	const wstring& GetTypeParentFilters() const;

	int64_t GetDelegationID(GridBackendRow* p_Row) const;

	const static rttr::type::type_id g_ElementTypeIDPrivilegesParent;
	const static rttr::type::type_id g_ElementTypeIDFiltersParent;
	const static rttr::type::type_id g_ElementTypeIDMembersParent;
	const static rttr::type::type_id g_ElementTypeIDLicensePoolsParent;
	const static rttr::type::type_id g_ElementTypeIDDelegation;
	const static rttr::type::type_id g_ElementTypeIDPrivilege;
	const static rttr::type::type_id g_ElementTypeIDFilter;
	const static rttr::type::type_id g_ElementTypeIDMember;
	const static rttr::type::type_id g_ElementTypeIDLicensePool;

protected:
	virtual void customizeGridSpecific() override;
	virtual void prepareSQLoadSpecific() override;
	virtual bool finalizeSQLoad() override;
	virtual void customizeGridPostProcessSpecific() override;
	virtual void loadPostProcessSpecific(GridBackendRow* p_Row) override;

private:
	void setKey(GridBackendRow* p_Row, const RoleDelegation& p_rd);

	GridBackendColumn* m_ColumnFilterType;// user, group, site
	GridBackendColumn* m_ColumnKey;// filter, privilege, license pool or member user
	GridBackendColumn* m_ColumnName;
	GridBackendColumn* m_ColumnMemberPrincipalName;
	GridBackendColumn* m_ColumnMemberGraphID;
	GridBackendColumn* m_ColumnOptionLogSessionLogin;
	GridBackendColumn* m_ColumnOptionLogModuleOpen;
	GridBackendColumn* m_ColumnOptionEnforceRole;

	static vector<std::tuple<rttr::type::type_id, int32_t, wstring>> g_ObjectTypes;

	static wstring g_ElementTypeRightsParent;
	static wstring g_ElementTypeFiltersParent;
	static wstring g_ElementTypeMembersParent;
	static wstring g_ElementTypeLicensePoolsParent;
	static wstring g_ElementTypeDelegation;
	static wstring g_ElementTypePrivilege;
	static wstring g_ElementTypeFilter;
	static wstring g_ElementTypeMember;
	static wstring g_ElementTypeLicensePool;

	enum IconNumber
	{
		ICON_KEY = GridBackendUtil::g_FirstGridIcon,

		// filter type
		ICON_USER,
		ICON_SITE,
		ICON_GROUP,
		ICON_OPTION_ENFORCEROLE,
		ICON_OPTION_LOGMODULEOPEN,
		ICON_OPTION_LOGSESSIONLOGIN
	};
};