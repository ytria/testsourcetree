#pragma once

#include "JobCenterGrid.h"
#include "Sapio365Session.h"

class Sapio365Session;
class CodeJockFrameBase;

class IActiveSetterJC
{
public:
	virtual ~IActiveSetterJC() = default;
	virtual void OnSetActive() = 0;
	virtual void OnKillActive() = 0;
};

class BackstagePanelJobCenterBase : public CExtResizableDialog, public GridSelectionObserver, public IActiveSetterJC
{
public:
	BackstagePanelJobCenterBase(CodeJockFrameBase* p_Frame, UINT nID, bool p_ForModule);

	void DoDataExchange(CDataExchange* pDX) override;
	virtual void OnKillActive() override;

	virtual void SetTheme(const XTPControlTheme nTheme);

	void SetConfigurationWasUpdated(const bool p_Updated);

	virtual void SetSimpleView(bool p_SimpleView);
	bool IsSimpleView() const;

	afx_msg void OnEdit();
	virtual afx_msg void OnGridDoubleClick();

	void ImportScripts(const vector<wstring>& p_FilePath);

protected:
	DECLARE_MESSAGE_MAP()

	void processScheduleRemoveErrors();

	virtual BOOL OnInitDialog() override;
	virtual BOOL OnInitDialogSpecific() = 0;
	virtual void updateButtons() = 0;

	JobCenterGrid		m_Grid;
	CodeJockFrameBase*	m_Frame;
	bool m_ForModule;

	CXTPRibbonBackstageButton m_BtnImport;
	CXTPRibbonBackstageButton m_BtnExport;
	CXTPRibbonBackstageButton m_BtnEdit;
	CXTPRibbonBackstageButton m_BtnRemove;
	CXTPRibbonBackstageButton m_BtnUpdate;

	CXTPImageManager m_ImageList;

	AutomationWizardO365* m_JobCenter;

	bool isDebugMode();
	std::shared_ptr<const Sapio365Session> getSession() const;

private:
	afx_msg void onImport();
	afx_msg void onExport();
	afx_msg void onUpdate();// refresh xml content from local file
	afx_msg void onRemove();

	afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor); // Copy pasted from CXTPRibbonBackstagePage::SetTheme for color handling
	
	virtual void SelectionChanged(const CacheGrid& grid) override;

	virtual void setThemeSpecific(const XTPControlTheme nTheme) = 0;

	void invertRemoveAndImport();

	bool edit(Script& p_Script);
	bool edit(ScriptPreset& p_ScriptPreset);

	CXTPImageManager m_Imagelist;

	CXTPRibbonBackstageLabel m_staticGridArea;

	bool m_ConfigurationWasUpdated;

	CXTPBrush m_xtpBrushBack;
	XTP_SUBSTITUTE_GDI_MEMBER_WITH_CACHED(CBrush, m_brBack, m_xtpBrushBack, GetBackBrushHandle);
	COLORREF m_clrBack;
	COLORREF m_clrText;
};