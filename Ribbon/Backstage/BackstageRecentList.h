#pragma once

#include "RecentOpeningManager.h"

class CodeJockFrameBase;

class BackstageRecentList : public CXTPRecentFileListBox
{
public:
	BackstageRecentList();

	void SetRecentOpeningManager(RecentOpeningManager* recentOpeningManager);
	void SavePinStates();

	// Voluntarily hide CXTPRecentFileListBox::BuildItems()
	void BuildItems();

	int		GetSelectedItemCommand() const;
	wstring GetSelectedItemPath() const;

private:
	RecentOpeningManager* m_RecentOpeningManager;

	class CXTPRecentFileListDecorator;
	std::unique_ptr<CXTPRecentFileListDecorator> m_RecentFileList;
};