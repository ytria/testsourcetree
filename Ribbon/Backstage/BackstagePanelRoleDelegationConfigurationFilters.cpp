#include "BackstagePanelRoleDelegationConfigurationFilters.h"

#include "BackstageLogger.h"
#include "FileUtil.h"
#include "MainFrame.h"
#include "Sapio365Session.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTranslate.h"

BEGIN_MESSAGE_MAP(BackstagePanelRoleDelegationConfigurationFilters, BackstagePanelWithGrid<RoleDelegationCfgFiltersGrid>)
END_MESSAGE_MAP()


BackstagePanelRoleDelegationConfigurationFilters::BackstagePanelRoleDelegationConfigurationFilters()
	: BackstagePanelWithGrid<RoleDelegationCfgFiltersGrid>(IDD)
{
	m_Grid = std::make_shared<RoleDelegationCfgFiltersGrid>();
}

BackstagePanelRoleDelegationConfigurationFilters::~BackstagePanelRoleDelegationConfigurationFilters()
{
}

BOOL BackstagePanelRoleDelegationConfigurationFilters::OnInitDialog()
{
	BackstagePanelWithGrid<RoleDelegationCfgFiltersGrid>::OnInitDialog();
	setCaption(YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationFilters_OnInitDialog_1, _YLOC("Build Filters")).c_str());
	return TRUE;
}

void BackstagePanelRoleDelegationConfigurationFilters::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelWithGrid<RoleDelegationCfgFiltersGrid>::DoDataExchange(pDX);
}

void BackstagePanelRoleDelegationConfigurationFilters::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelWithGrid<RoleDelegationCfgFiltersGrid>::SetTheme(nTheme);
}

BOOL BackstagePanelRoleDelegationConfigurationFilters::OnSetActive()
{
	if (BackstagePanelWithGrid<RoleDelegationCfgFiltersGrid>::OnSetActive() == TRUE)
	{
		m_Grid->SetIsActive(true);
		return TRUE;
	}

	return FALSE;
}

BOOL BackstagePanelRoleDelegationConfigurationFilters::OnKillActive()
{
	if (BackstagePanelWithGrid<RoleDelegationCfgFiltersGrid>::OnKillActive() == TRUE)
	{
		m_Grid->SetIsActive(false);
		return TRUE;
	}

	return FALSE;
}