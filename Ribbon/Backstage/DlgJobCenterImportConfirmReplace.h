#pragma once

#include "DlgFormsHTML.h"
#include "JobCenterUtil.h"

class DlgJobCenterImportConfirmReplace : public DlgFormsHTML
{
public:
	DlgJobCenterImportConfirmReplace(map<wstring, Script>& p_ScriptsToReplace, map<wstring, ScriptPreset>& p_PresetsToReplace, CWnd* p_Parent);

protected:
	virtual wstring getDialogTitle() const override;
	virtual void	generateJSONScriptData() override;
	virtual bool	processPostedData(const IPostedDataTarget::PostedData& data) override;

private:
	void initJson(const wstring& p_DlgName);
	void addMappingEditors();
	void addButtons();

	map<wstring, Script>&		m_ScriptsToReplace;
	map<wstring, ScriptPreset>& m_PresetsToReplace;
};