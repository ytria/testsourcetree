#pragma once

#include "LogUserActivityGrid.h"
#include "..\..\Resource.h"

class BackstageTabPanelLogUserActivityLocal : public CExtResizableDialog, public GridSelectionObserver
{
public:
	BackstageTabPanelLogUserActivityLocal();
	~BackstageTabPanelLogUserActivityLocal() override;

	enum { IDD = IDD_BACKSTAGEPAGE_LOG_USERACTIVITY_LOCAL };

	void DoDataExchange(CDataExchange* pDX);

	void SetTheme(const XTPControlTheme nTheme);

	virtual BOOL OnInitDialog() override;
	BOOL OnSetActive();
	BOOL OnKillActive();

	void ClearGrid();

	virtual void SelectionChanged(const CacheGrid& grid) override;

protected:
	DECLARE_MESSAGE_MAP();

private:
	std::shared_ptr<const Sapio365Session> getSession() const;

	afx_msg void OnBnClickedBackstageButtonUseractivityFilterByOwner();
	afx_msg void OnBnClickedBackstageButtonUseractivityFilterClear();

	void updateButtons();

	CXTPRibbonBackstageButton	m_BtnFilterByOwner;
	CXTPRibbonBackstageButton	m_BtnFitlerClear;
	CXTPImageManager			m_ImageList;

	CXTPRibbonBackstageLabel m_staticGridArea;

	LogUserActivityGrid m_Grid;
};