#include "BackstagePanelRoleDelegation.h"

#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "RoleDelegationManager.h"
#include "Sapio365Session.h"
#include "YtriaTranslate.h"

BEGIN_MESSAGE_MAP(BackstagePanelRoleDelegation, BackstagePanelWithGrid<RoleDelegationSelectionGrid>)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_SEERBACINFO,	OnSeeInfo)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_RELOAD,			OnReload)
END_MESSAGE_MAP()

BackstagePanelRoleDelegation::BackstagePanelRoleDelegation() : 
	BackstagePanelWithGrid<RoleDelegationSelectionGrid>(IDD),
	m_ReadyToRoll(false)
{
	m_Grid = std::make_shared<RoleDelegationSelectionGrid>();
}

BackstagePanelRoleDelegation::~BackstagePanelRoleDelegation()
{
}

BOOL BackstagePanelRoleDelegation::OnInitDialog()
{
	BackstagePanelWithGrid<RoleDelegationSelectionGrid>::OnInitDialog();

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_SEERBACINFO };
		m_ImageList.SetIcons(IDB_BTN_ROLEDELEGATION_SEEROLEINFO, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnSeeInfo, YtriaTranslate::Do(BackstagePanelRoleDelegation_OnInitDialog_1, _YLOC("Show Role Details")).c_str(), _YTEXT(""), m_ImageList);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_RELOAD };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_ROLE_RELOAD, ids, 1, CSize(0, 0), xtpImageNormal);
		// BackstagePanelBase::SetupButton(m_BtnReload, YtriaTranslate::Do(Command_ToString_26, _YLOC("Refresh")).c_str(), _YTEXT(""), m_ImageList, false);
		BackstagePanelBase::SetupButton(m_BtnReload, _YTEXT(""), _YTEXT(""), m_ImageList);
		m_BtnReload.SetCheck(BST_UNCHECKED);
		m_BtnReload.EnableWindow();
	}

	SetResize(IDC_BACKSTAGE_BUTTON_ROLE_SEERBACINFO, { 0, 0 }, { .33f, 0 });
	SetResize(IDC_BACKSTAGE_BUTTON_ROLE_RELOAD, XTP_ATTR_HORREPOS(1.f));

	setCaption(YtriaTranslate::Do(BackstagePanelRoleDelegation_OnInitDialog_3, _YLOC("Role-Based Access Control - Available Roles")).c_str());
	updateButton();

	ASSERT(m_Grid);
	if (m_Grid)
		m_Grid->AddSelectionObserver(this);

	return TRUE;
}

void BackstagePanelRoleDelegation::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelWithGrid<RoleDelegationSelectionGrid>::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_SEERBACINFO, m_BtnSeeInfo);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_RELOAD,		m_BtnReload);
	DDX_Control(pDX, IDC_STATIC_ROLEDELEGATIONERROR_TOP,	m_LabelError);
}

void BackstagePanelRoleDelegation::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelWithGrid<RoleDelegationSelectionGrid>::SetTheme(nTheme);
	m_BtnSeeInfo.SetTheme(nTheme);
	m_BtnReload.SetTheme(nTheme);
}

BOOL BackstagePanelRoleDelegation::OnSetActive()
{
	m_Grid->RemoveAllRows();
	SetWindowText(_YTEXT(""));
	
	m_Grid->ShowWindow(SW_HIDE);
	m_BtnSeeInfo.ShowWindow(SW_HIDE);
	m_BtnReload.ShowWindow(SW_HIDE);
	m_LabelError.ShowWindow(SW_SHOW);

	if (BackstagePanelWithGrid<RoleDelegationSelectionGrid>::OnSetActive() == TRUE)
	{
		m_ReadyToRoll = false;

		auto o365App = dynamic_cast<Office365AdminApp*>(AfxGetApp());
		ASSERT(nullptr != o365App);
		wstring licenseStatus;
		if (nullptr != o365App && !o365App->IsLicenseFullOrTrial(licenseStatus))
			m_LabelError.SetWindowText(_YFORMAT(L"This feature is only available with a full or trial license. Your license status: %s", licenseStatus.c_str()).c_str());
		else if (!Office365AdminApp::IsLicense<LicenseTag::RBAC>())
			m_LabelError.SetWindowText(Office365AdminApp::GetLicenseWarnMessage<LicenseTag::RBAC>().c_str());
		else
		{
			m_Grid->ShowWindow(SW_SHOW);
			m_BtnSeeInfo.ShowWindow(SW_SHOW);
			m_BtnReload.ShowWindow(SW_SHOW);

			auto session = getSession();
			if (session)
			{
				m_LabelError.ShowWindow(SW_HIDE);
				m_ReadyToRoll = true;
				m_Grid->SetIsActive(true);
				m_Grid->SetSession(session);
				m_Grid->SetRoleDelegationID(session->GetRoleDelegationID());
				OnReload();
			}
			else
				m_LabelError.SetWindowText(_T("Please activate a session."));
		}

		return TRUE;
	}

	return FALSE;
}

BOOL BackstagePanelRoleDelegation::OnKillActive()
{
	if (BackstagePanelWithGrid<RoleDelegationSelectionGrid>::OnKillActive() == TRUE)
	{
		if (m_ReadyToRoll)
			m_Grid->SetIsActive(false);
		return TRUE;
	}

	return FALSE;
}

void BackstagePanelRoleDelegation::updateButton()
{
	m_BtnSeeInfo.EnableWindow(m_Grid && m_Grid->GetRoleDelegationIDFromSelection() > 0);
}

std::shared_ptr<Sapio365Session> BackstagePanelRoleDelegation::getSession() const
{
	return MainFrameSapio365Session();
}

void BackstagePanelRoleDelegation::OnSeeInfo()
{
	ASSERT(nullptr != m_Grid);
	if (nullptr != m_Grid)
	{
		auto RBACid = m_Grid->GetRoleDelegationIDFromSelection();
		ASSERT(RBACid > 0);
		if (RBACid > 0)
		{
			DlgRoleSeeInfo dlgRoleSeeInfo(RBACid, getSession(), this);
			dlgRoleSeeInfo.DoModal();
		}
	}
}

void BackstagePanelRoleDelegation::manageSession(const bool p_Connect)
{
	auto session = getSession();
	ASSERT(session);
	ASSERT(m_Grid);
	if (session && m_Grid)
	{	
		if (p_Connect)
			session->SetUseRoleDelegation(m_Grid->GetRoleDelegationIDFromSelection(), this);
		else
			session->SetUseRoleDelegation(0, nullptr);
	}
}

void BackstagePanelRoleDelegation::OnReload()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		CWaitCursor c;

		RoleDelegationManager::GetInstance().UpdateSQLiteFromCloud(getSession(), this);
		m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
		updateButton();
	}
}

void BackstagePanelRoleDelegation::SelectionChanged(const CacheGrid& grid)
{
	updateButton();
}
