#include "DlgScheduleConfigHTML.h"

#include "TimeUtil.h"
#include "WindowsScheduledTask.h"

namespace
{
	static const wstring g_StartDate = _YTEXT("StartDate");
	static const wstring g_StartTime = _YTEXT("StartTime");
	static const wstring g_Schedule	 = _YTEXT("Schedule");

	static const wstring g_DayFrequency = _YTEXT("DayFrequency");
	static const wstring g_WeekFrequency = _YTEXT("WeekFrequency");
	static const wstring g_WeekDay = _YTEXT("WeekDay");
	static const wstring g_MonthDay = _YTEXT("MonthDay");
	static const wstring g_LastDay = _YTEXT("LastDay");

	//static const wstring g_ScheduleOnce		= _YTEXT("01Once");
	//static const wstring g_ScheduleOnStart	= _YTEXT("02OnStart");
	//static const wstring g_ScheduleOnIdle	= _YTEXT("03OnIdle");
	//static const wstring g_ScheduleOnLogon	= _YTEXT("04OnLogon");
	static const wstring g_ScheduleMinute	= _YTEXT("05Minute");
	static const wstring g_ScheduleHourly	= _YTEXT("06Hourly");
	static const wstring g_ScheduleDaily	= _YTEXT("07Daily");
	static const wstring g_ScheduleWeekly	= _YTEXT("08Weekly");
	static const wstring g_ScheduleMonthly	= _YTEXT("09Monthly");

	static const wstring g_Name = _YTEXT("Name");

}

DlgScheduleConfigHTML::DlgScheduleConfigHTML(const wstring& p_DefaultDisplayName, const wstring& p_JobTitle, CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
	, m_DisplayName(p_DefaultDisplayName)
	, m_JobTitle(p_JobTitle)
{

}

void DlgScheduleConfigHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgScheduleConfigHTML"));

	
	getGenerator().addIconTextField(_YTEXT("STATIC_INTRO1"), _YFORMAT(L"Scheduling job '%s'", m_JobTitle.c_str()), _T("far fa-info-circle"));
	getGenerator().addIconTextField(_YTEXT("STATIC_INTRO2"), _T("Fill in the scheduler settings below, and then the settings required to run the job (i.e. which session to use, notification settings, etc�).<br>sapio365 will then create a scheduled task in the Windows Task Scheduler for the selected job.")
											, _T("far fa-angle-right"));

	getGenerator().Add(StringEditor(g_Name, _T("Reference Name"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, m_DisplayName));

	getGenerator().Add(Expanded<AsRadio<CheckListEditor>>({ g_Schedule, _T("Schedule Type"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, { g_ScheduleDaily } },
		{
			{ _T("Daily")	, g_ScheduleDaily },
			//{ _T("Minute")	, g_ScheduleMinute },
			{ _T("Monthly")	, g_ScheduleMonthly },
			//{ _T("On Start"), g_ScheduleOnStart },
			//{ _T("On Idle")	, g_ScheduleOnIdle },
			//{ _T("On Logon"), g_ScheduleOnLogon },
			//{ _T("Once")	, g_ScheduleOnce },
			//{ _T("Hourly")	, g_ScheduleHourly },
			{ _T("Weekly")	, g_ScheduleWeekly },
		}));

	{
		const auto currentTD = YTimeDate::GetCurrentTimeDate();
		const auto dateStr = TimeUtil::GetInstance().GetISO8601String(currentTD, TimeUtil::ISO8601_HAS_DATE, false);

		wostringstream timeStr;
		timeStr << std::setfill(_YTEXT('0')) << std::setw(2) << currentTD.getHour()
			<< _YTEXT(':') << std::setfill(_YTEXT('0')) << std::setw(2) << currentTD.getMin();

		getGenerator().Add(DateEditor({ g_StartDate, _T("Start Date"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, dateStr }, boost::none));
		getGenerator().Add(TimeEditor(g_StartTime, _T("Start Time"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, timeStr.str()));
	}												 

	getGenerator().Add(IntegerEditor({ g_DayFrequency, _T("Recur every (x) days"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, 1 }, { 1, 365 }));
	getGenerator().Add(IntegerEditor({ g_WeekFrequency, _T("Recur every (x) weeks"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, 1 }, { 1, 52 }));
	getGenerator().revertLastHbsHeightIncrease(); // Both editors share the same space

	getGenerator().Add(Expanded<CheckListEditor>({ g_WeekDay, _T("Recur on"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, { Str::g_EmptyString } },
		{
			{ _T("Sunday")		, Str::getStringFromNumber(TriggerDays::SUN, 2) },
			{ _T("Monday")		, Str::getStringFromNumber(TriggerDays::MON, 2) },
			{ _T("Tuesday")		, Str::getStringFromNumber(TriggerDays::TUE, 2) },
			{ _T("Wednesday")	, Str::getStringFromNumber(TriggerDays::WED, 2) },
			{ _T("Thursday")	, Str::getStringFromNumber(TriggerDays::THU, 2) },
			{ _T("Friday")		, Str::getStringFromNumber(TriggerDays::FRI, 2) },
			{ _T("Saturday")	, Str::getStringFromNumber(TriggerDays::SAT, 2) },
		}));

	getGenerator().Add(Expanded<AsRadio<CheckListEditor>>({ g_MonthDay, _T("Recur on"), EditorFlags::DIRECT_EDIT | EditorFlags::REQUIRED, { Str::getStringFromNumber(1, 2) }  },
		{
			{ _YTEXT("1")		, Str::getStringFromNumber(1, 2) },
			{ _YTEXT("2")		, Str::getStringFromNumber(2, 2) },
			{ _YTEXT("3")		, Str::getStringFromNumber(3, 2) },
			{ _YTEXT("4")		, Str::getStringFromNumber(4, 2) },
			{ _YTEXT("5")		, Str::getStringFromNumber(5, 2) },
			{ _YTEXT("6")		, Str::getStringFromNumber(6, 2) },
			{ _YTEXT("7")		, Str::getStringFromNumber(7, 2) },
			{ _YTEXT("9")		, Str::getStringFromNumber(9, 2) },
			{ _YTEXT("10")		, Str::getStringFromNumber(10)	 },
			{ _YTEXT("11")		, Str::getStringFromNumber(11)	 },
			{ _YTEXT("12")		, Str::getStringFromNumber(12)	 },
			{ _YTEXT("13")		, Str::getStringFromNumber(13)	 },
			{ _YTEXT("14")		, Str::getStringFromNumber(14)	 },
			{ _YTEXT("15")		, Str::getStringFromNumber(15)	 },
			{ _YTEXT("16")		, Str::getStringFromNumber(16)	 },
			{ _YTEXT("17")		, Str::getStringFromNumber(17)	 },
			{ _YTEXT("18")		, Str::getStringFromNumber(18)	 },
			{ _YTEXT("19")		, Str::getStringFromNumber(19)	 },
			{ _YTEXT("20")		, Str::getStringFromNumber(20)	 },
			{ _YTEXT("21")		, Str::getStringFromNumber(21)	 },
			{ _YTEXT("22")		, Str::getStringFromNumber(22)	 },
			{ _YTEXT("23")		, Str::getStringFromNumber(23)	 },
			{ _YTEXT("24")		, Str::getStringFromNumber(24)	 },
			{ _YTEXT("25")		, Str::getStringFromNumber(25)	 },
			{ _YTEXT("26")		, Str::getStringFromNumber(26)	 },
			{ _YTEXT("27")		, Str::getStringFromNumber(27)	 },
			{ _YTEXT("28")		, Str::getStringFromNumber(28)	 },
			{ _YTEXT("29")		, Str::getStringFromNumber(29)	 },
			{ _YTEXT("30")		, Str::getStringFromNumber(30)	 },
			{ _YTEXT("31")		, Str::getStringFromNumber(31)	 },
			{ _T("Last Day"),	g_LastDay						 },
		}));
	getGenerator().revertLastHbsHeightIncrease(); // Share the same space as g_WeekDay

	getGenerator().addEvent(g_Schedule,	{ g_DayFrequency,		EventTarget::g_HideIfNotEqual,	g_ScheduleDaily });

	getGenerator().addEvent(g_Schedule,	{ g_WeekFrequency,		EventTarget::g_HideIfNotEqual,	g_ScheduleWeekly });
	getGenerator().addEvent(g_Schedule,	{ g_WeekDay,			EventTarget::g_HideIfNotEqual,	g_ScheduleWeekly });

	//getGenerator().addEvent(g_Schedule,	{ g_MonthFrequency,		EventTarget::g_HideIfNotEqual,	g_ScheduleMonthly });
	getGenerator().addEvent(g_Schedule,	{ g_MonthDay,			EventTarget::g_HideIfNotEqual,	g_ScheduleMonthly });
	/*getGenerator().addEvent(g_Schedule,	{ g_LastDay,			EventTarget::g_HideIfNotEqual,	g_ScheduleMonthly });
	getGenerator().addEvent(g_LastDay,		{ g_MonthDay,			EventTarget::g_DisableIfEqual,	UsefulStrings::g_ToggleValueTrue });*/

	/*getGenerator().addEvent(g_Schedule,	{ g_StartTime,			EventTarget::g_HideIfEqual,	g_ScheduleOnLogon });
	getGenerator().addEvent(g_Schedule,	{ g_StartTime,			EventTarget::g_HideIfEqual,	g_ScheduleOnStart });
	addEvent(g_Schedule,	{ g_StartDate,			EventTarget::g_HideIfEqual,	g_ScheduleOnLogon });
	addEvent(g_Schedule,	{ g_StartDate,			EventTarget::g_HideIfEqual,	g_ScheduleOnStart });*/

	getGenerator().addApplyButton(_T("Next..."));
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str());
}

bool DlgScheduleConfigHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	boost::YOpt<YTimeDate> startDateTime;
	wstring scheduleType;
	wstring frequency;

	TriggerDays triggerDays;
	TriggerMonthDays triggerMonthDays;
	bool lastDay = false;

	for (const auto& item : data)
	{
		if (g_Name == item.first)
		{
			m_DisplayName = item.second;
		}
		else if (g_Schedule == item.first)
		{
			scheduleType = item.second;
		}
		else if (g_StartDate == item.first)
		{
			YTimeDate td;
			const auto success = TimeUtil::GetInstance().ConvertTextToDate(item.second, td);
			ASSERT(success && td.HasDate() && !td.HasTime());

			if (startDateTime)
			{
				auto sysTime = startDateTime->GetSystemTime();
				sysTime.wDay = td.GetSystemTime().wDay;
				sysTime.wDayOfWeek = td.GetSystemTime().wDayOfWeek;
				sysTime.wMonth = td.GetSystemTime().wMonth;
				sysTime.wYear = td.GetSystemTime().wYear;

				startDateTime = YTimeDate(sysTime, YTimeDate::GetLocalTimeZoneOffset());
			}
			else
			{
				startDateTime = td;
			}
		}
		else if (g_StartTime == item.first)
		{
			YTimeDate td;
			const auto success = TimeUtil::GetInstance().ConvertTextToDate(item.second, td);
			ASSERT(success && !td.HasDate() && td.HasTime());
			
			if (startDateTime)
			{
				auto sysTime = startDateTime->GetSystemTime();
				sysTime.wHour = td.GetSystemTime().wHour;
				sysTime.wMinute = td.GetSystemTime().wMinute;
				sysTime.wSecond = td.GetSystemTime().wSecond;
				sysTime.wMilliseconds = td.GetSystemTime().wMilliseconds;

				startDateTime = YTimeDate(sysTime, YTimeDate::GetLocalTimeZoneOffset());
			}
			else
			{
				startDateTime = td;
			}
		}
		else if (g_DayFrequency == item.first || g_WeekFrequency == item.first/* || g_MonthFrequency == item.first*/)
		{
			ASSERT(frequency.empty());
			frequency = item.second;
		}
		else if (g_WeekDay == item.first)
		{
			triggerDays += TriggerDays(static_cast<int>(Str::GetNumber(item.second)));
		}
		else if (g_MonthDay == item.first)
		{
			ASSERT(!triggerMonthDays.IsSet() && !lastDay);
			if (item.second == g_LastDay)
				lastDay = true;
			else
				triggerMonthDays = TriggerMonthDays(static_cast<int>(Str::GetNumber(item.second)));
		}
		/*else if (g_LastDay == item.first)
		{
			lastDay = Str::getBoolFromString(item.second);
		}*/
	}

	ASSERT(!scheduleType.empty());
	if (!scheduleType.empty())
	{
		ASSERT(!m_TaskSchedule);
		/*if (g_ScheduleOnce == scheduleType)
			m_TaskSchedule = new TaskScheduleOnce();
		else if (g_ScheduleOnStart == scheduleType)
			m_TaskSchedule.reset(new TaskScheduleOnStart());
		else if (g_ScheduleOnIdle == scheduleType)
			m_TaskSchedule.reset(new TaskScheduleOnIdle());
		else if (g_ScheduleOnLogon == scheduleType)
			m_TaskSchedule.reset(new TaskScheduleOnLogon());
		else if (g_ScheduleMinute == scheduleType)
			m_TaskSchedule.reset(new TaskScheduleMinute());
		else if (g_ScheduleHourly == scheduleType)
			m_TaskSchedule.reset(new TaskScheduleHourly());
		else */if (g_ScheduleDaily == scheduleType && !frequency.empty())
			m_TaskSchedule.reset(new TaskScheduleDaily(static_cast<int>(Str::GetNumber(frequency)), startDateTime));
		else if (g_ScheduleWeekly == scheduleType && !frequency.empty() && triggerDays.IsSet())
			m_TaskSchedule.reset(new TaskScheduleWeekly(static_cast<int>(Str::GetNumber(frequency)), triggerDays, startDateTime));
		else if (g_ScheduleMonthly == scheduleType)
		{
			/*if (!frequency.empty())
			{
				const auto freq = static_cast<int>(Str::GetNumber(frequency));
				if (lastDay)
					m_TaskSchedule.reset(new TaskScheduleMonthly(freq, lastDay));
				else if (triggerMonthDays.IsSet())
					m_TaskSchedule.reset(new TaskScheduleMonthly(freq, triggerMonthDays));
				else
					m_TaskSchedule.reset(new TaskScheduleMonthly(freq));
			}
			else */if (lastDay)
				m_TaskSchedule.reset(new TaskScheduleMonthly(TriggerMonths(TriggerMonths::ALL), true, startDateTime));
			else if (triggerMonthDays.IsSet())
				m_TaskSchedule.reset(new TaskScheduleMonthly(TriggerMonths(TriggerMonths::ALL), triggerMonthDays, startDateTime));
		}
	}

	return true;
}

wstring DlgScheduleConfigHTML::getDialogTitle() const
{
	return _T("Schedule Task");
}

const std::unique_ptr<TaskSchedule>& DlgScheduleConfigHTML::GetTaskSchedule() const
{
	return m_TaskSchedule;
}

const wstring& DlgScheduleConfigHTML::GetDisplayName() const
{
	return m_DisplayName;
}
