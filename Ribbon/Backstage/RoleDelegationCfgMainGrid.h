#pragma once

#include "CacheGrid.h"
#include "FrameLogger.h"

class RoleDelegationCfgMainGrid : public CacheGrid
{
public:
	RoleDelegationCfgMainGrid();

	void SetIsActive(const bool p_Active);
	void LoadRoleDelegations();

protected:
	virtual void customizeGrid() override;

private:
	bool m_Active;
};