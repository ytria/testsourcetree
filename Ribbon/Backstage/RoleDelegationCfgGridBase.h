#pragma once

#include "SQLiteGrid.h"

class RoleDelegationCfgGridBase : public SQLiteGrid
{
public:
	using OnDoubleClickRowFunction = std::function<void()>;
	RoleDelegationCfgGridBase();

	void SetOnDoubleClickRowFunction(OnDoubleClickRowFunction p_OnDoubleClickRowFunction);
	
protected:
	virtual void customizeGridPostProcess() override;
	virtual void customizeGridPostProcessSpecific() = 0;
	virtual void loadPostProcess(GridBackendRow* p_Row) override;
	virtual void loadPostProcessSpecific(GridBackendRow* p_Row) = 0;

	virtual void prepareSQLoad() override;
	virtual void prepareSQLoadSpecific() = 0;
	virtual bool finalizeSQLoad() override;

	virtual void OnCustomDoubleClick() override;

	GridBackendColumn* m_ColumnStatus;
	GridBackendColumn* m_ColumnName;
	GridBackendColumn* m_ColumnInfo;
	GridBackendColumn* m_ColumnUpdDate;
	GridBackendColumn* m_ColumnUpdID;
	GridBackendColumn* m_ColumnUpdName;
	GridBackendColumn* m_ColumnUpdPrincipalName;

	uint32_t m_ColumnSize1;
	uint32_t m_ColumnSize3;
	uint32_t m_ColumnSize12;
	uint32_t m_ColumnSize22;
	uint32_t m_ColumnSize32;

	OnDoubleClickRowFunction m_OnDoubleClickRowFunction;
};