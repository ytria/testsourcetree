#include "BackstagePanelSettingsContent.h"

#include "BaseO365Grid.h"
#include "CodeJockThemed.h"
#include "HistoryMode.h"
#include "ModuleBase.h"
#include "NetworkUtils.h"
#include "Sapio365Settings.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTranslate.h"

BEGIN_MESSAGE_MAP(BackstagePanelSettingsContent, CExtResizableDialog)
	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_USE_CACHE_YES, ID_BACKSTAGE_SETTINGS_USE_CACHE_ASK, OnUseCache)
	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_AUTORELOAD_YES, ID_BACKSTAGE_SETTINGS_AUTORELOAD_NO, OnAutoReload)
	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_YES, ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_NO, OnEnableAutoLoadLastSession)

	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLASK, OnHideDLGRAGroupsL)
	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSASK, OnHideDLGRAGroups)
	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAASK, OnHideDLGRAUsersUA)
	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSASK, OnHideDLGRAUsers)
	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTASK, OnHideDLGRAConsent)
	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDYES, ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDASK, OnHideUAdminDeprecated)

	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_NEW, ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_ASK_EVERYTIME, OnRadioHistoryMode)

	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKYES, ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKNO, OnFlatViewGroupByHPK)
	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPYES, ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPNO, OnFlatViewHideHTop)
	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPYES, ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPNO, OnFlatViewAskForceUponGroup)
	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSYES, ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSNO, OnHierarchyShowHideDescendantCounters)

	ON_CONTROL_RANGE(BN_CLICKED, ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONYES, ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONASK, OnRadioExplodeAllWhenShortSelection)

	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
#if ENABLE_DRAGGING
 	ON_WM_LBUTTONDOWN()
 	ON_WM_LBUTTONUP()
 	ON_WM_MOUSEMOVE()
 	ON_WM_KILLFOCUS()
#endif

END_MESSAGE_MAP()

BackstagePanelSettingsContent::BackstagePanelSettingsContent(CWnd * pParent)
	: CExtResizableDialog(IDD_BACKSTAGEPANELSETTINGSCONTENT)
	, m_nScrollPos(0)
#if ENABLE_DRAGGING
	, m_bDragging(false)
#endif
{
	Create(IDD_BACKSTAGEPANELSETTINGSCONTENT, pParent);
}

void BackstagePanelSettingsContent::DoDataExchange(CDataExchange* pDX)
{
	// GLOBAL
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_GLOBAL, m_BoxGlobal);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_USE_CACHE_LBL, m_LabelUseCache);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_USE_CACHE_YES, m_RadioUseCacheYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_USE_CACHE_NO, m_RadioUseCacheNo);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_USE_CACHE_ASK, m_RadioUseCacheAsk);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_LBL, m_LabelAutoLoadLastSession);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_YES, m_RadioAutoLoadLastSessionYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_NO, m_RadioAutoLoadLastSessionNo);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_AUTORELOAD_LBL, m_LabelAutoReload);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_AUTORELOAD_YES, m_RadioAutoReloadYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_AUTORELOAD_NO, m_RadioAutoReloadNo);

	// REMINDERS
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_REMINDERS, m_BoxReminders);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLBOX, m_LabelHideDLGRAGroupsL);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLYES, m_RadioHideDLGRAGroupsLYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLNO, m_RadioHideDLGRAGroupsLNo);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLASK, m_RadioHideDLGRAGroupsLAsk);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSBOX, m_LabelHideDLGRAGroups);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSYES, m_RadioHideDLGRAGroupsYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSNO, m_RadioHideDLGRAGroupsNo);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSASK, m_RadioHideDLGRAGroupsAsk);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUABOX, m_LabelHideDLGRAUsersUA);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAYES, m_RadioHideDLGRAUsersUAYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUANO, m_RadioHideDLGRAUsersUANo);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAASK, m_RadioHideDLGRAUsersUAAsk);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSBOX, m_LabelHideDLGRAUsers);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSYES, m_RadioHideDLGRAUsersYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSNO, m_RadioHideDLGRAUsersNo);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSASK, m_RadioHideDLGRAUsersAsk);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTBOX, m_LabelHideDLGRAConsent);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTYES, m_RadioHideDLGRAConsentYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTNO, m_RadioHideDLGRAConsentNo);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTASK, m_RadioHideDLGRAConsentAsk);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDBOX, m_LabelHideUltraAdminDeprecated);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDYES, m_RadioHideUltraAdminDeprecatedYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDNO, m_RadioHideUltraAdminDeprecatedNo);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDASK, m_RadioHideUltraAdminDeprecatedAsk);
	////////////////

	// HISTORY MODE
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HISTORYMODE, m_BoxHistoryMode);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_NEW, m_RadioNewHistory);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_DETACH, m_RadioDetachHistory);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_SAME, m_RadioRetainHistory);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_ASK_EVERYTIME, m_RadioAskEveryTime);
	////////////////

	// GRID OPTIONS
	DDX_Control(pDX, IDC_GROUP_GRIDOPTIONS, m_GridOptionBox);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPK, m_LabelFlatViewGroupByHPK);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKYES, m_RadioFlatViewGroupByHPKYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKNO, m_RadioFlatViewGroupByHPKNo);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOP, m_LabelFlatViewHideTopRows);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPYES, m_RadioFlatViewHideTopRowsYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPNO, m_RadioFlatViewHideTopRowsNo);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUP, m_LabelFlatViewAskForceUponGrouping);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPYES, m_RadioFlatViewAskForceUponGroupingYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPNO, m_RadioFlatViewAskForceUponGroupingNo);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERS, m_LabelHierarchyShowHideDescendantCounters);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSYES, m_RadioHierarchyShowHideDescendantCountersYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSNO, m_RadioHierarchyShowHideDescendantCountersNo);

	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTION, m_LabelExplodeAllWShortSelection);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONYES, m_RadioExplodeAllWShortSelectionYes);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONNO, m_RadioExplodeAllWShortSelectionNo);
	DDX_Control(pDX, ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONASK, m_RadioExplodeAllWShortSelectionAsk);
	////////////////
}

void BackstagePanelSettingsContent::Reset()
{
	//Initializing the preferences :

	// Kind of a hack. There's one CreateNewFrameSetting and one HideNewWindowDialogSetting per module.
	// Instead of trying to list all the possible modules, just remove the parent Key
	// WARNING: will also delete UseCacheSetting, AutoLoadLastSessionSetting and AutoReloadSetting
	AllNewWindowKeySettings().Reset();

	UseCacheSetting().Reset();
	initUseCache();

	AutoLoadLastSessionSetting().Reset();
	initEnableAutoLoadLastSession();

	AutoReloadSetting().Reset();
	initAutoReload();

	HideGroupLicenseRestrictedAccessDialogSetting().Reset();
	initHideDLGRAGroupsL();

	HideGroupAdminRestrictedAccessDialogSetting().Reset();
	initHideDLGRAGroups();

	HideUltraAdminRequirementDialogSetting().Reset();
	initHideDLGRAUsersUA();

	HideUserPermissionRequirementDialogSetting().Reset();
	initHideDLGRAUsers();

	HideConsentReminderDialogSetting().Reset();
	initHideDLGRAConsent();

	HideUltraAdminDeprecatedReminderSetting().Reset();
	initHideUAdminDeprecated();

	HistoryModeSetting().Reset();
	initHistoryMode();

	//Grid preferences : 
	m_GridOptions.resetAll();
	initFlatViewGroupByHPK();
	initViewHideHTop();
	initViewAskForceUponGroup();
	initShowHideHierarchyCounters();
	initExplodeAllWhenShortSelection();
}

void BackstagePanelSettingsContent::SetTheme()
{
	auto setBkColor = [](auto& c)
	{
		c.SetBkColor(RGB(255, 255, 255));
	};
	
	setBkColor(*this);
	setBkColor(m_BoxGlobal);
	setBkColor(m_LabelUseCache);
	setBkColor(m_RadioUseCacheYes);
	setBkColor(m_RadioUseCacheNo);
	setBkColor(m_RadioUseCacheAsk);
	setBkColor(m_LabelAutoLoadLastSession);
	setBkColor(m_RadioAutoLoadLastSessionYes);
	setBkColor(m_RadioAutoLoadLastSessionNo);
	setBkColor(m_LabelAutoReload);
	setBkColor(m_RadioAutoReloadYes);
	setBkColor(m_RadioAutoReloadNo);
	setBkColor(m_BoxReminders);
	setBkColor(m_LabelHideDLGRAGroupsL);
	setBkColor(m_RadioHideDLGRAGroupsLYes);
	setBkColor(m_RadioHideDLGRAGroupsLNo);
	setBkColor(m_RadioHideDLGRAGroupsLAsk);
	setBkColor(m_LabelHideDLGRAGroups);
	setBkColor(m_RadioHideDLGRAGroupsYes);
	setBkColor(m_RadioHideDLGRAGroupsNo);
	setBkColor(m_RadioHideDLGRAGroupsAsk);
	setBkColor(m_LabelHideDLGRAUsersUA);
	setBkColor(m_RadioHideDLGRAUsersUAYes);
	setBkColor(m_RadioHideDLGRAUsersUANo);
	setBkColor(m_RadioHideDLGRAUsersUAAsk);
	setBkColor(m_LabelHideDLGRAUsers);
	setBkColor(m_RadioHideDLGRAUsersYes);
	setBkColor(m_RadioHideDLGRAUsersNo);
	setBkColor(m_RadioHideDLGRAUsersAsk);
	setBkColor(m_LabelHideDLGRAConsent);
	setBkColor(m_RadioHideDLGRAConsentYes);
	setBkColor(m_RadioHideDLGRAConsentNo);
	setBkColor(m_RadioHideDLGRAConsentAsk);
	setBkColor(m_LabelHideUltraAdminDeprecated);
	setBkColor(m_RadioHideUltraAdminDeprecatedYes);
	setBkColor(m_RadioHideUltraAdminDeprecatedNo);
	setBkColor(m_RadioHideUltraAdminDeprecatedAsk);
	setBkColor(m_BoxHistoryMode);
	setBkColor(m_RadioAskEveryTime);
	setBkColor(m_RadioNewHistory);
	setBkColor(m_RadioDetachHistory);
	setBkColor(m_RadioRetainHistory);
	setBkColor(m_GridOptionBox);
	setBkColor(m_LabelFlatViewGroupByHPK);
	setBkColor(m_RadioFlatViewGroupByHPKYes);
	setBkColor(m_RadioFlatViewGroupByHPKNo);
	setBkColor(m_LabelFlatViewHideTopRows);
	setBkColor(m_RadioFlatViewHideTopRowsYes);
	setBkColor(m_RadioFlatViewHideTopRowsNo);
	setBkColor(m_LabelFlatViewAskForceUponGrouping);
	setBkColor(m_RadioFlatViewAskForceUponGroupingYes);
	setBkColor(m_RadioFlatViewAskForceUponGroupingNo);
	setBkColor(m_LabelHierarchyShowHideDescendantCounters);
	setBkColor(m_RadioHierarchyShowHideDescendantCountersYes);
	setBkColor(m_RadioHierarchyShowHideDescendantCountersNo);
	setBkColor(m_LabelExplodeAllWShortSelection);
	setBkColor(m_RadioExplodeAllWShortSelectionYes);
	setBkColor(m_RadioExplodeAllWShortSelectionNo);
	setBkColor(m_RadioExplodeAllWShortSelectionAsk);
}

BOOL BackstagePanelSettingsContent::OnInitDialog()
{
	CExtResizableDialog::OnInitDialog();

	// save the original size for scrolling
	GetWindowRect(m_rcOriginalRect);

	// GLOBAL
	m_BoxGlobal.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_1, _YLOC("Global")).c_str());

	m_LabelUseCache.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_2, _YLOC("Use Cache")).c_str());
	m_RadioUseCacheYes.SetWindowText(_T("Always"));
	m_RadioUseCacheNo.SetWindowText(YtriaTranslate::Do(DlgAdvancedProperties_OnInitDialog_5, _YLOC("Never")).c_str());
	m_RadioUseCacheAsk.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_5, _YLOC("Ask Once")).c_str());
	initUseCache();

	m_LabelAutoLoadLastSession.SetWindowText(YtriaTranslate::Do(BackstagePanelSettings_initEnableAutoLoadLastSession_1, _YLOC("Automatically reload last session on launch")).c_str());
	m_RadioAutoLoadLastSessionYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioAutoLoadLastSessionNo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	initEnableAutoLoadLastSession();

	m_LabelAutoReload.SetWindowText(YtriaTranslate::Do(BackstagePanelSettings_initAutoReload_1, _YLOC("Automatically reload existing windows")).c_str());
	m_RadioAutoReloadYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioAutoReloadNo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	initAutoReload();


	// REMINDERS
	m_BoxReminders.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_10, _YLOC("Reminders")).c_str());
	m_LabelHideDLGRAGroupsL.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_11, _YLOC("Show license requirements reminder (groups)")).c_str());
	m_RadioHideDLGRAGroupsLAsk.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_12, _YLOC("Ask everytime")).c_str());
	m_RadioHideDLGRAGroupsLYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioHideDLGRAGroupsLNo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	initHideDLGRAGroupsL();

	m_LabelHideDLGRAGroups.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_33, _YLOC("Show 'Advanced Session' graph API restrictions reminder (groups)")).c_str());
	m_RadioHideDLGRAGroupsAsk.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_12, _YLOC("Ask everytime")).c_str());
	m_RadioHideDLGRAGroupsYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioHideDLGRAGroupsNo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	initHideDLGRAGroups();

	m_LabelHideDLGRAUsersUA.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_46, _YLOC("Show 'Advanced Session' graph API restrictions reminder (users)")).c_str());
	m_RadioHideDLGRAUsersUAAsk.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_12, _YLOC("Ask everytime")).c_str());
	m_RadioHideDLGRAUsersUAYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioHideDLGRAUsersUANo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	initHideDLGRAUsersUA();

	m_LabelHideDLGRAUsers.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_45, _YLOC("Show 'Advanced Session' permissions reminder")).c_str());
	m_RadioHideDLGRAUsersAsk.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_12, _YLOC("Ask everytime")).c_str());
	m_RadioHideDLGRAUsersYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioHideDLGRAUsersNo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	initHideDLGRAUsers();

	m_LabelHideDLGRAConsent.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_44, _YLOC("Show Consent Reminder")).c_str());
	m_RadioHideDLGRAConsentAsk.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_12, _YLOC("Ask everytime")).c_str());
	m_RadioHideDLGRAConsentYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioHideDLGRAConsentNo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	initHideDLGRAConsent();

	m_LabelHideUltraAdminDeprecated.SetWindowText(_T("Show Ultra Admin deprecated reminder"));
	m_RadioHideUltraAdminDeprecatedAsk.SetWindowText(_T("Ask everytime"));
	m_RadioHideUltraAdminDeprecatedYes.SetWindowText(_T("Yes"));
	m_RadioHideUltraAdminDeprecatedNo.SetWindowText(_T("No"));
	initHideUAdminDeprecated();

	// HISTORY MODE
	m_BoxHistoryMode.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_43, _YLOC("History Mode")).c_str());
	m_RadioAskEveryTime.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_12, _YLOC("Ask everytime")).c_str());
	m_RadioNewHistory.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_42, _YLOC("New History")).c_str());
	m_RadioDetachHistory.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_41, _YLOC("Detach History")).c_str());
	m_RadioRetainHistory.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_40, _YLOC("Retain History")).c_str());
	initHistoryMode();

	m_GridOptionBox.SetWindowText(YtriaTranslate::Do(BackstagePanelSettings_OnInitDialog_3, _YLOC("Grid preferences")).c_str());

	m_LabelFlatViewGroupByHPK.SetWindowText(YtriaTranslate::Do(BackstagePanelSettings_initFlatViewGroupByHPK_1, _YLOC("Automatically categorise by hierarchy key when switching to flat view")).c_str());
	m_RadioFlatViewGroupByHPKYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioFlatViewGroupByHPKNo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	initFlatViewGroupByHPK();

	m_LabelFlatViewHideTopRows.SetWindowText(YtriaTranslate::Do(BackstagePanelSettings_initViewHideHTop_1, _YLOC("Hide top-level rows when switching to flat view")).c_str());//-Flatten/remove hierarchy ranking of rows in flat view
	m_RadioFlatViewHideTopRowsYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioFlatViewHideTopRowsNo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	initViewHideHTop();

	m_LabelFlatViewAskForceUponGrouping.SetWindowText(YtriaTranslate::Do(BackstagePanelSettings_initViewAskForceUponGroup_1, _YLOC("Ask for confirmation before grouping in hierarchy view")).c_str());
	m_RadioFlatViewAskForceUponGroupingYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioFlatViewAskForceUponGroupingNo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	initViewAskForceUponGroup();

	m_LabelHierarchyShowHideDescendantCounters.SetWindowText(YtriaTranslate::Do(BackstagePanelSettings_initShowHideHierarchyCounters_1, _YLOC("Automatically re-order descendant columns according to their count")).c_str());
	m_RadioHierarchyShowHideDescendantCountersYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioHierarchyShowHideDescendantCountersNo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	initShowHideHierarchyCounters();

	m_LabelExplodeAllWShortSelection.SetWindowText(YtriaTranslate::Do(BackstagePanelSettings_initExplodeAllWhenShortSelection_1, _YLOC("Explode all multivalues in grid when only one or fewer rows is selected")).c_str());
	m_RadioExplodeAllWShortSelectionYes.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_1, _YLOC("Yes")).c_str());
	m_RadioExplodeAllWShortSelectionNo.SetWindowText(YtriaTranslate::Do(DlgMessageBox_OnInitDialog_2, _YLOC("No")).c_str());
	m_RadioExplodeAllWShortSelectionAsk.SetWindowText(YtriaTranslate::Do(BackstagePanelSettingsContent_OnInitDialog_12, _YLOC("Ask Everytime")).c_str());
	initExplodeAllWhenShortSelection();

	//AddAnchor(m_BtnClose, CSize(80, 100), CSize(100, 100));

	return TRUE;  // return TRUE unless you set the focus to a control
}

void BackstagePanelSettingsContent::SetActive()
{
	// in case options were modified outside the panel

	initUseCache();
	initAutoReload();
	initEnableAutoLoadLastSession();
	initHistoryMode();
	initHideDLGRAGroupsL();
	initHideDLGRAGroups();
	initHideDLGRAUsersUA();
	initHideDLGRAUsers();
	initHideDLGRAConsent();
	initFlatViewGroupByHPK();
	initViewHideHTop();
	initViewAskForceUponGroup();
	initShowHideHierarchyCounters();
	initExplodeAllWhenShortSelection();

	ValidateRect(m_rcOriginalRect);
}

void BackstagePanelSettingsContent::OnUseCache(UINT p_Id)
{
	UseCacheSetting().Set(p_Id == ID_BACKSTAGE_SETTINGS_USE_CACHE_YES);
	CheckRadioButton(ID_BACKSTAGE_SETTINGS_USE_CACHE_YES, ID_BACKSTAGE_SETTINGS_USE_CACHE_ASK, p_Id);
	m_RadioUseCacheAsk.ShowWindow(SW_HIDE);
}

void BackstagePanelSettingsContent::OnAutoReload(UINT p_Id)
{
	if (p_Id == ID_BACKSTAGE_SETTINGS_AUTORELOAD_YES)
	{
		AutoReloadSetting().Set(true);
	}
	else
	{
		ASSERT(p_Id == ID_BACKSTAGE_SETTINGS_AUTORELOAD_NO);
		AutoReloadSetting().Set(false);
	}
	CheckRadioButton(ID_BACKSTAGE_SETTINGS_AUTORELOAD_YES, ID_BACKSTAGE_SETTINGS_AUTORELOAD_NO, p_Id);
}

void BackstagePanelSettingsContent::OnRadioHistoryMode(UINT p_Id)
{
	switch (p_Id)
	{
	case ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_NEW:
		HistoryModeSetting().Set(HistoryMode::g_RegNewHistoryMode);
		break;
	case ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_DETACH:
		HistoryModeSetting().Set(HistoryMode::g_RegDetachHistoryMode);
		break;
	case ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_SAME:
		HistoryModeSetting().Set(HistoryMode::g_RegRetainHistoryMode);
		break;
	case ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_ASK_EVERYTIME:
		HistoryModeSetting().Set(boost::none);
		break;
	default:
		ASSERT(false);
		break;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_NEW, ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_ASK_EVERYTIME, p_Id);
}

void BackstagePanelSettingsContent::OnHideDLGRAGroupsL(UINT p_Id)
{
	switch(p_Id)
	{
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLYES:
		HideGroupLicenseRestrictedAccessDialogSetting().Set(true);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLNO:
		HideGroupLicenseRestrictedAccessDialogSetting().Set(false);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLASK:
		HideGroupLicenseRestrictedAccessDialogSetting().Set(boost::none);
		break;
	default:
		ASSERT(false);
		break;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLASK, p_Id);
}

void BackstagePanelSettingsContent::OnHideDLGRAGroups(UINT p_Id)
{
	switch (p_Id)
	{
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSYES:
		HideGroupAdminRestrictedAccessDialogSetting().Set(true);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSNO:
		HideGroupAdminRestrictedAccessDialogSetting().Set(false);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSASK:
		HideGroupAdminRestrictedAccessDialogSetting().Set(boost::none);
		break;
	default:
		ASSERT(false);
		break;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSASK, p_Id);
}

void BackstagePanelSettingsContent::OnHideDLGRAUsersUA(UINT p_Id)
{
	switch (p_Id)
	{
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAYES:
		HideUltraAdminRequirementDialogSetting().Set(true);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUANO:
		HideUltraAdminRequirementDialogSetting().Set(false);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAASK:
		HideUltraAdminRequirementDialogSetting().Set(boost::none);
		break;
	default:
		ASSERT(false);
		break;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAASK, p_Id);
}

void BackstagePanelSettingsContent::OnHideDLGRAUsers(UINT p_Id)
{
	switch (p_Id)
	{
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSYES:
		HideUserPermissionRequirementDialogSetting().Set(true);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSNO:
		HideUserPermissionRequirementDialogSetting().Set(false);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSASK:
		HideUserPermissionRequirementDialogSetting().Set(boost::none);
		break;
	default:
		ASSERT(false);
		break;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSASK, p_Id);
}

void BackstagePanelSettingsContent::OnHideDLGRAConsent(UINT p_Id)
{
	switch (p_Id)
	{
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTYES:
		HideConsentReminderDialogSetting().Set(true);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTNO:
		HideConsentReminderDialogSetting().Set(false);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTASK:
		HideConsentReminderDialogSetting().Set(boost::none);
		break;
	default:
		ASSERT(false);
		break;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSASK, p_Id);
}

void BackstagePanelSettingsContent::OnHideUAdminDeprecated(UINT p_Id)
{
	switch (p_Id)
	{
	case ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDYES:
		HideUltraAdminDeprecatedReminderSetting().Set(true);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDNO:
		HideUltraAdminDeprecatedReminderSetting().Set(false);
		break;
	case ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDASK:
		HideUltraAdminDeprecatedReminderSetting().Set(boost::none);
		break;
	default:
		ASSERT(false);
		break;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDYES, ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDASK, p_Id);
}

void BackstagePanelSettingsContent::OnEnableAutoLoadLastSession(UINT p_Id)
{
	if (p_Id == ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_YES)
	{
		AutoLoadLastSessionSetting().Set(true);
	}
	else
	{
		ASSERT(p_Id == ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_NO);
		AutoLoadLastSessionSetting().Set(false);
	}
	CheckRadioButton(ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_YES, ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_NO, p_Id);
}

void BackstagePanelSettingsContent::OnFlatViewGroupByHPK(UINT p_Id)
{
	ASSERT(ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKYES == p_Id || ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKNO == p_Id);
	m_GridOptions.SetFlatViewGroupByHPK(ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKYES == p_Id);
	CheckRadioButton(ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKYES, ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKNO, p_Id);
}

void BackstagePanelSettingsContent::OnFlatViewHideHTop(UINT p_Id)
{
	ASSERT(ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPYES == p_Id || ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPNO == p_Id);
	m_GridOptions.SetFlatViewHideHTop(ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPYES == p_Id);
	CheckRadioButton(ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPYES, ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPNO, p_Id);
}

void BackstagePanelSettingsContent::OnFlatViewAskForceUponGroup(UINT p_Id)
{
	ASSERT(ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPYES == p_Id || ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPNO == p_Id);
	m_GridOptions.SetFlatViewAskForceUponGroup(ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPYES == p_Id);
	CheckRadioButton(ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPYES, ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPNO, p_Id);
}

void BackstagePanelSettingsContent::OnHierarchyShowHideDescendantCounters(UINT p_Id)
{
	ASSERT(ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSYES == p_Id || ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSNO == p_Id);
	m_GridOptions.SetShowHideHierarchyCounters(ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSYES == p_Id);
	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSYES, ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSNO, p_Id);
}

void BackstagePanelSettingsContent::OnRadioExplodeAllWhenShortSelection(UINT p_Id)
{
	switch (p_Id)
	{
	case ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONYES:
		m_GridOptions.SetExplodeAllRowsWhenShortSelection(true);
		break;
	case ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONNO:
		m_GridOptions.SetExplodeAllRowsWhenShortSelection(false);
		break;
	case ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONASK:
		m_GridOptions.eraseExplodeAllRowsKeys();
		break;
	default:
		ASSERT(false);
		break;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONYES, ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONASK, p_Id);
}

void BackstagePanelSettingsContent::initFlatViewGroupByHPK()
{
	CheckRadioButton(ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKYES, ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKNO, m_GridOptions.IsFlatViewGroupByHPK() ? ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKYES : ID_BACKSTAGE_SETTINGS_FLATVIEW_GROUPBY_HPKNO);
}

void BackstagePanelSettingsContent::initViewHideHTop()
{
	CheckRadioButton(ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPYES, ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPNO, m_GridOptions.IsFlatViewHideHTop() ? ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPYES : ID_BACKSTAGE_SETTINGS_FLATVIEW_HIDE_HTOPNO);
}

void BackstagePanelSettingsContent::initViewAskForceUponGroup()
{
	CheckRadioButton(ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPYES, ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPNO, m_GridOptions.IsFlatViewAskForceUponGroup() ? ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPYES : ID_BACKSTAGE_SETTINGS_FLATVIEW_ASKFORCE_UPONGROUPNO);
}

void BackstagePanelSettingsContent::initShowHideHierarchyCounters()
{
	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSYES, ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSNO, m_GridOptions.IsShowHideHierarchyCounters() ? ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSYES : ID_BACKSTAGE_SETTINGS_HIERARCHY_DESCCOUNTERSNO);
}

void BackstagePanelSettingsContent::initExplodeAllWhenShortSelection()
{
	UINT id = ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONASK;
	if (m_GridOptions.IsExplodeAllRowsWhenShortSelection())
		id = *m_GridOptions.IsExplodeAllRowsWhenShortSelection() ? ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONYES : ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONNO;

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONYES, ID_BACKSTAGE_SETTINGS_EXPLODEALLWSHORTSELECTIONASK, id);
}

void BackstagePanelSettingsContent::initUseCache()
{
	const auto val = UseCacheSetting().Get();

	UINT id = ID_BACKSTAGE_SETTINGS_USE_CACHE_ASK;

	if (val)
	{
		id = *val ? ID_BACKSTAGE_SETTINGS_USE_CACHE_YES : ID_BACKSTAGE_SETTINGS_USE_CACHE_NO;
		m_RadioUseCacheAsk.ShowWindow(SW_HIDE);
	}
	else
		m_RadioUseCacheAsk.ShowWindow(SW_SHOW);

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_USE_CACHE_YES, ID_BACKSTAGE_SETTINGS_USE_CACHE_ASK, id);
}

void BackstagePanelSettingsContent::initAutoReload()
{
	const auto val = AutoReloadSetting().Get();
	ASSERT(val); // Default is false, see AutoReloadSetting ctor

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_AUTORELOAD_YES, ID_BACKSTAGE_SETTINGS_AUTORELOAD_NO, *val ? ID_BACKSTAGE_SETTINGS_AUTORELOAD_YES : ID_BACKSTAGE_SETTINGS_AUTORELOAD_NO);
}

void BackstagePanelSettingsContent::initHistoryMode()
{
	UINT id = ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_ASK_EVERYTIME;

	const auto historyMode = HistoryModeSetting().Get();
	if (historyMode)
	{
		if (*historyMode == HistoryMode::g_RegDetachHistoryMode)
			id = ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_DETACH;
		else if (*historyMode == HistoryMode::g_RegRetainHistoryMode)
			id = ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_SAME;
		else if (*historyMode == HistoryMode::g_RegNewHistoryMode)
			id = ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_NEW;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_NEW, ID_BACKSTAGE_SETTINGS_RADIO_HISTORY_ASK_EVERYTIME, id);
}

void BackstagePanelSettingsContent::initHideDLGRAGroupsL()
{
	UINT id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLASK;

	const auto setting = HideGroupLicenseRestrictedAccessDialogSetting().Get();
	if (setting)
	{
		if (*setting)
			id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLYES;
		else
			id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLNO;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSLASK, id);
}

void BackstagePanelSettingsContent::initHideDLGRAGroups()
{
	UINT id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSASK;

	const auto setting = HideGroupAdminRestrictedAccessDialogSetting().Get();
	if (setting)
	{
		if (*setting)
			id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSYES;
		else
			id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSNO;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAGROUPSASK, id);
}

void BackstagePanelSettingsContent::initHideDLGRAUsersUA()
{
	UINT id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAASK;

	const auto setting = HideUltraAdminRequirementDialogSetting().Get();
	if (setting)
	{
		if (*setting)
			id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAYES;
		else
			id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUANO;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSUAASK, id);
}

void BackstagePanelSettingsContent::initHideDLGRAUsers()
{
	UINT id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSASK;

	const auto setting = HideUserPermissionRequirementDialogSetting().Get();
	if (setting)
	{
		if (*setting)
			id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSYES;
		else
			id = ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSNO;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRAUSERSASK, id);
}

void BackstagePanelSettingsContent::initHideDLGRAConsent()
{
	UINT id = ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTASK;

	const auto setting = HideConsentReminderDialogSetting().Get();
	if (setting)
	{
		if (*setting)
			id = ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTYES;
		else
			id = ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTNO;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTYES, ID_BACKSTAGE_SETTINGS_HIDEDLGRACONSENTASK, id);
}

void BackstagePanelSettingsContent::initHideUAdminDeprecated()
{
	UINT id = ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDASK;

	const auto setting = HideUltraAdminDeprecatedReminderSetting().Get();
	if (setting)
	{
		if (*setting)
			id = ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDYES;
		else
			id = ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDNO;
	}

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDYES, ID_BACKSTAGE_SETTINGS_HIDEULTRAADMINDEPRECATEDASK, id);
}

void BackstagePanelSettingsContent::initEnableAutoLoadLastSession()
{
	auto val = AutoLoadLastSessionSetting().Get();
	ASSERT(val); // Default is true, see AutoLoadLastSession ctor

	CheckRadioButton(ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_YES, ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_NO, *val ? ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_YES : ID_BACKSTAGE_SETTINGS_AUTOLOADLASTSESSION_NO);
}

void BackstagePanelSettingsContent::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	int nDelta;
	int nMaxPos = m_rcOriginalRect.Height() - m_nCurHeight;

	switch (nSBCode)
	{
	case SB_LINEDOWN:
		if (m_nScrollPos >= nMaxPos)
			return;

		nDelta = min(max(nMaxPos / 20, 5), nMaxPos - m_nScrollPos);
		break;

	case SB_LINEUP:
		if (m_nScrollPos <= 0)
			return;
		nDelta = -min(max(nMaxPos / 20, 5), m_nScrollPos);
		break;
	case SB_PAGEDOWN:
		if (m_nScrollPos >= nMaxPos)
			return;
		nDelta = min(max(nMaxPos / 10,5), nMaxPos - m_nScrollPos);
		break;
	case SB_THUMBTRACK:
	case SB_THUMBPOSITION:
		nDelta = (int)nPos - m_nScrollPos;
		break;

	case SB_PAGEUP:
		if (m_nScrollPos <= 0)
			return;
		nDelta = -min(max(nMaxPos / 10,5), m_nScrollPos);
		break;

	default:
		return;
	}
	m_nScrollPos += nDelta;
	SetScrollPos(SB_VERT, m_nScrollPos, TRUE);
	ScrollWindow(0, -nDelta);
	CExtResizableDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}

void BackstagePanelSettingsContent::OnSize(UINT nType, int cx, int cy)
{
	CExtResizableDialog::OnSize(nType, cx, cy);

	m_nCurHeight = cy;
	SCROLLINFO scrollInfo;
	scrollInfo.cbSize = sizeof(SCROLLINFO);
	scrollInfo.fMask = SIF_ALL;
	scrollInfo.nMin = 0;
	scrollInfo.nMax = m_rcOriginalRect.Height();
	scrollInfo.nPage = cy;
	scrollInfo.nPos = 0;
	SetScrollInfo(SB_VERT, &scrollInfo, TRUE);
}

BOOL BackstagePanelSettingsContent::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	int nMaxPos = m_rcOriginalRect.Height() - m_nCurHeight;

	if (zDelta < 0)
	{
		if (m_nScrollPos < nMaxPos)
		{
			zDelta = min(max(nMaxPos / 20, 5), nMaxPos - m_nScrollPos);

			m_nScrollPos += zDelta;
			SetScrollPos(SB_VERT, m_nScrollPos, TRUE);
			ScrollWindow(0, -zDelta);
		}
	}
	else
	{
		if (m_nScrollPos > 0)
		{
			zDelta = -min(max(nMaxPos / 20, 5), m_nScrollPos);

			m_nScrollPos += zDelta;
			SetScrollPos(SB_VERT, m_nScrollPos, TRUE);
			ScrollWindow(0, -zDelta);
		}
	}
	return CExtResizableDialog::OnMouseWheel(nFlags, zDelta, pt);
}

#if ENABLE_DRAGGING
void BackstagePanelSettingsContent::EndDrag()
{
	m_bDragging = false;
	ReleaseCapture();
}
 
 void BackstagePanelSettingsContent::OnLButtonDown(UINT nFlags, CPoint point)
 {
 	m_bDragging = true;
 	SetCapture();
 
 	m_ptDragPoint = point;
 
 	//SetCursor(m_hCursor2);
 
 	CExtResizableDialog::OnLButtonDown(nFlags, point);
 }
 
 void BackstagePanelSettingsContent::OnLButtonUp(UINT nFlags, CPoint point)
 {
 	EndDrag();
 
 	CExtResizableDialog::OnLButtonUp(nFlags, point);
 }
 
 void BackstagePanelSettingsContent::OnMouseMove(UINT nFlags, CPoint point)
 {
 	if (m_bDragging)
 	{
 		int nDelta = m_ptDragPoint.y - point.y;
 		m_ptDragPoint = point;
 
 		int nNewPos = m_nScrollPos + nDelta;
 
 		if (nNewPos < 0)
 			nNewPos = 0;
 		else if (nNewPos > m_rcOriginalRect.Height() - m_nCurHeight)
 			nNewPos = m_rcOriginalRect.Height() - m_nCurHeight;
 
 		nDelta = nNewPos - m_nScrollPos;
 		m_nScrollPos = nNewPos;
 
 		SetScrollPos(SB_VERT, m_nScrollPos, TRUE);
 		ScrollWindow(0, -nDelta);
 	}
 
 	CExtResizableDialog::OnMouseMove(nFlags, point);
 }
 
 void BackstagePanelSettingsContent::OnKillFocus(CWnd* pNewWnd)
 {
 	CExtResizableDialog::OnKillFocus(pNewWnd);
 
 	EndDrag();
 }
#endif
