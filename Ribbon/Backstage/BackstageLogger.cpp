#include "BackstageLogger.h"

BackstageLogger* BackstageLogger::g_Instance = nullptr;
bool BackstageLogger::g_createdOnce = false;

BackstageLogger::BackstageLogger()
{
	ASSERT(!g_createdOnce && g_Instance == nullptr);

	g_createdOnce = true;
	g_Instance = this;
}

BackstageLogger::~BackstageLogger()
{
	ASSERT(g_Instance == this);
	g_Instance = nullptr;
}

bool BackstageLogger::HasInstance()
{
	return nullptr != g_Instance;
}

BackstageLogger* BackstageLogger::Instance()
{
	ASSERT(HasInstance());
	return g_Instance;
}