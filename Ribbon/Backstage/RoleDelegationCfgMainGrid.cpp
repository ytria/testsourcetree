#include "RoleDelegationCfgMainGrid.h"

#include "YCodeJockMessageBox.h"

RoleDelegationCfgMainGrid::RoleDelegationCfgMainGrid() : CacheGrid(0,	GridBackendUtil::CREATEGROUPAREA |
																		GridBackendUtil::CREATECOLORG |
																		GridBackendUtil::CREATESORTING |
																		GridBackendUtil::CREATESORTING |
																		GridBackendUtil::CREATEPIVOT |
																		GridBackendUtil::CREATECOMPARE |
																		GridBackendUtil::CREATESAVEXML |
																		GridBackendUtil::CREATESTATUSBAR),
	m_Active(false)
{
	SetLiveGrid(true);
}

void RoleDelegationCfgMainGrid::customizeGrid()
{
	SetAutomationName(_YTEXT("RoleDelegationCfgMainGrid"));
	SetAutomationActionRecording(false);

	LoadRoleDelegations();
}

void RoleDelegationCfgMainGrid::LoadRoleDelegations()
{
}

void RoleDelegationCfgMainGrid::SetIsActive(const bool p_Active)
{
	m_Active = p_Active;
}