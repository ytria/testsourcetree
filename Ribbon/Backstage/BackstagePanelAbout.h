#pragma once

#include "BackstagePanelBase.h"
#include "..\..\Resource.h"

class CXTPRibbonBackstageLabelRightAlign : public CXTPRibbonBackstageLabel
{
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};

class BackstagePanelAbout : public BackstagePanelBase
{
public:
	BackstagePanelAbout();
	~BackstagePanelAbout() override;

	enum { IDD = IDD_BACKSTAGEPAGE_ABOUT };

	virtual void SetTheme(const XTPControlTheme nTheme) override;

	void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog() override;
	// why public?
	afx_msg void OnWebsite();
	afx_msg void OnEnterLicenseKey();
	afx_msg void OnConfigureLicense();
	afx_msg void OnUpgradeLicense();
	afx_msg void OnRefreshLicense();
	afx_msg void OnCheckForUpdate();
	afx_msg void OnDeauthorizeComputer();
	afx_msg void OnManageCosmosAccount();
	afx_msg void OnManageLicenseHistory();

	DECLARE_MESSAGE_MAP()

	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

private:
	void updateLicenseInfo(const bool p_Refresh);

	CXTPRibbonBackstageLabelRightAlign	m_PatentLabel;
	CXTPRibbonBackstageLabelRightAlign	m_CopyrightsLabel;
	CXTPMarkupStatic			m_LicenseLabel;
	CXTPRibbonBackstageButton	m_WebsiteButton;
	CXTPRibbonBackstageButton	m_LicenseKeyButton;
	CXTPRibbonBackstageButton	m_LicenseCfgButton;
	CXTPRibbonBackstageButton	m_LicenseUpgradeButton;
	CXTPRibbonBackstageButton	m_LicenseRefreshButton;
	CXTPRibbonBackstageButton	m_CheckForUpdateButton;
	CXTPRibbonBackstageButton	m_LicenseDeauthorizeComputerButton;
	CXTPRibbonBackstageButton	m_LicenseCosmosCNX;
	CXTPRibbonBackstageButton	m_LicenseHistoryButton;
	CXTPImageManager			m_imagelist;

	bool						m_IsCosmosSetForAnotherTenant;
	wstring						m_CurrentLicenseCode;
	bool						m_LicenseChanged;

	// Crash trigger
	static const std::vector<UINT> g_CrashSequence;
	size_t m_CrashSequenceIndex;
	void doCrashWinChangeNullPointer();
	void doCrashPureVirtualCall();
	virtual BOOL PreTranslateMessage(MSG* pMsg) override;
};