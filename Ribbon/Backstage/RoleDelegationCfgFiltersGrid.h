#pragma once

#include "RoleDelegationCfgGridBase.h"

class RoleDelegationCfgFiltersGrid : public RoleDelegationCfgGridBase
{
public:
	RoleDelegationCfgFiltersGrid();
	
protected:
	virtual void customizeGridSpecific() override;
	virtual void customizeGridPostProcessSpecific() override;
	virtual void loadPostProcessSpecific(GridBackendRow* p_Row) override;
	virtual void prepareSQLoadSpecific() override;

private:
	GridBackendColumn* m_ColumnFilterType;
	GridBackendColumn* m_ColumnProperties;
	GridBackendColumn* m_ColumnTaggerMatchMethod;

	enum IconNumber
	{
		ICON_USER = GridBackendUtil::g_FirstGridIcon,
		ICON_SITE,
		ICON_GROUP
	};
};