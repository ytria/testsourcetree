#pragma once

#include "BackstageTabPanelRoleDelegationConfigurationBase.h"
#include "RoleDelegationCfgKeysGrid.h"
#include "..\..\Resource.h"

class Sapio365Session;

class BackstageTabPanelRoleDelegationConfigurationKeys : public BackstageTabPanelRoleDelegationConfigurationBase<RoleDelegationCfgKeysGrid>
{
public:
	BackstageTabPanelRoleDelegationConfigurationKeys();
	~BackstageTabPanelRoleDelegationConfigurationKeys() override;

	enum { IDD = IDD_BACKSTAGEPAGE_ROLEDELEGATION_CFG_KEYS };

protected:
	virtual void OnInitDialogSpecific() override;	

private:
	virtual void add() override;
	virtual void edit() override;
	virtual void remove() override;
	virtual void updateButtons(const vector<GridBackendRow*>& p_SelectedNonGroupRows) override;

	std::shared_ptr<const Sapio365Session>	getSession() const;
};
