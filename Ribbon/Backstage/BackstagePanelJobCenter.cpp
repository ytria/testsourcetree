#include "BackstagePanelJobCenter.h"

#include "AutomatedApp.h"
#include "AutomatedMainFrame.h"
#include "AutomationPresetMaker.h"
#include "BackstagePanelBase.h"
#include "CodeJockThemed.h"
#include "CodeJockFrameBase.h"
#include "DlgJobPresetEdit.h"
#include "DlgScheduleConfigHTML.h"
#include "FileUtil.h"
#include "SharedResource.h"
#include "WindowsScheduledTask.h"
#include "YCodeJockMessageBox.h"
#include "YFileDialog.h"

BEGIN_MESSAGE_MAP(BackstagePanelJobCenter, BackstagePanelJobCenterBase)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_TOGGLE,		onToggleDisplay)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETADD,		onPresetAdd)
//	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETIMPORT,	onPresetImport)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_RUN,			onRun)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_TOGGLEVIEW,	onToggleView)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_JOBCENTER_SUGGEST,		onSuggest)
END_MESSAGE_MAP()

BackstagePanelJobCenter::BackstagePanelJobCenter(CodeJockFrameBase* p_Frame)
	: BackstagePanelJobCenterBase(p_Frame, IDD, true)
{
}

BackstagePanelJobCenter::~BackstagePanelJobCenter()
{
}

void BackstagePanelJobCenter::setThemeSpecific(const XTPControlTheme nTheme)
{
	m_BtnToggleDisplay.SetTheme(nTheme);
	m_BtnPresetAdd.SetTheme(nTheme);
//	m_BtnPresetImport.SetTheme(nTheme);
	m_BtnRun.SetTheme(nTheme);
	m_BtnToggleView.SetTheme(nTheme);
	m_BtnSuggest.SetTheme(nTheme);
}

void BackstagePanelJobCenter::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelJobCenterBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_TOGGLE,				m_BtnToggleDisplay);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETADD,			m_BtnPresetAdd);
//	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETIMPORT,		m_BtnPresetImport);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_RUN,				m_BtnRun);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_TOGGLEVIEW,			m_BtnToggleView);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_JOBCENTER_SUGGEST,			m_BtnSuggest);
}

BOOL BackstagePanelJobCenter::OnInitDialogSpecific()
{
	//{
	//	UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETIMPORT };
	//	m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_PRESETIMPORT, ids, 1, CSize(0, 0), xtpImageNormal);
	//	BackstagePanelBase::SetupButton(m_BtnPresetImport, _T("Import Saved Settings"), _T("Import the saved input settings from an XML file for the selected job."), m_ImageList);
	//	m_BtnPresetImport.SetTooltip(_T("Import the saved input settings from an XML file for the selected job."));
	//}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_RUN };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_RUN, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnRun, _YTEXT("RUN"), _YTEXT(""), m_ImageList);
		m_BtnRun.SetTooltip(_T("Run the Job\nExecute the selected job (along with its associated 'preset', if selected)."));
	}

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_PRESETADD };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_PRESETADD, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnPresetAdd, _YTEXT("NEW PRESET"), _YTEXT(""), m_ImageList);
		m_BtnPresetAdd.SetTooltip(_T("Create Job Preset\nSave a pre-defined set of user inputs for re-use with the selected job."));
		if (m_Grid.IsSimpleView())
			m_BtnPresetAdd.ShowWindow(SW_HIDE);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_TOGGLE };
		m_ImageList.SetIcons(IDB_BUTTON_JOBCENTER_TOGGLEDISPLAY, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnToggleDisplay, _YTEXT("TOGGLE"), _YTEXT(""), m_ImageList);
		m_BtnToggleDisplay.SetTooltip(_T("Show/Hide\nToggle the display of selected jobs in the current Job Center"));
	}

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_SUGGEST };
		m_ImageList.SetIcons(IDB_BACKSTAGE_BUTTON_JOBCENTER_SUGGEST, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnSuggest, _YTEXT("SUGGEST"), _YTEXT(""), m_ImageList);
		m_BtnSuggest.SetTooltip(_T("Suggest a new 'job'"));

		AddAnchor(IDC_BACKSTAGE_BUTTON_JOBCENTER_SUGGEST, CSize(100, 0), CSize(100, 0));
	}

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_JOBCENTER_TOGGLEVIEW };
		m_ImageList.SetIcons(IDB_BACKSTAGE_BUTTON_JOBCENTER_TOGGLEVIEW, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnToggleView, _YTEXT("ADVANCED"), _YTEXT(""), m_ImageList);
		m_BtnToggleView.SetTooltip(_T("Toggle between simple and avdanced view."));

		m_BtnToggleView.SetCheck(!m_Grid.IsSimpleView() ? BST_CHECKED : BST_UNCHECKED);

		AddAnchor(IDC_BACKSTAGE_BUTTON_JOBCENTER_TOGGLEVIEW, CSize(100, 0), CSize(100, 0));
	}

	return TRUE;
}

void BackstagePanelJobCenter::OnSetActive()
{
	CWaitCursor _;

	SetConfigurationWasUpdated(false);
	isDebugMode();

	auto gridFrame = dynamic_cast<GridFrameBase*>(m_Frame);
	ASSERT(nullptr != gridFrame);
	if (nullptr != gridFrame)
	{
		m_JobCenter = gridFrame->GetGrid().GetAutomationWizard();
		m_Grid.LoadScripts(m_JobCenter);
	}
	else
	{
		m_JobCenter = nullptr;
		ASSERT(false);
	}

	updateButtons();
}

void BackstagePanelJobCenter::updateButtons()
{
	m_BtnImport.EnableWindow(FALSE);
	m_BtnExport.EnableWindow(FALSE);
	m_BtnUpdate.EnableWindow(FALSE);
	m_BtnToggleDisplay.EnableWindow(FALSE);
	m_BtnRemove.EnableWindow(FALSE);
	m_BtnEdit.EnableWindow(FALSE);
	m_BtnPresetAdd.EnableWindow(FALSE);
//	m_BtnPresetImport.EnableWindow(FALSE);
	m_BtnRun.EnableWindow(FALSE);
	m_BtnToggleView.EnableWindow(TRUE);
	m_BtnSuggest.EnableWindow(TRUE);

	if (!isDebugMode() && getSession())
	{
		vector<Script>			scripts;
		vector<ScriptPreset>	scriptPresets;
		vector<ScriptSchedule>	scriptSchedules;
		m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);

		bool uUpdate = false;
		bool uRemove = !scriptPresets.empty();
		for (const auto& s : scripts)
		{
			if (!s.IsSystem())
				uRemove = true;
			if (!s.IsSystem() && FileUtil::FileExists(s.m_SourceXMLfilePath))
				uUpdate = true;
			if (uUpdate && uRemove)
				break;
		}

		for (const auto& sp : scriptPresets)
		{
			if (uUpdate && uRemove)
				break;
			if (!uUpdate && !sp.m_SourceXMLfilePath.empty() && FileUtil::FileExists(sp.m_SourceXMLfilePath))
				uUpdate = true;
		}
		
		const bool runnableScript	= scripts.size() == 1 && scripts.front().IsRunnable();
		const bool lockedScript		= scripts.size() == 1 && scripts.front().IsPositionLocked();
		const bool runnablePreset = [&scripts, &scriptPresets, this]()
		{
			if (scriptPresets.size() == 1 && scripts.empty())
			{
				auto script = m_JobCenter->GetScript(scriptPresets.front().m_ScriptID);
				if (script.IsRunnable())
					return true;
			}

			return false;
		}();

		m_BtnImport.EnableWindow(TRUE);
		m_BtnExport.EnableWindow(!scripts.empty() || !scriptPresets.empty() ? TRUE : FALSE);
		m_BtnUpdate.EnableWindow(uUpdate ? TRUE : FALSE);
		m_BtnToggleDisplay.EnableWindow(runnableScript && !lockedScript ? TRUE : FALSE);
		m_BtnRemove.EnableWindow(uRemove && !lockedScript  ? TRUE : FALSE);
		m_BtnEdit.EnableWindow(uRemove && ((scripts.size() == 1 && scriptPresets.empty()) || (scriptPresets.size() == 1 && scripts.empty())) ? TRUE : FALSE);
		m_BtnPresetAdd.EnableWindow(runnableScript ? TRUE : FALSE);
//		m_BtnPresetImport.EnableWindow(scripts.size() == 1 ? TRUE : FALSE);
		m_BtnRun.EnableWindow((runnableScript && scriptPresets.empty()) || runnablePreset ? TRUE : FALSE);
	}
}

void BackstagePanelJobCenter::onToggleDisplay()
{
	ASSERT(nullptr != m_JobCenter && nullptr != m_Grid);
	if (nullptr != m_JobCenter && nullptr != m_Grid && !isDebugMode())
	{
		bool update = false;
		vector<Script>			scripts;
		vector<ScriptPreset>	scriptPresets;
		vector<ScriptSchedule>	scriptSchedules;
		m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);
		for (auto& s : scripts)
		{
			if (!s.IsPositionLocked())
			{
				s.m_LogUpdate_vol = false;
				m_JobCenter->ToggleScriptDisplay(s);
				update = true;
			}
		}
		if (update)
		{
			m_Grid.LoadScripts(m_JobCenter);
			SetConfigurationWasUpdated(true);
		}
	}
}

ScriptPreset BackstagePanelJobCenter::addPreset(Script& p_Script)
{
	ScriptPreset rvPreset;

	ASSERT(nullptr != m_JobCenter);
	if (nullptr != m_JobCenter)
	{
		AutomationPresetMaker maker(p_Script, m_JobCenter, this);
		const auto& errors = maker.GetErrors();
		if (errors.empty())
		{
			DlgJobPresetEdit dlg(p_Script, this);
			if (IDOK == dlg.DoModal())
			{
				rvPreset = dlg.GetPreset();
				m_JobCenter->AddScriptPreset(p_Script, maker.GetGeneratedPresetContent(), rvPreset);
				m_Grid.LoadScripts(m_JobCenter);

				if (!maker.GetWarnings().empty())
				{
				/*	YCodeJockMessageBox dlg(this,
											DlgMessageBox::eIcon_Warning,
											_T("Job preset generated with warnings"),
											Str::implode(maker.GetWarnings(), _YTEXT("\n")),
											_YFORMAT(L"Job: %s", s.m_Title.c_str()),
											{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
					dlg.DoModal();*/
				}
			}
		}
		else
		{
			if (maker.isEmptyPreset())
			{
				YCodeJockMessageBox dlg(this,
										DlgMessageBox::eIcon_Information,
										_T("No specific preset required"),
										Str::implode(errors, _YTEXT("\n")),
										_YFORMAT(L"For job: %s", p_Script.m_Title.c_str()),
										{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
				dlg.DoModal();
			}
			else
			{
				YCodeJockMessageBox dlg(this,
										DlgMessageBox::eIcon_ExclamationWarning,
										_T("Job preset not created"),
										Str::implode(errors, _YTEXT("\n")),
										_YFORMAT(L"For job: %s", p_Script.m_Title.c_str()),
										{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
				dlg.DoModal();
			}
		}
	}

	return rvPreset;
}

void BackstagePanelJobCenter::onPresetAdd()
{
	vector<Script>			scripts;
	vector<ScriptPreset>	scriptPresets;
	vector<ScriptSchedule>	scriptSchedules;
	m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);
	ASSERT(scripts.size() == 1 && nullptr != m_JobCenter);
	if (scripts.size() == 1 && nullptr != m_JobCenter)
		addPreset(scripts.front());
}

//void BackstagePanelJobCenter::onPresetImport()
//{
//	ASSERT(nullptr != m_JobCenter && nullptr != m_Grid);
//	if (nullptr != m_JobCenter && nullptr != m_Grid && !isDebugMode())
//	{
//		vector<Script>			scripts;
//		vector<ScriptPreset>	scriptPresets;
//		vector<ScriptSchedule>	scriptSchedules;
//		m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);
//		ASSERT(scripts.size() == 1);
//		if (scripts.size() == 1)
//		{
//			wstring path = YFileDialog::AskOpenXmlFile(nullptr, this);
//			if (!path.empty())
//			{
//				auto newScriptPreset = ScriptParser().MakeScriptPresetFromXMLFile(m_JobCenter, path);
//				if (newScriptPreset.m_LoadError_vol.empty())
//				{
//					const auto& s = scripts.front();
//					DlgJobPresetEdit dlg(s, this);
//					if (IDOK == dlg.DoModal())
//					{
//						m_JobCenter->AddScriptPreset(s, newScriptPreset.m_ScriptContent, dlg.GetPreset());
//						m_Grid.LoadScripts(m_JobCenter);
//						SetConfigurationWasUpdated(true);
//					}
//				}
//				else
//				{
//					YCodeJockMessageBox dlg(this,
//											DlgMessageBox::eIcon_Error,
//											_YFORMAT(L"Unable to import job from: %s", path.c_str()),
//											Str::implode(newScriptPreset.m_LoadError_vol, _YTEXT("\n")).c_str(),
//											_YTEXT(""),
//											{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
//					dlg.DoModal();
//				}
//			}
//		}
//	}
//}

void BackstagePanelJobCenter::onRun()
{
	runSelected();
}

void BackstagePanelJobCenter::runSelected()
{
	ASSERT(m_Grid);
	ASSERT(nullptr != m_JobCenter);
	if (m_Grid && nullptr != m_JobCenter)
	{
		const auto keys = [this]()
		{
			vector<Script>			scripts;
			vector<ScriptPreset>	scriptPresets;
			vector<ScriptSchedule>	scriptSchedules;
			m_Grid.GetSelected(scripts, scriptPresets, scriptSchedules);

			if (scripts.size() == 1 && scripts.front().IsRunnable() && scriptPresets.empty())
				return std::make_pair(scripts.front().m_Key, wstring());

			if (scriptPresets.size() == 1 && scripts.empty())
			{
				auto script = m_JobCenter->GetScript(scriptPresets.front().m_ScriptID);
				if (script.IsRunnable())
					return std::make_pair(script.m_Key, scriptPresets.front().m_Key);
			}

			return std::make_pair(wstring(), wstring());
		}();

		ASSERT(!keys.first.empty());
		if (!keys.first.empty())
		{
			BackstagePanelBase* motherPanel = dynamic_cast<BackstagePanelBase*>(GetParent());
			ASSERT(nullptr != motherPanel);
			if (nullptr != motherPanel)
				motherPanel->CloseBackstage();
			m_JobCenter->Run(keys.first, keys.second);
		}
	}
}

void BackstagePanelJobCenter::onToggleView()
{
	GetParent()->SendMessage(WM_COMMAND, MAKELONG(IDC_BACKSTAGE_JOBCENTER_TOGGLE_VIEW, 0), NULL);
	m_BtnToggleView.SetCheck(!m_Grid.IsSimpleView() ? BST_CHECKED : BST_UNCHECKED);
}

void BackstagePanelJobCenter::onSuggest()
{
	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != app)
		app->SuggestNewJob(m_Frame);
}

void BackstagePanelJobCenter::OnGridDoubleClick()
{
	onRun();
}