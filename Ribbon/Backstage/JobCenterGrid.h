#pragma once

#include "AutomationWizard.h"
#include "BaseO365Grid.h"

class BackstagePanelJobCenterBase;
class GridFrameBase;
class JobCenterGrid : public CacheGrid, public GridFilterObserver
{
public:
	JobCenterGrid(bool p_WithJobsOrdering);
	virtual ~JobCenterGrid();

	void LoadScripts(AutomationWizardO365* p_JC);

	void GetSelected(vector<Script>& p_Scripts, vector<ScriptPreset>& p_ScriptPresets, vector<ScriptSchedule>& p_ScriptSchedules);

	void FixPositions();

	virtual void OnCustomDoubleClick() override;

	void SetSimpleView(bool p_SimpleView, bool p_TriggerUpdate = true);
	bool IsSimpleView() const;

	void SetJobSelectionIDs(const set<wstring>& p_SelectedJobKeys);
	void ResetJobSelectionIDs();

	virtual void InitializeCommands() override;

protected:
	virtual void customizeGrid() override;
	virtual void customizeGridPostProcess() override;
	virtual void OnCustomClickUp() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;

	afx_msg void OnDropFiles(HDROP dropInfo);
	DECLARE_MESSAGE_MAP()

	void FilterChanged(const CacheGrid& grid) override;

private:
	void loadScripts();
	void addRow(const size_t p_ScriptIndex, const vector<Script>& p_Scripts, const bool isMianFrame);
	void manageScriptPosition(GridBackendRow* p_RowToMove, GridBackendColumn* p_MoverColumn);
	void manageUpDownArrows();
	
	afx_msg void OnUpdateSelectJobs(CCmdUI* pCmdUI);
	afx_msg void OnSelectAJLjobs();

	bool m_SimpleView;
	bool m_WithJobsOrdering;
	
	boost::YOpt<set<wstring>> m_SelectedJobIDs;// library IDs - this set defines which jobs were bought with the license and are allowed to run

	GridBackendColumn* m_ColumnMoveUp;
	GridBackendColumn* m_ColumnMoveDown;

	GridBackendColumn* m_ColumnDisplayed;
	GridBackendColumn* m_ColumnShared;
	GridBackendColumn* m_ColumnSystem;// is script provided by Ytria
	GridBackendColumn* m_ColumnLib;// is script a library element

	GridBackendColumn* m_ColumnKey;
	GridBackendColumn* m_ColumnTitle;
	GridBackendColumn* m_ColumnDesc;
	GridBackendColumn* m_ColumnTooltip;
	GridBackendColumn* m_ColumnIcon;
	GridBackendColumn* m_ColumnPosition;
	GridBackendColumn* m_ColumnSourceFilePath;

	GridBackendColumn* m_ColumnDateLastUpdate;
	GridBackendColumn* m_ColumnDateLastUpdateBy;
	GridBackendColumn* m_ColumnDateLastUpdateByEmail;
	GridBackendColumn* m_ColumnDateLastUpdateByID;// user graph ID
	GridBackendColumn* m_ColumnDateLastShared;
	
	AutomationWizardO365*			m_JobCenter;
	BackstagePanelJobCenterBase*	m_PanelJobCenter;

	bool m_HasTooltips;

	int32_t ICON_SYSTEM, ICON_DISPLAYED, ICON_SHARED, ICON_LIB;

	enum
	{
		ICON_UP = GridBackendUtil::g_FirstGridIcon,
		ICON_DOWN,
		ICON_UPDISABLED,
		ICON_DOWNDISABLED
	};

	static wstring g_ElementTypeScript;
	static wstring g_ElementTypeScriptPreset;
	static wstring g_ElementTypeScriptSchedule;

	const static rttr::type::type_id g_ElementTypeIDScript;
	const static rttr::type::type_id g_ElementTypeIDScriptPreset;
	const static rttr::type::type_id g_ElementTypeIDScriptSchedule;
};
