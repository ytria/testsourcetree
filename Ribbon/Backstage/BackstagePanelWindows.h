#pragma once

#include "BackstagePanelWithGrid.h"
#include "WindowsListObserver.h"
#include "WindowsGrid.h"
#include "..\..\Resource.h"

class BackstagePanelWindows : 
	public BackstagePanelWithGrid<WindowsGrid>, 
	public GridSelectionObserver,
	public WindowsListObserver
{
public:
	BackstagePanelWindows(UINT resourceID);
	virtual ~BackstagePanelWindows();

	enum
	{
		IDD_FOR_MAIN = IDD_BACKSTAGEPAGE_WINDOWS_FOR_MAIN,
		IDD = IDD_BACKSTAGEPAGE_WINDOWS
	};

	virtual BOOL OnInitDialog() override;
	virtual BOOL OnSetActive() override;
	virtual void SelectionChanged(const CacheGrid& grid);
	virtual void UpdateWindowList();

	void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP();

	virtual void SetTheme(const XTPControlTheme nTheme) override;

private:
	afx_msg void OnDestroy();
	afx_msg void OnActivateSelected();
	afx_msg void OnCloseSelected();
	afx_msg void OnMaxSelected();
	afx_msg void OnMinSelected();
	afx_msg void OnRestoreSelected();
	afx_msg void OnGotoMainWindow();
	afx_msg void OnArrange();

	void DoCascade();
	void DoHorizontal();
	void DoVertical();
	void DoTileHorizontal();
	void DoTileVertically();

	void ArrangeModules(const UINT p_mdiTile);
	void TileModules(const UINT p_mdiTile);
	void CascadeModules();
	bool GetMainMonitorWorkingArea(CRect& rect);

	CXTPRibbonBackstageButton m_btnActivateSelected;
	CXTPRibbonBackstageButton m_btnCloseSelected;
	CXTPRibbonBackstageButton m_btnMaxSelected;
	CXTPRibbonBackstageButton m_btnMinSelected;
	CXTPRibbonBackstageButton m_btnRestoreSelected;
	CXTPRibbonBackstageButton m_btnGotoMainWindow;
	CXTPRibbonBackstageButton m_btnArrange;

	CXTPImageManager m_imagelist;
};