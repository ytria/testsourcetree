#pragma once

#include "BackstagePanelBase.h"
#include "..\..\Resource.h"

class BackstagePanelSettingsContent;

class BackstagePanelSettings : public BackstagePanelBase
{
public:
	BackstagePanelSettings();
	~BackstagePanelSettings() override;

	enum { IDD = IDD_BACKSTAGEPAGE_SETTINGS };

 	virtual void SetTheme(const XTPControlTheme nTheme) override;
 	void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog() override;
 	DECLARE_MESSAGE_MAP()
 	virtual BOOL OnSetActive() override;
 	virtual BOOL OnKillActive() override;

private:
 	afx_msg void OnReset();
	afx_msg void OnProxySettings();
	afx_msg void OnEnableURL();
 	CXTPRibbonBackstageButton m_btnReset;
 	CXTPRibbonBackstageButton m_btnProxySettings;
	CXTPRibbonBackstageButton m_btnEnableUrlLaunch;
	CXTPImageManager m_imagelist;
	std::unique_ptr<BackstagePanelSettingsContent>	m_Content;
};
