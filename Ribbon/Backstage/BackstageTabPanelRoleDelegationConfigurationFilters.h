#pragma once

#include "BackstageTabPanelRoleDelegationConfigurationBase.h"
#include "RoleDelegationCfgFiltersGrid.h"
#include "..\..\Resource.h"

class BackstageTabPanelRoleDelegationConfigurationFilters : public BackstageTabPanelRoleDelegationConfigurationBase<RoleDelegationCfgFiltersGrid>
{
public:
	BackstageTabPanelRoleDelegationConfigurationFilters();
	~BackstageTabPanelRoleDelegationConfigurationFilters() override;

	enum { IDD = IDD_BACKSTAGEPAGE_ROLEDELEGATION_CFG_FILTERS };

protected:
	virtual void OnInitDialogSpecific() override;

private:
	virtual void add() override;
	virtual void edit() override;
	virtual void remove() override;
	virtual void updateButtons(const vector<GridBackendRow*>& p_SelectedNonGroupRows) override;
};
