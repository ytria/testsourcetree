#pragma once

#include "BackstagePanelWithGrid.h"
#include "RoleDelegationCfgFiltersGrid.h"
#include "..\..\Resource.h"

class BackstagePanelRoleDelegationConfigurationFilters : public BackstagePanelWithGrid<RoleDelegationCfgFiltersGrid>
{
public:
	BackstagePanelRoleDelegationConfigurationFilters();
	~BackstagePanelRoleDelegationConfigurationFilters() override;

	enum { IDD = IDD_BACKSTAGEPAGE_ROLEDELEGATION_CFG_FILTERS };

	void DoDataExchange(CDataExchange* pDX);
	virtual void SetTheme(const XTPControlTheme nTheme) override;

	virtual BOOL OnInitDialog() override;
	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

	DECLARE_MESSAGE_MAP();
};