#include "BackstagePanelAutomation.h"

#include "AutomatedMainFrame.h"
#include "CodeJockThemed.h"
#include "CodeJockFrameBase.h"
#include "DlgLicenseMessageBoxHTML.h"
#include "MainFrame.h"
#include "Office365Admin.h"
#include "SharedResource.h"

BEGIN_MESSAGE_MAP(BackstagePanelAutomation, BackstagePanelBase)
	ON_BN_CLICKED(IDC_BACKSTAGE_RECENT_LIST,			OnRecentFileListClick)
	ON_BN_CLICKED(ID_BACKSTAGE_AUTOMATION_LOAD,			OnLoad)
	ON_BN_CLICKED(ID_BACKSTAGE_AUTOMATION_RECORDER,		OnRecorder)
	ON_BN_CLICKED(ID_BACKSTAGE_AUTOMATION_CONSOLE,		OnConsole)
	ON_BN_CLICKED(ID_BACKSTAGE_AUTOMATION_QUICKLAUNCH,	OnQuickLaunch)
END_MESSAGE_MAP()

BackstagePanelAutomation::BackstagePanelAutomation()
	: BackstagePanelBase(IDD)
	, m_RecentList(make_unique<BackstageRecentList>())
	, m_Frame(nullptr)
{

}

BackstagePanelAutomation::BackstagePanelAutomation(CodeJockFrameBase* p_Frame)
	: BackstagePanelBase(IDD)
	, m_RecentList(make_unique<BackstageRecentList>())
	, m_Frame(p_Frame)
{}

BackstagePanelAutomation::~BackstagePanelAutomation()
{
}

void BackstagePanelAutomation::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelBase::SetTheme(nTheme);

	if (m_RecentList && nullptr != m_RecentList->GetPaintManager())
	{
		m_RecentList->GetGallery()->SetTransparent(FALSE);
		m_RecentList->SetPaintManager(CXTPPaintManager::CreateTheme(CodeJockAppThemeUtil::toXTPPaintTheme(GetTheme())));
	}

	m_LoadFileButton.SetTheme(nTheme);
	m_RecorderButton.SetTheme(nTheme);
	m_ConsoleButton.SetTheme(nTheme);
	m_QuickLaunchButton.SetTheme(nTheme);
	m_RecentCaption.SetTheme(nTheme);
	m_RecentSeparator.SetTheme(nTheme);
}

void BackstagePanelAutomation::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelBase::DoDataExchange(pDX);

	ASSERT(nullptr != m_RecentList);
	if (nullptr != m_RecentList)
		DDX_Control(pDX, IDC_BACKSTAGE_RECENT_LIST, *m_RecentList);

	DDX_Control(pDX, ID_BACKSTAGE_AUTOMATION_LOAD,				m_LoadFileButton);
	DDX_Control(pDX, ID_BACKSTAGE_AUTOMATION_RECORDER,			m_RecorderButton);
	DDX_Control(pDX, ID_BACKSTAGE_AUTOMATION_CONSOLE,			m_ConsoleButton);
	DDX_Control(pDX, ID_BACKSTAGE_AUTOMATION_QUICKLAUNCH,		m_QuickLaunchButton);
	DDX_Control(pDX, ID_BACKSTAGE_AUTOMATION_RECENTCAPTION,		m_RecentCaption);
	DDX_Control(pDX, ID_BACKSTAGE_AUTOMATION_RECENTSEPARATOR,	m_RecentSeparator);
}

BOOL BackstagePanelAutomation::OnInitDialog()
{
	BackstagePanelBase::OnInitDialog();

	m_RecentSeparator.ShowWindow(SW_HIDE);

	setCaption(YtriaTranslate::Do(BackstagePanelAutomation_OnInitDialog_1, _YLOC("Automation")).c_str());
	m_RecentCaption.SetWindowText(YtriaTranslate::Do(BackstagePanelAutomation_OnInitDialog_2, _YLOC("Recent Automation files")).c_str());
	m_RecentCaption.SetFont(&m_xtpFontCaption);

	SetDlgCtrlID(IDD);
	ModifyStyleEx(0, WS_EX_CONTROLPARENT);

	ASSERT(nullptr != m_RecentList);
	if (nullptr != m_RecentList)
	{
		// For mouse notification on static control
		m_RecentList->ModifyStyle(0, SS_NOTIFY);
		m_RecentList->SetUseShellIcon(TRUE);

		m_RecentList->GetGallery()->SetTransparent(FALSE);
		m_RecentList->EnableWindow(TRUE);
		m_RecentList->ShowWindow(SW_SHOW);
		m_RecentList->BringWindowToTop();
	}

	UINT ids[]{ ID_BACKSTAGE_AUTOMATION_LOAD, ID_BACKSTAGE_AUTOMATION_RECORDER, ID_BACKSTAGE_AUTOMATION_CONSOLE, ID_BACKSTAGE_AUTOMATION_QUICKLAUNCH };
	m_imagelist.SetIcons(IDB_BACKSTAGE_AUTOMATION_BUTTON_ICONS, ids, 4, CSize(0, 0), xtpImageNormal);

	auto mainFrame = getMainWindow();
	ASSERT(nullptr != mainFrame);
	auto result = MFCUtil::GetAcceleratorTableTexts(nullptr != mainFrame ? mainFrame->m_hAccelTable  :nullptr);
	ASSERT(result.first && result.second.size() > 0);
	const auto& acceleratorTexts = result.second;

	{
		const wstring shortcut = (acceleratorTexts.end() != acceleratorTexts.find(ID_FILE_LOADAUTOMATIONXML)) ? acceleratorTexts.at(ID_FILE_LOADAUTOMATIONXML) : _YTEXT("");
		SetupButton(m_LoadFileButton
			, YtriaTranslate::Do(BackstagePanelAutomation_OnInitDialog_3, _YLOC("Load")).c_str() + (shortcut.empty() ? _YTEXT("") : _YTEXTFORMAT(L" (%s)", shortcut.c_str()))
			, YtriaTranslate::Do(BackstagePanelAutomation_OnInitDialog_4, _YLOC("Load Automation File")).c_str()
			, m_imagelist);
	}

	{
		const wstring shortcut = (acceleratorTexts.end() != acceleratorTexts.find(ID_AUTOMATION_RECORDER)) ? acceleratorTexts.at(ID_AUTOMATION_RECORDER) : _YTEXT("");
		SetupButton(m_RecorderButton
			, YtriaTranslate::Do(BackstagePanelAutomation_OnInitDialog_5, _YLOC("Recorder")).c_str() + (shortcut.empty() ? _YTEXT("") : _YTEXTFORMAT(L" (%s)", shortcut.c_str()))
			, YtriaTranslate::Do(BackstagePanelAutomation_OnInitDialog_6, _YLOC("Open Automation Recorder")).c_str()
			, m_imagelist);
	}

	{
		const wstring shortcut = (acceleratorTexts.end() != acceleratorTexts.find(ID_AUTOMATION_VIEWCONSOLE)) ? acceleratorTexts.at(ID_AUTOMATION_VIEWCONSOLE) : _YTEXT("");
		SetupButton(m_ConsoleButton
			, shortcut.empty() ? YtriaTranslate::Do(BackstagePanelAutomation_OnInitDialog_7, _YLOC("Console")).c_str() : YtriaTranslate::Do(BackstagePanelAutomation_OnInitDialog_11, _YLOC("Console (%1)"), shortcut.c_str()).c_str()
			, YtriaTranslate::Do(BackstagePanelAutomation_OnInitDialog_8, _YLOC("Open Automation Console")).c_str()
			, m_imagelist);
	}

	{
		const wstring shortcut = (acceleratorTexts.end() != acceleratorTexts.find(ID_AUTOMATION_SHORTCUTS)) ? acceleratorTexts.at(ID_AUTOMATION_SHORTCUTS) : _YTEXT("");
		SetupButton(m_QuickLaunchButton
			, YtriaTranslate::Do(BackstagePanelAutomation_OnInitDialog_9, _YLOC("Shortcuts")).c_str() + (shortcut.empty() ? _YTEXT("") : _YTEXTFORMAT(L" (%s)", shortcut.c_str()))
			, YtriaTranslate::Do(BackstagePanelAutomation_OnInitDialog_10, _YLOC("Edit Automation Shortcuts")).c_str()
			, m_imagelist);
	}

	SetResize(ID_BACKSTAGE_AUTOMATION_LOAD				, XTP_ATTR_HORREPOS(0) + XTP_ATTR_HORRESIZE(.25f));//CXTPResizeRect(0, 1.f, .25f, 1.f));
	SetResize(ID_BACKSTAGE_AUTOMATION_RECORDER			, XTP_ATTR_HORREPOS(.25f) + XTP_ATTR_HORRESIZE(.25f));//CXTPResizeRect(.25f, 1.f, .5f, 1.f));
	SetResize(ID_BACKSTAGE_AUTOMATION_CONSOLE			, XTP_ATTR_HORREPOS(.5f) + XTP_ATTR_HORRESIZE(.25f));//CXTPResizeRect(.5f, 1.f, .75f, 1.f));
	SetResize(ID_BACKSTAGE_AUTOMATION_QUICKLAUNCH		, XTP_ATTR_HORREPOS(.75f) + XTP_ATTR_HORRESIZE(.25f));//CXTPResizeRect(.75f, 1.f, 1.f, 1.f));
	SetResize(IDC_BACKSTAGE_RECENT_LIST					, XTP_ATTR_RESIZE(1.f));
	SetResize(ID_BACKSTAGE_AUTOMATION_RECENTSEPARATOR	, XTP_ATTR_HORRESIZE(1.f));
	SetResize(ID_BACKSTAGE_AUTOMATION_RECENTCAPTION		, XTP_ATTR_HORRESIZE(1.f));

	return TRUE;
}

void BackstagePanelAutomation::OnRecentFileListClick()
{
	if (isAuthorizedByLicense(_T("Running custom jobs is only authorized by a pro license.")))
	{
		ASSERT(m_RecentList);
		if (m_RecentList)
		{
			const int command = m_RecentList->GetSelectedItemCommand();
			ASSERT(command > 0);
			if (command > 0)
			{
				// Save pin state changes
				saveRecentListPinStates();

				// TODO multi list management in AutomatedMainFrame::OnCmdMsg with frame ID-indexed map of ID lists
				// getMainWindow()->SendMessage(WM_COMMAND, command);
				// TEMP - execute directly	
				AutomatedApp* app = dynamic_cast<AutomatedApp*>(AfxGetApp());
				ASSERT(nullptr != app);
				if (nullptr != app)
				{
					CloseBackstage();
					if (nullptr != m_Frame)
						setExecutionContext(m_Frame->GetAutomationContext());
					app->Automate(m_RecentList->GetSelectedItemPath(), true, true);
				}

				// Refresh recent list and buttons.
				updateRecentList();
				updateButtons();
			}
		}
	}
}

void BackstagePanelAutomation::setExecutionContext(const AutomationContext& autoContext)
{
	AutomatedApp* app = dynamic_cast<AutomatedApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != app)
		app->SetAutomationContext(autoContext);
}

bool BackstagePanelAutomation::isAuthorizedByLicense(const wstring& p_Message) const
{
	auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(nullptr != app);
	const auto authorizedByLicense = nullptr != app && app->IsLicensePro();
	if (!authorizedByLicense)
	{
		DlgLicenseMessageBoxHTML dlg(nullptr,
			DlgMessageBox::eIcon_ExclamationWarning,
			p_Message,
			_YTEXT(""),
			_YTEXT(""),
			{ { IDOK, YtriaTranslate::Do(LicenseManager_isVendorInitOK_1, _YLOC("OK")).c_str() } },
			{ 0, _YTEXT("") },
			app->GetApplicationColor());
		dlg.DoModal();
	}

	return authorizedByLicense;
}

void BackstagePanelAutomation::OnLoad()
{
	if (isAuthorizedByLicense(_T("Running custom jobs is only authorized by a pro license.")))
	{
		// Save pin state changes
		saveRecentListPinStates();

		CloseBackstage();

		if (nullptr != m_Frame)
			getMainWindow()->LoadAutomationXML(m_Frame, [autoContext = m_Frame->GetAutomationContext()]()
		{
			BackstagePanelAutomation::setExecutionContext(autoContext);
		});
		else
			getMainWindow()->SendMessage(WM_COMMAND, ID_FILE_LOADAUTOMATIONXML);

		// Refresh recent list and buttons.
		updateRecentList();
		updateButtons();
	}
}

void BackstagePanelAutomation::OnRecorder()
{
	if (isAuthorizedByLicense(_T("Recording custom jobs is only authorized by a pro license.")))
	{
		getMainWindow()->SendMessage(WM_COMMAND, ID_AUTOMATION_RECORDER);
		updateButtons();
	}
}

void BackstagePanelAutomation::OnConsole()
{
	getMainWindow()->SendMessage(WM_COMMAND, ID_AUTOMATION_VIEWCONSOLE);
	updateButtons();
}

void BackstagePanelAutomation::OnQuickLaunch()
{
	if (isAuthorizedByLicense(_T("Creating custom shortcuts is only authorized by a pro license.")))
	{
		getMainWindow()->SendMessage(WM_COMMAND, ID_AUTOMATION_SHORTCUTS);
		updateButtons();
	}
}

BOOL BackstagePanelAutomation::OnSetActive()
{
	updateRecentList();
	updateButtons();

	return TRUE;
}

BOOL BackstagePanelAutomation::OnKillActive()
{
	saveRecentListPinStates();

	return TRUE;
}

void BackstagePanelAutomation::updateButtons()
{
	if (IsWindow(m_hWnd))
	{
		YTestCmdUI cmdUI;

		cmdUI.m_nID = ID_FILE_LOADAUTOMATIONXML;
		cmdUI.m_bEnabled = TRUE;
		getMainWindow()->OnCmdMsg(cmdUI.m_nID, CN_UPDATE_COMMAND_UI, &cmdUI, NULL);
		m_LoadFileButton.EnableWindow(cmdUI.m_bEnabled);

		cmdUI.m_nID = ID_AUTOMATION_RECORDER;
		cmdUI.m_bEnabled = TRUE;
		getMainWindow()->OnCmdMsg(cmdUI.m_nID, CN_UPDATE_COMMAND_UI, &cmdUI, NULL);
		m_RecorderButton.EnableWindow(cmdUI.m_bEnabled);
		m_RecorderButton.ShowWindow(CRMpipe().IsFeatureSandboxed() ? TRUE : FALSE);// remove when recorder is ready

		cmdUI.m_nID = ID_AUTOMATION_VIEWCONSOLE;
		cmdUI.m_bEnabled = TRUE;
		getMainWindow()->OnCmdMsg(cmdUI.m_nID, CN_UPDATE_COMMAND_UI, &cmdUI, NULL);
		m_ConsoleButton.EnableWindow(cmdUI.m_bEnabled);

		cmdUI.m_nID = ID_AUTOMATION_SHORTCUTS;
		cmdUI.m_bEnabled = TRUE;
		getMainWindow()->OnCmdMsg(cmdUI.m_nID, CN_UPDATE_COMMAND_UI, &cmdUI, NULL);
		m_QuickLaunchButton.EnableWindow(cmdUI.m_bEnabled);
		m_QuickLaunchButton.ShowWindow(dynamic_cast<CodeJockFrameBase*>(AfxGetApp()->m_pMainWnd) == m_Frame ? TRUE : FALSE);
	}
}	

void BackstagePanelAutomation::updateRecentList()
{
	ASSERT(nullptr != m_RecentList);
	if (nullptr != m_RecentList)
	{
		RecentAutomations* recentAutomation = nullptr;
		if (nullptr == m_Frame)
		{
			AutomatedApp* app = dynamic_cast<AutomatedApp*>(AfxGetApp());
			ASSERT(nullptr != app);
			if (nullptr != app)
				recentAutomation = app->GetRecentAutomationManager();
		}
		else if (IsWindow(m_Frame->m_hWnd))
		{
			recentAutomation = m_Frame->GetRecentAutomationManager();
		}

		if (nullptr != recentAutomation)
		{
			recentAutomation->RefreshCommandIDMap();
			m_RecentList->SetRecentOpeningManager(recentAutomation);
			m_RecentList->BuildItems();
		}
	}
}

void BackstagePanelAutomation::saveRecentListPinStates()
{
	ASSERT(nullptr != m_RecentList);
	if (nullptr != m_RecentList)
		m_RecentList->SavePinStates();
}

MainFrame* BackstagePanelAutomation::getMainWindow() const
{
	return dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
}
