#include "BackstageTabPanelRoleDelegationConfigurationDelegation.h"

#include "BackstageLogger.h"
#include "BusinessObjectManager.h"
#include "BusinessUser.h"
#include "DlgAddItemsToGroups.h"
#include "DlgRoleEditFilter.h"
#include "DlgRoleEditAddFilter.h"
#include "DlgRoleEditDelegation.h"
#include "DlgRoleEditKey.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "RoleDelegationManager.h"
#include "Sapio365Session.h"
#include "SmartDlgProgress.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTranslate.h"
#include "QueryCreateRole.h"
#include "QueryUpdateRole.h"

BEGIN_MESSAGE_MAP(BackstageTabPanelRoleDelegationConfigurationDelegation, BackstageTabPanelRoleDelegationConfigurationBase<RoleDelegationCfgDelegationGrid>)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_ADD_FILTER,		OnAddFilter)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_REMOVE_FILTER,	OnRemoveFilter)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_ADD_MEMBER,		OnAddMember)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_REMOVE_MEMBER,	OnRemoveMember)
END_MESSAGE_MAP()

BackstageTabPanelRoleDelegationConfigurationDelegation::BackstageTabPanelRoleDelegationConfigurationDelegation()
	: BackstageTabPanelRoleDelegationConfigurationBase<RoleDelegationCfgDelegationGrid>()
{
	m_Grid = std::make_shared<RoleDelegationCfgDelegationGrid>();

	m_LambdaCredentialAdder = std::make_unique<LambdaCredentialAdder>([this](CWnd* p_Parent)
	{
		RoleDelegationUtil::BusinessKey newKey;

		auto session = getSession();
		ASSERT(session);

		DlgRoleEditKey dlg(session, p_Parent);
		if (IDOK == dlg.DoModal())
		{
			auto key = dlg.GetKey();
			key.m_Status = RoleDelegationUtil::RoleStatus::STATUS_OK;
			if (RoleDelegationManager::GetInstance().Write(key, session) != RoleDelegationManager::ErrorSource::ERROR_SQLITE && key.m_ID)
				newKey = RoleDelegationManager::GetInstance().GetKey(*key.m_ID, getSession());
		}

		return newKey;
	});
}

BackstageTabPanelRoleDelegationConfigurationDelegation::~BackstageTabPanelRoleDelegationConfigurationDelegation()
{
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::DoDataExchange(CDataExchange* pDX)
{
	BackstageTabPanelRoleDelegationConfigurationBase::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_ADD_FILTER,		m_BtnAddFilter);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_REMOVE_FILTER,	m_btnRemoveFilter);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_ADD_MEMBER,		m_BtnAddMember);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_REMOVE_MEMBER,	m_btnRemoveMember);
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::OnInitDialogSpecific()
{
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_ADD_FILTER };
		m_Imagelist.SetIcons(IDC_BACKSTAGE_BTN_ROLE_ADD_FILTER, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnAddFilter, YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_OnInitDialogSpecific_1, _YLOC("Assign Scopes...")).c_str(), _YTEXT(""), m_Imagelist);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_REMOVE_FILTER };
		m_Imagelist.SetIcons(IDC_BACKSTAGE_BTN_ROLE_REMOVE_FILTER, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_btnRemoveFilter, YtriaTranslate::Do(ColumnSelectionGrid_OnCustomPopupMenu_5, _YLOC("Remove")).c_str(), _YTEXT(""), m_Imagelist);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_ADD_MEMBER };
		m_Imagelist.SetIcons(IDC_BACKSTAGE_BTN_ROLE_ADD_MEMBER, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnAddMember, YtriaTranslate::Do(GridDirectoryRoles_InitializeCommands_14, _YLOC("Assign Users...")).c_str(), _YTEXT(""), m_Imagelist);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_REMOVE_MEMBER };
		m_Imagelist.SetIcons(IDC_BACKSTAGE_BTN_ROLE_REMOVE_MEMBER, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_btnRemoveMember, YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_OnInitDialogSpecific_4, _YLOC("Un-assign")).c_str(), _YTEXT(""), m_Imagelist);
	}

	BackstagePanelBase::SetupButton(m_BtnAdd, YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_OnInitDialogSpecific_5, _YLOC("Create Role...")).c_str(), _YTEXT(""), m_Imagelist);
	BackstagePanelBase::SetupButton(m_BtnEdit, YtriaTranslate::Do(PanelACL_OnRclickLstRoles_2, _YLOC("Edit Role...")).c_str(), _YTEXT(""), m_Imagelist);
	BackstagePanelBase::SetupButton(m_BtnRemove, YtriaTranslate::Do(PanelACL_OnRclickLstRoles_3, _YLOC("Delete Role")).c_str(), _YTEXT(""), m_Imagelist);

	m_Grid->SetOnDoubleClickRowFunction(std::bind(&BackstageTabPanelRoleDelegationConfigurationDelegation::customDoubleClick, this));

	AddAnchor(IDC_BACKSTAGE_BUTTON_ROLE_ADD,			CSize(0, 0), CSize(14, 0));
	AddAnchor(IDC_BACKSTAGE_BUTTON_ROLE_EDIT,			CSize(14, 0), CSize(28, 0));
	AddAnchor(IDC_BACKSTAGE_BUTTON_ROLE_REMOVE,			CSize(28, 0), CSize(42, 0));

	AddAnchor(IDC_BACKSTAGE_BUTTON_ROLE_ADD_FILTER,		CSize(44, 0), CSize(58, 0));
	AddAnchor(IDC_BACKSTAGE_BUTTON_ROLE_REMOVE_FILTER,	CSize(58, 0), CSize(72, 0));
	AddAnchor(IDC_BACKSTAGE_BUTTON_ROLE_ADD_MEMBER,		CSize(72, 0), CSize(86, 0));
	AddAnchor(IDC_BACKSTAGE_BUTTON_ROLE_REMOVE_MEMBER,	CSize(86, 0), CSize(100, 0));
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::setThemeSpecific(const XTPControlTheme nTheme)
{
	m_BtnAddFilter.SetTheme(nTheme);
	m_btnRemoveFilter.SetTheme(nTheme);
	m_BtnAddMember.SetTheme(nTheme); 
	m_btnRemoveMember.SetTheme(nTheme);
}

auto BackstageTabPanelRoleDelegationConfigurationDelegation::retrieveSkus()
{
	auto bom = std::make_shared<BusinessObjectManager>(getSession());
	return bom->GetSubscribedSkus(YtriaTaskData()).GetTask().get();
}

auto BackstageTabPanelRoleDelegationConfigurationDelegation::getAlreadyAssignedSkus() const
{
	std::map<wstring, int32_t> assignedSkus;

	const auto roleIDs = m_Grid->GetAllRoleDelegationIDs();
	for (const auto& roleID : roleIDs)
	{
		auto delegation = RoleDelegationManager::GetInstance().GetDelegation(roleID, getSession());
		for (const auto& skuLimit : delegation.GetSkuAccessLimits())
		{
			if (skuLimit.second >= 0)
				assignedSkus[skuLimit.first] += skuLimit.second;
		}
	}

	return assignedSkus;
}

bool BackstageTabPanelRoleDelegationConfigurationDelegation::writeDelegation(RoleDelegation& p_Delegation)
{
	RoleDelegationManager::GetInstance().RemoveFromCache(p_Delegation.GetID());

	RoleDelegationManager::ErrorSource writeError = RoleDelegationManager::GetInstance().Write(p_Delegation.GetDelegation(), getSession());

	bool rvSuccess = RoleDelegationManager::ErrorSource::ERROR_NONE == writeError;

	if (RoleDelegationManager::ErrorSource::ERROR_SQLITE != writeError)
		for (auto& p : p_Delegation.GetPrivileges())
		{
			ASSERT(p_Delegation.GetDelegation().m_ID.is_initialized());
			if (p_Delegation.GetDelegation().m_ID.is_initialized())
			{
				p.m_IDDelegation	= *p_Delegation.GetDelegation().m_ID;
				writeError			= RoleDelegationManager::GetInstance().Write(p, getSession());
				rvSuccess			= rvSuccess && RoleDelegationManager::ErrorSource::ERROR_NONE == writeError;
			}
		}

	return rvSuccess;
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::add()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		DlgRoleEditDelegation dlg(RoleDelegationManager::GetInstance().GetKeys(getSession()), getAlreadyAssignedSkus(), getSession(), this);
		dlg.SetCredentialAdder(m_LambdaCredentialAdder);

		if (IDOK == dlg.DoModal())
		{
			CWaitCursor _;

			auto delegation = dlg.GetDelegation();
			delegation.GetDelegation().m_Status = RoleDelegationUtil::RoleStatus::STATUS_OK;
			writeDelegation(delegation);

			QueryCreateRole queryCreateRole(delegation.GetID(), delegation.GetDelegation().m_Name, SessionsSqlEngine::Get());
			queryCreateRole.Run();
			ASSERT(queryCreateRole.IsSuccess());

			m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::edit()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		auto delegationIDs = m_Grid->GetRoleDelegationIDsFromSelection();
		if (!delegationIDs.empty())
		{
			auto delegationID = delegationIDs.front();
			ASSERT(delegationID > 0);
			if (delegationID > 0)
			{
				auto delegation = RoleDelegationManager::GetInstance().LoadRoleDelegationFromSQLite(delegationID, getSession());
				ASSERT(delegation.GetDelegation().IsValid());
				if (delegation.GetDelegation().IsValid())
				{
					DlgRoleEditDelegation dlg(RoleDelegationManager::GetInstance().GetKeys(getSession()), getAlreadyAssignedSkus(), getSession(), this);
					dlg.SetDelegation(delegation);
					dlg.SetCredentialAdder(m_LambdaCredentialAdder);

					if (IDOK == dlg.DoModal())
					{
						CWaitCursor _;

						QueryUpdateRole roleUpdater(dlg.GetDelegation().GetID(), dlg.GetDelegation().GetDelegation().m_Name, SessionsSqlEngine::Get());
						roleUpdater.Run();
						ASSERT(roleUpdater.GetStatus());
						// Update role name in mainframe ribbon
						MainFrame* mainFrame = dynamic_cast<MainFrame*>(::AfxGetMainWnd());
						ASSERT(nullptr != mainFrame);
						if (nullptr != mainFrame)
							mainFrame->PopulateRecentSessions();

						auto newDelegation = dlg.GetDelegation();
						writeDelegation(newDelegation);
						m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
					}
				}
			}
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::remove()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		auto selectedDelegationIDs = m_Grid->GetRoleDelegationIDsFromSelection();
		if (!selectedDelegationIDs.empty())
		{
			YCodeJockMessageBox dlg(this,
									DlgMessageBox::eIcon_Question,
									YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_remove_3, _YLOC("%1 role(s) selected for deletion\nAre you sure?"), Str::getStringFromNumber<size_t>(selectedDelegationIDs.size()).c_str()),
									_YTEXT(""),
									_YTEXT(""),
									{ { IDOK, YtriaTranslate::Do(BackstagePanelSettings_OnReset_3, _YLOC("Yes")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
			if (IDOK == dlg.DoModal())
			{
				for (const auto& delegationID : selectedDelegationIDs)
				{
					auto delegation = RoleDelegationManager::GetInstance().GetRoleDelegationSQLite().ReadDelegation(delegationID, getSession());
					delegation.m_Delegation.m_Status = RoleDelegationUtil::STATUS_REMOVED;
					RoleDelegationManager::GetInstance().Write(delegation.m_Delegation, getSession());

					// cancel filter associations
					auto delegationFilters = RoleDelegationManager::GetInstance().GetRoleDelegationSQLite().ReadDelegationFilters(delegationID, getSession());
					for (auto& df : delegationFilters)
					{
						df.m_DelegationFilter.m_Status = RoleDelegationUtil::STATUS_REMOVED;
						RoleDelegationManager::GetInstance().Write(df.m_DelegationFilter, getSession());
					}

					// cancel member associations
					auto delegationMembers = RoleDelegationManager::GetInstance().GetRoleDelegationSQLite().ReadDelegationMembers(delegationID, getSession());
					for (auto& du : delegationMembers)
					{
						du.m_DelegationMember.m_Status = RoleDelegationUtil::STATUS_REMOVED;
						RoleDelegationManager::GetInstance().Write(du.m_DelegationMember, getSession());
					}
				}

				m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
			}
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::OnAddFilter()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		auto IDsDelegation = m_Grid->GetRoleDelegationIDsFromSelection();
		if (!IDsDelegation.empty())
		{
			vector<RoleDelegation> selectedDelegations;
			for (const auto id : IDsDelegation)
			{
				auto rd = RoleDelegationManager::GetInstance().LoadRoleDelegationFromSQLite(id, getSession());
				ASSERT(rd.IsLoaded());
				if (rd.IsLoaded())
					selectedDelegations.push_back(rd);
			}
			ASSERT(!selectedDelegations.empty());
			if (!selectedDelegations.empty())
			{
				DlgRoleEditAddFilter dlg(selectedDelegations, getSession(), this);
				if (IDOK == dlg.DoModal())
				{
					bool updateGrid = false;
					bool updateRD;

					const auto& filterIDNames = dlg.GetSelectedFiltersIDNames();
					SmartDlgProgress prog(new DlgProgress(_T("Adding scope(s) to Role(s)"), _T("Processing..."), static_cast<int>(selectedDelegations.size() * filterIDNames.size()), true, this));
					for (auto& rd : selectedDelegations)
					{
						updateRD = false;
						for (const auto& fp : filterIDNames)
						{
							const auto& fid		= fp.first;
							const auto& fName	= fp.second;
							prog->IncrementGaugePosition(_YFORMAT(L"Adding scope '%s' to Role: %s", fName.c_str(), rd.GetDelegation().m_Name.c_str()));
							if (!rd.HasFilter(fid))
							{
								RoleDelegationUtil::BusinessDelegationFilter bdf;
								bdf.m_Name			= _YFORMAT(L"For role: %s :: %s", rd.GetDelegation().m_Name.c_str(), fName.c_str());
								bdf.m_Info			= fName;
								bdf.m_IDDelegation	= rd.GetDelegation().m_ID.get();
								bdf.m_IDFilter		= fid;
								bdf.m_Status		= RoleDelegationUtil::RoleStatus::STATUS_OK;
								RoleDelegationManager::GetInstance().Write(bdf, getSession());
								updateRD	= true;
								updateGrid	= true;
							}
						}

						if (updateRD)
							writeDelegation(rd);
					}

					if (updateGrid)
						m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
				}
			}
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::OnRemoveFilter()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		vector<GridBackendRow*> selectedNonGroupRows;
		m_Grid->GetSelectedNonGroupRows(selectedNonGroupRows);

		auto IDsByType	= m_Grid->GetElementIDsFromSelection(selectedNonGroupRows);
		auto findIt		= IDsByType.find(m_Grid->GetTypeFilter());

		if (IDsByType.end() != findIt && !findIt->second.empty())
		{
			YCodeJockMessageBox dlg(this,
									DlgMessageBox::eIcon_Question,
									YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_OnRemoveFilter_3, _YLOC("%1 scope(s) selected for removal\nAre you sure?"), Str::getStringFromNumber<size_t>(findIt->second.size()).c_str()),
									_YTEXT(""),
									_YTEXT(""),
									{ { IDOK, YtriaTranslate::Do(BackstagePanelSettings_OnReset_3, _YLOC("Yes")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
			if (IDOK == dlg.DoModal())
			{
				map<int64_t, set<int64_t>> filtersToPurgeByDelegation;
				for (const auto& fidP : findIt->second)
					filtersToPurgeByDelegation[fidP.second].insert(fidP.first);

				map<int64_t, RoleDelegation> rdToSave;
				{
					SmartDlgProgress prog(new DlgProgress(_T("Removing selected Scope(s) from Role(s)"), _T("Processing..."), static_cast<int>(filtersToPurgeByDelegation.size()), true, this));
					for (const auto& p : filtersToPurgeByDelegation)
					{
						auto rd = RoleDelegationManager::GetInstance().LoadRoleDelegationFromSQLite(p.first, getSession());

						prog->IncrementGaugePosition(_YFORMAT(L"Removing Scope(s) from Role: %s", rd.GetDelegation().m_Name.c_str()));
						auto delegationFilters = RoleDelegationManager::GetInstance().GetRoleDelegationSQLite().ReadDelegationFilters(p.first, getSession());// delegation ID
						for (auto& df : delegationFilters)
							if (p.second.find(df.m_DelegationFilter.m_IDFilter) != p.second.end())
							{
								df.m_DelegationFilter.m_Status = RoleDelegationUtil::STATUS_REMOVED;
								RoleDelegationManager::GetInstance().Write(df.m_DelegationFilter, getSession());
								rdToSave[rd.GetID()] = rd;
							}
					}
				}

				CWaitCursor _;
				for (auto& rd : rdToSave)
					writeDelegation(rd.second);

				if (!rdToSave.empty())
					m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
			}
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::OnAddMember()
{
	ASSERT(m_Grid);
	std::shared_ptr<Sapio365Session> session = getSession();
	ASSERT(session);
	if (session && m_Grid)
	{
		CWaitCursor interminable;

		const auto IDsDelegation = m_Grid->GetRoleDelegationIDsFromSelection();
		if (!IDsDelegation.empty())
		{
			vector<RoleDelegation> selectedDelegations;
			for (const auto id : IDsDelegation)
			{
				const auto rd = RoleDelegationManager::GetInstance().LoadRoleDelegationFromSQLite(id, getSession());
				ASSERT(rd.IsLoaded());
				if (rd.IsLoaded())
					selectedDelegations.push_back(rd);
			}
			ASSERT(!selectedDelegations.empty());
			if (!selectedDelegations.empty())
			{
				std::map<PooledString, std::pair<PooledString, std::set<PooledString>>> parentsForSelection;

				{
					CWaitCursor c;
					for (const auto& rd : selectedDelegations)
					{
						auto& item = parentsForSelection[Str::getStringFromNumber(rd.GetID())];
						item.first = rd.GetDelegation().m_Name;
						for (const auto& m : rd.GetMembers())
							item.second.insert(m.m_UserID);
					}
				}

				DlgAddItemsToGroups dlg(session,
										parentsForSelection,
										DlgAddItemsToGroups::SELECT_USERS_FOR_ROLEDELEGATION,
										{},
										{},
										{},
										YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_OnAddMember_1, _YLOC("Assign Users to Roles")).c_str(),
										YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_OnAddMember_2, _YLOC("Selected users from the list below will be assigned to the selected roles.")).c_str(),
										RoleDelegationUtil::RBAC_NONE,
										this);

				if (IDOK == dlg.DoModal())
				{
					CWaitCursor c;
					auto& newMembers	= dlg.GetSelectedItems();
					bool updateGrid		= false;
					bool updateRD;
					{
						SmartDlgProgress prog(new DlgProgress(_T("Adding Member(s) to selected Role(s)"), _T("Processing..."), static_cast<int>(selectedDelegations.size() * newMembers.size()), true, this));
						for (auto& rd : selectedDelegations)
						{
							updateRD = false;
							for (const auto& member : newMembers)
							{
								prog->IncrementGaugePosition(_YFORMAT(L"Adding Member(s) to Role: %s", rd.GetDelegation().m_Name.c_str()));
								if (!rd.HasMember(member.GetID()))
								{
									RoleDelegationUtil::BusinessDelegationMember bdm;
									bdm.m_IDDelegation		= rd.GetID();
									bdm.m_UserName			= member.GetDisplayName();
									bdm.m_UserPrincipalName = member.GetPrincipalName();
									bdm.m_UserID			= member.GetID();
									bdm.m_Status			= RoleDelegationUtil::RoleStatus::STATUS_OK;
									bdm.m_Info				= YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_OnAddMember_3, _YLOC("assigned manually")).c_str(); //??
									bdm.m_Name				= _YFORMAT(L"For role: %s :: %s - %s", rd.GetDelegation().m_Name.c_str(), bdm.m_UserName.c_str(), bdm.m_UserPrincipalName.c_str());
									RoleDelegationManager::GetInstance().Write(bdm, getSession());
									updateRD = true;
									updateGrid = true;
								}
							}

							if (updateRD)
								writeDelegation(rd);
						}
					}

					if (updateGrid)
						m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
				}

			}
		}
	}
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::OnRemoveMember()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		vector<GridBackendRow*> selectedNonGroupRows;
		m_Grid->GetSelectedNonGroupRows(selectedNonGroupRows);

		auto IDsByType = m_Grid->GetElementIDsFromSelection(selectedNonGroupRows);
		auto findIt = IDsByType.find(m_Grid->GetTypeMember());

		if (IDsByType.end() != findIt && !findIt->second.empty())
		{
			YCodeJockMessageBox dlg(this,
									DlgMessageBox::eIcon_Question,
									YtriaTranslate::Do(BackstageTabPanelRoleDelegationConfigurationDelegation_OnRemoveMember_3, _YLOC("%1 user(s) selected to un-assign\nAre you sure?"), Str::getStringFromNumber<size_t>(findIt->second.size()).c_str()),
									_YTEXT(""),
									_YTEXT(""),
									{ { IDOK, YtriaTranslate::Do(BackstagePanelSettings_OnReset_3, _YLOC("Yes")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
			if (IDOK == dlg.DoModal())
			{
				map<int64_t, set<int64_t>> membersToPurgeByDelegation;
				for (const auto& fidP : findIt->second)
					membersToPurgeByDelegation[fidP.second].insert(fidP.first);

				map<int64_t, RoleDelegation> rdToSave;
				{
					SmartDlgProgress prog(new DlgProgress(_T("Removing selected Member(s) from Role(s)"), _T("Processing..."), static_cast<int>(membersToPurgeByDelegation.size()), true, this));
					for (const auto& p : membersToPurgeByDelegation)
					{
						auto rd = RoleDelegationManager::GetInstance().LoadRoleDelegationFromSQLite(p.first, getSession());

						prog->IncrementGaugePosition(_YFORMAT(L"Removing selected member(s) from role: %s", rd.GetDelegation().m_Name.c_str()));
						auto delegationMembers = RoleDelegationManager::GetInstance().GetRoleDelegationSQLite().ReadDelegationMembers(p.first, getSession());// delegation ID
						for (auto& dm : delegationMembers)
						{
							ASSERT(dm.m_DelegationMember.m_ID.is_initialized());
							if (dm.m_DelegationMember.m_ID.is_initialized() && p.second.find(dm.m_DelegationMember.m_ID.get()) != p.second.end())
							{
								dm.m_DelegationMember.m_Status = RoleDelegationUtil::STATUS_REMOVED;
								RoleDelegationManager::GetInstance().Write(dm.m_DelegationMember, getSession());
								rdToSave[rd.GetID()] = rd;
							}
						}
					}
				}

				CWaitCursor _;
				for (auto& rd : rdToSave)
					writeDelegation(rd.second);

				if (!rdToSave.empty())
					m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
			}
		}
	}
}

std::shared_ptr<Sapio365Session> BackstageTabPanelRoleDelegationConfigurationDelegation::getSession() const
{
	return MainFrameSapio365Session();
}

const PersistentSession& BackstageTabPanelRoleDelegationConfigurationDelegation::getSessionInfo() const
{
	PersistentSession g_FakeNews;

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->GetMainWnd());
	ASSERT(nullptr != mainFrame);
	return nullptr == mainFrame ? g_FakeNews : mainFrame->GetSessionInfo();
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::updateButtons(const vector<GridBackendRow*>& p_SelectedNonGroupRows)
{
	m_BtnEdit.EnableWindow(FALSE);
	m_BtnRemove.EnableWindow(FALSE);
	m_BtnAddFilter.EnableWindow(FALSE);
	m_btnRemoveFilter.EnableWindow(FALSE);
	m_BtnAddMember.EnableWindow(FALSE);
	m_btnRemoveMember.EnableWindow(FALSE);

	ASSERT(m_Grid);
	if (m_Grid)
	{
		auto IDsByType = m_Grid->GetElementIDsFromSelection(p_SelectedNonGroupRows);
		BOOL selection = !IDsByType.empty();
		for (const auto& p : IDsByType)
		{
			if (MFCUtil::StringMatchW(p.first, m_Grid->GetTypeMember()))
				m_btnRemoveMember.EnableWindow();
			else if (MFCUtil::StringMatchW(p.first, m_Grid->GetTypeFilter()))
				m_btnRemoveFilter.EnableWindow();
		}

		m_BtnRemove.EnableWindow(selection);
		m_BtnEdit.EnableWindow(selection);
		m_BtnRemove.EnableWindow(selection);
		m_BtnAddFilter.EnableWindow(selection);
		m_BtnAddMember.EnableWindow(selection);
	}
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::customDoubleClick()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		const auto tid = m_Grid->GetRowObjectType(m_Grid->GetBackend().GetLastFocusedRow());
		if (RoleDelegationCfgDelegationGrid::g_ElementTypeIDDelegation == tid ||
			RoleDelegationCfgDelegationGrid::g_ElementTypeIDPrivilegesParent == tid ||
			RoleDelegationCfgDelegationGrid::g_ElementTypeIDPrivilege == tid ||
			RoleDelegationCfgDelegationGrid::g_ElementTypeIDLicensePoolsParent == tid ||
			RoleDelegationCfgDelegationGrid::g_ElementTypeIDLicensePool == tid)
			OnEdit();
		else if (RoleDelegationCfgDelegationGrid::g_ElementTypeIDMembersParent == tid ||
				 RoleDelegationCfgDelegationGrid::g_ElementTypeIDMember == tid)
			OnAddMember();
		else if (RoleDelegationCfgDelegationGrid::g_ElementTypeIDFiltersParent == tid)
			OnAddFilter();
		else if (RoleDelegationCfgDelegationGrid::g_ElementTypeIDFilter == tid)
			editFilterFromFocusedRow();
		else
			ASSERT(false);
	}
}

void BackstageTabPanelRoleDelegationConfigurationDelegation::editFilterFromFocusedRow()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		const auto	typeAndID	= m_Grid->GetElementIDsFromSelection({m_Grid->GetBackend().GetLastFocusedRow()});		
		auto		findIt		= typeAndID.find(m_Grid->GetTypeFilter());
		ASSERT(typeAndID.end() != findIt);
		if (typeAndID.end() != findIt)
		{
			ASSERT(findIt->second.size() == 1);
			if (findIt->second.size() == 1)
			{
				auto filterID	= findIt->second.front().first;
				auto roleFilter = RoleDelegationManager::GetInstance().GetFilter(filterID, getSession());
				ASSERT(roleFilter.IsValid());
				if (roleFilter.IsValid())
				{
					DlgRoleEditFilter dlg(this);
					dlg.SetFilter(roleFilter);
					if (IDOK == dlg.DoModal())
					{
						auto roleFilterEdited = dlg.GetFilter();
						RoleDelegationManager::GetInstance().Write(roleFilterEdited, getSession());
						m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
					}
				}
			}
		}
	}
}