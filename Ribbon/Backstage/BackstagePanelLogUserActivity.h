#pragma once

#include "BackstagePanelBase.h"
#include "BackstageTabPanelLogUserActivityCloud.h"
#include "BackstageTabPanelLogUserActivityLocal.h"
#include "..\..\Resource.h"

class BackstagePanelLogUserActivity : public BackstagePanelBase
{
public:
	BackstagePanelLogUserActivity();
	~BackstagePanelLogUserActivity() override;

	enum { IDD = IDD_BACKSTAGEPAGE_LOG_USERACTIVITY };

	void DoDataExchange(CDataExchange* pDX);

	virtual void SetTheme(const XTPControlTheme nTheme) override;

	virtual BOOL OnInitDialog() override;
	virtual BOOL OnSetActive() override;
	virtual BOOL OnKillActive() override;

	void ClearGrids();

protected:
	DECLARE_MESSAGE_MAP();

private:
	afx_msg void OnBnClickedBackstageButtonUseractivityShowLocal();
	afx_msg void OnBnClickedBackstageButtonUseractivityShowCloud();
	afx_msg void OnReload();// from cloud

	afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);

	std::shared_ptr<Sapio365Session> getSession() const;

	void showPanel(const int p_PanelIndex); // Hides every panel whose index is not p_PanelIndex

	CXTPRibbonBackstageButton	m_BtnShowLocal;
	CXTPRibbonBackstageButton	m_BtnShowCloud;
	CXTPRibbonBackstageButton	m_BtnReload;

	CStatic			 m_PseudoTab;
	CXTPImageManager m_ImageList;

	BackstageTabPanelLogUserActivityCloud m_TabPanelCloud;
	BackstageTabPanelLogUserActivityLocal m_TabPanelLocal;

	map<int, CWnd*>	m_Panels;

	CXTPBrush				m_brushButtonBack;
	static const COLORREF	g_ButtonBackColor;
public:
	afx_msg void OnBnClickedBackstageButtonUseractivityShowcloud2();
};