#pragma once

#include "BackstageTabPanelRoleDelegationConfigurationBase.h"
#include "BackstagePanelBase.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "..\Resource.h"
#include "RoleDelegationManager.h"

BEGIN_TEMPLATE_MESSAGE_MAP(BackstageTabPanelRoleDelegationConfigurationBase, RoleCacheGridClass, CWnd)
	ON_WM_CREATE()
	ON_WM_CTLCOLOR() // Copy pasted from CXTPRibbonBackstagePage for color handling
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_ADD,	OnAdd)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_EDIT,	OnEdit)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_REMOVE, OnRemove)
END_MESSAGE_MAP()

template <class RoleCacheGridClass>
BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::BackstageTabPanelRoleDelegationConfigurationBase() : 
	m_OnModifyPostProcessFunction(nullptr)
{
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::SetTheme(const XTPControlTheme nTheme)
{
	/* Copy pasted from CXTPRibbonBackstagePage::SetTheme for color handling */
	// get the background color from the selected theme if available.
	m_clrText = XTPIniColor(_YTEXT("CommandBars.Ribbon.Backstage"), _YTEXT("PageText"), ::GetSysColor(COLOR_WINDOWTEXT));
	m_clrBack = XTPIniColor(_YTEXT("CommandBars.Ribbon.Backstage"), _YTEXT("PageBackground"), ::GetSysColor(COLOR_WINDOW));

	// destroy the previous brush.
	if (m_xtpBrushBack.GetSafeHandle())
		m_xtpBrushBack.DeleteObject();

	// create a new brush.
	m_xtpBrushBack.CreateSolidBrush(m_clrBack);
	/**********************/

	m_BtnAdd.SetTheme(nTheme);
	m_BtnEdit.SetTheme(nTheme);
	m_BtnRemove.SetTheme(nTheme);
	setThemeSpecific(nTheme);
}

template <class RoleCacheGridClass>
HBRUSH BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::OnCtlColor(CDC* pDC, CWnd* /*pWnd*/, UINT /*nCtlColor*/)
{
	/* Copy pasted from CXTPRibbonBackstagePage::OnCtlColor for color handling */
	pDC->SetTextColor(m_clrText);
	pDC->SetBkColor(m_clrBack);
	return (HBRUSH)m_xtpBrushBack;
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::setThemeSpecific(const XTPControlTheme nTheme)
{
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::DoDataExchange(CDataExchange* pDX)
{
	CWnd::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_ADD,			m_BtnAdd);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_EDIT,		m_BtnEdit);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_REMOVE,		m_BtnRemove);
	DDX_Control(pDX, IDC_BACKSTAGE_GRID_AREA,				m_staticGridArea);
	DDX_Control(pDX, IDD_BACKSTAGEPAGE_ROLEDELEGATION_INFO,	m_Info);
}

template <class RoleCacheGridClass>
BOOL BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::OnInitDialog()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		m_Grid->AddSelectionObserver(this);
		m_Grid->SetOnDoubleClickRowFunction(std::bind(&BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::OnEdit, this));
	}

	BOOL rv = CDialog::OnInitDialog();

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_ADD };
		m_Imagelist.SetIcons(IDC_BACKSTAGE_BTN_ROLE_ADD, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnAdd, YtriaTranslate::Do(GLOBAL_ON_WM_CREATE_19, _YLOC("Create...")).c_str(), _YTEXT(""), m_Imagelist);
		m_BtnAdd.EnableWindow();
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_EDIT };
		m_Imagelist.SetIcons(IDC_BACKSTAGE_BTN_ROLE_EDIT, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnEdit, YtriaTranslate::Do(CacheGridAudit_OnCustomPopupMenu_1, _YLOC("Edit...")).c_str(), _YTEXT(""), m_Imagelist);
	}
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_REMOVE };
		m_Imagelist.SetIcons(IDC_BACKSTAGE_BTN_ROLE_REMOVE, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnRemove, YtriaTranslate::Do(ApplicationShortcuts_ApplicationShortcuts_166, _YLOC("Delete")).c_str(), _YTEXT(""), m_Imagelist);
	}

	if (m_Grid)
	{
		CRect aRect;
		m_staticGridArea.GetWindowRect(&aRect);
		m_staticGridArea.ShowWindow(SW_HIDE);
		ScreenToClient(&aRect);

		if (m_Grid->Create(this, aRect, IDC_BACKSTAGE_GRID))
		{
			m_Grid->ModifyStyle(0, WS_BORDER);
			m_Grid->ShowWindow(SW_SHOW);
			m_Grid->BringWindowToTop();
		}
		else
		{
			ASSERT(FALSE);
			rv = -1;
		}
	}

	SetBkColor(RGB(255, 255, 255));
	AddAnchor(IDC_BACKSTAGE_GRID,						CSize(0, 0), CSize(100, 100));
	AddAnchor(IDC_BACKSTAGE_BUTTON_ROLE_ADD,			CSize(0, 0), CSize(33, 0));
	AddAnchor(IDC_BACKSTAGE_BUTTON_ROLE_EDIT,			CSize(33, 0), CSize(67, 0));
	AddAnchor(IDC_BACKSTAGE_BUTTON_ROLE_REMOVE,			CSize(67, 0), CSize(100, 0));
	AddAnchor(IDD_BACKSTAGEPAGE_ROLEDELEGATION_INFO,	CSize(0, 100), CSize(100, 100));
	OnInitDialogSpecific();

	setInfo(_YTEXT(""));

	m_Grid->UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);// create headers

	return rv;
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::setInfo(const wstring& p_Info)
{
	CString markup;

	if (p_Info.empty())
	{
		m_Info.ShowWindow(SW_HIDE);
	}
	else
	{
		m_Info.ShowWindow(SW_SHOW);

		// case warning
		static const CString iconString = Str::getStringFromNumber(IDB_INFO_ROLEDELEGATION_WARNING).c_str();
		static const CString infoMarkup =
			L"<StackPanel Margin='0' Orientation='Horizontal' Background='#fafafa' VerticalAlignment='Top'>"
			L"	<StackPanel VerticalAlignment='Center' Orientation='Horizontal' Background='#fafafa' Margin='5'>"
			L"		<TextBlock VerticalAlignment='Center'><Image Source='res://#%s' VerticalAlignment='Center' Margin='10, 0, 0, 0' /></TextBlock>"
			L"		<TextBlock VerticalAlignment='Center' Margin='10, 0, 10, 0' Foreground='#262626' FontWeight='bold' FontSize='12'>%s</TextBlock>"
			L"	</StackPanel>"
			L"</StackPanel>"
			;
		markup.Format(infoMarkup, iconString, p_Info.c_str());
	}

	m_Info.SetMarkupText(markup);
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::OnSetActive()
{
	if (Sapio365Session::IsAdminRBAC(getSession()))
	{
		ASSERT(m_Grid);
		if (m_Grid)
		{
			m_Grid->SetIsActive(true);
			m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
			setInfo(m_Grid->IsSelectAllDecryptionFailure() ? YtriaTranslate::Do(GLOBAL_ON_WM_CREATE_17, _YLOC("Some configuration data set for a different Office365 tenant was not loaded in this grid")).c_str() : _YTEXT(""));
		}
	}
	else
	{
		ASSERT(false);
		YCodeJockMessageBox dlg(this,
								DlgMessageBox::eIcon_ExclamationWarning,
								YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationTop_OnSetActive_1, _YLOC("Only current Tenant's Global Administrator or sapio365 Administrators can configure Role-Based Access Control.")).c_str(),
								_YTEXT(""),
								_YTEXT(""),
								{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
		dlg.DoModal();
	}
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::OnKillActive()
{
	ASSERT(m_Grid);
	if (m_Grid)
		m_Grid->SetIsActive(false);
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::SetOnModifyPostProcessFunction(OnModifyPostProcessFunction p_OnModifyPostProcessFunction)
{
	m_OnModifyPostProcessFunction = p_OnModifyPostProcessFunction;
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::OnAdd()
{
	if (Sapio365Session::IsAdminRBAC(getSession()))
	{
		CWaitCursor _;

		add();
		RoleDelegationManager::GetInstance().ClearCache();

		if (nullptr != m_OnModifyPostProcessFunction)
			m_OnModifyPostProcessFunction();

		ASSERT(m_Grid);
		if (m_Grid)
			SelectionChanged(*m_Grid);
	}
	else
	{
		ASSERT(false);
		YCodeJockMessageBox dlg(this,
								DlgMessageBox::eIcon_ExclamationWarning,
								YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationTop_OnSetActive_1, _YLOC("Only current Tenant's Global Administrator or sapio365 Administrators can configure Role-Based Access Control.")).c_str(),
								_YTEXT(""),
								_YTEXT(""),
								{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
		dlg.DoModal();
	}
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::OnEdit()
{
	if (Sapio365Session::IsAdminRBAC(getSession()))
	{
		CWaitCursor _;

		edit();
		RoleDelegationManager::GetInstance().ClearCache();

		if (nullptr != m_OnModifyPostProcessFunction)
			m_OnModifyPostProcessFunction();

		ASSERT(m_Grid);
		if (m_Grid)
			SelectionChanged(*m_Grid);
	}
	else
	{
		ASSERT(false);
		YCodeJockMessageBox dlg(this,
								DlgMessageBox::eIcon_ExclamationWarning,
								YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationTop_OnSetActive_1, _YLOC("Only current Tenant's Global Administrator or sapio365 Administrators can configure Role-Based Access Control.")).c_str(),
								_YTEXT(""),
								_YTEXT(""),
								{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
		dlg.DoModal();
	}
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::OnRemove()
{
	if (Sapio365Session::IsAdminRBAC(getSession()))
	{
		CWaitCursor _;

		remove();
		RoleDelegationManager::GetInstance().ClearCache();

		if (nullptr != m_OnModifyPostProcessFunction)
			m_OnModifyPostProcessFunction();

		ASSERT(m_Grid);
		if (m_Grid)
			SelectionChanged(*m_Grid);
	}
	else
	{
		ASSERT(false);
		YCodeJockMessageBox dlg(this,
								DlgMessageBox::eIcon_ExclamationWarning,
								YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationTop_OnSetActive_1, _YLOC("Only current Tenant's Global Administrator or sapio365 Administrators can configure Role-Based Access Control.")).c_str(),
								_YTEXT(""),
								_YTEXT(""),
								{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
		dlg.DoModal();
	}
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::SelectionChanged(const CacheGrid& grid)
{
	vector<GridBackendRow*> selectedRows;
	grid.GetSelectedNonGroupRows(selectedRows);
	updateButtons(selectedRows);
}

template <class RoleCacheGridClass>
void BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::OnReload()
{
	ASSERT(m_Grid);
	if (m_Grid)
	{
		m_Grid->LoadSQLintoGrid(O365Krypter(getSession()));
		setInfo(m_Grid->IsSelectAllDecryptionFailure() ? YtriaTranslate::Do(GLOBAL_ON_WM_CREATE_18, _YLOC("Configuration data set for a different O365 tenant was found and not loaded in this grid")).c_str() : _YTEXT(""));
	}
}

template<class RoleCacheGridClass>
std::shared_ptr<const Sapio365Session> BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::getSession() const
{
	return MainFrameSapio365Session();
}

template<class RoleCacheGridClass>
bool BackstageTabPanelRoleDelegationConfigurationBase<RoleCacheGridClass>::IsSelectAllDecryptionFailure() const
{
	return m_Grid && m_Grid->IsSelectAllDecryptionFailure();
}
