#pragma once

#include "CacheGrid.h"
#include "FrameLogger.h"

class LogGrid : public CacheGrid
			  , public std::enable_shared_from_this<LogGrid>
			  , public FrameLogger
{
public:
	LogGrid(CFrameWnd& contextFrame, bool p_LogOnlyFromThisFrame);

	virtual void LogSpecific(const ILogger::LogEntry& p_Entry) const;
	void SetIsActive(const bool p_Active);

protected:
	virtual void customizeGrid() override;
	//virtual void postSetPaused() override;

private:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void InsertLogCallback(const ILogger::LogEntry& p_Entry);

	GridBackendColumn* m_LevelColumn;
	GridBackendColumn* m_MessageColumn;
	GridBackendColumn* m_DateReceivedColumn;

	static wstring getLogLevelText(LogLevel level);
	static const int64_t g_UpdateMinPeriodMs;

	static wstring g_StrDebugLevel;
	static wstring g_StrInfoLevel;
	static wstring g_StrWarningLevel;
	static wstring g_StrErrorLevel;
	static wstring g_StrFatalLevel;
	static wstring g_StrUserLevel;
	static bool g_HasLocalizedStaticMembers;
	static void LocalizeStaticMembers();

	class InsertLogMessage;
	friend class InsertLogMessage;

	bool m_Active;
	bool m_TimerRunning;
	bool m_UpdatePending;
	static const int g_TimerID;
};