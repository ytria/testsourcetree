#pragma once

#include "BackstagePanelJobCenterBase.h"

#include "..\..\Resource.h"

class CodeJockFrameBase;

class BackstagePanelJobCenterMainFrame : public BackstagePanelJobCenterBase
{
public:
	BackstagePanelJobCenterMainFrame(CodeJockFrameBase* p_Frame);
	~BackstagePanelJobCenterMainFrame() override;

	enum { IDD = IDD_BACKSTAGEPAGE_JOBCENTER_MAINFRAME };

	void DoDataExchange(CDataExchange* pDX) override;

	virtual void			OnSetActive() override;
	virtual void afx_msg	OnGridDoubleClick() override;

protected:
	DECLARE_MESSAGE_MAP()

	virtual BOOL OnInitDialogSpecific() override;

private:
	void runSelected();

	afx_msg void onPresetAdd();
//	afx_msg void onPresetImport();
	
	afx_msg void onSchedule();
	afx_msg void onScheduleRemove();
	afx_msg void onRun();

	afx_msg void onToggleView();
	afx_msg void onSuggest();

	virtual void updateButtons() override;
	virtual void setThemeSpecific(const XTPControlTheme nTheme) override;

	ScriptPreset addPreset(Script& p_Script);
	
	CXTPRibbonBackstageButton	m_BtnPresetAdd;
//	CXTPRibbonBackstageButton	m_BtnPresetImport;
	CXTPRibbonBackstageButton	m_BtnRun;
	CXTPRibbonBackstageButton	m_BtnSchedule;
	CXTPRibbonBackstageButton	m_BtnScheduleRemove;
	CXTPRibbonBackstageButton	m_BtnSuggest;
	CXTPRibbonBackstageButton	m_BtnToggleView;

	//std::unique_ptr<DummyCacheGridHTMLArea> m_Dummy;
};