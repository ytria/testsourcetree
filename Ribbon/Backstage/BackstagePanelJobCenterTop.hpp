#pragma once

#include "DlgLicenseMessageBoxHTML.h"
#include "GridFrameBase.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "Sapio365Session.h"
#include "YtriaTranslate.h"

template <class BackstagePanelJobCenterClass>
const COLORREF BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::g_ButtonBackColor = RGB(240, 240, 240);

BEGIN_TEMPLATE_MESSAGE_MAP(BackstagePanelJobCenterTop, BackstagePanelJobCenterClass, BackstagePanelBase)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS,			OnShowJobs)
	ON_BN_CLICKED(IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS_COMMON,	OnShowJobsCommon)
	ON_COMMAND(IDC_BACKSTAGE_JOBCENTER_TOGGLE_VIEW,				OnToggleView)
END_MESSAGE_MAP()

template <class BackstagePanelJobCenterClass>
void BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::OnShowJobs()
{
	showPanel(IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS);
}

template <class BackstagePanelJobCenterClass>
void BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::OnShowJobsCommon()
{
	showPanel(IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS_COMMON);
}

template <class BackstagePanelJobCenterClass>
void BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::OnToggleView()
{
	m_SimpleView = !m_SimpleView;
	const bool switchView = [this]()
	{
		if (!m_SimpleView && !Office365AdminApp::CheckAndWarn<LicenseTag::JobEditor>(this))
		{
			m_SimpleView = true;
			return false;
		}
		return true;
	}();

	if (switchView)
		updateView();
}

template <class BackstagePanelJobCenterClass>
void BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::updateView()
{
	if (m_SimpleView)
	{
		showPanel(IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS);
		m_TabPanelJobs->SetSimpleView(true);
		m_BtnShowJobsCommon.ShowWindow(SW_HIDE);
	}
	else
	{
		if (m_BtnShowJobsCommon.GetCheck())
			showPanel(IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS_COMMON);
		else
			showPanel(IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS);
		m_TabPanelJobs->SetSimpleView(false);
		m_BtnShowJobsCommon.ShowWindow(SW_SHOW);
	}
}


template <class BackstagePanelJobCenterClass>
HBRUSH BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	// Give 'tab' buttons a gray background.
	if (m_BtnShowJobs.m_hWnd == pWnd->m_hWnd ||
		m_BtnShowJobsCommon.m_hWnd == pWnd->m_hWnd)
	{
		// Copy pasted from CXTPRibbonBackstagePage::OnCtlColor
		pDC->SetBkColor(g_ButtonBackColor);
		return (HBRUSH)m_brushButtonBack;
	}

	return BackstagePanelBase::OnCtlColor(pDC, pWnd, nCtlColor);
}

template <class BackstagePanelJobCenterClass>
void BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::showPanel(const int p_PanelIndex)
{
	for (const auto& p : m_Panels)
	{
		ASSERT(p.second != nullptr);
		if (p.second != nullptr)
		{
			const bool thisPanel = p.first == p_PanelIndex;
			p.second->ShowWindow(thisPanel ? SW_SHOW : SW_HIDE);

			auto panel = dynamic_cast<IActiveSetterJC*>(p.second);
			ASSERT(nullptr != panel);
			if (nullptr != panel)
			{
				if (thisPanel)
				{
					if (m_SetActiveNeeded[p.first])
					{
						panel->OnSetActive();
						m_SetActiveNeeded[p.first] = false;
					}
				}
				else
					panel->OnKillActive();
			}

			auto btn = dynamic_cast<CXTPRibbonBackstageButton*>(GetDlgItem(p.first));
			ASSERT(nullptr != btn);
			if (nullptr != btn)
				btn->SetCheck(thisPanel ? BST_CHECKED : BST_UNCHECKED);
		}
	}

	//RedrawWindow();
}

template <class BackstagePanelJobCenterClass>
BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::BackstagePanelJobCenterTop(CodeJockFrameBase* p_Frame)
	: BackstagePanelBase(IDD)
	, m_Frame(p_Frame)
	, m_SimpleView(false)
{
	m_TabPanelJobs			= make_unique<BackstagePanelJobCenterClass>(p_Frame);
	m_SimpleView = m_TabPanelJobs->IsSimpleView();
	ASSERT(m_SimpleView);

	m_TabPanelJobsCommon	= make_unique<BackstagePanelJobCenterCommon>(p_Frame);
}

template <class BackstagePanelJobCenterClass>
BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::~BackstagePanelJobCenterTop()
{

}

template <class BackstagePanelJobCenterClass>
void BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelBase::DoDataExchange(pDX);

	DDX_Control(pDX, IDD_BACKSTAGEPAGE_JOBCENTER_TAB,			m_PseudoTab);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS,		m_BtnShowJobs);
	DDX_Control(pDX, IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS_COMMON, m_BtnShowJobsCommon);
}

template <class BackstagePanelJobCenterClass>
BOOL BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::OnInitDialog()
{
	BackstagePanelBase::OnInitDialog();
	setCaption(YtriaTranslate::Do(BackstagePanelJobCenter_OnInitDialog_1, _YLOC("Job Center Configuration")).c_str());

	ASSERT(nullptr != m_Frame);
	if (nullptr != dynamic_cast<MainFrame*>(m_Frame))
	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_JOBCENTER_SHOW, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnShowJobs, _T("Global Jobs"), _YTEXT(""), m_ImageList);
	}
	else
	{
		auto f = dynamic_cast<GridFrameBase*>(m_Frame);
		ASSERT(nullptr != f);
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_JOBCENTER_SHOW, ids, 1, CSize(0, 0), xtpImageNormal);
		if (nullptr != f)
			BackstagePanelBase::SetupButton(m_BtnShowJobs, _YFORMAT(L"%s Module Jobs", f->GetCustomTitle().c_str()), _YTEXT(""), m_ImageList);
		else
			BackstagePanelBase::SetupButton(m_BtnShowJobs, _T("Module Jobs"), _YTEXT(""), m_ImageList);// shall never happen
	}

	{
		UINT ids[]{ IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS_COMMON };
		m_ImageList.SetIcons(IDC_BACKSTAGE_BTN_JOBCENTER_SHOWCOMMON, ids, 1, CSize(0, 0), xtpImageNormal);
		BackstagePanelBase::SetupButton(m_BtnShowJobsCommon, _T("Job Components"), _YTEXT(""), m_ImageList);
	}

	auto panel1 = m_TabPanelJobs->Create(BackstagePanelJobCenterClass::IDD, this);
	auto panel2 = m_TabPanelJobsCommon->Create(BackstagePanelJobCenterCommon::IDD, this);
	ASSERT(panel1);
	ASSERT(panel2);

	m_Panels[IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS] = m_TabPanelJobs.get();
	m_Panels[IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS_COMMON] = m_TabPanelJobsCommon.get();

	m_SetActiveNeeded[IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS] = true;
	m_SetActiveNeeded[IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS_COMMON] = true;

	CRect aRect;
	m_PseudoTab.GetWindowRect(&aRect);
	m_PseudoTab.ShowWindow(SW_HIDE);
	ScreenToClient(&aRect);

	SetResize(IDD_BACKSTAGEPAGE_JOBCENTER_TAB, XTP_ATTR_RESIZE(1.f));
	SetResize(m_Panels[IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS], XTP_ATTR_RESIZE(1.f), aRect);
	SetResize(m_Panels[IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS_COMMON], XTP_ATTR_RESIZE(1.f), aRect);

	SetResize(IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS, { 0, 0 }, { .50f, 0 });
	SetResize(IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS_COMMON, { .50f, 0 }, { 1.f, 0 });

	m_brushButtonBack.CreateSolidBrush(g_ButtonBackColor);

	m_BtnShowJobs.EnableWindow(TRUE);
	m_BtnShowJobsCommon.EnableWindow(TRUE);

	return TRUE == panel1 && TRUE == panel2;
}

template <class BackstagePanelJobCenterClass>
void BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelBase::SetTheme(nTheme);
	m_TabPanelJobs->SetTheme(nTheme);
	m_TabPanelJobsCommon->SetTheme(nTheme);

	m_BtnShowJobs.SetTheme(nTheme);
	m_BtnShowJobsCommon.SetTheme(nTheme);
}

template <class BackstagePanelJobCenterClass>
BOOL BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::OnSetActive()
{
	if (BackstagePanelBase::OnSetActive() == TRUE)
	{
		m_SetActiveNeeded[IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS_COMMON] = true;
		m_SetActiveNeeded[IDC_BACKSTAGE_BUTTON_ROLE_SHOWJOBS] = true;
		updateView();
		return TRUE;
	}

	return FALSE;
}

template <class BackstagePanelJobCenterClass>
BOOL BackstagePanelJobCenterTop<BackstagePanelJobCenterClass>::OnKillActive()
{
	if (BackstagePanelBase::OnKillActive() == TRUE)
	{
		m_TabPanelJobs->OnKillActive();
		m_TabPanelJobsCommon->OnKillActive();

		return TRUE;
	}

	return FALSE;
}