#include "BackstagePanelRoleDelegationConfigurationMain.h"

#include "BackstageLogger.h"
#include "FileUtil.h"
#include "MainFrame.h"
#include "Sapio365Session.h"
#include "YCodeJockMessageBox.h"
#include "YtriaTranslate.h"

BEGIN_MESSAGE_MAP(BackstagePanelRoleDelegationConfigurationMain, BackstagePanelWithGrid<RoleDelegationCfgMainGrid>)
END_MESSAGE_MAP()


BackstagePanelRoleDelegationConfigurationMain::BackstagePanelRoleDelegationConfigurationMain()
	: BackstagePanelWithGrid<RoleDelegationCfgMainGrid>(IDD)
{
	m_Grid = std::make_shared<RoleDelegationCfgMainGrid>();
}

BackstagePanelRoleDelegationConfigurationMain::~BackstagePanelRoleDelegationConfigurationMain()
{
}

BOOL BackstagePanelRoleDelegationConfigurationMain::OnInitDialog()
{
	BackstagePanelWithGrid<RoleDelegationCfgMainGrid>::OnInitDialog();
	setCaption(YtriaTranslate::Do(BackstagePanelRoleDelegationConfigurationMain_OnInitDialog_1, _YLOC("Build Role Delegation")).c_str());
	return TRUE;
}

void BackstagePanelRoleDelegationConfigurationMain::DoDataExchange(CDataExchange* pDX)
{
	BackstagePanelWithGrid<RoleDelegationCfgMainGrid>::DoDataExchange(pDX);
}

void BackstagePanelRoleDelegationConfigurationMain::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelWithGrid<RoleDelegationCfgMainGrid>::SetTheme(nTheme);
}

BOOL BackstagePanelRoleDelegationConfigurationMain::OnSetActive()
{
	if (BackstagePanelWithGrid<RoleDelegationCfgMainGrid>::OnSetActive() == TRUE)
	{
		m_Grid->SetIsActive(true);
		return TRUE;
	}

	return FALSE;
}

BOOL BackstagePanelRoleDelegationConfigurationMain::OnKillActive()
{
	if (BackstagePanelWithGrid<RoleDelegationCfgMainGrid>::OnKillActive() == TRUE)
	{
		m_Grid->SetIsActive(false);
		return TRUE;
	}

	return FALSE;
}