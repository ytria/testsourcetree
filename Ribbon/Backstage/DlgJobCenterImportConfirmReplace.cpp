#include "DlgJobCenterImportConfirmReplace.h"

DlgJobCenterImportConfirmReplace::DlgJobCenterImportConfirmReplace(map<wstring, Script>& p_ScriptsToReplace, map<wstring, ScriptPreset>& p_PresetsToReplace, CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
	, m_ScriptsToReplace(p_ScriptsToReplace)
	, m_PresetsToReplace(p_PresetsToReplace)
{
	ASSERT(!m_ScriptsToReplace.empty() || !m_PresetsToReplace.empty());
}

wstring DlgJobCenterImportConfirmReplace::getDialogTitle() const
{
	return m_ScriptsToReplace.empty()
		? _T("Replace existing presets")
			: m_PresetsToReplace.empty()
				? _T("Replace existing jobs")
				: _T("Replace existing jobs and presets");
}

void DlgJobCenterImportConfirmReplace::generateJSONScriptData()
{
	initJson(_YTEXT("DlgImportJobsConfirmReplace"));
	addMappingEditors();
	addButtons();
}

void DlgJobCenterImportConfirmReplace::initJson(const wstring& p_DlgName)
{
	initMain(p_DlgName, false, false);
}

void DlgJobCenterImportConfirmReplace::addMappingEditors()
{
	if (!m_ScriptsToReplace.empty())
	{
		getGenerator().addIconTextField(_YTEXT("text"), _T("The following jobs already exist."), _YTEXT("fas fa-info-circle"), _YTEXT("#e4f0fb"), _YTEXT("#5882bc"));
		for (const auto& p : m_ScriptsToReplace)
			getGenerator().Add(BoolToggleEditor({ { p.first, _YFORMAT(L"%s<br>[%s]", p.second.m_Title.c_str(), p.first.c_str()), EditorFlags::DIRECT_EDIT, true }, { _T("Import new job"), _T("Keep current job") } }));
	}

	if (!m_PresetsToReplace.empty())
	{
		getGenerator().addIconTextField(_YTEXT("text"), _T("The following presets already exist."), _YTEXT("fas fa-info-circle"), _YTEXT("#e4f0fb"), _YTEXT("#5882bc"));
		for (const auto& p : m_PresetsToReplace)
			getGenerator().Add(BoolToggleEditor({ { p.first, _YFORMAT(L"%s<br>[%s]", p.second.m_Title.c_str(), p.first.c_str()), EditorFlags::DIRECT_EDIT, true }, { _T("Import new preset"), _T("Keep current preset") } }));
	}
}

void DlgJobCenterImportConfirmReplace::addButtons()
{
	getGenerator().addApplyButton(_T("OK"));
	getGenerator().addCancelButton(_T("Cancel"));
}

bool DlgJobCenterImportConfirmReplace::processPostedData(const IPostedDataTarget::PostedData& data)
{
	for (const auto& item : data)
		if (!Str::getBoolFromString(item.second))
			m_ScriptsToReplace.erase(item.first);

	return true;// return true if the dialog can be closed
}
