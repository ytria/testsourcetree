#pragma once

#include "IRibbonSystemMenuBuilder.h"

class MainFrameSystemMenuBuilder : public IRibbonSystemMenuBuilder
{
public:
	MainFrameSystemMenuBuilder();

	virtual wstring GetTitle() const;
	virtual bool UseBackstageView() const;

protected:
	virtual CXTPRibbonBackstagePage* createBackstagePanel(UINT commandID, CXTPRibbonBackstagePage* oldPanel);
	virtual wstring getBackstageItemText(UINT commandID);
};

class GridFrameBase;
class GridFrameSystemMenuBuilder : public IRibbonSystemMenuBuilder
{
public:
	GridFrameSystemMenuBuilder(GridFrameBase* frame);

	virtual wstring GetTitle() const;
	virtual bool UseBackstageView() const;

protected:
	virtual CXTPRibbonBackstagePage* createBackstagePanel(UINT commandID, CXTPRibbonBackstagePage* oldPanel);
	virtual wstring getBackstageItemText(UINT commandID);

private:
	GridFrameBase* m_Frame;
};
