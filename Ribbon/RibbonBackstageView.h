#pragma once

class RibbonBackstageView : public CXTPRibbonBackstageView
{
public:

	RibbonBackstageView() = default;
	~RibbonBackstageView() = default;

protected:

	virtual int OnHookMessage(HWND hWnd, UINT nMessage, WPARAM& wParam, LPARAM& lParam, LRESULT& lResult) override;
};
