#include "RibbonSystemMenuBuilders.h"

#include "AutomatedApp.h"
#include "BackstagePanelAbout.h"
#include "BackstagePanelAdmin.h"
#include "BackstagePanelAnnotations.h"
#include "BackstagePanelAutomation.h"
#include "BackstagePanelAutomationShortcuts.h"
#include "BackstagePanelJobCenterTop.h"
#include "BackstagePanelLog.h"
#include "BackstagePanelLogUserActivity.h"
#include "BackstagePanelRoleDelegation.h"
#include "BackstagePanelRoleDelegationConfigurationTop.h"
#include "BackstagePanelSessions.h"
#include "BackstagePanelSettings.h"
#include "BackstagePanelUpdate.h"
#include "BackstagePanelViews.h"
#include "BackstagePanelWindows.h"
#include "InterProductLaunch.h"
#include "GridFrameBase.h"
#include "..\Resource.h"
#include "SharedResource.h"
#include "YtriaTranslate.h"

// If order changes in below list, update asserts in MainFrame::showBackstageMenuUpdateEntry accordingly.
MainFrameSystemMenuBuilder::MainFrameSystemMenuBuilder()
	: IRibbonSystemMenuBuilder({
									ID_PRODUCT_UPDATE
								,	ID_JOBCENTERCFG
								,	ID_AUTOMATION

								,	ID_ITEM_SEPARATOR

								,	ID_BACKSTAGE_SESSIONS
								,	ID_WINDOWS_LIST

								,	ID_ITEM_SEPARATOR

								,	ID_ROLEDELEGATION
								,	ID_LOG_USERACTIVITY
								,	ID_PANEL_ANNOTATIONS
								,	ID_ROLEDELEGATIONCFG
								,	ID_SAPIOADMIN

								,	ID_ITEM_SEPARATOR

								,	ID_LOG
								,	ID_SETTINGS
								,	ID_APP_ABOUT

		})
{

}

wstring MainFrameSystemMenuBuilder::GetTitle() const
{
	return InterProductLaunch::g_sapio365;
}

bool MainFrameSystemMenuBuilder::UseBackstageView() const
{
	return true;
}

CXTPRibbonBackstagePage* MainFrameSystemMenuBuilder::createBackstagePanel(UINT commandID, CXTPRibbonBackstagePage* oldPanel)
{
	// Keep the old school pop-up for now.
	/*if (ID_AUTOMATION_SHORTCUTS == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelAutomationShortcuts*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		CXTPRibbonBackstagePage* newPanel = nullptr;

		AutomatedApp* app = dynamic_cast<AutomatedApp*>(AfxGetApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
		{
			newPanel = new BackstagePanelAutomationShortcuts(app);
			newPanel->Create(BackstagePanelAutomationShortcuts::IDD, AfxGetMainWnd());
		}

		return newPanel;
	}*/
    if (ID_BACKSTAGE_SESSIONS == commandID)
    {
        if (nullptr != dynamic_cast<BackstagePanelSessions*>(oldPanel))
            return oldPanel;

        delete oldPanel;

        CXTPRibbonBackstagePage* newPanel = new BackstagePanelSessions();
        newPanel->Create(BackstagePanelSessions::IDD, AfxGetApp()->m_pMainWnd);

        return newPanel;
    }

	if (ID_WINDOWS_LIST == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelWindows*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		CXTPRibbonBackstagePage* newPanel = new BackstagePanelWindows(BackstagePanelWindows::IDD_FOR_MAIN);
		newPanel->Create(BackstagePanelWindows::IDD_FOR_MAIN, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}

	if (ID_LOG == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelLog*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		CFrameWnd* frame = dynamic_cast<CFrameWnd*>(AfxGetApp()->m_pMainWnd);
		ASSERT(nullptr != frame);
		CXTPRibbonBackstagePage* newPanel = new BackstagePanelLog(*frame);
		newPanel->Create(BackstagePanelLog::IDD, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}

	if (ID_LOG_USERACTIVITY == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelLogUserActivity*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		CXTPRibbonBackstagePage* newPanel = new BackstagePanelLogUserActivity();
		newPanel->Create(BackstagePanelLogUserActivity::IDD, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}

	if (ID_PANEL_ANNOTATIONS == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelAnnotations*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		CXTPRibbonBackstagePage* newPanel = new BackstagePanelAnnotations();
		newPanel->Create(BackstagePanelAnnotations::IDD, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}

	if (/*ID_RECENT_AUTOMATIONS*/ID_AUTOMATION == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelAutomation*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		CodeJockFrameBase* frame = dynamic_cast<CodeJockFrameBase*>(AfxGetApp()->m_pMainWnd);
		ASSERT(nullptr != frame);
		BackstagePanelAutomation* newPanel = new BackstagePanelAutomation(frame);// each frame has its own automation history
		newPanel->Create(BackstagePanelAutomation::IDD, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}

	if (ID_APP_ABOUT == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelAbout*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		BackstagePanelAbout* newPanel = new BackstagePanelAbout();
		newPanel->Create(BackstagePanelAbout::IDD, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}

	if (ID_SETTINGS == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelSettings*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		BackstagePanelSettings* newPanel = new BackstagePanelSettings();
		newPanel->Create(BackstagePanelSettings::IDD, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}

	if (ID_PRODUCT_UPDATE == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelUpdate*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		BackstagePanelUpdate* newPanel = new BackstagePanelUpdate();
		newPanel->Create(BackstagePanelUpdate::IDD, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}
	
	if (ID_ROLEDELEGATIONCFG == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelRoleDelegationConfigurationTop*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		BackstagePanelRoleDelegationConfigurationTop* newPanel = new BackstagePanelRoleDelegationConfigurationTop();
		newPanel->Create(BackstagePanelRoleDelegationConfigurationTop::IDD, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}

	if (ID_SAPIOADMIN == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelAdmin*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		BackstagePanelAdmin* newPanel = new BackstagePanelAdmin();
		newPanel->Create(BackstagePanelAdmin::IDD, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}

	if (ID_ROLEDELEGATION == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelRoleDelegation*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		BackstagePanelRoleDelegation* newPanel = new BackstagePanelRoleDelegation();
		newPanel->Create(BackstagePanelRoleDelegation::IDD, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}

	if (ID_JOBCENTERCFG == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelJobCenterTop<BackstagePanelJobCenterMainFrame>*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		BackstagePanelJobCenterTop<BackstagePanelJobCenterMainFrame>* newPanel = new BackstagePanelJobCenterTop<BackstagePanelJobCenterMainFrame>(dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd));
		newPanel->Create(BackstagePanelJobCenterTop<BackstagePanelJobCenterMainFrame>::IDD, AfxGetApp()->m_pMainWnd);

		return newPanel;
	}

	return nullptr;
}

wstring MainFrameSystemMenuBuilder::getBackstageItemText(UINT commandID)
{
	switch (commandID)
	{
	case ID_FILE_LOADAUTOMATIONXML:
		return YtriaTranslate::Do(MainFrame_localizeCmdItems_739, _YLOC("Load &Automation file"));

	case ID_AUTOMATION_SHORTCUTS:
		return YtriaTranslate::Do(DlgMain_localizeCmdItems_280, _YLOC("Automation Files: Quick Launch Manager..."));

	/*case ID_RECENT_AUTOMATIONS:
		return YtriaTranslate::Do(DlgMain_localizeSubMenus_48, _YLOC("Recent Automation files"));*/

	case ID_AUTOMATION:
		return YtriaTranslate::Do(MainFrameSystemMenuBuilder_getBackstageItemText_1, _YLOC("Automation")).c_str();

	case ID_AUTOMATION_RECORDER:
		return YtriaTranslate::Do(MainFrame_localizeCmdItems_895, _YLOC("Automation Recorder..."));

	case ID_AUTOMATION_VIEWCONSOLE:
		return YtriaTranslate::Do(DlgMain_localizeCmdItems_289, _YLOC("Show Automation Console"));

	case ID_APP_EXIT:
		return YtriaTranslate::Do(DlgMain_localizeCmdItems_4, _YLOC("E&xit"));

	case ID_WINDOWS_LIST:
		return YtriaTranslate::Do(MainFrameSystemMenuBuilder_getBackstageItemText_2, _YLOC("Open Windows")).c_str();

	case ID_LOG:
		return YtriaTranslate::Do(MainFrameSystemMenuBuilder_getBackstageItemText_3, _YLOC("Process Log")).c_str();

	case ID_LOG_USERACTIVITY:
		return YtriaTranslate::Do(MainFrameSystemMenuBuilder_getBackstageItemText_8, _YLOC("User Activity Logs")).c_str();

	case ID_PANEL_ANNOTATIONS:
		return _T("Comment History");

   case ID_BACKSTAGE_SESSIONS:
        return YtriaTranslate::Do(MainFrameSystemMenuBuilder_getBackstageItemText_4, _YLOC("Recent Sessions")).c_str();

	case ID_ITEM_SEPARATOR:
		break;

	case ID_APP_ABOUT:
		return YtriaTranslate::Do(MainFrameSystemMenuBuilder_getBackstageItemText_5, _YLOC("About sapio365")).c_str();

	case ID_SETTINGS:
		return YtriaTranslate::Do(MainFrameSystemMenuBuilder_getBackstageItemText_6, _YLOC("Preferences")).c_str();

	case ID_PRODUCT_UPDATE:
		return YtriaTranslate::Do(MainFrameSystemMenuBuilder_getBackstageItemText_7, _YLOC("Update")).c_str();

	case ID_ROLEDELEGATION:
		return YtriaTranslate::Do(MainFrameSystemMenuBuilder_getBackstageItemText_10, _YLOC("RBAC - Available Roles")).c_str();

	case ID_ROLEDELEGATIONCFG:
		return YtriaTranslate::Do(MainFrameSystemMenuBuilder_getBackstageItemText_11, _YLOC("RBAC - Configuration")).c_str();

	case ID_JOBCENTERCFG:
		return YtriaTranslate::Do(GridFrameSystemMenuBuilder_getBackstageItemText_5, _YLOC("Job Center Configuration"));

	case ID_SAPIOADMIN:
		return _T("sapio365 Access && Restrictions");

	default:
		ASSERT(false);
		break;
	}

	return _YTEXT("");
}

// =================================================================================================

GridFrameSystemMenuBuilder::GridFrameSystemMenuBuilder(GridFrameBase* frame)
	: IRibbonSystemMenuBuilder(	{	ID_JOBCENTERCFG
								,	ID_AUTOMATION

								,	ID_ITEM_SEPARATOR
								
								,	ID_BACKSTAGE_GRIDVIEWS
	
								,	ID_ITEM_SEPARATOR

								,	ID_WINDOWS_LIST
								,	ID_LOG

								,	ID_ITEM_SEPARATOR

								,	ID_APP_EXIT })
	, m_Frame(frame)
{
	ASSERT(nullptr != frame);
}

wstring GridFrameSystemMenuBuilder::GetTitle() const
{
	return InterProductLaunch::g_sapio365;
}

bool GridFrameSystemMenuBuilder::UseBackstageView() const
{
	return true;
}

CXTPRibbonBackstagePage* GridFrameSystemMenuBuilder::createBackstagePanel(UINT commandID, CXTPRibbonBackstagePage* oldPanel)
{
	if (ID_WINDOWS_LIST == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelWindows*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		CXTPRibbonBackstagePage* newPanel = new BackstagePanelWindows(BackstagePanelWindows::IDD);
		newPanel->Create(BackstagePanelWindows::IDD, m_Frame);

		return newPanel;
	}

	if (ID_LOG == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelLog*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		ASSERT(nullptr != m_Frame);
		CXTPRibbonBackstagePage* newPanel = new BackstagePanelLog(*m_Frame);
		newPanel->Create(BackstagePanelLog::IDD, m_Frame);

		return newPanel;
	}

	if (ID_BACKSTAGE_GRIDVIEWS == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelViews*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		ASSERT(nullptr != m_Frame);
		CXTPRibbonBackstagePage* newPanel = new BackstagePanelViews(*m_Frame);
		newPanel->Create(BackstagePanelViews::IDD, m_Frame);

		return newPanel;
	}

	if (ID_AUTOMATION == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelAutomation*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		ASSERT(nullptr != m_Frame);
		BackstagePanelAutomation* newPanel = new BackstagePanelAutomation(m_Frame);
		newPanel->Create(BackstagePanelAutomation::IDD, m_Frame);

		return newPanel;
	}

	if (ID_JOBCENTERCFG == commandID)
	{
		if (nullptr != dynamic_cast<BackstagePanelJobCenterTop<BackstagePanelJobCenter>*>(oldPanel))
			return oldPanel;

		delete oldPanel;

		ASSERT(nullptr != m_Frame);
		BackstagePanelJobCenterTop<BackstagePanelJobCenter>* newPanel = new BackstagePanelJobCenterTop<BackstagePanelJobCenter>(m_Frame);
		newPanel->Create(BackstagePanelJobCenterTop<BackstagePanelJobCenter>::IDD, m_Frame);

		return newPanel;
	}

	return nullptr;
}

wstring GridFrameSystemMenuBuilder::getBackstageItemText(UINT commandID)
{
	switch (commandID)
	{
	case ID_WINDOWS_LIST:
		return YtriaTranslate::Do(GridFrameSystemMenuBuilder_getBackstageItemText_1, _YLOC("Open Windows"));

	case ID_LOG:
		return YtriaTranslate::Do(GridFrameSystemMenuBuilder_getBackstageItemText_2, _YLOC("Process Log"));

	case ID_BACKSTAGE_GRIDVIEWS:
		return _T("Views");

	case ID_LOG_USERACTIVITY:
		return YtriaTranslate::Do(MainFrameSystemMenuBuilder_getBackstageItemText_8, _YLOC("User Activity Logs"));

	case ID_PANEL_ANNOTATIONS:
		return _T("Comment History");

	case ID_AUTOMATION:
		return YtriaTranslate::Do(GridFrameSystemMenuBuilder_getBackstageItemText_3, _YLOC("Automation"));

	case ID_ITEM_SEPARATOR:
		break;

	case ID_CLOSE_GRID_FRAME:
		return YtriaTranslate::Do(GridFrameSystemMenuBuilder_getBackstageItemText_4, _YLOC("Close Frame"));

	case ID_APP_EXIT:
		return _T("E&xit sapio365");

	case ID_JOBCENTERCFG:
		return YtriaTranslate::Do(GridFrameSystemMenuBuilder_getBackstageItemText_5, _YLOC("Job Center Configuration"));

	default:
		ASSERT(false);
		break;
	}

	return _YTEXT("");
}
