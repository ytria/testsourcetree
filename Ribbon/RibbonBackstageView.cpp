#include "RibbonBackstageView.h"

int RibbonBackstageView::OnHookMessage(HWND hWnd, UINT nMessage, WPARAM& wParam, LPARAM& lParam, LRESULT& lResult)
{
	int Result = FALSE;

	// WM_ACTIVATEAPP - Sent when a window belonging to a different application than the active window is about to be activated.
	// Seems to only be called when switching from frame to frame. Makes sense from description I guess.
	// In the base class, around line 362, SetFocus is called when WM_ACTIVATEAPP is received.
	// This causes Bug #170928.RL.006552. It's possible that this SetFocus is there to give the user
	// immediate keyboard access to the backstage view when it becomes active.
	if (nMessage != WM_ACTIVATEAPP)
		Result = CXTPRibbonBackstageView::OnHookMessage(hWnd, nMessage, wParam, lParam, lResult);

	// 	else
// 	// 		TRACE( _YCOM("RibbonBackstageView, WM_ACTIVATEAPP, (hWnd = 0x%x - wParam = 0x%x - lParam = 0x%x) - WM_ACTIVATEAPP\n"), 
	// 			hWnd,
	// 			wParam, 
	// 			lParam);

	return Result;
}