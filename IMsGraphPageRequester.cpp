#include "IMsGraphPageRequester.h"

IMsGraphPageRequester::IMsGraphPageRequester()
	: m_IsCutOff(false)
	, m_DeserializedCount(0)
	, m_UseBeta(false)
{
}

bool IMsGraphPageRequester::GetIsCutOff() const
{
	return m_IsCutOff;
}

size_t IMsGraphPageRequester::GetDeserializedCount() const
{
	return m_DeserializedCount;
}

void IMsGraphPageRequester::SetHeaders(const web::http::http_headers& p_Headers)
{
	m_Headers = p_Headers;
}

const web::http::http_headers& IMsGraphPageRequester::GetHeaders() const
{
	return m_Headers;
}

void IMsGraphPageRequester::SetUseBetaEndpoint(bool p_UseBetaEndpoint)
{
	m_UseBeta = p_UseBetaEndpoint;
}

void IMsGraphPageRequester::SetHttpRequestLogger(const std::shared_ptr<IHttpRequestLogger>& p_HttpLogger)
{
	m_HttpLogger = p_HttpLogger;
}

const std::shared_ptr<IHttpRequestLogger>& IMsGraphPageRequester::GetHttpRequestLogger() const
{
	return m_HttpLogger;
}

std::shared_ptr<IHttpRequestLogger>& IMsGraphPageRequester::GetHttpRequestLogger()
{
	return m_HttpLogger;
}

void IMsGraphPageRequester::Copy(IMsGraphPageRequester& p_Requester)
{
	m_Headers = p_Requester.m_Headers;
	m_IsCutOff = p_Requester.m_IsCutOff;
	m_DeserializedCount = p_Requester.m_DeserializedCount;
	m_HttpLogger = p_Requester.m_HttpLogger;
	m_UseBeta = p_Requester.m_UseBeta;
}
