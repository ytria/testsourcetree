#include "ModuleConversation.h"

#include "AutomationDataStructure.h"
#include "BusinessConversation.h"
#include "BusinessObjectManager.h"
#include "FrameConversations.h"
#include "GraphCache.h"
#include "O365AdminUtil.h"
#include "safeTaskCall.h"
#include "TaskDataManager.h"
#include "YDataCallbackMessage.h"
#include "MultiObjectsRequestLogger.h"

void ModuleConversation::executeImpl(const Command & p_Command)
{
    switch (p_Command.GetTask())
    {
    case Command::ModuleTask::List:
		showConversations(p_Command);
		break;
    case Command::ModuleTask::UpdateRefresh:
        refresh(p_Command);
        break;
        
    default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
		break;
    }
}

void ModuleConversation::showConversations(const Command& p_Command)
{
    const auto& info = p_Command.GetCommandInfo();

    const auto&	p_IDs			= info.GetIds();
    auto		p_ExistingFrame = dynamic_cast<FrameConversations*>(info.GetFrame());
    auto		p_SourceWindow	= p_Command.GetSourceWindow();
    auto		p_Action		= p_Command.GetAutomationAction();
    auto		sourceCWnd		= p_SourceWindow.GetCWnd();

    if (!FrameConversations::CanCreateNewFrame(true, sourceCWnd, p_Action))
        return;

    FrameConversations* frame = p_ExistingFrame;
    if (nullptr == frame)
    {
        ModuleCriteria moduleCriteria;
		moduleCriteria.m_Origin = Origin::Group;
		moduleCriteria.m_UsedContainer = ModuleCriteria::UsedContainer::IDS;
		moduleCriteria.m_IDs = p_IDs;
		moduleCriteria.m_Privilege = info.GetRBACPrivilege();
		moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

        if (ShouldCreateFrame<FrameConversations>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
        {
            CWaitCursor _;

            frame = new FrameConversations(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleConversation_showConversations_1, _YLOC("Group Conversations")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
            frame->InitModuleCriteria(moduleCriteria);
            AddGridFrame(frame);
            frame->Erect();
            auto taskData = addFrameTask(YtriaTranslate::Do(ModuleConversation_showConversations_2, _YLOC("Show conversations")).c_str(), frame, p_Command, true);
            doRefresh(frame, moduleCriteria, p_Action, taskData, false, RefreshSpecificData());
        }
    }
}

void ModuleConversation::refresh(const Command& p_Command)
{
    auto p_Action = p_Command.GetAutomationAction();

    FrameConversations* existingFrame = dynamic_cast<FrameConversations*>(p_Command.GetCommandInfo().GetFrame());
    ASSERT(nullptr != existingFrame);
    if (nullptr != existingFrame)
    {
        auto taskData = addFrameTask(YtriaTranslate::Do(ModuleConversation_refresh_1, _YLOC("Show Conversations")).c_str(), existingFrame, p_Command, false);

        RefreshSpecificData refreshSelectedData;
        if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
            refreshSelectedData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

        doRefresh(existingFrame, existingFrame->GetModuleCriteria(), p_Action, taskData, true, refreshSelectedData);
    }
    else
        SetAutomationGreenLight(p_Action, _YTEXT(""));
}

void ModuleConversation::doRefresh(FrameConversations* p_pFrame, const ModuleCriteria& p_ModuleCriteria, AutomationAction * p_Action, YtriaTaskData p_TaskData, bool isUpdate, const RefreshSpecificData& p_RefreshData)
{
	YSafeCreateTask([p_TaskData, hwnd = p_pFrame->GetSafeHwnd(), p_Action, p_RefreshData, currentModuleCriteriaIds = p_pFrame->GetModuleCriteria().m_IDs, bom = p_pFrame->GetBusinessObjectManager(), isUpdate, wantsAdditionalMetadata = (bool)p_pFrame->GetMetaDataColumnInfo()]()
    {
		O365DataMap<BusinessGroup, vector<BusinessConversation>> conversations;

        const bool partialRefresh = !p_RefreshData.m_Criteria.m_IDs.empty();
        const auto& groupIds = partialRefresh ? p_RefreshData.m_Criteria.m_IDs : currentModuleCriteriaIds;

		vector<BusinessGroup> groups;
		if (wantsAdditionalMetadata)
		{
			// Do not pass p_TaskData if we don't want to allow cancellation of this step.
			groups = bom->GetGraphCache().GetSqlCachedGroups(p_TaskData.GetOriginator(), groupIds, false, p_TaskData);
		}

		auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("conversations"), groupIds.size());

		auto processGroup = [&bom, &p_TaskData, &conversations, &logger](auto& group)
		{
			logger->IncrementObjCount();
			logger->SetContextualInfo(GetDisplayNameOrId(group));

			vector<BusinessConversation> groupConversations;
			if (!p_TaskData.IsCanceled())
				groupConversations = bom->GetBusinessConversations(group.GetID(), logger, p_TaskData).GetTask().get();

			if (p_TaskData.IsCanceled())
			{
				group.SetFlags(BusinessObject::CANCELED);
				conversations[group] = {};
			}
			else
			{
				conversations[group] = groupConversations;
			}
		};

		auto ids = groupIds;
		for (auto& group : groups)
		{
			if (1 == ids.erase(group.GetID()))
				processGroup(group);
		}

		ASSERT(!wantsAdditionalMetadata || ids.empty()); // Is it normal not all groups are in SQL cache?

        for (const auto& id : ids)
        {
			BusinessGroup group;
			group.SetID(id);
			logger->SetContextualInfo(bom->GetGraphCache().GetGroupContextualInfo(id));
			bom->GetGraphCache().SyncUncachedGroup(group, std::make_shared<MultiObjectsRequestLogger>(*logger), p_TaskData);

			processGroup(group);
        }

		YDataCallbackMessage<O365DataMap<BusinessGroup, vector<BusinessConversation>>>::DoPost(conversations, [hwnd, partialRefresh, p_TaskData, p_Action, isUpdate, isCanceled = p_TaskData.IsCanceled()](const O365DataMap<BusinessGroup, vector<BusinessConversation>>& p_Conversations)
        {
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameConversations*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, p_TaskData, false))
            {
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting conversations for %s groups...", Str::getStringFromNumber(p_Conversations.size()).c_str()));
                frame->GetGrid().ClearLog(p_TaskData.GetId());

				{
					CWaitCursor _;
					frame->ShowConversations(p_Conversations, !partialRefresh);
				}
                
				if (!isUpdate)
				{
					frame->UpdateContext(p_TaskData, hwnd);
					shouldFinishTask = false;
				}
            }

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, p_TaskData, p_Action);
			}
        });
    });
}
