#include "BusinessUserGuestDeleted.h"

#include "BusinessGroup.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessUserGuestDeleted>("Deleted Guest") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Deleted Guest"))))
		.constructor()(policy::ctor::as_object)
	;
}
