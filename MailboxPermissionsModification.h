#pragma once

#include "RowModification.h"
#include <type_traits>

class GridMailboxPermissions;

class MailboxPermissionsModification : public RowModification
{
public:
	MailboxPermissionsModification(GridMailboxPermissions& p_Grid, const row_pk_t& p_Row, const wstring& p_RightsToAdd, const wstring& p_RightsToRemove);

	void RefreshState() override;
	vector<ModificationLog> GetModificationLogs() const override;

	void Merge(const MailboxPermissionsModification& p_Mod);
	void Revert();

private:
	wstring GetCombinedAccessRightsString(const wstring& p_Current) const;
	void AddToList(wstring& p_List, const wstring& p_ToAdd);
	void RemoveFromList(wstring& p_List, const wstring& p_ToRemove);

	std::reference_wrapper<GridMailboxPermissions> m_Grid; // Enable copy
	wstring m_RightsToAdd;
	wstring m_RightsToRemove;
	wstring m_OldAccessRights;

	static const wchar_t* g_Sep;
};
