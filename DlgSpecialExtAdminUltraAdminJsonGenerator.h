#pragma once
#include "IDlgSpecialExtAdminJsonGenerator.h"

class DlgSpecialExtAdminUltraAdminJsonGenerator : public IDlgSpecialExtAdminJsonGenerator
{
public:
	DlgSpecialExtAdminUltraAdminJsonGenerator(bool p_EditOnlySecretKey);
	web::json::value Generate(const DlgSpecialExtAdminAccess& p_Dlg) override;

private:
	bool m_EditOnlySecretKey;
};

