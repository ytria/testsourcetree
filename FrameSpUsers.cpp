#include "FrameSpUsers.h"
#include "GridSpUsers.h"
#include "Command.h"

IMPLEMENT_DYNAMIC(FrameSpUsers, GridFrameBase)

void FrameSpUsers::BuildView(const O365DataMap<BusinessSite, vector<Sp::User>>& p_SpSites, bool p_FullPurge)
{
    m_Grid.BuildView(p_SpSites, p_FullPurge);
}

O365Grid& FrameSpUsers::GetGrid()
{
    return m_Grid;
}

void FrameSpUsers::ApplySelectedSpecific()
{
    ApplySpecific(true);
}

void FrameSpUsers::ApplyAllSpecific()
{
    ApplySpecific(false);
}

void FrameSpUsers::ApplySpecific(bool p_Selected)
{
    ASSERT(false); //TODO
}

void FrameSpUsers::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::SpUser, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameSpUsers::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::SpUser, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

bool FrameSpUsers::HasApplyButton()
{
    return true;
}

void FrameSpUsers::createGrid()
{
    m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameSpUsers::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigUsers();
}

bool FrameSpUsers::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
    m_CreateRefreshPartial = true;
    CreateViewGroup(tab);
    CreateRefreshGroup(tab);

    return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameSpUsers::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
    AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

    return statusRV;
}
