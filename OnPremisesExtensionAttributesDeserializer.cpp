#include "OnPremisesExtensionAttributesDeserializer.h"

#include "JsonSerializeUtil.h"

void OnPremisesExtensionAttributesDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute1"), m_Data.m_ExtensionAttribute1, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute2"), m_Data.m_ExtensionAttribute2, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute3"), m_Data.m_ExtensionAttribute3, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute4"), m_Data.m_ExtensionAttribute4, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute5"), m_Data.m_ExtensionAttribute5, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute6"), m_Data.m_ExtensionAttribute6, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute7"), m_Data.m_ExtensionAttribute7, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute8"), m_Data.m_ExtensionAttribute8, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute9"), m_Data.m_ExtensionAttribute9, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute10"), m_Data.m_ExtensionAttribute10, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute11"), m_Data.m_ExtensionAttribute11, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute12"), m_Data.m_ExtensionAttribute12, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute13"), m_Data.m_ExtensionAttribute13, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute14"), m_Data.m_ExtensionAttribute14, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("extensionAttribute15"), m_Data.m_ExtensionAttribute15, p_Object);
}
