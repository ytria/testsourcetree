#pragma once

#include "IHttpRequestLogger.h"

class IMsGraphPageRequester
{
public:
	IMsGraphPageRequester();
	virtual ~IMsGraphPageRequester() = default;

	// Return value: received json of the current page
	virtual web::json::value Request(const web::uri& p_Uri, const shared_ptr<MSGraphSession>& p_Session, YtriaTaskData p_TaskData) = 0;

	bool GetIsCutOff() const;
	size_t GetDeserializedCount() const;
	void SetHeaders(const web::http::http_headers& p_Headers);
	const web::http::http_headers& GetHeaders() const;
	virtual void SetUseBetaEndpoint(bool p_UseBetaEndpoint);

	void SetHttpRequestLogger(const std::shared_ptr<IHttpRequestLogger>& p_HttpLogger);
	const std::shared_ptr<IHttpRequestLogger>& GetHttpRequestLogger() const;
	std::shared_ptr<IHttpRequestLogger>& GetHttpRequestLogger();

protected:
	void Copy(IMsGraphPageRequester& p_Requester);

	web::http::http_headers m_Headers;
	bool m_IsCutOff;
	size_t m_DeserializedCount;
	std::shared_ptr<IHttpRequestLogger> m_HttpLogger;
	bool m_UseBeta;
};

