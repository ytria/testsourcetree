#pragma once

#include "BaseO365Grid.h"

#include "BusinessColumnDefinition.h"
#include "BusinessList.h"
#include "Site.h"

class FrameColumns;

class GridColumns : public ModuleO365Grid<BusinessColumnDefinition>
{
public:
    GridColumns();
    virtual void customizeGrid() override;

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildView(const O365DataMap<BusinessSite, vector<BusinessList>>& p_Lists, bool p_FullPurge);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
    virtual BusinessColumnDefinition getBusinessObject(GridBackendRow* p_Row) const override;

private:
    GridBackendRow* FillRow(const BusinessSite& p_Site, const BusinessList& p_List, const BusinessColumnDefinition& p_Column, const wstring& p_HierarchyDisplayName);
    DECLARE_MESSAGE_MAP()

private:
    GridBackendColumn* m_ColMetaSiteId;
    GridBackendColumn* m_ColMetaSiteDisplayName;
    GridBackendColumn* m_ColMetaId;
    GridBackendColumn* m_ColMetaDisplayName;
    GridBackendColumn* m_ColMetaName;
    GridBackendColumn* m_ColName;
    GridBackendColumn* m_ColDisplayName;
    GridBackendColumn* m_ColColumnGroup;
    GridBackendColumn* m_ColDescription;
    GridBackendColumn* m_ColEnforceUniqueValues;
    GridBackendColumn* m_ColHidden;
    GridBackendColumn* m_ColIndexed;
    GridBackendColumn* m_ColReadOnly;
    GridBackendColumn* m_ColRequired;
    GridBackendColumn* m_ColCalculatedFormat;
    GridBackendColumn* m_ColCalculatedFormula;
    GridBackendColumn* m_ColCalculatedOutputType;
    GridBackendColumn* m_ColChoiceAllowTextEntry;
    GridBackendColumn* m_ColChoiceChoices;
    GridBackendColumn* m_ColChoiceDisplayAs;
    GridBackendColumn* m_ColCurrencyLocale;
    GridBackendColumn* m_ColDateTimeDisplayAs;
    GridBackendColumn* m_ColDateTimeFormat;
    GridBackendColumn* m_ColDefaultValueFormula;
    GridBackendColumn* m_ColDefaultValueValue;
    GridBackendColumn* m_ColLookupAllowMultipleValues;
    GridBackendColumn* m_ColLookupAllowUnlimitedLength;
    GridBackendColumn* m_ColLookupColumnName;
    GridBackendColumn* m_ColLookupListId;
    GridBackendColumn* m_ColLookupPrimaryLookupColumnId;
    GridBackendColumn* m_ColNumberDecimalPlaces;
    GridBackendColumn* m_ColNumberDisplayAs;
    GridBackendColumn* m_ColNumberMaximum;
    GridBackendColumn* m_ColNumberMinimum;
    GridBackendColumn* m_ColPersonOrGroupAllowMultipleSelection;
    GridBackendColumn* m_ColPersonOrGroupDisplayAs;
    GridBackendColumn* m_ColPersonOrGroupChooseFromType;
    GridBackendColumn* m_ColTextAllowMultipleLines;
    GridBackendColumn* m_ColTextAppendChangesToExistingText;
    GridBackendColumn* m_ColTextLinesForEditing;
    GridBackendColumn* m_ColTextMaxLength;
    GridBackendColumn* m_ColTextTextType;

    FrameColumns*			m_FrameColumns;
};