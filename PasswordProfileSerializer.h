#pragma once

#include "IJsonSerializer.h"
#include "PasswordProfile.h"

class PasswordProfileSerializer : public IJsonSerializer
{
public:
    PasswordProfileSerializer(const PasswordProfile& p_PasswordProfile);
    void Serialize() override;

private:
    const PasswordProfile& m_PasswordProfile;
};

