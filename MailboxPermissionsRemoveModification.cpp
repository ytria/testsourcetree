#include "MailboxPermissionsRemoveModification.h"
#include "GridMailboxPermissions.h"

MailboxPermissionsRemoveModification::MailboxPermissionsRemoveModification(GridMailboxPermissions& p_Grid, const row_pk_t& p_RowPk)
	:ModificationWithRequestFeedback(_YTEXT("")),
	m_Grid(p_Grid)
{
	m_RowKey = p_RowPk;
}

vector<Modification::ModificationLog> MailboxPermissionsRemoveModification::GetModificationLogs() const
{
	vector<Modification::ModificationLog> modLogs;
	modLogs.emplace_back(GridBackendUtil::ToString(GetRowKey()), GetObjectName(), _YTEXT("Remove mailbox permissions"), _YTEXT("Access Rights"), m_Permissions, GetState());
	return modLogs;
}

void MailboxPermissionsRemoveModification::Apply()
{
	auto row = m_Grid.get().GetRowIndex().GetExistingRow(m_RowKey);
	if (nullptr != row)
	{
		auto permissions = row->GetField(m_Grid.get().GetColAccessRights()).GetValuesMulti<PooledString>();
		if (nullptr != permissions)
			m_Permissions = Str::ToStrVector(*permissions);
		else
			m_Permissions = vector<wstring>{ row->GetField(m_Grid.get().GetColAccessRights()).GetValueStr() };
		m_UserId = row->GetField(m_Grid.get().GetColPermissionsUser()).GetValueStr();
		m_MailboxOwner = row->GetField(m_Grid.get().GetColOwnerId()).GetValueStr();
		m_Grid.get().OnRowDeleted(row, false);

		SetObjectName(row->GetField(m_Grid.get().GetColOwnerDisplayName()).GetValueStr() + wstring(_YTEXT(" -> ")) + m_UserId);
	}
	else
	{
		Revert();
	}
}

void MailboxPermissionsRemoveModification::Revert()
{
	auto row = m_Grid.get().GetRowIndex().GetExistingRow(GetRowKey());
	if (nullptr != row)
		m_Grid.get().OnRowNothingToSave(row, false);
}

const vector<wstring>& MailboxPermissionsRemoveModification::GetPermissions() const
{
	return m_Permissions;
}

const wstring& MailboxPermissionsRemoveModification::GetMailboxOwner() const
{
	return m_MailboxOwner;
}

const wstring& MailboxPermissionsRemoveModification::GetUserId() const
{
	return m_UserId;
}

