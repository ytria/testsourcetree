#include "ParentalControlSettingsDeserializer.h"
#include "ListDeserializer.h"

void ParentalControlSettingsDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		ListStringDeserializer listStrDeserializer;
		if (JsonSerializeUtil::DeserializeAny(listStrDeserializer, _YTEXT("countriesBlockedForMinors"), p_Object))
			m_Data.m_CountriesBlockedForMinors = std::move(listStrDeserializer.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("legalAgeGroupRule"), m_Data.m_LegalAgeGroupRule, p_Object);
}
