#include "SiteRequester.h"
#include "MsGraphHttpRequestLogger.h"

SiteRequester::SiteRequester(const wstring& p_SiteId, const std::shared_ptr<IRequestLogger>& p_Logger)
	:m_SiteId(p_SiteId),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> SiteRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<SiteDeserializer>(p_Session);

	web::uri_builder uri(_YTEXT("sites"));
	uri.append_path(m_SiteId);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, uri.to_uri(), false, m_Logger, httpLogger, p_TaskData);
}

const BusinessSite& SiteRequester::GetData() const
{
	return m_Deserializer->GetData();
}

BusinessSite& SiteRequester::GetData()
{
	return m_Deserializer->GetData();
}
