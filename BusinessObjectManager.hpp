#include "BusinessObjectManager.h"

#include "LoggerService.h"
#include "Office365Admin.h"

using namespace Util;

template<class T>
vector<O365UpdateOperation> BusinessObjectManager::WriteSingleRequest(const vector<T>& p_BusinessObjects, const vector<row_primary_key_t>& p_Keys, YtriaTaskData p_TaskData)
{
    vector<O365UpdateOperation> operations;

    ASSERT(p_BusinessObjects.size() == p_Keys.size());
    if (p_BusinessObjects.size() == p_Keys.size())
    {
        size_t pkIndex = 0;
        for (const T& businessObj : p_BusinessObjects)
        {
			if (p_TaskData.IsCanceled())
				break;

			if (O365Grid::IsTemporaryCreatedObjectID(businessObj.GetID()))
				LoggerService::User(_YFORMAT(L"Creating new %s", businessObj.GetObjectName().c_str()), p_TaskData.GetOriginator());
			else
				LoggerService::User(YtriaTranslate::Do(BusinessObjectManager_WriteSingleRequest_1, _YLOC("Updating \"%1\" with id \"%2\""), businessObj.GetObjectName().c_str(), businessObj.GetID().c_str()), p_TaskData.GetOriginator());
			operations.emplace_back(p_Keys[pkIndex++]);
			operations.back().StoreResults(businessObj.SendWriteSingleRequest(GetSapio365Session(), p_TaskData).GetTask().get());
        }
    }

    return operations;
}

template<class T>
vector<O365UpdateOperation> BusinessObjectManager::WriteMultiRequests(const vector<T>& p_BusinessObjects, const vector<row_primary_key_t>& p_Keys, YtriaTaskData p_TaskData)
{
	vector<O365UpdateOperation> operations;

	size_t pkIndex = 0;
	for (const T& businessObj : p_BusinessObjects)
	{
		if (p_TaskData.IsCanceled())
			break;

		if (O365Grid::IsTemporaryCreatedObjectID(businessObj.GetID()))
			LoggerService::User(_YFORMAT(L"Creating new %s", businessObj.GetObjectName().c_str()), p_TaskData.GetOriginator());
		else
			LoggerService::User(YtriaTranslate::Do(BusinessObjectManager_WriteSingleRequest_1, _YLOC("Updating \"%1\" with id \"%2\""), businessObj.GetObjectName().c_str(), businessObj.GetID().c_str()), p_TaskData.GetOriginator());
		for (auto request : businessObj.SendWriteMultipleRequests(GetSapio365Session(), p_TaskData))
		{
			ASSERT(pkIndex < p_Keys.size());
			if (pkIndex < p_Keys.size())
			{
				operations.emplace_back(p_Keys[pkIndex++]);
				operations.back().StoreResult(request.GetTask().get());
			}
		}
	}

	return operations;
}
