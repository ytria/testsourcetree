#pragma once

class AppRole
{
public:
	boost::YOpt<vector<PooledString>> m_AllowedMemberTypes;
	boost::YOpt<PooledString> m_Description;
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<PooledString> m_Id;
	boost::YOpt<bool> m_IsEnabled;
	boost::YOpt<PooledString> m_Origin;
	boost::YOpt<PooledString> m_Value;
};

