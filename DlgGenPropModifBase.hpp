#pragma once

#include "BusinessObject.h"
#include "DlgGenPropModif.h"
#include "resource.h"

template<class T>
DlgGenPropModif<T>::DlgGenPropModif(vector<T>& p_Objects, UINT dlgID, DlgGenPropModifAction p_Type, CWnd* p_Parent)
	: ResizableDialog(dlgID, p_Parent)
	, m_Action(p_Type)
	, m_Objects(p_Objects)
{
}

template<class T>
DlgGenPropModifAction DlgGenPropModif<T>::GetAction() const
{
	return m_Action;
}

template<class T>
vector<T>& DlgGenPropModif<T>::getObjects()
{
	return m_Objects;
}
