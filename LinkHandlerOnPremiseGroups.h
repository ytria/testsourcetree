#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerOnPremiseGroups : public IBrowserLinkHandler
{
public:
	void Handle(YBrowserLink& p_Link) const override;
};

