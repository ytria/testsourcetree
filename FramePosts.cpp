#include "FramePosts.h"

#include "AutomationNames.h"

IMPLEMENT_DYNAMIC(FramePosts, GridFrameBase)

O365Grid& FramePosts::GetGrid()
{
    return m_Grid;
}

void FramePosts::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
	ASSERT(m_Grid.GetBackendRows().empty() || GetModuleCriteria().m_SubSubIDs == m_Grid.GetRefreshInfos());
    info.GetSubSubIds() = GetModuleCriteria().m_SubSubIDs;
    info.SetFrame(this);
    info.SetOrigin(Origin::Conversations);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);

    try
    {
        GridPosts& grid = dynamic_cast<GridPosts&>(GetGrid());
        grid.SetShowInlineAttachments(false);
    }
    catch (std::bad_cast)
    {
        ASSERT(false);
    }

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Post, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FramePosts::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Post, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

void FramePosts::ApplySpecific(bool p_Selected)
{
    m_Grid.ApplyDeleteAttachments(p_Selected);
}

void FramePosts::ApplyAllSpecific()
{
    m_Grid.ApplyDeleteAttachments(false);
}

void FramePosts::ApplySelectedSpecific()
{
    m_Grid.ApplyDeleteAttachments(true);
}

void FramePosts::ShowPosts(const O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>>& p_Posts, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_FullPurge)
{
    if (CompliesWithDataLoadPolicy())
        m_Grid.BuildView(p_Posts, p_UpdateOperations, p_Refresh, p_FullPurge);
}

void FramePosts::ShowPostsWithAttachments(const O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>>& p_Posts, const vector<O365UpdateOperation>& p_UpdateOperations, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge)
{
	if (CompliesWithDataLoadPolicy())
		m_Grid.BuildViewWithAttachments(p_Posts, p_UpdateOperations, p_BusinessAttachments, p_Refresh, p_FullPurge);
}

void FramePosts::ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments)
{
    m_Grid.ViewAttachments(p_BusinessAttachments, true, true);
}

void FramePosts::ShowItemAttachment(const BusinessItemAttachment& p_Attachment)
{
    m_Grid.ShowItemAttachment(p_Attachment);
}

void FramePosts::HiddenViaHistory()
{
	m_Grid.HideMessageViewer();
}

bool FramePosts::HasApplyButton()
{
    return true;
}

void FramePosts::createGrid()
{
    m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FramePosts::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(tab);
	CreateRefreshGroup(tab);
	CreateApplyGroup(tab);
	CreateRevertGroup(tab);

	auto groupPreview = tab.AddGroup(YtriaTranslate::Do(FramePosts_customizeActionsRibbonTab_2, _YLOC("Preview")).c_str());
	ASSERT(nullptr != groupPreview);
	if (nullptr != groupPreview)
	{
		auto ctrl = groupPreview->Add(xtpControlButton, ID_POSTSGRID_SHOWBODY);
		setImage({ ID_POSTSGRID_SHOWBODY }, IDB_MESSAGE_CONTENT, xtpImageNormal);
		setImage({ ID_POSTSGRID_SHOWBODY }, IDB_MESSAGE_CONTENT_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	auto group = tab.AddGroup(YtriaTranslate::Do(FramePosts_customizeActionsRibbonTab_1, _YLOC("Attachments")).c_str());
    ASSERT(nullptr != group);
    if (nullptr != group)
    {
		setGroupImage(*group, 0);

		{
			auto ctrl = group->Add(xtpControlButton, ID_POSTSGRID_SHOWATTACHMENTS);
			setImage({ ID_POSTSGRID_SHOWATTACHMENTS }, IDB_MESSAGES_SHOWATTACHMENTS, xtpImageNormal);
			setImage({ ID_POSTSGRID_SHOWATTACHMENTS }, IDB_MESSAGES_SHOWATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_POSTSGRID_TOGGLEINLINEATTACHMENTS);
			GridFrameBase::setImage({ ID_POSTSGRID_TOGGLEINLINEATTACHMENTS }, IDB_POSTS_TOGGLEINLINEATTACHMENTS, xtpImageNormal);
			GridFrameBase::setImage({ ID_POSTSGRID_TOGGLEINLINEATTACHMENTS }, IDB_POSTS_TOGGLEINLINEATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_POSTSGRID_DOWNLOADATTACHMENTS);
			setImage({ ID_POSTSGRID_DOWNLOADATTACHMENTS }, IDB_MESSAGES_DOWNLOADATTACHMENTS, xtpImageNormal);
			setImage({ ID_POSTSGRID_DOWNLOADATTACHMENTS }, IDB_MESSAGES_DOWNLOADATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_POSTSGRID_VIEWITEMATTACHMENT);
			setImage({ ID_POSTSGRID_VIEWITEMATTACHMENT }, IDB_MESSAGES_VIEWITEMATTACHMENT, xtpImageNormal);
			setImage({ ID_POSTSGRID_VIEWITEMATTACHMENT }, IDB_MESSAGES_VIEWITEMATTACHMENT_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		{
			auto ctrl = group->Add(xtpControlButton, ID_POSTSGRID_DELETEATTACHMENT);
			setImage({ ID_POSTSGRID_DELETEATTACHMENT }, IDB_MESSAGES_DELETEATTACHMENTS, xtpImageNormal);
			setImage({ ID_POSTSGRID_DELETEATTACHMENT }, IDB_MESSAGES_DELETEATTACHMENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
    }

    CreateLinkGroups(tab, GetOrigin() == Origin::User, GetOrigin() == Origin::Group);

	automationAddCommandIDToGreenLight(ID_POSTSGRID_SHOWBODY);

    return true;
}

AutomatedApp::AUTOMATIONSTATUS FramePosts::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
    AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (IsActionSelectedPreviewBody(p_Action))
		p_CommandID = ID_POSTSGRID_SHOWBODY;
	else if (IsActionSelectedAttachmentLoadInfo(p_Action))
	{
		p_CommandID = ID_POSTSGRID_SHOWATTACHMENTS;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedAttachmentDownload(p_Action))
	{
		p_CommandID = ID_POSTSGRID_DOWNLOADATTACHMENTS;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedPreviewItem(p_Action))
		p_CommandID = ID_POSTSGRID_VIEWITEMATTACHMENT;
	else if (IsActionSelectedAttachmentDelete(p_Action))
		p_CommandID = ID_POSTSGRID_DELETEATTACHMENT;

    return statusRV;
}