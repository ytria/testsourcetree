#include "AuditActivityInitiatorDeserializer.h"

#include "AppIdentityDeserializer.h"
#include "UserIdentityDeserializer.h"

void AuditActivityInitiatorDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		AppIdentityDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("app"), p_Object))
			m_Data.m_App = std::move(d.GetData());
	}
	{
		UserIdentityDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("user"), p_Object))
			m_Data.m_User = std::move(d.GetData());
	}
}
