#include "MigrationRunner.h"
#include "IMigrationVersion.h"
#include "IMigrationsProvisioner.h"
#include "LoggerService.h"
#include "MigrationUtil.h"
#include "FileUtil.h"
#include "SessionsSqlEngine.h"

MigrationRunner::MigrationRunner(std::unique_ptr<IMigrationsProvisioner> p_Provisioner)
	:m_Provisioner(std::move(p_Provisioner))
{
	m_Provisioner->Provision();
}

void MigrationRunner::Run()
{
	ASSERT(!SessionsSqlEngine::IsInit()); // Engine must not be initialized before Migration!

	VersionSourceTarget currentMigrationVer(-1, -1);
	try
	{
		for (auto& migration : m_Provisioner->GetMigrations())
		{
			currentMigrationVer = migration->GetVersionInfo();
			migration->Build();

			auto success = migration->Apply(); // TODO: pass logger and/or stuff for updating dialog box to the migration
			ASSERT(success); // If error occurred, an exception should have been thrown.
		}
	}
	catch (const std::exception& e)
	{
		ASSERT(false);
		LoggerService::Debug(_YFORMAT(L"Unable to apply migration with source version %s and target version %s.",
			std::to_wstring(currentMigrationVer.m_SourceVersion).c_str(), std::to_wstring(currentMigrationVer.m_TargetVersion).c_str()));
		LoggerService::Debug(MFCUtil::convertUTF8_to_UNICODE(e.what()));

		FileUtil::DeleteFiles({ MigrationUtil::GetMigrationFilePath(currentMigrationVer.m_TargetVersion) }, false, false);
	}
}

