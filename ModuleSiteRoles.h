#pragma once

#include "ModuleBase.h"

class FrameSiteRoles;

class ModuleSiteRoles : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

private:
    virtual void executeImpl(const Command& p_Command) override;

    void doRefresh(FrameSiteRoles* p_pFrame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command);

    void showSiteRoles(const Command& p_Command);
};

