#include "ChatMessagePropertySet.h"

//#include "rttr/property.h"

vector<rttr::property> ChatMessagePropertySet::GetPropertySet() const
{
	// Use GetStringPropertySet instead!!
	ASSERT(false);

	return {};
}

vector<PooledString> ChatMessagePropertySet::GetStringPropertySet() const
{
	return
	{
		_YTEXT("id"),
		_YTEXT("replyToId"),
		_YTEXT("from"),
		_YTEXT("etag"),
		_YTEXT("messageType"),
		_YTEXT("createdDateTime"),
		_YTEXT("lastModifiedDateTime"),
		_YTEXT("isDeleted"),
		_YTEXT("deletedDateTime"),
		_YTEXT("subject"),
		_YTEXT("body"),
		_YTEXT("summary"),
		_YTEXT("attachments"),
		_YTEXT("mentions"),
		_YTEXT("importance"),
		_YTEXT("reactions"),
		_YTEXT("locale")
	};
}
