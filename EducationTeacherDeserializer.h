#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "EducationTeacher.h"

class EducationTeacherDeserializer : public JsonObjectDeserializer, public Encapsulate<EducationTeacher>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

