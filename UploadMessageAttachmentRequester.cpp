#include "UploadMessageAttachmentRequester.h"
#include "FileUtil.h"
#include "MsGraphHttpRequestLogger.h"
#include "O365AdminUtil.h"
#include "JsonSerializeUtil.h"

UploadMessageAttachmentRequester::UploadMessageAttachmentRequester(const wstring& p_FilePath, const wstring& p_MessageId)
	: m_FilePath(p_FilePath),
	m_MessageId(p_MessageId),
	m_UseMainGraphSession(false)
{
}

TaskWrapper<void> UploadMessageAttachmentRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	boost::YOpt<wstring> error;
	const auto fileSize = FileUtil::GetFileSize(m_FilePath, error);
	const auto fileSizeInBytes = fileSize ? *fileSize : 0;
	const auto fileName = FileUtil::FileGetFileName(m_FilePath);
	
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	if (fileSizeInBytes >= 0)
	{
		static const size_t thresholdInMB = 3;

		if (fileSizeInBytes <= thresholdInMB * 1024 * 1024)
		{
			// Simple upload (less than 3 MB)
			return YSafeCreateTask([useMainMSGraphSession = m_UseMainGraphSession, httpLogger, result = m_Result, msgId = m_MessageId, fileSizeInBytes, fileName, filePath = m_FilePath, p_Session, p_TaskData]()
			{
				web::uri_builder uri;
				uri.append_path(_YTEXT("me"));
				uri.append_path(_YTEXT("messages"));
				uri.append_path(msgId);
				uri.append_path(_YTEXT("attachments"));

				vector<uint8_t> fileData;
				fileData.resize(static_cast<uint32_t>(fileSizeInBytes));

				std::ifstream file;
				file.open(filePath, ios::in | ios::binary);
				ASSERT(file);
				if (file)
				{
					file.read((char*)&fileData[0], fileSizeInBytes);

					json::value json = web::json::value::object({
						{ _YTEXT("@odata.type"), web::json::value::string(_YTEXT("#microsoft.graph.fileAttachment")) },
						{ _YTEXT("name"), web::json::value::string(fileName) },
						{ _YTEXT("contentBytes"), web::json::value::string(utility::conversions::to_base64(fileData)) }
					});

					auto graphSession = p_Session->GetMSGraphSession(httpLogger->GetSessionType());
					if (useMainMSGraphSession)
						graphSession = p_Session->GetMainMSGraphSession();

					*result = HttpResultWithError(graphSession->Post(uri.to_uri(), httpLogger, p_TaskData, WebPayloadJSON(json)).GetTask().get());
					LoggerService::User(_T("Uploading... 100%"), p_TaskData.GetOriginator());
				}
			});
		}
		else
		{
			// Resumable upload
			return YSafeCreateTask([httpLogger, result = m_Result, fileSizeInBytes, filePath = m_FilePath, msgId = m_MessageId, fileName, p_Session, p_TaskData]()
			{
				web::uri_builder uri;
				uri.append_path(_YTEXT("me"));
				uri.append_path(_YTEXT("messages"));
				uri.append_path(msgId);
				uri.append_path(_YTEXT("attachments"));
				uri.append_path(_YTEXT("createUploadSession"));

				WebPayloadJSON payload(web::json::value::object(
					{
						{
							_YTEXT("AttachmentItem"), json::value::object({
								{ _YTEXT("attachmentType"), web::json::value::string(_YTEXT("file")) },
								{ _YTEXT("name"), web::json::value::string(fileName) },
								{ _YTEXT("size"), web::json::value::number(fileSizeInBytes) }
							})
						}
					}
				));

				*result = Util::GetResult(p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_uri(), httpLogger, p_TaskData, payload, _YTEXT("beta")));
				auto body = result->m_ResultInfo->GetBodyAsString();
				if (body)
				{
					const auto resultJson = json::value::parse(*body);
					wstring url;
					JsonSerializeUtil::DeserializeString(PooledString(_YTEXT("uploadUrl")), url, resultJson.as_object());
					ASSERT(!url.empty());
					if (!url.empty())
					{
						static const int blockSize = 4 * 1024 * 1024; //4 MB
						auto dataBlock = std::make_shared<vector<uint8_t>>();
						dataBlock->reserve(blockSize);
						WebPayloadBinaryBlock payloadBlock(dataBlock);

						std::ifstream inputFile;
						inputFile.open(filePath, ios::binary | ios::ate);
						const auto fileSize = inputFile.tellg();

						http_headers headers;
						std::size_t pos = 0;

						size_t bytesLeft = static_cast<size_t>(fileSize) - pos;
						while (inputFile && bytesLeft != 0)
						{
							size_t currentBlockSize = bytesLeft >= blockSize ? blockSize : bytesLeft;
							dataBlock->resize(currentBlockSize);
							inputFile.seekg(pos, inputFile.beg);
							inputFile.read((char*)&(*dataBlock)[0], currentBlockSize);

							headers[web::http::header_names::content_length] = Str::getStringFromNumber(currentBlockSize);
							headers[web::http::header_names::content_range] = _YTEXTFORMAT(L"bytes %s-%s/%s", Str::getStringFromNumber(pos).c_str(), Str::getStringFromNumber(pos + currentBlockSize - 1).c_str(), Str::getStringFromNumber(fileSize).c_str());

							*result = Util::GetResult(p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Put(url, httpLogger, p_TaskData, payloadBlock, {}, headers));

							pos += currentBlockSize;
							bytesLeft = static_cast<size_t>(fileSize) - pos;

							LoggerService::User(_YFORMAT(L"Uploading... %s%%", Str::getStringFromNumber(100 * pos / fileSize).c_str()), p_TaskData.GetOriginator());

							if (result->m_ResultInfo->Response.status_code() == http::status_codes::Created)
								ASSERT(pos == fileSize);
							else if (result->m_ResultInfo->Response.status_code() != http::status_codes::OK)
								return;
						}
					}
				}
			});
		}
	}

	return TaskWrapper<void>();
}

const HttpResultWithError& UploadMessageAttachmentRequester::GetResult() const
{
	return *m_Result;
}

void UploadMessageAttachmentRequester::UseMainMSGraphSession()
{
	m_UseMainGraphSession = true;
}
