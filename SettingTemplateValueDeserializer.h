#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "SettingTemplateValue.h"

class SettingTemplateValueDeserializer : public JsonObjectDeserializer, public Encapsulate<SettingTemplateValue>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

