#include "BackstagePanelSessions.h"
#include "CommandDispatcher.h"
#include "GridFrameBase.h"
#include "MainFrame.h"
#include "MainFrameSapio365Session.h"
#include "MSGraphSession.h"
#include "Office365Admin.h"
#include "SessionTypes.h"
#include "TaskDataManager.h"
#include "SessionsSqlEngine.h"
#include "QueryDeleteSession.h"
#include "SessionSwitcher.h"
#include "SessionLoaderFactory.h"

BEGIN_MESSAGE_MAP(BackstagePanelSessions, BackstagePanelWithGrid<GridSessionsBackstage>)
    ON_BN_CLICKED(IDC_BACKSTAGE_SESSIONS_BTN_LOAD,		OnLoadSession)
    ON_BN_CLICKED(IDC_BACKSTAGE_SESSIONS_BTN_DELETE,	OnDeleteSession)
	ON_BN_CLICKED(IDC_BACKSTAGE_SESSIONS_BTN_FAVORITE,	OnSetFavorite)
    ON_BN_CLICKED(IDC_BACKSTAGE_SESSIONS_BTN_SIGNOUT,	OnSignout)
	ON_BN_CLICKED(IDC_BACKSTAGE_SESSIONS_BTN_REMOVEULTRAADMINPART, OnRemoveUltraAdminPart)
END_MESSAGE_MAP()

BackstagePanelSessions::BackstagePanelSessions()
    : BackstagePanelWithGrid<GridSessionsBackstage>(IDD)
	, m_IsSigningOut(false)
{
    m_Grid = std::make_shared<GridSessionsBackstage>(*this);

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		mainFrame->SetBackstagePanelSessions(this);
}

BackstagePanelSessions::~BackstagePanelSessions()
{
	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	if (nullptr != mainFrame)
		mainFrame->SetBackstagePanelSessions(nullptr);
}

BOOL BackstagePanelSessions::OnInitDialog()
{
    BackstagePanelWithGrid<GridSessionsBackstage>::OnInitDialog();

    setCaption(YtriaTranslate::Do(BackstagePanelSessions_OnInitDialog_1, _YLOC("Recent Sessions")).c_str());

	m_Grid->AddSelectionObserver(this);

	UINT ids[] { IDC_BACKSTAGE_SESSIONS_BTN_DELETE, IDC_BACKSTAGE_SESSIONS_BTN_LOAD, IDC_BACKSTAGE_SESSIONS_BTN_FAVORITE, IDC_BACKSTAGE_SESSIONS_BTN_SIGNOUT, IDC_BACKSTAGE_SESSIONS_BTN_REMOVEULTRAADMINPART };
	m_imagelist.SetIcons(IDB_BACKSTAGE_SESSIONS_BUTTON_ICONS, ids, 5, CSize(0, 0), xtpImageNormal);

	SetupButton(m_BtnLoad
		, YtriaTranslate::Do(BackstagePanelSessions_OnInitDialog_2, _YLOC("Load selected session")).c_str()
		, YtriaTranslate::Do(BackstagePanelSessions_OnInitDialog_3, _YLOC("Any open windows from other sessions will remain active.")).c_str()
		, m_imagelist);

	SetupButton(m_BtnDelete
		, YtriaTranslate::Do(BackstagePanelSessions_OnInitDialog_4, _YLOC("Delete")).c_str()
		, YtriaTranslate::Do(BackstagePanelSessions_OnInitDialog_5, _YLOC("Any information for the selected sessions will be securely deleted from this computer.")).c_str()
		, m_imagelist);

	SetupButton(m_BtnFavorite
		, YtriaTranslate::Do(BackstagePanelSessions_OnInitDialog_6, _YLOC("Add to favorites")).c_str()
		, YtriaTranslate::Do(BackstagePanelSessions_OnInitDialog_7, _YLOC("Favorite sessions are pinned to the top of your session list.")).c_str()
		, m_imagelist);

    SetupButton(m_BtnSignOut
        , YtriaTranslate::Do(BackstagePanelSessions_OnInitDialog_8, _YLOC("Sign Out")).c_str()
        , YtriaTranslate::Do(BackstagePanelSessions_OnInitDialog_9, _YLOC("Lock the selected sessions: password will need to be entered again.")).c_str()
        , m_imagelist);

	SetupButton(m_BtnRemoveUltraAdminPart
		, _T("Downgrade")
		, _T("Downgrade the level of the selected Advanced session by removing its elevated privileges.")
		, m_imagelist);

	const float g_NbButtons = 5.0f;

	SetResize(IDC_BACKSTAGE_SESSIONS_BTN_LOAD, CXTPResizePoint(0, 0), CXTPResizePoint(1 / g_NbButtons, 0));
	SetResize(IDC_BACKSTAGE_SESSIONS_BTN_SIGNOUT, CXTPResizePoint(1 / g_NbButtons, 0), CXTPResizePoint(2 / g_NbButtons, 0));
	SetResize(IDC_BACKSTAGE_SESSIONS_BTN_FAVORITE, CXTPResizePoint(2 / g_NbButtons, 0), CXTPResizePoint(3 / g_NbButtons, 0));
    SetResize(IDC_BACKSTAGE_SESSIONS_BTN_DELETE, CXTPResizePoint(3 / g_NbButtons, 0), CXTPResizePoint(4 / g_NbButtons, 0));
	SetResize(IDC_BACKSTAGE_SESSIONS_BTN_REMOVEULTRAADMINPART, CXTPResizePoint(4 / g_NbButtons, 0), CXTPResizePoint(1.f, 0));

	// To initialize the detailed view.
	m_DetailedView.ShowWindow(SW_SHOW);
	SelectionChanged(*m_Grid);

    return TRUE;
}

BOOL BackstagePanelSessions::OnSetActive()
{
	BackstagePanelWithGrid<GridSessionsBackstage>::OnSetActive();

    ASSERT(m_Grid);

	m_Grid->Load(true);

	SelectionChanged(*m_Grid);
	UpdateButtons();
    return TRUE;
}

void BackstagePanelSessions::DoDataExchange(CDataExchange * pDX)
{
    BackstagePanelWithGrid<GridSessionsBackstage>::DoDataExchange(pDX);

    DDX_Control(pDX, IDC_BACKSTAGE_SESSIONS_BTN_LOAD, m_BtnLoad);
    DDX_Control(pDX, IDC_BACKSTAGE_SESSIONS_BTN_DELETE, m_BtnDelete);
	DDX_Control(pDX, IDC_BACKSTAGE_SESSIONS_BTN_FAVORITE, m_BtnFavorite);
    DDX_Control(pDX, IDC_BACKSTAGE_SESSIONS_BTN_SIGNOUT, m_BtnSignOut);
	DDX_Control(pDX, IDC_BACKSTAGE_SESSIONS_BTN_REMOVEULTRAADMINPART, m_BtnRemoveUltraAdminPart);
}

void BackstagePanelSessions::OnLoadSession()
{
	if (!TaskDataManager::Get().HasTaskRunning())
	{
		if (m_Grid->GetSelectedRows().size() == 1 && !m_Grid->IsMainFrameSession(*(m_Grid->GetSelectedRows().begin()))) // && !m_Grid->RowStatusIs(*(m_Grid->GetSelectedRows().begin()), SessionStatus::ACTIVE)
		{
			auto emailOrAppIdCol = m_Grid->GetEmailOrAppIdCol();
			auto sessionTypeCol = m_Grid->GetSessionTypeTechnicalCol();
			auto roleIDCol = m_Grid->GetColumnRoleID();
			ASSERT(nullptr != emailOrAppIdCol && nullptr != roleIDCol && nullptr != sessionTypeCol);
			if (nullptr != emailOrAppIdCol && nullptr != roleIDCol && nullptr != sessionTypeCol)
			{
				auto row = *(m_Grid->GetSelectedRows().begin());
				const wstring emailOrAppId = row->GetField(emailOrAppIdCol).GetValueStr();
				const wstring sessionType = row->GetField(sessionTypeCol).GetValueStr();
				const int64_t roleID = row->HasField(roleIDCol) ? Str::getINT64FromString(row->GetField(roleIDCol).GetValueStr()) : 0;

				Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
				ASSERT(nullptr != app);
				if (nullptr != app)
				{
					CloseBackstage();
					
					auto loader = SessionLoaderFactory({ emailOrAppId, sessionType, roleID }).CreateLoader();
					SessionSwitcher(std::move(loader)).Switch();
				}
			}
		}
	}
}

void BackstagePanelSessions::OnDeleteSession()
{
	if (!TaskDataManager::Get().HasTaskRunning())
	{
		auto emailOrAppIdCol = m_Grid->GetEmailOrAppIdCol();
		auto sessionTypeCol = m_Grid->GetSessionTypeTechnicalCol();
		auto roleIDCol = m_Grid->GetColumnRoleID();
		ASSERT(nullptr != emailOrAppIdCol && nullptr != roleIDCol && nullptr != sessionTypeCol);
		if (nullptr != emailOrAppIdCol && nullptr != roleIDCol && nullptr != sessionTypeCol)
		{
            Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
            ASSERT(nullptr != app);
            if (nullptr != app)
            {
			    for (auto row : m_Grid->GetSelectedRows())
			    {
					if (!m_Grid->RowStatusIs(row, SessionStatus::ACTIVE))
					{
						const wstring emailOrAppId = row->GetField(emailOrAppIdCol).GetValueStr();
						const wstring sessionType = row->GetField(sessionTypeCol).GetValueStr();
						const int64_t roleID = row->HasField(roleIDCol) ? Str::getINT64FromString(row->GetField(roleIDCol).GetValueStr()) : 0;
						QueryDeleteSession queryDelete(SessionsSqlEngine::Get(), { emailOrAppId, sessionType, roleID });
						queryDelete.Run();
					}
			    }

                // Update list after deletion.
                MainFrame* mainFrame = dynamic_cast<MainFrame*>(app->m_pMainWnd);
                ASSERT(nullptr != mainFrame);
                if (nullptr != mainFrame)
                    mainFrame->PopulateRecentSessions();

			    OnSetActive(); // Refresh view
            }
		}
	}
}

void BackstagePanelSessions::OnSetFavorite()
{
	const bool shouldSetFavorite = !m_Grid->AllSelectedRowsAreAlreadyFavorite();

	auto emailOrAppIdCol = m_Grid->GetEmailOrAppIdCol();
	auto sessionTypeCol = m_Grid->GetSessionTypeTechnicalCol();
	auto roleIDCol = m_Grid->GetColumnRoleID();
	ASSERT(nullptr != emailOrAppIdCol && nullptr != roleIDCol && nullptr != sessionTypeCol);
	if (nullptr != emailOrAppIdCol && nullptr != roleIDCol && nullptr != sessionTypeCol)
	{
		for (auto row : m_Grid->GetSelectedRows())
		{
			if (row && !(!CRMpipe().IsFeatureSandboxed() && m_Grid->GetSelectedSession(*row)->IsUltraAdmin()))
			{
				const wstring emailOrAppId = row->GetField(emailOrAppIdCol).GetValueStr();
				const wstring sessionType = row->GetField(sessionTypeCol).GetValueStr();
				const int64_t roleID = row->HasField(roleIDCol) ? Str::getINT64FromString(row->GetField(roleIDCol).GetValueStr()) : 0;

				Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
				ASSERT(nullptr != app);
				if (nullptr != app)
					app->SetFavoriteSession({ emailOrAppId, sessionType, roleID }, shouldSetFavorite);
			}
		}

		OnSetActive(); // Refresh view
	}
}

void BackstagePanelSessions::OnSignout()
{
	if (!TaskDataManager::Get().HasTaskRunning() && !AutomatedApp::IsAutomationRunning())
	{
		const bool shouldSetFavorite = !m_Grid->AllSelectedRowsAreAlreadyFavorite();

		Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());

		MainFrame* mainFrame = dynamic_cast<MainFrame*>(app->m_pMainWnd);
		ASSERT(nullptr != mainFrame);

		auto emailOrAppIdCol = m_Grid->GetEmailOrAppIdCol();
		auto sessionTypeCol = m_Grid->GetSessionTypeTechnicalCol();
		auto roleIDCol = m_Grid->GetColumnRoleID();
		auto roleNameCol = m_Grid->GetColumnRoleName();
		ASSERT(nullptr != emailOrAppIdCol && nullptr != roleIDCol && nullptr != sessionTypeCol);
		if (nullptr != emailOrAppIdCol && nullptr != roleIDCol && nullptr != sessionTypeCol)
		{
			// Because of the warning dialog (and the roles), we prevent signing out several sessions at a time.
			ASSERT(m_Grid->GetSelectedRows().size() == 1);

			{
				ScopedBool _(m_IsSigningOut, true);
				for (auto row : m_Grid->GetSelectedRows())// sign out can trigger a grid reset, but m_IsSigningOut prevent sessionChanged from doing anything.
				{
					if (!m_Grid->RowStatusIs(row, SessionStatus::LOCKED))
					{
						const wstring emailOrAppId = row->GetField(emailOrAppIdCol).GetValueStr();
						const wstring sessionType = row->GetField(sessionTypeCol).GetValueStr();
						const int64_t roleID = row->HasField(roleIDCol) ? Str::getINT64FromString(row->GetField(roleIDCol).GetValueStr()) : 0;
						const wstring roleName = row->HasField(roleNameCol) ? row->GetField(roleNameCol).GetValueStr() : _YTEXT("");

						SessionIdentifier sessionIdentifier{ emailOrAppId, sessionType, roleID };

						std::shared_ptr<Sapio365Session> graphSession;

						bool frameWithPendingMod = false;
						auto gridFrames = CommandDispatcher::GetInstance().GetAllGridFrames();
						for (const auto& gridFrame : gridFrames)
						{
							if (nullptr != gridFrame
								&& gridFrame->GetSessionInfo().GetIdentifier() == sessionIdentifier)
							{
								graphSession = gridFrame->GetSapio365Session();
								if (!gridFrame->CanBeClosedWithoutConfirmation())
								{
									frameWithPendingMod = true;
									break;
								}
							}
						}

						if (!graphSession && nullptr != mainFrame && mainFrame->GetSessionInfo().GetIdentifier() == sessionIdentifier)
							graphSession = mainFrame->GetSapio365Session();

						bool proceed = true;
						if (frameWithPendingMod && graphSession)
						{
							wstring message;
							if (sessionIdentifier.IsStandard())
							{
								message = _YFORMAT(L"At least one window using the Standard session '%s' has unsaved changes.\nDo you want to continue?", emailOrAppId.c_str());
							}
							else if (sessionIdentifier.IsRole())
							{
								message = _YFORMAT(L"At least one window using the Role session '[%s] %s' has unsaved changes.\nDo you want to continue?", roleName.c_str(), emailOrAppId.c_str());
							}
							else if (sessionIdentifier.IsAdvanced())
							{
								message = _YFORMAT(L"At least one window using the Advanced session '%s' has unsaved changes.\nDo you want to continue?", emailOrAppId.c_str());
							}
							else if (sessionIdentifier.IsElevated())
							{
								message = _YFORMAT(L"At least one window using the Advanced session with Elevated privileges '%s' has unsaved changes.\nDo you want to continue?", emailOrAppId.c_str());
							}
							else if (sessionIdentifier.IsUltraAdmin())
							{
								message = _YFORMAT(L"At least one window using the Ultra Admin session '%s' has unsaved changes.\nDo you want to continue?", emailOrAppId.c_str());
							}

							YCodeJockMessageBox msgBox(this,
								DlgMessageBox::eIcon::eIcon_ExclamationWarning,
								_T("Changes pending"),
								message,
								_YTEXT("You can cancel and go save them before signing out."),
								{ { IDOK, YtriaTranslate::Do(consoleEZApp_InitInstanceSpecific_1, _YLOC("Continue")).c_str() }, { IDCANCEL, YtriaTranslate::Do(DlgChartToClipboard_OnInitDialog_9, _YLOC("Cancel")).c_str() } });
							proceed = IDOK == msgBox.DoModal();
						}

						if (proceed)
						{
							wstring message;
							if (sessionIdentifier.IsStandard())
							{
								message = _YFORMAT(L"Do you really want to lock the Standard session '%s'?", emailOrAppId.c_str());
							}
							else if (sessionIdentifier.IsRole())
							{
								message = _YFORMAT(L"Do you really want to lock the Role session '[%s] %s'?", roleName.c_str(), emailOrAppId.c_str());
							}
							else if (sessionIdentifier.IsAdvanced())
							{
								message = _YFORMAT(L"Do you really want to lock the Advanced session '%s'?", emailOrAppId.c_str());
							}
							else if (sessionIdentifier.IsElevated())
							{
								message = _YFORMAT(L"Do you really want to lock the Advanced session with Elevated privileges '%s'?", emailOrAppId.c_str());
							}
							else if (sessionIdentifier.IsUltraAdmin())
							{
								message = _YFORMAT(L"Do you really want to lock the Ultra Admin session '%s'?", emailOrAppId.c_str());
							}

							proceed = IDOK == YCodeJockMessageBox(this,
								DlgMessageBox::eIcon_Question,
								YtriaTranslate::Do(GridFrameBase_logout_1, _YLOC("Sign out")).c_str(),
								message,
								YtriaTranslate::Do(GridFrameBase_logout_3, _YLOC("Your password for this session will be lost. You will need to reenter it when signing in.")).c_str(),
								{ { IDOK, YtriaTranslate::Do(GridFrameBase_logout_4, _YLOC("Ok")).c_str() }, { IDCANCEL, YtriaTranslate::Do(GridFrameBase_logout_5, _YLOC("Cancel")).c_str() } }
							).DoModal();
						}

						if (proceed)
						{
							if (graphSession)
							{
								Sapio365Session::SignOut(graphSession, this);
							}
							else
							{
								if (SessionsSqlEngine::Get().HasSharedCredentials(sessionIdentifier))
								{
									YCodeJockMessageBox msgBox(this,
										DlgMessageBox::eIcon::eIcon_ExclamationWarning,
										YtriaTranslate::Do(BackstagePanelSessions_OnSignout_3, _YLOC("Several sessions share the same credentials.")).c_str(),
										YtriaTranslate::Do(BackstagePanelSessions_OnSignout_7, _YLOC("Every Standard or Role session using %1 account will be signed-out"), sessionIdentifier.m_EmailOrAppId.c_str()),
										_YTEXT(""),
										{ { IDOK, YtriaTranslate::Do(consoleEZApp_InitInstanceSpecific_1, _YLOC("Continue")).c_str() }, { IDCANCEL, YtriaTranslate::Do(DlgChartToClipboard_OnInitDialog_9, _YLOC("Cancel")).c_str() } });
									proceed = IDOK == msgBox.DoModal();
								}

								if (proceed)
									SessionsSqlEngine::Get().Logout(sessionIdentifier);
							}

							if (nullptr != mainFrame)
								mainFrame->PopulateRecentSessions();
						}
					}
				}
			}

			OnSetActive(); // Refresh view
		}
	}
}

void BackstagePanelSessions::OnRemoveUltraAdminPart()
{
	for (auto row : m_Grid->GetSelectedRows())
	{
		ASSERT(nullptr != row);
		if (nullptr != row)
		{
			const auto& session = m_Grid->GetSelectedSession(*row);
			if (nullptr != session && (session->IsElevated() || session->IsPartnerElevated()) && !m_Grid->RowStatusIs(row, SessionStatus::ACTIVE))
				SessionsSqlEngine::Get().DowngradeElevatedSession(session->GetIdentifier());
		}
	}

	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != app)
	{
		MainFrame* mainFrame = dynamic_cast<MainFrame*>(app->m_pMainWnd);
		ASSERT(nullptr != mainFrame);
		if (nullptr != mainFrame)
			mainFrame->PopulateRecentSessions();

		OnSetActive(); // Refresh view
	}
}

void BackstagePanelSessions::UpdateButtons()
{
	bool hasTaskRunning = TaskDataManager::Get().HasTaskRunning();
	bool automationRunning = AutomatedApp::IsAutomationRunning();

	bool ultraAdminDisabled = !CRMpipe().IsFeatureSandboxed();
	bool hasAllUltraAdminSelected = m_Grid->HasAnyUltraAdminSelectedSession();

	m_BtnLoad.EnableWindow(!(ultraAdminDisabled && hasAllUltraAdminSelected) && !m_Grid->IsMainFrameSessionSelected() && !hasTaskRunning && !automationRunning); // && m_Grid->HasNonActiveSelectedSession(true)
    m_BtnDelete.EnableWindow(m_Grid->HasNonActiveSelectedSession(false) && !hasTaskRunning && !automationRunning);
	m_BtnSignOut.EnableWindow(!(ultraAdminDisabled && hasAllUltraAdminSelected) && m_Grid->HasNonLoggedOutSelectedSession(true) && !hasTaskRunning && !automationRunning);
	m_BtnRemoveUltraAdminPart.EnableWindow(m_Grid->HasInactiveElevatedSessionSelected() && !hasTaskRunning && !automationRunning);

	if (m_Grid->HasOneNonGroupRowSelectedAtLeast())
	{
		m_BtnFavorite.EnableWindow(!(m_Grid->HasOneUltraAdminSelectedSession()));

		if (m_Grid->AllSelectedRowsAreAlreadyFavorite())
		{
			SetupButton(m_BtnFavorite
				, YtriaTranslate::Do(BackstagePanelSessions_UpdateButtons_1, _YLOC("Remove from favorites")).c_str()
				, YtriaTranslate::Do(BackstagePanelSessions_UpdateButtons_2, _YLOC("They won't stay on top of your session list anymore.")).c_str()
				, m_imagelist);
		}
		else
		{
			SetupButton(m_BtnFavorite
				, YtriaTranslate::Do(BackstagePanelSessions_UpdateButtons_3, _YLOC("Add to favorites")).c_str()
				, YtriaTranslate::Do(BackstagePanelSessions_UpdateButtons_4, _YLOC("Favorite sessions are pinned to the top of your session list.")).c_str()
				, m_imagelist);
		}
	}
	else
	{
		m_BtnFavorite.EnableWindow(FALSE);
		SetupButton(m_BtnFavorite
			, YtriaTranslate::Do(BackstagePanelSessions_UpdateButtons_5, _YLOC("Add to favorites")).c_str()
			, YtriaTranslate::Do(BackstagePanelSessions_UpdateButtons_6, _YLOC("Favorite sessions are pinned to the top of your session list.")).c_str()
			, m_imagelist);
	}
}

void BackstagePanelSessions::SessionChanged()
{
	if (IsActive() && !m_IsSigningOut)
		OnSetActive(); // Refresh view
}

void BackstagePanelSessions::SetTheme(const XTPControlTheme nTheme)
{
	BackstagePanelWithGrid<GridSessionsBackstage>::SetTheme(nTheme);

	m_BtnLoad.SetTheme(nTheme);
	m_BtnDelete.SetTheme(nTheme);
	m_BtnFavorite.SetTheme(nTheme);
	m_BtnSignOut.SetTheme(nTheme);
	m_BtnRemoveUltraAdminPart.SetTheme(nTheme);
}

void BackstagePanelSessions::SelectionChanged(const CacheGrid& grid)
{
	UpdateButtons();

	CString markup;

	auto selectedSession = m_Grid->GetSelectedSession();
	if (nullptr != selectedSession)
	{
		wstring sessionName = selectedSession->GetSessionName().c_str();
        if (selectedSession->GetRoleId() != 0)
            sessionName = _YTEXTFORMAT(L"[ %s ]", selectedSession->GetRoleNameInDb().c_str());
		const CString tenantName = selectedSession->GetTenantDisplayName().c_str();
		CString creationDate;
		TimeUtil::GetInstance().ConvertDateToText(DateTimeFormat(), selectedSession->GetCreatedOn(), creationDate);
		creationDate = YtriaTranslate::Do(BackstagePanelSessions_SelectionChanged_6, _YLOC("Created on %1"), creationDate).c_str();
		const CString sessionTypeIconString = [selectedSession]()
		{
			if (selectedSession->GetRoleId() != 0)
			{
				if (RoleDelegationManager::GetInstance().IsRoleEnforced(selectedSession->GetRoleId(), MainFrameSapio365Session()))
					return CString(Str::getStringFromNumber(IDB_SESSIONINFO_ROLE_ENFORCED).c_str());
				else
					return CString(Str::getStringFromNumber(IDB_SESSIONINFO_ROLE).c_str());
			}
			if (selectedSession->IsStandard())
				return CString(Str::getStringFromNumber(IDB_SESSIONINFO_STANDARD).c_str());
			if (selectedSession->IsAdvanced())
				return CString(Str::getStringFromNumber(IDB_SESSIONINFO_ADVANCED).c_str());
			if (selectedSession->IsUltraAdmin())
				return CString(Str::getStringFromNumber(IDB_SESSIONINFO_ULTRAADMIN).c_str());
			if (selectedSession->IsElevated())
				return CString(Str::getStringFromNumber(IDB_SESSIONINFO_ELEVATED).c_str());
			if (selectedSession->IsPartnerAdvanced())
				return CString(Str::getStringFromNumber(IDB_SESSIONINFO_PARTNER).c_str());
			if (selectedSession->IsPartnerElevated())
				return CString(Str::getStringFromNumber(IDB_SESSIONINFO_PARTNER_ELEVATED).c_str());

			ASSERT(FALSE);
			return CString(_YTEXT(""));
		}();
			
		CString lastUsedDate;
		TimeUtil::GetInstance().ConvertDateToText(DateTimeFormat(), selectedSession->GetLastUsedOn(), lastUsedDate);
		const CString status = PersistentSession::GetStatusString(selectedSession->GetStatus(selectedSession->GetRoleId())).c_str();
		const CString statusIconString = [selectedSession]()
		{
			switch (selectedSession->GetStatus(selectedSession->GetRoleId()))
			{
			case SessionStatus::INVALID:
				break;
			case SessionStatus::ACTIVE:
				return CString(Str::getStringFromNumber(IDB_SESSIONINFO_STATUSCURRENT).c_str());
			case SessionStatus::CACHED:
				return CString(Str::getStringFromNumber(IDB_SESSIONINFO_STATUSCACHED).c_str());
			case SessionStatus::INACTIVE:
				return CString(Str::getStringFromNumber(IDB_SESSIONINFO_STATUSINACTIVE).c_str());
			case SessionStatus::LOCKED:
				return CString(Str::getStringFromNumber(IDB_SESSIONINFO_STATUSLOCKED).c_str());
			}
			ASSERT(FALSE);
			return CString(_YTEXT(""));
		}();
		const CString accessCount = Str::getStringFromNumber(selectedSession->GetAccessCount()).c_str();

		//static const CString lastUsedIconString = Str::getStringFromNumber(IDB_SESSIONINFO_LASTUSED).c_str();
		//static const CString accessCountIconString = Str::getStringFromNumber(IDB_SESSIONINFO_ACCESSCOUNT).c_str();
		//static const CString tenantIconString = Str::getStringFromNumber(IDB_SESSIONINFO_TENANT).c_str();

		static const CString tenantLabel = YtriaTranslate::Do(BackstagePanelSessions_SelectionChanged_1, _YLOC("Tenant Name")).c_str();
		static const CString lastUsedLabel = YtriaTranslate::Do(BackstagePanelSessions_SelectionChanged_2, _YLOC("Last Used On")).c_str();
		static const CString statusLabel = YtriaTranslate::Do(BackstagePanelSessions_SelectionChanged_3, _YLOC("Status")).c_str();
		static const CString accessCountLabel = YtriaTranslate::Do(BackstagePanelSessions_SelectionChanged_4, _YLOC("Access Count")).c_str();


		// %s order : 
		// - favorite icon resource ID (only if selectedSessionMarkupFavoriteLine inserted)
		// - session type icon resource ID
		// - session name
		// - creation date
		// - tenant icon resource ID
		// - tenant label
		// - tenant name
		// - last used icon resource ID
		// - last used label
		// - last used date
		// - status icon resource ID
		// - status label
		// - status
		// - access count icon resource ID
		// - access count label
		// - access count

		static const CString selectedSessionMarkupBegin =
			L"<DockPanel>"
			L""
			L"    <DockPanel Background='#ffffff' Width='38'>"
			L"        <TextBlock VerticalAlignment='Stretch' DockPanel.Dock='Left'>"
			;

		static const CString selectedSessionMarkupFavoriteLine =
			L"            <!--If favorite, add the image-->"
			L"            <Image Source='res://#%s' VerticalAlignment='Top'/>"
			;

		static const CString selectedSessionMarkupEnd =
			L"        </TextBlock>"
			L"    </DockPanel>"
			L""
			L"    <WrapPanel Margin='0' Orientation='Horizontal' Background='#ffffff' VerticalAlignment='Top' DockPanel.Dock='Right' Height='Auto'>"
			L""
			L"        <StackPanel Margin='0' VerticalAlignment='Top' Orientation='Horizontal'>"
			L"            <TextBlock Background='#336699' Width='36' Margin='0, 2, 0, 0' Height='35'>"
			L"                <Image Source='res://#%s' Margin='5, 4, 5, 0'/>"
			L"            </TextBlock>"
			L"            <TextBlock Margin='10, 5, 0, 0' FontFamily='Segoe UI' FontSize='12'>"
			L"                <Span Foreground='#444444' FontWeight='bold'>%s</Span>"
			L"                <LineBreak/>"
			L"                <Span Foreground='#6a6a6a'>%s</Span>"
			L"            </TextBlock>"
			L"        </StackPanel>"
			L""
			L"        <StackPanel Width='2' Background='#336699' Margin='10, 0, 10, 0' Height='40' VerticalAlignment='Top'/>"
			L""
			L"        <StackPanel Margin='0' VerticalAlignment='Top' Orientation='Horizontal'>"
			L"            <TextBlock Background='#336699' Width='36' Margin='0, 2, 0, 0' Height='35'>"
			L"                <Image Source='res://#%s' Margin='5, 4, 5, 0'/>"
			L"            </TextBlock>"
			L"            <TextBlock Margin='10, 5, 10, 0' FontFamily='Segoe UI' FontSize='12'>"
			L"                <Span Foreground='#444444' FontWeight='bold'>%s</Span>"
			L"                <LineBreak/>"
			L"                <Span Foreground='#6a6a6a' FontFamily='segoe UI' FontSize='12'>%s</Span>"
			L"            </TextBlock>"
			L"        </StackPanel>"
			L""
			L"        <StackPanel Width='2' Background='#336699' Margin='10, 0, 10, 0' Height='40' VerticalAlignment='Top'/>"
			L""
			L"        <StackPanel Margin='0' VerticalAlignment='Top' Orientation='Horizontal'>"
			L"            <TextBlock Margin='0, 5, 0, 0'  FontFamily='Segoe UI' FontSize='12'>"
			L"                <Span Foreground='#444444' FontWeight='bold'>%s</Span>"
			L"                <LineBreak/>"
			L"                <Span Foreground='#6a6a6a'>%s</Span>"
			L"            </TextBlock>"
			L"        </StackPanel>"
			L""
			L"        <StackPanel Width='2' Background='#336699' Margin='10, 0, 10, 0' Height='40' VerticalAlignment='Top'/>"
			L""
			L"        <StackPanel Margin='0' VerticalAlignment='Top' Orientation='Horizontal'>"
			L"            <TextBlock Margin='0, 5, 0, 0' FontFamily='Segoe UI' FontSize='12'>"
			L"                <Span Foreground='#444444' FontWeight='bold'>%s</Span>"
			L"                <LineBreak/>"
			L"                <Span Foreground='#6a6a6a'>%s</Span>"
			L"            </TextBlock>"
			L"        </StackPanel>"
			L""
			L"        <StackPanel Width='2' Background='#336699' Margin='10, 0, 10, 0' Height='40' VerticalAlignment='Top'/>"
			L""
			L"        <StackPanel Margin='0' VerticalAlignment='Top' Orientation='Horizontal'>"
			L"            <TextBlock Margin='0, 5, 0, 0' FontFamily='Segoe UI' FontSize='12'>"
			L"                <Span Foreground='#444444' FontWeight='bold'>%s</Span>"
			L"                <LineBreak/>"
			L"                <Span Foreground='#6a6a6a'>%s</Span>"
			L"            </TextBlock>"
			L"        </StackPanel>"
			L""
			L"    </WrapPanel>"
			L"</DockPanel>"
			;

		if (selectedSession->IsFavorite())
		{
			static const CString favoriteIconString = Str::getStringFromNumber(IDB_SESSIONINFO_FAVORITE).c_str();

			markup.Format(selectedSessionMarkupBegin + selectedSessionMarkupFavoriteLine + selectedSessionMarkupEnd
				, favoriteIconString

				, sessionTypeIconString
				, sessionName.c_str()
				, creationDate

				, statusIconString
				, statusLabel
				, status

				, tenantLabel
				, tenantName

				, lastUsedLabel
				, lastUsedDate

				, accessCountLabel
				, accessCount);
		}
		else
		{
			markup.Format(selectedSessionMarkupBegin + selectedSessionMarkupEnd
				, sessionTypeIconString
				, sessionName.c_str()
				, creationDate

				, statusIconString
				, statusLabel
				, status

				, tenantLabel
				, tenantName

				, lastUsedLabel
				, lastUsedDate

				, accessCountLabel
				, accessCount);
		}
	}
	else
	{
		static const CString iconString = Str::getStringFromNumber(IDB_SESSIONINFO_SELECT).c_str();
		static const CString noSelectedSessionMarkup =
			L"<StackPanel Margin='0' Orientation='Horizontal' Background='#fafafa' VerticalAlignment='Top'>"
			L"	<StackPanel VerticalAlignment='Center' Orientation='Horizontal' Background='#fafafa' Margin='5'>"
			L"		<TextBlock VerticalAlignment='Center'><Image Source='res://#%s' VerticalAlignment='Center' Margin='10, 0, 0, 0' /></TextBlock>"
			L"		<TextBlock VerticalAlignment='Center' Margin='10, 0, 10, 0' Foreground='#262626' FontWeight='bold' FontSize='12'>%s</TextBlock>"
			L"	</StackPanel>"
			L"</StackPanel>"
			;
		static const CString text = YtriaTranslate::Do(BackstagePanelSessions_SelectionChanged_5, _YLOC("Select a session from the list to see its details.")).c_str();

		markup.Format(noSelectedSessionMarkup, iconString, text);
	}

	m_DetailedView.SetMarkupText(markup);
}
