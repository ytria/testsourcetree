#pragma once

#include "BaseO365Grid.h"

#include "BusinessList.h"
#include "Site.h"

class FrameLists;

class GridLists : public ModuleO365Grid<BusinessList>
{
public:
    GridLists();
    virtual void customizeGrid() override;

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildView(const O365DataMap<BusinessSite, vector<BusinessList>>& p_Lists, Origin p_Origin, bool p_FullPurge);

	void ShowSystemLists(const bool p_Show);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
    virtual BusinessList getBusinessObject(GridBackendRow* p_Row) const override;
	GridBackendRow* FillRow(const BusinessSite& p_Site, const BusinessList& p_List);
    virtual void InitializeCommands() override;

    void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart);
	O365SubIdsContainer GetSelectedListsIDs();

private:
    afx_msg void OnShowListItems();
	afx_msg void OnUpdateShowListItems(CCmdUI* pCmdUI);
    afx_msg void OnShowListColumns();
    afx_msg void OnUpdateShowListColumns(CCmdUI* pCmdUI);
	afx_msg void OnToggleSystemRowsVisibility();
	afx_msg void OnUpdateToggleSystemRowsVisibility(CCmdUI* pCmdUI);
    DECLARE_MESSAGE_MAP()

	O365IdsContainer GetSelectedIds() const;

private:
    GridBackendColumn* m_ColMetaId;
    GridBackendColumn* m_ColMetaDisplayName;

    GridBackendColumn* m_ColListName;
    GridBackendColumn* m_ColCreatedByUserName;
    GridBackendColumn* m_ColCreatedByUserId;
    GridBackendColumn* m_ColCreatedByUserEmail;
    GridBackendColumn* m_ColCreatedByApplicationName;
    GridBackendColumn* m_ColCreatedByApplicationId;
    GridBackendColumn* m_ColCreatedByDeviceName;
    GridBackendColumn* m_ColCreatedByDeviceId;
    GridBackendColumn* m_ColCreatedDateTime;
    GridBackendColumn* m_ColDescription;
    GridBackendColumn* m_ColLastModifiedByUserName;
    GridBackendColumn* m_ColLastModifiedByUserId;
    GridBackendColumn* m_ColLastModifiedByUserEmail;
    GridBackendColumn* m_ColLastModifiedByApplicationName;
    GridBackendColumn* m_ColLastModifiedByApplicationId;
    GridBackendColumn* m_ColLastModifiedByDeviceName;
    GridBackendColumn* m_ColLastModifiedByDeviceId;
    GridBackendColumn* m_ColLastModifiedDateTime;
    GridBackendColumn* m_ColWebUrl;
    GridBackendColumn* m_ColDisplayName;
    GridBackendColumn* m_ColListInfoContentTypesEnabled;
    GridBackendColumn* m_ColListInfoHidden;
    GridBackendColumn* m_ColListInfoTemplate;
    GridBackendColumn* m_ColEtag;
    GridBackendColumn* m_ColSystem;

	GridBackendColumn* m_ColParentDriveType;
	GridBackendColumn* m_ColParentDriveID;
	GridBackendColumn* m_ColParentName;
	GridBackendColumn* m_ColParentPath;
	GridBackendColumn* m_ColParentID;
	GridBackendColumn* m_ColParentShareID;
	GridBackendColumn* m_ColParentSharePointListID;
	GridBackendColumn* m_ColParentSharePointListItemID;
	GridBackendColumn* m_ColParentSharePointListItemUniqueID;
	GridBackendColumn* m_ColParentSharePointSiteURL;
	GridBackendColumn* m_ColParentSharePointSiteID;
	GridBackendColumn* m_ColParentSharePointWebID;

    FrameLists*	m_FrameLists;

	bool m_ShowSystemRows;
};
