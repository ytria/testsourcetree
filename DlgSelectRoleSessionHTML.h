#pragma once

#include "PersistentSession.h"
#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "YAdvancedHtmlView.h"

#include <memory>
#include "RoleDelegationUtil.h"

class ModuleBase;

class DlgSelectRoleSessionHTML	: public ResizableDialog
								, public YAdvancedHtmlView::IPostedDataTarget
{
public:
    enum class Action
    {
        LoadStandard,
        LoadRole
    };

	DlgSelectRoleSessionHTML(const vector<RoleDelegation>& p_Roles, CWnd* p_Parent);
    DlgSelectRoleSessionHTML(const vector<RoleDelegation>& p_Roles, CWnd* p_Parent, const bool p_isAddRole);

    bool HasAnyChoice() const;
    bool GetHideUnscoped() const;

    int64_t GetSelectedRole() const;
    Action GetSelectedAction() const;

	enum { IDD = IDD_DLG_DEFAULT_HTML };

	/* YAdvancedHtmlView::IPostedDataTarget */
    virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data) override;

protected:
    virtual BOOL OnInitDialogSpecificResizable() override;
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    DECLARE_MESSAGE_MAP()

    ENDDIALOG_FIXED(ResizableDialog)

	static const wstring g_StandardSessionBtn;
	static const wstring g_OptionSeeOnlyMineBtn;

private:
	void generateJSONScriptData(wstring& p_Output);
	// TODO - TODO - TODO - We need the Role of the current User here
	web::json::value getJSONRoleEntry(const RoleDelegation& p_Role);
	static UINT getHbsHeight(const wstring& hbsName);

private:
	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	static std::map<wstring, int, Str::keyLessInsensitive> g_HbsHeights;
	int m_totalHbsHeight;
	int m_MaxWidth;

    int64_t m_SelectedRoleId;
    Action m_Action;
	bool m_OptionHideUnscoped;

	bool m_IsAddRole;

    vector<RoleDelegation> m_Roles;

};

