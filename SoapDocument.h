#pragma once

#include "xercesc\dom\DOMElement.hpp"
#include "xercesc\parsers\XercesDOMParser.hpp"

namespace XERCES_CPP_NAMESPACE
{
    class DOMElement;
    class DOMDocumentFragment;
}

using XERCES_CPP_NAMESPACE::DOMDocumentFragment;
using XERCES_CPP_NAMESPACE::DOMElement;
using XERCES_CPP_NAMESPACE::DOMNode;
using XERCES_CPP_NAMESPACE::XercesDOMParser;

class IXmlSerializer;

namespace Ex
{
class SoapDocument
{
public:
    SoapDocument();
    ~SoapDocument();

    void AddToBody(IXmlSerializer& p_Serializer);
    void AddToBody(DOMElement* p_Element);

    XERCES_CPP_NAMESPACE::DOMDocument* GetDocument() const;
    DOMDocumentFragment* CreateFragment() const;

    static const wstring TypesNs;
    static const wstring MessagesNs;

private:
    struct DomElementDeleter
    {
        void operator()(DOMElement* p_Element) const
        {
            p_Element->release();
        }
    };

    struct XercesDOMParserDeleter
    {
        void operator()(XercesDOMParser* p_Parser) const
        {
            XERCES_CPP_NAMESPACE::XMLPlatformUtils::Terminate();
            delete p_Parser;
        }
    };

    XERCES_CPP_NAMESPACE::DOMDocument* m_Document = nullptr;
    DOMElement* m_Envelope = nullptr;
    DOMElement* m_Header = nullptr;
    DOMElement* m_Body = nullptr;

    static unique_ptr<XercesDOMParser, XercesDOMParserDeleter> g_SkeletonParser;
};
}

