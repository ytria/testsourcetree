#pragma once

#include "AppIdentity.h"
#include "UserIdentity.h"

class AuditActivityInitiator
{
public:
	boost::YOpt<AppIdentity>	m_App;
	boost::YOpt<UserIdentity>	m_User;
};

