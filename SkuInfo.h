#pragma once

#include "GridBackendField.h"

struct PlanInfo
{
    PlanInfo(PooledString p_Id)
        : Id(p_Id)
    {
        Enabled = false;
    }
    PooledString Id;
	mutable PooledString Name;
	mutable PooledString FriendlyName;
    mutable bool Enabled;
    mutable vector<GridBackendField> RowPk;

    bool operator<(const PlanInfo& p_Other) const
    {
        return Id < p_Other.Id;
    }
};

struct SkuInfo
{
    SkuInfo(PooledString p_Id)
        : Id(p_Id)
    {
        Consumed = 0;
        Total = 0;
        Enabled = false;
		OffMixMode = false;
    }
    PooledString Id;
    mutable PooledString PartNumber;
    mutable PooledString Name;
    mutable std::set<PlanInfo> Plans;
    mutable int32_t Consumed;
    mutable int32_t Total;
    mutable bool Enabled;
	mutable bool OffMixMode;
    mutable vector<GridBackendField> RowPk;

    bool operator<(const SkuInfo& p_Other) const
    {
        return Id < p_Other.Id;
    }
};
