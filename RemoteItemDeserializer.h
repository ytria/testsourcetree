#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "RemoteItem.h"

class RemoteItemDeserializer : public JsonObjectDeserializer, public Encapsulate<RemoteItem>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

