#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "RelatedContact.h"

class RelatedContactDeserializer : public JsonObjectDeserializer, public Encapsulate<RelatedContact>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

