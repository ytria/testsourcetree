#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "BasePermissions.h"

namespace Sp
{
    class BasePermissionsDeserializer : public JsonObjectDeserializer, public Encapsulate<Sp::BasePermissions>
    {
    protected:
        void DeserializeObject(const web::json::object& p_Object) override;
    };
}

