#pragma once

#include "AssignedLicense.h"
#include "BusinessObject.h"

class BusinessAssignedLicense : public BusinessObject
{
public:
	BusinessAssignedLicense();
	BusinessAssignedLicense(const AssignedLicense& p_AssignedLicense);

	const AssignedLicense& ToAssignedLicense() const;

	const boost::YOpt<vector<PooledString>>& GetDisabledPlans() const;
	void SetDisabledPlans(const boost::YOpt<vector<PooledString>>& p_DisabledPlans);

	const boost::YOpt<PooledString>& GetSkuPartNumber() const;
	void SetSkuPartNumber(const boost::YOpt<PooledString>& p_SkuPartNumber);

	// Same as BusinessObject::GetID()/BusinessObject::SetID()
	const PooledString& GetSkuId() const;
	void SetSkuId(const PooledString& p_SkuId);

private:
	mutable AssignedLicense m_AssignedLicense;

	boost::YOpt<PooledString> m_SkuPartNumber;

	friend class AssignedLicenseDeserializer;
    friend class AssignedLicenseNonPropDeserializer;
};