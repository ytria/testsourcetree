#pragma once

#include "Encapsulate.h"
#include "IDatedJsonDeserializer.h"
#include "IDeserializerFactory.h"

class ListDeserializerBase : public IDatedJsonDeserializer
{
public:
    virtual void Deserialize(const web::json::value& p_Json) override final
    {
        ASSERT(p_Json.is_array() || p_Json.is_null());
        if (p_Json.is_array())
            DeserializeList(p_Json.as_array());
    }

protected:
    virtual void DeserializeList(const web::json::array& p_Array) = 0;
};

class ListStringDeserializer : public ListDeserializerBase, public Encapsulate<vector<PooledString>>
{
protected:
    virtual void DeserializeList(const web::json::array& p_Array) override
    {
        m_Data.reserve(p_Array.size());
        for (const auto& str : p_Array)
        {
            ASSERT(str.is_string());
            if (str.is_string())
                m_Data.push_back(str.as_string());
        }
    }
};

// Use this list deserializer when Deserialize() is called only once
template <typename ObjType, typename DeserializerType>
class ListDeserializer : public ListDeserializerBase, public Encapsulate<vector<ObjType>>
{
protected:
    virtual void DeserializeList(const web::json::array& p_Array) override
    {
        m_Data.reserve(p_Array.size());
        for (const auto& obj : p_Array)
        {
            DeserializerType deserializer;
			deserializer.SetDate(GetDate());
            deserializer.Deserialize(obj);
            m_Data.push_back(std::move(deserializer.GetData()));
        }
    }
};

// Use this list deserializer when Deserialize() is called more than once (when there is pagination)
template <typename ObjType, typename DeserializerType>
class ListMultiPassDeserializer : public ListDeserializerBase
{
public:
    ListMultiPassDeserializer(IDeserializerFactory<DeserializerType>& p_DeserializerFactory, vector<ObjType>& p_Vector)
        : m_Vector(p_Vector)
		, m_DeserializerFactory(p_DeserializerFactory)
		, m_CutOffCondition([](const web::json::value&) { return false; })
		, m_FilterOutCondition([](const web::json::value&) { return false; })
		, m_IsCutOff(false)
    {}

	void SetCutOffCondition(const std::function<bool(const web::json::value&)>& p_Condition)
	{
		m_CutOffCondition = p_Condition;
	}

	void SetFilterOutCondition(const std::function<bool(const web::json::value&)>& p_Condition)
	{
		m_FilterOutCondition = p_Condition;
	}

	bool IsCutOff() const
	{
		return m_IsCutOff;
	}

	size_t GetObjectCount() const
	{
		return m_Vector.size();
	}

protected:
    void DeserializeList(const web::json::array& p_Array) override
    {
        m_Vector.reserve(m_Vector.size() + p_Array.size());

		for (auto itt = p_Array.begin(); itt != p_Array.end() && !m_IsCutOff; ++itt)
		{
			m_IsCutOff = m_CutOffCondition(*itt);
			if (!m_IsCutOff && !m_FilterOutCondition(*itt))
			{
				auto deserializer = m_DeserializerFactory.Create();
				deserializer.SetDate(GetDate());
				deserializer.Deserialize(*itt);
				m_Vector.push_back(std::move(deserializer.GetData()));
			}
		}
    }

private:
    vector<ObjType>& m_Vector;
    IDeserializerFactory<DeserializerType>& m_DeserializerFactory;

	std::function<bool(const web::json::value&)> m_CutOffCondition; // Return value: true = CutOff
	std::function<bool(const web::json::value&)> m_FilterOutCondition; // Return value: true = Filter out
	bool m_IsCutOff;
};