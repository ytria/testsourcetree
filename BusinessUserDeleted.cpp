#include "BusinessUserDeleted.h"

#include "BusinessGroup.h" // Doesn't compile without.

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessUserDeleted>("Deleted User") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Deleted User"))))
		.constructor()(policy::ctor::as_object)
	;
}
