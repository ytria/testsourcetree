#include "DbAccountListRequester.h"


#include "AzureSession.h"
#include "CosmosDbAccount.h"
#include "CosmosDbAccountDeserializer.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "ValueListDeserializer.h"
#include "BasicHttpRequestLogger.h"

Azure::DbAccountListRequester::DbAccountListRequester(const wstring& p_SubscriptionId, const wstring& p_ResGroup)
	:m_SubscriptionId(p_SubscriptionId),
	m_ResGroup(p_ResGroup)
{
}

TaskWrapper<void> Azure::DbAccountListRequester::Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<Azure::CosmosDbAccount, Azure::CosmosDbAccountDeserializer>>();
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri(_YTEXT("subscriptions"));
	uri.append_path(m_SubscriptionId);
	uri.append_path(_YTEXT("resourceGroups"));
	uri.append_path(m_ResGroup);
	uri.append_path(_YTEXT("providers/Microsoft.DocumentDB/databaseAccounts"));
	uri.append_query(AzureSession::g_ApiVersionKey, _YTEXT("2015-04-08"));

	return p_Session->GetAzureSession()->Get(uri.to_uri(), std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier()), m_Result, m_Deserializer, p_TaskData);
}

const vector<Azure::CosmosDbAccount>& Azure::DbAccountListRequester::GetData() const
{
	return m_Deserializer->GetData();
}
