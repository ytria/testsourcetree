#include "O365SQLiteEngine.h"

#include "MainFrameSapio365Session.h"
#include "MFCUtil.h"
#include "Office365Admin.h"
#include "Sapio365Session.h"
#include "SQLCosmosSyncManager.h"

O365Krypter::O365Krypter(std::shared_ptr<const Sapio365Session> p_Session) :
	m_Session(p_Session)
{
}

O365Krypter::O365Krypter(std::shared_ptr<Sapio365Session> p_Session)
	: O365Krypter((std::shared_ptr<const Sapio365Session>) p_Session)
{
}

void O365Krypter::SetDumpDebugKey(const std::string& p_EncryptionKey) const
{
	m_DumpDebugKey = p_EncryptionKey;
}

std::string O365Krypter::getKrypt() const
{
	ASSERT(m_Session);
	if (m_Krypt.empty() && m_Session)
	{
		auto org = m_Session->GetGraphCache().GetCachedOrganization();
		ASSERT(org.is_initialized());
		if (org.is_initialized())
		{
			// tossin' salad
			std::string orgID = m_DumpDebugKey.empty() ? MFCUtil::convertUNICODE_to_ASCII(org.get().m_Id.c_str()) : m_DumpDebugKey;
			std::string k2;
			size_t k = 1;
			for (auto c : orgID)
			{
				k2.push_back(c);
				k2.push_back((c + ((c - 1) % 5)) % 128);
				k2.push_back((orgID.at(orgID.size() - k) + ((c + 666) % 69)) % 128);
				k++;
			}

			m_Krypt = k2;
			k = k2.size();
			for (auto it = k2.rbegin(); it != k2.rend(); it++)
			{
				auto c = *it;
				auto z = ((c + k) % 2 == 0 ? c + 11 : c + 5) % 128;
				m_Krypt[(999 * z) % m_Krypt.size()] = z;
				k += c;
			}
		}
	}

	ASSERT(!m_Krypt.empty());
	return m_Krypt;
}

void O365Krypter::setSession(std::shared_ptr<Sapio365Session> p_Session)
{
	ASSERT(!m_Session);
	m_Session = p_Session;
}

const std::shared_ptr<const Sapio365Session> O365Krypter::GetSession() const
{
	return m_Session;
}

// =================================================================================================

O365MainFrameKrypter::O365MainFrameKrypter()
	: O365Krypter(std::shared_ptr<const Sapio365Session>())
{
	std::shared_ptr<Sapio365Session> session;

	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());
	ASSERT(nullptr != app);
	if (nullptr != app && nullptr != app->GetSapioSessionBeingLoaded())
		session = app->GetSapioSessionBeingLoaded();
	else
		session = MainFrameSapio365Session();

	// ASSERT(session); // no assert, can be nullptr
	setSession(session);
}

// =================================================================================================

O365SQLiteEngine::O365SQLiteEngine(const wstring& p_DBfile) : SQLiteEngine(getLocalAppDataDBFileFolder() + p_DBfile)
{
}

O365SQLiteEngine::O365SQLiteEngine(const wstring& p_DBfile, const std::string& p_EncryptionKeySQLite, const std::string& p_EncryptionKeyYtria) : 
	SQLiteEngine(getLocalAppDataDBFileFolder() + p_DBfile, p_EncryptionKeySQLite, p_EncryptionKeyYtria)
{
}

void O365SQLiteEngine::SetCloudSyncDate(std::shared_ptr<const Sapio365Session> p_Session)
{
	auto syncDate = YTimeDate::GetCurrentTimeDateGMT();
	for (const auto& t : GetTables())
		SQLCosmosSyncManager::GetInstance().SetCloudSyncDate(GetDBfilename(), t.GetSQLcompliantName(), p_Session, syncDate);
}

const SQLiteUtil::WhereClause O365SQLiteEngine::GetSelectDefaultWhereClause(const IKrypter& p_Krypter, const SQLiteUtil::Table& p_Table) const
{
	SQLiteUtil::WhereClause defaultWhereClause;

	auto tenantColumn = p_Table.GetColumnOrganization();
	if (!tenantColumn.IsDummy())
	{
		const auto k = dynamic_cast<const O365Krypter*>(&p_Krypter);
		ASSERT(nullptr != k);
		if (nullptr != k && k->GetSession())
		{
			defaultWhereClause.m_Column = tenantColumn;
			defaultWhereClause.m_Operator = SQLiteUtil::Operator::Equal;
			defaultWhereClause.m_Values.push_back(k->GetSession()->GetTenantName(true));
		}
	}

	return defaultWhereClause;
}