#pragma once

#include "IRequester.h"

class SingleRequestResult;

namespace Azure
{
	class DeleteDbAccountRequester : public IRequester
	{
	public:
		DeleteDbAccountRequester(const wstring& p_SubscriptionId, const wstring& p_ResourceGroup, const wstring& p_DbAccountName);
		TaskWrapper<void> Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	private:
		std::shared_ptr<SingleRequestResult> m_Result;

		wstring m_SubscriptionId;
		wstring m_ResourceGroup;
		wstring m_DbAccountName;
	};
}
