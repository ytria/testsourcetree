#include "SessionMigrationsProvisioner.h"
#include "FileUtil.h"
#include "MigrationRegistrations.h"
#include "MigrationUtil.h"

SessionMigrationsProvisioner::SessionMigrationsProvisioner(MigrationRegistrations& p_Registrations)
	: m_Registrations(p_Registrations)
{
}

void SessionMigrationsProvisioner::Provision()
{
	ASSERT(MigrationUtil::GetCurrentMigrationVersion() > 0);

	int sourceVersion = MigrationUtil::GetCurrentMigrationVersion();
	while (sourceVersion > 0 && !FileUtil::FileExists(MigrationUtil::GetMigrationFilePath(sourceVersion)))
		--sourceVersion;
	sourceVersion = (std::max)(0, sourceVersion);

	for (auto targetVersion = sourceVersion + 1; targetVersion <= MigrationUtil::GetCurrentMigrationVersion(); ++targetVersion)
		m_Migrations.push_back(m_Registrations.GetVersionMigration(targetVersion));
}
