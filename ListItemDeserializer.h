#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "BusinessListItem.h"

class ListItemDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessListItem>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

