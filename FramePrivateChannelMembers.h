#pragma once

#include "GridFrameBase.h"
#include "GridPrivateChannelMembers.h"

class FramePrivateChannelMembers : public GridFrameBase
{
public:
	FramePrivateChannelMembers(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

	DECLARE_DYNAMIC(FramePrivateChannelMembers)
	virtual ~FramePrivateChannelMembers() = default;

	void ShowPrivateChannelMembers(const vector<BusinessChannel>& p_Channels, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_FullPurge);

	virtual O365Grid& GetGrid() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction);
	virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

	const std::map<PooledString, PooledString>& GetChannelNames() const;
	void SetChannelNames(const map<PooledString, PooledString>& p_ChannelNames);

protected:
	virtual void createGrid() override;

	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;
	virtual O365ControlConfig GetApplyPartialControlConfig() override;

	virtual void ApplyAllSpecific() override;
	virtual void ApplySelectedSpecific() override;

	void apply(bool p_Selected);

private:
	GridPrivateChannelMembers m_GridPrivateChannelMembers;
	std::map<PooledString, PooledString> m_ChannelNames;
};
