#include "ClassTeachersListRequester.h"
#include "EducationTeacher.h"
#include "EducationTeacherDeserializer.h"
#include "LoggerService.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"

ClassTeachersListRequester::ClassTeachersListRequester(const wstring& p_ClassId)
	:m_ClassId(p_ClassId)
{
}

pplx::task<void> ClassTeachersListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<EducationTeacher, EducationTeacherDeserializer>>();

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("classes"));
	uri.append_path(m_ClassId);
	uri.append_path(_YTEXT("teachers"));

	LoggerService::Friendly(_YFORMAT(L"Loading teachers for class with id \"%s\"", m_ClassId.c_str()), p_TaskData.GetOriginator());
	return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Deserializer, _T("teachers")).Paginate(p_Session->GetMSGraphSession(), p_TaskData);
}

const vector<EducationTeacher>& ClassTeachersListRequester::GetData() const
{
	return m_Deserializer->GetData();
}
