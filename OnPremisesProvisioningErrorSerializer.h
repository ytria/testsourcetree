#pragma once

#include "IJsonSerializer.h"
#include "OnPremisesProvisioningError.h"

class OnPremisesProvisioningErrorSerializer : public IJsonSerializer
{
public:
    OnPremisesProvisioningErrorSerializer(const OnPremisesProvisioningError& p_OnPremisesProvisioningError);
    void Serialize() override;

private:
    const OnPremisesProvisioningError& m_OnPremProvErr;
};

