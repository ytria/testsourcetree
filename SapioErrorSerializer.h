#pragma once

#include "IJsonSerializer.h"
#include "SapioError.h"

class SapioErrorSerializer : public IJsonSerializer
{
public:
	SapioErrorSerializer(const SapioError& p_SapioError);
	void Serialize() override;

private:
	const SapioError& m_SapioError;
};

