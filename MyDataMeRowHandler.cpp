#include "MyDataMeRowHandler.h"

#include "CacheGrid.h"
#include "GridBackendRow.h"

void MyDataMeRowHandler::GetRidOfMeRow(CacheGrid& p_Grid, GridBackendRow* p_Row)
{
	m_Fields = p_Row->GetFields();
	// WARNING: GetChildRows(parentRow); doesn't work, because it relies on parents being added before children, which is false on update. 
	for (auto& row : p_Grid.GetBackendRows())
	{
		if (row->GetParentRow() == p_Row)
			row->SetParentRow(nullptr);
	}
	p_Grid.DeleteHierarchyObjectTypeFromListToFilterOn(p_Grid.GetRowObjectTypeIndex(p_Row));
	p_Grid.RemoveRowFromProcesses(p_Row);
	p_Grid.RemoveRow(p_Row);
}

GridBackendField& MyDataMeRowHandler::GetField(UINT p_ColID)
{
	return m_Fields.find(p_ColID) == m_Fields.end() ? GridBackendField::g_GenericField : m_Fields.at(p_ColID);
}

GridBackendField& MyDataMeRowHandler::GetField(GridBackendColumn* p_Col)
{
	if (nullptr != p_Col)
		return GetField(p_Col->GetID());
	return GridBackendField::g_GenericField;
}

const GridBackendField& MyDataMeRowHandler::GetField(GridBackendColumn* p_Col) const
{
	if (nullptr != p_Col)
		return GetField(p_Col->GetID());
	return GridBackendField::g_GenericField;
}

const GridBackendField& MyDataMeRowHandler::GetField(UINT p_ColID) const
{
	return m_Fields.find(p_ColID) == m_Fields.end() ? GridBackendField::g_GenericField : m_Fields.at(p_ColID);
}

bool MyDataMeRowHandler::HasField(UINT p_ColID) const
{
	return m_Fields.find(p_ColID) != m_Fields.end()
		&& m_Fields.at(p_ColID).HasValue();
}

bool MyDataMeRowHandler::HasField(GridBackendColumn* p_Col) const
{
	return nullptr != p_Col && HasField(p_Col->GetID());
}
