#include "DlgUserPasswordHTML.h"

#include "AutomationNames.h"
#include "MsGraphFieldNames.h"

DlgUserPasswordHTML::DlgUserPasswordHTML(vector<BusinessUser>& p_Users, CWnd* p_Parent)
	: BaseDlgBusinessUserEditHTML(p_Users, DlgFormsHTML::Action::EDIT, p_Parent, g_ActionNameSelectedResetPassword)
	, m_AutoGeneratePasswords(false)
{
}

DlgUserPasswordHTML::~DlgUserPasswordHTML()
{
}

wstring DlgUserPasswordHTML::getDialogTitle() const
{
	return YtriaTranslate::Do(DlgUserPasswordHTML_getDialogTitle_1, _YLOC("Reset Password")).c_str();
}

void DlgUserPasswordHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgUserPasswordHTML"));

	ASSERT(isEditDialog());
	if (isEditDialog())
	{
		bool allTheSamePwd = true, allTheSameFC = true, allTheSameFCMFA = true;
		const auto valPwd = !m_BusinessUsers.front().GetPassword() || m_BusinessUsers.front().GetPassword()->IsEmpty();
		const auto valFC = m_BusinessUsers.front().GetForceChangePassword();
		const auto valFCMFA = m_BusinessUsers.front().GetForceChangePasswordWithMfa();
		for (const auto& object : m_BusinessUsers)
		{
			{
				const auto newVal = !object.GetPassword() || object.GetPassword()->IsEmpty();
				if (valPwd != newVal)
					allTheSamePwd = false;
			}
			{
				const auto newValFC = object.GetForceChangePassword();
				if (valFC != newValFC)
					allTheSameFC = false;
			}
			{
				const auto newValFCMFA = object.GetForceChangePasswordWithMfa();
				if (valFCMFA != newValFCMFA)
					allTheSameFCMFA = false;
			}
		}

		getGenerator().addCategory(_T("New Password"), CategoryFlags::EXPAND_BY_DEFAULT);
		getGenerator().Add(BoolToggleEditor({ { g_ParamNameCreatePassword, _T("Create new password"), EditorFlags::DIRECT_EDIT, true } }));
		{
			uint32_t flags = EditorFlags::DIRECT_EDIT;

			bool value = true;
			if (allTheSamePwd)
				value = valPwd;
			else
				flags = EditorFlags::NOCHANGE;

			getGenerator().Add(BoolToggleEditor({ { g_ParamNameAutoGenerate, YtriaTranslate::Do(DlgUserPasswordHTML_generateJSONScriptData_1, _YLOC("Auto-generate")).c_str(), flags, value } }));
		}

		const auto directEditPwd = m_BusinessUsers.size() == 1 ? EditorFlags::DIRECT_EDIT : 0;
		addStringEditor(&BusinessUser::GetPassword, &BusinessUser::SetPassword, g_ParamNamePassword, YtriaTranslate::Do(DlgUserPasswordHTML_generateJSONScriptData_2, _YLOC("Password")).c_str(), EditorFlags::REQUIRED | directEditPwd, {});

		getGenerator().addCategory(_T("Enforce one-time password update on next login"), CategoryFlags::EXPAND_BY_DEFAULT);
		{
			{
				uint32_t flags = valFC ? EditorFlags::DIRECT_EDIT : 0;

				bool value = true;
				if (allTheSameFC)
					value = valFC && *valFC;
				else
					flags = EditorFlags::NOCHANGE;

				getGenerator().Add(BoolToggleEditor({ { g_ParamNameForceChange, YtriaTranslate::Do(DlgUserPasswordHTML_generateJSONScriptData_3, _YLOC("Force change on next login")).c_str(), flags, value } }));
			}

			{
				uint32_t flags = valFCMFA ? EditorFlags::DIRECT_EDIT : 0;

				bool value = true;
				if (allTheSameFCMFA)
					value = valFCMFA && *valFCMFA;
				else
					flags = EditorFlags::NOCHANGE;

				getGenerator().Add(BoolToggleEditor({ { g_ParamNameForceChangeMFA, _T("Force change with MFA on next login"), flags, value } }));
			}
			//addBoolEditor(&BusinessUser::GetForceChangePassword, &BusinessUser::SetForceChangePassword, g_ParamNameForceChange, YtriaTranslate::Do(DlgUserPasswordHTML_generateJSONScriptData_3, _YLOC("Force change on next login")).c_str(), 0, {});
			//addBoolEditor(&BusinessUser::GetForceChangePasswordWithMfa, &BusinessUser::SetForceChangePasswordWithMfa, g_ParamNameForceChangeMFA, _T("Force change with MFA on next login"), 0, {});
		}

		getGenerator().addApplyButton(LocalizedStrings::g_ApplyButtonText);
		getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);

		// Disable autogenerate and password field if no create password
		getGenerator().addEvent(g_ParamNameCreatePassword, EventTarget({ g_ParamNameAutoGenerate, EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueFalse }));
		getGenerator().addEvent(g_ParamNameCreatePassword, EventTarget({ g_ParamNamePassword, EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueFalse }));

		// Disable password field if autogenerate
		getGenerator().addEvent(g_ParamNameAutoGenerate, EventTarget({ g_ParamNamePassword, EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueTrue }));

		// "Force change with MFA" only works if "Force change is set"
		getGenerator().addEvent(g_ParamNameForceChange, EventTarget({ g_ParamNameForceChangeMFA, EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueFalse }));

		getGenerator().addGlobalEventTrigger(g_ParamNameAutoGenerate); // Bug #200217.RL.00B566: one case of  Password is enabled while Auto-generate is set to True at reset password in users
	}
}

bool DlgUserPasswordHTML::processPostedDataSpecific(const IPostedDataTarget::PostedData::value_type& p_Value)
{
	if (g_ParamNameAutoGenerate == p_Value.first)
	{
		m_AutoGeneratePasswords = Str::getBoolFromString(p_Value.second);
		return true;
	}

	if (g_ParamNameForceChange == p_Value.first)
	{
		auto val = Str::getBoolFromString(p_Value.second);
		for (auto& businessUser : m_BusinessUsers)
		{
			businessUser.SetForceChangePassword(val);
			if (!val) // This only works because g_ParamNameForceChange is alphabetically before g_ParamNameForceChangeMFA
				businessUser.SetForceChangePasswordWithMfa(val);
		}
		return true;
	}

	if (g_ParamNameForceChangeMFA == p_Value.first)
	{
		auto val = Str::getBoolFromString(p_Value.second);
		for (auto& businessUser : m_BusinessUsers)
			businessUser.SetForceChangePasswordWithMfa(val);
		return true;
	}

	if (g_ParamNameCreatePassword == p_Value.first)
		return true;

	ASSERT(false);
	return false;
}

void DlgUserPasswordHTML::postProcessPostedData()
{
	// Nothing to do.
}

bool DlgUserPasswordHTML::ShouldAutogeneratePasswords() const
{
	return m_AutoGeneratePasswords;
}