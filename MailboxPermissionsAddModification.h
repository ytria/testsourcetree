#pragma once

#include "ModificationWithRequestFeedback.h"
#include "GridUtil.h"

class GridMailboxPermissions;

// User manual:
// 1- Create the row using the CreateRow static method
// 2- Call the constructor using the returned row's pk

class MailboxPermissionsAddModification : public ModificationWithRequestFeedback
{
public:
	MailboxPermissionsAddModification(GridMailboxPermissions& p_Grid, const row_pk_t& p_Row, const wstring& p_UserId);

	void Apply() override;
	void Revert() override;
	vector<ModificationLog> GetModificationLogs() const override;

	void SetPermissions(const vector<wstring>& p_Permissions);

	const vector<wstring>& GetPermissions() const;
	const wstring& GetMailboxOwner() const;
	const wstring& GetUserId() const;

	static row_pk_t CreateRow(GridMailboxPermissions& p_Grid, GridBackendRow& p_OwnerRow, const wstring& p_PermissionsUserId, const wstring& p_InheritanceType, const vector<wstring>& p_Rights);

private:
	wstring m_UserId;
	vector<wstring> m_Permissions;
	wstring m_MailboxOwner;

	wstring m_PermissionsUserId;
	wstring m_InheritanceType;
	row_pk_t m_MailboxOwnerRowPk;

	std::reference_wrapper<GridMailboxPermissions> m_Grid;

	static bool SimilarRowExists(GridMailboxPermissions& p_Grid, const row_pk_t& p_Pk, GridBackendRow& p_UserRow);
};

