#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Attachment.h"

class AttachmentDeserializer : public JsonObjectDeserializer, public Encapsulate<Attachment>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

