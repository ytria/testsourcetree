#include "ChoiceColumnDeserializer.h"

#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"

void ChoiceColumnDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeBool(_YTEXT("allowTextEntry"), m_Data.AllowTextEntry, p_Object);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("choices"), p_Object))
            m_Data.Choices = std::move(lsd.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("displayAs"), m_Data.DisplayAs, p_Object);
}
