#pragma once

#include "IJsonSerializer.h"

#include "BusinessSite.h"

class DbSiteSerializer : public IJsonSerializer
{
public:
    DbSiteSerializer(const BusinessSite& p_Site);
    void Serialize() override;

private:
    const BusinessSite& m_Site;
};
