#pragma once

#include "IRequester.h"
#include "AddDelegate.h"

class SingleRequestResult;

namespace Ex
{
class DelegatesAdder : public IRequester
{
public:
    void AddDelegate(const Ex::AddDelegate& p_AddDelegate);

    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

private:
    //std::shared_ptr<AddDelegatesResponseDeserializer> m_Deserializer;
    std::shared_ptr<SingleRequestResult> m_Result;

    vector<Ex::AddDelegate> m_AddDelegates;
};
}

