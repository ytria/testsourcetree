#pragma once

#include "IJsonSerializer.h"

class MailboxSettingsSerializer : public IJsonSerializer
{
public:
    MailboxSettingsSerializer(const MailboxSettings& p_MailboxSettings);
    void Serialize() override;

private:
    const MailboxSettings& m_MailboxSettings;
};

