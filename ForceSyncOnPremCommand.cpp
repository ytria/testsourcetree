#include "ForceSyncOnPremCommand.h"
#include "AutomationNames.h"
#include "FrameUsers.h"
#include "OnPremiseExecScriptRequester.h"
#include "SimpleMessageRequestLogger.h"
#include "Sapio365Settings.h"

ForceSyncOnPremCommand::ForceSyncOnPremCommand(GridFrameBase& p_Frame)
	: IActionCommand(g_ActionNameShowOnPremUsers)
	, m_Frame(p_Frame)
{
}

void ForceSyncOnPremCommand::ExecuteImpl() const
{
	ASSERT(m_Setup);
	if (!m_Setup)
		return;

	if (!Office365AdminApp::CheckAndWarn<LicenseTag::OnPremise>(&m_Frame))
		return;

	auto setup = *m_Setup;
	auto hwnd = m_Frame.GetSafeHwnd();
	if (!ModuleUtil::WarnIfPowerShellHostIncompatible(CWnd::FromHandle(hwnd)))
		return;

	auto taskData = Util::AddFrameTask(_T("Force sync cycle"), &m_Frame, setup, m_Frame.GetLicenseContext(), false);

	YSafeCreateTask([bom = m_Frame.GetBusinessObjectManager(), hwnd, taskData]()
	{
		auto logger = std::make_shared<SimpleMessageRequestLogger>(_T("Force sync cycle"));

		auto initResult = bom->GetSapio365Session()->InitBasicPowerShell(taskData.GetOriginator());
		wstring initErrors = GetPSErrorString(initResult);

		wstring error;
		if (initErrors.empty())
		{
			wstring script = _YTEXTFORMAT(LR"(
				Invoke-Command -ComputerName "%s" -ScriptBlock {Import-Module ADSync;Start-ADSyncSyncCycle -PolicyType Delta}
			)", bom->GetGraphCache().GetAADComputerName().c_str());
			auto requester = std::make_shared<OnPremiseExecScriptRequester>(script, logger);
			requester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();

			error = GetPSErrorString(requester->GetResult());
		}
		else
			error = initErrors;

		YCallbackMessage::DoPost([hwnd, taskData, error]()
		{
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<GridFrameBase*>(CWnd::FromHandle(hwnd)) : nullptr;
			ASSERT(nullptr != frame);
			if (nullptr != frame)
				frame->GetGrid().ClearLog(taskData.GetId());

			frame->TaskFinished(taskData, nullptr, hwnd);

			if (!error.empty())
			{
				YCodeJockMessageBox(frame
					, DlgMessageBox::eIcon_Error
					, _T("Error")
					, _T("Could not Force Sync.")
					, error
					, { {IDOK, _T("Close")} }).DoModal();
			}
		});
	});
}
