#include "QueryCreateRole.h"
#include "SessionsSqlEngine.h"
#include "SqlQueryPreparedStatement.h"

QueryCreateRole::QueryCreateRole(int64_t p_RoleId, const wstring& p_Name, SessionsSqlEngine& p_Engine)
	: m_Engine(p_Engine),
	m_RoleId(p_RoleId),
	m_Name(p_Name)
{
}

void QueryCreateRole::Run()
{
	wstring queryStr = _YTEXT(R"(INSERT INTO Roles(Id,Name) VALUES (?, ?))");
	SqlQueryPreparedStatement query(m_Engine, queryStr);
	query.BindInt64(1, m_RoleId);
	query.BindString(2, m_Name);
	query.Run();

	m_Status = query.GetStatus();
}
