#include "PaginatorUtil.h"

#include "GraphPageRequester.h"
#include "MultiObjectsPageRequestLogger.h"
#include "ValueListDeserializer.h"
#include "MSGraphSession.h"
#include "YtriaTaskData.h"
#include "MsGraphPageRequesterRetryer.h"

std::shared_ptr<IMsGraphPageRequester> Util::CreateDefaultGraphPageRequester(const shared_ptr<IValueListDeserializer>& p_Deserializer, const std::shared_ptr<IHttpRequestLogger>& p_HttpLogger, HWND p_Hwnd)
{
	auto graphPageRequester = std::make_shared<GraphPageRequester>(p_Deserializer, nullptr, p_HttpLogger);
	return std::make_shared<MsGraphPageRequesterRetryer>(graphPageRequester, MsGraphPageRequesterRetryer::DefaultAskIfRetry(), p_Hwnd);
}

MsGraphPaginator Util::CreateDefaultGraphPaginator(const web::uri& p_Uri, const shared_ptr<IValueListDeserializer>& p_Deserializer, size_t p_ElemsPerPage, const std::shared_ptr<IPageRequestLogger>& p_Logger, const std::shared_ptr<IHttpRequestLogger>& p_HttpLogger, HWND p_Hwnd)
{
	auto pagRequester = CreateDefaultGraphPageRequester(p_Deserializer, p_HttpLogger, p_Hwnd);
	ASSERT(p_Logger); // Logger is mandatory. If you don't want to log, create a "NullLogger"
	if (p_Logger)
		return MsGraphPaginator(p_Uri, pagRequester, p_Logger, p_ElemsPerPage);
	throw std::exception("No logger found");
}

MsGraphPaginator Util::CreateDefaultGraphPaginator(const web::uri& p_Uri, const shared_ptr<IValueListDeserializer>& p_Deserializer, const std::shared_ptr<IPageRequestLogger>& p_Logger, const std::shared_ptr<IHttpRequestLogger>& p_HttpLogger, HWND p_Hwnd)
{
	auto pagRequester = CreateDefaultGraphPageRequester(p_Deserializer, p_HttpLogger, p_Hwnd);
	ASSERT(p_Logger); // Logger is mandatory. If you don't want to log, create a "NullLogger"
	if (p_Logger)
		return MsGraphPaginator(p_Uri, pagRequester, p_Logger, MsGraphPaginator::g_DefaultPageSize);
	throw std::exception("No logger found");
}

