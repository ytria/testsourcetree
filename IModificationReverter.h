#pragma once

class IModificationReverter
{
public:
	virtual ~IModificationReverter() = default;

	virtual void RevertAll() = 0;
	virtual void RevertSelected() = 0;
};

