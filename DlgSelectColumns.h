#pragma once

#include "GridCrust.h"
#include "ResizableDialog.h"

class DlgSelectColumns	: public ResizableDialog
						, public GridCrust
						, public GridDoubleClickObserver
{
public:
	static const wstring g_AutomationName;

public:
	DlgSelectColumns(const vector<GridBackendColumn*>& p_SelectedColumns, std::function<bool(GridBackendColumn*)> p_ColumnFilter, CacheGrid* p_ParentGrid);
	virtual ~DlgSelectColumns() override;

	const vector<GridBackendColumn*>& GetSelectedColumns() const;

	void SetTitle(const wstring& p_Title);
	void SetIntroText(const wstring& p_IntroText);
	void SetCancelButtonText(const wstring& p_CancelText);
	void SetOkButtonText(const wstring& p_OkText);

	void SetHideCancelButton(bool p_Hide);

	void SetVerificationText(const wstring& p_VerificationText, bool p_DefaultState, bool p_CheckOnFirstAdditionIfEmpty);
	bool IsVerificationChecked() const;

	void SetEnableOkIfNoSelection(bool p_EnableIfEmpty);

public: // GridCrust
	virtual CacheGrid* GetMotherGrid() const override;
	virtual void UpdateFromGrid(CacheGrid* i_FromGrid = nullptr) override;

public: // GridDoubleClickObserver
	virtual void DoubleClicked(const CacheGrid& grid) override;

protected:
	virtual void DoDataExchange(CDataExchange* pDX) override;
	virtual BOOL OnInitDialogSpecificResizable() override;
	virtual afx_msg void OnOK() override;
	afx_msg void OnAdd();
	afx_msg void OnRemove();
	afx_msg void OnCheck();
	afx_msg void OnFilterClear();
	afx_msg void OnFilterChange();

	DECLARE_MESSAGE_MAP()

	virtual void		setupClearSpecific() override;
	virtual const bool	setupLoadSpecific(AutomationAction* i_settingsAction, list < wstring >& o_Errors) override;

private:
	void updateOkButton();

private:
	wstring m_Title;
	wstring m_IntroText;
	wstring m_VerificationText;
	wstring m_CustomCancelText;
	wstring m_CustomOkText;
	wstring m_ResetText;
	bool m_EnableIfEmpty;
	bool m_HideCancel;
	bool m_Check;
	bool m_ShouldCheckOnFirstAdditionIfEmpty;

	CacheGrid*		m_MotherGrid;
	ColumnSelectionGrid	m_GridSelectedColumns;
	CXTPButton		m_BtnOK;
	CXTPButton		m_BtnCancel;
	CExtGroupBox	m_GroupBoxSrc;
	CExtGroupBox	m_GroupBoxDest;
	CExtIconButton	m_BtnAdd;
	CExtIconButton	m_BtnRemove;
	CExtLabel		m_Text;
	CExtCheckBox	m_Checkbox;
	CExtEdit		m_FilterEdit;
	CXTPButton		m_FilterClear;

	vector<GridBackendColumn*> m_SelectedColumns;
};
