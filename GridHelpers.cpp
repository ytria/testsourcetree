#include "GridHelpers.h"

#include "GridBackendRow.h"

std::pair<vector<wstring>, vector<wstring>> GridHelpers::ExtractEmailAddresses(const vector<Recipient>& p_Recipients)
{
    vector<wstring> addresses;
    vector<wstring> names;
    for (const auto& recipient : p_Recipients)
    {
        if (recipient.m_EmailAddress)
        {
            if (recipient.m_EmailAddress->m_Address)
                addresses.push_back(*recipient.m_EmailAddress->m_Address);
            else
                addresses.push_back(GridBackendUtil::g_NoValueString);
            if (recipient.m_EmailAddress->m_Name)
                names.push_back(*recipient.m_EmailAddress->m_Name);
            else
                names.push_back(GridBackendUtil::g_NoValueString);
        }
    }

    return std::make_pair(addresses, names);
}

vector<wstring> GridHelpers::ExtractEmailAddressesToString(const vector<Recipient>& p_Recipients)
{
	vector<wstring> strings;
	for (const auto& recipient : p_Recipients)
	{
		if (recipient.m_EmailAddress)
			strings.push_back(recipient.ToString());
	}
	return strings;
}

vector<Recipient> GridHelpers::ExtractRecipients(const vector<PooledString>& p_Addresses, const vector<PooledString>& p_Names)
{
	vector<Recipient> recipients;

	ASSERT(p_Addresses.size() == p_Names.size());
	const auto size = min(p_Addresses.size(), p_Names.size());

	recipients.reserve(size);
	for (size_t i = 0; i < size; ++i)
		recipients.emplace_back(ExtractRecipient(p_Addresses[i], p_Names[i]));

	return recipients;
}

Recipient GridHelpers::ExtractRecipient(const PooledString& p_Address, const PooledString& p_Name)
{
	Recipient recipient;
	recipient.m_EmailAddress.emplace();
	recipient.m_EmailAddress->m_Address.emplace(p_Address);
	recipient.m_EmailAddress->m_Name.emplace(p_Name);
	return recipient;
}

vector<Recipient> GridHelpers::ExtractRecipients(GridBackendRow* p_Row, GridBackendColumn* p_ColAddresses, GridBackendColumn* p_ColNames)
{
	vector<Recipient> recipients;

	vector<PooledString>* addresses = nullptr;
	vector<PooledString>* names = nullptr;
	vector<PooledString> temp1;
	vector<PooledString> temp2;
	{
		const auto& field = p_Row->GetField(p_ColAddresses);
		if (field.HasValue())
		{
			if (field.IsMulti())
			{
				addresses = field.GetValuesMulti<PooledString>();
			}
			else
			{
				temp1.push_back(field.GetValueStr());
				addresses = &temp1;
			}
		}
	}
	{
		const auto& field = p_Row->GetField(p_ColNames);
		if (field.HasValue())
		{
			if (field.IsMulti())
			{
				names = field.GetValuesMulti<PooledString>();
			}
			else
			{
				temp2.push_back(field.GetValueStr());
				names = &temp2;
			}
		}
	}

	if (nullptr != addresses && nullptr != names)
		recipients = GridHelpers::ExtractRecipients(*addresses, *names);

	return recipients;
}

std::pair<vector<wstring>, vector<wstring>> GridHelpers::ExtractHeaders(const vector<InternetMessageHeader>& p_Headers)
{
	vector<wstring> names;
	vector<wstring> values;
	for (const auto& h : p_Headers)
	{
		if (h.m_Name)
			names.push_back(*h.m_Name);
		else
			names.push_back(GridBackendUtil::g_NoValueString);
		if (h.m_Value)
			values.push_back(*h.m_Value);
		else
			values.push_back(GridBackendUtil::g_NoValueString);
	}

	return std::make_pair(names, values);
}

vector<InternetMessageHeader> GridHelpers::ExtractInternetMessageHeaders(GridBackendRow* p_Row, GridBackendColumn* p_ColNames, GridBackendColumn* p_ColValues)
{
	vector<InternetMessageHeader> messageHeaders;

	vector<PooledString>* names = nullptr;
	vector<PooledString>* values = nullptr;
	vector<PooledString> temp1;
	vector<PooledString> temp2;
	{
		const auto& field = p_Row->GetField(p_ColNames);
		if (field.HasValue())
		{
			if (field.IsMulti())
			{
				names = field.GetValuesMulti<PooledString>();
			}
			else
			{
				temp1.push_back(field.GetValueStr());
				names = &temp1;
			}
		}
	}
	{
		const auto& field = p_Row->GetField(p_ColValues);
		if (field.HasValue())
		{
			if (field.IsMulti())
			{
				values = field.GetValuesMulti<PooledString>();
			}
			else
			{
				temp2.push_back(field.GetValueStr());
				values = &temp2;
			}
		}
	}

	if (nullptr != names && nullptr != values)
		messageHeaders = GridHelpers::ExtractInternetMessageHeaders(*names, *values);

	return messageHeaders;
}

vector<InternetMessageHeader> GridHelpers::ExtractInternetMessageHeaders(const vector<PooledString>& p_Names, const vector<PooledString>& p_Values)
{
	vector<InternetMessageHeader> messageHeaders;

	ASSERT(p_Names.size() == p_Values.size());
	const auto size = min(p_Names.size(), p_Values.size());

	messageHeaders.reserve(size);
	for (size_t i = 0; i < size; ++i)
	{
		messageHeaders.emplace_back();
		messageHeaders.back().m_Name = p_Names[i];
		messageHeaders.back().m_Value = p_Values[i];
	}

	return messageHeaders;
}

SizeRange GridHelpers::ExtractSizeRange(GridBackendRow* p_Row, GridBackendColumn* p_ColMin, GridBackendColumn* p_ColMax)
{
	SizeRange sizeRange;
	const auto& fieldMin = p_Row->GetField(p_ColMin);
	const auto& fieldMax = p_Row->GetField(p_ColMax);

	ASSERT(fieldMin.HasValue() == fieldMax.HasValue());

	if (fieldMin.HasValue())
	{
		ASSERT(fieldMax.HasValue());

		sizeRange.m_MinimumSize = static_cast<int32_t>(fieldMin.GetValueLong());
		sizeRange.m_MaximumSize = static_cast<int32_t>(fieldMax.GetValueLong());
	}
	else
	{
		ASSERT(!fieldMax.HasValue());
	}

	return sizeRange;
}
