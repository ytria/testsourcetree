#include "ConversationDeserializer.h"

#include "ConversationThreadDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"

void ConversationDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("hasAttachments"), m_Data.m_HasAttachments, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastDeliveredDateTime"), m_Data.m_LastDeliveredDateTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("preview"), m_Data.m_Preview, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("topic"), m_Data.m_Topic, p_Object);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("uniqueSenders"), p_Object))
            m_Data.SetUniqueSenders(lsd.GetData());
    }

    {
        ListDeserializer<BusinessConversationThread, ConversationThreadDeserializer> deserializer;
        if (JsonSerializeUtil::DeserializeAny(deserializer, _YTEXT("threads"), p_Object))
            m_Data.SetConversationThreads(deserializer.GetData());
    }
}
