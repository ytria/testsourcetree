#pragma once

#include "ISessionLoader.h"

class RoleSessionLoader : public ISessionLoader
{
public:
	RoleSessionLoader(const SessionIdentifier& p_Identifier);
	void SetHideRoleUnscopedObjectsOverride(bool p_HideRoleUnscopedObjects);

protected:
	TaskWrapper<std::shared_ptr<Sapio365Session>> Load(const std::shared_ptr<Sapio365Session>& p_SapioSession) override;

private:
	boost::YOpt<bool> m_HideRoleUnscopedObjectsOverride;
};

