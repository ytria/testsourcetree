#pragma once

#include "DlgContentViewer.h"
#include "GridEvents.h"
#include "GridMessages.h"
#include "GridPosts.h"
#include "GridTeamChannelMessages.h"

class CacheGrid;
class GridBackendColumn;
class BusinessMessage;
class BusinessEvent;
class BusinessPost;

class MessageBodyViewer : public DlgContentViewer::NavigationCallback
						, public DlgContentViewer::AttachmentIDGetter
{
public:
	MessageBodyViewer(GridMessages& p_Grid, GridBackendColumn* p_ColBodyContentType, GridBackendColumn* p_ColSubject, GridBackendColumn* p_ColIsDraft);
	MessageBodyViewer(GridEvents& p_Grid, GridBackendColumn* p_ColBodyContentType, GridBackendColumn* p_ColSubject);
    MessageBodyViewer(GridPosts& p_Grid, GridBackendColumn* p_ColBodyContentType, GridBackendColumn* p_ColSubject);
	MessageBodyViewer(GridTeamChannelMessages& p_Grid, GridBackendColumn* p_ColBodyContentType, GridBackendColumn* p_ColSubject);

	virtual ~MessageBodyViewer();

    void ShowSelectedMessageBody();
    void ToggleView();
    bool IsShowing() const;

	void SetAddBoilerplateHtml(const bool& p_Val);
public: // DlgContentViewer::NavigationCallback
	virtual void Previous() override;
	virtual void Next() override;

public: // DlgContentViewer::AttachmentIDGetter
	virtual AttachmentsInfo::IDs GetIds(GridBackendRow* p_Row) override;

private:
	struct TypeSpecificUtil
	{
		virtual bool IsValidRow(const GridBackendRow* p_Row) const = 0;
		virtual const wstring& GetNoSelectionText() const = 0;
        virtual const wstring& GetIsDraftText() const = 0;
        virtual const wstring& GetMultiSelectionText() const = 0;
	};

	MessageBodyViewer(O365Grid& p_Grid, GridBackendColumn* p_ColBodyContentType, GridBackendColumn* p_ColSubject, GridBackendColumn* p_ColIsDraft, std::unique_ptr<TypeSpecificUtil> p_IsType);

    void Close();


	template<class RTTRRegisteredType>
	struct BusinessTypeSpecificUtil : public TypeSpecificUtil
	{
		virtual bool IsValidRow(const GridBackendRow* p_Row) const;
		virtual const wstring& GetNoSelectionText() const;
		virtual const wstring& GetMultiSelectionText() const;
        virtual const wstring& GetIsDraftText() const;
	};

	std::unique_ptr<TypeSpecificUtil> m_TypeSpecificUtil;

private:
	O365Grid& m_Grid;
	GridMessages* m_BMGrid;
	GridEvents* m_BEGrid;
	GridPosts* m_BPGrid;
	GridTeamChannelMessages* m_BTCMGrid;
    DlgContentViewer* m_DlgContentViewer;
    GridBackendColumn* m_ColBodyContentType;
    GridBackendColumn* m_ColSubject;
    GridBackendColumn* m_ColIsDraft;

    bool m_IsShowingMessageBody;
	bool m_AddBoilerplateHtml;
};

