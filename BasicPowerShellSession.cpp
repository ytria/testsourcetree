#include "BasicPowerShellSession.h"

#include "IPowerShell.h"
#include "PowerShellDeleter.h"
#include "YtriaPowerShellDll.h"

BasicPowerShellSession::BasicPowerShellSession(bool p_ExecutionPolicyUnrestricted)
	: m_PowerShell(std::shared_ptr<IPowerShell>(YtriaPowerShellDll::Get().CreatePowerShell(p_ExecutionPolicyUnrestricted), PowerShellDeleter()))
{
}

void BasicPowerShellSession::AddScript(const wchar_t* p_Script, HWND p_Originator)
{
	m_BufferedScript += p_Script;
	ASSERT(nullptr != m_PowerShell);
	if (nullptr != m_PowerShell)
		m_PowerShell->AddScript(p_Script);
}

void BasicPowerShellSession::AddScriptStopOnError(const wchar_t* p_Script, HWND p_Originator)
{
	AddScript(_YTEXTFORMAT(LR"(
		$oldActionPreference = $ErrorActionPreference
		$ErrorActionPreference='Stop'
%s
		$ErrorActionPreference=$oldActionPreference
	)", p_Script).c_str(), p_Originator);
}

InvokeResult BasicPowerShellSession::Invoke(HWND p_Originator)
{
	InvokeResult result;
	ASSERT(nullptr != m_PowerShell);
	if (m_PowerShell)
	{
		LoggerService::Debug(_YTEXTFORMAT(L"PowerShell Invoke: %s", m_BufferedScript.c_str()), p_Originator);
		result = m_PowerShell->Invoke();
		m_BufferedScript.clear();

		result.m_ErrorStream = GetPowerShell()->GetStream(IPSStream::StreamType::Error);
		if (result.m_ErrorStream->GetSize() > 0)
			result.m_Success = false;
		for (int i = 0; i < result.m_ErrorStream->GetSize(); ++i)
			LoggerService::Error(_YTEXTFORMAT(L"PowerShell Error: %s", result.m_ErrorStream->GetAsString(i)), p_Originator);
	}

	return result;
}

const std::shared_ptr<IPowerShell>& BasicPowerShellSession::GetPowerShell() const
{
	return m_PowerShell;
}

bool BasicPowerShellSession::IsHostCompatible()
{
	wstring psCompatibleVersions;
	Registry::readString(HKEY_LOCAL_MACHINE, _YTEXT("Software\\Microsoft\\PowerShell\\3\\PowerShellEngine"), _YTEXT("PSCompatibleVersion"), psCompatibleVersions);

	return psCompatibleVersions.find(_YTEXT("5.1")) != wstring::npos;
}
