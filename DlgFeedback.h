#pragma once

#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "YAdvancedHtmlView.h"

class DlgFeedback	: public ResizableDialog
					, public YAdvancedHtmlView::IPostedDataTarget
{
public:
	enum class MOOD
	{
		HAPPY = 0,
		ANGRY,
		QUESTION,
		SUGGEST_NEW_JOB
	};

	DlgFeedback(const wstring& previousText, MOOD mood, CWnd* p_Parent);
	virtual ~DlgFeedback() override;

	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data);

	const wstring& GetText() const;

protected:
	virtual BOOL OnInitDialogSpecificResizable() override;

	ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);

	wstring GetIconString() const;
	wstring GetSubtitle(const wstring& p_UserEmail) const;
	wstring GetPlaceHolder() const;
	wstring GetSendBtnText() const;

public:
	// Internal purpose only, but can't be private.
	int m_EndDialogFixedResult;

private:
	wstring m_Text;
	MOOD m_Mood;

	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;

	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;
	static const wstring g_CancelButtonName;
	static const wstring g_SubmitButtonName;
	static const wstring g_TextAreaName;
};
