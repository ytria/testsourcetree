#include "DbAccountUsageRequester.h"

#include "AzureSession.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "UsageDeserializer.h"
#include "ValueListDeserializer.h"
#include "BasicHttpRequestLogger.h"

Azure::DbAccountUsageRequester::DbAccountUsageRequester(const wstring& p_SubscriptionId, const wstring& p_ResGroup, const wstring& p_DbAccountName)
	:m_SubscriptionId(p_SubscriptionId),
	m_ResGroup(p_ResGroup),
	m_DbAccountName(p_DbAccountName)
{
}

TaskWrapper<void> Azure::DbAccountUsageRequester::Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<Azure::Usage, Azure::UsageDeserializer>>();
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri(_YTEXT("subscriptions"));
	uri.append_path(m_SubscriptionId);
	uri.append_path(_YTEXT("resourceGroups"));
	uri.append_path(m_ResGroup);
	uri.append_path(_YTEXT("providers"));
	uri.append_path(_YTEXT("Microsoft.DocumentDB"));
	uri.append_path(_YTEXT("databaseAccounts"));
	uri.append_path(m_DbAccountName);
	uri.append_path(_YTEXT("usages"));
	uri.append_query(AzureSession::g_ApiVersionKey, _YTEXT("2015-04-08"));

	return p_Session->GetAzureSession()->Get(uri.to_uri(), std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier()), m_Result, m_Deserializer, p_TaskData);
}

const vector<Azure::Usage>& Azure::DbAccountUsageRequester::GetData() const
{
	return m_Deserializer->GetData();
}
