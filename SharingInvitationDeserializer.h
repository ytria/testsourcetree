#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "SharingInvitation.h"

class SharingInvitationDeserializer : public JsonObjectDeserializer, public Encapsulate<SharingInvitation>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;    
};

