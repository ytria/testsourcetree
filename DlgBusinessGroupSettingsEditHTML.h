#pragma once

#include "DlgFormsHTML.h"

#include "BusinessGroupSetting.h"
#include "BusinessGroupSettingTemplate.h"

class DlgBusinessGroupSettingsEditHTML : public DlgFormsHTML
{
public:
    DlgBusinessGroupSettingsEditHTML(const vector<BusinessGroupSetting>& p_TenantSettings,
        const std::vector<BusinessGroupSettingTemplate>& p_GroupSettingsTemplates,
        DlgFormsHTML::Action p_Action, CWnd* p_Parent);
    const map<PooledString, BusinessGroupSetting>& GetUpdatedSettings() const;

protected:
    virtual void generateJSONScriptData() override;
    virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

private:
    BusinessGroupSetting CreateUpdatedSetting(const PooledString& p_SettingsId, const PooledString& p_TemplateId);
    BusinessGroupSetting GetOrCreateSettingById(const PooledString& settingsId);
    void AssignProperty(BusinessGroupSetting& p_GroupSetting, const PooledString& p_PropertyName, const PooledString& p_PropertyValue);
    SettingTemplateValue GetSettingPropertyInfo(const PooledString& p_TemplateId, const PooledString& p_PropertyName) const;
    PooledString InsertSpaceBetweenWords(const PooledString& p_InputStr);

    map<PooledString, BusinessGroupSetting> m_TenantSettingsByTemplateId;
    map<PooledString, BusinessGroupSetting> m_TenantSettingsBySettingsId;
    map<PooledString, BusinessGroupSettingTemplate> m_BusinessGroupSettingsTemplates;

    map<PooledString, BusinessGroupSetting> m_UpdatedSettingsBySettingsId;

    static const wstring g_IdDelim;
    static const wstring g_TemplateIdGroupUnifiedGuest;
    static const wstring g_TemplateIdProhibitedNamesRestrictedSettings;
    static const wstring g_TemplateIdPasswordRuleSettings;
    static const wstring g_PropertyBannedPasswordCheckOnPremisesMode;
};

