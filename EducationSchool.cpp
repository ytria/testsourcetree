#include "EducationSchool.h"

bool EducationSchool::operator==(const EducationSchool& p_Other) const
{
	return GetID() == p_Other.GetID();
}

bool EducationSchool::operator<(const EducationSchool& p_Other) const
{
	return GetID() < p_Other.GetID();
}

const boost::YOpt<PooledString>& EducationSchool::GetDisplayName() const
{
	return m_DisplayName;
}

void EducationSchool::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetDescription() const
{
	return m_Description;
}

void EducationSchool::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
	m_Description = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetStatus() const
{
	return m_Status;
}

void EducationSchool::SetStatus(const boost::YOpt<PooledString>& p_Val)
{
	m_Status = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetExternalSource() const
{
	return m_ExternalSource;
}

void EducationSchool::SetExternalSource(const boost::YOpt<PooledString>& p_Val)
{
	m_ExternalSource = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetPrincipalEmail() const
{
	return m_PrincipalEmail;
}

void EducationSchool::SetPrincipalEmail(const boost::YOpt<PooledString>& p_Val)
{
	m_PrincipalEmail = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetPrincipalName() const
{
	return m_PrincipalName;
}

void EducationSchool::SetPrincipalName(const boost::YOpt<PooledString>& p_Val)
{
	m_PrincipalName = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetExternalPrincipalId() const
{
	return m_ExternalPrincipalId;
}

void EducationSchool::SetExternalPrincipalId(const boost::YOpt<PooledString>& p_Val)
{
	m_ExternalPrincipalId = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetPhone() const
{
	return m_Phone;
}

void EducationSchool::SetPhone(const boost::YOpt<PooledString>& p_Val)
{
	m_Phone = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetFax() const
{
	return m_Fax;
}

void EducationSchool::SetFax(const boost::YOpt<PooledString>& p_Val)
{
	m_Fax = p_Val;
}

const boost::YOpt<PhysicalAddress>& EducationSchool::GetAddress() const
{
	return m_Address;
}

void EducationSchool::SetAddress(const boost::YOpt<PhysicalAddress>& p_Val)
{
	m_Address = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetAddressCity() const
{
	if (m_Address)
		return m_Address->City;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationSchool::SetAddressCity(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_Address)
		m_Address = PhysicalAddress();
	m_Address->City = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetAddressCountryOrRegion() const
{
	if (m_Address)
		return m_Address->CountryOrRegion;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationSchool::SetAddressCountryOrRegion(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_Address)
		m_Address = PhysicalAddress();
	m_Address->CountryOrRegion = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetAddressPostalCode() const
{
	if (m_Address)
		return m_Address->PostalCode;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationSchool::SetAddressPostalCode(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_Address)
		m_Address = PhysicalAddress();
	m_Address->PostalCode = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetAddressState() const
{
	if (m_Address)
		return m_Address->State;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationSchool::SetAddressState(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_Address)
		m_Address = PhysicalAddress();
	m_Address->State = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetAddressStreet() const
{
	if (m_Address)
		return m_Address->Street;

	static boost::YOpt<PooledString> dummy;
	return dummy;
}

void EducationSchool::SetAddressStreet(const boost::YOpt<PooledString>& p_Val)
{
	if (!m_Address)
		m_Address = PhysicalAddress();
	m_Address->Street = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetHighestGrade() const
{
	return m_HighestGrade;
}

void EducationSchool::SetHighestGrade(const boost::YOpt<PooledString>& p_Val)
{
	m_HighestGrade = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetLowestGrade() const
{
	return m_LowestGrade;
}

void EducationSchool::SetLowestGrade(const boost::YOpt<PooledString>& p_Val)
{
	m_LowestGrade = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetSchoolNumber() const
{
	return m_SchoolNumber;
}

void EducationSchool::SetSchoolNumber(const boost::YOpt<PooledString>& p_Val)
{
	m_SchoolNumber = p_Val;
}

const boost::YOpt<PooledString>& EducationSchool::GetExternalId() const
{
	return m_ExternalId;
}

void EducationSchool::SetExternalId(const boost::YOpt<PooledString>& p_Val)
{
	m_ExternalId = p_Val;
}

const boost::YOpt<IdentitySet>& EducationSchool::GetCreatedBy() const
{
	return m_CreatedBy;
}

void EducationSchool::SetCreatedBy(const boost::YOpt<IdentitySet>& p_Val)
{
	m_CreatedBy = p_Val;
}
