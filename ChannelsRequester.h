#pragma once

#include "IRequester.h"

#include "ChannelDeserializer.h"
#include "ValueListDeserializer.h"
#include "IRequestLogger.h"

class ChannelsRequester : public IRequester
{
public:
	ChannelsRequester(PooledString p_TeamId, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const vector<BusinessChannel>& GetData() const;

private:
	PooledString m_TeamId;

	shared_ptr<ValueListDeserializer<BusinessChannel, ChannelDeserializer>> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
};

