#include "SubscribedSkuDeserializer.h"

#include "JsonSerializeUtil.h"
#include "LicenseUnitsDetailDeserializer.h"
#include "ListDeserializer.h"
#include "SQL + Cloud\Role Delegation\RoleDelegationManager.h"
#include "Sapio365Session.h"
#include "ServicePlanInfoDeserializer.h"

SubscribedSkuDeserializer::SubscribedSkuDeserializer(std::shared_ptr<const Sapio365Session> p_Session)
	: m_Session(p_Session)
{

}

SubscribedSkuDeserializer::SubscribedSkuDeserializer()
{

}

void SubscribedSkuDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	static const auto objectType = MFCUtil::convertASCII_to_UNICODE(BusinessSubscribedSku().get_type().get_name().to_string());

	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeString(_YTEXT("capabilityStatus"), m_Data.m_CapabilityStatus, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("consumedUnits"), m_Data.m_ConsumedUnits, p_Object);

    {
        ListDeserializer<ServicePlanInfo, ServicePlanInfoDeserializer> spiDeserializer;
        if (JsonSerializeUtil::DeserializeAny(spiDeserializer, _YTEXT("servicePlans"), p_Object))
            m_Data.m_ServicePlans = std::move(spiDeserializer.GetData());
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("skuId"), m_Data.m_SkuId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("skuPartNumber"), m_Data.m_SkuPartNumber, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("appliesTo"), m_Data.m_AppliesTo, p_Object);

	{
		LicenseUnitsDetailDeserializer ludd;
		if (JsonSerializeUtil::DeserializeAny(ludd, _YTEXT("prepaidUnits"), p_Object))
		{
			m_Data.m_PrepaidUnits = std::move(ludd.GetData());

			ASSERT(m_Data.m_PrepaidUnits && m_Data.m_SkuPartNumber);
			if (m_Data.m_PrepaidUnits && m_Data.m_SkuPartNumber && m_Session && m_Session->IsUseRoleDelegation())
			{
				int32_t skuAccessLimit = -1;
				const auto& delegation = RoleDelegationManager::GetInstance().GetRoleDelegationFromCache(m_Session->GetRoleDelegationID(), m_Session);
				const auto val = delegation.GetSkuAccessLimit(*m_Data.m_SkuPartNumber);
				if (skuAccessLimit < 0 || 0 <= val && val < skuAccessLimit)
					skuAccessLimit = val;

				if (skuAccessLimit >= 0 && m_Data.m_PrepaidUnits->m_Enabled)
					m_Data.m_PrepaidUnits->m_Accessible = min(*m_Data.m_PrepaidUnits->m_Enabled, skuAccessLimit);
			}
		}
	}
}
