#pragma once

#include "RESTException.h"
#include <exception>

class SapioError
{
public:
	SapioError(const std::exception& e);
	SapioError(const wstring& message, unsigned short code);
	wstring GetFullErrorMessage() const;

    void AddErrorMessagePrefix(const wstring& p_Prefix);
	void SetReason(const wstring& p_Reason);

	const uint32_t& GetStatusCode() const;
	const wstring& GetReason() const;

private:
	uint32_t m_StatusCode;
	wstring m_ErrorMessage;
	wstring m_Reason;
	SapioError();

	friend class SapioErrorDeserializer;

	template<typename T>
	friend class Encapsulate;
};

class HttpError : public SapioError
{
public:
	HttpError(const RestException& e);

private:
	static wstring generateFullErrorMessage(const RestException& e);
};

class SkippedError : public SapioError
{
public:
	SkippedError();

	static bool IsSkippedError(const SapioError& p_SapioError);

private:
	static const wstring& getErrorMessage();
};