#pragma once

#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "VendorLicense.h"
#include "YAdvancedHtmlView.h"

class ModuleBase;
class DlgLicenseTenantUserCaps : public ResizableDialog, public YAdvancedHtmlView::IPostedDataTarget, public YAdvancedHtmlView::IPostedURLTarget
{
public:
	DlgLicenseTenantUserCaps(const VendorLicense& p_License, const uint32_t p_MaxQuantity, CWnd* p_Parent);

	enum { IDD = IDD_DLG_LICENSETENANTUSECAPS_HTML };

	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data) override;
	virtual bool OnNewPostedURL(const wstring& url) override;

	const LinearMap<wstring, uint32_t>&	GetUserTenantCaps() const;
	const wstring&						GetKeyValidation() const;

protected:
	virtual BOOL OnInitDialogSpecificResizable() override;

	ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);

private:
	uint32_t						m_MaxQuantity;
	LinearMap<wstring, uint32_t>	m_UserTenantCaps;
	wstring							m_KeyValidation;
	VendorLicense					m_License;
	
	static const wstring g_JSONBtnNameCancel;
	static const wstring g_JSONBtnNameOK;

	static const wstring g_JSONTenantKey;
	static const wstring g_JSONQuantityKey;
	static const wstring g_JSONKeyValidation;

	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;
};