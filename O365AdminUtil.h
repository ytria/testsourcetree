#pragma once

#include "CommandInfo.h"
#include "DlgRestrictedAccess.h"
#include "HttpResultWithError.h"
#include "ISetting.h"
#include "LicenseUtil.h"
#include "LoadingScope.h"
#include "Office365Admin.h"
#include "PooledString.h"

#include <functional>

struct AutomationSetup;
class GraphCache;
class GridFrameBase;
class MultiObjectsRequestLogger;
class MultiObjectsPageRequestLogger;
class PersistentSession;
class Sapio365Session;

namespace Util
{
	struct HybridCheckResult
	{
		bool m_CanDoHybrid = false;
		wstring m_ADDSDomain;
		vector<wstring> m_VerifiedDomains;
	};

	HybridCheckResult DoHybridCheck(const std::shared_ptr<Sapio365Session>& p_Session);
	void InitOnPrem(const std::shared_ptr<Sapio365Session>& p_Session, bool p_ForceDlgEdit = false);
	wstring PromptForNewApplicationName(const wstring& p_DefaultName, CWnd* p_Parent); // Returns empty if canceled.

	wstring GetElevatedSessionAppName(const PersistentSession& p_Session);
	YtriaTaskData AddFrameTask(const wstring& p_LogMessage, GridFrameBase* p_Frame, AutomationSetup& p_AutoSetup, const LicenseContext& p_LicenseContext, bool p_IsMotherTask); // set p_IsMotherTask to true if task corresponds to module opening (first load).
	namespace detail
	{
		YtriaTaskData AddTaskData(const wstring& p_Name, CFrameWnd* p_Originator);
	}
    wstring GetFileAttachmentStr();
    wstring GetItemAttachmentStr();
    wstring GetReferenceAttachmentStr();

	HttpResultWithError GetResult(pplx::task<RestResultInfo> p_Task);

	wstring RemoveDeltaFromDate(const wstring p_OrigDateIso8601, LoadingScope p_Delta);
    void RemoveLicensePlansProperties(vector<rttr::property>& p_Properties);
	template <class T>
	wstring GetDisplayNameOrId(const T& p_BusinessObj)
	{
		return p_BusinessObj.GetDisplayName() ? *p_BusinessObj.GetDisplayName() : p_BusinessObj.GetID();
	}

	template <typename T>
	json::value ObjectToJson(const T& p_Object, const vector<wstring>& p_Properties)
	{
		json::value json;

		rttr::type type = rttr::type::get<T>();
		ASSERT(type.is_valid());
		if (type.is_valid())
		{
			for (const auto& propertyStr : p_Properties)
			{
				rttr::variant value = type.get_property_value(MFCUtil::convertUNICODE_to_UTF8(propertyStr), p_Object);
				ASSERT(value.is_valid());
				if (value.is_valid())
				{
					if (value.is_type<boost::YOpt<PooledString>>())
					{
						boost::YOpt<PooledString> strVal = value.get_value<boost::YOpt<PooledString>>();
						if (strVal)
							json.as_object()[propertyStr] = json::value::string(*strVal);
						else
							json.as_object()[propertyStr] = json::value::null();
					}
					else
						ASSERT(false);
				}
			}
		}
		return json;
	}

	template <typename T>
    std::vector<T> ListErrorHandler(const std::exception_ptr& p_ExceptPtr);
    template <typename T>
    T ObjectErrorHandler(const std::exception_ptr& p_ExceptPtr);

    template <typename T>
    T ObjectErrorHandler(const std::exception_ptr& p_ExceptPtr)
    {
        ASSERT(p_ExceptPtr);
        if (p_ExceptPtr)
        {
            try
            {
                std::rethrow_exception(p_ExceptPtr);
            }
            catch (const RestException& e)
            {
                T object;
                object.SetError(HttpError(e));
                return object;
            }
            catch (const std::exception& e)
            {
                T object;
                object.SetError(SapioError(e));
                return object;
            }
        }

        ASSERT(false);
        return T();
    }

    HttpResultWithError WriteErrorHandler(const std::exception_ptr& p_ExceptPtr);

	RestResultInfo RestResultInfoErrorHandler(const std::exception_ptr& p_ExceptPtr);

	map<PooledString, int32_t> GetConsumedMap(const vector<BusinessSubscribedSku>& p_BusinessSubscribedSkus);

	struct RestrictedAccessDialogConfig
	{
		const wstring m_Title;
		const wstring m_Text;
		const wstring m_ImportantText;
		const wstring m_LearnMoreText;
		const wstring m_LearnMoreURL;
		const wstring m_OkButtonTextOverride;
		const wstring m_CancelButtonTextOverride;
	};

	// If p_Setting == NoSetting<bool>(), "don't ask again" is not available.
	bool ShowRestrictedAccessDialog(DlgRestrictedAccess::Image p_Image, RestrictedAccessDialogConfig p_Config, const ISetting<bool>& p_Setting, CWnd* p_Parent, bool p_ShowOnlyOK = false, int p_AdditionalHeight = 0, int p_AdditionalWidth = 0);

    enum class ElevatedPrivilegesDlgResult { Cancel, Elevate, ElevateManually };
    ElevatedPrivilegesDlgResult ShowExplainElevatedPrivilegesDialog(CWnd* p_Parent, bool p_JustCreatedFullSession);

	INT_PTR ShowApplicationConnectionError(const wstring& p_Title, const wstring& p_JsonError, CWnd* p_Parent, bool p_WithRetryButton = false);

	web::json::value GetFileSystemInfo(const wstring& p_FilePath);

	void ProcessSubUserItems(std::function<void(BusinessUser&)> p_Process, const O365IdsContainer& p_IDs, Origin p_Origin, GraphCache& p_GraphCache, bool p_UseSQLCache, std::shared_ptr<MultiObjectsRequestLogger> p_Logger, YtriaTaskData p_TaskData);
	void ProcessSubUserItems(std::function<void(BusinessUser&)> p_Process, const O365IdsContainer& p_IDs, Origin p_Origin, GraphCache& p_GraphCache, bool p_UseSQLCache, std::shared_ptr<MultiObjectsPageRequestLogger> p_Logger, YtriaTaskData p_TaskData);

	LicenseContext ForSubModule(const LicenseContext& p_LC);
}

template <typename T>
std::vector<T> Util::ListErrorHandler(const std::exception_ptr& p_ExceptPtr)
{
    ASSERT(p_ExceptPtr);
    if (p_ExceptPtr)
    {
        try
        {
            std::rethrow_exception(p_ExceptPtr);
        }
        catch (const RestException& e)
        {
            T object;
            object.SetError(HttpError(e));

            return vector<T>{ object };
        }
        catch (const std::exception& e)
        {
            T object;
            object.SetError(SapioError(e));

            return vector<T>{ object };
        }
    }

    ASSERT(false);
    return vector<T>();
}

