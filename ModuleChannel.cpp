#include "ModuleChannel.h"

#include "BusinessObjectManager.h"
#include "ChannelLoadMoreRequester.h"
#include "FrameChannels.h"
#include "MultiObjectsRequestLogger.h"
#include "O365AdminUtil.h"
#include "RunOnScopeEnd.h"
#include "UpdatedObjectsGenerator.h"
#include "YCallbackMessage.h"

void ModuleChannel::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
    {
    case Command::ModuleTask::List:
        showChannels(p_Command);
        break;
	case Command::ModuleTask::UpdateMoreInfo:
		loadMore(p_Command);
		break;
    default:
        ASSERT(false);
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
        break;
    }
}

void ModuleChannel::showChannels(const Command& p_Command)
{
    const auto& info = p_Command.GetCommandInfo();
    const auto	p_Origin = info.GetOrigin();

    ASSERT(p_Origin != Origin::NotSet);
    if (p_Origin != Origin::NotSet)
    {
        auto	p_IDs = info.GetIds();
        auto	p_Frame = dynamic_cast<FrameChannels*>(info.GetFrame());
        auto	p_SourceWindow = p_Command.GetSourceWindow();
        auto	p_Action = p_Command.GetAutomationAction();
        auto	sourceCWnd = p_SourceWindow.GetCWnd();

        if (p_IDs.empty())
        {
            auto& id = GetConnectedSession().GetConnectedUserId();
            ASSERT(!id.empty());
            if (!id.empty())
                p_IDs.insert(id);
        }
		
        auto frame = p_Frame;
        if (nullptr == frame)
        {
            if (!FrameChannels::CanCreateNewFrame(true, sourceCWnd, p_Action))
                return;

            ModuleCriteria moduleCriteria;
            moduleCriteria.m_Origin				= p_Origin;
            moduleCriteria.m_UsedContainer		= ModuleCriteria::UsedContainer::IDS;
            moduleCriteria.m_IDs				= p_IDs;
			moduleCriteria.m_Privilege			= info.GetRBACPrivilege();
			moduleCriteria.m_MetaDataColumnInfo	= info.GetMetaDataColumnInfo();

            if (ShouldCreateFrame<FrameChannels>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
            {
                CWaitCursor _;

                frame = new FrameChannels(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleChannel_showChannels_1, _YLOC("Team Channels")).c_str(), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
                frame->InitModuleCriteria(moduleCriteria);
                AddGridFrame(frame);
                frame->Erect();
            }
        }

		if (nullptr != frame)
		{
			HWND hwnd = frame->GetSafeHwnd();
			const bool isRefresh = nullptr != p_Frame;
			auto taskData = addFrameTask(YtriaTranslate::Do(ModuleChannel_showChannels_2, _YLOC("Show Team Channels")).c_str(), frame, p_Command, !isRefresh);

			RefreshSpecificData refreshSpecificData;
			if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
				refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

			std::vector<GridBackendRow*> rowsWithMoreLoaded;
			frame->GetGrid().GetRowsWithMoreLoaded(rowsWithMoreLoaded);

			std::vector<BusinessChannel> channelsLoadMoreRequestInfo;
			if (!rowsWithMoreLoaded.empty())
			{
				auto gridChannels = dynamic_cast<GridChannels*>(&frame->GetGrid());
				ASSERT(nullptr != gridChannels);
				if (nullptr != gridChannels)
					channelsLoadMoreRequestInfo = gridChannels->GetLoadMoreRequestInfo(rowsWithMoreLoaded);
			}

			YSafeCreateTaskMutable([this, taskData, refreshSpecificData, hwnd, p_Origin, p_IDs, currentModuleCriteria = frame->GetModuleCriteria(), p_Action, bom = frame->GetBusinessObjectManager(), isRefresh = nullptr != p_Frame, wantsAdditionalMetadata = (bool)frame->GetMetaDataColumnInfo(), channelsLoadMoreRequestInfo, gridLogger = frame->GetGridLogger()]() mutable
			{
				const bool partialRefresh = !refreshSpecificData.m_Criteria.m_IDs.empty();
				const auto& criteria = partialRefresh ? refreshSpecificData.m_Criteria : currentModuleCriteria;
				auto channels = retrieveChannels(bom, criteria, wantsAdditionalMetadata, taskData);

				if (!channelsLoadMoreRequestInfo.empty())
				{
					auto logger = std::make_shared<BasicRequestLogger>(_YTEXT(""));
					{
						gridLogger->SetIgnoreHttp404Errors(true);
						RunOnScopeEnd _([&gridLogger]()
							{
								gridLogger->SetIgnoreHttp404Errors(false);
							});
						for (auto& channel : channelsLoadMoreRequestInfo)
						{
							ASSERT(channel.GetDisplayName());
							if (channel.GetDisplayName())
								logger->SetContextualInfo(*channel.GetDisplayName());

							auto loadMoreRequester = std::make_shared<ChannelLoadMoreRequester>(channel.GetGroupId(), channel.GetID(), channel.GetMembershipType(), ChannelLoadMoreRequester::REQUEST_MEMBERS | ChannelLoadMoreRequester::REQUEST_DRIVE, logger);
							loadMoreRequester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();
							channel = loadMoreRequester->GetData();
						}
					}
				}

				YCallbackMessage::DoPost([hwnd, taskData, partialRefresh, p_Action, channels, isRefresh, isCanceled = taskData.IsCanceled(), channelsLoadMore = channelsLoadMoreRequestInfo]
				{
					bool shouldFinishTask = true;
					auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameChannels*>(CWnd::FromHandle(hwnd)) : nullptr;
					if (ShouldBuildView(hwnd, isCanceled, taskData, false))
					{
						ASSERT(nullptr != frame);

						frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting channels for %s teams...", Str::getStringFromNumber(channels.size()).c_str()));
						frame->GetGrid().ClearLog(taskData.GetId());
						//frame->GetGrid().ClearStatus();

						{
							CWaitCursor _;
							frame->ShowChannels(channels, channelsLoadMore, !partialRefresh);
						}

						if (!isRefresh)
						{
							frame->UpdateContext(taskData, hwnd);
							shouldFinishTask = false;
						}
					}

					if (shouldFinishTask)
					{
						// Because we didn't (and can't) call UpdateContext here.
						ModuleBase::TaskFinished(frame, taskData, p_Action);
					}
				});
			});
		}
		else
		{
			ASSERT(nullptr == p_Action);
			SetAutomationGreenLight(p_Action, _YTEXT(""));
		}
    }
    else
    {
        ASSERT(false);
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(ModuleContact_showContacts_3, _YLOC(" - origin not set"),_YR("Y2466")).c_str());
    }
}

void ModuleChannel::loadMore(const Command& p_Command)
{
	auto frame = dynamic_cast<FrameChannels*>(p_Command.GetCommandInfo().GetFrame());

	using DataType = UpdatedObjectsGenerator<BusinessChannel>;
	bool isDataValid = p_Command.GetCommandInfo().Data().is_type<DataType>();

	ASSERT(nullptr != frame && isDataValid);
	if (nullptr != frame && isDataValid)
	{
		auto taskData = addFrameTask(_T("More Info"), frame, p_Command, false);

		YSafeCreateTask([this, taskData, bom = frame->GetBusinessObjectManager(), p_Command, hwnd = frame->GetSafeHwnd(), gridLogger = frame->GetGridLogger()]() {
			auto logger = std::make_shared<BasicRequestLogger>(_T(""));
			auto channels = p_Command.GetCommandInfo().Data().get_value<DataType>().GetObjects();

			{
				gridLogger->SetIgnoreHttp404Errors(true);
				RunOnScopeEnd _([&gridLogger]()
					{
						gridLogger->SetIgnoreHttp404Errors(false);
					});
				for (auto& channel : channels)
				{
					ASSERT(channel.GetDisplayName());
					if (channel.GetDisplayName())
						logger->SetContextualInfo(*channel.GetDisplayName());

					auto loadMoreRequester = std::make_shared<ChannelLoadMoreRequester>(channel.GetGroupId(), channel.GetID(), channel.GetMembershipType(), ChannelLoadMoreRequester::REQUEST_MEMBERS | ChannelLoadMoreRequester::REQUEST_DRIVE, logger);
					loadMoreRequester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();
					channel = loadMoreRequester->GetData();
				}
			}

			YCallbackMessage::DoPost([canceled = taskData.IsCanceled(), hwnd, taskData, channels, command = p_Command]() {
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameChannels*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, canceled, taskData, false))
				{
					ASSERT(nullptr != frame);

					frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
					frame->GetGrid().ClearLog(taskData.GetId());
					{
						CWaitCursor cursor;
						frame->UpdateChannelsLoadMore(channels);
					}
				}
				ModuleBase::TaskFinished(frame, taskData, command.GetAutomationAction());
			});
		});
	}
}

O365DataMap<BusinessGroup, vector<BusinessChannel>> ModuleChannel::retrieveChannels(std::shared_ptr<BusinessObjectManager> p_BOM, const ModuleCriteria& p_Criteria, bool p_UseSQLCacheForGroups, YtriaTaskData p_TaskData) const
{
	O365DataMap<BusinessGroup, vector<BusinessChannel>> channels;

	vector<BusinessGroup> groups;
	if (p_UseSQLCacheForGroups)
	{
		// Do not pass p_TaskData if we don't want to allow cancellation of this step.
		groups = p_BOM->GetGraphCache().GetSqlCachedGroups(p_TaskData.GetOriginator(), p_Criteria.m_IDs, false, p_TaskData);
	}

	auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("group channels"), p_Criteria.m_IDs.size());

	auto processGroup = [&p_BOM, &p_TaskData, &channels, &logger](auto& group)
	{
		vector<BusinessChannel> groupChannels;

		logger->IncrementObjCount();
		logger->SetContextualInfo(GetDisplayNameOrId(group));

		if (!p_TaskData.IsCanceled())
			groupChannels = p_BOM->GetBusinessChannels(group.GetID(), logger, p_TaskData).GetTask().get();

		if (p_TaskData.IsCanceled())
		{
			group.SetFlags(BusinessObject::CANCELED);
			channels[group] = {};
		}
		else
		{
			channels[group] = groupChannels;
		}
	};

	auto ids = p_Criteria.m_IDs;
	for (auto& group : groups)
	{
		if (1 == ids.erase(group.GetID()))
			processGroup(group);
	}

	ASSERT(!p_UseSQLCacheForGroups || ids.empty()); // Is it normal not all groups are in SQL cache?

    for (const auto& id : ids)
    {
		BusinessGroup bg;
		bg.SetID(id);
		logger->SetContextualInfo(p_BOM->GetGraphCache().GetGroupContextualInfo(id));
		p_BOM->GetGraphCache().SyncUncachedGroup(bg, logger, p_TaskData);

		processGroup(bg);        
    }

    return channels;
}
