#include "BusinessColumnDefinition.h"

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessColumnDefinition>("ColumnDefinition") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Column Definition"))))
		.constructor()(policy::ctor::as_object)
		.property("Id", &BusinessColumnDefinition::m_Id)
		.property("DisplayName", &BusinessColumnDefinition::m_DisplayName)
		;
}

BusinessColumnDefinition::BusinessColumnDefinition()
{
}

BusinessColumnDefinition::BusinessColumnDefinition(const ColumnDefinition& p_Column)
{
    m_ColumnGroup = p_Column.ColumnGroup;
    m_Description = p_Column.Description;
    m_DisplayName = p_Column.DisplayName;
    m_EnforceUniqueValues = p_Column.EnforceUniqueValues;
    m_Hidden = p_Column.Hidden;
    m_Id = p_Column.Id ? *p_Column.Id : _YTEXT("");
    m_Indexed = p_Column.Indexed;
    m_Name = p_Column.Name;
    m_ReadOnly = p_Column.ReadOnly;
    m_Required = p_Column.Required;
    m_Boolean = p_Column.Boolean;
    m_Calculated = p_Column.Calculated;
    m_Choice = p_Column.Choice;
    m_Currency = p_Column.Currency;
    m_DateTime = p_Column.DateTime;
    m_DefaultValue = p_Column.DefaultValue;
    m_Lookup = p_Column.Lookup;
    m_Number = p_Column.Number;
    m_PersonOrGroup = p_Column.PersonOrGroup;
    m_Text = p_Column.Text;
}

const boost::YOpt<PooledString>& BusinessColumnDefinition::GetColumnGroup() const
{
    return m_ColumnGroup;
}

void BusinessColumnDefinition::SetColumnGroup(const boost::YOpt<PooledString>& p_Val)
{
    m_ColumnGroup = p_Val;
}

const boost::YOpt<PooledString>& BusinessColumnDefinition::GetDescription() const
{
    return m_Description;
}

void BusinessColumnDefinition::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
    m_Description = p_Val;
}

const boost::YOpt<PooledString>& BusinessColumnDefinition::GetDisplayName() const
{
    return m_DisplayName;
}

void BusinessColumnDefinition::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
    m_DisplayName = p_Val;
}

const boost::YOpt<bool>& BusinessColumnDefinition::GetEnforceUniqueValues() const
{
    return m_EnforceUniqueValues;
}

void BusinessColumnDefinition::SetEnforceUniqueValues(const boost::YOpt<bool>& p_Val)
{
    m_EnforceUniqueValues = p_Val;
}

const boost::YOpt<bool>& BusinessColumnDefinition::GetHidden() const
{
    return m_Hidden;
}

void BusinessColumnDefinition::SetHidden(const boost::YOpt<bool>& p_Val)
{
    m_Hidden = p_Val;
}

const boost::YOpt<bool>& BusinessColumnDefinition::GetIndexed() const
{
    return m_Indexed;
}

void BusinessColumnDefinition::SetIndexed(const boost::YOpt<bool>& p_Val)
{
    m_Indexed = p_Val;
}

const boost::YOpt<PooledString>& BusinessColumnDefinition::GetName() const
{
    return m_Name;
}

void BusinessColumnDefinition::SetName(const boost::YOpt<PooledString>& p_Val)
{
    m_Name = p_Val;
}

const boost::YOpt<bool>& BusinessColumnDefinition::GetReadOnly() const
{
    return m_ReadOnly;
}

void BusinessColumnDefinition::SetReadOnly(const boost::YOpt<bool>& p_Val)
{
    m_ReadOnly = p_Val;
}

const boost::YOpt<bool>& BusinessColumnDefinition::GetRequired() const
{
    return m_Required;
}

void BusinessColumnDefinition::SetRequired(const boost::YOpt<bool>& p_Val)
{
    m_Required = p_Val;
}

const boost::YOpt<BooleanColumn>& BusinessColumnDefinition::GetBooleanFacet() const
{
    return m_Boolean;
}

void BusinessColumnDefinition::SetBooleanFacet(const boost::YOpt<BooleanColumn>& p_Val)
{
    m_Boolean = p_Val;
}

const boost::YOpt<CalculatedColumn>& BusinessColumnDefinition::GetCalculatedFacet() const
{
    return m_Calculated;
}

void BusinessColumnDefinition::SetCalculatedFacet(const boost::YOpt<CalculatedColumn>& p_Val)
{
    m_Calculated = p_Val;
}

const boost::YOpt<ChoiceColumn>& BusinessColumnDefinition::GetChoiceFacet() const
{
    return m_Choice;
}

void BusinessColumnDefinition::SetChoiceFacet(const boost::YOpt<ChoiceColumn>& p_Val)
{
    m_Choice = p_Val;
}

const boost::YOpt<CurrencyColumn>& BusinessColumnDefinition::GetCurrencyFacet() const
{
    return m_Currency;
}

void BusinessColumnDefinition::SetCurrencyFacet(const boost::YOpt<CurrencyColumn>& p_Val)
{
    m_Currency = p_Val;
}

const boost::YOpt<DateTimeColumn>& BusinessColumnDefinition::GetDateTimeFacet() const
{
    return m_DateTime;
}

void BusinessColumnDefinition::SetDateTimeFacet(const boost::YOpt<DateTimeColumn>& p_Val)
{
    m_DateTime = p_Val;
}

const boost::YOpt<DefaultValueColumn>& BusinessColumnDefinition::GetDefaultValueFacet() const
{
    return m_DefaultValue;
}

void BusinessColumnDefinition::SetDefaultValueFacet(const boost::YOpt<DefaultValueColumn>& p_Val)
{
    m_DefaultValue = p_Val;
}

const boost::YOpt<LookupColumn>& BusinessColumnDefinition::GetLookupFacet() const
{
    return m_Lookup;
}

void BusinessColumnDefinition::SetLookupFacet(const boost::YOpt<LookupColumn>& p_Val)
{
    m_Lookup = p_Val;
}

const boost::YOpt<NumberColumn>& BusinessColumnDefinition::GetNumberFacet() const
{
    return m_Number;
}

void BusinessColumnDefinition::SetNumberFacet(const boost::YOpt<NumberColumn>& p_Val)
{
    m_Number = p_Val;
}

const boost::YOpt<PersonOrGroupColumn>& BusinessColumnDefinition::GetPersonOrGroupFacet() const
{
    return m_PersonOrGroup;
}

void BusinessColumnDefinition::SetPersonOrGroupFacet(const boost::YOpt<PersonOrGroupColumn>& p_Val)
{
    m_PersonOrGroup = p_Val;
}

const boost::YOpt<TextColumn>& BusinessColumnDefinition::GetTextFacet() const
{
    return m_Text;
}

void BusinessColumnDefinition::SetTextFacet(const boost::YOpt<TextColumn>& p_Val)
{
    m_Text = p_Val;
}
