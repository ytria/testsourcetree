#pragma once

#include "FieldUpdate.h"

class RevokeAccessFieldUpdate : public FieldUpdateO365
{
public:
	static const wstring& GetPlaceHolder();

public:
	RevokeAccessFieldUpdate(O365Grid& p_Grid,
		const row_pk_t& p_RowKey,
		const UINT& p_ColumnId,
		const GridBackendField& p_OldValue,
		const GridBackendField& p_NewValue);

	virtual void RefreshState() override;
	virtual vector<ModificationLog> GetModificationLogs() const override;
	virtual std::unique_ptr<FieldUpdateO365> CreateReplacement(std::unique_ptr<FieldUpdateO365> p_Replacement) override;
};