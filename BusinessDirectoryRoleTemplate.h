#pragma once

#include "BusinessObject.h"

class DirectoryRoleTemplate;

class BusinessDirectoryRoleTemplate : public BusinessObject
{
public:
    BusinessDirectoryRoleTemplate() = default;
    BusinessDirectoryRoleTemplate(const DirectoryRoleTemplate& p_DirectoryRoleTemplate);

    const boost::YOpt<PooledString>& GetDescription() const;
    void SetDescription(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<PooledString>& GetDisplayName() const;
    void SetDisplayName(const boost::YOpt<PooledString>& p_Val);

private:
    boost::YOpt<PooledString> m_Description;
    boost::YOpt<PooledString> m_DisplayName;

    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND
    friend class DirectoryRoleTemplateDeserializer;
};

