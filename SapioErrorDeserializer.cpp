#include "SapioErrorDeserializer.h"

#include "JsonSerializeUtil.h"

void SapioErrorDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeUint32(_YTEXT("statusCode"), m_Data.m_StatusCode, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("errorMessage"), m_Data.m_ErrorMessage, p_Object);
}
