#include "BusinessPost.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessPost>("Post")
	(metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Post"))))
	.constructor()(policy::ctor::as_object);
}

BusinessPost::BusinessPost()
{
}

BusinessPost::BusinessPost(const Post& p_Post)
{
    m_Body = p_Post.Body;
    m_Categories = p_Post.Categories;
    m_ChangeKey = p_Post.ChangeKey;
    m_ConversationId = p_Post.ConversationId;
    m_ConversationThreadId = p_Post.ConversationThreadId;
    m_CreatedDateTime = p_Post.CreatedDateTime;
    m_From = p_Post.From;
    m_HasAttachments = p_Post.HasAttachments;
    m_LastModifiedDateTime = p_Post.LastModifiedDateTime;
    m_NewParticipants = p_Post.NewParticipants;
    m_ReceivedDateTime = p_Post.ReceivedDateTime;
    m_Sender = p_Post.Sender;
    ASSERT(p_Post.Id);
    if (p_Post.Id)
        m_Id = *p_Post.Id;
}

const boost::YOpt<ItemBody>& BusinessPost::GetBody() const
{
    return m_Body;
}

void BusinessPost::SetBody(const boost::YOpt<ItemBody>& p_Val)
{
    m_Body = p_Val;
}

const boost::YOpt<vector<PooledString>>& BusinessPost::GetCategories() const
{
    return m_Categories;
}

void BusinessPost::SetCategories(const boost::YOpt<vector<PooledString>>& p_Val)
{
    m_Categories = p_Val;
}

const boost::YOpt<PooledString>& BusinessPost::GetChangeKey() const
{
    return m_ChangeKey;
}

void BusinessPost::SetChangeKey(const boost::YOpt<PooledString>& p_Val)
{
    m_ChangeKey = p_Val;
}

const boost::YOpt<PooledString>& BusinessPost::GetConversationId() const
{
    return m_ConversationId;
}

void BusinessPost::SetConversationId(const boost::YOpt<PooledString>& p_Val)
{
    m_ConversationId = p_Val;
}

const boost::YOpt<PooledString>& BusinessPost::GetConversationThreadId() const
{
    return m_ConversationThreadId;
}

void BusinessPost::SetConversationThreadId(const boost::YOpt<PooledString>& p_Val)
{
    m_ConversationThreadId = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessPost::GetCreatedDateTime() const
{
    return m_CreatedDateTime;
}

void BusinessPost::SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_CreatedDateTime = p_Val;
}

const boost::YOpt<Recipient>& BusinessPost::GetFrom() const
{
    return m_From;
}

void BusinessPost::SetFrom(const boost::YOpt<Recipient>& p_Val)
{
    m_From = p_Val;
}

const boost::YOpt<bool>& BusinessPost::GetHasAttachments() const
{
    return m_HasAttachments;
}

void BusinessPost::SetHasAttachments(const boost::YOpt<bool>& p_Val)
{
    m_HasAttachments = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessPost::GetLastModifiedDateTime() const
{
    return m_LastModifiedDateTime;
}

void BusinessPost::SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_LastModifiedDateTime = p_Val;
}

const vector<Recipient>& BusinessPost::GetNewParticipants() const
{
    return m_NewParticipants;
}

void BusinessPost::SetNewParticipants(const vector<Recipient>& p_Val)
{
    m_NewParticipants = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessPost::GetReceivedDateTime() const
{
    return m_ReceivedDateTime;
}

void BusinessPost::SetReceivedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_ReceivedDateTime = p_Val;
}

const boost::YOpt<Recipient>& BusinessPost::GetSender() const
{
    return m_Sender;
}

void BusinessPost::SetSender(const boost::YOpt<Recipient>& p_Val)
{
    m_Sender = p_Val;
}

Post BusinessPost::ToPost(const BusinessPost& p_BusinessPost)
{
	Post post;

	post.Body = p_BusinessPost.GetBody();
	post.Categories = p_BusinessPost.GetCategories();
	post.ChangeKey = p_BusinessPost.GetChangeKey();
	post.ConversationId = p_BusinessPost.GetConversationId();
	post.ConversationThreadId = p_BusinessPost.GetConversationThreadId();
	post.CreatedDateTime = p_BusinessPost.GetCreatedDateTime();
	post.From = p_BusinessPost.GetFrom();
	post.HasAttachments = p_BusinessPost.GetHasAttachments();
	post.Id = p_BusinessPost.GetID();
	post.LastModifiedDateTime = p_BusinessPost.GetLastModifiedDateTime();
	post.NewParticipants = p_BusinessPost.GetNewParticipants();
	post.Sender = p_BusinessPost.GetSender();

	return post;
}
