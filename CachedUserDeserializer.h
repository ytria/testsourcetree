#pragma once

#include "JsonObjectDeserializer.h"
#include "CachedUser.h"
#include "Encapsulate.h"

class CachedUserDeserializer : public JsonObjectDeserializer
                             , public Encapsulate<CachedUser>
{
public:
    CachedUserDeserializer() = default;

    void DeserializeObject(const web::json::object& p_Object) override;
};

