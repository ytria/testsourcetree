#pragma once

class PSUtil
{
public:
	static void AddParamToCmd(wstring& p_Cmd, const wstring& p_PropName, const boost::YOpt<bool>& p_Val);
	static void AddParamToCmd(wstring& p_Cmd, const wstring& p_PropName, const boost::YOpt<vector<PooledString>>& p_Val, const wstring& p_Separator = _YTEXT(","));
	static void AddParamToCmd(wstring& p_Cmd, const wstring& p_PropName, const boost::YOpt<wstring>& p_Val);
	static void AddParamToCmd(wstring& p_Cmd, const wstring& p_PropName, const boost::YOpt<PooledString>& p_Val);
	static void AddParamToCmd(wstring& p_Cmd, const wstring& p_PropName, const boost::YOpt<YTimeDate>& p_Date);
};

