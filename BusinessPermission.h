#pragma once

#include "BusinessObject.h"

#include "Permission.h"

class BusinessPermission : public BusinessObject
{
public:
    BusinessPermission();
    BusinessPermission(const Permission& p_Permission);
    
    const boost::YOpt<IdentitySet>& GetGrantedTo() const;
    void SetGrantedTo(const boost::YOpt<IdentitySet>& p_Val);
    
    const boost::YOpt<SharingInvitation>& GetInvitation() const;
    void SetInvitation(const boost::YOpt<SharingInvitation>& p_Val);
    
    const boost::YOpt<ItemReference>& GetInheritedFrom() const;
    void SetInheritedFrom(const boost::YOpt<ItemReference>& p_Val);
    
    const boost::YOpt<SharingLink>& GetLink() const;
    void SetLink(const boost::YOpt<SharingLink>& p_Val);
    
    const boost::YOpt<vector<PooledString>>& GetRole() const;
    void SetRole(const boost::YOpt<vector<PooledString>>& p_Val);
    
    const boost::YOpt<PooledString>& GetShareId() const;
    void SetShareId(const boost::YOpt<PooledString>& p_Val);

    Permission ToPermission() const;

private:
    boost::YOpt<IdentitySet> m_GrantedTo;
    boost::YOpt<SharingInvitation> m_Invitation;
    boost::YOpt<ItemReference> m_InheritedFrom;
    boost::YOpt<SharingLink> m_Link;
    boost::YOpt<vector<PooledString>> m_Role;
    boost::YOpt<PooledString> m_ShareId;
};

