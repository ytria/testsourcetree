#pragma once

class WindowsListObserver
{
public:

	virtual void UpdateWindowList() = 0;
};