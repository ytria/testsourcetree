#include "AddClassToSchoolRequester.h"
#include "O365AdminUtil.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

AddClassToSchoolRequester::AddClassToSchoolRequester(const wstring& p_SchoolId, const wstring& p_ClassId)
	:m_SchoolId(p_SchoolId),
	m_ClassId(p_ClassId)
{

}

TaskWrapper<void> AddClassToSchoolRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("schools"));
	uri.append_path(m_SchoolId);
	uri.append_path(_YTEXT("classes"));
	uri.append_path(_YTEXT("$ref"));

	LoggerService::User(_YFORMAT(L"Creating class for school with id %s", m_SchoolId.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_uri(), httpLogger, p_TaskData).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResInfo) {
		*result = Util::GetResult(p_ResInfo);
	});
}

const HttpResultWithError& AddClassToSchoolRequester::GetResult() const
{
	return *m_Result;
}
