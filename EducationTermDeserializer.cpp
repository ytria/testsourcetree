#include "EducationTermDeserializer.h"
#include "JsonSerializeUtil.h"

void EducationTermDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("externalId"), m_Data.m_ExternalId, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("startDate"), m_Data.m_StartDate, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("endDate"), m_Data.m_EndDate, p_Object);
}
