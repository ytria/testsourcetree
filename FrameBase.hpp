#pragma once

#include "FrameBase.h"

#include "DlgRoleSeeInfo.h"
#include "DlgTokenHistory.h"
#include "ModuleBase.h"
#include "RegexException.h"
#include "Registry.h"
#include "WindowsListUpdater.h"
#include "Office365Admin.h"
#include "PersistentSession.h"
#include "RoleDelegationManager.h"
#include "Product.h"
#include "RibbonBackstageView.h"
#include "SessionsSqlEngine.h"
#include "SessionTypes.h"
#include "YCodeJockMessageBox.h"
#include "YDialog.h"
#include <CommandBars/StatusBar/XTPStatusBarPaintManager.h>
#include <CommandBars/MessageBar/XTPMessageBarPaintManager.h>

template <class CodeJockFrameClass>
wstring FrameBase<CodeJockFrameClass>::g_InfoSessionGroup;

template <class CodeJockFrameClass>
wstring FrameBase<CodeJockFrameClass>::g_FeedbackTabName;

template <class CodeJockFrameClass>
bool FrameBase<CodeJockFrameClass>::g_staticInit = false;

BEGIN_TEMPLATE_MESSAGE_MAP(FrameBase, CodeJockFrameClass, CodeJockFrameClass)
	ON_COMMAND(ID_LOGOUT,							OnLogout)
	ON_UPDATE_COMMAND_UI(ID_LOGOUT,					OnUpdateLogout)
	ON_COMMAND(ID_SESSION_SEE_ROLEINFO,				OnSeeRoleInfo)
	ON_UPDATE_COMMAND_UI(ID_SESSION_SEE_ROLEINFO,	OnUpdateSeeRoleInfo)
	ON_COMMAND(ID_RIBBON_MINIMIZE,					OnToggleRibbon)
	ON_COMMAND(ID_RIBBON_EXPAND,					OnToggleRibbon)
	ON_UPDATE_COMMAND_UI(ID_RIBBON_MINIMIZE,		OnUpdateRibbonMinimize)
	ON_UPDATE_COMMAND_UI(ID_RIBBON_EXPAND,			OnUpdateRibbonExpand)
	ON_COMMAND(ID_RIBBON_HELP,						OnRibbonHelp)
	ON_UPDATE_COMMAND_UI(ID_RIBBON_HELP,			OnUpdateRibbonHelp)
	ON_UPDATE_COMMAND_UI(ID_RIBBON_TOKENMANAGER,	OnUpdateTokenManager)
	ON_COMMAND(ID_RIBBON_TOKENREFRESH,				OnTokenRefresh)
	ON_UPDATE_COMMAND_UI(ID_RIBBON_TOKENREFRESH,	OnUpdateTokenRefresh)
	ON_COMMAND(ID_RIBBON_TOKENLASTCONSUME,			OnTokenLastConsume)
	ON_UPDATE_COMMAND_UI(ID_RIBBON_TOKENLASTCONSUME, OnUpdateTokenLastConsume)
	ON_COMMAND(ID_RIBBON_TOKENBUY,					OnTokenBuy)
	ON_UPDATE_COMMAND_UI(ID_RIBBON_TOKENBUY,		OnUpdateTokenBuy)
	ON_COMMAND(XTP_ID_RIBBONCUSTOMIZE,				OnCustomizeQuickAccess)
	ON_COMMAND(ID_SENDFEEDBACKHAPPY,				OnSendFeedbackHappy)
	ON_UPDATE_COMMAND_UI(ID_SENDFEEDBACKHAPPY,		OnUpdateSendFeedbackHappy)
	ON_COMMAND(ID_SENDFEEDBACKANGRY,				OnSendFeedbackAngry)
	ON_UPDATE_COMMAND_UI(ID_SENDFEEDBACKANGRY,		OnUpdateSendFeedbackAngry)
	ON_COMMAND(ID_SENDFEEDBACKQUESTION,				OnSendFeedbackQuestion)
	ON_UPDATE_COMMAND_UI(ID_SENDFEEDBACKQUESTION,	OnUpdateSendFeedbackQuestion)
	ON_WM_WINDOWPOSCHANGED()
	ON_MESSAGE(WM_XTP_TOOLBARCONTEXTMENU, OnRibbonContextMenu)
	ON_MESSAGE(WM_XTP_MESSAGEBARCLOSED, OnMessageBarClosed)
END_MESSAGE_MAP()

template <class CodeJockFrameClass>
bool FrameBase<CodeJockFrameClass>::customizeStatusBar()
{
	static UINT indicators[] =
	{
		ID_SEPARATOR,           // status line indicator (text)
		ID_INDICATOR_CAPS,
		ID_INDICATOR_NUM,
		ID_INDICATOR_SCRL,
	};

	ASSERT(nullptr != getStatusBar());
	if (nullptr != getStatusBar())
		getStatusBar()->SetIndicators(indicators, sizeof(indicators) / sizeof(UINT));
	return true;
}

template <class CodeJockFrameClass>
CXTPRibbonBar* FrameBase<CodeJockFrameClass>::createRibbonBar()
{
	CXTPRibbonBar* myRibbonBar = nullptr;

	CXTPCommandBars* pCommandBars = GetCommandBars();

	// Remove default menu
	{
		CMenu menu;
		menu.Attach(::GetMenu(m_hWnd));
		SetMenu(nullptr);
	}

	// Configure tooltip context
	{
		CXTPToolTipContext* tooltipContext = pCommandBars->GetToolTipContext();

		tooltipContext->SetStyle(xtpToolTipOffice2013);
		tooltipContext->ShowTitleAndDescription();

		tooltipContext->SetMaxTipWidth(XTP_DPI_X(500));
		tooltipContext->SetFont(GetCommandBars()->GetPaintManager()->GetIconFont());

		tooltipContext->SetDelayTime(TTDT_INITIAL, 900);
	}

	myRibbonBar = dynamic_cast<CXTPRibbonBar*>(pCommandBars->Add(_YTEXT("Office365Ribbon"), xtpBarTop, RUNTIME_CLASS(CXTPRibbonBar)));
	if (nullptr == myRibbonBar)
		return myRibbonBar;

	myRibbonBar->EnableDocking(xtpFlagAlignTop);

	//myRibbonBar->GetQuickAccessControls()->CreateOriginalControls();
	myRibbonBar->ShowQuickAccess(TRUE);
	myRibbonBar->SetCloseable(FALSE);

	myRibbonBar->EnableFrameTheme();

	//Token Manager
	{
		m_TokenManagerControl = myRibbonBar->GetControls()->Add(xtpControlButtonPopup, ID_RIBBON_TOKENMANAGER);
		m_TokenManagerControl->SetStyle(xtpButtonIconAndCaption);
		m_TokenManagerControl->SetFlags(xtpFlagRightAlign);

		m_TokenStateControl = m_TokenManagerControl->GetCommandBar()->GetControls()->Add(xtpControlLabel, ID_RIBBON_TOKENSTATE);
		m_TokenManagerControl->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_RIBBON_TOKENLASTCONSUME);
		m_TokenManagerControl->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_RIBBON_TOKENREFRESH);
		m_TokenManagerControl->GetCommandBar()->GetControls()->Add(xtpControlButton, ID_RIBBON_TOKENBUY);

		UINT ids[] = { ID_RIBBON_TOKENMANAGER };
		pCommandBars->GetImageManager()->SetIcons(IDR_RIBBON_TOKENMANAGER, ids, _countof(ids), CSize(16, 16));

		ShowTokenMenu(Office365AdminApp::Get().GetLicenseManager()->GetCachedLicense().GetFeatureToken().IsAuthorized());
	}

	// Expand/minimize
	{
		CXTPControl* pControlExpand = myRibbonBar->GetControls()->Add(xtpControlButton, ID_RIBBON_EXPAND);
		pControlExpand->SetFlags(xtpFlagRightAlign);

		CXTPControl* pControlMinimize = myRibbonBar->GetControls()->Add(xtpControlButton, ID_RIBBON_MINIMIZE);
		pControlMinimize->SetFlags(xtpFlagRightAlign);

		UINT uiRibbonMinimize[] = { ID_RIBBON_EXPAND, ID_RIBBON_MINIMIZE };
		pCommandBars->GetImageManager()->SetIcons(IDR_RIBBON_MINIMIZE, uiRibbonMinimize, _countof(uiRibbonMinimize), CSize(16, 16));
	}

	// Help
	{
		CXTPControl* pControlHelp = myRibbonBar->GetControls()->Add(xtpControlButton, ID_RIBBON_HELP);
		pControlHelp->SetFlags(xtpFlagRightAlign);

		UINT ids[] = { ID_RIBBON_HELP };
		pCommandBars->GetImageManager()->SetIcons(IDR_RIBBON_HELP, ids, _countof(ids), CSize(16, 16));
	}

	// Needed for quick access toolbar
	pCommandBars->EnableCustomization(TRUE);
	myRibbonBar->AllowQuickAccessCustomization(TRUE);
	myRibbonBar->AllowQuickAccessDuplicates(FALSE);

	// Call it only if your Ribbon is support customization using Ribbon Customization page.
	//myRibbonBar->EnableCustomization(TRUE);

	vector<IRibbonSystemMenuBuilder::Item> menuItems;
	if (m_menuBuilder)
	{
		m_menuBuilder->GetMenuItems(menuItems);

		if (!menuItems.empty())
		{
			CXTPControlPopup* systemButton = myRibbonBar->AddSystemButton();
			systemButton->SetCaption(m_menuBuilder->GetTitle().c_str());
			if (m_menuBuilder->UseBackstageView())
			{
				// CXTPRibbonBackstageView* backstageView = CXTPRibbonBackstageView::CreateBackstageView(pCommandBars);
				RibbonBackstageView* backstageView = new RibbonBackstageView();
				backstageView->SetCommandBars(pCommandBars);

				for (const auto& item : menuItems)
				{
					if (item.m_id == IRibbonSystemMenuBuilder::ID_ITEM_SEPARATOR)
					{
						backstageView->AddMenuSeparator();
					}
					else
					{
						if (nullptr != item.m_backstagePage)
						{
							CXTPRibbonBackstageTab* tab = backstageView->AddTab(item.m_backstagePage, item.m_id);
							ASSERT(nullptr != tab);
							if (nullptr != tab)
							{
								tab->SetCaption(item.m_text.c_str());
								// useful?
								tab->SetFlags(xtpFlagManualUpdate); // No Need update with Update handler
							}
						}
						else
						{

							CXTPRibbonBackstageCommand* command = backstageView->AddCommand(item.m_id);
							ASSERT(nullptr != command);
							if (nullptr != command)
							{
								command->SetCaption(item.m_text.c_str());
								command->SetTooltip(item.m_text.c_str());
							}
						}
					}
				}

				backstageView->GetControls()->GetFirst()->SetItemDefault(TRUE);

				systemButton->SetCommandBar(backstageView);
				backstageView->InternalRelease();
			}
			else
			{
				systemButton->SetIconId(m_menuBuilder->GetSystemButtonIconID());
				UINT uCommand = { m_menuBuilder->GetSystemButtonIconID() };
				pCommandBars->GetImageManager()->SetIcons(m_menuBuilder->GetSystemButtonIconID(), &uCommand, 1, CSize(0, 0), xtpImageNormal);

				CXTPRibbonSystemPopupBar* systemCommandBar = new CXTPRibbonSystemPopupBar();
				systemCommandBar->SetCommandBars(pCommandBars);
				// Useful?
				//systemCommandBar->SetIconSize(CSize(36, 36));

				CMenu cMenu;
				MENUITEMINFO info;
				for (size_t i = 0; i < menuItems.size(); ++i)
				{
					const auto& item = menuItems[i];

					ASSERT(nullptr == item.m_backstagePage);

					ZeroMemory(&info, sizeof(MENUITEMINFO));
					info.cbSize = sizeof(MENUITEMINFO);
					info.fMask = MIIM_FTYPE;
					if (item.m_id == IRibbonSystemMenuBuilder::ID_ITEM_SEPARATOR)
					{
						info.fType = MFT_SEPARATOR;
					}
					else
					{
						info.fMask = info.fMask | MIIM_STRING | MIIM_ID;
						info.fType = MFT_STRING;
						info.dwTypeData = const_cast<wchar_t*>(item.m_text.c_str());
						info.wID = item.m_id;
					}

					cMenu.InsertMenuItem(static_cast<UINT>(i), &info, TRUE);
				}
				systemCommandBar->LoadMenu(&cMenu);

				systemButton->SetCommandBar(systemCommandBar);
				systemCommandBar->InternalRelease();
			}
		}
	}

	return myRibbonBar;
}

template <class CodeJockFrameClass>
bool FrameBase<CodeJockFrameClass>::showBackstageTab(const UINT tabID)
{
	if (nullptr != getRibbonBar().GetSystemButton())
	{
		CXTPRibbonBackstageView* backstage = dynamic_cast<CXTPRibbonBackstageView*>(getRibbonBar().GetSystemButton()->GetCommandBar());
		if (nullptr != backstage)
		{
			for (int i = 0; i < backstage->GetControlCount(); ++i)
			{
				CXTPRibbonBackstageTab* tab = dynamic_cast<CXTPRibbonBackstageTab*>(backstage->GetControl(i));
				if (nullptr != tab && tabID == tab->GetID())
				{
					getRibbonBar().SetPopuped(0);
					backstage->SetFocusedControl(tab);
					return true;
				}
			}
		}
	}

	return false;
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::initStaticStrings()
{
	if (!g_staticInit)
	{
		g_staticInit = true;

		g_InfoSessionGroup	= YtriaTranslate::Do(WindowsGrid_customizeGrid_5, _YLOC("Current Session")).c_str();
		g_FeedbackTabName	= YtriaTranslate::Do(GLOBAL_BEGIN_TEMPLATE_MESSAGE_MAP_2, _YLOC("Feedback")).c_str();
	}
}

template <class CodeJockFrameClass>
CXTPRibbonGroup* FrameBase<CodeJockFrameClass>::addSessionGroupToRibbonTab(CXTPRibbonTab& tab, bool addConnectedInfo)
{
	auto sessionGroup = tab.AddGroup(g_InfoSessionGroup.c_str());

	ASSERT(nullptr != sessionGroup);
	if (nullptr != sessionGroup)
	{
		setGroupImage(*sessionGroup, 0);

		auto pictureControl = new FixedSizeControlBitmap({ HIDPI_XW(64), HIDPI_YH(64) });
		sessionGroup->Add(pictureControl, getUserPictureID());
		pictureControl->SetIconId(getUserPictureID());

		sessionGroup->Add(xtpControlLabel, ID_USER_NAME);

		sessionGroup->Add(xtpControlLabel, ID_USER_MAIL);

		sessionGroup->Add(xtpControlLabel, ID_USER_DETAIL);

		if (addConnectedInfo)
		{
			sessionGroup->Add(xtpControlLabel, ID_CONNECTED);

			sessionGroup->SetControlsGrouping(TRUE);
			auto relogControl = sessionGroup->Add(xtpControlButton, ID_RELOG);
			setImage({ ID_RELOG }, IDB_RELOG, xtpImageNormal);
			setImage({ ID_RELOG }, IDB_RELOG_16X16, xtpImageNormal);
			relogControl->SetVisible(FALSE);
			setControlTexts(relogControl,
				YtriaTranslate::Do(GridSpGroups_customizeGrid_2, _YLOC("Login")).c_str(),
				YtriaTranslate::Do(GLOBAL_BEGIN_TEMPLATE_MESSAGE_MAP_4, _YLOC("Re-Open the session")).c_str(),
				YtriaTranslate::Do(GLOBAL_BEGIN_TEMPLATE_MESSAGE_MAP_5, _YLOC("Re-open the session used to create this module.")).c_str());
		}

		m_LogOutControl = sessionGroup->Add(xtpControlButton, ID_LOGOUT);
		setImage({ ID_LOGOUT }, IDB_LOGOUT, xtpImageNormal);
		setImage({ ID_LOGOUT }, IDB_LOGOUT_16X16, xtpImageNormal);

		updateLogoutControlTexts();

		// Controls are always enabled by default and OnUpdate not called while app is busy initializing...
		m_LogOutControl->SetEnabled(FALSE); // Bug #191210.RO.00B12E: Three buttons Sign out, Choose Role and Edit Ultra Admin should be disabled while launching sapio365

		if (addConnectedInfo)
			sessionGroup->SetControlsGrouping(FALSE);

		{
			m_CtrlEditElevated = sessionGroup->Add(xtpControlButton, ID_SESSION_ULTRAADMIN_EDIT);
			m_CtrlEditElevated->SetIconId(getEditUAorElevatedID());
			updateEditElevatedButton();
		}

		updateSessionGroup(*sessionGroup);
	}

	return sessionGroup;
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::updateEditElevatedButton()
{
	if (nullptr != m_CtrlEditElevated)
	{
		BOOL visible = FALSE;
		const UINT iconId = m_CtrlEditElevated->GetIconId();
		const auto s = GetSapio365Session();
		if (s)
		{
			if (s->IsAdvanced() || s->IsPartnerAdvanced())
			{
				setImage({ iconId }, IDB_ELEVATE, xtpImageNormal);
				setImage({ iconId }, IDB_ELEVATE_16X16, xtpImageNormal);
				setControlTexts(m_CtrlEditElevated, _T("Elevate Privileges"), _T("Add elevated privileges"), _T("Promote an Advanced session with elevated privileges.\r\nAdding elevated privileges to an advanced session will grant access to all personal data for calendars, mailboxes, drive items, etc..."));
				visible = TRUE;
			}
			else if (s->IsElevated() || s->IsPartnerElevated())
			{
				setImage({ iconId }, IDB_MANAGE_ELEVATED, xtpImageNormal);
				setImage({ iconId }, IDB_MANAGE_ELEVATED_16X16, xtpImageNormal);
				setControlTexts(m_CtrlEditElevated, _T("Manage Privileges"), _T("Manage elevated privileges"), _T("Edit the elevated privileges of this session."));
				visible = TRUE;
			}
			else if (s->IsUltraAdmin())
			{
				setImage({ iconId }, IDB_EDIT, xtpImageNormal);
				setImage({ iconId }, IDB_EDIT_16X16, xtpImageNormal);
				setControlTexts(m_CtrlEditElevated, _T("Edit Ultra Admin"), _T("Edit Ultra Admin info."), _T("This will edit and reload the Ultra Admin current session."));
				visible = TRUE;
			}
		}

		m_CtrlEditElevated->SetVisible(visible);
	}
}

template <class CodeJockFrameClass>
CXTPControl* FrameBase<CodeJockFrameClass>::getCtrlEditElevated()
{
	return m_CtrlEditElevated;
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::updateConnectedControl(CXTPControl& connectedControl)
{
	// Should be overridden in your child class.
	ASSERT(false);
}


template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::updateSessionGroup(CXTPRibbonGroup& sessionGroup)
{
	// User picture
	{
		CXTPCommandBars* pCommandBars = GetCommandBars();

		ASSERT(nullptr != pCommandBars);
		if (nullptr != pCommandBars)
		{
			// Otherwise, some previous user picture stays on screen in HiDPI :-o
			pCommandBars->GetImageManager()->RemoveIcon(getUserPictureID());

			if (GetSessionInfo().IsValid())
			{
				bool hasUserPicture = false;
				if (GetSessionInfo().GetProfilePhoto().size() > 0)
				{
					auto userPicture = MFCUtil::LoadJpegUsingTempFile(&(GetSessionInfo().GetProfilePhoto().front()), GetSessionInfo().GetProfilePhoto().size());
					if (nullptr != userPicture)
					{
						hasUserPicture = true;

						int overlayResourceID = 0;
						if (GetSessionInfo().IsStandard())
							overlayResourceID = IDB_BASIC_PICTURE_OVERLAY;
						else if (GetSessionInfo().IsElevated())
							overlayResourceID = IDB_FULL_PICTURE_OVERLAY;
						else if (GetSessionInfo().IsAdvanced())
							overlayResourceID = IDB_ADMIN_PICTURE_OVERLAY;
						else if (GetSessionInfo().IsRole())
						{
							if (RoleDelegationManager::GetInstance().IsRoleEnforced(GetSessionInfo().GetRoleId(), GetSapio365Session()))
								overlayResourceID = IDB_ROLE_ENFORCED_PICTURE_OVERLAY;
							else
								overlayResourceID = IDB_ROLE_PICTURE_OVERLAY;
						}
						else
						{
							ASSERT(false);
							overlayResourceID = IDB_ADMIN_PICTURE_OVERLAY;
						}

						CBitmap* resizedAndOverlayedUserPicture = nullptr;
						{
							// FIXME: This resizing routine is really crap. Get a better one (FreeImage for instance https://sourceforge.net/projects/freeimage/)
							auto resizedUserPicture = MFCUtil::ResizeBitmap(*userPicture, HIDPI_XW(64), HIDPI_YH(64), this);
							resizedAndOverlayedUserPicture = MFCUtil::AddOverlay(resizedUserPicture, overlayResourceID, RGB(0, 0, 0), this, resizedUserPicture != userPicture);
							resizedUserPicture = nullptr;
						}

						vector<UINT> commandIDs{ getUserPictureID() };
						pCommandBars->GetImageManager()->SetIcons(*resizedAndOverlayedUserPicture, (UINT*)&commandIDs.front(), static_cast<int>(commandIDs.size()), CSize(0, 0), xtpImageNormal);

						// If the image has been resized, delete it.
						if (resizedAndOverlayedUserPicture != userPicture)
							delete resizedAndOverlayedUserPicture;
					}
					delete userPicture;
				}

				if (!hasUserPicture)
				{
                    if (GetSessionInfo().IsUltraAdmin())
                        setImage({ getUserPictureID() }, IDB_FULL_ADMIN_PICTURE, xtpImageNormal);
                    else if (GetSessionInfo().IsStandard())
                        setImage({ getUserPictureID() }, IDB_DEFAULT_BASIC_USER_PICTURE, xtpImageNormal);
					else if (GetSessionInfo().IsElevated())
						setImage({ getUserPictureID() }, IDB_DEFAULT_FULL_PICTURE, xtpImageNormal);
					else if (GetSessionInfo().IsPartnerAdvanced())
						setImage({ getUserPictureID() }, IDB_DEFAULT_PARTNER_PICTURE, xtpImageNormal);
					else if (GetSessionInfo().IsPartnerElevated())
						setImage({ getUserPictureID() }, IDB_DEFAULT_PARTNER_ELEVATED_PICTURE, xtpImageNormal);
					else if (GetSessionInfo().IsRole())
					{
						if (RoleDelegationManager::GetInstance().IsRoleEnforced(GetSessionInfo().GetRoleId(), GetSapio365Session()))
						{
							setImage({ getUserPictureID() }, IDB_DEFAULT_ROLE_PICTURE_ENFORCED, xtpImageNormal);
							setImage({ ID_SESSION_SEE_ROLEINFO }, IDB_SESSION_SEE_ROLEINFO_ENFORCED, xtpImageNormal);
							setImage({ ID_SESSION_SEE_ROLEINFO }, IDB_SESSION_SEE_ROLEINFO_ENFORCED_16X16, xtpImageNormal);
						}
						else
						{
							setImage({ getUserPictureID() }, IDB_DEFAULT_ROLE_PICTURE, xtpImageNormal);
							setImage({ ID_SESSION_SEE_ROLEINFO }, IDB_SESSION_SEE_ROLEINFO, xtpImageNormal);
							setImage({ ID_SESSION_SEE_ROLEINFO }, IDB_SESSION_SEE_ROLEINFO_16X16, xtpImageNormal);
						}
					}
					else
					{
						ASSERT(GetSessionInfo().IsAdvanced());
						setImage({ getUserPictureID() }, IDB_DEFAULT_USER_PICTURE, xtpImageNormal);
					}
				}
			}
			else
			{
				setImage({ getUserPictureID() }, IDB_NO_SESSION_PICTURE, xtpImageNormal);
			}
		}
	}

	auto nameControl = sessionGroup.FindControl(ID_USER_NAME);
	ASSERT(nullptr != nameControl);
    if (nullptr != nameControl)
    {
		if (GetSessionInfo().IsValid())
		{
            if (GetSessionInfo().IsUltraAdmin())
            {
				nameControl->SetCaption(GetSessionInfo().GetTenantDisplayName().c_str());
            }
            else
            {
                if (0 != GetSessionInfo().GetRoleId())
                {
                    const auto roleName = _YTEXTFORMAT(L"[ %s ]", GetSessionInfo().GetRoleNameInDb().c_str());
                    nameControl->SetCaption(roleName.c_str());
                }
                else
                {
				    nameControl->SetCaption(GetSessionInfo().GetFullname().c_str());
                }
            }
		}
		else
		{
			nameControl->SetCaption(_YTEXT(""));
		}
    }

	auto mailControl = sessionGroup.FindControl(ID_USER_MAIL);
	ASSERT(nullptr != mailControl);
	if (nullptr != mailControl)
	{
		if (GetSessionInfo().IsValid())
		{
			if (GetSessionInfo().GetSessionType() != SessionTypes::g_UltraAdmin)
				mailControl->SetCaption(GetSessionInfo().GetEmailOrAppId().c_str());
			else
				mailControl->SetCaption(GetSessionInfo().GetSessionName().c_str());
		}
		else
			mailControl->SetCaption(YtriaTranslate::Do(GLOBAL_BEGIN_TEMPLATE_MESSAGE_MAP_9, _YLOC("No session currently loaded.")).c_str());
	}

	auto detailControl = sessionGroup.FindControl(ID_USER_DETAIL);
	ASSERT(nullptr != detailControl);
	if (nullptr != detailControl)
	{
		if (!GetSessionInfo().GetSessionType().empty())
			detailControl->SetCaption(SessionTypes::GetDisplayedSessionType(GetSessionInfo().GetSessionType()).c_str());
		else
			detailControl->SetCaption(_YTEXT(""));
	}

	auto connectedControl = sessionGroup.FindControl(ID_CONNECTED);
	if (nullptr != connectedControl)
		updateConnectedControl(*connectedControl);
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::Init()
{
	initStaticStrings();
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::SetAutomationName(const wstring& p_AutomationName)
{
	m_AutomationName = p_AutomationName;
}

template <class CodeJockFrameClass>
const wstring& FrameBase<CodeJockFrameClass>::GetAutomationName() const
{
	return m_AutomationName;
}

template <class CodeJockFrameClass>
BOOL FrameBase<CodeJockFrameClass>::OnFrameLoaded()
{
	if (CodeJockFrameClass::OnFrameLoaded())
	{
		if (m_ShouldRestorePositionAndSize)
			restorePositionAndSize();
		return TRUE;
	}

	return FALSE;
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnClose()
{
	storePositionAndSize();

	CodeJockFrameClass::OnClose();
}


template <class CodeJockFrameClass>
const wstring FrameBase<CodeJockFrameClass>::g_WindowPlacementFormat = _YTEXT("%d,%d,%d,%d,%d,%d");
template <class CodeJockFrameClass>
const wstring FrameBase<CodeJockFrameClass>::g_PositionAndSizeRegistryKey = _YTEXT("XY");

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::storePositionAndSize()
{
	auto app = AfxGetApp();
	ASSERT(nullptr != app);
	if (nullptr != app && ::IsWindow(GetSafeHwnd()))
	{
		WINDOWPLACEMENT wp;
		::memset(&wp, 0, sizeof(WINDOWPLACEMENT));
		wp.length = sizeof(WINDOWPLACEMENT);
		::GetWindowPlacement(GetSafeHwnd(), &wp);
		CString formattedRegValue;
		formattedRegValue.Format(g_WindowPlacementFormat.c_str()
			, MFCUtil::RevertHiDPI_XW(wp.rcNormalPosition.left)
			, MFCUtil::RevertHiDPI_YH(wp.rcNormalPosition.top)
			, MFCUtil::RevertHiDPI_XW(wp.rcNormalPosition.right)
			, MFCUtil::RevertHiDPI_YH(wp.rcNormalPosition.bottom)
			, wp.showCmd
			, wp.flags);
		auto res = app->WriteProfileString(getPositionAndSizeRegistrySectionName().c_str(), g_PositionAndSizeRegistryKey.c_str(), formattedRegValue.GetBuffer());
		ASSERT(res);
	}
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::restorePositionAndSize()
{
	auto app = AfxGetApp();
	ASSERT(nullptr != app);
	if (nullptr != app)
	{
		const wstring windowPlacementString = app->GetProfileString(getPositionAndSizeRegistrySectionName().c_str(), g_PositionAndSizeRegistryKey.c_str());

		bool proceed = false;
		try
		{
			proceed = RegexManager::matchRegex(false, _YTEXT("^[0-9,-]+$"), windowPlacementString, false); // Ensure we have comma separated numbers
		}
		catch (RegexException re)
		{

		}

		if (proceed)
		{
			DWORD wpleft, wptop, wpright, wpbottom, wpshowcmd, wpflags;
			::swscanf(windowPlacementString.c_str(), g_WindowPlacementFormat.c_str(), &wpleft, &wptop, &wpright, &wpbottom, &wpshowcmd, &wpflags);

			WINDOWPLACEMENT wp;
			::memset(&wp, 0, sizeof(WINDOWPLACEMENT));
			::GetWindowPlacement(GetSafeHwnd(), &wp);
			wp.rcNormalPosition.left	= MFCUtil::HiDPI_XW(wpleft);
			wp.rcNormalPosition.top		= MFCUtil::HiDPI_YH(wptop);
			wp.rcNormalPosition.right	= MFCUtil::HiDPI_XW(wpright);
			wp.rcNormalPosition.bottom	= MFCUtil::HiDPI_YH(wpbottom);
			wp.showCmd					= SW_SHOWNORMAL ;//wpshowcmd;
			wp.flags					= wpflags;

			::SetWindowPlacement(GetSafeHwnd(), &wp);
		}
	}
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::setImage(const vector<UINT> commandIDs, UINT resourceID, XTPImageState imageState/* = xtpImageNormal*/)
{
	CXTPCommandBars* pCommandBars = GetCommandBars();
	ASSERT(nullptr != pCommandBars);
	if (nullptr != pCommandBars)
		pCommandBars->GetImageManager()->SetIcons(resourceID, (UINT*)&commandIDs.front(), static_cast<int>(commandIDs.size()), CSize(0, 0), imageState);
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::setGroupImage(CXTPRibbonGroup& p_Group, UINT resourceID)
{
	if (0 != resourceID)
		setImage({ resourceID }, resourceID, xtpImageNormal);

	// FIXME: The first line should work but it doesn't (bug in codejock)
	//p_Group->SetIconId(resourceID);
	ASSERT(p_Group.GetControlGroupPopup() != nullptr);
	p_Group.GetControlGroupPopup()->SetIconId(resourceID);
}

template <class CodeJockFrameClass>
CXTPRibbonGroup* FrameBase<CodeJockFrameClass>::findGroup(CXTPRibbonTab& tab, const CString& name)
{
	CXTPRibbonGroups * groups = tab.GetGroups();
	if (nullptr != groups)
	{
		for (int i = 0; i < groups->GetCount(); ++i)
		{
			auto group = groups->GetAt(i);
			if (nullptr != group && group->GetCaption() == name)
				return group;
		}
	}

	return nullptr;
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateLogout(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(IsConnected() && !AutomatedApp::IsAutomationRunning() && !TaskDataManager::Get().HasTaskRunning());
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnLogout()
{
	logout();
	WindowsListUpdater::GetInstance().Update();
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateSeeRoleInfo(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!AutomatedApp::IsAutomationRunning() && HasSession() && 0 != GetSessionInfo().GetRoleId());
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnSeeRoleInfo()
{
    if (0 != GetSessionInfo().GetRoleId())
    {
        DlgRoleSeeInfo dlgRoleSeeInfo(GetSessionInfo().GetRoleId(), GetSapio365Session(), this);
        dlgRoleSeeInfo.DoModal();
    }
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnSendFeedbackHappy()
{
	sendFeedback(DlgFeedback::MOOD::HAPPY);
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateSendFeedbackHappy(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnSendFeedbackAngry()
{
	sendFeedback(DlgFeedback::MOOD::ANGRY);
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateSendFeedbackAngry(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnSendFeedbackQuestion()
{
	sendFeedback(DlgFeedback::MOOD::QUESTION);
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateSendFeedbackQuestion(CCmdUI* pCmdUI)
{
	pCmdUI->Enable();
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::sendFeedback(DlgFeedback::MOOD mood)
{
	Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
	ASSERT(nullptr != app);

	if (nullptr != app)
	{
		bool done = false;
		wstring userText;
		while (!done)
		{
			DlgFeedback dlg(userText, mood, this);
			const auto res = dlg.DoModal();

			if (IDCANCEL == res)
			{
				done = true;
			}
			else
			{
				CString windowTitle;
				GetWindowText(windowTitle);
				userText = dlg.GetText();
				const auto error = app->SendFeedback(DlgFeedback::MOOD::HAPPY == mood ? _YTEXT("happy") : DlgFeedback::MOOD::ANGRY == mood ? _YTEXT("angry") : _YTEXT("question"), userText, (LPCTSTR)windowTitle);
				if (error.empty())
				{
					YCodeJockMessageBox msgBox(this
						, DlgMessageBox::eIcon_Information
						, YtriaTranslate::Do(GLOBAL_BEGIN_TEMPLATE_MESSAGE_MAP_10, _YLOC("Thank you!")).c_str()
						, _YTEXT("Your feedback has successfully been sent to Ytria.")
						, _YTEXT("")
						, { {IDOK, YtriaTranslate::Do(AutomatedApp_ProcessWndProcException_21, _YLOC("Close")).c_str()} });
					msgBox.DoModal();
					done = true;
				}
				else
				{
					YCodeJockMessageBox msgBox(this
						, DlgMessageBox::eIcon_Error
						, YtriaTranslate::Do(GLOBAL_BEGIN_TEMPLATE_MESSAGE_MAP_12, _YLOC("Feedback has not been sent.")).c_str()
						, _T("An error occurred:")
						, error
						, { { IDOK, YtriaTranslate::Do(BackstagePanelLog_OnSend_5, _YLOC("Try Again")).c_str() }
							,{ IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
					if (IDCANCEL == msgBox.DoModal())
						done = true;
				}
			}
		}
	}
}


template <class CodeJockFrameClass>
constexpr
UINT FrameBase<CodeJockFrameClass>::getUserPictureID() const
{
	return m_userPictureId;
}

template <class CodeJockFrameClass>
constexpr UINT FrameBase<CodeJockFrameClass>::getEditUAorElevatedID() const
{
	return m_EditUAorElevatedID;
}

template <class CodeJockFrameClass>
bool FrameBase<CodeJockFrameClass>::hasFreeIdForNewFrame()
{
	return PreAllocatedIDS::HasFreeUserPictureID();
}

struct PreAllocatedIDS
{
public:
    static UINT GetFreeGroupPolicyIconID()
    {
        UINT index = 0;

        while (g_usedGroupPolicyIconIds.end() != g_usedGroupPolicyIconIds.find(g_minGroupPolicyIconID + index))
            ++index;

        if (g_maxGroupPolicyIconID >= g_minGroupPolicyIconID + index)
        {
            g_usedGroupPolicyIconIds.insert(g_minGroupPolicyIconID + index);
            return g_minGroupPolicyIconID + index;
        }

        ASSERT(false);
        return 0xffffffff;
    }

    static void ReleaseGroupPolicyIconID(UINT id)
    {
        ASSERT(g_maxGroupPolicyIconID >= id);
        if (g_maxGroupPolicyIconID >= id)
            g_usedGroupPolicyIconIds.erase(id);
    }

    static bool HasFreeGroupPolicyIconID()
    {
        return g_usedGroupPolicyIconIds.size() < (g_maxGroupPolicyIconID - g_minGroupPolicyIconID + 1);
    }

	static UINT GetFreeUserPictureID()
	{
		UINT index = 0;

		while (g_usedPictureIds.end() != g_usedPictureIds.find(g_minUserPictureID + index))
			++index;

		if (g_maxUserPictureID >= g_minUserPictureID + index)
		{
			g_usedPictureIds.insert(g_minUserPictureID + index);
			return g_minUserPictureID + index;
		}

		// You're trying to instantiate more frame than the maximum allowed (60).
		// If you want to be able to to this, you must reserve more ID_USER_PICTURE_CONTAINERxx ids in resource.h
		// and use the new max ID for g_maxUserPictureID.
		ASSERT(false);
		return 0xffffffff;
	}

	static void ReleaseUserPictureID(UINT id)
	{
		ASSERT(g_maxUserPictureID >= id);
		if (g_maxUserPictureID >= id)
			g_usedPictureIds.erase(id);
	}

	static bool HasFreeUserPictureID()
	{
		return g_usedPictureIds.size() < (g_maxUserPictureID - g_minUserPictureID + 1);
	}

	static UINT GetFreeEditUAorElevatedID()
	{
		UINT index = 0;

		while (g_usedEditUAorElevatedIds.end() != g_usedEditUAorElevatedIds.find(g_minEditUAorElevatedIconID + index))
			++index;

		if (g_maxEditUAorElevatedIconID >= g_minEditUAorElevatedIconID + index)
		{
			g_usedEditUAorElevatedIds.insert(g_minEditUAorElevatedIconID + index);
			return g_minEditUAorElevatedIconID + index;
		}

		// You're trying to instantiate more frame than the maximum allowed (60).
		// If you want to be able to to this, you must reserve more ID_EDIT_UA_OR_ELEVATEDxx ids in resource.h
		// and use the new max ID for g_maxEditUAorElevatedIconID.
		ASSERT(false);
		return 0xffffffff;
	}

	static void ReleaseEditUAorElevatedID(UINT id)
	{
		ASSERT(g_maxEditUAorElevatedIconID >= id);
		if (g_maxEditUAorElevatedIconID >= id)
			g_usedEditUAorElevatedIds.erase(id);
	}

	static bool HasFreeEditUAorElevatedID()
	{
		return g_usedEditUAorElevatedIds.size() < (g_maxEditUAorElevatedIconID - g_minEditUAorElevatedIconID + 1);
	}

private:
	static const UINT g_minUserPictureID = ID_USER_PICTURE_CONTAINER0;
	static const UINT g_maxUserPictureID = ID_USER_PICTURE_CONTAINER59;

    static const UINT g_minGroupPolicyIconID = ID_GROUP_POLICY0;
    static const UINT g_maxGroupPolicyIconID = ID_GROUP_POLICY39;

	static const UINT g_minEditUAorElevatedIconID = ID_EDIT_UA_OR_ELEVATED0;
	static const UINT g_maxEditUAorElevatedIconID = ID_EDIT_UA_OR_ELEVATED59;

	static set<UINT> g_usedPictureIds;
    static set<UINT> g_usedGroupPolicyIconIds;
	static set<UINT> g_usedEditUAorElevatedIds;
};

template <class CodeJockFrameClass>
FrameBase<CodeJockFrameClass>::~FrameBase()
{
	PreAllocatedIDS::ReleaseUserPictureID(getUserPictureID());
	PreAllocatedIDS::ReleaseEditUAorElevatedID(getEditUAorElevatedID());
}

template <class CodeJockFrameClass>
UINT FrameBase<CodeJockFrameClass>::initUserPictureID()
{
	return PreAllocatedIDS::GetFreeUserPictureID();
}

template <class CodeJockFrameClass>
UINT FrameBase<CodeJockFrameClass>::initEditUAorElevatedID()
{
	return PreAllocatedIDS::GetFreeEditUAorElevatedID();
}

template <class CodeJockFrameClass>
bool FrameBase<CodeJockFrameClass>::CanCreateNewFrame(bool warnIfNot, CWnd* parentForDlg, AutomationAction* p_Action, bool p_HasFreeIdForNewFrame/* = hasFreeIdForNewFrame()*/)
{
	if (!p_HasFreeIdForNewFrame)
	{
		if (!warnIfNot)
		{
			YCodeJockMessageBox msgBox (parentForDlg
										, DlgMessageBox::eIcon_Error
										, YtriaTranslate::Do(GLOBAL_hasFreeIdForNewFrame_2, _YLOC("Cannot create new frame.")).c_str()
										, YtriaTranslate::Do(GLOBAL_hasFreeIdForNewFrame_3, _YLOC("Too many frames are opened. Please close one and retry.")).c_str()
										, _YTEXT("")
										, { {IDOK, YtriaTranslate::Do(CosmosUtil__detail_DeleteCosmosDbAccountImpl_1, _YLOC("OK")).c_str()} });

			msgBox.DoModal();
		}

		ModuleBase::SetAutomationGreenLight(p_Action, YtriaTranslate::Do(GLOBAL_hasFreeIdForNewFrame_3, _YLOC("Too many frames are opened. Please close one and retry.")).c_str());
	}

	return p_HasFreeIdForNewFrame;
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateRibbonExpand(CCmdUI* pCmdUI)
{
	CXTPControl* pControl = CXTPControl::FromUI(pCmdUI);
	if (nullptr != pControl)
		pControl->SetVisible(hasRibbonBar() && getRibbonBar().IsRibbonMinimized());
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateRibbonMinimize(CCmdUI* pCmdUI)
{
	CXTPControl* pControl = CXTPControl::FromUI(pCmdUI);
	if (nullptr != pControl)
		pControl->SetVisible(hasRibbonBar() && !getRibbonBar().IsRibbonMinimized());
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnToggleRibbon()
{
	getRibbonBar().SetRibbonMinimized(!getRibbonBar().IsRibbonMinimized());
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateRibbonHelp(CCmdUI* pCmdUI)
{
	// Anything?
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnRibbonHelp()
{
	Product::getInstance().gotoAbsoluteUrl(_YTEXT("https://help.ytria.com/sapio365"));
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateTokenManager(CCmdUI* pCmdUI)
{
	const auto tokenState = Office365AdminApp::Get().GetLicenseTokenState(false);
	pCmdUI->Enable(tokenState.is_initialized());
	pCmdUI->SetText(_T("Tokens"));
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateTokenRefresh(CCmdUI* pCmdUI)
{
	const auto tokenState = Office365AdminApp::Get().GetLicenseTokenState(true);
	ASSERT(tokenState); // This control should only exist if top menu is enabled, i.e. if license has tokens.

	// Anything?
	pCmdUI->Enable(tokenState.is_initialized());
	pCmdUI->SetText(_T("Refresh License Information"));

	// This control being a label, there's no OnUpdate.
	// As it's only displayed along with "token refresh" menu, update it in the same time.
	if (nullptr != m_TokenStateControl)
	{
		if (tokenState)
		{
			m_TokenStateControl->SetCaption(YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_23, _YLOC("Activated: %1 token(s) - Remaining: %2 token(s)"),
				Str::getStringFromNumber(tokenState->total).c_str(),
				tokenState->available != LicenseUtil::g_NoValueUINT32 ?
				Str::getStringFromNumber(tokenState->available).c_str() :
				YtriaTranslate::Do(BackstagePanelAbout_updateLicenseInfo_25, _YLOC("[unknown number]")).c_str()).c_str()
			);
		}
		else
		{
			// shouldn't be ever displayed
			ASSERT(false);
			m_TokenStateControl->SetCaption(_YTEXT("Unlimited tokens"));
		}
	}
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnTokenRefresh()
{
	Office365AdminApp::Get().GetRefreshedLicense(true);
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateTokenLastConsume(CCmdUI* pCmdUI)
{
	const auto tokenState = Office365AdminApp::Get().GetLicenseTokenState(false);
	ASSERT(tokenState); // This control should only exist if top menu is enabled, i.e. if license has tokens.
	pCmdUI->Enable(tokenState.is_initialized());
	pCmdUI->SetText(_T("Token consumption history (current session)"));
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnTokenLastConsume()
{
	DlgTokenHistory dlg(Office365AdminApp::Get().GetLicenseManager(), this);
	dlg.DoModal();
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnUpdateTokenBuy(CCmdUI* pCmdUI)
{
	const auto tokenState = Office365AdminApp::Get().GetLicenseTokenState(false);
	ASSERT(tokenState); // This control should only exist if top menu is enabled, i.e. if license has tokens.
	pCmdUI->Enable(tokenState.is_initialized());
	pCmdUI->SetText(_T("Buy tokens"));
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnTokenBuy()
{
	// You must handle this message in child class
	ASSERT(false);
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::ShowTokenMenu(bool p_Show)
{
	ASSERT(nullptr != m_TokenManagerControl);
	m_TokenManagerControl->SetVisible(p_Show);
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnCustomizeQuickAccess()
{
	// Shouldn't be called, we remove the menu item in OnRibbonContextMenu
	// Finish the work (New Dev #171012.SR.006672: Really add ribbon quick access toolbar.) and remove assert ;)
	ASSERT(false);

	// TODO: Implement me.
	CXTPCustomizeSheet cs(GetCommandBars());

	CXTPRibbonCustomizeQuickAccessPage pageQuickAccess(&cs);
	cs.AddPage(&pageQuickAccess);

	CXTPRibbonBar& ribbon = getRibbonBar();

	for (int i = 0; i < ribbon.GetTabCount(); ++i)
	{
		CXTPRibbonTab* tab = ribbon.GetTab(i);

		CXTPControls* pCategoryControls = pageQuickAccess.InsertCategory(tab->GetCaption());

		CXTPRibbonGroups* groups = tab->GetGroups();
		for (int j = 0; j < groups->GetCount(); ++j)
		{
			CXTPRibbonGroup* group = groups->GetAt(j);
			for (int k = 0; k < group->GetCount(); ++k)
			{
				CXTPControl* control = group->GetAt(k);
				pCategoryControls->AddClone(control);
			}
		}
		
	}

	cs.DoModal();
}

template <class CodeJockFrameClass>
CXTPRibbonTab* FrameBase<CodeJockFrameClass>::addFeedbackRibbonTab(CXTPRibbonBar& p_RibbonBar)
{
	auto betaRibbonTab = p_RibbonBar.AddTab(g_FeedbackTabName.c_str());
	ASSERT(nullptr != betaRibbonTab);
	if (nullptr != betaRibbonTab)
	{
		auto group = betaRibbonTab->AddGroup(L"");

		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

            auto label = dynamic_cast<CXTPControlMarkupLabel*>(group->Add(new CXTPControlMarkupLabel, ID_HIERARCHY_FILTERS_LABEL));
            if (nullptr != label)
            {
#define FEEDBACK_CAPTION_XAML \
				L"<StackPanel Margin='5, 3, 5, 3'>" \
				"<TextBlock><Bold>%s</Bold></TextBlock>" \
				"<TextBlock TextWrapping='Wrap' Foreground='#6f6f6f' Margin='0, 3, 0, 0' Width='200'>%s</TextBlock>" \
				"</StackPanel>"

                CString caption;
                caption.Format(FEEDBACK_CAPTION_XAML, YtriaTranslate::Do(GLOBAL_BEGIN_TEMPLATE_MESSAGE_MAP_2, _YLOC("Feedback")).c_str(), YtriaTranslate::Do(GLOBAL_hasFreeIdForNewFrame_7, _YLOC("Send us feedback to help us improve sapio365.")).c_str());
                label->SetCaption(caption);
            }

			auto happyFeedbackControl = group->Add(xtpControlButton, ID_SENDFEEDBACKHAPPY);
			setImage({ ID_SENDFEEDBACKHAPPY }, IDB_FEEDBACK_HAPPY, xtpImageNormal);
			setImage({ ID_SENDFEEDBACKHAPPY }, IDB_FEEDBACK_HAPPY_16X16, xtpImageNormal);
			setControlTexts(happyFeedbackControl,
						YtriaTranslate::Do(GLOBAL_hasFreeIdForNewFrame_8, _YLOC("I Like...")).c_str(),
						YtriaTranslate::Do(GLOBAL_hasFreeIdForNewFrame_9, _YLOC("Tell us what worked for you")).c_str(),
						_YTEXT(""));

			auto angryFeedbackControl = group->Add(xtpControlButton, ID_SENDFEEDBACKANGRY);
			setImage({ ID_SENDFEEDBACKANGRY }, IDB_FEEDBACK_ANGRY, xtpImageNormal);
			setImage({ ID_SENDFEEDBACKANGRY }, IDB_FEEDBACK_ANGRY_16X16, xtpImageNormal);
			setControlTexts(angryFeedbackControl,
				YtriaTranslate::Do(GLOBAL_hasFreeIdForNewFrame_10, _YLOC("I Don't Like...")).c_str(),
				YtriaTranslate::Do(GLOBAL_hasFreeIdForNewFrame_11, _YLOC("Tell us what doesn't work for you")).c_str(),
				_YTEXT(""));

			auto questionFeedbackControl = group->Add(xtpControlButton, ID_SENDFEEDBACKQUESTION);
			setImage({ ID_SENDFEEDBACKQUESTION }, IDB_FEEDBACK_QUESTION, xtpImageNormal);
			setImage({ ID_SENDFEEDBACKQUESTION }, IDB_FEEDBACK_QUESTION_16X16, xtpImageNormal);
			setControlTexts(questionFeedbackControl,
				YtriaTranslate::Do(GLOBAL_hasFreeIdForNewFrame_12, _YLOC("Question...")).c_str(),
				YtriaTranslate::Do(GLOBAL_hasFreeIdForNewFrame_13, _YLOC("Ask us anything about sapio365")).c_str(),
				_YTEXT(""));
		}
	}

	return betaRibbonTab;
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::setControlTexts(CXTPControl* p_Control, const wstring& p_Caption, const wstring& p_TooltipTitle, const wstring& p_Description)
{
	ASSERT(nullptr != p_Control);
	if (nullptr != p_Control)
	{
		p_Control->SetCaption(p_Caption.c_str());			// Button caption in ribbon
		p_Control->SetTooltip(p_TooltipTitle.c_str());		// Tooltip's title (displayed in bold)
		if (p_TooltipTitle == p_Description) // If both are equal, description is ignored in tooltip. Add a space to trick the system.
			p_Control->SetDescription((p_Description + _YTEXT(" ")).c_str()); // Used as description in tooltip and displayed in status bar when hovering
		else
			p_Control->SetDescription(p_Description.c_str()); // Used as description in tooltip and displayed in status bar when hovering
	}
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::setDoNotRestorePositionAndSize(bool doNot)
{
	m_ShouldRestorePositionAndSize = !doNot;
}

template <class CodeJockFrameClass>
std::shared_ptr<Sapio365Session> FrameBase<CodeJockFrameClass>::GetSapio365Session() const
{
	return m_sapio365Session;
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::SetSapio365Session(const std::shared_ptr<Sapio365Session>& p_O365GraphSession)
{
	const bool sessionChanged = m_sapio365Session != p_O365GraphSession;

	if (sessionChanged)
		Sapio365Session::CancelAsyncUserCountTaskIfNeeded(m_sapio365Session);

	m_sapio365Session = p_O365GraphSession;
	
	if (!m_sapio365Session || !m_sapio365Session->IsConnected())
		ClearLicenseContext();

	ASSERT(!m_sapio365Session || SessionsSqlEngine::Get().SessionExists(m_sapio365Session->GetIdentifier()));
	if (!m_sapio365Session || !SessionsSqlEngine::Get().SessionExists(m_sapio365Session->GetIdentifier()))
	{
		m_SessionInfo = PersistentSession();
	}
	else
	{
		PersistentSession loadedSession = SessionsSqlEngine::Get().Load(m_sapio365Session->GetIdentifier());
		ASSERT(loadedSession.IsValid());
		if (loadedSession.IsValid())
		{
			m_SessionInfo = loadedSession;
			if (nullptr != p_O365GraphSession)
				m_SessionInfo.SetRoleId(p_O365GraphSession->GetRoleDelegationID());
			else
				m_SessionInfo.SetRoleId(0);
			const auto org = m_sapio365Session->GetGraphCache().GetCachedOrganization();
			if (org)
				m_SessionInfo.SetTenant(*org);
			if (!m_SessionInfo.IsUltraAdmin() && !m_SessionInfo.IsPartnerAdvanced() && !m_SessionInfo.IsPartnerElevated())
			{
				auto user = m_sapio365Session->GetGraphCache().GetCachedConnectedUser();
				ASSERT(user);
				if (user)
					m_SessionInfo.SetConnectedUserId(user->m_Id);
			}
		}
	}

	if (m_sapio365Session)
		m_sapio365Session->GetGraphCache().SetPersistentSession(m_SessionInfo);

	sessionWasSet(sessionChanged);
}

template <class CodeJockFrameClass>
bool FrameBase<CodeJockFrameClass>::IsConnected() const
{
	return m_sapio365Session && m_sapio365Session->IsConnected();
}

template <class CodeJockFrameClass>
bool FrameBase<CodeJockFrameClass>::HasSession() const
{
	return (bool)m_sapio365Session;
}

template <class CodeJockFrameClass>
PersistentSession& FrameBase<CodeJockFrameClass>::getSessionInfo()
{
	return m_SessionInfo;
}

template <class CodeJockFrameClass>
const PersistentSession& FrameBase<CodeJockFrameClass>::GetSessionInfo() const
{
	return m_SessionInfo;
}

template <class CodeJockFrameClass>
bool FrameBase<CodeJockFrameClass>::IsSameTenant(const std::shared_ptr<Sapio365Session>& p_OtherSession) const
{
	return (bool)p_OtherSession
		&& (bool)m_sapio365Session
		&& m_sapio365Session->GetTenantName(true) == p_OtherSession->GetTenantName(true);
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnWindowPosChanged(WINDOWPOS* lpwndpos)
{
	CodeJockFrameClass::OnWindowPosChanged(lpwndpos);

	YDialog::EnsureCurrentAlwaysOnTopWindowIsAlwaysOnTop();
}

template <class CodeJockFrameClass>
LRESULT FrameBase<CodeJockFrameClass>::OnRibbonContextMenu(WPARAM wParam, LPARAM lParam)
{
	// Bug #190111.EH.0098BE: [QUICKIE] remove menu "quick access toolbar" in menu

	auto ribbon = (CXTPRibbonBar*)wParam;
	ASSERT(&getRibbonBar() == ribbon);

	auto popupBar = (CXTPPopupBar*)lParam;
	if (nullptr != popupBar)
	{
		auto ctrl = popupBar->GetControls()->GetAt(popupBar->GetControls()->CommandToIndex(XTP_ID_RIBBONCUSTOMIZE));
		if (nullptr != ctrl)
			popupBar->GetControls()->Remove(ctrl);
	}

	return 0;
}

template <class CodeJockFrameClass>
LRESULT FrameBase<CodeJockFrameClass>::OnMessageBarClosed(WPARAM, LPARAM)
{
	m_MessageBar.reset(nullptr);
	return 0;
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::temporarySetStatusbarLeftText(bool p_Condition, const wstring& p_Text, const wstring& p_Tooltip)
{
	if (p_Condition)
	{
		auto indicator = getStatusBar()->FindPane(IDC_STATUS_BAR_START_TEXT);
		if (nullptr == indicator)
			indicator = getStatusBar()->AddIndicator(IDC_STATUS_BAR_START_TEXT, 0);

		ASSERT(nullptr != indicator);
		if (nullptr != indicator)
		{
			indicator->SetText(p_Text.c_str());
			indicator->SetTooltip(p_Tooltip.c_str());
			indicator->BestFit();
		}
	}
	else
	{
		const auto index = getStatusBar()->CommandToIndex(IDC_STATUS_BAR_START_TEXT);
		if (-1 != index)
			getStatusBar()->RemoveAt(index);
	}
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::temporaryChangeStatusbarBackgroundColor(bool p_Condition, const COLORREF& p_TempColor)
{
	if (p_Condition)
	{
		getStatusBar()->GetStatusBarPaintManager()->m_clrBackground = CXTPPaintManagerColor(p_TempColor);
	}
	else
	{
		// reload colors form theme.
		getStatusBar()->GetStatusBarPaintManager()->RefreshMetrics();
	}
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::OnThemeWasSet()
{
	// Nothing to do in base class.
}


template <class CodeJockFrameClass>
bool FrameBase<CodeJockFrameClass>::showMessageBar(const CString& p_xamlContent, /*const CString& p_Tooltip,*/ const std::unordered_map<UINT, CString>& buttons/* = { {IDOK, _YTEXT("OK")} }*/, const COLORREF p_CustomBackgroundColor/* = COLORREF(-1)*/)
{
	//if (!m_MessageBar)
	{
		auto myMessageBar = new ColoredMessageBar;
		myMessageBar->SetBackgroundColor(p_CustomBackgroundColor);
		m_MessageBar.reset(myMessageBar);
		if (!m_MessageBar->Create(GetCommandBars()))
		{
			m_MessageBar.reset();
			return false;
		}
	}

	m_MessageBar->AddButton(SC_CLOSE, nullptr, _T("Hide"));

	for (auto& but : buttons)
		m_MessageBar->AddButton(but.first, but.second, _YTEXT(""));

	m_MessageBar->EnableMarkup();
	// FIXME: Doesn't exist :-o
	//m_MessageBar->SetTooltip(p_Tooltip);
	m_MessageBar->SetMessage(p_xamlContent);

	GetCommandBars()->RecalcFrameLayout();

	return true;
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::removeMessageBar()
{
	if (m_MessageBar)
	{
		// Is there another way of correctly closing the message bar?
		CXTPMessageBarButton button;
		button.m_nID = SC_CLOSE;
		m_MessageBar->Click(&button);
	}
}

template <class CodeJockFrameClass>
bool FrameBase<CodeJockFrameClass>::IsMessageBarOnScreen()
{
	return (bool)m_MessageBar;
}

template <class CodeJockFrameClass>
void FrameBase<CodeJockFrameClass>::updateLogoutControlTexts()
{
	if (nullptr != m_LogOutControl)
	{
		const auto isElevated = GetSapio365Session() && GetSapio365Session()->IsElevated();
		setControlTexts(m_LogOutControl,
			YtriaTranslate::Do(GridFrameBase_logout_1, _YLOC("Sign out")).c_str(),
			YtriaTranslate::Do(GLOBAL_BEGIN_TEMPLATE_MESSAGE_MAP_7, _YLOC("Close Current Session")).c_str(),
			isElevated
			? _T("Lock the current session. The password will need to be re-entered when signing back in.\n\nNote: If you are using an elevated session, signing out will automatically demote it to an Advanced session, and will remove the password of the application that is providing these elevated privileges.")
			: YtriaTranslate::Do(GLOBAL_BEGIN_TEMPLATE_MESSAGE_MAP_8, _YLOC("Lock the current session. The password will need to be re-entered when signing back in.")).c_str());
	}
}
