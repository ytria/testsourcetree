#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "SpGroup.h"

namespace Sp
{
    class GroupDeserializer : public JsonObjectDeserializer, public Encapsulate<Sp::Group>
    {
    protected:
        void DeserializeObject(const web::json::object& p_Object) override;
    };
}

