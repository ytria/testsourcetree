#pragma once

#include "IRequester.h"
#include "IRequestLogger.h"

class BusinessOrgContact;
class IPropertySetBuilder;
class SingleRequestResult;
class OrgContactDeserializer;

class OrgContactRequester : public IRequester
{
public:
	OrgContactRequester(PooledString p_OrgContactId, IPropertySetBuilder&& p_PropertySet, const std::shared_ptr<IRequestLogger>& p_Logger);
    virtual TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	BusinessOrgContact& GetData();
    const SingleRequestResult& GetResult() const;

private:
    PooledString m_OrgContactId;
    vector<rttr::property> m_Properties;

    std::shared_ptr<OrgContactDeserializer> m_Deserializer;
    std::shared_ptr<SingleRequestResult> m_Result;
	std::shared_ptr<IRequestLogger> m_Logger;
};
