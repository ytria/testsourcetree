#include "UserDeserializerFactory.h"

UserDeserializerFactory::UserDeserializerFactory(std::shared_ptr<const Sapio365Session> p_Session)
	: m_Session(p_Session)
{
}

UserDeserializer UserDeserializerFactory::Create()
{
    return UserDeserializer(m_Session);
}
