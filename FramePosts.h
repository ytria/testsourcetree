#pragma once

#include "GridFrameBase.h"
#include "GridPosts.h"

class FramePosts : public GridFrameBase
{
public:
	using GridFrameBase::GridFrameBase;

    DECLARE_DYNAMIC(FramePosts)
    virtual ~FramePosts() = default;

    void ShowPosts(const O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>>& p_Posts, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_Refresh, bool p_FullPurge);
	void ShowPostsWithAttachments(const O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>>& p_Posts, const vector<O365UpdateOperation>& p_UpdateOperations, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge);
    void ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments);
    void ShowItemAttachment(const BusinessItemAttachment& p_Attachment);

    virtual O365Grid& GetGrid() override;
    
    virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

    void ApplySpecific(bool p_Selected);
    virtual void ApplyAllSpecific() override;
    virtual void ApplySelectedSpecific() override;

	virtual void HiddenViaHistory() override;
    virtual bool HasApplyButton() override;

protected:
    //DECLARE_MESSAGE_MAP()

    virtual void createGrid() override;

    virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

    virtual AutomatedApp::AUTOMATIONSTATUS automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
    GridPosts m_Grid;
};

