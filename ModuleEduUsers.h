#pragma once

#include "ModuleBase.h"

class ModuleEduUsers : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	void executeImpl(const Command& p_Command) override;
	void showEduUsers(const Command& p_Command);
	void applyChanges(const Command& p_Command);
};

