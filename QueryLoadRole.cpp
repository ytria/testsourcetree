#include "QueryLoadRole.h"
#include "SqlQuerySelect.h"
#include "SessionsSqlEngine.h"
#include "InlineSqlQueryRowHandler.h"
#include "ISqlQueryRowHandler.h"

QueryLoadRole::QueryLoadRole(int64_t p_RoleId, SessionsSqlEngine& p_Engine)
	:m_RoleId(p_RoleId),
	m_Engine(p_Engine)
{
}

void QueryLoadRole::Run()
{
	auto handler = [this](sqlite3_stmt* p_Stmt) {
		m_Name = GetStringFromColumn(0, p_Stmt);
	};
	SqlQuerySelect query(m_Engine, InlineSqlQueryRowHandler<decltype(handler)>(handler), _YTEXT(R"(SELECT Name FROM Roles WHERE Id=?)"));
	query.BindInt64(1, m_RoleId);
	query.Run();

	m_Status = query.GetStatus();
}

wstring QueryLoadRole::GetRoleName() const
{
	return m_Name;
}

