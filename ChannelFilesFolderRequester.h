#pragma once

#include "IRequester.h"

class BusinessChannel;
class BusinessDriveItem;
class DriveItemDeserializer;
class IRequestLogger;

class ChannelFilesFolderRequester : public IRequester
{
public:
	ChannelFilesFolderRequester(const wstring& p_TeamId, const wstring& p_ChannelId, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const BusinessDriveItem& GetData() const;

private:
	std::shared_ptr<DriveItemDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
	const wstring m_TeamId;
	const wstring m_ChannelId;
};
