#pragma once

#define SKIP_NEXT_REQUESTS_ON_ERROR 0

class MacroRequest
{
public:
	MacroRequest();
	virtual ~MacroRequest() = default;

	class IStep
	{
	public:
		virtual ~IStep() = default;

		virtual bool ShouldProcess() const = 0;
		virtual bool Process() const = 0;
	};

	class Step : public IStep
	{
	public:
		Step(std::function<bool()> p_ShouldProcess, std::function<bool()> p_Process);

		virtual bool ShouldProcess() const override;
		virtual bool Process() const override;

	private:
		std::function<bool()> m_ShouldProcess;
		std::function<bool()> m_Process;
	};

	void AddStep(std::unique_ptr<IStep>&& p_Step);

	std::unique_ptr<IStep>& CurrentStep();

	bool Next();

	bool Finished() const;

	bool Run();

protected:
	bool isFlagSet(int p_Flag, int p_Value) const;

private:
	vector<std::unique_ptr<IStep>> m_Steps;
	__int3264 m_Pos;
};

