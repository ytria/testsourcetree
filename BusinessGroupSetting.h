#pragma once

#include "BusinessObject.h"
#include "GroupSetting.h"
#include "SettingValue.h"

class BusinessGroupSetting : public BusinessObject
{
public:
    BusinessGroupSetting();
    BusinessGroupSetting(const GroupSetting& p_GroupSetting);

    static GroupSetting ToGroupSetting(const BusinessGroupSetting& p_GroupSetting);

	const boost::YOpt<PooledString>& GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<PooledString>& GetTemplateId() const;
    void SetTemplateId(const boost::YOpt<PooledString>& p_Val);

    const vector<SettingValue>& GetValues() const;
    vector<SettingValue>& GetValues();
    void SetValues(const vector<SettingValue>& p_Val);

    const boost::YOpt<bool>& GetCreatedOrDeleted() const;
    void SetCreatedOrDeleted(const boost::YOpt<bool>& p_Val);

	bool operator==(const BusinessGroupSetting& p_Other) const;

private:
	boost::YOpt<PooledString> m_DisplayName;
    boost::YOpt<PooledString> m_TemplateId;
    vector<SettingValue> m_Values;
    boost::YOpt<bool> m_CreatedOrDeleted;

    RTTR_ENABLE(BusinessObject)
        friend class GroupSettingDeserializer;
};

