#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "Subscription.h"

namespace Azure
{
	class SubscriptionDeserializer : public JsonObjectDeserializer, public Encapsulate<Azure::Subscription>
	{
	public:
		void DeserializeObject(const web::json::object& p_Object) override;
	};
}
