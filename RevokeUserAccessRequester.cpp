#include "RevokeUserAccessRequester.h"

#include "MSGraphSession.h"
#include "MSGraphCommonData.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "MsGraphHttpRequestLogger.h"

RevokeUserAccessRequester::RevokeUserAccessRequester(wstring p_UserId, wstring p_NameOrIdForLogging)
	: m_UserId(p_UserId)
	, m_NameOrIdForLogging(p_NameOrIdForLogging)
{

}

TaskWrapper<void> RevokeUserAccessRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri;
	uri.append_path(Rest::USERS_PATH);
	uri.append_path(GetUserId());
	uri.append_path(_YTEXT("revokeSignInSessions"));

	LoggerService::User(_YFORMAT(L"Revoking access to Office 365 application for %s", m_NameOrIdForLogging.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_string(), httpLogger, p_TaskData).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResultInfo)
		{
			try
			{
				result->SetResult(p_ResultInfo.get());
			}
			catch (const RestException& e)
			{
				RestResultInfo info(e.GetRequestResult());
				info.m_RestException = std::make_shared<RestException>(e);
				result->SetResult(info);
			}
			catch (const std::exception& e)
			{
				RestResultInfo info;
				info.m_Exception = e;
				result->SetResult(info);
			}
			catch (...)
			{
				result->SetResult(RestResultInfo());
			}
		});
}

const wstring& RevokeUserAccessRequester::GetUserId() const
{
	return m_UserId;
}

const SingleRequestResult& RevokeUserAccessRequester::GetResult() const
{
	ASSERT(m_Result);
	return *m_Result;
}
