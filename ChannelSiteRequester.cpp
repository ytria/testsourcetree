#include "ChannelSiteRequester.h"

#include "BusinessDriveItem.h"
#include "ChannelFilesFolderRequester.h"
#include "DriveRequester.h"
#include "O365AdminUtil.h"
#include "RESTUtils.h"
#include "RunOnScopeEnd.h"
#include "safeTaskCall.h"
#include "SiteDeserializer.h"
#include "SiteRequester.h"

#define USE_DRIVE_FOR_SITEID 1

ChannelSiteRequester::ChannelSiteRequester(const wstring& p_TeamId, const wstring& p_channelId, const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_TeamId(p_TeamId)
	, m_ChannelId(p_channelId)
	, m_Logger(p_Logger)
{

}

TaskWrapper<void> ChannelSiteRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Site = std::make_shared<BusinessSite>();
	return YSafeCreateTask([site = m_Site, logger = m_Logger, teamId = m_TeamId, channelId = m_ChannelId, p_Session, p_TaskData]() {
		// Whatever happens, put back old message value in logger
		RunOnScopeEnd([logger, oldMessage = logger->GetLogMessage()]() {
			logger->SetLogMessage(oldMessage);
		});

		logger->SetLogMessage(_T("file folders"));

		auto filesFolderRequester = std::make_shared<ChannelFilesFolderRequester>(teamId, channelId, logger);
		auto filesFolder = safeTaskCall(filesFolderRequester->Send(p_Session, p_TaskData).Then([filesFolderRequester]() {
			return filesFolderRequester->GetData();
			}), p_Session->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ObjectErrorHandler<BusinessDriveItem>, p_TaskData).get();

		if (filesFolder.GetError() && filesFolder.GetError()->GetStatusCode() != web::http::status_codes::NotFound)
		{
			site->SetError(filesFolder.GetError());
		}
		else if (!filesFolder.GetError())
		{
			PooledString siteId;
#if USE_DRIVE_FOR_SITEID
			{
				// Sync drive

				logger->SetLogMessage(_T("document library"));
				auto driveRequester = std::make_shared<DriveRequester>(DriveRequester::DriveSource::Drive, filesFolder.GetDriveId(), logger, _YTEXT("id,createdDateTime,description,lastModifiedDateTime,name,lastModifiedBy,createdBy,owner,quota,sharePointIds"));
				auto drive = safeTaskCall(driveRequester->Send(p_Session, p_TaskData).Then([driveRequester]() {
					return driveRequester->GetData();
					}), p_Session->GetMSGraphSession(Sapio365Session::APP_SESSION), Util::ObjectErrorHandler<BusinessDrive>, p_TaskData).get();

				if (drive.GetError() && drive.GetError()->GetStatusCode() != web::http::status_codes::NotFound)
					site->SetError(drive.GetError());
				else if (drive.GetSharepointIds() && drive.GetSharepointIds()->SiteId)
					siteId = *drive.GetSharepointIds()->SiteId;
			}
#else
			if (filesFolder.GetSharepointIds() && filesFolder.GetSharepointIds()->SiteId)
				siteId = *filesFolder.GetSharepointIds()->SiteId;
#endif
			if (!siteId.IsEmpty())
			{
				logger->SetLogMessage(_T("site"));
				auto siteRequester = std::make_shared<SiteRequester>(siteId, logger);
				*site = safeTaskCall(siteRequester->Send(p_Session, p_TaskData).Then([siteRequester]() {
					return siteRequester->GetData();
					}), p_Session->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ObjectErrorHandler<BusinessSite>, p_TaskData).get();
			}
		}
	});
}

const BusinessSite& ChannelSiteRequester::GetData() const
{
	ASSERT(m_Site);
	return *m_Site;
}
