#include "SpGroupDeserializer.h"

#include "JsonSerializeUtil.h"

void Sp::GroupDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("AllowMembersEditMembership"), m_Data.AllowMembersEditMembership, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("AllowRequestToJoinLeave"), m_Data.AllowRequestToJoinLeave, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("AutoAcceptRequestToJoinLeave"), m_Data.AutoAcceptRequestToJoinLeave, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("CanCurrentUserEditMembership"), m_Data.CanCurrentUserEditMembership, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("CanCurrentUserManageGroup"), m_Data.CanCurrentUserManageGroup, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("CanCurrentUserViewMembership"), m_Data.CanCurrentUserViewMembership, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("Description"), m_Data.Description, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("Id"), m_Data.Id, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("IsHiddenInUI"), m_Data.IsHiddenInUI, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("LoginName"), m_Data.LoginName, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("OnlyAllowMembersViewMembership"), m_Data.OnlyAllowMembersViewMembership, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("OwnerTitle"), m_Data.OwnerTitle, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("RequestToJoinLeaveEmailSetting"), m_Data.RequestToJoinLeaveEmailSetting, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("PrincipalType"), m_Data.PrincipalType, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("Title"), m_Data.Title, p_Object);
}
