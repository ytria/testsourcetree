#pragma once

#include "IRequester.h"
#include "BusinessMessageRule.h"
#include "IRequestLogger.h"

class MessageRuleDeserializer;
class SingleRequestResult;

template <class T, class U>
class ValueListDeserializer;

class MessageRulesRequester : public IRequester
{
public:
    MessageRulesRequester(const PooledString& p_UserId, const std::shared_ptr<IRequestLogger>& p_Logger);
    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    vector<BusinessMessageRule>& GetData();
    const SingleRequestResult& GetResult() const;

private:
    PooledString m_UserId;

    std::shared_ptr<ValueListDeserializer<BusinessMessageRule, MessageRuleDeserializer>> m_Deserializer;
    std::shared_ptr<SingleRequestResult> m_Result;
	std::shared_ptr<IRequestLogger> m_Logger;
};

