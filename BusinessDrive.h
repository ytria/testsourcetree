#pragma once

#include "BusinessObject.h"

#include "BusinessDriveItem.h"
#include "Drive.h"
#include "IdentitySet.h"
#include "Quota.h"

class BusinessDrive : public BusinessObject
{
public:
    BusinessDrive() = default;
    BusinessDrive(const Drive& p_Drive);

    const boost::YOpt<IdentitySet>& GetOwner() const;
    void SetOwner(const boost::YOpt<IdentitySet>& p_Val);
    
    const boost::YOpt<Quota>& GetQuota() const;
    void SetQuota(const boost::YOpt<Quota>& p_Val);

    const boost::YOpt<PooledString>& GetDriveType() const;
    void SetDriveType(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<IdentitySet>& GetCreatedBy() const;
	void SetCreatedBy(const boost::YOpt<IdentitySet>& p_Val);

	const boost::YOpt<YTimeDate>& GetCreatedDateTime() const;
	void SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_Val);

	const boost::YOpt<PooledString>& GetDescription() const;
	void SetDescription(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<YTimeDate>& GetLastModifiedDateTime() const;
	void SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val);

	const boost::YOpt<PooledString>& GetName() const;
	void SetName(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetWebUrl() const;
	void SetWebUrl(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<SharepointIds>& GetSharepointIds() const;
	void SetSharepointIds(const boost::YOpt<SharepointIds>& p_Val);

private:
	boost::YOpt<IdentitySet> m_CreatedBy;
	boost::YOpt<YTimeDate> m_CreatedDateTime;
	boost::YOpt<PooledString> m_Description;
    boost::YOpt<PooledString> m_DriveType;
	boost::YOpt<YTimeDate> m_LastModifiedDateTime;
	boost::YOpt<PooledString> m_Name;
	boost::YOpt<PooledString> m_WebUrl;
    boost::YOpt<IdentitySet> m_Owner;
    boost::YOpt<Quota> m_Quota;
	boost::YOpt<SharepointIds> m_SharepointIds;

    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND
    friend class DriveDeserializer;
	friend class DbDriveSerializer;
};

