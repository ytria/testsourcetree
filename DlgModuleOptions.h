#pragma once

#include "DlgFormsHTML.h"
#include "ModuleOptions.h"
#include "LoadingScope.h"
#include "Sapio365Session.h"

#include <boost/bimap.hpp>

class DlgModuleOptions : public DlgFormsHTML
{
public:
	//enum class Context {Messages, Events, MailFolders};

	DlgModuleOptions(CWnd* p_Parent, const std::shared_ptr<Sapio365Session>& p_Session, const ModuleOptions& p_Options = ModuleOptions());
	
	const ModuleOptions& GetOptions() const;

	static void TranslateAction(AutomatedApp* p_App, AutomationAction* p_Action, list<AutomationAction*>& p_ActionList);

public:
	//static const wstring g_AutoParamMessageType;
	//static const wstring g_AutoParamEmailsAndChats;
	static const wstring g_AutoParamEmails;
	static const wstring g_AutoParamChats;

	static const wstring g_AutoParamOnlineInPlaceArchive;
	static const wstring g_AutoParamClutterFolders;
	static const wstring g_AutoParamSoftDeletedFolders;
	static const wstring g_AutoParamScheduledFolders;
	static const wstring g_AutoParamSearchFolders;


	static const wstring g_AutoParamBodyPreview;
	static const wstring g_AutoParamFullBody;
	static const wstring g_AutoParamMailHeaders;
	static const wstring g_AutoParamFilterBuilder;
	static const wstring g_AutoParamFilter;

	struct FilterFieldProperties
	{
		struct Type
		{
		public:
			virtual ~Type();
			void FillJson(web::json::value& p_Properties) const;

			bool IsDate() const;
			bool IsDateTime() const;

		protected:
			web::json::value m_Properties;
		};

		struct StringType : public Type
		{
		public:
			StringType();
		};

		struct BoolType : public Type
		{
		public:
			BoolType(const std::pair<wstring, wstring>& p_TrueFalseLabelOverride = {});
		};

		struct EnumType : public StringType
		{
		public:
			EnumType(const std::vector<std::pair<wstring, wstring>>& p_Values);
		};

		struct DateType : public Type
		{
		public:
			DateType();
		};

		// Uncomment if necessary
		struct DateTimeType : public Type
		{
		public:
			DateTimeType();
		};

		struct IntegerType : public Type
		{
		public:
			IntegerType();
		};

		wstring m_DisplayName;
		// Add type!
		bool m_AddQuotes = false;
		Type m_Type;
		vector<wstring> m_OperatorRestrictions;
	};

protected:
	void addCutOffEditor(const wstring& p_TypeLabel, const wstring& p_DateLabel);
	void addFilterEditor(bool p_DirectEdit = false);

	virtual bool	setParamSpecific(AutomationAction* i_Action) override;

	virtual std::map<wstring, FilterFieldProperties> getFilterFields() const;

	static const vector<wstring> g_FilterOperatorsNoEQ;
	static const vector<wstring> g_FilterOperatorsNoEQNE;
	static const vector<wstring> g_FilterOperatorsNoStartWith;
	static const vector<wstring> g_FilterOperatorsEQStartsWith;
	static const vector<wstring> g_FilterOperatorsEQ;
	static const vector<wstring> g_FilterOperatorsEQGELE;
	static const vector<wstring> g_FilterOperatorsGELE;

	//void generateJSONScriptData() override;
	bool processPostedData(const IPostedDataTarget::PostedData& data) override;
	//wstring getDialogTitle() const override;
	static wstring GetLoadingScopeStr(LoadingScope scope);
	static wstring GetLoadingScopeFriendlyStr(LoadingScope scope);
	void UpdateCutOffDateTime(const wstring& p_CutOffType, const boost::YOpt<YTimeDate>& p_ScopeDateTime);

	wstring externalFilterProcessCallback(const wstring p_PropertyName, const wstring p_CurrentFilterJson) override;


protected:
	bool m_UpdateOptions;

	ModuleOptions m_ModuleOpts;
	const std::shared_ptr<Sapio365Session>& m_Session;

	static const wstring g_CutOffType;
	static const wstring g_CutOffDate;

private:
	web::json::value getFilterConfig() const;

private:
	AutomationAction* m_CurrentMotherAction; // Only for setParamSpecific
	size_t m_CurrentSetParamTotalCount;

	class FilterParser;
};

