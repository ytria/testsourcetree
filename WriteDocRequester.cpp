#include "WriteDocRequester.h"
#include "CosmosDBSqlSession.h"
#include "DumpDeserializer.h"
#include "LoggerService.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "RESTUtils.h"
#include "AzureADCommonData.h"
#include "BasicHttpRequestLogger.h"



Cosmos::WriteDocRequester::WriteDocRequester(const wstring& p_DatabaseName, const wstring& p_CollectionName, const web::json::value& p_Document, bool p_UpdateIfExists)
    : m_DbName(p_DatabaseName),
    m_CollectionName(p_CollectionName),
    m_Document(p_Document),
    m_UpdateIfExists(p_UpdateIfExists)
{
}

TaskWrapper<void> Cosmos::WriteDocRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<DumpDeserializer>();
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("dbs"));
    uri.append_path(m_DbName);
    uri.append_path(_YTEXT("colls"));
    uri.append_path(m_CollectionName);
    uri.append_path(_YTEXT("docs"));

    auto body = json::value::object();
    body = m_Document;

    const WebPayloadJSON payload(body);

    LoggerService::User(YtriaTranslate::Do(Cosmos__WriteDocRequester_Send_1, _YLOC("Creating document in collection \"%1\" inside database \"%2\" in CosmosDB"), m_CollectionName.c_str(), m_DbName.c_str()), p_TaskData.GetOriginator());

    auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetCosmosDBSqlSession()->Post(uri.to_uri(),
        httpLogger,
        payload,
        _YTEXT("docs"),
        wstring(_YTEXT("dbs/")) + m_DbName + wstring(_YTEXT("/colls/")) + m_CollectionName,
        p_Session->GetCosmosDbMasterKey(),
        m_Result,
        m_Deserializer,
        p_TaskData,
        { { _YTEXT("x-ms-documentdb-is-upsert"), m_UpdateIfExists ? _YTEXT("true") : _YTEXT("false") } });
}

const wstring& Cosmos::WriteDocRequester::GetData() const
{
    return m_Deserializer->GetData();
}

const SingleRequestResult& Cosmos::WriteDocRequester::GetResult() const
{
    return *m_Result;
}
