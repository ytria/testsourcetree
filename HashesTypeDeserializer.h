#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "HashesType.h"

class HashesTypeDeserializer : public JsonObjectDeserializer, public Encapsulate<HashesType>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

