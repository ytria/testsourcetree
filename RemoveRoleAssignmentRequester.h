#pragma once

#include "IRequester.h"
#include "SpUtils.h"

class SingleRequestResult;

namespace Sp
{
    class RemoveRoleAssignmentRequester : public IRequester
    {
    public:
        RemoveRoleAssignmentRequester(PooledString p_SiteName, PooledString p_PrincipalId, PooledString p_RoleDefId);
        TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

        const SingleRequestResult& GetResult() const;
    private:
        PooledString m_SiteName;
        PooledString m_PrincipalId;
        PooledString m_RoleDefId;

        std::shared_ptr<SingleRequestResult> m_Result;
    };
}
