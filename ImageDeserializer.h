#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Image.h"

class ImageDeserializer : public JsonObjectDeserializer, public Encapsulate<Image>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

