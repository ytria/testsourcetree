#include "BusinessAttachment.h"


BusinessAttachment::BusinessAttachment()
{
}

BusinessAttachment::BusinessAttachment(const Attachment& p_Attachment)
{
    m_ContentType = p_Attachment.ContentType;
	//m_ContentId = p_Attachment.ContentId;
    m_Id = p_Attachment.Id ? *p_Attachment.Id : _YTEXT("");
    m_IsInline = p_Attachment.IsInline;
    m_LastModifiedDateTime = p_Attachment.LastModifiedDateTime;
    m_Name = p_Attachment.Name;
    m_Size = p_Attachment.Size;
    m_DataType = p_Attachment.DataType;
}

const boost::YOpt<PooledString>& BusinessAttachment::GetContentType() const
{
    return m_ContentType;
}

void BusinessAttachment::SetContentType(const boost::YOpt<PooledString>& p_Val)
{
    m_ContentType = p_Val;
}

//const boost::YOpt<PooledString>& BusinessAttachment::GetContentId() const
//{
//	return m_ContentId;
//}

//void BusinessAttachment::SetContentId(const boost::YOpt<PooledString>& p_Val)
//{
//	m_ContentId = p_Val;
//}

const boost::YOpt<bool>& BusinessAttachment::GetIsInline() const
{
    return m_IsInline;
}

void BusinessAttachment::SetIsInline(const boost::YOpt<bool>& p_Val)
{
    m_IsInline = p_Val;
}

const boost::YOpt<YTimeDate>& BusinessAttachment::GetLastModifiedDateTime() const
{
    return m_LastModifiedDateTime;
}

void BusinessAttachment::SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val)
{
    m_LastModifiedDateTime = p_Val;
}

const boost::YOpt<PooledString>& BusinessAttachment::GetName() const
{
    return m_Name;
}

void BusinessAttachment::SetName(const boost::YOpt<PooledString>& p_Val)
{
    m_Name = p_Val;
}

const boost::YOpt<int32_t>& BusinessAttachment::GetSize() const
{
    return m_Size;
}

void BusinessAttachment::SetSize(const boost::YOpt<int32_t>& p_Val)
{
    m_Size = p_Val;
}

const boost::YOpt<PooledString>& BusinessAttachment::GetDataType() const
{
    return m_DataType;
}

void BusinessAttachment::SetDataType(const boost::YOpt<PooledString>& p_Val)
{
    m_DataType = p_Val;
}
