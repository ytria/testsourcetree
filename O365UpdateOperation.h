#pragma once

#include "GridBackendField.h"
#include "HttpResultWithError.h"
#include <string>

class O365UpdateOperation
{
public:
    O365UpdateOperation(const vector<GridBackendField>& p_PrimaryKey);
	O365UpdateOperation(const O365UpdateOperation& other, const vector<GridBackendField>& p_PrimaryKey);

	void StoreResult(const HttpResultWithError& p_HttpResultWithError);
	void StoreResults(const vector<HttpResultWithError>& p_HttpResultsWithError);

	const std::vector<HttpResultWithError>& GetResults() const;
	const vector<GridBackendField>& GetPrimaryKey() const;
    PooledString GetPkFieldStr(GridBackendColumn* p_Column) const;
	PooledString GetPkFieldStr(UINT p_ColumnID) const;
	const GridBackendField& GetPkField(GridBackendColumn* p_Column) const;
	const GridBackendField& GetPkField(UINT p_ColumnID) const;

private:
    vector<GridBackendField> m_PrimaryKey;

	std::vector<HttpResultWithError> m_Results;
};
