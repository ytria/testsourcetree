#pragma once

#include "IRequester.h"

#include "CosmosDbAccountDeserializer.h"

class SingleRequestResult;

namespace Azure
{
	// https://docs.microsoft.com/en-us/rest/api/cosmos-db-resource-provider/databaseaccounts/get
	class GetDbAccountRequester : public IRequester
	{
	public:
		GetDbAccountRequester(PooledString p_SubscriptionId, PooledString p_ResourceGroup, PooledString p_DbAccountName);
		TaskWrapper<void> Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

		const Azure::CosmosDbAccount& GetData() const;

	private:
		std::shared_ptr<Azure::CosmosDbAccountDeserializer> m_Deserializer;
		std::shared_ptr<SingleRequestResult> m_Result;

		PooledString m_SubscriptionId;
		PooledString m_ResourceGroup;
		PooledString m_DbAccountName;
	};
}

