#include "PreAuthorizedApplicationDeserializer.h"
#include "ListDeserializer.h"

void PreAuthorizedApplicationDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("appId"), m_Data.m_AppId, p_Object);

	ListStringDeserializer delegatedPermissionIdsDeserializer;
	if (JsonSerializeUtil::DeserializeAny(delegatedPermissionIdsDeserializer, _YTEXT("delegatedPermissionIds"), p_Object))
		m_Data.m_DelegatedPermissionIds = std::move(delegatedPermissionIdsDeserializer.GetData());

}
