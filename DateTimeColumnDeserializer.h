#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "DateTimeColumn.h"

class DateTimeColumnDeserializer : public JsonObjectDeserializer, public Encapsulate<DateTimeColumn>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

