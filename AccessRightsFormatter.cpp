#include "AccessRightsFormatter.h"
#include "MailboxPermissionsAccessRights.h"

class Sorter
{
public:
	enum SortOrder
	{
		FullAccess = 0, ExternalAccount, DeleteItem, ReadPermission, ChangePermission, ChangeOwner
	};

	bool operator()(const wstring& p_Str1, const wstring& p_Str2)
	{
		static const map<wstring, SortOrder> g_SortOrder = {
			{ MailboxPermissionsAccessRights::g_FullAccess, FullAccess },
			{ MailboxPermissionsAccessRights::g_ExternalAccount, ExternalAccount },
			{ MailboxPermissionsAccessRights::g_DeleteItem, DeleteItem },
			{ MailboxPermissionsAccessRights::g_ReadPermission, ReadPermission },
			{ MailboxPermissionsAccessRights::g_ChangePermission, ChangePermission },
			{ MailboxPermissionsAccessRights::g_ChangeOwner, ChangeOwner },
		};

		return g_SortOrder.at(p_Str1) < g_SortOrder.at(p_Str2);
	}
};

AccessRightsFormatter::AccessRightsFormatter(const wstring& p_AccessRights)
	:m_AccessRights(p_AccessRights)
{
}

wstring AccessRightsFormatter::Format()
{
	vector<wstring> tokens = Str::explodeIntoVector(m_AccessRights, _YTEXT(","), false, true);
	std::sort(begin(tokens), end(tokens), Sorter());

	return Str::implode(tokens, _YTEXT(", "));
}
