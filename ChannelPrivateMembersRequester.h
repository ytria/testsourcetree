#pragma once

#include "IRequester.h"
#include "SingleRequestResult.h"
#include "ValueListDeserializer.h"

class BusinessAADUserConversationMember;
class BusinessAADUserConversationMemberDeserializer;
class IRequestLogger;

class ChannelPrivateMembersRequester : public IRequester
{
public:
	ChannelPrivateMembersRequester(const boost::YOpt<PooledString>& p_TeamId, const wstring& p_ChannelId, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const vector<BusinessAADUserConversationMember>& GetData() const;

private:
	wstring m_TeamId;
	wstring m_ChannelId;

	std::shared_ptr<ValueListDeserializer<BusinessAADUserConversationMember, BusinessAADUserConversationMemberDeserializer>> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
};