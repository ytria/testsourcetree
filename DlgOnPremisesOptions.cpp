#include "DlgOnPremisesOptions.h"
#include "Sapio365Settings.h"
#include "SessionsSqlEngine.h"
#include "PersistentSession.h"
#include "GraphCache.h"

namespace
{
	static const wstring g_UseOnPrem = _YTEXT("UseOnPrem");
	static const wstring g_DontAskAgainUseOnPrem = _YTEXT("DontAskAgain");
	static const wstring g_AADComputerName = _YTEXT("AADComputerName");
	static const wstring g_UseConsistencyGuid = _YTEXT("UseConsistencyGuid");
	static const wstring g_AutoLoadOnPrem = _YTEXT("AutoLoadOnPrem");
	static const wstring g_IntroLoad = _YTEXT("IntroLoad");
	static const wstring g_OutroLoad = _YTEXT("OutroLoad");
	static const wstring g_ConnectToRemoteRSAT = _YTEXT("ConnectToRemoteRSAT");
	static const wstring g_ADDSServer = _YTEXT("ADDSServer");
	static const wstring g_ADDSUsername = _YTEXT("ADDSUsername");
	static const wstring g_ADDSPassword = _YTEXT("ADDSPassword");
}

DlgOnPremisesOptions::DlgOnPremisesOptions(CWnd* p_Parent, bool p_UseOnPrem, bool p_DontAskOnLogin, const wstring& p_AADComputername, bool p_AutoLoadOnPrem, bool p_UseConsistencyGuid, bool p_ConnectToRemoteRSAT, const wstring& p_Server, const wstring& p_Username)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent),
	m_InitialUseOnPrem(p_UseOnPrem),
	m_InitialDontAskOnLogin(p_DontAskOnLogin),
	m_InitialAADComputerName(p_AADComputername),
	m_InitialAutoLoadOnPrem(p_AutoLoadOnPrem),
	m_InitialUseConsistencyGuid(p_UseConsistencyGuid),
	m_InitialConnectToRemoteRSAT(p_ConnectToRemoteRSAT),
	m_InitialADDSServer(p_Server),
	m_InitialADDSUsername(p_Username),
	m_InitialADDSPassword(_T("Password not shown when saved"))
{
}

bool DlgOnPremisesOptions::processPostedData(const IPostedDataTarget::PostedData& data)
{
	for (const auto& d : data)
	{
		if (d.first == g_UseOnPrem)
			m_UseOnPrem = d.second == UsefulStrings::g_ToggleValueTrue;
		else if (d.first == g_DontAskAgainUseOnPrem)
			m_DontAskAgainUseOnPrem = d.second == UsefulStrings::g_ToggleValueTrue;
		else if (d.first == g_AADComputerName)
			m_AADComputerName = d.second;
		else if (d.first == g_AutoLoadOnPrem)
			m_AutoLoadOnPrem = d.second == UsefulStrings::g_ToggleValueTrue;
		else if (d.first == g_UseConsistencyGuid)
			m_UseConsistencyGuid = d.second == UsefulStrings::g_ToggleValueTrue;
		else if (d.first == g_ConnectToRemoteRSAT)
			m_ConnectToRemoteRSAT = d.second == UsefulStrings::g_ToggleValueTrue;
		else if (d.first == g_ADDSServer)
			m_ADDSServer = d.second;
		else if (d.first == g_ADDSUsername)
			m_ADDSUsername = d.second;
		else if (d.first == g_ADDSPassword)
			m_ADDSPassword = d.second;
	}

	return true;
}

const boost::YOpt<bool>& DlgOnPremisesOptions::GetUseOnPrem() const
{
	return m_UseOnPrem;
}

const boost::YOpt<bool>& DlgOnPremisesOptions::GetDontAskAgainUseOnPrem() const
{
	return m_DontAskAgainUseOnPrem;
}

const boost::YOpt<wstring>& DlgOnPremisesOptions::GetAADComputerName() const
{
	return m_AADComputerName;
}

const boost::YOpt<bool>& DlgOnPremisesOptions::GetAutoLoadOnPrem() const
{
	return m_AutoLoadOnPrem;
}

const boost::YOpt<bool>& DlgOnPremisesOptions::GetUseConsistencyGuid() const
{
	return m_UseConsistencyGuid;
}

const boost::YOpt<bool>& DlgOnPremisesOptions::GetConnectToRemoteRSAT() const
{
	return m_ConnectToRemoteRSAT;
}

const boost::YOpt<wstring>& DlgOnPremisesOptions::GetADDSServer() const
{
	return m_ADDSServer;
}

const boost::YOpt<wstring>& DlgOnPremisesOptions::GetADDSUsername() const
{
	return m_ADDSUsername;
}

const boost::YOpt<wstring>& DlgOnPremisesOptions::GetADDSPassword() const
{
	return m_ADDSPassword;
}

int DlgOnPremisesOptions::getMinWidthOverride() const
{
	return 485;
}

void DlgOnPremisesOptions::SetVerifiedDomains(const vector<wstring>& p_Domains)
{
	m_VerifiedDomains = p_Domains;
}

void DlgOnPremisesOptions::SetADDSDomain(const wstring& p_Domain)
{
	m_ADDSDomain = p_Domain;
}

void DlgOnPremisesOptions::SaveOnPremisesOptions(const DlgOnPremisesOptions& p_Dlg, PersistentSession& p_Session, GraphCache* p_Cache)
{
	if (p_Dlg.GetUseOnPrem())
		p_Session.SetUseOnPrem(*p_Dlg.GetUseOnPrem());
	if (p_Dlg.GetDontAskAgainUseOnPrem())
		p_Session.SetDontAskAgainUseOnPrem(*p_Dlg.GetDontAskAgainUseOnPrem());
	if (p_Dlg.GetAADComputerName())
		p_Session.SetAADComputerName(*p_Dlg.GetAADComputerName());
	if (p_Dlg.GetAutoLoadOnPrem())
		p_Session.SetAutoLoadOnPrem(*p_Dlg.GetAutoLoadOnPrem());
	if (p_Dlg.GetUseConsistencyGuid())
		p_Session.SetUseMsDsConsistencyGuid(*p_Dlg.GetUseConsistencyGuid());
	if (p_Dlg.GetConnectToRemoteRSAT())
		p_Session.SetUseRemoteRSAT(*p_Dlg.GetConnectToRemoteRSAT());
	if (p_Dlg.GetADDSServer())
		p_Session.SetADDSServer(*p_Dlg.GetADDSServer());
	if (p_Dlg.GetADDSUsername())
		p_Session.SetADDSUsername(*p_Dlg.GetADDSUsername());
	if (p_Dlg.GetADDSPassword())
		p_Session.SetADDSPassword(*p_Dlg.GetADDSPassword());

	SessionsSqlEngine::Get().Save(p_Session);
	if (p_Cache)
		p_Cache->SetPersistentSession(p_Session);
}

wstring DlgOnPremisesOptions::getDialogTitle() const
{
	return _T("On-Premises options");
}

void DlgOnPremisesOptions::generateJSONScriptData()
{
	initMain(_YTEXT("OnPremisesOptions"));

	if (!m_VerifiedDomains.empty())
	{
		CheckListEditor::LabelsAndValues lblsAndValues;
		for (const auto& domain : m_VerifiedDomains)
			lblsAndValues.emplace_back(domain, domain);

		getGenerator().Add(WithTooltip<CheckListEditor>(
			{
				{
					_YTEXT("verifiedDomains")
					, _T("Verified Domains")
					, EditorFlags::READONLY
					, m_VerifiedDomains
				}
				, lblsAndValues
			}
		, _T("Verified domains in the current session")));

		getGenerator().addIconTextField(_YTEXT("localDomain"), _YFORMAT(L"AD DS Domain: %s", m_ADDSDomain.c_str()), _YTEXT(""));
	}
	else
	{
		getGenerator().addIconTextField(_YTEXT("intro1"), _T("To view the verified domains, please login to the corresponding session."), _YTEXT(""));
	}

	getGenerator().addIconTextField(_YTEXT("intro2"), _T("Do you want to enable the \"Load On-Premises\" functionality for this session?"), _YTEXT(""));

	getGenerator().Add(BoolToggleEditor({ { g_UseOnPrem, _T("Enable"), EditorFlags::DIRECT_EDIT, m_InitialUseOnPrem } }));

	// If no load on-prem
	getGenerator().Add(BoolToggleEditor({ { g_DontAskAgainUseOnPrem, _T("Don't ask on login"), EditorFlags::DIRECT_EDIT, m_InitialDontAskOnLogin } }));

	// If load on-prem
	getGenerator().addIconTextField(g_IntroLoad, _T("To 'Sync AD DS/Azure AD' directly from sapio365, the computer name of the Azure AD Connect server is required.<br><br>Please enter it below and select the source anchor* set in Azure AD Connect."), _YTEXT(""));
	getGenerator().Add(StringEditor(g_AADComputerName, _T("Computer Name"), EditorFlags::DIRECT_EDIT, m_InitialAADComputerName));
	getGenerator().Add(BoolToggleEditor({ { g_AutoLoadOnPrem, _T("Autoload on-premises data"), EditorFlags::DIRECT_EDIT, m_InitialAutoLoadOnPrem } }));
	getGenerator().Add(BoolToggleEditor({ { g_UseConsistencyGuid, _T("Use ms-DS-ConsistencyGuid as Source Anchor"), EditorFlags::DIRECT_EDIT, m_InitialUseConsistencyGuid } }));
	getGenerator().addIconTextField(g_OutroLoad, _T("*The source anchor attribute is used to match on-premises objects in Active Directory Domain Services (AD DS) to objects in Azure Active Directory (Azure AD)."), _YTEXT(""));
	
	if (CRMpipe().IsFeatureSandboxed())
	{
		getGenerator().addCategory(_T("Remote RSAT"), CategoryFlags::NOFLAG);
		getGenerator().Add(BoolToggleEditor({ { g_ConnectToRemoteRSAT, _T("Connect to remote RSAT"), EditorFlags::DIRECT_EDIT, m_InitialConnectToRemoteRSAT } }));
		getGenerator().Add(StringEditor(g_ADDSServer, _T("Server"), EditorFlags::DIRECT_EDIT, m_InitialADDSServer));
		getGenerator().Add(StringEditor(g_ADDSUsername, _T("Username"), EditorFlags::DIRECT_EDIT, m_InitialADDSUsername));
		getGenerator().Add(PasswordEditor(g_ADDSPassword, _T("Password"), EditorFlags::DIRECT_EDIT, m_InitialADDSPassword));
	}
	
	getGenerator().addEvent(g_UseOnPrem, { g_IntroLoad, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueFalse });
	getGenerator().addEvent(g_UseOnPrem, { g_DontAskAgainUseOnPrem, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueTrue });
	getGenerator().addEvent(g_UseOnPrem, { g_AADComputerName, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueFalse });
	getGenerator().addEvent(g_UseOnPrem, { g_AutoLoadOnPrem, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueFalse });
	getGenerator().addEvent(g_UseOnPrem, { g_UseConsistencyGuid, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueFalse });
	getGenerator().addEvent(g_UseOnPrem, { g_OutroLoad, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueFalse });
	getGenerator().addEvent(g_UseOnPrem, { g_ConnectToRemoteRSAT, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueFalse });
	getGenerator().addEvent(g_UseOnPrem, { g_ADDSServer, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueFalse });
	getGenerator().addEvent(g_UseOnPrem, { g_ADDSUsername, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueFalse });
	getGenerator().addEvent(g_UseOnPrem, { g_ADDSPassword, EventTarget::g_HideIfEqual, UsefulStrings::g_ToggleValueFalse });


	getGenerator().addApplyButton(_T("Ok"));
	getGenerator().addCancelButton(_T("Cancel"));
}
