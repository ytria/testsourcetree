#pragma once

#include "ModuleBase.h"

class ModuleClassMembers : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	void executeImpl(const Command& p_Command) override;
	void showClassMembers(const Command& p_Command);
	void applyChanges(const Command& p_Command);
};

