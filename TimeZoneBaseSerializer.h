#pragma once

#include "IJsonSerializer.h"

class TimeZoneBaseSerializer : public IJsonSerializer
{
public:
	TimeZoneBaseSerializer(const TimeZoneBase& p_TimeZoneBase);
	void Serialize() override;

private:
	const TimeZoneBase& m_TimeZoneBase;
};

