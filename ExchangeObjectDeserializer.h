#pragma once

#include "IXmlDeserializer.h"

#include "xercesc\dom\DOMElement.hpp"

using XERCES_CPP_NAMESPACE::DOMElement;

namespace Ex
{
class ExchangeObjectDeserializer : public IXmlDeserializer
{
public:
    ExchangeObjectDeserializer();
    virtual void DeserializeObject(const DOMElement& p_Element) = 0;

    bool HasGlobalError() const;
    const wstring& GetGlobalErrorMessage() const;
    const wstring& GetGlobalErrorResponseCode() const;

protected:
    void DeserializeImpl(const vector<uint8_t>& p_Xml) override final;

    DOMElement* GetSoapBody(const DOMElement& p_Envelope) const;
    DOMElement* GetElementByTagName(const DOMElement& p_ParentElem, const wstring& p_TagName) const;
    
    bool ElementHasError(const DOMElement& p_ParentElem) const;
    std::pair<wstring, wstring> GetError(const DOMElement& p_ParentElem) const;

    boost::YOpt<bool> DeserializeBool(const DOMElement& p_ParentElem, const wstring& p_TagName) const;
    boost::YOpt<wstring> DeserializeString(const DOMElement& p_ParentElem, const wstring& p_TagName) const;

    bool m_HasGlobalError;
    wstring m_GlobalErrorMessage;
    wstring m_GlobalErrorResponseCode;

};
}

