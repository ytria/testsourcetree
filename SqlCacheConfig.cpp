#include "SqlCacheConfig.h"

const std::map<wstring, std::vector<PooledString>>& SqlCacheObjectSpecific<UserCache>::DataBlocks()
{
	static std::map<wstring, std::vector<PooledString>> blocks
	{
		{ DataBlockName(BlockId::MIN), DbUserSerializer::MemoryCacheRequestFields() },
		{ DataBlockName(BlockId::LIST), DbUserSerializer::NotInMemoryCacheListRequestFields() },
		{ DataBlockName(BlockId::SYNCV1), DbUserSerializer::SyncOnlyRequestFields_v1() },
		{ DataBlockName(BlockId::SYNCBETA), DbUserSerializer::SyncOnlyRequestFields_beta() },
		{ DataBlockName(BlockId::MAILBOXSETTINGS), DbUserSerializer::MailboxSettingsRequestFields() },
		{ DataBlockName(BlockId::ARCHIVEFOLDERNAME), DbUserSerializer::ArchiveFolderNameRequestFields() },
		{ DataBlockName(BlockId::DRIVE), DbUserSerializer::DriveRequestFields() },
		{ DataBlockName(BlockId::MANAGER), DbUserSerializer::ManagerRequestFields() },
		{ DataBlockName(BlockId::MAILBOX), DbUserSerializer::MailboxRequestFields() },
		{ DataBlockName(BlockId::RECIPIENTPERMISSIONS), DbUserSerializer::RecipientPermissionsRequestFields() },
		{ DataBlockName(BlockId::ASSIGNEDLICENSES), DbUserSerializer::AssignedLicensesRequestFields() },
	};

	return blocks;
}

const wstring& SqlCacheObjectSpecific<UserCache>::DataBlockName(typename BlockId p_BlockId)
{
	static std::map<BlockId, wstring> blocks
	{
		{ BlockId::MIN, _YTEXT("min") },
		{ BlockId::LIST, _YTEXT("list") },
		{ BlockId::SYNCV1, _YTEXT("syncv1") },
		{ BlockId::SYNCBETA, _YTEXT("syncbeta") },
		{ BlockId::MAILBOXSETTINGS, _YTEXT("mailboxsettings") },
		{ BlockId::ARCHIVEFOLDERNAME, _YTEXT("archivefoldername") },
		{ BlockId::DRIVE, _YTEXT("drive") },
		{ BlockId::MANAGER, _YTEXT("manager") },
		{ BlockId::MAILBOX, _YTEXT("mailbox") },
		{ BlockId::RECIPIENTPERMISSIONS, _YTEXT("recipientPermissions") },
		{ BlockId::ASSIGNEDLICENSES, _YTEXT("assignedLicenses") },
	};

	auto it = blocks.find(p_BlockId);
	ASSERT(blocks.end() != it);
	return it->second;
}

// ==================================================================================

const std::map<wstring, std::vector<PooledString>>& SqlCacheObjectSpecific<GroupCache>::DataBlocks()
{
	static std::map<wstring, std::vector<PooledString>> blocks
	{
		{ DataBlockName(BlockId::MIN), DbGroupSerializer::MemoryCacheRequestFields() },
		{ DataBlockName(BlockId::LIST), DbGroupSerializer::NotInMemoryCacheListRequestFields() },
		{ DataBlockName(BlockId::SYNCV1), DbGroupSerializer::SyncOnlyRequestFields_v1() },
		{ DataBlockName(BlockId::SYNCBETA), DbGroupSerializer::SyncOnlyRequestFields_beta() },
		{ DataBlockName(BlockId::CREATEDONBEHALFOF), DbGroupSerializer::CreatedOnBehalfOfRequestFields() },
		{ DataBlockName(BlockId::TEAM), DbGroupSerializer::TeamRequestFields() },
		{ DataBlockName(BlockId::GROUPSETTINGS), DbGroupSerializer::GroupSettingsRequestFields() },
		{ DataBlockName(BlockId::LIFECYCLEPOLICIES), DbGroupSerializer::LifeCyclePoliciesRequestFields() },
		{ DataBlockName(BlockId::OWNERS), DbGroupSerializer::OwnersRequestFields() },
		{ DataBlockName(BlockId::MEMBERSCOUNT), DbGroupSerializer::MembersCountRequestFields() },
		{ DataBlockName(BlockId::DRIVE), DbGroupSerializer::DriveRequestFields() },
		{ DataBlockName(BlockId::ISYAMMER), DbGroupSerializer::IsYammerRequestFields() },
	};

	return blocks;
}

const wstring& SqlCacheObjectSpecific<GroupCache>::DataBlockName(typename BlockId p_BlockId)
{
	static std::map<BlockId, wstring> blocks
	{
		{ BlockId::MIN, _YTEXT("min") },
		{ BlockId::LIST, _YTEXT("list") },
		{ BlockId::SYNCV1, _YTEXT("syncv1") },
		{ BlockId::SYNCBETA, _YTEXT("syncbeta") },
		{ BlockId::CREATEDONBEHALFOF, _YTEXT("createdonbehalfof") },
		{ BlockId::TEAM, _YTEXT("team") },
		{ BlockId::GROUPSETTINGS, _YTEXT("groupsettings") },
		{ BlockId::LIFECYCLEPOLICIES, _YTEXT("lifecyclepolicies") },
		{ BlockId::OWNERS, _YTEXT("owners") },
		{ BlockId::MEMBERSCOUNT, _YTEXT("memberscount") },
		{ BlockId::DRIVE, _YTEXT("drive") },
		{ BlockId::ISYAMMER, _YTEXT("isyammer") },
	};

	auto it = blocks.find(p_BlockId);
	ASSERT(blocks.end() != it);
	return it->second;
}
