#pragma once

#include "BusinessObjectManager.h"
#include "BusinessGroup.h"
#include "MacroRequest.h"
#include "MultiObjectsRequestLogger.h"

#include <boost/functional/hash/hash.hpp>

template<>
struct std::hash<std::vector<GBI>>
{
	size_t operator()(const std::vector<GBI>& _Keyval) const
	{
		return boost::hash_range(_Keyval.begin(), _Keyval.end());
	}
};

class GroupMacroRequest : public MacroRequest
{
public:
	GroupMacroRequest(BusinessGroup& p_BG, const std::unordered_map<std::vector<GBI>, std::vector<rttr::property>>& p_Properties, boost::YOpt<BusinessLifeCyclePolicies> p_LCP, uint32_t p_GroupAdditionnalInfoFlags, std::shared_ptr<BusinessObjectManager> p_BOM, const std::shared_ptr<MultiObjectsRequestLogger>& p_Logger, YtriaTaskData p_TaskData);

private:
	void addPropertiesStep(size_t index, const vector<rttr::property>& p_Properties, bool p_KeepOnlyNotNullvalues, const std::vector<GBI>& p_BlockIds);

	std::unique_ptr<MacroRequest::Step> CreateCreatedOnBehalfOfStep(BusinessGroup& p_Group, int p_Flags) const;
	std::unique_ptr<MacroRequest::Step> CreateTeamStep(BusinessGroup& p_Group, int p_Flags) const;
	std::unique_ptr<MacroRequest::Step> CreateGroupSettingsStep(BusinessGroup& p_Group, int p_Flags) const;
	std::unique_ptr<MacroRequest::Step> CreateLifeCyclePoliciesStep(BusinessGroup& p_Group, int p_Flags) const;
	std::unique_ptr<MacroRequest::Step> CreateOwnersStep(BusinessGroup& p_Group, int p_Flags) const;
	std::unique_ptr<MacroRequest::Step> CreateMembersCountStep(BusinessGroup& p_Group, int p_Flags) const;
	std::unique_ptr<MacroRequest::Step> CreateDriveStep(BusinessGroup& p_Group, int p_Flags) const;
	std::unique_ptr<MacroRequest::Step> CreateIsYammerStep(BusinessGroup& p_Group, int p_Flags) const;

	BusinessGroup& m_Group;
	const std::unordered_map<std::vector<GBI>, std::vector<rttr::property>>& m_Properties;
	std::shared_ptr<BusinessObjectManager> m_BOM;
	YtriaTaskData m_TaskData;
	boost::YOpt<BusinessLifeCyclePolicies> m_LCP;
	const std::shared_ptr<MultiObjectsRequestLogger>& m_Logger;
};
