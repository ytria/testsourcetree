#include "WorkingHoursSerializer.h"

#include "JsonSerializeUtil.h"
#include "NullableSerializer.h"
#include "TimeZoneBaseSerializer.h"

WorkingHoursSerializer::WorkingHoursSerializer(const WorkingHours& p_WorkingHours)
    : m_WorkingHours(p_WorkingHours)
{
}

void WorkingHoursSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

	JsonSerializeUtil::SerializeListString(_YTEXT("daysOfWeek"), m_WorkingHours.DaysOfWeek, obj);
	JsonSerializeUtil::SerializeTimeOnly(_YTEXT("startTime"), m_WorkingHours.StartTime, obj);
	JsonSerializeUtil::SerializeTimeOnly(_YTEXT("endTime"), m_WorkingHours.EndTime, obj);
	JsonSerializeUtil::SerializeAny(_YTEXT("timeZone"), NullableSerializer<TimeZoneBase, TimeZoneBaseSerializer>(m_WorkingHours.TimeZone), obj);
}
