#pragma once

#include "ModificationWithRequestFeedback.h"
#include "GridUtil.h"
#include <type_traits>

class GridMailboxPermissions;

class MailboxPermissionsRemoveModification : public ModificationWithRequestFeedback
{
public:
	MailboxPermissionsRemoveModification(GridMailboxPermissions& p_Grid, const row_pk_t& p_RowPk);

	void Apply() override;
	void Revert() override;
	vector<ModificationLog> GetModificationLogs() const override;

	const vector<wstring>& GetPermissions() const;
	const wstring& GetMailboxOwner() const;
	const wstring& GetUserId() const;

private:
	wstring m_UserId;
	vector<wstring> m_Permissions;
	wstring m_MailboxOwner;

	std::reference_wrapper<GridMailboxPermissions> m_Grid;
};

