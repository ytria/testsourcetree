#include "GridColumns.h"

#include "AutomationWizardColumns.h"
#include "FrameColumns.h"
#include "O365AdminUtil.h"
#include "GridUpdater.h"
#include "BasicGridSetup.h"
#include "GraphCache.h"

BEGIN_MESSAGE_MAP(GridColumns, ModuleO365Grid<BusinessColumnDefinition>)
    
END_MESSAGE_MAP()

GridColumns::GridColumns()
    : m_FrameColumns(nullptr)
{
    //UseDefaultActions(/*O365Grid::ACTION_CREATE | */O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT/* | O365Grid::ACTION_VIEW*/);

	initWizard<AutomationWizardColumns>(_YTEXT("Automation\\Columns"));
}

void GridColumns::customizeGrid()
{
	static const wstring g_FamilyList = YtriaTranslate::Do(GridColumns_customizeGrid_1, _YLOC("List")).c_str();

	{
		BasicGridSetup gridSetup(GridUtil::g_AutoNameColumns, LicenseUtil::g_codeSapio365columns);
		gridSetup.AddMetaColumn(GridSetupMetaColumn(&m_ColMetaDisplayName, YtriaTranslate::Do(GridColumns_customizeGrid_2, _YLOC("List Display Name")).c_str(), g_FamilyList, GridSetupMetaColumn::g_ColUIDMetaDisplayName));
		gridSetup.AddMetaColumn(GridSetupMetaColumn(&m_ColMetaName, YtriaTranslate::Do(GridColumns_customizeGrid_3, _YLOC("List Name")).c_str(), g_FamilyList, _YUID("meta.listName")));
		gridSetup.AddMetaColumn(GridSetupMetaColumn(&m_ColMetaSiteDisplayName, YtriaTranslate::Do(GridColumns_customizeGrid_4, _YLOC("Site Display Name")).c_str(), g_FamilySite, GridSetupMetaColumn::g_ColUIDMetaUserName));
		gridSetup.AddMetaColumn(GridSetupMetaColumn(&m_ColMetaId, YtriaTranslate::Do(GridColumns_customizeGrid_5, _YLOC("List ID")).c_str(), g_FamilyList, GridSetupMetaColumn::g_ColUIDMetaId));
		gridSetup.Setup(*this, false);
	}

	setBasicGridSetupHierarchy({ { { m_ColMetaDisplayName, 0 } }
								,{ rttr::type::get<BusinessColumnDefinition>().get_id() }
								,{ rttr::type::get<BusinessColumnDefinition>().get_id(), rttr::type::get<BusinessList>().get_id() } });

    m_ColMetaSiteId								= AddColumn(_YUID("meta.siteId"),									YtriaTranslate::Do(GridColumns_customizeGrid_6, _YLOC("Site ID")).c_str(),										g_FamilyInfo,			g_ColumnSize12,	{ g_ColumnsPresetTech });
    m_ColName									= AddColumn(_YUID("name"),											YtriaTranslate::Do(GridColumns_customizeGrid_7, _YLOC("Name")).c_str(),											g_FamilyInfo,			g_ColumnSize22);
    m_ColDisplayName							= AddColumn(_YUID("displayName"),									YtriaTranslate::Do(GridColumns_customizeGrid_8, _YLOC("Display Name")).c_str(),									g_FamilyInfo,			g_ColumnSize22,	{ g_ColumnsPresetDefault });
    m_ColColumnGroup							= AddColumn(_YUID("columnGroup"),									YtriaTranslate::Do(GridColumns_customizeGrid_9, _YLOC("Column Group")).c_str(),									g_FamilyInfo,			g_ColumnSize22,	{ g_ColumnsPresetDefault });
    m_ColDescription							= AddColumn(_YUID("description"),									YtriaTranslate::Do(GridColumns_customizeGrid_10, _YLOC("Description")).c_str(),									g_FamilyInfo,			g_ColumnSize32,	{ g_ColumnsPresetDefault });
    m_ColEnforceUniqueValues					= AddColumnCheckBox(_YUID("enforceUniqueValues"),					YtriaTranslate::Do(GridColumns_customizeGrid_11, _YLOC("Enforce Unique Values")).c_str(),						g_FamilyInfo,			g_ColumnSize6,	{ g_ColumnsPresetDefault });
    m_ColHidden									= AddColumnCheckBox(_YUID("hidden"),								YtriaTranslate::Do(GridColumns_customizeGrid_12, _YLOC("Hidden")).c_str(),										g_FamilyInfo,			g_ColumnSize6);
    m_ColIndexed								= AddColumnCheckBox(_YUID("indexed"),								YtriaTranslate::Do(GridColumns_customizeGrid_13, _YLOC("Indexed")).c_str(),										g_FamilyInfo,			g_ColumnSize6);
    m_ColReadOnly								= AddColumnCheckBox(_YUID("readOnly"),								YtriaTranslate::Do(GridColumns_customizeGrid_14, _YLOC("Read Only")).c_str(),									g_FamilyInfo,			g_ColumnSize6);
    m_ColRequired								= AddColumnCheckBox(_YUID("required"),								YtriaTranslate::Do(GridColumns_customizeGrid_15, _YLOC("Required")).c_str(),										g_FamilyInfo,			g_ColumnSize6,	{ g_ColumnsPresetDefault });
	static const wstring g_FamilyColumnSetup = YtriaTranslate::Do(GridColumns_customizeGrid_16, _YLOC("Column Setup")).c_str();
    m_ColCalculatedFormat						= AddColumn(_YUID("calculated.format"),								YtriaTranslate::Do(GridColumns_customizeGrid_17, _YLOC("Calculated - Format")).c_str(),							g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColCalculatedFormula						= AddColumn(_YUID("calculated.formula"),							YtriaTranslate::Do(GridColumns_customizeGrid_18, _YLOC("Calculated - Formula")).c_str(),							g_FamilyColumnSetup,	g_ColumnSize32,	{ g_ColumnsPresetDefault });
    m_ColCalculatedOutputType					= AddColumn(_YUID("calculated.outputType"),							YtriaTranslate::Do(GridColumns_customizeGrid_19, _YLOC("Calculated - Output Type")).c_str(),						g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColChoiceAllowTextEntry					= AddColumnCheckBox(_YUID("choice.allowTextEntry"),					YtriaTranslate::Do(GridColumns_customizeGrid_20, _YLOC("Choice - Allow Text Entry")).c_str(),					g_FamilyColumnSetup,	g_ColumnSize6);
    m_ColChoiceChoices							= AddColumn(_YUID("choice.choices"),								YtriaTranslate::Do(GridColumns_customizeGrid_21, _YLOC("Choice - Choices")).c_str(),								g_FamilyColumnSetup,	g_ColumnSize22,	{ g_ColumnsPresetDefault });
    m_ColChoiceDisplayAs						= AddColumn(_YUID("choice.displayAs"),								YtriaTranslate::Do(GridColumns_customizeGrid_23, _YLOC("Choice - Display As")).c_str(),							g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColCurrencyLocale							= AddColumn(_YUID("currency.locale"),								YtriaTranslate::Do(GridColumns_customizeGrid_22, _YLOC("Currency - Locale")).c_str(),							g_FamilyColumnSetup,	g_ColumnSize12,	{ g_ColumnsPresetDefault });
    m_ColDateTimeDisplayAs						= AddColumn(_YUID("dateTime.displayAs"),							YtriaTranslate::Do(GridColumns_customizeGrid_24, _YLOC("DateTime - Display As")).c_str(),						g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColDateTimeFormat							= AddColumn(_YUID("dateTime.format"),								YtriaTranslate::Do(GridColumns_customizeGrid_25, _YLOC("DateTime - Format")).c_str(),							g_FamilyColumnSetup,	g_ColumnSize12,	{ g_ColumnsPresetDefault });
    m_ColDefaultValueFormula					= AddColumn(_YUID("defaultValue.formula"),							YtriaTranslate::Do(GridColumns_customizeGrid_26, _YLOC("Default Value - Formula")).c_str(),						g_FamilyColumnSetup,	g_ColumnSize12,	{ g_ColumnsPresetDefault });
    m_ColDefaultValueValue						= AddColumn(_YUID("defaultValue.value"),							YtriaTranslate::Do(GridColumns_customizeGrid_27, _YLOC("Default Value - Value")).c_str(),						g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColLookupAllowMultipleValues				= AddColumnCheckBox(_YUID("lookup.allowMultipleValues"),			YtriaTranslate::Do(GridColumns_customizeGrid_28, _YLOC("Lookup - Allow Multiple Values")).c_str(),				g_FamilyColumnSetup,	g_ColumnSize6,	{ g_ColumnsPresetDefault });
    m_ColLookupAllowUnlimitedLength				= AddColumnCheckBox(_YUID("lookup.allowUnlimitedLength"),			YtriaTranslate::Do(GridColumns_customizeGrid_29, _YLOC("Lookup - Allow Limited Length")).c_str(),				g_FamilyColumnSetup,	g_ColumnSize6);
    m_ColLookupColumnName						= AddColumn(_YUID("lookup.columnName"),								YtriaTranslate::Do(GridColumns_customizeGrid_30, _YLOC("Lookup - Column Name")).c_str(),							g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColLookupListId							= AddColumn(_YUID("lookup.listId"),									YtriaTranslate::Do(GridColumns_customizeGrid_31, _YLOC("Lookup - List ID")).c_str(),								g_FamilyColumnSetup,	g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColLookupPrimaryLookupColumnId			= AddColumn(_YUID("lookup.primaryLookupColumnId"),					YtriaTranslate::Do(GridColumns_customizeGrid_32, _YLOC("Lookup - Primary Lookup Column ID")).c_str(),			g_FamilyColumnSetup,	g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColNumberDecimalPlaces					= AddColumn(_YUID("number.decimalPlaces"),							YtriaTranslate::Do(GridColumns_customizeGrid_33, _YLOC("Number - Decimal Places")).c_str(),						g_FamilyColumnSetup,	g_ColumnSize12,	{ g_ColumnsPresetDefault });
    m_ColNumberDisplayAs						= AddColumn(_YUID("number.displayAs"),								YtriaTranslate::Do(GridColumns_customizeGrid_34, _YLOC("Number - Display As")).c_str(),							g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColNumberMaximum							= AddColumnNumber(_YUID("number.maximum"),							YtriaTranslate::Do(GridColumns_customizeGrid_35, _YLOC("Number - Maximum")).c_str(),								g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColNumberMinimum							= AddColumnNumber(_YUID("number.minimum"),							YtriaTranslate::Do(GridColumns_customizeGrid_36, _YLOC("Number - Minimum")).c_str(),								g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColPersonOrGroupAllowMultipleSelection	= AddColumnCheckBox(_YUID("personOrGroup.allowMultipleSelection"),	YtriaTranslate::Do(GridColumns_customizeGrid_37, _YLOC("Person Or Group - Allow Multiple Selection")).c_str(),	g_FamilyColumnSetup,	g_ColumnSize6,	{ g_ColumnsPresetDefault });
    m_ColPersonOrGroupDisplayAs					= AddColumn(_YUID("personOrGroup.displayAs"),						YtriaTranslate::Do(GridColumns_customizeGrid_38, _YLOC("Person Or Group - Display As")).c_str(),					g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColPersonOrGroupChooseFromType			= AddColumn(_YUID("personOrGroup.chooseFromType"),					YtriaTranslate::Do(GridColumns_customizeGrid_39, _YLOC("Person Or Group - Choose From Type")).c_str(),			g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColTextAllowMultipleLines					= AddColumnCheckBox(_YUID("text.allowMultipleLines"),				YtriaTranslate::Do(GridColumns_customizeGrid_40, _YLOC("Text - Allow Multiple Lines")).c_str(),					g_FamilyColumnSetup,	g_ColumnSize6);
    m_ColTextAppendChangesToExistingText		= AddColumnCheckBox(_YUID("text.appendChangesToExistingText"),		YtriaTranslate::Do(GridColumns_customizeGrid_41, _YLOC("Text - Append Changes To Existing Text")).c_str(),		g_FamilyColumnSetup,	g_ColumnSize6);
    m_ColTextLinesForEditing					= AddColumnNumber(_YUID("text.linesForEditing"),					YtriaTranslate::Do(GridColumns_customizeGrid_42, _YLOC("Text - Lines For Editing")).c_str(),						g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColTextMaxLength							= AddColumnNumber(_YUID("text.maxLength"),							YtriaTranslate::Do(GridColumns_customizeGrid_43, _YLOC("Text - Max Length")).c_str(),							g_FamilyColumnSetup,	g_ColumnSize12);
    m_ColTextTextType							= AddColumn(_YUID("text.textType"),									YtriaTranslate::Do(GridColumns_customizeGrid_44, _YLOC("Text - Text Type")).c_str(),								g_FamilyColumnSetup,	g_ColumnSize12,	{ g_ColumnsPresetDefault });
	addColumnGraphID();

    AddColumnForRowPK(m_ColMetaSiteId);
    AddColumnForRowPK(m_ColMetaId);
    AddColumnForRowPK(GetColId());

    m_FrameColumns = dynamic_cast<FrameColumns*>(GetParentFrame());
	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_FrameColumns);
}

wstring GridColumns::GetName(const GridBackendRow* p_Row) const
{
    return makeRowName(p_Row, { {_T("Site"), m_ColMetaSiteDisplayName }, { _T("List"), m_ColMetaDisplayName } }, m_ColDisplayName);
}

ModuleCriteria GridColumns::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_FrameColumns->GetModuleCriteria();

    criteria.m_SubIDs.clear();
    for (auto row : p_Rows)
    {
        const PooledString& siteId = row->GetField(m_ColMetaSiteId).GetValueStr();
        const PooledString& listId = row->GetField(m_ColMetaId).GetValueStr();

        criteria.m_SubIDs[siteId].insert(listId);
    }
    return criteria;
}

void GridColumns::BuildView(const O365DataMap<BusinessSite, vector<BusinessList>>& p_Lists, bool p_FullPurge)
{
	GridUpdater updater(*this,
        GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID
            | (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
            | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
            | GridUpdaterOptions::REFRESH_PROCESS_DATA
        )
    );
    GridIgnoreModification ignoramus(*this);
    for (const auto& keyVal : p_Lists)
    {
        const auto& site = keyVal.first;
        const auto& lists = keyVal.second;

        for (const auto& list : lists)
        {
            GridBackendField fieldSiteId(site.GetID(), m_ColMetaSiteId);
            GridBackendField fieldListId(list.GetID(), m_ColMetaId);
            GridBackendField fieldColumnId(site.GetID(), GetColId());

            GridBackendRow* listRow = m_RowIndex.GetRow({ fieldSiteId, fieldListId, fieldColumnId }, true, true);
			SetRowObjectType(listRow, list, list.HasFlag(BusinessObject::CANCELED));
            ASSERT(nullptr != listRow);
			if (nullptr != listRow)
				listRow->AddField(list.GetDisplayName(), m_ColMetaDisplayName);
            if (nullptr != listRow && !list.HasFlag(BusinessObject::CANCELED))
            {
				updater.GetOptions().AddRowWithRefreshedValues(listRow);
                updater.GetOptions().AddPartialPurgeParentRow(listRow);

                if (list.GetError())
                {
                    updater.OnLoadingError(listRow, *list.GetError());
                }
                else
                {
					// Clear previous error
					if (listRow->IsError())
						listRow->ClearStatus();

                    const wstring hierarchyDisplayName = GetGraphCache().GetSiteHierarchyDisplayName(site.GetID());
                    listRow->SetHierarchyParentToHide(true);
                    listRow->AddField(hierarchyDisplayName, m_ColMetaSiteDisplayName);

                    for (const auto& column : list.GetColumns())
                    {
                        auto columnRow = FillRow(site, list, column, hierarchyDisplayName);
						ASSERT(nullptr != columnRow);
						if (nullptr != columnRow)
						{
							updater.GetOptions().AddRowWithRefreshedValues(columnRow);
							columnRow->SetParentRow(listRow);
						}
                    }
                }
            }
        }
    }
}

BusinessColumnDefinition GridColumns::getBusinessObject(GridBackendRow* p_Row) const
{
    ASSERT(false);
    return BusinessColumnDefinition();
}

GridBackendRow* GridColumns::FillRow(const BusinessSite& p_Site, const BusinessList& p_List, const BusinessColumnDefinition& p_Column, const wstring& p_HierarchyDisplayName)
{
    GridBackendField fieldSiteId(p_Site.GetID(), m_ColMetaSiteId);
    GridBackendField fieldListId(p_List.GetID(), m_ColMetaId);
    GridBackendField fieldColumnId(p_Column.GetID(), GetColId());

    GridBackendRow* row = m_RowIndex.GetRow({fieldSiteId, fieldListId, fieldColumnId}, true, true);

    ASSERT(nullptr != row);
    if (nullptr != row)
    {
		SetRowObjectType(row, BusinessColumnDefinition()/*, BusinessColumnDefinition().HasFlag(BusinessObject::CANCELED)*/);
        row->AddField(p_HierarchyDisplayName, m_ColMetaSiteDisplayName);
        row->AddField(p_List.GetDisplayName(), m_ColMetaDisplayName);
        row->AddField(p_List.GetName(), m_ColMetaName);

        row->AddField(p_Column.GetName(), m_ColName);
        row->AddField(p_Column.GetDisplayName(), m_ColDisplayName);
        row->AddField(p_Column.GetColumnGroup(), m_ColColumnGroup);
        row->AddField(p_Column.GetDescription(), m_ColDescription);
        row->AddField(p_Column.GetEnforceUniqueValues(), m_ColEnforceUniqueValues);
        row->AddField(p_Column.GetHidden(), m_ColHidden);
        row->AddField(p_Column.GetIndexed(), m_ColIndexed);
        row->AddField(p_Column.GetReadOnly(), m_ColReadOnly);
        row->AddField(p_Column.GetRequired(), m_ColRequired);

        if (p_Column.GetCalculatedFacet())
        {
            const auto& calculatedFacet = p_Column.GetCalculatedFacet();
            row->AddField(calculatedFacet->Format, m_ColCalculatedFormat);
            row->AddField(calculatedFacet->Formula, m_ColCalculatedFormula);
            row->AddField(calculatedFacet->OutputType, m_ColCalculatedOutputType);
        }
		else
		{
			row->RemoveField(m_ColCalculatedFormat);
			row->RemoveField(m_ColCalculatedFormula);
			row->RemoveField(m_ColCalculatedOutputType);
		}

        if (p_Column.GetChoiceFacet())
        {
            const auto& choiceFacet = p_Column.GetChoiceFacet();
            row->AddField(choiceFacet->AllowTextEntry, m_ColChoiceAllowTextEntry);
            row->AddField(choiceFacet->Choices, m_ColChoiceChoices);
            row->AddField(choiceFacet->DisplayAs, m_ColChoiceAllowTextEntry);
        }
		else
		{
			row->RemoveField(m_ColChoiceAllowTextEntry);
			row->RemoveField(m_ColChoiceChoices);
			row->RemoveField(m_ColChoiceAllowTextEntry);
		}

        if (p_Column.GetCurrencyFacet())
        {
            row->AddField(p_Column.GetCurrencyFacet()->Locale, m_ColCurrencyLocale);
        }
		else
		{
			row->RemoveField(m_ColCurrencyLocale);
		}

        if (p_Column.GetDateTimeFacet())
        {
            const auto& dateTimeFacet = p_Column.GetDateTimeFacet();
            row->AddField(dateTimeFacet->DisplayAs, m_ColDateTimeDisplayAs);
            row->AddField(dateTimeFacet->Format, m_ColDateTimeFormat);
        }
		else
		{
			row->RemoveField(m_ColDateTimeDisplayAs);
			row->RemoveField(m_ColDateTimeFormat);
		}

        if (p_Column.GetDefaultValueFacet())
        {
            const auto& defaultValueFacet = p_Column.GetDefaultValueFacet();
            row->AddField(defaultValueFacet->Formula, m_ColDefaultValueFormula);
            row->AddField(defaultValueFacet->Value, m_ColDefaultValueValue);
        }
		else
		{
			row->RemoveField(m_ColDefaultValueFormula);
			row->RemoveField(m_ColDefaultValueValue);
		}

        if (p_Column.GetLookupFacet())
        {
            const auto& lookupFacet = p_Column.GetLookupFacet();
            row->AddField(lookupFacet->AllowMultipleValues, m_ColLookupAllowMultipleValues);
            row->AddField(lookupFacet->AllowUnlimitedLength, m_ColLookupAllowUnlimitedLength);
            row->AddField(lookupFacet->ColumnName, m_ColLookupColumnName);
            row->AddField(lookupFacet->ListId, m_ColLookupListId);
            row->AddField(lookupFacet->PrimaryLookupColumnId, m_ColLookupPrimaryLookupColumnId);
        }
		else
		{
			row->RemoveField(m_ColLookupAllowMultipleValues);
			row->RemoveField(m_ColLookupAllowUnlimitedLength);
			row->RemoveField(m_ColLookupColumnName);
			row->RemoveField(m_ColLookupListId);
			row->RemoveField(m_ColLookupPrimaryLookupColumnId);
		}

        if (p_Column.GetNumberFacet())
        {
            const auto& numberFacet = p_Column.GetNumberFacet();
            row->AddField(numberFacet->DecimalPlaces, m_ColNumberDecimalPlaces);
            row->AddField(numberFacet->DisplayAs, m_ColNumberDisplayAs);
            row->AddField(numberFacet->Maximum, m_ColNumberMaximum);
            row->AddField(numberFacet->Minimum, m_ColNumberMinimum);
        }
		else
		{
			row->RemoveField(m_ColNumberDecimalPlaces);
			row->RemoveField(m_ColNumberDisplayAs);
			row->RemoveField(m_ColNumberMaximum);
			row->RemoveField(m_ColNumberMinimum);
		}

        if (p_Column.GetPersonOrGroupFacet())
        {
            const auto& personOrGroupFacet = p_Column.GetPersonOrGroupFacet();
            row->AddField(personOrGroupFacet->AllowMultipleSelection, m_ColPersonOrGroupAllowMultipleSelection);
            row->AddField(personOrGroupFacet->DisplayAs, m_ColPersonOrGroupDisplayAs);
            row->AddField(personOrGroupFacet->ChooseFromType, m_ColPersonOrGroupChooseFromType);
        }
		else
		{
			row->RemoveField(m_ColPersonOrGroupAllowMultipleSelection);
			row->RemoveField(m_ColPersonOrGroupDisplayAs);
			row->RemoveField(m_ColPersonOrGroupChooseFromType);
		}

        if (p_Column.GetTextFacet())
        {
            const auto& textFacet = p_Column.GetTextFacet();
            row->AddField(textFacet->AllowMultipleLines, m_ColTextAllowMultipleLines);
            row->AddField(textFacet->AppendChangesToExistingText, m_ColTextAppendChangesToExistingText);
            row->AddField(textFacet->LinesForEditing, m_ColTextLinesForEditing);
            row->AddField(textFacet->MaxLength, m_ColTextMaxLength);
            row->AddField(textFacet->TextType, m_ColTextTextType);
        }
		else
		{
			row->RemoveField(m_ColTextAllowMultipleLines);
			row->RemoveField(m_ColTextAppendChangesToExistingText);
			row->RemoveField(m_ColTextLinesForEditing);
			row->RemoveField(m_ColTextMaxLength);
			row->RemoveField(m_ColTextTextType);
		}
    }

    return row;
}
