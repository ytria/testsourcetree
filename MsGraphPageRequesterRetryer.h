#pragma once

#include "IMsGraphPageRequester.h"

class MsGraphPageRequesterRetryer : public IMsGraphPageRequester
{
public:
	using askifretry_t = std::function<bool(const std::string&, HWND p_Hwnd)>;
	static askifretry_t DefaultAskIfRetry();

public:
	MsGraphPageRequesterRetryer(const std::shared_ptr<IMsGraphPageRequester>& p_Requester, const askifretry_t& p_AskIfRetry, HWND p_Hwnd);
	web::json::value Request(const web::uri& p_Uri, const shared_ptr<MSGraphSession>& p_Session, YtriaTaskData p_TaskData) override;

	virtual void SetUseBetaEndpoint(bool p_UseBetaEndpoint) override;

private:
	std::shared_ptr<IMsGraphPageRequester> m_Requester;
	askifretry_t m_AskIfRetry;
	HWND m_Hwnd;
};

