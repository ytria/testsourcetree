#include "YBrowserLinkHandler.h"

#include "DlgLicenseMessageBoxHTML.h"
#include "Office365Admin.h"
#include "LinkHandlerAuditLogs.h"
#include "LinkHandlerContacts.h"
#include "LinkHandlerDirectoryRoles.h"
#include "LinkHandlerDrive.h"
#include "LinkHandlerEvents.h"
#include "LinkHandlerGroups.h"
#include "LinkHandlerJobDelete.h"
#include "LinkHandlerJobExecute.h"
#include "LinkHandlerJobSchedule.h"
#include "LinkHandlerJobScheduleDelete.h"
#include "LinkHandlerJobScheduleEdit.h"
#include "LinkHandlerLicenses.h"
#include "LinkHandlerMessages.h"
#include "LinkHandlerMessageRules.h"
#include "LinkHandlerO365Reports.h"
#include "LinkHandlerSchools.h"
#include "LinkHandlerSignIns.h"
#include "LinkHandlerSites.h"
#include "LinkHandlersMisc.h"
#include "LinkHandlerUsers.h"
#include "YCodeJockMessageBox.h"
#include "LinkHandlerOnPremiseUsers.h"
#include "LinkHandlerOnPremiseGroups.h"

YBrowserLinkHandler::YBrowserLinkHandler()
{
    registerLinkHandlers();
}

void YBrowserLinkHandler::Handle(YBrowserLink& link) const
{
	auto handlerNodeItt = m_LinkHandlers.find(link.GetUrl().host());
	if (handlerNodeItt != std::end(m_LinkHandlers))
	{
		const std::unique_ptr<IBrowserLinkHandler>& linkHandler = handlerNodeItt->second;
		ASSERT(nullptr != linkHandler);
		if (nullptr != linkHandler)
		{
			auto app = dynamic_cast<Office365AdminApp*>(AfxGetApp());
			ASSERT(nullptr != app);
			const auto isLicenseAJL = Office365AdminApp::IsLicense<LicenseTag::AJL>();
			const auto authorizedByLicense = link.IsAllowedWithAJLLicense() || linkHandler->IsAllowedWithAJLLicense() || nullptr != app && !isLicenseAJL;
			if (!authorizedByLicense)
			{
				TraceIntoFile::trace(_YDUMP("YBrowserLinkHandler"), _YDUMP("Handle"), _YDUMPFORMAT(L"Module not allowed by AJL"));// , link.GetUrl().
				DlgLicenseMessageBoxHTML dlg(nullptr,
					DlgMessageBox::eIcon_ExclamationWarning,
					_T("This module is not accessible with your license."),
					_YTEXT(""),
					_YTEXT(""),
					{ { IDOK, YtriaTranslate::Do(LicenseManager_isVendorInitOK_1, _YLOC("OK")).c_str() } },
					{ 0, _YTEXT("") },
					app->GetApplicationColor());
				dlg.DoModal();
			}
			else if (!linkHandler->IsAllowedDuringAutomation() && AutomatedApp::IsAutomationRunning())
			{
				YCodeJockMessageBox dlg(nullptr,
					DlgMessageBox::eIcon_ExclamationWarning,
					YtriaTranslate::Do(CommandDispatcher_ExecuteImpl_1, _YLOC("This command cannot be executed at this time.")).c_str(),
					YtriaTranslate::Do(AutomationWizard_Run_2, _YLOC("Please wait for the current task to complete and try again.")).c_str(),
					_YTEXT(""),
					{ { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
				dlg.SetBlockAutomation(true);
				dlg.DoModal();
			}
			else
			{
				link.SetIsLicenseAJL(isLicenseAJL);
				linkHandler->Handle(link);
			}
		}
	}
}

void YBrowserLinkHandler::registerLinkHandlers()
{
	m_LinkHandlers[_YTEXT("onpremise_users_module")]		= std::make_unique<LinkHandlerOnPremiseUsers>();
	m_LinkHandlers[_YTEXT("onpremise_groups_module")]		= std::make_unique<LinkHandlerOnPremiseGroups>();

	m_LinkHandlers[_YTEXT("users_module")]					= std::make_unique<LinkHandlerUsers>();
	m_LinkHandlers[_YTEXT("prefiltered_users_module")]		= std::make_unique<LinkHandlerPrefilteredUsers>();
	m_LinkHandlers[_YTEXT("groups_module")]					= std::make_unique<LinkHandlerGroups>();
    m_LinkHandlers[_YTEXT("prefiltered_groups_module")]		= std::make_unique<LinkHandlerPrefilteredGroups>();
	m_LinkHandlers[_YTEXT("sites_module")]  				= std::make_unique<LinkHandlerSites>();
	m_LinkHandlers[_YTEXT("schools_module")]				= std::make_unique<LinkHandlerSchools>();
    m_LinkHandlers[_YTEXT("directory_roles_module")]		= std::make_unique<LinkHandlerDirectoryRoles>();
	m_LinkHandlers[_YTEXT("licenses_module")]				= std::make_unique<LinkHandlerLicenses>();
	m_LinkHandlers[_YTEXT("signins_module")]				= std::make_unique<LinkHandlerSignIns>();
	m_LinkHandlers[_YTEXT("auditlogs_module")]				= std::make_unique<LinkHandlerAuditLogs>();

	m_LinkHandlers[_YTEXT("curusercontacts_module")]		= std::make_unique<LinkHandlerContacts>();
    m_LinkHandlers[_YTEXT("curuserdrives_module")]			= std::make_unique<LinkHandlerDrive>();
	m_LinkHandlers[_YTEXT("curusermessages_module")]		= std::make_unique<LinkHandlerMessages>();
    m_LinkHandlers[_YTEXT("curuserevents_module")]			= std::make_unique<LinkHandlerEvents>();
	m_LinkHandlers[_YTEXT("curusermessagerules_module")]	= std::make_unique<LinkHandlerMessageRules>();
	m_LinkHandlers[_YTEXT("curusergroupmemberships_module")]= std::make_unique<LinkHandlerGroupMemberships>();
	m_LinkHandlers[_YTEXT("curuserlicenses_module")]		= std::make_unique<LinkHandlerMyLicenses>();

	m_LinkHandlers[_YTEXT("users_recyclebin_module")]		= std::make_unique<LinkHandlerUsersRecycleBin>();
	m_LinkHandlers[_YTEXT("groups_recyclebin_module")]		= std::make_unique<LinkHandlerGroupsRecycleBin>();

	m_LinkHandlers[_YTEXT("reports_module")]				= std::make_unique<LinkHandlerO365Reports>();

	m_LinkHandlers[_YTEXT("show_jobcenter")]				= std::make_unique<LinkHandlerJobCenter>();

	m_LinkHandlers[_YTEXT("delete")]						= std::make_unique<LinkHandlerJobDelete>();
	m_LinkHandlers[_YTEXT("jobexec")]						= std::make_unique<LinkHandlerJobExecute>();
	m_LinkHandlers[_YTEXT("schedule")]						= std::make_unique<LinkHandlerJobSchedule>();
	m_LinkHandlers[_YTEXT("delsched")]						= std::make_unique<LinkHandlerJobScheduleDelete>();
	m_LinkHandlers[_YTEXT("editsched")]						= std::make_unique<LinkHandlerJobScheduleEdit>();
}