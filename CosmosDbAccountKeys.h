#pragma once

namespace Azure
{
	class CosmosDbAccountKeys
	{
	public:
		boost::YOpt<PooledString> m_PrimaryMasterKey;
		boost::YOpt<PooledString> m_SecondaryMasterKey;
	};
}
