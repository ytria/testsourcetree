#include "DefaultValueColumnDeserializer.h"

#include "JsonSerializeUtil.h"

void DefaultValueColumnDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("formula"), m_Data.Formula, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("value"), m_Data.Value, p_Object);
}
