#pragma once

namespace Sp
{
class List
{
public:
    boost::YOpt<bool> AllowContentTypes;
    boost::YOpt<int32_t> BaseTemplate;
    boost::YOpt<int32_t> BaseType;
    boost::YOpt<int32_t> BrowserFileHandling;
    //boost::YOpt<Sp::ContentTypeCollection> ContentTypes;
    boost::YOpt<bool> ContentTypesEnabled;
    boost::YOpt<YTimeDate> Created;
    //boost::YOpt<Sp::ListDataSource> DataSource;
    boost::YOpt<PooledString> DefaultContentApprovalWorkflowId;
    boost::YOpt<PooledString> DefaultDisplayFormUrl;
    boost::YOpt<PooledString> DefaultEditFormUrl;
    boost::YOpt<PooledString> DefaultNewFormUrl;
    //boost::YOpt<Sp::View> DefaultView;
    boost::YOpt<PooledString> DefaultViewUrl;
    boost::YOpt<PooledString> Description;
    boost::YOpt<PooledString> Direction;
    boost::YOpt<PooledString> DocumentTemplateUrl;
    boost::YOpt<int32_t> DraftVersionVisibility;
    //boost::YOpt<Sp::BasePermissions> EffectiveBasePermissions;
    //boost::YOpt<Sp::BasePermissions> EffectiveBasePermissionsForUI;
    boost::YOpt<bool> EnableAttachments;
    boost::YOpt<bool> EnableFolderCreation;
    boost::YOpt<bool> EnableMinorVersions;
    boost::YOpt<bool> EnableModeration;
    boost::YOpt<bool> EnableVersioning;
    boost::YOpt<PooledString> EntityTypeName;
    //boost::YOpt<Sp::EventReceiverDefinitionCollection> EventReceivers;
    //boost::YOpt<Sp::FieldCollection> Fields;
    //boost::YOpt<Sp::SecurableObject> Fields;
    boost::YOpt<bool> ForceCheckout;
    //boost::YOpt<Sp::FormCollection> Forms;
    boost::YOpt<bool> HasExternalDataSource;
    boost::YOpt<bool> HasUniqueRoleAssignments;
    boost::YOpt<bool> Hidden;
    boost::YOpt<PooledString> Id;
    boost::YOpt<PooledString> ImageUrl;
    //boost::YOpt<Sp::InformationRightsManagementSettings> InformationRightsManagementSettings;
    boost::YOpt<bool> IrmEnabled;
    boost::YOpt<bool> IrmExpire;
    boost::YOpt<bool> IrmReject;
    boost::YOpt<bool> IsApplicationList;
    boost::YOpt<bool> IsCatalog;
    boost::YOpt<bool> IsPrivate;
    boost::YOpt<bool> IsSiteAssetsLibrary;
    boost::YOpt<int32_t> ItemCount;
    //boost::YOpt<Sp::ListItemCollection> Items;
    boost::YOpt<YTimeDate> LastItemDeletedDate;
    boost::YOpt<YTimeDate> LastItemModifiedDate;
    boost::YOpt<PooledString> ListItemEntityTypeFullName;
    boost::YOpt<bool> MultipleDataList;
    boost::YOpt<bool> NoCrawl;
    boost::YOpt<bool> OnQuickLaunch;
    //boost::YOpt<Sp::Web> ParentWeb;
    boost::YOpt<PooledString> ParentWebUrl;
    //boost::YOpt<Sp::RoleAssignmentCollection> RoleAssignments;
    //boost::YOpt<Sp::Folder> RootFolder;
    boost::YOpt<PooledString> SchemaXml;
    boost::YOpt<bool> ServerTemplateCanCreateFolders;
    boost::YOpt<PooledString> TemplateFeatureId;
    boost::YOpt<PooledString> Title;
    //boost::YOpt<Sp::UserCustomActionCollection> UserCustomActions;
    boost::YOpt<PooledString> ValidationFormula;
    boost::YOpt<PooledString> ValidationMessage;
    //boost::YOpt<Sp::ViewCollection> Views;
    //boost::YOpt<Sp::Workflow::WorkflowAssociationCollection> WorkflowAssociations;
};
}

