#pragma once

#include "IButtonUpdater.h"

class GridFrameBase;

class GenericRevertSelectedButtonUpdater : public IButtonUpdater
{
public:
	GenericRevertSelectedButtonUpdater(GridFrameBase& p_Frame);
	void Update(CCmdUI* p_CmdUi) override;

private:
	GridFrameBase& m_Frame;
};

