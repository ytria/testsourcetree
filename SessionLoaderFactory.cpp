#include "SessionLoaderFactory.h"

#include "AdvancedSessionLoader.h"
#include "ElevatedSessionLoader.h"
#include "ISessionLoader.h"
#include "PartnerAdvancedSessionLoader.h"
#include "PartnerElevatedSessionLoader.h"
#include "RoleSessionLoader.h"
#include "StandardSessionLoader.h"
#include "UltraAdminSessionLoader.h"

SessionLoaderFactory::SessionLoaderFactory(const SessionIdentifier& p_Identifier)
	: m_Identifier(p_Identifier)
{
}

std::unique_ptr<ISessionLoader> SessionLoaderFactory::CreateLoader()
{
	if (m_Identifier.IsUltraAdmin())
		return std::make_unique<UltraAdminSessionLoader>(m_Identifier);
	if (m_Identifier.IsElevated())
		return std::make_unique<ElevatedSessionLoader>(m_Identifier);
	if (m_Identifier.IsRole())
		return std::make_unique<RoleSessionLoader>(m_Identifier);
	if (m_Identifier.IsAdvanced())
		return std::make_unique<AdvancedSessionLoader>(m_Identifier);
	if (m_Identifier.IsStandard())
		return std::make_unique<StandardSessionLoader>(m_Identifier);
	if (m_Identifier.IsPartnerAdvanced())
		return std::make_unique<PartnerAdvancedSessionLoader>(m_Identifier);
	if (m_Identifier.IsPartnerElevated())
		return std::make_unique<PartnerElevatedSessionLoader>(m_Identifier);

	ASSERT(false);
	throw std::exception("Unable to create switcher: session type unknown");
}
