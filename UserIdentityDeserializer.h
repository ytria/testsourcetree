#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "UserIdentity.h"

class UserIdentityDeserializer : public JsonObjectDeserializer, public Encapsulate<UserIdentity>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

