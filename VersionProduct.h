// This file is also includes in the .rc2 file. It's important to keep the trailing "\0" in the defined
// strings below.
#define STRCOMPANYNAME		"Ytria Inc.\0"
#ifdef _WIN64
	#define STRFILEDESCRIPTION	"sapio365 (x64)\0"
#else
	#define STRFILEDESCRIPTION	"sapio365\0"
#endif
#define STRINTERNALNAME		"sapio365\0"
#define STRLEGALCOPYRIGHT	"Copyright � 2006-2020 Ytria, Inc.\0"
#define STRORIGINALFILENAME	"sapio365.exe\0"
#define STRPRODUCTNAME		"Ytria Inc. sapio365\0"
