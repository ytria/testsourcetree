#pragma once

#include "BusinessObject.h"
#include "CacheGrid.h"
#include "DlgFormsHTML.h"

template<class T>
class DlgGenPropModifHTML	: public DlgFormsHTML
{
public:
	DlgGenPropModifHTML(vector<T>& p_Objects, DlgFormsHTML::Action p_Action, bool p_ObjectsAreMoreLoaded, CWnd* p_Parent);
	virtual ~DlgGenPropModifHTML();

	virtual void generateJSONScriptData();

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data);

private:
	vector<T>& m_Objects;
	bool m_ObjectsAreMoreLoaded;
};

#include "DlgGenPropModifHTML.hpp"
