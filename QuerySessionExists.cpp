#include "QuerySessionExists.h"
#include "InlineSqlQueryRowHandler.h"
#include "SqlQuerySelect.h"
#include "SessionsSqlEngine.h"

QuerySessionExists::QuerySessionExists(const SessionIdentifier& p_Identifier, SessionsSqlEngine& p_Engine)
	:m_Identifier(p_Identifier),
	m_Engine(p_Engine),
	m_SessionId(0),
	m_Exists(false)
{
}

void QuerySessionExists::Run()
{
	auto handler = [this](sqlite3_stmt* p_Stmt) {
		m_Exists = true;
		m_SessionId = sqlite3_column_int64(p_Stmt, 0);
	};

	SqlQuerySelect queryExists(SessionsSqlEngine::Get(), InlineSqlQueryRowHandler<decltype(handler)>(handler), _YTEXT(R"(SELECT Id FROM Sessions WHERE EmailOrAppId=? AND SessionType=? AND RoleId=?)"));
	queryExists.BindString(1, m_Identifier.m_EmailOrAppId);
	queryExists.BindString(2, m_Identifier.m_SessionType);
	queryExists.BindInt64(3, m_Identifier.m_RoleID);
	queryExists.Run();

	m_Status = queryExists.GetStatus();
}

bool QuerySessionExists::Exists() const
{
	return m_Exists;
}

int64_t QuerySessionExists::GetSessionId() const
{
	return m_SessionId;
}
