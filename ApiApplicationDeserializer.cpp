#include "ApiApplicationDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "PermissionScope.h"
#include "PermissionScopeDeserializer.h"
#include "PreAuthorizedApplication.h"
#include "PreAuthorizedApplicationDeserializer.h"

void ApiApplicationDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeBool(_YTEXT("acceptMappedClaims"), m_Data.m_AcceptMappedClaims, p_Object);

	{
		ListStringDeserializer knownClientAppsDeserializer;
		if (JsonSerializeUtil::DeserializeAny(knownClientAppsDeserializer, _YTEXT("knownClientApplications"), p_Object))
			m_Data.m_KnownClientApplications = std::move(knownClientAppsDeserializer.GetData());
	}

	{
		ListDeserializer<PermissionScope, PermissionScopeDeserializer>  oauth2PermScopesDeserializer;
		if (JsonSerializeUtil::DeserializeAny(oauth2PermScopesDeserializer, _YTEXT("oauth2PermissionScopes"), p_Object))
			m_Data.m_OAuth2PermissionScopes = std::move(oauth2PermScopesDeserializer.GetData());
	}

	{
		ListDeserializer<PreAuthorizedApplication, PreAuthorizedApplicationDeserializer> preAuthAppsDeserializer;
		if (JsonSerializeUtil::DeserializeAny(preAuthAppsDeserializer, _YTEXT("preAuthorizedApplications"), p_Object))
			m_Data.m_PreAuthorizedApplications = std::move(preAuthAppsDeserializer.GetData());
	}

	JsonSerializeUtil::DeserializeInt32(_YTEXT("requestedAccessTokenVersion"), m_Data.m_RequestedAccessTokenVersion, p_Object);
}
