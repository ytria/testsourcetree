#pragma once

#include "IXmlSerializer.h"

namespace Ex
{
    class DelegatePermissions;
}

namespace Ex
{
class DelegatePermissionsSerializer : public IXmlSerializer
{
public:
    DelegatePermissionsSerializer(XERCES_CPP_NAMESPACE::DOMDocument& p_Document, const Ex::DelegatePermissions& p_Obj);

protected:
    void SerializeImpl(DOMElement& p_Element) override;

private:
    const Ex::DelegatePermissions& m_Obj;
};
}

