#pragma once

#include "DbGroupSerializer.h"
#include "DbUserSerializer.h"

template<class Object>
const std::vector<wstring>& SqlCacheConfig<Object>::DataBlockNames()
{
	static const std::vector<wstring> names = []()
	{
		std::vector<wstring> names;
		for (const auto& item : SqlCacheObjectSpecific<Object>::DataBlocks())
			names.push_back(item.first);
		return names;
	}();

	return names;
}

template<class Object>
const std::vector<PooledString>& SqlCacheConfig<Object>::DataBlockFieldsByName(const wstring& p_BlockName)
{
	auto it = SqlCacheObjectSpecific<Object>::DataBlocks().find(p_BlockName);
	ASSERT(SqlCacheObjectSpecific<Object>::DataBlocks().end() != it);
	return it->second;
}

template<class Object>
const std::vector<PooledString>& SqlCacheConfig<Object>::DataBlockFieldsById(typename SqlCacheObjectSpecific<Object>::BlockId p_BlockId)
{
	auto it = SqlCacheObjectSpecific<Object>::DataBlocks().find(SqlCacheObjectSpecific<Object>::DataBlockName(p_BlockId));
	ASSERT(SqlCacheObjectSpecific<Object>::DataBlocks().end() != it);
	return it->second;
}

template<class Object>
const std::vector<wstring> SqlCacheConfig<Object>::DataBlockNames(const std::vector<typename SqlCacheObjectSpecific<Object>::BlockId>& p_BlockIds)
{
	std::vector<wstring> blocks;
	for (const auto& blockId : p_BlockIds)
		blocks.push_back(SqlCacheObjectSpecific<Object>::DataBlockName(blockId));
	return blocks;
}

template<class Object>
wstring SqlCacheConfig<Object>::DataBlockDateFieldByName(const wstring& p_BlockName)
{
	return wstring(_YTEXT("_blockdate_")) + p_BlockName;
}

template<class Object>
wstring SqlCacheConfig<Object>::DataBlockDateFieldById(typename SqlCacheObjectSpecific<Object>::BlockId p_BlockId)
{
	return DataBlockDateFieldByName(SqlCacheObjectSpecific<Object>::DataBlockName(p_BlockId));
}

