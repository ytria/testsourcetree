#pragma once

namespace Azure
{
	class Subscription
	{
	public:
		bool operator<(const Azure::Subscription& p_Other) const;
		boost::YOpt<PooledString> m_SubscriptionId;
		boost::YOpt<PooledString> m_DisplayName;
		boost::YOpt<PooledString> m_State;
	};
}

