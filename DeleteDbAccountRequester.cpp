#include "DeleteDbAccountRequester.h"

#include "AzureSession.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "BasicHttpRequestLogger.h"

Azure::DeleteDbAccountRequester::DeleteDbAccountRequester(const wstring& p_SubscriptionId, const wstring& p_ResourceGroup, const wstring& p_DbAccountName)
	: m_SubscriptionId(p_SubscriptionId),
	m_ResourceGroup(p_ResourceGroup),
	m_DbAccountName(p_DbAccountName)
{
}

TaskWrapper<void> Azure::DeleteDbAccountRequester::Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri(_YTEXT("subscriptions"));
	uri.append_path(m_SubscriptionId);
	uri.append_path(_YTEXT("resourceGroups"));
	uri.append_path(m_ResourceGroup);
	uri.append_path(_YTEXT("providers"));
	uri.append_path(_YTEXT("Microsoft.DocumentDB"));
	uri.append_path(_YTEXT("databaseAccounts"));
	uri.append_path(m_DbAccountName);
	uri.append_query(AzureSession::g_ApiVersionKey, _YTEXT("2015-04-08"));

	return p_Session->GetAzureSession()->Delete(uri.to_uri(), std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier()), m_Result, nullptr, p_TaskData);
}
