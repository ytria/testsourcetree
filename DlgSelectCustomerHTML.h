#pragma once

#include "Contract.h"
#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "SessionIdentifier.h"
#include "YAdvancedHtmlView.h"

#include <memory>

class ModuleBase;

class DlgSelectCustomerHTML	: public ResizableDialog
								, public YAdvancedHtmlView::IPostedDataTarget
{
public:
	DlgSelectCustomerHTML(const vector<Contract>& p_Contracts);

    int GetSelectedContractIndex() const;

	enum { IDD = IDD_DLG_DEFAULT_HTML };

	/* YAdvancedHtmlView::IPostedDataTarget */
	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data);

protected:
    virtual BOOL OnInitDialogSpecificResizable() override;
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    DECLARE_MESSAGE_MAP()

    ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);
	web::json::value getJSONSessionEntry(const Contract& p_Contract, size_t p_Index);
	static UINT getHbsHeight(const wstring& hbsName);

private:
	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	static std::map<wstring, int, Str::keyLessInsensitive> g_HbsHeights;
	int m_totalHbsHeight;
	int m_MaxWidth;

	const vector<Contract>& m_Contracts;
	int m_Selected;
};

