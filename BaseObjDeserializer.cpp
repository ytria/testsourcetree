#include "BaseObjDeserializer.h"

#include "JsonSerializeUtil.h"

void Cosmos::BaseObjDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
    JsonSerializeUtil::DeserializeString(_YTEXT("_id"), m_Data._id, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("_etag"), m_Data._etag, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("_ts"), m_Data._ts, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("_rid"), m_Data._rid, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("_self"), m_Data._self, p_Object);
}

void Cosmos::BaseObjDeserializer::Deserialize(const web::json::object& p_Object, Cosmos::BaseObj& p_BaseObj)
{
    BaseObjDeserializer deserializer;
    deserializer.DeserializeObject(p_Object);
    p_BaseObj = deserializer.GetData();
}
