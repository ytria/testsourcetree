#include "FileSystemInfoDeserializer.h"

#include "JsonSerializeUtil.h"

void FileSystemInfoDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.CreatedDateTime, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.LastModifiedDateTime, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastAccessedDateTime"), m_Data.LastAccessedDateTime, p_Object);
}
