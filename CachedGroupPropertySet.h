#pragma once

#include "IPropertySetBuilder.h"

class CachedGroupPropertySet : public IPropertySetBuilder
{
public:
    virtual vector<rttr::property> GetPropertySet() const override;
};

class CachedDeletedGroupPropertySet : public CachedGroupPropertySet
{
public:
	virtual vector<rttr::property> GetPropertySet() const override;
};

