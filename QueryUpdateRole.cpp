#include "QueryUpdateRole.h"
#include "SessionsSqlEngine.h"
#include "SqlQueryPreparedStatement.h"

QueryUpdateRole::QueryUpdateRole(int64_t p_RoleId, const wstring& p_Name, SessionsSqlEngine& p_Engine)
	: m_Engine(p_Engine),
	m_RoleId(p_RoleId),
	m_Name(p_Name)
{
}

void QueryUpdateRole::Run()
{
	wstring queryStr = _YTEXT(R"(UPDATE Roles SET Name=? WHERE Id=?)");
	SqlQueryPreparedStatement query(m_Engine, queryStr);
	query.BindString(1, m_Name);
	query.BindInt64(2, m_RoleId);
	query.Run();

	m_Status = query.GetStatus();
}
