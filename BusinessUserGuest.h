#pragma once

#include "BusinessUser.h"

// only for identification issues in grids
class BusinessUserGuest : public BusinessUser
{
public:
	using BusinessUser::BusinessUser;

	RTTR_ENABLE(BusinessUser)
};
