#pragma once

#include "GridFrameBase.h"
#include "OnPremiseUser.h"
#include "GridOnPremiseUsers.h"

class FrameOnPremiseUsers : public GridFrameBase
{
public:
	FrameOnPremiseUsers(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

	DECLARE_DYNAMIC(FrameOnPremiseUsers)
	virtual ~FrameOnPremiseUsers() = default;

	void ShowOnPremiseUsers(const vector<OnPremiseUser>& p_OnPremiseUsers, bool p_FullPurge);

	virtual void ApplyAllSpecific();
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
	virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

	virtual O365Grid& GetGrid() override;

protected:
	virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

	//virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	GridOnPremiseUsers m_OnPremiseUsersGrid;
};

