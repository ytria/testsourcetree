#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerAuditLogs : public IBrowserLinkHandler
{
public:
	void Handle(YBrowserLink& p_Link) const override;
};

