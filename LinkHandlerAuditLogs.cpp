#include "LinkHandlerAuditLogs.h"

#include "Command.h"
#include "CommandInfo.h"
#include "CommandDispatcher.h"

void LinkHandlerAuditLogs::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	info.SetOrigin(Origin::Tenant);
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::DirectoryAudit, Command::ModuleTask::List, info, { nullptr, g_ActionNameShowAuditLogs, nullptr, p_Link.GetAutomationAction() }));
}
