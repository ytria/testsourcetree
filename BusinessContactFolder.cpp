#include "BusinessContactFolder.h"

BusinessContactFolder::BusinessContactFolder(const ContactFolder& p_ContactFolder)
{
    SetDisplayName(p_ContactFolder.DisplayName);
    SetParentFolderId(p_ContactFolder.ParentFolderId);

    vector<BusinessContactFolder> childrenContactFolders;
    for (const auto& childrenFolder : p_ContactFolder.m_ChildrenFolders)
        childrenContactFolders.emplace_back(childrenFolder);
    SetChildrenContactFolders(childrenContactFolders);

    vector<BusinessContact> contacts;
    for (const auto& contact : p_ContactFolder.m_Contacts)
        contacts.emplace_back(contact);
    SetContacts(contacts);
}

const boost::YOpt<PooledString>& BusinessContactFolder::GetDisplayName() const
{
    return m_DisplayName;
}

const boost::YOpt<PooledString>& BusinessContactFolder::GetParentFolderId() const
{
    return m_ParentFolderId;
}

void BusinessContactFolder::SetDisplayName(const boost::YOpt<PooledString>& p_DisplayName)
{
    m_DisplayName = p_DisplayName;
}

void BusinessContactFolder::SetParentFolderId(const boost::YOpt<PooledString>& p_ParentFolderId)
{
    m_ParentFolderId = p_ParentFolderId;
}

const vector<BusinessContactFolder>& BusinessContactFolder::GetChildrenContactFolders() const
{
    return m_ChildrenContactFolders;
}

void BusinessContactFolder::SetChildrenContactFolders(const vector<BusinessContactFolder>& p_Val)
{
    m_ChildrenContactFolders = p_Val;
}

const vector<BusinessContact>& BusinessContactFolder::GetContacts() const
{
    return m_Contacts;
}

void BusinessContactFolder::SetContacts(const vector<BusinessContact>& p_Val)
{
    m_Contacts = p_Val;
}
