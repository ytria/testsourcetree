#pragma once

class CachedObjectsSqlEngine;

struct SessionIdentifier;

class PersistentCacheUtil
{
public:
	PersistentCacheUtil() = default;

	static bool TableEmpty(const CachedObjectsSqlEngine& p_Sql, const wstring& p_TableName);

	static bool DatabaseExists(const CachedObjectsSqlEngine& p_Sql, const SessionIdentifier& p_SessionId);
	static int DeleteDatabase(const SessionIdentifier& p_SessionId);

	static wstring GetFilenameFromSessionAndMigrateIfNecessary(const SessionIdentifier& p_SessionId, const int p_CurrentVersion);

private:
	// Specify template for custom migration -- Default is just file rename.
	template<int vFrom, int vTo>
	static void migrateDatabase(const SessionIdentifier& p_SessionId);

	static bool databaseExists(const SessionIdentifier& p_SessionId, const int p_Version);
	static wstring getFilenameFromSession(const SessionIdentifier& p_SessionId, const int p_Version);
	static int deleteDatabase(const SessionIdentifier& p_SessionId, const int p_Version);
};

