#include "ErrorRecordCollectionDeserializer.h"
#include "PSSerializeUtil.h"

void ErrorRecordCollectionDeserializer::Deserialize(IPSObjectPropertyReader& p_Reader)
{
	ErrorRecord errorRecord;

	errorRecord.m_CategoryInfoReason = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("CategoryInfo.Reason"));
	
	m_Data.push_back(errorRecord);
}

void ErrorRecordCollectionDeserializer::SetCount(size_t p_Count)
{
	m_Data.reserve(p_Count);
}

const std::vector<ErrorRecord>& ErrorRecordCollectionDeserializer::GetData() const
{
	return m_Data;
}
