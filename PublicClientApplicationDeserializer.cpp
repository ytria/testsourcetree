#include "PublicClientApplicationDeserializer.h"
#include "ListDeserializer.h"

void PublicClientApplicationDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		ListStringDeserializer listStrDeserializer;
		if (JsonSerializeUtil::DeserializeAny(listStrDeserializer, _YTEXT("redirectUris"), p_Object))
			m_Data.m_RedirectUris = std::move(listStrDeserializer.GetData());
	}
}
