#pragma once

#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "ResourceGroup.h"
#include "Subscription.h"
#include "YAdvancedHtmlView.h"

class Sapio365Session;

class DlgNewCosmosDbAccount : public ResizableDialog, public YAdvancedHtmlView::IPostedDataTarget
{
public:
	DlgNewCosmosDbAccount(const map<Azure::Subscription, vector<Azure::ResourceGroup>>& p_Choices, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent);

	enum { IDD = IDD_DLG_NEW_COSMOS_ACCOUNT };

	/* YAdvancedHtmlView::IPostedDataTarget */
	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data) override;

	const wstring& GetSubscriptionId() const;
	const wstring& GetResGroupName() const;

protected:
	virtual BOOL OnInitDialogSpecificResizable() override;

	ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);
	void AddDynamicFields(web::json::object& p_Object);
	
	const map<Azure::Subscription, vector<Azure::ResourceGroup>>& m_Choices;
	std::shared_ptr<Sapio365Session> m_Session;
	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;

	wstring m_SubscriptionId;
	wstring m_ResGroupName;

	static const wstring g_SubscriptionIdKey;
	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;
};

