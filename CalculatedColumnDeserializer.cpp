#include "CalculatedColumnDeserializer.h"

#include "JsonSerializeUtil.h"

void CalculatedColumnDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("format"), m_Data.Format, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("formula"), m_Data.Formula, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("outputType"), m_Data.OutputType, p_Object);
}
