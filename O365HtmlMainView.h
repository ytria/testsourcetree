#pragma once

#include "JobCenterUtil.h"
#include "JSONUtil.h"
#include "YAdvancedHtmlView.h"

class IBrowserLinkHandler;

class O365HtmlMainView : public YAdvancedHtmlView, public YAdvancedHtmlView::IPostedDataTarget
{
public:
	DECLARE_DYNCREATE(O365HtmlMainView)
	O365HtmlMainView();
	virtual ~O365HtmlMainView() = default;

	void Navigate(const wstring& url) override;

    void EnableUserMode();
    void EnableAdminMode();
	void EnableOnPremise();

	void RefreshHTMLBulldozer();// probably obsolete
	void UpdateAJLInfo();
	void RemoveOneJob(const Script& p_Job);
	void RemoveOneSchedule(const ScriptSchedule& p_JobSchedule);
	void UpdateAll();
	void UpdateAllJobs();
	void UpdateAllSchedules();
	void UpdateCanRemoveJobs();

	bool HasNoJob() const;
	void SetReadyForExecuteScript();

	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data) override final;

protected:
	virtual void OnBeforeNavigate2(LPCTSTR lpszURL, DWORD nFlags, LPCTSTR lpszTargetFrameName, CByteArray& baPostedData, LPCTSTR lpszHeaders, BOOL* pbCancel) override;

protected:
	DECLARE_MESSAGE_MAP()

private:
    void registerLinkHandlers();
	void registerScriptsAndLinksToReplace();
	void clearScriptsAndLinksToReplace();
	void generateJSONScriptData(wstring& p_Output);
	void generateJSONscriptDataJS(wstring& p_Output);

	vector<Script> getAllJobs();

	json::value makeJSONjob(Script& p_Job);
	json::value makeJSONschedule(const ScriptSchedule& js, const Script& j, const wstring& p_JobPresetKey);

private:
    std::unordered_map<wstring, std::unique_ptr<IBrowserLinkHandler>> m_LinkHandlers;
	static const wstring g_AJLtag;
	static const wstring g_PostedDataButtonFeedback;
	static const wstring g_PostedDataButtonAJL;
	static const wstring g_PostedDataAJLlist;

	bool m_HasNoJob;
	bool m_IsReadyForExecuteScript;
	
	const bool m_IsSandbox;
};
