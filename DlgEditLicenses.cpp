#include "DlgEditLicenses.h"

#include "AutomationNames.h"
#include "GridLicenses.h"
#include "GridUtil.h"
#include "SkuAndPlanReference.h"
#include "SkuInfo.h"

const wstring DlgEditLicenses::g_ON		= _YTEXT("on");
const wstring DlgEditLicenses::g_OFF	= _YTEXT("off");
const wstring DlgEditLicenses::g_MIX	= _YTEXT("mix");

DlgEditLicenses::DlgEditLicenses(GridLicenses& p_Grid, CWnd* p_Parent)
	: DlgFormsHTML(Action::EDIT, p_Parent, g_ActionNameSelectedEditLicense),
    m_Grid(p_Grid)
{
}

const map<PooledString, std::set<SkuInfo>>& DlgEditLicenses::GetUsersSkus() const
{
    return m_UserSkus;
}

const map<PooledString, std::set<SkuInfo>>& DlgEditLicenses::GetNewUsersSkus() const
{
	return m_NewUserSkus;
}

void DlgEditLicenses::generateJSONScriptData()
{
    initMain(_YTEXT("DlgEditLicenses"));

    std::set<GridBackendRow*> parentRows;
	for (auto row : m_Grid.GetSelectedRows())
	{
		if (!row->IsGroupRow())
			parentRows.insert(row->GetTopAncestorOrThis());
	}

	// m_JsonMain[_YTEXT("formTitle")] = web::json::value::string(YtriaTranslate::Do(DlgEditLicenses_generateJSONScriptData_1, _YLOC("Edit Licenses")).c_str());
	if (parentRows.size() > 1)
		m_JsonMain[_YTEXT("formTitle")] = web::json::value::string(YtriaTranslate::Do(DlgEditLicenses_generateJSONScriptData_2, _YLOC("Edit Licenses (%1 users selected)"), Str::getStringFromNumber(parentRows.size()).c_str()));
	else if (parentRows.size() == 1)
		m_JsonMain[_YTEXT("formTitle")] = web::json::value::string(YtriaTranslate::Do(DlgEditLicenses_generateJSONScriptData_3, _YLOC("Edit Licenses for %1"), m_Grid.GetDisplayName(*parentRows.begin()).c_str()));

	std::set<SkuInfo> allSkus; // "Enabled" value makes no sense in this container. Don't use it! Instead, we need to check in m_UserSkus to get the real values

    // Loop over all service plan rows. For each plan row, put the user, sku info and plan info in a map.
    for (auto row : m_Grid.GetBackendRows())
    {
        if (GridUtil::isBusinessType<ServicePlanInfo>(row))
        {
            auto userRowItt = parentRows.find(row->GetTopAncestorOrThis()); // Is this row a child of a selected row?
            if (userRowItt != parentRows.end())
            {
                auto skuRow = row->GetParentRow();
                ASSERT(nullptr != skuRow);
                if (nullptr != skuRow)
                {
                    PlanInfo plan(row->GetField(m_Grid.GetColServicePlanID()).GetValueStr());
					plan.Name = row->GetField(m_Grid.GetColServicePlanName()).GetValueStr();
					plan.FriendlyName = row->GetField(m_Grid.GetColServicePlanFriendlyName()).GetValueStr();
                    plan.Enabled = row->GetField(m_Grid.GetColIsAssignedBool()).GetValueBool();
                    vector<GridBackendField> planRowPk;
                    m_Grid.GetRowPK(row, planRowPk);
                    plan.RowPk = planRowPk;

                    SkuInfo skuInfo(skuRow->GetField(m_Grid.GetColSkuId()).GetValueStr());

                    auto& userSkus = m_UserSkus[(*userRowItt)->GetField(m_Grid.GetColId()).GetValueStr()];
                    auto itt = userSkus.find(skuInfo);
                    if (itt != userSkus.end())
                    {
                        itt->Plans.insert(plan);
                        
                        auto ittSku = allSkus.find(skuInfo);
                        ASSERT(ittSku != allSkus.end());
                        if (ittSku != allSkus.end())
                            ittSku->Plans.insert(plan);
                    }
                    else
                    {
						skuInfo.PartNumber = skuRow->GetField(m_Grid.GetColSkuPartNumber()).GetValueStr();
						skuInfo.Name = skuRow->GetField(m_Grid.GetColSkuName()).GetValueStr();
						if (nullptr != m_Grid.GetColConsumedUnitsInRbacScope() && skuRow->HasField(m_Grid.GetColConsumedUnitsInRbacScope()))
							skuInfo.Consumed = static_cast<int>(skuRow->GetField(m_Grid.GetColConsumedUnitsInRbacScope()).GetValueLong());
						else
							skuInfo.Consumed = static_cast<int>(skuRow->GetField(m_Grid.GetColConsumedUnits()).GetValueLong());
						if (nullptr != m_Grid.GetColEnabledUnitsInRbacScope() && skuRow->HasField(m_Grid.GetColEnabledUnitsInRbacScope()))
							skuInfo.Total = static_cast<int>(skuRow->GetField(m_Grid.GetColEnabledUnitsInRbacScope()).GetValueLong());
						else
							skuInfo.Total = static_cast<int>(skuRow->GetField(m_Grid.GetColEnabledUnits()).GetValueLong());
                        skuInfo.Enabled = skuRow->GetField(m_Grid.GetColIsAssignedBool()).GetValueBool();
                        vector<GridBackendField> skuRowPk;
                        m_Grid.GetRowPK(skuRow, skuRowPk);
                        skuInfo.RowPk = skuRowPk;
                        skuInfo.Plans.insert(plan);
						skuInfo.OffMixMode = !((int)parentRows.size() <= (skuInfo.Total - skuInfo.Consumed));
                        userSkus.insert(skuInfo);
                        allSkus.insert(skuInfo);
                    }
                }
            }
        }
    }

    // Now that we have a map with the skus/plans for each user, we can generate the json
    for (auto&& item : generateItems(allSkus))
		getGenerator().addItem(std::move(item), 300);

    getGenerator().addApplyButton(LocalizedStrings::g_ApplyButtonText);
    getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
    getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);
}

bool DlgEditLicenses::processPostedData(const IPostedDataTarget::PostedData& data)
{
	// We assume that all skus exists for all users
	const auto& skus = m_UserSkus.begin()->second;

	m_NewUserSkus = m_UserSkus;
	std::set<wstring> newPlans;
	std::set<wstring> allEnabledPlans;

	for (const auto& keyVal : data)
	{
		const auto& entityId = keyVal.first;
		const auto& entityState = keyVal.second;

		auto ittSku = skus.find(SkuInfo(entityId));
		for (auto& userStateKeyVal : m_NewUserSkus)
		{
			if (ittSku != skus.end())
			{
				// Sku
				auto ittUserSku = userStateKeyVal.second.find(SkuInfo(entityId));
				ASSERT(ittUserSku != userStateKeyVal.second.end());
				if (ittUserSku != userStateKeyVal.second.end() && (entityState == DlgEditLicenses::g_ON || entityState == DlgEditLicenses::g_OFF))
					ittUserSku->Enabled = entityState == DlgEditLicenses::g_ON;
			}
			else
			{
				// Plan
				auto skuIdAndPlanId = Str::explodeIntoVector(entityId, wstring(_YTEXT("_")));
				ASSERT(skuIdAndPlanId.size() == 2);
				if (skuIdAndPlanId.size() == 2)
				{
					bool planFound = false;
					PooledString skuId = skuIdAndPlanId[0];
					PooledString planId = skuIdAndPlanId[1];

					auto ittCorrespondingSku = userStateKeyVal.second.find(skuId);
					ASSERT(ittCorrespondingSku != userStateKeyVal.second.end());
					if (ittCorrespondingSku != userStateKeyVal.second.end())
					{
						auto& desiredPlans = ittCorrespondingSku->Plans;
						auto ittUserPlan = desiredPlans.find(PlanInfo(planId));
						if (ittUserPlan != desiredPlans.end())
						{
							if (entityState == DlgEditLicenses::g_ON)
							{
								allEnabledPlans.insert(planId);
								if (!ittUserPlan->Enabled)
									newPlans.insert(planId);
								ittUserPlan->Enabled = true;
							}
							else if (entityState == DlgEditLicenses::g_OFF)
								ittUserPlan->Enabled = false;

							planFound = true;
						}
					}
					ASSERT(planFound);
				}
			}
		}
	}

	bool close = true;

	if (!newPlans.empty())
	{
		std::map<wstring, std::set<wstring>> plansCombinationsWarning;
		for (const auto& newPlan : newPlans)
		{
			const auto& exclusions = SkuAndPlanReference::GetInstance().GetPlanExclusionsForPlan(newPlan);

			auto& comb = plansCombinationsWarning[newPlan];
			for (const auto& plan : allEnabledPlans)
			{
				if (plan != newPlan && exclusions.end() != exclusions.find(plan))
					comb.insert(plan);
			}
			if (comb.empty())
				plansCombinationsWarning.erase(newPlan);
		}

		auto getPlanDisplayName = [](const auto& id)
		{
			auto planName = SkuAndPlanReference::GetInstance().GetPlanLabel(id);
			if (planName.IsEmpty())
			{
				planName = SkuAndPlanReference::GetInstance().GetPlanName(id);
				if (planName.IsEmpty())
					planName = id;
			}
			return planName;
		};

		if (!plansCombinationsWarning.empty())
		{
			std::vector<wstring> warnings;
			for (const auto& combWarning : plansCombinationsWarning)
			{
				ASSERT(!combWarning.second.empty());

				wstring warning = _YFORMAT(L"%s is incompatible with:", getPlanDisplayName(combWarning.first).c_str());
				for (const auto& otherPlan : combWarning.second)
				{
					warning += _YFORMAT(L"\n- %s", getPlanDisplayName(otherPlan).c_str());
				}
				warnings.push_back(std::move(warning));
			}

			if (!warnings.empty())
			{
				YCodeJockMessageBox dlg(
					this,
					DlgMessageBox::eIcon_ExclamationWarning,
					_T("Forbidden plan combinations"),
					Str::implode(warnings, _YTEXT("\n"), false),
					_T("You may get an error upon saving. Continue?"),
					{ { IDOK, _T("OK") }, { IDCANCEL, _T("Cancel") } });

				close = IDOK == dlg.DoModal();
			}
		}
	}

    return close;
}

wstring DlgEditLicenses::getHbsJs() const
{
    return _YTEXT("hbs-editLicenses.js");
}

wstring DlgEditLicenses::getOtherJs() const
{
    return _YTEXT("editLicenses.js");
}

wstring DlgEditLicenses::getCss() const
{
    return _YTEXT("editLicenses.css");
}

vector<web::json::value> DlgEditLicenses::generateItems(const std::set<SkuInfo>& p_Skus)
{
    vector<web::json::value> items;

	size_t onChangeIndex = 0;
    for (const auto& sku : p_Skus)
    {
		wstring skuState = GetSkuState(m_UserSkus, sku);

		uint32_t dummyFlags		= 0;
		bool automationValue	= false;
		bool automated			= getAutomationValue(sku.PartNumber, automationValue, dummyFlags);

		wstring ratioConsumed;
		enum { FULL, FREE, MIX } mode = FREE;
		if (sku.OffMixMode && skuState != g_ON && sku.Consumed != sku.Total)
		{
			mode = MIX;
			ratioConsumed = YtriaTranslate::Do(DlgEditLicenses_generateItems_3, _YLOC("Used %1 / Total %2 / Available %3 (not enough)"), std::to_wstring(sku.Consumed).c_str(), std::to_wstring(sku.Total).c_str(), std::to_wstring(sku.Total - sku.Consumed).c_str());
		}
		else
		{
			if (sku.Consumed == sku.Total)
				mode = FULL;
			ratioConsumed = YtriaTranslate::Do(DlgEditLicenses_generateItems_4, _YLOC("Used %1 / Total %2 / Available %3"), std::to_wstring(sku.Consumed).c_str(), std::to_wstring(sku.Total).c_str(), std::to_wstring(sku.Total - sku.Consumed).c_str());
		}

		auto item = json::value::object
		({
			{ _YTEXT("hbs"),				    web::json::value::string(_YTEXT("licensesList")) },
			{ _YTEXT("fontIcon"),			    web::json::value::string(_YTEXT("fal fa-chevron-right")) },
			{ _YTEXT("name"),				    web::json::value::string(sku.Id) },
			{ _YTEXT("labelParentSection"),     web::json::value::string(sku.Name) },
			{ _YTEXT("subLabelParentSection"),  web::json::value::string(sku.PartNumber) },
			{ _YTEXT("state"),				    web::json::value::string(skuState) },
			{ _YTEXT("currentUsed"),		    web::json::value::string(ratioConsumed) },
			{ _YTEXT("choices"),			    web::json::value::array() }
		});

		if (MIX == mode)
		{
			item.as_object()[_YTEXT("offMixMode")] = web::json::value::string(_YTEXT("true"));
			if (automated && automationValue)
			{
				automated = false;
				addAutomationError(YtriaTranslate::Do(DlgEditLicenses_generateItems_1, _YLOC("Not enough licenses available")).c_str(), sku.PartNumber, _YTEXT("true"));
			}
		}

		if (FULL == mode)
		{
            item.as_object()[_YTEXT("full")] = web::json::value::string(_YTEXT("true"));
			if (automated && automationValue && g_ON != skuState)
			{
				automated = false;
				addAutomationError(YtriaTranslate::Do(DlgEditLicenses_generateItems_1, _YLOC("Not enough licenses available")).c_str(), sku.PartNumber, _YTEXT("true"));
			}
		}
		
		if (automated)
		{
			skuState = automationValue ? g_ON : g_OFF;
			item.as_object()[_YTEXT("state")] = web::json::value::string(skuState);
			if (m_JsonMain[_YTEXT("onchange")].is_null())
				m_JsonMain[_YTEXT("onchange")] = web::json::value::array();
			m_JsonMain[_YTEXT("onchange")].as_array()[onChangeIndex++]
				= web::json::value::object({	{ _YTEXT("name"),	web::json::value::string(sku.Id) },
												{ _YTEXT("state"),	web::json::value::string(skuState) }
											});
		}

        generateItemChoices(sku, skuState, item);
        items.push_back(std::move(item));
    }

    return items;
}

wstring DlgEditLicenses::GetSkuState(const map<PooledString, std::set<SkuInfo>>& p_UserSkus, const SkuInfo& sku) const
{
    wstring state;

    bool atLeastOneAssigned = false;
    bool atLeastOneNotAssigned = false;

    for (const auto& userSku : p_UserSkus)
    {
        const auto& userSkus = userSku.second;
        auto userSkuInfoItt = userSkus.find(sku);
        ASSERT(userSkuInfoItt != userSkus.end());
        if (userSkuInfoItt != userSkus.end())
        {
            if (userSkuInfoItt->Enabled)
                atLeastOneAssigned = true;
            else
                atLeastOneNotAssigned = true;
        }
        if (atLeastOneAssigned && atLeastOneNotAssigned)
            break;
    }

    if (atLeastOneAssigned && !atLeastOneNotAssigned)
        state = g_ON;
    else if (!atLeastOneAssigned && atLeastOneNotAssigned)
        state = g_OFF;
    else if (atLeastOneAssigned && atLeastOneNotAssigned)
        state = g_MIX;

    return state;
}

void DlgEditLicenses::generateItemChoices(const SkuInfo& p_Sku, const wstring& p_SkuState, web::json::value& p_Item) const
{
    auto& choices = p_Item.as_object()[_YTEXT("choices")].as_array();
    SkuInfo ctxSku(p_Sku.Id);

    if (p_SkuState == g_OFF)
    {
        size_t index = 0;
        for (const auto& plan : p_Sku.Plans)
        {
            choices[index] = web::json::value::object({
				{ _YTEXT("labelField"), web::json::value::string(plan.FriendlyName) },
				{ _YTEXT("subLabelField"), web::json::value::string(plan.Name) },
                { _YTEXT("name"), web::json::value::string(p_Sku.Id + wstring(_YTEXT("_")) + plan.Id) }
            });
            ++index;
        }
    }
    else
    {
        size_t index = 0;
        for (const auto& plan : p_Sku.Plans)
        {
            wstring planState;

            bool deactivatedAtLeastOnce = false;
            bool activatedAtLeastOnce = false;

            for (const auto& userSkus : m_UserSkus)
            {
                const auto& skus = userSkus.second;
                auto ittSku = skus.find(ctxSku);
                if (ittSku != skus.end())
                {
                    auto ittPlan = ittSku->Plans.find(PlanInfo(plan.Id));
                    ASSERT(ittPlan != ittSku->Plans.end());
                    if (ittPlan != ittSku->Plans.end())
                    {
                        if (ittPlan->Enabled)
                            activatedAtLeastOnce = true;
                        else
                            deactivatedAtLeastOnce = true;
                    }

                    if (activatedAtLeastOnce && deactivatedAtLeastOnce)
                        break;
                }
            }

			uint32_t dummyFlags		= 0;
			bool automationValue	= false;
			bool automated			= getAutomationValue(plan.Name, automationValue, dummyFlags);

			if (automated)
				planState = automationValue ? g_ON : g_OFF;
			else
			{
				if (activatedAtLeastOnce && !deactivatedAtLeastOnce)
					planState = g_ON;
				else if (deactivatedAtLeastOnce && !activatedAtLeastOnce)
					planState = g_OFF;
				else if (activatedAtLeastOnce && deactivatedAtLeastOnce)
					planState = g_MIX;
			}

            choices[index] = web::json::value::object({
                { _YTEXT("labelField"), web::json::value::string(plan.Name) },
                { _YTEXT("name"), web::json::value::string(p_Sku.Id + wstring(_YTEXT("_")) + plan.Id) },
                { _YTEXT("state"), web::json::value::string(planState) }
            });
            ++index;
        }
    }
}
