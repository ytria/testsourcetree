#pragma once

#include "HistoryMode.h"

class IHistoryFrame
{
public:
	IHistoryFrame(CFrameWnd* p_That, HistoryMode::Mode p_HistoryMode, IHistoryFrame* p_PreviousHistoryFrame);
	virtual ~IHistoryFrame();

	void Erect();

	bool HasPrevious() const;
	bool HasNext() const;

	IHistoryFrame* GetNext() const;

protected:
	bool GoToPrevious();
	bool GoToNext(bool p_Refresh);

	bool JumpTo(IHistoryFrame* pFrame);

	vector<IHistoryFrame*> GetPreviousFrames() const;
	vector<IHistoryFrame*> GetNextFrames() const;

	virtual void HistoryChanged();
	virtual void ErectSpecific() = 0;
	virtual void ShownViaHistory();
	virtual void HiddenViaHistory();

private:
	void applyFramePlacement(CFrameWnd* p_SrcFrame, CFrameWnd* p_Destframe);
	bool isInSameHistory(IHistoryFrame* pFrame) const;
	bool GoTo(IHistoryFrame* pFrame);

	// Like ShowWindow but bypass OS animations if they're enabled.
	void CallShowWindow(CFrameWnd* p_Frame, int p_CmdShow);

private:
	CFrameWnd* m_That;
	IHistoryFrame* m_PreviousHistoryFrame;
	IHistoryFrame* m_NextHistoryFrame;
	HistoryMode::Mode m_HistoryMode;
};
