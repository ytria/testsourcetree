#include "MailboxPermissionsModificationsReverter.h"

#include "GenericModificationsReverter.h"
#include "GridMailboxPermissions.h"
#include "CacheGridTools.h"

MailboxPermissionsModificationsReverter::MailboxPermissionsModificationsReverter(GridMailboxPermissions& p_Grid)
	:m_Grid(p_Grid),
	m_Confirm(true)
{
}

void MailboxPermissionsModificationsReverter::RevertAll()
{
	bool hasMods = m_Grid.HasModificationsPending(false);
	if (hasMods && (!m_Confirm || (m_Confirm && GenericModificationsReverter::RevertAllConfirm(m_Grid.GetParent()))))
	{
		TemporarilyImplodeNonGroupRowsAndExec(m_Grid, TemporarilyImplodeNonGroupRowsAndExec::ALL_ROWS, [=](const bool) {
			for (auto& mod : m_Grid.m_MailboxPermEditMods)
				mod.second.Revert();
			for (auto& mod : m_Grid.m_MailboxPermAddMods)
				mod.second.Revert();
			for (auto& mod : m_Grid.m_MailboxPermEditMods)
				mod.second.Revert();
			for (auto& mod : m_Grid.m_MailboxPermRemoveMods)
				mod.second.Revert();
			m_Grid.m_MailboxPermEditMods.clear();
			m_Grid.m_MailboxPermAddMods.clear();
			m_Grid.m_MailboxPermRemoveMods.clear();
		})();

		m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
	}
}

void MailboxPermissionsModificationsReverter::RevertSelected()
{
	bool hasMods = m_Grid.HasModificationsPending(true);
	if (hasMods && (!m_Confirm || (m_Confirm && GenericModificationsReverter::RevertSelectedConfirm(m_Grid.GetParent()))))
	{
		TemporarilyImplodeNonGroupRowsAndExec(m_Grid, TemporarilyImplodeNonGroupRowsAndExec::SELECTED_ROWS, [=](const bool) {
			for (auto itt = m_Grid.m_MailboxPermEditMods.begin(); itt != m_Grid.m_MailboxPermEditMods.end();)
				itt = Revert(m_Grid.m_MailboxPermEditMods, itt);
			for (auto itt = m_Grid.m_MailboxPermAddMods.begin(); itt != m_Grid.m_MailboxPermAddMods.end();)
				itt = Revert(m_Grid.m_MailboxPermAddMods, itt);
			for (auto itt = m_Grid.m_MailboxPermRemoveMods.begin(); itt != m_Grid.m_MailboxPermRemoveMods.end();)
				itt = Revert(m_Grid.m_MailboxPermRemoveMods, itt);
	
		})();
		m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);
	}
}

void MailboxPermissionsModificationsReverter::SetConfirm(bool p_Confirm)
{
	m_Confirm = p_Confirm;
}
