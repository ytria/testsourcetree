#include "DlgMessagesModuleOptions.h"

#include "RoleDelegationManager.h"
#include "MsGraphFieldNames.h"

void DlgMessagesModuleOptions::generateJSONScriptData()
{
	initMain(_YTEXT("DlgMessageModuleOptions"));

	getGenerator().addIconTextField(_YTEXT("iconText"), YtriaTranslate::Do(DlgMessageModuleOptions_generateJSONScriptData_44, _YLOC("Limit the number of messages to load by selecting a date/time range. You can request more later.")).c_str(), _YTEXT("fas fa-calendar-alt"));
		
	addCutOffEditor(_T("Get messages received within: "), _T("Received after (excluding)"));

	getGenerator().addIconTextField(_YTEXT("iconText2"), _T("Refine your selection"), _YTEXT("fas fa-search"));
	getGenerator().Add(BoolToggleEditor({ { g_AutoParamSoftDeletedFolders, _T("Soft-Deleted Folder"), EditorFlags::DIRECT_EDIT, m_ModuleOpts.m_RequestSoftDeletedFolders }, { YtriaTranslate::Do(DlgFilterTree_OnInitDialog_4, _YLOC("Include")).c_str(), YtriaTranslate::Do(DlgFilterTree_OnInitDialog_5, _YLOC("Exclude")).c_str() } }));

	addFilterEditor();

	const auto roleId = m_Session->GetRoleDelegationID();
	const auto hasPrivilege = RoleDelegationManager::GetInstance().DelegationHasPrivilege(roleId, RoleDelegationUtil::RBAC_USER_MESSAGES_SEE_MAIL_CONTENT, m_Session);
	const auto canSeeBody = roleId == 0 || (m_Session && hasPrivilege);
	const uint32_t flags = canSeeBody ? EditorFlags::DIRECT_EDIT : EditorFlags::READONLY;

	getGenerator().addIconTextField(_YTEXT("iconText"), _T("Select details to include (exclude all for fastest results)."), _YTEXT("fas fa-clipboard-list-check"));
	getGenerator().Add(BoolToggleEditor({ { g_AutoParamBodyPreview, YtriaTranslate::Do(GridMessages_customizeGrid_24, _YLOC("Mail preview")).c_str(), flags, canSeeBody && m_ModuleOpts.m_BodyPreview }, { YtriaTranslate::Do(DlgFilterTree_OnInitDialog_4, _YLOC("Include")).c_str(), YtriaTranslate::Do(DlgFilterTree_OnInitDialog_5, _YLOC("Exclude")).c_str() }  }));
	getGenerator().Add(BoolToggleEditor({ { g_AutoParamFullBody, YtriaTranslate::Do(GridTeamChannelMessages_customizeGrid_28, _YLOC("Body content")).c_str(), flags, canSeeBody && m_ModuleOpts.m_BodyContent }, { YtriaTranslate::Do(DlgFilterTree_OnInitDialog_4, _YLOC("Include")).c_str(), YtriaTranslate::Do(DlgFilterTree_OnInitDialog_5, _YLOC("Exclude")).c_str() } }));
	getGenerator().Add(BoolToggleEditor({ { g_AutoParamMailHeaders, YtriaTranslate::Do(DlgMessageModuleOptions_generateJSONScriptData_75, _YLOC("Mail headers")).c_str(), EditorFlags::DIRECT_EDIT, m_ModuleOpts.m_MailHeaders }, { YtriaTranslate::Do(DlgFilterTree_OnInitDialog_4, _YLOC("Include")).c_str(), YtriaTranslate::Do(DlgFilterTree_OnInitDialog_5, _YLOC("Exclude")).c_str() } }));

	getGenerator().addApplyButton(YtriaTranslate::Do(CDlg_OnInitDialog_2, _YLOC("Ok")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str());
}

wstring DlgMessagesModuleOptions::getDialogTitle() const
{
	return YtriaTranslate::Do(DlgMessageModuleOptions_getDialogTitle_2, _YLOC("Load Messages - Options")).c_str();
}

std::map<wstring, DlgModuleOptions::FilterFieldProperties> DlgMessagesModuleOptions::getFilterFields() const
{
	return
	{
			{ _YTEXT(O365_MESSAGE_INTERNETMESSAGEID), { YtriaTranslate::Do(GridMessages_customizeGrid_27, _YLOC("Internet Message ID")).c_str(), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_MESSAGE_SUBJECT), { YtriaTranslate::Do(GridMessages_customizeGrid_10, _YLOC("Subject")).c_str(), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT(O365_MESSAGE_IMPORTANCE), { YtriaTranslate::Do(GridMessages_customizeGrid_26, _YLOC("Importance")).c_str(), true, FilterFieldProperties::EnumType({ { _YTEXT("low"), _T("Low (0)") }, { _YTEXT("normal"), _T("Normal (1)") }, { _YTEXT("high"), _T("High (2)") } }), g_FilterOperatorsNoStartWith } }				// (enum)
		,	{ _YTEXT(O365_MESSAGE_INFERENCECLASSIFICATION), { YtriaTranslate::Do(GridMessages_customizeGrid_4, _YLOC("Classification")).c_str(), true, FilterFieldProperties::EnumType({ { _YTEXT("focused"), _T("Focused (0)") }, { _YTEXT("other"), _T("Other (1)") } }), g_FilterOperatorsNoStartWith } }	// (enum)
		,	{ _YTEXT("sender/emailAddress/name"), { YtriaTranslate::Do(GridMessages_customizeGrid_8, _YLOC("Sender - Name")).c_str(), true, FilterFieldProperties::StringType(), {} } }
		,	{ _YTEXT("sender/emailAddress/address"), { YtriaTranslate::Do(GridMessages_customizeGrid_9, _YLOC("Sender - Email")).c_str(), true, FilterFieldProperties::StringType(), {} } }
		//,	{ _YTEXT("flag/flagStatus"), true }			// (enum, BUT ne works as eq, eq doesn't)

		// FIXME: Our DateTime editor is not good, better with Date (with calendar) but that prevent from using EQ or NE...
		//,	{ _YTEXT(O365_MESSAGE_CREATEDDATETIME), { YtriaTranslate::Do(GridMessages_customizeGrid_13, _YLOC("Created On")).c_str(), false, FilterFieldProperties::DateTimeType(), g_FilterOperatorsNoEQ } }		// (except eq) // NE is authorized but as we use DateType and data contains time, result is incorrect. FIXME when DateTimeType has a calendar.
		//,	{ _YTEXT(O365_MESSAGE_LASTMODIFIEDDATETIME), { YtriaTranslate::Do(GridMessages_customizeGrid_14, _YLOC("Last Modified On")).c_str(), false, FilterFieldProperties::DateTimeType(), g_FilterOperatorsNoEQ } }	// (except eq)
		//,	{ _YTEXT(O365_MESSAGE_RECEIVEDDATETIME), { YtriaTranslate::Do(GridMessages_customizeGrid_12, _YLOC("Received On")).c_str(), false, FilterFieldProperties::DateTimeType(), g_FilterOperatorsNoEQ } }		// (except eq)
		//,	{ _YTEXT(O365_MESSAGE_SENTDATETIME), { YtriaTranslate::Do(GridMessages_customizeGrid_11, _YLOC("Sent On")).c_str(), false, FilterFieldProperties::DateTimeType(), g_FilterOperatorsNoEQ } }			// (except eq)
		,	{ _YTEXT(O365_MESSAGE_CREATEDDATETIME), { YtriaTranslate::Do(GridMessages_customizeGrid_13, _YLOC("Created On")).c_str(), false, FilterFieldProperties::DateType(), g_FilterOperatorsNoEQNE } }		// (except eq)
		,	{ _YTEXT(O365_MESSAGE_LASTMODIFIEDDATETIME), { YtriaTranslate::Do(GridMessages_customizeGrid_14, _YLOC("Last Modified On")).c_str(), false, FilterFieldProperties::DateType(), g_FilterOperatorsNoEQNE } }	// (except eq)
		,	{ _YTEXT(O365_MESSAGE_RECEIVEDDATETIME), { YtriaTranslate::Do(GridMessages_customizeGrid_12, _YLOC("Received On")).c_str(), false, FilterFieldProperties::DateType(), g_FilterOperatorsNoEQNE } }		// (except eq)
		,	{ _YTEXT(O365_MESSAGE_SENTDATETIME), { YtriaTranslate::Do(GridMessages_customizeGrid_11, _YLOC("Sent On")).c_str(), false, FilterFieldProperties::DateType(), g_FilterOperatorsNoEQNE } }			// (except eq)

		,	{ _YTEXT(O365_MESSAGE_HASATTACHMENTS), { YtriaTranslate::Do(GridMessages_customizeGrid_37, _YLOC("Has Attachment")).c_str(), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } }				// bool
		,	{ _YTEXT(O365_MESSAGE_ISDELIVERYRECEIPTREQUESTED), { YtriaTranslate::Do(GridMessages_customizeGrid_30, _YLOC("Delivery Receipt Requested")).c_str(), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } }	// bool
		,	{ _YTEXT(O365_MESSAGE_ISREADRECEIPTREQUESTED), { YtriaTranslate::Do(GridMessages_customizeGrid_29, _YLOC("Read Receipt Requested")).c_str(), false,FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } }		// bool
		,	{ _YTEXT(O365_MESSAGE_ISREAD), { YtriaTranslate::Do(GridMessages_customizeGrid_28, _YLOC("Read")).c_str(), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } }						// bool
		,	{ _YTEXT(O365_MESSAGE_ISDRAFT), { YtriaTranslate::Do(GridMessages_customizeGrid_5, _YLOC("Draft")).c_str(), false, FilterFieldProperties::BoolType(), g_FilterOperatorsNoStartWith } }					// bool
	};
}