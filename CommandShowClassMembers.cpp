#include "CommandShowClassMembers.h"
#include "GridClassMembers.h"

#include "FrameClassMembers.h"
#include "GridClassMembers.h"
#include "O365AdminUtil.h"
#include "ClassMembersListRequester.h"

CommandShowClassMembers::CommandShowClassMembers(const vector<EducationSchool>& p_Schools, FrameClassMembers& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, bool p_IsUpdate)
	:IActionCommand(_YTEXT("")),
	m_Frame(p_Frame),
	m_AutoSetup(p_AutoSetup),
	m_LicenseCtx(p_LicenseCtx),
	m_AutoAction(p_AutoAction),
	m_IsUpdate(p_IsUpdate),
	m_Schools(p_Schools)
{
}

void CommandShowClassMembers::ExecuteImpl() const
{
	GridClassMembers* grid = dynamic_cast<GridClassMembers*>(&m_Frame.GetGrid());

	auto taskData = Util::AddFrameTask(_T("Show schools"), &m_Frame, m_AutoSetup, m_LicenseCtx, false);

	YSafeCreateTaskMutable([schools = m_Schools, isUpdate = m_IsUpdate, taskData, hwnd = m_Frame.GetSafeHwnd(), moduleCriteria = m_Frame.GetModuleCriteria(), autoAction = m_AutoAction, grid, bom = m_Frame.GetBusinessObjectManager()]() mutable
	{
		size_t count = 0;
		for (auto& school : schools)
			count += school.m_Classes.size();

		auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("class members"), count);

		for (auto& school : schools)
		{
			for (auto& curClass : school.m_Classes)
			{
				logger->IncrementObjCount();
				logger->SetContextualInfo(GetDisplayNameOrId(curClass));

				auto membersRequester = std::make_shared<ClassMembersListRequester>(curClass.GetID(), logger);

				vector<EducationUser> users = safeTaskCall(membersRequester->Send(bom->GetSapio365Session(), taskData).Then([membersRequester]() {
					return membersRequester->GetData();
				}), bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ListErrorHandler<EducationUser>, taskData).get();

				curClass.m_Users = users;
			}
		}

		YCallbackMessage::DoPost([=]()
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameClassMembers*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ModuleBase::ShouldBuildView(hwnd, taskData.IsCanceled(), taskData, false))
			{
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting members of %s classes for %s schools...", Str::getStringFromNumber(count).c_str(), Str::getStringFromNumber(schools.size()).c_str()));
				frame->GetGrid().ClearLog(taskData.GetId());

				{
					CWaitCursor _;
					if (frame->HasLastUpdateOperations())
					{
						frame->BuildView(schools, frame->GetLastUpdateOperations(), false);
						frame->ForgetLastUpdateOperations();
					}
					else
					{
						frame->BuildView(schools, {}, true);
					}
				}

				if (!isUpdate)
				{
					frame->UpdateContext(taskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
				frame->TaskFinished(taskData, nullptr, hwnd);
		});
	});
}
