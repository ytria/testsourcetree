#pragma once

#include "BusinessObject.h"

class EducationStudent : public BusinessObject
{
public:
	boost::YOpt<YTimeDate> m_BirthDate;
	boost::YOpt<PooledString> m_ExternalId;
	boost::YOpt<PooledString> m_Gender;
	boost::YOpt<PooledString> m_Grade;
	boost::YOpt<PooledString> m_GraduationYear;
	boost::YOpt<PooledString> m_StudentNumber;
	
	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
};

