#pragma once

#include "Command.h"
#include "ModuleBase.h"
#include "Sapio365Session.h"

class CommandDispatcher
{
public:
	static CommandDispatcher& GetInstance();
	void Execute(const Command& p_Command);

	using ModulesPerTarget = map<Command::ModuleTarget, std::unique_ptr<ModuleBase>>;
	using ModulesPerSession = map<SessionIdentifier, ModulesPerTarget>;

	const ModulesPerSession& GetAllModules() const;
	// Used by WindowsGrid only.
	static wstring getModuleName(Command::ModuleTarget target);

	vector<GridFrameBase*> CommandDispatcher::GetAllGridFrames();

private:
	CommandDispatcher();
	ModuleBase* getModule(const SessionIdentifier& p_SessionIdentifier, Command::ModuleTarget p_Target);
	void ExecuteImpl(const Command& p_Command);

	ModulesPerSession m_Modules;// modules per session technical name
	std::unique_ptr<ModuleBase> m_DefaultModule;
};
