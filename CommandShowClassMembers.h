#pragma once

#include "IActionCommand.h"
#include "EducationSchool.h"
#include "RefreshSpecificData.h"

struct AutomationSetup;
class AutomationAction;
class BusinessObjectManager;
class FrameClassMembers;
class LicenseContext;

class CommandShowClassMembers : public IActionCommand
{
public:
	CommandShowClassMembers(const vector<EducationSchool>& p_Schools, FrameClassMembers& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, bool p_IsUpdate);

protected:
	void ExecuteImpl() const override;

	AutomationSetup& m_AutoSetup;
	FrameClassMembers& m_Frame;
	const LicenseContext& m_LicenseCtx;
	AutomationAction* m_AutoAction;
	bool m_IsUpdate;
	const vector<EducationSchool>& m_Schools;
};

