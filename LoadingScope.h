#pragma once

enum LoadingScope
{	// Order is important (used in GetLoadingScopeStr)
	All = 0,
	LastHour,
	Last24h,
	LastWeek,
	LastMonth,
	Other
};
