#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Root.h"

class RootDeserializer : public JsonObjectDeserializer, public Encapsulate<Root>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

