#include "MailFolderHierarchyRequester.h"
#include "ChildrenMailFoldersRequester.h"
#include "IRequestLogger.h"
#include "O365AdminUtil.h"
#include "MailFoldersTopLevelRequester.h"
#include "PaginatorUtil.h"
#include "safeTaskCall.h"
#include "MessageInMailfolderPropertySet.h"
#include "MailFolderRequester.h"

MailFolderHierarchyRequester::MailFolderHierarchyRequester(const wstring& p_UserId, const wstring& p_FolderName, const std::shared_ptr<IRequestLogger>& p_TopMailFolderLogger, const std::shared_ptr<IPageRequestLogger>& p_TopMailFolderMessagesLogger, const std::shared_ptr<IPageRequestLogger>& p_SubMailFoldersLogger)
	: m_UserId(p_UserId)
	, m_FolderName(p_FolderName)
	, m_TopMailFolderLogger(p_TopMailFolderLogger)
	, m_TopMailFolderMessagesLogger(p_TopMailFolderMessagesLogger)
	, m_SubMailFoldersLogger(p_SubMailFoldersLogger)
{
}

void MailFolderHierarchyRequester::SetMessagesCutOffDateTime(const wstring& p_CutOffDateTime)
{
	m_CutOffDateTime = p_CutOffDateTime;
}

void MailFolderHierarchyRequester::SetMessagesCustomFilter(const ODataFilter& p_CustomFilter)
{
	m_CustomFilter = p_CustomFilter;
}

TaskWrapper<void> MailFolderHierarchyRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	auto topLevelFolderRequester = std::make_shared<MailFolderRequester>(m_UserId, m_FolderName, m_TopMailFolderLogger);

	m_MailFolder = std::make_shared<BusinessMailFolder>();

	return safeTaskCall(topLevelFolderRequester->Send(p_Session, p_TaskData).ThenByTask(
		[cutOff = m_CutOffDateTime
		, customFilter = m_CustomFilter
		, userId = m_UserId
		, p_Session
		, topLevelFolderRequester
		, mailFolder = m_MailFolder
		, p_TaskData
		, subLogger = m_SubMailFoldersLogger
		, topMsgLogger = m_TopMailFolderMessagesLogger
		](pplx::task<void> p_PreviousTask) {
		
		try
		{
			p_PreviousTask.get();
		}
		catch (const RestException& e)
		{
			BusinessMailFolder errObj;
			errObj.SetError(HttpError(e));
			*mailFolder = errObj;
		}
		catch (const std::exception& e)
		{
			BusinessMailFolder errObj;
			errObj.SetError(SapioError(e));
			*mailFolder = errObj;
		}

		if (p_TaskData.IsCanceled())
			return;

		try
		{
			*mailFolder = topLevelFolderRequester->GetData();

			// Get all subfolders of this top-level mailfolder and their respective messages
			subLogger->SetLogMessage(_T("mail subfolders"));
			subLogger->SetContextualInfo(mailFolder->m_DisplayName ? *mailFolder->m_DisplayName : Str::MakeMidEllipsis(mailFolder->GetID()));
			ChildrenMailFoldersRequester childrenRequester(userId, mailFolder->GetID(), true, subLogger);
			if (!cutOff.empty())
				childrenRequester.SetMessagesCutOffDateTime(cutOff);
			childrenRequester.SetMessagesCustomFilter(customFilter);

			childrenRequester.Send(p_Session, p_TaskData).GetTask().wait();

			vector<BusinessMailFolder> childrenBusinessMailFolders;
			for (const auto& mailFolder : childrenRequester.GetData())
				childrenBusinessMailFolders.emplace_back(mailFolder);
			mailFolder->m_ChildrenMailFolders = childrenBusinessMailFolders;

			// Get messages in this immediate top-level folder
			topMsgLogger->SetContextualInfo(mailFolder->m_DisplayName ? *mailFolder->m_DisplayName : Str::MakeMidEllipsis(mailFolder->GetID()));
			MessageListRequester thisFolderReq(MessageInMailfolderPropertySet(), userId, wstring(mailFolder->m_Id), topMsgLogger);
			if (!cutOff.empty())
				thisFolderReq.SetCutOffDateTime(cutOff);
			thisFolderReq.Send(p_Session, p_TaskData).GetTask().wait();

			mailFolder->m_Messages = thisFolderReq.GetData();
		}
		catch (RestException& e)
		{
			BusinessMailFolder errObj;
			errObj.SetError(HttpError(e));
			*mailFolder = errObj;
		}
		catch (const pplx::task_canceled&)
		{
		}
		catch (const std::exception& e)
		{
			BusinessMailFolder errObj;
			errObj.SetError(SapioError(e));
			*mailFolder = errObj;
		}
		
	}), p_Session->GetMSGraphSession(Sapio365Session::APP_SESSION), p_TaskData);
}

BusinessMailFolder& MailFolderHierarchyRequester::GetData()
{
	return *m_MailFolder;
}
