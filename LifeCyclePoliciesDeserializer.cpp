#include "LifeCyclePoliciesDeserializer.h"

#include "JsonSerializeUtil.h"

void LifeCyclePoliciesDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("groupLifetimeInDays"), m_Data.m_NumberOfDays, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("managedGroupTypes"), m_Data.m_ManagedGroupTypes, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("alternateNotificationEmails"), m_Data.m_AlternativeNotificationEmails, p_Object);
}
