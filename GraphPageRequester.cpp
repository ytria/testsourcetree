#include "GraphPageRequester.h"
#include "ValueListDeserializer.h"
#include "MSGraphSession.h"
#include "PaginatedRequestResults.h"
#include "YtriaTaskData.h"
#include "YCodeJockMessageBox.h"

GraphPageRequester::GraphPageRequester(const shared_ptr<IValueListDeserializer>& p_Deserializer, const shared_ptr<PaginatedRequestResults>& p_Result, const shared_ptr<IHttpRequestLogger>& p_HttpLogger)
	: m_Deserializer(p_Deserializer),
	m_Result(p_Result)
{
	SetHttpRequestLogger(p_HttpLogger);
	if (nullptr == m_Deserializer)
	{
		ASSERT(false);
		throw std::invalid_argument("GraphPaginatorRequester:: deserializer cannot be null");
	}
	if (nullptr == m_HttpLogger)
	{
		ASSERT(false);
		throw std::invalid_argument("GraphPaginatorRequester:: HttpLogger cannot be null");
	}
}

web::json::value GraphPageRequester::Request(const web::uri& p_Uri, const shared_ptr<MSGraphSession>& p_Session, YtriaTaskData p_TaskData)
{
	json::value jsonReceived;

	wstring overrideBaseUri;
	if (m_UseBeta)
		overrideBaseUri = _YTEXT("beta");

	RestResultInfo result = p_Session->Read(p_Uri, m_HttpLogger, p_TaskData, WebPayload(), overrideBaseUri, m_Headers).GetTask().get();

	if (!p_TaskData.GetLastRequestOn())
		p_TaskData.SetLastRequestOn(YTimeDate::GetCurrentTimeDate());

	if (result.Response.status_code() == web::http::status_codes::OK)
	{
		boost::YOpt<wstring> body = result.GetBodyAsString();
		if (body != boost::none)
		{
			jsonReceived = json::value::parse(*body);
			m_Deserializer->SetDate(p_TaskData.GetLastRequestOn());
			m_Deserializer->Deserialize(jsonReceived);
			m_IsCutOff = m_Deserializer->GetIsCutOff();
			m_DeserializedCount = m_Deserializer->GetRunningDeserializedCount();
		}
	}

	if (nullptr != m_Result)
		m_Result->AddResult(result);

	return jsonReceived;
}

void GraphPageRequester::SetHeaders(const web::http::http_headers& p_Headers)
{
	m_Headers = p_Headers;
}

