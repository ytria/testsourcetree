#include "UserListRequester.h"

#include "BusinessGroup.h"
#include "BusinessUser.h"
#include "IPropertySetBuilder.h"
#include "MSGraphCommonData.h"
#include "MSGraphUtil.h"
#include "MSGraphSession.h"
#include "PaginatedRequestResults.h"
#include "PaginatorUtil.h"
#include "SQL + Cloud/Role Delegation/RoleDelegationManager.h"
#include "Sapio365Session.h"
#include "UserDeserializer.h"
#include "UserDeserializerFactory.h"
#include "ValueListDeserializer.h"
#include "RbacRequiredPropertySet.h"
#include "MsGraphHttpRequestLogger.h"
#include "MsGraphDeltaPaginator.h"

UserListRequester::UserListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger, std::unique_ptr<UserListSubRequester> p_SpecificRequester)
    : m_Logger(p_Logger)
	, m_SubRequester(std::move(p_SpecificRequester))

{
}

TaskWrapper<void> UserListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	ASSERT(nullptr != m_SubRequester);
	if (nullptr != m_SubRequester)
	{
		auto factory = std::make_unique<UserDeserializerFactory>(p_Session);
		m_Deserializer = std::make_shared<ValueListDeserializer<BusinessUser, UserDeserializer>>(std::move(factory));

		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());

		m_SubRequester->SetParentRequester(this);
		m_SubRequester->SetLogger(httpLogger);

		return m_SubRequester->Send(p_Session, p_TaskData);
	}

	return TaskWrapper<void>();
}

vector<BusinessUser>& UserListRequester::GetData()
{
    return m_Deserializer->GetData();
}

void UserListRequester::SetCustomFilter(const ODataFilter& p_CustomFilter)
{
	m_CustomFilter = p_CustomFilter;
}

TaskWrapper<void> UserListRequester::RegularRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	web::uri_builder uri(_YTEXT("users"));
	uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(m_Properties));

	m_Requester->m_CustomFilter.ApplyTo(uri);
	
	return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Requester->m_Deserializer, m_Requester->m_Logger, m_Logger, p_TaskData.GetOriginator())
		.Paginate(p_Session->GetMSGraphSession(m_Logger->GetSessionType()), p_TaskData);
}

UserListRequester::InitialDeltaRequester::InitialDeltaRequester()
	: m_NextDeltaLink(std::make_shared<std::wstring>())
{
}

TaskWrapper<void> UserListRequester::InitialDeltaRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	web::uri_builder uri(_YTEXT("users/delta"));
	uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(m_Properties));

	// Be careful, only works on 'id' !!
	m_Requester->m_CustomFilter.ApplyTo(uri);

	auto paginator = MsGraphDeltaPaginator(uri.to_uri(),
		Util::CreateDefaultGraphPageRequester(m_Requester->m_Deserializer, m_Logger, p_TaskData.GetOriginator()),
		m_Requester->m_Logger);
	paginator.SetDeltaLinkPtr(m_NextDeltaLink);

	return paginator.Paginate(p_Session->GetMSGraphSession(m_Logger->GetSessionType()), p_TaskData);
}

wstring UserListRequester::InitialDeltaRequester::GetNextDeltalink() const
{
	return m_NextDeltaLink ? *m_NextDeltaLink : wstring();
}

UserListRequester::DeltaLinkRequester::DeltaLinkRequester(const wstring& p_DeltaLink)
	: m_NextDeltaLink(std::make_shared<std::wstring>(p_DeltaLink))
	, m_DeltaLink(p_DeltaLink)
{
}

TaskWrapper<void> UserListRequester::DeltaLinkRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	web::uri_builder uri(m_DeltaLink);

	auto paginator = MsGraphDeltaPaginator(uri.to_uri(),
		Util::CreateDefaultGraphPageRequester(m_Requester->m_Deserializer, m_Logger, p_TaskData.GetOriginator()),
		m_Requester->m_Logger);
	paginator.SetDeltaLinkPtr(m_NextDeltaLink);

	return paginator.Paginate(p_Session->GetMSGraphSession(m_Logger->GetSessionType()), p_TaskData);
}

wstring UserListRequester::DeltaLinkRequester::GetNextDeltalink() const
{
	return m_NextDeltaLink ? *m_NextDeltaLink : wstring();
}

UserListRequester::UserListSubRequester::UserListSubRequester()
	:m_Requester(nullptr)
{
}

void UserListRequester::UserListSubRequester::SetParentRequester(UserListRequester* p_ParentRequester)
{
	m_Requester = p_ParentRequester;
}

void UserListRequester::UserListSubRequester::SetProperties(const vector<rttr::property>& p_Properties)
{
	m_Properties = p_Properties;
}

void UserListRequester::UserListSubRequester::SetLogger(const std::shared_ptr<MsGraphHttpRequestLogger>& p_Logger)
{
	m_Logger = p_Logger;
}
