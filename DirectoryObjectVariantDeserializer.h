#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessDirectoryObjectVariant.h"

class Sapio365Session;

class DirectoryObjectVariantDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessDirectoryObjectVariant>
{
public:
	DirectoryObjectVariantDeserializer(std::shared_ptr<Sapio365Session> p_Session);

#ifdef _DEBUG
	// This ctor is for debug purpose only, as Authors Rejected/Accepted hierarchies requests don't accept $select query,
	// we want to prevent having an assert in nested UserDeserializer/GroupDeserializer
	// Eventually removed when Graph API is fixed.
	DirectoryObjectVariantDeserializer(std::shared_ptr<Sapio365Session> p_Session, bool p_NoSelectUsed);
#endif

protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;

private:
	std::shared_ptr<Sapio365Session> m_Session;
#ifdef _DEBUG
	bool m_NoSelectUsed = false; // Eventually removed when Graph API is fixed.
#endif
};
