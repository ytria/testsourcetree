#include "UserDeserializer.h"

#include "AssignedLicenseDeserializer.h"
#include "AssignedPlanDeserializer.h"
#include "BusinessUserConfiguration.h"
#include "JsonSerializeUtil.h"
#include "LicenseAssignmentStateDeserializer.h"
#include "ListDeserializer.h"
#include "MailboxSettingsDeserializer.h"
#include "MsGraphFieldNames.h"
#include "ObjectIdentityDeserializer.h"
#include "OnPremisesExtensionAttributesDeserializer.h"
#include "OnPremisesProvisioningErrorDeserializer.h"
#include "PasswordProfileDeserializer.h"
#include "ProvisionedPlanDeserializer.h"

UserDeserializer::UserDeserializer()
{
}

UserDeserializer::UserDeserializer(std::shared_ptr<const Sapio365Session> p_Session)
	: RoleDelegationTagger(p_Session, &m_Data)
{
}

#ifdef _DEBUG
UserDeserializer::UserDeserializer(std::shared_ptr<const Sapio365Session> p_Session, bool p_NoSelectUsed)
	: UserDeserializer(p_Session)
{
	m_NoSelectUsed = p_NoSelectUsed;
}
#endif

void UserDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	// FIXME: This assert is obsolete with delta which sometimes returns nothing instead of "empty" value ... Fix it.
	//ASSERT(m_NoSelectUsed || containsAllRequiredProperties<BusinessUser>(p_Object));

	m_Data.SetLastRequestTime(GetDate());

	if (p_Object.size() == 2
		&& p_Object.find(_YUID(O365_ID)) != p_Object.end()
		&& p_Object.find(_YUID(O365_USER_CREATEDDATETIME)) != p_Object.end())
	{
		// 2020/11/16 - This is a specific buggy case we noticed.
		// Cause of Bug #201115.EH.00D1EA (R): [-CRITICAL-] When opening my users, my data is missing some users. A Server Sync doesn't fix my issue.
		// For any reason, a delta sync request sometimes returns partial users that only contain an ID and a CreationDate (whereas doc says we should always have full users).
		// After a few tests, as crazy as it seems, it looks like logging in to OneDrive triggers that empty user ... To be continued....
		// With this hacky flag, we can ignore the user later and workaround the issue.
		m_Data.SetFlags(m_Data.GetFlags() | BusinessObject::SHOULDBEIGNORED);
		if (p_Object.at(_YUID(O365_ID)).is_string())
			LoggerService::Debug(_YFORMATERROR(L"Ignored incomplete User (%s).", p_Object.at(_YUID(O365_ID)).as_string().c_str()));
		else
			LoggerService::Debug(_YERROR("Ignored incomplete User."));
		return;
	}

	JsonSerializeUtil::DeserializeId(_YUID(O365_ID), m_Data.m_Id, p_Object, true);

	if (p_Object.find(L"@removed") != p_Object.end())
	{
		m_Data.SetFlags(m_Data.GetFlags() | BusinessObject::REMOVEDBYDELTASYNC);
		return;
	}

    {
        PasswordProfileDeserializer ppd;
		ppd.SetDate(GetDate());

        if (JsonSerializeUtil::DeserializeAny(ppd, _YUID(O365_USER_PASSWORDPROFILE), p_Object))
            m_Data.SetPasswordProfile(ppd.GetData());

        tagObject(_YUID(O365_USER_PASSWORDPROFILE_FORCECHANGE), ppd.GetData().ForceChangePasswordNextSignIn);
		tagObject(_YUID(O365_USER_PASSWORDPROFILE_FORCECHANGEWITHMFA), ppd.GetData().ForceChangePasswordNextSignInWithMfa);
    }
	
	deserializeStringAndTagObject(_YUID(O365_USER_ABOUTME), m_Data.m_AboutMe, p_Object);
	deserializeBoolAndTagObject(_YUID(O365_USER_ACCOUNTENABLED), m_Data.m_AccountEnabled, p_Object);
        
    {
		const JsonSerializeUtil::String propName = _YUID(O365_USER_ASSIGNEDLICENSES);
        ListDeserializer<BusinessAssignedLicense, AssignedLicenseDeserializer> assLicDeserializer;
		assLicDeserializer.SetDate(GetDate());
        if (JsonSerializeUtil::DeserializeAny(assLicDeserializer, propName, p_Object))
            m_Data.m_AssignedLicenses = std::move(assLicDeserializer.GetData());

        if (m_Data.m_AssignedLicenses != boost::none)
        {
            auto subscribedSkus = GetSession().GetGraphCache().GetCachedSubscribedSkus();
            for (const auto& assignedLicense : *m_Data.m_AssignedLicenses)
            {
                auto itt = subscribedSkus.find(assignedLicense.GetID());
                if (itt != subscribedSkus.end())
                    tagObject(propName, itt->second.m_SkuPartNumber);
            }
        }

		if (m_Data.m_AssignedLicenses == boost::none ||
			m_Data.m_AssignedLicenses != boost::none && m_Data.m_AssignedLicenses->empty())
			tagObject(propName, BusinessUserConfiguration::GetInstance().GetValueStringLicenses_Unlicensed());
    }
        
    {
        ListDeserializer<AssignedPlan, AssignedPlanDeserializer> assPlanDeserializer;
		assPlanDeserializer.SetDate(GetDate());
        if (JsonSerializeUtil::DeserializeAny(assPlanDeserializer, _YUID(O365_USER_ASSIGNEDPLANS), p_Object))
            m_Data.m_AssignedPlans = std::move(assPlanDeserializer.GetData());
    }
        
	deserializeDateOnlyAndTagObject(_YUID(O365_USER_BIRTHDAY), m_Data.m_Birthday, p_Object);

    {
        ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_USER_BUSINESSPHONES), p_Object))
            m_Data.m_BusinessPhones = std::move(lsd.GetData());
    }
       
	deserializeStringAndTagObject(_YUID(O365_USER_CITY), m_Data.m_City, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_COMPANYNAME), m_Data.m_CompanyName, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_COUNTRY), m_Data.m_Country, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_DEPARTMENT), m_Data.m_Department, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_DISPLAYNAME), m_Data.m_DisplayName, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_GIVENNAME), m_Data.m_GivenName, p_Object);
	deserializeDateOnlyAndTagObject(_YUID(O365_USER_HIREDATE), m_Data.m_HireDate, p_Object);

    {
        ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_USER_IMADDRESSES), p_Object))
            m_Data.m_ImAddresses = std::move(lsd.GetData());
    }
        
    {
        ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_USER_INTERESTS), p_Object))
            m_Data.m_Interests = std::move(lsd.GetData());
    }

	deserializeStringAndTagObject(_YUID(O365_USER_JOBTITLE), m_Data.m_JobTitle, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_MAIL), m_Data.m_Mail, p_Object);

    {
        MailboxSettingsDeserializer msd;
		msd.SetDate(GetDate());
        if (JsonSerializeUtil::DeserializeAny(msd, _YUID(O365_USER_MAILBOXSETTINGS), p_Object))
            m_Data.m_MailboxSettings = std::move(msd.GetData());
    }
        
	deserializeStringAndTagObject(_YUID(O365_USER_MAILNICKNAME), m_Data.m_MailNickname, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_MOBILEPHONE), m_Data.m_MobilePhone, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_MYSITE), m_Data.m_MySite, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_OFFICELOCATION), m_Data.m_OfficeLocation, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_ONPREMISESIMMUTABLEID), m_Data.m_OnPremisesImmutableId, p_Object);
	deserializeTimeDateAndTagObject(_YUID(O365_USER_ONPREMISESLASTSYNCDATETIME), m_Data.m_OnPremisesLastSyncDateTime, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_ONPREMISESSECURITYIDENTIFIER), m_Data.m_OnPremisesSecurityIdentifier, p_Object);
	deserializeBoolAndTagObject(_YUID(O365_USER_ONPREMISESSYNCENABLED), m_Data.m_OnPremisesSyncEnabled, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_PASSWORDPOLICIES), m_Data.m_PasswordPolicies, p_Object);
        
    {
        ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_USER_PASTPROJECTS), p_Object))
            m_Data.m_PastProjects = std::move(lsd.GetData());
    }

	deserializeStringAndTagObject(_YUID(O365_USER_POSTALCODE), m_Data.m_PostalCode, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_PREFERREDLANGUAGE), m_Data.m_PreferredLanguage, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_PREFERREDNAME), m_Data.m_PreferredName, p_Object);

    {
        ListDeserializer<ProvisionedPlan, ProvisionedPlanDeserializer> provPlanDeserializer;
		provPlanDeserializer.SetDate(GetDate());
        if (JsonSerializeUtil::DeserializeAny(provPlanDeserializer, _YUID(O365_USER_PROVISIONEDPLANS), p_Object))
            m_Data.m_ProvisionedPlans = std::move(provPlanDeserializer.GetData());
    }
        
    {
        ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_USER_PROXYADDRESSES), p_Object))
            m_Data.m_ProxyAddresses = std::move(lsd.GetData());
    }
    
    {
        ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_USER_RESPONSIBILITIES), p_Object))
            m_Data.m_Responsibilities = std::move(lsd.GetData());
    }
    
    {
        ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_USER_SCHOOLS), p_Object))
            m_Data.m_Schools = std::move(lsd.GetData());
    }

    {
        ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_USER_SKILLS), p_Object))
            m_Data.m_Skills = std::move(lsd.GetData());
    }

	deserializeStringAndTagObject(_YUID(O365_USER_STATE), m_Data.m_State, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_STREETADDRESS), m_Data.m_StreetAddress, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_SURNAME), m_Data.m_Surname, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_USAGELOCATION), m_Data.m_UsageLocation, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_USERPRINCIPALNAME), m_Data.m_UserPrincipalName, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_USERTYPE), m_Data.m_UserType, p_Object);
	if (!m_Data.m_Id.IsEmpty() && (!m_Data.m_UserType || m_Data.m_UserType->IsEmpty()))
		 LoggerService::Debug(_YFORMATERROR(L"Missing User Type (%s).", m_Data.m_Id.c_str()));
	deserializeTimeDateAndTagObject(_YUID(O365_USER_DELETEDDATETIME), m_Data.m_DeletedDateTime, p_Object);

	deserializeStringAndTagObject(_YUID(O365_USER_AGEGROUP), m_Data.m_AgeGroup, p_Object);
	deserializeTimeDateAndTagObject(_YUID(O365_USER_CREATEDDATETIME), m_Data.m_CreatedDateTime, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_CONSENTPROVIDEDFORMINOR), m_Data.m_ConsentProvidedForMinor, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_LEGALAGEGROUPCLASSIFICATION), m_Data.m_LegalAgeGroupClassification, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_ONPREMISESDOMAINNAME), m_Data.m_OnPremisesDomainName, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_ONPREMISESSAMACCOUNTNAME), m_Data.m_OnPremisesSamAccountName, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_ONPREMISESUSERPRINCIPALNAME), m_Data.m_OnPremisesUserPrincipalName, p_Object);
	deserializeTimeDateAndTagObject(_YUID(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME), m_Data.m_SignInSessionsValidFromDateTime, p_Object);

	{
		ListDeserializer<OnPremisesProvisioningError, OnPremisesProvisioningErrorDeserializer> oppeld;
		oppeld.SetDate(GetDate());
        if (JsonSerializeUtil::DeserializeAny(oppeld, _YUID(O365_USER_ONPREMISESPROVISIONINGERRORS), p_Object))
			m_Data.m_OnPremisesProvisioningErrors = std::move(oppeld.GetData());

		const JsonSerializeUtil::String errCat		= _YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY);
		const JsonSerializeUtil::String errOccDT	= _YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME);
		const JsonSerializeUtil::String errProp		= _YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR);
		const JsonSerializeUtil::String errVal		= _YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE);
        if (m_Data.m_OnPremisesProvisioningErrors != boost::none)
        {
            for (auto& error : *m_Data.m_OnPremisesProvisioningErrors)
            {
                tagObject(errCat,	error.m_Category);
                tagObject(errOccDT,	error.m_OccurredDateTime);
                tagObject(errProp,	error.m_PropertyCausingError);
                tagObject(errVal,	error.m_Value);
            }
        }

        if (m_Data.m_OnPremisesProvisioningErrors == boost::none ||
            m_Data.m_OnPremisesProvisioningErrors != boost::none && m_Data.m_OnPremisesProvisioningErrors->empty())
        {
			markPropertyAsChecked(errCat);
            markPropertyAsChecked(errOccDT);
            markPropertyAsChecked(errProp);
            markPropertyAsChecked(errVal);
        }
	}

	{
		ListStringDeserializer lsd;
		lsd.SetDate(GetDate());
        if (deserializeVectorStringAndTagObject(lsd, lsd.GetData(), _YUID(O365_USER_OTHERMAILS), p_Object))
			m_Data.m_OtherMails = std::move(lsd.GetData());
	}

	deserializeStringAndTagObject(_YUID(O365_USER_PREFERREDDATALOCATION), m_Data.m_PreferredDataLocation, p_Object);

	deserializeBoolAndTagObject(_YUID(O365_USER_SHOWINADDRESSLIST),				m_Data.m_ShowInAddressList, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_EXTERNALUSERSTATUS),			m_Data.m_ExternalUserStatus, p_Object);
	deserializeTimeDateAndTagObject(_YUID(O365_USER_EXTERNALUSERSTATUSCHANGEDON),	m_Data.m_ExternalUserStatusChangedOn, p_Object);
	
	{
		OnPremisesExtensionAttributesDeserializer opead;
		opead.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(opead, _YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES), p_Object))
		{
			m_Data.m_OnPremisesExtensionAttributes = std::move(opead.GetData());

			const JsonSerializeUtil::String uids[15]
			{
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute1"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute2"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute3"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute4"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute5"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute6"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute7"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute8"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute9"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute10"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute11"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute12"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute13"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute14"),
				_YUID(O365_USER_ONPREMISESEXTENSIONATTRIBUTES) _YTEXT(".extensionAttribute15"),
			};

			if (m_Data.m_OnPremisesExtensionAttributes)
			{
				for (size_t i = 0; i < 15; ++i)
					tagObject(uids[i], m_Data.GetValue(uids[i]));
			}
		}
	}

	deserializeStringAndTagObject(_YUID(O365_USER_EMPLOYEEID), m_Data.m_EmployeeID, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_FAXNUMBER), m_Data.m_FaxNumber, p_Object);
	deserializeBoolAndTagObject(_YUID(O365_USER_ISRESOURCEACCOUNT), m_Data.m_IsResourceAccount, p_Object);
	deserializeStringAndTagObject(_YUID(O365_USER_ONPREMISESDISTINGUISHEDNAME), m_Data.m_OnPremisesDistinguishedName, p_Object);

	deserializeStringAndTagObject(_YUID(O365_USER_CREATIONTYPE), m_Data.m_CreationType, p_Object);

	{
		ListDeserializer<ObjectIdentity, ObjectIdentityDeserializer> identitiesDeserializer;
		identitiesDeserializer.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(identitiesDeserializer, _YUID(O365_USER_IDENTITIES), p_Object))
			m_Data.m_Identities = std::move(identitiesDeserializer.GetData());
	}
	deserializeTimeDateAndTagObject(_YUID(O365_USER_LASTPASSWORDCHANGEDATETIME), m_Data.m_LastPasswordChangeDateTime, p_Object);
	{
		ListDeserializer<LicenseAssignmentState, LicenseAssignmentStateDeserializer> licAssStatesDeserializer;
		licAssStatesDeserializer.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(licAssStatesDeserializer, _YUID(O365_USER_LICENSEASSIGNMENTSTATES), p_Object))
			m_Data.m_LicenseAssignmentStates = std::move(licAssStatesDeserializer.GetData());
	}
	deserializeTimeDateAndTagObject(_YUID(O365_USER_REFRESTOKENVALIDFROMDATETIME), m_Data.m_RefresTokenValidFromDateTime, p_Object);

	tagObjectFromComputedProperties();
}
