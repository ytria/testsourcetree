#include "RequiredResourceAccessDeserializer.h"
#include "ListDeserializer.h"
#include "ResourceAccess.h"
#include "JsonSerializeUtil.h"
#include "ResourceAccessDeserializer.h"

void RequiredResourceAccessDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		ListDeserializer<ResourceAccess, ResourceAccessDeserializer> resAccessDeserializer;
		if (JsonSerializeUtil::DeserializeAny(resAccessDeserializer, _YTEXT("resourceAccess"), p_Object))
			m_Data.m_ResourceAccess = std::move(resAccessDeserializer.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("resourceAppId"), m_Data.m_ResourceAppId, p_Object);
}
