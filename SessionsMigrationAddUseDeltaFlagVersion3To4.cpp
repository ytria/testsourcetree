#include "SessionsMigrationAddUseDeltaFlagVersion3To4.h"
#include "SessionMigrationUtil.h"
#include "SessionsSqlEngine.h"
#include "SQLSafeTransaction.h"
#include "SqlQueryMultiPreparedStatement.h"

void SessionsMigrationAddUseDeltaFlagVersion3To4::Build()
{
	AddStep(std::bind(&SessionsMigrationAddUseDeltaFlagVersion3To4::CreateNewFromOld, this));
	AddStep(std::bind(&SessionsMigrationAddUseDeltaFlagVersion3To4::AddDeltaFlag, this));
}

VersionSourceTarget SessionsMigrationAddUseDeltaFlagVersion3To4::GetVersionInfo() const
{
	return { 3, 4 };
}

bool SessionsMigrationAddUseDeltaFlagVersion3To4::CreateNewFromOld()
{
	return SessionMigrationUtil::CopyDatabase(GetVersionInfo());
}

bool SessionsMigrationAddUseDeltaFlagVersion3To4::AddDeltaFlag()
{
	bool success = false;

	SQLSafeTransaction transaction(GetTargetSQLEngine(), [this, &success] {
		const std::vector<wstring> statements =
		{
			LR"(ALTER TABLE Sessions ADD COLUMN UseDeltaUsers INTEGER DEFAULT 1;)",
			LR"(ALTER TABLE Sessions ADD COLUMN UseDeltaGroups INTEGER DEFAULT 1;)"
		};

		SqlQueryMultiPreparedStatement query(GetTargetSQLEngine(), statements);
		query.Run();

		success = query.IsSuccess();
	}, true);
	
	return transaction.Run() && success;
}
