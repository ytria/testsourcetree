#pragma once

#include "IRequester.h"
#include "HttpResultWithError.h"

class SendMessageRequester : public IRequester
{
public:
	SendMessageRequester(const wstring& p_MessageId);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const HttpResultWithError& GetResult() const;

	void UseMainMSGraphSession();

private:
	wstring m_MessageId;
	std::shared_ptr<HttpResultWithError> m_Result;

	bool m_UseMainMSGraphSession;
};

