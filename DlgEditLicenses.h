#pragma once

#include "DlgFormsHTML.h"
#include "SkuInfo.h"

class GridLicenses;

class DlgEditLicenses : public DlgFormsHTML
{
public:
    DlgEditLicenses(GridLicenses& p_Grid, CWnd* p_Parent);

	const map<PooledString, std::set<SkuInfo>>& GetUsersSkus() const;
	const map<PooledString, std::set<SkuInfo>>& GetNewUsersSkus() const;

	static const wstring g_ON;
	static const wstring g_OFF;
	static const wstring g_MIX;

protected:
	void generateJSONScriptData() override;
	bool processPostedData(const IPostedDataTarget::PostedData& data) override;

    wstring getHbsJs() const override;
    wstring getOtherJs() const override;
    wstring getCss() const override;

private:
	vector<web::json::value> generateItems(const std::set<SkuInfo>& p_Skus);
    wstring GetSkuState(const map<PooledString, std::set<SkuInfo>>& p_UserSkus, const SkuInfo& sku) const;
    void generateItemChoices(const SkuInfo& p_Sku, const wstring& p_SkuState, web::json::value& p_Item) const;

    GridLicenses& m_Grid;

	map<PooledString, std::set<SkuInfo>> m_UserSkus;
	map<PooledString, std::set<SkuInfo>> m_NewUserSkus;
};
