#include "DlgBusinessMessageRuleEditHTML.h"

#include "GridMessageRules.h"


const wstring DlgBusinessMessageRuleEditHTML::g_ListSeparator = _YTEXT(";");

DlgBusinessMessageRuleEditHTML::DlgBusinessMessageRuleEditHTML(vector<BusinessMessageRule>& p_BusinessMessageRules, DlgFormsHTML::Action p_Action, const wstring& p_ComponentType, CWnd* p_Parent)
	: DlgFormsHTML(p_Action, p_Parent, _YTEXT(""))
	, m_ComponentType(p_ComponentType)
	, m_BusinessMessageRules(p_BusinessMessageRules)
{
}

DlgBusinessMessageRuleEditHTML::~DlgBusinessMessageRuleEditHTML()
{

}

bool DlgBusinessMessageRuleEditHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	std::set<wstring> alreadyProcessedProperties;
	for (const auto& item : data)
	{
		const auto& propName = item.first;
		ASSERT(!propName.empty());
		const auto& propValue = item.second;

		{
			auto it = m_RuleStringProps.find(propName);
			if (m_RuleStringProps.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties.insert(propName);

				//boost::YOpt<PooledString> value;
				//if (!propValue.empty())
				//	value = propValue;
				for (auto& rule : m_BusinessMessageRules)
					rule.*(it->second) = propValue/*value*/;

				continue;
			}
		}

		{
			auto it = m_RuleBoolProps.find(propName);
			if (m_RuleBoolProps.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties.insert(propName);

				const bool boolVal = Str::getBoolFromString(propValue);
				/*boost::YOpt<bool> value;
				if (boolVal)
					value = boolVal;*/
				for (auto& rule : m_BusinessMessageRules)
					rule.*(it->second) = boolVal/*value*/;

				continue;
			}
		}

		{
			auto it = m_ActionStringProps.find(propName);
			if (m_ActionStringProps.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties.insert(propName);

				bool eraseSiblingIfModified = false;
				boost::YOpt<PooledString> BusinessMessageRuleActions::*sibling;

				if (propName == _YTEXT("copyToFolderName"))
				{
					eraseSiblingIfModified = true;

					sibling = &BusinessMessageRuleActions::m_CopyToFolderID;
				}
				else if (propName == _YTEXT("moveToFolderName"))
				{
					eraseSiblingIfModified = true;
					sibling = &BusinessMessageRuleActions::m_MoveToFolderID;
				}

				boost::YOpt<PooledString> value;
				if (!propValue.empty())
					value = propValue;
				for (auto& rule : m_BusinessMessageRules)
				{
					if (eraseSiblingIfModified && value != rule.m_Actions.*(it->second) && rule.m_Actions.*(sibling))
						rule.m_Actions.*(sibling) = boost::none;
					rule.m_Actions.*(it->second) = value;
				}			

				continue;
			}
		}

		{
			auto it = m_ActionBoolProps.find(propName);
			if (m_ActionBoolProps.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties.insert(propName);

				const bool boolVal = Str::getBoolFromString(propValue);
				boost::YOpt<bool> value;
				if (boolVal)
					value = boolVal;
				for (auto& rule : m_BusinessMessageRules)
					rule.m_Actions.*(it->second) = value;

				continue;
			}
		}

		{
			auto it = m_PredicateStringProps.find(propName);
			if (m_PredicateStringProps.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties.insert(propName);

				boost::YOpt<PooledString> value;
				if (!propValue.empty())
					value = propValue;
				if (PredicateType::CONDITION == it->second.first)
				{
					for (auto& rule : m_BusinessMessageRules)
						rule.m_Conditions.*(it->second.second) = value;
				}
				else
				{
					ASSERT(PredicateType::EXCEPTION == it->second.first);
					for (auto& rule : m_BusinessMessageRules)
						rule.m_Exceptions.*(it->second.second) = value;
				}

				continue;
			}
		}

		{
			auto it = m_PredicateBoolProps.find(propName);
			if (m_PredicateBoolProps.end() != it)
			{
				ASSERT(alreadyProcessedProperties.end() == alreadyProcessedProperties.find(propName));
				alreadyProcessedProperties.insert(propName);

				const bool boolVal = Str::getBoolFromString(propValue);
				boost::YOpt<bool> value;
				if (boolVal)
					value = boolVal;
				if (PredicateType::CONDITION == it->second.first)
				{
					for (auto& rule : m_BusinessMessageRules)
						rule.m_Conditions.*(it->second.second) = value;
				}
				else
				{
					ASSERT(PredicateType::EXCEPTION == it->second.first);
					for (auto& rule : m_BusinessMessageRules)
						rule.m_Exceptions.*(it->second.second) = value;
				}

				continue;
			}
		}

		{
			auto it = m_PredicateStringVectorProps.find(propName);
			if (m_PredicateStringVectorProps.end() != it)
			{
				auto first = alreadyProcessedProperties.insert(propName).second;

				if (PredicateType::CONDITION == it->second.first)
				{
					for (auto& rule : m_BusinessMessageRules)
					{
						if (first)
							(rule.m_Conditions.*(it->second.second)).emplace();
						(rule.m_Conditions.*(it->second.second))->emplace_back(propValue);
					}
				}
				else
				{
					ASSERT(PredicateType::EXCEPTION == it->second.first);
					for (auto& rule : m_BusinessMessageRules)
					{
						if (first)
							(rule.m_Exceptions.*(it->second.second)).emplace();
						(rule.m_Exceptions.*(it->second.second))->emplace_back(propValue);
					}
				}

				continue;
			}
		}
	}

	return true;
}

void DlgBusinessMessageRuleEditHTML::generateJSONScriptData()
{
	static const ComboEditor::LabelsAndValues importances{
		{ YtriaTranslate::Do(NDatabaseInfoExt_getString_6, _YLOC("Low")).c_str()		, _YTEXT("low")		},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_2, _YLOC("Normal")).c_str()	, _YTEXT("normal")	},
		{ YtriaTranslate::Do(DlgMiscProperties_OnInitDialog_15, _YLOC("High")).c_str()	, _YTEXT("high")	} };

	static const ComboEditor::LabelsAndValues messageActionFlags{
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_4, _YLOC("Any")).c_str()						, _YTEXT("any")					},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_5, _YLOC("Call")).c_str()					, _YTEXT("call")				},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_6, _YLOC("Do Not Forward")).c_str()			, _YTEXT("doNotForward")		},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_7, _YLOC("Follow Up")).c_str()				, _YTEXT("followUp")			},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_8, _YLOC("For Your Information")).c_str()	, _YTEXT("fyi")					},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_9, _YLOC("Forward")).c_str()					, _YTEXT("forward")				},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_10, _YLOC("No Response Necessary")).c_str()	, _YTEXT("noResponseNecessary")	},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_11, _YLOC("Read")).c_str()					, _YTEXT("read")				},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_12, _YLOC("Reply")).c_str()					, _YTEXT("reply")				},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_13, _YLOC("Reply To All")).c_str()			, _YTEXT("replyToAll")			},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_14, _YLOC("Review")).c_str()					, _YTEXT("review")				} };

	static const ComboEditor::LabelsAndValues sensitivities{
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_15, _YLOC("Normal")).c_str()			, _YTEXT("normal")			},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_16, _YLOC("Personal")).c_str()		, _YTEXT("personal")		},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_17, _YLOC("Private")).c_str()			, _YTEXT("private")			},
		{ YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_18, _YLOC("Confidential")).c_str()	, _YTEXT("confidential")	} };

	initMain(_YTEXT("DlgBusinessMessageRuleEditHTML"), false, false, true);

	if (isCreateDialog() || isEditCreateDialog())
	{
		ASSERT(false);
	}
	else
	{
		ASSERT(isEditDialog()/* || isReadDialog()*/);

		getGenerator().addCategory(YtriaTranslate::Do(ColumnInfoGrid_AddColumnData_3, _YLOC("Info")).c_str(), m_ComponentType.empty() ? CategoryFlags::EXPAND_BY_DEFAULT : CategoryFlags::NOFLAG);
		{
			addStringEditor(&BusinessMessageRule::m_DisplayName, _YTEXT("displayName"), YtriaTranslate::Do(GridMessageRules_customizeGrid_2, _YLOC("Rule Name")).c_str(), 0);
			addBoolToggleEditor(&BusinessMessageRule::m_IsEnabled, _YTEXT("isEnabled"), YtriaTranslate::Do(GridMessageRules_customizeGrid_4, _YLOC("Is Enabled")).c_str(), 0);
// 			/*Editing this is buggy on microsoft side and makes no sense anyway*///addBoolToggleEditor(&BusinessMessageRule::m_IsReadOnly, _YTEXT("isReadOnly"),  _YCOM("Read Only"), 0);
		}

		getGenerator().addCategory(YtriaTranslate::Do(GridMessageRules_customizeGrid_10, _YLOC("Conditions")).c_str(), m_ComponentType == GridMessageRules::GetComponentTypeCondition() ? CategoryFlags::EXPAND_BY_DEFAULT : CategoryFlags::NOFLAG);
		{
			addMultiValueEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_BodyContains			, _YTEXT("cond-bodyContains")			, YtriaTranslate::Do(GridMessageRules_customizeGrid_11, _YLOC("Body Contains")).c_str(), 0);//auto BusinessMessageRulePredicates::*BodyContains = &BusinessMessageRulePredicates::m_BodyContains; // vector
			addMultiValueEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_BodyOrSubjectContains	, _YTEXT("cond-bodyOrSubjectContains")	, YtriaTranslate::Do(GridMessageRules_customizeGrid_12, _YLOC("Body Or Subject Contains")).c_str(), 0);//auto BusinessMessageRulePredicates::*BodyOrSubjectContains = &BusinessMessageRulePredicates::m_BodyOrSubjectContains; // vector
			addMultiValueEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_Categories				, _YTEXT("cond-categories")				, YtriaTranslate::Do(GridContacts_customizeGrid_64, _YLOC("Categories")).c_str(), 0);//auto BusinessMessageRulePredicates::*Categories = &BusinessMessageRulePredicates::m_Categories; // vector
			//auto BusinessMessageRulePredicates::*FromAddresses = &BusinessMessageRulePredicates::m_FromAddresses; // vector
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_HasAttachments			, _YTEXT("cond-hasAttachments")			, YtriaTranslate::Do(GridConversations_customizeGrid_5, _YLOC("Has Attachments")).c_str(), 0);// auto BusinessMessageRulePredicates::*HasAttachments = &BusinessMessageRulePredicates::m_HasAttachments; // bool
			addMultiValueEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_HeaderContains			, _YTEXT("cond-headerContains")			, YtriaTranslate::Do(GridMessageRules_customizeGrid_18, _YLOC("Header Contains")).c_str(), 0);//auto BusinessMessageRulePredicates::*HeaderContains = &BusinessMessageRulePredicates::m_HeaderContains; // vector
			addComboEditor		(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_Importance				, _YTEXT("cond-importance")				, YtriaTranslate::Do(GridEvents_customizeGrid_33, _YLOC("Importance")).c_str(), 0, importances);// auto BusinessMessageRulePredicates::*Importance = &BusinessMessageRulePredicates::m_Importance; // String
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_IsApprovalRequest		, _YTEXT("cond-isApprovalRequest")		, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_29, _YLOC("Is Approval Request")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsApprovalRequest = &BusinessMessageRulePredicates::m_IsApprovalRequest; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_IsAutomaticForward		, _YTEXT("cond-isAutomaticForward")		, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_30, _YLOC("Is Automatic Forward")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsAutomaticForward = &BusinessMessageRulePredicates::m_IsAutomaticForward; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_IsAutomaticReply		, _YTEXT("cond-isAutomaticReply")		, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_31, _YLOC("Is Automatic Reply")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsAutomaticReply = &BusinessMessageRulePredicates::m_IsAutomaticReply; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_IsEncrypted			, _YTEXT("cond-isEncrypted")			, YtriaTranslate::Do(GridMessageRules_customizeGrid_23, _YLOC("Is Encrypted")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsEncrypted = &BusinessMessageRulePredicates::m_IsEncrypted; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_IsMeetingRequest		, _YTEXT("cond-isMeetingRequest")		, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_33, _YLOC("Is Meeting Request")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsMeetingRequest = &BusinessMessageRulePredicates::m_IsMeetingRequest; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_IsMeetingResponse		, _YTEXT("cond-isMeetingResponse")		, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_34, _YLOC("Is Meeting Response")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsMeetingResponse = &BusinessMessageRulePredicates::m_IsMeetingResponse; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_IsNonDeliveryReport	, _YTEXT("cond-isNonDeliveryReport")	, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_35, _YLOC("Is Non Delivery Report")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsNonDeliveryReport = &BusinessMessageRulePredicates::m_IsNonDeliveryReport; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_IsPermissionControlled	, _YTEXT("cond-isPermissionControlled")	, YtriaTranslate::Do(GridMessageRules_customizeGrid_27, _YLOC("Is Permission Controlled")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsPermissionControlled = &BusinessMessageRulePredicates::m_IsPermissionControlled; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_IsReadReceipt			, _YTEXT("cond-isReadReceipt")			, YtriaTranslate::Do(GridMessageRules_customizeGrid_28, _YLOC("Is Read Receipt")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsReadReceipt = &BusinessMessageRulePredicates::m_IsReadReceipt; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_IsSigned				, _YTEXT("cond-isSigned")				, YtriaTranslate::Do(GridMessageRules_customizeGrid_29, _YLOC("Is Signed")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsSigned = &BusinessMessageRulePredicates::m_IsSigned; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_IsVoicemail			, _YTEXT("cond-isVoicemail")			, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_39, _YLOC("Is Voicemail")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsVoicemail = &BusinessMessageRulePredicates::m_IsVoicemail; // bool
			addComboEditor		(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_MessageActionFlag		, _YTEXT("cond-messageActionFlag")		, YtriaTranslate::Do(GridMessageRules_customizeGrid_31, _YLOC("Message Action Flag")).c_str(), 0, messageActionFlags);//auto BusinessMessageRulePredicates::*MessageActionFlag = &BusinessMessageRulePredicates::m_MessageActionFlag; // String
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_NotSentToMe			, _YTEXT("cond-notSentToMe")			, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_41, _YLOC("Not Sent To Me")).c_str(), 0);//auto BusinessMessageRulePredicates::*NotSentToMe = &BusinessMessageRulePredicates::m_NotSentToMe; // bool
			//auto BusinessMessageRulePredicates::*RecipientContains = &BusinessMessageRulePredicates::m_RecipientContains; // vector
			//auto BusinessMessageRulePredicates::*SenderContains = &BusinessMessageRulePredicates::m_SenderContains; // vector
			addComboEditor		(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_Sensitivity			, _YTEXT("cond-sensitivity")			, YtriaTranslate::Do(GridMessageRules_customizeGrid_35, _YLOC("Sensitivity")).c_str(), 0, sensitivities);//auto BusinessMessageRulePredicates::*Sensitivity = &BusinessMessageRulePredicates::m_Sensitivity; // String
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_SentCCMe				, _YTEXT("cond-sentCCMe")				, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_43, _YLOC("Sent CC Me")).c_str(), 0);//auto BusinessMessageRulePredicates::*SentCCMe = &BusinessMessageRulePredicates::m_SentCCMe; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_SentOnlyToMe			, _YTEXT("cond-sentOnlyToMe")			, YtriaTranslate::Do(GridMessageRules_customizeGrid_37, _YLOC("Sent Only To Me")).c_str(), 0);//auto BusinessMessageRulePredicates::*SentOnlyToMe = &BusinessMessageRulePredicates::m_SentOnlyToMe; // bool
			//auto BusinessMessageRulePredicates::*SentToAddresses = &BusinessMessageRulePredicates::m_SentToAddresses; // vector<Recipients>
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_SentToMe				, _YTEXT("cond-sentToMe")				, YtriaTranslate::Do(GridMessageRules_customizeGrid_41, _YLOC("Sent To Me")).c_str(), 0);//auto BusinessMessageRulePredicates::*SentToMe = &BusinessMessageRulePredicates::m_SentToMe; // bool
			addBoolToggleEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_SentToMe				, _YTEXT("cond-sentToOrCCMe")			, YtriaTranslate::Do(GridMessageRules_customizeGrid_42, _YLOC("Sent To or CC Me")).c_str(), 0);//auto BusinessMessageRulePredicates::*SentToOrCCMe = &BusinessMessageRulePredicates::m_SentToOrCCMe; // bool
			addMultiValueEditor	(PredicateType::CONDITION, &BusinessMessageRulePredicates::m_SubjectContains		, _YTEXT("cond-subjectContains")		, YtriaTranslate::Do(GridMessageRules_customizeGrid_43, _YLOC("Subject Contains")).c_str(), 0);//auto BusinessMessageRulePredicates::*SubjectContains = &BusinessMessageRulePredicates::m_SubjectContains; //vector
			//auto BusinessMessageRulePredicates::*WithinSizeRange = &BusinessMessageRulePredicates::m_WithinSizeRange; //SizeRange
		}

		getGenerator().addCategory(YtriaTranslate::Do(GridMessageRules_customizeGrid_46, _YLOC("Actions")).c_str(), m_ComponentType == GridMessageRules::GetComponentTypeAction() ? CategoryFlags::EXPAND_BY_DEFAULT : CategoryFlags::NOFLAG);
		{
			//auto BusinessMessageRuleActions::*AssignCategories = &BusinessMessageRuleActions::m_AssignCategories; //vector
			/*Id won't be displayed in editor*/ //addStringEditor(&BusinessMessageRuleActions::m_CopyToFolderID, _YTEXT("copyToFolderId"), YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_49, _YLOC("Copy To Folder ID")).c_str(), 0);//auto BusinessMessageRuleActions::*CopyToFolderID = &BusinessMessageRuleActions::m_CopyToFolderID; //String
			/* ? Let the user enter a folder name and try to find in after closing dialog*/
			//addStringEditor(&BusinessMessageRuleActions::m_CopyToFolderName, _YTEXT("copyToFolderName"), YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_50, _YLOC("Copy To Folder Name")).c_str(), 0);//auto BusinessMessageRuleActions::*CopyToFolderName = &BusinessMessageRuleActions::m_CopyToFolderName; //STring
			addBoolToggleEditor(&BusinessMessageRuleActions::m_Delete, _YTEXT("delete"), YtriaTranslate::Do(GridMessageRules_customizeGrid_51, _YLOC("Delete")).c_str(), 0);//auto BusinessMessageRuleActions::*Delete = &BusinessMessageRuleActions::m_Delete; // bool
			//auto BusinessMessageRuleActions::*ForwardAsAttachmentTo = &BusinessMessageRuleActions::m_ForwardAsAttachmentTo; //vector<Recipient>
			//auto BusinessMessageRuleActions::*ForwardTo = &BusinessMessageRuleActions::m_ForwardTo; // vector<Recipient>
			addBoolToggleEditor(&BusinessMessageRuleActions::m_MarkAsRead, _YTEXT("markAsRead"), YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_52, _YLOC("MarkAsRead")).c_str(), 0);//auto BusinessMessageRuleActions::*MarkAsRead = &BusinessMessageRuleActions::m_MarkAsRead; // bool
			addStringEditor(&BusinessMessageRuleActions::m_MarkImportance, _YTEXT("markImportance"), YtriaTranslate::Do(GridMessageRules_customizeGrid_59, _YLOC("Mark Importance")).c_str(), 0);//auto BusinessMessageRuleActions::*MarkImportance = &BusinessMessageRuleActions::m_MarkImportance; //String
			/*Id won't be displayed in editor*/ addStringEditor(&BusinessMessageRuleActions::m_MoveToFolderID, _YTEXT("moveToFolderID"), YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_54, _YLOC("Move To Folder ID")).c_str(), 0);//auto BusinessMessageRuleActions::*MoveToFolderID = &BusinessMessageRuleActions::m_MoveToFolderID; //String
			/* ? Let the user enter a folder name and try to find in after closing dialog*/
			//addStringEditor(&BusinessMessageRuleActions::m_MoveToFolderName, _YTEXT("moveToFolderName"), YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_55, _YLOC("Move To Folder Name")).c_str(), 0);//auto BusinessMessageRuleActions::*MoveToFolderName = &BusinessMessageRuleActions::m_MoveToFolderName; //String
			addBoolToggleEditor(&BusinessMessageRuleActions::m_PermanentDelete, _YTEXT("permanentDelete"), YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_56, _YLOC("PermanentDelete")).c_str(), 0);//auto BusinessMessageRuleActions::*PermanentDelete = &BusinessMessageRuleActions::m_PermanentDelete; // bool
			//auto BusinessMessageRuleActions::*RedirectTo = &BusinessMessageRuleActions::m_RedirectTo; // vecto<recipient>
			addBoolToggleEditor(&BusinessMessageRuleActions::m_StopProcessingRules, _YTEXT("stopProcessingRules"), YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_57, _YLOC("StopProcessingRules")).c_str(), 0);//auto BusinessMessageRuleActions::*StopProcessingRules = &BusinessMessageRuleActions::m_StopProcessingRules; // bool
		}

		getGenerator().addCategory(YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_58, _YLOC("Exceptions")).c_str(), m_ComponentType == GridMessageRules::GetComponentTypeException() ? CategoryFlags::EXPAND_BY_DEFAULT : CategoryFlags::NOFLAG);
		{
			addMultiValueEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_BodyContains			, _YTEXT("ex-bodyContains")				, YtriaTranslate::Do(GridMessageRules_customizeGrid_11, _YLOC("Body Contains")).c_str(), 0);//auto BusinessMessageRulePredicates::*BodyContains = &BusinessMessageRulePredicates::m_BodyContains; // vector
			addMultiValueEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_BodyOrSubjectContains	, _YTEXT("ex-bodyOrSubjectContains")	, YtriaTranslate::Do(GridMessageRules_customizeGrid_12, _YLOC("Body Or Subject Contains")).c_str(), 0);//auto BusinessMessageRulePredicates::*BodyOrSubjectContains = &BusinessMessageRulePredicates::m_BodyOrSubjectContains; // vector
			addMultiValueEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_Categories				, _YTEXT("ex-categories")				, YtriaTranslate::Do(GridContacts_customizeGrid_64, _YLOC("Categories")).c_str(), 0);//auto BusinessMessageRulePredicates::*Categories = &BusinessMessageRulePredicates::m_Categories; // vector
			//auto BusinessMessageRulePredicates::*FromAddresses = &BusinessMessageRulePredicates::m_FromAddresses; // vector
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_HasAttachments			, _YTEXT("ex-hasAttachments")			, YtriaTranslate::Do(GridMessageRules_customizeGrid_17, _YLOC("Has Attachments")).c_str(), 0);// auto BusinessMessageRulePredicates::*HasAttachments = &BusinessMessageRulePredicates::m_HasAttachments; // bool
			addMultiValueEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_HeaderContains			, _YTEXT("ex-headerContains")			, YtriaTranslate::Do(GridMessageRules_customizeGrid_18, _YLOC("Header Contains")).c_str(), 0);//auto BusinessMessageRulePredicates::*HeaderContains = &BusinessMessageRulePredicates::m_HeaderContains; // vector
			addComboEditor		(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_Importance				, _YTEXT("ex-importance")				, YtriaTranslate::Do(GridEvents_customizeGrid_33, _YLOC("Importance")).c_str(), 0, importances);// auto BusinessMessageRulePredicates::*Importance = &BusinessMessageRulePredicates::m_Importance; // String
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_IsApprovalRequest		, _YTEXT("ex-isApprovalRequest")		, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_29, _YLOC("Is Approval Request")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsApprovalRequest = &BusinessMessageRulePredicates::m_IsApprovalRequest; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_IsAutomaticForward		, _YTEXT("ex-isAutomaticForward")		, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_30, _YLOC("Is Automatic Forward")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsAutomaticForward = &BusinessMessageRulePredicates::m_IsAutomaticForward; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_IsAutomaticReply		, _YTEXT("ex-isAutomaticReply")			, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_31, _YLOC("Is Automatic Reply")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsAutomaticReply = &BusinessMessageRulePredicates::m_IsAutomaticReply; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_IsEncrypted			, _YTEXT("ex-isEncrypted")				, YtriaTranslate::Do(GridMessageRules_customizeGrid_23, _YLOC("Is Encrypted")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsEncrypted = &BusinessMessageRulePredicates::m_IsEncrypted; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_IsMeetingRequest		, _YTEXT("ex-isMeetingRequest")			, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_33, _YLOC("Is Meeting Request")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsMeetingRequest = &BusinessMessageRulePredicates::m_IsMeetingRequest; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_IsMeetingResponse		, _YTEXT("ex-isMeetingResponse")		, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_34, _YLOC("Is Meeting Response")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsMeetingResponse = &BusinessMessageRulePredicates::m_IsMeetingResponse; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_IsNonDeliveryReport	, _YTEXT("ex-isNonDeliveryReport")		, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_35, _YLOC("Is Non Delivery Report")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsNonDeliveryReport = &BusinessMessageRulePredicates::m_IsNonDeliveryReport; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_IsPermissionControlled	, _YTEXT("ex-isPermissionControlled")	, YtriaTranslate::Do(GridMessageRules_customizeGrid_27, _YLOC("Is Permission Controlled")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsPermissionControlled = &BusinessMessageRulePredicates::m_IsPermissionControlled; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_IsReadReceipt			, _YTEXT("ex-isReadReceipt")			, YtriaTranslate::Do(GridMessageRules_customizeGrid_28, _YLOC("Is Read Receipt")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsReadReceipt = &BusinessMessageRulePredicates::m_IsReadReceipt; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_IsSigned				, _YTEXT("ex-isSigned")					, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_74, _YLOC("IsS igned")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsSigned = &BusinessMessageRulePredicates::m_IsSigned; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_IsVoicemail			, _YTEXT("ex-isVoicemail")				, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_39, _YLOC("Is Voicemail")).c_str(), 0);//auto BusinessMessageRulePredicates::*IsVoicemail = &BusinessMessageRulePredicates::m_IsVoicemail; // bool
			addComboEditor		(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_MessageActionFlag		, _YTEXT("ex-messageActionFlag")		, YtriaTranslate::Do(GridMessageRules_customizeGrid_31, _YLOC("Message Action Flag")).c_str(), 0, messageActionFlags);//auto BusinessMessageRulePredicates::*MessageActionFlag = &BusinessMessageRulePredicates::m_MessageActionFlag; // String
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_NotSentToMe			, _YTEXT("ex-notSentToMe")				, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_41, _YLOC("Not Sent To Me")).c_str(), 0);//auto BusinessMessageRulePredicates::*NotSentToMe = &BusinessMessageRulePredicates::m_NotSentToMe; // bool
			//auto BusinessMessageRulePredicates::*RecipientContains = &BusinessMessageRulePredicates::m_RecipientContains; // vector
			//auto BusinessMessageRulePredicates::*SenderContains = &BusinessMessageRulePredicates::m_SenderContains; // vector
			addComboEditor		(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_Sensitivity			, _YTEXT("ex-sensitivity")				, YtriaTranslate::Do(GridMessageRules_customizeGrid_35, _YLOC("Sensitivity")).c_str(), 0, sensitivities);//auto BusinessMessageRulePredicates::*Sensitivity = &BusinessMessageRulePredicates::m_Sensitivity; // String
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_SentCCMe				, _YTEXT("ex-sentCCMe")					, YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_generateJSONScriptData_43, _YLOC("Sent CC Me")).c_str(), 0);//auto BusinessMessageRulePredicates::*SentCCMe = &BusinessMessageRulePredicates::m_SentCCMe; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_SentOnlyToMe			, _YTEXT("ex-sentOnlyToMe")				, YtriaTranslate::Do(GridMessageRules_customizeGrid_37, _YLOC("Sent Only To Me")).c_str(), 0);//auto BusinessMessageRulePredicates::*SentOnlyToMe = &BusinessMessageRulePredicates::m_SentOnlyToMe; // bool
			//auto BusinessMessageRulePredicates::*SentToAddresses = &BusinessMessageRulePredicates::m_SentToAddresses; // vector<Recipients>
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_SentToMe				, _YTEXT("ex-sentToMe")					, YtriaTranslate::Do(GridMessageRules_customizeGrid_41, _YLOC("Sent To Me")).c_str(), 0);//auto BusinessMessageRulePredicates::*SentToMe = &BusinessMessageRulePredicates::m_SentToMe; // bool
			addBoolToggleEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_SentToMe				, _YTEXT("ex-sentToOrCCMe")				, YtriaTranslate::Do(GridMessageRules_customizeGrid_42, _YLOC("Sent To or CC Me")).c_str(), 0);//auto BusinessMessageRulePredicates::*SentToOrCCMe = &BusinessMessageRulePredicates::m_SentToOrCCMe; // bool
			addMultiValueEditor	(PredicateType::EXCEPTION, &BusinessMessageRulePredicates::m_SubjectContains		, _YTEXT("ex-subjectContains")			, YtriaTranslate::Do(GridMessageRules_customizeGrid_43, _YLOC("Subject Contains")).c_str(), 0);//auto BusinessMessageRulePredicates::*SubjectContains = &BusinessMessageRulePredicates::m_SubjectContains; //vector
			//auto BusinessMessageRulePredicates::*WithinSizeRange = &BusinessMessageRulePredicates::m_WithinSizeRange; //SizeRange
		}
	}


	/****/


	/*if (isReadDialog())
	{
		addCloseButton(LocalizedStrings::g_CloseButtonText);
	}
	else*/
	{
		getGenerator().addApplyButton(isEditDialog() ? LocalizedStrings::g_ApplyButtonText : LocalizedStrings::g_CreateButtonText);
		getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
		getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);
	}
}

// =================================================================================================

void DlgBusinessMessageRuleEditHTML::addStringEditor(boost::YOpt<PooledString> BusinessMessageRule::*p_String, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags)
{
	// "No change" flag is computed here. You should not set it.
	ASSERT(0 == (EditorFlags::NOCHANGE & p_Flags));

	wstring value;

	{
		bool allTheSame = true;
		const auto val = m_BusinessMessageRules.front().*p_String;
		for (const auto& object : m_BusinessMessageRules)
		{
			if (val != object.*p_String)
			{
				allTheSame = false;
				break;
			}
		}

		if (allTheSame)
			value = val.is_initialized() ? (const wstring)val.get() : L"";
		else
			p_Flags = p_Flags | EditorFlags::NOCHANGE;
	}

	getGenerator().Add(StringEditor(p_PropName, p_PropLabel, p_Flags, value));

	ASSERT(!hasProperty(p_PropName));
	m_RuleStringProps[p_PropName] = p_String;
}

void DlgBusinessMessageRuleEditHTML::addBoolToggleEditor(boost::YOpt<bool> BusinessMessageRule::*p_Bool, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides /*= BoolEditor::CheckUncheckedLabelOverrides()*/)
{
	// "No change" flag is computed here. You should not set it.
	ASSERT(0 == (EditorFlags::NOCHANGE & p_Flags));

	bool value = false;

	{
		bool allTheSame = true;
		const auto val = m_BusinessMessageRules.front().*p_Bool;
		for (const auto& object : m_BusinessMessageRules)
		{
			if (val != object.*p_Bool)
			{
				allTheSame = false;
				break;
			}
		}

		if (allTheSame)
			value = val.is_initialized() ? val.get() : false;
		else
			p_Flags = p_Flags | EditorFlags::NOCHANGE;
	}

	getGenerator().Add(BoolToggleEditor({ { p_PropName, p_PropLabel, p_Flags, value }, p_CheckUncheckedLabelOverrides }));

	ASSERT(!hasProperty(p_PropName));
	m_RuleBoolProps[p_PropName] = p_Bool;
}

// =================================================================================================

void DlgBusinessMessageRuleEditHTML::addStringEditor(boost::YOpt<PooledString> BusinessMessageRuleActions::*p_String, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags)
{
	// "No change" flag is computed here. You should not set it.
	ASSERT(0 == (EditorFlags::NOCHANGE & p_Flags));

	wstring value;

	{
		bool allTheSame = true;
		const auto val = m_BusinessMessageRules.front().m_Actions.*p_String;
		for (const auto& object : m_BusinessMessageRules)
		{
			if (val != object.m_Actions.*p_String)
			{
				allTheSame = false;
				break;
			}
		}

		if (allTheSame)
			value = val.is_initialized() ? (const wstring)val.get() : L"";
		else
			p_Flags = p_Flags | EditorFlags::NOCHANGE;
	}

	getGenerator().Add(StringEditor(p_PropName, p_PropLabel, p_Flags, value));

	ASSERT(!hasProperty(p_PropName));
	m_ActionStringProps[p_PropName] = p_String;
}

void DlgBusinessMessageRuleEditHTML::addComboEditor(boost::YOpt<PooledString> BusinessMessageRuleActions::*p_String, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues)
{
	// "No change" flag is computed here. You should not set it.
	ASSERT(0 == (EditorFlags::NOCHANGE & p_Flags));

	// "restriction" flags are computed via p_GetRestrictions. You should not set them.
	ASSERT(0 == (EditorFlags::RESTRICT_LOAD_MORE & p_Flags) && 0 == (EditorFlags::RESTRICT_NOT_CREATED & p_Flags) && 0 == (EditorFlags::RESTRICT_TYPE & p_Flags));

	wstring value;

	{
		bool allTheSame = true;
		const auto val = m_BusinessMessageRules.front().m_Actions.*p_String;
		for (const auto& object : m_BusinessMessageRules)
		{
			if (val != object.m_Actions.*p_String)
			{
				allTheSame = false;
				break;
			}
		}

		if (allTheSame)
			value = val.is_initialized() ? (const wstring)val.get() : L"";
		else
			p_Flags = p_Flags | EditorFlags::NOCHANGE;
	}

	getGenerator().Add(ComboEditor({ p_PropName, p_PropLabel, p_Flags, value }, p_LabelsAndValues));

	ASSERT(!hasProperty(p_PropName));
	m_ActionStringProps[p_PropName] = p_String;
}

void DlgBusinessMessageRuleEditHTML::addBoolToggleEditor(boost::YOpt<bool> BusinessMessageRuleActions::*p_Bool, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides /*= BoolEditor::CheckUncheckedLabelOverrides()*/)
{
	// "No change" flag is computed here. You should not set it.
	ASSERT(0 == (EditorFlags::NOCHANGE & p_Flags));

	bool value = false;

	{
		bool allTheSame = true;
		const auto val = m_BusinessMessageRules.front().m_Actions.*p_Bool;
		for (const auto& object : m_BusinessMessageRules)
		{
			if (val != object.m_Actions.*p_Bool)
			{
				allTheSame = false;
				break;
			}
		}

		if (allTheSame)
			value = val.is_initialized() ? val.get() : false;
		else
			p_Flags = p_Flags | EditorFlags::NOCHANGE;
	}

	getGenerator().Add(BoolToggleEditor({ { p_PropName, p_PropLabel, p_Flags, value }, p_CheckUncheckedLabelOverrides }));

	ASSERT(!hasProperty(p_PropName));
	m_ActionBoolProps[p_PropName] = p_Bool;
}

// =================================================================================================

void DlgBusinessMessageRuleEditHTML::addStringEditor(PredicateType p_PredicateType, boost::YOpt<PooledString> BusinessMessageRulePredicates::*p_String, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags)
{
	// "No change" flag is computed here. You should not set it.
	ASSERT(0 == (EditorFlags::NOCHANGE & p_Flags));

	wstring value;

	{
		bool allTheSame = true;
		const auto val = PredicateType::CONDITION == p_PredicateType ? m_BusinessMessageRules.front().m_Conditions.*p_String : m_BusinessMessageRules.front().m_Exceptions.*p_String;
		for (const auto& object : m_BusinessMessageRules)
		{
			if (val != (PredicateType::CONDITION == p_PredicateType ? object.m_Conditions.*p_String : object.m_Exceptions.*p_String))
			{
				allTheSame = false;
				break;
			}
		}

		if (allTheSame)
			value = val.is_initialized() ? (const wstring)val.get() : L"";
		else
			p_Flags = p_Flags | EditorFlags::NOCHANGE;
	}

	getGenerator().Add(StringEditor(p_PropName, p_PropLabel, p_Flags, value));

	ASSERT(!hasProperty(p_PropName));
	m_PredicateStringProps[p_PropName] = { p_PredicateType, p_String };
}

void DlgBusinessMessageRuleEditHTML::addComboEditor(PredicateType p_PredicateType, boost::YOpt<PooledString> BusinessMessageRulePredicates::*p_String, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues)
{
	// "No change" flag is computed here. You should not set it.
	ASSERT(0 == (EditorFlags::NOCHANGE & p_Flags));

	// "restriction" flags are computed via p_GetRestrictions. You should not set them.
	ASSERT(0 == (EditorFlags::RESTRICT_LOAD_MORE & p_Flags) && 0 == (EditorFlags::RESTRICT_NOT_CREATED & p_Flags) && 0 == (EditorFlags::RESTRICT_TYPE & p_Flags));

	wstring value;

	{
		bool allTheSame = true;
		const auto val = PredicateType::CONDITION == p_PredicateType ? m_BusinessMessageRules.front().m_Conditions.*p_String : m_BusinessMessageRules.front().m_Exceptions.*p_String;
		for (const auto& object : m_BusinessMessageRules)
		{
			if (val != (PredicateType::CONDITION == p_PredicateType ? object.m_Conditions.*p_String : object.m_Exceptions.*p_String))
			{
				allTheSame = false;
				break;
			}
		}

		if (allTheSame)
			value = val.is_initialized() ? (const wstring)val.get() : L"";
		else
			p_Flags = p_Flags | EditorFlags::NOCHANGE;
	}

	getGenerator().Add(ComboEditor({ p_PropName, p_PropLabel, p_Flags, value }, p_LabelsAndValues));

	ASSERT(!hasProperty(p_PropName));
	m_PredicateStringProps[p_PropName] = { p_PredicateType, p_String };
}

void DlgBusinessMessageRuleEditHTML::addBoolToggleEditor(PredicateType p_PredicateType, boost::YOpt<bool> BusinessMessageRulePredicates::*p_Bool, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const BoolEditor::CheckUncheckedLabelOverrides p_CheckUncheckedLabelOverrides /*= BoolEditor::CheckUncheckedLabelOverrides()*/)
{
	// "No change" flag is computed here. You should not set it.
	ASSERT(0 == (EditorFlags::NOCHANGE & p_Flags));

	bool value = false;

	{
		bool allTheSame = true;
		const auto val = PredicateType::CONDITION == p_PredicateType ? m_BusinessMessageRules.front().m_Conditions.*p_Bool : m_BusinessMessageRules.front().m_Exceptions.*p_Bool;
		for (const auto& object : m_BusinessMessageRules)
		{
			if (val != (PredicateType::CONDITION == p_PredicateType ? object.m_Conditions.*p_Bool : object.m_Exceptions.*p_Bool))
			{
				allTheSame = false;
				break;
			}
		}

		if (allTheSame)
			value = val.is_initialized() ? val.get() : false;
		else
			p_Flags = p_Flags | EditorFlags::NOCHANGE;
	}

	getGenerator().Add(BoolToggleEditor({ { p_PropName, p_PropLabel, p_Flags, value }, p_CheckUncheckedLabelOverrides }));

	ASSERT(!hasProperty(p_PropName));
	m_PredicateBoolProps[p_PropName] = { p_PredicateType, p_Bool };
}

void DlgBusinessMessageRuleEditHTML::addMultiValueEditor(PredicateType p_PredicateType, boost::YOpt<vector<PooledString>> BusinessMessageRulePredicates::*p_StringVector, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags)
{
	// "No change" flag is computed here. You should not set it.
	ASSERT(0 == (EditorFlags::NOCHANGE & p_Flags));

	vector<wstring> values;

	{
		bool allTheSame = true;
		const auto val = PredicateType::CONDITION == p_PredicateType ? m_BusinessMessageRules.front().m_Conditions.*p_StringVector : m_BusinessMessageRules.front().m_Exceptions.*p_StringVector;
		for (const auto& object : m_BusinessMessageRules)
		{
			if (val != (PredicateType::CONDITION == p_PredicateType ? object.m_Conditions.*p_StringVector : object.m_Exceptions.*p_StringVector))
			{
				allTheSame = false;
				break;
			}
		}

		if (allTheSame)
		{
			if (val.is_initialized())
				std::copy(val->begin(), val->end(), std::back_inserter(values));
			else
				values = {};
		}
		else
			p_Flags = p_Flags | EditorFlags::NOCHANGE;
	}

	DlgFormsJSONGenerator::InputPattern pattern;
	ASSERT(g_ListSeparator == _YTEXT(";")); // The pattern below corresponds to this separator
	pattern.m_PatternString = _YTEXT("[^;]+");
	pattern.m_PatternLabel = YtriaTranslate::Do(DlgBusinessMessageRuleEditHTML_addMultiValueEditor_1, _YLOC("Cannot contain the ; character.")).c_str();
	getGenerator().addMultiValueEditor(p_PropName, p_PropLabel, _YTEXT(""), pattern, p_Flags, values, g_ListSeparator);

	ASSERT(!hasProperty(p_PropName));
	m_PredicateStringVectorProps[p_PropName] = { p_PredicateType, p_StringVector };
}

bool DlgBusinessMessageRuleEditHTML::hasProperty(const wstring& p_PropertyName) const
{
	return m_RuleStringProps.end()				!= m_RuleStringProps.find(p_PropertyName)
		|| m_RuleBoolProps.end()				!= m_RuleBoolProps.find(p_PropertyName)
		|| m_ActionStringProps.end()			!= m_ActionStringProps.find(p_PropertyName)
		|| m_ActionBoolProps.end()				!= m_ActionBoolProps.find(p_PropertyName)
		|| m_PredicateStringProps.end()			!= m_PredicateStringProps.find(p_PropertyName)
		|| m_PredicateBoolProps.end()			!= m_PredicateBoolProps.find(p_PropertyName)
		|| m_PredicateStringVectorProps.end()	!= m_PredicateStringVectorProps.find(p_PropertyName);
}
