#pragma once

#include "BusinessObject.h"
#include "LicenseUnitsDetail.h"
#include "ServicePlanInfo.h"

class Sapio365Session;

class BusinessSubscribedSku : public BusinessObject
{
public:
	BusinessSubscribedSku() = default;

	const boost::YOpt<PooledString>& GetCapabilityStatus() const;
	void SetCapabilityStatus(const boost::YOpt<PooledString>& val);

	const boost::YOpt<int32_t>& GetConsumedUnits() const;
	void SetConsumedUnits(const boost::YOpt<int32_t>& val);

	const boost::YOpt<LicenseUnitsDetail>& GetPrepaidUnits() const;
	void SetPrepaidUnits(const boost::YOpt<LicenseUnitsDetail>& val);

	const vector<ServicePlanInfo>& GetServicePlans() const;
	void SetServicePlans(const vector<ServicePlanInfo>& val);

	const boost::YOpt<PooledString>& GetSkuId() const;
	void SetSkuId(const boost::YOpt<PooledString>& val);

	const boost::YOpt<PooledString>& GetSkuPartNumber() const;
	void SetSkuPartNumber(const boost::YOpt<PooledString>& val);

	const boost::YOpt<PooledString>& GetAppliesTo() const;
	void SetAppliesTo(const boost::YOpt<PooledString>& val);

protected:
	pplx::task<vector<HttpResultWithError>> SendEditRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;
	TaskWrapper<HttpResultWithError> SendCreateRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;
	TaskWrapper<HttpResultWithError> SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;

private:
	boost::YOpt<PooledString> m_CapabilityStatus;
	boost::YOpt<int32_t> m_ConsumedUnits;
	boost::YOpt<LicenseUnitsDetail> m_PrepaidUnits;
	vector<ServicePlanInfo> m_ServicePlans;
	boost::YOpt<PooledString> m_SkuId;
	boost::YOpt<PooledString> m_SkuPartNumber;
	boost::YOpt<PooledString> m_AppliesTo;

    RTTR_ENABLE(BusinessObject)
    RTTR_REGISTRATION_FRIEND
    friend class SubscribedSkuDeserializer;
};