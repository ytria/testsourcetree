#pragma once

#include "ModuleBase.h"

class FrameSpUsers;

class ModuleSpUsers : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

private:
    virtual void executeImpl(const Command& p_Command) override;
    void doRefresh(FrameSpUsers* p_pFrame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const Command& p_Command);
    void showSpUsers(const Command& p_Command);
};

