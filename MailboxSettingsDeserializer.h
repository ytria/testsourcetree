#pragma once

#include "Encapsulate.h"
#include "MailboxSettings.h"
#include "JsonObjectDeserializer.h"

class MailboxSettingsDeserializer : public JsonObjectDeserializer, public Encapsulate<MailboxSettings>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

