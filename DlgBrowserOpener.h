#pragma once

#include "IBrowserOpener.h"

class MainFrame;
class GridFrameBase;
class DlgBrowser;

class DlgBrowserOpener : public IBrowserOpener
{
public:
    DlgBrowserOpener();
    virtual int OpenBrowser(CWnd* p_Parent = nullptr) override;
	void CloseBrowser() override;

private:
	DlgBrowser* GetOauth2Browser();
	void SetOauth2Browser(DlgBrowser* p_DlgBrowser);
};

