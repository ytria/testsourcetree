#include "GroupListRequester.h"

#include "BusinessOrgContact.h"
#include "BusinessUser.h"
#include "GroupDeserializer.h"
#include "GroupDeserializerFactory.h"
#include "IPropertySetBuilder.h"
#include "MSGraphUtil.h"
#include "MSGraphSession.h"
#include "PaginatedRequestResults.h"
#include "ValueListDeserializer.h"
#include "MSGraphCommonData.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "RbacRequiredPropertySet.h"
#include "MsGraphHttpRequestLogger.h"
#include "MsGraphDeltaPaginator.h"

GroupListRequester::GroupListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger, std::unique_ptr<GroupListSubRequester> p_SpecificRequester)
	: m_Logger(p_Logger)
	, m_SubRequester(std::move(p_SpecificRequester))
{
}

TaskWrapper<void> GroupListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	ASSERT(nullptr != m_SubRequester);
	if (nullptr != m_SubRequester)
	{
		auto factory = std::make_unique<GroupDeserializerFactory>(p_Session);
		m_Deserializer = std::make_shared<ValueListDeserializer<BusinessGroup, GroupDeserializer>>(std::move(factory));
		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());

		m_SubRequester->SetParentRequester(this);
		m_SubRequester->SetLogger(httpLogger);

		return m_SubRequester->Send(p_Session, p_TaskData);
	}

	return TaskWrapper<void>();

}

vector<BusinessGroup>& GroupListRequester::GetData()
{
    return m_Deserializer->GetData();
}

void GroupListRequester::SetCustomFilter(const ODataFilter& p_CustomFilter)
{
	m_CustomFilter = p_CustomFilter;
}

GroupListRequester::GroupListSubRequester::GroupListSubRequester()
	: m_Requester(nullptr)
{
}

void GroupListRequester::GroupListSubRequester::SetParentRequester(GroupListRequester* p_ParentRequester)
{
	m_Requester = p_ParentRequester;
}

void GroupListRequester::GroupListSubRequester::SetProperties(const vector<rttr::property>& p_Properties)
{
	m_Properties = p_Properties;
}

void GroupListRequester::GroupListSubRequester::SetLogger(const std::shared_ptr<MsGraphHttpRequestLogger>& p_Logger)
{
	m_Logger = p_Logger;
}

TaskWrapper<void> GroupListRequester::RegularRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	uri_builder uri(_YTEXT("groups"));
	uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(m_Properties));

	m_Requester->m_CustomFilter.ApplyTo(uri);

	return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Requester->m_Deserializer, m_Requester->m_Logger, m_Logger, p_TaskData.GetOriginator())
		.Paginate(p_Session->GetMSGraphSession(m_Logger->GetSessionType()), p_TaskData);
}

GroupListRequester::InitialDeltaRequester::InitialDeltaRequester()
	: m_NextDeltaLink(std::make_shared<std::wstring>())
{
}

TaskWrapper<void> GroupListRequester::InitialDeltaRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	web::uri_builder uri(_YTEXT("groups/delta"));
	uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(m_Properties));

	// Be careful, only works on 'id' !!
	m_Requester->m_CustomFilter.ApplyTo(uri);

	auto paginator = MsGraphDeltaPaginator(uri.to_uri(),
		Util::CreateDefaultGraphPageRequester(m_Requester->m_Deserializer, m_Logger, p_TaskData.GetOriginator()),
		m_Requester->m_Logger);
	paginator.SetDeltaLinkPtr(m_NextDeltaLink);

	return paginator.Paginate(p_Session->GetMSGraphSession(m_Logger->GetSessionType()), p_TaskData);
}

wstring GroupListRequester::InitialDeltaRequester::GetNextDeltalink() const
{
	return m_NextDeltaLink ? *m_NextDeltaLink : wstring();
}

GroupListRequester::DeltaLinkRequester::DeltaLinkRequester(const wstring& p_DeltaLink)
	: m_NextDeltaLink(std::make_shared<std::wstring>())
	, m_DeltaLink(p_DeltaLink)
{
}

TaskWrapper<void> GroupListRequester::DeltaLinkRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	web::uri_builder uri(m_DeltaLink);

	auto paginator = MsGraphDeltaPaginator(uri.to_uri(),
		Util::CreateDefaultGraphPageRequester(m_Requester->m_Deserializer, m_Logger, p_TaskData.GetOriginator()),
		m_Requester->m_Logger);
	paginator.SetDeltaLinkPtr(m_NextDeltaLink);

	return paginator.Paginate(p_Session->GetMSGraphSession(m_Logger->GetSessionType()), p_TaskData);
}

wstring GroupListRequester::DeltaLinkRequester::GetNextDeltalink() const
{
	return m_NextDeltaLink ? *m_NextDeltaLink : wstring();
}
