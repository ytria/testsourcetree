#include "GridSpGroups.h"
#include "AutomationWizardSpGroups.h"
#include "BaseO365Grid.h"
#include "BasicGridSetup.h"
#include "FrameSpGroups.h"
#include "GridUtil.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"

BEGIN_MESSAGE_MAP(GridSpGroups, ModuleO365Grid<Sp::Group>)
END_MESSAGE_MAP()

GridSpGroups::GridSpGroups()
    : m_Frame(nullptr)
{
    // FIXME: Add desired actions when the required methods are implemented below!
    UseDefaultActions(/*O365Grid::ACTION_CREATE | *//*O365Grid::ACTION_DELETE *//*| O365Grid::ACTION_EDIT | O365Grid::ACTION_VIEW*/0);
    EnableGridModifications();

    initWizard<AutomationWizardSpGroups>(_YTEXT("Automation\\SpGroup"));
}

GridSpGroups::~GridSpGroups()
{
    GetBackend().Clear();
}

ModuleCriteria GridSpGroups::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_Frame->GetModuleCriteria();

    criteria.m_IDs.clear();
    for (auto row : p_Rows)
        criteria.m_IDs.insert(row->GetField(m_ColMetaId).GetValueStr());

    return criteria;
}

void GridSpGroups::BuildView(const O365DataMap<BusinessSite, vector<Sp::Group>>& p_SpGroups, bool p_FullPurge)
{
    GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
        | (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
        | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
        | GridUpdaterOptions::REFRESH_PROCESS_DATA
        );
    GridUpdater updater(*this, options);

    GridIgnoreModification ignoramus(*this);
    for (const auto& keyVal : p_SpGroups)
    {
        const auto& site = keyVal.first;
        const auto& SpGroups = keyVal.second;

        GridBackendField fID(site.GetID(), m_ColMetaId);
        GridBackendRow* siteRow = m_RowIndex.GetRow({ fID }, true, true);
        ASSERT(nullptr != siteRow);
        if (nullptr != siteRow)
        {
            updater.GetOptions().AddRowWithRefreshedValues(siteRow);
            updater.GetOptions().AddPartialPurgeParentRow(siteRow);

            siteRow->SetHierarchyParentToHide(true);
            siteRow->AddField(site.GetDisplayName(), m_ColMetaDisplayName);

            SetRowObjectType(siteRow, site);
            if (!site.HasFlag(BusinessObject::CANCELED))
            {
                if (keyVal.second.size() == 1 && keyVal.second[0].GetError())
                {
                    updater.OnLoadingError(siteRow, *keyVal.second[0].GetError());
                }
                else
                {
					// Clear previous error
					if (siteRow->IsError())
						siteRow->ClearStatus();

                    for (const auto& spGroup : SpGroups)
                    {
                        GridBackendField fieldGroupId(spGroup.Id ? std::to_wstring(*spGroup.Id) : _YTEXT(""), m_ColGroupId);
                        GridBackendField fieldSiteId(site.GetID(), m_ColMetaId);

                        GridBackendRow* userRow = m_RowIndex.GetRow({ fieldGroupId, fieldSiteId }, true, true);
                        ASSERT(nullptr != userRow);
                        if (nullptr != userRow)
                        {
                            updater.GetOptions().AddRowWithRefreshedValues(userRow);
                            userRow->SetParentRow(siteRow);

                            SetRowObjectType(userRow, spGroup, spGroup.HasFlag(BusinessObject::CANCELED));
                            if (spGroup.GetError())
                            {
                                updater.OnLoadingError(userRow, *spGroup.GetError());
                            }
                            else
                            {
								// Clear previous error
								if (userRow->IsError())
									userRow->ClearStatus();

                                siteRow->AddField(site.GetDisplayName(), m_ColMetaDisplayName);
                                FillSiteFields(site, spGroup, userRow, updater);
                            }
                        }
                    }
                }
            }
        }
    }
}

void GridSpGroups::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameSpGroups, LicenseUtil::g_codeSapio365spGroups,
		{
			{ &m_ColMetaDisplayName, YtriaTranslate::Do(GridLists_customizeGrid_1, _YLOC("Site Display Name")).c_str(), g_FamilyOwner },
			{ &m_ColMetaUserDisplayName, g_TitleOwnerUserName, g_FamilyOwner },
			{ &m_ColMetaId, g_TitleOwnerID,	g_FamilyOwner }
		}, { Sp::Group() });
		setup.Setup(*this, false);
	}

    setBasicGridSetupHierarchy({ { { m_ColMetaDisplayName, 0 } }
								,{ rttr::type::get<Sp::Group>().get_id() }
								,{ rttr::type::get<Sp::Group>().get_id(), rttr::type::get<BusinessSite>().get_id() } });

    addColumnGraphID(); // FIXME: Shouldn't this replace m_ColGroupId?

    m_ColLoginName						= AddColumn(_YUID("loginName"), YtriaTranslate::Do(GridSpGroups_customizeGrid_2, _YLOC("Login Name")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColTitle							= AddColumn(_YUID("title"), YtriaTranslate::Do(GridSpGroups_customizeGrid_3, _YLOC("Title")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColPrincipalType					= AddColumn(_YUID("principalType"), YtriaTranslate::Do(GridSpGroups_customizeGrid_4, _YLOC("Principal Type")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColDescription					= AddColumn(_YUID("description"), YtriaTranslate::Do(GridSpGroups_customizeGrid_5, _YLOC("Description")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColAllowMembersEditMembership		= AddColumnCheckBox(_YUID("allowMembersEditMembership"), YtriaTranslate::Do(GridSpGroups_customizeGrid_6, _YLOC("Allow Members Edit Membership")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColAllowRequestToJoinLeave		= AddColumnCheckBox(_YUID("allowRequestToJoinLeave"), YtriaTranslate::Do(GridSpGroups_customizeGrid_7, _YLOC("Allow Request To Join Leave")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColAutoAcceptRequestToJoinLeave	= AddColumnCheckBox(_YUID("autoAcceptRequestToJoinLeave"), YtriaTranslate::Do(GridSpGroups_customizeGrid_8, _YLOC("Auto Accept Request To Join Leave")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColCanCurrentUserEditMembership	= AddColumnCheckBox(_YUID("canCurrentUserEditMembership"), YtriaTranslate::Do(GridSpGroups_customizeGrid_9, _YLOC("Can Current User Edit Membership")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColCanCurrentUserManageGroup		= AddColumnCheckBox(_YUID("canCurrentUserManageGroup"), YtriaTranslate::Do(GridSpGroups_customizeGrid_10, _YLOC("Can Current User Manage Group")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColCanCurrentUserViewMembership	= AddColumnCheckBox(_YUID("canCurrentUserViewMembership"), YtriaTranslate::Do(GridSpGroups_customizeGrid_11, _YLOC("Can Current User View Membership")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColOnlyAllowMembersViewMembership = AddColumnCheckBox(_YUID("OnlyAllowMembersViewMembership"), YtriaTranslate::Do(GridSpGroups_customizeGrid_12, _YLOC("Only Allow Members View Membership")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColOwnerTitle						= AddColumn(_YUID("ownerTitle"), YtriaTranslate::Do(GridSpGroups_customizeGrid_13, _YLOC("Owner Title")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColRequestToJoinLeaveEmailSetting = AddColumn(_YUID("requestToJoinLeaveEmailSetting"), YtriaTranslate::Do(GridSpGroups_customizeGrid_14, _YLOC("Request To Join Leave Email Setting")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
    m_ColIsHiddenInUI					= AddColumnCheckBox(_YUID("isHiddenInUI"), YtriaTranslate::Do(GridSpGroups_customizeGrid_15, _YLOC("Is Hidden In UI")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });
	// FIXME: Shouldn't we use O365Grid::m_ColId instead?
    m_ColGroupId						= AddColumn(_YUID("id"), YtriaTranslate::Do(GridSpGroups_customizeGrid_16, _YLOC("ID")).c_str(), _YTEXT(""), g_ColumnSize12, { g_ColumnsPresetDefault });

    AddColumnForRowPK(m_ColMetaId); // Site id
	// FIXME: Shouldn't we use O365Grid::m_ColId instead?
    AddColumnForRowPK(m_ColGroupId); // Group id

    m_Frame = dynamic_cast<FrameSpGroups*>(GetParentFrame());

    if (nullptr != GetAutomationWizard())
        GetAutomationWizard()->SetFrame(m_Frame);
}

wstring GridSpGroups::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Site"), m_ColMetaDisplayName } }, m_ColTitle);
}

Sp::Group GridSpGroups::getBusinessObject(GridBackendRow* p_Row) const
{
    ASSERT(GridUtil::isBusinessType<Sp::Group>(p_Row));
    return Sp::Group();
}

void GridSpGroups::UpdateBusinessObjects(const vector<Sp::Group>& p_Users, bool p_SetModifiedStatus)
{
    ASSERT(false);
}

void GridSpGroups::RemoveBusinessObjects(const vector<Sp::Group>& p_Users)
{
    ASSERT(false);
}

void GridSpGroups::InitializeCommands()
{
    ModuleO365Grid<Sp::Group>::InitializeCommands();

    const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);
}

void GridSpGroups::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {

    }

    ModuleO365Grid<Sp::Group>::OnCustomPopupMenu(pPopup, nStart);
}

void GridSpGroups::FillSiteFields(const BusinessSite& p_Site, const Sp::Group& p_SpGroup, GridBackendRow* p_Row, GridUpdater& p_Updater)
{
    p_Row->AddField(p_SpGroup.AllowMembersEditMembership, m_ColAllowMembersEditMembership);
    p_Row->AddField(p_SpGroup.AllowRequestToJoinLeave, m_ColAllowRequestToJoinLeave);
    p_Row->AddField(p_SpGroup.AutoAcceptRequestToJoinLeave, m_ColAutoAcceptRequestToJoinLeave);
    p_Row->AddField(p_SpGroup.CanCurrentUserEditMembership, m_ColCanCurrentUserEditMembership);
    p_Row->AddField(p_SpGroup.CanCurrentUserManageGroup, m_ColCanCurrentUserManageGroup);
    p_Row->AddField(p_SpGroup.CanCurrentUserViewMembership, m_ColCanCurrentUserViewMembership);
    p_Row->AddField(p_SpGroup.Description, m_ColDescription);
    p_Row->AddField(p_SpGroup.Id, m_ColGroupId);
    p_Row->AddField(p_SpGroup.IsHiddenInUI, m_ColIsHiddenInUI);
    p_Row->AddField(p_SpGroup.LoginName, m_ColLoginName);
    p_Row->AddField(p_SpGroup.OnlyAllowMembersViewMembership, m_ColOnlyAllowMembersViewMembership);
    p_Row->AddField(p_SpGroup.OwnerTitle, m_ColOwnerTitle);
    p_Row->AddField(p_SpGroup.RequestToJoinLeaveEmailSetting, m_ColRequestToJoinLeaveEmailSetting);
    p_Row->AddField(p_SpGroup.PrincipalType, m_ColPrincipalType);
    p_Row->AddField(p_SpGroup.Title, m_ColTitle);
}
