#pragma once

class IMsGraphPageRequester;
class IPageRequestLogger;

class MsGraphDeltaPaginator
{
public:
	MsGraphDeltaPaginator(const web::uri& p_Uri, const shared_ptr<IMsGraphPageRequester>& p_Requester, const std::shared_ptr<IPageRequestLogger>& p_Logger);

	TaskWrapper<void> Paginate(const shared_ptr<MSGraphSession>& p_Session, YtriaTaskData p_TaskData);
	void SetDeltaLinkPtr(const std::shared_ptr<wstring>& p_DeltaLink);
	
private:
	void ValidateObj();

	web::uri m_OriginalUri;
	unsigned m_Temporisator;

	shared_ptr<wstring> m_DeltaLinkPtr;
	shared_ptr<IMsGraphPageRequester> m_Requester;
	shared_ptr<IPageRequestLogger> m_RequestLogger;

	static bool HasNextPage(const web::json::value& p_Json);
	static void Minimize(web::uri_builder& p_Uri);

	static const wstring g_NextLinkKey;
	static const wstring g_DeltaLinkKey;
};

