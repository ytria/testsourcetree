#pragma once

class DateFieldCutOffCondition
{
public:
	DateFieldCutOffCondition(const wstring& p_FieldName, const wstring& p_DateCutOffIso8601);
	bool operator()(const web::json::value& p_Json) const;

private:
	wstring m_FieldName;
	wstring m_DateCutOffIso8601;
};

