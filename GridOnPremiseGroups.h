#pragma once

#include "BaseO365Grid.h"
#include "OnPremiseGroup.h"
#include "GridTemplateGroups.h"

class FrameOnPremiseGroups;

class GridOnPremiseGroups : public ModuleO365Grid<OnPremiseGroup>
{
public:
	GridOnPremiseGroups();

	void customizeGrid() override;

	void BuildView(const vector<OnPremiseGroup>& p_Groups, bool p_FullPurge);

protected:
	OnPremiseGroup getBusinessObject(GridBackendRow*) const;
	void UpdateBusinessObjects(const vector<OnPremiseGroup>& p_Permissions, bool p_SetModifiedStatus);
	void RemoveBusinessObjects(const vector<OnPremiseGroup>& p_Permissions);
	wstring GetName(const GridBackendRow* p_Row) const override;

private:
	GridBackendRow* FillRow(GridBackendRow* p_Row, const OnPremiseGroup& p_Permissions);
	void AddDateOrNeverField(const boost::YOpt<YTimeDate>& p_Date, GridBackendRow& p_Row, GridBackendColumn& p_Col);

	bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

private:
	FrameOnPremiseGroups* m_Frame;

	GridBackendColumn* m_ColLastRequestDateTime;

	GridBackendColumn* m_ColAdminCount;
	GridBackendColumn* m_ColCanonicalName;
	GridBackendColumn* m_ColCN;
	GridBackendColumn* m_ColCreated;
	GridBackendColumn* m_ColCreateTimeStamp;
	GridBackendColumn* m_ColDeleted;
	GridBackendColumn* m_ColDescription;
	GridBackendColumn* m_ColDisplayName;
	GridBackendColumn* m_ColDistinguishedName;
	GridBackendColumn* m_ColDSCorePropagationData;
	GridBackendColumn* m_ColGroupCategory;
	GridBackendColumn* m_ColGroupScope;
	GridBackendColumn* m_ColGroupType;
	GridBackendColumn* m_ColHomePage;
	GridBackendColumn* m_ColInstanceType;
	GridBackendColumn* m_ColIsCriticalSystemObject;
	GridBackendColumn* m_ColIsDeleted;
	GridBackendColumn* m_ColLastKnownParent;
	GridBackendColumn* m_ColManagedBy;
	GridBackendColumn* m_ColMember;
	GridBackendColumn* m_ColMemberOf;
	GridBackendColumn* m_ColMembers;
	GridBackendColumn* m_ColModified;
	GridBackendColumn* m_ColModifyTimeStamp;
	GridBackendColumn* m_ColName;
	GridBackendColumn* m_ColNTSecurityDescriptor;
	GridBackendColumn* m_ColObjectCategory;
	GridBackendColumn* m_ColObjectClass;
	GridBackendColumn* m_ColObjectGUID;
	GridBackendColumn* m_ColObjectSid;
	GridBackendColumn* m_ColProtectedFromAccidentalDeletion;
	GridBackendColumn* m_ColSamAccountName;
	GridBackendColumn* m_ColSAMAccountType;
	GridBackendColumn* m_ColSDRightsEffective;
	GridBackendColumn* m_ColSID;
	GridBackendColumn* m_ColSIDHistory;
	GridBackendColumn* m_ColSystemFlags;
	GridBackendColumn* m_ColUSNChanged;
	GridBackendColumn* m_ColUSNCreated;
	GridBackendColumn* m_ColWhenChanged;
	GridBackendColumn* m_ColWhenCreated;
};

