#include "QueryUpdateSessionType.h"
#include "SqlQueryPreparedStatement.h"

QueryUpdateSessionType::QueryUpdateSessionType(SessionsSqlEngine& p_Engine, const SessionIdentifier& p_Identifier, const wstring& p_NewSessionType)
	:m_Identifier(p_Identifier)
	, m_Engine(p_Engine)
	, m_NewSessionType(p_NewSessionType)
{
}

void QueryUpdateSessionType::Run()
{
	SqlQueryPreparedStatement query(m_Engine, _YTEXT("UPDATE Sessions SET SessionType=? WHERE EmailOrAppId=? AND SessionType=? AND RoleId=?"));
	query.BindString(1, m_NewSessionType);
	query.BindString(2, m_Identifier.m_EmailOrAppId);
	query.BindString(3, m_Identifier.m_SessionType);
	query.BindInt64(4, m_Identifier.m_RoleID);
	query.Run();
	int changes = sqlite3_changes(m_Engine.GetDbHandle());
	ASSERT(changes >= 1);
	m_Status = query.GetStatus();
}
