#include "RequestsDlgUtil.h"

#include "BusinessDirectoryRole.h"
#include "Str.h"
#include "MsGraphFieldNames.h"
#include "Sapio365Session.h"
#include "DirectoryRolesRequester.h"
#include "YCallbackMessage.h"
#include "YCodeJockMessageBox.h"
#include "BasicRequestLogger.h"
#include "MsGraphHttpRequestLogger.h"

pair<wstring, wstring> Util::CreateGlobalAdmin(const wstring& p_TenantName, std::shared_ptr<Sapio365Session> p_Session)
{
	pair<wstring, wstring> usernameAndPasswd;

	User userToCreate;
	userToCreate.AccountEnabled = true;
	userToCreate.DisplayName = wstring(YtriaTranslate::Do(Util_CreateGlobalAdmin_3, _YLOC("sapio365 RBAC Service Account")).c_str());
	userToCreate.MailNickname = wstring(_YTEXT("sapio365_rbac_")) + Str::GeneratePassword(8, true);
	userToCreate.PasswordProfile.emplace();
	userToCreate.PasswordProfile->Password = Str::GeneratePassword(20, false);
	userToCreate.PasswordProfile->ForceChangePasswordNextSignIn = false;
	userToCreate.UserPrincipalName = _YTEXTFORMAT(L"%s@%s", userToCreate.MailNickname->c_str(), p_TenantName.c_str());

	usernameAndPasswd.first = *userToCreate.UserPrincipalName;
	usernameAndPasswd.second = *userToCreate.PasswordProfile->Password;

	vector<rttr::property> properties;
	properties.push_back(rttr::type::get<User>().get_property(O365_USER_ACCOUNTENABLED));
	properties.push_back(rttr::type::get<User>().get_property(O365_USER_DISPLAYNAME));
	properties.push_back(rttr::type::get<User>().get_property(O365_USER_MAILNICKNAME));
	properties.push_back(rttr::type::get<User>().get_property(O365_USER_PASSWORDPROFILE));
	properties.push_back(rttr::type::get<User>().get_property(O365_USER_USERPRINCIPALNAME));

	try
	{
		CWaitCursor waitCur;
		auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
		auto createRes = userToCreate.Create(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, YtriaTaskData(), properties).GetTask().get();
		auto strBody = createRes.GetBodyAsString();
		ASSERT(boost::none != strBody);
		wstring userId;
		if (boost::none != strBody)
		{
			auto userJson = web::json::value::parse(*strBody);
			ASSERT(userJson.has_string_field(_YTEXT("id")));
			if (userJson.has_string_field(_YTEXT("id")))
				userId = userJson[_YTEXT("id")].as_string();
		}

		DirectoryRolesRequester requester(std::make_shared<BasicRequestLogger>(_T("directory roles")));
		requester.Send(p_Session, YtriaTaskData()).GetTask().wait();

		const wstring g_CompanyAdminRoleId = _YTEXT("62e90394-69f5-4237-9190-012177145e10");
		wstring companyAdminRoleId;
		for (const auto& dirRole : requester.GetData())
		{
			if (dirRole.GetRoleTemplateId() != boost::none &&
				*dirRole.GetRoleTemplateId() == g_CompanyAdminRoleId)
			{
				companyAdminRoleId = dirRole.GetID();
				break;
			}
		}

		ASSERT(!companyAdminRoleId.empty());
		if (!companyAdminRoleId.empty())
		{
			auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
			p_Session->GetMSGraphSession(httpLogger->GetSessionType())->AddDirectoryRoleMember(userId, companyAdminRoleId, httpLogger, YtriaTaskData()).GetTask().wait();
		}
	}
	catch (const RestException& e)
	{
		YCallbackMessage::DoSend([e]()
		{
			YCodeJockMessageBox dlg(nullptr,
				DlgMessageBox::eIcon_Error,
				YtriaTranslate::DoError(Util_CreateGlobalAdmin_1, _YLOC("Can't create new Global Admin User"),_YR("Y2433")).c_str(),
				e.WhatUnicode(),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_1, _YLOC("OK")).c_str() } });
			dlg.DoModal();
		});

		usernameAndPasswd = {};
	}
	catch (const std::exception& e)
	{
		YCallbackMessage::DoSend([e]()
			{
				YCodeJockMessageBox dlg(nullptr,
					DlgMessageBox::eIcon_Error,
					YtriaTranslate::DoError(Util_CreateGlobalAdmin_1, _YLOC("Can't create new Global Admin User"),_YR("Y2434")).c_str(),
					MFCUtil::convertUTF8_to_UNICODE(e.what()),
					_YTEXT(""),
					{ { IDOK, YtriaTranslate::Do(CosmosUtil_CreateCosmosDbAccount_1, _YLOC("OK")).c_str() } });
				dlg.DoModal();
			});

		usernameAndPasswd = {};
	}

	return usernameAndPasswd;
}
