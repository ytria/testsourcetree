#include "BusinessAADUserConversationMember.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessAADUserConversationMember>("AADUserConversationMember") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Member"))))
	.constructor()(policy::ctor::as_object);
}

const boost::YOpt<PooledString>& BusinessAADUserConversationMember::GetDisplayName() const
{
	return m_DisplayName;
}

void BusinessAADUserConversationMember::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}

const boost::YOpt<PooledString>& BusinessAADUserConversationMember::GetUserID() const
{
	return m_UserID;
}

void BusinessAADUserConversationMember::SetUserID(const boost::YOpt<PooledString>& p_Val)
{
	m_UserID = p_Val;
}

const boost::YOpt<PooledString>& BusinessAADUserConversationMember::GetEmail() const
{
	return m_Email;
}

void BusinessAADUserConversationMember::SetEmail(const boost::YOpt<PooledString>& p_Val)
{
	m_Email = p_Val;
}

const boost::YOpt<vector<PooledString>>& BusinessAADUserConversationMember::GetRoles() const
{
	return m_Roles;
}

void BusinessAADUserConversationMember::SetRoles(const boost::YOpt<vector<PooledString>>& p_Val)
{
	m_Roles = p_Val;
}

