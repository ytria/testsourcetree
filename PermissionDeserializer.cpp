#include "PermissionDeserializer.h"

#include "IdentitySetDeserializer.h"
#include "ItemReferenceDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "SharingInvitationDeserializer.h"
#include "SharingLinkDeserializer.h"

void PermissionDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.Id, p_Object, true);

    {
        IdentitySetDeserializer isd;
        if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("grantedTo"), p_Object))
            m_Data.GrantedTo = isd.GetData();
    }

    {
        SharingInvitationDeserializer sid;
        if (JsonSerializeUtil::DeserializeAny(sid, _YTEXT("invitation"), p_Object))
            m_Data.Invitation = sid.GetData();
    }

    {
        ItemReferenceDeserializer ird;
        if (JsonSerializeUtil::DeserializeAny(ird, _YTEXT("inheritedFrom"), p_Object))
            m_Data.InheritedFrom = ird.GetData();
    }

    {
        SharingLinkDeserializer sld;
        if (JsonSerializeUtil::DeserializeAny(sld, _YTEXT("link"), p_Object))
            m_Data.Link = sld.GetData();
    }

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT("roles"), p_Object))
            m_Data.Role = lsd.GetData();
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("shareId"), m_Data.ShareId, p_Object);
}
