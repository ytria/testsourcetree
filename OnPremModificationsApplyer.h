#pragma once

#include "IModificationApplyer.h"

#include "GridFrameBase.h"
#include "GridGroups.h"
#include "GridUsers.h"

template<class GridClass>
class OnPremModificationsApplyer : public IModificationApplyer
{
public:
	OnPremModificationsApplyer(GridClass& p_Grid);

	void ApplyAll() override;
	void ApplySelected() override;

private:
	GridClass& m_Grid;
};

template<class GridClass>
struct FrameType
{
	using Value = GridFrameBase;
};

template<>
struct FrameType<GridUsers> {
	using Value = FrameUsers;
};

template<>
struct FrameType<GridGroups> {
	using Value = FrameGroups;
};


template<class GridClass>
OnPremModificationsApplyer<GridClass>::OnPremModificationsApplyer(GridClass& p_Grid)
	: m_Grid(p_Grid)
{

}

template<class GridClass>
void OnPremModificationsApplyer<GridClass>::ApplyAll()
{
	const bool regularMods = m_Grid.HasModificationsPending(false);
	const bool onPremMods = m_Grid.HasOnPremiseChanges(false);
	ASSERT(regularMods || onPremMods);
	auto frame = dynamic_cast<typename FrameType<GridClass>::Value*>(m_Grid.GetParentGridFrameBase());

	enum : INT_PTR
	{
		CANCEL = IDCANCEL,
		APPLYREGULAR = IDOK,
		APPLYONPREM = IDYES,
	};
	const auto doApply = [=] {
		if (frame->m_DisplayApplyConfirm || regularMods && onPremMods)
		{
			//auto applyConfirmationConfig = p_Frame.GetApplyConfirmationConfig(true);
			YCodeJockMessageBox confirmation(frame,
				DlgMessageBox::eIcon_Question,
				_T("Save All Changes"),
				_T("Are you sure?"),
				_YTEXT(""),
				regularMods && onPremMods
				? std::unordered_map<UINT, wstring>{ { APPLYREGULAR, _T("Save O365") }, { APPLYONPREM, _T("Save On-Premises") }, { CANCEL, _T("Cancel") } }
				: std::unordered_map<UINT, wstring>{ { regularMods ? APPLYREGULAR : APPLYONPREM, _T("Yes") }, { CANCEL, _T("No") } });
			return confirmation.DoModal();
		}
		return regularMods ? (INT_PTR)APPLYREGULAR : (INT_PTR)APPLYONPREM;
	}();

	if (APPLYREGULAR == doApply)
	{
		m_Grid.StoreSelection();
		frame->SetSyncNow(frame->HasDeltaFeature());
		frame->ApplyAllSpecific();
		m_Grid.RestoreSelection();
	}
	else if (APPLYONPREM == doApply)
	{
		frame->applyOnPremSpecific(false);
	}
}

template<class GridClass>
void OnPremModificationsApplyer<GridClass>::ApplySelected()
{
	const bool regularMods = m_Grid.HasModificationsPending(true);
	const bool onPremMods = m_Grid.HasOnPremiseChanges(true);
	ASSERT(regularMods || onPremMods);
	auto frame = dynamic_cast<typename FrameType<GridClass>::Value*>(m_Grid.GetParentGridFrameBase());

	enum : INT_PTR
	{
		CANCEL = IDCANCEL,
		APPLYREGULAR = IDOK,
		APPLYONPREM = IDYES,
	};
	const auto doApply = [=] {
		if (frame->m_DisplayApplyConfirm || regularMods && onPremMods)
		{
			//auto applyConfirmationConfig = p_Frame.GetApplyConfirmationConfig(true);
			YCodeJockMessageBox confirmation(frame,
				DlgMessageBox::eIcon_Question,
				_T("Save Selected Changes"),
				regularMods && onPremMods
				? _T("Are you sure?")
				: _T("Are you sure?"),
				_YTEXT(""),
				regularMods && onPremMods
				? std::unordered_map<UINT, wstring>{ { APPLYREGULAR, _T("Save O365") }, { APPLYONPREM, _T("Save AD DS") }, { CANCEL, _T("Cancel") } }
			: std::unordered_map<UINT, wstring>{ { regularMods ? APPLYREGULAR : APPLYONPREM, _T("Yes") }, { CANCEL, _T("No") } });
			return confirmation.DoModal();
		}
		return regularMods ? (INT_PTR)APPLYREGULAR : (INT_PTR)APPLYONPREM;
	}();

	if (APPLYREGULAR == doApply)
	{
		frame->SetSyncNow(frame->HasDeltaFeature());
		frame->m_DisplayApplyConfirm = false;
		frame->ApplySelectedImpl(true);
	}
	else if (APPLYONPREM == doApply)
	{
		frame->applyOnPremSpecific(true);
	}
}

