#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerLicenses : public IBrowserLinkHandler
{
public:
    virtual void Handle(YBrowserLink& p_Link) const override;
};

// =================================================================================================

class LinkHandlerMyLicenses : public IBrowserLinkHandler
{
public:
	virtual void Handle(YBrowserLink& p_Link) const override;
	virtual bool IsAllowedWithAJLLicense() const override;
};