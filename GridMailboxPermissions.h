#pragma once

#include "BaseO365Grid.h"
#include "MailboxPermissions.h"
#include "GridTemplateUsers.h"
#include "IButtonUpdater.h"
#include "MailboxPermissionsAddModification.h"
#include "MailboxPermissionsEditModification.h"
#include "MailboxPermissionsRemoveModification.h"
#include "MailboxPermissionsModificationsReverter.h"
#include "MailboxPermissionsModificationsApplyer.h"
#include "IModificationWithRequestFeedbackProvider.h"

class FrameMailboxPermissions;

class MailboxPermissionsGridRefresher;

class GridMailboxPermissions : public ModuleO365Grid<MailboxPermissions>
{
public:
	GridMailboxPermissions();

	void customizeGrid() override;

	void BuildTreeView(const O365DataMap<BusinessUser, vector<MailboxPermissions>>& p_Permissions, bool p_FullPurge);
	ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;
	wstring GetName(const GridBackendRow* p_Row) const override;

	RoleDelegationUtil::RBAC_Privilege GetPrivilegeEdit() const;

	void SetAccessRights(GridBackendRow& p_Row, const vector<wstring>& p_AccessRights);
	vector<wstring> GetAccessRights(const GridBackendRow& p_Row);

	GridBackendColumn* GetColAccessRights();
	GridBackendColumn* GetColOwnerId();
	GridBackendColumn* GetColOwnerDisplayName();
	GridBackendColumn* GetColPermissionsUser();
	GridBackendColumn* GetColInheritanceType();
	const GridTemplateUsers& GetTemplateUsers() const;

	bool HasModificationUnder(GridBackendRow& p_Row);
	bool HasModificationsPending(bool inSelectionOnly) override;
	bool HasModificationsPendingInParentOfSelected() override;

	void SetUpdatedEditions(vector<MailboxPermissionsEditModification>&& p_Editions);
	void SetUpdatedAdditions(vector<MailboxPermissionsAddModification>&& p_Additions);
	void SetUpdatedRemovals(vector<MailboxPermissionsRemoveModification>&& p_Removals);

	wstring HandleUpdatedEditions();
	wstring HandleUpdatedAdditions();
	wstring HandleUpdatedRemovals();

	void SelectModificationsInSelectionParents();

	vector<GridBackendField> CreatePk(const wstring& p_UserId, const wstring& p_InheritanceType, const wstring& p_OwnerId);
	bool AreSimilarRows(const row_pk_t& p_Pk1, const row_pk_t& p_Pk2) const;

	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

protected:
	MailboxPermissions getBusinessObject(GridBackendRow*) const;
	void UpdateBusinessObjects(const vector<MailboxPermissions>& p_Permissions, bool p_SetModifiedStatus);
	void RemoveBusinessObjects(const vector<MailboxPermissions>& p_Permissions);

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

private:
	bool hasSelectedMod();
	bool hasSelectedPermission();

	void createGridEditModifications(const vector<vector<GridBackendField>>& p_Pks, const map<wstring, bool>& p_Changes);
	bool confirmRemove();
	void AddModification(MailboxPermissionsEditModification p_Mod);
	void AddModification(const MailboxPermissionsAddModification& p_Mod);

	GridBackendRow* FillRow(const MailboxPermissions& p_Permissions, GridBackendRow* p_ParentUserRow);
	void UpdateRow(const MailboxPermissions& p_Permissions, GridBackendRow* p_Row);
	void InitializeCommands() override;
	void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart);

	bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

	void ShowSystemRows(bool p_Show);

	afx_msg void OnEditMailboxPermissions();
	afx_msg void OnUpdateEditMailboxPermissions(CCmdUI* pCmdUI);
	afx_msg void OnAddMailboxPermissions();
	afx_msg void OnUpdateAddMailboxPermissions(CCmdUI* pCmdUI);
	afx_msg void OnRemoveMailboxPermissions();
	afx_msg void OnUpdateRemoveMailboxPermissions(CCmdUI* pCmdUI);
	afx_msg void OnToggleSystemRows();
	afx_msg void OnUpdateToggleSystemRows(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()

private:
	GridTemplateUsers m_Template;

	FrameMailboxPermissions* m_Frame;

	GridBackendColumn* m_ColumnLastRequestDateTime;
	GridBackendColumn* m_ColUser;
	GridBackendColumn* m_ColAccessRights;
	GridBackendColumn* m_ColDeny;
	GridBackendColumn* m_ColIdentity;
	GridBackendColumn* m_ColInheritanceType;
	GridBackendColumn* m_ColRealAce;
	GridBackendColumn* m_ColIsInherited;
	GridBackendColumn* m_ColIsValid;

	GridBackendColumn* m_ColSystem;

	bool m_HasLoadMoreAdditionalInfo;
	bool m_ShowSystemRows;
	vector<GridBackendColumn*>	m_MetaColumns;

	map<row_pk_t, MailboxPermissionsAddModification> m_MailboxPermAddMods;
	map<row_pk_t, MailboxPermissionsEditModification> m_MailboxPermEditMods;
	map<row_pk_t, MailboxPermissionsRemoveModification> m_MailboxPermRemoveMods;

	shared_ptr<MailboxPermissionsModificationsReverter> m_Reverter;
	shared_ptr<MailboxPermissionsModificationsApplyer> m_Applyer;

	vector<MailboxPermissionsAddModification> m_AppliedAdditions;
	vector<MailboxPermissionsEditModification> m_AppliedEditions;
	vector<MailboxPermissionsRemoveModification> m_AppliedRemovals;

public:
	class MailboxPermissionsGridRefresher : public IGridRefresher
	{
	public:
		MailboxPermissionsGridRefresher(GridMailboxPermissions& p_Grid);

		void RefreshAll() override;
		void RefreshSelection() override;

	private:
		GridMailboxPermissions& m_Grid;
	};

	class MailboxPermissionsSaveAllUpdater : public IButtonUpdater
	{
	public:
		MailboxPermissionsSaveAllUpdater(GridFrameBase& p_Frame, GridMailboxPermissions& p_Grid);
		void Update(CCmdUI* p_CmdUi) override;

	private:
		GridFrameBase& m_Frame;
		GridMailboxPermissions& m_Grid;
	};

	class MailboxPermissionsSaveSelectedUpdater : public IButtonUpdater
	{
	public:
		MailboxPermissionsSaveSelectedUpdater(GridFrameBase& p_Frame, GridMailboxPermissions& p_Grid);
		void Update(CCmdUI* p_CmdUi) override;

	private:
		GridFrameBase& m_Frame;
		GridMailboxPermissions& m_Grid;
	};

	class MailboxPermissionsRevertAllUpdater : public IButtonUpdater
	{
	public:
		MailboxPermissionsRevertAllUpdater(GridFrameBase& p_Frame, GridMailboxPermissions& p_Grid);
		void Update(CCmdUI* p_CmdUi) override;

	private:
		GridFrameBase& m_Frame;
		GridMailboxPermissions& m_Grid;
	};

	class MailboxPermissionsRevertSelectedUpdater : public IButtonUpdater
	{
	public:
		MailboxPermissionsRevertSelectedUpdater(GridFrameBase& p_Frame, GridMailboxPermissions& p_Grid);
		void Update(CCmdUI* p_CmdUi) override;

	private:
		GridFrameBase& m_Frame;
		GridMailboxPermissions& m_Grid;
	};

	class MailboxPermissionsModsWithRequestFeedbackProvider : public IModificationWithRequestFeedbackProvider
	{
	public:
		MailboxPermissionsModsWithRequestFeedbackProvider(GridMailboxPermissions& p_Grid);
		vector<const Modification*> Provide() override;

	private:
		GridMailboxPermissions& m_Grid;
	};

private:
	unique_ptr<MailboxPermissionsGridRefresher> m_Refresher;

	friend class MailboxPermissionsModificationsReverter;
	friend class MailboxPermissionsModificationsApplyer;
};

