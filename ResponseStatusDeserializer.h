#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ResponseStatus.h"

class ResponseStatusDeserializer : public JsonObjectDeserializer, public Encapsulate<ResponseStatus>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

