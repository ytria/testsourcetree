#include "BusinessGroupDeleted.h"

RTTR_REGISTRATION
{
	using namespace rttr;

	registration::class_<BusinessGroupDeleted>("Deleted Group") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Deleted Group"))))
		.constructor()(policy::ctor::as_object)
	;
}
