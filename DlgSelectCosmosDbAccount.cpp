#include "DlgSelectCosmosDbAccount.h"
#include "Sapio365Session.h"

namespace
{
	class ExternalFctsHandler : public CCmdTarget
	{
	public:
		ExternalFctsHandler(CWnd* p_Parent, std::shared_ptr<Sapio365Session> p_Session, YAdvancedHtmlView* p_HtmlView)
			: m_Parent(p_Parent)
			, m_Session(p_Session)
			, m_HtmlView(p_HtmlView)
		{
			EnableAutomation();
		}

		BSTR deleteAccount()
		{
			_bstr_t rv = "";
			ASSERT(false);
			return rv;
		}

	private:
		DECLARE_DISPATCH_MAP()

		std::shared_ptr<Sapio365Session> m_Session;
		CWnd* m_Parent;
		YAdvancedHtmlView* m_HtmlView;
	};

	BEGIN_DISPATCH_MAP(ExternalFctsHandler, CCmdTarget)
		DISP_FUNCTION(ExternalFctsHandler, "deleteAccount", deleteAccount, VT_BSTR, VTS_NONE)
	END_DISPATCH_MAP()
}


DlgSelectCosmosDbAccount::DlgSelectCosmosDbAccount(Mode p_Mode, const vector<CosmosDbAccountInfo>& p_DbAccounts, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent, bool p_OnlyShowAccountsWithYtriaDbs)
	: ResizableDialog(IDD, p_Parent)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_DbAccounts(p_DbAccounts)
	, m_OnlyShowAccountsWithYtriaDbs(p_OnlyShowAccountsWithYtriaDbs)
	, m_Mode(p_Mode)
{
	m_HtmlView->SetExternal(std::make_shared<ExternalFctsHandler>(this, p_Session, m_HtmlView.get()));

	// Load hbs sizes
	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_COSMOSDBINFO_SIZE, g_Sizes);
		ASSERT(success);
	}
}

bool DlgSelectCosmosDbAccount::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	const auto it = data.find(_YTEXT("cancel"));
	if (data.end() != it)
	{
		endDialogFixed(IDCANCEL);
		return false;
	}

	auto itt = data.find(g_DbNameKey);
	ASSERT(itt != data.end());
	if (itt != data.end())
		m_DbAccountName = itt->second;

	if (!m_OnOkValidation || m_OnOkValidation(*this))
		endDialogFixed(IDOK);
	return false;
}

const wstring& DlgSelectCosmosDbAccount::GetDbAccountName() const
{
	return m_DbAccountName;
}

void DlgSelectCosmosDbAccount::SetOnOkValidation(OnOkValidation p_OnOkValid)
{
	m_OnOkValidation = p_OnOkValid;
}

BOOL DlgSelectCosmosDbAccount::OnInitDialogSpecificResizable()
{
	switch (m_Mode)
	{
	case Mode::DELETION:
		SetWindowText(_T("Delete Cosmos DB Account - Select Cosmos DB Account"));
		break;
	case Mode::SELECTION:
		SetWindowText(_T("Set Cosmos DB Account - Select Cosmos DB Account"));
		break;
	default:
		ASSERT(false);
		break;
	}

	CWnd* pCtrl = GetDlgItem(IDC_GRID_PLACEHOLDER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, NULL);

	m_HtmlView->SetLinkOpeningPolicy(true, true);
	m_HtmlView->SetPostedDataTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
		{
			{ _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
			,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-cosmosDbInfo.js"), _YTEXT("") } } // This file is in resources
			,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("cosmosDbInfo.js"), _YTEXT("") } }, // This file is in resources
		}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("cosmosDbInfo.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-height"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-width"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);

	BlockVResize(true);

	return TRUE;
}

void DlgSelectCosmosDbAccount::generateJSONScriptData(wstring& p_Output)
{
	json::value main = json::value::object({
		{ _YTEXT("id"), json::value::string(_YTEXT("DlgSelectCosmosDbAccount")) },
		{ _YTEXT("method"), json::value::string(_YTEXT("POST")) },
		{ _YTEXT("action"), json::value::string(_YTEXT("#")) },
	});

	wstring text;
	switch (m_Mode)
	{
	case Mode::DELETION:
		text = YtriaTranslate::Do(DlgDeleteCosmosDbAccount_generateJSONScriptData_5, _YLOC("Please select the Cosmos DB accounts to be deleted from the list of available accounts:")).c_str();
		break;
	case Mode::SELECTION:
		text = YtriaTranslate::Do(DlgDeleteCosmosDbAccount_generateJSONScriptData_2, _YLOC("Please select a Cosmos DB account from the list of available accounts:")).c_str();
		break;
	default:
		ASSERT(false);
		break;
	}

	json::value items = json::value::array();
	items.as_array()[0] = json::value::object({
		{ _YTEXT("hbs"), json::value::string(_YTEXT("cosmosDbSelect")) },
		{ _YTEXT("introTextOne"), json::value::string(text) },
		{ _YTEXT("subscriptionField"), json::value::string(g_DbNameKey) },
		{ _YTEXT("subscriptionLabel"), json::value::string(_YTEXT("Available Cosmos DB Account")) },
		{ _YTEXT("noSizeLimit"), json::value::string(_YTEXT("X")) },
		{ _YTEXT("allCosmosDBLabel"), json::value::string(YtriaTranslate::Do(DlgDeleteCosmosDbAccount_generateJSONScriptData_4, _YLOC("CosmosDB Account Status")).c_str()) },
	});

	AddDynamicFields(items.as_array()[0].as_object());

	items.as_array()[1] = json::value::object({
		{ _YTEXT("hbs"), json::value::string(_YTEXT("Button")) },
		{
			_YTEXT("choices"), json::value::array({
				json::value::object({
					{ _YTEXT("type"), json::value::string(_YTEXT("submit")) },
					{ _YTEXT("label"), json::value::string(_YTEXT("OK")) },
					{ _YTEXT("attribute"), json::value::string(_YTEXT("post")) },
					{ _YTEXT("name"), json::value::string(_YTEXT("ok")) },
					{ _YTEXT("class"), json::value::string(_YTEXT("is-primary")) }
				}),
				json::value::object({
					{ _YTEXT("type"), json::value::string(_YTEXT("submit")) },
					{ _YTEXT("label"), json::value::string(_YTEXT("Cancel")) },
					{ _YTEXT("name"), json::value::string(_YTEXT("cancel")) },
					{ _YTEXT("attribute"), json::value::string(_YTEXT("cancel")) },
				})
			})
		}
	});
	
	auto root = json::value::object({
		{ _YTEXT("main"), main },
		{ _YTEXT("items"), items }
	});

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}

void DlgSelectCosmosDbAccount::AddDynamicFields(web::json::object& p_Object)
{
	size_t subChoiceIdx = 0;
	size_t cosmosChoiceIdx = 0;
	p_Object[_YTEXT("subscriptionChoice")] = json::value::array();
	p_Object[_YTEXT("allCosmosDBChoice")] = json::value::array();
	
	for (const auto& account : m_DbAccounts)
	{
		bool show = !account.m_HasDatabase || account.m_OnlyHasYtriaDatabase;
		if (m_OnlyShowAccountsWithYtriaDbs)
			show = account.m_HasDatabase && account.m_OnlyHasYtriaDatabase;
		if (show)
		{
			p_Object[_YTEXT("subscriptionChoice")][subChoiceIdx++] = json::value::object({
				{ _YTEXT("value"), json::value::string(account.m_Name) },
				{ _YTEXT("label"), json::value::string(account.m_Name) },
				{ _YTEXT("default"), json::value::string(subChoiceIdx == 0 ? _YTEXT("X") : _YTEXT("")) }
			});
			p_Object[_YTEXT("allCosmosDBChoice")][cosmosChoiceIdx++] = json::value::object({
				{ _YTEXT("cosmosDBField"), json::value::string(account.m_Name) },
				{ _YTEXT("cosmosDBLabel"), json::value::string(account.m_Info) }
			});
		}
	}
}

std::map<wstring, int, Str::keyLessInsensitive> DlgSelectCosmosDbAccount::g_Sizes;

const wstring DlgSelectCosmosDbAccount::g_DbNameKey = _YTEXT("dbname");
