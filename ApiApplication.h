#pragma once
#include "PermissionScope.h"
#include "PreAuthorizedApplication.h"

class ApiApplication
{
public:
	boost::YOpt<bool> m_AcceptMappedClaims;
	boost::YOpt<vector<PooledString>> m_KnownClientApplications;
	boost::YOpt<vector<PermissionScope>> m_OAuth2PermissionScopes;
	boost::YOpt<vector<PreAuthorizedApplication>> m_PreAuthorizedApplications;
	boost::YOpt<int32_t> m_RequestedAccessTokenVersion;
};

