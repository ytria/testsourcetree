#pragma once

#include "IMsGraphPageRequester.h"

class MSGraphSession;
class IHttpRequestLogger;
class IValueListDeserializer;
class PaginatedRequestResults;
class YtriaTaskData;

class GraphPageRequester : public IMsGraphPageRequester
{
public:
	GraphPageRequester(const shared_ptr<IValueListDeserializer>& p_Deserializer, const shared_ptr<PaginatedRequestResults>& p_Result, const shared_ptr<IHttpRequestLogger>& p_HttpLogger);

	web::json::value Request(const web::uri& p_Uri, const shared_ptr<MSGraphSession>& p_Session, YtriaTaskData p_TaskData) override;

	void SetHeaders(const web::http::http_headers& p_Headers);

private:
	shared_ptr<IValueListDeserializer> m_Deserializer;
	shared_ptr<PaginatedRequestResults> m_Result;
};

