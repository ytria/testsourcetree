#pragma once

#include "IRequester.h"

#include "BusinessMailFolder.h"
#include "MailFolderDeserializer.h"
#include "ValueListDeserializer.h"
#include "IRequestLogger.h"

class MailFolderRequester : public IRequester
{
public:
	MailFolderRequester(const wstring& p_UserId, const wstring& p_FolderName, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	BusinessMailFolder& GetData();

private:
	shared_ptr<MailFolderDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
	wstring m_UserId;
	wstring m_FolderName;
};

