#include "MessageDeserializer.h"
#include "InternetMessageHeaderDeserializer.h"
#include "ItemBodyDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "Recipient.h"
#include "RecipientDeserializer.h"
#include "MsGraphFieldNames.h"

void MessageDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT(O365_ID), m_Data.m_Id, p_Object, true);

    {
        ListDeserializer<Recipient, RecipientDeserializer> rld;
        if (JsonSerializeUtil::DeserializeAny(rld, _YTEXT("bccRecipients"), p_Object))
            m_Data.m_BccRecipients = rld.GetData();
    }

    {
        ListDeserializer<Recipient, RecipientDeserializer> rld;
        if (JsonSerializeUtil::DeserializeAny(rld, _YTEXT("ccRecipients"), p_Object))
            m_Data.m_CcRecipients = rld.GetData();
    }

    {
        ListDeserializer<Recipient, RecipientDeserializer> rld;
        if (JsonSerializeUtil::DeserializeAny(rld, _YTEXT("replyTo"), p_Object))
            m_Data.m_ReplyTo = rld.GetData();
    }

    {
        ListDeserializer<Recipient, RecipientDeserializer> rld;
        if (JsonSerializeUtil::DeserializeAny(rld, _YTEXT("toRecipients"), p_Object))
            m_Data.m_ToRecipients = rld.GetData();
    }

    {
        RecipientDeserializer rd;
        if (JsonSerializeUtil::DeserializeAny(rd, _YTEXT("sender"), p_Object))
        {
            if (rd.GetData().m_EmailAddress != boost::none)
            {
                m_Data.m_SenderName = rd.GetData().m_EmailAddress->m_Name;
                m_Data.m_SenderAddress = rd.GetData().m_EmailAddress->m_Address;
            }
        }
    }

    {
        RecipientDeserializer rd;
        if (JsonSerializeUtil::DeserializeAny(rd, _YTEXT("from"), p_Object))
        {
            if (rd.GetData().m_EmailAddress != boost::none)
            {
                m_Data.m_FromName = rd.GetData().m_EmailAddress->m_Name;
                m_Data.m_FromAddress = rd.GetData().m_EmailAddress->m_Address;
            }
        }
    }

    {
        ItemBodyDeserializer ibd;
        if (JsonSerializeUtil::DeserializeAny(ibd, _YTEXT("body"), p_Object))
        {
            if (ibd.GetData().Content != boost::none)
                m_Data.m_BodyContent = ibd.GetData().Content;
            if (ibd.GetData().ContentType != boost::none)
                m_Data.m_BodyContentType = ibd.GetData().ContentType;
        }
    }

    {
        ItemBodyDeserializer ibd;
        if (JsonSerializeUtil::DeserializeAny(ibd, _YTEXT("uniqueBody"), p_Object))
        {
            if (ibd.GetData().Content != boost::none)
                m_Data.m_BodyContent = ibd.GetData().Content;
            if (ibd.GetData().ContentType != boost::none)
                m_Data.m_BodyContentType = ibd.GetData().ContentType;
        }
    }

    {
        ListStringDeserializer svd;
        if (JsonSerializeUtil::DeserializeAny(svd, _YTEXT("categories"), p_Object))
            m_Data.m_Categories = svd.GetData();
    }

	{
		ListDeserializer<InternetMessageHeader, InternetMessageHeaderDeserializer> imhld;
		if (JsonSerializeUtil::DeserializeAny(imhld, _YTEXT("internetMessageHeaders"), p_Object))
			m_Data.m_InternetMessageHeaders = imhld.GetData();
	}

    JsonSerializeUtil::DeserializeString(_YTEXT("bodyPreview"), m_Data.m_BodyPreview, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("changeKey"), m_Data.m_ChangeKey, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("conversationId"), m_Data.m_ConversationId, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.m_CreatedDateTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("importance"), m_Data.m_Importance, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("inferenceClassification"), m_Data.m_InferenceClassification, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("internetMessageId"), m_Data.m_InternetMessageId, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.m_LastModifiedDateTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("parentFolderId"), m_Data.m_ParentFolderId, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("receivedDateTime"), m_Data.m_ReceivedDateTime, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("sentDateTime"), m_Data.m_SentDateTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("subject"), m_Data.m_Subject, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("webLink"), m_Data.m_WebLink, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("hasAttachments"), m_Data.m_HasAttachments, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isDeliveryReceiptRequested"), m_Data.m_IsDeliveryReceiptRequested, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isDraft"), m_Data.m_IsDraft, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isRead"), m_Data.m_IsRead, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isReadReceiptRequested"), m_Data.m_IsReadReceiptRequested, p_Object);
}
