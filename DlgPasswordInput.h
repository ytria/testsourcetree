#pragma once

#include "DlgFormsHTML.h"

class DlgPasswordInput : public DlgFormsHTML
{
public:
	DlgPasswordInput(CWnd* p_Parent, bool p_CreatePassword, const wstring& p_WarningIntroText = Str::g_EmptyString);
	virtual ~DlgPasswordInput();

	//Returns true if processed
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data) override;

	const std::string& GetPassword();

	//bool ShouldAutogeneratePasswords() const;

protected:
	virtual wstring getDialogTitle() const override;
	virtual wstring externalValidationCallback(const wstring p_PropertyName, const wstring p_CurrentValue) override;

private:
	virtual void generateJSONScriptData() override;

	//bool m_AutoGeneratePasswords;
	std::string m_Password;
	bool m_CreatePassword;
	const wstring m_WarningIntroText;
};
