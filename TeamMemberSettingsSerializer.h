#pragma once

#include "IJsonSerializer.h"
#include "TeamMemberSettings.h"

class TeamMemberSettingsSerializer : public IJsonSerializer
{
public:
    TeamMemberSettingsSerializer(const TeamMemberSettings& p_TeamMemberSettings);
    void Serialize() override;

private:
    const TeamMemberSettings& m_TeamMemberSettings;
};

