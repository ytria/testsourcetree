#pragma once

template <class BaseModificationType, class ConcreteModificationType>
bool WithSiblings<BaseModificationType, ConcreteModificationType>::IsCorrespondingRow(const row_pk_t& p_RowPk) const
{
	return RowModification::IsCorrespondingRow(p_RowPk) || std::any_of(m_SiblingModifications.begin(), m_SiblingModifications.end()
		, [&p_RowPk](const std::unique_ptr<ConcreteModificationType>& p_Mod)
		{
			return p_Mod->IsCorrespondingRow(p_RowPk);
		}
	);
}

template <class BaseModificationType, class ConcreteModificationType>
void WithSiblings<BaseModificationType, ConcreteModificationType>::AddSiblingModification(std::unique_ptr<ConcreteModificationType> p_SiblingMod)
{
	ASSERT(p_SiblingMod);
	if (p_SiblingMod)
	{
		// Only one level allowed
		ASSERT(p_SiblingMod->m_SiblingModifications.empty());
		ASSERT(std::none_of(m_SiblingModifications.begin(), m_SiblingModifications.end(), [&p_SiblingMod](const std::unique_ptr<ConcreteModificationType>& p_Mod)
			{
				return p_SiblingMod->IsCorrespondingRow(p_Mod->GetRowKey());
			})
		);

		m_SiblingModifications.push_back(std::move(p_SiblingMod));
	}
}
