#pragma once

#include "BusinessObject.h"
#include "LifeCyclePolicies.h"

class BusinessLifeCyclePolicies : public BusinessObject
{
public:
	static const wstring& g_StatusNoPolicyAvailable;
	static const wstring& g_StatusPolicyIsNone;
	static const wstring& g_StatusApplied;
	static const wstring& g_StatusNotApplied;
	static const wstring& g_StatusPolicyError;

public:
	BusinessLifeCyclePolicies();
	BusinessLifeCyclePolicies(const LifeCyclePolicies& p_MailFolder);

	const boost::YOpt<PooledString>& GetNumberOfDaysAsString() const;
	void SetNumberOfDaysAsString(const boost::YOpt<PooledString>& p_Days);

	const boost::YOpt<int32_t>& GetNumberOfDays() const;
	void SetNumberOfDays(const boost::YOpt<int32_t>& p_Days);

	const boost::YOpt<PooledString>& GetManagedGroupTypes() const;
	void SetManagedGroupTypes(const boost::YOpt<PooledString>& p_Types);

	const boost::YOpt<PooledString>& GetAlternativeNotificationEmails() const;
	void SetAlternativeNotificationEmails(const boost::YOpt<PooledString>& p_Emails);

	bool IsPresent() const;

    static LifeCyclePolicies ToLifeCyclePolicies(const BusinessLifeCyclePolicies& p_BusinessLifeCyclePolicies);

	mutable boost::YOpt<PooledString> m_NumberOfDaysAsString; // Only filled on GetNumberOfDaysAsString call

	boost::YOpt<int32_t> m_NumberOfDays;
	boost::YOpt<PooledString> m_ManagedGroupTypes;
	boost::YOpt<PooledString> m_AlternativeNotificationEmails;

    RTTR_ENABLE(BusinessObject)

protected:
	pplx::task<vector<HttpResultWithError>> SendEditRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;
	TaskWrapper<HttpResultWithError> SendCreateRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;
	TaskWrapper<HttpResultWithError> SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;

private:
	static wstring g_StatusNoPolicyAvailableValue;
	static wstring g_StatusPolicyIsNoneValue;
	static wstring g_StatusAppliedValue;
	static wstring g_StatusNotAppliedValue;
	static wstring g_StatusPolicyErrorValue;
};
