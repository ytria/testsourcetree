#pragma once

#include "BaseO365Grid.h"
#include "EducationSchool.h"

class FrameSchools;

struct SchoolDeletion
{
	vector<GridBackendField> m_RowPk;
	PooledString m_SchoolId;
};

struct SchoolUpdate
{
	vector<GridBackendField> m_RowPk;
	PooledString m_SchoolId;
	json::value m_NewValues;
};

struct ClassRemoval
{
	vector<GridBackendField> m_RowPk;
	PooledString m_ClassId;
};

struct ClassAddition
{
	vector<GridBackendField> m_RowPk;
	PooledString m_SchoolId;
	PooledString m_ClassId;
};

struct ClassUpdate
{
	vector<GridBackendField> m_RowPk;
	PooledString m_ClassId;
	json::value m_NewValues;
};

struct SchoolsChanges
{
	vector<SchoolDeletion> m_SchoolDeletions;
	vector<SchoolUpdate> m_SchoolUpdates;
	vector<ClassRemoval> m_ClassRemovals;
	vector<ClassAddition> m_ClassAdditions;
	vector<ClassUpdate> m_ClassUpdates;
};

class GridSchools : public ModuleO365Grid<EducationSchool>
{
public:
	GridSchools();
	virtual ~GridSchools() override;
	void BuildView(const vector<EducationSchool>& p_Schools, const vector<O365UpdateOperation>& p_Updates, bool p_FullPurge);
	SchoolsChanges GetChanges();
	SchoolsChanges GetChanges(const vector<GridBackendRow*>& p_Rows);

	wstring GetName(const GridBackendRow* p_Row) const override;
	virtual row_pk_t UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody) override;

protected:
	void customizeGrid() override;

	EducationSchool	getBusinessObject(GridBackendRow*) const override;
	EducationClass  getEducationClass(GridBackendRow*) const;

	void UpdateAddressField(const wstring& p_FieldName, const wstring& p_NewValue, web::json::value& p_Value);

	boost::YOpt<PooledString> GetStrValue(GridBackendRow& p_Row, GridBackendColumn* p_Col) const;

private:
	afx_msg void OnEdit();
	afx_msg void OnUpdateEdit(CCmdUI* pCmdUI);
	afx_msg void OnDelete();
	afx_msg void OnUpdateDelete(CCmdUI* pCmdUI);
	afx_msg void OnAddClass();
	afx_msg void OnUpdateAddClass(CCmdUI* pCmdUI);
	afx_msg void OnRemoveClass();
	afx_msg void OnUpdateRemoveClass(CCmdUI* pCmdUI);
	afx_msg void OnShowClassMembers();
	afx_msg void OnUpdateShowClassMembers(CCmdUI* pCmdUI);

	DECLARE_MESSAGE_MAP()

	O365IdsContainer GetSelectedClassIds() const;
	void AddDeleteMod(const DeletedObjectModification& p_Mod, SchoolsChanges& p_Changes);
	void AddCreateMod(const CreatedObjectModification& p_Mod, SchoolsChanges& p_Changes);
	void AddUpdateMod(const RowFieldUpdates<Scope::O365>& p_Mod, SchoolsChanges& p_Changes);

	void InitializeCommands() override;
	void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart) override;
	void FillFields(GridBackendRow* p_Row, const EducationSchool& p_DirAudit, GridUpdater& p_Updater);

	void AddPendingClassChanges(GridBackendRow& p_ClassRow, const row_pk_t& p_RowPk, const EducationClass& p_CurClass);
	void AddPendingSchoolChanges(GridBackendRow& p_SchoolRow, const row_pk_t& p_RowPk, const EducationSchool& p_School);
	void UpdateClassRow(GridBackendRow* p_ClassRow, const EducationClass& p_CurClass);
	void AddFieldChange(GridBackendRow& p_ClassRow, const row_pk_t& p_RowPk, GridBackendColumn* p_Col, PooledString p_Val);

	set<GridBackendRow*> GetSelectedSchoolRows() const;
	GridBackendRow* CreateNewClassRow(PooledString p_SchoolId, PooledString p_ClassId, const SelectedItem& p_Class);

	vector<GridBackendField> CreatePk(PooledString p_SchoolId, PooledString p_ClassId) const;
	void RemoveSchoolRelatedModifications(GridBackendRow* p_Row);
	void AddPendingChanges(const vector<EducationClass>& p_Classes);
	void AddPendingChanges(const vector<EducationSchool>& p_Schools);
	void AddPendingClassChanges(const EducationClass& p_Class);
	void AddPendingSchoolChanges(const EducationSchool& p_School);

	// School columns
	GridBackendColumn* m_ColSchoolId;
	GridBackendColumn* m_ColDisplayName;
	GridBackendColumn* m_ColSchoolDescription;
	GridBackendColumn* m_ColSchoolStatus;
	GridBackendColumn* m_ColSchoolExternalSource;
	GridBackendColumn* m_ColSchoolPrincipalEmail;
	GridBackendColumn* m_ColSchoolPrincipalName;
	GridBackendColumn* m_ColSchoolExternalPrincipalId;
	GridBackendColumn* m_ColSchoolHighestGrade;
	GridBackendColumn* m_ColSchoolLowestGrade;
	GridBackendColumn* m_ColSchoolNumber;
	GridBackendColumn* m_ColSchoolExternalId;
	GridBackendColumn* m_ColSchoolPhone;
	GridBackendColumn* m_ColSchoolFax;
	GridBackendColumn* m_ColSchoolAddressCity;
	GridBackendColumn* m_ColSchoolAddressCountryOrRegion;
	GridBackendColumn* m_ColSchoolAddressPostalCode;
	GridBackendColumn* m_ColSchoolAddressState;
	GridBackendColumn* m_ColSchoolAddressStreet;
	GridBackendColumn* m_ColCreatedByAppDisplayName;
	GridBackendColumn* m_ColCreatedByAppId;
	GridBackendColumn* m_ColCreatedByAppEmail;
	GridBackendColumn* m_ColCreatedByAppIdentityProvider;
	GridBackendColumn* m_ColCreatedByDeviceDisplayName;
	GridBackendColumn* m_ColCreatedByDeviceId;
	GridBackendColumn* m_ColCreatedByDeviceEmail;
	GridBackendColumn* m_ColCreatedByDeviceIdentityProvider;
	GridBackendColumn* m_ColCreatedByUserDisplayName;
	GridBackendColumn* m_ColCreatedByUserId;
	GridBackendColumn* m_ColCreatedByUserEmail;
	GridBackendColumn* m_ColCreatedByUserIdentityProvider;

	// Class columns
	GridBackendColumn* m_ColClassDescription;
	GridBackendColumn* m_ColClassMailNickname;
	GridBackendColumn* m_ColClassClassCode;
	GridBackendColumn* m_ColClassExternalId;
	GridBackendColumn* m_ColClassExternalName;
	GridBackendColumn* m_ColClassExternalSource;
	GridBackendColumn* m_ColClassTermDisplayName;
	GridBackendColumn* m_ColClassTermExternalId;
	GridBackendColumn* m_ColClassTermStartDate;
	GridBackendColumn* m_ColClassTermEndDate;

	friend class FrameSchools;
};

