#include "FileDeserializer.h"

#include "HashesTypeDeserializer.h"
#include "JsonSerializeUtil.h"

void FileDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        HashesTypeDeserializer htd;
        if (JsonSerializeUtil::DeserializeAny(htd, _YTEXT("hashes"), p_Object))
            m_Data.Hashes = htd.GetData();
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("mimeType"), m_Data.MimeType, p_Object);
}
