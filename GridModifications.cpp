#include "BaseO365Grid.h"
#include "GridUpdater.h"
#include "Modification.h"
#include <algorithm>
#include "UserLicenseModification.h"
#include "FakeModification.h"
#include "IModificationWithRequestFeedbackProvider.h"

template<Scope scope>
GridFieldModifications<scope>::GridFieldModifications(O365Grid& p_Grid)
	: m_Grid(p_Grid)
{
}

template<Scope scope>
GridFieldModifications<scope>::~GridFieldModifications()
{
	// we are destroying the object (then probably the grid).
	// Prevent the modifications from reverting themselves.
	for (auto& item : m_RowFieldModifications)
		item->DontRevertOnDestruction();
}

template<Scope scope>
void GridFieldModifications<scope>::Add(std::unique_ptr<FieldUpdate<scope>> p_FieldModification)
{
	// Find the "RowFieldModifications" object if any. If one was found, add the FieldModification instance to it
	ASSERT(nullptr != p_FieldModification);
	if (nullptr != p_FieldModification)
	{
		const auto rowKey = p_FieldModification->GetRowKey();
		bool rowModExisted = false;
		for (const auto& rowModification : m_RowFieldModifications)
		{
			if (rowModification->IsCorrespondingRow(rowKey))
			{
				rowModification->AddFieldUpdate(std::move(p_FieldModification));
				rowModExisted = true;
				break;
			}
		}

		if (!rowModExisted)
		{
			m_RowFieldModifications.push_back(std::make_unique<RowFieldUpdates<scope>>(m_Grid, rowKey));
			m_RowFieldModifications.back()->AddFieldUpdate(std::move(p_FieldModification));
		}
	}
}

template<Scope scope>
const typename GridFieldModifications<scope>::RowFieldModifications& GridFieldModifications<scope>::GetRowFieldModifications() const
{
	return m_RowFieldModifications;
}

template<Scope scope>
void GridFieldModifications<scope>::RefreshStates(GridUpdater& p_Updater)
{
	if (p_Updater.HasUpdatedRowPk()
		|| p_Updater.HasAppliedRowPk()) // Should we?
	{
		m_Grid.GetRowIndex().LoadRows(true); // Index not loaded properly sometimes
		for (const auto& rowMod : m_RowFieldModifications)
		{
			if (p_Updater.IsAppliedRowPK(rowMod->GetRowKey()) || p_Updater.IsUpdatedRowPK(rowMod->GetRowKey()))
				rowMod->RefreshState();
		}
	}
}

template<Scope scope>
void GridFieldModifications<scope>::RefreshStatesPostProcess(GridUpdater& p_Updater)
{
	if (p_Updater.HasUpdatedRowPk())
	{
		for (const auto& rowMod : m_RowFieldModifications)
		{
			if (p_Updater.IsAppliedRowPK(rowMod->GetRowKey()) || p_Updater.IsUpdatedRowPK(rowMod->GetRowKey()))
				rowMod->RefreshStatesPostProcess(p_Updater);
		}
	}
}

template<Scope scope>
void GridFieldModifications<scope>::RemoveFinalStateModifications()
{
	RemoveFinalStateModificationsForParentRows(boost::none);
}

template<Scope scope>
void GridFieldModifications<scope>::RemoveErrorModifications()
{
	m_RowFieldModifications.erase(std::remove_if(m_RowFieldModifications.begin(), m_RowFieldModifications.end(), Modification::IsError()), m_RowFieldModifications.end());
}

template<Scope scope>
void GridFieldModifications<scope>::RemoveSentAndReceivedModifications()
{
	m_RowFieldModifications.erase(std::remove_if(m_RowFieldModifications.begin(), m_RowFieldModifications.end(), Modification::IsSentAndReceived()), m_RowFieldModifications.end());
}

template<Scope scope>
void GridFieldModifications<scope>::UpdateShownValues()
{
	for (const auto& rowFieldModification : m_RowFieldModifications)
		rowFieldModification->UpdateShownValues();
}

template<Scope scope>
bool GridFieldModifications<scope>::HasModifications() const
{
	return std::any_of(m_RowFieldModifications.begin(), m_RowFieldModifications.end(), Modification::IsLocal());
}


template<Scope scope>
bool GridFieldModifications<scope>::RevertAll()
{
	return RevertAllRowModifications();
}

template<Scope scope>
bool GridFieldModifications<scope>::Revert(const row_pk_t& rowKey, bool p_RevertOnDestruction/* = true*/)
{
	const auto size = m_RowFieldModifications.size();
	m_RowFieldModifications.erase(std::remove_if(m_RowFieldModifications.begin()
		, m_RowFieldModifications.end()
		, [=](const auto& item)
		{
			bool del = IMainItemModification::MustSurviveRefresh()(item) && item->IsCorrespondingRow(rowKey);
			if (del && !p_RevertOnDestruction)
				item->DontRevertOnDestruction();
			return del;
		})
		, m_RowFieldModifications.end());

	return size != m_RowFieldModifications.size();
}

template<Scope scope>
bool GridFieldModifications<scope>::RevertSelectedRows(bool p_RevertOnDestruction/* = true*/)
{
	const auto size = m_RowFieldModifications.size();
	m_RowFieldModifications.erase(std::remove_if(m_RowFieldModifications.begin()
		, m_RowFieldModifications.end()
		, [=](const auto& item)
		{
			bool del = IMainItemModification::MustSurviveRefresh()(item);
			if (del)
			{
				auto row = m_Grid.GetRowIndex().GetExistingRow(item->GetRowKey());
				del = nullptr != row && row->IsSelected();
				if (del && !p_RevertOnDestruction)
					item->DontRevertOnDestruction();
			}
			return del;
		})
		, m_RowFieldModifications.end());

	return size != m_RowFieldModifications.size();
}

template<Scope scope>
bool GridFieldModifications<scope>::RevertFieldUpdates(const row_pk_t& rowKey, const std::vector<UINT>& p_ColIDs)
{
	bool rowModExisted = false;

	for (auto it = m_RowFieldModifications.begin(); it != m_RowFieldModifications.end(); ++it)
	{
		auto& rowModification = *it;
		if (rowModification->IsCorrespondingRow(rowKey))
		{
			for (const auto colId : p_ColIDs)
			{
				rowModExisted = rowModification->RevertFieldUpdate(colId);
				if (rowModExisted && rowModification->GetFieldUpdates().empty())
				{
					m_RowFieldModifications.erase(it);
					break;
				}
			}
			break;
		}
	}

	return rowModExisted;
}

template<Scope scope>
bool GridFieldModifications<scope>::RemoveRowModifications(const row_pk_t& rowKey, bool p_RevertOnDestruction)
{
	const auto size = m_RowFieldModifications.size();
	m_RowFieldModifications.erase(std::remove_if(m_RowFieldModifications.begin()
		, m_RowFieldModifications.end()
		, [=](const auto& item)
		{
			bool del = IMainItemModification::MustSurviveRefresh()(item) && item->IsCorrespondingRow(rowKey);
			if (del && !p_RevertOnDestruction)
				item->DontRevertOnDestruction();
			return del;
		})
		, m_RowFieldModifications.end());

	return size != m_RowFieldModifications.size();
}


template<Scope scope>
RowFieldUpdates<scope>* GridFieldModifications<scope>::GetRowFieldModification(const row_pk_t& rowKey)
{
	auto it = std::find_if(m_RowFieldModifications.begin(), m_RowFieldModifications.end(), [=](const auto& item)
		{
			return item->IsCorrespondingRow(rowKey);
		});

	if (m_RowFieldModifications.end() != it)
		return it->get();
	return nullptr;
}

template<Scope scope>
set<GridBackendRow*> GridFieldModifications<scope>::GetModifiedRows(bool p_OnlyAppliedLocally, bool p_IncludeExplosionSiblings/* = true*/) const
{
	set<GridBackendRow*> rows;

	for (auto& mod : m_RowFieldModifications)
	{
		if (!p_OnlyAppliedLocally || Modification::IsLocal()(mod))
		{
			auto row = m_Grid.GetRowIndex().GetExistingRow(mod->GetRowKey());
			ASSERT(nullptr != row);
			if (nullptr != row)
				rows.insert(row);
		}
	}

	// Also get explosion siblings, they are modified too!
	if (p_IncludeExplosionSiblings && !rows.empty())
	{
		vector<GridBackendRow*> rowsCopy(rows.begin(), rows.end());
		vector<pair<GridBackendRow*, map<GridBackendColumn*, vector<GridBackendRow*>>>>	implosionSetup;// source row vs. col + exploded rows
		m_Grid.GetMultivalueManager().GetImplosionSetup(rowsCopy, implosionSetup);
		for (const auto& item1 : implosionSetup)
			for (const auto& item2 : item1.second)
				rows.insert(item2.second.begin(), item2.second.end());
	}

	return rows;
}

template<Scope scope>
bool GridFieldModifications<scope>::IsModified(GridBackendRow* p_Row) const
{
	auto testRowMod = [p_Row, this](const auto& p_Modification)
	{
		// When multivalue explosion occurs after row edit, the row PK is not accurate anymore in this modification
		// because the explosion adds columns to the grid PK.
		// If p_Row was added by an explosion, its parent row modification is its modification too 
		auto tsrRow = m_Grid.GetMultivalueManager().GetTopSourceRow(p_Row);
		auto modRow = m_Grid.GetRowIndex().GetExistingRow(p_Modification->GetRowKey());
		return p_Modification->GetState() == Modification::State::AppliedLocally && (modRow == p_Row || tsrRow == p_Row);
	};

	return std::any_of(m_RowFieldModifications.begin(), m_RowFieldModifications.end(), testRowMod);
}

template<Scope scope>
bool GridFieldModifications<scope>::HasAnyRowModificationOnOrUnder(GridBackendRow* p_TopAncestorRow) const
{
	auto testRowMod = [p_TopAncestorRow, this](const auto& p_Modification)
	{
		auto row = m_Grid.GetRowIndex().GetExistingRow(p_Modification->GetRowKey());
		return p_Modification->GetState() == Modification::State::AppliedLocally && nullptr != row && row->GetTopAncestorOrThis() == p_TopAncestorRow;
	};

	return std::any_of(m_RowFieldModifications.begin(), m_RowFieldModifications.end(), testRowMod);
}

template<Scope scope>
void GridFieldModifications<scope>::RemoveFinalStateModificationsForParentRows(const boost::YOpt<set<GridBackendRow*>>& p_AncestorRows)
{
	if (p_AncestorRows && p_AncestorRows->empty())
		return;

	// Row field modifications
	for (auto it = m_RowFieldModifications.begin(); it != m_RowFieldModifications.end();)
	{
		if (Modification::MustSurviveRefresh()(*it))
		{
			GridBackendRow* modRow = p_AncestorRows ? m_Grid.GetRowIndex().GetExistingRow((*it)->GetRowKey()) : nullptr;
			if (!p_AncestorRows || nullptr != modRow && p_AncestorRows->find(modRow->GetTopAncestorOrThis()) != p_AncestorRows->end())
			{
				it = m_RowFieldModifications.erase(it);
				continue;
			}
		}
		++it;
	}
}

template<Scope scope>
bool GridFieldModifications<scope>::RevertAllRowModifications()
{
	bool ret = !m_RowFieldModifications.empty();

	m_RowFieldModifications.clear();

	return ret;
}

template<Scope scope>
const O365Grid& GridFieldModifications<scope>::GetGrid() const
{
	return m_Grid;
}

// ============================================================================

GridModificationsO365::~GridModificationsO365()
{
	// we are destroying the object (then probably the grid).
	// Prevent the modifications from reverting themselves.
	for (auto& item : m_RowModifications)
		item->DontRevertOnDestruction();
	//for (auto& item : m_SubItemDeletions)
	//	item->DontRevertOnDestruction();
	//for (auto& item : m_SubItemUpdates)
	//	item->DontRevertOnDestruction();
}

bool GridModificationsO365::Add(std::unique_ptr<ISubItemDeletion> p_Deletion)
{
    bool added = false;
    ASSERT(nullptr != p_Deletion);
    if (nullptr != p_Deletion)
    {
        size_t index = 0;
        size_t foundIndex = 0;
        bool foundWithError = false;

        auto itt = std::find_if(m_SubItemDeletions.begin(), m_SubItemDeletions.end(), [&p_Deletion, &foundWithError, &index, &foundIndex](const std::unique_ptr<ISubItemDeletion>& p_Item) {
            const bool found = p_Item->GetKey() == p_Deletion->GetKey();
            const bool hasError = p_Item->GetError() != boost::none;
            if (found && hasError)
            {
                foundWithError = true;
                foundIndex = index;
            }
            ++index;

            return found;
        });
        if (itt == m_SubItemDeletions.end() || foundWithError)
        {
            // Are we replacing an update that produced an error with a new deletion?
            if (foundWithError)
            {
                auto key = m_SubItemDeletions[foundIndex]->GetKey();
                m_SubItemDeletions.erase(m_SubItemDeletions.begin() + foundIndex);
                m_SubItemDeletionErrors.erase(key);
            }
            added = true;
            m_SubItemDeletions.push_back(std::move(p_Deletion));
        }
    }

    return added;
}

CreatedObjectModification* GridModificationsO365::Add(std::unique_ptr<CreatedObjectModification> p_RowModification)
{
	return dynamic_cast<CreatedObjectModification*>(add(std::move(p_RowModification)));
}

DeletedObjectModification* GridModificationsO365::Add(std::unique_ptr<DeletedObjectModification> p_RowModification)
{
	return dynamic_cast<DeletedObjectModification*>(add(std::move(p_RowModification)));
}

void GridModificationsO365::Add(std::unique_ptr<RestoreObjectModification> p_RowModification)
{
	add(std::move(p_RowModification));
}

void GridModificationsO365::Add(std::unique_ptr<SubItemFieldUpdate> p_FieldUpdate, GridBackendRow* p_Row)
{
	ASSERT(nullptr != p_FieldUpdate);
	if (nullptr != p_FieldUpdate)
	{
        bool subItemUpdateExisted = false;
        for (const auto& subItemUpdate : m_SubItemUpdates)
        {
            if (subItemUpdate->GetKey() == p_FieldUpdate->GetKey())
            {
                subItemUpdate->AddFieldUpdate(std::move(p_FieldUpdate));
                subItemUpdateExisted = true;
                break;
            }
        }

        if (!subItemUpdateExisted)
        {
            m_SubItemUpdates.push_back(std::make_unique<RowSubItemFieldUpdates>(m_Grid, p_FieldUpdate->GetKey(), p_FieldUpdate->GetSubItemElderCol(), p_FieldUpdate->GetSubItemPkCol(), p_Row));
            m_SubItemUpdates.back()->AddFieldUpdate(std::move(p_FieldUpdate));
        }
    }
}

void GridModificationsO365::Add(std::unique_ptr<UserLicenseModification> p_RowModification)
{
    auto sameKeyMod = std::find_if(m_RowModifications.begin(), m_RowModifications.end()
        , [&p_RowModification](const std::unique_ptr<RowModification>& item)
    {
        return p_RowModification->IsCorrespondingRow(item->GetRowKey());
    });

    // Remove any modification on the same user
    if (sameKeyMod != m_RowModifications.end())
    {
        if ((*sameKeyMod)->GetState() == Modification::State::AppliedLocally)
        {
            auto licenseMod = dynamic_cast<UserLicenseModification*>((*sameKeyMod).get());
            ASSERT(nullptr != licenseMod);
            if (nullptr != licenseMod)
            {
                licenseMod->Merge(*p_RowModification);

                // If the merge made the modification empty, delete it
                if (licenseMod->GetDisabledSkus().empty() && licenseMod->GetModifiedSkus().empty())
                    m_RowModifications.erase(sameKeyMod);
                
                p_RowModification->DontRevertOnDestruction();
            }
        }
        else
        {
            (*sameKeyMod)->DontRevertOnDestruction();
            m_RowModifications.erase(sameKeyMod);
            add(std::move(p_RowModification));
        }
    }
    else
    {
        add(std::move(p_RowModification));
    }
}

const std::deque<std::unique_ptr<ISubItemDeletion>>& GridModificationsO365::GetSubItemDeletions() const
{
    return m_SubItemDeletions;
}

const std::deque<std::unique_ptr<RowSubItemFieldUpdates>>& GridModificationsO365::GetSubItemUpdates() const
{
    return m_SubItemUpdates;
}

const std::deque<std::unique_ptr<RowModification>>& GridModificationsO365::GetRowModifications() const
{
	return m_RowModifications;
}

vector<const Modification*> GridModificationsO365::GetModsRequestFeedback() const
{
	vector<const Modification*> mods;
	if (nullptr != m_ModsRequestFeedbackProvider)
		mods = m_ModsRequestFeedbackProvider->Provide();
	return mods;
}

void GridModificationsO365::ShowSubItemModificationAppliedLocally(GridBackendRow* p_Row)
{
	map<size_t, PooledString> statuses;
	GridBackendColumn* elderCol = nullptr;
	for (auto& subDel : m_SubItemDeletions)
	{
		if (subDel->IsCorrespondingRow(p_Row)
			&& !m_Grid.GetMultivalueManager().IsRowExploded(p_Row, subDel->GetSubItemPkCol()))
		{
			if (nullptr == elderCol)
				elderCol = subDel->GetSubItemElderCol();
			else
				ASSERT(elderCol == subDel->GetSubItemElderCol());

			statuses[subDel->GetMultiValueIndex(p_Row)] = GridBackend::g_RowStatusDeleted;
		}
	}

	for (auto& subUp : m_SubItemUpdates)
	{
		if (subUp->IsCorrespondingRow(p_Row)
			&& !m_Grid.GetMultivalueManager().IsRowExploded(p_Row, subUp->GetSubItemPkCol()))
		{
			if (nullptr == elderCol)
				elderCol = subUp->GetSubItemElderCol();
			else
				ASSERT(elderCol == subUp->GetSubItemElderCol());

			statuses[subUp->GetMultiValueIndex(p_Row)] = GridBackend::g_RowStatusModified;
		}
	}
	
	if (nullptr != elderCol && !statuses.empty())
	{
		const auto dataTitle = nullptr != elderCol->GetTopCategory() ? elderCol->GetTopCategory()->GetTitle() : elderCol->GetTitleDisplayed();
		const auto multival = p_Row->GetField(elderCol).GetValuesMulti<PooledString>();

		vector<wstring> msgStatus;
		if (nullptr != multival)
		{
			for (const auto& status : statuses)
				msgStatus.push_back(YtriaTranslate::Do(GridModifications_ShowSubItemModificationAppliedLocally_1, _YLOC("Modification - Value %1 in %2: %3"), Str::getStringFromNumber(status.first + 1).c_str(), dataTitle.c_str(), status.second.c_str()));// +1 for display
		}
		else
		{
			ASSERT(statuses.size() == 1);
			msgStatus.push_back(YtriaTranslate::Do(GridModifications_ShowSubItemModificationAppliedLocally_2, _YLOC("Modification - Value in %1: %2"), dataTitle.c_str(), statuses.begin()->second.c_str()));
		}

		ASSERT(!msgStatus.empty());
		if (!msgStatus.empty())
		{
			auto statusCol = m_Grid.GetBackend().GetStatusColumn();
			p_Row->AddField(msgStatus, statusCol, GridBackendUtil::ICON_MODIFIED);
		}

		for (auto col : elderCol->GetMultiValueExplosionSisters())
		{
			auto& field = p_Row->GetField(col);
			if (col->IsConcernedByCellFormatter(field))
				field.SetModified();
		}
	}
}

void GridModificationsO365::RefreshStates(GridUpdater& p_Updater)
{
	GridFieldModifications<Scope::O365>::RefreshStates(p_Updater);

	for (const auto& rowMod : m_RowModifications)
	{
		if (p_Updater.IsAppliedRowPK(rowMod->GetRowKey()) || p_Updater.IsUpdatedRowPK(rowMod->GetRowKey()))
			rowMod->RefreshState();
	}
}

void GridModificationsO365::RefreshStatesPostProcess(GridUpdater& p_Updater)
{
	GridFieldModifications<Scope::O365>::RefreshStatesPostProcess(p_Updater);

	for (const auto& rowMod : m_RowModifications)
	{
		if (p_Updater.IsAppliedRowPK(rowMod->GetRowKey()) || p_Updater.IsUpdatedRowPK(rowMod->GetRowKey()))
			rowMod->RefreshStatesPostProcess(p_Updater);
	}
}

void GridModificationsO365::RefreshSubItemModificationsStates(GridUpdater& p_Updater)
{
	// FIXME: What was this for??
    //for (auto& col : m_Grid.GetBackend().GetColumns())
    //    col->SetCellFormatter(nullptr);

	std::set<SubItemKey> delModsNotApplied;
	for (const auto& deletedMod : m_SubItemDeletions)
	{
		if (m_SubItemDeletionErrors.end() == m_SubItemDeletionErrors.find(deletedMod->GetKey()))
			delModsNotApplied.insert(deletedMod->GetKey());
	}

    for (auto row : p_Updater.GetOptions().GetRowsWithRefreshedValues())
    {   
		for (const auto& deletedMod : m_SubItemDeletions)
        {
            if (deletedMod->IsCorrespondingRow(row))
            {
				if (deletedMod->Refresh(row, m_SubItemDeletionErrors))
				{
					if (!m_Grid.UpdateRowStatus(row))
						m_Grid.OnRowNothingToSave(row, false);

					// We found the corresponding row, it means the deletion wasn't applied.
					// If there's no error (status would be RemoteError), we can assume we didn't even try to apply it and keep it in the list,
					// otherwise, we tried to apply it so remove it from that list
					if (deletedMod->GetState() != Modification::State::AppliedLocally)
						delModsNotApplied.erase(deletedMod->GetKey());
				}
            }			
        }

		for (auto it = m_SubItemUpdates.begin(); it != m_SubItemUpdates.end();)
        {
			auto& updateMod = *it;
            if (updateMod->IsCorrespondingRow(row))
            {
				if (updateMod->Refresh(row, m_SubItemUpdateErrors)
					&& !m_Grid.UpdateRowStatus(row)
					&& !row->IsChangesSaved()) // Refresh may have set this state which is not handled by UpdateRowStatus
					m_Grid.OnRowNothingToSave(row, false);
            }
			++it;
        }
    }

	for (auto& p_Deletion : m_SubItemDeletions)
	{
		if (p_Deletion->GetState() == Modification::State::AppliedLocally
			&& delModsNotApplied.find(p_Deletion->GetKey()) == delModsNotApplied.end())
		{
			p_Deletion->SetState(Modification::State::RemoteHasNewValue);
			auto row = p_Deletion->GetMainRowIfCollapsed();
			if (nullptr != row && row->IsModified())
			{
				row_pk_t pk;
				m_Grid.GetRowPK(row, pk);
				auto fakeSavedMod = std::make_unique<FakeModification>(m_Grid, pk);
				m_RowModifications.push_back(std::move(fakeSavedMod));
				m_Grid.OnRowSaved(row, false, YtriaTranslate::Do(RowFieldUpdates_RefreshState_2, _YLOC("All values have been modified successfully.")).c_str());
			}
		}
	}
}

bool GridModificationsO365::HasSubItemDeletion(const SubItemKey& p_Key, bool p_OnlyAppliedLocally) const
{
    for (const auto& subItemDeletion : m_SubItemDeletions)
    {
        if (subItemDeletion->GetKey() == p_Key && (!p_OnlyAppliedLocally || Modification::IsLocal()(subItemDeletion)))
            return true;
    }
    return false;
}

bool GridModificationsO365::HasSubItemUpdate(const SubItemKey& p_Key, bool p_OnlyAppliedLocally) const
{
    for (const auto& subItemUpdate : m_SubItemUpdates)
    {
		if (subItemUpdate->GetKey() == p_Key && (!p_OnlyAppliedLocally || Modification::IsLocal()(subItemUpdate)))
            return true;
    }
    return false;
}

void GridModificationsO365::RemoveFinalStateModifications()
{
	GridFieldModifications<Scope::O365>::RemoveFinalStateModifications();
    RemoveFinalStateModificationsForParentRows(boost::none);
}

void GridModificationsO365::RemoveErrorModifications()
{
	GridFieldModifications<Scope::O365>::RemoveErrorModifications();
	m_RowModifications.erase(std::remove_if(m_RowModifications.begin(), m_RowModifications.end(), Modification::IsError()), m_RowModifications.end());
}

void GridModificationsO365::RemoveSentAndReceivedModifications()
{
	GridFieldModifications<Scope::O365>::RemoveSentAndReceivedModifications();
	m_RowModifications.erase(std::remove_if(m_RowModifications.begin(), m_RowModifications.end(), Modification::IsSentAndReceived()), m_RowModifications.end());
	m_SubItemDeletions.erase(std::remove_if(m_SubItemDeletions.begin(), m_SubItemDeletions.end(), Modification::IsSentAndReceived()), m_SubItemDeletions.end());
	m_SubItemUpdates.erase(std::remove_if(m_SubItemUpdates.begin(), m_SubItemUpdates.end(), Modification::IsSentAndReceived()), m_SubItemUpdates.end());
}

bool GridModificationsO365::HasModifications() const
{
    return GridFieldModifications<Scope::O365>::HasModifications()
		|| std::any_of(m_SubItemDeletions.begin(), m_SubItemDeletions.end(), Modification::IsLocal())
        || std::any_of(m_SubItemUpdates.begin(), m_SubItemUpdates.end(), Modification::IsLocal())
        || std::any_of(m_RowModifications.begin(), m_RowModifications.end(), Modification::IsLocal());
}

bool GridModificationsO365::HasSubItemModifications() const
{
	return std::any_of(m_SubItemDeletions.begin(), m_SubItemDeletions.end(), Modification::IsLocal())
		|| std::any_of(m_SubItemUpdates.begin(), m_SubItemUpdates.end(), Modification::IsLocal());
}

bool GridModificationsO365::HasSubItemResults() const
{
	return !m_SubItemDeletionErrors.empty() || !m_SubItemUpdateErrors.empty();
}

bool GridModificationsO365::IsModInSelectedRows(ISubItemModification* p_Mod)
{
    if (nullptr != p_Mod)
	{
		for (auto row : m_Grid.GetSelectedRows())
		{
			if (!row->IsGroupRow() && p_Mod->IsCorrespondingRow(row))
				return true;
		}
	}

    return false;
}

bool GridModificationsO365::RevertAll()
{
	bool reverted = RevertAllSubItemModifications();
	reverted = GridFieldModifications<Scope::O365>::RevertAll() || reverted;
	return reverted;
}

bool GridModificationsO365::Revert(const row_pk_t& rowKey, bool alsoRevertSubItems)
{
	const auto revertedSubItem	= alsoRevertSubItems && RevertSubItem(rowKey);
	const auto revertedRow		= RevertRowModifications(rowKey);
	ASSERT(!(revertedSubItem && revertedRow));
    return revertedRow || revertedSubItem;
}

bool GridModificationsO365::RevertSubItem(const row_pk_t& rowKey)
{
	bool revertedSubItemDeletions = false;
	GridBackendRow* row = m_Grid.GetRowIndex().GetExistingRow(rowKey);
	if (nullptr != row)
	{
		// Revert sub item deletions
		{
			const auto size = m_SubItemDeletions.size();

			for (auto& deletion : m_SubItemDeletions)
			{
                if (IMainItemModification::MustSurviveRefresh()(deletion) && deletion->IsCorrespondingRow(row))
					deletion->Revert();
			}

			m_SubItemDeletions.erase(std::remove_if(m_SubItemDeletions.begin(), m_SubItemDeletions.end()
				, [row](const std::unique_ptr<ISubItemDeletion>& p_Item)
			{
				return IMainItemModification::MustSurviveRefresh()(p_Item) && p_Item->IsCorrespondingRow(row);
			}), m_SubItemDeletions.end());

			revertedSubItemDeletions = size != m_SubItemDeletions.size();
		}
	}

	bool revertedSubItemUpdates = false;
	if (nullptr != row)
	{
		// Revert sub item updates
		{
			const auto size = m_SubItemUpdates.size();

			for (auto& update : m_SubItemUpdates)
			{
                if (IMainItemModification::MustSurviveRefresh()(update) && update->IsCorrespondingRow(row))
					update->Revert();
			}

			m_SubItemUpdates.erase(std::remove_if(m_SubItemUpdates.begin(), m_SubItemUpdates.end()
				, [row](const std::unique_ptr<RowSubItemFieldUpdates>& p_Item)
			{
				return IMainItemModification::MustSurviveRefresh()(p_Item) && p_Item->IsCorrespondingRow(row);
			}), m_SubItemUpdates.end());

			revertedSubItemUpdates = size != m_SubItemUpdates.size();
		}
	}

	return revertedSubItemDeletions || revertedSubItemUpdates;
}

bool GridModificationsO365::RemoveSubItem(const row_pk_t& rowKey)
{
	bool removedSubItemDeletions = false;
	GridBackendRow* row = m_Grid.GetRowIndex().GetExistingRow(rowKey);
	if (nullptr != row)
	{
		// Revert sub item deletions
		{
			const auto size = m_SubItemDeletions.size();

			m_SubItemDeletions.erase(std::remove_if(m_SubItemDeletions.begin(), m_SubItemDeletions.end()
				, [row](const std::unique_ptr<ISubItemDeletion>& p_Item)
			{
				return IMainItemModification::MustSurviveRefresh()(p_Item) && p_Item->IsCorrespondingRow(row);
			}), m_SubItemDeletions.end());

			removedSubItemDeletions = size != m_SubItemDeletions.size();
		}
	}

	bool removedSubItemUpdates = false;
	if (nullptr != row)
	{
		// Revert sub item updates
		{
			const auto size = m_SubItemUpdates.size();

			m_SubItemUpdates.erase(std::remove_if(m_SubItemUpdates.begin(), m_SubItemUpdates.end()
				, [row](const std::unique_ptr<RowSubItemFieldUpdates>& p_Item)
			{
				return IMainItemModification::MustSurviveRefresh()(p_Item) && p_Item->IsCorrespondingRow(row);
			}), m_SubItemUpdates.end());

			removedSubItemUpdates = size != m_SubItemUpdates.size();
		}
	}

	return removedSubItemDeletions || removedSubItemUpdates;
}

bool GridModificationsO365::RevertRowModifications(const row_pk_t& rowKey)
{
	bool revertedFieldModifications = GridFieldModifications<Scope::O365>::Revert(rowKey);

	bool revertedRowModifications = false;
	{
		const auto size = m_RowModifications.size();
		m_RowModifications.erase(std::remove_if(m_RowModifications.begin()
			, m_RowModifications.end()
			, [=](const std::unique_ptr<RowModification>& item)
			{
				return IMainItemModification::MustSurviveRefresh()(item) && item->IsCorrespondingRow(rowKey);
			})
			, m_RowModifications.end());

		revertedRowModifications = size != m_RowModifications.size();
	}

	return revertedFieldModifications || revertedRowModifications;
}

bool GridModificationsO365::RemoveRowModifications(const row_pk_t& rowKey, bool p_RevertOnDestruction)
{
	bool removedFieldModifications = GridFieldModifications<Scope::O365>::Revert(rowKey, p_RevertOnDestruction);
	bool removedRowModifications = false;
	{
		const auto size = m_RowModifications.size();
		m_RowModifications.erase(std::remove_if(m_RowModifications.begin()
			, m_RowModifications.end()
			, [=](const std::unique_ptr<RowModification>& item)
			{
				bool del = IMainItemModification::MustSurviveRefresh()(item) && item->IsCorrespondingRow(rowKey);
				if (del && !p_RevertOnDestruction)
					item->DontRevertOnDestruction();
				return del;
			})
			, m_RowModifications.end());

		removedRowModifications = size != m_RowModifications.size();
	}

	return removedFieldModifications || removedRowModifications;
}

void GridModificationsO365::SetSubItemDeletionsErrors(const std::map<SubItemKey, HttpResultWithError>& p_Errors)
{
    m_SubItemDeletionErrors = p_Errors;
}

void GridModificationsO365::SetSubItemUpdateErrors(const std::map<SubItemKey, HttpResultWithError>& p_Errors)
{
    m_SubItemUpdateErrors = p_Errors;
}

void GridModificationsO365::RemoveSubItemDeletionErrors()
{
    m_SubItemDeletions.erase(std::remove_if(m_SubItemDeletions.begin(), m_SubItemDeletions.end(), [](const std::unique_ptr<ISubItemDeletion>& p_Deletion) {
        return p_Deletion->GetState() == Modification::State::RemoteError;
    }), m_SubItemDeletions.end());
}

void GridModificationsO365::RemoveSubItemUpdateErrors()
{
    m_SubItemUpdates.erase(std::remove_if(m_SubItemUpdates.begin(), m_SubItemUpdates.end(), [](const std::unique_ptr<RowSubItemFieldUpdates>& p_Update) {
        return p_Update->GetState() == Modification::State::RemoteError;
    }), m_SubItemUpdates.end());
}

void GridModificationsO365::RevertSubItemDeletion(const SubItemKey& p_Key)
{
    bool found = false;
    for (auto itt = m_SubItemDeletions.begin(); itt != m_SubItemDeletions.end(); ++itt)
    {
        auto& subItemDel = *itt;
        if (subItemDel->GetKey() == p_Key)
        {
            subItemDel->Revert();
            m_SubItemDeletions.erase(itt);
            found = true;
            break;
        }
    }
    ASSERT(found);
}

void GridModificationsO365::RevertSubItemUpdate(const SubItemKey& p_Key)
{
    bool found = false;
    for (auto itt = m_SubItemUpdates.begin(); itt != m_SubItemUpdates.end(); ++itt)
    {
        auto& subItemUpdate = *itt;
        if (subItemUpdate->GetKey() == p_Key)
        {
            // Remove all updates from that row
            subItemUpdate->Revert();
            m_SubItemUpdates.erase(itt);
            found = true;
            break;
        }
    }
    ASSERT(found);
}

vector<RowModification*> GridModificationsO365::GetRowModifications(std::function<bool(RowModification*)> p_Criteria)
{
    vector<RowModification*> modifications;

    for (const auto& mod : m_RowModifications)
    {
        const auto& modPtr = mod.get();
        if (p_Criteria(modPtr))
            modifications.push_back(modPtr);
    }

    return modifications;
}

set<GridBackendRow*> GridModificationsO365::GetModifiedRows(bool p_OnlyAppliedLocally, bool p_IncludeSubItems) const
{
	auto rows = GridFieldModifications<Scope::O365>::GetModifiedRows(p_OnlyAppliedLocally, false);

	for (auto& mod : m_RowModifications)
	{
		if (!p_OnlyAppliedLocally || Modification::IsLocal()(mod))
		{
			auto row = m_Grid.GetRowIndex().GetExistingRow(mod->GetRowKey());
			ASSERT(nullptr != row);
			if (nullptr != row)
				rows.insert(row);
		}
	}

	// Also get explosion siblings, they are modified too!
	if (!rows.empty())
	{
		vector<GridBackendRow*> rowsCopy(rows.begin(), rows.end());
		vector<pair<GridBackendRow*, map<GridBackendColumn*, vector<GridBackendRow*>>>>	implosionSetup;// source row vs. col + exploded rows
		m_Grid.GetMultivalueManager().GetImplosionSetup(rowsCopy, implosionSetup);
		for (const auto& item1 : implosionSetup)
			for (const auto& item2 : item1.second)
				rows.insert(item2.second.begin(), item2.second.end());
	}

	// Get sub-item rows.
	if (p_IncludeSubItems && (!m_SubItemDeletions.empty() || !m_SubItemUpdates.empty()))
	{
		for (auto row : m_Grid.GetBackendRows())
		{
			bool done = false;
			for (auto& deletion : m_SubItemDeletions)
			{
				if ((!p_OnlyAppliedLocally || Modification::IsLocal()(deletion)) && deletion->IsCorrespondingRow(row))
				{
					rows.insert(row);
					done = true;
					break;
				}
			}

			if (!done)
			{
				for (auto& update : m_SubItemUpdates)
				{
					if ((!p_OnlyAppliedLocally || Modification::IsLocal()(update)) && update->IsCorrespondingRow(row))
					{
						rows.insert(row);
						break;
					}
				}
			}
		}
	}

	return rows;
}

set<GridBackendRow*> GridModificationsO365::GetCreatedRows(bool p_OnlyAppliedLocally) const
{
	set<GridBackendRow*> rows;

	for (auto& mod : m_RowModifications)
	{
		if (nullptr != dynamic_cast<CreatedObjectModification*>(mod.get())
			&& (!p_OnlyAppliedLocally || Modification::IsLocal()(mod)))
		{
			auto row = m_Grid.GetRowIndex().GetExistingRow(mod->GetRowKey());
			ASSERT(nullptr != row);
			if (nullptr != row)
				rows.insert(row);
		}
	}

	return rows;
}

bool GridModificationsO365::IsModified(GridBackendRow* p_Row) const
{
	auto testRowMod = [p_Row, this](const auto& p_Modification)
	{
		// When multivalue explosion occurs after row edit, the row PK is not accurate anymore in this modification
		// because the explosion adds columns to the grid PK.
		// If p_Row was added by an explosion, its parent row modification is its modification too 
		auto tsrRow = m_Grid.GetMultivalueManager().GetTopSourceRow(p_Row);
		auto modRow = m_Grid.GetRowIndex().GetExistingRow(p_Modification->GetRowKey());
		return p_Modification->GetState() == Modification::State::AppliedLocally && (modRow == p_Row || modRow == tsrRow);
	};

	auto testSubItemMod = [p_Row, this](const auto& p_SubItemModification)
	{
		return p_SubItemModification->GetState() == Modification::State::AppliedLocally && p_SubItemModification->IsCorrespondingRow(p_Row);
	};

	return GridFieldModifications<Scope::O365>::IsModified(p_Row)
		|| std::any_of(m_RowModifications.begin(), m_RowModifications.end(), testRowMod)
		|| std::any_of(m_SubItemDeletions.begin(), m_SubItemDeletions.end(), testSubItemMod)
		|| std::any_of(m_SubItemUpdates.begin(), m_SubItemUpdates.end(), testSubItemMod);
}

bool GridModificationsO365::HasAnyRowModificationOnOrUnder(GridBackendRow* p_TopAncestorRow) const
{
	auto testRowMod = [p_TopAncestorRow, this](const auto& p_Modification)
	{
		auto row = m_Grid.GetRowIndex().GetExistingRow(p_Modification->GetRowKey());
		return p_Modification->GetState() == Modification::State::AppliedLocally && nullptr != row && row->GetTopAncestorOrThis() == p_TopAncestorRow;
	};

	auto children = (!m_SubItemDeletions.empty() || !m_SubItemUpdates.empty()) ? m_Grid.GetChildRows(p_TopAncestorRow, false) : vector<GridBackendRow*>{};
	auto testRowSubItemMod = [p_TopAncestorRow, &children, this](const auto& p_SubItemModification)
	{
		return p_SubItemModification->GetState() == Modification::State::AppliedLocally
			&& (p_SubItemModification->IsCorrespondingRow(p_TopAncestorRow)
				|| std::any_of(children.begin(), children.end(), [&p_SubItemModification](auto row)
					{
						return p_SubItemModification->IsCorrespondingRow(row);
					})
				);
	};

	return  GridFieldModifications<Scope::O365>::HasAnyRowModificationOnOrUnder(p_TopAncestorRow)
		|| std::any_of(m_RowModifications.begin(), m_RowModifications.end(), testRowMod)
		|| std::any_of(m_SubItemDeletions.begin(), m_SubItemDeletions.end(), testRowSubItemMod)
		|| std::any_of(m_SubItemUpdates.begin(), m_SubItemUpdates.end(), testRowSubItemMod);
}

void GridModificationsO365::RemoveFinalStateModificationsForParentRows(const boost::YOpt<set<GridBackendRow*>>& p_AncestorRows)
{
	if (p_AncestorRows && p_AncestorRows->empty())
		return;

	GridFieldModifications<Scope::O365>::RemoveFinalStateModificationsForParentRows(p_AncestorRows);

	// Row modifications
	for (auto it = m_RowModifications.begin(); it != m_RowModifications.end();)
	{
		if (Modification::MustSurviveRefresh()(*it))
		{
			GridBackendRow* modRow = p_AncestorRows ? m_Grid.GetRowIndex().GetExistingRow((*it)->GetRowKey()) : nullptr;
			if (!p_AncestorRows || nullptr != modRow && p_AncestorRows->find(modRow->GetTopAncestorOrThis()) != p_AncestorRows->end())
			{
				it = m_RowModifications.erase(it);
				continue;
			}
		}
		++it;
	}

	// Sub item deletions
	for (auto it = m_SubItemDeletions.begin(); it != m_SubItemDeletions.end();)
	{
		if (!Modification::IsError()(*it) && Modification::MustSurviveRefresh()(*it))
		{
			(*it)->Revert();
			it = m_SubItemDeletions.erase(it);
			continue;
		}
		++it;
	}

	// Sub items
	for (auto row : m_Grid.GetBackendRows())
	{
		if (!p_AncestorRows || p_AncestorRows->find(row->GetTopAncestorOrThis()) != p_AncestorRows->end())
		{
			// Error sub item deletions
			for (auto it = m_SubItemDeletions.begin(); it != m_SubItemDeletions.end();)
			{
				if (Modification::MustSurviveRefresh()(*it) && (*it)->IsCorrespondingRow(row))
				{
					ASSERT(Modification::IsError()(*it));
					(*it)->Revert(row);
					it = m_SubItemDeletions.erase(it);
					continue;
				}
				++it;
			}

			// Sub item updates
			for (auto it = m_SubItemUpdates.begin(); it != m_SubItemUpdates.end();)
			{
				if (Modification::MustSurviveRefresh()(*it) && (*it)->IsCorrespondingRow(row))
				{
					(*it)->Revert(row);
					it = m_SubItemUpdates.erase(it);
					continue;
				}
				++it;
			}
		}
	}
}

RowModification* GridModificationsO365::add(std::unique_ptr<RowModification> p_RowModification)
{
	ASSERT(p_RowModification);
	if (p_RowModification)
	{
		{
			auto it = std::find_if(m_RowModifications.begin(), m_RowModifications.end()
				, [&p_RowModification](const std::unique_ptr<RowModification>& item)
			{
				return IMainItemModification::MustSurviveRefresh()(item) && p_RowModification->IsCorrespondingRow(item->GetRowKey());
			});

			if (m_RowModifications.end() != it)
			{
				if (IMainItemModification::IsSent()(*it))
				{
					(*it)->DontRevertOnDestruction();
					m_RowModifications.erase(it);
				}
				else
				{
					ASSERT(false);
				}
			}
		}

		// Be caredul we modify base class here
		{
			auto it = std::find_if(m_RowFieldModifications.begin(), m_RowFieldModifications.end()
				, [&p_RowModification](const auto& item)
			{
				return IMainItemModification::MustSurviveRefresh()(item) && p_RowModification->IsCorrespondingRow(item->GetRowKey());
			});

			if (m_RowFieldModifications.end() != it)
			{
				if (IMainItemModification::IsSent()(*it))
				{
					(*it)->DontRevertOnDestruction();
					m_RowFieldModifications.erase(it);
				}
				else
				{
					ASSERT(false);
				}
			}
		}

		m_RowModifications.push_back(std::move(p_RowModification));
		return m_RowModifications.back().get();
	}

	return nullptr;
}

bool GridModificationsO365::RevertAllSubItemModifications()
{
	bool hadModifications = !m_SubItemDeletions.empty() || !m_SubItemUpdates.empty();

	for (auto row : m_Grid.GetBackendRows())
	{
		for (auto& deletion : m_SubItemDeletions)
		{
			if (deletion->IsCorrespondingRow(row))
				deletion->Revert(row);
		}

		for (auto& update : m_SubItemUpdates)
		{
			if (update->IsCorrespondingRow(row))
				update->Revert(row);
		}
	}
	m_SubItemDeletions.clear();
	m_SubItemUpdates.clear();

	return hadModifications;
}

bool GridModificationsO365::RevertAllRowModifications()
{
	bool ret = GridFieldModifications<Scope::O365>::RevertAllRowModifications()
			|| !m_RowModifications.empty();

	m_RowModifications.clear();

	return ret;
}

void GridModificationsO365::SetModsWithRequestFeedbackProvider(std::unique_ptr<IModificationWithRequestFeedbackProvider> p_Provider)
{
	m_ModsRequestFeedbackProvider = std::move(p_Provider);
}

// Explicit template instantiation
template GridFieldModifications<Scope::O365>;
template GridFieldModifications<Scope::ONPREM>;