#pragma once

#include "CheckoutStatus.h"
#include "IRequester.h"
#include "IRequestLogger.h"

class ListItemsFieldsForCheckoutStatusDeserializer;

class CheckoutStatusRequester : public IRequester
{
public:
	CheckoutStatusRequester(wstring p_DriveId, wstring p_ItemId, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const CheckoutStatus& GetCheckoutStatus() const;

private:
	wstring m_DriveId;
	wstring m_ItemId;

	std::shared_ptr<ListItemsFieldsForCheckoutStatusDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
};
