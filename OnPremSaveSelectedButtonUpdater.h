#pragma once

#include "IButtonUpdater.h"

template<class FrameClass>
class OnPremSaveSelectedButtonUpdater : public IButtonUpdater
{
public:
	OnPremSaveSelectedButtonUpdater(FrameClass& p_Frame);
	void Update(CCmdUI* p_CmdUi) override;

private:
	FrameClass& m_Frame;
};

template<class FrameClass>
OnPremSaveSelectedButtonUpdater<FrameClass>::OnPremSaveSelectedButtonUpdater(FrameClass& p_Frame)
	: m_Frame(p_Frame)
{

}

template<class FrameClass>
void OnPremSaveSelectedButtonUpdater<FrameClass>::Update(CCmdUI* p_CmdUi)
{
	p_CmdUi->Enable(m_Frame.IsConnected()
		&& m_Frame.HasNoTaskRunning()
		&& (m_Frame.hasModificationsPending(true) || m_Frame.hasOnPremiseChanges(true))
	);
}

