#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Package.h"

class PackageDeserializer : public JsonObjectDeserializer, public Encapsulate<Package>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

