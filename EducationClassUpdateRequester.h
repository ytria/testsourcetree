#pragma once

#include "IRequester.h"
#include "EducationClass.h"

class EducationClassUpdateRequester : public IRequester
{
public:
	EducationClassUpdateRequester(const wstring& p_ClassId, const web::json::value& p_ClassChanges);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const HttpResultWithError& GetResult() const;

private:
	std::shared_ptr<HttpResultWithError> m_Result;

	wstring m_ClassId;
	web::json::value m_ClassChanges;
};

