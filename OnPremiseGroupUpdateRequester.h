#pragma once

#include "IRequester.h"
#include "InvokeResultWrapper.h"
#include "OnPremiseGroup.h"

class IRequestLogger;

class OnPremiseGroupUpdateRequester : public IRequester
{
public:
	OnPremiseGroupUpdateRequester(const boost::YOpt<OnPremiseGroup>& p_Group = boost::none);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	std::shared_ptr<InvokeResultWrapper> GetResult();

	// useful?
	//void SetGroup(const OnPremiseGroup& p_OnPremiseGroup);

	void SetAuthType(const wstring& p_AuthType);
	void SetPartition(const wstring& p_Partition);

private:
	std::shared_ptr<InvokeResultWrapper> m_Result;
	//std::shared_ptr<IRequestLogger> m_Logger;
	std::shared_ptr<IPSObjectCollectionDeserializer> m_Deserializer;

	boost::YOpt<OnPremiseGroup> m_Group;
	boost::YOpt<wstring> m_AuthType;
	boost::YOpt<wstring> m_Partition;
};

