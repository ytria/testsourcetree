#pragma once

#include "IRequester.h"

class DumpDeserializer;
class SingleRequestResult;

namespace Cosmos
{
    class CreateDbRequester : public IRequester
    {
    public:
        CreateDbRequester(const wstring& p_DatabaseName);
        TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

        const wstring& GetData() const;
        const SingleRequestResult& GetResult() const;

        std::shared_ptr<DumpDeserializer> m_Deserializer;
        std::shared_ptr<SingleRequestResult> m_Result;

    private:
        wstring m_DbName;
    };
}
