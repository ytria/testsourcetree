#pragma once

#include "IRequester.h"
#include "Application.h"
#include "PaginatedRequestResults.h"
#include "IPageRequestLogger.h"

template <class T, class U>
class ValueListDeserializer;

class ApplicationDeserializer;

class ApplicationListRequester : public IRequester
{
public:
	ApplicationListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const PaginatedRequestResults& GetResult() const;
	const vector<Application>& GetData() const;

	static Application GetAppFromAppId(const std::shared_ptr<Sapio365Session>& p_Session, const wstring& p_AppId);

private:
	std::shared_ptr<IPageRequestLogger> m_Logger;
	std::shared_ptr<PaginatedRequestResults> m_Results;
	std::shared_ptr<ValueListDeserializer<Application, ApplicationDeserializer>> m_Deserializer;
};

