#include "ChannelMessagesRequester.h"

#include "BusinessChatMessage.h"
#include "ChatMessageDeserializer.h"
#include "GraphPageRequester.h"
#include "MSGraphCommonData.h"
#include "MsGraphPaginator.h"
#include "O365AdminUtil.h"
#include "RunOnScopeEnd.h"
#include "safeTaskCall.h"
#include "Sapio365Session.h"
#include "ValueListDeserializer.h"
#include "RESTUtils.h"
#include "MsGraphHttpRequestLogger.h"
#include "MsGraphPageRequesterRetryer.h"

using namespace Rest;

ChannelMessagesRequester::ChannelMessagesRequester(PooledString p_TeamId, PooledString p_ChannelId, const std::shared_ptr<IPageRequestLogger>& p_Logger)
    : m_TeamId(p_TeamId)
    , m_ChannelId(p_ChannelId)
    , m_Logger(p_Logger)
    , m_SessionMode(Sapio365Session::SessionType::USER_SESSION)
{
}

TaskWrapper<void> ChannelMessagesRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<BusinessChatMessage, ChatMessageDeserializer>>();

	web::uri_builder uri;
	uri.append_path(TEAMS_PATH);
	uri.append_path(m_TeamId);
	uri.append_path(CHANNELS_PATH);
	uri.append_path(m_ChannelId);
	uri.append_path(MESSAGES_PATH);

    auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(m_SessionMode, p_Session->GetIdentifier());
	auto paginatorReq = std::make_shared<GraphPageRequester>(m_Deserializer, nullptr, httpLogger);
	paginatorReq->SetUseBetaEndpoint(true);

	MsGraphPaginator paginator(uri.to_uri(), std::make_shared<MsGraphPageRequesterRetryer>(paginatorReq, MsGraphPageRequesterRetryer::DefaultAskIfRetry(), p_TaskData.GetOriginator()), m_Logger, 100);

    return paginator.Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData).Then([httpLogger, logger = m_Logger, deserializer = m_Deserializer, teamId = m_TeamId, channelId = m_ChannelId, p_Session, p_TaskData]() {
        auto& chatMessages = deserializer->GetData();

		RunOnScopeEnd _([objName = logger->GetLogMessage(), logger]() { logger->SetLogMessage(objName); });
        
		wstring nbMessages = std::to_wstring(chatMessages.size());
		size_t curMessage = 1;

		for (auto& chatMessage : chatMessages)
        {
            if (p_TaskData.IsCanceled())
                break;
			logger->SetLogMessage(_YFORMAT(L"%s/%s chat message replies", std::to_wstring(curMessage).c_str(), nbMessages.c_str()));
            auto repliesDeserializer = std::make_shared<ValueListDeserializer<BusinessChatMessage, ChatMessageDeserializer>>();
            auto replies = safeTaskCall(p_Session->GetMSGraphSession(httpLogger->GetSessionType())->GetChatMessageReplies(repliesDeserializer, teamId, channelId, chatMessage.GetID(), {}, logger, httpLogger, p_TaskData).Then([repliesDeserializer]
            {
                return repliesDeserializer->GetData();
            }), p_Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::ListErrorHandler<BusinessChatMessage>, p_TaskData).get();
            chatMessage.SetReplies(replies);
			++curMessage;
        }
    });
}

const vector<BusinessChatMessage>& ChannelMessagesRequester::GetData() const
{
    return m_Deserializer->GetData();
}

void ChannelMessagesRequester::SetSessionMode(Sapio365Session::SessionType p_SessionMode)
{
    m_SessionMode = p_SessionMode;
}
