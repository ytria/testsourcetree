#pragma once
#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ImplicitGrantSettings.h"

class ImplicitGrantSettingsDeserializer : public JsonObjectDeserializer, public Encapsulate<ImplicitGrantSettings>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

