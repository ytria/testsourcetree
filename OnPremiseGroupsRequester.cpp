#include "OnPremiseGroupsRequester.h"

#include "IPSObjectCollectionDeserializer.h"
#include "Sapio365Session.h"

OnPremiseGroupsRequester::OnPremiseGroupsRequester(const std::shared_ptr<IPSObjectCollectionDeserializer>& p_Deserializer, const std::shared_ptr<IRequestLogger>& p_RequestLogger)
	: m_Deserializer(p_Deserializer),
	m_Logger(p_RequestLogger),
	m_Result(std::make_shared<InvokeResultWrapper>())
{
	if (!m_Deserializer)
		throw std::exception("OnPremiseGroupsRequester: Deserializer cannot be nullptr");
}

TaskWrapper<void> OnPremiseGroupsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	return YSafeCreateTaskMutable([result = m_Result, deserializer = m_Deserializer, session = p_Session->GetBasicPowershellSession(), logger = m_Logger, taskData = p_TaskData]() {
		if (nullptr != session)
		{
			logger->Log(taskData.GetOriginator());

			session->AddScript(_YTEXT("Get-ADGroup -Filter * -Properties *"), taskData.GetOriginator());

			result->SetResult(session->Invoke(taskData.GetOriginator()));
			if (result->Get().m_Success && nullptr != result->Get().m_Collection)
				result->Get().m_Collection->Deserialize(*deserializer);
		}
	});
}

const std::shared_ptr<InvokeResultWrapper>& OnPremiseGroupsRequester::GetResult() const
{
	return m_Result;
}
