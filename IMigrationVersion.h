#pragma once

#include "Migration.h"

struct VersionSourceTarget
{
	VersionSourceTarget(int p_SourceVersion, int p_TargetVersion)
		: m_SourceVersion(p_SourceVersion), m_TargetVersion(p_TargetVersion)
	{}
	bool operator==(const VersionSourceTarget& p_Other);

	int m_SourceVersion;
	int m_TargetVersion;
};

class IMigrationVersion
{
public:
	virtual ~IMigrationVersion() = default;
	virtual void Build() = 0;
	virtual VersionSourceTarget GetVersionInfo() const = 0;

	bool Apply();

protected:
	void AddStep(const Migration::StepType& p_Step);

private:
	Migration m_Migration;
};

