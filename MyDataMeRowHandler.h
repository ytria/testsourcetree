#pragma once

#include "GridBackendField.h"

class CacheGrid;
class GridBackendRow;

class MyDataMeRowHandler
{
public:
	void GetRidOfMeRow(CacheGrid& p_Grid, GridBackendRow* p_Row);
	GridBackendField& GetField(GridBackendColumn* p_Col);
	GridBackendField& GetField(UINT p_ColID);
	const GridBackendField& GetField(GridBackendColumn* p_Col) const;
	const GridBackendField& GetField(UINT p_ColID) const;
	bool HasField(GridBackendColumn* p_Col) const;
	bool HasField(UINT p_ColID) const;

private:
	std::map<UINT, GridBackendField> m_Fields;
};