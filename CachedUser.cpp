#include "CachedUser.h"

#include "BusinessUser.h"
#include "MsGraphFieldNames.h"

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<CachedUser>("CachedUser") (metadata(MetadataKeys::TypeDisplayName, _YTEXT("Cached User")))
		.constructor()(policy::ctor::as_object)
		.property(O365_USER_DISPLAYNAME, &CachedUser::m_DisplayName)
		.property(O365_USER_USERPRINCIPALNAME, &CachedUser::m_UserPrincipalName)
		.property(O365_USER_USERTYPE, &CachedUser::m_UserType)
		.property(O365_USER_DELETEDDATETIME, &CachedUser::m_DeletedDateTime);
}

CachedUser::CachedUser(const User& user)
	: CachedObject(user.m_Id)
	, m_DisplayName(user.DisplayName ? *user.DisplayName : PooledString(_YTEXT("")))
	, m_UserPrincipalName(user.UserPrincipalName ? *user.UserPrincipalName : PooledString(_YTEXT("")))
	, m_UserType(user.UserType ? user.UserType : PooledString(_YTEXT("")))
	, m_Mail(user.Mail ? user.Mail : PooledString(_YTEXT("")))
{
	setDeletedDateTime(user.DeletedDateTime);
}

CachedUser::CachedUser(const BusinessUser& p_User)
	: CachedObject(p_User.m_Id)
    , m_DisplayName(p_User.GetDisplayName())
    , m_UserPrincipalName(p_User.GetUserPrincipalName())
    , m_UserType(p_User.GetUserType())
	, m_DeletedDateTime(p_User.GetDeletedDateTime())
	, m_Mail(p_User.GetMail())
{
	SetRBACDelegationID(p_User.GetRBACDelegationID());
}

PooledString CachedUser::GetDisplayName() const
{
	return m_DisplayName ? m_DisplayName.get() : _YTEXT("");
}

PooledString CachedUser::GetUserPrincipalName() const
{
    return m_UserPrincipalName ? m_UserPrincipalName.get() : _YTEXT("");
}

PooledString CachedUser::GetUserType() const
{
	return m_UserType ? m_UserType.get() : _YTEXT("");
}

const boost::YOpt<YTimeDate>& CachedUser::GetDeletedDateTime() const
{
	return m_DeletedDateTime;
}

void CachedUser::setDeletedDateTime(const boost::YOpt<PooledString>& p_DelDate)
{
	if (p_DelDate.is_initialized())
	{
		YTimeDate td;
		if (TimeUtil::GetInstance().ConvertTextToDate(p_DelDate.get(), td))
			m_DeletedDateTime = td;
	}
}

PooledString CachedUser::GetMail() const
{
	return m_Mail ? m_Mail.get() : _YTEXT("");
}

bool CachedUser::IsEmailPresent() const
{
	return !GetMail().IsEmpty();
}