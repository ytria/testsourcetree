#pragma once

#include "IJsonSerializer.h"
#include "OnPremisesExtensionAttributes.h"

class OnPremisesExtensionAttributesSerializer : public IJsonSerializer
{
public:
    OnPremisesExtensionAttributesSerializer(const OnPremisesExtensionAttributes& p_OnPremExtAttr);
    void Serialize() override;

private:
    const OnPremisesExtensionAttributes& m_OnPremExtAttr;
};

