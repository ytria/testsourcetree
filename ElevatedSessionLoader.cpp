#include "ElevatedSessionLoader.h"

#include "AdvancedSessionLoader.h"
#include "AzureADOAuth2BrowserAuthenticator.h"
#include "DbSessionPersister.h"
#include "DlgBrowserOpener.h"
#include "DlgSpecialExtAdminAccess.h"
#include "DlgSpecialExtAdminManageElevatedJsonGenerator.h"
#include "MainFrame.h"
#include "Messages.h"
#include "MSGraphCommonData.h"
#include "O365AdminUtil.h"
#include "Office365Admin.h"
#include "QueryDeleteSession.h"
#include "SessionIdsEmitter.h"
#include "SessionSwitcher.h"
#include "SessionTypes.h"

using namespace std::literals::string_literals;

ElevatedSessionLoader::ElevatedSessionLoader(const SessionIdentifier& p_Id)
	: ElevatedSessionLoader(p_Id, false)
{
}

ElevatedSessionLoader::ElevatedSessionLoader(const SessionIdentifier& p_Id, bool p_Partner)
	: ISessionLoader(p_Id)
	, m_Partner(p_Partner)
{
	ASSERT(!p_Partner && p_Id.m_SessionType == SessionTypes::g_ElevatedSession
			|| p_Partner && p_Id.m_SessionType == SessionTypes::g_PartnerElevatedSession);
}

TaskWrapper<std::shared_ptr<Sapio365Session>> ElevatedSessionLoader::Load(const std::shared_ptr<Sapio365Session>& p_SapioSession)
{
	TaskWrapper<std::shared_ptr<Sapio365Session>> result = pplx::task_from_result(std::shared_ptr<Sapio365Session>());

	PersistentSession persistentSession = GetPersistentSession(p_SapioSession);
	ASSERT(persistentSession.GetIdentifier() == GetSessionId());

	auto& app = Office365AdminApp::Get();
	app.SetSessionBeingLoadedIdentifier(GetSessionId());

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(app.m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr == mainFrame)
		return result;

	{
		auto authorizationAuthenticator = std::make_unique<AzureADOAuth2BrowserAuthenticator>(p_SapioSession->GetMainMSGraphSession(),
			Rest::GetMsGraphEndpoint(),
			_YTEXT(""),
			SessionTypes::GetAppId(persistentSession.GetSessionType()),
			SessionTypes::GetUserRedirectUri(persistentSession.GetSessionType()),
			std::make_unique<DlgBrowserOpener>(),
			m_Partner ? persistentSession.GetTenantName() : _YTEXT("common")
			);
		p_SapioSession->GetMainMSGraphSession()->SetAuthenticator(std::move(authorizationAuthenticator));
	}

	p_SapioSession->GetMainMSGraphSession()->SetSessionPersister(std::make_shared<DbSessionPersister>(persistentSession));

	bool isLoggedIn = p_SapioSession->GetMainMSGraphSession()->LoadSession(false); // Do not send WM_READY_TO_TEST_AUTHENTICATION
	if (isLoggedIn)
	{
		p_SapioSession->SetIdentifier(persistentSession.GetIdentifier());
		auto prepare = app.TestElevatedSession(p_SapioSession, *mainFrame, persistentSession, false);
		if (!prepare.first)
		{
			Util::ShowApplicationConnectionError(_T("Unable to open the application."), prepare.second, mainFrame);

			bool done = false;
			while (!done)
			{
				DlgSpecialExtAdminAccess dlg(persistentSession.GetTenantName(), persistentSession.GetAppId(), persistentSession.GetAppClientSecret(), persistentSession.GetSessionName(), mainFrame, mainFrame->GetSapio365Session(), std::make_unique<DlgSpecialExtAdminManageElevatedJsonGenerator>(mainFrame->GetSapio365Session()), true);
				dlg.SetTitle(_T("Manage Elevated Privileges"));
				dlg.SetAdditionalHeight(190);
				if (dlg.DoModal() == IDOK)
				{
					persistentSession.SetSessionName(dlg.GetDisplayName());
					persistentSession.SetTenantName(dlg.GetTenant());
					persistentSession.SetAppId(dlg.GetAppId());
					persistentSession.SetAppClientSecret(dlg.GetClientSecret());
					p_SapioSession->GetMainMSGraphSession()->SetSessionPersister(std::make_shared<DbSessionPersister>(persistentSession));
					auto prepareResult = app.TestElevatedSession(p_SapioSession, *mainFrame, persistentSession, true);
					if (prepareResult.first)
					{
						SessionsSqlEngine::Get().Save(persistentSession);
						done = true;
						app.SetSessionType(SessionTypes::g_ElevatedSession);
						result = pplx::task_from_result(p_SapioSession);
						// Will send WM_AUTHENTICATION_SUCCESS
						mainFrame->SendMessage(WM_READY_TO_TEST_AUTHENTICATION, (WPARAM) new wstring(p_SapioSession->GetMainMSGraphSession()->GetName()), 0);
						TraceIntoFile::trace(_YDUMP("[["s) + SessionIdsEmitter(persistentSession.GetIdentifier()).GetId() + _YDUMP("]]") + _YDUMP("Office365AdminApp"), _YDUMP("LoadSession"), _YDUMPFORMAT(L"Successfully loaded %s session: %s", app.GetSessionType().c_str(), persistentSession.GetIdentifier().m_EmailOrAppId.c_str()));
					}
					else
					{
						Util::ShowApplicationConnectionError(_T("Unable to open the application."), prepareResult.second, mainFrame);
					}
				}
				else
				{
					done = true;

					auto oldSessionId = persistentSession.GetIdentifier();
					persistentSession.SetSessionType(SessionTypes::g_AdvancedSession);
					SessionsSqlEngine::Get().Save(persistentSession);

					QueryDeleteSession queryDelete(SessionsSqlEngine::Get(), oldSessionId);
					queryDelete.Run();

					// Fallback on regular admin session
					SessionIdentifier newId = GetSessionId();
					newId.m_SessionType = SessionTypes::g_AdvancedSession;
					SessionSwitcher switcher(std::make_unique<AdvancedSessionLoader>(newId));
					result = switcher.Switch();
				}
			}
		}
		else
		{
			app.SetSessionType(GetSessionId().m_SessionType);
			result = pplx::task_from_result(p_SapioSession);
			mainFrame->SendMessage(WM_READY_TO_TEST_AUTHENTICATION, (WPARAM) new wstring(p_SapioSession->GetMainMSGraphSession()->GetName()), 0);
			TraceIntoFile::trace(_YDUMP("[["s) + SessionIdsEmitter(persistentSession.GetIdentifier()).GetId() + _YDUMP("]]") + _YDUMP("Office365AdminApp"), _YDUMP("LoadSession"), _YDUMPFORMAT(L"Successfully loaded %s session: %s", GetSessionId().m_SessionType.c_str(), GetSessionId().m_EmailOrAppId.c_str()));
		}
	}
	else
	{
		result = app.RelogElevatedSession(p_SapioSession, persistentSession, persistentSession.GetIdentifier(), false, mainFrame);
	}

	return result;
}
