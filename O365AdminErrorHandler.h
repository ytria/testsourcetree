#pragma once

#include "IRestErrorHandler.h"

class RestSession;
class Sapio365Session;

class O365AdminErrorHandler : public IRestErrorHandler
{
public:
    O365AdminErrorHandler(const std::shared_ptr<RestSession>& p_Session);
    O365AdminErrorHandler(const std::shared_ptr<RestSession>& p_Session, const std::shared_ptr<Sapio365Session>& p_sapioSession);
    virtual ~O365AdminErrorHandler() = default;

    virtual wstring HandleRestException(const RestException& e, HWND p_Originator) noexcept override;
    virtual wstring HandleHttpException(const web::http::http_exception& e, HWND p_Originator) noexcept override;
    virtual wstring HandleOAuth2Exception(const web::http::oauth2::experimental::oauth2_exception& e, YtriaTaskData p_TaskData) noexcept override;
    virtual wstring HandleTaskCanceledException(const pplx::task_canceled& e, HWND p_Originator) noexcept override;
    virtual wstring HandleStdException(const std::exception& e, HWND p_Originator) noexcept override;
    virtual wstring HandleUnknownException(HWND p_Originator) noexcept override;

private:
    std::shared_ptr<RestSession> m_Session;

	// As O365AdminErrorHandler is a unique_ptr in Sapio365Session, we can't store the session's shared_ptr
	// without creating a circular dependency 
	//std::shared_ptr<Sapio365Session> m_Sapio365Session;
	std::weak_ptr<Sapio365Session> m_Sapio365Session;
	std::shared_ptr<Sapio365Session> getSapio365Session() const;
};
