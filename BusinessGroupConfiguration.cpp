#include "BusinessGroupConfiguration.h"

#include "MsGraphFieldNames.h"
#include "YtriaFieldsConstants.h"

wstring BusinessGroupConfiguration::g_Unlicensed;

BusinessGroupConfiguration::BusinessGroupConfiguration()
{
	m_YUIDandTitles[_YUID(O365_GROUP_DISPLAYNAME)]			= YtriaTranslate::Do(GridGroups_customizeGrid_1, _YLOC("Group Display Name"));
	m_YUIDandTitles[_YUID("additionalInfoStatus")]			= YtriaTranslate::Do(GridGroups_customizeGrid_2, _YLOC("Is Loaded"));
	m_YUIDandTitles[_YUID(O365_GROUP_ISTEAM)]				= YtriaTranslate::Do(GridGroups_customizeGrid_3, _YLOC("Is a Team"));
	m_YUIDandTitles[_YUID(O365_GROUP_GROUPTYPE)]			= YtriaTranslate::Do(GridGroups_customizeGrid_4, _YLOC("Group Type"));
	m_YUIDandTitles[_YUID(O365_GROUP_DYNAMICMEMBERSHIP)]	= YtriaTranslate::Do(GridGroups_customizeGrid_56, _YLOC("Dynamic Membership"));
	m_YUIDandTitles[_YUID(O365_GROUP_DESCRIPTION)]			= YtriaTranslate::Do(GridGroups_customizeGrid_5, _YLOC("Description"));
	m_YUIDandTitles[_YUID(O365_GROUP_VISIBILITY)]			= YtriaTranslate::Do(GridGroups_customizeGrid_6, _YLOC("Privacy"));
	m_YUIDandTitles[_YUID(O365_GROUP_CREATEDDATETIME)]		= YtriaTranslate::Do(GridGroups_customizeGrid_7, _YLOC("Created On"));
	m_YUIDandTitles[_YUID(O365_GROUP_DELETEDDATETIME)]		= YtriaTranslate::Do(GridGroupsRecycleBin_customizeGrid_7, _YLOC("Deleted On"));

	m_YUIDandTitles[_YUID(O365_GROUP_MAILNICKNAME)]	= YtriaTranslate::Do(GridGroups_customizeGrid_10, _YLOC("Email nickname"));
	m_YUIDandTitles[_YUID(O365_GROUP_MAIL)]			= YtriaTranslate::Do(GridGroups_customizeGrid_11, _YLOC("Email"));
	m_YUIDandTitles[_YUID("memberCount")]			= YtriaTranslate::Do(GridGroups_customizeGrid_61, _YLOC("Members"));

	m_YUIDandTitles[_YUID(O365_GROUP_CLASSIFICATION)]				= YtriaTranslate::Do(GridGroups_customizeGrid_12, _YLOC("Classification"));
	m_YUIDandTitles[_YUID(O365_GROUP_PROXYADDRESSES)]				= YtriaTranslate::Do(GridGroups_customizeGrid_13, _YLOC("SMTP Address"));
	m_YUIDandTitles[_YUID(O365_GROUP_RESOURCEBEHAVIOROPTIONS)]		= YtriaTranslate::Do(GridGroups_customizeGrid_59, _YLOC("Resource Behavior Options"));
	m_YUIDandTitles[_YUID(O365_GROUP_RESOURCEPROVISIONINGOPTIONS)]	= YtriaTranslate::Do(GridGroups_customizeGrid_60, _YLOC("Resource Provisioning Options"));
	m_YUIDandTitles[_YUID(O365_GROUP_PREFERREDDATALOCATION)]		= YtriaTranslate::Do(GridGroups_customizeGrid_62, _YLOC("Preferred Data Location"));

	m_YUIDandTitles[_YUID(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE)]	= YtriaTranslate::Do(GridGroups_customizeGrid_63, _YLOC("Membership Rule Processing State"));
	m_YUIDandTitles[_YUID(O365_GROUP_MEMBERSHIPRULE)]					= YtriaTranslate::Do(GridGroups_customizeGrid_64, _YLOC("Membership Rule"));
	m_YUIDandTitles[_YUID(O365_GROUP_THEME)]							= YtriaTranslate::Do(GridGroups_customizeGrid_65, _YLOC("Theme"));
	m_YUIDandTitles[_YUID(O365_GROUP_PREFERREDLANGUAGE)]				= YtriaTranslate::Do(GridGroups_customizeGrid_66, _YLOC("Preferred Language"));

	m_YUIDandTitles[_YUID(O365_GROUP_ONPREMISESLASTSYNCDATETIME)]		= YtriaTranslate::Do(GridGroups_customizeGrid_15, _YLOC("Last Sync - On-Premises"));
	m_YUIDandTitles[_YUID(O365_GROUP_ONPREMISESSECURITYIDENTIFIER)]		= YtriaTranslate::Do(GridGroups_customizeGrid_16, _YLOC("Security ID - On-Premises"));
	m_YUIDandTitles[_YUID(O365_GROUP_ONPREMISESSYNCENABLED)]			= YtriaTranslate::Do(GridGroups_customizeGrid_17, _YLOC("Sync Enabled - On-Premises"));

	m_YUIDandTitles[_YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY)]				= YtriaTranslate::Do(GridGroups_customizeGrid_67, _YLOC("Category of Prov. Error"));
	m_YUIDandTitles[_YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME)]		= YtriaTranslate::Do(GridGroups_customizeGrid_68, _YLOC("Date of Prov. Error"));
	m_YUIDandTitles[_YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR)]	= YtriaTranslate::Do(GridGroups_customizeGrid_69, _YLOC("Property causing Prov. Error"));
	m_YUIDandTitles[_YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE)]					= YtriaTranslate::Do(GridGroups_customizeGrid_70, _YLOC("Value of property causing Prov. Error"));

	m_YUIDandTitles[_YUID(O365_GROUP_GROUPTYPES)]		= YtriaTranslate::Do(GridGroups_customizeGrid_19, _YLOC("Type Code"));
	m_YUIDandTitles[_YUID(O365_GROUP_SECURITYENABLED)]	= YtriaTranslate::Do(GridGroups_customizeGrid_20, _YLOC("Security Enabled"));
	m_YUIDandTitles[_YUID(O365_GROUP_MAILENABLED)]		= YtriaTranslate::Do(GridGroups_customizeGrid_21, _YLOC("Mail Enabled"));

	m_YUIDandTitles[_YUID(O365_GROUP_ALLOWEXTERNALSENDERS)]					= YtriaTranslate::Do(GridGroups_customizeGrid_23, _YLOC("Allows External Senders"));
	m_YUIDandTitles[_YUID(O365_GROUP_UNSEENCOUNT)]							= YtriaTranslate::Do(GridGroups_customizeGrid_24, _YLOC("Unseen Count"));
	m_YUIDandTitles[_YUID(O365_GROUP_AUTOSUBSCRIBENEWMEMBERS)]				= YtriaTranslate::Do(GridGroups_customizeGrid_25, _YLOC("Auto Subscribe New members"));
	m_YUIDandTitles[_YUID("ownerusernames")]								= YtriaTranslate::Do(GridGroups_customizeGrid_26, _YLOC("Owner User Names"));
	m_YUIDandTitles[_YUID("owneruserids")]									= YtriaTranslate::Do(GridGroups_customizeGrid_27, _YLOC("Owner User IDs"));
	m_YUIDandTitles[_YUID(YTRIA_GROUP_GROUPUNIFIEDGUESTSETTINGACTIVATED)]	= YtriaTranslate::Do(GridGroups_customizeGrid_28, _YLOC("Group.Unified - Guest Setting Activated"));
	m_YUIDandTitles[_YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTS)]				= YtriaTranslate::Do(GridGroups_customizeGrid_29, _YLOC("Group.Unified - Allow To Add Guests"));
	m_YUIDandTitles[_YUID("statusAllowToAddGuests")]						= YtriaTranslate::Do(BusinessGroupConfiguration_BusinessGroupConfiguration_1, _YLOC("Add Guests")).c_str();

	m_YUIDandTitles[_YUID("createdOnBehalfOfUserId")]				= YtriaTranslate::Do(GridGroups_customizeGrid_31, _YLOC("User ID - Created On Behalf Of"));
	m_YUIDandTitles[_YUID("createdOnBehalfOfUserDisplayName")]		= YtriaTranslate::Do(GridGroups_customizeGrid_32, _YLOC("Username - Created On Behalf Of"));
	m_YUIDandTitles[_YUID("createdOnBehalfOfUserPrincipalName")]	= YtriaTranslate::Do(GridGroups_customizeGrid_33, _YLOC("User Principal Name - Created On Behalf Of"));

	m_YUIDandTitles[_YUID(O365_GROUP_RENEWEDDATETIME)]	= YtriaTranslate::Do(GridGroups_customizeGrid_8, _YLOC("Renewed On"));
	m_YUIDandTitles[_YUID("expirationPolicyStatus")]	= YtriaTranslate::Do(GridGroups_customizeGrid_9, _YLOC("Expiration Policy Status"));
	m_YUIDandTitles[_YUID("expireOn")]					= YtriaTranslate::Do(GridGroups_customizeGrid_57, _YLOC("Expire On"));

	m_YUIDandTitles[_YUID("teamMemberSettingsAllowCreateUpdateChannels")]			= YtriaTranslate::Do(GridGroups_customizeGrid_35, _YLOC("Member Settings - Allow Create Update Channels"));
	m_YUIDandTitles[_YUID("teamMemberSettingsAllowCreatePrivateChannels")]			= _T("Member Settings - Allow Create Private Channels");
	m_YUIDandTitles[_YUID("teamMemberSettingsAllowDeleteChannels")]					= YtriaTranslate::Do(GridGroups_customizeGrid_36, _YLOC("Member Settings - Allow Delete Channels"));
	m_YUIDandTitles[_YUID("teamMemberSettingsAllowAddRemoveApps")]					= YtriaTranslate::Do(GridGroups_customizeGrid_37, _YLOC("Member Settings - Allow Add Remove Apps"));
	m_YUIDandTitles[_YUID("teamMemberSettingsAllowCreateUpdateRemoveTabs")]			= YtriaTranslate::Do(GridGroups_customizeGrid_38, _YLOC("Member Settings - Allow Create Update Remove Tabs"));
	m_YUIDandTitles[_YUID("teamMemberSettingsAllowCreateUpdateRemoveConnectors")]	= YtriaTranslate::Do(GridGroups_customizeGrid_39, _YLOC("Member Settings - Allow Create Update Remove Connectors"));

	m_YUIDandTitles[_YUID("guestSettingsAllowCreateUpdateChannels")]	= YtriaTranslate::Do(GridGroups_customizeGrid_41, _YLOC("Guest Settings - Allow Create Update Channels"));
	m_YUIDandTitles[_YUID("guestSettingsAllowDeleteChannels")]			= YtriaTranslate::Do(GridGroups_customizeGrid_42, _YLOC("Guest Settings - Allow Delete Channels"));

	m_YUIDandTitles[_YUID("messagingSettingsAllowUserEditMessages")]	= YtriaTranslate::Do(GridGroups_customizeGrid_44, _YLOC("Messaging Settings - Allow User Edit Messages"));
	m_YUIDandTitles[_YUID("messagingSettingsAllowUserDeleteMessages")]	= YtriaTranslate::Do(GridGroups_customizeGrid_45, _YLOC("Messaging Settings - Allow User Delete Messages"));
	m_YUIDandTitles[_YUID("messagingSettingsAllowOwnerDeleteMessages")] = YtriaTranslate::Do(GridGroups_customizeGrid_46, _YLOC("Messaging Settings - Allow Owner Delete Messages"));
	m_YUIDandTitles[_YUID("messagingSettingsAllowTeamMentions")]		= YtriaTranslate::Do(GridGroups_customizeGrid_47, _YLOC("Messaging Settings - Allow Team Mentions"));
	m_YUIDandTitles[_YUID("messagingSettingsAllowChannelMentions")]		= YtriaTranslate::Do(GridGroups_customizeGrid_48, _YLOC("Messaging Settings - Allow Channel Mentions"));

	m_YUIDandTitles[_YUID("funSettingsAllowGiphy")]				= YtriaTranslate::Do(GridGroups_customizeGrid_50, _YLOC("Fun Settings - Allow Giphy"));
	m_YUIDandTitles[_YUID("funSettingsGiphyContentRating")]		= YtriaTranslate::Do(GridGroups_customizeGrid_51, _YLOC("Fun Settings - Giphy Content Rating"));
	m_YUIDandTitles[_YUID("funSettingsAllowStickersAndMemes")]	= YtriaTranslate::Do(GridGroups_customizeGrid_52, _YLOC("Fun Settings - Allow Stickers and Memes"));
	m_YUIDandTitles[_YUID("funSettingsAllowCustomMemes")]		= YtriaTranslate::Do(GridGroups_customizeGrid_53, _YLOC("Fun Settings - Allow Custom Memes"));

	m_YUIDandTitles[_YUID("webUrl")]							= YtriaTranslate::Do(GridGroups_customizeGrid_55, _YLOC("Team - Web Url"));
	m_YUIDandTitles[_YUID("isArchived")]						= YtriaTranslate::Do(BusinessGroupConfiguration_BusinessGroupConfiguration_3, _YLOC("Archived Team")).c_str();
	m_YUIDandTitles[_YUID("setSpoSiteReadOnlyForMembers")]		= _T("Make the SharePoint Online site read-only for team members");
	m_YUIDandTitles[_YUID(YTRIA_GROUP_ISYAMMER)]				= _T("Is Yammer");
	m_YUIDandTitles[_YUID("internalId")]						= YtriaTranslate::Do(BusinessGroupConfiguration_BusinessGroupConfiguration_4, _YLOC("Team - Internal Id")).c_str();

	m_YUIDandTitles[_YUID(O365_COLDRIVENAME)]					= _T("Doc Library Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVEID)]						= _T("Doc Library Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVEDESCRIPTION)]			= _T("Doc Library Description");
	m_YUIDandTitles[_YUID(O365_COLDRIVEQUOTASTATE)]				= _T("Doc Library - Storage State");
	m_YUIDandTitles[_YUID(O365_COLDRIVEQUOTAUSED)]				= _T("Doc Library - Storage Used");
	m_YUIDandTitles[_YUID(O365_COLDRIVEQUOTAREMAINING)]			= _T("Doc Library - Storage Remaining");
	m_YUIDandTitles[_YUID(O365_COLDRIVEQUOTADELETED)]			= _T("Doc Library - Storage Deleted");
	m_YUIDandTitles[_YUID(O365_COLDRIVEQUOTATOTAL)]				= _T("Doc Library - Storage Total");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATIONTIME)]			= _T("Doc Library - Creation Time");
	m_YUIDandTitles[_YUID(O365_COLDRIVELASTMODIFIEDDATETIME)]	= _T("Doc Library - Last Modified Date Time");
	m_YUIDandTitles[_YUID(O365_COLDRIVETYPE)]					= _T("Doc Library Type");
	m_YUIDandTitles[_YUID(O365_COLDRIVEWEBURL)]					= _T("Doc Library Web Url");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYUSEREMAIL)]		= _T("Doc Library - Created By User Email");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYUSERID)]		= _T("Doc Library - Created By User Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYAPPNAME)]		= _T("Doc Library - Created By App Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYAPPID)]			= _T("Doc Library - Created By App Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYDEVICENAME)]	= _T("Doc Library - Created By Device Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVECREATEDBYDEVICEID)]		= _T("Doc Library - Created By Device Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERUSERNAME)]			= _T("Doc Library - Created Owner User Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERUSERID)]			= _T("Doc Library - Owner - User Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERUSEREMAIL)]			= _T("Doc Library - Owner - User Email");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERAPPNAME)]			= _T("Doc Library - Owner - App Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERAPPID)]				= _T("Doc Library - Owner - App Id");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERDEVICENAME)]		= _T("Doc Library - Owner - Device Name");
	m_YUIDandTitles[_YUID(O365_COLDRIVEOWNERDEVICEID)]			= _T("Doc Library - Owner - Device Id");

	m_YUIDandTitles[_YUID(O365_GROUP_HIDEFROMOUTLOOKCLIENTS)]	= _T("Hide from Outlook clients");
	m_YUIDandTitles[_YUID(O365_GROUP_HIDEFROMADDRESSLISTS)]		= _T("Hide from address lists");
	m_YUIDandTitles[_YUID(O365_GROUP_LICENSEPROCESSINGSTATE)]	= _T("License processing state");
	m_YUIDandTitles[_YUID(O365_GROUP_SECURITYIDENTIFIER)]		= _T("Security identifier");
	m_YUIDandTitles[_YUID(O365_GROUP_ASSIGNEDLICENSES)]			= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_1, _YLOC("Assigned Licences Sku Part Number")).c_str();
	m_YUIDandTitles[_YUID("ASSIGNEDLICENSENAMES")]				= _T("Assigned Licenses");
	m_YUIDandTitles[_YUID("assignedLicenses.skuId")]			= YtriaTranslate::Do(BusinessUserConfiguration_BusinessUserConfiguration_2, _YLOC("Assigned Licenses Sku ID")).c_str();
	
	// List properties (commented ones are of unhandled types)
	m_Properties4RoleDelegation =
	{
		_YUID(O365_GROUP_GROUPTYPES),
		_YUID(O365_GROUP_SECURITYENABLED),
		_YUID(O365_GROUP_ISTEAM),
		_YUID(O365_GROUP_GROUPTYPE),
		_YUID(O365_GROUP_DYNAMICMEMBERSHIP),
		_YUID(O365_GROUP_PREFERREDDATALOCATION),
		_YUID(O365_GROUP_VISIBILITY),
		_YUID(O365_GROUP_DESCRIPTION),
		_YUID(O365_GROUP_DISPLAYNAME),
		_YUID(O365_GROUP_MAILENABLED),
		_YUID(O365_GROUP_MAIL),
		_YUID(O365_GROUP_MAILNICKNAME),
		_YUID(O365_GROUP_PROXYADDRESSES),
		_YUID(O365_GROUP_CREATEDDATETIME),
		_YUID(O365_GROUP_RENEWEDDATETIME),
		_YUID(O365_GROUP_ONPREMISESLASTSYNCDATETIME),
		_YUID(O365_GROUP_ONPREMISESSECURITYIDENTIFIER),
        _YUID(O365_ONPREMISESPROVISIONINGERRORSCATEGORY),
        _YUID(O365_ONPREMISESPROVISIONINGERRORSOCCUREDDATETIME),
        _YUID(O365_ONPREMISESPROVISIONINGERRORSPROPERTYCAUSINGERROR),
        _YUID(O365_ONPREMISESPROVISIONINGERRORSVALUE),
		_YUID(O365_GROUP_ONPREMISESSYNCENABLED),
		_YUID(O365_GROUP_RESOURCEBEHAVIOROPTIONS),
		_YUID(O365_GROUP_RESOURCEPROVISIONINGOPTIONS),
	};

	m_ComputedProperties =
	{
		_YUID(O365_GROUP_ISTEAM),
		_YUID(O365_GROUP_GROUPTYPE),
		_YUID(O365_GROUP_DYNAMICMEMBERSHIP),
	};

	g_Unlicensed = _T("-- No assignments --");
}

const wstring& BusinessGroupConfiguration::GetValueStringLicenses_Unlicensed() const
{
	return g_Unlicensed;
}
