#pragma once

#include "IFastDeserializer.h"

class ObjectDeserializerBase : public IFastDeserializer
{
public:
    virtual void Deserialize(const web::json::value& p_Json) override final
    {
        ASSERT(p_Json.is_object() || p_Json.is_null());
        if (p_Json.is_object())
            DeserializeObject(p_Json.as_object());
    }

protected:
    virtual void DeserializeObject(const web::json::object& p_Object) = 0;
};

