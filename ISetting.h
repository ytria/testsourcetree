#pragma once

template<typename T>
class ISetting
{
public:
	ISetting()
		: ISetting(boost::none)
	{
	}

	ISetting(const boost::YOpt<T> p_Default)
		: m_Default(p_Default)
	{
	}

	virtual ~ISetting() = default;

	virtual boost::YOpt<T> Get() const = 0;
	virtual bool Set(boost::YOpt<T> p_Val) const = 0;
	virtual bool Reset() const
	{
		return Set(m_Default);
	}

	// Returns true if this is a real setting.
	virtual operator bool() const
	{
		return true;
	}

protected:
	void init()
	{
		if (!Get())
			Reset();
	}

private:
	const boost::YOpt<T> m_Default;
};