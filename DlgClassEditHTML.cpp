#include "DlgClassEditHTML.h"
#include "AutomationNames.h"

DlgClassEditHTML::DlgClassEditHTML(vector<EducationClass>& p_Classes, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
	: DlgFormsHTML(p_Action, p_Parent, p_Action == DlgFormsHTML::Action::CREATE ? g_ActionNameCreateClass : g_ActionNameSelectedEditClass)
	, m_Classes(p_Classes)
{
}

void DlgClassEditHTML::generateJSONScriptData()
{
	initMain(_YTEXT("Edit Classes"), false, false, true);

	addStringEditor(&EducationClass::GetDescription, &EducationClass::SetDescription, _YTEXT("classDescription"), _T("Description"), 0);
	addStringEditor(&EducationClass::GetDisplayName, &EducationClass::SetDisplayName, _YTEXT("displayName"), _T("Display Name"), 0);
	addStringEditor(&EducationClass::GetMailNickname, &EducationClass::SetMailNickname, _YTEXT("classMailNickname"), _T("Mail nickname"), 0);

	addStringEditor(&EducationClass::GetClassCode, &EducationClass::SetClassCode, _YTEXT("classCode"), _T("Class Code"), 0);
	addStringEditor(&EducationClass::GetExternalId, &EducationClass::SetExternalId, _YTEXT("classExternalId"), _T("External Id"), 0);
	addStringEditor(&EducationClass::GetExternalName, &EducationClass::SetExternalName, _YTEXT("classExternalName"), _T("External Name"), 0);

	addComboEditor(&EducationClass::GetExternalSource, &EducationClass::SetExternalSource, _YTEXT("classExternalSource"), _T("External Source"), 0, { { _YTEXT("sis"), _YTEXT("sis") }, { _YTEXT("manual"), _YTEXT("manual") }, { _YTEXT("enum_sentinel"), _YTEXT("enum_sentinel") } });

	getGenerator().addApplyButton(isEditDialog() ? LocalizedStrings::g_ApplyButtonText : LocalizedStrings::g_CreateButtonText);
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
	getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);

	for (auto& curClass : m_Classes)
	{
		curClass.SetDescription(boost::none);
		curClass.SetDisplayName(boost::none);
		curClass.SetMailNickname(boost::none);
		curClass.SetClassCode(boost::none);
		curClass.SetExternalId(boost::none);
		curClass.SetExternalName(boost::none);
		curClass.SetExternalSource(boost::none);
	}
}

wstring DlgClassEditHTML::getDialogTitle() const
{
	return _T("Edit classes");
}

bool DlgClassEditHTML::processPostedData(const IPostedDataTarget::PostedData& properties)
{
	for (const auto& property : properties)
	{
		const auto& propName = property.first;
		const auto& propVal = property.second;
		ASSERT(!propName.empty());

		auto itt = m_StringSetters.find(propName);
		ASSERT(itt != m_StringSetters.end());
		if (itt != m_StringSetters.end())
		{
			for (auto& curClass : m_Classes)
			{
				const auto& setter = itt->second;
				setter(curClass, boost::YOpt<PooledString>(propVal));
			}
		}
	}

	return true;
}

void DlgClassEditHTML::addStringEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags)
{
	addObjectStringEditor<EducationClass>(m_Classes
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags
		, [p_Flags](const EducationClass& curClass)
		{
			return 0; // TODO
			// getRestrictionsImpl(bg, p_Flags, p_ApplicableGroupTypes);
		}
	);

	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

void DlgClassEditHTML::addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues)
{
	addObjectListEditor<EducationClass>(m_Classes
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags
		, p_LabelsAndValues
		, [p_Flags](const EducationClass& curClass)
		{
			return 0; // TODO
			//return getRestrictionsImpl(bu, p_Flags, p_ApplicableUserTypes);
		}
	);
	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

bool DlgClassEditHTML::hasProperty(const wstring& p_PropertyName) const
{
	return m_StringSetters.find(p_PropertyName) != m_StringSetters.end();
}
