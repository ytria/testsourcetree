#include "GroupSettingTemplateDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "SettingTemplateValueDeserializer.h"

void GroupSettingTemplateDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.m_Description, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);

    {
        ListDeserializer<SettingTemplateValue, SettingTemplateValueDeserializer> stvDeserializer;
        if (JsonSerializeUtil::DeserializeAny(stvDeserializer, _YTEXT("values"), p_Object))
            m_Data.m_Values = std::move(stvDeserializer.GetData());
    }
}
