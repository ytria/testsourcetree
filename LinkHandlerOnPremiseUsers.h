#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerOnPremiseUsers : public IBrowserLinkHandler
{
public:
	void Handle(YBrowserLink& p_Link) const override;
};

