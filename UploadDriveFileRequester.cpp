#include "UploadDriveFileRequester.h"

#include "FileUtil.h"
#include "JsonSerializeUtil.h"
#include "MSGraphCommonData.h"
#include "MsGraphFieldNames.h"
#include "O365AdminUtil.h"
#include "Sapio365Session.h"

#include <fstream>
#include <iterator>
#include <cpprest/http_msg.h>
#include "MsGraphHttpRequestLogger.h"

UploadDriveFileRequester::UploadDriveFileRequester(const wstring& p_FilePath, const wstring& p_DriveId, const wstring& p_ParentFolderId, const wstring& p_OptionalFileName/* = Str::g_EmptyString*/)
	: m_FilePath(p_FilePath)
	, m_DriveId(p_DriveId)
	, m_FolderId(p_ParentFolderId)
	, m_OptionalFileName(p_OptionalFileName)
{	  

}

TaskWrapper<void> UploadDriveFileRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	boost::YOpt<wstring> error;
	const auto fileSize = FileUtil::GetFileSize(m_FilePath, error);
	const auto fileSizeInBytes = fileSize ? *fileSize : 0;

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());

	if (fileSizeInBytes >= 0)
	{
		const auto fileName = m_OptionalFileName.empty() ? FileUtil::FileGetFileName(m_FilePath) : m_OptionalFileName;
		static const size_t thresholdInMB = 4; // Doc is unclear between 4 and 10, but confirmed it's 4, by testing.
		if (fileSizeInBytes <= thresholdInMB * 1024 * 1024)
		{
			// Use simple upload: https://docs.microsoft.com/en-us/graph/api/driveitem-put-content?view=graph-rest-1.0&tabs=http

			return YSafeCreateTask([httpLogger, result = m_Result, driveId = m_DriveId, folderId = m_FolderId.empty() ? _YTEXT(O365_SITE_ROOT) : m_FolderId, filePath = m_FilePath, fileName, p_Session, p_TaskData]()
				{
					web::uri_builder uri;
					uri.append_path(Rest::DRIVES_PATH);
					uri.append_path(driveId);
					uri.append_path(Rest::ITEMS_PATH);
					uri.append_path(folderId + _YTEXT(":"));
					// NOTE: filename is allowed to contain folders (relative to parent folderID, separated by / ).
					// Can be useful to create the parent folder in the same request, or if we prefer using the folder's name instead of ID.
					uri.append_path(web::uri::encode_uri(fileName, web::uri::components::path) + _YTEXT(":"));
					uri.append_path(Rest::CONTENT_PATH);

					*result = HttpResultWithError(p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Put(uri.to_uri(), httpLogger, p_TaskData, WebPayloadBinaryFile(filePath)).GetTask().get());

					LoggerService::User(_T("Uploading... 100%"), p_TaskData.GetOriginator());
				});
		}
		else
		{
			// Use resumable upload: https://docs.microsoft.com/en-us/graph/api/driveitem-createuploadsession?view=graph-rest-1.0

			return YSafeCreateTask([httpLogger, result = m_Result, driveId = m_DriveId, folderId = m_FolderId.empty() ? _YTEXT(O365_SITE_ROOT) : m_FolderId, filePath = m_FilePath, p_Session, p_TaskData]()
			{
				const auto fileName = FileUtil::FileGetFileName(filePath);
				web::uri_builder uri;
				uri.append_path(Rest::DRIVES_PATH);
				uri.append_path(driveId);
				uri.append_path(Rest::ITEMS_PATH);
				uri.append_path(folderId + _YTEXT(":"));
				// NOTE: filename is allowed to contain folders (relative to parent folderID, separated by / ).
				// Can be useful to create the parent folder in the same request, or if we prefer using the folder's name instead of ID.
				uri.append_path(web::uri::encode_uri(fileName, web::uri::components::path) + _YTEXT(":"));
				uri.append_path(_YTEXT("createUploadSession"));

				WebPayloadJSON payload(web::json::value::object(
					{
						std::make_pair(_YTEXT("@microsoft.graph.conflictBehavior"), web::json::value(_YTEXT("replace"))),
						std::make_pair(_YTEXT("description"), web::json::value(_YTEXT(""))), // FIXME: TODO?
						std::make_pair(_YTEXT("fileSystemInfo"), Util::GetFileSystemInfo(filePath)),
						std::make_pair(_YTEXT("name"), web::json::value(fileName)),
					}
				));

				*result = Util::GetResult(p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_uri(), httpLogger, p_TaskData, payload));
				auto body = result->m_ResultInfo->GetBodyAsString();
				if (body)
				{
					const auto resultJson = json::value::parse(*body);
					wstring url;
					JsonSerializeUtil::DeserializeString(PooledString(_YTEXT("uploadUrl")), url, resultJson.as_object());
					ASSERT(!url.empty());
					if (!url.empty())
					{
						// Should be between 5MB and 10MB, multiple of 320KB
						// 10 said to be optimal with high speed connection.
						static const int blockSize = 5 * 1024 * 1024; //5MB
						//static const int blockSize = 320 * 1024; // 320kB
						auto dataBlockPtr = std::make_shared<vector<uint8_t>>();
						dataBlockPtr->reserve(blockSize);
						WebPayloadBinaryBlock payloadFileBlock(dataBlockPtr);

						std::ifstream inputFile;
						inputFile.open(filePath, ios::binary | ios::ate);
						const auto fileSize = inputFile.tellg();

						http_headers headers;
						std::streamsize pos = 0;
						auto& dataBlock = *dataBlockPtr;
						while (inputFile)
						{
							dataBlock.resize(blockSize);
							inputFile.seekg(pos, inputFile.beg); // Is this useful?
							inputFile.read((char*)&dataBlock[0], dataBlock.size());
							dataBlock.resize(static_cast<size_t>(inputFile.gcount()));

							headers[web::http::header_names::content_length] = Str::getStringFromNumber(dataBlock.size());
							headers[web::http::header_names::content_range] = _YTEXTFORMAT(L"bytes %s-%s/%s", Str::getStringFromNumber(pos).c_str(), Str::getStringFromNumber(pos + dataBlock.size() - 1).c_str(), Str::getStringFromNumber(fileSize).c_str());

							*result = Util::GetResult(p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Put(url, httpLogger, p_TaskData, payloadFileBlock, {}, headers));

							pos += dataBlock.size();

							LoggerService::User(_YFORMAT(L"Uploading... %s%%", Str::getStringFromNumber(100 * pos / fileSize).c_str()), p_TaskData.GetOriginator());

							if (result->m_ResultInfo->Response.status_code() == http::status_codes::Accepted)
							{
								// "nextExpectedRanges" are always "0-", so useless...

								/*const auto resultJson = json::value::parse(*body);
								ASSERT(resultJson.is_object());
								if (resultJson.is_object())
								{
									auto& obj = resultJson.as_object();
									auto itt = obj.find(_YTEXT("nextExpectedRanges"));
									if (itt != obj.end() && itt->second.is_array())
									{
										const auto& ranges = itt->second.as_array();
										ASSERT(ranges.size() > 0);
										const auto& firstvalue = *ranges.begin();
										ASSERT(firstvalue.is_string());
										auto& rangeAsString = firstvalue.as_string();
										auto values = Str::explodeIntoVector(rangeAsString, _YTEXT("-"));
										ASSERT(values.size() >= 1);
										const auto nextPos = static_cast<std::streamsize>(Str::GetNumber(values[0]));
										//ASSERT(nextPos == pos);
									}
								}*/
							}
							else if (result->m_ResultInfo->Response.status_code() == http::status_codes::Created)
							{
								// Upload finished.
								ASSERT(pos >= fileSize);
							}
							else
							{
								// Error
								return;
							}
						}
					}
				}
			});
		}
	}

	if (error && !error->empty() && !m_Result->m_ResultInfo && !m_Result->m_Error)
		*m_Result = HttpResultWithError(boost::none, SapioError(*error, 0));

	return pplx::task_from_result();
}

const HttpResultWithError& UploadDriveFileRequester::GetResult() const
{
	ASSERT(m_Result);
	return *m_Result;
}
