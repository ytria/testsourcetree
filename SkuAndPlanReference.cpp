#include "SkuAndPlanReference.h"

#include "CStdioFileEx.h"
#include "FileUtil.h"
#include "Resource.h"
#include "Sapio365Settings.h"
#include "SQLiteEngine.h"
#include "TimeUtil.h"
#include "TraceIntoFile.h"
#include "YFileDownload.h"

std::unique_ptr<SkuAndPlanReference> SkuAndPlanReference::g_Instance;

SkuAndPlanReference::SkuAndPlanReference(std::unique_ptr<ISkuAndPlanInfoProvider>&& p_SkuAndPlanInfoProvider, _ConstructorCookie)
	: m_SkuAndPlanInfoProvider(std::move(p_SkuAndPlanInfoProvider))
{

}

SkuAndPlanReference& SkuAndPlanReference::GetInstance()
{
	if (!g_Instance)
		InitInstance(std::make_unique<SQLSkuAndPlanInfoProvider>(
			std::initializer_list<std::unique_ptr<MappedSkuAndPlanInfoProvider>>
			{	std::make_unique<CachedOnlineFileSkuAndPlanInfoProvider>(_YTEXT("https://downloads.ytria.com/skus/ytria_skus_label.json")
																		, SQLiteEngine::getLocalAppDataDBFileFolder() + _YTEXT("ytria_skus_label.json")
																		, 7
																		, true)
				, std::make_unique<FileSkuAndPlanInfoProvider>(SQLiteEngine::getLocalAppDataDBFileFolder() + _YTEXT("cstm_skus_label.json"))
			}
		));
	return *g_Instance;
}

SkuAndPlanReference& SkuAndPlanReference::InitInstance(std::unique_ptr<ISkuAndPlanInfoProvider>&& p_SkuAndPlanInfoProvider)
{
	g_Instance = std::make_unique<SkuAndPlanReference>(std::move(p_SkuAndPlanInfoProvider), _ConstructorCookie());
	return *g_Instance;
}

void SkuAndPlanReference::DeleteInstance()
{
	g_Instance.reset();
}

const PooledString& SkuAndPlanReference::GetSkuLabel(const PooledString& p_SkuId)
{
	ASSERT(m_SkuAndPlanInfoProvider);
	if (m_SkuAndPlanInfoProvider)
		return m_SkuAndPlanInfoProvider->GetSkuLabelById(p_SkuId);

	return PooledString::g_EmptyString;
}

const PooledString& SkuAndPlanReference::GetSkuPartNumber(const PooledString& p_SkuId)
{
	ASSERT(m_SkuAndPlanInfoProvider);
	if (m_SkuAndPlanInfoProvider)
		return m_SkuAndPlanInfoProvider->GetSkuPartNumberById(p_SkuId);
	return PooledString::g_EmptyString;
}

const PooledString& SkuAndPlanReference::GetPlanLabel(const PooledString& p_PlanId)
{
	ASSERT(m_SkuAndPlanInfoProvider);
	if (m_SkuAndPlanInfoProvider)
		return m_SkuAndPlanInfoProvider->GetPlanLabelById(p_PlanId);
	return PooledString::g_EmptyString;
}

const PooledString& SkuAndPlanReference::GetPlanName(const PooledString& p_PlanId)
{
	ASSERT(m_SkuAndPlanInfoProvider);
	if (m_SkuAndPlanInfoProvider)
		return m_SkuAndPlanInfoProvider->GetPlanNameById(p_PlanId);

	return PooledString::g_EmptyString;
}

const std::set<PooledString>& SkuAndPlanReference::GetPlansForSku(const PooledString& p_SkuId)
{
	ASSERT(m_SkuAndPlanInfoProvider);
	if (m_SkuAndPlanInfoProvider)
		return m_SkuAndPlanInfoProvider->GetPlansForSkuById(p_SkuId);

	static std::set<PooledString> emptySet;
	return emptySet;
}

const std::set<PooledString>& SkuAndPlanReference::GetPlanExclusionsForPlan(const PooledString& p_SkuId)
{
	ASSERT(m_SkuAndPlanInfoProvider);
	if (m_SkuAndPlanInfoProvider)
		return m_SkuAndPlanInfoProvider->GetPlanExclusionsForPlanById(p_SkuId);

	static std::set<PooledString> emptySet;
	return emptySet;
}

void SkuAndPlanReference::Update(const vector<BusinessSubscribedSku>& p_Skus)
{
	auto sqlProvider = dynamic_cast<SQLSkuAndPlanInfoProvider*>(m_SkuAndPlanInfoProvider.get());
	ASSERT(nullptr != sqlProvider);
	if (nullptr != sqlProvider)
		sqlProvider->UpdateWith(std::make_unique<TenantSkuAndPlanInfoProvider>(p_Skus), true);
}

// ============================================================================

void MappedSkuAndPlanInfoProvider::Clear()
{
	m_SkuInfoById.clear();
	m_PlanInfoById.clear();
}

const PooledString& MappedSkuAndPlanInfoProvider::GetSkuLabelById(const PooledString& p_SkuId)
{
	const auto it = m_SkuInfoById.find(p_SkuId);
	if (m_SkuInfoById.end() != it)
		return it->second.m_Label;
	return PooledString::g_EmptyString;
}

const PooledString& MappedSkuAndPlanInfoProvider::GetSkuPartNumberById(const PooledString& p_SkuId)
{
	const auto it = m_SkuInfoById.find(p_SkuId);
	if (m_SkuInfoById.end() != it)
		return it->second.m_Name;
	return PooledString::g_EmptyString;
}

const PooledString& MappedSkuAndPlanInfoProvider::GetPlanLabelById(const PooledString& p_PlanId)
{
	const auto it = m_PlanInfoById.find(p_PlanId);
	if (m_PlanInfoById.end() != it)
		return it->second.m_Label;
	return PooledString::g_EmptyString;
}

const PooledString& MappedSkuAndPlanInfoProvider::GetPlanNameById(const PooledString& p_PlanId)
{
	const auto it = m_PlanInfoById.find(p_PlanId);
	if (m_PlanInfoById.end() != it)
		return it->second.m_Name;
	return PooledString::g_EmptyString;
}

const std::set<PooledString>& MappedSkuAndPlanInfoProvider::GetPlansForSkuById(const PooledString& p_SkuId)
{
	const auto it = m_SkuInfoById.find(p_SkuId);
	if (m_SkuInfoById.end() != it)
		return it->second.m_PlanIds;
	static std::set<PooledString> emptySet;
	return emptySet;
}

const std::set<PooledString>& MappedSkuAndPlanInfoProvider::GetPlanExclusionsForPlanById(const PooledString& p_PlanId)
{
	const auto it = m_PlanInfoById.find(p_PlanId);
	if (m_PlanInfoById.end() != it && it->second.m_Exclusions)
		return *it->second.m_Exclusions;
	static std::set<PooledString> emptySet;
	return emptySet;
}

const std::map<PooledString, SkuAndPlanInfoSQLEngine::SkuInfo> MappedSkuAndPlanInfoProvider::GetSkuInfos() const
{
	return m_SkuInfoById;
}

const std::map<PooledString, SkuAndPlanInfoSQLEngine::PlanInfo> MappedSkuAndPlanInfoProvider::GetPlanInfos() const
{
	return m_PlanInfoById;
}

// ============================================================================

JsonSkuAndPlanInfoProvider::JsonSkuAndPlanInfoProvider()
{

}

bool JsonSkuAndPlanInfoProvider::Load(const char* p_UTF8JsonString, size_t p_Length)
{
	if (p_Length >= 3 && 0 == strncmp(p_UTF8JsonString, (const char*)UTF8_BOM, 3))
	{
		// Remove UTF8 bom
		p_UTF8JsonString += 3;
	}

	wstring unicodeString;
	MFCUtil::convertUTF8_to_UNICODE(p_UTF8JsonString, p_Length, unicodeString);
	return Load(unicodeString);
}

bool JsonSkuAndPlanInfoProvider::Load(std::string& p_UTF8JsonString)
{
	return Load(p_UTF8JsonString.c_str(), p_UTF8JsonString.size());
}

bool JsonSkuAndPlanInfoProvider::Load(const wstring& p_JsonString)
{
	bool success = true;
	Clear();

	std::error_code err;
	auto val = web::json::value::parse(p_JsonString, err);
	if (val.is_null() || err)
	{
		// error
		ASSERT(false);
		TraceIntoFile::trace(_YDUMP("JsonSkuAndPlanInfoProvider"), _YDUMP("Load"), _YDUMPFORMAT(L"ERROR %s", Str::convertFromUTF8(err.message()).c_str()));
		success = false;
	}
	else
	{
		ASSERT(val.is_object());
		if (val.is_object())
		{
			auto& obj = val.as_object();
			{
				wstring source;
				wstring date;
				wstring ref;
				if (obj.end() != obj.find(_YTEXT("source")))
					source = obj[_YTEXT("source")].as_string();
				if (obj.end() != obj.find(_YTEXT("date")))
					date = obj[_YTEXT("date")].as_string();
				if (obj.end() != obj.find(_YTEXT("ref")))
					ref = obj[_YTEXT("ref")].as_string();

				TraceIntoFile::trace(_YDUMP("JsonSkuAndPlanInfoProvider"), _YDUMP("Load"), _YDUMPFORMAT(L"Source: %s Date: %s Ref: %s", source.c_str(), date.c_str(), ref.c_str()));
			}

			if (obj.end() != obj.find(_YTEXT("products")))
			{
				auto& prodVal = obj[_YTEXT("products")];
				ASSERT(prodVal.is_array());
				if (prodVal.is_array())
				{
					auto& arr = prodVal.as_array();
					for (auto& prod : arr)
					{
						ASSERT(prod.is_object());
						if (prod.is_object())
						{
							SkuAndPlanInfoSQLEngine::SkuInfo skuInfo;
							skuInfo.m_Id = prod[_YTEXT("guid")].as_string();
							skuInfo.m_Name = prod[_YTEXT("Name")].as_string();
							skuInfo.m_Label = prod[_YTEXT("label")].as_string();
							for (const auto& p : prod[_YTEXT("plans")].as_array())
								skuInfo.m_PlanIds.insert(p.as_string());

							m_SkuInfoById[skuInfo.m_Id] = skuInfo;
						}
					}
				}
			}
			if (obj.end() != obj.find(_YTEXT("plans")))
			{
				auto& plansVal = obj[_YTEXT("plans")];
				ASSERT(plansVal.is_array());
				if (plansVal.is_array())
				{
					auto& arr = plansVal.as_array();
					for (auto& plan : arr)
					{
						ASSERT(plan.is_object());
						if (plan.is_object())
						{
							SkuAndPlanInfoSQLEngine::PlanInfo planInfo;
							planInfo.m_Id = plan[_YTEXT("guid")].as_string();
							planInfo.m_Name = plan[_YTEXT("Name")].as_string();
							planInfo.m_Label = plan[_YTEXT("label")].as_string();

							m_PlanInfoById[planInfo.m_Id] = planInfo;
						}
					}
				}
			}
			if (obj.end() != obj.find(_YTEXT("not_together")))
			{
				auto& ntVal = obj[_YTEXT("not_together")];
				ASSERT(ntVal.is_array());
				if (ntVal.is_array())
				{
					auto& arr = ntVal.as_array();
					for (auto& nt : arr)
					{
						ASSERT(nt.is_array());
						if (nt.is_array())
						{
							auto& arrids = nt.as_array();
							auto ids = std::make_shared<std::set<PooledString>>();
							std::transform(arrids.begin(), arrids.end(), std::inserter(*ids, ids->begin()), [](auto& a) { return PooledString(a.as_string()); });

							for (auto id : *ids)
							{
								auto it = m_PlanInfoById.find(id);
								if (m_PlanInfoById.end() == it)
								{
									// Create the missing plan even if we only have its ID.
									SkuAndPlanInfoSQLEngine::PlanInfo planInfo;
									planInfo.m_Id = id;
									auto res =  m_PlanInfoById.insert({ id , planInfo });
									ASSERT(res.second);
									if (res.second)
										it = res.first;
								}

								ASSERT(m_PlanInfoById.end() != it);
								if (m_PlanInfoById.end() != it)
								{
									ASSERT(!it->second.m_Exclusions); // Rethink this if an plan can be in several list...
									if (!it->second.m_Exclusions)
										it->second.m_Exclusions = ids;
								}
							}
						}
					}
				}
			}

			TraceIntoFile::trace(_YDUMP("JsonSkuAndPlanInfoProvider"), _YDUMP("Load"), _YDUMPFORMAT(L"%s SKUs, %s Plans", std::to_wstring(m_SkuInfoById.size()).c_str(), std::to_wstring(m_PlanInfoById.size()).c_str()));
		}
	}

	return success;
}

// ============================================================================

StaticSkuAndPlanInfoProvider::StaticSkuAndPlanInfoProvider(bool p_Load)
{
	if (p_Load)
		Load();
}

void StaticSkuAndPlanInfoProvider::Load()
{
	TraceIntoFile::trace(_YDUMP("StaticSkuAndPlanInfoProvider"), _YDUMP("Load"), _YDUMP(""));

	wstring unicodeString;
	size_t size = 0;
	const char* data = MFCUtil::ReadFileFromResources(IDB_YTRIA_SKUS_LABEL_JSON, _YTEXT("TEXT"), size);
	ASSERT(nullptr != data && 0 != size);
	if (nullptr != data && 0 != size)
		JsonSkuAndPlanInfoProvider::Load(data, size);
}

// ============================================================================

FileSkuAndPlanInfoProvider::FileSkuAndPlanInfoProvider(const wstring& p_FilePath)
{
	if (!p_FilePath.empty())
		LoadFile(p_FilePath);
}

bool FileSkuAndPlanInfoProvider::LoadFile(const wstring& p_FilePath)
{
	TraceIntoFile::trace(_YDUMP("FileSkuAndPlanInfoProvider"), _YDUMP("LoadFile"), p_FilePath);
	std::string str;
	{
		std::ifstream file(p_FilePath, std::ios::in | std::ios::binary);
		if (file)
		{
			file.seekg(0, std::ios::end);
			auto pos = file.tellg();
			if (pos >= 0)
				str.resize(static_cast<size_t>(pos));
			file.seekg(0, std::ios::beg);
			file.read(&str[0], str.size());
		}
	}

	if (!str.empty())
		return Load(str);

	return false;
}

// ============================================================================

OnlineFileSkuAndPlanInfoProvider::OnlineFileSkuAndPlanInfoProvider(const wstring& p_FileURL)
	: FileSkuAndPlanInfoProvider(Str::g_EmptyString)
{
	if (!p_FileURL.empty())
		LoadURL(p_FileURL);
}

wstring OnlineFileSkuAndPlanInfoProvider::LoadURL(const wstring& p_FileURL, bool p_OnlyDownload/* = false*/)
{
	TraceIntoFile::trace(_YDUMP("OnlineFileSkuAndPlanInfoProvider"), _YDUMP("LoadURL"), p_FileURL);
	auto callback = [&p_FileURL](ULONG progress, ULONG progressMax, ULONG statusCode, wstring statusText)
	{
		TraceIntoFile::trace(_YDUMP("OnlineFileSkuAndPlanInfoProvider"), _YDUMP("LoadURL"), _YDUMPFORMAT(L"%s %s/%s", p_FileURL.c_str(), std::to_wstring(progress).c_str(), std::to_wstring(progressMax).c_str()));
		return true;
	};
	auto res = YFileDownload::downloadFile(p_FileURL, callback);

	if (res.first)
	{
		if (!p_OnlyDownload)
			LoadFile(res.second);
		return res.second;
	}

	TraceIntoFile::trace(_YDUMP("OnlineFileSkuAndPlanInfoProvider"), _YDUMP("LoadURL"), _YDUMPFORMAT(L"Download error %s", res.second.c_str()));
	return Str::g_EmptyString;
}

// ============================================================================

CachedOnlineFileSkuAndPlanInfoProvider::CachedOnlineFileSkuAndPlanInfoProvider(const wstring& p_FileURL, const wstring& p_CacheFilePath, int p_DayIntervalForRefresh, bool p_Init)
	: OnlineFileSkuAndPlanInfoProvider(Str::g_EmptyString)
	, StaticSkuAndPlanInfoProvider(false)
{
	if (p_Init)
		Init(p_FileURL, p_CacheFilePath, p_DayIntervalForRefresh);
}

void CachedOnlineFileSkuAndPlanInfoProvider::Init(const wstring& p_FileURL, const wstring& p_CacheFilePath, int p_DayIntervalForRefresh)
{
	if (!p_CacheFilePath.empty())
	{
		bool downloadNow = !FileUtil::FileExists(p_CacheFilePath);
		auto lastCheck = OnlineSkuSetting().Get();
		if (!downloadNow && lastCheck)
		{
			YTimeDate td;
			TimeUtil::GetInstance().GetTimedateFromISO8601String(*lastCheck, td, false);
			downloadNow = YTimeDate::GetCurrentTimeDateGMT().DifferenceInSeconds(td) > p_DayIntervalForRefresh * YTimeDate::g_OneDay;
			if (!downloadNow)
				TraceIntoFile::trace(_YDUMP("CachedOnlineFileSkuAndPlanInfoProvider"), _YDUMP("CachedOnlineFileSkuAndPlanInfoProvider"), _YDUMPFORMAT(L"Last check is less than %s days old. No download.", std::to_wstring(p_DayIntervalForRefresh).c_str()));
		}

		if (downloadNow && !p_FileURL.empty())
		{
			auto filePath = LoadURL(p_FileURL, true);
			if (!filePath.empty())
			{
				OnlineSkuSetting().Set(TimeUtil::GetInstance().GetISO8601String(YTimeDate::GetCurrentTimeDateGMT()));
				DeleteFile(p_CacheFilePath.c_str());
				MoveFile(filePath.c_str(), p_CacheFilePath.c_str());
			}
		}

		if (!FileUtil::FileExists(p_CacheFilePath) || !LoadFile(p_CacheFilePath))
		{
			TraceIntoFile::trace(_YDUMP("CachedOnlineFileSkuAndPlanInfoProvider"), _YDUMP("CachedOnlineFileSkuAndPlanInfoProvider"), _YDUMPFORMAT(L"Unable to load file %s. Fallback to resource.", p_CacheFilePath.c_str()));
			Load();
		}
	}
}

// ============================================================================

SQLSkuAndPlanInfoProvider::SQLSkuAndPlanInfoProvider(std::initializer_list<std::unique_ptr<MappedSkuAndPlanInfoProvider>> p_AltenativeSkuAndPlanInfoProviders)
	: m_Engine(std::make_unique<SkuAndPlanInfoSQLEngine>(_YTEXT("_sapi1_.ytr")))
{
	LoadFromSql();
	for (const auto& p : p_AltenativeSkuAndPlanInfoProviders)
		UpdateWith(p, false);
	LoadFromSql();
}

void SQLSkuAndPlanInfoProvider::LoadFromSql()
{
	TraceIntoFile::trace(_YDUMP("SQLSkuAndPlanInfoProvider"), _YDUMP("LoadFromSql"), _YDUMP(""));
	m_SkuInfoById = m_Engine->LoadSkus();
	m_PlanInfoById = m_Engine->LoadPlans();
}

void SQLSkuAndPlanInfoProvider::SaveToSql() const
{
	TraceIntoFile::trace(_YDUMP("SQLSkuAndPlanInfoProvider"), _YDUMP("SaveToSql"), _YDUMP(""));
	m_Engine->StartTransaction();
	m_Engine->Save(m_SkuInfoById);
	m_Engine->Save(m_PlanInfoById);
	m_Engine->EndTransaction();
}

void SQLSkuAndPlanInfoProvider::UpdateWith(const std::unique_ptr<MappedSkuAndPlanInfoProvider>& p_AltenativeSkuAndPlanInfoProvider, bool p_Reload)
{
	if (p_AltenativeSkuAndPlanInfoProvider)
	{
		TraceIntoFile::trace(_YDUMP("SQLSkuAndPlanInfoProvider"), _YDUMP("UpdateWith"), _YDUMP(""));
		for (auto& sku : p_AltenativeSkuAndPlanInfoProvider->GetSkuInfos())
		{
			if (!sku.second.m_Id.IsEmpty())
			{
				m_SkuInfoById[sku.first].m_Id = sku.first;
				if (!sku.second.m_Label.IsEmpty())
					m_SkuInfoById[sku.first].m_Label = sku.second.m_Label;
				if (!sku.second.m_Name.IsEmpty())
					m_SkuInfoById[sku.first].m_Name = sku.second.m_Name;
				if (!sku.second.m_PlanIds.empty())
					m_SkuInfoById[sku.first].m_PlanIds = sku.second.m_PlanIds;
			}
		}

		for (auto& plan : p_AltenativeSkuAndPlanInfoProvider->GetPlanInfos())
		{
			if (!plan.second.m_Id.IsEmpty())
			{
				m_PlanInfoById[plan.first].m_Id = plan.first;
				if (!plan.second.m_Label.IsEmpty())
					m_PlanInfoById[plan.first].m_Label = plan.second.m_Label;
				if (!plan.second.m_Name.IsEmpty())
					m_PlanInfoById[plan.first].m_Name = plan.second.m_Name;
				if (plan.second.m_Exclusions && !plan.second.m_Exclusions->empty())
					m_PlanInfoById[plan.first].m_Exclusions = plan.second.m_Exclusions;
			}
		}

		SaveToSql();
		if (p_Reload)
			LoadFromSql();
	}
}

// ============================================================================

TenantSkuAndPlanInfoProvider::TenantSkuAndPlanInfoProvider(const vector<BusinessSubscribedSku>& p_Skus)
{
	for (auto& sku : p_Skus)
	{
		ASSERT(sku.GetSkuId() && !sku.GetSkuId()->IsEmpty());
		if (sku.GetSkuId() && !sku.GetSkuId()->IsEmpty())
		{
			auto& skuInfo = m_SkuInfoById[*sku.GetSkuId()];
			if (sku.GetSkuPartNumber())
			{
				skuInfo.m_Id = *sku.GetSkuId();
				skuInfo.m_Name = *sku.GetSkuPartNumber();
			}
			for (auto& plan : sku.GetServicePlans())
			{
				if (plan.m_ServicePlanId)
				{
					ASSERT(!plan.m_ServicePlanId->IsEmpty());
					if (!plan.m_ServicePlanId->IsEmpty())
					{
						skuInfo.m_PlanIds.insert(*plan.m_ServicePlanId);
						if (plan.m_ServicePlanName)
						{
							auto& planInfo = m_PlanInfoById[*plan.m_ServicePlanId];
							planInfo.m_Id = *plan.m_ServicePlanId;
							planInfo.m_Name = *plan.m_ServicePlanName;
						}
					}
				}
			}
		}
	}
}
