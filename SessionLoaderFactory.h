#pragma once

#include "SessionIdentifier.h"

class ISessionLoader;

class SessionLoaderFactory
{
public:
	SessionLoaderFactory(const SessionIdentifier& p_Identifier);

	std::unique_ptr<ISessionLoader> CreateLoader();

private:
	SessionIdentifier m_Identifier;
};

