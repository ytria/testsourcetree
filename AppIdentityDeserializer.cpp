#include "AppIdentityDeserializer.h"

void AppIdentityDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("appId"), m_Data.m_AppId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("servicePrincipalId"), m_Data.m_ServicePrincipalId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("servicePrincipalName"), m_Data.m_ServicePrincipalName, p_Object);
}
