#pragma once

#include "SessionIdentifier.h"

class SessionIdsEmitter
{
public:
	SessionIdsEmitter(const SessionIdentifier& p_Identifier);
	wstring GetId() const;

private:
	wstring CreateId() const;

	const SessionIdentifier& m_Identifier;
	static map<SessionIdentifier, wstring> g_Ids;
};

