#include "PSUtil.h"

#include "TimeUtil.h"

void PSUtil::AddParamToCmd(wstring& p_Cmd, const wstring& p_PropName, const boost::YOpt<bool>& p_Val)
{
	if (p_Val)
	{
		p_Cmd += _YTEXT(" -");
		p_Cmd += p_PropName + L' ';
		p_Cmd += p_Val ? L'1' : '0';
	}
}

void PSUtil::AddParamToCmd(wstring& p_Cmd, const wstring& p_PropName, const boost::YOpt<vector<PooledString>>& p_Val, const wstring& p_Separator)
{
	// TODO: Support multiple replace properties. This does NOT work if we edit more than one multivalue field in the saame request
	if (p_Val)
	{
		if (p_Val->size() == 1 && (*p_Val)[0] == GridBackendUtil::g_NoValueString) // This case means "Empty"
		{
			p_Cmd += _YTEXTFORMAT(LR"( -Clear %s)", p_PropName.c_str());
		}
		else
		{
			wstring joinedStr = Str::implode(*p_Val, p_Separator);
			p_Cmd += _YTEXTFORMAT(LR"( -replace @{%s="%s" -split "%s"})", p_PropName.c_str(), joinedStr.c_str(), p_Separator.c_str());
		}
	}
}

void PSUtil::AddParamToCmd(wstring& p_Cmd, const wstring& p_PropName, const boost::YOpt<wstring>& p_Val)
{
	if (p_Val)
	{
		p_Cmd += _YTEXT(" -");
		p_Cmd += p_PropName + _YTEXT(" \"");
		p_Cmd += *p_Val;
		p_Cmd += L'\"';
	}
}

void PSUtil::AddParamToCmd(wstring& p_Cmd, const wstring& p_PropName, const boost::YOpt<YTimeDate>& p_Date)
{
	if (p_Date)
	{
		p_Cmd += _YTEXT(" -");
		p_Cmd += p_PropName + _YTEXT(" \"");
		p_Cmd += TimeUtil::GetInstance().GetISO8601String(*p_Date
			, p_Date->HasDate() && p_Date->HasTime() ? TimeUtil::ISO8601_HAS_DATE_AND_TIME : p_Date->HasDate() ? TimeUtil::ISO8601_HAS_DATE : TimeUtil::ISO8601_HAS_TIME
			, true);
		p_Cmd += L'\"';
	}
}

void PSUtil::AddParamToCmd(wstring& p_Cmd, const wstring& p_PropName, const boost::YOpt<PooledString>& p_Val)
{
	if (p_Val)
	{
		p_Cmd += _YTEXT(" -");
		p_Cmd += p_PropName + _YTEXT(" \"");
		p_Cmd += *p_Val;
		p_Cmd += L'\"';
	}
}
