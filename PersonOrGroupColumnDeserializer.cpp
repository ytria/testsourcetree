#include "PersonOrGroupColumnDeserializer.h"

#include "JsonSerializeUtil.h"

void PersonOrGroupColumnDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowMultipleSelection"), m_Data.AllowMultipleSelection, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("displayAs"), m_Data.DisplayAs, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("chooseFromType"), m_Data.ChooseFromType, p_Object);
}
