#pragma once

#include "EmailInfo.h"
#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "YAdvancedHtmlView.h"

// FIXME: This class seems unused. Consider deleting it.

class DlgEmailConsent : public ResizableDialog, public YAdvancedHtmlView::IPostedDataTarget, public YAdvancedHtmlView::IPostedURLTarget
{
public:
	DlgEmailConsent(bool p_UltraAdmin, CWnd* p_Parent);
    virtual ~DlgEmailConsent() = default;

	enum { IDD = IDD_DLG_EMAILCONSENT };

	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data);
	virtual bool OnNewPostedURL(const wstring& url);

	const wstring& GetRecipients() const;
	const wstring& GetCCRecipients() const;
    const wstring& GetSubject() const;
    const wstring& GetContent() const;
    const wstring& GetTenantName() const;
    const wstring& GetAppId() const;
	const wstring& GetRedirectURL() const;

    EmailInfo GetEmailInfoForAdvancedUserSession() const;
    EmailInfo GetEmailInfoForUltraAdminSession() const;

protected:
	virtual BOOL OnInitDialogSpecificResizable() override;

	void OnSelectEmailRecipients();

	ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);

    wstring getEmailTemplate() const;
    wstring FillConsentUrlPlaceholders(wstring p_Content, const wstring& p_ConsentUrl) const;

private:
	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;
	bool m_UltraAdmin;
	wstring m_Recipients;
	wstring m_CCRecipients;
	wstring m_Subject;
	wstring m_TenantName;
    wstring m_AppId;
    wstring m_Content;
	wstring m_RedirectURL;
	wstring	m_CancelButtonText;
	wstring	m_SendButtonText;

	static const wstring g_EmailIDFieldName;
	static const wstring g_ForwardIDFieldName;
	static const wstring g_SubjectIDFieldName;
	static const wstring g_ContentIDFieldName;
	static const wstring g_TenantFieldName;
    static const wstring g_AppIdFieldName;
	static const wstring g_RedirectURLFieldName;
};

