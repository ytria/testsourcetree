#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "FileSystemInfo.h"

class FileSystemInfoDeserializer : public JsonObjectDeserializer, public Encapsulate<FileSystemInfo>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

