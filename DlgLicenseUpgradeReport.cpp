#include "DlgLicenseUpgradeReport.h"

#include "DlgLicenseMessageBoxHTML.h"
#include "JSONUtil.h"
#include "LicenseUtil.h"
#include "MainFrame.h"
#include "MFCUtil.h"

std::map<wstring, int, Str::keyLessInsensitive> DlgLicenseUpgradeReport::g_Sizes;
const wstring DlgLicenseUpgradeReport::g_JSONBtnNameOK	= _YTEXT("BTN_OK");

DlgLicenseUpgradeReport::DlgLicenseUpgradeReport(const uint32_t p_NewCapacity, const CRMpipe::RequestResponse& rr, CWnd* p_Parent)
	: m_NewCapacity(p_NewCapacity)
	, m_Report(rr)
	, ResizableDialog(IDD, p_Parent)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
{
	const bool success = Str::LoadSizeFile(IDR_LICENSEPOSTUSERCOUNTCHANGE_SIZE, g_Sizes);
	ASSERT(success);
}

BOOL DlgLicenseUpgradeReport::OnInitDialogSpecificResizable()
{
	SetWindowText(YtriaTranslate::Do(DlgLicenseUpgradeReport_OnInitDialogSpecificResizable_1, _YLOC("License Modification")).c_str());

	CWnd* pCtrl = GetDlgItem(IDC_DLG_LICENSEUPGRADEREPORTHTML);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_DLG_LICENSEUPGRADEHTML, NULL);

	// FIXME: Temporary - Remove when no more needed.
	//m_HtmlView->SetDebugDisplayPostedData(true);
	
	m_HtmlView->SetPostedDataTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
	{
		{ _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } },
		{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-licensePostUserCountChange.js"), _YTEXT("") } }, // This file is in resources
		{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("licensePostUserCountChange.js"), _YTEXT("") } } // This file is in resources
	}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("licensePostUserCountChange.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	auto		mapReport	= m_Report.GetMap(_YTEXT("result"));	
	const bool	Use2Tables	= Str::getDoubleFromString(mapReport[CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerNetPrice]) > 0;

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const auto it = Use2Tables ? g_Sizes.find(_YTEXT("min-height-2table")) : g_Sizes.find(_YTEXT("min-height-1table"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-width"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);

	return TRUE;
}


bool DlgLicenseUpgradeReport::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	endDialogFixed(IDOK);
	return false;
}

void DlgLicenseUpgradeReport::generateJSONScriptData(wstring& p_Output)
{
	auto mapReport	= m_Report.GetMap(_YTEXT("result"));
	auto mapSentTo	= m_Report.GetMap(_YTEXT("sentto"));

	vector<web::json::value> dlgJsonItems;
	dlgJsonItems.push_back(	JSON_BEGIN_OBJECT
								JSON_PAIR_KEY(_YTEXT("hbs"))						JSON_PAIR_VALUE(JSON_STRING(_YTEXT("licensePostUserCountChange"))),
								JSON_PAIR_KEY(_YTEXT("labelIntro"))					JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgLicenseUpgradeReport_generateJSONScriptData_1, _YLOC("Your license modification has been requested � further action is required")).c_str())),
								JSON_PAIR_KEY(_YTEXT("labelText"))					JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgLicenseUpgradeReport_generateJSONScriptData_8, _YLOC("Here is a preview of your new subscription pricing with a capacity of %1 users:"), Str::getStringFromNumber(m_NewCapacity).c_str()))),
								JSON_PAIR_KEY(_YTEXT("NextBillingCustomerTitle"))	JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgLicenseUpgradeReport_generateJSONScriptData_2, _YLOC("Your next invoice amount")).c_str())),
								JSON_PAIR_KEY(_YTEXT("AlignmentCustomerTitle"))		JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgLicenseUpgradeReport_generateJSONScriptData_3, _YLOC("Remaining balance due immediately")).c_str())),
								JSON_PAIR_KEY(_YTEXT("labelNet"))					JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgLicenseUpgradeReport_generateJSONScriptData_4, _YLOC("NET")).c_str())),
								JSON_PAIR_KEY(_YTEXT("labelVat"))					JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgLicenseUpgradeReport_generateJSONScriptData_5, _YLOC("VAT")).c_str())),
								JSON_PAIR_KEY(_YTEXT("labelTotal"))					JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgLicenseUpgradeReport_generateJSONScriptData_6, _YLOC("Total")).c_str())),

								JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultPriceCurrencyId)					JSON_PAIR_VALUE(JSON_STRING(mapReport[CRMpipe::g_CRMfieldKey_UpgradeResultPriceCurrencyId])),
								JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingDate)					JSON_PAIR_VALUE(JSON_STRING(mapReport[CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingDate])),
								JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerNetPrice)		JSON_PAIR_VALUE(JSON_STRING(mapReport[CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerNetPrice])),
								JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerVatPrice)		JSON_PAIR_VALUE(JSON_STRING(mapReport[CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerVatPrice])),
								JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerGrossPrice)	JSON_PAIR_VALUE(JSON_STRING(mapReport[CRMpipe::g_CRMfieldKey_UpgradeResultNextBillingCustomerGrossPrice])),
								JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerNetPrice)		JSON_PAIR_VALUE(JSON_STRING(mapReport[CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerNetPrice])),
								JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerVatPrice)		JSON_PAIR_VALUE(JSON_STRING(mapReport[CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerVatPrice])),
								JSON_PAIR_KEY(CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerGrossPrice)		JSON_PAIR_VALUE(JSON_STRING(mapReport[CRMpipe::g_CRMfieldKey_UpgradeResultAlignmentCustomerGrossPrice])),

								JSON_PAIR_KEY(_YTEXT("labelFooter"))				JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgLicenseUpgradeReport_generateJSONScriptData_9, _YLOC("A confirmation email containing a link to validate this request was sent to %1 at %2.<BR>This request must be validated for your changes to take effect."), mapSentTo[CRMpipe::g_CRMfieldKey_UpgradeSentToName].c_str(), mapSentTo[CRMpipe::g_CRMfieldKey_UpgradeSentToEmail].c_str()))),
							JSON_END_OBJECT);

	dlgJsonItems.push_back(JSON_BEGIN_OBJECT
								JSON_PAIR_KEY(_YTEXT("hbs")) JSON_PAIR_VALUE(JSON_STRING(_YTEXT("Button"))),
								JSON_BEGIN_PAIR
										_YTEXT("choices"),
										JSON_BEGIN_ARRAY
											JSON_BEGIN_OBJECT
												JSON_PAIR_KEY(_YTEXT("type"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("submit"))),
												JSON_PAIR_KEY(_YTEXT("label"))		JSON_PAIR_VALUE(JSON_STRING(YtriaTranslate::Do(DlgLicenseUpgradeReport_generateJSONScriptData_7, _YLOC("OK")).c_str())),
												JSON_PAIR_KEY(_YTEXT("attribute"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("post"))),
												JSON_PAIR_KEY(_YTEXT("name"))		JSON_PAIR_VALUE(JSON_STRING(g_JSONBtnNameOK)),
											JSON_END_OBJECT,
									   JSON_END_ARRAY
								   JSON_END_PAIR
						   JSON_END_OBJECT);


	json::value root =
		JSON_BEGIN_OBJECT
			JSON_BEGIN_PAIR
				_YTEXT("main"),
				JSON_BEGIN_OBJECT
					JSON_PAIR_KEY(_YTEXT("id"))		JSON_PAIR_VALUE(JSON_STRING(_YTEXT("DlgLicenseUpgradeReport"))),
					JSON_PAIR_KEY(_YTEXT("method"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("POST"))),
					JSON_PAIR_KEY(_YTEXT("action"))	JSON_PAIR_VALUE(JSON_STRING(_YTEXT("#"))),
				JSON_END_OBJECT
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("items"),
				web::json::value::array(dlgJsonItems)
			JSON_END_PAIR
		JSON_END_OBJECT;

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}