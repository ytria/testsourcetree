#pragma once

#include "Modification.h"

class GridUpdater;

class IMainItemModification : public Modification
{
public:
    using Modification::Modification;
    virtual void RefreshState() = 0;
	virtual void RefreshStatesPostProcess(GridUpdater&) {}
};
