#pragma once

#include "CacheGrid.h"

class GraphCache;

class GridSelectRecipients : public CacheGrid
{
public:
    GridSelectRecipients(GraphCache& p_GraphCache);

    virtual void customizeGrid() override;
    
    void BuildView();

    vector<PooledString> GetSelectedEmails();

private:
    GridBackendColumn* m_ColEmail;
    GridBackendColumn* m_ColDisplayName;
	GraphCache& m_GraphCache;
};

