#include "ServicePlanInfoDeserializer.h"

#include "JsonSerializeUtil.h"

void ServicePlanInfoDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("servicePlanId"), m_Data.m_ServicePlanId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("servicePlanName"), m_Data.m_ServicePlanName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("provisioningStatus"), m_Data.m_ProvisioningStatus, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("appliesTo"), m_Data.m_AppliesTo, p_Object);
}
