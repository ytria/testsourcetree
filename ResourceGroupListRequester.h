#pragma once

#include "IRequester.h"
#include "ResourceGroup.h"
#include "ResourceGroupDeserializer.h"

class SingleRequestResult;

template <class T, class U>
class ValueListDeserializer;

class ResourceGroupListRequester : public IRequester
{
public:
	ResourceGroupListRequester(PooledString p_SubscriptionId);

	TaskWrapper<void> Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const vector<Azure::ResourceGroup>& GetData() const;

private:
	PooledString m_SubscriptionId;
	std::shared_ptr<ValueListDeserializer<Azure::ResourceGroup, Azure::ResourceGroupDeserializer>> m_Deserializer;
	shared_ptr<SingleRequestResult> m_Result;
};

