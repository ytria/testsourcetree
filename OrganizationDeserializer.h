#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Organization.h"

class OrganizationDeserializer : public JsonObjectDeserializer, public Encapsulate<Organization>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

