#include "PatternedRecurrenceDeserializer.h"

#include "JsonSerializeUtil.h"
#include "RecurrencePatternDeserializer.h"
#include "RecurrenceRangeDeserializer.h"

void PatternedRecurrenceDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        RecurrencePatternDeserializer rpd;
        if (JsonSerializeUtil::DeserializeAny(rpd, _YTEXT("pattern"), p_Object))
            m_Data.Pattern = rpd.GetData();
    }

    {
        RecurrenceRangeDeserializer rrd;
        if (JsonSerializeUtil::DeserializeAny(rrd, _YTEXT("range"), p_Object))
            m_Data.Range = rrd.GetData();
    }
}
