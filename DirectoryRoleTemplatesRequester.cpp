#include "DirectoryRoleTemplatesRequester.h"

#include "BusinessDirectoryRoleTemplate.h"
#include "DirectoryRoleTemplateDeserializer.h"
#include "MSGraphSession.h"
#include "ValueListDeserializer.h"
#include "PaginatedRequestResults.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

DirectoryRoleTemplatesRequester::DirectoryRoleTemplatesRequester(const std::shared_ptr<IRequestLogger>& p_Logger)
	:m_Logger(p_Logger)
{
}

TaskWrapper<void> DirectoryRoleTemplatesRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<BusinessDirectoryRoleTemplate, DirectoryRoleTemplateDeserializer>>();

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
    return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObjects(m_Deserializer, { _YTEXT("directoryRoleTemplates") }, {}, false, m_Logger, httpLogger, p_TaskData);
}

const vector<BusinessDirectoryRoleTemplate>& DirectoryRoleTemplatesRequester::GetData() const
{
    return m_Deserializer->GetData();
}
