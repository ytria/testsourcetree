#pragma once

#include "DlgFormsHTML.h"

class PersistentSession;
class GraphCache;

class DlgOnPremisesOptions : public DlgFormsHTML
{
public:
	DlgOnPremisesOptions(CWnd* p_Parent, bool p_UseOnPrem, bool p_DontAskOnLogin, const wstring& p_AADComputername, bool p_AutoLoadOnPrem, bool p_UseConsistencyGuid, bool p_ConnectToRemoteRSAT, const wstring& p_Server, const wstring& p_Username);

	bool processPostedData(const IPostedDataTarget::PostedData& data) override;

	const boost::YOpt<bool>& GetUseOnPrem() const;
	const boost::YOpt<bool>& GetDontAskAgainUseOnPrem() const;
	const boost::YOpt<wstring>& GetAADComputerName() const;
	const boost::YOpt<bool>& GetAutoLoadOnPrem() const;
	const boost::YOpt<bool>& GetUseConsistencyGuid() const;
	const boost::YOpt<bool>& GetConnectToRemoteRSAT() const;
	const boost::YOpt<wstring>& GetADDSServer() const;
	const boost::YOpt<wstring>& GetADDSUsername() const;
	const boost::YOpt<wstring>& GetADDSPassword() const;

	int getMinWidthOverride() const override;

	void SetVerifiedDomains(const vector<wstring>& p_Domains);
	void SetADDSDomain(const wstring& p_Domain);

	static void SaveOnPremisesOptions(const DlgOnPremisesOptions& p_Dlg, PersistentSession& p_Session, GraphCache* p_Cache);

protected:
	wstring getDialogTitle() const override;

private:
	void generateJSONScriptData() override;

	wstring m_ADDSDomain;
	vector<wstring> m_VerifiedDomains;

	bool m_InitialUseOnPrem;
	bool m_InitialDontAskOnLogin;
	wstring m_InitialAADComputerName;
	bool m_InitialAutoLoadOnPrem;
	bool m_InitialUseConsistencyGuid;
	bool m_InitialConnectToRemoteRSAT;
	wstring m_InitialADDSServer;
	wstring m_InitialADDSUsername;
	wstring m_InitialADDSPassword;

	boost::YOpt<bool> m_UseOnPrem;
	boost::YOpt<bool> m_DontAskAgainUseOnPrem;
	boost::YOpt<wstring> m_AADComputerName;
	boost::YOpt<bool> m_AutoLoadOnPrem;
	boost::YOpt<bool> m_UseConsistencyGuid;
	boost::YOpt<bool> m_ConnectToRemoteRSAT;
	boost::YOpt<wstring> m_ADDSServer;
	boost::YOpt<wstring> m_ADDSUsername;
	boost::YOpt<wstring> m_ADDSPassword;
};

