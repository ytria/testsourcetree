#include "DlgSchoolEditHTML.h"
#include "AutomationNames.h"

DlgSchoolEditHTML::DlgSchoolEditHTML(vector<EducationSchool>& p_Schools, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
	: DlgFormsHTML(p_Action, p_Parent, p_Action == DlgFormsHTML::Action::CREATE ? g_ActionNameCreateSchool : g_ActionNameSelectedEditSchool)
	, m_Schools(p_Schools)
{

}

void DlgSchoolEditHTML::generateJSONScriptData()
{
	initMain(_YTEXT("Edit Schools"), false, false, true);

	getGenerator().addCategory(_T("General Info"), CategoryFlags::EXPAND_BY_DEFAULT);
	addStringEditor(&EducationSchool::GetDisplayName, &EducationSchool::SetDisplayName, _YTEXT("displayName"), _T("Display Name"), 0);
	addStringEditor(&EducationSchool::GetDescription, &EducationSchool::SetDescription, _YTEXT("schoolDescription"), _T("Description"), 0);
	addStringEditor(&EducationSchool::GetStatus, &EducationSchool::SetStatus, _YTEXT("schoolStatus"), _T("Status"), 0);
	addStringEditor(&EducationSchool::GetExternalSource, &EducationSchool::SetExternalSource, _YTEXT("schoolExternalSource"), _T("External Source"), 0);
	addStringEditor(&EducationSchool::GetPrincipalEmail, &EducationSchool::SetPrincipalEmail, _YTEXT("schoolPrincipalEmail"), _T("Principal Email"), 0);
	addStringEditor(&EducationSchool::GetPrincipalName, &EducationSchool::SetPrincipalName, _YTEXT("schoolPrincipalName"), _T("Principal Name"), 0);
	addStringEditor(&EducationSchool::GetExternalPrincipalId, &EducationSchool::SetExternalPrincipalId, _YTEXT("schoolExternalPrincipalId"), _T("External Principal Id"), 0);
	addStringEditor(&EducationSchool::GetHighestGrade, &EducationSchool::SetHighestGrade, _YTEXT("schoolHighestGrade"), _T("Highest Grade"), 0);
	addStringEditor(&EducationSchool::GetLowestGrade, &EducationSchool::SetLowestGrade, _YTEXT("schoolLowestGrade"), _T("Lowest Grade"), 0);
	addStringEditor(&EducationSchool::GetSchoolNumber, &EducationSchool::SetSchoolNumber, _YTEXT("schoolNumber"), _T("School Number"), 0);
	addStringEditor(&EducationSchool::GetExternalId, &EducationSchool::SetExternalId, _YTEXT("schoolExternalId"), _T("External Id"), 0);
	addStringEditor(&EducationSchool::GetPhone, &EducationSchool::SetPhone, _YTEXT("schoolPhone"), _T("Phone"), 0);
	addStringEditor(&EducationSchool::GetFax, &EducationSchool::SetFax, _YTEXT("schoolFax"), _T("Fax"), 0);

	getGenerator().addCategory(_T("Address Info"), CategoryFlags::NOFLAG);
	addStringEditor(&EducationSchool::GetAddressCity, &EducationSchool::SetAddressCity, _YTEXT("schoolAddressCity"), _T("Address City"), 0);
	addStringEditor(&EducationSchool::GetAddressCountryOrRegion, &EducationSchool::SetAddressCountryOrRegion, _YTEXT("schoolAddressCountryOrRegion"), _T("Address Country or region"), 0);
	addStringEditor(&EducationSchool::GetAddressPostalCode, &EducationSchool::SetAddressPostalCode, _YTEXT("schoolAddressPostalCode"), _T("Address Postal Code"), 0);
	addStringEditor(&EducationSchool::GetAddressState, &EducationSchool::SetAddressState, _YTEXT("schoolAddressState"), _T("Address State"), 0);
	addStringEditor(&EducationSchool::GetAddressStreet, &EducationSchool::SetAddressStreet, _YTEXT("schoolAddressStreet"), _T("Address Street"), 0);

	getGenerator().addApplyButton(isEditDialog() ? LocalizedStrings::g_ApplyButtonText : LocalizedStrings::g_CreateButtonText);
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
	getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);

	for (auto& school : m_Schools)
	{
		school.SetDisplayName(boost::none);
		school.SetDescription(boost::none);
		school.SetStatus(boost::none);
		school.SetExternalSource(boost::none);
		school.SetPrincipalEmail(boost::none);
		school.SetPrincipalName(boost::none);
		school.SetExternalPrincipalId(boost::none);
		school.SetHighestGrade(boost::none);
		school.SetLowestGrade(boost::none);
		school.SetSchoolNumber(boost::none);
		school.SetExternalId(boost::none);
		school.SetPhone(boost::none);
		school.SetFax(boost::none);

		school.SetAddressCity(boost::none);
		school.SetAddressCountryOrRegion(boost::none);
		school.SetAddressPostalCode(boost::none);
		school.SetAddressState(boost::none);
		school.SetAddressStreet(boost::none);
	}
}

wstring DlgSchoolEditHTML::getDialogTitle() const
{
	return _T("Edit schools");
}

bool DlgSchoolEditHTML::processPostedData(const IPostedDataTarget::PostedData& properties)
{
	for (const auto& property : properties)
	{
		const auto& propName = property.first;
		const auto& propVal = property.second;
		ASSERT(!propName.empty());

		auto itt = m_StringSetters.find(propName);
		ASSERT(itt != m_StringSetters.end());
		if (itt != m_StringSetters.end())
		{
			for (auto& school : m_Schools)
			{
				const auto& setter = itt->second;
				setter(school, boost::YOpt<PooledString>(propVal));
			}
		}
	}

	return true;
}

void DlgSchoolEditHTML::addStringEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags)
{
	addObjectStringEditor<EducationSchool>(m_Schools
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags
		, [p_Flags](const EducationSchool& curSchool)
		{
			return 0; // TODO
			// getRestrictionsImpl(bg, p_Flags, p_ApplicableGroupTypes);
		}
	);

	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

void DlgSchoolEditHTML::addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues)
{
	addObjectListEditor<EducationSchool>(m_Schools
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags
		, p_LabelsAndValues
		, [p_Flags](const EducationSchool& curSchool)
		{
			return 0; // TODO
			//return getRestrictionsImpl(bu, p_Flags, p_ApplicableUserTypes);
		}
	);
	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

bool DlgSchoolEditHTML::hasProperty(const wstring& p_PropertyName) const
{
	return m_StringSetters.find(p_PropertyName) != m_StringSetters.end();
}
