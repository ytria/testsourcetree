#include "PublicationFacetDeserializer.h"

#include "JsonSerializeUtil.h"

void PublicationFacetDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("level"), m_Data.m_Level, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("versionId"), m_Data.m_VersionId, p_Object);
}
