#include "O365AdminErrorHandler.h"

#include "LoggerService.h"
#include "MessageContainerObjects.h"
#include "MFCUtil.h"
#include "RestSession.h"
#include "Sapio365Session.h"
#include "YtriaTranslate.h"
#include "YCodeJockMessageBox.h"

O365AdminErrorHandler::O365AdminErrorHandler(const std::shared_ptr<RestSession>& p_Session)
	: m_Session(p_Session)
{
}

O365AdminErrorHandler::O365AdminErrorHandler(const std::shared_ptr<RestSession>& p_Session, const std::shared_ptr<Sapio365Session>& p_SapioSession)
	: m_Session(p_Session)
	, m_Sapio365Session(p_SapioSession)
{
}

wstring O365AdminErrorHandler::HandleRestException(const RestException& e, HWND p_Originator) noexcept
{
	const wstring error = _YFORMAT(L"HTTP %s: %s", std::to_wstring(e.GetRequestResult().Response.status_code()).c_str(), e.WhatUnicode().c_str());
    LoggerService::Error(error, p_Originator);
	return error;
}

wstring O365AdminErrorHandler::HandleHttpException(const web::http::http_exception& e, HWND p_Originator) noexcept
{
	const wstring error = Str::convertFromUTF8(e.what());
    LoggerService::Error(error, p_Originator);
	return error;
}

wstring O365AdminErrorHandler::HandleOAuth2Exception(const web::http::oauth2::experimental::oauth2_exception& e, YtriaTaskData p_TaskData) noexcept
{
	const wstring error = Str::convertFromUTF8(e.what());
    LoggerService::Error(error, p_TaskData.GetOriginator());
	m_Session->SetAuthenticationState(RestSession::AuthenticationState::NotReady);
	p_TaskData.Cancel();

	auto sapio365Session = getSapio365Session();
	if (sapio365Session)
	{
		LoggerService::Debug(_T("Signing-out because of oauth2 exception"), p_TaskData.GetOriginator());
		YCallbackMessage::DoPost(CWnd::FromHandle(p_TaskData.GetOriginator()), [=] {
			
			YCodeJockMessageBox dlgErr(
				CWnd::FromHandle(p_TaskData.GetOriginator())
				, DlgMessageBox::eIcon_ExclamationWarning
				, _T("Signed-out")
				, _T("You authentication token is no longer valid. Please login to continue.")
				, error
				, { { IDOK, _YTEXT("OK") } }
			);
			dlgErr.DoModal();

			Sapio365Session::SignOut(sapio365Session, CWnd::FromHandle(p_TaskData.GetOriginator()));
		});
	}

	return error;
}

wstring O365AdminErrorHandler::HandleTaskCanceledException(const pplx::task_canceled& e, HWND p_Originator) noexcept
{
	const wstring error = Str::convertFromUTF8(e.what());
    LoggerService::Error(error, p_Originator);
	return error;
}

wstring O365AdminErrorHandler::HandleStdException(const std::exception& e, HWND p_Originator) noexcept
{
	const wstring error = Str::convertFromUTF8(e.what());
    LoggerService::Error(error, p_Originator);
	return error;
}

wstring O365AdminErrorHandler::HandleUnknownException(HWND p_Originator) noexcept
{
	const wstring error = YtriaTranslate::DoError(O365AdminErrorHandler_HandleUnknownException_1, _YLOC("An unknown exception occurred"),_YR("Y2502")).c_str();
    LoggerService::Error(error, p_Originator);
	return error;
}

std::shared_ptr<Sapio365Session> O365AdminErrorHandler::getSapio365Session() const
{
	return m_Sapio365Session.lock();
}
