#include "SendMessageRequester.h"
#include "HttpResultWithError.h"
#include "MsGraphHttpRequestLogger.h"

SendMessageRequester::SendMessageRequester(const wstring& p_MessageId)
	:m_MessageId(p_MessageId),
	m_UseMainMSGraphSession(false)
{
}

TaskWrapper<void> SendMessageRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	web::uri_builder uri;
	uri.append_path(_YTEXT("me"));
	uri.append_path(_YTEXT("messages"));
	uri.append_path(m_MessageId);
	uri.append_path(_YTEXT("send"));

	m_Result = std::make_shared<HttpResultWithError>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());

	auto graphSession = p_Session->GetMSGraphSession(httpLogger->GetSessionType());
	if (m_UseMainMSGraphSession)
		graphSession = p_Session->GetMainMSGraphSession();

	return graphSession->Post(uri.to_uri(), httpLogger, p_TaskData).Then([result = m_Result](const RestResultInfo& p_Result) {
		*result = HttpResultWithError(p_Result);
	});
}

const HttpResultWithError& SendMessageRequester::GetResult() const
{
	return *m_Result;
}

void SendMessageRequester::UseMainMSGraphSession()
{
	m_UseMainMSGraphSession = true;
}
