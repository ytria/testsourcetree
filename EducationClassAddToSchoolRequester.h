#pragma once

#include "IRequester.h"
#include "HttpResultWithError.h"

class EducationClassAddToSchoolRequester : public IRequester
{
public:
	EducationClassAddToSchoolRequester(const wstring& p_SchoolId, const wstring& p_ClassId);
	
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const HttpResultWithError& GetResult() const;

private:
	std::shared_ptr<HttpResultWithError> m_Result;

	wstring m_SchoolId;
	wstring m_ClassId;
};

