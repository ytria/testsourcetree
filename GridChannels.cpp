#include "GridChannels.h"

#include "AutomationWizardChannels.h"
#include "BasicGridSetup.h"
#include "BusinessGroupConfiguration.h"
#include "FrameChannels.h"
#include "GridUpdater.h"
#include "UpdatedObjectsGenerator.h"

#define ENABLE_SHOW_ACTIONS_ON_PARENT 0

BEGIN_MESSAGE_MAP(GridChannels, ModuleO365Grid<BusinessChannel>)
	ON_COMMAND(ID_CHANNELSGRID_LOADMORE, OnChannelLoadMore)
	ON_UPDATE_COMMAND_UI(ID_CHANNELSGRID_LOADMORE, OnUpdateChannelLoadMore)

	NEWMODULE_COMMAND(ID_CHANNELSGRID_SHOWMESSAGES, ID_CHANNELSGRID_SHOWMESSAGES_FRAME, ID_CHANNELSGRID_SHOWMESSAGES_NEWFRAME, OnShowMessages, OnUpdateShowMessages)
	NEWMODULE_COMMAND(ID_CHANNELSGRID_SHOWMEMBERS, ID_CHANNELSGRID_SHOWMEMBERS_FRAME, ID_CHANNELSGRID_SHOWMEMBERS_NEWFRAME, OnShowMembers, OnUpdateShowMembers)
	NEWMODULE_COMMAND(ID_CHANNELSGRID_SHOWSITES, ID_CHANNELSGRID_SHOWSITES_FRAME, ID_CHANNELSGRID_SHOWSITES_NEWFRAME, OnShowSites, OnUpdateShowSites)
	NEWMODULE_COMMAND(ID_CHANNELSGRID_SHOWDRIVEITEMS, ID_CHANNELSGRID_SHOWDRIVEITEMS_FRAME, ID_CHANNELSGRID_SHOWDRIVEITEMS_NEWFRAME, OnShowDriveItems, OnUpdateShowDriveItems)
END_MESSAGE_MAP()

GridChannels::GridChannels()
    : m_FrameChannels(nullptr)
	, m_ColDisplayName(nullptr)
	, m_ColDescription(nullptr)
	, m_ColEmail(nullptr)
	, m_ColIsFavoriteByDefault(nullptr)
	, m_ColWebUrl(nullptr)
	, m_ColMembershipType(nullptr)
	, m_ColPrivateChannelMembersDisplayName(nullptr)
	, m_ColPrivateChannelMembersUserID(nullptr)
	, m_ColFolderId(nullptr)
	, m_ColFolderWebURL(nullptr)
	, m_ColFolderCreatedDateTime(nullptr)
	, m_ColFolderLastModifiedDateTime(nullptr)
	//, m_ColFolderSharepointIDsListID(nullptr)
	//, m_ColFolderSharepointIDsSiteID(nullptr)
	//, m_ColFolderSharepointIDsSiteURL(nullptr)
	//, m_ColFolderSharepointIDsWebID(nullptr)
	, m_ColDriveName(nullptr)
	, m_ColDriveId(nullptr)
	, m_ColDriveDescription(nullptr)
	, m_ColDriveQuotaState(nullptr)
	, m_ColDriveQuotaUsed(nullptr)
	, m_ColDriveQuotaRemaining(nullptr)
	, m_ColDriveQuotaDeleted(nullptr)
	, m_ColDriveQuotaTotal(nullptr)
	, m_ColDriveCreationTime(nullptr)
	, m_ColDriveLastModifiedDateTime(nullptr)
	, m_ColDriveType(nullptr)
	, m_ColDriveWebUrl(nullptr)
	, m_ColDriveCreatedByUserEmail(nullptr)
	, m_ColDriveCreatedByUserId(nullptr)
	, m_ColDriveCreatedByAppName(nullptr)
	, m_ColDriveCreatedByAppId(nullptr)
	, m_ColDriveCreatedByDeviceName(nullptr)
	, m_ColDriveCreatedByDeviceId(nullptr)
	, m_ColDriveOwnerUserName(nullptr)
	, m_ColDriveOwnerUserId(nullptr)
	, m_ColDriveOwnerUserEmail(nullptr)
	, m_ColDriveOwnerAppName(nullptr)
	, m_ColDriveOwnerAppId(nullptr)
	, m_ColDriveOwnerDeviceName(nullptr)
	, m_ColDriveOwnerDeviceId(nullptr)
{
	initWizard<AutomationWizardChannels>(_YTEXT("Automation\\Channels"));
}

ModuleCriteria GridChannels::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_FrameChannels->GetModuleCriteria();

    criteria.m_IDs.clear();
    for (auto row : p_Rows)
        criteria.m_IDs.insert(row->GetField(m_Template.m_ColumnID).GetValueStr());

    return criteria;
}

void GridChannels::customizeGrid()
{
	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_GRIDCHANNELS_ACCELERATOR));

	const wstring familyGroupInfo = YtriaTranslate::Do(GridChannels_customizeGrid_2, _YLOC("Group Info")).c_str();
	{
		BasicGridSetup gridSetup(GridUtil::g_AutoNameChannels, LicenseUtil::g_codeSapio365channels,
			{ 
				{ &m_Template.m_ColumnDisplayName,  _T("Group Display Name"), familyGroupInfo },
				{ nullptr, _YTEXT(""), _YTEXT("") },
				{ &m_Template.m_ColumnID, YtriaTranslate::Do(GridChannels_customizeGrid_1, _YLOC("Group ID")).c_str(), familyGroupInfo }
			});
		gridSetup.Setup(*this, false);
	}

	setBasicGridSetupHierarchy({ { { m_Template.m_ColumnID, 0 } }
								,{ rttr::type::get<BusinessChannel>().get_id() }
								,{ rttr::type::get<BusinessChannel>().get_id(), rttr::type::get<BusinessGroup>().get_id() } });

    static const wstring g_FamilyInfo = YtriaTranslate::Do(GridChannels_customizeGrid_3, _YLOC("Info")).c_str();
	addColumnIsLoadMore(_YUID("additionalInfoStatus"), _T("Status - Load Additional Information"));
	m_Template.m_ColumnIsTeam = AddColumnIcon(_YUID("meta.isTeam"), YtriaTranslate::Do(GridGroups_customizeGrid_3, _YLOC("Is a Team")).c_str(), familyGroupInfo);
	MoveColumnBefore(m_Template.m_ColumnIsTeam, m_Template.m_ColumnID);
	m_Template.m_ColumnCombinedGroupType = AddColumn(_YUID("meta.groupType"), YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_5, _YLOC("Group Type")).c_str(), familyGroupInfo, g_ColumnSize12);
	MoveColumnBefore(m_Template.m_ColumnCombinedGroupType, m_Template.m_ColumnID);

	m_MetaColumns.insert(m_MetaColumns.end(), { m_Template.m_ColumnID, m_Template.m_ColumnDisplayName, m_Template.m_ColumnIsTeam , m_Template.m_ColumnCombinedGroupType });

	m_ColDisplayName = AddColumn(_YUID("displayName"), YtriaTranslate::Do(GridChannels_customizeGrid_4, _YLOC("Display Name")).c_str(), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
    m_ColDescription = AddColumn(_YUID("description"), YtriaTranslate::Do(GridChannels_customizeGrid_5, _YLOC("Description")).c_str(), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColEmail = AddColumn(_YUID("email"), _T("Email"), g_FamilyInfo, g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColIsFavoriteByDefault = AddColumnCheckBox(_YUID("isFavoriteByDefault"), _T("Is favorite by default"), g_FamilyInfo, g_ColumnSize6, { g_ColumnsPresetDefault });
	m_ColWebUrl = AddColumnHyperlink(_YUID("webUrl"), _T("Web url"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColMembershipType = AddColumn(_YUID("membershipType"), _T("Membership type"), g_FamilyInfo, g_ColumnSize12, { g_ColumnsPresetDefault });

	/*Add additional meta columns*/
	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		for (const auto& c : *metaDataColumnInfos)
		{
			ASSERT(nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()));
			if (nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()))
			{
				auto col = c.AddTo(*this, familyGroupInfo);
				m_Template.GetColumnForProperty(c.GetPropertyName()) = col;
				m_MetaColumns.emplace_back(col);
				m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
			}
		}
	}

	{
		static const wstring g_FamilyPrivateChannelMembers = _T("Private Channel Members");
		m_ColPrivateChannelMembersDisplayName = AddColumn(_YUID("privateChannelMembers.displayName"), _T("Channel Member"), g_FamilyPrivateChannelMembers, g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetDefault });
		m_ColPrivateChannelMembersUserID = AddColumn(_YUID("privateChannelMembers.userId"), _T("User ID"), g_FamilyPrivateChannelMembers, g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
		//m_ColPrivateChannelMembersEmail = AddColumn(_YUID("privateChannelMembers.email"), _T("User Email"), g_FamilyPrivateChanneldMembers, g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColPrivateChannelMembersUserID->SetMultivalueFamily(_T("Private Channel Members"));
		m_ColPrivateChannelMembersUserID->AddMultiValueExplosionSister(m_ColPrivateChannelMembersUserID);
		//m_ColPrivateChannelMembersUserID->AddMultiValueExplosionSister(m_ColPrivateChannelMembersEmail);
	
		static const wstring g_FamilyDrive = _T("Doc Library");

		m_ColFolderId = AddColumn(_YUID("folderId"), _T("Files Folder Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
		m_ColFolderWebURL = AddColumnHyperlink(_YUID("folderWebUrl"), _T("Files Folder Web Url"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColFolderCreatedDateTime = AddColumnDate(_YUID("folderCreationTime"), _T("Files Folder - Creation Time"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColFolderLastModifiedDateTime = AddColumnDate(_YUID("folderLastModifiedTime"), _T("Files Folder - Last Modified Date Time"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });

		//m_ColFolderSharepointIDsListID	= AddColumn(_YUID("folder.sharepointIds.listId"),		_T("SharePoint IDs - List ID"),				g_FamilyDrive, g_ColumnSize12,	{ g_ColumnsPresetTech });
		//m_ColFolderSharepointIDsSiteURL	= AddColumnHyperlink(_YUID("sharepointIds.siteUrl"),	_T("SharePoint IDs - Site URL"),			g_FamilyDrive, g_ColumnSize8,	{ g_ColumnsPresetTech });
		//m_ColFolderSharepointIDsSiteID	= AddColumn(_YUID("folder.sharepointIds.siteId"),		_T("SharePoint IDs - Site ID"),				g_FamilyDrive, g_ColumnSize12,	{ g_ColumnsPresetTech });
		//m_ColFolderSharepointIDsWebID	= AddColumn(_YUID("folder.sharepointIds.webId"),		_T("SharePoint IDs - Web ID"),				g_FamilyDrive, g_ColumnSize12,	{ g_ColumnsPresetTech });

		m_ColDriveName = AddColumn(_YUID(O365_COLDRIVENAME), _T("Doc Library Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveId = AddColumn(_YUID(O365_COLDRIVEID), _T("Doc Library Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	
		m_ColDriveDescription = AddColumn(_YUID(O365_COLDRIVEDESCRIPTION), _T("Doc Library Description"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveQuotaState = AddColumn(_YUID(O365_COLDRIVEQUOTASTATE), _T("Doc Library - Storage State"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveQuotaUsed = AddColumnNumber(_YUID(O365_COLDRIVEQUOTAUSED), _T("Doc Library - Storage Used"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetDefault });
		m_ColDriveQuotaUsed->SetXBytesColumnFormat();
		m_ColDriveQuotaRemaining = AddColumnNumber(_YUID(O365_COLDRIVEQUOTAREMAINING), _T("Doc Library - Storage Remaining"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveQuotaRemaining->SetXBytesColumnFormat();
		m_ColDriveQuotaDeleted = AddColumnNumber(_YUID(O365_COLDRIVEQUOTADELETED), _T("Doc Library - Storage Deleted"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveQuotaDeleted->SetXBytesColumnFormat();
		m_ColDriveQuotaTotal = AddColumnNumber(_YUID(O365_COLDRIVEQUOTATOTAL), _T("Doc Library - Storage Total"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetDefault });
		m_ColDriveQuotaTotal->SetXBytesColumnFormat();
		m_ColDriveCreationTime = AddColumnDate(_YUID(O365_COLDRIVECREATIONTIME), _T("Doc Library - Creation Time"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveLastModifiedDateTime = AddColumnDate(_YUID(O365_COLDRIVELASTMODIFIEDDATETIME), _T("Doc Library - Last Modified Date Time"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveType = AddColumn(_YUID(O365_COLDRIVETYPE), _T("Doc Library Type"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveWebUrl = AddColumnHyperlink(_YUID(O365_COLDRIVEWEBURL), _T("Doc Library Web Url"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveCreatedByUserEmail = AddColumn(_YUID(O365_COLDRIVECREATEDBYUSEREMAIL), _T("Doc Library - Created By User Email"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveCreatedByUserId = AddColumn(_YUID(O365_COLDRIVECREATEDBYUSERID), _T("Doc Library - Created By User Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
		m_ColDriveCreatedByAppName = AddColumn(_YUID(O365_COLDRIVECREATEDBYAPPNAME), _T("Doc Library - Created By App Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveCreatedByAppId = AddColumn(_YUID(O365_COLDRIVECREATEDBYAPPID), _T("Doc Library - Created By App Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
		m_ColDriveCreatedByDeviceName = AddColumn(_YUID(O365_COLDRIVECREATEDBYDEVICENAME), _T("Doc Library - Created By Device Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveCreatedByDeviceId = AddColumn(_YUID(O365_COLDRIVECREATEDBYDEVICEID), _T("Doc Library - Created By Device Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
		m_ColDriveOwnerUserName = AddColumn(_YUID(O365_COLDRIVEOWNERUSERNAME), _T("Doc Library - Owner - User Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveOwnerUserId = AddColumn(_YUID(O365_COLDRIVEOWNERUSERID), _T("Doc Library - Owner - User Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
		m_ColDriveOwnerUserEmail = AddColumn(_YUID(O365_COLDRIVEOWNERUSEREMAIL), _T("Doc Library - Owner - User Email"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveOwnerAppName = AddColumn(_YUID(O365_COLDRIVEOWNERAPPNAME), _T("Doc Library - Owner - App Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveOwnerAppId = AddColumn(_YUID(O365_COLDRIVEOWNERAPPID), _T("Doc Library - Owner - App Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
		m_ColDriveOwnerDeviceName = AddColumn(_YUID(O365_COLDRIVEOWNERDEVICENAME), _T("Doc Library - Owner - Device Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
		m_ColDriveOwnerDeviceId = AddColumn(_YUID(O365_COLDRIVEOWNERDEVICEID), _T("Doc Library - Owner - Device Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
	}

    addColumnGraphID();
    AddColumnForRowPK(GetColId());
	AddColumnForRowPK(m_Template.m_ColumnID);

	m_Template.CustomizeGrid(*this);

    m_FrameChannels = dynamic_cast<FrameChannels*>(GetParentFrame());
	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_FrameChannels);
}

void GridChannels::customizeGridPostProcess()
{
	O365Grid::customizeGridPostProcess();

	SetNoFieldTextAndIcon(GetColumnsByPresets({ g_ColumnsPresetMore }), IDB_ICON_LM, _T("To see this information, use the 'Load Info' button"), _T(" - Use the 'Load Info' button to display additional information - "));
	SetLoadMoreStatus(true, _T("Loaded"), IDB_ICON_LMSTATUS_LOADED);
	SetLoadMoreStatus(false, _T("Not Loaded"), IDB_ICON_LMSTATUS_NOTLOADED);
}

wstring GridChannels::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, nullptr != p_Row && p_Row->HasField(m_ColDisplayName) ? m_ColDisplayName : m_Template.m_ColumnDisplayName);
}

vector<BusinessChannel> GridChannels::GetLoadMoreRequestInfo(const std::vector<GridBackendRow*>& p_Rows)
{
	vector<BusinessChannel> channels;
	for (auto row : p_Rows)
	{
		if (IsRowForLoadMore(row, nullptr))
		{
			auto businessChannel = getBusinessObject(row);
	
			// avoid useless queries (multivalue explosion side effect)
			if (std::none_of(channels.begin(), channels.end(), [id = businessChannel.GetID()](const BusinessChannel& channel){return id == channel.GetID(); }))
				channels.push_back(businessChannel);
		}
	}
	
	return channels;
}

BusinessChannel GridChannels::getBusinessObject(GridBackendRow* p_Row) const
{
	BusinessChannel channel;

	{
		const auto& field = p_Row->GetField(GetColId());
		if (field.HasValue())
			channel.SetID(field.GetValueStr());
	}

	{
		const auto& field = p_Row->GetField(m_Template.m_ColumnID);
		if (field.HasValue())
			channel.SetGroupId(PooledString(field.GetValueStr()));
	}

	{
		const auto& field = p_Row->GetField(m_ColDisplayName);
		if (field.HasValue())
			channel.SetDisplayName(PooledString(field.GetValueStr()));
	}

	{
		const auto& field = p_Row->GetField(m_ColMembershipType);
		if (field.HasValue())
			channel.SetMembershipType(PooledString(field.GetValueStr()));
	}

	// FIXME: Do we need more fields? (Only used for load more right now...)

	return channel;
}

GridBackendRow* GridChannels::FillRow(GridBackendRow* p_ParentRow, const BusinessChannel& p_Channel)
{
	GridBackendField fID(p_Channel.GetID(), GetColId());
	GridBackendRow* row = m_RowIndex.GetRow({ fID, p_ParentRow->GetField(m_Template.m_ColumnID) }, true, true);

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		// Meta fields
		for (auto c : m_MetaColumns)
		{
			if (p_ParentRow->HasField(c))
				row->AddField(p_ParentRow->GetField(c));
			else
				row->RemoveField(c);
		}

		row->AddField(p_Channel.GetID(), GetColId());
		row->AddField(p_Channel.GetDisplayName(), m_ColDisplayName);
		row->AddField(p_Channel.GetDescription(), m_ColDescription);
		row->AddField(p_Channel.GetEmail(), m_ColEmail);
		row->AddField(p_Channel.GetIsFavoriteByDefault(), m_ColIsFavoriteByDefault);
		row->AddFieldForHyperlink(p_Channel.GetWebUrl(), m_ColWebUrl);
		row->AddField(p_Channel.GetMembershipType(), m_ColMembershipType);

		SetRowObjectType(row, p_Channel, p_Channel.HasFlag(BusinessObject::CANCELED));
	}

	return row;
}

void GridChannels::UpdateChannelsLoadMore(const vector<BusinessChannel>& p_Channels)
{
	GridIgnoreModification ignoramus(*this);
	GridUpdaterOptions updateOptions(GridUpdaterOptions::UPDATE_GRID
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		| GridUpdaterOptions::PROCESS_LOADMORE
	/*| (p_DoNotShowLoadingErrors ? GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG : 0) TODO? */);
	GridUpdater updater(*this, updateOptions);

	for (const auto& channel : p_Channels)
	{
		GridBackendField fID(channel.GetID(), GetColId());
		ASSERT(channel.GetGroupId());
		GridBackendField fmetaID = channel.GetGroupId() ? GridBackendField(*channel.GetGroupId(), m_Template.m_ColumnID) : GridBackendField(_YTEXT(""), m_Template.m_ColumnID);
		GridBackendRow* row = m_RowIndex.GetRow({ fID, fmetaID }, false, true);
		ASSERT(GridUtil::IsBusinessChannel(row));
		if (GridUtil::IsBusinessChannel(row))
		{
			if (channel.GetError())
			{
				ASSERT(false); // Should never happen, as Load More for channel never puts a global error (no main request)
				for (const auto& col : GetColumnsByPresets({ O365Grid::g_ColumnsPresetMore }))
					row->AddField(channel.GetError()->GetFullErrorMessage(), col, GridBackendUtil::ICON_ERROR);

				updater.OnLoadingError(row, *channel.GetError());
			}
			else
			{
				bool has403 = false;
				bool has403Description = false;
				wstring error403;
				vector<PooledString> errors;

				boost::YOpt<vector<PooledString>> names;
				boost::YOpt<vector<PooledString>> ids;
				//boost::YOpt<vector<PooledString>> emails;

				if (channel.GetPrivateChannelMembersError())
				{
					if (channel.GetPrivateChannelMembersError()->GetStatusCode() == web::http::status_codes::Forbidden)
					{
						has403 = true;
						if (!has403Description && error403.empty())
							error403 = channel.GetPrivateChannelMembersError()->GetFullErrorMessage();
						else
							has403Description = false;
					}
					errors.push_back(_YFORMAT(L"Members: %s", channel.GetPrivateChannelMembersError()->GetFullErrorMessage().c_str()));
				}
				else if (channel.GetPrivateChannelMembers())
				{
					names.emplace();
					ids.emplace();
					//emails.emplace();
					for (auto& member : *channel.GetPrivateChannelMembers())
					{
						if (member.GetDisplayName())
							names->emplace_back(*member.GetDisplayName());
						else
							names->emplace_back(GridBackendUtil::g_NoValueString);

						if (member.GetUserID())
							ids->emplace_back(*member.GetUserID());
						else
							ids->emplace_back(GridBackendUtil::g_NoValueString);

						//if (member.GetEmail())
						//	emails->emplace_back(*member.GetEmail());
						//else
						//	emails->emplace_back(GridBackendUtil::g_NoValueString);
					}

					row->AddField(names, m_ColPrivateChannelMembersDisplayName);
					row->AddField(ids, m_ColPrivateChannelMembersUserID);
					//row->AddField(emails, m_ColPrivateChannelMembersEmail);
				}

				if (channel.GetFilesFolder())
				{
					const auto& filesFolder = *channel.GetFilesFolder();
					if (filesFolder.GetError())
					{
						auto setError = [row, errMsg = filesFolder.GetError()->GetFullErrorMessage()](GridBackendColumn* p_Col)
						{
							if (nullptr != p_Col)
								row->AddField(errMsg, p_Col, GridBackendUtil::ICON_ERROR);
						};

						setError(m_ColFolderId);
						setError(m_ColFolderWebURL);
						setError(m_ColFolderCreatedDateTime);
						setError(m_ColFolderLastModifiedDateTime);
						//setError(m_ColFolderSharepointIDsListID);
						//setError(m_ColFolderSharepointIDsSiteURL);
						//setError(m_ColFolderSharepointIDsSiteID);
						//setError(m_ColFolderSharepointIDsWebID);
						setError(m_ColDriveId);
						setError(m_ColDriveType);

						if (filesFolder.GetError()->GetStatusCode() == web::http::status_codes::Forbidden)
						{
							has403 = true;
							if (!has403Description && error403.empty())
								error403 = filesFolder.GetError()->GetFullErrorMessage();
							else
								has403Description = false;
						}
						errors.push_back(_YFORMAT(L"Files Folder: %s", filesFolder.GetError()->GetFullErrorMessage().c_str()));
					}
					else
					{
						row->AddField(filesFolder.GetID(), m_ColFolderId);
						row->AddFieldForHyperlink(filesFolder.GetWebUrl(), m_ColFolderWebURL);

						if (filesFolder.GetFileSystemInfo())
						{
							row->AddField(filesFolder.GetFileSystemInfo()->CreatedDateTime, m_ColFolderCreatedDateTime);
							row->AddField(filesFolder.GetFileSystemInfo()->LastModifiedDateTime, m_ColFolderLastModifiedDateTime);
						}

						if (filesFolder.GetParentReference())
						{
							row->AddField(filesFolder.GetParentReference()->DriveId, m_ColDriveId);
							row->AddField(filesFolder.GetParentReference()->DriveType, m_ColDriveType);
						}

						/*if (filesFolder.GetSharepointIds())
						{
							row->AddField(filesFolder.GetSharepointIds()->ListId, m_ColFolderSharepointIDsListID);
							row->AddField(filesFolder.GetSharepointIds()->SiteId, m_ColFolderSharepointIDsSiteID);
							row->AddField(filesFolder.GetSharepointIds()->SiteUrl, m_ColFolderSharepointIDsSiteURL);
							row->AddField(filesFolder.GetSharepointIds()->WebId, m_ColFolderSharepointIDsWebID);
						}*/
					}
				}

				if (channel.GetDrive())
				{
					const auto& drive = *channel.GetDrive();
					if (drive.GetError())
					{
						auto setError = [row, errMsg = drive.GetError()->GetFullErrorMessage()](GridBackendColumn* p_Col)
						{
							if (nullptr != p_Col)
								row->AddField(errMsg, p_Col, GridBackendUtil::ICON_ERROR);
						};

						setError(m_ColDriveName);
						setError(m_ColDriveDescription);
						setError(m_ColDriveCreationTime);
						setError(m_ColDriveLastModifiedDateTime);
						setError(m_ColDriveWebUrl);
						setError(m_ColDriveQuotaState);
						setError(m_ColDriveQuotaUsed);
						setError(m_ColDriveQuotaRemaining);
						setError(m_ColDriveQuotaDeleted);
						setError(m_ColDriveQuotaTotal);
						setError(m_ColDriveCreatedByUserEmail);
						setError(m_ColDriveCreatedByUserId);
						setError(m_ColDriveCreatedByAppName);
						setError(m_ColDriveCreatedByAppId);
						setError(m_ColDriveCreatedByDeviceName);
						setError(m_ColDriveCreatedByDeviceId);
						setError(m_ColDriveOwnerUserName);
						setError(m_ColDriveOwnerUserId);
						setError(m_ColDriveOwnerAppName);
						setError(m_ColDriveOwnerAppId);
						setError(m_ColDriveOwnerDeviceName);
						setError(m_ColDriveOwnerDeviceId);

						if (drive.GetError()->GetStatusCode() == web::http::status_codes::Forbidden)
						{
							has403 = true;
							if (!has403Description && error403.empty())
								error403 = drive.GetError()->GetFullErrorMessage();
							else
								has403Description = false;
						}
						errors.push_back(_YFORMAT(L"Document Library: %s", drive.GetError()->GetFullErrorMessage().c_str()));
					}
					else
					{
						ASSERT(channel.GetFilesFolder());

						row->AddField(drive.GetName(), m_ColDriveName);
						row->AddField(drive.GetDescription(), m_ColDriveDescription);

						if (drive.GetQuota())
						{
							const auto& quota = *drive.GetQuota();
							row->AddField(quota.State, m_ColDriveQuotaState);
							row->AddField(quota.Used, m_ColDriveQuotaUsed);
							row->AddField(quota.Remaining, m_ColDriveQuotaRemaining);
							row->AddField(quota.Deleted, m_ColDriveQuotaDeleted);
							row->AddField(quota.Total, m_ColDriveQuotaTotal);
						}

						row->AddField(drive.GetCreatedDateTime(), m_ColDriveCreationTime);
						row->AddField(drive.GetLastModifiedDateTime(), m_ColDriveLastModifiedDateTime);
						row->AddFieldForHyperlink(drive.GetWebUrl(), m_ColDriveWebUrl);

						if (drive.GetCreatedBy() != boost::none)
						{
							const auto& createdBy = *drive.GetCreatedBy();
							if (createdBy.User != boost::none)
							{
								const auto& user = *createdBy.User;
								row->AddField(user.Email, m_ColDriveCreatedByUserEmail);
								row->AddField(user.Id, m_ColDriveCreatedByUserId);
							}
							if (createdBy.Application != boost::none)
							{
								const auto& app = *createdBy.Application;
								row->AddField(app.DisplayName, m_ColDriveCreatedByUserEmail);
								row->AddField(app.Id, m_ColDriveCreatedByUserId);
							}
							if (createdBy.Device != boost::none)
							{
								const auto& device = *createdBy.Device;
								row->AddField(device.DisplayName, m_ColDriveCreatedByDeviceName);
								row->AddField(device.Id, m_ColDriveCreatedByDeviceId);
							}
						}

						if (drive.GetOwner() != boost::none)
						{
							const auto& owner = *drive.GetOwner();

							if (owner.User)
							{
								const auto& user = *owner.User;
								row->AddField(user.DisplayName, m_ColDriveOwnerUserName);
								row->AddField(user.Email, m_ColDriveOwnerUserEmail);
							}
							if (owner.Application)
							{
								const auto& app = *owner.Application;
								row->AddField(app.DisplayName, m_ColDriveOwnerUserEmail);
								row->AddField(app.Id, m_ColDriveOwnerUserId);
							}
							if (owner.Device)
							{
								const auto& device = *owner.Device;
								row->AddField(device.DisplayName, m_ColDriveOwnerDeviceName);
								row->AddField(device.Id, m_ColDriveOwnerDeviceId);
							}
						}
					}
				}

				if (!errors.empty())
				{
					if (!row->IsChangesSaved() && !row->IsError()) // Keep existing saved or error status that may be the result of apply.
					{
						SapioError sapioError(has403 ? (has403Description ? error403 : _YTEXT("")) : errors.front(), has403 ? web::http::status_codes::Forbidden : 0);
						updater.OnLoadingError(row, sapioError);
						row->AddField(errors, GetBackend().GetStatusColumn(), row->GetField(GetBackend().GetStatusColumn()).GetIcon());
					}
					else if (has403)
					{
						if (has403Description)
							updater.Set403LoadingError(error403);
						else
							updater.Set403LoadingError();
					}
				}
			}

			updater.GetOptions().AddRowWithRefreshedValues(row);
		}
	}
}

void GridChannels::OnShowMessages()
{
	if (openModuleConfirmation())
	{
		CommandInfo info;
		auto& ids = info.GetSubIds();

		map<PooledString, PooledString> channelNames;
		for (auto& row : GetBackend().GetSelectedRowsInOrder())
		{
			if (IsGroupAuthorizedByRoleDelegation(row->GetField(m_Template.m_ColumnID).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CHANNELS_READ))
			{
				vector<GridBackendRow*> channelRows;
#if ENABLE_SHOW_ACTIONS_ON_PARENT
				if (GridUtil::isBusinessType<BusinessGroup>(row))
					channelRows = GetChildRows(row);
				else
#endif
					channelRows.push_back(row);

				for (auto channelRow : channelRows)
				{
					if (GridUtil::isBusinessType<BusinessChannel>(channelRow))
					{
						auto channelId = channelRow->GetField(GetColId()).GetValueStr();
						ids[PooledString(channelRow->GetField(m_Template.m_ColumnID).GetValueStr())].insert(channelId);
						channelNames[channelId] = channelRow->GetField(m_ColDisplayName).GetValueStr();
					}
				}
			}
		}

		const bool noID = ids.empty();
		if (!noID && showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT))
		{
			info.Data() = channelNames;
			info.SetOrigin(Origin::Channel);
			ASSERT(nullptr != m_FrameChannels);
			if (nullptr != m_FrameChannels)
				info.SetRBACPrivilege(m_FrameChannels->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::ChannelMessages, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowChannelsMessages, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
			AutomationGreenlight(noID ? YtriaTranslate::DoError(GridChannels_OnShowMessages_3, _YLOC("No Channel ID"),_YR("Y2442")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"),_YR("Y2438")).c_str());
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2440")).c_str());
	}
}

void GridChannels::OnUpdateShowMessages(CCmdUI* pCmdUI)
{
	const bool enable = std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& p_Row)
	{
#if ENABLE_SHOW_ACTIONS_ON_PARENT
		return (GridUtil::IsBusinessChannel(p_Row) || GridUtil::IsBusinessGroup(p_Row) && p_Row->HasChildrenRows())
#else
		return GridUtil::IsBusinessChannel(p_Row)
#endif
			&& IsGroupAuthorizedByRoleDelegation(p_Row->GetField(m_Template.m_ColumnID).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CHANNELS_READ);
	});
	
	pCmdUI->Enable(IsFrameConnected() && enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridChannels::OnShowMembers()
{
	if (openModuleConfirmation())
	{
		CommandInfo info;
		auto& ids = info.GetSubIds();

		map<PooledString, PooledString> channelNames;
		for (auto& row : GetBackend().GetSelectedRowsInOrder())
		{
			if (IsGroupAuthorizedByRoleDelegation(row->GetField(m_Template.m_ColumnID).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CHANNELS_READ))
			{
				vector<GridBackendRow*> channelRows;
#if ENABLE_SHOW_ACTIONS_ON_PARENT
				if (GridUtil::IsBusinessGroup(row))
					channelRows = GetChildRows(row);
				else
#endif
					channelRows.push_back(row);

				for (auto channelRow : channelRows)
				{
					if (GridUtil::IsBusinessChannel(channelRow) && wstring(_YTEXT("private")) == channelRow->GetField(m_ColMembershipType).GetValueStr())
					{
						auto channelId = channelRow->GetField(GetColId()).GetValueStr();
						ids[PooledString(channelRow->GetField(m_Template.m_ColumnID).GetValueStr())].insert(channelId);
						channelNames[channelId] = channelRow->GetField(m_ColDisplayName).GetValueStr();
					}
				}
			}
		}

		const bool noID = ids.empty();
		if (!noID && showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT))
		{
			info.Data() = channelNames;
			info.SetOrigin(Origin::Channel);
			ASSERT(nullptr != m_FrameChannels);
			if (nullptr != m_FrameChannels)
				info.SetRBACPrivilege(m_FrameChannels->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::ChannelMembers, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowChannelMembers, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
			AutomationGreenlight(noID ? YtriaTranslate::DoError(GridChannels_OnShowMessages_3, _YLOC("No Channel ID"), _YR("Y2442")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"), _YR("Y2438")).c_str());
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"), _YR("Y2440")).c_str());
	}
}

void GridChannels::OnUpdateShowMembers(CCmdUI* pCmdUI)
{
	const auto channelCondition = [this](auto& p_Row)
	{
		return GridUtil::IsBusinessChannel(p_Row)
			&& wstring(_YTEXT("private")) == p_Row->GetField(m_ColMembershipType).GetValueStr()
			&& IsGroupAuthorizedByRoleDelegation(p_Row->GetField(m_Template.m_ColumnID).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CHANNELS_READ);
	};

	const bool enable =
		std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, channelCondition](auto& p_Row)
		{
#if ENABLE_SHOW_ACTIONS_ON_PARENT
			if (GridUtil::IsBusinessGroup(p_Row) && p_Row->HasChildrenRows())
			{
				auto children = GetChildRows(p_Row);
				return std::any_of(children.begin(), children.end(), [this, channelCondition](auto& p_Row)
					{
						return channelCondition(p_Row);
					});
			}
#endif
			return GridUtil::IsBusinessChannel(p_Row) && channelCondition(p_Row);
		});

	pCmdUI->Enable(IsFrameConnected() && enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridChannels::OnShowSites()
{
	if (openModuleConfirmation())
	{
		CommandInfo info;
		auto& ids = info.GetSubIds();

		map<PooledString, PooledString> channelNames;
		for (auto& row : GetBackend().GetSelectedRowsInOrder())
		{
			if (IsGroupAuthorizedByRoleDelegation(row->GetField(m_Template.m_ColumnID).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CHANNELS_READ))
			{
				vector<GridBackendRow*> channelRows;
#if ENABLE_SHOW_ACTIONS_ON_PARENT
				if (GridUtil::IsBusinessGroup(row))
					channelRows = GetChildRows(row);
				else
#endif
					channelRows.push_back(row);

				for (auto channelRow : channelRows)
				{
					if (GridUtil::IsBusinessChannel(channelRow) && wstring(_YTEXT("private")) == channelRow->GetField(m_ColMembershipType).GetValueStr())
					{
						auto channelId = channelRow->GetField(GetColId()).GetValueStr();
						ids[PooledString(channelRow->GetField(m_Template.m_ColumnID).GetValueStr())].insert(channelId);
						channelNames[channelId] = channelRow->GetField(m_ColDisplayName).GetValueStr();
					}
				}
			}
		}

		const bool noID = ids.empty();
		if (!noID && showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT))
		{
			info.Data() = channelNames;
			info.SetOrigin(Origin::Channel);
			ASSERT(nullptr != m_FrameChannels);
			if (nullptr != m_FrameChannels)
				info.SetRBACPrivilege(m_FrameChannels->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Sites, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowSites, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
			AutomationGreenlight(noID ? YtriaTranslate::DoError(GridChannels_OnShowMessages_3, _YLOC("No Channel ID"), _YR("Y2442")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"), _YR("Y2438")).c_str());
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"), _YR("Y2440")).c_str());
	}
}

void GridChannels::OnUpdateShowSites(CCmdUI* pCmdUI)
{
	const auto channelCondition = [this](auto& p_Row)
	{
		return GridUtil::IsBusinessChannel(p_Row)
			&& wstring(_YTEXT("private")) == p_Row->GetField(m_ColMembershipType).GetValueStr()
			&& IsGroupAuthorizedByRoleDelegation(p_Row->GetField(m_Template.m_ColumnID).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CHANNELS_READ);
	};

	const bool enable = 
		std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, channelCondition](auto& p_Row)
			{
#if ENABLE_SHOW_ACTIONS_ON_PARENT
				if (GridUtil::IsBusinessGroup(p_Row) && p_Row->HasChildrenRows())
				{
					auto children = GetChildRows(p_Row);
					return std::any_of(children.begin(), children.end(), [this, channelCondition](auto& p_Row)
						{
							return channelCondition(p_Row);
						});
				}
#endif
				return GridUtil::IsBusinessChannel(p_Row) && channelCondition(p_Row);
			});

	pCmdUI->Enable(IsFrameConnected() && enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridChannels::OnShowDriveItems()
{
	if (openModuleConfirmation())
	{
		CommandInfo info;
		auto& ids = info.GetSubIds();

		map<PooledString, PooledString> channelNames;
		for (auto& row : GetBackend().GetSelectedRowsInOrder())
		{
			if (IsGroupAuthorizedByRoleDelegation(row->GetField(m_Template.m_ColumnID).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CHANNELS_READ))
			{
				vector<GridBackendRow*> channelRows;
#if ENABLE_SHOW_ACTIONS_ON_PARENT
				if (GridUtil::IsBusinessGroup(row))
					channelRows = GetChildRows(row);
				else
#endif
					channelRows.push_back(row);

				for (auto channelRow : channelRows)
				{
					if (GridUtil::IsBusinessChannel(channelRow) && wstring(_YTEXT("private")) == channelRow->GetField(m_ColMembershipType).GetValueStr())
					{
						auto channelId = channelRow->GetField(GetColId()).GetValueStr();
						ids[PooledString(channelRow->GetField(m_Template.m_ColumnID).GetValueStr())].insert(channelId);
						channelNames[channelId] = channelRow->GetField(m_ColDisplayName).GetValueStr();
					}
				}
			}
		}

		const bool noID = ids.empty();
		if (!noID && showGroupRestrictedAccessDialog(GROUP_ADMIN_REQUIREMENT))
		{
			info.Data() = channelNames;
			info.SetOrigin(Origin::Channel);
			ASSERT(nullptr != m_FrameChannels);
			if (nullptr != m_FrameChannels)
				info.SetRBACPrivilege(m_FrameChannels->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Drive, Command::ModuleTask::ListSubItems, info, { this, g_ActionNameSelectedShowDriveItemsChannels, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
			AutomationGreenlight(noID ? YtriaTranslate::DoError(GridChannels_OnShowMessages_3, _YLOC("No Channel ID"), _YR("Y2442")).c_str() : YtriaTranslate::DoError(O365Grid_OnShowDriveItems_2, _YLOC("Canceled by restricted access"), _YR("Y2438")).c_str());
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"), _YR("Y2440")).c_str());
	}
}

void GridChannels::OnUpdateShowDriveItems(CCmdUI* pCmdUI)
{
	const auto channelCondition = [this](auto& p_Row)
	{
		return GridUtil::IsBusinessChannel(p_Row)
			&& wstring(_YTEXT("private")) == p_Row->GetField(m_ColMembershipType).GetValueStr()
			&& IsGroupAuthorizedByRoleDelegation(p_Row->GetField(m_Template.m_ColumnID).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CHANNELS_READ);
	};

	const bool enable =
		std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this, channelCondition](auto& p_Row)
			{
#if ENABLE_SHOW_ACTIONS_ON_PARENT
				if (GridUtil::IsBusinessGroup(p_Row) && p_Row->HasChildrenRows())
				{
					auto children = GetChildRows(p_Row);
					return std::any_of(children.begin(), children.end(), [this, channelCondition](auto& p_Row)
						{
							return channelCondition(p_Row);
						});
				}
#endif
				return GridUtil::IsBusinessChannel(p_Row) && channelCondition(p_Row);
			});

	pCmdUI->Enable(IsFrameConnected() && enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridChannels::OnChannelLoadMore()
{
	auto updatedGroupsGen = UpdatedObjectsGenerator<BusinessChannel>();

	vector<BusinessChannel> channels;

	vector<GridBackendRow*> rows;
	GetSelectedNonGroupRows(rows);
	for (auto row : rows)
	{
		if (GridUtil::IsBusinessChannel(row))
			channels.emplace_back(getBusinessObject(row));
	}

	updatedGroupsGen.SetObjects(channels);

	CommandInfo info;
	info.Data() = updatedGroupsGen;
	info.SetFrame(m_FrameChannels);
	info.SetRBACPrivilege(m_FrameChannels->GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Channel, Command::ModuleTask::UpdateMoreInfo, info, { this, g_ActionNameSelectedChannelLoadMore, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
}

void GridChannels::OnUpdateChannelLoadMore(CCmdUI* pCmdUI)
{
	const auto condition = [](const GridBackendRow* p_Row) { return GridUtil::IsBusinessChannel(p_Row); };
	pCmdUI->Enable(IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CHANNELS_LOADMORE, condition));
	setTextFromProfUISCommand(*pCmdUI);
}

BOOL GridChannels::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return ModuleO365Grid<BusinessChannel>::PreTranslateMessage(pMsg);
}

void GridChannels::InitializeCommands()
{
	std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_CHANNELSGRID_LOADMORE;
		_cmd.m_sMenuText = _T("Load Info");
		_cmd.m_sToolbarText = _T("Load Info");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd, true);

		auto hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LOADMORE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CHANNELSGRID_LOADMORE, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridUsers_InitializeCommands_3, _YLOC("Load Additional Information")).c_str(),
			_T("Load document library information for the selected entries only."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWMESSAGES;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridChannels_InitializeCommands_1, _YLOC("Show Messages...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridChannels_InitializeCommands_2, _YLOC("Show Messages...")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANNELS_SHOW_MESSAGES_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CHANNELSGRID_SHOWMESSAGES, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridChannels_InitializeCommands_3, _YLOC("Show Messages")).c_str(),
			YtriaTranslate::Do(GridChannels_InitializeCommands_4, _YLOC("Display all messages for the selected channels.")).c_str(),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWMESSAGES_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridChannels_InitializeCommands_5, _YLOC("Show Messages...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWMESSAGES_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridChannels_InitializeCommands_6, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWMEMBERS;
		_cmd.m_sMenuText = _T("Show Members...");
		_cmd.m_sToolbarText = _T("Show Members...");
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANNELS_SHOW_MEMBERS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CHANNELSGRID_SHOWMESSAGES, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Show Members"),
			_T("Display all members for the selected private channels."),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWMEMBERS_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = _T("Show Members...");
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWMEMBERS_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridChannels_InitializeCommands_6, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWSITES;
		_cmd.m_sMenuText = _T("Show Private Channel Sites...");
		_cmd.m_sToolbarText = _T("Sites...");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANNELS_SHOW_SITES_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CHANNELSGRID_SHOWSITES, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Show Channel Sites"),
			_T("Show a detailed list of all sites for all selected private channels.\nDrill down into sites to see all files and sites directly from this view."),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWSITES_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_76, _YLOC("Show Group Sites...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWSITES_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_106, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWDRIVEITEMS;
		_cmd.m_sMenuText = _T("Show Private Channel Files...");
		_cmd.m_sToolbarText = _T("Files...");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CHANNELS_SHOW_SITES_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CHANNELSGRID_SHOWDRIVEITEMS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			_T("Show Channel Files"),
			_T("Get a detailed hierarchy of all files for selected private channels.\nManage file permissions, preview files directly, and download files en - masse from this view."),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWDRIVEITEMS_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_76, _YLOC("Show Group Sites...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_CHANNELSGRID_SHOWDRIVEITEMS_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(O365Grid_InitializeCommands_106, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	ModuleO365Grid<BusinessChannel>::InitializeCommands();
}

void GridChannels::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
		pPopup->ItemInsert(ID_CHANNELSGRID_LOADMORE);
		pPopup->ItemInsert(ID_CHANNELSGRID_SHOWMESSAGES);
		pPopup->ItemInsert(ID_CHANNELSGRID_SHOWMEMBERS);
		pPopup->ItemInsert(ID_CHANNELSGRID_SHOWSITES);
		pPopup->ItemInsert(ID_CHANNELSGRID_SHOWDRIVEITEMS);
		pPopup->ItemInsert(); // Sep
	}

	ModuleO365Grid<BusinessChannel>::OnCustomPopupMenu(pPopup, nStart);
}

bool GridChannels::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return ModuleO365Grid<BusinessChannel>::IsRowValidForNoFieldText(p_Row, p_Col)
		&& (!p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore)
			|| m_MetaIDsMoreLoaded.end() == m_MetaIDsMoreLoaded.find(p_Row->GetField(m_Template.m_ColumnID).GetValueStr()));
}

bool GridChannels::IsRowForLoadMore(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return CacheGrid::IsRowForLoadMore(p_Row, p_Col) && GridUtil::IsBusinessChannel(p_Row);
}

void GridChannels::BuildView(const O365DataMap<BusinessGroup, std::vector<BusinessChannel>>& p_Channels, const vector<BusinessChannel>& p_LoadedMoreChannels, bool p_FullPurge)
{
	{
		GridUpdater updater(*this,
			GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID
				| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
				| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
				| GridUpdaterOptions::REFRESH_PROCESS_DATA
			)
		);
		GridIgnoreModification ignoramus(*this);

		for (const auto& keyVal : p_Channels)
		{
			const auto& group = keyVal.first;
			if (m_HasLoadMoreAdditionalInfo)
			{
				if (group.IsMoreLoaded())
					m_MetaIDsMoreLoaded.insert(group.GetID());
				else
					m_MetaIDsMoreLoaded.erase(group.GetID());
			}

			auto parentRow = m_Template.AddRow(*this, group, {}, updater, false, nullptr, {}, true);
			ASSERT(nullptr != parentRow);
			if (nullptr != parentRow)
			{
				updater.GetOptions().AddRowWithRefreshedValues(parentRow);
				updater.GetOptions().AddPartialPurgeParentRow(parentRow);

				parentRow->SetHierarchyParentToHide(true);

				if (keyVal.second.size() == 1 && keyVal.second[0].GetError())
				{
					updater.OnLoadingError(parentRow, *keyVal.second[0].GetError());
				}
				else
				{
					// Clear previous error
					if (parentRow->IsError())
						parentRow->ClearStatus();

					for (const auto& channel : keyVal.second)
					{
						auto channelRow = FillRow(parentRow, channel);
						ASSERT(nullptr != channelRow);
						if (nullptr != channelRow)
						{
							updater.GetOptions().AddRowWithRefreshedValues(channelRow);

							channelRow->SetParentRow(parentRow);
						}
					}
				}

				AddRoleDelegationFlag(parentRow, keyVal.first);
			}
		}
	}

	if (!p_LoadedMoreChannels.empty())
		UpdateChannelsLoadMore(p_LoadedMoreChannels);
}
