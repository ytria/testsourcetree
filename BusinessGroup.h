#pragma once

#include "BusinessAssignedLicense.h"
#include "BusinessDrive.h"
#include "BusinessObject.h"
#include "BusinessOrgContact.h"
#include "CachedUser.h"
#include "Group.h"
#include "OnPremiseGroup.h"
#include "PooledString.h"
#include "Team.h"

class BusinessUser;
class CachedGroup;

class BusinessGroup : public BusinessObject
{
public:
	static wstring g_Office365Group;
	//static const wstring g_DynamicGroup;
	static wstring g_DistributionList;
	static wstring g_SecurityGroup;
	static wstring g_MailEnabledSecurityGroup;
	static wstring g_Team;
	static wstring g_Yammer;
	static wstring g_Archived;

	static const wstring g_DynamicMembership;
	static const wstring g_TeamProvisionningOption;

    BusinessGroup();
    BusinessGroup(const Group& p_JsonGroup);
	BusinessGroup(const CachedGroup& p_CachedGroup);

	BusinessGroup(const BusinessGroup& p_Other);
	BusinessGroup& operator=(const BusinessGroup& p_Other);

	BusinessGroup& MergeWith(const BusinessGroup& p_Other);

	void SetValuesFrom(const Group& p_JsonGroup);
	void SetValuesFrom(const CachedGroup& p_CachedGroup);

	bool IsMoreLoaded() const override;

	const boost::YOpt<PooledString>&			GetDisplayName() const;
	const boost::YOpt<bool>&					IsAllowExternalSenders() const;
	const boost::YOpt<bool>&					IsAutoSubscribeNewMembers() const;
	const boost::YOpt<PooledString>&			GetClassification() const;
	const boost::YOpt<YTimeDate>&				GetCreatedDateTime() const;
	const boost::YOpt<YTimeDate>&				GetRenewedDateTime() const;
	const boost::YOpt<PooledString>&			GetDescription() const;
	const boost::YOpt<vector<PooledString>>&	GetGroupTypes() const;
	const boost::YOpt<vector<PooledString>>&	GetProxyAddresses() const;
	//const boost::YOpt<bool>&					IsSubscribedByMail() const;
	const boost::YOpt<PooledString>&			GetMail() const;
	const boost::YOpt<bool>&					IsMailEnabled() const;
	const boost::YOpt<PooledString>&			GetMailNickname() const;
	const boost::YOpt<YTimeDate>&				GetOnPremisesLastSyncDateTime() const;
	const boost::YOpt<PooledString>&			GetOnPremisesSecurityIdentifier() const;
	const boost::YOpt<vector<OnPremisesProvisioningError>>&	GetOnPremisesProvisioningErrors() const;
	const boost::YOpt<bool>&					IsOnPremisesSyncEnabled() const;
	const boost::YOpt<bool>&					IsSecurityEnabled() const;
	const boost::YOpt<int32_t>&					GetUnseenCount() const;
	const boost::YOpt<PooledString>&			GetVisibility() const;
	const boost::YOpt<PooledString>&			GetCombinedGroupType() const;
	const boost::YOpt<PooledString>&			GetLCPStatus() const;
	const boost::YOpt<SapioError>&				GetLCPStatusError() const;
	const boost::YOpt<bool>&					IsDynamicMembership() const;
	
	void SetDisplayName(const boost::YOpt<PooledString>& p_Val);
	void SetIsAllowExternalSenders(const boost::YOpt<bool>& p_IsAllowExternalSenders);
	void SetIsAutoSubscribeNewMembers(const boost::YOpt<bool>& p_IsAutoSubscribeNewMembers);
	void SetClassification(const boost::YOpt<PooledString>& p_Classification);
	void SetCreatedDateTime(const boost::YOpt<YTimeDate>& p_CreatedDateTime);
	void SetCreatedDateTime(const boost::YOpt<PooledString>& p_CreatedDateTime);
	void SetRenewedDateTime(const boost::YOpt<YTimeDate>& p_RenewedDateTime);
	void SetRenewedDateTime(const boost::YOpt<PooledString>& p_RenewedDateTime);
	void SetDescription(const boost::YOpt<PooledString>& p_Description);
	void SetGroupTypes(const boost::YOpt<vector<PooledString>>& p_GroupTypes);
	void SetProxyAddresses(const boost::YOpt<vector<PooledString>>& p_ProxyAddresses);
	//void SetIsSubscribedByMail(const boost::YOpt<bool>& p_IsSubscribedByMail);
	void SetMail(const boost::YOpt<PooledString>& p_Mail);
	void SetIsMailEnabled(const boost::YOpt<bool>&p_IsMailEnabled);
	void SetMailNickname(const boost::YOpt<PooledString>& p_MailNickname);
	void SetOnPremisesLastSyncDateTime(const boost::YOpt<YTimeDate>& p_OnPremisesLastSyncDateTime);
	void SetOnPremisesLastSyncDateTime(const boost::YOpt<PooledString>& p_OnPremisesLastSyncDateTime);
	void SetOnPremisesSecurityIdentifier(const boost::YOpt<PooledString>& p_OnPremisesSecurityIdentifier);
	void SetOnPremisesProvisioningErrors(const boost::YOpt<vector<OnPremisesProvisioningError>>& p_OnPremisesProvisioningErrors);
	void SetOnPremisesSyncEnabled(const boost::YOpt<bool>& p_OnPremisesSyncEnabled);
	void SetIsSecurityEnabled(const boost::YOpt<bool>& p_IsSecurityEnabled);
	void SetUnseenCount(const boost::YOpt<int32_t> p_UnseenCount);
	void SetVisibility(const boost::YOpt<PooledString>& p_Visibility);
	void SetLCPStatus(const boost::YOpt<PooledString>& p_LCPStatus);
	void SetLCPStatusError(const boost::YOpt<SapioError>& p_LCPStatusError);
	void SetDynamicMembership(const boost::YOpt<bool>& p_DynamicMembership);

	virtual wstring GetValue(const wstring& p_PropName) const override;
	virtual vector<PooledString> GetProperties(const wstring& p_PropName) const override;

	// Will set the related values so that combined group type is what's requested.
	void SetCombinedGroupType(const boost::YOpt<PooledString>& p_CombinedGroupType);

	/**** For group creation editor ****/
	const boost::YOpt<PooledString>& GetMailWithoutDomain() const;
	void SetMailWithoutDomain(const boost::YOpt<PooledString>& val);

	const boost::YOpt<PooledString>& GetMailDomain() const;
	void SetMailDomain(const boost::YOpt<PooledString>& val);
	/**********************************/
    
    const boost::YOpt<PooledString>& GetUserCreatedOnBehalfOf() const;
    void SetUserCreatedOnBehalfOf(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<SapioError>& GetUserCreatedOnBehalfOfError() const;
	void SetUserCreatedOnBehalfOfError(const boost::YOpt<SapioError>& p_UserCreatedOnBehalfOfError);

	void AddParent(const BusinessGroup& p_Parent);
	void AddChild(const BusinessUser& p_Child);
	void AddChild(const BusinessGroup& p_Child);
	void AddChild(const BusinessOrgContact& p_Child);
	void SetParents(const vector<BusinessGroup>& p_Parents);

	void AddOwner(const BusinessUser& p_Owner);
	//void AddOwner(const BusinessGroup& p_Owner);
	const vector<PooledString>&	GetOwnerUsers() const;
	//const vector<PooledString>&	GetOwnerGroups() const;
	void ClearOwners();
	void SetOwnersError(const boost::YOpt<SapioError>& p_Val);
	const boost::YOpt<SapioError>& GetOwnersError() const;

	void AddAuthorAccepted(const BusinessUser& p_Author);
	void AddAuthorAccepted(const BusinessGroup& p_Author);
	void AddAuthorAccepted(const BusinessOrgContact& p_Author);
	void AddAuthorRejected(const BusinessUser& p_Author);
	void AddAuthorRejected(const BusinessGroup& p_Author);
	void AddAuthorRejected(const BusinessOrgContact& p_Author);

	const vector<PooledString>&	GetAuthorAcceptedUsers() const;
	const vector<PooledString>&	GetAuthorAcceptedGroups() const;
	const vector<PooledString>&	GetAuthorAcceptedOrgContacts() const;
	const vector<PooledString>&	GetAuthorRejectedUsers() const;
	const vector<PooledString>&	GetAuthorRejectedGroups() const;
	const vector<PooledString>&	GetAuthorRejectedOrgContacts() const;

	const boost::YOpt<SapioError>&	GetAuthorsAcceptedError() const;
	void							SetAuthorsAcceptedError(const boost::YOpt<SapioError>& p_Val);
	const boost::YOpt<SapioError>&	GetAuthorsRejectedError() const;
	void							SetAuthorsRejectedError(const boost::YOpt<SapioError>& p_Val);

	const vector<PooledString>&		GetChildrenUsers() const;
	const vector<PooledString>&		GetChildrenGroups() const;
	const vector<PooledString>&		GetChildrenOrgContacts() const;
	void							SetChildrenUsers(const vector<PooledString>& p_IDs);
	void							SetChildrenGroups(const vector<PooledString>& p_IDs);
	void							SetChildrenOrgContacts(const vector<PooledString>& p_IDs);
	const boost::YOpt<SapioError>&	GetChildrenError() const;
	void							SetChildrenError(const boost::YOpt<SapioError>& p_Val);
	const vector<PooledString>&		GetParents() const;
    
	bool								GetIsTeam() const;

	// For editing only !!!
	const boost::YOpt<bool>&			GetEditIsTeam() const;
	void								SetEditHasTeam(const boost::YOpt<bool>& p_HasTeam);
	///////////////////////////

	bool								GetHasTeamInfo() const;
	void								SetHasTeamInfo(bool p_HasTeamInfo);
	const boost::YOpt<Team>&			GetTeam() const;
	void								SetTeam(const boost::YOpt<Team>& p_Team);
	const boost::YOpt<SapioError>&		GetTeamError() const;
	void								SetTeamError(const boost::YOpt<SapioError>& p_Val);

	// For editing only !!!
	const boost::YOpt<bool>&			IsTeamArchived() const;
	const boost::YOpt<bool>&			GetPendingTeamArchiving() const;
	void								SetTeamArchived(const boost::YOpt<bool>& p_Archived);
	const boost::YOpt<bool>&			GetSpoSiteReadOnlyForMembers() const;
	void								SetSpoSiteReadOnlyForMembers(const boost::YOpt<bool>& p_SpoSiteReadOnlyForMembers);
	///////////////////////////

	const boost::YOpt<PooledString>&	GetSettingAllowToAddGuestsId() const;
	const boost::YOpt<bool>&			GetSettingAllowToAddGuests() const;
	void								SetSettingAllowToAddGuests(const boost::YOpt<bool>& p_Val);
	void								SetSettingAllowToAddGuestsId(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<bool>&            GetGroupUnifiedGuestSettingTemplateActivated() const;
	void                                SetGroupUnifiedGuestSettingTemplateActivated(const boost::YOpt<bool>& p_Val);
	const boost::YOpt<SapioError>&		GetGroupSettingsError() const;
	void								SetGroupSettingsError(const boost::YOpt<SapioError>& p_Val);
	const boost::YOpt<BusinessDrive>&	GetDrive() const;
	void								SetDrive(const boost::YOpt<BusinessDrive>& p_Val);

	const boost::YOpt<vector<PooledString>>	GetResourceBehaviorOptions() const;
	const boost::YOpt<vector<PooledString>>	GetResourceProvisioningOptions() const;
	void SetResourceBehaviorOptions(const vector<PooledString>& p_ResourceBehaviorOptions);
	void SetResourceProvisioningOptions(const vector<PooledString>& p_ResourceProvisioningOptions);

	const boost::YOpt<PooledString>& GetPreferredDataLocation() const;
	void SetPreferredDataLocation(const boost::YOpt<PooledString>& p_PreferredDataLocation);

	const boost::YOpt<uint32_t> GetMembersCount() const;
	void SetMembersCount(const boost::YOpt<uint32_t>& p_MembersCount);

	const boost::YOpt<bool>& GetHideFromOutlookClients() const;
	void SetHideFromOutlookClients(const boost::YOpt<bool>& p_Val);
	const boost::YOpt<bool>& GetHideFromAddressLists() const;
	void SetHideFromAddressLists(const boost::YOpt<bool>& p_Val);
	const boost::YOpt<LicenseProcessingState>& GetLicenseProcessingState() const;
	void SetLicenseProcessingState(const boost::YOpt<LicenseProcessingState>& p_Val);
	const boost::YOpt<PooledString>& GetSecurityIdentifier() const;
	void SetSecurityIdentifier(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<vector<BusinessAssignedLicense>>& GetAssignedLicenses() const;
	boost::YOpt<vector<BusinessAssignedLicense>>& GetAssignedLicenses();
	void SetAssignedLicenses(const vector<BusinessAssignedLicense>& val);

	/* Beta API */
	const boost::YOpt<PooledString>&  GetMembershipRuleProcessingState() const;
	void SetMembershipRuleProcessingState(const boost::YOpt<PooledString>& p_MembershipRuleProcessingState);
	const boost::YOpt<PooledString>&  GetMembershipRule() const;
	void SetMembershipRule(const boost::YOpt<PooledString>& p_MembershipRule);
	const boost::YOpt<PooledString>&  GetTheme() const;
	void SetTheme(const boost::YOpt<PooledString>& p_Theme);

	const boost::YOpt<PooledString>&  GetMembershipRuleProcessingStateForEdit() const; // Returns GridBackendUtil::g_NoValueString when boost::none or empty string
	const boost::YOpt<PooledString>&  GetThemeForEdit() const; // Returns GridBackendUtil::g_NoValueString when boost::none or empty string

	const boost::YOpt<PooledString>&  GetPreferredLanguage() const;
	void SetPreferredLanguage(const boost::YOpt<PooledString>& p_PreferredLanguage);

	const boost::YOpt<OnPremiseGroup>& GetOnPremiseGroup() const;
	boost::YOpt<OnPremiseGroup>& GetOnPremiseGroup();

	// For deleted group
	void							SetDeletedDateTime(const boost::YOpt<YTimeDate>& p_DelDate);
	void							SetDeletedDateTime(const boost::YOpt<PooledString>& p_DelDate);
	const boost::YOpt<YTimeDate>&	GetDeletedDateTime() const;
	bool							IsFromRecycleBin() const;

	/**********/

    static Group ToGroup(const BusinessGroup& p_BusinessGroup);

	bool operator<(const BusinessGroup& p_Other) const;

	// Kind of a hack: Only for SendLCPAddGroupRequest and SendLCPRemoveGroupRequest
	void SetLCPId(const PooledString& p_LCPId);

	const boost::YOpt<SapioError>& GetDriveError() const;
	void SetDriveError(const boost::YOpt<SapioError>& p_Val);

	const boost::YOpt<bool>& GetIsYammer() const;
	void SetIsYammer(const boost::YOpt<bool>& p_Val);
	const boost::YOpt<SapioError>& GetIsYammerError() const;
	void SetIsYammerError(const boost::YOpt<SapioError>& p_Val);

	bool IsRemoved() const;

protected:
    pplx::task<vector<HttpResultWithError>> SendEditRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	TaskWrapper<HttpResultWithError> SendCreateRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	TaskWrapper<HttpResultWithError> SendDeleteRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	TaskWrapper<HttpResultWithError> SendRestoreRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	TaskWrapper<HttpResultWithError> SendHardDeleteRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	TaskWrapper<HttpResultWithError> SendLCPRenewRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	TaskWrapper<HttpResultWithError> SendLCPAddGroupRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	TaskWrapper<HttpResultWithError> SendLCPRemoveGroupRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;

	vector<TaskWrapper<HttpResultWithError>> SendAddMembersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	vector<TaskWrapper<HttpResultWithError>> SendDeleteMembersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	vector<TaskWrapper<HttpResultWithError>> SendAddOwnersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	vector<TaskWrapper<HttpResultWithError>> SendDeleteOwnersRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	vector<TaskWrapper<HttpResultWithError>> SendAddAcceptedRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	vector<TaskWrapper<HttpResultWithError>> SendAddRejectedRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	vector<TaskWrapper<HttpResultWithError>> SendDeleteAuthorizationRequest(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;
	vector<TaskWrapper<HttpResultWithError>> SendGroupSettingsModify(std::shared_ptr<Sapio365Session> p_MsGraphSession, YtriaTaskData p_TaskData) const override;

private:
	void SetAssignedLicenses(const vector<AssignedLicense>& val);
	void CopyAssignedLicensesTo(vector<AssignedLicense>& val) const;

private:
	/*****************************************/
	/*** WARNING: operator=() is overriden ***/
	/*****************************************/

	boost::YOpt<vector<PooledString>>	m_ProxyAddresses;

	boost::YOpt<PooledString>			m_DisplayName;
	boost::YOpt<bool>					m_IsAllowExternalSenders;
	boost::YOpt<bool>					m_IsAutoSubscribeNewMembers;
	boost::YOpt<PooledString>			m_Classification;
	boost::YOpt<YTimeDate>				m_CreatedDateTime;
	boost::YOpt<YTimeDate>				m_RenewedDateTime;
	boost::YOpt<PooledString>			m_Description;
	boost::YOpt<vector<PooledString>>	m_GroupTypes;
	//boost::YOpt<bool>					m_IsSubscribedByMail;
	boost::YOpt<PooledString>			m_Mail;
	boost::YOpt<bool>					m_IsMailEnabled;
	boost::YOpt<PooledString>			m_MailNickname;
	boost::YOpt<YTimeDate>				m_OnPremisesLastSyncDateTime;
	boost::YOpt<PooledString>			m_OnPremisesSecurityIdentifier;
	boost::YOpt<vector<OnPremisesProvisioningError>>	m_OnPremisesProvisioningErrors;
	boost::YOpt<bool>					m_IsOnPremisesSyncEnabled;
	boost::YOpt<bool>					m_IsSecurityEnabled;
	boost::YOpt<int32_t>				m_UnseenCount;
	boost::YOpt<PooledString>			m_Visibility;
    boost::YOpt<Team>			        m_Team;
    boost::YOpt<PooledString>			m_UserCreatedOnBehalfOf;
	bool				                m_HasTeamInfo;
    boost::YOpt<bool>                   m_SettingAllowToAddGuestsIsActivated;
    boost::YOpt<bool>                   m_SettingAllowToAddGuests;
    boost::YOpt<PooledString>           m_SettingAllowToAddGuestsId;
	mutable boost::YOpt<bool>			m_IsTeamForEdit; // Only for team edition.
	boost::YOpt<bool>					m_PendingArchivedTeam; // Only for team edition (archiving).
	boost::YOpt<bool>					m_PendingSpoSiteReadOnlyForMembers; // Only for team edition (archiving).

	boost::YOpt<YTimeDate>				m_DeletedDateTime; // For deleted group

	boost::YOpt<PooledString>	m_LCPStatus;
	mutable boost::YOpt<PooledString>	m_CombinedGroupType; // Only filled when calling GetCombinedGroupType()
	mutable boost::YOpt<bool>			m_DynamicMembership; // Only filled when calling IsDynamicMembership()

	boost::YOpt<bool>					m_IsYammer;

	vector<PooledString> m_ChildrenUsers;
	vector<PooledString> m_ChildrenGroups;
	vector<PooledString> m_ChildrenOrgContacts;
	vector<PooledString> m_Parents;

	vector<PooledString> m_OwnersUsers;
	//vector<PooledString> m_OwnersGroups;

	vector<PooledString> m_AuthorAcceptedUsers;
	vector<PooledString> m_AuthorAcceptedGroups;
	vector<PooledString> m_AuthorAcceptedOrgContacts;
	vector<PooledString> m_AuthorRejectedUsers;
	vector<PooledString> m_AuthorRejectedGroups;
	vector<PooledString> m_AuthorRejectedOrgContacts;

	boost::YOpt<PooledString>	m_LCPId; // Kind of a hack: Only for SendLCPAddGroupRequest and SendLCPRemoveGroupRequest

    boost::YOpt<SapioError>	m_TeamError; // Done
    boost::YOpt<SapioError>	m_GroupSettingsError;
	boost::YOpt<SapioError>	m_UserCreatedOnBehalfOfError; // Done
	boost::YOpt<SapioError>	m_OwnersError;
	boost::YOpt<SapioError>	m_LCPStatusError;
	boost::YOpt<SapioError>	m_DriveError;
	boost::YOpt<SapioError>	m_IsYammerError;
	boost::YOpt<SapioError>	m_ChildrenError;
	boost::YOpt<SapioError>	m_AuthorsAcceptedError;
	boost::YOpt<SapioError>	m_AuthorsRejectedError;

	boost::YOpt<vector<PooledString>>	m_ResourceBehaviorOptions;
	boost::YOpt<vector<PooledString>>	m_ResourceProvisioningOptions;

	boost::YOpt<PooledString>			m_PreferredDataLocation;

	boost::YOpt<uint32_t>				m_MembersCount;

	boost::YOpt<BusinessDrive>			m_Drive;

	boost::YOpt<bool>					m_HideFromOutlookClients;
	boost::YOpt<bool>					m_HideFromAddressLists;
	boost::YOpt<LicenseProcessingState> m_LicenseProcessingState;
	boost::YOpt<PooledString>			m_SecurityIdentifier;
	boost::YOpt<vector<BusinessAssignedLicense>> m_AssignedLicenses;

	boost::YOpt<OnPremiseGroup>			m_OnPremGroup;

	/* Beta API */
	boost::YOpt<PooledString>			m_MembershipRuleProcessingState;
	boost::YOpt<PooledString>			m_MembershipRule;
	boost::YOpt<PooledString>			m_Theme;
	boost::YOpt<PooledString>			m_PreferredLanguage;

	/*****************************************/
	/*** WARNING: operator=() is overriden ***/
	/*****************************************/

	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND

    friend class GroupDeserializer;
    friend class GroupNonPropDeserializer;
    friend class DbGroupSerializer;

	static bool g_InitLocalization;
};

DECLARE_BUSINESSOBJECT_EQUALTO_AND_HASH_SPECIALIZATION(BusinessGroup)
