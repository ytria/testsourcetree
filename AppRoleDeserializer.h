#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "AppRole.h"

class AppRoleDeserializer : public JsonObjectDeserializer, public Encapsulate<AppRole>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

