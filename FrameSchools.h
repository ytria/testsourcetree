#pragma once

#include "GridFrameBase.h"
#include "GridSchools.h"
#include "EducationSchool.h"

class FrameSchools : public GridFrameBase
{
public:
	DECLARE_DYNAMIC(FrameSchools)
	FrameSchools(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
	virtual ~FrameSchools() = default;

	void BuildView(const vector<EducationSchool>& p_DirAudits, const vector<O365UpdateOperation>& p_Updates, bool p_FullPurge);

	O365Grid& GetGrid() override;

	void RefreshSpecific(AutomationAction* p_CurrentAction) override;
	bool HasApplyButton() override;

	void ApplySpecific(bool p_Selected);
	void ApplyAllSpecific() override;
	void ApplySelectedSpecific() override;

protected:
	void createGrid() override;
	void revertSelectedImpl() override;
	bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;
	AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	vector<EducationSchool> GetInfoForRefreshAfterUpdate();

	GridSchools m_Grid;
};

