#include "DbSiteSerializer.h"

#include "JsonSerializeUtil.h"
#include "ListSerializer.h"
#include "MsGraphFieldNames.h"
#include "NullableSerializer.h"
#include "RootSerializer.h"
#include "SharepointIdsSerializer.h"
#include "SiteCollectionSerializer.h"
#include "YtriaFieldsConstants.h"

DbSiteSerializer::DbSiteSerializer(const BusinessSite& p_Site)
    : m_Site(p_Site)
{
}

void DbSiteSerializer::Serialize()
{
    m_Json = web::json::value::object();
    auto& obj = m_Json.as_object();

    // Ms Graph
    JsonSerializeUtil::SerializeString(_YUID(O365_ID), m_Site.GetID(), obj);
    JsonSerializeUtil::SerializeTimeDate(_YUID(O365_SITE_CREATEDDATETIME), m_Site.GetCreatedDateTime(), obj);
    JsonSerializeUtil::SerializeString(_YUID(O365_SITE_DESCRIPTION), m_Site.GetDescription(), obj);
    JsonSerializeUtil::SerializeString(_YUID(O365_SITE_DISPLAYNAME), m_Site.GetDisplayName(), obj);
    JsonSerializeUtil::SerializeTimeDate(_YUID(O365_SITE_LASTMODIFIEDDATETIME), m_Site.GetLastModifiedDateTime(), obj);
    JsonSerializeUtil::SerializeString(_YUID(O365_SITE_NAME), m_Site.GetName(), obj);
    JsonSerializeUtil::SerializeAny(_YUID(O365_SITE_ROOT), NullableSerializer<Root, RootSerializer>(m_Site.GetRoot()), obj);
    JsonSerializeUtil::SerializeAny(_YUID(O365_SITE_SHAREPOINTIDS), NullableSerializer<SharepointIds, SharepointIdsSerializer>(m_Site.GetSharepointIds()), obj);    
    JsonSerializeUtil::SerializeAny(_YUID(O365_SITE_SITECOLLECTION), NullableSerializer<SiteCollection, SiteCollectionSerializer>(m_Site.GetSiteCollection()), obj);
    JsonSerializeUtil::SerializeString(_YUID(O365_SITE_WEBURL), m_Site.GetWebUrl(), obj);

    // Ytria
    JsonSerializeUtil::SerializeTimeDate(_YUID(YTRIA_LASTREQUESTEDDATETIME), m_Site.GetLastRequestTime(), obj);
	ASSERT(!m_Site.HasFlag(BusinessObject::CANCELED));  // Don't want to store canceled sites
    JsonSerializeUtil::SerializeUint32(_YUID(YTRIA_FLAGS), m_Site.GetFlags(), obj);
    JsonSerializeUtil::SerializeAny(_YUID(YTRIA_SITE_SUBSITES), ListSerializer<BusinessSite, DbSiteSerializer>(m_Site.GetSubSites()), obj);
}
