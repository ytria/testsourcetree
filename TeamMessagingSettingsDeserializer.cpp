#include "TeamMessagingSettingsDeserializer.h"

#include "JsonSerializeUtil.h"

void TeamMessagingSettingsDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowUserEditMessages"), m_Data.AllowUserEditMessages, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowUserDeleteMessages"), m_Data.AllowUserDeleteMessages, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowOwnerDeleteMessages"), m_Data.AllowOwnerDeleteMessages, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowTeamMentions"), m_Data.AllowTeamMentions, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowChannelMentions"), m_Data.AllowChannelMentions, p_Object);
}
