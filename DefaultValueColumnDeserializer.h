#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "DefaultValueColumn.h"

class DefaultValueColumnDeserializer : public JsonObjectDeserializer, public Encapsulate<DefaultValueColumn>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

