#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessChannel.h"

class ChannelDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessChannel>
{
public:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

