#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "SignIn.h"

class AppliedConditionalAccessPolicyDeserializer : public JsonObjectDeserializer, public Encapsulate<AppliedConditionalAccessPolicy>
{
public:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};

