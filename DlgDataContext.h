#pragma once

#include "CacheGrid.h"
#include "LinearMap.h"
#include "ResizableDialog.h"
#include "Resource.h"

class GridDataContext : public CacheGrid
{
public:
	GridDataContext(bool useDetailsColumn);

	virtual void customizeGrid() override;
	virtual void OnCustomDoubleClick() override;

	GridBackendColumn* m_NameColumn;
	GridBackendColumn* m_IDColumn;
	GridBackendColumn* m_DetailsColumn;

	wstring m_SelectedId;
	bool m_UseDetailsColumn;
};

class DlgDataContext	: public ResizableDialog
						, public GridSelectionObserver
{
public:
	enum { IDD = IDD_DLG_DATA_CONTEXT };
	DlgDataContext(const LinearMap<PooledString, PooledString>& p_Context, bool p_CanEdit, CWnd* p_Parent);
	DlgDataContext(const LinearMap<PooledString, std::pair<PooledString, PooledString>>& p_Context, CWnd* p_Parent);

    const boost::YOpt<LinearMap<PooledString, PooledString>>& GetContext1() const;

	void OnSelect();

	wstring m_SelectedId;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialogSpecificResizable() override;

    afx_msg void OnAddEntries();
    afx_msg void OnRemoveEntries();
    DECLARE_MESSAGE_MAP()

private:
	void fillGrid();
	void UpdateRemoveButton();
	void UpdateOkButton();

	virtual void SelectionChanged(const CacheGrid& grid) override;

private:
	boost::YOpt<LinearMap<PooledString, PooledString>> m_Context1;
	const boost::YOpt<LinearMap<PooledString, PooledString>> m_Context1Copy;
	boost::YOpt<LinearMap<PooledString, std::pair<PooledString, PooledString>>> m_Context2;
    CExtButton m_AddEntriesButton;
    CExtButton m_RemoveEntriesButton;
    CExtButton m_OkButton;
    CExtButton m_CancelButton;
	CExtLabel m_Label;
	GridDataContext m_Grid;
	const bool m_Edit;
};