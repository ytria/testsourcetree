#pragma once

#include "ModuleBase.h"
#include "CommandInfo.h"

class AutomationAction;
class FrameMessageRules;
struct RefreshSpecificData;

class ModuleMessageRules : public ModuleBase
{
public:
	using ModuleBase::ModuleBase;

private:
	virtual void executeImpl(const Command& p_Command) override;
	void showMessageRules(const Command& p_Command);
	void refresh(const Command& p_Command);
	void applyChanges(const Command& p_Command);
	// FIXME: Finish this eventually
	//void retrieveFolderIDs(const Command& p_Command);
	void loadSnapshot(const Command& p_Command);

	void doRefresh(FrameMessageRules* p_pFrame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const RefreshSpecificData& p_RefreshData);
};