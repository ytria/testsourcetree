#pragma once

#include "BaseO365Grid.h"
#include "BusinessMessageRule.h"
#include "GridTemplateUsers.h"
#include "MyDataMeRowHandler.h"

#define HAS_MESSAGE_RULES_EDIT 1
class GridUpdater;
class BusinessUser;

class GridMessageRules : public ModuleO365Grid<BusinessMessageRule>
{
public:
	GridMessageRules(bool p_IsMyData = false);

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

	virtual void customizeGrid() override;
	virtual void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
    void BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessMessageRule>>& p_MessageRules, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_PartialRefresh);
	void RefreshTreeView(const O365DataMap<PooledString, vector<BusinessMessageRule>>& p_MessageRules, const vector<O365UpdateOperation>& p_UpdateOperations);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	virtual void	AddCreatedBusinessObjects(const vector<BusinessMessageRule>& p_Rules, bool p_ScrollToNew) override;
	virtual void	UpdateBusinessObjects(const vector<BusinessMessageRule>& p_Rules, bool p_SetModifiedStatus) override;
	virtual void	RemoveBusinessObjects(const vector<BusinessMessageRule>& p_Rules) override;
	virtual BusinessMessageRule getBusinessObject(GridBackendRow* p_Row) const override;
#if HAS_MESSAGE_RULES_EDIT
	virtual bool	canEdit(GridBackendRow* p_Row) const override;
#endif
	virtual bool	canDelete(GridBackendRow* p_Row) const override;

	BusinessMessageRule GetBusinessMessageRule(GridBackendRow* p_Row, const std::set<UINT>& p_SpecificColumnsOnly) const;
	using ModifiedRule = std::pair<BusinessMessageRule, row_pk_t>;
	using ModifiedRules = O365DataMap<PooledString, vector<ModifiedRule>>;

#if HAS_MESSAGE_RULES_EDIT
	ModifiedRules GetModifiedRules(const O365IdsContainer& p_UserIds);
#endif
	ModifiedRules GetDeletedRules(const O365IdsContainer& p_UserIds);

	GridBackendColumn* GetColMetaId() const;

#if HAS_MESSAGE_RULES_EDIT
	virtual void ModificationStateChanged(GridBackendRow* p_Row, RowFieldUpdates<Scope::O365>& p_Modification) override;
#endif

	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeEdit() const override;
	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeCreate() const override;
	virtual RoleDelegationUtil::RBAC_Privilege GetPrivilegeDelete() const override;

	static const wstring& GetComponentTypeCondition();
	static const wstring& GetComponentTypeAction();
	static const wstring& GetComponentTypeException();

	virtual bool HasModificationsPending(bool inSelectionOnly = false) override;

	bool IsMyData() const override;
	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg void OnShowUnsetComponents();
	afx_msg void OnUpdateShowUnsetComponents(CCmdUI* pCmdUI);

#if HAS_MESSAGE_RULES_EDIT
	afx_msg void OnMoveUpInSequence();
	afx_msg void OnUpdateMoveUpInSequence(CCmdUI* pCmdUI);
	afx_msg void OnMoveDownInSequence();
	afx_msg void OnUpdateMoveDownInSequence(CCmdUI* pCmdUI);
#endif

	virtual void InitializeCommands() override;

#if HAS_MESSAGE_RULES_EDIT
	virtual INT_PTR showDialog(vector<BusinessMessageRule>& p_Objects, const std::set<wstring>& p_ListFieldErrors, DlgFormsHTML::Action p_Action, CWnd* p_Parent) override;
#endif

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

private:
    void buildTreeViewImpl(const BusinessMessageRule& p_BusinessMessageRule, GridBackendRow* p_Row, GridUpdater& p_Updater);
	void fillRow(const BusinessMessageRule& p_BusinessMessageRule, GridBackendRow* p_Row, bool p_SetModifiedStatus, GridUpdater& p_Updater);
	void techHideUnsetComponents(bool p_TechHide, bool p_UpdateGrid);

	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

	void addRuleCompRow(const wstring& p_CompType, const boost::YOpt<PooledString>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_Col, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater);
	void addRuleCompRow(const wstring& p_CompType, const boost::YOpt<bool>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_Col, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater);
	void addRuleCompRow(const wstring& p_CompType, const boost::YOpt<vector<PooledString>>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_Col, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater);
	void addRuleCompRow(const wstring& p_CompType, const boost::YOpt<vector<Recipient>>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_ColAddress, GridBackendColumn* p_ColName, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater);
	void addRuleCompRow(const wstring& p_CompType, const boost::YOpt<SizeRange>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_ColMin, GridBackendColumn* p_ColMax, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater);
	void addRuleCompRow(const wstring& p_CompType, const boost::YOpt<PooledString>& p_valId, const boost::YOpt<PooledString>& p_valName, bool p_ValNameIsError, const PooledString& p_RuleComp, GridBackendColumn* p_ColId, GridBackendColumn* p_ColName, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater);

	GridBackendRow* addRuleCompRow(const wstring& p_CompType, const PooledString& p_RuleComp, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater);

	int getComponentTypeIcon(const wstring& p_CompType) const;

	void extractGridValue(const std::set<UINT>& p_ColumnFilter, boost::YOpt<PooledString>& p_val, GridBackendColumn* p_Col, GridBackendRow* p_Row) const;
	void extractGridValue(const std::set<UINT>& p_ColumnFilter, boost::YOpt<bool>& p_val, GridBackendColumn* p_Col, GridBackendRow* p_Row) const;
	void extractGridValue(const std::set<UINT>& p_ColumnFilter, boost::YOpt<int32_t>& p_val, GridBackendColumn* p_Col, GridBackendRow* p_Row) const;
	bool extractGridValue(const wstring& p_CompType, boost::YOpt<PooledString>& p_val, const PooledString& p_RuleComp, GridBackendRow* p_ParentRow) const;
	bool extractGridValue(const wstring& p_CompType, boost::YOpt<bool>& p_val, const PooledString& p_RuleComp, GridBackendRow* p_ParentRow) const;
	bool extractGridValue(const wstring& p_CompType, boost::YOpt<vector<PooledString>>& p_val, const PooledString& p_RuleComp, GridBackendRow* p_ParentRow) const;
	bool extractGridValue(const wstring& p_CompType, boost::YOpt<vector<Recipient>>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_ColAddresses, GridBackendColumn* p_ColNames, GridBackendRow* p_ParentRow) const;
	bool extractGridValue(const wstring& p_CompType, boost::YOpt<SizeRange>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_ColMin, GridBackendColumn* p_ColMax, GridBackendRow* p_ParentRow) const;

	std::pair<GridBackendRow*, GridBackendField*> extractComponentValueField(const wstring& p_CompType, const PooledString& p_RuleComp, GridBackendRow* p_ParentRow) const;

	template<typename T>
	GridBackendField* updateField(const T& p_Value, GridBackendRow* p_Row, GridBackendColumn* p_Col, bool p_StoreModification);

	static bool isAnyColumnWanted(const std::set<UINT>& p_ColumnFilter, const std::vector<GridBackendColumn*>&& p_Columns);

private:
	bool m_HideUnsetComponents;

	GridTemplateUsers m_Template;

    GridBackendColumn* m_ColRuleName;
	GridBackendColumn* m_ColSequence;
	GridBackendColumn* m_ColEnabled;
	GridBackendColumn* m_ColHasError;
	GridBackendColumn* m_ColReadOnly;

	// Ytria columns
	GridBackendColumn* m_ColComponentType;
	GridBackendColumn* m_ColRuleComponent;
	GridBackendColumn* m_ColComponentValue;

	// Conditions columns
	GridBackendColumn* m_ColConditionsBodyContains;
	GridBackendColumn* m_ColConditionsBodyOrSubjectContains;
	GridBackendColumn* m_ColConditionsCategories;
	GridBackendColumn* m_ColConditionsFromAddressesName;
	GridBackendColumn* m_ColConditionsFromAddressesAddress;
	GridBackendColumn* m_ColConditionsHasAttachments;
	GridBackendColumn* m_ColConditionsHeaderContains;
	GridBackendColumn* m_ColConditionsImportance;
	GridBackendColumn* m_ColConditionsIsApprovalRequest;
	GridBackendColumn* m_ColConditionsIsAutomaticForward;
	GridBackendColumn* m_ColConditionsIsAutomaticReply;
	GridBackendColumn* m_ColConditionsIsEncrypted;
	GridBackendColumn* m_ColConditionsIsMeetingRequest;
	GridBackendColumn* m_ColConditionsIsMeetingResponse;
	GridBackendColumn* m_ColConditionsIsNonDeliveryReport;
	GridBackendColumn* m_ColConditionsIsPermissionControlled;
	GridBackendColumn* m_ColConditionsIsReadReceipt;
	GridBackendColumn* m_ColConditionsIsSigned;
	GridBackendColumn* m_ColConditionsIsVoiceMail;
	GridBackendColumn* m_ColConditionsMessageActionFlag;
	GridBackendColumn* m_ColConditionsNotSentToMe;
	GridBackendColumn* m_ColConditionsRecipientContains;
	GridBackendColumn* m_ColConditionsSenderContains;
	GridBackendColumn* m_ColConditionsSensitivity;
	GridBackendColumn* m_ColConditionsSentCCMe;
	GridBackendColumn* m_ColConditionsSentOnlyToMe;
	GridBackendColumn* m_ColConditionsSentToAddressesName;
	GridBackendColumn* m_ColConditionsSentToAddressesAddress;
	GridBackendColumn* m_ColConditionsSentToMe;
	GridBackendColumn* m_ColConditionsSentToOrCCMe;
	GridBackendColumn* m_ColConditionsSubjectContains;
	GridBackendColumn* m_ColConditionsWithinSizeRangeMin;
	GridBackendColumn* m_ColConditionsWithinSizeRangeMax;

	// Actions columns
	GridBackendColumn* m_ColActionsAssignCategories;
	GridBackendColumn* m_ColActionsCopyToFolder;
	GridBackendColumn* m_ColActionsCopyToFolderName;
	GridBackendColumn* m_ColActionsDelete;
	GridBackendColumn* m_ColActionsForwardAsAttachmentEmailAddressName;
	GridBackendColumn* m_ColActionsForwardAsAttachmentEmailAddressAddress;
	GridBackendColumn* m_ColActionsForwardToEmailAddressName;
	GridBackendColumn* m_ColActionsForwardToEmailAddressAddress;
	GridBackendColumn* m_ColActionsMarkAsRead;
	GridBackendColumn* m_ColActionsMarkImportance;
	GridBackendColumn* m_ColActionsMoveToFolder;
	GridBackendColumn* m_ColActionsMoveToFolderName;
	GridBackendColumn* m_ColActionsPermanentDelete;
	GridBackendColumn* m_ColActionsRedirectToEmailAddressName;
	GridBackendColumn* m_ColActionsRedirectToEmailAddressAddress;
	GridBackendColumn* m_ColActionsStopProcessingRules;

	int m_ComponentTypeConditionIconId;
	int m_ComponentTypeActionIconId;
	int m_ComponentTypeExceptionIconId;
	int m_EnabledRuleIconId;
	int m_DisabledRuleIconId;

	vector<row_pk_t> m_ModifiedReadOnlyRowPks;

	static wstring g_ComponentTypeCondition;
	static wstring g_ComponentTypeAction;
	static wstring g_ComponentTypeException;

	std::map<PooledString, PooledString> m_FolderIdToNames;

	bool m_HasLoadMoreAdditionalInfo;
	O365IdsContainer m_MetaIDsMoreLoaded;
	vector<GridBackendColumn*>	m_MetaColumns;

	bool m_IsMyData;
	boost::YOpt<MyDataMeRowHandler> m_MyDataMeHandler;
};
