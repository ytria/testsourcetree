#include "Office365Admin.h"

#include "AutomationNames.h"
#include "CommandDispatcher.h"
#include "FrameMessageRules.h"
#include "UpdatedObjectsGenerator.h"

IMPLEMENT_DYNAMIC(FrameMessageRules, GridFrameBase)

//BEGIN_MESSAGE_MAP(FrameMessageRules, GridFrameBase)
//END_MESSAGE_MAP()

FrameMessageRules::FrameMessageRules(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
	, m_MessageRulesGrid(p_IsMyData)
{
    m_CreateAddRemoveToSelection = !p_IsMyData;
    m_CreateRefreshPartial = !p_IsMyData;
	m_CreateShowContext = !p_IsMyData;
	m_CreateApplyPartial = !p_IsMyData;
}

void FrameMessageRules::ShowMessageRules(const O365DataMap<BusinessUser, vector<BusinessMessageRule>>& p_MessageRules, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_PartialRefresh)
{
	if (CompliesWithDataLoadPolicy())
		m_MessageRulesGrid.BuildTreeView(p_MessageRules, p_UpdateOperations, p_PartialRefresh);
}

void FrameMessageRules::ShowMessageRulesAfterUpdate(const O365DataMap<PooledString, vector<BusinessMessageRule>>& p_MessageRules, const vector<O365UpdateOperation>& p_UpdateOperations)
{
	if (CompliesWithDataLoadPolicy())
		m_MessageRulesGrid.RefreshTreeView(p_MessageRules, p_UpdateOperations);
}

// FIXME: Finish this eventually
//void FrameMessageRules::SetNewFolderNames(std::map<PooledString, std::map<PooledString, PooledString>> p_FolderNamesToIdsByUser)
//{
//
//}

void FrameMessageRules::createGrid()
{
	m_MessageRulesGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameMessageRules::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigUsers();
}

GridFrameBase::O365ControlConfig FrameMessageRules::GetApplyPartialControlConfig()
{
	return GetApplyPartialControlConfigUsers();
}

// returns false if no frame specific tab is needed.
bool FrameMessageRules::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewDataEditGroups(tab);

	auto dataGroup = tab.AddGroup(_T("Components"));
	{
		{
			auto ctrl = dataGroup->Add(xtpControlButton, ID_MESSAGERULESGRID_SHOWUNSETCOMPONENTS);
			setImage({ ID_MESSAGERULESGRID_SHOWUNSETCOMPONENTS }, IDB_MESSAGERULESGRID_SHOWUNSETCOMPONENTS, xtpImageNormal);
			setImage({ ID_MESSAGERULESGRID_SHOWUNSETCOMPONENTS }, IDB_MESSAGERULESGRID_SHOWUNSETCOMPONENTS_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}
	}

	CreateLinkGroups(tab, !m_MessageRulesGrid.IsMyData(), false);

	return true;
}

O365Grid& FrameMessageRules::GetGrid()
{
	return m_MessageRulesGrid;
}

void FrameMessageRules::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	// FIXME: Right now, explosion refresh doesn't work due to "multivalue cousin" columns.
	// Then we implode before any refresh (grid update is not necessary though).
	m_MessageRulesGrid.MultiValuesImplodeAll(false);
	////////////////////////////////////////////////////////////////////////////////////////////////

    auto updatedObjectsGen = UpdatedObjectsGenerator<BusinessMailFolder>();
    
    CommandInfo info;
    info.Data() = updatedObjectsGen;
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::MessageRules, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameMessageRules::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
	// FIXME: Right now, explosion refresh doesn't work due to "multivalue cousin" columns.
	// Then we implode before any refresh (grid update is not necessary though).
	if (p_RefreshData.m_SelectedRowsAncestors)
	{
		vector<GridBackendRow*> explodedRows;
		m_MessageRulesGrid.GetMultivalueManager().GetTopSourceRowsAll(explodedRows);

		vector<GridBackendRow*> rowsToImplode;
		for (const auto& row : explodedRows)
		{
			if (p_RefreshData.m_SelectedRowsAncestors->end() != p_RefreshData.m_SelectedRowsAncestors->find(row->GetTopAncestorOrThis()))
				rowsToImplode.push_back(row);
		}

		if (!rowsToImplode.empty())
			m_MessageRulesGrid.MultivalueImplode(rowsToImplode, false);
	}
	else
	{
		m_MessageRulesGrid.MultiValuesImplodeAll(false);
	}
	////////////////////////////////////////////////////////////////////////////////////////////////

    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::MessageRules, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameMessageRules::ApplyAllSpecific()
{
	ApplySpecific(false);
}

void FrameMessageRules::ApplySelectedSpecific()
{
	ApplySpecific(true);
}

AutomatedApp::AUTOMATIONSTATUS FrameMessageRules::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;
	
	return statusRV;
}

void FrameMessageRules::ApplySpecific(bool p_Selected)
{
	CommandInfo info;

	if (!p_Selected)
	{
		// Get ids for all users

		// Check on whether a user is modified will be done when calling GridMessageRules::GetModifiedRules in module.
		// We can just send the full user list.

		info.GetIds() = GetModuleCriteria().m_IDs;

		/*for (auto mod : GetGrid().GetModifications().GetRowModifications([](RowModification*) { return true; }))
		{
			auto& pk = mod->GetRowKey();
			for (const auto& field : pk)
			{
				if (field.GetColID() == m_MessageRulesGrid.GetColMetaId()->GetID());
					ids.insert(field.GetValueStr());
			}
		}*/
	}
	else
	{
		// Get ids for selected users
		for (auto row : m_MessageRulesGrid.GetSelectedRows())
		{
			if (!row->IsGroupRow())
			{
				auto userRow = row->GetTopAncestorOrThis();
				// Don't check this, it'll be checked when calling GridMessageRules::GetModifiedRules in module.
				//if (userRow->IsModified()) // FIXME: Only works while we make sure row status is synced to RowModifications.
				{
					auto& field = userRow->GetField(m_MessageRulesGrid.GetColId());
					if (field.HasValue())
						info.GetIds().insert(field.GetValueStr());
				}
			}
		}
	}

	info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::MessageRules, Command::ModuleTask::ApplyChanges, info, { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
}
