#pragma once

#include "BusinessMessageRule.h"
#include "GridFrameBase.h"
#include "GridMessageRules.h"

class FrameMessageRules : public GridFrameBase
{
public:
	FrameMessageRules(bool p_IsMyData, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);

	DECLARE_DYNAMIC(FrameMessageRules)
	virtual ~FrameMessageRules() = default;

	void ShowMessageRules(const O365DataMap<BusinessUser, vector<BusinessMessageRule>>& p_MessageRules, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_PartialRefresh);
	void ShowMessageRulesAfterUpdate(const O365DataMap<PooledString, vector<BusinessMessageRule>>& p_MessageRules, const vector<O365UpdateOperation>& p_UpdateOperations);

	// FIXME: Finish this eventually
	//void SetNewFolderNames(std::map<PooledString, std::map<PooledString, PooledString>> p_FolderNamesToIdsByUser);

	virtual O365Grid& GetGrid() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction);
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

	virtual void ApplyAllSpecific() override;
	virtual void ApplySelectedSpecific() override;

protected:
	//DECLARE_MESSAGE_MAP()

	virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;
	virtual O365ControlConfig GetApplyPartialControlConfig() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
	void ApplySpecific(bool p_Selected);

	GridMessageRules m_MessageRulesGrid;

	// FIXME: Finish this eventually
	//std::map<PooledString, std::map<PooledString, PooledString>> m_FolderNamesToIdsByUser;
};
