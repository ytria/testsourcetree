#include "LinkHandlerMessageRules.h"

#include "CommandDispatcher.h"
#include "CommandInfo.h"
#include "FrameMessageRules.h"
#include "Office365Admin.h"

void LinkHandlerMessageRules::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
    info.SetOrigin(Origin::User);
	p_Link.ENABLE_FREE_ACCESS();// free access
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::MessageRules, Command::ModuleTask::ListMe, info, { nullptr, g_ActionNameMyMessageRules, nullptr, p_Link.GetAutomationAction() }));
}

bool LinkHandlerMessageRules::IsAllowedWithAJLLicense() const
{
	return true; // only MyData is allowed
}