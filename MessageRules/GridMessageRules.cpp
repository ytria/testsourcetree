#include "GridMessageRules.h"

#include "AutomationWizardMessageRules.h"
#include "BasicGridSetup.h"
#include "BusinessUser.h"
#include "CacheGridTools.h"
#include "GridSnapshotConstants.h"
#include "DlgBusinessMessageRuleEditHTML.h"
#include "FrameMessageRules.h"
#include "GridHelpers.h"
#include "GridUtil.h"
#include "MsGraphFieldNames.h"
#include "O365AdminUtil.h"
#include "GridUpdater.h"

wstring GridMessageRules::g_ComponentTypeCondition;
wstring GridMessageRules::g_ComponentTypeAction;
wstring GridMessageRules::g_ComponentTypeException;

BEGIN_MESSAGE_MAP(GridMessageRules, ModuleO365Grid<BusinessMessageRule>)
	ON_COMMAND(ID_MESSAGERULESGRID_SHOWUNSETCOMPONENTS, OnShowUnsetComponents)
	ON_UPDATE_COMMAND_UI(ID_MESSAGERULESGRID_SHOWUNSETCOMPONENTS, OnUpdateShowUnsetComponents)
#if HAS_MESSAGE_RULES_EDIT
	ON_COMMAND(ID_MESSAGERULESGRID_MOVEUPINSEQUENCE, OnMoveUpInSequence)
	ON_UPDATE_COMMAND_UI(ID_MESSAGERULESGRID_MOVEUPINSEQUENCE, OnUpdateMoveUpInSequence)

	ON_COMMAND(ID_MESSAGERULESGRID_MOVEDOWNINSEQUENCE, OnMoveDownInSequence)
	ON_UPDATE_COMMAND_UI(ID_MESSAGERULESGRID_MOVEDOWNINSEQUENCE, OnUpdateMoveDownInSequence)
#endif
END_MESSAGE_MAP()

GridMessageRules::GridMessageRules(bool p_IsMyData/* = false*/)
	: m_HideUnsetComponents(true)
	, m_ColRuleName(nullptr)
	, m_ColSequence(nullptr)
	, m_ColEnabled(nullptr)
	, m_ColHasError(nullptr)
	, m_ColReadOnly(nullptr)
	, m_ColComponentType(nullptr)
	, m_ColRuleComponent(nullptr)
	, m_ColComponentValue(nullptr)

	, m_ColConditionsBodyContains(nullptr)
	, m_ColConditionsBodyOrSubjectContains(nullptr)
	, m_ColConditionsCategories(nullptr)
	, m_ColConditionsFromAddressesName(nullptr)
	, m_ColConditionsFromAddressesAddress(nullptr)
	, m_ColConditionsHasAttachments(nullptr)
	, m_ColConditionsHeaderContains(nullptr)
	, m_ColConditionsImportance(nullptr)
	, m_ColConditionsIsApprovalRequest(nullptr)
	, m_ColConditionsIsAutomaticForward(nullptr)
	, m_ColConditionsIsAutomaticReply(nullptr)
	, m_ColConditionsIsEncrypted(nullptr)
	, m_ColConditionsIsMeetingRequest(nullptr)
	, m_ColConditionsIsMeetingResponse(nullptr)
	, m_ColConditionsIsNonDeliveryReport(nullptr)
	, m_ColConditionsIsPermissionControlled(nullptr)
	, m_ColConditionsIsReadReceipt(nullptr)
	, m_ColConditionsIsSigned(nullptr)
	, m_ColConditionsIsVoiceMail(nullptr)
	, m_ColConditionsMessageActionFlag(nullptr)
	, m_ColConditionsNotSentToMe(nullptr)
	, m_ColConditionsRecipientContains(nullptr)
	, m_ColConditionsSenderContains(nullptr)
	, m_ColConditionsSensitivity(nullptr)
	, m_ColConditionsSentCCMe(nullptr)
	, m_ColConditionsSentOnlyToMe(nullptr)
	, m_ColConditionsSentToAddressesName(nullptr)
	, m_ColConditionsSentToAddressesAddress(nullptr)
	, m_ColConditionsSentToMe(nullptr)
	, m_ColConditionsSentToOrCCMe(nullptr)
	, m_ColConditionsSubjectContains(nullptr)
	, m_ColConditionsWithinSizeRangeMin(nullptr)
	, m_ColConditionsWithinSizeRangeMax(nullptr)

	, m_ColActionsAssignCategories(nullptr)
	, m_ColActionsCopyToFolder(nullptr)
	, m_ColActionsCopyToFolderName(nullptr)
	, m_ColActionsDelete(nullptr)
	, m_ColActionsForwardAsAttachmentEmailAddressName(nullptr)
	, m_ColActionsForwardAsAttachmentEmailAddressAddress(nullptr)
	, m_ColActionsForwardToEmailAddressName(nullptr)
	, m_ColActionsForwardToEmailAddressAddress(nullptr)
	, m_ColActionsMarkAsRead(nullptr)
	, m_ColActionsMarkImportance(nullptr)
	, m_ColActionsMoveToFolder(nullptr)
	, m_ColActionsMoveToFolderName(nullptr)
	, m_ColActionsPermanentDelete(nullptr)
	, m_ColActionsRedirectToEmailAddressName(nullptr)
	, m_ColActionsRedirectToEmailAddressAddress(nullptr)
	, m_ColActionsStopProcessingRules(nullptr)

	, m_ComponentTypeConditionIconId(NO_ICON)
	, m_ComponentTypeActionIconId(NO_ICON)
	, m_ComponentTypeExceptionIconId(NO_ICON)
	, m_EnabledRuleIconId(NO_ICON)
	, m_DisabledRuleIconId(NO_ICON)

	, m_HasLoadMoreAdditionalInfo(false)
	, m_IsMyData(p_IsMyData)
{
	m_WithOwnerAsTopAncestor = true;

	if (CRMpipe().IsFeatureSandboxed())
	{
		// FIXME: Add desired actions when the required methods are implemented below!
#if HAS_MESSAGE_RULES_EDIT
		UseDefaultActions(O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT);
#else
		UseDefaultActions(O365Grid::ACTION_DELETE);
#endif
	}
	else
	{
		UseDefaultActions(O365Grid::ACTION_DELETE);
	}

	EnableGridModifications();
	SetEditParentWhenChildSelected(true);

	initWizard<AutomationWizardMessageRules>(_YTEXT("Automation\\MessageRules"));

	if (g_ComponentTypeCondition.empty())
		g_ComponentTypeCondition = YtriaTranslate::Do(GLOBAL_BEGIN_MESSAGE_MAP_1, _YLOC("Condition")).c_str();
	if (g_ComponentTypeAction.empty())
		g_ComponentTypeAction = YtriaTranslate::Do(GLOBAL_BEGIN_MESSAGE_MAP_2, _YLOC("Action")).c_str();
	if (g_ComponentTypeException.empty())
		g_ComponentTypeException = YtriaTranslate::Do(GLOBAL_BEGIN_MESSAGE_MAP_3, _YLOC("Exception")).c_str();

	if (IsMyData())
		m_MyDataMeHandler.emplace();
}

ModuleCriteria GridMessageRules::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria;

    auto frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
    ASSERT(nullptr != frame);
    if (nullptr != frame)
    {
        criteria = frame->GetModuleCriteria();
        criteria.m_IDs.clear();
        for (auto row : p_Rows)
            criteria.m_IDs.insert(row->GetField(GetColId()).GetValueStr());
    }

    return criteria;
}

void GridMessageRules::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameMessageRules, LicenseUtil::g_codeSapio365messageRules,
		{
			{ &m_Template.m_ColumnDisplayName, g_TitleOwnerDisplayName, g_FamilyOwner },
			{ &m_Template.m_ColumnUserPrincipalName, g_TitleOwnerUserName, g_FamilyOwner },
			{ &m_Template.m_ColumnID, g_TitleOwnerID, g_FamilyOwner }
		});
		setup.Setup(*this, false);
	}

	if (IsMyData())
		m_Template.m_ColumnDisplayName->RemovePresetFlags(g_ColumnsPresetDefault);

	m_MetaColumns.push_back(m_Template.m_ColumnID);
	m_MetaColumns.push_back(m_Template.m_ColumnDisplayName);
	m_MetaColumns.push_back(m_Template.m_ColumnUserPrincipalName);

	/*Add additional meta columns*/
	const auto& metaDataColumnInfos = getMetaDataColumnInfo();
	if (metaDataColumnInfos)
	{
		for (const auto& c : *metaDataColumnInfos)
		{
			ASSERT(nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()));
			if (nullptr == m_Template.GetColumnForProperty(c.GetPropertyName()))
			{
				auto col = c.AddTo(*this, g_FamilyOwner);
				m_Template.GetColumnForProperty(c.GetPropertyName()) = col;
				m_MetaColumns.push_back(col);
				m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
			}
		}
	}

	static const wstring g_FamilyRule = YtriaTranslate::Do(GridMessageRules_customizeGrid_1, _YLOC("Rule")).c_str();
	m_ColRuleName = AddColumn(_YUID(O365_MESSAGERULE_DISPLAYNAME), YtriaTranslate::Do(GridMessageRules_customizeGrid_2, _YLOC("Rule Name")).c_str(), g_FamilyRule, g_ColumnSize22, { g_ColumnsPresetDefault });

	setBasicGridSetupHierarchy({ { { m_Template.m_ColumnUserPrincipalName, 0 },{ m_ColRuleName, GridBackendUtil::g_AllHierarchyLevels } }
								,{ rttr::type::get<BusinessMessageRuleComponent>().get_id() }
								,{ rttr::type::get<BusinessMessageRuleComponent>().get_id(), rttr::type::get<BusinessMessageRule>().get_id(), rttr::type::get<BusinessUser>().get_id() } });

	m_ColSequence					= AddColumnNumberInt(_YUID(O365_MESSAGERULE_SEQUENCE),	YtriaTranslate::Do(GridMessageRules_customizeGrid_3, _YLOC("Sequence")).c_str(),	g_FamilyRule, g_ColumnSize4, { g_ColumnsPresetDefault });
	// Add icon to this column:
	m_ColEnabled					= AddColumnCheckBox(_YUID(O365_MESSAGERULE_ISENABLED), YtriaTranslate::Do(GridMessageRules_customizeGrid_4, _YLOC("Is Enabled")).c_str(), g_FamilyRule, g_ColumnSize6, { g_ColumnsPresetDefault });

	m_ColHasError					= AddColumnCheckBox(_YUID(O365_MESSAGERULE_HASERROR), YtriaTranslate::Do(GridMessageRules_customizeGrid_5, _YLOC("Has Error")).c_str(), g_FamilyRule, g_ColumnSize6, { g_ColumnsPresetDefault });
	m_ColReadOnly					= AddColumnCheckBox(_YUID(O365_MESSAGERULE_ISREADONLY), YtriaTranslate::Do(GridMessageRules_customizeGrid_6, _YLOC("Read Only")).c_str(), g_FamilyRule, g_ColumnSize6, { g_ColumnsPresetDefault });

	m_ColComponentType				= AddColumn(_YUID("COMPONENTTYPE"), YtriaTranslate::Do(GridMessageRules_customizeGrid_7, _YLOC("Component Type")).c_str(), g_FamilyRule, g_ColumnSize12, { g_ColumnsPresetDefault });
	m_ColRuleComponent				= AddColumn(_YUID("RULECOMPONENT"), YtriaTranslate::Do(GridMessageRules_customizeGrid_8, _YLOC("Rule Component")).c_str(), g_FamilyRule, g_ColumnSize22, { g_ColumnsPresetDefault });

	m_ColComponentValue				= AddColumnVariant(_YUID("COMPONENTVALUE"), YtriaTranslate::Do(GridMessageRules_customizeGrid_9, _YLOC("Component Value")).c_str(), g_FamilyRule, g_ColumnSize32, { g_ColumnsPresetDefault });	
	m_ColComponentValue->SetReadOnly(true);// disable check boxes
	m_ColComponentValue->SetIsMultivalueCousin(true);

	// Conditions columns

	static const wstring g_FamilyConditionsOrExceptions = YtriaTranslate::Do(GridMessageRules_customizeGrid_10, _YLOC("Conditions Or Exceptions")).c_str();

	m_ColConditionsBodyContains				= AddColumn(_YUID("conditions.bodyContains"), YtriaTranslate::Do(GridMessageRules_customizeGrid_11, _YLOC("Body Contains")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);
	m_ColConditionsBodyOrSubjectContains	= AddColumn(_YUID("conditions.bodyOrSubjectContains"), YtriaTranslate::Do(GridMessageRules_customizeGrid_12, _YLOC("Body Or Subject Contains")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);
	m_ColConditionsCategories				= AddColumn(_YUID("conditions.categories"), YtriaTranslate::Do(GridMessageRules_customizeGrid_13, _YLOC("Categories")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);

	m_ColConditionsFromAddressesName		= AddColumn(_YUID("conditions.fromAddresses.emailAddress.name"), YtriaTranslate::Do(GridMessageRules_customizeGrid_14, _YLOC("From")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);
	m_ColConditionsFromAddressesAddress		= AddColumn(_YUID("conditions.fromAddresses.emailAddress.address"), YtriaTranslate::Do(GridMessageRules_customizeGrid_15, _YLOC("From Addresses")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);
	m_ColConditionsFromAddressesName->SetMultivalueFamily(YtriaTranslate::Do(GridMessageRules_customizeGrid_16, _YLOC("Senders")).c_str());
	m_ColConditionsFromAddressesName->AddMultiValueExplosionSister(m_ColConditionsFromAddressesAddress);

	m_ColConditionsHasAttachments	= AddColumnCheckBox(_YUID("conditions.hasAttachments"), YtriaTranslate::Do(GridMessageRules_customizeGrid_17, _YLOC("Has Attachments")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsHeaderContains	= AddColumn(_YUID("conditions.headerContains"), YtriaTranslate::Do(GridMessageRules_customizeGrid_18, _YLOC("Header Contains")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);
	// Low, normal, high
	m_ColConditionsImportance				= AddColumn(_YUID("conditions.importance"), YtriaTranslate::Do(GridMessageRules_customizeGrid_19, _YLOC("Importance")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);
	m_ColConditionsIsApprovalRequest		= AddColumnCheckBox(_YUID("conditions.isApprovalRequest"), YtriaTranslate::Do(GridMessageRules_customizeGrid_20, _YLOC("Is an Approval Request")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsIsAutomaticForward		= AddColumnCheckBox(_YUID("conditions.isAutomaticForward"), YtriaTranslate::Do(GridMessageRules_customizeGrid_21, _YLOC("Is an Automatic Forward")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsIsAutomaticReply			= AddColumnCheckBox(_YUID("conditions.isAutomaticReply"), YtriaTranslate::Do(GridMessageRules_customizeGrid_22, _YLOC("Is an Automatic Reply")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsIsEncrypted				= AddColumnCheckBox(_YUID("conditions.isEncrypted"), YtriaTranslate::Do(GridMessageRules_customizeGrid_23, _YLOC("Is Encrypted")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsIsMeetingRequest			= AddColumnCheckBox(_YUID("conditions.isMeetingRequest"), YtriaTranslate::Do(GridMessageRules_customizeGrid_24, _YLOC("Is a Meeting Request")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsIsMeetingResponse		= AddColumnCheckBox(_YUID("conditions.isMeetingResponse"), YtriaTranslate::Do(GridMessageRules_customizeGrid_25, _YLOC("Is a Meeting Response")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsIsNonDeliveryReport		= AddColumnCheckBox(_YUID("conditions.isNonDeliveryReport"), YtriaTranslate::Do(GridMessageRules_customizeGrid_26, _YLOC("Is a Non Delivery Report")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsIsPermissionControlled	= AddColumnCheckBox(_YUID("conditions.isPermissionControlled"), YtriaTranslate::Do(GridMessageRules_customizeGrid_27, _YLOC("Is Permission Controlled")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsIsReadReceipt			= AddColumnCheckBox(_YUID("conditions.isReadReceipt"), YtriaTranslate::Do(GridMessageRules_customizeGrid_28, _YLOC("Is Read Receipt")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsIsSigned					= AddColumnCheckBox(_YUID("conditions.isSigned"), YtriaTranslate::Do(GridMessageRules_customizeGrid_29, _YLOC("Is Signed")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsIsVoiceMail				= AddColumnCheckBox(_YUID("conditions.isVoicemail"), YtriaTranslate::Do(GridMessageRules_customizeGrid_30, _YLOC("Is a Voicemail")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	// any, call, donotForward, followUp, fyi, forward, noResponseNecessary, read, reply, replyToAll, review
	m_ColConditionsMessageActionFlag	= AddColumn(_YUID("conditions.messageActionFlag"), YtriaTranslate::Do(GridMessageRules_customizeGrid_31, _YLOC("Message Action Flag")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize12);
	m_ColConditionsNotSentToMe			= AddColumnCheckBox(_YUID("conditions.notSentToMe"), YtriaTranslate::Do(GridMessageRules_customizeGrid_32, _YLOC("Is not Sent to Me")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsRecipientContains	= AddColumn(_YUID("conditions.recipientContains"), YtriaTranslate::Do(GridMessageRules_customizeGrid_33, _YLOC("Recipient Contains")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);
	m_ColConditionsSenderContains		= AddColumn(_YUID("conditions.senderContains"), YtriaTranslate::Do(GridMessageRules_customizeGrid_34, _YLOC("Sender Contains")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);
	// normal, personal, private, confidential
	m_ColConditionsSensitivity	= AddColumn(_YUID("conditions.sensitivity"), YtriaTranslate::Do(GridMessageRules_customizeGrid_35, _YLOC("Sensitivity")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize12);
	m_ColConditionsSentCCMe		= AddColumnCheckBox(_YUID("conditions.sentCcMe"), YtriaTranslate::Do(GridMessageRules_customizeGrid_36, _YLOC("Sent CC to Me")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsSentOnlyToMe = AddColumnCheckBox(_YUID("conditions.sentOnlyToMe"), YtriaTranslate::Do(GridMessageRules_customizeGrid_37, _YLOC("Sent only TO Me")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);

	m_ColConditionsSentToAddressesName		= AddColumn(_YUID("conditions.sentToAddresses.emailAddres.name"), YtriaTranslate::Do(GridMessageRules_customizeGrid_38, _YLOC("Sent TO")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);
	m_ColConditionsSentToAddressesAddress	= AddColumn(_YUID("conditions.sentToAddresses.emailAddres.address"), YtriaTranslate::Do(GridMessageRules_customizeGrid_39, _YLOC("Sent TO addresses")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);
	m_ColConditionsSentToAddressesName->SetMultivalueFamily(YtriaTranslate::Do(GridMessageRules_customizeGrid_40, _YLOC("Recipients")).c_str());
	m_ColConditionsSentToAddressesName->AddMultiValueExplosionSister(m_ColConditionsSentToAddressesAddress);

	m_ColConditionsSentToMe			= AddColumnCheckBox(_YUID("conditions.sentToMe"), YtriaTranslate::Do(GridMessageRules_customizeGrid_41, _YLOC("Sent TO Me")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsSentToOrCCMe		= AddColumnCheckBox(_YUID("conditions.sentToOrCCMe"), YtriaTranslate::Do(GridMessageRules_customizeGrid_42, _YLOC("Sent TO or CC Me")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize6);
	m_ColConditionsSubjectContains	= AddColumn(_YUID("conditions.subjectContains"), YtriaTranslate::Do(GridMessageRules_customizeGrid_43, _YLOC("Subject Contains")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize22);

	m_ColConditionsWithinSizeRangeMin = AddColumnNumberInt(_YUID("conditions.withinSizeRange.minimumSize"), YtriaTranslate::Do(GridMessageRules_customizeGrid_44, _YLOC("Minimum size")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize8);
	m_ColConditionsWithinSizeRangeMax = AddColumnNumberInt(_YUID("conditions.withinSizeRange.maximumSize"), YtriaTranslate::Do(GridMessageRules_customizeGrid_45, _YLOC("Maximum size")).c_str(), g_FamilyConditionsOrExceptions, g_ColumnSize8);
	m_ColConditionsWithinSizeRangeMin->SetNumberFormatDisplay(NumberFormat::BYTES, false);
	m_ColConditionsWithinSizeRangeMin->SetNumberFormatDisplay(NumberFormat::BYTES, true);
	m_ColConditionsWithinSizeRangeMax->SetNumberFormatDisplay(NumberFormat::BYTES, false);
	m_ColConditionsWithinSizeRangeMax->SetNumberFormatDisplay(NumberFormat::BYTES, true);


	static const wstring g_FamilyActions = YtriaTranslate::Do(GridMessageRules_customizeGrid_46, _YLOC("Actions")).c_str();

	m_ColActionsAssignCategories = AddColumn(_YUID("actions.assignCategories"), YtriaTranslate::Do(GridMessageRules_customizeGrid_47, _YLOC("Assign Categories")).c_str(), g_FamilyActions, g_ColumnSize22);
	
	m_ColActionsCopyToFolder		= AddColumn(_YUID("actions.copyToFolder"), YtriaTranslate::Do(GridMessageRules_customizeGrid_48, _YLOC("ID - Copy to Folder")).c_str(), g_FamilyActions, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColActionsCopyToFolderName	= AddColumn(_YUID("COPYTOFOLDERNAME"), YtriaTranslate::Do(GridMessageRules_customizeGrid_49, _YLOC("Copy to Folder")).c_str(), g_FamilyActions, g_ColumnSize32);
	m_ColActionsCopyToFolder->SetMultivalueFamily(YtriaTranslate::Do(GridMessageRules_customizeGrid_50, _YLOC("Copy To Folder")).c_str());
	m_ColActionsCopyToFolder->AddMultiValueExplosionSister(m_ColActionsCopyToFolderName);

	m_ColActionsDelete = AddColumnCheckBox(_YUID("actions.delete"), YtriaTranslate::Do(GridMessageRules_customizeGrid_51, _YLOC("Delete")).c_str(), g_FamilyActions, g_ColumnSize6);

	m_ColActionsForwardAsAttachmentEmailAddressName		= AddColumn(_YUID("actions.forwardAsAttachmentTo.emailAddress.name"), YtriaTranslate::Do(GridMessageRules_customizeGrid_52, _YLOC("Forward as Attachment To")).c_str(), g_FamilyActions, g_ColumnSize22);
	m_ColActionsForwardAsAttachmentEmailAddressAddress	= AddColumn(_YUID("actions.forwardAsAttachmentTo.emailAddress.address"), YtriaTranslate::Do(GridMessageRules_customizeGrid_53, _YLOC("Forward as Attachment To Addresses")).c_str(), g_FamilyActions, g_ColumnSize22);
	m_ColActionsForwardAsAttachmentEmailAddressName->SetMultivalueFamily(YtriaTranslate::Do(GridMessageRules_customizeGrid_54, _YLOC("Forward As Attachment")).c_str());
	m_ColActionsForwardAsAttachmentEmailAddressName->AddMultiValueExplosionSister(m_ColActionsForwardAsAttachmentEmailAddressAddress);

	m_ColActionsForwardToEmailAddressName		= AddColumn(_YUID("actions.forwardTo.emailAddress.name"), YtriaTranslate::Do(GridMessageRules_customizeGrid_55, _YLOC("Forward To")).c_str(), g_FamilyActions, g_ColumnSize22);
	m_ColActionsForwardToEmailAddressAddress	= AddColumn(_YUID("actions.forwardTo.emailAddress.address"), YtriaTranslate::Do(GridMessageRules_customizeGrid_56, _YLOC("Forward To Addresses")).c_str(), g_FamilyActions, g_ColumnSize22);
	m_ColActionsForwardToEmailAddressName->SetMultivalueFamily(YtriaTranslate::Do(GridMessageRules_customizeGrid_57, _YLOC("Forward To")).c_str());
	m_ColActionsForwardToEmailAddressName->AddMultiValueExplosionSister(m_ColActionsForwardToEmailAddressAddress);

	m_ColActionsMarkAsRead = AddColumnCheckBox(_YUID("actions.markAsRead"), YtriaTranslate::Do(GridMessageRules_customizeGrid_58, _YLOC("Mark as Read")).c_str(), g_FamilyActions, g_ColumnSize6);
	// low, normal, high
	m_ColActionsMarkImportance = AddColumn(_YUID("actions.markImportance"), YtriaTranslate::Do(GridMessageRules_customizeGrid_59, _YLOC("Mark Importance as")).c_str(), g_FamilyActions, g_ColumnSize12);

	m_ColActionsMoveToFolder		= AddColumn(_YUID("actions.moveToFolder"), YtriaTranslate::Do(GridMessageRules_customizeGrid_60, _YLOC("ID - Move to Folder")).c_str(), g_FamilyActions, g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColActionsMoveToFolderName	= AddColumn(_YUID("MOVETOFOLDERNAME"), YtriaTranslate::Do(GridMessageRules_customizeGrid_61, _YLOC("Move to Folder")).c_str(), g_FamilyActions, g_ColumnSize32);
	m_ColActionsMoveToFolder->SetMultivalueFamily(YtriaTranslate::Do(GridMessageRules_customizeGrid_62, _YLOC("Move To Folder")).c_str());
	m_ColActionsMoveToFolder->AddMultiValueExplosionSister(m_ColActionsMoveToFolderName);

	m_ColActionsPermanentDelete = AddColumnCheckBox(_YUID("actions.permanentDelete"), YtriaTranslate::Do(GridMessageRules_customizeGrid_63, _YLOC("Permanent Delete")).c_str(), g_FamilyActions, g_ColumnSize6);

	m_ColActionsRedirectToEmailAddressName		= AddColumn(_YUID("actions.redirectTo.emailAddress.name"), YtriaTranslate::Do(GridMessageRules_customizeGrid_64, _YLOC("Redirect To")).c_str(), g_FamilyActions, g_ColumnSize22);
	m_ColActionsRedirectToEmailAddressAddress	= AddColumn(_YUID("actions.redirectTo.emailAddress.address"), YtriaTranslate::Do(GridMessageRules_customizeGrid_65, _YLOC("Redirect To Addresses")).c_str(), g_FamilyActions, g_ColumnSize22);
	m_ColActionsRedirectToEmailAddressName->SetMultivalueFamily(YtriaTranslate::Do(GridMessageRules_customizeGrid_66, _YLOC("Redirect To")).c_str());
	m_ColActionsRedirectToEmailAddressName->AddMultiValueExplosionSister(m_ColActionsRedirectToEmailAddressAddress);

	m_ColActionsStopProcessingRules = AddColumnCheckBox(_YUID("actions.stopProcessingRules"), YtriaTranslate::Do(GridMessageRules_customizeGrid_67, _YLOC("Stop Processing Rules")).c_str(), g_FamilyActions, g_ColumnSize6);

	m_ColConditionsBodyContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsBodyOrSubjectContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsCategories->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsFromAddressesName->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsHasAttachments->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsHeaderContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsImportance->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsApprovalRequest->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsAutomaticForward->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsAutomaticReply->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsEncrypted->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsMeetingRequest->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsMeetingResponse->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsNonDeliveryReport->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsPermissionControlled->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsReadReceipt->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsSigned->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsVoiceMail->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsVoiceMail->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsNotSentToMe->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsRecipientContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSenderContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSensitivity->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSentCCMe->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSentOnlyToMe->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSentToAddressesName->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSentToMe->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSentToOrCCMe->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSubjectContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsWithinSizeRangeMax->AddMultiValueExplosionSister(m_ColComponentValue);

	m_ColActionsAssignCategories->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColActionsCopyToFolderName->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColActionsDelete->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColActionsForwardAsAttachmentEmailAddressName->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColActionsForwardToEmailAddressName->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColActionsMarkAsRead->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColActionsMarkImportance->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColActionsMoveToFolderName->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColActionsPermanentDelete->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColActionsRedirectToEmailAddressName->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColActionsStopProcessingRules->AddMultiValueExplosionSister(m_ColComponentValue);

	m_ColConditionsBodyContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsBodyOrSubjectContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsCategories->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsFromAddressesName->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsHasAttachments->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsHeaderContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsImportance->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsApprovalRequest->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsAutomaticForward->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsAutomaticReply->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsEncrypted->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsMeetingRequest->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsMeetingResponse->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsNonDeliveryReport->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsPermissionControlled->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsReadReceipt->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsSigned->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsVoiceMail->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsIsVoiceMail->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsNotSentToMe->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsRecipientContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSenderContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSensitivity->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSentCCMe->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSentOnlyToMe->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSentToAddressesName->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSentToMe->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSentToOrCCMe->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsSubjectContains->AddMultiValueExplosionSister(m_ColComponentValue);
	m_ColConditionsWithinSizeRangeMax->AddMultiValueExplosionSister(m_ColComponentValue);

	addColumnGraphID();
	AddColumnForRowPK(GetColMetaId());
	AddColumnForRowPK(GetColId());
	AddColumnForRowPK(m_ColComponentType);
	AddColumnForRowPK(m_ColRuleComponent);

	m_Template.CustomizeGrid(*this);

	m_ComponentTypeConditionIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_MESSAGERULE_CONDITION));
	m_ComponentTypeActionIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_MESSAGERULE_ACTION));
	m_ComponentTypeExceptionIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_MESSAGERULE_EXCEPTION));
	m_EnabledRuleIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_MESSAGERULE_ENABLED));
	m_DisabledRuleIconId = AddImage(MFCUtil::getCExtCmdIconFromImageResourceID(IDB_ICON_MESSAGERULE_DISABLED));

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameMessageRules*>(GetParentFrame()));
} 

wstring GridMessageRules::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Owner"), m_Template.m_ColumnDisplayName }, { _T("Rule"), m_ColRuleName } },
		_YFORMAT(L"%s (%s)", p_Row->GetField(m_ColRuleComponent).ToString().c_str(), p_Row->GetField(m_ColComponentType).ToString().c_str()));
}

void GridMessageRules::AddCreatedBusinessObjects(const vector<BusinessMessageRule>& p_Rules, bool p_ScrollToNew)
{
	// TODO
	ASSERT(false);
}

void GridMessageRules::UpdateBusinessObjects(const vector<BusinessMessageRule>& p_Rules, bool p_SetModifiedStatus)
{
	GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID/* | GridUpdaterOptions::PURGE | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS/* | GridUpdaterOptions::REFRESH_PROCESS_DATA | GridUpdaterOptions::PROCESS_LOADMORE*/));
	for (const auto& rule : p_Rules)
	{
		GridBackendField fMetaID(rule.m_UserId, GetColMetaId());
		GridBackendField fID(rule.m_Id, GetColId());
		GridBackendRow* row = GetRowIndex().GetExistingRow({ fMetaID, fID });
		ASSERT(nullptr != row);

		if (nullptr != row)
		{
			auto userRow = row->GetParentRow();
			ASSERT(GridUtil::IsBusinessUser(userRow));
			if (nullptr != userRow)
			{
				for (auto c : m_MetaColumns)
				{
					if (userRow->HasField(c))
						row->AddField(userRow->GetField(c));
					else
						row->RemoveField(c);
				}

				fillRow(rule, row, p_SetModifiedStatus, updater);

				/*ASSERT(!user.HasFlag(BusinessObject::CREATED) && !IsTemporaryCreatedObjectID(user.GetID()));
				auto row = m_Template.UpdateRow(*this, user, p_SetModifiedStatus && !user.HasFlag(BusinessObject::CREATED), updater, false);
				ASSERT(!row->IsCreated() && !row->IsDeleted());
				if (row->IsModified() && (row->IsError() || row->IsDeleted()))
				{
					row->SetEditError(false);
					row->SetEditDeleted(false);
				}*/
			}
		}
	}
}

void GridMessageRules::RemoveBusinessObjects(const vector<BusinessMessageRule>& p_Rules)
{
	// TODO
	ASSERT(false);
}

BusinessMessageRule GridMessageRules::GetBusinessMessageRule(GridBackendRow* p_Row, const std::set<UINT>& p_SpecificColumnsOnly) const
{
	ASSERT(GridUtil::isBusinessType<BusinessMessageRule>(p_Row));
	BusinessMessageRule rule;

	rule.m_UserId	= p_Row->GetField(GetColMetaId()).GetValueStr();
	rule.m_Id		= p_Row->GetField(GetColId()).GetValueStr();

	extractGridValue(p_SpecificColumnsOnly, rule.m_DisplayName, m_ColRuleName, p_Row);
	extractGridValue(p_SpecificColumnsOnly, rule.m_IsEnabled, m_ColEnabled, p_Row);
	extractGridValue(p_SpecificColumnsOnly, rule.m_IsReadOnly, m_ColReadOnly, p_Row);
	extractGridValue(p_SpecificColumnsOnly, rule.m_Sequence, m_ColSequence, p_Row);
	extractGridValue(p_SpecificColumnsOnly, rule.m_HasError, m_ColHasError, p_Row);

	/*************************/
	// FIXME:	1. We should find a better way to deal with p_SpecificColumnsOnly below, instead of copy-pasting...
	//			2. The component names are localized and used at several places in this file. We should find a way to define them only once, instead of copy-pasting them.
	/*************************/

	// Conditions
	{
		bool hasValue = false;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsBodyContains })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_BodyContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_11, _YLOC("Body Contains")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsBodyOrSubjectContains })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_BodyOrSubjectContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_12, _YLOC("Body Or Subject Contains")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsCategories })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_Categories, YtriaTranslate::Do(GridMessageRules_customizeGrid_13, _YLOC("Categories")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsFromAddressesAddress, m_ColConditionsFromAddressesName })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_FromAddresses, YtriaTranslate::Do(GridMessageRules_customizeGrid_15, _YLOC("From Addresses")).c_str(), m_ColConditionsFromAddressesAddress, m_ColConditionsFromAddressesName, p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsHasAttachments })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_HasAttachments, YtriaTranslate::Do(GridMessageRules_customizeGrid_17, _YLOC("Has Attachments")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsHeaderContains })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_HeaderContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_18, _YLOC("Header Contains")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsImportance })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_Importance, YtriaTranslate::Do(GridMessageRules_customizeGrid_19, _YLOC("Importance")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsApprovalRequest })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_IsApprovalRequest, YtriaTranslate::Do(GridMessageRules_customizeGrid_20, _YLOC("Is an Approval Request")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsAutomaticForward })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_IsAutomaticForward, YtriaTranslate::Do(GridMessageRules_customizeGrid_21, _YLOC("Is an Automatic Forward")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsAutomaticReply })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_IsAutomaticReply, YtriaTranslate::Do(GridMessageRules_customizeGrid_22, _YLOC("Is an Automatic Reply")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsEncrypted })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_IsEncrypted, YtriaTranslate::Do(GridMessageRules_customizeGrid_23, _YLOC("Is Encrypted")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsMeetingRequest })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_IsMeetingRequest, YtriaTranslate::Do(GridMessageRules_customizeGrid_24, _YLOC("Is a Meeting Request")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsMeetingResponse })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_IsMeetingResponse, YtriaTranslate::Do(GridMessageRules_customizeGrid_25, _YLOC("Is a Meeting Response")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsNonDeliveryReport })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_IsNonDeliveryReport, YtriaTranslate::Do(GridMessageRules_customizeGrid_26, _YLOC("Is a Non Delivery Report")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsPermissionControlled })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_IsPermissionControlled, YtriaTranslate::Do(GridMessageRules_customizeGrid_27, _YLOC("Is Permission Controlled")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsReadReceipt })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_IsReadReceipt, YtriaTranslate::Do(GridMessageRules_customizeGrid_28, _YLOC("Is Read Receipt")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsSigned })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_IsSigned, YtriaTranslate::Do(GridMessageRules_customizeGrid_29, _YLOC("Is Signed")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsVoiceMail })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_IsVoicemail, YtriaTranslate::Do(GridMessageRules_customizeGrid_30, _YLOC("Is a Voicemail")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsMessageActionFlag })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_MessageActionFlag, YtriaTranslate::Do(GridMessageRules_customizeGrid_31, _YLOC("Message Action Flag")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsNotSentToMe })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_NotSentToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_32, _YLOC("Is not Sent To Me")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsRecipientContains })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_RecipientContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_33, _YLOC("Recipient Contains")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSenderContains })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_SenderContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_34, _YLOC("Sender Contains")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSensitivity })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_Sensitivity, YtriaTranslate::Do(GridMessageRules_customizeGrid_35, _YLOC("Sensitivity")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSentCCMe })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_SentCCMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_36, _YLOC("Sent CC to Me")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSentOnlyToMe })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_SentOnlyToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_37, _YLOC("Sent Only To Me")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSentToAddressesAddress, m_ColConditionsSentToAddressesName })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_SentToAddresses, YtriaTranslate::Do(GridMessageRules_customizeGrid_39, _YLOC("Sent To Addresses")).c_str(), m_ColConditionsSentToAddressesAddress, m_ColConditionsSentToAddressesName, p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSentToMe })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_SentToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_41, _YLOC("Sent To Me")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSentToOrCCMe })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_SentToOrCCMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_42, _YLOC("Sent To Or CC Me")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSubjectContains })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_SubjectContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_43, _YLOC("Subject Contains")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsWithinSizeRangeMin, m_ColConditionsWithinSizeRangeMax })
						&& extractGridValue(g_ComponentTypeCondition, rule.m_Conditions.m_WithinSizeRange, YtriaTranslate::Do(GridMessageRules_fillRow_70, _YLOC("Within Size Range")).c_str(), m_ColConditionsWithinSizeRangeMin, m_ColConditionsWithinSizeRangeMax, p_Row))
				|| hasValue;

		if (!hasValue) // No valid value, clear whole object
			rule.m_Conditions = BusinessMessageRulePredicates();
	}

	// Actions
	{
		bool hasValue = false;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColActionsAssignCategories })
						&& extractGridValue(g_ComponentTypeAction, rule.m_Actions.m_AssignCategories, YtriaTranslate::Do(GridMessageRules_customizeGrid_47, _YLOC("Assign Categories")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColActionsCopyToFolder })
						&& extractGridValue(g_ComponentTypeAction, rule.m_Actions.m_CopyToFolderID, /*p_BusinessMessageRule.m_Actions.m_CopyToFolderName, p_BusinessMessageRule.m_Actions.m_CopyToFolderNameError,*/ YtriaTranslate::Do(GridMessageRules_customizeGrid_49, _YLOC("Copy To Folder")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColActionsDelete })
						&& extractGridValue(g_ComponentTypeAction, rule.m_Actions.m_Delete, YtriaTranslate::Do(GridMessageRules_customizeGrid_51, _YLOC("Delete")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColActionsForwardAsAttachmentEmailAddressAddress, m_ColActionsForwardAsAttachmentEmailAddressName })
						&& extractGridValue(g_ComponentTypeAction, rule.m_Actions.m_ForwardAsAttachmentTo, YtriaTranslate::Do(GridMessageRules_customizeGrid_52, _YLOC("Forward As Attachment To")).c_str(), m_ColActionsForwardAsAttachmentEmailAddressAddress, m_ColActionsForwardAsAttachmentEmailAddressName, p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColActionsForwardToEmailAddressAddress, m_ColActionsForwardToEmailAddressName })
						&& extractGridValue(g_ComponentTypeAction, rule.m_Actions.m_ForwardTo, YtriaTranslate::Do(GridMessageRules_customizeGrid_55, _YLOC("Forward To")).c_str(), m_ColActionsForwardToEmailAddressAddress, m_ColActionsForwardToEmailAddressName, p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColActionsMarkAsRead })
						&& extractGridValue(g_ComponentTypeAction, rule.m_Actions.m_MarkAsRead, YtriaTranslate::Do(GridMessageRules_customizeGrid_58, _YLOC("Mark As Read")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColActionsMarkImportance })
						&& extractGridValue(g_ComponentTypeAction, rule.m_Actions.m_MarkImportance, YtriaTranslate::Do(GridMessageRules_customizeGrid_59, _YLOC("Mark Importance")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColActionsMoveToFolder })
						&& extractGridValue(g_ComponentTypeAction, rule.m_Actions.m_MoveToFolderID, /*p_BusinessMessageRule.m_Actions.m_MoveToFolderName, p_BusinessMessageRule.m_Actions.m_MoveToFolderNameError,*/ YtriaTranslate::Do(GridMessageRules_customizeGrid_61, _YLOC("Move To Folder")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColActionsPermanentDelete })
						&& extractGridValue(g_ComponentTypeAction, rule.m_Actions.m_PermanentDelete, YtriaTranslate::Do(GridMessageRules_customizeGrid_63, _YLOC("Permanent Delete")).c_str(), p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColActionsRedirectToEmailAddressAddress, m_ColActionsRedirectToEmailAddressName })
						&& extractGridValue(g_ComponentTypeAction, rule.m_Actions.m_RedirectTo, YtriaTranslate::Do(GridMessageRules_customizeGrid_64, _YLOC("Redirect To")).c_str(), m_ColActionsRedirectToEmailAddressAddress, m_ColActionsRedirectToEmailAddressName, p_Row))
				|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColActionsStopProcessingRules })
						&& extractGridValue(g_ComponentTypeAction, rule.m_Actions.m_StopProcessingRules, YtriaTranslate::Do(GridMessageRules_customizeGrid_67, _YLOC("Stop Processing Rules")).c_str(), p_Row))
				|| hasValue;

		if (!hasValue) // No valid value, clear whole object
			rule.m_Actions = BusinessMessageRuleActions();
	}

	// Exceptions
	{
		bool hasValue = false;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsBodyContains })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_BodyContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_11, _YLOC("Body Contains")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsBodyOrSubjectContains })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_BodyOrSubjectContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_12, _YLOC("Body Or Subject Contains")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsCategories })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_Categories, YtriaTranslate::Do(GridMessageRules_customizeGrid_13, _YLOC("Categories")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsFromAddressesAddress, m_ColConditionsFromAddressesName })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_FromAddresses, YtriaTranslate::Do(GridMessageRules_customizeGrid_15, _YLOC("From Addresses")).c_str(), m_ColConditionsFromAddressesAddress, m_ColConditionsFromAddressesName, p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsHasAttachments })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_HasAttachments, YtriaTranslate::Do(GridMessageRules_customizeGrid_17, _YLOC("Has Attachments")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsHeaderContains })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_HeaderContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_18, _YLOC("Header Contains")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsImportance })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_Importance, YtriaTranslate::Do(GridMessageRules_customizeGrid_19, _YLOC("Importance")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsApprovalRequest })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_IsApprovalRequest, YtriaTranslate::Do(GridMessageRules_customizeGrid_20, _YLOC("Is an Approval Request")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsAutomaticForward })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_IsAutomaticForward, YtriaTranslate::Do(GridMessageRules_customizeGrid_21, _YLOC("Is an Automatic Forward")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsAutomaticReply })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_IsAutomaticReply, YtriaTranslate::Do(GridMessageRules_customizeGrid_22, _YLOC("Is an Automatic Reply")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsEncrypted })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_IsEncrypted, YtriaTranslate::Do(GridMessageRules_customizeGrid_23, _YLOC("Is Encrypted")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsMeetingRequest })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_IsMeetingRequest, YtriaTranslate::Do(GridMessageRules_customizeGrid_24, _YLOC("Is a Meeting Request")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsMeetingResponse })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_IsMeetingResponse, YtriaTranslate::Do(GridMessageRules_customizeGrid_25, _YLOC("Is a Meeting Response")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsNonDeliveryReport })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_IsNonDeliveryReport, YtriaTranslate::Do(GridMessageRules_customizeGrid_26, _YLOC("Is a Non Delivery Report")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsPermissionControlled })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_IsPermissionControlled, YtriaTranslate::Do(GridMessageRules_customizeGrid_27, _YLOC("Is Permission Controlled")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsReadReceipt })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_IsReadReceipt, YtriaTranslate::Do(GridMessageRules_customizeGrid_28, _YLOC("Is Read Receipt")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsSigned })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_IsSigned, YtriaTranslate::Do(GridMessageRules_customizeGrid_29, _YLOC("Is Signed")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsIsVoiceMail })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_IsVoicemail, YtriaTranslate::Do(GridMessageRules_customizeGrid_30, _YLOC("Is a Voicemail")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsMessageActionFlag })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_MessageActionFlag, YtriaTranslate::Do(GridMessageRules_customizeGrid_31, _YLOC("Message Action Flag")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsNotSentToMe })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_NotSentToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_32, _YLOC("Is not Sent To Me")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsRecipientContains })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_RecipientContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_33, _YLOC("Recipient Contains")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSenderContains })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_SenderContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_34, _YLOC("Sender Contains")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSensitivity })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_Sensitivity, YtriaTranslate::Do(GridMessageRules_customizeGrid_35, _YLOC("Sensitivity")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSentCCMe })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_SentCCMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_36, _YLOC("Sent CC to Me")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSentOnlyToMe })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_SentOnlyToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_37, _YLOC("Sent Only To Me")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSentToAddressesAddress, m_ColConditionsSentToAddressesName })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_SentToAddresses, YtriaTranslate::Do(GridMessageRules_customizeGrid_39, _YLOC("Sent To Addresses")).c_str(), m_ColConditionsSentToAddressesAddress, m_ColConditionsSentToAddressesName, p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSentToMe })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_SentToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_41, _YLOC("Sent To Me")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSentToOrCCMe })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_SentToOrCCMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_42, _YLOC("Sent To Or CC Me")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsSubjectContains })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_SubjectContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_43, _YLOC("Subject Contains")).c_str(), p_Row))
			|| hasValue;
		hasValue = (isAnyColumnWanted(p_SpecificColumnsOnly, { m_ColConditionsWithinSizeRangeMin, m_ColConditionsWithinSizeRangeMax })
			&& extractGridValue(g_ComponentTypeException, rule.m_Exceptions.m_WithinSizeRange, YtriaTranslate::Do(GridMessageRules_fillRow_70, _YLOC("Within Size Range")).c_str(), m_ColConditionsWithinSizeRangeMin, m_ColConditionsWithinSizeRangeMax, p_Row))
			|| hasValue;

		if (!hasValue) // No valid value, clear whole object
			rule.m_Exceptions = BusinessMessageRulePredicates();
	}

	return rule;
}

#if HAS_MESSAGE_RULES_EDIT
GridMessageRules::ModifiedRules GridMessageRules::GetModifiedRules(const O365IdsContainer& p_UserIds)
{
	ModifiedRules rules;

	ASSERT(m_ModifiedReadOnlyRowPks.empty());

	vector<GridBackendRow*> explodedSourceRows;
	map<GridBackendColumn*, vector<GridBackendRow*>> explosionsToRestore;

	{
		set<GridBackendRow*> rows;
		for (auto row : GetBackendRows())
		{
			if (!row->IsGroupRow())
			{
				if (row->IsExplosionFragment())
				{
					auto sourceRow = GetMultivalueManager().GetTopSourceRow(row);
					ASSERT(nullptr != sourceRow);
					if (rows.insert(sourceRow).second)
						explodedSourceRows.push_back(sourceRow);
				}
				else
				{
					rows.insert(row);
				}
			}
		}
	}

	if (!explodedSourceRows.empty())
	{
		vector<pair<GridBackendRow*, map<GridBackendColumn*, vector<GridBackendRow*>>>>	implosionSetup;// source row vs. col + exploded rows
		GetMultivalueManager().GetImplosionSetup(explodedSourceRows, implosionSetup);

		ASSERT(implosionSetup.size() == explodedSourceRows.size());
		for (const auto& item1 : implosionSetup)
			for (const auto& item2 : item1.second)
				explosionsToRestore[item2.first].push_back(item1.first);

		MultivalueImplode(explodedSourceRows, true);
	}

	row_pk_t rowPk;
	row_pk_t compPk;
	vector<RowFieldUpdates<Scope::O365>*> ruleMods;
	for (auto& userId : p_UserIds)
	{
		GridBackendRow* userRow = nullptr;
		if (IsMyData())
		{
			ASSERT(p_UserIds.size() == 1);
		}
		else
		{
			GridBackendField fMetaID(userId, GetColMetaId());
			GridBackendField fID(userId, GetColId());
			userRow = GetRowIndex().GetExistingRow({ fMetaID, fID });
		}
		ASSERT(nullptr != userRow || IsMyData());
		if (nullptr != userRow || IsMyData())
		{
			auto ruleRows = GetChildRows(userRow);
			for (auto& ruleRow : ruleRows)
			{
				ruleMods.clear();

				GetRowPK(ruleRow, rowPk);
				auto mod = GetModifications().GetRowFieldModification(rowPk);
				if (nullptr != mod && mod->GetState() == Modification::State::AppliedLocally)
					ruleMods.push_back(mod);

				auto componentRows = GetChildRows(ruleRow);
				for (auto& componentRow : componentRows)
				{
					GetRowPK(componentRow, compPk);
					auto mod = GetModifications().GetRowFieldModification(compPk);
					if (nullptr != mod && mod->GetState() == Modification::State::AppliedLocally)
						ruleMods.push_back(mod);
				}

				if (!ruleMods.empty())
				{
					// FIXME: Right now, explosion refresh doesn't work due to "multivalue cousin" columns.
					// As after apply only modified rules will be refresh, we want to keep them imploded => remove them from the ones to re-eplode.
					for (auto& item : explosionsToRestore)
					{
						item.second.erase(std::remove_if(item.second.begin(), item.second.end(), [ruleRow](GridBackendRow* p_Row)
						{
							return p_Row == ruleRow || p_Row->GetParentRow() == ruleRow;
						}), item.second.end());
					}

					if (ruleRow->GetField(m_ColReadOnly).GetValueBool())
					{
						ASSERT(ruleMods.size() == 1 && ruleMods.back()->GetFieldUpdates().size() == 1
							&& ruleMods.back()->GetFieldUpdates().back()->GetColumnID() == m_ColSequence->GetID());

						if (m_ModifiedReadOnlyRowPks.end() == std::find(m_ModifiedReadOnlyRowPks.begin(), m_ModifiedReadOnlyRowPks.end(), rowPk))
							m_ModifiedReadOnlyRowPks.push_back(rowPk);
					}
					else
					{
						std::set<UINT> modifiedColumnIDs;
						for (auto ruleMod : ruleMods)
						{
							for (const auto& fieldUp : ruleMod->GetFieldUpdates())
							{
								if (fieldUp->GetState() == Modification::State::AppliedLocally)
									modifiedColumnIDs.insert(fieldUp->GetColumnID());
							}
						}

						rules[userId].emplace_back(GetBusinessMessageRule(ruleRow, modifiedColumnIDs), rowPk);
					}
				}
			}
		}
	}

	if (!explosionsToRestore.empty())
	{
		bool update = false;
		for (auto& item : explosionsToRestore)
		{
			if (!item.second.empty())
				update = MultivalueExplode(item.first, item.second, false).first || update;
		}

		if (update)
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	}

	return rules;
}
#endif

GridMessageRules::ModifiedRules GridMessageRules::GetDeletedRules(const O365IdsContainer& p_UserIds)
{
	GridMessageRules::ModifiedRules modRules;

	BusinessMessageRule rule;
	for (auto& del : GetModifications().GetRowModifications([](auto rowMod) {return nullptr != dynamic_cast<DeletedObjectModification*>(rowMod); }))
	{
		auto row = GetRowIndex().GetExistingRow(del->GetRowKey());
		auto userId = PooledString(row->GetField(GetColMetaId()).GetValueStr());
		if (p_UserIds.end() != p_UserIds.find(userId))
		{
			rule.m_Id = row->GetField(GetColId()).GetValueStr();
			rule.m_ShouldDelete = true;
			modRules[userId].emplace_back(rule, del->GetRowKey());
		}
	}

	return modRules;
}

GridBackendColumn* GridMessageRules::GetColMetaId() const
{
	return m_Template.m_ColumnID;
}

#if HAS_MESSAGE_RULES_EDIT
void GridMessageRules::ModificationStateChanged(GridBackendRow* p_Row, RowFieldUpdates<Scope::O365>& p_Modification)
{
	//ASSERT(GridUtil::isBusinessType<BusinessMessageRule>(p_Row));
	if (GridUtil::isBusinessType<BusinessMessageRule>(p_Row))
	{
		auto childRows = GetChildRows(p_Row);
		{
			for (const auto& fieldUpdate : p_Modification.GetFieldUpdates())
			{
				const auto colID = fieldUpdate->GetColumnID();
				const auto& parentField = p_Row->GetField(colID);
				auto fixModifiedFlag = [isModified = parentField.IsModified()](GridBackendField& p_Field)
				{
					if (isModified)
						p_Field.SetModified();
					else
						p_Field.RemoveModified();
				};

				if (parentField.IsGeneric())
				{
					for (auto& row : childRows)
						row->RemoveField(colID);
				}
				else if (!parentField.HasValue())
				{
					for (auto& row : childRows)
						fixModifiedFlag(row->AddGenericFieldCopy(colID));
				}
				else
				{
					for (auto& row : childRows)
						fixModifiedFlag(row->AddField(parentField));
				}
			}
		}
	}
}
#endif

const wstring& GridMessageRules::GetComponentTypeCondition()
{
	return g_ComponentTypeCondition;
}

const wstring& GridMessageRules::GetComponentTypeAction()
{
	return g_ComponentTypeAction;
}

const wstring& GridMessageRules::GetComponentTypeException()
{
	return g_ComponentTypeException;
}

bool GridMessageRules::HasModificationsPending(bool inSelectionOnly /*= false*/)
{
	if (inSelectionOnly)
		return HasModificationsPendingInParentOfSelected();
	return ModuleO365Grid<BusinessMessageRule>::HasModificationsPending(false);
}

bool GridMessageRules::IsMyData() const
{
	return m_IsMyData;
}

O365Grid::SnapshotCaps GridMessageRules::GetSnapshotCapabilities() const
{
	return { true };
}

BusinessMessageRule GridMessageRules::getBusinessObject(GridBackendRow* p_Row) const
{
	return GetBusinessMessageRule(p_Row, {});
}

#if HAS_MESSAGE_RULES_EDIT
bool GridMessageRules::canEdit(GridBackendRow* p_Row) const
{
	ASSERT(GridUtil::isBusinessType<BusinessMessageRule>(p_Row));
	return GridUtil::isBusinessType<BusinessMessageRule>(p_Row) && !p_Row->GetField(m_ColReadOnly).GetValueBool();
}
#endif

bool GridMessageRules::canDelete(GridBackendRow* p_Row) const
{
	ASSERT(GridUtil::isBusinessType<BusinessMessageRule>(p_Row));
	return GridUtil::isBusinessType<BusinessMessageRule>(p_Row) && !p_Row->GetField(m_ColReadOnly).GetValueBool();
}

void GridMessageRules::OnShowUnsetComponents()
{
	m_HideUnsetComponents = !m_HideUnsetComponents;
	techHideUnsetComponents(m_HideUnsetComponents, true);
}

void GridMessageRules::OnUpdateShowUnsetComponents(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning());
	pCmdUI->SetCheck(m_HideUnsetComponents ? FALSE : TRUE);
	setTextFromProfUISCommand(*pCmdUI);
}

#if HAS_MESSAGE_RULES_EDIT
void GridMessageRules::OnMoveUpInSequence()
{
	if (CRMpipe().IsFeatureSandboxed())
	{
		std::map<GridBackendRow*, tsl::ordered_set<GridBackendRow*>> rulesToMoveUp;
		for (auto& row : GetSelectedRows())
		{
			if (!row->IsGroupRow() && row->GetHierarchyLevel() > 0)
			{
				auto ruleRow = row->GetAncestorAtLevel(1);
				auto userRow = ruleRow->GetParentRow();
				if (ruleRow->GetField(m_ColSequence).GetValueLong() <= 1)
				{
					ASSERT(false);
					return;
				}

				rulesToMoveUp[userRow].insert(ruleRow);
			}
		}

		if (!rulesToMoveUp.empty())
		{
			set<GridBackendRow*> modifiedRules;
			for (auto& item : rulesToMoveUp)
			{
				auto allRules = GetChildRows(item.first);

				// Sort by increasing sequence number
				std::sort(allRules.begin(), allRules.end(), [this](const GridBackendRow* p_Left, const GridBackendRow* p_Right)
				{
					return p_Left->GetField(m_ColSequence).GetValueLong() < p_Right->GetField(m_ColSequence).GetValueLong();
				});

				for (size_t sequenceIndex = 0; sequenceIndex < allRules.size(); ++sequenceIndex)
				{
					if (sequenceIndex > 0 && item.second.end() != item.second.find(allRules[sequenceIndex]))
					{
						std::swap(allRules[sequenceIndex], allRules[sequenceIndex - 1]);
						updateField(__int3264((sequenceIndex)+1), allRules[sequenceIndex], m_ColSequence, true);
						updateField(__int3264((sequenceIndex - 1) + 1), allRules[sequenceIndex - 1], m_ColSequence, true);

						modifiedRules.insert(allRules[sequenceIndex]);
						modifiedRules.insert(allRules[sequenceIndex - 1]);
					}
				}
			}

			for (auto row : modifiedRules)
			{
				auto children = GetChildRows(row);

				for (auto child : children)
				{
					if (row->HasField(m_ColSequence))
						child->AddField(row->GetField(m_ColSequence));
					else
						child->RemoveField(m_ColSequence);
				}
			}

			UpdateMegaSharkOptimized(m_ColSequence->GetID());
		}
	}
}

void GridMessageRules::OnUpdateMoveUpInSequence(CCmdUI* pCmdUI)
{
	bool enable = false;
	if (CRMpipe().IsFeatureSandboxed() && hasNoTaskRunning())
	{
		for (auto& row : GetSelectedRows())
		{
			if (!row->IsGroupRow() && row->GetHierarchyLevel() > 0)
			{
				enable = true;
				auto ruleRow = row->GetAncestorAtLevel(1);
				if (ruleRow->GetField(m_ColSequence).GetValueLong() <= 1)
				{
					enable = false;
					break;
				}
			}
		}
	}

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridMessageRules::OnMoveDownInSequence()
{
	if (CRMpipe().IsFeatureSandboxed())
	{
		std::map<GridBackendRow*, tsl::ordered_set<GridBackendRow*>> rulesToMoveDown;
		for (auto& row : GetSelectedRows())
		{
			if (!row->IsGroupRow() && row->GetHierarchyLevel() > 0)
			{
				auto ruleRow = row->GetAncestorAtLevel(1);
				auto userRow = ruleRow->GetParentRow();
				if (ruleRow->GetField(m_ColSequence).GetValueLong() >= GetBackend().GetChildrenCount(userRow))
				{
					ASSERT(false);
					return;
				}

				rulesToMoveDown[userRow].insert(ruleRow);
			}
		}

		if (!rulesToMoveDown.empty())
		{
			set<GridBackendRow*> modifiedRules;
			for (auto& item : rulesToMoveDown)
			{
				auto allRules = GetChildRows(item.first);

				// Sort by decreasing sequence number
				std::sort(allRules.begin(), allRules.end(), [this](const GridBackendRow* p_Left, const GridBackendRow* p_Right)
				{
					return p_Left->GetField(m_ColSequence).GetValueLong() > p_Right->GetField(m_ColSequence).GetValueLong();
				});

				for (size_t sequenceReverseIndex = 0; sequenceReverseIndex < allRules.size(); ++sequenceReverseIndex)
				{
					if (sequenceReverseIndex > 0 && item.second.end() != item.second.find(allRules[sequenceReverseIndex]))
					{
						std::swap(allRules[sequenceReverseIndex], allRules[sequenceReverseIndex - 1]);

						updateField(__int3264(allRules.size() - sequenceReverseIndex), allRules[sequenceReverseIndex], m_ColSequence, true);
						updateField(__int3264(allRules.size() - (sequenceReverseIndex - 1)), allRules[sequenceReverseIndex - 1], m_ColSequence, true);

						modifiedRules.insert(allRules[sequenceReverseIndex]);
						modifiedRules.insert(allRules[sequenceReverseIndex - 1]);
					}
				}
			}
			for (auto row : modifiedRules)
			{
				auto children = GetChildRows(row);

				for (auto child : children)
				{
					if (row->HasField(m_ColSequence))
						child->AddField(row->GetField(m_ColSequence));
					else
						child->RemoveField(m_ColSequence);
				}
			}

			UpdateMegaSharkOptimized(m_ColSequence->GetID());
		}
	}
}

void GridMessageRules::OnUpdateMoveDownInSequence(CCmdUI* pCmdUI)
{
	bool enable = false;
	if (CRMpipe().IsFeatureSandboxed() && hasNoTaskRunning())
	{
		for (auto& row : GetSelectedRows())
		{
			if (!row->IsGroupRow() && row->GetHierarchyLevel() > 0)
			{
				enable = true;
				auto ruleRow = row->GetAncestorAtLevel(1);
				auto userRow = ruleRow->GetParentRow();
				if (ruleRow->GetField(m_ColSequence).GetValueLong() >= GetBackend().GetChildrenCount(userRow))
				{
					enable = false;
					break;
				}
			}
		}
	}

	pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}
#endif

void GridMessageRules::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ASSERT(nullptr != pPopup);
	if (nullptr != pPopup)
	{
#if HAS_MESSAGE_RULES_EDIT
		pPopup->ItemInsert(ID_MESSAGERULESGRID_MOVEUPINSEQUENCE);
		pPopup->ItemInsert(ID_MESSAGERULESGRID_MOVEDOWNINSEQUENCE);
		pPopup->ItemInsert(); // Sep
#endif
		pPopup->ItemInsert(ID_MESSAGERULESGRID_SHOWUNSETCOMPONENTS);
		pPopup->ItemInsert(); // Sep
	}

	ModuleO365Grid<BusinessMessageRule>::OnCustomPopupMenu(pPopup, nStart);
}

void GridMessageRules::BuildTreeView(const O365DataMap<BusinessUser, vector<BusinessMessageRule>>& p_MessageRules, const vector<O365UpdateOperation>& p_UpdateOperations, bool p_PartialRefresh)
{
	ASSERT(!IsMyData() || p_MessageRules.size() == 1);

	const uint32_t options = GridUpdaterOptions::UPDATE_GRID
		| (!p_PartialRefresh || IsMyData() ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
		| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		| GridUpdaterOptions::REFRESH_PROCESS_DATA
		/*| GridUpdaterOptions::PROCESS_LOADMORE*/;

	GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateOperations);
	GridIgnoreModification ignoramus(*this);

	for (const auto& keyVal : p_MessageRules)
	{
		if (m_HasLoadMoreAdditionalInfo)
		{
			if (keyVal.first.IsMoreLoaded())
				m_MetaIDsMoreLoaded.insert(keyVal.first.GetID());
			else
				m_MetaIDsMoreLoaded.erase(keyVal.first.GetID());
		}

		auto parentRow = m_Template.AddRow(*this, keyVal.first, {}, updater, false, true);
		ASSERT(nullptr != parentRow);
		if (nullptr != parentRow)
		{
			if (!IsMyData())
			{
				updater.GetOptions().AddRowWithRefreshedValues(parentRow);
				updater.GetOptions().AddPartialPurgeParentRow(parentRow);
			}

			parentRow->SetHierarchyParentToHide(true);

			if (!keyVal.first.HasFlag(BusinessObject::CANCELED))
			{
				if (keyVal.second.size() == 1 && keyVal.second[0].GetError())
				{
					updater.OnLoadingError(parentRow, *keyVal.second[0].GetError());
				}
				else
				{
					// Clear previous error
					if (parentRow->IsError())
						parentRow->ClearStatus();

					for (const auto& messageRule : keyVal.second)
					{
						GridBackendField fID(messageRule.m_Id, GetColId());
						GridBackendRow* row = GetRowIndex().GetRow({ parentRow->GetField(GetColMetaId()), fID }, true, true);
						ASSERT(nullptr != row);
						if (nullptr != row)
						{
							updater.GetOptions().AddRowWithRefreshedValues(row);
							updater.AddUpdatedRowPk({ fID, parentRow->GetField(GetColMetaId()) });

							row->SetParentRow(parentRow);
							buildTreeViewImpl(messageRule, row, updater);
						}
					}
				}
			}

			if (IsMyData())
			{
				ASSERT(m_MyDataMeHandler);
				ASSERT(parentRow->HasChildrenRows());
				m_MyDataMeHandler->GetRidOfMeRow(*this, parentRow);
			}
		}
	}

	// Erase the modified row pks for modifications that have been finalized by last update
	m_ModifiedReadOnlyRowPks.erase(std::remove_if(m_ModifiedReadOnlyRowPks.begin(), m_ModifiedReadOnlyRowPks.end(), [this](const row_pk_t& pk)
	{
		return nullptr == GetModifications().GetRowFieldModification(pk);
	}), m_ModifiedReadOnlyRowPks.end());

	// Refresh modification state for those read-only rows (for sequence number)
	for (const auto& rowPk : m_ModifiedReadOnlyRowPks)
		updater.AddUpdatedRowPk(rowPk);
}

void GridMessageRules::RefreshTreeView(const O365DataMap<PooledString, vector<BusinessMessageRule>>& p_MessageRules, const vector<O365UpdateOperation>& p_UpdateOperations)
{
	const uint32_t options = GridUpdaterOptions::UPDATE_GRID
		/*| GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE*/
		| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		| GridUpdaterOptions::REFRESH_PROCESS_DATA
		/*| GridUpdaterOptions::PROCESS_LOADMORE*/;

	GridUpdater updater(*this, GridUpdaterOptions(options), p_UpdateOperations);
	GridIgnoreModification ignoramus(*this);

	for (const auto& keyVal : p_MessageRules)
	{
		const auto& userID = keyVal.first;

		GridBackendField fMetaID(userID, GetColMetaId());
		GridBackendField fID(userID, GetColId());
		GridBackendRow* parentRow = GetRowIndex().GetExistingRow({ fMetaID, fID });
		ASSERT(nullptr != parentRow);
		if (nullptr != parentRow)
		{
			//updater.GetOptions().AddPartialPurgeParentRow(parentRow);

			// Clear previous error
			if (parentRow->IsError())
				parentRow->ClearStatus();

			for (const auto& messageRule : keyVal.second)
			{
				GridBackendField fID(messageRule.m_Id, GetColId());
				GridBackendRow* row = GetRowIndex().GetRow({ parentRow->GetField(GetColMetaId()), fID }, true, true);
				ASSERT(nullptr != row);
				if (nullptr != row)
				{
					updater.GetOptions().AddRowWithRefreshedValues(row);
					updater.AddUpdatedRowPk({ fID, parentRow->GetField(GetColMetaId()) });

					row->SetParentRow(parentRow);
					buildTreeViewImpl(messageRule, row, updater);
				}
			}
		}
	}

	// Erase the modified row pks for modifications that have been finalized by last update
	m_ModifiedReadOnlyRowPks.erase(std::remove_if(m_ModifiedReadOnlyRowPks.begin(), m_ModifiedReadOnlyRowPks.end(), [this](const row_pk_t& pk)
	{
		return nullptr == GetModifications().GetRowFieldModification(pk);
	}), m_ModifiedReadOnlyRowPks.end());

	// Refresh modification state for those read-only two (for sequence number)
	for (const auto& rowPk : m_ModifiedReadOnlyRowPks)
		updater.AddUpdatedRowPk(rowPk);
}

void GridMessageRules::buildTreeViewImpl(const BusinessMessageRule& p_BusinessMessageRule, GridBackendRow* p_Row, GridUpdater& p_Updater)
{
	SetRowObjectType(p_Row, p_BusinessMessageRule, p_BusinessMessageRule.HasFlag(BusinessObject::CANCELED));
	if (!p_BusinessMessageRule.HasFlag(BusinessObject::CANCELED))
	{
		if (p_BusinessMessageRule.GetError())
		{
			p_Updater.OnLoadingError(p_Row, *p_BusinessMessageRule.GetError());
		}
		else
		{
			// Clear previous error
			if (p_Row->IsError())
				p_Row->ClearStatus();

			auto userRow = p_Row->GetParentRow();
			ASSERT(nullptr != userRow && GridUtil::IsBusinessUser(userRow));

			for (auto c : m_MetaColumns)
			{
				if (userRow->HasField(c))
					p_Row->AddField(userRow->GetField(c));
				else
					p_Row->RemoveField(c);
			}

			fillRow(p_BusinessMessageRule, p_Row, false, p_Updater);
		}
	}
}

void GridMessageRules::fillRow(const BusinessMessageRule& p_BusinessMessageRule, GridBackendRow* p_Row, bool p_SetModifiedStatus, GridUpdater& p_Updater)
{
	updateField(p_BusinessMessageRule.m_DisplayName, p_Row, m_ColRuleName, p_SetModifiedStatus);
	updateField(p_BusinessMessageRule.m_Sequence, p_Row, m_ColSequence, p_SetModifiedStatus);
	auto field = updateField(p_BusinessMessageRule.m_IsEnabled, p_Row, m_ColEnabled, p_SetModifiedStatus);
	if (nullptr != field && field->HasValue())
		field->SetIcon(field->GetValueBool() ? m_EnabledRuleIconId : m_DisabledRuleIconId);

	updateField(p_BusinessMessageRule.m_HasError, p_Row, m_ColHasError, p_SetModifiedStatus);
	updateField(p_BusinessMessageRule.m_IsReadOnly, p_Row, m_ColReadOnly, p_SetModifiedStatus);

	// Conditions
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_BodyContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_11, _YLOC("Body Contains")).c_str(), m_ColConditionsBodyContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_BodyOrSubjectContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_12, _YLOC("Body Or Subject Contains")).c_str(), m_ColConditionsBodyOrSubjectContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_Categories, YtriaTranslate::Do(GridMessageRules_customizeGrid_13, _YLOC("Categories")).c_str(), m_ColConditionsCategories, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_FromAddresses, YtriaTranslate::Do(GridMessageRules_customizeGrid_15, _YLOC("From Addresses")).c_str(), m_ColConditionsFromAddressesAddress, m_ColConditionsFromAddressesName, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_HasAttachments, YtriaTranslate::Do(GridMessageRules_customizeGrid_17, _YLOC("Has Attachments")).c_str(), m_ColConditionsHasAttachments, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_HeaderContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_18, _YLOC("Header Contains")).c_str(), m_ColConditionsHeaderContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_Importance, YtriaTranslate::Do(GridMessageRules_customizeGrid_19, _YLOC("Importance")).c_str(), m_ColConditionsImportance, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_IsApprovalRequest, YtriaTranslate::Do(GridMessageRules_customizeGrid_20, _YLOC("Is an Approval Request")).c_str(), m_ColConditionsIsApprovalRequest, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_IsAutomaticForward, YtriaTranslate::Do(GridMessageRules_customizeGrid_21, _YLOC("Is an Automatic Forward")).c_str(), m_ColConditionsIsAutomaticForward, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_IsAutomaticReply, YtriaTranslate::Do(GridMessageRules_customizeGrid_22, _YLOC("Is an Automatic Reply")).c_str(), m_ColConditionsIsAutomaticReply, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_IsEncrypted, YtriaTranslate::Do(GridMessageRules_customizeGrid_23, _YLOC("Is Encrypted")).c_str(), m_ColConditionsIsEncrypted, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_IsMeetingRequest, YtriaTranslate::Do(GridMessageRules_customizeGrid_24, _YLOC("Is a Meeting Request")).c_str(), m_ColConditionsIsMeetingRequest, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_IsMeetingResponse, YtriaTranslate::Do(GridMessageRules_customizeGrid_25, _YLOC("Is a Meeting Response")).c_str(), m_ColConditionsIsMeetingResponse, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_IsNonDeliveryReport, YtriaTranslate::Do(GridMessageRules_customizeGrid_26, _YLOC("Is a Non Delivery Report")).c_str(), m_ColConditionsIsNonDeliveryReport, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_IsPermissionControlled, YtriaTranslate::Do(GridMessageRules_customizeGrid_27, _YLOC("Is Permission Controlled")).c_str(), m_ColConditionsIsPermissionControlled, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_IsReadReceipt, YtriaTranslate::Do(GridMessageRules_customizeGrid_28, _YLOC("Is Read Receipt")).c_str(), m_ColConditionsIsReadReceipt, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_IsSigned, YtriaTranslate::Do(GridMessageRules_customizeGrid_29, _YLOC("Is Signed")).c_str(), m_ColConditionsIsSigned, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_IsVoicemail, YtriaTranslate::Do(GridMessageRules_customizeGrid_30, _YLOC("Is a Voicemail")).c_str(), m_ColConditionsIsVoiceMail, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_MessageActionFlag, YtriaTranslate::Do(GridMessageRules_customizeGrid_31, _YLOC("Message Action Flag")).c_str(), m_ColConditionsIsVoiceMail, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_NotSentToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_32, _YLOC("Is not Sent To Me")).c_str(), m_ColConditionsNotSentToMe, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_RecipientContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_33, _YLOC("Recipient Contains")).c_str(), m_ColConditionsRecipientContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_SenderContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_34, _YLOC("Sender Contains")).c_str(), m_ColConditionsSenderContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_Sensitivity, YtriaTranslate::Do(GridMessageRules_customizeGrid_35, _YLOC("Sensitivity")).c_str(), m_ColConditionsSensitivity, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_SentCCMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_36, _YLOC("Sent CC to Me")).c_str(), m_ColConditionsSentCCMe, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_SentOnlyToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_37, _YLOC("Sent Only To Me")).c_str(), m_ColConditionsSentOnlyToMe, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_SentToAddresses, YtriaTranslate::Do(GridMessageRules_customizeGrid_39, _YLOC("Sent To Addresses")).c_str(), m_ColConditionsSentToAddressesAddress, m_ColConditionsSentToAddressesName, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_SentToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_41, _YLOC("Sent To Me")).c_str(), m_ColConditionsSentToMe, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_SentToOrCCMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_42, _YLOC("Sent To Or CC Me")).c_str(), m_ColConditionsSentToOrCCMe, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_SubjectContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_43, _YLOC("Subject Contains")).c_str(), m_ColConditionsSubjectContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeCondition, p_BusinessMessageRule.m_Conditions.m_WithinSizeRange, YtriaTranslate::Do(GridMessageRules_fillRow_70, _YLOC("Within Size Range")).c_str(), m_ColConditionsWithinSizeRangeMin, m_ColConditionsWithinSizeRangeMax, p_Row, p_SetModifiedStatus, p_Updater);

	// Actions
	addRuleCompRow(g_ComponentTypeAction, p_BusinessMessageRule.m_Actions.m_AssignCategories, YtriaTranslate::Do(GridMessageRules_customizeGrid_47, _YLOC("Assign Categories")).c_str(), m_ColActionsAssignCategories, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeAction, p_BusinessMessageRule.m_Actions.m_CopyToFolderID, p_BusinessMessageRule.m_Actions.m_CopyToFolderName, p_BusinessMessageRule.m_Actions.m_CopyToFolderNameError, YtriaTranslate::Do(GridMessageRules_customizeGrid_49, _YLOC("Copy To Folder")).c_str(), m_ColActionsCopyToFolder, m_ColActionsCopyToFolderName, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeAction, p_BusinessMessageRule.m_Actions.m_Delete, YtriaTranslate::Do(GridMessageRules_customizeGrid_51, _YLOC("Delete")).c_str(), m_ColActionsDelete, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeAction, p_BusinessMessageRule.m_Actions.m_ForwardAsAttachmentTo, YtriaTranslate::Do(GridMessageRules_customizeGrid_52, _YLOC("Forward As Attachment To")).c_str(), m_ColActionsForwardAsAttachmentEmailAddressAddress, m_ColActionsForwardAsAttachmentEmailAddressName, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeAction, p_BusinessMessageRule.m_Actions.m_ForwardTo, YtriaTranslate::Do(GridMessageRules_customizeGrid_55, _YLOC("Forward To")).c_str(), m_ColActionsForwardToEmailAddressAddress, m_ColActionsForwardToEmailAddressName, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeAction, p_BusinessMessageRule.m_Actions.m_MarkAsRead, YtriaTranslate::Do(GridMessageRules_customizeGrid_58, _YLOC("Mark As Read")).c_str(), m_ColActionsMarkAsRead, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeAction, p_BusinessMessageRule.m_Actions.m_MarkImportance, YtriaTranslate::Do(GridMessageRules_customizeGrid_59, _YLOC("Mark Importance")).c_str(), m_ColActionsMarkImportance, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeAction, p_BusinessMessageRule.m_Actions.m_MoveToFolderID, p_BusinessMessageRule.m_Actions.m_MoveToFolderName, p_BusinessMessageRule.m_Actions.m_MoveToFolderNameError, YtriaTranslate::Do(GridMessageRules_customizeGrid_61, _YLOC("Move To Folder")).c_str(), m_ColActionsMoveToFolder, m_ColActionsMoveToFolderName, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeAction, p_BusinessMessageRule.m_Actions.m_PermanentDelete, YtriaTranslate::Do(GridMessageRules_customizeGrid_63, _YLOC("Permanent Delete")).c_str(), m_ColActionsPermanentDelete, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeAction, p_BusinessMessageRule.m_Actions.m_RedirectTo, YtriaTranslate::Do(GridMessageRules_customizeGrid_64, _YLOC("Redirect To")).c_str(), m_ColActionsRedirectToEmailAddressAddress, m_ColActionsRedirectToEmailAddressName, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeAction, p_BusinessMessageRule.m_Actions.m_StopProcessingRules, YtriaTranslate::Do(GridMessageRules_customizeGrid_67, _YLOC("Stop Processing Rules")).c_str(), m_ColActionsStopProcessingRules, p_Row, p_SetModifiedStatus, p_Updater);

	// Exceptions
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_BodyContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_11, _YLOC("Body Contains")).c_str(), m_ColConditionsBodyContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_BodyOrSubjectContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_12, _YLOC("Body Or Subject Contains")).c_str(), m_ColConditionsBodyOrSubjectContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_Categories, YtriaTranslate::Do(GridMessageRules_customizeGrid_13, _YLOC("Categories")).c_str(), m_ColConditionsCategories, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_FromAddresses, YtriaTranslate::Do(GridMessageRules_customizeGrid_15, _YLOC("From Addresses")).c_str(), m_ColConditionsFromAddressesAddress, m_ColConditionsFromAddressesName, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_HasAttachments, YtriaTranslate::Do(GridMessageRules_customizeGrid_17, _YLOC("Has Attachments")).c_str(), m_ColConditionsHasAttachments, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_HeaderContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_18, _YLOC("Header Contains")).c_str(), m_ColConditionsHeaderContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_Importance, YtriaTranslate::Do(GridMessageRules_customizeGrid_19, _YLOC("Importance")).c_str(), m_ColConditionsImportance, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_IsApprovalRequest, YtriaTranslate::Do(GridMessageRules_customizeGrid_20, _YLOC("Is an Approval Request")).c_str(), m_ColConditionsIsApprovalRequest, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_IsAutomaticForward, YtriaTranslate::Do(GridMessageRules_customizeGrid_21, _YLOC("Is an Automatic Forward")).c_str(), m_ColConditionsIsAutomaticForward, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_IsAutomaticReply, YtriaTranslate::Do(GridMessageRules_customizeGrid_22, _YLOC("Is an Automatic Reply")).c_str(), m_ColConditionsIsAutomaticReply, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_IsEncrypted, YtriaTranslate::Do(GridMessageRules_customizeGrid_23, _YLOC("Is Encrypted")).c_str(), m_ColConditionsIsEncrypted, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_IsMeetingRequest, YtriaTranslate::Do(GridMessageRules_customizeGrid_24, _YLOC("Is a Meeting Request")).c_str(), m_ColConditionsIsMeetingRequest, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_IsMeetingResponse, YtriaTranslate::Do(GridMessageRules_customizeGrid_25, _YLOC("Is a Meeting Response")).c_str(), m_ColConditionsIsMeetingResponse, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_IsNonDeliveryReport, YtriaTranslate::Do(GridMessageRules_customizeGrid_26, _YLOC("Is a Non Delivery Report")).c_str(), m_ColConditionsIsNonDeliveryReport, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_IsPermissionControlled, YtriaTranslate::Do(GridMessageRules_customizeGrid_27, _YLOC("Is Permission Controlled")).c_str(), m_ColConditionsIsPermissionControlled, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_IsReadReceipt, YtriaTranslate::Do(GridMessageRules_customizeGrid_28, _YLOC("Is Read Receipt")).c_str(), m_ColConditionsIsReadReceipt, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_IsSigned, YtriaTranslate::Do(GridMessageRules_customizeGrid_29, _YLOC("Is Signed")).c_str(), m_ColConditionsIsSigned, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_IsVoicemail, YtriaTranslate::Do(GridMessageRules_customizeGrid_30, _YLOC("Is a Voicemail")).c_str(), m_ColConditionsIsVoiceMail, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_MessageActionFlag, YtriaTranslate::Do(GridMessageRules_customizeGrid_31, _YLOC("Message Action Flag")).c_str(), m_ColConditionsIsVoiceMail, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_NotSentToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_32, _YLOC("Is not Sent to Me")).c_str(), m_ColConditionsNotSentToMe, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_RecipientContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_33, _YLOC("Recipient Contains")).c_str(), m_ColConditionsRecipientContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_SenderContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_34, _YLOC("Sender Contains")).c_str(), m_ColConditionsSenderContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_Sensitivity, YtriaTranslate::Do(GridMessageRules_customizeGrid_35, _YLOC("Sensitivity")).c_str(), m_ColConditionsSensitivity, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_SentCCMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_36, _YLOC("Sent CC to Me")).c_str(), m_ColConditionsSentCCMe, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_SentOnlyToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_37, _YLOC("Sent Only To Me")).c_str(), m_ColConditionsSentOnlyToMe, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_SentToAddresses, YtriaTranslate::Do(GridMessageRules_customizeGrid_39, _YLOC("Sent To Addresses")).c_str(), m_ColConditionsSentToAddressesAddress, m_ColConditionsSentToAddressesName, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_SentToMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_41, _YLOC("Sent To Me")).c_str(), m_ColConditionsSentToMe, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_SentToOrCCMe, YtriaTranslate::Do(GridMessageRules_customizeGrid_42, _YLOC("Sent To Or CC Me")).c_str(), m_ColConditionsSentToOrCCMe, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_SubjectContains, YtriaTranslate::Do(GridMessageRules_customizeGrid_43, _YLOC("Subject Contains")).c_str(), m_ColConditionsSubjectContains, p_Row, p_SetModifiedStatus, p_Updater);
	addRuleCompRow(g_ComponentTypeException, p_BusinessMessageRule.m_Exceptions.m_WithinSizeRange, YtriaTranslate::Do(GridMessageRules_fillRow_70, _YLOC("Within Size Range")).c_str(), m_ColConditionsWithinSizeRangeMin, m_ColConditionsWithinSizeRangeMax, p_Row, p_SetModifiedStatus, p_Updater);
}

void GridMessageRules::addRuleCompRow(const wstring& p_CompType, const boost::YOpt<PooledString>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_Col, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater)
{
	//if (p_val)
	{
		auto ruleCompRow = addRuleCompRow(p_CompType, p_RuleComp, p_ParentRow, p_SetModifiedStatus, p_Updater);
		ASSERT(nullptr != ruleCompRow);
		if (nullptr != ruleCompRow)
		{
			updateField(p_val, ruleCompRow, p_Col, p_SetModifiedStatus);
			const auto& field = ruleCompRow->GetField(p_Col);
			const bool hasField = !field.IsGeneric();
			if (hasField)
			{
				if (field.HasValue())
				{
					ruleCompRow->AddField(ruleCompRow->GetField(p_Col), m_ColComponentValue->GetID());
				}
				else
				{
					auto& newField = ruleCompRow->AddGenericFieldCopy(m_ColComponentValue->GetID());
					if (field.IsModified())
						newField.SetModified();
					else
						newField.RemoveModified();
				}
			}
			else
			{
				ruleCompRow->RemoveField(m_ColComponentValue);
			}
			ruleCompRow->SetTechHidden(m_HideUnsetComponents && !hasField);
			p_Updater.GetOptions().AddRowWithRefreshedValues(ruleCompRow);
		}
	}
}

void GridMessageRules::addRuleCompRow(const wstring& p_CompType, const boost::YOpt<bool>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_Col, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater)
{
	//if (p_val)
	{
		auto ruleCompRow = addRuleCompRow(p_CompType, p_RuleComp, p_ParentRow, p_SetModifiedStatus, p_Updater);
		ASSERT(nullptr != ruleCompRow);
		if (nullptr != ruleCompRow)
		{
			updateField(p_val, ruleCompRow, p_Col, p_SetModifiedStatus);
			const auto& field = ruleCompRow->GetField(p_Col);
			const bool hasField = !field.IsGeneric();
			if (hasField)
			{
				if (field.HasValue())
				{
					ruleCompRow->AddField(ruleCompRow->GetField(p_Col), m_ColComponentValue->GetID());
				}
				else
				{
					auto& newField = ruleCompRow->AddGenericFieldCopy(m_ColComponentValue->GetID());
					if (field.IsModified())
						newField.SetModified();
					else
						newField.RemoveModified();
				}
			}
			else
			{
				ruleCompRow->RemoveField(m_ColComponentValue);
			}
			ruleCompRow->SetTechHidden(m_HideUnsetComponents && !hasField);
			p_Updater.GetOptions().AddRowWithRefreshedValues(ruleCompRow);
		}
	}
}

void GridMessageRules::addRuleCompRow(const wstring& p_CompType, const boost::YOpt<vector<PooledString>>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_Col, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater)
{
	//if (p_val)
	{
		auto ruleCompRow = addRuleCompRow(p_CompType, p_RuleComp, p_ParentRow, p_SetModifiedStatus, p_Updater);
		ASSERT(nullptr != ruleCompRow);
		if (nullptr != ruleCompRow)
		{
			// FIXME:
			updateField(p_val, ruleCompRow, p_Col, p_SetModifiedStatus);
			ruleCompRow->AddField(p_val, p_Col);

			const auto& field = ruleCompRow->GetField(p_Col);
			const bool hasField = !field.IsGeneric();
			if (hasField)
			{
				if (field.HasValue())
				{
					ruleCompRow->AddField(ruleCompRow->GetField(p_Col), m_ColComponentValue->GetID());
				}
				else
				{
					auto& newField = ruleCompRow->AddGenericFieldCopy(m_ColComponentValue->GetID());
					if (field.IsModified())
						newField.SetModified();
					else
						newField.RemoveModified();
				}
			}
			else
			{
				ruleCompRow->RemoveField(m_ColComponentValue);
			}

			ruleCompRow->SetTechHidden(m_HideUnsetComponents && !hasField);
			p_Updater.GetOptions().AddRowWithRefreshedValues(ruleCompRow);
		}
	}
}

void GridMessageRules::addRuleCompRow(const wstring& p_CompType, const boost::YOpt<vector<Recipient>>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_ColAddress, GridBackendColumn* p_ColName, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater)
{
	//if (p_val)
	{
		auto ruleCompRow = addRuleCompRow(p_CompType, p_RuleComp, p_ParentRow, p_SetModifiedStatus, p_Updater);
		ASSERT(nullptr != ruleCompRow);
		if (nullptr != ruleCompRow)
		{
			vector<wstring> strings;
			if (p_val)
				strings = GridHelpers::ExtractEmailAddressesToString(*p_val);
			// FIXME:
			// updateField(p_val, ruleCompRow, m_ColComponentValue, p_SetModifiedStatus);
			ruleCompRow->AddField(strings, m_ColComponentValue);

			const bool hasField = !ruleCompRow->GetField(m_ColComponentValue).IsGeneric();
			if (hasField)
			{
				ASSERT(p_val);

				std::pair<vector<wstring>, vector<wstring>> values;
				values = GridHelpers::ExtractEmailAddresses(*p_val);
				ruleCompRow->AddField(values.first, p_ColAddress);
				ruleCompRow->AddField(values.second, p_ColName);
			}
			else
			{
				ruleCompRow->RemoveField(p_ColAddress);
				ruleCompRow->RemoveField(p_ColName);
			}

			ruleCompRow->SetTechHidden(m_HideUnsetComponents && !hasField);
			p_Updater.GetOptions().AddRowWithRefreshedValues(ruleCompRow);
		}
	}
}

void GridMessageRules::addRuleCompRow(const wstring& p_CompType, const boost::YOpt<SizeRange>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_ColMin, GridBackendColumn* p_ColMax, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater)
{
	//if (p_val)
	{
		auto ruleCompRow = addRuleCompRow(p_CompType, p_RuleComp, p_ParentRow, p_SetModifiedStatus, p_Updater);
		ASSERT(nullptr != ruleCompRow);
		if (nullptr != ruleCompRow)
		{
			// FIXME:
			// updateField(p_val, ruleCompRow, m_ColComponentValue, p_SetModifiedStatus);
			if (p_val)
				ruleCompRow->AddField(p_val->ToString(), m_ColComponentValue);

			const bool hasField = !ruleCompRow->GetField(m_ColComponentValue).IsGeneric();
			if (hasField)
			{
				ASSERT(p_val);

				if (p_val && p_val->m_MinimumSize)
					ruleCompRow->AddField(*p_val->m_MinimumSize * 1024, p_ColMin); // Converted to Bytes

				if (p_val &&  & p_val->m_MaximumSize)
					ruleCompRow->AddField(*p_val->m_MaximumSize * 1024, p_ColMax); // Converted to Bytes
			}
			else
			{
				ruleCompRow->RemoveField(p_ColMin); // Converted to Bytes
				ruleCompRow->RemoveField(p_ColMax); // Converted to Bytes
			}

			ruleCompRow->SetTechHidden(m_HideUnsetComponents && !hasField);

			p_Updater.GetOptions().AddRowWithRefreshedValues(ruleCompRow);
		}
	}
}

void GridMessageRules::addRuleCompRow(const wstring& p_CompType, const boost::YOpt<PooledString>& p_valId, const boost::YOpt<PooledString>& p_valName, bool p_ValNameIsError, const PooledString& p_RuleComp, GridBackendColumn* p_ColId, GridBackendColumn* p_ColName, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater)
{
	//if (p_valId)
	{
		auto ruleCompRow = addRuleCompRow(p_CompType, p_RuleComp, p_ParentRow, p_SetModifiedStatus, p_Updater);
		ASSERT(nullptr != ruleCompRow);
		if (nullptr != ruleCompRow)
		{
			// FIXME:
			// updateField(p_val, ruleCompRow, m_ColComponentValue, p_SetModifiedStatus);
			if (p_valName)
				ruleCompRow->AddField(p_valName, m_ColComponentValue);
			else
				ruleCompRow->AddField(p_valId, m_ColComponentValue);

			const bool hasField = !ruleCompRow->GetField(m_ColComponentValue).IsGeneric();
			if (hasField)
			{
				ruleCompRow->AddField(p_valId, p_ColId);
				auto& field = ruleCompRow->AddField(p_valName, p_ColName);
				if (!field.IsGeneric())
					field.SetIcon(p_ValNameIsError ? GridBackendUtil::ICON_ERROR : NO_ICON);
				else
					ASSERT(!p_ValNameIsError);
			}
			else
			{
				ruleCompRow->RemoveField(p_ColId);
				ruleCompRow->RemoveField(p_ColName);
			}

			ruleCompRow->SetTechHidden(m_HideUnsetComponents && !hasField);

			p_Updater.GetOptions().AddRowWithRefreshedValues(ruleCompRow);
		}
	}
}

GridBackendRow* GridMessageRules::addRuleCompRow(const wstring& p_CompType, const PooledString& p_RuleComp, GridBackendRow* p_ParentRow, bool p_SetModifiedStatus, GridUpdater& p_Updater)
{
	row_pk_t pk;
	GetRowPK(p_ParentRow, pk);
	pk.emplace_back(p_RuleComp, m_ColRuleComponent);
	pk.emplace_back(p_CompType, m_ColComponentType);

	auto ruleCompRow = GetRowIndex().GetRow(pk, true, !p_SetModifiedStatus);

	if (!p_SetModifiedStatus)
		p_Updater.AddUpdatedRowPk(pk);

	ruleCompRow->SetParentRow(p_ParentRow);

	// Copy parent fields
	for (auto c : m_MetaColumns)
	{
		if (p_ParentRow->HasField(c))
			ruleCompRow->AddField(p_ParentRow->GetField(c));
		else
			ruleCompRow->RemoveField(c);
	}

	if (p_ParentRow->HasField(m_ColRuleName))
		ruleCompRow->AddField(p_ParentRow->GetField(m_ColRuleName));
	else
		ruleCompRow->RemoveField(m_ColRuleName);

	if (p_ParentRow->HasField(m_ColSequence))
		ruleCompRow->AddField(p_ParentRow->GetField(m_ColSequence));
	else
		ruleCompRow->RemoveField(m_ColSequence);

	if (p_ParentRow->HasField(m_ColEnabled))
		ruleCompRow->AddField(p_ParentRow->GetField(m_ColEnabled));
	else
		ruleCompRow->RemoveField(m_ColEnabled);

	if (p_ParentRow->HasField(m_ColHasError))
		ruleCompRow->AddField(p_ParentRow->GetField(m_ColHasError));
	else
		ruleCompRow->RemoveField(m_ColHasError);

	if (p_ParentRow->HasField(m_ColReadOnly))
		ruleCompRow->AddField(p_ParentRow->GetField(m_ColReadOnly));
	else
		ruleCompRow->RemoveField(m_ColReadOnly);
	////

	ASSERT(ruleCompRow->GetField(m_ColComponentType).GetValueStr() == p_CompType);
	ruleCompRow->GetField(m_ColComponentType).SetIcon(getComponentTypeIcon(p_CompType));

	SetRowObjectType(ruleCompRow, BusinessMessageRuleComponent(), false);

	return ruleCompRow;
}

int GridMessageRules::getComponentTypeIcon(const wstring& p_CompType) const
{
	if (g_ComponentTypeAction == p_CompType)
		return m_ComponentTypeActionIconId;
	if (g_ComponentTypeCondition == p_CompType)
		return m_ComponentTypeConditionIconId;
	if (g_ComponentTypeException == p_CompType)
		return m_ComponentTypeExceptionIconId;
	ASSERT(false);
	return NO_ICON;
}

bool GridMessageRules::extractGridValue(const wstring& p_CompType, boost::YOpt<PooledString>& p_val, const PooledString& p_RuleComp, GridBackendRow* p_ParentRow) const
{
	auto field = extractComponentValueField(p_CompType, p_RuleComp, p_ParentRow).second;
	if (nullptr != field)
	{
		if (field->HasValue())
			p_val = field->GetValueStr();
		else
			p_val = boost::none;
	}
	return p_val.is_initialized();
}

bool GridMessageRules::extractGridValue(const wstring& p_CompType, boost::YOpt<bool>& p_val, const PooledString& p_RuleComp, GridBackendRow* p_ParentRow) const
{
	auto field = extractComponentValueField(p_CompType, p_RuleComp, p_ParentRow).second;
	if (nullptr != field)
	{
		if (field->HasValue())
			p_val = field->GetValueBool();
		else
			p_val = boost::none;
	}
	return p_val.is_initialized();
}

bool GridMessageRules::extractGridValue(const wstring& p_CompType, boost::YOpt<vector<PooledString>>& p_val, const PooledString& p_RuleComp, GridBackendRow* p_ParentRow) const
{
	auto field = extractComponentValueField(p_CompType, p_RuleComp, p_ParentRow).second;
	if (nullptr != field)
	{
		if (field->HasValue())
		{
			if (field->IsMulti() && nullptr != field->GetValuesMulti<PooledString>())
				p_val = *field->GetValuesMulti<PooledString>();
			else
				p_val = vector<PooledString>{ field->GetValueStr() };
		}
		else
			p_val = boost::none;
	}
	return p_val.is_initialized();
}

bool GridMessageRules::extractGridValue(const wstring& p_CompType, boost::YOpt<vector<Recipient>>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_ColAddresses, GridBackendColumn* p_ColNames, GridBackendRow* p_ParentRow) const
{
	auto res = extractComponentValueField(p_CompType, p_RuleComp, p_ParentRow);
	if (nullptr != res.second)
	{
		if (res.second->HasValue())
			p_val = GridHelpers::ExtractRecipients(res.first, p_ColAddresses, p_ColNames);
		else
			p_val = boost::none;
	}
	return p_val.is_initialized();
}

bool GridMessageRules::extractGridValue(const wstring& p_CompType, boost::YOpt<SizeRange>& p_val, const PooledString& p_RuleComp, GridBackendColumn* p_ColMin, GridBackendColumn* p_ColMax, GridBackendRow* p_ParentRow) const
{
	auto res = extractComponentValueField(p_CompType, p_RuleComp, p_ParentRow);
	if (nullptr != res.second)
	{
		if (res.second->HasValue())
			p_val = GridHelpers::ExtractSizeRange(res.first, p_ColMin, p_ColMax);
		else
			p_val = boost::none;
	}
	return p_val.is_initialized();
}

void GridMessageRules::extractGridValue(const std::set<UINT>& p_ColumnFilter, boost::YOpt<PooledString>& p_val, GridBackendColumn* p_Col, GridBackendRow* p_Row) const
{
	auto& field = p_Row->GetField(p_Col);
	if (field.HasValue() && isAnyColumnWanted(p_ColumnFilter, { p_Col }))
		p_val = field.GetValueStr();
}

void GridMessageRules::extractGridValue(const std::set<UINT>& p_ColumnFilter, boost::YOpt<bool>& p_val, GridBackendColumn* p_Col, GridBackendRow* p_Row) const
{
	auto& field = p_Row->GetField(p_Col);
	if (field.HasValue() && isAnyColumnWanted(p_ColumnFilter, { p_Col }))
		p_val = field.GetValueBool();
}

void GridMessageRules::extractGridValue(const std::set<UINT>& p_ColumnFilter, boost::YOpt<int32_t>& p_val, GridBackendColumn* p_Col, GridBackendRow* p_Row) const
{
	auto& field = p_Row->GetField(p_Col);
	if (field.HasValue() && isAnyColumnWanted(p_ColumnFilter, { p_Col }))
		p_val = static_cast<int32_t>(field.GetValueLong());
}

std::pair<GridBackendRow*, GridBackendField*> GridMessageRules::extractComponentValueField(const wstring& p_CompType, const PooledString& p_RuleComp, GridBackendRow* p_ParentRow) const
{
	std::pair<GridBackendRow*, GridBackendField*> result(nullptr, nullptr);
	result.first = GetChildRow(p_ParentRow, { { m_ColComponentType->GetID(), p_CompType } ,{ m_ColRuleComponent->GetID(), p_RuleComp } });
	ASSERT(nullptr != result.first);
	if (nullptr != result.first)
		result.second = &result.first->GetField(m_ColComponentValue);
	return result;
}

void GridMessageRules::techHideUnsetComponents(bool p_TechHide, bool p_UpdateGrid)
{
	for (auto& row : GetBackendRows())
	{
		if (GridUtil::isBusinessType<BusinessMessageRuleComponent>(row))
			row->SetTechHidden(p_TechHide && row->GetField(m_ColComponentValue).IsGeneric());
	}

	if (p_UpdateGrid)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

bool GridMessageRules::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return ModuleO365Grid<BusinessMessageRule>::IsRowValidForNoFieldText(p_Row, p_Col)
		&& (!p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore)
			|| m_MetaIDsMoreLoaded.end() == m_MetaIDsMoreLoaded.find(p_Row->GetField(GetColMetaId()).GetValueStr()));
}

void GridMessageRules::InitializeCommands()
{
	ModuleO365Grid<BusinessMessageRule>::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_MESSAGERULESGRID_SHOWUNSETCOMPONENTS;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridMessageRules_InitializeCommands_1, _YLOC("Show Unused Components")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridMessageRules_InitializeCommands_2, _YLOC("Show Unused Components")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGERULESGRID_SHOWUNSETCOMPONENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_MESSAGERULESGRID_SHOWUNSETCOMPONENTS, hIcon, false);
		setRibbonTooltip(_cmd.m_nCmdID, _T("Show Unset Mail Rule Components"), YtriaTranslate::Do(GridMessageRules_InitializeCommands_4, _YLOC("Display all unused mail rule components - conditions, actions, and exceptions that are not used in the rule.\n(By default these unused properties are not shown.)")).c_str(), _cmd.m_sAccelText);
	}

#if HAS_MESSAGE_RULES_EDIT
	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_MESSAGERULESGRID_MOVEUPINSEQUENCE;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridMessageRules_InitializeCommands_5, _YLOC("Move Up in Execution Order")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridMessageRules_InitializeCommands_5, _YLOC("Move Up in Execution Order")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		/*HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LICENSESGRID_SHOWUNASSIGNED_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_LICENSESGRID_SHOWUNASSIGNED, hIcon, false);*/
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridMessageRules_InitializeCommands_5, _YLOC("Move Up in Execution Order")).c_str(), YtriaTranslate::Do(GridMessageRules_InitializeCommands_8, _YLOC("Decrement the Sequence number so that the rule is executed earlier.")).c_str(), _cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_MESSAGERULESGRID_MOVEDOWNINSEQUENCE;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridMessageRules_InitializeCommands_9, _YLOC("Move Down in Execution Order")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridMessageRules_InitializeCommands_10, _YLOC("Move Down in Execution Order")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);
		/*HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LICENSESGRID_SHOWUNASSIGNED_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_LICENSESGRID_SHOWUNASSIGNED, hIcon, false);*/
		setRibbonTooltip(_cmd.m_nCmdID, YtriaTranslate::Do(GridMessageRules_InitializeCommands_10, _YLOC("Move Down in Execution Order")).c_str(), YtriaTranslate::Do(GridMessageRules_InitializeCommands_12, _YLOC("Increment the Sequence number so that the rule is executed later.")).c_str(), _cmd.m_sAccelText);
	}
#endif
}

#if HAS_MESSAGE_RULES_EDIT
INT_PTR GridMessageRules::showDialog(vector<BusinessMessageRule>& p_Objects, const std::set<wstring>& /*p_ListFieldErrors*/, DlgFormsHTML::Action p_Action, CWnd* p_Parent)
{
	ASSERT(DlgFormsHTML::Action::EDIT == p_Action);

	if (Office365AdminApp::ShowFeatureUnfinishedDialog(this))
	{
		// Decide which category to expand if any
		wstring commonCompType;
		for (auto& row : GetSelectedRows())
		{
			const wstring compType = row->GetField(m_ColComponentType).GetValueStr();
			if (!commonCompType.empty() && compType != commonCompType)
			{
				commonCompType.clear();
				break;
			}
			commonCompType = compType;
		}

		DlgBusinessMessageRuleEditHTML dlg(p_Objects, p_Action, commonCompType, p_Parent);
		auto ret = dlg.DoModal();

		/*if (IDOK == ret)
		{
			O365SubSubIdsContainer folderNamesByUser;
			for (auto& rule : p_Objects)
			{
				if (rule.m_Actions.m_CopyToFolderName && !rule.m_Actions.m_CopyToFolderID)
					folderNamesByUser[rule.m_UserId].insert(*rule.m_Actions.m_CopyToFolderName);
			}
		}*/

		return ret;
	}

	return IDCANCEL;
}

#endif

bool GridMessageRules::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	if (p_Field.HasValue())
	{
		if (&p_Column == m_ColComponentType)
		{
			if (g_ComponentTypeCondition == p_Field.GetValueStr())
				p_Value = _YTEXT("1");
			else if (g_ComponentTypeAction == p_Field.GetValueStr())
				p_Value = _YTEXT("2");
			else if (g_ComponentTypeException == p_Field.GetValueStr())
				p_Value = _YTEXT("3");
			else
				ASSERT(false);

			return true;
		}
	}

	return  m_Template.GetSnapshotValue(p_Value, p_Field, p_Column)
		|| ModuleO365Grid<BusinessMessageRule>::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridMessageRules::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	if (&p_Column == m_ColComponentType)
	{
		const auto& compType
			= p_Value == _YTEXT("1") ? g_ComponentTypeCondition
			: p_Value == _YTEXT("2") ? g_ComponentTypeAction
			: p_Value == _YTEXT("3") ? g_ComponentTypeException
			: Str::g_EmptyString;

		ASSERT(!compType.empty());
		if (!compType.empty())
			p_Row.AddField(compType, m_ColComponentType, getComponentTypeIcon(compType));

		return true;
	}
	else if (&p_Column == m_ColEnabled)
	{
		boost::YOpt<bool> val;
		if (p_Value == GridSnapshot::Constants::g_True)
			val = true;
		else if (p_Value == GridSnapshot::Constants::g_False)
			val = false;
		ASSERT(val);
		p_Row.AddField(val, m_ColEnabled, val && *val ? m_EnabledRuleIconId : val ? m_DisabledRuleIconId : NO_ICON);

		return true;
	}
	else if (&p_Column == m_ColComponentValue)
	{
		p_Row.SetTechHidden(false);
	}

	const auto res = m_Template.LoadSnapshotValue(p_Value, p_Row, p_Column)
		|| ModuleO365Grid<BusinessMessageRule>::LoadSnapshotValue(p_Value, p_Row, p_Column);

	if (res && &p_Column == GetColumnObjectType())
	{
		if (GridUtil::isBusinessType<BusinessMessageRuleComponent>(&p_Row))
			p_Row.SetTechHidden(m_HideUnsetComponents && p_Row.GetField(m_ColComponentValue).IsGeneric());
	}

	return res;
}

template<typename T>
GridBackendField* GridMessageRules::updateField(const T& p_Value, GridBackendRow* p_Row, GridBackendColumn* p_Col, bool p_StoreModification)
{
	GridBackendField* newField{ nullptr };
	if (nullptr != p_Col)
	{
		ASSERT(nullptr != p_Row);

		const auto colID = p_Col->GetID();
		vector<GridBackendField> rowPK;
		if (p_StoreModification)
			GetRowPK(p_Row, rowPK);

		const bool hadField = p_Row->HasField(colID);
		const GridBackendField oldField = p_StoreModification ? p_Row->GetField(colID) : GridBackendField();

		bool processAddField = true;
		if (p_StoreModification)
		{
			rttr::variant variantValue = p_Value;
			// FIXME: Add all opt types
			if (variantValue.is_type<boost::YOpt<PooledString>>())
			{
				if (!variantValue.get_value<boost::YOpt<PooledString>>())
				{
					if (p_Row->GetField(p_Col).HasValue())
					{
						newField = &p_Row->AddGenericFieldCopy(p_Col->GetID());
						processAddField = false;
					}
				}
			}
			else if (variantValue.is_type<boost::YOpt<bool>>())
			{
				if (!variantValue.get_value<boost::YOpt<bool>>())
				{
					if (p_Row->GetField(p_Col).HasValue())
					{
						newField = &p_Row->AddGenericFieldCopy(p_Col->GetID());
						processAddField = false;
					}
				}
			}
			else if (variantValue.is_type<boost::YOpt<vector<PooledString>>>())
			{
				if (!variantValue.get_value<boost::YOpt<vector<PooledString>>>())
				{
					if (p_Row->GetField(p_Col).HasValue())
					{
						newField = &p_Row->AddGenericFieldCopy(p_Col->GetID());
						processAddField = false;
					}
				}
			}
		}

		if (processAddField)
			newField = &GridUtil::AddFieldImpl(p_Row, p_Value, p_Col);

		ASSERT(p_Row->HasField(GetColId()));
		if (p_Row->HasField(GetColId()))
		{
			if (p_StoreModification)
			{
				ASSERT(nullptr != newField);
				if (nullptr != newField && newField->IsModified() && (hadField || !hadField && !newField->IsGeneric()))
				{
					GetModifications().Add(std::make_unique<FieldUpdateO365>(*this, rowPK, p_Col->GetID(), hadField ? oldField : GridBackendField::g_GenericConstField, *newField));
					// Show column containing modification if hidden
					/*{
						auto column = GetColumnByID(newField->GetColID());
						ASSERT(nullptr != column);
						if (nullptr != column && !column->IsVisible())
							GetBackend().SetColumnVisible(column, true, true, false);
					}*/
				}
			}
		}
	}

	return newField;
}

bool GridMessageRules::isAnyColumnWanted(const std::set<UINT>& p_ColumnFilter, const std::vector<GridBackendColumn*>&& p_Columns)
{
	return p_ColumnFilter.empty() || std::any_of(p_Columns.begin(), p_Columns.end(), [&p_ColumnFilter](GridBackendColumn* p_Col)
	{
		return p_ColumnFilter.end() != p_ColumnFilter.find(p_Col->GetID());
	});		
}

RoleDelegationUtil::RBAC_Privilege GridMessageRules::GetPrivilegeEdit() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MESSAGERULES_EDIT;
}

RoleDelegationUtil::RBAC_Privilege GridMessageRules::GetPrivilegeCreate() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MESSAGERULES_CREATE;
}

RoleDelegationUtil::RBAC_Privilege GridMessageRules::GetPrivilegeDelete() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_USER_MESSAGERULES_DELETE;
}