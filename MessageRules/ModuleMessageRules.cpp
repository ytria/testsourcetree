#include "ModuleMessageRules.h"

#include "AutomationDataStructure.h"
#include "BusinessMailFolder.h"
#include "BusinessObjectManager.h"
#include "FrameMessageRules.h"
#include "GraphCache.h"
#include "MessageRuleDeserializer.h"
#include "MessageRulesRequester.h"
#include "MessageRuleUpdater.h"
#include "MSGraphCommonData.h"
#include "MSGraphUtil.h"
#include "Office365Admin.h"
#include "RefreshSpecificData.h"
#include "RESTUtils.h"
#include "safeTaskCall.h"
#include "SingleRequestResult.h"
#include "TaskDataManager.h"
#include "YCallbackMessage.h"
#include "YDataCallbackMessage.h"
#include "MultiObjectsRequestLogger.h"
#include "YSafeCreateTask.h"

using namespace Rest;

void ModuleMessageRules::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
		case Command::ModuleTask::ListMe:
			ASSERT(p_Command.GetCommandInfo().GetIds().empty());
		case Command::ModuleTask::List:
			showMessageRules(p_Command);
			break;
		case Command::ModuleTask::UpdateRefresh:
			refresh(p_Command);
			break;
		case Command::ModuleTask::ApplyChanges:
			applyChanges(p_Command);
			break;
		// FIXME: Finish this eventually
		/*case Command::ModuleTask::RetrieveFoldersIDs:
			retrieveFolderIDs(p_Command);
			break;*/
		case Command::ModuleTask::LoadSnapshot:
			loadSnapshot(p_Command);
			break;
		default:
			ASSERT(false);
			SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
			break;
	}
}

void ModuleMessageRules::showMessageRules(const Command& p_Command)
{
	auto p_Action = p_Command.GetAutomationAction();

    const auto& info = p_Command.GetCommandInfo();
    const auto p_Origin = info.GetOrigin();

    ASSERT(p_Origin != Origin::NotSet);
    if (p_Origin != Origin::NotSet)
    {
        const auto&	p_IDs = info.GetIds();
        auto p_SourceWindow = p_Command.GetSourceWindow();

        auto sourceCWnd = p_SourceWindow.GetCWnd();
        if (FrameMessageRules::CanCreateNewFrame(true, sourceCWnd, p_Action))
        {
            ModuleCriteria moduleCriteria;
            moduleCriteria.m_Origin				= p_Origin;
            moduleCriteria.m_UsedContainer		= ModuleCriteria::UsedContainer::IDS;
            moduleCriteria.m_IDs				= p_IDs;
			moduleCriteria.m_Privilege			= info.GetRBACPrivilege();
			moduleCriteria.m_MetaDataColumnInfo = info.GetMetaDataColumnInfo();

            if (moduleCriteria.m_IDs.empty())
            {
                auto& id = GetConnectedSession().GetConnectedUserId();
                ASSERT(!id.empty());
                if (!id.empty())
                    moduleCriteria.m_IDs.insert(id);
            }

            if (ShouldCreateFrame<FrameMessageRules>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
            {
                CWaitCursor _;

				const wstring title = p_Command.GetTask() == Command::ModuleTask::ListMe
					? _T("My Message Rules")
					: YtriaTranslate::Do(ModuleMessageRules_showMessageRules_1, _YLOC("Message Rules")).c_str();
				auto newFrame = new FrameMessageRules(p_Command.GetTask() == Command::ModuleTask::ListMe, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), title, *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
                newFrame->InitModuleCriteria(moduleCriteria);
                AddGridFrame(newFrame);
                newFrame->Erect();

                auto taskData = addFrameTask(YtriaTranslate::Do(ModuleMessageRules_showMessageRules_2, _YLOC("Show Message Rules")).c_str(), newFrame, p_Command, true);
                doRefresh(newFrame, p_Action, taskData, false, RefreshSpecificData());
            }
        }
        else
        {
            SetAutomationGreenLight(p_Action, _YTEXT(""));
        }
    }
    else
    {
        SetAutomationGreenLight(p_Action, p_Command.ToString() + YtriaTranslate::DoError(ModuleContact_showContacts_3, _YLOC(" - origin not set"),_YR("Y2459")).c_str());
    }
}

void ModuleMessageRules::refresh(const Command& p_Command)
{
	AutomationAction*			p_Action	= p_Command.GetAutomationAction();

    auto existingFrame = dynamic_cast<FrameMessageRules*>(p_Command.GetCommandInfo().GetFrame());
    ASSERT(nullptr != existingFrame);
    if (nullptr != existingFrame)
    {
        auto taskData = addFrameTask(YtriaTranslate::Do(ModuleMessageRules_refresh_1, _YLOC("Show Message Rules")).c_str(), existingFrame, p_Command, false);

        RefreshSpecificData refreshSelectedData;
        if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>())
            refreshSelectedData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

        doRefresh(existingFrame, p_Action, taskData, true, refreshSelectedData);
    }
    else
        SetAutomationGreenLight(p_Action, _YTEXT(""));
}

void ModuleMessageRules::applyChanges(const Command& p_Command)
{
	CommandInfo info = p_Command.GetCommandInfo();
	AutomationAction* action = p_Command.GetAutomationAction();
	auto frame = dynamic_cast<FrameMessageRules*>(info.GetFrame());
	const auto& ids = info.GetIds();

	ASSERT(nullptr != frame);
	if (nullptr != frame)
	{
		auto grid = dynamic_cast<GridMessageRules*>(&frame->GetGrid());
		ASSERT(nullptr != grid);

		if (nullptr != grid)
		{
			auto rulesToDelete = grid->GetDeletedRules(ids);
			GridMessageRules::ModifiedRules rulesToUpdate
#if HAS_MESSAGE_RULES_EDIT
				= grid->GetModifiedRules(ids);
#else
				= {};
#endif

			auto taskData = addFrameTask(YtriaTranslate::Do(ModuleMessageRules_applyChanges_1, _YLOC("Applying Message Rules changes")).c_str(), frame, p_Command, false);

			YSafeCreateTask([bom = frame->GetBusinessObjectManager(), p_Command, rulesToDelete, rulesToUpdate, hwnd = frame->m_hWnd, taskData, action]()
			{
				vector<std::pair<MessageRuleUpdater, row_pk_t>> updaters;
				O365DataMap<PooledString, vector<BusinessMessageRule>> updatedRules;
#if HAS_MESSAGE_RULES_EDIT
				for (const auto& byUser : rulesToUpdate)
				{
					for (const auto& rule : byUser.second)
					{
						updaters.emplace_back(MessageRuleUpdater{ byUser.first, rule.first }, rule.second);
						updaters.back().first.Send(bom->GetSapio365Session(), taskData).Then([&updater = updaters.back().first, &updatedRulesForUser = updatedRules[byUser.first]]()
						{
							MessageRuleDeserializer mrd;
							const auto status = updater.GetResult().GetResult().Response.status_code();
							if (status < 200 || 300 <= status) // Is this an error?
							{
								updatedRulesForUser.emplace_back();
								updatedRulesForUser.back().m_Id = updater.GetRule().m_Id;
								updatedRulesForUser.back().SetError(HttpResultWithError(updater.GetResult().GetResult()).m_Error);
							}
							else
							{
								auto strResponse = updater.GetResult().GetResult().GetBodyAsString();
								if (strResponse)
								{
									mrd.Deserialize(json::value::parse(*strResponse));
									if (!mrd.GetData().m_Id.IsEmpty())
									{
										updatedRulesForUser.emplace_back(std::move(mrd.GetData()));
									}
									else
									{
										ASSERT(false);
									}
								}
							}
						}).GetTask().wait();
					}
				}
#else
				ASSERT(rulesToUpdate.empty());
#endif
				for (const auto& byUser : rulesToDelete)
				{
					for (const auto& rule : byUser.second)
					{
						updaters.emplace_back(MessageRuleDeleter{ byUser.first, rule.first }, rule.second);
						updaters.back().first.Send(bom->GetSapio365Session(), taskData).GetTask().wait();
					}
				}

				YCallbackMessage::DoPost([hwnd, updaters, updatedRules,	action, taskData]()
				{
					if (::IsWindow(hwnd))
					{
						auto frame = dynamic_cast<FrameMessageRules*>(CWnd::FromHandle(hwnd));
						ASSERT(nullptr != frame);
						if (nullptr != frame)
						{
							vector<O365UpdateOperation> updateOperations;
							RefreshSpecificData refreshData;
							refreshData.m_Criteria = frame->GetModuleCriteria();
							ASSERT(refreshData.m_Criteria.m_UsedContainer == ModuleCriteria::UsedContainer::IDS);
							refreshData.m_Criteria.m_IDs.clear();

							for (auto& updater : updaters)
							{
								refreshData.m_Criteria.m_IDs.insert(updater.first.GetUserId());
								updateOperations.emplace_back(updater.second);
								updateOperations.back().StoreResult(boost::YOpt<RestResultInfo>{ updater.first.GetResult().GetResult() });
							}

							frame->GetGrid().ClearLog(taskData.GetId());

#if HAS_MESSAGE_RULES_EDIT
							frame->ShowMessageRulesAfterUpdate(updatedRules, vector<O365UpdateOperation>{ updateOperations });

#else
							ASSERT(updatedRules.empty());
							frame->ShowMessageRulesAfterUpdate({}, vector<O365UpdateOperation>{ updateOperations });
#endif
							//frame->RefreshAfterUpdate(vector<O365UpdateOperation>{ updateOperations }, refreshData, action);
						}
						//else
						{
							SetAutomationGreenLight(action);
						}						
					}
					else
					{
						SetAutomationGreenLight(action);
					}

					TaskDataManager::Get().RemoveTaskData(taskData.GetId(), hwnd);
				});
			});
		}
	}
	else
	{
		// ?
	}
}

void ModuleMessageRules::loadSnapshot(const Command& p_Command)
{
	LoadSnapshot<FrameMessageRules>(p_Command, [&p_Command](const GridSnapshot::Metadata& p_SnapshotMeta, const ModuleCriteria& p_ModuleCriteria, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
		{
			const bool isMyData = p_SnapshotMeta.m_Flags && GridSnapshot::Flags::MyData == (GridSnapshot::Flags::MyData & *p_SnapshotMeta.m_Flags);
			const wstring title = isMyData
				? _T("My Message Rules")
				: YtriaTranslate::Do(ModuleMessageRules_showMessageRules_1, _YLOC("Message Rules")).c_str();
			return new FrameMessageRules(isMyData, p_LicenseContext, p_SessionIdentifier, title, p_Module, p_HistoryMode, p_PreviousFrame);
		});
}

// FIXME: Finish this eventually
//void ModuleMessageRules::retrieveFolderIDs(const Command& p_Command)
//{
//	CommandInfo info = p_Command.GetCommandInfo();
//	AutomationAction* action = p_Command.GetAutomationAction();
//	FrameMessageRules* frame = dynamic_cast<FrameMessageRules*>(info.GetFrame());
//	const auto& folderNamesByUser = info.GetSubIds();
//
//	ASSERT(nullptr != frame);
//	if (nullptr != frame)
//	{
// //		auto taskData = addFrameTask( _YCOM("Getting Mail Folders"), frame, p_Command, false);
//		pplx::create_task([bom = frame->GetBusinessObjectManager(), folderNamesByUser, hwnd = frame->m_hWnd, taskData, action]()
//		{
//			std::map<PooledString, std::map<PooledString, PooledString>> folderNamesToIdsByUser;
//			for (auto& folderNames : folderNamesByUser)
//			{
//				MailFoldersHierarchyRequester mfhr(folderNames.first, PooledString(), false);
//				mfhr.Send(bom->GetSapio365Session(), taskData).wait();
//				auto& folders = mfhr.GetData();
//
//				// FIXME: Also loop on folder.m_ChildrenMailFolders (and etc.)
//				for (auto& folder : folders)
//				{
//					if (folder.m_DisplayName)
//					{
//						auto it = folderNames.second.find(*folder.m_DisplayName);
//						if (folderNames.second.end() != it)
//						{
//							folderNamesToIdsByUser[folderNames.first][*folder.m_DisplayName] = folder.GetID();
//						}
//					}
//				}
//			}
//
//			YCallbackMessage::DoPost([hwnd, taskData, action, folderNamesToIdsByUser]()
//			{
//				bool shouldFinishTask = true;
//				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameMessageRules*>(CWnd::FromHandle(hwnd)) : nullptr;
//				if (nullptr != frame)
//				{
//					frame->GetGrid().SetProcessText(CacheGrid::g_DefaultProcessText);
//					frame->GetGrid().ClearLog(taskData.GetId());
//					frame->SetNewFolderNames(folderNamesToIdsByUser);
//
//					// Because we didn't (and can't) call UpdateContext here.
//					ModuleBase::taskFinished(frame, taskData, action);
//				}
//			});
//
//		});
//	}
//}

void ModuleMessageRules::doRefresh(FrameMessageRules* p_pFrame, AutomationAction* p_Action, YtriaTaskData p_TaskData, bool p_IsUpdate, const RefreshSpecificData& p_RefreshData)
{
	YSafeCreateTask([p_TaskData, hwnd = p_pFrame->GetSafeHwnd(), currentModuleCriteria = p_pFrame->GetModuleCriteria(), p_Action, p_RefreshData, p_IsUpdate, bom = p_pFrame->GetBusinessObjectManager(), wantsAdditionalMetadata = (bool)p_pFrame->GetMetaDataColumnInfo()]()
	{
        const bool partialRefresh = !p_RefreshData.m_Criteria.m_IDs.empty();
		const auto& ids = partialRefresh ? p_RefreshData.m_Criteria.m_IDs : currentModuleCriteria.m_IDs;

		O365DataMap<BusinessUser, vector<BusinessMessageRule>> messageRules;

		auto logger = std::make_shared<MultiObjectsRequestLogger>(_T("message rules"), ids.size());

		auto processUser = [&messageRules, &bom, &p_TaskData, &logger](BusinessUser& user)
		{
			if (p_TaskData.IsCanceled())
			{
				user.SetFlags(user.GetFlags() | BusinessObject::Flag::CANCELED);
				messageRules[user] = {};
			}
			else
			{
				MessageRulesRequester requester(user.GetID(), logger);
				requester.Send(bom->GetSapio365Session(), p_TaskData).GetTask().wait();
				if (p_TaskData.IsCanceled())
				{
					user.SetFlags(user.GetFlags() | BusinessObject::Flag::CANCELED);
					messageRules[user] = {};
				}
				else
				{
					messageRules[user] = std::move(requester.GetData());
				}
			}
		};

		Util::ProcessSubUserItems(processUser, ids, currentModuleCriteria.m_Origin, bom->GetGraphCache(), wantsAdditionalMetadata, logger, p_TaskData);

		YDataCallbackMessage<O365DataMap<BusinessUser, vector<BusinessMessageRule>>>::DoPost(messageRules, [hwnd, partialRefresh, p_TaskData, p_Action, p_IsUpdate, isCanceled = p_TaskData.IsCanceled()](const O365DataMap<BusinessUser, vector<BusinessMessageRule>>& p_MessageRules)
		{
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameMessageRules*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ShouldBuildView(hwnd, isCanceled, p_TaskData, false))
			{
				ASSERT(nullptr != frame);
				frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting message rules for %s users...", Str::getStringFromNumber(p_MessageRules.size()).c_str()));
				frame->GetGrid().ClearLog(p_TaskData.GetId());

				{
					CWaitCursor _;
					if (frame->HasLastUpdateOperations())
					{
						frame->ShowMessageRules(p_MessageRules, frame->GetLastUpdateOperations(), partialRefresh);
						frame->ForgetLastUpdateOperations();
					}
					else
					{
						frame->ShowMessageRules(p_MessageRules, {}, partialRefresh);
					}
				}

				if (!p_IsUpdate)
				{
					frame->UpdateContext(p_TaskData, hwnd);
					shouldFinishTask = false;
				}
			}

			if (shouldFinishTask)
			{
				// Because we didn't (and can't) call UpdateContext here.
				ModuleBase::TaskFinished(frame, p_TaskData, p_Action);
			}
		});
    });
}
