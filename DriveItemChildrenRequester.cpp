#include "DriveItemChildrenRequester.h"

#include "DriveItemDeserializer.h"
#include "DriveItemsRequester.h"
#include "MSGraphCommonData.h"
#include "MsGraphHttpRequestLogger.h"
#include "MSGraphSession.h"
#include "MSGraphUtil.h"
#include "PaginatedRequestResults.h"
#include "PaginatorUtil.h"
#include "RESTUtils.h"
#include "RunOnScopeEnd.h"
#include "Sapio365Session.h"
#include "ValueListDeserializer.h"

using namespace Rest;

DriveItemChildrenRequester::DriveItemChildrenRequester(PooledString p_DriveId, const BusinessDriveItem& p_DriveItem, bool p_Recursive, const std::shared_ptr<IPageRequestLogger>& p_Logger)
    :m_DriveId(p_DriveId),
    m_DriveItem(p_DriveItem),
    m_Recursive(p_Recursive),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> DriveItemChildrenRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<BusinessDriveItem, DriveItemDeserializer>>();
    m_Result = std::make_shared<PaginatedRequestResults>();

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());

    const auto& folder = m_DriveItem.GetFolder();
    if (folder != boost::none && folder->ChildCount && *folder->ChildCount > 0
		|| m_DriveItem.IsNotebook())
    {
        if (m_Recursive)
        {
            return YSafeCreateTask([item = m_DriveItem, httpLogger, p_Session, deserializer = m_Deserializer, driveId = m_DriveId, driveItemId = m_DriveItem.GetID(), logger = m_Logger, p_TaskData ]() {

				web::uri_builder uri;
				uri.append_path(DRIVES_PATH);
				uri.append_path(driveId);
				uri.append_path(ITEMS_PATH);
				uri.append_path(driveItemId);
				uri.append_path(CHILDREN_PATH);
				uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(DriveItemsRequester::GetSelectProperties()));

				RunOnScopeEnd _([objName = logger->GetLogMessage(), logger]() { logger->SetLogMessage(objName); });
				logger->SetLogMessage(_YFORMAT(L"drive items in folder \"%s\"", item.GetName() ? item.GetName()->c_str() : item.GetID().c_str()));

				Util::CreateDefaultGraphPaginator(uri.to_uri(), deserializer, logger, httpLogger, p_TaskData.GetOriginator()).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData).GetTask().wait();
                for (auto& child : deserializer->GetData())
                {
                    DriveItemChildrenRequester childRequester(driveId, child, true, logger);
                    auto childResult = childRequester.Send(p_Session, p_TaskData).GetTask().wait();
                    if (p_TaskData.IsCanceled() || childResult == pplx::task_status::canceled)
                        pplx::cancel_current_task();

                    child.SetDriveItemChildren(childRequester.GetData());
                }
            });
        }
		else
		{

			web::uri_builder uri;
			uri.append_path(DRIVES_PATH);
			uri.append_path(m_DriveId);
			uri.append_path(ITEMS_PATH);
			uri.append_path(m_DriveItem.GetID());
			uri.append_path(CHILDREN_PATH);
			uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(DriveItemsRequester::GetSelectProperties()));

			RunOnScopeEnd _([objName = m_Logger->GetLogMessage(), logger = m_Logger]() { logger->SetLogMessage(objName); });
			m_Logger->SetLogMessage(_T("drive items children"));

			return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Deserializer, nullptr, httpLogger, p_TaskData.GetOriginator()).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
		}
    }
    else
        return pplx::task_from_result();
}

vector<BusinessDriveItem>& DriveItemChildrenRequester::GetData()
{
    return m_Deserializer->GetData();
}

const PaginatedRequestResults& DriveItemChildrenRequester::GetResult() const
{
    return *m_Result;
}
