#pragma once

#include "BusinessConfigurationBase.h"

#define O365_GROUP_ISTEAM				"isTeam"
#define O365_GROUP_GROUPTYPE			"groupType"
#define O365_GROUP_DYNAMICMEMBERSHIP	"dynamicMembership"

class BusinessGroupConfiguration : public BusinessConfigurationBase
{
public:
	DECLARE_GET_INSTANCE(BusinessGroupConfiguration)

	const wstring& GetValueStringLicenses_Unlicensed() const;

private:
	BusinessGroupConfiguration();

	static wstring g_Unlicensed;
};
