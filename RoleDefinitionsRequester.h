#pragma once

#include "IRequester.h"
#include "SpUtils.h"

#include "RoleDefinition.h"
#include "RoleDefinitionDeserializer.h"
#include "ValueListDeserializer.h"

class SingleRequestResult;

namespace Sp
{
    class RoleDefinitionsRequester : public IRequester
    {
    public:
        RoleDefinitionsRequester(PooledString p_SiteUrl);
        TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

        const vector<Sp::RoleDefinition>& GetData() const;

    private:
        PooledString m_SiteUrl;
        std::shared_ptr<ValueListDeserializer<Sp::RoleDefinition, Sp::RoleDefinitionDeserializer>> m_Deserializer;
        std::shared_ptr<SingleRequestResult> m_Result;
    };
}
