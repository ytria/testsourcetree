#include "MacroRequest.h"

#include "MSGraphCommonData.h"

MacroRequest::MacroRequest()
	: m_Pos(-1)
{

}

bool MacroRequest::isFlagSet(int p_Flag, int p_Value) const
{
	return (p_Flag & p_Value) == p_Flag;
}

void MacroRequest::AddStep(std::unique_ptr<IStep>&& p_Step)
{
	m_Steps.emplace_back(std::move(p_Step));
}

std::unique_ptr<MacroRequest::MacroRequest::IStep>& MacroRequest::CurrentStep()
{
	OutputDebugString(_YTEXTFORMAT(L"\nStep %s/%s\n\n", Str::getStringFromNumber(m_Pos + 1).c_str(), Str::getStringFromNumber(m_Steps.size()).c_str()).c_str());
	ASSERT(m_Pos < static_cast<__int3264>(m_Steps.size()));
	if (m_Pos < static_cast<__int3264>(m_Steps.size()))
		return m_Steps[m_Pos];
	static std::unique_ptr<IStep> nullPtr;
	return nullPtr;
}

bool MacroRequest::Next()
{
	return ++m_Pos < static_cast<__int3264>(m_Steps.size());
}

bool MacroRequest::Finished() const
{
	return m_Pos >= static_cast<__int3264>(m_Steps.size());
}

bool MacroRequest::Run()
{
	while (Next())
	{
		auto& step = CurrentStep();
		ASSERT(step);
		if (step)
			if (step->ShouldProcess())
				if (!step->Process()) // Cancelled
					return false;
	}

	return true;
}

MacroRequest::Step::Step(std::function<bool()> p_ShouldProcess, std::function<bool()> p_Process) : m_ShouldProcess(p_ShouldProcess)
, m_Process(p_Process)
{

}

bool MacroRequest::Step::ShouldProcess() const
{
	return m_ShouldProcess();
}

bool MacroRequest::Step::Process() const
{
	return m_Process();
}


