#pragma once

#include "ResizableDialog.h"
#include "Resource.h"
#include "Sapio365Session.h"
#include "YAdvancedHtmlView.h"
#include "OnOKOnCancelFixed.h"

class CosmosDbAccountInfo
{
public:
	wstring m_Name;
	bool m_HasDatabase = false;
	bool m_OnlyHasYtriaDatabase = false;
	wstring m_Info;
};

class DlgSelectCosmosDbAccount : public ResizableDialog, public YAdvancedHtmlView::IPostedDataTarget
{
public:
	enum class Mode
	{
		DELETION,
		SELECTION
	};

	DlgSelectCosmosDbAccount(Mode p_Mode, const vector<CosmosDbAccountInfo>& p_DbAccounts, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent, bool p_OnlyShowAccountsWithYtriaDbs = false);

	enum { IDD = IDD_DLG_DELETE_COSMOS_ACCOUNT };

	bool OnNewPostedData(const IPostedDataTarget::PostedData& data) override;

	const wstring& GetDbAccountName() const;

	using OnOkValidation = std::function<bool(DlgSelectCosmosDbAccount&)>;
	void SetOnOkValidation(OnOkValidation p_OnOkValid);

protected:
	BOOL OnInitDialogSpecificResizable() override;

	ENDDIALOG_FIXED(ResizableDialog);

private:
	void generateJSONScriptData(wstring& p_Output);
	void AddDynamicFields(web::json::object& p_Object);

	const Mode m_Mode;

	const vector<CosmosDbAccountInfo>& m_DbAccounts;
	wstring m_DbAccountName;

	bool m_OnlyShowAccountsWithYtriaDbs;

	OnOkValidation m_OnOkValidation;

	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;
	static const wstring g_DbNameKey;
};

