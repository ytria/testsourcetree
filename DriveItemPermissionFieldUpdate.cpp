#include "DriveItemPermissionFieldUpdate.h"

#include "ActivityLoggerUtil.h"
#include "DriveItemsUtil.h"

DriveItemPermissionFieldUpdate::DriveItemPermissionFieldUpdate(const SubItemFieldValue& p_OldValue, const SubItemFieldValue& p_NewValue, UINT p_ColumnKey, GridDriveItems& p_Grid, const GridDriveItems::DriveItemPermissionInfo& p_Info)
    : SubItemFieldUpdate(	p_OldValue,
							p_NewValue,
							p_ColumnKey,
							p_Grid,
							SubItemKey(p_Info.m_OriginId, p_Info.m_DriveId, p_Info.m_DriveItemId, p_Info.m_PermissionId),
							p_Grid.GetColumnPermissionId(),
							p_Grid.GetColumnPermissionId(),
							p_Grid.GetName(p_Info.m_Row)),
    m_PermissionInfo(p_Info),
    m_GridDriveItems(p_Grid)
{
}

bool DriveItemPermissionFieldUpdate::IsCorrespondingRow(GridBackendRow* p_Row) const
{
    return DriveItemsUtil::IsCorrespondingRow(m_GridDriveItems, p_Row, m_PermissionInfo);
}

bool DriveItemPermissionFieldUpdate::ShouldCreateFormatter(GridBackendRow* p_Row, size_t p_Index) const
{
    auto multiVals = p_Row->GetField(GetColumnKey()).GetValuesMulti<PooledString>();
    ASSERT(nullptr != multiVals && p_Index < multiVals->size());
    if (nullptr != multiVals && p_Index < multiVals->size())
        return multiVals->operator[](p_Index) != PooledString(DriveItemsUtil::ROLE_VALUE_OWNER);

    return false;
}