#pragma once

#include "ModificationWithRequestFeedback.h"
#include "GridUtil.h"
#include <type_traits>

class GridMailboxPermissions;

class MailboxPermissionsEditModification : public ModificationWithRequestFeedback
{
public:
	MailboxPermissionsEditModification(GridMailboxPermissions& p_Grid, const row_pk_t& p_Row, const vector<wstring>& p_RightsToAdd, const vector<wstring>& p_RightsToRemove);

	void Merge(const MailboxPermissionsEditModification& p_Mod);

	void Apply() override;
	void Revert() override;
	vector<ModificationLog> GetModificationLogs() const override;

	wstring GetRightsToAdd() const;
	wstring GetRightsToRemove() const;
	const wstring& GetMailboxOwner() const;
	const wstring& GetUser() const;
	
	static vector<wstring> GetCombinedAccessRights(const vector<wstring>& p_Current, const vector<wstring>& p_ToAdd, const vector<wstring>& p_ToRemove);

private:
	void AddToList(wstring& p_List, const wstring& p_ToAdd);
	void RemoveFromList(wstring& p_List, const wstring& p_ToRemove);
	vector<wstring> GetCombinedAccessRights(const vector<wstring>& p_Current) const;

	std::reference_wrapper<GridMailboxPermissions> m_Grid; // Enable copy
	vector<wstring> m_RightsToAdd;
	vector<wstring> m_RightsToRemove;
	vector<wstring> m_OldAccessRights;
	wstring m_MailboxOwner;
	vector<wstring> m_Permissions;
	wstring m_User;

	std::vector<row_pk_t> m_SimilarRows;

	static const wstring g_Sep;
};
