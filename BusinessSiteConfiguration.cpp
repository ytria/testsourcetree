#include "BusinessSiteConfiguration.h"

#include "MsGraphFieldNames.h"

BusinessSiteConfiguration::BusinessSiteConfiguration()
{
	m_YUIDandTitles[_YUID(O365_SITE_DISPLAYNAME)]			= YtriaTranslate::Do(GridSites_customizeGrid_5, _YLOC("Display Name"));
	m_YUIDandTitles[_YUID(O365_SITE_NAME)]					= YtriaTranslate::Do(GridSites_customizeGrid_6, _YLOC("Site Name"));
	m_YUIDandTitles[_YUID(O365_SITE_DESCRIPTION)]			= YtriaTranslate::Do(GridSites_customizeGrid_7, _YLOC("Description"));
	m_YUIDandTitles[_YUID(O365_SITE_CREATEDDATETIME)]		= YtriaTranslate::Do(GridSites_customizeGrid_8, _YLOC("Created On"));
	m_YUIDandTitles[_YUID(O365_SITE_LASTMODIFIEDDATETIME)]	= YtriaTranslate::Do(GridSites_customizeGrid_9, _YLOC("Last Modified"));
	m_YUIDandTitles[_YUID(O365_SITE_WEBURL)]				= YtriaTranslate::Do(GridSites_customizeGrid_10, _YLOC("URL"));
	m_YUIDandTitles[_YUID(O365_SITE_ROOT)]					= YtriaTranslate::Do(GridSites_customizeGrid_11, _YLOC("Root"));
	m_YUIDandTitles[_YUID("host")]							= YtriaTranslate::Do(GridSites_customizeGrid_12, _YLOC("Host Name"));
	m_YUIDandTitles[_YUID("dataLocation")]					= YtriaTranslate::Do(GridSites_customizeGrid_13, _YLOC("Data Location"));
	m_YUIDandTitles[_YUID("sharepointListId")] = YtriaTranslate::Do(GridSites_customizeGrid_15, _YLOC("SharePoint List ID"));
	m_YUIDandTitles[_YUID("sharepointListItemId")] = YtriaTranslate::Do(GridSites_customizeGrid_16, _YLOC("SharePoint List Item ID"));
	m_YUIDandTitles[_YUID("sharepointListItemUniqueId")] = YtriaTranslate::Do(GridSites_customizeGrid_17, _YLOC("SharePoint List Item Unique ID"));
	m_YUIDandTitles[_YUID("sharepointSiteURL")] = YtriaTranslate::Do(GridSites_customizeGrid_18, _YLOC("SharePoint Site URL"));
	m_YUIDandTitles[_YUID("sharepointSiteId")] = YtriaTranslate::Do(GridSites_customizeGrid_19, _YLOC("SharePoint Site ID"));
	m_YUIDandTitles[_YUID("sharepointWebId")] = YtriaTranslate::Do(GridSites_customizeGrid_20, _YLOC("SharePoint Web ID"));

	// List properties (commented ones are of unhandled types)
	m_Properties4RoleDelegation = 
	{
		_YUID(O365_SITE_NAME),
		_YUID(O365_SITE_DISPLAYNAME),
		_YUID(O365_SITE_WEBURL),
		_YUID(O365_SITE_DESCRIPTION),
		_YUID(O365_SITE_CREATEDDATETIME),
		_YUID(O365_SITE_LASTMODIFIEDDATETIME),
		_YUID(O365_SITE_ROOT),
		_YUID(O365_SITE_SHAREPOINTIDSLISTID),
        _YUID(O365_SITE_SHAREPOINTIDSLISTITEMID),
        _YUID(O365_SITE_SHAREPOINTIDSLISTITEMUNIQUEID),
        _YUID(O365_SITE_SHAREPOINTIDSSITEID),
        _YUID(O365_SITE_SHAREPOINTIDSSITEURL),
        _YUID(O365_SITE_SHAREPOINTIDSWEBID),
	};
}
