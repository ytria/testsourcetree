#include "ChannelsRequester.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

ChannelsRequester::ChannelsRequester(PooledString p_TeamId, const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_TeamId(p_TeamId)
	, m_Logger(p_Logger)
{
}

TaskWrapper<void> ChannelsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<BusinessChannel, ChannelDeserializer>>();
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObjects(m_Deserializer, { _YTEXT("teams"), m_TeamId, _YTEXT("channels") }, {}, true, m_Logger, httpLogger, p_TaskData);
}

const vector<BusinessChannel>& ChannelsRequester::GetData() const
{
	ASSERT(m_Deserializer);
	return m_Deserializer->GetData();
}
