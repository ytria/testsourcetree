#include "AttachmentInfo.h"
#include "GridEvents.h"
#include "GridMessages.h"
#include "GridPosts.h"

AttachmentInfo::AttachmentInfo(GridEvents& p_Grid, const wstring& p_AttachmentId, GridBackendRow* p_Row)
{
    Construct(p_Grid, p_AttachmentId, p_Row);
}

AttachmentInfo::AttachmentInfo(GridMessages& p_Grid, const wstring& p_AttachmentId, GridBackendRow* p_Row)
{
    Construct(p_Grid, p_AttachmentId, p_Row);
}

AttachmentInfo::AttachmentInfo(GridPosts& p_Grid, const wstring& p_AttachmentId, GridBackendRow* p_Row)
{
	m_Row = p_Row;
	ASSERT(nullptr != m_Row);
	if (nullptr != m_Row)
	{
		UserOrGroupID()						= m_Row->GetField(p_Grid.m_ColMetaGroupId).GetValueStr();
		EventOrMessageOrConversationID()	= m_Row->GetField(p_Grid.m_ColMetaId).GetValueStr();
		ThreadID()                          = m_Row->GetField(p_Grid.m_ColConversationThreadId).GetValueStr();
		PostID()							= m_Row->GetField(p_Grid.m_ColId).GetValueStr();

		PooledString attachmentId = m_Row->GetField(p_Grid.m_ColAttachmentId).GetValueStr();

		std::vector<PooledString>* attachmentIds = m_Row->GetField(p_Grid.m_ColAttachmentId).GetValuesMulti<PooledString>();
		if (nullptr != attachmentIds)
		{
			bool found = false;

			// Bug #181004.RL.008C86 
			//
			//     size_t index = 0;
			//     for (size_t i = 0;index < attachmentIds->size() && !found; ++i)
			//     {
			//         if (attachmentIds->operator[](i) == p_AttachmentId)
			//         {
			//             index = i;
			//             found = true;
			//         }
			//     }

			for (size_t i = 0; i < attachmentIds->size() && !found; ++i)
			{
				if (attachmentIds->operator[](i) == p_AttachmentId)
					found = true;
			}

			// Bug #181004.RL.008C86 
			//
			// ASSERT(found);

			if (found)
				attachmentId = p_AttachmentId;
		}
		else // Already exploded row
		{
			attachmentId = p_AttachmentId;
		}	
		AttachmentID() = attachmentId;
	}
}
