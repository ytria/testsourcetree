#pragma once

#include "BusinessObject.h"

namespace Sp
{
    class UserIdInfo : public BusinessObject
    {
    public:
        boost::YOpt<PooledString> NameId;
        boost::YOpt<PooledString> NameIdIssuer;

    private:
        RTTR_ENABLE(BusinessObject)
        RTTR_REGISTRATION_FRIEND
    };
}

