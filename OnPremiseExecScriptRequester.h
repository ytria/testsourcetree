#pragma once

#include "IRequester.h"
#include "InvokeResultWrapper.h"

class IPSObjectCollectionDeserializer;
class IRequestLogger;

class OnPremiseExecScriptRequester : public IRequester
{
public:
	OnPremiseExecScriptRequester(const wstring& p_Script, const std::shared_ptr<IRequestLogger>& p_RequestLogger);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	void SetDeserializer(const std::shared_ptr<IPSObjectCollectionDeserializer>& p_Deserializer);

	const std::shared_ptr<InvokeResultWrapper>& GetResult() const;

private:
	const wstring m_Script;

	std::shared_ptr<IRequestLogger> m_Logger;
	std::shared_ptr<InvokeResultWrapper> m_Result;
	std::shared_ptr<IPSObjectCollectionDeserializer> m_Deserializer;
};

