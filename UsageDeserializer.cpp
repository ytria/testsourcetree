#include "UsageDeserializer.h"

#include "JsonSerializeUtil.h"

void Azure::UsageDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeInt32(_YTEXT("currentValue"), m_Data.m_CurrentValue, p_Object);
	JsonSerializeUtil::DeserializeInt32(_YTEXT("limit"), m_Data.m_CurrentValue, p_Object);

	auto itt = p_Object.find(_YTEXT("name"));
	ASSERT(itt != p_Object.end());
	if (itt != p_Object.end())
	{
		ASSERT(itt->second.has_string_field(_YTEXT("value")));
		if (itt->second.has_string_field(_YTEXT("value")))
			JsonSerializeUtil::DeserializeString(_YTEXT("value"), m_Data.m_Name, itt->second.as_object());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("quotaPeriod"), m_Data.m_QuotaPeriod, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("unit"), m_Data.m_Unit, p_Object);
}
