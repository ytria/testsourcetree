#include "DlgOnPremAADConnectServer.h"
#include "Sapio365Settings.h"

namespace
{
	static const wstring g_AADComputerName = _YTEXT("AADComputerName");
	static const wstring g_UseObjectGuid = _YTEXT("UseObjectGuid");
}

DlgOnPremAADConnectServer::DlgOnPremAADConnectServer(bool p_ShowWarningLoaded, CWnd* p_Parent)
	: DlgFormsHTML(DlgFormsHTML::Action::EDIT, p_Parent)
	, m_ShowWarningLoaded(p_ShowWarningLoaded)
{
	m_AADConnectServer = OnPremAADComputerNameSetting().Get();
	m_UseMsDsConsistencyGuid = OnPremUseMsDsConsistencyGuidSetting().Get();
}

bool DlgOnPremAADConnectServer::processPostedData(const IPostedDataTarget::PostedData& data)
{
	if (data.size() == 2)
	{
		for (const auto& d : data)
		{
			if (g_AADComputerName == d.first)
				m_AADConnectServer = d.second;
			else if (g_UseObjectGuid == d.first)
				m_UseMsDsConsistencyGuid = d.second == UsefulStrings::g_CheckboxValueUnchecked;
		}
		OnPremAADComputerNameSetting().Set(m_AADConnectServer);
		OnPremUseMsDsConsistencyGuidSetting().Set(m_UseMsDsConsistencyGuid);
	}
	else
	{
		OnPremAADComputerNameSetting().Set(boost::none);
	}

	return true;
}

wstring DlgOnPremAADConnectServer::getDialogTitle() const
{
	return _T("Azure AD Connect options");
}

int DlgOnPremAADConnectServer::getMinWidthOverride() const
{
	return 485;
}

void DlgOnPremAADConnectServer::generateJSONScriptData()
{
	initMain(_YTEXT("DlgonPremAADComputerName"));

	ASSERT(isEditDialog());

	getGenerator().addIconTextField(_YTEXT("intro"), _T("To 'Sync AD DS/Azure AD' directly from sapio365, the computer name of the Azure AD Connect server is required.<br><br>Please enter it below and select the source anchor* set in Azure AD Connect."), _YTEXT(""));
	getGenerator().Add(StringEditor(g_AADComputerName, _T("Computer name:"), EditorFlags::DIRECT_EDIT, m_AADConnectServer ? *m_AADConnectServer : _YTEXT("")));
	auto defaultVal = m_UseMsDsConsistencyGuid ? !*m_UseMsDsConsistencyGuid : true;
	getGenerator().Add(BoolEditor({ g_UseObjectGuid, _T("Source anchor"), EditorFlags::DIRECT_EDIT, defaultVal }, { _T("objectGUID"), _T("ms-DS-ConsistencyGuid") }));
	if (m_ShowWarningLoaded)
	{
		getGenerator().addIconTextField(_YTEXT("warning"), _T("Changing source anchor attribute won't have any effect until you open a new module instance."), _YTEXT("fas fa-exclamation-triangle"), _YTEXT("#ffcccc"), _YTEXT("#ff0000"));
		// FIXME: Adding this event makes fucks the layout. 
		//getGenerator().addEvent(g_UseObjectGuid, { _YTEXT("warning"), EventTarget::g_HideIfEqual, defaultVal ? UsefulStrings::g_CheckboxValueChecked : UsefulStrings::g_CheckboxValueUnchecked });
	}
	getGenerator().addIconTextField(_YTEXT("outro"), _T("*The source anchor attribute is used to match on-premises objects in Active Directory Domain Services (AD DS) to objects in Azure Active Directory (Azure AD)."), _YTEXT(""));

	getGenerator().addApplyButton(_T("OK"));
	getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
}
