#pragma once

class PersistentSession;

class UISessionListSorter
{
public:
	bool operator()(const PersistentSession& p_Elem1, const PersistentSession& p_Elem2) const;
};

