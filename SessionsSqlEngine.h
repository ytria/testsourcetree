#pragma once

#include "O365SQLiteEngine.h"
#include "MigrationUtil.h"
#include "PersistentSession.h"
#include "SessionIdentifier.h"

class OAuth2AuthenticatorBase;

class SessionsSqlEngine : public O365SQLiteEngine
{
public:
	SessionsSqlEngine(const wstring& p_DBfile);

	vector<SQLiteUtil::Table> GetTables() const override;

	vector<PersistentSession> GetList();
	vector<PersistentSession> GetList(const wstring& p_Type);
	PersistentSession Load(const SessionIdentifier& p_Identifier);
	PersistentSession Load(int64_t p_SessionId);
	PersistentSession LoadWithAnyRole(const SessionIdentifier& p_Identifier);
	void Save(const PersistentSession& p_Session);
	void Logout(const SessionIdentifier& p_Identifier);
	void DeleteCredentials(const SessionIdentifier& p_Identifier);
	void DowngradeElevatedSession(const SessionIdentifier& p_Identifier);
	bool SessionExists(const SessionIdentifier& p_Identifier);
	bool HasSharedCredentials(const SessionIdentifier& p_Identifier);
	bool HasSharedCredentials(int64_t p_CredId);
	int64_t GetRoleIdFromRoleName(const wstring& p_RoleName);

	static SessionsSqlEngine& Get();
	static void Init();
	static bool IsInit();
	static std::unique_ptr<SessionsSqlEngine> CreateEngine(int32_t p_Version);

	uint32_t GetVersion() const override
	{
		return MigrationUtil::GetCurrentMigrationVersion();
	}

private:
	PersistentSession RowToSession(sqlite3_stmt* p_Stmt);

	static std::unique_ptr<SessionsSqlEngine> g_Instance;
};

