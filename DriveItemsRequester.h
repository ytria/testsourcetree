#pragma once

#include "IRequester.h"
#include "IPageRequestLogger.h"

class BusinessDriveItem;
class DriveItemDeserializer;

template <class T, class U>
class ValueListDeserializer;

class DriveItemsRequester : public IRequester
{
public:
	static vector<PooledString> GetSelectProperties();

public:
    DriveItemsRequester(PooledString p_DriveId, bool p_SyncChildren, bool p_QueryCheckoutStatuses, const std::shared_ptr<IPageRequestLogger>& p_Logger);
    virtual TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    vector<BusinessDriveItem>& GetData();

	void SetSearchCriteria(const boost::YOpt<wstring>& p_SearchCriteria);

private:
    PooledString m_DriveId;
    bool m_SyncChildren;
    bool m_QueryCheckoutStatuses;
    boost::YOpt<wstring> m_SearchCriteria;

    std::shared_ptr<ValueListDeserializer<BusinessDriveItem, DriveItemDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;
};
