#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "NumberColumn.h"

class NumberColumnDeserializer : public JsonObjectDeserializer, public Encapsulate<NumberColumn>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};
