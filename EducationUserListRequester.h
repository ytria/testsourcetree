#pragma once

#include "IRequester.h"

#include "EducationUser.h"
#include "ValueListDeserializer.h"
#include "IPageRequestLogger.h"

class EducationUserDeserializer;

class EducationUserListRequester : public IRequester
{
public:
	EducationUserListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const vector<EducationUser>& GetData() const;

private:
	std::shared_ptr<ValueListDeserializer<EducationUser, EducationUserDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;
};

