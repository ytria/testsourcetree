#include "SqlQueryPreparedStatement.h"

#include "MFCUtil.h"
#include "SQLiteEngine.h"
#include <exception>

SqlQueryPreparedStatement::SqlQueryPreparedStatement(SQLiteEngine& p_Engine, const wstring& p_Query, bool p_ThrowOnInitError/* = true*/)
	: m_Stmt(nullptr)
	, m_Engine(p_Engine)
	, m_QueryWstring(p_Query)
{
	m_Query = MFCUtil::convertUNICODE_to_UTF8(p_Query);
	int querySize = static_cast<int>(m_Query.size());

	m_Status = sqlite3_prepare_v2(m_Engine.GetDbHandle(), m_Query.c_str(), querySize, &m_Stmt, nullptr);
	if (m_Status != SQLITE_OK)
	{
		ASSERT(false);
		m_Engine.ProcessError(m_Status, nullptr, p_Query);

		if (p_ThrowOnInitError)
		{
			std::string err = std::string("SqlQueryPreparedStatement: Malformed query:") + m_Query;
			throw std::exception(err.c_str());
		}
	}
}

SqlQueryPreparedStatement::~SqlQueryPreparedStatement()
{
	sqlite3_finalize(m_Stmt);
}

void SqlQueryPreparedStatement::Run()
{
	m_Status = m_Engine.sqlite3_step(m_Stmt);
	HandleError();
}

int SqlQueryPreparedStatement::GetNbRowsChanged() const
{
	return sqlite3_changes(m_Engine.GetDbHandle()); // TODO: Not safe because of multithreading
}

void SqlQueryPreparedStatement::BindString(int p_ParamIndex, const wstring& p_Value)
{
	sqlite3_bind_text(m_Stmt, p_ParamIndex, MFCUtil::convertUNICODE_to_UTF8(p_Value).c_str(), -1, SQLITE_TRANSIENT);
	HandleError();
}

void SqlQueryPreparedStatement::BindInt(int p_ParamIndex, int p_Value)
{
	sqlite3_bind_int(m_Stmt, p_ParamIndex, p_Value);
	HandleError();
}

void SqlQueryPreparedStatement::BindNull(int p_ParamIndex)
{
	sqlite3_bind_null(m_Stmt, p_ParamIndex);
	HandleError();
}

void SqlQueryPreparedStatement::BindInt64(int p_ParamIndex, int64_t p_Value)
{
	sqlite3_bind_int64(m_Stmt, p_ParamIndex, p_Value);
	HandleError();
}

void SqlQueryPreparedStatement::BindBlob(int p_ParamIndex, const void* p_Data, int p_DataSize)
{
	sqlite3_bind_blob(m_Stmt, p_ParamIndex, p_Data, p_DataSize, SQLITE_TRANSIENT);
	HandleError();
}

void SqlQueryPreparedStatement::HandleError() const
{
	m_Engine.ProcessError(m_Status, nullptr, m_QueryWstring);
}
