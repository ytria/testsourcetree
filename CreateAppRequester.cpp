#include "MsGraphHttpRequestLogger.h"
#include "CreateAppRequester.h"
#include "ApplicationDeserializer.h"
#include "Sapio365Session.h"
#include "SessionTypes.h"


CreateApplicationRequester::CreateApplicationRequester(const wstring& p_DisplayName, const web::json::value& p_ResourceAccess)
	:m_DisplayName(p_DisplayName),
	m_ResourceAccess(p_ResourceAccess)
{
}

TaskWrapper<void> CreateApplicationRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	const bool forPartner = p_Session && (p_Session->IsPartnerAdvanced() || p_Session->IsPartnerElevated());
	m_Result = std::make_shared<SingleRequestResult>();
	m_Deserializer = std::make_shared<ApplicationDeserializer>();

	web::uri_builder uri;
	uri.append_path(_YTEXT("applications"));
	
	// TODO: Use appSerializer
	web::json::value payloadJson = web::json::value::object();

	payloadJson.as_object()[_YTEXT("displayName")] = web::json::value::string(m_DisplayName);
	
	auto web = web::json::value::object();
	web[_YTEXT("redirectUris")] = web::json::value::array(
		forPartner && SessionTypes::GetDefaultUltraAdminConsentRedirectUri() != SessionTypes::GetPartnerAlternativeUltraAdminConsentRedirectUri()
		? vector<web::json::value>{ web::json::value::string(SessionTypes::GetDefaultUltraAdminConsentRedirectUri()), web::json::value::string(SessionTypes::GetPartnerAlternativeUltraAdminConsentRedirectUri()) }
		: vector<web::json::value>{ web::json::value::string(SessionTypes::GetDefaultUltraAdminConsentRedirectUri()) });
	payloadJson.as_object()[_YTEXT("web")] = web;
	payloadJson.as_object()[_YTEXT("requiredResourceAccess")] = web::json::value::array({ m_ResourceAccess });

	WebPayloadJSON payload(payloadJson);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_uri(), httpLogger, p_TaskData, payload).ThenByTask([result = m_Result, deserializer = m_Deserializer](pplx::task<RestResultInfo> p_ResInfo) {
		try
		{
			result->SetResult(p_ResInfo.get());
			auto body = result->GetResult().GetBodyAsString();
			ASSERT(body.is_initialized());
			if (body.is_initialized())
				deserializer->Deserialize(web::json::value::parse(*body));
		}
		catch (const RestException& e)
		{
			RestResultInfo info(e.GetRequestResult());
			info.m_RestException = std::make_shared<RestException>(e);
			result->SetResult(info);
		}
		catch (const std::exception& e)
		{
			RestResultInfo info;
			info.m_Exception = e;
			result->SetResult(info);
		}
	});
}

const SingleRequestResult& CreateApplicationRequester::GetResult() const
{
	return *m_Result;
}

const Application& CreateApplicationRequester::GetData() const
{
	return m_Deserializer->GetData();
}
