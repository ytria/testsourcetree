#include "GroupSettingsRequester.h"

#include "BusinessGroupSetting.h"
#include "GroupSettingDeserializer.h"
#include "MSGraphSession.h"
#include "MultiObjectsPageRequestLogger.h"
#include "PaginatedRequestResults.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "ValueListDeserializer.h"
#include "MsGraphHttpRequestLogger.h"

GroupSettingsRequester::GroupSettingsRequester(PooledString p_GroupId, const std::shared_ptr<IPageRequestLogger>& p_Logger)
    : m_GroupId(p_GroupId)
	, m_Logger(p_Logger)
{
}

GroupSettingsRequester::GroupSettingsRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_Logger(p_Logger)
{
}

TaskWrapper<void> GroupSettingsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<BusinessGroupSetting, GroupSettingDeserializer>>();
    m_Result = std::make_shared<PaginatedRequestResults>();

	web::uri_builder uri;

    if (m_GroupId.IsEmpty())
    {
		uri.append_path(_YTEXT("groupSettings"));
    }
    else
    {
        uri.append_path(_YTEXT("groups"));
        uri.append_path(m_GroupId);
        uri.append_path(_YTEXT("settings"));
    }

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Deserializer, MsGraphPaginator::g_DefaultPageSize, m_Logger, httpLogger, p_TaskData.GetOriginator()).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

const vector<BusinessGroupSetting>& GroupSettingsRequester::GetData() const
{
    return m_Deserializer->GetData();
}

const PaginatedRequestResults& GroupSettingsRequester::GetResults() const
{
    return *m_Result;
}
