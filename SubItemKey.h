#pragma once

struct SubItemKey
{
    SubItemKey() = default;
    SubItemKey(const wstring& p_Id0);
    SubItemKey(const wstring& p_Id0, const wstring& p_Id1);
    SubItemKey(const wstring& p_Id0, const wstring& p_Id1, const wstring& p_Id2);
    SubItemKey(const wstring& p_Id0, const wstring& p_Id1, const wstring& p_Id2, const wstring& p_Id3);
	SubItemKey(const wstring& p_Id0, const wstring& p_Id1, const wstring& p_Id2, const wstring& p_Id3, const wstring& p_Id4);

    wstring m_Id0;
    wstring m_Id1;
    wstring m_Id2;
    wstring m_Id3;
	wstring m_Id4;

    bool operator==(const SubItemKey& p_Key) const;

    bool operator<(const SubItemKey& p_Key) const;
};

