#pragma once

#include "ExchangeObjectDeserializer.h"
#include "DelegateUser.h"

namespace Ex
{

class GetDelegatesResponseDeserializer : public ExchangeObjectDeserializer
{
public:
    void DeserializeObject(const DOMElement& p_Envelope) override;
    const vector<Ex::DelegateUser>& GetDelegateUsers() const;

private:
    vector<Ex::DelegateUser> m_DelegateUsers;
    boost::YOpt<PooledString> m_DeliverMeetingRequests;
};

}

