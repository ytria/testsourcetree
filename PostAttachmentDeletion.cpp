#include "PostAttachmentDeletion.h"

#include "ActivityLoggerUtil.h"
#include "GridPosts.h"

PostAttachmentDeletion::PostAttachmentDeletion(GridPosts& p_Grid, const AttachmentInfo& p_Info):
    ISubItemDeletion(	p_Grid,
						SubItemKey(p_Info.UserOrGroupID(), p_Info.EventOrMessageOrConversationID(), p_Info.ThreadID(), p_Info.PostID(), p_Info.AttachmentID()),
						p_Grid.m_ColAttachmentId,
						p_Grid.m_ColAttachmentId,
						p_Grid.GetName(p_Info.m_Row)),
    m_GridPosts(p_Grid),
    m_AttachmentInfo(p_Info)
{
}

bool PostAttachmentDeletion::IsCorrespondingRow(GridBackendRow* p_Row) const
{
    if (!IsCollapsedRow(p_Row))
    {
        // Exploded row
        if (p_Row->GetField(m_GridPosts.m_ColMetaGroupId).HasValue() &&
            p_Row->GetField(m_GridPosts.m_ColMetaId).HasValue() &&
			p_Row->GetField(m_GridPosts.m_ColConversationThreadId).HasValue() &&
            p_Row->GetField(m_GridPosts.m_ColId).HasValue() &&
            p_Row->GetField(m_GridPosts.m_ColAttachmentId).HasValue())
        {
            if (PooledString(p_Row->GetField(m_GridPosts.m_ColMetaGroupId).GetValueStr()) == m_AttachmentInfo.UserOrGroupID() &&
                PooledString(p_Row->GetField(m_GridPosts.m_ColMetaId).GetValueStr()) == m_AttachmentInfo.EventOrMessageOrConversationID() &&
				PooledString(p_Row->GetField(m_GridPosts.m_ColConversationThreadId).GetValueStr()) == m_AttachmentInfo.ThreadID() &&
                PooledString(p_Row->GetField(m_GridPosts.m_ColId).GetValueStr()) == m_AttachmentInfo.PostID() &&
                PooledString(p_Row->GetField(m_GridPosts.m_ColAttachmentId).GetValueStr()) == m_AttachmentInfo.AttachmentID())
            {
                return true;
            }
        }
    }
    else
    {
        // Collapsed row
        if (p_Row->GetField(m_GridPosts.m_ColMetaGroupId).HasValue() &&
            p_Row->GetField(m_GridPosts.m_ColMetaId).HasValue() &&
			p_Row->GetField(m_GridPosts.m_ColConversationThreadId).HasValue() &&
            p_Row->GetField(m_GridPosts.m_ColId).HasValue())
        {
            PooledString rowMetaGroupId = p_Row->GetField(m_GridPosts.m_ColMetaGroupId).GetValueStr();
            PooledString rowConvId = p_Row->GetField(m_GridPosts.m_ColMetaId).GetValueStr();
            PooledString rowConversationThreadId = p_Row->GetField(m_GridPosts.m_ColConversationThreadId).GetValueStr();
            PooledString rowPostId = p_Row->GetField(m_GridPosts.m_ColId).GetValueStr();
            if (rowMetaGroupId == m_AttachmentInfo.UserOrGroupID() &&
				rowConvId == m_AttachmentInfo.EventOrMessageOrConversationID() &&
				rowConversationThreadId == m_AttachmentInfo.ThreadID() &&
				rowPostId == m_AttachmentInfo.PostID())
            {
                std::vector<PooledString>* attachmentIds = p_Row->GetField(m_GridPosts.m_ColAttachmentId).GetValuesMulti<PooledString>();
                // Attachment id is corresponding?
                if (nullptr != attachmentIds)
                {
                    for (const auto& attachId : *attachmentIds)
                    {
                        if (attachId == m_AttachmentInfo.AttachmentID())
                            return true;
                    }
                }
                else  if(PooledString(p_Row->GetField(m_GridPosts.m_ColAttachmentId).GetValueStr()) == m_AttachmentInfo.AttachmentID()) // Row with only one attachment
                {
                    return true;
                }
            }

        }
    }

    return false;
}

vector<Modification::ModificationLog> PostAttachmentDeletion::GetModificationLogs() const
{
	return { ModificationLog(getPk(), GetObjectName(), ActivityLoggerUtil::g_ActionDelete, _YTEXT("Attachment"), GetState()) };
}

GridBackendRow* PostAttachmentDeletion::GetMainRowIfCollapsed() const
{
	vector<GridBackendRow*> rows;
	m_GridPosts.GetRowsByCriteria(rows, [this](GridBackendRow* p_Row)
		{
			const auto groupId = p_Row->GetField(m_GridPosts.m_ColMetaGroupId).GetValueStr();
			const auto metaId = p_Row->GetField(m_GridPosts.m_ColMetaId).GetValueStr();
			const auto threadId = p_Row->GetField(m_GridPosts.m_ColConversationThreadId).GetValueStr();
			const auto postId = p_Row->GetField(m_GridPosts.m_ColId).GetValueStr();
			return nullptr != groupId && m_AttachmentInfo.UserOrGroupID() == groupId
				&& nullptr != metaId && m_AttachmentInfo.EventOrMessageOrConversationID() == metaId
				&& nullptr != threadId && m_AttachmentInfo.ThreadID() == threadId
				&& nullptr != postId && m_AttachmentInfo.PostID() == postId;
		});

	if (rows.size() == 1 && IsCollapsedRow(rows.front()))
		return rows.front();

	return nullptr;
}
