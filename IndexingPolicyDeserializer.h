#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "IndexingPolicy.h"

namespace Cosmos
{
    class IndexingPolicyDeserializer : public JsonObjectDeserializer, public Encapsulate<Cosmos::IndexingPolicy>
    {
    public:
        void DeserializeObject(const web::json::object& p_Object) override;
    };
}
