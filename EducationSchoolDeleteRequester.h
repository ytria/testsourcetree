#pragma once

#include "IRequester.h"
#include "HttpResultWithError.h"

class EducationSchoolDeleteRequester : public IRequester
{
public:
	EducationSchoolDeleteRequester(const wstring& p_SchoolId);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const HttpResultWithError& GetResult() const;

private:
	std::shared_ptr<HttpResultWithError> m_Result;
	wstring m_SchoolId;
};

