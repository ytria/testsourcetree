#include "UserIdInfoDeserializer.h"

#include "JsonSerializeUtil.h"

void Sp::UserIdInfoDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("NameId"), m_Data.NameId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("NameIdIssuer"), m_Data.NameIdIssuer, p_Object);
}
