#pragma once

#include "BaseO365Grid.h"
#include "Application.h"

class FrameApplications;

class GridApplications : public ModuleO365Grid<Application>
{
public:
	GridApplications();
	virtual ~GridApplications() override;
	void BuildView(const vector<Application>& p_Applications, bool p_FullPurge, bool p_NoPurge);

	void FillApplicationFields(GridBackendRow& p_Row, const Application& p_Application);

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
	void customizeGrid() override;

	Application getBusinessObject(GridBackendRow*) const override;
	void UpdateBusinessObjects(const vector<Application>& p_Applications, bool p_SetModifiedStatus) override;
	void RemoveBusinessObjects(const vector<Application>& p_Applications) override;

private:
	DECLARE_MESSAGE_MAP()

	void InitializeCommands() override;
	void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart) override;

	FrameApplications* m_Frame;

	GridBackendColumn* m_ColAddInsId;
	GridBackendColumn* m_ColAddInsPropertiesKey;
	GridBackendColumn* m_ColAddInsPropertiesValue;
	GridBackendColumn* m_ColAddInsType;
	GridBackendColumn* m_ColApiAcceptMappedClaims;
	GridBackendColumn* m_ColApiKnownClientApplications;
	GridBackendColumn* m_ColApiOauth2PermissionScopesAdminConsentDescription;
	GridBackendColumn* m_ColApiOauth2PermissionScopesAdminConsentDisplayName;
	GridBackendColumn* m_ColApiOauth2PermissionScopesId;
	GridBackendColumn* m_ColApiOauth2PermissionScopesIsEnabled;
	GridBackendColumn* m_ColApiOauth2PermissionScopesOrigin;
	GridBackendColumn* m_ColApiOauth2PermissionScopesType;
	GridBackendColumn* m_ColApiOauth2PermissionScopesUserConsentDescription;
	GridBackendColumn* m_ColApiOauth2PermissionScopesUserConsentDisplayName;
	GridBackendColumn* m_ColApiOauth2PermissionScopesValue;
	GridBackendColumn* m_ColApiPreAuthorizedApplicationsAppId;
	GridBackendColumn* m_ColApiPreAuthorizedApplicationsDelegatedPermissionIds;
	GridBackendColumn* m_ColApiRequestedAccessTokenVersion;
	GridBackendColumn* m_ColAppId;
	GridBackendColumn* m_ColAppRoleAllowedMemberTypes;
	GridBackendColumn* m_ColAppRoleDescription;
	GridBackendColumn* m_ColAppRoleDisplayName;
	GridBackendColumn* m_ColAppRoleId;
	GridBackendColumn* m_ColAppRoleIsEnabled;
	GridBackendColumn* m_ColAppRoleOrigin;
	GridBackendColumn* m_ColAppRoleValue;
	GridBackendColumn* m_ColCreatedDateTime;
	GridBackendColumn* m_ColDeletedDateTime;
	GridBackendColumn* m_ColDisplayName;
	GridBackendColumn* m_ColGroupMembershipClaims;
	GridBackendColumn* m_ColId;
	GridBackendColumn* m_ColIdentifierUris;
	GridBackendColumn* m_ColInfoLogoUrl;
	GridBackendColumn* m_ColInfoMarketingUrl;
	GridBackendColumn* m_ColInfoPrivacyStatementUrl;
	GridBackendColumn* m_ColInfoSupportUrl;
	GridBackendColumn* m_ColInfoTermsOfServiceUrl;
	GridBackendColumn* m_ColIsFallbackPublicClient;
	GridBackendColumn* m_ColKeyCredsCustomKeyIdentifier;
	GridBackendColumn* m_ColKeyCredsDisplayName;
	GridBackendColumn* m_ColKeyCredsEndDateTime;
	GridBackendColumn* m_ColKeyCredsKeyId;
	GridBackendColumn* m_ColKeyCredsStartDateTime;
	GridBackendColumn* m_ColKeyCredsType;
	GridBackendColumn* m_ColKeyCredsUsage;
	GridBackendColumn* m_ColKeyCredsKey;
	GridBackendColumn* m_ColLogo;
	GridBackendColumn* m_ColOptClaimsIdTokenAdditionalProperties;
	GridBackendColumn* m_ColOptClaimsIdTokenEssential;
	GridBackendColumn* m_ColOptClaimsIdTokenName;
	GridBackendColumn* m_ColOptClaimsIdTokenSource;
	GridBackendColumn* m_ColOptClaimsAccessTokenAdditionalProperties;
	GridBackendColumn* m_ColOptClaimsAccessTokenEssential;
	GridBackendColumn* m_ColOptClaimsAccessTokenName;
	GridBackendColumn* m_ColOptClaimsAccessTokenSource;
	GridBackendColumn* m_ColOptClaimsSaml2TokenAdditionalProperties;
	GridBackendColumn* m_ColOptClaimsSaml2TokenEssential;
	GridBackendColumn* m_ColOptClaimsSaml2TokenName;
	GridBackendColumn* m_ColOptClaimsSaml2TokenSource;
	GridBackendColumn* m_ColParentalControlSettingsCountriesBlockedForMinors;
	GridBackendColumn* m_ColParentalControlSettingsLegalAgeGroupRule;
	GridBackendColumn* m_ColPasswordCredentialsCustomKeyIdentifier;
	GridBackendColumn* m_ColPasswordCredentialsDisplayName;
	GridBackendColumn* m_ColPasswordCredentialsEndDateTime;
	GridBackendColumn* m_ColPasswordCredentialsHint;
	GridBackendColumn* m_ColPasswordCredentialsKeyId;
	GridBackendColumn* m_ColPasswordCredentialsSecretText;
	GridBackendColumn* m_ColPasswordCredentialsStartDateTime;
	GridBackendColumn* m_ColPublicClientRedirectUris;
	GridBackendColumn* m_ColPublisherDomain;
	GridBackendColumn* m_ColRequiredResourceAccessResourceAccessId;
	GridBackendColumn* m_ColRequiredResourceAccessResourceAccessType;
	GridBackendColumn* m_ColRequiredResourceAccessResourceAppId;
	GridBackendColumn* m_ColSignInAudience;
	GridBackendColumn* m_ColTags;
	GridBackendColumn* m_ColTokenEncryptionKeyId;
	GridBackendColumn* m_ColWebHomePageUrl;
	GridBackendColumn* m_ColWebImplicitGrantSettingsEnableIdTokenIssuance;
	GridBackendColumn* m_ColWebImplicitGrantSettingsEnableAccessTokenIssuance;
	GridBackendColumn* m_ColWebLogoutUrl;
	GridBackendColumn* m_ColWebRedirectUris;
};

