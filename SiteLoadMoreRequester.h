#pragma once

#include "IRequester.h"

class BusinessSite;
class IRequestLogger;

class SiteLoadMoreRequester : public IRequester
{
public:
	SiteLoadMoreRequester(const wstring& p_SiteId, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const BusinessSite& GetData() const;

private:
	std::shared_ptr<BusinessSite> m_Site;
	std::shared_ptr<IRequestLogger> m_Logger;
};

