#pragma once

#include "IPropertySetBuilder.h"

class GroupListFullPropertySet : public IPropertySetBuilder
{
public:
    virtual vector<rttr::property> GetPropertySet() const override;
};

class DeletedGroupListFullPropertySet : public GroupListFullPropertySet
{
public:
	virtual vector<rttr::property> GetPropertySet() const override;
};
