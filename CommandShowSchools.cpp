#include "CommandShowSchools.h"

#include "FrameSchools.h"
#include "GridSchools.h"
#include "EducationSchoolsListRequester.h"
#include "ModuleBase.h"
#include "O365AdminUtil.h"
#include "EducationClassListRequester.h"
#include "BasicPageRequestLogger.h"

CommandShowSchools::CommandShowSchools(FrameSchools& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, bool p_IsUpdate)
	: IActionCommand(_YTEXT("")),
	m_Frame(p_Frame),
	m_AutoSetup(p_AutoSetup),
	m_LicenseCtx(p_LicenseCtx),
	m_AutoAction(p_AutoAction),
	m_IsUpdate(p_IsUpdate)
{
}

CommandShowSchools::CommandShowSchools(const vector<EducationSchool>& p_Schools, FrameSchools& p_Frame, AutomationSetup& p_AutoSetup, AutomationAction* p_AutoAction, const LicenseContext& p_LicenseCtx, bool p_IsUpdate)
	: IActionCommand(_YTEXT("")),
	m_Frame(p_Frame),
	m_AutoSetup(p_AutoSetup),
	m_LicenseCtx(p_LicenseCtx),
	m_AutoAction(p_AutoAction),
	m_IsUpdate(p_IsUpdate),
	m_Schools(p_Schools)
{
}


void CommandShowSchools::ExecuteImpl() const
{
	GridSchools* grid = dynamic_cast<GridSchools*>(&m_Frame.GetGrid());

	auto taskData = Util::AddFrameTask(_T("Show schools"), &m_Frame, m_AutoSetup, m_LicenseCtx, false);

	YSafeCreateTaskMutable([schoolsToUpdate = m_Schools, isUpdate = m_IsUpdate, taskData, hwnd = m_Frame.GetSafeHwnd(), moduleCriteria = m_Frame.GetModuleCriteria(), autoAction = m_AutoAction, grid, bom = m_Frame.GetBusinessObjectManager()]() mutable {

		vector<EducationSchool> schools;

		auto schoolsLogger = std::make_shared<BasicPageRequestLogger>(_T("schools"));
		auto schoolsRequester = std::make_shared<EducationSchoolsListRequester>(schoolsLogger);

		schools = safeTaskCall(schoolsRequester->Send(bom->GetSapio365Session(), taskData).Then([schoolsRequester]() {
			return schoolsRequester->GetData();
		}), bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ListErrorHandler<EducationSchool>, taskData).get();

		if (!schoolsToUpdate.empty())
		{
			for (auto& schoolToUpdate : schoolsToUpdate)
			{
				auto schoolItt = std::find(schools.begin(), schools.end(), schoolToUpdate);
				if (schoolItt != schools.end())
					schoolToUpdate = *schoolItt;
			}
			schools = schoolsToUpdate;
		}

		auto classMembersLogger = std::make_shared<MultiObjectsPageRequestLogger>(_T("school classes"), schools.size());

		for (auto& school : schools)
		{
			classMembersLogger->IncrementObjCount();
			classMembersLogger->SetContextualInfo(GetDisplayNameOrId(school));

			auto classesRequester = std::make_shared<EducationClassListRequester>(school.m_Id, classMembersLogger);
			school.m_Classes = safeTaskCall(classesRequester->Send(bom->GetSapio365Session(), taskData).Then([classesRequester]() {
				return classesRequester->GetData();
			}), bom->GetSapio365Session()->GetMSGraphSession(Sapio365Session::USER_SESSION), Util::ListErrorHandler<EducationClass>, taskData).get();
		}

		YCallbackMessage::DoPost([=]() {
			bool shouldFinishTask = true;
			auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameSchools*>(CWnd::FromHandle(hwnd)) : nullptr;
			if (ModuleBase::ShouldBuildView(hwnd, taskData.IsCanceled(), taskData, false))
			{
				ASSERT(nullptr != frame);
				if (nullptr != frame)
				{
					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s schools...", Str::getStringFromNumber(schools.size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());

					{
						CWaitCursor _;
						if (frame->HasLastUpdateOperations())
						{
							frame->BuildView(schools, frame->GetLastUpdateOperations(), false);
							frame->ForgetLastUpdateOperations();
						}
						else
						{
							frame->BuildView(schools, {}, true);
						}
					}

					if (!isUpdate)
					{
						frame->UpdateContext(taskData, hwnd);
						shouldFinishTask = false;
					}
				}

				if (shouldFinishTask)
					frame->TaskFinished(taskData, nullptr, hwnd);
				}
		});
	});
}
