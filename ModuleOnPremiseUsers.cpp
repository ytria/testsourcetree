#include "ModuleOnPremiseUsers.h"

#include "FrameMailboxPermissions.h"
#include "RefreshSpecificData.h"
#include "YCallbackMessage.h"
#include "MultiObjectsRequestLogger.h"
#include "InvokeResultWrapper.h"
#include "FrameOnPremiseUsers.h"
#include "BasicPageRequestLogger.h"
#include "OnPremiseUsersRequester.h"
#include "safeTaskCall.h"
#include "OnPremiseUsersCollectionDeserializer.h"
#include "ModuleUtil.h"

void ModuleOnPremiseUsers::executeImpl(const Command& p_Command)
{
	switch (p_Command.GetTask())
	{
	case Command::ModuleTask::List:
		showOnPremiseUsers(p_Command);
		break;
	case Command::ModuleTask::UpdateRefresh:
		showOnPremiseUsers(p_Command);
		break;
	default:
		ASSERT(false);
		SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString());
		break;
	}
}

void ModuleOnPremiseUsers::showOnPremiseUsers(const Command& p_Command)
{
	auto		p_SourceWindow = p_Command.GetSourceWindow();
	auto		p_Action = p_Command.GetAutomationAction();

	bool isUpdate = false;

	auto& info = p_Command.GetCommandInfo();

	FrameOnPremiseUsers* frame = dynamic_cast<FrameOnPremiseUsers*>(info.GetFrame());

	const bool isRefresh = nullptr != frame;
	if (nullptr == frame)
	{
		if (!FrameOnPremiseUsers::CanCreateNewFrame(true, p_SourceWindow.GetCWnd(), p_Action, PreAllocatedIDS::HasFreeUserPictureID() && PreAllocatedIDS::HasFreeGroupPolicyIconID()))
			return;

		if (!ModuleUtil::WarnIfPowerShellHostIncompatible(p_SourceWindow.GetCWnd()))
			return;

		ModuleCriteria moduleCriteria;
		moduleCriteria.m_Origin = Origin::Tenant;
		moduleCriteria.m_UsedContainer = ModuleCriteria::UsedContainer::IDS;
		moduleCriteria.m_Privilege = info.GetRBACPrivilege();

		ASSERT(moduleCriteria.m_IDs.end() == std::find_if(moduleCriteria.m_IDs.begin(), moduleCriteria.m_IDs.end(), [](const PooledString& id) {return O365Grid::IsTemporaryCreatedObjectID(id); }));

		if (ShouldCreateFrame<FrameOnPremiseUsers>(moduleCriteria, p_SourceWindow, p_Action) && p_SourceWindow.UserConfirmation(p_Action))
		{
			CWaitCursor _;

			frame = new FrameOnPremiseUsers(false, p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), _T("On-Premises Users"), *this, p_SourceWindow.GetHistoryMode(), p_SourceWindow.GetCFrameWndForHistory());
			frame->InitModuleCriteria(moduleCriteria);
			AddGridFrame(frame);
			frame->Erect();
		}
	}
	else
	{
		isUpdate = true;
	}

	// When the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
	if (nullptr != frame)
	{
		GridOnPremiseUsers* grid = dynamic_cast<GridOnPremiseUsers*>(&frame->GetGrid());

		auto taskData = Util::AddFrameTask(_T("Show On-Premises Users"), frame, p_Command.GetAutomationSetup(), p_Command.GetLicenseContext(), !isUpdate);

		YSafeCreateTask([isUpdate, taskData, hwnd = frame->GetSafeHwnd(), moduleCriteria = frame->GetModuleCriteria(), autoAction = p_Command.GetAutomationAction(), grid, bom = frame->GetBusinessObjectManager()]()
		{
			auto deserializer = std::make_shared<OnPremiseUsersCollectionDeserializer>();

			auto logger = std::make_shared<BasicRequestLogger>(_T("users"));
			auto requester = std::make_shared<OnPremiseUsersRequester>(deserializer, logger);
			requester->Send(bom->GetSapio365Session(), taskData).GetTask().wait();

			wstring error = GetPSErrorString(requester->GetResult());
			if (!error.empty())
			{
				OnPremiseUser errorUser;
				errorUser.SetError(SapioError(error, 0));

				deserializer->GetData() = { errorUser };
			}

			YCallbackMessage::DoPost([=]() {
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameOnPremiseUsers*>(CWnd::FromHandle(hwnd)) : nullptr;

				if (ModuleBase::ShouldBuildView(hwnd, taskData.IsCanceled(), taskData, false))
				{
					ASSERT(nullptr != frame);
					if (nullptr != frame)
					{
						frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting %s users...", Str::getStringFromNumber(deserializer->GetData().size()).c_str()));
						frame->GetGrid().ClearLog(taskData.GetId());

						{
							CWaitCursor _;
							frame->ShowOnPremiseUsers(deserializer->GetData(), true);
						}

						if (!isUpdate)
						{
							frame->UpdateContext(taskData, hwnd);
							shouldFinishTask = false;
						}
					}
				}

				if (shouldFinishTask)
				{
					frame->ProcessLicenseContext(); // Let's consume license tokens if needed
					frame->TaskFinished(taskData, nullptr, hwnd);
				}
			});
		});
	}
	else
	{
		SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
}
