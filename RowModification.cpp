#include "RowModification.h"

#include "BaseO365Grid.h"

RowModification::RowModification(O365Grid& p_Grid, const row_pk_t& p_RowKey)
	: IMainItemModification(State::AppliedLocally, wstring())
	, m_Grid(p_Grid)
	, m_RowKey(p_RowKey)
	, m_Revert(true)
{
	auto row = p_Grid.GetRowIndex().GetExistingRow(p_RowKey);
	// Do not assert. It can happen when calling CreatedObjectModification kind of copy constructor.
	//ASSERT(nullptr != row);
	if (nullptr != row)
		SetObjectName(p_Grid.GetName(row));
}

const row_pk_t& RowModification::GetRowKey() const
{
	return m_RowKey;
}

bool RowModification::IsCorrespondingRow(const row_pk_t& p_RowPk) const
{
	return m_RowKey == p_RowPk;
}

void RowModification::DontRevertOnDestruction()
{
	m_Revert = false;
}

RowModification& RowModification::operator=(const RowModification& p_Other)
{
	this->IMainItemModification::operator=(p_Other);

	// Can't change grid.
	ASSERT(&m_Grid.get() == &p_Other.getGrid());
	m_RowKey = p_Other.m_RowKey;

	return *this;
}

O365Grid& RowModification::getGrid()
{
	return m_Grid.get();
}

const O365Grid& RowModification::getGrid() const
{
	return m_Grid.get();
}

void RowModification::setAppliedLocally(GridBackendRow* p_Row)
{
	m_State = IMainItemModification::State::AppliedLocally;
	if (nullptr != p_Row)
		m_Grid.get().OnRowModified(p_Row);
}

bool RowModification::shouldRevert() const
{
	return m_Revert;
}