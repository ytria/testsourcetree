#include "SubItemsFlagGetter.h"

SubItemsFlagGetter::SubItemsFlagGetter(GridModificationsO365& p_Modifications, GridBackendColumn* p_MultiValueElderColumn)
    : m_Modifications(p_Modifications)
	, m_MultiValueElderColumn(p_MultiValueElderColumn)
{
}

bool SubItemsFlagGetter::IsFlaggedAsDeletion(GridBackendRow* p_Row, GridBackendColumn* p_ExplosionColumn)
{
    for (const auto& deletion : m_Modifications.GetSubItemDeletions())
    {
        ASSERT(nullptr != deletion);
        if (nullptr != deletion)
        {
			// If our row corresponds to a modification linked to another multivalue sisterhood, many things won't work :-o
			ASSERT(!deletion->IsCorrespondingRow(p_Row) || deletion->GetSubItemPkCol()->GetMultiValueElderColumn() == m_MultiValueElderColumn);

			if (deletion->IsCorrespondingRow(p_Row)
				&& deletion->GetState() != Modification::State::RemoteError
				&& (nullptr == p_ExplosionColumn || deletion->GetSubItemPkCol()->GetMultiValueElderColumn() == p_ExplosionColumn->GetMultiValueElderColumn()))
				return true;
        }
    }
    return false;
}

bool SubItemsFlagGetter::IsFlaggedAsUpdated(GridBackendRow* p_Row, GridBackendColumn* p_ExplosionColumn)
{
    for (const auto& deletion : m_Modifications.GetSubItemDeletions())
    {
        ASSERT(nullptr != deletion);
        if (nullptr != deletion)
        {
			// If our row corresponds to a modification linked to another multivalue sisterhood, many things won't work :-o
			ASSERT(!deletion->IsCorrespondingRow(p_Row) || deletion->GetSubItemPkCol()->GetMultiValueElderColumn() == m_MultiValueElderColumn);

            if (deletion->IsCorrespondingRow(p_Row)
				&& deletion->GetState() != Modification::State::RemoteError
				&& (nullptr == p_ExplosionColumn || deletion->GetSubItemPkCol()->GetMultiValueElderColumn() == p_ExplosionColumn->GetMultiValueElderColumn()))
                return true;
        }
    }

    for (const auto& update : m_Modifications.GetSubItemUpdates())
    {
        ASSERT(nullptr != update);
        if (nullptr != update)
        {
			// If our row corresponds to a modification linked to another multivalue sisterhood, many things won't work :-o
			ASSERT(!update->IsCorrespondingRow(p_Row) || update->GetSubItemPkCol()->GetMultiValueElderColumn() == m_MultiValueElderColumn);

            if (update->IsCorrespondingRow(p_Row)
				&& update->GetState() == Modification::State::AppliedLocally
				&& (nullptr == p_ExplosionColumn || update->GetSubItemPkCol()->GetMultiValueElderColumn() == p_ExplosionColumn->GetMultiValueElderColumn()))
                return true;
        }
    }
    return false;
}

wstring SubItemsFlagGetter::IsFlaggedAsError(GridBackendRow* p_Row, GridBackendColumn* p_ExplosionColumn)
{
    wstring errorMessage;
    int errorCount = 0;

    for (const auto& deletion : m_Modifications.GetSubItemDeletions())
    {
        ASSERT(nullptr != deletion);
        if (nullptr != deletion)
        {
			// If our row corresponds to a modification linked to another multivalue sisterhood, many things won't work :-o
			ASSERT(!deletion->IsCorrespondingRow(p_Row) || deletion->GetSubItemPkCol()->GetMultiValueElderColumn() == m_MultiValueElderColumn);

            if (deletion->IsCorrespondingRow(p_Row)
				&& (nullptr == p_ExplosionColumn || deletion->GetSubItemPkCol()->GetMultiValueElderColumn() == p_ExplosionColumn->GetMultiValueElderColumn()))
            {
                auto error = deletion->GetError();
                if (error != boost::none)
                {
                    if (errorCount == 0)
                        errorMessage += GetErrorMessage(*error);
                    else
                        errorMessage += std::wstring(_YTEXT("\n")) + GetErrorMessage(*error);
                }
                ++errorCount;
            }
        }
    }

    // Loop on updates if needed (if we don't already have a deletion error - can't have both types of errors on the same row for now)
    if (errorCount == 0)
    {
        for (const auto& update : m_Modifications.GetSubItemUpdates())
        {
            ASSERT(nullptr != update);
            if (nullptr != update)
            {
				// If our row corresponds to a modification linked to another multivalue sisterhood, many things won't work :-o
				ASSERT(!update->IsCorrespondingRow(p_Row) || update->GetSubItemPkCol()->GetMultiValueElderColumn() == m_MultiValueElderColumn);

                if (update->IsCorrespondingRow(p_Row)
					&& (nullptr == p_ExplosionColumn || update->GetSubItemPkCol()->GetMultiValueElderColumn() == p_ExplosionColumn->GetMultiValueElderColumn()))
                {
                    auto error = update->GetError();
                    if (error != boost::none)
                    {
                        if (errorCount == 0)
                            errorMessage += GetErrorMessage(*error);
                        else
                            errorMessage += std::wstring(_YTEXT("\n")) + GetErrorMessage(*error);
                    }
                    ++errorCount;
                }
            }
        }
    }

    return errorMessage;
}

bool SubItemsFlagGetter::AppliesOn(GridBackendColumn* p_Column)
{
	return nullptr != p_Column && p_Column->GetMultiValueElderColumn() == m_MultiValueElderColumn;
}

GridBackendColumn* SubItemsFlagGetter::GetMultiValueElderColumn()
{
	return m_MultiValueElderColumn;
}

wstring SubItemsFlagGetter::GetErrorMessage(const SapioError& p_Error) const
{
    return std::to_wstring(p_Error.GetStatusCode()) +
           std::wstring(_YTEXT(" - ")) +
           p_Error.GetFullErrorMessage();
}
