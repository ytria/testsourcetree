#include "CachedOrgContactPropertySet.h"

#include "BusinessOrgContact.h"
#include "MsGraphFieldNames.h"

vector<rttr::property> CachedOrgContactPropertySet::GetPropertySet() const
{
    // FIXME: Don't use "BusinessOrgContact" class neither rttr::property
    const vector<rttr::property> properties = {
        rttr::type::get<BusinessOrgContact>().get_property("id"),
        rttr::type::get<BusinessOrgContact>().get_property("displayName"),
        rttr::type::get<BusinessOrgContact>().get_property("mail"),
    };
    return properties;
}
