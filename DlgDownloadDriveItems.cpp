#include "DlgDownloadDriveItems.h"

#include "AutomationNames.h"
#include "FileUtil.h"
#include "Sapio365Settings.h"
#include "YFileDialog.h"
#include "YCodeJockMessageBox.h"

wstring DlgDownloadDriveItems::g_LastFolderPath;

bool DlgDownloadDriveItems::g_PreserveHierarchy = false;
bool DlgDownloadDriveItems::g_UserOwnerName     = false;
bool DlgDownloadDriveItems::g_OpenAfterDownload = false;

FileExistsAction DlgDownloadDriveItems::g_FileExistsAction = FileExistsAction::Append;

BEGIN_MESSAGE_MAP(DlgDownloadDriveItems, ResizableDialog)
    ON_BN_CLICKED(IDC_DLG_DOWNLOAD_DRIVEITEMS_BTN_CHOOSE_FOLDER,		OnSelectFolder)
    ON_BN_CLICKED(IDC_DLG_DOWNLOAD_DRIVEITEMS_CHK_PRESERVE_HIERARCHY,	OnPreserveHierarchyChanged)
    ON_BN_CLICKED(IDC_DLG_DOWNLOAD_DRIVEITEMS_CHK_USER_OWNER_NAME,      OnUseOwnerNameChanged)
    ON_BN_CLICKED(IDC_DLG_DOWNLOAD_DRIVEITEMS_OPEN_FOLDER_AFTER_DOWNLOAD, OnOpenAfterDownloadChanged)
    ON_BN_CLICKED(IDC_DLG_DRIVEITEMS_APPEND,							OnSelectAppend)
    ON_BN_CLICKED(IDC_DLG_DRIVEITEMS_SKIP,								OnSelectSkip)
    ON_BN_CLICKED(IDC_DLG_DRIVEITEMS_OVERWRITE,							OnSelectOverwrite)
END_MESSAGE_MAP()

const wstring DlgDownloadDriveItems::g_AutomationName = g_ActionNameSelectedDownload;

DlgDownloadDriveItems::DlgDownloadDriveItems(CWnd* p_Parent)
    : ResizableDialog(IDD, p_Parent, g_AutomationName)
    , m_OpenAfterDownload(g_OpenAfterDownload)
	, m_PreserveHierarchy(g_PreserveHierarchy)
    , m_UseOwnerName(g_UserOwnerName)
{
}

const wstring& DlgDownloadDriveItems::GetFolderPath() const
{
    return m_FolderPath;
}

bool DlgDownloadDriveItems::GetPreserveHierarchy() const
{
    return m_PreserveHierarchy;
}

bool DlgDownloadDriveItems::GetUseOwnerName() const
{
	return m_UseOwnerName;
}

FileExistsAction DlgDownloadDriveItems::GetFileExistsAction() const
{
    return m_FileExistsAction;
}

bool DlgDownloadDriveItems::GetOpenAfterDownload() const
{
    return m_OpenAfterDownload;
}

void DlgDownloadDriveItems::DoDataExchange(CDataExchange* pDX)
{
    ResizableDialog::DoDataExchange(pDX);

    DDX_Control(pDX, IDC_DLG_DOWNLOAD_DRIVEITEMS_FOLDER_TXT_PATH,			m_TxtFolderPath);
    DDX_Control(pDX, IDC_DLG_DOWNLOAD_DRIVEITEMS_BTN_CHOOSE_FOLDER,			m_BtnChooseFolder);
    DDX_Control(pDX, IDC_DLG_DOWNLOAD_DRIVEITEMS_CHK_PRESERVE_HIERARCHY,	m_ChkPreserveHierarchy);
	DDX_Control(pDX, IDC_DLG_DOWNLOAD_DRIVEITEMS_CHK_USER_OWNER_NAME,       m_CheckUseOwnerName);
    
    DDX_Control(pDX, IDC_DLG_DRIVEITEMS_APPEND,								m_RadioBtnCreateNew);
    DDX_Control(pDX, IDC_DLG_DRIVEITEMS_SKIP,								m_RadioBtnSkip);
    DDX_Control(pDX, IDC_DLG_DRIVEITEMS_OVERWRITE,							m_RadioBtnOverwrite);

    DDX_Control(pDX, IDC_DLG_DOWNLOAD_DRIVEITEMS_GROUP,						m_GroupBox);

    DDX_Control(pDX, IDC_DLG_DOWNLOAD_DRIVEITEMS_OPEN_FOLDER_AFTER_DOWNLOAD, m_ChkOpenAfterDownload);
    DDX_Control(pDX, IDC_DLG_DRIVEITEMS_BUTTONLINE,                         m_ButtonLine);
    DDX_Control(pDX, IDOK,													m_BtnOk);
    DDX_Control(pDX, IDCANCEL,												m_BtnCancel);
}

BOOL DlgDownloadDriveItems::OnInitDialogSpecific()
{
    SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_1, _YLOC("Download Files")).c_str());

    m_BtnChooseFolder.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_2, _YLOC("Select Destination")).c_str());

    m_FolderPath = *DownloadFolderSetting().Get();
	m_TxtFolderPath.SetWindowText(GetFolderPath().c_str());
    m_TxtFolderPath.SetBkColor(RGB(255, 255, 255));

    m_RadioBtnCreateNew.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_5, _YLOC("Create New")).c_str());
    m_RadioBtnCreateNew.SetBkColor(RGB(255, 255, 255));

    m_RadioBtnOverwrite.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_6, _YLOC("Overwrite")).c_str());
    m_RadioBtnOverwrite.SetBkColor(RGB(255, 255, 255));

    m_RadioBtnSkip.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_7, _YLOC("Skip")).c_str());
    m_RadioBtnSkip.SetBkColor(RGB(255, 255, 255));
    
    if (g_FileExistsAction == FileExistsAction::Append)
        m_RadioBtnCreateNew.SetCheck(BST_CHECKED);
    else if (g_FileExistsAction == FileExistsAction::Overwrite)
        m_RadioBtnOverwrite.SetCheck(BST_CHECKED);
    else if (g_FileExistsAction == FileExistsAction::Skip)
        m_RadioBtnSkip.SetCheck(BST_CHECKED);
    m_FileExistsAction = g_FileExistsAction;

	m_CheckUseOwnerName.SetWindowText(_T("Use Owner Display Name"));
    m_CheckUseOwnerName.SetCheck(g_UserOwnerName? BST_CHECKED : BST_UNCHECKED);
    m_CheckUseOwnerName.SetBkColor(RGB(255, 255, 255));

	m_ChkPreserveHierarchy.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_3, _YLOC("Preserve Hierarchy")).c_str());
    m_ChkPreserveHierarchy.SetCheck(m_PreserveHierarchy ? BST_CHECKED : BST_UNCHECKED);
    m_ChkPreserveHierarchy.SetBkColor(RGB(255, 255, 255));

    m_ChkOpenAfterDownload.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_4, _YLOC("Open folder after download")).c_str());
    m_ChkOpenAfterDownload.SetCheck(m_OpenAfterDownload ? BST_CHECKED : BST_UNCHECKED);
    m_ChkOpenAfterDownload.SetBkColor(RGB(255, 255, 255));

    m_GroupBox.SetWindowText(YtriaTranslate::Do(DlgDownloadDriveItems_OnInitDialogSpecific_8, _YLOC("If destination file already exists")).c_str());
    m_GroupBox.SetBkColor(RGB(255, 255, 255));

    m_ButtonLine.SetWindowText(_YTEXT(""));
    m_ButtonLine.SetBkColor(RGB(240, 240, 240));

	AddAnchor(m_BtnChooseFolder, CSize(100, 0), CSize(100, 0));
	AddAnchor(m_TxtFolderPath, CSize(0, 0), CSize(100, 0));
	AddAnchor(m_RadioBtnCreateNew, CSize(0, 0), CSize(33, 0));
	AddAnchor(m_RadioBtnOverwrite, CSize(33, 0), CSize(67, 0));
	AddAnchor(m_RadioBtnSkip, CSize(67, 0), CSize(100, 0));
    AddAnchor(m_GroupBox, CSize(0, 0), CSize(100, 0));
    AddAnchor(m_CheckUseOwnerName, CSize(0, 0), CSize(100, 0));
    AddAnchor(m_ChkPreserveHierarchy, CSize(0, 0), CSize(100, 0));
    AddAnchor(m_ChkOpenAfterDownload, CSize(0, 0), CSize(100, 0));
    AddAnchor(m_BtnOk, CSize(100, 100), CSize(100, 100));
    AddAnchor(m_BtnCancel, CSize(100, 100), CSize(100, 100));
    AddAnchor(m_ButtonLine, CSize(0, 100), CSize(100, 100));

    SetBkColor(RGB(255, 255, 255));

    return TRUE;
}

void DlgDownloadDriveItems::OnOK()
{
    CString folderPath;
    m_TxtFolderPath.GetWindowText(folderPath);
	m_FolderPath = FileUtil::MakeValidFilePath(folderPath.GetBuffer(), _YTEXT(""));
	if (m_FolderPath.empty())
    {
        YCodeJockMessageBox dlg(this, DlgMessageBox::eIcon_Error, YtriaTranslate::Do(DlgDownloadDriveItems_OnOK_1, _YLOC("Error")).c_str(), YtriaTranslate::Do(DlgDownloadDriveItems_OnOK_2, _YLOC("Please choose a valid destination folder.")).c_str(), _YTEXT(""), { { IDOK, YtriaTranslate::Do(DlgDownloadDriveItems_OnOK_3, _YLOC("OK")).c_str() } });
        dlg.DoModal();
	}
	else
	{
        DownloadFolderSetting().Set(m_FolderPath);

		m_OpenAfterDownload = m_ChkOpenAfterDownload.GetCheck() == BST_CHECKED;

		ResizableDialog::OnOK();
	}
}

void DlgDownloadDriveItems::OnSelectFolder()
{
    YFileDialog folderChooser(GetFolderPath().c_str(), 0, this);
    if (folderChooser.DoModal() == IDOK)
    {
        const CString folderPath = folderChooser.GetPathName()/*GetFolderPath()*/; //Bug #190201.RL.0099CE
        m_TxtFolderPath.SetWindowText(folderPath);
        g_LastFolderPath = GetFolderPath();
    }
}

void DlgDownloadDriveItems::OnPreserveHierarchyChanged()
{
    m_PreserveHierarchy = m_ChkPreserveHierarchy.GetCheck() == BST_CHECKED;
    g_PreserveHierarchy = m_PreserveHierarchy;
}

void DlgDownloadDriveItems::OnUseOwnerNameChanged()
{
	m_UseOwnerName  = m_CheckUseOwnerName.GetCheck() == BST_CHECKED;
	g_UserOwnerName = m_UseOwnerName;
}

void DlgDownloadDriveItems::OnOpenAfterDownloadChanged()
{
    m_OpenAfterDownload = m_ChkOpenAfterDownload.GetCheck() == BST_CHECKED;
    g_OpenAfterDownload = m_OpenAfterDownload;
}

void DlgDownloadDriveItems::OnSelectAppend()
{
    m_FileExistsAction = FileExistsAction::Append;
    g_FileExistsAction = m_FileExistsAction;
}

void DlgDownloadDriveItems::OnSelectSkip()
{
    m_FileExistsAction = FileExistsAction::Skip;
    g_FileExistsAction = m_FileExistsAction;
}

void DlgDownloadDriveItems::OnSelectOverwrite()
{
    m_FileExistsAction = FileExistsAction::Overwrite;
    g_FileExistsAction = m_FileExistsAction;
}

////////////////////// Automation

void DlgDownloadDriveItems::setAutomationFieldMap()
{
	addAutomationFieldMap(AutomationConstant::val().m_ParamNameFilePath,	&m_TxtFolderPath);
	addAutomationFieldMap(g_Hierarchy,										&m_ChkPreserveHierarchy);
    addAutomationFieldMap(g_OwnerName,										&m_CheckUseOwnerName);
	addAutomationFieldMap(g_OpenFolderAfterDownload,						&m_ChkOpenAfterDownload);
	//addAutomationFieldMap(AutomationConstant::val().m_Click,				&m_BtnChooseFolder);

	addAutomationFieldMap(g_CreateNew,	&m_RadioBtnCreateNew);
	addAutomationFieldMap(g_Overwrite,	&m_RadioBtnOverwrite);
	addAutomationFieldMap(g_Skip,		&m_RadioBtnSkip);

	fieldAutomationOverridesReadOnly(&m_TxtFolderPath);
}

void DlgDownloadDriveItems::setAutomationRadioButtonGroups()
{
	addAutomationRadioButtonToGroup(g_IfFileExists, &m_RadioBtnCreateNew);
	addAutomationRadioButtonToGroup(g_IfFileExists, &m_RadioBtnOverwrite);
	addAutomationRadioButtonToGroup(g_IfFileExists, &m_RadioBtnSkip);
}

void  DlgDownloadDriveItems::setAutomationListChoicesGroups()
{
}

void DlgDownloadDriveItems::automationSetParamPostProcess()
{
	// cf revision 128097
// 	CString fp;
// 	m_TxtFolderPath.GetWindowText(fp);
// 	m_FolderPath = fp;
}