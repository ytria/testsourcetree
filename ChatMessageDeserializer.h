#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessChatMessage.h"

class ChatMessageDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessChatMessage>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

