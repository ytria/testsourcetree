#include "MailFolderRequester.h"
#include "MailFolderDeserializer.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

MailFolderRequester::MailFolderRequester(const wstring& p_UserId, const wstring& p_FolderName, const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_UserId(p_UserId)
	, m_FolderName(p_FolderName)
	, m_Logger(p_Logger)
{
}

TaskWrapper<void> MailFolderRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<MailFolderDeserializer>();

	web::uri_builder uri(_YTEXT("users"));
	uri.append_path(m_UserId);
	uri.append_path(_YTEXT("mailFolders"));
	uri.append_path(m_FolderName);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, uri.to_uri(), false, m_Logger, httpLogger, p_TaskData);
}

BusinessMailFolder& MailFolderRequester::GetData()
{
	return m_Deserializer->GetData();
}
