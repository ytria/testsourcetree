#include "DlgMailfoldersModuleOptions.h"

void DlgMailfoldersModuleOptions::generateJSONScriptData()
{
	initMain(_YTEXT("DlgMailfoldersModuleOptions"));

	getGenerator().addIconTextField(_YTEXT("iconText"), YtriaTranslate::Do(DlgMessageModuleOptions_generateJSONScriptData_44, _YLOC("Limit the number of messages to load by selecting a date/time range. You can request more later.")).c_str(), _YTEXT("fas fa-calendar-alt"));
	addCutOffEditor(_T("Get messages received within: "), _T("Received after (excluding)"));
	/*getGenerator().addIconTextField(_YTEXT("iconText2"), _T("Refine your selection"), _YTEXT("fas fa-search"));
	addFilterEditor();*/

	getGenerator().addApplyButton(YtriaTranslate::Do(CDlg_OnInitDialog_2, _YLOC("Ok")).c_str());
	getGenerator().addCancelButton(YtriaTranslate::Do(DlgEditFields_OnInitDialog_3, _YLOC("Cancel")).c_str());
}

wstring DlgMailfoldersModuleOptions::getDialogTitle() const
{
	return _T("Load Mail Folders - Options");
}
