#pragma once

#include "IGraphSessionCreator.h"

class AppGraphSessionCreator : public IGraphSessionCreator
{
public:
	AppGraphSessionCreator(const wstring& p_Tenant, const wstring& p_Username, const wstring& p_Password, const wstring& p_AppRefName);

	std::shared_ptr<MSGraphSession> Create();

	const wstring& GetTenant() const;
	const wstring& GetClientId() const;
	const wstring& GetClientSecret() const;
	const wstring& GetApplicationName() const;

private:
	wstring m_Tenant;
	wstring m_ClientId;
	wstring m_ClientSecret;
	wstring m_ApplicationRefName;
};

