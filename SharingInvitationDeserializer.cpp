#include "SharingInvitationDeserializer.h"

#include "IdentitySetDeserializer.h"
#include "JsonSerializeUtil.h"

void SharingInvitationDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("email"), m_Data.Email, p_Object);

    {
        IdentitySetDeserializer isd;
        if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("invitedBy"), p_Object))
            m_Data.InvitedBy = isd.GetData();
    }

	JsonSerializeUtil::DeserializeBool(_YTEXT("signInRequired"), m_Data.SignInRequired, p_Object);
}
