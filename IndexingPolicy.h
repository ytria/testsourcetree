#pragma once

#include "IncludedPath.h"
#include "PartitionKey.h"

namespace Cosmos
{
    class IndexingPolicy
    {
    public:
        boost::YOpt<bool> Automatic;
        boost::YOpt<PooledString> IndexingMode;
        boost::YOpt<vector<Cosmos::IncludedPath>> IncludedPaths;
        boost::YOpt<Cosmos::PartitionKey> PartitionKey;
    };
}
