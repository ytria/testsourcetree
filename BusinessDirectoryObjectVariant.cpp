#include "BusinessDirectoryObjectVariant.h"

bool BusinessDirectoryObjectVariant::IsBusinessDirectoryRole() const
{
	return m_BusinessDirectoryRole.is_initialized();
}

bool BusinessDirectoryObjectVariant::IsBusinessGroup() const
{
	return m_BusinessGroup.is_initialized();
}

bool BusinessDirectoryObjectVariant::IsBusinessOrgContact() const
{
	return m_BusinessOrgContact.is_initialized();
}

bool BusinessDirectoryObjectVariant::IsBusinessUser() const
{
	return m_BusinessUser.is_initialized();
}

const BusinessGroup& BusinessDirectoryObjectVariant::AsBusinessGroup() const
{
	ASSERT(IsBusinessGroup());
	if (IsBusinessGroup())
		return *m_BusinessGroup;
	static const BusinessGroup empty;
	return empty;
}

const BusinessOrgContact& BusinessDirectoryObjectVariant::AsBusinessOrgContact() const
{
	ASSERT(IsBusinessOrgContact());
	if (IsBusinessOrgContact())
		return *m_BusinessOrgContact;
	static const BusinessOrgContact empty;
	return empty;
}

const BusinessUser& BusinessDirectoryObjectVariant::AsBusinessUser() const
{
	ASSERT(IsBusinessUser());
	if (IsBusinessUser())
		return *m_BusinessUser;
	static const BusinessUser empty;
	return empty;
}

const BusinessDirectoryRole& BusinessDirectoryObjectVariant::AsBusinessDirectoryRole() const
{
	ASSERT(IsBusinessDirectoryRole());
	if (IsBusinessDirectoryRole())
		return *m_BusinessDirectoryRole;
	static const BusinessDirectoryRole empty;
	return empty;
}
