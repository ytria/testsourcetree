#include "EventStartCutOffCondition.h"

EventStartCutOffCondition::EventStartCutOffCondition(const wstring& p_DateCutOffIso8601)
	:m_DateCutOffIso8601(p_DateCutOffIso8601)
{
}

bool EventStartCutOffCondition::operator()(const web::json::value& p_Json) const
{
	bool isBeforeCutOff = false;

	static const wstring startField = _YTEXT("start");
	static const wstring dateTimeField = _YTEXT("dateTime");

	ASSERT(p_Json.has_object_field(startField));
	if (p_Json.has_object_field(startField))
	{
		const auto& dateTimeTimeZone = p_Json.as_object().at(startField);
		if (dateTimeTimeZone.has_string_field(dateTimeField))
			isBeforeCutOff = dateTimeTimeZone.as_object().at(dateTimeField).as_string() < m_DateCutOffIso8601;
	}

	return isBeforeCutOff;
}
