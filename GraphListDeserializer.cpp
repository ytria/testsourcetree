#include "GraphListDeserializer.h"

#include "ColumnDefinitionDeserializer.h"
#include "ContentTypeDeserializer.h"
#include "IdentitySetDeserializer.h"
#include "ItemReferenceDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "ListInfoDeserializer.h"
#include "ListItemDeserializer.h"
#include "SystemFacetDeserializer.h"

void GraphListDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);

    {
        IdentitySetDeserializer isd;
        if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("createdBy"), p_Object))
            m_Data.SetCreatedBy(isd.GetData());
    }

    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.m_CreatedDateTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.m_Description, p_Object);

    {
        ListDeserializer<BusinessListItem, ListItemDeserializer> lstItemDeserializer;
        if (JsonSerializeUtil::DeserializeAny(lstItemDeserializer, _YTEXT("items"), p_Object))
            m_Data.m_ListItems = std::move(lstItemDeserializer.GetData());
    }

    {
        IdentitySetDeserializer isd;
        if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("lastModifiedBy"), p_Object))
            m_Data.SetLastModifiedBy(isd.GetData());
    }

	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.m_LastModifiedDateTime, p_Object);

    {
        ItemReferenceDeserializer ird;
        if (JsonSerializeUtil::DeserializeAny(ird, _YTEXT("parentReference"), p_Object))
            m_Data.m_ParentReference = std::move(ird.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("webUrl"), m_Data.m_WebUrl, p_Object);

    {
        ListDeserializer<BusinessColumnDefinition, ColumnDefinitionDeserializer> colDefDeserializer;
        if (JsonSerializeUtil::DeserializeAny(colDefDeserializer, _YTEXT("columns"), p_Object))
            m_Data.m_Columns = std::move(colDefDeserializer.GetData());
    }

    {
        ListDeserializer<ContentType, ContentTypeDeserializer> cTypeDeserializer;
        if (JsonSerializeUtil::DeserializeAny(cTypeDeserializer, _YTEXT("contentTypes"), p_Object))
            m_Data.m_ContentTypes = std::move(cTypeDeserializer.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);

    {
        ListInfoDeserializer lid;
        if (JsonSerializeUtil::DeserializeAny(lid, _YTEXT("list"), p_Object))
            m_Data.m_ListInfo = std::move(lid.GetData());
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("eTag"), m_Data.m_ETag, p_Object);

    {
        SystemFacetDeserializer lid;
        if (JsonSerializeUtil::DeserializeAny(lid, _YTEXT("system"), p_Object))
            m_Data.m_System = std::move(lid.GetData());
    }
}
