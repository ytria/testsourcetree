#pragma once

#include "BasicPowerShellSession.h"

class IPowerShell;

class ExchangePowerShellSession : public BasicPowerShellSession
{
public:
	ExchangePowerShellSession(const wstring& p_DefaultDomain);
	~ExchangePowerShellSession();

	void AddScript(const wchar_t* p_Script, HWND p_Originator) override;

	void SetHasLiveSession(bool p_HasLiveSession);

	int64_t GetExchangeTokenExpiration() const;
	void SetExchangeTokenExpiration(int64_t p_Expiration);

	std::shared_ptr<InvokeResultWrapper> CreateExchangeToken(HWND p_Originator, bool p_Renew = false);

private:
	bool m_HasLiveSession;
	wstring m_DefaultDomain;
	int64_t m_ExchangeTokenExpiration;
};

