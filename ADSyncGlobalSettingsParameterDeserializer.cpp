#include "ADSyncGlobalSettingsParameterDeserializer.h"
#include "PSSerializeUtil.h"

void ADSyncGlobalSettingsParameterDeserializer::Deserialize(IPSObjectPropertyReader& p_Reader)
{
	m_SourceAnchor = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Value"));
}

void ADSyncGlobalSettingsParameterDeserializer::SetCount(size_t p_Count)
{
	ASSERT(p_Count != 1);
}

const wstring& ADSyncGlobalSettingsParameterDeserializer::GetSourceAnchor() const
{
	return m_SourceAnchor;
}
