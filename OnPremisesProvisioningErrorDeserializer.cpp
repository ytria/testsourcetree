#include "OnPremisesProvisioningErrorDeserializer.h"

#include "JsonSerializeUtil.h"

void OnPremisesProvisioningErrorDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("category"), m_Data.m_Category, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("occurredDateTime"), m_Data.m_OccurredDateTime, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("propertyCausingError"), m_Data.m_PropertyCausingError, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("value"), m_Data.m_Value, p_Object);
}
