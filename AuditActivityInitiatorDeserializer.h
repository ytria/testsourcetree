#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "AuditActivityInitiator.h"

class AuditActivityInitiatorDeserializer : public JsonObjectDeserializer, public Encapsulate<AuditActivityInitiator>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

