#pragma once

class SizeRange
{
public:
	SizeRange() = default;
	boost::YOpt<int32_t> m_MaximumSize; // In KB
	boost::YOpt<int32_t> m_MinimumSize; // in KB

	wstring ToString() const;
};