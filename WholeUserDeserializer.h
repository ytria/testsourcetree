#pragma once

#include "JsonObjectDeserializer.h"
#include "BusinessUser.h"
#include "Encapsulate.h"

class Sapio365Session;

class WholeUserDeserializer : public JsonObjectDeserializer
                            , public Encapsulate<BusinessUser>
{
public:
    WholeUserDeserializer(const std::shared_ptr<Sapio365Session>& p_Session);
    void DeserializeObject(const web::json::object& p_Object) override;

private:
    const std::shared_ptr<Sapio365Session>& m_Session;
};

