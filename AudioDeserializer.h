#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Audio.h"

class AudioDeserializer : public JsonObjectDeserializer, public Encapsulate<Audio>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

