#pragma once

#include "DlgFormsHTML.h"
#include "EducationClass.h"

class DlgClassEditHTML : public DlgFormsHTML
{
public:
	DlgClassEditHTML(vector<EducationClass>& p_Classes, DlgFormsHTML::Action p_Action, CWnd* p_Parent);

protected:
	void generateJSONScriptData() override;
	wstring getDialogTitle() const override;
	bool processPostedData(const IPostedDataTarget::PostedData& data) override;

private:
	using StringGetter = std::function<const boost::YOpt<PooledString>& (const EducationClass&)>;
	using StringSetter = std::function<void(EducationClass&, const boost::YOpt<PooledString>&)>;

	void addStringEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags);
	void addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues);

	bool hasProperty(const wstring& p_PropertyName) const;

	vector<EducationClass>& m_Classes;

	std::map<wstring, StringSetter> m_StringSetters;
};

