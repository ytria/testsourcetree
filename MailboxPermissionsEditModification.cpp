#include "MailboxPermissionsEditModification.h"
#include "GridMailboxPermissions.h"
#include "AccessRightsSorter.h"

const wstring MailboxPermissionsEditModification::g_Sep = _YTEXT(",");

MailboxPermissionsEditModification::MailboxPermissionsEditModification(GridMailboxPermissions& p_Grid, const row_pk_t& p_RowPk, const vector<wstring>& p_RightsToAdd, const vector<wstring>& p_RightsToRemove)
	: ModificationWithRequestFeedback(_YTEXT("")),
	m_Grid(p_Grid),
	m_RightsToAdd(p_RightsToAdd),
	m_RightsToRemove(p_RightsToRemove)
{
	m_RowKey = p_RowPk;
}

vector<Modification::ModificationLog> MailboxPermissionsEditModification::GetModificationLogs() const
{
	vector<Modification::ModificationLog> modLogs;
	modLogs.emplace_back(GridBackendUtil::ToString(GetRowKey()), GetObjectName(), _YTEXT("Edit mailbox permissions"), _YTEXT("Access Rights"), m_Permissions, GetState());
	return modLogs;
}

void MailboxPermissionsEditModification::Apply()
{
	auto& grid = m_Grid.get();
	auto row = grid.GetRowIndex().GetExistingRow(GetRowKey());
	if (nullptr != row)
	{
		m_OldAccessRights = grid.GetAccessRights(*row);
		m_MailboxOwner = row->GetField(grid.GetColOwnerId()).GetValueStr();
		m_User = row->GetField(grid.GetColPermissionsUser()).GetValueStr();
		m_Permissions = GetCombinedAccessRights(m_OldAccessRights);
		grid.SetAccessRights(*row, m_Permissions);

		auto childRows = grid.GetChildRows(row->GetParentRow(), true);
		for (auto childRow : childRows)
		{
			row_pk_t childPk;
			grid.GetRowPK(childRow, childPk);
			if (m_Grid.get().AreSimilarRows(childPk, GetRowKey()))
			{
				childRow->SetEditModified(true);
				grid.OnRowModified(childRow);
				m_SimilarRows.push_back(childPk);
			}
		}
		SetObjectName(row->GetField(m_Grid.get().GetColOwnerDisplayName()).GetValueStr() + wstring(_YTEXT(" -> ")) + m_User);
	}
	else
	{
		Revert();
	}
}

vector<wstring> MailboxPermissionsEditModification::GetCombinedAccessRights(const vector<wstring>& p_Current, const vector<wstring>& p_ToAdd, const vector<wstring>& p_ToRemove)
{
	vector<wstring> result = p_Current;

	bool changeOwner = false, changePermission = false, deleteItem = false, externalAccount = false, fullAccess = false, readPermission = false;
	for (const auto& toRemove : p_ToRemove)
		result.erase(std::remove_if(result.begin(), result.end(), [toRemove](const wstring& str) { return str == toRemove; }), result.end());
	for (const auto& toAdd : p_ToAdd)
		result.push_back(toAdd);

	std::sort(result.begin(), result.end(), AccessRightsSorter());

	return result;
}

vector<wstring> MailboxPermissionsEditModification::GetCombinedAccessRights(const vector<wstring>& p_Current) const
{
	return GetCombinedAccessRights(p_Current, m_RightsToAdd, m_RightsToRemove);
}

wstring MailboxPermissionsEditModification::GetRightsToAdd() const
{
	return Str::implode(m_RightsToAdd, g_Sep);
}

wstring MailboxPermissionsEditModification::GetRightsToRemove() const
{
	return Str::implode(m_RightsToRemove, g_Sep);
}

const wstring& MailboxPermissionsEditModification::GetMailboxOwner() const
{
	return m_MailboxOwner;
}

const wstring& MailboxPermissionsEditModification::GetUser() const
{
	return m_User;
}

void MailboxPermissionsEditModification::AddToList(wstring& p_List, const wstring& p_ToAdd)
{
	vector<wstring> list = Str::explodeIntoVector(p_List, g_Sep, false, true);
	list.push_back(p_ToAdd);

	p_List = AccessRightsFormatter(Str::implode(list, g_Sep)).Format();
}

void MailboxPermissionsEditModification::RemoveFromList(wstring& p_List, const wstring& p_ToRemove)
{
	vector<wstring> list = Str::explodeIntoVector(p_List, g_Sep, false, true);
	list.erase(std::remove(list.begin(), list.end(), p_ToRemove), list.end());

	p_List = AccessRightsFormatter(Str::implode(list, g_Sep)).Format();
}

void MailboxPermissionsEditModification::Merge(const MailboxPermissionsEditModification& p_Mod)
{
	const vector<wstring>& incomingAdds = p_Mod.m_RightsToAdd;
	const vector<wstring>& incomingRemoves = p_Mod.m_RightsToRemove;

	for (auto itt = incomingAdds.begin(); itt != incomingAdds.end(); ++itt)
	{
		auto ittInOwnAddList = std::find(m_RightsToAdd.begin(), m_RightsToAdd.end(), *itt);
		auto ittInOwnRemoveList = std::find(m_RightsToRemove.begin(), m_RightsToRemove.end(), *itt);

		if (ittInOwnAddList == m_RightsToAdd.end() && ittInOwnRemoveList == m_RightsToRemove.end())
			m_RightsToAdd.push_back(*itt);
		else if (ittInOwnAddList == m_RightsToAdd.end() && ittInOwnRemoveList != m_RightsToRemove.end())
			m_RightsToAdd.erase(std::remove(m_RightsToAdd.begin(), m_RightsToAdd.end(), *itt), m_RightsToAdd.end());
	}
	std::sort(m_RightsToAdd.begin(), m_RightsToAdd.end(), AccessRightsSorter());

	for (auto itt = incomingRemoves.begin(); itt != incomingRemoves.end(); ++itt)
	{
		auto ittInOwnAddList = std::find(m_RightsToAdd.begin(), m_RightsToAdd.end(), *itt);
		auto ittInOwnRemoveList = std::find(m_RightsToRemove.begin(), m_RightsToRemove.end(), *itt);

		if (ittInOwnRemoveList == m_RightsToRemove.end() && ittInOwnAddList == m_RightsToAdd.end())
			m_RightsToRemove.push_back(*itt);
		else if (ittInOwnRemoveList == m_RightsToRemove.end() && ittInOwnAddList != m_RightsToAdd.end())
			m_RightsToAdd.erase(std::remove(m_RightsToAdd.begin(), m_RightsToAdd.end(), *itt), m_RightsToAdd.end());
	}
	std::sort(m_RightsToRemove.begin(), m_RightsToRemove.end(), AccessRightsSorter());
}

void MailboxPermissionsEditModification::Revert()
{
	auto& grid = m_Grid.get();
	auto row = grid.GetRowIndex().GetExistingRow(GetRowKey());
	if (nullptr != row)
	{
		grid.SetAccessRights(*row, m_OldAccessRights);
		grid.OnRowNothingToSave(row, false);

		for (const auto& similarRowPk : m_SimilarRows)
		{
			auto similarRow = m_Grid.get().GetRowIndex().GetExistingRow(similarRowPk);
			ASSERT(nullptr != similarRow);
			if (nullptr != similarRow)
				grid.OnRowNothingToSave(similarRow, false);
		}
	}
}

