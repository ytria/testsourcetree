#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ApiApplication.h"

class ApiApplicationDeserializer : public JsonObjectDeserializer, public Encapsulate<ApiApplication>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

