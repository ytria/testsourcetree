#pragma once

#include "IKrypter.h"
#include "SQLiteEngine.h"

class Sapio365Session;
struct O365Krypter : public IKrypter
{
public:
	O365Krypter(std::shared_ptr<Sapio365Session> p_Session);
	O365Krypter(std::shared_ptr<const Sapio365Session> p_Session);
	virtual std::string	getKrypt() const override;

	virtual void SetDumpDebugKey(const std::string& p_DebugKey) const override;

	const std::shared_ptr<const Sapio365Session> GetSession() const;

protected:
	void setSession(std::shared_ptr<Sapio365Session> p_Session);

private:
	std::shared_ptr<const Sapio365Session> m_Session;
	mutable std::string m_Krypt;
	mutable std::string m_DumpDebugKey;
};

struct O365MainFrameKrypter : public O365Krypter
{
public:
	O365MainFrameKrypter();
};

class O365SQLiteEngine : public SQLiteEngine
{
public:
	O365SQLiteEngine(const wstring& p_DBfile);

	void SetCloudSyncDate(std::shared_ptr<const Sapio365Session> p_Session);

	virtual const SQLiteUtil::WhereClause GetSelectDefaultWhereClause(const IKrypter& p_Krypter, const SQLiteUtil::Table& p_Table) const override;// always filter on current tenant (original domain); other tenant data cannot be decrypted anyway

protected:
	O365SQLiteEngine(const wstring& p_DBfile, const std::string& p_EncryptionKeySQLite, const std::string& p_EncryptionKeyYtria);// debug for SQL dump
};
