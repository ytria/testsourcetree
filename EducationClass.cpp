#include "EducationClass.h"

RTTR_REGISTRATION
{
	using namespace rttr;
	
registration::class_<EducationClass>("EducationClass") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Class"))))
	.constructor()(policy::ctor::as_object)
	.property("classDescription", &EducationClass::m_Description)
	.property("displayName", &EducationClass::m_DisplayName)
	.property("classMailNickname", &EducationClass::m_MailNickname)
	// IdentitySet
	.property("classCode", &EducationClass::m_ClassCode)
	.property("classExternalId", &EducationClass::m_ExternalId)
	.property("classExternalName", &EducationClass::m_ExternalName)
	.property("classExternalSource", &EducationClass::m_ExternalSource);
	// Term
}

bool EducationClass::operator==(const EducationClass& p_Other) const
{
	return GetID() == p_Other.GetID();
}

const boost::YOpt<PooledString>& EducationClass::GetDescription() const
{
	return m_Description;
}

void EducationClass::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
	m_Description = p_Val;
}

const boost::YOpt<PooledString>& EducationClass::GetDisplayName() const
{
	return m_DisplayName;
}

void EducationClass::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}

const boost::YOpt<PooledString>& EducationClass::GetMailNickname() const
{
	return m_MailNickname;
}

void EducationClass::SetMailNickname(const boost::YOpt<PooledString>& p_Val)
{
	m_MailNickname = p_Val;
}

const boost::YOpt<PooledString>& EducationClass::GetClassCode() const
{
	return m_ClassCode;
}

void EducationClass::SetClassCode(const boost::YOpt<PooledString>& p_Val)
{
	m_ClassCode = p_Val;
}

const boost::YOpt<PooledString>& EducationClass::GetExternalId() const
{
	return m_ExternalId;
}

void EducationClass::SetExternalId(const boost::YOpt<PooledString>& p_Val)
{
	m_ExternalId = p_Val;
}

const boost::YOpt<PooledString>& EducationClass::GetExternalName() const
{
	return m_ExternalName;
}

void EducationClass::SetExternalName(const boost::YOpt<PooledString>& p_Val)
{
	m_ExternalName = p_Val;
}

const boost::YOpt<PooledString>& EducationClass::GetExternalSource() const
{
	return m_ExternalSource;
}

void EducationClass::SetExternalSource(const boost::YOpt<PooledString>& p_Val)
{
	m_ExternalSource = p_Val;
}

bool EducationClass::operator<(const EducationClass& p_Other) const
{
	return GetID() < p_Other.GetID();
}
