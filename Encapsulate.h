#pragma once

template <typename T>
class Encapsulate
{
public:
    const T& GetData() const
    {
        return m_Data;
    }

    T& GetData()
    {
        return m_Data;
    }

protected:
    T m_Data;
};
