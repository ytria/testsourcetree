#pragma once

#include "IPropertySetBuilder.h"

class MessageInMailfolderPropertySet : public IPropertySetBuilder
{
public:
	vector<rttr::property> GetPropertySet() const override;
	vector<PooledString> GetStringPropertySet() const override;
};

