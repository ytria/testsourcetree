#include "SignInDeserializer.h"

#include "JsonSerializeUtil.h"
#include "AppliedConditionalAccessPolicyDeserializer.h"
#include "DeviceDetailDeserializer.h"
#include "SignInLocationDeserializer.h"
#include "SignInStatusDeserializer.h"

void SignInDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("appDisplayName"), m_Data.m_AppDisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("appId"), m_Data.m_AppId, p_Object);
	{
		AppliedConditionalAccessPolicyDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("appliedConditionalAccessPolicy"), p_Object))
			m_Data.m_AppliedConditionalAccessPolicy = d.GetData();
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("clientAppUsed"), m_Data.m_ClientAppUsed, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("conditionalAccessStatus"), m_Data.m_ConditionalAccessStatus, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("correlationId"), m_Data.m_CorrelationId, p_Object);
	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.m_CreatedDateTime, p_Object);
	{
		DeviceDetailDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("deviceDetail"), p_Object))
			m_Data.m_DeviceDetail = d.GetData();
	}
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	JsonSerializeUtil::DeserializeString(_YTEXT("ipAddress"), m_Data.m_IpAddress, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("isInteractive"), m_Data.m_IsInteractive, p_Object);
	{
		SignInLocationDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("location"), p_Object))
			m_Data.m_Location = d.GetData();
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("resourceDisplayName"), m_Data.m_ResourceDisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("resourceId"), m_Data.m_ResourceId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("riskDetail"), m_Data.m_RiskDetail, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("riskEventTypes"), m_Data.m_RiskEventTypes, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("riskLevelAggregated"), m_Data.m_RiskLevelAggregated, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("riskLevelDuringSignIn"), m_Data.m_RiskLevelDuringSignIn, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("riskState"), m_Data.m_RiskState, p_Object);
	{
		SignInStatusDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("status"), p_Object))
			m_Data.m_Status = d.GetData();
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("userDisplayName"), m_Data.m_UserDisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("userId"), m_Data.m_UserId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("userPrincipalName"), m_Data.m_UserPrincipalName, p_Object);
}
