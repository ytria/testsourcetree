#pragma once

#include "IRequester.h"

#include "IPageRequestLogger.h"
#include "ODataFilter.h"

template <class T, class U>
class ValueListDeserializer;

class BusinessUser;
class UserDeserializer;
class IPropertySetBuilder;
class MsGraphHttpRequestLogger;
class PaginatedRequestResults;

class UserListRequester : public IRequester
{
public:
	class UserListSubRequester : public IRequester
	{
	public:
		UserListSubRequester();
		void SetParentRequester(UserListRequester* p_ParentRequester);
		void SetProperties(const vector<rttr::property>& p_Properties);
		void SetLogger(const std::shared_ptr<MsGraphHttpRequestLogger>& p_Logger);

	protected:
		UserListRequester* m_Requester;
		vector<rttr::property> m_Properties;
		std::shared_ptr<MsGraphHttpRequestLogger> m_Logger;
	};

	class RegularRequester : public UserListSubRequester
	{
	public:
		TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	};

	class InitialDeltaRequester : public UserListSubRequester
	{
	public:
		InitialDeltaRequester();
		TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

		wstring GetNextDeltalink() const;

	private:
		std::shared_ptr<wstring> m_NextDeltaLink;
	};

	class DeltaLinkRequester : public UserListSubRequester
	{
	public:
		DeltaLinkRequester(const wstring& p_DeltaLink);
		TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

		wstring GetNextDeltalink() const;

	private:
		std::shared_ptr<wstring> m_NextDeltaLink;
		wstring m_DeltaLink;
	};

    UserListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger, std::unique_ptr<UserListSubRequester> p_SpecificRequester);

    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    vector<BusinessUser>& GetData();

    void SetCustomFilter(const ODataFilter& p_CustomFilter);

private:
    std::shared_ptr<ValueListDeserializer<BusinessUser, UserDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;
    
    std::unique_ptr<UserListSubRequester> m_SubRequester;

    ODataFilter m_CustomFilter;
};

