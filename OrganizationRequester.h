#pragma once

#include "IRequester.h"
#include "IPageRequestLogger.h"

template <class T, class U>
class ValueListDeserializer;

class Organization;
class OrganizationDeserializer;
class PaginatedRequestResults;

class OrganizationRequester : public IRequester
{
public:
	OrganizationRequester(bool p_UseRoleSession, const std::shared_ptr<IPageRequestLogger>& p_Logger);

public:
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	TaskWrapper<void> Send(const std::shared_ptr<const Sapio365Session>& p_Session, std::shared_ptr<MSGraphSession> p_GraphSession, YtriaTaskData p_TaskData);

    const vector<Organization>& GetData() const;

private:
    std::shared_ptr<ValueListDeserializer<Organization, OrganizationDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;
	
	bool m_UseRoleSession;
};

