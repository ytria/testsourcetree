#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "LicenseAssignmentState.h"

class LicenseAssignmentStateDeserializer : public JsonObjectDeserializer, public Encapsulate<LicenseAssignmentState>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};
