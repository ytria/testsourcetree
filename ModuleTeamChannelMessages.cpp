#include "ModuleTeamChannelMessages.h"

#include "BusinessObjectManager.h"
#include "FrameTeamChannelMessages.h"
#include "GraphCache.h"
#include "ModuleUtil.h"
#include "O365AdminUtil.h"
#include "YCallbackMessage.h"
#include "ChannelMessagesRequester.h"
#include "MultiObjectsPageRequestLogger.h"
#include "YSafeCreateTask.h"

void ModuleTeamChannelMessages::executeImpl(const Command& p_Command)
{
    switch (p_Command.GetTask())
	{
    case Command::ModuleTask::List:
		showMessages(p_Command);
		break;
    case Command::ModuleTask::ListAttachments:
		showAttachments(p_Command);
		break;
    case Command::ModuleTask::DownloadAttachments:
		downloadAttachments(p_Command);
	    break;
    case Command::ModuleTask::ViewItemAttachment:
		showItemAttachment(p_Command);
	    break;
    case Command::ModuleTask::DeleteAttachment:
		deleteAttachments(p_Command);
	    break;
	default:
		ASSERT(false);
    }
}

void ModuleTeamChannelMessages::showMessages(const Command& p_Command)
{
	auto		p_Action	= p_Command.GetAutomationAction();
    const auto& info		= p_Command.GetCommandInfo();
    const auto&	subIds		= info.GetSubIds();

    RefreshSpecificData refreshSpecificData;

	auto tcmFrame = dynamic_cast<FrameTeamChannelMessages*>(info.GetFrame());

	using DataType = std::map<PooledString, PooledString>;
	DataType channelNames;

	if (info.Data().is_type<DataType>()) // Only happens when the window is first opened
		channelNames = info.Data().get_value<DataType>();
	else if (p_Command.GetCommandInfo().Data().is_type<RefreshSpecificData>()) // Only during a partial refresh
		refreshSpecificData = p_Command.GetCommandInfo().Data().get_value<RefreshSpecificData>();

	if (channelNames.empty())
	{
		ASSERT(nullptr != tcmFrame);
		if (nullptr != tcmFrame)
			channelNames = tcmFrame->GetChannelNames();
	}

	const auto		origin			= info.GetOrigin();
	auto			sourceWindow	= p_Command.GetSourceWindow();

	auto sourceCWnd = sourceWindow.GetCWnd();

	auto frame = tcmFrame;
	const bool isUpdate = nullptr != tcmFrame;

	if (nullptr == tcmFrame)
	{
		if (FrameTeamChannelMessages::CanCreateNewFrame(true, sourceCWnd, p_Action))
		{
			ModuleCriteria moduleCriteria;
			moduleCriteria.m_Origin			= origin;
			moduleCriteria.m_UsedContainer	= ModuleCriteria::UsedContainer::SUBIDS;
			moduleCriteria.m_SubIDs			= info.GetSubIds();
			moduleCriteria.m_Privilege		= info.GetRBACPrivilege();

			if (ShouldCreateFrame<FrameTeamChannelMessages>(moduleCriteria, sourceWindow, p_Action) && sourceWindow.UserConfirmation(p_Action))
			{
				CWaitCursor _;

				auto newFrame = new FrameTeamChannelMessages(p_Command.GetLicenseContext(), p_Command.GetSessionIdentifier(), YtriaTranslate::Do(ModuleTeamChannelMessages_showMessages_1, _YLOC("Team Channel Messages")).c_str(), *this, sourceWindow.GetHistoryMode(), sourceWindow.GetCFrameWndForHistory());
				newFrame->InitModuleCriteria(moduleCriteria);
				AddGridFrame(newFrame);
				newFrame->Erect();
				frame = newFrame;
			}
		}
	}

	if (nullptr != frame)
	{
		auto taskData = addFrameTask(YtriaTranslate::Do(ModuleTeamChannelMessages_showMessages_2, _YLOC("Show Team Channel Messages")).c_str(), frame, p_Command, !isUpdate);

        /*auto gridTeamChannelMessages = dynamic_cast<GridTeamChannelMessages*>(&frame->GetGrid());
        std::vector<GridBackendRow*> rowsWithMoreLoaded;
        gridTeamChannelMessages->GetRowsWithMoreLoaded(rowsWithMoreLoaded);*/

        //auto attachmentsRequestInfo = gridTeamChannelMessages->GetAttachmentsRequestInfo(rowsWithMoreLoaded, {});
		YSafeCreateTask([this, subIds, taskData, p_Action/*, gridTeamChannelMessages*/, refreshSpecificData, hwnd = frame->GetSafeHwnd(), isUpdate, origin/*, attachmentsRequestInfo*/, bom = frame->GetBusinessObjectManager(), channelNames]()
		{
            // Get posts
			const bool partialRefresh = !refreshSpecificData.m_Criteria.m_SubSubIDs.empty();
			const auto& ids = partialRefresh ? refreshSpecificData.m_Criteria.m_SubIDs : subIds;

			auto messages = retrieveMessages(bom, ids, channelNames, taskData);

            // Get attachments
			/*ModuleBase::AttachmentsContainer attachments;
            bool showAttachments = false;
            if (!attachmentsRequestInfo.empty())
            {
                std::vector<AttachmentsInfo::IDs> ids;
                for (const auto& postInfo : attachmentsRequestInfo)
                    ids.emplace_back(postInfo.GroupId, postInfo.ConversationId, postInfo.ThreadId, postInfo.PostId, PooledString());

				attachments = retrieveAttachments(bom, ids, p_Origin, taskData);
                showAttachments = true;
            }*/

			YDataCallbackMessage<O365DataMap<BusinessGroup, std::map<BusinessChannel, vector<BusinessChatMessage>>>>::DoPost(messages, [hwnd, partialRefresh, taskData, p_Action, channelNames/*, attachments, showAttachments*/, isUpdate, isCanceled = taskData.IsCanceled()](const O365DataMap<BusinessGroup, std::map<BusinessChannel, vector<BusinessChatMessage>>>& p_Messages)
			{
				bool shouldFinishTask = true;
				auto frame = ::IsWindow(hwnd) ? dynamic_cast<FrameTeamChannelMessages*>(CWnd::FromHandle(hwnd)) : nullptr;
				if (ShouldBuildView(hwnd, isCanceled, taskData, false))
				{
					ASSERT(nullptr != frame);

					frame->GetGrid().SetProcessText(_YFORMAT(L"Inserting channels and messages for %s teams...", Str::getStringFromNumber(p_Messages.size()).c_str()));
					frame->GetGrid().ClearLog(taskData.GetId());
                    //frame->GetGrid().ClearStatus();
					{
						CWaitCursor _;
						frame->ShowMessages(p_Messages, true, !partialRefresh);
						frame->SetChannelNames(channelNames); // Store this for next refresh
					}
                    //if (showAttachments)
                    //    frame->ViewAttachments(attachments);
					if (!isUpdate)
					{
						frame->UpdateContext(taskData, hwnd);
						shouldFinishTask = false;
					}
				}

				if (shouldFinishTask)
				{
					// Because we didn't (and can't) call UpdateContext here.
					ModuleBase::TaskFinished(frame, taskData, p_Action);
				}
			});
		});
	}
	else
	{
		// No error: when the user wants to load the same module as the next frame, next frame is restored and refreshed (see ShouldCreateFrame<>()).
		ASSERT(nullptr == p_Action);
		SetAutomationGreenLight(p_Action, _YTEXT(""));
	}
}

void ModuleTeamChannelMessages::showAttachments(const Command& p_Command)
{
	auto p_Action		= p_Command.GetAutomationAction();
	/*CommandInfo info = p_Command.GetCommandInfo();

	if (info.Data().is_type<std::set<ModuleUtil::PostMetaInfo>>())
	{
		auto p_PostsInfo	= info.Data().get_value<std::set<ModuleUtil::PostMetaInfo>>();
		auto p_Frame = dynamic_cast<FrameTeamChannelMessages*>(info.GetFrame());

		std::vector<AttachmentsInfo::IDs> ids;
		for (const auto& postInfo : p_PostsInfo)
			ids.emplace_back(postInfo.GroupId, postInfo.ConversationId, postInfo.ThreadId, postInfo.PostId, PooledString());

		ModuleBase::showAttachments(p_Frame->GetBusinessObjectManager(), ids, p_Frame, Origin::Conversations, p_Command);
	}
	else*/
	{
		ASSERT(false);
		SetAutomationGreenLight(p_Action, p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2486")).c_str());
	}
}

void ModuleTeamChannelMessages::downloadAttachments(const Command& p_Command)
{
	auto p_AutomationAction = p_Command.GetAutomationAction();

    /*CommandInfo info = p_Command.GetCommandInfo();
	auto p_ExistingFrame = dynamic_cast<FrameTeamChannelMessages*>(info.GetFrame());

	if (info.Data().is_type<AttachmentsInfo>())
	{
		auto& p_AttachmentInfos = info.Data().get_value<AttachmentsInfo>();
		ModuleBase::downloadAttachments(p_ExistingFrame->GetBusinessObjectManager(), p_AttachmentInfos, p_ExistingFrame, Origin::Conversations, p_Command);
	}
	else*/
	{
		SetAutomationGreenLight(p_AutomationAction, p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2487")).c_str());
	}
}

void ModuleTeamChannelMessages::showItemAttachment(const Command& p_Command)
{			
	auto p_AutomationAction = p_Command.GetAutomationAction();
	
	/*CommandInfo info	= p_Command.GetCommandInfo();
	auto p_Frame = dynamic_cast<FrameTeamChannelMessages*>(info.GetFrame());

	ASSERT(info.Data().is_type<AttachmentsInfo::IDs>());
	if (info.Data().is_type<AttachmentsInfo::IDs>())
		ModuleBase::showItemAttachment(p_Frame->GetBusinessObjectManager(), info.Data().get_value<AttachmentsInfo::IDs>(), info.GetOrigin(), p_Frame, p_Command);
	else*/
		SetAutomationGreenLight(p_AutomationAction, p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2488")).c_str());

}

void ModuleTeamChannelMessages::deleteAttachments(const Command& p_Command)
{
    CommandInfo info = p_Command.GetCommandInfo();
    AutomationAction* action = p_Command.GetAutomationAction();
  //  auto frame = dynamic_cast<FrameTeamChannelMessages*>(info.GetFrame());

  //  auto gridPosts = dynamic_cast<GridTeamChannelMessages*>(&frame->GetGrid());
  //  std::vector<GridBackendRow*> rowsWithMoreLoaded;
  //  gridPosts->GetRowsWithMoreLoaded(rowsWithMoreLoaded);

  //  ASSERT(info.Data().is_type<std::vector<SubItemKey>>());
  //  if (info.Data().is_type<std::vector<SubItemKey>>())
  //  {
  //      std::vector<SubItemKey> attachments = info.Data().get_value<std::vector<SubItemKey>>();

		//auto taskData = addFrameTask(YtriaTranslate::Do(ModuleTeamChannelMessages_deleteAttachments_1, _TYLOC("Delete Attachments")).c_str(), frame, p_Command);
  //      RefreshSpecificData refreshData = frame->GetRefreshSelectedData();
  //      pplx::create_task([this, attachments, frame, refreshData, taskData, action, gridPosts, rowsWithMoreLoaded]()
		//{
  //          std::map<SubItemKey, HttpResultWithError> results;

  //          std::set<PooledString> postsFilter;
  //          // Delete attachments
  //          for (auto& attachment : attachments)
  //          {
  //              results[attachment] = frame->GetBusinessObjectManager()->DeleteGroupPostAttachment(attachment.m_Id0, attachment.m_Id1, attachment.m_Id2, attachment.m_Id3, attachment.m_Id4, taskData).get();
  //              postsFilter.insert(attachment.m_Id3);
  //          }

  //          YCallbackMessage::DoPost([frame, gridPosts, refreshData, action, taskData, results]()
  //          {
  //              if (::IsWindow(frame->GetSafeHwnd()))
  //              {
  //                  gridPosts->GetModifications().SetSubItemDeletionsErrors(results);
  //                  gridPosts->ClearLog(taskData.GetId());
  //                  frame->RefreshAfterSubItemUpdates(refreshData, action);
  //              }
  //              else
  //              {
  //                  SetAutomationGreenLight(action);
  //              }
  //              TaskDataManager::Get().RemoveTaskData(taskData.GetId());
  //          });
  //      });
  //  }
  //  else
    {
        SetAutomationGreenLight(p_Command.GetAutomationAction(), p_Command.ToString() + YtriaTranslate::DoError(consoleEZApp_ProcessCommandLine_1, _YLOC(" - invalid command"),_YR("Y2489")).c_str());
    }
}

O365DataMap<BusinessGroup, std::map<BusinessChannel, vector<BusinessChatMessage>>> ModuleTeamChannelMessages::retrieveMessages(std::shared_ptr<BusinessObjectManager> p_BOM, const O365SubIdsContainer& p_TeamChannelIds, std::map<PooledString, PooledString> p_ChannelNames, YtriaTaskData p_TaskData)
{
	O365DataMap<BusinessGroup, std::map<BusinessChannel, vector<BusinessChatMessage>>> messages;

	auto logger = std::make_shared<MultiObjectsPageRequestLogger>(_T("team channel messages"), p_TeamChannelIds.size());

	bool first = true;
	auto mode = Sapio365Session::SessionType::APP_SESSION;
    for (const auto& teamChannelIds : p_TeamChannelIds)
    {
		BusinessGroup group;
		group.SetID(teamChannelIds.first.GetId());

		logger->IncrementObjCount();

		std::map<BusinessChannel, vector<BusinessChatMessage>> channelMessages;

		if (!p_TaskData.IsCanceled())
		{
			logger->SetContextualInfo(p_BOM->GetGraphCache().GetGroupContextualInfo(group.GetID()));
			p_BOM->GetGraphCache().SyncUncachedGroup(group, std::make_shared<MultiObjectsRequestLogger>(*logger), p_TaskData);

			for (auto& channelId : teamChannelIds.second)
			{
				BusinessChannel channel;
				channel.SetID(channelId);
				auto itt = p_ChannelNames.find(channelId);
				ASSERT(itt != p_ChannelNames.end());
				if (itt != p_ChannelNames.end())
					channel.SetDisplayName(itt->second);

				vector<BusinessChatMessage> chatMessages;

				logger->SetContextualInfo(GetDisplayNameOrId(channel));
                if (!p_TaskData.IsCanceled())
                {
                    auto messagesRequester = std::make_shared<ChannelMessagesRequester>(teamChannelIds.first.first, channelId, logger);
					messagesRequester->SetSessionMode(mode);
                    chatMessages = safeTaskCall(messagesRequester->Send(p_BOM->GetSapio365Session(), p_TaskData).Then([messagesRequester]() {
                        return messagesRequester->GetData();
                    }), p_BOM->GetMSGraphSession(mode), ListErrorHandler<BusinessChatMessage>, p_TaskData).get();

					// If first channel failed with Application credentials, it probably means Protected APIs access is not granted.
					if (first 
						&& mode == Sapio365Session::APP_SESSION
						&& (p_BOM->GetSapio365Session()->IsRole() || p_BOM->GetSapio365Session()->IsElevated())
						&& chatMessages.size() == 1
						&& chatMessages.back().GetError()
						&& chatMessages.back().GetError()->GetStatusCode() == http::status_codes::Forbidden)
					{
						// Fall back to admin credentials
						mode = Sapio365Session::USER_SESSION;
						messagesRequester->SetSessionMode(mode);
						chatMessages = safeTaskCall(messagesRequester->Send(p_BOM->GetSapio365Session(), p_TaskData).Then([messagesRequester]() {
							return messagesRequester->GetData();
							}), p_BOM->GetMSGraphSession(mode), ListErrorHandler<BusinessChatMessage>, p_TaskData).get();
					}

					first = false;
                }

				if (p_TaskData.IsCanceled())
				{
					channel.SetFlags(BusinessObject::CANCELED);
					channelMessages[channel] = {};
				}
				else
				{
					channelMessages[channel] = chatMessages;
				}
			}
		}
		else
		{
			const auto gp = p_BOM->GetGraphCache().GetGroupInCache(group.GetID());
			ASSERT(group.GetID() == gp.GetID());
			if (group.GetID() == gp.GetID())
				group.SetDisplayName(gp.GetDisplayName());
		}
		
		if (p_TaskData.IsCanceled() && channelMessages.empty())
		{
			group.SetFlags(BusinessObject::CANCELED);
			messages[group] = {};
		}
		else
		{
			messages[group] = channelMessages;
		}
    }

    return messages;
}
