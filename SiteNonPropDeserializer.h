#pragma once

#include "BusinessSite.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class SiteNonPropDeserializer : public JsonObjectDeserializer
                              , public Encapsulate<BusinessSite>
{
public:
    SiteNonPropDeserializer(const std::shared_ptr<Sapio365Session>& p_Session);
    void DeserializeObject(const web::json::object& p_Object) override;

private:
    const std::shared_ptr<Sapio365Session>& m_Session;
};

