#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "SystemFacet.h"

class SystemFacetDeserializer : public JsonObjectDeserializer, public Encapsulate<SystemFacet>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

