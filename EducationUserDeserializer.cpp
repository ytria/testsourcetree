#include "EducationUserDeserializer.h"
#include "IdentitySetDeserializer.h"
#include "PhysicalAddressDeserializer.h"
#include "EducationStudentDeserializer.h"
#include "EducationTeacherDeserializer.h"
#include "ListDeserializer.h"
#include "RelatedContactDeserializer.h"
#include "UserDeserializer.h"

EducationUserDeserializer::EducationUserDeserializer()
{
}

EducationUserDeserializer::EducationUserDeserializer(std::shared_ptr<const Sapio365Session> p_Session)
	: m_Session(p_Session)
{
}

void EducationUserDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	{
		UserDeserializer d(m_Session);
		d.DeserializeObject(p_Object);

		m_Data.SetID(d.GetData().GetID());
		m_Data.SetDisplayName(d.GetData().GetDisplayName());
		ASSERT(m_Data.GetDisplayName());
	}

	{
		IdentitySetDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("createdBy"), p_Object))
			m_Data.m_CreatedBy = std::move(d.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("externalSource"), m_Data.m_ExternalSource, p_Object);
	{
		PhysicalAddressDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("mailingAddress"), p_Object))
			m_Data.m_MailingAddress = std::move(d.GetData());
	}
	{
		PhysicalAddressDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("residenceAddress"), p_Object))
			m_Data.m_ResidenceAddress = std::move(d.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("middleName"), m_Data.m_MiddleName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("primaryRole"), m_Data.m_PrimaryRole, p_Object);
	{
		ListDeserializer<RelatedContact, RelatedContactDeserializer> d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("relatedContacts"), p_Object))
			m_Data.m_RelatedContacts = std::move(d.GetData());
	}
	{
		EducationStudentDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("student"), p_Object))
			m_Data.m_Student = std::move(d.GetData());
	}
	{
		EducationTeacherDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("teacher"), p_Object))
			m_Data.m_Teacher = std::move(d.GetData());
	}
}
