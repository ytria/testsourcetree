#include "PackageDeserializer.h"
#include "JsonSerializeUtil.h"

void PackageDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.Type, p_Object);
}
