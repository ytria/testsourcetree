#include "SubscribedSkusRequester.h"
#include "IRequestLogger.h"
#include "Sapio365Session.h"
#include "SubscribedSkuDeserializerFactory.h"
#include "BusinessSubscribedSku.h"
#include "SubscribedSkuDeserializer.h"
#include "ValueListDeserializer.h"
#include "MsGraphHttpRequestLogger.h"

SubscribedSkusRequester::SubscribedSkusRequester(const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_Logger(p_Logger)
	, m_ForceUseMainMsGraphSession(false)
{
}

TaskWrapper<void> SubscribedSkusRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<SingleRequestResult>();
	auto factory = std::make_unique<SubscribedSkuDeserializerFactory>(p_Session);

	m_Deserializer = std::make_shared<ValueListDeserializer<BusinessSubscribedSku, SubscribedSkuDeserializer>>(std::move(factory));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	auto session = m_ForceUseMainMsGraphSession ? p_Session->GetMainMSGraphSession() : p_Session->GetMSGraphSession(httpLogger->GetSessionType());
	return session->getObjects(m_Deserializer, { _YTEXT("subscribedSkus") },
		{ { _YTEXT("$select"), _YTEXT("capabilityStatus,consumedUnits,prepaidUnits,servicePlans,skuId,skuPartNumber,appliesTo") } },
		false, m_Logger, httpLogger, p_TaskData);
}

void SubscribedSkusRequester::SetForceUseMainMsGraphSession(bool p_Force)
{
	m_ForceUseMainMsGraphSession = p_Force;
}

const SingleRequestResult& SubscribedSkusRequester::GetResult() const
{
	return *m_Result;
}

const vector<BusinessSubscribedSku>& SubscribedSkusRequester::GetData() const
{
	return m_Deserializer->GetData();
}
