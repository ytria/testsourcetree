#pragma once
#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "OptionalClaims.h"

class OptionalClaimsDeserializer : public JsonObjectDeserializer, public Encapsulate<OptionalClaims>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

