#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BooleanColumn.h"

class BooleanColumnDeserializer : public JsonObjectDeserializer, public Encapsulate<BooleanColumn>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

