#pragma once

#include "BaseO365Grid.h"
#include "BusinessDirectoryRoleTemplate.h"
#include "DirectoryRolesInfo.h"

// Define to have the activate role command available.
// [DISABLED by default - role automatically activated when adding members]
#define HAS_ACTIVATE_ROLE_COMMAND 0

// Define to have the deactivate role command available.
// [DISABLED dy default - SEEMS IMPOSSIBLE ON O365 USING GRAPH API]
#define HAS_DEACTIVATE_ROLE_COMMAND 0

class GridDirectoryRoles : public O365Grid
{
public:
    GridDirectoryRoles();

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;
    
    virtual void customizeGrid() override;
    virtual void InitializeCommands() override;

    void BuildView(DirectoryRolesInfo p_RolesInfo, const vector<O365UpdateOperation>& p_UpdateOperations, const RefreshSpecificData& p_RefreshSpecificData, bool p_FullPurge);
    void AddDirectoryRoleData(GridBackendRow* p_RowTemplate, const BusinessDirectoryRoleTemplate& p_Template, const BusinessDirectoryRole& p_Role, GridUpdater& p_Updater);

    GridBackendColumn* GetColRoleId();

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

	struct RoleChange
	{
		PooledString m_TemplateID;
		boost::YOpt<PooledString> m_RoleID;
		boost::YOpt<bool> m_Activate;
		boost::YOpt<row_pk_t> m_RowPrimaryKey;

		boost::YOpt<vector<std::pair<PooledString, row_pk_t>>> m_UsersToAdd;
		boost::YOpt<vector<std::pair<PooledString, row_pk_t>>> m_UsersToRemove;
	};
	using RoleChanges = std::vector<RoleChange>;
	RoleChanges GetChanges(const vector<GridBackendRow*>& p_Rows) const;
	RoleChanges GetChanges(const set<GridBackendRow*>& p_Rows) const;

	virtual bool HasModificationsPending(bool inSelectionOnly = false) override;

protected:
    virtual void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart) override;

	afx_msg void OnAddMember();
	afx_msg void OnUpdateAddMember(CCmdUI* pCmdUI);

	afx_msg void OnRemoveMember();
	afx_msg void OnUpdateRemoveMember(CCmdUI* pCmdUI);

#if HAS_ACTIVATE_ROLE_COMMAND
    afx_msg void OnActivateRole();
	afx_msg void OnUpdateActivateRole(CCmdUI* pCmdUI);
#endif

#if HAS_DEACTIVATE_ROLE_COMMAND
    afx_msg void OnDeactivateRole();
    afx_msg void OnUpdateDeactivateRole(CCmdUI* pCmdUI);
#endif

    DECLARE_MESSAGE_MAP()

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

private:
	template<class Container>
	RoleChanges getChangesImpl(const Container& p_Rows) const;

	RoleChange initChange(GridBackendRow* p_RowTemplate) const;
    std::map<PooledString, BusinessDirectoryRole> GetRolesMap(DirectoryRolesInfo &p_RolesInfo);

	bool activateRole(GridBackendRow* p_RoleRow);

	void getUsers(vector<BusinessUser>& p_Users) const;

    // Directory Role columns
    GridBackendColumn* m_ColRoleDisplayName;
    GridBackendColumn* m_ColUserDisplayName;
    GridBackendColumn* m_ColDescription;
    GridBackendColumn* m_ColActivated;
	GridBackendColumn* m_ColRoleId;
	GridBackendColumn* m_ColRoleTemplateId;
  
    GridBackendColumn* m_ColUserPrincipalName;
	GridBackendColumn* m_ColUserType;

	GridBackendColumn* m_ColumnLastRequestDateTime;

    int m_RoleActivated;
    int m_RoleDeactivated;
};
