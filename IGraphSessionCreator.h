#pragma once

#include "MSGraphSession.h"

class IGraphSessionCreator
{
public:
	struct Result
	{
		bool m_Success = false;
		wstring m_GraphError;
	};

	virtual ~IGraphSessionCreator() = default;
	virtual std::shared_ptr<MSGraphSession> Create() = 0;

	const Result& GetResult() const;

protected:
	Result m_Result;
};

