#include "GridPosts.h"

#include "AttachmentInfo.h"
#include "AutomationWizardPosts.h"
#include "BasicGridSetup.h"
#include "BusinessConversationThread.h"
#include "BusinessGroup.h"
#include "DlgDownloadAttachments.h"
#include "FramePosts.h"
#include "GraphCache.h"
#include "GridHelpers.h"
#include "GridUpdater.h"
#include "MessageBodyData.h"
#include "MessageBodyViewer.h"
#include "O365AdminUtil.h"
#include "PostAttachmentDeletion.h"
#include "SubItemsFlagGetter.h"
#include "Resource.h"
#include "YCallbackMessage.h"

IMPLEMENT_CacheGrid_DeleteRowLParam(GridPosts, MessageBodyData)

BEGIN_MESSAGE_MAP(GridPosts, ModuleO365Grid<BusinessPost>)
    ON_COMMAND(ID_POSTSGRID_SHOWBODY,				 OnShowBody)
    ON_COMMAND(ID_POSTSGRID_SHOWATTACHMENTS,		 OnShowPostAttachments)
    ON_COMMAND(ID_POSTSGRID_DOWNLOADATTACHMENTS,	 OnDownloadPostAttachments)
    ON_COMMAND(ID_POSTSGRID_VIEWITEMATTACHMENT,		 OnViewPostItemAttachment)
    ON_COMMAND(ID_POSTSGRID_DELETEATTACHMENT,		 OnDeleteAttachment)
    ON_COMMAND(ID_POSTSGRID_TOGGLEINLINEATTACHMENTS, OnToggleInlineAttachments)

    ON_UPDATE_COMMAND_UI(ID_POSTSGRID_SHOWBODY,				   OnUpdateShowBody)
    ON_UPDATE_COMMAND_UI(ID_POSTSGRID_SHOWATTACHMENTS,		   OnUpdateShowPostAttachments)
    ON_UPDATE_COMMAND_UI(ID_POSTSGRID_DOWNLOADATTACHMENTS,	   OnUpdateDownloadPostAttachments)
    ON_UPDATE_COMMAND_UI(ID_POSTSGRID_VIEWITEMATTACHMENT,	   OnUpdatePostItemAttachment)
    ON_UPDATE_COMMAND_UI(ID_POSTSGRID_DELETEATTACHMENT,		   OnUpdateDeleteAttachment)
    ON_UPDATE_COMMAND_UI(ID_POSTSGRID_TOGGLEINLINEATTACHMENTS, OnUpdateToggleInlineAttachments)
END_MESSAGE_MAP()

GridPosts::GridPosts()
    : m_FramePosts(nullptr)
	, m_ShowInlineAttachments(false)
{
    //UseDefaultActions(/*O365Grid::ACTION_CREATE | */O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT/* | O365Grid::ACTION_VIEW*/);
	EnableGridModifications();
    SetShowInlineAttachments(false);

	initWizard<AutomationWizardPosts>(_YTEXT("Automation\\Posts"));
}

GridPosts::~GridPosts()
{
    GetBackend().Clear();
}

ModuleCriteria GridPosts::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
    ModuleCriteria criteria = m_FramePosts->GetModuleCriteria();

    criteria.m_SubSubIDs.clear();
    for (auto row : p_Rows)
    {
        const auto& groupId = row->GetField(m_ColMetaGroupId).GetValueStr();
        const auto& convId = PooledString(row->GetField(m_ColMetaId).GetValueStr());
        const auto& threadId = PooledString(row->GetField(m_ColConversationThreadId).GetValueStr());
        criteria.m_SubSubIDs[PooledString(groupId)][convId].insert(threadId);
    }

    return criteria;
}

void GridPosts::customizeGrid()
{
	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_GRIDPOSTS_ACCELERATOR));

	static const wstring g_FamilyGroup			= YtriaTranslate::Do(GridPosts_customizeGrid_1, _YLOC("Group")).c_str();
	static const wstring g_FamilyConversation	= YtriaTranslate::Do(GridPosts_customizeGrid_2, _YLOC("Conversation")).c_str();

	{
		BasicGridSetup setup(GridUtil::g_AutoNamePosts, LicenseUtil::g_codeSapio365posts,
		{
			{ &m_ColMetaDisplayName, YtriaTranslate::Do(GridPosts_customizeGrid_3, _YLOC("Conversation Display Name")).c_str(), g_FamilyConversation },
			{ &m_ColMetaGroupName, YtriaTranslate::Do(GridPosts_customizeGrid_4, _YLOC("Group Name")).c_str(), g_FamilyGroup },
			{ &m_ColMetaId, YtriaTranslate::Do(GridPosts_customizeGrid_5, _YLOC("Conversation ID")).c_str(), g_FamilyConversation }
		}, { BusinessPost() });
		setup.Setup(*this, false);
	}

	m_ColMetaGroupName->SetUniqueID(_YUID("metaGroup.Name"));
	setBasicGridSetupHierarchy({ { { m_ColMetaDisplayName, 0 } }
								,{ rttr::type::get<BusinessPost>().get_id() }
								,{ rttr::type::get<BusinessPost>().get_id(), rttr::type::get<BusinessConversationThread>().get_id() } });

    m_ColMetaGroupId						= AddColumn(_YUID("metaGroupId"),							YtriaTranslate::Do(GridPosts_customizeGrid_6, _YLOC("Group ID")).c_str(),							g_FamilyGroup,			g_ColumnSize12, { g_ColumnsPresetTech });
    //m_ColConversationId						= AddColumn(_YUID("conversationId"),						_TLOC("Conversation ID??"),				g_FamilyConversation,	g_ColumnSize12, { g_ColumnsPresetTech });// ?? 2 columns Conversation ID ??
    m_ColConversationThreadId				= AddColumn(_YUID("conversationThreadId"),					YtriaTranslate::Do(GridPosts_customizeGrid_7, _YLOC("Conversation Thread ID")).c_str(),				g_FamilyConversation,	g_ColumnSize12, { g_ColumnsPresetTech });
    m_ColFromName							= AddColumn(_YUID("fromName"),								YtriaTranslate::Do(GridPosts_customizeGrid_8, _YLOC("From - Name")).c_str(),						g_FamilyInfo,			g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColFromAddress						= AddColumn(_YUID("fromAddress"),							YtriaTranslate::Do(GridPosts_customizeGrid_9, _YLOC("From - Email")).c_str(),						g_FamilyInfo,			g_ColumnSize32);
    m_ColSenderName							= AddColumn(_YUID("senderName"),							YtriaTranslate::Do(GridPosts_customizeGrid_10, _YLOC("Sender - Name")).c_str(),						g_FamilyInfo,			g_ColumnSize22);
    m_ColSenderAddress						= AddColumn(_YUID("senderAddress"),							YtriaTranslate::Do(GridPosts_customizeGrid_11, _YLOC("Sender - Email")).c_str(),					g_FamilyInfo,			g_ColumnSize32);
	m_ColCreatedDateTime					= AddColumnDate(_YUID("createdDateTime"),					YtriaTranslate::Do(GridPosts_customizeGrid_12, _YLOC("Created On")).c_str(),						g_FamilyInfo,			g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColReceivedDateTime					= AddColumnDate(_YUID("receivedDateTime"),					YtriaTranslate::Do(GridPosts_customizeGrid_13, _YLOC("Received On")).c_str(),						g_FamilyInfo,			g_ColumnSize16, { g_ColumnsPresetDefault });
    m_ColLastModifiedDateTime				= AddColumnDate(_YUID("lastModifiedDateTime"),				YtriaTranslate::Do(GridPosts_customizeGrid_14, _YLOC("Last Modified On")).c_str(),					g_FamilyInfo,			g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColNewParticipantsName				= AddColumn(_YUID("newParticipantsName"),					YtriaTranslate::Do(GridPosts_customizeGrid_15, _YLOC("New Participants - Name")).c_str(),			g_FamilyInfo,			g_ColumnSize22, { g_ColumnsPresetDefault });
    m_ColNewParticipantsAddress				= AddColumn(_YUID("newParticipantsAddress"),				YtriaTranslate::Do(GridPosts_customizeGrid_16, _YLOC("New Participants - Email")).c_str(),			g_FamilyInfo,			g_ColumnSize32);
    m_ColBodyContentType					= AddColumn(_YUID("bodyContentType"),						YtriaTranslate::Do(GridPosts_customizeGrid_17, _YLOC("Body Content Type")).c_str(),					g_FamilyInfo,			g_ColumnSize6);
	m_ColBodyPreview						= AddColumnRichText(_YUID(O365_MESSAGE_BODYPREVIEW),		YtriaTranslate::Do(GridPosts_customizeGrid_18, _YLOC("Preview")).c_str(),							g_FamilyInfo,			g_ColumnSize32, { g_ColumnsPresetDefault });
    m_ColCategories							= AddColumn(_YUID("categories"),							YtriaTranslate::Do(GridPosts_customizeGrid_19, _YLOC("Categories")).c_str(),						g_FamilyInfo,			g_ColumnSize12);
    m_ColChangeKey							= AddColumn(_YUID("changeKey"),								YtriaTranslate::Do(GridPosts_customizeGrid_20, _YLOC("Change Key")).c_str(),						g_FamilyInfo,			g_ColumnSize2);
	addColumnGraphID();// do not break categories

	static const wstring g_FamilyAttachment = YtriaTranslate::Do(GridPosts_customizeGrid_36, _YLOC("Attachment")).c_str();
    m_ColHasGraphAttachment                 = AddColumnCheckBox(_YUID("hasAttachments"),				YtriaTranslate::Do(GridPosts_customizeGrid_37, _YLOC("Has Attachment")).c_str(),					g_FamilyAttachment,     g_ColumnSize6,  { g_ColumnsPresetDefault });
	addColumnIsLoadMore(_YUID("attachmentInfoLoaded"), YtriaTranslate::Do(GridPosts_customizeGrid_38, _YLOC("Status - Load Attachment Info")).c_str());
    m_ColHasAnyAttachment				    = AddColumnCheckBox(_YUID("containsAttachments"),			YtriaTranslate::Do(GridPosts_customizeGrid_39, _YLOC("Contains Attachments incl. inline")).c_str(),	g_FamilyAttachment,		g_ColumnSize6);
	m_ColAttachmentType						= AddColumn(_YUID("attachment.type"),						YtriaTranslate::Do(GridPosts_customizeGrid_40, _YLOC("Type")).c_str(),				                g_FamilyAttachment,		g_ColumnSize12, { g_ColumnsPresetDefault });
	SetColumnPresetFlags(AddColumnFileType(_T("Icon"), g_FamilyAttachment), { g_ColumnsPresetDefault });
    m_ColAttachmentName						= AddColumn(_YUID("attachment.name"),						YtriaTranslate::Do(GridPosts_customizeGrid_41, _YLOC("Name")).c_str(),				                g_FamilyAttachment,		g_ColumnSize32, { g_ColumnsPresetDefault });
	if (nullptr != m_ColAttachmentName)
		m_ColAttachmentName->SetManageFileFormat(true);
	m_ColAttachmentNameExtOnly				= AddColumn(_YUID("ATTACHMENTNAMEEXTONLY"),					_T("Extension"), g_FamilyAttachment, g_ColumnSize6);
	m_ColAttachmentContentType				= AddColumn(_YUID("attachment.contentType"),				YtriaTranslate::Do(GridPosts_customizeGrid_42, _YLOC("Content Type")).c_str(),		                g_FamilyAttachment,		g_ColumnSize12);
    m_ColAttachmentSize						= AddColumnNumber(_YUID("attachment.size"),					YtriaTranslate::Do(GridPosts_customizeGrid_43, _YLOC("Size")).c_str(),				                g_FamilyAttachment,		g_ColumnSize8,	{ g_ColumnsPresetDefault });
	m_ColAttachmentSize->SetXBytesColumnFormat();
	m_ColAttachmentLastModifiedDateTime		= AddColumnDate(_YUID("attachment.lastModifiedDateTime"),	YtriaTranslate::Do(GridPosts_customizeGrid_29, _YLOC("Last Modified Date")).c_str(),				g_FamilyAttachment,		g_ColumnSize16);
    m_ColAttachmentIsInline					= AddColumnCheckBox(_YUID("attachment.isInline"),			YtriaTranslate::Do(GridPosts_customizeGrid_30, _YLOC("Inline")).c_str(),							g_FamilyAttachment,		g_ColumnSize6);
	m_ColAttachmentId						= AddColumn(_YUID("attachment.id"),							YtriaTranslate::Do(GridPosts_customizeGrid_33, _YLOC("Attachment ID")).c_str(),						g_FamilyAttachment,		g_ColumnSize12, { g_ColumnsPresetTech });

	AddColumnForRowPK(m_ColMetaGroupId);
	AddColumnForRowPK(m_ColMetaId);
	AddColumnForRowPK(m_ColConversationThreadId);
	AddColumnForRowPK(GetColId());

	m_ColNewParticipantsAddress->SetMultivalueFamily(YtriaTranslate::Do(GridPosts_customizeGrid_44, _YLOC("Participants")).c_str());
	m_ColNewParticipantsAddress->AddMultiValueExplosionSister(m_ColNewParticipantsName);
	
	// This column will be the elder of the sisterhood. Keep it the same as the one used in row flagger to identify subitems. 
	m_ColAttachmentId->SetMultivalueFamily(YtriaTranslate::Do(GridPosts_customizeGrid_45, _YLOC("Attachments")).c_str());
    m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentName);
    m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentType);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentContentType);
    m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentSize);
    m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentLastModifiedDateTime);
    m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentIsInline);
	m_ColAttachmentId->AddMultiValueExplosionSister(m_ColAttachmentNameExtOnly);
	m_ColAttachmentId->AddMultiValueExplosionSister(GetColumnFileType());

	SetColumnsPresetFlags( {	m_ColHasAnyAttachment,
                                m_ColAttachmentType,
								m_ColAttachmentId,
								m_ColAttachmentName,
								m_ColAttachmentContentType,
								m_ColAttachmentSize,
								m_ColAttachmentLastModifiedDateTime,
								m_ColAttachmentIsInline,
								m_ColAttachmentNameExtOnly, 
								GetColumnFileType() },
							{ g_ColumnsPresetMore });

	addTopCategories(YtriaTranslate::Do(GridPosts_customizeGrid_34, _YLOC("Post Info")).c_str(), YtriaTranslate::Do(GridPosts_customizeGrid_35, _YLOC("Attachment Info")).c_str(), true);

	m_FramePosts	= dynamic_cast<FramePosts*>(GetParentFrame());
	m_Icons			= GridUtil::LoadAttachmentIcons(*this);
	m_MessageViewer = std::make_unique<MessageBodyViewer>(*this, m_ColBodyContentType, nullptr);

    SetRowFlagger(std::make_unique<SubItemsFlagGetter>(GetModifications(), m_ColAttachmentId->GetMultiValueElderColumn()));

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_FramePosts);
}

wstring GridPosts::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, { {_T("Group"), m_ColMetaGroupName }, { _T("Conversation"), m_ColMetaDisplayName } }, 
		_YFORMAT(L"Created on %s by %s", p_Row->GetField(m_ColCreatedDateTime).ToString().c_str(), p_Row->GetField(m_ColFromName).ToString().c_str()));
}

void GridPosts::customizeGridPostProcess()
{
	O365Grid::customizeGridPostProcess();
	SetNoFieldTextAndIcon(GetColumnsByPresets({ g_ColumnsPresetMore }), IDB_ICON_LM_FILE_ATTACHMENT, YtriaTranslate::Do(GridPosts_customizeGridPostProcess_1, _YLOC("To see this information, use the 'Load Attachment Info' button")).c_str(), YtriaTranslate::Do(GridPosts_customizeGridPostProcess_2, _YLOC(" - Use the 'Load Attachment Info' button to display additional information - ")).c_str());
	SetLoadMoreStatus(true,		YtriaTranslate::Do(GridPosts_customizeGridPostProcess_3, _YLOC("Loaded")).c_str(),		IDB_ICON_LMSTATUS_LOADED_ATTACHMENTS);
	SetLoadMoreStatus(false,	YtriaTranslate::Do(GridPosts_customizeGridPostProcess_4, _YLOC("Not loaded")).c_str(),	IDB_ICON_LMSTATUS_NOTLOADED_ATTACHMENTS);
}

O365SubSubIdsContainer GridPosts::GetRefreshInfos()
{
    std::vector<GridBackendRow*> rows;
    GetAllNonGroupVisibleRows(rows);

    O365SubSubIdsContainer ids;

    for (auto row : rows)
    {
        if (GridUtil::IsBusinessConversationThread(row))
        {
            PooledString groupId = row->GetField(m_ColMetaGroupId).GetValueStr();
            PooledString convId = row->GetField(m_ColMetaId).GetValueStr();
            PooledString threadId = row->GetField(GetColId()).GetValueStr();

            if (!groupId.IsEmpty() && !convId.IsEmpty() && !threadId.IsEmpty())
                ids[groupId][convId].insert(threadId);
        }
    }

    return ids;
}

void GridPosts::ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_RefreshModificationStates, bool p_ShowPostUpdateError)
{
    ModuleUtil::MergeAttachments(m_AllAttachments, p_BusinessAttachments);

	GridIgnoreModification ignoramus(*this);

	const uint32_t options	= GridUpdaterOptions::UPDATE_GRID
							/*| GridUpdaterOptions::PURGE*/
							| (p_RefreshModificationStates ? GridUpdaterOptions::REFRESH_MODIFICATION_STATES : 0)
							| (p_ShowPostUpdateError ? GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS : 0)
							/*| GridUpdaterOptions::REFRESH_PROCESS_DATA*/
							| GridUpdaterOptions::PROCESS_LOADMORE;

	GridUpdaterOptions updateOptions(options, GetColumnsByPresets({ g_ColumnsPresetMore }));
	GridUpdater updater(*this, updateOptions);

	loadAttachments(p_BusinessAttachments, updater);
}

void GridPosts::loadAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, GridUpdater& updater)
{
	for (const auto& byGroup : p_BusinessAttachments)
	{
		GridBackendField fGroupId(byGroup.first, m_ColMetaGroupId);

		for (const auto& keyVal : byGroup.second)
		{
			auto convIdthreadIdPPostId = Str::explodeIntoVector(wstring(keyVal.first), wstring(_YTEXT("@")), true);
			ASSERT(convIdthreadIdPPostId.size() == 3);
			if (convIdthreadIdPPostId.size() == 3)
			{
				const auto& attachments = keyVal.second;
				GridBackendField fConvId(convIdthreadIdPPostId[0], m_ColMetaId);
				GridBackendField fThreadId(convIdthreadIdPPostId[1], m_ColConversationThreadId);
				GridBackendField fPostId(convIdthreadIdPPostId[2], GetColId());

				vector<GridBackendField> pk = { fGroupId, fConvId, fThreadId, fPostId };

				GridBackendRow* row = m_RowIndex.GetExistingRow(pk, updater);

				if (nullptr != row)
				{
					updater.GetOptions().AddRowWithRefreshedValues(row);

					if (attachments.size() == 1 && attachments[0].GetError() != boost::none)
					{
						ClearFieldsByPreset(row, O365Grid::g_ColumnsPresetMore);
						updater.OnLoadingError(row, *attachments[0].GetError());
					}
					else
					{
						// Should we?
						// Clear previous error
						// NB: this is active in GridMessages

						//if (row->IsError())
						//	row->ClearStatus();

						fillAttachmentFields(row, attachments);
					}
				}
			}
		}
	}
}

BusinessPost GridPosts::getBusinessObject(GridBackendRow* p_Row) const
{
    BusinessPost post;

	{
		const auto& field = p_Row->GetField(m_ColMetaId);
		if (field.HasValue())
			post.SetConversationId(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = p_Row->GetField(m_ColCreatedDateTime);
		if (field.HasValue())
			post.SetCreatedDateTime(field.GetValueTimeDate());
	}

	{
		const auto& field = p_Row->GetField(m_ColLastModifiedDateTime);
		if (field.HasValue())
			post.SetLastModifiedDateTime(field.GetValueTimeDate());
	}

	{
		const auto& field = p_Row->GetField(m_ColChangeKey);
		if (field.HasValue())
			post.SetChangeKey(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	{
		const auto& field = p_Row->GetField(m_ColCategories);
		if (field.HasValue())
		{
			if (field.IsMulti())
				post.SetCategories(*field.GetValuesMulti<PooledString>());
			else
				post.SetCategories(boost::YOpt<vector<PooledString>>({ field.GetValueStr() }));
		}
	}

	{
		const auto& field = p_Row->GetField(m_ColBodyContentType);
		if (field.HasValue())
		{
			ItemBody itemBody;
			itemBody.ContentType = field.GetValueStr();
			post.SetBody(itemBody);
		}
	}

	{
		const auto& field = p_Row->GetField(m_ColReceivedDateTime);
		if (field.HasValue())
			post.SetReceivedDateTime(field.GetValueTimeDate());
	}


	{
		const auto& field = p_Row->GetField(m_ColReceivedDateTime);
		if (field.HasValue())
			post.SetHasAttachments(field.GetValueBool());
	}

	post.SetFrom(extractRecipient(p_Row, m_ColFromAddress, m_ColFromName));
	post.SetSender(extractRecipient(p_Row, m_ColSenderAddress, m_ColSenderName));

	{
		const auto& field = p_Row->GetField(m_ColConversationThreadId);
		if (field.HasValue())
			post.SetConversationThreadId(boost::YOpt<PooledString>(field.GetValueStr()));
	}

	if (p_Row->IsExplosionFragment())
	{
		GridPosts* pGrid = const_cast<GridPosts*>(this);
		auto ListSibs = pGrid->GetMultivalueManager().GetExplosionSiblings(p_Row);
		ListSibs.insert(pGrid->GetMultivalueManager().GetTopSourceRow(p_Row));

		vector<Recipient> TempTotal;
		for (GridBackendRow* pRowSib : ListSibs)
		{
			vector<Recipient> Temp1 = GridHelpers::ExtractRecipients(pRowSib, m_ColNewParticipantsAddress, m_ColNewParticipantsName);
			TempTotal.insert(TempTotal.end(), Temp1.begin(), Temp1.end());
		}

		// Remove duplicates. operator== is defined in Recipient.
		sort(TempTotal.begin(), TempTotal.end());
		TempTotal.erase(unique(TempTotal.begin(), TempTotal.end()), TempTotal.end());
		post.SetNewParticipants(TempTotal);
	}
	else
		post.SetNewParticipants(GridHelpers::ExtractRecipients(p_Row, m_ColNewParticipantsAddress, m_ColNewParticipantsName));

    return post;
}

Recipient GridPosts::extractRecipient(GridBackendRow* p_Row, GridBackendColumn* p_ColAddress, GridBackendColumn* p_ColName)
{
	const wchar_t* address = nullptr;
	const wchar_t* name = nullptr;
	{
		const auto& field = p_Row->GetField(p_ColAddress);
		if (field.HasValue())
			address = field.GetValueStr();
	}
	{
		const auto& field = p_Row->GetField(p_ColName);
		if (field.HasValue())
			name = field.GetValueStr();
	}

	return GridHelpers::ExtractRecipient(address, name);
}

void GridPosts::ApplyDeleteAttachments(bool p_Selected)
{
    std::vector<SubItemKey> attachments;
    if (!p_Selected)
    {
        for (const auto& deletedAttachment : GetModifications().GetSubItemDeletions())
            attachments.push_back(deletedAttachment->GetKey());
    }
    else
    {
        for (auto row : GetSelectedRows())
        {
			if (!row->IsGroupRow())
			{
				for (const auto& deletedAttachment : GetModifications().GetSubItemDeletions())
				{
					if (deletedAttachment->IsCorrespondingRow(row))
						attachments.push_back(deletedAttachment->GetKey());
				}
			}
        }
    }

    CommandInfo info;
    info.Data() = attachments;
    info.SetFrame(m_FramePosts);
	info.SetRBACPrivilege(m_FramePosts->GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Post, Command::ModuleTask::DeleteAttachment, info, { this, g_ActionNameSelectedViewPermissions, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
}

const map<PooledString, PooledString>& GridPosts::GetThreadsTopics() const
{
    return m_ThreadsTopics;
}

void GridPosts::SetThreadsTopics(const map<PooledString, PooledString>& p_ThreadsTopics)
{
    m_ThreadsTopics = p_ThreadsTopics;
}

void GridPosts::SetShowInlineAttachments(const bool& p_Val)
{
    m_ShowInlineAttachments = p_Val;
}

void GridPosts::HideMessageViewer()
{
	if (m_MessageViewer && m_MessageViewer->IsShowing())
		m_MessageViewer->ToggleView();
}

std::set<ModuleUtil::PostMetaInfo> GridPosts::GetAttachmentsRequestInfo(const std::vector<GridBackendRow *>& selectedRows, const O365IdsContainer& p_PostsIdsFilter)
{
    std::set<ModuleUtil::PostMetaInfo> ids;

    for (auto& row : selectedRows)
    {
        if (GridUtil::isBusinessType<BusinessPost>(row) && IsGroupAuthorizedByRoleDelegation(row->GetField(m_ColMetaGroupId).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CONVERSATIONS_LOADMORE))
        {
            ModuleUtil::PostMetaInfo postInfo;

            PooledString groupId		= row->GetField(m_ColMetaGroupId).GetValueStr();
            PooledString conversationId	= row->GetField(m_ColMetaId).GetValueStr();
            PooledString postId			= row->GetField(GetColId()).GetValueStr();
			
            if (!groupId.IsEmpty() && !conversationId.IsEmpty() && !postId.IsEmpty())
            {
                postInfo.GroupId		= groupId;
                postInfo.ConversationId = conversationId;
                postInfo.PostId			= postId;
				postInfo.ThreadId		= row->GetField(m_ColConversationThreadId).GetValueStr();
				postInfo.m_Context		= _YFORMAT(L"conversation '%s' from group '%s'", row->GetField(m_ColMetaDisplayName).GetValueStr(), row->GetField(m_ColMetaGroupName).GetValueStr());
                if (p_PostsIdsFilter.empty() || p_PostsIdsFilter.find(postInfo.PostId) != p_PostsIdsFilter.end())
                    ids.insert(postInfo);
            }
        }
    }

    return ids;
}

void GridPosts::loadMoreImpl(const std::vector<GridBackendRow*>& p_Rows)
{
	std::set<AttachmentsInfo::IDs> ids;

    CommandInfo info;
    info.Data() = GetAttachmentsRequestInfo(p_Rows, {});
    info.SetFrame(m_FramePosts);
	if (nullptr != m_FramePosts)
		info.SetRBACPrivilege(m_FramePosts->GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Post, Command::ModuleTask::ListAttachments, info, { this, g_ActionNameSelectedAttachmentLoadInfo, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
}

void GridPosts::OnGbwSelectionChangedSpecific()
{
    if (m_MessageViewer && m_MessageViewer->IsShowing())
    {
        // Try to less freeze on selection change by posting a message.
        YCallbackMessage::DoPost([this]()
        {
            if (::IsWindow(m_hWnd) && m_MessageViewer->IsShowing())
                m_MessageViewer->ShowSelectedMessageBody();
        });
    }
}

void GridPosts::OnShowPostAttachments()
{
    std::vector<GridBackendRow*> selectedRows;
    GetSelectedNonGroupRows(selectedRows);
    loadMoreImpl(selectedRows);
}

void GridPosts::OnDownloadPostAttachments()
{
    DlgDownloadAttachments dlg(this, DlgDownloadAttachments::CONTEXT::POST);
    if (dlg.DoModal() != IDOK)
        return;

    AttachmentsInfo attachmentsMetaInfo;
    attachmentsMetaInfo.m_FileExistsAction		= dlg.GetFileExistsAction();
    attachmentsMetaInfo.m_DownloadFolderName	= dlg.GetFolderPath();
    attachmentsMetaInfo.m_OpenFolder			= dlg.GetOpenFolder();
	const auto useOwnerName						= dlg.GetUseOwnerName();

	std::set<AttachmentsInfo::IDs> alreadyProcessedAttachmentIDs;
    for (auto& row : GetSelectedRows())
    {
        if (GridUtil::isBusinessType<BusinessPost>(row) && row->GetField(m_ColAttachmentId).HasValue() && 
			IsGroupAuthorizedByRoleDelegation(row->GetField(m_ColMetaGroupId).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CONVERSATIONS_ATTACHMENTS_DOWNLOAD))
        {
			const wstring groupId	= row->GetField(m_ColMetaGroupId).GetValueStr();
			const wstring convId	= row->GetField(m_ColMetaId).GetValueStr();
			const wstring threadId	= row->GetField(m_ColConversationThreadId).GetValueStr();
			const wstring postId	= row->GetField(GetColId()).GetValueStr();

			wstring columnsPath;
			if (useOwnerName)
				columnsPath = row->GetField(m_ColMetaDisplayName).GetValueStr();
			if (!columnsPath.empty())
				columnsPath += _YTEXT("\\");
			columnsPath += GridUtil::GetColumnsPath(*this, row, dlg.GetSelectedColumnsTitleDisplayed());

			if (nullptr != row->GetField(m_ColAttachmentId).GetValuesMulti<PooledString>())
			{
				auto attachmentIds = row->GetField(m_ColAttachmentId).GetValuesMulti<PooledString>();
				auto attachmentTypes = row->GetField(m_ColAttachmentType).GetValuesMulti<PooledString>();
				ASSERT(nullptr != attachmentIds && nullptr != attachmentTypes);
				if (nullptr != attachmentIds && nullptr != attachmentTypes)
				{
					ASSERT(attachmentTypes->size() == attachmentIds->size());
					if (attachmentTypes->size() == attachmentIds->size())
					{
						size_t index = 0;
						for (const auto& attachmentId : *attachmentIds)
						{
							if ((*attachmentTypes)[index] == Util::GetFileAttachmentStr())
							{
								AttachmentsInfo::Infos infos;
								infos.m_IDs.UserOrGroupID() = groupId;
								infos.m_IDs.EventOrMessageOrConversationID() = convId;
								infos.m_IDs.ThreadID() = threadId;
								infos.m_IDs.PostID() = postId;
								infos.m_IDs.AttachmentID() = attachmentId;

								if (alreadyProcessedAttachmentIDs.insert(infos.m_IDs).second)
								{
									infos.m_ColumnsPath = columnsPath;
									attachmentsMetaInfo.m_AttachmentInfos.push_back(infos);
								}
							}
							++index;
						}
					}
				}
			}
			else
			{
				const wstring attachmentId = row->GetField(m_ColAttachmentId).GetValueStr();
				const wstring attachmentType = row->GetField(m_ColAttachmentType).GetValueStr();
				if (attachmentType == Util::GetFileAttachmentStr())
				{
					AttachmentsInfo::Infos infos;
					infos.m_IDs.UserOrGroupID() = groupId;
					infos.m_IDs.EventOrMessageOrConversationID() = convId;
					infos.m_IDs.ThreadID() = threadId;
					infos.m_IDs.PostID() = postId;
					infos.m_IDs.AttachmentID() = attachmentId;

					if (alreadyProcessedAttachmentIDs.insert(infos.m_IDs).second)
					{
						infos.m_ColumnsPath = columnsPath;
						attachmentsMetaInfo.m_AttachmentInfos.push_back(infos);
					}
				}
			}
        }
    }

	CommandInfo info;
    info.Data() = attachmentsMetaInfo;
    info.SetFrame(m_FramePosts);
	info.SetRBACPrivilege(m_FramePosts->GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Post, Command::ModuleTask::DownloadAttachments, info, { this, g_ActionNameSelectedAttachmentDownload, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
}

void GridPosts::OnViewPostItemAttachment()
{
    CommandInfo info;

    ASSERT(GetSelectedRows().size() == 1);
    if (GetSelectedRows().size() == 1)
    {
        auto& row = *(GetSelectedRows().begin());
        GridBackendField& groupIdField = row->GetField(m_ColMetaGroupId);
		GridBackendField& conversationIdField = row->GetField(m_ColMetaId);
		GridBackendField& threadIdField = row->GetField(m_ColConversationThreadId);
        GridBackendField& postIdField = row->GetField(GetColId());
        GridBackendField& attachmentIdField = row->GetField(m_ColAttachmentId);

        if (groupIdField.HasValue() && conversationIdField.HasValue() && threadIdField.HasValue() && postIdField.HasValue() && attachmentIdField.HasValue())
        {
            info.Data() = AttachmentsInfo::IDs(groupIdField.GetValueStr(), conversationIdField.GetValueStr(), threadIdField.GetValueStr(), postIdField.GetValueStr(), attachmentIdField.GetValueStr(), conversationIdField.GetValueStr());
            info.SetFrame(m_FramePosts);
            info.SetOrigin(Origin::Post);
			info.SetRBACPrivilege(m_FramePosts->GetModuleCriteria().m_Privilege);
			CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Post, Command::ModuleTask::ViewItemAttachment, info, { this, g_ActionNameSelectedPreviewItem, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
        }
    }
}

void GridPosts::OnDeleteAttachment()
{
	const auto deleteAttachment = [this](GridBackendRow* p_Row, const PooledString& p_attId)
	{
		AttachmentInfo info(*this, p_attId, p_Row);

		auto deletion = std::make_unique<PostAttachmentDeletion>(*this, info);
		deletion->ShowAppliedLocally(p_Row);
		if (GetModifications().Add(std::move(deletion)))
		{
			// Successfully added deletion? Remove error flag in case we just overwrote an old error with the new deletion
			p_Row->SetEditError(false);
		}
	};

	/*std::map<GridBackendRow*, bool> alreadyCheckedAncestors;
	boost::YOpt<bool> shouldRevertExistingModifications;
	const auto checkExistingMod = [this, &alreadyCheckedAncestors, &shouldRevertExistingModifications](GridBackendRow* p_Row)
	{
		if (alreadyCheckedAncestors.end() == alreadyCheckedAncestors.find(p_Row))
		{
			bool proceedWithDeletion = true;
			row_primary_key_t pk;
			GetRowPKNoDynamicColumns(p_Row, pk);

			auto mod = GetModifications().GetRowModification<RowModification>(pk);

			if (nullptr != mod)
			{
				if (!shouldRevertExistingModifications)
				{
					YCodeJockMessageBox errorMessage(this,
						DlgMessageBox::eIcon_Question,
						YtriaTranslate::Do(GridMessages_OnDeleteAttachment_1, _YLOC("Delete Attachments")).c_str(),
						YtriaTranslate::Do(GridMessages_OnDeleteAttachment_2, _YLOC("Some message are flagged for delete. Do you want to restore them and delete attachments instead?")).c_str(),
						_YTEXT(""),
						{ { IDOK, YtriaTranslate::Do(GridMessages_OnDeleteAttachment_3, _YLOC("Yes")).c_str() },{ IDCANCEL, YtriaTranslate::Do(GridMessages_OnDeleteAttachment_4, _YLOC("No")).c_str() } });

					shouldRevertExistingModifications = errorMessage.DoModal() == IDOK;
				}

				ASSERT(shouldRevertExistingModifications);
				if (shouldRevertExistingModifications && *shouldRevertExistingModifications)
				{
					GetModifications().RevertRowModifications(pk);
					proceedWithDeletion = true;
				}
				else
				{
					proceedWithDeletion = false;
				}
			}

			alreadyCheckedAncestors[p_Row] = proceedWithDeletion;
		}

		return alreadyCheckedAncestors[p_Row];
	};*/

	std::vector<GridBackendRow*> rows;
	GetSelectedNonGroupRows(rows);
	TemporarilyImplodeOutOfSisterhoodColumns(*this, m_ColAttachmentId, rows, [=](const bool p_HasRowsToReExplode)
	{
		for (auto row : GetSelectedRows())
		{
			if (GridUtil::IsBusinessPost(row)
				&& IsGroupAuthorizedByRoleDelegation(row->GetField(m_ColMetaGroupId).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CONVERSATIONS_ATTACHMENTS_EDIT))
			{
				auto& field = row->GetField(m_ColAttachmentId);
				if (field.HasValue())
				{
					if (GetMultivalueManager().IsRowExploded(row, m_ColAttachmentId))
					{
						ASSERT(row->IsExplosionFragment());
						ASSERT(!field.IsMulti());
						if (field.HasValue())
						{
							auto topRow = GetMultivalueManager().GetTopSourceRow(row);
							//if (checkExistingMod(topRow))
								deleteAttachment(row, field.GetValueStr());
						}
					}
					else
					{
						if (field.IsMulti())
						{
							std::vector<PooledString>* attachmentIds = row->GetField(m_ColAttachmentId).GetValuesMulti<PooledString>();
							ASSERT(nullptr != attachmentIds);
							if (nullptr != attachmentIds)
							{
								//if (checkExistingMod(row))
								{
									for (const auto& attachmentId : *attachmentIds)
										deleteAttachment(row, attachmentId);
								}
							}
						}
						else
						{
							//if (checkExistingMod(row))
								deleteAttachment(row, field.GetValueStr());
						}
					}

					GetModifications().ShowSubItemModificationAppliedLocally(row);
				}
			}
		}

		if (!p_HasRowsToReExplode)
			UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
	})();
}

void GridPosts::OnToggleInlineAttachments()
{
    if (!m_ShowInlineAttachments)
    {
        SetShowInlineAttachments(true);
        ModuleUtil::ClearModifiedRowsStatus(*this);
        ViewAttachments(m_AllAttachments, false, false);
    }
}

void GridPosts::OnUpdateShowPostAttachments(CCmdUI* pCmdUI)
{
	bool enable = false;

    if (IsFrameConnected() && hasNoTaskRunning())
    {
        wstring metaID;
        for (auto& row : GetSelectedRows())
        {
            if (GridUtil::isBusinessType<BusinessPost>(row) && IsGroupAuthorizedByRoleDelegation(row->GetField(m_ColMetaGroupId).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CONVERSATIONS_LOADMORE))
            {
                metaID = row->GetField(m_ColMetaId).GetValueStr();
                if (!metaID.empty() && !row->IsMoreLoaded())
                {
                    enable = true;
                    break;
                }
            }
        }
    }

    pCmdUI->Enable(enable ? TRUE : FALSE);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridPosts::OnUpdateDownloadPostAttachments(CCmdUI* pCmdUI)
{
    BOOL enable = FALSE;

	if (IsFrameConnected() && hasNoTaskRunning())
    {
        // Enable only if attachment(s) are loaded
        for (auto& row : GetSelectedRows())
        {
            if (GridUtil::isBusinessType<BusinessPost>(row) && IsGroupAuthorizedByRoleDelegation(row->GetField(m_ColMetaGroupId).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CONVERSATIONS_ATTACHMENTS_DOWNLOAD))
            {
                const auto& aType = row->GetField(m_ColAttachmentType);
                if (aType.HasValue())
                {
                    if (aType.IsMulti())
                    {
                        if (aType.MultiValueContains<PooledString>(Util::GetFileAttachmentStr()))
                        {
                            enable = TRUE;
                            break;
                        }
                    }
                    else if (Util::GetFileAttachmentStr() == aType.GetValueStr())
                    {
                        enable = TRUE;
                        break;
                    }
                }
            }
        }
    }

    pCmdUI->Enable(enable);
    setTextFromProfUISCommand(*pCmdUI);
}

void GridPosts::OnUpdatePostItemAttachment(CCmdUI* pCmdUI)
{
    setTextFromProfUISCommand(*pCmdUI);

    BOOL enable = FALSE;
	if (hasNoTaskRunning() && GetSelectedRows().size() == 1)
    {
        GridBackendRow* row = *(GetSelectedRows().begin());
        if (GridUtil::isBusinessType<BusinessPost>(row))
        {
            BusinessAttachment attachment;

            wstring conversationId = row->GetField(m_ColMetaId).GetValueStr();
            wstring postId = row->GetField(GetColId()).GetValueStr();
            wstring attachmentId = row->GetField(m_ColAttachmentId).GetValueStr();
            wstring attachmentType = row->GetField(m_ColAttachmentType).GetValueStr();

            if (!conversationId.empty() && !postId.empty() && !attachmentId.empty() && attachmentType == Util::GetItemAttachmentStr())
                enable = TRUE;
        }
    }

    pCmdUI->Enable(enable);
}

void GridPosts::OnUpdateDeleteAttachment(CCmdUI* pCmdUI)
{
    // Enable only if all necessary info for deleting the attachment(s) are loaded
	BOOL enable = FALSE;
	if (hasNoTaskRunning()
		&& std::any_of(GetSelectedRows().begin(), GetSelectedRows().end(), [this](auto& p_Row)
		{
			return GridUtil::isBusinessType<BusinessPost>(p_Row)
				&& p_Row->GetField(m_ColAttachmentId).HasValue()
				&& !PooledString(p_Row->GetField(m_ColMetaGroupId).GetValueStr()).IsEmpty()
				&& !PooledString(p_Row->GetField(m_ColMetaId).GetValueStr()).IsEmpty()
				&& !PooledString(p_Row->GetField(GetColId()).GetValueStr()).IsEmpty()
				&& IsGroupAuthorizedByRoleDelegation(p_Row->GetField(m_ColMetaGroupId).GetValueStr(), false, RoleDelegationUtil::RBAC_Privilege::RBAC_GROUP_CONVERSATIONS_ATTACHMENTS_EDIT);
			}))
		enable = TRUE;

    pCmdUI->Enable(enable);
	setTextFromProfUISCommand(*pCmdUI);
}

void GridPosts::OnUpdateToggleInlineAttachments(CCmdUI* pCmdUI)
{
    setTextFromProfUISCommand(*pCmdUI);
    pCmdUI->Enable(hasNoTaskRunning() && !m_ShowInlineAttachments && !m_AllAttachments.empty());
}

BOOL GridPosts::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return ModuleO365Grid<BusinessPost>::PreTranslateMessage(pMsg);
}

void GridPosts::InitializeCommands()
{
    ModuleO365Grid<BusinessPost>::InitializeCommands();

	std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

    const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_POSTSGRID_SHOWBODY;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridPosts_InitializeCommands_1, _YLOC("Preview Body...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridPosts_InitializeCommands_2, _YLOC("Preview Body...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGE_CONTENT_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_POSTSGRID_SHOWBODY, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridPosts_InitializeCommands_3, _YLOC("Preview Post Body")).c_str(),
			YtriaTranslate::Do(GridPosts_InitializeCommands_4, _YLOC("Preview the post message body.\nOnly one post can be previewed at a time.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_POSTSGRID_SHOWATTACHMENTS;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridPosts_InitializeCommands_5, _YLOC("Load Attachment Info")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridPosts_InitializeCommands_6, _YLOC("Load Attachment Info")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGES_SHOWATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_POSTSGRID_SHOWATTACHMENTS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridPosts_InitializeCommands_7, _YLOC("Load Attachment Info")).c_str(),
			YtriaTranslate::Do(GridPosts_InitializeCommands_8, _YLOC("Load the additional attachment info for all selected rows.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_POSTSGRID_DOWNLOADATTACHMENTS;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridPosts_InitializeCommands_9, _YLOC("Download Attachments...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridPosts_InitializeCommands_10, _YLOC("Download Attachments...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGES_DOWNLOADATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_POSTSGRID_DOWNLOADATTACHMENTS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridPosts_InitializeCommands_11, _YLOC("Download Attachments")).c_str(),
			YtriaTranslate::Do(GridPosts_InitializeCommands_12, _YLOC("Download the attachment files of all selected items to your computer.\r\nYou can select a destination folder in the resulting dialog.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID		= ID_POSTSGRID_VIEWITEMATTACHMENT;
		_cmd.m_sMenuText	= YtriaTranslate::Do(GridPosts_InitializeCommands_13, _YLOC("Preview Item...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridPosts_InitializeCommands_14, _YLOC("Preview Item...")).c_str();
		_cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGES_VIEWITEMATTACHMENT_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_POSTSGRID_VIEWITEMATTACHMENT, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridPosts_InitializeCommands_15, _YLOC("Preview Attached Item")).c_str(),
			YtriaTranslate::Do(GridPosts_InitializeCommands_16, _YLOC("Preview the body of an attached email, vCard, or calendar.\nYou must first load the attachment info and select the item in the grid for this function to be active.\nOnly one item can be previewed at a time.")).c_str(),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
        _cmd.m_nCmdID		= ID_POSTSGRID_DELETEATTACHMENT;
        _cmd.m_sMenuText	= YtriaTranslate::Do(GridPosts_InitializeCommands_17, _YLOC("Delete Attachment")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridPosts_InitializeCommands_18, _YLOC("Delete Attachment")).c_str();
        _cmd.m_sAccelText	= MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_MESSAGES_DELETEATTACHMENTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_POSTSGRID_DELETEATTACHMENT, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridPosts_InitializeCommands_19, _YLOC("Delete Attachment")).c_str(),
			YtriaTranslate::Do(GridPosts_InitializeCommands_20, _YLOC("Delete all selected attachment files.")).c_str(),
			_cmd.m_sAccelText);
    }

    {
        CExtCmdItem _cmd;
        _cmd.m_nCmdID = ID_POSTSGRID_TOGGLEINLINEATTACHMENTS;
        _cmd.m_sMenuText = YtriaTranslate::Do(GridPosts_InitializeCommands_21, _YLOC("Show Inline Attachments")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridPosts_InitializeCommands_22, _YLOC("Show Inline Attachments")).c_str();
        _cmd.m_sAccelText = _YTEXT("");
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

        HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_POSTS_TOGGLEINLINEATTACHMENTS_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_POSTSGRID_TOGGLEINLINEATTACHMENTS, hIcon, false);

        setRibbonTooltip(_cmd.m_nCmdID,
            YtriaTranslate::Do(GridPosts_InitializeCommands_23, _YLOC("Show Inline Attachments")).c_str(),
            YtriaTranslate::Do(GridPosts_InitializeCommands_24, _YLOC("Include inline attachments in the grid.\nBy default, inline attachments, like embedded images, are not listed.\nTo go back to default, use the Refresh All button.")).c_str(),
            _cmd.m_sAccelText);
    }
}

void GridPosts::OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart)
{
    ModuleO365Grid<BusinessPost>::OnCustomPopupMenu(pPopup, nStart);

    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {
        pPopup->ItemInsert(ID_POSTSGRID_SHOWBODY);
		pPopup->ItemInsert(); // Separator
        pPopup->ItemInsert(ID_POSTSGRID_SHOWATTACHMENTS);
		pPopup->ItemInsert(ID_POSTSGRID_TOGGLEINLINEATTACHMENTS);
        pPopup->ItemInsert(ID_POSTSGRID_DOWNLOADATTACHMENTS);
        pPopup->ItemInsert(ID_POSTSGRID_VIEWITEMATTACHMENT);
        pPopup->ItemInsert(ID_POSTSGRID_DELETEATTACHMENT);
        pPopup->ItemInsert(); // Separator
    }
}

GridBackendRow* GridPosts::fillRow(const BusinessPost& p_Post, const ModuleUtil::PostMetaInfo& p_PostInfo)
{
	GridBackendField fieldGroupId(p_PostInfo.GroupId, m_ColMetaGroupId);
	GridBackendField fieldConvId(p_PostInfo.ConversationId, m_ColMetaId);
	GridBackendField fieldThreadId(p_PostInfo.ThreadId, m_ColConversationThreadId);
    GridBackendField fieldPostId(p_Post.GetID(), GetColId());

    auto postRow = m_RowIndex.GetRow({ fieldGroupId, fieldConvId, fieldThreadId, fieldPostId }, true, true);

    ASSERT(nullptr != postRow);
    if (nullptr != postRow)
    {
        postRow->AddField(p_PostInfo.ConversationTopic, m_ColMetaDisplayName);
        //auto itt = m_ThreadsTopics.find(fieldThreadId.GetValueStr());
        //ASSERT(itt != m_ThreadsTopics.end());
        //if (itt != m_ThreadsTopics.end())
        //    postRow->AddField(itt->second, m_ColMetaDisplayName);

		const auto gp = GetGraphCache().GetGroupInCache(p_PostInfo.GroupId);
		ASSERT(!gp.GetDisplayName().IsEmpty());
        postRow->AddField(gp.GetDisplayName(), m_ColMetaGroupName);
		updateRow(p_Post, postRow);
    }

    return postRow;
}

void GridPosts::updateRow(const BusinessPost& p_Post, GridBackendRow* p_PostRow)
{
    ASSERT(nullptr != p_PostRow);
    if (nullptr != p_PostRow)
    {
		SetRowObjectType(p_PostRow, p_Post, p_Post.HasFlag(BusinessObject::CANCELED));
        //p_PostRow->AddField(p_Post.GetID(), GetColId());
        p_PostRow->AddField(p_Post.GetCreatedDateTime(), m_ColCreatedDateTime);
        p_PostRow->AddField(p_Post.GetLastModifiedDateTime(), m_ColLastModifiedDateTime);
        p_PostRow->AddField(p_Post.GetChangeKey(), m_ColChangeKey);
        p_PostRow->AddField(p_Post.GetCategories(), m_ColCategories);
        p_PostRow->AddField(p_Post.GetReceivedDateTime(), m_ColReceivedDateTime);
        p_PostRow->AddField(p_Post.GetHasAttachments(), m_ColHasGraphAttachment);
        
        if (p_Post.GetFrom() && p_Post.GetFrom()->m_EmailAddress)
        {
            const auto& emailAddress = p_Post.GetFrom()->m_EmailAddress;
            p_PostRow->AddField(emailAddress->m_Name, m_ColFromName);
            p_PostRow->AddField(emailAddress->m_Address, m_ColFromAddress);
        }
		else
		{
			p_PostRow->RemoveField(m_ColFromName);
			p_PostRow->RemoveField(m_ColFromAddress);
		}
        
        if (p_Post.GetSender() && p_Post.GetSender()->m_EmailAddress)
        {
            const auto& emailAddress = p_Post.GetSender()->m_EmailAddress;
            p_PostRow->AddField(emailAddress->m_Name, m_ColSenderName);
            p_PostRow->AddField(emailAddress->m_Address, m_ColSenderAddress);
        }
		else
		{
			p_PostRow->RemoveField(m_ColSenderName);
			p_PostRow->RemoveField(m_ColSenderAddress);
		}
        p_PostRow->AddField(p_Post.GetConversationThreadId(), m_ColConversationThreadId);

        auto newParticipants = GridHelpers::ExtractEmailAddresses(p_Post.GetNewParticipants());
        p_PostRow->AddField(newParticipants.first, m_ColNewParticipantsAddress);
        p_PostRow->AddField(newParticipants.second, m_ColNewParticipantsName);
        //p_PostRow->AddField(p_Post.GetConversationId(), m_ColConversationId);

		const boost::YOpt<PooledString>& bodyContent = p_Post.GetBody() ? p_Post.GetBody()->Content : boost::none;
		const boost::YOpt<PooledString>& bodyContentType = p_Post.GetBody() ? p_Post.GetBody()->ContentType : boost::none;
		p_PostRow->AddField(bodyContentType, m_ColBodyContentType);
		p_PostRow->AddField((bodyContentType && *bodyContentType == _YTEXT("html")) ? GridUtil::ExtractHtmlBodyPreview(bodyContent) : bodyContent, m_ColBodyPreview);

        MessageBodyData* messageBodyData = (MessageBodyData*)p_PostRow->GetLParam();
        if (nullptr != messageBodyData)
        {
            if (bodyContent)
            {
                messageBodyData->m_BodyContent = bodyContent;
            }
            else
            {
                p_PostRow->SetLParam(NULL);
                delete messageBodyData;
            }
        }
        else
        {
			if (bodyContent)
				p_PostRow->SetLParam((LPARAM)new MessageBodyData(bodyContent));
        }
    }
}

void GridPosts::OnShowBody()
{
    if (nullptr != m_MessageViewer)
        m_MessageViewer->ToggleView();
}

void GridPosts::OnUpdateShowBody(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && GridUtil::IsSelectionAtLeastOneOfType<BusinessPost>(*this));
	pCmdUI->SetCheck(nullptr != m_MessageViewer && m_MessageViewer->IsShowing());

	setTextFromProfUISCommand(*pCmdUI);
}

void GridPosts::fillAttachmentFields(GridBackendRow* p_Row, const vector<BusinessAttachment>& p_Attachments)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		vector<PooledString>	attachmentType;
		vector<PooledString>	attachmentId;
		vector<PooledString>	attachmentName;
		vector<PooledString>	attachmentContentType;
		vector<__int3264>		attachmentSize;
		vector<YTimeDate>		attachmentLastModifiedDateTime;
		vector<BOOLItem>		attachmentIsInline;
		vector<PooledString>	attachmentExt;
		vector<PooledString>	attachmentNoDotExt;
		int32_t					fileTypeIcon = NO_ICON;

        for (const auto& attachment : p_Attachments)
        {
            ASSERT(attachment.GetIsInline().is_initialized());
            if (attachment.GetIsInline().is_initialized())
            {
                if (m_ShowInlineAttachments || (!m_ShowInlineAttachments && attachment.GetDataType() &&
                    !(*attachment.GetIsInline() == true &&
                        *attachment.GetDataType() == Util::GetFileAttachmentStr())))
                {
                    attachmentType.push_back(attachment.GetDataType() ? *attachment.GetDataType() : GridBackendUtil::g_NoValueString);
                    attachmentId.push_back(attachment.GetID());
                    attachmentName.push_back(attachment.GetName() ? *attachment.GetName() : GridBackendUtil::g_NoValueString);
                    attachmentContentType.push_back(attachment.GetContentType() ? *attachment.GetContentType() : GridBackendUtil::g_NoValueString);
                    attachmentSize.push_back(attachment.GetSize() ? static_cast<__int3264>(*attachment.GetSize()) : GridBackendUtil::g_NoValueNumberInt);
                    attachmentLastModifiedDateTime.push_back(attachment.GetLastModifiedDateTime() ? *attachment.GetLastModifiedDateTime() : GridBackendUtil::g_NoValueDate);
                    if (attachment.GetIsInline())
                        attachmentIsInline.push_back(*attachment.GetIsInline() ? TRUE : FALSE);
                    else
                        attachmentIsInline.push_back(GridBackendUtil::g_NoValueBoolItem);

					if (attachment.GetName())
					{
						const auto data = GetFileTypeAndIcon(*attachment.GetName(), false);
						attachmentNoDotExt.push_back(std::get<0>(data));
						attachmentExt.push_back(std::get<1>(data));
						if (p_Attachments.size() == 1)
							fileTypeIcon = std::get<2>(data);
					}
					else
					{
						attachmentNoDotExt.push_back(GridBackendUtil::g_NoValueString);
						attachmentExt.push_back(GridBackendUtil::g_NoValueString);
					}
                }
            }
        }

        p_Row->AddField(!p_Attachments.empty(), m_ColHasAnyAttachment);
		if (!p_Attachments.empty())
		{
			auto& attachmentTypeField = p_Row->AddField(attachmentType, m_ColAttachmentType);
			if (attachmentType.size() == 1)
                GridUtil::SetAttachmentIcon(attachmentTypeField, m_Icons);

			p_Row->AddField(attachmentId, m_ColAttachmentId);
			p_Row->AddField(attachmentName, m_ColAttachmentName);
			p_Row->AddField(attachmentContentType, m_ColAttachmentContentType);
			p_Row->AddField(attachmentSize, m_ColAttachmentSize);
			p_Row->AddField(attachmentLastModifiedDateTime, m_ColAttachmentLastModifiedDateTime);
			if (attachmentIsInline.size() == 1)
				p_Row->AddFieldForCheckBox(TRUE == attachmentIsInline.back().m_BOOL ? true : false, m_ColAttachmentIsInline);
			else
				p_Row->AddField(attachmentIsInline, m_ColAttachmentIsInline);

			p_Row->AddField(attachmentNoDotExt, GetColumnFileType(), fileTypeIcon);
			p_Row->AddField(attachmentExt, m_ColAttachmentNameExtOnly);
		}
		else
		{
			p_Row->RemoveField(m_ColAttachmentType);
			p_Row->RemoveField(m_ColAttachmentId);
			p_Row->RemoveField(m_ColAttachmentName);
			p_Row->RemoveField(m_ColAttachmentContentType);
			p_Row->RemoveField(m_ColAttachmentSize);
			p_Row->RemoveField(m_ColAttachmentLastModifiedDateTime);
			p_Row->RemoveField(m_ColAttachmentIsInline);

			p_Row->RemoveField(GetColumnFileType());
			p_Row->RemoveField(m_ColAttachmentNameExtOnly);
		}
	}
}

void GridPosts::onPostExplodeMultiValueSisterhood(GridBackendRow* p_Row, const std::set<GridBackendColumn*>& p_Columns)
{
	if (p_Columns.end() != p_Columns.find(m_ColAttachmentType))
	{
		GridUtil::SetAttachmentIcon(p_Row->GetField(m_ColAttachmentType), m_Icons);
		SetRowFileType(p_Row, p_Row->GetField(m_ColAttachmentName).GetValueStr(), false);
	}
}

void GridPosts::BuildViewWithAttachments(const O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>>& p_Posts, const vector<O365UpdateOperation>& p_Updates, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge)
{
	// Full purge with updates will fail.
	ASSERT(!p_FullPurge || p_Updates.empty());

	const uint32_t options = (p_Refresh ? GridUpdaterOptions::UPDATE_GRID : 0)
		| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
		| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		| GridUpdaterOptions::REFRESH_PROCESS_DATA
		/*| GridUpdaterOptions::PROCESS_LOADMORE*/
		| (!p_Updates.empty() ? GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG : 0)
		;

	GridUpdater updater(*this, GridUpdaterOptions(options), p_Updates);
	GridIgnoreModification ignoramus(*this);
	for (const auto& conversationPost : p_Posts)
	{
		const auto& posts = conversationPost.second;

		GridBackendField fieldGroupId(conversationPost.first.GroupId, m_ColMetaGroupId);
		GridBackendField fieldConvId(conversationPost.first.ConversationId, m_ColMetaId);
		GridBackendField fieldThreadId(conversationPost.first.ThreadId, m_ColConversationThreadId);
		GridBackendField fieldGraphId(conversationPost.first.ThreadId, GetColId());

		GridBackendRow* conversationRow = m_RowIndex.GetRow({ fieldGroupId, fieldConvId, fieldThreadId, fieldGraphId }, true, true);
		ASSERT(nullptr != conversationRow);
		if (nullptr != conversationRow)
		{
			updater.GetOptions().AddRowWithRefreshedValues(conversationRow);
			updater.GetOptions().AddPartialPurgeParentRow(conversationRow);

			const auto gp = GetGraphCache().GetGroupInCache(conversationPost.first.GroupId);
			ASSERT(!gp.GetDisplayName().IsEmpty());
			conversationRow->AddField(gp.GetDisplayName(), m_ColMetaGroupName);

			auto itt = m_ThreadsTopics.find(fieldThreadId.GetValueStr());
			ASSERT(itt != m_ThreadsTopics.end());
			if (itt != m_ThreadsTopics.end())
			{
				conversationRow->AddField(itt->second, m_ColMetaDisplayName);
			}
			conversationRow->SetHierarchyParentToHide(true);

			// FIXME: Change p_Posts so that it contains BusinessConversationThread objects to handle cancel.
			SetRowObjectType(conversationRow, BusinessConversationThread()/*, BusinessConversationThread().HasFlag(BusinessObject::CANCELED)*/);

			//if (!BusinessConversationThread().HasFlag(BusinessObject::CANCELED))
			{
				if (posts.size() == 1 && posts[0].GetError())
				{
					updater.OnLoadingError(conversationRow, *posts[0].GetError());
				}
				else
				{
					// Clear previous error
					if (conversationRow->IsError())
						conversationRow->ClearStatus();

					for (const auto& post : posts)
					{
						GridBackendRow* postRow = fillRow(post, conversationPost.first);
						ASSERT(nullptr != postRow);
						if (nullptr != postRow)
						{
							updater.GetOptions().AddRowWithRefreshedValues(postRow);
							postRow->SetParentRow(conversationRow);
						}
					}
				}
			}
		}
	}

	loadAttachments(p_BusinessAttachments, updater);
}

void GridPosts::BuildView(const O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>>& p_Posts, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_FullPurge)
{
	BuildViewWithAttachments(p_Posts, p_Updates, {}, p_Refresh, p_FullPurge);
}
