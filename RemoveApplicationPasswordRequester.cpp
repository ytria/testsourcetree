#include "RemoveApplicationPasswordRequester.h"
#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

RemoveApplicationPasswordRequester::RemoveApplicationPasswordRequester(const wstring& p_AppId, const wstring& p_KeyId)
	:m_AppId(p_AppId),
	m_KeyId(p_KeyId)
{
}

TaskWrapper<void> RemoveApplicationPasswordRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri;
	uri.append_path(_YTEXT("applications"));
	uri.append_path(m_AppId);
	uri.append_path(_YTEXT("removePassword"));

	web::json::value payloadJson = web::json::value::object();
	payloadJson.as_object()[_YTEXT("keyId")] = web::json::value::string(m_KeyId);

	WebPayloadJSON payload(payloadJson);
	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_uri(), httpLogger, p_TaskData, payload).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResInfo) {
		try
		{
			result->SetResult(p_ResInfo.get());
		}
		catch (const RestException& e)
		{
			RestResultInfo info(e.GetRequestResult());
			info.m_RestException = std::make_shared<RestException>(e);
			result->SetResult(info);
		}
		catch (const std::exception& e)
		{
			RestResultInfo info;
			info.m_Exception = e;
			result->SetResult(info);
		}
	});
}

const SingleRequestResult& RemoveApplicationPasswordRequester::GetResult() const
{
	return *m_Result;
}
