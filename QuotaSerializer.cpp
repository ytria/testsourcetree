#include "QuotaSerializer.h"

QuotaSerializer::QuotaSerializer(const Quota& p_Quota)
	:m_Quota(p_Quota)
{
}

void QuotaSerializer::Serialize()
{
	m_Json = web::json::value::object();
	auto& obj = m_Json.as_object();

	JsonSerializeUtil::SerializeString(_YTEXT("state"), m_Quota.State, obj);
	JsonSerializeUtil::SerializeInt64(_YTEXT("total"), m_Quota.Total, obj);
	JsonSerializeUtil::SerializeInt64(_YTEXT("used"), m_Quota.Used, obj);
	JsonSerializeUtil::SerializeInt64(_YTEXT("remaining"), m_Quota.Remaining, obj);
	JsonSerializeUtil::SerializeInt64(_YTEXT("deleted"), m_Quota.Deleted, obj);
}
