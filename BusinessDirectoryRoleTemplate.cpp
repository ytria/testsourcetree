#include "BusinessDirectoryRoleTemplate.h"
#include "DirectoryRoleTemplate.h"
#include "MsGraphFieldNames.h"

RTTR_REGISTRATION
{
    using namespace rttr;

    registration::class_<BusinessDirectoryRoleTemplate>("DirectoryRoleTemplate") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Directory Role"))))
		.constructor()(policy::ctor::as_object)
		.property(O365_DIRECTORYROLETEMPLATE_ID, &BusinessDirectoryRoleTemplate::m_Id)
		.property(O365_DIRECTORYROLETEMPLATE_DISPLAYNAME, &BusinessDirectoryRoleTemplate::m_DisplayName)
		.property(O365_DIRECTORYROLETEMPLATE_DESCRIPTION, &BusinessDirectoryRoleTemplate::m_Description)
		;
}

BusinessDirectoryRoleTemplate::BusinessDirectoryRoleTemplate(const DirectoryRoleTemplate& p_DirectoryRoleTemplate)
{
    SetID(p_DirectoryRoleTemplate.m_Id);
    SetDescription(p_DirectoryRoleTemplate.Description);
    SetDisplayName(p_DirectoryRoleTemplate.DisplayName);
}

const boost::YOpt<PooledString>& BusinessDirectoryRoleTemplate::GetDescription() const
{
    return m_Description;
}

void BusinessDirectoryRoleTemplate::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
    m_Description = p_Val;
}

const boost::YOpt<PooledString>& BusinessDirectoryRoleTemplate::GetDisplayName() const
{
    return m_DisplayName;
}

void BusinessDirectoryRoleTemplate::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
    m_DisplayName = p_Val;
}
