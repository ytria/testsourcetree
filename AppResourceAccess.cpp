#include "AppResourceAccess.h"
#include "CRMpipe.h"

web::json::value AppResourceAccess::GetUltraAdminResourceAccess()
{
	auto obj = web::json::value::object();

	obj[_YTEXT("resourceAppId")] = web::json::value::string(_YTEXT("00000003-0000-0000-c000-000000000000"));
	obj[_YTEXT("resourceAccess")] = web::json::value::array();

	auto& resAccessObj = obj[_YTEXT("resourceAccess")].as_array();
	size_t idx = 0;
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("e1fe6dd8-ba31-4d61-89e7-88639da4683d")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Scope")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("ef5f7d5c-338f-44b0-86c3-351f46c8bb5f")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("1bfefb4e-e0b5-418b-a88f-73c46d2cc8e9")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("b0afded3-3588-46d8-8b3d-9842eff778da")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("ef54d2bf-783f-4e0f-bca1-3210c0444d99")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("7b2449af-6ccd-4f4d-9f78-e550c193f0d1")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("294ce7c9-31ba-490a-ad7d-97a7d075e4ed")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("6918b873-d17a-4dc1-b314-35f528134491")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("1138cb37-bd11-4084-a2b7-9f71582aeddb")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("19dbc75e-c2e2-444c-a770-ec69d8559fc7")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("75359482-378d-4052-8f01-80520e7db3cd")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("62a82d76-70ea-41e2-9197-370581804d09")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("e2a3a72e-5f79-4c64-b1b1-878b674786c9")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("b633e1c5-b582-4048-a93e-9f11b44c7e96")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("6931bccd-447a-43d1-b442-00a195474933")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("658aa5d8-239f-45c4-aa12-864f4fc7e490")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("0c458cef-11f3-48c2-a568-c66751c238c0")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("b528084d-ad10-4598-8b93-929746b4d7d6")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("60a901ed-09f7-4aa5-a16e-7dd3d6f9de36")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("230c1aed-a721-4c5d-9cb4-a90514e508ef")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("f2bf083f-0179-402a-bedb-b2784de8a49b")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("d903a879-88e0-4c09-b0c9-82f6a1333f84")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("a82116e5-55eb-4c41-a434-62fe8a61c773")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	resAccessObj[idx++] = web::json::value::object({
		{ _YTEXT("id"), web::json::value::string(_YTEXT("741f803b-c850-494e-b5df-cde7c675a1ca")) },
		{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});

	// EduRoster.ReadWrite.All
	if (CRMpipe().IsFeatureSandboxed())
	{
		resAccessObj[idx++] = web::json::value::object({
			{ _YTEXT("id"), web::json::value::string(_YTEXT("d1808e82-ce13-47af-ae0d-f9b254e6d58a")) },
			{ _YTEXT("type"), web::json::value::string(_YTEXT("Role")) },
		});
	}

	return obj;
}
