#include "CheckoutStatusRequester.h"

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "ListItemsFieldsForCheckoutStatusDeserializer.h"
#include "MSGraphCommonData.h"
#include "MsGraphHttpRequestLogger.h"
#include "Sapio365Session.h"

CheckoutStatusRequester::CheckoutStatusRequester(wstring p_DriveId, wstring p_ItemId, const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_DriveId(p_DriveId)
	, m_ItemId(p_ItemId)
	, m_Logger(p_Logger)
{
}

TaskWrapper<void> CheckoutStatusRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ListItemsFieldsForCheckoutStatusDeserializer>();

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::APP_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObjects(m_Deserializer
		, { Rest::DRIVES_PATH, m_DriveId, Rest::ITEMS_PATH, m_ItemId, Rest::LISTITEM_PATH }
		, { { _YTEXT("expand"), _YTEXT("fields(select=CheckoutUser,_CheckinComment,_ComplianceTag,_ComplianceTagWrittenTime)") } }
		, false
		, m_Logger
		, httpLogger
		, p_TaskData);
}

const CheckoutStatus& CheckoutStatusRequester::GetCheckoutStatus() const
{
	ASSERT(m_Deserializer);
	return m_Deserializer->GetData();
}
