#include "EducationClassListRequester.h"

#include "EducationClassDeserializer.h"
#include "LoggerService.h"
#include "PaginatorUtil.h"
#include "Sapio365Session.h"
#include "BasicPageRequestLogger.h"
#include "MsGraphHttpRequestLogger.h"

EducationClassListRequester::EducationClassListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger)
	: m_Logger(p_Logger)
{
}

EducationClassListRequester::EducationClassListRequester(const wstring& p_SchoolId, const std::shared_ptr<IPageRequestLogger>& p_Logger)
: m_SchoolId(p_SchoolId)
, m_Logger(p_Logger)
{
}

TaskWrapper<void> EducationClassListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<EducationClass, EducationClassDeserializer>>();

	web::uri_builder uri(_YTEXT("education"));
	if (!m_SchoolId.empty())
	{
		uri.append_path(_YTEXT("schools"));
		uri.append_path(m_SchoolId);
	}
	uri.append_path(_YTEXT("classes"));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	MsGraphPaginator paginator(uri.to_uri(), Util::CreateDefaultGraphPageRequester(m_Deserializer, httpLogger, p_TaskData.GetOriginator()), m_Logger);
	return paginator.Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

const vector<EducationClass>& EducationClassListRequester::GetData() const
{
	return m_Deserializer->GetData();
}
