#pragma once

#include "BaseO365Grid.h"
#include "BusinessConversation.h"
#include "GridTemplateGroups.h"
#include "ModuleUtil.h"

using GridConversationsBaseClass = ModuleO365Grid<BusinessConversation>;
class GridConversations : public GridConversationsBaseClass
{
public:
    GridConversations();
    void BuildView(const O365DataMap<BusinessGroup, vector<BusinessConversation>>& p_Conversations, bool p_FullPurge);
    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

	virtual wstring GetName(const GridBackendRow* p_Row) const override;

protected:
    virtual void customizeGrid() override;

	virtual BusinessConversation getBusinessObject(GridBackendRow*) const override;
	virtual void UpdateBusinessObjects(const vector<BusinessConversation>& p_Conversations, bool p_SetModifiedStatus) override;
	virtual void RemoveBusinessObjects(const vector<BusinessConversation>& p_Conversations) override;

    virtual void InitializeCommands() override;
    virtual void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart) override;

    DECLARE_MESSAGE_MAP()

private:
    afx_msg void OnShowConversationPosts();
    afx_msg void OnUpdateShowPosts(CCmdUI* pCmdUI);

	GridBackendRow* fillRow(const BusinessConversation& p_Conversation, GridBackendRow* p_ParentRow, GridUpdater& p_Updater);

	void updateRow(const BusinessConversation& p_Conversation, GridBackendRow* p_Row, GridUpdater* p_Updater);
	bool updateRowForBusinessConversation(const BusinessConversation& p_Conversation, bool p_SetModifiedStatus);
	bool removeRowForBusinessConversation(const BusinessConversation& p_Conversation);

    void addThreadToInfo(GridBackendRow* threadRow, O365SubSubIdsContainer& p_ViewPostsInfo, std::map<PooledString, PooledString>& p_ThreadsTopic, std::set<PooledString>& p_UsedThreadIds);

	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

private:
	GridTemplateGroups m_Template;

    GridBackendColumn* m_ColTopic;
    GridBackendColumn* m_ColHasAttachments;
    GridBackendColumn* m_ColLastDeliveredDateTime;
    GridBackendColumn* m_ColUniqueSenders;
    GridBackendColumn* m_ColPreview;

    GridBackendColumn* m_ColCcRecipientsName;
    GridBackendColumn* m_ColCcRecipientsAddress;
    GridBackendColumn* m_ColToRecipientsName;
    GridBackendColumn* m_ColToRecipientsAddress;
    GridBackendColumn* m_ColIsLocked;

	vector<GridBackendColumn*>	m_MetaColumns;
	bool m_HasLoadMoreAdditionalInfo;
	O365IdsContainer m_MetaIDsMoreLoaded;
};