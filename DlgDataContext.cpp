#include "DlgDataContext.h"
#include "GridFrameBase.h"

BEGIN_MESSAGE_MAP(DlgDataContext, ResizableDialog)
    ON_BN_CLICKED(IDC_DLG_DATACONTEXT_ADDENTRIES,		OnAddEntries)
    ON_BN_CLICKED(IDC_DLG_DATACONTEXT_REMOVEENTRIES,	OnRemoveEntries)
END_MESSAGE_MAP()

DlgDataContext::DlgDataContext(const LinearMap<PooledString, PooledString>& p_Context, bool p_CanEdit, CWnd* p_Parent)
	: ResizableDialog(DlgDataContext::IDD, p_Parent)
	, m_Context1(p_Context)
	, m_Context1Copy(p_Context)
	, m_Context2(boost::none)
	, m_Grid(false)
	, m_Edit(p_CanEdit)
{
	if (m_Edit)
		m_Grid.AddSelectionObserver(this);
}

DlgDataContext::DlgDataContext(const LinearMap<PooledString, std::pair<PooledString, PooledString>>& p_Context, CWnd* p_Parent)
	: ResizableDialog(DlgDataContext::IDD, p_Parent)
	, m_Context1(boost::none)
	, m_Context2(p_Context)
	, m_Grid(true)
	, m_Edit(false)
{
}

const boost::YOpt<LinearMap<PooledString, PooledString>>& DlgDataContext::GetContext1() const
{
    return m_Context1;
}

void DlgDataContext::OnSelect()
{
	m_SelectedId = m_Grid.m_SelectedId;
	EndDialog(IDYES);
}

void DlgDataContext::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDOK, m_OkButton);
    DDX_Control(pDX, IDCANCEL, m_CancelButton);
    DDX_Control(pDX, IDC_DLG_DATACONTEXT_ADDENTRIES, m_AddEntriesButton);
    DDX_Control(pDX, IDC_DLG_DATACONTEXT_REMOVEENTRIES, m_RemoveEntriesButton);
	DDX_Control(pDX, ID_DLG_CONTEXT_TEXT, m_Label);
}

BOOL DlgDataContext::OnInitDialogSpecificResizable()
{
	GridFrameBase* pParent = dynamic_cast<GridFrameBase*>(GetParent());
	ASSERT(nullptr != pParent);
	wstring Title;
	if (pParent->GetOrigin() == Origin::User)
		Title = YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_9, _YLOC("Selected Users")).c_str();
	else if (pParent->GetOrigin() == Origin::Group)
		Title = YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_10, _YLOC("Selected Groups")).c_str();
	else if (pParent->GetOrigin() == Origin::Lists)
		Title = YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_11, _YLOC("Selected Lists")).c_str();
	else if (pParent->GetOrigin() == Origin::Site)
		Title = YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_12, _YLOC("Selected Sites")).c_str();
	else if (pParent->GetOrigin() == Origin::Conversations)
		Title = YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_13, _YLOC("Selected Conversations")).c_str();
	else
		ASSERT(FALSE);

	SetWindowText(Title.c_str());

	if (m_Edit)
	{
		m_OkButton.SetWindowText(YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_4, _YLOC("OK")).c_str());
		m_CancelButton.SetWindowText(YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_5, _YLOC("Cancel")).c_str());
	}
	else
	{
		m_AddEntriesButton.ShowWindow(SW_HIDE);
		m_RemoveEntriesButton.ShowWindow(SW_HIDE);
		m_OkButton.ShowWindow(SW_HIDE);

		m_CancelButton.SetWindowText(YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_6, _YLOC("OK")).c_str());
		m_OkButton.SetWindowText(_YTEXT(""));
	}

	m_OkButton.EnableWindow(FALSE);

	m_Label.SetWindowText(CString(YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_3, _YLOC("Double-click to select in grid.")).c_str()));
    m_AddEntriesButton.SetWindowText(YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_7, _YLOC("Add")).c_str());
	m_RemoveEntriesButton.SetWindowText(YtriaTranslate::Do(DlgDataContext_OnInitDialogSpecificResizable_8, _YLOC("Remove")).c_str());

	m_AddEntriesButton.EnableWindow(pParent->IsConnected());

	CRect aRect;
	CWnd* gridPlacement = GetDlgItem(ID_DLG_CONTEXT_GRID_PLACEMENT);
	ASSERT(nullptr != gridPlacement);
	if (nullptr != gridPlacement)
	{
		gridPlacement->GetWindowRect(&aRect);
		ScreenToClient(&aRect);
		if (!m_Grid.Create(this, aRect))
		{
			// Creation of grid failed.
			ASSERT(FALSE);
			return -1;
		}

		fillGrid();
		/*m_Grid.AutoResizeColumns(
			map<GridBackendColumn*, LONG>
			({
				{ m_Grid.m_NameColumn, -1 },
			}),
			CacheGrid::AutoResizeRule(true, true));
		
		m_Grid.m_IDColumn->SetAutoResizeWithGridResize(true);
		if (nullptr != m_Grid.m_DetailsColumn)
			m_Grid.SetColumnsAutoSizeWithGridResize({ m_Grid.m_IDColumn, m_Grid.m_DetailsColumn }, true);
		else
			m_Grid.SetColumnsAutoSizeWithGridResize({ m_Grid.m_IDColumn }, true);*/
	}

	AddAnchor(m_Label, __RDA_KEEP, __RDA_X);
	AddAnchor(m_AddEntriesButton, __RDA_KEEP, CSize(10, 0));
	AddAnchor(m_RemoveEntriesButton, CSize(10, 0), CSize(20, 0));
	AddAnchor(m_Grid, __RDA_KEEP, __RDA_XY);
	AddAnchor(m_OkButton, CSize(80, 100), CSize(90, 100));
	AddAnchor(m_CancelButton, CSize(90, 100), __RDA_XY);

	UpdateRemoveButton();

	return FALSE;
}

void DlgDataContext::OnAddEntries()
{
    GridFrameBase* frame = dynamic_cast<GridFrameBase*>(GetParentFrame());
    ASSERT(nullptr != frame);
    if (nullptr != frame && frame->IsConnected())
    {
        auto itemsToAdd = frame->AskForItemsToAdd(this);
        for (const auto& item : itemsToAdd)
        {
            m_Context1->push_back(item.GetID(), item.GetDisplayName());
            m_Grid.RemoveAllRows();
            fillGrid();
        }
    }

	UpdateOkButton();
	UpdateRemoveButton();
}

void DlgDataContext::OnRemoveEntries()
{
    if (boost::none != m_Context1)
    {
        const auto& selectedRows = m_Grid.GetSelectedRows();
        for (auto selectedRow : selectedRows)
        {
            const auto& id = selectedRow->GetField(m_Grid.m_IDColumn).GetValueStr();
            m_Context1->erase(id);

			// Keep at least one item
			if (m_Context1->size() == 1)
				break;
        }

        if (!selectedRows.empty())
        {
            m_Grid.RemoveAllRows();
            fillGrid();
        }
    }

	UpdateOkButton();
	UpdateRemoveButton();
}

void DlgDataContext::fillGrid()
{
	if (boost::none != m_Context1)
	{
		for (size_t i = 0; i < m_Context1->size(); ++i)
		{
			const auto& id = m_Context1->at(i);
			const auto& name = (*m_Context1)[id];

			auto row = m_Grid.AddRow();

			row->AddField(name, m_Grid.m_NameColumn->GetID());
			row->AddField(id, m_Grid.m_IDColumn->GetID());
		}
	}
	else if (boost::none != m_Context2)
	{
		for (size_t i = 0; i < m_Context2->size(); ++i)
		{
			const auto& id = m_Context2->at(i);
			const auto& data = (*m_Context2)[id];

			auto row = m_Grid.AddRow();

			row->AddField(data.first, m_Grid.m_NameColumn->GetID());
			row->AddField(id, m_Grid.m_IDColumn->GetID());
			ASSERT(nullptr != m_Grid.m_DetailsColumn);
			row->AddField(data.second, m_Grid.m_DetailsColumn->GetID());
		}
	}

	m_Grid.UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_NOFILTERS);

	m_Grid.AutoResizeColumns(
		map<GridBackendColumn*, LONG>
		({
			{ m_Grid.m_NameColumn, -1 },
	}),
	CacheGrid::AutoResizeRule(true, true));

	m_Grid.m_IDColumn->SetAutoResizeWithGridResize(true);
	if (nullptr != m_Grid.m_DetailsColumn)
		m_Grid.SetColumnsAutoSizeWithGridResize({ m_Grid.m_IDColumn, m_Grid.m_DetailsColumn }, true);
	else
		m_Grid.SetColumnsAutoSizeWithGridResize({ m_Grid.m_IDColumn }, true);
}

void DlgDataContext::UpdateRemoveButton()
{
	m_RemoveEntriesButton.EnableWindow(0 < m_Grid.GetSelectedRows().size() && m_Grid.GetSelectedRows().size() < m_Context1->size());
}

void DlgDataContext::UpdateOkButton()
{
	m_OkButton.EnableWindow(m_Context1Copy != m_Context1);
}

void DlgDataContext::SelectionChanged(const CacheGrid& grid)
{
	UpdateRemoveButton();
}

//==================================================================================================

GridDataContext::GridDataContext(bool useDetailsColumn)
	: CacheGrid(0, GridBackendUtil::CREATESORTING)
	, m_UseDetailsColumn(useDetailsColumn)
	, m_NameColumn(nullptr)
	, m_IDColumn(nullptr)
	, m_DetailsColumn(nullptr)
{

}

void GridDataContext::customizeGrid()
{
	m_NameColumn = AddColumn(_YUID("S1"), YtriaTranslate::Do(GridDataContext_customizeGrid_1, _YLOC("Name")).c_str(), _YTEXT(""));
	m_IDColumn = AddColumn(_YUID("S2"), YtriaTranslate::Do(GridDataContext_customizeGrid_2, _YLOC("ID")).c_str(), _YTEXT(""));

	if (m_UseDetailsColumn)
	{
		m_DetailsColumn = AddColumn(_YUID("S3"), YtriaTranslate::Do(GridDataContext_customizeGrid_3, _YLOC("Details")).c_str(), _YTEXT(""));

		SetAutomationName(_YTEXT("GridDataContext2"));
	}
	else
	{
		SetAutomationName(_YTEXT("GridDataContext"));
	}
}

void GridDataContext::OnCustomDoubleClick()
{
	const auto& selectedRows = GetSelectedRows();

	if (!selectedRows.empty())
	{
		auto row = *selectedRows.begin();
		m_SelectedId = row->GetField(m_IDColumn).GetValueStr();

		auto parent = dynamic_cast<DlgDataContext*>(GetParent());
		ASSERT(nullptr != parent);
		parent->OnSelect();
	}
}
