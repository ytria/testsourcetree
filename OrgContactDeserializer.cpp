#include "OrgContactDeserializer.h"

#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "MsGraphFieldNames.h"

void OrgContactDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT(O365_ID), m_Data.m_Id, p_Object, true);

    {
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT(O365_USER_BUSINESSPHONES), p_Object))
            m_Data.m_BusinessPhones = std::move(lsd.GetData());
    }
       
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_CITY), m_Data.m_City, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_COMPANYNAME), m_Data.m_CompanyName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_COUNTRY), m_Data.m_Country, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_DEPARTMENT), m_Data.m_Department, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_DISPLAYNAME), m_Data.m_DisplayName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_GIVENNAME), m_Data.m_GivenName, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_JOBTITLE), m_Data.m_JobTitle, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_MAIL), m_Data.m_Mail, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_MAILNICKNAME), m_Data.m_MailNickname, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_MOBILEPHONE), m_Data.m_MobilePhone, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_OFFICELOCATION), m_Data.m_OfficeLocation, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_ONPREMISESLASTSYNCDATETIME), m_Data.m_OnPremisesLastSyncDateTime, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT(O365_USER_ONPREMISESSYNCENABLED), m_Data.m_OnPremisesSyncEnabled, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_POSTALCODE), m_Data.m_PostalCode, p_Object);

	{
        ListStringDeserializer lsd;
        if (JsonSerializeUtil::DeserializeAny(lsd, _YTEXT(O365_USER_PROXYADDRESSES), p_Object))
            m_Data.m_ProxyAddresses = std::move(lsd.GetData());
    }
    
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_STATE), m_Data.m_State, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_STREETADDRESS), m_Data.m_StreetAddress, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT(O365_USER_SURNAME), m_Data.m_Surname, p_Object);
}

