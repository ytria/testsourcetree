#pragma once

#include "IRequester.h"

class SingleRequestResult;

class UserLicenseUpdateRequester : public IRequester
{
public:
    UserLicenseUpdateRequester(PooledString p_UserId);

    virtual pplx::task<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
    
    PooledString GetUserId() const;
    const SingleRequestResult& GetResult() const;

    void SetDisabledSkus(const std::set<PooledString>& p_DisabledSkus);
    void SetModifiedSkus(const map<PooledString, std::set<PooledString>>& p_ModifiedSkus);

private:
    PooledString m_UserId;
    std::set<PooledString> m_DisabledSkus;
    map<PooledString, std::set<PooledString>> m_ModifiedSkus; // Key: Sku id Value: Disabled Plans
    std::shared_ptr<SingleRequestResult> m_Result;
};

