#pragma once

#include "IRequester.h"

class DumpDeserializer;
class SingleRequestResult;

namespace Cosmos
{
    // Important: the json document must have an "id" field or the request will fail!
    class WriteDocRequester : public IRequester
    {
    public:
        WriteDocRequester(	const wstring&		p_DatabaseName,
							const wstring&		p_CollectionName,
							const web::json::value& p_Document,
							bool				p_UpdateIfExists);
        TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

        const wstring& GetData() const;
        const SingleRequestResult& GetResult() const;

        std::shared_ptr<DumpDeserializer> m_Deserializer;
        std::shared_ptr<SingleRequestResult> m_Result;

    private:
        wstring m_DbName;
        wstring m_CollectionName;
		web::json::value m_Document;
        bool m_UpdateIfExists;
    };
}
