#pragma once

#include "Str.h"

namespace Sp
{

    static PooledString GetSiteUrlFromWebUrl(const web::uri& p_Uri)
    {
        PooledString siteName;
        auto explodedPath = p_Uri.split_path(p_Uri.path());
        if (!explodedPath.empty())
        {
            std::vector<wstring> siteUrlExploded = std::vector<wstring>(explodedPath.begin() + 1, explodedPath.end());
            siteName = Str::implode(siteUrlExploded, _YTEXT("/"));
        }

        return siteName;
    }
}