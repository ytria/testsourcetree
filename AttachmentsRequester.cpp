#include "AttachmentsRequester.h"
#include "O365AdminUtil.h"
#include "PaginatorUtil.h"
#include "MsGraphHttpRequestLogger.h"

AttachmentsRequester::AttachmentsRequester(AttachmentsSource p_Source, PooledString p_UserOrGroupId, PooledString p_EventOrMsgId, const std::shared_ptr<IPageRequestLogger>& p_Logger)
	:m_Source(p_Source),
	m_UserOrGroupID(p_UserOrGroupId),
	m_MsgOrEventId(p_EventOrMsgId),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> AttachmentsRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<ValueListDeserializer<BusinessAttachment, AttachmentDeserializer>>();

	web::uri_builder uri;
	switch (m_Source)
	{
	case AttachmentsSource::UserEvents:
		uri.append_path(_YTEXT("users"));
		uri.append_path(m_UserOrGroupID);
		uri.append_path(_YTEXT("events"));
		uri.append_path(m_MsgOrEventId);
		uri.append_path(_YTEXT("attachments"));
		break;
	case AttachmentsSource::GroupEvents:
		uri.append_path(_YTEXT("groups"));
		uri.append_path(m_UserOrGroupID);
		uri.append_path(_YTEXT("events"));
		uri.append_path(m_MsgOrEventId);
		uri.append_path(_YTEXT("attachments"));
		break;
	case AttachmentsSource::Messages:
		uri.append_path(_YTEXT("users"));
		uri.append_path(m_UserOrGroupID);
		uri.append_path(_YTEXT("events"));
		uri.append_path(m_MsgOrEventId);
		uri.append_path(_YTEXT("attachments"));
		break;
	}

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	auto paginator = Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Deserializer, m_Logger, httpLogger, p_TaskData.GetOriginator());
	return paginator.Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

const vector<BusinessAttachment>& AttachmentsRequester::GetData() const
{
	return m_Deserializer->GetData();
}
