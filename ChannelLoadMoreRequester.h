#pragma once

#include "IRequester.h"

class BusinessChannel;
class IRequestLogger;

class ChannelLoadMoreRequester: public IRequester
{
public:
	enum Flags : int32_t
	{
		REQUEST_MEMBERS = 0x1,
		REQUEST_DRIVE	= 0x2
	};
	ChannelLoadMoreRequester(const boost::YOpt<PooledString>& p_TeamId, const wstring& p_ChannelId, const boost::YOpt<PooledString>& p_MembershipType, int32_t p_Flags, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const BusinessChannel& GetData() const;

private:
	std::shared_ptr<BusinessChannel> m_Channel;
	std::shared_ptr<IRequestLogger> m_Logger;
	bool m_RequestMembers;
	bool m_RequestDrive;
};
