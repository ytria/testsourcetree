#include "RoleDelegationSessions.h"
#include "MSGraphCommonData.h"
#include "MSGraphSession.h"
#include "NetworkUtils.h"
#include "O365AdminErrorHandler.h"
#include "OAuth2Authenticators.h"
#include "RESTUtils.h"
#include "SessionTypes.h"
#include "RoleDelegationUtil.h"

RoleDelegationSessions::RoleDelegationSessions()
    :m_AreSessionsValid(false)
{
}

std::shared_ptr<MSGraphSession> RoleDelegationSessions::GetGraphSessionUser() const
{
    return m_MsGraphSessionUser;
}

std::shared_ptr<MSGraphSession> RoleDelegationSessions::GetGraphSessionUltraAdmin() const
{
    return m_MsGraphSessionUltraAdmin;
}

void RoleDelegationSessions::SetTenant(const wstring& p_Tenant)
{
    m_Tenant = p_Tenant;
}

void RoleDelegationSessions::SetUserSessionCredentials(const wstring& p_Username, const wstring& p_Password)
{
    m_UserSessionUsername = p_Username;
    m_UserSessionPassword = p_Password;
}

void RoleDelegationSessions::SetUltraAdminSessionCredentials(const wstring& p_ClientId, const wstring& p_ClientSecret)
{
    m_UltraAdminSessionClientId		= p_ClientId;
    m_UltraAdminSessionClientSecret = p_ClientSecret;
}

void RoleDelegationSessions::CreateSessions()
{
	const auto userResult = CreateUserSession();
	std::pair<bool, wstring> ultraAdminResult{ false, _YTEXT("") };
	if (userResult.first)
		ultraAdminResult = CreateUltraAdminSession();

	m_SessionError = !ultraAdminResult.second.empty() ? ultraAdminResult.second : userResult.second;

    m_AreSessionsValid = userResult.first && ultraAdminResult.first;
}

void RoleDelegationSessions::ClearSessions()
{
    if (nullptr != m_MsGraphSessionUser && nullptr != m_MsGraphSessionUltraAdmin)
    {
        m_MsGraphSessionUser->ClearToken();
        m_MsGraphSessionUltraAdmin->ClearToken();
    }
    m_MsGraphSessionUser.reset();
    m_MsGraphSessionUltraAdmin.reset();

    m_AreSessionsValid = false;
}

bool RoleDelegationSessions::AreSessionsValid() const
{
    return m_AreSessionsValid;
}

const wstring& RoleDelegationSessions::GetSessionError() const
{
	return m_SessionError;
}

std::pair<bool, wstring> RoleDelegationSessions::CreateUserSession()
{
	std::pair<bool, wstring> returnValue{ false, _YTEXT("") };

    ASSERT(!m_UserSessionUsername.empty());

    m_MsGraphSessionUser = RoleDelegationUtil::CreateAdminSession(m_Tenant, m_UserSessionUsername, m_UserSessionPassword);

    auto result = m_MsGraphSessionUser->Start(nullptr).get();
	if (result.state == RestAuthenticationResult::State::Success)
	{
		LoggerService::Debug(YtriaTranslate::Do(RoleDelegationSessions_CreateUserSession_1, _YLOC("Successfully created Admin Role session for %1"), m_UserSessionUsername.c_str()));
		returnValue.first = true;
		returnValue.second.clear();
	}
	else
	{
		returnValue.first = false;
		returnValue.second = result.error_message;
		LoggerService::Debug(YtriaTranslate::Do(RoleDelegationSessions_CreateUserSession_2, _YLOC("Unable to create Admin Role session for %1"), m_UserSessionUsername.c_str()));
	}

    return returnValue;
}

std::pair<bool, wstring> RoleDelegationSessions::CreateUltraAdminSession()
{
	std::pair<bool, wstring> returnValue{ false, _YTEXT("") };

    ASSERT(!m_UltraAdminSessionClientId.empty());
    ASSERT(!m_Tenant.empty());

    m_MsGraphSessionUltraAdmin = RoleDelegationUtil::CreateUltraAdminSession(m_Tenant, m_UltraAdminSessionClientId, m_UltraAdminSessionClientSecret);

    auto result = m_MsGraphSessionUltraAdmin->Start(nullptr).get();
	if (result.state == RestAuthenticationResult::State::Success)
	{
		returnValue.first = true;
		returnValue.second.clear();
		LoggerService::Debug(YtriaTranslate::Do(RoleDelegationSessions_CreateUltraAdminSession_1, _YLOC("Successfully created Role Ultra Admin session for id %1"), m_UltraAdminSessionClientId.c_str()));
	}
	else
	{
		returnValue.first = false;
		returnValue.second = result.error_message;
		LoggerService::Debug(YtriaTranslate::Do(RoleDelegationSessions_CreateUltraAdminSession_2, _YLOC("Unable to create Role Ultra Admin session for id %1"), m_UltraAdminSessionClientId.c_str()));
	}

    return returnValue;
}
