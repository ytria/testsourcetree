#include "DelegatePermissions.h"

const PooledString Ex::DelegatePermissions::g_None = _YTEXT("None");
const PooledString Ex::DelegatePermissions::g_Editor = _YTEXT("Editor");
const PooledString Ex::DelegatePermissions::g_Reviewer = _YTEXT("Reviewer");
const PooledString Ex::DelegatePermissions::g_Author = _YTEXT("Author");
const PooledString Ex::DelegatePermissions::g_Custom = _YTEXT("Custom");
