#pragma once

#include "BaseO365Grid.h"
#include "OnPremiseUser.h"
#include "GridTemplateUsers.h"

class FrameOnPremiseUsers;

class GridOnPremiseUsers : public ModuleO365Grid<OnPremiseUser>
{
public:
	GridOnPremiseUsers();

	void customizeGrid() override;

	void BuildView(const vector<OnPremiseUser>& p_Users, bool p_FullPurge);

protected:
	OnPremiseUser getBusinessObject(GridBackendRow*) const;
	void UpdateBusinessObjects(const vector<OnPremiseUser>& p_Permissions, bool p_SetModifiedStatus);
	void RemoveBusinessObjects(const vector<OnPremiseUser>& p_Permissions);
	wstring GetName(const GridBackendRow* p_Row) const override;

	RoleDelegationUtil::RBAC_Privilege GetPrivilegeEdit() const override;

private:
	GridBackendRow* FillRow(GridBackendRow* p_Row, const OnPremiseUser& p_Permissions);
	void AddDateOrNeverField(const boost::YOpt<YTimeDate>& p_Date, GridBackendRow& p_Row, GridBackendColumn& p_Col);

	bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

private:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnEditUsers();
	afx_msg void OnUpdateEditUsers(CCmdUI* pCmdUI);

	FrameOnPremiseUsers* m_Frame;

	GridBackendColumn* m_ColLastRequestDateTime;
	
	GridBackendColumn* m_ColAccountExpires;
	GridBackendColumn* m_ColAccountExpirationDate;
	GridBackendColumn* m_ColAccountLockoutTime;
	GridBackendColumn* m_ColAccountNotDelegated;
	GridBackendColumn* m_ColAdminCount;
	GridBackendColumn* m_ColAllowReversiblePasswordEncryption;
	GridBackendColumn* m_ColAuthenticationPolicy;
	GridBackendColumn* m_ColAuthenticationPolicySilo;
	GridBackendColumn* m_ColBadLogonCount;
	GridBackendColumn* m_ColBadPasswordTime;
	GridBackendColumn* m_ColBadPwdCount;
	GridBackendColumn* m_ColCannotChangePassword;
	GridBackendColumn* m_ColCanonicalName;
	GridBackendColumn* m_ColCertificates;
	GridBackendColumn* m_ColCity;
	GridBackendColumn* m_ColCompoundIdentitySupported;
	GridBackendColumn* m_ColCN;
	GridBackendColumn* m_ColCodePage;
	GridBackendColumn* m_ColCompany;
	GridBackendColumn* m_ColCountry;
	GridBackendColumn* m_ColCountryCode;
	GridBackendColumn* m_ColCreated;
	GridBackendColumn* m_ColCreateTimeStamp;
	GridBackendColumn* m_ColDeleted;
	GridBackendColumn* m_ColDepartment;
	GridBackendColumn* m_ColDescription;
	GridBackendColumn* m_ColDisplayName;
	GridBackendColumn* m_ColDistinguishedName;
	GridBackendColumn* m_ColDivision;
	GridBackendColumn* m_ColDoesNotRequirePreAuth;
	GridBackendColumn* m_ColDSCorePropagationData;
	GridBackendColumn* m_ColEmailAddress;
	GridBackendColumn* m_ColEmployeeID;
	GridBackendColumn* m_ColEmployeeNumber;
	GridBackendColumn* m_ColEnabled;
	GridBackendColumn* m_ColFax;
	GridBackendColumn* m_ColGivenName;
	GridBackendColumn* m_ColHomeDirectory;
	GridBackendColumn* m_ColHomedirRequired;
	GridBackendColumn* m_ColHomeDrive;
	GridBackendColumn* m_ColHomePage;
	GridBackendColumn* m_ColHomePhone;
	GridBackendColumn* m_ColInitials;
	GridBackendColumn* m_ColInstanceType;
	GridBackendColumn* m_ColIsCriticalSystemObject;
	GridBackendColumn* m_ColIsDeleted;
	GridBackendColumn* m_ColKerberosEncryptionType;
	GridBackendColumn* m_ColLastBadPasswordAttempt;
	GridBackendColumn* m_ColLastKnownParent;
	GridBackendColumn* m_ColLastLogoff;
	GridBackendColumn* m_ColLastLogon;
	GridBackendColumn* m_ColLastLogonDate;
	GridBackendColumn* m_ColLastLogonTimestamp;
	GridBackendColumn* m_ColLockedOut;
	GridBackendColumn* m_ColLogonCount;
	GridBackendColumn* m_ColLogonHours;
	GridBackendColumn* m_ColLogonWorkstations;
	GridBackendColumn* m_ColManager;
	GridBackendColumn* m_ColMemberOf;
	GridBackendColumn* m_ColMNSLogonAccount;
	GridBackendColumn* m_ColMobilePhone;
	GridBackendColumn* m_ColModified;
	GridBackendColumn* m_ColModifyTimeStamp;
	GridBackendColumn* m_ColMSDSUserAccountControlComputed;
	GridBackendColumn* m_ColName;
	GridBackendColumn* m_ColNTSecurityDescriptor;
	GridBackendColumn* m_ColObjectCategory;
	GridBackendColumn* m_ColObjectClass;
	GridBackendColumn* m_ColObjectGUID;
	GridBackendColumn* m_ColObjectSid;
	GridBackendColumn* m_ColOffice;
	GridBackendColumn* m_ColOfficePhone;
	GridBackendColumn* m_ColOrganization;
	GridBackendColumn* m_ColOtherName;
	GridBackendColumn* m_ColPasswordExpired;
	GridBackendColumn* m_ColPasswordLastSet;
	GridBackendColumn* m_ColPasswordNeverExpires;
	GridBackendColumn* m_ColPasswordNotRequired;
	GridBackendColumn* m_ColPOBox;
	GridBackendColumn* m_ColPostalCode;
	GridBackendColumn* m_ColPrimaryGroup;
	GridBackendColumn* m_ColPrimaryGroupId;
	GridBackendColumn* m_ColPrincipalsAllowedToDelegateToAccount;
	GridBackendColumn* m_ColProfilePath;
	GridBackendColumn* m_ColProtectedFromAccidentalDeletion;
	GridBackendColumn* m_ColPwdLastSet;
	GridBackendColumn* m_ColSamAccountName;
	GridBackendColumn* m_ColSAMAccountType;
	GridBackendColumn* m_ColScriptPath;
	GridBackendColumn* m_ColServicePrincipalNames;
	GridBackendColumn* m_ColSDRightsEffective;
	GridBackendColumn* m_ColSID;
	GridBackendColumn* m_ColSIDHistory;
	GridBackendColumn* m_ColSmartcardLogonRequired;
	GridBackendColumn* m_ColState;
	GridBackendColumn* m_ColStreetAddress;
	GridBackendColumn* m_ColSurname;
	GridBackendColumn* m_ColTitle;
	GridBackendColumn* m_ColTrustedForDelegation;
	GridBackendColumn* m_ColTrustedToAuthForDelegation;
	GridBackendColumn* m_ColUseDESKeyOnly;
	GridBackendColumn* m_ColUserAccountControl;
	GridBackendColumn* m_ColUserCertificate;
	GridBackendColumn* m_ColUserPrincipalName;
	GridBackendColumn* m_ColUSNChanged;
	GridBackendColumn* m_ColUSNCreated;
	GridBackendColumn* m_ColWhenChanged;
	GridBackendColumn* m_ColWhenCreated;
};

