#pragma once

#include "ModifiedProperty.h"

class TargetResource
{
public:
	boost::YOpt<PooledString>				m_Id;
	boost::YOpt<PooledString>				m_DisplayName;
	boost::YOpt<PooledString>				m_Type;
	boost::YOpt<PooledString>				m_UserPrincipalName;
	boost::YOpt<PooledString>				m_GroupType;
	boost::YOpt<vector<ModifiedProperty>>	m_ModifiedProperties;
};

