#pragma once

namespace Ex
{
class DelegatePermissions
{
public:
    static const PooledString g_None;
    static const PooledString g_Editor;
    static const PooledString g_Reviewer;
    static const PooledString g_Author;
    static const PooledString g_Custom;

    boost::YOpt<PooledString> m_CalendarFolder;
    boost::YOpt<PooledString> m_TasksFolder;
    boost::YOpt<PooledString> m_InboxFolder;
    boost::YOpt<PooledString> m_ContactsFolder;
    boost::YOpt<PooledString> m_NotesFolder;
    boost::YOpt<PooledString> m_JournalFolder;
};
}
