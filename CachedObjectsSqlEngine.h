#pragma once

#include "CommandInfo.h" // For O365IdsContainer
#include "O365SQLiteEngine.h"
#include "SessionIdentifier.h"

class CachedObjectsSqlEngine : public O365SQLiteEngine
{
public:
	// static const int g_Version = 0;
	//static const int g_Version = 1; // Added Provisioned and AssignedPlans to Users
	static const int g_Version = 2; // Bug #201005.YT.00CFAE: <2.0>[-CRITICAL-] Make the Cache "modular"

public:
	static const wstring g_TableMetadata;
	static const wstring g_TableUsers;
	static const wstring g_TableGroups;

    CachedObjectsSqlEngine(const SessionIdentifier& p_SessionId);
	virtual ~CachedObjectsSqlEngine();

	void Init(const std::vector<wstring>& p_UserDataColumns, const std::vector<wstring>& p_GroupDataColumns); // Mandatory to call before using

	bool IsForSession(const SessionIdentifier& p_SessionId) const;

	struct SQLCachedObject
	{
		SQLCachedObject() = default;
		SQLCachedObject(const PooledString& p_Id, const std::map<wstring, wstring>& p_ObjectBody)
			: m_Id(p_Id)
			, m_ObjectBody(p_ObjectBody)
		{
		}

		PooledString m_Id;
		std::map<wstring, wstring> m_ObjectBody;
	};

	void SaveUser(const PooledString& p_Id, const std::map<wstring, wstring>& p_ObjectBody);
	void SaveUser(const SQLCachedObject& p_Obj);
	void SaveGroup(const PooledString& p_Id, const std::map<wstring, wstring>& p_ObjectBody);
	void SaveGroup(const SQLCachedObject& p_Obj);

	void DeleteUser(const PooledString& p_Id);
	void DeleteGroup(const PooledString& p_Id);

	int CountUsersMissingRequirements(const std::vector<wstring>& p_RequiredDataColumns) const;
	int CountGroupsMissingRequirements(const std::vector<wstring>& p_RequiredDataColumns) const;

	int GetRowCount(const wstring& p_TableName) const;

	struct Metadata
	{
		boost::YOpt<int> m_FullListSize;
		boost::YOpt<YTimeDate> m_FullRefreshDate;
		boost::YOpt<int> m_Version;
		boost::YOpt<wstring> m_LastDelta;
	};

	void SaveUsersMetadata(const Metadata& p_Metadata);
	void SaveGroupsMetadata(const Metadata& p_Metadata);

	Metadata LoadUsersMetadata() const;
	Metadata LoadGroupsMetadata() const;

	vector<SQLCachedObject> LoadUsers(const O365IdsContainer& p_IDs, const std::vector<wstring>& p_RequiredDataColumns = {});
    vector<SQLCachedObject> LoadGroups(const O365IdsContainer& p_IDs, const std::vector<wstring>& p_RequiredDataColumns = {});

    vector<SQLiteUtil::Table> GetTables() const override;

	wstring Clear(); // Returns error
	bool ClearUsers();
	bool ClearGroups();

	bool UserTableHasLicensePlans() const;

	uint32_t GetVersion() const override
	{
		return g_Version;
	}

private:
	wstring getUpsertQuery(const wstring& p_TableName, const wstring& p_DataCol = g_ColValue);
	wstring getDeleteQuery(const wstring& p_TableName);

	sqlite3_stmt* getUpsertStatement(const wstring& p_TableName, const wstring& p_DataCol);
	sqlite3_stmt* getDeleteStatement(const wstring& p_TableName);

	vector<SQLCachedObject> load(const SQLiteUtil::Table& p_Table, const O365IdsContainer& p_IDs, const std::vector<wstring>& p_RequiredDataColumns);
    void save(sqlite3_stmt* p_Statement, const PooledString& p_Id, const wstring& p_ObjectBody);
	void deleteEntry(sqlite3_stmt* p_Statement, const PooledString& p_Id);
    void addColsToTable(SQLiteUtil::Table& p_Table, const std::vector<wstring>& p_DataColumns);
	Metadata loadMetadata(const SQLiteUtil::Table& p_Table) const;
	void saveMetadata(const SQLiteUtil::Table& p_Table, const Metadata& p_Metadata);

	int countObjectsMissingRequirements(const wstring& p_TableName, const std::vector<wstring>& p_RequiredDataColumns) const;
	bool clearTableAndMetadata(const SQLiteUtil::Table& p_Table);
	bool clearTable(const SQLiteUtil::Table& p_Table);
	bool clearMetadataFor(const SQLiteUtil::Table& p_Table);

    SessionIdentifier m_Identifier;

	sqlite3_stmt* m_SqlStatementGetMetadata;
	sqlite3_stmt* m_SqlStatementSetMetadata;
	sqlite3_stmt* m_SqlStatementDeleteMetadata;

	std::map<wstring, std::map<wstring, sqlite3_stmt*>> m_UpsertStatements;
	std::map<wstring, sqlite3_stmt*> m_DeleteStatements;

	SQLiteUtil::Table m_TableMetadata;
    SQLiteUtil::Table m_TableUsers;
    SQLiteUtil::Table m_TableGroups;

    static const wstring g_ColId;
    static const wstring g_ColValue;

	static const wstring g_ColMetadataName;
	static const wstring g_ColMetadataFullListSize;
	static const wstring g_ColMetadataDate;
	static const wstring g_ColVersion;
	static const wstring g_ColLastDelta;
};

static int countCallback(void* count, int argc, char** argv, char** colName);
