#pragma once

#include "DlgFormsHTML.h"

class DlgSimpleEditHTML : public DlgFormsHTML
{
public:
	DlgSimpleEditHTML(CWnd* p_Parent);
	virtual ~DlgSimpleEditHTML();

	const wstring& GetText() const;

protected:
	virtual void generateJSONScriptData();

	// Returns true if the dialog can be closed.
	virtual bool processPostedData(const IPostedDataTarget::PostedData& data);

	virtual wstring getDialogTitle() const override;

private:
	wstring m_Text;
	static const wstring g_PropName;
};

