#pragma once

#include "IRequester.h"

#include "ChannelDeserializer.h"
#include "IRequestLogger.h"

class ChannelRequester : public IRequester
{
public:
	ChannelRequester(const PooledString& p_TeamId, const PooledString& p_ChannelId, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const BusinessChannel& GetData() const;

private:
	PooledString m_TeamId;
	PooledString m_ChannelId;

	shared_ptr<ChannelDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
};