#pragma once

#include "IRequester.h"
#include "BusinessSite.h"
#include "IRequestLogger.h"
#include "SiteDeserializer.h"

class SiteRequester : public IRequester
{
public:
	SiteRequester(const wstring& p_SiteId, const std::shared_ptr<IRequestLogger>& p_Logger);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const BusinessSite& GetData() const;
	BusinessSite& GetData();

private:
	wstring m_SiteId;
	std::shared_ptr<SiteDeserializer> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
};

