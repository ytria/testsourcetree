#include "MsGraphPageRequesterRetryer.h"
#include "RESTException.h"
#include "YCallbackMessage.h"
#include "YCodeJockMessageBox.h"

MsGraphPageRequesterRetryer::askifretry_t MsGraphPageRequesterRetryer::DefaultAskIfRetry()
{
	return [](const std::string& p_ErrorMsg, HWND p_Hwnd) {
		bool retry = false;

		YCallbackMessage::DoSend([&retry, &p_ErrorMsg, p_Hwnd]() {
			YCodeJockMessageBox msgBox(CWnd::FromHandle(p_Hwnd),
				DlgMessageBox::eIcon_Error,
				_T("An error occured during request"),
				wstring(_T("The request failed. Would you like to try again?")),
				Str::convertFromUTF8(p_ErrorMsg),
				{ { IDOK, _T("Retry") }, { IDCANCEL, _T("Cancel") } }
			);
			retry = msgBox.DoModal() == IDOK;
		});

		return retry;
	};
}

MsGraphPageRequesterRetryer::MsGraphPageRequesterRetryer(const std::shared_ptr<IMsGraphPageRequester>& p_Requester, const askifretry_t& p_AskIfRetry, HWND p_Hwnd)
	:m_Requester(p_Requester),
	m_AskIfRetry(p_AskIfRetry),
	m_Hwnd(p_Hwnd)
{
	ASSERT(nullptr != p_Requester);
	if (nullptr == p_Requester)
		throw std::invalid_argument("MsGraphPageRequesterRetryer: requester cannot be nullptr");

	Copy(*m_Requester);
}

web::json::value MsGraphPageRequesterRetryer::Request(const web::uri& p_Uri, const shared_ptr<MSGraphSession>& p_Session, YtriaTaskData p_TaskData)
{
	bool retry = false;
	web::json::value jsonReceived;

	do
	{
		try
		{
			jsonReceived = m_Requester->Request(p_Uri, p_Session, p_TaskData);
			retry = false;
			Copy(*m_Requester);
		}
		catch (const RestException& e)
		{
			auto statusCode = e.GetRequestResult().Response.status_code();
			if (!(statusCode >= 400 && statusCode < 500) && !p_TaskData.IsCanceled())
			{
				retry = m_AskIfRetry(Str::convertToUTF8(_YFORMAT(L"HTTP %s - ", std::to_wstring(statusCode).c_str())) + e.what(), m_Hwnd);
				if (!retry)
					throw;
			}
			else
				throw;
		}
		catch (const web::http::oauth2::experimental::oauth2_exception&)
		{
			// Authentication issues cannot be retried. They must be handled by the caller (by showing the authentication window).
			throw;
		}
		catch (const std::exception& e)
		{
			if (!p_TaskData.IsCanceled())
			{
				retry = m_AskIfRetry(e.what(), m_Hwnd);
				if (!retry)
					throw;
			}
			else
				throw;
		}
	} while (retry);

	return jsonReceived;
}

void MsGraphPageRequesterRetryer::SetUseBetaEndpoint(bool p_UseBetaEndpoint)
{
	IMsGraphPageRequester::SetUseBetaEndpoint(p_UseBetaEndpoint);
	if (m_Requester)
		m_Requester->SetUseBetaEndpoint(p_UseBetaEndpoint);
}
