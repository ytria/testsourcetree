#pragma once

#include "BusinessObject.h"
#include "Message.h"

class BusinessMessage : public BusinessObject
{
public:
    BusinessMessage() = default;
    BusinessMessage(const Message& p_Message);

	vector<PooledString> m_Categories;

	/*vector<PooledString> m_BccRecipientsAddress;
	vector<PooledString> m_BccRecipientsName;
	vector<PooledString> m_CcRecipientsAddress;
	vector<PooledString> m_CcRecipientsName;
	vector<PooledString> m_ReplyToAddress;
	vector<PooledString> m_ReplyToName;
	vector<PooledString> m_ToRecipientsAddress;
	vector<PooledString> m_ToRecipientsName;*/

    vector<Recipient> m_BccRecipients;
    vector<Recipient> m_CcRecipients;
    vector<Recipient> m_ReplyTo;
    vector<Recipient> m_ToRecipients;

	boost::YOpt<PooledString> m_SenderUnused;
	boost::YOpt<PooledString> m_SenderAddress;
	boost::YOpt<PooledString> m_SenderName;

	boost::YOpt<PooledString> m_FromUnused;
	boost::YOpt<PooledString> m_FromAddress;
	boost::YOpt<PooledString> m_FromName;

	boost::YOpt<PooledString> m_Subject;

	boost::YOpt<PooledString> m_UniqueUnused;
	boost::YOpt<PooledString> m_UniqueBodyContent;
	boost::YOpt<PooledString> m_UniqueBodyContentType;

	boost::YOpt<PooledString> m_BodyContent;
	boost::YOpt<PooledString> m_BodyContentType;
	boost::YOpt<PooledString> m_BodyPreview;
	boost::YOpt<PooledString> m_ChangeKey;
	boost::YOpt<PooledString> m_ConversationId;
	boost::YOpt<YTimeDate>    m_CreatedDateTime;
	boost::YOpt<PooledString> m_Importance;
	boost::YOpt<PooledString> m_InferenceClassification;
	boost::YOpt<PooledString> m_InternetMessageId;
	boost::YOpt<YTimeDate>    m_LastModifiedDateTime;
	boost::YOpt<PooledString> m_ParentFolderId;
	boost::YOpt<YTimeDate>    m_ReceivedDateTime;
	boost::YOpt<YTimeDate>    m_SentDateTime;
	boost::YOpt<PooledString> m_WebLink;

	boost::YOpt<bool> m_IsDeliveryReceiptRequested;
	boost::YOpt<bool> m_HasAttachments;
	boost::YOpt<bool> m_IsDraft;
	boost::YOpt<bool> m_IsRead;
	boost::YOpt<bool> m_IsReadReceiptRequested;

	vector<InternetMessageHeader> m_InternetMessageHeaders;

    // Non property
    boost::YOpt<PooledString> m_ParentFolderName;
	boost::YOpt<PooledString> m_UserId; // Owner

	bool ConvertProperty(Message& p_Message, const rttr::property& p_BizProp, vector<rttr::property>& p_Properties) const;


    static Message ToMessage(const BusinessMessage& p_BusinessMessage);

protected:
	virtual TaskWrapper<HttpResultWithError> SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const override;

private:
    RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
};

wstring GetMessageFolderContext(const BusinessMessage& p_Msg);
