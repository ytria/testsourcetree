#pragma once

#include "DlgModuleOptions.h"

class DlgEventsModuleOptions : public DlgModuleOptions
{
public:
	using DlgModuleOptions::DlgModuleOptions;

protected:
	void generateJSONScriptData() override;
	wstring getDialogTitle() const override;
	std::map<wstring, DlgModuleOptions::FilterFieldProperties> getFilterFields() const override;
};

