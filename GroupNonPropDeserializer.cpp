#include "GroupNonPropDeserializer.h"

#include "CachedUserDeserializer.h"
#include "DriveDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"
#include "SapioErrorDeserializer.h"
#include "SqlCacheConfig.h"
#include "TeamDeserializer.h"
#include "YtriaFieldsConstants.h"

void GroupNonPropDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	// Shouldn't exist anymore.
	ASSERT(p_Object.end() == p_Object.find(_YUID(YTRIA_LASTREQUESTEDDATETIME)));
    //JsonSerializeUtil::DeserializeTimeDate(_YUID(YTRIA_LASTREQUESTEDDATETIME), m_Data.m_LastRequestTime, p_Object);

	// date per block
#define DATE_FIELD_FOR(_blockId_) JsonSerializeUtil::DeserializeTimeDate(SqlCacheConfig<GroupCache>::DataBlockDateFieldById((_blockId_)), m_Data.m_DataBlockDates[_blockId_], p_Object);

	DATE_FIELD_FOR(GBI::MIN)
	DATE_FIELD_FOR(GBI::LIST)
	DATE_FIELD_FOR(GBI::SYNCV1)
	DATE_FIELD_FOR(GBI::SYNCBETA)
	DATE_FIELD_FOR(GBI::CREATEDONBEHALFOF)
	DATE_FIELD_FOR(GBI::TEAM)
	DATE_FIELD_FOR(GBI::GROUPSETTINGS)
	DATE_FIELD_FOR(GBI::LIFECYCLEPOLICIES)
	DATE_FIELD_FOR(GBI::OWNERS)
	DATE_FIELD_FOR(GBI::MEMBERSCOUNT)
	DATE_FIELD_FOR(GBI::DRIVE)	
	DATE_FIELD_FOR(GBI::ISYAMMER)

#undef DATE_FIELD_FOR

	// Shouldn't exist anymore.
	ASSERT(p_Object.end() == p_Object.find(_YUID(YTRIA_FLAGS)));
	//JsonSerializeUtil::DeserializeUint32(_YUID(YTRIA_FLAGS), m_Data.m_Flags, p_Object);

    {
        TeamDeserializer d;
        if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_GROUP_TEAM), p_Object))
            m_Data.m_Team = d.GetData();
    }

	JsonSerializeUtil::DeserializeString(_YUID(YTRIA_GROUP_CREATEDONBEHALFOF), m_Data.m_UserCreatedOnBehalfOf, p_Object);

    JsonSerializeUtil::DeserializeString(_YUID(YTRIA_GROUP_LCPSTATUS), m_Data.m_LCPStatus, p_Object);
    JsonSerializeUtil::DeserializeUint32(_YUID(YTRIA_GROUP_MEMBERSCOUNT), m_Data.m_MembersCount, p_Object);

	JsonSerializeUtil::DeserializeBool(_YUID(YTRIA_GROUP_GROUPUNIFIEDGUESTSETTINGACTIVATED), m_Data.m_SettingAllowToAddGuestsIsActivated, p_Object);
	JsonSerializeUtil::DeserializeBool(_YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTS), m_Data.m_SettingAllowToAddGuests, p_Object);
	JsonSerializeUtil::DeserializeString(_YUID(YTRIA_GROUP_SETTINGALLOWTOADDGUESTSID), m_Data.m_SettingAllowToAddGuestsId, p_Object);

	{
		ListStringDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_GROUP_OWNERS), p_Object))
			m_Data.m_OwnersUsers = std::move(d.GetData());
	}

	{
		DriveDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_DRIVE), p_Object))
			m_Data.m_Drive = std::move(d.GetData());
	}

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_DRIVE_ERROR), p_Object))
			m_Data.m_DriveError = std::move(d.GetData());
	}

	JsonSerializeUtil::DeserializeBool(_YUID(YTRIA_GROUP_ISYAMMER), m_Data.m_IsYammer, p_Object);

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_GROUP_TEAM_ERROR), p_Object))
			m_Data.SetTeamError(d.GetData());
	}

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_GROUP_GROUPSETTINGS_ERROR), p_Object))
			m_Data.SetGroupSettingsError(d.GetData());
	}

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_GROUP_USERCREATEDONBEHALFOF_ERROR), p_Object))
			m_Data.SetUserCreatedOnBehalfOfError(d.GetData());
	}

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_GROUP_OWNERS_ERROR), p_Object))
			m_Data.SetOwnersError(d.GetData());
	}

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_GROUP_LCPSTATUS_ERROR), p_Object))
			m_Data.SetLCPStatusError(d.GetData());
	}

	{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_GROUP_ISYAMMER_ERROR), p_Object))
			m_Data.SetIsYammerError(d.GetData());
	}

	// Shouldn't exist anymore
	ASSERT(p_Object.end() == p_Object.find(_YUID(YTRIA_ERROR)));
	/*{
		SapioErrorDeserializer d;
		d.SetDate(GetDate());
		if (JsonSerializeUtil::DeserializeAny(d, _YUID(YTRIA_ERROR), p_Object))
			m_Data.m_ErrorMessage = std::move(d.GetData());
	}*/
}
