#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "PublicationFacet.h"

class PublicationFacetDeserializer : public JsonObjectDeserializer, public Encapsulate<PublicationFacet>
{
protected:
	virtual void DeserializeObject(const web::json::object& p_Object) override;
};
