#include "DlgUserUsageLocationHTML.h"

DlgUserUsageLocationHTML::DlgUserUsageLocationHTML(vector<BusinessUser>& p_Users, CWnd* p_Parent, const wstring p_AutomationActionName)
	: BaseDlgBusinessUserEditHTML(p_Users, DlgFormsHTML::Action::EDIT, p_Parent, p_AutomationActionName)
{
}

DlgUserUsageLocationHTML::~DlgUserUsageLocationHTML()
{
}

bool DlgUserUsageLocationHTML::processPostedDataSpecific(const IPostedDataTarget::PostedData::value_type& p_Value)
{
	ASSERT(false);
	return false;
}

wstring DlgUserUsageLocationHTML::getDialogTitle() const
{
	ASSERT(!m_BusinessUsers.empty());
	wstring title = YtriaTranslate::Do(DlgUserUsageLocationHTML_getDialogTitle_1, _YLOC("Edit Usage Location"));
	
	if (m_BusinessUsers.size() > 1)
		title = YtriaTranslate::Do(DlgUserUsageLocationHTML_getDialogTitle_2, _YLOC("Edit Usage Location (%1 users selected)"), Str::getStringFromNumber(m_BusinessUsers.size()).c_str());
	else if (m_BusinessUsers.size() == 1 && m_BusinessUsers.back().GetDisplayName().is_initialized())
		title = YtriaTranslate::Do(DlgUserUsageLocationHTML_getDialogTitle_3, _YLOC("Edit Usage Location for %1"), m_BusinessUsers.back().GetDisplayName().get().c_str());

	return title;
}

void DlgUserUsageLocationHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgUserUsageLocationHTML"));

	ASSERT(isEditDialog());
	if (isEditDialog())
	{
		// A two letter country code (ISO standard 3166), not nullable
		addComboEditor(&BusinessUser::GetUsageLocation, &BusinessUser::SetUsageLocation, _YTEXT("usageLocation"), YtriaTranslate::Do(DlgBusinessUserEditHTML_generateJSONScriptData_13, _YLOC("Location for License Usage")).c_str(), m_BusinessUsers.size() == 1 ? EditorFlags::DIRECT_EDIT : 0, getLocationList(), {});
		getGenerator().addApplyButton(LocalizedStrings::g_ApplyButtonText);
		getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
	}
}

void DlgUserUsageLocationHTML::postProcessPostedData()
{
	// Nothing to do.
}
