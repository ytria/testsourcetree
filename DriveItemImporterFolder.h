#pragma once

#include "DriveItemImporter.h"

class DriveItemImporterFolder : public DriveItemImporter
{
public:
	enum class Mode
	{
		SYNC,
		IMPORT_CONTENT_ONLY,
		IMPORT_FOLDER_AND_CONTENT,
		SUB_FOLDER_SYNC, // For internal purpose only
	};
	DriveItemImporterFolder(GridDriveItems& p_Grid, Mode p_Mode);

	virtual void Run(const wstring& p_FolderPath, const std::set<GridBackendRow*>& p_ParentRows, std::shared_ptr<DlgLoading>& p_Dlg = std::shared_ptr<DlgLoading>()) override;

private:
	wstring generateUniqueFolderName(const wstring& desiredFolderName, GridBackendRow* p_ParentRow);

private:
	Mode m_Mode;

	friend class DriveItemImporterFile;
};