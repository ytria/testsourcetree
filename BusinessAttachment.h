#pragma once

#include "Attachment.h"

#include "BusinessObject.h"

class BusinessAttachment : public BusinessObject
{
public:
    BusinessAttachment();
    BusinessAttachment(const Attachment& p_Attachment);
    
    const boost::YOpt<PooledString>& GetContentType() const;
    void SetContentType(const boost::YOpt<PooledString>& p_Val);

	/*const boost::YOpt<PooledString>& GetContentId() const;
	void SetContentId(const boost::YOpt<PooledString>& p_Val);*/

    const boost::YOpt<bool>& GetIsInline() const;
    void SetIsInline(const boost::YOpt<bool>& p_Val);
    
    const boost::YOpt<YTimeDate>& GetLastModifiedDateTime() const;
    void SetLastModifiedDateTime(const boost::YOpt<YTimeDate>& p_Val);

    const boost::YOpt<PooledString>& GetName() const;
    void SetName(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<int32_t>& GetSize() const;
    void SetSize(const boost::YOpt<int32_t>& p_Val);

    const boost::YOpt<PooledString>& GetDataType() const;
    void SetDataType(const boost::YOpt<PooledString>& p_Val);

protected:
    boost::YOpt<PooledString> m_ContentType;
	//boost::YOpt<PooledString> m_ContentId; // For FileAttachment only
    boost::YOpt<bool> m_IsInline;
    boost::YOpt<YTimeDate> m_LastModifiedDateTime;
    boost::YOpt<PooledString> m_Name;
    boost::YOpt<int32_t> m_Size;

    boost::YOpt<PooledString> m_DataType;
};

