#include "GridClassMembers.h"

#include "AutomationWizardClassMembers.h"
#include "BasicGridSetup.h"
#include "DlgAddItemsToGroups.h"
#include "EducationUserDeserializer.h"
#include "FrameClassMembers.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"
#include "MultiObjectsRequestLogger.h"
#include "Resource.h"

BEGIN_MESSAGE_MAP(GridClassMembers, ModuleO365Grid<EducationClass>)
	ON_COMMAND(ID_CLASSMEMBERSGRID_ADDMEMBER, OnAddMember)
	ON_UPDATE_COMMAND_UI(ID_CLASSMEMBERSGRID_ADDMEMBER, OnUpdateAddMember)
	ON_COMMAND(ID_CLASSMEMBERSGRID_REMOVEMEMBER, OnRemoveMember)
	ON_UPDATE_COMMAND_UI(ID_CLASSMEMBERSGRID_REMOVEMEMBER, OnUpdateRemoveMember)
END_MESSAGE_MAP()

GridClassMembers::GridClassMembers()
	: m_ColSchoolId(nullptr)
	, m_ColClassId(nullptr)
	, m_ColDisplayName(nullptr)
	, m_ColExternalSource(nullptr)
	, m_ColMiddleName(nullptr)
	, m_ColPrimaryRole(nullptr)
	, m_ColCreatedByAppDisplayName(nullptr)
	, m_ColCreatedByAppId(nullptr)
	, m_ColCreatedByAppEmail(nullptr)
	, m_ColCreatedByAppIdentityProvider(nullptr)
	, m_ColCreatedByDeviceDisplayName(nullptr)
	, m_ColCreatedByDeviceId(nullptr)
	, m_ColCreatedByDeviceEmail(nullptr)
	, m_ColCreatedByDeviceIdentityProvider(nullptr)
	, m_ColCreatedByUserDisplayName(nullptr)
	, m_ColCreatedByUserId(nullptr)
	, m_ColCreatedByUserEmail(nullptr)
	, m_ColCreatedByUserIdentityProvider(nullptr)
	, m_ColMailingAddressCity(nullptr)
	, m_ColMailingAddressCountryOrRegion(nullptr)
	, m_ColMailingAddressPostalCode(nullptr)
	, m_ColMailingAddressState(nullptr)
	, m_ColMailingAddressStreet(nullptr)
	, m_ColRelatedContactId(nullptr)
	, m_ColRelatedContactDisplayName(nullptr)
	, m_ColRelatedContactEmailAddress(nullptr)
	, m_ColRelatedContactMobilePhone(nullptr)
	, m_ColRelatedContactRelationship(nullptr)
	, m_ColRelatedContactAccessContent(nullptr)
	, m_ColStudentBirthDate(nullptr)
	, m_ColStudentExternalId(nullptr)
	, m_ColStudentGender(nullptr)
	, m_ColStudentGrade(nullptr)
	, m_ColStudentGraduationYear(nullptr)
	, m_ColStudentNumber(nullptr)
	, m_ColTeacherExternalId(nullptr)
	, m_ColTeacherNumber(nullptr)
{
	initWizard<AutomationWizardClassMembers>(_YTEXT("Automation\\ClassMembers"));
}

GridClassMembers::~GridClassMembers()
{
	GetBackend().Clear();
}

void GridClassMembers::BuildView(const vector<EducationSchool>& p_Schools, const vector<O365UpdateOperation>& p_Updates, bool p_FullPurge)
{
	GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
		| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
		| GridUpdaterOptions::REFRESH_MODIFICATION_STATES
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		| GridUpdaterOptions::REFRESH_PROCESS_DATA
	);
	GridUpdater updater(*this, options, p_Updates);

	for (const auto& school : p_Schools)
	{
		auto schoolRowPk = CreatePk(school.GetID(), _YTEXT(""), _YTEXT(""));
		GridBackendRow* schoolRow = m_RowIndex.GetRow(schoolRowPk, true, true);
		updater.GetOptions().AddRowWithRefreshedValues(schoolRow);
		updater.AddUpdatedRowPk(schoolRowPk);

		SetRowObjectType(schoolRow, school);
		schoolRow->AddField(school.GetID(), m_ColSchoolId);
		schoolRow->AddField(school.GetDisplayName(), m_ColDisplayName);

		for (const auto& currentClass : school.m_Classes)
		{
			auto classRowPk = CreatePk(school.GetID(), currentClass.GetID(), _YTEXT(""));
			GridBackendRow* classRow = m_RowIndex.GetRow(classRowPk, true, true);
			updater.GetOptions().AddRowWithRefreshedValues(classRow);
			updater.GetOptions().AddPartialPurgeParentRow(classRow);
			updater.AddUpdatedRowPk(classRowPk);

			SetRowObjectType(classRow, currentClass);
			classRow->AddField(school.GetID(), m_ColSchoolId);
			classRow->AddField(currentClass.GetID(), m_ColClassId);
			classRow->AddField(currentClass.GetDisplayName(), m_ColDisplayName);
			classRow->SetParentRow(schoolRow);

			for (const auto& educationUser : currentClass.m_Users)
			{
				auto userRowPk = CreatePk(school.GetID(), currentClass.GetID(), educationUser.GetID());
				GridBackendRow* userRow = m_RowIndex.GetRow(userRowPk, true, true);
				updater.GetOptions().AddRowWithRefreshedValues(userRow);
				updater.AddUpdatedRowPk(userRowPk);

				userRow->AddField(school.GetID(), m_ColSchoolId);
				userRow->AddField(currentClass.GetID(), m_ColClassId);
				SetRowObjectType(userRow, educationUser);

				FillEducationUserFields(userRow, educationUser);

				userRow->SetParentRow(classRow);
			}
		}
	}
}

ClassMembersChanges GridClassMembers::GetChanges()
{
	ClassMembersChanges changes;

	// Sorry for this textbook example of things not-to-do (sequence of dynamic_cast) :'(
	// To fix this, we would need to store concrete implementations of all modifications in the grid (in addition to GridModifications)
	// without relying exclusively on pointers to an abstract interface (RowModification),
	for (const auto& mod : GetModifications().GetRowModifications())
	{
		if (mod->GetState() == Modification::State::AppliedLocally)
		{
			CreatedObjectModification* addMemberMod = dynamic_cast<CreatedObjectModification*>(&(*mod));
			if (nullptr != addMemberMod)
				AddMemberMod(*addMemberMod, changes);
		
			DeletedObjectModification* remMemberMod = dynamic_cast<DeletedObjectModification*>(&(*mod));
			if (nullptr != remMemberMod)
				RemoveMemberMod(*remMemberMod, changes);
		}
	}

	return changes;
}

ClassMembersChanges GridClassMembers::GetChanges(const vector<GridBackendRow*>& p_Rows)
{
	ClassMembersChanges changes;
	for (auto row : p_Rows)
	{
		vector<GridBackendField> rowPk;
		GetRowPK(row, rowPk);
		
		CreatedObjectModification* addMemberMod = GetModifications().GetRowModification<CreatedObjectModification>(rowPk);
		if (nullptr != addMemberMod)
			AddMemberMod(*addMemberMod, changes);
		else
		{
			DeletedObjectModification* removeMemberMod = GetModifications().GetRowModification<DeletedObjectModification>(rowPk);
			if (nullptr != removeMemberMod)
				RemoveMemberMod(*removeMemberMod, changes);
		}
	}
	return changes;
}

void GridClassMembers::AddMemberMod(const CreatedObjectModification& addMemberMod, ClassMembersChanges& changes)
{
	auto rowPk = addMemberMod.GetRowKey();
	GridBackendRow* addMemberRow = m_RowIndex.GetRow(rowPk, false, false);
	ASSERT(nullptr != addMemberRow);
	if (nullptr != addMemberRow)
	{
		MemberAddition memberAdd;
		memberAdd.m_RowPk = rowPk;
		memberAdd.m_ClassId = addMemberRow->GetField(m_ColClassId).GetValueStr();
		memberAdd.m_UserId = addMemberRow->GetField(m_ColId).GetValueStr();
		changes.m_MemberAdditions.push_back(memberAdd);
	}
}

void GridClassMembers::RemoveMemberMod(const DeletedObjectModification& removeMemberMod, ClassMembersChanges& changes)
{
	auto rowPk = removeMemberMod.GetRowKey();
	GridBackendRow* removeMemberRow = m_RowIndex.GetRow(rowPk, false, false);
	if (nullptr != removeMemberRow)
	{
		MemberRemoval memberRemoval;
		memberRemoval.m_RowPk = rowPk;
		memberRemoval.m_ClassId = removeMemberRow->GetField(m_ColClassId).GetValueStr();
		memberRemoval.m_UserId = removeMemberRow->GetField(m_ColId).GetValueStr();
		changes.m_MemberRemovals.push_back(memberRemoval);
	}
}

void GridClassMembers::FillEducationUserFields(GridBackendRow* p_UserRow, const EducationUser& p_EducationUser)
{
	// Simple values
	p_UserRow->AddField(p_EducationUser.GetDisplayName(), m_ColDisplayName);
	p_UserRow->AddField(p_EducationUser.GetMiddleName(), m_ColMiddleName);
	p_UserRow->AddField(p_EducationUser.GetPrimaryRole(), m_ColPrimaryRole);

	// Created by
	if (p_EducationUser.GetCreatedBy())
	{
		if (p_EducationUser.GetCreatedBy()->Application)
		{
			const auto& app = *p_EducationUser.GetCreatedBy()->Application;
			p_UserRow->AddField(app.DisplayName, m_ColDisplayName);
			p_UserRow->AddField(app.Id, m_ColCreatedByAppId);
			p_UserRow->AddField(app.Email, m_ColCreatedByAppDisplayName);
			p_UserRow->AddField(app.IdentityProvider, m_ColCreatedByAppIdentityProvider);
		}
		if (p_EducationUser.GetCreatedBy()->Device)
		{
			const auto& device = *p_EducationUser.GetCreatedBy()->Device;
			p_UserRow->AddField(device.DisplayName, m_ColDisplayName);
			p_UserRow->AddField(device.Id, m_ColCreatedByAppId);
			p_UserRow->AddField(device.Email, m_ColCreatedByAppDisplayName);
			p_UserRow->AddField(device.IdentityProvider, m_ColCreatedByAppIdentityProvider);
		}
		if (p_EducationUser.GetCreatedBy()->User)
		{
			const auto& user = *p_EducationUser.GetCreatedBy()->User;
			p_UserRow->AddField(user.DisplayName, m_ColDisplayName);
			p_UserRow->AddField(user.Id, m_ColCreatedByAppId);
			p_UserRow->AddField(user.Email, m_ColCreatedByAppDisplayName);
			p_UserRow->AddField(user.IdentityProvider, m_ColCreatedByAppIdentityProvider);
		}
	}

	// Mailing address
	if (p_EducationUser.GetMailingAddress())
	{
		const auto& mailingAddr = *p_EducationUser.GetMailingAddress();
		p_UserRow->AddField(mailingAddr.City, m_ColMailingAddressCity);
		p_UserRow->AddField(mailingAddr.CountryOrRegion, m_ColMailingAddressCountryOrRegion);
		p_UserRow->AddField(mailingAddr.PostalCode, m_ColMailingAddressPostalCode);
		p_UserRow->AddField(mailingAddr.State, m_ColMailingAddressState);
		p_UserRow->AddField(mailingAddr.Street, m_ColMailingAddressStreet);
	}

	// Related contacts
	if (p_EducationUser.GetRelatedContacts())
	{
		const auto& relatedContacts = *p_EducationUser.GetRelatedContacts();

		vector<boost::YOpt<PooledString>> ids;
		vector<boost::YOpt<PooledString>> displayNames;
		vector<boost::YOpt<PooledString>> emailAddresses;
		vector<boost::YOpt<PooledString>> mobilePhones;
		vector<boost::YOpt<PooledString>> relationships;
		vector<boost::YOpt<bool>> accessConsents;
		
		for (const auto& contact : relatedContacts)
		{
			ids.push_back(contact.m_Id);
			displayNames.push_back(contact.m_DisplayName);
			emailAddresses.push_back(contact.m_EmailAddress);
			mobilePhones.push_back(contact.m_MobilePhone);
			relationships.push_back(contact.m_Relationship);
			accessConsents.push_back(contact.m_AccessConsent);
		}
	}

	// Education student
	if (p_EducationUser.GetStudent())
	{
		const auto& student = *p_EducationUser.GetStudent();
		p_UserRow->AddField(student.m_BirthDate, m_ColStudentBirthDate);
		p_UserRow->AddField(student.m_ExternalId, m_ColStudentBirthDate);
		p_UserRow->AddField(student.m_Gender, m_ColStudentBirthDate);
		p_UserRow->AddField(student.m_Grade, m_ColStudentBirthDate);
		p_UserRow->AddField(student.m_GraduationYear, m_ColStudentBirthDate);
		p_UserRow->AddField(student.m_StudentNumber, m_ColStudentBirthDate);
	}

	// Education teacher
	if (p_EducationUser.GetTeacher())
	{
		const auto& teacher = *p_EducationUser.GetTeacher();
		p_UserRow->AddField(teacher.m_ExternalId, m_ColTeacherExternalId);
		p_UserRow->AddField(teacher.m_TeacherNumber, m_ColTeacherNumber);
	}
}

wstring GridClassMembers::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, m_ColDisplayName);
}

row_pk_t GridClassMembers::UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody)
{
	ASSERT(IsGridModificationsEnabled());
	if (IsGridModificationsEnabled())
	{
		auto mod = GetModifications().GetRowModification<CreatedObjectModification>(rowPk);
		ASSERT(nullptr != mod);
		if (nullptr != mod)
		{
			wstring schoolId;
			wstring classId;
			auto row = GetRowIndex().GetExistingRow(rowPk);
			ASSERT(nullptr != row);
			if (nullptr != row)
			{
				schoolId = row->GetField(m_ColSchoolId).GetValueStr();
				classId = row->GetField(m_ColClassId).GetValueStr();
			}

			// Check Create call that generated this data: Need to request RBAC props?
			EducationUserDeserializer deserializer(GetSession());
			deserializer.Deserialize(json::value::parse(creationResultBody));
			const auto& eduUser = deserializer.GetData();
			auto newPk = CreatePk(schoolId, classId, eduUser.GetID());
			mod->SetNewPK(newPk);

			return newPk;
		}
	}

	return rowPk;
}

void GridClassMembers::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameClassMembers, LicenseUtil::g_codeSapio365classMembers/*,
			{
				{ &m_ColMetaDisplayName, g_TitleOwnerDisplayName, g_FamilyOwner },
				{ &m_ColMetaUserPrincipalName, g_TitleOwnerUserName, g_FamilyOwner },
				{ &m_ColMetaId, g_TitleOwnerID, g_FamilyOwner }
			}*/);
		setup.Setup(*this, false);
	}

	addColumnGraphID();
	setBasicGridSetupHierarchy({ { { m_ColId, 0 } }
								,{ rttr::type::get<EducationUser>().get_id() }
								,{ rttr::type::get<EducationSchool>().get_id(), rttr::type::get<EducationClass>().get_id(), rttr::type::get<EducationUser>().get_id() } });

	// Simple columns
	m_ColSchoolId = AddColumn(_YUID("schoolId"), _T("School Id"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetTech });
	m_ColClassId = AddColumn(_YUID("classId"), _T("Class Id"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetTech });
	m_ColDisplayName = AddColumn(_YUID("displayName"), _T("Display Name"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColExternalSource = AddColumn(_YUID("externalSource"), _T("External source"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMiddleName = AddColumn(_YUID("middleName"), _T("Middle name"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColPrimaryRole = AddColumn(_YUID("primaryRole"), _T("Primary role"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Created by
	m_ColCreatedByAppDisplayName = AddColumn(_YUID("createdByAppDisplayName"), _T("Display name - Created by - App"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByAppId = AddColumn(_YUID("createdByAppId"), _T("Id - Created by - App"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByAppEmail = AddColumn(_YUID("createdByAppEmail"), _T("Email - Created by - App"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByAppIdentityProvider = AddColumn(_YUID("createdByAppIdentityProvider"), _T("Identity Provider - Created by - App"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceDisplayName = AddColumn(_YUID("createdByDeviceDisplayName"), _T("Display name - Created by - Device"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceId = AddColumn(_YUID("createdByDeviceId"), _T("Id - Created by - Device"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceEmail = AddColumn(_YUID("createdByDeviceEmail"), _T("Email - Created by - Device"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByDeviceIdentityProvider = AddColumn(_YUID("createdByDeviceIdentityProvider"), _T("Identity provider - Created by - Device"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserDisplayName = AddColumn(_YUID("createdByUserDisplayName"), _T("Display name - Created by - User"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserId = AddColumn(_YUID("createdByUserId"), _T("Id - Created by - User"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserEmail = AddColumn(_YUID("createdByUserEmail"), _T("Email - Created by - User"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColCreatedByUserIdentityProvider = AddColumn(_YUID("createdByUserIdentityProvider"), _T("Identity provider - Created by - User"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Mailing address
	m_ColMailingAddressCity = AddColumn(_YUID("mailingAddressCity"), _T("City - Mailing address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressCountryOrRegion = AddColumn(_YUID("mailingAddressCountryOrRegion"), _T("Country or region - Mailing address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressPostalCode = AddColumn(_YUID("mailingAddressPostalCode"), _T("Postal code - Mailing address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressState = AddColumn(_YUID("mailingAddressState"), _T("State - Mailing address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColMailingAddressStreet = AddColumn(_YUID("mailingAddressStreet"), _T("Street - Mailing address"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Related contacts
	m_ColRelatedContactId = AddColumn(_YUID("relatedContactId"), _T("Id - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColRelatedContactDisplayName = AddColumn(_YUID("relatedContactDisplayName"), _T("Display name - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColRelatedContactEmailAddress = AddColumn(_YUID("relatedContactEmailAddress"), _T("Email address - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColRelatedContactMobilePhone = AddColumn(_YUID("relatedContactMobilePhone"), _T("Mobile phone - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColRelatedContactRelationship = AddColumn(_YUID("relatedContactRelationship"), _T("Relationship - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColRelatedContactAccessContent = AddColumn(_YUID("relatedContactAccessContent"), _T("Access content - Related contact"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Education student
	m_ColStudentBirthDate = AddColumn(_YUID("studentBirthDate"), _T("Birth date - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColStudentExternalId = AddColumn(_YUID("studentExternalId"), _T("External id - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColStudentGender = AddColumn(_YUID("studentGender"), _T("Gender - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColStudentGrade = AddColumn(_YUID("studentGrade"), _T("Grade - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColStudentGraduationYear = AddColumn(_YUID("studentGraduationYear"), _T("Graduation year - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColStudentNumber = AddColumn(_YUID("studentNumber"), _T("Number - Student"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	// Education teacher
	m_ColTeacherExternalId = AddColumn(_YUID("teacherExternalId"), _T("External id - Teacher"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });
	m_ColTeacherNumber = AddColumn(_YUID("teacherNumber"), _T("Number - Teacher"), g_FamilyInfo, g_ColumnSize16, { g_ColumnsPresetDefault });

	EnableGridModifications();

	AddColumnForRowPK(m_ColSchoolId);
	AddColumnForRowPK(m_ColClassId);
	AddColumnForRowPK(GetColId());

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameClassMembers*>(GetParentFrame()));
}

EducationClass GridClassMembers::getBusinessObject(GridBackendRow*) const
{
	return EducationClass();
}

void GridClassMembers::OnAddMember()
{
	if (IsFrameConnected() && showUserRestrictedAccessDialog(USER_ULTRA_ADMIN_REQUIREMENT))
	{
		const auto usersInGrid = GetUsersInGrid();

		// Get the users to add to selected classes
		DlgAddItemsToGroups dlg(
			GetSession(),
			{},
			DlgAddItemsToGroups::SELECT_USERS_FOR_SELECTION,
			usersInGrid, {}, {},
			_T("Select members to add to selected classes"),
			_T(""), RoleDelegationUtil::RBAC_Privilege::RBAC_NONE, this);
		if (dlg.DoModal() == IDOK)
		{
			auto users = dlg.GetSelectedItems();

			// Add "to be created" rows
			bool refresh = false;
			for (auto classRow : GetSelectedClassRows())
			{
				for (const auto& user : dlg.GetSelectedItems())
				{
					PooledString schoolId = classRow->GetField(m_ColSchoolId).GetValueStr();
					PooledString classId = classRow->GetField(m_ColClassId).GetValueStr();

					row_pk_t userRowPk = CreatePk(schoolId, classId, user.GetID());
					GridBackendRow* userRow = m_RowIndex.GetExistingRow(userRowPk);
					if (userRow == nullptr)
					{
						GridBackendRow* newUserRow = CreateNewUserRow(schoolId, classId, user);
						ASSERT(nullptr != newUserRow);
						if (nullptr != newUserRow)
						{
							newUserRow->SetParentRow(classRow);
							vector<GridBackendField> rowPk;
							GetRowPK(newUserRow, rowPk);

							GetModifications().Add(std::make_unique<CreatedObjectModification>(*this, rowPk));
							refresh = true;
						}
					}
					else if (userRow->IsDeleted())
					{
						refresh = GetModifications().Revert(userRowPk, false);
					}
				}
			}

			if (refresh)
				UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
		}
	}
}

set<GridBackendRow*> GridClassMembers::GetSelectedClassRows() const
{
	set<GridBackendRow*> classRows;

	vector<GridBackendRow*> selectedRows;
	GetBackend().GetSelectedNonGroupRows(selectedRows);

	for (auto selectedRow : selectedRows)
	{
		if (GridUtil::isBusinessType<EducationClass>(selectedRow))
		{
			classRows.insert(selectedRow);
		}
		else
		{
			GridBackendRow* parentRow = selectedRow->GetParentRow();
			if (GridUtil::isBusinessType<EducationClass>(parentRow))
				classRows.insert(parentRow);
		}
	}

	return classRows;
}

GridBackendRow* GridClassMembers::CreateNewUserRow(PooledString p_SchoolId, PooledString p_ClassId, const SelectedItem& p_User)
{
	GridBackendRow* row = m_RowIndex.GetRow({ CreatePk(p_SchoolId, p_ClassId, p_User.GetID()) }, true, false);

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		row->AddField(p_User.GetDisplayName(), m_ColDisplayName);
		SetRowObjectType(row, EducationUser());
	}
	
	return row;
}

vector<GridBackendField> GridClassMembers::CreatePk(PooledString p_SchoolId, PooledString p_ClassId, PooledString p_UserId) const
{
	if (p_UserId.IsEmpty())
	{
		if (!p_ClassId.IsEmpty())
			p_UserId = p_ClassId;
		else if (!p_SchoolId.IsEmpty())
			p_UserId = p_SchoolId;
		else
			ASSERT(false);
	}

	return vector<GridBackendField> {
		GridBackendField(p_UserId, GetColId()),
		GridBackendField(p_SchoolId, m_ColSchoolId),
		GridBackendField(p_ClassId, m_ColClassId)
	};
}

vector<BusinessUser> GridClassMembers::GetUsersInGrid()
{
	vector<BusinessUser> users;

	for (auto row : GetBackendRows())
	{
		if (!row->IsExplosionAdded() && GridUtil::isBusinessType<EducationUser>(row))
		{
			BusinessUser user;
			user.SetID(PooledString(row->GetField(m_ColId).GetValueStr()));
			user.SetDisplayName(PooledString(row->GetField(m_ColDisplayName).GetValueStr()));
			users.push_back(user);
		}
	}

	GetSession()->GetGraphCache().SyncUncachedUsers(users, YtriaTaskData(), false);

	return users;
}

void GridClassMembers::OnUpdateAddMember(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && HasClassOrUserSelected());
	setTextFromProfUISCommand(*pCmdUI);
}

void GridClassMembers::OnRemoveMember()
{
	if (!showUserRestrictedAccessDialog(USER_ULTRA_ADMIN_REQUIREMENT))
		return;

	vector<GridBackendRow*> rows;
	GetSelectedNonGroupRows(rows);

	bool update = false;
	for (auto row : rows)
	{
		if (GridUtil::isBusinessType<EducationUser>(row))
		{
			row_pk_t rowPk;
			GetRowPK(row, rowPk);
			if (row->IsCreated())
			{
				if (GetModifications().Revert(rowPk, false))
					update = true;
			}
			else
			{
				GetModifications().Add(std::make_unique<DeletedObjectModification>(*this, rowPk));
				update = true;
			}
		}
	}

	if (update)
		UpdateMegaShark(GridBackendUtil::GRIDUPDATE_MAIN_WITHFILTERS);
}

void GridClassMembers::OnUpdateRemoveMember(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(hasNoTaskRunning() && HasUserSelected());
	setTextFromProfUISCommand(*pCmdUI);
}

bool GridClassMembers::HasUserSelected()
{
	bool hasUserSelected = false;

	for (auto row : GetBackend().GetSelectedRows())
	{
		if (GridUtil::isBusinessType<EducationUser>(row))
		{
			hasUserSelected = true;
			break;
		}
	}

	return hasUserSelected;
}

bool GridClassMembers::HasClassOrUserSelected()
{
	bool hasClassOrUserSelected = false;

	for (auto row : GetBackend().GetSelectedRows())
	{
		if (GridUtil::isBusinessType<EducationUser>(row) || GridUtil::isBusinessType<EducationClass>(row))
		{
			hasClassOrUserSelected = true;
			break;
		}
	}

	return hasClassOrUserSelected;
}

void GridClassMembers::InitializeCommands()
{
	ModuleO365Grid<EducationClass>::InitializeCommands();

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);
	
	{
		CExtCmdItem cmd;
		cmd.m_nCmdID = ID_CLASSMEMBERSGRID_ADDMEMBER;
		cmd.m_sMenuText = _T("Add member");
		cmd.m_sToolbarText = _T("Add member");
		cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, cmd, true);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CLASSMEMBERSGRID_ADDMEMBER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CLASSMEMBERSGRID_ADDMEMBER, hIcon, false);
		setRibbonTooltip(cmd.m_nCmdID,
			_T(""),
			_T(""),
			cmd.m_sAccelText);
	}

	{
		CExtCmdItem cmd;
		cmd.m_nCmdID = ID_CLASSMEMBERSGRID_REMOVEMEMBER;
		cmd.m_sMenuText = _T("Remove member");
		cmd.m_sToolbarText = _T("Remove member");
		cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, cmd, true);
		HICON hIcon = MFCUtil::getHICONFromImageResourceID(IDB_CLASSMEMBERSGRID_REMOVEMEMBER_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_CLASSMEMBERSGRID_REMOVEMEMBER, hIcon, false);
		setRibbonTooltip(cmd.m_nCmdID,
			_T(""),
			_T(""),
			cmd.m_sAccelText);
	}
}

void GridClassMembers::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart)
{
	ModuleO365Grid<EducationClass>::OnCustomPopupMenu(pPopup, nStart);	
}
