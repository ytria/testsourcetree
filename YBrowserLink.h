#pragma once

#include "LicenseUtil.h"
#include <cpprest/uri.h>

class AutomationAction;
class YBrowserLink
{
public:
    YBrowserLink(const LicenseContext& p_LicenseContext, const web::uri& p_Uri);
	YBrowserLink(const LicenseContext& p_LicenseContext, const web::uri& p_Uri, AutomationAction* p_Action);

    const web::uri& GetUrl() const;

	const LicenseContext& GetLicenseContext(const LicenseContext::TokenRateType p_TR);
	const LicenseContext& GetLicenseContext() const;

	AutomationAction* GetAutomationAction() const;

	void ENABLE_FREE_ACCESS();// set license free access
	void SetIsLicenseAJL(const bool p_LicenseIsAJL);

	virtual bool IsAllowedWithAJLLicense() const;

private:
    web::uri m_Uri;
	AutomationAction* m_Action;

	LicenseContext m_LicenseContext;
};