#include "EducationClassCreateRequester.h"
#include "O365AdminUtil.h"
#include "Sapio365Session.h"

EducationClassCreateRequester::EducationClassCreateRequester(const wstring& p_SchoolId, const wstring& p_ClassId)
	:m_SchoolId(p_SchoolId),
	m_ClassId(p_ClassId)
{

}

pplx::task<void> EducationClassCreateRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	web::uri_builder uri(_YTEXT("education"));
	uri.append_path(_YTEXT("schools"));
	uri.append_path(m_SchoolId);
	uri.append_path(_YTEXT("classes"));
	uri.append_path(_YTEXT("$ref"));

	LoggerService::Friendly(_YFORMAT(L"Creating class for school with id %s", m_SchoolId.c_str()), p_TaskData.GetOriginator());

	return p_Session->GetMSGraphSession(false)->Post(uri.to_uri()).ThenByTask([result = m_Result](pplx::task<RestResultInfo> p_ResInfo) {
		*result = Util::GetResult(p_ResInfo);
	});
}

const HttpResultWithError& EducationClassCreateRequester::GetResult() const
{
	return *m_Result;
}
