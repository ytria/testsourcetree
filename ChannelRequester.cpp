#include "ChannelRequester.h"

#include "Sapio365Session.h"
#include "MsGraphHttpRequestLogger.h"

ChannelRequester::ChannelRequester(const PooledString& p_TeamId, const PooledString& p_ChannelId, const std::shared_ptr<IRequestLogger>& p_Logger)
	: m_TeamId(p_TeamId)
	, m_ChannelId(p_ChannelId)
	, m_Logger(p_Logger)
{

}

TaskWrapper<void> ChannelRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	ASSERT(m_Deserializer);
	web::uri_builder uri;
	uri.append_path(_YTEXT("teams"));
	uri.append_path(m_TeamId);
	uri.append_path(_YTEXT("channels"));
	uri.append_path(m_ChannelId);

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->getObject(m_Deserializer, uri, true, m_Logger, httpLogger, p_TaskData);
}

const BusinessChannel& ChannelRequester::GetData() const
{
	ASSERT(m_Deserializer);
	return m_Deserializer->GetData();
}
