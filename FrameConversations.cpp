#include "FrameConversations.h"

#include "AutomationNames.h"
#include "Command.h"
#include "CommandInfo.h"
#include "CommandDispatcher.h"
#include "ModuleBase.h"

IMPLEMENT_DYNAMIC(FrameConversations, GridFrameBase)

//BEGIN_MESSAGE_MAP(FrameConversations, GridFrameBase)
//END_MESSAGE_MAP()

FrameConversations::FrameConversations(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
    m_CreateAddRemoveToSelection = true;
    m_CreateRefreshPartial = true;
	m_CreateShowContext = true;
}

O365Grid& FrameConversations::GetGrid()
{
    return m_ConversationsGrid;
}

void FrameConversations::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
	ASSERT(ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer);
	if (ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer)
		info.GetIds() = GetModuleCriteria().m_IDs;
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Conversation, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameConversations::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Conversation, Command::ModuleTask::UpdateRefresh, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameConversations::ApplyAllSpecific()
{
}

void FrameConversations::ShowConversations(const O365DataMap<BusinessGroup, vector<BusinessConversation>>& p_Conversations, bool p_FullPurge)
{
	if (CompliesWithDataLoadPolicy())
		m_ConversationsGrid.BuildView(p_Conversations, p_FullPurge);
}

void FrameConversations::createGrid()
{
    m_ConversationsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameConversations::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigGroups();
}

// returns false if no frame specific tab is needed.
bool FrameConversations::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewDataEditGroups(tab);

	{
		auto group = tab.AddGroup(g_ActionsLinkConversation.c_str());
		ASSERT(nullptr != group);
		if (nullptr != group)
		{
			setGroupImage(*group, 0);

			addNewModuleCommandControl(*group, ID_CONVERSATIONSGRID_SHOWPOSTS, { ID_CONVERSATIONSGRID_SHOWPOSTS_FRAME, ID_CONVERSATIONSGRID_SHOWPOSTS_NEWFRAME }, { IDB_CONVERSATIONS_SHOWPOSTS, IDB_CONVERSATIONS_SHOWPOSTS_16X16 });
		}
	}

	CreateLinkGroups(tab, GetOrigin() == Origin::User, GetOrigin() == Origin::Group);

    return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameConversations::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	const bool newFrame = nullptr != p_Action && p_Action->IsParamTrue(g_NewFrame);

	if (IsActionSelectedShowPosts(p_Action))
	{
		p_CommandID = newFrame ? ID_CONVERSATIONSGRID_SHOWPOSTS_NEWFRAME : ID_CONVERSATIONSGRID_SHOWPOSTS_FRAME;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}

	return statusRV;
}