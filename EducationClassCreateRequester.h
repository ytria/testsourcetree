#pragma once

#include "IRequester.h"
#include "HttpResultWithError.h"

class EducationClassCreateRequester : public IRequester
{
public:
	EducationClassCreateRequester(const wstring& p_SchoolId, const wstring& p_ClassId);
	pplx::task<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const HttpResultWithError& GetResult() const;

private:
	std::shared_ptr<HttpResultWithError> m_Result;

	wstring m_SchoolId;
	wstring m_ClassId;
};

