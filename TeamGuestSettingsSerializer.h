#pragma once

#include "IJsonSerializer.h"
#include "TeamGuestSettings.h"

class TeamGuestSettingsSerializer : public IJsonSerializer
{
public:
    TeamGuestSettingsSerializer(const TeamGuestSettings& p_Settings);
    void Serialize() override;

private:
    const TeamGuestSettings& m_Settings;
};

