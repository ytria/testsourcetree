#pragma once

#include "IRequester.h"
#include "IPageRequestLogger.h"

template <class T, class U>
class ValueListDeserializer;

class BusinessOrgContact;
class OrgContactDeserializer;
class IPropertySetBuilder;
class PaginatedRequestResults;

class OrgContactListRequester : public IRequester
{
public:
	OrgContactListRequester(IPropertySetBuilder& p_PropertySet, const std::shared_ptr<IPageRequestLogger>& p_Logger);

    virtual TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    vector<BusinessOrgContact>& GetData();

private:
    vector<rttr::property> m_Properties;

    std::shared_ptr<ValueListDeserializer<BusinessOrgContact, OrgContactDeserializer>> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;
};

