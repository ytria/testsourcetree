#include "SharingLinkDeserializer.h"

#include "IdentityDeserializer.h"
#include "JsonSerializeUtil.h"

void SharingLinkDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        IdentityDeserializer id;
        if (JsonSerializeUtil::DeserializeAny(id, _YTEXT("application"), p_Object))
            m_Data.Application = id.GetData();
    }

    JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.Type, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("scope"), m_Data.Scope, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("webHtml"), m_Data.WebHtml, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("webUrl"), m_Data.WebUrl, p_Object);
}
