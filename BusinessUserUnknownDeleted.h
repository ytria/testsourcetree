#pragma once

#include "BusinessUser.h"

// only for identification issues in grids
class BusinessUserUnknownDeleted	: public BusinessUser
{
public:
	using BusinessUser::BusinessUser;

private:
	RTTR_ENABLE(BusinessUser)
};
