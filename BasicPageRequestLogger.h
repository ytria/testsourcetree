#pragma once

#include "IPageRequestLogger.h"

class BasicPageRequestLogger : public IPageRequestLogger
{
public:
	BasicPageRequestLogger(const wstring& p_LogMessage);

	void Log(const web::uri& p_Uri, size_t p_ObjCountToDate, HWND p_Originator) override;
	std::shared_ptr<IPageRequestLogger> Clone() const override;
};

