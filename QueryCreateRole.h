#pragma once

#include "ISqlQuery.h"

class PersistentSession;
class SessionsSqlEngine;

class QueryCreateRole : public ISqlQuery
{
public:
	QueryCreateRole(int64_t p_RoleId, const wstring& p_Name, SessionsSqlEngine& p_Engine);
	void Run() override;

private:
	SessionsSqlEngine& m_Engine;
	int64_t m_RoleId;
	wstring m_Name;
};

