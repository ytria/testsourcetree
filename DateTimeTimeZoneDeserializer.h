#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class DateTimeTimeZoneDeserializer : public JsonObjectDeserializer, public Encapsulate<DateTimeTimeZone>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

