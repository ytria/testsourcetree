#include "GridOnPremiseGroups.h"

#include "AutomationWizardOnPremiseGroups.h"
#include "BasicGridSetup.h"
#include "GridUpdaterOptions.h"
#include "GridUpdater.h"
#include "FrameOnPremiseGroups.h"
#include "OnPremiseGroup.h"

GridOnPremiseGroups::GridOnPremiseGroups()
	: m_Frame(nullptr),
	m_ColLastRequestDateTime(nullptr),
	m_ColAdminCount(nullptr),
	m_ColCanonicalName(nullptr),
	m_ColCN(nullptr),
	m_ColCreated(nullptr),
	m_ColCreateTimeStamp(nullptr),
	m_ColDeleted(nullptr),
	m_ColDescription(nullptr),
	m_ColDisplayName(nullptr),
	m_ColDistinguishedName(nullptr),
	m_ColDSCorePropagationData(nullptr),
	m_ColGroupCategory(nullptr),
	m_ColGroupScope(nullptr),
	m_ColGroupType(nullptr),
	m_ColHomePage(nullptr),
	m_ColInstanceType(nullptr),
	m_ColIsCriticalSystemObject(nullptr),
	m_ColIsDeleted(nullptr),
	m_ColLastKnownParent(nullptr),
	m_ColManagedBy(nullptr),
	m_ColMember(nullptr),
	m_ColMemberOf(nullptr),
	m_ColMembers(nullptr),
	m_ColModified(nullptr),
	m_ColModifyTimeStamp(nullptr),
	m_ColName(nullptr),
	m_ColNTSecurityDescriptor(nullptr),
	m_ColObjectCategory(nullptr),
	m_ColObjectClass(nullptr),
	m_ColObjectGUID(nullptr),
	m_ColObjectSid(nullptr),
	m_ColProtectedFromAccidentalDeletion(nullptr),
	m_ColSamAccountName(nullptr),
	m_ColSAMAccountType(nullptr),
	m_ColSDRightsEffective(nullptr),
	m_ColSID(nullptr),
	m_ColSIDHistory(nullptr),
	m_ColSystemFlags(nullptr),
	m_ColUSNChanged(nullptr),
	m_ColUSNCreated(nullptr),
	m_ColWhenChanged(nullptr),
	m_ColWhenCreated(nullptr)
{
	UseDefaultActions(0);
	initWizard<AutomationWizardOnPremiseGroups>(_YTEXT("Automation\\OnPremiseGroups"));
}

void GridOnPremiseGroups::customizeGrid()
{
	{
		BasicGridSetup setup(GridUtil::g_AutoNameOnPremiseGroups, LicenseUtil::g_codeSapio365onPremiseGroups);
		setup.Setup(*this, true);
		m_ColLastRequestDateTime = setup.GetColumnLastRequestDateTime();
	}

	setBasicGridSetupHierarchy({ { { m_ColDistinguishedName, 0 } }
								, { rttr::type::get<OnPremiseGroup>().get_id() }
								, { rttr::type::get<OnPremiseGroup>().get_id() } });

	m_ColSamAccountName = AddColumn(_YUID("samAccountname"), _T("Sam Account Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });

	m_ColCanonicalName = AddColumn(_YUID("canonicalName"), _T("Canonical name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColAdminCount = AddColumnNumberInt(_YUID("adminCount"), _T("Admin Count"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColCN = AddColumn(_YUID("cn"), _T("CN"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColCreated = AddColumnDate(_YUID("created"), _T("Created"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColCreateTimeStamp = AddColumnDate(_YUID("createTimeStamp"), _T("Create TimeStamp"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColDeleted = AddColumnCheckBox(_YUID("deleted"), _T("Deleted"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColDescription = AddColumn(_YUID("description"), _T("Description"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColDisplayName = AddColumn(_YUID("displayName"), _T("Display Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColDistinguishedName = AddColumn(_YUID("distinguishedName"), _T("Distinguished Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColDSCorePropagationData = AddColumn(_YUID("dsCorePropagationData"), _T("DS Core Propagation Data"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColGroupCategory = AddColumn(_YUID("groupCategory"), _T("Group Category"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColGroupScope = AddColumn(_YUID("groupScope"), _T("Group Scope"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColGroupType = AddColumnNumberInt(_YUID("groupType"), _T("Group Type"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColHomePage = AddColumn(_YUID("homePage"), _T("HomePage"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColInstanceType = AddColumnNumberInt(_YUID("instanceType"), _T("Instance Type"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColIsCriticalSystemObject = AddColumnCheckBox(_YUID("isCriticalSystemObject"), _T("Is Critical System Object"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColIsDeleted = AddColumnCheckBox(_YUID("isDeleted"), _T("Is Deleted"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColLastKnownParent = AddColumn(_YUID("lastKnownParent"), _T("Last Known Parent"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColManagedBy = AddColumn(_YUID("managedBy"), _T("Managed By"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColMember = AddColumn(_YUID("member"), _T("Member"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColMemberOf = AddColumn(_YUID("memberOf"), _T("Member of"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColMembers = AddColumn(_YUID("members"), _T("Members"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColModified = AddColumnDate(_YUID("modified"), _T("Modified"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColModifyTimeStamp = AddColumnDate(_YUID("modifyTimeStamp"), _T("Modify TimeStamp"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColName = AddColumn(_YUID("name"), _T("Name"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColObjectCategory = AddColumn(_YUID("objectCategory"), _T("Object Category"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColObjectClass = AddColumn(_YUID("objectClass"), _T("Object Class"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColObjectGUID = AddColumn(_YUID("objectGUID"), _T("Object GUID"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColObjectSid = AddColumn(_YUID("objectSid"), _T("Object Sid"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColProtectedFromAccidentalDeletion = AddColumnCheckBox(_YUID("protectedFromAccidentalDeletion"), _T("Protected from accidental deletion"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColSAMAccountType = AddColumnNumberInt(_YUID("sAMAccountType"), _T("SAM Account Type"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColSDRightsEffective = AddColumnNumberInt(_YUID("sDRightsEffective"), _T("SD Rights Effective"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColSID = AddColumn(_YUID("sid"), _T("SID"), g_FamilyInfo, g_ColumnSize22, { g_ColumnsPresetDefault });
	m_ColSystemFlags = AddColumnNumberInt(_YUID("systemFlags"), _T("System Flags"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColUSNChanged = AddColumnDate(_YUID("uSNChanged"), _T("USN Changed"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColUSNCreated = AddColumnDate(_YUID("usnCreated"), _T("USN Created"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColWhenChanged = AddColumnDate(_YUID("whenChanged"), _T("When Changed"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });
	m_ColWhenCreated = AddColumnDate(_YUID("whenCreated"), _T("When Created"), g_FamilyInfo, g_ColumnSize8, { g_ColumnsPresetDefault });

	addColumnGraphID();
	AddColumnForRowPK(GetColId());

	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(dynamic_cast<FrameOnPremiseGroups*>(GetParentFrame()));
}

void GridOnPremiseGroups::BuildView(const vector<OnPremiseGroup>& p_Groups, bool p_FullPurge)
{
	if (p_Groups.size() == 1 && p_Groups[0].GetError() != boost::none && p_Groups[0].GetID().IsEmpty())
	{
		HandlePostUpdateError(p_Groups[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_Groups[0].GetError()->GetFullErrorMessage());
	}
	else
	{
		GridUpdaterOptions options(GridUpdaterOptions::UPDATE_GRID
			| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : GridUpdaterOptions::PARTIALPURGE)
			| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
			| GridUpdaterOptions::REFRESH_PROCESS_DATA
		);
		GridUpdater updater(*this, options);

		for (const auto& onPremGroup : p_Groups)
		{
			GridBackendField fID(onPremGroup.GetObjectGUID(), GetColId());
			GridBackendRow* row = m_RowIndex.GetRow({ fID }, true, true);

			SetRowObjectType(row, onPremGroup);
			FillRow(row, onPremGroup);
		}
	}
}

OnPremiseGroup GridOnPremiseGroups::getBusinessObject(GridBackendRow*) const
{
	return OnPremiseGroup();
}

void GridOnPremiseGroups::UpdateBusinessObjects(const vector<OnPremiseGroup>& p_Permissions, bool p_SetModifiedStatus)
{

}

void GridOnPremiseGroups::RemoveBusinessObjects(const vector<OnPremiseGroup>& p_Permissions)
{

}

wstring GridOnPremiseGroups::GetName(const GridBackendRow* p_Row) const
{
	return wstring();
}

GridBackendRow* GridOnPremiseGroups::FillRow(GridBackendRow* p_Row, const OnPremiseGroup& p_OnPremiseGroup)
{
	ASSERT(nullptr != p_Row);
	if (nullptr != p_Row)
	{
		p_Row->AddField(p_OnPremiseGroup.GetAdminCount(), m_ColAdminCount);
		p_Row->AddField(p_OnPremiseGroup.GetCanonicalName(), m_ColCanonicalName);
		p_Row->AddField(p_OnPremiseGroup.GetCN(), m_ColCN);
		p_Row->AddField(p_OnPremiseGroup.GetCreated(), m_ColCreated);
		p_Row->AddField(p_OnPremiseGroup.GetCreateTimeStamp(), m_ColCreateTimeStamp);
		p_Row->AddField(p_OnPremiseGroup.GetDeleted(), m_ColDeleted);
		p_Row->AddField(p_OnPremiseGroup.GetDescription(), m_ColDescription);
		p_Row->AddField(p_OnPremiseGroup.GetDisplayName(), m_ColDisplayName);
		p_Row->AddField(p_OnPremiseGroup.GetDistinguishedName(), m_ColDistinguishedName);
		p_Row->AddField(p_OnPremiseGroup.GetDSCorePropagationData(), m_ColDSCorePropagationData);
		p_Row->AddField(p_OnPremiseGroup.GetGroupCategory(), m_ColGroupCategory);
		p_Row->AddField(p_OnPremiseGroup.GetGroupScope(), m_ColGroupScope);
		p_Row->AddField(p_OnPremiseGroup.GetGroupType(), m_ColGroupType);
		p_Row->AddField(p_OnPremiseGroup.GetHomePage(), m_ColHomePage);
		p_Row->AddField(p_OnPremiseGroup.GetInstanceType(), m_ColInstanceType);
		p_Row->AddField(p_OnPremiseGroup.GetIsCriticalSystemObject(), m_ColIsCriticalSystemObject);
		p_Row->AddField(p_OnPremiseGroup.GetIsDeleted(), m_ColIsDeleted);
		p_Row->AddField(p_OnPremiseGroup.GetLastKnownParent(), m_ColLastKnownParent);
		p_Row->AddField(p_OnPremiseGroup.GetManagedBy(), m_ColManagedBy);
		p_Row->AddField(p_OnPremiseGroup.GetMember(), m_ColMember);
		p_Row->AddField(p_OnPremiseGroup.GetMemberOf(), m_ColMemberOf);
		p_Row->AddField(p_OnPremiseGroup.GetMembers(), m_ColMembers);
		p_Row->AddField(p_OnPremiseGroup.GetModified(), m_ColModified);
		p_Row->AddField(p_OnPremiseGroup.GetModifyTimeStamp(), m_ColModifyTimeStamp);
		p_Row->AddField(p_OnPremiseGroup.GetName(), m_ColName);
		p_Row->AddField(p_OnPremiseGroup.GetObjectCategory(), m_ColObjectCategory);
		p_Row->AddField(p_OnPremiseGroup.GetObjectClass(), m_ColObjectClass);
		p_Row->AddField(p_OnPremiseGroup.GetObjectGUID(), m_ColObjectGUID);
		p_Row->AddField(p_OnPremiseGroup.GetObjectSid(), m_ColObjectSid);
		p_Row->AddField(p_OnPremiseGroup.GetProtectedFromAccidentalDeletion(), m_ColProtectedFromAccidentalDeletion);
		p_Row->AddField(p_OnPremiseGroup.GetSamAccountName(), m_ColSamAccountName);
		p_Row->AddField(p_OnPremiseGroup.GetSAMAccountType(), m_ColSAMAccountType);
		p_Row->AddField(p_OnPremiseGroup.GetSDRightsEffective(), m_ColSDRightsEffective);
		p_Row->AddField(p_OnPremiseGroup.GetSID(), m_ColSID);
		p_Row->AddField(p_OnPremiseGroup.GetSystemFlags(), m_ColSystemFlags);
		AddDateOrNeverField(p_OnPremiseGroup.GetUSNChanged(), *p_Row, *m_ColUSNChanged);
		AddDateOrNeverField(p_OnPremiseGroup.GetUSNCreated(), *p_Row, *m_ColUSNCreated);
		p_Row->AddField(p_OnPremiseGroup.GetWhenChanged(), m_ColWhenChanged);
		p_Row->AddField(p_OnPremiseGroup.GetWhenCreated(), m_ColWhenCreated);
	}

	return p_Row;
}

void GridOnPremiseGroups::AddDateOrNeverField(const boost::YOpt<YTimeDate>& p_Date, GridBackendRow& p_Row, GridBackendColumn& p_Col)
{
	if (p_Date)
		p_Row.AddField(*p_Date, &p_Col);
	else
		p_Row.AddField(_T("Never"), &p_Col);
}

bool GridOnPremiseGroups::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return false;
}
