#include "FrameSites.h"

#include "AutomationNames.h"
#include "CommandInfo.h"
#include "CommandDispatcher.h"

IMPLEMENT_DYNAMIC(FrameSites, GridFrameBase)

FrameSites::FrameSites(Origin p_Origin, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
	, m_SitesGrid(p_Origin)
{
	m_AddRBACHideOutOfScope = Origin::Tenant == p_Origin;
}

void FrameSites::ShowSites(const vector<BusinessSite>& p_Sites, const vector<BusinessSite>& p_LoadedMoreSites, bool p_FromRefresh)
{
	ASSERT(Origin::Tenant == GetOrigin());
	m_SitesGrid.BuildView(p_Sites, p_LoadedMoreSites);
}

void FrameSites::ShowSites(const O365DataMap<BusinessGroup, BusinessSite>& p_Sites, const vector<BusinessSite>& p_LoadedMoreSites, bool p_FullPurge)
{
	ASSERT(Origin::Group == GetOrigin());
	m_SitesGrid.BuildView(p_Sites, p_LoadedMoreSites, p_FullPurge);
}

void FrameSites::ShowSites(const O365DataMap<BusinessChannel, BusinessSite>& p_Sites, const vector<BusinessSite>& p_LoadedMoreSites, bool p_FullPurge)
{
	ASSERT(Origin::Channel == GetOrigin());
	m_SitesGrid.BuildView(p_Sites, p_LoadedMoreSites, p_FullPurge);
}

void FrameSites::UpdateSitesLoadMore(const vector<BusinessSite>& p_Sites, bool p_FromRefresh)
{
	m_SitesGrid.UpdateSitesLoadMore(p_Sites);
}

O365Grid& FrameSites::GetGrid()
{
    return m_SitesGrid;
}

void FrameSites::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.SetOrigin(GetModuleCriteria().m_Origin);

	if (Origin::Group == info.GetOrigin())
	{
		ASSERT(ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer);
		if (ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer)
			info.GetIds() = GetModuleCriteria().m_IDs;
	}
	else if (Origin::Channel == info.GetOrigin())
	{
		ASSERT(ModuleCriteria::UsedContainer::SUBIDS == GetModuleCriteria().m_UsedContainer);
		if (ModuleCriteria::UsedContainer::SUBIDS == GetModuleCriteria().m_UsedContainer)
			info.GetSubIds() = GetModuleCriteria().m_SubIDs;
	}
	else
	{
		ASSERT(Origin::Tenant == info.GetOrigin());
	}
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Sites, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameSites::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.Data() = p_RefreshData;
	info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	if (ModuleCriteria::UsedContainer::SUBIDS == p_RefreshData.m_Criteria.m_UsedContainer)
		info.GetSubIds() = p_RefreshData.m_Criteria.m_SubIDs;
	else if (ModuleCriteria::UsedContainer::IDS == p_RefreshData.m_Criteria.m_UsedContainer)
		info.GetIds() = p_RefreshData.m_Criteria.m_IDs;
	else
		ASSERT(false);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Sites,Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameSites::createGrid()
{
    m_SitesGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameSites::GetRefreshPartialControlConfig()
{
	if (Origin::Group == GetOrigin())
		return GetRefreshPartialControlConfigGroups();
	
	ASSERT(Origin::Channel == GetOrigin());
	return GetRefreshPartialControlConfigChannels();
}

bool FrameSites::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	if (Origin::Group == GetOrigin() || Origin::Channel == GetOrigin())
	{
		m_CreateRefreshPartial = true;

		CreateViewDataEditGroups(tab);
		
		// Load more button
		CXTPRibbonGroup* dataGroup = findGroup(tab, g_ActionsDataGroup.c_str());
		ASSERT(nullptr != dataGroup);
		if (nullptr != dataGroup)
		{
			auto ctrl = dataGroup->Add(xtpControlButton, ID_SITESGRID_LOADMORE);
			GridFrameBase::setImage({ ID_SITESGRID_LOADMORE }, IDB_LOADMORE, xtpImageNormal);
			GridFrameBase::setImage({ ID_SITESGRID_LOADMORE }, IDB_LOADMORE_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		CXTPRibbonGroup* linkGroup = tab.AddGroup(g_ActionsLinkSite.c_str());
		ASSERT(nullptr != linkGroup);
		if (nullptr != linkGroup)
		{
			setGroupImage(*linkGroup, 0);

			addNewModuleCommandControl(*linkGroup, ID_SITESGRID_SHOWDRIVEITEMS, { ID_SITESGRID_SHOWDRIVEITEMS_FRAME, ID_SITESGRID_SHOWDRIVEITEMS_NEWFRAME }, { IDB_USERS_SHOW_DRIVEITEMS, IDB_USERS_SHOW_DRIVEITEMS_16X16 });
			addNewModuleCommandControl(*linkGroup, ID_SITESGRID_SHOWLISTS, { ID_SITESGRID_SHOWLISTS_FRAME, ID_SITESGRID_SHOWLISTS_NEWFRAME }, { IDB_SITES_SHOW_LISTS, IDB_SITES_SHOW_LISTS_16X16 });
			if (CRMpipe().IsFeatureSandboxed())
			{
				addNewModuleCommandControl(*dataGroup, ID_SITESGRID_SHOWPERMISSIONS, { ID_SITESGRID_SHOWPERMISSIONS_FRAME, ID_SITESGRID_SHOWPERMISSIONS_NEWFRAME }, { IDB_SITES_SHOW_PERMISSIONS, IDB_SITES_SHOW_PERMISSIONS_16X16 });
				//addNewModuleCommandControl(*dataGroup, ID_SITESGRID_SHOWROLES, { ID_SITESGRID_SHOWROLES_FRAME, ID_SITESGRID_SHOWROLES_NEWFRAME }, { IDB_SITES_SHOW_ROLES, IDB_SITES_SHOW_ROLES_16X16 });
			}
		}


		CreateGroupInfoGroup(tab);
	}
	else if (Origin::Tenant == GetOrigin())
	{
		m_CreateRefreshPartial = false;

		GridFrameBase::customizeActionsRibbonTab(tab, false, false);

		CXTPRibbonGroup* dataGroup = findGroup(tab, g_ActionsDataGroup.c_str());
		ASSERT(nullptr != dataGroup);
		if (nullptr != dataGroup)
		{
			auto ctrl = dataGroup->Add(xtpControlButton, ID_SITESGRID_LOADMORE);
			GridFrameBase::setImage({ ID_SITESGRID_LOADMORE }, IDB_LOADMORE, xtpImageNormal);
			GridFrameBase::setImage({ ID_SITESGRID_LOADMORE }, IDB_LOADMORE_16X16, xtpImageNormal);
			setGridControlTooltip(ctrl);
		}

		CXTPRibbonGroup* linkGroup = tab.AddGroup(g_ActionsLinkSite.c_str());
		ASSERT(nullptr != linkGroup);
		if (nullptr != linkGroup)
		{
			setGroupImage(*linkGroup, 0);

			addNewModuleCommandControl(*linkGroup, ID_SITESGRID_SHOWDRIVEITEMS, { ID_SITESGRID_SHOWDRIVEITEMS_FRAME, ID_SITESGRID_SHOWDRIVEITEMS_NEWFRAME }, { IDB_USERS_SHOW_DRIVEITEMS, IDB_USERS_SHOW_DRIVEITEMS_16X16 });
			addNewModuleCommandControl(*linkGroup, ID_SITESGRID_SHOWLISTS, { ID_SITESGRID_SHOWLISTS_FRAME, ID_SITESGRID_SHOWLISTS_NEWFRAME }, { IDB_SITES_SHOW_LISTS, IDB_SITES_SHOW_LISTS_16X16 });
            if (CRMpipe().IsFeatureSandboxed())
            {
                addNewModuleCommandControl(*linkGroup, ID_SITESGRID_SHOWPERMISSIONS, { ID_SITESGRID_SHOWPERMISSIONS_FRAME, ID_SITESGRID_SHOWPERMISSIONS_NEWFRAME }, { IDB_SITES_SHOW_PERMISSIONS, IDB_SITES_SHOW_PERMISSIONS_16X16 });
                //addNewModuleCommandControl(*linkGroup, ID_SITESGRID_SHOWROLES, { ID_SITESGRID_SHOWROLES_FRAME, ID_SITESGRID_SHOWROLES_NEWFRAME }, { IDB_SITES_SHOW_ROLES, IDB_SITES_SHOW_ROLES_16X16 });
            }
		}
	}
	else
		ASSERT(FALSE);

	return true;
}

void FrameSites::InitModuleCriteria(const ModuleCriteria& p_ModuleCriteria)
{
	GridFrameBase::InitModuleCriteria(p_ModuleCriteria);
	if (p_ModuleCriteria.m_Origin == Origin::Group)
	{
		m_CreateAddRemoveToSelection = true;
		m_CreateRefreshPartial = true;
		m_CreateShowContext = true;
	}
}

const std::map<PooledString, PooledString>& FrameSites::GetChannelNames() const
{
	ASSERT(Origin::Channel == GetOrigin());
	return m_ChannelNames;
}

void FrameSites::SetChannelNames(const std::map<PooledString, PooledString>& p_ChannelNames)
{
	ASSERT(Origin::Channel == GetOrigin());
	m_ChannelNames = p_ChannelNames;
}

AutomatedApp::AUTOMATIONSTATUS FrameSites::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	const bool newFrame = nullptr != p_Action && p_Action->IsParamTrue(g_NewFrame);

	if (IsActionSelectedSiteLoadMore(p_Action))
	{
		p_CommandID = ID_SITESGRID_LOADMORE;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedShowLists(p_Action))
	{
		p_CommandID = newFrame ? ID_SITESGRID_SHOWLISTS_NEWFRAME : ID_SITESGRID_SHOWLISTS_FRAME;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}
	else if (IsActionSelectedShowDriveItemsSites(p_Action))
	{
		p_CommandID = newFrame ? ID_SITESGRID_SHOWDRIVEITEMS_NEWFRAME : ID_SITESGRID_SHOWDRIVEITEMS_FRAME;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}

	return statusRV;
}
