#pragma once

#include "BaseO365Grid.h"
#include "BusinessChannel.h"
#include "BusinessSite.h"
#include "CommandInfo.h"
#include "GridTemplateGroups.h"

class FrameSites;

class GridSites : public ModuleO365Grid<BusinessSite>
{
public:
    GridSites(Origin p_Origin);

    void BuildView(const vector<BusinessSite>& p_Sites, const vector<BusinessSite>& p_LoadedMoreSites);
    void BuildView(const O365DataMap<BusinessGroup, BusinessSite>& p_SitesByGroup, const vector<BusinessSite>& p_LoadedMoreSites, bool p_FullPurge);
	void BuildView(const O365DataMap<BusinessChannel, BusinessSite>& p_SitesByChannel, const vector<BusinessSite>& p_LoadedMoreSites, bool p_FullPurge);

	vector<BusinessSite> GetLoadMoreRequestInfo(const std::vector<GridBackendRow*>& p_Rows);

	O365IdsContainer GetSelectedSiteIDs(const RoleDelegationUtil::RBAC_Privilege p_Privilege);
	ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;
    void AddCreatedBusinessObjects(const vector<BusinessSite>& p_Events, bool p_ScrollToNew) override;
    void UpdateBusinessObjects(const vector<BusinessSite>& p_Events, bool p_SetModifiedStatus) override;
    void RemoveBusinessObjects(const vector<BusinessSite>& p_Events) override;

	wstring GetName(const GridBackendRow* p_Row) const override;
		
	RoleDelegationUtil::RBAC_Privilege GetPrivilegeEdit() const override;

	void UpdateSitesLoadMore(const vector<BusinessSite>& p_Sites);

	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

protected:
	virtual void customizeGrid() override;
	void customizeGridPostProcess() override;

    void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart = 0) override;
    BusinessSite getBusinessObject(GridBackendRow* p_Row) const override;
	void InitializeCommands() override;
	bool IsRowForLoadMore(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

protected:
	HACCEL m_hAccelSpecific = nullptr;

private:
    afx_msg void OnShowDriveItems();
    afx_msg void OnShowLists();
    afx_msg void OnShowRoles();
    afx_msg void OnShowUsers();
    afx_msg void OnShowGroups();
    afx_msg void OnShowPermissions();
	afx_msg void OnSiteLoadMore();
    afx_msg void OnUpdateShowDriveItems(CCmdUI* pCmdUI);
    afx_msg void OnUpdateShowLists(CCmdUI* pCmdUI);
    afx_msg void OnUpdateShowRoles(CCmdUI* pCmdUI);
    afx_msg void OnUpdateShowUsers(CCmdUI* pCmdUI);
    afx_msg void OnUpdateShowGroups(CCmdUI* pCmdUI);
    afx_msg void OnUpdateShowPermissions(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSiteLoadMore(CCmdUI* pCmdUI);

    DECLARE_MESSAGE_MAP()

	virtual BOOL PreTranslateMessage(MSG* pMsg) override;

private:
	std::unique_ptr<GridTemplateGroups> m_TemplateGroups;

	struct ChannelMetaColumns
	{
		GridBackendColumn* m_ColumnTeamID = nullptr;
		GridBackendColumn* m_ColumnTeamDisplayName = nullptr;
		GridBackendColumn* m_ColumnChannelID = nullptr;
		GridBackendColumn* m_ColumnChannelDisplayName = nullptr;
	};
	std::unique_ptr<ChannelMetaColumns> m_ChannelMetaColumns;

    GridBackendColumn* m_ColCreatedDateTime;
    GridBackendColumn* m_ColDescription;
    GridBackendColumn* m_ColDisplayName;
    GridBackendColumn* m_ColLastModifiedDateTime;
    GridBackendColumn* m_ColName;
    GridBackendColumn* m_ColRoot;
    GridBackendColumn* m_ColWebUrl;
	GridBackendColumn* m_ColHostName;
    GridBackendColumn* m_ColDataLocationCode;
	GridBackendColumn* m_ColSharepointListID;
	GridBackendColumn* m_ColSharepointListItemID;
	GridBackendColumn* m_ColSharepointListItemUniqueID;
	GridBackendColumn* m_ColSharepointSiteURL;
	GridBackendColumn* m_ColSharepointSiteID;
	GridBackendColumn* m_ColSharepointWebID;

	GridBackendColumn* m_ColDriveName;
	GridBackendColumn* m_ColDriveId;
	GridBackendColumn* m_ColDriveDescription;
	GridBackendColumn* m_ColDriveQuotaState;
	GridBackendColumn* m_ColDriveQuotaUsed;
	GridBackendColumn* m_ColDriveQuotaRemaining;
	GridBackendColumn* m_ColDriveQuotaDeleted;
	GridBackendColumn* m_ColDriveQuotaTotal;
	GridBackendColumn* m_ColDriveCreationTime;
	GridBackendColumn* m_ColDriveLastModifiedDateTime;
	GridBackendColumn* m_ColDriveType;
	GridBackendColumn* m_ColDriveWebUrl;
	GridBackendColumn* m_ColDriveCreatedByUserEmail;
	GridBackendColumn* m_ColDriveCreatedByUserId;
	GridBackendColumn* m_ColDriveCreatedByAppName;
	GridBackendColumn* m_ColDriveCreatedByAppId;
	GridBackendColumn* m_ColDriveCreatedByDeviceName;
	GridBackendColumn* m_ColDriveCreatedByDeviceId;
	GridBackendColumn* m_ColDriveOwnerUserName;
	GridBackendColumn* m_ColDriveOwnerUserId;
	GridBackendColumn* m_ColDriveOwnerUserEmail;
	GridBackendColumn* m_ColDriveOwnerAppName;
	GridBackendColumn* m_ColDriveOwnerAppId;
	GridBackendColumn* m_ColDriveOwnerDeviceName;
	GridBackendColumn* m_ColDriveOwnerDeviceId;

	vector<GridBackendColumn*>	m_MetaColumns;
	bool m_HasLoadMoreAdditionalInfo;
	O365IdsContainer m_MetaIDsMoreLoaded;

    FrameSites*				m_Frame;

private:
	void fillSiteFields(const BusinessSite& p_Site, GridBackendRow* p_Row);

	virtual bool IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const override;

	// only for tenant sites grid //
	struct SiteAndSub;
	using SiteAndSubs = std::list<SiteAndSub>;

	struct SiteAndSub
	{
		SiteAndSub(const BusinessSite& p_Site)
			: m_Site(p_Site)
		{}

		BusinessSite	m_Site;
		SiteAndSubs		m_SubSites;
	};

    void buildSubsitesView(const SiteAndSubs& p_SubSites, GridBackendRow* p_ParentRow, GridUpdater& p_Updater);
    void updateHierarchyCache(GridBackendRow* p_ParentRow, const BusinessSite& p_Site);
	boost::YOpt<wstring> getPathKey(const wstring& p_Path, const map<wstring, SiteAndSub*>& p_PathSites);
	/////////////////////////////

private: // temporary
	bool confirmUnfinishedSpModule();
};
