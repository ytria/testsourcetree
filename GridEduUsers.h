#pragma once

#include "BaseO365Grid.h"
#include "EducationUser.h"

struct EduUserUpdate
{
	vector<GridBackendField> m_RowPk;
	PooledString m_UserId;
	json::value m_NewValues;
};

struct EduUsersChanges
{
	vector<EduUserUpdate> m_Updates;
};

class GridEduUsers : public ModuleO365Grid<EducationUser>
{
public:
	GridEduUsers();
	~GridEduUsers() override;
	void BuildView(const vector<EducationUser>& p_EduUsers, const vector<O365UpdateOperation>& p_Updates, bool p_FullPurge);

	void FillEducationUserFields(GridBackendRow* userRow, const  EducationUser& eduUser);

	EduUsersChanges GetChanges(const vector<GridBackendRow*>& p_Rows);
	EduUsersChanges GetChanges();
	void AddUpdateMod(const RowFieldUpdates<Scope::O365>& p_Mod, EduUsersChanges& p_Changes);

	wstring GetName(const GridBackendRow* p_Row) const override;
	boost::YOpt<PooledString> GetStrValue(GridBackendRow& p_Row, GridBackendColumn* p_Col) const;
	boost::YOpt<YTimeDate> GetDateValue(GridBackendRow& p_Row, GridBackendColumn* p_Col) const;

protected:
	void customizeGrid() override;
	EducationUser getBusinessObject(GridBackendRow*) const override;

	void UpdateMailingAddressField(const wstring& p_FieldName, const wstring& p_NewValue, web::json::value& p_Value);
	void UpdateResidenceAddressField(const wstring& p_FieldName, const wstring& p_NewValue, web::json::value& p_Value);
	void UpdateStudentField(const wstring& p_FieldName, const wstring& p_NewValue, web::json::value& p_Value);
	void UpdateTeacherField(const wstring& p_FieldName, const wstring& p_NewValue, web::json::value& p_Value);


private:
	afx_msg void OnEdit();
	afx_msg void OnUpdateEdit(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()
	void AddPendingChanges(const vector<EducationUser>& p_Users);
	void AddPendingUserChanges(const EducationUser& p_User);
	void AddPendingUserChanges(GridBackendRow& p_Row, const row_pk_t& p_RowPk, const EducationUser& p_User);
	void AddFieldChange(GridBackendRow& p_Row, const row_pk_t& p_RowPk, GridBackendColumn* p_Col, PooledString p_Val);
	void AddFieldChange(GridBackendRow& p_Row, const row_pk_t& p_RowPk, GridBackendColumn* p_Col, const YTimeDate& p_Val);

	void InitializeCommands() override;
	void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart) override;

	// Basic
	GridBackendColumn* m_ColDisplayName;
	GridBackendColumn* m_ColMiddleName;
	GridBackendColumn* m_ColPrimaryRole;
	GridBackendColumn* m_ColExternalSource;

	// Created by
	GridBackendColumn* m_ColCreatedByAppDisplayName;
	GridBackendColumn* m_ColCreatedByAppId;
	GridBackendColumn* m_ColCreatedByAppEmail;
	GridBackendColumn* m_ColCreatedByAppIdentityProvider;
	GridBackendColumn* m_ColCreatedByDeviceDisplayName;
	GridBackendColumn* m_ColCreatedByDeviceId;
	GridBackendColumn* m_ColCreatedByDeviceEmail;
	GridBackendColumn* m_ColCreatedByDeviceIdentityProvider;
	GridBackendColumn* m_ColCreatedByUserDisplayName;
	GridBackendColumn* m_ColCreatedByUserId;
	GridBackendColumn* m_ColCreatedByUserEmail;
	GridBackendColumn* m_ColCreatedByUserIdentityProvider;

	// Mailing address
	GridBackendColumn* m_ColMailingAddressCity;
	GridBackendColumn* m_ColMailingAddressCountryOrRegion;
	GridBackendColumn* m_ColMailingAddressPostalCode;
	GridBackendColumn* m_ColMailingAddressState;
	GridBackendColumn* m_ColMailingAddressStreet;

	// Residence address
	GridBackendColumn* m_ColResidenceAddressCity;
	GridBackendColumn* m_ColResidenceAddressCountryOrRegion;
	GridBackendColumn* m_ColResidenceAddressPostalCode;
	GridBackendColumn* m_ColResidenceAddressState;
	GridBackendColumn* m_ColResidenceAddressStreet;

	// Related contacts
	GridBackendColumn* m_ColRelatedContactId;
	GridBackendColumn* m_ColRelatedContactDisplayName;
	GridBackendColumn* m_ColRelatedContactEmailAddress;
	GridBackendColumn* m_ColRelatedContactMobilePhone;
	GridBackendColumn* m_ColRelatedContactRelationship;
	GridBackendColumn* m_ColRelatedContactAccessContent;

	// Education student
	GridBackendColumn* m_ColStudentBirthDate;
	GridBackendColumn* m_ColStudentExternalId;
	GridBackendColumn* m_ColStudentGender;
	GridBackendColumn* m_ColStudentGrade;
	GridBackendColumn* m_ColStudentGraduationYear;
	GridBackendColumn* m_ColStudentNumber;

	// Education teacher
	GridBackendColumn* m_ColTeacherExternalId;
	GridBackendColumn* m_ColTeacherNumber;

};

