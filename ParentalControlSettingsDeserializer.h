#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ParentalControlSettings.h"

class ParentalControlSettingsDeserializer : public JsonObjectDeserializer, public Encapsulate<ParentalControlSettings>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

