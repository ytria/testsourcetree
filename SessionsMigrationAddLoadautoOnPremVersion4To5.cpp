#include "SessionsMigrationAddLoadautoOnPremVersion4To5.h"
#include "SessionMigrationUtil.h"
#include "SessionsSqlEngine.h"
#include "SQLSafeTransaction.h"
#include "SqlQueryMultiPreparedStatement.h"
#include "ISessionsMigrationVersion.h"

void SessionsMigrationAddLoadAutoOnPremVersion4To5::Build()
{
	AddStep(std::bind(&SessionsMigrationAddLoadAutoOnPremVersion4To5::CreateNewFromOld, this));
	AddStep(std::bind(&SessionsMigrationAddLoadAutoOnPremVersion4To5::AddLoadAutoFlag, this));
}

VersionSourceTarget SessionsMigrationAddLoadAutoOnPremVersion4To5::GetVersionInfo() const
{
	return { 4, 5 };
}

bool SessionsMigrationAddLoadAutoOnPremVersion4To5::CreateNewFromOld()
{
	return SessionMigrationUtil::CopyDatabase(GetVersionInfo());
}

bool SessionsMigrationAddLoadAutoOnPremVersion4To5::AddLoadAutoFlag()
{
	bool success = false;

	SQLSafeTransaction transaction(GetTargetSQLEngine(), [this, &success] {
		const std::vector<wstring> statements =
		{
			LR"(ALTER TABLE Sessions ADD COLUMN AutoLoadOnPrem INTEGER DEFAULT 0;)",
			LR"(ALTER TABLE Sessions ADD COLUMN AskedAutoLoadOnPrem INTEGER DEFAULT 0;)"
		};

		SqlQueryMultiPreparedStatement query(GetTargetSQLEngine(), statements);
		query.Run();

		success = query.IsSuccess();
	}, true);

	return transaction.Run() && success;
}
