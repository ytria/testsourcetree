#include "AddMailboxPermissionRequester.h"

#include "Sapio365Session.h"

AddMailboxPermissionRequester::AddMailboxPermissionRequester(const wstring& p_MailboxOwner, const wstring& p_UserThatGainsRights, const wstring& p_AccessRights, const std::shared_ptr<IRequestLogger>& p_RequestLogger)
	:m_MailboxOwner(p_MailboxOwner),
	m_UserThatGainsRights(p_UserThatGainsRights),
	m_AccessRights(p_AccessRights),
	m_Logger(p_RequestLogger),
	m_Result(std::make_shared<InvokeResultWrapper>())
{
}

TaskWrapper<void> AddMailboxPermissionRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	return YSafeCreateTaskMutable([identity = m_MailboxOwner, user = m_UserThatGainsRights, accessRights = m_AccessRights, result = m_Result, session = p_Session->GetExchangePowershellSession(), logger = m_Logger, taskData = p_TaskData]() {
		if (nullptr != session)
		{
			logger->Log(taskData.GetOriginator());

			wstring script = _YTEXTFORMAT(L"Add-MailboxPermission -Identity %s -User %s -AccessRights %s", identity.c_str(), user.c_str(), accessRights.c_str());
			session->AddScript(script.c_str(), taskData.GetOriginator());

			result->SetResult(session->Invoke(taskData.GetOriginator()));
		}
	});
}

const std::shared_ptr<InvokeResultWrapper>& AddMailboxPermissionRequester::GetResult() const
{
	return m_Result;
}
