#pragma once

#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "YAdvancedHtmlView.h"

class ModuleBase;
class Sapio365Session;
class IDlgSpecialExtAdminJsonGenerator;

class DlgSpecialExtAdminAccess : public ResizableDialog, public YAdvancedHtmlView::IPostedDataTarget, public YAdvancedHtmlView::IPostedURLTarget
{
public:
	DlgSpecialExtAdminAccess(const wstring& p_Tenant, const wstring& p_AppId, const wstring& p_AppClientSecret, const wstring& p_AppDisplayName, CWnd* p_Parent, shared_ptr<Sapio365Session> p_Session, std::unique_ptr<IDlgSpecialExtAdminJsonGenerator> p_JsonGenerator, bool p_EditOnlySecretKey);

	enum { IDD = IDD_DLG_SPECIALEXTADMINACCESS_HTML };

	void SetTitle(const wstring& p_Title);
	void SetAdditionalHeight(int p_Additionalheight);

	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data) override;
	virtual bool OnNewPostedURL(const wstring& url) override;

	const wstring& GetAppId() const;
	const wstring& GetClientSecret() const;
	const wstring& GetTenant() const;
	const wstring& GetDisplayName() const;
	bool GetSaveSecretKey() const;
	bool IsRemoveRequested() const;

	void SetCreatedAppId(const wstring& p_AppId);
	const wstring& GetCreatedAppId() const;

	static const wstring g_URL1;
	static wstring g_URL2;
	static const wstring g_URL3;
	static wstring g_KeyNotShown;

	static const wstring g_Cancel;
	static const wstring g_Ok;
	static const wstring g_ApplicationId;
	static const wstring g_SecretKey;
	static const wstring g_DisplayName;
	static const wstring g_TenantName;
	static const wstring g_KeepSecretKey;

protected:
    virtual BOOL OnInitDialogSpecificResizable() override;

    ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);

	void OnGetConsentForApp(const wstring& p_Tenant, const wstring& p_AppID/*, const wstring& p_RedirectURL*/);

private:
	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;
	shared_ptr<Sapio365Session> m_Session;
	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;

	wstring m_AppId;
	wstring m_ClientSecret;
	wstring m_Tenant;
	wstring m_DisplayName;
	wstring m_Title;
	bool m_SaveSecretKey;
	bool m_EditOnlySecretKey;
	int m_AdditionalHeight;
	bool m_RemoveRequested;
	wstring m_CreatedAppId;

	std::unique_ptr<IDlgSpecialExtAdminJsonGenerator> m_JsonGenerator;
};

