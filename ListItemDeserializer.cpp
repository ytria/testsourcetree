#include "ListItemDeserializer.h"

#include "ContentTypeInfoDeserializer.h"
#include "FieldValueSetDeserializer.h"
#include "IdentitySetDeserializer.h"
#include "JsonSerializeUtil.h"

void ListItemDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        ContentTypeInfoDeserializer ctid;
        if (JsonSerializeUtil::DeserializeAny(ctid, _YTEXT("contentType"), p_Object))
            m_Data.m_ContentType = std::move(ctid.GetData());
    }

    {
        FieldValueSetDeserializer fvsd;
        if (JsonSerializeUtil::DeserializeAny(fvsd, _YTEXT("fields"), p_Object))
            m_Data.m_Fields = std::move(fvsd.GetData());
    }

	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);
    
    {
        IdentitySetDeserializer isd;
        if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("createdBy"), p_Object))
            m_Data.m_CreatedBy = std::move(isd.GetData());
    }

    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.m_CreatedDateTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.m_Description, p_Object);

    {
        IdentitySetDeserializer isd;
        if (JsonSerializeUtil::DeserializeAny(isd, _YTEXT("lastModifiedBy"), p_Object))
            m_Data.m_LastModifiedBy = std::move(isd.GetData());
    }

    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.m_LastModifiedDateTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("webUrl"), m_Data.m_WebUrl, p_Object);
}
