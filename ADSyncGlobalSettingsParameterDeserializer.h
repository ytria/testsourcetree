#pragma once

#include "IPSObjectCollectionDeserializer.h"

class ADSyncGlobalSettingsParameterDeserializer : public IPSObjectCollectionDeserializer
{
public:
	void Deserialize(IPSObjectPropertyReader& p_Reader) override;
	void SetCount(size_t p_Count) override;

	const wstring& GetSourceAnchor() const;

private:
	wstring m_SourceAnchor;
};

