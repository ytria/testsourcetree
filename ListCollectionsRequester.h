#pragma once

#include "IRequester.h"

#include "ValueListDeserializer.h"
#include "Collection.h"
#include "CollectionDeserializer.h"

class SingleRequestResult;

namespace Cosmos
{
    class ListCollectionsRequester : public IRequester
    {
    public:
        ListCollectionsRequester(const wstring& p_DbName);
        TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

        const vector<Cosmos::Collection>& GetData() const;
        const SingleRequestResult& GetResult() const;

        std::shared_ptr<ValueListDeserializer<Cosmos::Collection, Cosmos::CollectionDeserializer>> m_Deserializer;
        std::shared_ptr<SingleRequestResult> m_Result;

    private:
        wstring m_DbName;
    };
}

