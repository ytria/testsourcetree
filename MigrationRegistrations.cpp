#include "MigrationRegistrations.h"
#include "MigrationUtil.h"
#include "SessionsMigrationVersion0To1.h"
#include "SessionsMigrationVersion1To2.h"
#include "SessionsMigrationVersion2To3.h"
#include "SessionsMigrationAddUseDeltaFlagVersion3To4.h"
#include "SessionsSqlEngine.h"
#include "SessionsMigrationAddLoadautoOnPremVersion4To5.h"
#include "SessionsMigrationAddUseOnPremVersion5To6.h"

MigrationRegistrations::MigrationRegistrations()
{
	RegisterMigrations();
}

std::shared_ptr<IMigrationVersion> MigrationRegistrations::GetCurrentVersionMigration() const
{
	return GetVersionMigration(MigrationUtil::GetCurrentMigrationVersion());
}

std::shared_ptr<IMigrationVersion> MigrationRegistrations::GetVersionMigration(int p_Version) const
{
	std::shared_ptr<IMigrationVersion> migration;

	auto itt = std::find_if(m_Migrations.begin(), m_Migrations.end(), [p_Version](const std::shared_ptr<IMigrationVersion>& p_Migration) {
		return p_Migration->GetVersionInfo().m_TargetVersion == p_Version;
	});

	if (itt != m_Migrations.end())
		migration = *itt;

	return migration;
}

void MigrationRegistrations::RegisterMigrations()
{
	m_Migrations.push_back(std::make_unique<SessionsMigrationVersion0To1>());
	m_Migrations.push_back(std::make_unique<SessionsMigrationVersion1To2>());
	m_Migrations.push_back(std::make_unique<SessionsMigrationVersion2To3>());
	m_Migrations.push_back(std::make_unique<SessionsMigrationAddUseDeltaFlagVersion3To4>());
	m_Migrations.push_back(std::make_unique<SessionsMigrationAddLoadAutoOnPremVersion4To5>());
	m_Migrations.push_back(std::make_unique<SessionsMigrationAddUseOnPremVersion5To6>());

	ASSERT(MigrationUtil::GetCurrentMigrationVersion() == m_Migrations.size());
}
