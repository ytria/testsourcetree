#include "FrameSchools.h"

#include "GridUpdater.h"

IMPLEMENT_DYNAMIC(FrameSchools, GridFrameBase)

FrameSchools::FrameSchools(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
	m_CreateRefreshPartial = false;
}

void FrameSchools::BuildView(const vector<EducationSchool>& p_Schools, const vector<O365UpdateOperation>& p_Updates, bool p_FullPurge)
{
	m_Grid.BuildView(p_Schools, p_Updates, p_FullPurge);
}

O365Grid& FrameSchools::GetGrid()
{
	return m_Grid;
}

void FrameSchools::RefreshSpecific(AutomationAction* p_CurrentAction)
{
	CommandInfo info;
	info.SetFrame(this);
	info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	ASSERT(info.GetOrigin() == Origin::Tenant);

	if (HasLastUpdateOperations())
	{
		info.Data() = GetInfoForRefreshAfterUpdate();
		GridUpdater::HandleCreatedModifications(GetGrid(), AccessLastUpdateOperations());
	}

	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Schools, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

bool FrameSchools::HasApplyButton()
{
	return true;
}

void FrameSchools::ApplySpecific(bool p_Selected)
{
	auto grid = dynamic_cast<GridSchools*>(&GetGrid());

	ASSERT(nullptr != grid);
	if (nullptr != grid)
	{
		CommandInfo info;
		if (p_Selected)
		{
			vector<GridBackendRow*> selectedRows(grid->GetSelectedRows().begin(), grid->GetSelectedRows().end());
			info.Data() = grid->GetChanges(selectedRows);
		}
		else
			info.Data() = grid->GetChanges();
		info.SetFrame(this);
		info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
		CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Schools, Command::ModuleTask::ApplyChanges, info, { &GetGrid(), AutomationConstant::val().m_ActionNameSave, GetAutomationActionRecording(), GetAutomationAction() }));
	}
}

void FrameSchools::ApplyAllSpecific()
{
	ApplySpecific(false);
}

void FrameSchools::ApplySelectedSpecific()
{
	ApplySpecific(true);
}

void FrameSchools::revertSelectedImpl()
{
	GetGrid().RevertSelectedRows(O365Grid::RevertFlags::UPDATE_GRID_IF_NEEDED | O365Grid::RevertFlags::ANY_LEVEL_REVERT_CHILDREN);
}

void FrameSchools::createGrid()
{
	m_Grid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

bool FrameSchools::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
	CreateViewGroup(tab);
	CreateRefreshGroup(tab);
	CreateApplyGroup(tab);
	CreateRevertGroup(tab);
	CreateEditGroup(tab, false);

	auto editGroup = findGroup(tab, g_ActionsEditGroup.c_str());
	ASSERT(nullptr != editGroup);
	if (nullptr != editGroup)
	{
		auto ctrl = editGroup->Add(xtpControlButton, ID_SCHOOLSGRID_ADDCLASS);
		GridFrameBase::setImage({ ID_SCHOOLSGRID_ADDCLASS }, IDB_SCHOOLSGRID_ADDCLASS, xtpImageNormal);
		GridFrameBase::setImage({ ID_SCHOOLSGRID_ADDCLASS }, IDB_SCHOOLSGRID_ADDCLASS_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);

		ctrl = editGroup->Add(xtpControlButton, ID_SCHOOLSGRID_REMOVECLASS);
		GridFrameBase::setImage({ ID_SCHOOLSGRID_REMOVECLASS }, IDB_SCHOOLSGRID_REMOVECLASS, xtpImageNormal);
		GridFrameBase::setImage({ ID_SCHOOLSGRID_REMOVECLASS }, IDB_SCHOOLSGRID_REMOVECLASS_16X16, xtpImageNormal);
		setGridControlTooltip(ctrl);
	}

	CXTPRibbonGroup* dataGroup = tab.AddGroup(g_ActionsLinkSchools.c_str());
	ASSERT(nullptr != dataGroup);
	if (nullptr != dataGroup)
	{
		setGroupImage(*dataGroup, 0);

		addNewModuleCommandControl(*dataGroup, ID_SCHOOLSGRID_SHOWCLASS_MEMBERS, { ID_SCHOOLSGRID_SHOWCLASS_MEMBERS_FRAME, ID_SCHOOLSGRID_SHOWCLASS_MEMBERS_NEWFRAME }, { IDB_SCHOOLSGRID_SHOW_CLASS_MEMBERS, IDB_SCHOOLSGRID_SHOW_CLASS_MEMBERS_16X16 });
	}

	return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameSchools::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	bool newFrame = p_Action && p_Action->IsParamTrue(g_NewFrame);

	if (IsActionSelectedShowClassMembers(p_Action))
	{
		p_CommandID = newFrame ? ID_SCHOOLSGRID_SHOWCLASS_MEMBERS_NEWFRAME : ID_SCHOOLSGRID_SHOWCLASS_MEMBERS_FRAME;
		Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
	}

	return statusRV;
}

vector<EducationSchool> FrameSchools::GetInfoForRefreshAfterUpdate()
{
	vector<EducationSchool> schools;

	GridSchools* grid = dynamic_cast<GridSchools*>(&GetGrid());
	ASSERT(nullptr != grid);
	if (nullptr != grid)
	{
		for (const auto& update : GetLastUpdateOperations())
		{
			const auto& pk = update.GetPrimaryKey();
			auto row = grid->GetRowIndex().GetRow(pk, false, false);
			ASSERT(nullptr != row);
			if (nullptr != row)
			{
				/*if (GridUtil::isBusinessType<EducationSchool>(row))
				{

				}
				else */if (GridUtil::isBusinessType<EducationClass>(row))
				{
					EducationSchool school;
					school.m_Id = row->GetField(grid->m_ColSchoolId).GetValueStr();

					if (std::find(schools.begin(), schools.end(), school) == schools.end())
						schools.push_back(school);
				}
			}
		}
	}

	return schools;
}
