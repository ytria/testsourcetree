#include "CreateDeploymentRequester.h"

#include "AzureSession.h"
#include "Sapio365Session.h"
#include "SingleRequestResult.h"
#include "BasicHttpRequestLogger.h"

Azure::CreateDeploymentRequester::CreateDeploymentRequester(PooledString p_SubscriptionId, PooledString p_ResourceGroup, PooledString p_DeploymentName)
	:m_SubscriptionId(p_SubscriptionId),
	m_ResourceGroup(p_ResourceGroup),
	m_DeploymentName(p_DeploymentName)
{
}

TaskWrapper<void> Azure::CreateDeploymentRequester::Send(shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Deserializer = std::make_shared<Azure::DeploymentDeserializer>();
	m_Result = std::make_shared<SingleRequestResult>();

	web::uri_builder uri(_YTEXT("subscriptions"));
	uri.append_path(m_SubscriptionId);
	uri.append_path(_YTEXT("resourceGroups"));
	uri.append_path(m_ResourceGroup);
	uri.append_path(_YTEXT("providers"));
	uri.append_path(_YTEXT("Microsoft.Resources"));
	uri.append_path(_YTEXT("deployments"));
	uri.append_path(m_DeploymentName);
	uri.append_query(AzureSession::g_ApiVersionKey, _YTEXT("2018-05-01"));

	static const wstring body = _YTEXT(R"({"properties":{"mode":"Incremental","template":{"$schema":"http://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#","contentVersion":"1.0.0.0","parameters":{"dbaccountname":{"type":"string","defaultValue":"[concat('ytria-sapio365-', uniqueString(utcNow()))]"},"location":{"type":"string","defaultValue":"[resourceGroup().location]"}},"resources":[{"apiVersion":"2018-02-01","name":"pid-44d69070-1fea-5d25-a328-35380a9d6614","type":"Microsoft.Resources/deployments","properties":{"mode":"Incremental","template":{"$schema":"https://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#","contentVersion":"1.0.0.0","resources":[]}}},{"name":"[parameters('dbaccountname')]","type":"Microsoft.DocumentDB/databaseAccounts","apiVersion":"2015-04-08","location":"[parameters('location')]","kind":"GlobalDocumentDB","properties":{"databaseAccountOfferType":"Standard","consistencyPolicy":{"defaultConsistencyLevel":"Strong"},"locations":[{"locationName":"[parameters('location')]","failoverPriority":0}]}}],"outputs":{"cosmosDBAccountName":{"type":"string","value":"[parameters('dbaccountname')]"}}},"debugSetting":{"detailLevel":"requestContent, responseContent"}}})");
	WebPayloadJSON payload(web::json::value::parse(body));

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
	return p_Session->GetAzureSession()->Put(uri.to_uri(), httpLogger, payload, m_Result, m_Deserializer, p_TaskData);
}
