#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "TeamGuestSettings.h"

class TeamGuestSettingsDeserializer : public JsonObjectDeserializer, public Encapsulate<TeamGuestSettings>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

