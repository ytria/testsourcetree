#pragma once

#include "IRequester.h"
#include "SpUtils.h"

#include "RoleDefinition.h"
#include "RoleDefinitionDeserializer.h"

template <class T, class U>
class ValueListDeserializer;

class SingleRequestResult;

namespace Sp
{
    class RoleDefinition;
    class RoleDefinitionDeserializer;

    class RoleDefinitionBindingsRequester : public IRequester
    {
    public:
        RoleDefinitionBindingsRequester(PooledString p_SiteUrl, PooledString p_PrincipalId);
        TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

        const vector<Sp::RoleDefinition>& GetData() const;
        const SingleRequestResult& GetResult() const;

    private:
        PooledString m_SiteUrl;
        PooledString m_PrincipalId;
        std::shared_ptr<ValueListDeserializer<Sp::RoleDefinition, Sp::RoleDefinitionDeserializer>> m_Deserializer;
        std::shared_ptr<SingleRequestResult> m_Result;
    };
}
