#pragma once
#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessContactFolder.h"

class ContactFolderDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessContactFolder>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

