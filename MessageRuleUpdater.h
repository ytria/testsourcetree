#pragma once

#include "IRequester.h"
#include "BusinessMessageRule.h"

class SingleRequestResult;

class MessageRuleUpdater : public IRequester
{
public:
	MessageRuleUpdater(const PooledString& p_UserId, const BusinessMessageRule& p_Rule);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override final;

	const PooledString& GetUserId() const;
	const BusinessMessageRule& GetRule() const;
	const SingleRequestResult& GetResult() const;

protected:
	MessageRuleUpdater(const PooledString& p_UserId, const BusinessMessageRule& p_Rule, bool p_DeleteRule);

private:
	void toJson(web::json::value& p_Json, const wstring& p_PropertyName, const wstring& p_SubPropertyName, const boost::YOpt<PooledString>& p_Val);
	void toJson(web::json::value& p_Json, const wstring& p_PropertyName, const wstring& p_SubPropertyName, const boost::YOpt<bool>& p_Val);
	void toJson(web::json::value& p_Json, const wstring& p_PropertyName, const wstring& p_SubPropertyName, const boost::YOpt<int32_t>& p_Val);
	void toJson(web::json::value& p_Json, const wstring& p_PropertyName, const wstring& p_SubPropertyName, const boost::YOpt<vector<PooledString>>& p_Val);

private:
	PooledString m_UserId;
	BusinessMessageRule m_Rule;
	bool m_DeleteRule;
	std::shared_ptr<SingleRequestResult> m_Result;
};

class MessageRuleDeleter : public MessageRuleUpdater
{
public:
	MessageRuleDeleter(const PooledString& p_UserId, const BusinessMessageRule& p_Rule)
		: MessageRuleUpdater(p_UserId, p_Rule, true)
	{}
};