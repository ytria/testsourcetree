#include "TeamGuestSettingsDeserializer.h"

#include "JsonSerializeUtil.h"

void TeamGuestSettingsDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowCreateUpdateChannels"), m_Data.AllowCreateUpdateChannels, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowDeleteChannels"), m_Data.AllowDeleteChannels, p_Object);
}
