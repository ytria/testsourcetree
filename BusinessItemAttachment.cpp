#include "BusinessItemAttachment.h"


BusinessItemAttachment::BusinessItemAttachment()
{
}

BusinessItemAttachment::BusinessItemAttachment(const ItemAttachment& p_ItemAttachment)
{
    m_ContentType = p_ItemAttachment.ContentType;
    m_Id = p_ItemAttachment.Id ? *p_ItemAttachment.Id : _YTEXT("");
    m_IsInline = p_ItemAttachment.IsInline;
    m_LastModifiedDateTime = p_ItemAttachment.LastModifiedDateTime;
    m_Name = p_ItemAttachment.Name;
    m_Size = p_ItemAttachment.Size;
    m_Item = p_ItemAttachment.Item;
}

const boost::YOpt<AnyItemAttachment>& BusinessItemAttachment::GetItem() const
{
    return m_Item;
}

void BusinessItemAttachment::SetItem(const boost::YOpt<AnyItemAttachment>& p_Val)
{
    m_Item = p_Val;
}
