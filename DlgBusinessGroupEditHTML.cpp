#include "DlgBusinessGroupEditHTML.h"

#include "AutomationNames.h"
#include "GraphCache.h"
#include "MsGraphFieldNames.h"
#include "YtriaFieldsConstants.h"

DlgBusinessGroupEditHTML::DlgBusinessGroupEditHTML(
	vector<BusinessGroup>& p_BusinessGroups, 
	const std::set<wstring>& p_ListFieldErrors,
	boost::YOpt<Organization> p_Org, 
	const GraphCache& p_GraphCache, 
	DlgFormsHTML::Action p_Action, CWnd* p_Parent)
	: DlgFormsHTML(p_Action, p_Parent, p_Action == DlgFormsHTML::Action::CREATE ? g_ActionNameCreateGroup : g_ActionNameSelectedEditGroup)
	, m_BusinessGroups(p_BusinessGroups)
	, m_ListFieldErrors(p_ListFieldErrors)
	, m_Org(p_Org)
	, m_GraphCache(p_GraphCache)
{
	ASSERT(!m_BusinessGroups.empty());
}

DlgBusinessGroupEditHTML::~DlgBusinessGroupEditHTML()
{
}

void DlgBusinessGroupEditHTML::generateJSONScriptData()
{
	initMain(_YTEXT("DlgBusinessGroupEditHTML"), false, true, true);

	static const ComboEditor::LabelsAndValues g_GroupTypeLabelsAndValuesForCreation
	{
		{ BusinessGroup::g_Office365Group, BusinessGroup::g_Office365Group },
		{ BusinessGroup::g_SecurityGroup, BusinessGroup::g_SecurityGroup },
	};

	static const ComboEditor::LabelsAndValues g_GroupTypeLabelsAndValues
	{
		{ BusinessGroup::g_Office365Group, BusinessGroup::g_Office365Group },
		{ BusinessGroup::g_DistributionList, BusinessGroup::g_DistributionList },
		{ BusinessGroup::g_SecurityGroup, BusinessGroup::g_SecurityGroup },
		{ BusinessGroup::g_MailEnabledSecurityGroup, BusinessGroup::g_MailEnabledSecurityGroup },
	};

	const auto groupTypeFieldName = _YTEXT("type");

	if (isCreateDialog() || isEditCreateDialog() || isPrefilledCreateDialog())
	{
		if (isPrefilledCreateDialog()) // FIXME: icon and color ?
			getGenerator().addIconTextField(_YTEXT("prefilledCreateInfo"), _T("Reminder: Some fields have been pre-filled with the properties of the currently selected groups."), _YTEXT("fas fa-info-circle"), _YTEXT("#e4f0fb"), _YTEXT("#5882bc"));

		// If you add/remove an editable property here, don't forget to update GridTemplateGroups::updateRow as well,
		// otherwise new editable properties won't show up in the grid.

		const UINT directEditFlag = (isCreateDialog() || isPrefilledCreateDialog()) ? EditorFlags::DIRECT_EDIT : 0;

		addComboEditor(&BusinessGroup::GetCombinedGroupType, &BusinessGroup::SetCombinedGroupType, groupTypeFieldName, wstring(YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_1, _YLOC("Group Type")).c_str()) + _YTEXT(" *"), EditorFlags::REQUIRED | directEditFlag, g_GroupTypeLabelsAndValuesForCreation, {});
		addStringEditor(&BusinessGroup::GetDisplayName, &BusinessGroup::SetDisplayName, _YTEXT("groupDisplayName"), wstring(YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_2, _YLOC("Group Display Name")).c_str()) + _YTEXT(" *"), EditorFlags::REQUIRED | directEditFlag, {});

		{
			// Display the domain as read-only, force it to default domain as "Office 365" type only accept this one.

			// WARNING
			// Graph API doesn't allow domain setting, as the o365 portal.
			// But unlike o365 portal, the domain used is not the organization's default: it's the organization's initial.

			wstring defaultDomain;
			//map<wstring, wstring> domains;
			ASSERT(m_Org);
			if (m_Org)
			{
				//defaultDomain = m_Org->GetDefaultDomain();
				defaultDomain = m_Org->GetInitialDomain();
				//for (const auto& dom : m_Org->GetDomains())
				//	domains[dom] = dom;
			}

			//addUserNameEditor(&BusinessGroup::GetMail, &BusinessGroup::SetMailWithoutDomain, &BusinessGroup::SetMailDomain, _YTEXT("email-name"), _YTEXT("email-domain"), _YTEXT("Mail *"), EditorFlags::REQUIRED | directEditFlag, /*domains*/{ { defaultDomain, defaultDomain } }, {});
			addUserNameEditor(&BusinessGroup::GetMail, &BusinessGroup::SetMailWithoutDomain, &BusinessGroup::SetMailDomain, _YTEXT("email-name"), _YTEXT("email-domain"), wstring(YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_4, _YLOC("Email")).c_str()) + _YTEXT(" *"), EditorFlags::REQUIRED | directEditFlag, /*domains*/{ { defaultDomain, defaultDomain } }, {});
			getGenerator().addEvent(groupTypeFieldName, EventTarget({ _YTEXT("email-name"), EventTarget::g_HideIfNotEqual, BusinessGroup::g_Office365Group }));
			getGenerator().addEvent(groupTypeFieldName, EventTarget({ _YTEXT("email-domain"), EventTarget::g_HideIfNotEqual, BusinessGroup::g_Office365Group }));
		}

		// Disabled for now: Dynamic Membership edition requires Rules (Bug #180711.SB.00873E)
		/*addBoolToggleEditor(&BusinessGroup::IsDynamicMembership, &BusinessGroup::SetDynamicMembership, _YTEXT("dynamicMembership"), _TLoc("Dynamic"), directEditFlag, {});
		addEvent(groupTypeFieldName, EventTarget({ _YTEXT("dynamicMembership"), EventTarget::g_HideIfEqual, BusinessGroup::g_DistributionList }));
		addEvent(groupTypeFieldName, EventTarget({ _YTEXT("dynamicMembership"), EventTarget::g_HideIfEqual, BusinessGroup::g_MailEnabledSecurityGroup }));*/

		addStringEditor(&BusinessGroup::GetDescription, &BusinessGroup::SetDescription, _YTEXT("description"), YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_5, _YLOC("Description")).c_str(), 0, {});

		addComboEditor(&BusinessGroup::GetVisibility, &BusinessGroup::SetVisibility, _YTEXT("privacy"), wstring(YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_6, _YLOC("Privacy")).c_str()) + _YTEXT(" *"), EditorFlags::REQUIRED | directEditFlag,
			{
				{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_7, _YLOC("Public - Anyone can see group content")).c_str(), _YTEXT("Public") },
				{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_8, _YLOC("Private - Only members can see group content")).c_str(), _YTEXT("Private") },
				{ _T("Hidden Membership - Only members can see group members and content"), _YTEXT("HiddenMembership") }
			},
			{}
		);
		getGenerator().addEvent(groupTypeFieldName, EventTarget({ _YTEXT("privacy"), EventTarget::g_HideIfEqual, BusinessGroup::g_SecurityGroup }));

		{
			wstring owner;
			int32_t noChange = 0;
			if (isCreateDialog())
			{
				// We instantiate the creation dialog with only one empty group whose owner is the current user.
				if (1 == m_BusinessGroups.size()
					&& 1 == m_BusinessGroups.front().GetOwnerUsers().size()
					/*&& m_BusinessGroups.front().GetOwnerGroups().empty()*/)
				{
					auto& ownerId = m_BusinessGroups.front().GetOwnerUsers().front();
					const auto cachedOwner = m_GraphCache.GetUserInCache(ownerId, false);
					ASSERT(cachedOwner.GetID() == ownerId && !cachedOwner.GetUserPrincipalName().IsEmpty());
					if (cachedOwner.GetID() == ownerId)
						owner = cachedOwner.GetUserPrincipalName();
				}
			}
			else
			{
				{
					ASSERT(!m_BusinessGroups.empty());
					bool allTheSame = true;
					const auto& ownerId = !m_BusinessGroups.front().GetOwnerUsers().empty() ? m_BusinessGroups.front().GetOwnerUsers().front() : PooledString();
					for (const auto& group : m_BusinessGroups)
					{
						const auto otherVal = !group.GetOwnerUsers().empty() ? group.GetOwnerUsers().front() : PooledString();
						if (ownerId != otherVal)
						{
							allTheSame = false;
							break;
						}
					}

					if (allTheSame)
					{
						if (!ownerId.IsEmpty())
						{
							const auto cachedOwner = m_GraphCache.GetUserInCache(ownerId, false);
							ASSERT(cachedOwner.GetID() == ownerId && !cachedOwner.GetUserPrincipalName().IsEmpty());
							if (cachedOwner.GetID() == ownerId)
								owner = cachedOwner.GetUserPrincipalName();
						}
						else
						{
							owner = PooledString();
						}
					}
					else
						noChange = EditorFlags::NOCHANGE;
				}
			}

			ASSERT(0 != noChange || !owner.empty());

			getGenerator().Add(StringEditor(_YTEXT("owner"), YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_9, _YLOC("Owner")).c_str(), /*EditorFlags::REQUIRED | */ EditorFlags::READONLY | noChange, owner));
			getGenerator().addEvent(groupTypeFieldName, EventTarget({ _YTEXT("owner"), EventTarget::g_HideIfEqual, BusinessGroup::g_SecurityGroup }));
		}
	}
	else
	{
		ASSERT(isEditDialog() || isReadDialog());

		if (std::any_of(m_BusinessGroups.begin(), m_BusinessGroups.end(), [](auto& bg) {return !bg.IsMoreLoaded(); }))
			getGenerator().addIconTextField(_YTEXT("bewareLoadInfo"), _T("Reminder: Some fields may only be editable once you click the 'Load Info' button in the grid."), _YTEXT("fas fa-exclamation-triangle"), _YTEXT("#ff1de"), _YTEXT("#de7e00"));

		getGenerator().addCategory(YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_10, _YLOC("Basic Information")).c_str(), CategoryFlags::EXPAND_BY_DEFAULT);
		{
			addComboEditor(&BusinessGroup::GetCombinedGroupType, &BusinessGroup::SetCombinedGroupType, groupTypeFieldName, YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_11, _YLOC("Group Type")).c_str(), EditorFlags::READONLY, g_GroupTypeLabelsAndValues, {});
			addIsTeamEditor();
			// Disabled for now: Dynamic Membership edition requires Rules (Bug #180711.SB.00873E)
			/*addBoolToggleEditor(&BusinessGroup::IsDynamicMembership, &BusinessGroup::SetDynamicMembership, _YTEXT("dynamicMembership"), _TLoc("Dynamic"), 0, {});
			addEvent(groupTypeFieldName, EventTarget({ _YTEXT("dynamicMembership"), EventTarget::g_HideIfEqual, BusinessGroup::g_DistributionList }));
			addEvent(groupTypeFieldName, EventTarget({ _YTEXT("dynamicMembership"), EventTarget::g_HideIfEqual, BusinessGroup::g_MailEnabledSecurityGroup }));*/

			addStringEditor(&BusinessGroup::GetDisplayName
				, &BusinessGroup::SetDisplayName
				, _YTEXT("groupDisplayName")
				, wstring(YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_12, _YLOC("Group Display Name")).c_str()) + _YTEXT(" *")
				, addErrorFlagIfNecessary(EditorFlags::REQUIRED, _YTEXT(O365_GROUP_DISPLAYNAME))
				, {});
			addStringEditor(&BusinessGroup::GetDescription
				, &BusinessGroup::SetDescription
				, _YTEXT("description")
				, YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_13, _YLOC("Description")).c_str()
				, addErrorFlagIfNecessary(0, _YTEXT(O365_GROUP_DESCRIPTION))
				, {});
			//addStringEditor(&BusinessGroup::GetClassification, &BusinessGroup::SetClassification, _YTEXT("classification"), _TLoc("Classification"), EditorFlags::READONLY | EditorFlags::RESTRICT_NOT_CREATED, {});

			addComboEditor(&BusinessGroup::GetVisibility
				, &BusinessGroup::SetVisibility
				, _YTEXT("privacy")
				, YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_14, _YLOC("Privacy")).c_str()
				, addErrorFlagIfNecessary(EditorFlags::REQUIRED, _YTEXT(O365_GROUP_VISIBILITY))
				, {
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_15, _YLOC("Public - Anyone can see group content")).c_str(), _YTEXT("Public") },
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_16, _YLOC("Private - Only members can see group content")).c_str(), _YTEXT("Private") },
					{ _T("Hidden Membership - Only members can see group members and content"), _YTEXT("HiddenMembership") }
				}
				, {}
			);

			addStringEditor(&BusinessGroup::GetPreferredDataLocation
				, &BusinessGroup::SetPreferredDataLocation
				, _YTEXT("preferredDataLocation")
				, YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_25, _YLOC("Preferred Data Location")).c_str()
				, addErrorFlagIfNecessary(0, _YTEXT(O365_GROUP_PREFERREDDATALOCATION))
				, {}
			);

			addComboEditor(
				&BusinessGroup::GetMembershipRuleProcessingStateForEdit,
				&BusinessGroup::SetMembershipRuleProcessingState,
				_YTEXT("membershipRuleProcessingState"),
				YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_26, _YLOC("Membership Rule processing State")).c_str(),
				addErrorFlagIfNecessary(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::REQUIRED, _YTEXT(O365_GROUP_MEMBERSHIPRULEPROCESSINGSTATE)),
				{
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_27, _YLOC("-- none --")).c_str(),	GridBackendUtil::g_NoValueString },
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_28, _YLOC("On")).c_str(),		_YTEXT("On") },
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_29, _YLOC("Paused")).c_str(),	_YTEXT("Paused") },
				},
				{}
			);

			addStringEditor(
				&BusinessGroup::GetMembershipRule,
				&BusinessGroup::SetMembershipRule,
				_YTEXT("membershipRule"),
				YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_30, _YLOC("Membership Rule")).c_str(),
				addErrorFlagIfNecessary(EditorFlags::RESTRICT_LOAD_MORE, _YTEXT(O365_GROUP_MEMBERSHIPRULE)),
				{});

			addComboEditor(
				&BusinessGroup::GetThemeForEdit,
				&BusinessGroup::SetTheme,
				_YTEXT("theme"),
				YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_31, _YLOC("Theme")).c_str(),
				addErrorFlagIfNecessary(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::REQUIRED, _YTEXT(O365_GROUP_THEME)),
				{
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_32, _YLOC("-- none --")).c_str(),	GridBackendUtil::g_NoValueString },
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_33, _YLOC("Blue")).c_str(),	_YTEXT("Blue") },
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_34, _YLOC("Green")).c_str(),	_YTEXT("Green") },
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_35, _YLOC("Orange")).c_str(),	_YTEXT("Orange") },
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_36, _YLOC("Pink")).c_str(),	_YTEXT("Pink") },
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_37, _YLOC("Purple")).c_str(),	_YTEXT("Purple") },
					{ YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_38, _YLOC("Red")).c_str(),	_YTEXT("Red") },
				},
				{ BusinessGroup::g_Office365Group }
			);

			//addBoolEditor(&BusinessGroup::IsSecurityEnabled, &BusinessGroup::SetIsSecurityEnabled, _YTEXT("securityEnabled"), _TLoc("Security Enabled"), EditorFlags::READONLY | EditorFlags::RESTRICT_NOT_CREATED, {});
		}

		addCategory(YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_17, _YLOC("Mail Config")).c_str(), false, { BusinessGroup::g_Office365Group, BusinessGroup::g_DistributionList, BusinessGroup::g_MailEnabledSecurityGroup });
		{
			//addBoolEditor(&BusinessGroup::IsMailEnabled, &BusinessGroup::SetIsMailEnabled, _YTEXT("mailEnabled"), _TLoc("Mail Enabled"), EditorFlags::READONLY | EditorFlags::RESTRICT_NOT_CREATED, {});
			//addStringEditor(&BusinessGroup::GetMail, &BusinessGroup::SetMail, _YTEXT("mail"), _TLoc("Mail"), EditorFlags::READONLY | EditorFlags::RESTRICT_NOT_CREATED, {});

			addStringEditor(&BusinessGroup::GetMailNickname
				, &BusinessGroup::SetMailNickname
				, _YTEXT("mailNickname")
				, wstring(YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_18, _YLOC("Mail Nickname")).c_str()) + _YTEXT(" *")
				, addErrorFlagIfNecessary(EditorFlags::REQUIRED, _YTEXT(O365_GROUP_MAILNICKNAME))
				, {}
			);

			getGenerator().addEvent(groupTypeFieldName, EventTarget({ _YTEXT("mailNickname"), EventTarget::g_HideIfEqual, BusinessGroup::g_SecurityGroup }));

			//addBoolEditor(&BusinessGroup::IsSubscribedByMail, &BusinessGroup::SetIsSubscribedByMail, _YTEXT("isSubscribedBymail"), _TLoc("Is Subscribed By Mail"), EditorFlags::READONLY | EditorFlags::RESTRICT_NOT_CREATED, {});

			addBoolEditor(
				&BusinessGroup::IsAllowExternalSenders,
				&BusinessGroup::SetIsAllowExternalSenders,
				_YTEXT(O365_GROUP_ALLOWEXTERNALSENDERS),
				YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_19, _YLOC("Allow External Senders")).c_str(),
				addErrorFlagIfNecessary(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED, _YTEXT(O365_GROUP_ALLOWEXTERNALSENDERS)),
				{ BusinessGroup::g_Office365Group });

			addBoolEditor(
				&BusinessGroup::IsAutoSubscribeNewMembers,
				&BusinessGroup::SetIsAutoSubscribeNewMembers,
				_YTEXT(O365_GROUP_AUTOSUBSCRIBENEWMEMBERS),
				YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_20, _YLOC("Auto Subscribe New Members")).c_str(),
				addErrorFlagIfNecessary(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED, _YTEXT(O365_GROUP_AUTOSUBSCRIBENEWMEMBERS)),
				{ BusinessGroup::g_Office365Group });

			addBoolEditor(
				&BusinessGroup::GetHideFromOutlookClients,
				&BusinessGroup::SetHideFromOutlookClients,
				_YTEXT(O365_GROUP_HIDEFROMOUTLOOKCLIENTS),
				_T("Hide from Outlook clients"),
				addErrorFlagIfNecessary(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED, _YTEXT(O365_GROUP_HIDEFROMOUTLOOKCLIENTS)),
				{ BusinessGroup::g_Office365Group });

			addBoolEditor(
				&BusinessGroup::GetHideFromAddressLists,
				&BusinessGroup::SetHideFromAddressLists,
				_YTEXT(O365_GROUP_HIDEFROMADDRESSLISTS),
				_T("Hide from address lists"),
				addErrorFlagIfNecessary(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED, _YTEXT(O365_GROUP_HIDEFROMADDRESSLISTS)),
				{ BusinessGroup::g_Office365Group });
		}

		/*getGenerator().addCategory(_TLoc("On-Premises Information"), NOFLAG);
		{
			addBoolEditor(&BusinessGroup::IsOnPremisesSyncEnabled, &BusinessGroup::SetOnPremisesSyncEnabled, _YTEXT("syncEnabled"), _TLoc("Sync Enabled"), EditorFlags::READONLY | EditorFlags::RESTRICT_NOT_CREATED, {});
			addStringEditor(&BusinessGroup::GetOnPremisesSecurityIdentifier, &BusinessGroup::SetOnPremisesSecurityIdentifier, _YTEXT("securityId"), _TLoc("Security Identifier"), EditorFlags::READONLY | EditorFlags::RESTRICT_NOT_CREATED, {});
		}*/

		addCategory(YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_22, _YLOC("Other")).c_str(), false, { BusinessGroup::g_Office365Group });
        {
            const auto groupUnifiedGuestSettingTemplateName = _YTEXT("activateGroupUnifiedGuestSetting");
            const auto allowToAddGuestsName = _YTEXT("allowToAddGuests");
			getGenerator().addEvent(groupUnifiedGuestSettingTemplateName, EventTarget({ allowToAddGuestsName, EventTarget::g_DisableIfEqual, UsefulStrings::g_ToggleValueFalse }));

			addBoolToggleEditor(&BusinessGroup::GetGroupUnifiedGuestSettingTemplateActivated
				, &BusinessGroup::SetGroupUnifiedGuestSettingTemplateActivated
				, groupUnifiedGuestSettingTemplateName
				, YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_23, _YLOC("Group.Unified.Guest Setting Activated")).c_str()
				, addErrorFlagIfNecessary(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED, _YTEXT(YTRIA_GROUP_GROUPUNIFIEDGUESTSETTINGACTIVATED))
				, { BusinessGroup::g_Office365Group });
            addBoolToggleEditor(&BusinessGroup::GetSettingAllowToAddGuests
				, &BusinessGroup::SetSettingAllowToAddGuests
				, allowToAddGuestsName
				, YtriaTranslate::Do(DlgBusinessGroupEditHTML_generateJSONScriptData_24, _YLOC("Group.Unified - Allow To Add Guests")).c_str()
				, addErrorFlagIfNecessary(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED, _YTEXT(YTRIA_GROUP_SETTINGALLOWTOADDGUESTS))
				, { BusinessGroup::g_Office365Group });
			getGenerator().addEvent(groupTypeFieldName, EventTarget({ groupUnifiedGuestSettingTemplateName, EventTarget::g_HideIfNotEqual, BusinessGroup::g_Office365Group }));
			getGenerator().addEvent(groupTypeFieldName, EventTarget({ allowToAddGuestsName, EventTarget::g_HideIfNotEqual, BusinessGroup::g_Office365Group }));
        }

		{
			int flags = EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED;

			getGenerator().addCategory(_T("Team Archiving"), CategoryFlags::NOFLAG);
			{
				const PooledString& isArchived = _YTEXT("isArchived");
				addBoolToggleEditor(&BusinessGroup::IsTeamArchived
					, &BusinessGroup::SetTeamArchived
					, isArchived
					, _T("Archived")
					, addErrorFlagIfNecessary(flags, isArchived),
					{ BusinessGroup::g_Office365Group });

				const bool hasOneUnarchivedTeam = std::any_of(m_BusinessGroups.begin(), m_BusinessGroups.end(), [](const auto& g)
					{
						return (g.GetIsTeam()/* || g.GetEditIsTeam() && *g.GetEditIsTeam()*/) && g.GetTeam() && (!g.GetTeam()->IsArchived || !*g.GetTeam()->IsArchived);// (!g.IsTeamArchived() || !*g.IsTeamArchived());
					});
				
				if (hasOneUnarchivedTeam)
				{
					const PooledString& setSpoSiteReadOnlyForMembers = _YTEXT("setSpoSiteReadOnlyForMembers");
					addBoolToggleEditor(&BusinessGroup::GetSpoSiteReadOnlyForMembers
						, &BusinessGroup::SetSpoSiteReadOnlyForMembers
						, setSpoSiteReadOnlyForMembers
						, _T("Make the SharePoint Online site read-only for team members")
						, addErrorFlagIfNecessary(flags, setSpoSiteReadOnlyForMembers),
						{ BusinessGroup::g_Office365Group });

					getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ setSpoSiteReadOnlyForMembers, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
					getGenerator().addEvent(isArchived, EventTarget({ setSpoSiteReadOnlyForMembers, EventTarget::g_DisableIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				}

				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ isArchived, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
			}

			getGenerator().addCategory(_T("Team Member Settings"), CategoryFlags::NOFLAG);
			{
				const PooledString& allowCreateUpdateChannels = _YTEXT("teamMemberSettingsAllowCreateUpdateChannels");
				addBoolToggleEditor(&TeamMemberSettings::GetAllowCreateUpdateChannels
					, &TeamMemberSettings::SetAllowCreateUpdateChannels
					, allowCreateUpdateChannels
					, _T("Allow Create Update Channels")
					, addErrorFlagIfNecessary(flags, allowCreateUpdateChannels));

				const PooledString& allowCreatePrivateChannels = _YTEXT("teamMemberSettingsAllowCreatePrivateChannels");
				addBoolToggleEditor(&TeamMemberSettings::GetAllowCreatePrivateChannels
					, &TeamMemberSettings::SetAllowCreatePrivateChannels
					, allowCreatePrivateChannels
					, _T("Allow Create Private Channels")
					, addErrorFlagIfNecessary(flags, allowCreatePrivateChannels));

				const PooledString& allowDeleteChannels = _YTEXT("teamMemberSettingsAllowDeleteChannels");
				addBoolToggleEditor(&TeamMemberSettings::GetAllowDeleteChannels
					, &TeamMemberSettings::SetAllowDeleteChannels
					, allowDeleteChannels
					, _T("Allow Delete Channels")
					, addErrorFlagIfNecessary(flags, allowDeleteChannels));

				const PooledString& allowAddRemoveApps = _YTEXT("teamMemberSettingsAllowAddRemoveApps");
				addBoolToggleEditor(&TeamMemberSettings::GetAllowAddRemoveApps
					, &TeamMemberSettings::SetAllowAddRemoveApps
					, allowAddRemoveApps
					, _T("Allow Add Remove Apps")
					, addErrorFlagIfNecessary(flags, allowAddRemoveApps));

				const wstring allowCreateUpdateRemoveTabs = _YTEXT("teamMemberSettingsAllowCreateUpdateRemoveTabs");
				addBoolToggleEditor(&TeamMemberSettings::GetAllowCreateUpdateRemoveTabs
					, &TeamMemberSettings::SetAllowCreateUpdateRemoveTabs
					, allowCreateUpdateRemoveTabs
					, _T("Allow Create Update Remove Tabs")
					, addErrorFlagIfNecessary(flags, allowCreateUpdateRemoveTabs));

				const wstring allowCreateUpdateRemoveConnectors = _YTEXT("teamMemberSettingsAllowCreateUpdateRemoveConnectors");
				addBoolToggleEditor(&TeamMemberSettings::GetAllowCreateUpdateRemoveConnectors
					, &TeamMemberSettings::SetAllowCreateUpdateRemoveConnectors
					, allowCreateUpdateRemoveConnectors
					, _T("Allow Create Update Remove Connnectors")
					, addErrorFlagIfNecessary(flags, allowCreateUpdateRemoveConnectors));

				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowCreateUpdateChannels, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowCreatePrivateChannels, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowDeleteChannels, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowAddRemoveApps, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowCreateUpdateRemoveTabs, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowCreateUpdateRemoveConnectors, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
			}

			getGenerator().addCategory(_T("Team Guest Settings"), CategoryFlags::NOFLAG);
			{
				const wstring allowCreateUpdateChannels = _YTEXT("guestSettingsAllowCreateUpdateChannels");
				addBoolToggleEditor(&TeamGuestSettings::GetAllowCreateUpdateChannels
					, &TeamGuestSettings::SetAllowCreateUpdateChannels
					, allowCreateUpdateChannels
					, _T("Allow Create Update Channels")
					, addErrorFlagIfNecessary(flags, allowCreateUpdateChannels));

				const wstring allowDeleteChannels = _YTEXT("guestSettingsAllowDeleteChannels");
				addBoolToggleEditor(&TeamGuestSettings::GetAllowDeleteChannels
					, &TeamGuestSettings::SetAllowDeleteChannels
					, allowDeleteChannels
					, _T("Allow Delete Channels")
					, addErrorFlagIfNecessary(flags, allowDeleteChannels));

				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowCreateUpdateChannels, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowDeleteChannels, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
			}

			getGenerator().addCategory(_T("Team Messaging Settings"), CategoryFlags::NOFLAG);
			{
				const wstring allowUserEditMessages = _YTEXT("messagingSettingsAllowUserEditMessages");
				addBoolToggleEditor(&TeamMessagingSettings::GetAllowUserEditMessages
					, &TeamMessagingSettings::SetAllowUserEditMessages
					, allowUserEditMessages
					, _T("Allow User Edit Messages")
					, addErrorFlagIfNecessary(flags, allowUserEditMessages));

				const wstring allowUserDeleteMessages = _YTEXT("messagingSettingsAllowUserDeleteMessages");
				addBoolToggleEditor(&TeamMessagingSettings::GetAllowUserDeleteMessages
					, &TeamMessagingSettings::SetAllowUserDeleteMessages
					, allowUserDeleteMessages
					, _T("Allow User Delete Messages")
					, addErrorFlagIfNecessary(flags, allowUserDeleteMessages));

				const wstring allowOwnerDeleteMessages = _YTEXT("messagingSettingsAllowOwnerDeleteMessages");
				addBoolToggleEditor(&TeamMessagingSettings::GetAllowOwnerDeleteMessages
					, &TeamMessagingSettings::SetAllowOwnerDeleteMessages
					, allowOwnerDeleteMessages
					, _T("Allow Owner Delete Messages")
					, addErrorFlagIfNecessary(flags, allowOwnerDeleteMessages));

				const wstring allowTeamMentions = _YTEXT("messagingSettingsAllowTeamMentions");
				addBoolToggleEditor(&TeamMessagingSettings::GetAllowTeamMentions
					, &TeamMessagingSettings::SetAllowTeamMentions
					, allowTeamMentions
					, _T("Allow Team Mentions")
					, addErrorFlagIfNecessary(flags, allowTeamMentions));

				const wstring allowChannelMentions = _YTEXT("messagingSettingsAllowChannelMentions");
				addBoolToggleEditor(&TeamMessagingSettings::GetAllowChannelMentions
					, &TeamMessagingSettings::SetAllowChannelMentions
					, allowChannelMentions
					, _T("Allow Channel Mentions")
					, addErrorFlagIfNecessary(flags, allowChannelMentions));

				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowUserEditMessages, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowUserDeleteMessages, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowOwnerDeleteMessages, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowTeamMentions, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowChannelMentions, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
			}

			getGenerator().addCategory(_T("Team Fun Settings"), CategoryFlags::NOFLAG);
			{
				const wstring allowGiphy = _YTEXT("funSettingsAllowGiphy");
				addBoolToggleEditor(&TeamFunSettings::GetAllowGiphy
					, &TeamFunSettings::SetAllowGiphy
					, allowGiphy
					, _T("Allow Giphy")
					, addErrorFlagIfNecessary(flags, allowGiphy));

				const wstring giphyContentRating = _YTEXT("funSettingsGiphyContentRating");
				const ComboEditor::LabelsAndValues giphyContentRatingLabelsAndValues
				{
					{ _T("Moderate"), _YTEXT("moderate") },
					{ _T("Strict"), _YTEXT("strict") },
				};
				addComboEditor(&TeamFunSettings::GetGiphyContentRating
					, &TeamFunSettings::SetGiphyContentRating
					, giphyContentRating
					, _T("Giphy Content Rating")
					, addErrorFlagIfNecessary(flags | EditorFlags::REQUIRED, giphyContentRating)
					, giphyContentRatingLabelsAndValues);

				const wstring allowStickersAndMemes = _YTEXT("funSettingsAllowStickersAndMemes");
				addBoolToggleEditor(&TeamFunSettings::GetAllowStickersAndMemes
					, &TeamFunSettings::SetAllowStickersAndMemes
					, allowStickersAndMemes
					, _T("Allow Stickers and Memes")
					, addErrorFlagIfNecessary(flags, allowStickersAndMemes));

				const wstring allowCustomMemes = _YTEXT("funSettingsAllowCustomMemes");
				addBoolToggleEditor(&TeamFunSettings::GetAllowCustomMemes
					, &TeamFunSettings::SetAllowCustomMemes
					, allowCustomMemes
					, _T("Allow Custom Memes")
					, addErrorFlagIfNecessary(flags, allowCustomMemes));

				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowGiphy, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ giphyContentRating, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowStickersAndMemes, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
				getGenerator().addEvent(_YTEXT("isTeam"), EventTarget({ allowCustomMemes, EventTarget::g_HideIfNotEqual, UsefulStrings::g_ToggleValueTrue }));
			}
		}
	}

	if (isReadDialog())
	{
		getGenerator().addCloseButton(LocalizedStrings::g_CloseButtonText);
	}
	else
	{
		getGenerator().addApplyButton(isEditDialog() ? LocalizedStrings::g_ApplyButtonText : LocalizedStrings::g_CreateButtonText);
		getGenerator().addCancelButton(LocalizedStrings::g_CancelButtonText);
		getGenerator().addResetButton(LocalizedStrings::g_ResetButtonText);
	}
}

void DlgBusinessGroupEditHTML::addIsTeamEditor()
{
	boost::YOpt<bool> value;
	bool allTheSame = true;

	for (const auto& object : m_BusinessGroups)
	{
		if (object.GetIsTeam())
		{
			if (!value.is_initialized())
				value = true;
			else if (!*value)
				allTheSame = false;
		}
		else if (object.GetEditIsTeam().is_initialized())
		{
			if (!value.is_initialized())
				value = object.GetEditIsTeam();
			else if (value != object.GetEditIsTeam())
				allTheSame = false;
		}
		else
		{
			if (!value.is_initialized())
				value = false;
			else if (*value)
				allTheSame = false;
		}
	}

	uint32_t flags = EditorFlags::READONLY;
	if (!allTheSame)
		flags |= EditorFlags::NOCHANGE;

	getGenerator().Add(BoolToggleEditor({ { _YTEXT("isTeam"), YtriaTranslate::Do(GridGroups_customizeGrid_3, _YLOC("Is a Team")).c_str(), flags, value && *value } }));
}

bool DlgBusinessGroupEditHTML::processPostedData(const IPostedDataTarget::PostedData& data)
{
	for (const auto& item : data)
	{
		const auto& propName = item.first;
		const auto& propValue = item.second;

		auto itStr = m_StringSetters.find(propName);
		if (m_StringSetters.end() != itStr)
		{
			for (auto& businessGroup : m_BusinessGroups)
			{
				// FIXME: We should only do this test for list editors!
				if (GridBackendUtil::g_NoValueString != propValue)
					itStr->second(businessGroup, boost::YOpt<PooledString>(propValue));
				else
					itStr->second(businessGroup, PooledString(_YTEXT("")));
			}
		}
		else
		{
			auto itBool = m_BoolSetters.find(propName);
			if (m_BoolSetters.end() != itBool)
			{
				for (auto& businessGroup : m_BusinessGroups)
					itBool->second(businessGroup, boost::YOpt<bool>(Str::getBoolFromString(propValue)));
			}
			else
			{
#define TEAM_SETTINGS_PROCESS_BOOL(_Prefix) \
				auto itTeam = m_Team##_Prefix##SettingsBoolSetters.find(propName); \
				if (m_Team##_Prefix##SettingsBoolSetters.end() != itTeam) \
				{ \
					for (auto& businessGroup : m_BusinessGroups) \
					{ \
						auto team = businessGroup.GetTeam(); \
						if (!team->##_Prefix##Settings) \
							team->##_Prefix##Settings.emplace(); \
						itTeam->second(*team->##_Prefix##Settings, boost::YOpt<bool>(Str::getBoolFromString(propValue))); \
						businessGroup.SetTeam(team); \
					} \
				} \

#define TEAM_SETTINGS_PROCESS_STRING(_Prefix) \
				auto itTeam = m_Team##_Prefix##SettingsStringSetters.find(propName); \
				if (m_Team##_Prefix##SettingsStringSetters.end() != itTeam) \
				{ \
					for (auto& businessGroup : m_BusinessGroups) \
					{ \
						auto team = businessGroup.GetTeam(); \
						if (team) \
						{ \
							if (!team->##_Prefix##Settings) \
								team->##_Prefix##Settings.emplace(); \
							itTeam->second(*team->##_Prefix##Settings, boost::YOpt<PooledString>(propValue)); \
							businessGroup.SetTeam(team); \
						} \
					} \
				} \

				TEAM_SETTINGS_PROCESS_BOOL(Member)
				else
				{
					TEAM_SETTINGS_PROCESS_BOOL(Guest)
					else
					{
						TEAM_SETTINGS_PROCESS_BOOL(Messaging)
						else
						{
							TEAM_SETTINGS_PROCESS_BOOL(Fun)
							else
							{
								TEAM_SETTINGS_PROCESS_STRING(Fun)
								else
								{
									if (propName == _YTEXT("owner"))
									{
										// read-only and only for creation, should not be posted.
										ASSERT(false);
									}
									else
									{
										// TODO: Other types?
										ASSERT(false);
									}
								}
							}
						}
					}
				}
#undef TEAM_SETTINGS_PROCESS_BOOL
#undef TEAM_SETTINGS_PROCESS_STRING
			}
		}
	}

	return true;
}

wstring DlgBusinessGroupEditHTML::getDialogTitle() const
{
	wstring title;

	if (isReadDialog())
		title = YtriaTranslate::Do(DlgBusinessGroupEditHTML_getDialogTitle_2, _YLOC("View %1 group(s)"), Str::getStringFromNumber(m_BusinessGroups.size()).c_str());
	else if (isEditDialog())
		title = YtriaTranslate::Do(DlgBusinessGroupEditHTML_getDialogTitle_3, _YLOC("Edit %1 group(s)"), Str::getStringFromNumber(m_BusinessGroups.size()).c_str());
	else if (isEditCreateDialog())
		title = YtriaTranslate::Do(DlgBusinessGroupEditHTML_getDialogTitle_4, _YLOC("Edit %1 new group(s)"), Str::getStringFromNumber(m_BusinessGroups.size()).c_str());
	else if (isCreateDialog())
		title = YtriaTranslate::Do(DlgBusinessGroupEditHTML_getDialogTitle_1, _YLOC("Create a new group")).c_str();
	else if (isPrefilledCreateDialog())
		title = YtriaTranslate::Do(DlgBusinessGroupEditHTML_getDialogTitle_1, _YLOC("Create a new group")).c_str();

	return title;
}

void DlgBusinessGroupEditHTML::addStringEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableGroupTypes)
{
	addObjectStringEditor<BusinessGroup>(m_BusinessGroups
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_TYPE | EditorFlags::RESTRICT_ERROR | EditorFlags::RESTRICT_NOT_CREATED)
		, [p_Flags, &p_ApplicableGroupTypes](const BusinessGroup& bg)
		{
			return getRestrictionsImpl(bg, p_Flags, p_ApplicableGroupTypes);
		}
	);

	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

void DlgBusinessGroupEditHTML::addComboEditor(StringGetter p_Getter, StringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues, vector<wstring> p_ApplicableGroupTypes)
{
	addObjectListEditor<BusinessGroup>(m_BusinessGroups
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_TYPE | EditorFlags::RESTRICT_ERROR | EditorFlags::RESTRICT_NOT_CREATED)
		, p_LabelsAndValues
		, [p_Flags, &p_ApplicableGroupTypes](const BusinessGroup& bg)
		{
			return getRestrictionsImpl(bg, p_Flags, p_ApplicableGroupTypes);
		}
	);
	
	ASSERT(!hasProperty(p_PropName));
	m_StringSetters[p_PropName] = p_Setter;
}

void DlgBusinessGroupEditHTML::addUserNameEditor(StringGetter p_Getter, StringSetter p_SetterLeft, StringSetter p_SetterRight, const wstring& p_PropNameLeft, const wstring& p_PropNameRight, const wstring& p_PropLabel, uint32_t p_Flags, const vector<std::tuple<wstring, wstring>>& p_LabelsAndValues, vector<wstring> p_ApplicableGroupTypes)
{
	addObjectUserNameEditor<BusinessGroup>(m_BusinessGroups
		, p_Getter
		, p_PropNameLeft
		, p_PropNameRight
		, p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_TYPE | EditorFlags::RESTRICT_ERROR | EditorFlags::RESTRICT_NOT_CREATED)
		, p_LabelsAndValues
		, [p_Flags, &p_ApplicableGroupTypes](const BusinessGroup& bg)
		{
			return getRestrictionsImpl(bg, p_Flags, p_ApplicableGroupTypes);
		}
	);

	ASSERT(!hasProperty(p_PropNameLeft));
	ASSERT(!hasProperty(p_PropNameRight));
	m_StringSetters[p_PropNameLeft] = p_SetterLeft;
	m_StringSetters[p_PropNameRight] = p_SetterRight;
}

void DlgBusinessGroupEditHTML::addBoolEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableGroupTypes)
{
	addObjectBoolEditor<BusinessGroup>(m_BusinessGroups
		, p_Getter
		, p_PropName
		, p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_TYPE | EditorFlags::RESTRICT_ERROR | EditorFlags::RESTRICT_NOT_CREATED)
		, [p_Flags, &p_ApplicableGroupTypes](const BusinessGroup& bg)
		{
			return getRestrictionsImpl(bg, p_Flags, p_ApplicableGroupTypes);
		}
	);
	
	ASSERT(!hasProperty(p_PropName));
	m_BoolSetters[p_PropName] = p_Setter;
}

void DlgBusinessGroupEditHTML::addBoolToggleEditor(BoolGetter p_Getter, BoolSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, vector<wstring> p_ApplicableGroupTypes)
{
    addObjectBoolToggleEditor<BusinessGroup>(m_BusinessGroups
        , p_Getter
        , p_PropName
        , p_PropLabel
		, p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_TYPE | EditorFlags::RESTRICT_ERROR | EditorFlags::RESTRICT_NOT_CREATED)
		, [p_Flags, &p_ApplicableGroupTypes](const BusinessGroup& bg)
		{
			return getRestrictionsImpl(bg, p_Flags, p_ApplicableGroupTypes);
		}
    );

	ASSERT(!hasProperty(p_PropName));
    m_BoolSetters[p_PropName] = p_Setter;
}

bool DlgBusinessGroupEditHTML::hasProperty(const wstring& p_PropertyName) const
{
#define TEAM_SETTINGS_BOOL(_Prefix) \
	m_Team##_Prefix##SettingsBoolSetters.end() != m_Team##_Prefix##SettingsBoolSetters.find(p_PropertyName)

#define TEAM_SETTINGS_STRING(_Prefix) \
	m_Team##_Prefix##SettingsStringSetters.end() != m_Team##_Prefix##SettingsStringSetters.find(p_PropertyName)

	return m_StringSetters.end() != m_StringSetters.find(p_PropertyName)
		|| m_BoolSetters.end() != m_BoolSetters.find(p_PropertyName)
		|| TEAM_SETTINGS_BOOL(Member)
		|| TEAM_SETTINGS_BOOL(Guest)
		|| TEAM_SETTINGS_BOOL(Messaging)
		|| TEAM_SETTINGS_BOOL(Fun)
		|| TEAM_SETTINGS_STRING(Fun);

#undef TEAM_SETTINGS_BOOL
#undef TEAM_SETTINGS_STRING
}

int DlgBusinessGroupEditHTML::addErrorFlagIfNecessary(int p_Flags, const wstring& p_PropName)
{
	ASSERT(EditorFlags::READONLY != (EditorFlags::READONLY & p_Flags));
	if (m_ListFieldErrors.end() != m_ListFieldErrors.find(p_PropName))
		p_Flags |= EditorFlags::RESTRICT_ERROR;
	return p_Flags;
}

void DlgBusinessGroupEditHTML::addCategory(const wstring& p_CategoryName, bool p_ExpandByDefault, vector<wstring> p_ApplicableGroupTypes)
{
	bool showCategory = true;
	for (auto& bg : m_BusinessGroups)
	{
		if (bg.GetCombinedGroupType()
			&& p_ApplicableGroupTypes.end() == std::find(p_ApplicableGroupTypes.begin(), p_ApplicableGroupTypes.end(), bg.GetCombinedGroupType()->c_str()))
		{
			showCategory = false;
			break;
		}
	}

	getGenerator().addCategory(p_CategoryName, (p_ExpandByDefault ? CategoryFlags::EXPAND_BY_DEFAULT : CategoryFlags::NOFLAG) | (!showCategory ? CategoryFlags::HIDE_BY_DEFAULT : CategoryFlags::NOFLAG));
}

uint32_t DlgBusinessGroupEditHTML::getRestrictionsImpl(const BusinessGroup& bg, uint32_t p_Flags, const vector<wstring>& p_ApplicableGroupTypes)
{
	uint32_t restrictFlags = 0;

	if (EditorFlags::RESTRICT_LOAD_MORE == (EditorFlags::RESTRICT_LOAD_MORE & p_Flags) && !bg.IsMoreLoaded())
		restrictFlags |= EditorFlags::RESTRICT_LOAD_MORE;

	if (EditorFlags::RESTRICT_NOT_CREATED == (EditorFlags::RESTRICT_NOT_CREATED & p_Flags) && BusinessGroup::Flag::CREATED == (bg.GetFlags() & BusinessGroup::Flag::CREATED))
		restrictFlags |= EditorFlags::RESTRICT_NOT_CREATED;

	if (!p_ApplicableGroupTypes.empty() && bg.GetCombinedGroupType() && p_ApplicableGroupTypes.end() == std::find(p_ApplicableGroupTypes.begin(), p_ApplicableGroupTypes.end(), (wstring)*bg.GetCombinedGroupType()))
		restrictFlags |= EditorFlags::RESTRICT_TYPE;

	if (EditorFlags::RESTRICT_ERROR == (EditorFlags::RESTRICT_ERROR & p_Flags))
		restrictFlags |= EditorFlags::RESTRICT_ERROR;

	return restrictFlags;
}

#define TEAM_SETTINGS_BOOL_EDITOR_IMPL(_Prefix) \
void DlgBusinessGroupEditHTML::addBoolToggleEditor(Team##_Prefix##SettingsBoolGetter p_Getter, Team##_Prefix##SettingsBoolSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, const uint32_t p_Flags) \
{ \
	bool value = false; \
 \
	auto accessor = [&p_Getter](const BusinessGroup& p_Group) \
	{ \
		static const boost::YOpt<bool> noValue; \
		return p_Group.GetTeam() && p_Group.GetTeam()->##_Prefix##Settings ? p_Getter(*p_Group.GetTeam()->##_Prefix##Settings) : noValue; \
	}; \
 \
	uint32_t flags = p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE); \
	{ \
		bool allTheSame = true; \
		const auto val = accessor(m_BusinessGroups.front()); \
		for (const auto& object : m_BusinessGroups) \
		{ \
			if (val != accessor(object)) \
				allTheSame = false; \
 \
			flags = flags | getRestrictionsImpl(object, p_Flags, { BusinessGroup::g_Office365Group }); \
		} \
 \
		if (allTheSame) \
			value = val.is_initialized() ? val.get() : false; \
		else \
			flags = flags | EditorFlags::NOCHANGE; \
	} \
 \
	getGenerator().Add(BoolToggleEditor({ { p_PropName, p_PropLabel, flags, value } })); \
 \
	ASSERT(!hasProperty(p_PropName)); \
	m_Team##_Prefix##SettingsBoolSetters[p_PropName] = p_Setter; \
} \

TEAM_SETTINGS_BOOL_EDITOR_IMPL(Member)
TEAM_SETTINGS_BOOL_EDITOR_IMPL(Guest)
TEAM_SETTINGS_BOOL_EDITOR_IMPL(Messaging)
TEAM_SETTINGS_BOOL_EDITOR_IMPL(Fun)

#undef TEAM_SETTINGS_BOOL_EDITOR_IMPL

#define TEAM_SETTINGS_STRING_EDITOR_IMPL(_Prefix) \
void DlgBusinessGroupEditHTML::addComboEditor(Team##_Prefix##SettingsStringGetter p_Getter, Team##_Prefix##SettingsStringSetter p_Setter, const wstring& p_PropName, const wstring& p_PropLabel, uint32_t p_Flags, const ComboEditor::LabelsAndValues& p_LabelsAndValues) \
{ \
	wstring value; \
 \
	auto accessor = [&p_Getter](const BusinessGroup& p_Group) \
	{ \
		static const boost::YOpt<PooledString> noValue; \
		return p_Group.GetTeam() && p_Group.GetTeam()->##_Prefix##Settings ? p_Getter(*p_Group.GetTeam()->##_Prefix##Settings) : noValue; \
	}; \
 \
	uint32_t flags = p_Flags & ~(EditorFlags::RESTRICT_LOAD_MORE | EditorFlags::RESTRICT_NOT_CREATED | EditorFlags::RESTRICT_TYPE); \
	{ \
		bool allTheSame = true; \
		const auto val = accessor(m_BusinessGroups.front()); \
		for (const auto& object : m_BusinessGroups) \
		{ \
			if (val != accessor(object)) \
				allTheSame = false; \
 \
			flags = flags | getRestrictionsImpl(object, p_Flags, { BusinessGroup::g_Office365Group }); \
		} \
 \
		if (allTheSame) \
			value = val.is_initialized() ? (const wstring)val.get() : L""; \
		else \
			flags = flags | EditorFlags::NOCHANGE; \
	} \
 \
	getGenerator().Add(ComboEditor( { p_PropName, p_PropLabel, flags, value }, p_LabelsAndValues)); \
 \
	ASSERT(!hasProperty(p_PropName)); \
	m_Team##_Prefix##SettingsStringSetters[p_PropName] = p_Setter; \
} \

TEAM_SETTINGS_STRING_EDITOR_IMPL(Fun)

#undef TEAM_SETTINGS_STRING_EDITOR_IMPL
