#pragma once

#include "ISqlQuery.h"

class SessionsSqlEngine;

class QueryRemoveUnusedCredentials : public ISqlQuery
{
public:
	QueryRemoveUnusedCredentials(SessionsSqlEngine& p_Engine);
	void Run() override;

private:
	SessionsSqlEngine& m_Engine;
};

