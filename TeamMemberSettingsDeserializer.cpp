#include "TeamMemberSettingsDeserializer.h"

#include "JsonSerializeUtil.h"

void TeamMemberSettingsDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowCreateUpdateChannels"), m_Data.AllowCreateUpdateChannels, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("allowCreatePrivateChannels"), m_Data.AllowCreatePrivateChannels, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowDeleteChannels"), m_Data.AllowDeleteChannels, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowAddRemoveApps"), m_Data.AllowAddRemoveApps, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowCreateUpdateRemoveTabs"), m_Data.AllowCreateUpdateRemoveTabs, p_Object);
    JsonSerializeUtil::DeserializeBool(_YTEXT("allowCreateUpdateRemoveConnectors"), m_Data.AllowCreateUpdateRemoveConnectors, p_Object);
}
