#pragma once

#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "ResUtil.h"
#include "YAdvancedHtmlView.h"

class DlgUserCacheDataHTML
	: public ResizableDialog
	, public YAdvancedHtmlView::IPostedDataTarget
	, public YAdvancedHtmlView::IDocumentCompleteCallback
{
public:
	

	DlgUserCacheDataHTML(CWnd* p_Parent);
	virtual ~DlgUserCacheDataHTML();

	enum {	IDD = RUTIL_DLG_USERCACHE	};

	void SetCacheTxtReg(const wstring& p_CacheTxtReg);
	void SetIntroWindow(const wstring& p_IntroWindow);
	void SetTitleWindow(const wstring& p_TitleWindow);
	void SetCacheTxtBold(const wstring& p_CacheTxtBold);
	void SetCacheTxtLine(const wstring& p_CacheTxtLine);
	void SetServerBold(const wstring& p_ServerLTxtBold);
	void SetServerLine(const wstring& p_ServerTxtLine);
	void SetServerReg(const wstring& p_ServerTxtReg);
	void SetCommentTextBold(const wstring& p_ServerTxtBold);
	void SetCommentTextReg(const wstring& p_ServerTxtReg);

	virtual bool OnNewPostedData(const IPostedDataTarget::PostedData& data);

	/* YAdvancedHtmlView::IDocumentCompleteCallback */
	virtual void OnDocumentComplete();

protected:
	virtual BOOL OnInitDialogSpecificResizable() override;
	bool ChangeText(const wstring& p_Id, const wstring& p_newText);
	void SetOkButton(const wstring& p_OkText);

	ENDDIALOG_FIXED(ResizableDialog)

private:
	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;

private :
	wstring m_OkButton;
	wstring m_TitleWindow;
	wstring m_IntroWindow;
	wstring m_CacheTxtBold;
	wstring m_CacheTxtLine;
	wstring m_CacheTxtReg;
	wstring m_ServerBold;
	wstring m_ServerReg;
	wstring m_ServerLine;
	wstring m_CommentTextBold;
	wstring m_CommentTextReg;
};
