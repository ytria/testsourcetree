#include "PhysicalAddressDeserializer.h"

#include "JsonSerializeUtil.h"

void PhysicalAddressDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("city"), m_Data.City, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("countryOrRegion"), m_Data.CountryOrRegion, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("postalCode"), m_Data.PostalCode, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("state"), m_Data.State, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("street"), m_Data.Street, p_Object);
}
