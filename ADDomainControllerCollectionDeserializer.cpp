#include "ADDomainControllerCollectionDeserializer.h"
#include "PSSerializeUtil.h"

void ADDomainControllerCollectionDeserializer::Deserialize(IPSObjectPropertyReader& p_Reader)
{
	m_Domains.push_back(PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Domain")));
}

void ADDomainControllerCollectionDeserializer::SetCount(size_t p_Count)
{
	m_Domains.reserve(p_Count);
}

wstring ADDomainControllerCollectionDeserializer::GetDomain() const
{
	wstring domain;

	if (!m_Domains.empty())
		domain = m_Domains[0];
	return domain;
}
