#include "AddPrivateChannelMemberRequester.h"

#include "O365AdminUtil.h"
#include "MsGraphFieldNames.h"
#include "MsGraphHttpRequestLogger.h"
#include "RestResultInfo.h"
#include "safeTaskCall.h"

AddPrivateChannelMemberRequester::AddPrivateChannelMemberRequester(BusinessAADUserConversationMember p_Member, std::wstring p_TeamId, std::wstring p_ChannelId)
	: m_Member(p_Member)
	, m_TeamId(p_TeamId)
	, m_ChannelId(p_ChannelId)
{

}

TaskWrapper<void> AddPrivateChannelMemberRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	m_Result = std::make_shared<HttpResultWithError>();

	web::uri_builder uri(_YTEXT("teams"));
	uri.append_path(m_TeamId);
	uri.append_path(_YTEXT("channels"));
	uri.append_path(m_ChannelId);
	uri.append_path(_YTEXT("members"));

	auto body = web::json::value::object();

	body[_YTEXT("@odata.type")] = web::json::value::string(_YTEXT("#microsoft.graph.aadUserConversationMember"));

	{
		auto array = web::json::value::array();
		if (m_Member.GetRoles())
		{
			int i = 0;
			for (auto& role : *m_Member.GetRoles())
			{
				if (!role.IsEmpty())
					array[i++] = web::json::value::string(role.c_str());
			}
		}
		body[_YTEXT("roles")] = array;
	}

	// Remove this bool when available in v1.0
	const bool useBeta = true;

	ASSERT(m_Member.GetUserID());
	if (useBeta)
		body[_YTEXT("user@odata.bind")] = web::json::value::string(_YTEXTFORMAT(L"https://graph.microsoft.com/beta/users/%s", m_Member.GetUserID()->c_str()));
	else
		body[_YTEXT("user@odata.bind")] = web::json::value::string(_YTEXTFORMAT(L"https://graph.microsoft.com/v1.0/users/%s", m_Member.GetUserID()->c_str()));

	LoggerService::User(_YFORMAT(L"Adding member with id %s to channel id %s (group id %s)", m_Member.GetUserID()->c_str(), m_ChannelId.c_str(), m_TeamId.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return p_Session->GetMSGraphSession(httpLogger->GetSessionType())->Post(uri.to_uri(), httpLogger, p_TaskData, WebPayloadJSON(body), useBeta ? _YTEXT("beta") : _YTEXT("")).ThenByTask([p_Session, httpLogger, p_TaskData, result = m_Result, member = m_Member, teamId = m_TeamId, channelId = m_ChannelId](pplx::task<RestResultInfo> p_ResInfo)
		{
			*result = Util::GetResult(p_ResInfo);
			if (result->m_Error
				&& web::http::status_codes::NotFound == result->m_Error->GetStatusCode()
				// FIXME: Is it safe? (i.e. will it change eventually?)
				&& result->m_Error->GetFullErrorMessage() == _YTEXT("NotFound: User is not found in the team."))
			{
				ASSERT(member.GetUserID());
				if (member.GetUserID())
				{
					// Add user to the team then retry.
					Group group;
					group.m_Id = teamId;
					auto addMemberResult = safeTaskCall(group.AddMember(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), httpLogger, *member.GetUserID(), p_TaskData).Then([p_TaskData](const RestResultInfo& p_Result)
					{
						return HttpResultWithError(p_Result, boost::none);
					}), p_Session->GetMSGraphSession(httpLogger->GetSessionType()), Util::WriteErrorHandler, p_TaskData).get();

					// Is it a success?
					if (200 <= addMemberResult.m_ResultInfo->Response.status_code() && addMemberResult.m_ResultInfo->Response.status_code() < 300)
					{
						ASSERT(web::http::status_codes::NoContent == addMemberResult.m_ResultInfo->Response.status_code());
						AddPrivateChannelMemberRequester requester(member, teamId, channelId);
						requester.Send(p_Session, p_TaskData).GetTask().get();
						*result = requester.GetResult();
					}
					else
					{
						*result = addMemberResult;
					}
				}
			}
		});
}

const HttpResultWithError& AddPrivateChannelMemberRequester::GetResult() const
{
	ASSERT(m_Result);
	return *m_Result;
}
