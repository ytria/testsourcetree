#include "SpUsersRequester.h"

#include "Sapio365Session.h"
#include "SharepointOnlineSession.h"
#include "SingleRequestResult.h"
#include "BasicHttpRequestLogger.h"

Sp::SpUsersRequester::SpUsersRequester(PooledString p_SiteUrl)
    : m_SiteUrl(p_SiteUrl)
{
}

TaskWrapper<void> Sp::SpUsersRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
    m_Deserializer = std::make_shared<ValueListDeserializer<Sp::User, Sp::UserDeserializer>>();
    m_Result = std::make_shared<SingleRequestResult>();

    web::uri_builder uri(_YTEXT("sites"));
    uri.append_path(m_SiteUrl);
    uri.append_path(_YTEXT("_api"));
    uri.append_path(_YTEXT("web"));
    uri.append_path(_YTEXT("siteUsers"));

    LoggerService::User(YtriaTranslate::Do(Sp__SpUsersRequester_Send_1, _YLOC("Requesting users for site \"%1\""), m_SiteUrl.c_str()), p_TaskData.GetOriginator());

	auto httpLogger = std::make_shared<BasicHttpRequestLogger>(p_Session->GetIdentifier());
    return p_Session->GetSharepointSession()->Get(uri.to_uri(), httpLogger, m_Deserializer, p_TaskData);
}

const vector<Sp::User>& Sp::SpUsersRequester::GetData() const
{
    return m_Deserializer->GetData();
}
