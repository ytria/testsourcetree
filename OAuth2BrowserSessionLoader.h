#pragma once

#include "ISessionLoader.h"

class OAuth2BrowserSessionLoader : public ISessionLoader
{
public:
	OAuth2BrowserSessionLoader(const SessionIdentifier& p_Identifier, const PersistentSession& p_PersistentSession);
	TaskWrapper<std::shared_ptr<Sapio365Session>> Load(const std::shared_ptr<Sapio365Session>& p_SapioSession) override;

private:
	PersistentSession m_PersistentSession;
};

