#pragma once

#include "BusinessObject.h"

class RecipientPermission : public BusinessObject
{
public:
	std::vector<wstring> m_AccessRights;
	wstring m_Trustee;
};

