#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "RecurrenceRange.h"


class RecurrenceRangeDeserializer : public JsonObjectDeserializer, public Encapsulate<RecurrenceRange>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

