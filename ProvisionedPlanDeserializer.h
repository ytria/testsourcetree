#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class ProvisionedPlanDeserializer : public JsonObjectDeserializer, public Encapsulate<ProvisionedPlan>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

