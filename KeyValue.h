#pragma once

class KeyValue
{
public:
	boost::YOpt<PooledString> m_Key;
	boost::YOpt<PooledString> m_Value;
};

