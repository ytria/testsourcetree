#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class AssignedPlanDeserializer : public JsonObjectDeserializer, public Encapsulate<AssignedPlan>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

