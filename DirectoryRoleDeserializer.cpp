#include "DirectoryRoleDeserializer.h"

#include "JsonSerializeUtil.h"

void DirectoryRoleDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
	JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.m_Description, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("roleTemplateId"), m_Data.m_RoleTemplateId, p_Object);
}
