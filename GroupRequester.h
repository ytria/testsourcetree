#pragma once

#include "IRequester.h"
#include "IPropertySetBuilder.h"
#include "IRequestLogger.h"

class BusinessGroup;
class GroupDeserializer;
class SingleRequestResult;

class GroupRequester : public IRequester
{
public:
    GroupRequester(const PooledString& p_GroupId, IPropertySetBuilder&& p_PropertySet, const std::shared_ptr<IRequestLogger>& p_Logger);
    GroupRequester(const PooledString& p_GroupId, const std::vector<rttr::property>& p_Properties, const std::shared_ptr<IRequestLogger>& p_Logger); // FIXME: Remove this constructor by creating the appropriate IPropertySetBuilder(s) for group

	void SetUseBetaEndpoint(bool p_UseBetaEndpoint);

    TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    BusinessGroup& GetData();
    const SingleRequestResult& GetResult() const;

protected:
    PooledString m_GroupId;
    vector<rttr::property> m_Properties;

    std::shared_ptr<GroupDeserializer> m_Deserializer;
    std::shared_ptr<SingleRequestResult> m_Result;
	std::shared_ptr<IRequestLogger> m_Logger;

	bool m_UseBetaEndpoint;
};

