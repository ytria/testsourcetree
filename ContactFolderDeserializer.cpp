#include "ContactFolderDeserializer.h"

#include "JsonSerializeUtil.h"

void ContactFolderDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeString(_YTEXT("parentFolderId"), m_Data.m_ParentFolderId, p_Object);
}
