#pragma once

#include "BusinessObject.h"

class EducationTeacher : public BusinessObject
{
public:
	boost::YOpt<PooledString> m_ExternalId;
	boost::YOpt<PooledString> m_TeacherNumber;

	RTTR_ENABLE(BusinessObject)
		RTTR_REGISTRATION_FRIEND
};

