#pragma once

#include "BusinessAADUserConversationMember.h"
#include "IRequester.h"
#include "HttpResultWithError.h"

class DeletePrivateChannelMemberRequester : public IRequester
{
public:
	DeletePrivateChannelMemberRequester(std::wstring p_MemberId, std::wstring p_TeamId, std::wstring p_ChannelId);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const HttpResultWithError& GetResult() const;

private:
	std::wstring m_MemberId;
	std::wstring m_TeamId;
	std::wstring m_ChannelId;

	std::shared_ptr<HttpResultWithError> m_Result;
};