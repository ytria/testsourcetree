#include "SubItemKey.h"



SubItemKey::SubItemKey(const wstring& p_Id0)
	:	SubItemKey(p_Id0, _YTEXT(""), _YTEXT(""), _YTEXT(""), _YTEXT(""))
{

}

SubItemKey::SubItemKey(const wstring& p_Id0, const wstring& p_Id1)
	: SubItemKey(p_Id0, p_Id1, _YTEXT(""), _YTEXT(""), _YTEXT(""))
{

}

SubItemKey::SubItemKey(const wstring& p_Id0, const wstring& p_Id1, const wstring& p_Id2)
	: SubItemKey(p_Id0, p_Id1, p_Id2, _YTEXT(""), _YTEXT(""))
{

}

SubItemKey::SubItemKey(const wstring& p_Id0, const wstring& p_Id1, const wstring& p_Id2, const wstring& p_Id3)
	: SubItemKey(p_Id0, p_Id1, p_Id2, p_Id3, _YTEXT(""))
{

}

SubItemKey::SubItemKey(const wstring& p_Id0, const wstring& p_Id1, const wstring& p_Id2, const wstring& p_Id3, const wstring& p_Id4)
	: m_Id0(p_Id0)
	, m_Id1(p_Id1)
	, m_Id2(p_Id2)
	, m_Id3(p_Id3)
	, m_Id4(p_Id4)
{

}

bool SubItemKey::operator<(const SubItemKey& p_Key) const
{
	return std::tie(m_Id0, m_Id1, m_Id2, m_Id3, m_Id4)
		< std::tie(p_Key.m_Id0, p_Key.m_Id1, p_Key.m_Id2, p_Key.m_Id3, p_Key.m_Id4);
}

bool SubItemKey::operator==(const SubItemKey& p_Key) const
{
    return	m_Id0 == p_Key.m_Id0
		&&	m_Id1 == p_Key.m_Id1
		&&	m_Id2 == p_Key.m_Id2
		&&	m_Id3 == p_Key.m_Id3
		&&	m_Id4 == p_Key.m_Id4;
}
