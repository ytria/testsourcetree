#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "PermissionScope.h"

class PermissionScopeDeserializer : public JsonObjectDeserializer, public Encapsulate<PermissionScope>
{
public:
	void DeserializeObject(const web::json::object& p_Object) override;
};

