#pragma once

#include "IRequester.h"
#include "IRequestLogger.h"

template <class T, class U>
class ValueListDeserializer;

class BusinessDirectoryRoleTemplate;
class DirectoryRoleTemplateDeserializer;

class DirectoryRoleTemplatesRequester : public IRequester
{
public:
	DirectoryRoleTemplatesRequester(const std::shared_ptr<IRequestLogger>& p_Logger);
    virtual TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

    const vector<BusinessDirectoryRoleTemplate>& GetData() const;

private:
    std::shared_ptr<ValueListDeserializer<BusinessDirectoryRoleTemplate, DirectoryRoleTemplateDeserializer>> m_Deserializer;
	std::shared_ptr<IRequestLogger> m_Logger;
};

