#include "DeletedUserListRequester.h"

#include "BusinessGroup.h"
#include "BusinessUserDeleted.h"
#include "IPropertySetBuilder.h"
#include "MSGraphCommonData.h"
#include "MSGraphUtil.h"
#include "MSGraphSession.h"
#include "PaginatedRequestResults.h"
#include "PaginatorUtil.h"
#include "SQL + Cloud/Role Delegation/RoleDelegationManager.h"
#include "Sapio365Session.h"
#include "UserDeserializer.h"
#include "UserDeserializerFactory.h"
#include "ValueListDeserializer.h"
#include "RbacRequiredPropertySet.h"
#include "MsGraphHttpRequestLogger.h"

DeletedUserListRequester::DeletedUserListRequester(const IPropertySetBuilder& p_PropertySet, const std::shared_ptr<IPageRequestLogger>& p_Logger)
    : m_Properties(std::move(p_PropertySet.GetPropertySet())),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> DeletedUserListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	auto factory	= std::make_unique<UserDeserializerFactory>(p_Session);
    m_Deserializer	= std::make_shared<ValueListDeserializer<BusinessUser, UserDeserializer>>(std::move(factory));

	// Need to request RBAC props?
	const auto properties = RbacRequiredPropertySet<BusinessUserDeleted>(m_Properties, *p_Session).GetPropertySet();

	web::uri_builder uri;
	uri.append_path(_YTEXT("directory/deletedItems/microsoft.graph.user"));
	uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(properties));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Deserializer, m_Logger, httpLogger, p_TaskData.GetOriginator()).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

vector<BusinessUser>& DeletedUserListRequester::GetData()
{
    return m_Deserializer->GetData();
}
