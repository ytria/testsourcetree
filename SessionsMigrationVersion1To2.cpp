#include "SessionsMigrationVersion1To2.h"

#include "FileUtil.h"
#include "MigrationUtil.h"
#include "SessionsSqlEngine.h"
#include "SqlQueryMultiPreparedStatement.h"
#include "SQLSafeTransaction.h"
#include "SessionMigrationUtil.h"

void SessionsMigrationVersion1To2::Build()
{
	AddStep(std::bind(&SessionsMigrationVersion1To2::CreateNewFromOld, this));
	AddStep(std::bind(&SessionsMigrationVersion1To2::Exec, this));
}

VersionSourceTarget SessionsMigrationVersion1To2::GetVersionInfo() const
{
	return { 1, 2 };
}

bool SessionsMigrationVersion1To2::CreateNewFromOld()
{
	return SessionMigrationUtil::CopyDatabase(GetVersionInfo());
}

bool SessionsMigrationVersion1To2::Exec()
{
	// This migration make some columns case-insensitive (COLLATE NOCASE)

	bool success = false;
	return SQLSafeTransaction(GetTargetSQLEngine(), [this, &success]()
		{
			const std::vector<wstring> statements =
			{
				LR"(CREATE TEMPORARY TABLE sessionBackup(Id,EmailOrAppId,SessionType,DisplayName,AccessCount,CreatedOn,IsFavorite,UserFullName,LastUsed,ProfilePicture,TenantName,TenantDomain,ShowOwnScopeOnly,RoleId,CredentialsId);)",
				LR"(INSERT INTO sessionBackup SELECT Id,EmailOrAppId,SessionType,DisplayName,AccessCount,CreatedOn,IsFavorite,UserFullName,LastUsed,ProfilePicture,TenantName,TenantDomain,ShowOwnScopeOnly,RoleId,CredentialsId FROM Sessions;)",
				LR"(DROP TABLE Sessions;)",

				LR"(CREATE TEMPORARY TABLE rolesBackup(Id,Name);)",
				LR"(INSERT INTO rolesBackup SELECT Id,Name FROM Roles;)",
				LR"(DROP TABLE Roles;)",
				LR"(CREATE TABLE Roles (
							Id INTEGER NOT NULL PRIMARY KEY UNIQUE,
							Name TEXT NOT NULL COLLATE NOCASE);)",
				LR"(INSERT INTO Roles SELECT Id,Name FROM rolesBackup;)",
				LR"(DROP TABLE rolesBackup;)",

				LR"(CREATE TEMPORARY TABLE credBackup(Id,AccessToken,RefreshToken,AppId,AppTenant,AppClientSecret);)",
				LR"(INSERT INTO credBackup SELECT Id,AccessToken,RefreshToken,AppId,AppTenant,AppClientSecret FROM Credentials;)",
				LR"(DROP TABLE Credentials;)",
				LR"(CREATE TABLE Credentials (
							Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
							AccessToken	TEXT,
							RefreshToken TEXT,
							AppId TEXT COLLATE NOCASE,
							AppTenant TEXT COLLATE NOCASE,
							AppClientSecret TEXT);)",
				LR"(INSERT INTO Credentials SELECT Id,AccessToken,RefreshToken,AppId,AppTenant,AppClientSecret FROM credBackup;)",
				LR"(DROP TABLE credBackup;)",

				LR"(CREATE TABLE Sessions (
							Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
							EmailOrAppId TEXT NOT NULL COLLATE NOCASE,
							SessionType	TEXT NOT NULL COLLATE NOCASE,
							DisplayName	TEXT NOT NULL COLLATE NOCASE,
							AccessCount	INTEGER NOT NULL,
							CreatedOn TEXT NOT NULL,
							IsFavorite INTEGER NOT NULL,
							UserFullName TEXT NOT NULL COLLATE NOCASE,
							LastUsed TEXT NOT NULL,
							ProfilePicture BLOB,
							TenantName TEXT NOT NULL COLLATE NOCASE,
							TenantDomain TEXT NOT NULL COLLATE NOCASE,
							ShowOwnScopeOnly INTEGER NOT NULL,
							RoleId INTEGER,
							CredentialsId INTEGER,
							UNIQUE(EmailOrAppId, SessionType, RoleId),
							FOREIGN KEY(RoleId) REFERENCES Roles(Id),
							FOREIGN KEY(CredentialsId) REFERENCES Credentials(Id));)",
				LR"(INSERT INTO Sessions SELECT Id,EmailOrAppId,SessionType,DisplayName,AccessCount,CreatedOn,IsFavorite,UserFullName,LastUsed,ProfilePicture,TenantName,TenantDomain,ShowOwnScopeOnly,RoleId,CredentialsId FROM sessionBackup;)",
				LR"(DROP TABLE sessionBackup;)",
			};

			SqlQueryMultiPreparedStatement query(GetTargetSQLEngine(), statements);
			query.Run();
			success = query.IsSuccess();
		}, true).Run() && success;
}
