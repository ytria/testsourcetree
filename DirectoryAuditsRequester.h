#pragma once

#include "DirectoryAuditDeserializer.h"
#include "IPageRequestLogger.h"
#include "IRequester.h"
#include "MsGraphPaginator.h"
#include "ODataFilter.h"
#include "ValueListDeserializer.h"

class PaginatedRequestResults;
class DirectoryAudit;

class DirectoryAuditsRequester : public IRequester
{
public:
	using DeserializerType = ValueListDeserializer<DirectoryAudit, DirectoryAuditDeserializer>;

	DirectoryAuditsRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger);

	void SetCutOffDateTime(const boost::YOpt<wstring>& p_CutOff);

	void SetCustomFilter(const ODataFilter& p_CustomFilter);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const vector<DirectoryAudit>& GetData() const;

private:
	MsGraphPaginator CreatePaginator(const web::uri& p_Uri, const std::shared_ptr<DeserializerType>& p_Deserializer, const std::shared_ptr<IPageRequestLogger>& p_Logger, const std::shared_ptr<IHttpRequestLogger>& p_HttpLogger, HWND p_Hwnd) const;
	std::shared_ptr<DeserializerType> m_Deserializer;
	std::shared_ptr<IPageRequestLogger> m_Logger;

	// Cutoff related
	boost::YOpt<MsGraphPaginator> m_Paginator;
	boost::YOpt<wstring> m_CutOffDateTime;
	ODataFilter m_CustomFilter;
};

