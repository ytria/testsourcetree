#pragma once

#include "GridFrameBase.h"
#include "GridTeamChannelMessages.h"

class FrameTeamChannelMessages : public GridFrameBase
{
public:
	using GridFrameBase::GridFrameBase;

    DECLARE_DYNAMIC(FrameTeamChannelMessages)
    virtual ~FrameTeamChannelMessages() = default;

    virtual O365Grid& GetGrid() override;
    
    virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
    virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

    /*void ApplySpecific(bool p_Selected);
    virtual void ApplyAllSpecific() override;
    virtual void ApplySelectedSpecific() override;*/

    void ShowMessages(const O365DataMap<BusinessGroup, std::map<BusinessChannel, vector<BusinessChatMessage>>>& p_Messages, bool p_Refresh, bool p_FullPurge);
    /*void ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments);
    void ShowItemAttachment(const BusinessItemAttachment& p_Attachment);*/

	virtual void HiddenViaHistory() override;
    virtual bool HasApplyButton() override;

	const map<PooledString, PooledString>& GetChannelNames() const;
	void SetChannelNames(const map<PooledString, PooledString>& p_ChannelNames);

protected:
    //DECLARE_MESSAGE_MAP()

    virtual void createGrid() override;

    virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

    virtual AutomatedApp::AUTOMATIONSTATUS automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
    GridTeamChannelMessages m_Grid;

	map<PooledString, PooledString> m_ChannelNames;
};

