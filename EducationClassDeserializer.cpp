#include "EducationClassDeserializer.h"
#include "EducationTermDeserializer.h"
#include "IdentitySetDeserializer.h"

void EducationClassDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.m_Description, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("mailNickname"), m_Data.m_MailNickname, p_Object);
	{
		IdentitySetDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("createdBy"), p_Object))
			m_Data.m_CreatedBy = std::move(d.GetData());
	}
	JsonSerializeUtil::DeserializeString(_YTEXT("classCode"), m_Data.m_ClassCode, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("externalId"), m_Data.m_ExternalId, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("externalName"), m_Data.m_ExternalName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("externalSource"), m_Data.m_ExternalSource, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	{
		EducationTermDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("term"), p_Object))
			m_Data.m_Term = std::move(d.GetData());
	}
}
