#include "CachedGroupPropertySet.h"

#include "MsGraphFieldNames.h"

vector<rttr::property> CachedGroupPropertySet::GetPropertySet() const
{
    // FIXME: Don't use "Group" class neither rttr::property
    const vector<rttr::property> properties = {
		rttr::type::get<Group>().get_property(O365_ID),
        rttr::type::get<Group>().get_property(O365_GROUP_DISPLAYNAME),
        rttr::type::get<Group>().get_property(O365_GROUP_GROUPTYPES),
        rttr::type::get<Group>().get_property(O365_GROUP_MAILENABLED),
        rttr::type::get<Group>().get_property(O365_GROUP_SECURITYENABLED),
		rttr::type::get<Group>().get_property(O365_GROUP_RESOURCEPROVISIONINGOPTIONS) // For isTeam
    };
    ASSERT(properties == Group().GetCacheProperties(Group()));
    return properties;
}

vector<rttr::property> CachedDeletedGroupPropertySet::GetPropertySet() const
{
	auto propSet = CachedGroupPropertySet::GetPropertySet();

	// Same as CachedGroupPropertySet::GetPropertySet(), just add O365_GROUP_DELETEDDATETIME
	propSet.push_back(rttr::type::get<User>().get_property(O365_GROUP_DELETEDDATETIME));

	return propSet;
}
