#pragma once

#include "IPropertySetBuilder.h"

class UserListLicensePropertySet : public IPropertySetBuilder
{
public:
    virtual vector<rttr::property> GetPropertySet() const override;
};

class DeletedUserListLicensePropertySet : public UserListLicensePropertySet
{
public:
	virtual vector<rttr::property> GetPropertySet() const override;
};

class UserListLicenseOnlyPropertySet : public IPropertySetBuilder
{
public:
	virtual vector<rttr::property> GetPropertySet() const override;
};
