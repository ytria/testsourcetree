#pragma once

#include "BaseO365Grid.h"
#include "SignIn.h"

class FrameSignIns;

class GridSignIns : public ModuleO365Grid<SignIn>
{
public:
	GridSignIns();
	virtual ~GridSignIns() override;
	void BuildView(const vector<SignIn>& p_SignIns, bool p_FullPurge, bool p_NoPurge);

	void FillSignInFields(GridBackendRow* p_Row, const SignIn& p_SignIn);

	wstring GetName(const GridBackendRow* p_Row) const override;
	O365Grid::SnapshotCaps GetSnapshotCapabilities() const override;

protected:
	void customizeGrid() override;

	SignIn			getBusinessObject(GridBackendRow*) const override;
	void			UpdateBusinessObjects(const vector<SignIn>& p_SignIns, bool p_SetModifiedStatus) override;
	void			RemoveBusinessObjects(const vector<SignIn>& p_SignIns) override;

	bool GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column) override;
	bool LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column) override;

private:
	DECLARE_MESSAGE_MAP()

	afx_msg void OnChangeOptions();
	afx_msg void OnUpdateChangeOptions(CCmdUI* pCmdUI);

	void InitializeCommands() override;
	void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart) override;
	//void FillUserFields(const BusinessSite& p_Site, const Sp::User& p_SpUser, GridBackendRow* p_Row, GridUpdater& p_Updater);

	FrameSignIns* m_Frame;

	GridBackendColumn* m_ColAppDisplayName;
	GridBackendColumn* m_ColAppId;
	GridBackendColumn* m_ColAppliedConditionalAccessPolicyDisplayName;
	GridBackendColumn* m_ColAppliedConditionalAccessPolicyEnforcedGrantControls;
	GridBackendColumn* m_ColAppliedConditionalAccessPolicyEnforcedSessionControls;
	GridBackendColumn* m_ColAppliedConditionalAccessPolicyId;
	GridBackendColumn* m_ColAppliedConditionalAccessPolicyResult;
	GridBackendColumn* m_ColClientAppUsed;
	GridBackendColumn* m_ColConditionalAccessStatus;
	GridBackendColumn* m_ColCorrelationId;
	GridBackendColumn* m_ColCreatedDateTime;
	GridBackendColumn* m_ColDeviceDetailBrowser;
	GridBackendColumn* m_ColDeviceDetailDeviceId;
	GridBackendColumn* m_ColDeviceDetailDisplayName;
	GridBackendColumn* m_ColDeviceDetailIsCompliant;
	GridBackendColumn* m_ColDeviceDetailIsManaged;
	GridBackendColumn* m_ColDeviceDetailOperatingSystem;
	GridBackendColumn* m_ColDeviceDetailTrustType;
	GridBackendColumn* m_ColIpAddress;
	GridBackendColumn* m_ColIsInteractive;
	GridBackendColumn* m_ColLocationCity;
	GridBackendColumn* m_ColLocationCountryOrRegion;
	GridBackendColumn* m_ColLocationGeoCoordinatesAltitude;
	GridBackendColumn* m_ColLocationGeoCoordinatesLatitude;
	GridBackendColumn* m_ColLocationGeoCoordinatesLongitude;
	GridBackendColumn* m_ColLocationState;
	GridBackendColumn* m_ColResourceDisplayName;
	GridBackendColumn* m_ColResourceId;
	GridBackendColumn* m_ColRiskDetail;
	GridBackendColumn* m_ColRiskEventTypes;
	GridBackendColumn* m_ColRiskLevelAggregated;
	GridBackendColumn* m_ColRiskLevelDuringSignIn;
	GridBackendColumn* m_ColRiskState;
	GridBackendColumn* m_ColStatusAdditionalDetails;
	GridBackendColumn* m_ColStatusError;
	GridBackendColumn* m_ColStatusErrorCode;
	GridBackendColumn* m_ColStatusFailureReason;
	GridBackendColumn* m_ColUserDisplayName;
	GridBackendColumn* m_ColUserId;
	GridBackendColumn* m_ColUserPrincipalName;

	static wstring g_Success;
	static wstring g_Error;
};

