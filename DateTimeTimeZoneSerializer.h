#pragma once

#include "IJsonSerializer.h"

class DateTimeTimeZoneSerializer : public IJsonSerializer
{
public:
    DateTimeTimeZoneSerializer(const DateTimeTimeZone& p_DateTimeTimeZone);
    void Serialize() override;

private:
    const DateTimeTimeZone& m_Dttz;
};
