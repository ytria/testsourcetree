#pragma once

namespace Azure
{
	class Deployment
	{
	public:
		boost::YOpt<PooledString> m_Name;
		boost::YOpt<PooledString> m_ProvisioningState;
		boost::YOpt<PooledString> m_CosmosDbAccountName;
		boost::YOpt<PooledString> m_ErrorCode;
		boost::YOpt<PooledString> m_ErrorMessage;
	};
}
