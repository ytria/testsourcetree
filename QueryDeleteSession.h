#pragma once

#include "ISqlQuery.h"
#include "SessionIdentifier.h"

class O365SQLiteEngine;

class QueryDeleteSession : public ISqlQuery
{
public:
	QueryDeleteSession(O365SQLiteEngine& p_Engine, const SessionIdentifier& p_Identifier);
	void Run() override;

private:
	O365SQLiteEngine& m_Engine;
	SessionIdentifier m_Identifier;
};

