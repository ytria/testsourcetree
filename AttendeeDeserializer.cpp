#include "AttendeeDeserializer.h"

#include "EmailAddressDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ResponseStatusDeserializer.h"

void AttendeeDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    {
        ResponseStatusDeserializer rsd;
		if (JsonSerializeUtil::DeserializeAny(rsd, _YTEXT("status"), p_Object))
            m_Data.Status = rsd.GetData();
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("type"), m_Data.Type, p_Object);

    {
        EmailAddressDeserializer ead;
        if (JsonSerializeUtil::DeserializeAny(ead, _YTEXT("emailAddress"), p_Object))
            m_Data.EmailAddress = ead.GetData();
    }
}
