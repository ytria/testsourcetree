#pragma once

#include "IPropertySetBuilder.h"

#include "MFCUtil.h"
#include "Str.h"
#include "Sapio365Session.h"

template <class T>
class RbacRequiredPropertySet : public IPropertySetBuilder
{
public:
	RbacRequiredPropertySet(const vector<rttr::property>& p_Props, const Sapio365Session& p_Session)
		:m_RttrProperties(p_Props),
		m_Session(p_Session)
	{
	}

	RbacRequiredPropertySet(const vector<PooledString>& p_Props, const Sapio365Session& p_Session)
		:m_StrProperties(p_Props),
		m_Session(p_Session)
	{
	}

	vector<rttr::property> GetPropertySet() const override
	{
		auto properties = m_RttrProperties;
		if (m_Session.IsUseRoleDelegation())
		{
			T dummy;
			auto& del = m_Session.GetRoleDelegationFromCache();
			for (const auto& propName : del.GetRequiredProperties(MFCUtil::convertASCII_to_UNICODE(dummy.get_type().get_name().to_string())))
			{
				const auto props = dummy.GetProperties(propName);
				for (const auto& prop : props)
				{
					const auto propName = Str::convertToASCII(prop);
					if (properties.end() == std::find_if(properties.begin(), properties.end(), [&propName](auto& p_Prop) {return p_Prop.get_name() == propName; }))
						properties.push_back(dummy.get_type().get_property(propName));
				}
			}
		}
		return properties;
	}

	vector<PooledString> GetStringPropertySet() const override
	{
		auto properties = m_StrProperties;
		if (m_Session.IsUseRoleDelegation())
		{
			T dummy;
			auto& del = m_Session.GetRoleDelegationFromCache();
			for (const auto& propName : del.GetRequiredProperties(MFCUtil::convertASCII_to_UNICODE(dummy.get_type().get_name().to_string())))
			{
				const auto props = dummy.GetProperties(propName);
				for (auto& prop : props)
				{
					if (properties.end() == std::find(properties.begin(), properties.end(), prop))
						properties.push_back(prop);
				}
			}
		}
		return properties;
	}

private:
	vector<rttr::property> m_RttrProperties;
	vector<PooledString> m_StrProperties;
	const Sapio365Session& m_Session;
};

template <>
class RbacRequiredPropertySet<BusinessUser>
{
	// Don't need this for User.
};

