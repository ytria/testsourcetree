#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "RoleDefinition.h"


namespace Sp
{
    class RoleDefinitionDeserializer : public JsonObjectDeserializer, public Encapsulate<Sp::RoleDefinition>
    {
    protected:
        void DeserializeObject(const web::json::object& p_Object) override;
    };
}

