#pragma once

#include "CosmosDbAccount.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

namespace Azure
{
	class CosmosDbAccountDeserializer : public JsonObjectDeserializer, public Encapsulate<Azure::CosmosDbAccount>
	{
	public:
		void DeserializeObject(const web::json::object& p_Object) override;
	};
}

