#pragma once

#include "BusinessObject.h"

#include "BasePermissions.h"

namespace Sp
{
    class RoleDefinition : public BusinessObject
    {
    public:
        RoleDefinition() = default;
        RoleDefinition(int32_t p_Id);

        boost::YOpt<PooledString> Description;
        boost::YOpt<bool> Hidden;
        boost::YOpt<int32_t> Id;
        boost::YOpt<PooledString> Name;
        boost::YOpt<int32_t> Order;
        boost::YOpt<int32_t> RoleTypeKind;
        boost::YOpt<Sp::BasePermissions> BasePermissions;

        // Not a "real" property
        boost::YOpt<PooledString> EditLink;

        bool operator<(const RoleDefinition& p_Other) const
        {
            return Id < p_Other.Id;
        }

    private:
        RTTR_ENABLE(BusinessObject)
            RTTR_REGISTRATION_FRIEND
    };
}

