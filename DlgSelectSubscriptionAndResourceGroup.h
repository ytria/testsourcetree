#pragma once

#include "OnOKOnCancelFixed.h"
#include "ResizableDialog.h"
#include "Resource.h"
#include "ResourceGroup.h"
#include "Subscription.h"
#include "YAdvancedHtmlView.h"

class Sapio365Session;

class DlgSelectSubscriptionAndResourceGroup : public ResizableDialog, public YAdvancedHtmlView::IPostedDataTarget
{
public:
	enum class Mode
	{
		CREATION,
		DELETION,
		SELECTION
	};

	DlgSelectSubscriptionAndResourceGroup(const Mode p_Mode, const map<Azure::Subscription, vector<Azure::ResourceGroup>>& p_Choices, std::shared_ptr<Sapio365Session> p_Session, CWnd* p_Parent);

	enum { IDD = IDD_DLG_NEW_COSMOS_ACCOUNT };

	/* YAdvancedHtmlView::IPostedDataTarget */
	bool OnNewPostedData(const IPostedDataTarget::PostedData& data) override;

	const wstring& GetSubscriptionId() const;
	const wstring& GetResGroupName() const;

	using OnOkValidation = std::function<bool(DlgSelectSubscriptionAndResourceGroup&)>;
	void SetOnOkValidation(OnOkValidation p_OnOkValid);

protected:
	virtual BOOL OnInitDialogSpecificResizable() override;

	ENDDIALOG_FIXED(ResizableDialog)

private:
	void generateJSONScriptData(wstring& p_Output);
	void AddDynamicFields(web::json::object& p_Object);
	
	const map<Azure::Subscription, vector<Azure::ResourceGroup>>& m_Choices;
	std::shared_ptr<Sapio365Session> m_Session;
	std::unique_ptr<YAdvancedHtmlView> m_HtmlView;

	const Mode m_Mode;
	wstring m_SubscriptionId;
	wstring m_ResGroupName;

	OnOkValidation m_OnOkValidation;

	static const wstring g_SubscriptionIdKey;
	static std::map<wstring, int, Str::keyLessInsensitive> g_Sizes;
};

