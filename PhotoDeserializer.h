#pragma once
#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "Photo.h"

class PhotoDeserializer : public JsonObjectDeserializer, public Encapsulate<Photo>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

