#pragma once

#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"
#include "Permission.h"

class PermissionDeserializer : public JsonObjectDeserializer, public Encapsulate<Permission>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

