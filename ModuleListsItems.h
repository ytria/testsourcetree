#pragma once

#include "ModuleBase.h"

#include "BusinessList.h"
#include "CommandInfo.h"

class FrameListItems;

class ModuleListsItems : public ModuleBase
{
public:
    using ModuleBase::ModuleBase;

private:
    virtual void executeImpl(const Command& p_Command) override;
	void showListsItems(const Command& p_Command);
	pplx::task<vector<BusinessListItem>> getListItemsWithAllFields(std::shared_ptr<BusinessObjectManager> p_BOM, BusinessList p_List, const PooledString& p_SiteId, const std::shared_ptr<IPageRequestLogger>& p_Logger, YtriaTaskData p_TaskData);

    vector<wstring> GetAllFields(BusinessList &p_List);

    void doRefresh(FrameListItems* p_NewFrame, const O365SubIdsContainer& p_Lists, Origin p_Origin, const Command& p_Command, bool p_IsRefresh);
};

