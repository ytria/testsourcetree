#include "BusinessChannel.h"
#include "Channel.h"

RTTR_REGISTRATION
{
    using namespace rttr;

	registration::class_<BusinessChannel>("Channel") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Channel"))))
		.constructor()(policy::ctor::as_object)
		;
}

BusinessChannel::BusinessChannel(const Channel& p_Channel)
{
    SetID(p_Channel.Id ? *p_Channel.Id : _YTEXT(""));
    SetDescription(p_Channel.Description);
	SetDisplayName(p_Channel.DisplayName);
}

const boost::YOpt<PooledString>& BusinessChannel::GetDescription() const
{
    return m_Description;
}

void BusinessChannel::SetDescription(const boost::YOpt<PooledString>& p_Val)
{
    m_Description = p_Val;
}

const boost::YOpt<PooledString>& BusinessChannel::GetDisplayName() const
{
	return m_DisplayName;
}

void BusinessChannel::SetDisplayName(const boost::YOpt<PooledString>& p_Val)
{
	m_DisplayName = p_Val;
}

bool BusinessChannel::operator<(const BusinessChannel& p_Other) const
{
	return GetID() < p_Other.GetID();
}

const boost::YOpt<PooledString>& BusinessChannel::GetEmail() const
{
	return m_Email;
}

void BusinessChannel::SetEmail(const boost::YOpt<PooledString>& p_Val)
{
	m_Email = p_Val;
}

const boost::YOpt<bool>& BusinessChannel::GetIsFavoriteByDefault() const
{
	return m_IsFavoriteByDefault;
}

void BusinessChannel::SetIsFavoriteByDefault(const boost::YOpt<bool>& p_Val)
{
	m_IsFavoriteByDefault = p_Val;
}

const boost::YOpt<PooledString>& BusinessChannel::GetWebUrl() const
{
	return m_WebUrl;
}

void BusinessChannel::SetWebUrl(const boost::YOpt<PooledString>& p_Val)
{
	m_WebUrl = p_Val;
}

const boost::YOpt<PooledString>& BusinessChannel::GetMembershipType() const
{
	return m_MembershipType;
}

void BusinessChannel::SetMembershipType(const boost::YOpt<PooledString>& p_Val)
{
	m_MembershipType = p_Val;
}

const boost::YOpt<BusinessDrive>& BusinessChannel::GetDrive() const
{
	return m_Drive;
}

void BusinessChannel::SetDrive(const boost::YOpt<BusinessDrive>& p_Val)
{
	m_Drive = p_Val;
}

const boost::YOpt<BusinessDriveItem>& BusinessChannel::GetFilesFolder() const
{
	return m_FilesFolder;
}

void BusinessChannel::SetFilesFolder(const boost::YOpt<BusinessDriveItem>& p_Val)
{
	m_FilesFolder = p_Val;
}

const boost::YOpt<vector<BusinessAADUserConversationMember>>& BusinessChannel::GetPrivateChannelMembers() const
{
	return m_PrivateChannelMembers;
}

void BusinessChannel::SetPrivateChannelMembers(const boost::YOpt<vector<BusinessAADUserConversationMember>>& p_Val)
{
	m_PrivateChannelMembers = p_Val;
}

void BusinessChannel::SetPrivateChannelMembersError(const boost::YOpt<SapioError>& p_Error)
{
	m_PrivateChannelMembersError = p_Error;
}

const boost::YOpt<SapioError>& BusinessChannel::GetPrivateChannelMembersError() const
{
	return m_PrivateChannelMembersError;
}

const boost::YOpt<PooledString>& BusinessChannel::GetGroupId() const
{
	return m_GroupId;
}

void BusinessChannel::SetGroupId(const boost::YOpt<PooledString>& p_Val)
{
	m_GroupId = p_Val;
}
