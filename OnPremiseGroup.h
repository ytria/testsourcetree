#pragma once

#include "BusinessObject.h"

class OnPremiseGroup : public BusinessObject
{
public:
	OnPremiseGroup();
	//wstring m_Item; // Property collection

	// Array properties
	const boost::YOpt<vector<PooledString>>& GetMember() const;
	void SetMember(const boost::YOpt<vector<PooledString>>& val);
	const boost::YOpt<vector<PooledString>>& GetMemberOf() const;
	void SetMemberOf(const boost::YOpt<vector<PooledString>>& val);
	const boost::YOpt<vector<PooledString>>& GetMembers() const;
	void SetMembers(const boost::YOpt<vector<PooledString>>& val);
	const boost::YOpt<vector<YTimeDate>>& GetDSCorePropagationData() const;
	void SetDSCorePropagationData(const boost::YOpt<vector<YTimeDate>>& val);
	
	// Int32 properties
	const boost::YOpt<int32_t>& GetAdminCount() const;
	void SetAdminCount(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetGroupType() const;
	void SetGroupType(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetInstanceType() const;
	void SetInstanceType(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetSAMAccountType() const;
	void SetSAMAccountType(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetSDRightsEffective() const;
	void SetSDRightsEffective(const boost::YOpt<int32_t>& val);
	const boost::YOpt<int32_t>& GetSystemFlags() const;
	void SetSystemFlags(const boost::YOpt<int32_t>& val);

	// String properties
	const boost::YOpt<PooledString>& GetCanonicalName() const;
	void SetCanonicalName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetCN() const;
	void SetCN(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetDescription() const;
	void SetDescription(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetDistinguishedName() const;
	void SetDistinguishedName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetGroupCategory() const;
	void SetGroupCategory(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetGroupScope() const;
	void SetGroupScope(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetHomePage() const;
	void SetHomePage(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetLastKnownParent() const;
	void SetLastKnownParent(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetManagedBy() const;
	void SetManagedBy(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetName() const;
	void SetName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetObjectCategory() const;
	void SetObjectCategory(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetObjectClass() const;
	void SetObjectClass(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetObjectGUID() const;
	void SetObjectGUID(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetObjectSid() const;
	void SetObjectSid(const boost::YOpt<PooledString>& p_Val);
	const boost::YOpt<PooledString>& GetSamAccountName() const;
	void SetSamAccountName(const boost::YOpt<PooledString>& val);
	const boost::YOpt<PooledString>& GetSID() const;
	void SetSID(const boost::YOpt<PooledString>& val);

	// DateTime properties
	const boost::YOpt<YTimeDate>& GetCreated() const;
	void SetCreated(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<YTimeDate>& GetCreateTimeStamp() const;
	void SetCreateTimeStamp(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<YTimeDate>& GetModified() const;
	void SetModified(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<YTimeDate>& GetModifyTimeStamp() const;
	void SetModifyTimeStamp(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<YTimeDate>& GetUSNChanged() const;
	void SetUSNChanged(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<YTimeDate>& GetUSNCreated() const;
	void SetUSNCreated(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<YTimeDate>& GetWhenChanged() const;
	void SetWhenChanged(const boost::YOpt<YTimeDate>& val);
	const boost::YOpt<YTimeDate>& GetWhenCreated() const;
	void SetWhenCreated(const boost::YOpt<YTimeDate>& val);

	// Bool properties
	const boost::YOpt<bool>& GetDeleted() const;
	void SetDeleted(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetIsCriticalSystemObject() const;
	void SetIsCriticalSystemObject(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetIsDeleted() const;
	void SetIsDeleted(const boost::YOpt<bool>& val);
	const boost::YOpt<bool>& GetProtectedFromAccidentalDeletion() const;
	void SetProtectedFromAccidentalDeletion(const boost::YOpt<bool>& val);

private:
	// Array properties
	boost::YOpt<vector<PooledString>> m_Member;
	boost::YOpt<vector<PooledString>> m_MemberOf;
	boost::YOpt<vector<PooledString>> m_Members;
	boost::YOpt<vector<YTimeDate>> m_DSCorePropagationData;

	boost::YOpt<bool> m_Deleted;
	boost::YOpt<bool> m_IsCriticalSystemObject;
	boost::YOpt<bool> m_IsDeleted;
	boost::YOpt<bool> m_ProtectedFromAccidentalDeletion;

	boost::YOpt<int32_t> m_AdminCount;
	boost::YOpt<int32_t> m_GroupType;
	boost::YOpt<int32_t> m_InstanceType;
	boost::YOpt<int32_t> m_SAMAccountType;
	boost::YOpt<int32_t> m_SDRightsEffective;
	boost::YOpt<int32_t> m_SystemFlags;

	boost::YOpt<PooledString> m_CanonicalName;
	boost::YOpt<PooledString> m_CN;
	boost::YOpt<PooledString> m_Description;
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<PooledString> m_DistinguishedName;
	boost::YOpt<PooledString> m_GroupCategory;
	boost::YOpt<PooledString> m_GroupScope;
	boost::YOpt<PooledString> m_HomePage;
	boost::YOpt<PooledString> m_LastKnownParent;
	boost::YOpt<PooledString> m_ManagedBy;
	boost::YOpt<PooledString> m_Name;
	boost::YOpt<PooledString> m_ObjectCategory;
	boost::YOpt<PooledString> m_ObjectClass;
	boost::YOpt<PooledString> m_ObjectGUID;
	boost::YOpt<PooledString> m_ObjectSid;
	boost::YOpt<PooledString> m_SamAccountName;
	boost::YOpt<PooledString> m_SID;

	boost::YOpt<YTimeDate> m_Created;
	boost::YOpt<YTimeDate> m_CreateTimeStamp;
	boost::YOpt<YTimeDate> m_Modified;
	boost::YOpt<YTimeDate> m_ModifyTimeStamp;
	boost::YOpt<YTimeDate> m_USNChanged;
	boost::YOpt<YTimeDate> m_USNCreated;
	boost::YOpt<YTimeDate> m_WhenChanged;
	boost::YOpt<YTimeDate> m_WhenCreated;

	friend class OnPremiseGroupsCollectionDeserializer;

	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
};

// only for identification issues in grids
// Finally unused.
//class HybridGroup : public BusinessObject
//{
//public:
//	using BusinessObject::BusinessObject;
//
//private:
//	RTTR_ENABLE(BusinessObject)
//};
