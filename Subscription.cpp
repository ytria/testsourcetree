#include "Subscription.h"

bool Azure::Subscription::operator<(const Azure::Subscription& p_Other) const
{
	bool smallerThan = false;
	if (m_SubscriptionId != boost::none && p_Other.m_SubscriptionId != boost::none)
		smallerThan = *m_SubscriptionId < *p_Other.m_SubscriptionId;

	return smallerThan;
}
