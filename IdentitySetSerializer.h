#pragma once

#include "IJsonSerializer.h"
#include "IdentitySet.h"

class IdentitySetSerializer : public IJsonSerializer
{
public:
	IdentitySetSerializer(const IdentitySet& p_IdentitySet);
	void Serialize() override;

private:
	IdentitySet m_IdentitySet;
};

