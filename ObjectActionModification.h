#pragma once

#include "RowModification.h"

class O365Grid;

class ObjectActionModification : public RowModification
{
protected:
	// Empty statusText with use the default of the grid.
	ObjectActionModification(O365Grid& p_Grid, const row_pk_t& p_RowKey, bool p_RowStateIsDeletedInsteadOfModified, const wstring& statusText);

public:
	~ObjectActionModification();

	// Pure virtual class due to base classes.
	// To be implemented in derived class
	/*
	// IMainItemModification
	virtual void RefreshState() = 0;
	// Modification
	virtual wstring GetModificationActionNameForLog() const = 0;// for user activity database: object key (e.g. row pk, not graph ID)
	*/

private:
	const bool m_RowStateIsDeletedInsteadOfModified;
};
