#pragma once

// TODO integrate with dynamic grid renderer

#include "GridBackendUtil.h"

class O365Grid;
class GridTemplate
{
public:
	GridTemplate();

	GridBackendField GetMetaIDField(const vector<PooledString>& p_MetaID);
	GridBackendField GetMetaIDField(const PooledString& p_MetaID);

	virtual void CustomizeGrid(O365Grid& p_Grid);

	bool IsO365Metatype(const GridBackendRow& p_Row) const;
	bool IsOnPremMetatype(const GridBackendRow& p_Row) const;
	bool IsHybridMetatype(const GridBackendRow& p_Row) const;
	void SetO365Metatype(GridBackendRow& p_Row);
	void SetOnPremMetatype(GridBackendRow& p_Row);
	void SetHybridMetatype(GridBackendRow& p_Row);

	enum class UpdateFieldOption
	{
		NONE = 0,
		IS_MODIFICATION,
		IS_OBJECT_CREATION
	};

	static bool hasValidErrorlessValue(const GridBackendField& p_Field);
	static bool isError(const GridBackendField& p_Field);

protected:
	static bool g_Initialized;

	GridBackendField& AddDateOrNeverField(const boost::YOpt<YTimeDate>& p_Date, GridBackendRow& p_Row, GridBackendColumn& p_Col);

	int m_O365IconId;
	int m_OnPremIconId;
	int m_HybridIconId;

public:
	static PooledString g_TypeGroup;
	static PooledString g_TypeUser;

	GridBackendColumn* m_ColumnID;
	GridBackendColumn* m_ColumnMetaID;
	GridBackendColumn* m_ColumnMetaType;
	GridBackendColumn* m_ColumnDisplayName;
};