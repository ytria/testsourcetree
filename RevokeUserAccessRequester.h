#pragma once

#include "IRequester.h"

class SingleRequestResult;

class RevokeUserAccessRequester : public IRequester
{
public:
	RevokeUserAccessRequester(wstring p_UserId, wstring p_NameOrIdForLogging);

	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const wstring& GetUserId() const;
	const SingleRequestResult& GetResult() const;

private:
	wstring m_UserId;
	wstring m_NameOrIdForLogging;
	std::shared_ptr<SingleRequestResult> m_Result;
};
