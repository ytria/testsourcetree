#pragma once

#include "BaseO365Grid.h"
#include "BusinessPost.h"
#include "ModuleUtil.h"

class FramePosts;
class MessageBodyViewer;

class GridPosts : public ModuleO365Grid<BusinessPost>
{
public:
    GridPosts();
    ~GridPosts();

    virtual ModuleCriteria GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows) override;

    void BuildView(const O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>>& p_Posts, const vector<O365UpdateOperation>& p_Updates, bool p_Refresh, bool p_FullPurge);
	void BuildViewWithAttachments(const O365DataMap<ModuleUtil::PostMetaInfo, vector<BusinessPost>>& p_Posts, const vector<O365UpdateOperation>& p_Updates, const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_Refresh, bool p_FullPurge);
     
    O365SubSubIdsContainer GetRefreshInfos();
    void ViewAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, bool p_RefreshModificationStates, bool p_ShowPostUpdateError);
	std::set<ModuleUtil::PostMetaInfo> GetAttachmentsRequestInfo(const std::vector<GridBackendRow *>& rowsWithMoreLoaded, const O365IdsContainer& p_PostsIdsFilter);
    void ApplyDeleteAttachments(bool p_Selected);

    const map<PooledString, PooledString>& GetThreadsTopics() const;
    void SetThreadsTopics(const map<PooledString, PooledString>& p_ThreadsTopics);

    void SetShowInlineAttachments(const bool& p_Val);

	void HideMessageViewer();
	
	virtual wstring GetName(const GridBackendRow* p_Row) const override;
	
protected:
    virtual BusinessPost getBusinessObject(GridBackendRow* p_Row) const;
	virtual void customizeGrid() override;
	virtual void customizeGridPostProcess() override;

private:
    void loadMoreImpl(const std::vector<GridBackendRow*>& p_Rows);
    virtual void OnGbwSelectionChangedSpecific() override;
    
    afx_msg void OnShowPostAttachments();
    afx_msg void OnDownloadPostAttachments();
    afx_msg void OnViewPostItemAttachment();
    afx_msg void OnDeleteAttachment();
    afx_msg void OnToggleInlineAttachments();
    afx_msg void OnUpdateShowPostAttachments(CCmdUI* pCmdUI);
    afx_msg void OnUpdateDownloadPostAttachments(CCmdUI* pCmdUI);
    afx_msg void OnUpdatePostItemAttachment(CCmdUI* pCmdUI);
    afx_msg void OnUpdateDeleteAttachment(CCmdUI* pCmdUI);
    afx_msg void OnUpdateToggleInlineAttachments(CCmdUI* pCmdUI);
    DECLARE_MESSAGE_MAP()

	virtual BOOL PreTranslateMessage(MSG* pMsg) override;

    virtual void InitializeCommands() override;
    virtual void OnCustomPopupMenu(CExtPopupMenuWnd * pPopup, int nStart) override;

    GridBackendRow* fillRow(const BusinessPost& p_Post, const ModuleUtil::PostMetaInfo& p_PostInfo);

    void updateRow(const BusinessPost& p_Post, GridBackendRow * p_PostRow);

    afx_msg void OnShowBody();

    afx_msg void OnUpdateShowBody(CCmdUI* pCmdUI);

	void loadAttachments(const ModuleBase::AttachmentsContainer& p_BusinessAttachments, GridUpdater& updater);
	void fillAttachmentFields(GridBackendRow* p_Row, const vector<BusinessAttachment>& p_Attachments);

	virtual void onPostExplodeMultiValueSisterhood(GridBackendRow* p_Row, const std::set<GridBackendColumn*>& p_Columns) override;
	static Recipient extractRecipient(GridBackendRow* p_Row, GridBackendColumn* p_ColAddress, GridBackendColumn* p_ColName);

private:
    DECLARE_CacheGrid_DeleteRowLParam

    std::unique_ptr<MessageBodyViewer> m_MessageViewer;

    GridBackendColumn* m_ColMetaId; // Tech hide
    GridBackendColumn* m_ColMetaGroupName;
    GridBackendColumn* m_ColMetaGroupId; // Tech hide
    GridBackendColumn* m_ColMetaDisplayName;
    GridBackendColumn* m_ColCreatedDateTime;
    GridBackendColumn* m_ColLastModifiedDateTime;
    GridBackendColumn* m_ColChangeKey; // Tech hide
    GridBackendColumn* m_ColCategories;
    GridBackendColumn* m_ColBodyContentType;
	GridBackendColumn* m_ColBodyPreview;
	GridBackendColumn* m_ColReceivedDateTime;
    GridBackendColumn* m_ColHasAnyAttachment;
    GridBackendColumn* m_ColHasGraphAttachment;
    GridBackendColumn* m_ColFromName;
    GridBackendColumn* m_ColFromAddress;
    GridBackendColumn* m_ColSenderName;
    GridBackendColumn* m_ColSenderAddress;
    GridBackendColumn* m_ColConversationThreadId; // Tech hide
    GridBackendColumn* m_ColNewParticipantsName;
    GridBackendColumn* m_ColNewParticipantsAddress;
    //GridBackendColumn* m_ColConversationId; // Tech hide

    GridBackendColumn* m_ColAttachmentType;
    GridBackendColumn* m_ColAttachmentId;
    GridBackendColumn* m_ColAttachmentName;
	GridBackendColumn* m_ColAttachmentNameExtOnly;
    GridBackendColumn* m_ColAttachmentContentType;
    GridBackendColumn* m_ColAttachmentSize;
    GridBackendColumn* m_ColAttachmentLastModifiedDateTime;
    GridBackendColumn* m_ColAttachmentIsInline;

    FramePosts*				m_FramePosts;

    GridUtil::AttachmentIcons m_Icons;

    bool m_ShowInlineAttachments;
    ModuleBase::AttachmentsContainer m_AllAttachments;

	HACCEL m_hAccelSpecific = nullptr;

    map<PooledString, PooledString> m_ThreadsTopics;

    friend class AttachmentInfo;
    friend class PostAttachmentDeletion;
	friend class MessageBodyViewer;
};
