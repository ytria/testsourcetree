#pragma once

#include "BaseO365Grid.h"
#include "EducationSchool.h"
#include "EducationUser.h"

class FrameClassMembers;

struct MemberAddition
{
	vector<GridBackendField> m_RowPk;
	wstring m_ClassId;
	wstring m_UserId;
};

struct MemberRemoval
{
	vector<GridBackendField> m_RowPk;
	wstring m_ClassId;
	wstring m_UserId;
};

struct ClassMembersChanges
{
	vector<MemberAddition> m_MemberAdditions;
	vector<MemberRemoval> m_MemberRemovals;
};

class GridClassMembers : public ModuleO365Grid<EducationClass>
{
public:
	GridClassMembers();
	virtual ~GridClassMembers() override;
	void BuildView(const vector<EducationSchool>& p_Schools, const vector<O365UpdateOperation>& p_Updates,bool p_FullPurge);
	
	ClassMembersChanges GetChanges();
	ClassMembersChanges GetChanges(const vector<GridBackendRow*>& p_Rows);

	void AddMemberMod(const CreatedObjectModification& addMemberMod, ClassMembersChanges& changes);
	void RemoveMemberMod(const DeletedObjectModification& removeMemberMod, ClassMembersChanges& changes);

	wstring GetName(const GridBackendRow* p_Row) const override;
	virtual row_pk_t UpdateCreatedModification(row_pk_t rowPk, const wstring& creationResultBody) override;

protected:
	void customizeGrid() override;
	EducationClass	getBusinessObject(GridBackendRow*) const override;

private:
	afx_msg void OnAddMember();

	afx_msg void OnUpdateAddMember(CCmdUI* pCmdUI);
	afx_msg void OnRemoveMember();
	afx_msg void OnUpdateRemoveMember(CCmdUI* pCmdUI);

	bool HasUserSelected();
	bool HasClassOrUserSelected();

	DECLARE_MESSAGE_MAP()

	void InitializeCommands() override;
	void OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart) override;

	void FillEducationUserFields(GridBackendRow* p_UserRow, const EducationUser& p_EducationUser);

	set<GridBackendRow*> GetSelectedClassRows() const;
	GridBackendRow* CreateNewUserRow(PooledString p_SchoolId, PooledString p_ClassId, const SelectedItem& p_User);
	vector<GridBackendField> CreatePk(PooledString p_SchoolId, PooledString p_ClassId, PooledString p_UserId) const;
	vector<BusinessUser> GetUsersInGrid();

	GridBackendColumn* m_ColDisplayName;
	GridBackendColumn* m_ColSchoolId;
	GridBackendColumn* m_ColClassId;
	// Education user columns

	// Simple columns
	GridBackendColumn* m_ColExternalSource;
	GridBackendColumn* m_ColMiddleName;
	GridBackendColumn* m_ColPrimaryRole;
	GridBackendColumn* m_ColRelatedContacts;

	// Created by
	GridBackendColumn* m_ColCreatedByAppDisplayName;
	GridBackendColumn* m_ColCreatedByAppId;
	GridBackendColumn* m_ColCreatedByAppEmail;
	GridBackendColumn* m_ColCreatedByAppIdentityProvider;
	GridBackendColumn* m_ColCreatedByDeviceDisplayName;
	GridBackendColumn* m_ColCreatedByDeviceId;
	GridBackendColumn* m_ColCreatedByDeviceEmail;
	GridBackendColumn* m_ColCreatedByDeviceIdentityProvider;
	GridBackendColumn* m_ColCreatedByUserDisplayName;
	GridBackendColumn* m_ColCreatedByUserId;
	GridBackendColumn* m_ColCreatedByUserEmail;
	GridBackendColumn* m_ColCreatedByUserIdentityProvider;

	// Mailing address
	GridBackendColumn* m_ColMailingAddressCity;
	GridBackendColumn* m_ColMailingAddressCountryOrRegion;
	GridBackendColumn* m_ColMailingAddressPostalCode;
	GridBackendColumn* m_ColMailingAddressState;
	GridBackendColumn* m_ColMailingAddressStreet;

	// Related contacts
	GridBackendColumn* m_ColRelatedContactId;
	GridBackendColumn* m_ColRelatedContactDisplayName;
	GridBackendColumn* m_ColRelatedContactEmailAddress;
	GridBackendColumn* m_ColRelatedContactMobilePhone;
	GridBackendColumn* m_ColRelatedContactRelationship;
	GridBackendColumn* m_ColRelatedContactAccessContent;

	// Education student
	GridBackendColumn* m_ColStudentBirthDate;
	GridBackendColumn* m_ColStudentExternalId;
	GridBackendColumn* m_ColStudentGender;
	GridBackendColumn* m_ColStudentGrade;
	GridBackendColumn* m_ColStudentGraduationYear;
	GridBackendColumn* m_ColStudentNumber;

	// Education teacher
	GridBackendColumn* m_ColTeacherExternalId;
	GridBackendColumn* m_ColTeacherNumber;

	friend class FrameClassMembers;
};

