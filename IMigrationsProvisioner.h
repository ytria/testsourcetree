#pragma once

#include "IMigrationVersion.h"

class IMigrationsProvisioner
{
public:
	virtual ~IMigrationsProvisioner() = default;
	virtual void Provision() = 0;

	const std::vector<std::shared_ptr<IMigrationVersion>>& GetMigrations() const;

protected:
	std::vector<std::shared_ptr<IMigrationVersion>> m_Migrations;
};

