#include "DlgSelectRoleSessionHTML.h"

#include "MainFrame.h"
#include "Str.h"
#include "TimeUtil.h"
#include "JSONUtil.h"
#include "RoleDelegationManager.h"

const wstring DlgSelectRoleSessionHTML::g_StandardSessionBtn	= _YTEXT("__YtriaStandardSession");
const wstring DlgSelectRoleSessionHTML::g_OptionSeeOnlyMineBtn	= _YTEXT("__YtriaOptionOnlyMine");

BEGIN_MESSAGE_MAP(DlgSelectRoleSessionHTML, ResizableDialog)
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()

std::map<wstring, int, Str::keyLessInsensitive> DlgSelectRoleSessionHTML::g_HbsHeights;

DlgSelectRoleSessionHTML::DlgSelectRoleSessionHTML(const vector<RoleDelegation>& p_Roles, CWnd* p_Parent)
	: ResizableDialog(IDD)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_totalHbsHeight(0)
	, m_Action(Action::LoadStandard)
	, m_OptionHideUnscoped(false)
	, m_MaxWidth(0)
	, m_IsAddRole(false)
    , m_Roles(p_Roles)
    , m_SelectedRoleId(0)
{
	// Load hbs sizes
	if (g_HbsHeights.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_SESSIONLIST_SIZE, g_HbsHeights);
		ASSERT(success);
	}
}

DlgSelectRoleSessionHTML::DlgSelectRoleSessionHTML(const vector<RoleDelegation>& p_Roles, CWnd* p_Parent, const bool p_IsAddrRole)
    :DlgSelectRoleSessionHTML(p_Roles, p_Parent)
{
    m_IsAddRole = p_IsAddrRole;
}

bool DlgSelectRoleSessionHTML::HasAnyChoice() const
{
    return !m_Roles.empty();
}

bool DlgSelectRoleSessionHTML::GetHideUnscoped() const
{
    return m_OptionHideUnscoped;
}

int64_t DlgSelectRoleSessionHTML::GetSelectedRole() const
{
    return m_SelectedRoleId;
}

DlgSelectRoleSessionHTML::Action DlgSelectRoleSessionHTML::GetSelectedAction() const
{
    return m_Action;
}

bool DlgSelectRoleSessionHTML::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
    auto dataCopy = data;

    bool ok				= false;
    auto onlyMineItt	= dataCopy.find(g_OptionSeeOnlyMineBtn);
    auto stdSessionItt	= dataCopy.find(g_StandardSessionBtn);

    m_OptionHideUnscoped	= onlyMineItt != dataCopy.end() && onlyMineItt->second == g_OptionSeeOnlyMineBtn;
    m_Action				= stdSessionItt != dataCopy.end() ? Action::LoadStandard : Action::LoadRole;

    if (onlyMineItt != dataCopy.end())
        dataCopy.erase(onlyMineItt);
    if (stdSessionItt != dataCopy.end())
        dataCopy.erase(stdSessionItt);
    
    if (m_Action == Action::LoadRole)
    {
        ASSERT(dataCopy.size() == 1);
        if (dataCopy.size() == 1)
        {
            m_SelectedRoleId = stoll(dataCopy.begin()->first);
            ok = true;
            endDialogFixed(IDOK);
        }
    }
    else if (m_Action == Action::LoadStandard)
    {
        ASSERT(dataCopy.empty());
        if (dataCopy.empty())
        {
            endDialogFixed(IDOK);
            ok = true;
        }
    }

    return !ok;
}

BOOL DlgSelectRoleSessionHTML::OnInitDialogSpecificResizable()
{
	SetWindowText(YtriaTranslate::Do(DlgSelectRoleSessionHTML_OnInitDialogSpecificResizable_1, _YLOC("Role Selection")).c_str());

	CWnd* pCtrl = GetDlgItem(IDC_GRID_PLACEHOLDER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, NULL);
	m_HtmlView->SetPostedDataTarget(this);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
		{
			 { _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
			,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-sessionList.js"), _YTEXT("") } } // This file is in resources
			,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("sessionList.js"), _YTEXT("") } }, // This file is in resources
		}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("sessionList.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	const int height = [=]()
	{
		const int maxHeight = [=]()
		{
			HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
			MONITORINFO info;
			info.cbSize = sizeof(MONITORINFO);
			GetMonitorInfo(monitor, &info);
			return 2 * (info.rcMonitor.bottom - info.rcMonitor.top) / 3;
		}();

		ASSERT(0 != m_totalHbsHeight);
		if (0 != m_totalHbsHeight)
		{
			{
				const auto it = g_HbsHeights.find(_YTEXT("marginTop"));
				if (g_HbsHeights.end() != it)
					m_totalHbsHeight += it->second;
			}
			{
				const auto it = g_HbsHeights.find(_YTEXT("marginBottom"));
				if (g_HbsHeights.end() != it)
					m_totalHbsHeight += it->second;
			}
			const int deltaH = HIDPI_YH(m_totalHbsHeight) - htmlRect.Height();
			return min(maxHeight, dlgRect.Height() + deltaH);
		}

		return maxHeight;
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_HbsHeights.find(_YTEXT("min-width-Window"));
			ASSERT(g_HbsHeights.end() != it);
			if (g_HbsHeights.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();
		const int delta = targetHtmlWidth - htmlRect.Width();
		return dlgRect.Width() + delta;
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);

	// Limit width to avoid erratic behavior that appears beyond this width.
	m_MaxWidth = HIDPI_XW(1040);

	BlockVResize(true);

    return TRUE;
}

void DlgSelectRoleSessionHTML::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI)
{
	ResizableDialog::OnGetMinMaxInfo(lpMMI);

	lpMMI->ptMaxSize.x = m_MaxWidth;
	lpMMI->ptMaxTrackSize.x = m_MaxWidth;
}

void DlgSelectRoleSessionHTML::generateJSONScriptData(wstring& p_Output)
{    
	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	const PersistentSession& currentSession = nullptr != mainFrame ? mainFrame->GetSessionInfo() : PersistentSession();
	const wstring type = currentSession.GetSessionType();

	m_totalHbsHeight = 0;

    web::json::value main = web::json::value::object({
		{ _YTEXT("optionField"), web::json::value::string(g_OptionSeeOnlyMineBtn) },
		{ _YTEXT("optionVal"), web::json::value::string(g_OptionSeeOnlyMineBtn) },
		{ _YTEXT("optionLabel"), web::json::value::string(YtriaTranslate::Do(DlgSelectRoleSessionHTML_generateJSONScriptData_3, _YLOC("Option: Hide items outside the scope of this role.")).c_str()) },
		{ _YTEXT("optionLabelSecondLine"), web::json::value::string(YtriaTranslate::Do(DlgSelectRoleSessionHTML_generateJSONScriptData_4, _YLOC("(This will hide the items that fall outside the scope of this role.)")).c_str()) },
		{ _YTEXT("isRoleSelect"), web::json::value::string(_YTEXT("X")) },
		{ _YTEXT("isWithCancel"), m_IsAddRole ? web::json::value::string(_YTEXT("X")) : web::json::value::string(_YTEXT("")) },
		{ _YTEXT("method"), web::json::value::string(_YTEXT("POST")) },
        { _YTEXT("action"), web::json::value::string(_YTEXT("#")) },
    });

    web::json::value items = web::json::value::array();


	int itemIndex = 0;
    
	items[itemIndex++] = 
        JSON_BEGIN_OBJECT
            JSON_BEGIN_PAIR
                _YTEXT("hbs"),  JSON_STRING(m_IsAddRole ? _YTEXT("ExitSession") : _YTEXT("RegularSession"))
            JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("type"), JSON_STRING(type)
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("btnValue"), JSON_STRING(g_StandardSessionBtn)
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("label"), JSON_STRING(m_IsAddRole ? MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str()) : MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(Product_s_STRING_UPDATE_BUTTON_LAUNCH_UPDATE_1, _YLOC("Continue")).c_str()))
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("desc"), JSON_STRING(m_IsAddRole ? _YTEXT("") : MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSelectRoleSessionHTML_generateJSONScriptData_5, _YLOC("Your usual credentials will be used for the session.")).c_str()) )
			JSON_END_PAIR,
			JSON_BEGIN_PAIR
				_YTEXT("introrole"), JSON_STRING(MFCUtil::encodeToHtmlEntitiesExceptTags(YtriaTranslate::Do(DlgSelectRoleSessionHTML_generateJSONScriptData_6, _YLOC("Select a role:")).c_str()))
			JSON_END_PAIR,
        JSON_END_OBJECT;

	m_totalHbsHeight += getHbsHeight(_YTEXT("SessionRegular"));

    for (const auto& role : m_Roles)
		items[itemIndex++] = getJSONRoleEntry(role);

	const web::json::value root = web::json::value::object({
        { _YTEXT("main"), main },
        { _YTEXT("items"), items}
    });

	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(root.serialize());
}

web::json::value DlgSelectRoleSessionHTML::getJSONRoleEntry(const RoleDelegation& p_Role)
{
    web::json::value entry = web::json::value::object();
    entry[_YTEXT("hbs")] = web::json::value::string(_YTEXT("SessionCard"));
    entry[_YTEXT("sessionName")] = web::json::value::string(p_Role.GetDelegation().m_Name);
	entry[_YTEXT("btnValue")] = web::json::value::string(std::to_wstring(p_Role.GetID()));
	entry[_YTEXT("type")] = web::json::value::string(_YTEXT("role_select"));
	entry[_YTEXT("roledesc")] = web::json::value::string(MFCUtil::encodeToHtmlEntitiesExceptTags(p_Role.GetDelegation().m_Info));

	m_totalHbsHeight += getHbsHeight(_YTEXT("SessionCard"));

	return entry;
}

UINT DlgSelectRoleSessionHTML::getHbsHeight(const wstring& hbsName)
{
	auto it = g_HbsHeights.find(hbsName);
	ASSERT(g_HbsHeights.end() != it);
	if (g_HbsHeights.end() != it)
		return it->second;
	return 0;
}
