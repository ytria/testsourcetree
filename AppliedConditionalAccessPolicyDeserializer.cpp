#include "AppliedConditionalAccessPolicyDeserializer.h"
#include "JsonSerializeUtil.h"
#include "ListDeserializer.h"

void AppliedConditionalAccessPolicyDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	{
		ListStringDeserializer d;
		JsonSerializeUtil::DeserializeAny(d, _YTEXT("enforcedGrantControls"), p_Object);
		m_Data.m_EnforcedGrantControls = d.GetData();
	}
	{
		ListStringDeserializer d;
		JsonSerializeUtil::DeserializeAny(d, _YTEXT("enforcedSessionControls"), p_Object);
		m_Data.m_EnforcedSessionControls = d.GetData();
	}
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	JsonSerializeUtil::DeserializeString(_YTEXT("result"), m_Data.m_Result, p_Object);
}
