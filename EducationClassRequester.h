#pragma once

#include "IRequester.h"

class EducationClassDeserializer;
class EducationClass;

class EducationClassRequester : public IRequester
{
public:
	pplx::task<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;

	const EducationClass& GetData() const;

private:
	std::shared_ptr<EducationClassDeserializer> m_Deserializer;
	wstring m_ClassId;
};

