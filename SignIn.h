#pragma once

#include "BusinessObject.h"

#include "AppliedConditionalAccessPolicy.h"
#include "DeviceDetail.h"
#include "SignInLocation.h"
#include "SignInStatus.h"

class SignIn : public BusinessObject
{
public:
	boost::YOpt<PooledString> m_AppDisplayName;
	boost::YOpt<PooledString> m_AppId;
	boost::YOpt<AppliedConditionalAccessPolicy> m_AppliedConditionalAccessPolicy;
	boost::YOpt<PooledString> m_ClientAppUsed;
	boost::YOpt<PooledString> m_ConditionalAccessStatus;
	boost::YOpt<PooledString> m_CorrelationId;
	boost::YOpt<YTimeDate>	  m_CreatedDateTime;
	boost::YOpt<DeviceDetail> m_DeviceDetail;
	boost::YOpt<PooledString> m_IpAddress;
	boost::YOpt<bool>		  m_IsInteractive;
	boost::YOpt<SignInLocation> m_Location;
	boost::YOpt<PooledString> m_ResourceDisplayName;
	boost::YOpt<PooledString> m_ResourceId;
	boost::YOpt<PooledString> m_RiskDetail;
	boost::YOpt<PooledString> m_RiskEventTypes;
	boost::YOpt<PooledString> m_RiskLevelAggregated;
	boost::YOpt<PooledString> m_RiskLevelDuringSignIn;
	boost::YOpt<PooledString> m_RiskState;
	boost::YOpt<SignInStatus> m_Status;
	boost::YOpt<PooledString> m_UserDisplayName;
	boost::YOpt<PooledString> m_UserId;
	boost::YOpt<PooledString> m_UserPrincipalName;

	RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
};

