#include "GridSites.h"

#include "AutomationWizardSites.h"
#include "BasicGridSetup.h"
#include "BusinessGroup.h"
#include "BusinessGroupConfiguration.h"
#include "BusinessSiteConfiguration.h"
#include "CommandInfo.h"
#include "FrameSites.h"
#include "GridUtil.h"
#include "O365AdminUtil.h"
#include "GridUpdater.h"
#include "GraphCache.h"
#include "UpdatedObjectsGenerator.h"
#include "BusinessSite.h"

BEGIN_MESSAGE_MAP(GridSites, ModuleO365Grid<BusinessSite>)
	ON_COMMAND(ID_SITESGRID_LOADMORE,			OnSiteLoadMore)
	ON_UPDATE_COMMAND_UI(ID_SITESGRID_LOADMORE, OnUpdateSiteLoadMore)

	NEWMODULE_COMMAND(ID_SITESGRID_SHOWDRIVEITEMS, ID_SITESGRID_SHOWDRIVEITEMS_FRAME, ID_SITESGRID_SHOWDRIVEITEMS_NEWFRAME,		OnShowDriveItems,	OnUpdateShowDriveItems)
	NEWMODULE_COMMAND(ID_SITESGRID_SHOWLISTS, ID_SITESGRID_SHOWLISTS_FRAME, ID_SITESGRID_SHOWLISTS_NEWFRAME,					OnShowLists,		OnUpdateShowLists)
    //NEWMODULE_COMMAND(ID_SITESGRID_SHOWROLES, ID_SITESGRID_SHOWROLES_FRAME, ID_SITESGRID_SHOWROLES_NEWFRAME,					OnShowRoles,		OnUpdateShowRoles)
    NEWMODULE_COMMAND(ID_SITESGRID_SHOWPERMISSIONS, ID_SITESGRID_SHOWPERMISSIONS_FRAME, ID_SITESGRID_SHOWPERMISSIONS_NEWFRAME,	OnShowPermissions,	OnUpdateShowPermissions)
END_MESSAGE_MAP()

GridSites::GridSites(Origin p_Origin)
	: m_ColCreatedDateTime(nullptr)
	, m_ColDescription(nullptr)
	, m_ColDisplayName(nullptr)
	, m_ColLastModifiedDateTime(nullptr)
	, m_ColName(nullptr)
	, m_ColRoot(nullptr)
	, m_ColWebUrl(nullptr)
	, m_ColHostName(nullptr)
    , m_ColDataLocationCode(nullptr)
	, m_ColSharepointListID(nullptr)
	, m_ColSharepointListItemID(nullptr)
	, m_ColSharepointListItemUniqueID(nullptr)
	, m_ColSharepointSiteURL(nullptr)
	, m_ColSharepointSiteID(nullptr)
	, m_ColSharepointWebID(nullptr)
	, m_Frame(nullptr)
	, m_HasLoadMoreAdditionalInfo(false)
	, m_ColDriveName(nullptr)
	, m_ColDriveId(nullptr)
	, m_ColDriveDescription(nullptr)
	, m_ColDriveQuotaState(nullptr)
	, m_ColDriveQuotaUsed(nullptr)
	, m_ColDriveQuotaRemaining(nullptr)
	, m_ColDriveQuotaDeleted(nullptr)
	, m_ColDriveQuotaTotal(nullptr)
	, m_ColDriveCreationTime(nullptr)
	, m_ColDriveLastModifiedDateTime(nullptr)
	, m_ColDriveType(nullptr)
	, m_ColDriveWebUrl(nullptr)
	, m_ColDriveCreatedByUserEmail(nullptr)
	, m_ColDriveCreatedByUserId(nullptr)
	, m_ColDriveCreatedByAppName(nullptr)
	, m_ColDriveCreatedByAppId(nullptr)
	, m_ColDriveCreatedByDeviceName(nullptr)
	, m_ColDriveCreatedByDeviceId(nullptr)
	, m_ColDriveOwnerUserName(nullptr)
	, m_ColDriveOwnerUserId(nullptr)
	, m_ColDriveOwnerUserEmail(nullptr)
	, m_ColDriveOwnerAppName(nullptr)
	, m_ColDriveOwnerAppId(nullptr)
	, m_ColDriveOwnerDeviceName(nullptr)
	, m_ColDriveOwnerDeviceId(nullptr)
{
    //UseDefaultActions(/*O365Grid::ACTION_CREATE | */O365Grid::ACTION_DELETE | O365Grid::ACTION_EDIT/* | O365Grid::ACTION_VIEW*/);

	m_Origin = p_Origin;
	// FIXME: Do we want to separate both wizards?
	ASSERT(m_Origin == Origin::Group || m_Origin == Origin::Tenant || m_Origin == Origin::Channel);
	if (m_Origin == Origin::Group)
	{
		m_TemplateGroups = std::make_unique<GridTemplateGroups>();
		initWizard<AutomationWizardSites>(_YTEXT("Automation\\Sites"));
	}
	else if (m_Origin == Origin::Channel)
	{
		m_ChannelMetaColumns = std::make_unique<ChannelMetaColumns>();
		initWizard<AutomationWizardSites>(_YTEXT("Automation\\Sites"));
	}
	else
	{
		initWizard<AutomationWizardSites>(_YTEXT("Automation\\Sites"));
	}
}

void GridSites::customizeGrid()
{
	m_hAccelSpecific = ::LoadAccelerators(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_GRIDSITES_ACCELERATOR));

	m_MayContainUnscopedUserGroupOrSite = GetOrigin() == Origin::Tenant;

	static const wstring g_FamilyGroup = YtriaTranslate::Do(GridSites_customizeGrid_1, _YLOC("Group")).c_str();
	static const wstring g_FamilyChannel = _T("Channel");

	if (m_TemplateGroups)
	{
		BasicGridSetup setup(GridUtil::g_AutoNameSites, LicenseUtil::g_codeSapio365sites,
		{
			{ &m_TemplateGroups->m_ColumnDisplayName, YtriaTranslate::Do(GridSites_customizeGrid_2, _YLOC("Group Display Name")).c_str(), g_FamilyGroup },
			{ nullptr, _YTEXT(""), _YTEXT("") },
			{ &m_TemplateGroups->m_ColumnID, YtriaTranslate::Do(GridSites_customizeGrid_3, _YLOC("Group ID")).c_str(), g_FamilyGroup }
		});
		setup.Setup(*this, false);

		// There's only one site per group, one sub-site per site, etc.
		//AddColumnForRowPK(m_TemplateGroups->m_ColumnID);
	}
	else if (m_ChannelMetaColumns)
	{
		BasicGridSetup setup(GridUtil::g_AutoNameSites, LicenseUtil::g_codeSapio365sites);
		setup.AddMetaColumn({ &m_ChannelMetaColumns->m_ColumnTeamID, _T("Team ID"), g_FamilyChannel, _YUID("teamId") });
		setup.AddMetaColumn({ &m_ChannelMetaColumns->m_ColumnChannelID, _T("Channel ID"), g_FamilyChannel, _YUID("channelId") });
		setup.Setup(*this, false);

		m_ChannelMetaColumns->m_ColumnTeamDisplayName = AddColumn(_YUID("teamDisplayName"), _T("Team Display Name"), g_FamilyChannel, g_ColumnSize12, { g_ColumnsPresetDefault });
		m_ChannelMetaColumns->m_ColumnChannelDisplayName = AddColumn(_YUID("channelDisplayName"), _T("Channel Display Name"), g_FamilyChannel, g_ColumnSize12, { g_ColumnsPresetDefault });

		// There's only one site per channel, one sub-site per site, etc.
		//AddColumnForRowPK(m_TemplateGroups->m_ColumnID);
	}
	else
	{
		BasicGridSetup setup(GridUtil::g_AutoNameSites, LicenseUtil::g_codeSapio365sites);
		setup.Setup(*this, false);
	}

	const auto& scfg = BusinessSiteConfiguration::GetInstance();

	if (m_TemplateGroups)
	{
		m_TemplateGroups->m_ColumnIsTeam = AddColumnIcon(_YUID("meta.isTeam"), YtriaTranslate::Do(GridGroups_customizeGrid_3, _YLOC("Is a Team")).c_str(), g_FamilyGroup);
		SetColumnPresetFlags(m_TemplateGroups->m_ColumnIsTeam, { g_ColumnsPresetDefault });

		m_TemplateGroups->m_ColumnCombinedGroupType = AddColumn(_YUID("meta.groupType"), YtriaTranslate::Do(DlgAddItemsToGroups_initGrid_5, _YLOC("Group Type")).c_str(), g_FamilyGroup, g_ColumnSize12, { g_ColumnsPresetDefault });
		SetColumnPresetFlags(m_TemplateGroups->m_ColumnCombinedGroupType, { g_ColumnsPresetDefault });

		m_MetaColumns.insert(m_MetaColumns.end(), { /*m_TemplateGroups->m_ColumnID, */m_TemplateGroups->m_ColumnDisplayName, m_TemplateGroups->m_ColumnIsTeam, m_TemplateGroups->m_ColumnCombinedGroupType });

		/*Add additional meta columns*/
		const auto& metaDataColumnInfos = getMetaDataColumnInfo();
		if (metaDataColumnInfos)
		{
			for (const auto& c : *metaDataColumnInfos)
			{
				ASSERT(nullptr == m_TemplateGroups->GetColumnForProperty(c.GetPropertyName()));
				if (nullptr == m_TemplateGroups->GetColumnForProperty(c.GetPropertyName()))
				{
					auto col = c.AddTo(*this, g_FamilyGroup);
					m_TemplateGroups->GetColumnForProperty(c.GetPropertyName()) = col;
					m_MetaColumns.emplace_back(col);
					m_HasLoadMoreAdditionalInfo = m_HasLoadMoreAdditionalInfo || c.IsLoadMore();
				}
			}
		}
	}
	else if (m_ChannelMetaColumns)
	{
		// ?
		m_MetaColumns.insert(m_MetaColumns.end(), { m_ChannelMetaColumns->m_ColumnChannelDisplayName, m_ChannelMetaColumns->m_ColumnChannelID, m_ChannelMetaColumns->m_ColumnTeamDisplayName, m_ChannelMetaColumns->m_ColumnTeamID });
	}

	m_ColDisplayName					= AddColumn(_YUID(O365_SITE_DISPLAYNAME),				scfg.GetTitle(_YUID(O365_SITE_DISPLAYNAME)),			g_FamilyInfo,			g_ColumnSize22, { g_ColumnsPresetDefault });
	addColumnIsLoadMore(_YUID("additionalInfoStatus"), _T("Status - Load Additional Information"));
    m_ColName							= AddColumn(_YUID(O365_SITE_NAME),						scfg.GetTitle(_YUID(O365_SITE_NAME)),					g_FamilyInfo,			g_ColumnSize22, { g_ColumnsPresetDefault });
    m_ColDescription					= AddColumn(_YUID(O365_SITE_DESCRIPTION),				scfg.GetTitle(_YUID(O365_SITE_DESCRIPTION)),			g_FamilyInfo,			g_ColumnSize32, { g_ColumnsPresetDefault });
    m_ColCreatedDateTime				= AddColumnDate(_YUID(O365_SITE_CREATEDDATETIME),		scfg.GetTitle(_YUID(O365_SITE_CREATEDDATETIME)),		g_FamilyInfo,			g_ColumnSize16, { g_ColumnsPresetDefault });
    m_ColLastModifiedDateTime			= AddColumnDate(_YUID(O365_SITE_LASTMODIFIEDDATETIME),	scfg.GetTitle(_YUID(O365_SITE_LASTMODIFIEDDATETIME)),	g_FamilyInfo,			g_ColumnSize16, { g_ColumnsPresetDefault });
    m_ColWebUrl							= AddColumnHyperlink(_YUID(O365_SITE_WEBURL),			scfg.GetTitle(_YUID(O365_SITE_WEBURL)),					g_FamilyInfo,			g_ColumnSize8,	{ g_ColumnsPresetDefault });
    m_ColRoot							= AddColumnCheckBox(_YUID(O365_SITE_ROOT),				scfg.GetTitle(_YUID(O365_SITE_ROOT)),					g_FamilyInfo,			g_ColumnSize6);
	m_ColHostName						= AddColumn(_YUID("host"),								scfg.GetTitle(_YUID("host")),							g_FamilyInfo,			g_ColumnSize22);
    m_ColDataLocationCode               = AddColumn(_YUID("dataLocation"),						scfg.GetTitle(_YUID("dataLocation")),					g_FamilyInfo,           g_ColumnSize8);
	static const wstring g_FamilySharePointID = YtriaTranslate::Do(GridSites_customizeGrid_14, _YLOC("SharePoint ID")).c_str();
    m_ColSharepointListID				= AddColumn(_YUID("sharepointListId"),					scfg.GetTitle(_YUID("sharepointListId")),				g_FamilySharePointID,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSharepointListItemID			= AddColumn(_YUID("sharepointListItemId"),				scfg.GetTitle(_YUID("sharepointListItemId")),			g_FamilySharePointID,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSharepointListItemUniqueID		= AddColumn(_YUID("sharepointListItemUniqueId"),		scfg.GetTitle(_YUID("sharepointListItemUniqueId")),		g_FamilySharePointID,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSharepointSiteURL				= AddColumnHyperlink(_YUID("sharepointSiteURL"),					scfg.GetTitle(_YUID("sharepointSiteURL")),				g_FamilySharePointID,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSharepointSiteID				= AddColumn(_YUID("sharepointSiteId"),					scfg.GetTitle(_YUID("sharepointSiteId")),				g_FamilySharePointID,	g_ColumnSize12, { g_ColumnsPresetTech });
	m_ColSharepointWebID				= AddColumn(_YUID("sharepointWebId"),					scfg.GetTitle(_YUID("sharepointWebId")),				g_FamilySharePointID,	g_ColumnSize12, { g_ColumnsPresetTech });
	
	static const wstring g_FamilyDrive = _T("Doc Library");
	m_ColDriveName = AddColumn(_YUID(O365_COLDRIVENAME), _T("Doc Library Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveId = AddColumn(_YUID(O365_COLDRIVEID), _T("Doc Library Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });

	m_ColDriveDescription = AddColumn(_YUID(O365_COLDRIVEDESCRIPTION), _T("Doc Library Description"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveQuotaState = AddColumn(_YUID(O365_COLDRIVEQUOTASTATE), _T("Doc Library - Storage State"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveQuotaUsed = AddColumnNumber(_YUID(O365_COLDRIVEQUOTAUSED), _T("Doc Library - Storage Used"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetDefault });
	m_ColDriveQuotaUsed->SetXBytesColumnFormat();
	m_ColDriveQuotaRemaining = AddColumnNumber(_YUID(O365_COLDRIVEQUOTAREMAINING), _T("Doc Library - Storage Remaining"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveQuotaRemaining->SetXBytesColumnFormat();
	m_ColDriveQuotaDeleted = AddColumnNumber(_YUID(O365_COLDRIVEQUOTADELETED), _T("Doc Library - Storage Deleted"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveQuotaDeleted->SetXBytesColumnFormat();
	m_ColDriveQuotaTotal = AddColumnNumber(_YUID(O365_COLDRIVEQUOTATOTAL), _T("Doc Library - Storage Total"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetDefault });
	m_ColDriveQuotaTotal->SetXBytesColumnFormat();
	m_ColDriveCreationTime = AddColumnDate(_YUID(O365_COLDRIVECREATIONTIME), _T("Doc Library - Creation Time"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveLastModifiedDateTime = AddColumnDate(_YUID(O365_COLDRIVELASTMODIFIEDDATETIME), _T("Doc Library - Last Modified Date Time"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveType = AddColumn(_YUID(O365_COLDRIVETYPE), _T("Doc Library Type"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveWebUrl = AddColumnHyperlink(_YUID(O365_COLDRIVEWEBURL), _T("Doc Library Web Url"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveCreatedByUserEmail = AddColumn(_YUID(O365_COLDRIVECREATEDBYUSEREMAIL), _T("Doc Library - Created By User Email"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveCreatedByUserId = AddColumn(_YUID(O365_COLDRIVECREATEDBYUSERID), _T("Doc Library - Created By User Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
	m_ColDriveCreatedByAppName = AddColumn(_YUID(O365_COLDRIVECREATEDBYAPPNAME), _T("Doc Library - Created By App Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveCreatedByAppId = AddColumn(_YUID(O365_COLDRIVECREATEDBYAPPID), _T("Doc Library - Created By App Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
	m_ColDriveCreatedByDeviceName = AddColumn(_YUID(O365_COLDRIVECREATEDBYDEVICENAME), _T("Doc Library - Created By Device Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveCreatedByDeviceId = AddColumn(_YUID(O365_COLDRIVECREATEDBYDEVICEID), _T("Doc Library - Created By Device Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
	m_ColDriveOwnerUserName = AddColumn(_YUID(O365_COLDRIVEOWNERUSERNAME), _T("Doc Library - Owner - User Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveOwnerUserId = AddColumn(_YUID(O365_COLDRIVEOWNERUSERID), _T("Doc Library - Owner - User Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
	m_ColDriveOwnerUserEmail = AddColumn(_YUID(O365_COLDRIVEOWNERUSEREMAIL), _T("Doc Library - Owner - User Email"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveOwnerAppName = AddColumn(_YUID(O365_COLDRIVEOWNERAPPNAME), _T("Doc Library - Owner - App Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveOwnerAppId = AddColumn(_YUID(O365_COLDRIVEOWNERAPPID), _T("Doc Library - Owner - App Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });
	m_ColDriveOwnerDeviceName = AddColumn(_YUID(O365_COLDRIVEOWNERDEVICENAME), _T("Doc Library - Owner - Device Name"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore });
	m_ColDriveOwnerDeviceId = AddColumn(_YUID(O365_COLDRIVEOWNERDEVICEID), _T("Doc Library - Owner - Device Id"), g_FamilyDrive, CacheGrid::g_ColumnSize12, { g_ColumnsPresetMore, g_ColumnsPresetTech });

	addColumnGraphID();
	AddColumnForRowPK(GetColId());

	if (m_TemplateGroups)
	{
		m_TemplateGroups->CustomizeGrid(*this);
		SetHierarchyColumnsParentPKs({ { m_TemplateGroups->m_ColumnDisplayName, 0 }, { m_ColName, GridBackendUtil::g_AllHierarchyLevels } });
	}
	else if (m_ChannelMetaColumns)
	{
		SetHierarchyColumnsParentPKs({ { m_ChannelMetaColumns->m_ColumnChannelDisplayName, 0 }, { m_ColName, GridBackendUtil::g_AllHierarchyLevels } });
	}
	else
		SetHierarchyColumnsParentPKs( { { m_ColName, GridBackendUtil::g_AllHierarchyLevels } });

    m_Frame = dynamic_cast<FrameSites*>(GetParentFrame());
	if (nullptr != GetAutomationWizard())
		GetAutomationWizard()->SetFrame(m_Frame);

	//loadRoleDelegationIcons();
}

wstring GridSites::GetName(const GridBackendRow* p_Row) const
{
	return makeRowName(p_Row, m_ColDisplayName);
}

void GridSites::BuildView(const vector<BusinessSite>& p_Sites, const vector<BusinessSite>& p_LoadedMoreSites)
{
	ASSERT(!m_TemplateGroups && !m_ChannelMetaColumns);

    if (p_Sites.size() == 1 && p_Sites[0].GetError())
    {
		HandlePostUpdateError(p_Sites[0].GetError()->GetStatusCode() == web::http::status_codes::Forbidden, false, p_Sites[0].GetError()->GetFullErrorMessage());
    }
	else
	{
		GridUpdater updater(*this, GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID | GridUpdaterOptions::FULLPURGE/* | GridUpdaterOptions::REFRESH_MODIFICATION_STATES*/ | GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS | GridUpdaterOptions::REFRESH_PROCESS_DATA/* | GridUpdaterOptions::PROCESS_LOADMORE*/));
		GridIgnoreModification ignoramus(*this);

		resetRBACHiddenCounter();

#define ONLY_SORT_TO_PUT_PARENTS_FIRST 1
		std::vector<BusinessSite> sortedSites{ p_Sites };
		std::sort(std::begin(sortedSites), std::end(sortedSites), [](const BusinessSite& p_SiteLeft, const BusinessSite& p_SiteRight)
		{
#if ONLY_SORT_TO_PUT_PARENTS_FIRST
			// Make sure the parents of the hierarchy are first
			const auto& uriLeft{ p_SiteLeft.GetWebUrl() };
			const auto& uriRight{ p_SiteRight.GetWebUrl() };
			if (uriLeft && uriRight)
			{
				try
				{
					return web::uri::split_path(web::uri(web::uri::encode_uri(*uriLeft)).path()).size()
						 < web::uri::split_path(web::uri(web::uri::encode_uri(*uriRight)).path()).size();
				}
				catch (std::exception&)
				{
					ASSERT(false);
					return ((const wstring)*uriLeft).size() < ((const wstring)*uriRight).size();
				}
			}

			return !uriLeft && uriRight;
#else
			return p_SiteLeft.GetWebUrl() < p_SiteRight.GetWebUrl();
#endif
		});

		SiteAndSubs allSites;
		{
			std::map<wstring, SiteAndSub*> pathSites; // Key: Path, value: Site corresponding to that path
			for (const auto& site : sortedSites)
			{
				if (site.GetWebUrl())
				{
					const auto siteUrl = web::uri::encode_uri(*site.GetWebUrl());
					const auto path = [&siteUrl]()
					{
						try
						{
							return web::uri(siteUrl).path();
						}
						catch (const std::exception& e)
						{
							ASSERT(false);
							LoggerService::Debug(_YFORMATERROR(L"Invalid URL: %s (%s)", siteUrl.c_str(), Str::convertFromUTF8(e.what()).c_str()));
							return Str::g_EmptyString;
						}
					}();

					const auto pathKey = !path.empty() ? getPathKey(path, pathSites) : boost::none;
					if (!pathKey)
					{
						allSites.emplace_back(site);
						pathSites[path] = &allSites.back();
					}
					else
					{
						auto existingSite = pathSites[*pathKey];
						ASSERT(nullptr != existingSite);
						if (nullptr != existingSite)
						{
							existingSite->m_SubSites.emplace_back(site);
							pathSites[path] = &existingSite->m_SubSites.back();
						}
					}
				}
			}
		}

		buildSubsitesView(allSites, nullptr, updater);

		updateRBACStatusBarText();
	}

	if (!p_LoadedMoreSites.empty())
		UpdateSitesLoadMore(p_LoadedMoreSites);
}

void GridSites::BuildView(const O365DataMap<BusinessGroup, BusinessSite>& p_SitesByGroup, const vector<BusinessSite>& p_LoadedMoreSites, bool p_FullPurge)
{
	ASSERT(m_TemplateGroups && !m_ChannelMetaColumns);

	{
		GridUpdater updater(
			*this, 
			GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID
				| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : 0)
				| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
				| GridUpdaterOptions::REFRESH_PROCESS_DATA
			)
		);

		for (const auto& siteInfo : p_SitesByGroup)
		{
			const auto& group = siteInfo.first;

			if (m_HasLoadMoreAdditionalInfo)
			{
				if (group.IsMoreLoaded())
					m_MetaIDsMoreLoaded.insert(group.GetID());
				else
					m_MetaIDsMoreLoaded.erase(group.GetID());
			}

			auto parentRow = m_TemplateGroups->AddRow(*this, group, {}, updater, false, nullptr, {}, true);

			ASSERT(nullptr != parentRow);
			if (nullptr != parentRow)
			{
				updater.GetOptions().AddRowWithRefreshedValues(parentRow);
				parentRow->SetHierarchyParentToHide(true);

				if (siteInfo.second.GetError())
				{
					updater.OnLoadingError(parentRow, *siteInfo.second.GetError());
				}
				else
				{
					// Clear previous error
					if (parentRow->IsError())
						parentRow->ClearStatus();

					if (!siteInfo.second.GetID().IsEmpty())
					{
						const GridBackendField fID(siteInfo.second.GetID(), GetColId());
						auto row = m_RowIndex.GetRow({ fID }, true, true);
						ASSERT(nullptr != row);
						if (nullptr != row)
						{
							updater.GetOptions().AddRowWithRefreshedValues(row);

							fillSiteFields(siteInfo.second, row);

							// Meta fields
							for (auto c : m_MetaColumns)
							{
								if (parentRow->HasField(c))
									row->AddField(parentRow->GetField(c));
								else
									row->RemoveField(c);
							}

							for (const auto& subSite : siteInfo.second.GetSubSites())
							{
								const GridBackendField fID(subSite.GetID(), GetColId());
								auto subSiteRow = m_RowIndex.GetRow({ fID }, true, true);
								ASSERT(nullptr != subSiteRow);
								if (nullptr != subSiteRow)
								{
									updater.GetOptions().AddRowWithRefreshedValues(subSiteRow);

									fillSiteFields(subSite, subSiteRow);
									subSiteRow->SetParentRow(row);
									// Meta fields
									for (auto c : m_MetaColumns)
									{
										if (row->HasField(c))
											subSiteRow->AddField(row->GetField(c));
										else
											subSiteRow->RemoveField(c);
									}
								}
							}
							row->SetParentRow(parentRow);
						}
					}
				}
				AddRoleDelegationFlag(parentRow, group);
			}
		}
	}

	if (!p_LoadedMoreSites.empty())
		UpdateSitesLoadMore(p_LoadedMoreSites);
}

void GridSites::BuildView(const O365DataMap<BusinessChannel, BusinessSite>& p_SitesByChannel, const vector<BusinessSite>& p_LoadedMoreSites, bool p_FullPurge)
{
	ASSERT(!m_TemplateGroups && m_ChannelMetaColumns);

	{
		GridUpdater updater(
			*this,
			GridUpdaterOptions(GridUpdaterOptions::UPDATE_GRID
				| (p_FullPurge ? GridUpdaterOptions::FULLPURGE : 0)
				| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
				| GridUpdaterOptions::REFRESH_PROCESS_DATA
			)
		);

		for (const auto& siteInfo : p_SitesByChannel)
		{
			const auto& channel = siteInfo.first;

			ASSERT(channel.GetGroupId());

			auto channelRow = GetRowIndex().GetRow({ GridBackendField(channel.GetID(), GetColId()) }, true, true);

			ASSERT(nullptr != channelRow);
			if (nullptr != channelRow)
			{
				GridUtil::SetRowObjectType(*this, channelRow, channel);
				updater.GetOptions().AddRowWithRefreshedValues(channelRow);
				channelRow->SetHierarchyParentToHide(true);
				channelRow->AddField(channel.GetDisplayName(), m_ChannelMetaColumns->m_ColumnChannelDisplayName);
				channelRow->AddField(channel.GetID(), m_ChannelMetaColumns->m_ColumnChannelID);
				channelRow->AddField(channel.GetGroupId(), m_ChannelMetaColumns->m_ColumnTeamID);
				ASSERT(channel.GetGroupId());
				if (channel.GetGroupId())
				{
					auto cachedGroup = GetGraphCache().GetGroupInCache(*channel.GetGroupId());
					ASSERT(cachedGroup.GetID() == *channel.GetGroupId());
					channelRow->AddField(cachedGroup.GetDisplayName(), m_ChannelMetaColumns->m_ColumnTeamDisplayName);
				}

				if (siteInfo.second.GetError())
				{
					updater.OnLoadingError(channelRow, *siteInfo.second.GetError());
				}
				else
				{
					// Clear previous error
					if (channelRow->IsError())
						channelRow->ClearStatus();

					if (!siteInfo.second.GetID().IsEmpty())
					{
						const GridBackendField fID(siteInfo.second.GetID(), GetColId());
						auto row = m_RowIndex.GetRow({ fID }, true, true);
						ASSERT(nullptr != row);
						if (nullptr != row)
						{
							updater.GetOptions().AddRowWithRefreshedValues(row);

							fillSiteFields(siteInfo.second, row);

							// Meta fields
							for (auto c : m_MetaColumns)
							{
								if (channelRow->HasField(c))
									row->AddField(channelRow->GetField(c));
								else
									row->RemoveField(c);
							}

							for (const auto& subSite : siteInfo.second.GetSubSites())
							{
								const GridBackendField fID(subSite.GetID(), GetColId());
								auto subSiteRow = m_RowIndex.GetRow({ fID }, true, true);
								ASSERT(nullptr != subSiteRow);
								if (nullptr != subSiteRow)
								{
									updater.GetOptions().AddRowWithRefreshedValues(subSiteRow);

									fillSiteFields(subSite, subSiteRow);
									subSiteRow->SetParentRow(row);
									// Meta fields
									for (auto c : m_MetaColumns)
									{
										if (row->HasField(c))
											subSiteRow->AddField(row->GetField(c));
										else
											subSiteRow->RemoveField(c);
									}
								}
							}
							row->SetParentRow(channelRow);
						}
					}
				}
				//AddRoleDelegationFlag(channelRow, group);
			}
		}
	}

	if (!p_LoadedMoreSites.empty())
		UpdateSitesLoadMore(p_LoadedMoreSites);
}

vector<BusinessSite> GridSites::GetLoadMoreRequestInfo(const std::vector<GridBackendRow*>& p_Rows)
{
	vector<BusinessSite> sites;
	for (auto row : p_Rows)
	{
		if (IsRowForLoadMore(row, nullptr))
		{
			BusinessSite businessSite = getBusinessObject(row);

			// avoid useless queries (multivalue explosion side effect)
			if (std::none_of(sites.begin(), sites.end(), [id = businessSite.GetID()](const BusinessSite& site){return id == site.GetID(); }))
				sites.push_back(businessSite);
		}
	}

	return sites;
}

void GridSites::AddCreatedBusinessObjects(const vector<BusinessSite>& p_Events, bool p_ScrollToNew)
{
    ASSERT(false);
}

void GridSites::UpdateBusinessObjects(const vector<BusinessSite>& p_Events, bool p_SetModifiedStatus)
{
    ASSERT(false);
}

void GridSites::RemoveBusinessObjects(const vector<BusinessSite>& p_Events)
{
    ASSERT(false);
}

void GridSites::OnCustomPopupMenu(CExtPopupMenuWnd* pPopup, int nStart /*= 0*/)
{
    ASSERT(nullptr != pPopup);
    if (nullptr != pPopup)
    {
		pPopup->ItemInsert(ID_SITESGRID_LOADMORE);
		pPopup->ItemInsert(); // Sep
		pPopup->ItemInsert(ID_SITESGRID_SHOWDRIVEITEMS);
        pPopup->ItemInsert(ID_SITESGRID_SHOWLISTS);
        pPopup->ItemInsert(ID_SITESGRID_SHOWPERMISSIONS);
        //pPopup->ItemInsert(ID_SITESGRID_SHOWROLES);
        pPopup->ItemInsert(); // Sep
    }

	ModuleO365Grid<BusinessSite>::OnCustomPopupMenu(pPopup, nStart);
}

BusinessSite GridSites::getBusinessObject(GridBackendRow* p_Row) const
{
    BusinessSite businessSite;

    {
        const auto& field = p_Row->GetField(GetColId());
        if (field.HasValue())
            businessSite.SetID(field.GetValueStr());
    }

    return businessSite;
}

void GridSites::InitializeCommands()
{
	ModuleO365Grid<BusinessSite>::InitializeCommands();

	std::map<UINT, CString> acceleratorTexts;
	const bool validAccelTexts = MFCUtil::GetAcceleratorTableTexts(m_hAccelSpecific, acceleratorTexts);
	ASSERT(validAccelTexts && acceleratorTexts.size() > 0);

	const CString profileName = g_CmdManager->ProfileNameFromWnd(m_hWnd);

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_SITESGRID_LOADMORE;
		_cmd.m_sMenuText = _T("Load Info");
		_cmd.m_sToolbarText = _T("Load Info");
		_cmd.m_sAccelText = MFCUtil::getCommandAccelText(_cmd.m_nCmdID, acceleratorTexts);
		g_CmdManager->CmdSetup(profileName, _cmd, true);

		auto hIcon = MFCUtil::getHICONFromImageResourceID(IDB_LOADMORE_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_SITESGRID_LOADMORE, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridUsers_InitializeCommands_3, _YLOC("Load Additional Information")).c_str(),
			_T("Load document library information for the selected entries only."),
			_cmd.m_sAccelText);
	}

	{
		CExtCmdItem _cmd;
		_cmd.m_nCmdID = ID_SITESGRID_SHOWDRIVEITEMS;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridSites_InitializeCommands_6, _YLOC("Show Files...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridSites_InitializeCommands_7, _YLOC("Files...")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		auto hIcon = MFCUtil::getHICONFromImageResourceID(IDB_USERS_SHOW_DRIVEITEMS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_SITESGRID_SHOWDRIVEITEMS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridSites_InitializeCommands_8, _YLOC("Show Files")).c_str(),
			YtriaTranslate::Do(GridSites_InitializeCommands_9, _YLOC("Show a detailed hierarchy of all files for all selected sites.\nManage file permissions, preview files directly, and download files en-masse from this view.")).c_str(),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			CExtCmdItem _cmd;
			_cmd.m_nCmdID = ID_SITESGRID_SHOWDRIVEITEMS_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridSites_InitializeCommands_10, _YLOC("Show Files...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_SITESGRID_SHOWDRIVEITEMS_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridSites_InitializeCommands_11, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}

	{
		CExtCmdItem _cmd;

		_cmd.m_nCmdID = ID_SITESGRID_SHOWLISTS;
		_cmd.m_sMenuText = YtriaTranslate::Do(GridSites_InitializeCommands_1, _YLOC("Show Site Lists...")).c_str();
		_cmd.m_sToolbarText = YtriaTranslate::Do(GridSites_InitializeCommands_2, _YLOC("Show Lists...")).c_str();
		_cmd.m_sAccelText = _YTEXT("");
		g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		auto hIcon = MFCUtil::getHICONFromImageResourceID(IDB_SITES_SHOW_LISTS_16X16);
		ASSERT(hIcon);
		g_CmdManager->CmdSetIcon(profileName, ID_SITESGRID_SHOWLISTS, hIcon, false);

		setRibbonTooltip(_cmd.m_nCmdID,
			YtriaTranslate::Do(GridSites_InitializeCommands_3, _YLOC("Show Site Lists")).c_str(),
			YtriaTranslate::Do(GridSites_InitializeCommands_4, _YLOC("Show all lists for all selected sites.\nSee system lists directly, and drill down to see list items and list columns for any number of sites from this view.")).c_str(),
			_cmd.m_sAccelText);

		// Only for drop down in ribbon
		{
			_cmd.m_nCmdID = ID_SITESGRID_SHOWLISTS_FRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridSites_InitializeCommands_5, _YLOC("Show Site Lists...")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);

			_cmd.m_nCmdID = ID_SITESGRID_SHOWLISTS_NEWFRAME;
			_cmd.m_sMenuText = _YTEXT("");
			_cmd.m_sToolbarText = YtriaTranslate::Do(GridSites_InitializeCommands_12, _YLOC("Show in a new window")).c_str();
			_cmd.m_sAccelText = _YTEXT("");
			g_CmdManager->CmdSetup(profileName, _cmd);
		}
	}


    {
        CExtCmdItem _cmd;

        _cmd.m_nCmdID = ID_SITESGRID_SHOWPERMISSIONS;
        _cmd.m_sMenuText = YtriaTranslate::Do(GridSites_InitializeCommands_25, _YLOC("Show Site Permissions...")).c_str();
        _cmd.m_sToolbarText = YtriaTranslate::Do(GridSites_InitializeCommands_26, _YLOC("Show Permissions")).c_str();
        _cmd.m_sAccelText = _YTEXT("");
        g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

		auto hIcon = MFCUtil::getHICONFromImageResourceID(IDB_SITES_SHOW_PERMISSIONS_16X16);
        ASSERT(hIcon);
        g_CmdManager->CmdSetIcon(profileName, ID_SITESGRID_SHOWPERMISSIONS, hIcon, false);

        setRibbonTooltip(_cmd.m_nCmdID,
            YtriaTranslate::Do(GridSites_InitializeCommands_27, _YLOC("Show SharePoint Site Permissions")).c_str(),
            YtriaTranslate::Do(GridSites_InitializeCommands_28, _YLOC("Show a detailed list of permissions for all selected sites.")).c_str(),
            _cmd.m_sAccelText);

        // Only for drop down in ribbon
        {
            _cmd.m_nCmdID = ID_SITESGRID_SHOWPERMISSIONS_FRAME;
            _cmd.m_sMenuText = _YTEXT("");
            _cmd.m_sToolbarText = YtriaTranslate::Do(GridSites_InitializeCommands_29, _YLOC("Show Site Permissions...")).c_str();
            _cmd.m_sAccelText = _YTEXT("");
            g_CmdManager->CmdSetup(profileName, _cmd);

            _cmd.m_nCmdID = ID_SITESGRID_SHOWPERMISSIONS_NEWFRAME;
            _cmd.m_sMenuText = _YTEXT("");
            _cmd.m_sToolbarText = YtriaTranslate::Do(GridSites_InitializeCommands_30, _YLOC("Show in a new window")).c_str();
            _cmd.m_sAccelText = _YTEXT("");
            g_CmdManager->CmdSetup(profileName, _cmd);
        }
    }

    //{
    //    CExtCmdItem _cmd;

    //    _cmd.m_nCmdID = ID_SITESGRID_SHOWROLES;
    //    _cmd.m_sMenuText = YtriaTranslate::Do(GridSites_InitializeCommands_31, _YLOC("Show Site Roles")).c_str();
    //    _cmd.m_sToolbarText = YtriaTranslate::Do(PanelACL_OnUniqueInitialUpdate_3, _YLOC("Show Roles")).c_str();
    //    _cmd.m_sAccelText = _YTEXT("");
    //    g_CmdManager->CmdSetup(m_RCMenuProfileName, _cmd, true);

    //    auto hIcon = MFCUtil::getHICONFromImageResourceID(IDB_SITES_SHOW_ROLES_16X16);
    //    ASSERT(hIcon);
    //    g_CmdManager->CmdSetIcon(profileName, ID_SITESGRID_SHOWROLES, hIcon, false);

    //    setRibbonTooltip(_cmd.m_nCmdID,
    //        YtriaTranslate::Do(GridSites_InitializeCommands_31, _YLOC("Show Site Roles")).c_str(),
    //        YtriaTranslate::Do(GridSites_InitializeCommands_31, _YLOC("Show Site Roles")).c_str(),
    //        _cmd.m_sAccelText);

    //    // Only for drop down in ribbon
    //    {
    //        _cmd.m_nCmdID = ID_SITESGRID_SHOWROLES_FRAME;
    //        _cmd.m_sMenuText = _YTEXT("");
    //        _cmd.m_sToolbarText = YtriaTranslate::Do(GridSites_InitializeCommands_31, _YLOC("Show Site Roles")).c_str();
    //        _cmd.m_sAccelText = _YTEXT("");
    //        g_CmdManager->CmdSetup(profileName, _cmd);

    //        _cmd.m_nCmdID = ID_SITESGRID_SHOWROLES_NEWFRAME;
    //        _cmd.m_sMenuText = _YTEXT("");
    //        _cmd.m_sToolbarText = YtriaTranslate::Do(GridChannels_InitializeCommands_6, _YLOC("Show in a new window")).c_str();
    //        _cmd.m_sAccelText = _YTEXT("");
    //        g_CmdManager->CmdSetup(profileName, _cmd);
    //    }
    //}

}

bool GridSites::IsRowForLoadMore(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return CacheGrid::IsRowForLoadMore(p_Row, p_Col) && GridUtil::IsBusinessSite(p_Row);
}

bool GridSites::GetSnapshotValue(wstring& p_Value, const GridBackendField& p_Field, const GridBackendColumn& p_Column)
{
	return m_TemplateGroups && m_TemplateGroups->GetSnapshotValue(p_Value, p_Field, p_Column)
		|| ModuleO365Grid<BusinessSite>::GetSnapshotValue(p_Value, p_Field, p_Column);
}

bool GridSites::LoadSnapshotValue(const wstring& p_Value, GridBackendRow& p_Row, const GridBackendColumn& p_Column)
{
	return m_TemplateGroups && m_TemplateGroups->LoadSnapshotValue(p_Value, p_Row, p_Column)
		|| ModuleO365Grid<BusinessSite>::LoadSnapshotValue(p_Value, p_Row, p_Column);
}

void GridSites::fillSiteFields(const BusinessSite& p_Site, GridBackendRow* p_Row)
{
    ASSERT(nullptr != p_Row);
    if (nullptr != p_Row)
    {
		SetRowObjectType(p_Row, p_Site, p_Site.HasFlag(BusinessObject::CANCELED));
        p_Row->AddField(p_Site.GetCreatedDateTime(), m_ColCreatedDateTime);
        p_Row->AddField(p_Site.GetDescription(), m_ColDescription);
        p_Row->AddField(p_Site.GetDisplayName(), m_ColDisplayName);
        p_Row->AddField(p_Site.GetLastModifiedDateTime(), m_ColLastModifiedDateTime);
        p_Row->AddField(p_Site.GetName(), m_ColName);
        p_Row->AddField(p_Site.GetRoot() != boost::none, m_ColRoot);
        p_Row->AddFieldForHyperlink(p_Site.GetWebUrl(), m_ColWebUrl);
        if (p_Site.GetSiteCollection())
		{
            p_Row->AddField(p_Site.GetSiteCollection()->Hostname, m_ColHostName);
            p_Row->AddField(p_Site.GetSiteCollection()->DataLocationCode, m_ColDataLocationCode);
		}
		else
		{
			p_Row->RemoveField(m_ColHostName);
			p_Row->RemoveField(m_ColDataLocationCode);

			auto parent = p_Row->GetParentRow();
            if (nullptr != parent)
            {
                if (parent->HasField(m_ColHostName))
				    p_Row->AddField(parent->GetField(m_ColHostName).GetValueStr(), m_ColHostName);
                if (parent->HasField(m_ColDataLocationCode))
                    p_Row->AddField(parent->GetField(m_ColDataLocationCode).GetValueStr(), m_ColDataLocationCode);
            }
		}
        if (p_Site.GetSharepointIds())
        {
            const auto& sharepointIds = p_Site.GetSharepointIds();
            p_Row->AddField(sharepointIds->ListId, m_ColSharepointListID);
            p_Row->AddField(sharepointIds->ListItemId, m_ColSharepointListItemID);
            p_Row->AddField(sharepointIds->ListItemUniqueId, m_ColSharepointListItemUniqueID);
            p_Row->AddFieldForHyperlink(sharepointIds->SiteUrl, m_ColSharepointSiteURL);
            p_Row->AddField(sharepointIds->SiteId, m_ColSharepointSiteID);
            p_Row->AddField(sharepointIds->WebId, m_ColSharepointWebID);
        }
		else
		{
			p_Row->RemoveField(m_ColSharepointListID);
			p_Row->RemoveField(m_ColSharepointListItemID);
			p_Row->RemoveField(m_ColSharepointListItemUniqueID);
			p_Row->RemoveField(m_ColSharepointSiteURL);
			p_Row->RemoveField(m_ColSharepointSiteID);
			p_Row->RemoveField(m_ColSharepointWebID);
		}

		AddRoleDelegationFlag(p_Row, p_Site);
    }
}

bool GridSites::IsRowValidForNoFieldText(const GridBackendRow* p_Row, const GridBackendColumn* p_Col) const
{
	return ModuleO365Grid<BusinessSite>::IsRowValidForNoFieldText(p_Row, p_Col)
		&& (!p_Col->HasPresetFlag(O365Grid::g_ColumnsPresetMetaMore)
			|| m_TemplateGroups && m_MetaIDsMoreLoaded.end() == m_MetaIDsMoreLoaded.find(p_Row->GetField(m_TemplateGroups->m_ColumnID).GetValueStr()));
}

void GridSites::OnShowDriveItems()
{
	if (openModuleConfirmation())
	{
		CommandInfo info;
		info.SetOrigin(Origin::Site);
		info.SetRBACPrivilege(RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_ONEDRIVE_READ);
		info.GetIds() = GetSelectedSiteIDs(info.GetRBACPrivilege());
		CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Drive, Command::ModuleTask::ListSubItems, info, { this, g_ActionNameSelectedShowDriveItemsSites, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2446")).c_str());
	}
}

void GridSites::OnShowLists()
{
	if (openModuleConfirmation())
	{
		CommandInfo info;
		info.SetOrigin(Origin::Site);
		info.GetIds() = GetSelectedSiteIDs(RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_READ);
		CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::Lists, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowLists, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
	else
	{
		AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2447")).c_str());
	}
}

void GridSites::OnShowRoles()
{
	if (confirmUnfinishedSpModule())
	{
		CommandInfo info;
		info.SetOrigin(Origin::Site);
		info.SetRBACPrivilege(RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_READ);
		info.GetIds() = GetSelectedSiteIDs(info.GetRBACPrivilege());
		CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::SiteRoles, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowLists, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
}

void GridSites::OnShowUsers()
{
	if (confirmUnfinishedSpModule())
	{
		CommandInfo info;
		info.SetOrigin(Origin::Site);
		info.SetRBACPrivilege(RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_READ);
		info.GetIds() = GetSelectedSiteIDs(info.GetRBACPrivilege());
		CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::SpUser, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowLists, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
}

void GridSites::OnShowGroups()
{
	if (confirmUnfinishedSpModule())
	{
		CommandInfo info;
		info.SetOrigin(Origin::Site);
		info.SetRBACPrivilege(RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_READ);
		info.GetIds() = GetSelectedSiteIDs(info.GetRBACPrivilege());
		CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::SpGroup, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowLists, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
	}
}

void GridSites::OnShowPermissions()
{
	if (confirmUnfinishedSpModule())
	{
		if (openModuleConfirmation())
		{
			CommandInfo info;
			info.SetOrigin(Origin::Site);
			info.SetRBACPrivilege(RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_PERMISSIONS_READ);
			info.GetIds() = GetSelectedSiteIDs(info.GetRBACPrivilege());
			CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), m_hWnd, Command::ModuleTarget::SitePermissions, Command::ModuleTask::List, info, { this, g_ActionNameSelectedShowLists, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));
		}
		else
		{
			AutomationGreenlight(YtriaTranslate::DoError(O365Grid_OnShowDriveItems_3, _YLOC("Canceled by user (pending changes)"),_YR("Y2448")).c_str());
		}
	}
}

void GridSites::OnSiteLoadMore()
{
	auto updatedGroupsGen = UpdatedObjectsGenerator<BusinessSite>();

	vector<BusinessSite> sites;

	vector<GridBackendRow*> rows;
	GetSelectedNonGroupRows(rows);
	for (auto row : rows)
	{
		if (GridUtil::IsBusinessSite(row) && IsAuthorizedByRoleDelegation(row, RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_READ))
			sites.emplace_back(getBusinessObject(row));
	}

	updatedGroupsGen.SetObjects(sites);

	CommandInfo info;
	info.Data() = updatedGroupsGen;
	info.SetFrame(m_Frame);
	info.SetRBACPrivilege(m_Frame->GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(getLicenseContextForSubModule(), GetSafeHwnd(), Command::ModuleTarget::Sites, Command::ModuleTask::UpdateMoreInfo, info, { this, g_ActionNameSelectedSiteLoadMore, GetAutomationActionRecording(), GetAutomationActionFromFrame() }));

}

void GridSites::OnUpdateShowDriveItems(CCmdUI* pCmdUI)
{
	const auto condition = [](const GridBackendRow* p_Row) { return GridUtil::IsBusinessSite(p_Row); };
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_ONEDRIVE_READ, condition));// GridUtil::IsSelectionAtLeastOneOfType<BusinessSite>(*this));
	setTextFromProfUISCommand(*pCmdUI);
}

void GridSites::OnUpdateShowLists(CCmdUI* pCmdUI)
{
	const auto condition = [](const GridBackendRow* p_Row) { return GridUtil::IsBusinessSite(p_Row); };
    pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_READ, condition));
    setTextFromProfUISCommand(*pCmdUI);
}

void GridSites::OnUpdateShowRoles(CCmdUI* pCmdUI)
{
	const auto condition = [](const GridBackendRow* p_Row) { return GridUtil::IsBusinessSite(p_Row); };
    pCmdUI->Enable(CRMpipe().IsFeatureSandboxed() && hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_READ, condition));
    setTextFromProfUISCommand(*pCmdUI);
}

void GridSites::OnUpdateShowUsers(CCmdUI* pCmdUI)
{
	const auto condition = [](const GridBackendRow* p_Row) { return GridUtil::IsBusinessSite(p_Row); };
    pCmdUI->Enable(CRMpipe().IsFeatureSandboxed() && hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_READ, condition));
    setTextFromProfUISCommand(*pCmdUI);
}

void GridSites::OnUpdateShowGroups(CCmdUI* pCmdUI)
{
	const auto condition = [](const GridBackendRow* p_Row) { return GridUtil::IsBusinessSite(p_Row); };
    pCmdUI->Enable(CRMpipe().IsFeatureSandboxed() && hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_READ, condition));
    setTextFromProfUISCommand(*pCmdUI);
}

void GridSites::OnUpdateShowPermissions(CCmdUI* pCmdUI)
{
	const auto condition = [](const GridBackendRow* p_Row) { return GridUtil::IsBusinessSite(p_Row); };
    pCmdUI->Enable(CRMpipe().IsFeatureSandboxed() && !m_Frame->GetSessionInfo().IsUltraAdmin() && hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_PERMISSIONS_READ, condition));
    setTextFromProfUISCommand(*pCmdUI);
}

void GridSites::OnUpdateSiteLoadMore(CCmdUI* pCmdUI)
{
	const auto condition = [](const GridBackendRow* p_Row) { return GridUtil::IsBusinessSite(p_Row); };
	pCmdUI->Enable(hasNoTaskRunning() && IsFrameConnected() && GridUtil::IsSelectionAuthorizedByRoleDelegation(*this, RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_READ, condition));
	setTextFromProfUISCommand(*pCmdUI);
}

BOOL GridSites::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		// Translate the message using accelerator table
		ASSERT(m_hAccelSpecific);
		const int ret = ::TranslateAccelerator(m_hWnd, m_hAccelSpecific, pMsg);
		if (ret != FALSE)
			return ret;
	}

	return ModuleO365Grid<BusinessSite>::PreTranslateMessage(pMsg);
}

boost::YOpt<wstring> GridSites::getPathKey(const wstring& p_Path, const map<wstring, SiteAndSub*>& p_PathSites)
{
	boost::YOpt<wstring> pathKey;

	wstring path = p_Path;
	if (path.size() > 1)
	{
		if (path.back() == _YTEXT('/'))
			path.pop_back();
		const auto pos = path.find_last_of(_YTEXT('/'));
		if (pos != wstring::npos && pos > 0)
			path = path.substr(0, pos);
	}

	while (!pathKey && !path.empty())
	{
		auto it = p_PathSites.find(path);
		if (p_PathSites.end() != it)
		{
			pathKey = it->first;
		}
		else
		{
			if (path.size() > 1)
			{
				const auto pos = path.find_last_of(_YTEXT('/'));
				if (pos != wstring::npos)
					path = path.substr(0, (std::max)(static_cast<size_t>(1), pos));
			}
			else
				path.clear(); // To exit the loop
		}
	}

    return pathKey;
}

void GridSites::buildSubsitesView(const SiteAndSubs& p_SubSites, GridBackendRow* p_ParentRow, GridUpdater& p_Updater)
{
	ASSERT(!m_TemplateGroups);
    for (const auto& siteAndSub : p_SubSites)
    {
		const auto& site = siteAndSub.m_Site;
		if (!site.GetID().IsEmpty())
		{
            updateHierarchyCache(p_ParentRow, site);

			GridBackendField fID(site.GetID(), GetColId());
			auto subSiteRow = m_RowIndex.GetRow({ fID }, true, true);
			ASSERT(nullptr != subSiteRow);
			if (nullptr != subSiteRow)
			{
				p_Updater.GetOptions().AddRowWithRefreshedValues(subSiteRow);

				if (nullptr == p_ParentRow)
					subSiteRow->SetHierarchyParentToHide(true);
				else
					subSiteRow->SetParentRow(p_ParentRow);
				fillSiteFields(site, subSiteRow);
			}
			buildSubsitesView(siteAndSub.m_SubSites, subSiteRow, p_Updater);
		}
    }
}

void GridSites::updateHierarchyCache(GridBackendRow* p_ParentRow, const BusinessSite& p_Site)
{
    if (p_ParentRow && p_Site.GetDisplayName())
    {
        wstring parentDisplayName = p_ParentRow->GetField(m_ColDisplayName).GetValueStr();

        wstring newDisplayName = wstring(GetGraphCache().GetSiteHierarchyDisplayName(p_Site.GetID())) + wstring(_YTEXT(" - ")) + parentDisplayName;
        GetGraphCache().SetSiteHierarchyDisplayName(p_Site.GetID(), newDisplayName);
        
        GridBackendRow* nextParentRow = p_ParentRow->GetParentRow();
        if (nullptr != nextParentRow)
            updateHierarchyCache(nextParentRow, p_Site);
    }
}

bool GridSites::confirmUnfinishedSpModule()
{
	// Remove this confirmation method when out of sandbox.

	bool go = false;
	if (CRMpipe().IsFeatureSandboxed())
	{
		YCodeJockMessageBox dlg(this
			, DlgMessageBox::eIcon_ExclamationWarning
			, _YTEXT("WARNING")
			, _YTEXT("You shouldn't go there. Still in progress, can crash anytime!")
			, _YTEXT("")
			, { { IDOK, _YTEXT("Continue") },{ IDCANCEL, _YTEXT("Cancel") } });

		go = dlg.DoModal() == IDOK;
	}

	return go;
}

O365IdsContainer GridSites::GetSelectedSiteIDs(const RoleDelegationUtil::RBAC_Privilege p_Privilege)
{
	O365IdsContainer result;

	for (auto row : GetBackend().GetSelectedRowsInOrder())
    {
		if (IsSiteAuthorizedByRoleDelegation(row, p_Privilege))
			result.insert(row->GetField(GetColId()).GetValueStr());
    }

    return result;
}

ModuleCriteria GridSites::GetModuleCriteriaFromTopRows(const std::vector<GridBackendRow*>& p_Rows)
{
	auto frame = dynamic_cast<GridFrameBase*>(GetParent());
	ASSERT(nullptr != frame);
	if (nullptr != frame)
	{
		ModuleCriteria criteria = frame->GetModuleCriteria();

		if (criteria.m_Origin == Origin::Channel)
		{
			criteria.m_SubIDs.clear();

			ASSERT(m_ChannelMetaColumns && nullptr != m_ChannelMetaColumns->m_ColumnTeamID && nullptr != m_ChannelMetaColumns->m_ColumnChannelID);
			if (m_ChannelMetaColumns && nullptr != m_ChannelMetaColumns->m_ColumnTeamID && nullptr != m_ChannelMetaColumns->m_ColumnChannelID)
			{
				for (auto row : p_Rows)
				{
					ASSERT(GridUtil::IsBusinessChannel(row));
					if (GridUtil::IsBusinessChannel(row))
						criteria.m_SubIDs[PooledString(row->GetField(m_ChannelMetaColumns->m_ColumnTeamID).GetValueStr())].insert(row->GetField(m_ChannelMetaColumns->m_ColumnChannelID).GetValueStr());
				}
			}
		}
		else
		{
			criteria.m_IDs.clear();

			ASSERT(GetColId() != nullptr);
			if (GetColId() != nullptr)
				for (auto row : p_Rows)
					criteria.m_IDs.insert(row->GetField(GetColId()).GetValueStr());
		}

		return criteria;
	}

	return ModuleCriteria();
}

RoleDelegationUtil::RBAC_Privilege GridSites::GetPrivilegeEdit() const
{
	return RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_EDIT;
}

void GridSites::UpdateSitesLoadMore(const vector<BusinessSite>& p_Sites)
{
	GridIgnoreModification ignoramus(*this);
	GridUpdaterOptions updateOptions(GridUpdaterOptions::UPDATE_GRID
		| GridUpdaterOptions::HANDLE_POSTUPDATE_ERRORS
		| GridUpdaterOptions::PROCESS_LOADMORE
		/*| (p_DoNotShowLoadingErrors ? GridUpdaterOptions::DO_NOT_SHOW_LOADING_ERROR_DIALOG : 0) TODO? */);
	GridUpdater updater(*this, updateOptions);

	for (const auto& site : p_Sites)
	{
		const GridBackendField fID(site.GetID(), GetColId());
		auto siteRow = m_RowIndex.GetRow({ fID }, false, false);
		if (GridUtil::IsBusinessSite(siteRow))
		{
			if (site.GetError())
			{
				updater.OnLoadingError(siteRow, *site.GetError());
			}
			else
			{
				siteRow->ClearStatus();
				fillSiteFields(site, siteRow);

				if (site.GetDrive())
				{
					const auto& drive = *site.GetDrive();

					if (drive.GetError())
					{
						auto setError = [siteRow, errMsg = drive.GetError()->GetFullErrorMessage()](GridBackendColumn* p_Col)
						{
							if (nullptr != p_Col)
								siteRow->AddField(errMsg, p_Col, GridBackendUtil::ICON_ERROR);
						};

						setError(m_ColDriveName);
						setError(m_ColDriveId);
						setError(m_ColDriveDescription);
						setError(m_ColDriveCreationTime);
						setError(m_ColDriveLastModifiedDateTime);
						setError(m_ColDriveType);
						setError(m_ColDriveWebUrl);
						setError(m_ColDriveQuotaState);
						setError(m_ColDriveQuotaUsed);
						setError(m_ColDriveQuotaRemaining);
						setError(m_ColDriveQuotaDeleted);
						setError(m_ColDriveQuotaTotal);
						setError(m_ColDriveCreatedByUserEmail);
						setError(m_ColDriveCreatedByUserId);
						setError(m_ColDriveCreatedByAppName);
						setError(m_ColDriveCreatedByAppId);
						setError(m_ColDriveCreatedByDeviceName);
						setError(m_ColDriveCreatedByDeviceId);
						setError(m_ColDriveOwnerUserName);
						setError(m_ColDriveOwnerUserId);
						setError(m_ColDriveOwnerAppName);
						setError(m_ColDriveOwnerAppId);
						setError(m_ColDriveOwnerDeviceName);
						setError(m_ColDriveOwnerDeviceId);

						updater.OnLoadingError(siteRow, *site.GetError(), _T("Document Library: "));
					}
					else
					{
						siteRow->AddField(drive.GetName(), m_ColDriveName);
						siteRow->AddField(drive.GetID(), m_ColDriveId);

						siteRow->AddField(drive.GetDescription(), m_ColDriveDescription);

						if (drive.GetQuota())
						{
							const auto& quota = *drive.GetQuota();
							siteRow->AddField(quota.State, m_ColDriveQuotaState);
							siteRow->AddField(quota.Used, m_ColDriveQuotaUsed);
							siteRow->AddField(quota.Remaining, m_ColDriveQuotaRemaining);
							siteRow->AddField(quota.Deleted, m_ColDriveQuotaDeleted);
							siteRow->AddField(quota.Total, m_ColDriveQuotaTotal);
						}

						siteRow->AddField(drive.GetCreatedDateTime(), m_ColDriveCreationTime);
						siteRow->AddField(drive.GetLastModifiedDateTime(), m_ColDriveLastModifiedDateTime);
						siteRow->AddField(drive.GetDriveType(), m_ColDriveType);
						siteRow->AddFieldForHyperlink(drive.GetWebUrl(), m_ColDriveWebUrl);

						if (drive.GetCreatedBy() != boost::none)
						{
							const auto& createdBy = *drive.GetCreatedBy();
							if (createdBy.User != boost::none)
							{
								const auto& user = *createdBy.User;
								siteRow->AddField(user.Email, m_ColDriveCreatedByUserEmail);
								siteRow->AddField(user.Id, m_ColDriveCreatedByUserId);
							}
							if (createdBy.Application != boost::none)
							{
								const auto& app = *createdBy.Application;
								siteRow->AddField(app.DisplayName, m_ColDriveCreatedByUserEmail);
								siteRow->AddField(app.Id, m_ColDriveCreatedByUserId);
							}
							if (createdBy.Device != boost::none)
							{
								const auto& device = *createdBy.Device;
								siteRow->AddField(device.DisplayName, m_ColDriveCreatedByDeviceName);
								siteRow->AddField(device.Id, m_ColDriveCreatedByDeviceId);
							}
						}

						if (drive.GetOwner() != boost::none)
						{
							const auto& owner = *drive.GetOwner();

							if (owner.User)
							{
								const auto& user = *owner.User;
								siteRow->AddField(user.DisplayName, m_ColDriveOwnerUserName);
								siteRow->AddField(user.Email, m_ColDriveOwnerUserEmail);
							}
							if (owner.Application)
							{
								const auto& app = *owner.Application;
								siteRow->AddField(app.DisplayName, m_ColDriveOwnerUserEmail);
								siteRow->AddField(app.Id, m_ColDriveOwnerUserId);
							}
							if (owner.Device)
							{
								const auto& device = *owner.Device;
								siteRow->AddField(device.DisplayName, m_ColDriveOwnerDeviceName);
								siteRow->AddField(device.Id, m_ColDriveOwnerDeviceId);
							}
						}
					}
				}
			}

			updater.GetOptions().AddRowWithRefreshedValues(siteRow);
		}
	}
}

O365Grid::SnapshotCaps GridSites::GetSnapshotCapabilities() const
{
	return { true };
}

void GridSites::customizeGridPostProcess()
{
	O365Grid::customizeGridPostProcess();
	SetNoFieldTextAndIcon(GetColumnsByPresets({ g_ColumnsPresetMore }), IDB_ICON_LM, _T("To see this information, use the 'Load Info' button"), _T(" - Use the 'Load Info' button to display additional information - "));
	SetLoadMoreStatus(true, _T("Loaded"), IDB_ICON_LMSTATUS_LOADED);
	SetLoadMoreStatus(false, _T("Not Loaded"), IDB_ICON_LMSTATUS_NOTLOADED);
}
