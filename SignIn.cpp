#include "SignIn.h"

RTTR_REGISTRATION
{
	using namespace rttr;
registration::class_<SignIn>("SignIn") (metadata(MetadataKeys::TypeDisplayName, wstring(_YTEXT("Sign-in log entry"))))
.constructor()(policy::ctor::as_object);
}