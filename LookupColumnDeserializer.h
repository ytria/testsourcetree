#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "LookupColumn.h"

class LookupColumnDeserializer : public JsonObjectDeserializer, public Encapsulate<LookupColumn>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

