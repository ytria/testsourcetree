#pragma once

#include "Application.h"
#include "Encapsulate.h"
#include "JsonObjectDeserializer.h"

class ApplicationDeserializer : public JsonObjectDeserializer, public Encapsulate<Application>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

