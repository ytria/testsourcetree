#include "DriveItemDeserializer.h"

//#include "AudioDeserializer.h"
#include "DeletedDeserializer.h"
#include "FileDeserializer.h"
#include "FileSystemInfoDeserializer.h"
//#include "GeoCoordinatesDeserializer.h"
#include "GraphFolderDeserializer.h"
#include "IdentitySetDeserializer.h"
//#include "ImageDeserializer.h"
#include "ItemReferenceDeserializer.h"
#include "ListItemsFieldsForCheckoutStatusDeserializer.h"
#include "JsonSerializeUtil.h"
#include "PackageDeserializer.h"
#include "PublicationFacetDeserializer.h"
//#include "PhotoDeserializer.h"
//#include "RemoteItemDeserializer.h"
//#include "SearchResultDeserializer.h"
#include "SharedDeserializer.h"
#include "SharepointIdsDeserializer.h"
//#include "SpecialFolderDeserializer.h"
//#include "VideoDeserializer.h"

void DriveItemDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    //{
    //    AudioDeserializer d;
    //    if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("audio"), p_Object))
    //        m_Data.SetAudio(d.GetData());
    //}
    
	//JsonSerializeUtil::DeserializeString(_YTEXT("content"), m_Data.m_Content, p_Object);

    {
        IdentitySetDeserializer d;
        if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("createdBy"), p_Object))
            m_Data.SetCreatedBy(d.GetData());
    }

    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("createdDateTime"), m_Data.m_CreatedDateTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("cTag"), m_Data.m_CTag, p_Object);

    {
        auto ittDeleted = p_Object.find(_YTEXT("deleted"));
        if (ittDeleted != p_Object.end())
            m_Data.m_IsDeleted = !ittDeleted->second.is_null();
        else
            m_Data.m_IsDeleted = false;
    }

	JsonSerializeUtil::DeserializeString(_YTEXT("description"), m_Data.m_Description, p_Object);

	JsonSerializeUtil::DeserializeString(_YTEXT("eTag"), m_Data.m_ETag, p_Object);

    {
        FileDeserializer d;
        if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("file"), p_Object))
            m_Data.SetFile(d.GetData());
    }

    {
        FileSystemInfoDeserializer d;
        if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("fileSystemInfo"), p_Object))
            m_Data.SetFileSystemInfo(d.GetData());
    }

    static const wstring g_FolderKey = _YTEXT("folder");
    auto ittFolder = p_Object.find(g_FolderKey);
    if (ittFolder != p_Object.end())
    {
        m_Data.m_IsFolder = !ittFolder->second.is_null();
        GraphFolderDeserializer d;
        if (JsonSerializeUtil::DeserializeAny(d, g_FolderKey, p_Object))
            m_Data.SetFolder(d.GetData());
    }
    else
    {
        m_Data.m_IsFolder = false;
    }

	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);

    //{
    //    ImageDeserializer d;
    //    if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("image"), p_Object))
    //        m_Data.SetImage(d.GetData());
    //}

    {
        IdentitySetDeserializer d;
        if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("lastModifiedBy"), p_Object))
            m_Data.SetLastModifiedBy(d.GetData());
    }

	JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.m_LastModifiedDateTime, p_Object);

    //{
    //    GeoCoordinatesDeserializer d;
    //    if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("location"), p_Object))
    //        m_Data.SetLocation(d.GetData());
    //}

	JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);

    {
        PackageDeserializer d;
        if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("package"), p_Object))
            m_Data.SetPackage(d.GetData());
    }

    {
        ItemReferenceDeserializer d;
        if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("parentReference"), p_Object))
            m_Data.SetParentReference(d.GetData());

        ASSERT(m_Data.m_ParentReference != boost::none && m_Data.m_ParentReference->DriveId != boost::none);
        if (m_Data.m_ParentReference != boost::none && m_Data.m_ParentReference->DriveId != boost::none)
            m_Data.m_DriveId = *m_Data.m_ParentReference->DriveId;
    }

    m_Data.m_OneDrivePath = GetOneDrivePath();

    //{
    //    PhotoDeserializer d;
    //    if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("photo"), p_Object))
    //        m_Data.SetPhoto(d.GetData());
    //}

	{
		PublicationFacetDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("publication"), p_Object))
			m_Data.SetPublication(d.GetData());
	}

    //{
    //    RemoteItemDeserializer d;
    //    if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("remoteItem"), p_Object))
    //        m_Data.SetRemoteItem(d.GetData());
    //}

    //{
    //    SearchResultDeserializer d;
    //    if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("searchResult"), p_Object))
    //        m_Data.SetSearchResult(d.GetData());
    //}

    {
        SharedDeserializer d;
        if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("shared"), p_Object))
            m_Data.SetShared(d.GetData());
    }

	{
		SharepointIdsDeserializer d;
		if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("sharepointIds"), p_Object))
			m_Data.SetSharepointIds(d.GetData());
	}

	JsonSerializeUtil::DeserializeInt64(_YTEXT("size"), m_Data.m_TotalSize, p_Object);
    
	//{
	//	SpecialFolderDeserializer d;
	//	if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("specialFolder"), p_Object))
	//		m_Data.SetSpecialFolder(d.GetData());
	//}

	//{
	//	VideoDeserializer d;
	//	if (JsonSerializeUtil::DeserializeAny(d, _YTEXT("video"), p_Object))
	//		m_Data.SetVideo(d.GetData());
	//}

	JsonSerializeUtil::DeserializeString(_YTEXT("webDavUrl"), m_Data.m_WebDavUrl, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("webUrl"), m_Data.m_WebUrl, p_Object);

	{
        ListItemsFieldsForCheckoutStatusDeserializer liffcsd;
		if (JsonSerializeUtil::DeserializeAny(liffcsd, _YTEXT("listItem"), p_Object))
			m_Data.SetCheckoutStatus(liffcsd.GetData());
	}
}

PooledString DriveItemDeserializer::GetOneDrivePath() const
{
    wstring fullPath;

    if (m_Data.m_ParentReference && m_Data.m_ParentReference->Path)
        fullPath += *m_Data.m_ParentReference->Path;

    if (m_Data.m_Name && !m_Data.m_Name->IsEmpty())
    {
        fullPath += L"/";
        fullPath += *m_Data.m_Name;
    }

    PooledString oneDrivePath;

    const wstring root = _YTEXT("/root:");
    size_t rootIndex = fullPath.find(root);
    if (rootIndex != wstring::npos)
        oneDrivePath = std::wstring(fullPath.begin() + rootIndex + root.size(), fullPath.end());
    else
        oneDrivePath = fullPath;

    return oneDrivePath;
}
