#pragma once

#include "AttachmentsInfo.h"
#include "AutomationUtil.h"
#include "CacheGrid.h"
#include "Command.h"
#include "GridSnapshotUtil.h"
#include "LinearMap.h"
#include "ModuleCriteria.h"
#include "ModuleOptions.h"
#include "YtriaTaskData.h"

class GridFrameBase;
class AutomationAction;
class BusinessObjectManager;
class GraphCache;
class BusinessGroup;
class DlgContentViewer;
class O365Grid;
class PersistentSession;

class DownloadInlineAttachmentsCallback
{
public:
	virtual ~DownloadInlineAttachmentsCallback() = default;
	virtual void processInlineAttachments(const std::map<AttachmentsInfo::IDs, std::map<wstring, wstring>>& p_IdsToCidsToPaths) = 0;
};

class ModuleBase
{
public:
	ModuleBase(const PooledString& p_Name);
	virtual ~ModuleBase();

	void AddGridFrame(GridFrameBase* p_Frame);
	void RemoveGridFrame(GridFrameBase* p_Frame);
	const PooledString& GetName() const;
	const std::unordered_set<GridFrameBase*>& GetGridFrames() const;

	template<class T>
	bool ShouldCreateFrame(const ModuleCriteria& p_ModuleCriteria, Command::HistorySourceWindow& p_SourceWindow, AutomationAction* p_Action);
	template<class T>
	bool ShouldCreateFrame(const ModuleCriteria& p_ModuleCriteria, Command::HistorySourceWindow& p_SourceWindow, AutomationAction* p_Action, const ModuleOptions& p_Options);

	static const PersistentSession& GetConnectedSession();
	static std::shared_ptr<Sapio365Session> GetConnectedSapio365Session();

	void Execute(const Command& p_Command);

	static void TaskFinished(GridFrameBase* p_Frame, const YtriaTaskData& taskData, AutomationAction* p_ActionToGreenlight);

    void CreateFrameLogger(GridFrameBase* p_GridFrame, const wstring& p_Message);

	static void SetAutomationGreenLight(AutomationAction* p_Action);
	static void SetAutomationGreenLight(AutomationAction* p_Action, const wstring& p_ErrorMessage);
	static void SetActionCanceledByUser(AutomationAction* p_Action);

	static LinearMap<PooledString, PooledString> GetOriginIdsAndNames(const O365IdsContainer& p_IDs, O365Grid& p_Grid);

	static void SelectOriginId(CacheGrid& p_Grid, const wstring& p_Id);
    static bool ShouldBuildView(HWND p_FrameHwnd, bool p_HasBeenCanceled, const YtriaTaskData& p_TaskData, bool p_AskToKeepPartial);

	template <class BusinessType>
	static bool everythingCanceled(const vector<BusinessType>& p_Vec);
	template <class BusinessType1, class BusinessType2>
	static bool everythingCanceled(const O365DataMap<BusinessType1, vector<BusinessType2>>& p_Map);

	using AttachmentsContainer = map<PooledString, map<PooledString, vector<BusinessAttachment>>>;

	static bool ShouldDoListRequests(size_t p_CacheSize, size_t p_NbElementsToRefresh, size_t p_NbElementsPerPage);

	template<class FrameT>
	void LoadSnapshot(const Command& p_Command, const PooledString& p_FrameTitle);

	template<class FrameT>
	using FrameCreator = std::function<FrameT* (const GridSnapshot::Metadata& p_SnapshotMeta, const ModuleCriteria& p_ModuleCriteria, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, ModuleBase& p_Module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)>;
	template<class FrameT>
	void LoadSnapshot(const Command& p_Command, FrameCreator<FrameT> p_FrameCreator);

protected:
    template <class T>
	void showAttachments(std::shared_ptr<BusinessObjectManager> p_BOM, const std::vector<AttachmentsInfo::IDs>& p_IDs, T p_Frame, Origin p_Origin, const Command& p_Command);

    template <class T>
	void downloadAttachments(std::shared_ptr<BusinessObjectManager> p_BOM, const AttachmentsInfo& p_AttachmentInfos, T p_Frame, Origin p_Origin, const Command& p_Command);

    template <class T>
    void showItemAttachment(std::shared_ptr<BusinessObjectManager> p_BOM, const AttachmentsInfo::IDs& p_ItemAttachment, Origin p_Origin, T p_Frame, const Command& p_Command);

    template <class T>
    void deleteAttachments(std::shared_ptr<BusinessObjectManager> p_BOM, const AttachmentsInfo& p_AttachmentInfos, Origin p_Origin, T p_Frame, const Command& p_Command);

    void downloadInlineAttachments(const AttachmentsInfo& p_AttachmentInfos, DownloadInlineAttachmentsCallback* p_Callback, const Command& p_Cmd);

	AttachmentsContainer retrieveAttachments(std::shared_ptr<BusinessObjectManager> p_BOM, const std::vector<AttachmentsInfo::IDs>& p_IDs, Origin p_Origin, YtriaTaskData taskData);

    YtriaTaskData addFrameTask(const wstring& p_LogMessage, GridFrameBase* p_Frame, const Command& p_Command, bool p_IsMotherTask); // set p_IsMotherTask to true if task corresponds to module opening (first load).

	static void AddCanceledObjects(vector<BusinessGroup>& p_Objects, const O365IdsContainer& p_Ids, GraphCache& p_GraphCache);

	// Use last parameter if you need the BusinessSubscribedSku list (pass an initialized boost::YOpt<vector<BusinessSubscribedSku>>).
	static std::pair<boost::YOpt<SapioError>, std::map<PooledString, boost::YOpt<PooledString>>> getSubscribedSkus(const std::shared_ptr<BusinessObjectManager>& p_BOM, YtriaTaskData p_TaskData, boost::YOpt<vector<BusinessSubscribedSku>>& p_sskus);
	static std::pair<boost::YOpt<SapioError>, std::map<PooledString, boost::YOpt<PooledString>>> getSubscribedSkus(const std::shared_ptr<BusinessObjectManager>& p_BOM, YtriaTaskData p_TaskData);

private:
	YtriaTaskData addTaskData(const wstring& p_Name, CFrameWnd* p_Originator);

	virtual void executeImpl(const Command& p_Command) = 0;

	template<class T>
	bool shouldCreateFrame(const ModuleCriteria& p_ModuleCriteria, Command::HistorySourceWindow& p_SourceWindow, AutomationAction* p_Action, const ModuleOptions* p_Options);

private:
	PooledString						m_Name;
	std::unordered_set<GridFrameBase*>	m_GridFrames;// no shared_ptr: MFC self-destruct
};

#include "ModuleBase.hpp"