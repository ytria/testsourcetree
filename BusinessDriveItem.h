#pragma once

#include "BusinessObject.h"
#include "CheckoutStatus.h"
#include "Deleted.h"
#include "DriveItem.h"
#include "GraphFolder.h"
#include "IdentitySet.h"
#include "ItemReference.h"
#include "FileSystemInfo.h"

class BusinessDriveItem : public BusinessObject
{
public:
    BusinessDriveItem();
    BusinessDriveItem(const DriveItem& p_DriveItem);

    virtual pplx::task<RestResultInfo> Sync(const vector<rttr::property>& properties);

    
    bool IsFolder() const;
	bool IsNotebook() const;
	void SetIsFolder(const bool p_IsFolder);
	void SetIsNotebook(const bool p_IsNotebook);

    const PooledString& GetOneDrivePath() const;
	void SetOneDrivePath(const PooledString& p_Val);

    const PooledString& GetDriveId() const;
    virtual void SetDriveId(const PooledString& p_Val);


public: // Graph Properties
	//const boost::YOpt<Audio>& GetAudio() const;
	//void SetAudio(const boost::YOpt<Audio>& p_Val);

	//const boost::YOpt<PooledString>& GetContent() const;
	//void SetContent(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<IdentitySet>& GetCreatedBy() const;
	void SetCreatedBy(const boost::YOpt<IdentitySet>& p_Val);

	const boost::YOpt<YTimeDate>& GetCreatedDateTime() const;

	const boost::YOpt<PooledString>& GetCTag() const;
	void SetCTag(const boost::YOpt<PooledString>& p_Val);

    bool IsDeleted() const;

	const boost::YOpt<PooledString>& GetDescription() const;
	void SetDescription(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetETag() const;
	void SetETag(const boost::YOpt<PooledString>& p_Val);

    const boost::YOpt<File>& GetFile() const;
    void SetFile(const boost::YOpt<File>& p_Val);

    const boost::YOpt<FileSystemInfo>& GetFileSystemInfo() const;
    void SetFileSystemInfo(const boost::YOpt<FileSystemInfo>& p_Val);

	const boost::YOpt<GraphFolder>& GetFolder() const;
	void SetFolder(const boost::YOpt<GraphFolder>& p_Val);

	//const boost::YOpt<Image>& GetImage() const;
	//void SetImage(const boost::YOpt<Image>& p_Val);

    const boost::YOpt<IdentitySet>& GetLastModifiedBy() const;
    void SetLastModifiedBy(const boost::YOpt<IdentitySet>& p_Val);

	const boost::YOpt<YTimeDate>& GetLastModifiedDateTime() const;

	//const boost::YOpt<GeoCoordinates>& GetLocation() const;
	//void SetLocation(const boost::YOpt<GeoCoordinates>& p_Val);

	const boost::YOpt<PooledString>& GetName() const;
	void SetName(boost::YOpt<PooledString> p_Name);
	
	const boost::YOpt<Package>& GetPackage() const;
	void SetPackage(const boost::YOpt<Package>& p_Val);

    const boost::YOpt<ItemReference>& GetParentReference() const;
    void SetParentReference(const boost::YOpt<ItemReference>& p_Val);

	//const boost::YOpt<Photo>& GetPhoto() const;
	//void SetPhoto(const boost::YOpt<Photo>& p_Val);

	const boost::YOpt<PublicationFacet>& GetPublication() const;
	void SetPublication(const boost::YOpt<PublicationFacet>& p_Val);

	//const boost::YOpt<RemoteItem>& GetRemoteItem() const;
	//void SetRemoteItem(const boost::YOpt<RemoteItem>& p_Val);

	//const boost::YOpt<SearchResult>& GetSearchResult() const;
	//void SetSearchResult(const boost::YOpt<SearchResult>& p_Val);

	const boost::YOpt<Shared>& GetShared() const;
	void SetShared(const boost::YOpt<Shared>& p_Val);

	const boost::YOpt<SharepointIds>& GetSharepointIds() const;
	void SetSharepointIds(const boost::YOpt<SharepointIds>& p_Val);

	int64_t GetTotalSize() const;
	void SetTotalSize(const int64_t& p_Size);

	//const boost::YOpt<SpecialFolder>& GetSpecialFolder() const;
    //void SetSpecialFolder(const boost::YOpt<SpecialFolder>& p_Val);
    
    //const boost::YOpt<Video>& GetVideo() const;
    //void SetVideo(const boost::YOpt<Video>& p_Val);

	const PooledString& GetWebDavUrl() const;

	const PooledString& GetWebUrl() const;

	const boost::YOpt<CheckoutStatus>& GetCheckoutStatus() const;
	void SetCheckoutStatus(const boost::YOpt<CheckoutStatus>& p_Val);

public:
    static DriveItem ToDriveItem(const BusinessDriveItem& p_BusinessDriveItem);

    const vector<BusinessDriveItem>& GetDriveItemChildren() const;
    void SetDriveItemChildren(const vector<BusinessDriveItem>& p_Val);
    void SetDriveItemChildren(vector<BusinessDriveItem>&& p_Val);

	// Only for item creation
	const boost::YOpt<PooledString>& GetParentFolderId() const;
	void SetParentFolderId(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetLocalFilePath() const;
	void SetLocalFilePath(const boost::YOpt<PooledString>& p_Val);
	/////////////////////////

	// WARNING ! Only for edition, only use when saving modifications.
	const int GetHierarchyLevel() const;
	void SetHierarchyLevel(int p_Level);
	/////////////////////////

	enum Flag
	{
		RENAME_ON_CONFLICT = CUSTOM_FLAG_BEGIN,
		REPLACE_ON_CONFLICT = RENAME_ON_CONFLICT << 1,
	};

	virtual wstring GetObjectName() const override;

protected:
    virtual pplx::task<vector<HttpResultWithError>> SendEditRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const;
	virtual TaskWrapper<HttpResultWithError> SendDeleteRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const;
	virtual TaskWrapper<HttpResultWithError> SendCreateRequest(std::shared_ptr<Sapio365Session> p_Sapio365Session, YtriaTaskData p_TaskData) const;

private:
    //boost::YOpt<Audio> m_Audio;
    //boost::YOpt<PooledString> m_Content;
    boost::YOpt<IdentitySet> m_CreatedBy;
    boost::YOpt<YTimeDate> m_CreatedDateTime;
    boost::YOpt<PooledString> m_CTag;
    bool m_IsDeleted;
	boost::YOpt<PooledString> m_Description;
    boost::YOpt<PooledString> m_ETag;
    boost::YOpt<File> m_File;
    boost::YOpt<FileSystemInfo> m_FileSystemInfo;
    boost::YOpt<GraphFolder> m_Folder;

    //boost::YOpt<Image> m_Image;
    boost::YOpt<IdentitySet> m_LastModifiedBy;
    boost::YOpt<YTimeDate> m_LastModifiedDateTime;
    //boost::YOpt<GeoCoordinates> m_Location;
	boost::YOpt<PooledString> m_Name;
    boost::YOpt<Package> m_Package;
    boost::YOpt<ItemReference> m_ParentReference;
    //boost::YOpt<Photo> m_Photo;
	boost::YOpt<PublicationFacet> m_Publication;
    //boost::YOpt<RemoteItem> m_RemoteItem;
    //boost::YOpt<SearchResult> m_SearchResult;
    boost::YOpt<Shared> m_Shared;
	boost::YOpt<SharepointIds> m_SharepointIds;
	int64_t m_TotalSize;
    //boost::YOpt<SpecialFolder> m_SpecialFolder;
    //boost::YOpt<Video> m_Video;
	PooledString m_WebDavUrl;
    PooledString m_WebUrl;

    // Non property fields
    bool m_IsFolder;
    PooledString m_OneDrivePath;
    PooledString m_DriveId;
    vector<BusinessDriveItem> m_DriveItemChildren;
	boost::YOpt<PooledString> m_ParentFolderId;
	boost::YOpt<CheckoutStatus> m_CheckoutStatus;

	boost::YOpt<PooledString> m_LocalFilePath;

	int m_HierarchyLevel; // Only for edition
	
    RTTR_ENABLE(BusinessObject)
	RTTR_REGISTRATION_FRIEND
    friend class DriveItemDeserializer;
    friend class DriveItemChildrenDeserializer;
};

