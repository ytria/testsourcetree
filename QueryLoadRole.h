#pragma once

#include "ISqlQuery.h"

class SessionsSqlEngine;

class QueryLoadRole : public ISqlQuery
{
public:
	QueryLoadRole(int64_t p_RoleId, SessionsSqlEngine& p_Engine);
	void Run() override;

	wstring GetRoleName() const;

private:
	int64_t m_RoleId;
	SessionsSqlEngine& m_Engine;

	wstring m_Name;
};

