#include "InformationalUrlDeserializer.h"

void InformationalUrlDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeString(_YTEXT("logoUrl"), m_Data.m_LogoUrl, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("marketingUrl"), m_Data.m_MarketingUrl, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("privacyStatementUrl"), m_Data.m_PrivacyStatementUrl, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("supportUrl"), m_Data.m_SupportUrl, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("termsOfServiceUrl"), m_Data.m_TermsOfServiceUrl, p_Object);
}
