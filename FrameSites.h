#pragma once

#include "GridFrameBase.h"

#include "BusinessSite.h"
#include "GridSites.h"

class FrameSites : public GridFrameBase
{
public:
	FrameSites(Origin p_Origin, const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame);
    virtual ~FrameSites() = default;
    DECLARE_DYNAMIC(FrameSites)

    void ShowSites(const vector<BusinessSite>& p_Sites, const vector<BusinessSite>& p_LoadedMoreSites, bool p_FromRefresh);
    void ShowSites(const O365DataMap<BusinessGroup, BusinessSite>& p_Sites, const vector<BusinessSite>& p_LoadedMoreSites, bool p_FullPurge);
	void ShowSites(const O365DataMap<BusinessChannel, BusinessSite>& p_Sites, const vector<BusinessSite>& p_LoadedMoreSites, bool p_FullPurge);

	void UpdateSitesLoadMore(const vector<BusinessSite>& p_Sites, bool p_FromRefresh);

    virtual O365Grid& GetGrid() override;
	virtual void RefreshSpecific(AutomationAction* p_CurrentAction) override;
	virtual void RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction) override;

    virtual void createGrid() override;

	virtual O365ControlConfig GetRefreshPartialControlConfig() override;

	// returns false if no frame specific tab is needed.
	virtual bool customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup) override;

	virtual void InitModuleCriteria(const ModuleCriteria& p_ModuleCriteria) override;

	// For Origin::Channel only!!!
	const std::map<PooledString, PooledString>& GetChannelNames() const;
	void SetChannelNames(const std::map<PooledString, PooledString>& p_ChannelNames);

protected:
    //DECLARE_MESSAGE_MAP()

	virtual AutomatedApp::AUTOMATIONSTATUS	automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action) override;

private:
    GridSites m_SitesGrid;
	std::map<PooledString, PooledString> m_ChannelNames;
};

