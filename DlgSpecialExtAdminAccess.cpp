#include "DlgSpecialExtAdminAccess.h"

#include "AddApplicationPasswordRequester.h"
#include "ApplicationListRequester.h"
#include "ApplicationRequester.h"
#include "AppResourceAccess.h"
#include "BasicRequestLogger.h"
#include "BasicPageRequestLogger.h"
#include "CreateAppRequester.h"
#include "DlgLoading.h"
#include "IDlgSpecialExtAdminJsonGenerator.h"
#include "JSONUtil.h"
#include "MainFrame.h"
#include "MFCUtil.h"
#include "NetworkUtils.h"
#include "O365AdminUtil.h"
#include "Office365Admin.h"
#include "OrganizationRequester.h"
#include "Product.h"
#include "RemoveApplicationPasswordRequester.h"
#include "RESTUtils.h"
#include "RunOnScopeEnd.h"
#include "Sapio365Session.h"
#include "SessionTypes.h"
#include "TimeUtil.h"
#include "UpdateAppRequester.h"

const wstring DlgSpecialExtAdminAccess::g_URL1 = _YTEXT("https://apps.dev.microsoft.com/");
wstring DlgSpecialExtAdminAccess::g_URL2;
const wstring DlgSpecialExtAdminAccess::g_URL3 = _YTEXT("ytriaurl://getconsent/");
wstring DlgSpecialExtAdminAccess::g_KeyNotShown;

const wstring DlgSpecialExtAdminAccess::g_Cancel = _YTEXT("CANCEL");
const wstring DlgSpecialExtAdminAccess::g_Ok = _YTEXT("OK");
const wstring DlgSpecialExtAdminAccess::g_ApplicationId = _YTEXT("applicationid");
const wstring DlgSpecialExtAdminAccess::g_SecretKey = _YTEXT("secretKey");
const wstring DlgSpecialExtAdminAccess::g_DisplayName = _YTEXT("displayname");
const wstring DlgSpecialExtAdminAccess::g_TenantName = _YTEXT("tenantname");
const wstring DlgSpecialExtAdminAccess::g_KeepSecretKey = _YTEXT("keepSecretKey");

namespace
{
	class ExternalFctsHandler : public CCmdTarget
	{
	public:
		ExternalFctsHandler(CWnd* p_Parent, std::shared_ptr<Sapio365Session> p_Session, YAdvancedHtmlView* p_HtmlView, DlgSpecialExtAdminAccess& p_Dlg)
			: m_Parent(p_Parent)
			, m_Session(p_Session)
			, m_HtmlView(p_HtmlView)
			, m_Dlg(p_Dlg)
		{
			EnableAutomation();
		}

		void CreateNewApplication()
		{
			auto appDisplayName = Util::PromptForNewApplicationName(
				m_Session	? Util::GetElevatedSessionAppName(SessionsSqlEngine::Get().Load(m_Session->GetIdentifier()))
							: YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_5, _YLOC("sapio365 Ultra Admin")).c_str()
				, m_Parent);
			
			if (!appDisplayName.empty()) // not canceled
			{
				m_Parent->EnableWindow(FALSE);
				auto dlgLoading = std::make_shared<DlgLoading>(YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_17, _YLOC("Creating new application...")).c_str());
				auto taskFinisher = std::make_shared<RunOnScopeEnd>([parent = m_Parent, dlgLoading]() {
					YCallbackMessage::DoPost([parent, dlgLoading]() {
							parent->EnableWindow(TRUE);
						});
					});
				if (!m_Session)
				{
					auto tempSession = Sapio365Session::CreateMSGraphSession();
					tempSession->Start(m_Parent).ThenMutable([this, dlgLoading, taskFinisher, appDisplayName, tempSession, sapioSession = m_Session, parent = m_Parent, htmlView = m_HtmlView](RestAuthenticationResult p_Result) mutable {
						if (p_Result.state == RestAuthenticationResult::State::Success)
						{
							YCallbackMessage::DoPost([dlgLoading]() { dlgLoading->DoModal(); });
							RunOnScopeEnd finisher([sapioSession, dlgLoading, parent]() {
								if (sapioSession)
									sapioSession->SetMSGraphSession(nullptr);
								YCallbackMessage::DoPost([dlgLoading]() { dlgLoading->EndDialog(IDOK); });
							});

							wstring password;
							wstring tenant;

							CreateApplicationRequester createAppRequester(appDisplayName, AppResourceAccess::GetUltraAdminResourceAccess());
							bool createdAppSuccess = false;
							try
							{
								// Create temporary Sapio365Session if needed
								if (!sapioSession)
									sapioSession = std::make_shared<Sapio365Session>();

								sapioSession->SetMSGraphSession(tempSession);
								createAppRequester.Send(sapioSession, YtriaTaskData()).GetTask().wait();

								wstring appId = createAppRequester.GetData().m_Id ? *createAppRequester.GetData().m_Id : _YTEXT("");
								createdAppSuccess = QueryApp(sapioSession, appId);

								if (createdAppSuccess)
								{
									AddApplicationPasswordRequester appPwdRequester(appDisplayName, appId);
									appPwdRequester.Send(sapioSession, YtriaTaskData()).GetTask().wait();

									createdAppSuccess = appPwdRequester.GetData().m_SecretText.is_initialized();
									if (createdAppSuccess)
										password = *appPwdRequester.GetData().m_SecretText;

									auto logger = std::make_shared<BasicPageRequestLogger>(_T("organization"));
									OrganizationRequester orgReq(false, logger);
									orgReq.Send(sapioSession, YtriaTaskData()).GetTask().wait();
									ASSERT(orgReq.GetData().size() == 1);
									if (orgReq.GetData().size() == 1)
										tenant = orgReq.GetData()[0].GetInitialDomain();
								}
							}
							catch (const std::exception& e)
							{
								ASSERT(false);
								LoggerService::Debug(YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_42, _YLOC("An error occurred while creating application: "), MFCUtil::convertUTF8_to_UNICODE(e.what()).c_str()));
							}

							const auto& res = createAppRequester.GetResult();
							ASSERT(res.GetResult().Response.status_code() == 201 && createdAppSuccess);
							if (res.GetResult().Response.status_code() == 201 && createdAppSuccess)
							{
								ASSERT(createAppRequester.GetData().m_AppId);
								if (createAppRequester.GetData().m_AppId)
									m_Dlg.SetCreatedAppId(*createAppRequester.GetData().m_AppId);
								YCallbackMessage::DoPost([=]() {
									
									htmlView->ExecuteScript(_YTEXTFORMAT(LR"~(setApplicationInfo("%s", "%s", "%s", "%s");)~", createAppRequester.GetData().m_AppId ? createAppRequester.GetData().m_AppId->c_str() : _YTEXT(""), password.c_str(), tenant.c_str(), createAppRequester.GetData().m_DisplayName ? createAppRequester.GetData().m_DisplayName->c_str() : _YTEXT("")));

									YCodeJockMessageBox resultDlg(parent,
										DlgMessageBox::eIcon_Information,
										YtriaTranslate::Do(DlgRegConnectionCheck_successState_1, _YLOC("Success!")).c_str(),
										YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_18, _YLOC("A new application has been successfully created on the related Azure AD.")).c_str(),
										YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_19, _YLOC("Don�t forget to click the Provide Admin Consent button.")).c_str(),
										{ { IDOK, YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_20, _YLOC("OK")).c_str() } });
									resultDlg.DoModal();
								});
							}
							else
							{
								YCallbackMessage::DoPost([=]() {
									YCodeJockMessageBox dlg(parent
										, DlgMessageBox::eIcon::eIcon_Error
										, YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_38, _YLOC("Can't create new application")).c_str()
										, res.GetResult().GetErrorMessage()
										, _YTEXT("")
										, { { IDOK, _YTEXT("OK") } });

									dlg.DoModal();
								});
							}
						}
					});
				}
				else
				{
					YSafeCreateTask([=]()
						{
							YCallbackMessage::DoPost([dlgLoading]()
								{
									dlgLoading->DoModal();
								});

							RunOnScopeEnd finisher([dlgLoading]()
								{
									YCallbackMessage::DoPost([dlgLoading]() { dlgLoading->EndDialog(IDOK); });
								});

							wstring password = Str::GeneratePassword(64, false);
							CreateApplicationRequester createAppRequester(appDisplayName, AppResourceAccess::GetUltraAdminResourceAccess());
							createAppRequester.Send(m_Session, YtriaTaskData()).GetTask().wait();

							wstring appId = createAppRequester.GetData().m_Id ? createAppRequester.GetData().m_Id->c_str() : _YTEXT("");
							bool createdAppSuccess = QueryApp(m_Session, appId);

							if (createdAppSuccess)
							{
								AddApplicationPasswordRequester appPwdRequester(appDisplayName, appId);
								appPwdRequester.Send(m_Session, YtriaTaskData()).GetTask().wait();

								createdAppSuccess = appPwdRequester.GetData().m_SecretText.is_initialized();
								if (createdAppSuccess)
									password = *appPwdRequester.GetData().m_SecretText;
							}

							const auto& res = createAppRequester.GetResult();
							ASSERT(res.GetResult().Response.status_code() == 201 && createdAppSuccess);
							if (res.GetResult().Response.status_code() == 201 && createdAppSuccess)
							{
								wstring tenant;
								auto logger = std::make_shared<BasicPageRequestLogger>(_T("organization"));
								OrganizationRequester orgReq(false, logger);
								orgReq.Send(m_Session, YtriaTaskData()).GetTask().wait();
								ASSERT(orgReq.GetData().size() == 1);
								if (orgReq.GetData().size() == 1)
									tenant = orgReq.GetData()[0].GetInitialDomain();

								ASSERT(createAppRequester.GetData().m_AppId);
								if (createAppRequester.GetData().m_AppId)
									m_Dlg.SetCreatedAppId(*createAppRequester.GetData().m_AppId);
								YCallbackMessage::DoPost([=]()
									{
										m_HtmlView->ExecuteScript(_YTEXTFORMAT(LR"~(setApplicationInfo("%s", "%s", "%s", "%s");)~", createAppRequester.GetData().m_AppId->c_str(), password.c_str(), tenant.c_str(), createAppRequester.GetData().m_DisplayName ? createAppRequester.GetData().m_DisplayName->c_str() : _YTEXT("")));

										YCodeJockMessageBox resultDlg(m_Parent,
											DlgMessageBox::eIcon_Information,
											YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_21, _YLOC("Success!")).c_str(),
											YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_18, _YLOC("A new application has been successfully created on the related Azure AD.")).c_str(),
											YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_19, _YLOC("Don�t forget to click the Provide Admin Consent button.")).c_str(),
											{ { IDOK, YtriaTranslate::Do(CommandDispatcher_Execute_3, _YLOC("OK")).c_str() } });
										resultDlg.DoModal();
									});
							}
							else
							{
								YCodeJockMessageBox dlg(m_Parent
									, DlgMessageBox::eIcon::eIcon_Error
									, YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_38, _YLOC("Can't create new application")).c_str()
									, res.GetResult().GetErrorMessage()
									, _YTEXT("")
									, { { IDOK, YtriaTranslate::Do(DlgLicenseUpgradeReport_generateJSONScriptData_7, _YLOC("OK")).c_str() } });
								dlg.DoModal();
							}
						});
				}
			}
		}

		void ResetSecretKey(LPCWSTR p_AppId, LPCWSTR p_TenantName)
		{
			wstring appId(p_AppId);

			YCodeJockMessageBox dlg(m_Parent,
				DlgMessageBox::eIcon_Information,
				_YTEXT("sapio365"),
				YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_26, _YLOC("Are you sure you want to generate a new password for this application?")).c_str(),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_27, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_28, _YLOC("Cancel")).c_str() } });
			if (dlg.DoModal() != IDOK)
				return;
			
			m_Parent->EnableWindow(FALSE);
			auto dlgLoading = std::make_shared<DlgLoading>(YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeepSecretKey_29, _YLOC("Updating application...")).c_str());
			auto taskFinisher = std::make_shared<RunOnScopeEnd>([parent = m_Parent, dlgLoading]() {
				YCallbackMessage::DoPost([parent, dlgLoading]() {
					parent->EnableWindow(TRUE);
				});
			});

			if (!m_Session
				|| !m_Session->IsConnected()
				|| m_Session->GetTenantName(true) != p_TenantName)
			{
				auto tempSession = Sapio365Session::CreateMSGraphSession();
				tempSession->Start(m_Parent).ThenMutable([this, appId, dlgLoading, taskFinisher, tempSession, parent = m_Parent, htmlView = m_HtmlView](RestAuthenticationResult p_Result) mutable {
					if (p_Result.state == RestAuthenticationResult::State::Success)
					{
						// Create temporary Sapio365Session if needed
						auto sapioSession = std::make_shared<Sapio365Session>();

						sapioSession->SetMSGraphSession(tempSession);

						YSafeCreateTask([=]() {
							GeneratePassword(sapioSession, dlgLoading, appId, m_Parent);
						});
					}
				});
			}
			else
			{
				YSafeCreateTask([this, dlgLoading, appId]() {
					GeneratePassword(m_Session, dlgLoading, appId, m_Parent);
				});
			}
			
		}

		void GeneratePassword(const shared_ptr<Sapio365Session>& p_Session, const shared_ptr<DlgLoading>& dlgLoading, const wstring& appId, CWnd* p_Parent)
		{
			YCallbackMessage::DoPost([dlgLoading]() { dlgLoading->DoModal(); });

			const auto res = Office365AdminApp::RegenerateAppPassword(p_Parent, appId, p_Session, dlgLoading);
			if (res.first)
			{
				YCallbackMessage::DoPost([this, res]() {
					m_HtmlView->ExecuteScript(_YTEXTFORMAT(LR"~(setNewKey("%s");)~", res.second.c_str()));
				});
			}
		}

		bool QueryApp(const shared_ptr<Sapio365Session>& p_Session, const wstring& p_Id)
		{
			bool success = false;

			auto logger = std::make_shared<BasicRequestLogger>(_T("application"));

			ApplicationRequester appReq(p_Id, logger);
			appReq.Send(p_Session, YtriaTaskData()).GetTask().wait();

			while (appReq.GetResult().GetResult().Response.status_code() != 200)
			{
				appReq.Send(p_Session, YtriaTaskData()).GetTask().wait();

				if (appReq.GetResult().GetResult().Response.status_code() != 404 &&
					appReq.GetResult().GetResult().Response.status_code() != 200)
				{
					wstring exceptMessage;
					if (appReq.GetResult().GetResult().m_Exception)
						exceptMessage = MFCUtil::convertUTF8_to_UNICODE(appReq.GetResult().GetResult().m_Exception->what());
					else if (appReq.GetResult().GetResult().m_RestException)
						exceptMessage = appReq.GetResult().GetResult().m_RestException->WhatUnicode();
					LoggerService::Debug(wstring(_YTEXT("Querying created application failed: ")) + exceptMessage);
					break;
				}
				pplx::wait(1000);
			}

			success = appReq.GetResult().GetResult().Response.status_code() == 200;

			return success;
		}

	private:
		DECLARE_DISPATCH_MAP()

		std::shared_ptr<Sapio365Session> m_Session;
		CWnd* m_Parent;
		YAdvancedHtmlView* m_HtmlView;
		DlgSpecialExtAdminAccess& m_Dlg;
	};

	BEGIN_DISPATCH_MAP(ExternalFctsHandler, CCmdTarget)
		DISP_FUNCTION(ExternalFctsHandler, "createNewApplication", CreateNewApplication, VT_EMPTY, VTS_NONE)
		DISP_FUNCTION(ExternalFctsHandler, "resetAppKey", ResetSecretKey, VT_EMPTY, VTS_WBSTR VTS_WBSTR)
	END_DISPATCH_MAP()
}

std::map<wstring, int, Str::keyLessInsensitive> DlgSpecialExtAdminAccess::g_Sizes;

DlgSpecialExtAdminAccess::DlgSpecialExtAdminAccess(const wstring& p_Tenant, const wstring& p_AppId, const wstring& p_AppClientSecret, const wstring& p_AppDisplayName, CWnd* p_Parent, shared_ptr<Sapio365Session> p_Session, std::unique_ptr<IDlgSpecialExtAdminJsonGenerator> p_JsonGenerator, bool p_EditOnlySecretKey)
    : ResizableDialog(IDD, p_Parent)
	, m_HtmlView(make_unique<YAdvancedHtmlView>(true))
	, m_Tenant(p_Tenant)
	, m_ClientSecret(p_AppClientSecret)
	, m_DisplayName(p_AppDisplayName)
	, m_AppId(p_AppId)
	, m_SaveSecretKey(false)
	, m_Session(p_Session)
	, m_JsonGenerator(std::move(p_JsonGenerator))
	, m_EditOnlySecretKey(p_EditOnlySecretKey)
	, m_Title(YtriaTranslate::Do(DlgSpecialExtAdminAccess_OnInitDialogSpecificResizable_1, _YLOC("Ultra Admin Session Activation")).c_str())
	, m_AdditionalHeight(0)
	, m_RemoveRequested(false)
{
	m_HtmlView->SetExternal(std::make_shared<ExternalFctsHandler>(this, p_Session, m_HtmlView.get(), *this));
	m_SaveSecretKey = !m_ClientSecret.empty();

	if (g_Sizes.empty())
	{
		const bool success = Str::LoadSizeFile(IDR_FULLADMINLOGIN_SIZE, g_Sizes);
		ASSERT(success);
	}
	if (g_KeyNotShown.empty())
		g_KeyNotShown = YtriaTranslate::Do(DlgSpecialExtAdminAccess_g_KeyNotShown_1, _YLOC("Application Secret Password Not Shown When Saved")).c_str();
}

void DlgSpecialExtAdminAccess::SetTitle(const wstring& p_Title)
{
	m_Title = p_Title;
}

void DlgSpecialExtAdminAccess::SetAdditionalHeight(int p_Additionalheight)
{
	m_AdditionalHeight = p_Additionalheight;
}

bool DlgSpecialExtAdminAccess::OnNewPostedData(const IPostedDataTarget::PostedData& data)
{
	const wstring& btnClicked = data.begin()->first;
	ASSERT(data.size() >= 1);

	bool retVal = true;

	if (data.find(_YTEXT("remove")) != data.end())
	{
		m_RemoveRequested = true;
		retVal = false;
		endDialogFixed(IDOK);
	}
	else
	{
		if (btnClicked == _YTEXT("CANCEL"))
		{
			endDialogFixed(IDCANCEL);
			retVal = false;
		}
		else if (btnClicked == _YTEXT("OK"))
		{
			m_SaveSecretKey = false;
			for (const auto& Chunk : data)
			{
				if (Chunk.first == g_ApplicationId)
					m_AppId = Chunk.second;
				else if (Chunk.first == g_SecretKey && Chunk.second != g_KeyNotShown)
					m_ClientSecret = Chunk.second;
				else if (Chunk.first == g_TenantName)
					m_Tenant = Chunk.second;
				else if (Chunk.first == g_KeepSecretKey)
					m_SaveSecretKey = true;
				else if (Chunk.first == g_DisplayName)
					m_DisplayName = Chunk.second;
			}

			endDialogFixed(IDOK);
			retVal = false;
		}
	}

	return retVal;
}

bool DlgSpecialExtAdminAccess::OnNewPostedURL(const wstring& url)
{
	ASSERT(!url.empty());
	if (!url.empty())
	{
		if (url.find(g_URL3) == 0)
		{
			const wstring test(url.substr(g_URL3.size()));
			const auto pos1 = test.find(_YTEXT("/"));
			const wstring tenant(test.substr(0, pos1));
			const auto pos2 = test.find(_YTEXT("/"), pos1 + 1);
			const wstring appID(test.substr(pos1 + 1, pos2 - pos1 - 1));
			OnGetConsentForApp(tenant, appID);
		}
		else if (url.find(_YTEXT("https://")) == 0 || url.find(_YTEXT("http://")) == 0)
			::ShellExecute(NULL, _YTEXT("open"), url.c_str(), NULL, NULL, SW_SHOWNORMAL);
	}

	return true; // Maybe should be void.
}

const wstring& DlgSpecialExtAdminAccess::GetAppId() const
{
	return m_AppId;
}

const wstring& DlgSpecialExtAdminAccess::GetClientSecret() const
{
	return m_ClientSecret;
}

const wstring & DlgSpecialExtAdminAccess::GetTenant() const
{
	return m_Tenant;
}

const wstring& DlgSpecialExtAdminAccess::GetDisplayName() const
{
	return m_DisplayName;
}

bool DlgSpecialExtAdminAccess::GetSaveSecretKey() const
{
	return m_SaveSecretKey;
}

bool DlgSpecialExtAdminAccess::IsRemoveRequested() const
{
	return m_RemoveRequested;
}

void DlgSpecialExtAdminAccess::SetCreatedAppId(const wstring& p_AppId)
{
	m_CreatedAppId = p_AppId;
}

const wstring& DlgSpecialExtAdminAccess::GetCreatedAppId() const
{
	return m_CreatedAppId;
}

BOOL DlgSpecialExtAdminAccess::OnInitDialogSpecificResizable()
{
	SetWindowText(m_Title.c_str());

	CWnd* pCtrl = GetDlgItem(IDC_GRID_PLACEHOLDER);
	ASSERT(nullptr != pCtrl);

	CRect htmlRect;
	pCtrl->GetWindowRect(&htmlRect);
	pCtrl->ShowWindow(SW_HIDE);
	pCtrl->DestroyWindow();
	ScreenToClient(&htmlRect);

	ASSERT(m_HtmlView);

	m_HtmlView->Create(NULL, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, htmlRect, this, IDC_GRID_PLACEHOLDER, NULL);

	// FIXME: Temporary - Remove when no more needed.
	//m_HtmlView->SetDebugDisplayPostedData(true);

	m_HtmlView->SetPostedDataTarget(this);
	m_HtmlView->SetPostedURLTarget(this);
	m_HtmlView->AddURL(g_URL1);
	if (g_URL2.empty())
		g_URL2 = Product::getURLHelpWebSitePage(_YTEXT("Help/dialog-how-to-create-application-ID"));
	m_HtmlView->AddURL(g_URL2);
	m_HtmlView->AddURL(g_URL3);

	wstring jsonScriptData;
	generateJSONScriptData(jsonScriptData);

	m_HtmlView->SetScriptsToReplace(
		{
			 { _YTEXT("JSON")		,{ _YTEXT(""), _YTEXT(""), jsonScriptData.c_str() } }
			,{ _YTEXT("HBS-JS")	    ,{ _YTEXT("text/javascript"), _YTEXT("hbs-fullAdminLogin.js"), _YTEXT("") } } // This file is in resources
			,{ _YTEXT("OTHER-JS")	,{ _YTEXT("text/javascript"), _YTEXT("fullAdminLogin.js"), _YTEXT("") } }, // This file is in resources
		}
	);

	m_HtmlView->SetLinksToReplace({ { _YTEXT("OTHER-CSS"),{ _YTEXT("stylesheet"), _YTEXT("fullAdminLogin.css") } } }); // This file is in resources
	m_HtmlView->YLoadFromResource(_YTEXT("hbshandler.html"));

	AddAnchor(*m_HtmlView, CSize(0, 0), CSize(100, 100));

	CRect dlgRect;
	GetWindowRect(dlgRect);

	HMONITOR monitor = MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	const int height = [=]()
	{
		const int targetHtmlHeight = [=]()
		{
			const auto it = g_Sizes.find(wstring(_YTEXT("min-height")) + (m_EditOnlySecretKey ? _YTEXT("-small") : _YTEXT("")));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_YH(it->second);
			return htmlRect.Height();
		}();

		const int maxHeight = info.rcMonitor.bottom - info.rcMonitor.top;
		const int deltaH = targetHtmlHeight + m_AdditionalHeight - htmlRect.Height();
		return min(maxHeight, dlgRect.Height() + deltaH);
	}();

	const int width = [=]()
	{
		const int targetHtmlWidth = [=]()
		{
			const auto it = g_Sizes.find(_YTEXT("min-width"));
			ASSERT(g_Sizes.end() != it);
			if (g_Sizes.end() != it)
				return HIDPI_XW(it->second);
			return htmlRect.Width();
		}();

		const int maxWidth = info.rcMonitor.right - info.rcMonitor.left;
		const int deltaW = targetHtmlWidth - htmlRect.Width();
		return min(maxWidth, dlgRect.Width() + deltaW);
	}();

	SetMinSize(CPoint(width, height));
	SetWindowPos(nullptr, 0, 0, width, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
	BlockVResize(true);

    return TRUE;
}

void DlgSpecialExtAdminAccess::generateJSONScriptData(wstring& p_Output)
{
	p_Output = YAdvancedHtmlView::MakeJavascriptConfigVar(m_JsonGenerator->Generate(*this).serialize());
}

void DlgSpecialExtAdminAccess::OnGetConsentForApp(const wstring& p_Tenant, const wstring& p_AppID/*, const wstring& p_RedirectURL*/)
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);
	if (nullptr != mainFrame)
		mainFrame->GiveConsentForApp(p_Tenant, p_AppID, this);
}
