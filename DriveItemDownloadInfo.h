#pragma once

#include "FileExistsAction.h"

struct DriveItemDownloadInfo 
{
    DriveItemDownloadInfo()
    {
        m_PreserveHierarchy = false;
        m_FileExistsAction = FileExistsAction::Append;
    }
    PooledString m_DriveId;
    PooledString m_DriveItemId;
    PooledString m_Filename;
    PooledString m_Filepath;
    PooledString m_DrivePath;
    PooledString m_OwnerName;
    bool         m_PreserveHierarchy;
    bool         m_OpenAfterDownload;
    YTimeDate    m_CreatedDateTime;
    YTimeDate    m_ModifiedDateTime;
    GridBackendRow* m_Row;
    FileExistsAction m_FileExistsAction;
};