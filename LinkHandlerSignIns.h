#pragma once

#include "IBrowserLinkHandler.h"

class LinkHandlerSignIns : public IBrowserLinkHandler
{
public:
	void Handle(YBrowserLink& p_Link) const override;
};
