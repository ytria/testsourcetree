#pragma once

#include "ordered_map.h"
#include <tuple>

class BatchImporter
{
public:
	struct Tip
	{
		wstring m_Text;
		bool m_AlwaysShown = false;
	};
	// {propname, {displayName, isMandatory, tip}}
	using MappingProperties = tsl::ordered_map<wstring, std::tuple<wstring, bool, Tip>>;
	// {propname, displayName}
	using KeyProperties = tsl::ordered_map<wstring, wstring>;

	enum ObjectType
	{
		User,
		Group,
	};

	static bool DoCreate(ObjectType p_ObjectType, const MappingProperties& p_MappingProperties, std::function<wstring(std::map<wstring, wstring>)> p_Callback, CWnd* p_Parent);
	static bool DoUpdate(ObjectType p_ObjectType, const KeyProperties& p_KeyProperties, const MappingProperties& p_MappingProperties, std::function<wstring(std::pair<wstring, wstring>, std::map<wstring, wstring>)> p_Callback, CWnd* p_Parent);
};