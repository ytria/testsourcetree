#include "GenericRevertAllButtonUpdater.h"

#include "GridFrameBase.h"
#include "BaseO365Grid.h"

GenericRevertAllButtonUpdater::GenericRevertAllButtonUpdater(GridFrameBase& p_Frame)
	:m_Frame(p_Frame)
{
}

void GenericRevertAllButtonUpdater::Update(CCmdUI* p_CmdUi)
{
	p_CmdUi->Enable(m_Frame.HasNoTaskRunning() && m_Frame.GetGrid().IsGridModificationsEnabled() && m_Frame.GetGrid().GetModifications().HasModifications());
}
