#pragma once
#include "OptionalClaim.h"

class OptionalClaims
{
public:
	boost::YOpt<vector<OptionalClaim>> m_IdToken;
	boost::YOpt<vector<OptionalClaim>> m_AccessToken;
	boost::YOpt<vector<OptionalClaim>> m_Saml2Token;
};

