#include "LinkHandlerJobScheduleDelete.h"

#include "MainFrame.h"

void LinkHandlerJobScheduleDelete::Handle(YBrowserLink& p_Link) const
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame && mainFrame->GetAutomationWizard() != nullptr);
	if (nullptr != mainFrame && mainFrame->GetAutomationWizard() != nullptr)
	{
		auto schID	= p_Link.GetUrl().path().substr(1);// .substr(1): path starts with '/'
		auto sch	= mainFrame->GetAutomationWizard()->GetScriptScheduleByID(schID);
		ASSERT(sch.HasID());
		if (sch.HasID())
		{
			YCodeJockMessageBox dlg(mainFrame,
				DlgMessageBox::eIcon_Question,
				_T("Remove Task?"),
				_YFORMAT(L"This will remove the selected task \"%s\" from Windows Scheduler.", sch.m_Key.c_str()),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() }, { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_9, _YLOC("Cancel")).c_str() } });
			if (dlg.DoModal() == IDOK)
				mainFrame->GetAutomationWizard()->RemoveScriptSchedule(sch);
		}
		else
			YCodeJockMessageBox(mainFrame,
				DlgMessageBox::eIcon_ExclamationWarning,
				_T("Error"),
				_T("Schedule not found"),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } }).DoModal();
	}

}