#include "RelatedContactDeserializer.h"

void RelatedContactDeserializer::DeserializeObject(const web::json::object& p_Object)
{
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, false);
	JsonSerializeUtil::DeserializeString(_YTEXT("displayName"), m_Data.m_DisplayName, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("emailAddress"), m_Data.m_EmailAddress, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("mobilePhone"), m_Data.m_MobilePhone, p_Object);
	JsonSerializeUtil::DeserializeString(_YTEXT("relationship"), m_Data.m_Relationship, p_Object);
	JsonSerializeUtil::DeserializeBool(_YTEXT("accessConsent"), m_Data.m_AccessConsent, p_Object);
}
