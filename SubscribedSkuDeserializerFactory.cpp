#include "SubscribedSkuDeserializerFactory.h"
#include "Sapio365Session.h"

SubscribedSkuDeserializerFactory::SubscribedSkuDeserializerFactory(std::shared_ptr<const Sapio365Session> p_Session)
	: m_Session(p_Session)
{
}

SubscribedSkuDeserializer SubscribedSkuDeserializerFactory::Create()
{
    return SubscribedSkuDeserializer(m_Session);
}
