#include "SoapDocument.h"

#include "xercesc\dom\DOMDocumentFragment.hpp"
#include "xercesc\dom\DOMElement.hpp"
#include "xercesc\dom\DOMNode.hpp"
#include "xercesc\framework\MemBufInputSource.hpp"
#include "xercesc\parsers\XercesDOMParser.hpp"
#include "xercesc\util\PlatformUtils.hpp"

#include "IXmlSerializer.h"
#include "TodoXmlErrorReporter.h"
#include "xercesc\dom\DOMException.hpp"
#include "XMLUtil.h"

using XERCES_CPP_NAMESPACE::DOMNode;
using XERCES_CPP_NAMESPACE::MemBufInputSource;

const wstring Ex::SoapDocument::TypesNs = _YTEXT("http://schemas.microsoft.com/exchange/services/2006/types");
const wstring Ex::SoapDocument::MessagesNs = _YTEXT("http://schemas.microsoft.com/exchange/services/2006/messages");

std::unique_ptr<XercesDOMParser, Ex::SoapDocument::XercesDOMParserDeleter> Ex::SoapDocument::g_SkeletonParser;

Ex::SoapDocument::SoapDocument()
{
    if (nullptr == g_SkeletonParser)
    {
        XERCES_CPP_NAMESPACE::XMLPlatformUtils::Initialize(); // Terminate called in unique_ptr deleter

        // Init soap document only once
        static const XMLByte soapSkeleton[] =
            R"(<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:m="http://schemas.microsoft.com/exchange/services/2006/messages"
                xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types"
                xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Header>
                    <t:RequestServerVersion Version="Exchange2007_SP1" />
                </soap:Header>
                <soap:Body>
                </soap:Body>
            </soap:Envelope>
        )";

        g_SkeletonParser = std::unique_ptr<XercesDOMParser, XercesDOMParserDeleter>(new XercesDOMParser);
        g_SkeletonParser->setDoSchema(true);
        g_SkeletonParser->setValidationScheme(XercesDOMParser::Val_Never);
        g_SkeletonParser->setDoNamespaces(true);
        g_SkeletonParser->setLoadExternalDTD(false);

        TodoXmlErrorReporter errorHandler;
        g_SkeletonParser->setErrorHandler(&errorHandler);

        MemBufInputSource inputSource(soapSkeleton, sizeof(soapSkeleton) / sizeof(XMLByte), _YTEXT("SoapSkeletonSerializer"));
        g_SkeletonParser->parse(inputSource);

        ASSERT(!errorHandler.HasError());
        if (errorHandler.HasError())
            throw std::exception("Unable to parse soap document skeleton.");
    }

    // Clone the skeleton's root node into m_Envelope
    auto document = g_SkeletonParser->getDocument();
    ASSERT(nullptr != document);
    if (nullptr != document)
    {
        m_Document = dynamic_cast<XERCES_CPP_NAMESPACE::DOMDocument*>(document->cloneNode(true));
        ASSERT(nullptr != m_Document);
        if (nullptr != m_Document)
        {
            m_Envelope = dynamic_cast<DOMElement*>(m_Document->getDocumentElement());
            ASSERT(nullptr != m_Envelope);
            if (nullptr != m_Envelope)
            {
                m_Header = m_Envelope->getFirstElementChild();
                m_Body = m_Envelope->getLastElementChild();

                ASSERT(nullptr != m_Header && m_Body != nullptr);
                if (nullptr == m_Header || m_Body == nullptr)
                    throw std::exception("Unable to read header or body of soap document.");
            }
        }
    }
}

Ex::SoapDocument::~SoapDocument()
{
    m_Document->release();
}

void Ex::SoapDocument::AddToBody(IXmlSerializer& p_Serializer)
{
    p_Serializer.Serialize(*m_Body);
}

void Ex::SoapDocument::AddToBody(DOMElement* p_Element)
{
    auto appended = m_Body->appendChild(p_Element);
    ASSERT(nullptr != appended);
}

XERCES_CPP_NAMESPACE::DOMDocument* Ex::SoapDocument::GetDocument() const
{
    return m_Envelope->getOwnerDocument();
}

DOMDocumentFragment* Ex::SoapDocument::CreateFragment() const
{
    return m_Envelope->getOwnerDocument()->createDocumentFragment();
}


